@echo off
rem -------------------------------------------------------------------------
rem Omega Build System
rem -------------------------------------------------------------------------

if "%1"=="core" goto CORE
if "%1"=="-dbip" goto DBIP

if "%1"=="ofs" goto OFS
if "%1"=="ndoh" goto NDOH
if "%1"=="wspha" goto WSPHA
if "%1"=="sjnkgh" goto SJNKGH
if "%1"=="ndoh241" goto NDOH241
if "%1"=="vangh" goto VANGH
if "%1"=="mengh" goto MENGH
if "%1"=="sjngh" goto SJNGH
if "%1"=="anmgh" goto ANMGH
if "%1"=="hagen" goto HAGEN
if "%1"=="bsmart" goto BSMART
if "%1"=="test" goto TEST
if "%1"=="obcl" goto OBCL
if "%1"=="obci" goto OBCI
if "%1"=="hms" goto HMS
if "%1"=="hmst" goto HMST
if "%1"=="pgh" goto PGH
if "%1"=="pmgh" goto PMGH
if "%1"=="xmind" goto XMIND
if "%1"=="xmind2" goto XMIND2
if "%1"=="demo" goto DEMO
if "%1"=="tbx" goto TBX
if "%1"=="pcspc" goto PCSPC
if "%1"=="amon" goto AMON
if "%1"=="albergus" goto ALBERGUS
if "%1"=="tbx2" goto TBX2
if "%1"=="tmc" goto TMC
if "%1"=="fundline" goto FUNDLINE
if "%1"=="vps" goto VPS
if "%1"=="rsa" goto RSA
if "%1"=="wv" goto WV
if "%1"=="tumnir" goto TUMNIR

if "%1"=="hcl" goto HCL
if "%1"=="hppl" goto HPPL
if "%1"=="sjtl" goto SJTL
if "%1"=="camh" goto CAMH
if "%1"=="camh2" goto CAMH2
if "%1"=="sun" goto SUN
if "%1"=="jva" goto JVA
if "%1"=="dynast" goto DYNAST
if "%1"=="dynast2" goto DYNAST2
if "%1"=="dof" goto DOF
if "%1"=="24h" goto 24H
if "%1"=="24h2" goto 24H2
if "%1"=="claire" goto CLAIRE
if "%1"=="tpe" goto TPE
if "%1"=="tpe2" goto TPE2
if "%1"=="hmpa" goto HMPA

if "%1"=="ml" goto ML
if "%1"=="ncl" goto NCL
if "%1"=="kcg" goto KCG
if "%1"=="xyz" goto XYZ
if "%1"=="ssj" goto SSJ


if "%1"=="jrh" goto JRH
if "%1"=="jrhp" goto JRHP
if "%1"=="jrhlmb" goto JRHLMB
if "%1"=="jrhmt" goto JRHMT
if "%1"=="jrhorp" goto JRHORP
if "%1"=="jrhp" goto JRHP
if "%1"=="jrhorp" goto JRHORP
if "%1"=="tru" goto TRU
if "%1"=="superjani" goto SUPERJANI
if "%1"=="courts" goto COURTS
if "%1"=="daltron" goto DALTRON
if "%1"=="hardware" goto HARDWARE
if "%1"=="report" goto REPORT

if "%1"=="ccdf" goto CCDF
if "%1"=="eda" goto EDA
if "%1"=="ssj" goto SSJ
if "%1"=="test" goto TEST
if "%1"=="casas2" goto CASAS2
if "%1"=="icei" goto ICEI
if "%1"=="icei2018" goto ICEI2018


if "%1"=="superjani" goto SUPERJANI
if "%1"=="jid" goto JID

:CORE
echo 			================ BUILDING CORE ================
"%JAVA5_HOME%/bin/java" -Xms512m -Xmx1024m -cp c:/opt/apache-ant-1.6.1/lib/ant.jar;c:/opt/apache-ant-1.6.1/lib/ant-launcher.jar;c:/opt/apache-ant-1.6.1/lib/ant-junit.jar;c:/opt/apache-ant-1.6.1/lib/junit/junit.jar;c:/opt/apache-ant-1.6.1/lib/ant-swing.jar;"%JAVA5_HOME%"/lib/tools.jar org.apache.tools.ant.Main -f build.bin.xml
if "%1"=="core" goto END

:OFS
echo 			 ============= BUILDING OFS =============
"%JAVA5_HOME%/bin/java" -Xms512m -Xmx1024m -cp c:/opt/apache-ant-1.6.1/lib/ant.jar;c:/opt/apache-ant-1.6.1/lib/ant-launcher.jar;c:/opt/apache-ant-1.6.1/lib/ant-junit.jar;c:/opt/apache-ant-1.6.1/lib/junit/junit.jar;c:/opt/apache-ant-1.6.1/lib/ant-swing.jar;"%JAVA5_HOME%"/lib/tools.jar org.apache.tools.ant.Main -f build.bin.xml deploy -Dcustom.name=ofs -Dcontextroot.name=ofs -Ddbhost.name=localhost -Ddb.name=OFS -Djboss.dir=/opt/jboss-4.0.3SP1 -DexpirationDay=-1 -Ddb.username=root -Ddb.password=asdf
if "%1"=="ofs" goto END

:NDOH
echo 			 ============= BUILDING NDOH =============
"%JAVA5_HOME%/bin/java" -Xms512m -Xmx1024m -cp c:/opt/apache-ant-1.6.1/lib/ant.jar;c:/opt/apache-ant-1.6.1/lib/ant-launcher.jar;c:/opt/apache-ant-1.6.1/lib/ant-junit.jar;c:/opt/apache-ant-1.6.1/lib/junit/junit.jar;c:/opt/apache-ant-1.6.1/lib/ant-swing.jar;"%JAVA5_HOME%"/lib/tools.jar org.apache.tools.ant.Main -f build.bin.xml deploy -Dcustom.name=ndoh -Dcontextroot.name=ndoh -Ddbhost.name=localhost -Ddb.name=NDOH -Djboss.dir=/opt/jboss-4.0.3SP1 -DexpirationDay=-1 -Ddb.username=root -Ddb.password=asdf
if "%1"=="ndoh" goto END

:WSPHA
echo 			 ============= BUILDING WSPHA =============
"%JAVA5_HOME%/bin/java" -Xms512m -Xmx1024m -cp c:/opt/apache-ant-1.6.1/lib/ant.jar;c:/opt/apache-ant-1.6.1/lib/ant-launcher.jar;c:/opt/apache-ant-1.6.1/lib/ant-junit.jar;c:/opt/apache-ant-1.6.1/lib/junit/junit.jar;c:/opt/apache-ant-1.6.1/lib/ant-swing.jar;"%JAVA5_HOME%"/lib/tools.jar org.apache.tools.ant.Main -f build.bin.xml deploy -Dcustom.name=wspha -Dcontextroot.name=wspha -Ddbhost.name=localhost -Ddb.name=WSPHA -Djboss.dir=/opt/jboss-4.0.3SP1 -DexpirationDay=-1 -Ddb.username=root -Ddb.password=asdf
if "%1"=="wspha" goto END

:SJNKGH
echo 			 ============= BUILDING SJNKGH =============
"%JAVA5_HOME%/bin/java" -Xms512m -Xmx1024m -cp c:/opt/apache-ant-1.6.1/lib/ant.jar;c:/opt/apache-ant-1.6.1/lib/ant-launcher.jar;c:/opt/apache-ant-1.6.1/lib/ant-junit.jar;c:/opt/apache-ant-1.6.1/lib/junit/junit.jar;c:/opt/apache-ant-1.6.1/lib/ant-swing.jar;"%JAVA5_HOME%"/lib/tools.jar org.apache.tools.ant.Main -f build.bin.xml deploy -Dcustom.name=sjnkgh -Dcontextroot.name=sjnkgh -Ddbhost.name=localhost -Ddb.name=SJNKGH -Djboss.dir=/opt/jboss-4.0.3SP1 -DexpirationDay=-1 -Ddb.username=root -Ddb.password=asdf
if "%1"=="sjnkgh" goto END

:NDOH241
echo 			 ============= BUILDING NDOH241 =============
"%JAVA5_HOME%/bin/java" -Xms512m -Xmx1024m -cp c:/opt/apache-ant-1.6.1/lib/ant.jar;c:/opt/apache-ant-1.6.1/lib/ant-launcher.jar;c:/opt/apache-ant-1.6.1/lib/ant-junit.jar;c:/opt/apache-ant-1.6.1/lib/junit/junit.jar;c:/opt/apache-ant-1.6.1/lib/ant-swing.jar;"%JAVA5_HOME%"/lib/tools.jar org.apache.tools.ant.Main -f build.bin.xml deploy -Dcustom.name=ndoh241 -Dcontextroot.name=ndoh241 -Ddbhost.name=localhost -Ddb.name=NDOH241 -Djboss.dir=/opt/jboss-4.0.3SP1 -DexpirationDay=-1 -Ddb.username=root -Ddb.password=asdf
if "%1"=="ndoh241" goto END

:VANGH
echo 			 ============= BUILDING VANGH =============
"%JAVA5_HOME%/bin/java" -Xms512m -Xmx1024m -cp c:/opt/apache-ant-1.6.1/lib/ant.jar;c:/opt/apache-ant-1.6.1/lib/ant-launcher.jar;c:/opt/apache-ant-1.6.1/lib/ant-junit.jar;c:/opt/apache-ant-1.6.1/lib/junit/junit.jar;c:/opt/apache-ant-1.6.1/lib/ant-swing.jar;"%JAVA5_HOME%"/lib/tools.jar org.apache.tools.ant.Main -f build.bin.xml deploy -Dcustom.name=vangh -Dcontextroot.name=vangh -Ddbhost.name=localhost -Ddb.name=VANGH -Djboss.dir=/opt/jboss-4.0.3SP1 -DexpirationDay=-1 -Ddb.username=root -Ddb.password=asdf
if "%1"=="vangh" goto END

:MENGH
echo 			 ============= BUILDING MENGH =============
"%JAVA5_HOME%/bin/java" -Xms512m -Xmx1024m -cp c:/opt/apache-ant-1.6.1/lib/ant.jar;c:/opt/apache-ant-1.6.1/lib/ant-launcher.jar;c:/opt/apache-ant-1.6.1/lib/ant-junit.jar;c:/opt/apache-ant-1.6.1/lib/junit/junit.jar;c:/opt/apache-ant-1.6.1/lib/ant-swing.jar;"%JAVA5_HOME%"/lib/tools.jar org.apache.tools.ant.Main -f build.bin.xml deploy -Dcustom.name=mengh -Dcontextroot.name=mengh -Ddbhost.name=localhost -Ddb.name=MENGH -Djboss.dir=/opt/jboss-4.0.3SP1 -DexpirationDay=-1 -Ddb.username=root -Ddb.password=asdf
if "%1"=="mengh" goto END

:SJNGH
echo 			 ============= BUILDING SJNGH =============
"%JAVA5_HOME%/bin/java" -Xms512m -Xmx1024m -cp c:/opt/apache-ant-1.6.1/lib/ant.jar;c:/opt/apache-ant-1.6.1/lib/ant-launcher.jar;c:/opt/apache-ant-1.6.1/lib/ant-junit.jar;c:/opt/apache-ant-1.6.1/lib/junit/junit.jar;c:/opt/apache-ant-1.6.1/lib/ant-swing.jar;"%JAVA5_HOME%"/lib/tools.jar org.apache.tools.ant.Main -f build.bin.xml deploy -Dcustom.name=sjngh -Dcontextroot.name=sjngh -Ddbhost.name=localhost -Ddb.name=SJNGH -Djboss.dir=/opt/jboss-4.0.3SP1 -DexpirationDay=-1 -Ddb.username=root -Ddb.password=asdf
if "%1"=="sjngh" goto END

:ANMGH
echo 			 ============= BUILDING ANMGH =============
"%JAVA5_HOME%/bin/java" -Xms512m -Xmx1024m -cp c:/opt/apache-ant-1.6.1/lib/ant.jar;c:/opt/apache-ant-1.6.1/lib/ant-launcher.jar;c:/opt/apache-ant-1.6.1/lib/ant-junit.jar;c:/opt/apache-ant-1.6.1/lib/junit/junit.jar;c:/opt/apache-ant-1.6.1/lib/ant-swing.jar;"%JAVA5_HOME%"/lib/tools.jar org.apache.tools.ant.Main -f build.bin.xml deploy -Dcustom.name=anmgh -Dcontextroot.name=anmgh -Ddbhost.name=localhost -Ddb.name=ANMGH -Djboss.dir=/opt/jboss-4.0.3SP1 -DexpirationDay=-1 -Ddb.username=root -Ddb.password=asdf
if "%1"=="anmgh" goto END

:HAGEN
echo 			 ============= BUILDING HAGEN =============
"%JAVA5_HOME%/bin/java" -Xms512m -Xmx1024m -cp c:/opt/apache-ant-1.6.1/lib/ant.jar;c:/opt/apache-ant-1.6.1/lib/ant-launcher.jar;c:/opt/apache-ant-1.6.1/lib/ant-junit.jar;c:/opt/apache-ant-1.6.1/lib/junit/junit.jar;c:/opt/apache-ant-1.6.1/lib/ant-swing.jar;"%JAVA5_HOME%"/lib/tools.jar org.apache.tools.ant.Main -f build.bin.xml deploy -Dcustom.name=hagen -Dcontextroot.name=hagen -Ddbhost.name=localhost -Ddb.name=HAGEN -Djboss.dir=/opt/jboss-4.0.3SP1 -DexpirationDay=-1 -Ddb.username=root -Ddb.password=asdf
if "%1"=="hagen" goto END

:BSMART
echo 			 ============= BUILDING BSMART =============
"%JAVA5_HOME%/bin/java" -Xms512m -Xmx1024m -cp c:/opt/apache-ant-1.6.1/lib/ant.jar;c:/opt/apache-ant-1.6.1/lib/ant-launcher.jar;c:/opt/apache-ant-1.6.1/lib/ant-junit.jar;c:/opt/apache-ant-1.6.1/lib/junit/junit.jar;c:/opt/apache-ant-1.6.1/lib/ant-swing.jar;"%JAVA5_HOME%"/lib/tools.jar org.apache.tools.ant.Main -f build.bin.xml deploy -Dcustom.name=bsmart -Dcontextroot.name=bsmart -Ddbhost.name=localhost -Ddb.name=BSMART -Djboss.dir=/opt/jboss-4.0.3SP1 -DexpirationDay=-1 -Ddb.username=root -Ddb.password=asdf
if "%1"=="bsmart" goto END

:TEST
echo 			 ============= BUILDING TEST =============
"%JAVA5_HOME%/bin/java" -Xms512m -Xmx1024m -cp c:/opt/apache-ant-1.6.1/lib/ant.jar;c:/opt/apache-ant-1.6.1/lib/ant-launcher.jar;c:/opt/apache-ant-1.6.1/lib/ant-junit.jar;c:/opt/apache-ant-1.6.1/lib/junit/junit.jar;c:/opt/apache-ant-1.6.1/lib/ant-swing.jar;"%JAVA5_HOME%"/lib/tools.jar org.apache.tools.ant.Main -f build.bin.xml deploy -Dcustom.name=test -Dcontextroot.name=test -Ddbhost.name=localhost -Ddb.name=TEST -Djboss.dir=/opt/jboss-4.0.3SP1 -DexpirationDay=-1 -Ddb.username=root -Ddb.password=asdf
if "%1"=="test" goto END



:HARDWARE
echo 			 ============= BUILDING HARDWARE =============
"%JAVA5_HOME%/bin/java" -Xms512m -Xmx1024m -cp c:/opt/apache-ant-1.6.1/lib/ant.jar;c:/opt/apache-ant-1.6.1/lib/ant-launcher.jar;c:/opt/apache-ant-1.6.1/lib/ant-junit.jar;c:/opt/apache-ant-1.6.1/lib/junit/junit.jar;c:/opt/apache-ant-1.6.1/lib/ant-swing.jar;"%JAVA5_HOME%"/lib/tools.jar org.apache.tools.ant.Main -f build.bin.xml deploy -Dcustom.name=hardware -Dcontextroot.name=hardware -Ddbhost.name=localhost -Ddb.name=HARDWARE -Djboss.dir=/opt/jboss-4.0.3SP1 -DexpirationDay=-1 -Ddb.username=root -Ddb.password=asdf
if "%1"=="hardware" goto END

:REPORT
echo 			 ============= BUILDING REPORT =============
"%JAVA5_HOME%/bin/java" -Xms512m -Xmx1024m -cp c:/opt/apache-ant-1.6.1/lib/ant.jar;c:/opt/apache-ant-1.6.1/lib/ant-launcher.jar;c:/opt/apache-ant-1.6.1/lib/ant-junit.jar;c:/opt/apache-ant-1.6.1/lib/junit/junit.jar;c:/opt/apache-ant-1.6.1/lib/ant-swing.jar;"%JAVA5_HOME%"/lib/tools.jar org.apache.tools.ant.Main -f build.bin.xml deploy -Dcustom.name=report -Dcontextroot.name=report -Ddbhost.name=10.109.10.68 -Ddb.name=REPORT -Djboss.dir=/opt/jboss-4.0.3SP1 -DexpirationDay=-1 -Ddb.username=root -Ddb.password=asdf
if "%1"=="report" goto END


:OBCL
echo 			 ============= BUILDING OBCL =============
"%JAVA5_HOME%/bin/java" -Xms512m -Xmx1024m -cp c:/opt/apache-ant-1.6.1/lib/ant.jar;c:/opt/apache-ant-1.6.1/lib/ant-launcher.jar;c:/opt/apache-ant-1.6.1/lib/ant-junit.jar;c:/opt/apache-ant-1.6.1/lib/junit/junit.jar;c:/opt/apache-ant-1.6.1/lib/ant-swing.jar;"%JAVA5_HOME%"/lib/tools.jar org.apache.tools.ant.Main -f build.bin.xml deploy -Dcustom.name=obcl -Dcontextroot.name=obcl -Ddbhost.name=localhost -Ddb.name=OBCL -Djboss.dir=/opt/jboss-4.0.3SP1 -DexpirationDay=-1 -Ddb.username=root -Ddb.password=asdf
if "%1"=="obcl" goto END 

:OBCI
echo 			 ============= BUILDING OBCI =============
"%JAVA5_HOME%/bin/java" -Xms512m -Xmx1024m -cp c:/opt/apache-ant-1.6.1/lib/ant.jar;c:/opt/apache-ant-1.6.1/lib/ant-launcher.jar;c:/opt/apache-ant-1.6.1/lib/ant-junit.jar;c:/opt/apache-ant-1.6.1/lib/junit/junit.jar;c:/opt/apache-ant-1.6.1/lib/ant-swing.jar;"%JAVA5_HOME%"/lib/tools.jar org.apache.tools.ant.Main -f build.bin.xml deploy -Dcustom.name=obci -Dcontextroot.name=obci -Ddbhost.name=localhost -Ddb.name=OBCI -Djboss.dir=/opt/jboss-4.0.3SP1 -DexpirationDay=-1 -Ddb.username=root -Ddb.password=asdf
if "%1"=="obci" goto END 

:HMS
echo 			 ============= BUILDING HMS =============
"%JAVA5_HOME%/bin/java" -Xms512m -Xmx1024m -cp c:/opt/apache-ant-1.6.1/lib/ant.jar;c:/opt/apache-ant-1.6.1/lib/ant-launcher.jar;c:/opt/apache-ant-1.6.1/lib/ant-junit.jar;c:/opt/apache-ant-1.6.1/lib/junit/junit.jar;c:/opt/apache-ant-1.6.1/lib/ant-swing.jar;"%JAVA5_HOME%"/lib/tools.jar org.apache.tools.ant.Main -f build.bin.xml deploy -Dcustom.name=hms -Dcontextroot.name=hms -Ddbhost.name=localhost -Ddb.name=HMS -Djboss.dir=/opt/jboss-4.0.3SP1 -DexpirationDay=-1 -Ddb.username=root -Ddb.password=asdf
if "%1"=="hms" goto END

:HMST
echo 			 ============= BUILDING HMST =============
"%JAVA5_HOME%/bin/java" -Xms512m -Xmx1024m -cp c:/opt/apache-ant-1.6.1/lib/ant.jar;c:/opt/apache-ant-1.6.1/lib/ant-launcher.jar;c:/opt/apache-ant-1.6.1/lib/ant-junit.jar;c:/opt/apache-ant-1.6.1/lib/junit/junit.jar;c:/opt/apache-ant-1.6.1/lib/ant-swing.jar;"%JAVA5_HOME%"/lib/tools.jar org.apache.tools.ant.Main -f build.bin.xml deploy -Dcustom.name=hmst -Dcontextroot.name=hmst -Ddbhost.name=localhost -Ddb.name=HMST -Djboss.dir=/opt/jboss-4.0.3SP1 -DexpirationDay=-1 -Ddb.username=root -Ddb.password=asdf
if "%1"=="hmst" goto END


:PGH
echo 			 ============= BUILDING PGH =============
"%JAVA5_HOME%/bin/java" -Xms512m -Xmx1024m -cp c:/opt/apache-ant-1.6.1/lib/ant.jar;c:/opt/apache-ant-1.6.1/lib/ant-launcher.jar;c:/opt/apache-ant-1.6.1/lib/ant-junit.jar;c:/opt/apache-ant-1.6.1/lib/junit/junit.jar;c:/opt/apache-ant-1.6.1/lib/ant-swing.jar;"%JAVA5_HOME%"/lib/tools.jar org.apache.tools.ant.Main -f build.bin.xml deploy -Dcustom.name=pgh -Dcontextroot.name=pgh -Ddbhost.name=localhost -Ddb.name=PGH -Djboss.dir=/opt/jboss-4.0.3SP1 -DexpirationDay=-1 -Ddb.username=root -Ddb.password=asdf
if "%1"=="pgh" goto END 

:PMGH
echo 			 ============= BUILDING PMGH =============
"%JAVA5_HOME%/bin/java" -Xms512m -Xmx1024m -cp c:/opt/apache-ant-1.6.1/lib/ant.jar;c:/opt/apache-ant-1.6.1/lib/ant-launcher.jar;c:/opt/apache-ant-1.6.1/lib/ant-junit.jar;c:/opt/apache-ant-1.6.1/lib/junit/junit.jar;c:/opt/apache-ant-1.6.1/lib/ant-swing.jar;"%JAVA5_HOME%"/lib/tools.jar org.apache.tools.ant.Main -f build.bin.xml deploy -Dcustom.name=pmgh -Dcontextroot.name=pmgh -Ddbhost.name=localhost -Ddb.name=PMGH -Djboss.dir=/opt/jboss-4.0.3SP1 -DexpirationDay=-1 -Ddb.username=root -Ddb.password=asdf
if "%1"=="pmgh" goto END

:XMIND
echo 			 ============= BUILDING XMIND 192.168.0.15 =============
"%JAVA5_HOME%/bin/java" -Xms512m -Xmx1024m -cp c:/opt/apache-ant-1.6.1/lib/ant.jar;c:/opt/apache-ant-1.6.1/lib/ant-launcher.jar;c:/opt/apache-ant-1.6.1/lib/ant-junit.jar;c:/opt/apache-ant-1.6.1/lib/junit/junit.jar;c:/opt/apache-ant-1.6.1/lib/ant-swing.jar;"%JAVA5_HOME%"/lib/tools.jar org.apache.tools.ant.Main -f build.bin.xml deploy -Dcustom.name=xmind -Dcontextroot.name=xmind -Ddbhost.name=localhost -Ddb.name=XMINDOFS -Djboss.dir=/opt/jboss-4.0.3SP1 -DexpirationDay=-1 -Ddb.username=root -Ddb.password=asdf
if "%1"=="xmind" goto END


:XMIND2
echo 			 ============= BUILDING XMIND2 192.168.0.16=============
"%JAVA5_HOME%/bin/java" -Xms512m -Xmx1024m -cp c:/opt/apache-ant-1.6.1/lib/ant.jar;c:/opt/apache-ant-1.6.1/lib/ant-launcher.jar;c:/opt/apache-ant-1.6.1/lib/ant-junit.jar;c:/opt/apache-ant-1.6.1/lib/junit/junit.jar;c:/opt/apache-ant-1.6.1/lib/ant-swing.jar;"%JAVA5_HOME%"/lib/tools.jar org.apache.tools.ant.Main -f build.bin.xml deploy -Dcustom.name=xmind2 -Dcontextroot.name=xmind2 -Ddbhost.name=localhost -Ddb.name=XMINDOFS2 -Djboss.dir=/opt/jboss-4.0.3SP1 -DexpirationDay=-1 -Ddb.username=root -Ddb.password=asdf
if "%1"=="xmind2" goto END

:DEMO
echo 			 ============= BUILDING DEMO =============
"%JAVA5_HOME%/bin/java" -Xms512m -Xmx1024m -cp c:/opt/apache-ant-1.6.1/lib/ant.jar;c:/opt/apache-ant-1.6.1/lib/ant-launcher.jar;c:/opt/apache-ant-1.6.1/lib/ant-junit.jar;c:/opt/apache-ant-1.6.1/lib/junit/junit.jar;c:/opt/apache-ant-1.6.1/lib/ant-swing.jar;"%JAVA5_HOME%"/lib/tools.jar org.apache.tools.ant.Main -f build.bin.xml deploy -Dcustom.name=demo -Dcontextroot.name=demo -Ddbhost.name=localhost -Ddb.name=DEMO -Djboss.dir=/opt/jboss-4.0.3SP1 -DexpirationDay=-1 -Ddb.username=root -Ddb.password=asdf
if "%1"=="demo" goto END 

:TBX
echo 			 ============= BUILDING TBX =============
"%JAVA5_HOME%/bin/java" -Xms512m -Xmx1024m -cp c:/opt/apache-ant-1.6.1/lib/ant.jar;c:/opt/apache-ant-1.6.1/lib/ant-launcher.jar;c:/opt/apache-ant-1.6.1/lib/ant-junit.jar;c:/opt/apache-ant-1.6.1/lib/junit/junit.jar;c:/opt/apache-ant-1.6.1/lib/ant-swing.jar;"%JAVA5_HOME%"/lib/tools.jar org.apache.tools.ant.Main -f build.bin.xml deploy -Dcustom.name=tbx -Dcontextroot.name=tbx -Ddbhost.name=192.168.0.89 -Ddb.name=TBXOFS -Djboss.dir=/opt/jboss-4.0.3SP1 -DexpirationDay=-1 -Ddb.username=root -Ddb.password=asdf
if "%1"=="tbx" goto END 

:PCSPC
echo 			 ============= BUILDING PCSPC =============
"%JAVA5_HOME%/bin/java" -Xms512m -Xmx1024m -cp c:/opt/apache-ant-1.6.1/lib/ant.jar;c:/opt/apache-ant-1.6.1/lib/ant-launcher.jar;c:/opt/apache-ant-1.6.1/lib/ant-junit.jar;c:/opt/apache-ant-1.6.1/lib/junit/junit.jar;c:/opt/apache-ant-1.6.1/lib/ant-swing.jar;"%JAVA5_HOME%"/lib/tools.jar org.apache.tools.ant.Main -f build.bin.xml deploy -Dcustom.name=pcspc -Dcontextroot.name=pcspc -Ddbhost.name=localhost -Ddb.name=PCSPCOFS -Djboss.dir=/opt/jboss-4.0.3SP1 -DexpirationDay=-1 -Ddb.username=root -Ddb.password=asdf
if "%1"=="pcspc" goto END 

:AMON
echo 			 ============= BUILDING AMON =============
"%JAVA5_HOME%/bin/java" -Xms512m -Xmx1024m -cp c:/opt/apache-ant-1.6.1/lib/ant.jar;c:/opt/apache-ant-1.6.1/lib/ant-launcher.jar;c:/opt/apache-ant-1.6.1/lib/ant-junit.jar;c:/opt/apache-ant-1.6.1/lib/junit/junit.jar;c:/opt/apache-ant-1.6.1/lib/ant-swing.jar;"%JAVA5_HOME%"/lib/tools.jar org.apache.tools.ant.Main -f build.bin.xml deploy -Dcustom.name=amon -Dcontextroot.name=amon -Ddbhost.name=localhost -Ddb.name=AMONOFS -Djboss.dir=/opt/jboss-4.0.3SP1 -DexpirationDay=-1 -Ddb.username=root -Ddb.password=#:Mh3V#V
if "%1"=="amon" goto END 

:ALBERGUS
echo 			 ============= BUILDING ALBERGUS =============
"%JAVA5_HOME%/bin/java" -Xms512m -Xmx1024m -cp c:/opt/apache-ant-1.6.1/lib/ant.jar;c:/opt/apache-ant-1.6.1/lib/ant-launcher.jar;c:/opt/apache-ant-1.6.1/lib/ant-junit.jar;c:/opt/apache-ant-1.6.1/lib/junit/junit.jar;c:/opt/apache-ant-1.6.1/lib/ant-swing.jar;"%JAVA5_HOME%"/lib/tools.jar org.apache.tools.ant.Main -f build.bin.xml deploy -Dcustom.name=albergus -Dcontextroot.name=albergus -Ddbhost.name=localhost -Ddb.name=ALBERGUSOFS -Djboss.dir=/opt/jboss-4.0.3SP1 -DexpirationDay=-1 -Ddb.username=root -Ddb.password=asdf
if "%1"=="albergus" goto END 


:TBX2
echo 			 ============= BUILDING TBX2 =============
"%JAVA5_HOME%/bin/java" -Xms512m -Xmx1024m -cp c:/opt/apache-ant-1.6.1/lib/ant.jar;c:/opt/apache-ant-1.6.1/lib/ant-launcher.jar;c:/opt/apache-ant-1.6.1/lib/ant-junit.jar;c:/opt/apache-ant-1.6.1/lib/junit/junit.jar;c:/opt/apache-ant-1.6.1/lib/ant-swing.jar;"%JAVA5_HOME%"/lib/tools.jar org.apache.tools.ant.Main -f build.bin.xml deploy -Dcustom.name=tbx2 -Dcontextroot.name=tbx2 -Ddbhost.name=192.168.0.89 -Ddb.name=TBXOFS -Djboss.dir=/opt/jboss-4.0.3SP1 -DexpirationDay=-1 -Ddb.username=root -Ddb.password=asdf
if "%1"=="tbx2" goto END

:TMC
echo 			 ============= BUILDING TMC =============
"%JAVA5_HOME%/bin/java" -Xms512m -Xmx1024m -cp c:/opt/apache-ant-1.6.1/lib/ant.jar;c:/opt/apache-ant-1.6.1/lib/ant-launcher.jar;c:/opt/apache-ant-1.6.1/lib/ant-junit.jar;c:/opt/apache-ant-1.6.1/lib/junit/junit.jar;c:/opt/apache-ant-1.6.1/lib/ant-swing.jar;"%JAVA5_HOME%"/lib/tools.jar org.apache.tools.ant.Main -f build.bin.xml deploy -Dcustom.name=tmc -Dcontextroot.name=tmc -Ddbhost.name=192.168.1.3 -Ddb.name=OFS -Djboss.dir=/opt/jboss-4.0.3SP1 -DexpirationDay=-1 -Ddb.username=root -Ddb.password=asdf
if "%1"=="tmc" goto END 


:FUNDLINE
echo 			 ============= BUILDING FUNDLINE =============
"%JAVA5_HOME%/bin/java" -Xms512m -Xmx1024m -cp c:/opt/apache-ant-1.6.1/lib/ant.jar;c:/opt/apache-ant-1.6.1/lib/ant-launcher.jar;c:/opt/apache-ant-1.6.1/lib/ant-junit.jar;c:/opt/apache-ant-1.6.1/lib/junit/junit.jar;c:/opt/apache-ant-1.6.1/lib/ant-swing.jar;"%JAVA5_HOME%"/lib/tools.jar org.apache.tools.ant.Main -f build.bin.xml deploy -Dcustom.name=fundline -Dcontextroot.name=fundline -Ddbhost.name=192.168.1.10 -Ddb.name=OFS -Djboss.dir=/opt/jboss-4.0.3SP1 -DexpirationDay=-1 -Ddb.username=root -Ddb.password=asdf
if "%1"=="tmc" goto END 


:VPS
echo 			 ============= BUILDING VPS =============
"%JAVA5_HOME%/bin/java" -Xms512m -Xmx1024m -cp c:/opt/apache-ant-1.6.1/lib/ant.jar;c:/opt/apache-ant-1.6.1/lib/ant-launcher.jar;c:/opt/apache-ant-1.6.1/lib/ant-junit.jar;c:/opt/apache-ant-1.6.1/lib/junit/junit.jar;c:/opt/apache-ant-1.6.1/lib/ant-swing.jar;"%JAVA5_HOME%"/lib/tools.jar org.apache.tools.ant.Main -f build.bin.xml deploy -Dcustom.name=vps -Dcontextroot.name=vps -Ddbhost.name=localhost -Ddb.name=OFS -Djboss.dir=/opt/jboss-4.0.3SP1 -DexpirationDay=-1 -Ddb.username=vps -Ddb.password=asdf
if "%1"=="vps" goto END 


:RSA
echo 			 ============= BUILDING RSA =============
"%JAVA5_HOME%/bin/java" -Xms512m -Xmx1024m -cp c:/opt/apache-ant-1.6.1/lib/ant.jar;c:/opt/apache-ant-1.6.1/lib/ant-launcher.jar;c:/opt/apache-ant-1.6.1/lib/ant-junit.jar;c:/opt/apache-ant-1.6.1/lib/junit/junit.jar;c:/opt/apache-ant-1.6.1/lib/ant-swing.jar;"%JAVA5_HOME%"/lib/tools.jar org.apache.tools.ant.Main -f build.bin.xml deploy -Dcustom.name=rsa -Dcontextroot.name=rsa -Ddbhost.name=localhost -Ddb.name=RSA -Djboss.dir=/opt/jboss-4.0.3SP1 -DexpirationDay=-1 -Ddb.username=root -Ddb.password=asdf
if "%1"=="rsa" goto END 

:WV
echo 			 ============= BUILDING WV =============
"%JAVA5_HOME%/bin/java" -Xms512m -Xmx1024m -cp c:/opt/apache-ant-1.6.1/lib/ant.jar;c:/opt/apache-ant-1.6.1/lib/ant-launcher.jar;c:/opt/apache-ant-1.6.1/lib/ant-junit.jar;c:/opt/apache-ant-1.6.1/lib/junit/junit.jar;c:/opt/apache-ant-1.6.1/lib/ant-swing.jar;"%JAVA5_HOME%"/lib/tools.jar org.apache.tools.ant.Main -f build.bin.xml deploy -Dcustom.name=wv -Dcontextroot.name=wv -Ddbhost.name=localhost -Ddb.name=WV -Djboss.dir=/opt/jboss-4.0.3SP1 -DexpirationDay=-1 -Ddb.username=root -Ddb.password=asdf
if "%1"=="wv" goto END 


:TUMNIR
echo 			 ============= BUILDING TUMNIR =============
"%JAVA5_HOME%/bin/java" -Xms512m -Xmx1024m -cp c:/opt/apache-ant-1.6.1/lib/ant.jar;c:/opt/apache-ant-1.6.1/lib/ant-launcher.jar;c:/opt/apache-ant-1.6.1/lib/ant-junit.jar;c:/opt/apache-ant-1.6.1/lib/junit/junit.jar;c:/opt/apache-ant-1.6.1/lib/ant-swing.jar;"%JAVA5_HOME%"/lib/tools.jar org.apache.tools.ant.Main -f build.bin.xml deploy -Dcustom.name=tumnir -Dcontextroot.name=tumnir -Ddbhost.name=localhost -Ddb.name=TUMNIR -Djboss.dir=/opt/jboss-4.0.3SP1 -DexpirationDay=-1 -Ddb.username=root -Ddb.password=asdf
if "%1"=="tumnir" goto END 

:HCL
echo 			 ============= BUILDING HCL =============
"%JAVA5_HOME%/bin/java" -Xms512m -Xmx1024m -cp c:/opt/apache-ant-1.6.1/lib/ant.jar;c:/opt/apache-ant-1.6.1/lib/ant-launcher.jar;c:/opt/apache-ant-1.6.1/lib/ant-junit.jar;c:/opt/apache-ant-1.6.1/lib/junit/junit.jar;c:/opt/apache-ant-1.6.1/lib/ant-swing.jar;"%JAVA5_HOME%"/lib/tools.jar org.apache.tools.ant.Main -f build.bin.xml deploy -Dcustom.name=hcl -Dcontextroot.name=hcl -Ddbhost.name=localhost -Ddb.name=HCL -Djboss.dir=/opt/jboss-4.0.3SP1 -DexpirationDay=-1 -Ddb.username=root -Ddb.password=asdf
if "%1"=="hcl" goto END 

:HPPL
echo 			 ============= BUILDING HPPL =============
"%JAVA5_HOME%/bin/java" -Xms512m -Xmx1024m -cp c:/opt/apache-ant-1.6.1/lib/ant.jar;c:/opt/apache-ant-1.6.1/lib/ant-launcher.jar;c:/opt/apache-ant-1.6.1/lib/ant-junit.jar;c:/opt/apache-ant-1.6.1/lib/junit/junit.jar;c:/opt/apache-ant-1.6.1/lib/ant-swing.jar;"%JAVA5_HOME%"/lib/tools.jar org.apache.tools.ant.Main -f build.bin.xml deploy -Dcustom.name=hppl -Dcontextroot.name=hppl -Ddbhost.name=localhost -Ddb.name=HPPL -Djboss.dir=/opt/jboss-4.0.3SP1 -DexpirationDay=-1 -Ddb.username=root -Ddb.password=asdf
if "%1"=="hppl" goto END 

:SJTL
echo 			 ============= BUILDING SJTL =============
"%JAVA5_HOME%/bin/java" -Xms512m -Xmx1024m -cp c:/opt/apache-ant-1.6.1/lib/ant.jar;c:/opt/apache-ant-1.6.1/lib/ant-launcher.jar;c:/opt/apache-ant-1.6.1/lib/ant-junit.jar;c:/opt/apache-ant-1.6.1/lib/junit/junit.jar;c:/opt/apache-ant-1.6.1/lib/ant-swing.jar;"%JAVA5_HOME%"/lib/tools.jar org.apache.tools.ant.Main -f build.bin.xml deploy -Dcustom.name=sjtl -Dcontextroot.name=sjtl -Ddbhost.name=localhost -Ddb.name=SJTL -Djboss.dir=/opt/jboss-4.0.3SP1 -DexpirationDay=-1 -Ddb.username=root -Ddb.password=asdf
if "%1"=="sjtl" goto END 

:CAMH
echo 			 ============= BUILDING CAMH =============
"%JAVA5_HOME%/bin/java" -Xms512m -Xmx1024m -cp c:/opt/apache-ant-1.6.1/lib/ant.jar;c:/opt/apache-ant-1.6.1/lib/ant-launcher.jar;c:/opt/apache-ant-1.6.1/lib/ant-junit.jar;c:/opt/apache-ant-1.6.1/lib/junit/junit.jar;c:/opt/apache-ant-1.6.1/lib/ant-swing.jar;"%JAVA5_HOME%"/lib/tools.jar org.apache.tools.ant.Main -f build.bin.xml deploy -Dcustom.name=camh -Dcontextroot.name=camh -Ddbhost.name=localhost -Ddb.name=CAMH -Djboss.dir=/opt/jboss-4.0.3SP1 -DexpirationDay=-1 -Ddb.username=root -Ddb.password=asdf
if "%1"=="camh" goto END 

:CAMH2
echo 			 ============= BUILDING CAMH2 =============
"%JAVA5_HOME%/bin/java" -Xms512m -Xmx1024m -cp c:/opt/apache-ant-1.6.1/lib/ant.jar;c:/opt/apache-ant-1.6.1/lib/ant-launcher.jar;c:/opt/apache-ant-1.6.1/lib/ant-junit.jar;c:/opt/apache-ant-1.6.1/lib/junit/junit.jar;c:/opt/apache-ant-1.6.1/lib/ant-swing.jar;"%JAVA5_HOME%"/lib/tools.jar org.apache.tools.ant.Main -f build.bin.xml deploy -Dcustom.name=camh2 -Dcontextroot.name=camh2 -Ddbhost.name=localhost -Ddb.name=CAMH2 -Djboss.dir=/opt/jboss-4.0.3SP1 -DexpirationDay=-1 -Ddb.username=root -Ddb.password=asdf
if "%1"=="camh2" goto END 

:SUN
echo 			 ============= BUILDING SUN =============
"%JAVA5_HOME%/bin/java" -Xms512m -Xmx1024m -cp c:/opt/apache-ant-1.6.1/lib/ant.jar;c:/opt/apache-ant-1.6.1/lib/ant-launcher.jar;c:/opt/apache-ant-1.6.1/lib/ant-junit.jar;c:/opt/apache-ant-1.6.1/lib/junit/junit.jar;c:/opt/apache-ant-1.6.1/lib/ant-swing.jar;"%JAVA5_HOME%"/lib/tools.jar org.apache.tools.ant.Main -f build.bin.xml deploy -Dcustom.name=sun -Dcontextroot.name=sun -Ddbhost.name=localhost -Ddb.name=SUN -Djboss.dir=/opt/jboss-4.0.3SP1 -DexpirationDay=-1 -Ddb.username=root -Ddb.password=asdf
if "%1"=="sun" goto END 

:JVA
echo 			 ============= BUILDING JVA =============
"%JAVA5_HOME%/bin/java" -Xms512m -Xmx1024m -cp c:/opt/apache-ant-1.6.1/lib/ant.jar;c:/opt/apache-ant-1.6.1/lib/ant-launcher.jar;c:/opt/apache-ant-1.6.1/lib/ant-junit.jar;c:/opt/apache-ant-1.6.1/lib/junit/junit.jar;c:/opt/apache-ant-1.6.1/lib/ant-swing.jar;"%JAVA5_HOME%"/lib/tools.jar org.apache.tools.ant.Main -f build.bin.xml deploy -Dcustom.name=jva -Dcontextroot.name=jva -Ddbhost.name=localhost -Ddb.name=JVA -Djboss.dir=/opt/jboss-4.0.3SP1 -DexpirationDay=-1 -Ddb.username=root -Ddb.password=asdf
if "%1"=="jva" goto END 

:DYNAST
echo 			 ============= BUILDING DYNAST =============
"%JAVA5_HOME%/bin/java" -Xms512m -Xmx1024m -cp c:/opt/apache-ant-1.6.1/lib/ant.jar;c:/opt/apache-ant-1.6.1/lib/ant-launcher.jar;c:/opt/apache-ant-1.6.1/lib/ant-junit.jar;c:/opt/apache-ant-1.6.1/lib/junit/junit.jar;c:/opt/apache-ant-1.6.1/lib/ant-swing.jar;"%JAVA5_HOME%"/lib/tools.jar org.apache.tools.ant.Main -f build.bin.xml deploy -Dcustom.name=dynast -Dcontextroot.name=dynast -Ddbhost.name=localhost -Ddb.name=DYNAST -Djboss.dir=/opt/jboss-4.0.3SP1 -DexpirationDay=-1 -Ddb.username=root -Ddb.password=asdf
if "%1"=="dynast" goto END 

:DYNAST2
echo 			 ============= BUILDING DYNAST2 =============
"%JAVA5_HOME%/bin/java" -Xms512m -Xmx1024m -cp c:/opt/apache-ant-1.6.1/lib/ant.jar;c:/opt/apache-ant-1.6.1/lib/ant-launcher.jar;c:/opt/apache-ant-1.6.1/lib/ant-junit.jar;c:/opt/apache-ant-1.6.1/lib/junit/junit.jar;c:/opt/apache-ant-1.6.1/lib/ant-swing.jar;"%JAVA5_HOME%"/lib/tools.jar org.apache.tools.ant.Main -f build.bin.xml deploy -Dcustom.name=dynast2 -Dcontextroot.name=dynast2 -Ddbhost.name=localhost -Ddb.name=DYNAST2 -Djboss.dir=/opt/jboss-4.0.3SP1 -DexpirationDay=-1 -Ddb.username=root -Ddb.password=asdf
if "%1"=="dynast2" goto END 

:DOF
echo 			 ============= BUILDING DOF =============
"%JAVA5_HOME%/bin/java" -Xms512m -Xmx1024m -cp c:/opt/apache-ant-1.6.1/lib/ant.jar;c:/opt/apache-ant-1.6.1/lib/ant-launcher.jar;c:/opt/apache-ant-1.6.1/lib/ant-junit.jar;c:/opt/apache-ant-1.6.1/lib/junit/junit.jar;c:/opt/apache-ant-1.6.1/lib/ant-swing.jar;"%JAVA5_HOME%"/lib/tools.jar org.apache.tools.ant.Main -f build.bin.xml deploy -Dcustom.name=dof -Dcontextroot.name=dof -Ddbhost.name=localhost -Ddb.name=DOF -Djboss.dir=/opt/jboss-4.0.3SP1 -DexpirationDay=-1 -Ddb.username=root -Ddb.password=asdf
if "%1"=="dof" goto END 

:24H
echo 			 ============= BUILDING 24H =============
"%JAVA5_HOME%/bin/java" -Xms512m -Xmx1024m -cp c:/opt/apache-ant-1.6.1/lib/ant.jar;c:/opt/apache-ant-1.6.1/lib/ant-launcher.jar;c:/opt/apache-ant-1.6.1/lib/ant-junit.jar;c:/opt/apache-ant-1.6.1/lib/junit/junit.jar;c:/opt/apache-ant-1.6.1/lib/ant-swing.jar;"%JAVA5_HOME%"/lib/tools.jar org.apache.tools.ant.Main -f build.bin.xml deploy -Dcustom.name=24h -Dcontextroot.name=24h -Ddbhost.name=localhost -Ddb.name=24H -Djboss.dir=/opt/jboss-4.0.3SP1 -DexpirationDay=-1 -Ddb.username=root -Ddb.password=asdf
if "%1"=="24h" goto END 

:24H2
echo 			 ============= BUILDING 24H2 =============
"%JAVA5_HOME%/bin/java" -Xms512m -Xmx1024m -cp c:/opt/apache-ant-1.6.1/lib/ant.jar;c:/opt/apache-ant-1.6.1/lib/ant-launcher.jar;c:/opt/apache-ant-1.6.1/lib/ant-junit.jar;c:/opt/apache-ant-1.6.1/lib/junit/junit.jar;c:/opt/apache-ant-1.6.1/lib/ant-swing.jar;"%JAVA5_HOME%"/lib/tools.jar org.apache.tools.ant.Main -f build.bin.xml deploy -Dcustom.name=24h2 -Dcontextroot.name=24h2 -Ddbhost.name=localhost -Ddb.name=24H2 -Djboss.dir=/opt/jboss-4.0.3SP1 -DexpirationDay=-1 -Ddb.username=root -Ddb.password=asdf
if "%1"=="24h2" goto END 

:CLAIRE
echo 			 ============= BUILDING CLAIRE =============
"%JAVA5_HOME%/bin/java" -Xms512m -Xmx1024m -cp c:/opt/apache-ant-1.6.1/lib/ant.jar;c:/opt/apache-ant-1.6.1/lib/ant-launcher.jar;c:/opt/apache-ant-1.6.1/lib/ant-junit.jar;c:/opt/apache-ant-1.6.1/lib/junit/junit.jar;c:/opt/apache-ant-1.6.1/lib/ant-swing.jar;"%JAVA5_HOME%"/lib/tools.jar org.apache.tools.ant.Main -f build.bin.xml deploy -Dcustom.name=claire -Dcontextroot.name=claire -Ddbhost.name=localhost -Ddb.name=CLAIRE -Djboss.dir=/opt/jboss-4.0.3SP1 -DexpirationDay=-1 -Ddb.username=root -Ddb.password=asdf
if "%1"=="claire" goto END 

:TPE
echo 			 ============= BUILDING TPE =============
"%JAVA5_HOME%/bin/java" -Xms512m -Xmx1024m -cp c:/opt/apache-ant-1.6.1/lib/ant.jar;c:/opt/apache-ant-1.6.1/lib/ant-launcher.jar;c:/opt/apache-ant-1.6.1/lib/ant-junit.jar;c:/opt/apache-ant-1.6.1/lib/junit/junit.jar;c:/opt/apache-ant-1.6.1/lib/ant-swing.jar;"%JAVA5_HOME%"/lib/tools.jar org.apache.tools.ant.Main -f build.bin.xml deploy -Dcustom.name=tpe -Dcontextroot.name=tpe -Ddbhost.name=localhost -Ddb.name=TPE -Djboss.dir=/opt/jboss-4.0.3SP1 -DexpirationDay=-1 -Ddb.username=root -Ddb.password=asdf
if "%1"=="tpe" goto END 


:TPE2
echo 			 ============= BUILDING TPE2 =============
"%JAVA5_HOME%/bin/java" -Xms512m -Xmx1024m -cp c:/opt/apache-ant-1.6.1/lib/ant.jar;c:/opt/apache-ant-1.6.1/lib/ant-launcher.jar;c:/opt/apache-ant-1.6.1/lib/ant-junit.jar;c:/opt/apache-ant-1.6.1/lib/junit/junit.jar;c:/opt/apache-ant-1.6.1/lib/ant-swing.jar;"%JAVA5_HOME%"/lib/tools.jar org.apache.tools.ant.Main -f build.bin.xml deploy -Dcustom.name=tpe2 -Dcontextroot.name=tpe2 -Ddbhost.name=localhost -Ddb.name=TPE2 -Djboss.dir=/opt/jboss-4.0.3SP1 -DexpirationDay=-1 -Ddb.username=root -Ddb.password=asdf
if "%1"=="tpe2" goto END 

:HMPA
echo 			 ============= BUILDING HMPA =============
"%JAVA5_HOME%/bin/java" -Xms512m -Xmx1024m -cp c:/opt/apache-ant-1.6.1/lib/ant.jar;c:/opt/apache-ant-1.6.1/lib/ant-launcher.jar;c:/opt/apache-ant-1.6.1/lib/ant-junit.jar;c:/opt/apache-ant-1.6.1/lib/junit/junit.jar;c:/opt/apache-ant-1.6.1/lib/ant-swing.jar;"%JAVA5_HOME%"/lib/tools.jar org.apache.tools.ant.Main -f build.bin.xml deploy -Dcustom.name=hmpa -Dcontextroot.name=hmpa -Ddbhost.name=localhost -Ddb.name=HMPA -Djboss.dir=/opt/jboss-4.0.3SP1 -DexpirationDay=-1 -Ddb.username=root -Ddb.password=asdf
if "%1"=="hmpa" goto END 

:ML
echo 			 ============= BUILDING ML =============
"%JAVA5_HOME%/bin/java" -Xms512m -Xmx1024m -cp c:/opt/apache-ant-1.6.1/lib/ant.jar;c:/opt/apache-ant-1.6.1/lib/ant-launcher.jar;c:/opt/apache-ant-1.6.1/lib/ant-junit.jar;c:/opt/apache-ant-1.6.1/lib/junit/junit.jar;c:/opt/apache-ant-1.6.1/lib/ant-swing.jar;"%JAVA5_HOME%"/lib/tools.jar org.apache.tools.ant.Main -f build.bin.xml deploy -Dcustom.name=ml -Dcontextroot.name=ml -Ddbhost.name=localhost -Ddb.name=ML -Djboss.dir=/opt/jboss-4.0.3SP1 -DexpirationDay=-1 -Ddb.username=root -Ddb.password=asdf
if "%1"=="ml" goto END 

:NCL
echo 			 ============= BUILDING NCL =============
"%JAVA5_HOME%/bin/java" -Xms512m -Xmx1024m -cp c:/opt/apache-ant-1.6.1/lib/ant.jar;c:/opt/apache-ant-1.6.1/lib/ant-launcher.jar;c:/opt/apache-ant-1.6.1/lib/ant-junit.jar;c:/opt/apache-ant-1.6.1/lib/junit/junit.jar;c:/opt/apache-ant-1.6.1/lib/ant-swing.jar;"%JAVA5_HOME%"/lib/tools.jar org.apache.tools.ant.Main -f build.bin.xml deploy -Dcustom.name=ncl -Dcontextroot.name=ncl -Ddbhost.name=localhost -Ddb.name=NCL -Djboss.dir=/opt/jboss-4.0.3SP1 -DexpirationDay=-1 -Ddb.username=root -Ddb.password=asdf
if "%1"=="ncl" goto END 

:KCG
echo 			 ============= BUILDING KCG =============
"%JAVA5_HOME%/bin/java" -Xms512m -Xmx1024m -cp c:/opt/apache-ant-1.6.1/lib/ant.jar;c:/opt/apache-ant-1.6.1/lib/ant-launcher.jar;c:/opt/apache-ant-1.6.1/lib/ant-junit.jar;c:/opt/apache-ant-1.6.1/lib/junit/junit.jar;c:/opt/apache-ant-1.6.1/lib/ant-swing.jar;"%JAVA5_HOME%"/lib/tools.jar org.apache.tools.ant.Main -f build.bin.xml deploy -Dcustom.name=kcg -Dcontextroot.name=kcg -Ddbhost.name=localhost -Ddb.name=KCG -Djboss.dir=/opt/jboss-4.0.3SP1 -DexpirationDay=-1 -Ddb.username=root -Ddb.password=asdf
if "%1"=="kcg" goto END 


:JRH
echo 			 ============= BUILDING JRH =============
"%JAVA5_HOME%/bin/java" -Xms512m -Xmx1024m -cp c:/opt/apache-ant-1.6.1/lib/ant.jar;c:/opt/apache-ant-1.6.1/lib/ant-launcher.jar;c:/opt/apache-ant-1.6.1/lib/ant-junit.jar;c:/opt/apache-ant-1.6.1/lib/junit/junit.jar;c:/opt/apache-ant-1.6.1/lib/ant-swing.jar;"%JAVA5_HOME%"/lib/tools.jar org.apache.tools.ant.Main -f build.bin.xml deploy -Dcustom.name=jrh -Dcontextroot.name=jrh -Ddbhost.name=localhost -Ddb.name=JRH -Djboss.dir=/opt/jboss-4.0.3SP1 -DexpirationDay=-1 -Ddb.username=root -Ddb.password=asdf
if "%1"=="jrh" goto END 

:JRHP
echo 			 ============= BUILDING JRHP =============
"%JAVA5_HOME%/bin/java" -Xms512m -Xmx1024m -cp c:/opt/apache-ant-1.6.1/lib/ant.jar;c:/opt/apache-ant-1.6.1/lib/ant-launcher.jar;c:/opt/apache-ant-1.6.1/lib/ant-junit.jar;c:/opt/apache-ant-1.6.1/lib/junit/junit.jar;c:/opt/apache-ant-1.6.1/lib/ant-swing.jar;"%JAVA5_HOME%"/lib/tools.jar org.apache.tools.ant.Main -f build.bin.xml deploy -Dcustom.name=jrhp -Dcontextroot.name=jrhp -Ddbhost.name=localhost -Ddb.name=JRHP -Djboss.dir=/opt/jboss-4.0.3SP1 -DexpirationDay=-1 -Ddb.username=root -Ddb.password=asdf
if "%1"=="jrhp" goto END

:JRHLMB
echo 			 ============= BUILDING JRHLMB =============
"%JAVA5_HOME%/bin/java" -Xms512m -Xmx1024m -cp c:/opt/apache-ant-1.6.1/lib/ant.jar;c:/opt/apache-ant-1.6.1/lib/ant-launcher.jar;c:/opt/apache-ant-1.6.1/lib/ant-junit.jar;c:/opt/apache-ant-1.6.1/lib/junit/junit.jar;c:/opt/apache-ant-1.6.1/lib/ant-swing.jar;"%JAVA5_HOME%"/lib/tools.jar org.apache.tools.ant.Main -f build.bin.xml deploy -Dcustom.name=jrhlmb -Dcontextroot.name=jrhlmb -Ddbhost.name=localhost -Ddb.name=JRHLMB -Djboss.dir=/opt/jboss-4.0.3SP1 -DexpirationDay=-1 -Ddb.username=root -Ddb.password=asdf
if "%1"=="jrhlmb" goto END 

:JRHORP
echo 			 ============= BUILDING JRHORP =============
"%JAVA5_HOME%/bin/java" -Xms512m -Xmx1024m -cp c:/opt/apache-ant-1.6.1/lib/ant.jar;c:/opt/apache-ant-1.6.1/lib/ant-launcher.jar;c:/opt/apache-ant-1.6.1/lib/ant-junit.jar;c:/opt/apache-ant-1.6.1/lib/junit/junit.jar;c:/opt/apache-ant-1.6.1/lib/ant-swing.jar;"%JAVA5_HOME%"/lib/tools.jar org.apache.tools.ant.Main -f build.bin.xml deploy -Dcustom.name=jrhorp -Dcontextroot.name=jrhorp -Ddbhost.name=localhost -Ddb.name=JRHORP -Djboss.dir=/opt/jboss-4.0.3SP1 -DexpirationDay=-1 -Ddb.username=root -Ddb.password=asdf
if "%1"=="jrhorp" goto END 


:JRHMT
echo 			 ============= BUILDING JRHMT =============
"%JAVA5_HOME%/bin/java" -Xms512m -Xmx1024m -cp c:/opt/apache-ant-1.6.1/lib/ant.jar;c:/opt/apache-ant-1.6.1/lib/ant-launcher.jar;c:/opt/apache-ant-1.6.1/lib/ant-junit.jar;c:/opt/apache-ant-1.6.1/lib/junit/junit.jar;c:/opt/apache-ant-1.6.1/lib/ant-swing.jar;"%JAVA5_HOME%"/lib/tools.jar org.apache.tools.ant.Main -f build.bin.xml deploy -Dcustom.name=jrhmt -Dcontextroot.name=jrhmt -Ddbhost.name=localhost -Ddb.name=JRHMT -Djboss.dir=/opt/jboss-4.0.3SP1 -DexpirationDay=-1 -Ddb.username=root -Ddb.password=asdf
if "%1"=="jrhmt" goto END 

:TRU
echo 			 ============= BUILDING TRU =============
"%JAVA5_HOME%/bin/java" -Xms512m -Xmx1024m -cp c:/opt/apache-ant-1.6.1/lib/ant.jar;c:/opt/apache-ant-1.6.1/lib/ant-launcher.jar;c:/opt/apache-ant-1.6.1/lib/ant-junit.jar;c:/opt/apache-ant-1.6.1/lib/junit/junit.jar;c:/opt/apache-ant-1.6.1/lib/ant-swing.jar;"%JAVA5_HOME%"/lib/tools.jar org.apache.tools.ant.Main -f build.bin.xml deploy -Dcustom.name=tru -Dcontextroot.name=tru -Ddbhost.name=localhost -Ddb.name=TRU -Djboss.dir=/opt/jboss-4.0.3SP1 -DexpirationDay=-1 -Ddb.username=root -Ddb.password=asdf
if "%1"=="tru" goto END 

:EDA
echo 			 ============= BUILDING EDA =============
"%JAVA5_HOME%/bin/java" -Xms512m -Xmx1024m -cp c:/opt/apache-ant-1.6.1/lib/ant.jar;c:/opt/apache-ant-1.6.1/lib/ant-launcher.jar;c:/opt/apache-ant-1.6.1/lib/ant-junit.jar;c:/opt/apache-ant-1.6.1/lib/junit/junit.jar;c:/opt/apache-ant-1.6.1/lib/ant-swing.jar;"%JAVA5_HOME%"/lib/tools.jar org.apache.tools.ant.Main -f build.bin.xml deploy -Dcustom.name=eda -Dcontextroot.name=eda -Ddbhost.name=localhost -Ddb.name=EDA -Djboss.dir=/opt/jboss-4.0.3SP1 -DexpirationDay=-1 -Ddb.username=root -Ddb.password=asdf
if "%1"=="eda" goto END 

:COURTS
echo 			 ============= BUILDING COURTS =============
"%JAVA5_HOME%/bin/java" -Xms512m -Xmx1024m -cp c:/opt/apache-ant-1.6.1/lib/ant.jar;c:/opt/apache-ant-1.6.1/lib/ant-launcher.jar;c:/opt/apache-ant-1.6.1/lib/ant-junit.jar;c:/opt/apache-ant-1.6.1/lib/junit/junit.jar;c:/opt/apache-ant-1.6.1/lib/ant-swing.jar;"%JAVA5_HOME%"/lib/tools.jar org.apache.tools.ant.Main -f build.bin.xml deploy -Dcustom.name=courts -Dcontextroot.name=courts -Ddbhost.name=localhost -Ddb.name=COURTS -Djboss.dir=/opt/jboss-4.0.3SP1 -DexpirationDay=-1 -Ddb.username=root -Ddb.password=asdf
if "%1"=="courts" goto END 

:DALTRON
echo 			 ============= BUILDING DALTRON =============
"%JAVA5_HOME%/bin/java" -Xms512m -Xmx1024m -cp c:/opt/apache-ant-1.6.1/lib/ant.jar;c:/opt/apache-ant-1.6.1/lib/ant-launcher.jar;c:/opt/apache-ant-1.6.1/lib/ant-junit.jar;c:/opt/apache-ant-1.6.1/lib/junit/junit.jar;c:/opt/apache-ant-1.6.1/lib/ant-swing.jar;"%JAVA5_HOME%"/lib/tools.jar org.apache.tools.ant.Main -f build.bin.xml deploy -Dcustom.name=daltron -Dcontextroot.name=daltron -Ddbhost.name=localhost -Ddb.name=DALTRON -Djboss.dir=/opt/jboss-4.0.3SP1 -DexpirationDay=-1 -Ddb.username=root -Ddb.password=asdf
if "%1"=="daltron" goto END 

:CCDF
echo 			 ============= BUILDING CCDF =============
"%JAVA5_HOME%/bin/java" -Xms512m -Xmx1024m -cp c:/opt/apache-ant-1.6.1/lib/ant.jar;c:/opt/apache-ant-1.6.1/lib/ant-launcher.jar;c:/opt/apache-ant-1.6.1/lib/ant-junit.jar;c:/opt/apache-ant-1.6.1/lib/junit/junit.jar;c:/opt/apache-ant-1.6.1/lib/ant-swing.jar;"%JAVA5_HOME%"/lib/tools.jar org.apache.tools.ant.Main -f build.bin.xml deploy -Dcustom.name=ccdf -Dcontextroot.name=ccdf -Ddbhost.name=localhost -Ddb.name=CCDF -Djboss.dir=/opt/jboss-4.0.3SP1 -DexpirationDay=-1 -Ddb.username=root -Ddb.password=asdf
if "%1"=="ccdf" goto END 

:SSJ
echo 			 ============= BUILDING SSJ =============
"%JAVA5_HOME%/bin/java" -Xms512m -Xmx1024m -cp c:/opt/apache-ant-1.6.1/lib/ant.jar;c:/opt/apache-ant-1.6.1/lib/ant-launcher.jar;c:/opt/apache-ant-1.6.1/lib/ant-junit.jar;c:/opt/apache-ant-1.6.1/lib/junit/junit.jar;c:/opt/apache-ant-1.6.1/lib/ant-swing.jar;"%JAVA5_HOME%"/lib/tools.jar org.apache.tools.ant.Main -f build.bin.xml deploy -Dcustom.name=ssj -Dcontextroot.name=ssj -Ddbhost.name=localhost -Ddb.name=SSJ -Djboss.dir=/opt/jboss-4.0.3SP1 -DexpirationDay=-1 -Ddb.username=root -Ddb.password=asdf
if "%1"=="ssj" goto END 

:TEST
echo 			 ============= BUILDING TEST =============
"%JAVA5_HOME%/bin/java" -Xms512m -Xmx1024m -cp c:/opt/apache-ant-1.6.1/lib/ant.jar;c:/opt/apache-ant-1.6.1/lib/ant-launcher.jar;c:/opt/apache-ant-1.6.1/lib/ant-junit.jar;c:/opt/apache-ant-1.6.1/lib/junit/junit.jar;c:/opt/apache-ant-1.6.1/lib/ant-swing.jar;"%JAVA5_HOME%"/lib/tools.jar org.apache.tools.ant.Main -f build.bin.xml deploy -Dcustom.name=test -Dcontextroot.name=test -Ddbhost.name=localhost -Ddb.name=TEST -Djboss.dir=/opt/jboss-4.0.3SP1 -DexpirationDay=-1 -Ddb.username=root -Ddb.password=asdf
if "%1"=="test" goto END 


:XYZ
echo 			 ============= BUILDING XYZ =============
"%JAVA5_HOME%/bin/java" -Xms512m -Xmx1024m -cp c:/opt/apache-ant-1.6.1/lib/ant.jar;c:/opt/apache-ant-1.6.1/lib/ant-launcher.jar;c:/opt/apache-ant-1.6.1/lib/ant-junit.jar;c:/opt/apache-ant-1.6.1/lib/junit/junit.jar;c:/opt/apache-ant-1.6.1/lib/ant-swing.jar;"%JAVA5_HOME%"/lib/tools.jar org.apache.tools.ant.Main -f build.bin.xml deploy -Dcustom.name=xyz -Dcontextroot.name=xyz -Ddbhost.name=localhost -Ddb.name=XYZ -Djboss.dir=/opt/jboss-4.0.3SP1 -DexpirationDay=-1 -Ddb.username=root -Ddb.password=asdf
if "%1"=="xyz" goto END 


:SSJ
echo 			 ============= BUILDING SSJ =============
"%JAVA5_HOME%/bin/java" -Xms512m -Xmx1024m -cp c:/opt/apache-ant-1.6.1/lib/ant.jar;c:/opt/apache-ant-1.6.1/lib/ant-launcher.jar;c:/opt/apache-ant-1.6.1/lib/ant-junit.jar;c:/opt/apache-ant-1.6.1/lib/junit/junit.jar;c:/opt/apache-ant-1.6.1/lib/ant-swing.jar;"%JAVA5_HOME%"/lib/tools.jar org.apache.tools.ant.Main -f build.bin.xml deploy -Dcustom.name=ssj -Dcontextroot.name=ssj -Ddbhost.name=localhost -Ddb.name=SSJ -Djboss.dir=/opt/jboss-4.0.3SP1 -DexpirationDay=-1 -Ddb.username=root -Ddb.password=asdf
if "%1"=="ssj" goto END 

:SUPERJANI
echo 			 ============= BUILDING SUPERJANI =============
"%JAVA5_HOME%/bin/java" -Xms512m -Xmx1024m -cp c:/opt/apache-ant-1.6.1/lib/ant.jar;c:/opt/apache-ant-1.6.1/lib/ant-launcher.jar;c:/opt/apache-ant-1.6.1/lib/ant-junit.jar;c:/opt/apache-ant-1.6.1/lib/junit/junit.jar;c:/opt/apache-ant-1.6.1/lib/ant-swing.jar;"%JAVA5_HOME%"/lib/tools.jar org.apache.tools.ant.Main -f build.bin.xml deploy -Dcustom.name=superjani -Dcontextroot.name=superjani -Ddbhost.name=localhost -Ddb.name=SUPERJANI -Djboss.dir=/opt/jboss-4.0.3SP1 -DexpirationDay=-1 -Ddb.username=root -Ddb.password=asdf
if "%1"=="superjani" goto END 

:CASAS2
echo 			 ============= BUILDING CASAS2=============
"%JAVA5_HOME%/bin/java" -Xms512m -Xmx1024m -cp c:/opt/apache-ant-1.6.1/lib/ant.jar;c:/opt/apache-ant-1.6.1/lib/ant-launcher.jar;c:/opt/apache-ant-1.6.1/lib/ant-junit.jar;c:/opt/apache-ant-1.6.1/lib/junit/junit.jar;c:/opt/apache-ant-1.6.1/lib/ant-swing.jar;"%JAVA5_HOME%"/lib/tools.jar org.apache.tools.ant.Main -f build.bin.xml deploy -Dcustom.name=casas2 -Dcontextroot.name=casas2 -Ddbhost.name=localhost -Ddb.name=CASAS2 -Djboss.dir=/opt/jboss-4.0.3SP1 -DexpirationDay=-1 -Ddb.username=root -Ddb.password=asdf
if "%1"=="casas2" goto END 



:SUPERJANI
echo 			 ============= BUILDING SUPERJANI =============
"%JAVA5_HOME%/bin/java" -Xms512m -Xmx1024m -cp c:/opt/apache-ant-1.6.1/lib/ant.jar;c:/opt/apache-ant-1.6.1/lib/ant-launcher.jar;c:/opt/apache-ant-1.6.1/lib/ant-junit.jar;c:/opt/apache-ant-1.6.1/lib/junit/junit.jar;c:/opt/apache-ant-1.6.1/lib/ant-swing.jar;"%JAVA5_HOME%"/lib/tools.jar org.apache.tools.ant.Main -f build.bin.xml deploy -Dcustom.name=superjani -Dcontextroot.name=superjani -Ddbhost.name=localhost -Ddb.name=SUPERJANI -Djboss.dir=/opt/jboss-4.0.3SP1 -DexpirationDay=-1 -Ddb.username=root -Ddb.password=asdf
if "%1"=="superjani" goto END


:ICEI
echo 			 ============= BUILDING ICEI =============
"%JAVA5_HOME%/bin/java" -Xms512m -Xmx1024m -cp c:/opt/apache-ant-1.6.1/lib/ant.jar;c:/opt/apache-ant-1.6.1/lib/ant-launcher.jar;c:/opt/apache-ant-1.6.1/lib/ant-junit.jar;c:/opt/apache-ant-1.6.1/lib/junit/junit.jar;c:/opt/apache-ant-1.6.1/lib/ant-swing.jar;"%JAVA5_HOME%"/lib/tools.jar org.apache.tools.ant.Main -f build.bin.xml deploy -Dcustom.name=icei -Dcontextroot.name=icei -Ddbhost.name=localhost -Ddb.name=ICEI -Djboss.dir=/opt/jboss-4.0.3SP1 -DexpirationDay=-1 -Ddb.username=root -Ddb.password=asdf
if "%1"=="icei" goto END

:ICEI2018
echo 			 ============= BUILDING ICEI2018 =============
"%JAVA5_HOME%/bin/java" -Xms512m -Xmx1024m -cp c:/opt/apache-ant-1.6.1/lib/ant.jar;c:/opt/apache-ant-1.6.1/lib/ant-launcher.jar;c:/opt/apache-ant-1.6.1/lib/ant-junit.jar;c:/opt/apache-ant-1.6.1/lib/junit/junit.jar;c:/opt/apache-ant-1.6.1/lib/ant-swing.jar;"%JAVA5_HOME%"/lib/tools.jar org.apache.tools.ant.Main -f build.bin.xml deploy -Dcustom.name=icei2018 -Dcontextroot.name=icei2018 -Ddbhost.name=localhost -Ddb.name=ICEI2018 -Djboss.dir=/opt/jboss-4.0.3SP1 -DexpirationDay=-1 -Ddb.username=root -Ddb.password=asdf
if "%1"=="icei2018" goto END

:JID
echo 			 ============= BUILDING JID =============
"%JAVA5_HOME%/bin/java" -Xms512m -Xmx1024m -cp c:/opt/apache-ant-1.6.1/lib/ant.jar;c:/opt/apache-ant-1.6.1/lib/ant-launcher.jar;c:/opt/apache-ant-1.6.1/lib/ant-junit.jar;c:/opt/apache-ant-1.6.1/lib/junit/junit.jar;c:/opt/apache-ant-1.6.1/lib/ant-swing.jar;"%JAVA5_HOME%"/lib/tools.jar org.apache.tools.ant.Main -f build.bin.xml deploy -Dcustom.name=jid -Dcontextroot.name=jid -Ddbhost.name=localhost -Ddb.name=JID -Djboss.dir=/opt/jboss-4.0.3SP1 -DexpirationDay=-1 -Ddb.username=root -Ddb.password=asdf
if "%1"=="jid" goto END

:DFNN
echo 			 ============= BUILDING DFNN =============
"%JAVA5_HOME%/bin/java" -Xms512m -Xmx1024m -cp c:/opt/apache-ant-1.6.1/lib/ant.jar;c:/opt/apache-ant-1.6.1/lib/ant-launcher.jar;c:/opt/apache-ant-1.6.1/lib/ant-junit.jar;c:/opt/apache-ant-1.6.1/lib/junit/junit.jar;c:/opt/apache-ant-1.6.1/lib/ant-swing.jar;"%JAVA5_HOME%"/lib/tools.jar org.apache.tools.ant.Main -f build.bin.xml deploy -Dcustom.name=dfnn -Dcontextroot.name=dfnn -Ddbhost.name=localhost -Ddb.name=DFNN -Djboss.dir=/opt/jboss-4.0.3SP1 -DexpirationDay=-1 -Ddb.username=root -Ddb.password=asdf
if "%1"=="dfnn" goto END


:DBIP

IF "%4" == "" GOTO DBIPERR

"%JAVA5_HOME%/bin/java" -Xms512m -Xmx1024m -cp c:/opt/apache-ant-1.6.1/lib/ant.jar;c:/opt/apache-ant-1.6.1/lib/ant-launcher.jar;c:/opt/apache-ant-1.6.1/lib/ant-junit.jar;c:/opt/apache-ant-1.6.1/lib/junit/junit.jar;c:/opt/apache-ant-1.6.1/lib/ant-swing.jar;"%JAVA5_HOME%"/lib/tools.jar org.apache.tools.ant.Main -f build.bin.xml dbip -Dcustom.name=%2 -Dtoken.name=%3 -Ddbhost.name=%4

goto END

:DBIPERR

echo
echo Incomplete Parameters!
echo Usage: build.bin -dbip [custom name] [token ip address] [new ip address]
echo

:END

