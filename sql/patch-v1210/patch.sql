
//---------------------------Hanzel
INSERT INTO AD_FRM_FNCTN (FF_CODE, FF_NM) VALUES (5026, "Adjustment Request");
INSERT INTO AD_FRM_FNCTN VALUES(5526,'Rep Adjustment Request Print');
ALTER TABLE INV_ADJSTMNT_LN ADD AL_SRVD int(11) DEFAULT 0 AFTER AL_ADJST_QTY;
ALTER TABLE AD_APPRVL ADD APR_ENBL_INV_ADJSTMNT_RQST TINYINT(1) DEFAULT 0 AFTER APR_ENBL_AP_CNVSS;


//---------------------------Jess

//--jan 24, 2012
ALTER TABLE AD_APPRVL ADD APR_ENBL_AP_CHCK_PYMNT_RQST TINYINT(1) NOT NULL DEFAULT 0 AFTER APR_ENBL_AP_DBT_MMO;
//----
//--feb 7, 2012 changes from feb 6, 2012 patch
CREATE TABLE INV_TRNSCTNL_BDGT (
	TB_CODE int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
	TB_QNTTY_JAN double,TB_QNTTY_FEB double,TB_QNTTY_MRCH double,
	TB_QNTTY_APRL double,TB_QNTTY_MAY double,TB_QNTTY_JUN double,
	TB_QNTTY_JUL double,TB_QNTTY_AUG double,TB_QNTTY_SEP double,
	TB_QNTTY_OCT double,TB_QNTTY_NOV double,TB_QNTTY_DEC double,
	TB_AMNT double,TB_TTL_AMNT double,TB_YR int(11), INV_UNIT_OF_MEASURE int(11), 
	INV_ITEM int(11), TB_AD_CMPNY int(11) NOT NULL
);
//--
//--feb 13, 2012 changes
ALTER TABLE INV_TRNSCTNL_BDGT ADD TB_DESC varchar(100) AFTER TB_CODE;
INSERT INTO AD_FRM_FNCTN (FF_CODE, FF_NM) VALUES (5027, "Transactional Budget");



//--feb 16, 2012 changes
ALTER TABLE INV_TRNSCTNL_BDGT ADD TB_NAME VARCHAR(100) AFTER TB_CODE;
//----

//--feb 17, 2012
INSERT INTO AD_FRM_FNCTN (FF_CODE, FF_NM) VALUES (3525, "Rep Annual Procurement Plan");
//----

//--feb 21, 2012 changes
INSERT INTO AD_FRM_FNCTN (FF_CODE, FF_NM) VALUES (3037, "Annual Procurement Plan Upload");
//----
//--feb 22, 2012 changes
ALTER TABLE INV_TRNSCTNL_BDGT ADD AD_LOOK_UP_VALUE int(11) AFTER INV_ITEM;
//----
//--feb 25, 2012 changes
ALTER TABLE INV_ITM ADD II_VS_ITM TINYINT(1) DEFAULT 0 AFTER II_ENBL;
//----
//--feb 27, 2012 changes
INSERT INTO AD_LK_UP (LU_NM, LU_DESC, LU_AD_CMPNY) VALUES ('PR TYPE','PR TYPE',1);
//----
//--feb 29, 2012 changes
CREATE TABLE INV_TAG (
	TG_CODE int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
	TG_PRPRTY_CODE VARCHAR(25),
	TG_SRL_NMBR VARCHAR(100),
	TG_SPCS VARCHAR(100),
	TG_EXPRY_DT DATETIME,
	AD_USER int(11),
	AP_PURCHASE_REQUISITION_LINE int(11),
	AP_PURCHASE_ORDER_LINE int(11),
	INV_ADJUSTMENT_LINE int(11),
	INV_BRANCH_STOCK_TRANSFER_LINE int(11),
	TG_AD_CMPNY int(11)
	);
//----
//--march 15, 2012 changes
ALTER TABLE INV_TAG ADD TG_DCMNT_NMBR VARCHAR(25) AFTER TG_SRL_NMBR;
ALTER TABLE AD_PRFRNC ADD PRF_INV_NXT_CSTDN_NMBR1 VARCHAR(25);
ALTER TABLE AD_PRFRNC ADD PRF_INV_NXT_CSTDN_NMBR2 VARCHAR(25) AFTER PRF_INV_NXT_CSTDN_NMBR1;
//--march 20, 2012 changes
ALTER TABLE AD_USR ADD USR_PSTN VARCHAR(25) AFTER USR_DESC;
ALTER TABLE AD_USR ADD USR_INSPCTR TINYINT(1) NOT NULL AFTER USR_HEAD ;
ALTER TABLE AP_PRCHS_ORDR ADD PO_INSPCTD_BY2 VARCHAR(25) AFTER PO_INSPCTD_BY;
ALTER TABLE AP_PRCHS_ORDR ADD PO_INSPCTD_BY3 VARCHAR(25) AFTER PO_INSPCTD_BY2;
ALTER TABLE AP_PRCHS_ORDR ADD PO_INSPCTD_BY4 VARCHAR(25) AFTER PO_INSPCTD_BY3;
//----
//--may 8, 2012 changes
ALTER TABLE INV_TAG MODIFY COLUMN TG_SPCS VARCHAR(255);
ALTER TABLE AD_LK_UP_VL MODIFY COLUMN LV_DESC VARCHAR(255);
//--may 28, 2012 changes
ALTER TABLE AP_PRCHS_RQSTN ADD PR_DLVRY_PRD INT(11);
//----
//--may 30, 2012 changes
ALTER TABLE AP_PRCHS_ORDR ADD PO_DT_OF_OBLGTN DATETIME;
ALTER TABLE AP_PRCHS_ORDR ADD PO_RSN_FR_OBLGTN VARCHAR(255);
//----
//--june 4,2012 changes
ALTER TABLE INV_TAG ADD TG_TXN_DT DATETIME;
ALTER TABLE INV_TAG ADD TG_TYP VARCHAR(25);
//----
INSERT INTO AD_FRM_FNCTN (FF_CODE, FF_NM) VALUES (5025, "Branch Stock Transfer Order");
INSERT INTO AD_FRM_FNCTN (FF_CODE, FF_NM) VALUES (3035, "Check Payment Request Entry");
ALTER TABLE INV_BRNCH_STCK_TRNSFR ADD BST_TRNSFR_ORDER_NMBR varchar(25) AFTER BST_TRNSFR_OUT_NMBR;
ALTER TABLE INV_BRNCH_STCK_TRNSFR MODIFY BST_TYP char(5);
ALTER TABLE AP_VCHR ADD VOU_TYP char(10) AFTER VOU_CODE;
ALTER TABLE AD_APPRVL ADD APR_ENBL_INV_BRNCH_STCK_TRNSFR_ORDR tinyint(1) NOT NULL DEFAULT 0 AFTER APR_ENBL_INV_BRNCH_STCK_TRNSFR;


	



//---------------------------Raymart
ALTER TABLE AD_PRFRNC ADD PRF_AP_DM_OVRRD_CST TINYINT(1) DEFAULT 0 AFTER PRF_INV_IL_SHW_ALL;

ALTER TABLE INV_ITM MODIFY II_NM VARCHAR(100);
ALTER TABLE INV_ITM MODIFY II_DESC VARCHAR(100);
ALTER TABLE AD_PRFRNC ADD PRF_AR_CHK_INSFFCNT_STCK TINYINT(1) DEFAULT 1 AFTER PRF_AR_ALLW_PRR_DT;
ALTER TABLE GL_FC_RT MODIFY FR_X_TO_USD DOUBLE;
ALTER TABLE AR_SLS_ORDR_LN ADD SOL_IDESC VARCHAR(100) DEFAULT "" AFTER SOL_LN;

ALTER TABLE AD_PRFRNC ADD PRF_AR_VLDT_CST_EMAIL TINYINT(1) DEFAULT 0 AFTER PRF_AR_DSBL_SLS_PRC;
ALTER TABLE GL_COA ADD COA_AP_TAG TINYINT(1) DEFAULT 0 AFTER COA_FR_RVLTN;
UPDATE AD_PRFRNC SET PRF_AD_CMPNY=PRF_CODE;

ALTER TABLE AD_PRFRNC ADD PRF_GL_YR_END_CLS_RSTRCTN TINYINT(1);




ALTER TABLE INV_ITM ADD II_TRC_MSC int(11) DEFAULT 0;
ALTER TABLE INV_ITM ADD II_ACQSTN_CST int(11) DEFAULT 0 AFTER II_TRC_MSC;
ALTER TABLE INV_ITM ADD II_LF_SPN int(11) DEFAULT 0 AFTER II_ACQSTN_CST;
ALTER TABLE INV_ITM ADD II_GL_COA_FXD_ASST_ACCNT int(11) AFTER II_LF_SPN;
ALTER TABLE INV_ITM ADD II_GL_COA_DPRCTN_ACCNT int(11) AFTER II_GL_COA_FXD_ASST_ACCNT;
INSERT INTO AD_LK_UP VALUES ('1', 'INV DEPARTMENT', 'INV DEPARTMENT', '2');

ALTER TABLE INV_LCTN ADD LOC_BRNCH varchar(50) AFTER LOC_EML_ADDRSS;
ALTER TABLE INV_LCTN ADD LOC_DPRTMNT varchar(50) AFTER LOC_BRNCH;
ALTER TABLE INV_LCTN ADD LOC_PSTN varchar(50) AFTER LOC_DPRTMNT;
ALTER TABLE INV_LCTN ADD LOC_DT_HRD varchar(50) AFTER LOC_PSTN;
ALTER TABLE INV_LCTN ADD LOC_EMPLYMNT_STTS varchar(50) AFTER LOC_DT_HRD;
ALTER TABLE AD_PRFRNC ADD PRF_INV_CST_PRCSN_UNT SMALLINT(6) DEFAULT 2 AFTER PRF_INV_QTY_PRCSN_UNT;


ALTER TABLE INV_ITM ADD II_GL_ACCMLTD_DPRCTN_ACCNT INT(11) AFTER II_GL_COA_DPRCTN_ACCNT;
ALTER TABLE INV_ITM ADD II_CNDTN VARCHAR(25) AFTER II_GL_ACCMLTD_DPRCTN_ACCNT;
ALTER TABLE INV_ITM ADD II_TX_CD VARCHAR(25) AFTER II_CNDTN;
ALTER TABLE INV_ITM ADD II_RSDL_VL DOUBLE DEFAULT 0 AFTER II_TX_CD;


UPDATE AD_PRFRNC SET PRF_GL_YR_END_CLS_RSTRCTN=0;
ALTER TABLE AD_BNK_ACCNT ADD BA_DC_NMBR_SHW TINYINT(1) DEFAULT 0 AFTER BA_MM_LFT;
ALTER TABLE AD_BNK_ACCNT ADD BA_DC_NMBR_TP INT(11) DEFAULT 0 AFTER BA_DC_NMBR_SHW;
ALTER TABLE AD_BNK_ACCNT ADD BA_DC_NMBR_LFT INT(11) DEFAULT 0 AFTER BA_DC_NMBR_TP;
ALTER TABLE AD_PRFRNC ADD COLUMN PRF_INV_CST_PRCSN_UNT SMALLINT(6) DEFAULT 2 AFTER PRF_INV_QTY_PRCSN_UNT;
ALTER TABLE AD_DLT_ADT_TRL MODIFY DAT_TYP VARCHAR(125);
ALTER TABLE INV_ITM ADD II_LF_SPN INT(11) DEFAULT 0 AFTER II_ACQSTN_CST;
UPDATE INV_ITM SET II_ACQSTN_CST=0 WHERE II_ACQSTN_CST IS NULL;

INSERT INTO INV_UNT_OF_MSR_CNVrSN (UMC_CNVRSN_FCTR,UMC_BS_UNT,UMC_DWNLD_STATUS,INV_ITEM,AP_SUPPLIER,INV_UNIT_OF_MEASURE,UMC_AD_CMPNY) VALUES( 1,1,'N',1139,1,57,5);
INSERT INTO INV_UNT_OF_MSR_CNVrSN (UMC_CNVRSN_FCTR,UMC_BS_UNT,UMC_DWNLD_STATUS,INV_ITEM,AP_SUPPLIER,INV_UNIT_OF_MEASURE,UMC_AD_CMPNY) VALUES( 250,0,'N',1139,1,58,5);
//----------------------------------



ALTER TABLE INV_ITM MODIFY II_PRCHS_UNT VARCHAR(25);
ALTER TABLE INV_ITM MODIFY II_LOOSE VARCHAR(25);




//------------------------------Clint

ALTER TABLE AP_PRCHS_RQSTN ADD PR_TG_STATUS varchar(25) AFTER PR_DT_PSTD;
ALTER TABLE AP_PRCHS_RQSTN ADD PR_TYPE varchar(25) AFTER PR_TG_STATUS;

ALTER TABLE AD_APPRVL ADD APR_ENBL_AP_PRCHS_RQSTN tinyint(1) NOT NULL AFTER APR_ENBL_INV_BLD;
insert into AD_LK_UP (LU_CODE, LU_NM, LU_DESC, LU_AD_CMPNY) VALUES (38,'DEPARTMENT','DEPARTMENT',3);
ALTER TABLE AD_USR ADD USR_DEPT varchar(25) AFTER USR_NM;
ALTER TABLE AD_USR ADD USR_HEAD tinyint(1) NOT NULL AFTER USR_DESC;

insert into AD_APPRVL_DCMNT (ADC_CODE,ADC_TYP,ADC_PRNT_OPTN,ADC_ALLW_DPLCT,ADC_TRCK_DPLCT,ADC_ENBL_CRDT_LMT_CHCKNG,ADC_AD_CMPNY) VALUES (81 ,"AP CANVASS","PRINT ANYTIME",1,0,0,2);

ALTER TABLE AD_APPRVL ADD APR_ENBL_AP_CNVSS tinyint(1) NOT NULL AFTER APR_ENBL_AP_PRCHS_RQSTN; 

ALTER TABLE AP_PRCHS_RQSTN ADD PR_CNVSS_APPRVL_STATUS varchar(10) AFTER PR_PSTD;
ALTER TABLE AP_PRCHS_RQSTN ADD PR_CNVSS_PSTD tinyint(1) AFTER PR_CNVSS_APPRVL_STATUS;

ALTER TABLE AP_PRCHS_RQSTN ADD PR_CNVSS_APPRVD_RJCTD_BY varchar(25) AFTER PR_DT_PSTD;
ALTER TABLE AP_PRCHS_RQSTN ADD PR_CNVSS_DT_APPRVD_RJCTD datetime AFTER PR_CNVSS_APPRVD_RJCTD_BY; 
ALTER TABLE AP_PRCHS_RQSTN ADD PR_CNVSS_PSTD_BY varchar(25) AFTER PR_CNVSS_DT_APPRVD_RJCTD; 
ALTER TABLE AP_PRCHS_RQSTN ADD PR_CNVSS_DT_PSTD datetime AFTER PR_CNVSS_PSTD_BY; 


ALTER TABLE AP_SPPLR ADD INV_ITEM INTEGER AFTER AD_BANK_ACCOUNT;
ALTER TABLE AP_SPPLR ADD INDEX INV_ITEM_IDX(INV_ITEM);

// create a script to transfer ap_supplier fk to inv_item

ALTER TABLE INV_UNT_OF_MSR_CNVRSN ADD AP_SUPPLIER INTEGER AFTER INV_ITEM;
ALTER TABLE INV_UNT_OF_MSR_CNVRSN ADD INDEX AP_SUPPLIER_IDX(AP_SUPPLIER);

    

ALTER TABLE INV_ITM ADD II_ACQSTN_CST int(11)AFTER II_TRC_MSC;
ALTER TABLE INV_ITM ADD II_LF_SPN int(11) AFTER II_ACQSTN_CST; 
ALTER TABLE INV_ITM ADD II_RSDL_VL double AFTER II_LF_SPN; 
ALTER TABLE INV_ITM ADD II_GL_COA_FXD_ASST_ACCNT int(11) AFTER II_RSDL_VL; 
ALTER TABLE INV_ITM ADD II_GL_COA_DPRCTN_ACCNT int(11) AFTER II_GL_COA_FXD_ASST_ACCNT;
ALTER TABLE INV_ITM ADD II_GL_ACCMLTD_DPRCTN_ACCNT int(11) AFTER II_GL_COA_DPRCTN_ACCNT;
ALTER TABLE INV_ITM ADD II_CNDTN varchar(25) AFTER II_GL_ACCMLTD_DPRCTN_ACCNT;
ALTER TABLE INV_ITM ADD II_TX_CD varchar(25) AFTER II_CNDTN;

ALTER TABLE INV_ITM ADD II_YLD double AFTER II_SHPPNG_CST;
ALTER TABLE INV_ITM ADD	II_STD_LST_PRCNTG double AFTER II_YLD; 

//--------------------------------------------------------
//UPDATES JAN 12,2012

ALTER TABLE AP_PRCHS_RQSTN ADD PR_GNRTD tinyint(1) AFTER PR_PSTD;
ALTER TABLE AD_RSPNSBLTY modify RS_NM varchar(255);

//---------------------------Jay
ALTER TABLE AP_PRCHS_ORDR_LN ADD PL_CNVRSN_FCTR double AFTER PL_AMNT;

//UPDATES JAN 24,2012
ALTER TABLE AP_PRCHS_RQSTN ADD PR_AD_USR_NM1 int(11) AFTER PR_TYPE;
ALTER TABLE AP_PRCHS_RQSTN ADD PR_SCHDL VARCHAR(25) AFTER PR_AD_USR_NM1;
ALTER TABLE AP_PRCHS_RQSTN ADD PR_NXT_RN_DT DATETIME AFTER PR_SCHDL;
ALTER TABLE AP_PRCHS_RQSTN ADD PR_LST_RN_DT	DATETIME AFTER PR_NXT_RN_DT;

//UPDATES JAN 27,2012
ALTER TABLE INV_ITM ADD II_SC_SNDY tinyint(1) NOT NULL AFTER II_TRC_MSC;
ALTER TABLE INV_ITM ADD II_SC_MNDY tinyint(1) NOT NULL AFTER II_SC_SNDY;
ALTER TABLE INV_ITM ADD II_SC_TSDY tinyint(1) NOT NULL AFTER II_SC_MNDY;
ALTER TABLE INV_ITM ADD II_SC_WDNSDY tinyint(1) NOT NULL AFTER II_SC_TSDY;
ALTER TABLE INV_ITM ADD II_SC_THRSDY tinyint(1) NOT NULL AFTER II_SC_WDNSDY;
ALTER TABLE INV_ITM ADD II_SC_FRDY tinyint(1) NOT NULL AFTER II_SC_THRSDY;
ALTER TABLE INV_ITM ADD II_SC_STRDY tinyint(1) NOT NULL AFTER II_SC_FRDY;

//UPDATES FEB 15, 2012
INSERT INTO AD_FRM_FNCTN VALUES(3036, 'CANVASS');

//UPDATES FEB 17, 2012
ALTER TABLE AP_PRCHS_ORDR ADD PO_SHPMNT_NMBR VARCHAR(25) AFTER PO_VD;
ALTER TABLE AP_PRCHS_ORDR_LN ADD PL_IMPRT_CST DOUBLE AFTER PL_DSCNT_4;

//UPDATE FEB 24, 2012

INSERT INTO AD_FRM_FNCTN (FF_CODE, FF_NM) VALUES (3524, "Purchase Requisition Register");

//UPDATE FEB 27, 2012

ALTER TABLE AD_PRFRNC ADD PRF_INV_GNRL_ADJSTMNT_ACCNT int(11) AFTER PRF_INV_POS_ADJSTMNT_ACCNT;
ALTER TABLE AD_PRFRNC ADD PRF_INV_ISSNC_ADJSTMNT_ACCNT int(11) AFTER PRF_INV_GNRL_ADJSTMNT_ACCNT;
ALTER TABLE AD_PRFRNC ADD PRF_INV_PRDCTN_ADJSTMNT_ACCNT int(11) AFTER PRF_INV_ISSNC_ADJSTMNT_ACCNT;
ALTER TABLE AD_PRFRNC ADD PRF_INV_WSTG_ADJSTMNT_ACCNT int(11) AFTER PRF_INV_PRDCTN_ADJSTMNT_ACCNT;

UPDATE AD_PRFRNC SET PRF_INV_GNRL_ADJSTMNT_ACCNT=1;
UPDATE AD_PRFRNC SET PRF_INV_ISSNC_ADJSTMNT_ACCNT=1;
UPDATE AD_PRFRNC SET PRF_INV_PRDCTN_ADJSTMNT_ACCNT=1;
UPDATE AD_PRFRNC SET PRF_INV_WSTG_ADJSTMNT_ACCNT=1;

//UPDATE MARCH 9, 2012
ALTER TABLE AP_PRCHS_ORDR ADD PO_INSPCTD_BY VARCHAR(25) AFTER PO_OTHRS4;
ALTER TABLE AP_PRCHS_ORDR ADD PO_DT_INSPCTD datetime AFTER PO_INSPCTD_BY;
ALTER TABLE AP_PRCHS_ORDR ADD PO_RLSD_BY VARCHAR(25) AFTER PO_DT_INSPCTD;
ALTER TABLE AP_PRCHS_ORDR ADD PO_DT_RLSD datetime AFTER PO_RLSD_BY;



// UPDATE NOVEMBER 12, 2012
//FOR DOCUMENT



INSERT INTO AD_DCMNT_CTGRY (DC_CODE, DC_NM, DC_DESC, AD_APPLICATION, DC_AD_CMPNY)
	VALUES (76, 'AP CHECK PAYMENT REQUEST', 'CHECK PAYMENT REQUEST', 9, 2);
	
INSERT INTO AD_DCMNT_SQNC (DS_CODE, DS_SQNC_NM, DS_DT_FRM, DS_NMBRNG_TYP, DS_INTL_VL, AD_APPLICATION, DS_AD_CMPNY)
	VALUES (76, 'AP CHECK PAYMENT REQUEST DOCUMENT', '2012-01-01', 'A', 1, 9, 2); 

INSERT INTO AD_DCMNT_SQNC_ASSGNMNT (DSA_CODE, DSA_SOB_CODE, DSA_NXT_SQNC, AD_DOCUMENT_CATEGORY, AD_DOCUMENT_SEQUENCE,DS_DT_FRM, DSA_AD_CMPNY)
	VALUES (76, 1, 'CPR0001', 76, 76,, 2);



INSERT INTO AD_DCMNT_CTGRY (DC_CODE,DC_NM, DC_DESC, AD_APPLICATION, DC_AD_CMPNY)
	VALUES (77,'INV TRANSACTIONAL BUDGET', 'TRANSACTIONAL BUDGET', 12, 2);

INSERT INTO AD_DCMNT_SQNC (DS_CODE, DS_SQNC_NM, DS_NMBRNG_TYP, DS_DT_FRM, DS_INTL_VL, AD_APPLICATION, DS_AD_CMPNY)
	VALUES (77,'INV TRANSACTIONAL BUDGET DOCUMENT', '2012-01-01', 'A', 1, 12, 2);

INSERT INTO AD_DCMNT_SQNC_ASSGNMNT (DSA_CODE,DSA_SOB_CODE, DSA_NXT_SQNC, AD_DOCUMENT_CATEGORY, AD_DOCUMENT_SEQUENCE, DSA_AD_CMPNY)
	VALUES (77,1, 'TB0001', 77, 77, 2);




INSERT INTO AD_DCMNT_CTGRY (DC_CODE, DC_NM, DC_DESC, AD_APPLICATION, DC_AD_CMPNY)
	VALUES (78, 'INV BRANCH STOCK TRANSFER ORDER', 'BRANCH STOCK TRANSFER ORDER', 12, 2);

INSERT INTO AD_DCMNT_SQNC (DS_CODE, DS_SQNC_NM, DS_DT_FRM, DS_NMBRNG_TYP, DS_INTL_VL, AD_APPLICATION, DS_AD_CMPNY)
	VALUES (78, 'INV BRANCH STOCK TRANSFER ORDER DOCUMENT', '2012-01-01', 'A', 1, 12, 2);

INSERT INTO AD_DCMNT_SQNC_ASSGNMNT (DSA_CODE, DSA_SOB_CODE, DSA_NXT_SQNC, AD_DOCUMENT_CATEGORY, AD_DOCUMENT_SEQUENCE, DSA_AD_CMPNY)
	VALUES (78, 1, 'BSOS0001', 78, 78, 2);

	
	



-----------------------jimmy
//UPDATES MARCH 20 2012

ALTER TABLE INV_ADJSTMNT ADD ADJ_NT_BY VARCHAR(100) AFTER ADJ_VD;

// UPDATES MARCH 23 2012

ALTER TABLE AP_SPPLR DROP INV_ITEM;
ALTER TABLE INV_UNT_OF_MSR_CNVRSN DROP AP_SUPPLIER;

// UPDATE APRIL 10, 2012
ALTER TABLE AP_VCHR ADD VOU_GNRTD tinyint(1) NOT NULL AFTER VOU_PSTD; 

ALTER TABLE INV_ADJSTMNT ADD ADJ_NT_BY VARCHAR(150);

//UPDATES MAY 09, 2012

ALTER TABLE INV_PHYSCL_INVNTRY_LN ADD PIL_EXPRY_DT LONGTEXT AFTER PIL_VRNC;

//UPDATES JUNE 01, 2012

ALTER TABLE AR_SLS_ORDR ADD SO_ORDR_STTS VARCHAR(50) AFTER SO_MMO;
ALTER TABLE AR_INVC ADD INV_RCVD_DT DATETIME AFTER INV_EFFCTVTY_DT;
ALTER TABLE AR_DSTRBTN_RCRD ADD DR_SC_ACCNT INT(11) AFTER DR_RVRSD;

ALTER TABLE INV_BRNCH_STCK_TRNSFR MODIFY COLUMN BST_TYP CHAR(10);

ALTER TABLE INV_ADJSTMNT ADD ADJ_GNRTD TINYINT(1) AFTER ADJ_PSTD;


ALTER TABLE AR_INVC ADD INV_CM_RFRNC_NMBR VARCHAR(100) AFTER INV_CM_INVC_NMBR;
ALTER TABLE AD_USR ADD USR_PO_APPRVL_CTR VARCHAR(7) DEFAULT '000000' AFTER USR_PSSWRD_EXPRTN_ACCSS;

-----------------------riel
//UPDATES MAY 02 2013

ALTER TABLE AR_INVC_LN_ITM ADD ILI_IMEI VARCHAR(100) AFTER ILI_EXPRY_DT;
ALTER TABLE AP_VCHR_LN_ITM ADD VLI_MISC VARCHAR(100) AFTER VLI_TTL_DSCNT;



