-- --------------------------------------------------------------------------------
-- Routine DDL
-- Note: comments before and after the routine body will not be stored by the server
-- --------------------------------------------------------------------------------
DELIMITER $$

CREATE DEFINER=`root`@`localhost` PROCEDURE `executeSpRecomputeCoaBalance`(
IN ACCNT_FRM varchar(100),
IN ACCNT_TO varchar(100)
 )
BEGIN

SELECT C.COA_ACCNT_NMBR AS ACCNT_NMBR , C.COA_ACCNT_TYP AS ACCNT_TYP, C.COA_ACCNT_DESC AS ACCNT_DESC, 
		LAST_DAY(B.JR_EFFCTV_DT) AS DATE, A.JL_DBT AS DEBIT, ROUND(SUM(A.JL_AMNT),2) AS AMOUNT
FROM GL_JRNL_LN A 
LEFT JOIN GL_JRNL B ON A.GL_JOURNAL = B.JR_CODE
LEFT JOIN GL_COA C ON A.GL_CHART_OF_ACCOUNT = C.COA_CODE
LEFT JOIN AD_BRNCH D ON B.JR_AD_BRNCH = D.BR_CODE
LEFT JOIN GL_JRNL_SRC E ON B.GL_JOURNAL_SOURCE = E.JS_CODE
WHERE B.JR_PSTD = 1 AND C.COA_ACCNT_NMBR BETWEEN ACCNT_FRM AND ACCNT_TO
GROUP BY C.COA_ACCNT_NMBR,A.JL_DBT, DATE
ORDER BY C.COA_ACCNT_NMBR, DATE;



END