/*
 * com/ejb/ar/LocalAdAgingBucketBean.java
 *
 * Created on June 9, 2003, 4:24 PM
 *
 */
 
package com.ejb.ad;

import java.util.Collection;

import javax.ejb.CreateException;
import javax.ejb.EJBException;

import com.util.AbstractEntityBean;
import com.util.Debug;

/**
 *
 * @author  Neil Andrew M. Ajero
 *
 * @ejb:bean name="AdAgingBucketEJB"
 *           display-name="Aging Bucket Entity"
 *           type="CMP"
 *           cmp-version="2.x"
 *           view-type="local"
 *           local-jndi-name="omega-ejb/AdAgingBucketEJB"
 *           schema="AdAgingBucket"
 *           primkey-field="abCode"
 *
 * @ejb:pk class="java.lang.Integer"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="aruser"
 *                        role-link="aruserlink"
 *
 * @ejb:permission role-name="aruser"
 *
 * @ejb:interface local-class="com.ejb.ad.LocalAdAgingBucket"
 *                local-extends="javax.ejb.EJBLocalObject"
 *
 * @ejb:home local-class="com.ejb.ad.LocalAdAgingBucketHome"
 *           local-extends="javax.ejb.EJBLocalHome"
 *
 * @ejb:finder signature="Collection findAbAll(java.lang.Integer AB_AD_CMPNY)"
 *             query="SELECT OBJECT(ab) FROM AdAgingBucket ab WHERE ab.abAdCompany = ?1"
 *
 * @jboss:query signature="Collection findAbAll(java.lang.Integer AB_AD_CMPNY)"
 *             query="SELECT OBJECT(ab) FROM AdAgingBucket ab WHERE ab.abAdCompany = ?1 ORDER BY ab.abName"
 *
 * @ejb:finder signature="Collection findEnabledAbAll(java.lang.Integer AB_AD_CMPNY)"
 *             query="SELECT OBJECT(ab) FROM AdAgingBucket ab WHERE ab.abEnable=1 AND ab.abAdCompany = ?1"
 *
 * @ejb:finder signature="LocalAdAgingBucket findByAbName(java.lang.String AB_NM, java.lang.Integer AB_AD_CMPNY)"
 *             query="SELECT OBJECT(ab) FROM AdAgingBucket ab WHERE ab.abName = ?1 AND ab.abAdCompany = ?2"
 *
 * @ejb:value-object match="*"
 *             name="AdAgingBucket"
 *
 * @jboss:persistence table-name="AD_AGNG_BCKT"
 *
 * @jboss:entity-command name="mysql-get-generated-keys"
 */

public abstract class AdAgingBucketBean extends AbstractEntityBean {
    
   
    // PERSISTENT METHODS
    
    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     * @ejb:pk-field
     *
     * @jboss:column-name name="AB_CODE"
     *
     * @jboss:persistence auto-increment="true"
     **/
    public abstract Integer getAbCode();
    public abstract void setAbCode(Integer AB_CODE);
    
    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="AB_NM"
     **/
    public abstract String getAbName();
     /**
      * @ejb:interface-method view-type="local"
      **/  
    public abstract void setAbName(String AB_NM);
    
    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="AB_DESC"
     **/
    public abstract String getAbDescription();
     /**
      * @ejb:interface-method view-type="local"
      **/  
    public abstract void setAbDescription(String AB_DESC);

    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="AB_TYP"
     **/
    public abstract String getAbType();
     /**
      * @ejb:interface-method view-type="local"
      **/  
    public abstract void setAbType(String AB_TYP);

    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="AB_ENBL"
     **/
    public abstract byte getAbEnable();
     /**
      * @ejb:interface-method view-type="local"
      **/  
    public abstract void setAbEnable(byte AB_ENBL);
    
    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="AB_AD_CMPNY"
     **/
    public abstract Integer getAbAdCompany();
     /**
      * @ejb:interface-method view-type="local"
      **/  
    public abstract void setAbAdCompany(Integer AB_AD_CMPNY);
    
    
    // RELATIONSHIP METHODS
    
    /**
     * @ejb:interface-method view-type="local"
     * @ejb:relation name="agingbucket-agingbucketvalues"
     *               role-name="agingbucket-has-many-agingbucketvalues"
     * 
     */
     public abstract Collection getAdAgingBucketValues();
     public abstract void setAdAgingBucketValues(Collection adAgingBucketValues);
     
    // BUSINESS METHODS
     
    /**
     * @ejb:interface-method view-type="local"
     **/
     public void addAdAgingBucketValue(LocalAdAgingBucketValue adAgingBucketValue) {

         Debug.print("AdAgingBucketBean addAdAgingBucketValue");
      
         try {
      	
            Collection adAgingBucketValues = getAdAgingBucketValues();
	        adAgingBucketValues.add(adAgingBucketValue);
	         
         } catch (Exception ex) {
      	
           throw new EJBException(ex.getMessage());
      
         }   
     }

    /**
     * @ejb:interface-method view-type="local"
     **/
     public void dropAdAgingBucketValue(LocalAdAgingBucketValue adAgingBucketValue) {

         Debug.print("AdAgingBucketBean dropAdAgingBucketValue");
      
         try {
      	
            Collection adAgingBucketValues = getAdAgingBucketValues();
	        adAgingBucketValues.remove(adAgingBucketValue);
	      
         } catch (Exception ex) {
      	
           throw new EJBException(ex.getMessage());
         
         }   
     } 
   
    // ENTITY METHODS
    
    /**
     * @ejb:create-method view-type="local"
     **/
     public Integer ejbCreate(Integer AB_CODE, 
         String AB_NM, String AB_DESC, String AB_TYP,
         byte AB_ENBL, Integer AB_AD_CMPNY)
         throws CreateException {
      	
         Debug.print("AdAgingBucketBean ejbCreate");
      
         setAbCode(AB_CODE);
         setAbName(AB_NM);
         setAbDescription(AB_DESC);
         setAbType(AB_TYP);
         setAbEnable(AB_ENBL);
         setAbAdCompany(AB_AD_CMPNY);
        
         return null;
    }
    
    /**
     * @ejb:create-method view-type="local"
     **/
     public Integer ejbCreate(
         String AB_NM, String AB_DESC, String AB_TYP,
         byte AB_ENBL, Integer AB_AD_CMPNY)
         throws CreateException {
      	
         Debug.print("AdLookUpBean ejbCreate");
         
         setAbName(AB_NM);
         setAbDescription(AB_DESC);
         setAbType(AB_TYP);
         setAbEnable(AB_ENBL);
         setAbAdCompany(AB_AD_CMPNY);
        
         return null;
     }
    
     public void ejbPostCreate(Integer AB_CODE, 
         String AB_NM, String AB_DESC, String AB_TYP,
         byte AB_ENBL, Integer AB_AD_CMPNY)      
         throws CreateException { }
      
     public void ejbPostCreate( 
         String AB_NM, String AB_DESC, String AB_TYP,
         byte AB_ENBL, Integer AB_AD_CMPNY)
         throws CreateException { }
}

      
      	
      	