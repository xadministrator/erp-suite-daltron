/*
 * com/ejb/ar/LocalAdAgingBucketValueBean.java
 *
 * Created on June 9, 2003, 4:45 PM
 *
 */
 
package com.ejb.ad;

import javax.ejb.CreateException;

import com.util.AbstractEntityBean;
import com.util.Debug;

/**
 *
 * @author  Neil Andrew M. Ajero
 *
 * @ejb:bean name="AdAgingBucketValueEJB"
 *           display-name="Aging Bucket Value Entity"
 *           type="CMP"
 *           cmp-version="2.x"
 *           view-type="local"
 *           local-jndi-name="omega-ejb/AdAgingBucketValueEJB"
 *           schema="AdAgingBucketValue"
 *           primkey-field="avCode"
 *
 * @ejb:pk class="java.lang.Integer"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="aruser"
 *                        role-link="aruserlink"
 *
 * @ejb:permission role-name="aruser"
 *
 * @ejb:interface local-class="com.ejb.ad.LocalAdAgingBucketValue"
 *                local-extends="javax.ejb.EJBLocalObject"
 *
 * @ejb:home local-class="com.ejb.ad.LocalAdAgingBucketValueHome"
 *           local-extends="javax.ejb.EJBLocalHome"
 *
 * @ejb:finder signature="Collection findByAbCode(java.lang.Integer AB_CODE, java.lang.Integer AV_AD_CMPNY)"
 *             query="SELECT OBJECT(av) FROM AdAgingBucket ab, IN(ab.adAgingBucketValues) av WHERE ab.abCode = ?1 AND av.avAdCompany = ?2"
 *
 * @jboss:query signature="Collection findByAbCode(java.lang.Integer AB_CODE, java.lang.Integer AV_AD_CMPNY)"
 *             query="SELECT OBJECT(av) FROM AdAgingBucket ab, IN(ab.adAgingBucketValues) av WHERE ab.abCode = ?1 AND av.avAdCompany = ?2 ORDER BY av.avSequenceNumber"
 *
 * @ejb:finder signature="Collection findByAbName(java.lang.String AB_NM, java.lang.Integer AV_AD_CMPNY)"
 *             query="SELECT OBJECT(av) FROM AdAgingBucket ab, IN(ab.adAgingBucketValues) av WHERE ab.abName = ?1 AND av.avAdCompany = ?2" 
 *
 * @jboss:query signature="Collection findByAbName(java.lang.String AB_NM, java.lang.Integer AV_AD_CMPNY)"
 *             query="SELECT OBJECT(av) FROM AdAgingBucket ab, IN(ab.adAgingBucketValues) av WHERE ab.abName = ?1 AND av.avAdCompany = ?2 ORDER BY av.avSequenceNumber" 
 *
 * @ejb:finder signature="LocalAdAgingBucketValue findByAvSequenceNumberAndAbCode(short AV_SQNC_NMBR, java.lang.Integer AB_CODE, java.lang.Integer AV_AD_CMPNY)"
 *             query="SELECT OBJECT(av) FROM AdAgingBucket ab, IN(ab.adAgingBucketValues) av WHERE av.avSequenceNumber=?1 AND ab.abCode=?2 AND av.avAdCompany = ?3"
 *
 * @ejb:value-object match="*"
 *             name="AdAgingBucketValue"
 *
 * @jboss:persistence table-name="AD_AGNG_BCKT_VL"
 *
 * @jboss:entity-command name="mysql-get-generated-keys"
 */

public abstract class AdAgingBucketValueBean extends AbstractEntityBean {
    
   
    // PERSISTENT METHODS
    
    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     * @ejb:pk-field
     *
     * @jboss:column-name name="AV_CODE"
     *
     * @jboss:persistence auto-increment="true"
     **/
    public abstract Integer getAvCode();
    public abstract void setAvCode(Integer AV_CODE);
    
    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="AV_SQNC_NMBR"
     **/
    public abstract short getAvSequenceNumber();
     /**
      * @ejb:interface-method view-type="local"
      **/  
    public abstract void setAvSequenceNumber(short AV_SQNC_NMBR);

    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="AV_TYP"
     **/
    public abstract String getAvType();
     /**
      * @ejb:interface-method view-type="local"
      **/  
    public abstract void setAvType(String AV_TYP);


    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="AV_DYS_FRM"
     **/
    public abstract long getAvDaysFrom();
     /**
      * @ejb:interface-method view-type="local"
      **/  
    public abstract void setAvDaysFrom(long AV_DYS_FRM);
    

    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="AV_DYS_TO"
     **/
    public abstract long getAvDaysTo();
     /**
      * @ejb:interface-method view-type="local"
      **/  
    public abstract void setAvDaysTo(long AV_DYS_TO);
        
    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="AV_DESC"
     **/
    public abstract String getAvDescription();
     /**
      * @ejb:interface-method view-type="local"
      **/  
    public abstract void setAvDescription(String AV_DESC);
    
    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="AV_AD_CMPNY"
     **/
    public abstract Integer getAvAdCompany();
     /**
      * @ejb:interface-method view-type="local"
      **/  
    public abstract void setAvAdCompany(Integer AV_AD_CMPNY);

    
    // RELATIONSHIP METHODS
    
    /**
     * @ejb:interface-method view-type="local"
     * @ejb:relation name="agingbucket-agingbucketvalues"
     *               role-name="agingbucketvalue-has-one-agingbucket"
     * 
     * @jboss:relation related-pk-field="abCode"
     *                 fk-column="AD_AGING_BUCKET"
     */
     public abstract LocalAdAgingBucket getAdAgingBucket();
     public abstract void setAdAgingBucket(LocalAdAgingBucket adAgingBucket);
     
    // BUSINESS METHODS
     
    // ENTITY METHODS
    
    /**
     * @ejb:create-method view-type="local"
     **/
     public Integer ejbCreate(Integer AV_CODE, 
         short AV_SQNC_NMBR, String AV_TYP, long AV_DYS_FRM, long AV_DYS_TO,
         String AV_DESC, Integer AV_AD_CMPNY)
         throws CreateException {
      	
         Debug.print("AdAgingBucketValueBean ejbCreate");
      
         setAvCode(AV_CODE);
         setAvSequenceNumber(AV_SQNC_NMBR);
         setAvType(AV_TYP);
         setAvDaysFrom(AV_DYS_FRM);
         setAvDaysTo(AV_DYS_TO);
         setAvDescription(AV_DESC);
         setAvAdCompany(AV_AD_CMPNY);
        
         return null;
     } 
    
    /**
     * @ejb:create-method view-type="local"
     **/
     public Integer ejbCreate(
         short AV_SQNC_NMBR, String AV_TYP, long AV_DYS_FRM, long AV_DYS_TO,
         String AV_DESC, Integer AV_AD_CMPNY)
         throws CreateException {
      	
         Debug.print("AdAgingBucketValueBean ejbCreate");
        
         setAvSequenceNumber(AV_SQNC_NMBR);
         setAvType(AV_TYP);
         setAvDaysFrom(AV_DYS_FRM);
         setAvDaysTo(AV_DYS_TO);
         setAvDescription(AV_DESC);
         setAvAdCompany(AV_AD_CMPNY);
        
         return null;
     }
    
     public void ejbPostCreate(Integer AV_CODE, 
         short AV_SQNC_NMBR, String AV_TYP, long AV_DYS_FRM, long AV_DYS_TO,
         String AV_DESC, Integer AV_AD_CMPNY)      
         throws CreateException { }
      
     public void ejbPostCreate(
         short AV_SQNC_NMBR, String AV_TYP, long AV_DYS_FRM, long AV_DYS_TO,
         String AV_DESC, Integer AV_AD_CMPNY)
         throws CreateException { }     
}