/*
 * com/ejb/ad/LocalAdAmountLimitBean.java
 *
 * Created on March 19, 2004, 3:06 PM
 */
 
package com.ejb.ad;

import java.util.Collection;

import javax.ejb.CreateException;
import javax.ejb.EJBException;

import com.util.AbstractEntityBean;
import com.util.Debug;

/**
 *
 * @author  Neil Andrew M. Ajero
 *
 * @ejb:bean name="AdAmountLimitEJB"
 *           display-name="Amount Limit Entity"
 *           type="CMP"
 *           cmp-version="2.x"
 *           view-type="local"
 *           local-jndi-name="omega-ejb/AdAmountLimitEJB"
 *           schema="AdAmountLimit"
 *           primkey-field="calCode"
 *
 * @ejb:pk class="java.lang.Integer"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="aduser"
 *                        role-link="aduserlink"
 *
 * @ejb:permission role-name="aduser"
 *
 * @ejb:interface local-class="com.ejb.ad.LocalAdAmountLimit"
 *                local-extends="javax.ejb.EJBLocalObject"
 *
 * @ejb:home local-class="com.ejb.ad.LocalAdAmountLimitHome"
 *           local-extends="javax.ejb.EJBLocalHome"
 *
 * @ejb:finder signature="LocalAdAmountLimit findByAdcTypeAndAuTypeAndUsrName(java.lang.String ADC_TYP, java.lang.String AU_TYP, java.lang.String USR_NM, java.lang.Integer CAL_AD_CMPNY)"
 *             query="SELECT OBJECT(cal) FROM AdAmountLimit cal, IN(cal.adApprovalUsers) au WHERE cal.adApprovalDocument.adcType = ?1 AND au.auType = ?2 AND au.adUser.usrName = ?3 AND cal.calAdCompany = ?4"
 *             
 * @ejb:finder signature="Collection findByAdcTypeAndAuTypeAndUsrNameAndAmountLimit(java.lang.String ADC_TYP, java.lang.String AU_TYP, java.lang.String USR_NM, double AMNT, java.lang.Integer CAL_AD_CMPNY)"
 *             query="SELECT OBJECT(cal) FROM AdAmountLimit cal, IN(cal.adApprovalUsers) au WHERE cal.adApprovalDocument.adcType = ?1 AND au.auType = ?2 AND au.adUser.usrName = ?3 AND cal.calAmountLimit >= ?4 AND cal.calAdCompany = ?5 ORDER BY cal.calAmountLimit ASC"
 *
 * @ejb:finder signature="LocalAdAmountLimit findByCoaCodeAndAuTypeAndUsrName(java.lang.Integer COA_CODE, java.lang.String AU_TYP, java.lang.String USR_NM, java.lang.Integer CAL_AD_CMPNY)"
 *             query="SELECT OBJECT(cal) FROM AdAmountLimit cal, IN(cal.adApprovalUsers) au WHERE cal.adApprovalCoaLine.glChartOfAccount.coaCode = ?1 AND au.auType = ?2 AND au.adUser.usrName = ?3 AND cal.calAdCompany = ?4"
 *
 * @ejb:finder signature="Collection findByAdcTypeAndGreaterThanCalAmountLimit(java.lang.String ADC_TYP, double CAL_AMNT_LMT, java.lang.Integer CAL_AD_CMPNY)"
 *             query="SELECT OBJECT(cal) FROM AdAmountLimit cal WHERE cal.adApprovalDocument.adcType = ?1 AND cal.calAmountLimit > ?2 AND cal.calAdCompany = ?3"
 *
 * @jboss:query signature="Collection findByAdcTypeAndGreaterThanCalAmountLimit(java.lang.String ADC_TYP, double CAL_AMNT_LMT, java.lang.Integer CAL_AD_CMPNY)"
 *             query="SELECT OBJECT(cal) FROM AdAmountLimit cal WHERE cal.adApprovalDocument.adcType = ?1 AND cal.calAmountLimit > ?2 AND cal.calAdCompany = ?3 ORDER BY cal.calAmountLimit"
 *
 * @ejb:finder signature="Collection findByCoaCodeAndGreaterThanCalAmountLimit(java.lang.Integer COA_CODE, double CAL_AMNT_LMT, java.lang.Integer CAL_AD_CMPNY)"
 *             query="SELECT OBJECT(cal) FROM AdAmountLimit cal WHERE cal.adApprovalCoaLine.glChartOfAccount.coaCode = ?1 AND cal.calAmountLimit > ?2 AND cal.calAdCompany = ?3"
 *
 * @jboss:query signature="Collection findByCoaCodeAndGreaterThanCalAmountLimit(java.lang.Integer COA_CODE, double CAL_AMNT_LMT, java.lang.Integer CAL_AD_CMPNY)"
 *             query="SELECT OBJECT(cal) FROM AdAmountLimit cal WHERE cal.adApprovalCoaLine.glChartOfAccount.coaCode = ?1 AND cal.calAmountLimit > ?2 AND cal.calAdCompany = ?3 ORDER BY cal.calAmountLimit"
 *
 * @ejb:finder signature="Collection findByAdcCode(java.lang.Integer ADC_CODE, java.lang.Integer CAL_AD_CMPNY)"
 *             query="SELECT OBJECT(cal) FROM AdApprovalDocument adc, IN(adc.adAmountLimits) cal WHERE adc.adcCode = ?1 AND cal.calAdCompany = ?2"
 *
 * @jboss:query signature="Collection findByAdcCode(java.lang.Integer ADC_CODE, java.lang.Integer CAL_AD_CMPNY)"
 *             query="SELECT OBJECT(cal) FROM AdApprovalDocument adc, IN(adc.adAmountLimits) cal WHERE adc.adcCode = ?1 AND cal.calAdCompany = ?2 ORDER BY cal.calAmountLimit"
 *
 * @ejb:finder signature="Collection findByAclCode(java.lang.Integer ACL_CODE, java.lang.Integer CAL_AD_CMPNY)"
 *             query="SELECT OBJECT(cal) FROM AdApprovalCoaLine acl, IN(acl.adAmountLimits) cal WHERE acl.aclCode = ?1 AND cal.calAdCompany = ?2"
 *
 * @jboss:query signature="Collection findByAclCode(java.lang.Integer ACL_CODE, java.lang.Integer CAL_AD_CMPNY)"
 *             query="SELECT OBJECT(cal) FROM AdApprovalCoaLine acl, IN(acl.adAmountLimits) cal WHERE acl.aclCode = ?1 AND cal.calAdCompany = ?2 ORDER BY cal.calAmountLimit"
 *
 * @ejb:finder signature="LocalAdAmountLimit findByCalDeptAndCalAmountLimitAndAdcCode(java.lang.String CAL_DEPT, double CAL_AMNT_LMT, java.lang.Integer ADC_CODE, java.lang.Integer CAL_AD_CMPNY)"
 *             query="SELECT OBJECT(cal) FROM AdApprovalDocument adc, IN(adc.adAmountLimits) cal WHERE cal.calDept=?1 AND cal.calAmountLimit=?2 AND adc.adcCode=?3 AND cal.calAdCompany = ?4"
 *
 * @ejb:finder signature="LocalAdAmountLimit findByCalAmountLimitAndAclCode(double CAL_AMNT_LMT, java.lang.Integer ACL_CODE, java.lang.Integer CAL_AD_CMPNY)"
 *             query="SELECT OBJECT(cal) FROM AdApprovalCoaLine acl, IN(acl.adAmountLimits) cal WHERE cal.calAmountLimit=?1 AND acl.aclCode=?2 AND cal.calAdCompany = ?3"
 *             
 * @ejb:finder signature="LocalAdAmountLimit findByCalDeptAdcTypeAndAuTypeAndUsrName(java.lang.String CAL_DEPT, java.lang.String ADC_TYP, java.lang.String AU_TYP, java.lang.String USR_NM, java.lang.Integer CAL_AD_CMPNY)"
 *             query="SELECT OBJECT(cal) FROM AdAmountLimit cal, IN(cal.adApprovalUsers) au WHERE cal.calDept=?1 AND cal.adApprovalDocument.adcType = ?2 AND au.auType = ?3 AND au.adUser.usrName = ?4 AND cal.calAdCompany = ?5"
 * 
 * 
 * @ejb:finder signature="Collection findByAdcTypeAndGreaterThanCalDeptAndCalAmountLimit(java.lang.String ADC_TYP, java.lang.String CAL_DEPT, double CAL_AMNT_LMT, java.lang.Integer CAL_AD_CMPNY)"
 *             query="SELECT OBJECT(cal) FROM AdAmountLimit cal WHERE cal.adApprovalDocument.adcType = ?1 AND cal.calDept=?2 AND cal.calAmountLimit > ?3 AND cal.calAdCompany = ?4"
 *
 * @jboss:query signature="Collection findByAdcTypeAndGreaterThanCalDeptAndCalAmountLimit(java.lang.String ADC_TYP, java.lang.String CAL_DEPT, double CAL_AMNT_LMT, java.lang.Integer CAL_AD_CMPNY)"
 *             query="SELECT OBJECT(cal) FROM AdAmountLimit cal WHERE cal.adApprovalDocument.adcType = ?1 AND cal.calDept=?2 AND cal.calAmountLimit > ?3 AND cal.calAdCompany = ?4 ORDER BY cal.calAmountLimit"
 *
 * @ejb:value-object match="*"
 *             name="AdAmountLimit"
 *
 * @jboss:persistence table-name="AD_AMNT_LMT"
 *
 * @jboss:entity-command name="mysql-get-generated-keys"
 */

public abstract class AdAmountLimitBean extends AbstractEntityBean {
    

    // PERSISTENT METHODS
    
    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     * @ejb:pk-field
     *
     * @jboss:column-name name="CAL_CODE"
     *
     * @jboss:persistence auto-increment="true"
     **/
    public abstract Integer getCalCode();
    public abstract void setCalCode(Integer CAL_CODE);
    
    
    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="CAL_DEPT"
     **/
    public abstract String getCalDept();
     /**
      * @ejb:interface-method view-type="local"
      **/  
    public abstract void setCalDept(String CAL_DEPT);
    
    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="CAL_AMNT_LMT"
     **/
    public abstract double getCalAmountLimit();
     /**
      * @ejb:interface-method view-type="local"
      **/
    public abstract void setCalAmountLimit(double CAL_AMNT_LMT);
    
    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="CAL_AND_OR"
     **/
    public abstract String getCalAndOr();
     /**
      * @ejb:interface-method view-type="local"
      **/
    public abstract void setCalAndOr(String CAL_AND_OR);
    
    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="CAL_AD_CMPNY"
     **/
    public abstract Integer getCalAdCompany();
     /**
      * @ejb:interface-method view-type="local"
      **/
    public abstract void setCalAdCompany(Integer CAL_AD_CMPNY);
    
     
    // RELATIONSHIP METHODS
   
   /**
     * @ejb:interface-method view-type="local"
     * @ejb:relation name="approvaldocument-amountlimits"
     *               role-name="amountlimit-has-one-approvaldocument"
     * 
     * @jboss:relation related-pk-field="adcCode"
     *                 fk-column="AD_APPROVAL_DOCUMENT"
     */
     public abstract LocalAdApprovalDocument getAdApprovalDocument();
     public abstract void setAdApprovalDocument(LocalAdApprovalDocument adApprovalDocument);

   /**
     * @ejb:interface-method view-type="local"
     * @ejb:relation name="approvalcoaline-amountlimits"
     *               role-name="amountlimit-has-one-approvalcoaline"
     *               cascade-delete="yes"
     * 
     * @jboss:relation related-pk-field="aclCode"
     *                 fk-column="AD_APPROVAL_COA_LINE"
     */
     public abstract LocalAdApprovalCoaLine getAdApprovalCoaLine();
     public abstract void setAdApprovalCoaLine(LocalAdApprovalCoaLine adApprovalCoaLine);
   
      
   
     /**
     * @ejb:interface-method view-type="local"
     * @ejb:relation name="amountlimit-approvalusers"
     *               role-name="amountlimit-has-many-approvalusers"
     * 
     */
     public abstract Collection getAdApprovalUsers();
     public abstract void setAdApprovalUsers(Collection adApprovalUsers);

 
    // BUSINESS METHODS
    
        
     /**
     * @ejb:interface-method view-type="local"
     **/
     public void addAdApprovalUser(LocalAdApprovalUser adApprovalUser) {

         Debug.print("AdAmountLimitBean addAdApprovalUser");
      
         try {
      	
            Collection adApprovalUsers = getAdApprovalUsers();
	        adApprovalUsers.add(adApprovalUser);
	         
         } catch (Exception ex) {
      	
           throw new EJBException(ex.getMessage());
      
         }   
     }

    /**
     * @ejb:interface-method view-type="local"
     **/
     public void dropAdApprovalUser(LocalAdApprovalUser adApprovalUser) {

         Debug.print("AdAmountLimitBean dropAdApprovalUser");
      
         try {
      	
            Collection adApprovalUsers = getAdApprovalUsers();
	        adApprovalUsers.remove(adApprovalUser);
	      
         } catch (Exception ex) {
      	
           throw new EJBException(ex.getMessage());
         
         }   
     }

     
    // ENTITY METHODS
    
    /**
     * @ejb:create-method view-type="local"
     **/
     public Integer ejbCreate(Integer CAL_CODE, 
         String CAL_DEPT, double CAL_AMNT_LMT, String CAL_AND_OR, Integer CAL_AD_CMPNY)
         throws CreateException {
      	
         Debug.print("AdAmountLimitBean ejbCreate");
      
         setCalCode(CAL_CODE);
         setCalDept(CAL_DEPT);
         setCalAmountLimit(CAL_AMNT_LMT);
         setCalAndOr(CAL_AND_OR);
         setCalAdCompany(CAL_AD_CMPNY);
        
         return null;
     } 
    
    /**
     * @ejb:create-method view-type="local"
     **/
     public Integer ejbCreate(
		 String CAL_DEPT, double CAL_AMNT_LMT, String CAL_AND_OR, Integer CAL_AD_CMPNY)
         throws CreateException {
      	
         Debug.print("AdAmountLimitBean ejbCreate");
              
         setCalDept(CAL_DEPT);
         setCalAmountLimit(CAL_AMNT_LMT);
         setCalAndOr(CAL_AND_OR);  
         setCalAdCompany(CAL_AD_CMPNY);
        
         return null;
     }
    
     public void ejbPostCreate(Integer CAL_CODE,
         String CAL_DEPT, double CAL_AMNT_LMT, String CAL_AND_OR, Integer CAL_AD_CMPNY)      
         throws CreateException { }
      
     public void ejbPostCreate(
         String CAL_DEPT, double CAL_AMNT_LMT, String CAL_AND_OR, Integer CAL_AD_CMPNY)
         throws CreateException { }     
}