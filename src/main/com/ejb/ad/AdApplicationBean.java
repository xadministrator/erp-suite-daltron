package com.ejb.ad;

import java.util.Collection;

import javax.ejb.CreateException;
import javax.ejb.EJBException;

import com.ejb.gl.LocalAdDocumentCategory;
import com.ejb.gl.LocalAdDocumentSequence;
import com.util.AbstractEntityBean;
import com.util.Debug;

/**
 * @ejb:bean name="AdApplicationEJB"
 *           display-name="Application Entity"
 *           type="CMP"
 *           cmp-version="2.x"
 *           view-type="local"
 *           local-jndi-name="omega-ejb/AdApplicationEJB"
 *           schema="AdApplication"
 *           primkey-field="appCode"
 *       
 * @ejb:pk class="java.lang.Integer"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="gluser"
 *                        role-link="gluserlink"
 *
 * @ejb:permission role-name="gluser"
 *
 * @ejb:interface local-class="com.ejb.gl.LocalAdApplication"
 *                local-extends="javax.ejb.EJBLocalObject"
 *
 * @ejb:home local-class="com.ejb.gl.LocalAdApplicationHome"
 *                local-extends="javax.ejb.EJBLocalHome"
 *
 * @ejb:finder signature="Collection findAppAll(java.lang.Integer APP_AD_CMPNY)"
 *             query="SELECT OBJECT(app) FROM AdApplication app WHERE app.appAdCompany = ?1"
 *
 * @ejb:finder signature="LocalAdApplication findByAppName(java.lang.String APP_NM, java.lang.Integer APP_AD_CMPNY)"
 *             query="SELECT OBJECT(app) FROM AdApplication app WHERE app.appName=?1 AND app.appAdCompany = ?2"
 *
 * @jboss:persistence table-name="AD_APPLCTN"
 *
 * @jboss:entity-command name="mysql-get-generated-keys"
 *
 */

public abstract class AdApplicationBean extends AbstractEntityBean {


   // Access methods for persistent fields

   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    * @ejb:pk-field
    *
    * @jboss:column-name name="APP_CODE"
    *
    * @jboss:persistence auto-increment="true"
    **/
   public abstract java.lang.Integer getAppCode();
   public abstract void setAppCode(java.lang.Integer APP_CODE);

   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="APP_NM"
    **/
   public abstract java.lang.String getAppName();
   /**
    * @ejb:interface-method view-type="local"
    **/   
   public abstract void setAppName(java.lang.String APP_NM);

   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="APP_DESC"
    **/
   public abstract java.lang.String getAppDescription();
   /**
    * @ejb:interface-method view-type="local"
    **/   
   public abstract void setAppDescription(java.lang.String APP_DESC);
   
   
   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="APP_INSTLLD"
    **/
   public abstract byte getAppInstalled();
   /**
    * @ejb:interface-method view-type="local"
    **/   
   public abstract void setAppInstalled(byte APP_INSTLLD);
   
   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="APP_AD_CMPNY"
    **/
   public abstract Integer getAppAdCompany();
   /**
    * @ejb:interface-method view-type="local"
    **/   
   public abstract void setAppAdCompany(Integer APP_AD_CMPNY);

   // Access methods for relationship fields

   /**
    * @ejb:interface-method view-type="local"
    * @ejb:relation name="application-documentcategories"
    *               role-name="application-has-many-documentcategories"
    */
   public abstract Collection getAdDocumentCategories();
   public abstract void setAdDocumentCategories(Collection adDocumentCategories);

   /**
    * @ejb:interface-method view-type="local"
    * @ejb:relation name="application-documentsequences"
    *               role-name="application-has-many-documentsequences"
    */
   public abstract Collection getAdDocumentSequences();
   public abstract void setAdDocumentSequences(Collection adDocumentSequences);
   
    /**
     * @ejb:interface-method view-type="local"
     * @ejb:relation name="application-adresponsibilities"
     *               role-name="application-has-many-adresponsibilities"
     * 
     */
   public abstract Collection getAdResponsibilities();
   public abstract void setAdResponsibilities(Collection adResponsibilities);   

   // Business methods

   /**
    * @ejb:interface-method view-type="local"
    **/
   public void addAdDocumentCategory(LocalAdDocumentCategory adDocumentCategory) {

      Debug.print("AdApplicationBean addAdDocumentCategory");
      try {
         Collection adDocumentCategories = getAdDocumentCategories();
	 adDocumentCategories.add(adDocumentCategory);
      } catch (Exception ex) {
         throw new EJBException(ex.getMessage());
      }
   }

   /**
    * @ejb:interface-method view-type="local"
    **/
   public void dropAdDocumentCategory(LocalAdDocumentCategory adDocumentCategory) {

      Debug.print("AdApplicationBean dropAdDocumentCategory");
      try {
         Collection adDocumentCategories = getAdDocumentCategories();
	 adDocumentCategories.remove(adDocumentCategory);
      } catch (Exception ex) {
         throw new EJBException(ex.getMessage());
      }
   }

   /**
    * @ejb:interface-method view-type="local"
    **/
   public void addAdDocumentSequence(LocalAdDocumentSequence adDocumentSequence) {

      Debug.print("AdApplicationBean addAdDocumentSequence");
      try {
         Collection adDocumentSequences = getAdDocumentSequences();
	 adDocumentSequences.add(adDocumentSequence);
      } catch (Exception ex) {
         throw new EJBException(ex.getMessage());
      }
   }

   /**
    * @ejb:interface-method view-type="local"
    **/
   public void dropAdDocumentSequence(LocalAdDocumentSequence adDocumentSequence) {

      Debug.print("AdApplicationBean dropAdDocumentSequence");
      try {
         Collection adDocumentSequences = getAdDocumentSequences();
	 adDocumentSequences.remove(adDocumentSequence);
      } catch (Exception ex) {
         throw new EJBException(ex.getMessage());
      }
   }
   
    /**
     * @ejb:interface-method view-type="local"
     **/
     public void addAdResponsibility(com.ejb.ad.LocalAdResponsibility adResponsibility) {

         Debug.print("AdApplicationBean addAdResponsibility");
      
         try {
      	
            Collection adResponsibilities = getAdResponsibilities();
	        adResponsibilities.add(adResponsibility);
	         
         } catch (Exception ex) {
      	
           throw new EJBException(ex.getMessage());
      
         }   
     }

    /**
     * @ejb:interface-method view-type="local"
     **/
     public void dropAdResponsibility(com.ejb.ad.LocalAdResponsibility adResponsibility) {

         Debug.print("AdApplicationBean dropAdResponsibility");
      
         try {
      	
            Collection adResponsibilities = getAdResponsibilities();
	        adResponsibilities.remove(adResponsibility);
	      
         } catch (Exception ex) {
      	
           throw new EJBException(ex.getMessage());
         
         }   
     }   

   // EntityBean methods 

   /**
    * @ejb:create-method view-type="local"
    **/
   public java.lang.Integer ejbCreate (java.lang.Integer APP_CODE, java.lang.String APP_NM, 
      java.lang.String APP_DESC, byte APP_INSTLLD, Integer APP_AD_CMPNY)
      throws CreateException {

      Debug.print("AdApplication ejbCreate");
      setAppCode(APP_CODE);
      setAppName(APP_NM);
      setAppDescription(APP_DESC);
      setAppInstalled(APP_INSTLLD);
      setAppAdCompany(APP_AD_CMPNY);
      
      return null;
   }
   
   /**
    * @ejb:create-method view-type="local"
    **/
   public java.lang.Integer ejbCreate (java.lang.String APP_NM, 
      java.lang.String APP_DESC, byte APP_INSTLLD, Integer APP_AD_CMPNY)
      throws CreateException {

      Debug.print("AdApplication ejbCreate");
      
      setAppName(APP_NM);
      setAppDescription(APP_DESC);
      setAppInstalled(APP_INSTLLD);
      setAppAdCompany(APP_AD_CMPNY);
      
      return null;
   }
   
   public void ejbPostCreate (java.lang.Integer APP_CODE, java.lang.String APP_NM,
      java.lang.String APP_DESC, byte APP_INSTLLD, Integer APP_AD_CMPNY)
      throws CreateException { }
   
   public void ejbPostCreate (java.lang.String APP_NM,
        java.lang.String APP_DESC, byte APP_INSTLLD, Integer APP_AD_CMPNY)
        throws CreateException { }

}     
