/*
 * com/ejb/ap/LocalAdApprovalBean.java
 *
 * Created on March 19, 2004, 2:56 PM
 */

package com.ejb.ad;

import javax.ejb.CreateException;

import com.util.AbstractEntityBean;
import com.util.Debug;

/**
 *
 * @author  Neil Andrew M. Ajero
 *
 * @ejb:bean name="AdApprovalEJB"
 *           display-name="Approval Entity"
 *           type="CMP"
 *           cmp-version="2.x"
 *           view-type="local"
 *           local-jndi-name="omega-ejb/AdApprovalEJB"
 *           schema="AdApproval"
 *           primkey-field="aprCode"
 *
 * @ejb:pk class="java.lang.Integer"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="aduser"
 *                        role-link="aduserlink"
 *
 * @ejb:permission role-name="aduser"
 *
 * @ejb:interface local-class="com.ejb.ad.LocalAdApproval"
 *                local-extends="javax.ejb.EJBLocalObject"
 *
 * @ejb:home local-class="com.ejb.ad.LocalAdApprovalHome"
 *           local-extends="javax.ejb.EJBLocalHome"
 * 
 * @ejb:finder signature="LocalAdApproval findByAprAdCompany(java.lang.Integer APR_AD_CMPNY)"
 *             query="SELECT OBJECT(apr) FROM AdApproval apr WHERE apr.aprAdCompany = ?1"
 *
 * @ejb:value-object match="*"
 *             name="AdApproval"
 *
 * @jboss:persistence table-name="AD_APPRVL"
 *
 * @jboss:entity-command name="mysql-get-generated-keys"
 */

public abstract class AdApprovalBean extends AbstractEntityBean {
    
    
    // PERSITCENT METHODS
    
    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     * @ejb:pk-field
     *
     * @jboss:column-name name="APR_CODE"
     *
     * @jboss:persistence auto-increment="true"
     **/
    public abstract Integer getAprCode();
    public abstract void setAprCode(Integer APR_CODE);
    
    
    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"   
     *
     * @jboss:column-name name="APR_ENBL_GL_JRNL"
     **/
    public abstract byte getAprEnableGlJournal();
     /**
      * @ejb:interface-method view-type="local"
      **/ 
    public abstract void setAprEnableGlJournal(byte APR_ENBL_GL_JRNL);
    

    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"   
     *
     * @jboss:column-name name="APR_ENBL_AP_VCHR"
     **/
    public abstract byte getAprEnableApVoucher();
     /**
      * @ejb:interface-method view-type="local"
      **/ 
    public abstract void setAprEnableApVoucher(byte APR_ENBL_AP_VCHR);
    
    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"   
     *
     * @jboss:column-name name="APR_ENBL_AP_VCHR_DEPT"
     **/
    public abstract byte getAprEnableApVoucherDepartment();
     /**
      * @ejb:interface-method view-type="local"
      **/ 
    public abstract void setAprEnableApVoucherDepartment(byte APR_ENBL_AP_VCHR_DEPT);
    
    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"   
     *
     * @jboss:column-name name="APR_ENBL_AP_DBT_MMO"
     **/
    public abstract byte getAprEnableApDebitMemo();
     /**
      * @ejb:interface-method view-type="local"
      **/ 
    public abstract void setAprEnableApDebitMemo(byte APR_ENBL_AP_DBT_MMO);
    
    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"   
     *
     * @jboss:column-name name="APR_ENBL_AP_CHCK"
     **/
    public abstract byte getAprEnableApCheck();
     /**
      * @ejb:interface-method view-type="local"
      **/ 
    public abstract void setAprEnableApCheck(byte APR_ENBL_AP_CHCK);
    
    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"   
     *
     * @jboss:column-name name="APR_ENBL_AR_INVC"
     **/
    public abstract byte getAprEnableArInvoice();
     /**
      * @ejb:interface-method view-type="local"
      **/ 
    public abstract void setAprEnableArInvoice(byte APR_ENBL_AR_INVC);    
    
    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"   
     *
     * @jboss:column-name name="APR_ENBL_AR_CRDT_MMO"
     **/
    public abstract byte getAprEnableArCreditMemo();
     /**
      * @ejb:interface-method view-type="local"
      **/ 
    public abstract void setAprEnableArCreditMemo(byte APR_ENBL_AR_CRDT_MMO);
    
    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"   
     *
     * @jboss:column-name name="APR_ENBL_AR_RCPT"
     **/
    public abstract byte getAprEnableArReceipt();
     /**
      * @ejb:interface-method view-type="local"
      **/ 
    public abstract void setAprEnableArReceipt(byte APR_ENBL_AR_RCPT);
    
    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"   
     *
     * @jboss:column-name name="APR_ENBL_CM_FND_TRNSFR"
     **/
    public abstract byte getAprEnableCmFundTransfer();
     /**
      * @ejb:interface-method view-type="local"
      **/ 
    public abstract void setAprEnableCmFundTransfer(byte APR_ENBL_CM_FND_TRNSFR);
    
    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"   
     *
     * @jboss:column-name name="APR_ENBL_CM_ADJSTMNT"
     **/
    public abstract byte getAprEnableCmAdjustment();
     /**
      * @ejb:interface-method view-type="local"
      **/ 
    public abstract void setAprEnableCmAdjustment(byte APR_ENBL_CM_ADJSTMNT);
    
    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"   
     *
     * @jboss:column-name name="APR_APPRVL_QUEUE_EXPRTN"
     **/
    public abstract int getAprApprovalQueueExpiration();
     /**
      * @ejb:interface-method view-type="local"
      **/ 
    public abstract void setAprApprovalQueueExpiration(int APR_APPRVL_QUEUE_EXPRTN);
    
    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"   
     *
     * @jboss:column-name name="APR_ENBL_INV_ADJSTMNT"
     **/
    public abstract byte getAprEnableInvAdjustment();
     /**
      * @ejb:interface-method view-type="local"
      **/ 
    public abstract void setAprEnableInvAdjustment(byte APR_ENBL_INV_ADJSTMNT);
    

    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"   
     *
     * @jboss:column-name name="APR_ENBL_INV_BLD"
     **/
    public abstract byte getAprEnableInvBuild();
     /**
      * @ejb:interface-method view-type="local"
      **/ 
    public abstract void setAprEnableInvBuild(byte APR_ENBL_INV_BLD);
    
    
    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"   
     *
     * @jboss:column-name name="APR_ENBL_INV_BLD_ORDR"
     **/
    public abstract byte getAprEnableInvBuildOrder();
     /**
      * @ejb:interface-method view-type="local"
      **/ 
    public abstract void setAprEnableInvBuildOrder(byte APR_ENBL_INV_BLD_ORDR);
    
  
    
    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"   
     *
     * @jboss:column-name name="APR_ENBL_AP_PRCHS_RQSTN "
     **/
    public abstract byte getAprEnableApPurReq();
     /**
      * @ejb:interface-method view-type="local"
      **/ 
    public abstract void setAprEnableApPurReq(byte APR_ENBL_AP_PRCHS_RQSTN );
    
    
    
    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"   
     *
     * @jboss:column-name name="APR_ENBL_AP_CNVSS"
     **/
    public abstract byte getAprEnableApCanvass();
     /**
      * @ejb:interface-method view-type="local"
      **/ 
    public abstract void setAprEnableApCanvass(byte APR_ENBL_AP_CNVSS );
    
    
    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"   
     *
     * @jboss:column-name name="APR_ENBL_INV_ADJSTMNT_RQST"
     **/
    public abstract byte getAprEnableInvAdjustmentRequest();
     /**
      * @ejb:interface-method view-type="local"
      **/ 
    public abstract void setAprEnableInvAdjustmentRequest(byte APR_ENBL_INV_ADJSTMNT_RQST );

    
    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"   
     *
     * @jboss:column-name name="APR_ENBL_INV_STCK_TRNSFR"
     **/
    public abstract byte getAprEnableInvStockTransfer();
     /**
      * @ejb:interface-method view-type="local"
      **/ 
    public abstract void setAprEnableInvStockTransfer(byte APR_ENBL_INV_STCK_TRNSFR); 
    
    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"   
     *
     * @jboss:column-name name="APR_ENBL_AP_PRCHS_ORDR"
     **/
    public abstract byte getAprEnableApPurchaseOrder();
     /**
      * @ejb:interface-method view-type="local"
      **/ 
    public abstract void setAprEnableApPurchaseOrder(byte APR_ENBL_PRCHS_ORDR);  
    
    
    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"   
     *
     * @jboss:column-name name="APR_ENBL_AP_RCVNG_ITM"
     **/
    public abstract byte getAprEnableApReceivingItem();
     /**
      * @ejb:interface-method view-type="local"
      **/ 
    public abstract void setAprEnableApReceivingItem(byte APR_ENBL_AP_RCVNG_ITM);
    

    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"   
     *
     * @jboss:column-name name="APR_ENBL_INV_BRNCH_STCK_TRNSFR"
     **/
    public abstract byte getAprEnableInvBranchStockTransfer();
     /**
      * @ejb:interface-method view-type="local"
      **/ 
    public abstract void setAprEnableInvBranchStockTransfer(byte APR_ENBL_INV_BRNCH_STCK_TRNSFR); 
    
    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"   
     *
     * @jboss:column-name name="APR_ENBL_INV_BRNCH_STCK_TRNSFR_ORDR"
     **/
    public abstract byte getAprEnableInvBranchStockTransferOrder();
     /**
      * @ejb:interface-method view-type="local"
      **/ 
    public abstract void setAprEnableInvBranchStockTransferOrder(byte APR_ENBL_INV_BRNCH_STCK_TRNSFR_ORDR); 
    
    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"   
     *
     * @jboss:column-name name="APR_ENBL_AP_CHCK_PYMNT_RQST"
     **/
    public abstract byte getAprEnableApCheckPaymentRequest();
     /**
      * @ejb:interface-method view-type="local"
      **/ 
    public abstract void setAprEnableApCheckPaymentRequest(byte APR_ENBL_AP_CHCK_PYMNT_RQST);
    
    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"   
     *
     * @jboss:column-name name="APR_ENBL_AP_CHCK_PYMNT_RQST_DEPT"
     **/
    public abstract byte getAprEnableApCheckPaymentRequestDepartment();
     /**
      * @ejb:interface-method view-type="local"
      **/ 
    public abstract void setAprEnableApCheckPaymentRequestDepartment(byte APR_ENBL_AP_CHCK_PYMNT_RQST_DEPT);
    
    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"   
     *
     * @jboss:column-name name="APR_ENBL_AR_SLS_ORDR"
     **/
    public abstract byte getAprEnableArSalesOrder();
     /**
      * @ejb:interface-method view-type="local"
      **/ 
    public abstract void setAprEnableArSalesOrder(byte APR_ENBL_AR_SLS_ORDR);
    
    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"   
     *
     * @jboss:column-name name="APR_ENBL_AR_CSTMR"
     **/
    public abstract byte getAprEnableArCustomer();
     /**
      * @ejb:interface-method view-type="local"
      **/ 
    public abstract void setAprEnableArCustomer(byte APR_ENBL_AR_CSTMR);
    
    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"   
     *
     * @jboss:column-name name="APR_AD_CMPNY"
     **/
    public abstract Integer getAprAdCompany();
     /**
      * @ejb:interface-method view-type="local"
      **/ 
    public abstract void setAprAdCompany(Integer APR_AD_CMPNY);
    
    
   
    // NO RELATIONSHIP METHODS


    // NO BUSINESS METHODS
     

    // ENTITY METHODS
    
    /**
     * @ejb:create-method view-type="local"
     **/
    public Integer ejbCreate(Integer APR_CODE, 
         byte APR_ENBL_GL_JRNL, byte APR_ENBL_AP_VCHR, byte APR_ENBL_AP_VCHR_DEPT,
         byte APR_ENBL_AP_DBT_MMO, byte APR_ENBL_AP_CHCK, 
         byte APR_ENBL_AR_INVC, byte APR_ENBL_AR_CRDT_MMO, byte APR_ENBL_CHCK_PYMNT_RQST, byte APR_ENBL_AP_CHCK_PYMNT_RQST_DEPT,
         byte APR_ENBL_AR_RCPT, byte APR_ENBL_CM_FND_TRNSFR, 
         byte APR_ENBL_CM_ADJSTMNT, int APR_APPRVL_QUEUE_EXPRTN,
		 byte APR_ENBL_INV_ADJSTMNT, byte APR_ENBL_INV_BLD, byte APR_ENBL_INV_BLD_ORDR,
		 byte APR_ENBL_AP_PRCHS_RQSTN, byte APR_ENBL_AP_CNVSS,byte APR_ENBL_INV_ADJSTMNT_RQST,
		 byte APR_ENBL_INV_STCK_TRNSFR, byte APR_ENBL_AP_PRCHS_ORDR, byte APR_ENBL_AP_RCVNG_ITM,
		 byte APR_ENBL_INV_BRNCH_STCK_TRNSFR, byte APR_ENBL_INV_BRNCH_STCK_TRNSFR_ORDR, byte APR_ENBL_AR_SLS_ORDR, byte APR_ENBL_AR_CSTMR,
		 Integer APR_AD_CMPNY)     	
         throws CreateException {	
     	
         Debug.print("ApGlobalPreferenceBean ejbCreate");
     
	     setAprCode(APR_CODE);
	     setAprEnableGlJournal(APR_ENBL_GL_JRNL);
         setAprEnableApVoucher(APR_ENBL_AP_VCHR);
         setAprEnableApVoucherDepartment(APR_ENBL_AP_VCHR_DEPT);
         setAprEnableApDebitMemo(APR_ENBL_AP_DBT_MMO);
         setAprEnableApCheckPaymentRequest(APR_ENBL_CHCK_PYMNT_RQST);
         setAprEnableApCheckPaymentRequestDepartment(APR_ENBL_AP_CHCK_PYMNT_RQST_DEPT);
         setAprEnableApCheck(APR_ENBL_AP_CHCK);
         setAprEnableArInvoice(APR_ENBL_AR_INVC);
         setAprEnableArCreditMemo(APR_ENBL_AR_CRDT_MMO);
         setAprEnableArReceipt(APR_ENBL_AR_RCPT);
         setAprEnableCmFundTransfer(APR_ENBL_CM_FND_TRNSFR);
         setAprEnableCmAdjustment(APR_ENBL_CM_ADJSTMNT);
         setAprApprovalQueueExpiration(APR_APPRVL_QUEUE_EXPRTN);
         setAprEnableInvAdjustment(APR_ENBL_INV_ADJSTMNT);
         setAprEnableInvBuild(APR_ENBL_INV_BLD);
         setAprEnableInvBuildOrder(APR_ENBL_INV_BLD_ORDR);
         setAprEnableApPurReq(APR_ENBL_AP_PRCHS_RQSTN);
         setAprEnableApCanvass(APR_ENBL_AP_CNVSS);
         setAprEnableInvAdjustmentRequest(APR_ENBL_INV_ADJSTMNT_RQST);
         
         setAprEnableInvStockTransfer(APR_ENBL_INV_STCK_TRNSFR);
         setAprEnableApPurchaseOrder(APR_ENBL_AP_PRCHS_ORDR);
         setAprEnableApReceivingItem(APR_ENBL_AP_RCVNG_ITM);
         setAprEnableInvBranchStockTransfer(APR_ENBL_INV_BRNCH_STCK_TRNSFR);
         setAprEnableInvBranchStockTransferOrder(APR_ENBL_INV_BRNCH_STCK_TRNSFR_ORDR);
         setAprEnableArSalesOrder(APR_ENBL_AR_SLS_ORDR);
         setAprEnableArCustomer(APR_ENBL_AR_CSTMR);
         setAprAdCompany(APR_AD_CMPNY);

         return null;
     }
     
    /**
     * @ejb:create-method view-type="local"
     **/
    public Integer ejbCreate(
         byte APR_ENBL_GL_JRNL, byte APR_ENBL_AP_VCHR, byte APR_ENBL_AP_VCHR_DEPT,
         byte APR_ENBL_AP_DBT_MMO, byte APR_ENBL_AP_CHCK, 
         byte APR_ENBL_AR_INVC, byte APR_ENBL_AR_CRDT_MMO, byte APR_ENBL_CHCK_PYMNT_RQST, byte APR_ENBL_AP_CHCK_PYMNT_RQST_DEPT,
         byte APR_ENBL_AR_RCPT, byte APR_ENBL_CM_FND_TRNSFR, 
         byte APR_ENBL_CM_ADJSTMNT, int APR_APPRVL_QUEUE_EXPRTN,
		 byte APR_ENBL_INV_ADJSTMNT, byte APR_ENBL_INV_BLD, byte APR_ENBL_INV_BLD_ORDR,
		 byte APR_ENBL_AP_PRCHS_RQSTN,byte APR_ENBL_AP_CNVSS,byte APR_ENBL_INV_ADJSTMNT_RQST,
		 byte APR_ENBL_INV_STCK_TRNSFR, byte APR_ENBL_AP_PRCHS_ORDR, byte APR_ENBL_AP_RCVNG_ITM,
		 byte APR_ENBL_INV_BRNCH_STCK_TRNSFR, byte APR_ENBL_INV_BRNCH_STCK_TRNSFR_ORDR, byte APR_ENBL_AR_SLS_ORDR, byte APR_ENBL_AR_CSTMR,
		 Integer APR_AD_CMPNY)     	
         throws CreateException {	
     	
         Debug.print("ApGlobalPreferenceBean ejbCreate");
     
	     setAprEnableGlJournal(APR_ENBL_GL_JRNL);
         setAprEnableApVoucher(APR_ENBL_AP_VCHR);
         setAprEnableApVoucherDepartment(APR_ENBL_AP_VCHR_DEPT);
         setAprEnableApDebitMemo(APR_ENBL_AP_DBT_MMO);
         setAprEnableApCheck(APR_ENBL_AP_CHCK);
         setAprEnableArInvoice(APR_ENBL_AR_INVC);
         setAprEnableArCreditMemo(APR_ENBL_AR_CRDT_MMO);
         setAprEnableArReceipt(APR_ENBL_AR_RCPT);
         setAprEnableCmFundTransfer(APR_ENBL_CM_FND_TRNSFR);
         setAprEnableCmAdjustment(APR_ENBL_CM_ADJSTMNT);
         setAprApprovalQueueExpiration(APR_APPRVL_QUEUE_EXPRTN);
         setAprEnableInvAdjustment(APR_ENBL_INV_ADJSTMNT);
         setAprEnableInvBuild(APR_ENBL_INV_BLD);
         setAprEnableInvBuildOrder(APR_ENBL_INV_BLD_ORDR);
         setAprEnableApCheckPaymentRequest(APR_ENBL_CHCK_PYMNT_RQST);
         setAprEnableApCheckPaymentRequestDepartment(APR_ENBL_AP_CHCK_PYMNT_RQST_DEPT);
         
         setAprEnableApPurReq(APR_ENBL_AP_PRCHS_RQSTN);
         setAprEnableApCanvass(APR_ENBL_AP_CNVSS);
         setAprEnableInvAdjustmentRequest(APR_ENBL_INV_ADJSTMNT_RQST);
         
         setAprEnableInvStockTransfer(APR_ENBL_INV_STCK_TRNSFR);
         setAprEnableApPurchaseOrder(APR_ENBL_AP_PRCHS_ORDR);
         setAprEnableApReceivingItem(APR_ENBL_AP_RCVNG_ITM);
         setAprEnableInvBranchStockTransfer(APR_ENBL_INV_BRNCH_STCK_TRNSFR);
         setAprEnableInvBranchStockTransferOrder(APR_ENBL_INV_BRNCH_STCK_TRNSFR_ORDR);
         setAprEnableArSalesOrder(APR_ENBL_AR_SLS_ORDR);
         setAprEnableArCustomer(APR_ENBL_AR_CSTMR);
         setAprAdCompany(APR_AD_CMPNY);

         return null;
     }
     
     public void ejbPostCreate(Integer APR_CODE, 
         byte APR_ENBL_GL_JRNL, byte APR_ENBL_AP_VCHR, byte APR_ENBL_AP_VCHR_DEPT,
         byte APR_ENBL_AP_DBT_MMO, byte APR_ENBL_AP_CHCK, 
         byte APR_ENBL_AR_INVC, byte APR_ENBL_AR_CRDT_MMO, byte APR_ENBL_CHCK_PYMNT_RQST, byte APR_ENBL_AP_CHCK_PYMNT_RQST_DEPT,
         byte APR_ENBL_AR_RCPT, byte APR_ENBL_CM_FND_TRNSFR, 
         byte APR_ENBL_CM_ADJSTMNT, int APR_APPRVL_QUEUE_EXPRTN,
		 byte APR_ENBL_INV_ADJSTMNT, byte APR_ENBL_INV_BLD, byte APR_ENBL_INV_BLD_ORDR,
		 byte APR_ENBL_AP_PRCHS_RQSTN, 
		 byte APR_ENBL_AP_CNVSS,byte APR_ENBL_INV_ADJSTMNT_RQST,
		 byte APR_ENBL_INV_STCK_TRNSFR, byte APR_ENBL_AP_PRCHS_ORDR, byte APR_ENBL_AP_RCVNG_ITM,
		 byte APR_ENBL_INV_BRNCH_STCK_TRNSFR, byte APR_ENBL_INV_BRNCH_STCK_TRNSFR_ORDR, byte APR_ENBL_AR_SLS_ORDR, byte APR_ENBL_AR_CSTMR,
		 Integer APR_AD_CMPNY)
         throws CreateException { }
      
     public void ejbPostCreate( 
         byte APR_ENBL_GL_JRNL, byte APR_ENBL_AP_VCHR, byte APR_ENBL_AP_VCHR_DEPT,
         byte APR_ENBL_AP_DBT_MMO, byte APR_ENBL_AP_CHCK, 
         byte APR_ENBL_AR_INVC, byte APR_ENBL_AR_CRDT_MMO, byte APR_ENBL_CHCK_PYMNT_RQST, byte APR_ENBL_AP_CHCK_PYMNT_RQST_DEPT,
         byte APR_ENBL_AR_RCPT, byte APR_ENBL_CM_FND_TRNSFR, 
         byte APR_ENBL_CM_ADJSTMNT, int APR_APPRVL_QUEUE_EXPRTN,
		 byte APR_ENBL_INV_ADJSTMNT, byte APR_ENBL_INV_BLD, byte APR_ENBL_INV_BLD_ORDR,
		 byte APR_ENBL_AP_PRCHS_RQSTN,byte APR_ENBL_AP_CNVSS,byte APR_ENBL_INV_ADJSTMNT_RQST,
		 byte APR_ENBL_INV_STCK_TRNSFR, byte APR_ENBL_AP_PRCHS_ORDR, byte APR_ENBL_AP_RCVNG_ITM,
		 byte APR_ENBL_INV_BRNCH_STCK_TRNSFR, byte APR_ENBL_INV_BRNCH_STCK_TRNSFR_ORDR, byte APR_ENBL_AR_SLS_ORDR, byte APR_ENBL_AR_CSTMR,
		 Integer APR_AD_CMPNY)
         throws CreateException { }     
}