/*
 * com/ejb/ad/LocalAdApprovalCoaLineBean.java
 *
 * Created on March 19, 2004, 3:23 PM
 */
 
package com.ejb.ad;

import java.util.Collection;

import javax.ejb.CreateException;
import javax.ejb.EJBException;

import com.util.AbstractEntityBean;
import com.util.Debug;

/**
 *
 * @author  Neil Andrew M. Ajero
 *
 * @ejb:bean name="AdApprovalCoaLineEJB"
 *           display-name="Approval Coa Line Entity"
 *           type="CMP"
 *           cmp-version="2.x"
 *           view-type="local"
 *           local-jndi-name="omega-ejb/AdApprovalCoaLineEJB"
 *           schema="AdApprovalCoaLine"
 *           primkey-field="aclCode"
 *
 * @ejb:pk class="java.lang.Integer"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="aduser"
 *                        role-link="aduserlink"
 *
 * @ejb:permission role-name="aduser"
 *
 * @ejb:interface local-class="com.ejb.ad.LocalAdApprovalCoaLine"
 *                local-extends="javax.ejb.EJBLocalObject"
 *
 * @ejb:home local-class="com.ejb.ad.LocalAdApprovalCoaLineHome"
 *           local-extends="javax.ejb.EJBLocalHome"
 *
 * @ejb:finder signature="Collection findAclAll(java.lang.Integer ACL_AD_CMPNY)"
 *             query="SELECT OBJECT(acl) FROM AdApprovalCoaLine acl WHERE acl.aclAdCompany = ?1"
 *
 * @jboss:query signature="Collection findAclAll(java.lang.Integer ACL_AD_CMPNY)"
 *             query="SELECT OBJECT(acl) FROM AdApprovalCoaLine acl WHERE acl.aclAdCompany = ?1 ORDER BY acl.glChartOfAccount.coaAccountNumber"
 *
 * @ejb:finder signature="LocalAdApprovalCoaLine findByAclCodeAndCoaAccountNumber(java.lang.Integer COA_CODE, java.lang.String COA_ACCNT_NMBR, java.lang.Integer ACL_AD_CMPNY)"
 *             query="SELECT OBJECT(acl) FROM AdApprovalCoaLine acl WHERE acl.aclCode<>?1 AND acl.glChartOfAccount.coaAccountNumber=?2 AND acl.aclAdCompany = ?3"
 *
 * @ejb:finder signature="LocalAdApprovalCoaLine findByCoaAccountNumber(java.lang.String COA_ACCNT_NMBR, java.lang.Integer ACL_AD_CMPNY)"
 *             query="SELECT OBJECT(acl) FROM AdApprovalCoaLine acl WHERE acl.glChartOfAccount.coaAccountNumber=?1 AND acl.aclAdCompany = ?2"
 *
 * @ejb:value-object match="*"
 *             name="AdApprovalCoaLine"
 *
 * @jboss:persistence table-name="AD_APPRVL_COA_LN"
 *
 * @jboss:entity-command name="mysql-get-generated-keys"
 */

public abstract class AdApprovalCoaLineBean extends AbstractEntityBean {
    

    // PERSISTENT METHODS
    
    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     * @ejb:pk-field
     *
     * @jboss:column-name name="ACL_CODE"
     *
     * @jboss:persistence auto-increment="true"
     **/
    public abstract Integer getAclCode();
    public abstract void setAclCode(Integer ACL_CODE);

    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="ACL_AD_CMPNY"
     **/
    public abstract Integer getAclAdCompany();
     /**
      * @ejb:interface-method view-type="local"
      **/
    public abstract void setAclAdCompany(Integer ACL_AD_CMPNY);
    
    // RELATIONSHIP METHODS

    /**
     * @ejb:interface-method view-type="local"
     * @ejb:relation name="chartofaccount-approvalcoalines"
     *               role-name="approvalcoaline-has-one-chartofaccount"
     * 
     * @jboss:relation related-pk-field="coaCode"
     *                 fk-column="GL_CHART_OF_ACCOUNT"
     */
    public abstract com.ejb.gl.LocalGlChartOfAccount getGlChartOfAccount();
    public abstract void setGlChartOfAccount(com.ejb.gl.LocalGlChartOfAccount glChartOfAccount);
    
   /**
     * @ejb:interface-method view-type="local"
     * @ejb:relation name="approvalcoaline-amountlimits"
     *               role-name="approvalcoaline-has-many-amountlimits"
     * 
     */
     public abstract Collection getAdAmountLimits();
     public abstract void setAdAmountLimits(Collection adAmountLimits);
        
    
    // BUSINESS METHODS
    
        
     /**
     * @ejb:interface-method view-type="local"
     **/
     public void addAdAmountLimit(LocalAdAmountLimit adAmountLimit) {

         Debug.print("AdApprovalCoaLineBean addAdAmountLimit");
      
         try {
      	
            Collection adAmountLimits = getAdAmountLimits();
	        adAmountLimits.add(adAmountLimit);
	         
         } catch (Exception ex) {
      	
           throw new EJBException(ex.getMessage());
      
         }   
     }

    /**
     * @ejb:interface-method view-type="local"
     **/
     public void dropAdAmountLimit(LocalAdAmountLimit adAmountLimit) {

         Debug.print("AdApprovalCoaLineBean dropAdAmountLimit");
      
         try {
      	
            Collection adAmountLimits = getAdAmountLimits();
	        adAmountLimits.remove(adAmountLimit);
	      
         } catch (Exception ex) {
      	
           throw new EJBException(ex.getMessage());
         
         }   
     }

    // ENTITY METHODS
    
    /**
     * @ejb:create-method view-type="local"
     **/
     public Integer ejbCreate(Integer ACL_CODE, Integer ACL_AD_CMPNY)
         throws CreateException {
      	
         Debug.print("AdApprovalCoaLineBean ejbCreate");
      
         setAclCode(ACL_CODE); 
         setAclAdCompany(ACL_AD_CMPNY);
        
         return null;
     } 
    
    /**
     * @ejb:create-method view-type="local"
     **/
     public Integer ejbCreate(Integer ACL_AD_CMPNY)
         throws CreateException {
      	
         Debug.print("AdApprovalCoaLineBean ejbCreate");

         setAclAdCompany(ACL_AD_CMPNY);
         
         return null;
     }
    
     public void ejbPostCreate(Integer ACL_CODE, Integer ACL_AD_CMPNY)      
         throws CreateException { }
      
     public void ejbPostCreate(Integer ACL_AD_CMPNY)
         throws CreateException { }     
}