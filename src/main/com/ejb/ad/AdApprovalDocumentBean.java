/*
 * com/ejb/ad/LocalAdApprovalDocumentBean.java
 *
 * Created on March 19, 2004, 3:06 PM
 */
 
package com.ejb.ad;

import java.util.Collection;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;

import com.util.AbstractEntityBean;
import com.util.Debug;

/**
 *
 * @author  Neil Andrew M. Ajero
 *
 * @ejb:bean name="AdApprovalDocumentEJB"
 *           display-name="Approval Document Entity"
 *           type="CMP"
 *           cmp-version="2.x"
 *           view-type="local"
 *           local-jndi-name="omega-ejb/AdApprovalDocumentEJB"
 *           schema="AdApprovalDocument"
 *           primkey-field="adcCode"
 *
 * @ejb:pk class="java.lang.Integer"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="aduser"
 *                        role-link="aduserlink"
 *
 * @ejb:permission role-name="aduser"
 *
 * @ejb:interface local-class="com.ejb.ad.LocalAdApprovalDocument"
 *                local-extends="javax.ejb.EJBLocalObject"
 *
 * @ejb:home local-class="com.ejb.ad.LocalAdApprovalDocumentHome"
 *           local-extends="javax.ejb.EJBLocalHome"
 *
 * @ejb:finder signature="LocalAdApprovalDocument findByAdcType(java.lang.String ADC_TYP, java.lang.Integer ADC_AD_CMPNY)"
 *             query="SELECT OBJECT(adc) FROM AdApprovalDocument adc WHERE adc.adcType = ?1 AND adc.adcAdCompany = ?2"
 *
 * @ejb:finder signature="Collection findAdcAll(java.lang.Integer AD_CMPNY)"
 *             query="SELECT OBJECT(adc) FROM AdApprovalDocument adc WHERE adc.adcAdCompany = ?1"
 *
 * @jboss:query signature="Collection ejbSelectGeneric(java.lang.String jbossQl, java.lang.Object[] args)"
 *                         dynamic="true"
 * 
 * @ejb:value-object match="*"
 *             name="AdApprovalDocument"
 *
 * @jboss:persistence table-name="AD_APPRVL_DCMNT"
 *
 * @jboss:entity-command name="mysql-get-generated-keys"
 */

public abstract class AdApprovalDocumentBean extends AbstractEntityBean {
    

    // PERSISTENT METHODS
    
    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     * @ejb:pk-field
     *
     * @jboss:column-name name="ADC_CODE"
     *
     * @jboss:persistence auto-increment="true"
     **/
    public abstract Integer getAdcCode();
    public abstract void setAdcCode(Integer ADC_CODE);
    
    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="ADC_TYP"
     **/
    public abstract String getAdcType();
     /**
      * @ejb:interface-method view-type="local"
      **/
    public abstract void setAdcType(String ADC_TYP);
    
    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="ADC_PRNT_OPTN"
     **/
    public abstract String getAdcPrintOption();
     /**
      * @ejb:interface-method view-type="local"
      **/
    public abstract void setAdcPrintOption(String ADC_PRNT_OPTN);
    
    
    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="ADC_ALLW_DPLCT"
     **/
    public abstract byte getAdcAllowDuplicate();
     /**
      * @ejb:interface-method view-type="local"
      **/
    public abstract void setAdcAllowDuplicate(byte ADC_ALLW_DPLCT);
    
    
    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="ADC_TRCK_DPLCT"
     **/
    public abstract byte getAdcTrackDuplicate();
     /**
      * @ejb:interface-method view-type="local"
      **/
    public abstract void setAdcTrackDuplicate(byte ADC_TRCK_DPLCT);
    

    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="ADC_ENBL_CRDT_LMT_CHCKNG"
     **/
    public abstract byte getAdcEnableCreditLimitChecking();
     /**
      * @ejb:interface-method view-type="local"
      **/
    public abstract void setAdcEnableCreditLimitChecking(byte ADC_ENBL_CRDT_LMT_CHCKNG);

    
    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="ADC_AD_CMPNY"
     **/
    public abstract Integer getAdcAdCompany();
     /**
      * @ejb:interface-method view-type="local"
      **/
    public abstract void setAdcAdCompany(Integer ADC_AD_CMPNY);
    
     
    // RELATIONSHIP METHODS
   
     /**
     * @ejb:interface-method view-type="local"
     * @ejb:relation name="approvaldocument-amountlimits"
     *               role-name="approvaldocument-has-many-amountlimits"
     * 
     */
     public abstract Collection getAdAmountLimits();
     public abstract void setAdAmountLimits(Collection adAmountLimits);

     /**
      * @jboss:dynamic-ql
      */
     public abstract Collection ejbSelectGeneric(java.lang.String jbossQl, java.lang.Object[] args)
     throws FinderException;     
     
     // BUSINESS METHODS
     
     /**
      * @ejb:home-method view-type="local"
      */
     public Collection ejbHomeGetAdcByCriteria(java.lang.String jbossQl, java.lang.Object[] args)
     throws FinderException {
     	
     	return ejbSelectGeneric(jbossQl, args);
     	
     } 
           
     /**
     * @ejb:interface-method view-type="local"
     **/
     public void addAdAmountLimit(LocalAdAmountLimit adAmountLimit) {

         Debug.print("AdApprovalDocumentBean addAdAmountLimit");
      
         try {
      	
            Collection adAmountLimits = getAdAmountLimits();
	        adAmountLimits.add(adAmountLimit);
	         
         } catch (Exception ex) {
      	
           throw new EJBException(ex.getMessage());
      
         }   
     }

    /**
     * @ejb:interface-method view-type="local"
     **/
     public void dropAdAmountLimit(LocalAdAmountLimit adAmountLimit) {

         Debug.print("AdApprovalDocumentBean dropAdAmountLimit");
      
         try {
      	
            Collection adAmountLimits = getAdAmountLimits();
	        adAmountLimits.remove(adAmountLimit);
	      
         } catch (Exception ex) {
      	
           throw new EJBException(ex.getMessage());
         
         }   
     }

     
    // ENTITY METHODS
    
    /**
     * @ejb:create-method view-type="local"
     **/
     public Integer ejbCreate(Integer ADC_CODE, 
         String ADC_TYP, String ADC_PRNT_OPTN,
		 byte ADC_ALLW_DPLCT, byte ADC_TRCK_DPLCT,
		 byte ADC_ENBL_CRDT_LMT_CHCKNG, Integer ADC_AD_CMPNY)
         throws CreateException {
      	
         Debug.print("AdApprovalDocumentBean ejbCreate");
      
         setAdcCode(ADC_CODE);
         setAdcType(ADC_TYP);           
         setAdcPrintOption(ADC_PRNT_OPTN);
         setAdcAllowDuplicate(ADC_ALLW_DPLCT);
         setAdcTrackDuplicate(ADC_TRCK_DPLCT);
         setAdcEnableCreditLimitChecking(ADC_ENBL_CRDT_LMT_CHCKNG);
         setAdcAdCompany(ADC_AD_CMPNY);
        
         return null;
     } 
    
    /**
     * @ejb:create-method view-type="local"
     **/
     public Integer ejbCreate(
		 String ADC_TYP, String ADC_PRNT_OPTN,
		 byte ADC_ALLW_DPLCT, byte ADC_TRCK_DPLCT,
		 byte ADC_ENBL_CRDT_LMT_CHCKNG, Integer ADC_AD_CMPNY)
         throws CreateException {
      	
         Debug.print("AdApprovalDocumentBean ejbCreate");
                    
         setAdcType(ADC_TYP);
         setAdcPrintOption(ADC_PRNT_OPTN);
         setAdcAllowDuplicate(ADC_ALLW_DPLCT);
         setAdcTrackDuplicate(ADC_TRCK_DPLCT);
         setAdcEnableCreditLimitChecking(ADC_ENBL_CRDT_LMT_CHCKNG);
         setAdcAdCompany(ADC_AD_CMPNY);        
        
         return null;
     }
    
     public void ejbPostCreate(Integer ADC_CODE,
         String ADC_TYP, String ADC_PRNT_OPTN,
		 byte ADC_ALLW_DPLCT, byte ADC_TRCK_DPLCT,
		 byte ADC_ENBL_CRDT_LMT_CHCKNG, Integer ADC_AD_CMPNY)      
         throws CreateException { }
      
     public void ejbPostCreate(
         String ADC_TYP, String ADC_PRNT_OPTN,
		 byte ADC_ALLW_DPLCT, byte ADC_TRCK_DPLCT,
		 byte ADC_ENBL_CRDT_LMT_CHCKNG, Integer ADC_AD_CMPNY)
         throws CreateException { }     
}