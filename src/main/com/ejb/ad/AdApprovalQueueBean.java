/*
 * com/ejb/ad/LocalAdApprovalQueueBean.java
 *
 * Created on March 19, 2004, 3:06 PM
 */
 
package com.ejb.ad;

import java.util.Collection;
import java.util.Date;

import javax.ejb.CreateException;
import javax.ejb.FinderException;

import com.util.AbstractEntityBean;
import com.util.Debug;

/**
 *
 * @author  Neil Andrew M. Ajero
 *
 * @ejb:bean name="AdApprovalQueueEJB"
 *           display-name="Approval Queue Entity"
 *           type="CMP"
 *           cmp-version="2.x"
 *           view-type="local"
 *           local-jndi-name="omega-ejb/AdApprovalQueueEJB"
 *           schema="AdApprovalQueue"
 *           primkey-field="aqCode"
 *
 * @ejb:pk class="java.lang.Integer"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="aduser"
 *                        role-link="aduserlink"
 *
 * @ejb:permission role-name="aduser"
 *
 * @ejb:interface local-class="com.ejb.ad.LocalAdApprovalQueue"
 *                local-extends="javax.ejb.EJBLocalObject"
 *
 * @ejb:home local-class="com.ejb.ad.LocalAdApprovalQueueHome"
 *           local-extends="javax.ejb.EJBLocalHome"
 *
 * @ejb:finder signature="Collection findByAqDocumentAndAqDocumentCode(java.lang.String AQ_DCMNT, java.lang.Integer AQ_DCMNT_CODE, java.lang.Integer AQ_AD_CMPNY)"
 *             query="SELECT OBJECT(aq) FROM AdApprovalQueue aq WHERE aq.aqApproved = 0 AND aq.aqDocument = ?1 AND aq.aqDocumentCode = ?2 AND aq.aqAdCompany = ?3"
 * 
 * @jboss:query signature="Collection findByAqDocumentAndAqDocumentCode(java.lang.String AQ_DCMNT, java.lang.Integer AQ_DCMNT_CODE, java.lang.Integer AQ_AD_CMPNY)"
 *             query="SELECT OBJECT(aq) FROM AdApprovalQueue aq WHERE aq.aqApproved = 0 AND aq.aqDocument = ?1 AND aq.aqDocumentCode = ?2 AND aq.aqAdCompany = ?3 ORDER BY aq.aqCode"
 *
 * @ejb:finder signature="Collection findAllByAqDocumentAndAqDocumentCode(java.lang.String AQ_DCMNT, java.lang.Integer AQ_DCMNT_CODE, java.lang.Integer AQ_AD_CMPNY)"
 *             query="SELECT OBJECT(aq) FROM AdApprovalQueue aq WHERE aq.aqDocument = ?1 AND aq.aqDocumentCode = ?2 AND aq.aqAdCompany = ?3"
 * 
 * @jboss:query signature="Collection findAllByAqDocumentAndAqDocumentCode(java.lang.String AQ_DCMNT, java.lang.Integer AQ_DCMNT_CODE, java.lang.Integer AQ_AD_CMPNY)"
 *             query="SELECT OBJECT(aq) FROM AdApprovalQueue aq WHERE aq.aqDocument = ?1 AND aq.aqDocumentCode = ?2 AND aq.aqAdCompany = ?3 ORDER BY aq.aqCode"
 *
 ** @ejb:finder signature="Collection findByAqDocumentAndAqDocumentCodeAll(java.lang.String AQ_DCMNT, java.lang.Integer AQ_DCMNT_CODE, java.lang.Integer AQ_AD_CMPNY)"
 *             query="SELECT OBJECT(aq) FROM AdApprovalQueue aq WHERE aq.aqDocument = ?1 AND aq.aqDocumentCode = ?2 AND aq.aqAdCompany = ?3"
 * 
 * @jboss:query signature="Collection findByAqDocumentAndAqDocumentCodeAll(java.lang.String AQ_DCMNT, java.lang.Integer AQ_DCMNT_CODE, java.lang.Integer AQ_AD_CMPNY)"
 *             query="SELECT OBJECT(aq) FROM AdApprovalQueue aq WHERE aq.aqDocument = ?1 AND aq.aqDocumentCode = ?2 AND aq.aqAdCompany = ?3 ORDER BY aq.aqLevel"
 *
 *
 * @ejb:finder signature="LocalAdApprovalQueue findByAqDocumentAndAqDocumentCodeAndUsrName(java.lang.String AQ_DCMNT, java.lang.Integer AQ_DCMNT_CODE, java.lang.String USR_NM, java.lang.Integer AQ_AD_CMPNY)"
 *             query="SELECT OBJECT(aq) FROM AdApprovalQueue aq WHERE aq.aqDocument = ?1 AND aq.aqDocumentCode = ?2 AND aq.adUser.usrName = ?3 AND aq.aqAdCompany = ?4"
 *
 *
 * @ejb:finder signature="LocalAdApprovalQueue findByAqDeptAndAqLevelAndAqDocumentCode(java.lang.String AQ_DPT, java.lang.String AQ_LVL, java.lang.Integer AQ_DCMNT_CODE, java.lang.Integer AQ_AD_CMPNY)"
 *             query="SELECT OBJECT(aq) FROM AdApprovalQueue aq WHERE aq.aqDepartment = ?1 AND aq.aqLevel = ?2 AND aq.aqDocumentCode = ?3 AND aq.aqAdCompany = ?4"
 *
 *
 * @ejb:finder signature="Collection findByAqDocumentAndUserName(java.lang.String AQ_DCMNT, java.lang.String USR_NM, java.lang.Integer OFFSET, java.lang.Integer LIMIT, java.lang.Integer AQ_AD_CMPNY)"
 *             query="SELECT OBJECT(aq) FROM AdApprovalQueue aq WHERE aq.aqApproved = 0 AND aq.aqDocument = ?1 AND aq.adUser.usrName = ?2 AND aq.aqAdCompany = ?5 OFFSET ?3 LIMIT ?4"
 *
 * @jboss:query signature="Collection findByAqDocumentAndUserName(java.lang.String AQ_DCMNT, java.lang.String USR_NM, java.lang.Integer OFFSET, java.lang.Integer LIMIT, java.lang.Integer AQ_AD_CMPNY)"
 *             query="SELECT OBJECT(aq) FROM AdApprovalQueue aq WHERE aq.aqApproved = 0 AND aq.aqDocument = ?1 AND aq.adUser.usrName = ?2 AND aq.aqAdCompany = ?5 ORDER BY aq.aqDocumentCode OFFSET ?3 LIMIT ?4"
 *
 * @ejb:finder signature="Collection findByAqDocumentAndUsrCode(java.lang.String AQ_DCMNT, java.lang.Integer USR_CODE, java.lang.Integer AQ_AD_BRNCH, java.lang.Integer AQ_AD_CMPNY)"
 *             query="SELECT OBJECT(aq) FROM AdApprovalQueue aq WHERE aq.aqForApproval = 1 AND aq.aqApproved = 0 AND aq.aqDocument = ?1 AND aq.adUser.usrCode = ?2 AND aq.aqAdBranch = ?3 AND aq.aqAdCompany = ?4"
 *
 * @ejb:finder signature="Collection findByAqDocumentAndAqDocumentCodeGreaterThanAsc(java.lang.String AQ_DCMNT, java.lang.Integer AQ_DCMNT_CODE, java.lang.Integer AQ_CODE, java.lang.Integer AQ_AD_CMPNY)"
 *             query="SELECT OBJECT(aq) FROM AdApprovalQueue aq WHERE aq.aqApproved = 0 AND aq.aqDocument = ?1 AND aq.aqDocumentCode = ?2 AND aq.aqCode > ?3 AND aq.aqAdCompany = ?4"
 *
 * @jboss:query signature="Collection findByAqDocumentAndAqDocumentCodeGreaterThanAsc(java.lang.String AQ_DCMNT, java.lang.Integer AQ_DCMNT_CODE, java.lang.Integer AQ_CODE, java.lang.Integer AQ_AD_CMPNY)"
 *             query="SELECT OBJECT(aq) FROM AdApprovalQueue aq WHERE aq.aqApproved = 0 AND aq.aqDocument = ?1 AND aq.aqDocumentCode = ?2 AND aq.aqCode > ?3 AND aq.aqAdCompany = ?4 ORDER BY aq.aqCode"
 * 
 * @ejb:finder signature="Collection findByAqDocumentAndAqDocumentCodeLessThanDesc(java.lang.String AQ_DCMNT, java.lang.Integer AQ_DCMNT_CODE, java.lang.Integer AQ_CODE, java.lang.Integer AQ_AD_CMPNY)"
 *             query="SELECT OBJECT(aq) FROM AdApprovalQueue aq WHERE aq.aqApproved = 0 AND aq.aqDocument = ?1 AND aq.aqDocumentCode = ?2 AND aq.aqCode < ?3 AND aq.aqAdCompany = ?4"
 *
 * @jboss:query signature="Collection findByAqDocumentAndAqDocumentCodeLessThanDesc(java.lang.String AQ_DCMNT, java.lang.Integer AQ_DCMNT_CODE, java.lang.Integer AQ_CODE, java.lang.Integer AQ_AD_CMPNY)"
 *             query="SELECT OBJECT(aq) FROM AdApprovalQueue aq WHERE aq.aqApproved = 0 AND aq.aqDocument = ?1 AND aq.aqDocumentCode = ?2 AND aq.aqCode < ?3 AND aq.aqAdCompany = ?4 ORDER BY aq.aqCode DESC"
 *
 * @ejb:select signature="Collection ejbSelectGeneric(java.lang.String jbossQl, java.lang.Object[] args)"
 *             result-type-mapping="Local"
 *             method-intf="LocalHome"
 *             query="SELECT OBJECT(aq) FROM AdApprovalQueue aq"
 *
 * @jboss:query signature="Collection ejbSelectGeneric(java.lang.String jbossQl, java.lang.Object[] args)"
 *                         dynamic="true"
 *
 * @ejb:value-object match="*"
 *             name="AdApprovalQueue"
 *
 * @jboss:persistence table-name="AD_APPRVL_QUEUE"
 *
 * @jboss:entity-command name="mysql-get-generated-keys"
 */

public abstract class AdApprovalQueueBean extends AbstractEntityBean {
    

    // PERSISTENT METHODS
    
    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     * @ejb:pk-field
     *
     * @jboss:column-name name="AQ_CODE"
     *
     * @jboss:persistence auto-increment="true"
     **/
    public abstract Integer getAqCode();
    public abstract void setAqCode(Integer AQ_CODE);
    
    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="AQ_DEPT"
     **/
    public abstract String getAqDepartment();
     /**
      * @ejb:interface-method view-type="local"
      **/
    public abstract void setAqDepartment(String AQ_DEPT);
    
    
    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="AQ_LVL"
     **/
    public abstract String getAqLevel();
     /**
      * @ejb:interface-method view-type="local"
      **/
    public abstract void setAqLevel(String AQ_LVL);
    
    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="AQ_FOR_APPRVL"
     **/
    public abstract byte getAqForApproval();
     /**
      * @ejb:interface-method view-type="local"
      **/
    public abstract void setAqForApproval(byte AQ_FOR_APPRVL);
    
    
    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="AQ_NXT_LVL"
     **/
    public abstract String getAqNextLevel();
     /**
      * @ejb:interface-method view-type="local"
      **/
    public abstract void setAqNextLevel(String AQ_NXT_LVL);
    
    
    
    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="AQ_DCMNT"
     **/
    public abstract String getAqDocument();
     /**
      * @ejb:interface-method view-type="local"
      **/
    public abstract void setAqDocument(String AQ_DCMNT);
    
    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="AQ_DCMNT_CODE"
     **/
    public abstract Integer getAqDocumentCode();
     /**
      * @ejb:interface-method view-type="local"
      **/
    public abstract void setAqDocumentCode(Integer AQ_DCMNT_CODE);
    
    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="AQ_DCMNT_NMBR"
     **/
    public abstract String getAqDocumentNumber();
     /**
      * @ejb:interface-method view-type="local"
      **/
    public abstract void setAqDocumentNumber(String AQ_DCMNT_NMBR);
    
    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="AQ_DT"
     **/
    public abstract Date getAqDate();
     /**
      * @ejb:interface-method view-type="local"
      **/
    public abstract void setAqDate(Date AQ_DT);
    
    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="AQ_APPRVD_DT"
     **/
    public abstract Date getAqApprovedDate();
     /**
      * @ejb:interface-method view-type="local"
      **/
    public abstract void setAqApprovedDate(Date AQ_APPRVD_DT);
    
    
    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="AQ_AND_OR"
     **/
    public abstract String getAqAndOr();
     /**
      * @ejb:interface-method view-type="local"
      **/
    public abstract void setAqAndOr(String AQ_AND_OR);
    
    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="AQ_USR_OR"
     **/
    public abstract byte getAqUserOr();
     /**
      * @ejb:interface-method view-type="local"
      **/
    public abstract void setAqUserOr(byte AQ_USR_OR);

    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="AQ_EML_SNT"
     **/
    public abstract byte getAqEmailSent();
     /**
      * @ejb:interface-method view-type="local"
      **/
    public abstract void setAqEmailSent(byte AQ_EML_SNT);
    
    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="AQ_APPRVD"
     **/
    public abstract byte getAqApproved();
     /**
      * @ejb:interface-method view-type="local"
      **/
    public abstract void setAqApproved(byte AQ_APPRVD);
    
    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="AQ_RQSTR_NM"
     **/
    public abstract String getAqRequesterName();
     /**
      * @ejb:interface-method view-type="local"
      **/
    public abstract void setAqRequesterName(String AQ_RQSTR_NM);
    
    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="AQ_AD_BRNCH"
     **/
    public abstract Integer getAqAdBranch();
     /**
      * @ejb:interface-method view-type="local"
      **/
    public abstract void setAqAdBranch(Integer AQ_AD_BRNCH);

    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="AQ_AD_CMPNY"
     **/
    public abstract Integer getAqAdCompany();
     /**
      * @ejb:interface-method view-type="local"
      **/
    public abstract void setAqAdCompany(Integer AQ_AD_CMPNY);
     
    // RELATIONSHIP METHODS
   
    /**
     * @ejb:interface-method view-type="local"
     * @ejb:relation name="aduser-approvalqueues"
     *               role-name="approvalqueue-has-one-aduser"
     * 
     * @jboss:relation related-pk-field="usrCode"
     *                 fk-column="AD_USER"
     */
     public abstract LocalAdUser getAdUser();
     public abstract void setAdUser(LocalAdUser adUser);

     
     /**
      * @jboss:dynamic-ql
      */
     public abstract Collection ejbSelectGeneric(java.lang.String jbossQl, java.lang.Object[] args)
     throws FinderException;
     
     
     // BUSINESS METHODS
     
     /**
      * @ejb:home-method view-type="local"
      */
     public Collection ejbHomeGetAqByCriteria(java.lang.String jbossQl, java.lang.Object[] args)
     throws FinderException {
     	
     	return ejbSelectGeneric(jbossQl, args);
     }   
     
     
    // ENTITY METHODS
    
    /**
     * @ejb:create-method view-type="local"
     **/
     public Integer ejbCreate(Integer AQ_CODE, 
    		 byte AQ_FOR_APPRVL, String AQ_DCMNT, Integer AQ_DCMNT_CODE, String AQ_DCMNT_NMBR, Date AQ_DT, String AQ_AND_OR,
         byte AQ_USR_OR, Integer AQ_AD_BRNCH, Integer AQ_AD_CMPNY)
         throws CreateException {
      	
         Debug.print("AdApprovalQueueBean ejbCreate");
      
         setAqCode(AQ_CODE);
         setAqForApproval(AQ_FOR_APPRVL);
         setAqDocument(AQ_DCMNT);
         setAqDocumentCode(AQ_DCMNT_CODE);  
         setAqDocumentNumber(AQ_DCMNT_NMBR);
         setAqDate(AQ_DT);
         setAqAndOr(AQ_AND_OR);
         setAqUserOr(AQ_USR_OR);
         setAqAdBranch(AQ_AD_BRNCH);
         setAqAdCompany(AQ_AD_CMPNY);
        
         return null;
     } 
    
    /**
     * @ejb:create-method view-type="local"
     **/
     public Integer ejbCreate(
    		 byte AQ_FOR_APPRVL, String AQ_DCMNT, Integer AQ_DCMNT_CODE, String AQ_DCMNT_NMBR, Date AQ_DT, String AQ_AND_OR,
    		 byte AQ_USR_OR, Integer AQ_AD_BRNCH, Integer AQ_AD_CMPNY)
         throws CreateException {
      	
         Debug.print("AdApprovalQueueBean ejbCreate");
                    
         setAqForApproval(AQ_FOR_APPRVL);
         setAqDocument(AQ_DCMNT);
         setAqDocumentCode(AQ_DCMNT_CODE);
         setAqDocumentNumber(AQ_DCMNT_NMBR);
         setAqDate(AQ_DT);
         setAqAndOr(AQ_AND_OR);
         setAqUserOr(AQ_USR_OR);
         setAqAdBranch(AQ_AD_BRNCH);
         setAqAdCompany(AQ_AD_CMPNY);
        
         return null;
     }
     
     
     /**
      * @ejb:create-method view-type="local"
      **/
      public Integer ejbCreate(
 		 String AQ_DEPT, String AQ_LVL, byte AQ_FOR_APPRVL, String AQ_NXT_LVL, String AQ_DCMNT, Integer AQ_DCMNT_CODE, String AQ_DCMNT_NMBR, Date AQ_DT, Date AQ_APPRVD_DT, String AQ_AND_OR,
 		 byte AQ_USR_OR, byte AQ_EML_SNT, String AQ_RQSTR_NM,
 		 Integer AQ_AD_BRNCH, Integer AQ_AD_CMPNY)
          throws CreateException {
       	
          Debug.print("AdApprovalQueueBean ejbCreate");
          
          setAqDepartment(AQ_DEPT);
          setAqLevel(AQ_LVL);
          setAqForApproval(AQ_FOR_APPRVL);
          setAqNextLevel(AQ_NXT_LVL);
          setAqDocument(AQ_DCMNT);
          setAqDocumentCode(AQ_DCMNT_CODE);
          setAqDocumentNumber(AQ_DCMNT_NMBR);
          setAqDate(AQ_DT);
          setAqAndOr(AQ_AND_OR);
          setAqUserOr(AQ_USR_OR);
          setAqEmailSent(AQ_EML_SNT);
          setAqRequesterName(AQ_RQSTR_NM);
          setAqApprovedDate(AQ_APPRVD_DT);
          setAqAdBranch(AQ_AD_BRNCH);
          setAqAdCompany(AQ_AD_CMPNY);
         
          return null;
      }
    
     public void ejbPostCreate(Integer AQ_CODE,
    		 byte AQ_FOR_APPRVL, String AQ_DCMNT, Integer AQ_DCMNT_CODE, String AQ_DCMNT_NMBR, Date AQ_DT, String AQ_AND_OR,
    		 byte AQ_USR_OR, Integer AQ_AD_BRNCH, Integer AQ_AD_CMPNY)      
         throws CreateException { }
      
     public void ejbPostCreate(
    		 byte AQ_FOR_APPRVL, String AQ_DCMNT, Integer AQ_DCMNT_CODE, String AQ_DCMNT_NMBR, Date AQ_DT, String AQ_AND_OR,
    		 byte AQ_USR_OR, Integer AQ_AD_BRNCH, Integer AQ_AD_CMPNY)
         throws CreateException { } 
     
     public void ejbPostCreate(
             String AQ_DEPT, String AQ_LVL, byte AQ_FOR_APPRVL, String AQ_NXT_LVL, String AQ_DCMNT, Integer AQ_DCMNT_CODE, String AQ_DCMNT_NMBR, Date AQ_DT, Date AQ_APPRVD_DT, String AQ_AND_OR,
             byte AQ_USR_OR, byte AQ_EML_SNT, String AQ_RQSTR_NM,
             Integer AQ_AD_BRNCH, Integer AQ_AD_CMPNY)
        throws CreateException { }
}