/*
 * com/ejb/ad/LocalAdApprovalUserBean.java
 *
 * Created on March 19, 2004, 3:23 PM
 */
 
package com.ejb.ad;

import javax.ejb.CreateException;

import com.util.AbstractEntityBean;
import com.util.Debug;

/**
 *
 * @author  Neil Andrew M. Ajero
 *
 * @ejb:bean name="AdApprovalUserEJB"
 *           display-name="Approval User Entity"
 *           type="CMP"
 *           cmp-version="2.x"
 *           view-type="local"
 *           local-jndi-name="omega-ejb/AdApprovalUserEJB"
 *           schema="AdApprovalUser"
 *           primkey-field="auCode"
 *
 * @ejb:pk class="java.lang.Integer"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="aduser"
 *                        role-link="aduserlink"
 *
 * @ejb:permission role-name="aduser"
 *
 * @ejb:interface local-class="com.ejb.ad.LocalAdApprovalUser"
 *                local-extends="javax.ejb.EJBLocalObject"
 *
 * @ejb:home local-class="com.ejb.ad.LocalAdApprovalUserHome"
 *           local-extends="javax.ejb.EJBLocalHome"
 *
 *
 * @ejb:finder signature="Collection findByAuTypeAndCalCode(java.lang.String AU_TYP, java.lang.Integer CAL_CODE, java.lang.Integer AU_AD_CMPNY)"
 *             query="SELECT OBJECT(au) FROM AdApprovalUser au WHERE au.auType = ?1 AND au.adAmountLimit.calCode = ?2 AND au.auAdCompany = ?3"
 *
 * @jboss:query signature="Collection findByAuTypeAndCalCode(java.lang.String AU_TYP, java.lang.Integer CAL_CODE, java.lang.Integer AU_AD_CMPNY)"
 *             query="SELECT OBJECT(au) FROM AdApprovalUser au WHERE au.auType = ?1 AND au.adAmountLimit.calCode = ?2 AND au.auAdCompany = ?3 ORDER BY au.auCode"
 *
 *
 * @ejb:finder signature="Collection findByCalDeptAndAuTypeAndCalCode(java.lang.String CAL_DEPT, java.lang.String AU_TYP, java.lang.Integer CAL_CODE, java.lang.Integer AU_AD_CMPNY)"
 *             query="SELECT OBJECT(au) FROM AdApprovalUser au WHERE au.adAmountLimit.calDept = ?1 AND au.auType = ?2 AND au.adAmountLimit.calCode = ?3 AND au.auAdCompany = ?4"
 *
 * @jboss:query signature="Collection findByCalDeptAndAuTypeAndCalCode(java.lang.String CAL_DEPT, java.lang.String AU_TYP, java.lang.Integer CAL_CODE, java.lang.Integer AU_AD_CMPNY)"
 *             query="SELECT OBJECT(au) FROM AdApprovalUser au WHERE au.adAmountLimit.calDept = ?1 AND au.auType = ?2 AND au.adAmountLimit.calCode = ?3 AND au.auAdCompany = ?4 ORDER BY au.auLevel"
 *
 *
 *
 *@ejb:finder signature="Collection findByCalCode(java.lang.Integer CAL_CODE, java.lang.Integer AU_AD_CMPNY)"
 *             query="SELECT OBJECT(au) FROM AdAmountLimit cal, IN(cal.adApprovalUsers) au WHERE cal.calCode = ?1 AND au.auAdCompany = ?2"
 *
 * @jboss:query signature="Collection findByCalCode(java.lang.Integer CAL_CODE, java.lang.Integer AU_AD_CMPNY)"
 *             query="SELECT OBJECT(au) FROM AdAmountLimit cal, IN(cal.adApprovalUsers) au WHERE cal.calCode = ?1 AND au.auAdCompany = ?2 ORDER BY au.adUser.usrName"
 *
 * @ejb:finder signature="LocalAdApprovalUser findByUsrNameAndCalCode(java.lang.String USR_NM, java.lang.Integer CAL_CODE, java.lang.Integer AU_AD_CMPNY)"
 *             query="SELECT OBJECT(au) FROM AdAmountLimit cal, IN(cal.adApprovalUsers) au WHERE au.adUser.usrName = ?1 AND cal.calCode = ?2 AND au.auAdCompany = ?3"
 *
 * @ejb:finder signature="LocalAdApprovalUser findByAdcCodeAndApprovalTypeAndUsrNameAndAclDept(java.lang.String AU_TYP, java.lang.Integer ADC_CODE, java.lang.String USR_NM, java.lang.String CAL_DEPT, double CAL_AMNT_LMT, java.lang.Integer AU_AD_CMPNY)"
 *             query="SELECT OBJECT(au) FROM AdApprovalDocument adc, IN(adc.adAmountLimits) cal, IN(cal.adApprovalUsers) au WHERE au.auType = ?1 AND adc.adcCode = ?2 AND au.adUser.usrName = ?3 AND cal.calDept = ?4 AND cal.calAmountLimit = ?5 AND au.auAdCompany = ?6"
 *
 * @ejb:finder signature="LocalAdApprovalUser findByAdcCodeAndUsrNameAndRequesterTypeLessThanAmountLimit(java.lang.String USR_NM, java.lang.String AU_TYP, double CAL_AMNT_LMT, java.lang.Integer ADC_CODE, java.lang.Integer AU_AD_CMPNY)"
 *             query="SELECT OBJECT(au) FROM AdApprovalDocument adc, IN(adc.adAmountLimits) cal, IN(cal.adApprovalUsers) au WHERE au.adUser.usrName = ?1 AND au.auType = ?2 AND cal.calAmountLimit < ?3 AND adc.adcCode = ?4 AND au.auAdCompany = ?5"
 *
 * @ejb:finder signature="LocalAdApprovalUser findByAdcCodeAndUsrNameAndApproverTypeLessThanAmountLimit(java.lang.String USR_NM, java.lang.String AU_TYP, double CAL_AMNT_LMT, java.lang.Integer ADC_CODE, java.lang.Integer AU_AD_CMPNY)"
 *             query="SELECT OBJECT(au) FROM AdApprovalDocument adc, IN(adc.adAmountLimits) cal, IN(cal.adApprovalUsers) au WHERE au.adUser.usrName = ?1 AND au.auType = ?2 AND cal.calAmountLimit > ?3 AND adc.adcCode = ?4 AND au.auAdCompany = ?5"
 *
 * @ejb:finder signature="LocalAdApprovalUser findByAclCodeAndApprovalTypeAndUsrName(java.lang.String AU_TYP, java.lang.Integer ACL_CODE, java.lang.String USR_NM, java.lang.Integer AU_AD_CMPNY)"
 *             query="SELECT OBJECT(au) FROM AdApprovalCoaLine acl, IN(acl.adAmountLimits) cal, IN(cal.adApprovalUsers) au WHERE au.auType = ?1 AND acl.aclCode = ?2 AND au.adUser.usrName = ?3 AND au.auAdCompany = ?4"
 *
 * @ejb:finder signature="LocalAdApprovalUser findByAclCodeAndUsrNameAndRequesterTypeLessThanAmountLimit(java.lang.String USR_NM, java.lang.String AU_TYP, double CAL_AMNT_LMT, java.lang.Integer ACL_CODE, java.lang.Integer AU_AD_CMPNY)"
 *             query="SELECT OBJECT(au) FROM AdApprovalCoaLine acl, IN(acl.adAmountLimits) cal, IN(cal.adApprovalUsers) au WHERE au.adUser.usrName = ?1 AND au.auType = ?2 AND cal.calAmountLimit < ?3 AND acl.aclCode = ?4 AND au.auAdCompany = ?5"
 *
 * @ejb:finder signature="LocalAdApprovalUser findByAclCodeAndUsrNameAndApproverTypeLessThanAmountLimit(java.lang.String USR_NM, java.lang.String AU_TYP, double CAL_AMNT_LMT, java.lang.Integer ACL_CODE, java.lang.Integer AU_AD_CMPNY)"
 *             query="SELECT OBJECT(au) FROM AdApprovalCoaLine acl, IN(acl.adAmountLimits) cal, IN(cal.adApprovalUsers) au WHERE au.adUser.usrName = ?1 AND au.auType = ?2 AND cal.calAmountLimit > ?3 AND acl.aclCode = ?4 AND au.auAdCompany = ?5"
 *
 * @ejb:value-object match="*"
 *             name="AdApprovalUser"
 *
 * @jboss:persistence table-name="AD_APPRVL_USR"
 *
 * @jboss:entity-command name="mysql-get-generated-keys"
 */

public abstract class AdApprovalUserBean extends AbstractEntityBean {
    

    // PERSISTENT METHODS
    
    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     * @ejb:pk-field
     *
     * @jboss:column-name name="AU_CODE"
     *
     * @jboss:persistence auto-increment="true"
     **/
    public abstract Integer getAuCode();
    public abstract void setAuCode(Integer AU_CODE);
    
    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="AU_LVL"
     **/
    public abstract String getAuLevel();
     /**
      * @ejb:interface-method view-type="local"
      **/
    public abstract void setAuLevel(String AU_LVL);
    
    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="AU_TYP"
     **/
    public abstract String getAuType();
     /**
      * @ejb:interface-method view-type="local"
      **/
    public abstract void setAuType(String AU_TYP);
    
    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="AU_OR"
     **/
    public abstract byte getAuOr();
     /**
      * @ejb:interface-method view-type="local"
      **/
    public abstract void setAuOr(byte AU_OR);
    
    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="AU_AD_CMPNY"
     **/
    public abstract Integer getAuAdCompany();
     /**
      * @ejb:interface-method view-type="local"
      **/
    public abstract void setAuAdCompany(Integer AU_AD_CMPNY);
    
      
    // RELATIONSHIP METHODS
   
   /**
     * @ejb:interface-method view-type="local"
     * @ejb:relation name="amountlimit-approvalusers"
     *               role-name="approvaluser-has-one-amountlimit"
     *               cascade-delete="yes"
     * 
     * @jboss:relation related-pk-field="calCode"
     *                 fk-column="AD_AMOUNT_LIMIT"
     */
     public abstract LocalAdAmountLimit getAdAmountLimit();
     public abstract void setAdAmountLimit(LocalAdAmountLimit adAmountLimit);

   /**
     * @ejb:interface-method view-type="local"
     * @ejb:relation name="aduser-approvalusers"
     *               role-name="approvaluser-has-one-aduser"
     * 
     * @jboss:relation related-pk-field="usrCode"
     *                 fk-column="AD_USER"
     */
     public abstract LocalAdUser getAdUser();
     public abstract void setAdUser(LocalAdUser adUser);
   

    // NO BUSINESS METHODS

    // ENTITY METHODS
    
    /**
     * @ejb:create-method view-type="local"
     **/
     public Integer ejbCreate(Integer AU_CODE, String AU_LVL, String AU_TYP, byte AU_OR, Integer AU_AD_CMPNY)
         throws CreateException {
      	
         Debug.print("AdApprovalUserBean ejbCreate");
      
         setAuCode(AU_CODE);
         setAuLevel(AU_LVL);
         setAuType(AU_TYP);  
         setAuOr(AU_OR);
         setAuAdCompany(AU_AD_CMPNY);
        
         return null;
     } 
    
    /**
     * @ejb:create-method view-type="local"
     **/
     public Integer ejbCreate(String AU_LVL, String AU_TYP, byte AU_OR, Integer AU_AD_CMPNY)
         throws CreateException {
      	
         Debug.print("AdApprovalUserBean ejbCreate");
         
         setAuLevel(AU_LVL);
         setAuType(AU_TYP);  
         setAuOr(AU_OR);
         setAuAdCompany(AU_AD_CMPNY);
        
         return null;
     }
    
     public void ejbPostCreate(Integer AU_CODE, String AU_LVL, String AU_TYP, byte AU_OR, Integer AU_AD_CMPNY)      
         throws CreateException { }
      
     public void ejbPostCreate(String AU_LVL, String AU_TYP, byte AU_OR, Integer AU_AD_CMPNY)
         throws CreateException { }     
}