/*
 * com/ejb/ad/LocalAdBankAccountBalanceBean.java
 *
 * Created on January 16, 2006, 1:12 PM
 */

package com.ejb.ad;

import java.util.Date;

import javax.ejb.CreateException;

import com.util.AbstractEntityBean;
import com.util.Debug;

/**
 *
 * @author  Franco Antonio R. Roig
 *
 * @ejb:bean name="AdBankAccountBalanceEJB"
 *           display-name="BankAccountBalance Entity"
 *           type="CMP"
 *           cmp-version="2.x"
 *           view-type="local"
 *           local-jndi-name="omega-ejb/AdBankAccountBalanceEJB"
 *           schema="AdBankAccountBalance"
 *           primkey-field="babCode"
 *
 * @ejb:pk class="java.lang.Integer"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="aduser"
 *                        role-link="aduserlink"
 *
 * @ejb:permission role-name="aduser"
 *
 * @ejb:interface local-class="com.ejb.ad.LocalAdBankAccountBalance"
 *                local-extends="javax.ejb.EJBLocalObject"
 *
 * @ejb:home local-class="com.ejb.ad.LocalAdBankAccountBalanceHome"
 *           local-extends="javax.ejb.EJBLocalHome"
 *
 * @ejb:finder signature="Collection findByBeforeOrEqualDateAndBaCodeAndType(java.util.Date DT, java.lang.Integer BA_CODE, java.lang.String BAB_TYP, java.lang.Integer BAB_AD_CMPNY)"
 *             query="SELECT OBJECT(bab) FROM AdBankAccountBalance bab WHERE bab.babDate <= ?1 AND bab.adBankAccount.baCode = ?2 AND bab.babType = ?3 AND bab.babAdCompany = ?4"
 *
 * @jboss:query signature="Collection findByBeforeOrEqualDateAndBaCodeAndType(java.util.Date DT, java.lang.Integer BA_CODE, java.lang.String BAB_TYP, java.lang.Integer BAB_AD_CMPNY)"
 *             query="SELECT OBJECT(bab) FROM AdBankAccountBalance bab WHERE bab.babDate <= ?1 AND bab.adBankAccount.baCode = ?2 AND bab.babType = ?3 AND bab.babAdCompany = ?4 ORDER BY bab.babDate"
 *
 * @ejb:finder signature="Collection findByAfterDateAndBaCodeAndType(java.util.Date DT, java.lang.Integer BA_CODE, java.lang.String BAB_TYP, java.lang.Integer BAB_AD_CMPNY)"
 *             query="SELECT OBJECT(bab) FROM AdBankAccountBalance bab WHERE bab.babDate > ?1 AND bab.adBankAccount.baCode = ?2 AND bab.babType = ?3 AND bab.babAdCompany = ?4"
 *
 * @jboss:query signature="Collection findByAfterDateAndBaCodeAndType(java.util.Date DT, java.lang.Integer BA_CODE, java.lang.String BAB_TYP, java.lang.Integer BAB_AD_CMPNY)"
 *             query="SELECT OBJECT(bab) FROM AdBankAccountBalance bab WHERE bab.babDate > ?1 AND bab.adBankAccount.baCode = ?2 AND bab.babType = ?3 AND bab.babAdCompany = ?4 ORDER BY bab.babDate"
 * 
 * @ejb:finder signature="Collection findByBaCodeAndType(java.lang.Integer BA_CODE, java.lang.String BAB_TYP, java.lang.Integer BAB_AD_CMPNY)"
 *             query="SELECT OBJECT(bab) FROM AdBankAccountBalance bab WHERE bab.adBankAccount.baCode = ?1 AND bab.babType = ?2 AND bab.babAdCompany = ?3"
 *
 * @jboss:query signature="Collection findByBaCodeAndType(java.lang.Integer BA_CODE, java.lang.String BAB_TYP, java.lang.Integer BAB_AD_CMPNY)"
 *             query="SELECT OBJECT(bab) FROM AdBankAccountBalance bab WHERE bab.adBankAccount.baCode = ?1 AND bab.babType = ?2 AND bab.babAdCompany = ?3 ORDER BY bab.babDate"
 *
 * @ejb:finder signature="Collection findByBeforeDateAndBaCodeAndType(java.util.Date DT, java.lang.Integer BA_CODE, java.lang.String BAB_TYP, java.lang.Integer BAB_AD_CMPNY)"
 *             query="SELECT OBJECT(bab) FROM AdBankAccountBalance bab WHERE bab.babDate < ?1 AND bab.adBankAccount.baCode = ?2 AND bab.babType = ?3 AND bab.babAdCompany = ?4"
 *
 * @jboss:query signature="Collection findByBeforeDateAndBaCodeAndType(java.util.Date DT, java.lang.Integer BA_CODE, java.lang.String BAB_TYP, java.lang.Integer BAB_AD_CMPNY)"
 *             query="SELECT OBJECT(bab) FROM AdBankAccountBalance bab WHERE bab.babDate < ?1 AND bab.adBankAccount.baCode = ?2 AND bab.babType = ?3 AND bab.babAdCompany = ?4 ORDER BY bab.babDate"
 *
 * @ejb:value-object match="*"
 *             name="AdBankAccountBalance"
 *
 * @jboss:persistence table-name="AD_BNK_ACCNT_BLNC"
 *
 * @jboss:entity-command name="mysql-get-generated-keys"
 */

public abstract class AdBankAccountBalanceBean extends AbstractEntityBean {
    
    
    // PERSISTENT METHODS
    
    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     * @ejb:pk-field
     *
     * @jboss:column-name name="BAB_CODE"
     *
     * @jboss:persistence auto-increment="true"
     **/
    public abstract Integer getBabCode();
    public abstract void setBabCode(Integer BAB_CODE);
    
    
    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"   
     *
     * @jboss:column-name name="BAB_DT"
     **/
    public abstract Date getBabDate();
     /**
      * @ejb:interface-method view-type="local"
      **/ 
    public abstract void setBabDate(Date BAB_DT);
    

    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"   
     *
     * @jboss:column-name name="BAB_BLNC"
     **/
    public abstract double getBabBalance();
     /**
      * @ejb:interface-method view-type="local"
      **/ 
    public abstract void setBabBalance(double BAB_BLNC);
    
    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"   
     *
     * @jboss:column-name name="BAB_TYP"
     **/
    public abstract String getBabType();
     /**
      * @ejb:interface-method view-type="local"
      **/ 
    public abstract void setBabType(String BAB_TYP);
    
    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"   
     *
     * @jboss:column-name name="BAB_AD_CMPNY"
     **/
    public abstract Integer getBabAdCompany();
     /**
      * @ejb:interface-method view-type="local"
      **/ 
    public abstract void setBabAdCompany(Integer BAB_AD_CMPNY);
    
   
    // RELATIONSHIP METHODS
    
    /**
     * @ejb:interface-method view-type="local"
     * @ejb:relation name="bankaccount-bankaccountbalances"
     *               role-name="bankaccountbalance-has-one-bankaccount"
     * 
     * @jboss:relation related-pk-field="baCode"
     *                 fk-column="AD_BANK_ACCOUNT"
     */
    public abstract LocalAdBankAccount getAdBankAccount();
    /**
     * @ejb:interface-method view-type="local"
     **/
    public abstract void setAdBankAccount(LocalAdBankAccount adBankAccount);


    // NO BUSINESS METHODS
     
 
    // ENTITY METHODS
    
    /**
     * @ejb:create-method view-type="local"
     **/
    public Integer ejbCreate(Integer BAB_CODE, 
         Date BAB_DT, double BAB_BLNC, String BAB_TYP, Integer BAB_AD_CMPNY)     	
         throws CreateException {	
     	
         Debug.print("AdBankAccountBalanceBean ejbCreate");
     
	     setBabCode(BAB_CODE);
	     setBabDate(BAB_DT);
	     setBabBalance(BAB_BLNC);   
	     setBabType(BAB_TYP);
	     setBabAdCompany(BAB_AD_CMPNY);

         return null;
     }
     
    /**
     * @ejb:create-method view-type="local"
     **/
    public Integer ejbCreate( 
         Date BAB_DT, double BAB_BLNC, String BAB_TYP, Integer BAB_AD_CMPNY)     	
         throws CreateException {	
     	
         Debug.print("AdBankAccountBalanceBean ejbCreate");
     
	     setBabDate(BAB_DT);
	     setBabBalance(BAB_BLNC);   
	     setBabType(BAB_TYP);
	     setBabAdCompany(BAB_AD_CMPNY);

         return null;
     }
     
     public void ejbPostCreate(Integer BAB_CODE, 
         Date BAB_DT, double BAB_BLNC, String BAB_TYP, Integer BAB_AD_CMPNY)      
         throws CreateException { }
      
     public void ejbPostCreate( 
         Date BAB_DT, double BAB_BLNC, String BAB_TYP, Integer BAB_AD_CMPNY)
         throws CreateException { }     
}