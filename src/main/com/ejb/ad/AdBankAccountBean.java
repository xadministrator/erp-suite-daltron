/*
 * com/ejb/ad/LocalAdBankAccountBean.java
 *
 * Created on February 21, 2003, 1:06 PM
 */
 
package com.ejb.ad;

import java.util.Collection;
import java.util.Date;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;

import com.ejb.ad.LocalAdBankAccountBalance;
import com.util.AbstractEntityBean;
import com.util.Debug;

/**
 *
 * @author  Dennis M. Hilario
 *
 * @ejb:bean name="AdBankAccountEJB"
 *           display-name="Bank Account Entity"
 *           type="CMP"
 *           cmp-version="2.x"
 *           view-type="local"
 *           local-jndi-name="omega-ejb/AdBankAccountEJB"
 *           schema="AdBankAccount"
 *           primkey-field="baCode"
 *
 * @ejb:pk class="java.lang.Integer"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="aruser"
 *                        role-link="aruserlink"
 *
 * @ejb:permission role-name="aruser"
 *
 * @ejb:interface local-class="com.ejb.ad.LocalAdBankAccount"
 *                local-extends="javax.ejb.EJBLocalObject"
 *
 * @ejb:home local-class="com.ejb.ad.LocalAdBankAccountHome"
 *           local-extends="javax.ejb.EJBLocalHome"
 *
 * @ejb:finder signature="Collection findEnabledBaAll(java.lang.Integer BA_AD_BRNCH, java.lang.Integer BA_AD_CMPNY)"
 *             query="SELECT OBJECT(ba) FROM AdBankAccount ba, IN(ba.adBranchBankAccounts)bba WHERE ba.baEnable = 1 AND bba.adBranch.brCode = ?1 AND ba.baAdCompany = ?2"
 *
 * @jboss:query signature="Collection findEnabledBaAll(java.lang.Integer BA_AD_BRNCH, java.lang.Integer BA_AD_CMPNY)"
 *             query="SELECT OBJECT(ba) FROM AdBankAccount ba, IN(ba.adBranchBankAccounts)bba WHERE ba.baEnable = 1 AND bba.adBranch.brCode = ?1 AND ba.baAdCompany = ?2 ORDER BY ba.baName"
 *
 * @ejb:finder signature="Collection findEnabledBaAll(java.lang.Integer BA_AD_CMPNY)"
 *             query="SELECT OBJECT(ba) FROM AdBankAccount ba WHERE ba.baAdCompany = ?1"
 *
 * @jboss:query signature="Collection findEnabledBaAll(java.lang.Integer BA_AD_CMPNY)"
 *             query="SELECT OBJECT(ba) FROM AdBankAccount ba WHERE ba.baAdCompany = ?1 ORDER BY ba.baName"
 *
 * @ejb:finder signature="Collection findBaAll(java.lang.Integer BA_AD_CMPNY)"
 *             query="SELECT OBJECT(ba) FROM AdBankAccount WHERE ba.baAdCompany = ?1"
 *
 * @jboss:query signature="Collection findBaAll(java.lang.Integer BA_AD_CMPNY)"
 *             query="SELECT OBJECT(ba) FROM AdBankAccount ba WHERE ba.baAdCompany = ?1 ORDER BY ba.baName"
 *
 * @ejb:finder signature="LocalAdBankAccount findByBaName(java.lang.String BA_NM, java.lang.Integer BA_AD_CMPNY)"
 *             query="SELECT OBJECT(ba) FROM AdBankAccount ba WHERE ba.baName = ?1 AND ba.baAdCompany = ?2"
 * 
 * @ejb:finder signature="Collection findByBaGlCoaCashAccount(java.lang.Integer COA_CODE, java.lang.Integer BA_AD_CMPNY)"
 *             query="SELECT OBJECT(ba) FROM AdBankAccount ba WHERE ba.baCoaGlCashAccount=?1 AND ba.baAdCompany = ?2"
 * 
 * @ejb:finder signature="Collection findByBaGlCoaOnAccountReceipt(java.lang.Integer COA_CODE, java.lang.Integer BA_AD_CMPNY)"
 *             query="SELECT OBJECT(ba) FROM AdBankAccount ba WHERE ba.baCoaGlOnAccountReceipt=?1 AND ba.baAdCompany = ?2"
 * 
 * @ejb:finder signature="Collection findByBaGlCoaUnappliedReceipt(java.lang.Integer COA_CODE, java.lang.Integer BA_AD_CMPNY)"
 *             query="SELECT OBJECT(ba) FROM AdBankAccount ba WHERE ba.baCoaGlUnappliedReceipt=?1 AND ba.baAdCompany = ?2"
 * 
 * @ejb:finder signature="Collection findByBaGlCoaBankChargeAccount(java.lang.Integer COA_CODE, java.lang.Integer BA_AD_CMPNY)"
 *             query="SELECT OBJECT(ba) FROM AdBankAccount ba WHERE ba.baCoaGlBankChargeAccount=?1 AND ba.baAdCompany = ?2"
 * 
 * @ejb:finder signature="Collection findByBaGlCoaClearingAccount(java.lang.Integer COA_CODE, java.lang.Integer BA_AD_CMPNY)"
 *             query="SELECT OBJECT(ba) FROM AdBankAccount ba WHERE ba.baCoaGlClearingAccount=?1 AND ba.baAdCompany = ?2"
 * 
 * @ejb:finder signature="Collection findByBaGlCoaInterestAccount(java.lang.Integer COA_CODE, java.lang.Integer BA_AD_CMPNY)"
 *             query="SELECT OBJECT(ba) FROM AdBankAccount ba WHERE ba.baCoaGlInterestAccount=?1 AND ba.baAdCompany = ?2"
 * 
 * @ejb:finder signature="Collection findByBaGlCoaAdjustmentAccount(java.lang.Integer COA_CODE, java.lang.Integer BA_AD_CMPNY)"
 *             query="SELECT OBJECT(ba) FROM AdBankAccount ba WHERE ba.baCoaGlAdjustmentAccount=?1 AND ba.baAdCompany = ?2"
 * 
 * @ejb:finder signature="Collection findByBaGlCoaCashDiscount(java.lang.Integer COA_CODE, java.lang.Integer BA_AD_CMPNY)"
 *             query="SELECT OBJECT(ba) FROM AdBankAccount ba WHERE ba.baCoaGlCashDiscount=?1 AND ba.baAdCompany = ?2"
 * 
 * @ejb:finder signature="Collection findByBaGlCoaSalesDiscount(java.lang.Integer COA_CODE, java.lang.Integer BA_AD_CMPNY)"
 *             query="SELECT OBJECT(ba) FROM AdBankAccount ba WHERE ba.baCoaGlSalesDiscount=?1 AND ba.baAdCompany = ?2"
 * 
 * @ejb:finder signature="Collection findByBaGlCoaUnappliedCheck(java.lang.Integer COA_CODE, java.lang.Integer BA_AD_CMPNY)"
 *             query="SELECT OBJECT(ba) FROM AdBankAccount ba WHERE ba.baCoaGlUnappliedCheck=?1 AND ba.baAdCompany = ?2"
 * 
 * @ejb:finder signature="Collection findEnabledAndNotCashAccountBaAll(java.lang.Integer BA_AD_BRNCH, java.lang.Integer BA_AD_CMPNY)"
 *             query="SELECT OBJECT(ba) FROM AdBankAccount ba, IN(ba.adBranchBankAccounts)bba WHERE ba.baEnable = 1 AND ba.baIsCashAccount = 0 AND bba.adBranch.brCode = ?1 AND ba.baAdCompany = ?2"
 *
 * @jboss:query signature="Collection findEnabledAndNotCashAccountBaAll(java.lang.Integer BA_AD_BRNCH, java.lang.Integer BA_AD_CMPNY)"
 *             query="SELECT OBJECT(ba) FROM AdBankAccount ba, IN(ba.adBranchBankAccounts)bba WHERE ba.baEnable = 1 AND ba.baIsCashAccount = 0 AND bba.adBranch.brCode = ?1 AND ba.baAdCompany = ?2 ORDER BY ba.baName"
 *
 * @ejb:finder signature="LocalAdBankAccount findByBaNameAndBrCode(java.lang.String BA_NM, java.lang.Integer BR_CODE, java.lang.Integer BA_AD_CMPNY)"
 * 			   query="SELECT OBJECT(ba) FROM AdBankAccount ba, IN(ba.adBranchBankAccounts)bba WHERE ba.baName = ?1 AND bba.adBranch.brCode = ?2 AND ba.baAdCompany = ?3"
 * 
 * @ejb:finder signature="Collection findBaByBaNewAndUpdated(java.lang.Integer BR_CODE, java.lang.Integer AD_CMPNY, char NEW, char UPDATED, char DOWNLOADED_UPDATED)"
 * 			   query="SELECT DISTINCT OBJECT(ba) FROM AdBankAccount ba, IN(ba.adBranchBankAccounts) bba WHERE (bba.bbaDownloadStatus = ?3 OR bba.bbaDownloadStatus = ?4 OR bba.bbaDownloadStatus = ?5) AND bba.adBranch.brCode = ?1 AND bba.bbaAdCompany = ?2"
 * 
 * @ejb:finder signature="Collection findEnabledAndNotCashAccountBaAll(java.lang.Integer BA_AD_CMPNY)"
 *             query="SELECT OBJECT(ba) FROM AdBankAccount ba WHERE ba.baEnable = 1 AND ba.baIsCashAccount = 0 AND ba.baAdCompany = ?1"
 *
 * @jboss:query signature="Collection findEnabledAndNotCashAccountBaAll(java.lang.Integer BA_AD_CMPNY)"
 *             query="SELECT OBJECT(ba) FROM AdBankAccount ba WHERE ba.baEnable = 1 AND ba.baIsCashAccount = 0 AND ba.baAdCompany = ?1 ORDER BY ba.baName"             
 *
 * @ejb:select signature="Collection ejbSelectGeneric(java.lang.String jbossQl, java.lang.Object[] args)"
 *             result-type-mapping="Local"
 *             method-intf="LocalHome"
 *             query="SELECT OBJECT(ba) FROM AdBankAccount ba"
 *
 * @jboss:query signature="Collection ejbSelectGeneric(java.lang.String jbossQl, java.lang.Object[] args)"
 *                         dynamic="true"
 *
 * @ejb:value-object match="*"
 *             name="AdBankAccount"
 *
 * @jboss:persistence table-name="AD_BNK_ACCNT"
 *
 * @jboss:entity-command name="mysql-get-generated-keys"
 */

public abstract class AdBankAccountBean extends AbstractEntityBean {
    

    // PERSISTENT METHODS
    
    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     * @ejb:pk-field
     *
     * @jboss:column-name name="BA_CODE"
     *
     * @jboss:persistence auto-increment="true"
     **/
    public abstract Integer getBaCode();
    public abstract void setBaCode(Integer BA_CODE);
    
    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="BA_NM"
     **/
    public abstract String getBaName();
     /**
      * @ejb:interface-method view-type="local"
      **/
    public abstract void setBaName(String BA_NM);
    
    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="BA_DESC"
     **/
    public abstract String getBaDescription();
     /**
      * @ejb:interface-method view-type="local"
      **/
    public abstract void setBaDescription(String BA_DESC);
    
    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="BA_ACCNT_TYP"
     **/
    public abstract String getBaAccountType();
     /**
      * @ejb:interface-method view-type="local"
      **/
    public abstract void setBaAccountType(String BA_ACCNT_TYP);
    
    
    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="BA_ACCNT_NMBR"
     **/
    public abstract String getBaAccountNumber();
     /**
      * @ejb:interface-method view-type="local"
      **/
    public abstract void setBaAccountNumber(String BA_ACCNT_NMBR);
    
    
    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="BA_ACCNT_USE"
     **/
    public abstract String getBaAccountUse();
     /**
      * @ejb:interface-method view-type="local"
      **/
    public abstract void setBaAccountUse(String BA_ACCNT_USE);
    
    
    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="BA_COA_GL_CSH_ACCNT"
     **/
    public abstract Integer getBaCoaGlCashAccount();
     /**
      * @ejb:interface-method view-type="local"
      **/
    public abstract void setBaCoaGlCashAccount(Integer BA_COA_GL_CSH_ACCNT);
    
    
    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="BA_COA_GL_ON_ACCNT_RCPT"
     **/
    public abstract Integer getBaCoaGlOnAccountReceipt();
     /**
      * @ejb:interface-method view-type="local"
      **/
    public abstract void setBaCoaGlOnAccountReceipt(Integer BA_COA_GL_ON_ACCNT_RCPT);
    
    
    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="BA_COA_GL_UNPPLD_RCPT"
     **/
    public abstract Integer getBaCoaGlUnappliedReceipt();
     /**
      * @ejb:interface-method view-type="local"
      **/
    public abstract void setBaCoaGlUnappliedReceipt(Integer BA_COA_GL_UNPPLD_RCPT);
    
    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="BA_COA_GL_UNPPLD_CHK"
     **/
    public abstract Integer getBaCoaGlUnappliedCheck();
     /**
      * @ejb:interface-method view-type="local"
      **/
    public abstract void setBaCoaGlUnappliedCheck(Integer BA_COA_GL_UNPPLD_CHK);    
    
    
    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="BA_COA_GL_BNK_CHRG_ACCNT"
     **/
    public abstract Integer getBaCoaGlBankChargeAccount();
     /**
      * @ejb:interface-method view-type="local"
      **/
    public abstract void setBaCoaGlBankChargeAccount(Integer BA_COA_GL_BNK_CHRG_ACCNT);
    
    
    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="BA_COA_GL_CLRNG_ACCNT"
     **/
    public abstract Integer getBaCoaGlClearingAccount();
     /**
      * @ejb:interface-method view-type="local"
      **/
    public abstract void setBaCoaGlClearingAccount(Integer BA_COA_GL_CLRNG_ACCNT);
    
    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="BA_COA_GL_INTRST_ACCNT"
     **/
    public abstract Integer getBaCoaGlInterestAccount();
     /**
      * @ejb:interface-method view-type="local"
      **/
    public abstract void setBaCoaGlInterestAccount(Integer BA_COA_GL_INTRST_ACCNT);
    
    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="BA_COA_GL_ADJSTMNT_ACCNT"
     **/
    public abstract Integer getBaCoaGlAdjustmentAccount();
     /**
      * @ejb:interface-method view-type="local"
      **/
    public abstract void setBaCoaGlAdjustmentAccount(Integer BA_COA_GL_ADJSTMNT_ACCNT);
    
    
    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="BA_COA_GL_CSH_DSCNT"
     **/	
    public abstract Integer getBaCoaGlCashDiscount();
     /**
      * @ejb:interface-method view-type="local"
      **/
    public abstract void setBaCoaGlCashDiscount(Integer BA_COA_GL_CSH_DSCNT);
    
    
    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="BA_COA_GL_SLS_DSCNT"
     **/
    public abstract Integer getBaCoaGlSalesDiscount();
     /**
      * @ejb:interface-method view-type="local"
      **/
    public abstract void setBaCoaGlSalesDiscount(Integer BA_COA_GL_SLS_DSCNT);
    
    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="BA_COA_GL_ADVNC_ACCNT"
     **/
    public abstract Integer getBaCoaGlAdvanceAccount();
     /**
      * @ejb:interface-method view-type="local"
      **/
    public abstract void setBaCoaGlAdvanceAccount(Integer BA_COA_GL_ADVNC_ACCNT);
    
    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="BA_FLT_BLNC"
     **/
    public abstract double getBaFloatBalance();
     /**
      * @ejb:interface-method view-type="local"
      **/
    public abstract void setBaFloatBalance(double BA_FLT_BLNC);
    
    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="BA_LST_RCNCLD_DT"
     **/
    public abstract Date getBaLastReconciledDate();
     /**
      * @ejb:interface-method view-type="local"
      **/
    public abstract void setBaLastReconciledDate(Date BA_LST_RCNCLD_DT);
    
    
    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="BA_LST_RCNCLD_BLNC"
     **/
    public abstract double getBaLastReconciledBalance();
     /**
      * @ejb:interface-method view-type="local"
      **/
    public abstract void setBaLastReconciledBalance(double BA_LST_RCNCLD_BLNC);
    
    
    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="BA_NXT_CHK_NMBR"
     **/
    public abstract String getBaNextCheckNumber();
     /**
      * @ejb:interface-method view-type="local"
      **/
    public abstract void setBaNextCheckNumber(String BA_NXT_CHK_NMBR);
    
    
    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="BA_ENBL"
     **/
    public abstract byte getBaEnable();
     /**
      * @ejb:interface-method view-type="local"
      **/
    public abstract void setBaEnable(byte BA_ENBL);
    
    
    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="BA_ACCNT_NMBR_SHW"
     **/
    public abstract byte getBaAccountNumberShow();
     /**
      * @ejb:interface-method view-type="local"
      **/
    public abstract void setBaAccountNumberShow(byte BA_ACCNT_NMBR_SHW);
    
    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="BA_ACCNT_NMBR_TP"
     **/
    public abstract int getBaAccountNumberTop();
     /**
      * @ejb:interface-method view-type="local"
      **/
    public abstract void setBaAccountNumberTop(int BA_ACCNT_NMBR_TP);
    
        
    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="BA_ACCNT_NMBR_LFT"
     **/
    public abstract int getBaAccountNumberLeft();
     /**
      * @ejb:interface-method view-type="local"
      **/
    public abstract void setBaAccountNumberLeft(int BA_ACCNT_NMBR_LFT);
    
    
    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="BA_ACCNT_NM_SHW"
     **/
    public abstract byte getBaAccountNameShow();
     /**
      * @ejb:interface-method view-type="local"
      **/
    public abstract void setBaAccountNameShow(byte BA_ACCNT_NM_SHW);
    
    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="BA_ACCNT_NM_TP"
     **/
    public abstract int getBaAccountNameTop();
     /**
      * @ejb:interface-method view-type="local"
      **/
    public abstract void setBaAccountNameTop(int BA_ACCNT_NM_TP);
    
        
    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="BA_ACCNT_NM_LFT"
     **/
    public abstract int getBaAccountNameLeft();
     /**
      * @ejb:interface-method view-type="local"
      **/
    public abstract void setBaAccountNameLeft(int BA_ACCNT_NM_LFT);
    
    
    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="BA_NMBR_SHW"
     **/
    public abstract byte getBaNumberShow();
     /**
      * @ejb:interface-method view-type="local"
      **/
    public abstract void setBaNumberShow(byte BA_NMBR_SHW);
    
    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="BA_NMBR_TP"
     **/
    public abstract int getBaNumberTop();
     /**
      * @ejb:interface-method view-type="local"
      **/
    public abstract void setBaNumberTop(int BA_NMBR_TP);
    
        
    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="BA_NMBR_LFT"
     **/
    public abstract int getBaNumberLeft();
     /**
      * @ejb:interface-method view-type="local"
      **/
    public abstract void setBaNumberLeft(int BA_NMBR_LFT);
    
    
    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="BA_DT_SHW"
     **/
    public abstract byte getBaDateShow();
     /**
      * @ejb:interface-method view-type="local"
      **/
    public abstract void setBaDateShow(byte BA_DT_SHW);
    
    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="BA_DT_TP"
     **/
    public abstract int getBaDateTop();
     /**
      * @ejb:interface-method view-type="local"
      **/
    public abstract void setBaDateTop(int BA_DT_TP);
    
        
    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="BA_DT_LFT"
     **/
    public abstract int getBaDateLeft();
     /**
      * @ejb:interface-method view-type="local"
      **/
    public abstract void setBaDateLeft(int BA_DT_LFT);
    
    
    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="BA_PY_SHW"
     **/
    public abstract byte getBaPayeeShow();
     /**
      * @ejb:interface-method view-type="local"
      **/
    public abstract void setBaPayeeShow(byte BA_PY_SHW);
    
    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="BA_PY_TP"
     **/
    public abstract int getBaPayeeTop();
     /**
      * @ejb:interface-method view-type="local"
      **/
    public abstract void setBaPayeeTop(int BA_PY_TP);
    
        
    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="BA_PY_LFT"
     **/
    public abstract int getBaPayeeLeft();
     /**
      * @ejb:interface-method view-type="local"
      **/
    public abstract void setBaPayeeLeft(int BA_PY_LFT);
    
    
    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="BA_AMNT_SHW"
     **/
    public abstract byte getBaAmountShow();
     /**
      * @ejb:interface-method view-type="local"
      **/
    public abstract void setBaAmountShow(byte BA_AMNT_SHW);
    
    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="BA_AMNT_TP"
     **/
    public abstract int getBaAmountTop();
     /**
      * @ejb:interface-method view-type="local"
      **/
    public abstract void setBaAmountTop(int BA_AMNT_TP);
    
        
    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="BA_AMNT_LFT"
     **/
    public abstract int getBaAmountLeft();
     /**
      * @ejb:interface-method view-type="local"
      **/
    public abstract void setBaAmountLeft(int BA_AMNT_LFT);
    
    
    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="BA_WRD_AMNT_SHW"
     **/
    public abstract byte getBaWordAmountShow();
     /**
      * @ejb:interface-method view-type="local"
      **/
    public abstract void setBaWordAmountShow(byte BA_WRD_AMNT_SHW);
    
    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="BA_WRD_AMNT_TP"
     **/
    public abstract int getBaWordAmountTop();
     /**
      * @ejb:interface-method view-type="local"
      **/
    public abstract void setBaWordAmountTop(int BA_WRD_AMNT_TP);
    
        
    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="BA_WRD_AMNT_LFT"
     **/
    public abstract int getBaWordAmountLeft();
     /**
      * @ejb:interface-method view-type="local"
      **/
    public abstract void setBaWordAmountLeft(int BA_WRD_AMNT_LFT);
    
    
    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="BA_CRRNCY_SHW"
     **/
    public abstract byte getBaCurrencyShow();
     /**
      * @ejb:interface-method view-type="local"
      **/
    public abstract void setBaCurrencyShow(byte BA_CRRNCY_SHW);
    
    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="BA_CRRNCY_TP"
     **/
    public abstract int getBaCurrencyTop();
     /**
      * @ejb:interface-method view-type="local"
      **/
    public abstract void setBaCurrencyTop(int BA_CRRNCY_TP);
    
        
    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="BA_CRRNCY_LFT"
     **/
    public abstract int getBaCurrencyLeft();
     /**
      * @ejb:interface-method view-type="local"
      **/
    public abstract void setBaCurrencyLeft(int BA_CRRNCY_LFT);
    
    
    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="BA_ADDRSS_SHW"
     **/
    public abstract byte getBaAddressShow();
     /**
      * @ejb:interface-method view-type="local"
      **/
    public abstract void setBaAddressShow(byte BA_ADDRSS_SHW);
    
    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="BA_ADDRSS_TP"
     **/
    public abstract int getBaAddressTop();
     /**
      * @ejb:interface-method view-type="local"
      **/
    public abstract void setBaAddressTop(int BA_ADDRSS_TP);
    
        
    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="BA_ADDRSS_LFT"
     **/
    public abstract int getBaAddressLeft();
     /**
      * @ejb:interface-method view-type="local"
      **/
    public abstract void setBaAddressLeft(int BA_ADDRSS_LFT);
    
    
    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="BA_MM_SHW"
     **/
    public abstract byte getBaMemoShow();
     /**
      * @ejb:interface-method view-type="local"
      **/
    public abstract void setBaMemoShow(byte BA_MM_SHW);
    
    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="BA_MM_TP"
     **/
    public abstract int getBaMemoTop();
     /**
      * @ejb:interface-method view-type="local"
      **/
    public abstract void setBaMemoTop(int BA_MM_TP);
    
        
    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="BA_MM_LFT"
     **/
    public abstract int getBaMemoLeft();
     /**
      * @ejb:interface-method view-type="local"
      **/
    public abstract void setBaMemoLeft(int BA_MM_LFT);
    

    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="BA_DC_NMBR_SHW"
     **/
    public abstract byte getBadocNumberShow();
     /**
      * @ejb:interface-method view-type="local"
      **/
    public abstract void setBadocNumberShow(byte BA_DC_NMBR_SHW);
    
    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="BA_DC_NMBR_TP"
     **/
    public abstract int getBadocNumberTop();
     /**
      * @ejb:interface-method view-type="local"
      **/
    public abstract void setBadocNumberTop(int BA_DC_NMBR_TP);
    
        
    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="BA_DC_NMBR_LFT"
     **/
    public abstract int getBadocNumberLeft();
     /**
      * @ejb:interface-method view-type="local"
      **/
    public abstract void setBadocNumberLeft(int BA_DC_NMBR_LFT);
    
    
    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="BA_FNT_SZ"
     **/
    public abstract int getBaFontSize();
     /**
      * @ejb:interface-method view-type="local"
      **/
    public abstract void setBaFontSize(int BA_FNT_SZ);
    
    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="BA_FNT_STYL"
     **/
    public abstract String getBaFontStyle();
     /**
      * @ejb:interface-method view-type="local"
      **/
    public abstract void setBaFontStyle(String BA_FNT_STYL);
    
    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="BA_IS_CSH_ACCNT"
     **/
    public abstract byte getBaIsCashAccount();
     /**
      * @ejb:interface-method view-type="local"
      **/
    public abstract void setBaIsCashAccount(byte BA_IS_CSH_ACCNT);
    
    
    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="BA_AD_CMPNY"
     **/
    public abstract Integer getBaAdCompany();
     /**
      * @ejb:interface-method view-type="local"
      **/
    public abstract void setBaAdCompany(Integer BA_AD_CMPNY);
    	   
    
    // RELATIONSHIP METHODS
    
    /**
     * @ejb:interface-method view-type="local"
     * @ejb:relation name="bank-bankaccounts"
     *               role-name="bankaccount-has-one-bank"
     * 
     * @jboss:relation related-pk-field="bnkCode"
     *                 fk-column="AD_BANK"
     */
     public abstract LocalAdBank getAdBank();
     public abstract void setAdBank(LocalAdBank adBank);
     
     
     /**
     * @ejb:interface-method view-type="local"
     * @ejb:relation name="functionalcurrency-bankaccounts"
     *               role-name="bankaccount-has-one-functionalcurrency"
     * 
     * @jboss:relation related-pk-field="fcCode"
     *                 fk-column="GL_FUNCTIONAL_CURRENCY"
     */
     public abstract com.ejb.gl.LocalGlFunctionalCurrency getGlFunctionalCurrency();
     public abstract void setGlFunctionalCurrency(com.ejb.gl.LocalGlFunctionalCurrency glFunctionalCurrency);
                   
     /**
     * @ejb:interface-method view-type="local"
     * @ejb:relation name="bankaccount-adjustments"
     *               role-name="bankaccount-has-many-adjustments"
     * 
     */
     public abstract Collection getCmAdjustments();
     public abstract void setCmAdjustments(Collection cmAdjustments);

    /**
     * @ejb:interface-method view-type="local"
     * @ejb:relation name="bankaccount-suppliertypes"
     *               role-name="bankaccount-has-many-suppliertypes"
     * 
     */
     public abstract Collection getApSupplierTypes();
     public abstract void setApSupplierTypes(Collection apSupplierTypes);

    /**
     * @ejb:interface-method view-type="local"
     * @ejb:relation name="bankaccount-checks"
     *               role-name="bankaccount-has-many-checks"
     * 
     */
     public abstract Collection getApChecks();
     public abstract void setApChecks(Collection apChecks);

    /**
     * @ejb:interface-method view-type="local"
     * @ejb:relation name="bankaccount-customertypes"
     *               role-name="bankaccount-has-many-customertypes"
     * 
     */
     public abstract Collection getArCustomerTypes();
     public abstract void setArCustomerTypes(Collection arCustomerTypes);

    /**
     * @ejb:interface-method view-type="local"
     * @ejb:relation name="bankaccount-receipts"
     *               role-name="bankaccount-has-many-receipts"
     * 
     */
     public abstract Collection getArReceipts();
     public abstract void setArReceipts(Collection arReceipts);
     
     /**
      * @ejb:interface-method view-type="local"
      * @ejb:relation name="bankaccountcard1-receipts"
      *               role-name="bankaccount-has-many-receipts"
      * 
      */
      public abstract Collection getArCard1Receipts();
      public abstract void setArCard1Receipts(Collection arReceipts);
      
      /**
       * @ejb:interface-method view-type="local"
       * @ejb:relation name="bankaccountcard2-receipts"
       *               role-name="bankaccount-has-many-receipts"
       * 
       */
       public abstract Collection getArCard2Receipts();
       public abstract void setArCard2Receipts(Collection arReceipts);
       
       /**
        * @ejb:interface-method view-type="local"
        * @ejb:relation name="bankaccountcard3-receipts"
        *               role-name="bankaccount-has-many-receipts"
        * 
        */
        public abstract Collection getArCard3Receipts();
        public abstract void setArCard3Receipts(Collection arReceipts);
     
     /**
      * @ejb:interface-method view-type="local"
      * @ejb:relation name="bankaccount-pdcs"
      *               role-name="bankaccount-has-many-pdcs"
      * 
      */
      public abstract Collection getArPdcs();
      public abstract void setArPdcs(Collection arPdcs);
     
     
     /**
      * @ejb:interface-method view-type="local"
      * @ejb:relation name="bankaccount-suppliers"
      *               role-name="bankaccount-has-many-suppliers"
      * 
      */
      public abstract Collection getApSuppliers();
      public abstract void setApSuppliers(Collection apSuppliers);
      
      /**
       * @ejb:interface-method view-type="local"
       * @ejb:relation name="bankaccount-customers"
       *               role-name="bankaccount-has-many-customers"
       * 
       */
       public abstract Collection getArCustomers();
       public abstract void setArCustomers(Collection arCustomers);
       
       /**
        * @ejb:interface-method view-type="local"
        * @ejb:relation name="bankaccount-branchbankaccount"
        *               role-name="bankaccount-has-many-branchbankaccount"
        */
       public abstract Collection getAdBranchBankAccounts();
       public abstract void setAdBranchBankAccounts(Collection adBranchBankAccounts); 

       /**
        * @ejb:interface-method view-type="local"
        * @ejb:relation name="bankaccount-bankaccountbalances"
        *               role-name="bankaccount-has-many-bankaccountbalances"
        * 
        */
       public abstract Collection getAdBankAccountBalances();
       public abstract void setAdBankAccountBalances(Collection adBankAccountBalances);
       
       /**
        * @jboss:dynamic-ql
        */
        public abstract Collection ejbSelectGeneric(java.lang.String jbossQl, java.lang.Object[] args)
           throws FinderException;
           
        

       // BUSINESS METHODS
       
       /**
        * @ejb:home-method view-type="local"
        */
        public Collection ejbHomeGetBaByCriteria(java.lang.String jbossQl, java.lang.Object[] args)
           throws FinderException {
           	
           return ejbSelectGeneric(jbossQl, args);
        }
    
        
     /**
     * @ejb:interface-method view-type="local"
     **/
     public void addCmAdjustment(com.ejb.cm.LocalCmAdjustment cmAdjustment) {

         Debug.print("AdBankAccountBean addCmAdjustment");
      
         try {
      	
            Collection cmAdjustments = getCmAdjustments();
	        cmAdjustments.add(cmAdjustment);
	         
         } catch (Exception ex) {
      	
           throw new EJBException(ex.getMessage());
      
         }   
     }

    /**
     * @ejb:interface-method view-type="local"
     **/
     public void dropCmAdjustment(com.ejb.cm.LocalCmAdjustment cmAdjustment) {

         Debug.print("AdBankAccountBean dropCmAdjustment");
      
         try {
      	
            Collection cmAdjustments = getCmAdjustments();
	        cmAdjustments.remove(cmAdjustment);
	      
         } catch (Exception ex) {
      	
           throw new EJBException(ex.getMessage());
         
         }   
     }

   /**
     * @ejb:interface-method view-type="local"
     **/
    public void addApSupplierType(com.ejb.ap.LocalApSupplierType apSupplierType) {

         Debug.print("AdBankAccountBean addApSupplierType");
      
         try {
      	
            Collection apSupplierTypes = getApSupplierTypes();
	        apSupplierTypes.add(apSupplierType);
	          
         } catch (Exception ex) {
      	
           throw new EJBException(ex.getMessage());
      
         }   
     }

    /**
     * @ejb:interface-method view-type="local"
     **/
    public void dropApSupplierType(com.ejb.ap.LocalApSupplierType apSupplierType) {

         Debug.print("AdBankAccountBean dropApSupplierType");
      
         try {
      	
            Collection apSupplierTypes = getApSupplierTypes();
	        apSupplierTypes.remove(apSupplierType);
	      
         } catch (Exception ex) {
      	
           throw new EJBException(ex.getMessage());
         
         } 
           
     }

   /**
     * @ejb:interface-method view-type="local"
     **/
    public void addApCheck(com.ejb.ap.LocalApCheck apCheck) {

         Debug.print("AdBankAccountBean addApCheck");
      
         try {
      	
            Collection apChecks = getApChecks();
	        apChecks.add(apCheck);
	          
         } catch (Exception ex) {
      	
           throw new EJBException(ex.getMessage());
      
         }   
     }

    /**
     * @ejb:interface-method view-type="local"
     **/
    public void dropApCheck(com.ejb.ap.LocalApCheck apCheck) {

         Debug.print("AdBankAccountBean dropApCheck");
      
         try {
      	
            Collection apChecks = getApChecks();
	        apChecks.remove(apCheck);
	      
         } catch (Exception ex) {
      	
           throw new EJBException(ex.getMessage());
         
         } 
           
     }
     

   /**
     * @ejb:interface-method view-type="local"
     **/
    public void addArCustomerType(com.ejb.ar.LocalArCustomerType arCustomerType) {

         Debug.print("AdBankAccountBean addArCustomerType");
      
         try {
      	
            Collection arCustomerTypes = getArCustomerTypes();
	        arCustomerTypes.add(arCustomerType);
	          
         } catch (Exception ex) {
      	
           throw new EJBException(ex.getMessage());
      
         }   
     }

    /**
     * @ejb:interface-method view-type="local"
     **/
    public void dropArCustomerType(com.ejb.ar.LocalArCustomerType arCustomerType) {

         Debug.print("AdBankAccountBean dropArCustomerType");
      
         try {
      	
            Collection arCustomerTypes = getArCustomerTypes();
	        arCustomerTypes.remove(arCustomerType);
	      
         } catch (Exception ex) {
      	
           throw new EJBException(ex.getMessage());
         
         } 
           
     }

   /**
     * @ejb:interface-method view-type="local"
     **/
    public void addArReceipt(com.ejb.ar.LocalArReceipt arReceipt) {

         Debug.print("AdBankAccountBean addArReceipt");
      
         try {
      	
            Collection arReceipts = getArReceipts();
	        arReceipts.add(arReceipt);
	          
         } catch (Exception ex) {
      	
           throw new EJBException(ex.getMessage());
      
         }   
     }

    /**
     * @ejb:interface-method view-type="local"
     **/
    public void dropArReceipt(com.ejb.ar.LocalArReceipt arReceipt) {

         Debug.print("AdBankAccountBean dropArReceipt");
      
         try {
      	
            Collection arReceipts = getArReceipts();
	        arReceipts.remove(arReceipt);
	      
         } catch (Exception ex) {
      	
           throw new EJBException(ex.getMessage());
         
         } 
           
     }
    
    
    /**
     * @ejb:interface-method view-type="local"
     **/
    public void addApSupplier(com.ejb.ap.LocalApSupplier apSupplier) {

         Debug.print("AdBankAccountBean addArSupplier");
      
         try {
      	
            Collection apSuppliers = getApSuppliers();
	        apSuppliers.add(apSupplier);
	          
         } catch (Exception ex) {
      	
           throw new EJBException(ex.getMessage());
      
         }   
     }

    /**
     * @ejb:interface-method view-type="local"
     **/
    public void dropApSupplier(com.ejb.ap.LocalApSupplier apSupplier) {

         Debug.print("AdBankAccountBean dropApSupplier");
      
         try {
      	
            Collection apSuppliers = getApSuppliers();
	        apSuppliers.remove(apSupplier);
	      
         } catch (Exception ex) {
      	
           throw new EJBException(ex.getMessage());
         
         } 
           
     }
    
     /**
     * @ejb:interface-method view-type="local"
     **/
    public void addArCustomer(com.ejb.ar.LocalArCustomer arCustomer) {

         Debug.print("AdBankAccountBean addArSupplier");
      
         try {
      	
            Collection arCustomers = getArCustomers();
	        arCustomers.add(arCustomer);
	          
         } catch (Exception ex) {
      	
           throw new EJBException(ex.getMessage());
      
         }   
     }

    /**
     * @ejb:interface-method view-type="local"
     **/
    public void dropArCustomer(com.ejb.ar.LocalArCustomer arCustomer) {

         Debug.print("AdBankAccountBean dropArCustomer");
      
         try {
      	
            Collection arCustomers = getArCustomers();
	        arCustomers.remove(arCustomer);
	      
         } catch (Exception ex) {
      	
           throw new EJBException(ex.getMessage());
         
         } 
           
     }
    
    /**
     * @ejb:interface-method view-type="local"
     **/
    public void addAdBranchBankAccount(LocalAdBranchBankAccount adBranchBankAccount) {

         Debug.print("AdBankAccountBean addAdBranchBankAccount");
   
         try {
   	
	        Collection adBranchBankAccounts = getAdBranchBankAccounts();
	        adBranchBankAccounts.add(adBranchBankAccount);
 	     
         } catch (Exception ex) {
   	
             throw new EJBException(ex.getMessage());
      
         }
       
    }
 	
    /**
     * @ejb:interface-method view-type="local"
     **/ 
     public void dropAdBranchBankAccount(LocalAdBranchBankAccount adBranchBankAccount) {
 		
 		  Debug.print("AdBankAccountBean dropAdBranchBankAccount");
 		  
 		  try {
 		  	
 		     Collection adBranchBankAccounts = getAdBranchBankAccounts();
 		     adBranchBankAccounts.remove(adBranchBankAccount);
 			     
 		  } catch (Exception ex) {
 		  	
 		     throw new EJBException(ex.getMessage());
 		     
 		  }
 		  
    }
     
     /**
      * @ejb:interface-method view-type="local"
      **/
     public void addAdBankAccountBalance(LocalAdBankAccountBalance adBankAccountBalance) {

          Debug.print("AdBankAccountBean addAdBankAccountBalance");
    
          try {
    	
 	        Collection adBankAccountBalances = getAdBankAccountBalances();
 	        adBankAccountBalances.add(adBankAccountBalance);
  	     
          } catch (Exception ex) {
    	
              throw new EJBException(ex.getMessage());
       
          }
        
     }
  	
     /**
      * @ejb:interface-method view-type="local"
      **/ 
      public void dropAdBankAccountBalance(LocalAdBankAccountBalance adBankAccountBalance) {
  		
  		  Debug.print("AdBankAccountBean dropAdBankAccountBalance");
  		  
  		  try {
  		  	
  		     Collection adBankAccountBalances = getAdBankAccountBalances();
  		     adBankAccountBalances.remove(adBankAccountBalance);
  			     
  		  } catch (Exception ex) {
  		  	
  		     throw new EJBException(ex.getMessage());
  		     
  		  }
  		  
     }
      
      /**
       * @ejb:interface-method view-type="local"
       **/
      public void addArPdc(com.ejb.ar.LocalArPdc arPdc) {

           Debug.print("AdBankAccountBean addArPdc");
        
           try {
        	
              Collection arPdcs = getArPdcs();
  	        arPdcs.add(arPdc);
  	          
           } catch (Exception ex) {
        	
             throw new EJBException(ex.getMessage());
        
           }   
       }

      /**
       * @ejb:interface-method view-type="local"
       **/
      public void dropArPdc(com.ejb.ar.LocalArPdc arPdc) {

           Debug.print("AdBankAccountBean dropArPdc");
        
           try {
        	
              Collection arPdcs = getArPdcs();
  	        arPdcs.remove(arPdc);
  	      
           } catch (Exception ex) {
        	
             throw new EJBException(ex.getMessage());
           
           } 
             
       }
      
      

     // ENTITY METHODS
    
    /**
     * @ejb:create-method view-type="local"
     **/
     public Integer ejbCreate(Integer BA_CODE, String BA_NM, String BA_DESC,
         String BA_ACCNT_TYP, String BA_ACCNT_NMBR, String BA_ACCNT_USE,
         Integer BA_COA_GL_CSH_ACCNT, Integer BA_COA_GL_ON_ACCNT_RCPT,
         Integer BA_COA_GL_UNPPLD_RCPT, Integer BA_COA_GL_BNK_CHRG_ACCNT,
         Integer BA_COA_GL_CLRNG_ACCNT, Integer BA_COA_GL_INTRST_ACCNT,
         Integer BA_COA_GL_ADJSTMNT_ACCNT, Integer BA_COA_GL_CSH_DSCNT,
         Integer BA_COA_GL_SLS_DSCNT, Integer BA_COA_GL_ADVNC_ACCNT,
         Integer BA_COA_GL_UNPPLD_CHK, double BA_FLT_BLNC,
         Date BA_LST_RCNCLD_DT, double BA_LST_RCNCLD_BLNC,
         String BA_NXT_CHK_NMBR, byte BA_ENBL, byte BA_ACCNT_NMBR_SHW, int BA_ACCNT_NMBR_TP,
         int BA_ACCNT_NMBR_LFT, byte BA_ACCNT_NM_SHW, int BA_ACCNT_NM_TP, int BA_ACCNT_NM_LFT,
         byte BA_NMBR_SHW, int BA_NMBR_TP, int BA_NMBR_LFT,
         byte BA_DT_SHW, int BA_DT_TP, int BA_DT_LFT,
         byte BA_PY_SHW, int BA_PY_TP, int BA_PY_LFT,
         byte BA_AMNT_SHW, int BA_AMNT_TP, int BA_AMNT_LFT,
         byte BA_WRD_AMNT_SHW, int BA_WRD_AMNT_TP, int BA_WRD_AMNT_LFT,
         byte BA_CRRNCY_SHW, int BA_CRRNCY_TP, int BA_CRRNCY_LFT,
         byte BA_ADDRSS_SHW, int BA_ADDRSS_TP, int BA_ADDRSS_LFT,
         byte BA_MM_SHW, int BA_MM_TP, int BA_MM_LFT,
         byte BA_DC_NMBR_SHW, int BA_DC_NMBR_TP, int BA_DC_NMBR_LFT, int BA_FNT_SZ, String BA_FNT_STYL, byte BA_IS_CSH_ACCNT,
		 Integer BA_AD_CMPNY)
         throws CreateException {
      	
         Debug.print("AdBankAccountBean ejbCreate");
      
         setBaCode(BA_CODE);
         setBaName(BA_NM);
         setBaDescription(BA_DESC);
         setBaAccountType(BA_ACCNT_TYP);
         setBaAccountNumber(BA_ACCNT_NMBR);
         setBaAccountUse(BA_ACCNT_USE);
         setBaCoaGlCashAccount(BA_COA_GL_CSH_ACCNT);
         setBaCoaGlOnAccountReceipt(BA_COA_GL_ON_ACCNT_RCPT);
         setBaCoaGlUnappliedReceipt(BA_COA_GL_UNPPLD_RCPT);
         setBaCoaGlBankChargeAccount(BA_COA_GL_BNK_CHRG_ACCNT);
         setBaCoaGlClearingAccount(BA_COA_GL_CLRNG_ACCNT);
         setBaCoaGlInterestAccount(BA_COA_GL_INTRST_ACCNT);
         setBaCoaGlAdjustmentAccount(BA_COA_GL_ADJSTMNT_ACCNT);
         setBaCoaGlCashDiscount(BA_COA_GL_CSH_DSCNT);
         setBaCoaGlSalesDiscount(BA_COA_GL_SLS_DSCNT);
         setBaCoaGlAdvanceAccount(BA_COA_GL_ADVNC_ACCNT);
         setBaCoaGlUnappliedCheck(BA_COA_GL_UNPPLD_CHK);
         setBaFloatBalance(BA_FLT_BLNC);
         setBaLastReconciledDate(BA_LST_RCNCLD_DT);
         setBaLastReconciledBalance(BA_LST_RCNCLD_BLNC);
         setBaNextCheckNumber(BA_NXT_CHK_NMBR);
         setBaEnable(BA_ENBL);
         setBaAccountNumberShow(BA_ACCNT_NMBR_SHW);
         setBaAccountNumberTop(BA_ACCNT_NMBR_TP);
         setBaAccountNumberLeft(BA_ACCNT_NMBR_LFT);
         setBaAccountNameShow(BA_ACCNT_NM_SHW);
         setBaAccountNameTop(BA_ACCNT_NM_TP);
         setBaAccountNameLeft(BA_ACCNT_NM_LFT);
         setBaNumberShow(BA_NMBR_SHW);
         setBaNumberTop(BA_NMBR_TP);
         setBaNumberLeft(BA_NMBR_LFT);
         setBaDateShow(BA_DT_SHW);
         setBaDateTop(BA_DT_TP);
         setBaDateLeft(BA_DT_LFT);
         setBaPayeeShow(BA_PY_SHW);
         setBaPayeeTop(BA_PY_TP);
         setBaPayeeLeft(BA_PY_LFT);
         setBaAmountShow(BA_AMNT_SHW);
         setBaAmountTop(BA_AMNT_TP);
         setBaAmountLeft(BA_AMNT_LFT);
         setBaWordAmountShow(BA_WRD_AMNT_SHW);
         setBaWordAmountTop(BA_WRD_AMNT_TP);
         setBaWordAmountLeft(BA_WRD_AMNT_LFT);
         setBaCurrencyShow(BA_CRRNCY_SHW);
         setBaCurrencyTop(BA_CRRNCY_TP);
         setBaCurrencyLeft(BA_CRRNCY_LFT);
         setBaAddressShow(BA_ADDRSS_SHW);
         setBaAddressTop(BA_ADDRSS_TP);
         setBaAddressLeft(BA_ADDRSS_LFT);
         setBaMemoShow(BA_MM_SHW);
         setBaMemoTop(BA_MM_TP);
         setBaMemoLeft(BA_MM_LFT);    
         setBadocNumberShow(BA_DC_NMBR_SHW);
         setBadocNumberTop(BA_DC_NMBR_TP);
         setBadocNumberLeft(BA_DC_NMBR_LFT); 
         setBaFontSize(BA_FNT_SZ);
         setBaFontStyle(BA_FNT_STYL);
         setBaIsCashAccount(BA_IS_CSH_ACCNT);
         setBaAdCompany(BA_AD_CMPNY);
        
         return null;
     } 
    
    /**
     * @ejb:create-method view-type="local"
     **/
     public Integer ejbCreate(String BA_NM, String BA_DESC,
         String BA_ACCNT_TYP, String BA_ACCNT_NMBR, String BA_ACCNT_USE,
         Integer BA_COA_GL_CSH_ACCNT, Integer BA_COA_GL_ON_ACCNT_RCPT,
         Integer BA_COA_GL_UNPPLD_RCPT, Integer BA_COA_GL_BNK_CHRG_ACCNT,
         Integer BA_COA_GL_CLRNG_ACCNT, Integer BA_COA_GL_INTRST_ACCNT,
         Integer BA_COA_GL_ADJSTMNT_ACCNT, Integer BA_COA_GL_CSH_DSCNT,
         Integer BA_COA_GL_SLS_DSCNT,
         Integer BA_COA_GL_ADVNC_ACCNT,
         Integer BA_COA_GL_UNPPLD_CHK, double BA_FLT_BLNC,
         Date BA_LST_RCNCLD_DT, double BA_LST_RCNCLD_BLNC,
         String BA_NXT_CHK_NMBR, byte BA_ENBL, byte BA_ACCNT_NMBR_SHW, int BA_ACCNT_NMBR_TP,
         int BA_ACCNT_NMBR_LFT, byte BA_ACCNT_NM_SHW, int BA_ACCNT_NM_TP, int BA_ACCNT_NM_LFT,
         byte BA_NMBR_SHW, int BA_NMBR_TP, int BA_NMBR_LFT,
         byte BA_DT_SHW, int BA_DT_TP, int BA_DT_LFT,
         byte BA_PY_SHW, int BA_PY_TP, int BA_PY_LFT,
         byte BA_AMNT_SHW, int BA_AMNT_TP, int BA_AMNT_LFT,
         byte BA_WRD_AMNT_SHW, int BA_WRD_AMNT_TP, int BA_WRD_AMNT_LFT,
         byte BA_CRRNCY_SHW, int BA_CRRNCY_TP, int BA_CRRNCY_LFT,
         byte BA_ADDRSS_SHW, int BA_ADDRSS_TP, int BA_ADDRSS_LFT,
         byte BA_MM_SHW, int BA_MM_TP, int BA_MM_LFT,
         byte BA_DC_NMBR_SHW, int BA_DC_NMBR_TP, int BA_DC_NMBR_LFT, int BA_FNT_SZ, String BA_FNT_STYL, byte BA_IS_CSH_ACCNT,
		 Integer BA_AD_CMPNY)
         throws CreateException {
      	
         Debug.print("AdBankAccountBean ejbCreate");
                    
         setBaName(BA_NM);
         setBaDescription(BA_DESC);
         setBaAccountType(BA_ACCNT_TYP);
         setBaAccountNumber(BA_ACCNT_NMBR);
         setBaAccountUse(BA_ACCNT_USE);
         setBaCoaGlCashAccount(BA_COA_GL_CSH_ACCNT);
         setBaCoaGlOnAccountReceipt(BA_COA_GL_ON_ACCNT_RCPT);
         setBaCoaGlUnappliedReceipt(BA_COA_GL_UNPPLD_RCPT);
         setBaCoaGlBankChargeAccount(BA_COA_GL_BNK_CHRG_ACCNT);
         setBaCoaGlClearingAccount(BA_COA_GL_CLRNG_ACCNT);
         setBaCoaGlInterestAccount(BA_COA_GL_INTRST_ACCNT);
         setBaCoaGlAdjustmentAccount(BA_COA_GL_ADJSTMNT_ACCNT);
         setBaCoaGlCashDiscount(BA_COA_GL_CSH_DSCNT);
         setBaCoaGlSalesDiscount(BA_COA_GL_SLS_DSCNT);
         setBaCoaGlAdvanceAccount(BA_COA_GL_ADVNC_ACCNT);
         setBaCoaGlUnappliedCheck(BA_COA_GL_UNPPLD_CHK);         	                  
         setBaFloatBalance(BA_FLT_BLNC);
         setBaLastReconciledDate(BA_LST_RCNCLD_DT);
         setBaLastReconciledBalance(BA_LST_RCNCLD_BLNC);
         setBaNextCheckNumber(BA_NXT_CHK_NMBR);
         setBaEnable(BA_ENBL);
         setBaAccountNumberShow(BA_ACCNT_NMBR_SHW);
         setBaAccountNumberTop(BA_ACCNT_NMBR_TP);
         setBaAccountNumberLeft(BA_ACCNT_NMBR_LFT);
         setBaAccountNameShow(BA_ACCNT_NM_SHW);
         setBaAccountNameTop(BA_ACCNT_NM_TP);
         setBaAccountNameLeft(BA_ACCNT_NM_LFT);
         setBaNumberShow(BA_NMBR_SHW);
         setBaNumberTop(BA_NMBR_TP);
         setBaNumberLeft(BA_NMBR_LFT);
         setBaDateShow(BA_DT_SHW);
         setBaDateTop(BA_DT_TP);
         setBaDateLeft(BA_DT_LFT);
         setBaPayeeShow(BA_PY_SHW);
         setBaPayeeTop(BA_PY_TP);
         setBaPayeeLeft(BA_PY_LFT);
         setBaAmountShow(BA_AMNT_SHW);
         setBaAmountTop(BA_AMNT_TP);
         setBaAmountLeft(BA_AMNT_LFT);
         setBaWordAmountShow(BA_WRD_AMNT_SHW);
         setBaWordAmountTop(BA_WRD_AMNT_TP);
         setBaWordAmountLeft(BA_WRD_AMNT_LFT);
         setBaCurrencyShow(BA_CRRNCY_SHW);
         setBaCurrencyTop(BA_CRRNCY_TP);
         setBaCurrencyLeft(BA_CRRNCY_LFT);
         setBaAddressShow(BA_ADDRSS_SHW);
         setBaAddressTop(BA_ADDRSS_TP);
         setBaAddressLeft(BA_ADDRSS_LFT);
         setBaMemoShow(BA_MM_SHW);
         setBaMemoTop(BA_MM_TP);
         setBaMemoLeft(BA_MM_LFT);
         setBadocNumberShow(BA_DC_NMBR_SHW);
         setBadocNumberTop(BA_DC_NMBR_TP);
         setBadocNumberLeft(BA_DC_NMBR_LFT);
         setBaFontSize(BA_FNT_SZ);
         setBaFontStyle(BA_FNT_STYL);
         setBaIsCashAccount(BA_IS_CSH_ACCNT);
         setBaAdCompany(BA_AD_CMPNY);
        
         return null;
     }
    
     public void ejbPostCreate(Integer BA_CODE, String BA_NM, String BA_DESC,
         String BA_ACCNT_TYP, String BA_ACCNT_NMBR, String BA_ACCNT_USE,
         Integer BA_COA_GL_CSH_ACCNT, Integer BA_COA_GL_ON_ACCNT_RCPT,
         Integer BA_COA_GL_UNPPLD_RCPT, Integer BA_COA_GL_BNK_CHRG_ACCNT,
         Integer BA_COA_GL_CLRNG_ACCNT, Integer BA_COA_GL_INTRST_ACCNT,
         Integer BA_COA_GL_ADJSTMNT_ACCNT, Integer BA_COA_GL_CSH_DSCNT,
         Integer BA_COA_GL_SLS_DSCNT,
         Integer BA_COA_GL_ADVNC_ACCNT,
         Integer BA_COA_GL_UNPPLD_CHK, double BA_FLT_BLNC,
         Date BA_LST_RCNCLD_DT, double BA_LST_RCNCLD_BLNC,
         String BA_NXT_CHK_NMBR, byte BA_ENBL, byte BA_ACCNT_NMBR_SHW, int BA_ACCNT_NMBR_TP,
         int BA_ACCNT_NMBR_LFT, byte BA_ACCNT_NM_SHW, int BA_ACCNT_NM_TP, int BA_ACCNT_NM_LFT,
         byte BA_NMBR_SHW, int BA_NMBR_TP, int BA_NMBR_LFT,
         byte BA_DT_SHW, int BA_DT_TP, int BA_DT_LFT,
         byte BA_PY_SHW, int BA_PY_TP, int BA_PY_LFT,
         byte BA_AMNT_SHW, int BA_AMNT_TP, int BA_AMNT_LFT,
         byte BA_WRD_AMNT_SHW, int BA_WRD_AMNT_TP, int BA_WRD_AMNT_LFT,
         byte BA_CRRNCY_SHW, int BA_CRRNCY_TP, int BA_CRRNCY_LFT,
         byte BA_ADDRSS_SHW, int BA_ADDRSS_TP, int BA_ADDRSS_LFT,
         byte BA_MM_SHW, int BA_MM_TP, int BA_MM_LFT,
         byte BA_DC_NMBR_SHW, int BA_DC_NMBR_TP, int BA_DC_NMBR_LFT, int BA_FNT_SZ, String BA_FNT_STYL, byte BA_IS_CSH_ACCNT,
		 Integer BA_AD_CMPNY)      
         throws CreateException { }
      
     public void ejbPostCreate(String BA_NM, String BA_DESC,
         String BA_ACCNT_TYP, String BA_ACCNT_NMBR, String BA_ACCNT_USE,
         Integer BA_COA_GL_CSH_ACCNT, Integer BA_COA_GL_ON_ACCNT_RCPT,
         Integer BA_COA_GL_UNPPLD_RCPT, Integer BA_COA_GL_BNK_CHRG_ACCNT,
         Integer BA_COA_GL_CLRNG_ACCNT, Integer BA_COA_GL_INTRST_ACCNT,
         Integer BA_COA_GL_ADJSTMNT_ACCNT, Integer BA_COA_GL_CSH_DSCNT,
         Integer BA_COA_GL_SLS_DSCNT,
         Integer BA_COA_GL_ADVNC_ACCNT,
         Integer BA_COA_GL_UNPPLD_CHK, double BA_FLT_BLNC,
         Date BA_LST_RCNCLD_DT, double BA_LST_RCNCLD_BLNC,
         String BA_NXT_CHK_NMBR, byte BA_ENBL, byte BA_ACCNT_NMBR_SHW, int BA_ACCNT_NMBR_TP,
         int BA_ACCNT_NMBR_LFT, byte BA_ACCNT_NM_SHW, int BA_ACCNT_NM_TP, int BA_ACCNT_NM_LFT,
         byte BA_NMBR_SHW, int BA_NMBR_TP, int BA_NMBR_LFT,
         byte BA_DT_SHW, int BA_DT_TP, int BA_DT_LFT,
         byte BA_PY_SHW, int BA_PY_TP, int BA_PY_LFT,
         byte BA_AMNT_SHW, int BA_AMNT_TP, int BA_AMNT_LFT,
         byte BA_WRD_AMNT_SHW, int BA_WRD_AMNT_TP, int BA_WRD_AMNT_LFT,
         byte BA_CRRNCY_SHW, int BA_CRRNCY_TP, int BA_CRRNCY_LFT,
         byte BA_ADDRSS_SHW, int BA_ADDRSS_TP, int BA_ADDRSS_LFT,
         byte BA_MM_SHW, int BA_MM_TP, int BA_MM_LFT,
         byte BA_DC_NMBR_SHW, int BA_DC_NMBR_TP, int BA_DC_NMBR_LFT, int BA_FNT_SZ, String BA_FNT_STYL, byte BA_IS_CSH_ACCNT,
		 Integer BA_AD_CMPNY)
         throws CreateException { }     
}