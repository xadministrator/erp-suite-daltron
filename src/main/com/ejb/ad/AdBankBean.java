/*
 * com/ejb/ad/LocalAdBankBean.java
 *
 * Created on February 21, 2003, 12:55 PM
 */
 
package com.ejb.ad;

import java.util.Collection;

import javax.ejb.CreateException;
import javax.ejb.EJBException;

import com.util.AbstractEntityBean;
import com.util.Debug;

/**
 *
 * @author  Neil Andrew M. Ajero
 *
 * @ejb:bean name="AdBankEJB"
 *           display-name="Bank Entity"
 *           type="CMP"
 *           cmp-version="2.x"
 *           view-type="local"
 *           local-jndi-name="omega-ejb/AdBankEJB"
 *           schema="AdBank"
 *           primkey-field="bnkCode"
 *
 * @ejb:pk class="java.lang.Integer"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="aruser"
 *                        role-link="aruserlink"
 *
 * @ejb:permission role-name="aruser"
 *
 * @ejb:interface local-class="com.ejb.ad.LocalAdBank"
 *                local-extends="javax.ejb.EJBLocalObject"
 *
 * @ejb:home local-class="com.ejb.ad.LocalAdBankHome"
 *           local-extends="javax.ejb.EJBLocalHome"
 *
 * @ejb:finder signature="Collection findBnkAll(java.lang.Integer BNK_AD_CMPNY)"
 *             query="SELECT OBJECT(bnk) FROM AdBank bnk WHERE bnk.bnkAdCompany = ?1"
 *
 * @jboss:query signature="Collection findBnkAll(java.lang.Integer BNK_AD_CMPNY)"
 *             query="SELECT OBJECT(bnk) FROM AdBank bnk WHERE bnk.bnkAdCompany = ?1 ORDER BY bnk.bnkName"
 *
 * @ejb:finder signature="LocalAdBank findByBnkName(java.lang.String BNK_NM, java.lang.Integer BNK_AD_CMPNY)"
 *             query="SELECT OBJECT(bnk) FROM AdBank bnk WHERE bnk.bnkName = ?1 AND bnk.bnkAdCompany = ?2"
 *
 * @ejb:finder signature="Collection findEnabledBnkAll(java.lang.Integer BNK_AD_CMPNY)"
 *             query="SELECT OBJECT(bnk) FROM AdBank bnk WHERE bnk.bnkEnable = 1 AND bnk.bnkAdCompany = ?1"
 *
 * @ejb:value-object match="*"
 *             name="AdBank"
 *
 * @jboss:persistence table-name="AD_BNK"
 *
 * @jboss:entity-command name="mysql-get-generated-keys"
 */

public abstract class AdBankBean extends AbstractEntityBean {
    
   
    // PERSISTENT METHODS
    
    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     * @ejb:pk-field
     *
     * @jboss:column-name name="BNK_CODE"
     *
     * @jboss:persistence auto-increment="true"
     **/
    public abstract Integer getBnkCode();
    public abstract void setBnkCode(Integer BNK_CODE);
    
    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="BNK_NM"
     **/
    public abstract String getBnkName();
     /**
      * @ejb:interface-method view-type="local"
      **/
    public abstract void setBnkName(String BNK_NM);
    
    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="BNK_BRNCH"
     **/
    public abstract String getBnkBranch();
     /**
      * @ejb:interface-method view-type="local"
      **/
    public abstract void setBnkBranch(String BNK_BRNCH);
    
    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="BNK_NMBR"
     **/
    public abstract long getBnkNumber();
     /**
      * @ejb:interface-method view-type="local"
      **/
    public abstract void setBnkNumber(long BNK_NMBR);
    
    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="BNK_INSTTTN"
     **/
    public abstract String getBnkInstitution();
     /**
      * @ejb:interface-method view-type="local"
      **/
    public abstract void setBnkInstitution(String BNK_INSTTTN);
    
    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="BNK_DESC"
     **/
    public abstract String getBnkDescription();
     /**
      * @ejb:interface-method view-type="local"
      **/
    public abstract void setBnkDescription(String BNK_DESC);
    
    
    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="BNK_ADDRSS"
     **/
    public abstract String getBnkAddress();
     /**
      * @ejb:interface-method view-type="local"
      **/
    public abstract void setBnkAddress(String BNK_ADDRSS);
    
    
    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="BNK_ENBL"
     **/
    public abstract byte getBnkEnable();
     /**
      * @ejb:interface-method view-type="local"
      **/
    public abstract void setBnkEnable(byte BNK_ENBL);
    
    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="BNK_AD_CMPNY"
     **/
    public abstract Integer getBnkAdCompany();
     /**
      * @ejb:interface-method view-type="local"
      **/
    public abstract void setBnkAdCompany(Integer BNK_AD_CMPNY);
    
    
    // RELATIONSHIP METHODS
    
    /**
     * @ejb:interface-method view-type="local"
     * @ejb:relation name="bank-bankaccounts"
     *               role-name="bank-has-many-bankaccounts"
     * 
     */
     public abstract Collection getAdBankAccounts();
     public abstract void setAdBankAccounts(Collection adBankAccounts);
     
     
    // BUSINESS METHODS
    
    
    /**
     * @ejb:interface-method view-type="local"
     **/
     public void addAdBankAccount(LocalAdBankAccount adBankAccount) {

         Debug.print("AdBankBean addAdBankAccount");
      
         try {
      	
            Collection adBankAccounts = getAdBankAccounts();
	        adBankAccounts.add(adBankAccount);
	         
         } catch (Exception ex) {
      	
           throw new EJBException(ex.getMessage());
      
         }   
     }

    /**
     * @ejb:interface-method view-type="local"
     **/
     public void dropAdBankAccount(LocalAdBankAccount adBankAccount) {

         Debug.print("AdBankBean dropAdBankAccount");
      
         try {
      	
            Collection adBankAccounts = getAdBankAccounts();
	        adBankAccounts.remove(adBankAccount);
	      
         } catch (Exception ex) {
      	
           throw new EJBException(ex.getMessage());
         
         }   
     } 
     
    // ENTITY METHODS
    
    /**
     * @ejb:create-method view-type="local"
     **/
     public Integer ejbCreate(Integer BNK_CODE, String BNK_NM, String BNK_BRNCH,
         long BNK_NMBR, String BNK_INSTTTN, String BNK_DESC, String BNK_ADDRSS,
         byte BNK_ENBL, Integer BNK_AD_CMPNY)
         throws CreateException {
      	
         Debug.print("AdBankBean ejbCreate");
      
		 setBnkCode(BNK_CODE);
		 setBnkName(BNK_NM);
		 setBnkBranch(BNK_BRNCH);
		 setBnkNumber(BNK_NMBR);
		 setBnkInstitution(BNK_INSTTTN);
		 setBnkDescription(BNK_DESC);
		 setBnkAddress(BNK_ADDRSS);
		 setBnkEnable(BNK_ENBL);
		 setBnkAdCompany(BNK_AD_CMPNY);
		         
         return null;
     } 
    
    /**
     * @ejb:create-method view-type="local"
     **/
     public Integer ejbCreate(
         String BNK_NM, String BNK_BRNCH,
         long BNK_NMBR, String BNK_INSTTTN, String BNK_DESC, String BNK_ADDRSS,
         byte BNK_ENBL, Integer BNK_AD_CMPNY)
         throws CreateException {
      	
         Debug.print("AdBankBean ejbCreate");
                  
         setBnkName(BNK_NM);
		 setBnkBranch(BNK_BRNCH);
		 setBnkNumber(BNK_NMBR);
		 setBnkInstitution(BNK_INSTTTN);
		 setBnkDescription(BNK_DESC);
		 setBnkAddress(BNK_ADDRSS);
		 setBnkEnable(BNK_ENBL);
		 setBnkAdCompany(BNK_AD_CMPNY);
        
         return null;
     }
    
     public void ejbPostCreate(Integer BNK_CODE, String BNK_NM, String BNK_BRNCH,
         long BNK_NMBR, String BNK_INSTTTN, String BNK_DESC, String BNK_ADDRSS,
         byte BNK_ENBL, Integer BNK_AD_CMPNY)      
         throws CreateException { }
      
     public void ejbPostCreate( 
         String BNK_NM, String BNK_BRNCH,
         long BNK_NMBR, String BNK_INSTTTN, String BNK_DESC, String BNK_ADDRSS,
         byte BNK_ENBL, Integer BNK_AD_CMPNY)
         throws CreateException { }     
}