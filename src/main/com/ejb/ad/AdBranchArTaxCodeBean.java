package com.ejb.ad;

import javax.ejb.CreateException;
import com.ejb.ar.LocalArTaxCode;
import com.util.AbstractEntityBean;
import com.util.Debug;

/**
*
* @author  Clint L. Arrogante
*
* @ejb:bean name="AdBranchArTaxCodeBeanEJB"
*           display-name="Branch Tax Code Entity"
*           type="CMP"
*           cmp-version="2.x"
*           view-type="local"
*           local-jndi-name="omega-ejb/AdBranchArTaxCodeEJB"
*           schema="AdBranchArTaxCode"
*           primkey-field="btcCode"
*       
* @ejb:pk class="java.lang.Integer"
*
* @ejb:transaction type="Required"
*
* @ejb:security-role-ref role-name="gluser"
*                        role-link="gluserlink"
*
* @ejb:permission role-name="gluser"
*
* @ejb:interface local-class="com.ejb.ad.LocalAdBranchArTaxCode"
*                local-extends="javax.ejb.EJBLocalObject"
*
* @ejb:home local-class="com.ejb.ad.LocalAdBranchArTaxCodeHome"
*           local-extends="javax.ejb.EJBLocalHome"
* 
* @ejb:finder signature="Collection findBtcByTcCodeAndRsName(java.lang.Integer BA_CODE, java.lang.String RS_NM, java.lang.Integer AD_CMPNY)"
* 			query="SELECT OBJECT(btc) FROM AdBranchArTaxCode btc, IN(btc.adBranch.adBranchResponsibilities) brs WHERE btc.arTaxCode.tcCode = ?1 AND brs.adResponsibility.rsName = ?2 AND btc.btcAdCompany = ?3"
*
* @ejb:finder signature="Collection findBtcByTcCode(java.lang.Integer TC_CODE, java.lang.Integer AD_CMPNY)"
* 			query="SELECT OBJECT(btc) FROM AdBranchArTaxCode btc WHERE btc.arTaxCode.tcCode = ?1 AND btc.btcAdCompany = ?2"
* 
* @ejb:finder signature="LocalAdBranchArTaxCode findBtcByTcCodeAndBrCode(java.lang.Integer BA_CODE, java.lang.Integer BR_CODE, java.lang.Integer AD_CMPNY)"
* 			query="SELECT OBJECT(btc) FROM AdBranchArTaxCode btc WHERE btc.arTaxCode.tcCode = ?1 AND btc.adBranch.brCode = ?2 AND btc.btcAdCompany = ?3"
* 
* @ejb:finder signature="Collection findBBTCByTcCodeAndRsName(java.lang.Integer TC_CODE, java.lang.String RS_NM, java.lang.Integer AD_CMPNY)"
* 			  query="SELECT OBJECT(btc) FROM AdBranchArTaxCode btc, IN(btc.adBranch.adBranchResponsibilities) brs WHERE btc.arTaxCode.tcCode = ?1 AND brs.adResponsibility.rsName = ?2 AND btc.btcAdCompany = ?3"
*
* @ejb:finder signature="Collection findBTcByTcNewAndUpdated(java.lang.Integer BR_CODE, java.lang.Integer AD_CMPNY, char NEW, char UPDATED, char DOWNLOADED_UPDATED)"
* 			  query="SELECT OBJECT(btc) FROM AdBranchArTaxCode btc WHERE (btc.btcDownloadStatus = ?3 OR btc.btcDownloadStatus = ?4 OR btc.btcDownloadStatus = ?5) AND btc.adBranch.brCode = ?1 AND btc.btcAdCompany = ?2"			

* @ejb:finder signature="LocalAdBranchArTaxCode findBtcByTcNameAndBrCode(java.lang.String BA_NM, java.lang.Integer BR_CODE, java.lang.Integer AD_CMPNY)"
*			query="SELECT OBJECT(btc) FROM AdBranchArTaxCode btc WHERE btc.arTaxCode.tcName = ?1 AND btc.adBranch.brCode = ?2 AND btc.btcAdCompany = ?3"
*
*
* @ejb:value-object match="*"
*             name="AdBranchArTaxCode"
*
* @jboss:persistence table-name="AD_BR_AR_TC"
*
* @jboss:entity-command name="mysql-get-generated-keys"
*
*/
public abstract class AdBranchArTaxCodeBean extends AbstractEntityBean {
    
    // PERSISTENT FIELDS

    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     * @ejb:pk-field
     *
     * @jboss:column-name name="BTC_CODE"
     *
     * @jboss:persistence auto-increment="true"
     **/
    public abstract java.lang.Integer getBtcCode();      
    public abstract void setBtcCode(java.lang.Integer BTC_CODE);
    


    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"   
     *
     * @jboss:column-name name="BTC_COA_GL_TX_CD"
     **/
    public abstract Integer getBtcGlCoaTaxCode();
     /**
      * @ejb:interface-method view-type="local"
      **/ 
    public abstract void setBtcGlCoaTaxCode(Integer BTC_COA_GL_TX_CD);
    
    
    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"   
     *
     * @jboss:column-name name="BTC_COA_GL_INTRM_CD"
     **/
    public abstract Integer getBtcGlCoaInterimCode();
     /**
      * @ejb:interface-method view-type="local"
      **/ 
    public abstract void setBtcGlCoaInterimCode(Integer BTC_COA_GL_INTRM_CD);
    
    
    
    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="BTC_DWNLD_STATUS"
     *
     **/
    public abstract char getBtcDownloadStatus();
    /**
 	* @ejb:interface-method view-type="local"
 	**/  
    public abstract void setBtcDownloadStatus(char BTC_DWNLD_STATUS);
    

    
    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="BTC_AD_CMPNY"
     **/
    public abstract Integer getBtcAdCompany();
    /**
 	* @ejb:interface-method view-type="local"
 	**/     
    public abstract void setBtcAdCompany(Integer BTC_AD_CMPNY);
    
    // RELATIONSHIP FIELDS
    
    /**
     * @ejb:interface-method view-type="local"
     * @ejb:relation name="branch-branchartaxcode"
     *               role-name="branchartaxcode-has-one-branch"
     *				 cascade-delete="yes"
     * 
     * @jboss:relation related-pk-field="brCode"
     *                 fk-column="AD_BRANCH"
     */
     public abstract LocalAdBranch getAdBranch();
     public abstract void setAdBranch(LocalAdBranch adBranch);
     
     /**
      * @ejb:interface-method view-type="local"
      * @ejb:relation name="artaxcode-branchartaxcode"
      *               role-name="branchartaxcode-has-one-artaxcode"
      *				  cascade-delete="yes"
      * 
      * @jboss:relation related-pk-field="tcCode"
      *                 fk-column="AR_TAX_CODE"
      */
      public abstract LocalArTaxCode getArTaxCode();
      public abstract void setArTaxCode(LocalArTaxCode arTaxCode);
      
      // ENTITY METHODS
      
      /**
       * @ejb:create-method view-type="local"
       **/
       public Integer ejbCreate(Integer BTC_CODE,
    		   Integer BTC_COA_GL_TX_CD, Integer BTC_COA_GL_INTRM_CD,
    		   char BTC_DWNLD_STATUS, Integer BTC_AD_CMPNY)
           throws CreateException {
        	
           Debug.print("AdBranchArTaxCodeBean ejbCreate");
        
           setBtcCode(BTC_CODE);  
           setBtcGlCoaTaxCode(BTC_COA_GL_TX_CD);
           setBtcGlCoaInterimCode(BTC_COA_GL_INTRM_CD);
           setBtcDownloadStatus(BTC_DWNLD_STATUS);
           setBtcAdCompany(BTC_AD_CMPNY);
          
           return null;
       }
       
	   /**
	    * @ejb:create-method view-type="local"
	    **/
	    public Integer ejbCreate(
	    		Integer BTC_COA_GL_TX_CD, Integer BTC_COA_GL_INTRM_CD,
	    		char BTC_DWNLD_STATUS,Integer BTC_AD_CMPNY)
	        throws CreateException {
	     	
	        Debug.print("AdBranchArTaxCodeBean ejbCreate");
	                             
	        setBtcGlCoaTaxCode(BTC_COA_GL_TX_CD);
	        setBtcGlCoaInterimCode(BTC_COA_GL_INTRM_CD);
	        setBtcDownloadStatus(BTC_DWNLD_STATUS);
	        setBtcAdCompany(BTC_AD_CMPNY);
	       
	        return null;
	    }
	    
	    public void ejbPostCreate(Integer BTC_CODE,
	    		Integer BTC_COA_GL_TX_CD, Integer BTC_COA_GL_INTRM_CD,
	    		char BTC_DWNLD_STATUS, Integer BTC_AD_CMPNY)
        	throws CreateException { }
	    
	    public void ejbPostCreate(
	    		Integer BTC_COA_GL_TX_CD, Integer BTC_COA_GL_INTRM_CD,
	    		char BTC_DWNLD_STATUS,Integer BTC_AD_CMPNY)
    		throws CreateException { }

}
