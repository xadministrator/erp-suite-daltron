package com.ejb.ad;

import javax.ejb.CreateException;

import com.util.AbstractEntityBean;
import com.util.Debug;

/**
*
* @author  Arnel A. Masikip
*
* @ejb:bean name="AdBranchBankAccountBeanEJB"
*           display-name="Branch Bank Account Entity"
*           type="CMP"
*           cmp-version="2.x"
*           view-type="local"
*           local-jndi-name="omega-ejb/AdBranchBankAccountEJB"
*           schema="AdBranchBankAccount"
*           primkey-field="bbaCode"
*       
* @ejb:pk class="java.lang.Integer"
*
* @ejb:transaction type="Required"
*
* @ejb:security-role-ref role-name="gluser"
*                        role-link="gluserlink"
*
* @ejb:permission role-name="gluser"
*
* @ejb:interface local-class="com.ejb.ad.LocalAdBranchBankAccount"
*                local-extends="javax.ejb.EJBLocalObject"
*
* @ejb:home local-class="com.ejb.ad.LocalAdBranchBankAccountHome"
*           local-extends="javax.ejb.EJBLocalHome"
* 
* @ejb:finder signature="Collection findBbaByBaCodeAndRsName(java.lang.Integer BA_CODE, java.lang.String RS_NM, java.lang.Integer AD_CMPNY)"
* 			query="SELECT OBJECT(bba) FROM AdBranchBankAccount bba, IN(bba.adBranch.adBranchResponsibilities) brs WHERE bba.adBankAccount.baCode = ?1 AND brs.adResponsibility.rsName = ?2 AND bba.bbaAdCompany = ?3"
*
* @ejb:finder signature="Collection findBbaByBaCode(java.lang.Integer BA_CODE, java.lang.Integer AD_CMPNY)"
* 			query="SELECT OBJECT(bba) FROM AdBranchBankAccount bba WHERE bba.adBankAccount.baCode = ?1 AND bba.bbaAdCompany = ?2"
* 
* @ejb:finder signature="LocalAdBranchBankAccount findBbaByBaCodeAndBrCode(java.lang.Integer BA_CODE, java.lang.Integer BR_CODE, java.lang.Integer AD_CMPNY)"
* 			query="SELECT OBJECT(bba) FROM AdBranchBankAccount bba WHERE bba.adBankAccount.baCode = ?1 AND bba.adBranch.brCode = ?2 AND bba.bbaAdCompany = ?3"
* 
* @ejb:finder signature="Collection findBBaByBaNewAndUpdated(java.lang.Integer BR_CODE, java.lang.Integer AD_CMPNY, char NEW, char UPDATED, char DOWNLOADED_UPDATED)"
* 			  query="SELECT OBJECT(bba) FROM AdBranchBankAccount bba WHERE (bba.bbaDownloadStatus = ?3 OR bba.bbaDownloadStatus = ?4 OR bba.bbaDownloadStatus = ?5) AND bba.adBranch.brCode = ?1 AND bba.bbaAdCompany = ?2"			

* @ejb:finder signature="LocalAdBranchBankAccount findBbaByBaNameAndBrCode(java.lang.String BA_NM, java.lang.Integer BR_CODE, java.lang.Integer AD_CMPNY)"
*			query="SELECT OBJECT(bba) FROM AdBranchBankAccount bba WHERE bba.adBankAccount.baName = ?1 AND bba.adBranch.brCode = ?2 AND bba.bbaAdCompany = ?3"
*
*
* @ejb:value-object match="*"
*             name="AdBranchBankAccount"
*
* @jboss:persistence table-name="AD_BR_BA"
*
* @jboss:entity-command name="mysql-get-generated-keys"
*
*/
public abstract class AdBranchBankAccountBean extends AbstractEntityBean {
    
    // PERSISTENT FIELDS

    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     * @ejb:pk-field
     *
     * @jboss:column-name name="BBA_CODE"
     *
     * @jboss:persistence auto-increment="true"
     **/
    public abstract java.lang.Integer getBbaCode();      
    public abstract void setBbaCode(java.lang.Integer BBA_CODE);
    
    
    
    

    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"   
     *
     * @jboss:column-name name="BBA_COA_GL_CSH_ACCNT"
     **/
    public abstract Integer getBbaGlCoaCashAccount();
     /**
      * @ejb:interface-method view-type="local"
      **/ 
    public abstract void setBbaGlCoaCashAccount(Integer BBA_COA_GL_CSH_ACCNT);
    
    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"   
     *
     * @jboss:column-name name="BBA_COA_GL_BNK_CHRG_ACCNT"
     **/
    public abstract Integer getBbaGlCoaBankChargeAccount();
     /**
      * @ejb:interface-method view-type="local"
      **/ 
    public abstract void setBbaGlCoaBankChargeAccount(Integer BBA_COA_GL_BNK_CHRG_ACCNT);
    
    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"   
     *
     * @jboss:column-name name="BBA_COA_GL_INTRST_ACCNT"
     **/
    public abstract Integer getBbaGlCoaInterestAccount();
     /**
      * @ejb:interface-method view-type="local"
      **/ 
    public abstract void setBbaGlCoaInterestAccount(Integer BBA_COA_GL_INTRST_ACCNT);
    
    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"   
     *
     * @jboss:column-name name="BBA_COA_GL_ADJSTMNT_ACCNT"
     **/
    public abstract Integer getBbaGlCoaAdjustmentAccount();
     /**
      * @ejb:interface-method view-type="local"
      **/ 
    public abstract void setBbaGlCoaAdjustmentAccount(Integer BBA_COA_GL_ADJSTMNT_ACCNT);
    
    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"   
     *
     * @jboss:column-name name="BBA_COA_GL_SLS_DSCNT_ACCNT"
     **/
    public abstract Integer getBbaGlCoaSalesDiscountAccount();
     /**
      * @ejb:interface-method view-type="local"
      **/ 
    public abstract void setBbaGlCoaSalesDiscountAccount(Integer BBA_COA_GL_SLS_DSCNT_ACCNT);
    
    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"   
     *
     * @jboss:column-name name="BBA_COA_GL_ADVNC_ACCNT"
     **/
    public abstract Integer getBbaGlCoaAdvanceAccount();
     /**
      * @ejb:interface-method view-type="local"
      **/ 
    public abstract void setBbaGlCoaAdvanceAccount(Integer BBA_COA_GL_ADVNC_ACCNT);
    
    
    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="BBA_DWNLD_STATUS"
     *
     **/
    public abstract char getBbaDownloadStatus();
    /**
 	* @ejb:interface-method view-type="local"
 	**/  
    public abstract void setBbaDownloadStatus(char BBA_DWNLD_STATUS);
    

    
    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="BBA_AD_CMPNY"
     **/
    public abstract Integer getBbaAdCompany();
    /**
 	* @ejb:interface-method view-type="local"
 	**/     
    public abstract void setBbaAdCompany(Integer BBA_AD_CMPNY);
    
    // RELATIONSHIP FIELDS
    
    /**
     * @ejb:interface-method view-type="local"
     * @ejb:relation name="branch-branchbankaccount"
     *               role-name="branchbankaccount-has-one-branch"
     *				 cascade-delete="yes"
     * 
     * @jboss:relation related-pk-field="brCode"
     *                 fk-column="AD_BRANCH"
     */
     public abstract LocalAdBranch getAdBranch();
     public abstract void setAdBranch(LocalAdBranch adBranch);
     
     /**
      * @ejb:interface-method view-type="local"
      * @ejb:relation name="bankaccount-branchbankaccount"
      *               role-name="branchbankaccount-has-one-bankaccount"
      *				  cascade-delete="yes"
      * 
      * @jboss:relation related-pk-field="baCode"
      *                 fk-column="AD_BANK_ACCOUNT"
      */
      public abstract LocalAdBankAccount getAdBankAccount();
      public abstract void setAdBankAccount(LocalAdBankAccount adBankAccount);
      
      // ENTITY METHODS
      
      /**
       * @ejb:create-method view-type="local"
       **/
       public Integer ejbCreate(Integer BBA_CODE,
    		   Integer BBA_COA_GL_CSH_ACCNT, 
		       	 Integer BBA_COA_GL_BNK_CHRG_ACCNT,
		       	 Integer BBA_COA_GL_INTRST_ACCNT,
		       	 Integer BBA_COA_GL_ADJSTMNT_ACCNT,
		       	Integer BBA_COA_GL_SLS_DSCNT_ACCNT,
		       	Integer BBA_COA_GL_ADVNC_ACCNT,
    		   char BBA_DWNLD_STATUS, Integer BBA_AD_CMPNY)
           throws CreateException {
        	
           Debug.print("AdBranchBankAccountBean ejbCreate");
        
           setBbaCode(BBA_CODE);  
           setBbaGlCoaCashAccount(BBA_COA_GL_CSH_ACCNT);
           setBbaGlCoaBankChargeAccount(BBA_COA_GL_BNK_CHRG_ACCNT);
           setBbaGlCoaInterestAccount(BBA_COA_GL_INTRST_ACCNT);
           setBbaGlCoaAdjustmentAccount(BBA_COA_GL_ADJSTMNT_ACCNT);
           setBbaGlCoaSalesDiscountAccount(BBA_COA_GL_SLS_DSCNT_ACCNT);
           setBbaGlCoaAdvanceAccount(BBA_COA_GL_ADVNC_ACCNT);
           setBbaDownloadStatus(BBA_DWNLD_STATUS);
           setBbaAdCompany(BBA_AD_CMPNY);
          
           return null;
       }
       
	   /**
	    * @ejb:create-method view-type="local"
	    **/
	    public Integer ejbCreate(
	    		Integer BBA_COA_GL_CSH_ACCNT, 
		       	 Integer BBA_COA_GL_BNK_CHRG_ACCNT,
		       	 Integer BBA_COA_GL_INTRST_ACCNT,
		       	 Integer BBA_COA_GL_ADJSTMNT_ACCNT,
		       	Integer BBA_COA_GL_SLS_DSCNT_ACCNT,
		       	Integer BBA_COA_GL_ADVNC_ACCNT,
	    		char BBA_DWNLD_STATUS,Integer BBA_AD_CMPNY)
	        throws CreateException {
	     	
	        Debug.print("AdBranchBankAccountBean ejbCreate");
	                             
	        
	        setBbaGlCoaCashAccount(BBA_COA_GL_CSH_ACCNT);
	         setBbaGlCoaBankChargeAccount(BBA_COA_GL_BNK_CHRG_ACCNT);
	         setBbaGlCoaInterestAccount(BBA_COA_GL_INTRST_ACCNT);
	         setBbaGlCoaAdjustmentAccount(BBA_COA_GL_ADJSTMNT_ACCNT);
	         setBbaGlCoaSalesDiscountAccount(BBA_COA_GL_SLS_DSCNT_ACCNT);
	         setBbaGlCoaAdvanceAccount(BBA_COA_GL_ADVNC_ACCNT);
	        setBbaDownloadStatus(BBA_DWNLD_STATUS);
	        setBbaAdCompany(BBA_AD_CMPNY);
	       
	        return null;
	    }
	    
	    public void ejbPostCreate(Integer BBA_CODE,
	    		Integer BBA_COA_GL_CSH_ACCNT, 
	       	 Integer BBA_COA_GL_BNK_CHRG_ACCNT,
	       	 Integer BBA_COA_GL_INTRST_ACCNT,
	       	 Integer BBA_COA_GL_ADJSTMNT_ACCNT,
	       	Integer BBA_COA_GL_SLS_DSCNT_ACCNT,
	       	Integer BBA_COA_GL_ADVNC_ACCNT,
	    		char BBA_DWNLD_STATUS, Integer BBA_AD_CMPNY)
        	throws CreateException { }
	    
	    public void ejbPostCreate(
	    		Integer BBA_COA_GL_CSH_ACCNT, 
	       	 Integer BBA_COA_GL_BNK_CHRG_ACCNT,
	       	 Integer BBA_COA_GL_INTRST_ACCNT,
	       	 Integer BBA_COA_GL_ADJSTMNT_ACCNT,
	       	Integer BBA_COA_GL_SLS_DSCNT_ACCNT,
	       	Integer BBA_COA_GL_ADVNC_ACCNT,
	    		char BBA_DWNLD_STATUS,Integer BBA_AD_CMPNY)
    		throws CreateException { }

}
