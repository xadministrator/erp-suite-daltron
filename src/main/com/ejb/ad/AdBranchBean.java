package com.ejb.ad;

import java.util.Collection;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import com.ejb.inv.LocalInvBranchStockTransfer;
import com.util.AbstractEntityBean;
import com.util.Debug;

/**
*
* @author  Arnel A. Masikip
*
* @ejb:bean name="AdBranchEJB"
*           display-name="Branch Entity"
*           type="CMP"
*           cmp-version="2.x"
*           view-type="local"
*           local-jndi-name="omega-ejb/AdBranchEJB"
*           schema="AdBranch"
*           primkey-field="brCode"
*       
* @ejb:pk class="java.lang.Integer"
*
* @ejb:transaction type="Required"
*
* @ejb:security-role-ref role-name="gluser"
*                        role-link="gluserlink"
*
* @ejb:permission role-name="gluser"
*
* @ejb:interface local-class="com.ejb.ad.LocalAdBranch"
*                local-extends="javax.ejb.EJBLocalObject"
*
* @ejb:home local-class="com.ejb.ad.LocalAdBranchHome"
*           local-extends="javax.ejb.EJBLocalHome"
*
* @ejb:finder signature="Collection findBrAll(java.lang.Integer AD_CMPNY)"
*             query="SELECT OBJECT(br) FROM AdBranch br WHERE br.brAdCompany = ?1"
* 
* @jboss:query signature="Collection findBrAll(java.lang.Integer AD_CMPNY)"
*             query="SELECT OBJECT(br) FROM AdBranch br WHERE br.brAdCompany = ?1 ORDER BY br.brBranchCode"
*
* @ejb:finder signature="LocalAdBranch findByBrBranchCode(java.lang.String BR_BRNCH_CODE, java.lang.Integer AD_CMPNY)"
*             query="SELECT OBJECT(br) FROM AdBranch br WHERE br.brBranchCode = ?1 AND br.brAdCompany = ?2"
* 
* @ejb:finder signature="LocalAdBranch findByBrName(java.lang.String BR_BRNCH_NM, java.lang.Integer AD_CMPNY)"
* 			  query="SELECT OBJECT(br) FROM AdBranch br WHERE br.brName = ?1 AND br.brAdCompany = ?2"
* 
* @ejb:finder signature="LocalAdBranch findByBrHeadQuarter(java.lang.Integer AD_CMPNY)"
* 			  query="SELECT OBJECT(br) FROM AdBranch br WHERE br.brHeadQuarter = 1 AND br.brAdCompany = ?1"				
* 
* @ejb:finder signature="Collection findByBrCodeAndBrDownloadStatus(java.lang.Integer BR_CODE, char BR_DWNLD_STATUS, java.lang.Integer BR_AD_CMPNY)"
*             query="SELECT OBJECT(br) FROM AdBranch br WHERE br.brCode = ?1 AND br.brDownloadStatus = ?2 AND br.brAdCompany = ?3"
* 
* @ejb:finder signature="Collection findByBrNewAndUpdated(java.lang.Integer BR_AD_CMPNY, char NEW, char UPDATED, char DOWNLOADED_UPDATED)"
*             query="SELECT OBJECT(br) FROM AdBranch br WHERE br.brAdCompany = ?1 AND (br.brDownloadStatus = ?2 OR br.brDownloadStatus = ?3 OR br.brDownloadStatus = ?4)"
*
* @jboss:query signature="Collection findByBrNewAndUpdated(java.lang.Integer BR_AD_CMPNY, char NEW, char UPDATED, char DOWNLOADED_UPDATED)"
*             query="SELECT OBJECT(br) FROM AdBranch br WHERE br.brAdCompany = ?1 AND (br.brDownloadStatus = ?2 OR br.brDownloadStatus = ?3 OR br.brDownloadStatus = ?4) ORDER BY br.brDownloadStatus, br.brBranchCode" 
*
* @ejb:value-object match="*"
*             name="AdBranch"
*
* @jboss:persistence table-name="AD_BRNCH"
*
* @jboss:entity-command name="mysql-get-generated-keys"
*
*/
public abstract class AdBranchBean extends AbstractEntityBean {

	// PERSISTENT FIELDS

	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 * @ejb:pk-field
	 *
	 * @jboss:column-name name="BR_CODE"
	 *
	 * @jboss:persistence auto-increment="true"
	 **/
	public abstract java.lang.Integer getBrCode();
	public abstract void setBrCode(java.lang.Integer BR_CODE);

	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="BR_BRNCH_CODE"
	 **/
	public abstract String getBrBranchCode();
	/**
	 * @ejb:interface-method view-type="local"
	 **/     
	public abstract void setBrBranchCode(String BR_BRNCH_CODE);

	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="BR_NM"
	 **/
	public abstract String getBrName();
	/**
	 * @ejb:interface-method view-type="local"
	 **/     
	public abstract void setBrName(String BR_NM);

	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="BR_DESC"
	 **/
	public abstract String getBrDescription();
	/**
	 * @ejb:interface-method view-type="local"
	 **/     
	public abstract void setBrDescription(String BR_DESC);

	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="BR_TYP"
	 **/
	public abstract String getBrType();
	/**
	 * @ejb:interface-method view-type="local"
	 **/     
	public abstract void setBrType(String BR_TYP);

	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="BR_HD_QTR"
	 **/
	public abstract byte getBrHeadQuarter();
	/**
	 * @ejb:interface-method view-type="local"
	 **/     
	public abstract void setBrHeadQuarter(byte BR_HD_QTR);

	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="BR_ADDRSS"
	 **/
	public abstract String getBrAddress();
	/**
	 * @ejb:interface-method view-type="local"
	 **/     
	public abstract void setBrAddress(String BR_ADDRSS);

	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="BR_CNTCT_PRSN"
	 **/
	public abstract String getBrContactPerson();
	/**
	 * @ejb:interface-method view-type="local"
	 **/     
	public abstract void setBrContactPerson(String BR_CNTCT_PRSN);

	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="BR_CNTCT_NMBR"
	 **/
	public abstract String getBrContactNumber();
	/**
	 * @ejb:interface-method view-type="local"
	 **/     
	public abstract void setBrContactNumber(String BR_CNTCT_NMBR);

	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="BR_COA_SGMNT"
	 **/
	public abstract String getBrCoaSegment();
	/**
	 * @ejb:interface-method view-type="local"
	 **/     
	public abstract void setBrCoaSegment(String BR_COA_SGMNT);

	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="BR_DWNLD_STATUS"
	 **/
	public abstract char getBrDownloadStatus();
	/**
	 * @ejb:interface-method view-type="local"
	 **/     
	public abstract void setBrDownloadStatus(char BR_DWNLD_STATUS);   

	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="BR_APPLY_SHPPNG"
	 **/
	public abstract byte getBrApplyShipping();
	/**
	 * @ejb:interface-method view-type="local"
	 **/     
	public abstract void setBrApplyShipping(byte BR_APPLY_SHPPNG);   

	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="BR_PRCNT_MRKP"
	 **/
	public abstract double getBrPercentMarkup();
	/**
	 * @ejb:interface-method view-type="local"
	 **/     
	public abstract void setBrPercentMarkup(double BR_PRCNT_MRKP);   

	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="BR_AD_CMPNY"
	 **/
	public abstract Integer getBrAdCompany();
	/**
	 * @ejb:interface-method view-type="local"
	 **/     
	public abstract void setBrAdCompany(Integer BR_AD_CMPNY);

	// RELATIONSHIP METHODS

	/**
	 * @ejb:interface-method view-type="local"
	 * @ejb:relation name="branch-branchresponsibility"
	 *               role-name="branch-has-many-branchresponsibility"
	 */
	public abstract Collection getAdBranchResponsibilities();
	public abstract void setAdBranchResponsibilities(Collection adBranchResponsibilities); 

	/**
	 * @ejb:interface-method view-type="local"
	 * @ejb:relation name="branch-branchdocumentsequenceassignment"
	 *               role-name="branch-has-many-branchdocumentsequenceassignment"
	 */
	public abstract Collection getAdBranchDocumentSequenceAssignments();
	public abstract void setAdBranchDocumentSequenceAssignments(Collection adBranchDocumentSequenceAssignment);

	/**
	 * @ejb:interface-method view-type="local"
	 * @ejb:relation name="branch-branchcoa"
	 *               role-name="branch-has-many-branchcoa"
	 */
	public abstract Collection getAdBranchCoas();
	public abstract void setAdBranchCoas(Collection adBranchCoas); 

	/**
	 * @ejb:interface-method view-type="local"
	 * @ejb:relation name="branch-branchbankaccount"
	 *               role-name="branch-has-many-branchbankaccount"
	 */
	public abstract Collection getAdBranchBankAccounts();
	public abstract void setAdBranchBankAccounts(Collection adBranchBankAccounts); 

	/**
	 * @ejb:interface-method view-type="local"
	 * @ejb:relation name="branch-branchartaxcode"
	 *               role-name="branch-has-many-branchartaxcode"
	 */
	public abstract Collection getAdBranchArTaxCodes();
	public abstract void setAdBranchArTaxCodes(Collection adBranchArTaxCodes); 
	
	/**
	 * @ejb:interface-method view-type="local"
	 * @ejb:relation name="branch-branchaptaxcode"
	 *               role-name="branch-has-many-branchaptaxcode"
	 */
	public abstract Collection getAdBranchApTaxCodes();
	public abstract void setAdBranchApTaxCodes(Collection adBranchApTaxCodes);

	/**
	 * @ejb:interface-method view-type="local"
	 * @ejb:relation name="branch-branchcustomers"
	 *               role-name="branch-has-many-branchcustomer"
	 */
	public abstract Collection getAdBranchCustomer();
	public abstract void setAdBranchCustomer(Collection adBranchCustomer);

	/**
	 * @ejb:interface-method view-type="local"
	 * @ejb:relation name="branch-branchstandardmemoline"
	 *               role-name="branch-has-many-branchstandardmemoline"
	 */
	public abstract Collection getAdBranchStandardMemoLine();
	public abstract void setAdBranchStandardMemoLine(Collection adBranchStandardMemoLine);

	/**
	 * @ejb:interface-method view-type="local"
	 * @ejb:relation name="branch-branchsuppliers"
	 *               role-name="branch-has-many-branchsupplier"
	 */
	public abstract Collection getAdBranchSupplier();
	public abstract void setAdBranchSupplier(Collection adBranchSupplier);

	/**
	 * @ejb:interface-method view-type="local"
	 * @ejb:relation name="branch-branchitemlocation"
	 *               role-name="branch-has-many-branchitemlocation"
	 */
	public abstract Collection getAdBranchItemLocation();
	public abstract void setAdBranchItemLocation(Collection adBranchItemLocation);

	/**
	 * @ejb:interface-method view-type="local"
	 * @ejb:relation name="branch-branchomls"
	 * 				role-name="branch-has-many-branchomls"
	 * 
	 */
	public abstract Collection getAdBranchOverheadMemoLines();
	public abstract void setAdBranchOverheadMemoLines(Collection adBranchOmls);

	/**
	 * @ejb:interface-method view-type="local"
	 * @ejb:relation name="branch-branchsalesperson"
	 *               role-name="branch-has-many-branchsalesperson"
	 */
	public abstract Collection getAdBranchSalespersons();
	public abstract void setAdBranchSalespersons(Collection adBranchBankAccounts); 

	/**
	 * @ejb:interface-method view-type="local"
	 * @ejb:relation name="branch-branchstocktransfers"
	 *               role-name="branch-has-many-branchstocktransfers"
	 */
	public abstract Collection getInvBranchStockTransfers();
	public abstract void setInvBranchStockTransfers(Collection invBranchStockTransfers);
	
	
	/**
	 * @ejb:interface-method view-type="local"
	 * @ejb:relation name="branch-branchprojecttypetype"
	 *               role-name="branch-has-many-branchprojecttypetype"
	 */
	public abstract Collection getAdBranchProjectTypeType();
	public abstract void setAdBranchProjectTypeType(Collection adBranchProjectTypeType);
	

	/**
	 * @ejb:interface-method view-type="local"
	 * @ejb:relation name="chartofaccount-branches"
	 *               role-name="branch-has-one-chartofaccount"
	 * 
	 * @jboss:relation related-pk-field="coaCode"
	 *                 fk-column="GL_CHART_OF_ACCOUNT"
	 */
	public abstract com.ejb.gl.LocalGlChartOfAccount getGlChartOfAccount();
	public abstract void setGlChartOfAccount(com.ejb.gl.LocalGlChartOfAccount glChartOfAccount);


	// BUSINESS METHODS

	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public void addAdBranchResponsibility(LocalAdBranchResponsibility adBranchResponsibility) {

		Debug.print("AdBranchBean addAdBranchResponsibility");

		try {

			Collection adBranchResponsibilities = getAdBranchResponsibilities();
			adBranchResponsibilities.add(adBranchResponsibility);

		} catch (Exception ex) {

			throw new EJBException(ex.getMessage());

		}

	}

	/**
	 * @ejb:interface-method view-type="local"
	 **/ 
	public void dropAdBranchResponsibility(LocalAdBranchResponsibility adBranchResponsibility) {

		Debug.print("AdBranchBean dropAdBranchResponsibility");

		try {

			Collection adBranchResponsibilities = getAdBranchResponsibilities();
			adBranchResponsibilities.remove(adBranchResponsibility);

		} catch (Exception ex) {

			throw new EJBException(ex.getMessage());

		}

	}

	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public void addAdBranchDocumentSequenceAssignments(LocalAdBranchDocumentSequenceAssignment adBranchDocumentSequenceAssignment) {

		Debug.print("AdBranchBean addAdBranchDocumentSequenceAssignments");

		try {

			Collection adBranchDsas = getAdBranchDocumentSequenceAssignments();
			adBranchDsas.add(adBranchDocumentSequenceAssignment);

		} catch (Exception ex) {

			throw new EJBException(ex.getMessage());

		}

	}

	/**
	 * @ejb:interface-method view-type="local"
	 **/ 
	public void dropAdBranchDocumentSequenceAssignments(LocalAdBranchDocumentSequenceAssignment adBranchDocumentSequenceAssignment) {

		Debug.print("AdBranchBean dropAdBranchDocumentSequenceAssignments");

		try {

			Collection adBranchDsas = getAdBranchDocumentSequenceAssignments();
			adBranchDsas.remove(adBranchDocumentSequenceAssignment);

		} catch (Exception ex) {

			throw new EJBException(ex.getMessage());

		}

	}

	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public void addAdBranchCoa(LocalAdBranchCoa adBranchCoa) {

		Debug.print("AdBranchBean addAdBranchCoa");

		try {

			Collection adBranchCoas = getAdBranchCoas();
			adBranchCoas.add(adBranchCoa);

		} catch (Exception ex) {

			throw new EJBException(ex.getMessage());

		}

	}

	/**
	 * @ejb:interface-method view-type="local"
	 **/ 
	public void dropAdBranchCoa(LocalAdBranchCoa adBranchCoa) {

		Debug.print("AdBranchBean dropAdBranchCoa");

		try {

			Collection adBranchCoas = getAdBranchCoas();
			adBranchCoas.remove(adBranchCoa);

		} catch (Exception ex) {

			throw new EJBException(ex.getMessage());

		}

	}

	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public void addAdBranchBankAccount(LocalAdBranchBankAccount adBranchBankAccount) {

		Debug.print("AdBranchBean addAdBranchBankAccount");

		try {

			Collection adBranchBankAccounts = getAdBranchBankAccounts();
			adBranchBankAccounts.add(adBranchBankAccount);

		} catch (Exception ex) {

			throw new EJBException(ex.getMessage());

		}

	}

	/**
	 * @ejb:interface-method view-type="local"
	 **/ 
	public void dropAdBranchBankAccount(LocalAdBranchBankAccount adBranchBankAccount) {

		Debug.print("AdBranchBean dropAdBranchBankAccount");

		try {

			Collection adBranchBankAccounts = getAdBranchBankAccounts();
			adBranchBankAccounts.remove(adBranchBankAccount);

		} catch (Exception ex) {

			throw new EJBException(ex.getMessage());

		}

	}

	
	
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public void addAdBranchArTaxCode(LocalAdBranchArTaxCode adBranchArTaxCode) {

		Debug.print("AdBranchBean addAdBranchArTaxCode");

		try {

			Collection arBranchTaxCodes = getAdBranchArTaxCodes();
			
			arBranchTaxCodes.add(adBranchArTaxCode);

		} catch (Exception ex) {

			throw new EJBException(ex.getMessage());

		}

	}

	/**
	 * @ejb:interface-method view-type="local"
	 **/ 
	public void dropAdBranchArTaxCode(LocalAdBranchArTaxCode adBranchArTaxCode) {

		Debug.print("AdBranchBean dropAdBranchArTaxCode");

		try {

			Collection arBranchTaxCodes = getAdBranchArTaxCodes();
			arBranchTaxCodes.remove(adBranchArTaxCode);

		} catch (Exception ex) {

			throw new EJBException(ex.getMessage());

		}

	}
	
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public void addAdBranchApTaxCode(LocalAdBranchApTaxCode adBranchApTaxCode) {

		Debug.print("AdBranchBean addAdBranchApTaxCode");

		try {

			Collection apBranchTaxCodes = getAdBranchApTaxCodes();
			
			apBranchTaxCodes.add(adBranchApTaxCode);

		} catch (Exception ex) {

			throw new EJBException(ex.getMessage());

		}

	}

	/**
	 * @ejb:interface-method view-type="local"
	 **/ 
	public void dropAdBranchApTaxCode(LocalAdBranchApTaxCode adBranchApTaxCode) {

		Debug.print("AdBranchBean dropAdBranchApTaxCode");

		try {

			Collection apBranchTaxCodes = getAdBranchApTaxCodes();
			apBranchTaxCodes.remove(adBranchApTaxCode);

		} catch (Exception ex) {

			throw new EJBException(ex.getMessage());

		}

	}
	
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public void addAdBranchCustomer(LocalAdBranchCustomer adBranchCustomer) {

		Debug.print("AdBranchBean addAdBranchCustomer");

		try {

			Collection adBranchCustomers = getAdBranchCustomer();
			adBranchCustomers.add(adBranchCustomer);

		} catch (Exception ex) {

			throw new EJBException(ex.getMessage());

		}   
	}

	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public void dropAdBranchCustomer(LocalAdBranchCustomer adBranchCustomer) {

		Debug.print("AdBranchBean dropAdBranchCustomer");

		try {

			Collection adBranchCustomers = getAdBranchCustomer();
			adBranchCustomers.remove(adBranchCustomer);

		} catch (Exception ex) {

			throw new EJBException(ex.getMessage());

		}   
	} 

	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public void addAdBranchStandardMemoLine(LocalAdBranchStandardMemoLine adBranchStandardMemoLine) {

		Debug.print("AdBranchBean addAdBranchStandardMemoLine");

		try {

			Collection adBranchStandardMemoLines = getAdBranchStandardMemoLine();
			adBranchStandardMemoLines.add(adBranchStandardMemoLine);

		} catch (Exception ex) {

			throw new EJBException(ex.getMessage());

		}   
	}

	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public void dropAdBranchStandardMemoLine(LocalAdBranchStandardMemoLine adBranchStandardMemoLine) {

		Debug.print("AdBranchBean dropAdBranchStandardMemoLine");

		try {

			Collection adBranchStandardMemoLines = getAdBranchStandardMemoLine();
			adBranchStandardMemoLines.remove(adBranchStandardMemoLine);

		} catch (Exception ex) {

			throw new EJBException(ex.getMessage());

		}   
	}

	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public void addAdBranchSupplier(LocalAdBranchSupplier adBranchSupplier) {

		Debug.print("AdBranchBean addAdBranchSupplier");

		try {

			Collection adBranchSuppliers = getAdBranchSupplier();
			adBranchSuppliers.add(adBranchSupplier);

		} catch (Exception ex) {

			throw new EJBException(ex.getMessage());

		}   
	}

	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public void dropAdBranchSupplier(LocalAdBranchSupplier adBranchSupplier) {

		Debug.print("AdBranchBean dropAdBranchSupplier");

		try {

			Collection adBranchSuppliers = getAdBranchSupplier();
			adBranchSuppliers.remove(adBranchSupplier);

		} catch (Exception ex) {

			throw new EJBException(ex.getMessage());

		}   
	}    

	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public void addAdBranchItemLocation(LocalAdBranchItemLocation adBranchItemLocation) {

		Debug.print("AdBranchBean addAdBranchItemLocation");

		try {

			Collection adBranchItemLocations = getAdBranchItemLocation();
			adBranchItemLocations.add(adBranchItemLocation);

		} catch (Exception ex) {

			throw new EJBException(ex.getMessage());

		}   
	}

	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public void dropAdBranchItemLocation(LocalAdBranchItemLocation adBranchItemLocation) {

		Debug.print("AdBranchBean dropAdBranchItemLocation");

		try {

			Collection adBranchItemLocations = getAdBranchItemLocation();
			adBranchItemLocations.remove(adBranchItemLocation);

		} catch (Exception ex) {

			throw new EJBException(ex.getMessage());

		}   
	}

	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public void addAdBranchOverheadMemoLine(LocalAdBranchOverheadMemoLine adBranchOverheadMemoLine) {

		Debug.print("AdBranchBean addAdBranchOverheadMemoLine");

		try {

			Collection adBranchOmls = getAdBranchOverheadMemoLines();
			adBranchOmls.add(adBranchOverheadMemoLine);

		} catch (Exception ex) {

			throw new EJBException(ex.getMessage());

		}

	}

	/**
	 * @ejb:interface-method view-type="local"
	 **/ 
	public void dropAdBranchOverheadMemoLine(LocalAdBranchOverheadMemoLine adBranchOverheadMemoLine) {

		Debug.print("AdBranchBean dropAdBranchOverheadMemoLine");

		try {

			Collection adBranchOmls = getAdBranchOverheadMemoLines();
			adBranchOmls.remove(adBranchOverheadMemoLine);

		} catch (Exception ex) {

			throw new EJBException(ex.getMessage());

		}

	}

	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public void addAdBranchSalesperson(LocalAdBranchSalesperson adBranchSalesperson) {

		Debug.print("AdBranchBean addAdBranchSalesperson");

		try {

			Collection adBranchSalespersons = getAdBranchSalespersons();
			adBranchSalespersons.add(adBranchSalesperson);

		} catch (Exception ex) {

			throw new EJBException(ex.getMessage());

		}

	}

	/**
	 * @ejb:interface-method view-type="local"
	 **/ 
	public void dropAdBranchSalesperson(LocalAdBranchSalesperson adBranchSalesperson) {

		Debug.print("AdBranchBean dropAdBranchSalesperson");

		try {

			Collection adBranchSalespersons = getAdBranchSalespersons();
			adBranchSalespersons.remove(adBranchSalesperson);

		} catch (Exception ex) {

			throw new EJBException(ex.getMessage());

		}

	}

	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public void addInvBranchStockTransfer(LocalInvBranchStockTransfer invBranchStockTransfer) {

		Debug.print("AdBranchBean addInvBranchStockTransfer");

		try {

			Collection invBranchStockTransfers = getInvBranchStockTransfers();
			invBranchStockTransfers.add(invBranchStockTransfer);

		} catch (Exception ex) {

			throw new EJBException(ex.getMessage());

		}

	}

	/**
	 * @ejb:interface-method view-type="local"
	 **/ 
	public void dropInvBranchStockTransfer(LocalInvBranchStockTransfer  invBranchStockTransfer) {

		Debug.print("AdBranchBean dropInvBranchStockTransfer");

		try {

			Collection invBranchStockTransfers = getInvBranchStockTransfers();
			invBranchStockTransfers.remove(invBranchStockTransfer);

		} catch (Exception ex) {

			throw new EJBException(ex.getMessage());

		}

	}


	// ENTITY METHODS

	/**
	 * @ejb:create-method view-type="local"
	 **/
	public Integer ejbCreate(Integer BR_CODE, String BR_BRNCH_CODE, String BR_NM, 
			String BR_DESC, String BR_TYP, byte BR_HD_QTR, String BR_ADDRSS, String BR_CNTCT_PRSN,
			String BR_CNTCT_NMBR, char BR_DWNLD_STATUS, byte BR_APPLY_SHPPNG, double BR_PRCNT_MRKP, Integer BR_AD_CMPNY)
	throws CreateException {

		Debug.print("AdBranchBean ejbCreate");

		setBrCode(BR_CODE);
		setBrBranchCode(BR_BRNCH_CODE);
		setBrName(BR_NM);
		setBrDescription(BR_DESC);
		setBrType(BR_TYP);
		setBrHeadQuarter(BR_HD_QTR);
		setBrAddress(BR_ADDRSS);
		setBrContactPerson(BR_CNTCT_PRSN);
		setBrContactNumber(BR_CNTCT_NMBR);
		setBrDownloadStatus(BR_DWNLD_STATUS);
		setBrApplyShipping(BR_APPLY_SHPPNG);
		setBrPercentMarkup(BR_PRCNT_MRKP);
		setBrAdCompany(BR_AD_CMPNY);

		return null;

	}

	/**
	 * @ejb:create-method view-type="local"
	 **/
	public Integer ejbCreate(String BR_BRNCH_CODE, String BR_NM, String BR_DESC, 
			String BR_TYP, byte BR_HD_QTR, String BR_ADDRSS, String BR_CNTCT_PRSN,
			String BR_CNTCT_NMBR, char BR_DWNLD_STATUS, byte BR_APPLY_SHPPNG, double BR_PRCNT_MRKP, Integer BR_AD_CMPNY) 
	throws CreateException {

		Debug.print("AdBranchBean ejbCreate");

		setBrBranchCode(BR_BRNCH_CODE);
		setBrName(BR_NM);
		setBrDescription(BR_DESC);
		setBrType(BR_TYP);
		setBrHeadQuarter(BR_HD_QTR);
		setBrAddress(BR_ADDRSS);
		setBrContactPerson(BR_CNTCT_PRSN);
		setBrContactNumber(BR_CNTCT_NMBR);
		setBrDownloadStatus(BR_DWNLD_STATUS);
		setBrApplyShipping(BR_APPLY_SHPPNG);
		setBrPercentMarkup(BR_PRCNT_MRKP);
		setBrAdCompany(BR_AD_CMPNY);

		return null;

	}

	public void ejbPostCreate(Integer BR_CODE, String BR_BRNCH_CODE, String BR_NM, 
			String BR_DESC, String BR_TYP, byte BR_HD_QTR, String BR_ADDRSS, String BR_CNTCT_PRSN,
			String BR_CNTCT_NMBR, char BR_DWNLD_STATUS, byte BR_APPLY_SHPPNG, double BR_PRCNT_MRKP, Integer BR_AD_CMPNY)
	throws CreateException { }

	public void ejbPostCreate(String BR_BRNCH_CODE, String BR_NM, 
			String BR_DESC, String BR_TYP, byte BR_HD_QTR, String BR_ADDRSS, String BR_CNTCT_PRSN,
			String BR_CNTCT_NMBR, char BR_DWNLD_STATUS, byte BR_APPLY_SHPPNG, double BR_PRCNT_MRKP, Integer BR_AD_CMPNY)
	throws CreateException { }

}
