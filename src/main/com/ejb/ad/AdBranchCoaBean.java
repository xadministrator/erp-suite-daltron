package com.ejb.ad;

import javax.ejb.CreateException;

import com.ejb.gl.LocalGlChartOfAccount;
import com.util.AbstractEntityBean;
import com.util.Debug;

/**
*
* @author  Arnel A. Masikip
*
* @ejb:bean name="AdBranchCoaBeanEJB"
*           display-name="Branch COA Entity"
*           type="CMP"
*           cmp-version="2.x"
*           view-type="local"
*           local-jndi-name="omega-ejb/AdBranchCoaEJB"
*           schema="AdBranchCoa"
*           primkey-field="bcoaCode"
*       
* @ejb:pk class="java.lang.Integer"
*
* @ejb:transaction type="Required"
*
* @ejb:security-role-ref role-name="aduser"
*                        role-link="aduserlink"
*
* @ejb:permission role-name="aduser"
*
* @ejb:interface local-class="com.ejb.ad.LocalAdBranchCoa"
*                local-extends="javax.ejb.EJBLocalObject"
*
* @ejb:home local-class="com.ejb.ad.LocalAdBranchCoaHome"
*           local-extends="javax.ejb.EJBLocalHome"
* 
* @ejb:finder signature="Collection findBcoaByCoaCodeAndRsName(java.lang.Integer COA_CODE, java.lang.String RS_NM, java.lang.Integer AD_CMPNY)"
* 			query="SELECT OBJECT(bcoa) FROM AdBranchCoa bcoa, IN(bcoa.adBranch.adBranchResponsibilities) brs WHERE bcoa.glChartOfAccount.coaCode = ?1 AND brs.adResponsibility.rsName = ?2 AND bcoa.bcoaAdCompany = ?3"
* 
* @ejb:finder signature="Collection findBcoaByCoaCode(java.lang.Integer COA_CODE, java.lang.Integer AD_CMPNY)"
* 			query="SELECT OBJECT(bcoa) FROM AdBranchCoa bcoa WHERE bcoa.glChartOfAccount.coaCode = ?1 AND bcoa.bcoaAdCompany = ?2"
*
* @ejb:value-object match="*"
*             name="AdBranchCoa"
*
* @jboss:persistence table-name="AD_BR_COA"
*
* @jboss:entity-command name="mysql-get-generated-keys"
*
*/
public abstract class AdBranchCoaBean extends AbstractEntityBean {
    
    // PERSISTENT FIELDS

    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     * @ejb:pk-field
     *
     * @jboss:column-name name="BCOA_CODE"
     *
     * @jboss:persistence auto-increment="true"
     **/
    public abstract java.lang.Integer getBcoaCode();
    public abstract void setBcoaCode(java.lang.Integer BCOA_CODE);

    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="BCOA_AD_CMPNY"
     **/
    public abstract Integer getBcoaAdCompany();
    /**
 	* @ejb:interface-method view-type="local"
 	**/     
    public abstract void setBcoaAdCompany(Integer BCOA_AD_CMPNY);
    
    // RELATIONSHIP FIELDS
    
    /**
     * @ejb:interface-method view-type="local"
     * @ejb:relation name="branch-branchcoa"
     *               role-name="branchcoa-has-one-branch"
     *				 cascade-delete="yes"
     * 
     * @jboss:relation related-pk-field="brCode"
     *                 fk-column="AD_BRANCH"
     */
     public abstract LocalAdBranch getAdBranch();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
     public abstract void setAdBranch(LocalAdBranch adBranch);
     
     /**
      * @ejb:interface-method view-type="local"
      * @ejb:relation name="chartofaccount-branchcoa"
      *              role-name="branchcoa-has-one-chartofaccount"
      *				 cascade-delete="yes"
      * 
      * @jboss:relation related-pk-field="coaCode"
      *                 fk-column="GL_COA"
      */
      public abstract LocalGlChartOfAccount getGlChartOfAccount();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
      public abstract void setGlChartOfAccount(LocalGlChartOfAccount glChartOfAccount);
      
      // ENTITY METHODS
      
      /**
       * @ejb:create-method view-type="local"
       **/
      public Integer ejbCreate(Integer BCOA_CODE, Integer BCOA_AD_CMPNY)
      throws CreateException {
          
          Debug.print("AdBranchDocumentSequenceAssignmentBean ejbCreate");
          
          setBcoaCode(BCOA_CODE);            
          setBcoaAdCompany(BCOA_AD_CMPNY);
          
          return null;
      }
      
      /**
       * @ejb:create-method view-type="local"
       **/
      public Integer ejbCreate(Integer BCOA_AD_CMPNY)
      throws CreateException {
          
          Debug.print("AdBranchDocumentSequenceAssignmentBean ejbCreate");
          
          setBcoaAdCompany(BCOA_AD_CMPNY);
          
          return null;
      }
      
      public void ejbPostCreate(Integer BCOA_CODE, Integer BCOA_AD_CMPNY)
      throws CreateException { }
      
      public void ejbPostCreate(Integer BCOA_AD_CMPNY)
      throws CreateException { }
}
