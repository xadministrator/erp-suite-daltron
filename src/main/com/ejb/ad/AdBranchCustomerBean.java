package com.ejb.ad;

import javax.ejb.CreateException;

import com.ejb.ar.LocalArCustomer;
import com.util.AbstractEntityBean;
import com.util.Debug;

/**
 * @author Franco Antonio R. Roig
 * 		   Created on Oct 20, 2005
 * 
 *
 * @ejb:bean name="AdBranchCustomerEJB"
 *           display-name="Branch Customer Entity"
 *           type="CMP"
 *           cmp-version="2.x"
 *           view-type="local"
 *           local-jndi-name="omega-ejb/AdBranchCustomerEJB"
 *           schema="AdBranchCustomer"
 *           primkey-field="bcstCode"
 *       
 * @ejb:pk class="java.lang.Integer"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="aduser"
 *                        role-link="aduserlink"
 *
 * @ejb:permission role-name="aduser"
 *
 * @ejb:interface local-class="com.ejb.ad.LocalAdBranchCustomer"
 *                local-extends="javax.ejb.EJBLocalObject"
 *
 * @ejb:home local-class="com.ejb.ad.LocalAdBranchCustomerHome"
 *           local-extends="javax.ejb.EJBLocalHome"
 * 
 * @ejb:finder signature="Collection findBcstByCstCode(java.lang.Integer CST_CODE, java.lang.Integer AD_CMPNY)"
 * 			  query="SELECT OBJECT(bcst) FROM AdBranchCustomer bcst WHERE bcst.arCustomer.cstCode = ?1 AND bcst.bcstAdCompany = ?2"
 * 
 * @ejb:finder signature="Collection findBcstByCstCodeAndRsName(java.lang.Integer CST_CODE, java.lang.String RS_NM, java.lang.Integer AD_CMPNY)"
 * 			  query="SELECT OBJECT(bcst) FROM AdBranchCustomer bcst, IN(bcst.adBranch.adBranchResponsibilities) brs WHERE bcst.arCustomer.cstCode = ?1 AND brs.adResponsibility.rsName = ?2 AND bcst.bcstAdCompany = ?3"
 *  
 * @ejb:finder signature="LocalAdBranchCustomer findBcstByCstCodeAndBrCode(java.lang.Integer CST_CODE, java.lang.Integer BR_CODE, java.lang.Integer AD_CMPNY)"
 * 			  query="SELECT OBJECT(bcst) FROM AdBranchCustomer bcst WHERE bcst.arCustomer.cstCode = ?1 AND bcst.adBranch.brCode = ?2 AND bcst.bcstAdCompany = ?3"
 * 
 * @ejb:finder signature="Collection findCstByCstNewAndUpdated(java.lang.Integer BR_CODE, java.lang.Integer AD_CMPNY, char NEW, char UPDATED, char DOWNLOADED_UPDATED)"
 * 			  query="SELECT OBJECT(bcst) FROM AdBranchCustomer bcst WHERE (bcst.bcstCustomerDownloadStatus = ?3 OR bcst.bcstCustomerDownloadStatus = ?4 OR bcst.bcstCustomerDownloadStatus = ?5) AND bcst.adBranch.brCode = ?1 AND bcst.bcstAdCompany = ?2"
 * 
 * @ejb:finder signature="Collection findByBcstGlCoaReceivableAccount(java.lang.Integer BR_CODE, java.lang.Integer BCST_AD_CMPNY)"
 *             query="SELECT OBJECT(bcst) FROM AdBranchCustomer bcst WHERE bcst.bcstGlCoaReceivableAccount=?1 AND bcst.bcstAdCompany = ?2"
 * 
 * @ejb:finder signature="Collection findByBcstGlCoaRevenueAccount(java.lang.Integer BR_CODE, java.lang.Integer BCST_AD_CMPNY)"
 *             query="SELECT OBJECT(bcst) FROM AdBranchCustomer bcst WHERE bcst.bcstGlCoaRevenueAccount=?1 AND bcst.bcstAdCompany = ?2"
 *
 * @ejb:value-object match="*"
 *             name="AdBranchCustomer"
 *
 * @jboss:persistence table-name="AD_BR_CST"
 *
 * @jboss:entity-command name="mysql-get-generated-keys"
 *
 */
public abstract class AdBranchCustomerBean extends AbstractEntityBean {

	// PERSISTENT METHODS
	
    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     * @ejb:pk-field
     *
     * @jboss:column-name name="BCST_CODE"
     *
     * @jboss:persistence auto-increment="true"
     **/
    public abstract Integer getBcstCode();
    public abstract void setBcstCode(Integer BCST_CODE);
	
    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"   
     *
     * @jboss:column-name name="BCST_GL_COA_RCVBL_ACCNT"
     **/
    public abstract Integer getBcstGlCoaReceivableAccount();
     /**
      * @ejb:interface-method view-type="local"
      **/ 
    public abstract void setBcstGlCoaReceivableAccount(Integer BCST_GL_COA_RCVBL_ACCNT);
    
    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"   
     *
     * @jboss:column-name name="BCST_GL_COA_RVN_ACCNT"
     **/
    public abstract Integer getBcstGlCoaRevenueAccount();
     /**
      * @ejb:interface-method view-type="local"
      **/ 
    public abstract void setBcstGlCoaRevenueAccount(Integer BCST_GL_COA_RVN_ACCNT);
    
    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"   
     *
     * @jboss:column-name name="BCST_GL_COA_UNERND_INT_ACCNT"
     **/
    public abstract Integer getBcstGlCoaUnEarnedInterestAccount();
     /**
      * @ejb:interface-method view-type="local"
      **/ 
    public abstract void setBcstGlCoaUnEarnedInterestAccount(Integer BCST_GL_COA_UNERND_INT_ACCNT);
    
    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"   
     *
     * @jboss:column-name name="BCST_GL_COA_ERND_INT_ACCNT"
     **/
    public abstract Integer getBcstGlCoaEarnedInterestAccount();
     /**
      * @ejb:interface-method view-type="local"
      **/ 
    public abstract void setBcstGlCoaEarnedInterestAccount(Integer BCST_GL_COA_ERND_INT_ACCNT);
    
    
    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"   
     *
     * @jboss:column-name name="BCST_GL_COA_UNERND_PNT_ACCNT"
     **/
    public abstract Integer getBcstGlCoaUnEarnedPenaltyAccount();
     /**
      * @ejb:interface-method view-type="local"
      **/ 
    public abstract void setBcstGlCoaUnEarnedPenaltyAccount(Integer BCST_GL_COA_UNERND_PNT_ACCNT);
    
    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"   
     *
     * @jboss:column-name name="BCST_GL_COA_ERND_PNT_ACCNT"
     **/
    public abstract Integer getBcstGlCoaEarnedPenaltyAccount();
     /**
      * @ejb:interface-method view-type="local"
      **/ 
    public abstract void setBcstGlCoaEarnedPenaltyAccount(Integer BCST_GL_COA_ERND_PNT_ACCNT);
      
    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"   
     *
     * @jboss:column-name name="BCST_DS_CSTMR"
     **/
    public abstract char getBcstCustomerDownloadStatus();
     /**
      * @ejb:interface-method view-type="local"
      **/ 
    public abstract void setBcstCustomerDownloadStatus(char BCST_DS_CSTMR);
    
    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"   
     *
     * @jboss:column-name name="AR_CUSTOMER"
     **/
    public abstract Integer getBcstArCustomer();
     /**
      * @ejb:interface-method view-type="local"
      **/ 
    public abstract void setBcstArCustomer(Integer AR_CUSTOMER);

    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"   
     *
     * @jboss:column-name name="AD_BRANCH"
     **/
    public abstract Integer getBcstAdBranch();
     /**
      * @ejb:interface-method view-type="local"
      **/ 
    public abstract void setBcstAdBranch(Integer AD_BRANCH);

    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"   
     *
     * @jboss:column-name name="BCST_AD_CMPNY"
     **/
    public abstract Integer getBcstAdCompany();
     /**
      * @ejb:interface-method view-type="local"
      **/ 
    public abstract void setBcstAdCompany(Integer BCST_AD_CMPNY);
    
	// RELATIONSHIP METHODS
	
    /**
     * @ejb:interface-method view-type="local"
     * @ejb:relation name="branch-branchcustomers"
     *               role-name="branchcustomer-has-one-branch"
     *
     * @jboss:relation related-pk-field="brCode"
     *                 fk-column="AD_BRANCH"
     */
    public abstract LocalAdBranch getAdBranch();
    /**
	 * @ejb:interface-method view-type="local"
	 **/
    public abstract void setAdBranch(LocalAdBranch adBranch);

    /**
     * @ejb:interface-method view-type="local"
     * @ejb:relation name="customer-branchcustomers"
     *               role-name="branchcustomer-has-one-customer"
     * 
     * @jboss:relation related-pk-field="cstCode"
     * 				   fk-column="AR_CUSTOMER"
     */
    public abstract LocalArCustomer getArCustomer();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
    public abstract void setArCustomer(LocalArCustomer arCustomer);
    
    
	// ENTITY BEAN METHODS
	
    /**
     * @ejb:create-method view-type="local"
     **/
    public Integer ejbCreate(Integer BCST_CODE, 
    	 Integer BCST_GL_COA_RCVBL_ACCNT, 
    	 Integer BCST_GL_COA_RVN_ACCNT,
    	 Integer BCST_GL_COA_UNERND_INT_ACCNT,
    	 Integer BCST_GL_COA_ERND_INT_ACCNT,
    	 Integer BCST_GL_COA_UNERND_PNT_ACCNT,
    	 Integer BCST_GL_COA_ERND_PNT_ACCNT,
    	 char BCST_DS_CSTMR, Integer BCST_AD_CMPNY)     	
         throws CreateException {	
     	
         Debug.print("AdBranchCustomerBean ejbCreate");
     
         setBcstCode(BCST_CODE);
         setBcstGlCoaReceivableAccount(BCST_GL_COA_RCVBL_ACCNT);
         setBcstGlCoaRevenueAccount(BCST_GL_COA_RVN_ACCNT);
         setBcstGlCoaUnEarnedInterestAccount(BCST_GL_COA_UNERND_INT_ACCNT);
         setBcstGlCoaEarnedInterestAccount(BCST_GL_COA_ERND_INT_ACCNT);
         setBcstGlCoaUnEarnedPenaltyAccount(BCST_GL_COA_UNERND_PNT_ACCNT);
         setBcstGlCoaEarnedPenaltyAccount(BCST_GL_COA_ERND_PNT_ACCNT);
         
         setBcstCustomerDownloadStatus(BCST_DS_CSTMR);
         setBcstAdCompany(BCST_AD_CMPNY);
         
         return null;
     }
     
    /**
     * @ejb:create-method view-type="local"
     **/    
    public Integer ejbCreate(Integer BCST_GL_COA_RCVBL_ACCNT, 
    		Integer BCST_GL_COA_RVN_ACCNT, 
    		Integer BCST_GL_COA_UNERND_INT_ACCNT,
       	 	Integer BCST_GL_COA_ERND_INT_ACCNT,
       	 	Integer BCST_GL_COA_UNERND_PNT_ACCNT,
       		Integer BCST_GL_COA_ERND_PNT_ACCNT,
    		char BCST_DS_CSTMR, Integer BCST_AD_CMPNY)     	
         throws CreateException {	
     	
         Debug.print("AdBranchCustomerBean ejbCreate");
     
         setBcstGlCoaReceivableAccount(BCST_GL_COA_RCVBL_ACCNT);
         setBcstGlCoaRevenueAccount(BCST_GL_COA_RVN_ACCNT);
         setBcstGlCoaUnEarnedInterestAccount(BCST_GL_COA_UNERND_INT_ACCNT);
         setBcstGlCoaEarnedInterestAccount(BCST_GL_COA_ERND_INT_ACCNT);
         setBcstGlCoaUnEarnedPenaltyAccount(BCST_GL_COA_UNERND_PNT_ACCNT);
         setBcstGlCoaEarnedPenaltyAccount(BCST_GL_COA_ERND_PNT_ACCNT);
         
         setBcstCustomerDownloadStatus(BCST_DS_CSTMR);
         setBcstAdCompany(BCST_AD_CMPNY);
         
         return null;
     }

    public void ejbPostCreate(Integer BCST_CODE, Integer BCST_GL_COA_RCVBL_ACCNT, 
    		Integer BCST_GL_COA_RVN_ACCNT, 
    		Integer BCST_GL_COA_UNERND_INT_ACCNT,
       	 	Integer BCST_GL_COA_ERND_INT_ACCNT,
       	 Integer BCST_GL_COA_UNERND_PNT_ACCNT,
    	 Integer BCST_GL_COA_ERND_PNT_ACCNT,
    		char BCST_DS_CSTMR, Integer BCST_AD_CMPNY)
         throws CreateException { }

    public void ejbPostCreate(Integer BCST_GL_COA_RCVBL_ACCNT, 
    		Integer BCST_GL_COA_RVN_ACCNT, 
    		Integer BCST_GL_COA_UNERND_INT_ACCNT,
       	 	Integer BCST_GL_COA_ERND_INT_ACCNT,
       	 Integer BCST_GL_COA_UNERND_PNT_ACCNT,
    	 Integer BCST_GL_COA_ERND_PNT_ACCNT,
    		char BCST_DS_CSTMR, Integer BCST_AD_CMPNY)
         throws CreateException { }
      
}
