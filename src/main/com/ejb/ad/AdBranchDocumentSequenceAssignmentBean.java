package com.ejb.ad;

import javax.ejb.CreateException;

import com.ejb.gl.LocalAdDocumentSequenceAssignment;
import com.util.AbstractEntityBean;
import com.util.Debug;

/**
*
* @author  Arnel A. Masikip
*
* @ejb:bean name="AdBranchDocumentSequenceAssignmentBeanEJB"
*           display-name="Branch Document Sequence Assignment Entity"
*           type="CMP"
*           cmp-version="2.x"
*           view-type="local"
*           local-jndi-name="omega-ejb/AdBranchDocumentSequenceAssignmentEJB"
*           schema="AdBranchDocumentSequenceAssignment"
*           primkey-field="bdsCode"
*       
* @ejb:pk class="java.lang.Integer"
*
* @ejb:transaction type="Required"
*
* @ejb:security-role-ref role-name="aduser"
*                        role-link="aduserlink"
*
* @ejb:permission role-name="aduser"
*
* @ejb:interface local-class="com.ejb.ad.LocalAdBranchDocumentSequenceAssignment"
*                local-extends="javax.ejb.EJBLocalObject"
*
* @ejb:home local-class="com.ejb.ad.LocalAdBranchDocumentSequenceAssignmentHome"
*           local-extends="javax.ejb.EJBLocalHome"
* 
* @ejb:finder signature="Collection findBdsByDsaCodeAndRsName(java.lang.Integer DSA_CODE, java.lang.String RS_NM, java.lang.Integer AD_CMPNY)"
* 			  query="SELECT OBJECT(bds) FROM AdBranchDocumentSequenceAssignment bds, IN(bds.adBranch.adBranchResponsibilities) brs WHERE bds.adDocumentSequenceAssignment.dsaCode = ?1 AND brs.adResponsibility.rsName = ?2 AND bds.bdsAdCompany = ?3"
* 
* @ejb:finder signature="LocalAdBranchDocumentSequenceAssignment findBdsByDsaCodeAndBrCode(java.lang.Integer DSA_CODE, java.lang.Integer AD_BRNCH, java.lang.Integer AD_CMPNY)"
* 			  query="SELECT OBJECT(bds) FROM AdBranchDocumentSequenceAssignment bds WHERE bds.adDocumentSequenceAssignment.dsaCode = ?1 AND bds.adBranch.brCode = ?2 AND bds.bdsAdCompany = ?3"
* 
* @ejb:finder signature="Collection findBdsByDsaCode(java.lang.Integer DSA_CODE, java.lang.Integer AD_CMPNY)"
* 			  query="SELECT OBJECT(bds) FROM AdBranchDocumentSequenceAssignment bds WHERE bds.adDocumentSequenceAssignment.dsaCode = ?1 AND bds.bdsAdCompany = ?2"
*
* @ejb:value-object match="*"
*             name="AdBranchDocumentSequenceAssignment"
*
* @jboss:persistence table-name="AD_BR_DSA"
*
* @jboss:entity-command name="mysql-get-generated-keys"
*
*/
public abstract class AdBranchDocumentSequenceAssignmentBean extends AbstractEntityBean {
    
    // PERSISTENT FIELDS

    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     * @ejb:pk-field
     *
     * @jboss:column-name name="BDS_CODE"
     *
     * @jboss:persistence auto-increment="true"
     **/
    public abstract java.lang.Integer getBdsCode();
    public abstract void setBdsCode(java.lang.Integer BDS_CODE);

    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="BDS_NXT_SQNC"
     **/
    public abstract String getBdsNextSequence();
    /**
 	* @ejb:interface-method view-type="local"
 	**/     
    public abstract void setBdsNextSequence(String BDS_NXT_SQNC);
    
    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="BDS_AD_CMPNY"
     **/
    public abstract Integer getBdsAdCompany();
    /**
 	* @ejb:interface-method view-type="local"
 	**/     
    public abstract void setBdsAdCompany(Integer BDS_AD_CMPNY);
    
    // RELATIONSHIP METHODS
    
    /**
     * @ejb:interface-method view-type="local"
     * @ejb:relation name="branch-branchdocumentsequenceassignment"
     *               role-name="branchdocumentsequenceassignment-has-one-branch"
     *				 cascade-delete="yes"
     * 
     * @jboss:relation related-pk-field="brCode"
     *                 fk-column="AD_BRANCH"
     */
     public abstract LocalAdBranch getAdBranch();
     public abstract void setAdBranch(LocalAdBranch adBranch);
     
     /**
      * @ejb:interface-method view-type="local"
      * @ejb:relation name="documentsequenceassignment-branchdocumentsequenceassignment"
      *              role-name="branchdocumentsequenceassignment-has-one-documentsequenceassignment"
      *				 cascade-delete="yes"
      * 
      * @jboss:relation related-pk-field="dsaCode"
      *                 fk-column="AD_DOCUMENT_SEQUENCE_ASSIGNMENT"
      */
      public abstract LocalAdDocumentSequenceAssignment getAdDocumentSequenceAssignment();
      public abstract void setAdDocumentSequenceAssignment(LocalAdDocumentSequenceAssignment adDsa);
      
      // BUSINESS METHODS
      
      // ENTITY METHODS
      
      /**
       * @ejb:create-method view-type="local"
       **/
       public Integer ejbCreate(Integer BDS_CODE, String BDS_NXT_SQNC, Integer BDS_AD_CMPNY)
           throws CreateException {
        	
           Debug.print("AdBranchDocumentSequenceAssignmentBean ejbCreate");
        
           setBdsCode(BDS_CODE); 
           setBdsNextSequence(BDS_NXT_SQNC);
           setBdsAdCompany(BDS_AD_CMPNY);
          
           return null;
       } 
       
       /**
        * @ejb:create-method view-type="local"
        **/
        public Integer ejbCreate(String BDS_NXT_SQNC, Integer BDS_AD_CMPNY)
            throws CreateException {
         	
            Debug.print("AdBranchDocumentSequenceAssignmentBean ejbCreate");
                   
            setBdsNextSequence(BDS_NXT_SQNC);
            setBdsAdCompany(BDS_AD_CMPNY);
           
            return null;
        }
        
        public void ejbPostCreate(Integer BDS_CODE, String BDS_NXT_SQNC, Integer BDS_AD_CMPNY) 
        	throws CreateException { }
        
        public void ejbPostCreate(String BDS_NXT_SQNC, Integer BDS_AD_CMPNY) 
    		throws CreateException { }
}
