package com.ejb.ad;

import java.util.Collection;

import javax.ejb.CreateException;
import javax.ejb.FinderException;

import com.ejb.inv.LocalInvItemLocation;
import com.util.AbstractEntityBean;
import com.util.Debug;

/**
*
* @author  Arnel A. Masikip
*
* @ejb:bean name="AdBranchItemLocationEJB"
*           display-name="Branch Item Location Entity"
*           type="CMP"
*           cmp-version="2.x"
*           view-type="local"
*           local-jndi-name="omega-ejb/AdBranchItemLocationEJB"
*           schema="AdBranchItemLocation"
*           primkey-field="bilCode"
*
* @ejb:pk class="java.lang.Integer"
*
* @ejb:transaction type="Required"
*
* @ejb:security-role-ref role-name="aduser"
*                        role-link="aduserlink"
*
* @ejb:permission role-name="aduser"
*
* @ejb:interface local-class="com.ejb.ad.LocalAdBranchItemLocation"
*                local-extends="javax.ejb.EJBLocalObject"
*
* @ejb:home local-class="com.ejb.ad.LocalAdBranchItemLocationHome"
*           local-extends="javax.ejb.EJBLocalHome"
*
* @ejb:finder signature="Collection findByInvIlAll(java.lang.Integer ITM_LCTN_CODE, java.lang.Integer BIL_AD_CMPNY)"
*             query="SELECT OBJECT(bil) FROM AdBranchItemLocation bil WHERE bil.invItemLocation.ilCode = ?1 AND bil.bilAdCompany = ?2"
*
* @ejb:finder signature="Collection findBilByIlCodeAndRsCode(java.lang.Integer IL_CODE, java.lang.Integer RS_CODE, java.lang.Integer AD_CMPNY)"
* 			  query="SELECT OBJECT(bil) FROM AdBranchItemLocation bil, IN(bil.adBranch.adBranchResponsibilities)brs WHERE bil.invItemLocation.ilCode = ?1 AND brs.adResponsibility.rsCode = ?2 AND bil.bilAdCompany = ?3"
*
* @ejb:finder signature="LocalAdBranchItemLocation findBilByIlCodeAndBrCode(java.lang.Integer IL_CODE, java.lang.Integer BR_CODE, java.lang.Integer AD_CMPNY)"
* 			  query="SELECT OBJECT(bil) FROM AdBranchItemLocation bil WHERE bil.invItemLocation.ilCode = ?1 AND  bil.adBranch.brCode = ?2 AND bil.bilAdCompany = ?3"
*
* @ejb:finder signature="Collection findBilByIiCodeAndBrCode(java.lang.Integer II_CODE, java.lang.Integer BR_CODE, java.lang.Integer AD_CMPNY)"
* 			  query="SELECT OBJECT(bil) FROM AdBranchItemLocation bil WHERE bil.invItemLocation.invItem.iiCode = ?1 AND bil.adBranch.brCode = ?2 AND bil.bilAdCompany = ?3"
*
* @ejb:finder signature="Collection findEnabledIiByIiNewAndUpdated(java.lang.Integer BR_CODE, java.lang.String II_IL_LOCATION, java.lang.Integer AD_CMPNY, char NEW, char UPDATED, char DOWNLOADED_UPDATED)"
* 			  query="SELECT OBJECT(bil) FROM AdBranchItemLocation bil WHERE (bil.bilItemDownloadStatus = ?4 OR bil.bilItemDownloadStatus = ?5 OR bil.bilItemDownloadStatus = ?6) AND bil.invItemLocation.invItem.iiEnable=1 AND bil.invItemLocation.invItem.iiRetailUom IS NOT NULL AND bil.adBranch.brCode = ?1 AND bil.invItemLocation.invLocation.locLvType = ?2 AND bil.bilAdCompany = ?3"
*
* @ejb:finder signature="Collection findAllIiByIiNewAndUpdated(java.lang.Integer BR_CODE, java.lang.String II_IL_LOCATION, java.lang.Integer AD_CMPNY, char NEW, char UPDATED, char DOWNLOADED_UPDATED)"
* 			  query="SELECT OBJECT(bil) FROM AdBranchItemLocation bil WHERE (bil.bilItemDownloadStatus = ?4 OR bil.bilItemDownloadStatus = ?5 OR bil.bilItemDownloadStatus = ?6) AND bil.invItemLocation.invItem.iiRetailUom IS NOT NULL AND bil.adBranch.brCode = ?1 AND bil.invItemLocation.invLocation.locLvType = ?2 AND bil.bilAdCompany = ?3"
*
* @ejb:finder signature="Collection findEnabledIiByIiNewAndUpdated(java.lang.Integer BR_CODE, java.lang.Integer AD_CMPNY, char NEW, char UPDATED, char DOWNLOADED_UPDATED)"
* 			  query="SELECT OBJECT(bil) FROM AdBranchItemLocation bil WHERE (bil.bilItemDownloadStatus = ?3 OR bil.bilItemDownloadStatus = ?4 OR bil.bilItemDownloadStatus = ?5) AND bil.invItemLocation.invItem.iiEnable=1 AND bil.adBranch.brCode = ?1 AND bil.bilAdCompany = ?2"
*
* @ejb:finder signature="Collection findLocByLocNewAndUpdated(java.lang.Integer BR_CODE, java.lang.Integer AD_CMPNY, char NEW, char UPDATED, char DOWNLOADED_UPDATED)"
* 			  query="SELECT OBJECT(bil) FROM AdBranchItemLocation bil WHERE (bil.bilLocationDownloadStatus = ?3 OR bil.bilLocationDownloadStatus = ?4 OR bil.bilLocationDownloadStatus = ?5) AND bil.adBranch.brCode = ?1 AND bil.bilAdCompany = ?2"
*
* @ejb:finder signature="Collection findIlByIlNewAndUpdated(java.lang.Integer BR_CODE, java.lang.Integer AD_CMPNY, char NEW, char UPDATED, char DOWNLOADED_UPDATED)"
* 			  query="SELECT OBJECT(bil) FROM AdBranchItemLocation bil WHERE (bil.bilItemLocationDownloadStatus = ?3 OR bil.bilItemLocationDownloadStatus = ?4 OR bil.bilItemLocationDownloadStatus = ?5) AND bil.adBranch.brCode = ?1 AND bil.bilAdCompany = ?2"
*
*  @ejb:finder signature="Collection findByBilGlCoaSalesAccount(java.lang.Integer BIL_CODE, java.lang.Integer BIL_AD_CMPNY)"
*             query="SELECT OBJECT(bil) FROM AdBranchItemLocation bil WHERE bil.bilCoaGlSalesAccount=?1 AND bil.bilAdCompany = ?2"
*
* @ejb:finder signature="Collection findByBilGlCoaInventoryAccount(java.lang.Integer BIL_CODE, java.lang.Integer BIL_AD_CMPNY)"
*             query="SELECT OBJECT(bil) FROM AdBranchItemLocation bil WHERE bil.bilCoaGlInventoryAccount=?1 AND bil.bilAdCompany = ?2"
*
* @ejb:finder signature="Collection findByBilGlCoaCostOfSalesAccount(java.lang.Integer BIL_CODE, java.lang.Integer BIL_AD_CMPNY)"
*             query="SELECT OBJECT(bil) FROM AdBranchItemLocation bil WHERE bil.bilCoaGlCostOfSalesAccount=?1 AND bil.bilAdCompany = ?2"
*
* @ejb:finder signature="Collection findByBilGlCoaWipAccount(java.lang.Integer BIL_CODE, java.lang.Integer BIL_AD_CMPNY)"
*             query="SELECT OBJECT(bil) FROM AdBranchItemLocation bil WHERE bil.bilCoaGlWipAccount=?1 AND bil.bilAdCompany = ?2"
*
* @ejb:finder signature="Collection findByBilGlCoaAccruedInventoryAccount(java.lang.Integer BIL_CODE, java.lang.Integer BIL_AD_CMPNY)"
*             query="SELECT OBJECT(bil) FROM AdBranchItemLocation bil WHERE bil.bilCoaGlAccruedInventoryAccount=?1 AND bil.bilAdCompany = ?2"
*
*
* @ejb:select signature="Collection ejbSelectGeneric(java.lang.String jbossQl, java.lang.Object[] args)"
*             result-type-mapping="Local"
*             method-intf="LocalHome"
*             query="SELECT OBJECT(bil) FROM AdBranchItemLocation bil"
*
* @jboss:query signature="Collection ejbSelectGeneric(java.lang.String jbossQl, java.lang.Object[] args)"
*                         dynamic="true"
*
* @ejb:value-object match="*"
*             name="AdBranchItemLocation"
*
* @jboss:persistence table-name="AD_BR_IL"
*
* @jboss:entity-command name="mysql-get-generated-keys"
*
*/
public abstract class AdBranchItemLocationBean extends AbstractEntityBean {

    //  PERSISTENT FIELDS

    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     * @ejb:pk-field
     *
     * @jboss:column-name name="BIL_CODE"
     *
     * @jboss:persistence auto-increment="true"
     **/
    public abstract java.lang.Integer getBilCode();
    public abstract void setBilCode(java.lang.Integer BIL_CODE);
    
    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="BIL_RCK"
     **/
    public abstract String getBilRack();
     /**
      * @ejb:interface-method view-type="local"
      **/
    public abstract void setBilRack(String BIL_RCK);
    
    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="BIL_BN"
     **/
    public abstract String getBilBin();
     /**
      * @ejb:interface-method view-type="local"
      **/
    public abstract void setBilBin(String BIL_BN);

    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="BIL_RRDR_PNT"
     **/
    public abstract double getBilReorderPoint();
    /**
     * @ejb:interface-method view-type="local"
     **/
    public abstract void setBilReorderPoint(double BIL_RRDR_PNT);

    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="BIL_RRDR_QTY"
     **/
    public abstract double getBilReorderQuantity();
    /**
     * @ejb:interface-method view-type="local"
     **/
    public abstract void setBilReorderQuantity(double BIL_RRDR_QTY);

    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="BIL_COA_GL_SLS_ACCNT"
     **/
    public abstract Integer getBilCoaGlSalesAccount();
    /**
     * @ejb:interface-method view-type="local"
     **/
    public abstract void setBilCoaGlSalesAccount(Integer BIL_COA_GL_SLS_ACCNT);

    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="BIL_COA_GL_INVNTRY_ACCNT"
     **/
    public abstract Integer getBilCoaGlInventoryAccount();
    /**
     * @ejb:interface-method view-type="local"
     **/
    public abstract void setBilCoaGlInventoryAccount(Integer BIL_COA_GL_INVNTRY_ACCNT);

    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="BIL_COA_GL_CST_OF_SLS_ACCNT"
     **/
    public abstract Integer getBilCoaGlCostOfSalesAccount();
    /**
     * @ejb:interface-method view-type="local"
     **/
    public abstract void setBilCoaGlCostOfSalesAccount(Integer BIL_COA_GL_CST_OF_SLS_ACCNT);

    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="BIL_COA_GL_WIP_ACCNT"
     **/
    public abstract Integer getBilCoaGlWipAccount();
    /**
     * @ejb:interface-method view-type="local"
     **/
    public abstract void setBilCoaGlWipAccount(Integer BIL_COA_GL_WIP_ACCNT);

    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="BIL_COA_GL_ACCRD_INVNTRY_ACCNT"
     **/
    public abstract Integer getBilCoaGlAccruedInventoryAccount();
    /**
     * @ejb:interface-method view-type="local"
     **/
    public abstract void setBilCoaGlAccruedInventoryAccount(Integer BIL_COA_GL_ACCRD_INVNTRY_ACCNT);

    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="BIL_COA_GL_SLS_RTRN_ACCNT"
     **/
    public abstract Integer getBilCoaGlSalesReturnAccount();
    /**
     * @ejb:interface-method view-type="local"
     **/
    public abstract void setBilCoaGlSalesReturnAccount(Integer BIL_COA_GL_SLS_RTRN_ACCNT);



    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="BIL_SBJCT_TO_CMMSSN"
     **/
    public abstract byte getBilSubjectToCommission();
    /**
     * @ejb:interface-method view-type="local"
     **/
    public abstract void setBilSubjectToCommission(byte BIL_SBJCT_TO_CMMSSN);

    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="BIL_HST1_SLS"
     **/
    public abstract double getBilHist1Sales();
    /**
     * @ejb:interface-method view-type="local"
     **/
    public abstract void setBilHist1Sales(double BIL_HIST1_SLS);
    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="BIL_HST2_SLS"
     **/
    public abstract double getBilHist2Sales();
    /**
     * @ejb:interface-method view-type="local"
     **/
    public abstract void setBilHist2Sales(double BIL_HIST2_SLS);
    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="BIL_PRJ_SLS"
     **/
    public abstract double getBilProjectedSales();
    /**
     * @ejb:interface-method view-type="local"
     **/
    public abstract void setBilProjectedSales(double BIL_PRJ_SLS);

    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="BIL_DLVRY_TME"
     **/
    public abstract double getBilDeliveryTime();
    /**
     * @ejb:interface-method view-type="local"
     **/
    public abstract void setBilDeliveryTime(double BIL_DLVRY_TME);

    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="BIL_DLVRY_BUFF"
     **/
    public abstract double getBilDeliveryBuffer();
    /**
     * @ejb:interface-method view-type="local"
     **/
    public abstract void setBilDeliveryBuffer(double BIL_DLVRY_BUFF);


    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="BIL_ORDR_PR_YR"
     **/
    public abstract double getBilOrderPerYear();
    /**
     * @ejb:interface-method view-type="local"
     **/
    public abstract void setBilOrderPerYear(double BIL_ORDR_PR_YR);

    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="BIL_DS_ITM"
     **/
    public abstract char getBilItemDownloadStatus();
    /**
     * @ejb:interface-method view-type="local"
     **/
    public abstract void setBilItemDownloadStatus(char BIL_DS_ITM);

    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="BIL_DS_LOC"
     **/
    public abstract char getBilLocationDownloadStatus();
    /**
     * @ejb:interface-method view-type="local"
     **/
    public abstract void setBilLocationDownloadStatus(char BIL_DS_LOC);

    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="BIL_DS_IL"
     **/
    public abstract char getBilItemLocationDownloadStatus();
    /**
     * @ejb:interface-method view-type="local"
     **/
    public abstract void setBilItemLocationDownloadStatus(char BIL_DS_IL);


    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="BIL_AD_CMPNY"
     **/
    public abstract Integer getBilAdCompany();
    /**
     * @ejb:interface-method view-type="local"
     **/
    public abstract void setBilAdCompany(Integer BIL_AD_CMPNY);


    // RELATIONSHIP FIELDS

    /**
     * @ejb:interface-method view-type="local"
     * @ejb:relation name="branch-branchitemlocation"
     *               role-name="branchitemlocation-has-one-branch"
     *				 cascade-delete="yes"
     *
     * @jboss:relation related-pk-field="brCode"
     *                 fk-column="AD_BRANCH"
     */
    public abstract LocalAdBranch getAdBranch();
    /**
     * @ejb:interface-method view-type="local"
     **/
    public abstract void setAdBranch(LocalAdBranch adBranch);

    /**
     * @ejb:interface-method view-type="local"
     * @ejb:relation name="itemlocation-branchitemlocation"
     *               role-name="branchitemlocation-has-one-itemlocation"
     *				 cascade-delete="yes"
     *
     * @jboss:relation related-pk-field="ilCode"
     *                 fk-column="INV_ITEM_LOCATION"
     */
    public abstract LocalInvItemLocation getInvItemLocation();
    /**
     * @ejb:interface-method view-type="local"
     **/
    public abstract void setInvItemLocation(LocalInvItemLocation invItemLocation);

	/**
	 * @jboss:dynamic-ql
	 */
	public abstract Collection ejbSelectGeneric(java.lang.String jbossQl, java.lang.Object[] args)
	throws FinderException;

    // BUSINESS METHODS

	/**
	 * @ejb:home-method view-type="local"
	 */
	public Collection ejbHomeGetBilByCriteria(java.lang.String jbossQl, java.lang.Object[] args)
	throws FinderException {

		return ejbSelectGeneric(jbossQl, args);
	}

    // ENTITY METHODS

    /**
     * @ejb:create-method view-type="local"
     **/
    public Integer ejbCreate(Integer BIL_CODE, 
    	String BIL_RCK, String BIL_BN,
    	double BIL_RRDR_PNT, double BIL_RRDR_QTY, Integer BIL_COA_GL_SLS_ACCNT,
        Integer BIL_COA_GL_INVNTRY_ACCNT, Integer BIL_COA_GL_CST_OF_SLS_ACCNT,
        Integer BIL_COA_GL_WIP_ACCNT, Integer BIL_COA_GL_ACCRD_INVNTRY_ACCNT, Integer BIL_COA_GL_SLS_RTRN_ACCNT,
        byte BIL_SBJCT_TO_CMMSSN,
        double BIL_HST1_SLS, double BIL_HST2_SLS,
        double BIL_PRJ_SLS, double BIL_DLVRY_TME, double BIL_DLVRY_BUFF, double BIL_ORDR_PR_YR,
        char BIL_DS_ITM, char BIL_DS_LOC, char BIL_DS_IL, Integer BIL_AD_CMPNY)
    	throws CreateException {

        Debug.print("AdBranchItemLocationBean ejbCreate");

        setBilCode(BIL_CODE);
        setBilRack(BIL_RCK);
        setBilBin(BIL_BN);
        setBilReorderPoint(BIL_RRDR_PNT);
        setBilReorderQuantity(BIL_RRDR_QTY);
        setBilCoaGlSalesAccount(BIL_COA_GL_SLS_ACCNT);
        setBilCoaGlInventoryAccount(BIL_COA_GL_INVNTRY_ACCNT);
        setBilCoaGlCostOfSalesAccount(BIL_COA_GL_CST_OF_SLS_ACCNT);
        setBilCoaGlWipAccount(BIL_COA_GL_WIP_ACCNT);
        setBilCoaGlAccruedInventoryAccount(BIL_COA_GL_ACCRD_INVNTRY_ACCNT);
        setBilCoaGlSalesReturnAccount(BIL_COA_GL_SLS_RTRN_ACCNT);
        setBilSubjectToCommission(BIL_SBJCT_TO_CMMSSN);
        setBilItemDownloadStatus(BIL_DS_ITM);
        setBilLocationDownloadStatus(BIL_DS_LOC);
        setBilItemLocationDownloadStatus(BIL_DS_IL);
        setBilAdCompany(BIL_AD_CMPNY);
        setBilHist1Sales(BIL_HST1_SLS);
        setBilHist2Sales(BIL_HST2_SLS);
        setBilProjectedSales(BIL_PRJ_SLS);
        setBilDeliveryTime(BIL_DLVRY_TME);
	    setBilDeliveryBuffer(BIL_DLVRY_BUFF);
	    setBilOrderPerYear(BIL_ORDR_PR_YR);

        return null;
    }

    /**
     * @ejb:create-method view-type="local"
     **/
    public Integer ejbCreate(
    		String BIL_RCK, String BIL_BN,
    		double BIL_RRDR_PNT, double BIL_RRDR_QTY, Integer BIL_COA_GL_SLS_ACCNT,
            Integer BIL_COA_GL_INVNTRY_ACCNT, Integer BIL_COA_GL_CST_OF_SLS_ACCNT,
            Integer BIL_COA_GL_WIP_ACCNT, Integer BIL_COA_GL_ACCRD_INVNTRY_ACCNT, Integer BIL_COA_GL_SLS_RTRN_ACCNT
            , byte BIL_SBJCT_TO_CMMSSN,
            double BIL_HST1_SLS, double BIL_HST2_SLS,
            double BIL_PRJ_SLS, double BIL_DLVRY_TME, double BIL_DLVRY_BUFF, double BIL_ORDR_PR_YR,
            char BIL_DS_ITM, char BIL_DS_LOC, char BIL_DS_IL, Integer BIL_AD_CMPNY)
    	throws CreateException {

        Debug.print("AdBranchItemLocationBean ejbCreate");

        setBilRack(BIL_RCK);
        setBilBin(BIL_BN);
        setBilReorderPoint(BIL_RRDR_PNT);
        setBilReorderQuantity(BIL_RRDR_QTY);
        setBilCoaGlSalesAccount(BIL_COA_GL_SLS_ACCNT);
        setBilCoaGlInventoryAccount(BIL_COA_GL_INVNTRY_ACCNT);
        setBilCoaGlCostOfSalesAccount(BIL_COA_GL_CST_OF_SLS_ACCNT);
        setBilCoaGlWipAccount(BIL_COA_GL_WIP_ACCNT);
        setBilCoaGlAccruedInventoryAccount(BIL_COA_GL_ACCRD_INVNTRY_ACCNT);
        setBilCoaGlSalesReturnAccount(BIL_COA_GL_SLS_RTRN_ACCNT);
        setBilSubjectToCommission(BIL_SBJCT_TO_CMMSSN);
        setBilItemDownloadStatus(BIL_DS_ITM);
        setBilLocationDownloadStatus(BIL_DS_LOC);
        setBilItemLocationDownloadStatus(BIL_DS_IL);
        setBilAdCompany(BIL_AD_CMPNY);
        setBilHist1Sales(BIL_HST1_SLS);
        setBilHist2Sales(BIL_HST2_SLS);
        setBilProjectedSales(BIL_PRJ_SLS);
        setBilDeliveryTime(BIL_DLVRY_TME);
	    setBilDeliveryBuffer(BIL_DLVRY_BUFF);
	    setBilOrderPerYear(BIL_ORDR_PR_YR);

        return null;
    }

    public void ejbPostCreate(Integer BIL_CODE, 
    		String BIL_RCK, String BIL_BN,
    		double BIL_RRDR_PNT, double BIL_RRDR_QTY, Integer BIL_COA_GL_SLS_ACCNT,
            Integer BIL_COA_GL_INVNTRY_ACCNT, Integer BIL_COA_GL_CST_OF_SLS_ACCNT,
            Integer BIL_COA_GL_WIP_ACCNT, Integer BIL_COA_GL_ACCRD_INVNTRY_ACCNT, Integer BIL_COA_GL_SLS_RTRN_ACCNT
            , byte BIL_SBJCT_TO_CMMSSN,
            double BIL_HST1_SLS, double BIL_HST2_SLS,
            double BIL_PRJ_SLS, double BIL_DLVRY_TME, double BIL_DLVRY_BUFF, double BIL_ORDR_PR_YR,
            char BIL_DS_ITM, char BIL_DS_LOC, char BIL_DS_IL, Integer BIL_AD_CMPNY)
    	throws CreateException { }

    public void ejbPostCreate(
    		String BIL_RCK, String BIL_BN,
    		double BIL_RRDR_PNT, double BIL_RRDR_QTY, Integer BIL_COA_GL_SLS_ACCNT,
            Integer BIL_COA_GL_INVNTRY_ACCNT, Integer BIL_COA_GL_CST_OF_SLS_ACCNT,
            Integer BIL_COA_GL_WIP_ACCNT, Integer BIL_COA_GL_ACCRD_INVNTRY_ACCNT, Integer BIL_COA_GL_SLS_RTRN_ACCNT
            , byte BIL_SBJCT_TO_CMMSSN,
            double BIL_HST1_SLS, double BIL_HST2_SLS,
            double BIL_PRJ_SLS, double BIL_DLVRY_TME, double BIL_DLVRY_BUFF, double BIL_ORDR_PR_YR,
            char BIL_DS_ITM, char BIL_DS_LOC, char BIL_DS_IL, Integer BIL_AD_CMPNY)
        	throws CreateException { }

}
