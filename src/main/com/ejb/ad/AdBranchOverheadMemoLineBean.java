package com.ejb.ad;

import javax.ejb.CreateException;

import com.ejb.inv.LocalInvOverheadMemoLine;
import com.util.AbstractEntityBean;
import com.util.Debug;

/**
 * @author Franco Antonio R. Roig
 * 		   Created on Oct 20, 2005
 * 
 *
 * @ejb:bean name="AdBranchOverheadMemoLineEJB"
 *           display-name="Branch Overhead Memo Lines Entity"
 *           type="CMP"
 *           cmp-version="2.x"
 *           view-type="local"
 *           local-jndi-name="omega-ejb/AdBranchOverheadMemoLineEJB"
 *           schema="AdBranchOverheadMemoLine"
 *           primkey-field="bomlCode"
 *       
 * @ejb:pk class="java.lang.Integer"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="aduser"
 *                        role-link="aduserlink"
 *
 * @ejb:permission role-name="aduser"
 *
 * @ejb:interface local-class="com.ejb.ad.LocalAdBranchOverheadMemoLine"
 *                local-extends="javax.ejb.EJBLocalObject"
 *
 * @ejb:home local-class="com.ejb.ad.LocalAdBranchOverheadMemoLineHome"
 *           local-extends="javax.ejb.EJBLocalHome"
 *
 *@ejb:finder signature="Collection findBomlAll(java.lang.Integer AD_CMPNY)"
 *             query="SELECT OBJECT(boml) FROM AdBranchOverheadMemoLine boml WHERE boml.bomlAdCompany = ?1"
 * 
 * @ejb:finder signature="Collection findBomlByOmlCode(java.lang.Integer OML_CODE, java.lang.Integer AD_CMPNY)"
 * 			  query="SELECT OBJECT(boml) FROM AdBranchOverheadMemoLine boml WHERE boml.invOverheadMemoLine.omlCode = ?1 AND boml.bomlAdCompany = ?2"
 *
 *  @ejb:finder signature="Collection findBomlByOmlCodeAndRsName(java.lang.Integer OML_CODE, java.lang.String RS_NM, java.lang.Integer AD_CMPNY)"
 * 			  query="SELECT OBJECT(boml) FROM AdBranchOverheadMemoLine boml, IN(boml.adBranch.adBranchResponsibilities) brs WHERE boml.invOverheadMemoLine.omlCode = ?1 AND brs.adResponsibility.rsName = ?2 AND boml.bomlAdCompany = ?3"
 * 
 * @ejb:finder signature="LocalAdBranchOverheadMemoLine findBomlByOmlCodeAndBrCode(java.lang.Integer OML_CODE, java.lang.Integer AD_BRNCH, java.lang.Integer AD_CMPNY)"
 * 			  query="SELECT OBJECT(boml) FROM AdBranchOverheadMemoLine boml WHERE boml.invOverheadMemoLine.omlCode = ?1 AND boml.adBranch.brCode = ?2 AND boml.bomlAdCompany = ?3"
 * 
 * @ejb:finder signature="Collection findByBomlGlCoaOverheadAccount(java.lang.Integer OML_CODE, java.lang.Integer OML_AD_CMPNY)"
 *             query="SELECT OBJECT(boml) FROM AdBranchOverheadMemoLine boml WHERE boml.bomlGlCoaOverheadAccount=?1 AND boml.bomlAdCompany = ?2"
 * 
 * @ejb:finder signature="Collection findByBomlGlCoaLiabilityAccount(java.lang.Integer OML_CODE, java.lang.Integer OML_AD_CMPNY)"
 *             query="SELECT OBJECT(boml) FROM AdBranchOverheadMemoLine boml WHERE boml.bomlGlCoaLiabilityAccount=?1 AND boml.bomlAdCompany = ?2"
 *
 * @ejb:value-object match="*"
 *             name="AdBranchOverheadMemoLine"
 *
 * @jboss:persistence table-name="AD_BR_OML"
 *
 * @jboss:entity-command name="mysql-get-generated-keys"
 *
 */
public abstract class AdBranchOverheadMemoLineBean extends AbstractEntityBean {

	// PERSISTENT METHODS
	
    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     * @ejb:pk-field
     *
     * @jboss:column-name name="BOML_CODE"
     *
     * @jboss:persistence auto-increment="true"
     **/
    public abstract Integer getBomlCode();
    public abstract void setBomlCode(Integer BOML_CODE);
	
    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"   
     *
     * @jboss:column-name name="BOML_GL_COA_OVRHD_ACCNT"
     **/
    public abstract Integer getBomlGlCoaOverheadAccount();
     /**
      * @ejb:interface-method view-type="local"
      **/ 
    public abstract void setBomlGlCoaOverheadAccount(Integer BOML_GL_COA_OVRHD_ACCNT);
    
    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"   
     *
     * @jboss:column-name name="BOML_GL_COA_LBLTY_ACCNT"
     **/
    public abstract Integer getBomlGlCoaLiabilityAccount();
     /**
      * @ejb:interface-method view-type="local"
      **/ 
    public abstract void setBomlGlCoaLiabilityAccount(Integer BOML_GL_COA_LBLTY_ACCNT);
      
    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"   
     *
     * @jboss:column-name name="INV_OVERHEAD_MEMO_LINE"
     **/
    public abstract Integer getBomlInvOverheadMemoLine();
     /**
      * @ejb:interface-method view-type="local"
      **/ 
    public abstract void setBomlInvOverheadMemoLine(Integer INV_OVERHEAD_MEMO_LINE);

    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"   
     *
     * @jboss:column-name name="AD_BRANCH"
     **/
    public abstract Integer getBomlAdBranch();
     /**
      * @ejb:interface-method view-type="local"
      **/ 
    public abstract void setBomlAdBranch(Integer AD_BRANCH);

    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"   
     *
     * @jboss:column-name name="BOML_AD_CMPNY"
     **/
    public abstract Integer getBomlAdCompany();
     /**
      * @ejb:interface-method view-type="local"
      **/ 
    public abstract void setBomlAdCompany(Integer BOML_AD_CMPNY);
    
	// RELATIONSHIP METHODS
	
    /**
     * @ejb:interface-method view-type="local"
     * @ejb:relation name="branch-branchomls"
     *               role-name="branchoml-has-one-branch"
     *
     * @jboss:relation related-pk-field="brCode"
     *                 fk-column="AD_BRANCH"
     */
    public abstract LocalAdBranch getAdBranch();
    public abstract void setAdBranch(LocalAdBranch adBranch);

    /**
     * @ejb:interface-method view-type="local"
     * @ejb:relation name="overheadmemoline-branchomls"
     *               role-name="branchomls-has-one-overheadmemoline"
     * 				 cascade-delete="yes"
     * 
     * @jboss:relation related-pk-field="omlCode"
     * 				   fk-column="INV_OVERHEAD_MEMO_LINE"
     * 
     */
    public abstract LocalInvOverheadMemoLine getInvOverheadMemoLine();
    public abstract void setInvOverheadMemoLine(LocalInvOverheadMemoLine invOverheadMemoLine);
    
    
	// ENTITY BEAN METHODS
	
    /**
     * @ejb:create-method view-type="local"
     **/
    public Integer ejbCreate(Integer BOML_CODE, Integer BOML_GL_COA_OVRHD_ACCNT, 
    		Integer BOML_GL_COA_LBLTY_ACCNT, Integer BOML_AD_CMPNY)     	
         throws CreateException {	
     	
         Debug.print("AdBranchOmlBean ejbCreate");
     
         setBomlCode(BOML_CODE);
         setBomlGlCoaOverheadAccount(BOML_GL_COA_OVRHD_ACCNT);
         setBomlGlCoaLiabilityAccount(BOML_GL_COA_LBLTY_ACCNT);
         setBomlAdCompany(BOML_AD_CMPNY);
         
         return null;
     }

    /**
     * @ejb:create-method view-type="local"
     **/
    public Integer ejbCreate(Integer BOML_GL_COA_OVRHD_ACCNT, 
    		Integer BOML_GL_COA_LBLTY_ACCNT, Integer BOML_AD_CMPNY)     	
         throws CreateException {	
     	
        Debug.print("AdBranchOmlBean ejbCreate");
        
        setBomlGlCoaOverheadAccount(BOML_GL_COA_OVRHD_ACCNT);
        setBomlGlCoaLiabilityAccount(BOML_GL_COA_LBLTY_ACCNT);
        setBomlAdCompany(BOML_AD_CMPNY);         
        
        return null;
     }

    public void ejbPostCreate(Integer BOML_CODE, Integer BOML_GL_COA_OVRHD_ACCNT, 
    		Integer BOML_GL_COA_LBLTY_ACCNT, Integer BOML_AD_CMPNY)
         throws CreateException { }

    public void ejbPostCreate(Integer BOML_GL_COA_OVRHD_ACCNT, 
    		Integer BOML_GL_COA_LBLTY_ACCNT, Integer BOML_AD_CMPNY)
         throws CreateException { }
      
}
