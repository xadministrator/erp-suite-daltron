package com.ejb.ad;

import java.util.Collection;

import javax.ejb.CreateException;
import javax.ejb.FinderException;

import com.ejb.pm.LocalPmProjectTypeType;
import com.util.AbstractEntityBean;
import com.util.Debug;

/**
*
* @author  Clint Arrogante
*
* @ejb:bean name="AdBranchProjectTypeTypeEJB"
*           display-name="Branch Project Type Type Entity"
*           type="CMP"
*           cmp-version="2.x"
*           view-type="local"
*           local-jndi-name="omega-ejb/AdBranchProjectTypeTypeEJB"
*           schema="AdBranchProjectTypeType"
*           primkey-field="bpttCode"
*       
* @ejb:pk class="java.lang.Integer"
*
* @ejb:transaction type="Required"
*
* @ejb:security-role-ref role-name="aduser"
*                        role-link="aduserlink"
*
* @ejb:permission role-name="aduser"
*
* @ejb:interface local-class="com.ejb.ad.LocalAdBranchProjectTypeType"
*                local-extends="javax.ejb.EJBLocalObject"
*
* @ejb:home local-class="com.ejb.ad.LocalAdBranchProjectTypeTypeHome"
*           local-extends="javax.ejb.EJBLocalHome"
*
* @ejb:finder signature="Collection findByPmPttAll(java.lang.Integer ITM_LCTN_CODE, java.lang.Integer BPTT_AD_CMPNY)"
*             query="SELECT OBJECT(bptt) FROM AdBranchProjectTypeType bptt WHERE bptt.pmProjectTypeType.pttCode = ?1 AND bptt.bpttAdCompany = ?2"
* 
* @ejb:finder signature="Collection findBpttByPttCodeAndRsCode(java.lang.Integer PTT_CODE, java.lang.Integer RS_CODE, java.lang.Integer AD_CMPNY)"
* 			  query="SELECT OBJECT(bptt) FROM AdBranchProjectTypeType bptt, IN(bptt.adBranch.adBranchResponsibilities)brs WHERE bptt.pmProjectTypeType.pttCode = ?1 AND brs.adResponsibility.rsCode = ?2 AND bptt.bpttAdCompany = ?3"
* 
* @ejb:finder signature="LocalAdBranchProjectTypeType findBpttByPttCodeAndBrCode(java.lang.Integer PTT_CODE, java.lang.Integer BR_CODE, java.lang.Integer AD_CMPNY)"
* 			  query="SELECT OBJECT(bptt) FROM AdBranchProjectTypeType bptt WHERE bptt.pmProjectTypeType.pttCode = ?1 AND  bptt.adBranch.brCode = ?2 AND bptt.bpttAdCompany = ?3"
* 
* @ejb:finder signature="Collection findBpttByPrjCodeAndBrCode(java.lang.Integer PRJ_CODE, java.lang.Integer BR_CODE, java.lang.Integer AD_CMPNY)"
* 			  query="SELECT OBJECT(bptt) FROM AdBranchProjectTypeType bptt WHERE bptt.pmProjectTypeType.pmProject.prjCode = ?1 AND bptt.adBranch.brCode = ?2 AND bptt.bpttAdCompany = ?3"
* 

* 
* @ejb:select signature="Collection ejbSelectGeneric(java.lang.String jbossQl, java.lang.Object[] args)"
*             result-type-mapping="Local"
*             method-intf="LocalHome"
*             query="SELECT OBJECT(bptt) FROM AdBranchProjectTypeType bptt"
*
* @jboss:query signature="Collection ejbSelectGeneric(java.lang.String jbossQl, java.lang.Object[] args)"
*                         dynamic="true"
*  
* @ejb:value-object match="*"
*             name="AdBranchProjectTypeType"
*
* @jboss:persistence table-name="AD_BR_PTT"
*
* @jboss:entity-command name="mysql-get-generated-keys"
*
*/
public abstract class AdBranchProjectTypeTypeBean extends AbstractEntityBean {
    
    //  PERSISTENT FIELDS

    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     * @ejb:pk-field
     *
     * @jboss:column-name name="BPTT_CODE"
     *
     * @jboss:persistence auto-increment="true"
     **/
    public abstract java.lang.Integer getBpttCode();
    public abstract void setBpttCode(java.lang.Integer BPTT_CODE);
    
    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="BPTT_AD_CMPNY"
     **/
    public abstract Integer getBpttAdCompany();
    /**
     * @ejb:interface-method view-type="local"
     **/     
    public abstract void setBpttAdCompany(Integer BPTT_AD_CMPNY);
    
            
    // RELATIONSHIP FIELDS
    
    /**
     * @ejb:interface-method view-type="local"
     * @ejb:relation name="branch-branchprojecttypetype"
     *               role-name="branchprojecttypetype-has-one-branch"
     *				 cascade-delete="yes"
     * 
     * @jboss:relation related-pk-field="brCode"
     *                 fk-column="AD_BRANCH"
     */
    public abstract LocalAdBranch getAdBranch();
    /**
     * @ejb:interface-method view-type="local"
     **/
    public abstract void setAdBranch(LocalAdBranch adBranch);
    
    /**
     * @ejb:interface-method view-type="local"
     * @ejb:relation name="projecttypetype-branchprojecttypetype"
     *               role-name="branchprojecttypetype-has-one-projecttypetype"
     *				 cascade-delete="yes"
     * 
     * @jboss:relation related-pk-field="pttCode"
     *                 fk-column="PM_PROJECT_TYPE_TYPE"
     */
    public abstract LocalPmProjectTypeType getPmProjectTypeType();
    /**
     * @ejb:interface-method view-type="local"
     **/
    public abstract void setPmProjectTypeType(LocalPmProjectTypeType pmProjectTypeType);
    
	/**
	 * @jboss:dynamic-ql
	 */
	public abstract Collection ejbSelectGeneric(java.lang.String jbossQl, java.lang.Object[] args)
	throws FinderException;
    
    // BUSINESS METHODS
	
	/**
	 * @ejb:home-method view-type="local"
	 */
	public Collection ejbHomeGetBpttByCriteria(java.lang.String jbossQl, java.lang.Object[] args)
	throws FinderException {
		
		return ejbSelectGeneric(jbossQl, args);
	}
    
    // ENTITY METHODS
    
    /**
     * @ejb:create-method view-type="local"
     **/
    public Integer ejbCreate(Integer BPTT_CODE, Integer BPTT_AD_CMPNY)
    	throws CreateException {
        
        Debug.print("AdBranchProjectTypeTypeBean ejbCreate");
        
        setBpttCode(BPTT_CODE);
        setBpttAdCompany(BPTT_AD_CMPNY);

        
        return null;
    } 
    
    /**
     * @ejb:create-method view-type="local"
     **/
    public Integer ejbCreate(Integer BPTT_AD_CMPNY)
    	throws CreateException {
        
        Debug.print("AdBranchProjectTypeTypeBean ejbCreate");
        

        setBpttAdCompany(BPTT_AD_CMPNY);

        
        return null;
    }
    
    public void ejbPostCreate(Integer BPTT_CODE, Integer BPTT_AD_CMPNY)
    	throws CreateException { }
    
    public void ejbPostCreate(Integer BPTT_AD_CMPNY)
        	throws CreateException { }
        
}
