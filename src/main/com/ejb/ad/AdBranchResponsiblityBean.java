package com.ejb.ad;

import javax.ejb.CreateException;

import com.util.AbstractEntityBean;
import com.util.Debug;

/**
*
* @author  Arnel A. Masikip
*
* @ejb:bean name="AdBranchResponsibilityEJB"
*           display-name="Branch Responsibility Entity"
*           type="CMP"
*           cmp-version="2.x"
*           view-type="local"
*           local-jndi-name="omega-ejb/AdBranchResponsibilityEJB"
*           schema="AdBranchResponsibility"
*           primkey-field="brsCode"
*       
* @ejb:pk class="java.lang.Integer"
*
* @ejb:transaction type="Required"
*
* @ejb:security-role-ref role-name="gluser"
*                        role-link="gluserlink"
*
* @ejb:permission role-name="gluser"
*
* @ejb:interface local-class="com.ejb.ad.LocalAdBranchResponsibility"
*                local-extends="javax.ejb.EJBLocalObject"
*
* @ejb:home local-class="com.ejb.ad.LocalAdBranchResponsibilityHome"
*           local-extends="javax.ejb.EJBLocalHome"
*
* @ejb:finder signature="Collection findBrsAll(java.lang.Integer AD_CMPNY)"
*             query="SELECT OBJECT(brs) FROM AdBranchResponsibility brs WHERE brs.brsAdCompany = ?1"
*
* @ejb:finder signature="Collection findByAdResponsibility(java.lang.Integer RS_CODE, java.lang.Integer BRS_AD_CMPNY)"
*             query="SELECT OBJECT(brs) FROM AdBranchResponsibility brs WHERE brs.adResponsibility.rsCode = ?1 AND brs.brsAdCompany = ?2"
* 
* @ejb:value-object match="*"
*             name="AdBranchResponsibility"
*
* @jboss:persistence table-name="AD_BR_RS"
*
* @jboss:entity-command name="mysql-get-generated-keys"
*
*/
public abstract class AdBranchResponsiblityBean extends AbstractEntityBean {

    // PERSISTENT FIELDS

    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     * @ejb:pk-field
     *
     * @jboss:column-name name="BRS_CODE"
     *
     * @jboss:persistence auto-increment="true"
     **/
    public abstract java.lang.Integer getBrsCode();
    public abstract void setBrsCode(java.lang.Integer BRS_CODE);
    
    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="BRS_AD_CMPNY"
     **/
     public abstract Integer getBrsAdCompany();
    /**
     * @ejb:interface-method view-type="local"
     **/     
     public abstract void setBrsAdCompany(Integer BRS_AD_CMPNY); 
    
    // RELATIONSHIP METHODS
    
    /**
     * @ejb:interface-method view-type="local"
     * @ejb:relation name="branch-branchresponsibility"
     *               role-name="branchresponsibility-has-one-branch"
     *				 cascade-delete="yes"
     * 
     * @jboss:relation related-pk-field="brCode"
     *                 fk-column="AD_BRANCH"
     */
     public abstract LocalAdBranch getAdBranch();
     public abstract void setAdBranch(LocalAdBranch adBranch);
     
     /**
      * @ejb:interface-method view-type="local"
      * @ejb:relation name="responsibility-branchresponsibility"
      *               role-name="branchresponsibility-has-one-responsibility"
      *				 cascade-delete="yes"
      * 
      * @jboss:relation related-pk-field="rsCode"
      *                 fk-column="AD_RESPONSIBILITY"
      */
      public abstract LocalAdResponsibility getAdResponsibility();
      public abstract void setAdResponsibility(LocalAdResponsibility adResponsibility);
      
      // BUSINESS METHODS
      
      // ENTITY METHODS
      
      /**
       * @ejb:create-method view-type="local"
       **/
       public Integer ejbCreate(Integer BRS_CODE, Integer BRS_AD_CMPNY)
           throws CreateException {
        	
           Debug.print("AdBranchResponsibilityBean ejbCreate");
        
           setBrsCode(BRS_CODE);   
           setBrsAdCompany(BRS_AD_CMPNY);
          
           return null;
       } 
       
       /**
        * @ejb:create-method view-type="local"
        **/
       public Integer ejbCreate(Integer BRS_AD_CMPNY) 
       	  throws CreateException {
           
          Debug.print("AdBranchResponsibilityBean ejbCreate");
          
          setBrsAdCompany(BRS_AD_CMPNY);
          
          return null;
          
       }
       
       public void ejbPostCreate(Integer BRS_CODE, Integer BRS_AD_CMPNY) 
       	  throws CreateException { }
       
       public void ejbPostCreate(Integer BRS_AD_CMPNY) 
    	  throws CreateException { }
}
