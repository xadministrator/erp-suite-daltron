package com.ejb.ad;

import javax.ejb.CreateException;

import com.ejb.ar.LocalArSalesperson;
import com.util.AbstractEntityBean;
import com.util.Debug;

/**
 *
 * @author  Franco Antonio R. Roig
 *
 * @ejb:bean name="AdBranchSalespersonEJB"
 *           display-name="Branch Salesperson Entity"
 *           type="CMP"
 *           cmp-version="2.x"
 *           view-type="local"
 *           local-jndi-name="omega-ejb/AdBranchSalespersonEJB"
 *           schema="AdBranchSalesperson"
 *           primkey-field="bslpCode"
 *       
 * @ejb:pk class="java.lang.Integer"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="aduser"
 *                        role-link="aduserlink"
 *
 * @ejb:permission role-name="aduser"
 *
 * @ejb:interface local-class="com.ejb.ad.LocalAdBranchSalesperson"
 *                local-extends="javax.ejb.EJBLocalObject"
 *
 * @ejb:home local-class="com.ejb.ad.LocalAdBranchSalespersonHome"
 *           local-extends="javax.ejb.EJBLocalHome"
 * 
 * @ejb:finder signature="Collection findBSLPBySLPCodeAndRsName(java.lang.Integer SLP_CODE, java.lang.String RS_NM, java.lang.Integer AD_CMPNY)"
 * 			  query="SELECT OBJECT(bslp) FROM AdBranchSalesperson bslp, IN(bslp.adBranch.adBranchResponsibilities) brs WHERE bslp.arSalesperson.slpCode = ?1 AND brs.adResponsibility.rsName = ?2 AND bslp.bslpAdCompany = ?3"
 * 
 * @ejb:finder signature="LocalAdBranchSalesperson findBySlpCodeAndBrCode(java.lang.Integer SLP_CODE, java.lang.Integer AD_BRNCH, java.lang.Integer AD_CMPNY)"
 * 			  query="SELECT OBJECT(bslp) FROM AdBranchSalesperson bslp WHERE bslp.arSalesperson.slpCode = ?1 AND bslp.adBranch.brCode = ?2 AND bslp.bslpAdCompany = ?3"
 * 
 * @ejb:value-object match="*"
 *             name="AdBranchSalesperson"
 *
 * @jboss:persistence table-name="AD_BR_SLP"
 *
 * @jboss:entity-command name="mysql-get-generated-keys"
 *
 */
public abstract class AdBranchSalespersonBean extends AbstractEntityBean {
	
	// PERSISTENT FIELDS
	
	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 * @ejb:pk-field
	 *
	 * @jboss:column-name name="BSLP_CODE"
	 *
	 * @jboss:persistence auto-increment="true"
	 **/
	public abstract java.lang.Integer getBslpCode();
	public abstract void setBslpCode(java.lang.Integer BSLP_CODE);
	
	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="BSLP_AD_CMPNY"
	 **/
	public abstract Integer getBslpAdCompany();
	/**
	 * @ejb:interface-method view-type="local"
	 **/     
	public abstract void setBslpAdCompany(Integer BSLP_AD_CMPNY);
	
	// RELATIONSHIP FIELDS
	
	/**
	 * @ejb:interface-method view-type="local"
	 * @ejb:relation name="branch-branchsalesperson"
	 *               role-name="branchsalesperson-has-one-branch"
	 *				 cascade-delete="yes"
	 * 
	 * @jboss:relation related-pk-field="brCode"
	 *                 fk-column="AD_BRANCH"
	 */
	public abstract LocalAdBranch getAdBranch();
	public abstract void setAdBranch(LocalAdBranch adBranch);
	
	/**
	 * @ejb:interface-method view-type="local"
	 * @ejb:relation name="salesperson-branchsalesperson"
	 *               role-name="branchsalesperson-has-one-salesperson"
	 *				  cascade-delete="yes"
	 * 
	 * @jboss:relation related-pk-field="slpCode"
	 *                 fk-column="AR_SALESPERSON"
	 */
	public abstract LocalArSalesperson getArSalesperson();
	public abstract void setArSalesperson(LocalArSalesperson arSalesPerson);
	
	// ENTITY METHODS
	
	/**
	 * @ejb:create-method view-type="local"
	 **/
	public Integer ejbCreate(Integer BSLP_CODE, Integer BSLP_AD_CMPNY)
	throws CreateException {
		
		Debug.print("AdBranchSalespersonBean ejbCreate");
		
		setBslpCode(BSLP_CODE);            
		setBslpAdCompany(BSLP_AD_CMPNY);
		
		return null;
	}
	
	/**
	 * @ejb:create-method view-type="local"
	 **/
	public Integer ejbCreate(Integer BSLP_AD_CMPNY)
	throws CreateException {
		
		Debug.print("AdBranchSalespersonBean ejbCreate");
		
		setBslpAdCompany(BSLP_AD_CMPNY);
		
		return null;
	}
	
	public void ejbPostCreate(Integer BSLP_CODE, Integer BSLP_AD_CMPNY)
	throws CreateException { }
	
	public void ejbPostCreate(Integer BSLP_AD_CMPNY)
	throws CreateException { }
	
}
