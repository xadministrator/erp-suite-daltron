package com.ejb.ad;

import javax.ejb.CreateException;

import com.ejb.ar.LocalArStandardMemoLine;
import com.util.AbstractEntityBean;
import com.util.Debug;

/**
 * @author Arnel A. Masikip
 * 		   Created on Oct 20, 2005
 * 
 *
 * @ejb:bean name="AdBranchStandardMemoLineEJB"
 *           display-name="Branch Standard Memo Line Entity"
 *           type="CMP"
 *           cmp-version="2.x"
 *           view-type="local"
 *           local-jndi-name="omega-ejb/AdBranchStandardMemoLineEJB"
 *           schema="AdBranchStandardMemoLine"
 *           primkey-field="bsmlCode"
 *       
 * @ejb:pk class="java.lang.Integer"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="aduser"
 *                        role-link="aduserlink"
 *
 * @ejb:permission role-name="aduser"
 *
 * @ejb:interface local-class="com.ejb.ad.LocalAdBranchStandardMemoLine"
 *                local-extends="javax.ejb.EJBLocalObject"
 *
 * @ejb:home local-class="com.ejb.ad.LocalAdBranchStandardMemoLineHome"
 *           local-extends="javax.ejb.EJBLocalHome"
 *
 * @ejb:finder signature="Collection findBSMLByBSMLNewAndUpdated(java.lang.Integer BR_CODE, java.lang.Integer AD_CMPNY, char NEW, char UPDATED, char DOWNLOADED_UPDATED)"
 * 			   query="SELECT  OBJECT(bsml) FROM AdBranchStandardMemoLine bsml WHERE (bsml.bsmlStandardMemoLineDownloadStatus = ?3 OR bsml.bsmlStandardMemoLineDownloadStatus = ?4 OR bsml.bsmlStandardMemoLineDownloadStatus = ?5) AND bsml.adBranch.brCode = ?1 AND bsml.bsmlAdCompany = ?2"
 *
 * @ejb:finder signature="Collection findBSMLAll(java.lang.Integer AD_CMPNY)"
 *             query="SELECT OBJECT(bsml) FROM AdBranchStandardMemoLine bsml WHERE bsml.bsmlAdCompany = ?1"
 * 
 * @ejb:finder signature="Collection findBSMLBySML(java.lang.Integer SML_CODE, java.lang.Integer AD_CMPNY)"
 * 			  query="SELECT OBJECT(bsml) FROM AdBranchStandardMemoLine bsml WHERE bsml.arStandardMemoLine.smlCode = ?1 AND bsml.bsmlAdCompany = ?2"
 *
 * @ejb:finder signature="Collection findBSMLBySMLCodeAndRsName(java.lang.Integer SML_CODE, java.lang.String RS_NM, java.lang.Integer AD_CMPNY)"
 * 			  query="SELECT OBJECT(bsml) FROM AdBranchStandardMemoLine bsml, IN(bsml.adBranch.adBranchResponsibilities) brs WHERE bsml.arStandardMemoLine.smlCode = ?1 AND brs.adResponsibility.rsName = ?2 AND bsml.bsmlAdCompany = ?3"
 * 
 * @ejb:finder signature="LocalAdBranchStandardMemoLine findBSMLBySMLCodeAndBrCode(java.lang.Integer SML_CODE, java.lang.Integer BR_CODE, java.lang.Integer AD_CMPNY)"
 * 			  query="SELECT OBJECT(bsml) FROM AdBranchStandardMemoLine bsml WHERE bsml.arStandardMemoLine.smlCode = ?1 AND bsml.adBranch.brCode = ?2 AND bsml.bsmlAdCompany = ?3"
 * 
 * @ejb:finder signature="Collection findByBSMLGlAccount(java.lang.Integer SML_CODE, java.lang.Integer SML_AD_CMPNY)"
 *             query="SELECT OBJECT(bsml) FROM AdBranchStandardMemoLine bsml WHERE bsml.bsmlGlAccount=?1 AND bsml.bsmlAdCompany = ?2"
 *             
 * @ejb:value-object match="*"
 *             name="AdBranchStandardMemoLine"
 *             
 * @jboss:persistence table-name="AD_BR_SML"
 *
 * @jboss:entity-command name="mysql-get-generated-keys"
 *
 */
public abstract class AdBranchStandardMemoLineBean extends AbstractEntityBean {
    
    //  PERSISTENT METHODS
	
    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     * @ejb:pk-field
     *
     * @jboss:column-name name="BSML_CODE"
     *
     * @jboss:persistence auto-increment="true"
     **/
    public abstract Integer getBsmlCode();
    public abstract void setBsmlCode(Integer BSML_CODE); 
    
    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"   
     *
     * @jboss:column-name name="BSML_GL_ACCNT"
     **/
    public abstract Integer getBsmlGlAccount();
     /**
      * @ejb:interface-method view-type="local"
      **/ 
    public abstract void setBsmlGlAccount(Integer BSML_GL_ACCNT);

    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"   
     *
     * @jboss:column-name name="BSML_GL_COA_RCVBL_ACCNT"
     **/
    public abstract Integer getBsmlGlCoaReceivableAccount();
     /**
      * @ejb:interface-method view-type="local"
      **/ 
    public abstract void setBsmlGlCoaReceivableAccount(Integer BSML_GL_COA_RCVBL_ACCNT);
    
    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"   
     *
     * @jboss:column-name name="BSML_GL_COA_RVNUE_ACCNT"
     **/
    public abstract Integer getBsmlGlCoaRevenueAccount();
     /**
      * @ejb:interface-method view-type="local"
      **/ 
    public abstract void setBsmlGlCoaRevenueAccount(Integer BSML_GL_COA_RVNUE_ACCNT);
    
    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"   
     *
     * @jboss:column-name name="BSML_SBJCT_TO_CMMSSN"
     **/
    public abstract byte getBsmlSubjectToCommission();
     /**
      * @ejb:interface-method view-type="local"
      **/ 
    public abstract void setBsmlSubjectToCommission(byte BSML_SBJCT_TO_CMMSSN);
    
    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"   
     *
     * @jboss:column-name name="BSML_AD_CMPNY"
     **/
    public abstract Integer getBsmlAdCompany();
     /**
      * @ejb:interface-method view-type="local"
      **/ 
    public abstract void setBsmlAdCompany(Integer BSML_AD_CMPNY);
       
    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"   
     *
     * @jboss:column-name name="BSML_DWNLD_STATUS"
     **/
    public abstract char getBsmlStandardMemoLineDownloadStatus();
     /**
      * @ejb:interface-method view-type="local"
      **/ 
    public abstract void setBsmlStandardMemoLineDownloadStatus(char BSML_DWNLD_STATUS);
    
    
    // RELATIONSHIP METHODS
    
    /**
     * @ejb:interface-method view-type="local"
     * @ejb:relation name="branch-branchstandardmemoline"
     *               role-name="branchstandaradmemoline-has-one-branch"
     *
     * @jboss:relation related-pk-field="brCode"
     *                 fk-column="AD_BRANCH"
     */
    public abstract LocalAdBranch getAdBranch();
    public abstract void setAdBranch(LocalAdBranch adBranch);
    
    /**
     * @ejb:interface-method view-type="local"
     * @ejb:relation name="standardmemoline-branchstandardmemoline"
     *               role-name="branchstandaradmemoline-has-one-standardmemoline"
     * 				 cascade-delete="yes"
     *
     * @jboss:relation related-pk-field="smlCode"
     *                 fk-column="AR_STANDARD_MEMO_LINE"
     */
    public abstract LocalArStandardMemoLine getArStandardMemoLine();
    public abstract void setArStandardMemoLine(LocalArStandardMemoLine arStandardMemoLine);
    
    // ENTITY METHODS
    
    /**
     * @ejb:create-method view-type="local"
     **/
    public Integer ejbCreate(Integer BSML_CODE, Integer BSML_GL_ACCNT, Integer BSML_GL_COA_RCVBL_ACCNT, Integer BSML_GL_COA_RVNUE_ACCNT, byte BSML_SBJCT_TO_CMMSSN,char BSML_DWNLD_STATUS, Integer BSML_AD_CMPNY)     	
		throws CreateException {	
     	
         Debug.print("AdBranchStandardMemoLineBean ejbCreate");
     
         setBsmlCode(BSML_CODE);
         setBsmlGlAccount(BSML_GL_ACCNT);
         setBsmlGlCoaReceivableAccount(BSML_GL_COA_RCVBL_ACCNT);
         setBsmlGlCoaRevenueAccount(BSML_GL_COA_RVNUE_ACCNT);
         setBsmlSubjectToCommission(BSML_SBJCT_TO_CMMSSN);
         setBsmlStandardMemoLineDownloadStatus(BSML_DWNLD_STATUS);
         setBsmlAdCompany(BSML_AD_CMPNY);
         
         return null;
     }
     
    /**
     * @ejb:create-method view-type="local"
     **/
    public Integer ejbCreate(Integer BSML_GL_ACCNT, Integer BSML_GL_COA_RCVBL_ACCNT, Integer BSML_GL_COA_RVNUE_ACCNT, byte BSML_SBJCT_TO_CMMSSN, char BSML_DWNLD_STATUS, Integer BSML_AD_CMPNY)     	
		throws CreateException {	
     	
         Debug.print("AdBranchStandardMemoLineBean ejbCreate");
             
         setBsmlGlAccount(BSML_GL_ACCNT);
         setBsmlGlCoaReceivableAccount(BSML_GL_COA_RCVBL_ACCNT);
         setBsmlGlCoaRevenueAccount(BSML_GL_COA_RVNUE_ACCNT);
         setBsmlSubjectToCommission(BSML_SBJCT_TO_CMMSSN);
         setBsmlStandardMemoLineDownloadStatus(BSML_DWNLD_STATUS);
         setBsmlAdCompany(BSML_AD_CMPNY);
         
         return null;
         
    }
    
    public void ejbPostCreate(Integer BSML_CODE, Integer BSML_GL_ACCNT, Integer BSML_GL_COA_RCVBL_ACCNT, Integer BSML_GL_COA_RVNUE_ACCNT, byte BSML_SBJCT_TO_CMMSSN, char BSML_DWNLD_STATUS, Integer BSML_AD_CMPNY)     	
		throws CreateException { }
    
    public void ejbPostCreate(Integer BSML_GL_ACCNT, Integer BSML_GL_COA_RCVBL_ACCNT, Integer BSML_GL_COA_RVNUE_ACCNT, byte BSML_SBJCT_TO_CMMSSN,char BSML_DWNLD_STATUS, Integer BSML_AD_CMPNY)     	
		throws CreateException { }
    
}
