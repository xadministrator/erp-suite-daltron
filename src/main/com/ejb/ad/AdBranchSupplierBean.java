package com.ejb.ad;

import javax.ejb.CreateException;

import com.ejb.ap.LocalApSupplier;
import com.util.AbstractEntityBean;
import com.util.Debug;

/**
 * @author Franco Antonio R. Roig
 * 		   Created on Oct 20, 2005
 * 
 *
 * @ejb:bean name="AdBranchSupplierEJB"
 *           display-name="Branch Supplier Entity"
 *           type="CMP"
 *           cmp-version="2.x"
 *           view-type="local"
 *           local-jndi-name="omega-ejb/AdBranchSupplierEJB"
 *           schema="AdBranchSupplier"
 *           primkey-field="bsplCode"
 *       
 * @ejb:pk class="java.lang.Integer"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="aduser"
 *                        role-link="aduserlink"
 *
 * @ejb:permission role-name="aduser"
 *
 * @ejb:interface local-class="com.ejb.ad.LocalAdBranchSupplier"
 *                local-extends="javax.ejb.EJBLocalObject"
 *
 * @ejb:home local-class="com.ejb.ad.LocalAdBranchSupplierHome"
 *           local-extends="javax.ejb.EJBLocalHome"
 *
 * @ejb:finder signature="Collection findBSplAll(java.lang.Integer AD_CMPNY)"
 *             query="SELECT OBJECT(bspl) FROM AdBranchSupplier bspl WHERE bspl.bsplAdCompany = ?1"
 *
 *  @ejb:finder signature="Collection findBSplBySplCodeAndRsName(java.lang.Integer SPL_CODE, java.lang.String RS_NM, java.lang.Integer AD_CMPNY)"
 * 			  query="SELECT OBJECT(bspl) FROM AdBranchSupplier bspl, IN(bspl.adBranch.adBranchResponsibilities) brs WHERE bspl.apSupplier.splCode = ?1 AND brs.adResponsibility.rsName = ?2 AND bspl.bsplAdCompany = ?3"
 *
 * @ejb:finder signature="Collection findBSplBySpl(java.lang.Integer SPL_CODE, java.lang.Integer AD_CMPNY)"
 * 			  query="SELECT OBJECT(bspl) FROM AdBranchSupplier bspl WHERE bspl.apSupplier.splCode = ?1 AND bspl.bsplAdCompany = ?2"
 * 
 * @ejb:finder signature="LocalAdBranchSupplier findBSplBySplCodeAndBrCode(java.lang.Integer SPL_CODE, java.lang.Integer AD_BRNCH, java.lang.Integer AD_CMPNY)"
 * 			  query="SELECT OBJECT(bspl) FROM AdBranchSupplier bspl WHERE bspl.apSupplier.splCode = ?1 AND bspl.adBranch.brCode = ?2 AND bspl.bsplAdCompany = ?3"
 *
 * @ejb:finder signature="Collection findByBsplGlCoaPayableAccount(java.lang.Integer SPL_CODE, java.lang.Integer BSPL_AD_CMPNY)"
 *             query="SELECT OBJECT(bspl) FROM AdBranchSupplier bspl WHERE bspl.bsplGlCoaPayableAccount=?1 AND bspl.bsplAdCompany = ?2"
 * 
 * @ejb:finder signature="Collection findByBsplGlCoaExpenseAccount(java.lang.Integer SPL_CODE, java.lang.Integer BSPL_AD_CMPNY)"
 *             query="SELECT OBJECT(bspl) FROM AdBranchSupplier bspl WHERE bspl.bsplGlCoaExpenseAccount=?1 AND bspl.bsplAdCompany = ?2"
 *
 * @ejb:finder signature="Collection findSplBySplNewAndUpdated(java.lang.Integer BR_CODE, java.lang.Integer AD_CMPNY, char NEW, char UPDATED, char DOWNLOADED_UPDATED)"
 * 			  query="SELECT OBJECT(bspl) FROM AdBranchSupplier bspl WHERE (bspl.bsplSupplierDownloadStatus = ?3 OR bspl.bsplSupplierDownloadStatus = ?4 OR bspl.bsplSupplierDownloadStatus = ?5) AND bspl.adBranch.brCode = ?1 AND bspl.bsplAdCompany = ?2"
 * 
 * @ejb:value-object match="*"
 *             name="AdBranchSupplier"
 *
 * @jboss:persistence table-name="AD_BR_SPL"
 *
 * @jboss:entity-command name="mysql-get-generated-keys"
 *
 */
public abstract class AdBranchSupplierBean extends AbstractEntityBean {

	// PERSISTENT METHODS
	
    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     * @ejb:pk-field
     *
     * @jboss:column-name name="BSPL_CODE"
     *
     * @jboss:persistence auto-increment="true"
     **/
    public abstract Integer getBsplCode();
    public abstract void setBsplCode(Integer BSPL_CODE);
	
    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"   
     *
     * @jboss:column-name name="BSPL_GL_COA_PYBL_ACCNT"
     **/
    public abstract Integer getBsplGlCoaPayableAccount();
     /**
      * @ejb:interface-method view-type="local"
      **/ 
    public abstract void setBsplGlCoaPayableAccount(Integer BSPL_GL_COA_PYBL_ACCNT);
    
    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"   
     *
     * @jboss:column-name name="BSPL_GL_COA_EXPNS_ACCNT"
     **/
    public abstract Integer getBsplGlCoaExpenseAccount();
     /**
      * @ejb:interface-method view-type="local"
      **/ 
    public abstract void setBsplGlCoaExpenseAccount(Integer BSPL_GL_COA_EXPNS_ACCNT);
      
    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"   
     *
     * @jboss:column-name name="BSPL_DWNLD_STATUS"
     **/
    public abstract char getBsplSupplierDownloadStatus();
     /**
      * @ejb:interface-method view-type="local"
      **/ 
    public abstract void setBsplSupplierDownloadStatus(char BSPL_DWNLD_STATUS);
    
    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"   
     *
     * @jboss:column-name name="AP_SUPPLIER"
     **/
    public abstract Integer getBsplApSupplier();
     /**
      * @ejb:interface-method view-type="local"
      **/ 
    public abstract void setBsplApSupplier(Integer AP_SUPPLIER);

    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"   
     *
     * @jboss:column-name name="AD_BRANCH"
     **/
    public abstract Integer getBsplAdBranch();
     /**
      * @ejb:interface-method view-type="local"
      **/ 
    public abstract void setBsplAdBranch(Integer AD_BRANCH);

    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"   
     *
     * @jboss:column-name name="BSPL_AD_CMPNY"
     **/
    public abstract Integer getBsplAdCompany();
     /**
      * @ejb:interface-method view-type="local"
      **/ 
    public abstract void setBsplAdCompany(Integer BSPL_AD_CMPNY);
    
	// RELATIONSHIP METHODS
	
    /**
     * @ejb:interface-method view-type="local"
     * @ejb:relation name="branch-branchsuppliers"
     *               role-name="branchsupplier-has-one-branch"
     *
     * @jboss:relation related-pk-field="brCode"
     *                 fk-column="AD_BRANCH"
     */
    public abstract LocalAdBranch getAdBranch();
    /**
	 * @ejb:interface-method view-type="local"
	 **/
    public abstract void setAdBranch(LocalAdBranch adBranch);

    /**
     * @ejb:interface-method view-type="local"
     * @ejb:relation name="supplier-branchsuppliers"
     *               role-name="branchsupplier-has-one-supplier"
     * 
     * @jboss:relation related-pk-field="splCode"
     * 				   fk-column="AP_SUPPLIER"
     */
    public abstract LocalApSupplier getApSupplier();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
    public abstract void setApSupplier(LocalApSupplier apSupplier);
    
    
	// ENTITY BEAN METHODS
	
    /**
     * @ejb:create-method view-type="local"
     **/
    public Integer ejbCreate(Integer BSPL_CODE, Integer BSPL_GL_COA_PYBL_ACCNT, 
    		Integer BSPL_GL_COA_EXPNS_ACCNT, char BSPL_DWNLD_STATUS, Integer BSPL_AD_CMPNY)     	
         throws CreateException {	
     	
         Debug.print("AdBranchSupplierBean ejbCreate");
     
         setBsplCode(BSPL_CODE);
         setBsplGlCoaPayableAccount(BSPL_GL_COA_PYBL_ACCNT);
         setBsplGlCoaExpenseAccount(BSPL_GL_COA_EXPNS_ACCNT);
         setBsplSupplierDownloadStatus(BSPL_DWNLD_STATUS);
         setBsplAdCompany(BSPL_AD_CMPNY);
         
         return null;
     }
     
    /**
     * @ejb:create-method view-type="local"
     **/
    public Integer ejbCreate(Integer BSPL_GL_COA_PYBL_ACCNT, 
    		Integer BSPL_GL_COA_EXPNS_ACCNT, char BSPL_DWNLD_STATUS, Integer BSPL_AD_CMPNY)     	
         throws CreateException {	
     	
         Debug.print("AdBranchSupplierBean ejbCreate");
     
         setBsplGlCoaPayableAccount(BSPL_GL_COA_PYBL_ACCNT);
         setBsplGlCoaExpenseAccount(BSPL_GL_COA_EXPNS_ACCNT);
         setBsplSupplierDownloadStatus(BSPL_DWNLD_STATUS);
         setBsplAdCompany(BSPL_AD_CMPNY);

         return null;
     }

    public void ejbPostCreate(Integer BSPL_CODE, Integer BSPL_GL_COA_PYBL_ACCNT, 
    		Integer BSPL_GL_COA_EXPNS_ACCNT, char BSPL_DWNLD_STATUS, Integer BSPL_AD_CMPNY)
         throws CreateException { }

    public void ejbPostCreate(Integer BSPL_GL_COA_PYBL_ACCNT, 
    		Integer BSPL_GL_COA_EXPNS_ACCNT, char BSPL_DWNLD_STATUS, Integer BSPL_AD_CMPNY)
         throws CreateException { }
      
}
