/*
 * com/ejb/ar/LocalAdCompanyBean.java
 *
 * Created on February 05, 2004, 9:41 AM
 */

package com.ejb.ad;

import javax.ejb.CreateException;

import com.util.AbstractEntityBean;
import com.util.Debug;

/**
 *
 * @author  Dennis M. Hilario
 *
 * @ejb:bean name="AdCompanyEJB"
 *           display-name="Company Entity"
 *           type="CMP"
 *           cmp-version="2.x"
 *           view-type="local"
 *           local-jndi-name="omega-ejb/AdCompanyEJB"
 *           schema="AdCompany"
 *           primkey-field="cmpCode"
 *       
 * @ejb:pk class="java.lang.Integer"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="gluser"
 *                        role-link="gluserlink"
 *
 * @ejb:permission role-name="gluser"
 *
 * @ejb:interface local-class="com.ejb.ad.LocalAdCompany"
 *                local-extends="javax.ejb.EJBLocalObject"
 *
 * @ejb:home local-class="com.ejb.ad.LocalAdCompanyHome"
 *           local-extends="javax.ejb.EJBLocalHome"
 * 
 * @ejb:finder signature="LocalAdCompany findByCmpName(java.lang.String CMP_NM)"
 *             query="SELECT OBJECT(cmp) FROM AdCompany cmp WHERE cmp.cmpName = ?1"
 * 
 * @ejb:finder signature="LocalAdCompany findByCmpShortName(java.lang.String CMP_SHRT_NM)"
 *             query="SELECT OBJECT(cmp) FROM AdCompany cmp WHERE cmp.cmpShortName = ?1"
 *
 * @ejb:value-object match="*"
 *             name="AdCompany"
 *
 * @jboss:persistence table-name="AD_CMPNY"
 *
 * @jboss:entity-command name="mysql-get-generated-keys"
 *
 */

public abstract class AdCompanyBean extends AbstractEntityBean {

   
   // Access methods for persistent fields

   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    * @ejb:pk-field
    *
    * @jboss:column-name name="CMP_CODE"
    *
    * @jboss:persistence auto-increment="true"
    **/
   public abstract java.lang.Integer getCmpCode();
   public abstract void setCmpCode(java.lang.Integer CMP_CODE);

   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="CMP_NM"
    **/
   public abstract String getCmpName();
   /**
	* @ejb:interface-method view-type="local"
	**/     
   public abstract void setCmpName(String CMP_NM);
   
   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="CMP_SHRT_NM"
    **/
   public abstract String getCmpShortName();
   /**
	* @ejb:interface-method view-type="local"
	**/     
   public abstract void setCmpShortName(String CMP_SHRT_NM);
   
   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="CMP_TX_PYR_NM"
    **/
   public abstract String getCmpTaxPayerName();
   /**
	* @ejb:interface-method view-type="local"
	**/     
   public abstract void setCmpTaxPayerName(String CMP_TX_PYR_NM);
   
   
   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="CMP_CNTCT"
    **/
   public abstract String getCmpContact();
   /**
	* @ejb:interface-method view-type="local"
	**/     
   public abstract void setCmpContact(String CMP_CNTCT);
   
   
   
   
   
   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="CMP_WLCM_NT"
    **/
   public abstract String getCmpWelcomeNote();
   /**
	* @ejb:interface-method view-type="local"
	**/     
   public abstract void setCmpWelcomeNote(String CMP_WLCM_NT);

   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="CMP_DESC"
    **/
   public abstract String getCmpDescription();
   /**
	* @ejb:interface-method view-type="local"
	**/  
   public abstract void setCmpDescription(String CMP_DESC);
   
   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="CMP_ADDRSS"
    **/
   public abstract String getCmpAddress();
   /**
	* @ejb:interface-method view-type="local"
	**/  
   public abstract void setCmpAddress(String CMP_ADDRSS);

   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="CMP_CTY"
    **/
   public abstract String getCmpCity();
   /**
	* @ejb:interface-method view-type="local"
	**/  
   public abstract void setCmpCity(String CMP_CTY);

   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="CMP_ZP"
    **/
   public abstract String getCmpZip();
   /**
	* @ejb:interface-method view-type="local"
	**/  
   public abstract void setCmpZip(String CMP_ZP);

   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="CMP_CNTRY"
    **/
   public abstract String getCmpCountry();
   /**
	* @ejb:interface-method view-type="local"
	**/  
   public abstract void setCmpCountry(String CMP_CNTRY);
   
   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="CMP_PHN"
    **/
   public abstract String getCmpPhone();
   /**
	* @ejb:interface-method view-type="local"
	**/  
   public abstract void setCmpPhone(String CMP_PHN);
   
   
   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="CMP_FX"
    **/
   public abstract String getCmpFax();
   /**
	* @ejb:interface-method view-type="local"
	**/  
   public abstract void setCmpFax(String CMP_FX);
   
   
   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="CMP_EML"
    **/
   public abstract String getCmpEmail();
   /**
	* @ejb:interface-method view-type="local"
	**/  
   public abstract void setCmpEmail(String CMP_EML);
   
   
   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="CMP_TIN"
    **/
   public abstract String getCmpTin();
   /**
	* @ejb:interface-method view-type="local"
	**/  
   public abstract void setCmpTin(String CMP_TIN);
   
   
   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="CMP_ML_SCTN_NO"
    **/
   public abstract String getCmpMailSectionNo();
   /**
	* @ejb:interface-method view-type="local"
	**/  
   public abstract void setCmpMailSectionNo(String CMP_ML_SCTN_NO);
   
   
   
   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="CMP_ML_LT_NO"
    **/
   public abstract String getCmpMailLotNo();
   /**
	* @ejb:interface-method view-type="local"
	**/  
   public abstract void setCmpMailLotNo(String CMP_ML_LT_NO);
   
   
   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="CMP_ML_STRT"
    **/
   public abstract String getCmpMailStreet();
   /**
	* @ejb:interface-method view-type="local"
	**/  
   public abstract void setCmpMailStreet(String CMP_ML_STRT);
   
   
   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="CMP_ML_PO_BX"
    **/
   public abstract String getCmpMailPoBox();
   /**
	* @ejb:interface-method view-type="local"
	**/  
   public abstract void setCmpMailPoBox(String CMP_ML_PO_BX);
   
   
   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="CMP_ML_CNTRY"
    **/
   public abstract String getCmpMailCountry();
   /**
	* @ejb:interface-method view-type="local"
	**/  
   public abstract void setCmpMailCountry(String CMP_ML_CNTRY);
   
   
   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="CMP_ML_PRVNC"
    **/
   public abstract String getCmpMailProvince();
   /**
	* @ejb:interface-method view-type="local"
	**/  
   public abstract void setCmpMailProvince(String CMP_ML_PRVNC);
   
   
   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="CMP_ML_PST_OFFC"
    **/
   public abstract String getCmpMailPostOffice();
   /**
	* @ejb:interface-method view-type="local"
	**/  
   public abstract void setCmpMailPostOffice(String CMP_ML_PST_OFFC);
   
   
   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="CMP_ML_CR_OFF"
    **/
   public abstract String getCmpMailCareOff();
   /**
	* @ejb:interface-method view-type="local"
	**/  
   public abstract void setCmpMailCareOff(String CMP_ML_CR_OFF);
   
   
   
   
   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="CMP_TX_PRD_FRM"
    **/
   public abstract String getCmpTaxPeriodFrom();
   /**
	* @ejb:interface-method view-type="local"
	**/  
   public abstract void setCmpTaxPeriodFrom(String CMP_TX_PRD_FRM);
   
   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="CMP_TX_PRD_TO"
    **/
   public abstract String getCmpTaxPeriodTo();
   /**
	* @ejb:interface-method view-type="local"
	**/  
   public abstract void setCmpTaxPeriodTo(String CMP_TX_PRD_TO);
   
   
   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="CMP_PBLC_OFFC_NM"
    **/
   public abstract String getCmpPublicOfficeName();
   /**
	* @ejb:interface-method view-type="local"
	**/  
   public abstract void setCmpPublicOfficeName(String CMP_PBLC_OFFC_NM);
   
   
   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="CMP_DT_APPNTMNT"
    **/
   public abstract String getCmpDateAppointment();
   /**
	* @ejb:interface-method view-type="local"
	**/  
   public abstract void setCmpDateAppointment(String CMP_DT_APPNTMNT);
   
   

   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="CMP_RVN_OFFC"
    **/
   public abstract String getCmpRevenueOffice();
   /**
	* @ejb:interface-method view-type="local"
	**/  
   public abstract void setCmpRevenueOffice(String CMP_RVN_OFFC);
   
   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="CMP_FSCL_YR_ENDNG"
    **/
   public abstract String getCmpFiscalYearEnding();
   /**
	* @ejb:interface-method view-type="local"
	**/  
   public abstract void setCmpFiscalYearEnding(String CMP_FSCL_YR_ENDNG);

   
   
   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="CMP_INDSTRY"
    **/
   public abstract String getCmpIndustry();
   /**
	* @ejb:interface-method view-type="local"
	**/  
   public abstract void setCmpIndustry(String CMP_INDSTRY);
   
   
   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="CMP_RTND_EARNNGS"
    **/
   public abstract String getCmpRetainedEarnings();
   /**
	* @ejb:interface-method view-type="local"
	**/  
   public abstract void setCmpRetainedEarnings(String CMP_RTND_EARNNGS);
      
   
   // RELATIONSHIP METHODS
   
   /**
     * @ejb:interface-method view-type="local"
     * @ejb:relation name="functionalcurrency-companies"
     *               role-name="company-has-one-functionalcurrency"
     * 
     * @jboss:relation related-pk-field="fcCode"
     *                 fk-column="GL_FUNCTIONAL_CURRENCY"
     */
     public abstract com.ejb.gl.LocalGlFunctionalCurrency getGlFunctionalCurrency();
     public abstract void setGlFunctionalCurrency(com.ejb.gl.LocalGlFunctionalCurrency glFunctionalCurrency);
     
     
     /**
    * @ejb:interface-method view-type="local"
    * @ejb:relation name="field-companies"
    *               role-name="company-has-one-field"
    *
    * @jboss:relation related-pk-field="flCode"
    *                 fk-column="GEN_FIELD"
    */
   public abstract com.ejb.genfld.LocalGenField getGenField();
   public abstract void setGenField(com.ejb.genfld.LocalGenField genField);
      

   // Access methods for relationship fields
   
   // No Business methods

   // EntityBean methods

   /**
    * @ejb:create-method view-type="local"
    **/
  public java.lang.Integer ejbCreate (Integer CMP_CODE, String CMP_NM, String CMP_SHRT_NM, String CMP_TX_PYR_NM, String CMP_CNTCT, String CMP_WLCM_NT, String CMP_DESC,
      String CMP_ADDRSS, String CMP_CTY, String CMP_ZP, String CMP_CNTRY, String CMP_PHN,
      String CMP_FX, String CMP_EML, String CMP_TIN, String CMP_ML_SCTN_NO, String CMP_ML_LT_NO, String CMP_ML_STRT, String CMP_ML_PO_BX, String CMP_ML_CNTRY, String CMP_ML_PRVNC, String CMP_ML_PST_OFFC, String CMP_ML_CR_OFF, String CMP_TX_PRD_FRM, String CMP_TX_PRD_TO, String CMP_PBLC_OFFC_NM, String CMP_DT_APPNTMNT,
      String CMP_RVN_OFFC, String CMP_FSCL_YR_ENDNG, String CMP_INDSTRY, String CMP_RTND_EARNNGS)
      throws CreateException {

      Debug.print("AdCompanyBean ejbCreate");

      setCmpCode(CMP_CODE);
      setCmpName(CMP_NM);
      setCmpShortName(CMP_SHRT_NM);
      setCmpTaxPayerName(CMP_TX_PYR_NM);
      setCmpContact(CMP_CNTCT);
      setCmpWelcomeNote(CMP_WLCM_NT);
      setCmpDescription(CMP_DESC);
      setCmpAddress(CMP_ADDRSS);
      setCmpCity(CMP_CTY);
      setCmpZip(CMP_ZP);
      setCmpCountry(CMP_CNTRY);
      setCmpPhone(CMP_PHN);
      setCmpFax(CMP_FX);
      setCmpEmail(CMP_EML);
      setCmpTin(CMP_TIN);
      setCmpMailSectionNo(CMP_ML_SCTN_NO);
      setCmpMailLotNo(CMP_ML_LT_NO);
      setCmpMailStreet(CMP_ML_STRT);
      setCmpMailPoBox(CMP_ML_PO_BX);
      setCmpMailCountry(CMP_ML_CNTRY);
      setCmpMailProvince(CMP_ML_PRVNC);
      setCmpMailPostOffice(CMP_ML_PST_OFFC);
      setCmpMailCareOff(CMP_ML_CR_OFF);
      setCmpTaxPeriodFrom(CMP_TX_PRD_FRM);
      setCmpTaxPeriodTo(CMP_TX_PRD_TO);
      setCmpPublicOfficeName(CMP_PBLC_OFFC_NM);
      setCmpDateAppointment(CMP_DT_APPNTMNT);
      setCmpRevenueOffice(CMP_RVN_OFFC);
      setCmpFiscalYearEnding(CMP_FSCL_YR_ENDNG);
      setCmpIndustry(CMP_INDSTRY);
      setCmpRetainedEarnings(CMP_RTND_EARNNGS);
      
      return null;
   }
   
  /**
    * @ejb:create-method view-type="local"
    **/
   public java.lang.Integer ejbCreate (String CMP_NM, String CMP_SHRT_NM, String CMP_TX_PYR_NM, String CMP_CNTCT, String CMP_WLCM_NT, String CMP_DESC,
      String CMP_ADDRSS, String CMP_CTY, String CMP_ZP, String CMP_CNTRY, String CMP_PHN,
      String CMP_FX, String CMP_EML, String CMP_TIN, String CMP_ML_SCTN_NO, String CMP_ML_LT_NO, String CMP_ML_STRT, String CMP_ML_PO_BX, String CMP_ML_CNTRY, String CMP_ML_PRVNC, String CMP_ML_PST_OFFC, String CMP_ML_CR_OFF, String CMP_TX_PRD_FRM, String CMP_TX_PRD_TO, String CMP_PBLC_OFFC_NM, String CMP_DT_APPNTMNT,
      String CMP_RVN_OFFC, String CMP_FSCL_YR_ENDNG, String CMP_INDSTRY, String CMP_RTND_EARNNGS)
      throws CreateException {
      
      Debug.print("AdCompanyBean ejbCreate"); 
         	
      setCmpName(CMP_NM);
      setCmpShortName(CMP_SHRT_NM);
      setCmpTaxPayerName(CMP_TX_PYR_NM);
      setCmpContact(CMP_CNTCT);
      setCmpWelcomeNote(CMP_WLCM_NT);
      setCmpDescription(CMP_DESC);
      setCmpAddress(CMP_ADDRSS);
      setCmpCity(CMP_CTY);
      setCmpZip(CMP_ZP);
      setCmpCountry(CMP_CNTRY);
      setCmpPhone(CMP_PHN);
      setCmpFax(CMP_FX);
      setCmpEmail(CMP_EML);
      setCmpTin(CMP_TIN);
      setCmpMailSectionNo(CMP_ML_SCTN_NO);
      setCmpMailLotNo(CMP_ML_LT_NO);
      setCmpMailStreet(CMP_ML_STRT);
      setCmpMailPoBox(CMP_ML_PO_BX);
      setCmpMailCountry(CMP_ML_CNTRY);
      setCmpMailProvince(CMP_ML_PRVNC);
      setCmpMailPostOffice(CMP_ML_PST_OFFC);
      setCmpMailCareOff(CMP_ML_CR_OFF);
      setCmpTaxPeriodFrom(CMP_TX_PRD_FRM);
      setCmpTaxPeriodTo(CMP_TX_PRD_TO);
      setCmpPublicOfficeName(CMP_PBLC_OFFC_NM);
      setCmpDateAppointment(CMP_DT_APPNTMNT);
      setCmpRevenueOffice(CMP_RVN_OFFC);
      setCmpFiscalYearEnding(CMP_FSCL_YR_ENDNG);
      setCmpIndustry(CMP_INDSTRY);
      setCmpRetainedEarnings(CMP_RTND_EARNNGS);
            
      return null;
   }   
   
   public void ejbPostCreate (Integer CMP_CODE, String CMP_NM, String CMP_SHRT_NM, String CMP_TX_PYR_NM, String CMP_CNTCT, String CMP_WLCM_NT, String CMP_DESC,
      String CMP_ADDRSS, String CMP_CTY, String CMP_ZP, String CMP_CNTRY, String CMP_PHN,
      String CMP_FX, String CMP_EML, String CMP_TIN, String CMP_ML_SCTN_NO, String CMP_ML_LT_NO, String CMP_ML_STRT, String CMP_ML_PO_BX, String CMP_ML_CNTRY, String CMP_ML_PRVNC, String CMP_ML_PST_OFFC, String CMP_ML_CR_OFF, String CMP_TX_PRD_FRM, String CMP_TX_PRD_TO, String CMP_PBLC_OFFC_NM, String CMP_DT_APPNTMNT,
      String CMP_RVN_OFFC, String CMP_FSCL_YR_ENDNG, String CMP_INDSTRY, String CMP_RTND_EARNNGS)
      throws CreateException { }

   public void ejbPostCreate (String CMP_NM, String CMP_SHRT_NM, String CMP_TX_PYR_NM, String CMP_CNTCT, String CMP_WLCM_NT, String CMP_DESC,
      String CMP_ADDRSS, String CMP_CTY, String CMP_ZP, String CMP_CNTRY, String CMP_PHN,
      String CMP_FX, String CMP_EML, String CMP_TIN, String CMP_ML_SCTN_NO, String CMP_ML_LT_NO, String CMP_ML_STRT, String CMP_ML_PO_BX, String CMP_ML_CNTRY, String CMP_ML_PRVNC, String CMP_ML_PST_OFFC, String CMP_ML_CR_OFF, String CMP_TX_PRD_FRM, String CMP_TX_PRD_TO, String CMP_PBLC_OFFC_NM, String CMP_DT_APPNTMNT,
      String CMP_RVN_OFFC, String CMP_FSCL_YR_ENDNG, String CMP_INDSTRY, String CMP_RTND_EARNNGS)
      throws CreateException { }

} // AdCompanyBean class
