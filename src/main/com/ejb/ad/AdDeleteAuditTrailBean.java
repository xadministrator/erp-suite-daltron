/*
 * com/ejb/ad/AdDeleteAuditTrail
 *
 * Created on November 9, 2007, 9:20 AM
 */
 
package com.ejb.ad;

import java.util.Collection;
import java.util.Date;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;

import com.util.AbstractEntityBean;
import com.util.Debug;

import com.ejb.gl.LocalGlUserStaticReport;

/**
 *
 * @author  Dennis M. Hilario
 *
 * @ejb:bean name="AdDeleteAuditTrailEJB"
 *           display-name="User Entity"
 *           type="CMP"
 *           cmp-version="2.x"
 *           view-type="local"
 *           local-jndi-name="omega-ejb/AdDeleteAuditTrailEJB"
 *           schema="AdDeleteAuditTrail"
 *           primkey-field="datCode"
 *
 * @ejb:pk class="java.lang.Integer"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="aduser"
 *                        role-link="aduserlink"
 *
 * @ejb:permission role-name="aduser"
 *
 * @ejb:interface local-class="com.ejb.ad.LocalAdDeleteAuditTrail"
 *                local-extends="javax.ejb.EJBLocalObject"
 *
 * @ejb:home local-class="com.ejb.ad.LocalAdDeleteAuditTrailHome"
 *           local-extends="javax.ejb.EJBLocalHome"
 *           
 * @ejb:select signature="Collection ejbSelectGeneric(java.lang.String jbossQl, java.lang.Object[] args)"
 *             result-type-mapping="Local"
 *             method-intf="LocalHome"
 *             query="SELECT OBJECT(dat) FROM AdDeleteAuditTrail dat"
 *
 * @jboss:query signature="Collection ejbSelectGeneric(java.lang.String jbossQl, java.lang.Object[] args)"
 *                         dynamic="true"
 
 * 
 * @ejb:value-object match="*"
 *             name="AdDeleteAuditTrail"
 *
 * @jboss:persistence table-name="AD_DLT_ADT_TRL"
 *
 * @jboss:entity-command name="mysql-get-generated-keys"
 */

public abstract class AdDeleteAuditTrailBean extends AbstractEntityBean {
    
   
    // PERSISTENT METHODS
    
    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     * @ejb:pk-field
     *
     * @jboss:column-name name="DAT_CODE"
     *
     * @jboss:persistence auto-increment="true"
     **/
    public abstract Integer getDatCode();
    public abstract void setDatCode(Integer DAT_CODE);
    
    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="DAT_TYP"
     **/
    public abstract String getDatType();
     /**
      * @ejb:interface-method view-type="local"
      **/  
    public abstract void setDatType(String DAT_TYP);
    
    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="DAT_TXN_DT"
     **/
    public abstract Date getDatTxnDate();
     /**
      * @ejb:interface-method view-type="local"
      **/  
    public abstract void setDatTxnDate(Date DAT_TXN_DT);
    
    
    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="DAT_DCMNT_NMBR"
     **/
    public abstract String getDatDocumentNumber();
     /**
      * @ejb:interface-method view-type="local"
      **/  
    public abstract void setDatDocumentNumber(String DAT_DCMNT_NMBR);
    
    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="DAT_RFRNC_NMBR"
     **/
    public abstract String getDatReferenceNumber();
     /**
      * @ejb:interface-method view-type="local"
      **/  
    public abstract void setDatReferenceNumber(String DAT_RFRNC_NMBR);

    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="DAT_AMNT"
     **/
    public abstract double getDatAmount();
     /**
      * @ejb:interface-method view-type="local"
      **/  
    public abstract void setDatAmount(double DAT_AMNT);

    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="DAT_USR"
     **/
    public abstract String getDatUser();
     /**
      * @ejb:interface-method view-type="local"
      **/  
    public abstract void setDatUser(String DAT_USR);

    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="DAT_DT"
     **/
    public abstract Date getDatDate();
     /**
      * @ejb:interface-method view-type="local"
      **/  
    public abstract void setDatDate(Date DAT_DT);
    
    
    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="DAT_AD_CMPNY"
     **/
    public abstract Integer getDatAdCompany();
     /**
      * @ejb:interface-method view-type="local"
      **/  
    public abstract void setDatAdCompany(Integer DAT_AD_CMPNY);
    
    /**
     * @jboss:dynamic-ql
     */
     public abstract Collection ejbSelectGeneric(java.lang.String jbossQl, java.lang.Object[] args)
        throws FinderException;
    
    /**
     * @ejb:home-method view-type="local"
     */
     public Collection ejbHomeGetDatByCriteria(java.lang.String jbossQl, java.lang.Object[] args)
        throws FinderException {
        	
        return ejbSelectGeneric(jbossQl, args);
     }

    // ENTITY METHODS
    
    /**
     * @ejb:create-method view-type="local"
     **/
     public Integer ejbCreate(Integer DAT_CODE, String DAT_TYP, Date DAT_TXN_DT, String DAT_DCMNT_NMBR, String DAT_RFRNC_NMBR,
    		 double DAT_AMNT, String DAT_USR, Date DAT_DT, Integer DAT_AD_CMPNY)
         throws CreateException {
      	
         Debug.print("AdUserBean ejbCreate");
      
         setDatCode(DAT_CODE);
         setDatType(DAT_TYP);
         setDatTxnDate(DAT_TXN_DT);
         setDatDocumentNumber(DAT_DCMNT_NMBR);
         setDatReferenceNumber(DAT_RFRNC_NMBR);
         setDatAmount(DAT_AMNT);
         setDatUser(DAT_USR);
         setDatDate(DAT_DT);
         setDatAdCompany(DAT_AD_CMPNY);
        
         return null;
    }
    
    /**
     * @ejb:create-method view-type="local"
     **/
     public Integer ejbCreate(String DAT_TYP, Date DAT_TXN_DT, String DAT_DCMNT_NMBR, String DAT_RFRNC_NMBR,
    		 double DAT_AMNT, String DAT_USR, Date DAT_DT, Integer DAT_AD_CMPNY)
         throws CreateException {
      	
         Debug.print("AdUserBean ejbCreate");
         
         setDatType(DAT_TYP);
         setDatTxnDate(DAT_TXN_DT);
         setDatDocumentNumber(DAT_DCMNT_NMBR);
         setDatReferenceNumber(DAT_RFRNC_NMBR);
         setDatAmount(DAT_AMNT);
         setDatUser(DAT_USR);
         setDatDate(DAT_DT);
         setDatAdCompany(DAT_AD_CMPNY);
        
         return null;
     }
    
     public void ejbPostCreate(Integer DAT_CODE, String DAT_TYP, Date DAT_TXN_DT, String DAT_DCMNT_NMBR, String DAT_RFRNC_NMBR,
    		 double DAT_AMNT, String DAT_USR, Date DAT_DT, Integer DAT_AD_CMPNY)      
         throws CreateException { }
      
     public void ejbPostCreate(String DAT_TYP, Date DAT_TXN_DT, String DAT_DCMNT_NMBR, String DAT_RFRNC_NMBR,
    		 double DAT_AMNT, String DAT_USR, Date DAT_DT, Integer DAT_AD_CMPNY)
         throws CreateException { }     
}

      
      	
      	