/*
 * com/ejb/ad/LocalAdDiscountBean.java
 *
 * Created on January 9, 2003, 12:08 PM
 */

package com.ejb.ad;

import javax.ejb.CreateException;

import com.util.AbstractEntityBean;
import com.util.Debug;

/**
 *
 * @author  Neil Andrew M. Ajero
 *
 * @ejb:bean name="AdDiscountEJB"
 *           display-name="Discount Entity"
 *           type="CMP"
 *           cmp-version="2.x"
 *           view-type="local"
 *           local-jndi-name="omega-ejb/AdDiscountEJB"
 *           schema="AdDiscount"
 *           primkey-field="dscCode"
 *
 * @ejb:pk class="java.lang.Integer"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="aruser"
 *                        role-link="aruserlink"
 *
 * @ejb:permission role-name="aruser"
 *
 * @ejb:interface local-class="com.ejb.ad.LocalAdDiscount"
 *                local-extends="javax.ejb.EJBLocalObject"
 *
 * @ejb:home local-class="com.ejb.ad.LocalAdDiscountHome"
 *           local-extends="javax.ejb.EJBLocalHome"
 *
 * @ejb:finder signature="Collection findByPsLineNumberAndPytName(short PS_LN_NMBR, java.lang.String PYT_NM, java.lang.Integer DSC_AD_CMPNY)"
 *             query="SELECT OBJECT(dsc) FROM AdPaymentSchedule ps, IN(ps.adDiscounts) dsc WHERE ps.psLineNumber = ?1 AND ps.adPaymentTerm.pytName = ?2 AND dsc.dscAdCompany = ?3"
 *
 * @ejb:finder signature="Collection findByPsCode(java.lang.Integer PS_CODE, java.lang.Integer DSC_AD_CMPNY)"
 *             query="SELECT OBJECT(dsc) FROM AdPaymentSchedule ps, IN(ps.adDiscounts) dsc WHERE ps.psCode = ?1 AND dsc.dscAdCompany = ?2"
 *
 * @jboss:query signature="Collection findByPsCode(java.lang.Integer PS_CODE, java.lang.Integer DSC_AD_CMPNY)"
 *             query="SELECT OBJECT(dsc) FROM AdPaymentSchedule ps, IN(ps.adDiscounts) dsc WHERE ps.psCode = ?1  AND dsc.dscAdCompany = ?2 ORDER BY dsc.dscPaidWithinDay"
 *
 * @ejb:finder signature="Collection findByPsCode(java.lang.Integer PS_CODE, java.lang.Integer DSC_AD_CMPNY)"
 *             query="SELECT OBJECT(dsc) FROM AdPaymentSchedule ps, IN(ps.adDiscounts) dsc WHERE ps.psCode = ?1 AND dsc.dscAdCompany = ?2"
 *
 * @jboss:query signature="Collection findByPsCode(java.lang.Integer PS_CODE, java.lang.Integer DSC_AD_CMPNY)"
 *             query="SELECT OBJECT(dsc) FROM AdPaymentSchedule ps, IN(ps.adDiscounts) dsc WHERE ps.psCode = ?1  AND dsc.dscAdCompany = ?2 ORDER BY dsc.dscPaidWithinDay"
 *
 * @ejb:finder signature="LocalAdDiscount findByPsCodeAndDscPaidWithinDate(java.lang.Integer PS_CODE, short DSC_PD_WTHN_DY, java.lang.Integer DSC_AD_CMPNY)"
 *             query="SELECT OBJECT(dsc) FROM AdPaymentSchedule ps, IN(ps.adDiscounts) dsc WHERE ps.psCode = ?1 AND dsc.dscPaidWithinDay = ?2 AND dsc.dscAdCompany = ?3"
 *             
 * @ejb:finder signature="LocalAdDiscount findByDscPaidWithinDate(short DSC_PD_WTHN_DY, java.lang.Integer DSC_AD_CMPNY)"
 *             query="SELECT OBJECT(dsc) FROM AdDiscount dsc WHERE dsc.dscPaidWithinDay = ?1 AND dsc.dscAdCompany = ?2"
 * 
 * @ejb:value-object match="*"
 *             name="AdDiscount"
 *
 * @jboss:persistence table-name="AD_DSCNT"
 *
 * @jboss:entity-command name="mysql-get-generated-keys"
 */

public abstract class AdDiscountBean extends AbstractEntityBean {
    
    
    // PERSISTENT METHODS
    
    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     * @ejb:pk-field
     *
     * @jboss:column-name name="DSC_CODE"
     *
     * @jboss:persistence auto-increment="true"
     **/
    public abstract Integer getDscCode();
    public abstract void setDscCode(Integer DSC_CODE);
    
    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="DSC_DSCNT_PRCNT"
     **/
    public abstract double getDscDiscountPercent();
     /**
      * @ejb:interface-method view-type="local"
      **/    
    public abstract void setDscDiscountPercent(double DSC_DSCNT_PRCNT);
    
    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="DSC_PD_WTHN_DY"
     **/
    public abstract short getDscPaidWithinDay();
     /**
      * @ejb:interface-method view-type="local"
      **/    
    public abstract void setDscPaidWithinDay(short DSC_PD_WTHN_DY);
    
    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="DSC_AD_CMPNY"
     **/
    public abstract Integer getDscAdCompany();
     /**
      * @ejb:interface-method view-type="local"
      **/    
    public abstract void setDscAdCompany(Integer DSC_AD_CMPNY);
    
    // RELATIONSHIP METHODS
    
    /**
     * @ejb:interface-method view-type="local"
     * @ejb:relation name="paymentschedule-discounts"
     *               role-name="discount-has-one-paymentschedule"
     *               cascade-delete="yes"
     * 
     * @jboss:relation related-pk-field="psCode"
     *                 fk-column="AD_PAYMENT_SCHEDULE"
     */
     public abstract LocalAdPaymentSchedule getAdPaymentSchedule();
     public abstract void setAdPaymentSchedule(LocalAdPaymentSchedule adPaymentSchedule);
     
    // BUSINESS METHODS
    
    // ENTITY METHODS
    
    /**
     * @ejb:create-method view-type="local"
     **/
     public Integer ejbCreate(Integer DSC_CODE, 
    		 double DSC_DSCNT_PRCNT, short DSC_PD_WTHN_DY,
		 Integer DSC_AD_CMPNY)
         throws CreateException {
      	
         Debug.print("AdDiscountBean ejbCreate");
      
         setDscCode(DSC_CODE);
         setDscDiscountPercent(DSC_DSCNT_PRCNT);
         setDscPaidWithinDay(DSC_PD_WTHN_DY);
         setDscAdCompany(DSC_AD_CMPNY);
        
         return null;
     }
     
    /**
     * @ejb:create-method view-type="local"
     **/
     public Integer ejbCreate( 
    		 double DSC_DSCNT_PRCNT, short DSC_PD_WTHN_DY,
		 Integer DSC_AD_CMPNY)
         throws CreateException {
      	
         Debug.print("AdDiscountBean ejbCreate");
      
         setDscDiscountPercent(DSC_DSCNT_PRCNT);
         setDscPaidWithinDay(DSC_PD_WTHN_DY);
         setDscAdCompany(DSC_AD_CMPNY);
        
         return null;
     }
     
     public void ejbPostCreate(Integer DSC_CODE, 
    		 double DSC_DSCNT_PRCNT, short DSC_PD_WTHN_DY,
		 Integer DSC_AD_CMPNY)      
         throws CreateException { }
      
     public void ejbPostCreate( 
    		 double DSC_DSCNT_PRCNT, short DSC_PD_WTHN_DY,
		 Integer DSC_AD_CMPNY)
         throws CreateException { } 
         
} 