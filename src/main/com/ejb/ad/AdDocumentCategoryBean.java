package com.ejb.ad;

import java.util.Collection;

import javax.ejb.CreateException;
import javax.ejb.EJBException;

import com.ejb.gl.LocalAdApplication;
import com.ejb.gl.LocalAdDocumentSequenceAssignment;
import com.util.AbstractEntityBean;
import com.util.Debug;

/**
 * @ejb:bean name="AdDocumentCategoryEJB"
 *           display-name="Document Category Entity"
 *           type="CMP"
 *           cmp-version="2.x"
 *           view-type="local"
 *           local-jndi-name="omega-ejb/AdDocumentCategoryEJB"
 *           schema="AdDocumentCategory"
 *           primkey-field="dcCode"
 *       
 * @ejb:pk class="java.lang.Integer"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="gluser"
 *                        role-link="gluserlink"
 *
 * @ejb:permission role-name="gluser"
 *
 * @ejb:interface local-class="com.ejb.gl.LocalAdDocumentCategory"
 *                local-extends="javax.ejb.EJBLocalObject"
 *
 * @ejb:home local-class="com.ejb.gl.LocalAdDocumentCategoryHome"
 *                local-extends="javax.ejb.EJBLocalHome"
 *
 * @ejb:finder signature="Collection findDcAll(java.lang.Integer DC_AD_CMPNY)"
 *             query="SELECT OBJECT(dc) FROM AdDocumentCategory dc WHERE dc.dcAdCompany = ?1"
 *
 * @jboss:query signature="Collection findDcAll(java.lang.Integer DC_AD_CMPNY)"
 *             query="SELECT OBJECT(dc) FROM AdDocumentCategory dc WHERE dc.dcAdCompany = ?1 ORDER BY dc.dcName"
 *
 * @ejb:finder signature="LocalAdDocumentCategory findByDcName(java.lang.String DC_NM, java.lang.Integer DC_AD_CMPNY)"
 *             query="SELECT OBJECT(dc) FROM AdDocumentCategory dc WHERE dc.dcName=?1 AND dc.dcAdCompany = ?2"
 *
 * @jboss:persistence table-name="AD_DCMNT_CTGRY"
 *
 * @jboss:entity-command name="mysql-get-generated-keys"
 *
 */

public abstract class AdDocumentCategoryBean extends AbstractEntityBean {

   // Access methods for persistent fields

   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    * @ejb:pk-field
    *
    * @jboss:column-name name="DC_CODE"
    * @jboss:persistence auto-increment="true"
    **/
   public abstract java.lang.Integer getDcCode();
   public abstract void setDcCode(java.lang.Integer DC_CODE);

   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="DC_NM"
    **/
   public abstract java.lang.String getDcName();
   /**
    * @ejb:interface-method view-type="local"
    **/   
   public abstract void setDcName(java.lang.String DC_NM);

   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="DC_DESC"
    **/
   public abstract java.lang.String getDcDescription();
   /**
    * @ejb:interface-method view-type="local"
    **/   
   public abstract void setDcDescription(java.lang.String DC_DESC);
   
   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="DC_AD_CMPNY"
    **/
   public abstract java.lang.Integer getDcAdCompany();
   /**
    * @ejb:interface-method view-type="local"
    **/   
   public abstract void setDcAdCompany(java.lang.Integer DC_AD_CMPNY);

   // Access methods for relationship fields

   /**
    * @ejb:interface-method view-type="local"
    * @ejb:relation name="documentcategory-documentsequenceassignments"
    *               role-name="documentcategory-has-many-documentsequenceassignments"
    */
   public abstract Collection getAdDocumentSequenceAssignments();
   public abstract void setAdDocumentSequenceAssignments(Collection adDocumentSequenceAssignments);

   /**
    * @ejb:interface-method view-type="local"
    * @ejb:relation name="application-documentcategories"
    *               role-name="documentcategory-has-one-application"
    *
    * @jboss:relation related-pk-field="appCode"
    *                 fk-column="AD_APPLICATION"
    */
   public abstract LocalAdApplication getAdApplication();
   public abstract void setAdApplication(LocalAdApplication adApplication);

   // Business methods

   /**
    * @ejb:interface-method view-type="local"
    **/
   public void addAdDocumentSequenceAssignment(LocalAdDocumentSequenceAssignment adDocumentSequenceAssignment) {

      Debug.print("AdDocumentCategoryBean addAdDocumentSequenceAssignment");
      try {
         Collection adDocumentSequenceAssignments = getAdDocumentSequenceAssignments();
	 adDocumentSequenceAssignments.add(adDocumentSequenceAssignment);
      } catch (Exception ex) {
         Debug.print(ex.getMessage());
         throw new EJBException(ex.getMessage());
      }
   }

   /**
    * @ejb:interface-method view-type="local"
    **/
   public void dropAdDocumentSequenceAssignment(LocalAdDocumentSequenceAssignment adDocumentSequenceAssignment) {

      Debug.print("AdDocumentCategoryBean dropAdDocumentSequenceAssignment");
      try {
         Collection adDocumentSequenceAssignments = getAdDocumentSequenceAssignments();
	 adDocumentSequenceAssignments.remove(adDocumentSequenceAssignment);
      } catch (Exception ex) {
         throw new EJBException(ex.getMessage());
      }
   }

   // EntityBean methods

   /**
    * @ejb:create-method view-type="local"
    **/
   public java.lang.Integer ejbCreate (java.lang.Integer DC_CODE, java.lang.String DC_NM, java.lang.String DC_DESC,
   		java.lang.Integer DC_AD_CMPNY)
      throws CreateException {
      
      Debug.print("AdDocumentCategoryBean ejbCreate");
      setDcCode(DC_CODE);
      setDcName(DC_NM);
      setDcDescription(DC_DESC);
      setDcAdCompany(DC_AD_CMPNY);
      
      return null;
   }
   
   /**
    * @ejb:create-method view-type="local"
    **/
   public java.lang.Integer ejbCreate (java.lang.String DC_NM, java.lang.String DC_DESC,
   		java.lang.Integer DC_AD_CMPNY)
      throws CreateException {
      
      Debug.print("AdDocumentCategoryBean ejbCreate");
      
      setDcName(DC_NM);
      setDcDescription(DC_DESC);
      setDcAdCompany(DC_AD_CMPNY);
      
      return null;
   }

   public void ejbPostCreate (java.lang.Integer DC_CODE, java.lang.String DC_NM, java.lang.String DC_DESC,
   		java.lang.Integer DC_AD_CMPNY)
      throws CreateException { }
   
   public void ejbPostCreate (java.lang.String DC_NM, java.lang.String DC_DESC,
   		java.lang.Integer DC_AD_CMPNY)
      throws CreateException { }

}
