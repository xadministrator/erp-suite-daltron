package com.ejb.ad;

import java.util.Collection;

import javax.ejb.CreateException;
import javax.ejb.EJBException;

import com.ejb.gl.LocalAdDocumentCategory;
import com.ejb.gl.LocalAdDocumentSequence;
import com.ejb.gl.LocalGlJournal;
import com.util.AbstractEntityBean;
import com.util.Debug;

/**
 * @ejb:bean name="AdDocumentSequenceAssignmentEJB"
 *           display-name="Document Sequence Assignment Entity"
 *           type="CMP"
 *           cmp-version="2.x"
 *           view-type="local"
 *           local-jndi-name="omega-ejb/AdDocumentSequenceAssignmentEJB"
 *           schema="AdDocumentSequenceAssignment"
 *           primkey-field="dsaCode"
 *       
 * @ejb:pk class="java.lang.Integer"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="gluser"
 *                        role-link="gluserlink"
 *
 * @ejb:permission role-name="gluser"
 *
 * @ejb:interface local-class="com.ejb.gl.LocalAdDocumentSequenceAssignment"
 *                local-extends="javax.ejb.EJBLocalObject"
 *
 * @ejb:home local-class="com.ejb.gl.LocalAdDocumentSequenceAssignmentHome"
 *                local-extends="javax.ejb.EJBLocalHome"
 *
 * @ejb:finder signature="Collection findDsaAll(java.lang.Integer DSA_AD_CMPNY)"
 *             query="SELECT OBJECT(dsa) FROM AdDocumentSequenceAssignment dsa WHERE dsa.dsaAdCompany = ?1"
 *
 * @ejb:finder signature="LocalAdDocumentSequenceAssignment findDsaByCode(java.lang.Integer DSA_CODE, java.lang.Integer DSA_AD_CMPNY)"
 *             query="SELECT OBJECT(dsa) FROM AdDocumentSequenceAssignment dsa WHERE dsa.dsaCode = ?1 AND dsa.dsaAdCompany = ?2"
 *
 * @ejb:finder signature="LocalAdDocumentSequenceAssignment findByDcName(java.lang.String DC_NM, java.lang.Integer DSA_AD_CMPNY)"
 *             query="SELECT OBJECT(dsa) FROM AdDocumentCategory dc, IN(dc.adDocumentSequenceAssignments) dsa WHERE dc.dcName=?1 AND dsa.dsaAdCompany = ?2"
 *
 * @jboss:persistence table-name="AD_DCMNT_SQNC_ASSGNMNT"
 *
 * @jboss:entity-command name="mysql-get-generated-keys"
 *
 */

public abstract class AdDocumentSequenceAssignmentBean extends AbstractEntityBean {

   // Access methods for persistent fields

   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    * @ejb:pk-field
    *
    * @jboss:column-name name="DSA_CODE"
    *
    * @jboss:persistence auto-increment="true"
    **/
   public abstract java.lang.Integer getDsaCode();
   public abstract void setDsaCode(java.lang.Integer DSA_CODE);

   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="DSA_SOB_CODE"
    **/
   public abstract java.lang.Integer getDsaSobCode();
   public abstract void setDsaSobCode(java.lang.Integer DSA_SOB_CODE);

   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="DSA_NXT_SQNC"
    **/
   public abstract String getDsaNextSequence();
   /**
    * @ejb:interface-method view-type="local"
    **/
   public abstract void setDsaNextSequence(String DSA_NXT_SQNC);
   
   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="DSA_AD_CMPNY"
    **/
   public abstract Integer getDsaAdCompany();
   /**
    * @ejb:interface-method view-type="local"
    **/
   public abstract void setDsaAdCompany(Integer DSA_AD_CMPNY);

   // Access methods for relationship fields

   /**
    * @ejb:interface-method view-type="local"
    * @ejb:relation name="documentsequenceassignment-journals"
    *               role-name="documentsequenceassignment-has-many-journals"
    */
   public abstract Collection getGlJournals();
   public abstract void setGlJournals(Collection glJournals);

   /**
    * @ejb:interface-method view-type="local"
    * @ejb:relation name="documentcategory-documentsequenceassignments"
    *               role-name="documentsequenceassignment-has-one-documentcategory"
    *
    * @jboss:relation related-pk-field="dcCode"
    *                 fk-column="AD_DOCUMENT_CATEGORY"
    */
   public abstract LocalAdDocumentCategory getAdDocumentCategory();
   public abstract void setAdDocumentCategory(LocalAdDocumentCategory adDocumentCategory);

   /**
    * @ejb:interface-method view-type="local"
    * @ejb:relation name="documentsequence-documentsequenceassignments"
    *               role-name="documentsequenceassignment-has-one-documentsequence"
    *
    * @jboss:relation related-pk-field="dsCode"
    *                 fk-column="AD_DOCUMENT_SEQUENCE"
    */
   public abstract LocalAdDocumentSequence getAdDocumentSequence();
   public abstract void setAdDocumentSequence (LocalAdDocumentSequence adDocumentSequence);
   
   /**
    * @ejb:interface-method view-type="local"
    * @ejb:relation name="documentsequenceassignment-branchdocumentsequenceassignment"
    *               role-name="documentsequenceassignment-has-many-branchdocumentsequenceassignment"
    */
   public abstract Collection getAdBranchDocumentSequenceAssignments();
   public abstract void setAdBranchDocumentSequenceAssignments(Collection adBranchDocumentSequenceAssignment); 

   // Business methods

   /**
    * @ejb:interface-method view-type="local"
    **/
   public void addGlJournal(LocalGlJournal glJournal) {

      Debug.print("AdDocumentSequenceAssignment addGlJournal");
      try {
         Collection glJournals = getGlJournals();
	 glJournals.add(glJournal);
      } catch (Exception ex) {
         throw new EJBException(ex.getMessage());
      }
   }

   /**
    * @ejb:interface-method view-type="local"
    **/
   public void dropGlJournal(LocalGlJournal glJournal) {

      Debug.print("AdDocumentSequenceAssignment dropGlJournal");
      try {
         Collection glJournals = getGlJournals();
	 glJournals.remove(glJournal);
      } catch (Exception ex) {
         throw new EJBException(ex.getMessage());
      }
   }
   
   /**
    * @ejb:interface-method view-type="local"
    **/
   public void addAdBranchDocumentSequenceAssignments(com.ejb.ad.LocalAdBranchDocumentSequenceAssignment adBranchDocumentSequenceAssignment) {

        Debug.print("AdDocumentSequenceAssignmentBean addAdBranchDocumentSequenceAssignments");
  
        try {
  	
  	        Collection adBranchDsas = getAdBranchDocumentSequenceAssignments();
  	        adBranchDsas.add(adBranchDocumentSequenceAssignment);
	     
        } catch (Exception ex) {
  	
            throw new EJBException(ex.getMessage());
     
        }
      
   }
	
   /**
    * @ejb:interface-method view-type="local"
    **/ 
    public void dropAdBranchDocumentSequenceAssignments(com.ejb.ad.LocalAdBranchDocumentSequenceAssignment adBranchDocumentSequenceAssignment) {
		
		  Debug.print("AdDocumentSequenceAssignmentBean dropAdBranchDocumentSequenceAssignments");
		  
		  try {
		  	
		     Collection adBranchDsas = getAdBranchDocumentSequenceAssignments();
		     adBranchDsas.remove(adBranchDocumentSequenceAssignment);
			     
		  } catch (Exception ex) {
		  	
		     throw new EJBException(ex.getMessage());
		     
		  }
		  
   }

   // EntityBean methods

   /**
    * @ejb:create-method view-type="local"
    **/
   public java.lang.Integer ejbCreate (java.lang.Integer DSA_CODE, java.lang.Integer DSA_SOB_CODE,
      String DSA_NXT_SQNC, Integer DSA_AD_CMPNY)
      throws CreateException {

      Debug.print("AdDocumentSequenceAssignment ejbCreate");
      setDsaCode(DSA_CODE);
      setDsaSobCode(DSA_SOB_CODE);
      setDsaNextSequence(DSA_NXT_SQNC);
      setDsaAdCompany(DSA_AD_CMPNY);
      
      return null;
   }

   /**
    * @ejb:create-method view-type="local"
    **/
   public java.lang.Integer ejbCreate (java.lang.Integer DSA_SOB_CODE,
      String DSA_NXT_SQNC, Integer DSA_AD_CMPNY)
      throws CreateException {

      Debug.print("AdDocumentSequenceAssignment ejbCreate");

      setDsaSobCode(DSA_SOB_CODE);
      setDsaNextSequence(DSA_NXT_SQNC);
      setDsaAdCompany(DSA_AD_CMPNY);
      
      return null;
   }

   public void ejbPostCreate (java.lang.Integer DSA_CODE, java.lang.Integer DSA_SOB_CODE,
      String DSA_NXT_SQNC, Integer DSA_AD_CMPNY)
      throws CreateException { }

   public void ejbPostCreate (java.lang.Integer DSA_SOB_CODE,
      String DSA_NXT_SQNC, Integer DSA_AD_CMPNY)
      throws CreateException { }

}
