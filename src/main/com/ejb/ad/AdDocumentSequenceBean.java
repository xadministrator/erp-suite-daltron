package com.ejb.ad;

import java.util.Collection;
import java.util.Date;

import javax.ejb.CreateException;
import javax.ejb.EJBException;

import com.ejb.gl.LocalAdApplication;
import com.ejb.gl.LocalAdDocumentSequenceAssignment;
import com.util.AbstractEntityBean;
import com.util.Debug;

/**
 * @ejb:bean name="AdDocumentSequenceEJB"
 *           display-name="Document Sequence Entity"
 *           type="CMP"
 *           cmp-version="2.x"
 *           view-type="local"
 *           local-jndi-name="omega-ejb/AdDocumentSequenceEJB"
 *           schema="AdDocumentSequence"
 *           primkey-field="dsCode"
 *       
 * @ejb:pk class="java.lang.Integer"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="gluser"
 *                        role-link="gluserlink"
 *
 * @ejb:permission role-name="gluser"
 *
 * @ejb:interface local-class="com.ejb.gl.LocalAdDocumentSequence"
 *                local-extends="javax.ejb.EJBLocalObject"
 *
 * @ejb:home local-class="com.ejb.gl.LocalAdDocumentSequenceHome"
 *                local-extends="javax.ejb.EJBLocalHome"
 *
 * @ejb:finder signature="Collection findDsAll(java.lang.Integer DS_AD_CMPNY)"
 *             query="SELECT OBJECT(ds) FROM AdDocumentSequence ds WHERE ds.dsAdCompany = ?1"
 * 
 * @jboss:query signature="Collection findDsAll(java.lang.Integer DS_AD_CMPNY)"
 *             query="SELECT OBJECT(ds) FROM AdDocumentSequence ds WHERE ds.dsAdCompany = ?1 ORDER BY ds.dsSequenceName"
 *
 * @ejb:finder signature="LocalAdDocumentSequence findByDsName(java.lang.String DS_NM, java.lang.Integer DS_AD_CMPNY)"
 *             query="SELECT OBJECT(ds) FROM AdDocumentSequence ds WHERE ds.dsSequenceName=?1 AND ds.dsAdCompany = ?2"
 *
 * @ejb:finder signature="Collection findDsEnabled(java.util.Date CRRNT_DT, java.lang.Integer DS_AD_CMPNY)"
 *             query="SELECT OBJECT(ds) FROM AdDocumentSequence ds WHERE ((ds.dsDateFrom <= ?1 AND ds.dsDateTo >= ?1) OR (ds.dsDateFrom <= ?1 AND ds.dsDateTo IS NULL)) AND ds.dsAdCompany = ?2"
 *
 * @jboss:query signature="Collection findDsEnabled(java.util.Date CRRNT_DT, java.lang.Integer DS_AD_CMPNY)"
 *             query="SELECT OBJECT(ds) FROM AdDocumentSequence ds WHERE ((ds.dsDateFrom <= ?1 AND ds.dsDateTo >= ?1) OR (ds.dsDateFrom <= ?1 AND ds.dsDateTo IS NULL)) AND ds.dsAdCompany = ?2 ORDER BY ds.dsSequenceName"
 *
 * @jboss:persistence table-name="AD_DCMNT_SQNC"
 *
 * @jboss:entity-command name="mysql-get-generated-keys"
 *
 */

public abstract class AdDocumentSequenceBean extends AbstractEntityBean {


   // Access methods for persistent fields

   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    * @ejb:pk-field
    *
    * @jboss:column-name name="DS_CODE"
    *
    * @jboss:persistence auto-increment="true"
    **/
   public abstract java.lang.Integer getDsCode();
   public abstract void setDsCode(java.lang.Integer DS_CODE);

   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="DS_SQNC_NM"
    **/
   public abstract java.lang.String getDsSequenceName();
   /**
    * @ejb:interface-method view-type="local"
    **/
   public abstract void setDsSequenceName(java.lang.String DS_SQNC_NM);

   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="DS_DT_FRM"
    **/
   public abstract Date getDsDateFrom();
   /**
    * @ejb:interface-method view-type="local"
    **/
   public abstract void setDsDateFrom(Date DS_DT_FRM);

   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="DS_DT_TO"
    **/
   public abstract Date getDsDateTo();
   /**
    * @ejb:interface-method view-type="local"
    **/
   public abstract void setDsDateTo(Date DS_DT_TO);

   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="DS_NMBRNG_TYP"
    **/
   public abstract char getDsNumberingType();
   /**
    * @ejb:interface-method view-type="local"
    **/
   public abstract void setDsNumberingType(char DS_NMBRNG_TYP);

   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="DS_INTL_VL"
    **/
   public abstract String getDsInitialValue();
   /**
    * @ejb:interface-method view-type="local"
    **/
   public abstract void setDsInitialValue(String DS_INTL_VL);
   
   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="DS_AD_CMPNY"
    **/
   public abstract Integer getDsAdCompany();
   /**
    * @ejb:interface-method view-type="local"
    **/
   public abstract void setDsAdCompany(Integer DS_AD_CMPNY);

   // Access methods for relationship fields

   /**
    * @ejb:interface-method view-type="local"
    * @ejb:relation name="documentsequence-documentsequenceassignments"
    *               role-name="documentsequence-has-many-documentsequenceassignments"
    */
   public abstract Collection getAdDocumentSequenceAssignments();
   public abstract void setAdDocumentSequenceAssignments(Collection adDocumentSequenceAssignments);
   
   /**
    * @ejb:interface-method view-type="local"
    * @ejb:relation name="application-documentsequences"
    *               role-name="documentsequence-has-one-application"
    *
    * @jboss:relation related-pk-field="appCode"
    *                 fk-column="AD_APPLICATION"
    */
   public abstract LocalAdApplication getAdApplication();
   public abstract void setAdApplication(LocalAdApplication adApplication);

   // Business methods 

   /**
    * @ejb:interface-method view-type="local"
    **/
   public void addAdDocumentSequenceAssignment(LocalAdDocumentSequenceAssignment adDocumentSequenceAssignment) {

      Debug.print("AdDocumentSequenceBean addAdDocumentSequenceAssignment");
      try {
         Collection adDocumentSequenceAssignments = getAdDocumentSequenceAssignments();
	 adDocumentSequenceAssignments.add(adDocumentSequenceAssignment);
      } catch (Exception ex) {
         throw new EJBException(ex.getMessage());
      }
   }

   /**
    * @ejb:interface-method view-type="local"
    **/
   public void dropAdDocumentSequenceAssignment(LocalAdDocumentSequenceAssignment adDocumentSequenceAssignment) {

      Debug.print("AdDocumentSequenceBean dropAdDocumentSequenceAssignment");
      try {
         Collection adDocumentSequenceAssignments = getAdDocumentSequenceAssignments();
	 adDocumentSequenceAssignments.remove(adDocumentSequenceAssignment);
      } catch (Exception ex) {
         throw new EJBException(ex.getMessage());
      }
   }

   // EntityBean methods

   /**
    * @ejb:create-method view-type="local"
    **/
   public java.lang.Integer ejbCreate (java.lang.Integer DS_CODE, java.lang.String DS_SQNC_NM,
      Date DS_DT_FRM, Date DS_DT_TO, char DS_NMBRNG_TYP,
      String DS_INTL_VL, Integer DS_AD_CMPNY)
      throws CreateException {

      Debug.print("AdDocumentSequenceBean ejbCreate");
      setDsCode(DS_CODE);
      setDsSequenceName(DS_SQNC_NM);
      setDsDateFrom(DS_DT_FRM);
      setDsDateTo(DS_DT_TO);
      setDsNumberingType(DS_NMBRNG_TYP);
      setDsInitialValue(DS_INTL_VL);
      setDsAdCompany(DS_AD_CMPNY);
      
      return null;
   }

   /**
    * @ejb:create-method view-type="local"
    **/
   public java.lang.Integer ejbCreate (java.lang.String DS_SQNC_NM,
      Date DS_DT_FRM, Date DS_DT_TO, char DS_NMBRNG_TYP,
      String DS_INTL_VL, Integer DS_AD_CMPNY)
      throws CreateException {

      Debug.print("AdDocumentSequenceBean ejbCreate");

      setDsSequenceName(DS_SQNC_NM);
      setDsDateFrom(DS_DT_FRM);
      setDsDateTo(DS_DT_TO);
      setDsNumberingType(DS_NMBRNG_TYP);
      setDsInitialValue(DS_INTL_VL);
      setDsAdCompany(DS_AD_CMPNY);
      
      return null;
   }

   public void ejbPostCreate (java.lang.Integer DS_CODE, java.lang.String DS_SQNC_NM,
      Date DS_DT_FRM, Date DS_DT_TO, char DS_NMBRNG_TYP,
      String DS_INTL_VL, Integer DS_AD_CMPNY)
      throws CreateException { }

   public void ejbPostCreate (java.lang.String DS_SQNC_NM,
      Date DS_DT_FRM, Date DS_DT_TO, char DS_NMBRNG_TYP,
      String DS_INTL_VL, Integer DS_AD_CMPNY)
      throws CreateException { }

} // AdDocumentSequenceBean class
