/*
 * com/ejb/ad/LocalAdFormFunctionBean.java
 *
 * Created on June 16, 2003, 01:19 PM
 */

package com.ejb.ad;

import java.util.Collection;

import javax.ejb.CreateException;
import javax.ejb.EJBException;

import com.util.AbstractEntityBean;
import com.util.Debug;

/**
 *
 * @author  Enrico C. Yap
 *
 * @ejb:bean name="AdFormFunctionEJB"
 *           display-name="Form Function Entity"
 *           type="CMP"
 *           cmp-version="2.x"
 *           view-type="local"
 *           local-jndi-name="omega-ejb/AdFormFunctionEJB"
 *           schema="AdFormFunction"
 *           primkey-field="ffCode"
 *
 * @ejb:pk class="java.lang.Integer"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="aduser"
 *                        role-link="aduserlink"
 *
 * @ejb:permission role-name="aduser"
 *
 * @ejb:interface local-class="com.ejb.ad.LocalAdFormFunction"
 *                local-extends="javax.ejb.EJBLocalObject"
 *
 * @ejb:home local-class="com.ejb.ad.LocalAdFormFunctionHome"
 *           local-extends="javax.ejb.EJBLocalHome"
 *
 * @ejb:value-object match="*"
 *             name="AdFormFunction"
 *
 * @jboss:persistence table-name="AD_FRM_FNCTN"
 *
 * @jboss:entity-command name="mysql-get-generated-keys"
 */

public abstract class AdFormFunctionBean extends AbstractEntityBean {
    
    
    // PERSISTENT METHODS
    
    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     * @ejb:pk-field
     *
     * @jboss:column-name name="FF_CODE"
     *
     * @jboss:persistence auto-increment="true"
     **/
    public abstract Integer getFfCode();
    public abstract void setFfCode(Integer FF_CODE); 
    
    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="FF_NM"
     **/
     public abstract String getFfName();
    /**
     * @ejb:interface-method view-type="local"
     **/     
     public abstract void setFfName(String FF_NM);
          
     
    // RELATIONSHIP METHODS    
     
    /**
     * @ejb:interface-method view-type="local"
     * @ejb:relation name="formfunction-formfunctionresponsibilities"
     *               role-name="formfunction-has-many-formfunctionresponsibilities)"
     */
    public abstract Collection getAdFormFunctionResponsibilities();
    public abstract void setAdFormFunctionResponsibilities(Collection adFormFunctionResponsibilities);
       
    
    // BUSINESS METHODS

   
    /**
     * @ejb:interface-method view-type="local"
     **/
    public void addAdFormFunctionResponsibility(LocalAdFormFunctionResponsibility adFormFunctionResponsibility) {

       Debug.print("AdFormFunctionBean addAdFormFunctionResponsibility");
      
       try {
      	
          Collection adFormFunctionResponsibilities = getAdFormFunctionResponsibilities();
	      adFormFunctionResponsibilities.add(adFormFunctionResponsibility);
	     
       } catch (Exception ex) {
      	
          throw new EJBException(ex.getMessage());
         
       }
        
    }

    /**
     * @ejb:interface-method view-type="local"
     **/
    public void dropAdFormFunctionResponsibility(LocalAdFormFunctionResponsibility adFormFunctionResponsibility) {

      Debug.print("AdFormFunctionBean dropAdFormFunctionResponsibility");
      
      try {
      	
         Collection adFormFunctionResponsibilities = getAdFormFunctionResponsibilities();
	     adFormFunctionResponsibilities.remove(adFormFunctionResponsibility);
	     
      } catch (Exception ex) {
      	
         throw new EJBException(ex.getMessage());
         
      }
      
    }
   
    // ENTITY METHODS
    
    /**
     * @ejb:create-method view-type="local"
     **/
    public Integer ejbCreate(Integer FF_CODE, String FF_NM)
        throws CreateException {
           
        Debug.print("AdFormFunctionBean ejbCreate");
        
        setFfCode(FF_CODE);
        setFfName(FF_NM);
        
        return null;
        
    }
    
    /**
     * @ejb:create-method view-type="local"
     **/
    public Integer ejbCreate(String FF_NM)
        throws CreateException {
           
        Debug.print("AdFormFunctionBean ejbCreate");
       
        setFfName(FF_NM);
        
        return null;
        
    }
    
    
    public void ejbPostCreate(Integer FF_CODE, String FF_NM)
        throws CreateException { }
   
    public void ejbPostCreate(String FF_NM)
        throws CreateException { }
        
}

