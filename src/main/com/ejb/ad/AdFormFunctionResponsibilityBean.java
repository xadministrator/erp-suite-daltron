/*
 * com/ejb/ad/LocalAdFormFunctionResponsibilityBean.java
 *
 * Created on June 16, 2003, 02:20 PM
 */

package com.ejb.ad;

import javax.ejb.CreateException;

import com.util.AbstractEntityBean;
import com.util.Debug;

/**
 *
 * @author  Enrico C. Yap
 *
 * @ejb:bean name="AdFormFunctionResponsibilityEJB"
 *           display-name="Form Function Responsibility Entity"
 *           type="CMP"
 *           cmp-version="2.x"
 *           view-type="local"
 *           local-jndi-name="omega-ejb/AdFormFunctionResponsibilityEJB"
 *           schema="AdFormFunctionResponsibility"
 *           primkey-field="frCode"
 *
 * @ejb:pk class="java.lang.Integer"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="aduser"
 *                        role-link="aduserlink"
 *
 * @ejb:permission role-name="aduser"
 *
 * @ejb:interface local-class="com.ejb.ad.LocalAdFormFunctionResponsibility"
 *                local-extends="javax.ejb.EJBLocalObject"
 *
 * @ejb:home local-class="com.ejb.ad.LocalAdFormFunctionResponsibilityHome"
 *           local-extends="javax.ejb.EJBLocalHome"
 *
 * @ejb:finder signature="Collection findByRsCode(java.lang.Integer RS_CODE, java.lang.Integer FR_AD_CMPNY)"
 *             query="SELECT OBJECT(fr) FROM AdResponsibility rs, IN(rs.adFormFunctionResponsibilities) fr WHERE rs.rsCode = ?1 AND fr.frAdCompany = ?2"
 *
 * @jboss:query signature="Collection findByRsCode(java.lang.Integer RS_CODE, java.lang.Integer FR_AD_CMPNY)"
 *             query="SELECT OBJECT(fr) FROM AdResponsibility rs, IN(rs.adFormFunctionResponsibilities) fr WHERE rs.rsCode = ?1 AND fr.frAdCompany = ?2 ORDER BY fr.adFormFunction.ffCode"
 * 
 * @ejb:finder signature="LocalAdFormFunctionResponsibility findByRsCodeAndFfName(java.lang.Integer RS_CODE, java.lang.String FF_NM, java.lang.Integer FR_AD_CMPNY)"
 *             query="SELECT OBJECT(fr) FROM AdFormFunctionResponsibility fr WHERE fr.adResponsibility.rsCode=?1 AND fr.adFormFunction.ffName=?2 AND fr.frAdCompany = ?3"
 * 
 * @ejb:finder signature="LocalAdFormFunctionResponsibility findByRsCodeAndFfCode(java.lang.Integer RS_CODE, java.lang.Integer FF_CODE, java.lang.Integer FR_AD_CMPNY)"
 *             query="SELECT OBJECT(fr) FROM AdFormFunctionResponsibility fr WHERE fr.adResponsibility.rsCode=?1 AND fr.adFormFunction.ffCode=?2 AND fr.frAdCompany = ?3"
 *
 * @ejb:value-object match="*"
 *             name="AdFormFunctionResponsibility"
 *
 * @jboss:persistence table-name="AD_FF_RSPNSBLTY"
 *
 * @jboss:entity-command name="mysql-get-generated-keys"
 */

public abstract class AdFormFunctionResponsibilityBean extends AbstractEntityBean {
    
    
    // PERSISTENT METHODS
    
    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     * @ejb:pk-field
     *
     * @jboss:column-name name="FR_CODE"
     *
     * @jboss:persistence auto-increment="true"
     **/
    public abstract Integer getFrCode();
    public abstract void setFrCode(Integer FR_CODE);    
    
    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="FR_PARAM"
     **/
     public abstract String getFrParameter();
    /**
     * @ejb:interface-method view-type="local"
     **/     
     public abstract void setFrParameter(String FR_PARAM);
     
     /**
      * @ejb:persistent-field
      * @ejb:interface-method view-type="local"
      *
      * @jboss:column-name name="FR_AD_CMPNY"
      **/
      public abstract Integer getFrAdCompany();
     /**
      * @ejb:interface-method view-type="local"
      **/     
      public abstract void setFrAdCompany(Integer FR_AD_CMPNY);
     
     
    // RELATIONSHIP METHODS    
          
    /**
     * @ejb:interface-method view-type="local"
     * @ejb:relation name="formfunction-formfunctionresponsibilities"
     *               role-name="formfunctionresponsibility-has-one-formfunction"
     *
     * @jboss:relation related-pk-field="ffCode"
     *                 fk-column="FR_FF_CODE"
     */
    public abstract LocalAdFormFunction getAdFormFunction();
    public abstract void setAdFormFunction(LocalAdFormFunction adFormFunction);     
    
    /**
     * @ejb:interface-method view-type="local"
     * @ejb:relation name="adresponsibility-formfunctionresponsibilities"
     *               role-name="formfunctionresponsibility-has-one-adresponsibility"
     *               cascade-delete="yes"
     *
     * @jboss:relation related-pk-field="rsCode"
     *                 fk-column="FR_RS_CODE"
     */
    public abstract LocalAdResponsibility getAdResponsibility();
    public abstract void setAdResponsibility(LocalAdResponsibility adResponsibility);         
    
    
    // ENTITY METHODS
    
    /**
     * @ejb:create-method view-type="local"
     **/
    public Integer ejbCreate(Integer FR_CODE, String FR_PARAM,
    	Integer FR_AD_CMPNY)
        throws CreateException {
           
        Debug.print("AdFormFunctionResponsibilityBean ejbCreate");
        
        setFrCode(FR_CODE);
        setFrParameter(FR_PARAM);
        setFrAdCompany(FR_AD_CMPNY);
        
        return null;
        
    }
    
    /**
     * @ejb:create-method view-type="local"
     **/
    public Integer ejbCreate(String FR_PARAM,
    	Integer FR_AD_CMPNY)
        throws CreateException {
           
        Debug.print("AdFormFunctionResponsibilityBean ejbCreate");
       
        setFrParameter(FR_PARAM);
        setFrAdCompany(FR_AD_CMPNY);
        
        return null;
        
    }
    
    
    public void ejbPostCreate(Integer FR_CODE, String FR_PARAM,
    	Integer FR_AD_CMPNY)
        throws CreateException { }
   
    public void ejbPostCreate(String FR_PARAM,
    	Integer FR_AD_CMPNY)
        throws CreateException { }
        
}

