/*
 * com/ejb/ar/LocalAdLogBean.java
 *
 * Created on January 15, 2019, 1:24 AM
 *
 */
 
package com.ejb.ad;

import java.util.Collection;
import java.util.Date;

import javax.ejb.CreateException;
import javax.ejb.EJBException;

import com.util.AbstractEntityBean;
import com.util.Debug;

/**
 *
 * @author  Ruben P. Lamberte
 *
 * @ejb:bean name="AdLogEJB"
 *           display-name="Log Entity"
 *           type="CMP"
 *           cmp-version="2.x"
 *           view-type="local"
 *           local-jndi-name="omega-ejb/AdLogEJB"
 *           schema="AdLog"
 *           primkey-field="logCode"
 *
 * @ejb:pk class="java.lang.Integer"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="aruser"
 *                        role-link="aruserlink"
 *
 * @ejb:permission role-name="aruser"
 *
 * @ejb:interface local-class="com.ejb.ad.LocalAdLog"
 *                local-extends="javax.ejb.EJBLocalObject"
 *
 * @ejb:home local-class="com.ejb.ad.LocalAdLogHome"
 *           local-extends="javax.ejb.EJBLocalHome"
 *
 *
 * @ejb:finder signature="Collection findByLogMdlAndLogMdlKy(java.lang.String LOG_MDL, java.lang.Integer LOG_MDL_KY)"
 *             query="SELECT OBJECT(log) FROM AdLog log WHERE log.logModule = ?1 AND log.logModuleKey = ?2"
 
 * @jboss:query signature="Collection findByLogMdlAndLogMdlKy(java.lang.String LOG_MDL, java.lang.Integer LOG_MDL_KY)"
 *             query="SELECT OBJECT(log) FROM AdLog log WHERE log.logModule = ?1 AND log.logModuleKey = ?2 ORDER BY log.logDate DESC"
 *             
 * @ejb:finder signature="Collection findByLogUsrnm(java.lang.String LOG_USRNM)"
 *             query="SELECT OBJECT(log) FROM AdLog log WHERE log.logUsername = ?1"
 *             
	
 * @ejb:value-object match="*"
 *             name="AdLog"
 *
 * @jboss:persistence table-name="AD_LOG"
 *
 * @jboss:entity-command name="mysql-get-generated-keys"
 */

public abstract class AdLogBean extends AbstractEntityBean {
    
   
    // PERSISTENT METHODS
    
    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     * @ejb:pk-field
     *
     * @jboss:column-name name="LOG_CODE"
     *
     * @jboss:persistence auto-increment="true"
     **/
    public abstract Integer getLogCode();
    public abstract void setLogCode(Integer LOG_CODE);
    
    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="LOG_DT"
     **/
    public abstract Date getLogDate();
     /**
      * @ejb:interface-method view-type="local"
      **/  
    public abstract void setLogDate(Date LOG_DT);
    
    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="LOG_USRNM"
     **/
    public abstract String getLogUsername();
     /**
      * @ejb:interface-method view-type="local"
      **/  
    public abstract void setLogUsername(String LOG_USRNM);
    
    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="LOG_ACTN"
     **/
    public abstract String getLogAction();
     /**
      * @ejb:interface-method view-type="local"
      **/  
    public abstract void setLogAction(String LOG_ACTN);

    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="LOG_DESC"
     **/
    public abstract String getLogDescription();
     /**
      * @ejb:interface-method view-type="local"
      **/  
    public abstract void setLogDescription(String LOG_DESC);

    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="LOG_MDL"
     **/
    public abstract String getLogModule();
     /**
      * @ejb:interface-method view-type="local"
      **/  
    public abstract void setLogModule(String LOG_MDL);
    
    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="LOG_MDL_KY"
     **/
    public abstract Integer getLogModuleKey();
     /**
      * @ejb:interface-method view-type="local"
      **/  
    public abstract void setLogModuleKey(Integer LOG_MDL_KY);
    
    
  
     
    // BUSINESS METHODS
   

  
   
    // ENTITY METHODS
    
    /**
     * @ejb:create-method view-type="local"
     **/
     public Integer ejbCreate(Integer LOG_CODE, 
         Date LOG_DT, String LOG_USRNM, String LOG_ACTN, String LOG_DESC,
         String LOG_MDL, Integer LOG_MDL_KY)
         throws CreateException {
      	
         Debug.print("AdLogBean ejbCreate");
      
         setLogCode(LOG_CODE);
         setLogDate(LOG_DT);
         setLogUsername(LOG_USRNM);
         setLogAction(LOG_ACTN);
         setLogDescription(LOG_DESC);
         setLogModule(LOG_MDL);
         setLogModuleKey(LOG_MDL_KY);
        
         return null;
    }
    
    /**
     * @ejb:create-method view-type="local"
     **/
     public Integer ejbCreate(
		 Date LOG_DT, String LOG_USRNM, String LOG_ACTN, String LOG_DESC,
         String LOG_MDL, Integer LOG_MDL_KY)
         throws CreateException {
      	
    	 Debug.print("AdLogBean ejbCreate");
         
         setLogDate(LOG_DT);
         setLogUsername(LOG_USRNM);
         setLogAction(LOG_ACTN);
         setLogDescription(LOG_DESC);
         setLogModule(LOG_MDL);
         setLogModuleKey(LOG_MDL_KY);
        
         return null;
     }
    
     public void ejbPostCreate(Integer LOG_CODE, 
             Date LOG_DT, String LOG_USRNM, String LOG_ACTN, String LOG_DESC,
             String LOG_MDL, Integer LOG_MDL_KY)      
         throws CreateException { }
      
     public void ejbPostCreate( 
    		 Date LOG_DT, String LOG_USRNM, String LOG_ACTN, String LOG_DESC,
             String LOG_MDL, Integer LOG_MDL_KY)
         throws CreateException { }
}

      
      	
      	