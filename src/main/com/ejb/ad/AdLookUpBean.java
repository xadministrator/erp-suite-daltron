/*
 * com/ejb/ar/LocalAdLookUpBean.java
 *
 * Created on January 9, 2003, 8:39 AM
 */
 
package com.ejb.ad;

import java.util.Collection;

import javax.ejb.CreateException;
import javax.ejb.EJBException;

import com.util.AbstractEntityBean;
import com.util.Debug;

/**
 *
 * @author  Neil Andrew M. Ajero
 *
 * @ejb:bean name="AdLookUpEJB"
 *           display-name="Look Up Entity"
 *           type="CMP"
 *           cmp-version="2.x"
 *           view-type="local"
 *           local-jndi-name="omega-ejb/AdLookUpEJB"
 *           schema="AdLookUp"
 *           primkey-field="luCode"
 *
 * @ejb:pk class="java.lang.Integer"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="aruser"
 *                        role-link="aruserlink"
 *
 * @ejb:permission role-name="aruser"
 *
 * @ejb:interface local-class="com.ejb.ad.LocalAdLookUp"
 *                local-extends="javax.ejb.EJBLocalObject"
 *
 * @ejb:home local-class="com.ejb.ad.LocalAdLookUpHome"
 *           local-extends="javax.ejb.EJBLocalHome"
 *
 * @ejb:finder signature="Collection findLuAll(java.lang.Integer LU_AD_CMPNY)"
 *             query="SELECT OBJECT(lu) FROM AdLookUp lu WHERE lu.luAdCompany = ?1"
 *
 * @ejb:finder signature="LocalAdLookUp findByLookUpName(java.lang.String LU_NM, java.lang.Integer LU_AD_CMPNY)"
 *             query="SELECT OBJECT(lu) FROM AdLookUp lu WHERE lu.luName = ?1 AND lu.luAdCompany = ?2"
 *
 * @ejb:value-object match="*"
 *             name="AdLookUp"
 *
 * @jboss:persistence table-name="AD_LK_UP"
 *
 * @jboss:entity-command name="mysql-get-generated-keys"
 */

public abstract class AdLookUpBean extends AbstractEntityBean {
    
   
    // PERSISTENT METHODS
    
    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     * @ejb:pk-field
     *
     * @jboss:column-name name="LU_CODE"
     *
     * @jboss:persistence auto-increment="true"
     **/
    public abstract Integer getLuCode();
    public abstract void setLuCode(Integer LU_CODE);
    
    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="LU_NM"
     **/
    public abstract String getLuName();
     /**
      * @ejb:interface-method view-type="local"
      **/  
    public abstract void setLuName(String LU_NM);
    
    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="LU_DESC"
     **/
    public abstract String getLuDescription();
     /**
      * @ejb:interface-method view-type="local"
      **/  
    public abstract void setLuDescription(String LU_DESC);
    
    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="LU_AD_CMPNY"
     **/
    public abstract Integer getLuAdCompany();
     /**
      * @ejb:interface-method view-type="local"
      **/  
    public abstract void setLuAdCompany(Integer LU_AD_CMPNY);
    
    
    // RELATIONSHIP METHODS
    
    /**
     * @ejb:interface-method view-type="local"
     * @ejb:relation name="lookup-lookupvalues"
     *               role-name="lookup-has-many-lookupvalues"
     * 
     */
     public abstract Collection getAdLookUpValues();
     public abstract void setAdLookUpValues(Collection adLookUpValues);
     
    // BUSINESS METHODS
     
    /**
     * @ejb:interface-method view-type="local"
     **/
     public void addAdLookUpValue(LocalAdLookUpValue adLookUpValue) {

         Debug.print("AdLookUpBean addAdLookUpValue");
      
         try {
      	
            Collection adLookUpValues = getAdLookUpValues();
	        adLookUpValues.add(adLookUpValue);
	         
         } catch (Exception ex) {
      	
           throw new EJBException(ex.getMessage());
      
         }   
     }

    /**
     * @ejb:interface-method view-type="local"
     **/
     public void dropAdLookUpValue(LocalAdLookUpValue adLookUpValue) {

         Debug.print("AdLookUpBean dropAdLookUpValue");
      
         try {
      	
            Collection adLookUpValues = getAdLookUpValues();
	        adLookUpValues.remove(adLookUpValue);
	      
         } catch (Exception ex) {
      	
           throw new EJBException(ex.getMessage());
         
         }   
     } 
   
    // ENTITY METHODS
    
    /**
     * @ejb:create-method view-type="local"
     **/
     public Integer ejbCreate(Integer LU_CODE, 
         String LU_NM, String LU_DESC, Integer LU_AD_CMPNY)
         throws CreateException {
      	
         Debug.print("AdLookUpBean ejbCreate");
      
         setLuCode(LU_CODE);
         setLuName(LU_NM);
         setLuDescription(LU_DESC);
         setLuAdCompany(LU_AD_CMPNY);
        
         return null;
    }
    
    /**
     * @ejb:create-method view-type="local"
     **/
     public Integer ejbCreate(
         String LU_NM, String LU_DESC, Integer LU_AD_CMPNY)
         throws CreateException {
      	
         Debug.print("AdLookUpBean ejbCreate");
         
         setLuName(LU_NM);
         setLuDescription(LU_DESC);
         setLuAdCompany(LU_AD_CMPNY);
        
         return null;
     }
    
     public void ejbPostCreate(Integer LU_CODE, 
         String LU_NM, String LU_DESC, Integer LU_AD_CMPNY)      
         throws CreateException { }
      
     public void ejbPostCreate( 
         String LU_NM, String LU_DESC, Integer LU_AD_CMPNY)
         throws CreateException { }     
}

      
      	
      	