/*
 * com/ejb/ar/LocalAdLookUpValueBean.java
 *
 * Created on January 9, 2003, 8:39 AM
 */

package com.ejb.ad;

import javax.ejb.CreateException;

import com.util.AbstractEntityBean;
import com.util.Debug;

/**
 *
 * @author  Neil Andrew M. Ajero
 *
 * @ejb:bean name="AdLookUpValueEJB"
 *           display-name="Look Up Value Entity"
 *           type="CMP"
 *           cmp-version="2.x"
 *           view-type="local"
 *           local-jndi-name="omega-ejb/AdLookUpValueEJB"
 *           schema="AdLookUpValue"
 *           primkey-field="lvCode"
 *
 * @ejb:pk class="java.lang.Integer"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="aruser"
 *                        role-link="aruserlink"
 *
 * @ejb:permission role-name="aruser"
 *
 * @ejb:interface local-class="com.ejb.ad.LocalAdLookUpValue"
 *                local-extends="javax.ejb.EJBLocalObject"
 *
 * @ejb:home local-class="com.ejb.ad.LocalAdLookUpValueHome"
 *           local-extends="javax.ejb.EJBLocalHome"
 *
 * @ejb:finder signature="Collection findByLuName(java.lang.String LU_NM, java.lang.Integer LV_AD_CMPNY)"
 *             query="SELECT OBJECT(lv) FROM AdLookUp lu, IN(lu.adLookUpValues) lv WHERE lu.luName = ?1 AND lv.lvAdCompany = ?2"
 *
 * @ejb:finder signature="LocalAdLookUpValue findByLuNameAndLvName(java.lang.String LU_NM, java.lang.String LV_NM, java.lang.Integer LV_AD_CMPNY)"
 *             query="SELECT OBJECT(lv) FROM AdLookUp lu, IN(lu.adLookUpValues) lv WHERE lu.luName=?1 AND lv.lvName=?2 AND lv.lvAdCompany = ?3"
 *
 *@ejb:finder signature="LocalAdLookUpValue findByLuNameAndLvMasterCode(java.lang.String LU_NM, java.lang.Integer MSTR_CD, java.lang.Integer LV_AD_CMPNY)"
 *             query="SELECT OBJECT(lv) FROM AdLookUp lu, IN(lu.adLookUpValues) lv WHERE lu.luName=?1 AND lv.lvMasterCode=?2 AND lv.lvAdCompany = ?3"
 *
 * @ejb:finder signature="LocalAdLookUpValue findByLvCodeAndLuName(java.lang.Integer LV_CODE, java.lang.String LU_NM, java.lang.Integer LV_AD_CMPNY)"
 *             query="SELECT OBJECT(lv) FROM AdLookUp lu, IN(lu.adLookUpValues) lv WHERE lv.lvCode=?1 AND lu.luName=?2 AND lv.lvAdCompany = ?3"
 *
 * @ejb:finder signature="LocalAdLookUpValue findByLvName(java.lang.String LV_NM, java.lang.Integer LV_AD_CMPNY)"
 *             query="SELECT OBJECT(lv) FROM AdLookUpValue lv WHERE lv.lvName = ?1 AND lv.lvAdCompany = ?2"
 *
 * @ejb:finder signature="Collection findByLvCodeAndLvDownloadStatus(java.lang.Integer LV_CODE, char LV_DWNLD_STATUS, java.lang.Integer LV_AD_CMPNY)"
 *             query="SELECT OBJECT(lv) FROM AdLookUpValue lv WHERE lv.lvCode = ?1 AND lv.lvDownloadStatus = ?2 AND lv.lvAdCompany = ?3"
 *
 * @ejb:finder signature="Collection findEnabledLvByLvNewAndUpdated(java.lang.String LU_NM, java.lang.Integer LV_AD_CMPNY, char NEW, char UPDATED, char DOWNLOADED_UPDATED)"
 *             query="SELECT OBJECT(lv) FROM AdLookUp lu, IN(lu.adLookUpValues) lv WHERE lu.luName = ?1 AND lv.lvAdCompany = ?2 AND lv.lvEnable = 1 AND (lv.lvDownloadStatus = ?3 OR lv.lvDownloadStatus = ?4 OR lv.lvDownloadStatus = ?5)"
 *
 * @ejb:value-object match="*"
 *             name="AdLookUpValue"
 *
 * @jboss:persistence table-name="AD_LK_UP_VL"
 *
 * @jboss:entity-command name="mysql-get-generated-keys"
 */

public abstract class AdLookUpValueBean extends AbstractEntityBean {


    // PERSISTENT METHODS

    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     * @ejb:pk-field
     *
     * @jboss:column-name name="LV_CODE"
     *
     * @jboss:persistence auto-increment="true"
     **/
    public abstract Integer getLvCode();
    public abstract void setLvCode(Integer LV_CODE);

    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="LV_NM"
     **/
    public abstract String getLvName();
     /**
      * @ejb:interface-method view-type="local"
      **/
    public abstract void setLvName(String LV_NM);

    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="LV_DESC"
     **/
    public abstract String getLvDescription();
     /**
      * @ejb:interface-method view-type="local"
      **/
    public abstract void setLvDescription(String LV_DESC);

    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="LV_MSTR_CD"
     **/
    public abstract Integer getLvMasterCode();
     /**
      * @ejb:interface-method view-type="local"
      **/
    public abstract void setLvMasterCode(Integer LV_MSTR_CD);

    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="LV_ENBL"
     **/
    public abstract byte getLvEnable();
     /**
      * @ejb:interface-method view-type="local"
      **/
    public abstract void setLvEnable(byte LV_ENBL);

    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="LV_DWNLD_STATUS"
     **/
    public abstract char getLvDownloadStatus();
     /**
      * @ejb:interface-method view-type="local"
      **/
    public abstract void setLvDownloadStatus(char LV_DWNLD_STATUS);

    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="LV_AD_CMPNY"
     **/
    public abstract Integer getLvAdCompany();
     /**
      * @ejb:interface-method view-type="local"
      **/
    public abstract void setLvAdCompany(Integer LV_AD_CMPNY);

    // RELATIONSHIP METHODS

    /**
     * @ejb:interface-method view-type="local"
     * @ejb:relation name="lookup-lookupvalues"
     *               role-name="lookupvalue-has-one-lookup"
     *
     * @jboss:relation related-pk-field="luCode"
     *                 fk-column="AD_LOOK_UP"
     */
     public abstract LocalAdLookUp getAdLookUp();
     public abstract void setAdLookUp(LocalAdLookUp adLookUp);

    // BUSINESS METHODS

    // ENTITY METHODS

    /**
     * @ejb:create-method view-type="local"
     **/
     public Integer ejbCreate(Integer LV_CODE,
         String LV_NM, String LV_DESC, Integer LV_MSTR_CD, byte LV_ENBL,
         char LV_DWNLD_STATUS, Integer LV_AD_CMPNY)
         throws CreateException {

         Debug.print("AdLookUpValueBean ejbCreate");

         setLvCode(LV_CODE);
         setLvName(LV_NM);
         setLvDescription(LV_DESC);
         setLvMasterCode(LV_MSTR_CD);
         setLvEnable(LV_ENBL);
         setLvDownloadStatus(LV_DWNLD_STATUS);
         setLvAdCompany(LV_AD_CMPNY);

         return null;
     }

    /**
     * @ejb:create-method view-type="local"
     **/
     public Integer ejbCreate(
         String LV_NM, String LV_DESC, Integer LV_MSTR_CD, byte LV_ENBL,
         char LV_DWNLD_STATUS, Integer LV_AD_CMPNY)
         throws CreateException {

         Debug.print("AdLookUpValueBean ejbCreate");

         setLvName(LV_NM);
         setLvDescription(LV_DESC);
         setLvMasterCode(LV_MSTR_CD);
         setLvEnable(LV_ENBL);
         setLvDownloadStatus(LV_DWNLD_STATUS);
         setLvAdCompany(LV_AD_CMPNY);

         return null;
     }

     public void ejbPostCreate(Integer LV_CODE,
         String LV_NM, String LV_DESC, Integer LV_MSTR_CD, byte LV_ENBL,
         char LV_DWNLD_STATUS, Integer LV_AD_CMPNY)
         throws CreateException { }

     public void ejbPostCreate(
         String LV_NM, String LV_DESC, Integer LV_MSTR_CD, byte LV_ENBL,
         char LV_DWNLD_STATUS, Integer LV_AD_CMPNY)
         throws CreateException { }
}