/*
 * com/ejb/ad/LocalAdPaymentScheduleBean.java
 *
 * Created on May 19, 2003, 09:43 AM
 */

package com.ejb.ad;

import java.util.Collection;

import javax.ejb.CreateException;
import javax.ejb.EJBException;

import com.util.AbstractEntityBean;
import com.util.Debug;

/**
 *
 * @author  Dennis M. Hilario
 *
 * @ejb:bean name="AdPaymentScheduleEJB"
 *           display-name="Payment Schedule Entity"
 *           type="CMP"
 *           cmp-version="2.x"
 *           view-type="local"
 *           local-jndi-name="omega-ejb/AdPaymentScheduleEJB"
 *           schema="AdPaymentSchedule"
 *           primkey-field="psCode"
 *
 * @ejb:pk class="java.lang.Integer"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="aruser"
 *                        role-link="aruserlink"
 *
 * @ejb:permission role-name="aruser"
 *
 * @ejb:interface local-class="com.ejb.ad.LocalAdPaymentSchedule"
 *                local-extends="javax.ejb.EJBLocalObject"
 *
 * @ejb:home local-class="com.ejb.ad.LocalAdPaymentScheduleHome"
 *           local-extends="javax.ejb.EJBLocalHome"
 *
 * @ejb:finder signature="Collection findByPytCode(java.lang.Integer PYT_CODE, java.lang.Integer PS_AD_CMPNY)"
 *             query="SELECT OBJECT(ps) FROM AdPaymentTerm pyt, IN(pyt.adPaymentSchedules) ps WHERE pyt.pytCode = ?1 AND ps.psAdCompany = ?2"
 *
 * @jboss:query signature="Collection findByPytCode(java.lang.Integer PYT_CODE, java.lang.Integer PS_AD_CMPNY)"
 *             query="SELECT OBJECT(ps) FROM AdPaymentTerm pyt, IN(pyt.adPaymentSchedules) ps WHERE pyt.pytCode = ?1 AND ps.psAdCompany = ?2 ORDER BY ps.psLineNumber"
 *
 * @ejb:value-object match="*"
 *             name="AdPaymentSchedule"
 *
 * @jboss:persistence table-name="AD_PYMNT_SCHDL"
 *
 * @jboss:entity-command name="mysql-get-generated-keys"
 */

public abstract class AdPaymentScheduleBean extends AbstractEntityBean {
    
    
    // PERSISTENT METHODS
    
    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     * @ejb:pk-field
     *
     * @jboss:column-name name="PS_CODE"
     *
     * @jboss:persistence auto-increment="true"
     **/
    public abstract Integer getPsCode();
    public abstract void setPsCode(Integer PS_CODE);
    
    
    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="PS_LN_NMBR"
     **/
     public abstract short getPsLineNumber();
    /**
     * @ejb:interface-method view-type="local"
     **/     
     public abstract void setPsLineNumber(short PS_LN_NMBR);
     
     
     /**
      * @ejb:persistent-field
      * @ejb:interface-method view-type="local"
      *
      * @jboss:column-name name="PS_RLTV_AMNT"
      **/
     public abstract double getPsRelativeAmount();
    /**
     * @ejb:interface-method view-type="local"
     **/     
     public abstract void setPsRelativeAmount(double PS_RLTV_AMNT);
     
     
    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="PS_DUE_DY"
     **/
    public abstract short getPsDueDay();
    /**
     * @ejb:interface-method view-type="local"
     **/    
    public abstract void setPsDueDay(short PS_DUE_DY);
    
    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="PS_AD_CMPNY"
     **/
    public abstract Integer getPsAdCompany();
    /**
     * @ejb:interface-method view-type="local"
     **/    
    public abstract void setPsAdCompany(Integer PS_AD_CMPNY);
         
     
    // RELATIONSHIP METHODS    
     
     /**
      * @ejb:interface-method view-type="local"
      * @ejb:relation name="paymentschedule-discounts"
      *               role-name="paymentschedule-has-many-discounts"
      */
     public abstract Collection getAdDiscounts();
     public abstract void setAdDiscounts(Collection adDiscounts);
    
    
     /**
      * @ejb:interface-method view-type="local"
      * @ejb:relation name="paymentterm-paymentschedules"
      *               role-name="paymentschedule-has-one-paymentterm"
      *               cascade-delete="yes"
      *
      * @jboss:relation related-pk-field="pytCode"
      *                 fk-column="AD_PAYMENT_TERM"
      */
     public abstract LocalAdPaymentTerm getAdPaymentTerm();
     public abstract void setAdPaymentTerm(LocalAdPaymentTerm adPaymentTerm);
    
     
    // BUSINESS METHODS

   
   /**
    * @ejb:interface-method view-type="local"
    **/
   public void addAdDiscount(LocalAdDiscount adDiscount) {

      Debug.print("AdPaymentScheduleBean addAdDiscount");
      
      try {
      	
         Collection adDiscounts = getAdDiscounts();
	     adDiscounts.add(adDiscount);
	     
      } catch (Exception ex) {
      	
         throw new EJBException(ex.getMessage());
         
      }
   }

   /**
    * @ejb:interface-method view-type="local"
    **/
   public void dropAdDiscount(LocalAdDiscount adDiscount) {

      Debug.print("AdPaymentScheduleBean dropAdDiscount");
      
      try {
      	
         Collection adDiscounts = getAdDiscounts();
	     adDiscounts.remove(adDiscount);
	     
      } catch (Exception ex) {
      	
         throw new EJBException(ex.getMessage());
         
      }
   }
   
    // ENTITY METHODS
    
    /**
     * @ejb:create-method view-type="local"
     **/
    public Integer ejbCreate(Integer PS_CODE, short PS_LN_NMBR, double PS_RLTV_AMNT,
        short PS_DUE_DY, Integer PS_AD_CMPNY)
        throws CreateException {
           
        Debug.print("AdPaymentScheduleBean ejbCreate");
        
        setPsCode(PS_CODE);
        setPsLineNumber(PS_LN_NMBR);
        setPsRelativeAmount(PS_RLTV_AMNT);
        setPsDueDay(PS_DUE_DY);
        setPsAdCompany(PS_AD_CMPNY);
        
        return null;
        
    }
    
    /**
     * @ejb:create-method view-type="local"
     **/
    public Integer ejbCreate(short PS_LN_NMBR, double PS_RLTV_AMNT,
        short PS_DUE_DY, Integer PS_AD_CMPNY)
        throws CreateException {
           
        Debug.print("AdPaymentScheduleBean ejbCreate");
       
        setPsLineNumber(PS_LN_NMBR);
        setPsRelativeAmount(PS_RLTV_AMNT);
        setPsDueDay(PS_DUE_DY);
        setPsAdCompany(PS_AD_CMPNY);
        
        return null;
        
    }
    
    
    public void ejbPostCreate(Integer PS_CODE, short PS_LN_NMBR, double PS_RLTV_AMNT,
        short PS_DUE_DY, Integer PS_AD_CMPNY)
        throws CreateException { }
   
    public void ejbPostCreate(short PS_LN_NMBR, double PS_RLTV_AMNT,
        short PS_DUE_DY, Integer PS_AD_CMPNY)
        throws CreateException { }
        
}

