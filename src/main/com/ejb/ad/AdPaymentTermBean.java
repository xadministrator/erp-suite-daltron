/*
 * com/ejb/ad/LocalAdPaymentTermBean.java
 *
 * Created on January 9, 2003, 09:04 AM
 */

package com.ejb.ad;

import java.util.Collection;

import javax.ejb.CreateException;
import javax.ejb.EJBException;

import com.ejb.ar.LocalArSalesOrder;
import com.util.AbstractEntityBean;
import com.util.Debug;

/**
 *
 * @author  Dennis M. Hilario
 *
 * @ejb:bean name="AdPaymentTermEJB"
 *           display-name="Payment Term Entity"
 *           type="CMP"
 *           cmp-version="2.x"
 *           view-type="local"
 *           local-jndi-name="omega-ejb/AdPaymentTermEJB"
 *           schema="AdPaymentTerm"
 *           primkey-field="pytCode"
 *
 * @ejb:pk class="java.lang.Integer"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="aruser"
 *                        role-link="aruserlink"
 *
 * @ejb:permission role-name="aruser"
 *
 * @ejb:interface local-class="com.ejb.ad.LocalAdPaymentTerm"
 *                local-extends="javax.ejb.EJBLocalObject"
 *
 * @ejb:home local-class="com.ejb.ad.LocalAdPaymentTermHome"
 *           local-extends="javax.ejb.EJBLocalHome"
 *
 * @ejb:finder signature="Collection findEnabledPytAll(java.lang.Integer PYT_AD_CMPNY)"
 *             query="SELECT OBJECT(pyt) FROM AdPaymentTerm pyt WHERE pyt.pytEnable = 1 AND pyt.pytAdCompany = ?1"
 *
 * @jboss:query signature="Collection findEnabledPytAll(java.lang.Integer PYT_AD_CMPNY)"
 *             query="SELECT OBJECT(pyt) FROM AdPaymentTerm pyt WHERE pyt.pytEnable = 1 AND pyt.pytAdCompany = ?1 ORDER BY pyt.pytName"
 *
 * @ejb:finder signature="Collection findPytAll(java.lang.Integer PYT_AD_CMPNY)"
 *             query="SELECT OBJECT(pyt) FROM AdPaymentTerm pyt WHERE pyt.pytAdCompany = ?1"
 *
 * @jboss:query signature="Collection findPytAll(java.lang.Integer PYT_AD_CMPNY)"
 *             query="SELECT OBJECT(pyt) FROM AdPaymentTerm pyt WHERE pyt.pytAdCompany = ?1 ORDER BY pyt.pytName"
 *
 * @ejb:finder signature="LocalAdPaymentTerm findByPytName(java.lang.String PYT_NM, java.lang.Integer PYT_AD_CMPNY)"
 *             query="SELECT OBJECT(pyt) FROM AdPaymentTerm pyt WHERE pyt.pytName = ?1 AND pyt.pytAdCompany = ?2"
 *
 * @ejb:value-object match="*"
 *             name="AdPaymentTerm"
 *
 * @jboss:persistence table-name="AD_PYMNT_TRM"
 *
 * @jboss:entity-command name="mysql-get-generated-keys"
 */

public abstract class AdPaymentTermBean extends AbstractEntityBean {
	
	
	// PERSISTENT METHODS
	
	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 * @ejb:pk-field
	 *
	 * @jboss:column-name name="PYT_CODE"
	 *
	 * @jboss:persistence auto-increment="true"
	 **/
	public abstract Integer getPytCode();
	public abstract void setPytCode(Integer PYT_CODE);
	
	
	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="PYT_NM"
	 **/
	public abstract String getPytName();
	/**
	 * @ejb:interface-method view-type="local"
	 **/          
	public abstract void setPytName(String PYT_NM);
	
	
	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="PYT_DESC"
	 **/
	public abstract String getPytDescription();
	/**
	 * @ejb:interface-method view-type="local"
	 **/          
	public abstract void setPytDescription(String PYT_DESC);
	
	
	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="PYT_BS_AMNT"
	 **/
	public abstract double getPytBaseAmount();
	/**
	 * @ejb:interface-method view-type="local"
	 **/         
	public abstract void setPytBaseAmount(double PYT_BS_AMNT);
	
	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="PYT_MNTHLY_INT_RT"
	 **/
	public abstract double getPytMonthlyInterestRate();
	/**
	 * @ejb:interface-method view-type="local"
	 **/         
	public abstract void setPytMonthlyInterestRate(double PYT_MNTHLY_INT_RT);
	
	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="PYT_ENBL"
	 **/
	public abstract byte getPytEnable();
	/**
	 * @ejb:interface-method view-type="local"
	 **/          
	public abstract void setPytEnable(byte PYT_ENBL);
	
	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="PYT_ENBL_RBT"
	 **/
	public abstract byte getPytEnableRebate();
	/**
	 * @ejb:interface-method view-type="local"
	 **/          
	public abstract void setPytEnableRebate(byte PYT_ENBL_RBT);
	
	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="PYT_ENBL_INT"
	 **/
	public abstract byte getPytEnableInterest();
	/**
	 * @ejb:interface-method view-type="local"
	 **/          
	public abstract void setPytEnableInterest(byte PYT_ENBL_INT);
	
	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="PYT_DSCNT_ON_INVC"
	 **/
	public abstract byte getPytDiscountOnInvoice();
	/**
	 * @ejb:interface-method view-type="local"
	 **/          
	public abstract void setPytDiscountOnInvoice(byte PYT_DSCNT_ON_INVC);
	
	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="PYT_DSCNT_DESC"
	 **/
	public abstract String getPytDiscountDescription();
	/**
	 * @ejb:interface-method view-type="local"
	 **/          
	public abstract void setPytDiscountDescription(String PYT_DSCNT_DESC);
	
	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="PYT_SCHDL_BSS"
	 **/
	public abstract String getPytScheduleBasis();
	/**
	 * @ejb:interface-method view-type="local"
	 **/          
	public abstract void setPytScheduleBasis(String PYT_SCHDL_BSS);
	
	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="PYT_AD_CMPNY"
	 **/
	public abstract Integer getPytAdCompany();
	/**
	 * @ejb:interface-method view-type="local"
	 **/          
	public abstract void setPytAdCompany(Integer PYT_AD_CMPNY);
	
	// RELATIONSHIP METHODS    
	
	/**
	 * @ejb:interface-method view-type="local"
	 * @ejb:relation name="paymentterm-paymentschedules"
	 *               role-name="paymentterm-has-many-paymentschedules"
	 */
	public abstract Collection getAdPaymentSchedules();
	public abstract void setAdPaymentSchedules(Collection adPaymentSchedules);
	
	/**
	 * @ejb:interface-method view-type="local"
	 * @ejb:relation name="paymentterm-suppliers"
	 *               role-name="paymentterm-has-many-suppliers"
	 */
	public abstract Collection getApSuppliers();
	public abstract void setApSuppliers(Collection apSuppliers);
	
	/**
	 * @ejb:interface-method view-type="local"
	 * @ejb:relation name="paymentterm-vouchers"
	 *               role-name="paymentterm-has-many-vouchers"
	 */
	public abstract Collection getApVouchers();
	public abstract void setApVouchers(Collection apVouchers);
	
	/**
	 * @ejb:interface-method view-type="local"
	 * @ejb:relation name="paymentterm2-vouchers"
	 *               role-name="paymentterm2-has-many-vouchers"
	 */
	public abstract Collection getApVouchers2();
	public abstract void setApVouchers2(Collection apVouchers);
	
	/**
	 * @ejb:interface-method view-type="local"
	 * @ejb:relation name="paymentterm-checks"
	 *               role-name="paymentterm-has-many-checks"
	 */
	public abstract Collection getApChecks();
	public abstract void setApChecks(Collection apChecks);
	
	
	/**
	 * @ejb:interface-method view-type="local"
	 * @ejb:relation name="paymentterm-recurringvouchers"
	 *               role-name="paymentterm-has-many-recurringvouchers"
	 */
	public abstract Collection getApRecurringVouchers();
	public abstract void setApRecurringVouchers(Collection apRecurringVouchers);
	
	/**
	 * @ejb:interface-method view-type="local"
	 * @ejb:relation name="paymentterm-customers"
	 *               role-name="paymentterm-has-many-customers"
	 */
	public abstract Collection getArCustomers();
	public abstract void setArCustomers(Collection arCustomers);
	
	/**
	 * @ejb:interface-method view-type="local"
	 * @ejb:relation name="paymentterm-invoices"
	 *               role-name="paymentterm-has-many-invoices"
	 */
	public abstract Collection getArInvoices();
	public abstract void setArInvoices(Collection arInvoices);
	
	/**
	 * @ejb:interface-method view-type="local"
	 * @ejb:relation name="paymentterm-purchaseorders"
	 *               role-name="paymentterm-has-many-purchaseorders"
	 */
	public abstract Collection getApPurchaseOrders();
	public abstract void setApPurchaseOrders(Collection apPurchaseOrders);
	
	/**
	 * @ejb:interface-method view-type="local"
	 * @ejb:relation name="paymentterm-pdcs"
	 *               role-name="paymentterm-has-many-pdcs"
	 */
	public abstract Collection getArPdcs();
	public abstract void setArPdcs(Collection arPdcs);	
	
	/**
	 * @ejb:interface-method view-type="local"
	 * @ejb:relation name="paymentterm-salesorder"
	 *               role-name="paymentterm-has-many-salesorder"
	 */
	public abstract Collection getArSalesOrders();
	public abstract void setArSalesOrders(Collection arSalesOrders);	
	

	/**
	 * @ejb:interface-method view-type="local"
	 * @ejb:relation name="paymentterm-joborders"
	 *               role-name="paymentterm-has-many-joborders"
	 */
	public abstract Collection getArJobOrders();
	public abstract void setArJobOrders(Collection arJobOrders);
	
	/**
	 * @ejb:interface-method view-type="local"
	 * @ejb:relation name="chartofaccount-paymentterms"
	 *               role-name="paymentterm-has-one-chartofaccount"
	 * 
	 * @jboss:relation related-pk-field="coaCode"
	 *                 fk-column="GL_CHART_OF_ACCOUNT"
	 */
	public abstract com.ejb.gl.LocalGlChartOfAccount getGlChartOfAccount();
	public abstract void setGlChartOfAccount(com.ejb.gl.LocalGlChartOfAccount glChartOfAccount);
	
	// BUSINESS METHODS
	
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public void addAdPaymentSchedule(LocalAdPaymentSchedule adPaymentSchedule) {
		
		Debug.print("AdPaymentTermBean addAdPaymentSchedule");
		
		try {
			
			Collection adPaymentSchedules = getAdPaymentSchedules();
			adPaymentSchedules.add(adPaymentSchedule);
			
		} catch (Exception ex) {
			
			throw new EJBException(ex.getMessage());
			
		}
	}
	
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public void dropAdPaymentSchedule(LocalAdPaymentSchedule adPaymentSchedule) {
		
		Debug.print("AdPaymentTermBean dropAdPaymentSchedule");
		
		try {
			
			Collection adPaymentSchedules = getAdPaymentSchedules();
			adPaymentSchedules.remove(adPaymentSchedule);
			
		} catch (Exception ex) {
			
			throw new EJBException(ex.getMessage());
			
		}
	}
	
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public void addApSupplier(com.ejb.ap.LocalApSupplier apSupplier) {
		
		Debug.print("AdPaymentTermBean addApSupplier");
		
		try {
			
			Collection apSuppliers = getApSuppliers();
			apSuppliers.add(apSupplier);
			
		} catch (Exception ex) {
			
			throw new EJBException(ex.getMessage());
			
		}   
	}
	
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public void dropApSupplier(com.ejb.ap.LocalApSupplier apSupplier) {
		
		Debug.print("AdPaymentTermBean dropApSupplier");
		
		try {
			
			Collection apSuppliers = getApSuppliers();
			apSuppliers.remove(apSupplier);
			
		} catch (Exception ex) {
			
			throw new EJBException(ex.getMessage());
			
		}   
	}
	
	
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public void addApVoucher(com.ejb.ap.LocalApVoucher apVoucher) {
		
		Debug.print("AdPaymentTermBean addApVoucher");
		
		try {
			
			Collection apVouchers = getApVouchers();
			apVouchers.add(apVoucher);
			
		} catch (Exception ex) {
			
			throw new EJBException(ex.getMessage());
			
		}   
	}
	
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public void dropApVoucher(com.ejb.ap.LocalApVoucher apVoucher) {
		
		Debug.print("AdPaymentTermBean dropApVoucher");
		
		try {
			
			Collection apVouchers = getApVouchers();
			apVouchers.remove(apVoucher);
			
		} catch (Exception ex) {
			
			throw new EJBException(ex.getMessage());
			
		}   
	}
	
	
	
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public void addApCheck(com.ejb.ap.LocalApCheck apCheck) {
		
		Debug.print("AdPaymentTermBean addApCheck");
		
		try {
			
			Collection apChecks = getApChecks();
			apChecks.add(apCheck);
			
		} catch (Exception ex) {
			
			throw new EJBException(ex.getMessage());
			
		}   
	}
	
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public void dropApCheck(com.ejb.ap.LocalApCheck apCheck) {
		
		Debug.print("AdPaymentTermBean dropApCheck");
		
		try {
			
			Collection apChecks = getApChecks();
			apChecks.remove(apCheck);
			
		} catch (Exception ex) {
			
			throw new EJBException(ex.getMessage());
			
		}   
	}
	
	
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public void addApRecurringVoucher(com.ejb.ap.LocalApRecurringVoucher apRecurringVoucher) {
		
		Debug.print("AdPaymentTermBean addApRecurringVoucher");
		
		try {
			
			Collection apRecurringVouchers = getApRecurringVouchers();
			apRecurringVouchers.add(apRecurringVoucher);
			
		} catch (Exception ex) {
			
			throw new EJBException(ex.getMessage());
			
		}   
	}
	
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public void dropApRecurringVoucher(com.ejb.ap.LocalApRecurringVoucher apRecurringVoucher) {
		
		Debug.print("AdPaymentTermBean dropApRecurringVoucher");
		
		try {
			
			Collection apRecurringVouchers = getApRecurringVouchers();
			apRecurringVouchers.remove(apRecurringVoucher);
			
		} catch (Exception ex) {
			
			throw new EJBException(ex.getMessage());
			
		}   
	}  
	
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public void addArCustomer(com.ejb.ar.LocalArCustomer arCustomer) {
		
		Debug.print("AdPaymentTermBean addArCustomer");
		
		try {
			
			Collection arCustomers = getArCustomers();
			arCustomers.add(arCustomer);
			
		} catch (Exception ex) {
			
			throw new EJBException(ex.getMessage());
			
		}   
	}
	
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public void dropArCustomer(com.ejb.ar.LocalArCustomer arCustomer) {
		
		Debug.print("AdPaymentTermBean dropArCustomer");
		
		try {
			
			Collection arCustomers = getArCustomers();
			arCustomers.remove(arCustomer);
			
		} catch (Exception ex) {
			
			throw new EJBException(ex.getMessage());
			
		}   
	}
	
	
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public void addArInvoice(com.ejb.ar.LocalArInvoice arInvoice) {
		
		Debug.print("AdPaymentTermBean addArInvoice");
		
		try {
			
			Collection arInvoices = getArInvoices();
			arInvoices.add(arInvoice);
			
		} catch (Exception ex) {
			
			throw new EJBException(ex.getMessage());
			
		}   
	}
	
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public void dropArInvoice(com.ejb.ar.LocalArInvoice arInvoice) {
		
		Debug.print("AdPaymentTermBean dropArInvoice");
		
		try {
			
			Collection arInvoices = getArInvoices();
			arInvoices.remove(arInvoice);
			
		} catch (Exception ex) {
			
			throw new EJBException(ex.getMessage());
			
		}   
	}
	
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public void addApPurchaseOrder(com.ejb.ap.LocalApPurchaseOrder apPurchaseOrder) {
		
		Debug.print("AdPaymentTermBean addApPurchaseOrder");
		
		try {
			
			Collection apPurchaseOrders = getApPurchaseOrders();
			apPurchaseOrders.add(apPurchaseOrder);
			
		} catch (Exception ex) {
			
			throw new EJBException(ex.getMessage());
			
		}   
	}
	
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public void dropApPurchaseOrder(com.ejb.ap.LocalApPurchaseOrder apPurchaseOrder) {
		
		Debug.print("AdPaymentTermBean dropApPurchaseOrder");
		
		try {
			
			Collection apPurchaseOrders = getApPurchaseOrders();
			apPurchaseOrders.remove(apPurchaseOrder);
			
		} catch (Exception ex) {
			
			throw new EJBException(ex.getMessage());
			
		}   
	}
	
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public void addArPdc(com.ejb.ar.LocalArPdc arPdc) {
		
		Debug.print("AdPaymentTermBean addArPdc");
		
		try {
			
			Collection arPdcs = getArPdcs();
			arPdcs.add(arPdc);
			
		} catch (Exception ex) {
			
			throw new EJBException(ex.getMessage());
			
		}   
	}
	
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public void dropArPdc(com.ejb.ar.LocalArPdc arPdc) {
		
		Debug.print("AdPaymentTermBean dropArPdc");
		
		try {
			
			Collection arPdcs = getArPdcs();
			arPdcs.remove(arPdc);
			
		} catch (Exception ex) {
			
			throw new EJBException(ex.getMessage());
			
		}   
	}    	
	
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public void addArSalesOrder(LocalArSalesOrder arSalesOrder) {
		
		Debug.print("AdPaymentTermBean addArSalesOrder");
		
		try {
			
			Collection arSalesOrders = getArSalesOrders();
			arSalesOrders.add(arSalesOrder);
			
		} catch (Exception ex) {
			
			throw new EJBException(ex.getMessage());
			
		}   
	}
	
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public void dropArSalesOrder(LocalArSalesOrder arSalesOrder) {
		
		Debug.print("AdPaymentTermBean dropArSalesOrder");
		
		try {
			
			Collection arSalesOrders = getArSalesOrders();
			arSalesOrders.remove(arSalesOrder);
			
		} catch (Exception ex) {
			
			throw new EJBException(ex.getMessage());
			
		}   
	}                
	
	
	// ENTITY METHODS
	
	/**
	 * @ejb:create-method view-type="local"
	 **/
	public Integer ejbCreate(Integer PYT_CODE, String PYT_NM, String PYT_DESC,
			double PYT_BS_AMNT, double PYT_MNTHLY_INT_RT, byte PYT_ENBL, byte PYT_ENBL_RBT, byte PYT_ENBL_INT, byte PYT_DSCNT_ON_INVC, String PYT_DSCNT_DESC, 
			String PYT_SCHDL_BSS, Integer PYT_AD_CMPNY)
	throws CreateException {
		
		Debug.print("AdPaymentTermBean ejbCreate");
		
		setPytCode(PYT_CODE);
		setPytName(PYT_NM);
		setPytDescription(PYT_DESC);
		setPytBaseAmount(PYT_BS_AMNT); 
		setPytMonthlyInterestRate(PYT_MNTHLY_INT_RT);
		setPytEnable(PYT_ENBL);
		setPytEnableRebate(PYT_ENBL_RBT);
		setPytEnableInterest(PYT_ENBL_INT);
		setPytDiscountOnInvoice(PYT_DSCNT_ON_INVC);
		setPytDiscountDescription(PYT_DSCNT_DESC);
		setPytScheduleBasis(PYT_SCHDL_BSS);
		setPytAdCompany(PYT_AD_CMPNY);
		
		return null;
		
	}
	
	/**
	 * @ejb:create-method view-type="local"
	 **/
	public Integer ejbCreate(String PYT_NM, String PYT_DESC,
			double PYT_BS_AMNT, double PYT_MNTHLY_INT_RT, byte PYT_ENBL, byte PYT_ENBL_RBT, byte PYT_ENBL_INT, byte PYT_DSCNT_ON_INVC, String PYT_DSCNT_DESC, 
			String PYT_SCHDL_BSS, Integer PYT_AD_CMPNY)
	throws CreateException {
		
		Debug.print("AdPaymentTermBean ejbCreate");
		
		setPytName(PYT_NM);
		setPytDescription(PYT_DESC);
		setPytBaseAmount(PYT_BS_AMNT); 
		setPytMonthlyInterestRate(PYT_MNTHLY_INT_RT);
		setPytEnable(PYT_ENBL);
		setPytEnableRebate(PYT_ENBL_RBT);
		setPytEnableInterest(PYT_ENBL_INT);
		setPytDiscountOnInvoice(PYT_DSCNT_ON_INVC);
		setPytDiscountDescription(PYT_DSCNT_DESC);
		setPytScheduleBasis(PYT_SCHDL_BSS);
		setPytAdCompany(PYT_AD_CMPNY);
		
		return null;
		
	}
	
	
	public void ejbPostCreate(Integer PYT_CODE, String PYT_NM, String PYT_DESC,
			double PYT_BS_AMNT, double PYT_MNTHLY_INT_RT, byte PYT_ENBL, byte PYT_ENBL_RBT, byte PYT_ENBL_INT, byte PYT_DSCNT_ON_INVC, String PYT_DSCNT_DESC, 
			String PYT_SCHDL_BSS, Integer PYT_AD_CMPNY)
	throws CreateException { }
	
	public void ejbPostCreate(String PYT_NM, String PYT_DESC,
			double PYT_BS_AMNT, double PYT_MNTHLY_INT_RT, byte PYT_ENBL, byte PYT_ENBL_RBT, byte PYT_ENBL_INT, byte PYT_DSCNT_ON_INVC, String PYT_DSCNT_DESC, 
			String PYT_SCHDL_BSS, Integer PYT_AD_CMPNY)
	throws CreateException { }
	
}

