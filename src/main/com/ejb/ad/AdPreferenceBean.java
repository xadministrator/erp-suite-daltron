/*
 * com/ejb/ar/LocalAdPreferenceBean.java
 *
 * Created on June 12, 2003, 1:54 PM
 */

package com.ejb.ad;

import javax.ejb.CreateException;

import com.ejb.ar.LocalArWithholdingTaxCode;
import com.util.AbstractEntityBean;
import com.util.Debug;

/**
 *
 * @author  Neil Andrew M. Ajero
 *
 * @ejb:bean name="AdPreferenceEJB"
 *           display-name="Preference Entity"
 *           type="CMP"
 *           cmp-version="2.x"
 *           view-type="local"
 *           local-jndi-name="omega-ejb/AdPreferenceEJB"
 *           schema="AdPreference"
 *           primkey-field="prfCode"
 *
 * @ejb:pk class="java.lang.Integer"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="aduser"
 *                        role-link="aduserlink"
 *
 * @ejb:permission role-name="aduser"
 *
 * @ejb:interface local-class="com.ejb.ad.LocalAdPreference"
 *                local-extends="javax.ejb.EJBLocalObject"
 *
 * @ejb:home local-class="com.ejb.ad.LocalAdPreferenceHome"
 *           local-extends="javax.ejb.EJBLocalHome"
 *
 * @ejb:finder signature="LocalAdPreference findByPrfAdCompany(java.lang.Integer PRF_AD_CMPNY)"
 *             query="SELECT OBJECT(prf) FROM AdPreference prf WHERE prf.prfAdCompany = ?1"
 *
 * @ejb:value-object match="*"
 *             name="AdPreference"
 *
 * @jboss:persistence table-name="AD_PRFRNC"
 *
 * @jboss:entity-command name="mysql-get-generated-keys"
 *
 */

public abstract class AdPreferenceBean extends AbstractEntityBean {


   // Access methods for persistent fields

   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    * @ejb:pk-field
    *
    * @jboss:column-name name="PRF_CODE"
    *
    * @jboss:persistence auto-increment="true"
    **/
   public abstract java.lang.Integer getPrfCode();
   public abstract void setPrfCode(java.lang.Integer PRF_CODE);

   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="PRF_ALLW_SSPNS_PSTNG"
    **/
   public abstract byte getPrfAllowSuspensePosting();
   /**
	* @ejb:interface-method view-type="local"
	**/
   public abstract void setPrfAllowSuspensePosting(byte PRF_ALLW_SSPNS_PSTNG);

   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="PRF_GL_JRNL_LN_NMBR"
    **/
   public abstract short getPrfGlJournalLineNumber();
   /**
	* @ejb:interface-method view-type="local"
	**/
   public abstract void setPrfGlJournalLineNumber(short PRF_GL_JRNL_LN_NMBR);


   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="PRF_AP_JRNL_LN_NMBR"
    **/
   public abstract short getPrfApJournalLineNumber();
   /**
	* @ejb:interface-method view-type="local"
	**/
   public abstract void setPrfApJournalLineNumber(short PRF_AP_JRNL_LN_NMBR);


   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="PRF_AR_INVC_LN_NMBR"
    **/
   public abstract short getPrfArInvoiceLineNumber();
   /**
	* @ejb:interface-method view-type="local"
	**/
   public abstract void setPrfArInvoiceLineNumber(short PRF_AR_INVC_LN_NMBR);


   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="PRF_AP_W_TX_RLZTN"
    **/
   public abstract String getPrfApWTaxRealization();
   /**
	* @ejb:interface-method view-type="local"
	**/
   public abstract void setPrfApWTaxRealization(String PRF_AP_W_TX_RLZTN);

   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="PRF_AR_W_TX_RLZTN"
    **/
   public abstract String getPrfArWTaxRealization();
   /**
	* @ejb:interface-method view-type="local"
	**/
   public abstract void setPrfArWTaxRealization(String PRF_AR_W_TX_RLZTN);

   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="PRF_ENBL_GL_JRNL_BTCH"
    **/
   public abstract byte getPrfEnableGlJournalBatch();
   /**
	* @ejb:interface-method view-type="local"
	**/
   public abstract void setPrfEnableGlJournalBatch(byte PRF_ENBL_GL_JRNL_BTCH);

   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="PRF_ENBL_GL_RCMPT_COA_BLNC"
    **/
   public abstract byte getPrfEnableGlRecomputeCoaBalance();
   /**
	* @ejb:interface-method view-type="local"
	**/
   public abstract void setPrfEnableGlRecomputeCoaBalance(byte PRF_ENBL_GL_RCMPT_COA_BLNC);

   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="PRF_ENBL_AP_VCHR_BTCH"
    **/
   public abstract byte getPrfEnableApVoucherBatch();
   /**
	* @ejb:interface-method view-type="local"
	**/
   public abstract void setPrfEnableApVoucherBatch(byte PRF_ENBL_AP_VCHR_BTCH);


   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="PRF_ENBL_AP_PO_BTCH"
    **/
   public abstract byte getPrfEnableApPOBatch();
   /**
	* @ejb:interface-method view-type="local"
	**/
   public abstract void setPrfEnableApPOBatch(byte PRF_ENBL_AP_PO_BTCH);

   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="PRF_ENBL_AP_CHCK_BTCH"
    **/
   public abstract byte getPrfEnableApCheckBatch();
   /**
	* @ejb:interface-method view-type="local"
	**/
   public abstract void setPrfEnableApCheckBatch(byte PRF_ENBL_AP_CHCK_BTCH);


   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="PRF_ENBL_AR_INVC_BTCH"
    **/
   public abstract byte getPrfEnableArInvoiceBatch();
   /**
	* @ejb:interface-method view-type="local"
	**/
   public abstract void setPrfEnableArInvoiceBatch(byte PRF_ENBL_AR_INVC_BTCH);

   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="PRF_ENBL_AR_INVC_INT_GNRTN"
    **/
   public abstract byte getPrfEnableArInvoiceInterestGeneration();
   /**
	* @ejb:interface-method view-type="local"
	**/
   public abstract void setPrfEnableArInvoiceInterestGeneration(byte PRF_ENBL_AR_INVC_INT_GNRTN);


   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="PRF_ENBL_AR_RCPT_BTCH"
    **/
   public abstract byte getPrfEnableArReceiptBatch();
   /**
	* @ejb:interface-method view-type="local"
	**/
   public abstract void setPrfEnableArReceiptBatch(byte PRF_ENBL_AR_RCPT_BTCH);

   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="PRF_ENBL_AR_MISC_RCPT_BTCH"
    **/
   public abstract byte getPrfEnableArMiscReceiptBatch();
   /**
	* @ejb:interface-method view-type="local"
	**/
   public abstract void setPrfEnableArMiscReceiptBatch(byte PRF_ENBL_AR_MISC_RCPT_BTCH);

   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="PRF_AP_GL_PSTNG_TYP"
    **/
   public abstract String getPrfApGlPostingType();
   /**
	* @ejb:interface-method view-type="local"
	**/
   public abstract void setPrfApGlPostingType(String PRF_AP_GL_PSTNG_TYP);


   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="PRF_AR_GL_PSTNG_TYP"
    **/
   public abstract String getPrfArGlPostingType();
   /**
	* @ejb:interface-method view-type="local"
	**/
   public abstract void setPrfArGlPostingType(String PRF_AR_GL_PSTNG_TYP);

   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="PRF_CM_GL_PSTNG_TYP"
    **/
   public abstract String getPrfCmGlPostingType();
   /**
	* @ejb:interface-method view-type="local"
	**/
   public abstract void setPrfCmGlPostingType(String PRF_CM_GL_PSTNG_TYP);

   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="PRF_CM_USE_BNK_FRM"
    **/
   public abstract byte getPrfCmUseBankForm();
   /**
	* @ejb:interface-method view-type="local"
	**/
   public abstract void setPrfCmUseBankForm(byte PRF_CM_USE_BNK_FRM);

   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="PRF_INV_INVNTRY_LN_NMBR"
    **/
   public abstract short getPrfInvInventoryLineNumber();
   /**
	* @ejb:interface-method view-type="local"
	**/
   public abstract void setPrfInvInventoryLineNumber(short PRF_INV_INVNTRY_LN_NMBR);


   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="PRF_INV_QTY_PRCSN_UNT"
    **/
   public abstract short getPrfInvQuantityPrecisionUnit();
   /**
	* @ejb:interface-method view-type="local"
	**/
   public abstract void setPrfInvQuantityPrecisionUnit(short PRF_INV_QTY_PRCSN_UNT);


   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="PRF_INV_CST_PRCSN_UNT"
    **/
   public abstract short getPrfInvCostPrecisionUnit();
   /**
	* @ejb:interface-method view-type="local"
	**/
   public abstract void setPrfInvCostPrecisionUnit(short PRF_INV_CST_PRCSN_UNT);

   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="PRF_INV_GL_PSTNG_TYP"
    **/
   public abstract String getPrfInvGlPostingType();
   /**
	* @ejb:interface-method view-type="local"
	**/
   public abstract void setPrfInvGlPostingType(String PRF_INV_GL_PSTNG_TYP);

   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="PRF_GL_PSTNG_TYP"
    **/
   public abstract String getPrfGlPostingType();
   /**
	* @ejb:interface-method view-type="local"
	**/
   public abstract void setPrfGlPostingType(String PRF_GL_PSTNG_TYP);

   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="PRF_INV_ENBL_SHFT"
    **/
   public abstract byte getPrfInvEnableShift();
   /**
	* @ejb:interface-method view-type="local"
	**/
   public abstract void setPrfInvEnableShift(byte PRF_INV_ENBL_SHFT);

   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="PRF_ENBL_INV_BUA_BTCH"
    **/
   public abstract byte getPrfEnableInvBUABatch();
   /**
	* @ejb:interface-method view-type="local"
	**/
   public abstract void setPrfEnableInvBUABatch(byte PRF_ENBL_INV_BUA_BTCH);


   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="PRF_AP_FND_CHCK_DFLT_TYP"
    **/
   public abstract String getPrfApFindCheckDefaultType();
   /**
	* @ejb:interface-method view-type="local"
	**/
   public abstract void setPrfApFindCheckDefaultType(String PRF_AP_FND_CHCK_DFLT_TYP);


   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="PRF_AR_FND_RCPT_DFLT_TYP"
    **/
   public abstract String getPrfArFindReceiptDefaultType();
   /**
	* @ejb:interface-method view-type="local"
	**/
   public abstract void setPrfArFindReceiptDefaultType(String PRF_AR_FND_RCPT_DFLT_TYP);

   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="PRF_AP_DFLT_CHCKR"
    **/
   public abstract String getPrfApDefaultChecker();
   /**
	* @ejb:interface-method view-type="local"
	**/
   public abstract void setPrfApDefaultChecker(String PRF_AP_DFLT_CHCKR);

   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="PRF_AP_DFLT_APPRVR"
    **/
   public abstract String getPrfApDefaultApprover();
   /**
	* @ejb:interface-method view-type="local"
	**/
   public abstract void setPrfApDefaultApprover(String PRF_AP_DFLT_APPRVR);

   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="PRF_AP_US_ACCRD_VAT"
    **/
   public abstract byte getPrfApUseAccruedVat();
   /**
	* @ejb:interface-method view-type="local"
	**/
   public abstract void setPrfApUseAccruedVat(byte PRF_AP_US_ACCRD_VAT);

   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="PRF_AP_DFLT_CHCK_DT"
    **/
   public abstract String getPrfApDefaultCheckDate();
   /**
	* @ejb:interface-method view-type="local"
	**/
   public abstract void setPrfApDefaultCheckDate(String PRF_AP_DFLT_CHCK_DT);

   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="PRF_AP_GL_COA_ACCRD_VAT_ACCNT"
    **/
   public abstract Integer getPrfApGlCoaAccruedVatAccount();
   /**
	* @ejb:interface-method view-type="local"
	**/
   public abstract void setPrfApGlCoaAccruedVatAccount(Integer PRF_AP_GL_COA_ACCRD_VAT_ACCNT);

   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="PRF_AP_GL_COA_PTTY_CSH_ACCNT"
    **/
   public abstract Integer getPrfApGlCoaPettyCashAccount();
   /**
	* @ejb:interface-method view-type="local"
	**/
   public abstract void setPrfApGlCoaPettyCashAccount(Integer PRF_AP_GL_COA_PTTY_CSH_ACCNT);

   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="PRF_AP_USE_SPPLR_PLLDWN"
    **/
   public abstract byte getPrfApUseSupplierPulldown();
   /**
	* @ejb:interface-method view-type="local"
	**/
   public abstract void setPrfApUseSupplierPulldown(byte PRF_AP_USE_SPPLR_PLLDWN);

   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="PRF_AR_USE_CSTMR_PLLDWN"
    **/
   public abstract byte getPrfArUseCustomerPulldown();
   /**
	* @ejb:interface-method view-type="local"
	**/
   public abstract void setPrfArUseCustomerPulldown(byte PRF_AR_USE_CSTMR_PLLDWN);

   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="PRF_AP_AT_GNRT_SPPLR_CODE"
    **/
   public abstract byte getPrfApAutoGenerateSupplierCode();
   /**
	* @ejb:interface-method view-type="local"
	**/
   public abstract void setPrfApAutoGenerateSupplierCode(byte PRF_AP_AT_GNRT_SPPLR_CODE);

   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="PRF_AP_NXT_SPPLR_CODE"
    **/
   public abstract String getPrfApNextSupplierCode();
   /**
	* @ejb:interface-method view-type="local"
	**/
   public abstract void setPrfApNextSupplierCode(String PRF_AP_NXT_SPPLR_CODE);

   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="PRF_AR_AT_GNRT_CSTMR_CODE"
    **/
   public abstract byte getPrfArAutoGenerateCustomerCode();
   /**
	* @ejb:interface-method view-type="local"
	**/
   public abstract void setPrfArAutoGenerateCustomerCode(byte PRF_AR_AT_GNRT_CSTMR_CODE);

   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="PRF_AR_NXT_CSTMR_CODE"
    **/
   public abstract String getPrfArNextCustomerCode();
   /**
	* @ejb:interface-method view-type="local"
	**/
   public abstract void setPrfArNextCustomerCode(String PRF_AR_NXT_CSTMR_CODE);

   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="PRF_AP_RFRNC_NMBR_VLDTN"
    **/
   public abstract byte getPrfApReferenceNumberValidation();
   /**
	* @ejb:interface-method view-type="local"
	**/
   public abstract void setPrfApReferenceNumberValidation(byte PRF_AP_RFRNC_NMBR_VLDTN);

   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="PRF_INV_ENBL_POS_INTGRTN"
    **/
   public abstract byte getPrfInvEnablePosIntegration();
   /**
	* @ejb:interface-method view-type="local"
	**/
   public abstract void setPrfInvEnablePosIntegration(byte PRF_INV_ENBL_POS_INTGRTN);

   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="PRF_INV_ENBL_POS_AUTO_POST_UP"
    **/
   public abstract byte getPrfInvEnablePosAutoPostUpload();
   /**
	* @ejb:interface-method view-type="local"
	**/
   public abstract void setPrfInvEnablePosAutoPostUpload(byte PRF_INV_ENBL_POS_AUTO_POST_UP);

   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="PRF_INV_POS_ADJSTMNT_ACCNT"
    **/
   public abstract Integer getPrfInvPosAdjustmentAccount();
   /**
	* @ejb:interface-method view-type="local"
	**/
   public abstract void setPrfInvPosAdjustmentAccount(Integer PRF_INV_POS_ADJSTMNT_ACCNT);

   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="PRF_INV_GNRL_ADJSTMNT_ACCNT"
    **/
   public abstract Integer getPrfInvGeneralAdjustmentAccount();
   /**
	* @ejb:interface-method view-type="local"
	**/
   public abstract void setPrfInvGeneralAdjustmentAccount(Integer PRF_INV_GNRL_ADJSTMNT_ACCNT);

   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="PRF_INV_ISSNC_ADJSTMNT_ACCNT"
    **/
   public abstract Integer getPrfInvIssuanceAdjustmentAccount();
   /**
	* @ejb:interface-method view-type="local"
	**/
   public abstract void setPrfInvIssuanceAdjustmentAccount(Integer PRF_INV_ISSNC_ADJSTMNT_ACCNT);

   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="PRF_INV_PRDCTN_ADJSTMNT_ACCNT"
    **/
   public abstract Integer getPrfInvProductionAdjustmentAccount();
   /**
	* @ejb:interface-method view-type="local"
	**/
   public abstract void setPrfInvProductionAdjustmentAccount(Integer PRF_INV_PRDCTN_ADJSTMNT_ACCNT);

   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="PRF_INV_WSTG_ADJSTMNT_ACCNT"
    **/
   public abstract Integer getPrfInvWastageAdjustmentAccount();
   /**
	* @ejb:interface-method view-type="local"
	**/
   public abstract void setPrfInvWastageAdjustmentAccount(Integer PRF_INV_WSTG_ADJSTMNT_ACCNT);

   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="PRF_AP_CHK_VCHR_DT_SRC"
    **/
   public abstract String getPrfApCheckVoucherDataSource();
   /**
	* @ejb:interface-method view-type="local"
	**/
   public abstract void setPrfApCheckVoucherDataSource(String PRF_AP_CHK_VCHR_DT_SRC);

   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    ** @jboss:column-name name="PRF_MISC_POS_DSCNT_ACCNT"
    **/
   public abstract Integer getPrfMiscPosDiscountAccount();
   /**
	* @ejb:interface-method view-type="local"
	**/
   public abstract void setPrfMiscPosDiscountAccount(Integer PRF_MISC_POS_DSCNT_ACCNT);

   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="PRF_MISC_POS_GFT_CRTFCT_ACCNT"
    **/
   public abstract Integer getPrfMiscPosGiftCertificateAccount();
   /**
	* @ejb:interface-method view-type="local"
	**/
   public abstract void setPrfMiscPosGiftCertificateAccount(Integer PRF_MISC_POS_GFT_CRTFCT_ACCNT);

   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="PRF_MISC_POS_SRVC_CHRG_ACCNT"
    **/
   public abstract Integer getPrfMiscPosServiceChargeAccount();
   /**
	* @ejb:interface-method view-type="local"
	**/
   public abstract void setPrfMiscPosServiceChargeAccount(Integer PRF_MISC_POS_SRVC_CHRG_ACCNT);

   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="PRF_MISC_POS_DN_IN_CHRG_ACCNT"
    **/
   public abstract Integer getPrfMiscPosDineInChargeAccount();
   /**
	* @ejb:interface-method view-type="local"
	**/
   public abstract void setPrfMiscPosDineInChargeAccount(Integer PRF_MISC_POS_DN_IN_CHRG_ACCNT);

   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="PRF_AP_DFLT_PR_TX"
    **/
   public abstract String getPrfApDefaultPrTax();
   /**
	* @ejb:interface-method view-type="local"
	**/
   public abstract void setPrfApDefaultPrTax(String PRF_AP_DFLT_PR_TX);

   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="PRF_AP_DFLT_PR_CRRNCY"
    **/
   public abstract String getPrfApDefaultPrCurrency();
   /**
	* @ejb:interface-method view-type="local"
	**/
   public abstract void setPrfApDefaultPrCurrency(String PRF_AP_DFLT_PR_CRRNCY);

   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="PRF_AR_GL_COA_CSTMR_DPST_ACCNT"
    **/
   public abstract Integer getPrfArGlCoaCustomerDepositAccount();
   /**
	* @ejb:interface-method view-type="local"
	**/
   public abstract void setPrfArGlCoaCustomerDepositAccount(Integer PRF_AR_GL_COA_CSTMR_DPST_ACCNT);

   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="PRF_AR_SLS_INVC_DT_SRC"
    **/
   public abstract String getPrfArSalesInvoiceDataSource();
   /**
	* @ejb:interface-method view-type="local"
	**/
   public abstract void setPrfArSalesInvoiceDataSource(String PRF_AR_SLS_INVC_DT_SRC);

   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="PRF_INV_NXT_CSTDN_NMBR1"
    **/
   public abstract String getPrfInvNextCustodianNumber1();
   /**
	* @ejb:interface-method view-type="local"
	**/
   public abstract void setPrfInvNextCustodianNumber1(String PRF_INV_NXT_CSTDN_NMBR1);

   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="PRF_INV_NXT_CSTDN_NMBR2"
    **/
   public abstract String getPrfInvNextCustodianNumber2();
   /**
	* @ejb:interface-method view-type="local"
	**/
   public abstract void setPrfInvNextCustodianNumber2(String PRF_INV_NXT_CSTDN_NMBR2);

   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="PRF_INV_GL_COA_VRNC_ACCNT"
    **/
   public abstract Integer getPrfInvGlCoaVarianceAccount();
   /**
	* @ejb:interface-method view-type="local"
	**/
   public abstract void setPrfInvGlCoaVarianceAccount(Integer PRF_INV_GL_COA_VRNC_ACCNT);


   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="PRF_AR_AUTO_CMPUTE_COGS"
    **/
   public abstract byte getPrfArAutoComputeCogs();
   /**
	* @ejb:interface-method view-type="local"
	**/
   public abstract void setPrfArAutoComputeCogs(byte PRF_AR_AUTO_CMPUTE_COGS);


   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="PRF_AR_MNTH_INT_RT"
    **/
   public abstract double getPrfArMonthlyInterestRate();
    /**
     * @ejb:interface-method view-type="local"
     **/
   public abstract void setPrfArMonthlyInterestRate(double PRF_AR_MNTH_INT_RT);



   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="PRF_AP_AGNG_BCKT"
    **/
   public abstract int getPrfApAgingBucket();
   /**
	* @ejb:interface-method view-type="local"
	**/
   public abstract void setPrfApAgingBucket(int PRF_AP_AGNG_BCKT);

   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="PRF_AR_AGNG_BCKT"
    **/
   public abstract int getPrfArAgingBucket();
   /**
	* @ejb:interface-method view-type="local"
	**/
   public abstract void setPrfArAgingBucket(int PRF_AR_AGNG_BCKT);

   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="PRF_AR_ALLW_PRR_DT"
    **/
   public abstract byte getPrfArAllowPriorDate();
   /**
	* @ejb:interface-method view-type="local"
	**/
   public abstract void setPrfArAllowPriorDate(byte PRF_AR_ALLW_PRR_DT);

   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="PRF_AR_CHK_INSFFCNT_STCK"
    **/
   public abstract byte getPrfArCheckInsufficientStock();
   /**
	* @ejb:interface-method view-type="local"
	**/
   public abstract void setPrfArCheckInsufficientStock(byte PRF_AR_CHK_INSFFCNT_STCK);

   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="PRF_AR_DTLD_RCVBL"
    **/
   public abstract byte getPrfArDetailedReceivable();
   /**
	* @ejb:interface-method view-type="local"
	**/
   public abstract void setPrfArDetailedReceivable(byte PRF_AR_DTLD_RCVBL);

   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="PRF_AP_SHW_PR_CST"
    **/
   public abstract byte getPrfApShowPrCost();
   /**
	* @ejb:interface-method view-type="local"
	**/
   public abstract void setPrfApShowPrCost(byte PRF_AP_SHW_PR_CST);

   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="PRF_AR_ENBL_PYMNT_TRM"
    **/
   public abstract byte getPrfArEnablePaymentTerm();
   /**
	* @ejb:interface-method view-type="local"
	**/
   public abstract void setPrfArEnablePaymentTerm(byte PRF_AR_ENBL_PYMNT_TRM);

   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="PRF_AR_DSBL_SLS_PRC"
    **/
   public abstract byte getPrfArDisableSalesPrice();
   /**
	* @ejb:interface-method view-type="local"
	**/
   public abstract void setPrfArDisableSalesPrice(byte PRF_AR_DSBL_SLS_PRC);

   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="PRF_AR_VLDT_CST_EMAIL"
    **/
   public abstract byte getPrfArValidateCustomerEmail();
   /**
	* @ejb:interface-method view-type="local"
	**/
   public abstract void setPrfArValidateCustomerEmail(byte PRF_AR_VLDT_CST_EMAIL);

   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="PRF_INV_IL_SHW_ALL"
    **/
   public abstract byte getPrfInvItemLocationShowAll();
   /**
	* @ejb:interface-method view-type="local"
	**/
   public abstract void setPrfInvItemLocationShowAll(byte PRF_INV_IL_SHW_ALL);


   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="PRF_INV_IL_ADD_BY_ITM_LST"
    **/
   public abstract byte getPrfInvItemLocationAddByItemList();
   /**
	* @ejb:interface-method view-type="local"
	**/
   public abstract void setPrfInvItemLocationAddByItemList(byte PRF_INV_IL_ADD_BY_ITM_LST);

   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="PRF_AP_DM_OVRRD_CST"
    **/
   public abstract byte getPrfApDebitMemoOverrideCost();
   /**
	* @ejb:interface-method view-type="local"
	**/
   public abstract void setPrfApDebitMemoOverrideCost(byte PRF_AP_DM_OVRRD_CST);

   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="PRF_GL_YR_END_CLS_RSTRCTN"
    **/
   public abstract byte getPrfGlYearEndCloseRestriction();
   /**
	* @ejb:interface-method view-type="local"
	**/
   public abstract void setPrfGlYearEndCloseRestriction(byte PRF_GL_YR_END_CLS_RSTRCTN);



   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="PRF_AD_DSBL_MTPL_LGN"
    **/
   public abstract byte getPrfAdDisableMultipleLogin();
   /**
	* @ejb:interface-method view-type="local"
	**/
   public abstract void setPrfAdDisableMultipleLogin(byte PRF_AD_DSBL_MTPL_LGN);


   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="PRF_AD_ENBL_EML_NTFCTN"
    **/
   public abstract byte getPrfAdEnableEmailNotification();
   /**
	* @ejb:interface-method view-type="local"
	**/
   public abstract void setPrfAdEnableEmailNotification(byte PRF_AD_ENBL_EML_NTFCTN);
   
   
   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="PRF_AR_SO_SLSPRSN_RQRD"
    **/
   public abstract byte getPrfArSoSalespersonRequired();
   /**
	* @ejb:interface-method view-type="local"
	**/
   public abstract void setPrfArSoSalespersonRequired(byte PRF_AR_SO_SLSPRSN_RQRD);
   
   
   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="PRF_AR_INVC_SLSPRSN_RQRD"
    **/
   public abstract byte getPrfArInvcSalespersonRequired();
   /**
	* @ejb:interface-method view-type="local"
	**/
   public abstract void setPrfArInvcSalespersonRequired(byte PRF_AR_INVC_SLSPRSN_RQRD);



   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="PRF_ML_HST"
    **/
   public abstract String getPrfMailHost();
   /**
	* @ejb:interface-method view-type="local"
	**/
   public abstract void setPrfMailHost(String PRF_ML_HST);

   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="PRF_ML_SCKT_FCTRY_PRT"
    **/
   public abstract String getPrfMailSocketFactoryPort();
   /**
	* @ejb:interface-method view-type="local"
	**/
   public abstract void setPrfMailSocketFactoryPort(String PRF_ML_SCKT_FCTRY_PRT);

   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="PRF_ML_PRT"
    **/
   public abstract String getPrfMailPort();
   /**
	* @ejb:interface-method view-type="local"
	**/
   public abstract void setPrfMailPort(String PRF_ML_PRT);

   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="PRF_ML_FRM"
    **/
   public abstract String getPrfMailFrom();
   /**
	* @ejb:interface-method view-type="local"
	**/
   public abstract void setPrfMailFrom(String PRF_ML_FRM);
   
   
   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="PRF_ML_AUTH"
    **/
   public abstract byte getPrfMailAuthenticator();
   /**
	* @ejb:interface-method view-type="local"
	**/
   public abstract void setPrfMailAuthenticator(byte PRF_ML_AUTH);
   
   
   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="PRF_ML_PSSWRD"
    **/
   public abstract String getPrfMailPassword();
   /**
	* @ejb:interface-method view-type="local"
	**/
   public abstract void setPrfMailPassword(String PRF_ML_PSSWRD);


   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="PRF_ML_TO"
    **/
   public abstract String getPrfMailTo();
   /**
	* @ejb:interface-method view-type="local"
	**/
   public abstract void setPrfMailTo(String PRF_ML_TO);


   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="PRF_ML_CC"
    **/
   public abstract String getPrfMailCc();
   /**
	* @ejb:interface-method view-type="local"
	**/
   public abstract void setPrfMailCc(String PRF_ML_CC);


   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="PRF_ML_BCC"
    **/
   public abstract String getPrfMailBcc();
   /**
	* @ejb:interface-method view-type="local"
	**/
   public abstract void setPrfMailBcc(String PRF_ML_BCC);

   
   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="PRF_ML_CNFG"
    **/
   public abstract String getPrfMailConfig();
   /**
	* @ejb:interface-method view-type="local"
	**/
   public abstract void setPrfMailConfig(String PRF_ML_CNFG);
   
   
   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="PRF_AD_CMPNY"
    **/
   public abstract Integer getPrfAdCompany();
   /**
	* @ejb:interface-method view-type="local"
	**/
   public abstract void setPrfAdCompany(Integer PRF_AD_CMPNY);
   
   
   
    /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="PRF_CNTRL_WRHS_BRNCH"
    **/
   public abstract String getPrfCentralWarehouseBranch();
   /**
    * @ejb:interface-method view-type="local"
    **/
   public abstract void setPrfCentralWarehouseBranch(String PRF_CNTRL_WRHS_BRNCH);
   
   
   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="PRF_CNTRL_WRHS_LCTN"
    **/
   public abstract String getPrfCentralWarehouseLocation();
   /**
    * @ejb:interface-method view-type="local"
    **/
   public abstract void setPrfCentralWarehouseLocation(String PRF_CNTRL_WRHS_LCTN);
   
   

   // Access methods for relationship fields

   /**
    * @ejb:interface-method view-type="local"
    * @ejb:relation name="arwithholdingtaxcode-preferences"
    *               role-name="preference-has-one-arwithholdingtaxcode"
    *               cascade-delete="yes"
    *
    * @jboss:relation related-pk-field="wtcCode"
    *                 fk-column="AR_WITHHOLDING_TAX_CODE"
    */
    public abstract LocalArWithholdingTaxCode getArWithholdingTaxCode();
    public abstract void setArWithholdingTaxCode(LocalArWithholdingTaxCode arWithholdingTaxCode);
    
    
    
    

   // No Business methods

   // EntityBean methods

   /**
    * @ejb:create-method view-type="local"
    **/
  public java.lang.Integer ejbCreate (java.lang.Integer PRF_CODE, byte PRF_ALLW_SSPNS_PSTNG,
        short PRF_GL_JRNL_LN_NMBR, short PRF_AP_JRNL_LN_NMBR, short PRF_AR_INVC_LN_NMBR,
	      String PRF_AP_W_TX_RLZTN, String PRF_AR_W_TX_RLZTN, byte PRF_ENBL_GL_JRNL_BTCH, byte PRF_ENBL_GL_RCMPT_COA_BLNC,
	      byte PRF_ENBL_AP_VCHR_BTCH, byte PRF_ENBL_AP_PO_BTCH, byte PRF_ENBL_AP_CHCK_BTCH, byte PRF_ENBL_AR_INVC_BTCH, byte PRF_ENBL_AR_INVC_INT_GNRTN,
	      byte PRF_ENBL_AR_RCPT_BTCH, byte PRF_ENBL_AR_MISC_RCPT_BTCH, String PRF_AP_GL_PSTNG_TYP, String PRF_AR_GL_PSTNG_TYP,
		  String PRF_CM_GL_PSTNG_TYP, short PRF_INV_INVNTRY_LN_NMBR, short PRF_INV_QTY_PRCSN_UNT,
		  short PRF_INV_CST_PRCSN_UNT, String PRF_INV_GL_PSTNG_TYP, String PRF_GL_PSTNG_TYP, byte PRF_INV_ENBL_SHFT, byte PRF_ENBL_INV_BUA_BTCH,
		  String PRF_AP_FND_CHCK_DFLT_TYP, String PRF_AR_FND_RCPT_DFLT_TYP, byte PRF_AP_US_ACCRD_VAT,
		  String PRF_AP_DFLT_CHCK_DT, String PRF_AP_DFLT_CHCKR, String PRF_AP_DFLT_APPRVR,
		  Integer PRF_AP_GL_COA_ACCRD_VAT_ACCNT, Integer PRF_AP_GL_COA_PTTY_CSH_ACCNT,
		  byte PRF_CM_USE_BNK_FRM, byte PRF_AP_USE_SPPLR_PLLDWN, byte PRF_AR_USE_CSTMR_PLLDWN,
		  byte PRF_AP_AT_GNRT_SPPLR_CODE, String PRF_AP_NXT_SPPLR_CODE, byte PRF_AR_AT_GNRT_CSTMR_CODE,
		  String PRF_AR_NXT_CSTMR_CODE, byte PRF_AP_RFRNC_NMBR_VLDTN, byte PRF_INV_ENBL_POS_INTGRTN, byte PRF_INV_ENBL_POS_AUTO_POST_UP,
		  Integer PRF_INV_POS_ADJSTMNT_ACCNT, String PRF_AP_CHK_VCHR_DT_SRC, Integer PRF_MISC_POS_DSCNT_ACCNT,
		  Integer PRF_MISC_POS_GFT_CRTFCT_ACCNT, Integer PRF_MISC_POS_SRVC_CHRG_ACCNT, Integer PRF_MISC_POS_DN_IN_CHRG_ACCNT,
		  Integer PRF_AR_GL_COA_CSTMR_DPST_ACCNT, String PRF_AP_DFLT_PR_TX, String PRF_AP_DFLT_PR_CRRNCY,
		  String PRF_AR_SLS_INVC_DT_SRC, Integer PRF_INV_GL_COA_VRNC_ACCNT, byte PRF_AR_AUTO_CMPUTE_COGS, double PRF_AR_MNTH_INT_RT,
		  int PRF_AP_AGNG_BCKT, int PRF_AR_AGNG_BCKT,
		  byte PRF_AR_ALLW_PRR_DT, byte PRF_AR_CHK_INSFFCNT_STCK, byte PRF_AR_DTLD_RCVBL,
		  byte PRF_AP_SHW_PR_CST, byte PRF_AR_ENBL_PYMNT_TRM,
		  byte PRF_AR_SO_SLSPRSN_RQRD , byte PRF_AR_INVC_SLSPRSN_RQRD,
		  String PRF_ML_HST, String PRF_ML_SCKT_FCTRY_PRT, String PRF_ML_PRT, String PRF_ML_FRM, byte PRF_ML_AUTH, String PRF_ML_PSSWRD, String PRF_ML_TO, String PRF_ML_CC, String PRF_ML_BCC, String PRF_ML_CNFG,
		  Integer PRF_AD_CMPNY)
      throws CreateException {

      Debug.print("AdPreferenceBean ejbCreate");

      setPrfCode(PRF_CODE);
      setPrfAllowSuspensePosting(PRF_ALLW_SSPNS_PSTNG);
      setPrfGlJournalLineNumber(PRF_GL_JRNL_LN_NMBR);
      setPrfApJournalLineNumber(PRF_AP_JRNL_LN_NMBR);
      setPrfArInvoiceLineNumber(PRF_AR_INVC_LN_NMBR);
      setPrfApWTaxRealization(PRF_AP_W_TX_RLZTN);
      setPrfArWTaxRealization(PRF_AR_W_TX_RLZTN);
      setPrfEnableGlJournalBatch(PRF_ENBL_GL_JRNL_BTCH);
      setPrfEnableGlRecomputeCoaBalance(PRF_ENBL_GL_RCMPT_COA_BLNC);
      setPrfEnableApVoucherBatch(PRF_ENBL_AP_VCHR_BTCH);
      setPrfEnableApPOBatch(PRF_ENBL_AP_PO_BTCH);
      setPrfEnableApCheckBatch(PRF_ENBL_AP_CHCK_BTCH);
      setPrfEnableArInvoiceBatch(PRF_ENBL_AR_INVC_BTCH);
      setPrfEnableArInvoiceInterestGeneration(PRF_ENBL_AR_INVC_INT_GNRTN);
      setPrfEnableArReceiptBatch(PRF_ENBL_AR_RCPT_BTCH);
      setPrfEnableArMiscReceiptBatch(PRF_ENBL_AR_MISC_RCPT_BTCH);
      setPrfApGlPostingType(PRF_AP_GL_PSTNG_TYP);
      setPrfArGlPostingType(PRF_AR_GL_PSTNG_TYP);
      setPrfCmGlPostingType(PRF_CM_GL_PSTNG_TYP);
      setPrfInvInventoryLineNumber(PRF_INV_INVNTRY_LN_NMBR);
      setPrfInvQuantityPrecisionUnit(PRF_INV_QTY_PRCSN_UNT);
      setPrfInvCostPrecisionUnit(PRF_INV_CST_PRCSN_UNT);
      setPrfInvGlPostingType(PRF_INV_GL_PSTNG_TYP);
      setPrfGlPostingType(PRF_GL_PSTNG_TYP);
      setPrfInvEnableShift(PRF_INV_ENBL_SHFT);
      setPrfEnableInvBUABatch(PRF_ENBL_INV_BUA_BTCH);
      setPrfApFindCheckDefaultType(PRF_AP_FND_CHCK_DFLT_TYP);
      setPrfArFindReceiptDefaultType(PRF_AR_FND_RCPT_DFLT_TYP);
      setPrfApUseAccruedVat(PRF_AP_US_ACCRD_VAT);
      setPrfApDefaultCheckDate(PRF_AP_DFLT_CHCK_DT);
      setPrfApDefaultChecker(PRF_AP_DFLT_CHCKR);
      setPrfApDefaultApprover(PRF_AP_DFLT_APPRVR);
      setPrfApGlCoaAccruedVatAccount(PRF_AP_GL_COA_ACCRD_VAT_ACCNT);
      setPrfApGlCoaPettyCashAccount(PRF_AP_GL_COA_PTTY_CSH_ACCNT);
      setPrfCmUseBankForm(PRF_CM_USE_BNK_FRM);
      setPrfApUseSupplierPulldown(PRF_AP_USE_SPPLR_PLLDWN);
      setPrfArUseCustomerPulldown(PRF_AR_USE_CSTMR_PLLDWN);
      setPrfApAutoGenerateSupplierCode(PRF_AP_AT_GNRT_SPPLR_CODE);
      setPrfApNextSupplierCode(PRF_AP_NXT_SPPLR_CODE);
      setPrfArAutoGenerateCustomerCode(PRF_AR_AT_GNRT_CSTMR_CODE);
      setPrfArNextCustomerCode(PRF_AR_NXT_CSTMR_CODE);
      setPrfApReferenceNumberValidation(PRF_AP_RFRNC_NMBR_VLDTN);
      setPrfInvEnablePosIntegration(PRF_INV_ENBL_POS_INTGRTN);
      setPrfInvEnablePosAutoPostUpload(PRF_INV_ENBL_POS_AUTO_POST_UP);
      setPrfInvPosAdjustmentAccount(PRF_INV_POS_ADJSTMNT_ACCNT);
      setPrfApCheckVoucherDataSource(PRF_AP_CHK_VCHR_DT_SRC);
      setPrfMiscPosDiscountAccount(PRF_MISC_POS_DSCNT_ACCNT);
      setPrfMiscPosGiftCertificateAccount(PRF_MISC_POS_GFT_CRTFCT_ACCNT);
      setPrfMiscPosServiceChargeAccount(PRF_MISC_POS_SRVC_CHRG_ACCNT);
      setPrfMiscPosDineInChargeAccount(PRF_MISC_POS_DN_IN_CHRG_ACCNT);
      setPrfArGlCoaCustomerDepositAccount(PRF_AR_GL_COA_CSTMR_DPST_ACCNT);
      setPrfApDefaultPrTax(PRF_AP_DFLT_PR_TX);
      setPrfApDefaultPrCurrency(PRF_AP_DFLT_PR_CRRNCY);
      setPrfArSalesInvoiceDataSource(PRF_AR_SLS_INVC_DT_SRC);
      setPrfInvGlCoaVarianceAccount(PRF_INV_GL_COA_VRNC_ACCNT);
      setPrfArAutoComputeCogs(PRF_AR_AUTO_CMPUTE_COGS);
      setPrfArMonthlyInterestRate(PRF_AR_MNTH_INT_RT);
      setPrfApAgingBucket(PRF_AP_AGNG_BCKT);
      setPrfArAgingBucket(PRF_AR_AGNG_BCKT);

      setPrfArAllowPriorDate(PRF_AR_ALLW_PRR_DT);
      setPrfArCheckInsufficientStock(PRF_AR_CHK_INSFFCNT_STCK);
      setPrfArDetailedReceivable(PRF_AR_DTLD_RCVBL);

      setPrfApShowPrCost(PRF_AP_SHW_PR_CST);
      setPrfArEnablePaymentTerm(PRF_AR_ENBL_PYMNT_TRM);

      setPrfArSoSalespersonRequired(PRF_AR_INVC_SLSPRSN_RQRD);
      setPrfArInvcSalespersonRequired(PRF_AR_INVC_SLSPRSN_RQRD);
      
      
      setPrfMailHost(PRF_ML_HST);
      setPrfMailSocketFactoryPort(PRF_ML_SCKT_FCTRY_PRT);
      setPrfMailPort(PRF_ML_PRT);
      setPrfMailFrom(PRF_ML_FRM);
      setPrfMailAuthenticator(PRF_ML_AUTH);
      setPrfMailPassword(PRF_ML_PSSWRD);
      setPrfMailTo(PRF_ML_TO);
      setPrfMailCc(PRF_ML_CC);
      setPrfMailBcc(PRF_ML_BCC);
      setPrfMailConfig(PRF_ML_CNFG);

      setPrfAdCompany(PRF_AD_CMPNY);

      return null;
   }

  /**
   * @ejb:create-method view-type="local"
   **/
 public java.lang.Integer ejbCreate (byte PRF_ALLW_SSPNS_PSTNG,
       short PRF_GL_JRNL_LN_NMBR, short PRF_AP_JRNL_LN_NMBR, short PRF_AR_INVC_LN_NMBR,
	      String PRF_AP_W_TX_RLZTN, String PRF_AR_W_TX_RLZTN, byte PRF_ENBL_GL_JRNL_BTCH, byte PRF_ENBL_GL_RCMPT_COA_BLNC,
	      byte PRF_ENBL_AP_VCHR_BTCH, byte PRF_ENBL_AP_PO_BTCH, byte PRF_ENBL_AP_CHCK_BTCH, byte PRF_ENBL_AR_INVC_BTCH, byte PRF_ENBL_AR_INVC_INT_GNRTN,
	      byte PRF_ENBL_AR_RCPT_BTCH, byte PRF_ENBL_AR_MISC_RCPT_BTCH, String PRF_AP_GL_PSTNG_TYP, String PRF_AR_GL_PSTNG_TYP,
		  String PRF_CM_GL_PSTNG_TYP, short PRF_INV_INVNTRY_LN_NMBR, short PRF_INV_QTY_PRCSN_UNT,
		  short PRF_INV_CST_PRCSN_UNT, String PRF_INV_GL_PSTNG_TYP, String PRF_GL_PSTNG_TYP, byte PRF_INV_ENBL_SHFT, byte PRF_ENBL_INV_BUA_BTCH,
		  String PRF_AP_FND_CHCK_DFLT_TYP, String PRF_AR_FND_RCPT_DFLT_TYP, byte PRF_AP_US_ACCRD_VAT,
		  String PRF_AP_DFLT_CHCK_DT, String PRF_AP_DFLT_CHCKR, String PRF_AP_DFLT_APPRVR,
		  Integer PRF_AP_GL_COA_ACCRD_VAT_ACCNT, Integer PRF_AP_GL_COA_PTTY_CSH_ACCNT,
		  byte PRF_CM_USE_BNK_FRM, byte PRF_AP_USE_SPPLR_PLLDWN, byte PRF_AR_USE_CSTMR_PLLDWN,
		  byte PRF_AP_AT_GNRT_SPPLR_CODE, String PRF_AP_NXT_SPPLR_CODE, byte PRF_AR_AT_GNRT_CSTMR_CODE,
		  String PRF_AR_NXT_CSTMR_CODE, byte PRF_AP_RFRNC_NMBR_VLDTN, byte PRF_INV_ENBL_POS_INTGRTN, byte PRF_INV_ENBL_POS_AUTO_POST_UP,
		  Integer PRF_INV_POS_ADJSTMNT_ACCNT, String PRF_AP_CHK_VCHR_DT_SRC, Integer PRF_MISC_POS_DSCNT_ACCNT,
		  Integer PRF_MISC_POS_GFT_CRTFCT_ACCNT, Integer PRF_MISC_POS_SRVC_CHRG_ACCNT, Integer PRF_MISC_POS_DN_IN_CHRG_ACCNT,
		  Integer PRF_AR_GL_COA_CSTMR_DPST_ACCNT, String PRF_AP_DFLT_PR_TX, String PRF_AP_DFLT_PR_CRRNCY,
		  String PRF_AR_SLS_INVC_DT_SRC, Integer PRF_INV_GL_COA_VRNC_ACCNT, byte PRF_AR_AUTO_CMPUTE_COGS, double PRF_AR_MNTH_INT_RT,
		  int PRF_AP_AGNG_BCKT, int PRF_AR_AGNG_BCKT,
		  byte PRF_AR_ALLW_PRR_DT, byte PRF_AR_CHK_INSFFCNT_STCK, byte PRF_AR_DTLD_RCVBL,
		  byte PRF_AP_SHW_PR_CST, byte PRF_AR_ENBL_PYMNT_TRM,
		  byte PRF_AR_SO_SLSPRSN_RQRD , byte PRF_AR_INVC_SLSPRSN_RQRD,
		  String PRF_ML_HST, String PRF_ML_SCKT_FCTRY_PRT, String PRF_ML_PRT, String PRF_ML_FRM, byte PRF_ML_AUTH, String PRF_ML_PSSWRD, String PRF_ML_TO, String PRF_ML_CC, String PRF_ML_BCC, String PRF_ML_CNFG,
		  Integer PRF_AD_CMPNY)
     throws CreateException {

     Debug.print("AdPreferenceBean ejbCreate");

     setPrfAllowSuspensePosting(PRF_ALLW_SSPNS_PSTNG);
     setPrfGlJournalLineNumber(PRF_GL_JRNL_LN_NMBR);
     setPrfApJournalLineNumber(PRF_AP_JRNL_LN_NMBR);
     setPrfArInvoiceLineNumber(PRF_AR_INVC_LN_NMBR);
     setPrfApWTaxRealization(PRF_AP_W_TX_RLZTN);
     setPrfArWTaxRealization(PRF_AR_W_TX_RLZTN);
     setPrfEnableGlJournalBatch(PRF_ENBL_GL_JRNL_BTCH);
     setPrfEnableGlRecomputeCoaBalance(PRF_ENBL_GL_RCMPT_COA_BLNC);
     setPrfEnableApVoucherBatch(PRF_ENBL_AP_VCHR_BTCH);
     setPrfEnableApPOBatch(PRF_ENBL_AP_PO_BTCH);
     setPrfEnableApCheckBatch(PRF_ENBL_AP_CHCK_BTCH);
     setPrfEnableArInvoiceBatch(PRF_ENBL_AR_INVC_BTCH);
     setPrfEnableArInvoiceInterestGeneration(PRF_ENBL_AR_INVC_INT_GNRTN);
     setPrfEnableArReceiptBatch(PRF_ENBL_AR_RCPT_BTCH);
     setPrfEnableArMiscReceiptBatch(PRF_ENBL_AR_MISC_RCPT_BTCH);
     setPrfApGlPostingType(PRF_AP_GL_PSTNG_TYP);
     setPrfArGlPostingType(PRF_AR_GL_PSTNG_TYP);
     setPrfCmGlPostingType(PRF_CM_GL_PSTNG_TYP);
     setPrfInvInventoryLineNumber(PRF_INV_INVNTRY_LN_NMBR);
     setPrfInvQuantityPrecisionUnit(PRF_INV_QTY_PRCSN_UNT);
     setPrfInvCostPrecisionUnit(PRF_INV_CST_PRCSN_UNT);
     setPrfInvGlPostingType(PRF_INV_GL_PSTNG_TYP);
     setPrfGlPostingType(PRF_GL_PSTNG_TYP);
     setPrfInvEnableShift(PRF_INV_ENBL_SHFT);
     setPrfEnableInvBUABatch(PRF_ENBL_INV_BUA_BTCH);
     setPrfApFindCheckDefaultType(PRF_AP_FND_CHCK_DFLT_TYP);
     setPrfArFindReceiptDefaultType(PRF_AR_FND_RCPT_DFLT_TYP);
     setPrfApUseAccruedVat(PRF_AP_US_ACCRD_VAT);
     setPrfApDefaultCheckDate(PRF_AP_DFLT_CHCK_DT);
     setPrfApDefaultChecker(PRF_AP_DFLT_CHCKR);
     setPrfApDefaultApprover(PRF_AP_DFLT_APPRVR);
     setPrfApGlCoaAccruedVatAccount(PRF_AP_GL_COA_ACCRD_VAT_ACCNT);
     setPrfApGlCoaPettyCashAccount(PRF_AP_GL_COA_PTTY_CSH_ACCNT);
     setPrfCmUseBankForm(PRF_CM_USE_BNK_FRM);
     setPrfApUseSupplierPulldown(PRF_AP_USE_SPPLR_PLLDWN);
     setPrfArUseCustomerPulldown(PRF_AR_USE_CSTMR_PLLDWN);
     setPrfApAutoGenerateSupplierCode(PRF_AP_AT_GNRT_SPPLR_CODE);
     setPrfApNextSupplierCode(PRF_AP_NXT_SPPLR_CODE);
     setPrfArAutoGenerateCustomerCode(PRF_AR_AT_GNRT_CSTMR_CODE);
     setPrfArNextCustomerCode(PRF_AR_NXT_CSTMR_CODE);
     setPrfApReferenceNumberValidation(PRF_AP_RFRNC_NMBR_VLDTN);
     setPrfInvEnablePosIntegration(PRF_INV_ENBL_POS_INTGRTN);
     setPrfInvEnablePosAutoPostUpload(PRF_INV_ENBL_POS_AUTO_POST_UP);
     setPrfInvPosAdjustmentAccount(PRF_INV_POS_ADJSTMNT_ACCNT);
     setPrfApCheckVoucherDataSource(PRF_AP_CHK_VCHR_DT_SRC);
     setPrfMiscPosDiscountAccount(PRF_MISC_POS_DSCNT_ACCNT);
     setPrfMiscPosGiftCertificateAccount(PRF_MISC_POS_GFT_CRTFCT_ACCNT);
     setPrfMiscPosServiceChargeAccount(PRF_MISC_POS_SRVC_CHRG_ACCNT);
     setPrfMiscPosDineInChargeAccount(PRF_MISC_POS_DN_IN_CHRG_ACCNT);
     setPrfArGlCoaCustomerDepositAccount(PRF_AR_GL_COA_CSTMR_DPST_ACCNT);
     setPrfApDefaultPrTax(PRF_AP_DFLT_PR_TX);
     setPrfApDefaultPrCurrency(PRF_AP_DFLT_PR_CRRNCY);
     setPrfArSalesInvoiceDataSource(PRF_AR_SLS_INVC_DT_SRC);
     setPrfInvGlCoaVarianceAccount(PRF_INV_GL_COA_VRNC_ACCNT);
     setPrfArAutoComputeCogs(PRF_AR_AUTO_CMPUTE_COGS);
     setPrfArMonthlyInterestRate(PRF_AR_MNTH_INT_RT);
     setPrfApAgingBucket(PRF_AP_AGNG_BCKT);
     setPrfArAgingBucket(PRF_AR_AGNG_BCKT);

     setPrfArAllowPriorDate(PRF_AR_ALLW_PRR_DT);
     setPrfArCheckInsufficientStock(PRF_AR_CHK_INSFFCNT_STCK);
     setPrfArDetailedReceivable(PRF_AR_DTLD_RCVBL);

     setPrfApShowPrCost(PRF_AP_SHW_PR_CST);
     setPrfArEnablePaymentTerm(PRF_AR_ENBL_PYMNT_TRM);
     
     setPrfArSoSalespersonRequired(PRF_AR_INVC_SLSPRSN_RQRD);
     setPrfArInvcSalespersonRequired(PRF_AR_INVC_SLSPRSN_RQRD);

     setPrfMailHost(PRF_ML_HST);
     setPrfMailSocketFactoryPort(PRF_ML_SCKT_FCTRY_PRT);
     setPrfMailPort(PRF_ML_PRT);
     setPrfMailFrom(PRF_ML_FRM);
     setPrfMailAuthenticator(PRF_ML_AUTH);
     setPrfMailPassword(PRF_ML_PSSWRD);
     setPrfMailTo(PRF_ML_TO);
     setPrfMailCc(PRF_ML_CC);
     setPrfMailBcc(PRF_ML_BCC);
     setPrfMailConfig(PRF_ML_CNFG);

     setPrfAdCompany(PRF_AD_CMPNY);

     return null;

  }

  /**
    * @ejb:create-method view-type="local"
    **/
  public java.lang.Integer ejbCreate (byte PRF_ALLW_SSPNS_PSTNG,
        short PRF_GL_JRNL_LN_NMBR, short PRF_AP_JRNL_LN_NMBR, short PRF_AR_INVC_LN_NMBR,
	      String PRF_AP_W_TX_RLZTN, String PRF_AR_W_TX_RLZTN, byte PRF_ENBL_GL_JRNL_BTCH, byte PRF_ENBL_GL_RCMPT_COA_BLNC,
	      byte PRF_ENBL_AP_VCHR_BTCH, byte PRF_ENBL_AP_PO_BTCH, byte PRF_ENBL_AP_CHCK_BTCH, byte PRF_ENBL_AR_INVC_BTCH, byte PRF_ENBL_AR_INVC_INT_GNRTN,
	      byte PRF_ENBL_AR_RCPT_BTCH, byte PRF_ENBL_AR_MISC_RCPT_BTCH, String PRF_AP_GL_PSTNG_TYP, String PRF_AR_GL_PSTNG_TYP,
		  String PRF_CM_GL_PSTNG_TYP, short PRF_INV_INVNTRY_LN_NMBR, short PRF_INV_QTY_PRCSN_UNT,
		  short PRF_INV_CST_PRCSN_UNT, String PRF_INV_GL_PSTNG_TYP, String PRF_GL_PSTNG_TYP, byte PRF_INV_ENBL_SHFT, byte PRF_ENBL_INV_BUA_BTCH,
		  String PRF_AP_FND_CHCK_DFLT_TYP, String PRF_AR_FND_RCPT_DFLT_TYP, byte PRF_AP_US_ACCRD_VAT,
		  String PRF_AP_DFLT_CHCK_DT, String PRF_AP_DFLT_CHCKR, String PRF_AP_DFLT_APPRVR,
		  Integer PRF_AP_GL_COA_ACCRD_VAT_ACCNT, Integer PRF_AP_GL_COA_PTTY_CSH_ACCNT,
		  byte PRF_CM_USE_BNK_FRM, byte PRF_AP_USE_SPPLR_PLLDWN, byte PRF_AR_USE_CSTMR_PLLDWN,
		  byte PRF_AP_AT_GNRT_SPPLR_CODE, String PRF_AP_NXT_SPPLR_CODE, byte PRF_AR_AT_GNRT_CSTMR_CODE,
		  String PRF_AR_NXT_CSTMR_CODE, byte PRF_AP_RFRNC_NMBR_VLDTN, byte PRF_INV_ENBL_POS_INTGRTN, byte PRF_INV_ENBL_POS_AUTO_POST_UP,
		  Integer PRF_INV_POS_ADJSTMNT_ACCNT, String PRF_AP_CHK_VCHR_DT_SRC, Integer PRF_MISC_POS_DSCNT_ACCNT,
		  Integer PRF_MISC_POS_GFT_CRTFCT_ACCNT, Integer PRF_MISC_POS_SRVC_CHRG_ACCNT, Integer PRF_MISC_POS_DN_IN_CHRG_ACCNT,
		  Integer PRF_AR_GL_COA_CSTMR_DPST_ACCNT, String PRF_AP_DFLT_PR_TX, String PRF_AP_DFLT_PR_CRRNCY,
		  String PRF_AR_SLS_INVC_DT_SRC, Integer PRF_INV_GL_COA_VRNC_ACCNT, byte PRF_AR_AUTO_CMPUTE_COGS, double PRF_AR_MNTH_INT_RT,
		  int PRF_AP_AGNG_BCKT, int PRF_AR_AGNG_BCKT,

		  byte PRF_AR_ALLW_PRR_DT, byte PRF_AR_CHK_INSFFCNT_STCK, byte PRF_AR_DTLD_RCVBL,

		  byte PRF_AP_SHW_PR_CST, byte PRF_AR_ENBL_PYMNT_TRM,
		  byte PRF_AR_DSBL_SLS_PRC, byte PRF_INV_IL_SHW_ALL, byte PRF_INV_IL_ADD_BY_ITM_LST, byte PRF_AP_DM_OVRRD_CST, byte PRF_GL_YR_END_CLS_RSTRCTN,
		  byte PRF_AD_DSBL_MTPL_LGN, byte PRF_AD_ENBL_EML_NTFCTN,
		  byte PRF_AR_SO_SLSPRSN_RQRD , byte PRF_AR_INVC_SLSPRSN_RQRD,
		  String PRF_ML_HST, String PRF_ML_SCKT_FCTRY_PRT, String PRF_ML_PRT, String PRF_ML_FRM, byte PRF_ML_AUTH, String PRF_ML_PSSWRD, String PRF_ML_TO, String PRF_ML_CC, String PRF_ML_BCC, String PRF_ML_CNFG,
		  Integer PRF_AD_CMPNY)
      throws CreateException {

      Debug.print("AdPreferenceBean ejbCreate");

      setPrfAllowSuspensePosting(PRF_ALLW_SSPNS_PSTNG);
      setPrfGlJournalLineNumber(PRF_GL_JRNL_LN_NMBR);
      setPrfApJournalLineNumber(PRF_AP_JRNL_LN_NMBR);
      setPrfArInvoiceLineNumber(PRF_AR_INVC_LN_NMBR);
      setPrfApWTaxRealization(PRF_AP_W_TX_RLZTN);
      setPrfArWTaxRealization(PRF_AR_W_TX_RLZTN);
      setPrfEnableGlJournalBatch(PRF_ENBL_GL_JRNL_BTCH);
      setPrfEnableGlRecomputeCoaBalance(PRF_ENBL_GL_RCMPT_COA_BLNC);
      setPrfEnableApVoucherBatch(PRF_ENBL_AP_VCHR_BTCH);
      setPrfEnableApPOBatch(PRF_ENBL_AP_PO_BTCH);
      setPrfEnableApCheckBatch(PRF_ENBL_AP_CHCK_BTCH);
      setPrfEnableArInvoiceBatch(PRF_ENBL_AR_INVC_BTCH);
      setPrfEnableArInvoiceInterestGeneration(PRF_ENBL_AR_INVC_INT_GNRTN);
      setPrfEnableArReceiptBatch(PRF_ENBL_AR_RCPT_BTCH);
      setPrfEnableArMiscReceiptBatch(PRF_ENBL_AR_MISC_RCPT_BTCH);
      setPrfApGlPostingType(PRF_AP_GL_PSTNG_TYP);
      setPrfArGlPostingType(PRF_AR_GL_PSTNG_TYP);
      setPrfCmGlPostingType(PRF_CM_GL_PSTNG_TYP);
      setPrfInvInventoryLineNumber(PRF_INV_INVNTRY_LN_NMBR);
      setPrfInvQuantityPrecisionUnit(PRF_INV_QTY_PRCSN_UNT);
      setPrfInvCostPrecisionUnit(PRF_INV_CST_PRCSN_UNT);
      setPrfInvGlPostingType(PRF_INV_GL_PSTNG_TYP);
      setPrfGlPostingType(PRF_GL_PSTNG_TYP);
      setPrfInvEnableShift(PRF_INV_ENBL_SHFT);
      setPrfEnableInvBUABatch(PRF_ENBL_INV_BUA_BTCH);
      setPrfApFindCheckDefaultType(PRF_AP_FND_CHCK_DFLT_TYP);
      setPrfArFindReceiptDefaultType(PRF_AR_FND_RCPT_DFLT_TYP);
      setPrfApUseAccruedVat(PRF_AP_US_ACCRD_VAT);
      setPrfApDefaultCheckDate(PRF_AP_DFLT_CHCK_DT);
      setPrfApDefaultChecker(PRF_AP_DFLT_CHCKR);
      setPrfApDefaultApprover(PRF_AP_DFLT_APPRVR);
      setPrfApGlCoaAccruedVatAccount(PRF_AP_GL_COA_ACCRD_VAT_ACCNT);
      setPrfApGlCoaPettyCashAccount(PRF_AP_GL_COA_PTTY_CSH_ACCNT);
      setPrfCmUseBankForm(PRF_CM_USE_BNK_FRM);
      setPrfApUseSupplierPulldown(PRF_AP_USE_SPPLR_PLLDWN);
      setPrfArUseCustomerPulldown(PRF_AR_USE_CSTMR_PLLDWN);
      setPrfApAutoGenerateSupplierCode(PRF_AP_AT_GNRT_SPPLR_CODE);
      setPrfApNextSupplierCode(PRF_AP_NXT_SPPLR_CODE);
      setPrfArAutoGenerateCustomerCode(PRF_AR_AT_GNRT_CSTMR_CODE);
      setPrfArNextCustomerCode(PRF_AR_NXT_CSTMR_CODE);
      setPrfApReferenceNumberValidation(PRF_AP_RFRNC_NMBR_VLDTN);
      setPrfInvEnablePosIntegration(PRF_INV_ENBL_POS_INTGRTN);
      setPrfInvEnablePosAutoPostUpload(PRF_INV_ENBL_POS_AUTO_POST_UP);
      setPrfInvPosAdjustmentAccount(PRF_INV_POS_ADJSTMNT_ACCNT);
      setPrfApCheckVoucherDataSource(PRF_AP_CHK_VCHR_DT_SRC);
      setPrfMiscPosDiscountAccount(PRF_MISC_POS_DSCNT_ACCNT);
      setPrfMiscPosGiftCertificateAccount(PRF_MISC_POS_GFT_CRTFCT_ACCNT);
      setPrfMiscPosServiceChargeAccount(PRF_MISC_POS_SRVC_CHRG_ACCNT);
      setPrfMiscPosDineInChargeAccount(PRF_MISC_POS_DN_IN_CHRG_ACCNT);
      setPrfArGlCoaCustomerDepositAccount(PRF_AR_GL_COA_CSTMR_DPST_ACCNT);
      setPrfApDefaultPrTax(PRF_AP_DFLT_PR_TX);
      setPrfApDefaultPrCurrency(PRF_AP_DFLT_PR_CRRNCY);
      setPrfArSalesInvoiceDataSource(PRF_AR_SLS_INVC_DT_SRC);
      setPrfInvGlCoaVarianceAccount(PRF_INV_GL_COA_VRNC_ACCNT);
      setPrfArAutoComputeCogs(PRF_AR_AUTO_CMPUTE_COGS);
      setPrfArMonthlyInterestRate(PRF_AR_MNTH_INT_RT);
      setPrfApAgingBucket(PRF_AP_AGNG_BCKT);
      setPrfArAgingBucket(PRF_AR_AGNG_BCKT);

      setPrfArAllowPriorDate(PRF_AR_ALLW_PRR_DT);
      setPrfArCheckInsufficientStock(PRF_AR_CHK_INSFFCNT_STCK);
      setPrfArDetailedReceivable(PRF_AR_DTLD_RCVBL);

      setPrfApShowPrCost(PRF_AP_SHW_PR_CST);
      setPrfArEnablePaymentTerm(PRF_AR_ENBL_PYMNT_TRM);
      setPrfArDisableSalesPrice(PRF_AR_DSBL_SLS_PRC);
      setPrfInvItemLocationShowAll(PRF_INV_IL_SHW_ALL);
      setPrfInvItemLocationAddByItemList(PRF_INV_IL_ADD_BY_ITM_LST);
      setPrfApDebitMemoOverrideCost(PRF_AP_DM_OVRRD_CST);
      setPrfGlYearEndCloseRestriction(PRF_GL_YR_END_CLS_RSTRCTN);
      setPrfAdDisableMultipleLogin(PRF_AD_DSBL_MTPL_LGN);
      setPrfAdEnableEmailNotification(PRF_AD_ENBL_EML_NTFCTN);

      setPrfArSoSalespersonRequired(PRF_AR_INVC_SLSPRSN_RQRD);
      setPrfArInvcSalespersonRequired(PRF_AR_INVC_SLSPRSN_RQRD);
      
      setPrfMailHost(PRF_ML_HST);
      setPrfMailSocketFactoryPort(PRF_ML_SCKT_FCTRY_PRT);
      setPrfMailPort(PRF_ML_PRT);
      setPrfMailFrom(PRF_ML_FRM);
      setPrfMailAuthenticator(PRF_ML_AUTH);
      setPrfMailPassword(PRF_ML_PSSWRD);
      setPrfMailTo(PRF_ML_TO);
      setPrfMailCc(PRF_ML_CC);
      setPrfMailBcc(PRF_ML_BCC);
      setPrfMailConfig(PRF_ML_CNFG);
      setPrfAdCompany(PRF_AD_CMPNY);

      return null;

   }

   public void ejbPostCreate (java.lang.Integer PRF_CODE, byte PRF_ALLW_SSPNS_PSTNG,
        short PRF_GL_JRNL_LN_NMBR, short PRF_AP_JRNL_LN_NMBR, short PRF_AR_INVC_LN_NMBR,
	      String PRF_AP_W_TX_RLZTN, String PRF_AR_W_TX_RLZTN, byte PRF_ENBL_GL_JRNL_BTCH, byte PRF_ENBL_GL_RCMPT_COA_BLNC,
	      byte PRF_ENBL_AP_VCHR_BTCH, byte PRF_ENBL_AP_PO_BTCH, byte PRF_ENBL_AP_CHCK_BTCH, byte PRF_ENBL_AR_INVC_BTCH, byte PRF_ENBL_AR_INVC_INT_GNRTN,
	      byte PRF_ENBL_AR_RCPT_BTCH, byte PRF_ENBL_AR_MISC_RCPT_BTCH, String PRF_AP_GL_PSTNG_TYP, String PRF_AR_GL_PSTNG_TYP,
		  String PRF_CM_GL_PSTNG_TYP, short PRF_INV_INVNTRY_LN_NMBR, short PRF_INV_QTY_PRCSN_UNT,
		  short PRF_INV_CST_PRCSN_UNT, String PRF_INV_GL_PSTNG_TYP, String PRF_GL_PSTNG_TYP, byte PRF_INV_ENBL_SHFT, byte PRF_ENBL_INV_BUA_BTCH,
		  String PRF_AP_FND_CHCK_DFLT_TYP, String PRF_AR_FND_RCPT_DFLT_TYP, byte PRF_AP_US_ACCRD_VAT,
		  String PRF_AP_DFLT_CHCK_DT, String PRF_AP_DFLT_CHCKR, String PRF_AP_DFLT_APPRVR,
		  Integer PRF_AP_GL_COA_ACCRD_VAT_ACCNT, Integer PRF_AP_GL_COA_PTTY_CSH_ACCNT,
		  byte PRF_CM_USE_BNK_FRM, byte PRF_AP_USE_SPPLR_PLLDWN, byte PRF_AR_USE_CSTMR_PLLDWN,
		  byte PRF_AP_AT_GNRT_SPPLR_CODE, String PRF_AP_NXT_SPPLR_CODE, byte PRF_AR_AT_GNRT_CSTMR_CODE,
		  String PRF_AR_NXT_CSTMR_CODE, byte PRF_AP_RFRNC_NMBR_VLDTN, byte PRF_INV_ENBL_POS_INTGRTN, byte PRF_INV_ENBL_POS_AUTO_POST_UP,
		  Integer PRF_INV_POS_ADJSTMNT_ACCNT, String PRF_AP_CHK_VCHR_DT_SRC, Integer PRF_MISC_POS_DSCNT_ACCNT,
		  Integer PRF_MISC_POS_GFT_CRTFCT_ACCNT, Integer PRF_MISC_POS_SRVC_CHRG_ACCNT, Integer PRF_MISC_POS_DN_IN_CHRG_ACCNT,
		  Integer PRF_AR_GL_COA_CSTMR_DPST_ACCNT, String PRF_AP_DFLT_PR_TX, String PRF_AP_DFLT_PR_CRRNCY,
		  String PRF_AR_SLS_INVC_DT_SRC, Integer PRF_INV_GL_COA_VRNC_ACCNT, byte PRF_AR_AUTO_CMPUTE_COGS, double PRF_AR_MNTH_INT_RT,
		  int PRF_AP_AGNG_BCKT, int PRF_AR_AGNG_BCKT,

		  byte PRF_AR_ALLW_PRR_DT, byte PRF_AR_CHK_INSFFCNT_STCK, byte PRF_AR_DTLD_RCVBL,

		  byte PRF_AP_SHW_PR_CST, byte PRF_AR_ENBL_PYMNT_TRM,
		  byte PRF_AR_SO_SLSPRSN_RQRD , byte PRF_AR_INVC_SLSPRSN_RQRD,
		  String PRF_ML_HST, String PRF_ML_SCKT_FCTRY_PRT, String PRF_ML_PRT, String PRF_ML_FRM, byte PRF_ML_AUTH, String PRF_ML_PSSWRD, String PRF_ML_TO, String PRF_ML_CC, String PRF_ML_BCC, String PRF_ML_CNFG,
		  Integer PRF_AD_CMPNY)
      throws CreateException { }

   public void ejbPostCreate (byte PRF_ALLW_SSPNS_PSTNG,
        short PRF_GL_JRNL_LN_NMBR, short PRF_AP_JRNL_LN_NMBR, short PRF_AR_INVC_LN_NMBR,
	      String PRF_AP_W_TX_RLZTN, String PRF_AR_W_TX_RLZTN, byte PRF_ENBL_GL_JRNL_BTCH, byte PRF_ENBL_GL_RCMPT_COA_BLNC,
	      byte PRF_ENBL_AP_VCHR_BTCH, byte PRF_ENBL_AP_PO_BTCH, byte PRF_ENBL_AP_CHCK_BTCH, byte PRF_ENBL_AR_INVC_BTCH, byte PRF_ENBL_AR_INVC_INT_GNRTN,
	      byte PRF_ENBL_AR_RCPT_BTCH, byte PRF_ENBL_AR_MISC_RCPT_BTCH, String PRF_AP_GL_PSTNG_TYP, String PRF_AR_GL_PSTNG_TYP,
		  String PRF_CM_GL_PSTNG_TYP, short PRF_INV_INVNTRY_LN_NMBR, short PRF_INV_QTY_PRCSN_UNT,
		  short PRF_INV_CST_PRCSN_UNT, String PRF_INV_GL_PSTNG_TYP, String PRF_GL_PSTNG_TYP, byte PRF_INV_ENBL_SHFT, byte PRF_ENBL_INV_BUA_BTCH,
		  String PRF_AP_FND_CHCK_DFLT_TYP, String PRF_AR_FND_RCPT_DFLT_TYP, byte PRF_AP_US_ACCRD_VAT,
		  String PRF_AP_DFLT_CHCK_DT, String PRF_AP_DFLT_CHCKR, String PRF_AP_DFLT_APPRVR,
		  Integer PRF_AP_GL_COA_ACCRD_VAT_ACCNT, Integer PRF_AP_GL_COA_PTTY_CSH_ACCNT,
		  byte PRF_CM_USE_BNK_FRM, byte PRF_AP_USE_SPPLR_PLLDWN, byte PRF_AR_USE_CSTMR_PLLDWN,
		  byte PRF_AP_AT_GNRT_SPPLR_CODE, String PRF_AP_NXT_SPPLR_CODE, byte PRF_AR_AT_GNRT_CSTMR_CODE,
		  String PRF_AR_NXT_CSTMR_CODE, byte PRF_AP_RFRNC_NMBR_VLDTN, byte PRF_INV_ENBL_POS_INTGRTN, byte PRF_INV_ENBL_POS_AUTO_POST_UP,
		  Integer PRF_INV_POS_ADJSTMNT_ACCNT, String PRF_AP_CHK_VCHR_DT_SRC, Integer PRF_MISC_POS_DSCNT_ACCNT,
		  Integer PRF_MISC_POS_GFT_CRTFCT_ACCNT, Integer PRF_MISC_POS_SRVC_CHRG_ACCNT, Integer PRF_MISC_POS_DN_IN_CHRG_ACCNT,
		  Integer PRF_AR_GL_COA_CSTMR_DPST_ACCNT, String PRF_AP_DFLT_PR_TX, String PRF_AP_DFLT_PR_CRRNCY,
		  String PRF_AR_SLS_INVC_DT_SRC, Integer PRF_INV_GL_COA_VRNC_ACCNT, byte PRF_AR_AUTO_CMPUTE_COGS, double PRF_AR_MNTH_INT_RT,
		  int PRF_AP_AGNG_BCKT, int PRF_AR_AGNG_BCKT,

		  byte PRF_AR_ALLW_PRR_DT, byte PRF_AR_CHK_INSFFCNT_STCK, byte PRF_AR_DTLD_RCVBL,

		  byte PRF_AP_SHW_PR_CST, byte PRF_AR_ENBL_PYMNT_TRM,
		  byte PRF_AR_SO_SLSPRSN_RQRD , byte PRF_AR_INVC_SLSPRSN_RQRD,
		  String PRF_ML_HST, String PRF_ML_SCKT_FCTRY_PRT, String PRF_ML_PRT, String PRF_ML_FRM, byte PRF_ML_AUTH, String PRF_ML_PSSWRD, String PRF_ML_TO, String PRF_ML_CC, String PRF_ML_BCC, String PRF_ML_CNFG,
		  Integer PRF_AD_CMPNY)
      throws CreateException { }

   public void ejbPostCreate (byte PRF_ALLW_SSPNS_PSTNG,
	        short PRF_GL_JRNL_LN_NMBR, short PRF_AP_JRNL_LN_NMBR, short PRF_AR_INVC_LN_NMBR,
		      String PRF_AP_W_TX_RLZTN, String PRF_AR_W_TX_RLZTN, byte PRF_ENBL_GL_JRNL_BTCH, byte PRF_ENBL_GL_RCMPT_COA_BLNC,
		      byte PRF_ENBL_AP_VCHR_BTCH, byte PRF_ENBL_AP_PO_BTCH, byte PRF_ENBL_AP_CHCK_BTCH, byte PRF_ENBL_AR_INVC_BTCH, byte PRF_ENBL_AR_INVC_INT_GNRTN,
		      byte PRF_ENBL_AR_RCPT_BTCH, byte PRF_ENBL_AR_MISC_RCPT_BTCH, String PRF_AP_GL_PSTNG_TYP, String PRF_AR_GL_PSTNG_TYP,
			  String PRF_CM_GL_PSTNG_TYP, short PRF_INV_INVNTRY_LN_NMBR, short PRF_INV_QTY_PRCSN_UNT,
			  short PRF_INV_CST_PRCSN_UNT, String PRF_INV_GL_PSTNG_TYP, String PRF_GL_PSTNG_TYP, byte PRF_INV_ENBL_SHFT, byte PRF_ENBL_INV_BUA_BTCH,
			  String PRF_AP_FND_CHCK_DFLT_TYP, String PRF_AR_FND_RCPT_DFLT_TYP, byte PRF_AP_US_ACCRD_VAT,
			  String PRF_AP_DFLT_CHCK_DT, String PRF_AP_DFLT_CHCKR, String PRF_AP_DFLT_APPRVR,
			  Integer PRF_AP_GL_COA_ACCRD_VAT_ACCNT, Integer PRF_AP_GL_COA_PTTY_CSH_ACCNT,
			  byte PRF_CM_USE_BNK_FRM, byte PRF_AP_USE_SPPLR_PLLDWN, byte PRF_AR_USE_CSTMR_PLLDWN,
			  byte PRF_AP_AT_GNRT_SPPLR_CODE, String PRF_AP_NXT_SPPLR_CODE, byte PRF_AR_AT_GNRT_CSTMR_CODE,
			  String PRF_AR_NXT_CSTMR_CODE, byte PRF_AP_RFRNC_NMBR_VLDTN, byte PRF_INV_ENBL_POS_INTGRTN, byte PRF_INV_ENBL_POS_AUTO_POST_UP,
			  Integer PRF_INV_POS_ADJSTMNT_ACCNT, String PRF_AP_CHK_VCHR_DT_SRC, Integer PRF_MISC_POS_DSCNT_ACCNT,
			  Integer PRF_MISC_POS_GFT_CRTFCT_ACCNT, Integer PRF_MISC_POS_SRVC_CHRG_ACCNT, Integer PRF_MISC_POS_DN_IN_CHRG_ACCNT,
			  Integer PRF_AR_GL_COA_CSTMR_DPST_ACCNT, String PRF_AP_DFLT_PR_TX, String PRF_AP_DFLT_PR_CRRNCY,
			  String PRF_AR_SLS_INVC_DT_SRC, Integer PRF_INV_GL_COA_VRNC_ACCNT, byte PRF_AR_AUTO_CMPUTE_COGS, double PRF_AR_MNTH_INT_RT,
			  int PRF_AP_AGNG_BCKT, int PRF_AR_AGNG_BCKT,

			  byte PRF_AR_ALLW_PRR_DT, byte PRF_AR_CHK_INSFFCNT_STCK, byte PRF_AR_DTLD_RCVBL,

			  byte PRF_AP_SHW_PR_CST, byte PRF_AR_ENBL_PYMNT_TRM,
			  byte PRF_AR_DSBL_SLS_PRC, byte PRF_INV_IL_SHW_ALL, byte PRF_INV_IL_ADD_BY_ITM_LST, byte PRF_AP_DM_OVRRD_CST, byte PRF_GL_YR_END_CLS_RSTRCTN,
			  byte PRF_AD_DSBL_MTPL_LGN, byte PRF_AD_ENBL_EML_NTFCTN,
			  byte PRF_AR_SO_SLSPRSN_RQRD , byte PRF_AR_INVC_SLSPRSN_RQRD,
			  String PRF_ML_HST, String PRF_ML_SCKT_FCTRY_PRT, String PRF_ML_PRT, String PRF_ML_FRM, byte PRF_ML_AUTH, String PRF_ML_PSSWRD,  String PRF_ML_TO, String PRF_ML_CC, String PRF_ML_BCC, String PRF_ML_CNFG,
			  Integer PRF_AD_CMPNY)
	      throws CreateException { }



} // AdPreferenceBean class
