/*
 * com/ejb/ad/LocalAdResponsibilityBean.java
 *
 * Created on June 16, 2003, 02:33 PM
 */

package com.ejb.ad;

import java.util.Collection;
import java.util.Date;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;

import com.util.AbstractEntityBean;
import com.util.Debug;

/**
 *
 * @author  Enrico C. Yap
 *
 * @ejb:bean name="AdResponsibilityEJB"
 *           display-name="Responsibility Entity"
 *           type="CMP"
 *           cmp-version="2.x"
 *           view-type="local"
 *           local-jndi-name="omega-ejb/AdResponsibilityEJB"
 *           schema="AdResponsibility"
 *           primkey-field="rsCode"
 *
 * @ejb:pk class="java.lang.Integer"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="aduser"
 *                        role-link="aduserlink"
 *
 * @ejb:permission role-name="aduser"
 *
 * @ejb:interface local-class="com.ejb.ad.LocalAdResponsibility"
 *                local-extends="javax.ejb.EJBLocalObject"
 *
 * @ejb:home local-class="com.ejb.ad.LocalAdResponsibilityHome"
 *           local-extends="javax.ejb.EJBLocalHome"
 *
 * @ejb:finder signature="Collection findRsAll(java.lang.Integer RS_AD_CMPNY)"
 *             query="SELECT OBJECT(rs) FROM AdResponsibility rs WHERE rs.rsAdCompany = ?1"
 *
 * @jboss:query signature="Collection findRsAll(java.lang.Integer RS_AD_CMPNY)"
 *             query="SELECT OBJECT(rs) FROM AdResponsibility rs WHERE rs.rsAdCompany = ?1 ORDER BY rs.rsName"
 *
 * @ejb:finder signature="LocalAdResponsibility findByRsName(java.lang.String RS_NM, java.lang.Integer RS_AD_CMPNY)"
 *             query="SELECT OBJECT(rs) FROM AdResponsibility rs WHERE rs.rsName = ?1 AND rs.rsAdCompany = ?2"
 *
 * @jboss:query signature="Collection ejbSelectGeneric(java.lang.String jbossQl, java.lang.Object[] args)"
 *                         dynamic="true"
 * 
 * @ejb:value-object match="*"
 *             name="AdResponsibility"
 *
 * @jboss:persistence table-name="AD_RSPNSBLTY"
 *
 * @jboss:entity-command name="mysql-get-generated-keys"
 */

public abstract class AdResponsibilityBean extends AbstractEntityBean {
    
    
    // PERSISTENT METHODS
    
    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     * @ejb:pk-field
     *
     * @jboss:column-name name="RS_CODE"
     *
     * @jboss:persistence auto-increment="true"
     **/
    public abstract Integer getRsCode();
    public abstract void setRsCode(Integer RS_CODE);
    
    
    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="RS_NM"
     **/
     public abstract String getRsName();
    /**
     * @ejb:interface-method view-type="local"
     **/     
     public abstract void setRsName(String RS_NM);
     
    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="RS_DESC"
     **/
     public abstract String getRsDescription();
    /**
     * @ejb:interface-method view-type="local"
     **/     
     public abstract void setRsDescription(String RS_DESC);
     
    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="RS_DT_FRM"
     **/
     public abstract Date getRsDateFrom();
    /**
     * @ejb:interface-method view-type="local"
     **/     
     public abstract void setRsDateFrom(Date RS_DT_FRM);          
     
    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="RS_DT_TO"
     **/
     public abstract Date getRsDateTo();
    /**
     * @ejb:interface-method view-type="local"
     **/     
     public abstract void setRsDateTo(Date RS_DT_TO); 
     
     /**
      * @ejb:persistent-field
      * @ejb:interface-method view-type="local"
      *
      * @jboss:column-name name="RS_AD_CMPNY"
      **/
      public abstract Integer getRsAdCompany();
     /**
      * @ejb:interface-method view-type="local"
      **/     
      public abstract void setRsAdCompany(Integer RS_AD_CMPNY); 
     
     
    // RELATIONSHIP METHODS    
     
    /**
     * @ejb:interface-method view-type="local"
     * @ejb:relation name="adresponsibility-formfunctionresponsibilities"
     *               role-name="adresponsibility-has-many-formfunctionresponsibilities)"
     */
    public abstract Collection getAdFormFunctionResponsibilities();
    public abstract void setAdFormFunctionResponsibilities(Collection adFormFunctionResponsibilities);
    
    /**
     * @ejb:interface-method view-type="local"
     * @ejb:relation name="adresponsibility-userresponsibilities"
     *               role-name="adresponsibility-has-many-userresponsibilities)"
     */
    public abstract Collection getAdUserResponsibilities();
    public abstract void setAdUserResponsibilities(Collection adUserResponsibilities);    
     
    /**
     * @ejb:interface-method view-type="local"
     * @ejb:relation name="application-adresponsibilities"
     *               role-name="adresponsibility-has-one-application"
     *
     * @jboss:relation related-pk-field="appCode"
     *                 fk-column="RS_APP_CODE"
     */
    public abstract com.ejb.gl.LocalAdApplication getAdApplication();
    public abstract void setAdApplication(com.ejb.gl.LocalAdApplication adApplication);  
    
    /**
     * @ejb:interface-method view-type="local"
     * @ejb:relation name="responsibility-branchresponsibility"
     *               role-name="responsibility-has-many-branchresponsibility"
     */
    public abstract Collection getAdBranchResponsibilities();
    public abstract void setAdBranchResponsibilities(Collection adBranchResponsibilities); 
    
    /**
     * @jboss:dynamic-ql
     */
    public abstract Collection ejbSelectGeneric(java.lang.String jbossQl, java.lang.Object[] args)
    throws FinderException;     
    
    // BUSINESS METHODS
    
    /**
     * @ejb:home-method view-type="local"
     */
    public Collection ejbHomeGetRsByCriteria(java.lang.String jbossQl, java.lang.Object[] args)
    throws FinderException {
    	
    	return ejbSelectGeneric(jbossQl, args);
    	
    }   
   
    /**
     * @ejb:interface-method view-type="local"
     **/
    public void addAdFormFunctionResponsibility(LocalAdFormFunctionResponsibility adFormFunctionResponsibility) {

       Debug.print("AdResponsibilityBean addAdFormFunctionResponsibility");
      
       try {
      	
          Collection adFormFunctionResponsibilities = getAdFormFunctionResponsibilities();
	      adFormFunctionResponsibilities.add(adFormFunctionResponsibility);
	     
       } catch (Exception ex) {
      	
          throw new EJBException(ex.getMessage());
         
       }
        
    }

    /**
     * @ejb:interface-method view-type="local"
     **/
    public void dropAdFormFunctionResponsibility(LocalAdFormFunctionResponsibility adFormFunctionResponsibility) {

      Debug.print("AdResponsibilityBean dropAdFormFunctionResponsibility");
      
      try {
      	
         Collection adFormFunctionResponsibilities = getAdFormFunctionResponsibilities();
	     adFormFunctionResponsibilities.remove(adFormFunctionResponsibility);
	     
      } catch (Exception ex) {
      	
         throw new EJBException(ex.getMessage());
         
      }
      
    }
    
    /**
     * @ejb:interface-method view-type="local"
     **/
    public void addAdUserResponsibility(LocalAdUserResponsibility adUserResponsibility) {

       Debug.print("AdResponsibilityBean addAdUserResponsibility");
      
       try {
      	
          Collection adUserResponsibilities = getAdUserResponsibilities();
	      adUserResponsibilities.add(adUserResponsibility);
	     
       } catch (Exception ex) {
      	
          throw new EJBException(ex.getMessage());
         
       }
        
    }

    /**
     * @ejb:interface-method view-type="local"
     **/
    public void dropAdUserResponsibility(LocalAdUserResponsibility adUserResponsibility) {

      Debug.print("AdResponsibilityBean dropAdUserResponsibility");
      
      try {
      	
         Collection adUserResponsibilities = getAdUserResponsibilities();
	     adUserResponsibilities.remove(adUserResponsibility);
	     
      } catch (Exception ex) {
      	
         throw new EJBException(ex.getMessage());
         
      }
      
    }
    
    /**
     * @ejb:interface-method view-type="local"
     **/
    public void addAdBranchResponsibility(LocalAdBranchResponsibility adBranchResponsibility) {

         Debug.print("AdResponsibilityBean addAdBranchResponsibility");
   
         try {
   	
   	        Collection adBranchResponsibilities = getAdBranchResponsibilities();
   	        adBranchResponsibilities.add(adBranchResponsibility);
 	     
         } catch (Exception ex) {
   	
             throw new EJBException(ex.getMessage());
      
         }
       
    }
 	
    /**
     * @ejb:interface-method view-type="local"
     **/ 
     public void dropAdBranchResponsibility(LocalAdBranchResponsibility adBranchResponsibility) {
 		
 		  Debug.print("AdResponsibilityBean dropAdBranchResponsibility");
 		  
 		  try {
 		  	
 		     Collection adBranchResponsibilities = getAdBranchResponsibilities();
 		     adBranchResponsibilities.remove(adBranchResponsibility);
 			     
 		  } catch (Exception ex) {
 		  	
 		     throw new EJBException(ex.getMessage());
 		     
 		  }
 		  
    }
   
    // ENTITY METHODS
    
    /**
     * @ejb:create-method view-type="local"
     **/
    public Integer ejbCreate(Integer RS_CODE, String RS_NM, String RS_DESC, Date RS_DT_FRM, Date RS_DT_TO,
    	Integer RS_AD_CMPNY)
        throws CreateException {
           
        Debug.print("AdResponsibilityBean ejbCreate");
        
        setRsCode(RS_CODE);
        setRsName(RS_NM);
        setRsDescription(RS_DESC);
        setRsDateFrom(RS_DT_FRM);
        setRsDateTo(RS_DT_TO);
        setRsAdCompany(RS_AD_CMPNY);
        
        return null;
        
    }
    
    /**
     * @ejb:create-method view-type="local"
     **/
    public Integer ejbCreate(String RS_NM, String RS_DESC, Date RS_DT_FRM, Date RS_DT_TO,
    	Integer RS_AD_CMPNY)
        throws CreateException {
           
        Debug.print("AdResponsibilityBean ejbCreate");
       
        setRsName(RS_NM);
        setRsDescription(RS_DESC);
        setRsDateFrom(RS_DT_FRM);
        setRsDateTo(RS_DT_TO);
        setRsAdCompany(RS_AD_CMPNY);
        
        return null;
        
    }
    
    
    public void ejbPostCreate(Integer RS_CODE, String RS_NM, String RS_DESC, Date RS_DT_FRM, Date RS_DT_TO,
    	Integer RS_AD_CMPNY)
        throws CreateException { }
   
    public void ejbPostCreate(String RS_NM, String RS_DESC, Date RS_DT_FRM, Date RS_DT_TO,
    	Integer RS_AD_CMPNY)
        throws CreateException { }
        
}

