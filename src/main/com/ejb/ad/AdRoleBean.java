/*
 * com/ejb/ar/LocalAdRoleBean.java
 *
 * Created on Aug 04, 2003, 8:31 PM
 */
 
package com.ejb.ad;

import javax.ejb.CreateException;

import com.util.AbstractEntityBean;
import com.util.Debug;

/**
 *
 * @author  Dennis Hilario
 *
 * @ejb:bean name="AdRoleEJB"
 *           display-name="Role Entity"
 *           type="CMP"
 *           cmp-version="2.x"
 *           view-type="local"
 *           local-jndi-name="omega-ejb/AdRoleEJB"
 *           schema="AdRole"
 *           primkey-field="rlCode"
 *
 * @ejb:pk class="java.lang.Integer"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="aduser"
 *                        role-link="aduserlink"
 *
 * @ejb:permission role-name="aduser"
 *
 * @ejb:interface local-class="com.ejb.ad.LocalAdRole"
 *                local-extends="javax.ejb.EJBLocalObject"
 *
 * @ejb:home local-class="com.ejb.ad.LocalAdRoleHome"
 *           local-extends="javax.ejb.EJBLocalHome"
 *
 * @ejb:value-object match="*"
 *             name="AdRole"
 *
 * @jboss:persistence table-name="AD_ROLE"
 *
 * @jboss:entity-command name="mysql-get-generated-keys"
 */

public abstract class AdRoleBean extends AbstractEntityBean {
    
   
    // PERSISTENT METHODS
    
    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     * @ejb:pk-field
     *
     * @jboss:column-name name="RL_CODE"
     *
     * @jboss:persistence auto-increment="true"
     **/
    public abstract Integer getRlCode();
    public abstract void setRlCode(Integer RL_CODE);
    
    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="RL_USR_NM"
     **/
    public abstract String getRlUserName();
     /**
      * @ejb:interface-method view-type="local"
      **/  
    public abstract void setRlUserName(String RL_USR_NM);
    
    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="RL_RL_NM"
     **/
    public abstract String getRlRoleName();
     /**
      * @ejb:interface-method view-type="local"
      **/  
    public abstract void setRlRoleName(String RL_RL_NM);      
    
    // ENTITY METHODS
    
    /**
     * @ejb:create-method view-type="local"
     **/
     public Integer ejbCreate(Integer RL_CODE, String RL_USR_NM, String RL_RL_NM)
         throws CreateException {
      	
         Debug.print("AdRoleBean ejbCreate");
      
         setRlCode(RL_CODE);
         setRlUserName(RL_USR_NM);
         setRlRoleName(RL_RL_NM);
        
         return null;
    }
    
    /**
     * @ejb:create-method view-type="local"
     **/
     public Integer ejbCreate(
         String RL_USR_NM, String RL_RL_NM)
         throws CreateException {
      	
         Debug.print("AdRoleBean ejbCreate");
         
         setRlUserName(RL_USR_NM);
         setRlRoleName(RL_RL_NM);
        
         return null;
     }
    
     public void ejbPostCreate(Integer RL_CODE, String RL_USR_NM, String RL_RL_NM)      
         throws CreateException { }
      
     public void ejbPostCreate(
         String RL_USR_NM, String RL_RL_NM)
         throws CreateException { }     
}

      
      	
      	