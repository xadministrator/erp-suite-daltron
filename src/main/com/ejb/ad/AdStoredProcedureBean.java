/*
 * com/ejb/ap/LocalAdStoredProcedureBean.java
 *
 * Created on March 19, 2004, 2:56 PM
 */

package com.ejb.ad;

import javax.ejb.CreateException;

import com.util.AbstractEntityBean;
import com.util.Debug;

/**
 *
 * @author  Clint Arrogante
 *
 * @ejb:bean name="AdStoredProcedureEJB"
 *           display-name="Store Procedure Entity"
 *           type="CMP"
 *           cmp-version="2.x"
 *           view-type="local"
 *           local-jndi-name="omega-ejb/AdStoredProcedureEJB"
 *           schema="AdStoredProcedure"
 *           primkey-field="spCode"
 *
 * @ejb:pk class="java.lang.Integer"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="aduser"
 *                        role-link="aduserlink"
 *
 * @ejb:permission role-name="aduser"
 *
 * @ejb:interface local-class="com.ejb.ad.LocalAdStoredProcedure"
 *                local-extends="javax.ejb.EJBLocalObject"
 *
 * @ejb:home local-class="com.ejb.ad.LocalAdStoredProcedureHome"
 *           local-extends="javax.ejb.EJBLocalHome"
 * 
 * @ejb:finder signature="LocalAdStoredProcedure findBySpAdCompany(java.lang.Integer SP_AD_CMPNY)"
 *             query="SELECT OBJECT(sp) FROM AdStoredProcedure sp WHERE sp.spAdCompany = ?1"
 *
 * @ejb:value-object match="*"
 *             name="AdStoredProcedure"
 *
 * @jboss:persistence table-name="AD_STRD_PRC"
 *
 * @jboss:entity-command name="mysql-get-generated-keys"
 */

public abstract class AdStoredProcedureBean extends AbstractEntityBean {
    
    
    // PERSITCENT METHODS
    
    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     * @ejb:pk-field
     *
     * @jboss:column-name name="SP_CODE"
     *
     * @jboss:persistence auto-increment="true"
     **/
    public abstract Integer getSpCode();
    public abstract void setSpCode(Integer SP_CODE);
    
    
    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"   
     *
     * @jboss:column-name name="SP_ENBL_GL_RPT"
     **/
    public abstract byte getSpEnableGlGeneralLedgerReport();
     /**
      * @ejb:interface-method view-type="local"
      **/ 
    public abstract void setSpEnableGlGeneralLedgerReport(byte SP_ENBL_GL_RPT);
    
    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"   
     *
     * @jboss:column-name name="SP_NM_GL_RPT"
     **/
    public abstract String getSpNameGlGeneralLedgerReport();
     /**
      * @ejb:interface-method view-type="local"
      **/ 
    public abstract void setSpNameGlGeneralLedgerReport(String SP_NM_GL_RPT);
    
    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"   
     *
     * @jboss:column-name name="SP_ENBL_TB_RPT"
     **/
    public abstract byte getSpEnableGlTrialBalanceReport();
     /**
      * @ejb:interface-method view-type="local"
      **/ 
    public abstract void setSpEnableGlTrialBalanceReport(byte SP_ENBL_TB_RPT);
    
    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"   
     *
     * @jboss:column-name name="SP_NM_TB_RPT"
     **/
    public abstract String getSpNameGlTrialBalanceReport();
     /**
      * @ejb:interface-method view-type="local"
      **/ 
    public abstract void setSpNameGlTrialBalanceReport(String SP_NM_TB_RPT);
    
    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"   
     *
     * @jboss:column-name name="SP_ENBL_IS_RPT"
     **/
    public abstract byte getSpEnableGlIncomeStatementReport();
     /**
      * @ejb:interface-method view-type="local"
      **/ 
    public abstract void setSpEnableGlIncomeStatementReport(byte SP_ENBL_IS_RPT);
    
    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"   
     *
     * @jboss:column-name name="SP_NM_IS_RPT"
     **/
    public abstract String getSpNameGlIncomeStatementReport();
     /**
      * @ejb:interface-method view-type="local"
      **/ 
    public abstract void setSpNameGlIncomeStatementReport(String SP_NM_IS_RPT);
    
    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"   
     *
     * @jboss:column-name name="SP_ENBL_BS_RPT"
     **/
    public abstract byte getSpEnableGlBalanceSheetReport();
     /**
      * @ejb:interface-method view-type="local"
      **/ 
    public abstract void setSpEnableGlBalanceSheetReport(byte SP_ENBL_BS_RPT);
    
    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"   
     *
     * @jboss:column-name name="SP_NM_BS_RPT"
     **/
    public abstract String getSpNameGlBalanceSheetReport();
     /**
      * @ejb:interface-method view-type="local"
      **/ 
    public abstract void setSpNameGlBalanceSheetReport(String SP_NM_BS_RPT);
    
    
    
    
    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"   
     *
     * @jboss:column-name name="SP_ENBL_SOA_RPT"
     **/
    public abstract byte getSpEnableArStatementOfAccountReport();
     /**
      * @ejb:interface-method view-type="local"
      **/ 
    public abstract void setSpEnableArStatementOfAccountReport(byte SP_ENBL_SOA_RPT);
    
    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"   
     *
     * @jboss:column-name name="SP_NM_SOA_RPT"
     **/
    public abstract String getSpNameArStatementOfAccountReport();
     /**
      * @ejb:interface-method view-type="local"
      **/ 
    public abstract void setSpNameArStatementOfAccountReport(String SP_NM_SOA_RPT);
    
    
    

    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"   
     *
     * @jboss:column-name name="SP_AD_CMPNY"
     **/
    public abstract Integer getSpAdCompany();
     /**
      * @ejb:interface-method view-type="local"
      **/ 
    public abstract void setSpAdCompany(Integer SP_AD_CMPNY);
    
    
   
    // NO RELATIONSHIP METHODS


    // NO BUSINESS METHODS
     

    // ENTITY METHODS
    
    /**
     * @ejb:create-method view-type="local"
     **/
    public Integer ejbCreate(Integer SP_CODE, 
            byte SP_ENBL_GL_RPT,
            String SP_NM_GL_RPT,
            byte SP_ENBL_TB_RPT,
            String SP_NM_TB_RPT,
            byte SP_ENBL_IS_RPT,
            String SP_NM_IS_RPT,
            byte SP_ENBL_BS_RPT,
            String SP_NM_BS_RPT,
            byte SP_ENBL_SOA_RPT,
            String SP_NM_SOA_RPT,

         Integer SP_AD_CMPNY)     	
         throws CreateException {	
     	
         Debug.print("AdStoredProcedure ejbCreate");
     
	     setSpCode(SP_CODE);
	     setSpEnableGlGeneralLedgerReport(SP_ENBL_GL_RPT);
         setSpNameGlGeneralLedgerReport(SP_NM_GL_RPT);
         setSpEnableGlTrialBalanceReport(SP_ENBL_TB_RPT);
         setSpNameGlTrialBalanceReport(SP_NM_TB_RPT);
         setSpEnableGlIncomeStatementReport(SP_ENBL_IS_RPT);
         setSpNameGlIncomeStatementReport(SP_NM_IS_RPT);
         setSpEnableGlBalanceSheetReport(SP_ENBL_BS_RPT);
         setSpNameGlBalanceSheetReport(SP_NM_BS_RPT);
          setSpEnableArStatementOfAccountReport(SP_ENBL_SOA_RPT);
         setSpNameArStatementOfAccountReport(SP_NM_SOA_RPT);

	     setSpAdCompany(SP_AD_CMPNY);

         return null;
     }
     
    /**
     * @ejb:create-method view-type="local"
     **/
    public Integer ejbCreate(
            byte SP_ENBL_GL_RPT,
            String SP_NM_GL_RPT,
            byte SP_ENBL_TB_RPT,
            String SP_NM_TB_RPT,
            byte SP_ENBL_IS_RPT,
            String SP_NM_IS_RPT,
            byte SP_ENBL_BS_RPT,
            String SP_NM_BS_RPT,
            byte SP_ENBL_SOA_RPT,
            String SP_NM_SOA_RPT,
         Integer SP_AD_CMPNY)     	
         throws CreateException {	
     	
         Debug.print("AdStoredProcedure ejbCreate");
     
         setSpEnableGlGeneralLedgerReport(SP_ENBL_GL_RPT);
         setSpNameGlGeneralLedgerReport(SP_NM_GL_RPT);
         setSpEnableGlTrialBalanceReport(SP_ENBL_TB_RPT);
         setSpNameGlTrialBalanceReport(SP_NM_TB_RPT);
         setSpEnableGlIncomeStatementReport(SP_ENBL_IS_RPT);
         setSpNameGlIncomeStatementReport(SP_NM_IS_RPT);
         setSpEnableGlBalanceSheetReport(SP_ENBL_BS_RPT);
         setSpNameGlBalanceSheetReport(SP_NM_BS_RPT);
         setSpEnableArStatementOfAccountReport(SP_ENBL_SOA_RPT);
         setSpNameArStatementOfAccountReport(SP_NM_SOA_RPT);
	     setSpAdCompany(SP_AD_CMPNY);

         return null;
     }
     
     public void ejbPostCreate(Integer SP_CODE, 
             byte SP_ENBL_GL_RPT,
             String SP_NM_GL_RPT,
             byte SP_ENBL_TB_RPT,
             String SP_NM_TB_RPT,
             byte SP_ENBL_IS_RPT,
             String SP_NM_IS_RPT,
             byte SP_ENBL_BS_RPT,
             String SP_NM_BS_RPT,
             byte SP_ENBL_SOA_RPT,
            String SP_NM_SOA_RPT,
		 Integer SP_AD_CMPNY)
         throws CreateException { }
      
     public void ejbPostCreate( 
             byte SP_ENBL_GL_RPT,
             String SP_NM_GL_RPT,
             byte SP_ENBL_TB_RPT,
             String SP_NM_TB_RPT,
             byte SP_ENBL_IS_RPT,
             String SP_NM_IS_RPT,
             byte SP_ENBL_BS_RPT,
             String SP_NM_BS_RPT,
             byte SP_ENBL_SOA_RPT,
            String SP_NM_SOA_RPT,
		 Integer APR_AD_CMPNY)
         throws CreateException { }     
}