/*
 * com/ejb/ar/LocalAdUserBean.java
 *
 * Created on June 16, 2003, 1:17 PM
 */
 
package com.ejb.ad;

import java.util.Collection;
import java.util.Date;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;

import com.util.AbstractEntityBean;
import com.util.Debug;

import com.ejb.gl.LocalGlUserStaticReport;
import com.ejb.hr.LocalHrEmployee;
import com.ejb.pm.LocalPmUser;

/**
 *
 * @author  Neil Andrew M. Ajero
 *
 * @ejb:bean name="AdUserEJB"
 *           display-name="User Entity"
 *           type="CMP"
 *           cmp-version="2.x"
 *           view-type="local"
 *           local-jndi-name="omega-ejb/AdUserEJB"
 *           schema="AdUser"
 *           primkey-field="usrCode"
 *
 * @ejb:pk class="java.lang.Integer"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="aduser"
 *                        role-link="aduserlink"
 *
 * @ejb:permission role-name="aduser"
 *
 * @ejb:interface local-class="com.ejb.ad.LocalAdUser"
 *                local-extends="javax.ejb.EJBLocalObject"
 *
 * @ejb:home local-class="com.ejb.ad.LocalAdUserHome"
 *           local-extends="javax.ejb.EJBLocalHome"
 *
 * @ejb:finder signature="Collection findUsrAll(java.lang.Integer USR_AD_CMPNY)"
 *             query="SELECT OBJECT(usr) FROM AdUser usr WHERE usr.usrAdCompany = ?1"
 *
 * @jboss:query signature="Collection findUsrAll(java.lang.Integer USR_AD_CMPNY)"
 *             query="SELECT OBJECT(usr) FROM AdUser usr WHERE usr.usrAdCompany = ?1 ORDER BY usr.usrName"
 *              
 * @ejb:finder signature="Collection findUsrInspectorAll(byte USR_INSPCTR, java.lang.Integer USR_AD_CMPNY)"
 *             query="SELECT OBJECT(usr) FROM AdUser usr WHERE usr.usrAdCompany = ?1 AND usr.usrInspector = ?2"
 *             
 * @ejb:finder signature="Collection findUsrByDepartmentHead(java.lang.String USR_DEPT, byte USR_HEAD, java.lang.Integer USR_AD_CMPNY)"
 *             query="SELECT OBJECT(usr) FROM AdUser usr WHERE usr.usrDept = ?1 AND usr.usrHead = ?2 AND usr.usrAdCompany = ?3"
 *
 * @ejb:finder signature="Collection findUsrByDepartment(java.lang.String USR_DEPT, java.lang.Integer USR_AD_CMPNY)"
 *             query="SELECT OBJECT(usr) FROM AdUser usr WHERE usr.usrDept = ?1 AND usr.usrAdCompany = ?2"
 *
 * @jboss:query signature="Collection findUsrByDepartmentHead(java.lang.String USR_DEPT, byte USR_HEAD, java.lang.Integer USR_AD_CMPNY)"
 *             query="SELECT OBJECT(usr) FROM AdUser usr WHERE usr.usrDept = ?1 AND usr.usrHead = ?2 AND usr.usrAdCompany = ?3"
 *
 * @ejb:finder signature="LocalAdUser findByUsrName(java.lang.String USR_NM, java.lang.Integer USR_AD_CMPNY)"
 *             query="SELECT OBJECT(usr) FROM AdUser usr WHERE usr.usrName = ?1 AND usr.usrAdCompany = ?2"
 *             
 * @ejb:finder signature="LocalAdUser findByUsrPmUsrReferenceID(java.lang.Integer USR_PM_USR_ID, java.lang.Integer USR_AD_CMPNY)"
 *             query="SELECT OBJECT(usr) FROM AdUser usr WHERE usr.pmUser.usrReferenceID = ?1 AND usr.usrAdCompany = ?2"
 *
 * @jboss:query signature="Collection ejbSelectGeneric(java.lang.String jbossQl, java.lang.Object[] args)"
 *                         dynamic="true"
 * 
 * @ejb:value-object match="*"
 *             name="AdUser"
 *
 * @jboss:persistence table-name="AD_USR"
 *
 * @jboss:entity-command name="mysql-get-generated-keys"
 */

public abstract class AdUserBean extends AbstractEntityBean {
    
   
    // PERSISTENT METHODS
    
    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     * @ejb:pk-field
     *
     * @jboss:column-name name="USR_CODE"
     *
     * @jboss:persistence auto-increment="true"
     **/
    public abstract Integer getUsrCode();
    public abstract void setUsrCode(Integer USR_CODE);
    
    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="USR_NM"
     **/
    public abstract String getUsrName();
     /**
      * @ejb:interface-method view-type="local"
      **/  
    public abstract void setUsrName(String USR_NM);
    
    
    
    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="USR_DEPT"
     **/
    public abstract String getUsrDept();
     /**
      * @ejb:interface-method view-type="local"
      **/  
    public abstract void setUsrDept(String USR_DEPT);
    
        
    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="USR_DESC"
     **/
    public abstract String getUsrDescription();
     /**
      * @ejb:interface-method view-type="local"
      **/  
    public abstract void setUsrDescription(String USR_DESC);
    
    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="USR_PSTN"
     **/
    public abstract String getUsrPosition();
     /**
      * @ejb:interface-method view-type="local"
      **/  
    public abstract void setUsrPosition(String USR_PSTN);
    
    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="USR_EML_ADDRSS"
     **/
    public abstract String getUsrEmailAddress();
     /**
      * @ejb:interface-method view-type="local"
      **/  
    public abstract void setUsrEmailAddress(String USR_EML_ADDRSS);
    
    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"   
     *
     * @jboss:column-name name="USR_HEAD"
     **/
    public abstract byte getUsrHead();
     /**
      * @ejb:interface-method view-type="local"
      **/ 
    public abstract void setUsrHead(byte USR_HEAD);
    
    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"   
     *
     * @jboss:column-name name="USR_INSPCTR"
     **/
    public abstract byte getUsrInspector();
     /**
      * @ejb:interface-method view-type="local"
      **/ 
    public abstract void setUsrInspector(byte USR_INSPCTR);
    
    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="USR_PSSWRD"
     **/
    public abstract String getUsrPassword();
     /**
      * @ejb:interface-method view-type="local"
      **/  
    public abstract void setUsrPassword(String USR_PSSWRD);

    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="USR_PSSWRD_EXPRTN_CODE"
     **/
    public abstract short getUsrPasswordExpirationCode();
     /**
      * @ejb:interface-method view-type="local"
      **/  
    public abstract void setUsrPasswordExpirationCode(short USR_PSSWRD_EXPRTN_CODE);

    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="USR_PSSWRD_EXPRTN_DYS"
     **/
    public abstract short getUsrPasswordExpirationDays();
     /**
      * @ejb:interface-method view-type="local"
      **/  
    public abstract void setUsrPasswordExpirationDays(short USR_PSSWRD_EXPRTN_DYS);

    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="USR_PSSWRD_EXPRTN_ACCSS"
     **/
    public abstract short getUsrPasswordExpirationAccess();
     /**
      * @ejb:interface-method view-type="local"
      **/  
    public abstract void setUsrPasswordExpirationAccess(short USR_PSSWRD_EXPRTN_ACCSS);
    
    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="USR_PO_APPRVL_CTR"
     **/
    public abstract String getUsrPurchaseOrderApprovalCounter();
    
     /**
      * @ejb:interface-method view-type="local"
      **/  
    public abstract void setUsrPurchaseOrderApprovalCounter(String USR_PO_APPRVL_CTR);
    
    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="USR_PSSWRD_CURR_ACCSS"
     **/
    public abstract short getUsrPasswordCurrentAccess();
     /**
      * @ejb:interface-method view-type="local"
      **/  
    public abstract void setUsrPasswordCurrentAccess(short USR_PSSWRD_CURR_ACCSS);

    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="USR_DT_FRM"
     **/
    public abstract Date getUsrDateFrom();
     /**
      * @ejb:interface-method view-type="local"
      **/  
    public abstract void setUsrDateFrom(Date USR_DT_FRM);

    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="USR_DT_TO"
     **/
    public abstract Date getUsrDateTo();
     /**
      * @ejb:interface-method view-type="local"
      **/  
    public abstract void setUsrDateTo(Date USR_DT_TO);
    
    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="USR_AD_CMPNY"
     **/
    public abstract Integer getUsrAdCompany();
     /**
      * @ejb:interface-method view-type="local"
      **/  
    public abstract void setUsrAdCompany(Integer USR_AD_CMPNY);

    
    // RELATIONSHIP METHODS
    
    
    /**
	 * @ejb:interface-method view-type="local"
	 * @ejb:relation name="pmuser-adusers"
	 *               role-name="aduser-has-one-pmuser"
	 *
	 * @jboss:relation related-pk-field="usrCode"
	 *                 fk-column="PM_USER"
	 */
	public abstract LocalPmUser getPmUser();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setPmUser(LocalPmUser pmUser);
    
    
    
    /**
     * @ejb:interface-method view-type="local"
     * @ejb:relation name="aduser-userresponsibilities"
     *               role-name="aduser-has-many-userresponsibilities"
     * 
     */
    public abstract Collection getAdUserResponsibilities();
    public abstract void setAdUserResponsibilities(Collection adUserResponsibilities); 

    /**
     * @ejb:interface-method view-type="local"
     * @ejb:relation name="aduser-approvalusers"
     *               role-name="aduser-has-many-approvalusers"
     * 
     */
    public abstract Collection getAdApprovalUsers();
    public abstract void setAdApprovalUsers(Collection adApprovalUsers); 
    
    /**
     * @ejb:interface-method view-type="local"
     * @ejb:relation name="aduser-approvalqueues"
     *               role-name="aduser-has-many-approvalqueues"
     * 
     */
    public abstract Collection getAdApprovalQueues();
    public abstract void setAdApprovalQueues(Collection adApprovalQueues); 
     
    /**
     * @ejb:interface-method view-type="local"
     * @ejb:relation name="user-userstaticreports"
     *               role-name="user-has-many-userstaticreports"
     */
    public abstract Collection getGlUserStaticReports();
    public abstract void setGlUserStaticReports(Collection glUserStaticReports);
    
    /**
     * @ejb:interface-method view-type="local"
     * @ejb:relation name="user-tags"
     *               role-name="user-has-many-tags"
     *               
     */
    public abstract Collection getInvTag();
    public abstract void setInvTag(Collection invTag);

    /**
     * @jboss:dynamic-ql
     */
    public abstract Collection ejbSelectGeneric(java.lang.String jbossQl, java.lang.Object[] args)
    throws FinderException;     
    
    // BUSINESS METHODS
    
    /**
     * @ejb:home-method view-type="local"
     */
    public Collection ejbHomeGetUsrByCriteria(java.lang.String jbossQl, java.lang.Object[] args)
    throws FinderException {
    	
    	return ejbSelectGeneric(jbossQl, args);
    	
    }   
     
    /**
     * @ejb:interface-method view-type="local"
     **/
     public void addAdUserResponsibility(LocalAdUserResponsibility adUserResponsibility) {

         Debug.print("AdUserBean addAdUserResponsibility");
      
         try {
      	
            Collection adUserResponsibilities = getAdUserResponsibilities();
	        adUserResponsibilities.add(adUserResponsibility);
	         
         } catch (Exception ex) {
      	
           throw new EJBException(ex.getMessage());
      
         }   
     }

    /**
     * @ejb:interface-method view-type="local"
     **/
     public void dropAdUserResponsibility(LocalAdUserResponsibility adUserResponsibility) {

         Debug.print("AdUserBean dropAdUserResponsibility");
      
         try {
      	
            Collection adUserResponsibilities = getAdUserResponsibilities();
	        adUserResponsibilities.remove(adUserResponsibility);
	      
         } catch (Exception ex) {
      	
           throw new EJBException(ex.getMessage());
         
         }   
     }
     
     /**
     * @ejb:interface-method view-type="local"
     **/
     public void addAdApprovalUser(LocalAdApprovalUser adApprovalUser) {

         Debug.print("AdUserBean addAdApprovalUser");
      
         try {
      	
            Collection adApprovalUsers = getAdApprovalUsers();
	        adApprovalUsers.add(adApprovalUser);
	         
         } catch (Exception ex) {
      	
           throw new EJBException(ex.getMessage());
      
         }   
     }

    /**
     * @ejb:interface-method view-type="local"
     **/
     public void dropAdApprovalUser(LocalAdApprovalUser adApprovalUser) {

         Debug.print("AdUserBean dropAdApprovalUser");
      
         try {
      	
            Collection adApprovalUsers = getAdApprovalUsers();
	        adApprovalUsers.remove(adApprovalUser);
	      
         } catch (Exception ex) {
      	
           throw new EJBException(ex.getMessage());
         
         }   
     }
     
     /**
     * @ejb:interface-method view-type="local"
     **/
     public void addAdApprovalQueue(LocalAdApprovalQueue adApprovalQueue) {

         Debug.print("AdUserBean addAdApprovalQueue");
      
         try {
      	
            Collection adApprovalQueues = getAdApprovalQueues();
	        adApprovalQueues.add(adApprovalQueue);
	         
         } catch (Exception ex) {
      	
           throw new EJBException(ex.getMessage());
      
         }   
     }

    /**
     * @ejb:interface-method view-type="local"
     **/
     public void dropAdApprovalQueue(LocalAdApprovalQueue adApprovalQueue) {

         Debug.print("AdUserBean dropAdApprovalQueue");
      
         try {
      	
            Collection adApprovalQueues = getAdApprovalQueues();
	        adApprovalQueues.remove(adApprovalQueue);
	      
         } catch (Exception ex) {
      	
           throw new EJBException(ex.getMessage());
         
         }   
     }  
     
     /**
      * @ejb:interface-method view-type="local"
      **/
     public void addGlUserStaticReport(LocalGlUserStaticReport glUserStaticReport) {

        Debug.print("AdUserBean addGlStaticReportBean");
        try {
           Collection glUserStaticReports = getGlUserStaticReports();
           glUserStaticReports.add(glUserStaticReport);
        } catch (Exception ex) {
           throw new EJBException(ex.getMessage());
        }
     }

     /**
      * @ejb:interface-method view-type="local"
      **/
     public void dropGlUserStaticReport(LocalGlUserStaticReport glUserStaticReport) {
     
        Debug.print("AdUserBean dropGlStaticReportBean");
        try {
           Collection glUserStaticReports = getGlUserStaticReports();
           glUserStaticReports.remove(glUserStaticReport);
        } catch (Exception ex) {
           throw new EJBException(ex.getMessage());
        }
     }

    // ENTITY METHODS
    
    /**
     * @ejb:create-method view-type="local"
     **/
     public Integer ejbCreate(Integer USR_CODE, 
         String USR_NM, String USR_DEPT, String USR_DESC, String USR_PSTN, String USR_EML_ADDRSS, byte USR_HEAD, byte USR_INSPCTR, String USR_PSSWRD, short USR_PSSWRD_EXPRTN_CODE,
         short USR_PSSWRD_EXPRTN_DYS, short USR_PSSWRD_EXPRTN_ACCSS, short USR_PSSWRD_CURR_ACCSS,
         Date USR_DT_FRM, Date USR_DT_TO, Integer USR_AD_CMPNY)
         throws CreateException {
      	
         Debug.print("AdUserBean ejbCreate");
      
         setUsrCode(USR_CODE);
         setUsrName(USR_NM);
         setUsrDept(USR_DEPT);
         setUsrPosition(USR_PSTN);
         setUsrEmailAddress(USR_EML_ADDRSS);
         setUsrDescription(USR_DESC);
         setUsrHead(USR_HEAD);
         setUsrInspector(USR_INSPCTR);
         setUsrPassword(USR_PSSWRD);
         setUsrPasswordExpirationCode(USR_PSSWRD_EXPRTN_CODE);
         setUsrPasswordExpirationDays(USR_PSSWRD_EXPRTN_DYS);
         setUsrPasswordExpirationAccess(USR_PSSWRD_EXPRTN_ACCSS);
         setUsrPasswordCurrentAccess(USR_PSSWRD_CURR_ACCSS);
         setUsrDateFrom(USR_DT_FRM);
         setUsrDateTo(USR_DT_TO);
         setUsrAdCompany(USR_AD_CMPNY);
        
         return null;
    }
    
    /**
     * @ejb:create-method view-type="local"
     **/
     public Integer ejbCreate(
         String USR_NM, String USR_DEPT, String USR_DESC, String USR_PSTN, String USR_EML_ADDRSS, byte USR_HEAD, byte USR_INSPCTR, String USR_PSSWRD, short USR_PSSWRD_EXPRTN_CODE,
         short USR_PSSWRD_EXPRTN_DYS, short USR_PSSWRD_EXPRTN_ACCSS, short USR_PSSWRD_CURR_ACCSS,
         Date USR_DT_FRM, Date USR_DT_TO, Integer USR_AD_CMPNY)
         throws CreateException {
      	
         Debug.print("AdUserBean ejbCreate");
         
         setUsrName(USR_NM);
         setUsrDept(USR_DEPT);
         setUsrDescription(USR_DESC);
         setUsrPosition(USR_PSTN);
         setUsrEmailAddress(USR_EML_ADDRSS);
         setUsrHead(USR_HEAD);
         setUsrInspector(USR_INSPCTR);
         setUsrPassword(USR_PSSWRD);
         setUsrPasswordExpirationCode(USR_PSSWRD_EXPRTN_CODE);
         setUsrPasswordExpirationDays(USR_PSSWRD_EXPRTN_DYS);
         setUsrPasswordExpirationAccess(USR_PSSWRD_EXPRTN_ACCSS);
         setUsrPasswordCurrentAccess(USR_PSSWRD_CURR_ACCSS);
         setUsrDateFrom(USR_DT_FRM);
         setUsrDateTo(USR_DT_TO);
         setUsrAdCompany(USR_AD_CMPNY);
        
         return null;
     }
    
     public void ejbPostCreate(Integer USR_CODE, 
         String USR_NM, String USR_DEPT, String USR_DESC, String USR_PSTN, String USR_EML_ADDRSS, byte USR_HEAD, byte USR_INSPCTR, String USR_PSSWRD, short USR_PSSWRD_EXPRTN_CODE,
         short USR_PSSWRD_EXPRTN_DYS, short USR_PSSWRD_EXPRTN_ACCSS, short USR_PSSWRD_CURR_ACCSS,
         Date USR_DT_FRM, Date USR_DT_TO, Integer USR_AD_CMPNY)      
         throws CreateException { }
      
     public void ejbPostCreate(
         String USR_NM, String USR_DEPT, String USR_DESC, String USR_PSTN, String USR_EML_ADDRSS, byte USR_HEAD, byte USR_INSPCTR, String USR_PSSWRD, short USR_PSSWRD_EXPRTN_CODE,
         short USR_PSSWRD_EXPRTN_DYS, short USR_PSSWRD_EXPRTN_ACCSS, short USR_PSSWRD_CURR_ACCSS,
         Date USR_DT_FRM, Date USR_DT_TO, Integer USR_AD_CMPNY)
         throws CreateException { }     
}

      
      	
      	