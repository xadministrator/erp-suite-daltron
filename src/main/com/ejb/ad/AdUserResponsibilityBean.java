/*
 * com/ejb/ar/LocalAdUserResponsibilityBean.java
 *
 * Created on June 16, 2003, 1:54 PM
 */
 
package com.ejb.ad;

import java.util.Date;

import javax.ejb.CreateException;

import com.util.AbstractEntityBean;
import com.util.Debug;

/**
 *
 * @author  Neil Andrew M. Ajero
 *
 * @ejb:bean name="AdUserResponsibilityEJB"
 *           display-name="User Responsibility Entity"
 *           type="CMP"
 *           cmp-version="2.x"
 *           view-type="local"
 *           local-jndi-name="omega-ejb/AdUserResponsibilityEJB"
 *           schema="AdUserResponsibility"
 *           primkey-field="urCode"
 *
 * @ejb:pk class="java.lang.Integer"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="aduser"
 *                        role-link="aduserlink"
 *
 * @ejb:permission role-name="aduser"
 *
 * @ejb:interface local-class="com.ejb.ad.LocalAdUserResponsibility"
 *                local-extends="javax.ejb.EJBLocalObject"
 *
 * @ejb:home local-class="com.ejb.ad.LocalAdUserResponsibilityHome"
 *           local-extends="javax.ejb.EJBLocalHome"
 *
 * @ejb:finder signature="Collection findByUsrCode(java.lang.Integer USR_CODE, java.lang.Integer UR_AD_CMPNY)"
 *             query="SELECT OBJECT(ur) FROM AdUser usr, IN(usr.adUserResponsibilities) ur WHERE usr.usrCode = ?1 AND ur.urAdCompany = ?2"
 *
 * @jboss:query signature="Collection findByUsrCode(java.lang.Integer USR_CODE, java.lang.Integer UR_AD_CMPNY)"
 *             query="SELECT OBJECT(ur) FROM AdUser usr, IN(usr.adUserResponsibilities) ur WHERE usr.usrCode = ?1 AND ur.urAdCompany = ?2 ORDER BY ur.adResponsibility.rsName"
 *
 * @ejb:finder signature="LocalAdUserResponsibility findByRsNameAndUsrCode(java.lang.String RS_NM, java.lang.Integer USR_CODE, java.lang.Integer UR_AD_CMPNY)"
 *             query="SELECT OBJECT(ur) FROM AdUserResponsibility ur WHERE ur.adResponsibility.rsName=?1 AND ur.adUser.usrCode=?2 AND ur.urAdCompany = ?3"
 *
 * @ejb:finder signature="LocalAdUserResponsibility findByUrPmUsrReferenceID(java.lang.Integer UR_PM_USR_ID, java.lang.Integer UR_AD_CMPNY)"
 *             query="SELECT OBJECT(ur) FROM AdUserResponsibility ur WHERE ur.adUser.pmUser.usrReferenceID = ?1 AND ur.urAdCompany = ?2"
 
 *
 * @ejb:value-object match="*"
 *             name="AdUserResponsibility"
 *
 * @jboss:persistence table-name="AD_USR_RSPNSBLTY"
 *
 * @jboss:entity-command name="mysql-get-generated-keys"
 */

public abstract class AdUserResponsibilityBean extends AbstractEntityBean {
    
   
    // PERSISTENT METHODS
    
    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     * @ejb:pk-field
     *
     * @jboss:column-name name="UR_CODE"
     *
     * @jboss:persistence auto-increment="true"
     **/
    public abstract Integer getUrCode();
    public abstract void setUrCode(Integer UR_CODE);
    
    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="UR_DT_FRM"
     **/
    public abstract Date getUrDateFrom();
     /**
      * @ejb:interface-method view-type="local"
      **/  
    public abstract void setUrDateFrom(Date UR_DT_FRM);

    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="UR_DT_TO"
     **/
    public abstract Date getUrDateTo();
     /**
      * @ejb:interface-method view-type="local"
      **/  
    public abstract void setUrDateTo(Date UR_DT_TO);
    
    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="UR_AD_CMPNY"
     **/
    public abstract Integer getUrAdCompany();
     /**
      * @ejb:interface-method view-type="local"
      **/  
    public abstract void setUrAdCompany(Integer UR_AD_CMPNY);

    
    // RELATIONSHIP METHODS
    
    /**
     * @ejb:interface-method view-type="local"
     * @ejb:relation name="aduser-userresponsibilities"
     *               role-name="userresponsibility-has-one-aduser"
     *				 cascade-delete="yes"
     * 
     * @jboss:relation related-pk-field="usrCode"
     *                 fk-column="UR_USR_CODE"
     */
     public abstract LocalAdUser getAdUser();
     public abstract void setAdUser(LocalAdUser adUser);

    // RELATIONSHIP METHODS
    
    /**
     * @ejb:interface-method view-type="local"
     * @ejb:relation name="adresponsibility-userresponsibilities"
     *               role-name="userresponsibility-has-one-adresponsibility"
     * 
     * @jboss:relation related-pk-field="rsCode"
     *                 fk-column="UR_RS_CODE"
     */
     public abstract LocalAdResponsibility getAdResponsibility();
     public abstract void setAdResponsibility(LocalAdResponsibility adResponsibility);
     
    // BUSINESS METHODS
     
    // ENTITY METHODS
    
    /**
     * @ejb:create-method view-type="local"
     **/
     public Integer ejbCreate(Integer UR_CODE, 
         Date UR_DT_FRM, Date UR_DT_TO, Integer UR_AD_CMPNY)
         throws CreateException {
      	
         Debug.print("AdUserResponsibilityBean ejbCreate");
      
         setUrCode(UR_CODE);
         setUrDateFrom(UR_DT_FRM);
         setUrDateTo(UR_DT_TO);
         setUrAdCompany(UR_AD_CMPNY);
        
         return null;
     } 
    
    /**
     * @ejb:create-method view-type="local"
     **/
     public Integer ejbCreate(
         Date UR_DT_FRM, Date UR_DT_TO, Integer UR_AD_CMPNY)
         throws CreateException {
      	
         Debug.print("AdUserResponsibilityBean ejbCreate");
        
         setUrDateFrom(UR_DT_FRM);
         setUrDateTo(UR_DT_TO);
         setUrAdCompany(UR_AD_CMPNY);
        
         return null;
     }
    
     public void ejbPostCreate(Integer UR_CODE, 
         Date UR_DT_FRM, Date UR_DT_TO, Integer UR_AD_CMPNY)      
         throws CreateException { }
      
     public void ejbPostCreate(
         Date UR_DT_FRM, Date UR_DT_TO, Integer UR_AD_CMPNY)
         throws CreateException { }     
}