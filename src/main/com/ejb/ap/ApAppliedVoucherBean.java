/*
 * com/ejb/ap/LocalApAppliedVoucherBean.java
 *
 * Created on February 13, 2004, 3:44 PM
 */

package com.ejb.ap;

import java.util.Collection;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;

import com.util.AbstractEntityBean;
import com.util.Debug;

/**
 *
 * @author  Neil Andrew M. Ajero
 *
 * @ejb:bean name="ApAppliedVoucherEJB"
 *           display-name="AppliedVoucher Entity"
 *           type="CMP"
 *           cmp-version="2.x"
 *           view-type="local"
 *           local-jndi-name="omega-ejb/ApAppliedVoucherEJB"
 *           schema="ApAppliedVoucher"
 *           primkey-field="avCode"
 *
 * @ejb:pk class="java.lang.Integer"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="apuser"
 *                        role-link="apuserlink"
 *
 * @ejb:permission role-name="apuser"
 *
 * @ejb:interface local-class="com.ejb.ap.LocalApAppliedVoucher"
 *                local-extends="javax.ejb.EJBLocalObject"
 *
 * @ejb:home local-class="com.ejb.ap.LocalApAppliedVoucherHome"
 *           local-extends="javax.ejb.EJBLocalHome"
 * 
 * @ejb:finder signature="Collection findByChkDateRangeAndWtcCode(java.util.Date CHK_DT_FRM, java.util.Date CHK_DT_TO, java.lang.Integer WTC_CODE, java.lang.Integer AV_AD_CMPNY)"
 *             query="SELECT OBJECT(av) FROM ApAppliedVoucher av WHERE av.apCheck.chkDate >= ?1 AND av.apCheck.chkDate <= ?2 AND av.apCheck.chkPosted = 1 AND av.apCheck.chkVoid = 0 AND av.apCheck.chkType = 'PAYMENT' AND av.apVoucherPaymentSchedule.apVoucher.apWithholdingTaxCode.wtcCode = ?3 AND av.avTaxWithheld > 0 AND av.avAdCompany = ?4"
 * 
 * @jboss:query signature="Collection findByChkDateRangeAndWtcCode(java.util.Date CHK_DT_FRM, java.util.Date CHK_DT_TO, java.lang.Integer WTC_CODE, java.lang.Integer AV_AD_CMPNY)"
 *             query="SELECT OBJECT(av) FROM ApAppliedVoucher av WHERE av.apCheck.chkDate >= ?1 AND av.apCheck.chkDate <= ?2 AND av.apCheck.chkPosted = 1 AND av.apCheck.chkVoid = 0 AND av.apCheck.chkType = 'PAYMENT' AND av.apVoucherPaymentSchedule.apVoucher.apWithholdingTaxCode.wtcCode = ?3 AND av.avTaxWithheld > 0 AND av.avAdCompany = ?4"
 * 
 * @ejb:finder signature="Collection findPostedAvByVpsCode(java.lang.Integer VPS_CODE, java.lang.Integer AV_AD_CMPNY)"
 *             query="SELECT OBJECT(av) FROM ApAppliedVoucher av WHERE av.apVoucherPaymentSchedule.vpsCode = ?1 AND av.apCheck.chkVoid = 0 AND av.apCheck.chkPosted = 1 AND av.avAdCompany = ?2"
 * 
 * @ejb:finder signature="Collection findByChkCode(java.lang.Integer CHK_CODE, java.lang.Integer AV_AD_CMPNY)"
 *             query="SELECT OBJECT(av) FROM ApAppliedVoucher av WHERE av.apCheck.chkCode = ?1 AND av.avAdCompany = ?2"
 * 
 * @ejb:select signature="Collection ejbSelectGeneric(java.lang.String jbossQl, java.lang.Object[] args)"
 *             result-type-mapping="Local"
 *             method-intf="LocalHome"
 *             query="SELECT OBJECT(av) FROM ApAppliedVoucher av"
 *
 * @jboss:query signature="Collection ejbSelectGeneric(java.lang.String jbossQl, java.lang.Object[] args)"
 *                         dynamic="true"
 *
 * @ejb:value-object match="*"
 *             name="ApAppliedVoucher"
 *
 * @jboss:persistence table-name="AP_APPLD_VCHR"
 *
 * @jboss:entity-command name="mysql-get-generated-keys"
 */

public abstract class ApAppliedVoucherBean extends AbstractEntityBean {
    
    
    // PERSITCENT METHODS
    
    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     * @ejb:pk-field
     *
     * @jboss:column-name name="AV_CODE"
     *
     * @jboss:persistence auto-increment="true"
     **/
    public abstract Integer getAvCode();
    public abstract void setAvCode(Integer AV_CODE);
    
    
    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"   
     *
     * @jboss:column-name name="AV_APPLY_AMNT"
     **/
    public abstract double getAvApplyAmount();
     /**
      * @ejb:interface-method view-type="local"
      **/ 
    public abstract void setAvApplyAmount(double AV_APPLY_AMNT);
    
    
    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"   
     *
     * @jboss:column-name name="AV_TX_WTHHLD"
     **/
    public abstract double getAvTaxWithheld();
     /**
      * @ejb:interface-method view-type="local"
      **/ 
    public abstract void setAvTaxWithheld(double AV_TX_WTHHLD);
    

    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"   
     *
     * @jboss:column-name name="AV_DSCNT_AMNT"
     **/
    public abstract double getAvDiscountAmount();
     /**
      * @ejb:interface-method view-type="local"
      **/ 
    public abstract void setAvDiscountAmount(double AV_DSCNT_AMNT);

    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"   
     *
     * @jboss:column-name name="AV_ALLCTD_PYMNT_AMNT"
     **/
    public abstract double getAvAllocatedPaymentAmount();
     /**
      * @ejb:interface-method view-type="local"
      **/ 
    public abstract void setAvAllocatedPaymentAmount(double AV_ALLCTD_PYMNT_AMNT);

    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"   
     *
     * @jboss:column-name name="AV_FRX_GN_LSS"
     **/
    public abstract double getAvForexGainLoss();
     /**
      * @ejb:interface-method view-type="local"
      **/ 
    public abstract void setAvForexGainLoss(double AV_FRX_GN_LSS);
    
    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"   
     *
     * @jboss:column-name name="AV_AD_CMPNY"
     **/
    public abstract Integer getAvAdCompany();
     /**
      * @ejb:interface-method view-type="local"
      **/ 
    public abstract void setAvAdCompany(Integer AV_AD_CMPNY);
        
   
    // RELATIONSHIP METHODS

    /**
     * @ejb:interface-method view-type="local"
     * @ejb:relation name="check-appliedvouchers"
     *               role-name="appliedvoucher-has-one-check"
     * 
     * @jboss:relation related-pk-field="chkCode"
     *                 fk-column="AP_CHECK"
     */
    public abstract LocalApCheck getApCheck();
    /**
     * @ejb:interface-method view-type="local"
     **/
    public abstract void setApCheck(LocalApCheck apCheck);

    /**
     * @ejb:interface-method view-type="local"
     * @ejb:relation name="voucherpaymentschedule-appliedvouchers"
     *               role-name="appliedvoucher-has-one-voucherpaymentschedule"
     * 
     * @jboss:relation related-pk-field="vpsCode"
     *                 fk-column="AP_VOUCHER_PAYMENT_SCHEDULE"
     */
    public abstract LocalApVoucherPaymentSchedule getApVoucherPaymentSchedule();
    /**
     * @ejb:interface-method view-type="local"
     **/
    public abstract void setApVoucherPaymentSchedule(LocalApVoucherPaymentSchedule apCheck); 
    
    /**
     * @ejb:interface-method view-type="local"
     * @ejb:relation name="appliedvoucher-apdistributionrecords"
     *               role-name="appliedvoucher-has-many-apdistributionrecords"
     * 
     */
    public abstract Collection getApDistributionRecords();
    /**
     * @ejb:interface-method view-type="local"
     **/
    public abstract void setApDistributionRecords(Collection apDistributionRecords);
    
    /**
     * @jboss:dynamic-ql
     */
     public abstract Collection ejbSelectGeneric(java.lang.String jbossQl, java.lang.Object[] args)
        throws FinderException;

    // BUSINESS METHODS
    
    /**
     * @ejb:home-method view-type="local"
     */
     public Collection ejbHomeGetAvByCriteria(java.lang.String jbossQl, java.lang.Object[] args)
        throws FinderException {
        	
        return ejbSelectGeneric(jbossQl, args);
     }
    
    
    /**
     * @ejb:interface-method view-type="local"
     **/
    public void addApDistributionRecord(LocalApDistributionRecord apDistributionRecord) {

         Debug.print("ApAppliedVoucherBean addApDistributionRecord");
      
         try {
      	
            Collection apDistributionRecords = getApDistributionRecords();
	        apDistributionRecords.add(apDistributionRecord);
	          
         } catch (Exception ex) {
      	
           throw new EJBException(ex.getMessage());
      
         }   
     }

    /**
     * @ejb:interface-method view-type="local"
     **/
    public void dropApDistributionRecord(LocalApDistributionRecord apDistributionRecord) {

         Debug.print("ApAppliedVoucherBean dropApDistributionRecord");
      
         try {
      	
            Collection apDistributionRecords = getApDistributionRecords();
	        apDistributionRecords.remove(apDistributionRecord);
	      
         } catch (Exception ex) {
      	
           throw new EJBException(ex.getMessage());
         
         }   
     }


    // NO BUSINESS METHODS

    // ENTITY METHODS
    
    /**
     * @ejb:create-method view-type="local"
     **/
    public Integer ejbCreate(Integer AV_CODE, 
         double AV_APPLY_AMNT, double AV_TX_WTHHLD, double AV_DSCNT_AMNT, 
         double AV_ALLCTD_PYMNT_AMNT, double AV_FRX_GN_LSS, Integer AV_AD_CMPNY)     	
         throws CreateException {	
     	
         Debug.print("ApAppliedVoucherBean ejbCreate");
     
	     setAvCode(AV_CODE);
	     setAvApplyAmount(AV_APPLY_AMNT);
	     setAvTaxWithheld(AV_TX_WTHHLD);
	     setAvDiscountAmount(AV_DSCNT_AMNT); 
	     setAvAllocatedPaymentAmount(AV_ALLCTD_PYMNT_AMNT);
	     setAvForexGainLoss(AV_FRX_GN_LSS);  	  
	     setAvAdCompany(AV_AD_CMPNY);

         return null;
     }
     
    /**
     * @ejb:create-method view-type="local"
     **/
    public Integer ejbCreate( 
         double AV_APPLY_AMNT, double AV_TX_WTHHLD, double AV_DSCNT_AMNT, 
         double AV_ALLCTD_PYMNT_AMNT, double AV_FRX_GN_LSS, Integer AV_AD_CMPNY)     	
         throws CreateException {	
     	
         Debug.print("ApAppliedVoucherBean ejbCreate");
     
	     setAvApplyAmount(AV_APPLY_AMNT);
	     setAvTaxWithheld(AV_TX_WTHHLD);
	     setAvDiscountAmount(AV_DSCNT_AMNT); 
	     setAvAllocatedPaymentAmount(AV_ALLCTD_PYMNT_AMNT);
	     setAvForexGainLoss(AV_FRX_GN_LSS);  
	     setAvAdCompany(AV_AD_CMPNY);
	     
         return null;
     }
     
     public void ejbPostCreate(Integer AV_CODE, 
         double AV_APPLY_AMNT, double AV_TX_WTHHLD, double AV_DSCNT_AMNT, 
         double AV_ALLCTD_PYMNT_AMNT, double AV_FRX_GN_LSS, Integer AV_AD_CMPNY)
         throws CreateException { }
      
     public void ejbPostCreate( 
         double AV_APPLY_AMNT, double AV_TX_WTHHLD, double AV_DSCNT_AMNT, 
         double AV_ALLCTD_PYMNT_AMNT, double AV_FRX_GN_LSS, Integer AV_AD_CMPNY)
         throws CreateException { }
}