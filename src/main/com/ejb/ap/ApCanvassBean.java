/*
 * com/ejb/ap/LocalApPurchaseOrderLineBean.java
 *
 * Created on April 17, 2006 05:20 PM
 */

package com.ejb.ap;

import java.util.Collection;

import javax.ejb.CreateException;
import javax.ejb.FinderException;

import com.util.AbstractEntityBean;
import com.util.Debug;

/**
 * 
 * @author  Aliza D.J. Anos
 * 
 * @ejb:bean name="ApCanvassEJB"
 *           display-name="Canvass Entity"
 *           type="CMP"
 *           cmp-version="2.x"
 *           view-type="local"
 *           local-jndi-name="omega-ejb/ApCanvassEJB"
 *           schema="ApCanvass"
 *           primkey-field="cnvCode"
 *       
 * @ejb:pk class="java.lang.Integer"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="apuser"
 *                        role-link="apuserlink"
 *
 * @ejb:permission role-name="apuser"
 *
 * @ejb:interface local-class="com.ejb.ap.LocalApCanvass"
 *                local-extends="javax.ejb.EJBLocalObject"
 *
 * @ejb:home local-class="com.ejb.ap.LocalApCanvassHome"
 *           local-extends="javax.ejb.EJBLocalHome"
 * 
 * 
 * @ejb:finder signature="Collection findByPrCodeAndCnvPo(java.lang.Integer PR_CODE, byte CNV_PO, java.lang.Integer CNV_AD_CMPNY)"
 *             query="SELECT OBJECT(cnv) FROM ApCanvass cnv WHERE cnv.apPurchaseRequisitionLine.apPurchaseRequisition.prCode=?1 AND cnv.cnvPo=?2 AND cnv.cnvAdCompany = ?3"
 *
 * @jboss:query signature="Collection findByPrCodeAndCnvPo(java.lang.Integer PR_CODE, byte CNV_PO, java.lang.Integer CNV_AD_CMPNY)"
 *             query="SELECT OBJECT(cnv) FROM ApCanvass cnv WHERE cnv.apPurchaseRequisitionLine.apPurchaseRequisition.prCode=?1 AND cnv.cnvPo=?2 AND cnv.cnvAdCompany = ?3 ORDER BY cnv.apSupplier.splSupplierCode"
 * 
 * @ejb:finder signature="Collection findByPrlCodeAndSplSupplierCode(java.lang.Integer PRL_CODE, java.lang.String SPL_SPPLR_CODE, java.lang.Integer CNV_AD_CMPNY)"
 *             query="SELECT OBJECT(cnv) FROM ApCanvass cnv WHERE cnv.apPurchaseRequisitionLine.prlCode=?1 AND cnv.apSupplier.splSupplierCode = ?2 AND cnv.cnvAdCompany = ?3"
 * 
 * @ejb:finder signature="Collection findByPrlCodeAndCnvPo(java.lang.Integer PRL_CODE, byte CNV_PO, java.lang.Integer CNV_AD_CMPNY)"
 *             query="SELECT OBJECT(cnv) FROM ApCanvass cnv WHERE cnv.apPurchaseRequisitionLine.prlCode=?1 AND cnv.cnvPo = ?2 AND cnv.cnvAdCompany = ?3"
 * 
 * 
 * @jboss:query signature="Collection ejbSelectGeneric(java.lang.String jbossQl, java.lang.Object[] args)"
 *                         dynamic="true"
 *
 * @ejb:value-object match="*"
 *             name="ApCanvass"
 * 
 * @jboss:persistence table-name="AP_CNVSS"
 *
 * @jboss:entity-command name="mysql-get-generated-keys"
 *
 */

public abstract class ApCanvassBean extends AbstractEntityBean {
	
	//	 Access methods for persistent fields
	
	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 * @ejb:pk-field
	 *
	 * @jboss:column-name name="CNV_CODE"
	 *
	 * @jboss:persistence auto-increment="true"
	 **/
	public abstract java.lang.Integer getCnvCode();
	public abstract void setCnvCode(java.lang.Integer CNV_CODE);
	
	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="CNV_LN"
	 **/
	public abstract short getCnvLine();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setCnvLine(short CNV_LN);
	
	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="CNV_RMKS"
	 **/
	public abstract String getCnvRemarks();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setCnvRemarks(String CNV_RMKS);
	
	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="CNV_QTY"
	 **/
	public abstract double getCnvQuantity();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setCnvQuantity(double CNV_QTY);
	
	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="CNV_UNT_CST"
	 **/
	public abstract double getCnvUnitCost();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setCnvUnitCost(double CNV_UNT_CST);
	
	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="CNV_AMNT"
	 **/
	public abstract double getCnvAmount();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setCnvAmount(double CNV_AMNT);
	
	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="CNV_PO"
	 **/
	public abstract byte getCnvPo();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setCnvPo(byte CNV_PO);
	
	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="CNV_AD_CMPNY"
	 **/
	public abstract Integer getCnvAdCompany();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setCnvAdCompany(Integer CNV_AD_CMPNY);
	
	//	 Access methods for relationship fields
	
	/**
	 * @ejb:interface-method view-type="local"
	 * @ejb:relation name="purchaserequisitionline-canvasses"
	 *               role-name="canvass-has-one-purchaserequisitionline"
	 *               cascade-delete="yes"
	 *
	 * @jboss:relation related-pk-field="prlCode"
	 *                 fk-column="AP_PURCHASE_REQUISITION_LINE"
	 */
	public abstract LocalApPurchaseRequisitionLine getApPurchaseRequisitionLine();
	public abstract void setApPurchaseRequisitionLine(LocalApPurchaseRequisitionLine apPurchaseRequisitionLine);
	
	/**
	 * @ejb:interface-method view-type="local"
	 * @ejb:relation name="supplier-canvasses"
	 *               role-name="canvass-has-one-supplier"
	 *
	 * @jboss:relation related-pk-field="splCode"
	 *                 fk-column="AP_SUPPLIER"
	 */
	public abstract LocalApSupplier getApSupplier();
	public abstract void setApSupplier(LocalApSupplier apSupplier);
	
	/**
	 * @jboss:dynamic-ql
	 */
	public abstract Collection ejbSelectGeneric(java.lang.String jbossQl, java.lang.Object[] args)
	throws FinderException;
	
//	 BUSINESS METHODS
	
	/**
	* @ejb:home-method view-type="local"
	*/
	public Collection ejbHomeGetCnvByCriteria(java.lang.String jbossQl, java.lang.Object[] args)
	   throws FinderException {
	   	
	   return ejbSelectGeneric(jbossQl, args);
	   
	}
	
//	 ENTITY METHODS
	
	/**
     * @ejb:create-method view-type="local"
     **/
    public Integer ejbCreate(Integer CNV_CODE, short CNV_LN, String CNV_RMKS, double CNV_QTY, double CNV_UNT_CST, double CNV_AMNT, byte CNV_PO, Integer CNV_AD_CMPNY)
        throws CreateException {
          
        Debug.print("ApCanvassBean ejbCreate");
       
        setCnvCode(CNV_CODE);
        setCnvLine(CNV_LN);
        setCnvRemarks(CNV_RMKS);
        setCnvQuantity(CNV_QTY);
        setCnvUnitCost(CNV_UNT_CST);
        setCnvAmount(CNV_AMNT);
        setCnvPo(CNV_PO);
        setCnvAdCompany(CNV_AD_CMPNY);
               
        return null;
       
    }
    
    /**
     * @ejb:create-method view-type="local"
     **/
    public Integer ejbCreate(short CNV_LN, String CNV_RMKS, double CNV_QTY, double CNV_UNT_CST, double CNV_AMNT, byte CNV_PO, Integer CNV_AD_CMPNY)
        throws CreateException {
          
        Debug.print("ApCanvassBean ejbCreate");
       
        setCnvLine(CNV_LN);
        setCnvRemarks(CNV_RMKS);
        setCnvQuantity(CNV_QTY);
        setCnvUnitCost(CNV_UNT_CST);
        setCnvAmount(CNV_AMNT);
        setCnvPo(CNV_PO);
        setCnvAdCompany(CNV_AD_CMPNY);
               
        return null;
       
    }
    
    public void ejbPostCreate(Integer CNV_CODE, short CNV_LN, String CNV_RMKS, double CNV_QTY, double CNV_UNT_CST, double CNV_AMNT, byte CNV_PO, Integer CNV_AD_CMPNY)
    	throws CreateException { }
    
    public void ejbPostCreate(short CNV_LN, String CNV_RMKS, double CNV_QTY, double CNV_UNT_CST, double CNV_AMNT, byte CNV_PO, Integer CNV_AD_CMPNY)
		throws CreateException { }
}