package com.ejb.ap;

import java.util.Collection;
import java.util.Date;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;

import com.util.AbstractEntityBean;
import com.util.Debug;

/**
 * @ejb:bean name="ApCheckBatchEJB"
 *           display-name="Check Batch Entity"
 *           type="CMP"
 *           cmp-version="2.x"
 *           view-type="local"
 *           local-jndi-name="omega-ejb/ApCheckBatchEJB"
 *           schema="ApCheckBatch"
 *           primkey-field="cbCode"
 *       
 * @ejb:pk class="Integer"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="apuser"
 *                        role-link="apuserlink"
 *
 * @ejb:permission role-name="apuser"
 *
 * @ejb:interface local-class="com.ejb.ap.LocalApCheckBatch"
 *                local-extends="javax.ejb.EJBLocalObject"
 *
 * @ejb:home local-class="com.ejb.ap.LocalApCheckBatchHome"
 *           local-extends="javax.ejb.EJBLocalHome"
 *
 * @ejb:finder signature="LocalApCheckBatch findByCbName(java.lang.String CB_NM, java.lang.Integer CB_AD_BRNCH, java.lang.Integer CB_AD_CMPNY)"
 *             query="SELECT OBJECT(cb) FROM ApCheckBatch cb WHERE cb.cbName=?1 AND cb.cbAdBranch=?2 AND cb.cbAdCompany = ?3"
 * 
 * @ejb:finder signature="LocalApCheckBatch findByPaymentCbName(java.lang.String CB_NM, java.lang.Integer CB_AD_BRNCH, java.lang.Integer CB_AD_CMPNY)"
 *             query="SELECT OBJECT(cb) FROM ApCheckBatch cb WHERE cb.cbType='PAYMENT' AND cb.cbName=?1 AND cb.cbAdBranch=?2 AND cb.cbAdCompany = ?3"
 *             
 * @ejb:finder signature="LocalApCheckBatch findByDirectCbName(java.lang.String CB_NM, java.lang.Integer CB_AD_BRNCH, java.lang.Integer CB_AD_CMPNY)"
 *             query="SELECT OBJECT(cb) FROM ApCheckBatch cb WHERE cb.cbType='DIRECT' AND cb.cbName=?1 AND cb.cbAdBranch=?2 AND cb.cbAdCompany = ?3"
 * 
 * @ejb:finder signature="Collection findOpenCbAll(java.lang.Integer CB_AD_BRNCH, java.lang.Integer CB_AD_CMPNY)"
 *             query="SELECT OBJECT(cb) FROM ApCheckBatch cb WHERE cb.cbStatus='OPEN' AND cb.cbAdBranch = ?1 AND cb.cbAdCompany = ?2"
 * 
 * @jboss:query signature="Collection findOpenCbAll(java.lang.Integer CB_AD_BRNCH, java.lang.Integer CB_AD_CMPNY)"
 *             query="SELECT OBJECT(cb) FROM ApCheckBatch cb WHERE cb.cbStatus='OPEN' AND cb.cbAdBranch = ?1 AND cb.cbAdCompany = ?2 ORDER BY cb.cbName"
 * 
 * @ejb:finder signature="Collection findOpenCbByCbType(java.lang.String CB_TYP, java.lang.Integer CB_AD_BRNCH, java.lang.Integer CB_AD_CMPNY)"
 *             query="SELECT OBJECT(cb) FROM ApCheckBatch cb WHERE cb.cbType=?1 AND cb.cbStatus='OPEN' AND cb.cbAdBranch = ?2 AND cb.cbAdCompany = ?3"
 * 
 * @jboss:query signature="Collection findOpenCbByCbType(java.lang.String CB_TYP, java.lang.Integer CB_AD_BRNCH, java.lang.Integer CB_AD_CMPNY)"
 *             query="SELECT OBJECT(cb) FROM ApCheckBatch cb WHERE cb.cbType=?1 AND cb.cbStatus='OPEN' AND cb.cbAdBranch = ?2 AND cb.cbAdCompany = ?3 ORDER BY cb.cbName"
 *
 * @ejb:finder signature="Collection findOpenCbByCbTypeDepartment(java.lang.String CB_TYP, java.lang.String VB_DPRTMNT, java.lang.Integer CB_AD_BRNCH, java.lang.Integer CB_AD_CMPNY)"
 *             query="SELECT OBJECT(cb) FROM ApCheckBatch cb WHERE cb.cbType=?1 AND cb.cbDepartment=?2 AND cb.cbStatus='OPEN' AND cb.cbAdBranch = ?3 AND cb.cbAdCompany = ?4"
 * 
 * @jboss:query signature="Collection findOpenCbByCbTypeDepartment(java.lang.String CB_TYP, java.lang.String VB_DPRTMNT, java.lang.Integer CB_AD_BRNCH, java.lang.Integer CB_AD_CMPNY)"
 *             query="SELECT OBJECT(cb) FROM ApCheckBatch cb WHERE cb.cbType=?1 AND cb.cbDepartment=?2 AND cb.cbStatus='OPEN' AND cb.cbAdBranch = ?3 AND cb.cbAdCompany = ?4 ORDER BY cb.cbName"
 *
 * @ejb:select signature="Collection ejbSelectGeneric(java.lang.String jbossQl, java.lang.Object[] args)"
 *             result-type-mapping="Local"
 *             method-intf="LocalHome"
 *             query="SELECT OBJECT(cb) FROM ApCheckBatch cb"
 *
 * @jboss:query signature="Collection ejbSelectGeneric(java.lang.String jbossQl, java.lang.Object[] args)"
 *                         dynamic="true"
 *
 * @ejb:value-object match="*"
 *             name="ApCheckBatch"
 *
 * @jboss:persistence table-name="AP_CHCK_BTCH"
 *
 * @jboss:entity-command name="mysql-get-generated-keys"
 *
 */

public abstract class ApCheckBatchBean extends AbstractEntityBean {


   // Access methods for persistent fields

   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    * @ejb:pk-field
    *
    * @jboss:column-name name="CB_CODE"
    *
    * @jboss:persistence auto-increment="true"
    **/
   public abstract Integer getCbCode();
   public abstract void setCbCode(Integer CB_CODE);

   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="CB_NM"
    **/
   public abstract String getCbName();
   /**
    * @ejb:interface-method view-type="local"
    **/
   public abstract void setCbName(String CB_NM);

   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="CB_DESC"
    **/
   public abstract String getCbDescription();
   /**
    * @ejb:interface-method view-type="local"
    **/
   public abstract void setCbDescription(String CB_DESC);
   
   
   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="CB_STATUS"
    **/
   public abstract String getCbStatus();
   /**
    * @ejb:interface-method view-type="local"
    **/
   public abstract void setCbStatus(String CB_STATUS);
   
   
   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="CB_TYP"
    **/
   public abstract String getCbType();
   /**
    * @ejb:interface-method view-type="local"
    **/
   public abstract void setCbType(String CB_TYP);

   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="CB_DT_CRTD"
    **/
   public abstract Date getCbDateCreated();
   /**
    * @ejb:interface-method view-type="local"
    **/
   public abstract void setCbDateCreated(Date CB_DT_CRTD);
   
   
   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="CB_CRTD_BY"
    **/
   public abstract String getCbCreatedBy();
   /**
    * @ejb:interface-method view-type="local"
    **/
   public abstract void setCbCreatedBy(String CB_CRTD_BY);

   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="CB_DPRTMNT"
    **/
   public abstract String getCbDepartment();
   /**
    * @ejb:interface-method view-type="local"
    **/
   public abstract void setCbDepartment(String CB_DPRTMNT);
   
   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="CB_AD_BRNCH"
    **/
   public abstract Integer getCbAdBranch();
   /**
    * @ejb:interface-method view-type="local"
    **/
   public abstract void setCbAdBranch(Integer CB_AD_BRNCH);

   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="CB_AD_CMPNY"
    **/
   public abstract Integer getCbAdCompany();
   /**
    * @ejb:interface-method view-type="local"
    **/
   public abstract void setCbAdCompany(Integer CB_AD_CMPNY);
   
   
   
   // Access methods for relationship fields

   /**
    * @ejb:interface-method view-type="local"
    * @ejb:relation name="checkbatch-checks"
    *               role-name="checkbatch-has-many-checks"
    */
   public abstract Collection getApChecks();
   public abstract void setApChecks(Collection apChecks);
   
  
  /**
    * @jboss:dynamic-ql
    */
    public abstract Collection ejbSelectGeneric(java.lang.String jbossQl, java.lang.Object[] args)
       throws FinderException;

   // Business methods
   
   /**
    * @ejb:home-method view-type="local"
    */
    public Collection ejbHomeGetCbByCriteria(java.lang.String jbossQl, java.lang.Object[] args)
       throws FinderException {
       	
       return ejbSelectGeneric(jbossQl, args);
    }
      
   
   /**
    * @ejb:interface-method view-type="local"
    **/
   public void addApCheck(LocalApCheck apCheck) {

      Debug.print("ApCheckBatchBean addApCheck");
      try {
         Collection apChecks = getApChecks();
	 apChecks.add(apCheck);
      } catch (Exception ex) {
         throw new EJBException(ex.getMessage());
      }
   }

   /**
    * @ejb:interface-method view-type="local"
    **/
   public void dropApCheck(LocalApCheck apCheck) {

      Debug.print("ApCheckBatchBean dropApCheck");
      try {
         Collection apChecks = getApChecks();
	     apChecks.remove(apCheck);
      } catch (Exception ex) {
         throw new EJBException(ex.getMessage());
      }
   }
   
   // EntityBean methods

   /**
    * @ejb:create-method view-type="local"
    **/
   public Integer ejbCreate (Integer CB_CODE, String CB_NM, String CB_DESC,
      String CB_STATUS, String CB_TYP, Date CB_DT_CRTD, String CB_CRTD_BY, String CB_DPRTMNT,
	  Integer CB_AD_BRNCH, Integer CB_AD_CMPNY)
      throws CreateException {

      Debug.print("ApCheckBatchBean ejbCreate");
      setCbCode(CB_CODE);
      setCbName(CB_NM);
      setCbDescription(CB_DESC);
      setCbStatus(CB_STATUS);
      setCbType(CB_TYP);
      setCbDateCreated(CB_DT_CRTD);
      setCbCreatedBy(CB_CRTD_BY);
      setCbDepartment(CB_DPRTMNT);
      setCbAdBranch(CB_AD_BRNCH);
      setCbAdCompany(CB_AD_CMPNY);
      
      return null;
   }

   /**
    * @ejb:create-method view-type="local"
    **/
   public Integer ejbCreate (String CB_NM, String CB_DESC,
      String CB_STATUS, String CB_TYP, Date CB_DT_CRTD, String CB_CRTD_BY, String CB_DPRTMNT,
	  Integer CB_AD_BRNCH, Integer CB_AD_CMPNY)
      throws CreateException {

      Debug.print("ApCheckBatchBean ejbCreate");

      setCbName(CB_NM);
      setCbDescription(CB_DESC);
      setCbStatus(CB_STATUS);
      setCbType(CB_TYP);
      setCbDateCreated(CB_DT_CRTD);
      setCbCreatedBy(CB_CRTD_BY);
      setCbDepartment(CB_DPRTMNT);
      setCbAdBranch(CB_AD_BRNCH);
      setCbAdCompany(CB_AD_CMPNY);
      
      return null;
   }

   public void ejbPostCreate (Integer CB_CODE, String CB_NM, String CB_DESC,
      String CB_STATUS, String CB_TYP, Date CB_DT_CRTD, String CB_CRTD_BY, String CB_DPRTMNT,
	  Integer CB_AD_BRNCH, Integer CB_AD_CMPNY)
      throws CreateException { }

   public void ejbPostCreate (String CB_NM, String CB_DESC,
      String CB_STATUS, String CB_TYP, Date CB_DT_CRTD, String CB_CRTD_BY, String CB_DPRTMNT,
	  Integer CB_AD_BRNCH, Integer CB_AD_CMPNY)
      throws CreateException { }

} // ApCheckBatchBean class
