/*
 * com/ejb/ap/LocalApCheckBean.java
 *
 * Created on February 13, 2003, 05:33 PM
 */

package com.ejb.ap;

import java.util.Collection;
import java.util.Date;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;

import com.util.AbstractEntityBean;
import com.util.Debug;

/**
 *
 * @author  Neil Andrew M. Ajero
 *
 * @ejb:bean name="ApCheckEJB"
 *           display-name="Check Entity"
 *           type="CMP"
 *           cmp-version="2.x"
 *           view-type="local"
 *           local-jndi-name="omega-ejb/ApCheckEJB"
 *           schema="ApCheck"
 *           primkey-field="chkCode"
 *
 * @ejb:pk class="java.lang.Integer"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="apuser"
 *                        role-link="apuserlink"
 *
 * @ejb:permission role-name="apuser"
 *
 * @ejb:interface local-class="com.ejb.ap.LocalApCheck"
 *                local-extends="javax.ejb.EJBLocalObject"
 *
 * @ejb:home local-class="com.ejb.ap.LocalApCheckHome"
 *           local-extends="javax.ejb.EJBLocalHome"
 * 
 * @ejb:finder signature="LocalApCheck findByChkDocumentNumberAndBrCode(java.lang.String CHK_DCMNT_NMBR, java.lang.Integer CHK_AD_BRNCH, java.lang.Integer CHK_AD_CMPNY)"
 *             query="SELECT OBJECT(chk) FROM ApCheck chk  WHERE chk.chkDocumentNumber = ?1 AND chk.chkAdBranch = ?2 AND chk.chkAdCompany = ?3"
 *
 * @ejb:finder signature="Collection findDirectChkForAccruedInterestISGeneration(java.lang.Integer CHK_AD_BRNCH, java.lang.Integer CHK_AD_CMPNY)"
 *             query="SELECT OBJECT(chk) FROM ApCheck chk  WHERE chk.chkType = 'DIRECT' AND chk.chkInvtInscribedStock = 1 AND chk.chkVoid = 0 AND chk.chkAdBranch = ?1 AND chk.chkAdCompany = ?2"
 *
 * @jboss:query signature="Collection findDirectChkForAccruedInterestISGeneration(java.lang.Integer CHK_AD_BRNCH, java.lang.Integer CHK_AD_CMPNY)"
 *             query="SELECT OBJECT(chk) FROM ApCheck chk  WHERE chk.chkType = 'DIRECT' AND chk.chkInvtInscribedStock = 1 AND chk.chkVoid = 0 AND chk.chkAdBranch = ?1 AND chk.chkAdCompany = ?2 ORDER BY chk.chkDate"
 *
 * @ejb:finder signature="Collection findDirectChkForAccruedInterestTBGeneration(java.lang.Integer CHK_AD_BRNCH, java.lang.Integer CHK_AD_CMPNY)"
 *             query="SELECT OBJECT(chk) FROM ApCheck chk  WHERE chk.chkType = 'DIRECT' AND chk.chkInvtTreasuryBill = 1 AND chk.chkVoid = 0 AND chk.chkAdBranch = ?1 AND chk.chkAdCompany = ?2"
 *
 * @jboss:query signature="Collection findDirectChkForLoanInterestGeneration(java.lang.Integer CHK_AD_BRNCH, java.lang.Integer CHK_AD_CMPNY)"
 *             query="SELECT OBJECT(chk) FROM ApCheck chk  WHERE chk.chkPosted = 1 AND chk.chkType = 'DIRECT' AND chk.chkLoan = 1 AND chk.chkLoanGenerated = 0 AND chk.chkVoid = 0 AND chk.chkAdBranch = ?1 AND chk.chkAdCompany = ?2 ORDER BY chk.chkDate"
 *
 * @ejb:finder signature="Collection findDirectChkForLoanInterestGeneration(java.lang.Integer CHK_AD_BRNCH, java.lang.Integer CHK_AD_CMPNY)"
 *             query="SELECT OBJECT(chk) FROM ApCheck chk  WHERE chk.chkPosted = 1 AND chk.chkType = 'DIRECT' AND chk.chkLoan = 1 AND chk.chkLoanGenerated = 0 AND chk.chkVoid = 0 AND chk.chkAdBranch = ?1 AND chk.chkAdCompany = ?2"
 *
 * @jboss:query signature="Collection findDirectChkForAccruedInterestISGeneration(java.lang.Integer CHK_AD_BRNCH, java.lang.Integer CHK_AD_CMPNY)"
 *             query="SELECT OBJECT(chk) FROM ApCheck chk  WHERE chk.chkType = 'DIRECT' AND chk.chkInvtInscribedStock = 1 AND chk.chkVoid = 0 AND chk.chkAdBranch = ?1 AND chk.chkAdCompany = ?2 ORDER BY chk.chkDate"
 *
 * @ejb:finder signature="LocalApCheck findByChkNumberAndBaName(java.lang.String CHK_NMBR, java.lang.String BA_NM, java.lang.Integer CHK_AD_CMPNY)"
 *             query="SELECT OBJECT(chk) FROM ApCheck chk  WHERE chk.chkNumber = ?1 AND chk.adBankAccount.baName = ?2 AND chk.chkAdCompany = ?3"
 *
 * @ejb:finder signature="Collection findPostedChkByChkDateRange(java.util.Date CHK_DT_FRM, java.util.Date CHK_DT_TO, java.lang.Integer CHK_AD_CMPNY)"
 *             query="SELECT OBJECT(chk) FROM ApCheck chk  WHERE chk.chkPosted = 1 AND chk.chkDate >= ?1 AND chk.chkDate <= ?2 AND chk.chkAdCompany = ?3"
 *
 * @jboss:query signature="Collection findPostedChkByChkDateRange(java.util.Date CHK_DT_FRM, java.util.Date CHK_DT_TO, java.lang.Integer CHK_AD_CMPNY)"
 *             query="SELECT OBJECT(chk) FROM ApCheck chk  WHERE chk.chkPosted = 1 AND chk.chkDate >= ?1 AND chk.chkDate <= ?2 AND chk.chkAdCompany = ?3 ORDER BY chk.chkDate"
 *
 * @ejb:finder signature="Collection findVoidPostedChkByChkDateRange(java.util.Date CHK_DT_FRM, java.util.Date CHK_DT_TO, java.lang.Integer CHK_AD_CMPNY)"
 *             query="SELECT OBJECT(chk) FROM ApCheck chk  WHERE chk.chkPosted = 1 AND chk.chkVoid = 1 AND chk.chkVoidPosted = 1 AND chk.chkDate >= ?1 AND chk.chkDate <= ?2 AND chk.chkAdCompany = ?3"
 *
 * @jboss:query signature="Collection findVoidPostedChkByChkDateRange(java.util.Date CHK_DT_FRM, java.util.Date CHK_DT_TO, java.lang.Integer CHK_AD_CMPNY)"
 *             query="SELECT OBJECT(chk) FROM ApCheck chk  WHERE chk.chkPosted = 1 AND chk.chkVoid = 1 AND chk.chkVoidPosted = 1 AND chk.chkDate >= ?1 AND chk.chkDate <= ?2 AND chk.chkAdCompany = ?3 ORDER BY chk.chkDate"
 *
 * @ejb:finder signature="Collection findUnreconciledPostedChkByDateAndBaNameAndBrCode(java.util.Date DT, java.lang.String BA_NM, java.lang.Integer CHK_AD_BRNCH, java.lang.Integer CHK_AD_CMPNY)"
 *             query="SELECT OBJECT(chk) FROM ApCheck chk WHERE chk.chkReconciled = 0 AND chk.chkCheckDate <= ?1 AND chk.chkPosted = 1 AND chk.chkVoid = 0 AND chk.adBankAccount.baName = ?2 AND chk.chkAdBranch = ?3 AND chk.chkAdCompany = ?4"
 *
 *
 * @ejb:finder signature="Collection findPostedChkByDateAndBaName(java.util.Date DT, java.lang.String BA_NM, java.lang.Integer CHK_AD_CMPNY)"
 *             query="SELECT OBJECT(chk) FROM ApCheck chk WHERE chk.chkCheckDate <= ?1 AND chk.chkPosted = 1 AND chk.chkVoid = 0 AND chk.adBankAccount.baName = ?2 AND chk.chkAdCompany = ?3"
 *
 *
 *
 * @jboss:query signature="Collection findUnreconciledPostedChkByDateAndBaNameAndBrCode(java.util.Date DT, java.lang.String BA_NM, java.lang.Integer CHK_AD_BRNCH, java.lang.Integer CHK_AD_CMPNY)"
 *             query="SELECT OBJECT(chk) FROM ApCheck chk WHERE chk.chkReconciled = 0 AND chk.chkCheckDate <= ?1 AND chk.chkPosted = 1 AND chk.chkVoid = 0 AND chk.adBankAccount.baName = ?2 AND chk.chkAdBranch = ?3 AND chk.chkAdCompany = ?4 ORDER BY chk.chkDate, chk.chkNumber"
 *
 * @ejb:finder signature="Collection findUnreconciledPostedChkByDateAndBaName(java.util.Date DT, java.lang.String BA_NM, java.lang.Integer CHK_AD_CMPNY)"
 *             query="SELECT OBJECT(chk) FROM ApCheck chk WHERE chk.chkReconciled = 0 AND chk.chkCheckDate <= ?1 AND chk.chkPosted = 1 AND chk.chkVoid = 0 AND chk.adBankAccount.baName = ?2 AND chk.chkAdCompany = ?3"
 *
 * @jboss:query signature="Collection findUnreconciledPostedChkByDateAndBaName(java.util.Date DT, java.lang.String BA_NM, java.lang.Integer CHK_AD_CMPNY)"
 *             query="SELECT OBJECT(chk) FROM ApCheck chk WHERE chk.chkReconciled = 0 AND chk.chkCheckDate <= ?1 AND chk.chkPosted = 1 AND chk.chkVoid = 0 AND chk.adBankAccount.baName = ?2 AND chk.chkAdCompany = ?3 ORDER BY chk.chkDate, chk.chkNumber"
 *
 * @ejb:finder signature="Collection findByVouCodeAndChkVoid(java.lang.Integer VOU_CODE, byte CHK_VD, java.lang.Integer CHK_AD_CMPNY)"
 *             query="SELECT OBJECT(chk) FROM ApCheck chk, IN(chk.apAppliedVouchers) av WHERE av.apVoucherPaymentSchedule.apVoucher.vouCode = ?1 AND chk.chkVoid = ?2 AND chk.chkAdCompany = ?3"
 *
 * @ejb:finder signature="Collection findByVouCodeAndChkVoidAndChkPosted(java.lang.Integer VOU_CODE, byte CHK_VD, byte CHK_PSTD, java.lang.Integer CHK_AD_CMPNY)"
 *             query="SELECT OBJECT(chk) FROM ApCheck chk, IN(chk.apAppliedVouchers) av WHERE av.apVoucherPaymentSchedule.apVoucher.vouCode = ?1 AND chk.chkVoid = ?2 AND chk.chkPosted = ?3 AND chk.chkAdCompany = ?4"
 *
 * @ejb:finder signature="Collection findByChkPostedAndChkVoidAndCbName(byte CHK_PSTD, byte CHK_VD, java.lang.String CB_NM, java.lang.Integer CHK_AD_CMPNY)"
 *             query="SELECT OBJECT(chk) FROM ApCheck chk WHERE chk.chkPosted=?1 AND chk.chkVoid = ?2 AND chk.apCheckBatch.cbName = ?3 AND chk.chkAdCompany = ?4"
 * 
 * @ejb:finder signature="Collection findRejectedChkByBrCodeAndChkCreatedBy(java.lang.Integer CHK_AD_BRNCH, java.lang.String CHK_CRTD_BY, java.lang.Integer CHK_AD_CMPNY)" 
 * 				query="SELECT OBJECT(chk) FROM ApCheck chk WHERE chk.chkType = 'PAYMENT' AND chk.chkVoid = 0 AND chk.chkApprovalStatus IS NULL AND chk.chkReasonForRejection IS NOT NULL AND chk.chkAdBranch = ?1 AND chk.chkCreatedBy=?2 AND chk.chkAdCompany = ?3"
 *
 * @ejb:finder signature="Collection findRejectedDirectChkByBrCodeAndChkCreatedBy(java.lang.Integer CHK_AD_BRNCH, java.lang.String CHK_CRTD_BY, java.lang.Integer CHK_AD_CMPNY)" 
 * 				query="SELECT OBJECT(chk) FROM ApCheck chk WHERE chk.chkType = 'DIRECT' AND chk.chkVoid = 0 AND chk.chkApprovalStatus IS NULL AND chk.chkReasonForRejection IS NOT NULL AND chk.chkAdBranch = ?1 AND chk.chkCreatedBy=?2 AND chk.chkAdCompany = ?3"
 *
 * @ejb:finder signature="Collection findPostedChkByChkTypeAndChkDateRange(java.lang.String CHK_TYP, java.util.Date RCT_DT_FRM, java.util.Date RCT_DT_TO, java.lang.Integer CHK_AD_CMPNY)"
 *             query="SELECT OBJECT(chk) FROM ApCheck chk  WHERE chk.chkPosted = 1 AND chk.chkVoid = 0 AND chk.chkType = ?1 AND chk.chkDate >= ?2 AND chk.chkDate <= ?3 AND chk.chkAdCompany = ?4"
 *
 * @jboss:query signature="Collection findPostedChkByChkTypeAndChkDateRange(java.lang.String CHK_TYP, java.util.Date RCT_DT_FRM, java.util.Date RCT_DT_TO, java.lang.Integer CHK_AD_CMPNY)"
 *             query="SELECT OBJECT(chk) FROM ApCheck chk  WHERE chk.chkPosted = 1 AND chk.chkVoid = 0 AND chk.chkType = ?1 AND chk.chkDate >= ?2 AND chk.chkDate <= ?3 AND chk.chkAdCompany = ?4 ORDER BY chk.chkDate"
 * 
 * @ejb:finder signature="Collection findByChkDateRangeAndWtcCode(java.util.Date CHK_DT_FRM, java.util.Date CHK_DT_TO, java.lang.Integer WTC_CODE, java.lang.Integer CHK_AD_CMPNY)"
 *             query="SELECT OBJECT(chk) FROM ApCheck chk, IN(chk.apDistributionRecords) dr WHERE chk.chkDate >= ?1 AND chk.chkDate <= ?2 AND chk.chkPosted = 1 AND chk.chkVoid = 0 AND chk.chkType = 'DIRECT' AND chk.apWithholdingTaxCode.wtcCode = ?3 AND dr.drClass='W-TAX' AND chk.chkAdCompany = ?4"
 * 
 * @jboss:query signature="Collection findByChkDateRangeAndWtcCode(java.util.Date CHK_DT_FRM, java.util.Date CHK_DT_TO, java.lang.Integer WTC_CODE, java.lang.Integer CHK_AD_CMPNY)"
 *             query="SELECT OBJECT(chk) FROM ApCheck chk, IN(chk.apDistributionRecords) dr WHERE chk.chkDate >= ?1 AND chk.chkDate <= ?2 AND chk.chkPosted = 1 AND chk.chkVoid = 0 AND chk.chkType = 'DIRECT' AND chk.apWithholdingTaxCode.wtcCode = ?3 AND dr.drClass='W-TAX' AND chk.chkAdCompany = ?4"
 * 
 * @ejb:finder signature="Collection findByCbName(java.lang.String CB_NM, java.lang.Integer CHK_AD_CMPNY)"
 *             query="SELECT OBJECT(chk) FROM ApCheck chk WHERE chk.apCheckBatch.cbName=?1 AND chk.chkAdCompany = ?2"
 * 
 * @jboss:query signature="Collection findByCbName(java.lang.String CB_NM, java.lang.Integer CHK_AD_CMPNY)"
 *             query="SELECT OBJECT(chk) FROM ApCheck chk WHERE chk.apCheckBatch.cbName=?1 AND chk.chkAdCompany = ?2 ORDER BY chk.chkNumber, chk.chkDate"
 * 
 * @ejb:finder signature="Collection findDraftChkAll(java.lang.Integer CHK_AD_CMPNY)"
 *             query="SELECT OBJECT(chk) FROM ApCheck chk WHERE chk.chkApprovalStatus IS NULL AND chk.chkVoid = 0 AND chk.chkAdCompany = ?1"
 * 
 * @ejb:finder signature="Collection findDraftChkByBrCode(java.lang.Integer CHK_AD_BRNCH, java.lang.Integer CHK_AD_CMPNY)"
 *             query="SELECT OBJECT(chk) FROM ApCheck chk WHERE chk.chkType = 'PAYMENT' AND chk.chkApprovalStatus IS NULL AND chk.chkReasonForRejection IS NULL AND chk.chkVoid = 0 AND chk.chkAdBranch = ?1 AND chk.chkAdCompany = ?2"
 *
 * @ejb:finder signature="Collection findDraftDirectChkByBrCode(java.lang.Integer CHK_AD_BRNCH, java.lang.Integer CHK_AD_CMPNY)"
 *             query="SELECT OBJECT(chk) FROM ApCheck chk WHERE chk.chkType = 'DIRECT' AND chk.chkApprovalStatus IS NULL AND chk.chkReasonForRejection IS NULL AND chk.chkVoid = 0 AND chk.chkAdBranch = ?1 AND chk.chkAdCompany = ?2"
 *
 * @ejb:finder signature="Collection findPostedChkByChkTypeAndChkDateRangeAndSplNameAndTcType(java.lang.String CHK_TYP, java.util.Date INV_DT_FRM, java.util.Date INV_DT_TO, java.lang.String SPL_NM, java.lang.String TC_TYP, java.lang.Integer CHK_AD_CMPNY)"
 *             query="SELECT OBJECT(chk) FROM ApCheck chk  WHERE chk.chkPosted = 1 AND chk.chkVoid = 0 AND chk.chkType = ?1 AND chk.chkDate >= ?2 AND chk.chkDate <= ?3 AND chk.apSupplier.splName = ?4 AND chk.apTaxCode.tcType = ?5 AND chk.chkAdCompany = ?6"
 *
 * @jboss:query signature="Collection findPostedChkByChkTypeAndChkDateRangeAndSplNameAndTcType(java.lang.String CHK_TYP, java.util.Date INV_DT_FRM, java.util.Date INV_DT_TO, java.lang.String SPL_NM, java.lang.String TC_TYP, java.lang.Integer CHK_AD_CMPNY)"
 *             query="SELECT OBJECT(chk) FROM ApCheck chk  WHERE chk.chkPosted = 1 AND chk.chkVoid = 0 AND chk.chkType = ?1 AND chk.chkDate >= ?2 AND chk.chkDate <= ?3 AND chk.apSupplier.splName = ?4 AND chk.apTaxCode.tcType = ?5 AND chk.chkAdCompany = ?6 ORDER BY chk.chkDate"
 * 
 * @ejb:finder signature="Collection findUnpostedChkByChkDateRange(java.util.Date CHK_DT_FRM, java.util.Date CHK_DT_TO, java.lang.Integer CHK_AD_CMPNY)"
 *             query="SELECT OBJECT(chk) FROM ApCheck chk  WHERE chk.chkPosted = 0 AND chk.chkVoid = 0 AND chk.chkDate >= ?1 AND chk.chkDate <= ?2 AND chk.chkAdCompany = ?3"
 *
 * @jboss:query signature="Collection findUnpostedChkByChkDateRange(java.util.Date CHK_DT_FRM, java.util.Date CHK_DT_TO, java.lang.Integer CHK_AD_CMPNY)"
 *             query="SELECT OBJECT(chk) FROM ApCheck chk  WHERE chk.chkPosted = 0 AND chk.chkVoid = 0 AND chk.chkDate >= ?1 AND chk.chkDate <= ?2 AND chk.chkAdCompany = ?3 ORDER BY chk.chkDate"
 * 
 * @ejb:finder signature="Collection findByAdCmpnyAll(java.lang.String CHK_TYP, java.lang.Integer CHK_AD_CMPNY)"
 *             query="SELECT OBJECT(chk) FROM ApCheck chk WHERE chk.chkType =?1 AND chk.chkAdCompany=?2"
 * 
 * @ejb:finder signature="Collection findByChkTypeAndCbName(java.lang.String CHK_TYP, java.lang.String CB_NM, java.lang.Integer CHK_AD_CMPNY)"
 *             query="SELECT OBJECT(chk) FROM ApCheck chk WHERE chk.chkType =?1 AND chk.apCheckBatch.cbName=?2 AND chk.chkAdCompany = ?3"
 *
 * @ejb:finder signature="Collection findPostedChkByBaNameAndChkDateRangeAndBrCode(java.lang.String BA_NM, java.util.Date CHK_DT_FRM, java.util.Date CHK_DT_TO, java.lang.Integer CHK_AD_BRNCH, java.lang.Integer CHK_AD_CMPNY)"
 *             query="SELECT OBJECT(chk) FROM ApCheck chk  WHERE chk.chkPosted = 1 AND chk.chkVoid = 0 AND chk.adBankAccount.baIsCashAccount = 0 AND chk.adBankAccount.baName = ?1 AND chk.chkCheckDate >= ?2 AND chk.chkCheckDate <= ?3 AND chk.chkAdBranch = ?4 AND chk.chkAdCompany = ?5"
 *                                                                                                      
 * @jboss:query signature="Collection findPostedChkByBaNameAndChkDateRangeAndBrCode(java.lang.String BA_NM, java.util.Date CHK_DT_FRM, java.util.Date CHK_DT_TO, java.lang.Integer CHK_AD_BRNCH, java.lang.Integer CHK_AD_CMPNY)"
 *             query="SELECT OBJECT(chk) FROM ApCheck chk  WHERE chk.chkPosted = 1 AND chk.chkVoid = 0 AND chk.adBankAccount.baIsCashAccount = 0 AND chk.adBankAccount.baName = ?1 AND chk.chkCheckDate >= ?2 AND chk.chkCheckDate <= ?3 AND chk.chkAdBranch = ?4 AND chk.chkAdCompany = ?5 ORDER BY chk.chkDate"
 *
 * @ejb:finder signature="Collection findReconciledPostedChkByBaNameAndChkDateRangeAndBrCode(java.lang.String BA_NM, java.util.Date CHK_DT_FRM, java.util.Date CHK_DT_TO, java.lang.Integer CHK_AD_BRNCH, java.lang.Integer CHK_AD_CMPNY)"
 *             query="SELECT OBJECT(chk) FROM ApCheck chk  WHERE chk.chkPosted = 1 AND chk.chkVoid = 0 AND chk.chkReconciled = 1 AND chk.adBankAccount.baIsCashAccount = 0 AND chk.adBankAccount.baName = ?1 AND chk.chkCheckDate >= ?2 AND chk.chkCheckDate <= ?3 AND chk.chkAdBranch = ?4 AND chk.chkAdCompany = ?5"
 *                                                                                                      
 * @jboss:query signature="Collection findReconciledPostedChkByBaNameAndChkDateRangeAndBrCode(java.lang.String BA_NM, java.util.Date CHK_DT_FRM, java.util.Date CHK_DT_TO, java.lang.Integer CHK_AD_BRNCH, java.lang.Integer CHK_AD_CMPNY)"
 *             query="SELECT OBJECT(chk) FROM ApCheck chk  WHERE chk.chkPosted = 1 AND chk.chkVoid = 0 AND chk.chkReconciled = 1 AND chk.adBankAccount.baIsCashAccount = 0 AND chk.adBankAccount.baName = ?1 AND chk.chkCheckDate >= ?2 AND chk.chkCheckDate <= ?3 AND chk.chkAdBranch = ?4 AND chk.chkAdCompany = ?5 ORDER BY chk.chkDate"
 *
 * @ejb:finder signature="Collection findChecksForReleaseByBrCode(java.lang.Integer CHK_AD_BRNCH, java.lang.Integer CHK_AD_CMPNY)"
 *             query="SELECT OBJECT(chk) FROM ApCheck chk  WHERE chk.chkPosted = 1 AND chk.chkVoid = 0 AND chk.chkAdBranch = ?1 AND chk.chkAdCompany = ?2"
 *                                                                                                      
 * @ejb:finder signature="Collection findChecksForPrintingByBrCode(java.lang.Integer CHK_AD_BRNCH, java.lang.Integer CHK_AD_CMPNY)"
 *             query="SELECT OBJECT(chk) FROM ApCheck chk  WHERE chk.chkPosted = 1 AND chk.chkApprovalStatus = 'APPROVED' AND chk.chkVoid = 0 AND chk.chkReleased = 1 AND chk.chkPrinted = 0 AND chk.chkAdBranch = ?1 AND chk.chkAdCompany = ?2"
 *
 * @jboss:query signature="Collection findChecksForReleaseByBrCode(java.lang.Integer CHK_AD_BRNCH, java.lang.Integer CHK_AD_CMPNY)"
 *             query="SELECT OBJECT(chk) FROM ApCheck chk  WHERE chk.chkPosted = 1 AND chk.chkReleased = 0 AND chk.chkVoid = 0 AND chk.chkAdBranch = ?1 AND chk.chkAdCompany = ?2"
 *
 * @ejb:finder signature="Collection findUnPostedChkByChkDateRangeAndSplCode(java.util.Date CHK_DT_FRM, java.util.Date CHK_DT_TO, java.lang.Integer SPL_CODE, java.lang.Integer CHK_AD_BRNCH, java.lang.Integer CHK_AD_CMPNY)"
 *             query="SELECT OBJECT(chk) FROM ApCheck chk WHERE chk.chkVoid = 0 AND chk.chkPosted = 0 AND chk.chkDate >= ?1 AND chk.chkDate <= ?2 AND chk.apSupplier.splCode = ?3 AND chk.chkAdBranch = ?4 AND chk.chkAdCompany = ?5 ORDER BY chk.chkDate, chk.chkDocumentNumber"
 *
 * @jboss:query signature="Collection findUnPostedChkByChkDateRangeAndSplCode(java.util.Date CHK_DT_FRM, java.util.Date CHK_DT_TO, java.lang.Integer SPL_CODE, java.lang.Integer CHK_AD_BRNCH, java.lang.Integer CHK_AD_CMPNY)"
 *             query="SELECT OBJECT(chk) FROM ApCheck chk WHERE chk.chkVoid = 0 AND chk.chkPosted = 0 AND chk.chkDate >= ?1 AND chk.chkDate <= ?2 AND chk.apSupplier.splCode = ?3 AND chk.chkAdBranch = ?4 AND chk.chkAdCompany = ?5 ORDER BY chk.chkDate, chk.chkDocumentNumber"
 * 
 * @ejb:finder signature="Collection findPostedChkByChkDateRangeAndSplCode(java.util.Date CHK_DT_FRM, java.util.Date CHK_DT_TO, java.lang.Integer SPL_CODE, java.lang.Integer CHK_AD_BRNCH, java.lang.Integer CHK_AD_CMPNY)"
 *             query="SELECT OBJECT(chk) FROM ApCheck chk WHERE chk.chkVoid = 0 AND chk.chkPosted = 1 AND chk.chkDate >= ?1 AND chk.chkDate <= ?2 AND chk.apSupplier.splCode = ?3 AND chk.chkAdBranch = ?4 AND chk.chkAdCompany = ?5 ORDER BY chk.chkDate, chk.chkDocumentNumber"
 *
 * @jboss:query signature="Collection findPostedChkByChkDateRangeAndSplCode(java.util.Date CHK_DT_FRM, java.util.Date CHK_DT_TO, java.lang.Integer SPL_CODE, java.lang.Integer CHK_AD_BRNCH, java.lang.Integer CHK_AD_CMPNY)"
 *             query="SELECT OBJECT(chk) FROM ApCheck chk WHERE chk.chkVoid = 0 AND chk.chkPosted = 1 AND chk.chkDate >= ?1 AND chk.chkDate <= ?2 AND chk.apSupplier.splCode = ?3 AND chk.chkAdBranch = ?4 AND chk.chkAdCompany = ?5 ORDER BY chk.chkDate, chk.chkDocumentNumber"
 *
 *
 * @ejb:finder signature="Collection findPostedTaxDirectChkByChkDateRange(java.util.Date CHK_DT_FRM, java.util.Date CHK_DT_TO, java.lang.Integer CHK_AD_BRNCH, java.lang.Integer CHK_AD_CMPNY)"
 *             query="SELECT OBJECT(chk) FROM ApCheck chk WHERE chk.chkVoid = 0 AND chk.chkPosted = 1 AND chk.apTaxCode.tcType IN ('EXEMPT','ZERO-RATED','INCLUSIVE','EXCLUSIVE') AND chk.chkDate >= ?1 AND chk.chkDate <= ?2 AND chk.chkAdBranch = ?3 AND chk.chkAdCompany = ?4 ORDER BY chk.chkDate, chk.chkDocumentNumber"
 *
 * @jboss:query signature="Collection findPostedTaxDirectChkByChkDateRange(java.util.Date CHK_DT_FRM, java.util.Date CHK_DT_TO, java.lang.Integer CHK_AD_BRNCH, java.lang.Integer CHK_AD_CMPNY)"
 *             query="SELECT OBJECT(chk) FROM ApCheck chk WHERE chk.chkVoid = 0 AND chk.chkPosted = 1 AND chk.apTaxCode.tcType IN ('EXEMPT','ZERO-RATED','INCLUSIVE','EXCLUSIVE') AND chk.chkDate >= ?1 AND chk.chkDate <= ?2 AND chk.chkAdBranch = ?3 AND chk.chkAdCompany = ?4 ORDER BY chk.chkDate, chk.chkDocumentNumber"
 *
 * @ejb:select signature="Collection ejbSelectGeneric(java.lang.String jbossQl, java.lang.Object[] args)"
 *             result-type-mapping="Local"
 *             method-intf="LocalHome"
 *             query="SELECT OBJECT(chk) FROM ApCheck chk"
 *
 * @jboss:query signature="Collection ejbSelectGeneric(java.lang.String jbossQl, java.lang.Object[] args)"
 *                         dynamic="true"
 *
 *
 * @ejb:value-object match="*"
 *             name="ApCheck"
 *
 * @jboss:persistence table-name="AP_CHCK"
 *
 * @jboss:entity-command name="mysql-get-generated-keys"
 */

public abstract class ApCheckBean extends AbstractEntityBean {
    
    
    // PERSISTENT METHODS
    
    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     * @ejb:pk-field
     *
     * @jboss:column-name name="CHK_CODE"
     *
     * @jboss:persistence auto-increment="true"
     **/
    public abstract Integer getChkCode();
    public abstract void setChkCode(Integer CHK_CODE);
     
    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="CHK_TYP"
     **/
     public abstract String getChkType();
     /**
      * @ejb:interface-method view-type="local"
      **/ 
     public abstract void setChkType(String CHK_TYP);

    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="CHK_DESC"
     **/
     public abstract String getChkDescription();
     /**
      * @ejb:interface-method view-type="local"
      **/
     public abstract void setChkDescription(String CHK_DESC);

    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="CHK_DT"
     **/
     public abstract Date getChkDate();
     /**
      * @ejb:interface-method view-type="local"
      **/
     public abstract void setChkDate(Date CHK_DT);
     
     /**
      * @ejb:persistent-field
      * @ejb:interface-method view-type="local"
      *
      * @jboss:column-name name="CHK_CHCK_DT"
      **/
      public abstract Date getChkCheckDate();
      /**
       * @ejb:interface-method view-type="local"
       **/
      public abstract void setChkCheckDate(Date CHK_CHCK_DT);

    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="CHK_NMBR"
     **/
     public abstract String getChkNumber();
     /**
      * @ejb:interface-method view-type="local"
      **/
     public abstract void setChkNumber(String CHK_NMBR);
     
    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="CHK_DCMNT_NMBR"
     **/
     public abstract String getChkDocumentNumber();
     /**
      * @ejb:interface-method view-type="local"
      **/
     public abstract void setChkDocumentNumber(String CHK_DCMNT_NMBR);
     
    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="CHK_RFRNC_NMBR"
     **/
     public abstract String getChkReferenceNumber();
     /**
      * @ejb:interface-method view-type="local"
      **/
     public abstract void setChkReferenceNumber(String CHK_RFRNC_NMBR);
     
     /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="CHK_INFO_TYP"
     **/
     public abstract String getChkInfoType();
     /**
      * @ejb:interface-method view-type="local"
      **/
     public abstract void setChkInfoType(String CHK_INFO_TYP);
     
      /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="CHK_INFO_BIO_NMBR"
     **/
     public abstract String getChkInfoBioNumber();
     /**
      * @ejb:interface-method view-type="local"
      **/
     public abstract void setChkInfoBioNumber(String CHK_INFO_BIO_NMBR);
     
        /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="CHK_INFO_BIO_DESC"
     **/
     public abstract String getChkInfoBioDescription();
     /**
      * @ejb:interface-method view-type="local"
      **/
     public abstract void setChkInfoBioDescription(String CHK_INFO_BIO_DESC);
     
         /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="CHK_INFO_TYP_STATUS"
     **/
     public abstract String getChkInfoTypeStatus();
     /**
      * @ejb:interface-method view-type="local"
      **/
     public abstract void setChkInfoTypeStatus(String CHK_INFO_TYP_STATUS);
     
     
         /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="CHK_INFO_RQST_STATUS"
     **/
     public abstract String getChkInfoRequestStatus();
     /**
      * @ejb:interface-method view-type="local"
      **/
     public abstract void setChkInfoRequestStatus(String CHK_INFO_RQST_STATUS);
     

	  /**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="CHK_INVT_IS"
	 **/
	 public abstract byte getChkInvtInscribedStock();
	 /**
	  * @ejb:interface-method view-type="local"
	  **/
	 public abstract void setChkInvtInscribedStock(byte CHK_INVT_IS);
	     
	 /**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="CHK_INVT_TB"
	 **/
	 public abstract byte getChkInvtTreasuryBill();
	 /**
	  * @ejb:interface-method view-type="local"
	  **/
	 public abstract void setChkInvtTreasuryBill(byte CHK_INVT_TB);
	 
	 
	 
	 /**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="CHK_INVT_NXT_RN_DT"
	 **/
	 public abstract Date getChkInvtNextRunDate();
	 /**
	  * @ejb:interface-method view-type="local"
	  **/
	 public abstract void setChkInvtNextRunDate(Date CHK_INVT_NXT_RN_DT);
	     
	     
	      
	  /**
	   * @ejb:persistent-field
	   * @ejb:interface-method view-type="local"
	   *
	   * @jboss:column-name name="CHK_INVT_STTLMNT_DT"
	   **/
	   public abstract Date getChkInvtSettlementDate();
	   /**
	    * @ejb:interface-method view-type="local"
	    **/
	   public abstract void setChkInvtSettlementDate(Date CHK_INVT_STTLMNT_DT);
	   
	   /**
	   * @ejb:persistent-field
	   * @ejb:interface-method view-type="local"
	   *
	   * @jboss:column-name name="CHK_INVT_MTRTY_DT"
	   **/
	   public abstract Date getChkInvtMaturityDate();
	   /**
	    * @ejb:interface-method view-type="local"
		**/
	   public abstract void setChkInvtMaturityDate(Date CHK_INVT_MTRTY_DT);
		   
		   
		/**
		 * @ejb:persistent-field
		 * @ejb:interface-method view-type="local"
		 *
		 * @jboss:column-name name="CHK_INVT_BD_YLD"
		 **/
		 public abstract double getChkInvtBidYield();
		 /**
		  * @ejb:interface-method view-type="local"
		  **/
		 public abstract void setChkInvtBidYield(double CHK_INVT_BD_YLD);
		 
	 
	 
		 /**
		 * @ejb:persistent-field
		 * @ejb:interface-method view-type="local"
		 *
		 * @jboss:column-name name="CHK_INVT_CPN_RT"
		 **/
		 public abstract double getChkInvtCouponRate();
		 /**
		  * @ejb:interface-method view-type="local"
		  **/
		 public abstract void setChkInvtCouponRate(double CHK_INVT_CPN_RT);
	     
	     
		 /**
		 * @ejb:persistent-field
		 * @ejb:interface-method view-type="local"
		 *
		 * @jboss:column-name name="CHK_INVT_STTLMNT_AMNT"
		 **/
		 public abstract double getChkInvtSettleAmount();
		 /**
		  * @ejb:interface-method view-type="local"
		  **/
		 public abstract void setChkInvtSettleAmount(double CHK_INVT_STTLMNT_AMNT);
		     
		 /**
		 * @ejb:persistent-field
		 * @ejb:interface-method view-type="local"
		 *
		 * @jboss:column-name name="CHK_INVT_FC_VL"
		 **/
		 public abstract double getChkInvtFaceValue();
		 /**
		  * @ejb:interface-method view-type="local"
		  **/
		 public abstract void setChkInvtFaceValue(double CHK_INVT_FC_VL);
	 
			     
		 /**
		 * @ejb:persistent-field
		 * @ejb:interface-method view-type="local"
		 *
		 * @jboss:column-name name="CHK_INVT_PM_AMNT"
		 **/
		 public abstract double getChkInvtPremiumAmount();
		 /**
		  * @ejb:interface-method view-type="local"
		  **/
		 public abstract void setChkInvtPremiumAmount(double CHK_INVT_PM_AMNT);
		 
		 
		 
		 
	 /**
		 * @ejb:persistent-field
		 * @ejb:interface-method view-type="local"
		 *
		 * @jboss:column-name name="CHK_LN"
		 **/
		 public abstract byte getChkLoan();
		 /**
		  * @ejb:interface-method view-type="local"
		  **/
		 public abstract void setChkLoan(byte CHK_LN);
		 
		 
	 /**
		 * @ejb:persistent-field
		 * @ejb:interface-method view-type="local"
		 *
		 * @jboss:column-name name="CHK_LN_GNRTD"
		 **/
		 public abstract byte getChkLoanGenerated();
		 /**
		  * @ejb:interface-method view-type="local"
		  **/
		 public abstract void setChkLoanGenerated(byte CHK_LN_GNRTD);


		
		     
	      

    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="CHK_CNVRSN_DT"
     **/
     public abstract Date getChkConversionDate();
     /**
      * @ejb:interface-method view-type="local"
      **/
     public abstract void setChkConversionDate(Date CHK_CNVRSN_DT);

    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="CHK_CNVRSN_RT"
     **/
     public abstract double getChkConversionRate();
     /**
      * @ejb:interface-method view-type="local"
      **/
     public abstract void setChkConversionRate(double CHK_CNVRSN_RT);
     
     
     /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="CHK_BLL_AMNT"
     **/
     public abstract double getChkBillAmount();
     /**
      * @ejb:interface-method view-type="local"
      **/
     public abstract void setChkBillAmount(double CHK_BLL_AMNT);

    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="CHK_AMNT"
     **/
     public abstract double getChkAmount();
     /**
      * @ejb:interface-method view-type="local"
      **/
     public abstract void setChkAmount(double CHK_AMNT);

    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="CHK_APPRVL_STATUS"
     **/
     public abstract String getChkApprovalStatus();
     /**
      * @ejb:interface-method view-type="local"
      **/
     public abstract void setChkApprovalStatus(String CHK_APPRVL_STATUS);
     
    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="CHK_RSN_FR_RJCTN"
     **/
     public abstract String getChkReasonForRejection();
     /**
      * @ejb:interface-method view-type="local"
      **/
     public abstract void setChkReasonForRejection(String CHK_RSN_FR_RJCTN);     
     
     /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="CHK_PSTD"
     **/
     public abstract byte getChkPosted();
     /**
      * @ejb:interface-method view-type="local"
      **/
     public abstract void setChkPosted(byte CHK_PSTD);
     
    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="CHK_VD"
     **/
     public abstract byte getChkVoid();
     /**
      * @ejb:interface-method view-type="local"
      **/
     public abstract void setChkVoid(byte CHK_VD);
     
     /**
      * @ejb:persistent-field
      * @ejb:interface-method view-type="local"
      *
      * @jboss:column-name name="CHK_CRSS_CHCK"
      **/
      public abstract byte getChkCrossCheck();
      /**
       * @ejb:interface-method view-type="local"
       **/
      public abstract void setChkCrossCheck(byte CHK_CRSS_CHCK);
     
     /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="CHK_VD_APPRVL_STATUS"
     **/
     public abstract String getChkVoidApprovalStatus();
     /**
      * @ejb:interface-method view-type="local"
      **/
     public abstract void setChkVoidApprovalStatus(String CHK_VD_APPRVL_STATUS);
     
     /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="CHK_VD_PSTD"
     **/
     public abstract byte getChkVoidPosted();
     /**
      * @ejb:interface-method view-type="local"
      **/
     public abstract void setChkVoidPosted(byte CHK_VD_PSTD);
     
     /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="CHK_CRTD_BY"
     **/
     public abstract String getChkCreatedBy();
     /**
      * @ejb:interface-method view-type="local"
      **/
     public abstract void setChkCreatedBy(String CHK_CRTD_BY);

    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="CHK_DT_CRTD"
     **/
     public abstract Date getChkDateCreated();
     /**
      * @ejb:interface-method view-type="local"
      **/
     public abstract void setChkDateCreated(Date CHK_DT_CRTD);

    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="CHK_LST_MDFD_BY"
     **/
     public abstract String getChkLastModifiedBy();
     /**
      * @ejb:interface-method view-type="local"
      **/
     public abstract void setChkLastModifiedBy(String CHK_LST_MDFD_BY);

    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="CHK_DT_LST_MDFD"
     **/
     public abstract Date getChkDateLastModified();
     /**
      * @ejb:interface-method view-type="local"
      **/
     public abstract void setChkDateLastModified(Date CHK_DT_LST_MDFD);

    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="CHK_APPRVD_RJCTD_BY"
     **/
     public abstract String getChkApprovedRejectedBy();
     /**
      * @ejb:interface-method view-type="local"
      **/
     public abstract void setChkApprovedRejectedBy(String CHK_APPRVD_RJCTD_BY);

    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="CHK_DT_APPRVD_RJCTD"
     **/
     public abstract Date getChkDateApprovedRejected();
     /**
      * @ejb:interface-method view-type="local"
      **/
     public abstract void setChkDateApprovedRejected(Date CHK_DT_APPRVD_RJCTD);

    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="CHK_PSTD_BY"
     **/
     public abstract String getChkPostedBy();
     /**
      * @ejb:interface-method view-type="local"
      **/
     public abstract void setChkPostedBy(String CHK_PSTD_BY);

    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="CHK_DT_PSTD"
     **/
     public abstract Date getChkDatePosted();
     /**
      * @ejb:interface-method view-type="local"
      **/
     public abstract void setChkDatePosted(Date CHK_DT_PSTD);
     
     /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="CHK_RCNCLD"
     **/
     public abstract byte getChkReconciled();
     /**
      * @ejb:interface-method view-type="local"
      **/
     public abstract void setChkReconciled(byte CHK_RCNCLD);  
     
     /**
      * @ejb:persistent-field
      * @ejb:interface-method view-type="local"
      *
      * @jboss:column-name name="CHK_DT_RCNCLD"
      **/
     public abstract Date getChkDateReconciled();
     /**
      * @ejb:interface-method view-type="local"
      **/
     public abstract void setChkDateReconciled(Date CHK_DT_RCNCLD);  
      
     /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="CHK_RLSD"
     **/
     public abstract byte getChkReleased();
     /**
      * @ejb:interface-method view-type="local"
      **/
     public abstract void setChkReleased(byte CHK_RLSD);  
     
     
     /**
      * @ejb:persistent-field
      * @ejb:interface-method view-type="local"
      *
      * @jboss:column-name name="CHK_DT_RLSD"
      **/
      public abstract Date getChkDateReleased();
      /**
       * @ejb:interface-method view-type="local"
       **/
      public abstract void setChkDateReleased(Date CHK_DT_RLSD);  

      
     /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="CHK_PRNTD"
     **/
     public abstract byte getChkPrinted();
     /**
      * @ejb:interface-method view-type="local"
      **/
     public abstract void setChkPrinted(byte CHK_PRNTD);
     
     
     /**
      * @ejb:persistent-field
      * @ejb:interface-method view-type="local"
      *
      * @jboss:column-name name="CHK_CV_PRNTD"
      **/
      public abstract byte getChkCvPrinted();
      /**
       * @ejb:interface-method view-type="local"
       **/
      public abstract void setChkCvPrinted(byte CHK_CV_PRNTD);

      
      /**
       * @ejb:persistent-field
       * @ejb:interface-method view-type="local"
       *
       * @jboss:column-name name="CHK_MMO"
       **/
      public abstract String getChkMemo();
      /**
       * @ejb:interface-method view-type="local"
       **/
      public abstract void setChkMemo(String CHK_MMO);
      
      /**
       * @ejb:persistent-field
       * @ejb:interface-method view-type="local"
       *
       * @jboss:column-name name="CHK_SPL_NM"
       **/
      public abstract String getChkSupplierName();
      /**
       * @ejb:interface-method view-type="local"
       **/
      public abstract void setChkSupplierName(String CHK_SPL_NM);
      
      
      
      


      /**
       * @ejb:persistent-field
       * @ejb:interface-method view-type="local"
       *
       * @jboss:column-name name="CHK_AD_BRNCH"
       **/
       public abstract Integer getChkAdBranch();
       /**
        * @ejb:interface-method view-type="local"
        **/
       public abstract void setChkAdBranch(Integer CHK_AD_BRNCH);

      /**
       * @ejb:persistent-field
       * @ejb:interface-method view-type="local"
       *
       * @jboss:column-name name="CHK_AD_CMPNY"
       **/
       public abstract Integer getChkAdCompany();
       /**
        * @ejb:interface-method view-type="local"
        **/
       public abstract void setChkAdCompany(Integer CHK_AD_CMPNY);

    // RELATIONSHIP METHODS    

     /**
      * @ejb:interface-method view-type="local"
      * @ejb:relation name="supplier-checks"
      *               role-name="check-has-one-supplier"
      *
      * @jboss:relation related-pk-field="splCode"
      *                 fk-column="AP_SUPPLIER"
      */
     public abstract LocalApSupplier getApSupplier();
     /**
      * @ejb:interface-method view-type="local"
      **/
     public abstract void setApSupplier(LocalApSupplier apSupplier);
     
     /**
      * @ejb:interface-method view-type="local"
      * @ejb:relation name="bankaccount-checks"
      *               role-name="check-has-one-bankaccount"
      *
      * @jboss:relation related-pk-field="baCode"
      *                 fk-column="AD_BANK_ACCOUNT"
      */
     public abstract com.ejb.ad.LocalAdBankAccount getAdBankAccount();
     /**
      * @ejb:interface-method view-type="local"
      **/
     public abstract void setAdBankAccount(com.ejb.ad.LocalAdBankAccount adBankAccount);
     
     /**
      * @ejb:interface-method view-type="local"
      * @ejb:relation name="paymentterm-checks"
      *               role-name="check-has-one-paymentterm"
      *
      * @jboss:relation related-pk-field="pytCode"
      *                 fk-column="AD_PAYMENT_TERM"
      */
     public abstract com.ejb.ad.LocalAdPaymentTerm getAdPaymentTerm();
     /**
      * @ejb:interface-method view-type="local"
      **/
     public abstract void setAdPaymentTerm(com.ejb.ad.LocalAdPaymentTerm adPaymentTerm);
     
     /**
      * @ejb:interface-method view-type="local"
      * @ejb:relation name="aptaxcode-checks"
      *               role-name="check-has-one-aptaxcode"
      *
      * @jboss:relation related-pk-field="tcCode"
      *                 fk-column="AP_TAX_CODE"
      */
     public abstract LocalApTaxCode getApTaxCode();
     /**
      * @ejb:interface-method view-type="local"
      **/
     public abstract void setApTaxCode(LocalApTaxCode apTaxCode);

     /**
      * @ejb:interface-method view-type="local"
      * @ejb:relation name="withholdingtaxcode-checks"
      *               role-name="check-has-one-withholdingtaxcode"
      *
      * @jboss:relation related-pk-field="wtcCode"
      *                 fk-column="AP_WITHHOLDING_TAX_CODE"
      */
     public abstract LocalApWithholdingTaxCode getApWithholdingTaxCode();
     /**
      * @ejb:interface-method view-type="local"
      **/
     public abstract void setApWithholdingTaxCode(LocalApWithholdingTaxCode apWithholdingTaxCode);

     /**
      * @ejb:interface-method view-type="local"
      * @ejb:relation name="functionalcurrency-checks"
      *               role-name="check-has-one-functionalcurrency"
      *
      * @jboss:relation related-pk-field="fcCode"
      *                 fk-column="GL_FUNCTIONAL_CURRENCY"
      */
     public abstract com.ejb.gl.LocalGlFunctionalCurrency getGlFunctionalCurrency();
     /**
      * @ejb:interface-method view-type="local"
      **/
     public abstract void setGlFunctionalCurrency(com.ejb.gl.LocalGlFunctionalCurrency glFunctionalCurrency);
     
     
    /**
      * @ejb:interface-method view-type="local"
      * @ejb:relation name="checkbatch-checks"
      *               role-name="check-has-one-checkbatch"
      *
      * @jboss:relation related-pk-field="cbCode"
      *                 fk-column="AP_CHECK_BATCH"
      */
     public abstract LocalApCheckBatch getApCheckBatch();
     /**
      * @ejb:interface-method view-type="local"
      **/
     public abstract void setApCheckBatch(LocalApCheckBatch apCheckBatch);
     
     

     /**
      * @ejb:interface-method view-type="local"
      * @ejb:relation name="check-appliedvouchers"
      *               role-name="check-has-many-appliedvouchers"
      * 
      */
     public abstract Collection getApAppliedVouchers();
     public abstract void setApAppliedVouchers(Collection apAppliedVouchers);

     /**
      * @ejb:interface-method view-type="local"
      * @ejb:relation name="check-apdistributionrecords"
      *               role-name="check-has-many-apdistributionrecords"
      * 
      */
     public abstract Collection getApDistributionRecords();
     public abstract void setApDistributionRecords(Collection apDistributionRecords);
     
     /**
      * @ejb:interface-method view-type="local"
      * @ejb:relation name="check-voucherlineitems"
      *               role-name="check-has-many-voucherlineitems"
      * 
      */
     public abstract Collection getApVoucherLineItems();
     public abstract void setApVoucherLineItems(Collection apVoucherLineItems);


   /**
    * @jboss:dynamic-ql
    */
    public abstract Collection ejbSelectGeneric(java.lang.String jbossQl, java.lang.Object[] args)
       throws FinderException;

   // BUSINESS METHODS
   
   /**
    * @ejb:home-method view-type="local"
    */
    public Collection ejbHomeGetChkByCriteria(java.lang.String jbossQl, java.lang.Object[] args)
       throws FinderException {
       	
       return ejbSelectGeneric(jbossQl, args);
    }
     
     /**
      * @ejb:interface-method view-type="local"
      **/
     public void addApAppliedVoucher(LocalApAppliedVoucher apAppliedVoucher) {

         Debug.print("ApCheckBean addApAppliedVoucher");
      
         try {
      	
            Collection apAppliedVouchers = getApAppliedVouchers();
	        apAppliedVouchers.add(apAppliedVoucher);
	          
         } catch (Exception ex) {
      	
           throw new EJBException(ex.getMessage());
      
         }   
     }

    /**
     * @ejb:interface-method view-type="local"
     **/
    public void dropApAppliedVoucher(LocalApAppliedVoucher apAppliedVoucher) {

         Debug.print("ApCheckBean dropApAppliedVoucher");
      
         try {
      	
            Collection apAppliedVouchers = getApAppliedVouchers();
	        apAppliedVouchers.remove(apAppliedVoucher);
	      
         } catch (Exception ex) {
      	
           throw new EJBException(ex.getMessage());
         
         }   
     }
     
    /**
     * @ejb:interface-method view-type="local"
     **/
    public void addApDistributionRecord(LocalApDistributionRecord apDistributionRecord) {

         Debug.print("ApCheckBean addApDistributionRecord");
      
         try {
      	
            Collection apDistributionRecords = getApDistributionRecords();
	        apDistributionRecords.add(apDistributionRecord);
	          
         } catch (Exception ex) {
      	
           throw new EJBException(ex.getMessage());
      
         }   
     }

    /**
     * @ejb:interface-method view-type="local"
     **/
    public void dropApDistributionRecord(LocalApDistributionRecord apDistributionRecord) {

         Debug.print("ApCheckBean dropApDistributionRecord");
      
         try {
      	
            Collection apDistributionRecords = getApDistributionRecords();
	        apDistributionRecords.remove(apDistributionRecord);
	      
         } catch (Exception ex) {
      	
           throw new EJBException(ex.getMessage());
         
         }   
     }
     
     /**
     * @ejb:interface-method view-type="local"
     **/
    public void addApVoucherLineItem(LocalApVoucherLineItem apVoucherLineItem) {

         Debug.print("ApCheckBean addApVoucherLineItem");
      
         try {
      	
            Collection apVoucherLineItems = getApVoucherLineItems();
	        apVoucherLineItems.add(apVoucherLineItem);
	          
         } catch (Exception ex) {
      	
           throw new EJBException(ex.getMessage());
      
         }   
     }

    /**
     * @ejb:interface-method view-type="local"
     **/
    public void dropApVoucherLineItem(LocalApVoucherLineItem apVoucherLineItem) {

         Debug.print("ApCheckBean dropApDistributionRecord");
      
         try {
      	
            Collection apVoucherLineItems = getApVoucherLineItems();
	        apVoucherLineItems.remove(apVoucherLineItem);
	      
         } catch (Exception ex) {
      	
           throw new EJBException(ex.getMessage());
         
         }   
     }
     
    /**
     * @ejb:interface-method view-type="local"
     **/
    public short getApDrNextLine() {

       Debug.print("ApCheckBean getApDrNextLine");
      
       try {
      	
          Collection apDistributionRecords = getApDistributionRecords();
	      return (short)(apDistributionRecords.size() + 1);
	     
       } catch (Exception ex) {
      	
          throw new EJBException(ex.getMessage());
         
       }
    }       

                     
     // ENTITY METHODS
    
     /**
      * @ejb:create-method view-type="local"
      **/
     public Integer ejbCreate(Integer CHK_CODE, 
         String CHK_TYP, String CHK_DESC, Date CHK_DT, Date CHK_CHCK_DT, String CHK_NMBR, String CHK_DCMNT_NMBR,
	     String CHK_RFRNC_NMBR, String CHK_INFO_TYP, String CHK_INFO_BIO_NMBR, String CHK_INFO_BIO_DESC, String CHK_INFO_TYP_STATUS, String CHK_INFO_RQST_STATUS,
	     byte CHK_INVT_IS, byte CHK_INVT_TB, Date CHK_INVT_NXT_RN_DT, Date CHK_INVT_STTLMNT_DT, Date CHK_INVT_MTRTY_DT, double CHK_INVT_BD_YLD, double CHK_INVT_CPN_RT, double CHK_INVT_STTLMNT_AMNT, double CHK_INVT_FC_VL, double CHK_INVT_PM_AMNT,
	     byte CHK_LN, byte CHK_LN_GNRTD,
	     Date CHK_CNVRSN_DT, double CHK_CNVRSN_RT,
	     double CHK_BLL_AMNT, double CHK_AMNT, String CHK_APPRVL_STATUS, String CHK_RSN_FR_RJCTN,byte CHK_PSTD, byte CHK_VD,
	     byte CHK_CRSS_CHCK, String CHK_VD_APPRVL_STATUS, byte CHK_VD_PSTD,
	     String CHK_CRTD_BY, Date CHK_DT_CRTD, String CHK_LST_MDFD_BY,
	     Date CHK_DT_LST_MDFD, String CHK_APPRVD_RJCTD_BY, Date CHK_DT_APPRVD_RJCTD,
	     String CHK_PSTD_BY, Date CHK_DT_PSTD, byte CHK_RCNCLD, Date CHK_DT_RCNCLD, byte CHK_RLSD, Date CHK_DT_RLSD, byte CHK_PRNTD, byte CHK_CV_PRNTD,
	     String CHK_MMO, String CHK_SPL_NM, Integer CHK_AD_BRNCH, Integer CHK_AD_CMPNY)
         throws CreateException {
           
         Debug.print("ApCheckBean ejbCreate");
        
         setChkCode(CHK_CODE);
         setChkType(CHK_TYP);
         setChkDescription(CHK_DESC);
         setChkDate(CHK_DT);
         setChkCheckDate(CHK_CHCK_DT);
         setChkNumber(CHK_NMBR);
         setChkDocumentNumber(CHK_DCMNT_NMBR);
         setChkInfoType(CHK_INFO_TYP);
         setChkInfoBioNumber(CHK_INFO_BIO_NMBR);
         setChkInfoBioDescription(CHK_INFO_BIO_DESC);
         setChkInfoTypeStatus(CHK_INFO_TYP_STATUS);
         setChkInfoRequestStatus(CHK_INFO_RQST_STATUS);
         setChkInvtInscribedStock(CHK_INVT_IS);
         setChkInvtTreasuryBill(CHK_INVT_TB);
         setChkInvtNextRunDate(CHK_INVT_NXT_RN_DT);
         setChkInvtSettlementDate(CHK_INVT_STTLMNT_DT);
         setChkInvtMaturityDate(CHK_INVT_MTRTY_DT);
         setChkInvtBidYield(CHK_INVT_BD_YLD);
         setChkInvtCouponRate(CHK_INVT_CPN_RT);
         setChkInvtSettleAmount(CHK_INVT_STTLMNT_AMNT);
         setChkInvtFaceValue(CHK_INVT_FC_VL);
         setChkInvtPremiumAmount(CHK_INVT_PM_AMNT);
         
         setChkLoan(CHK_LN);
         setChkLoanGenerated(CHK_LN_GNRTD);
         setChkReferenceNumber(CHK_RFRNC_NMBR);
         setChkConversionDate(CHK_CNVRSN_DT);
         setChkConversionRate(CHK_CNVRSN_RT);                 
         setChkBillAmount(CHK_BLL_AMNT);
         setChkAmount(CHK_AMNT);
         setChkApprovalStatus(CHK_APPRVL_STATUS);
         setChkReasonForRejection(CHK_RSN_FR_RJCTN);
         setChkPosted(CHK_PSTD);
         setChkVoid(CHK_VD);
         setChkCrossCheck(CHK_CRSS_CHCK);
         setChkVoidApprovalStatus(CHK_VD_APPRVL_STATUS);
         setChkVoidPosted(CHK_VD_PSTD);
         setChkCreatedBy(CHK_CRTD_BY);
         setChkDateCreated(CHK_DT_CRTD);
         setChkLastModifiedBy(CHK_LST_MDFD_BY);
         setChkDateLastModified(CHK_DT_LST_MDFD);
         setChkApprovedRejectedBy(CHK_APPRVD_RJCTD_BY);
         setChkDateApprovedRejected(CHK_DT_APPRVD_RJCTD);
         setChkPostedBy(CHK_PSTD_BY);
         setChkDatePosted(CHK_DT_PSTD);
         setChkReconciled(CHK_RCNCLD);
         setChkDateReconciled(CHK_DT_RCNCLD);
         setChkReleased(CHK_RLSD);
         setChkDateReleased(CHK_DT_RLSD);
         setChkPrinted(CHK_PRNTD);
         setChkCvPrinted(CHK_CV_PRNTD);
         setChkMemo(CHK_MMO);
         setChkSupplierName(CHK_SPL_NM);
         setChkAdBranch(CHK_AD_BRNCH);
         setChkAdCompany(CHK_AD_CMPNY);
                
         return null;
        
     }
    
     /**
      * @ejb:create-method view-type="local"
      **/
     public Integer ejbCreate(
         String CHK_TYP, String CHK_DESC, Date CHK_DT, Date CHK_CHCK_DT, String CHK_NMBR, String CHK_DCMNT_NMBR,
	     String CHK_RFRNC_NMBR, String CHK_INFO_TYP, String CHK_INFO_BIO_NMBR, String CHK_INFO_BIO_DESC, String CHK_INFO_TYP_STATUS, String CHK_INFO_RQST_STATUS,
	     byte CHK_INVT_IS, byte CHK_INVT_TB, Date CHK_INVT_NXT_RN_DT, Date CHK_INVT_STTLMNT_DT, Date CHK_INVT_MTRTY_DT, double CHK_INVT_BD_YLD, double CHK_INVT_CPN_RT, double CHK_INVT_STTLMNT_AMNT, double CHK_INVT_FC_VL, double CHK_INVT_PM_AMNT,
	     byte CHK_LN, byte CHK_LN_GNRTD,
	     Date CHK_CNVRSN_DT, double CHK_CNVRSN_RT,
	     double CHK_BLL_AMNT, double CHK_AMNT, String CHK_APPRVL_STATUS, String CHK_RSN_FR_RJCTN, byte CHK_PSTD, byte CHK_VD,
	     byte CHK_CRSS_CHCK, String CHK_VD_APPRVL_STATUS, byte CHK_VD_PSTD,
	     String CHK_CRTD_BY, Date CHK_DT_CRTD, String CHK_LST_MDFD_BY,
	     Date CHK_DT_LST_MDFD, String CHK_APPRVD_RJCTD_BY, Date CHK_DT_APPRVD_RJCTD,
	     String CHK_PSTD_BY, Date CHK_DT_PSTD, byte CHK_RCNCLD, Date CHK_DT_RCNCLD, byte CHK_RLSD, Date CHK_DT_RLSD, byte CHK_PRNTD, byte CHK_CV_PRNTD,
	     String CHK_MMO, String CHK_SPL_NM, Integer CHK_AD_BRNCH, Integer CHK_AD_CMPNY)
         throws CreateException {
           
         Debug.print("ApCheckBean ejbCreate");
        
         setChkType(CHK_TYP);
         setChkDescription(CHK_DESC);
         setChkDate(CHK_DT);
         setChkCheckDate(CHK_CHCK_DT);
         setChkNumber(CHK_NMBR);
         setChkDocumentNumber(CHK_DCMNT_NMBR);
         setChkReferenceNumber(CHK_RFRNC_NMBR);
         setChkInfoType(CHK_INFO_TYP);
         setChkInfoBioNumber(CHK_INFO_BIO_NMBR);
         setChkInfoBioDescription(CHK_INFO_BIO_DESC);
         setChkInfoTypeStatus(CHK_INFO_TYP_STATUS);
         setChkInfoRequestStatus(CHK_INFO_RQST_STATUS);
         setChkInvtInscribedStock(CHK_INVT_IS);
         setChkInvtTreasuryBill(CHK_INVT_TB);
         setChkInvtNextRunDate(CHK_INVT_NXT_RN_DT);
         setChkInvtSettlementDate(CHK_INVT_STTLMNT_DT);
         setChkInvtMaturityDate(CHK_INVT_MTRTY_DT);
         setChkInvtBidYield(CHK_INVT_BD_YLD);
         setChkInvtCouponRate(CHK_INVT_CPN_RT);
         setChkInvtSettleAmount(CHK_INVT_STTLMNT_AMNT);
         setChkInvtFaceValue(CHK_INVT_FC_VL);
         setChkInvtPremiumAmount(CHK_INVT_PM_AMNT);
         setChkLoan(CHK_LN);
         setChkLoanGenerated(CHK_LN_GNRTD);  
         setChkConversionDate(CHK_CNVRSN_DT);
         setChkConversionRate(CHK_CNVRSN_RT);                 
         setChkBillAmount(CHK_BLL_AMNT);
         setChkAmount(CHK_AMNT);
         setChkApprovalStatus(CHK_APPRVL_STATUS);
         setChkReasonForRejection(CHK_RSN_FR_RJCTN);
         setChkPosted(CHK_PSTD);
         setChkVoid(CHK_VD);
         setChkCrossCheck(CHK_CRSS_CHCK);
         setChkVoidApprovalStatus(CHK_VD_APPRVL_STATUS);
         setChkVoidPosted(CHK_VD_PSTD);
         setChkCreatedBy(CHK_CRTD_BY);
         setChkDateCreated(CHK_DT_CRTD);
         setChkLastModifiedBy(CHK_LST_MDFD_BY);
         setChkDateLastModified(CHK_DT_LST_MDFD);
         setChkApprovedRejectedBy(CHK_APPRVD_RJCTD_BY);
         setChkDateApprovedRejected(CHK_DT_APPRVD_RJCTD);
         setChkPostedBy(CHK_PSTD_BY);
         setChkDatePosted(CHK_DT_PSTD);
         setChkReconciled(CHK_RCNCLD);
         setChkDateReconciled(CHK_DT_RCNCLD);
         setChkReleased(CHK_RLSD);
         setChkDateReleased(CHK_DT_RLSD);
         setChkPrinted(CHK_PRNTD);
         setChkCvPrinted(CHK_CV_PRNTD);
         setChkMemo(CHK_MMO);
         setChkSupplierName(CHK_SPL_NM);
         setChkAdBranch(CHK_AD_BRNCH);
         setChkAdCompany(CHK_AD_CMPNY);
                
         return null;
        
     }
    
    
    public void ejbPostCreate(Integer CHK_CODE, 
         String CHK_TYP, String CHK_DESC, Date CHK_DT, Date CHK_CHCK_DT, String CHK_NMBR, String CHK_DCMNT_NMBR,
	     String CHK_RFRNC_NMBR, String CHK_INFO_TYP, String CHK_INFO_BIO_NMBR, String CHK_INFO_BIO_DESC, String CHK_INFO_TYP_STATUS, String CHK_INFO_RQST_STATUS,
	     byte CHK_INVT_IS, byte CHK_INVT_TB, Date CHK_INVT_NXT_RN_DT, Date CHK_INVT_STTLMNT_DT, Date CHK_INVT_MTRTY_DT, double CHK_INVT_BD_YLD, double CHK_INVT_CPN_RT, double CHK_INVT_STTLMNT_AMNT, double CHK_INVT_FC_VL, double CHK_INVT_PM_AMNT,
	     byte CHK_LN, byte CHK_LN_GNRTD,
	     Date CHK_CNVRSN_DT, double CHK_CNVRSN_RT,
	     double CHK_BLL_AMNT, double CHK_AMNT, String CHK_APPRVL_STATUS, String CHK_RSN_FR_RJCTN, byte CHK_PSTD, byte CHK_VD,
	     byte CHK_CRSS_CHCK, String CHK_VD_APPRVL_STATUS, byte CHK_VD_PSTD,
	     String CHK_CRTD_BY, Date CHK_DT_CRTD, String CHK_LST_MDFD_BY,
	     Date CHK_DT_LST_MDFD, String CHK_APPRVD_RJCTD_BY, Date CHK_DT_APPRVD_RJCTD,
	     String CHK_PSTD_BY, Date CHK_DT_PSTD, byte CHK_RCNCLD, Date CHK_DT_RCNCLD, byte CHK_RLSD, Date CHK_DT_RLSD, byte CHK_PRNTD, byte CHK_CV_PRNTD,
	     String CHK_MMO, String CHK_SPL_NM, Integer CHK_AD_BRNCH, Integer CHK_AD_CMPNY)
        throws CreateException { }
   
    public void ejbPostCreate( 
         String CHK_TYP, String CHK_DESC, Date CHK_DT, Date CHK_CHCK_DT, String CHK_NMBR, String CHK_DCMNT_NMBR,
	     String CHK_RFRNC_NMBR, String CHK_INFO_TYP, String CHK_INFO_BIO_NMBR, String CHK_INFO_BIO_DESC, String CHK_INFO_TYP_STATUS, String CHK_INFO_RQST_STATUS,
	     byte CHK_INVT_IS, byte CHK_INVT_TB, Date CHK_INVT_NXT_RN_DT, Date CHK_INVT_STTLMNT_DT, Date CHK_INVT_MTRTY_DT, double CHK_INVT_BD_YLD, double CHK_INVT_CPN_RT, double CHK_INVT_STTLMNT_AMNT, double CHK_INVT_FC_VL, double CHK_INVT_PM_AMNT,
	     byte CHK_LN, byte CHK_LN_GNRTD, 
	     Date CHK_CNVRSN_DT, double CHK_CNVRSN_RT,
	     double CHK_BLL_AMNT, double CHK_AMNT, String CHK_APPRVL_STATUS, String CHK_RSN_FR_RJCTN, byte CHK_PSTD, byte CHK_VD,
	     byte CHK_CRSS_CHCK, String CHK_VD_APPRVL_STATUS, byte CHK_VD_PSTD,
	     String CHK_CRTD_BY, Date CHK_DT_CRTD, String CHK_LST_MDFD_BY,
	     Date CHK_DT_LST_MDFD, String CHK_APPRVD_RJCTD_BY, Date CHK_DT_APPRVD_RJCTD,
	     String CHK_PSTD_BY, Date CHK_DT_PSTD, byte CHK_RCNCLD, Date CHK_DT_RCNCLD, byte CHK_RLSD, Date CHK_DT_RLSD, byte CHK_PRNTD, byte CHK_CV_PRNTD,
	     String CHK_MMO, String CHK_SPL_NM, Integer CHK_AD_BRNCH, Integer CHK_AD_CMPNY)
        throws CreateException { }
        
}

