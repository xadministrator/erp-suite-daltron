/*
 * com/ejb/ap/LocalApDistributionRecordBean.java
 *
 * Created on February 13, 2004, 06:56 PM
 */

package com.ejb.ap;

import javax.ejb.CreateException;

import com.util.AbstractEntityBean;
import com.util.Debug;

/**
 *
 * @author  Neil Andrew M.Ajero
 *
 * @ejb:bean name="ApDistributionRecordEJB"
 *           display-name="DistributionRecord Entity"
 *           type="CMP"
 *           cmp-version="2.x"
 *           view-type="local"
 *           local-jndi-name="omega-ejb/ApDistributionRecordEJB"
 *           schema="ApDistributionRecord"
 *           primkey-field="drCode"
 *
 * @ejb:pk class="java.lang.Integer"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="apuser"
 *                        role-link="apuserlink"
 *
 * @ejb:permission role-name="apuser"
 *
 * @ejb:interface local-class="com.ejb.ap.LocalApDistributionRecord"
 *                local-extends="javax.ejb.EJBLocalObject"
 *
 * @ejb:home local-class="com.ejb.ap.LocalApDistributionRecordHome"
 *           local-extends="javax.ejb.EJBLocalHome"
 *
 * @ejb:finder signature="LocalApDistributionRecord findByDrClassAndVouCode(java.lang.String DR_CLSS, java.lang.Integer VOU_CODE, java.lang.Integer DR_AD_CMPNY)"
 *             query="SELECT OBJECT(dr) FROM ApVoucher vou, IN(vou.apDistributionRecords) dr WHERE dr.drClass=?1 AND vou.vouCode=?2 AND dr.drAdCompany=?3"
 *
 * @ejb:finder signature="Collection findDrsByDrClassAndVouCode(java.lang.String DR_CLSS, java.lang.Integer VOU_CODE, java.lang.Integer DR_AD_CMPNY)"
 *             query="SELECT OBJECT(dr) FROM ApVoucher vou, IN(vou.apDistributionRecords) dr WHERE dr.drClass=?1 AND vou.vouCode=?2 AND dr.drAdCompany=?3"
 *
 * @jboss:query signature="Collection findDrsByDrClassAndVouCode(java.lang.String DR_CLSS, java.lang.Integer VOU_CODE, java.lang.Integer DR_AD_CMPNY)"
 *             query="SELECT OBJECT(dr) FROM ApVoucher vou, IN(vou.apDistributionRecords) dr WHERE dr.drClass=?1 AND vou.vouCode=?2 AND dr.drAdCompany=?3 ORDER BY dr.drLine"
 * 
 * 
 * @ejb:finder signature="Collection findDrsByDrTaxClassAndVouDateRange(java.util.Date VOU_DT_FRM, java.util.Date VOU_DT_TO, java.lang.Integer DR_AD_CMPNY)"
 *             query="SELECT OBJECT(dr) FROM ApVoucher vou, IN(vou.apDistributionRecords) dr WHERE dr.drClass='TAX' AND vou.vouDate >= ?1 AND vou.vouDate <= ?2 AND dr.drAdCompany=?3"
 *
 * @jboss:query signature="Collection findDrsByDrTaxClassAndVouDateRange(java.util.Date VOU_DT_FRM, java.util.Date VOU_DT_TO, java.lang.Integer DR_AD_CMPNY)"
 *             query="SELECT OBJECT(dr) FROM ApVoucher vou, IN(vou.apDistributionRecords) dr WHERE dr.drClass='TAX' AND vou.vouDate >= ?1 AND vou.vouDate <= ?2 AND dr.drAdCompany=?3 ORDER BY dr.drLine"
 * 
 * @ejb:finder signature="Collection findDrsByDrTaxClassAndChkDateRange(java.util.Date VOU_DT_FRM, java.util.Date VOU_DT_TO, java.lang.Integer DR_AD_CMPNY)"
 *             query="SELECT OBJECT(dr) FROM ApCheck chk, IN(chk.apDistributionRecords) dr WHERE dr.drClass='TAX' AND chk.chkDate >= ?1 AND chk.chkDate <= ?2 AND dr.drAdCompany=?3"
 *
 * @jboss:query signature="Collection findDrsByDrTaxClassAndChkDateRange(java.util.Date VOU_DT_FRM, java.util.Date VOU_DT_TO, java.lang.Integer DR_AD_CMPNY)"
 *             query="SELECT OBJECT(dr) FROM ApCheck chk, IN(chk.apDistributionRecords) dr WHERE dr.drClass='TAX' AND chk.chkDate >= ?1 AND chk.chkDate <= ?2 AND dr.drAdCompany=?3 ORDER BY dr.drLine"
 *
 * 
 * 
 *
 * @ejb:finder signature="Collection findDrsByDrClassAndChkCode(java.lang.String DR_CLSS, java.lang.Integer CHK_CODE, java.lang.Integer DR_AD_CMPNY)"
 *             query="SELECT OBJECT(dr) FROM ApCheck chk, IN(chk.apDistributionRecords) dr WHERE dr.drClass=?1 AND chk.chkCode=?2 AND dr.drAdCompany=?3"
 *
 * @jboss:query signature="Collection findDrsByDrClassAndChkCode(java.lang.String DR_CLSS, java.lang.Integer CHK_CODE, java.lang.Integer DR_AD_CMPNY)"
 *             query="SELECT OBJECT(dr) FROM ApCheck chk, IN(chk.apDistributionRecords) dr WHERE dr.drClass=?1 AND chk.chkCode=?2 AND dr.drAdCompany=?3 ORDER BY dr.drLine"
 *
 * @ejb:finder signature="Collection findImportableDrByVouCode(java.lang.Integer VOU_CODE, java.lang.Integer DR_AD_CMPNY)"
 *             query="SELECT OBJECT(dr) FROM ApVoucher vou, IN(vou.apDistributionRecords) dr WHERE dr.drImported = 0 AND vou.vouCode = ?1 AND dr.drAdCompany=?2"
 *
 * @jboss:query signature="Collection findImportableDrByVouCode(java.lang.Integer VOU_CODE, java.lang.Integer DR_AD_CMPNY)"
 *             query="SELECT OBJECT(dr) FROM ApVoucher vou, IN(vou.apDistributionRecords) dr WHERE dr.drImported = 0 AND vou.vouCode = ?1 AND dr.drAdCompany=?2 ORDER BY dr.drLine"
 * 
 * @ejb:finder signature="Collection findImportableDrByPoCode(java.lang.Integer PO_CODE, java.lang.Integer DR_AD_CMPNY)"
 *             query="SELECT OBJECT(dr) FROM ApPurchaseOrder po, IN(po.apDistributionRecords) dr WHERE dr.drImported = 0 AND po.poCode = ?1 AND dr.drAdCompany=?2"
 *
 * @jboss:query signature="Collection findImportableDrByPoCode(java.lang.Integer PO_CODE, java.lang.Integer DR_AD_CMPNY)"
 *             query="SELECT OBJECT(dr) FROM ApPurchaseOrder po, IN(po.apDistributionRecords) dr WHERE dr.drImported = 0 AND po.poCode = ?1 AND dr.drAdCompany=?2 ORDER BY dr.drLine"
 *
 * @ejb:finder signature="Collection findImportableDrByDrReversedAndChkCode(byte DR_RVRSD, java.lang.Integer CHK_CODE, java.lang.Integer DR_AD_CMPNY)"
 *             query="SELECT OBJECT(dr) FROM ApCheck chk, IN(chk.apDistributionRecords) dr WHERE dr.drImported = 0 AND dr.drReversed = ?1 AND chk.chkCode = ?2 AND dr.drAdCompany=?3"
 *
 * @jboss:query signature="Collection findImportableDrByDrReversedAndChkCode(byte DR_RVRSD, java.lang.Integer CHK_CODE, java.lang.Integer DR_AD_CMPNY)"
 *             query="SELECT OBJECT(dr) FROM ApCheck chk, IN(chk.apDistributionRecords) dr WHERE dr.drImported = 0 AND dr.drReversed = ?1 AND chk.chkCode = ?2 AND dr.drAdCompany=?3 ORDER BY dr.drLine"
 *
 * @ejb:finder signature="Collection findByDrClassAndVouDmVoucherNumber(java.lang.String DR_CLSS, java.lang.String VOU_DM_VCHR_NMBR, java.lang.Integer DR_AD_CMPNY)"
 *             query="SELECT OBJECT(dr) FROM ApVoucher vou, IN(vou.apDistributionRecords) dr WHERE dr.drClass=?1 AND vou.vouDmVoucherNumber = ?2 AND dr.drAdCompany=?3"
 *
 * @jboss:query signature="Collection findByDrClassAndVouDmVoucherNumber(java.lang.String DR_CLSS, java.lang.String VOU_DM_VCHR_NMBR, java.lang.Integer DR_AD_CMPNY)"
 *             query="SELECT OBJECT(dr) FROM ApVoucher vou, IN(vou.apDistributionRecords) dr WHERE dr.drClass=?1 AND vou.vouDmVoucherNumber = ?2 AND dr.drAdCompany=?3 ORDER BY dr.drLine"
 *
 * @ejb:finder signature="Collection findByDrClassAndVouAmountPaid(java.lang.String DR_CLSS, double VOU_AMNT_PD, java.lang.Integer DR_AD_CMPNY)"
 *             query="SELECT OBJECT(dr) FROM ApVoucher vou, IN(vou.apDistributionRecords) dr WHERE dr.drClass=?1 AND vou.vouAmountDue=vou.vouAmountPaid AND dr.drAdCompany=?3"
 *
 * @jboss:query signature="Collection findByDrClassAndVouAmountPaid(java.lang.String DR_CLSS, double VOU_AMNT_PD, java.lang.Integer DR_AD_CMPNY)"
 *             query="SELECT OBJECT(dr) FROM ApVoucher vou, IN(vou.apDistributionRecords) dr WHERE dr.drClass=?1 AND vou.vouAmountDue=vou.vouAmountPaid AND dr.drAdCompany=?3 ORDER BY dr.drLine"
 *
 * @ejb:finder signature="Collection findByVouCode(java.lang.Integer VOU_CODE, java.lang.Integer DR_AD_CMPNY)"
 *             query="SELECT OBJECT(dr) FROM ApVoucher vou, IN(vou.apDistributionRecords) dr WHERE vou.vouCode =?1 AND dr.drAdCompany=?2"
 *
 * @jboss:query signature="Collection findByVouCode(java.lang.Integer VOU_CODE, java.lang.Integer DR_AD_CMPNY)"
 *             query="SELECT OBJECT(dr) FROM ApVoucher vou, IN(vou.apDistributionRecords) dr WHERE vou.vouCode =?1 AND dr.drAdCompany=?2 ORDER BY dr.drDebit DESC"
 *
 * @ejb:finder signature="Collection findByChkCode(java.lang.Integer CHK_CODE, java.lang.Integer DR_AD_CMPNY)"
 *             query="SELECT OBJECT(dr) FROM ApCheck chk, IN(chk.apDistributionRecords) dr WHERE chk.chkCode =?1 AND dr.drAdCompany=?2"
 *
 * @jboss:query signature="Collection findByChkCode(java.lang.Integer CHK_CODE, java.lang.Integer DR_AD_CMPNY)"
 *             query="SELECT OBJECT(dr) FROM ApCheck chk, IN(chk.apDistributionRecords) dr WHERE chk.chkCode =?1 AND dr.drAdCompany=?2 ORDER BY dr.drDebit DESC, dr.glChartOfAccount.coaCode"
 *
 * @ejb:finder signature="Collection findByRvCode(java.lang.Integer RV_CODE, java.lang.Integer DR_AD_CMPNY)"
 *             query="SELECT OBJECT(dr) FROM ApRecurringVoucher rv, IN(rv.apDistributionRecords) dr WHERE rv.rvCode =?1 AND dr.drAdCompany=?2"
 *
 * @jboss:query signature="Collection findByRvCode(java.lang.Integer RV_CODE, java.lang.Integer DR_AD_CMPNY)"
 *             query="SELECT OBJECT(dr) FROM ApRecurringVoucher rv, IN(rv.apDistributionRecords) dr WHERE rv.rvCode =?1 AND dr.drAdCompany=?2 ORDER BY dr.drDebit DESC"
 * 
 * @ejb:finder signature="Collection findByPoCode(java.lang.Integer PO_CODE, java.lang.Integer DR_AD_CMPNY)"
 *             query="SELECT OBJECT(dr) FROM ApPurchaseOrder po, IN(po.apDistributionRecords) dr WHERE po.poCode =?1 AND dr.drAdCompany=?2"
 *
 * @jboss:query signature="Collection findByPoCode(java.lang.Integer PO_CODE, java.lang.Integer DR_AD_CMPNY)"
 *             query="SELECT OBJECT(dr) FROM ApPurchaseOrder po, IN(po.apDistributionRecords) dr WHERE po.poCode =?1 AND dr.drAdCompany=?2 ORDER BY dr.drDebit DESC"
 *
 * @ejb:finder signature="Collection findVouByDateRangeAndCoaAccountNumber(byte VOU_DBT_MMO, java.util.Date VOU_DT_FRM, java.util.Date VOU_DT_TO, java.lang.Integer COA_CODE, java.lang.Integer DR_AD_CMPNY)"
 *             query="SELECT OBJECT(dr) FROM ApVoucher vou, IN(vou.apDistributionRecords) dr WHERE vou.vouPosted=1 AND vou.vouDebitMemo=?1 AND vou.vouDate>=?2 AND vou.vouDate<=?3 AND (dr.glChartOfAccount.coaCode=?4 OR vou.apTaxCode.tcType IN ('EXEMPT','ZERO-RATED')) AND dr.drAdCompany=?5"
 *
 * @jboss:query signature="Collection findVouByDateRangeAndCoaAccountNumber(byte VOU_DBT_MMO, java.util.Date VOU_DT_FRM, java.util.Date VOU_DT_TO, java.lang.Integer COA_CODE, java.lang.Integer DR_AD_CMPNY)"
 *             query="SELECT OBJECT(dr) FROM ApVoucher vou, IN(vou.apDistributionRecords) dr WHERE vou.vouPosted=1 AND vou.vouDebitMemo=?1 AND vou.vouDate>=?2 AND vou.vouDate<=?3 AND (dr.glChartOfAccount.coaCode=?4 OR vou.apTaxCode.tcType IN ('EXEMPT','ZERO-RATED')) AND dr.drAdCompany=?5 ORDER BY dr.drLine"
 * 
 * @ejb:finder signature="Collection findChkByDateRangeAndCoaAccountNumber(java.util.Date CHK_DT_FRM, java.util.Date CHK_DT_TO, java.lang.Integer COA_CODE, java.lang.Integer DR_AD_CMPNY)"
 *             query="SELECT OBJECT(dr) FROM ApCheck chk, IN(chk.apDistributionRecords) dr WHERE chk.chkPosted=1 AND chk.chkDate>=?1 AND chk.chkDate<=?2 AND dr.glChartOfAccount.coaCode=?3 AND dr.drAdCompany=?4"
 *
 * @jboss:query signature="Collection findChkByDateRangeAndCoaAccountNumber(java.util.Date CHK_DT_FRM, java.util.Date CHK_DT_TO, java.lang.Integer COA_CODE, java.lang.Integer DR_AD_CMPNY)"
 *             query="SELECT OBJECT(dr) FROM ApCheck chk, IN(chk.apDistributionRecords) dr WHERE chk.chkPosted=1 AND chk.chkDate>=?1 AND chk.chkDate<=?2 AND dr.glChartOfAccount.coaCode=?3 AND dr.drAdCompany=?4 ORDER BY dr.drLine"
 * 
 * @ejb:finder signature="Collection findPoByDateRangeAndCoaAccountNumber(java.util.Date PO_DT_FRM, java.util.Date PO_DT_TO, java.lang.Integer COA_CODE, java.lang.Integer DR_AD_CMPNY)"
 *             query="SELECT OBJECT(dr) FROM ApPurchaseOrder po, IN(po.apDistributionRecords) dr WHERE po.poReceiving=1 AND po.poPosted=1 AND po.poDate>=?1 AND po.poDate<=?2 AND dr.glChartOfAccount.coaCode=?3 AND dr.drAdCompany=?4"
 *
 * @jboss:query signature="Collection findPoByDateRangeAndCoaAccountNumber(java.util.Date PO_DT_FRM, java.util.Date PO_DT_TO, java.lang.Integer COA_CODE, java.lang.Integer DR_AD_CMPNY)"
 *             query="SELECT OBJECT(dr) FROM ApPurchaseOrder po, IN(po.apDistributionRecords) dr WHERE po.poReceiving=1 AND po.poPosted=1 AND po.poDate>=?1 AND po.poDate<=?2 AND dr.glChartOfAccount.coaCode=?3 AND dr.drAdCompany=?4 ORDER BY dr.drLine"
 * 
 * @ejb:finder signature="Collection findUnpostedVouByDateRangeAndCoaAccountNumber(byte VOU_DBT_MMO, java.util.Date VOU_DT_FRM, java.util.Date VOU_DT_TO, java.lang.Integer COA_CODE, java.lang.Integer VOU_AD_BRNCH, java.lang.Integer DR_AD_CMPNY)"
 *             query="SELECT OBJECT(dr) FROM ApVoucher vou, IN(vou.apDistributionRecords) dr WHERE vou.vouVoid=0 AND vou.vouPosted=0 AND vou.vouDebitMemo=?1 AND vou.vouDate>=?2 AND vou.vouDate<=?3 AND dr.glChartOfAccount.coaCode=?4 AND vou.vouAdBranch=?5 AND dr.drAdCompany=?6"
 *
 * @jboss:query signature="Collection findUnpostedVouByDateRangeAndCoaAccountNumber(byte VOU_DBT_MMO, java.util.Date VOU_DT_FRM, java.util.Date VOU_DT_TO, java.lang.Integer COA_CODE, java.lang.Integer VOU_AD_BRNCH, java.lang.Integer DR_AD_CMPNY)"
 *             query="SELECT OBJECT(dr) FROM ApVoucher vou, IN(vou.apDistributionRecords) dr WHERE vou.vouVoid=0 AND vou.vouPosted=0 AND vou.vouDebitMemo=?1 AND vou.vouDate>=?2 AND vou.vouDate<=?3 AND dr.glChartOfAccount.coaCode=?4 AND vou.vouAdBranch=?5 AND dr.drAdCompany=?6 ORDER BY dr.drLine"
 * 
 * @ejb:finder signature="Collection findUnpostedChkByDateRangeAndCoaAccountNumber(java.util.Date CHK_DT_FRM, java.util.Date CHK_DT_TO, java.lang.Integer COA_CODE, java.lang.Integer CHK_AD_BRNCH, java.lang.Integer DR_AD_CMPNY)"
 *             query="SELECT OBJECT(dr) FROM ApCheck chk, IN(chk.apDistributionRecords) dr WHERE chk.chkVoid=0 AND chk.chkPosted=0 AND chk.chkDate>=?1 AND chk.chkDate<=?2 AND dr.glChartOfAccount.coaCode=?3 AND chk.chkAdBranch=?4 AND dr.drAdCompany=?5"
 *
 * @jboss:query signature="Collection findUnpostedChkByDateRangeAndCoaAccountNumber(java.util.Date CHK_DT_FRM, java.util.Date CHK_DT_TO, java.lang.Integer COA_CODE, java.lang.Integer CHK_AD_BRNCH, java.lang.Integer DR_AD_CMPNY)"
 *             query="SELECT OBJECT(dr) FROM ApCheck chk, IN(chk.apDistributionRecords) dr WHERE chk.chkVoid=0 AND chk.chkPosted=0 AND chk.chkDate>=?1 AND chk.chkDate<=?2 AND dr.glChartOfAccount.coaCode=?3 AND chk.chkAdBranch=?4 AND dr.drAdCompany=?5 ORDER BY dr.drLine"
 * 
 * @ejb:finder signature="Collection findUnpostedPoByDateRangeAndCoaAccountNumber(java.util.Date PO_DT_FRM, java.util.Date PO_DT_TO, java.lang.Integer COA_CODE, java.lang.Integer PO_AD_BRNCH, java.lang.Integer DR_AD_CMPNY)"
 *             query="SELECT OBJECT(dr) FROM ApPurchaseOrder po, IN(po.apDistributionRecords) dr WHERE po.poVoid=0 AND po.poReceiving=1 AND po.poPosted=0 AND po.poDate>=?1 AND po.poDate<=?2 AND dr.glChartOfAccount.coaCode=?3 AND po.poAdBranch=?4 AND dr.drAdCompany=?5"
 *
 * @jboss:query signature="Collection findUnpostedPoByDateRangeAndCoaAccountNumber(java.util.Date PO_DT_FRM, java.util.Date PO_DT_TO, java.lang.Integer COA_CODE, java.lang.Integer PO_AD_BRNCH, java.lang.Integer DR_AD_CMPNY)"
 *             query="SELECT OBJECT(dr) FROM ApPurchaseOrder po, IN(po.apDistributionRecords) dr WHERE po.poVoid=0 AND po.poReceiving=1 AND po.poPosted=0 AND po.poDate>=?1 AND po.poDate<=?2 AND dr.glChartOfAccount.coaCode=?3 AND po.poAdBranch=?4 AND dr.drAdCompany=?5 ORDER BY dr.drLine"
 * 
 * @ejb:finder signature="Collection findUnpostedVouByDrCoaAccountNumberAndBrCode(java.lang.Integer COA_CODE, java.lang.Integer VOU_AD_BRNCH, java.lang.Integer DR_AD_CMPNY)"
 *             query="SELECT OBJECT(dr) FROM ApVoucher vou, IN(vou.apDistributionRecords) dr WHERE vou.vouPosted=0 AND dr.glChartOfAccount.coaCode=?1 AND vou.vouAdBranch = ?2 AND dr.drAdCompany = ?3"
 *
 * @ejb:finder signature="Collection findUnpostedChkByDrCoaAccountNumberAndBrCode(java.lang.Integer COA_CODE, java.lang.Integer CHK_AD_BRNCH, java.lang.Integer DR_AD_CMPNY)"
 *             query="SELECT OBJECT(dr) FROM ApCheck chk, IN(chk.apDistributionRecords) dr WHERE chk.chkPosted=0 AND dr.glChartOfAccount.coaCode=?1 AND chk.chkAdBranch = ?2 AND dr.drAdCompany = ?3"
 *
 * @ejb:finder signature="Collection findUnpostedPoByDrCoaAccountNumberAndBrCode(java.lang.Integer COA_CODE, java.lang.Integer PO_AD_BRNCH, java.lang.Integer DR_AD_CMPNY)"
 *             query="SELECT OBJECT(dr) FROM ApPurchaseOrder po, IN(po.apDistributionRecords) dr WHERE po.poReceiving=1 AND po.poPosted=0 AND dr.glChartOfAccount.coaCode=?1 AND po.poAdBranch = ?2 AND dr.drAdCompany = ?3"
 * 
 * @ejb:finder signature="Collection findVouByDateAndCoaAccountNumberAndCurrencyAndVouPosted(java.util.Date VOU_DT, java.lang.Integer COA_CODE, java.lang.Integer FC_CODE, byte VOU_PSTD, java.lang.Integer DR_AD_CMPNY)"
 *             query="SELECT OBJECT(dr) FROM ApVoucher vou, IN(vou.apDistributionRecords) dr WHERE vou.vouDebitMemo=0 AND vou.vouDate<=?1 AND dr.glChartOfAccount.coaCode=?2 AND vou.glFunctionalCurrency.fcCode=?3 AND vou.vouPosted=?4 AND vou.vouVoid=0 AND dr.drAdCompany=?5"
 *             
 * @jboss:query signature="Collection findVouByDateAndCoaAccountNumberAndCurrencyAndVouPosted(java.util.Date VOU_DT, java.lang.Integer COA_CODE, java.lang.Integer FC_CODE, byte VOU_PSTD, java.lang.Integer DR_AD_CMPNY)"
 *             query="SELECT OBJECT(dr) FROM ApVoucher vou, IN(vou.apDistributionRecords) dr WHERE vou.vouDebitMemo=0 AND vou.vouDate<=?1 AND dr.glChartOfAccount.coaCode=?2 AND vou.glFunctionalCurrency.fcCode=?3 AND vou.vouPosted=?4 AND vou.vouVoid=0 AND dr.drAdCompany=?5 ORDER BY vou.vouDate, dr.drLine"
 * 
 * @ejb:finder signature="Collection findVouDebitMemoByDateAndCoaAccountNumberAndCurrencyAndVouPosted(java.util.Date VOU_DT, java.lang.Integer COA_CODE, java.lang.Integer FC_CODE, byte VOU_PSTD, java.lang.Integer DR_AD_CMPNY)"
 *             query="SELECT OBJECT(dr) FROM ApVoucher dm, IN(dm.apDistributionRecords) dr, ApVoucher vou WHERE dm.vouDebitMemo=1 AND dm.vouDmVoucherNumber=vou.vouDocumentNumber AND dm.vouDate<=?1 AND dr.glChartOfAccount.coaCode=?2 AND vou.glFunctionalCurrency.fcCode=?3 AND dm.vouPosted=?4 AND dm.vouVoid=0 AND dr.drAdCompany=?5"
 *             
 * @jboss:query signature="Collection findVouDebitMemoByDateAndCoaAccountNumberAndCurrencyAndVouPosted(java.util.Date VOU_DT, java.lang.Integer COA_CODE, java.lang.Integer FC_CODE, byte VOU_PSTD, java.lang.Integer DR_AD_CMPNY)"
 *             query="SELECT OBJECT(dr) FROM ApVoucher dm, IN(dm.apDistributionRecords) dr, ApVoucher vou WHERE dm.vouDebitMemo=1 AND dm.vouDmVoucherNumber=vou.vouDocumentNumber AND dm.vouDate<=?1 AND dr.glChartOfAccount.coaCode=?2 AND vou.glFunctionalCurrency.fcCode=?3 AND dm.vouPosted=?4 AND dm.vouVoid=0 AND dr.drAdCompany=?5 ORDER BY dm.vouDate, dr.drLine"
 * 
 * @ejb:finder signature="Collection findChkByDateAndCoaAccountNumberAndCurrencyAndChkPosted(java.util.Date CHK_DT, java.lang.Integer COA_CODE, java.lang.Integer FC_CODE, byte CHK_PSTD, java.lang.Integer DR_AD_CMPNY)"
 *             query="SELECT OBJECT(dr) FROM ApCheck chk, IN(chk.apDistributionRecords) dr, IN(chk.apAppliedVouchers) av  WHERE chk.chkType='PAYMENT' AND chk.chkDate<=?1 AND dr.glChartOfAccount.coaCode=?2 AND chk.glFunctionalCurrency.fcCode=?3 AND chk.chkPosted=?4 AND chk.chkVoid=0 AND dr.drAdCompany=?5"
 *             
 * @jboss:query signature="Collection findChkByDateAndCoaAccountNumberAndCurrencyAndChkPosted(java.util.Date CHK_DT, java.lang.Integer COA_CODE, java.lang.Integer FC_CODE, byte CHK_PSTD, java.lang.Integer DR_AD_CMPNY)"
 *             query="SELECT OBJECT(dr) FROM ApCheck chk, IN(chk.apDistributionRecords) dr, IN(chk.apAppliedVouchers) av  WHERE chk.chkType='PAYMENT' AND  chk.chkDate<=?1 AND dr.glChartOfAccount.coaCode=?2 AND chk.glFunctionalCurrency.fcCode=?3 AND chk.chkPosted=?4 AND chk.chkVoid=0 AND dr.drAdCompany=?5 ORDER BY chk.chkDate, dr.drLine"
 * 
 * @ejb:finder signature="Collection findChkDirectByDateAndCoaAccountNumberAndCurrencyAndChkPosted(java.util.Date CHK_DT, java.lang.Integer COA_CODE, java.lang.Integer FC_CODE, byte CHK_PSTD, java.lang.Integer DR_AD_CMPNY)"
 *             query="SELECT OBJECT(dr) FROM ApCheck chk, IN(chk.apDistributionRecords) dr WHERE chk.chkType='DIRECT' AND chk.chkDate<=?1 AND dr.glChartOfAccount.coaCode=?2 AND chk.glFunctionalCurrency.fcCode=?3 AND chk.chkPosted=?4 AND chk.chkVoid=0 AND dr.drAdCompany=?5"
 *             
 * @jboss:query signature="Collection findChkDirectByDateAndCoaAccountNumberAndCurrencyAndChkPosted(java.util.Date CHK_DT, java.lang.Integer COA_CODE, java.lang.Integer FC_CODE, byte CHK_PSTD, java.lang.Integer DR_AD_CMPNY)"
 *             query="SELECT OBJECT(dr) FROM ApCheck chk, IN(chk.apDistributionRecords) dr WHERE chk.chkType='DIRECT' AND  chk.chkDate<=?1 AND dr.glChartOfAccount.coaCode=?2 AND chk.glFunctionalCurrency.fcCode=?3 AND chk.chkPosted=?4 AND chk.chkVoid=0 AND dr.drAdCompany=?5 ORDER BY chk.chkDate, dr.drLine"
 * 
 * @ejb:finder signature="Collection findPoByDateAndCoaAccountNumberAndCurrencyAndPoPosted(java.util.Date PO_DT, java.lang.Integer COA_CODE, java.lang.Integer FC_CODE, byte PO_PSTD, java.lang.Integer DR_AD_CMPNY)"
 *             query="SELECT OBJECT(dr) FROM ApPurchaseOrder po, IN(po.apDistributionRecords) dr WHERE po.poReceiving=1 AND po.poDate<=?1 AND dr.glChartOfAccount.coaCode=?2 AND po.glFunctionalCurrency.fcCode=?3 AND po.poPosted=?4 AND po.poVoid=0 AND dr.drAdCompany=?5"
 *             
 * @jboss:query signature="Collection findPoByDateAndCoaAccountNumberAndCurrencyAndPoPosted(java.util.Date PO_DT, java.lang.Integer COA_CODE, java.lang.Integer FC_CODE, byte PO_PSTD, java.lang.Integer DR_AD_CMPNY)"
 *             query="SELECT OBJECT(dr) FROM ApPurchaseOrder po, IN(po.apDistributionRecords) dr WHERE po.poReceiving=1 AND po.poDate<=?1 AND dr.glChartOfAccount.coaCode=?2 AND po.glFunctionalCurrency.fcCode=?3 AND po.poPosted=?4 AND po.poVoid=0 AND dr.drAdCompany=?5 ORDER BY po.poDate, dr.drLine"
 * 
 * @ejb:finder signature="Collection findUnpostedVouByDateAndCoaAccountNumber(byte VOU_DBT_MMO, java.util.Date VOU_DT_TO, java.lang.Integer COA_CODE, java.lang.Integer VOU_AD_BRNCH, java.lang.Integer DR_AD_CMPNY)"
 *             query="SELECT OBJECT(dr) FROM ApVoucher vou, IN(vou.apDistributionRecords) dr WHERE vou.vouVoid=0 AND vou.vouPosted=0 AND vou.vouDebitMemo=?1 AND vou.vouDate<=?2 AND dr.glChartOfAccount.coaCode=?3 AND vou.vouAdBranch=?4 AND dr.drAdCompany=?5"
 *
 * @jboss:query signature="Collection findUnpostedVouByDateAndCoaAccountNumber(byte VOU_DBT_MMO, java.util.Date VOU_DT_TO, java.lang.Integer COA_CODE, java.lang.Integer VOU_AD_BRNCH, java.lang.Integer DR_AD_CMPNY)"
 *             query="SELECT OBJECT(dr) FROM ApVoucher vou, IN(vou.apDistributionRecords) dr WHERE vou.vouVoid=0 AND vou.vouPosted=0 AND vou.vouDebitMemo=?1 AND vou.vouDate<=?2 AND dr.glChartOfAccount.coaCode=?3 AND vou.vouAdBranch=?4 AND dr.drAdCompany=?5 ORDER BY dr.drLine"
 * 
 * @ejb:finder signature="Collection findUnpostedChkByDateAndCoaAccountNumber(java.util.Date CHK_DT_TO, java.lang.Integer COA_CODE, java.lang.Integer CHK_AD_BRNCH, java.lang.Integer DR_AD_CMPNY)"
 *             query="SELECT OBJECT(dr) FROM ApCheck chk, IN(chk.apDistributionRecords) dr WHERE chk.chkVoid=0 AND chk.chkPosted=0 AND chk.chkDate<=?1 AND dr.glChartOfAccount.coaCode=?2 AND chk.chkAdBranch=?3 AND dr.drAdCompany=?4"
 *
 * @jboss:query signature="Collection findUnpostedChkByDateAndCoaAccountNumber(java.util.Date CHK_DT_TO, java.lang.Integer COA_CODE, java.lang.Integer CHK_AD_BRNCH, java.lang.Integer DR_AD_CMPNY)"
 *             query="SELECT OBJECT(dr) FROM ApCheck chk, IN(chk.apDistributionRecords) dr WHERE chk.chkVoid=0 AND chk.chkPosted=0 AND chk.chkDate<=?1 AND dr.glChartOfAccount.coaCode=?2 AND chk.chkAdBranch=?3 AND dr.drAdCompany=?4 ORDER BY dr.drLine"
 * 
 * @ejb:finder signature="Collection findUnpostedPoByDateAndCoaAccountNumber(java.util.Date PO_DT_TO, java.lang.Integer COA_CODE, java.lang.Integer PO_AD_BRNCH, java.lang.Integer DR_AD_CMPNY)"
 *             query="SELECT OBJECT(dr) FROM ApPurchaseOrder po, IN(po.apDistributionRecords) dr WHERE po.poVoid=0 AND po.poReceiving=1 AND po.poPosted=0 AND po.poDate<=?1 AND dr.glChartOfAccount.coaCode=?2 AND po.poAdBranch=?3 AND dr.drAdCompany=?4"
 *
 * @jboss:query signature="Collection findUnpostedPoByDateAndCoaAccountNumber(java.util.Date PO_DT_TO, java.lang.Integer COA_CODE, java.lang.Integer PO_AD_BRNCH, java.lang.Integer DR_AD_CMPNY)"
 *             query="SELECT OBJECT(dr) FROM ApPurchaseOrder po, IN(po.apDistributionRecords) dr WHERE po.poVoid=0 AND po.poReceiving=1 AND po.poPosted=0 AND po.poDate<=?1 AND dr.glChartOfAccount.coaCode=?2 AND po.poAdBranch=?3 AND dr.drAdCompany=?4 ORDER BY dr.drLine"
 * 
 * @ejb:finder signature="Collection findChkByDateAndCoaAccountDescriptionAndSupplier(java.util.Date CHK_DT_TO, java.lang.Integer SPL_CODE, java.lang.Integer CHK_AD_BRNCH, java.lang.Integer DR_AD_CMPNY)"
 *             query="SELECT OBJECT(dr) FROM ApCheck chk, IN(chk.apDistributionRecords) dr WHERE chk.chkVoid=0 AND chk.chkPosted=1 AND chk.chkType='DIRECT' AND chk.chkDate<=?1 AND dr.glChartOfAccount.coaApTag=1 AND chk.apSupplier.splCode=?2 AND chk.chkAdBranch=?3 AND dr.drAdCompany=?4"
 *
 * @jboss:query signature="Collection findChkByDateAndCoaAccountDescriptionAndSupplier(java.util.Date CHK_DT_TO, java.lang.Integer SPL_CODE, java.lang.Integer CHK_AD_BRNCH, java.lang.Integer DR_AD_CMPNY)"
 *             query="SELECT OBJECT(dr) FROM ApCheck chk, IN(chk.apDistributionRecords) dr WHERE chk.chkVoid=0 AND chk.chkPosted=1 AND chk.chkType='DIRECT' AND chk.chkDate<=?1 AND dr.glChartOfAccount.coaApTag=1 AND chk.apSupplier.splCode=?2 AND chk.chkAdBranch=?3 AND dr.drAdCompany=?4 ORDER BY dr.drLine"
 *
 * 
 * @ejb:value-object match="*"
 *             name="ApDistributionRecord"
 *
 * @jboss:persistence table-name="AP_DSTRBTN_RCRD"
 *
 * @jboss:entity-command name="mysql-get-generated-keys"
 */

public abstract class ApDistributionRecordBean extends AbstractEntityBean {
    
    
    // PERSISTENT METHODS
    
    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     * @ejb:pk-field
     *
     * @jboss:column-name name="DR_CODE"
     *
     * @jboss:persistence auto-increment="true"
     **/
    public abstract Integer getDrCode();
    public abstract void setDrCode(Integer DR_CODE);
     
    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="DR_LN"
     **/
     public abstract short getDrLine();
     /**
      * @ejb:interface-method view-type="local"
      **/         
     public abstract void setDrLine(short DR_LN);

    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="DR_CLSS"
     **/
     public abstract String getDrClass();    
     /**
      * @ejb:interface-method view-type="local"
      **/     
     public abstract void setDrClass(String DR_CLSS);   

    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="DR_DBT"
     **/
     public abstract byte getDrDebit();    
     /**
      * @ejb:interface-method view-type="local"
      **/     
     public abstract void setDrDebit(byte DR_DBT);     
     
    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="DR_AMNT"
     **/
     public abstract double getDrAmount();    
     /**
      * @ejb:interface-method view-type="local"
      **/ 
     public abstract void setDrAmount(double DR_AMNT);   
     
    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="DR_IMPRTD"
     **/
     public abstract byte getDrImported();    
     /**
      * @ejb:interface-method view-type="local"
      **/ 
     public abstract void setDrImported(byte DR_IMPRTD); 
     
     
     /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="DR_RVRSD"
     **/
     public abstract byte getDrReversed();    
     /**
      * @ejb:interface-method view-type="local"
      **/ 
     public abstract void setDrReversed(byte DR_RVRSD); 
     
     /**
      * @ejb:persistent-field
      * @ejb:interface-method view-type="local"
      *
      * @jboss:column-name name="DR_AD_CMPNY"
      **/
      public abstract Integer getDrAdCompany();    
      /**
       * @ejb:interface-method view-type="local"
       **/ 
      public abstract void setDrAdCompany(Integer DR_AD_CMPNY);
     
     
     // RELATIONSHIP METHODS    
     
     /**
      * @ejb:interface-method view-type="local"
      * @ejb:relation name="recurringvoucher-apdistributionrecords"
      *               role-name="apdistributionrecord-has-one-recurringvoucher"
      *				  cascade-delete="yes"
      *
      * @jboss:relation related-pk-field="rvCode"
      *                 fk-column="AP_RECURRING_VOUCHER"
      */
     public abstract com.ejb.ap.LocalApRecurringVoucher getApRecurringVoucher();
     /**
      * @ejb:interface-method view-type="local"
      **/ 
     public abstract void setApRecurringVoucher(com.ejb.ap.LocalApRecurringVoucher apRecurringVoucher);

     /**
      * @ejb:interface-method view-type="local"
      * @ejb:relation name="voucher-apdistributionrecords"
      *               role-name="apdistributionrecord-has-one-voucher"
      *               cascade-delete="yes"
      *
      * @jboss:relation related-pk-field="vouCode"
      *                 fk-column="AP_VOUCHER"
      */
     public abstract com.ejb.ap.LocalApVoucher getApVoucher();
     /**
      * @ejb:interface-method view-type="local"
      **/ 
     public abstract void setApVoucher(com.ejb.ap.LocalApVoucher apVoucher);
     
     /**
      * @ejb:interface-method view-type="local"
      * @ejb:relation name="check-apdistributionrecords"
      *               role-name="apdistributionrecord-has-one-check"
      *               cascade-delete="yes"
      *             
      * @jboss:relation related-pk-field="chkCode"
      *                 fk-column="AP_CHECK"
      */
     public abstract com.ejb.ap.LocalApCheck getApCheck();
     /**
      * @ejb:interface-method view-type="local"
      **/ 
     public abstract void setApCheck(com.ejb.ap.LocalApCheck apCheck);
     
     /**
      * @ejb:interface-method view-type="local"
      * @ejb:relation name="appliedvoucher-apdistributionrecords"
      *               role-name="apdistributionrecord-has-one-appliedvoucher"
      *               cascade-delete="yes"
      *             
      * @jboss:relation related-pk-field="avCode"
      *                 fk-column="AP_APPLIED_VOUCHER"
      */
     public abstract com.ejb.ap.LocalApAppliedVoucher getApAppliedVoucher();
     /**
      * @ejb:interface-method view-type="local"
      **/ 
     public abstract void setApAppliedVoucher(com.ejb.ap.LocalApAppliedVoucher apAppliedVoucher);
          
     /**
      * @ejb:interface-method view-type="local"
      * @ejb:relation name="chartofaccount-apdistributionrecords"
      *               role-name="apdistributionrecord-has-one-chartofaccount"
      *
      * @jboss:relation related-pk-field="coaCode"
      *                 fk-column="GL_CHART_OF_ACCOUNT" 
      */
     public abstract com.ejb.gl.LocalGlChartOfAccount getGlChartOfAccount();
     /**
      * @ejb:interface-method view-type="local"
      **/ 
     public abstract void setGlChartOfAccount(com.ejb.gl.LocalGlChartOfAccount glChartOfAccount);
     
     /**
      * @ejb:interface-method view-type="local"
      * @ejb:relation name="purchaseorder-apdistributionrecords"
      *               role-name="apdistributionrecord-has-one-purchaseorder"
      *               cascade-delete="yes"
      *
      * @jboss:relation related-pk-field="poCode"
      *                 fk-column="AP_PURCHASE_ORDER"
      */
     public abstract LocalApPurchaseOrder getApPurchaseOrder();
     /**
      * @ejb:interface-method view-type="local"
      **/ 
     public abstract void setApPurchaseOrder(com.ejb.ap.LocalApPurchaseOrder apPurchaseOrder);
        
                   
     // ENTITY METHODS
    
     /**
      * @ejb:create-method view-type="local"
      **/
     public Integer ejbCreate(Integer DR_CODE, short DR_LN, String DR_CLSS,
         double DR_AMNT, byte DR_DBT, byte DR_IMPRTD, byte DR_RVRSD, Integer DR_AD_CMPNY)
         throws CreateException {
           
         Debug.print("ApDistributionRecordBean ejbCreate");
        
         setDrLine(DR_LN);
         setDrClass(DR_CLSS);
         setDrAmount(DR_AMNT);
         setDrDebit(DR_DBT);
         setDrImported(DR_IMPRTD);
         setDrReversed(DR_RVRSD);
         setDrAdCompany(DR_AD_CMPNY);
                
         return null;
        
     }
    
     /**
      * @ejb:create-method view-type="local"
      **/
     public Integer ejbCreate(short DR_LN, String DR_CLSS, 
         double DR_AMNT, byte DR_DBT, byte DR_IMPRTD, byte DR_RVRSD, Integer DR_AD_CMPNY)
         throws CreateException {
           
         Debug.print("ApDistributionRecordBean ejbCreate");
        
         setDrLine(DR_LN);
         setDrClass(DR_CLSS);
         setDrAmount(DR_AMNT);
         setDrDebit(DR_DBT);
         setDrImported(DR_IMPRTD);
         setDrReversed(DR_RVRSD);
         setDrAdCompany(DR_AD_CMPNY);

         return null;
        
     }
    
    
     public void ejbPostCreate(Integer DR_CODE, short DR_LN, String DR_CLSS, 
         double DR_AMNT, byte DR_DBT, byte DR_IMPRTD, byte DR_RVRSD, Integer DR_AD_CMPNY)
         throws CreateException { }
   
     public void ejbPostCreate(short DR_LN, String DR_CLSS,
         double DR_AMNT, byte DR_DBT, byte DR_IMPRTD, byte DR_RVRSD, Integer DR_AD_CMPNY)
         throws CreateException { }
        
}

