/*
 * com/ejb/ap/LocalApPurchaseOrderBean.java
 *
 * Created on April 13, 2005 10:18 AM
 */

package com.ejb.ap;

import java.util.Collection;
import java.util.Date;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;

import com.util.AbstractEntityBean;
import com.util.Debug;

/**
 *
 * @author Enrico C. Yap
 *
 * @ejb:bean name="ApPurchaseOrderEJB" display-name="Purchase Order Entity"
 *           type="CMP" cmp-version="2.x" view-type="local"
 *           local-jndi-name="omega-ejb/ApPurchaseOrderEJB"
 *           schema="ApPurchaseOrder" primkey-field="poCode"
 *
 * @ejb:pk class="java.lang.Integer"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="apuser" role-link="apuserlink"
 *
 * @ejb:permission role-name="apuser"
 *
 * @ejb:interface local-class="com.ejb.ap.LocalApPurchaseOrder"
 *                local-extends="javax.ejb.EJBLocalObject"
 *
 * @ejb:home local-class="com.ejb.ap.LocalApPurchaseOrderHome"
 *           local-extends="javax.ejb.EJBLocalHome"
 *
 * @ejb:finder signature="LocalApPurchaseOrder findByPoDocumentNumberAndBrCode(java.lang.String PO_DCMNT_NMBR, java.lang.Integer PO_AD_BRNCH, java.lang.Integer PO_AD_CMPNY)"
 *             query="SELECT OBJECT(po) FROM ApPurchaseOrder po WHERE po.poDocumentNumber = ?1 AND po.poAdBranch = ?2 AND po.poAdCompany = ?3"
 *
 * @ejb:finder signature="LocalApPurchaseOrder findByPoReferenceNumber(java.lang.String PO_RRFRNCE_NMBR, java.lang.Integer PO_AD_CMPNY)"
 *             query="SELECT OBJECT(po) FROM ApPurchaseOrder po WHERE po.poReferenceNumber = ?1 AND po.poAdCompany = ?2"
 *
 * @ejb:finder signature="LocalApPurchaseOrder findByPoDocumentNumberAndPoReceivingAndBrCode(java.lang.String PO_DCMNT_NMBR, byte PO_RCVNG, java.lang.Integer PO_AD_BRNCH, java.lang.Integer PO_AD_CMPNY)"
 * 			   query="SELECT OBJECT(po) FROM ApPurchaseOrder po WHERE po.poDocumentNumber = ?1 AND po.poReceiving = ?2 AND po.poAdBranch = ?3 AND po.poAdCompany = ?4"
 *
 * @ejb:finder signature="Collection findByPoReceivingAndSplSupplierCode(byte PO_RCVNG, java.lang.String SPL_SPPLR_CODE, java.lang.Integer PO_AD_CMPNY)"
 *             query="SELECT OBJECT(po) FROM ApPurchaseOrder po WHERE po.poReceiving = ?1 AND po.apSupplier.splSupplierCode = ?2 AND po.poLock = 0 AND po.poAdCompany = ?3"
 *
 * @ejb:finder signature="Collection findByPoReceiving(java.lang.Integer PO_AD_BRNCH, java.lang.Integer PO_AD_CMPNY)"
 *             query="SELECT OBJECT(po) FROM ApPurchaseOrder po WHERE po.poReceiving = 1 AND po.poAdBranch = ?1 AND po.poAdCompany = ?2"
 *
 * @ejb:finder signature="LocalApPurchaseOrder findByPoRcvPoNumberAndPoReceivingAndSplSupplierCodeAndBrCode(java.lang.String PO_RCV_PO_NMBR, byte PO_RCVNG, java.lang.String SPL_SPPLR_CODE, java.lang.Integer PO_AD_BRNCH, java.lang.Integer VOU_AD_CMPNY)"
 *             query="SELECT OBJECT(po) FROM ApPurchaseOrder po WHERE po.poDocumentNumber = ?1 AND po.poReceiving = ?2 AND po.apSupplier.splSupplierCode = ?3 AND po.poAdBranch = ?4 AND po.poAdCompany = ?5"
 *
 * @ejb:finder signature="LocalApPurchaseOrder findByPoRcvPoNumberAndPoReceivingAndBrCode(java.lang.String PO_RCV_PO_NMBR, byte PO_RCVNG, java.lang.Integer PO_AD_BRNCH, java.lang.Integer VOU_AD_CMPNY)"
 *             query="SELECT OBJECT(po) FROM ApPurchaseOrder po WHERE po.poRcvPoNumber = ?1 AND po.poReceiving = ?2 AND po.poAdBranch = ?3 AND po.poAdCompany = ?4"
 *
 * @ejb:finder signature="Collection findByPoRcvPoNumberAndPoReceivingAndPoAdBranchAndPoAdCompany(java.lang.String PO_RCV_PO_NMBR, java.lang.Integer PO_AD_BRNCH, java.lang.Integer VOU_AD_CMPNY)"
 *             query="SELECT OBJECT(po) FROM ApPurchaseOrder po WHERE po.poRcvPoNumber = ?1 AND po.poReceiving=1 AND po.poAdBranch = ?2 AND po.poAdCompany = ?3"
 *
 * @ejb:finder signature="Collection findDraftPoAll(java.lang.Integer PO_AD_CMPNY)"
 *             query="SELECT OBJECT(po) FROM ApPurchaseOrder po WHERE po.poApprovalStatus IS NULL AND po.poVoid = 0 AND po.poAdCompany = ?1"
 *
 * @ejb:finder signature="Collection findDraftPoByBrCode(java.lang.Integer PO_AD_BRNCH, java.lang.Integer PO_AD_CMPNY)"
 * 				query="SELECT OBJECT(po) FROM ApPurchaseOrder po WHERE po.poReceiving=0 AND po.poApprovalStatus IS NULL AND po.poReasonForRejection IS NULL AND po.poVoid = 0 AND po.poAdBranch = ?1 AND po.poAdCompany = ?2"
 *
 * @ejb:finder signature="Collection findDraftRiByBrCode(java.lang.Integer PO_AD_BRNCH, java.lang.Integer PO_AD_CMPNY)"
 * 				query="SELECT OBJECT(po) FROM ApPurchaseOrder po WHERE po.poReceiving=1 AND po.poApprovalStatus IS NULL AND po.poReasonForRejection IS NULL AND po.poVoid = 0 AND po.poAdBranch = ?1 AND po.poAdCompany = ?2"
 *
 * @ejb:finder signature="Collection findRejectedPoByBrCodeAndPoCreatedBy(java.lang.Integer PO_AD_BRNCH, java.lang.String PO_CRTD_BY, java.lang.Integer PO_AD_CMPNY)"
 * 				query="SELECT OBJECT(po) FROM ApPurchaseOrder po WHERE po.poReceiving = 0 AND po.poApprovalStatus IS NULL AND po.poReasonForRejection IS NOT NULL AND po.poVoid = 0 AND po.poAdBranch = ?1 AND po.poCreatedBy=?2 AND po.poAdCompany = ?3"
 *
 * @ejb:finder signature="Collection findRejectedRiByBrCodeAndPoCreatedBy(java.lang.Integer PO_AD_BRNCH, java.lang.String PO_CRTD_BY, java.lang.Integer PO_AD_CMPNY)"
 * 				query="SELECT OBJECT(po) FROM ApPurchaseOrder po WHERE po.poReceiving = 1 AND po.poApprovalStatus IS NULL AND po.poReasonForRejection IS NOT NULL AND po.poVoid = 0 AND po.poAdBranch = ?1 AND po.poCreatedBy=?2 AND po.poAdCompany = ?3"
 *
 * @ejb:finder signature="Collection findUnpostedPoReceivingByPoReceivingDateRange(java.util.Date PO_DT_FRM, java.util.Date PO_DT_TO, java.lang.Integer PO_AD_CMPNY)"
 *             query="SELECT OBJECT(po) FROM ApPurchaseOrder po WHERE po.poReceiving = 1 AND po.poPosted = 0 AND po.poVoid = 0 AND po.poDate >= ?1 AND po.poDate <= ?2 AND po.poAdCompany = ?3"
 *
 * @ejb:finder signature="Collection findPostedPoAll(java.lang.Integer PO_AD_BRNCH, java.lang.Integer PO_AD_CMPNY)"
 *             query="SELECT OBJECT(po) FROM ApPurchaseOrder po WHERE po.poReceiving = 0 AND po.poPosted = 1 AND po.poVoid = 0 AND po.poAdBranch = ?1 AND po.poAdCompany = ?2"
 *
 * @jboss:query signature="Collection findUnpostedPoReceivingByPoReceivingDateRange(java.util.Date PO_DT_FRM, java.util.Date PO_DT_TO, java.lang.Integer PO_AD_CMPNY)"
 *             query="SELECT OBJECT(po) FROM ApPurchaseOrder po WHERE po.poReceiving = 1 AND po.poPosted = 0 AND po.poVoid = 0 AND po.poDate >= ?1 AND po.poDate <= ?2 AND po.poAdCompany = ?3 ORDER BY po.poDate"
 *
 * @ejb:finder signature="Collection findPOforPrint(java.lang.Integer PO_AD_BRNCH, java.lang.Integer PO_AD_CMPNY)"
 * 				query="SELECT OBJECT(po) FROM ApPurchaseOrder po WHERE po.poReceiving=0 AND po.poApprovalStatus IS NOT NULL AND po.poReasonForRejection IS NULL AND po.poPrinted = 0 AND po.poAdBranch = ?1 AND po.poAdCompany = ?2"
 *
 * @ejb:finder signature="Collection findPostedTaxPoByVouDateRange(java.util.Date VOU_DT_FRM, java.util.Date PO_DT_TO, java.lang.Integer PO_AD_BRNCH, java.lang.Integer PO_AD_CMPNY)"
 *             query="SELECT OBJECT(po) FROM ApPurchaseOrder po WHERE po.poReceiving = 1 AND po.poPosted = 1 AND po.apTaxCode.tcType IN ('EXEMPT','ZERO-RATED','INCLUSIVE','EXCLUSIVE') AND po.poDate >= ?1 AND po.poDate <= ?2 AND po.poAdBranch = ?3 AND po.poAdCompany = ?4 ORDER BY po.poDate, po.poDocumentNumber"
 *
 * @jboss:query signature="Collection findPostedTaxPoByVouDateRange(java.util.Date VOU_DT_FRM, java.util.Date PO_DT_TO, java.lang.Integer PO_AD_BRNCH, java.lang.Integer PO_AD_CMPNY)"
 *             query="SELECT OBJECT(po) FROM ApPurchaseOrder po WHERE po.poReceiving = 1 AND po.poPosted = 1 AND po.apTaxCode.tcType IN ('EXEMPT','ZERO-RATED','INCLUSIVE','EXCLUSIVE') AND po.poDate >= ?1 AND po.poDate <= ?2 AND po.poAdBranch = ?3 AND po.poAdCompany = ?4 ORDER BY po.poDate, po.poDocumentNumber"
 *
 *
 * @ejb:select signature="Collection ejbSelectGeneric(java.lang.String jbossQl, java.lang.Object[] args)"
 *             result-type-mapping="Local"
 *             method-intf="LocalHome"
 *             query="SELECT OBJECT(po) FROM ApPurchaseOrder po"
 *
 * @jboss:query signature="Collection ejbSelectGeneric(java.lang.String jbossQl, java.lang.Object[] args)"
 *                         dynamic="true"
 *
 * @ejb:value-object match="*"
 *             name="ApPurchaseOrder"
 *
 * @jboss:persistence table-name="AP_PRCHS_ORDR"
 *
 * @jboss:entity-command name="mysql-get-generated-keys"
 */

public abstract class ApPurchaseOrderBean extends AbstractEntityBean {

	// PERSISTENT METHODS

	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 * @ejb:pk-field
	 *
	 * @jboss:column-name name="PO_CODE"
	 *
	 * @jboss:persistence auto-increment="true"
	 */
	public abstract Integer getPoCode();

	public abstract void setPoCode(Integer PO_CODE);

	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="PO_RCVNG"
	 */
	public abstract byte getPoReceiving();

	/**
	 * @ejb:interface-method view-type="local"
	 */
	public abstract void setPoReceiving(byte PO_RCVNG);

	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="PO_TYP"
	 */
	public abstract String getPoType();

	/**
	 * @ejb:interface-method view-type="local"
	 */
	public abstract void setPoType(String PO_TYP);

	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="PO_DT"
	 */
	public abstract Date getPoDate();

	/**
	 * @ejb:interface-method view-type="local"
	 */
	public abstract void setPoDate(Date PO_DT);

	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="PO_DLVRY_PRD"
	 */
	public abstract Date getPoDeliveryPeriod();

	/**
	 * @ejb:interface-method view-type="local"
	 */
	public abstract void setPoDeliveryPeriod(Date PO_DLVRY_PRD);

	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="PO_DCMNT_NMBR"
	 */
	public abstract String getPoDocumentNumber();

	/**
	 * @ejb:interface-method view-type="local"
	 */
	public abstract void setPoDocumentNumber(String PO_DCMNT_NMBR);

	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="PO_SHPMNT_NMBR"
	 */
	public abstract String getPoShipmentNumber();

	/**
	 * @ejb:interface-method view-type="local"
	 */
	public abstract void setPoShipmentNumber(String PO_SHPMNT_NMBR);


	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="PO_RFRNC_NMBR"
	 */
	public abstract String getPoReferenceNumber();

	/**
	 * @ejb:interface-method view-type="local"
	 */
	public abstract void setPoReferenceNumber(String PO_RFRNC_NMBR);

	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="PO_RCV_PO_NMBR"
	 */
	public abstract String getPoRcvPoNumber();

	/**
	 * @ejb:interface-method view-type="local"
	 */
	public abstract void setPoRcvPoNumber(String PO_RCV_PO_NMBR);

	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="PO_DESC"
	 */
	public abstract String getPoDescription();

	/**
	 * @ejb:interface-method view-type="local"
	 */
	public abstract void setPoDescription(String PO_DESC);

	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="PO_BLL_TO"
	 */
	public abstract String getPoBillTo();

	/**
	 * @ejb:interface-method view-type="local"
	 */
	public abstract void setPoBillTo(String PO_BLL_TO);

	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="PO_SHP_TO"
	 */
	public abstract String getPoShipTo();

	/**
	 * @ejb:interface-method view-type="local"
	 */
	public abstract void setPoShipTo(String PO_SHP_TO);

	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="PO_CNVRSN_DT"
	 */
	public abstract Date getPoConversionDate();

	/**
	 * @ejb:interface-method view-type="local"
	 */
	public abstract void setPoConversionDate(Date PO_CNVRSN_DT);

	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="PO_CNVRSN_RT"
	 */
	public abstract double getPoConversionRate();

	/**
	 * @ejb:interface-method view-type="local"
	 */
	public abstract void setPoConversionRate(double PO_CNVRSN_RT);
	
	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="PO_TTL_AMNT"
	 */
	public abstract double getPoTotalAmount();

	/**
	 * @ejb:interface-method view-type="local"
	 */
	public abstract void setPoTotalAmount(double PO_TTL_AMNT);

	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="PO_PRNTD"
	 */
	public abstract byte getPoPrinted();

	/**
	 * @ejb:interface-method view-type="local"
	 */
	public abstract void setPoPrinted(byte PO_PRNTD);

	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="PO_VD"
	 */
	public abstract byte getPoVoid();

	/**
	 * @ejb:interface-method view-type="local"
	 */
	public abstract void setPoVoid(byte PO_VD);

	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="PO_APPRVL_STATUS"
	 */
	public abstract String getPoApprovalStatus();

	/**
	 * @ejb:interface-method view-type="local"
	 */
	public abstract void setPoApprovalStatus(String PO_APPRVL_STATUS);

	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="PO_PSTD"
	 */
	public abstract byte getPoPosted();

	/**
	 * @ejb:interface-method view-type="local"
	 */
	public abstract void setPoPosted(byte PO_PSTD);

	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="PO_RSN_FR_RJCTN"
	 */
	public abstract String getPoReasonForRejection();

	/**
	 * @ejb:interface-method view-type="local"
	 */
	public abstract void setPoReasonForRejection(String PO_RSN_FR_RJCTN);

	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="PO_CRTD_BY"
	 */
	public abstract String getPoCreatedBy();

	/**
	 * @ejb:interface-method view-type="local"
	 */
	public abstract void setPoCreatedBy(String PO_CRTD_BY);

	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="PO_DT_CRTD"
	 */
	public abstract Date getPoDateCreated();

	/**
	 * @ejb:interface-method view-type="local"
	 */
	public abstract void setPoDateCreated(Date PO_DT_CRTD);

	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="PO_LST_MDFD_BY"
	 */
	public abstract String getPoLastModifiedBy();

	/**
	 * @ejb:interface-method view-type="local"
	 */
	public abstract void setPoLastModifiedBy(String PO_LST_MDFD_BY);

	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="PO_DT_LST_MDFD"
	 */
	public abstract Date getPoDateLastModified();

	/**
	 * @ejb:interface-method view-type="local"
	 */
	public abstract void setPoDateLastModified(Date PO_DT_LST_MDFD);

	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="PO_APPRVD_RJCTD_BY"
	 */
	public abstract String getPoApprovedRejectedBy();

	/**
	 * @ejb:interface-method view-type="local"
	 */
	public abstract void setPoApprovedRejectedBy(String PO_APPRVD_RJCTD_BY);

	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="PO_DT_APPRVD_RJCTD"
	 */
	public abstract Date getPoDateApprovedRejected();

	/**
	 * @ejb:interface-method view-type="local"
	 */
	public abstract void setPoDateApprovedRejected(Date PO_DT_APPRVD_RJCTD);

	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="PO_PSTD_BY"
	 */
	public abstract String getPoPostedBy();

	/**
	 * @ejb:interface-method view-type="local"
	 */
	public abstract void setPoPostedBy(String PO_PSTD_BY);

	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="PO_DT_PSTD"
	 */
	public abstract Date getPoDatePosted();

	/**
	 * @ejb:interface-method view-type="local"
	 */
	public abstract void setPoDatePosted(Date PO_DT_PSTD);

	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="PO_LCK"
	 */
	public abstract byte getPoLock();

	/**
	 * @ejb:interface-method view-type="local"
	 */
	public abstract void setPoLock(byte PO_LCK);


	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="PO_FRGHT"
	 */
	public abstract double getPoFreight();

	/**
	 * @ejb:interface-method view-type="local"
	 */
	public abstract void setPoFreight(double PO_FRGHT);

	
	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="PO_DTS"
	 */
	public abstract double getPoDuties();

	/**
	 * @ejb:interface-method view-type="local"
	 */
	public abstract void setPoDuties(double PO_DTS);
	
	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="PO_ENT_FEE"
	 */
	public abstract double getPoEntryFee();

	/**
	 * @ejb:interface-method view-type="local"
	 */
	public abstract void setPoEntryFee(double PO_ENT_FEE);
	
	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="PO_STRG"
	 */
	public abstract double getPoStorage();

	/**
	 * @ejb:interface-method view-type="local"
	 */
	public abstract void setPoStorage(double PO_STRG);
	
	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="PO_WHFG_HNDLNG"
	 */
	public abstract double getPoWharfageHandling();

	/**
	 * @ejb:interface-method view-type="local"
	 */
	public abstract void setPoWharfageHandling(double PO_WHFG_HNDLNG);


	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="PO_AD_BRNCH"
	 */
	public abstract Integer getPoAdBranch();

	/**
	 * @ejb:interface-method view-type="local"
	 */
	public abstract void setPoAdBranch(Integer PO_AD_BRNCH);

	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="PO_AD_CMPNY"
	 */
	public abstract Integer getPoAdCompany();

	/**
	 * @ejb:interface-method view-type="local"
	 */
	public abstract void setPoAdCompany(Integer PO_AD_CMPNY);

	// RELATIONSHIP METHODS

	/**
	 * @ejb:interface-method view-type="local"
	 * @ejb:relation name="supplier-purchaseorders"
	 *               role-name="purchaseorder-has-one-supplier"
	 *
	 * @jboss:relation related-pk-field="splCode" fk-column="AP_SUPPLIER"
	 */
	public abstract LocalApSupplier getApSupplier();
	/**
	 * @ejb:interface-method view-type="local"
	 */
	public abstract void setApSupplier(LocalApSupplier apSupplier);

	/**
	 * @ejb:interface-method view-type="local"
	 * @ejb:relation name="paymentterm-purchaseorders"
	 *               role-name="purchaseorder-has-one-paymentterm"
	 *
	 * @jboss:relation related-pk-field="pytCode" fk-column="AD_PAYMENT_TERM"
	 */
	public abstract com.ejb.ad.LocalAdPaymentTerm getAdPaymentTerm();
	/**
	 * @ejb:interface-method view-type="local"
	 */
	public abstract void setAdPaymentTerm(
			com.ejb.ad.LocalAdPaymentTerm adPaymentTerm);



        /**
	 * @ejb:interface-method view-type="local"
	 * @ejb:relation name="voucherbatch-purchaseorders"
	 *               role-name="purchaseorder-has-one-voucherbatch"
	 *
	 * @jboss:relation related-pk-field="vbCode" fk-column="AP_VOUCHER_BATCH"
	 */
	public abstract com.ejb.ap.LocalApVoucherBatch getApVoucherBatch();
	/**
	 * @ejb:interface-method view-type="local"
	 */
	public abstract void setApVoucherBatch(
			com.ejb.ap.LocalApVoucherBatch apVoucherBatch);




	/**
	 * @ejb:interface-method view-type="local"
	 * @ejb:relation name="functionalcurrency-purchaseorders"
	 *               role-name="purchaseorder-has-one-functionalcurrency"
	 *
	 * @jboss:relation related-pk-field="fcCode"
	 *                 fk-column="GL_FUNCTIONAL_CURRENCY"
	 */
	public abstract com.ejb.gl.LocalGlFunctionalCurrency getGlFunctionalCurrency();
	/**
	 * @ejb:interface-method view-type="local"
	 */
	public abstract void setGlFunctionalCurrency(
			com.ejb.gl.LocalGlFunctionalCurrency glFunctionalCurrency);

	/**
	 * @ejb:interface-method view-type="local"
	 * @ejb:relation name="aptaxcode-purchaseorders"
	 *               role-name="purchaseorder-has-one-aptaxcode"
	 *
	 * @jboss:relation related-pk-field="tcCode" fk-column="AP_TAX_CODE"
	 */
	public abstract LocalApTaxCode getApTaxCode();
	/**
	 * @ejb:interface-method view-type="local"
	 */
	public abstract void setApTaxCode(LocalApTaxCode apTaxCode);

	/**
	 * @ejb:interface-method view-type="local"
	 * @ejb:relation name="purchaseorder-purchaseorderlines"
	 *               role-name="purchaseorder-has-many-purchaseorderlines"
	 *
	 */
	public abstract Collection getApPurchaseOrderLines();

	public abstract void setApPurchaseOrderLines(Collection apPurchaseOrderLines);

	/**
	 * @ejb:interface-method view-type="local"
	 * @ejb:relation name="purchaseorder-apdistributionrecords"
	 *               role-name="purchaseorder-has-many-apdistributionrecords"
	 *
	 */
	public abstract Collection getApDistributionRecords();

	public abstract void setApDistributionRecords(
			Collection apDistributionRecords);

	/**
	 * @jboss:dynamic-ql
	 */
	public abstract Collection ejbSelectGeneric(java.lang.String jbossQl,
			java.lang.Object[] args) throws FinderException;

	// BUSINESS METHODS

	/**
	 * @ejb:home-method view-type="local"
	 */
	public Collection ejbHomeGetPoByCriteria(java.lang.String jbossQl,
			java.lang.Object[] args) throws FinderException {

		return ejbSelectGeneric(jbossQl, args);
	}

	/**
	 * @ejb:interface-method view-type="local"
	 */
	public void addApPurchaseOrderLine(
			LocalApPurchaseOrderLine apPurchaseOrderLine) {

		Debug.print("ApPurchaseOrderBean addApPurchaseOrderLine");

		try {

			Collection apPurchaseOrderLines = getApPurchaseOrderLines();
			apPurchaseOrderLines.add(apPurchaseOrderLine);

		} catch (Exception ex) {

			throw new EJBException(ex.getMessage());

		}

	}

	/**
	 * @ejb:interface-method view-type="local"
	 */
	public void dropApPurchaseOrderLine(
			LocalApPurchaseOrderLine apPurchaseOrderLine) {

		Debug.print("ApPurchaseOrderBean dropApPurchaseOrderLine");

		try {

			Collection apPurchaseOrderLines = getApPurchaseOrderLines();
			apPurchaseOrderLines.remove(apPurchaseOrderLine);

		} catch (Exception ex) {

			throw new EJBException(ex.getMessage());

		}

	}

	/**
	 * @ejb:interface-method view-type="local"
	 */
	public void addApDistributionRecord(
			LocalApDistributionRecord apDistributionRecord) {

		Debug.print("ApPurchaseOrderBean addApDistributionRecord");

		try {

			Collection apDistributionRecords = getApDistributionRecords();
			apDistributionRecords.add(apDistributionRecord);

		} catch (Exception ex) {

			throw new EJBException(ex.getMessage());

		}

	}

	/**
	 * @ejb:interface-method view-type="local"
	 */
	public void dropApDistributionRecord(
			LocalApDistributionRecord apDistributionRecord) {

		Debug.print("ApPurchaseOrderBean dropApDistributionRecord");

		try {

			Collection apDistributionRecords = getApDistributionRecords();
			apDistributionRecords.remove(apDistributionRecord);

		} catch (Exception ex) {

			throw new EJBException(ex.getMessage());

		}

	}

	/**
	 * @ejb:interface-method view-type="local"
	 */
	public short getApDrNextLine() {

		Debug.print("ApPurchaseOrderBean getApDrNextLine");

		try {

			Collection apDistributionRecords = getApDistributionRecords();
			return (short) (apDistributionRecords.size() + 1);

		} catch (Exception ex) {

			throw new EJBException(ex.getMessage());

		}
	}

	// ENTITY METHODS

	/**
	 * @ejb:create-method view-type="local"
	 */
	public Integer ejbCreate(Integer PO_CODE, byte PO_RCVNG, String PO_TYP,
			Date PO_DT, Date PO_DLVRY_PRD, String PO_DCMNT_NMBR, String PO_RFRNC_NMBR,
			String PO_RCV_PO_NMBR, String PO_DESC, String PO_BLL_TO,
			String PO_SHP_TO, Date PO_CNVRSN_DT, double PO_CNVRSN_RT, double PO_TTL_AMNT,
			byte PO_PRNTD, byte PO_VD, String PO_SHPMNT_NMBR, String PO_APPRVL_STATUS, byte PO_PSTD,
			String PO_RSN_FR_RJCTN, String PO_CRTD_BY, Date PO_DT_CRTD,
			String PO_LST_MDFD_BY, Date PO_DT_LST_MDFD,
			String PO_APPRVD_RJCTD_BY, Date PO_DT_APPRVD_RJCTD,
			String PO_PSTD_BY, Date PO_DT_PSTD, byte PO_LCK,
			double PO_FRGHT, double PO_DTS, double PO_ENT_FEE, double PO_STRG, double PO_WHFG_HNDLNG,
			Integer PO_AD_BRNCH,Integer PO_AD_CMPNY) throws CreateException {

		Debug.print("ApPurchaseOrderBean ejbCreate1");

		setPoCode(PO_CODE);
		setPoReceiving(PO_RCVNG);
		setPoType(PO_TYP);
		setPoDate(PO_DT);
		setPoDeliveryPeriod(PO_DLVRY_PRD);
		setPoDocumentNumber(PO_DCMNT_NMBR);
		setPoReferenceNumber(PO_RFRNC_NMBR);
		setPoRcvPoNumber(PO_RCV_PO_NMBR);
		setPoDescription(PO_DESC);
		setPoBillTo(PO_BLL_TO);
		setPoShipTo(PO_SHP_TO);
		setPoConversionDate(PO_CNVRSN_DT);
		setPoConversionRate(PO_CNVRSN_RT);
		setPoTotalAmount(PO_TTL_AMNT);
		setPoPrinted(PO_PRNTD);
		setPoVoid(PO_VD);
		setPoShipmentNumber(PO_SHPMNT_NMBR);
		setPoApprovalStatus(PO_APPRVL_STATUS);
		setPoPosted(PO_PSTD);
		setPoReasonForRejection(PO_RSN_FR_RJCTN);
		setPoCreatedBy(PO_CRTD_BY);
		setPoDateCreated(PO_DT_CRTD);
		setPoLastModifiedBy(PO_LST_MDFD_BY);
		setPoDateLastModified(PO_DT_LST_MDFD);
		setPoApprovedRejectedBy(PO_APPRVD_RJCTD_BY);
		setPoDateApprovedRejected(PO_DT_APPRVD_RJCTD);
		setPoPostedBy(PO_PSTD_BY);
		setPoDatePosted(PO_DT_PSTD);
		setPoLock(PO_LCK);
		setPoFreight(PO_FRGHT);
		setPoDuties(PO_DTS);
		setPoEntryFee(PO_ENT_FEE);
		setPoStorage(PO_STRG);
		setPoWharfageHandling(PO_WHFG_HNDLNG);
		setPoAdBranch(PO_AD_BRNCH);
		setPoAdCompany(PO_AD_CMPNY);

		return null;

	}

	/**
	 * @ejb:create-method view-type="local"
	 */
	public Integer ejbCreate(byte PO_RCVNG, String PO_TYP, Date PO_DT, Date PO_DLVRY_PRD,
			String PO_DCMNT_NMBR, String PO_RFRNC_NMBR, String PO_RCV_PO_NMBR,
			String PO_DESC, String PO_BLL_TO, String PO_SHP_TO,
			Date PO_CNVRSN_DT, double PO_CNVRSN_RT, double PO_TTL_AMNT, 
			byte PO_PRNTD, byte PO_VD, String PO_SHPMNT_NMBR,
			String PO_APPRVL_STATUS, byte PO_PSTD, String PO_RSN_FR_RJCTN,
			String PO_CRTD_BY, Date PO_DT_CRTD, String PO_LST_MDFD_BY,
			Date PO_DT_LST_MDFD, String PO_APPRVD_RJCTD_BY,
			Date PO_DT_APPRVD_RJCTD, String PO_PSTD_BY, Date PO_DT_PSTD, byte PO_LCK,
			double PO_FRGHT, double PO_DTS, double PO_ENT_FEE, double PO_STRG, double PO_WHFG_HNDLNG,
			Integer PO_AD_BRNCH,Integer PO_AD_CMPNY) throws CreateException {

		Debug.print("ApPurchaseOrderBean ejbCreate2");

		setPoDocumentNumber(PO_DCMNT_NMBR);
		setPoReceiving(PO_RCVNG);
		setPoType(PO_TYP);
		setPoDate(PO_DT);
		setPoDeliveryPeriod(PO_DLVRY_PRD);
		setPoDocumentNumber(PO_DCMNT_NMBR);
		setPoReferenceNumber(PO_RFRNC_NMBR);
		setPoRcvPoNumber(PO_RCV_PO_NMBR);
		setPoDescription(PO_DESC);
		setPoBillTo(PO_BLL_TO);
		setPoShipTo(PO_SHP_TO);
		setPoConversionDate(PO_CNVRSN_DT);
		setPoConversionRate(PO_CNVRSN_RT);
		setPoTotalAmount(PO_TTL_AMNT);
		setPoPrinted(PO_PRNTD);
		setPoVoid(PO_VD);
		setPoShipmentNumber(PO_SHPMNT_NMBR);
		setPoApprovalStatus(PO_APPRVL_STATUS);
		setPoPosted(PO_PSTD);
		setPoReasonForRejection(PO_RSN_FR_RJCTN);
		setPoCreatedBy(PO_CRTD_BY);
		setPoDateCreated(PO_DT_CRTD);
		setPoLastModifiedBy(PO_LST_MDFD_BY);
		setPoDateLastModified(PO_DT_LST_MDFD);
		setPoApprovedRejectedBy(PO_APPRVD_RJCTD_BY);
		setPoDateApprovedRejected(PO_DT_APPRVD_RJCTD);
		setPoPostedBy(PO_PSTD_BY);
		setPoDatePosted(PO_DT_PSTD);
		setPoLock(PO_LCK);
		setPoFreight(PO_FRGHT);
		setPoDuties(PO_DTS);
		setPoEntryFee(PO_ENT_FEE);
		setPoStorage(PO_STRG);
		setPoWharfageHandling(PO_WHFG_HNDLNG);
		setPoAdBranch(PO_AD_BRNCH);
		setPoAdCompany(PO_AD_CMPNY);

		return null;

	}

	public void ejbPostCreate(Integer PO_CODE, byte PO_RCVNG, String PO_TYP,
			Date PO_DT, Date PO_DLVRY_PRD, String PO_DCMNT_NMBR, String PO_RFRNC_NMBR,
			String PO_RCV_PO_NMBR, String PO_DESC, String PO_BLL_TO,
			String PO_SHP_TO, Date PO_CNVRSN_DT, double PO_CNVRSN_RT, double PO_TTL_AMNT,
			byte PO_PRNTD, byte PO_VD, String PO_SHPMNT_NMBR,
			String PO_APPRVL_STATUS, byte PO_PSTD,
			String PO_RSN_FR_RJCTN, String PO_CRTD_BY, Date PO_DT_CRTD,
			String PO_LST_MDFD_BY, Date PO_DT_LST_MDFD,
			String PO_APPRVD_RJCTD_BY, Date PO_DT_APPRVD_RJCTD,
			String PO_PSTD_BY, Date PO_DT_PSTD, byte PO_LCK,
			double PO_FRGHT, double PO_DTS, double PO_ENT_FEE, double PO_STRG, double PO_WHFG_HNDLNG,
			Integer PO_AD_BRNCH, Integer PO_AD_CMPNY) throws CreateException {
	}

	public void ejbPostCreate(byte PO_RCVNG, String PO_TYP, Date PO_DT, Date PO_DLVRY_PRD,
			String PO_DCMNT_NMBR, String PO_RFRNC_NMBR, String PO_RCV_PO_NMBR,
			String PO_DESC, String PO_BLL_TO, String PO_SHP_TO,
			Date PO_CNVRSN_DT, double PO_CNVRSN_RT, double PO_TTL_AMNT, byte PO_PRNTD, byte PO_VD, String PO_SHPMNT_NMBR,
			String PO_APPRVL_STATUS, byte PO_PSTD, String PO_RSN_FR_RJCTN,
			String PO_CRTD_BY, Date PO_DT_CRTD, String PO_LST_MDFD_BY,
			Date PO_DT_LST_MDFD, String PO_APPRVD_RJCTD_BY,
			Date PO_DT_APPRVD_RJCTD, String PO_PSTD_BY, Date PO_DT_PSTD, byte PO_LCK,
			double PO_FRGHT, double PO_DTS, double PO_ENT_FEE, double PO_STRG, double PO_WHFG_HNDLNG,
			Integer PO_AD_BRNCH, Integer PO_AD_CMPNY) throws CreateException {
	}

}
