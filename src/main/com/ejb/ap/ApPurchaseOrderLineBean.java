/*
 * com/ejb/ap/LocalApPurchaseOrderLineBean.java
 *
 * Created on April 13, 2005 01:16 PM
 */

package com.ejb.ap;

import java.util.Collection;
import java.util.Date;

import javax.ejb.CreateException;
import javax.ejb.FinderException;

import com.ejb.inv.LocalInvCosting;
import com.ejb.inv.LocalInvItemLocation;
import com.ejb.inv.LocalInvUnitOfMeasure;
import com.util.AbstractEntityBean;
import com.util.Debug;

/**
 * 
 * @author  Enrico C. Yap
 * 
 * @ejb:bean name="ApPurchaseOrderLineEJB"
 *           display-name="Purchase Order Line Entity"
 *           type="CMP"
 *           cmp-version="2.x"
 *           view-type="local"
 *           local-jndi-name="omega-ejb/ApPurchaseOrderLineEJB"
 *           schema="ApPurchaseOrderLine"
 *           primkey-field="plCode"
 *       
 * @ejb:pk class="java.lang.Integer"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="apuser"
 *                        role-link="apuserlink"
 *
 * @ejb:permission role-name="apuser"
 *
 * @ejb:interface local-class="com.ejb.ap.LocalApPurchaseOrderLine"
 *                local-extends="javax.ejb.EJBLocalObject"
 *
 * @ejb:home local-class="com.ejb.ap.LocalApPurchaseOrderLineHome"
 *           local-extends="javax.ejb.EJBLocalHome"
 * 
 * @ejb:select signature="java.util.Date ejbSelectMaxPoDateByIiNameAndLocNameAndPoReceivingAndPoPostedAndSplSupplierCode(java.lang.String II_NM, java.lang.String LOC_NM, byte PO_RCVNG, byte PO_PSTD, java.lang.String SPL_SPPLR_CD, java.lang.Integer PO_AD_BRNCH, java.lang.Integer PL_AD_CMPNY)"
 *             result-type-mapping="Local"
 *             method-intf="LocalHome"
 *             query="SELECT MAX(pl.apPurchaseOrder.poDate) FROM ApPurchaseOrderLine pl WHERE pl.invItemLocation.invItem.iiName=?1 AND pl.invItemLocation.invLocation.locName=?2 AND pl.apPurchaseOrder.poReceiving=?3 AND pl.apPurchaseOrder.poPosted=?4 AND pl.apPurchaseOrder.apSupplier.splSupplierCode=?5 AND pl.apPurchaseOrder.poAdBranch = ?6 AND pl.plAdCompany = ?7"
 * 
 * @jboss:query signature="java.util.Date ejbSelectMaxPoDateByIiNameAndLocNameAndPoReceivingAndPoPostedAndSplSupplierCode(java.lang.String II_NM, java.lang.String LOC_NM, byte PO_RCVNG, byte PO_PSTD, java.lang.String SPL_SPPLR_CD, java.lang.Integer PO_AD_BRNCH, java.lang.Integer PL_AD_CMPNY)"
 *             query="SELECT MAX(pl.apPurchaseOrder.poDate) FROM ApPurchaseOrderLine pl WHERE pl.invItemLocation.invItem.iiName=?1 AND pl.invItemLocation.invLocation.locName=?2 AND pl.apPurchaseOrder.poReceiving=?3 AND pl.apPurchaseOrder.poPosted=?4 AND pl.apPurchaseOrder.apSupplier.splSupplierCode=?5 AND pl.apPurchaseOrder.poAdBranch = ?6 AND pl.plAdCompany = ?7"
 * 
 * @ejb:select signature="java.lang.Double ejbSelectMaxPlCodeByPoDateAndIiNameAndLocNameAndPoReceivingAndPoPostedAndSplSupplierCode(java.util.Date PO_DT, java.lang.String II_NM, java.lang.String LOC_NM, byte PO_RCVNG, byte PO_PSTD, java.lang.String SPL_SPPLR_CD, java.lang.Integer PO_AD_BRNCH, java.lang.Integer PL_AD_CMPNY)"
 *             result-type-mapping="Local"
 *             method-intf="LocalHome"
 *             query="SELECT MAX(pl.plCode) FROM ApPurchaseOrderLine pl WHERE pl.apPurchaseOrder.poDate=?1 AND pl.invItemLocation.invItem.iiName=?2 AND pl.invItemLocation.invLocation.locName=?3 AND pl.apPurchaseOrder.poReceiving=?4 AND pl.apPurchaseOrder.poPosted=?5 AND pl.apPurchaseOrder.apSupplier.splSupplierCode=?6 AND pl.apPurchaseOrder.poAdBranch = ?7 AND pl.plAdCompany = ?8"
 * 
 * @jboss:query signature="java.lang.Double ejbSelectMaxPlCodeByPoDateAndIiNameAndLocNameAndPoReceivingAndPoPostedAndSplSupplierCode(java.util.Date PO_DT, java.lang.String II_NM, java.lang.String LOC_NM, byte PO_RCVNG, byte PO_PSTD, java.lang.String SPL_SPPLR_CD, java.lang.Integer PO_AD_BRNCH, java.lang.Integer PL_AD_CMPNY)"
 *             query="SELECT MAX(pl.plCode) FROM ApPurchaseOrderLine pl WHERE pl.apPurchaseOrder.poDate=?1 AND pl.invItemLocation.invItem.iiName=?2 AND pl.invItemLocation.invLocation.locName=?3 AND pl.apPurchaseOrder.poReceiving=?4 AND pl.apPurchaseOrder.poPosted=?5 AND pl.apPurchaseOrder.apSupplier.splSupplierCode=?6 AND pl.apPurchaseOrder.poAdBranch = ?7 AND pl.plAdCompany = ?8"
 * 
 * @ejb:select signature="java.util.Date ejbSelectMaxPoDateByIiNameAndLocNameAndPoReceivingAndPoPosted(java.lang.String II_NM, java.lang.String LOC_NM, byte PO_RCVNG, byte PO_PSTD java.lang.Integer PO_AD_BRNCH, java.lang.Integer PL_AD_CMPNY)"
 *             result-type-mapping="Local"
 *             method-intf="LocalHome"
 *             query="SELECT MAX(pl.apPurchaseOrder.poDate) FROM ApPurchaseOrderLine pl WHERE pl.invItemLocation.invItem.iiName=?1 AND pl.invItemLocation.invLocation.locName=?2 AND pl.apPurchaseOrder.poReceiving=?3 AND pl.apPurchaseOrder.poPosted=?4 AND pl.apPurchaseOrder.poAdBranch = ?5 AND pl.plAdCompany = ?6"
 * 
 * @jboss:query signature="java.util.Date ejbSelectMaxPoDateByIiNameAndLocNameAndPoReceivingAndPoPosted(java.lang.String II_NM, java.lang.String LOC_NM, byte PO_RCVNG, byte PO_PSTD, java.lang.Integer PO_AD_BRNCH, java.lang.Integer PL_AD_CMPNY)"
 *             query="SELECT MAX(pl.apPurchaseOrder.poDate) FROM ApPurchaseOrderLine pl WHERE pl.invItemLocation.invItem.iiName=?1 AND pl.invItemLocation.invLocation.locName=?2 AND pl.apPurchaseOrder.poReceiving=?3 AND pl.apPurchaseOrder.poPosted=?4 AND pl.apPurchaseOrder.poAdBranch = ?5 AND pl.plAdCompany = ?6"
 * 
 * @ejb:select signature="java.lang.Double ejbSelectMaxPlCodeByPoDateAndIiNameAndLocNameAndPoReceivingAndPoPosted(java.util.Date PO_DT, java.lang.String II_NM, java.lang.String LOC_NM, byte PO_RCVNG, byte PO_PSTD, java.lang.Integer PO_AD_BRNCH, java.lang.Integer PL_AD_CMPNY)"
 *             result-type-mapping="Local"
 *             method-intf="LocalHome"
 *             query="SELECT MAX(pl.plCode) FROM ApPurchaseOrderLine pl WHERE pl.apPurchaseOrder.poDate=?1 AND pl.invItemLocation.invItem.iiName=?2 AND pl.invItemLocation.invLocation.locName=?3 AND pl.apPurchaseOrder.poReceiving=?4 AND pl.apPurchaseOrder.poPosted=?5 AND pl.apPurchaseOrder.poAdBranch = ?6 AND pl.plAdCompany = ?7"
 * 
 * @jboss:query signature="java.lang.Double ejbSelectMaxPlCodeByPoDateAndIiNameAndLocNameAndPoReceivingAndPoPosted(java.util.Date PO_DT, java.lang.String II_NM, java.lang.String LOC_NM, byte PO_RCVNG, byte PO_PSTD, java.lang.Integer PO_AD_BRNCH, java.lang.Integer PL_AD_CMPNY)"
 *             query="SELECT MAX(pl.plCode) FROM ApPurchaseOrderLine pl WHERE pl.apPurchaseOrder.poDate=?1 AND pl.invItemLocation.invItem.iiName=?2 AND pl.invItemLocation.invLocation.locName=?3 AND pl.apPurchaseOrder.poReceiving=?4 AND pl.apPurchaseOrder.poPosted=?5 AND pl.apPurchaseOrder.poAdBranch = ?6 AND pl.plAdCompany = ?7"
 *
 * @ejb:select signature="LocalApPurchaseOrderLine ejbSelectByPlCode(java.lang.Integer PL_CD, java.lang.Integer PL_AD_CMPNY)"
 *             result-type-mapping="Local"
 *             method-intf="LocalHome"
 *             query="SELECT OBJECT(pl) FROM ApPurchaseOrderLine pl WHERE pl.plCode=?1 AND pl.plAdCompany = ?2"
 *
 * @jboss:query signature="LocalApPurchaseOrderLine ejbSelectByPlCode(java.lang.Integer PL_CD, java.lang.Integer PL_AD_CMPNY)"
 *             query="SELECT OBJECT(pl) FROM ApPurchaseOrderLine pl WHERE pl.plCode=?1 AND pl.plAdCompany = ?2"
 *
 * @ejb:select signature="java.util.Date ejbSelectMaxPoDateByIiNameAndLocNameAndPoReceivingAndPoPostedAndLessThanEqualPoDate(java.lang.String II_NM, java.lang.String LOC_NM, byte PO_RCVNG, byte PO_PSTD, java.util.Date PO_DT, java.lang.Integer PO_AD_BRNCH, java.lang.Integer PL_AD_CMPNY)"
 *             result-type-mapping="Local"
 *             method-intf="LocalHome"
 *             query="SELECT MAX(pl.apPurchaseOrder.poDate) FROM ApPurchaseOrderLine pl WHERE pl.invItemLocation.invItem.iiName=?1 AND pl.invItemLocation.invLocation.locName=?2 AND pl.apPurchaseOrder.poReceiving=?3 AND pl.apPurchaseOrder.poPosted=?4 AND pl.apPurchaseOrder.poDate <= ?5 AND pl.apPurchaseOrder.poAdBranch = ?6 AND pl.plAdCompany = ?7"
 * 
 * @jboss:query signature="java.util.Date ejbSelectMaxPoDateByIiNameAndLocNameAndPoReceivingAndPoPostedAndLessThanEqualPoDate(java.lang.String II_NM, java.lang.String LOC_NM, byte PO_RCVNG, byte PO_PSTD, java.util.Date PO_DT, java.lang.Integer PO_AD_BRNCH, java.lang.Integer PL_AD_CMPNY)"
 *             query="SELECT MAX(pl.apPurchaseOrder.poDate) FROM ApPurchaseOrderLine pl WHERE pl.invItemLocation.invItem.iiName=?1 AND pl.invItemLocation.invLocation.locName=?2 AND pl.apPurchaseOrder.poReceiving=?3 AND pl.apPurchaseOrder.poPosted=?4 AND pl.apPurchaseOrder.poDate <= ?5 AND pl.apPurchaseOrder.poAdBranch = ?6 AND pl.plAdCompany = ?7"
 * 
 * @ejb:finder signature="Collection findByPlPlCode(java.lang.Integer P_PL_CODE, java.lang.Integer CHK_AD_CMPNY)"
 *             query="SELECT OBJECT(pl) FROM ApPurchaseOrderLine pl WHERE pl.plPlCode = ?1 AND pl.plAdCompany = ?2"
 *             
 * @ejb:finder signature="Collection findRrForProcessing(java.lang.Integer AD_BRNCH, java.lang.Integer AD_CMPNY)"
 *             query="SELECT OBJECT(pl) FROM ApPurchaseOrderLine pl WHERE pl.apPurchaseOrder.poPosted = 1 AND pl.apPurchaseOrder.poReceiving = 1 AND pl.apVoucher IS NULL AND pl.apPurchaseOrder.poAdBranch = ?1 AND pl.plAdCompany = ?2"
 *             
 * @ejb:finder signature="Collection findByPlIlCodeAndPoReceivingAndPoPostedAndBrCode(java.lang.Integer IL_CODE, byte PO_RCVNG, byte PO_PSTD, java.lang.Integer PO_AD_BRNCH, java.lang.Integer PO_AD_CMPNY)"
 *             query="SELECT OBJECT(pl) FROM ApPurchaseOrderLine pl WHERE pl.invItemLocation.ilCode=?1 AND pl.apPurchaseOrder.poReceiving=?2 AND pl.apPurchaseOrder.poPosted=?3 AND pl.apPurchaseOrder.poAdBranch = ?4 AND pl.plAdCompany = ?5"
 *
 * @jboss:query signature="Collection findByPlIlCodeAndPoReceivingAndPoPostedAndBrCode(java.lang.Integer IL_CODE, byte PO_RCVNG, byte PO_PSTD, java.lang.Integer PO_AD_BRNCH, java.lang.Integer PO_AD_CMPNY)"
 *             query="SELECT OBJECT(pl) FROM ApPurchaseOrderLine pl WHERE pl.invItemLocation.ilCode=?1 AND pl.apPurchaseOrder.poReceiving=?2 AND pl.apPurchaseOrder.poPosted=?3 AND pl.apPurchaseOrder.poAdBranch = ?4 AND pl.plAdCompany = ?5 ORDER BY pl.apPurchaseOrder.poDate DESC, pl.apPurchaseOrder.poCode DESC"
 * 
 * @jboss:query signature="Collection ejbSelectGeneric(java.lang.String jbossQl, java.lang.Object[] args)"
 *                         dynamic="true"
 *
 * @ejb:value-object match="*"
 *             name="ApPurchaseOrderLine"
 * 
 * @ejb:finder signature="Collection findOpenPlBySplSupplierCodeAndBrCode(java.lang.String SPL_SPPLR_CODE, java.lang.Integer PL_AD_BRNCH, java.lang.Integer PL_AD_CMPNY)"
 *             query="SELECT OBJECT(pl) FROM ApPurchaseOrderLine pl WHERE pl.apPurchaseOrder.poPosted = 1 AND pl.apPurchaseOrder.poReceiving = 1 AND pl.apVoucher IS NULL AND pl.apPurchaseOrder.apSupplier.splSupplierCode = ?1 AND pl.apPurchaseOrder.poAdBranch = ?2 AND pl.plAdCompany = ?3"
 *
 * @ejb:finder signature="Collection findOpenPlByPoDcmntNmbrAndBrCode(java.lang.String PO_DCMNT_NMBR, java.lang.Integer PL_AD_BRNCH, java.lang.Integer PL_AD_CMPNY)"
 *             query="SELECT OBJECT(pl) FROM ApPurchaseOrderLine pl WHERE pl.apPurchaseOrder.poPosted = 1 AND pl.apPurchaseOrder.poReceiving = 1 AND pl.apVoucher IS NULL AND pl.apPurchaseOrder.poDocumentNumber = ?1 AND pl.apPurchaseOrder.poAdBranch = ?2 AND pl.plAdCompany = ?3"
 *
 * @ejb:finder signature="Collection findOpenPlByBrCode(java.lang.Integer PL_AD_BRNCH, java.lang.Integer PL_AD_CMPNY)"
 *             query="SELECT OBJECT(pl) FROM ApPurchaseOrderLine pl WHERE pl.apPurchaseOrder.poPosted = 1 AND pl.apPurchaseOrder.poReceiving = 1 AND pl.apVoucher IS NULL AND pl.apPurchaseOrder.poAdBranch = ?1 AND pl.plAdCompany = ?2"
 *
 * 
 * @ejb:finder signature="Collection findOpenPlBySplSupplierCodeAndBrCode1(java.lang.String SPL_SPPLR_CODE, java.lang.Integer PL_AD_BRNCH, java.lang.Integer PL_AD_CMPNY)"
 *             query="SELECT OBJECT(pl) FROM ApPurchaseOrderLine pl WHERE pl.apPurchaseOrder.poPosted = 1 AND pl.apVoucher IS NULL AND pl.apPurchaseOrder.apSupplier.splSupplierCode = ?1 AND pl.apPurchaseOrder.poAdBranch = ?2 AND pl.plAdCompany = ?3"
 *             
 * @ejb:finder signature="Collection findByPlIlCodeAndPoReceivingAndBrCode(java.lang.Integer IL_CODE, byte PO_RCVNG, java.lang.Integer PL_AD_BRNCH, java.lang.Integer PO_AD_CMPNY)"
 *             query="SELECT OBJECT(pl) FROM ApPurchaseOrderLine pl WHERE pl.invItemLocation.ilCode=?1 AND pl.apPurchaseOrder.poReceiving=?2 AND pl.apPurchaseOrder.poAdBranch = ?3 AND pl.plAdCompany = ?4"
 *
 * @jboss:query signature="Collection findByPlIlCodeAndPoReceivingAndBrCode(java.lang.Integer IL_CODE, byte PO_RCVNG, java.lang.Integer PL_AD_BRNCH, java.lang.Integer PO_AD_CMPNY)"
 *             query="SELECT OBJECT(pl) FROM ApPurchaseOrderLine pl WHERE pl.invItemLocation.ilCode=?1 AND pl.apPurchaseOrder.poReceiving=?2 AND pl.apPurchaseOrder.poAdBranch = ?3 AND pl.plAdCompany = ?4 ORDER BY pl.apPurchaseOrder.poDate DESC, pl.apPurchaseOrder.poCode DESC"
 *             
 *             
 * @ejb:finder signature="Collection findUnpostedPoByIiNameAndLocNameAndLessEqualDateAndPoAdBranch(java.lang.String II_NM, java.lang.String LOC_NM, java.util.Date PO_DT, java.lang.Integer PO_AD_BRNCH, java.lang.Integer PL_AD_CMPNY)"
 * 			   query="SELECT OBJECT(pl) FROM ApPurchaseOrderLine pl WHERE pl.apPurchaseOrder.poPosted = 0 AND pl.apPurchaseOrder.poVoid = 0 AND pl.apPurchaseOrder.poReceiving = 1 AND pl.invItemLocation.invItem.iiName = ?1 AND pl.invItemLocation.invLocation.locName = ?2 AND pl.apPurchaseOrder.poDate <= ?3 AND pl.apPurchaseOrder.poAdBranch=?4 AND pl.plAdCompany = ?5"
 *
 * @ejb:finder signature="Collection findUnservedPoByIiNameAndLocNameAndLessEqualDateAndPoAdBranch(java.lang.String II_NM, java.lang.String LOC_NM, java.util.Date PO_DT, java.lang.Integer PO_AD_BRNCH, java.lang.Integer PL_AD_CMPNY)"
 * 			   query="SELECT OBJECT(pl) FROM ApPurchaseOrderLine pl WHERE pl.apPurchaseOrder.poReceiving=0 AND pl.apPurchaseOrder.poVoid=0 AND pl.invItemLocation.invItem.iiName = ?1 AND pl.invItemLocation.invLocation.locName = ?2 AND pl.apPurchaseOrder.poDate <= ?3 AND pl.apPurchaseOrder.poAdBranch=?4 AND pl.plAdCompany = ?5"
 *
 * @ejb:finder signature="Collection findUnpostedPoByLocNameAndAdBranch(java.lang.String LOC_NM, java.lang.Integer PO_AD_BRNCH, java.lang.Integer PL_AD_CMPNY)"
 * 			   query="SELECT OBJECT(pl) FROM ApPurchaseOrderLine pl WHERE pl.apPurchaseOrder.poPosted = 0 AND pl.apPurchaseOrder.poVoid = 0 AND pl.invItemLocation.invLocation.locName = ?1 AND pl.apPurchaseOrder.poAdBranch = ?2 AND pl.plAdCompany = ?3"
 *
 * @ejb:finder signature="Collection findPoByIiCodeAndLocNameAndAdBranch(java.lang.Integer II_CODE, java.lang.String LOC_NM, java.lang.Integer PO_AD_BRNCH, java.lang.Integer PL_AD_CMPNY)"
 * 			   query="SELECT OBJECT(pl) FROM ApPurchaseOrderLine pl WHERE pl.apPurchaseOrder.poVoid = 0 AND pl.invItemLocation.invItem.iiCode=?1 AND pl.invItemLocation.invLocation.locName = ?2 AND pl.apPurchaseOrder.poAdBranch = ?3 AND pl.plAdCompany = ?4"
 *
 * @ejb:finder signature="LocalApPurchaseOrderLine findPlByPoNumberAndIiNameAndLocName(java.lang.String PO_DCMNT_NMBR, java.lang.String II_NM, java.lang.String LOC_NM, java.lang.Integer PO_AD_BRNCH, java.lang.Integer PL_AD_CMPNY)"
 * 			   query="SELECT OBJECT(pl) FROM ApPurchaseOrderLine pl WHERE pl.apPurchaseOrder.poVoid = 0 AND pl.apPurchaseOrder.poDocumentNumber = ?1 AND pl.invItemLocation.invItem.iiName = ?2 AND pl.invItemLocation.invLocation.locName = ?3 AND pl.apPurchaseOrder.poAdBranch = ?4 AND pl.plAdCompany = ?5"
 *
 * @ejb:finder signature="Collection findByPoReceivingAndPoShipmentAndBrCode(byte PO_RCVNG, java.lang.String PO_SHPMNT_NMBR, java.lang.Integer PO_AD_CMPNY)"
 *             query="SELECT OBJECT(pl) FROM ApPurchaseOrderLine pl WHERE pl.apPurchaseOrder.poPosted=1 AND pl.apPurchaseOrder.poReceiving=?1 AND pl.apPurchaseOrder.poShipmentNumber = ?2 AND pl.plAdCompany = ?3"
 *             
 * @jboss:persistence table-name="AP_PRCHS_ORDR_LN"
 *
 * @jboss:entity-command name="mysql-get-generated-keys"
 *
 */

public abstract class ApPurchaseOrderLineBean extends AbstractEntityBean {
	
	   // Access methods for persistent fields

	   /**
	    * @ejb:persistent-field
	    * @ejb:interface-method view-type="local"
	    * @ejb:pk-field
	    *
	    * @jboss:column-name name="PL_CODE"
	    *
	    * @jboss:persistence auto-increment="true"
	    **/
	   public abstract java.lang.Integer getPlCode();
	   public abstract void setPlCode(java.lang.Integer PL_CODE);
	   
	   /**
	    * @ejb:persistent-field
	    * @ejb:interface-method view-type="local"
	    *
	    * @jboss:column-name name="PL_LN"
	    **/
	   public abstract short getPlLine();
	   /**
	    * @ejb:interface-method view-type="local"
	    **/
	   public abstract void setPlLine(short PL_LN);

	   /**
	    * @ejb:persistent-field
	    * @ejb:interface-method view-type="local"
	    *
	    * @jboss:column-name name="PL_QTY"
	    **/
	   public abstract double getPlQuantity();
	   /**
	    * @ejb:interface-method view-type="local"
	    **/
	   public abstract void setPlQuantity(double PL_QTY);
	   
	   /**
	    * @ejb:persistent-field
	    * @ejb:interface-method view-type="local"
	    *
	    * @jboss:column-name name="PL_UNT_CST"
	    **/
	   public abstract double getPlUnitCost();
	   /**
	    * @ejb:interface-method view-type="local"
	    **/
	   public abstract void setPlUnitCost(double PL_UNT_CST);
	   
	   /**
	    * @ejb:persistent-field
	    * @ejb:interface-method view-type="local"
	    *
	    * @jboss:column-name name="PL_AMNT"
	    **/
	   public abstract double getPlAmount();
	   /**
	    * @ejb:interface-method view-type="local"
	    **/
	   public abstract void setPlAmount(double PL_AMNT);
	   
	   
	   /**
	    * @ejb:persistent-field
	    * @ejb:interface-method view-type="local"
	    *
	    * @jboss:column-name name="PL_UNT_CST_LCL"
	    **/
	   public abstract double getPlUnitCostLocal();
	   /**
	    * @ejb:interface-method view-type="local"
	    **/
	   public abstract void setPlUnitCostLocal(double PL_UNT_CST_LCL);
	   
	   
	   /**
	    * @ejb:persistent-field
	    * @ejb:interface-method view-type="local"
	    *
	    * @jboss:column-name name="PL_AMNT_LCL"
	    **/
	   public abstract double getPlAmountLocal();
	   /**
	    * @ejb:interface-method view-type="local"
	    **/
	   public abstract void setPlAmountLocal(double PL_AMNT_LCL);
	   
	   /**
	    * @ejb:persistent-field
	    * @ejb:interface-method view-type="local"
	    *
	    * @jboss:column-name name="PL_ADDON_CST"
	    **/
	   public abstract double getPlAddonCost();
	   
	   /**
	    * @ejb:interface-method view-type="local"
	    **/
	   public abstract void setPlAddonCost(double PL_ADDON_CST);
	   
	   
	   /**
	    * @ejb:persistent-field
	    * @ejb:interface-method view-type="local"
	    *
	    * @jboss:column-name name="PL_QC_NUM"
	    **/
	   public abstract String getPlQcNumber();
	   /**
	    * @ejb:interface-method view-type="local"
	    **/
	   public abstract void setPlQcNumber(String PL_QC_NUM);
	   
	   /**
		 * @ejb:persistent-field
		 * @ejb:interface-method view-type="local"
		 * 
		 * @jboss:column-name name="PL_QC_EXPRY_DT"
		 */
		public abstract Date getPlQcExpiryDate();

		/**
		 * @ejb:interface-method view-type="local"
		 */
		public abstract void setPlQcExpiryDate(Date PL_QC_EXPRY_DT);
	   

		
	   /**
	    * @ejb:persistent-field
	    * @ejb:interface-method view-type="local"
	    *
	    * @jboss:column-name name="PL_CNVRSN_FCTR"
	    **/
	   public abstract double getPlConversionFactor();
	   /**
	    * @ejb:interface-method view-type="local"
	    **/
	   public abstract void setPlConversionFactor(double PL_CNVRSN_FCTR);
	   
	   /**
	    * @ejb:persistent-field
	    * @ejb:interface-method view-type="local"
	    *
	    * @jboss:column-name name="PL_TX_AMNT"
	    **/
	   public abstract double getPlTaxAmount();
	   /**
	    * @ejb:interface-method view-type="local"
	    **/
	   public abstract void setPlTaxAmount(double PL_TX_AMNT);	   
	   
	   /**
	    * @ejb:persistent-field
	    * @ejb:interface-method view-type="local"
	    *
	    * @jboss:column-name name="PL_PL_CODE"
	    **/
	   public abstract Integer getPlPlCode();
	   /**
	    * @ejb:interface-method view-type="local"
	    **/
	   public abstract void setPlPlCode(Integer PL_PL_CODE);

	    /**
	     * @ejb:persistent-field
	     * @ejb:interface-method view-type="local"
	     *
	     * @jboss:column-name name="PL_DSCNT_1"
	     **/
	    
	   public abstract double getPlDiscount1();
	   /**
	    * @ejb:interface-method view-type="local"
	    **/
	   public abstract void setPlDiscount1(double PL_DSCNT_1);
	   
	   /**
	    * @ejb:persistent-field
	    * @ejb:interface-method view-type="local"
	    *
	    * @jboss:column-name name="PL_DSCNT_2"
	    **/
	   public abstract double getPlDiscount2();
	   /**
	    * @ejb:interface-method view-type="local"
	    **/
	   public abstract void setPlDiscount2(double PL_DSCNT_2);
	   
	   /**
	    * @ejb:persistent-field
	    * @ejb:interface-method view-type="local"
	    *
	    * @jboss:column-name name="PL_DSCNT_3"
	    **/
	   public abstract double getPlDiscount3();
	   /**
	    * @ejb:interface-method view-type="local"
	    **/
	   public abstract void setPlDiscount3(double PL_DSCNT_3);
	   
	   /**
	    * @ejb:persistent-field
	    * @ejb:interface-method view-type="local"
	    *
	    * @jboss:column-name name="PL_DSCNT_4"
	    **/
	   public abstract double getPlDiscount4();
	   /**
	    * @ejb:interface-method view-type="local"
	    **/
	   public abstract void setPlDiscount4(double PL_DSCNT_4);
	   

	   
	   /**
	    * @ejb:persistent-field
	    * @ejb:interface-method view-type="local"
	    *
	    * @jboss:column-name name="PL_TTL_DSCNT"
	    **/
	   public abstract double getPlTotalDiscount();
	   
	   /**
	    * @ejb:interface-method view-type="local"
	    **/
	   public abstract void setPlTotalDiscount(double PL_TTL_DSCNT);

	   
	   /**
	    * @ejb:persistent-field
	    * @ejb:interface-method view-type="local"
	    *
	    * @jboss:column-name name="PL_EXPRY_DT"
	    **/
	   public abstract String getPlMisc();
	   
	   /**
	    * @ejb:interface-method view-type="local"
	    **/
	   public abstract void setPlMisc(String PL_MISC);		   
	   
	   /**
	    * @ejb:persistent-field
	    * @ejb:interface-method view-type="local"
	    *
	    * @jboss:column-name name="PL_AD_CMPNY"
	    **/

	   public abstract Integer getPlAdCompany();
	   /**
	    * @ejb:interface-method view-type="local"
	    **/
	   public abstract void setPlAdCompany(Integer PL_AD_CMPNY);
	   
	   // Access methods for relationship fields

	   /**
	    * @ejb:interface-method view-type="local"
	    * @ejb:relation name="purchaseorder-purchaseorderlines"
	    *               role-name="purchaseorderline-has-one-purchaseorder"
	    *               cascade-delete="yes"
	    *
	    * @jboss:relation related-pk-field="poCode"
	    *                 fk-column="AP_PURCHASE_ORDER"
	    */
	   public abstract LocalApPurchaseOrder getApPurchaseOrder();
	   /**
	    * @ejb:interface-method view-type="local"
	    **/
	   public abstract void setApPurchaseOrder(LocalApPurchaseOrder apPurchaseOrder);
	   	   
	   
	   /**
	    * @ejb:interface-method view-type="local"
	    * @ejb:relation name="unitofmeasure-purchaseorderlines"
	    *               role-name="purchaseorderline-has-one-unitofmeasure"
	    *               cascade-delete="no"
	    *
	    * @jboss:relation related-pk-field="uomCode"
	    *                 fk-column="INV_UNIT_OF_MEASURE"
	    */
	   public abstract LocalInvUnitOfMeasure getInvUnitOfMeasure();
	   /**
	    * @ejb:interface-method view-type="local"
	    **/
	   public abstract void setInvUnitOfMeasure(LocalInvUnitOfMeasure invUnitOfMeasure);
	   
	   /**
	    * @ejb:interface-method view-type="local"
	    * @ejb:relation name="itemlocation-purchaseorderlines"
	    *               role-name="purchaseorderline-has-one-itemlocation"
	    *               cascade-delete="no"
	    *
	    * @jboss:relation related-pk-field="ilCode"
	    *                 fk-column="INV_ITEM_LOCATION"
	    */
	   public abstract LocalInvItemLocation getInvItemLocation();
	   /**
	    * @ejb:interface-method view-type="local"
	    **/
	   public abstract void setInvItemLocation(LocalInvItemLocation invItemLocation);
	   
	   /**
	    * @ejb:interface-method view-type="local"
	    * @ejb:relation name="purchaseorderline-costing"
	    *               role-name="purchaseorderline-has-one-costing"
	    */
	   public abstract LocalInvCosting getInvCosting();
	   /**
        * @ejb:interface-method view-type="local"
        **/
	   public abstract void setInvCosting(LocalInvCosting invCosting);
	   
	   /**
	    * @ejb:interface-method view-type="local"
	    * @ejb:relation name="voucher-purchaseorderlines"
	    *               role-name="purchaseorderline-has-one-voucher"
	    *               cascade-delete="no"
	    *
	    * @jboss:relation related-pk-field="vouCode"
	    *                 fk-column="AP_VOUCHER"
	    */
	   public abstract LocalApVoucher getApVoucher();
	   /**
        * @ejb:interface-method view-type="local"
        **/
	   public abstract void setApVoucher(LocalApVoucher apVoucher);
	   
	   /**
	    * @ejb:interface-method view-type="local"
	    * @ejb:relation name="purchaseorderline-tags"
	    *               role-name="purchaseorderline-has-many-tags"
	    *               
	    */
	   public abstract Collection getInvTags();
	   public abstract void setInvTags(Collection invTags);
	   
	   public abstract java.util.Date ejbSelectMaxPoDateByIiNameAndLocNameAndPoReceivingAndPoPostedAndSplSupplierCode(java.lang.String II_NM, java.lang.String LOC_NM, byte PO_RCVNG, byte PO_PSTD, java.lang.String SPL_PPLR_CD, java.lang.Integer PO_AD_BRNCH, java.lang.Integer PL_AD_CMPNY)
      	throws FinderException;
	   
	   public abstract java.lang.Integer ejbSelectMaxPlCodeByPoDateAndIiNameAndLocNameAndPoReceivingAndPoPostedAndSplSupplierCode(java.util.Date PO_DT, java.lang.String II_NM, java.lang.String LOC_NM, byte PO_RCVNG, byte PO_PSTD, java.lang.String SPL_SPPLR_CD, java.lang.Integer PO_AD_BRNCH, java.lang.Integer PL_AD_CMPNY)
     	throws FinderException;
	   
	   public abstract java.util.Date ejbSelectMaxPoDateByIiNameAndLocNameAndPoReceivingAndPoPosted(java.lang.String II_NM, java.lang.String LOC_NM, byte PO_RCVNG, byte PO_PSTD, java.lang.Integer PO_AD_BRNCH, java.lang.Integer PL_AD_CMPNY)
     	throws FinderException;
	   
	   public abstract java.lang.Integer ejbSelectMaxPlCodeByPoDateAndIiNameAndLocNameAndPoReceivingAndPoPosted(java.util.Date PO_DT, java.lang.String II_NM, java.lang.String LOC_NM, byte PO_RCVNG, byte PO_PSTD, java.lang.Integer PO_AD_BRNCH, java.lang.Integer PL_AD_CMPNY)
    	throws FinderException;
	   
	   public abstract LocalApPurchaseOrderLine ejbSelectByPlCode(Integer PL_CD, Integer PL_AD_CMPNY)
    	throws FinderException;
	   
	   public abstract java.util.Date ejbSelectMaxPoDateByIiNameAndLocNameAndPoReceivingAndPoPostedAndLessThanEqualPoDate(java.lang.String II_NM, java.lang.String LOC_NM, byte PO_RCVNG, byte PO_PSTD, java.util.Date PO_DT, java.lang.Integer PO_AD_BRNCH, java.lang.Integer PL_AD_CMPNY)
     	throws FinderException;
	   
	   /**
		* @jboss:dynamic-ql
		*/
		public abstract Collection ejbSelectGeneric(java.lang.String jbossQl, java.lang.Object[] args)
		   throws FinderException;	   
	  
	   // Business methods	   

		/**
		* @ejb:home-method view-type="local"
		*/
		public Collection ejbHomeGetPolByCriteria(java.lang.String jbossQl, java.lang.Object[] args)
		   throws FinderException {
		   	
		   return ejbSelectGeneric(jbossQl, args);
		   
		}	 
		
		/**
	      * @ejb:home-method view-type="local"
	      */
	     public LocalApPurchaseOrderLine ejbHomeGetByMaxPoDateAndMaxPlCodeAndIiNameAndLocNameAndPoReceivingAndPoPostedAndSplSpplrCode(java.lang.String II_NM, java.lang.String LOC_NM, byte PO_RCVNG, byte PO_PSTD, String SPL_SPPLR_CD, java.lang.Integer PO_AD_BRNCH, java.lang.Integer PL_AD_CMPNY)
	        throws FinderException {
	        
	     	try {	     	
	     	
	     	    Date poDate = ejbSelectMaxPoDateByIiNameAndLocNameAndPoReceivingAndPoPostedAndSplSupplierCode(II_NM, LOC_NM, PO_RCVNG, PO_PSTD, SPL_SPPLR_CD, PO_AD_BRNCH, PL_AD_CMPNY);	      	    
		        Integer cstLineNumber = ejbSelectMaxPlCodeByPoDateAndIiNameAndLocNameAndPoReceivingAndPoPostedAndSplSupplierCode(poDate, II_NM, LOC_NM, PO_RCVNG, PO_PSTD, SPL_SPPLR_CD, PO_AD_BRNCH, PL_AD_CMPNY);		        
		        return ejbSelectByPlCode(cstLineNumber, PL_AD_CMPNY);
		        
	     	} catch (NullPointerException ex) {

	     		throw new FinderException(ex.getMessage());
	     		
	     	}
	        
	     }
	     
	     
	     /**
	      * @ejb:home-method view-type="local"
	      */
	     public LocalApPurchaseOrderLine ejbHomeGetByMaxPoDateAndMaxPlCodeAndIiNameAndLocNameAndPoReceivingAndPoPosted(java.lang.String II_NM, java.lang.String LOC_NM, byte PO_RCVNG, byte PO_PSTD, java.lang.Integer PO_AD_BRNCH, java.lang.Integer PL_AD_CMPNY)
	        throws FinderException {
	        
	     	try {	     	
	     	
	     	    Date poDate = ejbSelectMaxPoDateByIiNameAndLocNameAndPoReceivingAndPoPosted(II_NM, LOC_NM, PO_RCVNG, PO_PSTD, PO_AD_BRNCH, PL_AD_CMPNY);	      	    
		        Integer cstLineNumber = ejbSelectMaxPlCodeByPoDateAndIiNameAndLocNameAndPoReceivingAndPoPosted(poDate, II_NM, LOC_NM, PO_RCVNG, PO_PSTD, PO_AD_BRNCH, PL_AD_CMPNY);		        
		        return ejbSelectByPlCode(cstLineNumber, PL_AD_CMPNY);
		        
	     	} catch (NullPointerException ex) {

	     		throw new FinderException(ex.getMessage());
	     		
	     	}
	        
	     }
	     
	     /**
	      * @ejb:home-method view-type="local"
	      */
	     public LocalApPurchaseOrderLine ejbHomeGetByMaxPoDateAndMaxPlCodeAndIiNameAndLocNameAndPoReceivingAndPoPostedAndLessThanEqualPoDate(java.lang.String II_NM, java.lang.String LOC_NM, byte PO_RCVNG, byte PO_PSTD, java.util.Date PO_DT, java.lang.Integer PO_AD_BRNCH, java.lang.Integer PL_AD_CMPNY)
	        throws FinderException {
	        
	     	try {	     	
	     	
	     	    Date poDate = ejbSelectMaxPoDateByIiNameAndLocNameAndPoReceivingAndPoPostedAndLessThanEqualPoDate(II_NM, LOC_NM, PO_RCVNG, PO_PSTD, PO_DT, PO_AD_BRNCH, PL_AD_CMPNY);	   
	     	    Integer cstLineNumber = ejbSelectMaxPlCodeByPoDateAndIiNameAndLocNameAndPoReceivingAndPoPosted(poDate, II_NM, LOC_NM, PO_RCVNG, PO_PSTD, PO_AD_BRNCH, PL_AD_CMPNY);		        
	     	    return ejbSelectByPlCode(cstLineNumber, PL_AD_CMPNY);
		        
	     	} catch (NullPointerException ex) {

	     		throw new FinderException(ex.getMessage());
	     		
	     	}
	        
	     }
	     
	   // EntityBean methods

	   /**
	    * @ejb:create-method view-type="local"
	    **/
	   public java.lang.Integer ejbCreate (java.lang.Integer PL_CODE, short PL_LN, double PL_QTY, double PL_UNT_CST,
	      double PL_AMNT, String PL_QC_NUM, Date PL_QC_EXPRY_DT, double PL_CNVRSN_FCTR, double PL_TX_AMNT, Integer PL_PL_CODE, double PL_DSCNT_1, double PL_DSCNT_2,
		  double PL_DSCNT_3, double PL_DSCNT_4, double TTL_PL_DSCNT, Integer PL_AD_CMPNY)
	      throws CreateException {

	      Debug.print("ApPurchaseOrderLineBean ejbCreate");
	      setPlCode(PL_CODE);
	      setPlLine(PL_LN);
	      setPlQuantity(PL_QTY);
	      setPlUnitCost(PL_UNT_CST);
	      setPlAmount(PL_AMNT);
	      setPlQcNumber(PL_QC_NUM);
	      setPlQcExpiryDate(PL_QC_EXPRY_DT);
	      setPlConversionFactor(PL_CNVRSN_FCTR);
	      setPlTaxAmount(PL_TX_AMNT);
	      setPlPlCode(PL_PL_CODE);
	      setPlDiscount1(PL_DSCNT_1);
	      setPlDiscount2(PL_DSCNT_2);
	      setPlDiscount3(PL_DSCNT_3);
	      setPlDiscount4(PL_DSCNT_4);
	      setPlTotalDiscount(TTL_PL_DSCNT);	   	      
	      setPlAdCompany(PL_AD_CMPNY);
	      
	      return null;
	   }

	   /**
	    * @ejb:create-method view-type="local"
	    **/
	   public java.lang.Integer ejbCreate (short PL_LN, double PL_QTY, double PL_UNT_CST,
		  double PL_AMNT, String PL_QC_NUM, Date PL_QC_EXPRY_DT, double PL_CNVRSN_FCTR, double PL_TX_AMNT, Integer PL_PL_CODE, double PL_DSCNT_1, double PL_DSCNT_2,
		  double PL_DSCNT_3, double PL_DSCNT_4, double TTL_PL_DSCNT, Integer PL_AD_CMPNY)
	      throws CreateException {

	      Debug.print("ApPurchaseOrderLineBean ejbCreate");
	      setPlLine(PL_LN);
	      setPlQuantity(PL_QTY);
	      setPlUnitCost(PL_UNT_CST);
	      setPlAmount(PL_AMNT);
	      setPlQcNumber(PL_QC_NUM);
	      setPlQcExpiryDate(PL_QC_EXPRY_DT);
	      setPlConversionFactor(PL_CNVRSN_FCTR);
	      setPlTaxAmount(PL_TX_AMNT);
	      setPlPlCode(PL_PL_CODE);
	      setPlDiscount1(PL_DSCNT_1);
	      setPlDiscount2(PL_DSCNT_2);
	      setPlDiscount3(PL_DSCNT_3);
	      setPlDiscount4(PL_DSCNT_4);
	      setPlTotalDiscount(TTL_PL_DSCNT);	   	      
	      setPlAdCompany(PL_AD_CMPNY);
	      	      
	      return null;
	   }

	   public void ejbPostCreate (java.lang.Integer PL_CODE, short PL_LN, double PL_QTY, double PL_UNT_CST,
		  double PL_AMNT, String PL_QC_NUM, Date PL_QC_EXPRY_DT, double PL_CNVRSN_FCTR, double PL_TX_AMNT, Integer PL_PL_CODE, double PL_DSCNT_1, double PL_DSCNT_2,
		  double PL_DSCNT_3, double PL_DSCNT_4, double TTL_PL_DSCNT, Integer PL_AD_CMPNY)
	      throws CreateException { }

	   public void ejbPostCreate (short PL_LN, double PL_QTY, double PL_UNT_CST,
		  double PL_AMNT, String PL_QC_NUM, Date PL_QC_EXPRY_DT, double PL_CNVRSN_FCTR, double PL_TX_AMNT, Integer PL_PL_CODE, double PL_DSCNT_1, double PL_DSCNT_2,
		  double PL_DSCNT_3, double PL_DSCNT_4, double TTL_PL_DSCNT, Integer PL_AD_CMPNY)
	      throws CreateException { }
	
}
