/*
 * com/ejb/ap/LocalApPurchaseOrderBean.java
 *
 * Created on April 17, 2006 04:50 PM
 */

package com.ejb.ap;

import java.util.Collection;
import java.util.Date;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;

import com.util.AbstractEntityBean;
import com.util.Debug;

/**
 *
 * @author  Aliza D.J. Anos
 *
 * @ejb:bean name="ApPurchaseRequisitionEJB"
 *           display-name="Purchase Requisition Entity"
 *           type="CMP"
 *           cmp-version="2.x"
 *           view-type="local"
 *           local-jndi-name="omega-ejb/ApPurchaseRequisitionEJB"
 *           schema="ApPurchaseRequisition"
 *           primkey-field="prCode"
 *
 * @ejb:pk class="java.lang.Integer"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="apuser"
 *                        role-link="apuserlink"
 *
 * @ejb:permission role-name="apuser"
 *
 * @ejb:interface local-class="com.ejb.ap.LocalApPurchaseRequisition"
 *                local-extends="javax.ejb.EJBLocalObject"
 *
 * @ejb:home local-class="com.ejb.ap.LocalApPurchaseRequisitionHome"
 *           local-extends="javax.ejb.EJBLocalHome"
 *
 * @ejb:finder signature="LocalApPurchaseRequisition findByPrNumberAndBrCode(java.lang.String PR_NMBR, java.lang.Integer PR_AD_BRNCH, java.lang.Integer PR_AD_CMPNY)"
 *             query="SELECT OBJECT(pr) FROM ApPurchaseRequisition pr  WHERE pr.prNumber = ?1 AND pr.prAdBranch = ?2 AND pr.prAdCompany = ?3"
 *
 * @ejb:finder signature="Collection findDraftPrByBrCode(java.lang.Integer PR_AD_BRNCH, java.lang.Integer PR_AD_CMPNY)"
 * 				query="SELECT OBJECT(pr) FROM ApPurchaseRequisition pr WHERE pr.prApprovalStatus IS NULL AND pr.prReasonForRejection IS NULL AND pr.prVoid = 0 AND pr.prAdBranch = ?1 AND pr.prAdCompany = ?2"
 *
 * @ejb:finder signature="Collection findPostedAndCanvassUnPostedPr(java.lang.Integer PR_AD_CMPNY)"
 * 				query="SELECT OBJECT(pr) FROM ApPurchaseRequisition pr WHERE pr.prPosted = 1 AND pr.prCanvassApprovalStatus IS NULL AND pr.prCanvassReasonForRejection IS NULL AND pr.prCanvassPosted = 0 AND pr.prVoid = 0 AND pr.prAdCompany = ?1"
 *
 * @ejb:finder signature="Collection findRejectedPrByBrCodeAndPrCreatedBy(java.lang.Integer PR_AD_BRNCH, java.lang.String PR_CRTD_BY, java.lang.Integer PR_AD_CMPNY)"
 * 				query="SELECT OBJECT(pr) FROM ApPurchaseRequisition pr WHERE pr.prApprovalStatus IS NULL AND pr.prReasonForRejection IS NOT NULL AND pr.prAdBranch = ?1 AND pr.prCreatedBy=?2 AND pr.prAdCompany = ?3"
 *
 * @ejb:finder signature="Collection findRejectedCanvassByBrCodeAndPrLastModifiedBy(java.lang.String PR_LST_MDFD_BY, java.lang.Integer PR_AD_CMPNY)"
 * 				query="SELECT OBJECT(pr) FROM ApPurchaseRequisition pr WHERE pr.prCanvassApprovalStatus IS NULL AND pr.prCanvassReasonForRejection IS NOT NULL AND pr.prLastModifiedBy=?1 AND pr.prAdCompany = ?2"
 *
 * @ejb:finder signature="Collection findUnPostedGeneratedPr(java.lang.Integer PR_AD_CMPNY)"
 * 				query="SELECT OBJECT(pr) FROM ApPurchaseRequisition pr WHERE pr.prPosted = 1 AND pr.prCanvassPosted = 1 AND pr.prGenerated = 0 AND pr.prVoid = 0 AND pr.prAdCompany = ?1"
 *
 * @ejb:finder signature="Collection findPrToGenerateByAdUsrCodeAndDate(java.lang.Integer USR_CODE, java.util.Date DT, java.lang.Integer PR_AD_BRNCH, java.lang.Integer PR_AD_CMPNY)"
 *             query="SELECT OBJECT(pr) FROM ApPurchaseRequisition pr WHERE pr.prPosted = 1 AND (pr.prAdUserName1=?1 ) AND pr.prNextRunDate <= ?2 AND pr.prAdBranch = ?3 AND pr.prAdCompany = ?4"
 *
 * @jboss:query signature="Collection findPrToGenerateByAdUsrCodeAndDate(java.lang.Integer USR_CODE, java.util.Date DT, java.lang.Integer PR_AD_BRNCH, java.lang.Integer PR_AD_CMPNY)"
 *             query="SELECT OBJECT(pr) FROM ApPurchaseRequisition pr WHERE pr.prPosted = 1 AND (pr.prAdUserName1=?1 ) AND pr.prNextRunDate <= ?2 AND pr.prAdBranch = ?3 AND pr.prAdCompany = ?4"
 *
 * @ejb:finder signature="Collection findByUserCode1(java.lang.Integer USR_CODE, java.lang.Integer PR_AD_CMPNY)"
 *             query="SELECT OBJECT(pr) FROM ApPurchaseRequisition pr WHERE pr.prAdUserName1 = ?1 AND pr.prAdCompany = ?2"
 *
 * @ejb:select signature="Collection ejbSelectGeneric(java.lang.String jbossQl, java.lang.Object[] args)"
 *             result-type-mapping="Local"
 *             method-intf="LocalHome"
 *             query="SELECT OBJECT(pr) FROM ApPurchaseRequisition pr"
 *
 * @jboss:query signature="Collection ejbSelectGeneric(java.lang.String jbossQl, java.lang.Object[] args)"
 *                         dynamic="true"
 *
 * @ejb:value-object match="*"
 *             name="ApPurchaseRequisition"
 *
 * @jboss:persistence table-name="AP_PRCHS_RQSTN"
 *
 * @jboss:entity-command name="mysql-get-generated-keys"
 */

public abstract class ApPurchaseRequisitionBean extends AbstractEntityBean {

	//	 PERSISTENT METHODS

	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 * @ejb:pk-field
	 *
	 * @jboss:column-name name="PR_CODE"
	 *
	 * @jboss:persistence auto-increment="true"
	 **/
	public abstract Integer getPrCode();
	public abstract void setPrCode(Integer PR_CODE);

	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="PR_DESC"
	 **/
	public abstract String getPrDescription();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setPrDescription(String PR_DESC);

	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="PR_NMBR"
	 **/
	public abstract String getPrNumber();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setPrNumber(String PR_NMBR);

	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="PR_DT"
	 **/
	public abstract Date getPrDate();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setPrDate(Date PR_DT);

	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="PR_DLVRY_PRD"
	 **/
	public abstract Date getPrDeliveryPeriod();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setPrDeliveryPeriod(Date PR_DLVRY_PRD);

	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="PR_RFRNC_NMBR"
	 **/
	public abstract String getPrReferenceNumber();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setPrReferenceNumber(String PR_RFRNC_NMBR);

	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="PR_CNVRSN_DT"
	 **/
	public abstract Date getPrConversionDate();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setPrConversionDate(Date PR_CNVRSN_DT);

	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="PR_CNVRSN_RT"
	 **/
	public abstract double getPrConversionRate();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setPrConversionRate(double PR_CNVRSN_RT);

	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="PR_APPRVL_STATUS"
	 **/
	public abstract String getPrApprovalStatus();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setPrApprovalStatus(String PR_APPRVL_STATUS);



	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="PR_CNVSS_APPRVL_STATUS"
	 **/
	public abstract String getPrCanvassApprovalStatus();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setPrCanvassApprovalStatus(String PR_CNVSS_APPRVL_STATUS);



	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="PR_PSTD"
	 **/
	public abstract byte getPrPosted();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setPrPosted(byte PR_PSTD);


	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="PR_GNRTD"
	 **/
	public abstract byte getPrGenerated();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setPrGenerated(byte PR_GNRTD);



	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="PR_CNVSS_PSTD"
	 **/
	public abstract byte getPrCanvassPosted();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setPrCanvassPosted(byte PR_CNVSS_PSTD);



	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="PR_VD"
	 **/
	public abstract byte getPrVoid();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setPrVoid(byte PR_VD);

	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="PR_RSN_FR_RJCTN"
	 **/
	public abstract String getPrReasonForRejection();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setPrReasonForRejection(String PR_RSN_FR_RJCTN);

	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="PR_CNVSS_RSN_FR_RJCTN"
	 **/
	public abstract String getPrCanvassReasonForRejection();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setPrCanvassReasonForRejection(String PR_CNVSS_RSN_FR_RJCTN);


	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="PR_CRTD_BY"
	 **/
	public abstract String getPrCreatedBy();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setPrCreatedBy(String PR_CRTD_BY);

	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="PR_DT_CRTD"
	 **/
	public abstract Date getPrDateCreated();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setPrDateCreated(Date PR_DT_CRTD);

	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="PR_LST_MDFD_BY"
	 **/
	public abstract String getPrLastModifiedBy();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setPrLastModifiedBy(String PR_LST_MDFD_BY);

	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="PR_DT_LST_MDFD"
	 **/
	public abstract Date getPrDateLastModified();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setPrDateLastModified(Date PR_DT_LST_MDFD);


	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="PR_APPRVD_RJCTD_BY"
	 **/
	public abstract String getPrApprovedRejectedBy();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setPrApprovedRejectedBy(String PR_APPRVD_RJCTD_BY);



	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="PR_CNVSS_APPRVD_RJCTD_BY"
	 **/
	public abstract String getPrCanvassApprovedRejectedBy();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setPrCanvassApprovedRejectedBy(String PR_CNVSS_APPRVD_RJCTD_BY);







	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="PR_DT_APPRVD_RJCTD"
	 **/
	public abstract Date getPrDateApprovedRejected();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setPrDateApprovedRejected(Date PR_DT_APPRVD_RJCTD);




	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="PR_CNVSS_DT_APPRVD_RJCTD"
	 **/
	public abstract Date getPrCanvassDateApprovedRejected();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setPrCanvassDateApprovedRejected(Date PR_CNVSS_DT_APPRVD_RJCTD);






	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="PR_PSTD_BY"
	 **/
	public abstract String getPrPostedBy();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setPrPostedBy(String PR_PSTD_BY);



	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="PR_CNVSS_PSTD_BY"
	 **/
	public abstract String getPrCanvassPostedBy();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setPrCanvassPostedBy(String PR_CNVSS_PSTD_BY);







	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="PR_DT_PSTD"
	 **/
	public abstract Date getPrDatePosted();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setPrDatePosted(Date PR_DT_PSTD);


	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="PR_CNVSS_DT_PSTD"
	 **/
	public abstract Date getPrCanvassDatePosted();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setPrCanvassDatePosted(Date PR_CNVSS_DT_PSTD);




	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="PR_TG_STATUS"
	 **/
	public abstract String getPrTagStatus();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setPrTagStatus(String PR_TG_STATUS);


	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="PR_TYPE"
	 **/
	public abstract String getPrType();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setPrType(String PR_TYPE);

	
	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="PR_AD_USR_NM1"
	 **/
	 public abstract java.lang.Integer getPrAdUserName1();
	 /**
	  * @ejb:interface-method view-type="local"
	  **/
	 public abstract void setPrAdUserName1(java.lang.Integer PR_AD_USR_NM1);



	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="PR_SCHDL"
	 **/
	 public abstract String getPrSchedule();
	 /**
	  * @ejb:interface-method view-type="local"
	  **/
	 public abstract void setPrSchedule(String PR_SCHDL);

	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="PR_NXT_RN_DT"
	 **/
	 public abstract Date getPrNextRunDate();
	 /**
	  * @ejb:interface-method view-type="local"
	  **/
	 public abstract void setPrNextRunDate(Date PR_NXT_RN_DT);

	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="PR_LST_RN_DT"
	 **/
	 public abstract Date getPrLastRunDate();
	 /**
	  * @ejb:interface-method view-type="local"
	  **/
	 public abstract void setPrLastRunDate(Date PR_LST_RN_DT);





	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="PR_AD_BRNCH"
	 **/
	public abstract Integer getPrAdBranch();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setPrAdBranch(Integer PR_AD_BRNCH);

	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="PR_AD_CMPNY"
	 **/
	public abstract Integer getPrAdCompany();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setPrAdCompany(Integer PR_AD_CMPNY);


	 // RELATIONSHIP METHODS


	/**
     * @ejb:interface-method view-type="local"
     * @ejb:relation name="functionalcurrency-purchaserequisitions"
     *               role-name="purchaserequisition-has-one-functionalcurrency"
     *
     * @jboss:relation related-pk-field="fcCode"
     *                 fk-column="GL_FUNCTIONAL_CURRENCY"
     */
    public abstract com.ejb.gl.LocalGlFunctionalCurrency getGlFunctionalCurrency();
    /**
	 * @ejb:interface-method view-type="local"
	 */
    public abstract void setGlFunctionalCurrency(com.ejb.gl.LocalGlFunctionalCurrency glFunctionalCurrency);


    /**
     * @ejb:interface-method view-type="local"
     * @ejb:relation name="aptaxcode-purchaserequisitions"
     *               role-name="purchaserequisition-has-one-aptaxcode"
     *
     * @jboss:relation related-pk-field="tcCode"
     *                 fk-column="AP_TAX_CODE"
     */
    public abstract LocalApTaxCode getApTaxCode();
    /**
	 * @ejb:interface-method view-type="local"
	 */
    public abstract void setApTaxCode(LocalApTaxCode apTaxCode);

    /**
     * @ejb:interface-method view-type="local"
     * @ejb:relation name="purchaserequisition-purchaserequisitionlines"
     *               role-name="purchaserequisition-has-many-purchaserequisitionlines"
     *
     */
    public abstract Collection getApPurchaseRequisitionLines();
    public abstract void setApPurchaseRequisitionLines(Collection apPurchaseRequisitionLines);

    /**
	* @jboss:dynamic-ql
	*/
	public abstract Collection ejbSelectGeneric(java.lang.String jbossQl, java.lang.Object[] args)
	   throws FinderException;


//	 BUSINESS METHODS

	/**
	* @ejb:home-method view-type="local"
	*/
	public Collection ejbHomeGetPrByCriteria(java.lang.String jbossQl, java.lang.Object[] args)
	   throws FinderException {

	   return ejbSelectGeneric(jbossQl, args);

	}

	/**
	  * @ejb:interface-method view-type="local"
	  **/
	public void addApPurchaseRequisitionLine(LocalApPurchaseRequisitionLine apPurchaseRequisitionLine) {

		Debug.print("ApPurchaseRequisitionBean addApPurchaseRequisitionLine");

		try {

			Collection apPurchaseRequisitionLines = getApPurchaseRequisitionLines();
			apPurchaseRequisitionLines.add(apPurchaseRequisitionLine);

		} catch (Exception ex) {

			throw new EJBException(ex.getMessage());

		}

	}

    /**
     * @ejb:interface-method view-type="local"
     **/
	 public void dropApPurchaseRequisitionLine(
			 LocalApPurchaseRequisitionLine apPurchaseRequisitionLine) {

	 	Debug.print("ApPurchaseRequisitionBean dropApPurchaseRequisitionLine");

	 	try {

	 		Collection apPurchaseRequisitionLines = getApPurchaseRequisitionLines();
	 		apPurchaseRequisitionLines.remove(apPurchaseRequisitionLine);

	 	} catch (Exception ex) {

	 		throw new EJBException(ex.getMessage());

	 	}

	 }

//	 ENTITY METHODS

	     /**
	      * @ejb:create-method view-type="local"
	      **/
	     public Integer ejbCreate(Integer PR_CODE, String PR_DESC, String PR_NMBR, Date PR_DT, Date PR_DLVRY_PRD, String PR_RFRNC_NMBR,
	         Date PR_CNVRSN_DT, double PR_CNVRSN_RT, String PR_APPRVL_STATUS, byte PR_PSTD, byte PR_GNRTD, String PR_CNVSS_APPRVL_STATUS, byte PR_CNVSS_PSTD, byte PR_VD,
			 String PR_RSN_FR_RJCTN, String PR_CNVSS_RSN_FR_RJCTN, String PR_CRTD_BY, Date PR_DT_CRTD, String PR_LST_MDFD_BY, Date PR_DT_LST_MDFD,
			 String PR_APPRVD_RJCTD_BY, Date PR_DT_APPRVD_RJCTD, String PR_PSTD_BY, Date PR_DT_PSTD,
			 String PR_CNVSS_APPRVD_RJCTD_BY, Date PR_CNVSS_DT_APPRVD_RJCTD, String PR_CNVSS_PSTD_BY, Date PR_CNVSS_DT_PSTD,
			 String PR_TG_STATUS, String PR_TYPE, 
			 java.lang.Integer PR_AD_USR_NM1, String PR_SCHDL, Date PR_NXT_RN_DT, Date PR_LST_RN_DT,
			 Integer PR_AD_BRNCH, Integer PR_AD_CMPNY)
	         throws CreateException {

	         Debug.print("ApPurchaseRequisitionBean ejbCreate");

	         setPrCode(PR_CODE);
	         setPrDescription(PR_DESC);
	         setPrNumber(PR_NMBR);
	         setPrDate(PR_DT);
	         setPrDeliveryPeriod(PR_DLVRY_PRD);
	         setPrReferenceNumber(PR_RFRNC_NMBR);
	         setPrConversionDate(PR_CNVRSN_DT);
	         setPrConversionRate(PR_CNVRSN_RT);
	         setPrApprovalStatus(PR_APPRVL_STATUS);
	         setPrPosted(PR_PSTD);
	         setPrGenerated(PR_GNRTD);
	         setPrCanvassApprovalStatus(PR_CNVSS_APPRVL_STATUS);
	         setPrCanvassPosted(PR_CNVSS_PSTD);
	         setPrVoid(PR_VD);
	         setPrReasonForRejection(PR_RSN_FR_RJCTN);
	         setPrCanvassReasonForRejection(PR_CNVSS_RSN_FR_RJCTN);
	         setPrCreatedBy(PR_CRTD_BY);
	         setPrDateCreated(PR_DT_CRTD);
	         setPrLastModifiedBy(PR_LST_MDFD_BY);
	         setPrDateLastModified(PR_DT_LST_MDFD);
	         setPrApprovedRejectedBy(PR_APPRVD_RJCTD_BY);
	         setPrDateApprovedRejected(PR_DT_APPRVD_RJCTD);
	         setPrPostedBy(PR_PSTD_BY);
	         setPrDatePosted(PR_DT_PSTD);
	         setPrCanvassApprovedRejectedBy(PR_CNVSS_APPRVD_RJCTD_BY);
	         setPrCanvassDateApprovedRejected(PR_CNVSS_DT_APPRVD_RJCTD);
	         setPrCanvassPostedBy(PR_CNVSS_PSTD_BY);
	         setPrCanvassDatePosted(PR_CNVSS_DT_PSTD);
	         setPrTagStatus(PR_TG_STATUS);
	         setPrType(PR_TYPE);
	         setPrAdUserName1(PR_AD_USR_NM1);
	 		 setPrSchedule(PR_SCHDL);
	 		 setPrNextRunDate(PR_NXT_RN_DT);
	 		 setPrLastRunDate(PR_LST_RN_DT);
	         setPrAdBranch(PR_AD_BRNCH);
	         setPrAdCompany(PR_AD_CMPNY);

	         return null;

	     }

	     /**
	      * @ejb:create-method view-type="local"
	      **/
	     public Integer ejbCreate(String PR_DESC, String PR_NMBR, Date PR_DT, Date PR_DLVRY_PRD, String PR_RFRNC_NMBR,
	         Date PR_CNVRSN_DT, double PR_CNVRSN_RT, String PR_APPRVL_STATUS, byte PR_PSTD, byte PR_GNRTD, String PR_CNVSS_APPRVL_STATUS, byte PR_CNVSS_PSTD, byte PR_VD,
			 String PR_RSN_FR_RJCTN, String PR_CNVSS_RSN_FR_RJCTN, String PR_CRTD_BY, Date PR_DT_CRTD, String PR_LST_MDFD_BY, Date PR_DT_LST_MDFD,
			 String PR_APPRVD_RJCTD_BY, Date PR_DT_APPRVD_RJCTD, String PR_PSTD_BY, Date PR_DT_PSTD,
			 String PR_CNVSS_APPRVD_RJCTD_BY, Date PR_CNVSS_DT_APPRVD_RJCTD, String PR_CNVSS_PSTD_BY, Date PR_CNVSS_DT_PSTD,
			 String PR_TG_STATUS, String PR_TYPE, 
			 java.lang.Integer PR_AD_USR_NM1, String PR_SCHDL, Date PR_NXT_RN_DT, Date PR_LST_RN_DT,
			 Integer PR_AD_BRNCH, Integer PR_AD_CMPNY)
	         throws CreateException {

	         Debug.print("ApPurchaseRequisitionBean ejbCreate");

	         setPrDescription(PR_DESC);
	         setPrNumber(PR_NMBR);
	         setPrDate(PR_DT);
	         setPrDeliveryPeriod(PR_DLVRY_PRD);
	         setPrReferenceNumber(PR_RFRNC_NMBR);
	         setPrConversionDate(PR_CNVRSN_DT);
	         setPrConversionRate(PR_CNVRSN_RT);
	         setPrApprovalStatus(PR_APPRVL_STATUS);
	         setPrPosted(PR_PSTD);
	         setPrGenerated(PR_GNRTD);
	         setPrCanvassApprovalStatus(PR_CNVSS_APPRVL_STATUS);
	         setPrCanvassPosted(PR_CNVSS_PSTD);
	         setPrVoid(PR_VD);
	         setPrReasonForRejection(PR_RSN_FR_RJCTN);
	         setPrCanvassReasonForRejection(PR_CNVSS_RSN_FR_RJCTN);
	         setPrCreatedBy(PR_CRTD_BY);
	         setPrDateCreated(PR_DT_CRTD);
	         setPrLastModifiedBy(PR_LST_MDFD_BY);
	         setPrDateLastModified(PR_DT_LST_MDFD);
	         setPrApprovedRejectedBy(PR_APPRVD_RJCTD_BY);
	         setPrDateApprovedRejected(PR_DT_APPRVD_RJCTD);
	         setPrPostedBy(PR_PSTD_BY);
	         setPrDatePosted(PR_DT_PSTD);
	         setPrCanvassApprovedRejectedBy(PR_CNVSS_APPRVD_RJCTD_BY);
	         setPrCanvassDateApprovedRejected(PR_CNVSS_DT_APPRVD_RJCTD);
	         setPrCanvassPostedBy(PR_CNVSS_PSTD_BY);
	         setPrCanvassDatePosted(PR_CNVSS_DT_PSTD);
	         setPrTagStatus(PR_TG_STATUS);
	         setPrType(PR_TYPE);
	         setPrAdUserName1(PR_AD_USR_NM1);
	 		 setPrSchedule(PR_SCHDL);
	 		 setPrNextRunDate(PR_NXT_RN_DT);
	 		 setPrLastRunDate(PR_LST_RN_DT);
	         setPrAdBranch(PR_AD_BRNCH);
	         setPrAdCompany(PR_AD_CMPNY);

	         return null;

	     }

	     public void ejbPostCreate(Integer PR_CODE, String PR_DESC, String PR_NMBR, Date PR_DT, Date PR_DLVRY_PRD, String PR_RFRNC_NMBR,
	     		Date PR_CNVRSN_DT, double PR_CNVRSN_RT, String PR_APPRVL_STATUS, byte PR_PSTD, byte PR_GNRTD, String PR_CNVSS_APPRVL_STATUS, byte PR_CNVSS_PSTD, byte PR_VD,
				String PR_RSN_FR_RJCTN, String PR_CNVSS_RSN_FR_RJCTN, String PR_CRTD_BY, Date PR_DT_CRTD, String PR_LST_MDFD_BY, Date PR_DT_LST_MDFD,
				String PR_APPRVD_RJCTD_BY, Date PR_DT_APPRVD_RJCTD, String PR_PSTD_BY, Date PR_DT_PSTD,
				String PR_CNVSS_APPRVD_RJCTD_BY, Date PR_CNVSS_DT_APPRVD_RJCTD, String PR_CNVSS_PSTD_BY, Date PR_CNVSS_DT_PSTD,
				String PR_TG_STATUS, String PR_TYPE, 
				java.lang.Integer PR_AD_USR_NM1, String PR_SCHDL, Date PR_NXT_RN_DT, Date PR_LST_RN_DT, Integer PR_AD_BRNCH, Integer PR_AD_CMPNY)
	     		throws CreateException { }

	     public void ejbPostCreate(String PR_DESC, String PR_NMBR, Date PR_DT, Date PR_DLVRY_PRD, String PR_RFRNC_NMBR,
	     		Date PR_CNVRSN_DT, double PR_CNVRSN_RT, String PR_APPRVL_STATUS, byte PR_PSTD, byte PR_GNRTD, String PR_CNVSS_APPRVL_STATUS, byte PR_CNVSS_PSTD, byte PR_VD,
				String PR_RSN_FR_RJCTN, String PR_CNVSS_RSN_FR_RJCTN, String PR_CRTD_BY, Date PR_DT_CRTD, String PR_LST_MDFD_BY, Date PR_DT_LST_MDFD,
				String PR_APPRVD_RJCTD_BY, Date PR_DT_APPRVD_RJCTD, String PR_PSTD_BY, Date PR_DT_PSTD,
				String PR_CNVSS_APPRVD_RJCTD_BY, Date PR_CNVSS_DT_APPRVD_RJCTD, String PR_CNVSS_PSTD_BY, Date PR_CNVSS_DT_PSTD,
				String PR_TG_STATUS, String PR_TYPE, 
				java.lang.Integer PR_AD_USR_NM1, String PR_SCHDL, Date PR_NXT_RN_DT, Date PR_LST_RN_DT, Integer PR_AD_BRNCH, Integer PR_AD_CMPNY)
	     		throws CreateException { }

}
