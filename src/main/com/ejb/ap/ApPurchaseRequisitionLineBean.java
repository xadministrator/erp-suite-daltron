/*
 * com/ejb/ap/LocalApPurchaseOrderLineBean.java
 *
 * Created on April 17, 2005 05:15 PM
 */

package com.ejb.ap;

import java.util.Collection;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;

import com.ejb.inv.LocalInvItemLocation;
import com.ejb.inv.LocalInvUnitOfMeasure;
import com.util.AbstractEntityBean;
import com.util.Debug;

/**
 * 
 * @author  Aliza D.J. Anos
 * 
 * @ejb:bean name="ApPurchaseRequisitionLineEJB"
 *           display-name="Purchase Requisition Line Entity"
 *           type="CMP"
 *           cmp-version="2.x"
 *           view-type="local"
 *           local-jndi-name="omega-ejb/ApPurchaseRequisitionLineEJB"
 *           schema="ApPurchaseRequisitionLine"
 *           primkey-field="prlCode"
 *       
 * @ejb:pk class="java.lang.Integer"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="apuser"
 *                        role-link="apuserlink"
 *
 * @ejb:permission role-name="apuser"
 *
 * @ejb:interface local-class="com.ejb.ap.LocalApPurchaseRequisitionLine"
 *                local-extends="javax.ejb.EJBLocalObject"
 *
 * @ejb:home local-class="com.ejb.ap.LocalApPurchaseRequisitionLineHome"
 *           local-extends="javax.ejb.EJBLocalHome"
 * 
 * @ejb:finder signature="LocalApPurchaseRequisitionLine findByPrCodeAndPrlLineAndBrCode(java.lang.Integer PR_CODE, short PRL_LN, java.lang.Integer PR_AD_BRNCH, java.lang.Integer PRL_AD_CMPNY)"
 *             query="SELECT OBJECT(prl) FROM ApPurchaseRequisitionLine prl  WHERE prl.apPurchaseRequisition.prCode=?1 AND prl.prlLine=?2 AND prl.apPurchaseRequisition.prAdBranch = ?3 AND prl.apPurchaseRequisition.prAdCompany = ?4"
 * 
 * @ejb:finder signature="LocalApPurchaseRequisitionLine findByPrCodeAndBrCode(java.lang.Integer PR_CODE, java.lang.Integer PR_AD_BRNCH, java.lang.Integer PRL_AD_CMPNY)"
 *             query="SELECT OBJECT(prl) FROM ApPurchaseRequisitionLine prl  WHERE prl.apPurchaseRequisition.prCode=?1 AND prl.apPurchaseRequisition.prAdBranch = ?2 AND prl.apPurchaseRequisition.prAdCompany = ?3"             
 *             
 * @ejb:finder signature="LocalApPurchaseRequisitionLine findByPrCodeAndPrlIiNameAndPrlIiLoc(java.lang.Integer PR_CODE, java.lang.String II_NM, java.lang.String LOC_NM, java.lang.Integer PRL_AD_CMPNY)"
 *             query="SELECT OBJECT(prl) FROM ApPurchaseRequisitionLine prl  WHERE prl.apPurchaseRequisition.prCode=?1 AND prl.invItemLocation.invItem.iiName=?2 AND prl.invItemLocation.invLocation.locName = ?3 AND prl.apPurchaseRequisition.prAdCompany = ?4"
 * 
 * @jboss:query signature="Collection ejbSelectGeneric(java.lang.String jbossQl, java.lang.Object[] args)"
 *                         dynamic="true"
 *
 * @ejb:value-object match="*"
 *             name="ApPurchaseRequisitionLine"
 * 
 * @jboss:persistence table-name="AP_PRCHS_RQSTN_LN"
 *
 * @jboss:entity-command name="mysql-get-generated-keys"
 *
 */

public abstract class ApPurchaseRequisitionLineBean extends AbstractEntityBean {
	
	//	 Access methods for persistent fields
	
	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 * @ejb:pk-field
	 *
	 * @jboss:column-name name="PRL_CODE"
	 *
	 * @jboss:persistence auto-increment="true"
	 **/
	public abstract java.lang.Integer getPrlCode();
	public abstract void setPrlCode(java.lang.Integer PRL_CODE);
	
	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="PRL_LN"
	 **/
	public abstract short getPrlLine();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setPrlLine(short PRL_LN);
	
	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="PRL_QTY"
	 **/
	public abstract double getPrlQuantity();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setPrlQuantity(double PRL_QTY);
	
	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="PRL_AMNT"
	 **/
	public abstract double getPrlAmount();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setPrlAmount(double PRL_AMNT);
	
	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="PRL_AD_CMPNY"
	 **/
	public abstract Integer getPrlAdCompany();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setPrlAdCompany(Integer PRL_AD_CMPNY);
	
	
	//	 Access methods for relationship fields
	
	/**
	 * @ejb:interface-method view-type="local"
	 * @ejb:relation name="purchaserequisition-purchaserequisitionlines"
	 *               role-name="purchaserequisitionline-has-one-purchaserequisition"
	 *               cascade-delete="yes"
	 *
	 * @jboss:relation related-pk-field="prCode"
	 *                 fk-column="AP_PURCHASE_REQUISITION"
	 */
	public abstract LocalApPurchaseRequisition getApPurchaseRequisition();
	public abstract void setApPurchaseRequisition(LocalApPurchaseRequisition apPurchaseRequisition);
	
	/**
	 * @ejb:interface-method view-type="local"
	 * @ejb:relation name="itemlocation-purchaserequisitionlines"
	 *               role-name="purchaserequisitionline-has-one-itemlocation"
	 *               cascade-delete="no"
	 *
	 * @jboss:relation related-pk-field="ilCode"
	 *                 fk-column="INV_ITEM_LOCATION"
	 */
	public abstract LocalInvItemLocation getInvItemLocation();
	public abstract void setInvItemLocation(LocalInvItemLocation invItemLocation);
	
	/**
	 * @ejb:interface-method view-type="local"
	 * @ejb:relation name="unitofmeasure-purchaserequisitionlines"
	 *               role-name="purchaserequisitionline-has-one-unitofmeasure"
	 *               cascade-delete="no"
	 *
	 * @jboss:relation related-pk-field="uomCode"
	 *                 fk-column="INV_UNIT_OF_MEASURE"
	 */
	public abstract LocalInvUnitOfMeasure getInvUnitOfMeasure();
	public abstract void setInvUnitOfMeasure(LocalInvUnitOfMeasure invUnitOfMeasure);
	
	/**
     * @ejb:interface-method view-type="local"
     * @ejb:relation name="purchaserequisitionline-canvasses"
     *               role-name="purchaserequisitionline-has-many-canvasses"
     * 
     */
    public abstract Collection getApCanvasses();
    public abstract void setApCanvasses(Collection apCanvasses);
	
    /**
     * @ejb:interface-method view-type="local"
     * @ejb:relation name="purchaserequisitionline-tags"
     *               role-name="purchaserequisitionline-has-many-tags"
     *               
     */
    public abstract Collection getInvTags();
    public abstract void setInvTags(Collection invTags);
	   
	/**
	 * @jboss:dynamic-ql
	 */
	public abstract Collection ejbSelectGeneric(java.lang.String jbossQl, java.lang.Object[] args)
	throws FinderException;	
	
	//		 Business methods	   
	
	/**
	 * @ejb:home-method view-type="local"
	 */
	public Collection ejbHomeGetPrlByCriteria(java.lang.String jbossQl, java.lang.Object[] args)
	throws FinderException {
		
		return ejbSelectGeneric(jbossQl, args);
		
	}	   
	
	/**
	  * @ejb:interface-method view-type="local"
	  **/
	public void addApCanvass(LocalApCanvass apCanvass) {
		
		Debug.print("ApPurchaseRequisitionLineBean addApCanvass");
		
		try {
			
			Collection apCanvasses = getApCanvasses();
			apCanvasses.add(apCanvass);
			
		} catch (Exception ex) {
			
			throw new EJBException(ex.getMessage());
			
		}
		
	}
	
   /**
    * @ejb:interface-method view-type="local"
    **/ 
	 public void dropApCanvass(LocalApCanvass apCanvass) {
	 	
	 	Debug.print("ApPurchaseRequisitionLineBean dropApCanvass");
	 	
	 	try {
	 		
	 		Collection apCanvasses = getApCanvasses();
	 		apCanvasses.remove(apCanvass);
	 		
	 	} catch (Exception ex) {
	 		
	 		throw new EJBException(ex.getMessage());
	 		
	 	}
	 	
	 }
	
	//		 EntityBean methods
	
	/**
	 * @ejb:create-method view-type="local"
	 **/
	public java.lang.Integer ejbCreate (Integer PRL_CODE, short PRL_LN, double PRL_QTY, double PRL_AMNT, Integer PRL_AD_CMPNY)
	throws CreateException {
		
		Debug.print("ApPurchaseRequisitionLineBean ejbCreate");
		setPrlCode(PRL_CODE);
		setPrlLine(PRL_LN);
		setPrlQuantity(PRL_QTY);
		setPrlAmount(PRL_AMNT);
		setPrlAdCompany(PRL_AD_CMPNY);
		
		return null;
	}
	
	/**
	 * @ejb:create-method view-type="local"
	 **/
	public java.lang.Integer ejbCreate (short PRL_LN, double PRL_QTY, double PRL_AMNT, Integer PRL_AD_CMPNY)
	throws CreateException {
		
		Debug.print("ApPurchaseRequisitionLineBean ejbCreate");
		setPrlLine(PRL_LN);
		setPrlQuantity(PRL_QTY);
		setPrlAmount(PRL_AMNT);
		setPrlAdCompany(PRL_AD_CMPNY);
		
		return null;
	}
	
	public void ejbPostCreate (Integer PRL_CODE, short PRL_LN, double PRL_QTY, double PRL_AMNT, Integer PRL_AD_CMPNY)
		throws CreateException { }
	
	public void ejbPostCreate (short PRL_LN, double PRL_QTY, double PRL_AMNT, Integer PRL_AD_CMPNY)
		throws CreateException { }
}