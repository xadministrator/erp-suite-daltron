/*
 * com/ejb/ap/LocalApRecurringVoucherBean.java
 *
 * Created on February 13, 2004, 06:00 PM
 */

package com.ejb.ap;

import java.util.Collection;
import java.util.Date;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;

import com.util.AbstractEntityBean;
import com.util.Debug;

/**
 *
 * @author  Neil Andrew M. Ajero
 *
 * @ejb:bean name="ApRecurringVoucherEJB"
 *           display-name="RecurringVoucher Entity"
 *           type="CMP"
 *           cmp-version="2.x"
 *           view-type="local"
 *           local-jndi-name="omega-ejb/ApRecurringVoucherEJB"
 *           schema="ApRecurringVoucher"
 *           primkey-field="rvCode"
 *
 * @ejb:pk class="java.lang.Integer"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="apuser"
 *                        role-link="apuserlink"
 *
 * @ejb:permission role-name="apuser"
 *
 * @ejb:interface local-class="com.ejb.ap.LocalApRecurringVoucher"
 *                local-extends="javax.ejb.EJBLocalObject"
 *
 * @ejb:home local-class="com.ejb.ap.LocalApRecurringVoucherHome"
 *           local-extends="javax.ejb.EJBLocalHome"
 *
 * @ejb:finder signature="LocalApRecurringVoucher findByRvName(java.lang.String RV_NM, java.lang.Integer RV_AD_BRNCH, java.lang.Integer RV_AD_CMPNY)"
 *             query="SELECT OBJECT(rv) FROM ApRecurringVoucher rv WHERE rv.rvName = ?1 AND rv.rvAdBranch = ?2 AND rv.rvAdCompany = ?3"
 *
 * @ejb:finder signature="Collection findRvToGenerateByAdUsrCodeAndDate(java.lang.Integer USR_CODE, java.util.Date DT, java.lang.Integer RV_AD_BRNCH, java.lang.Integer RV_AD_CMPNY)"
 *             query="SELECT OBJECT(rv) FROM ApRecurringVoucher rv WHERE (rv.rvAdUserName1=?1 OR rv.rvAdUserName2=?1 OR rv.rvAdUserName3=?1 OR rv.rvAdUserName4=?1 OR rv.rvAdUserName5=?1) AND rv.rvNextRunDate <= ?2 AND rv.rvAdBranch = ?3 AND rv.rvAdCompany = ?4"
 *
 * @jboss:query signature="Collection findRvToGenerateByAdUsrCodeAndDate(java.lang.Integer USR_CODE, java.util.Date DT, java.lang.Integer RV_AD_BRNCH, java.lang.Integer RV_AD_CMPNY)"
 *             query="SELECT OBJECT(rv) FROM ApRecurringVoucher rv WHERE (rv.rvAdUserName1=?1 OR rv.rvAdUserName2=?1 OR rv.rvAdUserName3=?1 OR rv.rvAdUserName4=?1 OR rv.rvAdUserName5=?1) AND rv.rvNextRunDate <= ?2 AND rv.rvAdBranch = ?3 AND rv.rvAdCompany = ?4"
 *
 * @ejb:finder signature="Collection findByUserCode1(java.lang.Integer USR_CODE, java.lang.Integer RV_AD_CMPNY)"
 *             query="SELECT OBJECT(rv) FROM ApRecurringVoucher rv WHERE rv.rvAdUserName1 = ?1 AND rv.rvAdCompany = ?2"
 * 
 * @ejb:finder signature="Collection findByUserCode2(java.lang.Integer USR_CODE, java.lang.Integer RV_AD_CMPNY)"
 *             query="SELECT OBJECT(rv) FROM ApRecurringVoucher rv WHERE rv.rvAdUserName2 = ?1 AND rv.rvAdCompany = ?2"
 * 
 * @ejb:finder signature="Collection findByUserCode3(java.lang.Integer USR_CODE, java.lang.Integer RV_AD_CMPNY)"
 *             query="SELECT OBJECT(rv) FROM ApRecurringVoucher rv WHERE rv.rvAdUserName3 = ?1 AND rv.rvAdCompany = ?2"
 * 
 * @ejb:finder signature="Collection findByUserCode4(java.lang.Integer USR_CODE, java.lang.Integer RV_AD_CMPNY)"
 *             query="SELECT OBJECT(rv) FROM ApRecurringVoucher rv WHERE rv.rvAdUserName4 = ?1 AND rv.rvAdCompany = ?2"
 * 
 * @ejb:finder signature="Collection findByUserCode5(java.lang.Integer USR_CODE, java.lang.Integer RV_AD_CMPNY)"
 *             query="SELECT OBJECT(rv) FROM ApRecurringVoucher rv WHERE rv.rvAdUserName5 = ?1 AND rv.rvAdCompany = ?2"
 *
 * @ejb:finder signature="Collection findByVbName(java.lang.String VB_NM, java.lang.Integer RV_AD_CMPNY)"
 *             query="SELECT OBJECT(rv) FROM ApRecurringVoucher rv  WHERE rv.apVoucherBatch.vbName=?1 AND rv.rvAdCompany = ?2"
 * 
 * @ejb:finder signature="LocalApRecurringVoucher findByRvNameAndSupplierCode(java.lang.String RV_NM, java.lang.String SPL_SPPLR_CODE, java.lang.Integer RV_AD_BRNCH, java.lang.Integer RV_AD_CMPNY)"
 *             query="SELECT OBJECT(rv) FROM ApRecurringVoucher rv WHERE rv.rvName = ?1 AND rv.apSupplier.splSupplierCode = ?2 AND rv.rvAdBranch = ?3 AND rv.rvAdCompany = ?4"
 *
 * @ejb:select signature="Collection ejbSelectGeneric(java.lang.String jbossQl, java.lang.Object[] args)"
 *             result-type-mapping="Local"
 *             method-intf="LocalHome"
 *             query="SELECT OBJECT(rv) FROM ApRecurringVoucher rv"
 *
 * @jboss:query signature="Collection ejbSelectGeneric(java.lang.String jbossQl, java.lang.Object[] args)"
 *                         dynamic="true"
 *
 * @ejb:value-object match="*"
 *             name="ApRecurringVoucher"
 *
 * @jboss:persistence table-name="AP_RCRRNG_VCHR"
 *
 * @jboss:entity-command name="mysql-get-generated-keys"
 */

public abstract class ApRecurringVoucherBean extends AbstractEntityBean {
    
    
    // PERSISTENT METHODS
    
    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     * @ejb:pk-field
     *
     * @jboss:column-name name="RV_CODE"
     *
     * @jboss:persistence auto-increment="true"
     **/
    public abstract Integer getRvCode();         
    public abstract void setRvCode(Integer RV_CODE);

    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="RV_NM"
     **/
     public abstract String getRvName();
     /**
      * @ejb:interface-method view-type="local"
      **/
     public abstract void setRvName(String RV_NM);

    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="RV_DESC"
     **/
     public abstract String getRvDescription();
     /**
      * @ejb:interface-method view-type="local"
      **/
     public abstract void setRvDescription(String RV_DESC);
     
    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="RV_CNVRSN_DT"
     **/
     public abstract Date getRvConversionDate();
     /**
      * @ejb:interface-method view-type="local"
      **/
     public abstract void setRvConversionDate(Date RV_CNVRSN_DT);

    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="RV_CNVRSN_RT"
     **/
     public abstract double getRvConversionRate();
     /**
      * @ejb:interface-method view-type="local"
      **/
     public abstract void setRvConversionRate(double RV_CNVRSN_RT);

    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="RV_AMNT"
     **/
     public abstract double getRvAmount();
     /**
      * @ejb:interface-method view-type="local"
      **/
     public abstract void setRvAmount(double RV_AMNT);

    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="RV_AMNT_DUE"
     **/
     public abstract double getRvAmountDue();
     /**
      * @ejb:interface-method view-type="local"
      **/
     public abstract void setRvAmountDue(double RV_AMNT_DUE);

	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="RV_AD_USR_NM1"
	 **/
	 public abstract java.lang.Integer getRvAdUserName1();
	 /**
	  * @ejb:interface-method view-type="local"
	  **/
	 public abstract void setRvAdUserName1(java.lang.Integer RV_AD_USR_NM1);

	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="RV_AD_USR_NM2"
	 **/
	 public abstract java.lang.Integer getRvAdUserName2();
	 /**
	  * @ejb:interface-method view-type="local"
	  **/
	 public abstract void setRvAdUserName2(java.lang.Integer RV_AD_USR_NM2);
	 
	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="RV_AD_USR_NM3"
	 **/
	 public abstract java.lang.Integer getRvAdUserName3();
	 /**
	  * @ejb:interface-method view-type="local"
	  **/
	 public abstract void setRvAdUserName3(java.lang.Integer RV_AD_USR_NM3);

	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="RV_AD_USR_NM4"
	 **/
	 public abstract java.lang.Integer getRvAdUserName4();
	 /**
	  * @ejb:interface-method view-type="local"
	  **/
	 public abstract void setRvAdUserName4(java.lang.Integer RV_AD_USR_NM4);
	 
	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="RV_AD_USR_NM5"
	 **/
	 public abstract java.lang.Integer getRvAdUserName5();
	 /**
	  * @ejb:interface-method view-type="local"
	  **/
	 public abstract void setRvAdUserName5(java.lang.Integer RV_AD_USR_NM5);	 

	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="RV_SCHDL"
	 **/
	 public abstract String getRvSchedule();
	 /**
	  * @ejb:interface-method view-type="local"
	  **/
	 public abstract void setRvSchedule(String RV_SCHDL);

	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="RV_NXT_RN_DT"
	 **/
	 public abstract Date getRvNextRunDate();
	 /**
	  * @ejb:interface-method view-type="local"
	  **/
	 public abstract void setRvNextRunDate(Date RV_NXT_RN_DT);
	 	 
	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="RV_LST_RN_DT"
	 **/
	 public abstract Date getRvLastRunDate();
	 /**
	  * @ejb:interface-method view-type="local"
	  **/
	 public abstract void setRvLastRunDate(Date RV_LST_RN_DT);

	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="RV_AD_BRNCH"
	 **/
	 public abstract Integer getRvAdBranch();
	 /**
	  * @ejb:interface-method view-type="local"
	  **/
	 public abstract void setRvAdBranch(Integer RV_AD_BRNCH);	 

	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="RV_AD_CMPNY"
	 **/
	 public abstract Integer getRvAdCompany();
	 /**
	  * @ejb:interface-method view-type="local"
	  **/
	 public abstract void setRvAdCompany(Integer RV_AD_CMPNY);	 


    // RELATIONSHIP METHODS    

     /**
      * @ejb:interface-method view-type="local"
      * @ejb:relation name="supplier-recurringvouchers"
      *               role-name="recurringvoucher-has-one-supplier"
      *
      * @jboss:relation related-pk-field="splCode"
      *                 fk-column="AP_SUPPLIER"
      */
     public abstract LocalApSupplier getApSupplier();
     public abstract void setApSupplier(LocalApSupplier apSupplier);

     /**
      * @ejb:interface-method view-type="local"
      * @ejb:relation name="withholdingtaxcode-recurringvouchers"
      *               role-name="recurringvoucher-has-one-withholdingtaxcode"
      *
      * @jboss:relation related-pk-field="wtcCode"
      *                 fk-column="AP_WITHHOLDING_TAX_CODE"
      */
     public abstract LocalApWithholdingTaxCode getApWithholdingTaxCode();
     public abstract void setApWithholdingTaxCode(LocalApWithholdingTaxCode apWithholdingTaxCode);

    /**
     * @ejb:interface-method view-type="local"
     * @ejb:relation name="paymentterm-recurringvouchers"
     *               role-name="recurringvoucher-has-one-paymentterm"
     * 
     * @jboss:relation related-pk-field="pytCode"
     *                 fk-column="AD_PAYMENT_TERM"
     */
    public abstract com.ejb.ad.LocalAdPaymentTerm getAdPaymentTerm();
    public abstract void setAdPaymentTerm(com.ejb.ad.LocalAdPaymentTerm adPaymentTerm);

    /**
     * @ejb:interface-method view-type="local"
     * @ejb:relation name="functionalcurrency-recurringvouchers"
     *               role-name="recurringvoucher-has-one-functionalcurrency"
     * 
     * @jboss:relation related-pk-field="fcCode"
     *                 fk-column="GL_FUNCTIONAL_CURRENCY"
     */
    public abstract com.ejb.gl.LocalGlFunctionalCurrency getGlFunctionalCurrency();
    public abstract void setGlFunctionalCurrency(com.ejb.gl.LocalGlFunctionalCurrency glFunctionalCurrency);

     /**
      * @ejb:interface-method view-type="local"
      * @ejb:relation name="aptaxcode-recurringvouchers"
      *               role-name="recurringvoucher-has-one-aptaxcode"
      *
      * @jboss:relation related-pk-field="tcCode"
      *                 fk-column="AP_TAX_CODE"
      */
     public abstract LocalApTaxCode getApTaxCode();
     public abstract void setApTaxCode(LocalApTaxCode apTaxCode);

     /**
      * @ejb:interface-method view-type="local"
      * @ejb:relation name="recurringvoucher-apdistributionrecords"
      *               role-name="recurringvoucher-has-many-apdistributionrecords"
      * 
      */
     public abstract Collection getApDistributionRecords();
     public abstract void setApDistributionRecords(Collection apDistributionRecords);
     
     /**
      * @ejb:interface-method view-type="local"
      * @ejb:relation name="voucherbatch-recurringvoucher"
      *               role-name="recurringvoucher-has-one-voucherbatch"
      *
      * @jboss:relation related-pk-field="vbCode"
      *                 fk-column="AP_VOUCHER_BATCH"
      */
     public abstract LocalApVoucherBatch getApVoucherBatch();
     public abstract void setApVoucherBatch(LocalApVoucherBatch apVoucherBatch);


	/**
	* @jboss:dynamic-ql
	*/
	public abstract Collection ejbSelectGeneric(java.lang.String jbossQl, java.lang.Object[] args)
	   throws FinderException;
	
	// BUSINESS METHODS
	
	/**
	* @ejb:home-method view-type="local"
	*/
	public Collection ejbHomeGetRvByCriteria(java.lang.String jbossQl, java.lang.Object[] args)
	   throws FinderException {
	   	
	   return ejbSelectGeneric(jbossQl, args);
	}

   /**
      * @ejb:interface-method view-type="local"
      **/
     public void addApDistributionRecord(LocalApDistributionRecord apDistributionRecord) {

          Debug.print("ApRecurringVoucherBean addApDistributionRecord");
      
          try {
      	
             Collection apDistributionRecords = getApDistributionRecords();
	         apDistributionRecords.add(apDistributionRecord);
	          
          } catch (Exception ex) {
      	
            throw new EJBException(ex.getMessage());
      
          }   
     }

     /**
      * @ejb:interface-method view-type="local"
      **/
     public void dropApDistributionRecord(LocalApDistributionRecord apDistributionRecord) {

          Debug.print("ApRecurringVoucherBean dropApDistributionRecord");
      
          try {
      	
             Collection apDistributionRecords = getApDistributionRecords();
	         apDistributionRecords.remove(apDistributionRecord);
	      
          } catch (Exception ex) {
      	
            throw new EJBException(ex.getMessage());
         
          }
             
     }
     
                 
     // ENTITY METHODS
    
     /**
      * @ejb:create-method view-type="local"
      **/
     public Integer ejbCreate(Integer RV_CODE, String RV_NM, String RV_DESC,
		Date RV_CNVRSN_DT, double RV_CNVRSN_RT, double RV_AMNT, double RV_AMNT_DUE, 
		java.lang.Integer RV_AD_USR_NM1, java.lang.Integer RV_AD_USR_NM2,
		java.lang.Integer RV_AD_USR_NM3, java.lang.Integer RV_AD_USR_NM4,
		java.lang.Integer RV_AD_USR_NM5, String RV_SCHDL,
		Date RV_NXT_RN_DT, Date RV_LST_RN_DT, 
		Integer RV_AD_BRNCH, Integer RV_AD_CMPNY)
        throws CreateException {
           
		Debug.print("ApRecurringVoucherBean ejbCreate");
		
		setRvCode(RV_CODE);
		setRvName(RV_NM);
		setRvDescription(RV_DESC);
		setRvConversionDate(RV_CNVRSN_DT);
		setRvConversionRate(RV_CNVRSN_RT);
		setRvAmount(RV_AMNT);
		setRvAmountDue(RV_AMNT_DUE);
		setRvAdUserName1(RV_AD_USR_NM1);
		setRvAdUserName2(RV_AD_USR_NM2);
		setRvAdUserName3(RV_AD_USR_NM3);
		setRvAdUserName4(RV_AD_USR_NM4);
		setRvAdUserName5(RV_AD_USR_NM5);
		setRvSchedule(RV_SCHDL);
		setRvNextRunDate(RV_NXT_RN_DT);
		setRvLastRunDate(RV_LST_RN_DT);
		setRvAdBranch(RV_AD_BRNCH);
		setRvAdCompany(RV_AD_CMPNY);
       
        return null;
        
     }
    
     /**
      * @ejb:create-method view-type="local"
      **/
     public Integer ejbCreate(String RV_NM, String RV_DESC,
		Date RV_CNVRSN_DT, double RV_CNVRSN_RT, double RV_AMNT, double RV_AMNT_DUE, 
		java.lang.Integer RV_AD_USR_NM1, java.lang.Integer RV_AD_USR_NM2,
		java.lang.Integer RV_AD_USR_NM3, java.lang.Integer RV_AD_USR_NM4,
		java.lang.Integer RV_AD_USR_NM5, String RV_SCHDL,
		Date RV_NXT_RN_DT, Date RV_LST_RN_DT, 
		Integer RV_AD_BRNCH, Integer RV_AD_CMPNY)
         throws CreateException {
           
		Debug.print("ApRecurringVoucherBean ejbCreate");
		
		setRvName(RV_NM);
		setRvDescription(RV_DESC);
		setRvConversionDate(RV_CNVRSN_DT);
		setRvConversionRate(RV_CNVRSN_RT);
		setRvAmount(RV_AMNT);
		setRvAmountDue(RV_AMNT_DUE);
		setRvAdUserName1(RV_AD_USR_NM1);
		setRvAdUserName2(RV_AD_USR_NM2);
		setRvAdUserName3(RV_AD_USR_NM3);
		setRvAdUserName4(RV_AD_USR_NM4);
		setRvAdUserName5(RV_AD_USR_NM5);
		setRvSchedule(RV_SCHDL);
		setRvNextRunDate(RV_NXT_RN_DT);
		setRvLastRunDate(RV_LST_RN_DT);
		setRvAdBranch(RV_AD_BRNCH);
		setRvAdCompany(RV_AD_CMPNY);
        
        return null;
        
     }
    
    
     public void ejbPostCreate(Integer RV_CODE, String RV_NM, String RV_DESC,
		Date RV_CNVRSN_DT, double RV_CNVRSN_RT, double RV_AMNT, double RV_AMNT_DUE, 
		java.lang.Integer RV_AD_USR_NM1, java.lang.Integer RV_AD_USR_NM2,
		java.lang.Integer RV_AD_USR_NM3, java.lang.Integer RV_AD_USR_NM4,
		java.lang.Integer RV_AD_USR_NM5, String RV_SCHDL,
		Date RV_NXT_RN_DT, Date RV_LST_RN_DT, 
		Integer RV_AD_BRNCH, Integer RV_AD_CMPNY)
        throws CreateException { }
   
     public void ejbPostCreate(String RV_NM, String RV_DESC,
		Date RV_CNVRSN_DT, double RV_CNVRSN_RT, double RV_AMNT, double RV_AMNT_DUE, 
		java.lang.Integer RV_AD_USR_NM1, java.lang.Integer RV_AD_USR_NM2,
		java.lang.Integer RV_AD_USR_NM3, java.lang.Integer RV_AD_USR_NM4,
		java.lang.Integer RV_AD_USR_NM5, String RV_SCHDL,
		Date RV_NXT_RN_DT, Date RV_LST_RN_DT, 
		Integer RV_AD_BRNCH, Integer RV_AD_CMPNY)
        throws CreateException { }
        
}

