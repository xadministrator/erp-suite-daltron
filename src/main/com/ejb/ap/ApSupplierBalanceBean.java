/*
 * com/ejb/ap/LocalApSupplierBalanceBean.java
 *
 * Created on February 13, 2004, 3:44 PM
 */

package com.ejb.ap;

import java.util.Date;

import javax.ejb.CreateException;

import com.util.AbstractEntityBean;
import com.util.Debug;

/**
 *
 * @author  Neil Andrew M. Ajero
 *
 * @ejb:bean name="ApSupplierBalanceEJB"
 *           display-name="SupplierBalance Entity"
 *           type="CMP"
 *           cmp-version="2.x"
 *           view-type="local"
 *           local-jndi-name="omega-ejb/ApSupplierBalanceEJB"
 *           schema="ApSupplierBalance"
 *           primkey-field="sbCode"
 *
 * @ejb:pk class="java.lang.Integer"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="apuser"
 *                        role-link="apuserlink"
 *
 * @ejb:permission role-name="apuser"
 *
 * @ejb:interface local-class="com.ejb.ap.LocalApSupplierBalance"
 *                local-extends="javax.ejb.EJBLocalObject"
 *
 * @ejb:home local-class="com.ejb.ap.LocalApSupplierBalanceHome"
 *           local-extends="javax.ejb.EJBLocalHome"
 *
 * @ejb:finder signature="Collection findByBeforeOrEqualVouDateAndSplSupplierCode(java.util.Date SB_DT, java.lang.String SPL_SPPLR_CODE, java.lang.Integer SB_AD_CMPNY)"
 *             query="SELECT OBJECT(sb) FROM ApSupplierBalance sb  WHERE sb.sbDate <= ?1 AND sb.apSupplier.splSupplierCode = ?2 AND sb.sbAdCompany = ?3"
 *
 * @jboss:query signature="Collection findByBeforeOrEqualVouDateAndSplSupplierCode(java.util.Date SB_DT, java.lang.String SPL_SPPLR_CODE, java.lang.Integer SB_AD_CMPNY)"
 *             query="SELECT OBJECT(sb) FROM ApSupplierBalance sb  WHERE sb.sbDate <= ?1 AND sb.apSupplier.splSupplierCode = ?2 AND sb.sbAdCompany = ?3 ORDER BY sb.sbDate"
 *
 * @ejb:finder signature="Collection findByAfterVouDateAndSplSupplierCode(java.util.Date SB_DT, java.lang.String SPL_SPPLR_CODE, java.lang.Integer SB_AD_CMPNY)"
 *             query="SELECT OBJECT(sb) FROM ApSupplierBalance sb  WHERE sb.sbDate > ?1 AND sb.apSupplier.splSupplierCode = ?2 AND sb.sbAdCompany = ?3"
 *
 * @jboss:query signature="Collection findByAfterVouDateAndSplSupplierCode(java.util.Date SB_DT, java.lang.String SPL_SPPLR_CODE, java.lang.Integer SB_AD_CMPNY)"
 *             query="SELECT OBJECT(sb) FROM ApSupplierBalance sb  WHERE sb.sbDate > ?1 AND sb.apSupplier.splSupplierCode = ?2 AND sb.sbAdCompany = ?3 ORDER BY sb.sbDate"
 *
 * @ejb:finder signature="Collection findByBeforeOrEqualSbDateAndSplCode(java.util.Date SB_DT, java.lang.Integer SPL_CODE, java.lang.Integer SB_AD_CMPNY)"
 *             query="SELECT OBJECT(sb) FROM ApSupplierBalance sb  WHERE sb.sbDate <= ?1 AND sb.apSupplier.splCode = ?2 AND sb.sbAdCompany = ?3"
 *
 * @jboss:query signature="Collection findByBeforeOrEqualSbDateAndSplCode(java.util.Date SB_DT, java.lang.Integer SPL_CODE, java.lang.Integer SB_AD_CMPNY)"
 *             query="SELECT OBJECT(sb) FROM ApSupplierBalance sb  WHERE sb.sbDate <= ?1 AND sb.apSupplier.splCode = ?2 AND sb.sbAdCompany = ?3 ORDER BY sb.sbDate, sb.sbCode" 
 *
 ** @ejb:finder signature="Collection findByBeforeSbDateAndSplCode(java.util.Date SB_DT, java.lang.Integer SPL_CODE, java.lang.Integer SB_AD_CMPNY)"
 *             query="SELECT OBJECT(sb) FROM ApSupplierBalance sb  WHERE sb.sbDate < ?1 AND sb.apSupplier.splCode = ?2 AND sb.sbAdCompany = ?3"
 *
 * @jboss:query signature="Collection findByBeforeSbDateAndSplCode(java.util.Date SB_DT, java.lang.Integer SPL_CODE, java.lang.Integer SB_AD_CMPNY)"
 *             query="SELECT OBJECT(sb) FROM ApSupplierBalance sb  WHERE sb.sbDate < ?1 AND sb.apSupplier.splCode = ?2 AND sb.sbAdCompany = ?3 ORDER BY sb.sbDate, sb.sbCode" 
 *
 * @ejb:finder signature="Collection findBySbDateFromAndSbDateToAndSplCode(java.util.Date SB_DT_FRM, java.util.Date SB_DT_TO, java.lang.Integer SPL_CODE, java.lang.Integer SB_AD_CMPNY)"
 *             query="SELECT OBJECT(sb) FROM ApSupplierBalance sb  WHERE sb.sbDate >= ?1 AND sb.sbDate <= ?2 AND sb.apSupplier.splCode = ?3 AND sb.sbAdCompany = ?4" 
 *
 * @jboss:query signature="Collection findBySbDateFromAndSbDateToAndSplCode(java.util.Date SB_DT_FRM, java.util.Date SB_DT_TO, java.lang.Integer SPL_CODE, java.lang.Integer SB_AD_CMPNY)"
 *             query="SELECT OBJECT(sb) FROM ApSupplierBalance sb  WHERE sb.sbDate >= ?1 AND sb.sbDate <= ?2 AND sb.apSupplier.splCode = ?3 AND sb.sbAdCompany = ?4 ORDER BY sb.sbDate" 
 *
 * @ejb:finder signature="Collection findByBeforeSbDateAndSplCode(java.util.Date SB_DT, java.lang.Integer SPL_CODE, java.lang.Integer SB_AD_CMPNY)"
 *             query="SELECT OBJECT(sb) FROM ApSupplierBalance sb  WHERE sb.sbDate < ?1 AND sb.apSupplier.splCode = ?2 AND sb.sbAdCompany = ?3"
 *
 * @jboss:query signature="Collection findByBeforeSbDateAndSplCode(java.util.Date SB_DT, java.lang.Integer SPL_CODE, java.lang.Integer SB_AD_CMPNY)"
 *             query="SELECT OBJECT(sb) FROM ApSupplierBalance sb  WHERE sb.sbDate < ?1 AND sb.apSupplier.splCode = ?2 AND sb.sbAdCompany = ?3 ORDER BY sb.sbDate" 
 
 * @ejb:value-object match="*"
 *             name="ApSupplierBalance"
 *
 * @jboss:persistence table-name="AP_SPPLR_BLNC"
 *
 * @jboss:entity-command name="mysql-get-generated-keys"
 */

public abstract class ApSupplierBalanceBean extends AbstractEntityBean {
    
    
    // PERSISTENT METHODS
    
    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     * @ejb:pk-field
     *
     * @jboss:column-name name="SB_CODE"
     *
     * @jboss:persistence auto-increment="true"
     **/
    public abstract Integer getSbCode();
    public abstract void setSbCode(Integer SB_CODE);
    
    
    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"   
     *
     * @jboss:column-name name="SB_DT"
     **/
    public abstract Date getSbDate();
     /**
      * @ejb:interface-method view-type="local"
      **/ 
    public abstract void setSbDate(Date SB_DT);
    

    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"   
     *
     * @jboss:column-name name="SB_BLNC"
     **/
    public abstract double getSbBalance();
     /**
      * @ejb:interface-method view-type="local"
      **/ 
    public abstract void setSbBalance(double SB_BLNC);
    
    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"   
     *
     * @jboss:column-name name="SB_AD_CMPNY"
     **/
    public abstract Integer getSbAdCompany();
     /**
      * @ejb:interface-method view-type="local"
      **/ 
    public abstract void setSbAdCompany(Integer SB_AD_CMPNY);
    
   
    // RELATIONSHIP METHODS
    
    /**
     * @ejb:interface-method view-type="local"
     * @ejb:relation name="supplier-supplierbalances"
     *               role-name="supplierbalance-has-one-supplier"
     * 
     * @jboss:relation related-pk-field="splCode"
     *                 fk-column="AP_SUPPLIER"
     */
    public abstract LocalApSupplier getApSupplier();
    /**
     * @ejb:interface-method view-type="local"
     **/
    public abstract void setApSupplier(LocalApSupplier apSupplier);


    // NO BUSINESS METHODS
     
 
    // ENTITY METHODS
    
    /**
     * @ejb:create-method view-type="local"
     **/
    public Integer ejbCreate(Integer SB_CODE, 
         Date SB_DT, double SB_BLNC, Integer SB_AD_CMPNY)     	
         throws CreateException {	
     	
         Debug.print("ApSupplierBalanceBean ejbCreate");
     
	     setSbCode(SB_CODE);
	     setSbDate(SB_DT);
	     setSbBalance(SB_BLNC);   
	     setSbAdCompany(SB_AD_CMPNY);

         return null;
     }
     
    /**
     * @ejb:create-method view-type="local"
     **/
    public Integer ejbCreate( 
         Date SB_DT, double SB_BLNC, Integer SB_AD_CMPNY)     	
         throws CreateException {	
     	
         Debug.print("ApSupplierBalanceBean ejbCreate");
     
	     setSbDate(SB_DT);
	     setSbBalance(SB_BLNC);
	     setSbAdCompany(SB_AD_CMPNY);

         return null;
     }
     
     public void ejbPostCreate(Integer SB_CODE, 
         Date SB_DT, double SB_BLNC, Integer SB_AD_CMPNY)      
         throws CreateException { }
      
     public void ejbPostCreate( 
         Date SB_DT, double SB_BLNC, Integer SB_AD_CMPNY)
         throws CreateException { }     
}