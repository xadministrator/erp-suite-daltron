/*
 * com/ejb/ap/LocalApSupplierBean.java
 *
 * Created on February 13, 2004, 3:44 PM
 */

package com.ejb.ap;

import java.util.Collection;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;

import com.ejb.ad.LocalAdBranchSupplier;
import com.ejb.ar.LocalArCustomer;
import com.ejb.gl.LocalGlChartOfAccountBalance;
import com.ejb.gl.LocalGlInvestorAccountBalance;
import com.ejb.hr.LocalHrDeployedBranch;
import com.ejb.hr.LocalHrEmployee;
import com.ejb.inv.LocalInvItem;
import com.ejb.inv.LocalInvLineItemTemplate;
import com.util.AbstractEntityBean;
import com.util.Debug;

/**
 *
 * @author  Neil Andrew M. Ajero
 *
 * @ejb:bean name="ApSupplierEJB"
 *           display-name="Supplier Entity"
 *           type="CMP"
 *           cmp-version="2.x"
 *           view-type="local"
 *           local-jndi-name="omega-ejb/ApSupplierEJB"
 *           schema="ApSupplier"
 *           primkey-field="splCode"
 *
 * @ejb:pk class="java.lang.Integer"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="apuser"
 *                        role-link="apuserlink"
 *
 * @ejb:permission role-name="apuser"
 *
 * @ejb:interface local-class="com.ejb.ap.LocalApSupplier"
 *                local-extends="javax.ejb.EJBLocalObject"
 *
 * @ejb:home local-class="com.ejb.ap.LocalApSupplierHome"
 *           local-extends="javax.ejb.EJBLocalHome"
 *
 * @ejb:finder signature="Collection findEnabledSplAll(java.lang.Integer SPL_AD_BRNCH, java.lang.Integer SPL_AD_CMPNY)"
 *             query="SELECT OBJECT(spl) FROM ApSupplier spl, IN(spl.adBranchSuppliers) bspl WHERE spl.splEnable = 1 AND bspl.adBranch.brCode = ?1 AND spl.splAdCompany spl.splAdCompany = ?2"
 *
 * @jboss:query signature="Collection findEnabledSplAll(java.lang.Integer SPL_AD_BRNCH, java.lang.Integer SPL_AD_CMPNY)"
 *             query="SELECT OBJECT(spl) FROM ApSupplier spl, IN(spl.adBranchSuppliers) bspl WHERE spl.splEnable = 1 AND bspl.adBranch.brCode = ?1 AND spl.splAdCompany = ?2 ORDER BY spl.splSupplierCode"
 *
 * @ejb:finder signature="Collection findEnabledSplTradeAll(java.lang.Integer SPL_AD_BRNCH, java.lang.Integer SPL_AD_CMPNY)"
 *             query="SELECT OBJECT(spl) FROM ApSupplier spl, IN(spl.adBranchSuppliers) bspl WHERE spl.splEnable = 1 AND spl.apSupplierClass.scName = 'Trade' AND bspl.adBranch.brCode = ?1 AND spl.splAdCompany spl.splAdCompany = ?2"
 *
 * @jboss:query signature="Collection findEnabledSplTradeAll(java.lang.Integer SPL_AD_BRNCH, java.lang.Integer SPL_AD_CMPNY)"
 *             query="SELECT OBJECT(spl) FROM ApSupplier spl, IN(spl.adBranchSuppliers) bspl WHERE spl.splEnable = 1 AND spl.apSupplierClass.scName = 'Trade' AND bspl.adBranch.brCode = ?1 AND spl.splAdCompany = ?2 ORDER BY spl.splSupplierCode"
 *             
 * @ejb:finder signature="Collection findEnabledSplAllOrderBySplName(java.lang.Integer SPL_AD_CMPNY)"
 *             query="SELECT OBJECT(spl) FROM ApSupplier spl WHERE spl.splEnable = 1 AND spl.splAdCompany = ?1"
 *
 * @jboss:query signature="Collection findEnabledSplAllOrderBySplName(java.lang.Integer SPL_AD_CMPNY)"
 *             query="SELECT OBJECT(spl) FROM ApSupplier spl WHERE spl.splEnable = 1 AND spl.splAdCompany = ?1 ORDER BY spl.splName"
 * 
 * @ejb:finder signature="Collection findAllEnabledSplScLedger(java.lang.Integer SPL_AD_CMPNY)"
 *             query="SELECT OBJECT(spl) FROM ApSupplier spl WHERE spl.splEnable = 1 AND spl.apSupplierClass.scLedger = 1 AND spl.splAdCompany = ?1"
 *
 * @jboss:query signature="Collection findAllEnabledSplScLedger(java.lang.Integer SPL_AD_CMPNY)"
 *             query="SELECT OBJECT(spl) FROM ApSupplier spl WHERE spl.splEnable = 1 AND spl.apSupplierClass.scLedger = 1 AND spl.splAdCompany = ?1 ORDER BY spl.splName"
 * 
 * @ejb:finder signature="Collection findAllSplByScCode(java.lang.Integer SC_CODE, java.lang.Integer SPL_AD_CMPNY)"
 *             query="SELECT OBJECT(spl) FROM ApSupplier spl WHERE spl.apSupplierClass.scCode = ?1 AND spl.splAdCompany = ?2"
 *
 * @jboss:query signature="Collection findAllSplByScCode(java.lang.Integer SC_CODE, java.lang.Integer SPL_AD_CMPNY)"
 *             query="SELECT OBJECT(spl) FROM ApSupplier spl WHERE spl.apSupplierClass.scCode = ?1 AND spl.splAdCompany = ?2 ORDER BY spl.splName"
 * 
 * @ejb:finder signature="Collection findAllSplInvestor(java.lang.Integer SPL_AD_CMPNY)"
 *             query="SELECT OBJECT(spl) FROM ApSupplier spl WHERE spl.apSupplierClass.scName = 'Investors' AND spl.splAdCompany = ?1"
 *
 * @jboss:query signature="Collection findAllSplInvestor(java.lang.Integer SPL_AD_CMPNY)"
 *             query="SELECT OBJECT(spl) FROM ApSupplier spl WHERE spl.apSupplierClass.scName = 'Investors' AND spl.splAdCompany = ?1 ORDER BY spl.splName"
 * 
 * @ejb:finder signature="LocalApSupplier findBySplSupplierCode(java.lang.String SPL_SPPLR_CODE, java.lang.Integer SPL_AD_CMPNY)"
 *             query="SELECT OBJECT(spl) FROM ApSupplier spl WHERE spl.splSupplierCode = ?1 AND spl.splAdCompany = ?2"
 * 
 * @ejb:finder signature="LocalApSupplier findBySplNameAndAddress(java.lang.String SPL_NM, java.lang.String SPL_ADDRSS, java.lang.Integer SPL_AD_CMPNY)"
 * 			   query="SELECT OBJECT(spl) FROM ApSupplier spl WHERE spl.splName = ?1 AND spl.splAddress = ?2 AND spl.splAdCompany = ?3"
 * 
 * @ejb:finder signature="Collection findBySplCoaGlPayableAccount(java.lang.Integer COA_CODE, java.lang.Integer SPL_AD_CMPNY)"
 *             query="SELECT OBJECT(spl) FROM ApSupplier spl WHERE spl.splCoaGlPayableAccount = ?1 AND spl.splAdCompany = ?2"
 * 
 * @ejb:finder signature="Collection findBySplCoaGlExpenseAccount(java.lang.Integer COA_CODE, java.lang.Integer SPL_AD_CMPNY)"
 *             query="SELECT OBJECT(spl) FROM ApSupplier spl WHERE spl.splCoaGlExpenseAccount = ?1 AND spl.splAdCompany = ?2"
 *
 * @ejb:finder signature="LocalApSupplier findBySplName(java.lang.String SPL_NM, java.lang.Integer SPL_AD_BRNCH, java.lang.Integer SPL_AD_CMPNY)"
 * 			   query="SELECT OBJECT(spl) FROM ApSupplier spl , IN(spl.adBranchSuppliers) bspl WHERE spl.splName = ?1 AND bspl.adBranch.brCode = ?2 AND spl.splAdCompany = ?3"
 * 
 * @ejb:finder signature="LocalApSupplier findBySplName(java.lang.String SPL_NM, java.lang.Integer SPL_AD_CMPNY)"
 * 			   query="SELECT OBJECT(spl) FROM ApSupplier spl WHERE spl.splName = ?1 AND spl.splAdCompany = ?2"
 *
 * @ejb:finder signature="Collection findEnabledSplAllOrderBySplSupplierCode(java.lang.Integer SPL_AD_CMPNY)"
 *             query="SELECT OBJECT(spl) FROM ApSupplier spl WHERE spl.splEnable = 1 AND spl.splAdCompany = ?1"
 *
 * @jboss:query signature="Collection findEnabledSplAllOrderBySplSupplierCode(java.lang.Integer SPL_AD_CMPNY)"
 *             query="SELECT OBJECT(spl) FROM ApSupplier spl WHERE spl.splEnable = 1 AND spl.splAdCompany = ?1 ORDER BY spl.splSupplierCode"
 *             
 * @ejb:finder signature="Collection findSplBySplNewAndUpdated(java.lang.Integer BR_CODE, java.lang.Integer AD_CMPNY, char NEW, char UPDATED, char DOWNLOADED_UPDATED)"
 * 			   query="SELECT DISTINCT OBJECT(spl) FROM ApSupplier spl, IN(spl.adBranchSuppliers) bspl WHERE (bspl.bsplSupplierDownloadStatus = ?3 OR bspl.bsplSupplierDownloadStatus = ?4 OR bspl.bsplSupplierDownloadStatus = ?5) AND bspl.adBranch.brCode = ?1 AND bspl.bsplAdCompany = ?2"
 * 
 * @ejb:select signature="Collection ejbSelectGeneric(java.lang.String jbossQl, java.lang.Object[] args)"
 *             result-type-mapping="Local"
 *             method-intf="LocalHome"
 *             query="SELECT OBJECT(spl) FROM ApSupplier spl"
 *
 * @jboss:query signature="Collection ejbSelectGeneric(java.lang.String jbossQl, java.lang.Object[] args)"
 *                         dynamic="true"
 *
 * @ejb:value-object match="*"
 *             name="ApSupplier"
 *
 * @jboss:persistence table-name="AP_SPPLR"
 *
 * @jboss:entity-command name="mysql-get-generated-keys"
 */

public abstract class ApSupplierBean extends AbstractEntityBean {
    
    
    // PERSISTENT METHODS
    
    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     * @ejb:pk-field
     *
     * @jboss:column-name name="SPL_CODE"
     *
     * @jboss:persistence auto-increment="true"
     **/
    public abstract Integer getSplCode();
    public abstract void setSplCode(Integer SPL_CODE);
    
    
    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"   
     *
     * @jboss:column-name name="SPL_SPPLR_CODE"
     **/
    public abstract String getSplSupplierCode();
     /**
      * @ejb:interface-method view-type="local"
      **/ 
    public abstract void setSplSupplierCode(String SPL_SPPLR_CODE);
    
    
    
    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"   
     *
     * @jboss:column-name name="SPL_ACCNT_NMBR"
     **/
    public abstract String getSplAccountNumber();
     /**
      * @ejb:interface-method view-type="local"
      **/ 
    public abstract void setSplAccountNumber(String SPL_ACCNT_NMBR);
    

    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"   
     *
     * @jboss:column-name name="SPL_NM"
     **/
    public abstract String getSplName();
     /**
      * @ejb:interface-method view-type="local"
      **/ 
    public abstract void setSplName(String SPL_NM);
    
    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="SPL_ADDRSS"
     **/
    public abstract String getSplAddress();
     /**
      * @ejb:interface-method view-type="local"
      **/ 
    public abstract void setSplAddress(String SPL_ADDRSS);
    

    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="SPL_CTY"
     **/
    public abstract String getSplCity();
     /**
      * @ejb:interface-method view-type="local"
      **/ 
    public abstract void setSplCity(String SPL_CTY);

 
    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="SPL_STT_PRVNC"
     **/
    public abstract String getSplStateProvince();
     /**
      * @ejb:interface-method view-type="local"
      **/ 
    public abstract void setSplStateProvince(String SPL_STT_PRVNC); 


    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="SPL_PSTL_CD"
     **/
    public abstract String getSplPostalCode();
     /**
      * @ejb:interface-method view-type="local"
      **/ 
    public abstract void setSplPostalCode(String SPL_PSTL_CD); 
        

    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="SPL_CNTRY"
     **/
    public abstract String getSplCountry();
     /**
      * @ejb:interface-method view-type="local"
      **/ 
    public abstract void setSplCountry(String SPL_CNTRY); 


    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="SPL_CNTCT"
     **/
    public abstract String getSplContact();
    /**
     * @ejb:interface-method view-type="local"
     **/
    public abstract void setSplContact(String SPL_CNTCT); 
    

    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="SPL_PHN"
     **/
    public abstract String getSplPhone();
     /**
      * @ejb:interface-method view-type="local"
      **/ 
    public abstract void setSplPhone(String SPL_PHN);
    

    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="SPL_FX"
     **/
    public abstract String getSplFax();
     /**
      * @ejb:interface-method view-type="local"
      **/ 
    public abstract void setSplFax(String SPL_FX); 
 

    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="SPL_ALTRNT_PHN"
     **/
    public abstract String getSplAlternatePhone();
     /**
      * @ejb:interface-method view-type="local"
      **/ 
    public abstract void setSplAlternatePhone(String SPL_ALTRNT_PHN); 


    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="SPL_ALTRNT_CNTCT"
     **/
    public abstract String getSplAlternateContact();
     /**
      * @ejb:interface-method view-type="local"
      **/ 
    public abstract void setSplAlternateContact(String SPL_ALTRNT_CNTCT);
        

    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="SPL_EML"
     **/
    public abstract String getSplEmail();
     /**
      * @ejb:interface-method view-type="local"
      **/ 
    public abstract void setSplEmail(String SPL_EML); 


    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="SPL_TIN"
     **/
    public abstract String getSplTin();
     /**
      * @ejb:interface-method view-type="local"
      **/ 
    public abstract void setSplTin(String SPL_TIN); 
    
    
    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="SPL_COA_GL_PYBL_ACCNT"
     **/
    public abstract Integer getSplCoaGlPayableAccount();
     /**
      * @ejb:interface-method view-type="local"
      **/ 
    public abstract void setSplCoaGlPayableAccount(Integer SPL_COA_GL_PYBL_ACCNT); 
    
    
    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="SPL_COA_GL_EXPNS_ACCNT"
     **/
    public abstract Integer getSplCoaGlExpenseAccount();
     /**
      * @ejb:interface-method view-type="local"
      **/ 
    public abstract void setSplCoaGlExpenseAccount(Integer SPL_COA_GL_EXPNS_ACCNT); 
    
    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="SPL_ENBL"
     **/
    public abstract byte getSplEnable();
     /**
      * @ejb:interface-method view-type="local"
      **/ 
    public abstract void setSplEnable(byte SPL_ENBL);
    
    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"   
     *
     * @jboss:column-name name="SPL_RMRKS"
     **/
    public abstract String getSplRemarks();
     /**
      * @ejb:interface-method view-type="local"
      **/ 
    public abstract void setSplRemarks(String SPL_RMRKS);

    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="SPL_AD_CMPNY"
     **/
    public abstract Integer getSplAdCompany();
     /**
      * @ejb:interface-method view-type="local"
      **/ 
    public abstract void setSplAdCompany(Integer SPL_AD_CMPNY);
    
    // RELATIONSHIP METHODS

    /**
	 * @ejb:interface-method view-type="local"
	 * @ejb:relation name="supplier-customers"
	 *               role-name="supplier-has-many-customers"
	 * 
	 */
	public abstract Collection getArCustomers();
	public abstract void setArCustomers(Collection arCustomers);

    /**
     * @ejb:interface-method view-type="local"
     * @ejb:relation name="paymentterm-suppliers"
     *               role-name="supplier-has-one-paymentterm"
     * 
     * @jboss:relation related-pk-field="pytCode"
     *                 fk-column="AD_PAYMENT_TERM"
     */
    public abstract com.ejb.ad.LocalAdPaymentTerm getAdPaymentTerm();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
    public abstract void setAdPaymentTerm(com.ejb.ad.LocalAdPaymentTerm adPaymentTerm);
    
    
    /**
	 * @ejb:interface-method view-type="local"
	 * @ejb:relation name="supplier-investoraccountbalances"
	 *               role-name="supplier-has-many-investoraccountbalances"
	 * 
	 */
	public abstract Collection getGlInvestorAccountBalances();
	public abstract void setGlInvestorAccountBalances(Collection glInvestor);
	


    /**
     * @ejb:interface-method view-type="local"
     * @ejb:relation name="suppliertype-suppliers"
     *               role-name="supplier-has-one-suppliertype"
     * 
     * @jboss:relation related-pk-field="stCode"
     *                 fk-column="AP_SUPPLIER_TYPE"
     */
    public abstract com.ejb.ap.LocalApSupplierType getApSupplierType();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
    public abstract void setApSupplierType(com.ejb.ap.LocalApSupplierType apSupplierType);

    /**
     * @ejb:interface-method view-type="local"
     * @ejb:relation name="supplierclass-suppliers"
     *               role-name="supplier-has-one-supplierclass"
     * 
     * @jboss:relation related-pk-field="scCode"
     *                 fk-column="AP_SUPPLIER_CLASS"
     */
    public abstract com.ejb.ap.LocalApSupplierClass getApSupplierClass();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
    public abstract void setApSupplierClass(com.ejb.ap.LocalApSupplierClass apSupplierClass);  
    
    /**
     * @ejb:interface-method view-type="local"
     * @ejb:relation name="bankaccount-suppliers"
     *               role-name="supplier-has-one-bankaccount"
     * 
     * @jboss:relation related-pk-field="baCode"
     *                 fk-column="AD_BANK_ACCOUNT"
     */
    public abstract com.ejb.ad.LocalAdBankAccount getAdBankAccount();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
    public abstract void setAdBankAccount(com.ejb.ad.LocalAdBankAccount adBankAccount);
     
    /**
     * @ejb:interface-method view-type="local"
     * @ejb:relation name="supplier-vouchers"
     *               role-name="supplier-has-many-vouchers"
     * 
     */
    public abstract Collection getApVouchers();
    public abstract void setApVouchers(Collection apVouchers);  
    
    
     
    /**
     * @ejb:interface-method view-type="local"
     * @ejb:relation name="supplier-checks"
     *               role-name="supplier-has-many-checks"
     * 
     */
    public abstract Collection getApChecks();
    public abstract void setApChecks(Collection apChecks);

    /**
     * @ejb:interface-method view-type="local"
     * @ejb:relation name="supplier-recurringvouchers"
     *               role-name="supplier-has-many-recurringvouchers"
     * 
     */
    public abstract Collection getApRecurringVouchers();
    public abstract void setApRecurringVouchers(Collection apRecurringVouchers);
    
    /**
     * @ejb:interface-method view-type="local"
     * @ejb:relation name="supplier-supplierbalances"
     *               role-name="supplier-has-many-supplierbalances"
     * 
     */
    public abstract Collection getApSupplierBalances();
    public abstract void setApSupplierBalances(Collection apSupplierBalances);
    
   /**
     * @ejb:interface-method view-type="local"
     * @ejb:relation name="supplier-purchaseorders"
     *               role-name="supplier-has-many-purchaseorders"
     * 
     */
    public abstract Collection getApPurchaseOrders();
    public abstract void setApPurchaseOrders(Collection apPurchaseOrders);
    
    /**
     * @ejb:interface-method view-type="local"
     * @ejb:relation name="supplier-adjustments"
     *               role-name="supplier-has-many-adjustments"
     * 
     */
    public abstract Collection getInvAdjustments();
    public abstract void setInvAdjustments(Collection invAdjustments);

    /**
     * @ejb:interface-method view-type="local"
     * @ejb:relation name="supplier-branchsuppliers" 
     *				 role-name="supplier-has-many-branchsupplier"
     *
     */
    public abstract Collection getAdBranchSuppliers();
    public abstract void setAdBranchSuppliers(Collection adBranchSuppliers);
    
    /**
     * @ejb:interface-method view-type="local"
     * @ejb:relation name="supplier-canvasses"
     *               role-name="supplier-has-many-canvasses"
     * 
     */
    public abstract Collection getApCanvasses();
    public abstract void setApCanvasses(Collection apCanvasses);
    
    /**
     * @ejb:interface-method view-type="local"
     * @ejb:relation name="supplier-items"
     *               role-name="supplier-has-many-items"
     * 
     */
    public abstract Collection getInvItems();
    public abstract void setInvItems(Collection invItems);
    
    /**
     * @ejb:interface-method view-type="local"
     * @ejb:relation name="lineItemTemplate-suppliers"
     *               role-name="supplier-has-one-lineItemTemplate"
     * 
     * @jboss:relation related-pk-field="litCode"
     *                 fk-column="INV_LINE_ITEM_TEMPLATE"
     * 
     */
    public abstract LocalInvLineItemTemplate getInvLineItemTemplate();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
    public abstract void setInvLineItemTemplate(LocalInvLineItemTemplate invLineItemTemplate);
    
    
   /**
    * @jboss:dynamic-ql
    */
    public abstract Collection ejbSelectGeneric(java.lang.String jbossQl, java.lang.Object[] args)
       throws FinderException;
       
    

   // BUSINESS METHODS
   
   /**
    * @ejb:home-method view-type="local"
    */
    public Collection ejbHomeGetSplByCriteria(java.lang.String jbossQl, java.lang.Object[] args)
       throws FinderException {
       	
       return ejbSelectGeneric(jbossQl, args);
    }
    
    
    
    
    /**
	 * @ejb:interface-method view-type="local"
	 **/
	public void addGlInvestorAccountBalance(LocalGlInvestorAccountBalance glInvestorAccountBalance) {

		Debug.print("ApSupplierBean addGlInvestorAccountBalance");
		try {
			Collection glInvestorAccountBalances = getGlInvestorAccountBalances();
			glInvestorAccountBalances.add(glInvestorAccountBalance);
			
		} catch (Exception ex) {
			throw new EJBException(ex.getMessage());
		}
	}

	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public void dropGlInvestorAccountBalance(LocalGlInvestorAccountBalance glInvestorAccountBalance) {

		Debug.print("ApSupplierBean dropGlInvestorAccountBalance");
		try {
			Collection glInvestorAccountBalances = getGlInvestorAccountBalances();
			glInvestorAccountBalances.remove(glInvestorAccountBalance);
		} catch (Exception ex) {
			throw new EJBException(ex.getMessage());
		}
	}
	
	
         
    /**
     * @ejb:interface-method view-type="local"
     **/
    public void addApVoucher(LocalApVoucher apVoucher) {

         Debug.print("ApSupplierBean addApVoucher");
      
         try {
      	
            Collection apVouchers = getApVouchers();
	        apVouchers.add(apVoucher);
	          
         } catch (Exception ex) {
      	
           throw new EJBException(ex.getMessage());
      
         }   
     }

    /**
     * @ejb:interface-method view-type="local"
     **/
    public void dropApVoucher(LocalApVoucher apVoucher) {

         Debug.print("ApSupplierBean dropApVoucher");
      
         try {
      	
            Collection apVouchers = getApVouchers();
	        apVouchers.remove(apVoucher);
	      
         } catch (Exception ex) {
      	
           throw new EJBException(ex.getMessage());
         
         }   
     }
    
    /**
     * @ejb:interface-method view-type="local"
     **/
    public void addApCheck(LocalApCheck apCheck) {

         Debug.print("ApSupplierBean addApCheck");
      
         try {
      	
            Collection apChecks = getApChecks();
	        apChecks.add(apCheck);
	          
         } catch (Exception ex) {
      	
           throw new EJBException(ex.getMessage());
      
         }   
     }

    /**
     * @ejb:interface-method view-type="local"
     **/
    public void dropApCheck(LocalApCheck apCheck) {

         Debug.print("ApSupplierBean dropApCheck");
      
         try {
      	
            Collection apChecks = getApChecks();
	        apChecks.remove(apCheck);
	      
         } catch (Exception ex) {
      	
           throw new EJBException(ex.getMessage());
         
         } 
           
     }
     
    /**
     * @ejb:interface-method view-type="local"
     **/
    public void addApRecurringVoucher(LocalApRecurringVoucher apRecurringVoucher) {

         Debug.print("ApSupplierBean addApRecurringVoucher");
      
         try {
      	
            Collection apRecurringVouchers = getApRecurringVouchers();
	        apRecurringVouchers.add(apRecurringVoucher);
	          
         } catch (Exception ex) {
      	
           throw new EJBException(ex.getMessage());
      
         }   
     }

    /**
     * @ejb:interface-method view-type="local"
     **/
    public void dropApRecurringVoucher(LocalApRecurringVoucher apRecurringVoucher) {

         Debug.print("ApSupplierBean dropApRecurringVoucher");
      
         try {
      	
            Collection apRecurringVouchers = getApRecurringVouchers();
	        apRecurringVouchers.remove(apRecurringVoucher);
	      
         } catch (Exception ex) {
      	
           throw new EJBException(ex.getMessage());
         
         }   
     }     
     
     
    /**
     * @ejb:interface-method view-type="local"
     **/
    public void addApSupplierBalance(LocalApSupplierBalance apSupplierBalance) {

         Debug.print("ApSupplierBean addApSupplierBalance");
      
         try {
      	
            Collection apSupplierBalances = getApSupplierBalances();
	        apSupplierBalances.add(apSupplierBalance);
	          
         } catch (Exception ex) {
      	
           throw new EJBException(ex.getMessage());
      
         }   
     }

    /**
     * @ejb:interface-method view-type="local"
     **/
    public void dropApSupplierBalance(LocalApSupplierBalance apSupplierBalance) {

         Debug.print("ApSupplierBean dropApSupplierBalance");
      
         try {
      	
            Collection apSupplierBalances = getApSupplierBalances();
	        apSupplierBalances.remove(apSupplierBalance);
	      
         } catch (Exception ex) {
      	
           throw new EJBException(ex.getMessage());
         
         }   
     }
     
    /**
     * @ejb:interface-method view-type="local"
     **/
    public void addApPurchaseOrder(LocalApPurchaseOrder apPurchaseOrder) {

         Debug.print("ApSupplierBean addApPurchaseOrder");
      
         try {
      	
            Collection apPurchaseOrders = getApPurchaseOrders();
	        apPurchaseOrders.add(apPurchaseOrder);
	          
         } catch (Exception ex) {
      	
           throw new EJBException(ex.getMessage());
      
         }   
     }

    /**
     * @ejb:interface-method view-type="local"
     **/
    public void dropApPurchaseOrder(LocalApPurchaseOrder apPurchaseOrder) {

         Debug.print("ApSupplierBean dropApPurchaseOrder");
      
         try {
      	
            Collection apPurchaseOrders = getApPurchaseOrders();
	        apPurchaseOrders.remove(apPurchaseOrder);
	      
         } catch (Exception ex) {
      	
           throw new EJBException(ex.getMessage());
         
         }   
     }
                      
    /**
     * @ejb:interface-method view-type="local"
     **/
    public void addAdBranchSupplier(LocalAdBranchSupplier adBranchSupplier) {

         Debug.print("ApSupplierBean addAdBranchSupplier");
      
         try {
      	
            Collection adBranchSuppliers = getAdBranchSuppliers();
	        adBranchSuppliers.add(adBranchSupplier);
	          
         } catch (Exception ex) {
      	
           throw new EJBException(ex.getMessage());
      
         }   
     }

    /**
     * @ejb:interface-method view-type="local"
     **/
    public void dropAdBranchSupplier(LocalAdBranchSupplier adBranchSupplier) {

         Debug.print("ApSupplierBean dropAdBranchSupplier");
      
         try {
      	
            Collection adBranchSuppliers = getAdBranchSuppliers();
	        adBranchSuppliers.remove(adBranchSupplier);
	      
         } catch (Exception ex) {
      	
           throw new EJBException(ex.getMessage());
         
         }   
     }    
    
    /**
     * @ejb:interface-method view-type="local"
     **/
    public void addApCanvass(LocalApCanvass apCanvass) {

         Debug.print("ApSupplierBean addApCanvass");
      
         try {
      	
            Collection apCanvasses = getApCanvasses();
	        apCanvasses.add(apCanvass);
	          
         } catch (Exception ex) {
      	
           throw new EJBException(ex.getMessage());
      
         }   
     }

    /**
     * @ejb:interface-method view-type="local"
     **/
    public void dropApCanvass(LocalApCanvass apCanvass) {

         Debug.print("ApSupplierBean dropApCanvass");
      
         try {
      	
            Collection apCanvasses = getApCanvasses();
	        apCanvasses.remove(apCanvass);
	      
         } catch (Exception ex) {
      	
           throw new EJBException(ex.getMessage());
         
         }   
     }
    
    /**
     * @ejb:interface-method view-type="local"
     **/
    public void addInvItem(LocalInvItem invItem) {

         Debug.print("ApSupplierBean addInvItem");
      
         try {
      	
            Collection invItems = getInvItems();
            invItems.add(invItem);
	          
         } catch (Exception ex) {
      	
           throw new EJBException(ex.getMessage());
      
         }   
     }

    /**
     * @ejb:interface-method view-type="local"
     **/
    public void dropInvItem(LocalInvItem invItem) {

         Debug.print("ApSupplierBean dropInvItem");
      
         try {
      	
            Collection invItems = getInvItems();
            invItems.remove(invItem);
	      
         } catch (Exception ex) {
      	
           throw new EJBException(ex.getMessage());
         
         }   
     }
    
    
    /**
	 * @ejb:interface-method view-type="local"
	 **/
	public void addArCustomer(LocalArCustomer arCustomer) {
		
		Debug.print("ApSupplierBean addArCustomer");
		
		try {
			
			Collection arCustomers = getArCustomers();
			arCustomers.add(arCustomer);
			
		} catch (Exception ex) {
			
			throw new EJBException(ex.getMessage());
			
		}   
	}
	
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public void dropArCustomer(LocalArCustomer arCustomer) {
		
		Debug.print("ArCustomerBean dropArCustomer");
		
		try {
			
			Collection arCustomers = getArCustomers();
			arCustomers.remove(arCustomer);
			
		} catch (Exception ex) {
			
			throw new EJBException(ex.getMessage());
			
		} 
		
	}
	
    
    // ENTITY METHODS
    
    /**
     * @ejb:create-method view-type="local"
     **/
    public Integer ejbCreate(Integer SPL_CODE,
         String SPL_SPPLR_CODE, String SPL_ACCNT_NMBR, String SPL_NM, String SPL_ADDRSS, 
         String SPL_CTY, String SPL_STT_PRVNC, String SPL_PSTL_CD, String SPL_CNTRY, String SPL_CNTCT, 
         String SPL_PHN, String SPL_FX, String SPL_ALTRNT_PHN, String SPL_ALTRNT_CNTCT, String SPL_EML, 
         String SPL_TIN, Integer SPL_COA_GL_PYBL_ACCNT, Integer SPL_COA_GL_EXPNS_ACCNT, byte SPL_ENBL,
		 String SPL_RMRKS, Integer SPL_AD_CMPNY)     	
         throws CreateException {	
     	
         Debug.print("ApSupplierBean ejbCreate");
     
	     setSplCode(SPL_CODE);	     
	     setSplSupplierCode(SPL_SPPLR_CODE);
             setSplAccountNumber(SPL_ACCNT_NMBR);
	     setSplName(SPL_NM);   
	     setSplAddress(SPL_ADDRSS);
	     setSplCity(SPL_CTY);
         setSplStateProvince(SPL_STT_PRVNC);
         setSplPostalCode(SPL_PSTL_CD);
         setSplCountry(SPL_CNTRY);
         setSplContact(SPL_CNTCT);
         setSplPhone(SPL_PHN);
         setSplFax(SPL_FX);
         setSplAlternatePhone(SPL_ALTRNT_PHN);
         setSplAlternateContact(SPL_ALTRNT_CNTCT);
         setSplEmail(SPL_EML);
         setSplTin(SPL_TIN);
         setSplCoaGlPayableAccount(SPL_COA_GL_PYBL_ACCNT);
		 setSplCoaGlExpenseAccount(SPL_COA_GL_EXPNS_ACCNT);
         setSplEnable(SPL_ENBL);
         setSplRemarks(SPL_RMRKS);
         setSplAdCompany(SPL_AD_CMPNY);
         

         return null;
     }
     
    /**
     * @ejb:create-method view-type="local"
     **/
    public Integer ejbCreate( 
         String SPL_SPPLR_CODE, String SPL_ACCNT_NMBR,String SPL_NM, String SPL_ADDRSS, 
         String SPL_CTY, String SPL_STT_PRVNC, String SPL_PSTL_CD, String SPL_CNTRY, String SPL_CNTCT, 
         String SPL_PHN, String SPL_FX, String SPL_ALTRNT_PHN, String SPL_ALTRNT_CNTCT, String SPL_EML, 
         String SPL_TIN, Integer SPL_COA_GL_PYBL_ACCNT, Integer SPL_COA_GL_EXPNS_ACCNT, byte SPL_ENBL,
         String SPL_RMRKS, Integer SPL_AD_CMPNY)     	
         throws CreateException {	
     	
         Debug.print("ApSupplierBean ejbCreate");
     
	     setSplSupplierCode(SPL_SPPLR_CODE);	
             setSplAccountNumber(SPL_ACCNT_NMBR);
	     setSplName(SPL_NM);   
	     setSplAddress(SPL_ADDRSS);
	     setSplCity(SPL_CTY);
         setSplStateProvince(SPL_STT_PRVNC);
         setSplPostalCode(SPL_PSTL_CD);
         setSplCountry(SPL_CNTRY);
         setSplContact(SPL_CNTCT);
         setSplPhone(SPL_PHN);
         setSplFax(SPL_FX);
         setSplAlternatePhone(SPL_ALTRNT_PHN);
         setSplAlternateContact(SPL_ALTRNT_CNTCT);
         setSplEmail(SPL_EML);
         setSplTin(SPL_TIN);
         setSplCoaGlPayableAccount(SPL_COA_GL_PYBL_ACCNT);
		 setSplCoaGlExpenseAccount(SPL_COA_GL_EXPNS_ACCNT);
         setSplEnable(SPL_ENBL);
         setSplRemarks(SPL_RMRKS);
         setSplAdCompany(SPL_AD_CMPNY);
         

         return null;
     }
     
     public void ejbPostCreate(Integer SPL_CODE,
         String SPL_SPPLR_CODE, String SPL_ACCNT_NMBR, String SPL_NM, String SPL_ADDRSS, 
         String SPL_CTY, String SPL_STT_PRVNC, String SPL_PSTL_CD, String SPL_CNTRY, String SPL_CNTCT, 
         String SPL_PHN, String SPL_FX, String SPL_ALTRNT_PHN, String SPL_ALTRNT_CNTCT, String SPL_EML, 
         String SPL_TIN, Integer SPL_COA_GL_PYBL_ACCNT, Integer SPL_COA_GL_EXPNS_ACCNT, byte SPL_ENBL,
         String SPL_RMRKS, Integer SPL_AD_CMPNY)      
         throws CreateException { }
      
     public void ejbPostCreate(
         String SPL_SPPLR_CODE, String SPL_ACCNT_NMBR, String SPL_NM, String SPL_ADDRSS, 
         String SPL_CTY, String SPL_STT_PRVNC, String SPL_PSTL_CD, String SPL_CNTRY, String SPL_CNTCT, 
         String SPL_PHN, String SPL_FX, String SPL_ALTRNT_PHN, String SPL_ALTRNT_CNTCT, String SPL_EML, 
         String SPL_TIN, Integer SPL_COA_GL_PYBL_ACCNT, Integer SPL_COA_GL_EXPNS_ACCNT, byte SPL_ENBL,
         String SPL_RMRKS, Integer SPL_AD_CMPNY)
         throws CreateException { }     
}