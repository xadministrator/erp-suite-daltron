/*
 * com/ejb/ap/LocalApSupplierClassBean.java
 *
 * Created on February 13, 2004, 3:44 PM
 */

package com.ejb.ap;

import java.util.Collection;
import java.util.Date;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;

import com.util.AbstractEntityBean;
import com.util.Debug;

/**
 *
 * @author  Neil Andrew M. Ajero
 *
 * @ejb:bean name="ApSupplierClassEJB"
 *           display-name="SupplierClass Entity"
 *           type="CMP"
 *           cmp-version="2.x"
 *           view-type="local"
 *           local-jndi-name="omega-ejb/ApSupplierClassEJB"
 *           schema="ApSupplierClass"
 *           primkey-field="scCode"
 *
 * @ejb:pk class="java.lang.Integer"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="apuser"
 *                        role-link="apuserlink"
 *
 * @ejb:permission role-name="apuser"
 *
 * @ejb:interface local-class="com.ejb.ap.LocalApSupplierClass"
 *                local-extends="javax.ejb.EJBLocalObject"
 *
 * @ejb:home local-class="com.ejb.ap.LocalApSupplierClassHome"
 *           local-extends="javax.ejb.EJBLocalHome"
 *
 * @ejb:finder signature="Collection findEnabledScAll(java.lang.Integer SC_AD_CMPNY)"
 *             query="SELECT OBJECT(sc) FROM ApSupplierClass sc WHERE sc.scEnable = 1 AND sc.scAdCompany = ?1"
 *
 * @jboss:query signature="Collection findEnabledScAll(java.lang.Integer SC_AD_CMPNY)"
 *             query="SELECT OBJECT(sc) FROM ApSupplierClass sc WHERE sc.scEnable = 1 AND sc.scAdCompany = ?1 ORDER BY sc.scName"
 *
 * @ejb:finder signature="Collection findScAll(java.lang.Integer SC_AD_CMPNY)"
 *             query="SELECT OBJECT(sc) FROM ApSupplierClass sc WHERE sc.scAdCompany = ?1"
 *
 * @jboss:query signature="Collection findScAll(java.lang.Integer SC_AD_CMPNY)"
 *             query="SELECT OBJECT(sc) FROM ApSupplierClass sc WHERE sc.scAdCompany = ?1 ORDER BY sc.scName"
 *
 * @ejb:finder signature="Collection findByScGlCoaPayableAccount(java.lang.Integer COA_CODE, java.lang.Integer SC_AD_CMPNY)"
 *             query="SELECT OBJECT(sc) FROM ApSupplierClass sc WHERE sc.scGlCoaPayableAccount=?1 AND sc.scAdCompany = ?2"
 *
 * @ejb:finder signature="Collection findByScGlCoaExpenseAccount(java.lang.Integer COA_CODE, java.lang.Integer SC_AD_CMPNY)"
 *             query="SELECT OBJECT(sc) FROM ApSupplierClass sc WHERE sc.scGlCoaExpenseAccount=?1 AND sc.scAdCompany = ?2"
 *
 * @ejb:finder signature="LocalApSupplierClass findByScName(java.lang.String SC_NM, java.lang.Integer SC_AD_CMPNY)"
 *             query="SELECT OBJECT(sc) FROM ApSupplierClass sc WHERE sc.scName = ?1 AND sc.scAdCompany = ?2"
 *
 * @ejb:select signature="Collection ejbSelectGeneric(java.lang.String jbossQl, java.lang.Object[] args)"
 *             result-type-mapping="Local"
 *             method-intf="LocalHome"
 *             query="SELECT OBJECT(sc) FROM ApSupplierClass sc"
 *
 * @jboss:query signature="Collection ejbSelectGeneric(java.lang.String jbossQl, java.lang.Object[] args)"
 *                         dynamic="true"
 *
 * @ejb:value-object match="*"
 *             name="ApSupplierClass"
 *
 * @jboss:persistence table-name="AP_SPPLR_CLSS"
 *
 * @jboss:entity-command name="mysql-get-generated-keys"
 */

public abstract class ApSupplierClassBean extends AbstractEntityBean {


    // PERSISTENT METHODS

    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     * @ejb:pk-field
     *
     * @jboss:column-name name="SC_CODE"
     *
     * @jboss:persistence auto-increment="true"
     **/
    public abstract Integer getScCode();
    public abstract void setScCode(Integer SC_CODE);


    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="SC_NM"
     **/
    public abstract String getScName();
     /**
      * @ejb:interface-method view-type="local"
      **/
    public abstract void setScName(String SC_NM);


    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="SC_DESC"
     **/
    public abstract String getScDescription();
     /**
      * @ejb:interface-method view-type="local"
      **/
    public abstract void setScDescription(String SC_DESC);


    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="SC_GL_COA_PYBL_ACCNT"
     **/
    public abstract java.lang.Integer getScGlCoaPayableAccount();
     /**
      * @ejb:interface-method view-type="local"
      **/
    public abstract void setScGlCoaPayableAccount(java.lang.Integer SC_GL_COA_PYBL_ACCNT);

    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="SC_GL_COA_EXPNS_ACCNT"
     **/
    public abstract java.lang.Integer getScGlCoaExpenseAccount();
     /**
      * @ejb:interface-method view-type="local"
      **/
    public abstract void setScGlCoaExpenseAccount(java.lang.Integer SC_GL_COA_EXPNS_ACCNT);

     /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="SC_INVSTR_BNS_RT"
     **/
    public abstract double getScInvestorBonusRate();
     /**
      * @ejb:interface-method view-type="local"
      **/
    public abstract void setScInvestorBonusRate(double SC_INVSTR_BNS_RT);


     /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="SC_INVSTR_INT_RT"
     **/
    public abstract double getScInvestorInterestRate();
     /**
      * @ejb:interface-method view-type="local"
      **/
    public abstract void setScInvestorInterestRate(double SC_INVSTR_INT_RT);


    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="SC_ENBL"
     **/
    public abstract byte getScEnable();
     /**
      * @ejb:interface-method view-type="local"
      **/
    public abstract void setScEnable(byte SC_ENBL);

    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="SC_LDGR"
     **/
    public abstract byte getScLedger();
     /**
      * @ejb:interface-method view-type="local"
      **/
    public abstract void setScLedger(byte SC_LDGR);

    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="SC_INVT"
     **/
    public abstract byte getScIsInvestment();
     /**
      * @ejb:interface-method view-type="local"
      **/
    public abstract void setScIsInvestment(byte SC_INVT);

    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="SC_LN"
     **/
    public abstract byte getScIsLoan();
     /**
      * @ejb:interface-method view-type="local"
      **/
    public abstract void setScIsLoan(byte SC_LN);

    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="SC_VT_RLF_VCHR_ITM"
     **/
    public abstract byte getScIsVatReliefVoucherItem();
     /**
      * @ejb:interface-method view-type="local"
      **/
    public abstract void setScIsVatReliefVoucherItem(byte SC_VT_RLF_VCHR_ITM);


    /**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="SC_LST_MDFD_BY"
	 **/
	public abstract String getScLastModifiedBy();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setScLastModifiedBy(String SC_LST_MDFD_BY);

	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="SC_DT_LST_MDFD"
	 **/
	public abstract Date getScDateLastModified();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setScDateLastModified(Date SC_DT_LST_MDFD);

    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="SC_AD_CMPNY"
     **/
    public abstract Integer getScAdCompany();
     /**
      * @ejb:interface-method view-type="local"
      **/
    public abstract void setScAdCompany(Integer SC_AD_CMPNY);


    // RELATIONSHIP METHODS

    /**
     * @ejb:interface-method view-type="local"
     * @ejb:relation name="aptaxcode-supplierclasses"
     *               role-name="supplierclass-has-one-aptaxcode"
     *
     * @jboss:relation related-pk-field="tcCode"
     *                 fk-column="AP_TAX_CODE"
     */
    public abstract LocalApTaxCode getApTaxCode();
    public abstract void setApTaxCode(LocalApTaxCode apTaxCode);

    /**
     * @ejb:interface-method view-type="local"
     * @ejb:relation name="withholdingtaxcode-supplierclasses"
     *               role-name="supplierclass-has-one-withholdingtaxcode"
     *
     * @jboss:relation related-pk-field="wtcCode"
     *                 fk-column="AP_WITHHOLDING_TAX_CODE"
     */
    public abstract LocalApWithholdingTaxCode getApWithholdingTaxCode();
    public abstract void setApWithholdingTaxCode(LocalApWithholdingTaxCode apWithholdingTaxCode);


    /**
     * @ejb:interface-method view-type="local"
     * @ejb:relation name="supplierclass-suppliers"
     *               role-name="supplierclass-has-many-suppliers"
     *
     */
    public abstract Collection getApSuppliers();
    public abstract void setApSuppliers(Collection apSuppliers);


    /**
     * @jboss:dynamic-ql
     */
     public abstract Collection ejbSelectGeneric(java.lang.String jbossQl, java.lang.Object[] args)
        throws FinderException;



    // BUSINESS METHODS

    /**
     * @ejb:home-method view-type="local"
     */
     public Collection ejbHomeGetScByCriteria(java.lang.String jbossQl, java.lang.Object[] args)
        throws FinderException {

        return ejbSelectGeneric(jbossQl, args);
     }

    /**
     * @ejb:interface-method view-type="local"
     **/
    public void addApSupplier(LocalApSupplier apSupplier) {

         Debug.print("ApSupplierClassBean addApSupplier");

         try {

            Collection apSuppliers = getApSuppliers();
	        apSuppliers.add(apSupplier);

         } catch (Exception ex) {

           throw new EJBException(ex.getMessage());

         }
     }

    /**
     * @ejb:interface-method view-type="local"
     **/
    public void dropApSupplier(LocalApSupplier apSupplier) {

         Debug.print("ApSupplierClassBean dropApSupplier");

         try {

            Collection apSuppliers = getApSuppliers();
	        apSuppliers.remove(apSupplier);

         } catch (Exception ex) {

           throw new EJBException(ex.getMessage());

         }
     }


    // ENTITY METHODS

    /**
     * @ejb:create-method view-type="local"
     **/
    public Integer ejbCreate(Integer SC_CODE,
         String SC_NM, String SC_DESC, Integer SC_GL_COA_PYBL_ACCNT, Integer SC_GL_COA_EXPNS_ACCNT,
         double SC_INVSTR_BNS_RT,
         double SC_INVSTR_INT_RT,
         byte SC_ENBL, byte SC_LDGR, byte SC_INVT, byte SC_LN, byte SC_VT_RLF_VCHR_ITM,
         String SC_LST_MDFD_BY,
         Date SC_DT_LST_MDFD,
         java.lang.Integer SC_AD_CMPNY)
         throws CreateException {

         Debug.print("ApSupplierClassBean ejbCreate");

	     setScCode(SC_CODE);
	     setScName(SC_NM);
	     setScDescription(SC_DESC);
	     setScGlCoaPayableAccount(SC_GL_COA_PYBL_ACCNT);
	     setScGlCoaExpenseAccount(SC_GL_COA_EXPNS_ACCNT);
	     setScInvestorBonusRate(SC_INVSTR_BNS_RT);
	     setScInvestorInterestRate(SC_INVSTR_INT_RT);
         setScEnable(SC_ENBL);
         setScLedger(SC_LDGR);
         setScIsInvestment(SC_INVT);
         setScIsLoan(SC_LN);
         setScIsVatReliefVoucherItem(SC_VT_RLF_VCHR_ITM);
         setScLastModifiedBy(SC_LST_MDFD_BY);
         setScDateLastModified(SC_DT_LST_MDFD);
         setScAdCompany(SC_AD_CMPNY);

         return null;
     }

    /**
     * @ejb:create-method view-type="local"
     **/
    public Integer ejbCreate(
         String SC_NM, String SC_DESC, Integer SC_GL_COA_PYBL_ACCNT, Integer SC_GL_COA_EXPNS_ACCNT,
         double SC_INVSTR_BNS_RT,
         double SC_INVSTR_INT_RT,
         byte SC_ENBL, byte SC_LDGR,byte SC_INVT, byte SC_LN, byte SC_VT_RLF_VCHR_ITM,
         String SC_LST_MDFD_BY,
         Date SC_DT_LST_MDFD,
         java.lang.Integer SC_AD_CMPNY)
         throws CreateException {

         Debug.print("ApSupplierClassBean ejbCreate");

	     setScName(SC_NM);
	     setScDescription(SC_DESC);
	     setScGlCoaPayableAccount(SC_GL_COA_PYBL_ACCNT);
	     setScGlCoaExpenseAccount(SC_GL_COA_EXPNS_ACCNT);
	     setScInvestorBonusRate(SC_INVSTR_BNS_RT);
	     setScInvestorInterestRate(SC_INVSTR_INT_RT);
         setScEnable(SC_ENBL);
         setScLedger(SC_LDGR);
         setScIsInvestment(SC_INVT);
         setScIsLoan(SC_LN);
         setScIsVatReliefVoucherItem(SC_VT_RLF_VCHR_ITM);
         setScLastModifiedBy(SC_LST_MDFD_BY);
         setScDateLastModified(SC_DT_LST_MDFD);
         setScAdCompany(SC_AD_CMPNY);

         return null;
     }


     public void ejbPostCreate(Integer SC_CODE,
         String SC_NM, String SC_DESC, Integer SC_GL_COA_PYBL_ACCNT, Integer SC_GL_COA_EXPNS_ACCNT,
         double SC_INVSTR_BNS_RT,
         double SC_INVSTR_INT_RT,
         byte SC_ENBL, byte SC_LDGR, byte SC_INVT, byte SC_LN, byte SC_VT_RLF_VCHR_ITM,
         String SC_LST_MDFD_BY,
         Date SC_DT_LST_MDFD,
         java.lang.Integer SC_AD_CMPNY)
         throws CreateException { }

     public void ejbPostCreate(
         String SC_NM, String SC_DESC, Integer SC_GL_COA_PYBL_ACCNT, Integer SC_GL_COA_EXPNS_ACCNT,
         double SC_INVSTR_BNS_RT,
         double SC_INVSTR_INT_RT,
         byte SC_ENBL, byte SC_LDGR, byte SC_INVT, byte SC_LN, byte SC_VT_RLF_VCHR_ITM,
         String SC_LST_MDFD_BY,
         Date SC_DT_LST_MDFD,
         java.lang.Integer SC_AD_CMPNY)
         throws CreateException { }


}