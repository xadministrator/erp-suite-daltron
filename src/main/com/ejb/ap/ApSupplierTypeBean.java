/*
 * com/ejb/ap/LocalApSupplierTypeBean.java
 *
 * Created on February 13, 2004, 3:44 PM
 */

package com.ejb.ap;

import java.util.Collection;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;

import com.util.AbstractEntityBean;
import com.util.Debug;

/**
 *
 * @author  Neil Andrew M. Ajero
 *
 * @ejb:bean name="ApSupplierTypeEJB"
 *           display-name="SupplierType Entity"
 *           type="CMP"
 *           cmp-version="2.x"
 *           view-type="local"
 *           local-jndi-name="omega-ejb/ApSupplierTypeEJB"
 *           schema="ApSupplierType"
 *           primkey-field="stCode"
 *
 * @ejb:pk class="java.lang.Integer"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="apuser"
 *                        role-link="apuserlink"
 *
 * @ejb:permission role-name="apuser"
 *
 * @ejb:interface local-class="com.ejb.ap.LocalApSupplierType"
 *                local-extends="javax.ejb.EJBLocalObject"
 *
 * @ejb:home local-class="com.ejb.ap.LocalApSupplierTypeHome"
 *           local-extends="javax.ejb.EJBLocalHome"
 *
 * @ejb:finder signature="Collection findEnabledStAll(java.lang.Integer ST_AD_CMPNY)"
 *             query="SELECT OBJECT(st) FROM ApSupplierType st WHERE st.stEnable = 1 AND st.stAdCompany = ?1"
 *
 * @jboss:query signature="Collection findEnabledStAll(java.lang.Integer ST_AD_CMPNY)"
 *             query="SELECT OBJECT(st) FROM ApSupplierType st WHERE st.stEnable = 1 AND st.stAdCompany = ?1 ORDER BY st.stName"
 *
 * @ejb:finder signature="LocalApSupplierType findByStName(java.lang.String ST_NM, java.lang.Integer ST_AD_CMPNY)"
 *             query="SELECT OBJECT(st) FROM ApSupplierType st WHERE st.stName = ?1 AND st.stAdCompany = ?2"
 *
 * @ejb:finder signature="Collection findStAll(java.lang.Integer ST_AD_CMPNY)"
 *             query="SELECT OBJECT(st) FROM ApSupplierType st WHERE st.stAdCompany = ?1"
 *
 * @jboss:query signature="Collection findStAll(java.lang.Integer ST_AD_CMPNY)"
 *             query="SELECT OBJECT(st) FROM ApSupplierType st WHERE st.stAdCompany = ?1 ORDER BY st.stName"
 * 
 * @ejb:select signature="Collection ejbSelectGeneric(java.lang.String jbossQl, java.lang.Object[] args)"
 *             result-type-mapping="Local"
 *             method-intf="LocalHome"
 *             query="SELECT OBJECT(st) FROM ApSupplierType st"
 *
 * @jboss:query signature="Collection ejbSelectGeneric(java.lang.String jbossQl, java.lang.Object[] args)"
 *                         dynamic="true"
 * 
 * @ejb:value-object match="*"
 *             name="ApSupplierType"
 *
 * @jboss:persistence table-name="AP_SPPLR_TYP"
 *
 * @jboss:entity-command name="mysql-get-generated-keys"
 */

public abstract class ApSupplierTypeBean extends AbstractEntityBean {
    
    
    // PERSISTENT METHODS
    
    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     * @ejb:pk-field
     *
     * @jboss:column-name name="ST_CODE"
     *
     * @jboss:persistence auto-increment="true"
     **/
    public abstract Integer getStCode();
    public abstract void setStCode(Integer ST_CODE);
    
    
    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"   
     *
     * @jboss:column-name name="ST_NM"
     **/
    public abstract String getStName();
     /**
      * @ejb:interface-method view-type="local"
      **/ 
    public abstract void setStName(String ST_NM);
    

    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"   
     *
     * @jboss:column-name name="ST_DESC"
     **/
    public abstract String getStDescription();
     /**
      * @ejb:interface-method view-type="local"
      **/ 
    public abstract void setStDescription(String ST_DESC);
    
    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="ST_ENBL"
     **/
    public abstract byte getStEnable();
     /**
      * @ejb:interface-method view-type="local"
      **/ 
    public abstract void setStEnable(byte ST_ENBL);
    
    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="ST_AD_CMPNY"
     **/
    public abstract Integer getStAdCompany();
     /**
      * @ejb:interface-method view-type="local"
      **/ 
    public abstract void setStAdCompany(Integer ST_AD_CMPNY);
    
   
    // RELATIONSHIP METHODS
    
    /**
     * @ejb:interface-method view-type="local"
     * @ejb:relation name="bankaccount-suppliertypes"
     *               role-name="suppliertype-has-one-bankaccount"
     * 
     * @jboss:relation related-pk-field="baCode"
     *                 fk-column="AD_BANK_ACCOUNT"
     */
    public abstract com.ejb.ad.LocalAdBankAccount getAdBankAccount();
    public abstract void setAdBankAccount(com.ejb.ad.LocalAdBankAccount adBankAccount);


    /**
     * @ejb:interface-method view-type="local"
     * @ejb:relation name="suppliertype-suppliers"
     *               role-name="suppliertype-has-many-suppliers"
     * 
     */
    public abstract Collection getApSuppliers();
    public abstract void setApSuppliers(Collection apSuppliers);    
     

    /**
     * @jboss:dynamic-ql
     */
     public abstract Collection ejbSelectGeneric(java.lang.String jbossQl, java.lang.Object[] args)
        throws FinderException;
        

    // BUSINESS METHODS
    
    /**
     * @ejb:home-method view-type="local"
     */
     public Collection ejbHomeGetStByCriteria(java.lang.String jbossQl, java.lang.Object[] args)
        throws FinderException {
        	
        return ejbSelectGeneric(jbossQl, args);
     }
     
    /**
     * @ejb:interface-method view-type="local"
     **/
    public void addApSupplier(LocalApSupplier apSupplier) {

         Debug.print("ApSupplierTypeBean addApSupplier");
      
         try {
      	
            Collection apSuppliers = getApSuppliers();
	        apSuppliers.add(apSupplier);
	          
         } catch (Exception ex) {
      	
           throw new EJBException(ex.getMessage());
      
         }   
     }

    /**
     * @ejb:interface-method view-type="local"
     **/
    public void dropApSupplier(LocalApSupplier apSupplier) {

         Debug.print("ApSupplierTypeBean dropApSupplier");
      
         try {
      	
            Collection apSuppliers = getApSuppliers();
	        apSuppliers.remove(apSupplier);
	      
         } catch (Exception ex) {
      	
           throw new EJBException(ex.getMessage());
         
         }   
     }
     

    // ENTITY METHODS
    
    /**
     * @ejb:create-method view-type="local"
     **/
    public Integer ejbCreate(Integer ST_CODE, 
         String ST_NM, String ST_DEST, byte ST_ENBL, Integer ST_AD_CMPNY)     	
         throws CreateException {	
     	
         Debug.print("ApSupplierTypeBean ejbCreate");
     
	     setStCode(ST_CODE);
	     setStName(ST_NM);
	     setStDescription(ST_DEST);   
         setStEnable(ST_ENBL);
         setStAdCompany(ST_AD_CMPNY);

         return null;
     }
     
    /**
     * @ejb:create-method view-type="local"
     **/
    public Integer ejbCreate(
         String ST_NM, String ST_DEST, byte ST_ENBL, Integer ST_AD_CMPNY)     	
         throws CreateException {	
     	
         Debug.print("ApSupplierTypeBean ejbCreate");
     
	     setStName(ST_NM);
	     setStDescription(ST_DEST);   
         setStEnable(ST_ENBL);
         setStAdCompany(ST_AD_CMPNY);

         return null;
     }
     
     public void ejbPostCreate(Integer ST_CODE, 
         String ST_NM, String ST_DEST, byte ST_ENBL, Integer ST_AD_CMPNY)      
         throws CreateException { }
      
     public void ejbPostCreate(
         String ST_NM, String ST_DEST, byte ST_ENBL, Integer ST_AD_CMPNY)
         throws CreateException { }     
}