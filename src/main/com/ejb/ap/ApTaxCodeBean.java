/*
 * com/ejb/ap/LocalApTaxCodeBean.java
 *
 * Created on February 13, 2004, 3:44 PM
 */

package com.ejb.ap;

import java.util.Collection;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;
import com.ejb.ad.LocalAdBranchApTaxCode;
import com.util.AbstractEntityBean;
import com.util.Debug;

/**
 *
 * @author  Neil Andrew M. Ajero
 *
 * @ejb:bean name="ApTaxCodeEJB"
 *           display-name="TaxCode Entity"
 *           type="CMP"
 *           cmp-version="2.x"
 *           view-type="local"
 *           local-jndi-name="omega-ejb/ApTaxCodeEJB"
 *           schema="ApTaxCode"
 *           primkey-field="tcCode"
 *
 * @ejb:pk class="java.lang.Integer"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="apuser"
 *                        role-link="apuserlink"
 *
 * @ejb:permission role-name="apuser"
 *
 * @ejb:interface local-class="com.ejb.ap.LocalApTaxCode"
 *                local-extends="javax.ejb.EJBLocalObject"
 *
 * @ejb:home local-class="com.ejb.ap.LocalApTaxCodeHome"
 *           local-extends="javax.ejb.EJBLocalHome"
 *
 * @ejb:finder signature="Collection findTcAll(java.lang.Integer TC_AD_CMPNY)"
 *             query="SELECT OBJECT(tc) FROM ApTaxCode tc WHERE tc.tcAdCompany = ?1"
 *
 * @jboss:query signature="Collection findTcAll(java.lang.Integer TC_AD_CMPNY)"
 *             query="SELECT OBJECT(tc) FROM ApTaxCode tc WHERE tc.tcAdCompany = ?1 ORDER BY tc.tcName"
 * 
 * @ejb:finder signature="Collection findNoneTc(java.lang.Integer TC_AD_CMPNY)"
 *             query="SELECT OBJECT(tc) FROM ApTaxCode tc WHERE tc.tcType='NONE' AND tc.tcAdCompany = ?1"
 *
 * @ejb:finder signature="Collection findEnabledTcAll(java.lang.Integer TC_AD_CMPNY)"
 *             query="SELECT OBJECT(tc) FROM ApTaxCode tc WHERE tc.tcEnable = 1 AND tc.tcAdCompany = ?1"
 *
 * @jboss:query signature="Collection findEnabledTcAll(java.lang.Integer TC_AD_CMPNY)"
 *             query="SELECT OBJECT(tc) FROM ApTaxCode tc WHERE tc.tcEnable = 1 AND tc.tcAdCompany = ?1 ORDER BY tc.tcName"
 *
 * @ejb:finder signature="LocalApTaxCode findByTcName(java.lang.String TC_NM, java.lang.Integer TC_AD_CMPNY)"
 *             query="SELECT OBJECT(tc) FROM ApTaxCode tc WHERE tc.tcName = ?1 AND tc.tcAdCompany = ?2"
 * 
 * @ejb:select signature="Collection ejbSelectGeneric(java.lang.String jbossQl, java.lang.Object[] args)"
 *             result-type-mapping="Local"
 *             method-intf="LocalHome"
 *             query="SELECT OBJECT(tc) FROM ApTaxCode tc"
 *
 * @jboss:query signature="Collection ejbSelectGeneric(java.lang.String jbossQl, java.lang.Object[] args)"
 *                         dynamic="true"
 *
 * @ejb:value-object match="*"
 *             name="ApTaxCode"
 *
 * @jboss:persistence table-name="AP_TX_CD"
 *
 * @jboss:entity-command name="mysql-get-generated-keys"
 */

public abstract class ApTaxCodeBean extends AbstractEntityBean {
    
    
    // PERSITCENT METHODS
    
    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     * @ejb:pk-field
     *
     * @jboss:column-name name="TC_CODE"
     *
     * @jboss:persistence auto-increment="true"
     **/
    public abstract Integer getTcCode();
    public abstract void setTcCode(Integer TC_CODE);
    
    
    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"   
     *
     * @jboss:column-name name="TC_NM"
     **/
    public abstract String getTcName();
     /**
      * @ejb:interface-method view-type="local"
      **/ 
    public abstract void setTcName(String TC_NM);
    

    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"   
     *
     * @jboss:column-name name="TC_DESC"
     **/
    public abstract String getTcDescription();
     /**
      * @ejb:interface-method view-type="local"
      **/ 
    public abstract void setTcDescription(String TC_DESC);


    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"   
     *
     * @jboss:column-name name="TC_TYP"
     **/
    public abstract String getTcType();
     /**
      * @ejb:interface-method view-type="local"
      **/ 
    public abstract void setTcType(String TC_TYP);


    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"   
     *
     * @jboss:column-name name="TC_RT"
     **/
    public abstract double getTcRate();
     /**
      * @ejb:interface-method view-type="local"
      **/ 
    public abstract void setTcRate(double TC_RT);
    
    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="TC_ENBL"
     **/
    public abstract byte getTcEnable();
     /**
      * @ejb:interface-method view-type="local"
      **/ 
    public abstract void setTcEnable(byte TC_ENBL);
    
    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="TC_AD_CMPNY"
     **/
    public abstract Integer getTcAdCompany();
     /**
      * @ejb:interface-method view-type="local"
      **/ 
    public abstract void setTcAdCompany(Integer TC_AD_CMPNY);
    
   
    // RELATIONSHIP METHODS
    
    /**
     * @ejb:interface-method view-type="local"
     * @ejb:relation name="chartofaccount-aptaxcodes"
     *               role-name="aptaxcode-has-one-chartofaccount"
     * 
     * @jboss:relation related-pk-field="coaCode"
     *                 fk-column="GL_CHART_OF_ACCOUNT"
     */
    public abstract com.ejb.gl.LocalGlChartOfAccount getGlChartOfAccount();
    public abstract void setGlChartOfAccount(com.ejb.gl.LocalGlChartOfAccount glChartOfAccount);


    /**
     * @ejb:interface-method view-type="local"
     * @ejb:relation name="aptaxcode-supplierclasses"
     *               role-name="aptaxcode-has-many-supplierclasses"
     * 
     */
    public abstract Collection getApSupplierClasses();
    public abstract void setApSupplierClasses(Collection apSupplierClasses);    


    /**
     * @ejb:interface-method view-type="local"
     * @ejb:relation name="aptaxcode-vouchers"
     *               role-name="aptaxcode-has-many-vouchers"
     * 
     */
    public abstract Collection getApVouchers();
    public abstract void setApVouchers(Collection apVouchers); 

    /**
     * @ejb:interface-method view-type="local"
     * @ejb:relation name="aptaxcode-checks"
     *               role-name="aptaxcode-has-many-checks"
     * 
     */
    public abstract Collection getApChecks();
    public abstract void setApChecks(Collection apChecks); 

    /**
     * @ejb:interface-method view-type="local"
     * @ejb:relation name="aptaxcode-recurringvouchers"
     *               role-name="aptaxcode-has-many-recurringvouchers"
     * 
     */
    public abstract Collection getApRecurringVouchers();
    public abstract void setApRecurringVouchers(Collection apRecurringVouchers);
    
    /**
     * @ejb:interface-method view-type="local"
     * @ejb:relation name="aptaxcode-purchaseorders"
     *               role-name="aptaxcode-has-many-purchaseorders"
     * 
     */
    public abstract Collection getApPurchaseOrders();
    public abstract void setApPurchaseOrders(Collection apPurchaseOrders);
    
    /**
     * @ejb:interface-method view-type="local"
     * @ejb:relation name="aptaxcode-purchaserequisitions"
     *               role-name="aptaxcode-has-many-purchaserequisitions"
     * 
     */
    public abstract Collection getApPurchaseRequisitions();
    public abstract void setApPurchaseRequisitions(Collection apPurchaseRequisitions);
    
    /**
     * @ejb:interface-method view-type="local"
     * @ejb:relation name="aptaxcode-branchaptaxcode"
     *               role-name="aptaxcode-has-many-branchaptaxcode"
     */
    public abstract Collection getAdBranchApTaxCodes();
    public abstract void setAdBranchApTaxCodes(Collection adBranchApTaxCodes);
    
    /**
     * @jboss:dynamic-ql
     */
     public abstract Collection ejbSelectGeneric(java.lang.String jbossQl, java.lang.Object[] args)
        throws FinderException;
        
     

    // BUSINESS METHODS
    
    /**
     * @ejb:home-method view-type="local"
     */
     public Collection ejbHomeGetTcByCriteria(java.lang.String jbossQl, java.lang.Object[] args)
        throws FinderException {
        	
        return ejbSelectGeneric(jbossQl, args);
     }
     
    /**
     * @ejb:interface-method view-type="local"
     **/
    public void addApSupplierClass(LocalApSupplierClass apSupplierClass) {

         Debug.print("ApTaxCodeBean addApSupplierClass");
      
         try {
      	
            Collection apSupplierClasses = getApSupplierClasses();
	        apSupplierClasses.add(apSupplierClass);
	          
         } catch (Exception ex) {
      	
           throw new EJBException(ex.getMessage());
      
         }   
     }

    /**
     * @ejb:interface-method view-type="local"
     **/
    public void dropApSupplierClass(LocalApSupplierClass apSupplierClass) {

         Debug.print("ApTaxCodeBean dropApSupplierClass");
      
         try {
      	
            Collection apSupplierClasses = getApSupplierClasses();
	        apSupplierClasses.remove(apSupplierClass);
	      
         } catch (Exception ex) {
      	
           throw new EJBException(ex.getMessage());
         
         }   
     }
     

   /**
     * @ejb:interface-method view-type="local"
     **/
    public void addApVoucher(LocalApVoucher apVoucher) {

         Debug.print("ApTaxCodeBean addApVoucher");
      
         try {
      	
            Collection apVouchers = getApVouchers();
	        apVouchers.add(apVoucher);
	          
         } catch (Exception ex) {
      	
           throw new EJBException(ex.getMessage());
      
         }   
     }

    /**
     * @ejb:interface-method view-type="local"
     **/
    public void dropApVoucher(LocalApVoucher apVoucher) {

         Debug.print("ApTaxCodeBean dropApVoucher");
      
         try {
      	
            Collection apVouchers = getApVouchers();
	        apVouchers.remove(apVoucher);
	      
         } catch (Exception ex) {
      	
           throw new EJBException(ex.getMessage());
         
         }   
     }
     
    /**
     * @ejb:interface-method view-type="local"
     **/
    public void addApCheck(LocalApCheck apCheck) {

         Debug.print("ApTaxCodeBean addApCheck");
      
         try {
      	
            Collection apChecks = getApChecks();
	        apChecks.add(apCheck);
	          
         } catch (Exception ex) {
      	
           throw new EJBException(ex.getMessage());
      
         }   
     }

    /**
     * @ejb:interface-method view-type="local"
     **/
    public void dropApCheck(LocalApCheck apCheck) {

         Debug.print("ApTaxCodeBean dropApCheck");
      
         try {
      	
            Collection apChecks = getApChecks();
	        apChecks.remove(apCheck);
	      
         } catch (Exception ex) {
      	
           throw new EJBException(ex.getMessage());
         
         } 
           
     }
     
    /**
     * @ejb:interface-method view-type="local"
     **/
    public void addApRecurringVoucher(LocalApRecurringVoucher apRecurringVoucher) {

         Debug.print("ApTaxCodeBean addApRecurringVoucher");
      
         try {
      	
            Collection apRecurringVouchers = getApRecurringVouchers();
	        apRecurringVouchers.add(apRecurringVoucher);
	          
         } catch (Exception ex) {
      	
           throw new EJBException(ex.getMessage());
      
         }   
     }

    /**
     * @ejb:interface-method view-type="local"
     **/
    public void dropApRecurringVoucher(LocalApRecurringVoucher apRecurringVoucher) {

         Debug.print("ApTaxCodeBean dropApRecurringVoucher");
      
         try {
      	
            Collection apRecurringVouchers = getApRecurringVouchers();
	        apRecurringVouchers.remove(apRecurringVoucher);
	      
         } catch (Exception ex) {
      	
           throw new EJBException(ex.getMessage());
         
         }   
     }
    
    /**
     * @ejb:interface-method view-type="local"
     **/
    public void addApPurchaseOrder(LocalApPurchaseOrder apPurchaseOrder) {

         Debug.print("ApTaxCodeBean addApPurchaseOrder");
      
         try {
      	
            Collection apPurchaseOrders = getApPurchaseOrders();
	        apPurchaseOrders.add(apPurchaseOrder);
	          
         } catch (Exception ex) {
      	
           throw new EJBException(ex.getMessage());
      
         }   
     }

    /**
     * @ejb:interface-method view-type="local"
     **/
    public void dropApPurchaseOrder(LocalApPurchaseOrder apPurchaseOrder) {

         Debug.print("ApTaxCodeBean dropApPurchaseOrder");
      
         try {
      	
            Collection apPurchaseOrders = getApPurchaseOrders();
	        apPurchaseOrders.remove(apPurchaseOrder);
	      
         } catch (Exception ex) {
      	
           throw new EJBException(ex.getMessage());
         
         }   
     }
    
    /**
     * @ejb:interface-method view-type="local"
     **/
    public void addApPurchaseRequisition(LocalApPurchaseRequisition apPurchaseRequisition) {

         Debug.print("ApTaxCodeBean addApPurchaseRequisition");
      
         try {
      	
            Collection apPurchaseRequisitions = getApPurchaseRequisitions();
	        apPurchaseRequisitions.add(apPurchaseRequisition);
	          
         } catch (Exception ex) {
      	
           throw new EJBException(ex.getMessage());
      
         }   
     }

    /**
     * @ejb:interface-method view-type="local"
     **/
    public void dropApPurchaseRequisition(LocalApPurchaseRequisition apPurchaseRequisition) {

         Debug.print("ApTaxCodeBean dropApPurchaseRequisition");
      
         try {
      	
            Collection apPurchaseRequisitions = getApPurchaseRequisitions();
	        apPurchaseRequisitions.remove(apPurchaseRequisition);
	      
         } catch (Exception ex) {
      	
           throw new EJBException(ex.getMessage());
         
         }   
     }
    
    
    /**
     * @ejb:interface-method view-type="local"
     **/
    public void addAdBranchApTaxCode(LocalAdBranchApTaxCode adBranchApTaxCode) {

         Debug.print("ApTaxCodeBean addAdBranchApTaxCode");
   
         try {
   	
	        Collection adBranchApTaxCodes = getAdBranchApTaxCodes();

	        adBranchApTaxCodes.add(adBranchApTaxCode);
	        
         } catch (Exception ex) {
   	
             throw new EJBException(ex.getMessage());
      
         }
       
    }
 	
    /**
     * @ejb:interface-method view-type="local"
     **/ 
     public void dropAdBranchApTaxCode(LocalAdBranchApTaxCode adBranchApTaxCode) {
 		
 		  Debug.print("ApTaxCodeBean dropAdBranchApTaxCode");
 		  
 		  try {
 		  	
 		        Collection adBranchApTaxCodes = getAdBranchApTaxCodes();

 		        adBranchApTaxCodes.remove(adBranchApTaxCode);

 			     
 		  } catch (Exception ex) {
 		  	
 		     throw new EJBException(ex.getMessage());
 		     
 		  }
 		  
    }
     
    // ENTITY METHODS
    
    /**
     * @ejb:create-method view-type="local"
     **/
    public Integer ejbCreate(Integer TC_CODE, 
         String TC_NM, String TC_DESC, String TC_TYP, 
         double TC_RT, byte TC_ENBL, Integer TC_AD_CMPNY)     	
         throws CreateException {	
     	
         Debug.print("ApTaxCodeBean ejbCreate");
     
	     setTcCode(TC_CODE);
	     setTcName(TC_NM);
	     setTcDescription(TC_DESC); 
	     setTcType(TC_TYP);
	     setTcRate(TC_RT);  
         setTcEnable(TC_ENBL);	   
         setTcAdCompany(TC_AD_CMPNY);

         return null;
     }
     
    /**
     * @ejb:create-method view-type="local"
     **/
    public Integer ejbCreate(
         String TC_NM, String TC_DESC, String TC_TYP, 
         double TC_RT, byte TC_ENBL, Integer TC_AD_CMPNY)     	
         throws CreateException {	
     	
         Debug.print("ApTaxCodeBean ejbCreate");
     
	     setTcName(TC_NM);
	     setTcDescription(TC_DESC); 
	     setTcType(TC_TYP);
	     setTcRate(TC_RT);  
         setTcEnable(TC_ENBL);
         setTcAdCompany(TC_AD_CMPNY);

         return null;
     }
     
     public void ejbPostCreate(Integer TC_CODE, 
         String TC_NM, String TC_DESC, String TC_TYP, 
         double TC_RT, byte TC_ENBL, Integer TC_AD_CMPNY)      
         throws CreateException { }
      
     public void ejbPostCreate(
         String TC_NM, String TC_DESC, String TC_TYP, 
         double TC_RT, byte TC_ENBL, Integer TC_AD_CMPNY)
         throws CreateException { }     
}