package com.ejb.ap;

import java.util.Collection;
import java.util.Date;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;

import com.util.AbstractEntityBean;
import com.util.Debug;

/**
 * @ejb:bean name="ApVoucherBatchEJB"
 *           display-name="Voucher Batch Entity"
 *           type="CMP"
 *           cmp-version="2.x"
 *           view-type="local"
 *           local-jndi-name="omega-ejb/ApVoucherBatchEJB"
 *           schema="ApVoucherBatch"
 *           primkey-field="vbCode"
 *       
 * @ejb:pk class="Integer"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="apuser"
 *                        role-link="apuserlink"
 *
 * @ejb:permission role-name="apuser"
 *
 * @ejb:interface local-class="com.ejb.ap.LocalApVoucherBatch"
 *                local-extends="javax.ejb.EJBLocalObject"
 *
 * @ejb:home local-class="com.ejb.ap.LocalApVoucherBatchHome"
 *           local-extends="javax.ejb.EJBLocalHome"
 *
 * @ejb:finder signature="LocalApVoucherBatch findByVbName(java.lang.String VB_NM, java.lang.Integer VB_AD_BRNCH, java.lang.Integer VB_AD_CMPNY)"
 *             query="SELECT OBJECT(vb) FROM ApVoucherBatch vb WHERE vb.vbName=?1 AND vb.vbAdBranch=?2 AND vb.vbAdCompany=?3"
 *
 * @ejb:finder signature="LocalApVoucherBatch findVoucherByVbName(java.lang.String VB_NM, java.lang.Integer VB_AD_BRNCH, java.lang.Integer VB_AD_CMPNY)"
 *             query="SELECT OBJECT(vb) FROM ApVoucherBatch vb WHERE vb.vbType='VOUCHER' AND vb.vbName=?1 AND vb.vbAdBranch=?2 AND vb.vbAdCompany=?3"
 *
 * @ejb:finder signature="LocalApVoucherBatch findDebitMemoByVbName(java.lang.String VB_NM, java.lang.Integer VB_AD_BRNCH, java.lang.Integer VB_AD_CMPNY)"
 *             query="SELECT OBJECT(vb) FROM ApVoucherBatch vb WHERE vb.vbType='DEBIT MEMO' AND vb.vbName=?1 AND vb.vbAdBranch=?2 AND vb.vbAdCompany=?3"
 *
 * @ejb:finder signature="Collection findOpenVbByVbType(java.lang.String VB_TYP, java.lang.Integer VB_AD_BRNCH, java.lang.Integer VB_AD_CMPNY)"
 *             query="SELECT OBJECT(vb) FROM ApVoucherBatch vb WHERE vb.vbType=?1 AND vb.vbStatus='OPEN' AND vb.vbAdBranch=?2 AND vb.vbAdCompany=?3"
 *
 * @jboss:query signature="Collection findOpenVbByVbType(java.lang.String VB_TYP, java.lang.Integer VB_AD_BRNCH, java.lang.Integer VB_AD_CMPNY)"
 *             query="SELECT OBJECT(vb) FROM ApVoucherBatch vb WHERE vb.vbType=?1 AND vb.vbStatus='OPEN' AND vb.vbAdBranch=?2 AND vb.vbAdCompany=?3 ORDER BY vb.vbName"
 *
 * @ejb:finder signature="Collection findOpenVbByVbTypeDepartment(java.lang.String VB_TYP, java.lang.String VB_DPRTMNT, java.lang.Integer VB_AD_BRNCH, java.lang.Integer VB_AD_CMPNY)"
 *             query="SELECT OBJECT(vb) FROM ApVoucherBatch vb WHERE vb.vbType=?1 AND vb.vbDepartment=?2 AND vb.vbStatus='OPEN' AND vb.vbAdBranch=?3 AND vb.vbAdCompany=?4"
 *
 * @jboss:query signature="Collection findOpenVbByVbTypeDepartment(java.lang.String VB_TYP, java.lang.String VB_DPRTMNT, java.lang.Integer VB_AD_BRNCH, java.lang.Integer VB_AD_CMPNY)"
 *             query="SELECT OBJECT(vb) FROM ApVoucherBatch vb WHERE vb.vbType=?1 AND vb.vbDepartment=?2 AND vb.vbStatus='OPEN' AND vb.vbAdBranch=?3 AND vb.vbAdCompany=?4 ORDER BY vb.vbName"
 *
 * @ejb:finder signature="Collection findOpenVbAll(java.lang.Integer VB_AD_BRNCH, java.lang.Integer VB_AD_CMPNY)"
 *             query="SELECT OBJECT(vb) FROM ApVoucherBatch vb WHERE vb.vbStatus='OPEN' AND vb.vbAdBranch = ?1 AND vb.vbAdCompany=?2"
 * 
 * @jboss:query signature="Collection findOpenVbAll(java.lang.Integer VB_AD_BRNCH, java.lang.Integer VB_AD_CMPNY)"
 *             query="SELECT OBJECT(vb) FROM ApVoucherBatch vb WHERE vb.vbStatus='OPEN' AND vb.vbAdBranch = ?1 AND vb.vbAdCompany=?2 ORDER BY vb.vbName" 
 *
 * @ejb:select signature="Collection ejbSelectGeneric(java.lang.String jbossQl, java.lang.Object[] args)"
 *             result-type-mapping="Local"
 *             method-intf="LocalHome"
 *             query="SELECT OBJECT(vb) FROM ApVoucherBatch vb"
 *
 * @jboss:query signature="Collection ejbSelectGeneric(java.lang.String jbossQl, java.lang.Object[] args)"
 *                         dynamic="true"
 *
 * @ejb:value-object match="*"
 *             name="ApVoucherBatch"
 *
 * @jboss:persistence table-name="AP_VCHR_BTCH"
 *
 * @jboss:entity-command name="mysql-get-generated-keys"
 *
 */

public abstract class ApVoucherBatchBean extends AbstractEntityBean {


   // Access methods for persistent fields

   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    * @ejb:pk-field
    *
    * @jboss:column-name name="VB_CODE"
    *
    * @jboss:persistence auto-increment="true"
    **/
   public abstract Integer getVbCode();
   public abstract void setVbCode(Integer VB_CODE);

   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="VB_NM"
    **/
   public abstract String getVbName();
   /**
    * @ejb:interface-method view-type="local"
    **/
   public abstract void setVbName(String VB_NM);

   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="VB_DESC"
    **/
   public abstract String getVbDescription();
   /**
    * @ejb:interface-method view-type="local"
    **/
   public abstract void setVbDescription(String VB_DESC);
   
   
   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="VB_STATUS"
    **/
   public abstract String getVbStatus();
   /**
    * @ejb:interface-method view-type="local"
    **/
   public abstract void setVbStatus(String VB_STATUS);
   
   
   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="VB_TYP"
    **/
   public abstract String getVbType();
   /**
    * @ejb:interface-method view-type="local"
    **/
   public abstract void setVbType(String VB_TYP);

   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="VB_DT_CRTD"
    **/
   public abstract Date getVbDateCreated();
   /**
    * @ejb:interface-method view-type="local"
    **/
   public abstract void setVbDateCreated(Date VB_DT_CRTD);
   
   
   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="VB_CRTD_BY"
    **/
   public abstract String getVbCreatedBy();
   /**
    * @ejb:interface-method view-type="local"
    **/
   public abstract void setVbCreatedBy(String VB_CRTD_BY);
   
   
   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="VB_DPRTMNT"
    **/
   public abstract String getVbDepartment();
   /**
    * @ejb:interface-method view-type="local"
    **/
   public abstract void setVbDepartment(String VB_DPRTMNT);
   
   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="VB_AD_BRNCH"
    **/
   public abstract Integer getVbAdBranch();
   /**
    * @ejb:interface-method view-type="local"
    **/
   public abstract void setVbAdBranch(Integer VB_AD_BRNCH);

   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="VB_AD_CMPNY"
    **/
   public abstract Integer getVbAdCompany();
   /**
    * @ejb:interface-method view-type="local"
    **/
   public abstract void setVbAdCompany(Integer VB_AD_CMPNY);
   
   
   // Access methods for relationship fields

   /**
    * @ejb:interface-method view-type="local"
    * @ejb:relation name="voucherbatch-vouchers"
    *               role-name="voucherbatch-has-many-vouchers"
    */
   public abstract Collection getApVouchers();
   public abstract void setApVouchers(Collection apVouchers);
   
    /**
    * @ejb:interface-method view-type="local"
    * @ejb:relation name="voucherbatch-purchaseorders"
    *               role-name="voucherbatch-has-many-purchaseorders"
    */
   public abstract Collection getApPurchaseOrders();
   public abstract void setApPurchaseOrders(Collection apPurchaseOrders);
   

   /**
    * @ejb:interface-method view-type="local"
    * @ejb:relation name="voucherbatch-recurringvoucher"
    *               role-name="voucherbatch-has-many-recurringvoucher"
    */
   public abstract Collection getApRecurringVouchers();
   public abstract void setApRecurringVouchers(Collection apRecurringVouchers);
   
  
  /**
    * @jboss:dynamic-ql
    */
    public abstract Collection ejbSelectGeneric(java.lang.String jbossQl, java.lang.Object[] args)
       throws FinderException;

   // Business methods
   
   /**
    * @ejb:home-method view-type="local"
    */
    public Collection ejbHomeGetVbByCriteria(java.lang.String jbossQl, java.lang.Object[] args)
       throws FinderException {
       	
       return ejbSelectGeneric(jbossQl, args);
    }
      
   
   /**
    * @ejb:interface-method view-type="local"
    **/
   public void addApVoucher(LocalApVoucher apVoucher) {

      Debug.print("ApVoucherBatchBean addApVoucher");
      try {
         Collection apVouchers = getApVouchers();
	 apVouchers.add(apVoucher);
      } catch (Exception ex) {
         throw new EJBException(ex.getMessage());
      }
   }

   /**
    * @ejb:interface-method view-type="local"
    **/
   public void dropApVoucher(LocalApVoucher apVoucher) {

      Debug.print("ApVoucherBatchBean dropApVoucher");
      try {
         Collection apVouchers = getApVouchers();
	     apVouchers.remove(apVoucher);
      } catch (Exception ex) {
         throw new EJBException(ex.getMessage());
      }
   }
   
   /**
    * @ejb:interface-method view-type="local"
    **/
   public void addApRecurringVoucher(LocalApRecurringVoucher apRecurringVoucher) {

      Debug.print("ApVoucherBatchBean addApRecurringVoucher");
      try {
         Collection apRecurringVouchers = getApRecurringVouchers();
	 apRecurringVouchers.add(apRecurringVoucher);
      } catch (Exception ex) {
         throw new EJBException(ex.getMessage());
      }
   }

   /**
    * @ejb:interface-method view-type="local"
    **/
   public void dropApRecurringVoucher(LocalApRecurringVoucher apRecurringVoucher) {

      Debug.print("ApVoucherBatchBean dropApRecurringVoucher");
      try {
         Collection apRecurringVouchers = getApRecurringVouchers();
	     apRecurringVouchers.remove(apRecurringVoucher);
      } catch (Exception ex) {
         throw new EJBException(ex.getMessage());
      }
   }
   
   // EntityBean methods

   /**
    * @ejb:create-method view-type="local"
    **/
   public Integer ejbCreate (Integer VB_CODE, String VB_NM, String VB_DESC,
      String VB_STATUS, String VB_TYP, Date VB_DT_CRTD, String VB_CRTD_BY, String VB_DPRTMNT,
	  Integer VB_AD_BRNCH, Integer VB_AD_CMPNY)
      throws CreateException {

      Debug.print("ApVoucherBatchBean ejbCreate");
      setVbCode(VB_CODE);
      setVbName(VB_NM);
      setVbDescription(VB_DESC);
      setVbStatus(VB_STATUS);
      setVbType(VB_TYP);
      setVbDateCreated(VB_DT_CRTD);
      setVbCreatedBy(VB_CRTD_BY);
      setVbDepartment(VB_DPRTMNT);
      setVbAdBranch(VB_AD_BRNCH);
      setVbAdCompany(VB_AD_CMPNY);
      
      return null;
   }

   /**
    * @ejb:create-method view-type="local"
    **/
   public Integer ejbCreate (String VB_NM, String VB_DESC,
      String VB_STATUS, String VB_TYP, Date VB_DT_CRTD, String VB_CRTD_BY, String VB_DPRTMNT,
	  Integer VB_AD_BRNCH, Integer VB_AD_CMPNY)
      throws CreateException {

      Debug.print("ApVoucherBatchBean ejbCreate");

      setVbName(VB_NM);
      setVbDescription(VB_DESC);
      setVbStatus(VB_STATUS);
      setVbType(VB_TYP);
      setVbDateCreated(VB_DT_CRTD);
      setVbCreatedBy(VB_CRTD_BY);
      setVbDepartment(VB_DPRTMNT);
      setVbAdBranch(VB_AD_BRNCH);
      setVbAdCompany(VB_AD_CMPNY);
      
      return null;
   }

   public void ejbPostCreate (Integer VB_CODE, String VB_NM, String VB_DESC,
      String VB_STATUS, String VB_TYP, Date VB_DT_CRTD, String VB_CRTD_BY, String VB_DPRTMNT,
	  Integer VB_AD_BRNCH, Integer VB_AD_CMPNY)
      throws CreateException { }

   public void ejbPostCreate (String VB_NM, String VB_DESC,
      String VB_STATUS, String VB_TYP, Date VB_DT_CRTD, String VB_CRTD_BY, String VB_DPRTMNT,
	  Integer VB_AD_BRNCH, Integer VB_AD_CMPNY)
      throws CreateException { }

} // ApVoucherBatchBean class
