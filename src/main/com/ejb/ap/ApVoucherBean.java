/*
 * com/ejb/ap/LocalApVoucherBean.java
 *
 * Created on February 13, 2004, 06:18 PM
 */

package com.ejb.ap;

import java.util.Collection;
import java.util.Date;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;

import com.util.AbstractEntityBean;
import com.util.Debug;

/**
 *
 * @author  Neil Andrew M.Ajero
 *
 * @ejb:bean name="ApVoucherEJB"
 *           display-name="Voucher Entity"
 *           type="CMP"
 *           cmp-version="2.x"
 *           view-type="local"
 *           local-jndi-name="omega-ejb/ApVoucherEJB"
 *           schema="ApVoucher"
 *           primkey-field="vouCode"
 *
 * @ejb:pk class="java.lang.Integer"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="apuser"
 *                        role-link="apuserlink"
 *
 * @ejb:permission role-name="apuser"
 *
 * @ejb:interface local-class="com.ejb.ap.LocalApVoucher"
 *                local-extends="javax.ejb.EJBLocalObject"
 *
 * @ejb:home local-class="com.ejb.ap.LocalApVoucherHome"
 *           local-extends="javax.ejb.EJBLocalHome"
 * 
 * @ejb:finder signature="LocalApVoucher findByVouDocumentNumberAndVouDebitMemoAndBrCode(java.lang.String VOU_DCMNT_NMBR, byte VOU_DBT_MMO, java.lang.Integer VOU_AD_BRNCH, java.lang.Integer VOU_AD_CMPNY)"
 *             query="SELECT OBJECT(vou) FROM ApVoucher vou  WHERE vou.vouDocumentNumber = ?1 AND vou.vouDebitMemo = ?2 AND vou.vouAdBranch = ?3 AND vou.vouAdCompany = ?4"
 *
 * @ejb:finder signature="LocalApVoucher findByVouDocumentNumberAndVouDebitMemoAndSplSupplierCodeBrCode(java.lang.String VOU_DCMNT_NMBR, byte VOU_DBT_MMO, java.lang.String SPL_SPPLR_CODE, java.lang.Integer VOU_AD_BRNCH, java.lang.Integer VOU_AD_CMPNY)"
 *             query="SELECT OBJECT(vou) FROM ApVoucher vou  WHERE vou.vouDocumentNumber = ?1 AND vou.vouDebitMemo = ?2 AND vou.apSupplier.splSupplierCode = ?3 AND vou.vouAdBranch = ?4 AND vou.vouAdCompany = ?5"
 *
 * @ejb:finder signature="LocalApVoucher findByVouDocumentNumber(java.lang.String VOU_DCMNT_NMBR, java.lang.Integer VOU_AD_CMPNY)"
 *             query="SELECT OBJECT(vou) FROM ApVoucher vou  WHERE vou.vouDocumentNumber = ?1 AND vou.vouAdCompany = ?2"
 *
 * @ejb:finder signature="Collection findPostedVouByVouDebitMemoAndVouDateRange(byte VOU_DBT_MMO, java.util.Date VOU_DT_FRM, java.util.Date VOU_DT_TO, java.lang.Integer VOU_AD_CMPNY)"
 *             query="SELECT OBJECT(vou) FROM ApVoucher vou  WHERE vou.vouPosted = 1 AND vou.vouDebitMemo = ?1 AND vou.vouDate >= ?2 AND vou.vouDate <= ?3 AND vou.vouAdCompany = ?4"
 *
 * @jboss:query signature="Collection findPostedVouByVouDebitMemoAndVouDateRange(byte VOU_DBT_MMO, java.util.Date VOU_DT_FRM, java.util.Date VOU_DT_TO, java.lang.Integer VOU_AD_CMPNY)"
 *             query="SELECT OBJECT(vou) FROM ApVoucher vou  WHERE vou.vouPosted = 1 AND vou.vouDebitMemo = ?1 AND vou.vouDate >= ?2 AND vou.vouDate <= ?3 AND vou.vouAdCompany = ?4 ORDER BY vou.vouDate"
 *
 * @ejb:finder signature="Collection findPostedVouByVouDebitMemoAndVouDateRangeExemptZero(byte VOU_DBT_MMO, java.util.Date VOU_DT_FRM, java.util.Date VOU_DT_TO, java.lang.Integer VOU_AD_CMPNY)"
 *             query="SELECT OBJECT(vou) FROM ApVoucher vou  WHERE vou.vouPosted = 1 AND vou.vouDebitMemo = ?1 AND vou.vouDate >= ?2 AND vou.vouDate <= ?3 AND vou.apTaxCode.tcType IN ('EXEMPT','ZERO-RATED') AND vou.vouAdCompany = ?4"
 *
 * @jboss:query signature="Collection findPostedVouByVouDebitMemoAndVouDateRangeExemptZero(byte VOU_DBT_MMO, java.util.Date VOU_DT_FRM, java.util.Date VOU_DT_TO, java.lang.Integer VOU_AD_CMPNY)"
 *             query="SELECT OBJECT(vou) FROM ApVoucher vou  WHERE vou.vouPosted = 1 AND vou.vouDebitMemo = ?1 AND vou.vouDate >= ?2 AND vou.vouDate <= ?3 AND vou.apTaxCode.tcType IN ('EXEMPT','ZERO-RATED') AND vou.vouAdCompany = ?4 ORDER BY vou.vouDate"
 *
 * @ejb:finder signature="Collection findByVouDebitMemoAndVouDmVoucherNumberAndVouVoid(java.lang.String VOU_DM_VCHR_NMBR, java.lang.Integer VOU_AD_BRNCH, java.lang.Integer VOU_AD_CMPNY)"
 *             query="SELECT OBJECT(vou) FROM ApVoucher vou  WHERE vou.vouDebitMemo = 1 AND vou.vouVoid = 0 AND vou.vouDmVoucherNumber = ?1 AND vou.vouAdBranch = ?2 AND vou.vouAdCompany = ?3"
 * 
 * @ejb:finder signature="Collection findByVouDebitMemoAndVouDmVoucherNumberAndVouVoidAndVouPosted(byte VOU_DBT_MMO, java.lang.String VOU_DM_VCHR_NMBR, byte VOU_VD, byte VOU_PSTD, java.lang.Integer VOU_AD_CMPNY)"
 *             query="SELECT OBJECT(vou) FROM ApVoucher vou  WHERE vou.vouDebitMemo = ?1 AND vou.vouDmVoucherNumber = ?2 AND vou.vouVoid = ?3 AND vou.vouPosted = ?4 AND vou.vouAdCompany = ?5"
 *
 * @ejb:finder signature="Collection findByVouDateRangeAndWtcCode(java.util.Date VOU_DT_FRM, java.util.Date VOU_DT_TO, java.lang.Integer WTC_CODE, java.lang.Integer VOU_AD_CMPNY)"
 *             query="SELECT OBJECT(vou) FROM ApVoucher vou  WHERE vou.vouPosted = 1 AND vou.vouDebitMemo = 0 AND vou.vouDate >= ?1 AND vou.vouDate <= ?2 AND vou.apWithholdingTaxCode.wtcCode = ?3 AND vou.vouAdCompany = ?4"
 *
 * @jboss:query signature="Collection findByVouDateRangeAndWtcCode(java.util.Date VOU_DT_FRM, java.util.Date VOU_DT_TO, java.lang.Integer WTC_CODE, java.lang.Integer VOU_AD_CMPNY)"
 *             query="SELECT OBJECT(vou) FROM ApVoucher vou  WHERE vou.vouPosted = 1 AND vou.vouDebitMemo = 0 AND vou.vouDate >= ?1 AND vou.vouDate <= ?2 AND vou.apWithholdingTaxCode.wtcCode = ?3 AND vou.vouAdCompany = ?4 ORDER BY vou.vouDate"
 *
 * @ejb:finder signature="Collection findByVouPostedAndVouVoidAndVbName(byte VOU_PSTD, byte VOU_VD, java.lang.String VB_NM, java.lang.Integer VOU_AD_CMPNY)"
 *             query="SELECT OBJECT(vou) FROM ApVoucher vou WHERE vou.vouPosted=?1 AND vou.vouVoid = ?2 AND vou.apVoucherBatch.vbName = ?3 AND vou.vouAdCompany = ?4"
 * 
 * @ejb:finder signature="Collection findByVbName(java.lang.String VB_NM, java.lang.Integer VOU_AD_CMPNY)"
 *             query="SELECT OBJECT(vou) FROM ApVoucher vou  WHERE vou.apVoucherBatch.vbName=?1 AND vou.vouAdCompany = ?2"
 * 
 * @jboss:query signature="Collection findByVbName(java.lang.String VB_NM, java.lang.Integer VOU_AD_CMPNY)"
 *             query="SELECT OBJECT(vou) FROM ApVoucher vou  WHERE vou.apVoucherBatch.vbName=?1 AND vou.vouAdCompany = ?2 ORDER BY vou.vouDocumentNumber, vou.vouDate"
 * 
 * @ejb:finder signature="Collection findDraftVouByVouTypeAndVouDebitMemoAndBrCode(java.lang.String VOU_TYP, byte VOU_DBT_MMO, java.lang.Integer VOU_AD_BRNCH, java.lang.Integer VOU_AD_CMPNY)"
 *             query="SELECT OBJECT(vou) FROM ApVoucher vou WHERE vou.vouReasonForRejection IS NULL AND vou.vouApprovalStatus IS NULL AND vou.vouVoid = 0 AND vou.vouType = ?1 AND vou.vouDebitMemo = ?2 AND vou.vouPosted=0 AND vou.vouAdBranch = ?3 AND vou.vouAdCompany = ?4"
 *             
 * @ejb:finder signature="Collection findDraftVouAndVouDebitMemoAndBrCode(byte VOU_DBT_MMO, java.lang.Integer VOU_AD_BRNCH, java.lang.Integer VOU_AD_CMPNY)"
 *             query="SELECT OBJECT(vou) FROM ApVoucher vou WHERE vou.vouReasonForRejection IS NULL AND vou.vouApprovalStatus IS NULL AND vou.vouVoid = 0 AND vou.vouDebitMemo = ?1 AND vou.vouPosted=0 AND vou.vouAdBranch = ?2 AND vou.vouAdCompany = ?3"
 *
 * @ejb:finder signature="Collection findDraftRequestVouByBrCode(java.lang.Integer VOU_AD_BRNCH, java.lang.Integer VOU_AD_CMPNY)"
 *             query="SELECT OBJECT(vou) FROM ApVoucher vou WHERE vou.vouApprovalStatus IS NULL AND vou.vouPosted = 0 AND vou.vouVoid = 0 AND vou.vouType = 'REQUEST' AND vou.vouGenerated = 0 AND vou.vouAdBranch = ?1 AND vou.vouAdCompany = ?2"
 *
 * @ejb:finder signature="Collection findForGenerationRequestVouByBrCode(java.lang.Integer VOU_AD_BRNCH, java.lang.Integer VOU_AD_CMPNY)"
 *             query="SELECT OBJECT(vou) FROM ApVoucher vou WHERE vou.vouApprovalStatus IS NOT NULL AND vou.vouPosted = 1 AND vou.vouVoid = 0 AND vou.vouType = 'REQUEST' AND vou.vouGenerated = 0 AND vou.vouAdBranch = ?1 AND vou.vouAdCompany = ?2"
 *
 * @ejb:finder signature="Collection findForProcessingRequestVouByBrCode(java.lang.Integer VOU_AD_BRNCH, java.lang.Integer VOU_AD_CMPNY)"
 *             query="SELECT OBJECT(vou) FROM ApVoucher vou WHERE vou.vouApprovalStatus IS NOT NULL AND vou.vouPosted = 0 AND vou.vouVoid = 0 AND vou.vouType = 'REQUEST' AND vou.vouAmountPaid < vou.vouAmountDue AND vou.vouGenerated = 0 AND vou.vouAdBranch = ?1 AND vou.vouAdCompany = ?2"
 *
 *
 * @ejb:finder signature="Collection findPostedVouByVouDebitMemoAndVouDateRangeAndSplNameAndTcType(byte INV_CRDT_MMO, java.util.Date INV_DT_FRM, java.util.Date INV_DT_TO, java.lang.String SPL_NM, java.lang.String TC_TYP, java.lang.Integer VOU_AD_CMPNY)"
 *             query="SELECT OBJECT(vou) FROM ApVoucher vou  WHERE vou.vouPosted = 1 AND vou.vouDebitMemo = ?1 AND vou.vouDate >= ?2 AND vou.vouDate <= ?3 AND vou.apSupplier.splName = ?4 AND vou.apTaxCode.tcType = ?5 AND vou.vouAdCompany = ?6"
 *
 * @jboss:query signature="Collection findPostedVouByVouDebitMemoAndVouDateRangeAndSplNameAndTcType(byte INV_CRDT_MMO, java.util.Date INV_DT_FRM, java.util.Date INV_DT_TO, java.lang.String SPL_NM, java.lang.String TC_TYP, java.lang.Integer VOU_AD_CMPNY)"
 *             query="SELECT OBJECT(vou) FROM ApVoucher vou  WHERE vou.vouPosted = 1 AND vou.vouDebitMemo = ?1 AND vou.vouDate >= ?2 AND vou.vouDate <= ?3 AND vou.apSupplier.splName = ?4 AND vou.apTaxCode.tcType = ?5 AND vou.vouAdCompany = ?6 ORDER BY vou.vouDate"
 * 
 * @ejb:finder signature="Collection findUnpostedVouByVouDateRange(java.util.Date VOU_DT_FRM, java.util.Date VOU_DT_TO, java.lang.Integer VOU_AD_CMPNY)"
 *             query="SELECT OBJECT(vou) FROM ApVoucher vou  WHERE vou.vouPosted = 0 AND vou.vouVoid = 0 AND vou.vouDate >= ?1 AND vou.vouDate <= ?2 AND vou.vouAdCompany = ?3"
 *
 * @jboss:query signature="Collection findUnpostedVouByVouDateRange(java.util.Date VOU_DT_FRM, java.util.Date VOU_DT_TO, java.lang.Integer VOU_AD_CMPNY)"
 *             query="SELECT OBJECT(vou) FROM ApVoucher vou  WHERE vou.vouPosted = 0 AND vou.vouVoid = 0 AND vou.vouDate >= ?1 AND vou.vouDate <= ?2 AND vou.vouAdCompany = ?3 ORDER BY vou.vouDate"
 * 
 * @ejb:finder signature="Collection findByVouReferenceNumber(java.lang.String VOU_RFRNC_NMBR, java.lang.Integer VOU_AD_BRNCH, java.lang.Integer VOU_AD_CMPNY)"
 *             query="SELECT OBJECT(vou) FROM ApVoucher vou  WHERE vou.vouReferenceNumber = ?1 AND vou.vouAdBranch = ?2 AND vou.vouAdCompany = ?3"
 *
 * @jboss:query signature="Collection findByVouAll(byte VOU_DBT_MMO, java.lang.Integer VOU_AD_CMPNY)"
 *             query="SELECT OBJECT(vou) FROM ApVoucher vou  WHERE vou.vouDebitMemo = ?1 AND vou.vouAdCompany = ?2 ORDER BY vou.vouCode"
 * 
 * @ejb:finder signature="Collection findByVouAll(byte VOU_DBT_MMO, java.lang.Integer VOU_AD_CMPNY)"
 *             query="SELECT OBJECT(vou) FROM ApVoucher vou  WHERE vou.vouDebitMemo = ?1 AND vou.vouAdCompany = ?2"
 * 
 * @ejb:finder signature="Collection findByVouDebitMemoAndVouVbNameVouAll(byte VOU_DBT_MMO, java.lang.String VB_NM, java.lang.Integer VOU_AD_CMPNY)"
 *             query="SELECT OBJECT(vou) FROM ApVoucher vou  WHERE vou.vouDebitMemo = ?1 AND vou.apVoucherBatch.vbName=?2 AND vou.vouAdCompany = ?3"
 * 
 * @ejb:finder signature="Collection findByVouReferenceNumberAndVouPostedAndSplName(java.lang.String VOU_RFRNC_NMBR, byte VOU_PSTD, java.lang.String SPL_NM, java.lang.Integer VOU_AD_CMPNY)"
 *             query="SELECT OBJECT(vou) FROM ApVoucher vou  WHERE vou.vouReferenceNumber = ?1 AND vou.vouPosted = ?2 AND vou.apSupplier.splName = ?3 AND vou.vouAdCompany = ?4"
 *
 *
 * @ejb:finder signature="Collection findByBeforeOrEqualVouDateAndSplCode(java.util.Date VOU_DT, java.lang.Integer SPL_CODE, java.lang.Integer VOU_AD_CMPNY)"
 *             query="SELECT OBJECT(vou) FROM ApVoucher vou  WHERE vou.vouDescription = 'Interest Income' AND vou.vouReferenceNumber LIKE 'INT-%' AND vou.vouDate <= ?1 AND vou.apSupplier.splCode = ?2 AND vou.vouAdCompany = ?3"
 *             
 * @jboss:query signature="Collection findByBeforeOrEqualVouDateAndSplCode(java.util.Date VOU_DT, java.lang.Integer SPL_CODE, java.lang.Integer VOU_AD_CMPNY)"
 *             query="SELECT OBJECT(vou) FROM ApVoucher vou  WHERE vou.vouDescription = 'Interest Income' AND vou.vouReferenceNumber LIKE 'INT-%' AND vou.vouDate <= ?1 AND vou.apSupplier.splCode = ?2 AND vou.vouAdCompany = ?3 ORDER BY vou.vouDate"
 *             
 * @ejb:finder signature="Collection findByVouReferenceNumberAndSplName(java.lang.String VOU_RFRNC_NMBR, java.lang.String SPL_NM, java.lang.Integer VOU_AD_CMPNY)"
 *             query="SELECT OBJECT(vou) FROM ApVoucher vou  WHERE vou.vouReferenceNumber = ?1 AND vou.apSupplier.splName = ?2 AND vou.vouAdCompany = ?3"
 *
 * @ejb:finder signature="Collection findByVouRejectedCPRByByBrCodeAndVouCreatedBy(java.lang.Integer AD_BRNCH, java.lang.String VOU_CRTD_BY, java.lang.Integer VOU_AD_CMPNY)"
 *             query="SELECT OBJECT(vou) FROM ApVoucher vou  WHERE vou.vouType = 'REQUEST' AND vou.vouReasonForRejection IS NOT NULL AND vou.vouAdBranch = ?1 AND vou.vouCreatedBy = ?2 AND vou.vouAdCompany = ?3"
 *
 * @ejb:finder signature="Collection findRejectedVouByBrCodeAndPoCreatedBy(java.lang.Integer vou_AD_BRNCH, java.lang.String vou_CRTD_BY, java.lang.Integer vou_AD_CMPNY)" 
 * 				query="SELECT OBJECT(vou) FROM ApVoucher vou WHERE vou.vouVoid = 0 AND vou.vouReasonForRejection IS NOT NULL AND vou.vouAdBranch = ?1 AND vou.vouCreatedBy=?2 AND vou.vouAdCompany = ?3"
 *
 * @ejb:finder signature="Collection findUnPostedVouByVouDateRangeAndSplCode(byte VOU_DBT_MMO, java.util.Date VOU_DT_FRM, java.util.Date VOU_DT_TO, java.lang.Integer SPL_CODE, java.lang.Integer VOU_AD_BRNCH, java.lang.Integer VOU_AD_CMPNY)"
 *             query="SELECT OBJECT(vou) FROM ApVoucher vou WHERE vou.vouPosted = 0 AND vou.vouDebitMemo = ?1 AND vou.vouDate >= ?2 AND vou.vouDate <= ?3 AND vou.apSupplier.splCode = ?4 AND vou.vouAdBranch = ?5 AND vou.vouAdCompany = ?6 ORDER BY vou.vouDate, vou.vouDocumentNumber"
 *
 * @jboss:query signature="Collection findUnPostedVouByVouDateRangeAndSplCode(byte VOU_DBT_MMO, java.util.Date VOU_DT_FRM, java.util.Date VOU_DT_TO, java.lang.Integer SPL_CODE, java.lang.Integer VOU_AD_BRNCH, java.lang.Integer VOU_AD_CMPNY)"
 *             query="SELECT OBJECT(vou) FROM ApVoucher vou WHERE vou.vouPosted = 0 AND vou.vouDebitMemo = ?1 AND vou.vouDate >= ?2 AND vou.vouDate <= ?3 AND vou.apSupplier.splCode = ?4 AND vou.vouAdBranch = ?5 AND vou.vouAdCompany = ?6 ORDER BY vou.vouDate, vou.vouDocumentNumber"
 *
 * @ejb:finder signature="Collection findPostedVouByVouDateRangeAndSplCode(byte VOU_DBT_MMO, java.util.Date VOU_DT_FRM, java.util.Date VOU_DT_TO, java.lang.Integer SPL_CODE, java.lang.Integer VOU_AD_BRNCH, java.lang.Integer VOU_AD_CMPNY)"
 *             query="SELECT OBJECT(vou) FROM ApVoucher vou WHERE vou.vouPosted = 1 AND vou.vouDebitMemo = ?1 AND vou.vouDate >= ?2 AND vou.vouDate <= ?3 AND vou.apSupplier.splCode = ?4 AND vou.vouAdBranch = ?5 AND vou.vouAdCompany = ?6 ORDER BY vou.vouDate, vou.vouDocumentNumber"
 *
 * @jboss:query signature="Collection findPostedVouByVouDateRangeAndSplCode(byte VOU_DBT_MMO, java.util.Date VOU_DT_FRM, java.util.Date VOU_DT_TO, java.lang.Integer SPL_CODE, java.lang.Integer VOU_AD_BRNCH, java.lang.Integer VOU_AD_CMPNY)"
 *             query="SELECT OBJECT(vou) FROM ApVoucher vou WHERE vou.vouPosted = 1 AND vou.vouDebitMemo = ?1 AND vou.vouDate >= ?2 AND vou.vouDate <= ?3 AND vou.apSupplier.splCode = ?4 AND vou.vouAdBranch = ?5 AND vou.vouAdCompany = ?6 ORDER BY vou.vouDate, vou.vouDocumentNumber"
 *
 *
 * @ejb:finder signature="Collection findPostedTaxVouByVouDateRange(byte VOU_DBT_MMO, java.util.Date VOU_DT_FRM, java.util.Date VOU_DT_TO, java.lang.Integer VOU_AD_BRNCH, java.lang.Integer VOU_AD_CMPNY)"
 *             query="SELECT OBJECT(vou) FROM ApVoucher vou WHERE vou.apSupplier.apSupplierClass.scName NOT IN ('Officers and Employees') AND vou.vouType NOT IN ('ITEMS') AND vou.vouPosted = 1 AND vou.apTaxCode.tcType IN ('EXEMPT','ZERO-RATED','INCLUSIVE','EXCLUSIVE') AND vou.vouDebitMemo = ?1 AND vou.vouDate >= ?2 AND vou.vouDate <= ?3 AND vou.vouAdBranch = ?4 AND vou.vouAdCompany = ?5 ORDER BY vou.vouDate, vou.vouDocumentNumber"
 *
 * @jboss:query signature="Collection findPostedTaxVouByVouDateRange(byte VOU_DBT_MMO, java.util.Date VOU_DT_FRM, java.util.Date VOU_DT_TO, java.lang.Integer VOU_AD_BRNCH, java.lang.Integer VOU_AD_CMPNY)"
 *             query="SELECT OBJECT(vou) FROM ApVoucher vou WHERE vou.apSupplier.apSupplierClass.scName NOT IN ('Officers and Employees') AND vou.vouType NOT IN ('ITEMS') AND vou.vouPosted = 1 AND vou.apTaxCode.tcType IN ('EXEMPT','ZERO-RATED','INCLUSIVE','EXCLUSIVE') AND vou.vouDebitMemo = ?1 AND vou.vouDate >= ?2 AND vou.vouDate <= ?3 AND vou.vouAdBranch = ?4 AND vou.vouAdCompany = ?5 ORDER BY vou.vouDate, vou.vouDocumentNumber"
 *
 * @ejb:finder signature="Collection findPostedTaxVouByVouOaeDateRange(byte VOU_DBT_MMO, java.util.Date VOU_DT_FRM, java.util.Date VOU_DT_TO, java.lang.Integer VOU_AD_BRNCH, java.lang.Integer VOU_AD_CMPNY)"
 *             query="SELECT OBJECT(vou) FROM ApVoucher vou WHERE vou.apSupplier.apSupplierClass.scName = 'Officers and Employees' AND vou.vouType = 'ITEMS' AND vou.vouPosted = 1 AND vou.apTaxCode.tcType IN ('EXEMPT','ZERO-RATED','INCLUSIVE','EXCLUSIVE') AND vou.vouDebitMemo = ?1 AND vou.vouDate >= ?2 AND vou.vouDate <= ?3 AND vou.vouAdBranch = ?4 AND vou.vouAdCompany = ?5 ORDER BY vou.vouDate, vou.vouDocumentNumber"
 *
 * @jboss:query signature="Collection findPostedTaxVouByVouOaeDateRange(byte VOU_DBT_MMO, java.util.Date VOU_DT_FRM, java.util.Date VOU_DT_TO, java.lang.Integer VOU_AD_BRNCH, java.lang.Integer VOU_AD_CMPNY)"
 *             query="SELECT OBJECT(vou) FROM ApVoucher vou WHERE vou.apSupplier.apSupplierClass.scName = 'Officers and Employees' AND vou.vouType = 'ITEMS' AND vou.vouPosted = 1 AND vou.apTaxCode.tcType IN ('EXEMPT','ZERO-RATED','INCLUSIVE','EXCLUSIVE') AND vou.vouDebitMemo = ?1 AND vou.vouDate >= ?2 AND vou.vouDate <= ?3 AND vou.vouAdBranch = ?4 AND vou.vouAdCompany = ?5 ORDER BY vou.vouDate, vou.vouDocumentNumber"
 *
 *
* @jboss:query signature="Collection findVouForLoanGeneration(java.lang.Integer VOU_AD_BRNCH, java.lang.Integer CHK_AD_CMPNY)"
 *             query="SELECT OBJECT(vou) FROM ApVoucher vou WHERE vou.vouDebitMemo = 0 AND vou.vouAmountDue <= vou.vouAmountPaid AND vou.vouPosted = 1 AND vou.vouType = 'ITEMS' AND vou.vouLoan = 1 AND vou.vouLoanGenerated = 0 AND vou.vouAdBranch = ?1 AND vou.vouAdCompany = ?2 ORDER BY vou.vouDate"
 *
 * @ejb:finder signature="Collection findVouForLoanGeneration(java.lang.Integer VOU_AD_BRNCH, java.lang.Integer CHK_AD_CMPNY)"
 *             query="SELECT OBJECT(vou) FROM ApVoucher vou WHERE vou.vouDebitMemo = 0 AND vou.vouAmountDue <= vou.vouAmountPaid AND vou.vouPosted = 1 AND vou.vouType = 'ITEMS' AND vou.vouLoan = 1 AND vou.vouLoanGenerated = 0 AND vou.vouAdBranch = ?1 AND vou.vouAdCompany = ?2"
 *
 *                          
 * @ejb:select signature="Collection ejbSelectGeneric(java.lang.String jbossQl, java.lang.Object[] args)"
 *             result-type-mapping="Local"
 *             method-intf="LocalHome"
 *             query="SELECT OBJECT(vou) FROM ApVoucher vou"
 *
 * @jboss:query signature="Collection ejbSelectGeneric(java.lang.String jbossQl, java.lang.Object[] args)"
 *                         dynamic="true"
 *
 * @ejb:value-object match="*"
 *             name="ApVoucher"
 *
 * @jboss:persistence table-name="AP_VCHR"
 *
 * @jboss:entity-command name="mysql-get-generated-keys"
 */

public abstract class ApVoucherBean extends AbstractEntityBean {
    
    
    // PERSISTENT METHODS
    
    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     * @ejb:pk-field
     *
     * @jboss:column-name name="VOU_CODE"
     *
     * @jboss:persistence auto-increment="true"
     **/
    public abstract Integer getVouCode();         
    public abstract void setVouCode(Integer VOU_CODE);
     
    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="VOU_DBT_MMO"
     **/
     public abstract byte getVouDebitMemo();
     /**
      * @ejb:interface-method view-type="local"
      **/
     public abstract void setVouDebitMemo(byte VOU_DBT_MMO);
     
     /**
      * @ejb:persistent-field
      * @ejb:interface-method view-type="local"
      *
      * @jboss:column-name name="VOU_TYP"
      **/
      public abstract String getVouType();
      /**
       * @ejb:interface-method view-type="local"
       **/
      public abstract void setVouType(String VOU_TYP);

    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="VOU_DESC"
     **/
     public abstract String getVouDescription();
     /**
      * @ejb:interface-method view-type="local"
      **/
     public abstract void setVouDescription(String VOU_DESC);


    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="VOU_DT"
     **/
     public abstract Date getVouDate();
     /**
      * @ejb:interface-method view-type="local"
      **/
     public abstract void setVouDate(Date VOU_DT);
     
     
     /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="VOU_DCMNT_NMBR"
     **/
     public abstract String getVouDocumentNumber();
     /**
      * @ejb:interface-method view-type="local"
      **/
     public abstract void setVouDocumentNumber(String VOU_DCMNT_NMBR);

     /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="VOU_RFRNC_NMBR"
     **/
     public abstract String getVouReferenceNumber();
     /**
      * @ejb:interface-method view-type="local"
      **/
     public abstract void setVouReferenceNumber(String VOU_RFRNC_NMBR);
     
     
     /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="VOU_DM_VCHR_NMBR"
     **/
     public abstract String getVouDmVoucherNumber();
     /**
      * @ejb:interface-method view-type="local"
      **/
     public abstract void setVouDmVoucherNumber(String VOU_DM_VCHR_NMBR);

    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="VOU_CNVRSN_DT"
     **/
     public abstract Date getVouConversionDate();
     /**
      * @ejb:interface-method view-type="local"
      **/
     public abstract void setVouConversionDate(Date VOU_CNVRSN_DT);

    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="VOU_CNVRSN_RT"
     **/
     public abstract double getVouConversionRate();
     /**
      * @ejb:interface-method view-type="local"
      **/
     public abstract void setVouConversionRate(double VOU_CNVRSN_RT);
    
    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="VOU_BLL_AMNT"
     **/
     public abstract double getVouBillAmount();
     /**
      * @ejb:interface-method view-type="local"
      **/
     public abstract void setVouBillAmount(double VOU_BLL_AMNT);

    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="VOU_AMNT_DUE"
     **/
     public abstract double getVouAmountDue();
     /**
      * @ejb:interface-method view-type="local"
      **/
     public abstract void setVouAmountDue(double VOU_AMNT_DUE);
     
     
     /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="VOU_AMNT_PD"
     **/
     public abstract double getVouAmountPaid();
     /**
      * @ejb:interface-method view-type="local"
      **/
     public abstract void setVouAmountPaid(double VOU_AMNT_PD);
     
     /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="VOU_APPRVL_STATUS"
     **/
     public abstract String getVouApprovalStatus();
     /**
      * @ejb:interface-method view-type="local"
      **/
     public abstract void setVouApprovalStatus(String VOU_APPRVL_STATUS);
     
     /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="VOU_RSN_FR_RJCTN"
     **/
     public abstract String getVouReasonForRejection();
     /**
      * @ejb:interface-method view-type="local"
      **/
     public abstract void setVouReasonForRejection(String VOU_RSN_FR_RJCTN);     

     /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="VOU_PSTD"
     **/
     public abstract byte getVouPosted();
     /**
      * @ejb:interface-method view-type="local"
      **/
     public abstract void setVouPosted(byte VOU_PSTD);
     
     /**
      * @ejb:persistent-field
      * @ejb:interface-method view-type="local"
      *
      * @jboss:column-name name="VOU_GNRTD"
      **/
      public abstract byte getVouGenerated();
      /**
       * @ejb:interface-method view-type="local"
       **/
      public abstract void setVouGenerated(byte VOU_GNRTD);
      
     
    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="VOU_VD"
     **/
     public abstract byte getVouVoid();
     /**
      * @ejb:interface-method view-type="local"
      **/
     public abstract void setVouVoid(byte VOU_VD);

     /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="VOU_CRTD_BY"
     **/
     public abstract String getVouCreatedBy();
     /**
      * @ejb:interface-method view-type="local"
      **/
     public abstract void setVouCreatedBy(String VOU_CRTD_BY);

    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="VOU_DT_CRTD"
     **/
     public abstract Date getVouDateCreated();
     /**
      * @ejb:interface-method view-type="local"
      **/
     public abstract void setVouDateCreated(Date VOU_DT_CRTD);

    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="VOU_LST_MDFD_BY"
     **/
     public abstract String getVouLastModifiedBy();
     /**
      * @ejb:interface-method view-type="local"
      **/
     public abstract void setVouLastModifiedBy(String VOU_LST_MDFD_BY);

    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="VOU_DT_LST_MDFD"
     **/
     public abstract Date getVouDateLastModified();
     /**
      * @ejb:interface-method view-type="local"
      **/
     public abstract void setVouDateLastModified(Date VOU_DT_LST_MDFD);

    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="VOU_APPRVD_RJCTD_BY"
     **/
     public abstract String getVouApprovedRejectedBy();
     /**
      * @ejb:interface-method view-type="local"
      **/
     public abstract void setVouApprovedRejectedBy(String VOU_APPRVD_RJCTD_BY);

    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="VOU_DT_APPRVD_RJCTD"
     **/
     public abstract Date getVouDateApprovedRejected();
     /**
      * @ejb:interface-method view-type="local"
      **/
     public abstract void setVouDateApprovedRejected(Date VOU_DT_APPRVD_RJCTD);

    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="VOU_PSTD_BY"
     **/
     public abstract String getVouPostedBy();
     /**
      * @ejb:interface-method view-type="local"
      **/
     public abstract void setVouPostedBy(String VOU_PSTD_BY);

    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="VOU_DT_PSTD"
     **/
     public abstract Date getVouDatePosted();
     /**
      * @ejb:interface-method view-type="local"
      **/
     public abstract void setVouDatePosted(Date VOU_DT_PSTD);
     
     /**
      * @ejb:persistent-field
      * @ejb:interface-method view-type="local"
      *
      * @jboss:column-name name="VOU_PRNTD"
      **/
      public abstract byte getVouPrinted();
      /**
       * @ejb:interface-method view-type="local"
       **/
      public abstract void setVouPrinted(byte VOU_PRNTD);
  
      /**
       * @ejb:persistent-field
       * @ejb:interface-method view-type="local"
       *
       * @jboss:column-name name="VOU_PO_NMBR"
       **/
       public abstract String getVouPoNumber();
       /**
        * @ejb:interface-method view-type="local"
        **/
       public abstract void setVouPoNumber(String VOU_PO_NMBR);

      /**
		 * @ejb:persistent-field
		 * @ejb:interface-method view-type="local"
		 *
		 * @jboss:column-name name="VOU_LN"
		 **/
		 public abstract byte getVouLoan();
		 /**
		  * @ejb:interface-method view-type="local"
		  **/
		 public abstract void setVouLoan(byte VOU_LN);
		 
		 
	 /**
		 * @ejb:persistent-field
		 * @ejb:interface-method view-type="local"
		 *
		 * @jboss:column-name name="VOU_LN_GNRTD"
		 **/
		 public abstract byte getVouLoanGenerated();
		 /**
		  * @ejb:interface-method view-type="local"
		  **/
		 public abstract void setVouLoanGenerated(byte VOU_LN_GNRTD);
 
       
       /**
        * @ejb:persistent-field
        * @ejb:interface-method view-type="local"
        *
        * @jboss:column-name name="VOU_AD_BRNCH"
        **/
        public abstract Integer getVouAdBranch();
        /**
         * @ejb:interface-method view-type="local"
         **/
        public abstract void setVouAdBranch(Integer VOU_AD_BRNCH);

        /**
       * @ejb:persistent-field
       * @ejb:interface-method view-type="local"
       *
       * @jboss:column-name name="VOU_AD_CMPNY"
       **/
       public abstract Integer getVouAdCompany();
       /**
        * @ejb:interface-method view-type="local"
        **/
       public abstract void setVouAdCompany(Integer VOU_AD_CMPNY);


    // RELATIONSHIP METHODS    

     /**
      * @ejb:interface-method view-type="local"
      * @ejb:relation name="supplier-vouchers"
      *               role-name="voucher-has-one-supplier"
      *
      * @jboss:relation related-pk-field="splCode"
      *                 fk-column="AP_SUPPLIER"
      */
     public abstract LocalApSupplier getApSupplier();
     /**
      * @ejb:interface-method view-type="local"
      **/
     public abstract void setApSupplier(LocalApSupplier apSupplier);
     
    

     /**
      * @ejb:interface-method view-type="local"
      * @ejb:relation name="withholdingtaxcode-vouchers"
      *               role-name="voucher-has-one-withholdingtaxcode"
      *
      * @jboss:relation related-pk-field="wtcCode"
      *                 fk-column="AP_WITHHOLDING_TAX_CODE"
      */
     public abstract LocalApWithholdingTaxCode getApWithholdingTaxCode();
     /**
      * @ejb:interface-method view-type="local"
      **/
     public abstract void setApWithholdingTaxCode(LocalApWithholdingTaxCode apWithholdingTaxCode);

     
     /**
      * @ejb:interface-method view-type="local"
      * @ejb:relation name="paymentterm-vouchers"
      *               role-name="voucher-has-one-paymentterm"
      *
      * @jboss:relation related-pk-field="pytCode"
      *                 fk-column="AD_PAYMENT_TERM"
      */
     public abstract com.ejb.ad.LocalAdPaymentTerm getAdPaymentTerm();
     /**
      * @ejb:interface-method view-type="local"
      **/
     public abstract void setAdPaymentTerm(com.ejb.ad.LocalAdPaymentTerm adPaymentTerm);
     
     
     /**
      * @ejb:interface-method view-type="local"
      * @ejb:relation name="paymentterm2-vouchers"
      *               role-name="voucher-has-one-paymentterm2"
      *
      * @jboss:relation related-pk-field="pytCode"
      *                 fk-column="AD_PAYMENT_TERM2"
      */
     public abstract com.ejb.ad.LocalAdPaymentTerm getAdPaymentTerm2();
     /**
      * @ejb:interface-method view-type="local"
      **/
     public abstract void setAdPaymentTerm2(com.ejb.ad.LocalAdPaymentTerm adPaymentTerm);
     

     /**
      * @ejb:interface-method view-type="local"
      * @ejb:relation name="functionalcurrency-vouchers"
      *               role-name="voucher-has-one-functionalcurrency"
      *
      * @jboss:relation related-pk-field="fcCode"
      *                 fk-column="GL_FUNCTIONAL_CURRENCY"
      */
     public abstract com.ejb.gl.LocalGlFunctionalCurrency getGlFunctionalCurrency();
     /**
      * @ejb:interface-method view-type="local"
      **/
     public abstract void setGlFunctionalCurrency(com.ejb.gl.LocalGlFunctionalCurrency glFunctionalCurrency);
     
     
     /**
      * @ejb:interface-method view-type="local"
      * @ejb:relation name="aptaxcode-vouchers"
      *               role-name="voucher-has-one-aptaxcode"
      *
      * @jboss:relation related-pk-field="tcCode"
      *                 fk-column="AP_TAX_CODE"
      */
     public abstract LocalApTaxCode getApTaxCode();
     /**
      * @ejb:interface-method view-type="local"
      **/
     public abstract void setApTaxCode(LocalApTaxCode apTaxCode);
     
     
     /**
      * @ejb:interface-method view-type="local"
      * @ejb:relation name="voucherbatch-vouchers"
      *               role-name="voucher-has-one-voucherbatch"
      *
      * @jboss:relation related-pk-field="vbCode"
      *                 fk-column="AP_VOUCHER_BATCH"
      */
     public abstract LocalApVoucherBatch getApVoucherBatch();
     /**
      * @ejb:interface-method view-type="local"
      **/
     public abstract void setApVoucherBatch(LocalApVoucherBatch apVoucherBatch);

     /**
      * @ejb:interface-method view-type="local"
      * @ejb:relation name="voucher-apdistributionrecords"
      *               role-name="voucher-has-many-apdistributionrecords"
      * 
      */
     public abstract Collection getApDistributionRecords();
     public abstract void setApDistributionRecords(Collection apDistributionRecords);

     /**
      * @ejb:interface-method view-type="local"
      * @ejb:relation name="voucher-voucherpaymentschedules"
      *               role-name="voucher-has-many-voucherpaymentschedules"
      */
     public abstract Collection getApVoucherPaymentSchedules();
     public abstract void setApVoucherPaymentSchedules(Collection apVoucherPaymentSchedules);
     
     /**
      * @ejb:interface-method view-type="local"
      * @ejb:relation name="voucher-voucherlineitems"
      *               role-name="voucher-has-many-voucherlineitems"
      * 
      */
     public abstract Collection getApVoucherLineItems();
     public abstract void setApVoucherLineItems(Collection apVoucherLineItems);
     
    /**
      * @ejb:interface-method view-type="local"
      * @ejb:relation name="voucher-purchaseorderlines"
      *               role-name="voucher-has-many-purchaseorderlines"
      * 
      */
     public abstract Collection getApPurchaseOrderLines();
     public abstract void setApPurchaseOrderLines(Collection apPurchaseOrderLines);     

	/**
	* @jboss:dynamic-ql
	*/
	public abstract Collection ejbSelectGeneric(java.lang.String jbossQl, java.lang.Object[] args)
	   throws FinderException;
	
	// BUSINESS METHODS
	
	/**
	* @ejb:home-method view-type="local"
	*/
	public Collection ejbHomeGetVouByCriteria(java.lang.String jbossQl, java.lang.Object[] args)
	   throws FinderException {
	   	
	   return ejbSelectGeneric(jbossQl, args);
	}

     
     /**
      * @ejb:interface-method view-type="local"
      **/
     public void addApDistributionRecord(LocalApDistributionRecord apDistributionRecord) {

          Debug.print("ApVoucherBean addApDistributionRecord");
      
          try {
      	
             Collection apDistributionRecords = getApDistributionRecords();
	         apDistributionRecords.add(apDistributionRecord);
	          
          } catch (Exception ex) {
      	
            throw new EJBException(ex.getMessage());
      
          }   
     }

     /**
      * @ejb:interface-method view-type="local"
      **/
     public void dropApDistributionRecord(LocalApDistributionRecord apDistributionRecord) {

          Debug.print("ApVoucherBean dropApDistributionRecord");
      
          try {
      	
             Collection apDistributionRecords = getApDistributionRecords();
	         apDistributionRecords.remove(apDistributionRecord);
	      
          } catch (Exception ex) {
      	
            throw new EJBException(ex.getMessage());
         
          }
             
     }
     
	 /**
	  * @ejb:interface-method view-type="local"
	  **/
	 public void addApVoucherPaymentSchedule(LocalApVoucherPaymentSchedule apVoucherPaymentSchedule) {
	
	      Debug.print("ApVoucherBean addApVoucherPaymentSchedule");
	  
	      try {
	  	
	      Collection apVoucherPaymentSchedules = getApVoucherPaymentSchedules();
		      apVoucherPaymentSchedules.add(apVoucherPaymentSchedule);
		     
	      } catch (Exception ex) {
	  	
	          throw new EJBException(ex.getMessage());
	     
	      }
	      
	 }
	
     /**
      * @ejb:interface-method view-type="local"
      **/ 
     public void dropApVoucherPaymentSchedule(LocalApVoucherPaymentSchedule apVoucherPaymentSchedule) {
		
		  Debug.print("ApVoucherBean dropApVoucherPaymentSchedule");
		  
		  try {
		  	
		     Collection apVoucherPaymentSchedules = getApVoucherPaymentSchedules();
			     apVoucherPaymentSchedules.remove(apVoucherPaymentSchedule);
			     
		  } catch (Exception ex) {
		  	
		     throw new EJBException(ex.getMessage());
		     
		  }
		  
     } 
     
	 /**
	  * @ejb:interface-method view-type="local"
	  **/
	 public void addApVoucherLineItem(LocalApVoucherLineItem apVoucherLineItem) {
	
	      Debug.print("ApVoucherBean addApVoucherLineItem");
	  
	      try {
	  	
	      Collection apVoucherLineItems = getApVoucherLineItems();
		      apVoucherLineItems.add(apVoucherLineItem);
		     
	      } catch (Exception ex) {
	  	
	          throw new EJBException(ex.getMessage());
	     
	      }
	      
	 }
	
     /**
      * @ejb:interface-method view-type="local"
      **/ 
     public void dropApVoucherLineItem(LocalApVoucherLineItem apVoucherLineItem) {
		
		  Debug.print("ApVoucherBean dropApVoucherLineItem");
		  
		  try {
		  	
		     Collection apVoucherLineItems = getApVoucherLineItems();
			     apVoucherLineItems.remove(apVoucherLineItem);
			     
		  } catch (Exception ex) {
		  	
		     throw new EJBException(ex.getMessage());
		     
		  }
		  
     }
     
     /**
	  * @ejb:interface-method view-type="local"
	  **/
	 public void addApPurchaseOrderLine(LocalApPurchaseOrderLine apPurchaseOrderLine) {
	
	      Debug.print("ApVoucherBean addApPurchaseOrderLine");
	  
	      try {
	  	
	      Collection apPurchaseOrderLines = getApPurchaseOrderLines();
		      apPurchaseOrderLines.add(apPurchaseOrderLine);
		     
	      } catch (Exception ex) {
	  	
	          throw new EJBException(ex.getMessage());
	     
	      }
	      
	 }
	
     /**
      * @ejb:interface-method view-type="local"
      **/ 
     public void dropApPurchaseOrderLine(LocalApPurchaseOrderLine apPurchaseOrderLine) {
		
		  Debug.print("ApVoucherBean dropApPurchaseOrderLine");
		  
		  try {
		  	
		     Collection apPurchaseOrderLines = getApPurchaseOrderLines();
			     apPurchaseOrderLines.remove(apPurchaseOrderLine);
			     
		  } catch (Exception ex) {
		  	
		     throw new EJBException(ex.getMessage());
		     
		  }
		  
     }    
				    
     
    /**
     * @ejb:interface-method view-type="local"
     **/
    public short getApDrNextLine() {

       Debug.print("ApVoucherBean getApDrNextLine");
      
       try {
      	
          Collection apDistributionRecords = getApDistributionRecords();
	      return (short)(apDistributionRecords.size() + 1);
	     
       } catch (Exception ex) {
      	
          throw new EJBException(ex.getMessage());
         
       }
    }     

              
     // ENTITY METHODS
    
     /**
      * @ejb:create-method view-type="local"
      **/
     public Integer ejbCreate(Integer VOU_CODE, byte VOU_DBT_MMO, String VOU_DESC, 
         Date VOU_DT, String VOU_DCMNT_NMBR, String VOU_RFRNC_NMBR, String VOU_DM_VCHR_NMBR, Date VOU_CNVRSN_DT, 
         double VOU_CNVRSN_RT, double VOU_BLL_AMNT, double VOU_AMNT_DUE, 
         double VOU_AMNT_PD, String VOU_APPRVL_STATUS, String VOU_RSN_FR_RJCTN, byte VOU_PSTD, byte VOU_GNRTD, byte VOU_VD,
	     String VOU_CRTD_BY, Date VOU_DT_CRTD, String VOU_LST_MDFD_BY,
	     Date VOU_DT_LST_MDFD, String VOU_APPRVD_RJCTD_BY, Date VOU_DT_APPRVD_RJCTD,
	     String VOU_PSTD_BY, Date VOU_DT_PSTD, byte VOU_PRNTD, String VOU_PO_NMBR,  
	     byte VOU_LN, byte VOU_LN_GNRTD,
		 Integer VOU_AD_BRNCH, Integer VOU_AD_CMPNY)
         throws CreateException {
           
         Debug.print("ApVoucherBean ejbCreate");
        
         setVouCode(VOU_CODE);
        
         setVouDebitMemo(VOU_DBT_MMO);
         setVouDescription(VOU_DESC);
         setVouDate(VOU_DT);	 
         setVouDocumentNumber(VOU_DCMNT_NMBR);
         setVouReferenceNumber(VOU_RFRNC_NMBR);
         setVouDmVoucherNumber(VOU_DM_VCHR_NMBR);
         setVouConversionDate(VOU_CNVRSN_DT);
         setVouConversionRate(VOU_CNVRSN_RT);
		 setVouBillAmount(VOU_BLL_AMNT);
		 setVouAmountDue(VOU_AMNT_DUE);
		 setVouAmountPaid(VOU_AMNT_PD);
		 setVouApprovalStatus(VOU_APPRVL_STATUS);
		 setVouReasonForRejection(VOU_RSN_FR_RJCTN);
         setVouPosted(VOU_PSTD);
         setVouGenerated(VOU_GNRTD);
         setVouVoid(VOU_VD);
         setVouCreatedBy(VOU_CRTD_BY);
         setVouDateCreated(VOU_DT_CRTD);
         setVouLastModifiedBy(VOU_LST_MDFD_BY);
         setVouDateLastModified(VOU_DT_LST_MDFD);
         setVouApprovedRejectedBy(VOU_APPRVD_RJCTD_BY);
         setVouDateApprovedRejected(VOU_DT_APPRVD_RJCTD);
         setVouPostedBy(VOU_PSTD_BY);
         setVouDatePosted(VOU_DT_PSTD);
         setVouPrinted(VOU_PRNTD);
         setVouPoNumber(VOU_PO_NMBR);
         setVouLoan(VOU_LN);
         setVouLoanGenerated(VOU_LN_GNRTD);
         

         setVouAdBranch(VOU_AD_BRNCH);
         setVouAdCompany(VOU_AD_CMPNY);
                
         return null;
        
     }
    
     /**
      * @ejb:create-method view-type="local"
      **/
     public Integer ejbCreate(byte VOU_DBT_MMO, String VOU_DESC, 
         Date VOU_DT, String VOU_DCMNT_NMBR, String VOU_RFRNC_NMBR, String VOU_DM_VCHR_NMBR, Date VOU_CNVRSN_DT, 
         double VOU_CNVRSN_RT, double VOU_BLL_AMNT, double VOU_AMNT_DUE, 
         double VOU_AMNT_PD, String VOU_APPRVL_STATUS, String VOU_RSN_FR_RJCTN, byte VOU_PSTD, byte VOU_GNRTD, byte VOU_VD,
	     String VOU_CRTD_BY, Date VOU_DT_CRTD, String VOU_LST_MDFD_BY,
	     Date VOU_DT_LST_MDFD, String VOU_APPRVD_RJCTD_BY, Date VOU_DT_APPRVD_RJCTD,
	     String VOU_PSTD_BY, Date VOU_DT_PSTD, byte VOU_PRNTD, String VOU_PO_NMBR,
	     byte VOU_LN, byte VOU_LN_GNRTD,
		 Integer VOU_AD_BRNCH, Integer VOU_AD_CMPNY)
         throws CreateException {
           
         Debug.print("ApVoucherBean ejbCreate");
         
         setVouDebitMemo(VOU_DBT_MMO);
         setVouDescription(VOU_DESC);
         setVouDate(VOU_DT);	 
         setVouDocumentNumber(VOU_DCMNT_NMBR);
         setVouReferenceNumber(VOU_RFRNC_NMBR);
         setVouDmVoucherNumber(VOU_DM_VCHR_NMBR);
         setVouConversionDate(VOU_CNVRSN_DT);
         setVouConversionRate(VOU_CNVRSN_RT);
		 setVouBillAmount(VOU_BLL_AMNT);
		 setVouAmountDue(VOU_AMNT_DUE);
		 setVouAmountPaid(VOU_AMNT_PD);
		 setVouApprovalStatus(VOU_APPRVL_STATUS);
		 setVouReasonForRejection(VOU_RSN_FR_RJCTN);
         setVouPosted(VOU_PSTD);
         setVouGenerated(VOU_GNRTD);
         setVouVoid(VOU_VD);
         setVouCreatedBy(VOU_CRTD_BY);
         setVouDateCreated(VOU_DT_CRTD);
         setVouLastModifiedBy(VOU_LST_MDFD_BY);
         setVouDateLastModified(VOU_DT_LST_MDFD);
         setVouApprovedRejectedBy(VOU_APPRVD_RJCTD_BY);
         setVouDateApprovedRejected(VOU_DT_APPRVD_RJCTD);
         setVouPostedBy(VOU_PSTD_BY);
         setVouDatePosted(VOU_DT_PSTD);
         setVouPrinted(VOU_PRNTD);
         setVouPoNumber(VOU_PO_NMBR);
         setVouLoan(VOU_LN);
         setVouLoanGenerated(VOU_LN_GNRTD);
         
         
         setVouAdBranch(VOU_AD_BRNCH);
         setVouAdCompany(VOU_AD_CMPNY);
        
         return null;
        
     }
     /**
      * @ejb:create-method view-type="local"
      **/
     public Integer ejbCreate(String VOU_TYP, byte VOU_DBT_MMO, String VOU_DESC, 
         Date VOU_DT, String VOU_DCMNT_NMBR, String VOU_RFRNC_NMBR, String VOU_DM_VCHR_NMBR, Date VOU_CNVRSN_DT, 
         double VOU_CNVRSN_RT, double VOU_BLL_AMNT, double VOU_AMNT_DUE, 
         double VOU_AMNT_PD, String VOU_APPRVL_STATUS, String VOU_RSN_FR_RJCTN, byte VOU_PSTD, byte VOU_GNRTD, byte VOU_VD,
	     String VOU_CRTD_BY, Date VOU_DT_CRTD, String VOU_LST_MDFD_BY,
	     Date VOU_DT_LST_MDFD, String VOU_APPRVD_RJCTD_BY, Date VOU_DT_APPRVD_RJCTD,
	     String VOU_PSTD_BY, Date VOU_DT_PSTD, byte VOU_PRNTD, String VOU_PO_NMBR, 
	     byte VOU_LN, byte VOU_LN_GNRTD,
		 Integer VOU_AD_BRNCH, Integer VOU_AD_CMPNY)
         throws CreateException {
           
         Debug.print("ApVoucherBean ejbCreate");
         
         setVouType(VOU_TYP);
         setVouDebitMemo(VOU_DBT_MMO);
         setVouDescription(VOU_DESC);
         setVouDate(VOU_DT);	 
         setVouDocumentNumber(VOU_DCMNT_NMBR);
         setVouReferenceNumber(VOU_RFRNC_NMBR);
         setVouDmVoucherNumber(VOU_DM_VCHR_NMBR);
         setVouConversionDate(VOU_CNVRSN_DT);
         setVouConversionRate(VOU_CNVRSN_RT);
		 setVouBillAmount(VOU_BLL_AMNT);
		 setVouAmountDue(VOU_AMNT_DUE);
		 setVouAmountPaid(VOU_AMNT_PD);
		 setVouApprovalStatus(VOU_APPRVL_STATUS);
		 setVouReasonForRejection(VOU_RSN_FR_RJCTN);
         setVouPosted(VOU_PSTD);
         setVouGenerated(VOU_GNRTD);
         setVouVoid(VOU_VD);
         setVouCreatedBy(VOU_CRTD_BY);
         setVouDateCreated(VOU_DT_CRTD);
         setVouLastModifiedBy(VOU_LST_MDFD_BY);
         setVouDateLastModified(VOU_DT_LST_MDFD);
         setVouApprovedRejectedBy(VOU_APPRVD_RJCTD_BY);
         setVouDateApprovedRejected(VOU_DT_APPRVD_RJCTD);
         setVouPostedBy(VOU_PSTD_BY);
         setVouDatePosted(VOU_DT_PSTD);
         setVouPrinted(VOU_PRNTD);
         setVouPoNumber(VOU_PO_NMBR);
         setVouLoan(VOU_LN);
         setVouLoanGenerated(VOU_LN_GNRTD);
         setVouAdBranch(VOU_AD_BRNCH);
         setVouAdCompany(VOU_AD_CMPNY);
        
         return null;
        
     }
    
    
     public void ejbPostCreate(Integer VOU_CODE, byte VOU_DBT_MMO, String VOU_DESC, 
         Date VOU_DT, String VOU_DCMNT_NMBR, String VOU_RFRNC_NMBR, String VOU_DM_VCHR_NMBR, Date VOU_CNVRSN_DT, 
         double VOU_CNVRSN_RT, double VOU_BLL_AMNT, double VOU_AMNT_DUE, 
         double VOU_AMNT_PD, String VOU_APPRVL_STATUS, String VOU_RSN_FR_RJCTN, byte VOU_PSTD, byte VOU_GNRTD, byte VOU_VD,
	     String VOU_CRTD_BY, Date VOU_DT_CRTD, String VOU_LST_MDFD_BY,
	     Date VOU_DT_LST_MDFD, String VOU_APPRVD_RJCTD_BY, Date VOU_DT_APPRVD_RJCTD,
	     String VOU_PSTD_BY, Date VOU_DT_PSTD, byte VOU_PRNTD, String VOU_PO_NMBR,
	     byte VOU_LN, byte VOU_LN_GNRTD,
		 Integer VOU_AD_BRNCH, Integer VOU_AD_CMPNY)
         throws CreateException { }
   
     public void ejbPostCreate(byte VOU_DBT_MMO, String VOU_DESC, 
         Date VOU_DT, String VOU_DCMNT_NMBR, String VOU_RFRNC_NMBR, String VOU_DM_VCHR_NMBR, Date VOU_CNVRSN_DT, 
         double VOU_CNVRSN_RT, double VOU_BLL_AMNT, double VOU_AMNT_DUE, 
         double VOU_AMNT_PD, String VOU_APPRVL_STATUS, String VOU_RSN_FR_RJCTN, byte VOU_PSTD, byte VOU_GNRTD, byte VOU_VD,
	     String VOU_CRTD_BY, Date VOU_DT_CRTD, String VOU_LST_MDFD_BY,
	     Date VOU_DT_LST_MDFD, String VOU_APPRVD_RJCTD_BY, Date VOU_DT_APPRVD_RJCTD,
	     String VOU_PSTD_BY, Date VOU_DT_PSTD, byte VOU_PRNTD, String VOU_PO_NMBR,
	     byte VOU_LN, byte VOU_LN_GNRTD,
		 Integer VOU_AD_BRNCH, Integer VOU_AD_CMPNY)
         throws CreateException { }
     
     public void ejbPostCreate(String VOU_TYP, byte VOU_DBT_MMO, String VOU_DESC, 
         Date VOU_DT, String VOU_DCMNT_NMBR, String VOU_RFRNC_NMBR, String VOU_DM_VCHR_NMBR, Date VOU_CNVRSN_DT, 
         double VOU_CNVRSN_RT, double VOU_BLL_AMNT, double VOU_AMNT_DUE, 
         double VOU_AMNT_PD, String VOU_APPRVL_STATUS, String VOU_RSN_FR_RJCTN, byte VOU_PSTD, byte VOU_GNRTD, byte VOU_VD,
	     String VOU_CRTD_BY, Date VOU_DT_CRTD, String VOU_LST_MDFD_BY,
	     Date VOU_DT_LST_MDFD, String VOU_APPRVD_RJCTD_BY, Date VOU_DT_APPRVD_RJCTD,
	     String VOU_PSTD_BY, Date VOU_DT_PSTD, byte VOU_PRNTD, String VOU_PO_NMBR,
	     byte VOU_LN, byte VOU_LN_GNRTD,
		 Integer VOU_AD_BRNCH, Integer VOU_AD_CMPNY)
         throws CreateException { }
}

