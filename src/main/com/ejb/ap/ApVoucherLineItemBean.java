package com.ejb.ap;

import java.util.Collection;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;

import com.ejb.inv.LocalInvCosting;
import com.ejb.inv.LocalInvItemLocation;
import com.ejb.inv.LocalInvUnitOfMeasure;
import com.ejb.pm.LocalPmProjecting;
import com.ejb.pm.LocalPmProject;
import com.ejb.pm.LocalPmProjectPhase;
import com.ejb.pm.LocalPmProjectTypeType;
import com.util.AbstractEntityBean;
import com.util.Debug;

/**
 * @ejb:bean name="ApVoucherLineItemEJB"
 *           display-name="Voucher Line Item Entity"
 *           type="CMP"
 *           cmp-version="2.x"
 *           view-type="local"
 *           local-jndi-name="omega-ejb/ApVoucherLineItemEJB"
 *           schema="ApVoucherLineItem"
 *           primkey-field="vliCode"
 *
 * @ejb:pk class="java.lang.Integer"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="apuser"
 *                        role-link="apuserlink"
 *
 * @ejb:permission role-name="apuser"
 *
 * @ejb:interface local-class="com.ejb.ap.LocalApVoucherLineItem"
 *                local-extends="javax.ejb.EJBLocalObject"
 *
 * @ejb:home local-class="com.ejb.ap.LocalApVoucherLineItemHome"
 *           local-extends="javax.ejb.EJBLocalHome"
 *
 * @ejb:finder signature = "Collection findByVouDebitMemoAndVouPostedAndIlCodeAndBrCode(byte VOU_DBT_MMO, byte VOU_PSTD, java.lang.Integer IL_CODE, java.lang.Integer VOU_AD_BRNCH, java.lang.Integer VLI_AD_CMPNY)"
 * 				query = "SELECT OBJECT(vli) FROM ApVoucherLineItem vli WHERE vli.apVoucher.vouDebitMemo=?1 AND vli.apVoucher.vouPosted=?2 AND vli.invItemLocation.ilCode=?3 AND vli.apVoucher.vouAdBranch=?4 AND vli.vliAdCompany=?5"
 *
 * @ejb:finder signature = "Collection findUnpostedVliPmProject(java.lang.Integer VLI_AD_CMPNY)"
 * 				query = "SELECT OBJECT(vli) FROM ApVoucherLineItem vli WHERE vli.apVoucher.vouPosted=0 AND vli.apVoucher.vouDebitMemo=0 AND vli.pmProject IS NOT NULL AND vli.vliAdCompany=?1"
 *
 * @ejb:finder signature = "Collection findByVouDebitMemoAndVouPostedVouDmVoucherNumberAndIlCodeAndBrCode(byte VOU_DBT_MMO, byte VOU_PSTD, java.lang.String VOU_DCMNT_NMBR, java.lang.Integer IL_CODE, java.lang.Integer VOU_AD_BRNCH, java.lang.Integer VLI_AD_CMPNY)"
 * 				query = "SELECT OBJECT(vli) FROM ApVoucherLineItem vli WHERE vli.apVoucher.vouDebitMemo=?1 AND vli.apVoucher.vouPosted=?2 AND vli.apVoucher.vouDmVoucherNumber=?3 AND vli.invItemLocation.ilCode=?4 AND vli.apVoucher.vouAdBranch=?5 AND vli.vliAdCompany=?6"
 *
 * @ejb:finder signature = "Collection findByVouCodeAndCmpnyCodeAndBrCode(java.lang.Integer VOU_CODE, java.lang.Integer VOU_AD_BRNCH, java.lang.Integer VLI_AD_CMPNY)"
 * 				query = "SELECT OBJECT(vli) FROM ApVoucherLineItem vli WHERE vli.apVoucher.vouCode = ?1 AND vli.apVoucher.vouAdBranch=?2 AND vli.vliAdCompany=?3"
 *
 * @ejb:finder signature = "Collection findByAdCmpnyAll(java.lang.Integer VLI_AD_CMPNY)"
 * 				query = "SELECT OBJECT(vli) FROM ApVoucherLineItem vli WHERE vli.vliAdCompany=?1"
 *
 *
 * @ejb:finder signature="Collection findUnpostedVouByIiNameAndLocNameAndLessEqualDateAndVouAdBranch(java.lang.String II_NM, java.lang.String LOC_NM, java.util.Date VOU_DT, java.lang.Integer VOU_AD_BRNCH, java.lang.Integer VLI_AD_CMPNY)"
 * 			   query="SELECT OBJECT(vli) FROM ApVoucherLineItem vli WHERE vli.apVoucher.vouPosted = 0 AND vli.apVoucher.vouVoid = 0 AND vli.invItemLocation.invItem.iiName = ?1 AND vli.invItemLocation.invLocation.locName = ?2 AND vli.apVoucher.vouDate <= ?3 AND vli.apVoucher.vouAdBranch=?4 AND vli.vliAdCompany = ?5"
 *
 * @ejb:finder signature="Collection findUnpostedChkByIiNameAndLocNameAndLessEqualDateAndChkAdBranch(java.lang.String II_NM, java.lang.String LOC_NM, java.util.Date CHK_DT, java.lang.Integer CHK_AD_BRNCH, java.lang.Integer VLI_AD_CMPNY)"
 * 			   query="SELECT OBJECT(vli) FROM ApVoucherLineItem vli WHERE vli.apCheck.chkPosted = 0 AND vli.apCheck.chkVoid = 0 AND vli.invItemLocation.invItem.iiName = ?1 AND vli.invItemLocation.invLocation.locName = ?2 AND vli.apCheck.chkDate <= ?3 AND vli.apCheck.chkAdBranch = ?4 AND vli.vliAdCompany = ?5"
 *
 *
 * @ejb:finder signature="Collection findUnpostedVouByLocNameAndAdBranch(java.lang.String LOC_NM, java.lang.Integer VOU_AD_BRNCH, java.lang.Integer VLI_AD_CMPNY)"
 * 			   query="SELECT OBJECT(vli) FROM ApVoucherLineItem vli WHERE vli.apVoucher.vouPosted = 0 AND vli.apVoucher.vouVoid = 0 AND vli.invItemLocation.invLocation.locName = ?1 AND vli.apVoucher.vouAdBranch = ?2 AND vli.vliAdCompany = ?3"
 *
 * @ejb:finder signature="Collection findVouByIiCodeAndLocNameAndAdBranch(java.lang.Integer II_CODE, java.lang.String LOC_NM, java.lang.Integer VOU_AD_BRNCH, java.lang.Integer VLI_AD_CMPNY)"
 * 			   query="SELECT OBJECT(vli) FROM ApVoucherLineItem vli WHERE vli.apVoucher.vouVoid = 0 AND vli.invItemLocation.invItem.iiCode=?1 AND vli.invItemLocation.invLocation.locName = ?2 AND vli.apVoucher.vouAdBranch = ?3 AND vli.vliAdCompany = ?4"
 *
 * @ejb:finder signature="Collection findUnpostedChkByLocNameAndAdBranch(java.lang.String LOC_NM, java.lang.Integer CHK_AD_BRNCH, java.lang.Integer VLI_AD_CMPNY)"
 * 			   query="SELECT OBJECT(vli) FROM ApVoucherLineItem vli WHERE vli.apCheck.chkPosted = 0 AND vli.apCheck.chkVoid = 0 AND vli.invItemLocation.invLocation.locName = ?1 AND vli.apCheck.chkAdBranch = ?2 AND vli.vliAdCompany = ?3"
 *
 *
 * @jboss:query signature="Collection ejbSelectGeneric(java.lang.String jbossQl, java.lang.Object[] args)"
 *                         dynamic="true"
 *
 * @ejb:value-object match="*"
 *             name="ApVoucherLineItem"
 *
 * @jboss:persistence table-name="AP_VCHR_LN_ITM"
 *
 * @jboss:entity-command name="mysql-get-generated-keys"
 *
 */

public abstract class ApVoucherLineItemBean extends AbstractEntityBean {

//	Access methods for persistent fields

	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 * @ejb:pk-field
	 *
	 * @jboss:column-name name="VLI_CODE"
	 *
	 * @jboss:persistence auto-increment="true"
	 **/
	public abstract java.lang.Integer getVliCode();
	public abstract void setVliCode(java.lang.Integer VLI_CODE);

	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="VLI_LN"
	 **/
	public abstract short getVliLine();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setVliLine(short VLI_LN);

	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="VLI_QTY"
	 **/
	public abstract double getVliQuantity();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setVliQuantity(double VLI_QTY);

	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="VLI_UNT_CST"
	 **/
	public abstract double getVliUnitCost();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setVliUnitCost(double VLI_UNT_CST);

	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="VLI_AMNT"
	 **/
	public abstract double getVliAmount();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setVliAmount(double VLI_AMNT);

	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="VLI_TX_AMNT"
	 **/
	public abstract double getVliTaxAmount();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setVliTaxAmount(double VLI_TX_AMNT);

	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="VLI_DSCNT_1"
	 **/
	public abstract double getVliDiscount1();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setVliDiscount1(double VLI_DSCNT_1);

	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="VLI_DSCNT_2"
	 **/
	public abstract double getVliDiscount2();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setVliDiscount2(double VLI_DSCNT_2);

	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="VLI_DSCNT_3"
	 **/
	public abstract double getVliDiscount3();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setVliDiscount3(double VLI_DSCNT_3);

	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="VLI_DSCNT_4"
	 **/
	public abstract double getVliDiscount4();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setVliDiscount4(double VLI_DSCNT_4);

	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="VLI_TTL_DSCNT"
	 **/
	public abstract double getVliTotalDiscount();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setVliTotalDiscount(double VLI_TTL_DSCNT);

	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="VLI_MISC"
	 **/
	public abstract String getVliMisc();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setVliMisc(String VLI_MISC);


	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="VLI_SPL_NM"
	 **/
	public abstract String getVliSplName();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setVliSplName(String VLI_SPL_NM);

	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="VLI_SPL_TIN"
	 **/
	public abstract String getVliSplTin();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setVliSplTin(String VLI_SPL_TIN);

	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="VLI_SPL_ADDRSS"
	 **/
	public abstract String getVliSplAddress();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setVliSplAddress(String VLI_SPL_ADDRSS);


	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="VLI_TX"
	 **/
	public abstract byte getVliTax();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setVliTax(byte VLI_TX);

	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="VLI_AD_CMPNY"
	 **/
	public abstract Integer getVliAdCompany();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setVliAdCompany(Integer VLI_AD_CMPNY);

	// Access methods for relationship fields

	/**
	 * @ejb:interface-method view-type="local"
	 * @ejb:relation name="voucher-voucherlineitems"
	 *               role-name="voucherlineitem-has-one-voucher"
	 *               cascade-delete="yes"
	 *
	 * @jboss:relation related-pk-field="vouCode"
	 *                 fk-column="AP_VOUCHER"
	 */
	public abstract LocalApVoucher getApVoucher();
	/**
     * @ejb:interface-method view-type="local"
     **/
	public abstract void setApVoucher(LocalApVoucher apVoucher);

	/**
	 * @ejb:interface-method view-type="local"
	 * @ejb:relation name="check-voucherlineitems"
	 *               role-name="voucherlineitem-has-one-check"
	 *               cascade-delete="yes"
	 *
	 * @jboss:relation related-pk-field="chkCode"
	 *                 fk-column="AP_CHECK"
	 */
	public abstract LocalApCheck getApCheck();
	/**
     * @ejb:interface-method view-type="local"
     **/
	public abstract void setApCheck(LocalApCheck apCheck);

	/**
	 * @ejb:interface-method view-type="local"
	 * @ejb:relation name="unitofmeasure-voucherlineitems"
	 *               role-name="voucherlineitem-has-one-unitofmeasure"
	 *               cascade-delete="no"
	 *
	 * @jboss:relation related-pk-field="uomCode"
	 *                 fk-column="INV_UNIT_OF_MEASURE"
	 */
	public abstract LocalInvUnitOfMeasure getInvUnitOfMeasure();
	/**
     * @ejb:interface-method view-type="local"
     **/
	public abstract void setInvUnitOfMeasure(LocalInvUnitOfMeasure invUnitOfMeasure);

	/**
	 * @ejb:interface-method view-type="local"
	 * @ejb:relation name="itemlocation-voucherlineitems"
	 *               role-name="voucherlineitem-has-one-itemlocation"
	 *               cascade-delete="no"
	 *
	 * @jboss:relation related-pk-field="ilCode"
	 *                 fk-column="INV_ITEM_LOCATION"
	 */
	public abstract LocalInvItemLocation getInvItemLocation();
	/**
     * @ejb:interface-method view-type="local"
     **/
	public abstract void setInvItemLocation(LocalInvItemLocation invItemLocation);

	/**
	 * @ejb:interface-method view-type="local"
	 * @ejb:relation name="voucherlineitem-costings"
	 *               role-name="voucherlineitem-has-many-costings"
	 */
	public abstract Collection getInvCostings();
	public abstract void setInvCostings(Collection invCostings);

	/**
	 * @ejb:interface-method view-type="local"
	 * @ejb:relation name="voucherlineitem-projectings"
	 *               role-name="voucherlineitem-has-many-projectings"
	 */
	public abstract Collection getPmProjectings();
	public abstract void setPmProjectings(Collection pmProjectings);


	/**
	 * @ejb:interface-method view-type="local"
	 * @ejb:relation name="voucherlineitem-tags"
	 *               role-name="voucherlineitem-has-many-tag"
	 */
	public abstract Collection getInvTags();
	public abstract void setInvTags(Collection intTags);

	/**
	 * @jboss:dynamic-ql
	 */
	public abstract Collection ejbSelectGeneric(java.lang.String jbossQl, java.lang.Object[] args)
	throws FinderException;

	// Business methods

	/**
	 * @ejb:home-method view-type="local"
	 */
	public Collection ejbHomeGetVliByCriteria(java.lang.String jbossQl, java.lang.Object[] args)
	throws FinderException {

		return ejbSelectGeneric(jbossQl, args);

	}

	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public void addInvCosting(LocalInvCosting invCosting) {

		Debug.print("ApVoucherLineItemBean addInvCosting");

		try {

			Collection invCostings = getInvCostings();
			invCostings.add(invCosting);

		} catch (Exception ex) {

			throw new EJBException(ex.getMessage());

		}

	}

	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public void dropInvCosting(LocalInvCosting invCosting) {

		Debug.print("ApVoucherLineItemBean dropInvCosting");

		try {

			Collection invCostings = getInvCostings();
			invCostings.remove(invCosting);

		} catch (Exception ex) {

			throw new EJBException(ex.getMessage());

		}

	}



	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public void addPmProjecting(LocalPmProjecting pmProjecting) {

		Debug.print("ApVoucherLineItemBean addPmProjecting");

		try {

			Collection pmProjectings = getInvCostings();
			pmProjectings.add(pmProjecting);

		} catch (Exception ex) {

			throw new EJBException(ex.getMessage());

		}

	}

	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public void dropPmProjecting(LocalPmProjecting pmProjecting) {

		Debug.print("ApVoucherLineItemBean dropPmProjecting");

		try {

			Collection pmProjectings = getInvCostings();
			pmProjectings.remove(pmProjecting);

		} catch (Exception ex) {

			throw new EJBException(ex.getMessage());

		}

	}


	//PMA

		/**
		 * @ejb:interface-method view-type="local"
		 * @ejb:relation name="project-voucherlineitems"
		 *               role-name="voucherlineitem-has-one-project"
		 *
		 * @jboss:relation related-pk-field="prjCode"
		 *                 fk-column="PM_PROJECT"
		 */
		public abstract LocalPmProject getPmProject();
		/**
		 * @ejb:interface-method view-type="local"
		 **/
		public abstract void setPmProject(LocalPmProject pmProject);


		/**
		 * @ejb:interface-method view-type="local"
		 * @ejb:relation name="projecttypetype-voucherlineitems"
		 *               role-name="voucherlineitem-has-one-projecttypetype"
		 *
		 * @jboss:relation related-pk-field="pttCode"
		 *                 fk-column="PM_PROJECT_TYPE_TYPE"
		 */
		public abstract LocalPmProjectTypeType getPmProjectTypeType();
		/**
		 * @ejb:interface-method view-type="local"
		 **/
		public abstract void setPmProjectTypeType(LocalPmProjectTypeType pmProjectTypeType);


		/**
		 * @ejb:interface-method view-type="local"
		 * @ejb:relation name="projectphase-voucherlineitems"
		 *               role-name="voucherlineitem-has-one-projectphase"
		 *
		 * @jboss:relation related-pk-field="ppCode"
		 *                 fk-column="PM_PROJECT_PHASE"
		 */
		public abstract LocalPmProjectPhase getPmProjectPhase();
		/**
		 * @ejb:interface-method view-type="local"
		 **/
		public abstract void setPmProjectPhase(LocalPmProjectPhase pmProjectPhase);

	// EntityBean methods

	/**
	 * @ejb:create-method view-type="local"
	 **/
	public java.lang.Integer ejbCreate (java.lang.Integer VLI_CODE, short VLI_LN, double VLI_QTY, double VLI_UNT_CST,
			double VLI_AMNT, double VLI_TX_AMNT, double VLI_DSCNT_1, double VLI_DSCNT_2,
			double VLI_DSCNT_3, double VLI_DSCNT_4, double TTL_VLI_DSCNT,
			String VLI_SPL_NM, String VLI_SPL_TIN, String VLI_SPL_ADDRSS, byte VLI_TX,
			Integer VLI_AD_CMPNY)
	throws CreateException {

		Debug.print("ApVoucherLineItemBean ejbCreate");
		setVliCode(VLI_CODE);
		setVliLine(VLI_LN);
		setVliQuantity(VLI_QTY);
		setVliUnitCost(VLI_UNT_CST);
		setVliAmount(VLI_AMNT);
		setVliTaxAmount(VLI_TX_AMNT);
		setVliDiscount1(VLI_DSCNT_1);
		setVliDiscount2(VLI_DSCNT_2);
		setVliDiscount3(VLI_DSCNT_3);
		setVliDiscount4(VLI_DSCNT_4);
		setVliTotalDiscount(TTL_VLI_DSCNT);
		setVliSplName(VLI_SPL_NM);
		setVliSplTin(VLI_SPL_TIN);
		setVliSplAddress(VLI_SPL_ADDRSS);
		setVliTax(VLI_TX);
		setVliAdCompany(VLI_AD_CMPNY);

		return null;
	}

	/**
	 * @ejb:create-method view-type="local"
	 **/
	public java.lang.Integer ejbCreate (short VLI_LN, double VLI_QTY, double VLI_UNT_CST,
			double VLI_AMNT, double VLI_TX_AMNT, double VLI_DSCNT_1, double VLI_DSCNT_2,
			double VLI_DSCNT_3, double VLI_DSCNT_4, double TTL_VLI_DSCNT,
			String VLI_SPL_NM, String VLI_SPL_TIN, String VLI_SPL_ADDRSS, byte VLI_TX,
			Integer VLI_AD_CMPNY)
	throws CreateException {

		Debug.print("ApVoucherLineItemBean ejbCreate");
		setVliLine(VLI_LN);
		setVliQuantity(VLI_QTY);
		setVliUnitCost(VLI_UNT_CST);
		setVliAmount(VLI_AMNT);
		setVliTaxAmount(VLI_TX_AMNT);
		setVliDiscount1(VLI_DSCNT_1);
		setVliDiscount2(VLI_DSCNT_2);
		setVliDiscount3(VLI_DSCNT_3);
		setVliDiscount4(VLI_DSCNT_4);
		setVliTotalDiscount(TTL_VLI_DSCNT);
		setVliSplName(VLI_SPL_NM);
		setVliSplTin(VLI_SPL_TIN);
		setVliSplAddress(VLI_SPL_ADDRSS);
		setVliTax(VLI_TX);
		setVliAdCompany(VLI_AD_CMPNY);

		return null;
	}

	public void ejbPostCreate (java.lang.Integer VLI_CODE, short VLI_LN, double VLI_QTY, double VLI_UNT_CST,
			double VLI_AMNT, double VLI_TX_AMNT, double VLI_DSCNT_1, double VLI_DSCNT_2,
			double VLI_DSCNT_3, double VLI_DSCNT_4, double TTL_VLI_DSCNT,
			String VLI_SPL_NM, String VLI_SPL_TIN, String VLI_SPL_ADDRSS, byte VLI_TX,
			Integer VLI_AD_CMPNY)
	throws CreateException { }

	public void ejbPostCreate (short VLI_LN, double VLI_QTY, double VLI_UNT_CST,
			double VLI_AMNT, double VLI_TX_AMNT, double VLI_DSCNT_1, double VLI_DSCNT_2,
			double VLI_DSCNT_3, double VLI_DSCNT_4, double TTL_VLI_DSCNT,
			String VLI_SPL_NM, String VLI_SPL_TIN, String VLI_SPL_ADDRSS, byte VLI_TX,
			Integer VLI_AD_CMPNY)
	throws CreateException { }

}
