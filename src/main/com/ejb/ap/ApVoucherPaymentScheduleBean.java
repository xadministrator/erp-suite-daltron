/*
 * com/ejb/ap/LocalApVoucherPaymentScheduleBean.java
 *
 * Created on February 13, 2004, 3:44 PM
 */

package com.ejb.ap;

import java.util.Collection;
import java.util.Date;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;

import com.util.AbstractEntityBean;
import com.util.Debug;

/**
 *
 * @author  Neil Andrew M. Ajero
 *
 * @ejb:bean name="ApVoucherPaymentScheduleEJB"
 *           display-name="VoucherPaymentSchedule Entity"
 *           type="CMP"
 *           cmp-version="2.x"
 *           view-type="local"
 *           local-jndi-name="omega-ejb/ApVoucherPaymentScheduleEJB"
 *           schema="ApVoucherPaymentSchedule"
 *           primkey-field="vpsCode"
 *
 * @ejb:pk class="java.lang.Integer"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="apuser"
 *                        role-link="apuserlink"
 *
 * @ejb:permission role-name="apuser"
 *
 * @ejb:interface local-class="com.ejb.ap.LocalApVoucherPaymentSchedule"
 *                local-extends="javax.ejb.EJBLocalObject"
 *
 * @ejb:home local-class="com.ejb.ap.LocalApVoucherPaymentScheduleHome"
 *           local-extends="javax.ejb.EJBLocalHome"
 *
 * @ejb:finder signature="Collection findOpenVpsByVpsLock(byte VPS_LCK, java.lang.Integer VOU_AD_BRNCH, java.lang.Integer VPS_AD_CMPNY)"
 *             query="SELECT OBJECT(vps) FROM ApVoucherPaymentSchedule vps WHERE vps.apVoucher.vouType <> 'REQUEST' AND vps.apVoucher.vouPosted = 1 AND (vps.apVoucher.vouApprovalStatus = 'APPROVED' OR vps.apVoucher.vouApprovalStatus = 'N/A') AND vps.vpsLock = ?1 AND vps.vpsAmountDue > vps.vpsAmountPaid AND vps.apVoucher.vouAdBranch=?2 AND vps.vpsAdCompany=?3"
 *
 * @ejb:finder signature="Collection findOpenVpsByVpsLockAndSplSupplierCode(byte VPS_LCK, java.lang.String SPL_SPPLR_CODE, java.lang.Integer VOU_AD_BRNCH, java.lang.Integer VPS_AD_CMPNY)"
 *              query="SELECT OBJECT(vps) FROM ApVoucherPaymentSchedule vps WHERE (vps.apVoucher.vouType NOT LIKE 'REQUEST' OR vps.apVoucher.vouType IS NULL ) AND vps.apVoucher.vouPosted = 1 AND vps.vpsLock = ?1 AND vps.apVoucher.apSupplier.splSupplierCode=?2 AND vps.vpsAmountDue > vps.vpsAmountPaid AND vps.apVoucher.vouAdBranch=?3 AND vps.vpsAdCompany=?4 ORDER BY vps.vpsDueDate"
 *
 * @ejb:finder signature="Collection findByVpsLockAndVouCode(byte VPS_LCK, java.lang.Integer VOU_CODE, java.lang.Integer VPS_AD_CMPNY)"
 *             query="SELECT OBJECT(vps) FROM ApVoucherPaymentSchedule vps WHERE vps.apVoucher.vouType <> 'REQUEST' AND vps.vpsLock = ?1 AND vps.apVoucher.vouCode = ?2 AND vps.vpsAdCompany=?3"
 *
 * @ejb:finder signature="Collection findOpenVpsByVouDocumentNumber(java.lang.String VOU_DCMNT_NMBR, java.lang.Integer VOU_AD_BRNCH, java.lang.Integer VPS_AD_CMPNY)"
 *             query="SELECT OBJECT(vps) FROM ApVoucherPaymentSchedule vps WHERE vps.apVoucher.vouType <> 'REQUEST' AND vps.apVoucher.vouPosted = 1 AND vps.apVoucher.vouDocumentNumber=?1 AND vps.vpsAmountDue > vps.vpsAmountPaid AND vps.apVoucher.vouAdBranch=?2 AND vps.vpsAdCompany=?3"
 *
 * @jboss:query signature="Collection findOpenVpsByVouDocumentNumber(java.lang.String VOU_DCMNT_NMBR, java.lang.Integer VOU_AD_BRNCH, java.lang.Integer VPS_AD_CMPNY)"
 *             query="SELECT OBJECT(vps) FROM ApVoucherPaymentSchedule vps WHERE vps.apVoucher.vouType <> 'REQUEST' AND vps.apVoucher.vouPosted = 1 AND vps.apVoucher.vouDocumentNumber=?1 AND vps.vpsAmountDue > vps.vpsAmountPaid AND vps.apVoucher.vouAdBranch=?2 AND vps.vpsAdCompany=?3 ORDER BY vps.vpsDueDate"
 *
 *  @ejb:finder signature="Collection findOpenVpsByVouCode(java.lang.Integer VOU_CODE, java.lang.Integer VPS_AD_CMPNY)"
 *             query="SELECT OBJECT(vps) FROM ApVoucherPaymentSchedule vps WHERE vps.apVoucher.vouType <> 'REQUEST' AND vps.apVoucher.vouPosted = 1 AND vps.apVoucher.vouCode=?1 AND vps.vpsAdCompany=?2"
 *
 * @jboss:query signature="Collection findOpenVpsByVouCode(java.lang.Integer VOU_CODE, java.lang.Integer VPS_AD_CMPNY)"
 *             query="SELECT OBJECT(vps) FROM ApVoucherPaymentSchedule vps WHERE vps.apVoucher.vouType <> 'REQUEST' AND vps.apVoucher.vouPosted = 1 AND vps.apVoucher.vouCode=?1 AND vps.vpsAdCompany=?2 ORDER BY vps.vpsDueDate"
 *
 *
 *
 * @ejb:finder signature="Collection findByVouCodeAndVouBranchAndAdCmpny(java.lang.Integer VOU_CODE, java.lang.Integer VOU_AD_BRNCH, java.lang.Integer VPS_AD_CMPNY)"
 *             query="SELECT OBJECT(vps) FROM ApVoucherPaymentSchedule vps WHERE vps.apVoucher.vouType <> 'REQUEST' AND vps.apVoucher.vouCode = ?1 AND vps.apVoucher.vouAdBranch=?2 AND vps.vpsAdCompany=?3"
 *
 * @ejb:finder signature="Collection findByAdCmpnyAll(java.lang.Integer VPS_AD_CMPNY)"
 *             query="SELECT OBJECT(vps) FROM ApVoucherPaymentSchedule vps WHERE vps.apVoucher.vouType <> 'REQUEST' AND vps.vpsAdCompany=?1"
 *
 * @ejb:select signature="Collection ejbSelectGeneric(java.lang.String jbossQl, java.lang.Object[] args)"
 *             result-type-mapping="Local"
 *             method-intf="LocalHome"
 *             query="SELECT OBJECT(vps) FROM ApVoucherPaymentSchedule vps"
 *
 * @jboss:query signature="Collection ejbSelectGeneric(java.lang.String jbossQl, java.lang.Object[] args)"
 *                         dynamic="true"
 *
 * @ejb:value-object match="*"
 *             name="ApVoucherPaymentSchedule"
 *
 * @jboss:persistence table-name="AP_VCHR_PYMNT_SCHDL"
 *
 * @jboss:entity-command name="mysql-get-generated-keys"
 */

public abstract class ApVoucherPaymentScheduleBean extends AbstractEntityBean {


    // PERSITCENT METHODS

    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     * @ejb:pk-field
     *
     * @jboss:column-name name="VPS_CODE"
     *
     * @jboss:persistence auto-increment="true"
     **/
    public abstract Integer getVpsCode();
    public abstract void setVpsCode(Integer VPS_CODE);


    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="VPS_DUE_DT"
     **/
    public abstract Date getVpsDueDate();
     /**
      * @ejb:interface-method view-type="local"
      **/
    public abstract void setVpsDueDate(Date VPS_DUE_DT);


    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="VPS_NMBR"
     **/
    public abstract short getVpsNumber();
     /**
      * @ejb:interface-method view-type="local"
      **/
    public abstract void setVpsNumber(short VPS_NMBR);

    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="VPS_AMNT_DUE"
     **/
    public abstract double getVpsAmountDue();
     /**
      * @ejb:interface-method view-type="local"
      **/
    public abstract void setVpsAmountDue(double VPS_AMNT_DUE);

    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="VPS_AMNT_PD"
     **/
    public abstract double getVpsAmountPaid();
     /**
      * @ejb:interface-method view-type="local"
      **/
    public abstract void setVpsAmountPaid(double VPS_AMNT_PD);


    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="VPS_LCK"
     **/
    public abstract byte getVpsLock();
     /**
      * @ejb:interface-method view-type="local"
      **/
    public abstract void setVpsLock(byte VPS_LCK);

    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="VPS_AD_CMPNY"
     **/
    public abstract Integer getVpsAdCompany();
     /**
      * @ejb:interface-method view-type="local"
      **/
    public abstract void setVpsAdCompany(Integer VPS_AD_CMPNY);


    // RELATIONSHIP METHODS

    /**
     * @ejb:interface-method view-type="local"
     * @ejb:relation name="voucher-voucherpaymentschedules"
     *               role-name="voucherpaymentschedule-has-one-voucher"
     *               cascade-delete="yes"
     *
     * @jboss:relation related-pk-field="vouCode"
     *                 fk-column="AP_VOUCHER"
     */
    public abstract LocalApVoucher getApVoucher();
    /**
     * @ejb:interface-method view-type="local"
     **/
    public abstract void setApVoucher(LocalApVoucher apVoucher);

    /**
     * @ejb:interface-method view-type="local"
     * @ejb:relation name="voucherpaymentschedule-appliedvouchers"
     *               role-name="voucherpaymentschedule-has-many-appliedvouchers"
     *
     */
    public abstract Collection getApAppliedVouchers();
    public abstract void setApAppliedVouchers(Collection apAppliedVouchers);


    // BUSINESS METHODS

    /**
     * @ejb:interface-method view-type="local"
     **/
    public void addApAppliedVoucher(LocalApAppliedVoucher apAppliedVoucher) {

         Debug.print("ApVoucherPaymentScheduleBean addApAppliedVoucher");

         try {

            Collection apAppliedVouchers = getApAppliedVouchers();
	        apAppliedVouchers.add(apAppliedVoucher);

         } catch (Exception ex) {

           throw new EJBException(ex.getMessage());

         }
     }

    /**
     * @ejb:interface-method view-type="local"
     **/
    public void dropApAppliedVoucher(LocalApAppliedVoucher apAppliedVoucher) {

         Debug.print("ApVoucherPaymentScheduleBean dropApAppliedVoucher");

         try {

            Collection apAppliedVouchers = getApAppliedVouchers();
	        apAppliedVouchers.remove(apAppliedVoucher);

         } catch (Exception ex) {

           throw new EJBException(ex.getMessage());

         }
     }

   /**
	* @jboss:dynamic-ql
	*/
	public abstract Collection ejbSelectGeneric(java.lang.String jbossQl, java.lang.Object[] args)
	   throws FinderException;

	// BUSINESS METHODS

	/**
	* @ejb:home-method view-type="local"
	*/
	public Collection ejbHomeGetVpsByCriteria(java.lang.String jbossQl, java.lang.Object[] args)
	   throws FinderException {

	   return ejbSelectGeneric(jbossQl, args);
	}



    // ENTITY METHODS

    /**
     * @ejb:create-method view-type="local"
     **/
    public Integer ejbCreate(Integer VPS_CODE,
         Date VPS_DUE_DT, short VPS_NMBR,
         double VPS_AMNT_DUE, double VPS_AMNT_PD, byte VPS_LCK,
		 Integer VPS_AD_CMPNY)
         throws CreateException {

         Debug.print("ApVoucherPaymentScheduleBean ejbCreate");

	     setVpsCode(VPS_CODE);
	     setVpsDueDate(VPS_DUE_DT);
	     setVpsNumber(VPS_NMBR);
	     setVpsAmountDue(VPS_AMNT_DUE);
	     setVpsAmountPaid(VPS_AMNT_PD);
	     setVpsLock(VPS_LCK);
	     setVpsAdCompany(VPS_AD_CMPNY);

         return null;
     }

    /**
     * @ejb:create-method view-type="local"
     **/
    public Integer ejbCreate(
         Date VPS_DUE_DT, short VPS_NMBR,
         double VPS_AMNT_DUE, double VPS_AMNT_PD, byte VPS_LCK,
		 Integer VPS_AD_CMPNY)
         throws CreateException {

         Debug.print("ApVoucherPaymentScheduleBean ejbCreate");

	     setVpsDueDate(VPS_DUE_DT);
	     setVpsNumber(VPS_NMBR);
	     setVpsAmountDue(VPS_AMNT_DUE);
	     setVpsAmountPaid(VPS_AMNT_PD);
	     setVpsLock(VPS_LCK);
	     setVpsAdCompany(VPS_AD_CMPNY);

         return null;
     }

     public void ejbPostCreate(Integer VPS_CODE,
         Date VPS_DUE_DT, short VPS_NMBR,
         double VPS_AMNT_DUE, double VPS_AMNT_PD, byte VPS_LCK,
		 Integer VPS_AD_CMPNY)
         throws CreateException { }

     public void ejbPostCreate(
         Date VPS_DUE_DT, short VPS_NMBR,
         double VPS_AMNT_DUE, double VPS_AMNT_PD, byte VPS_LCK,
		 Integer VPS_AD_CMPNY)
         throws CreateException { }
}