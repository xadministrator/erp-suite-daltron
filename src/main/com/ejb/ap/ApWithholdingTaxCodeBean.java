/*
 * com/ejb/ap/LocalApWithholdingTaxCodeBean.java
 *
 * Created on February 13, 2004, 3:44 PM
 */

package com.ejb.ap;

import java.util.Collection;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;

import com.util.AbstractEntityBean;
import com.util.Debug;

/**
 *
 * @author  Neil Andrew M. Ajero
 *
 * @ejb:bean name="ApWithholdingTaxCodeEJB"
 *           display-name="WithholdingTaxCode Entity"
 *           type="CMP"
 *           cmp-version="2.x"
 *           view-type="local"
 *           local-jndi-name="omega-ejb/ApWithholdingTaxCodeEJB"
 *           schema="ApWithholdingTaxCode"
 *           primkey-field="wtcCode"
 *
 * @ejb:pk class="java.lang.Integer"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="apuser"
 *                        role-link="apuserlink"
 *
 * @ejb:permission role-name="apuser"
 *
 * @ejb:interface local-class="com.ejb.ap.LocalApWithholdingTaxCode"
 *                local-extends="javax.ejb.EJBLocalObject"
 *
 * @ejb:home local-class="com.ejb.ap.LocalApWithholdingTaxCodeHome"
 *           local-extends="javax.ejb.EJBLocalHome"
 *
 * @ejb:finder signature="Collection findEnabledWtcAll(java.lang.Integer WTC_AD_CMPNY)"
 *             query="SELECT OBJECT(wtc) FROM ApWithholdingTaxCode wtc WHERE wtc.wtcEnable = 1 AND wtc.wtcAdCompany = ?1"
 *
 * @jboss:query signature="Collection findEnabledWtcAll(java.lang.Integer WTC_AD_CMPNY)"
 *             query="SELECT OBJECT(wtc) FROM ApWithholdingTaxCode wtc WHERE wtc.wtcEnable = 1 AND wtc.wtcAdCompany = ?1 ORDER BY wtc.wtcName"
 *
 * @ejb:finder signature="LocalApWithholdingTaxCode findByWtcName(java.lang.String WTC_NM, java.lang.Integer WTC_AD_CMPNY)"
 *             query="SELECT OBJECT(wtc) FROM ApWithholdingTaxCode wtc WHERE wtc.wtcName = ?1 AND wtc.wtcAdCompany = ?2"
 *
 * @ejb:finder signature="Collection findWtcAll(java.lang.Integer WTC_AD_CMPNY)"
 *             query="SELECT OBJECT(wtc) FROM ApWithholdingTaxCode wtc WHERE wtc.wtcAdCompany = ?1"
 *
 * @jboss:query signature="Collection findWtcAll(java.lang.Integer WTC_AD_CMPNY)"
 *             query="SELECT OBJECT(wtc) FROM ApWithholdingTaxCode wtc WHERE wtc.wtcAdCompany = ?1 ORDER BY wtc.wtcName"
 *
 * @ejb:select signature="Collection ejbSelectGeneric(java.lang.String jbossQl, java.lang.Object[] args)"
 *             result-type-mapping="Local"
 *             method-intf="LocalHome"
 *             query="SELECT OBJECT(wtc) FROM ApWithholdingTaxCode wtc"
 *
 * @jboss:query signature="Collection ejbSelectGeneric(java.lang.String jbossQl, java.lang.Object[] args)"
 *                         dynamic="true"
 *
 * @ejb:value-object match="*"
 *             name="ApWithholdingTaxCode"
 *
 * @jboss:persistence table-name="AP_WTHHLDNG_TX_CD"
 *
 * @jboss:entity-command name="mysql-get-generated-keys"
 */

public abstract class ApWithholdingTaxCodeBean extends AbstractEntityBean {
    
    
    // PERSITCENT METHODS
    
    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     * @ejb:pk-field
     *
     * @jboss:column-name name="WTC_CODE"
     *
     * @jboss:persistence auto-increment="true"
     **/
    public abstract Integer getWtcCode();
    public abstract void setWtcCode(Integer WTC_CODE);
    
    
    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"   
     *
     * @jboss:column-name name="WTC_NM"
     **/
    public abstract String getWtcName();
     /**
      * @ejb:interface-method view-type="local"
      **/ 
    public abstract void setWtcName(String WTC_NM);
    

    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"   
     *
     * @jboss:column-name name="WTC_DESC"
     **/
    public abstract String getWtcDescription();
     /**
      * @ejb:interface-method view-type="local"
      **/ 
    public abstract void setWtcDescription(String WTC_DESC);

    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"   
     *
     * @jboss:column-name name="WTC_RT"
     **/
    public abstract double getWtcRate();
     /**
      * @ejb:interface-method view-type="local"
      **/ 
    public abstract void setWtcRate(double WTC_RT);
    
    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="WTC_ENBL"
     **/
    public abstract byte getWtcEnable();
     /**
      * @ejb:interface-method view-type="local"
      **/ 
    public abstract void setWtcEnable(byte WTC_ENBL);
    
    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="WTC_AD_CMPNY"
     **/
    public abstract Integer getWtcAdCompany();
     /**
      * @ejb:interface-method view-type="local"
      **/ 
    public abstract void setWtcAdCompany(Integer WTC_AD_CMPNY);
    
   
    // RELATIONSHIP METHODS
    
    /**
     * @ejb:interface-method view-type="local"
     * @ejb:relation name="chartofaccount-withholdingtaxcodes"
     *               role-name="withholdingtaxcode-has-one-chartofaccount"
     * 
     * @jboss:relation related-pk-field="coaCode"
     *                 fk-column="GL_CHART_OF_ACCOUNT"
     */
    public abstract com.ejb.gl.LocalGlChartOfAccount getGlChartOfAccount();
    public abstract void setGlChartOfAccount(com.ejb.gl.LocalGlChartOfAccount glChartOfAccount);


    /**
     * @ejb:interface-method view-type="local"
     * @ejb:relation name="withholdingtaxcode-supplierclasses"
     *               role-name="withholdingtaxcode-has-many-supplierclasses"
     * 
     */
    public abstract Collection getApSupplierClasses();
    public abstract void setApSupplierClasses(Collection apSupplierClasses);    


    /**
     * @ejb:interface-method view-type="local"
     * @ejb:relation name="withholdingtaxcode-vouchers"
     *               role-name="withholdingtaxcode-has-many-vouchers"
     * 
     */
    public abstract Collection getApVouchers();
    public abstract void setApVouchers(Collection apVouchers); 

    /**
     * @ejb:interface-method view-type="local"
     * @ejb:relation name="withholdingtaxcode-checks"
     *               role-name="withholdingtaxcode-has-many-checks"
     * 
     */
    public abstract Collection getApChecks();
    public abstract void setApChecks(Collection apChecks); 

    /**
     * @ejb:interface-method view-type="local"
     * @ejb:relation name="withholdingtaxcode-recurringvouchers"
     *               role-name="withholdingtaxcode-has-many-recurringvouchers"
     * 
     */
    public abstract Collection getApRecurringVouchers();
    public abstract void setApRecurringVouchers(Collection apRecurringVouchers);


    /**
     * @jboss:dynamic-ql
     */
     public abstract Collection ejbSelectGeneric(java.lang.String jbossQl, java.lang.Object[] args)
        throws FinderException;
        
     

    // BUSINESS METHODS
    
    /**
     * @ejb:home-method view-type="local"
     */
     public Collection ejbHomeGetWtcByCriteria(java.lang.String jbossQl, java.lang.Object[] args)
        throws FinderException {
        	
        return ejbSelectGeneric(jbossQl, args);
     }
     
    /**
     * @ejb:interface-method view-type="local"
     **/
    public void addApSupplierClass(LocalApSupplierClass apSupplierClass) {

         Debug.print("ApWithholdingTaxCodeBean addApSupplierClass");
      
         try {
      	
            Collection apSupplierClasses = getApSupplierClasses();
	        apSupplierClasses.add(apSupplierClass);
	          
         } catch (Exception ex) {
      	
           throw new EJBException(ex.getMessage());
      
         }   
     }

    /**
     * @ejb:interface-method view-type="local"
     **/
    public void dropApSupplierClass(LocalApSupplierClass apSupplierClass) {

         Debug.print("ApWithholdingTaxCodeBean dropApSupplierClass");
      
         try {
      	
            Collection apSupplierClasses = getApSupplierClasses();
	        apSupplierClasses.remove(apSupplierClass);
	      
         } catch (Exception ex) {
      	
           throw new EJBException(ex.getMessage());
         
         }   
     }
     

   /**
     * @ejb:interface-method view-type="local"
     **/
    public void addApVoucher(LocalApVoucher apVoucher) {

         Debug.print("ApSupplierBean addApVoucher");
      
         try {
      	
            Collection apVouchers = getApVouchers();
	        apVouchers.add(apVoucher);
	          
         } catch (Exception ex) {
      	
           throw new EJBException(ex.getMessage());
      
         }   
     }

    /**
     * @ejb:interface-method view-type="local"
     **/
    public void dropApVoucher(LocalApVoucher apVoucher) {

         Debug.print("ApSupplierBean dropApVoucher");
      
         try {
      	
            Collection apVouchers = getApVouchers();
	        apVouchers.remove(apVoucher);
	      
         } catch (Exception ex) {
      	
           throw new EJBException(ex.getMessage());
         
         }   
     }
     
    /**
     * @ejb:interface-method view-type="local"
     **/
    public void addApCheck(LocalApCheck apCheck) {

         Debug.print("ApSupplierBean addApCheck");
      
         try {
      	
            Collection apChecks = getApChecks();
	        apChecks.add(apCheck);
	          
         } catch (Exception ex) {
      	
           throw new EJBException(ex.getMessage());
      
         }   
     }

    /**
     * @ejb:interface-method view-type="local"
     **/
    public void dropApCheck(LocalApCheck apCheck) {

         Debug.print("ApSupplierBean dropApCheck");
      
         try {
      	
            Collection apChecks = getApChecks();
	        apChecks.remove(apCheck);
	      
         } catch (Exception ex) {
      	
           throw new EJBException(ex.getMessage());
         
         } 
           
     }
     
    /**
     * @ejb:interface-method view-type="local"
     **/
    public void addApRecurringVoucher(LocalApRecurringVoucher apRecurringVoucher) {

         Debug.print("ApSupplierBean addApRecurringVoucher");
      
         try {
      	
            Collection apRecurringVouchers = getApRecurringVouchers();
	        apRecurringVouchers.add(apRecurringVoucher);
	          
         } catch (Exception ex) {
      	
           throw new EJBException(ex.getMessage());
      
         }   
     }

    /**
     * @ejb:interface-method view-type="local"
     **/
    public void dropApRecurringVoucher(LocalApRecurringVoucher apRecurringVoucher) {

         Debug.print("ApSupplierBean dropApRecurringVoucher");
      
         try {
      	
            Collection apRecurringVouchers = getApRecurringVouchers();
	        apRecurringVouchers.remove(apRecurringVoucher);
	      
         } catch (Exception ex) {
      	
           throw new EJBException(ex.getMessage());
         
         }   
     }          
     

    // ENTITY METHODS
    
    /**
     * @ejb:create-method view-type="local"
     **/
    public Integer ejbCreate(Integer WTC_CODE, 
         String WTC_NM, String WTC_DESC, double WTC_RT, byte WTC_ENBL,
		 Integer WTC_AD_CMPNY)     	
         throws CreateException {	
     	
         Debug.print("ApWithholdingTaxCodeBean ejbCreate");
     
	     setWtcCode(WTC_CODE);
	     setWtcName(WTC_NM);
	     setWtcDescription(WTC_DESC); 
	     setWtcRate(WTC_RT);  
         setWtcEnable(WTC_ENBL);	   
         setWtcAdCompany(WTC_AD_CMPNY);

         return null;
     }
     
    /**
     * @ejb:create-method view-type="local"
     **/
    public Integer ejbCreate(
         String WTC_NM, String WTC_DESC, double WTC_RT, byte WTC_ENBL,
		 Integer WTC_AD_CMPNY)     	
         throws CreateException {	
     	
         Debug.print("ApWithholdingTaxCodeBean ejbCreate");
     
	     setWtcName(WTC_NM);
	     setWtcDescription(WTC_DESC); 
	     setWtcRate(WTC_RT);  
         setWtcEnable(WTC_ENBL);
         setWtcAdCompany(WTC_AD_CMPNY);

         return null;
     }
     
     public void ejbPostCreate(Integer WTC_CODE, 
         String WTC_NM, String WTC_DESC, double WTC_RT, byte WTC_ENBL,
		 Integer WTC_AD_CMPNY)      
         throws CreateException { }
      
     public void ejbPostCreate(
         String WTC_NM, String WTC_DESC, double WTC_RT, byte WTC_ENBL,
		 Integer WTC_AD_CMPNY)
         throws CreateException { }     
}