/*
 * com/ejb/ar/LocalArAppliedCreditBean.java
 *
* Created on March 03, 2004, 3:20 PM
 */

package com.ejb.ar;

import java.util.Collection;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;

import com.ejb.cm.LocalCmAdjustment;
import com.util.AbstractEntityBean;
import com.util.Debug;

/**
 *
 * @author  Clint L. Arrogante
 *
 * @ejb:bean name="ArAppliedCreditEJB"
 *           display-name="AppliedCredit Entity"
 *           type="CMP"
 *           cmp-version="2.x"
 *           view-type="local"
 *           local-jndi-name="omega-ejb/ArAppliedCreditEJB"
 *           schema="ArAppliedCredit"
 *           primkey-field="acCode"
 *
 * @ejb:pk class="java.lang.Integer"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="aruser"
 *                        role-link="aruserlink"
 *
 * @ejb:permission role-name="aruser"
 *
 * @ejb:interface local-class="com.ejb.ar.LocalArAppliedCredit"
 *                local-extends="javax.ejb.EJBLocalObject"
 *
 * @ejb:home local-class="com.ejb.ar.LocalArAppliedCreditHome"
 *           local-extends="javax.ejb.EJBLocalHome"
 * 
 * @ejb:select signature="Collection ejbSelectGeneric(java.lang.String jbossQl, java.lang.Object[] args)"
 *             result-type-mapping="Local"
 *             method-intf="LocalHome"
 *             query="SELECT OBJECT(ac) FROM ArAppliedCredit ac"
 *
 * @jboss:query signature="Collection ejbSelectGeneric(java.lang.String jbossQl, java.lang.Object[] args)"
 *                         dynamic="true"
 *
 * @ejb:value-object match="*"
 *             name="ArAppliedCredit"
 *
 * @jboss:persistence table-name="AR_APPLD_CRDT"
 *
 * @jboss:entity-command name="mysql-get-generated-keys"
 */

public abstract class ArAppliedCreditBean extends AbstractEntityBean {
    
    
    // PERSITCENT METHODS
    
    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     * @ejb:pk-field
     *
     * @jboss:column-name name="AC_CODE"
     *
     * @jboss:persistence auto-increment="true"
     **/
    public abstract Integer getAcCode();
    public abstract void setAcCode(Integer AC_CODE);
    
    
    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"   
     *
     * @jboss:column-name name="AC_APPLY_CRDT"
     **/
    public abstract double getAcApplyCredit();
     /**
      * @ejb:interface-method view-type="local"
      **/ 
    public abstract void setAcApplyCredit(double AC_APPLY_CRDT);
    
    
    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"   
     *
     * @jboss:column-name name="AC_AD_CMPNY"
     **/
    public abstract Integer getAcAdCompany();
     /**
      * @ejb:interface-method view-type="local"
      **/ 
    public abstract void setAcAdCompany(Integer AC_AD_CMPNY);
        
   
    // RELATIONSHIP METHODS

    /**
     * @ejb:interface-method view-type="local"
     * @ejb:relation name="adjustment-appliedcredits"
     *               role-name="appliedcredit-has-one-adjustment"
     *				  cascade-delete="yes"
     * @jboss:relation related-pk-field="adjCode"
     *                 fk-column="CM_ADJUSTMENT"
     */
    public abstract LocalCmAdjustment getCmAdjustment();
    public abstract void setCmAdjustment(LocalCmAdjustment cmAdjustment);
	

    
    

     /**
      * @jboss:dynamic-ql
      */
      public abstract Collection ejbSelectGeneric(java.lang.String jbossQl, java.lang.Object[] args)
         throws FinderException;
      
      
      /**
       * @ejb:interface-method view-type="local"
       * @ejb:relation name="appliedinvoice-appliedcredits"
       *               role-name="appliedcredit-has-one-appliedinvoice"
       * 
       * @jboss:relation related-pk-field="aiCode"
       *                 fk-column="AR_APPLIED_INVOICE"
       */
      public abstract LocalArAppliedInvoice getArAppliedInvoice();
      public abstract void setArAppliedInvoice(LocalArAppliedInvoice arAppliedInvoice);
      

     // BUSINESS METHODS
     
     /**
      * @ejb:home-method view-type="local"
      */
      public Collection ejbHomeGetAcByCriteria(java.lang.String jbossQl, java.lang.Object[] args)
         throws FinderException {
         	
         return ejbSelectGeneric(jbossQl, args);
      }


    // ENTITY METHODS
    
    /**
     * @ejb:create-method view-type="local"
     **/
    public Integer ejbCreate( Integer AC_CODE, double AC_APPLY_CRDT, Integer AC_AD_CMPNY)     	
         throws CreateException {	
     	
         Debug.print("ArAppliedCreditBean ejbCreate");
     
	     setAcCode(AC_CODE);
	     setAcApplyCredit(AC_APPLY_CRDT);	
	     setAcAdCompany(AC_AD_CMPNY);

         return null;
     }
     
    /**
     * @ejb:create-method view-type="local"
     **/
    public Integer ejbCreate( double AC_APPLY_CRDT, Integer AC_AD_CMPNY)     	
         throws CreateException {	
     	
         Debug.print("ArAppliedCreditBean ejbCreate");
     
	     setAcApplyCredit(AC_APPLY_CRDT);
	     setAcAdCompany(AC_AD_CMPNY);

         return null;
     }
     
     public void ejbPostCreate( Integer AC_CODE, double AC_APPLY_CRDT, Integer AC_AD_CMPNY)      
         throws CreateException { }
      
     public void ejbPostCreate( double AC_APPLY_CRDT, Integer AC_AD_CMPNY)
         throws CreateException { }     
}