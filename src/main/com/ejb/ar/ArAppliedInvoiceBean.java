/*
 * com/ejb/ar/LocalArAppliedInvoiceBean.java
 *
* Created on March 03, 2004, 3:20 PM
 */

package com.ejb.ar;

import java.util.Collection;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;

import com.ejb.hr.LocalHrPayrollPeriod;
import com.util.AbstractEntityBean;
import com.util.Debug;

/**
 *
 * @author  Neil Andrew M. Ajero
 *
 * @ejb:bean name="ArAppliedInvoiceEJB"
 *           display-name="AppliedInvoice Entity"
 *           type="CMP"
 *           cmp-version="2.x"
 *           view-type="local"
 *           local-jndi-name="omega-ejb/ArAppliedInvoiceEJB"
 *           schema="ArAppliedInvoice"
 *           primkey-field="aiCode"
 *
 * @ejb:pk class="java.lang.Integer"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="aruser"
 *                        role-link="aruserlink"
 *
 * @ejb:permission role-name="aruser"
 *
 * @ejb:interface local-class="com.ejb.ar.LocalArAppliedInvoice"
 *                local-extends="javax.ejb.EJBLocalObject"
 *
 * @ejb:home local-class="com.ejb.ar.LocalArAppliedInvoiceHome"
 *           local-extends="javax.ejb.EJBLocalHome"
 * 
 * @ejb:finder signature="Collection findPostedAiByRctTypeAndRctDateRange(java.lang.String RCT_TYP, java.util.Date RCT_DT_FRM, java.util.Date RCT_DT_TO, java.lang.Integer AI_AD_CMPNY)"
 *             query="SELECT OBJECT(ai) FROM ArAppliedInvoice ai, IN(ai.arDistributionRecords) dr WHERE ai.arReceipt.rctType = ?1 AND ai.arReceipt.rctDate >= ?2 AND ai.arReceipt.rctDate <= ?3 AND ai.arReceipt.rctPosted = 1 AND ai.arReceipt.rctVoid = 0 AND dr.drDebit = 0 AND dr.drClass = 'TAX' AND ai.aiAdCompany = ?4 ORDER BY ai.arReceipt.rctDate"
 * 
 * @jboss:query signature="Collection findPostedAiByRctTypeAndRctDateRange(java.lang.String RCT_TYP, java.util.Date RCT_DT_FRM, java.util.Date RCT_DT_TO, java.lang.Integer AI_AD_CMPNY)"
 *             query="SELECT OBJECT(ai) FROM ArAppliedInvoice ai, IN(ai.arDistributionRecords) dr WHERE ai.arReceipt.rctType = ?1 AND ai.arReceipt.rctDate >= ?2 AND ai.arReceipt.rctDate <= ?3 AND ai.arReceipt.rctPosted = 1 AND ai.arReceipt.rctVoid = 0 AND dr.drDebit = 0 AND dr.drClass = 'TAX' AND ai.aiAdCompany = ?4 ORDER BY ai.arReceipt.rctDate"
 * 
 * @ejb:finder signature="Collection findPostedAiByIpsCode(java.lang.Integer IPS_CODE, java.lang.Integer AI_AD_CMPNY)"
 *             query="SELECT OBJECT(ai) FROM ArAppliedInvoice ai WHERE ai.arInvoicePaymentSchedule.ipsCode = ?1 AND ai.arReceipt.rctVoid = 0 AND ai.arReceipt.rctPosted = 1 AND ai.aiAdCompany = ?2"
 * 
 * @ejb:finder signature="Collection findPostedAiByRctTypeAndRctDateRangeAndCstNameAndTcType(java.lang.String RCT_TYP, java.util.Date RCT_DT_FRM, java.util.Date RCT_DT_TO, java.lang.String CST_NM, java.lang.String TC_TYP, java.lang.Integer AI_AD_CMPNY)"
 *             query="SELECT OBJECT(ai) FROM ArAppliedInvoice ai, IN(ai.arDistributionRecords) dr WHERE ai.arReceipt.rctType = ?1 AND ai.arReceipt.rctDate >= ?2 AND ai.arReceipt.rctDate <= ?3 AND  ai.arReceipt.arCustomer.cstName = ?4 AND ai.arReceipt.arTaxCode.tcType = ?5 AND ai.arReceipt.rctPosted = 1 AND ai.arReceipt.rctVoid = 0 AND dr.drDebit = 0 AND dr.drClass = 'TAX' AND ai.aiAdCompany = ?6"
 * 
 * @jboss:query signature="Collection findPostedAiByRctTypeAndRctDateRangeAndCstNameAndTcType(java.lang.String RCT_TYP, java.util.Date RCT_DT_FRM, java.util.Date RCT_DT_TO, java.lang.String CST_NM, java.lang.String TC_TYP, java.lang.Integer AI_AD_CMPNY)"
 *             query="SELECT OBJECT(ai) FROM ArAppliedInvoice ai, IN(ai.arDistributionRecords) dr WHERE ai.arReceipt.rctType = ?1 AND ai.arReceipt.rctDate >= ?2 AND ai.arReceipt.rctDate <= ?3 AND  ai.arReceipt.arCustomer.cstName = ?4 AND ai.arReceipt.arTaxCode.tcType = ?5 AND ai.arReceipt.rctPosted = 1 AND ai.arReceipt.rctVoid = 0 AND dr.drDebit = 0 AND dr.drClass = 'TAX' AND ai.aiAdCompany = ?6 ORDER BY ai.arReceipt.rctDate"
 * 
 * @ejb:finder signature="Collection findByIpsCode(java.lang.Integer IPS_CODE, java.lang.Integer AI_AD_CMPNY)"
 * 			   query="SELECT OBJECT(ai) FROM ArAppliedInvoice ai WHERE ai.arInvoicePaymentSchedule.ipsCode=?1 AND ai.aiAdCompany=?2"
 * 
 * @ejb:finder signature="LocalArAppliedInvoice findByPdcCode(java.lang.Integer PDC_CODE, java.lang.Integer AI_AD_CMPNY)"
 *			   query="SELECT OBJECT(ai) FROM ArAppliedInvoice ai WHERE ai.arPdc.pdcCode=?1 AND ai.aiAdCompany=?2"

 * @ejb:finder signature="Collection findByIpsCodeAndRctCode(java.lang.Integer IPS_CODE, java.lang.Integer RCT_CODE, java.lang.Integer AI_AD_CMPNY)"
 * 			   query="SELECT OBJECT(ai) FROM ArAppliedInvoice ai WHERE ai.arInvoicePaymentSchedule.ipsCode=?1 AND ai.arReceipt.rctCode=?2 AND ai.aiAdCompany=?3"
 * 
 * @ejb:finder signature="Collection findUnpostedAiWithDepositByCstCustomerCode(java.lang.String CST_CSTMR_CODE, java.lang.Integer AI_AD_BRNCH, java.lang.Integer AI_AD_CMPNY)"
 * 			   query="SELECT OBJECT(ai) FROM ArAppliedInvoice ai WHERE ai.arReceipt.rctPosted = 0 AND ai.arReceipt.rctVoid = 0 AND ai.aiAppliedDeposit > 0 AND ai.arReceipt.arCustomer.cstCustomerCode = ?1 AND ai.arReceipt.rctAdBranch = ?2 AND ai.aiAdCompany=?3"
 * 
 * @ejb:finder signature="Collection findAiWithCreditBalancePaidByCstCustomerCode(java.lang.String CST_CSTMR_CODE, java.lang.Integer AI_AD_BRNCH, java.lang.Integer AI_AD_CMPNY)"
 * 			   query="SELECT OBJECT(ai) FROM ArAppliedInvoice ai WHERE ai.arReceipt.rctVoid = 0 AND ai.aiCreditBalancePaid > 0 AND ai.arReceipt.arCustomer.cstCustomerCode = ?1 AND ai.arReceipt.rctAdBranch = ?2 AND ai.aiAdCompany=?3"
 * 
 * 
 * @ejb:finder signature="Collection findAiInvNumber(java.lang.String INV_NMBR, java.lang.Integer INV_AD_CMPNY)"
 *             query="SELECT OBJECT(ai) FROM ArAppliedInvoice ai WHERE ai.arReceipt.rctVoid = 0 AND ai.arReceipt.rctPosted = 1 AND ai.arInvoicePaymentSchedule.arInvoice.invNumber = ?1 AND ai.aiAdCompany = ?2"
 * 
 * @jboss:query signature="Collection findAiInvNumber(java.lang.String INV_NMBR, java.lang.Integer INV_AD_CMPNY)"
 *             query="SELECT OBJECT(ai) FROM ArAppliedInvoice ai WHERE ai.arReceipt.rctVoid = 0 AND ai.arReceipt.rctPosted = 1 AND ai.arInvoicePaymentSchedule.arInvoice.invNumber = ?1 AND ai.aiAdCompany = ?2 ORDER BY ai.arReceipt.rctDate"
 * 
 * @ejb:finder signature="Collection findAiPmProject(java.lang.Integer INV_AD_CMPNY)"
 *             query="SELECT OBJECT(ai) FROM ArAppliedInvoice ai WHERE ai.arReceipt.rctVoid = 0 AND ai.arInvoicePaymentSchedule.arInvoice.pmProject IS NOT NULL AND ai.aiAdCompany = ?1"
 * 
 * @jboss:query signature="Collection findAiPmProject(java.lang.Integer INV_AD_CMPNY)"
 *             query="SELECT OBJECT(ai) FROM ArAppliedInvoice ai WHERE ai.arReceipt.rctVoid = 0 AND ai.arInvoicePaymentSchedule.arInvoice.pmProject IS NOT NULL AND ai.aiAdCompany = ?1 ORDER BY ai.arReceipt.rctDate"
 *
 *
 * @ejb:select signature="Collection ejbSelectGeneric(java.lang.String jbossQl, java.lang.Object[] args)"
 *             result-type-mapping="Local"
 *             method-intf="LocalHome"
 *             query="SELECT OBJECT(ai) FROM ArAppliedInvoice ai"
 *
 * @jboss:query signature="Collection ejbSelectGeneric(java.lang.String jbossQl, java.lang.Object[] args)"
 *                         dynamic="true"
 *
 * @ejb:value-object match="*"
 *             name="ArAppliedInvoice"
 *
 * @jboss:persistence table-name="AR_APPLD_INVC"
 *
 * @jboss:entity-command name="mysql-get-generated-keys"
 */

public abstract class ArAppliedInvoiceBean extends AbstractEntityBean {
    
    
    // PERSITCENT METHODS
    
    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     * @ejb:pk-field
     *
     * @jboss:column-name name="AI_CODE"
     *
     * @jboss:persistence auto-increment="true"
     **/
    public abstract Integer getAiCode();
    public abstract void setAiCode(Integer AI_CODE);
    
    
    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"   
     *
     * @jboss:column-name name="AI_APPLY_AMNT"
     **/
    public abstract double getAiApplyAmount();
     /**
      * @ejb:interface-method view-type="local"
      **/ 
    public abstract void setAiApplyAmount(double AI_APPLY_AMNT);
    
    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"   
     *
     * @jboss:column-name name="AI_PNTLY_APPLY_AMNT"
     **/
    public abstract double getAiPenaltyApplyAmount();
     /**
      * @ejb:interface-method view-type="local"
      **/ 
    public abstract void setAiPenaltyApplyAmount(double AI_PNTLY_APPLY_AMNT);
    
    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"   
     *
     * @jboss:column-name name="AI_CRDTBL_W_TX"
     **/
    public abstract double getAiCreditableWTax();
     /**
      * @ejb:interface-method view-type="local"
      **/ 
    public abstract void setAiCreditableWTax(double AI_CRDTBL_W_TX);        

    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"   
     *
     * @jboss:column-name name="AI_DSCNT_AMNT"
     **/
    public abstract double getAiDiscountAmount();
     /**
      * @ejb:interface-method view-type="local"
      **/ 
    public abstract void setAiDiscountAmount(double AI_DSCNT_AMNT);
    
    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"   
     *
     * @jboss:column-name name="AI_RBT"
     **/
    public abstract double getAiRebate();
     /**
      * @ejb:interface-method view-type="local"
      **/ 
    public abstract void setAiRebate(double AI_RBT);
    
    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"   
     *
     * @jboss:column-name name="AI_APPLD_DPST"
     **/
    public abstract double getAiAppliedDeposit();
    /**
     * @ejb:interface-method view-type="local"
     **/ 
   public abstract void setAiAppliedDeposit(double AI_APPLD_DPST);
   
   
   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"   
    *
    * @jboss:column-name name="AI_CRDT_BLNC_PD"
    **/
   public abstract double getAiCreditBalancePaid();
   /**
    * @ejb:interface-method view-type="local"
    **/ 
  public abstract void setAiCreditBalancePaid(double AI_CRDT_BLNC_PD);
   
   

    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"   
     *
     * @jboss:column-name name="AI_ALLCTD_PYMNT_AMNT"
     **/
    public abstract double getAiAllocatedPaymentAmount();
     /**
      * @ejb:interface-method view-type="local"
      **/ 
    public abstract void setAiAllocatedPaymentAmount(double AI_ALLCTD_PYMNT_AMNT);

    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"   
     *
     * @jboss:column-name name="AI_FRX_GN_LSS"
     **/
    public abstract double getAiForexGainLoss();
     /**
      * @ejb:interface-method view-type="local"
      **/ 
    public abstract void setAiForexGainLoss(double AI_FRX_GN_LSS);
    
    /**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="AI_APPLY_RBT"
	 **/
	public abstract byte getAiApplyRebate();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setAiApplyRebate(byte AI_APPLY_RBT);
    
    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"   
     *
     * @jboss:column-name name="AI_AD_CMPNY"
     **/
    public abstract Integer getAiAdCompany();
     /**
      * @ejb:interface-method view-type="local"
      **/ 
    public abstract void setAiAdCompany(Integer AI_AD_CMPNY);
        
   
    // RELATIONSHIP METHODS

    /**
     * @ejb:interface-method view-type="local"
     * @ejb:relation name="receipt-appliedinvoices"
     *               role-name="appliedinvoice-has-one-receipt"
     * 
     * @jboss:relation related-pk-field="rctCode"
     *                 fk-column="AR_RECEIPT"
     */
    public abstract LocalArReceipt getArReceipt();
    public abstract void setArReceipt(LocalArReceipt arReceipt);
    
    
    
    
    /**
     * @ejb:interface-method view-type="local"
     * @ejb:relation name="pdc-appliedinvoices"
     *               role-name="appliedinvoice-has-one-pdc"
     * 
     * @jboss:relation related-pk-field="pdcCode"
     *                 fk-column="AR_PDC"
     */
    public abstract LocalArPdc getArPdc();
    public abstract void setArPdc(LocalArPdc arPdc);
    
    /**
     * @ejb:interface-method view-type="local"
     * @ejb:relation name="invoicepaymentschedule-appliedinvoices"
     *               role-name="appliedinvoice-has-one-invoicepaymentschedule"
     *               cascade-delete="yes"
     * 
     * @jboss:relation related-pk-field="ipsCode"
     *                 fk-column="AR_INVOICE_PAYMENT_SCHEDULE"
     */
    public abstract LocalArInvoicePaymentSchedule getArInvoicePaymentSchedule();
    public abstract void setArInvoicePaymentSchedule(LocalArInvoicePaymentSchedule arInvoicePaymentSchedule); 
    
	
    /**
      * @ejb:interface-method view-type="local"
      * @ejb:relation name="appliedinvoice-ardistributionrecords"
      *               role-name="appliedinvoice-has-many-ardistributionrecords"
      * 
      */
     public abstract Collection getArDistributionRecords();
     public abstract void setArDistributionRecords(Collection arDistributionRecords);
     
     /**
      * @ejb:interface-method view-type="local"
      * @ejb:relation name="appliedinvoice-appliedcredits"
      *               role-name="appliedinvoice-has-many-appliedcredits"
      * 
      */
     public abstract Collection getArAppliedCredits();
     public abstract void setArAppliedCredits(Collection arAppliedCredits);

     /**
      * @jboss:dynamic-ql
      */
      public abstract Collection ejbSelectGeneric(java.lang.String jbossQl, java.lang.Object[] args)
         throws FinderException;

     // BUSINESS METHODS
     
     /**
      * @ejb:home-method view-type="local"
      */
      public Collection ejbHomeGetAiByCriteria(java.lang.String jbossQl, java.lang.Object[] args)
         throws FinderException {
         	
         return ejbSelectGeneric(jbossQl, args);
      }

      
    /**
      * @ejb:interface-method view-type="local"
      **/
     public void addArDistributionRecord(LocalArDistributionRecord arDistributionRecord) {

          Debug.print("ArAppliedInvoiceBean addArDistributionRecord");
      
          try {
      	
             Collection arDistributionRecords = getArDistributionRecords();
	         arDistributionRecords.add(arDistributionRecord);
	          
          } catch (Exception ex) {
      	
            throw new EJBException(ex.getMessage());
      
          }   
     }

     /**
      * @ejb:interface-method view-type="local"
      **/
     public void dropArDistributionRecord(LocalArDistributionRecord arDistributionRecord) {

          Debug.print("ArAppliedInvoiceBean dropArDistributionRecord");
      
          try {
      	
             Collection arDistributionRecords = getArDistributionRecords();
	         arDistributionRecords.remove(arDistributionRecord);
	      
          } catch (Exception ex) {
      	
            throw new EJBException(ex.getMessage());
         
          }
             
     }
     
     
     /**
      * @ejb:interface-method view-type="local"
      **/
     public void addArAppliedCredit(LocalArAppliedCredit arAppliedCredit) {

          Debug.print("ArAppliedCreditBean addArAppliedCredit");
      
          try {
      	
             Collection arAppliedCredits = getArAppliedCredits();
	         arAppliedCredits.add(arAppliedCredit);
	          
          } catch (Exception ex) {
      	
            throw new EJBException(ex.getMessage());
      
          }   
     }

     /**
      * @ejb:interface-method view-type="local"
      **/
     public void dropArAppliedCredit(LocalArAppliedCredit arAppliedCredit) {

          Debug.print("ArAppliedCreditBean dropArAppliedCredit");
      
          try {
      	
             Collection arAppliedCredits = getArAppliedCredits();
	         arAppliedCredits.remove(arAppliedCredit);
	      
          } catch (Exception ex) {
      	
            throw new EJBException(ex.getMessage());
         
          }
             
     }

    // ENTITY METHODS
    
    /**
     * @ejb:create-method view-type="local"
     **/
    public Integer ejbCreate(Integer AI_CODE, 
         double AI_APPLY_AMNT, double AI_PNTLY_APPLY_AMNT, double AI_CRDTBL_W_TX, double AI_DSCNT_AMNT, double AI_RBT, double AI_APPLD_DPST, 
         double AI_ALLCTD_PYMNT_AMNT, double AI_FRX_GN_LSS, 
         byte AI_APPLY_RBT,
         Integer AI_AD_CMPNY)     	
         throws CreateException {	
     	
         Debug.print("ArAppliedInvoiceBean ejbCreate");
     
	     setAiCode(AI_CODE);
	     setAiApplyAmount(AI_APPLY_AMNT);
	     setAiPenaltyApplyAmount(AI_PNTLY_APPLY_AMNT);
	     setAiCreditableWTax(AI_CRDTBL_W_TX);
	     setAiDiscountAmount(AI_DSCNT_AMNT);
	     setAiRebate(AI_RBT);
	     setAiAppliedDeposit(AI_APPLD_DPST);
	     setAiAllocatedPaymentAmount(AI_ALLCTD_PYMNT_AMNT);
	     setAiForexGainLoss(AI_FRX_GN_LSS); 
	     setAiApplyRebate(AI_APPLY_RBT);
	     setAiAdCompany(AI_AD_CMPNY);

         return null;
     }
     
    /**
     * @ejb:create-method view-type="local"
     **/
    public Integer ejbCreate( 
         double AI_APPLY_AMNT, double AI_PNTLY_APPLY_AMNT, double AI_CRDTBL_W_TX, double AI_DSCNT_AMNT, double AI_RBT, double AI_APPLD_DPST,  
         double AI_ALLCTD_PYMNT_AMNT, double AI_FRX_GN_LSS, 
         byte AI_APPLY_RBT,
         Integer AI_AD_CMPNY)     	
         throws CreateException {	
     	
         Debug.print("ArAppliedInvoiceBean ejbCreate");
     
	     setAiApplyAmount(AI_APPLY_AMNT);
	     setAiPenaltyApplyAmount(AI_PNTLY_APPLY_AMNT);
	     setAiCreditableWTax(AI_CRDTBL_W_TX);
	     setAiDiscountAmount(AI_DSCNT_AMNT);
	     setAiRebate(AI_RBT);
	     setAiAppliedDeposit(AI_APPLD_DPST);
	     setAiAllocatedPaymentAmount(AI_ALLCTD_PYMNT_AMNT);
	     setAiForexGainLoss(AI_FRX_GN_LSS);
	     setAiApplyRebate(AI_APPLY_RBT);
	     setAiAdCompany(AI_AD_CMPNY);

         return null;
     }
     
     public void ejbPostCreate(Integer AI_CODE, 
         double AI_APPLY_AMNT, double AI_PNTLY_APPLY_AMNT, double AI_CRDTBL_W_TX, double AI_DSCNT_AMNT, double AI_RBT, double AI_APPLD_DPST,
         double AI_ALLCTD_PYMNT_AMNT, double AI_FRX_GN_LSS, 
         byte AI_APPLY_RBT,
         Integer AI_AD_CMPNY)      
         throws CreateException { }
      
     public void ejbPostCreate( 
         double AI_APPLY_AMNT, double AI_PNTLY_APPLY_AMNT, double AI_CRDTBL_W_TX, double AI_DSCNT_AMNT, double AI_RBT, double AI_APPLD_DPST,
         double AI_ALLCTD_PYMNT_AMNT, double AI_FRX_GN_LSS, 
         byte AI_APPLY_RBT,
         Integer AI_AD_CMPNY)
         throws CreateException { }     
}