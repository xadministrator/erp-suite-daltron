/*
 * com/ejb/ar/LocalArAutoAccountingBean.java
 *
 * Created on January 9, 2003, 9:41 AM
 */

package com.ejb.ar;

import java.util.Collection;

import javax.ejb.CreateException;
import javax.ejb.EJBException;

import com.util.AbstractEntityBean;
import com.util.Debug;

/**
 *
 * @author  Neil Andrew M. Ajero
 *
 * @ejb:bean name="ArAutoAccountingEJB"
 *           display-name="Auto Accounting Entity"
 *           type="CMP"
 *           cmp-version="2.x"
 *           view-type="local"
 *           local-jndi-name="omega-ejb/ArAutoAccountingEJB"
 *           schema="ArAutoAccounting"
 *           primkey-field="aaCode"
 *
 * @ejb:pk class="java.lang.Integer"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="aruser"
 *                        role-link="aruserlink"
 *
 * @ejb:permission role-name="aruser"
 *
 * @ejb:interface local-class="com.ejb.ar.LocalArAutoAccounting"
 *                local-extends="javax.ejb.EJBLocalObject"
 *
 * @ejb:home local-class="com.ejb.ar.LocalArAutoAccountingHome"
 *           local-extends="javax.ejb.EJBLocalHome"
 *
 * @ejb:finder signature="Collection findAaAll(java.lang.Integer AA_AD_CMPNY)"
 *             query="SELECT OBJECT(aa) FROM ArAutoAccounting aa WHERE aa.aaAdCompany = ?1"
 *
 * @jboss:query signature="Collection findAaAll(java.lang.Integer AA_AD_CMPNY)"
 *             query="SELECT OBJECT(aa) FROM ArAutoAccounting aa WHERE aa.aaAdCompany = ?1 ORDER BY aa.aaCode"
 *
 * @ejb:finder signature="LocalArAutoAccounting findByAaAccountType(java.lang.String AA_ACCNT_TYP, java.lang.Integer AA_AD_CMPNY)"
 *             query="SELECT OBJECT(aa) FROM ArAutoAccounting aa WHERE aa.aaAccountType = ?1 AND aa.aaAdCompany = ?2"
 *
 * @ejb:value-object match="*"
 *             name="ArAutoAccounting"
 *
 * @jboss:persistence table-name="AR_AT_ACCNTNG"
 *
 * @jboss:entity-command name="mysql-get-generated-keys"
 */

public abstract class ArAutoAccountingBean extends AbstractEntityBean {
    
    
    // PERSISTENT METHODS
    
    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     * @ejb:pk-field
     *
     * @jboss:column-name name="AA_CODE"
     *
     * @jboss:persistence auto-increment="true"
     **/
    public abstract Integer getAaCode();
    public abstract void setAaCode(Integer AA_CODE);
    
    
    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="AA_ACCNT_TYP"
     **/
    public abstract String getAaAccountType();
     /**
      * @ejb:interface-method view-type="local"
      **/      
    public abstract void setAaAccountType(String AA_ACCNT_TYP);
    
    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="AA_AD_CMPNY"
     **/
    public abstract Integer getAaAdCompany();
     /**
      * @ejb:interface-method view-type="local"
      **/      
    public abstract void setAaAdCompany(Integer AA_AD_CMPNY);
    
    // RELATIONSHIP METHODS
    
    /**
     * @ejb:interface-method view-type="local"
     * @ejb:relation name="autoaccounting-autoaccountingsegments"
     *               role-name="autoaccounting-has-many-autoaccountingsegments"
     * 
     */
     public abstract Collection getArAutoAccountingSegments();
     public abstract void setArAutoAccountingSegments(Collection arAutoAccountingSegments);
     
    // BUSINESS METHODS
     
    /**
     * @ejb:interface-method view-type="local"
     **/
     public void addArAutoAccountingSegment(LocalArAutoAccountingSegment arAutoAccountingSegment) {

         Debug.print("ArAutoAccountingBean addArAutoAccountingSegment");
      
         try {
      	
            Collection arAutoAccountingSegments = getArAutoAccountingSegments();
	        arAutoAccountingSegments.add(arAutoAccountingSegment);
	          
         } catch (Exception ex) {
      	
           throw new EJBException(ex.getMessage());
      
         }   
     }

    /**
     * @ejb:interface-method view-type="local"
     **/
     public void dropArAutoAccountingSegment(LocalArAutoAccountingSegment arAutoAccountingSegment) {

         Debug.print("ArAutoAccountingBean dropArAutoAccountingSegment");
      
         try {
      	
            Collection arAutoAccountingSegments = getArAutoAccountingSegments();
	        arAutoAccountingSegments.remove(arAutoAccountingSegment);
	      
         } catch (Exception ex) {
      	
           throw new EJBException(ex.getMessage());
         
         }   
     }
     
     // ENTITY METHODS
    
    /**
     * @ejb:create-method view-type="local"
     **/
     public Integer ejbCreate(Integer AA_CODE, 
         String AA_ACCNT_TYP, Integer AA_AD_CMPNY)
         throws CreateException {
      	
         Debug.print("ArAutoAccountingBean ejbCreate");
      
         setAaCode(AA_CODE);
         setAaAccountType(AA_ACCNT_TYP);
         setAaAdCompany(AA_AD_CMPNY);
        
         return null;
     }
    
    /**
     * @ejb:create-method view-type="local"
     **/
     public Integer ejbCreate(
         String AA_ACCNT_TYP, Integer AA_AD_CMPNY)
         throws CreateException {
       	
         Debug.print("ArAutoAccountingBean ejbCreate");
      
         setAaAccountType(AA_ACCNT_TYP);
         setAaAdCompany(AA_AD_CMPNY);
         
         return null;
     }
    
     public void ejbPostCreate(Integer AA_CODE, 
         String AA_ACCNT_TYP, Integer AA_AD_CMPNY)      
         throws CreateException { }
      
     public void ejbPostCreate(
         String AA_ACCNT_TYP, Integer AA_AD_CMPNY)
         throws CreateException { }     
}