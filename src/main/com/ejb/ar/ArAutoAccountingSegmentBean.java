/*
 * com/ejb/ar/LocalArAutoAccountingSegmentBean.java
 *
 * Created on January 9, 2003, 10:02 AM
 */

package com.ejb.ar;

import javax.ejb.CreateException;

import com.util.AbstractEntityBean;
import com.util.Debug;

/**
 *
 * @author  Neil Andrew M. Ajero
 *
 * @ejb:bean name="ArAutoAccountingSegmentEJB"
 *           display-name="Auto Accounting Segment Entity"
 *           type="CMP"
 *           cmp-version="2.x"
 *           view-type="local"
 *           local-jndi-name="omega-ejb/ArAutoAccountingSegmentEJB"
 *           schema="ArAutoAccountingSegment"
 *           primkey-field="aasCode"
 *
 * @ejb:pk class="java.lang.Integer"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="aruser"
 *                        role-link="aruserlink"
 *
 * @ejb:permission role-name="aruser"
 *
 * @ejb:interface local-class="com.ejb.ar.LocalArAutoAccountingSegment"
 *                local-extends="javax.ejb.EJBLocalObject"
 *
 * @ejb:home local-class="com.ejb.ar.LocalArAutoAccountingSegmentHome"
 *           local-extends="javax.ejb.EJBLocalHome"
 *
 * @ejb:finder signature="Collection findByAaAccountType(java.lang.String AA_ACCNT_TYP, java.lang.Integer AAS_AD_CMPNY)"
 *            query="SELECT OBJECT(aas) FROM ArAutoAccounting aa, IN(aa.arAutoAccountingSegments) aas WHERE aa.aaAccountType = ?1 AND aas.aasAdCompany = ?2"
 *
 * @jboss:query signature="Collection findByAaAccountType(java.lang.String AA_ACCNT_TYP, java.lang.Integer AAS_AD_CMPNY)"
 *            query="SELECT OBJECT(aas) FROM ArAutoAccounting aa, IN(aa.arAutoAccountingSegments) aas WHERE aa.aaAccountType = ?1 AND aas.aasAdCompany = ?2 ORDER BY aas.aasSegmentNumber"
 *
 * @ejb:finder signature="LocalArAutoAccountingSegment findByAasSegmentNumberAndAaAccountType(short AAS_SGMNT_NMBR, java.lang.String AA_ACCNT_TYP, java.lang.Integer AAS_AD_CMPNY)"
 *             query="SELECT OBJECT(aas) FROM ArAutoAccounting aa, IN(aa.arAutoAccountingSegments) aas WHERE aas.aasSegmentNumber=?1 AND aa.aaAccountType=?2 AND aas.aasAdCompany = ?3"
 *
 * @ejb:finder signature="Collection findByAasClassTypeAndAaAccountType(java.lang.String AAS_CLSS_TYP, java.lang.String AA_ACCNT_TYP, java.lang.Integer AAS_AD_CMPNY)"
 *            query="SELECT OBJECT(aas) FROM ArAutoAccounting aa, IN(aa.arAutoAccountingSegments) aas WHERE aas.aasClassType=?1 AND aa.aaAccountType = ?2 AND aas.aasAdCompany = ?3"
 *
 * @jboss:query signature="Collection findByAasClassTypeAndAaAccountType(java.lang.String AAS_CLSS_TYP, java.lang.String AA_ACCNT_TYP, java.lang.Integer AAS_AD_CMPNY)"
 *            query="SELECT OBJECT(aas) FROM ArAutoAccounting aa, IN(aa.arAutoAccountingSegments) aas WHERE aas.aasClassType=?1 AND aa.aaAccountType = ?2 AND aas.aasAdCompany = ?3 ORDER BY aas.aasSegmentNumber"
 *
 *
 * @ejb:value-object match="*"
 *             name="ArAutoAccountingSegment"
 *
 * @jboss:persistence table-name="AR_AT_ACCNTNG_SGMNT"
 *
 * @jboss:entity-command name="mysql-get-generated-keys"
 */

public abstract class ArAutoAccountingSegmentBean extends AbstractEntityBean {
    
    
    // PERSISTENT METHODS
    
    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     * @ejb:pk-field
     *
     * @jboss:column-name name="AAS_CODE"
     *
     * @jboss:persistence auto-increment="true"
     **/
    public abstract Integer getAasCode();
    public abstract void setAasCode(Integer AA_CODE);
    
    
    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="AAS_SGMNT_NMBR"
     **/
    public abstract short getAasSegmentNumber();
     /**
      * @ejb:interface-method view-type="local"
      **/  
    public abstract void setAasSegmentNumber(short AAS_SGMNT_NMBR);
    
    
    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="AAS_AD_CMPNY"
     **/
    public abstract Integer getAasAdCompany();
     /**
      * @ejb:interface-method view-type="local"
      **/  
    public abstract void setAasAdCompany(Integer AAS_AD_CMPNY);
    
    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="AAS_CLSS_TYP"
     **/
    public abstract String getAasClassType();
     /**
      * @ejb:interface-method view-type="local"
      **/  
    public abstract void setAasClassType(String AAS_CLSS_TYP);
    
    
    
    // RELATIONSHIP METHODS
    
    /**
     * @ejb:interface-method view-type="local"
     * @ejb:relation name="autoaccounting-autoaccountingsegments"
     *               role-name="autoaccountingsegment-has-one-autoaccounting"
     * 
     * @jboss:relation related-pk-field="aaCode"
     *                 fk-column="AR_AUTO_ACCOUNTING"
     */
     public abstract LocalArAutoAccounting getArAutoAccounting();
     public abstract void setArAutoAccounting(LocalArAutoAccounting arAutoAccounting);
     
     
          
     
    // BUSINESS METHODS  
     
    // ENTITY METHODS
    
    /**
     * @ejb:create-method view-type="local"
     **/
     public Integer ejbCreate(Integer AAS_CODE, 
         short AAS_SGMNT_NMBR, String AAS_CLSS_TYP, Integer AAS_AD_CMPNY)
         throws CreateException {
      	
         Debug.print("ArAutoAccountingSegmentBean ejbCreate");
      
         setAasCode(AAS_CODE);
         setAasSegmentNumber(AAS_SGMNT_NMBR);
         setAasClassType(AAS_CLSS_TYP);
         setAasAdCompany(AAS_AD_CMPNY);
                 
         return null;
     }
    
    /**
     * @ejb:create-method view-type="local"
     **/
     public Integer ejbCreate(
         short AAS_SGMNT_NMBR, String AAS_CLSS_TYP, Integer AAS_AD_CMPNY)
         throws CreateException {
       	
         Debug.print("ArAutoAccountingSegmentBean ejbCreate");
      
         setAasSegmentNumber(AAS_SGMNT_NMBR);
         setAasClassType(AAS_CLSS_TYP);
         setAasAdCompany(AAS_AD_CMPNY);
         
         return null;
     }
    
     public void ejbPostCreate(Integer AAS_CODE, 
         short AAS_SGMNT_NMBR, String AAS_CLSS_TYP, Integer AAS_AD_CMPNY)      
         throws CreateException { }
      
     public void ejbPostCreate( 
         short AAS_SGMNT_NMBR, String AAS_CLSS_TYP, Integer AAS_AD_CMPNY)
         throws CreateException { }     
}