/*
 * com/ejb/ar/LocalArCustomerBalanceBean.java
 *
 * Created on March 03, 2004, 2:03 PM
 */

package com.ejb.ar;

import java.util.Date;

import javax.ejb.CreateException;

import com.util.AbstractEntityBean;
import com.util.Debug;

/**
 *
 * @author  Neil Andrew M. Ajero
 *
 * @ejb:bean name="ArCustomerBalanceEJB"
 *           display-name="CustomerBalance Entity"
 *           type="CMP"
 *           cmp-version="2.x"
 *           view-type="local"
 *           local-jndi-name="omega-ejb/ArCustomerBalanceEJB"
 *           schema="ArCustomerBalance"
 *           primkey-field="cbCode"
 *
 * @ejb:pk class="java.lang.Integer"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="aruser"
 *                        role-link="aruserlink"
 *
 * @ejb:permission role-name="aruser"
 *
 * @ejb:interface local-class="com.ejb.ar.LocalArCustomerBalance"
 *                local-extends="javax.ejb.EJBLocalObject"
 *
 * @ejb:home local-class="com.ejb.ar.LocalArCustomerBalanceHome"
 *           local-extends="javax.ejb.EJBLocalHome"
 *
 * @ejb:finder signature="Collection findByBeforeOrEqualInvDateAndCstCustomerCode(java.util.Date INV_DT, java.lang.String CST_CSTMR_CODE, java.lang.Integer CB_AD_CMPNY)"
 *             query="SELECT OBJECT(cb) FROM ArCustomerBalance cb  WHERE cb.cbDate <= ?1 AND cb.arCustomer.cstCustomerCode = ?2 AND cb.cbAdCompany = ?3"
 *
 * @jboss:query signature="Collection findByBeforeOrEqualInvDateAndCstCustomerCode(java.util.Date INV_DT, java.lang.String CST_CSTMR_CODE, java.lang.Integer CB_AD_CMPNY)"
 *             query="SELECT OBJECT(cb) FROM ArCustomerBalance cb  WHERE cb.cbDate <= ?1 AND cb.arCustomer.cstCustomerCode = ?2 AND cb.cbAdCompany = ?3 ORDER BY cb.cbDate"
 *
 * @ejb:finder signature="Collection findByAfterInvDateAndCstCustomerCode(java.util.Date INV_DT, java.lang.String CST_CSTMR_CODE, java.lang.Integer CB_AD_CMPNY)"
 *             query="SELECT OBJECT(cb) FROM ArCustomerBalance cb  WHERE cb.cbDate > ?1 AND cb.arCustomer.cstCustomerCode = ?2 AND cb.cbAdCompany = ?3"
 *
 * @jboss:query signature="Collection findByAfterInvDateAndCstCustomerCode(java.util.Date INV_DT, java.lang.String CST_CSTMR_CODE, java.lang.Integer CB_AD_CMPNY)"
 *             query="SELECT OBJECT(cb) FROM ArCustomerBalance cb  WHERE cb.cbDate > ?1 AND cb.arCustomer.cstCustomerCode = ?2 AND cb.cbAdCompany = ?3 ORDER BY cb.cbDate"
 *
 * @ejb:finder signature="Collection findByBeforeOrEqualCbDateAndCstCode(java.util.Date CB_DT, java.lang.Integer CST_CODE, java.lang.Integer CB_AD_CMPNY)"
 *             query="SELECT OBJECT(cb) FROM ArCustomerBalance cb  WHERE cb.cbDate <= ?1 AND cb.arCustomer.cstCode = ?2 AND cb.cbAdCompany = ?3"
 *
 * @jboss:query signature="Collection findByBeforeOrEqualCbDateAndCstCode(java.util.Date CB_DT, java.lang.Integer CST_CODE, java.lang.Integer CB_AD_CMPNY)"
 *             query="SELECT OBJECT(cb) FROM ArCustomerBalance cb  WHERE cb.cbDate <= ?1 AND cb.arCustomer.cstCode = ?2 AND cb.cbAdCompany = ?3 ORDER BY cb.cbDate"
 *
 * @ejb:finder signature="Collection findByCstCustomerCode(java.lang.String CST_CSTMR_CODE, java.lang.Integer CB_AD_CMPNY)"
 * 			   query="SELECT OBJECT(cb) FROM ArCustomerBalance cb WHERE cb.arCustomer.cstCustomerCode = ?1 AND cb.cbAdCompany = ?2"
 * 
 * @jboss.:query signature="Collection findByCstCustomerCode(java.lang.String CST_CSTMR_CODE, java.lang.Integer CB_AD_CMPNY)"
 * 			   query="SELECT OBJECT(cb) FROM ArCustomerBalance cb WHERE cb.arCustomer.cstCustomerCode = ?1 AND cb.cbAdCompany = ?2 ORDER BY cb.cbDate"
 * @ejb:value-object match="*"
 *             name="ArCustomerBalance"
 *
 * @jboss:persistence table-name="AR_CSTMR_BLNC"
 *
 * @jboss:entity-command name="mysql-get-generated-keys"
 */

public abstract class ArCustomerBalanceBean extends AbstractEntityBean {
    
    
    // PERSISTENT METHODS
    
    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     * @ejb:pk-field
     *
     * @jboss:column-name name="CB_CODE"
     *
     * @jboss:persistence auto-increment="true"
     **/
    public abstract Integer getCbCode();
    public abstract void setCbCode(Integer CB_CODE);
    
    
    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"   
     *
     * @jboss:column-name name="CB_DT"
     **/
    public abstract Date getCbDate();
     /**
      * @ejb:interface-method view-type="local"
      **/ 
    public abstract void setCbDate(Date CB_DT);
    

    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"   
     *
     * @jboss:column-name name="CB_BLNC"
     **/
    public abstract double getCbBalance();
     /**
      * @ejb:interface-method view-type="local"
      **/ 
    public abstract void setCbBalance(double CB_BLNC);
    
    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"   
     *
     * @jboss:column-name name="CB_AD_CMPNY"
     **/
    public abstract Integer getCbAdCompany();
     /**
      * @ejb:interface-method view-type="local"
      **/ 
    public abstract void setCbAdCompany(Integer CB_AD_CMPNY);
    
   
    // RELATIONSHIP METHODS
    
    /**
     * @ejb:interface-method view-type="local"
     * @ejb:relation name="customer-customerbalances"
     *               role-name="customerbalance-has-one-customer"
     * 
     * @jboss:relation related-pk-field="cstCode"
     *                 fk-column="AR_CUSTOMER"
     */
    public abstract LocalArCustomer getArCustomer();
    /**
     * @ejb:interface-method view-type="local"
     **/ 
    public abstract void setArCustomer(LocalArCustomer arCustomer);


    // NO BUSINESS METHODS
     
 
    // ENTITY METHODS
    
    /**
     * @ejb:create-method view-type="local"
     **/
    public Integer ejbCreate(Integer CB_CODE, 
         Date CB_DT, double CB_BLNC, Integer CB_AD_CMPNY)     	
         throws CreateException {	
     	
         Debug.print("ArCustomerBalanceBean ejbCreate");
     
	     setCbCode(CB_CODE);
	     setCbDate(CB_DT);
	     setCbBalance(CB_BLNC);  
	     setCbAdCompany(CB_AD_CMPNY);

         return null;
     }
     
    /**
     * @ejb:create-method view-type="local"
     **/
    public Integer ejbCreate( 
         Date CB_DT, double CB_BLNC, Integer CB_AD_CMPNY)     	
         throws CreateException {	
     	
         Debug.print("ArCustomerBalanceBean ejbCreate");
     
	     setCbDate(CB_DT);
	     setCbBalance(CB_BLNC); 
	     setCbAdCompany(CB_AD_CMPNY);

         return null;
     }
     
     public void ejbPostCreate(Integer CB_CODE, 
         Date CB_DT, double CB_BLNC, Integer CB_AD_CMPNY)      
         throws CreateException { }
      
     public void ejbPostCreate( 
         Date CB_DT, double CB_BLNC, Integer CB_AD_CMPNY)
         throws CreateException { }     
}