/*
 * com/ejb/ar/LocalArCustomerBean.java
 *
 * Created on March 03, 2004, 2:46 PM
 */

package com.ejb.ar;

import java.util.Collection;
import java.util.Date;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;

import com.ejb.ad.LocalAdBranchCustomer;
import com.ejb.ap.LocalApSupplier;
import com.ejb.cm.LocalCmAdjustment;
import com.ejb.hr.LocalHrDeployedBranch;
import com.ejb.hr.LocalHrEmployee;
import com.ejb.inv.LocalInvItem;
import com.ejb.inv.LocalInvLineItemTemplate;
import com.ejb.pm.LocalPmUser;
import com.util.AbstractEntityBean;
import com.util.Debug;

/**
 *
 * @author  Neil Andrew M. Ajero
 *
 * @ejb:bean name="ArCustomerEJB"
 *           display-name="Customer Entity"
 *           type="CMP"
 *           cmp-version="2.x"
 *           view-type="local"
 *           local-jndi-name="omega-ejb/ArCustomerEJB"
 *           schema="ArCustomer"
 *           primkey-field="cstCode"
 *
 * @ejb:pk class="java.lang.Integer"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="aruser"
 *                        role-link="aruserlink"
 *
 * @ejb:permission role-name="aruser"
 *
 * @ejb:interface local-class="com.ejb.ar.LocalArCustomer"
 *                local-extends="javax.ejb.EJBLocalObject"
 *
 * @ejb:home local-class="com.ejb.ar.LocalArCustomerHome"
 *           local-extends="javax.ejb.EJBLocalHome"
 *
 * @ejb:finder signature="Collection findEnabledCstAll(java.lang.Integer CST_AD_BRNCH, java.lang.Integer CST_AD_CMPNY)"
 *			   query="SELECT OBJECT(cst) FROM ArCustomer cst, IN(cst.adBranchCustomers) bcst WHERE cst.cstEnable = 1 AND bcst.bcstAdBranch=?1 AND cst.cstAdCompany = ?2"
 *
 * @jboss:query signature="Collection findEnabledCstAll(java.lang.Integer CST_AD_BRNCH, java.lang.Integer CST_AD_CMPNY)"
 *             query="SELECT OBJECT(cst) FROM ArCustomer cst, IN(cst.adBranchCustomers) bcst WHERE cst.cstEnable = 1 AND bcst.bcstAdBranch=?1 AND cst.cstAdCompany = ?2 ORDER BY cst.cstCustomerCode"
 *
 * 
 * @ejb:finder signature="Collection findCstHrCashBondByCstHrDbReferenceID(java.lang.Integer DB_REF_ID, java.lang.Integer CST_AD_CMPNY)"
 *			   query="SELECT OBJECT(cst) FROM ArCustomer cst WHERE cst.cstHrEnableCashBond = 1 AND cst.cstHrCashBondAmount > 0 AND cst.cstEnable = 1 AND cst.hrDeployedBranch.dbReferenceID = ?1 AND cst.cstAdCompany = ?2"
 *
 * @jboss:query signature="Collection findCstHrCashBondByCstHrDbReferenceID(java.lang.Integer DB_REF_ID, java.lang.Integer CST_AD_CMPNY)"
 *             query="SELECT OBJECT(cst) FROM ArCustomer cst WHERE cst.cstHrEnableCashBond = 1 AND cst.cstHrCashBondAmount > 0 AND cst.cstEnable = 1 AND cst.hrDeployedBranch.dbReferenceID = ?1 AND cst.cstAdCompany = ?2 ORDER BY cst.cstCustomerCode"
 *
 *
 * @ejb:finder signature="Collection findCstHrCashBondByCstHrAll(java.lang.Integer CST_AD_CMPNY)"
 *			   query="SELECT OBJECT(cst) FROM ArCustomer cst WHERE cst.cstHrEnableCashBond = 1 AND cst.cstHrCashBondAmount > 0 AND cst.cstEnable = 1 AND cst.hrEmployee IS NOT NULL AND cst.cstAdCompany = ?1"
 *
 * @jboss:query signature="Collection findCstHrCashBondByCstHrAll(java.lang.Integer CST_AD_CMPNY)"
 *             query="SELECT OBJECT(cst) FROM ArCustomer cst WHERE cst.cstHrEnableCashBond = 1 AND cst.cstHrCashBondAmount > 0 AND cst.cstEnable = 1 AND cst.hrEmployee IS NOT NULL AND cst.cstAdCompany = ?1 ORDER BY cst.cstCustomerCode"
 *
 *
 *
 *
 *
 *
 * @ejb:finder signature="Collection findCstHrInsMiscByHrDbReferenceID(java.lang.Integer DB_REF_ID, java.lang.Integer CST_AD_CMPNY)"
 *			   query="SELECT OBJECT(cst) FROM ArCustomer cst WHERE cst.cstHrEnableInsMisc = 1 AND cst.cstHrInsMiscAmount > 0 AND cst.cstEnable = 1 AND cst.hrDeployedBranch.dbReferenceID =?1 AND cst.cstAdCompany = ?2"
 *
 * @jboss:query signature="Collection findCstHrInsMiscByHrDbReferenceID(java.lang.Integer DB_REF_ID, java.lang.Integer CST_AD_CMPNY)"
 *             query="SELECT OBJECT(cst) FROM ArCustomer cst WHERE cst.cstHrEnableInsMisc = 1 AND cst.cstHrInsMiscAmount > 0 AND cst.cstEnable = 1 AND cst.hrDeployedBranch.dbReferenceID =?1 AND cst.cstAdCompany = ?2 ORDER BY cst.cstCustomerCode"
 *
 *
 * @ejb:finder signature="Collection findCstHrInsMiscByCstHrAll(java.lang.Integer CST_AD_CMPNY)"
 *			   query="SELECT OBJECT(cst) FROM ArCustomer cst WHERE cst.cstHrEnableInsMisc = 1 AND cst.cstHrInsMiscAmount > 0 AND cst.cstEnable = 1 AND cst.hrEmployee IS NOT NULL AND cst.cstAdCompany = ?1"
 *
 * @jboss:query signature="Collection findCstHrInsMiscByCstHrAll(java.lang.Integer CST_AD_CMPNY)"
 *             query="SELECT OBJECT(cst) FROM ArCustomer cst WHERE cst.cstHrEnableInsMisc = 1 AND cst.cstHrInsMiscAmount > 0 AND cst.cstEnable = 1 AND cst.hrEmployee IS NOT NULL AND cst.cstAdCompany = ?1 ORDER BY cst.cstCustomerCode"
 *
 *
 * @ejb:finder signature="LocalArCustomer findByCstHrEmpReferenceID(java.lang.Integer CST_HR_EMP_ID, java.lang.Integer CST_AD_CMPNY)"
 * 			   query="SELECT OBJECT(cst) FROM ArCustomer cst WHERE cst.hrEmployee.empReferenceID = ?1 AND cst.cstAdCompany = ?2"
 *
 * @ejb:finder signature="LocalArCustomer findByCstPmUsrReferenceID(java.lang.Integer CST_PM_USR_ID, java.lang.Integer CST_AD_CMPNY)"
 * 			   query="SELECT OBJECT(cst) FROM ArCustomer cst WHERE cst.pmUser.usrReferenceID = ?1 AND cst.cstAdCompany = ?2"
 *
 *
 *
 *
 *
 *
 *
 * @ejb:finder signature="LocalArCustomer findByCstCustomerCodeAndHrEmpReferenceID(java.lang.String CST_CSTMR_CODE, java.lang.Integer CST_HR_EMP_ID, java.lang.Integer CST_AD_CMPNY)"
 * 			   query="SELECT OBJECT(cst) FROM ArCustomer cst WHERE cst.cstCustomerCode = ?1 AND cst.hrEmployee.empReferenceID = ?2 AND cst.cstAdCompany = ?3"
 *
 * @ejb:finder signature="Collection findEnabledCstAllByCcName(java.lang.String CC_NM, java.lang.Integer CST_AD_CMPNY)"
 *			   query="SELECT OBJECT(cst) FROM ArCustomer cst WHERE cst.arCustomerClass.ccName = ?1 AND cst.cstEnable = 1 AND cst.cstAdCompany = ?2"
 *
 * @jboss:query signature="Collection findEnabledCstAllByCcName(java.lang.String CC_NM, java.lang.Integer CST_AD_CMPNY)"
 *             query="SELECT OBJECT(cst) FROM ArCustomer cst WHERE cst.arCustomerClass.ccName = ?1 AND cst.cstEnable = 1 AND cst.cstAdCompany = ?2 ORDER BY cst.cstCustomerCode"
 *
 *
 * @ejb:finder signature="Collection findEnabledCstAllbyCustomerBatch(java.lang.String CST_CSTMR_BTCH, java.lang.Integer CST_AD_BRNCH, java.lang.Integer CST_AD_CMPNY)"
 *			   query="SELECT OBJECT(cst) FROM ArCustomer cst, IN(cst.adBranchCustomers) bcst WHERE cst.cstEnable = 1 AND cst.cstCustomerBatch = ?1  AND bcst.bcstAdBranch=?2 AND cst.cstAdCompany = ?3"
 *
 * @jboss:query signature="Collection findEnabledCstAllbyCustomerBatch(java.lang.String CST_CSTMR_BTCH, java.lang.Integer CST_AD_BRNCH, java.lang.Integer CST_AD_CMPNY)"
 *             query="SELECT OBJECT(cst) FROM ArCustomer cst, IN(cst.adBranchCustomers) bcst WHERE cst.cstEnable = 1 AND cst.cstCustomerBatch = ?1 AND bcst.bcstAdBranch=?2 AND cst.cstAdCompany = ?3 ORDER BY cst.cstCustomerCode"
 *
 * @ejb:finder signature="Collection findCstArea(java.lang.Integer CST_AD_CMPNY)"
 *             query="SELECT OBJECT(cst) FROM ArCustomer cst WHERE cst.cstAdCompany = ?1"
 *             
 * @ejb:finder signature="Collection findEnabledCstAllOrderByCstName(java.lang.Integer CST_AD_CMPNY)"
 * 			   query="SELECT OBJECT(cst) FROM ArCustomer cst WHERE cst.cstEnable = 1 AND cst.cstAdCompany = ?1"
 * 
 * @jboss:query signature="Collection findEnabledCstAllOrderByCstName(java.lang.Integer CST_AD_CMPNY)"
 *             query="SELECT OBJECT(cst) FROM ArCustomer cst WHERE cst.cstEnable = 1 AND cst.cstAdCompany = ?1 ORDER BY cst.cstName"
 *
 * @ejb:finder signature="LocalArCustomer findByCstCustomerCode(java.lang.String CST_CSTMR_CODE, java.lang.Integer CST_AD_CMPNY)"
 *             query="SELECT OBJECT(cst) FROM ArCustomer cst WHERE cst.cstCustomerCode = ?1 AND cst.cstAdCompany = ?2"
 * 
 * @ejb:finder signature="LocalArCustomer findByCstEmployeeID(java.lang.String CST_EMP_ID, java.lang.Integer CST_AD_CMPNY)"
 * 			   query="SELECT OBJECT(cst) FROM ArCustomer cst WHERE cst.cstEmployeeID = ?1 AND cst.cstAdCompany = ?2"
 *
 * @ejb:finder signature="Collection findManyByCstEmployeeID(java.lang.String CST_EMP_ID, java.lang.Integer CST_AD_CMPNY)"
 * 			   query="SELECT OBJECT(cst) FROM ArCustomer cst WHERE cst.cstEmployeeID = ?1 AND cst.cstAdCompany = ?2"
 *  
 * @ejb:finder signature="Collection findByCstRejectedByByBrCodeAndCstLastModifiedBy(java.lang.Integer AD_BRNCH, java.lang.String VOU_CRTD_BY, java.lang.Integer VOU_AD_CMPNY)"
 *             query="SELECT OBJECT(cst) FROM ArCustomer cst WHERE cst.cstReasonForRejection IS NOT NULL AND cst.cstAdBranch=?1 AND cst.cstLastModifiedBy = ?2 AND cst.cstAdCompany = ?3"
 *
 * @ejb:finder signature="LocalArCustomer findByCstNameAndAddress(java.lang.String CST_NM, java.lang.String CST_ADDRSS, java.lang.Integer CST_AD_CMPNY)"
 * 			   query="SELECT OBJECT(cst) FROM ArCustomer cst WHERE cst.cstName = ?1 AND cst.cstAddress = ?2 AND cst.cstAdCompany = ?3"
 * 
 * @ejb:finder signature="Collection findByCstGlCoaReceivableAccount(java.lang.Integer COA_CODE, java.lang.Integer CST_AD_CMPNY)"
 *             query="SELECT OBJECT(cst) FROM ArCustomer cst WHERE cst.cstGlCoaReceivableAccount = ?1 AND cst.cstAdCompany = ?2"
 * 
 * @ejb:finder signature="Collection findByCstGlCoaRevenueAccount(java.lang.Integer COA_CODE, java.lang.Integer CST_AD_CMPNY)"
 *             query="SELECT OBJECT(cst) FROM ArCustomer cst WHERE cst.cstGlCoaRevenueAccount = ?1 AND cst.cstAdCompany = ?2"
 *
 * @ejb:finder signature="LocalArCustomer findByCstName(java.lang.String CST_NM, java.lang.Integer CST_AD_BRNCH, java.lang.Integer CST_AD_CMPNY)"
 * 			   query="SELECT OBJECT(cst) FROM ArCustomer cst , IN(cst.adBranchCustomers) bcst WHERE cst.cstName = ?1 AND bcst.bcstAdBranch=?2 AND cst.cstAdCompany = ?3"
 * 
 * @ejb:finder signature="LocalArCustomer findByCstEmployeeID(java.lang.String CST_EMP_ID, java.lang.Integer CST_AD_BRNCH, java.lang.Integer CST_AD_CMPNY)"
 * 			   query="SELECT OBJECT(cst) FROM ArCustomer cst , IN(cst.adBranchCustomers) bcst WHERE cst.cstEmployeeID = ?1 AND bcst.bcstAdBranch=?2 AND cst.cstAdCompany = ?3"
 * 
 * @ejb:finder signature="LocalArCustomer findByCstName(java.lang.String CST_NM, java.lang.Integer CST_AD_CMPNY)"
 * 			   query="SELECT OBJECT(cst) FROM ArCustomer cst WHERE cst.cstName = ?1 AND cst.cstAdCompany = ?2"
 *  
 * @ejb:finder signature="LocalArCustomer findByCstBySupplierCode(java.lang.Integer SPL_CODE, java.lang.Integer CST_AD_CMPNY)"
 * 			   query="SELECT OBJECT(cst) FROM ArCustomer cst WHERE cst.apSupplier.splCode = ?1 AND cst.cstAdCompany = ?2"
 * 
 *  
 *  
 * @ejb:select signature="Collection ejbSelectGeneric(java.lang.String jbossQl, java.lang.Object[] args)"
 *             result-type-mapping="Local"
 *             method-intf="LocalHome"
 *             query="SELECT OBJECT(cst) FROM ArCustomer cst"
 * 
 * @ejb:finder signature="Collection findByCstDealPrice(java.lang.String CST_DL_PRC, java.lang.Integer CST_AD_CMPNY)"
 * 			   query="SELECT OBJECT(cst) FROM ArCustomer cst WHERE cst.cstDealPrice = ?1 AND cst.cstAdCompany = ?2"
 * 
 * @ejb:finder signature="Collection findByCstRegion(java.lang.String CST_RGN, java.lang.Integer CST_AD_CMPNY)"
 * 			   query="SELECT OBJECT(cst) FROM ArCustomer cst WHERE cst.cstAdLvRegion = ?1 AND cst.cstAdCompany = ?2"
 * 
 * @ejb:finder signature="Collection findCstByCstNewAndUpdated(java.lang.Integer BR_CODE, java.lang.Integer AD_CMPNY, char NEW, char UPDATED, char DOWNLOADED_UPDATED)"
 * 			   query="SELECT DISTINCT OBJECT(cst) FROM ArCustomer cst, IN(cst.adBranchCustomers) bcst WHERE (bcst.bcstCustomerDownloadStatus = ?3 OR bcst.bcstCustomerDownloadStatus = ?4 OR bcst.bcstCustomerDownloadStatus = ?5) AND cst.cstEnableRetailCashier = 1 AND bcst.adBranch.brCode = ?1 AND bcst.bcstAdCompany = ?2"
 *
 * @jboss:query signature="Collection ejbSelectGeneric(java.lang.String jbossQl, java.lang.Object[] args)"
 *                         dynamic="true"
 *
 * @ejb:value-object match="*"
 *             name="ArCustomer"
 *
 * @jboss:persistence table-name="AR_CSTMR"
 *
 * @jboss:entity-command name="mysql-get-generated-keys"
 */

public abstract class ArCustomerBean extends AbstractEntityBean {
	
	
	// PERSISTENT METHODS
	
	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 * @ejb:pk-field
	 *
	 * @jboss:column-name name="CST_CODE"
	 *
	 * @jboss:persistence auto-increment="true"
	 **/
	public abstract Integer getCstCode();
	public abstract void setCstCode(Integer CST_CODE);
	
	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"   
	 *
	 * @jboss:column-name name="CST_CSTMR_CODE"
	 **/
	public abstract String getCstCustomerCode();
	/**
	 * @ejb:interface-method view-type="local"
	 **/ 
	public abstract void setCstCustomerCode(String CST_CSTMR_CODE);
	
	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"   
	 *
	 * @jboss:column-name name="CST_NM"
	 **/
	public abstract String getCstName();
	/**
	 * @ejb:interface-method view-type="local"
	 **/ 
	public abstract void setCstName(String CST_NM);    
	
	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"   
	 *
	 * @jboss:column-name name="CST_DESC"
	 **/
	public abstract String getCstDescription();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setCstDescription(String CST_DESC);
	
	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="CST_PYMNT_MTHD"
	 **/
	public abstract String getCstPaymentMethod();
	/**
	 * @ejb:interface-method view-type="local"
	 **/ 
	public abstract void setCstPaymentMethod(String CST_PYMNT_MTHD);
	
	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="CST_CRDT_LMT"
	 **/
	public abstract double getCstCreditLimit();
	/**
	 * @ejb:interface-method view-type="local"
	 **/ 
	public abstract void setCstCreditLimit(double CST_CRDT_LMT);
	
	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="CST_ADDRSS"
	 **/
	public abstract String getCstAddress();
	/**
	 * @ejb:interface-method view-type="local"
	 **/ 
	public abstract void setCstAddress(String CST_ADDRSS);
	
	
	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="CST_CTY"
	 **/
	public abstract String getCstCity();
	/**
	 * @ejb:interface-method view-type="local"
	 **/ 
	public abstract void setCstCity(String CST_CTY);
	
	
	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="CST_STT_PRVNC"
	 **/
	public abstract String getCstStateProvince();
	/**
	 * @ejb:interface-method view-type="local"
	 **/ 
	public abstract void setCstStateProvince(String CST_STT_PRVNC); 
	
	
	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="CST_PSTL_CD"
	 **/
	public abstract String getCstPostalCode();
	/**
	 * @ejb:interface-method view-type="local"
	 **/ 
	public abstract void setCstPostalCode(String CST_PSTL_CD); 
	
	
	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="CST_CNTRY"
	 **/
	public abstract String getCstCountry();
	/**
	 * @ejb:interface-method view-type="local"
	 **/ 
	public abstract void setCstCountry(String CST_CNTRY); 
	
	
	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="CST_CNTCT"
	 **/
	public abstract String getCstContact();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setCstContact(String CST_CNTCT);
	
	
	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="CST_EMP_ID"
	 **/
	public abstract String getCstEmployeeID();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setCstEmployeeID(String CST_EMP_ID);
	
	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="CST_ACCNT_NO"
	 **/
	public abstract String getCstAccountNumber();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setCstAccountNumber(String CST_ACCNT_NO);
	
	
	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="CST_PHN"
	 **/
	public abstract String getCstPhone();
	/**
	 * @ejb:interface-method view-type="local"
	 **/ 
	public abstract void setCstPhone(String CST_PHN);
	
	
	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="CST_FX"
	 **/
	public abstract String getCstFax();
	/**
	 * @ejb:interface-method view-type="local"
	 **/ 
	public abstract void setCstFax(String CST_FX); 
	
	
	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="CST_ALTRNT_PHN"
	 **/
	public abstract String getCstAlternatePhone();
	/**
	 * @ejb:interface-method view-type="local"
	 **/ 
	public abstract void setCstAlternatePhone(String CST_ALTRNT_PHN); 
	
	
	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="CST_ALTRNT_CNTCT"
	 **/
	public abstract String getCstAlternateContact();
	/**
	 * @ejb:interface-method view-type="local"
	 **/ 
	public abstract void setCstAlternateContact(String CST_ALTRNT_CNTCT);
	
	
	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="CST_EML"
	 **/
	public abstract String getCstEmail();
	/**
	 * @ejb:interface-method view-type="local"
	 **/ 
	public abstract void setCstEmail(String CST_EML); 
	
	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="CST_BLL_TO_ADDRSS"
	 **/
	public abstract String getCstBillToAddress();
	/**
	 * @ejb:interface-method view-type="local"
	 **/ 
	public abstract void setCstBillToAddress(String CST_BLL_TO_ADDRSS); 
	
	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="CST_BLL_TO_CNTCT"
	 **/
	public abstract String getCstBillToContact();
	/**
	 * @ejb:interface-method view-type="local"
	 **/ 
	public abstract void setCstBillToContact(String CST_BLL_TO_CNTCT);
	
	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="CST_BLL_TO_ALT_CNTCT"
	 **/
	public abstract String getCstBillToAltContact();
	/**
	 * @ejb:interface-method view-type="local"
	 **/ 
	public abstract void setCstBillToAltContact(String CST_BLL_TO_ALT_CNTCT);
	
	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="CST_BLL_TO_PHN"
	 **/
	public abstract String getCstBillToPhone();
	/**
	 * @ejb:interface-method view-type="local"
	 **/ 
	public abstract void setCstBillToPhone(String CST_BLL_TO_PHN);
	
	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="CST_BLLNG_HDR"
	 **/
	public abstract String getCstBillingHeader();
	/**
	 * @ejb:interface-method view-type="local"
	 **/ 
	public abstract void setCstBillingHeader(String CST_BLLNG_HDR);
	
	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="CST_BLLNG_FTR"
	 **/
	public abstract String getCstBillingFooter();
	/**
	 * @ejb:interface-method view-type="local"
	 **/ 
	public abstract void setCstBillingFooter(String CST_BLLNG_FTR);
	
	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="CST_BLLNG_HDR2"
	 **/
	public abstract String getCstBillingHeader2();
	/**
	 * @ejb:interface-method view-type="local"
	 **/ 
	public abstract void setCstBillingHeader2(String CST_BLLNG_HDR2);
	
	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="CST_BLLNG_FTR2"
	 **/
	public abstract String getCstBillingFooter2();
	/**
	 * @ejb:interface-method view-type="local"
	 **/ 
	public abstract void setCstBillingFooter2(String CST_BLLNG_FTR2);
	
	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="CST_BLLNG_HDR3"
	 **/
	public abstract String getCstBillingHeader3();
	/**
	 * @ejb:interface-method view-type="local"
	 **/ 
	public abstract void setCstBillingHeader3(String CST_BLLNG_HDR3);
	
	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="CST_BLLNG_FTR3"
	 **/
	public abstract String getCstBillingFooter3();
	/**
	 * @ejb:interface-method view-type="local"
	 **/ 
	public abstract void setCstBillingFooter3(String CST_BLLNG_FTR3);
	
	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="CST_BLLNG_SGNTRY"
	 **/
	public abstract String getCstBillingSignatory();
	/**
	 * @ejb:interface-method view-type="local"
	 **/ 
	public abstract void setCstBillingSignatory(String CST_BLLNG_SGNTRY);
	
	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="CST_SGNTRY_TTL"
	 **/
	public abstract String getCstSignatoryTitle();
	/**
	 * @ejb:interface-method view-type="local"
	 **/ 
	public abstract void setCstSignatoryTitle(String CST_SGNTRY_TTL);
	
	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="CST_SHP_TO_ADDRSS"
	 **/
	public abstract String getCstShipToAddress();
	/**
	 * @ejb:interface-method view-type="local"
	 **/ 
	public abstract void setCstShipToAddress(String CST_SHP_TO_ADDRSS); 
	
	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="CST_SHP_TO_CNTCT"
	 **/
	public abstract String getCstShipToContact();
	/**
	 * @ejb:interface-method view-type="local"
	 **/ 
	public abstract void setCstShipToContact(String CST_SHP_TO_CNTCT);
	
	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="CST_SHP_TO_ALT_CNTCT"
	 **/
	public abstract String getCstShipToAltContact();
	/**
	 * @ejb:interface-method view-type="local"
	 **/ 
	public abstract void setCstShipToAltContact(String CST_SHP_TO_ALT_CNTCT);
	
	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="CST_SHP_TO_PHN"
	 **/
	public abstract String getCstShipToPhone();
	/**
	 * @ejb:interface-method view-type="local"
	 **/ 
	public abstract void setCstShipToPhone(String CST_SHP_TO_PHN);
	
	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="CST_TIN"
	 **/
	public abstract String getCstTin();
	/**
	 * @ejb:interface-method view-type="local"
	 **/ 
	public abstract void setCstTin(String CST_TIN); 

	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="CST_GL_COA_RCVBL_ACCNT"
	 **/
	public abstract Integer getCstGlCoaReceivableAccount();
	/**
	 * @ejb:interface-method view-type="local"
	 **/ 
	public abstract void setCstGlCoaReceivableAccount(Integer CST_GL_COA_RCVBL_ACCNT);
	
	
	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="CST_GL_COA_RVNUE_ACCNT"
	 **/
	public abstract Integer getCstGlCoaRevenueAccount();
	/**
	 * @ejb:interface-method view-type="local"
	 **/ 
	public abstract void setCstGlCoaRevenueAccount(Integer CST_GL_COA_RVNUE_ACCNT);
	
	
	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="CST_GL_COA_UNERND_INT_ACCNT"
	 **/
	public abstract Integer getCstGlCoaUnEarnedInterestAccount();
	/**
	 * @ejb:interface-method view-type="local"
	 **/ 
	public abstract void setCstGlCoaUnEarnedInterestAccount(Integer CST_GL_COA_UNERND_INT_ACCNT);
	
	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="CST_GL_COA_ERND_INT_ACCNT"
	 **/
	public abstract Integer getCstGlCoaEarnedInterestAccount();
	/**
	 * @ejb:interface-method view-type="local"
	 **/ 
	public abstract void setCstGlCoaEarnedInterestAccount(Integer CST_GL_COA_ERND_INT_ACCNT);
	
	
	
	
	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="CST_GL_COA_UNERND_PNT_ACCNT"
	 **/
	public abstract Integer getCstGlCoaUnEarnedPenaltyAccount();
	/**
	 * @ejb:interface-method view-type="local"
	 **/ 
	public abstract void setCstGlCoaUnEarnedPenaltyAccount(Integer CST_GL_COA_UNERND_PNT_ACCNT);
	
	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="CST_GL_COA_ERND_PNT_ACCNT"
	 **/
	public abstract Integer getCstGlCoaEarnedPenaltyAccount();
	/**
	 * @ejb:interface-method view-type="local"
	 **/ 
	public abstract void setCstGlCoaEarnedPenaltyAccount(Integer CST_GL_COA_ERND_PNT_ACCNT);
	
	
	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="CST_ENBL"
	 **/
	public abstract byte getCstEnable();
	/**
	 * @ejb:interface-method view-type="local"
	 **/ 
	public abstract void setCstEnable(byte CST_ENBL);
        
        /**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="CST_ENBL_PYRLL"
	 **/
	public abstract byte getCstEnablePayroll();
	/**
	 * @ejb:interface-method view-type="local"
	 **/ 
	public abstract void setCstEnablePayroll(byte CST_ENBL_PYRLL);
	
	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="CST_ENBL_RTL_CSHR"
	 **/
	public abstract byte getCstEnableRetailCashier();
	/**
	 * @ejb:interface-method view-type="local"
	 **/ 
	public abstract void setCstEnableRetailCashier(byte CST_ENBL_RTL_CSHR);
	
	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="CST_ENBL_RBT"
	 **/
	public abstract byte getCstEnableRebate();
	/**
	 * @ejb:interface-method view-type="local"
	 **/ 
	public abstract void setCstEnableRebate(byte CST_ENBL_RBT);
	
	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="CST_AUTO_CMPUTE_INT"
	 **/
	public abstract byte getCstAutoComputeInterest();
	/**
	 * @ejb:interface-method view-type="local"
	 **/ 
	public abstract void setCstAutoComputeInterest(byte CST_AUTO_CMPUTE_INT);
	
	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="CST_AUTO_CMPUTE_PNT"
	 **/
	public abstract byte getCstAutoComputePenalty();
	/**
	 * @ejb:interface-method view-type="local"
	 **/ 
	public abstract void setCstAutoComputePenalty(byte CST_AUTO_CMPUTE_PNT);
	
	/**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="CST_MBL_PHN"
     **/
    public abstract String getCstMobilePhone();
     /**
      * @ejb:interface-method view-type="local"
      **/ 
    public abstract void setCstMobilePhone(String CST_MBL_PHN);
    
	/**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="CST_ALTRNT_MBL_PHN"
     **/
    public abstract String getCstAlternateMobilePhone();
     /**
      * @ejb:interface-method view-type="local"
      **/ 
    public abstract void setCstAlternateMobilePhone(String CST_ALTRNT_MBL_PHN);
    
	/**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="CST_BRTHDY"
     **/
    public abstract Date getCstBirthday();
     /**
      * @ejb:interface-method view-type="local"
      **/ 
    public abstract void setCstBirthday(Date CST_BRTHDY);
    
	/**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="CST_DL_PRC"
     **/
    public abstract String getCstDealPrice();
     /**
      * @ejb:interface-method view-type="local"
      **/ 
    public abstract void setCstDealPrice(String CST_DL_PRC);
    
    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="CST_AREA"
     **/
    public abstract String getCstArea();
     /**
      * @ejb:interface-method view-type="local"
      **/ 
    public abstract void setCstArea(String CST_AREA);
    
    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="CST_SQ_MTR"
     **/
    public abstract double getCstSquareMeter();
     /**
      * @ejb:interface-method view-type="local"
      **/ 
    public abstract void setCstSquareMeter(double CST_SQ_MTR);
    
    /**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="CST_NO_PRKNG"
	 **/
	public abstract double getCstNumbersParking();
	/**
	 * @ejb:interface-method view-type="local"
	 **/ 
	public abstract void setCstNumbersParking(double CST_NO_PRKNG);
	
	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="CST_MNTHLY_INT_RT"
	 **/
	public abstract double getCstMonthlyInterestRate();
	/**
	 * @ejb:interface-method view-type="local"
	 **/ 
	public abstract void setCstMonthlyInterestRate(double CST_MNTHLY_INT_RT);
	
	 /**
	* @ejb:persistent-field
	* @ejb:interface-method view-type="local"
	*
	* @jboss:column-name name="CST_PRKNG_ID"
	**/
	public abstract String getCstParkingID();
	/**
	* @ejb:interface-method view-type="local"
	**/ 
	public abstract void setCstParkingID(String CST_PRKNG_ID);
	
	/**
	* @ejb:persistent-field
	* @ejb:interface-method view-type="local"
	*
	* @jboss:column-name name="CST_WP_CSTMR_ID"
	**/
	public abstract String getCstWordPressCustomerID();
	/**
	* @ejb:interface-method view-type="local"
	**/ 
	public abstract void setCstWordPressCustomerID(String CST_WP_CSTMR_ID);
	
	
	/**
	* @ejb:persistent-field
	* @ejb:interface-method view-type="local"
	*
	* @jboss:column-name name="CST_ASD_RT"
	**/
	public abstract double getCstAssociationDuesRate();
	/**
	* @ejb:interface-method view-type="local"
	**/ 
	public abstract void setCstAssociationDuesRate(double CST_ASD_RT);
	
	 /**
	* @ejb:persistent-field
	* @ejb:interface-method view-type="local"
	*
	* @jboss:column-name name="CST_RPT_RT"
	**/
	public abstract double getCstRealPropertyTaxRate();
	/**
	* @ejb:interface-method view-type="local"
	**/ 
	public abstract void setCstRealPropertyTaxRate(double CST_RPT_RT);
	
	
    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="CST_ENTRY_DT"
     **/
    public abstract Date getCstEntryDate();
     /**
      * @ejb:interface-method view-type="local"
      **/ 
    public abstract void setCstEntryDate(Date CST_ENTRY_DT);
    
    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="CST_EFFCTVTY_DYS"
     **/
    public abstract short getCstEffectivityDays();
     /**
      * @ejb:interface-method view-type="local"
      **/ 
    public abstract void setCstEffectivityDays(short CST_EFFCTVTY_DYS);
    
	/**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="CST_AD_LV_RGN"
     **/
    public abstract String getCstAdLvRegion();
     /**
      * @ejb:interface-method view-type="local"
      **/ 
    public abstract void setCstAdLvRegion(String CST_AD_LV_RGN);
 
    /**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="CST_MMO"
	 **/
	public abstract String getCstMemo();
	/**
	 * @ejb:interface-method view-type="local"
	 **/ 
	public abstract void setCstMemo(String CST_MMO);
	
	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="CST_CSTMR_BTCH"
	 **/
	public abstract String getCstCustomerBatch();
	/**
	 * @ejb:interface-method view-type="local"
	 **/ 
	public abstract void setCstCustomerBatch(String CST_CSTMR_BTCH);
	
	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="CST_CSTMR_DPRTMNT"
	 **/
	public abstract String getCstCustomerDepartment();
	/**
	 * @ejb:interface-method view-type="local"
	 **/ 
	public abstract void setCstCustomerDepartment(String CST_CSTMR_DPRTMNT);
	
	
	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="AR_SALESPERSON2"
	 **/
	public abstract Integer getCstArSalesperson2();
	/**
	 * @ejb:interface-method view-type="local"
	 **/ 
	public abstract void setCstArSalesperson2(Integer AR_SALESPERSON2);
	

	
	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="CST_HR_ENBL_CB"
	 **/
	public abstract byte getCstHrEnableCashBond();
	/**
	 * @ejb:interface-method view-type="local"
	 **/ 
	public abstract void setCstHrEnableCashBond(byte CST_HR_ENBL_CB);
	
	
	/**
	* @ejb:persistent-field
	* @ejb:interface-method view-type="local"
	*
	* @jboss:column-name name="CST_HR_CB_AMNT"
	**/
	public abstract double getCstHrCashBondAmount();
	/**
	* @ejb:interface-method view-type="local"
	**/ 
	public abstract void setCstHrCashBondAmount(double CST_HR_CB_AMNT);
	
	
	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="CST_HR_ENBL_IS"
	 **/
	public abstract byte getCstHrEnableInsMisc();
	/**
	 * @ejb:interface-method view-type="local"
	 **/ 
	public abstract void setCstHrEnableInsMisc(byte CST_HR_ENBL_IS);
	
	/**
	* @ejb:persistent-field
	* @ejb:interface-method view-type="local"
	*
	* @jboss:column-name name="CST_HR_IS_AMNT"
	**/
	public abstract double getCstHrInsMiscAmount();
	/**
	* @ejb:interface-method view-type="local"
	**/ 
	public abstract void setCstHrInsMiscAmount(double CST_HR_IS_AMNT);
	

	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="CST_APPRVL_STATUS"
	 **/
	public abstract String getCstApprovalStatus();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setCstApprovalStatus(String CST_APPRVL_STATUS);
	
	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="CST_RSN_FR_RJCTN"
	 **/
	public abstract String getCstReasonForRejection();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setCstReasonForRejection(String CST_RSN_FR_RJCTN);     
	
	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="CST_PSTD"
	 **/
	public abstract byte getCstPosted();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setCstPosted(byte CST_PSTD);



	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="CST_CRTD_BY"
	 **/
	public abstract String getCstCreatedBy();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setCstCreatedBy(String CST_CRTD_BY);
	
	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="CST_DT_CRTD"
	 **/
	public abstract Date getCstDateCreated();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setCstDateCreated(Date CST_DT_CRTD);
	
	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="CST_LST_MDFD_BY"
	 **/
	public abstract String getCstLastModifiedBy();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setCstLastModifiedBy(String CST_LST_MDFD_BY);
	
	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="CST_DT_LST_MDFD"
	 **/
	public abstract Date getCstDateLastModified();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setCstDateLastModified(Date CST_DT_LST_MDFD);
	
	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="CST_APPRVD_RJCTD_BY"
	 **/
	public abstract String getCstApprovedRejectedBy();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setCstApprovedRejectedBy(String CST_APPRVD_RJCTD_BY);
	
	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="CST_DT_APPRVD_RJCTD"
	 **/
	public abstract Date getCstDateApprovedRejected();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setCstDateApprovedRejected(Date CST_DT_APPRVD_RJCTD);



	/** @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="CST_PSTD_BY"
	 **/
	public abstract String getCstPostedBy();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setCstPostedBy(String CST_PSTD_BY);
	
	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="CST_DT_PSTD"
	 **/
	public abstract Date getCstDatePosted();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setCstDatePosted(Date CST_DT_PSTD);
	
	
	
	
	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="CST_AD_BRNCH"
	 **/
	public abstract Integer getCstAdBranch();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setCstAdBranch(Integer CST_AD_BRNCH);
	
	
	

	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="CST_AD_CMPNY"
	 **/
	public abstract Integer getCstAdCompany();
	/**
	 * @ejb:interface-method view-type="local"
	 **/ 
	public abstract void setCstAdCompany(Integer CST_AD_CMPNY);
	

	
	
	// RELATIONSHIP METHODS
	
	
	/**
	 * @ejb:interface-method view-type="local"
	 * @ejb:relation name="supplier-customers"
	 *               role-name="customer-has-one-supplier"
	 *
	 * @jboss:relation related-pk-field="splCode"
	 *                 fk-column="AP_SUPPLIER"
	 */
	public abstract LocalApSupplier getApSupplier();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setApSupplier(LocalApSupplier apSupplier);
	
	/**
	 * @ejb:interface-method view-type="local"
	 * @ejb:relation name="deployedbranch-customers"
	 *               role-name="customer-has-one-deployedbranch"
	 *
	 * @jboss:relation related-pk-field="dbCode"
	 *                 fk-column="HR_DEPLOYED_BRANCH"
	 */
	public abstract LocalHrDeployedBranch getHrDeployedBranch();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setHrDeployedBranch(LocalHrDeployedBranch hrDeployedBranch);
	
	/**
	 * @ejb:interface-method view-type="local"
	 * @ejb:relation name="employee-customers"
	 *               role-name="customer-has-one-employee"
	 *
	 * @jboss:relation related-pk-field="empCode"
	 *                 fk-column="HR_EMPLOYEE"
	 */
	public abstract LocalHrEmployee getHrEmployee();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setHrEmployee(LocalHrEmployee hrEmployee);
	
	
	
	/**
	 * @ejb:interface-method view-type="local"
	 * @ejb:relation name="pmuser-customers"
	 *               role-name="customer-has-one-pmuser"
	 *
	 * @jboss:relation related-pk-field="usrCode"
	 *                 fk-column="PM_USER"
	 */
	public abstract LocalPmUser getPmUser();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setPmUser(LocalPmUser pmUser);
	
	
	
	
	
	/**
	 * @ejb:interface-method view-type="local"
	 * @ejb:relation name="paymentterm-customers"
	 *               role-name="customer-has-one-paymentterm"
	 * 
	 * @jboss:relation related-pk-field="pytCode"
	 *                 fk-column="AD_PAYMENT_TERM"
	 */
	public abstract com.ejb.ad.LocalAdPaymentTerm getAdPaymentTerm();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setAdPaymentTerm(com.ejb.ad.LocalAdPaymentTerm adPaymentTerm);
	
	/**
	 * @ejb:interface-method view-type="local"
	 * @ejb:relation name="customertype-customers"
	 *               role-name="customer-has-one-customertype"
	 * 
	 * @jboss:relation related-pk-field="ctCode"
	 *                 fk-column="AR_CUSTOMER_TYPE"
	 */
	public abstract LocalArCustomerType getArCustomerType();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setArCustomerType(LocalArCustomerType arCustomerType);
	
	/**
	 * @ejb:interface-method view-type="local"
	 * @ejb:relation name="customerclass-customers"
	 *               role-name="customer-has-one-customerclass"
	 * 
	 * @jboss:relation related-pk-field="ccCode"
	 *                 fk-column="AR_CUSTOMER_CLASS"
	 */
	public abstract LocalArCustomerClass getArCustomerClass();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setArCustomerClass(LocalArCustomerClass arCustomerClass);     
	
	
	/**
	 * @ejb:interface-method view-type="local"
	 * @ejb:relation name="bankaccount-customers"
	 *               role-name="customer-has-one-bankaccount"
	 * 
	 * @jboss:relation related-pk-field="baCode"
	 *                 fk-column="AD_BANK_ACCOUNT"
	 */
	public abstract com.ejb.ad.LocalAdBankAccount getAdBankAccount();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setAdBankAccount(com.ejb.ad.LocalAdBankAccount adBankAccount);
	
	/**
	 * @ejb:interface-method view-type="local"
	 * @ejb:relation name="customer-invoices"
	 *               role-name="customer-has-many-invoices"
	 * 
	 */
	public abstract Collection getArInvoices();
	public abstract void setArInvoices(Collection arInvoices);  
	
	/**
	 * @ejb:interface-method view-type="local"
	 * @ejb:relation name="customer-buildunbuildassemblies"
	 *               role-name="customer-has-many-buildunbuildassemblies"
	 * 
	 */
	public abstract Collection getInvBuildUnBuildAssemblies();
	public abstract void setInvBuildUnBuildAssemblies(Collection buildunbuildassemblies);
	
	/**
	 * @ejb:interface-method view-type="local"
	 * @ejb:relation name="customer-receipts"
	 *               role-name="customer-has-many-receipts"
	 * 
	 */
	public abstract Collection getArReceipts();
	public abstract void setArReceipts(Collection arReceipts);
	

	/**
	 * @ejb:interface-method view-type="local"
	 * @ejb:relation name="customer-customerbalances"
	 *               role-name="customer-has-many-customerbalances"
	 * 
	 */
	public abstract Collection getArCustomerBalances();
	public abstract void setArCustomerBalances(Collection arCustomerBalances);

	/**
	 * @ejb:interface-method view-type="local"
	 * @ejb:relation name="customer-pdcs"
	 *               role-name="customer-has-many-pdcs"
	 * 
	 */
	public abstract Collection getArPdcs();
	public abstract void setArPdcs(Collection arPdcs);    
	
	/**
	 * @ejb:interface-method view-type="local"
	 * @ejb:relation name="customer-salesorder"
	 *               role-name="customer-has-many-salesorder"
	 * 
	 */
	public abstract Collection getArSalesOrders();
	public abstract void setArSalesOrders(Collection arSalesOrders); 
	
	/**
	 * @ejb:interface-method view-type="local"
	 * @ejb:relation name="customer-joborders"
	 *               role-name="customer-has-many-joborders"
	 * 
	 */
	public abstract Collection getArJobOrders();
	public abstract void setArJobOrders(Collection arJobOrders); 

    /**
     * @ejb:interface-method view-type="local"
     * @ejb:relation name="customer-branchcustomers" 
     *				 role-name="customer-has-many-branchcustomer"
     *
     */
    public abstract Collection getAdBranchCustomers();
    public abstract void setAdBranchCustomers(Collection adBranchCustomers);

	/**
	 * @ejb:interface-method view-type="local"
	 * @ejb:relation name="salesperson-customers"
	 *               role-name="customer-has-one-salesperson"
	 * 
	 * @jboss:relation related-pk-field="slpCode"
	 *                 fk-column="AR_SALESPERSON"
	 */
	public abstract LocalArSalesperson getArSalesperson();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setArSalesperson(LocalArSalesperson arSalesperson);
	
	/**
     * @ejb:interface-method view-type="local"
     * @ejb:relation name="customer-items"
     *               role-name="customer-has-many-items"
     * 
     */
    public abstract Collection getInvItems();
    public abstract void setInvItems(Collection invItems);
    
    
    
    /**
     * @ejb:interface-method view-type="local"
     * @ejb:relation name="customer-adjustments"
     *               role-name="customer-has-many-adjustment"
     * 
     */
    public abstract Collection getCmAdjustments();
    public abstract void setCmAdjustments(Collection cmAdjustments);
    
    
    /**
     * @ejb:interface-method view-type="local"
     * @ejb:relation name="customer-standardmemolineclasses"
     *               role-name="customer-has-many-standardmemolineclasses"
     * 
     */
    public abstract Collection getArStandardMemoLineClasses();
    public abstract void setArStandardMemoLineClasses(Collection arStandardMemoLineClasses);
    
    
	
	/**
     * @ejb:interface-method view-type="local"
     * @ejb:relation name="lineItemTemplate-customers"
     *               role-name="customer-has-one-lineItemTemplate"
     * 
     * @jboss:relation related-pk-field="litCode"
     *                 fk-column="INV_LINE_ITEM_TEMPLATE"
     * 
     */
    public abstract LocalInvLineItemTemplate getInvLineItemTemplate();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
    public abstract void setInvLineItemTemplate(LocalInvLineItemTemplate invLineItemTemplate);     
    
	/**
	 * @jboss:dynamic-ql
	 */
	public abstract Collection ejbSelectGeneric(java.lang.String jbossQl, java.lang.Object[] args)
	throws FinderException;
	
	
	
	/**
     * @ejb:interface-method view-type="local"
     **/
    public void addInvItem(LocalInvItem invItem) {

         Debug.print("ArCustomerBean addInvItem");
      
         try {
      	
            Collection invItems = getInvItems();
            invItems.add(invItem);
	          
         } catch (Exception ex) {
      	
           throw new EJBException(ex.getMessage());
      
         }   
     }

    /**
     * @ejb:interface-method view-type="local"
     **/
    public void dropInvItem(LocalInvItem invItem) {

         Debug.print("ArCustomerBean dropInvItem");
      
         try {
      	
            Collection invItems = getInvItems();
            invItems.remove(invItem);
	      
         } catch (Exception ex) {
      	
           throw new EJBException(ex.getMessage());
         
         }   
     }
    
    
    /**
     * @ejb:interface-method view-type="local"
     **/
    public void addCmAdjustment(LocalCmAdjustment cmAdjustment) {

         Debug.print("CmAdjustment Add cmAdjustment");
      
         try {
      	
            Collection cmAdjustments = getCmAdjustments();
            cmAdjustments.add(cmAdjustment);
	          
         } catch (Exception ex) {
      	
           throw new EJBException(ex.getMessage());
      
         }   
     }

    /**
     * @ejb:interface-method view-type="local"
     **/
    public void dropCmAdjustment(LocalCmAdjustment cmAdjustment) {

         Debug.print("CmAdjustment Drop cmAdjustment");
      
         try {
      	
            Collection cmAdjustments = getCmAdjustments();
            cmAdjustments.remove(cmAdjustment);
	      
         } catch (Exception ex) {
      	
           throw new EJBException(ex.getMessage());
         
         }   
     }
    
    
    
    
    
    
	
	// BUSINESS METHODS
	
	/**
	 * @ejb:home-method view-type="local"
	 */
	public Collection ejbHomeGetCstByCriteria(java.lang.String jbossQl, java.lang.Object[] args)
	throws FinderException {
		
		return ejbSelectGeneric(jbossQl, args);
	}
	
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public void addArInvoice(LocalArInvoice arInvoice) {
		
		Debug.print("ArCustomerBean addArInvoice");
		
		try {
			
			Collection arInvoices = getArInvoices();
			arInvoices.add(arInvoice);
			
		} catch (Exception ex) {
			
			throw new EJBException(ex.getMessage());
			
		}   
	}
	
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public void dropArInvoice(LocalArInvoice arInvoice) {
		
		Debug.print("ArCustomerBean dropArInvoice");
		
		try {
			
			Collection arInvoices = getArInvoices();
			arInvoices.remove(arInvoice);
			
		} catch (Exception ex) {
			
			throw new EJBException(ex.getMessage());
			
		}   
	}
	
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public void addArReceipt(LocalArReceipt arReceipt) {
		
		Debug.print("ArCustomerBean addArReceipt");
		
		try {
			
			Collection arReceipts = getArReceipts();
			arReceipts.add(arReceipt);
			
		} catch (Exception ex) {
			
			throw new EJBException(ex.getMessage());
			
		}   
	}
	
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public void dropArReceipt(LocalArReceipt arReceipt) {
		
		Debug.print("ArCustomerBean dropArReceipt");
		
		try {
			
			Collection arReceipts = getArReceipts();
			arReceipts.remove(arReceipt);
			
		} catch (Exception ex) {
			
			throw new EJBException(ex.getMessage());
			
		} 
		
	}
	
	
	
	
	
	
	
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public void addArCustomerBalance(LocalArCustomerBalance arCustomerBalance) {
		
		Debug.print("ArCustomerBean addArCustomerBalance");
		
		try {
			
			Collection arCustomerBalances = getArCustomerBalances();
			arCustomerBalances.add(arCustomerBalance);
			
		} catch (Exception ex) {
			
			throw new EJBException(ex.getMessage());
			
		}   
	}
	
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public void dropArCustomerBalance(LocalArCustomerBalance arCustomerBalance) {
		
		Debug.print("ArCustomerBean dropArCustomerBalance");
		
		try {
			
			Collection arCustomerBalances = getArCustomerBalances();
			arCustomerBalances.remove(arCustomerBalance);
			
		} catch (Exception ex) {
			
			throw new EJBException(ex.getMessage());
			
		}   
	}
	
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public void addArPdc(LocalArPdc arPdc) {
		
		Debug.print("ArCustomerBean addArPdc");
		
		try {
			
			Collection arPdcs = getArPdcs();
			arPdcs.add(arPdc);
			
		} catch (Exception ex) {
			
			throw new EJBException(ex.getMessage());
			
		}   
	}
	
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public void dropArPdc(LocalArPdc arPdc) {
		
		Debug.print("ArCustomerBean dropArPdc");
		
		try {
			
			Collection arPdcs = getArPdcs();
			arPdcs.remove(arPdc);
			
		} catch (Exception ex) {
			
			throw new EJBException(ex.getMessage());
			
		}   
	}    
	
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public void addArSalesOrder(LocalArSalesOrder arSalesOrder) {
		
		Debug.print("ArCustomerBean addArSalesOrder");
		
		try {
			
			Collection arSalesOrders = getArSalesOrders();
			arSalesOrders.add(arSalesOrder);
			
		} catch (Exception ex) {
			
			throw new EJBException(ex.getMessage());
			
		}   
	}
	
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public void dropArSalesOrder(LocalArSalesOrder arSalesOrder) {
		
		Debug.print("ArCustomerBean dropArSalesOrder");
		
		try {
			
			Collection arSalesOrders = getArSalesOrders();
			arSalesOrders.remove(arSalesOrder);
			
		} catch (Exception ex) {
			
			throw new EJBException(ex.getMessage());
			
		}   
	}            
	
    /**
     * @ejb:interface-method view-type="local"
     **/
    public void addAdBranchCustomer(LocalAdBranchCustomer adBranchCustomer) {

         Debug.print("ArCustomerBean addAdBranchCustomer");
      
         try {
      	
            Collection adBranchCustomers = getAdBranchCustomers();
	        adBranchCustomers.add(adBranchCustomer);
	          
         } catch (Exception ex) {
      	
           throw new EJBException(ex.getMessage());
      
         }   
     }

    /**
     * @ejb:interface-method view-type="local"
     **/
    public void dropAdBranchCustomer(LocalAdBranchCustomer adBranchCustomer) {

         Debug.print("ArCustomerBean dropAdBranchCustomer");
      
         try {
      	
            Collection adBranchCustomers = getAdBranchCustomers();
	        adBranchCustomers.remove(adBranchCustomer);
	      
         } catch (Exception ex) {
      	
           throw new EJBException(ex.getMessage());
         
         }   
     }    

	
	// ENTITY METHODS
	
	/**
	 * @ejb:create-method view-type="local"
	 **/
	public Integer ejbCreate(Integer CST_CODE, 
			String CST_CSTMR_CODE, String CST_NM, String CST_DESC, 
			String CST_PYMNT_MTHD, double CST_CRDT_LMT, String CST_ADDRSS, String CST_CTY, 
			String CST_STT_PRVNC, String CST_PSTL_CD, String CST_CNTRY, 
			String CST_CNTCT, String CST_EMP_ID, String CST_ACCNT_NO, String CST_PHN, String CST_MBL_PHN, String CST_FX, String CST_ALTRNT_PHN, 
			String CST_ALTRNT_MBL_PHN, String CST_ALTRNT_CNTCT, String CST_EML, String CST_BLL_TO_ADDRSS,
			String CST_BLL_TO_CNTCT, String CST_BLL_TO_ALT_CNTCT, String CST_BLL_TO_PHN, 
			String CST_BLLNG_HDR, String CST_BLLNG_FTR, String CST_BLLNG_HDR2, String CST_BLLNG_FTR2,
			String CST_BLLNG_HDR3, String CST_BLLNG_FTR3, String CST_BLLNG_SGNTRY, String CST_SGNTRY_TTL,
			String CST_SHP_TO_ADDRSS, String CST_SHP_TO_CNTCT, String CST_SHP_TO_ALT_CNTCT, 
			String CST_SHP_TO_PHN, String CST_TIN, double CST_NO_PRKNG, double CST_MNTHLY_INT_RT, String CST_PRKNG_ID, double CST_ASD_RT, double CST_RPT_RT, String CST_WP_CSTMR_ID, Integer CST_GL_COA_RCVBL_ACCNT, 
			Integer CST_GL_COA_RVNUE_ACCNT, 
			Integer CST_GL_COA_UNERND_INT_ACCNT, Integer CST_GL_COA_ERND_INT_ACCNT,
			Integer CST_GL_COA_UNERND_PNT_ACCNT, Integer CST_GL_COA_ERND_PNT_ACCNT,
			byte CST_ENBL, byte CST_ENBL_PYRLL, byte CST_ENBL_RTL_CSHR, byte CST_ENBL_RBT , byte CST_AUTO_CMPUTE_INT, byte CST_AUTO_CMPUTE_PNT, Date CST_BRTHDY, 
			String CST_DL_PRC, String CST_AREA, double CST_SQ_MTR, Date CST_ENTRY_DT, short CST_EFFCTVTY_DYS, String CST_AD_LV_RGN,
			String CST_MMO, String CST_CSTMR_BTCH, String CST_CSTMR_DPRTMNT,
			
			String CST_APPRVL_STATUS, 
			String CST_RSN_FR_RJCTN, 
			byte CST_PSTD,
			String CST_CRTD_BY, 
			Date CST_DT_CRTD, 	
			String CST_LST_MDFD_BY, 
			Date CST_DT_LST_MDFD, 
			String CST_APPRVD_RJCTD_BY, 
			Date CST_DT_APPRVD_RJCTD, 
			String CST_PSTD_BY, 
			Date CST_DT_PSTD,
			Integer CST_AD_BRNCH,
			Integer CST_AD_CMPNY)     	
	throws CreateException {	
		
		Debug.print("ArCustomerBean ejbCreate");
		
		setCstCode(CST_CODE);
		setCstCustomerCode(CST_CSTMR_CODE);
		setCstName(CST_NM);
		setCstDescription(CST_DESC);   
		setCstPaymentMethod(CST_PYMNT_MTHD);
		setCstCreditLimit(CST_CRDT_LMT);
		setCstAddress(CST_ADDRSS);
		setCstCity(CST_CTY);
		setCstStateProvince(CST_STT_PRVNC);
		setCstPostalCode(CST_PSTL_CD);
		setCstCountry(CST_CNTRY);
		setCstContact(CST_CNTCT);
		setCstEmployeeID(CST_EMP_ID);
		setCstAccountNumber(CST_ACCNT_NO);
		setCstPhone(CST_PHN);
		setCstMobilePhone(CST_MBL_PHN);
		setCstFax(CST_FX);
		setCstAlternatePhone(CST_ALTRNT_PHN);
		setCstAlternateMobilePhone(CST_ALTRNT_MBL_PHN);
		setCstAlternateContact(CST_ALTRNT_CNTCT);
		setCstEmail(CST_EML);
		setCstBillToAddress(CST_BLL_TO_ADDRSS);
		setCstBillToContact(CST_BLL_TO_CNTCT);
		setCstBillToAltContact(CST_BLL_TO_ALT_CNTCT);
		setCstBillToPhone(CST_BLL_TO_PHN);
		setCstBillingHeader(CST_BLLNG_HDR);
		setCstBillingFooter(CST_BLLNG_FTR);
		setCstBillingHeader2(CST_BLLNG_HDR2);
		setCstBillingFooter2(CST_BLLNG_FTR2);
		setCstBillingHeader3(CST_BLLNG_HDR3);
		setCstBillingFooter3(CST_BLLNG_FTR3);
		setCstBillingSignatory(CST_BLLNG_SGNTRY);
		setCstSignatoryTitle(CST_SGNTRY_TTL);
		setCstShipToAddress(CST_SHP_TO_ADDRSS);
		setCstShipToContact(CST_SHP_TO_CNTCT);
		setCstShipToAltContact(CST_SHP_TO_ALT_CNTCT);
		setCstShipToPhone(CST_SHP_TO_PHN);         
		setCstTin(CST_TIN);
		setCstNumbersParking(CST_NO_PRKNG);
		setCstMonthlyInterestRate(CST_MNTHLY_INT_RT);
		setCstParkingID(CST_PRKNG_ID);
		setCstAssociationDuesRate(CST_ASD_RT);
		setCstRealPropertyTaxRate(CST_RPT_RT);
		setCstWordPressCustomerID(CST_WP_CSTMR_ID);
		setCstGlCoaReceivableAccount(CST_GL_COA_RCVBL_ACCNT);
		setCstGlCoaRevenueAccount(CST_GL_COA_RVNUE_ACCNT);
		setCstGlCoaUnEarnedInterestAccount(CST_GL_COA_UNERND_INT_ACCNT);
		setCstGlCoaEarnedInterestAccount(CST_GL_COA_ERND_INT_ACCNT);
		setCstGlCoaUnEarnedPenaltyAccount(CST_GL_COA_UNERND_PNT_ACCNT);
		setCstGlCoaEarnedPenaltyAccount(CST_GL_COA_ERND_PNT_ACCNT);
		setCstEnable(CST_ENBL);
                setCstEnablePayroll(CST_ENBL_PYRLL);
		setCstEnableRetailCashier(CST_ENBL_RTL_CSHR);
		setCstEnableRebate(CST_ENBL_RBT);
		setCstAutoComputeInterest(CST_AUTO_CMPUTE_INT);
		setCstAutoComputePenalty(CST_AUTO_CMPUTE_PNT);
		setCstBirthday(CST_BRTHDY);
		setCstDealPrice(CST_DL_PRC);
		setCstArea(CST_AREA);
		setCstSquareMeter(CST_SQ_MTR);
		setCstEntryDate(CST_ENTRY_DT);
		setCstEffectivityDays(CST_EFFCTVTY_DYS);
		setCstAdLvRegion(CST_AD_LV_RGN);
		setCstMemo(CST_MMO);
		setCstCustomerBatch(CST_CSTMR_BTCH);
		setCstCustomerDepartment(CST_CSTMR_DPRTMNT);
		setCstApprovalStatus(CST_APPRVL_STATUS);
		setCstReasonForRejection(CST_RSN_FR_RJCTN);
		setCstPosted(CST_PSTD);
		setCstCreatedBy(CST_CRTD_BY);
		setCstDateCreated(CST_DT_CRTD);
		setCstLastModifiedBy(CST_LST_MDFD_BY);
		setCstDateLastModified(CST_DT_LST_MDFD);
		setCstApprovedRejectedBy(CST_APPRVD_RJCTD_BY);
		setCstDateApprovedRejected(CST_DT_APPRVD_RJCTD);
		setCstPostedBy(CST_PSTD_BY);
		setCstDatePosted(CST_DT_PSTD);
		
		setCstAdBranch(CST_AD_BRNCH);
		setCstAdCompany(CST_AD_CMPNY);
		
		
		return null;
	}
	
	/**
	 * @ejb:create-method view-type="local"
	 **/
	public Integer ejbCreate( 
			String CST_CSTMR_CODE, String CST_NM, String CST_DESC, 
			String CST_PYMNT_MTHD, double CST_CRDT_LMT, String CST_ADDRSS, String CST_CTY, 
			String CST_STT_PRVNC, String CST_PSTL_CD, String CST_CNTRY, 
			String CST_CNTCT, String CST_EMP_ID, String CST_ACCNT_NO, String CST_PHN, String CST_MBL_PHN, String CST_FX, String CST_ALTRNT_PHN, 
			String CST_ALTRNT_MBL_PHN, String CST_ALTRNT_CNTCT, String CST_EML, String CST_BLL_TO_ADDRSS,
			String CST_BLL_TO_CNTCT, String CST_BLL_TO_ALT_CNTCT, String CST_BLL_TO_PHN, 
			String CST_BLLNG_HDR, String CST_BLLNG_FTR, String CST_BLLNG_HDR2, String CST_BLLNG_FTR2,
			String CST_BLLNG_HDR3, String CST_BLLNG_FTR3, String CST_BLLNG_SGNTRY, String CST_SGNTRY_TTL,
			String CST_SHP_TO_ADDRSS, String CST_SHP_TO_CNTCT, String CST_SHP_TO_ALT_CNTCT, 
			String CST_SHP_TO_PHN, String CST_TIN, double CST_NO_PRKNG, double CST_MNTHLY_INT_RT, String CST_PRKNG_ID, double CST_ASD_RT, double CST_RPT_RT, String CST_WP_CSTMR_ID, Integer CST_GL_COA_RCVBL_ACCNT, 
			Integer CST_GL_COA_RVNUE_ACCNT, 
			Integer CST_GL_COA_UNERND_INT_ACCNT, Integer CST_GL_COA_ERND_INT_ACCNT,
			Integer CST_GL_COA_UNERND_PNT_ACCNT, Integer CST_GL_COA_ERND_PNT_ACCNT,
			byte CST_ENBL, byte CST_ENBL_PYRLL,byte CST_ENBL_RTL_CSHR, byte CST_ENBL_RBT, byte CST_AUTO_CMPUTE_INT, byte CST_AUTO_CMPUTE_PNT, Date CST_BRTHDY, 
			String CST_DL_PRC, String CST_AREA, double CST_SQ_MTR, Date CST_ENTRY_DT, short CST_EFFCTVTY_DYS, String CST_AD_LV_RGN, 
			String CST_MMO, String CST_CSTMR_BTCH, String CST_CSTMR_DPRTMNT,
			String CST_APPRVL_STATUS, 
			String CST_RSN_FR_RJCTN, 
			byte CST_PSTD,
			String CST_CRTD_BY, 
			Date CST_DT_CRTD, 	
			String CST_LST_MDFD_BY, 
			Date CST_DT_LST_MDFD, 
			String CST_APPRVD_RJCTD_BY, 
			Date CST_DT_APPRVD_RJCTD, 
			String CST_PSTD_BY, 
			Date CST_DT_PSTD,
			Integer CST_AD_BRNCH,
			Integer CST_AD_CMPNY)     	
	throws CreateException {	
		
		Debug.print("ArCustomerBean ejbCreate");
		
		setCstName(CST_NM);
		setCstCustomerCode(CST_CSTMR_CODE);
		setCstName(CST_NM);
		setCstDescription(CST_DESC);   
		setCstPaymentMethod(CST_PYMNT_MTHD);
		setCstCreditLimit(CST_CRDT_LMT);
		setCstAddress(CST_ADDRSS);
		setCstCity(CST_CTY);
		setCstStateProvince(CST_STT_PRVNC);
		setCstPostalCode(CST_PSTL_CD);
		setCstCountry(CST_CNTRY);
		
		setCstContact(CST_CNTCT);
		setCstEmployeeID(CST_EMP_ID);
		setCstAccountNumber(CST_ACCNT_NO);
		setCstPhone(CST_PHN);
		setCstMobilePhone(CST_MBL_PHN);
		setCstFax(CST_FX);
		setCstAlternatePhone(CST_ALTRNT_PHN);
		setCstAlternateMobilePhone(CST_ALTRNT_MBL_PHN);
		setCstAlternateContact(CST_ALTRNT_CNTCT);
		setCstEmail(CST_EML);
		setCstBillToAddress(CST_BLL_TO_ADDRSS);
		setCstBillToContact(CST_BLL_TO_CNTCT);
		setCstBillToAltContact(CST_BLL_TO_ALT_CNTCT);
		setCstBillToPhone(CST_BLL_TO_PHN);
		setCstBillingHeader(CST_BLLNG_HDR);
		setCstBillingFooter(CST_BLLNG_FTR);
		setCstBillingHeader2(CST_BLLNG_HDR2);
		setCstBillingFooter2(CST_BLLNG_FTR2);
		setCstBillingHeader3(CST_BLLNG_HDR3);
		setCstBillingFooter3(CST_BLLNG_FTR3);
		setCstBillingSignatory(CST_BLLNG_SGNTRY);
		setCstSignatoryTitle(CST_SGNTRY_TTL);
		setCstShipToAddress(CST_SHP_TO_ADDRSS);
		setCstShipToContact(CST_SHP_TO_CNTCT);
		setCstShipToAltContact(CST_SHP_TO_ALT_CNTCT);
		setCstShipToPhone(CST_SHP_TO_PHN);         
		setCstTin(CST_TIN);
		setCstNumbersParking(CST_NO_PRKNG);
		setCstMonthlyInterestRate(CST_MNTHLY_INT_RT);
		setCstParkingID(CST_PRKNG_ID);
		setCstAssociationDuesRate(CST_ASD_RT);
		setCstRealPropertyTaxRate(CST_RPT_RT);
		setCstWordPressCustomerID(CST_WP_CSTMR_ID);
		setCstGlCoaReceivableAccount(CST_GL_COA_RCVBL_ACCNT);
		setCstGlCoaRevenueAccount(CST_GL_COA_RVNUE_ACCNT);
		setCstGlCoaUnEarnedInterestAccount(CST_GL_COA_UNERND_INT_ACCNT);
		setCstGlCoaEarnedInterestAccount(CST_GL_COA_ERND_INT_ACCNT);
		setCstGlCoaUnEarnedPenaltyAccount(CST_GL_COA_UNERND_PNT_ACCNT);
		setCstGlCoaEarnedPenaltyAccount(CST_GL_COA_ERND_PNT_ACCNT);
		setCstEnable(CST_ENBL);
        setCstEnablePayroll(CST_ENBL_PYRLL);
		setCstEnableRetailCashier(CST_ENBL_RTL_CSHR);
		setCstEnableRebate(CST_ENBL_RBT);
		setCstAutoComputeInterest(CST_AUTO_CMPUTE_INT);
		setCstAutoComputePenalty(CST_AUTO_CMPUTE_PNT);
		setCstBirthday(CST_BRTHDY);
		setCstDealPrice(CST_DL_PRC);
		setCstArea(CST_AREA);
		setCstSquareMeter(CST_SQ_MTR);
		setCstEntryDate(CST_ENTRY_DT);
		setCstEffectivityDays(CST_EFFCTVTY_DYS);
		setCstAdLvRegion(CST_AD_LV_RGN);
		setCstMemo(CST_MMO);
		setCstCustomerBatch(CST_CSTMR_BTCH);
		setCstCustomerDepartment(CST_CSTMR_DPRTMNT);
		setCstApprovalStatus(CST_APPRVL_STATUS);
		setCstReasonForRejection(CST_RSN_FR_RJCTN);
		setCstPosted(CST_PSTD);
		setCstCreatedBy(CST_CRTD_BY);
		setCstDateCreated(CST_DT_CRTD);
		setCstLastModifiedBy(CST_LST_MDFD_BY);
		setCstDateLastModified(CST_DT_LST_MDFD);
		setCstApprovedRejectedBy(CST_APPRVD_RJCTD_BY);
		setCstDateApprovedRejected(CST_DT_APPRVD_RJCTD);
		setCstPostedBy(CST_PSTD_BY);
		setCstDatePosted(CST_DT_PSTD);
		
		setCstAdBranch(CST_AD_BRNCH);
		setCstAdCompany(CST_AD_CMPNY);
		
		return null;
	}
	
	public void ejbPostCreate(Integer CST_CODE, 
			String CST_CSTMR_CODE, String CST_NM, String CST_DESC, 
			String CST_PYMNT_MTHD, double CST_CRDT_LMT, String CST_ADDRSS, String CST_CTY, 
			String CST_STT_PRVNC, String CST_PSTL_CD, String CST_CNTRY, 
			String CST_CNTCT, String CST_EMP_ID, String CST_ACCNT_NO, String CST_PHN, String CST_MBL_PHN, String CST_FX, String CST_ALTRNT_PHN, 
			String CST_ALTRNT_MBL_PHN, String CST_ALTRNT_CNTCT, String CST_EML, String CST_BLL_TO_ADDRSS,
			String CST_BLL_TO_CNTCT, String CST_BLL_TO_ALT_CNTCT, String CST_BLL_TO_PHN, 
			String CST_BLLNG_HDR, String CST_BLLNG_FTR, String CST_BLLNG_HDR2, String CST_BLLNG_FTR2,
			String CST_BLLNG_HDR3, String CST_BLLNG_FTR3, String CST_BLLNG_SGNTRY, String CST_SGNTRY_TTL,
			String CST_SHP_TO_ADDRSS, String CST_SHP_TO_CNTCT, String CST_SHP_TO_ALT_CNTCT, 
			String CST_SHP_TO_PHN, String CST_TIN, double CST_NO_PRKNG, double CST_MNTHLY_INT_RT,  String CST_PRKNG_ID, double CST_ASD_RT, double CST_RPT_RT, String CST_WP_CSTMR_ID, Integer CST_GL_COA_RCVBL_ACCNT, 
			Integer CST_GL_COA_RVNUE_ACCNT, 
			Integer CST_GL_COA_UNERND_INT_ACCNT, Integer CST_GL_COA_ERND_INT_ACCNT,
			Integer CST_GL_COA_UNERND_PNT_ACCNT, Integer CST_GL_COA_ERND_PNT_ACCNT,
			byte CST_ENBL, byte CST_ENBL_PYRLL, byte CST_ENBL_RTL_CSHR, byte CST_ENBL_RBT, byte CST_AUTO_CMPUTE_INT, byte CST_AUTO_CMPUTE_PNT, Date CST_BRTHDY, 
			String CST_DL_PRC, String CST_AREA, double CST_SQ_MTR, Date CST_ENTRY_DT, short CST_EFFCTVTY_DYS, String CST_AD_LV_RGN, 
			String CST_MMO, String CST_CSTMR_BTCH, String CST_CSTMR_DPRTMNT, 
			String CST_APPRVL_STATUS, 
			String CST_RSN_FR_RJCTN, 
			byte CST_PSTD,
			String CST_CRTD_BY, 
			Date CST_DT_CRTD, 	
			String CST_LST_MDFD_BY, 
			Date CST_DT_LST_MDFD, 
			String CST_APPRVD_RJCTD_BY, 
			Date CST_DT_APPRVD_RJCTD, 
			String CST_PSTD_BY, 
			Date CST_DT_PSTD,
			Integer CST_AD_BRNCH,
			Integer CST_AD_CMPNY)      
	throws CreateException { }
	
	public void ejbPostCreate(
			String CST_CSTMR_CODE, String CST_NM, String CST_DESC, 
			String CST_PYMNT_MTHD, double CST_CRDT_LMT, String CST_ADDRSS, String CST_CTY, 
			String CST_STT_PRVNC, String CST_PSTL_CD, String CST_CNTRY, 
			String CST_CNTCT, String CST_EMP_ID, String CST_ACCNT_NO, String CST_PHN, String CST_MBL_PHN, String CST_FX, String CST_ALTRNT_PHN, 
			String CST_ALTRNT_MBL_PHN, String CST_ALTRNT_CNTCT, String CST_EML, String CST_BLL_TO_ADDRSS,
			String CST_BLL_TO_CNTCT, String CST_BLL_TO_ALT_CNTCT, String CST_BLL_TO_PHN, 
			String CST_BLLNG_HDR, String CST_BLLNG_FTR, String CST_BLLNG_HDR2, String CST_BLLNG_FTR2,
			String CST_BLLNG_HDR3, String CST_BLLNG_FTR3, String CST_BLLNG_SGNTRY, String CST_SGNTRY_TTL,
			String CST_SHP_TO_ADDRSS, String CST_SHP_TO_CNTCT, String CST_SHP_TO_ALT_CNTCT, 
			String CST_SHP_TO_PHN, String CST_TIN, double CST_NO_PRKNG, double CST_MNTHLY_INT_RT,  String CST_PRKNG_ID, double CST_ASD_RT, double CST_RPT_RT, String CST_WP_CSTMR_ID, Integer CST_GL_COA_RCVBL_ACCNT, 
			Integer CST_GL_COA_RVNUE_ACCNT, 
			Integer CST_GL_COA_UNERND_INT_ACCNT, Integer CST_GL_COA_ERND_INT_ACCNT, 
			Integer CST_GL_COA_UNERND_PNT_ACCNT, Integer CST_GL_COA_ERND_PNT_ACCNT,
			byte CST_ENBL, byte CST_ENBL_PYRLL, byte CST_ENBL_RTL_CSHR, byte CST_ENBL_RBT, byte CST_AUTO_CMPUTE_INT, byte CST_AUTO_CMPUTE_PNT, Date CST_BRTHDY, 
			String CST_DL_PRC, String CST_AREA, double CST_SQ_MTR, Date CST_ENTRY_DT, short CST_EFFCTVTY_DYS, String CST_AD_LV_RGN,  
			String CST_MMO, String CST_CSTMR_BTCH, String CST_CSTMR_DPRTMNT, 
			String CST_APPRVL_STATUS, 
			String CST_RSN_FR_RJCTN, 
			byte CST_PSTD,
			String CST_CRTD_BY, 
			Date CST_DT_CRTD, 	
			String CST_LST_MDFD_BY, 
			Date CST_DT_LST_MDFD, 
			String CST_APPRVD_RJCTD_BY, 
			Date CST_DT_APPRVD_RJCTD, 
			String CST_PSTD_BY, 
			Date CST_DT_PSTD,
			Integer CST_AD_BRNCH,
			Integer CST_AD_CMPNY)
	throws CreateException { }     
}