/*
 * com/ejb/ar/LocalArCustomerClassBean.java
 *
 * Created on March 03, 2004, 2:28 PM
 */

package com.ejb.ar;

import java.util.Collection;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;

import com.util.AbstractEntityBean;
import com.util.Debug;

/**
 *
 * @author  Neil Andrew M. Ajero
 *
 * @ejb:bean name="ArCustomerClassEJB"
 *           display-name="CustomerClass Entity"
 *           type="CMP"
 *           cmp-version="2.x"
 *           view-type="local"
 *           local-jndi-name="omega-ejb/ArCustomerClassEJB"
 *           schema="ArCustomerClass"
 *           primkey-field="ccCode"
 *
 * @ejb:pk class="java.lang.Integer"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="aruser"
 *                        role-link="aruserlink"
 *
 * @ejb:permission role-name="aruser"
 *
 * @ejb:interface local-class="com.ejb.ar.LocalArCustomerClass"
 *                local-extends="javax.ejb.EJBLocalObject"
 *
 * @ejb:home local-class="com.ejb.ar.LocalArCustomerClassHome"
 *           local-extends="javax.ejb.EJBLocalHome"
 *
 * @ejb:finder signature="Collection findEnabledCcAll(java.lang.Integer CC_AD_CMPNY)"
 *             query="SELECT OBJECT(cc) FROM ArCustomerClass cc WHERE cc.ccEnable = 1 AND cc.ccAdCompany = ?1"
 *
 * @jboss:query signature="Collection findEnabledCcAll(java.lang.Integer CC_AD_CMPNY)"
 *             query="SELECT OBJECT(cc) FROM ArCustomerClass cc WHERE cc.ccEnable = 1 AND cc.ccAdCompany = ?1 ORDER BY cc.ccName"
 *
 * @ejb:finder signature="LocalArCustomerClass findByCcName(java.lang.String CC_NM, java.lang.Integer CC_AD_CMPNY)"
 *             query="SELECT OBJECT(cc) FROM ArCustomerClass cc WHERE cc.ccName = ?1 AND cc.ccAdCompany = ?2"
 *
 * @ejb:finder signature="Collection findCcAll(java.lang.Integer CC_AD_CMPNY)"
 *             query="SELECT OBJECT(cc) FROM ArCustomerClass cc WHERE cc.ccAdCompany = ?1"
 *
 * @jboss:query signature="Collection findCcAll(java.lang.Integer CC_AD_CMPNY)"
 *             query="SELECT OBJECT(cc) FROM ArCustomerClass cc WHERE cc.ccAdCompany = ?1 ORDER BY cc.ccName"
 *             
 * @ejb:finder signature="Collection findByCcGlCoaChargeAccount(java.lang.Integer COA_CODE, java.lang.Integer CC_AD_CMPNY)"
 *             query="SELECT OBJECT(cc) FROM ArCustomerClass cc WHERE cc.ccGlCoaChargeAccount=?1 AND cc.ccAdCompany = ?2"
 * 
 * @ejb:finder signature="Collection findByCcGlCoaReceivableAccount(java.lang.Integer COA_CODE, java.lang.Integer CC_AD_CMPNY)"
 *             query="SELECT OBJECT(cc) FROM ArCustomerClass cc WHERE cc.ccGlCoaReceivableAccount=?1 AND cc.ccAdCompany = ?2"
 * 
 * @ejb:finder signature="Collection findByCcGlCoaRevenueAccount(java.lang.Integer COA_CODE, java.lang.Integer CC_AD_CMPNY)"
 *             query="SELECT OBJECT(cc) FROM ArCustomerClass cc WHERE cc.ccGlCoaRevenueAccount=?1 AND cc.ccAdCompany = ?2"
 *
 * @ejb:select signature="Collection ejbSelectGeneric(java.lang.String jbossQl, java.lang.Object[] args)"
 *             result-type-mapping="Local"
 *             method-intf="LocalHome"
 *             query="SELECT OBJECT(cc) FROM ArCustomerClass cc"
 *
 * @jboss:query signature="Collection ejbSelectGeneric(java.lang.String jbossQl, java.lang.Object[] args)"
 *                         dynamic="true"
 * 
 * @ejb:value-object match="*"
 *             name="ArCustomerClass"
 *
 * @jboss:persistence table-name="AR_CSTMR_CLSS"
 *
 * @jboss:entity-command name="mysql-get-generated-keys"
 */

public abstract class ArCustomerClassBean extends AbstractEntityBean {
    
    
    // PERSISTENT METHODS
    
    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     * @ejb:pk-field
     *
     * @jboss:column-name name="CC_CODE"
     *
     * @jboss:persistence auto-increment="true"
     **/
    public abstract Integer getCcCode();
    public abstract void setCcCode(Integer CC_CODE);
    
    
    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"   
     *
     * @jboss:column-name name="CC_NM"
     **/
    public abstract String getCcName();
     /**
      * @ejb:interface-method view-type="local"
      **/ 
    public abstract void setCcName(String CC_NM);
    
    
    

    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"   
     *
     * @jboss:column-name name="CC_DESC"
     **/
    public abstract String getCcDescription();
     /**
      * @ejb:interface-method view-type="local"
      **/ 
    public abstract void setCcDescription(String CC_DESC);
    
    /**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="CC_NXT_CSTMR_CODE"
	 **/
	public abstract String getCcNextCustomerCode();
	/**
	 * @ejb:interface-method view-type="local"
	 **/ 
	public abstract void setCcNextCustomerCode(String CC_NXT_CSTMR_CODE);
    
    /**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="CC_CSTMR_BTCH"
	 **/
	public abstract String getCcCustomerBatch();
	/**
	 * @ejb:interface-method view-type="local"
	 **/ 
	public abstract void setCcCustomerBatch(String CC_CSTMR_BTCH);
	
	/**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="CC_DL_PRC"
     **/
    public abstract String getCcDealPrice();
     /**
      * @ejb:interface-method view-type="local"
      **/ 
    public abstract void setCcDealPrice(String CC_DL_PRC);

    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"   
     *
     * @jboss:column-name name="CC_MNTHLY_INT_RT"
     **/
    public abstract double getCcMonthlyInterestRate();
     /**
      * @ejb:interface-method view-type="local"
      **/ 
    public abstract void setCcMonthlyInterestRate(double CC_MNTHLY_INT_RT);

     /**
      * @ejb:persistent-field
      * @ejb:interface-method view-type="local"
      *
      * @jboss:column-name name="CC_MNMM_FNNC_CHRG"
      **/
     public abstract double getCcMinimumFinanceCharge();
     /**
      * @ejb:interface-method view-type="local"
      **/
     public abstract void setCcMinimumFinanceCharge(double CC_MNMM_FNNC_CHRG);
     
     
     /**
      * @ejb:persistent-field
      * @ejb:interface-method view-type="local"
      *
      * @jboss:column-name name="CC_GRC_PRD_DY"
      **/
     public abstract short getCcGracePeriodDay();
     /**
      * @ejb:interface-method view-type="local"
      **/
     public abstract void setCcGracePeriodDay(short CC_GRC_PRD_DY);
     
     
     /**
      * @ejb:persistent-field
      * @ejb:interface-method view-type="local"
      *
      * @jboss:column-name name="CC_DYS_IN_PRD"
      **/
     public abstract short getCcDaysInPeriod();
     /**
      * @ejb:interface-method view-type="local"
      **/
     public abstract void setCcDaysInPeriod(short CC_DYS_IN_PRD);
         
     /**
      * @ejb:persistent-field
      * @ejb:interface-method view-type="local"
      *
      * @jboss:column-name name="CC_GL_COA_CHRG_ACCNT"
      **/
     public abstract Integer getCcGlCoaChargeAccount();
     /**
      * @ejb:interface-method view-type="local"
      **/
     public abstract void setCcGlCoaChargeAccount(Integer CC_GL_COA_CHRG_ACCNT);
     
     
     /**
      * @ejb:persistent-field
      * @ejb:interface-method view-type="local"
      *
      * @jboss:column-name name="CC_CHRG_BY_DUE_DT"
      **/
     public abstract byte getCcChargeByDueDate();
     /**
      * @ejb:interface-method view-type="local"
      **/
     public abstract void setCcChargeByDueDate(byte CC_CHRG_BY_DUE_DT);        

    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="CC_GL_COA_RCVBL_ACCNT"
     **/
    public abstract java.lang.Integer getCcGlCoaReceivableAccount();
     /**
      * @ejb:interface-method view-type="local"
      **/ 
    public abstract void setCcGlCoaReceivableAccount(java.lang.Integer CC_GL_COA_RCVBL_ACCNT);
    
    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="CC_GL_COA_RVNUE_ACCNT"
     **/
    public abstract java.lang.Integer getCcGlCoaRevenueAccount();
     /**
      * @ejb:interface-method view-type="local"
      **/ 
    public abstract void setCcGlCoaRevenueAccount(java.lang.Integer CC_GL_COA_RVNUE_ACCNT);
    
    
    
    /**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="CC_GL_COA_UNERND_INT_ACCNT"
	 **/
	public abstract Integer getCcGlCoaUnEarnedInterestAccount();
	/**
	 * @ejb:interface-method view-type="local"
	 **/ 
	public abstract void setCcGlCoaUnEarnedInterestAccount(Integer CC_GL_COA_UNERND_INT_ACCNT);
	
	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="CC_GL_COA_ERND_INT_ACCNT"
	 **/
	public abstract Integer getCcGlCoaEarnedInterestAccount();
	/**
	 * @ejb:interface-method view-type="local"
	 **/ 
	public abstract void setCcGlCoaEarnedInterestAccount(Integer CC_GL_COA_ERND_INT_ACCNT);
	
	
	
	
	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="CC_GL_COA_UNERND_PNT_ACCNT"
	 **/
	public abstract Integer getCcGlCoaUnEarnedPenaltyAccount();
	/**
	 * @ejb:interface-method view-type="local"
	 **/ 
	public abstract void setCcGlCoaUnEarnedPenaltyAccount(Integer CC_GL_COA_UNERND_PNT_ACCNT);
	
	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="CC_GL_COA_ERND_PNT_ACCNT"
	 **/
	public abstract Integer getCcGlCoaEarnedPenaltyAccount();
	/**
	 * @ejb:interface-method view-type="local"
	 **/ 
	public abstract void setCcGlCoaEarnedPenaltyAccount(Integer CC_GL_COA_ERND_PNT_ACCNT);
	
    
    
    
    
    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="CC_ENBL"
     **/
    public abstract byte getCcEnable();
     /**
      * @ejb:interface-method view-type="local"
      **/ 
    public abstract void setCcEnable(byte CC_ENBL);
    
    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="CC_ENBL_RBT"
     **/
    public abstract byte getCcEnableRebate();
     /**
      * @ejb:interface-method view-type="local"
      **/ 
    public abstract void setCcEnableRebate(byte CC_ENBL_RBT);
    
    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="CC_AUTO_CMPUTE_INT"
     **/
    public abstract byte getCcAutoComputeInterest();
     /**
      * @ejb:interface-method view-type="local"
      **/ 
    public abstract void setCcAutoComputeInterest(byte CC_AUTO_CMPUTE_INT);
    
    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="CC_AUTO_CMPUTE_PNT"
     **/
    public abstract byte getCcAutoComputePenalty();
     /**
      * @ejb:interface-method view-type="local"
      **/ 
    public abstract void setCcAutoComputePenalty(byte CC_AUTO_CMPUTE_PNT);
    
    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="CC_CRDT_LMT"
     **/
    public abstract double getCcCreditLimit();
     /**
      * @ejb:interface-method view-type="local"
      **/ 
    public abstract void setCcCreditLimit(double CC_CRDT_LMT);
       
    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="CC_AD_CMPNY"
     **/
    public abstract Integer getCcAdCompany();
     /**
      * @ejb:interface-method view-type="local"
      **/ 
    public abstract void setCcAdCompany(Integer CC_AD_CMPNY);
       
   
    // RELATIONSHIP METHODS
    
    /**
     * @ejb:interface-method view-type="local"
     * @ejb:relation name="artaxcode-customerclasses"
     *               role-name="customerclass-has-one-artaxcode"
     * 
     * @jboss:relation related-pk-field="tcCode"
     *                 fk-column="AR_TAX_CODE"
     */
    public abstract LocalArTaxCode getArTaxCode();
    public abstract void setArTaxCode(LocalArTaxCode arTaxCode);

    /**
     * @ejb:interface-method view-type="local"
     * @ejb:relation name="arwithholdingtaxcode-customerclasses"
     *               role-name="customerclass-has-one-arwithholdingtaxcode"
     * 
     * @jboss:relation related-pk-field="wtcCode"
     *                 fk-column="AR_WITHHOLDING_TAX_CODE"
     */
    public abstract LocalArWithholdingTaxCode getArWithholdingTaxCode();
    public abstract void setArWithholdingTaxCode(LocalArWithholdingTaxCode arWithholdingTaxCode);


    /**
     * @ejb:interface-method view-type="local"
     * @ejb:relation name="customerclass-customers"
     *               role-name="customerclass-has-many-customers"
     * 
     */
    public abstract Collection getArCustomers();
    public abstract void setArCustomers(Collection arCustomers);    

    
    
    /**
    * @ejb:interface-method view-type="local"
    * @ejb:relation name="customerclass-standardmemolineclasses"
    *               role-name="customerclass-has-many-standardmemolineclasses"
    * 
    */
    public abstract Collection getArStandardMemoLineClasses();
    public abstract void setArStandardMemoLineClasses(Collection arCustomers);
    
    
    
    
    
    
    /**
     * @jboss:dynamic-ql
     */
     public abstract Collection ejbSelectGeneric(java.lang.String jbossQl, java.lang.Object[] args)
        throws FinderException;
        
     

     
     
     
     
    // BUSINESS METHODS
    
    /**
     * @ejb:home-method view-type="local"
     */
     public Collection ejbHomeGetCcByCriteria(java.lang.String jbossQl, java.lang.Object[] args)
        throws FinderException {
        	
        return ejbSelectGeneric(jbossQl, args);
     }
     
    /**
     * @ejb:interface-method view-type="local"
     **/
    public void addArCustomer(LocalArCustomer arCustomer) {

         Debug.print("ArCustomerClassBean addArCustomer");
      
         try {
      	
            Collection arCustomers = getArCustomers();
	        arCustomers.add(arCustomer);
	          
         } catch (Exception ex) {
      	
           throw new EJBException(ex.getMessage());
      
         }   
     }

    /**
     * @ejb:interface-method view-type="local"
     **/
    public void dropArCustomer(LocalArCustomer arCustomer) {

         Debug.print("ArCustomerClassBean dropArCustomer");
      
         try {
      	
            Collection arCustomers = getArCustomers();
	        arCustomers.remove(arCustomer);
	      
         } catch (Exception ex) {
      	
           throw new EJBException(ex.getMessage());
         
         }   
     }
    
    
    /**
     * @ejb:interface-method view-type="local"
     **/
    public void addArStandardMemoLineClass(LocalArStandardMemoLineClass arStandardMemoLineClass) {

         Debug.print("ArCustomerClassBean addArStandardMemoLineClass");
      
         try {
      	
            Collection arStandardMemoLineClasses = getArStandardMemoLineClasses();
	        arStandardMemoLineClasses.add(arStandardMemoLineClass);
	          
         } catch (Exception ex) {
      	
           throw new EJBException(ex.getMessage());
      
         }   
     }

    /**
     * @ejb:interface-method view-type="local"
     **/
    public void dropArStandardMemoLineClass(LocalArStandardMemoLineClass arStandardMemoLineClass) {

         Debug.print("ArCustomerClassBean dropArStandardMemoLineClass");
      
         try {
      	
            Collection arStandardMemoLineClasses = getArStandardMemoLineClasses();
	        arStandardMemoLineClasses.remove(arStandardMemoLineClass);
	      
         } catch (Exception ex) {
      	
           throw new EJBException(ex.getMessage());
         
         }   
     }

    // ENTITY METHODS
    
    /**
     * @ejb:create-method view-type="local"
     **/
    public Integer ejbCreate(Integer CC_CODE, 
         String CC_NM, String CC_DESC, 
         String CC_NXT_CSTMR_CODE,
         String CC_CSTMR_BTCH, String CC_DL_PRC,
         double CC_MNTHLY_INT_RT, 
         double CC_MNMM_FNNC_CHRG, short CC_GRC_PRD_DY, short CC_DYS_IN_PRD, 
         Integer CC_GL_COA_CHRG_ACCNT, byte CC_CHRG_BY_DUE_DT,
         Integer CC_GL_COA_RCVBL_ACCNT, Integer CC_GL_COA_RVNUE_ACCNT, 
         Integer CC_GL_COA_UNERND_INT_ACCNT, Integer CC_GL_COA_ERND_INT_ACCNT,
			Integer CC_GL_COA_UNERND_PNT_ACCNT, Integer CC_GL_COA_ERND_PNT_ACCNT,
         byte CC_ENBL,
         byte CC_ENBL_RBT,
         byte CC_AUTO_CMPUTE_INT,
         byte CC_AUTO_CMPUTE_PNT,
         double CC_CRDT_LMT, Integer CC_AD_CMPNY)     	
         throws CreateException {	
     	
         Debug.print("ArCustomerClassBean ejbCreate");
     
	     setCcCode(CC_CODE);
	     setCcName(CC_NM);
	     setCcDescription(CC_DESC);
	     setCcNextCustomerCode(CC_NXT_CSTMR_CODE);
	     setCcCustomerBatch(CC_CSTMR_BTCH);
	     setCcDealPrice(CC_DL_PRC);
	     setCcMonthlyInterestRate(CC_MNTHLY_INT_RT);
	     setCcMinimumFinanceCharge(CC_MNMM_FNNC_CHRG);
	     setCcGracePeriodDay(CC_GRC_PRD_DY);
	     setCcDaysInPeriod(CC_DYS_IN_PRD);
	     setCcGlCoaChargeAccount(CC_GL_COA_CHRG_ACCNT);
	     setCcChargeByDueDate(CC_CHRG_BY_DUE_DT);
	     setCcGlCoaReceivableAccount(CC_GL_COA_RCVBL_ACCNT);
	     setCcGlCoaRevenueAccount(CC_GL_COA_RVNUE_ACCNT);
	     setCcGlCoaUnEarnedInterestAccount(CC_GL_COA_UNERND_INT_ACCNT);
	     setCcGlCoaEarnedInterestAccount(CC_GL_COA_ERND_INT_ACCNT);
	     setCcGlCoaUnEarnedPenaltyAccount(CC_GL_COA_UNERND_PNT_ACCNT);
	     setCcGlCoaEarnedPenaltyAccount(CC_GL_COA_ERND_PNT_ACCNT);
	     setCcEnable(CC_ENBL);
	     setCcEnableRebate(CC_ENBL_RBT);
	     setCcAutoComputeInterest(CC_AUTO_CMPUTE_INT);
	     setCcAutoComputePenalty(CC_AUTO_CMPUTE_PNT);
	     setCcCreditLimit(CC_CRDT_LMT);
	     setCcAdCompany(CC_AD_CMPNY);

         return null;
     }
     
    /**
     * @ejb:create-method view-type="local"
     **/
    public Integer ejbCreate(
         String CC_NM, String CC_DESC, 
         String CC_NXT_CSTMR_CODE,
         String CC_CSTMR_BTCH, String CC_DL_PRC,
         double CC_MNTHLY_INT_RT, 
         double CC_MNMM_FNNC_CHRG, short CC_GRC_PRD_DY, short CC_DYS_IN_PRD, 
         Integer CC_GL_COA_CHRG_ACCNT, byte CC_CHRG_BY_DUE_DT,
         Integer CC_GL_COA_RCVBL_ACCNT, Integer CC_GL_COA_RVNUE_ACCNT, 
         Integer CC_GL_COA_UNERND_INT_ACCNT, Integer CC_GL_COA_ERND_INT_ACCNT,
         Integer CC_GL_COA_UNERND_PNT_ACCNT, Integer CC_GL_COA_ERND_PNT_ACCNT,
         byte CC_ENBL,
         byte CC_ENBL_RBT,
         byte CC_AUTO_CMPUTE_INT,
         byte CC_AUTO_CMPUTE_PNT,
         double CC_CRDT_LMT, Integer CC_AD_CMPNY)     	
         throws CreateException {	
     	
         Debug.print("ArCustomerClassBean ejbCreate");
     
	     setCcName(CC_NM);
	     setCcDescription(CC_DESC);
	     setCcNextCustomerCode(CC_NXT_CSTMR_CODE);
	     setCcCustomerBatch(CC_CSTMR_BTCH);
	     setCcDealPrice(CC_DL_PRC);
	     setCcMonthlyInterestRate(CC_MNTHLY_INT_RT);
	     setCcMinimumFinanceCharge(CC_MNMM_FNNC_CHRG);
	     setCcGracePeriodDay(CC_GRC_PRD_DY);
	     setCcDaysInPeriod(CC_DYS_IN_PRD);
	     setCcGlCoaChargeAccount(CC_GL_COA_CHRG_ACCNT);
	     setCcChargeByDueDate(CC_CHRG_BY_DUE_DT);
	     setCcGlCoaReceivableAccount(CC_GL_COA_RCVBL_ACCNT);
	     setCcGlCoaRevenueAccount(CC_GL_COA_RVNUE_ACCNT);
	     setCcGlCoaUnEarnedInterestAccount(CC_GL_COA_UNERND_INT_ACCNT);
	     setCcGlCoaEarnedInterestAccount(CC_GL_COA_ERND_INT_ACCNT);
	     setCcGlCoaUnEarnedPenaltyAccount(CC_GL_COA_UNERND_PNT_ACCNT);
	     setCcGlCoaEarnedPenaltyAccount(CC_GL_COA_ERND_PNT_ACCNT);
	     setCcEnable(CC_ENBL);
	     setCcAutoComputeInterest(CC_AUTO_CMPUTE_INT);
	     setCcAutoComputePenalty(CC_AUTO_CMPUTE_PNT);
	     setCcCreditLimit(CC_CRDT_LMT);
	     setCcAdCompany(CC_AD_CMPNY);

         return null;
     }
     
     public void ejbPostCreate(Integer CC_CODE, 
         String CC_NM, String CC_DESC, 
         String CC_NXT_CSTMR_CODE,
         String CC_CSTMR_BTCH, String CC_DL_PRC,
         double CC_MNTHLY_INT_RT, 
         double CC_MNMM_FNNC_CHRG, short CC_GRC_PRD_DY, short CC_DYS_IN_PRD, 
         Integer CC_GL_COA_CHRG_ACCNT, byte CC_CHRG_BY_DUE_DT,
         Integer CC_COA_GL_RCVBL_ACCNT, Integer CC_GL_COA_RVNUE_ACCNT, 
         Integer CC_GL_COA_UNERND_INT_ACCNT, Integer CC_GL_COA_ERND_INT_ACCNT,
         Integer CC_GL_COA_UNERND_PNT_ACCNT, Integer CC_GL_COA_ERND_PNT_ACCNT,
         byte CC_ENBL,
         byte CC_ENBL_RBT,
         byte CC_AUTO_CMPUTE_INT,
         byte CC_AUTO_CMPUTE_PNT,
         
         double CC_CRDT_LMT, Integer CC_AD_CMPNY)      
         throws CreateException { }
      
     public void ejbPostCreate(
         String CC_NM, String CC_DESC, 
         String CC_NXT_CSTMR_CODE,
         String CC_CSTMR_BTCH, String CC_DL_PRC,
         double CC_MNTHLY_INT_RT, 
         double CC_MNMM_FNNC_CHRG, short CC_GRC_PRD_DY, short CC_DYS_IN_PRD, 
         Integer CC_GL_COA_CHRG_ACCNT, byte CC_CHRG_BY_DUE_DT,
         Integer CC_COA_GL_RCVBL_ACCNT, Integer CC_GL_COA_RVNUE_ACCNT, 
         Integer CC_GL_COA_UNERND_INT_ACCNT, Integer CC_GL_COA_ERND_INT_ACCNT,
         Integer CC_GL_COA_UNERND_PNT_ACCNT, Integer CC_GL_COA_ERND_PNT_ACCNT,
         byte CC_ENBL,
         byte CC_ENBL_RBT,
         byte CC_AUTO_CMPUTE_INT,
         byte CC_AUTO_CMPUTE_PNT,
         double CC_CRDT_LMT, Integer CC_AD_CMPNY)
         throws CreateException { }     
}