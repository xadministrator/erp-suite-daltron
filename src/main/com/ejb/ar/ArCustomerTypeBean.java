/*
 * com/ejb/ar/LocalArCustomerTypeBean.java
 *
 * Created on March 03, 2004, 2:11 PM
 */

package com.ejb.ar;

import java.util.Collection;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;

import com.util.AbstractEntityBean;
import com.util.Debug;

/**
 *
 * @author  Neil Andrew M. Ajero
 *
 * @ejb:bean name="ArCustomerTypeEJB"
 *           display-name="CustomerType Entity"
 *           type="CMP"
 *           cmp-version="2.x"
 *           view-type="local"
 *           local-jndi-name="omega-ejb/ArCustomerTypeEJB"
 *           schema="ArCustomerType"
 *           primkey-field="ctCode"
 *
 * @ejb:pk class="java.lang.Integer"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="aruser"
 *                        role-link="aruserlink"
 *
 * @ejb:permission role-name="aruser"
 *
 * @ejb:interface local-class="com.ejb.ar.LocalArCustomerType"
 *                local-extends="javax.ejb.EJBLocalObject"
 *
 * @ejb:home local-class="com.ejb.ar.LocalArCustomerTypeHome"
 *           local-extends="javax.ejb.EJBLocalHome"
 *
 * @ejb:finder signature="Collection findEnabledCtAll(java.lang.Integer CT_AD_CMPNY)"
 *             query="SELECT OBJECT(ct) FROM ArCustomerType ct WHERE ct.ctEnable = 1 AND ct.ctAdCompany = ?1"
 *
 * @jboss:query signature="Collection findEnabledCtAll(java.lang.Integer CT_AD_CMPNY)"
 *             query="SELECT OBJECT(ct) FROM ArCustomerType ct WHERE ct.ctEnable = 1 AND ct.ctAdCompany = ?1 ORDER BY ct.ctName"
 *
 * @ejb:finder signature="Collection findCtAll(java.lang.Integer CT_AD_CMPNY)"
 *             query="SELECT OBJECT(ct) FROM ArCustomerType ct WHERE ct.ctAdCompany = ?1"
 *
 * @jboss:query signature="Collection findCtAll(java.lang.Integer CT_AD_CMPNY)"
 *             query="SELECT OBJECT(ct) FROM ArCustomerType ct WHERE ct.ctAdCompany = ?1 ORDER BY ct.ctName"
 *
 * @ejb:finder signature="LocalArCustomerType findByCtName(java.lang.String CT_NM, java.lang.Integer CT_AD_CMPNY)"
 *             query="SELECT OBJECT(ct) FROM ArCustomerType ct WHERE ct.ctName = ?1 AND ct.ctAdCompany = ?2"
 * 
 * @ejb:select signature="Collection ejbSelectGeneric(java.lang.String jbossQl, java.lang.Object[] args)"
 *             result-type-mapping="Local"
 *             method-intf="LocalHome"
 *             query="SELECT OBJECT(ct) FROM ArCustomerType ct"
 *
 * @jboss:query signature="Collection ejbSelectGeneric(java.lang.String jbossQl, java.lang.Object[] args)"
 *                         dynamic="true"
 *
 * @ejb:value-object match="*"
 *             name="ArCustomerType"
 *
 * @jboss:persistence table-name="AR_CSTMR_TYP"
 *
 * @jboss:entity-command name="mysql-get-generated-keys"
 */

public abstract class ArCustomerTypeBean extends AbstractEntityBean {
    
    
    // PERSISTENT METHODS
    
    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     * @ejb:pk-field
     *
     * @jboss:column-name name="CT_CODE"
     *
     * @jboss:persistence auto-increment="true"
     **/
    public abstract Integer getCtCode();
    public abstract void setCtCode(Integer CT_CODE);
    
    
    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"   
     *
     * @jboss:column-name name="CT_NM"
     **/
    public abstract String getCtName();
     /**
      * @ejb:interface-method view-type="local"
      **/ 
    public abstract void setCtName(String CT_NM);
    

    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"   
     *
     * @jboss:column-name name="CT_DESC"
     **/
    public abstract String getCtDescription();
     /**
      * @ejb:interface-method view-type="local"
      **/ 
    public abstract void setCtDescription(String CT_DESC);
    
    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="CT_ENBL"
     **/
    public abstract byte getCtEnable();
     /**
      * @ejb:interface-method view-type="local"
      **/ 
    public abstract void setCtEnable(byte CT_ENBL);
    
    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="CT_AD_CMPNY"
     **/
    public abstract Integer getCtAdCompany();
     /**
      * @ejb:interface-method view-type="local"
      **/ 
    public abstract void setCtAdCompany(Integer CT_AD_CMPNY);
    
   
    // RELATIONSHIP METHODS
    
    /**
     * @ejb:interface-method view-type="local"
     * @ejb:relation name="bankaccount-customertypes"
     *               role-name="customertype-has-one-bankaccount"
     * 
     * @jboss:relation related-pk-field="baCode"
     *                 fk-column="AD_BANK_ACCOUNT"
     */
    public abstract com.ejb.ad.LocalAdBankAccount getAdBankAccount();
    public abstract void setAdBankAccount(com.ejb.ad.LocalAdBankAccount adBankAccount);


    /**
     * @ejb:interface-method view-type="local"
     * @ejb:relation name="customertype-customers"
     *               role-name="customertype-has-many-customers"
     * 
     */
    public abstract Collection getArCustomers();
    public abstract void setArCustomers(Collection arCustomer);    
     

    /**
     * @jboss:dynamic-ql
     */
     public abstract Collection ejbSelectGeneric(java.lang.String jbossQl, java.lang.Object[] args)
        throws FinderException;
        
     

    // BUSINESS METHODS
    
    /**
     * @ejb:home-method view-type="local"
     */
     public Collection ejbHomeGetCtByCriteria(java.lang.String jbossQl, java.lang.Object[] args)
        throws FinderException {
        	
        return ejbSelectGeneric(jbossQl, args);
     }
     
    /**
     * @ejb:interface-method view-type="local"
     **/
    public void addArCustomer(LocalArCustomer arCustomer) {

         Debug.print("ArCustomerTypeBean addArCustomer");
      
         try {
      	
            Collection arCustomers = getArCustomers();
	        arCustomers.add(arCustomer);
	          
         } catch (Exception ex) {
      	
           throw new EJBException(ex.getMessage());
      
         }   
     }

    /**
     * @ejb:interface-method view-type="local"
     **/
    public void dropArCustomer(LocalArCustomer arCustomer) {

         Debug.print("ArCustomerTypeBean dropArCustomer");
      
         try {
      	
            Collection arCustomers = getArCustomers();
	        arCustomers.remove(arCustomer);
	      
         } catch (Exception ex) {
      	
           throw new EJBException(ex.getMessage());
         
         }   
     }
     

    // ENTITY METHODS
    
    /**
     * @ejb:create-method view-type="local"
     **/
    public Integer ejbCreate(Integer CT_CODE, 
         String CT_NM, String CT_DESC, byte CT_ENBL, 
		 Integer CT_AD_CMPNY)     	
         throws CreateException {	
     	
         Debug.print("ArCustomerTypeBean ejbCreate");
     
	     setCtCode(CT_CODE);
	     setCtName(CT_NM);
	     setCtDescription(CT_DESC);   
         setCtEnable(CT_ENBL);
         setCtAdCompany(CT_AD_CMPNY);

         return null;
     }
     
    /**
     * @ejb:create-method view-type="local"
     **/
    public Integer ejbCreate(
         String CT_NM, String CT_DESC, byte CT_ENBL,
		 Integer CT_AD_CMPNY)     	
         throws CreateException {	
     	
         Debug.print("ArCustomerTypeBean ejbCreate");
     
	     setCtName(CT_NM);
	     setCtDescription(CT_DESC);   
         setCtEnable(CT_ENBL);
         setCtAdCompany(CT_AD_CMPNY);

         return null;
     }
     
     public void ejbPostCreate(Integer CT_CODE, 
         String CT_NM, String CT_DESC, byte CT_ENBL,
		 Integer CT_AD_CMPNY)      
         throws CreateException { }
      
     public void ejbPostCreate(
         String CT_NM, String CT_DESC, byte CT_ENBL,
		 Integer CT_AD_CMPNY)
         throws CreateException { }     
}