package com.ejb.ar;

import java.util.Collection;
import java.util.Date;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;

import com.util.AbstractEntityBean;
import com.util.Debug;

/**
 *
 * @ejb:bean name="ArDeliveryEJB"
 *           display-name="Delivery"
 *           type="CMP"
 *           cmp-version="2.x"
 *           view-type="local"
 *           local-jndi-name="omega-ejb/ArDeliveryEJB"
 *           schema="ArDelivery"
 *           primkey-field="dvCode"
 *
 * @ejb:pk class="java.lang.Integer"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="aruser"
 *                        role-link="aruserlink"
 *
 * @ejb:permission role-name="aruser"
 *
 * @ejb:interface local-class="com.ejb.ar.LocalArDelivery"
 *                local-extends="javax.ejb.EJBLocalObject"
 *
 * @ejb:home local-class="com.ejb.ar.LocalArDeliveryHome"
 *           local-extends="javax.ejb.EJBLocalHome"
 *           
 *           
 * @ejb:finder signature="LocalArDelivery findByDvContainerAndSoDocumentNumber(java.lang.String DV_CNTNR, java.lang.String SO_DCMNT_NMBR)"
 *           query="SELECT OBJECT(dv) FROM ArDelivery dv  WHERE dv.dvContainer = ?1 AND dv.arSalesOrder.soDocumentNumber = ?2"
 * 
 * @ejb:finder signature="LocalArDelivery findByDvDeliveryNumber(java.lang.String DV_DLVRY_NMBR)"
 *           query="SELECT OBJECT(dv) FROM ArDelivery dv  WHERE dv.dvDeliveryNumber = ?1"
 *           
 * @ejb:finder signature="Collection findBySoCode(java.lang.Integer SO_CODE)"
 *           query="SELECT OBJECT(dv) FROM ArDelivery dv  WHERE dv.arSalesOrder.soCode = ?1"
 *           
 * @ejb:finder signature="Collection findBySoReferenceNumber(java.lang.String SO_RFRNC_NMBR)"
 *           query="SELECT OBJECT(dv) FROM ArDelivery dv  WHERE dv.arSalesOrder.soReferenceNumber = ?1"
 * 
 * @ejb:finder signature="Collection findBySoDocumentNumber(java.lang.String SO_DCMNT_NMBR)"
 *           query="SELECT OBJECT(dv) FROM ArDelivery dv  WHERE dv.arSalesOrder.soDocumentNumber = ?1"
 * *
 * @ejb:select signature="Collection ejbSelectGeneric(java.lang.String jbossQl, java.lang.Object[] args)"
 *             result-type-mapping="Local"
 *             method-intf="LocalHome"
 *             query="SELECT OBJECT(dv) FROM ArDelivery dv"
 *
 * @jboss:query signature="Collection ejbSelectGeneric(java.lang.String jbossQl, java.lang.Object[] args)"
 *                         dynamic="true"
 *
 * @ejb:value-object match="*"
 *             name="ArDelivery"
 *
 * @jboss:persistence table-name="AR_DLVRY"
 *
 * @jboss:entity-command name="mysql-get-generated-keys"
 */

public abstract class ArDeliveryBean extends AbstractEntityBean {

    //  PERSISTENT METHODS

    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     * @ejb:pk-field
     *
     * @jboss:column-name name="DV_CODE"
     *
     * @jboss:persistence auto-increment="true"
     **/
    public abstract Integer getDvCode();
    public abstract void setDvCode(Integer DV_CODE);

    
    /**
	  * @ejb:persistent-field
	  * @ejb:interface-method view-type="local"
	  *
	  * @jboss:column-name name="DV_DLVRY_NMBR"
	  **/
	 public abstract String getDvDeliveryNumber();
	 /**
	  * @ejb:interface-method view-type="local"
	  **/
	 public abstract void setDvDeliveryNumber(String DV_DLVRY_NMBR);
    
    
    /**
	  * @ejb:persistent-field
	  * @ejb:interface-method view-type="local"
	  *
	  * @jboss:column-name name="DV_CNTNR"
	  **/
	 public abstract String getDvContainer();
	 /**
	  * @ejb:interface-method view-type="local"
	  **/
	 public abstract void setDvContainer(String DV_CNTNR);
    
	 
	 /**
	  * @ejb:persistent-field
	  * @ejb:interface-method view-type="local"
	  *
	  * @jboss:column-name name="DV_BKNG_TM"
	  **/
	 public abstract String getDvBookingTime();
	 /**
	  * @ejb:interface-method view-type="local"
	  **/
	 public abstract void setDvBookingTime(String DV_BKNG_TM);
    
	 /**
	  * @ejb:persistent-field
	  * @ejb:interface-method view-type="local"
	  *
	  * @jboss:column-name name="DV_TBS_FEE_PLL_OUT"
	  **/
	 public abstract String getDvTabsFeePullOut();
	 /**
	  * @ejb:interface-method view-type="local"
	  **/
	 public abstract void setDvTabsFeePullOut(String DV_TBS_FEE_PLL_OUT);
	 
	 
	 /**
	  * @ejb:persistent-field
	  * @ejb:interface-method view-type="local"
	  *
	  * @jboss:column-name name="DV_RLSD_DT"
	  **/
	 public abstract String getDvReleasedDate();
	 /**
	  * @ejb:interface-method view-type="local"
	  **/
	 public abstract void setDvReleasedDate(String DV_RLSD_DT);
    
    
	 /**
	  * @ejb:persistent-field
	  * @ejb:interface-method view-type="local"
	  *
	  * @jboss:column-name name="DV_DLVRD_DT"
	  **/
	 public abstract String getDvDeliveredDate();
	 /**
	  * @ejb:interface-method view-type="local"
	  **/
	 public abstract void setDvDeliveredDate(String DV_DLVRD_DT);
	 
	 
	 /**
	  * @ejb:persistent-field
	  * @ejb:interface-method view-type="local"
	  *
	  * @jboss:column-name name="DV_DLVRD_TO"
	  **/
	 public abstract String getDvDeliveredTo();
	 /**
	  * @ejb:interface-method view-type="local"
	  **/
	 public abstract void setDvDeliveredTo(String DV_DLVRD_TO);
	 
	 
	 /**
	  * @ejb:persistent-field
	  * @ejb:interface-method view-type="local"
	  *
	  * @jboss:column-name name="DV_PLT_NO"
	  **/
	 public abstract String getDvPlateNo();
	 /**
	  * @ejb:interface-method view-type="local"
	  **/
	 public abstract void setDvPlateNo(String DV_PLT_NO);
	 
	 /**
	  * @ejb:persistent-field
	  * @ejb:interface-method view-type="local"
	  *
	  * @jboss:column-name name="DV_DRVR_NM"
	  **/
	 public abstract String getDvDriverName();
	 /**
	  * @ejb:interface-method view-type="local"
	  **/
	 public abstract void setDvDriverName(String DV_DRVR_NM);
	 
	 
	 /**
	  * @ejb:persistent-field
	  * @ejb:interface-method view-type="local"
	  *
	  * @jboss:column-name name="DV_EMPTY_RTRN_DT"
	  **/
	 public abstract String getDvEmptyReturnDate();
	 /**
	  * @ejb:interface-method view-type="local"
	  **/
	 public abstract void setDvEmptyReturnDate(String DV_EMPTY_RTRN_DT);
    
    
	 
	 /**
	  * @ejb:persistent-field
	  * @ejb:interface-method view-type="local"
	  *
	  * @jboss:column-name name="DV_EMPTY_RTRN_TO"
	  **/
	 public abstract String getDvEmptyReturnTo();
	 /**
	  * @ejb:interface-method view-type="local"
	  **/
	 public abstract void setDvEmptyReturnTo(String DV_EMPTY_RTRN_TO);
	 
	 
	 /**
	  * @ejb:persistent-field
	  * @ejb:interface-method view-type="local"
	  *
	  * @jboss:column-name name="DV_TBS_FEE_RTRN"
	  **/
	 public abstract String getDvTabsFeeReturn();
	 /**
	  * @ejb:interface-method view-type="local"
	  **/
	 public abstract void setDvTabsFeeReturn(String DV_TBS_FEE_RTRN);
	 
	 
	 /**
	  * @ejb:persistent-field
	  * @ejb:interface-method view-type="local"
	  *
	  * @jboss:column-name name="DV_STTS"
	  **/
	 public abstract String getDvStatus();
	 /**
	  * @ejb:interface-method view-type="local"
	  **/
	 public abstract void setDvStatus(String DV_STTS);
	 
	 

	 /**
	  * @ejb:persistent-field
	  * @ejb:interface-method view-type="local"
	  *
	  * @jboss:column-name name="DV_RMRKS"
	  **/
	 public abstract String getDvRemarks();
	 /**
	  * @ejb:interface-method view-type="local"
	  **/
	 public abstract void setDvRemarks(String DV_RMRKS);
	 
 
	/**
	 * @ejb:home-method view-type="local"
	 */
	public Collection ejbHomeGetDvByCriteria(java.lang.String jbossQl, java.lang.Object[] args)
	throws FinderException {

		return ejbSelectGeneric(jbossQl, args);
	}
 
     
	 // RELATIONSHIP FIELDS

	 /**
      * @ejb:interface-method view-type="local"
      * @ejb:relation name="salesorder-deliveries"
      *               role-name="delivery-has-one-salesorder"
      *
      * @jboss:relation related-pk-field="soCode"
      *                 fk-column="AR_SALES_ORDER"
      */
     public abstract LocalArSalesOrder getArSalesOrder();
     /**
	  * @ejb:interface-method view-type="local"
	  **/
     public abstract void setArSalesOrder(LocalArSalesOrder arSalesOrder);

    
     /**
      * @jboss:dynamic-ql
      */
      public abstract Collection ejbSelectGeneric(java.lang.String jbossQl, java.lang.Object[] args)
         throws FinderException;

    

      
      // ENTITY METHODS

      
      
      /**
       * @ejb:create-method view-type="local"
       **/
       public Integer ejbCreate(Integer DV_CODE, String DV_DLVRY_NMBR, String DV_CNTNR, String DV_BKNG_TM, String DV_TBS_FEE_PLL_OUT, 
    		   String DV_RLSD_DT, String DV_DLVRD_DT, String DV_DLVRD_TO , String DV_PLT_NO , String DV_DRVR_NM , 
    		   String DV_EMPTY_RTRN_DT, String DV_EMPTY_RTRN_TO, String DV_TBS_FEE_RTRN, String DV_STTS,String DV_RMRKS) 
    		throws CreateException {
    	   
    	   setDvCode(DV_CODE);
    	   setDvDeliveryNumber(DV_DLVRY_NMBR);
    	   setDvContainer(DV_CNTNR);
    	   setDvBookingTime(DV_BKNG_TM);
    	   setDvTabsFeePullOut(DV_TBS_FEE_PLL_OUT);
    	   setDvReleasedDate(DV_RLSD_DT);
    	   setDvDeliveredDate(DV_DLVRD_DT);
    	   setDvDeliveredTo(DV_DLVRD_TO);
    	   setDvPlateNo(DV_PLT_NO);
    	   setDvDriverName(DV_DRVR_NM);
    	   setDvEmptyReturnDate(DV_EMPTY_RTRN_DT);
    	   setDvEmptyReturnTo(DV_EMPTY_RTRN_TO);
    	   setDvTabsFeeReturn(DV_TBS_FEE_RTRN);
    	   setDvStatus(DV_STTS);
    	   setDvRemarks(DV_RMRKS);
    	   
    	   return null;
    	   
    	   
       }
      
      
       /**
        * @ejb:create-method view-type="local"
        **/
        public Integer ejbCreate(String DV_CNTNR, String DV_DLVRY_NMBR, String DV_BKNG_TM, String DV_TBS_FEE_PLL_OUT, 
        		String DV_RLSD_DT, String DV_DLVRD_DT, String DV_DLVRD_TO , String DV_PLT_NO , String DV_DRVR_NM , 
        		String DV_EMPTY_RTRN_DT, String DV_EMPTY_RTRN_TO, String DV_TBS_FEE_RTRN, String DV_STTS,String DV_RMRKS) 
     		throws CreateException {
     	   
     	
     	   setDvContainer(DV_CNTNR);
     	  setDvDeliveryNumber(DV_DLVRY_NMBR);
     	   setDvBookingTime(DV_BKNG_TM);
     	   setDvTabsFeePullOut(DV_TBS_FEE_PLL_OUT);
     	   setDvReleasedDate(DV_RLSD_DT);
     	   setDvDeliveredDate(DV_DLVRD_DT);
     	   setDvDeliveredTo(DV_DLVRD_TO);
     	   setDvPlateNo(DV_PLT_NO);
     	   setDvDriverName(DV_DRVR_NM);
     	   setDvEmptyReturnDate(DV_EMPTY_RTRN_DT);
     	   setDvEmptyReturnTo(DV_EMPTY_RTRN_TO);
     	   setDvTabsFeeReturn(DV_TBS_FEE_RTRN);
     	   setDvStatus(DV_STTS);
     	   setDvRemarks(DV_RMRKS);
     	   
     	   return null;
     	   
     	   
        }
      
      
      
        public void ejbPostCreate(Integer DV_CODE,  String DV_DLVRY_NMBR, String DV_CNTNR, String DV_BKNG_TM, String DV_TBS_FEE_PLL_OUT, 
        		String DV_RLSD_DT, String DV_DLVRD_DT, String DV_DLVRD_TO , String DV_PLT_NO , String DV_DRVR_NM , 
        		String DV_EMPTY_RTRN_DT, String DV_EMPTY_RTRN_TO, String DV_TBS_FEE_RTRN, String DV_STTS,String DV_RMRKS) 
      		throws CreateException { }

        public void ejbPostCreate(String DV_CNTNR,  String DV_DLVRY_NMBR, String DV_BKNG_TM, String DV_TBS_FEE_PLL_OUT, 
        		String DV_RLSD_DT, String DV_DLVRD_DT, String DV_DLVRD_TO , String DV_PLT_NO , String DV_DRVR_NM , 
        		String DV_EMPTY_RTRN_DT, String DV_EMPTY_RTRN_TO, String DV_TBS_FEE_RTRN, String DV_STTS,String DV_RMRKS) 
       		throws CreateException { }
}
