/*
 * com/ejb/ar/LocalArDistributionRecordBean.java
 *
 * Created on March 3, 2003, 4:59 PM
 */

package com.ejb.ar;

import java.util.Collection;

import javax.ejb.CreateException;
import javax.ejb.FinderException;

import com.util.AbstractEntityBean;
import com.util.Debug;

/**
 *
 * @author  Neil Andrew M. Ajero
 *
 * @ejb:bean name="ArDistributionRecordEJB"
 *           display-name="DistributionRecord Entity"
 *           type="CMP"
 *           cmp-version="2.x"
 *           view-type="local"
 *           local-jndi-name="omega-ejb/ArDistributionRecordEJB"
 *           schema="ArDistributionRecord"
 *           primkey-field="drCode"
 *
 * @ejb:pk class="java.lang.Integer"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="aruser"
 *                        role-link="aruserlink"
 *
 * @ejb:permission role-name="aruser"
 *
 * @ejb:interface local-class="com.ejb.ar.LocalArDistributionRecord"
 *                local-extends="javax.ejb.EJBLocalObject"
 *
 * @ejb:home local-class="com.ejb.ar.LocalArDistributionRecordHome"
 *           local-extends="javax.ejb.EJBLocalHome"
 *
 * @ejb:finder signature="LocalArDistributionRecord findByDrClassAndInvCode(java.lang.String DR_CLSS, java.lang.Integer INV_CODE, java.lang.Integer DR_AD_CMPNY)"
 *             query="SELECT OBJECT(dr) FROM ArInvoice inv, IN(inv.arDistributionRecords) dr WHERE dr.drClass=?1 AND inv.invCode=?2 AND dr.drAdCompany = ?3" 
 *
 * * @ejb:finder signature="LocalArDistributionRecord findByDrClassAndRctCode(java.lang.String DR_CLSS, java.lang.Integer CHK_CODE, java.lang.Integer DR_AD_CMPNY)"
 *             query="SELECT OBJECT(dr) FROM ArReceipt rct, IN(rct.arDistributionRecords) dr WHERE dr.drClass=?1 AND rct.rctCode=?2 AND dr.drAdCompany = ?3" 
 *
 *
 * @ejb:finder signature="Collection findDrsByDrClassAndInvCode(java.lang.String DR_CLSS, java.lang.Integer INV_CODE, java.lang.Integer DR_AD_CMPNY)"
 *             query="SELECT OBJECT(dr) FROM ArInvoice inv, IN(inv.arDistributionRecords) dr WHERE dr.drClass=?1 AND inv.invCode=?2 AND dr.drAdCompany = ?3"
 *
 * @jboss:query signature="Collection findDrsByDrClassAndInvCode(java.lang.String DR_CLSS, java.lang.Integer INV_CODE, java.lang.Integer DR_AD_CMPNY)"
 *             query="SELECT OBJECT(dr) FROM ArInvoice inv, IN(inv.arDistributionRecords) dr WHERE dr.drClass=?1 AND inv.invCode=?2 AND dr.drAdCompany = ?3 ORDER BY dr.drLine"
 *
 * @ejb:finder signature="Collection findDrsByDrClassAndRctCode(java.lang.String DR_CLSS, java.lang.Integer CHK_CODE, java.lang.Integer DR_AD_CMPNY)"
 *             query="SELECT OBJECT(dr) FROM ArReceipt rct, IN(rct.arDistributionRecords) dr WHERE dr.drClass=?1 AND rct.rctCode=?2 AND dr.drAdCompany = ?3"
 *
 * @jboss:query signature="Collection findDrsByDrClassAndRctCode(java.lang.String DR_CLSS, java.lang.Integer CHK_CODE, java.lang.Integer DR_AD_CMPNY)"
 *             query="SELECT OBJECT(dr) FROM ArReceipt rct, IN(rct.arDistributionRecords) dr WHERE dr.drClass=?1 AND rct.rctCode=?2 AND dr.drAdCompany = ?3 ORDER BY dr.drLine"
 * 
 * @ejb:finder signature="Collection findDrsByDrClassAndAiCode(java.lang.String DR_CLSS, java.lang.Integer AI_CODE, java.lang.Integer DR_AD_CMPNY)"
 *             query="SELECT OBJECT(dr) FROM ArAppliedInvoice ai, IN(ai.arDistributionRecords) dr WHERE dr.drClass=?1 AND ai.aiCode=?2 AND dr.drAdCompany = ?3"
 * 
 * @jboss:query signature="Collection findDrsByDrClassAndAiCode(java.lang.String DR_CLSS, java.lang.Integer AI_CODE, java.lang.Integer DR_AD_CMPNY)"
 *             query="SELECT OBJECT(dr) FROM ArAppliedInvoice ai, IN(ai.arDistributionRecords) dr WHERE dr.drClass=?1 AND ai.aiCode=?2 AND dr.drAdCompany = ?3 ORDER BY dr.drLine"
 * 
 * @ejb:finder signature="Collection findImportableDrByInvCode(java.lang.Integer INV_CODE, java.lang.Integer DR_AD_CMPNY)"
 *             query="SELECT OBJECT(dr) FROM ArInvoice inv, IN(inv.arDistributionRecords) dr WHERE dr.drImported = 0 AND inv.invCode = ?1 AND dr.drAdCompany = ?2"
 *
 * @jboss:query signature="Collection findImportableDrByInvCode(java.lang.Integer INV_CODE, java.lang.Integer DR_AD_CMPNY)"
 *             query="SELECT OBJECT(dr) FROM ArInvoice inv, IN(inv.arDistributionRecords) dr WHERE dr.drImported = 0 AND inv.invCode = ?1 AND dr.drAdCompany = ?2 ORDER BY dr.drLine"
 *
 * @ejb:finder signature="Collection findImportableDrByDrReversedAndRctCode(byte DR_RVRSD, java.lang.Integer RCT_CODE, java.lang.Integer DR_AD_CMPNY)"
 *             query="SELECT OBJECT(dr) FROM ArReceipt rct, IN(rct.arDistributionRecords) dr WHERE dr.drImported = 0 AND dr.drReversed = ?1 AND rct.rctCode = ?2 AND dr.drAdCompany = ?3"
 *
 * @jboss:query signature="Collection findImportableDrByDrReversedAndRctCode(byte DR_RVRSD, java.lang.Integer RCT_CODE, java.lang.Integer DR_AD_CMPNY)"
 *             query="SELECT OBJECT(dr) FROM ArReceipt rct, IN(rct.arDistributionRecords) dr WHERE dr.drImported = 0 AND dr.drReversed = ?1 AND rct.rctCode = ?2 AND dr.drAdCompany = ?3 ORDER BY dr.drLine"
 *
 * @ejb:finder signature="Collection findByDrClassAndInvCmInvoiceNumber(java.lang.String DR_CLSS, java.lang.String INV_CM_INVC_NMBR, java.lang.Integer DR_AD_CMPNY)"
 *             query="SELECT OBJECT(dr) FROM ArInvoice inv, IN(inv.arDistributionRecords) dr WHERE dr.drClass=?1 AND inv.invCmInvoiceNumber = ?2 AND dr.drAdCompany = ?3"
 *
 * @jboss:query signature="Collection findByDrClassAndInvCmInvoiceNumber(java.lang.String DR_CLSS, java.lang.String INV_CM_INVC_NMBR, java.lang.Integer DR_AD_CMPNY)"
 *             query="SELECT OBJECT(dr) FROM ArInvoice inv, IN(inv.arDistributionRecords) dr WHERE dr.drClass=?1 AND inv.invCmInvoiceNumber = ?2 AND dr.drAdCompany = ?3 ORDER BY dr.drLine"
 *
 * @ejb:finder signature="Collection findByInvCode(java.lang.Integer INV_CODE, java.lang.Integer DR_AD_CMPNY)"
 *             query="SELECT OBJECT(dr) FROM ArInvoice inv, IN(inv.arDistributionRecords) dr WHERE inv.invCode =?1 AND dr.drAdCompany=?2"
 *
 * @jboss:query signature="Collection findByInvCode(java.lang.Integer INV_CODE, java.lang.Integer DR_AD_CMPNY)"
 *             query="SELECT OBJECT(dr) FROM ArInvoice inv, IN(inv.arDistributionRecords) dr WHERE inv.invCode =?1 AND dr.drAdCompany=?2 ORDER BY dr.drDebit DESC"
 *
 * @ejb:finder signature="Collection findByRctCode(java.lang.Integer INV_CODE, java.lang.Integer DR_AD_CMPNY)"
 *             query="SELECT OBJECT(dr) FROM ArReceipt rct, IN(rct.arDistributionRecords) dr WHERE rct.rctCode =?1 AND dr.drAdCompany=?2"
 *
 * @jboss:query signature="Collection findByRctCode(java.lang.Integer INV_CODE, java.lang.Integer DR_AD_CMPNY)"
 *             query="SELECT OBJECT(dr) FROM ArReceipt rct, IN(rct.arDistributionRecords) dr WHERE rct.rctCode =?1 AND dr.drAdCompany=?2 ORDER BY dr.drDebit DESC"
 * 
 * @ejb:finder signature="Collection findInvByDateRangeAndCoaAccountNumber(byte INV_CRDT_MMO, java.util.Date INV_DT_FRM, java.util.Date INV_DT_TO, java.lang.Integer COA_CODE, java.lang.Integer DR_AD_CMPNY)"
 *             query="SELECT OBJECT(dr) FROM ArInvoice inv, IN(inv.arDistributionRecords) dr WHERE inv.invPosted=1 AND inv.invCreditMemo=?1 AND inv.invDate>=?2 AND inv.invDate<=?3 AND dr.glChartOfAccount.coaCode=?4 AND dr.drAdCompany=?5"
 *
 * @jboss:query signature="Collection findInvByDateRangeAndCoaAccountNumber(byte INV_CRDT_MMO, java.util.Date INV_DT_FRM, java.util.Date INV_DT_TO, java.lang.Integer COA_CODE, java.lang.Integer DR_AD_CMPNY)"
 *             query="SELECT OBJECT(dr) FROM ArInvoice inv, IN(inv.arDistributionRecords) dr WHERE inv.invPosted=1 AND inv.invCreditMemo=?1 AND inv.invDate>=?2 AND inv.invDate<=?3 AND dr.glChartOfAccount.coaCode=?4 AND dr.drAdCompany=?5 ORDER BY dr.drLine"
 *
 ** @ejb:finder signature="Collection findRctByDateRangeAndCoaAccountNumber(java.util.Date RCT_DT_FRM, java.util.Date RCT_DT_TO, java.lang.Integer COA_CODE, java.lang.Integer DR_AD_CMPNY)"
 *             query="SELECT OBJECT(dr) FROM ArReceipt rct, IN(rct.arDistributionRecords) dr WHERE rct.rctPosted=1 AND rct.rctDate>=?1 AND rct.rctDate<=?2 AND dr.glChartOfAccount.coaCode=?3 AND dr.drAdCompany=?4"
 *
 * @jboss:query signature="Collection findRctByDateRangeAndCoaAccountNumber(java.util.Date RCT_DT_FRM, java.util.Date RCT_DT_TO, java.lang.Integer COA_CODE, java.lang.Integer DR_AD_CMPNY)"
 *             query="SELECT OBJECT(dr) FROM ArReceipt rct, IN(rct.arDistributionRecords) dr WHERE rct.rctPosted=1 AND rct.rctDate>=?1 AND rct.rctDate<=?2 AND dr.glChartOfAccount.coaCode=?3 AND dr.drAdCompany=?4 ORDER BY dr.drLine"
 *
 * @ejb:finder signature="Collection findUnpostedInvByDateRangeAndCoaAccountNumber(byte INV_CRDT_MMO, java.util.Date INV_DT_FRM, java.util.Date INV_DT_TO, java.lang.Integer COA_CODE, java.lang.Integer INV_AD_BRNCH, java.lang.Integer DR_AD_CMPNY)"
 *             query="SELECT OBJECT(dr) FROM ArInvoice inv, IN(inv.arDistributionRecords) dr WHERE inv.invVoid=0 AND inv.invPosted=0 AND inv.invCreditMemo=?1 AND inv.invDate>=?2 AND inv.invDate<=?3 AND dr.glChartOfAccount.coaCode=?4 AND inv.invAdBranch=?5 AND dr.drAdCompany=?6"
 *
 * @jboss:query signature="Collection findUnpostedInvByDateRangeAndCoaAccountNumber(byte INV_CRDT_MMO, java.util.Date INV_DT_FRM, java.util.Date INV_DT_TO, java.lang.Integer COA_CODE, java.lang.Integer INV_AD_BRNCH, java.lang.Integer DR_AD_CMPNY)"
 *             query="SELECT OBJECT(dr) FROM ArInvoice inv, IN(inv.arDistributionRecords) dr WHERE inv.invVoid=0 AND inv.invPosted=0 AND inv.invCreditMemo=?1 AND inv.invDate>=?2 AND inv.invDate<=?3 AND dr.glChartOfAccount.coaCode=?4 AND inv.invAdBranch=?5 AND dr.drAdCompany=?6 ORDER BY dr.drLine"
 *
 * @ejb:finder signature="Collection findUnpostedRctByDateRangeAndCoaAccountNumber(java.util.Date RCT_DT_FRM, java.util.Date RCT_DT_TO, java.lang.Integer COA_CODE, java.lang.Integer RCT_AD_BRNCH, java.lang.Integer DR_AD_CMPNY)"
 *             query="SELECT OBJECT(dr) FROM ArReceipt rct, IN(rct.arDistributionRecords) dr WHERE rct.rctVoid=0 AND rct.rctPosted=0 AND rct.rctDate>=?1 AND rct.rctDate<=?2 AND dr.glChartOfAccount.coaCode=?3 AND rct.rctAdBranch=?4 AND dr.drAdCompany=?5"
 *
 * @jboss:query signature="Collection findUnpostedRctByDateRangeAndCoaAccountNumber(java.util.Date RCT_DT_FRM, java.util.Date RCT_DT_TO, java.lang.Integer COA_CODE, java.lang.Integer RCT_AD_BRNCH, java.lang.Integer DR_AD_CMPNY)"
 *             query="SELECT OBJECT(dr) FROM ArReceipt rct, IN(rct.arDistributionRecords) dr WHERE rct.rctVoid=0 AND rct.rctPosted=0 AND rct.rctDate>=?1 AND rct.rctDate<=?2 AND dr.glChartOfAccount.coaCode=?3 AND rct.rctAdBranch=?4 AND dr.drAdCompany=?5 ORDER BY dr.drLine"
 * 
 * @ejb:finder signature="Collection findUnpostedInvByDrCoaAccountNumberAndBrCode(java.lang.Integer COA_CODE, java.lang.Integer INV_AD_BRNCH, java.lang.Integer DR_AD_CMPNY)"
 *             query="SELECT OBJECT(dr) FROM ArInvoice inv, IN(inv.arDistributionRecords) dr WHERE inv.invPosted=0 AND dr.glChartOfAccount.coaCode=?1 AND inv.invAdBranch = ?2 AND dr.drAdCompany = ?3"
 *
 * @ejb:finder signature="Collection findUnpostedRctByDrCoaAccountNumberAndBrCode(java.lang.Integer COA_CODE, java.lang.Integer RCT_AD_BRNCH, java.lang.Integer DR_AD_CMPNY)"
 *             query="SELECT OBJECT(dr) FROM ArReceipt rct, IN(rct.arDistributionRecords) dr WHERE rct.rctPosted=0 AND dr.glChartOfAccount.coaCode=?1 AND rct.rctAdBranch = ?2 AND dr.drAdCompany = ?3"
 *
 * @ejb:finder signature="Collection findInvByDateAndCoaAccountNumberAndCurrencyAndInvPosted(java.util.Date INV_DT, java.lang.Integer COA_CODE, java.lang.Integer FC_CODE, byte INV_PSTD, java.lang.Integer DR_AD_CMPNY)"
 *             query="SELECT OBJECT(dr) FROM ArInvoice inv, IN(inv.arDistributionRecords) dr WHERE inv.invCreditMemo=0 AND inv.invDate<=?1 AND dr.glChartOfAccount.coaCode=?2 AND inv.glFunctionalCurrency.fcCode=?3 AND inv.invPosted=?4 AND inv.invVoid=0 AND dr.drAdCompany=?5"
 *             
 * @jboss:query signature="Collection findInvByDateAndCoaAccountNumberAndCurrencyAndInvPosted(java.util.Date INV_DT, java.lang.Integer COA_CODE, java.lang.Integer FC_CODE, byte INV_PSTD, java.lang.Integer DR_AD_CMPNY)"
 *             query="SELECT OBJECT(dr) FROM ArInvoice inv, IN(inv.arDistributionRecords) dr WHERE inv.invCreditMemo=0 AND inv.invDate<=?1 AND dr.glChartOfAccount.coaCode=?2 AND inv.glFunctionalCurrency.fcCode=?3 AND inv.invPosted=?4 AND inv.invVoid=0 AND dr.drAdCompany=?5 ORDER BY inv.invDate, dr.drLine"
 * 
 * @ejb:finder signature="Collection findInvCreditMemoByDateAndCoaAccountNumberAndCurrencyAndInvPosted(java.util.Date INV_DT, java.lang.Integer COA_CODE, java.lang.Integer FC_CODE, byte INV_PSTD, java.lang.Integer DR_AD_CMPNY)"
 *             query="SELECT OBJECT(dr) FROM ArInvoice cm, IN(cm.arDistributionRecords) dr, ArInvoice inv WHERE cm.invCreditMemo=1 AND cm.invCmInvoiceNumber=inv.invNumber AND cm.invDate<=?1 AND dr.glChartOfAccount.coaCode=?2 AND inv.glFunctionalCurrency.fcCode=?3 AND cm.invPosted=?4 AND cm.invVoid=0 AND dr.drAdCompany=?5"
 *             
 * @jboss:query signature="Collection findInvCreditMemoByDateAndCoaAccountNumberAndCurrencyAndInvPosted(java.util.Date INV_DT, java.lang.Integer COA_CODE, java.lang.Integer FC_CODE, byte INV_PSTD, java.lang.Integer DR_AD_CMPNY)"
 *             query="SELECT OBJECT(dr) FROM ArInvoice cm, IN(cm.arDistributionRecords) dr, ArInvoice inv WHERE cm.invCreditMemo=1 AND cm.invCmInvoiceNumber=inv.invNumber AND cm.invDate<=?1 AND dr.glChartOfAccount.coaCode=?2 AND inv.glFunctionalCurrency.fcCode=?3 AND cm.invPosted=?4 AND cm.invVoid=0 AND dr.drAdCompany=?5 ORDER BY cm.invDate, dr.drLine"
 * 
 * @ejb:finder signature="Collection findRctByDateAndCoaAccountNumberAndCurrencyAndRctPosted(java.util.Date RCT_DT, java.lang.Integer COA_CODE, java.lang.Integer FC_CODE, byte RCT_PSTD, java.lang.Integer DR_AD_CMPNY)"
 *             query="SELECT OBJECT(dr) FROM ArReceipt rct, IN(rct.arDistributionRecords) dr, IN(rct.arAppliedInvoices) ai WHERE rct.rctType='COLLECTION' AND rct.rctDate<=?1 AND dr.glChartOfAccount.coaCode=?2 AND rct.glFunctionalCurrency.fcCode=?3 AND rct.rctPosted=?4 AND rct.rctVoid=0 AND dr.drAdCompany=?5"
 *             
 * @jboss:query signature="Collection findRctByDateAndCoaAccountNumberAndCurrencyAndRctPosted(java.util.Date RCT_DT, java.lang.Integer COA_CODE, java.lang.Integer FC_CODE, byte RCT_PSTD, java.lang.Integer DR_AD_CMPNY)"
 *             query="SELECT OBJECT(dr) FROM ArReceipt rct, IN(rct.arDistributionRecords) dr, IN(rct.arAppliedInvoices) ai WHERE rct.rctType='COLLECTION' AND rct.rctDate<=?1 AND dr.glChartOfAccount.coaCode=?2 AND rct.glFunctionalCurrency.fcCode=?3 AND rct.rctPosted=?4 AND rct.rctVoid=0 AND dr.drAdCompany=?5 ORDER BY rct.rctDate, dr.drLine"
 * 
 * @ejb:finder signature="Collection findRctMiscByDateAndCoaAccountNumberAndCurrencyAndRctPosted(java.util.Date RCT_DT, java.lang.Integer COA_CODE, java.lang.Integer FC_CODE, byte RCT_PSTD, java.lang.Integer DR_AD_CMPNY)"
 *             query="SELECT OBJECT(dr) FROM ArReceipt rct, IN(rct.arDistributionRecords) dr WHERE rct.rctType='MISC' AND rct.rctDate<=?1 AND dr.glChartOfAccount.coaCode=?2 AND rct.glFunctionalCurrency.fcCode=?3 AND rct.rctPosted=?4 AND rct.rctVoid=0 AND dr.drAdCompany=?5"
 *             
 * @jboss:query signature="Collection findRctMiscByDateAndCoaAccountNumberAndCurrencyAndRctPosted(java.util.Date RCT_DT, java.lang.Integer COA_CODE, java.lang.Integer FC_CODE, byte RCT_PSTD, java.lang.Integer DR_AD_CMPNY)"
 *             query="SELECT OBJECT(dr) FROM ArReceipt rct, IN(rct.arDistributionRecords) dr WHERE rct.rctType='MISC' AND rct.rctDate<=?1 AND dr.glChartOfAccount.coaCode=?2 AND rct.glFunctionalCurrency.fcCode=?3 AND rct.rctPosted=?4 AND rct.rctVoid=0 AND dr.drAdCompany=?5 ORDER BY rct.rctDate, dr.drLine"
 * 
 * @ejb:finder signature="Collection findUnpostedInvByDateAndCoaAccountNumber(byte INV_CRDT_MMO, java.util.Date INV_DT_TO, java.lang.Integer COA_CODE, java.lang.Integer INV_AD_BRNCH, java.lang.Integer DR_AD_CMPNY)"
 *             query="SELECT OBJECT(dr) FROM ArInvoice inv, IN(inv.arDistributionRecords) dr WHERE inv.invVoid=0 AND inv.invPosted=0 AND inv.invCreditMemo=?1 AND inv.invDate<=?2 AND dr.glChartOfAccount.coaCode=?3 AND inv.invAdBranch=?4 AND dr.drAdCompany=?5"
 *
 * @jboss:query signature="Collection findUnpostedInvByDateAndCoaAccountNumber(byte INV_CRDT_MMO, java.util.Date INV_DT_TO, java.lang.Integer COA_CODE, java.lang.Integer INV_AD_BRNCH, java.lang.Integer DR_AD_CMPNY)"
 *             query="SELECT OBJECT(dr) FROM ArInvoice inv, IN(inv.arDistributionRecords) dr WHERE inv.invVoid=0 AND inv.invPosted=0 AND inv.invCreditMemo=?1 AND inv.invDate<=?2 AND dr.glChartOfAccount.coaCode=?3 AND inv.invAdBranch=?4 AND dr.drAdCompany=?5 ORDER BY dr.drLine"
 *
 * @ejb:finder signature="Collection findUnpostedRctByDateAndCoaAccountNumber(java.util.Date RCT_DT_TO, java.lang.Integer COA_CODE, java.lang.Integer RCT_AD_BRNCH, java.lang.Integer DR_AD_CMPNY)"
 *             query="SELECT OBJECT(dr) FROM ArReceipt rct, IN(rct.arDistributionRecords) dr WHERE rct.rctVoid=0 AND rct.rctPosted=0 AND rct.rctDate<=?1 AND dr.glChartOfAccount.coaCode=?2 AND rct.rctAdBranch=?3 AND dr.drAdCompany=?4"
 *
 * @jboss:query signature="Collection findUnpostedRctByDateAndCoaAccountNumber(java.util.Date RCT_DT_TO, java.lang.Integer COA_CODE, java.lang.Integer RCT_AD_BRNCH, java.lang.Integer DR_AD_CMPNY)"
 *             query="SELECT OBJECT(dr) FROM ArReceipt rct, IN(rct.arDistributionRecords) dr WHERE rct.rctVoid=0 AND rct.rctPosted=0 AND rct.rctDate<=?1 AND dr.glChartOfAccount.coaCode=?2 AND rct.rctAdBranch=?3 AND dr.drAdCompany=?4 ORDER BY dr.drLine"
 *
 * @jboss:query signature="Collection ejbSelectGeneric(java.lang.String jbossQl, java.lang.Object[] args)"
 *                         dynamic="true"
 *
 *
 *
 * @ejb:value-object match="*"
 *             name="ArDistributionRecord"
 *
 * @jboss:persistence table-name="AR_DSTRBTN_RCRD"
 *
 * @jboss:entity-command name="mysql-get-generated-keys"
 */

public abstract class ArDistributionRecordBean extends AbstractEntityBean {
    
    
    // PERSISTENT METHODS
    
    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     * @ejb:pk-field
     *
     * @jboss:column-name name="DR_CODE"
     *
     * @jboss:persistence auto-increment="true"
     **/
    public abstract Integer getDrCode();
    public abstract void setDrCode(Integer DR_CODE);
    
    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="DR_LN"
     **/
    public abstract short getDrLine();
    public abstract void setDrLine(short DR_LN);
    
    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="DR_CLSS"
     **/
    public abstract String getDrClass();
    public abstract void setDrClass(String DR_CLSS);
    
    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="DR_DBT"
     **/
    public abstract byte getDrDebit();
    public abstract void setDrDebit(byte DR_DBT);

    
    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="DR_AMNT"
     **/
    public abstract double getDrAmount();
    /**
     * @ejb:interface-method view-type="local"
     **/
    public abstract void setDrAmount(double DR_AMNT);
    
    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="DR_IMPRTD"
     **/
    public abstract byte getDrImported();
     /**
      * @ejb:interface-method view-type="local"
      **/
    public abstract void setDrImported(byte DR_IMPRTD);
    
    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="DR_RVRSD"
     **/
    public abstract byte getDrReversed();
     /**
      * @ejb:interface-method view-type="local"
      **/    
    public abstract void setDrReversed(byte DR_RVRSD);   
    
    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="DR_SC_ACCNT"
     **/
    public abstract Integer getDrScAccount();
     /**
      * @ejb:interface-method view-type="local"
      **/    
    public abstract void setDrScAccount(Integer DR_SC_ACCNT);   
    
    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="DR_AD_CMPNY"
     **/
    public abstract Integer getDrAdCompany();
     /**
      * @ejb:interface-method view-type="local"
      **/    
    public abstract void setDrAdCompany(Integer DR_AD_CMPNY); 
    
    // RELATIONSHIP METHODS

    /**
     * @ejb:interface-method view-type="local"
     * @ejb:relation name="invoice-ardistributionrecords"
     *               role-name="ardistributionrecord-has-one-invoice"
     *               cascade-delete="yes"
     * 
     * @jboss:relation related-pk-field="invCode"
     *                 fk-column="AR_INVOICE"
     */
    public abstract LocalArInvoice getArInvoice();
    /**
	 * @ejb:interface-method view-type="local"
	 **/
    public abstract void setArInvoice(LocalArInvoice arInvoice);

    /**
     * @ejb:interface-method view-type="local"
     * @ejb:relation name="receipt-ardistributionrecords"
     *               role-name="ardistributionrecord-has-one-receipt"
     *               cascade-delete="yes"
     * 
     * @jboss:relation related-pk-field="rctCode"
     *                 fk-column="AR_RECEIPT"
     */
    public abstract LocalArReceipt getArReceipt();
    /**
	 * @ejb:interface-method view-type="local"
	 **/	
    public abstract void setArReceipt(LocalArReceipt arReceipt);

    /**
     * @ejb:interface-method view-type="local"
     * @ejb:relation name="chartofaccount-ardistributionrecords"
     *               role-name="ardistributionrecord-has-one-chartofaccount"
     * 
     * @jboss:relation related-pk-field="coaCode"
     *                 fk-column="GL_CHART_OF_ACCOUNT"
     */
    public abstract com.ejb.gl.LocalGlChartOfAccount getGlChartOfAccount();
    /**
	 * @ejb:interface-method view-type="local"
	 **/
    public abstract void setGlChartOfAccount(com.ejb.gl.LocalGlChartOfAccount glChartOfAccount);
    
    /**
     * @ejb:interface-method view-type="local"
     * @ejb:relation name="appliedinvoice-ardistributionrecords"
     *               role-name="ardistributionrecord-has-one-appliedinvoice"
     * 
     * @jboss:relation related-pk-field="aiCode"
     *                 fk-column="AR_APPLIED_INVOICE"
     */
    public abstract LocalArAppliedInvoice getArAppliedInvoice();
    public abstract void setArAppliedInvoice(LocalArAppliedInvoice arAppliedInvoice); 
            
    
    // BUSINESS METHODS
    
    /**
	 * @jboss:dynamic-ql
	 */
	public abstract Collection ejbSelectGeneric(java.lang.String jbossQl, java.lang.Object[] args)
	throws FinderException;


    /**
	 * @ejb:home-method view-type="local"
	 */
	public Collection ejbHomeGetDrByCriteria(java.lang.String jbossQl, java.lang.Object[] args)
	throws FinderException {

		return ejbSelectGeneric(jbossQl, args);
	}

    
    // ENTITY METHODS
    
    /**
     * @ejb:create-method view-type="local"
     **/
     public Integer ejbCreate(Integer DR_CODE, 
         short DR_LN, String DR_CLSS, byte DR_DBT, 
         double DR_AMNT, byte DR_IMPRTD, byte DR_RVRSD,
		 Integer DR_AD_CMPNY)
         throws CreateException {
      	
         Debug.print("ArDiscountBean ejbCreate");
      
         setDrCode(DR_CODE);
         setDrLine(DR_LN);
         setDrClass(DR_CLSS);
         setDrDebit(DR_DBT);
         setDrAmount(DR_AMNT);         
         setDrImported(DR_IMPRTD);
         setDrReversed(DR_RVRSD);
         setDrAdCompany(DR_AD_CMPNY);
       
         return null;
     }
     
     /**
     * @ejb:create-method view-type="local"
     **/
     public Integer ejbCreate( 
         short DR_LN, String DR_CLSS, byte DR_DBT, 
         double DR_AMNT, byte DR_IMPRTD, byte DR_RVRSD,
		 Integer DR_AD_CMPNY)
         throws CreateException {
      	
         Debug.print("ArDiscountBean ejbCreate");
      
         setDrLine(DR_LN);
         setDrClass(DR_CLSS);
         setDrDebit(DR_DBT);
         setDrAmount(DR_AMNT);         
         setDrImported(DR_IMPRTD);
         setDrReversed(DR_RVRSD);
         setDrAdCompany(DR_AD_CMPNY);
        
         return null;
     }
     
     public void ejbPostCreate(Integer DR_CODE, 
         short DR_LN, String DR_CLSS, byte DR_DBT, 
         double DR_AMNT, byte DR_IMPRTD, byte DR_RVRSD,
		 Integer DR_AD_CMPNY)      
         throws CreateException { }
      
     public void ejbPostCreate(
         short DR_LN, String DR_CLSS, byte DR_DBT, 
         double DR_AMNT, byte DR_IMPRTD, byte DR_RVRSD,
		 Integer DR_AD_CMPNY)
         throws CreateException { } 
         
} 