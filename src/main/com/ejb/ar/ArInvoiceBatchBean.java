/*
 * com/ejb/ar/LocalArInvoiceBatchBean.java
 *
 * Created on May 20, 2004, 10:28 AM
 */
package com.ejb.ar;

import java.util.Collection;
import java.util.Date;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;

import com.util.AbstractEntityBean;
import com.util.Debug;

/**
 *
 * @author  Neil Andrew M.Ajero
 *
 * @ejb:bean name="ArInvoiceBatchEJB"
 *           display-name="Invoice Batch Entity"
 *           type="CMP"
 *           cmp-version="2.x"
 *           view-type="local"
 *           local-jndi-name="omega-ejb/ArInvoiceBatchEJB"
 *           schema="ArInvoiceBatch"
 *           primkey-field="ibCode"
 *       
 * @ejb:pk class="Integer"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="aruser"
 *                        role-link="aruserlink"
 *
 * @ejb:permission role-name="aruser"
 *
 * @ejb:interface local-class="com.ejb.ar.LocalArInvoiceBatch"
 *                local-extends="javax.ejb.EJBLocalObject"
 *
 * @ejb:home local-class="com.ejb.ar.LocalArInvoiceBatchHome"
 *           local-extends="javax.ejb.EJBLocalHome"
 *
 * @ejb:finder signature="LocalArInvoiceBatch findByIbName(java.lang.String IB_NM, java.lang.Integer IB_AD_BRNCH, java.lang.Integer IB_AD_CMPNY)"
 *             query="SELECT OBJECT(ib) FROM ArInvoiceBatch ib WHERE ib.ibName=?1 AND ib.ibAdBranch = ?2 AND ib.ibAdCompany=?3"
 * 
 * @ejb:finder signature="Collection findOpenIbByIbType(java.lang.String IB_TYP, java.lang.Integer IB_AD_BRNCH, java.lang.Integer IB_AD_CMPNY)"
 *             query="SELECT OBJECT(ib) FROM ArInvoiceBatch ib WHERE ib.ibType=?1 AND ib.ibStatus='OPEN' AND ib.ibAdBranch=?2 AND ib.ibAdCompany=?3"
 *
 * @jboss:query signature="Collection findOpenIbByIbType(java.lang.String IB_TYP, java.lang.Integer IB_AD_BRNCH, java.lang.Integer IB_AD_CMPNY)"
 *             query="SELECT OBJECT(ib) FROM ArInvoiceBatch ib WHERE ib.ibType=?1 AND ib.ibStatus='OPEN' AND ib.ibAdBranch=?2 AND ib.ibAdCompany=?3 ORDER BY ib.ibName"
 *
 * @ejb:finder signature="Collection findOpenIbAll(java.lang.Integer AD_BRNCH, java.lang.Integer IB_AD_CMPNY)"
 *             query="SELECT OBJECT(ib) FROM ArInvoiceBatch ib WHERE ib.ibStatus='OPEN' AND ib.ibAdBranch=?1 AND ib.ibAdCompany=?2"
 * 
 * @jboss:query signature="Collection findOpenIbAll(java.lang.Integer AD_BRNCH, java.lang.Integer IB_AD_CMPNY)"
 *             query="SELECT OBJECT(ib) FROM ArInvoiceBatch ib WHERE ib.ibStatus='OPEN' AND ib.ibAdBranch=?1 AND ib.ibAdCompany=?2 ORDER BY ib.ibName"
 *
 * @ejb:select signature="Collection ejbSelectGeneric(java.lang.String jbossQl, java.lang.Object[] args)"
 *             result-type-mapping="Local"
 *             method-intf="LocalHome"
 *             query="SELECT OBJECT(ib) FROM ArInvoiceBatch ib"
 *
 * @jboss:query signature="Collection ejbSelectGeneric(java.lang.String jbossQl, java.lang.Object[] args)"
 *                         dynamic="true"
 *
 * @ejb:value-object match="*"
 *             name="ArInvoiceBatch"
 *
 * @jboss:persistence table-name="AR_INVC_BTCH"
 *
 * @jboss:entity-command name="mysql-get-generated-keys"
 *
 */

public abstract class ArInvoiceBatchBean extends AbstractEntityBean {


   // Access methods for persistent fields

   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    * @ejb:pk-field
    *
    * @jboss:column-name name="IB_CODE"
    *
    * @jboss:persistence auto-increment="true"
    **/
   public abstract Integer getIbCode();
   public abstract void setIbCode(Integer IB_CODE);

   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="IB_NM"
    **/
   public abstract String getIbName();
   /**
    * @ejb:interface-method view-type="local"
    **/
   public abstract void setIbName(String IB_NM);

   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="IB_DESC"
    **/
   public abstract String getIbDescription();
   /**
    * @ejb:interface-method view-type="local"
    **/
   public abstract void setIbDescription(String IB_DESC);
   
   
   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="IB_STATUS"
    **/
   public abstract String getIbStatus();
   /**
    * @ejb:interface-method view-type="local"
    **/
   public abstract void setIbStatus(String IB_STATUS);
   
   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="IB_TYP"
    **/
   public abstract String getIbType();
   /**
    * @ejb:interface-method view-type="local"
    **/
   public abstract void setIbType(String IB_TYP);

   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="IB_DT_CRTD"
    **/
   public abstract Date getIbDateCreated();
   /**
    * @ejb:interface-method view-type="local"
    **/
   public abstract void setIbDateCreated(Date IB_DT_CRTD);
   
   
   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="IB_CRTD_BY"
    **/
   public abstract String getIbCreatedBy();
   /**
    * @ejb:interface-method view-type="local"
    **/
   public abstract void setIbCreatedBy(String IB_CRTD_BY);
   
   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="IB_AD_BRNCH"
    **/
   public abstract Integer getIbAdBranch();
   /**
    * @ejb:interface-method view-type="local"
    **/
   public abstract void setIbAdBranch(Integer IB_AD_BRNCH);   
   
   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="IB_AD_CMPNY"
    **/
   public abstract Integer getIbAdCompany();
   /**
    * @ejb:interface-method view-type="local"
    **/
   public abstract void setIbAdCompany(Integer IB_AD_CMPNY);
   
   // Access methods for relationship fields

   /**
    * @ejb:interface-method view-type="local"
    * @ejb:relation name="invoicebatch-invoices"
    *               role-name="invoicebatch-has-many-invoices"
    */
   public abstract Collection getArInvoices();
   public abstract void setArInvoices(Collection arInvoices);
   
  
  /**
    * @jboss:dynamic-ql
    */
    public abstract Collection ejbSelectGeneric(java.lang.String jbossQl, java.lang.Object[] args)
       throws FinderException;

   // Business methods
   
   /**
    * @ejb:home-method view-type="local"
    */
    public Collection ejbHomeGetIbByCriteria(java.lang.String jbossQl, java.lang.Object[] args)
       throws FinderException {
       	
       return ejbSelectGeneric(jbossQl, args);
    }
      
   
   /**
    * @ejb:interface-method view-type="local"
    **/
   public void addArInvoice(LocalArInvoice arInvoice) {

      Debug.print("ArInvoiceBatchBean addArInvoice");
      try {
         Collection arInvoices = getArInvoices();
	 arInvoices.add(arInvoice);
      } catch (Exception ex) {
         throw new EJBException(ex.getMessage());
      }
   }

   /**
    * @ejb:interface-method view-type="local"
    **/
   public void dropArInvoice(LocalArInvoice arInvoice) {

      Debug.print("ArInvoiceBatchBean dropArInvoice");
      try {
         Collection arInvoices = getArInvoices();
	 arInvoices.remove(arInvoice);
      } catch (Exception ex) {
         throw new EJBException(ex.getMessage());
      }
   }
   
   // EntityBean methods

   /**
    * @ejb:create-method view-type="local"
    **/
   public Integer ejbCreate (Integer IB_CODE, String IB_NM, String IB_DESC,
      String IB_STATUS, String IB_TYP, Date IB_DT_CRTD, String IB_CRTD_BY,
	  Integer IB_AD_BRNCH, Integer IB_AD_CMPNY)
      throws CreateException {

      Debug.print("ArInvoiceBatchBean ejbCreate");
      setIbCode(IB_CODE);
      setIbName(IB_NM);
      setIbDescription(IB_DESC);
      setIbStatus(IB_STATUS);
      setIbType(IB_TYP);
      setIbDateCreated(IB_DT_CRTD);
      setIbCreatedBy(IB_CRTD_BY);
      setIbAdBranch(IB_AD_BRNCH);
      setIbAdCompany(IB_AD_CMPNY);
      
      return null;
   }

   /**
    * @ejb:create-method view-type="local"
    **/
   public Integer ejbCreate (String IB_NM, String IB_DESC,
      String IB_STATUS, String IB_TYP, Date IB_DT_CRTD, String IB_CRTD_BY,
	  Integer IB_AD_BRNCH, Integer IB_AD_CMPNY)
      throws CreateException {

      Debug.print("ArInvoiceBatchBean ejbCreate");

      setIbName(IB_NM);
      setIbDescription(IB_DESC);
      setIbStatus(IB_STATUS);
      setIbType(IB_TYP);
      setIbDateCreated(IB_DT_CRTD);
      setIbCreatedBy(IB_CRTD_BY);
      setIbAdBranch(IB_AD_BRNCH);
      setIbAdCompany(IB_AD_CMPNY);
      
      return null;
   }

   public void ejbPostCreate (Integer IB_CODE, String IB_NM, String IB_DESC,
      String IB_STATUS, String IB_TYP, Date IB_DT_CRTD, String IB_CRTD_BY,
	  Integer IB_AD_BRNCH, Integer IB_AD_CMPNY)
      throws CreateException { }

   public void ejbPostCreate (String IB_NM, String IB_DESC,
      String IB_STATUS, String IB_TYP, Date IB_DT_CRTD, String IB_CRTD_BY,
	  Integer IB_AD_BRNCH, Integer IB_AD_CMPNY)
      throws CreateException { }

} // ArInvoiceBatchBean class
