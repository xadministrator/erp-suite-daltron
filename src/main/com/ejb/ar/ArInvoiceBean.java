/*
 * com/ejb/ar/LocalArInvoiceBean.java
 *
 * Created on March 03, 2004, 03:27 PM
 */

package com.ejb.ar;

import java.util.Collection;
import java.util.Date;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;

import com.ejb.ar.LocalArSalesOrderInvoiceLine;
import com.ejb.hr.LocalHrDeployedBranch;
import com.ejb.pm.LocalPmContractTerm;
import com.ejb.pm.LocalPmProject;
import com.ejb.pm.LocalPmProjectPhase;
import com.ejb.pm.LocalPmProjectTypeType;
import com.util.AbstractEntityBean;
import com.util.Debug;

/**
 *
 * @author  Neil Andrew M.Ajero
 *
 * @ejb:bean name="ArInvoiceEJB"
 *           display-name="Invoice Entity"
 *           type="CMP"
 *           cmp-version="2.x"
 *           view-type="local"
 *           local-jndi-name="omega-ejb/ArInvoiceEJB"
 *           schema="ArInvoice"
 *           primkey-field="invCode"
 *
 * @ejb:pk class="java.lang.Integer"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="aruser"
 *                        role-link="aruserlink"
 *
 * @ejb:permission role-name="aruser"
 *
 * @ejb:interface local-class="com.ejb.ar.LocalArInvoice"
 *                local-extends="javax.ejb.EJBLocalObject"
 *
 * @ejb:home local-class="com.ejb.ar.LocalArInvoiceHome"
 *           local-extends="javax.ejb.EJBLocalHome"
 *
 * @ejb:finder signature="Collection findByInvNumberAndInvCreditMemoAndInvDateAndCustomerCode(byte INV_CRDT_MMO, java.util.Date INV_DT_TO, java.lang.String CST_CSTMR_CODE, java.lang.Integer INV_AD_CMPNY)"
 *             query="SELECT OBJECT(inv) FROM ArInvoice inv  WHERE inv.invCreditMemo = ?1 AND inv.invDate <= ?2 AND inv.arCustomer.cstCustomerCode = ?3 AND inv.invAdCompany = ?4"
 *
 * @jboss:query signature="Collection findByInvNumberAndInvCreditMemoAndInvDateAndCustomerCode(byte INV_CRDT_MMO, java.util.Date INV_DT_TO, java.lang.String CST_CSTMR_CODE, java.lang.Integer INV_AD_CMPNY)"
 *             query="SELECT OBJECT(inv) FROM ArInvoice inv  WHERE inv.invCreditMemo = ?1 AND inv.invDate <= ?2 AND inv.arCustomer.cstCustomerCode = ?3 AND inv.invAdCompany = ?4 ORDER BY inv.invDate"
 *
 * @ejb:finder signature="Collection findByInvPmProject(byte INV_CRDT_MMO, java.lang.Integer INV_AD_CMPNY)"
 *             query="SELECT OBJECT(inv) FROM ArInvoice inv  WHERE inv.invCreditMemo = ?1 AND inv.pmProject IS NOT NULL AND inv.pmProjectTypeType IS NOT NULL AND inv.pmContractTerm IS NOT NULL AND inv.invAdCompany = ?2"
 *
 * @jboss:query signature="Collection findByInvPmProject(byte INV_CRDT_MMO, java.lang.Integer INV_AD_CMPNY)"
 *             query="SELECT OBJECT(inv) FROM ArInvoice inv  WHERE inv.invCreditMemo = ?1 AND inv.pmProject IS NOT NULL AND inv.pmProjectTypeType IS NOT NULL AND inv.pmContractTerm IS NOT NULL AND inv.invAdCompany = ?2 ORDER BY inv.invDate"
 *
 * @ejb:finder signature="Collection findByInvNumberAndInvCreditMemoAndInvDateAndCustomerCodeOrderBySlp(byte INV_CRDT_MMO, java.util.Date INV_DT_TO, java.lang.String CST_CSTMR_CODE, java.lang.Integer INV_AD_CMPNY)"
 *             query="SELECT OBJECT(inv) FROM ArInvoice inv  WHERE inv.invCreditMemo = ?1 AND inv.invDate <= ?2 AND inv.arCustomer.cstCustomerCode = ?3 AND inv.invAdCompany = ?4"
 *
 * @jboss:query signature="Collection findByInvNumberAndInvCreditMemoAndInvDateAndCustomerCodeOrderBySlp(byte INV_CRDT_MMO, java.util.Date INV_DT_TO, java.lang.String CST_CSTMR_CODE, java.lang.Integer INV_AD_CMPNY)"
 *             query="SELECT OBJECT(inv) FROM ArInvoice inv  WHERE inv.invCreditMemo = ?1 AND inv.invDate <= ?2 AND inv.arCustomer.cstCustomerCode = ?3 AND inv.invAdCompany = ?4 ORDER BY inv.arSalesperson.slpSalespersonCode"
 *
 * @ejb:finder signature="Collection findByInvNumberAndInvCreditMemoAndInvDateAndCustomerCodeSlpCodeOrderBySlp(byte INV_CRDT_MMO, java.util.Date INV_DT_TO, java.lang.String CST_CSTMR_CODE, java.lang.String SLP_SLSPRSN_CODE, java.lang.Integer INV_AD_CMPNY)"
 *             query="SELECT OBJECT(inv) FROM ArInvoice inv  WHERE inv.invCreditMemo = ?1 AND inv.invDate <= ?2 AND inv.arCustomer.cstCustomerCode = ?3 AND inv.arSalesperson.slpSalespersonCode = ?4 AND inv.invAdCompany = ?5"
 *
 * @jboss:query signature="Collection findByInvNumberAndInvCreditMemoAndInvDateAndCustomerCodeSlpCodeOrderBySlp(byte INV_CRDT_MMO, java.util.Date INV_DT_TO, java.lang.String CST_CSTMR_CODE, java.lang.String SLP_SLSPRSN_CODE, java.lang.Integer INV_AD_CMPNY)"
 *             query="SELECT OBJECT(inv) FROM ArInvoice inv  WHERE inv.invCreditMemo = ?1 AND inv.invDate <= ?2 AND inv.arCustomer.cstCustomerCode = ?3 AND inv.arSalesperson.slpSalespersonCode = ?4 AND inv.invAdCompany = ?5 ORDER BY inv.arSalesperson.slpSalespersonCode"
 *
 * @ejb:finder signature="LocalArInvoice findByInvNumberAndInvCreditMemoAndBrCode(java.lang.String INV_NMBR, byte INV_CRDT_MMO, java.lang.Integer INV_AD_BRNCH, java.lang.Integer INV_AD_CMPNY)"
 *             query="SELECT OBJECT(inv) FROM ArInvoice inv  WHERE inv.invNumber = ?1 AND inv.invCreditMemo = ?2 AND inv.invAdBranch = ?3 AND inv.invAdCompany = ?4"
 *
 * @ejb:finder signature="LocalArInvoice findByReferenceNumberAndCompanyCode(java.lang.String INV_RFRNC_NMBR, java.lang.Integer INV_AD_CMPNY)"
 *             query="SELECT OBJECT(inv) FROM ArInvoice inv  WHERE inv.invPosted = 0 AND inv.invVoid = 0 AND inv.invReferenceNumber = ?1 AND inv.invCreditMemo = 0 AND inv.invAdCompany = ?2"
 *
 * @ejb:finder signature="LocalArInvoice findByUploadNumberAndCompanyCode(java.lang.String INV_UPLD_NMBR, java.lang.Integer INV_AD_CMPNY)"
 *             query="SELECT OBJECT(inv) FROM ArInvoice inv  WHERE inv.invVoid = 0 AND inv.invUploadNumber = ?1 AND inv.invCreditMemo = 0 AND inv.invAdCompany = ?2"
 *
 * @ejb:finder signature="Collection findByCmInvoiceNumberCreditMemoAndCompanyCode(java.lang.String INV_INVC_NMBR, java.lang.Integer INV_AD_BRNCH, java.lang.Integer INV_AD_CMPNY)"
 *             query="SELECT OBJECT(inv) FROM ArInvoice inv  WHERE inv.invCreditMemo = 1 AND inv.invPosted = 1  AND inv.invVoid = 0 AND inv.invVoidPosted = 0 AND inv.invCmInvoiceNumber = ?1 AND inv.invAdBranch = ?2 AND inv.invAdCompany = ?3"
 *
 * @jboss:query signature="Collection findByCmInvoiceNumberCreditMemoAndCompanyCode(java.lang.String INV_INVC_NMBR, java.lang.Integer INV_AD_BRNCH, java.lang.Integer INV_AD_CMPNY)"
 *             query="SELECT OBJECT(inv) FROM ArInvoice inv  WHERE inv.invCreditMemo = 1 AND inv.invPosted = 1  AND inv.invVoid = 0 AND inv.invVoidPosted = 0 AND inv.invCmInvoiceNumber = ?1 AND inv.invAdBranch = ?2 AND inv.invAdCompany = ?3"
 *
 * @ejb:finder signature="LocalArInvoice findByInvNumberAndInvCreditMemoAndCstCustomerCode(java.lang.String INV_NMBR, byte INV_CRDT_MMO, java.lang.String CST_CSTMR_CODE, java.lang.Integer INV_AD_CMPNY)"
 *             query="SELECT OBJECT(inv) FROM ArInvoice inv  WHERE inv.invNumber = ?1 AND inv.invCreditMemo = ?2 AND inv.arCustomer.cstCustomerCode = ?3 AND inv.invAdCompany = ?4"
 *
 * @ejb:finder signature="Collection findPostedInvByInvCreditMemoAndInvDateRange(byte VOU_CRDT_MMO, java.util.Date INV_DT_FRM, java.util.Date INV_DT_TO, java.lang.Integer INV_AD_CMPNY)"
 *             query="SELECT OBJECT(inv) FROM ArInvoice inv  WHERE inv.invPosted = 1 AND inv.invCreditMemo = ?1 AND inv.invDate >= ?2 AND inv.invDate <= ?3 AND inv.invAdCompany = ?4"
 *
 * @jboss:query signature="Collection findPostedInvByInvCreditMemoAndInvDateRange(byte VOU_CRDT_MMO, java.util.Date INV_DT_FRM, java.util.Date INV_DT_TO, java.lang.Integer INV_AD_CMPNY)"
 *             query="SELECT OBJECT(inv) FROM ArInvoice inv  WHERE inv.invPosted = 1 AND inv.invCreditMemo = ?1 AND inv.invDate >= ?2 AND inv.invDate <= ?3 AND inv.invAdCompany = ?4 ORDER BY inv.invDate"
 *
 * @ejb:finder signature="Collection findByInvCreditMemoAndInvCmInvoiceNumberAndInvVoid(byte INV_CRDT_MMO, java.lang.String INV_CM_INVC_NMBR, byte INV_VD, java.lang.Integer INV_AD_CMPNY)"
 *             query="SELECT OBJECT(inv) FROM ArInvoice inv  WHERE inv.invCreditMemo = ?1 AND inv.invCmInvoiceNumber = ?2 AND inv.invVoid = ?3 AND inv.invAdCompany = ?4"
 *
 * @ejb:finder signature="Collection findByInvCreditMemoAndInvCmInvoiceNumberAndInvVoidAndInvPosted(byte INV_CRDT_MMO, java.lang.String INV_CM_INVC_NMBR, byte INV_VD, byte INV_PSTD, java.lang.Integer INV_AD_CMPNY)"
 *             query="SELECT OBJECT(inv) FROM ArInvoice inv  WHERE inv.invCreditMemo = ?1 AND inv.invCmInvoiceNumber = ?2 AND inv.invVoid = ?3 AND inv.invPosted = ?4 AND inv.invAdCompany = ?5"
 *
 * @ejb:finder signature="Collection findByInvPostedAndInvVoidAndIbName(byte INV_PSTD, byte INV_VD, java.lang.String IB_NM, java.lang.Integer INV_AD_CMPNY)"
 *             query="SELECT OBJECT(inv) FROM ArInvoice inv WHERE inv.invPosted=?1 AND inv.invVoid = ?2 AND inv.arInvoiceBatch.ibName = ?3 AND inv.invAdCompany = ?4"
 *
 * @ejb:finder signature="Collection findByIbName(java.lang.String IB_NM, java.lang.Integer INV_AD_CMPNY)"
 *             query="SELECT OBJECT(inv) FROM ArInvoice inv  WHERE inv.arInvoiceBatch.ibName=?1 AND inv.invAdCompany = ?2"
 *
 * @jboss:query signature="Collection findByIbName(java.lang.String IB_NM, java.lang.Integer INV_AD_CMPNY)"
 *             query="SELECT OBJECT(inv) FROM ArInvoice inv  WHERE inv.arInvoiceBatch.ibName=?1 AND inv.invAdCompany = ?2 ORDER BY inv.invNumber, inv.invDate"
 *
 * @ejb:finder signature="Collection findDraftInvByInvCreditMemoAndBrCode(java.lang.String INV_TYP, byte INV_CRDT_MMO, java.lang.Integer INV_AD_BRNCH, java.lang.Integer INV_AD_CMPNY)"
 *             query="SELECT OBJECT(inv) FROM ArInvoice inv WHERE inv.invPosted = 0 AND inv.invApprovalStatus IS NULL AND inv.invVoid = 0 AND inv.invType = ?1 AND inv.invCreditMemo = ?2 AND inv.invAdBranch = ?3 AND inv.invAdCompany = ?4"
 *
 * @ejb:finder signature="Collection findPostedByInvCreditMemoAndInvDateRangeAndCstNameAndTcType(byte INV_CRDT_MMO, java.util.Date INV_DT_FRM, java.util.Date INV_DT_TO, java.lang.String CST_NM, java.lang.String TC_TYP, java.lang.Integer INV_AD_CMPNY)"
 *             query="SELECT OBJECT(inv) FROM ArInvoice inv  WHERE inv.invPosted = 1 AND inv.invCreditMemo = ?1 AND inv.invDate >= ?2 AND inv.invDate <= ?3 AND inv.arCustomer.cstName = ?4 AND inv.arTaxCode.tcType = ?5 AND inv.invAdCompany = ?6"
 *
 * @jboss:query signature="Collection findPostedByInvCreditMemoAndInvDateRangeAndCstNameAndTcType(byte INV_CRDT_MMO, java.util.Date INV_DT_FRM, java.util.Date INV_DT_TO, java.lang.String CST_NM, java.lang.String TC_TYP, java.lang.Integer INV_AD_CMPNY)"
 *             query="SELECT OBJECT(inv) FROM ArInvoice inv  WHERE inv.invPosted = 1 AND inv.invCreditMemo = ?1 AND inv.invDate >= ?2 AND inv.invDate <= ?3 AND inv.arCustomer.cstName = ?4 AND inv.arTaxCode.tcType = ?5 AND inv.invAdCompany = ?6 ORDER BY inv.invDate"
 *
 * @ejb:finder signature="Collection findPostedByInvCreditMemoAndInvDateRangeExemptZero(byte INV_CRDT_MMO, java.util.Date INV_DT_FRM, java.util.Date INV_DT_TO, java.lang.Integer INV_AD_CMPNY)"
 *             query="SELECT OBJECT(inv) FROM ArInvoice inv  WHERE inv.invPosted = 1 AND inv.invCreditMemo = ?1 AND inv.invDate >= ?2 AND inv.invDate <= ?3 AND inv.arTaxCode.tcType IN ('EXEMPT','ZERO-RATED') AND inv.invAdCompany = ?4"
 *
 * @jboss:query signature="Collection findPostedByInvCreditMemoAndInvDateRangeExemptZero(byte INV_CRDT_MMO, java.util.Date INV_DT_FRM, java.util.Date INV_DT_TO,java.lang.Integer INV_AD_CMPNY)"
 *             query="SELECT OBJECT(inv) FROM ArInvoice inv  WHERE inv.invPosted = 1 AND inv.invCreditMemo = ?1 AND inv.invDate >= ?2 AND inv.invDate <= ?3 AND inv.arTaxCode.tcType IN ('EXEMPT','ZERO-RATED') AND inv.invAdCompany = ?4 ORDER BY inv.invDate"
 *
 * @ejb:finder signature="Collection findPostedByInvCreditMemoAndInvDateToAndCstCustomerCode(java.util.Date INV_DT_TO, java.lang.String CST_CSTMR_CODE, java.lang.Integer INV_AD_CMPNY)"
 *             query="SELECT OBJECT(inv) FROM ArInvoice inv  WHERE inv.invPosted = 1 AND inv.invDate <= ?1 AND inv.arCustomer.cstCustomerCode = ?2 AND inv.invAdCompany = ?3"
 *
 * @jboss:query signature="Collection findPostedByInvCreditMemoAndInvDateToAndCstCustomerCode(java.util.Date INV_DT_TO, java.lang.String CST_CSTMR_CODE, java.lang.Integer INV_AD_CMPNY)"
 *             query="SELECT OBJECT(inv) FROM ArInvoice inv  WHERE inv.invPosted = 1 AND inv.invDate <= ?1 AND inv.arCustomer.cstCustomerCode = ?2 AND inv.invAdCompany = ?3"
 *
 * @ejb:finder signature="Collection findUnpostedInvByInvDateRange(java.util.Date INV_DT_FRM, java.util.Date INV_DT_TO, java.lang.Integer INV_AD_CMPNY)"
 *             query="SELECT OBJECT(inv) FROM ArInvoice inv  WHERE inv.invPosted = 0 AND inv.invVoid = 0 AND inv.invDate >= ?1 AND inv.invDate <= ?2 AND inv.invAdCompany = ?3"
 *
 * @jboss:query signature="Collection findUnpostedInvByInvDateRange(java.util.Date INV_DT_FRM, java.util.Date INV_DT_TO, java.lang.Integer INV_AD_CMPNY)"
 *             query="SELECT OBJECT(inv) FROM ArInvoice inv  WHERE inv.invPosted = 0 AND inv.invVoid = 0 AND inv.invDate >= ?1 AND inv.invDate <= ?2 AND inv.invAdCompany = ?3 ORDER BY inv.invDate"
 *
 * @ejb:finder signature="Collection findUnpostedInvByInvDateRangeByBranch(java.util.Date INV_DT_FRM, java.util.Date INV_DT_TO, java.lang.Integer INV_AD_BRNCH, java.lang.Integer INV_AD_CMPNY)"
 *             query="SELECT OBJECT(inv) FROM ArInvoice inv  WHERE inv.invPosted = 0 AND inv.invVoid = 0 AND inv.invDate >= ?1 AND inv.invDate <= ?2 AND inv.invAdBranch = ?3 AND inv.invAdCompany = ?4"
 *
 * @jboss:query signature="Collection findUnpostedInvByInvDateRangeByBranch(java.util.Date INV_DT_FRM, java.util.Date INV_DT_TO, java.lang.Integer INV_AD_BRNCH, java.lang.Integer INV_AD_CMPNY)"
 *             query="SELECT OBJECT(inv) FROM ArInvoice inv  WHERE inv.invPosted = 0 AND inv.invVoid = 0 AND inv.invDate >= ?1 AND inv.invDate <= ?2 AND inv.invAdBranch = ?3 AND inv.invAdCompany = ?4 ORDER BY inv.invDate"
 *
 * @ejb:finder signature="Collection findUnpostedInvByCstCustomerCode(java.lang.String CST_CSTMR_CODE, java.lang.Integer INV_AD_CMPNY)"
 * 			   query="SELECT OBJECT(inv) FROM ArInvoice inv WHERE inv.arCustomer.cstCustomerCode = ?1 AND inv.invPosted = 0 AND inv.invAdCompany = ?2"
 *
 * @ejb:finder signature="Collection findPostedSoMatchedInvByCstCustomerCode(java.lang.String CST_CSTMR_CD, java.lang.Integer INV_AD_CMPNY)"
 *			   query="SELECT OBJECT(inv) FROM ArInvoice inv WHERE inv.invPosted = 1 AND inv.invSoNumber IS NOT NULL AND inv.arCustomer.cstCustomerCode = ?1 AND inv.invAdCompany = ?2"
 *
 * @ejb:finder signature="Collection findPostedJoMatchedInvByCstCustomerCode(java.lang.String CST_CSTMR_CD, java.lang.Integer INV_AD_CMPNY)"
 *			   query="SELECT OBJECT(inv) FROM ArInvoice inv WHERE inv.invPosted = 1 AND inv.invJoNumber IS NOT NULL AND inv.arCustomer.cstCustomerCode = ?1 AND inv.invAdCompany = ?2"
 *
 * @ejb:finder signature="Collection findInvForPostingByBranch(java.lang.Integer INV_AD_BRNCH, java.lang.Integer INV_AD_CMPNY)"
 *             query="SELECT OBJECT(inv) FROM ArInvoice inv WHERE inv.invCreditMemo=0 AND (inv.invApprovalStatus='APPROVED' OR inv.invApprovalStatus='N/A') AND inv.invPosted = 0 AND inv.invVoid = 0 AND inv.invAdBranch=?1 AND inv.invAdCompany=?2"
 *
 * @ejb:finder signature="Collection findInvCreditMemoForPostingByBranch(java.lang.Integer INV_AD_BRNCH, java.lang.Integer INV_AD_CMPNY)"
 *             query="SELECT OBJECT(inv) FROM ArInvoice inv WHERE inv.invCreditMemo=1 AND (inv.invApprovalStatus='APPROVED' OR inv.invApprovalStatus='N/A') AND inv.invPosted = 0 AND inv.invVoid = 0 AND inv.invAdBranch=?1 AND inv.invAdCompany=?2"
 *
 * @ejb:select signature="Collection ejbSelectGeneric(java.lang.String jbossQl, java.lang.Object[] args)"
 *             result-type-mapping="Local"
 *             method-intf="LocalHome"
 *             query="SELECT OBJECT(inv) FROM ArInvoice inv"
 *
 * @ejb:finder signature="Collection findByInvLvFreight(java.lang.String INV_LV_FRGHT, java.lang.Integer INV_AD_CMPNY)"
 *             query="SELECT OBJECT(inv) FROM ArInvoice inv WHERE inv.invLvFreight=?1 AND inv.invAdCompany=?2"
 *
 * @ejb:finder signature="Collection findByInvLvShift(java.lang.String INV_LV_SHFT, java.lang.Integer INV_AD_CMPNY)"
 *             query="SELECT OBJECT(inv) FROM ArInvoice inv WHERE inv.invLvShift=?1 AND inv.invAdCompany=?2"
 *
 * @ejb:finder signature="Collection findByAdCmpnyAll(byte INV_CRDT_MMO, java.lang.Integer INV_AD_CMPNY)"
 *             query="SELECT OBJECT(inv) FROM ArInvoice inv WHERE inv.invCreditMemo = ?1 AND inv.invAdCompany=?2"
 *
 * @ejb:finder signature="Collection findByInvCreditMemoAndIbName(byte INV_CRDT_MMO, java.lang.String IB_NM, java.lang.Integer INV_AD_CMPNY)"
 *             query="SELECT OBJECT(inv) FROM ArInvoice inv WHERE inv.invCreditMemo = ?1 AND inv.arInvoiceBatch.ibName=?2 AND inv.invAdCompany=?3"
 *
 * @jboss:query signature="Collection findByInvNmbrAndInvCreditMemoAndBrCode(java.lang.String INV_NMBR, java.lang.Integer INV_AD_BRNCH, java.lang.Integer INV_AD_CMPNY)"
 *              query="SELECT OBJECT(inv) FROM ArInvoice inv  WHERE inv.invCreditMemo = 1 AND inv.invNumber = ?1 AND inv.invAdBranch = ?2 AND inv.invAdCompany = ?3"
 *
 * @ejb:finder signature="Collection findByInvNmbrAndInvCreditMemoAndBrCode(java.lang.String INV_NMBR, java.lang.Integer INV_AD_BRNCH, java.lang.Integer INV_AD_CMPNY)"
 *             query="SELECT OBJECT(inv) FROM ArInvoice inv  WHERE inv.invCreditMemo = 1 AND inv.invNumber = ?1 AND inv.invAdBranch = ?2 AND inv.invAdCompany = ?3"
 *
 * @ejb:finder signature="Collection findPostedTaxInvByInvDateRange(byte INV_CRDT_MMO, java.util.Date INV_DT_FRM, java.util.Date INV_DT_TO, java.lang.Integer INV_AD_BRNCH, java.lang.Integer INV_AD_CMPNY)"
 *             query="SELECT OBJECT(inv) FROM ArInvoice inv WHERE inv.invPosted = 1 AND inv.arTaxCode.tcType IN ('EXEMPT','ZERO-RATED','INCLUSIVE','EXCLUSIVE') AND inv.invCreditMemo = ?1 AND inv.invDate >= ?2 AND inv.invDate <= ?3 AND inv.invAdBranch = ?4 AND inv.invAdCompany = ?5 ORDER BY inv.invDate, inv.invNumber"
 *
 * @jboss:query signature="Collection findPostedTaxInvByInvDateRange(byte INV_CRDT_MMO, java.util.Date INV_DT_FRM, java.util.Date INV_DT_TO, java.lang.Integer INV_AD_BRNCH, java.lang.Integer INV_AD_CMPNY)"
 *             query="SELECT OBJECT(inv) FROM ArInvoice inv WHERE inv.invPosted = 1 AND inv.arTaxCode.tcType IN ('EXEMPT','ZERO-RATED','INCLUSIVE','EXCLUSIVE') AND inv.invCreditMemo = ?1 AND inv.invDate >= ?2 AND inv.invDate <= ?3 AND inv.invAdBranch = ?4 AND inv.invAdCompany = ?5 ORDER BY inv.invDate, inv.invNumber"
 *
 * @jboss:query signature="Collection ejbSelectGeneric(java.lang.String jbossQl, java.lang.Object[] args)"
 *                         dynamic="true"
 *
 * @ejb:value-object match="*"
 *             name="ArInvoice"
 *
 * @jboss:persistence table-name="AR_INVC"
 *
 * @jboss:entity-command name="mysql-get-generated-keys"
 */

public abstract class ArInvoiceBean extends AbstractEntityBean {


	// PERSISTENT METHODS

	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 * @ejb:pk-field
	 *
	 * @jboss:column-name name="INV_CODE"
	 *
	 * @jboss:persistence auto-increment="true"
	 **/
	public abstract Integer getInvCode();
	public abstract void setInvCode(Integer INV_CODE);


	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="INV_DCMNT_TYP"
	 **/
	public abstract String getInvDocumentType();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setInvDocumentType(String INV_DCMNT_TYP);



	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="INV_TYP"
	 **/
	public abstract String getInvType();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setInvType(String INV_TYP);

	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="INV_PRTL_CM"
	 **/
	public abstract byte getInvPartialCm();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setInvPartialCm(byte INV_PRTL_CM);

	
	
	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="INV_CRDT_MMO"
	 **/
	public abstract byte getInvCreditMemo();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setInvCreditMemo(byte INV_CRDT_MMO);
	
	
	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="INV_DESC"
	 **/
	public abstract String getInvDescription();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setInvDescription(String INV_DESC);


	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="INV_DT"
	 **/
	public abstract Date getInvDate();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setInvDate(Date INV_DT);

	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="INV_NMBR"
	 **/
	public abstract String getInvNumber();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setInvNumber(String INV_NMBR);

	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="INV_RFRNC_NMBR"
	 **/
	public abstract String getInvReferenceNumber();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setInvReferenceNumber(String INV_RFRNC_NMBR);


	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="INV_UPLD_NMBR"
	 **/
	public abstract String getInvUploadNumber();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setInvUploadNumber(String INV_UPLD_NMBR);


	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="INV_CM_INVC_NMBR"
	 **/
	public abstract String getInvCmInvoiceNumber();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setInvCmInvoiceNumber(String INV_CM_INVC_NMBR);

	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="INV_CM_RFRNC_NMBR"
	 **/
	public abstract String getInvCmReferenceNumber();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setInvCmReferenceNumber(String INV_CM_RFRNC_NMBR);


	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="INV_AMNT_DUE"
	 **/
	public abstract double getInvAmountDue();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setInvAmountDue(double INV_AMNT_DUE);

	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="INV_DWN_PYMNT"
	 **/
	public abstract double getInvDownPayment();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setInvDownPayment(double INV_DWN_PYMNT);

	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="INV_AMNT_PD"
	 **/
	public abstract double getInvAmountPaid();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setInvAmountPaid(double INV_AMNT_PD);


	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="INV_PNT_DUE"
	 **/
	public abstract double getInvPenaltyDue();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setInvPenaltyDue(double INV_PNT_DUE);

	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="INV_PNT_PD"
	 **/
	public abstract double getInvPenaltyPaid();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setInvPenaltyPaid(double INV_PNT_PD);



	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="INV_AMNT_UNEARND_INT"
	 **/
	public abstract double getInvAmountUnearnedInterest();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setInvAmountUnearnedInterest(double INV_AMNT_UNEARND_INT);

	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="INV_CNVRSN_DT"
	 **/
	public abstract Date getInvConversionDate();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setInvConversionDate(Date INV_CNVRSN_DT);

	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="INV_CNVRSN_RT"
	 **/
	public abstract double getInvConversionRate();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setInvConversionRate(double INV_CNVRSN_RT);

	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="INV_MMO"
	 **/
	public abstract String getInvMemo();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setInvMemo(String INV_MMO);


	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="INV_PRV_RDNG"
	 **/
	public abstract double getInvPreviousReading();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setInvPreviousReading(double INV_PRV_RDNG);

	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="INV_PRSNT_RDNG"
	 **/
	public abstract double getInvPresentReading();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setInvPresentReading(double INV_PRSNT_RDNG);


	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="INV_BLL_TO_ADDRSS"
	 **/
	public abstract String getInvBillToAddress();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setInvBillToAddress(String INV_BLL_TO_ADDRSS);

	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="INV_BLL_TO_CNTCT"
	 **/
	public abstract String getInvBillToContact();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setInvBillToContact(String INV_BLL_TO_CNTCT);

	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="INV_BLL_TO_ALT_CNTCT"
	 **/
	public abstract String getInvBillToAltContact();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setInvBillToAltContact(String INV_BLL_TO_ALT_CNTCT);

	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="INV_BLL_TO_PHN"
	 **/
	public abstract String getInvBillToPhone();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setInvBillToPhone(String INV_BLL_TO_PHN);

	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="INV_BLLNG_HDR"
	 **/
	public abstract String getInvBillingHeader();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setInvBillingHeader(String INV_BLLNG_HDR);

	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="INV_BLLNG_FTR"
	 **/
	public abstract String getInvBillingFooter();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setInvBillingFooter(String INV_BLLNG_FTR);
	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="INV_BLLNG_HDR2"
	 **/
	public abstract String getInvBillingHeader2();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setInvBillingHeader2(String INV_BLLNG_HDR2);

	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="INV_BLLNG_FTR2"
	 **/
	public abstract String getInvBillingFooter2();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setInvBillingFooter2(String INV_BLLNG_FTR2);

	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="INV_BLLNG_HDR3"
	 **/
	public abstract String getInvBillingHeader3();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setInvBillingHeader3(String INV_BLLNG_HDR3);

	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="INV_BLLNG_FTR3"
	 **/
	public abstract String getInvBillingFooter3();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setInvBillingFooter3(String INV_BLLNG_FTR3);

	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="INV_BLLNG_SGNTRY"
	 **/
	public abstract String getInvBillingSignatory();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setInvBillingSignatory(String INV_BLLNG_SGNTRY);

	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="INV_SGNTRY_TTL"
	 **/
	public abstract String getInvSignatoryTitle();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setInvSignatoryTitle(String INV_SGNTRY_TTL);

	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="INV_SHP_TO_ADDRSS"
	 **/
	public abstract String getInvShipToAddress();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setInvShipToAddress(String INV_SHP_TO_ADDRSS);

	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="INV_SHP_TO_CNTCT"
	 **/
	public abstract String getInvShipToContact();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setInvShipToContact(String INV_SHP_TO_CNTCT);

	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="INV_SHP_TO_ALT_CNTCT"
	 **/
	public abstract String getInvShipToAltContact();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setInvShipToAltContact(String INV_SHP_TO_ALT_CNTCT);

	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="INV_SHP_TO_PHN"
	 **/
	public abstract String getInvShipToPhone();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setInvShipToPhone(String INV_SHP_TO_PHN);

	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="INV_SHP_DT"
	 **/
	public abstract Date getInvShipDate();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setInvShipDate(Date INV_SHP_DT);

	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="INV_LV_FRGHT"
	 **/
	public abstract String getInvLvFreight();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setInvLvFreight(String INV_LV_FRGHT);

	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="INV_APPRVL_STATUS"
	 **/
	public abstract String getInvApprovalStatus();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setInvApprovalStatus(String INV_APPRVL_STATUS);

	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="INV_RSN_FR_RJCTN"
	 **/
	public abstract String getInvReasonForRejection();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setInvReasonForRejection(String INV_RSN_FR_RJCTN);

	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="INV_PSTD"
	 **/
	public abstract byte getInvPosted();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setInvPosted(byte INV_PSTD);


	/**
 	 * @ejb:persistent-field
 	 * @ejb:interface-method view-type="local"
 	 *
 	 * @jboss:column-name name="INV_VD_APPRVL_STATUS"
 	 **/
 	public abstract String getInvVoidApprovalStatus();
 	/**
 	 * @ejb:interface-method view-type="local"
 	 **/
 	public abstract void setInvVoidApprovalStatus(String INV_VD_APPRVL_STATUS);

 	/**
 	 * @ejb:persistent-field
 	 * @ejb:interface-method view-type="local"
 	 *
 	 * @jboss:column-name name="INV_VD_PSTD"
 	 **/
 	public abstract byte getInvVoidPosted();
 	/**
 	 * @ejb:interface-method view-type="local"
 	 **/
 	public abstract void setInvVoidPosted(byte INV_VD_PSTD);


	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="INV_VD"
	 **/
	public abstract byte getInvVoid();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setInvVoid(byte INV_VD);

	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="INV_CONT"
	 **/
	public abstract byte getInvContention();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setInvContention(byte INV_CONT);


	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="INV_DSBL_INTRST"
	 **/
	public abstract byte getInvDisableInterest();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setInvDisableInterest(byte INV_DSBL_INTRST);


	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="INV_INTRST"
	 **/
	public abstract byte getInvInterest();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setInvInterest(byte INV_INTRST);

	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="INV_INTRST_RFRNC_NMBR"
	 **/
	public abstract String getInvInterestReferenceNumber();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setInvInterestReferenceNumber(String INV_INTRST_RFRNC_NMBR);


	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="INV_INTRST_AMNT"
	 **/
	public abstract double getInvInterestAmount();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setInvInterestAmount(double INV_INTRST_AMNT);


	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="INV_INTRST_CRTD_BY"
	 **/
	public abstract String getInvInterestCreatedBy();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setInvInterestCreatedBy(String INV_INTRST_CRTD_BY);

	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="INV_INTRST_DT_CRTD"
	 **/
	public abstract Date getInvInterestDateCreated();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setInvInterestDateCreated(Date INV_INTRST_DT_CRTD);



	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="INV_INTRST_NXT_RN_DT"
	 **/
	 public abstract Date getInvInterestNextRunDate();
	 /**
	  * @ejb:interface-method view-type="local"
	  **/
	 public abstract void setInvInterestNextRunDate(Date INV_INTRST_NXT_RN_DT);

	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="INV_INTRST_LST_RN_DT"
	 **/
	 public abstract Date getInvInterestLastRunDate();
	 /**
	  * @ejb:interface-method view-type="local"
	  **/
	 public abstract void setInvInterestLastRunDate(Date INV_INTRST_LST_RN_DT);



	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="INV_CRTD_BY"
	 **/
	public abstract String getInvCreatedBy();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setInvCreatedBy(String INV_CRTD_BY);

	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="INV_DT_CRTD"
	 **/
	public abstract Date getInvDateCreated();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setInvDateCreated(Date INV_DT_CRTD);

	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="INV_LST_MDFD_BY"
	 **/
	public abstract String getInvLastModifiedBy();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setInvLastModifiedBy(String INV_LST_MDFD_BY);

	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="INV_DT_LST_MDFD"
	 **/
	public abstract Date getInvDateLastModified();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setInvDateLastModified(Date INV_DT_LST_MDFD);

	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="INV_APPRVD_RJCTD_BY"
	 **/
	public abstract String getInvApprovedRejectedBy();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setInvApprovedRejectedBy(String INV_APPRVD_RJCTD_BY);

	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="INV_DT_APPRVD_RJCTD"
	 **/
	public abstract Date getInvDateApprovedRejected();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setInvDateApprovedRejected(Date INV_DT_APPRVD_RJCTD);

	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="INV_PSTD_BY"
	 **/
	public abstract String getInvPostedBy();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setInvPostedBy(String INV_PSTD_BY);

	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="INV_DT_PSTD"
	 **/
	public abstract Date getInvDatePosted();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setInvDatePosted(Date INV_DT_PSTD);

	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="INV_PRNTD"
	 **/
	public abstract byte getInvPrinted();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setInvPrinted(byte INV_PRNTD);


	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="INV_LV_SHFT"
	 **/
	public abstract String getInvLvShift();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setInvLvShift(String INV_LV_SHFT);

	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="INV_SO_NMBR"
	 **/
	public abstract String getInvSoNumber();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setInvSoNumber(String INV_SO_NMBR);


	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="INV_JO_NMBR"
	 **/
	public abstract String getInvJoNumber();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setInvJoNumber(String INV_JO_NMBR);

	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="INV_DBT_MEMO"
	 **/
	public abstract byte getInvDebitMemo();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setInvDebitMemo(byte arDebitMemo);

	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="INV_SBJCT_TO_CMMSSN"
	 **/
	public abstract byte getInvSubjectToCommission();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setInvSubjectToCommission(byte INV_SBJCT_TO_CMMSSN);

	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="INV_CLNT_PO"
	 **/
	public abstract String getInvClientPO();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setInvClientPO(String INV_CLNT_PO);

	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="INV_EFFCTVTY_DT"
	 **/
	public abstract Date getInvEffectivityDate();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setInvEffectivityDate(Date INV_EFFCTVTY_DT);
	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="INV_RCVD_DT"
	 **/
	public abstract Date getInvRecieveDate();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setInvRecieveDate(Date INV_RCVD_DT);




	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="INV_HR_CRRNT_JB_PSTN"
	 **/
	public abstract String getInvHrCurrentJobPosition();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setInvHrCurrentJobPosition(String INV_HR_CRRNT_JB_PSTN);

	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="INV_HR_MNGNG_BRNCH"
	 **/
	public abstract String getInvHrManagingBranch();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setInvHrManagingBranch(String INV_HR_MNGNG_BRNCH);


	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="REPORT_PARAMETER"
	 **/
	public abstract String getReportParameter();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setReportParameter(String REPORT_PARAMETER);


	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="INV_AD_BRNCH"
	 **/
	public abstract Integer getInvAdBranch();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setInvAdBranch(Integer INV_AD_BRNCH);

	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="INV_AD_CMPNY"
	 **/
	public abstract Integer getInvAdCompany();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setInvAdCompany(Integer INV_AD_CMPNY);

	// RELATIONSHIP METHODS


	//HRIS

	/**
	 * @ejb:interface-method view-type="local"
	 * @ejb:relation name="deployedbranch-invoices"
	 *               role-name="invoice-has-one-deployedbranch"
	 *
	 * @jboss:relation related-pk-field="dbCode"
	 *                 fk-column="HR_DEPLOYED_BRANCH"
	 */
	public abstract LocalHrDeployedBranch getHrDeployedBranch();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setHrDeployedBranch(LocalHrDeployedBranch hrDeployedBranch);



	//PMA

	/**
	 * @ejb:interface-method view-type="local"
	 * @ejb:relation name="project-invoices"
	 *               role-name="invoice-has-one-project"
	 *
	 * @jboss:relation related-pk-field="prjCode"
	 *                 fk-column="PM_PROJECT"
	 */
	public abstract LocalPmProject getPmProject();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setPmProject(LocalPmProject pmProject);


	/**
	 * @ejb:interface-method view-type="local"
	 * @ejb:relation name="projecttypetype-invoices"
	 *               role-name="invoice-has-one-projecttypetype"
	 *
	 * @jboss:relation related-pk-field="pttCode"
	 *                 fk-column="PM_PROJECT_TYPE_TYPE"
	 */
	public abstract LocalPmProjectTypeType getPmProjectTypeType();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setPmProjectTypeType(LocalPmProjectTypeType pmProjectTypeType);


	/**
	 * @ejb:interface-method view-type="local"
	 * @ejb:relation name="projectphase-invoices"
	 *               role-name="invoice-has-one-projectphase"
	 *
	 * @jboss:relation related-pk-field="ppCode"
	 *                 fk-column="PM_PROJECT_PHASE"
	 */
	public abstract LocalPmProjectPhase getPmProjectPhase();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setPmProjectPhase(LocalPmProjectPhase pmProjectPhase);



	/**
	 * @ejb:interface-method view-type="local"
	 * @ejb:relation name="contractterm-invoices"
	 *               role-name="invoice-has-one-contractterm"
	 *
	 * @jboss:relation related-pk-field="ctCode"
	 *                 fk-column="PM_CONTRACT_TERM"
	 */
	public abstract LocalPmContractTerm getPmContractTerm();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setPmContractTerm(LocalPmContractTerm pmContractTerm);







	/**
	 * @ejb:interface-method view-type="local"
	 * @ejb:relation name="customer-invoices"
	 *               role-name="invoice-has-one-customer"
	 *
	 * @jboss:relation related-pk-field="cstCode"
	 *                 fk-column="AR_CUSTOMER"
	 */
	public abstract LocalArCustomer getArCustomer();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setArCustomer(LocalArCustomer arCustomer);

	/**
	 * @ejb:interface-method view-type="local"
	 * @ejb:relation name="arwithholdingtaxcode-invoices"
	 *               role-name="invoice-has-one-arwithholdingtaxcode"
	 *
	 * @jboss:relation related-pk-field="wtcCode"
	 *                 fk-column="AR_WITHHOLDING_TAX_CODE"
	 */
	public abstract LocalArWithholdingTaxCode getArWithholdingTaxCode();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setArWithholdingTaxCode(LocalArWithholdingTaxCode arWithholdingTaxCode);


	/**
	 * @ejb:interface-method view-type="local"
	 * @ejb:relation name="paymentterm-invoices"
	 *               role-name="invoice-has-one-paymentterm"
	 *
	 * @jboss:relation related-pk-field="pytCode"
	 *                 fk-column="AD_PAYMENT_TERM"
	 */
	public abstract com.ejb.ad.LocalAdPaymentTerm getAdPaymentTerm();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setAdPaymentTerm(com.ejb.ad.LocalAdPaymentTerm adPaymentTerm);

	/**
	 * @ejb:interface-method view-type="local"
	 * @ejb:relation name="functionalcurrency-invoices"
	 *               role-name="invoice-has-one-functionalcurrency"
	 *
	 * @jboss:relation related-pk-field="fcCode"
	 *                 fk-column="GL_FUNCTIONAL_CURRENCY"
	 */
	public abstract com.ejb.gl.LocalGlFunctionalCurrency getGlFunctionalCurrency();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setGlFunctionalCurrency(com.ejb.gl.LocalGlFunctionalCurrency glFunctionalCurrency);


	/**
	 * @ejb:interface-method view-type="local"
	 * @ejb:relation name="artaxcode-invoices"
	 *               role-name="invoice-has-one-artaxcode"
	 *
	 * @jboss:relation related-pk-field="tcCode"
	 *                 fk-column="AR_TAX_CODE"
	 */
	public abstract LocalArTaxCode getArTaxCode();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setArTaxCode(LocalArTaxCode arTaxCode);

	/**
	 * @ejb:interface-method view-type="local"
	 * @ejb:relation name="invoice-ardistributionrecords"
	 *               role-name="invoice-has-many-ardistributionrecords"
	 *
	 */
	public abstract Collection getArDistributionRecords();
	public abstract void setArDistributionRecords(Collection arDistributionRecords);

	/**
	 * @ejb:interface-method view-type="local"
	 * @ejb:relation name="invoice-invoicepaymentschedules"
	 *               role-name="invoice-has-many-invoicepaymentschedules"
	 */
	public abstract Collection getArInvoicePaymentSchedules();
	public abstract void setArInvoicePaymentSchedules(Collection arInvoicePaymentSchedules);

	/**
	 * @ejb:interface-method view-type="local"
	 * @ejb:relation name="invoice-invoicelines"
	 *               role-name="invoice-has-many-invoicelines"
	 */
	public abstract Collection getArInvoiceLines();
	public abstract void setArInvoiceLines(Collection arInvoiceLines);

	/**
	 * @ejb:interface-method view-type="local"
	 * @ejb:relation name="invoice-invoicelineitems"
	 *               role-name="invoice-has-many-invoicelineitems"
	 */
	public abstract Collection getArInvoiceLineItems();
	public abstract void setArInvoiceLineItems(Collection arInvoiceLineItems);

	/**
	 * @ejb:interface-method view-type="local"
	 * @ejb:relation name="invoice-salesorderinvoiceline"
	 *               role-name="invoice-has-many-salesorderinvoiceline"
	 */
	public abstract Collection getArSalesOrderInvoiceLines();
	public abstract void setArSalesOrderInvoiceLines(Collection arSalesOrderInvoiceLines);


	/**
	 * @ejb:interface-method view-type="local"
	 * @ejb:relation name="invoice-joborderinvoicelines"
	 *               role-name="invoice-has-many-joborderinvoicelines"
	 */
	public abstract Collection getArJobOrderInvoiceLines();
	public abstract void setArJobOrderInvoiceLines(Collection arJobOrderInvoiceLines);


	/**
	 * @ejb:interface-method view-type="local"
	 * @ejb:relation name="invoicebatch-invoices"
	 *               role-name="invoice-has-one-invoicebatch"
	 *
	 * @jboss:relation related-pk-field="ibCode"
	 *                 fk-column="AR_INVOICE_BATCH"
	 */
	public abstract LocalArInvoiceBatch getArInvoiceBatch();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setArInvoiceBatch(LocalArInvoiceBatch arInvoiceBatch);

	/**
	 * @ejb:interface-method view-type="local"
	 * @ejb:relation name="salesperson-invoices"
	 *               role-name="invoice-has-one-salesperson"
	 *
	 * @jboss:relation related-pk-field="slpCode"
	 *                 fk-column="AR_SALESPERSON"
	 */
	public abstract LocalArSalesperson getArSalesperson();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setArSalesperson(LocalArSalesperson arSalesperson);


	/**
	 * @jboss:dynamic-ql
	 */
	public abstract Collection ejbSelectGeneric(java.lang.String jbossQl, java.lang.Object[] args)
	throws FinderException;


	// BUSINESS METHODS

	/**
	 * @ejb:home-method view-type="local"
	 */
	public Collection ejbHomeGetInvByCriteria(java.lang.String jbossQl, java.lang.Object[] args)
	throws FinderException {

		return ejbSelectGeneric(jbossQl, args);
	}

	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public void addArDistributionRecord(LocalArDistributionRecord arDistributionRecord) {

		Debug.print("ArInvoiceBean addArDistributionRecord");

		try {

			Collection arDistributionRecords = getArDistributionRecords();
			arDistributionRecords.add(arDistributionRecord);

		} catch (Exception ex) {

			throw new EJBException(ex.getMessage());

		}
	}

	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public void dropArDistributionRecord(LocalArDistributionRecord arDistributionRecord) {

		Debug.print("ArInvoiceBean dropArDistributionRecord");

		try {

			Collection arDistributionRecords = getArDistributionRecords();
			arDistributionRecords.remove(arDistributionRecord);

		} catch (Exception ex) {

			throw new EJBException(ex.getMessage());

		}

	}

	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public void addArInvoicePaymentSchedule(LocalArInvoicePaymentSchedule arInvoicePaymentSchedule) {

		Debug.print("ArInvoiceBean addArInvoicePaymentSchedule");

		try {

			Collection arInvoicePaymentSchedules = getArInvoicePaymentSchedules();
			arInvoicePaymentSchedules.add(arInvoicePaymentSchedule);

		} catch (Exception ex) {

			throw new EJBException(ex.getMessage());

		}

	}

	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public void dropArInvoicePaymentSchedule(LocalArInvoicePaymentSchedule arInvoicePaymentSchedule) {

		Debug.print("ArInvoiceBean dropArInvoicePaymentSchedule");

		try {

			Collection arInvoicePaymentSchedules = getArInvoicePaymentSchedules();
			arInvoicePaymentSchedules.remove(arInvoicePaymentSchedule);

		} catch (Exception ex) {

			throw new EJBException(ex.getMessage());

		}

	}

	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public void addArInvoiceLine(LocalArInvoiceLine arInvoiceLine) {

		Debug.print("ArInvoiceBean addArInvoiceLine");

		try {

			Collection arInvoiceLines = getArInvoiceLines();
			arInvoiceLines.add(arInvoiceLine);

		} catch (Exception ex) {

			throw new EJBException(ex.getMessage());

		}

	}

	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public void dropArInvoiceLine(LocalArInvoiceLine arInvoiceLine) {

		Debug.print("ArInvoiceBean dropArInvoiceLine");

		try {

			Collection arInvoiceLines = getArInvoiceLines();
			arInvoiceLines.remove(arInvoiceLine);

		} catch (Exception ex) {

			throw new EJBException(ex.getMessage());

		}

	}

	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public void addArInvoiceLineItem(LocalArInvoiceLineItem arInvoiceLineItem) {

		Debug.print("ArInvoiceBean addArInvoiceLineItem");

		try {

			Collection arInvoiceLineItems = getArInvoiceLineItems();
			arInvoiceLineItems.add(arInvoiceLineItem);

		} catch (Exception ex) {

			throw new EJBException(ex.getMessage());

		}

	}

	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public void dropArInvoiceLineItem(LocalArInvoiceLineItem arInvoiceLineItem) {

		Debug.print("ArInvoiceBean dropArInvoiceLineItem");

		try {

			Collection arInvoiceLineItems = getArInvoiceLineItems();
			arInvoiceLineItems.remove(arInvoiceLineItem);

		} catch (Exception ex) {

			throw new EJBException(ex.getMessage());

		}

	}

	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public void addArSalesOrderInvoiceLine(LocalArSalesOrderInvoiceLine arSalesOrderInvoiceLine) {

		Debug.print("ArInvoiceBean addArSalesOrderInvoiceLine");

		try {

			Collection arSalesOrderInvoiceLines = getArSalesOrderInvoiceLines();
			arSalesOrderInvoiceLines.add(arSalesOrderInvoiceLine);

		} catch (Exception ex) {

			throw new EJBException(ex.getMessage());

		}

	}

	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public void dropArSalesOrderInvoiceLine(LocalArSalesOrderInvoiceLine arSalesOrderInvoiceLine) {

		Debug.print("ArInvoiceBean dropArSalesOrderInvoiceLine");

		try {

			Collection arSalesOrderInvoiceLines = getArSalesOrderInvoiceLines();
			arSalesOrderInvoiceLines.remove(arSalesOrderInvoiceLine);

		} catch (Exception ex) {

			throw new EJBException(ex.getMessage());

		}

	}


	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public short getArDrNextLine() {

		Debug.print("ArInvoiceBean getArDrNextLine");

		try {

			Collection arDistributionRecords = getArDistributionRecords();
			return (short)(arDistributionRecords.size() + 1);

		} catch (Exception ex) {

			throw new EJBException(ex.getMessage());

		}
	}


	// ENTITY METHODS

	/**
	 * @ejb:create-method view-type="local"
	 **/
	public Integer ejbCreate(Integer INV_CODE, String INV_TYP,
			byte INV_CRDT_MMO, String INV_DESC, Date INV_DT, String INV_NMBR,
			String INV_RFRNC_NMBR, String INV_UPLD_NMBR, String INV_CM_INVC_NMBR, String INV_CM_RFRNC_NMBR, double INV_AMNT_DUE, double INV_DWN_PYMNT,
			double INV_AMNT_PD, double INV_PNT_DUE, double INV_PNT_PD, double INV_AMNT_UNEARND_INT, Date INV_CNVRSN_DT, double INV_CNVRSN_RT,
			String INV_MMO,
			double INV_PRV_RDNG, double INV_PRSNT_RDNG,
			String INV_BLL_TO_ADDRSS, String INV_BLL_TO_CNTCT, String INV_BLL_TO_ALT_CNTCT, String INV_BLL_TO_PHN,
			String INV_BLLNG_HDR, String INV_BLLNG_FTR, String INV_BLLNG_HDR2, String INV_BLLNG_FTR2,
			String INV_BLLNG_HDR3, String INV_BLLNG_FTR3, String INV_BLLNG_SGNTRY, String INV_SGNTRY_TTL,
			String INV_SHP_TO_ADDRSS, String INV_SHP_TO_CNTCT, String INV_SHP_TO_ALT_CNTCT, String INV_SHP_TO_PHN,
			Date INV_SHP_DT, String INV_LV_FRGHT, String INV_APPRVL_STATUS, String INV_RSN_FR_RJCTN,
			byte INV_PSTD,
			String INV_VD_APPRVL_STATUS, byte INV_VD_PSTD,byte INV_VD,
			byte INV_CONT, byte INV_DSBL_INTRST,
			byte INV_INTRST, String INV_INTRST_RFRNC_NMBR, double INV_INTRST_AMNT, String INV_INTRST_CRTD_BY, Date INV_INTRST_DT_CRTD,
			Date INV_INTRST_NXT_RN_DT, Date INV_INTRST_LST_RN_DT,
			String INV_CRTD_BY, Date INV_DT_CRTD,
			String INV_LST_MDFD_BY, Date INV_DT_LST_MDFD, String INV_APPRVD_RJCTD_BY,
			Date INV_DT_APPRVD_RJCTD, String INV_PSTD_BY, Date INV_DT_PSTD, byte INV_PRNTD, String INV_LV_SHFT,
			String INV_SO_NMBR, String INV_JO_NMBR, byte INV_DBT_MEMO, byte INV_SBJCT_TO_CMMSSN, String INV_CLNT_PO,
			Date INV_EFFCTVTY_DT, Integer INV_AD_BRNCH, Integer INV_AD_CMPNY)
	throws CreateException {

		Debug.print("ArInvoiceBean ejbCreate");
		Debug.print("INV_AD BRNCH" + INV_AD_BRNCH);

		setInvCode(INV_CODE);
		setInvType(INV_TYP);
		setInvCreditMemo(INV_CRDT_MMO);
		setInvDescription(INV_DESC);
		setInvDate(INV_DT);
		setInvNumber(INV_NMBR);
		setInvReferenceNumber(INV_RFRNC_NMBR);
		setInvUploadNumber(INV_UPLD_NMBR);
		setInvCmInvoiceNumber(INV_CM_INVC_NMBR);
		setInvCmReferenceNumber(INV_CM_RFRNC_NMBR);
		setInvAmountDue(INV_AMNT_DUE);
		setInvDownPayment(INV_DWN_PYMNT);
		setInvAmountPaid(INV_AMNT_PD);
		setInvPenaltyDue(INV_PNT_DUE);
		setInvPenaltyPaid(INV_PNT_PD);
		setInvPenaltyDue(INV_PNT_DUE);
		setInvAmountUnearnedInterest(INV_AMNT_UNEARND_INT);
		setInvConversionDate(INV_CNVRSN_DT);
		setInvConversionRate(INV_CNVRSN_RT);
		setInvMemo(INV_MMO);
		setInvPreviousReading(INV_PRV_RDNG);
		setInvPresentReading(INV_PRSNT_RDNG);
		setInvBillToAddress(INV_BLL_TO_ADDRSS);
		setInvBillToContact(INV_BLL_TO_CNTCT);
		setInvBillToAltContact(INV_BLL_TO_ALT_CNTCT);
		setInvBillToPhone(INV_BLL_TO_PHN);
		setInvBillingHeader(INV_BLLNG_HDR);
		setInvBillingFooter(INV_BLLNG_FTR);
		setInvBillingHeader2(INV_BLLNG_HDR2);
		setInvBillingFooter2(INV_BLLNG_FTR2);
		setInvBillingHeader3(INV_BLLNG_HDR3);
		setInvBillingFooter3(INV_BLLNG_FTR3);
		setInvBillingSignatory(INV_BLLNG_SGNTRY);
		setInvSignatoryTitle(INV_SGNTRY_TTL);
		setInvShipToAddress(INV_SHP_TO_ADDRSS);
		setInvShipToContact(INV_SHP_TO_CNTCT);
		setInvShipToAltContact(INV_SHP_TO_ALT_CNTCT);
		setInvShipToPhone(INV_SHP_TO_PHN);
		setInvShipDate(INV_SHP_DT);
		setInvLvFreight(INV_LV_FRGHT);
		setInvApprovalStatus(INV_APPRVL_STATUS);
		setInvReasonForRejection(INV_RSN_FR_RJCTN);
		setInvPosted(INV_PSTD);
		setInvVoidApprovalStatus(INV_VD_APPRVL_STATUS);
		setInvVoidPosted(INV_VD_PSTD);
		setInvVoid(INV_VD);
		setInvContention(INV_CONT);
		setInvDisableInterest(INV_DSBL_INTRST);
		setInvInterest(INV_INTRST);
		setInvInterestReferenceNumber(INV_INTRST_RFRNC_NMBR);
		setInvInterestAmount(INV_INTRST_AMNT);
		setInvInterestCreatedBy(INV_INTRST_CRTD_BY);
		setInvInterestDateCreated(INV_INTRST_DT_CRTD);
		setInvInterestNextRunDate(INV_INTRST_NXT_RN_DT);
		setInvInterestLastRunDate(INV_INTRST_LST_RN_DT);

		setInvCreatedBy(INV_CRTD_BY);
		setInvDateCreated(INV_DT_CRTD);
		setInvLastModifiedBy(INV_LST_MDFD_BY);
		setInvDateLastModified(INV_DT_LST_MDFD);
		setInvApprovedRejectedBy(INV_APPRVD_RJCTD_BY);
		setInvDateApprovedRejected(INV_DT_APPRVD_RJCTD);
		setInvPostedBy(INV_PSTD_BY);
		setInvDatePosted(INV_DT_PSTD);
		setInvPrinted(INV_PRNTD);
		setInvLvShift(INV_LV_SHFT);
		setInvJoNumber(INV_JO_NMBR);
		setInvSoNumber(INV_SO_NMBR);
		setInvDebitMemo(INV_DBT_MEMO);
		setInvSubjectToCommission(INV_SBJCT_TO_CMMSSN);
		setInvClientPO(INV_CLNT_PO);
		setInvEffectivityDate(INV_EFFCTVTY_DT);
		setInvAdBranch(INV_AD_BRNCH);
		setInvAdCompany(INV_AD_CMPNY);

		return null;

	}

	/**
	 * @ejb:create-method view-type="local"
	 **/
	public Integer ejbCreate( String INV_TYP,
			byte INV_CRDT_MMO, String INV_DESC, Date INV_DT, String INV_NMBR,
			String INV_RFRNC_NMBR, String INV_UPLD_NMBR, String INV_CM_INVC_NMBR, String INV_CM_RFRNC_NMBR,  double INV_AMNT_DUE, double INV_DWN_PYMNT,
			double INV_AMNT_PD, double INV_PNT_DUE, double INV_PNT_PD, double INV_AMNT_UNEARND_INT, Date INV_CNVRSN_DT, double INV_CNVRSN_RT,
			String INV_MMO,
			double INV_PRV_RDNG, double INV_PRSNT_RDNG,
			String INV_BLL_TO_ADDRSS, String INV_BLL_TO_CNTCT, String INV_BLL_TO_ALT_CNTCT, String INV_BLL_TO_PHN,
			String INV_BLLNG_HDR, String INV_BLLNG_FTR, String INV_BLLNG_HDR2, String INV_BLLNG_FTR2,
			String INV_BLLNG_HDR3, String INV_BLLNG_FTR3, String INV_BLLNG_SGNTRY, String INV_SGNTRY_TTL,
			String INV_SHP_TO_ADDRSS, String INV_SHP_TO_CNTCT, String INV_SHP_TO_ALT_CNTCT, String INV_SHP_TO_PHN,
			Date INV_SHP_DT, String INV_LV_FRGHT, String INV_APPRVL_STATUS, String INV_RSN_FR_RJCTN,
			byte INV_PSTD,
			String INV_VD_APPRVL_STATUS, byte INV_VD_PSTD, byte INV_VD,
			byte INV_CONT, byte INV_DSBL_INTRST,
			byte INV_INTRST, String INV_INTRST_RFRNC_NMBR, double INV_INTRST_AMNT, String INV_INTRST_CRTD_BY, Date INV_INTRST_DT_CRTD,
			Date INV_INTRST_NXT_RN_DT, Date INV_INTRST_LST_RN_DT,
			String INV_CRTD_BY, Date INV_DT_CRTD,
			String INV_LST_MDFD_BY, Date INV_DT_LST_MDFD, String INV_APPRVD_RJCTD_BY,
			Date INV_DT_APPRVD_RJCTD, String INV_PSTD_BY, Date INV_DT_PSTD, byte INV_PRNTD, String INV_LV_SHFT,
			String INV_SO_NMBR, String INV_JO_NMBR, byte INV_DBT_MEMO, byte INV_SBJCT_TO_CMMSSN, String INV_CLNT_PO,
			Date INV_EFFCTVTY_DT, Integer INV_AD_BRNCH,  Integer INV_AD_CMPNY)
	throws CreateException {

		Debug.print("ArInvoiceBean ejbCreate");
		Debug.print("INV_AD BRNCH =" + INV_AD_BRNCH );

		setInvType(INV_TYP);
		setInvCreditMemo(INV_CRDT_MMO);
		setInvDescription(INV_DESC);
		setInvDate(INV_DT);
		setInvNumber(INV_NMBR);
		setInvReferenceNumber(INV_RFRNC_NMBR);
		setInvUploadNumber(INV_UPLD_NMBR);
		setInvCmInvoiceNumber(INV_CM_INVC_NMBR);
		setInvCmReferenceNumber(INV_CM_RFRNC_NMBR);
		setInvAmountDue(INV_AMNT_DUE);
		setInvDownPayment(INV_DWN_PYMNT);
		setInvAmountPaid(INV_AMNT_PD);
		setInvPenaltyDue(INV_PNT_DUE);
		setInvPenaltyPaid(INV_PNT_PD);
		setInvAmountUnearnedInterest(INV_AMNT_UNEARND_INT);
		setInvConversionDate(INV_CNVRSN_DT);
		setInvConversionRate(INV_CNVRSN_RT);
		setInvMemo(INV_MMO);
		setInvPreviousReading(INV_PRV_RDNG);
		setInvPresentReading(INV_PRSNT_RDNG);
		setInvBillToAddress(INV_BLL_TO_ADDRSS);
		setInvBillToContact(INV_BLL_TO_CNTCT);
		setInvBillToAltContact(INV_BLL_TO_ALT_CNTCT);
		setInvBillToPhone(INV_BLL_TO_PHN);
		setInvBillingHeader(INV_BLLNG_HDR);
		setInvBillingFooter(INV_BLLNG_FTR);
		setInvBillingHeader2(INV_BLLNG_HDR2);
		setInvBillingFooter2(INV_BLLNG_FTR2);
		setInvBillingHeader3(INV_BLLNG_HDR3);
		setInvBillingFooter3(INV_BLLNG_FTR3);
		setInvBillingSignatory(INV_BLLNG_SGNTRY);
		setInvSignatoryTitle(INV_SGNTRY_TTL);
		setInvShipToAddress(INV_SHP_TO_ADDRSS);
		setInvShipToContact(INV_SHP_TO_CNTCT);
		setInvShipToAltContact(INV_SHP_TO_ALT_CNTCT);
		setInvShipToPhone(INV_SHP_TO_PHN);
		setInvShipDate(INV_SHP_DT);
		setInvLvFreight(INV_LV_FRGHT);
		setInvApprovalStatus(INV_APPRVL_STATUS);
		setInvReasonForRejection(INV_RSN_FR_RJCTN);
		setInvPosted(INV_PSTD);
		setInvVoidApprovalStatus(INV_VD_APPRVL_STATUS);
		setInvVoidPosted(INV_VD_PSTD);
		setInvVoid(INV_VD);
		setInvContention(INV_CONT);
		setInvDisableInterest(INV_DSBL_INTRST);
		setInvInterest(INV_INTRST);
		setInvInterestReferenceNumber(INV_INTRST_RFRNC_NMBR);
		setInvInterestAmount(INV_INTRST_AMNT);
		setInvInterestCreatedBy(INV_INTRST_CRTD_BY);
		setInvInterestDateCreated(INV_INTRST_DT_CRTD);
		setInvInterestNextRunDate(INV_INTRST_NXT_RN_DT);
		setInvInterestLastRunDate(INV_INTRST_LST_RN_DT);

		setInvCreatedBy(INV_CRTD_BY);
		setInvDateCreated(INV_DT_CRTD);
		setInvLastModifiedBy(INV_LST_MDFD_BY);
		setInvDateLastModified(INV_DT_LST_MDFD);
		setInvApprovedRejectedBy(INV_APPRVD_RJCTD_BY);
		setInvDateApprovedRejected(INV_DT_APPRVD_RJCTD);
		setInvPostedBy(INV_PSTD_BY);
		setInvDatePosted(INV_DT_PSTD);
		setInvPrinted(INV_PRNTD);
		setInvLvShift(INV_LV_SHFT);
		setInvSoNumber(INV_SO_NMBR);
		setInvJoNumber(INV_JO_NMBR);
		setInvDebitMemo(INV_DBT_MEMO);
		setInvSubjectToCommission(INV_SBJCT_TO_CMMSSN);
		setInvClientPO(INV_CLNT_PO);
		setInvEffectivityDate(INV_EFFCTVTY_DT);
		setInvAdBranch(INV_AD_BRNCH);
		setInvAdCompany(INV_AD_CMPNY);

		return null;

	}


	public void ejbPostCreate(Integer INV_CODE, String INV_TYP,
			byte INV_CRDT_MMO, String INV_DESC, Date INV_DT, String INV_NMBR,
			String INV_RFRNC_NMBR, String INV_UPLD_NMBR, String INV_CM_INVC_NMBR, String INV_CM_RFRNC_NMBR, double INV_AMNT_DUE, double INV_DWN_PYMNT,
			double INV_AMNT_PD, double INV_PNT_DUE, double INV_PNT_PD, double INV_AMNT_UNEARND_INT, Date INV_CNVRSN_DT, double INV_CNVRSN_RT,
			String INV_MMO, double INV_PRV_RDNG, double INV_PRSNT_RDNG,
			String INV_BLL_TO_ADDRSS, String INV_BLL_TO_CNTCT, String INV_BLL_TO_ALT_CNTCT, String INV_BLL_TO_PHN,
			String INV_BLLNG_HDR, String INV_BLLNG_FTR, String INV_BLLNG_HDR2, String INV_BLLNG_FTR2,
			String INV_BLLNG_HDR3, String INV_BLLNG_FTR3, String INV_BLLNG_SGNTRY, String INV_SGNTRY_TTL,
			String INV_SHP_TO_ADDRSS, String INV_SHP_TO_CNTCT, String INV_SHP_TO_ALT_CNTCT, String INV_SHP_TO_PHN,
			Date INV_SHP_DT, String INV_LV_FRGHT, String INV_APPRVL_STATUS, String INV_RSN_FR_RJCTN,
			byte INV_PSTD,
			String INV_VD_APPRVL_STATUS, byte INV_VD_PSTD, byte INV_VD,
			byte INV_CONT, byte INV_DSBL_INTRST,
			byte INV_INTRST, String INV_INTRST_RFRNC_NMBR, double INV_INTRST_AMNT, String INV_INTRST_CRTD_BY, Date INV_INTRST_DT_CRTD,
			Date INV_INTRST_NXT_RN_DT, Date INV_INTRST_LST_RN_DT,
			String INV_CRTD_BY, Date INV_DT_CRTD,

			String INV_LST_MDFD_BY, Date INV_DT_LST_MDFD, String INV_APPRVD_RJCTD_BY,
			Date INV_DT_APPRVD_RJCTD, String INV_PSTD_BY, Date INV_DT_PSTD, byte INV_PRNTD, String INV_LV_SHFT,
			String INV_SO_NMBR, String INV_JO_NMBR, byte INV_DBT_MEMO, byte INV_SBJCT_TO_CMMSSN,  String INV_CLNT_PO,
			Date INV_EFFCTVTY_DT, Integer INV_AD_BRNCH, Integer INV_AD_CMPNY)
	throws CreateException { }

	public void ejbPostCreate( String INV_TYP,
			byte INV_CRDT_MMO, String INV_DESC, Date INV_DT, String INV_NMBR,
			String INV_RFRNC_NMBR, String INV_UPLD_NMBR, String INV_CM_INVC_NMBR, String INV_CM_RFRNC_NMBR, double INV_AMNT_DUE, double INV_DWN_PYMNT,
			double INV_AMNT_PD, double INV_PNT_DUE, double INV_PNT_PD, double INV_AMNT_UNEARND_INT, Date INV_CNVRSN_DT, double INV_CNVRSN_RT,
			String INV_MMO, double INV_PRV_RDNG, double INV_PRSNT_RDNG,
			String INV_BLL_TO_ADDRSS, String INV_BLL_TO_CNTCT, String INV_BLL_TO_ALT_CNTCT, String INV_BLL_TO_PHN,
			String INV_BLLNG_HDR, String INV_BLLNG_FTR, String INV_BLLNG_HDR2, String INV_BLLNG_FTR2,
			String INV_BLLNG_HDR3, String INV_BLLNG_FTR3, String INV_BLLNG_SGNTRY, String INV_SGNTRY_TTL,
			String INV_SHP_TO_ADDRSS, String INV_SHP_TO_CNTCT, String INV_SHP_TO_ALT_CNTCT, String INV_SHP_TO_PHN,
			Date INV_SHP_DT, String INV_LV_FRGHT, String INV_APPRVL_STATUS, String INV_RSN_FR_RJCTN,
			byte INV_PSTD,
			String INV_VD_APPRVL_STATUS, byte INV_VD_PSTD, byte INV_VD,
			byte INV_CONT, byte INV_DSBL_INTRST,
			byte INV_INTRST, String INV_INTRST_RFRNC_NMBR, double INV_INTRST_AMNT, String INV_INTRST_CRTD_BY, Date INV_INTRST_DT_CRTD,
			Date INV_INTRST_NXT_RN_DT, Date INV_INTRST_LST_RN_DT,
			String INV_CRTD_BY, Date INV_DT_CRTD,
			String INV_LST_MDFD_BY, Date INV_DT_LST_MDFD, String INV_APPRVD_RJCTD_BY,
			Date INV_DT_APPRVD_RJCTD, String INV_PSTD_BY, Date INV_DT_PSTD, byte INV_PRNTD, String INV_LV_SHFT,
			String INV_SO_NMBR, String INV_JO_NMBR, byte INV_DBT_MEMO, byte INV_SBJCT_TO_CMMSSN, String INV_CLNT_PO,
			Date INV_EFFCTVTY_DT, Integer INV_AD_BRNCH, Integer INV_AD_CMPNY)
	throws CreateException { }

}

