/*
 * com/ejb/ar/LocalArInvoiceLineBean.java
 *
 * Created on March 03, 2004, 3:39 PM
 */

package com.ejb.ar;

import javax.ejb.CreateException;

import com.util.AbstractEntityBean;
import com.util.Debug;

/**
 *
 * @author  Neil Andrew M. Ajero
 *
 * @ejb:bean name="ArInvoiceLineEJB"
 *           display-name="InvoiceLine Entity"
 *           type="CMP"
 *           cmp-version="2.x"
 *           view-type="local"
 *           local-jndi-name="omega-ejb/ArInvoiceLineEJB"
 *           schema="ArInvoiceLine"
 *           primkey-field="ilCode"
 *
 * @ejb:pk class="java.lang.Integer"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="aruser"
 *                        role-link="aruserlink"
 *
 * @ejb:permission role-name="aruser"
 *
 * @ejb:interface local-class="com.ejb.ar.LocalArInvoiceLine"
 *                local-extends="javax.ejb.EJBLocalObject"
 *
 * @ejb:home local-class="com.ejb.ar.LocalArInvoiceLineHome"
 *           local-extends="javax.ejb.EJBLocalHome"
 *
 * @ejb:finder signature="Collection findByAdCompanyAll(java.lang.Integer INV_AD_CMPNY)"
 *			   query="SELECT OBJECT(il) FROM ArInvoiceLine il WHERE il.ilAdCompany = ?1"
 *
 * @ejb:finder signature="Collection findInvoiceLineByInvCodeAndAdCompany(java.lang.Integer INV_CODE, java.lang.Integer INV_AD_CMPNY)"
 *			   query="SELECT OBJECT(il) FROM ArInvoiceLine il WHERE il.arInvoice.invCode = ?1 AND il.ilAdCompany = ?2"
 *
 * @ejb:finder signature="Collection findInvoiceLineByRctCodeAndAdCompany(java.lang.Integer RCT_CODE, java.lang.Integer INV_AD_CMPNY)"
 *			   query="SELECT OBJECT(il) FROM ArInvoiceLine il WHERE il.arReceipt.rctCode = ?1 AND il.ilAdCompany = ?2"
 *
 * @ejb:value-object match="*"
 *             name="ArInvoiceLine"
 *
 * @jboss:persistence table-name="AR_INVC_LN"
 *
 * @jboss:entity-command name="mysql-get-generated-keys"
 */

public abstract class ArInvoiceLineBean extends AbstractEntityBean {
    
    
    // PERSITCENT METHODS
    
    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     * @ejb:pk-field
     *
     * @jboss:column-name name="IL_CODE"
     *
     * @jboss:persistence auto-increment="true"
     **/
    public abstract Integer getIlCode();
    public abstract void setIlCode(Integer IL_CODE);
    
    
    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"   
     *
     * @jboss:column-name name="IL_DESC"
     **/
    public abstract String getIlDescription();
     /**
      * @ejb:interface-method view-type="local"
      **/ 
    public abstract void setIlDescription(String IL_DESC);
    

    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"   
     *
     * @jboss:column-name name="IL_QNTTY"
     **/
    public abstract double getIlQuantity();
     /**
      * @ejb:interface-method view-type="local"
      **/ 
    public abstract void setIlQuantity(double IL_QNTTY);

    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"   
     *
     * @jboss:column-name name="IL_UNT_PRC"
     **/
    public abstract double getIlUnitPrice();
     /**
      * @ejb:interface-method view-type="local"
      **/ 
    public abstract void setIlUnitPrice(double IL_UNT_PRC);

    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"   
     *
     * @jboss:column-name name="IL_AMNT"
     **/
    public abstract double getIlAmount();
     /**
      * @ejb:interface-method view-type="local"
      **/ 
    public abstract void setIlAmount(double IL_AMNT);
    
    
    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"   
     *
     * @jboss:column-name name="IL_TX_AMNT"
     **/
    public abstract double getIlTaxAmount();
     /**
      * @ejb:interface-method view-type="local"
      **/ 
    public abstract void setIlTaxAmount(double IL_TX_AMNT);

    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"   
     *
     * @jboss:column-name name="IL_TX"
     **/
    public abstract byte getIlTax();
     /**
      * @ejb:interface-method view-type="local"
      **/ 
    public abstract void setIlTax(byte IL_TX);
    
    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"   
     *
     * @jboss:column-name name="IL_AD_CMPNY"
     **/
    public abstract Integer getIlAdCompany();
     /**
      * @ejb:interface-method view-type="local"
      **/ 
    public abstract void setIlAdCompany(Integer IL_AD_CMPNY);
        
   
    // RELATIONSHIP METHODS

    /**
     * @ejb:interface-method view-type="local"
     * @ejb:relation name="receipt-invoicelines"
     *               role-name="invoiceline-has-one-receipt"
     * 
     * @jboss:relation related-pk-field="rctCode"
     *                 fk-column="AR_RECEIPT"
     */
    public abstract LocalArReceipt getArReceipt();
    /**
     * @ejb:interface-method view-type="local"
     **/ 
    public abstract void setArReceipt(LocalArReceipt arReceipt);

    /**
     * @ejb:interface-method view-type="local"
     * @ejb:relation name="invoice-invoicelines"
     *               role-name="invoiceline-has-one-invoice"
     *               cascade-delete="yes"
     * 
     * @jboss:relation related-pk-field="invCode"
     *                 fk-column="AR_INVOICE"
     */
    public abstract LocalArInvoice getArInvoice();
    /**
     * @ejb:interface-method view-type="local"
     **/ 
    public abstract void setArInvoice(LocalArInvoice arInvoice); 

    /**
     * @ejb:interface-method view-type="local"
     * @ejb:relation name="standardmemoline-invoicelines"
     *               role-name="invoiceline-has-one-standardmemoline"
     * 
     * @jboss:relation related-pk-field="smlCode"
     *                 fk-column="AR_STANDARD_MEMO_LINE"
     */
    public abstract LocalArStandardMemoLine getArStandardMemoLine();
    /**
     * @ejb:interface-method view-type="local"
     **/ 
    public abstract void setArStandardMemoLine(LocalArStandardMemoLine arStandardMemoLine);
    
    /**
     * @ejb:interface-method view-type="local"
     * @ejb:relation name="pdc-invoicelines"
     *               role-name="invoiceline-has-one-pdc"
     *               cascade-delete="yes"
     * 
     * @jboss:relation related-pk-field="pdcCode"
     *                 fk-column="AR_PDC"
     */
    public abstract LocalArPdc getArPdc();
    public abstract void setArPdc(LocalArPdc arPdc);    


    // NO BUSINESS METHODS

    // ENTITY METHODS
    
    /**
     * @ejb:create-method view-type="local"
     **/
    public Integer ejbCreate(Integer IL_CODE, 
         String IL_DESC, double IL_QNTTY, 
         double IL_UNT_PRC, double IL_AMNT, double IL_TX_AMNT, byte IL_TX,
		 Integer IL_AD_CMPNY)     	
         throws CreateException {	
     	
         Debug.print("ArInvoiceLineBean ejbCreate");
     
	     setIlCode(IL_CODE);
	     setIlDescription(IL_DESC);
	     setIlQuantity(IL_QNTTY); 
	     setIlUnitPrice(IL_UNT_PRC);
	     setIlAmount(IL_AMNT);  
	     setIlTaxAmount(IL_TX_AMNT);
	     setIlTax(IL_TX);	    
	     setIlAdCompany(IL_AD_CMPNY);

         return null;
     }
     
    /**
     * @ejb:create-method view-type="local"
     **/
    public Integer ejbCreate( 
         String IL_DESC, double IL_QNTTY, 
         double IL_UNT_PRC, double IL_AMNT, double IL_TX_AMNT, byte IL_TX,
		 Integer IL_AD_CMPNY)     	
         throws CreateException {	
     	
         Debug.print("ArInvoiceLineBean ejbCreate");
     
	     setIlDescription(IL_DESC);
	     setIlQuantity(IL_QNTTY); 
	     setIlUnitPrice(IL_UNT_PRC);
	     setIlAmount(IL_AMNT);  
	     setIlTaxAmount(IL_TX_AMNT);
	     setIlTax(IL_TX); 
	     setIlAdCompany(IL_AD_CMPNY);

         return null;
     }
     
     public void ejbPostCreate(Integer IL_CODE, 
         String IL_DESC, double IL_QNTTY, 
         double IL_UNT_PRC, double IL_AMNT, double IL_TX_AMNT, byte IL_TX,
		 Integer IL_AD_CMPNY)      
         throws CreateException { }
      
     public void ejbPostCreate(
         String IL_DESC, double IL_QNTTY, 
         double IL_UNT_PRC, double IL_AMNT, double IL_TX_AMNT, byte IL_TX,
		 Integer IL_AD_CMPNY)
         throws CreateException { }     
}