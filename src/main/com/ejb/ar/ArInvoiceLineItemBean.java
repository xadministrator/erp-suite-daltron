package com.ejb.ar;

import java.util.Collection;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;

import com.ejb.inv.LocalInvCosting;
import com.ejb.inv.LocalInvItemLocation;
import com.ejb.inv.LocalInvUnitOfMeasure;
import com.util.AbstractEntityBean;
import com.util.Debug;

/**
 * @ejb:bean name="ArInvoiceLineItemEJB"
 *           display-name="Invoice Line Item Entity"
 *           type="CMP"
 *           cmp-version="2.x"
 *           view-type="local"
 *           local-jndi-name="omega-ejb/ArInvoiceLineItemEJB"
 *           schema="ArInvoiceLineItem"
 *           primkey-field="iliCode"
 *
 * @ejb:pk class="java.lang.Integer"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="aruser"
 *                        role-link="aruserlink"
 *
 * @ejb:permission role-name="aruser"
 *
 * @ejb:interface local-class="com.ejb.ar.LocalArInvoiceLineItem"
 *                local-extends="javax.ejb.EJBLocalObject"
 *
 * @ejb:home local-class="com.ejb.ar.LocalArInvoiceLineItemHome"
 *           local-extends="javax.ejb.EJBLocalHome"
 *
 * @ejb:select signature="Collection ejbSelectGeneric(java.lang.String jbossQl, java.lang.Object[] args)"
 *             result-type-mapping="Local"
 *             method-intf="LocalHome"
 *             query="SELECT OBJECT(ili) FROM ArInvoiceLineItem ili"
 *
 * @ejb:finder signature = "Collection findByInvCode(java.lang.Integer INV_CODE, java.lang.Integer ILI_AD_CMPNY)"
 * 				query = "SELECT OBJECT(ili) FROM ArInvoiceLineItem ili WHERE ili.arInvoice.invCode=?1 AND ili.iliAdCompany=?2"
 *
 * @jboss:query signature = "Collection findByInvCode(java.lang.Integer INV_CODE, java.lang.Integer ILI_AD_CMPNY)"
 * 				query = "SELECT OBJECT(ili) FROM ArInvoiceLineItem ili WHERE ili.arInvoice.invCode=?1 AND ili.iliAdCompany=?2 ORDER BY ili.iliLine"
 *
 * @ejb:finder signature = "Collection findByInvCodeAndIlIiLvCtgry(java.lang.Integer INV_CODE, java.lang.String IL_II_LV_CTGRY, java.lang.Integer ILI_AD_CMPNY)"
 * 				query = "SELECT OBJECT(ili) FROM ArInvoiceLineItem ili WHERE ili.arInvoice.invCode=?1 AND ili.invItemLocation.invItem.iiAdLvCategory=?2 AND ili.iliAdCompany=?3"
 *
 * @ejb:finder signature = "LocalArInvoiceLineItem findByRctCodeAndIlCode(java.lang.Integer RCT_CODE, java.lang.Integer IL_CODE, java.lang.Integer ILI_AD_CMPNY)"
 * 				query = "SELECT OBJECT(ili) FROM ArInvoiceLineItem ili WHERE ili.arReceipt.rctCode=?1 AND ili.invItemLocation.ilCode=?2 AND ili.iliAdCompany=?3"
 *
 * @ejb:finder signature = "LocalArInvoiceLineItem findByRctCodeAndIlCodeAndUomName(java.lang.Integer RCT_CODE, java.lang.Integer IL_CODE, java.lang.String UOM_NM, java.lang.Integer ILI_AD_CMPNY)"
 * 				query = "SELECT OBJECT(ili) FROM ArInvoiceLineItem ili WHERE ili.arReceipt.rctCode=?1 AND ili.invItemLocation.ilCode=?2 AND ili.invUnitOfMeasure.uomName=?3 AND ili.iliAdCompany=?4"
 *
 * @ejb:finder signature = "Collection findByRctPostedAndIlCodeAndBrCode(byte RCT_PSTD, java.lang.Integer IL_CODE, java.lang.Integer RCT_AD_BRNCH, java.lang.Integer ILI_AD_CMPNY)"
 * 				query = "SELECT OBJECT(ili) FROM ArInvoiceLineItem ili WHERE ili.arReceipt.rctPosted=?1 AND ili.invItemLocation.ilCode=?2 AND ili.arReceipt.rctAdBranch=?3 AND ili.iliAdCompany=?4"
 *
 * @ejb:finder signature = "Collection findByInvCreditMemoAndInvPostedAndIlCodeAndBrCode(byte INV_CRDT_MM, byte INV_PSTD, java.lang.Integer IL_CODE, java.lang.Integer INV_AD_BRNCH, java.lang.Integer ILI_AD_CMPNY)"
 * 				query = "SELECT OBJECT(ili) FROM ArInvoiceLineItem ili WHERE ili.arInvoice.invCreditMemo=?1 AND ili.arInvoice.invPosted=?2 AND ili.invItemLocation.ilCode=?3 AND ili.arInvoice.invAdBranch=?4 AND ili.iliAdCompany=?5"
 *
 * @ejb:finder signature = "Collection findByAdCompanyAll(java.lang.Integer ILI_AD_CMPNY)"
 * 				query = "SELECT OBJECT(ili) FROM ArInvoiceLineItem ili WHERE ili.iliAdCompany=?1"
 *
 * @ejb:finder signature="Collection findiliByRctCodeAndAdCompany(java.lang.Integer RCT_CODE, java.lang.Integer INV_AD_CMPNY)"
 *			   query="SELECT OBJECT(ili) FROM ArInvoiceLineItem ili WHERE ili.arReceipt.rctCode = ?1 AND ili.iliAdCompany = ?2"
 *
 * @ejb:finder signature="Collection findiliByInvCodeAndAdCompany(java.lang.Integer INV_CODE, java.lang.Integer INV_AD_CMPNY)"
 *			   query="SELECT OBJECT(ili) FROM ArInvoiceLineItem ili WHERE ili.arInvoice.invCode = ?1 AND ili.iliAdCompany = ?2"
 *
 * @ejb:finder signature="Collection findByInvNumberAndInvCreditMemoAndInvPostedAndIlCodeAndBrCode(java.lang.Integer INV_CODE, byte INV_CRDT_MM, byte INV_PSTD, java.lang.Integer IL_CODE, java.lang.Integer INV_AD_BRNCH, java.lang.Integer ILI_AD_CMPNY)"
 * 				query="SELECT OBJECT(ili) FROM ArInvoiceLineItem ili WHERE ili.arInvoice.invCode = ?1 AND ili.arInvoice.invCreditMemo=?2 AND ili.arInvoice.invPosted=?3 AND ili.invItemLocation.ilCode=?4 AND ili.arInvoice.invAdBranch=?5 AND ili.iliAdCompany=?6"
 *
 * @jboss:query signature="Collection findByInvNumberAndInvCreditMemoAndInvPostedAndIlCodeAndBrCode(java.lang.Integer INV_CODE, byte INV_CRDT_MM, byte INV_PSTD, java.lang.Integer IL_CODE, java.lang.Integer INV_AD_BRNCH, java.lang.Integer ILI_AD_CMPNY)"
 * 				query="SELECT OBJECT(ili) FROM ArInvoiceLineItem ili WHERE ili.arInvoice.invCode = ?1 AND ili.arInvoice.invCreditMemo=?2 AND ili.arInvoice.invPosted=?3 AND ili.invItemLocation.ilCode=?4 AND ili.arInvoice.invAdBranch=?5 AND ili.iliAdCompany=?6 ORDER BY ili.iliLine"
 *
 *
 * @ejb:finder signature="Collection findUnpostedInvcByIiNameAndLocNameAndLessEqualDateAndInvAdBranch(java.lang.String II_NM, java.lang.String LOC_NM, java.util.Date INV_DT, java.lang.Integer INV_AD_BRNCH, java.lang.Integer ILI_AD_CMPNY)"
 * 			   query="SELECT OBJECT(ili) FROM ArInvoiceLineItem ili WHERE ili.arInvoice.invPosted = 0 AND ili.arInvoice.invVoid = 0 AND ili.invItemLocation.invItem.iiName = ?1 AND ili.invItemLocation.invLocation.locName = ?2 AND ili.arInvoice.invDate <= ?3 AND ili.arInvoice.invAdBranch=?4 AND ili.iliAdCompany = ?5"
 *
 * @ejb:finder signature="Collection findUnpostedRctByIiNameAndLocNameAndLessEqualDateAndRctAdBranch(java.lang.String II_NM, java.lang.String LOC_NM, java.util.Date RCT_DT, java.lang.Integer RCT_AD_BRNCH, java.lang.Integer ILI_AD_CMPNY)"
 * 			   query="SELECT OBJECT(ili) FROM ArInvoiceLineItem ili WHERE ili.arReceipt.rctPosted = 0 AND ili.arReceipt.rctVoid = 0 AND ili.invItemLocation.invItem.iiName = ?1 AND ili.invItemLocation.invLocation.locName = ?2 AND ili.arReceipt.rctDate <= ?3 AND ili.arReceipt.rctAdBranch=?4 AND ili.iliAdCompany = ?5"
 *
 *
 * @ejb:finder signature="Collection findUnpostedInvcByLocNameAndAdBranch(java.lang.String LOC_NM, java.lang.Integer INV_AD_BRNCH, java.lang.Integer ILI_AD_CMPNY)"
 * 			   query="SELECT OBJECT(ili) FROM ArInvoiceLineItem ili WHERE ili.arInvoice.invPosted = 0 AND ili.arInvoice.invVoid = 0 AND ili.invItemLocation.invLocation.locName = ?1 AND ili.arInvoice.invAdBranch = ?2 AND ili.iliAdCompany = ?3"
 *
 * @ejb:finder signature="Collection findInvcByIiCodeAndLocNameAndAdBranch(java.lang.Integer II_CODE, java.lang.String LOC_NM, java.lang.Integer INV_AD_BRNCH, java.lang.Integer ILI_AD_CMPNY)"
 * 			   query="SELECT OBJECT(ili) FROM ArInvoiceLineItem ili WHERE ili.arInvoice.invVoid = 0 AND ili.invItemLocation.invItem.iiCode=?1 AND ili.invItemLocation.invLocation.locName = ?2 AND ili.arInvoice.invAdBranch = ?3 AND ili.iliAdCompany = ?4"
 *
 * @ejb:finder signature="Collection findUnpostedRctByLocNameAndAdBranch(java.lang.String LOC_NM, java.lang.Integer RCT_AD_BRNCH, java.lang.Integer ILI_AD_CMPNY)"
 * 			   query="SELECT OBJECT(ili) FROM ArInvoiceLineItem ili WHERE ili.arReceipt.rctPosted = 0 AND ili.arReceipt.rctVoid = 0 AND ili.invItemLocation.invLocation.locName = ?1 AND ili.arReceipt.rctAdBranch = ?2 AND ili.iliAdCompany = ?3"
 *
 *
 * @jboss:query signature="Collection ejbSelectGeneric(java.lang.String jbossQl, java.lang.Object[] args)"
 *                         dynamic="true"
 *
 * @ejb:value-object match="*"
 *             name="ArInvoiceLineItem"
 *
 * @jboss:persistence table-name="AR_INVC_LN_ITM"
 *
 * @jboss:entity-command name="mysql-get-generated-keys"
 *
 */

public abstract class ArInvoiceLineItemBean extends AbstractEntityBean {

//	 Access methods for persistent fields

	   /**
	    * @ejb:persistent-field
	    * @ejb:interface-method view-type="local"
	    * @ejb:pk-field
	    *
	    * @jboss:column-name name="ILI_CODE"
	    *
	    * @jboss:persistence auto-increment="true"
	    **/
	   public abstract java.lang.Integer getIliCode();
	   public abstract void setIliCode(java.lang.Integer ILI_CODE);

	   /**
	    * @ejb:persistent-field
	    * @ejb:interface-method view-type="local"
	    *
	    * @jboss:column-name name="ILI_LN"
	    **/
	   public abstract short getIliLine();
	   /**
	    * @ejb:interface-method view-type="local"
	    **/
	   public abstract void setIliLine(short ILI_LN);

	   /**
	    * @ejb:persistent-field
	    * @ejb:interface-method view-type="local"
	    *
	    * @jboss:column-name name="ILI_QTY"
	    **/
	   public abstract double getIliQuantity();
	   /**
	    * @ejb:interface-method view-type="local"
	    **/
	   public abstract void setIliQuantity(double ILI_QTY);
	   /**
	    * @ejb:persistent-field
	    * @ejb:interface-method view-type="local"
	    *
	    * @jboss:column-name name="ILI_IMEI"
	    **/

	   public abstract String getIliImei();
	   /**
	    * @ejb:interface-method view-type="local"
	    **/
	   public abstract void setIliImei(String ILI_IMEI);
	   /**
	    * @ejb:persistent-field
	    * @ejb:interface-method view-type="local"
	    *
	    * @jboss:column-name name="ILI_UNT_PRC"
	    **/
	   public abstract double getIliUnitPrice();
	   /**
	    * @ejb:interface-method view-type="local"
	    **/
	   public abstract void setIliUnitPrice(double ILI_UNT_PRC);

	   /**
	    * @ejb:persistent-field
	    * @ejb:interface-method view-type="local"
	    *
	    * @jboss:column-name name="ILI_AMNT"
	    **/
	   public abstract double getIliAmount();
	   /**
	    * @ejb:interface-method view-type="local"
	    **/
	   public abstract void setIliAmount(double ILI_AMNT);

	   /**
	    * @ejb:persistent-field
	    * @ejb:interface-method view-type="local"
	    *
	    * @jboss:column-name name="ILI_ENBL_AT_BLD"
	    **/
	   public abstract byte getIliEnableAutoBuild();
	   /**
	    * @ejb:interface-method view-type="local"
	    **/
	   public abstract void setIliEnableAutoBuild(byte ILI_ENBL_AT_BLD);

	   /**
	    * @ejb:persistent-field
	    * @ejb:interface-method view-type="local"
	    *
	    * @jboss:column-name name="ILI_TX_AMNT"
	    **/
	   public abstract double getIliTaxAmount();
	   /**
	    * @ejb:interface-method view-type="local"
	    **/
	   public abstract void setIliTaxAmount(double ILI_TX_AMNT);

	   /**
	    * @ejb:persistent-field
	    * @ejb:interface-method view-type="local"
	    *
	    * @jboss:column-name name="ILI_DSCNT_1"
	    **/
	   public abstract double getIliDiscount1();
	   /**
	    * @ejb:interface-method view-type="local"
	    **/
	   public abstract void setIliDiscount1(double ILI_DSCNT_1);

	   /**
	    * @ejb:persistent-field
	    * @ejb:interface-method view-type="local"
	    *
	    * @jboss:column-name name="ILI_DSCNT_2"
	    **/
	   public abstract double getIliDiscount2();
	   /**
	    * @ejb:interface-method view-type="local"
	    **/
	   public abstract void setIliDiscount2(double ILI_DSCNT_2);

	   /**
	    * @ejb:persistent-field
	    * @ejb:interface-method view-type="local"
	    *
	    * @jboss:column-name name="ILI_DSCNT_3"
	    **/
	   public abstract double getIliDiscount3();
	   /**
	    * @ejb:interface-method view-type="local"
	    **/
	   public abstract void setIliDiscount3(double ILI_DSCNT_3);

	   /**
	    * @ejb:persistent-field
	    * @ejb:interface-method view-type="local"
	    *
	    * @jboss:column-name name="ILI_DSCNT_4"
	    **/
	   public abstract double getIliDiscount4();
	   /**
	    * @ejb:interface-method view-type="local"
	    **/
	   public abstract void setIliDiscount4(double ILI_DSCNT_4);

	   /**
	    * @ejb:persistent-field
	    * @ejb:interface-method view-type="local"
	    *
	    * @jboss:column-name name="ILI_TTL_DSCNT"
	    **/
	   public abstract double getIliTotalDiscount();
	   /**
	    * @ejb:interface-method view-type="local"
	    **/
	   public abstract void setIliTotalDiscount(double ILI_TTL_DSCNT);

	   /**
	    * @ejb:persistent-field
	    * @ejb:interface-method view-type="local"
	    *
	    * @jboss:column-name name="ILI_EXPRY_DT"
	    **/
	   public abstract String getIliMisc();
	   /**
	    * @ejb:interface-method view-type="local"
	    **/
	   public abstract void setIliMisc(String ILI_EXPRY_DT);

	   /**
	    * @ejb:persistent-field
	    * @ejb:interface-method view-type="local"
	    *
	    * @jboss:column-name name="ILI_TX"
	    **/
	   public abstract byte getIliTax();
	   /**
	    * @ejb:interface-method view-type="local"
	    **/
	   public abstract void setIliTax(byte ILI_TX);


	   /**
	    * @ejb:persistent-field
	    * @ejb:interface-method view-type="local"
	    *
	    * @jboss:column-name name="ILI_AD_CMPNY"
	    **/
	   public abstract Integer getIliAdCompany();
	   /**
	    * @ejb:interface-method view-type="local"
	    **/
	   public abstract void setIliAdCompany(Integer ILI_AD_CMPNY);

	   // Access methods for relationship fields

	   /**
	    * @ejb:interface-method view-type="local"
	    * @ejb:relation name="invoicelineitem-tags"
	    *               role-name="invoicelineitem-has-many-tags"
	    *
	    */
	   public abstract Collection getInvTags();
	   public abstract void setInvTags(Collection invTags);

	   /**
	    * @ejb:interface-method view-type="local"
	    * @ejb:relation name="invoice-invoicelineitems"
	    *               role-name="invoicelineitem-has-one-invoice"
	    *               cascade-delete="yes"
	    *
	    * @jboss:relation related-pk-field="invCode"
	    *                 fk-column="AR_INVOICE"
	    */
	   public abstract LocalArInvoice getArInvoice();
	   /**
		 * @ejb:interface-method view-type="local"
		 **/
	   public abstract void setArInvoice(LocalArInvoice arInvoice);

       /**
	    * @ejb:interface-method view-type="local"
	    * @ejb:relation name="receipt-invoicelineitems"
	    *               role-name="invoicelineitem-has-one-receipt"
	    *               cascade-delete="yes"
	    *
	    * @jboss:relation related-pk-field="rctCode"
	    *                 fk-column="AR_RECEIPT"
	    */
	   public abstract LocalArReceipt getArReceipt();
	   /**
		 * @ejb:interface-method view-type="local"
		 **/
	   public abstract void setArReceipt(LocalArReceipt arReceipt);

	   /**
	    * @ejb:interface-method view-type="local"
	    * @ejb:relation name="unitofmeasure-invoicelineitems"
	    *               role-name="invoicelineitem-has-one-unitofmeasure"
	    *               cascade-delete="no"
	    *
	    * @jboss:relation related-pk-field="uomCode"
	    *                 fk-column="INV_UNIT_OF_MEASURE"
	    */
	   public abstract LocalInvUnitOfMeasure getInvUnitOfMeasure();
	   /**
		 * @ejb:interface-method view-type="local"
		 **/
	   public abstract void setInvUnitOfMeasure(LocalInvUnitOfMeasure invUnitOfMeasure);

	   /**
	    * @ejb:interface-method view-type="local"
	    * @ejb:relation name="itemlocation-invoicelineitems"
	    *               role-name="invoicelineitem-has-one-itemlocation"
	    *               cascade-delete="no"
	    *
	    * @jboss:relation related-pk-field="ilCode"
	    *                 fk-column="INV_ITEM_LOCATION"
	    */
	   public abstract LocalInvItemLocation getInvItemLocation();
	   /**
		 * @ejb:interface-method view-type="local"
		 **/
	   public abstract void setInvItemLocation(LocalInvItemLocation invItemLocation);

	   /**
	    * @ejb:interface-method view-type="local"
	    * @ejb:relation name="invoicelineitem-costings"
	    *               role-name="invoicelineitem-has-many-costings"
	    */
	   public abstract Collection getInvCostings();
	   public abstract void setInvCostings(Collection invCostings);

	   /**
	    * @ejb:interface-method view-type="local"
	    * @ejb:relation name="pdc-invoicelineitems"
	    *               role-name="invoicelineitem-has-one-pdc"
	    *               cascade-delete="yes"
	    *
	    * @jboss:relation related-pk-field="pdcCode"
	    *                 fk-column="AR_PDC"
	    */
	   public abstract LocalArPdc getArPdc();
	   public abstract void setArPdc(LocalArPdc arPdc);

	   /**
	    * @jboss:dynamic-ql
	    */
	    public abstract Collection ejbSelectGeneric(java.lang.String jbossQl, java.lang.Object[] args)
	       throws FinderException;



	   // BUSINESS METHODS

	   /**
	    * @ejb:home-method view-type="local"
	    */
	    public Collection ejbHomeGetIliByCriteria(java.lang.String jbossQl, java.lang.Object[] args)
	       throws FinderException {

	       return ejbSelectGeneric(jbossQl, args);
	    }

	    /**
		 * @ejb:interface-method view-type="local"
		 **/
		public void addInvCosting(LocalInvCosting invCosting) {

			Debug.print("ArInvoiceLineItemBean addInvCosting");

			try {

				Collection invCostings = getInvCostings();
				invCostings.add(invCosting);

			} catch (Exception ex) {

				throw new EJBException(ex.getMessage());

			}

		}

		/**
		 * @ejb:interface-method view-type="local"
		 **/
		public void dropInvCosting(LocalInvCosting invCosting) {

			Debug.print("ArInvoiceLineItemBean dropInvCosting");

			try {

				Collection invCostings = getInvCostings();
				invCostings.remove(invCosting);

			} catch (Exception ex) {

				throw new EJBException(ex.getMessage());

			}

		}

	   // EntityBean methods

	   /**
	    * @ejb:create-method view-type="local"
	    **/
	   public java.lang.Integer ejbCreate (java.lang.Integer ILI_CODE, short ILI_LN, double ILI_QTY,
	   	  double ILI_UNT_PRC, double ILI_AMNT, double ILI_TX_AMNT, byte ILI_ENBL_AT_BLD, double ILI_DSCNT_1,
		  double ILI_DSCNT_2, double ILI_DSCNT_3, double ILI_DSCNT_4, double TTL_ILI_DSCNT, byte ILI_TX, Integer ILI_AD_CMPNY)
	      throws CreateException {

	      Debug.print("ArInvoiceLineItemBean ejbCreate");
	      setIliCode(ILI_CODE);
	      setIliLine(ILI_LN);
	      setIliQuantity(ILI_QTY);
	      setIliUnitPrice(ILI_UNT_PRC);
	      setIliAmount(ILI_AMNT);
	      setIliTaxAmount(ILI_TX_AMNT);
	      setIliEnableAutoBuild(ILI_ENBL_AT_BLD);
	      setIliDiscount1(ILI_DSCNT_1);
	      setIliDiscount2(ILI_DSCNT_2);
	      setIliDiscount3(ILI_DSCNT_3);
	      setIliDiscount4(ILI_DSCNT_4);
	      setIliTotalDiscount(TTL_ILI_DSCNT);
	      setIliTax(ILI_TX);

	      setIliAdCompany(ILI_AD_CMPNY);


	      return null;
	   }

	   /**
	    * @ejb:create-method view-type="local"
	    **/
	   public java.lang.Integer ejbCreate (short ILI_LN, double ILI_QTY,
		  double ILI_UNT_PRC, double ILI_AMNT, double ILI_TX_AMNT, byte ILI_ENBL_AT_BLD, double ILI_DSCNT_1,
		  double ILI_DSCNT_2, double ILI_DSCNT_3, double ILI_DSCNT_4, double TTL_ILI_DSCNT, byte ILI_TX, Integer ILI_AD_CMPNY)
	      throws CreateException {

	      Debug.print("ArInvoiceLineItemBean ejbCreate");
	      setIliLine(ILI_LN);
	      setIliQuantity(ILI_QTY);
	      setIliUnitPrice(ILI_UNT_PRC);
	      setIliAmount(ILI_AMNT);
	      setIliTaxAmount(ILI_TX_AMNT);
	      setIliEnableAutoBuild(ILI_ENBL_AT_BLD);
	      setIliDiscount1(ILI_DSCNT_1);
	      setIliDiscount2(ILI_DSCNT_2);
	      setIliDiscount3(ILI_DSCNT_3);
	      setIliDiscount4(ILI_DSCNT_4);
	      setIliTotalDiscount(TTL_ILI_DSCNT);
	      setIliTax(ILI_TX);
	      setIliAdCompany(ILI_AD_CMPNY);

	      return null;
	   }

	   public void ejbPostCreate (java.lang.Integer ILI_CODE, short ILI_LN, double ILI_QTY,
		  double ILI_UNT_PRC, double ILI_AMNT, double ILI_TX_AMNT, byte ILI_ENBL_AT_BLD, double ILI_DSCNT_1,
		  double ILI_DSCNT_2, double ILI_DSCNT_3, double ILI_DSCNT_4, double TTL_ILI_DSCNT, byte ILI_TX, Integer ILI_AD_CMPNY)
	      throws CreateException { }

	   public void ejbPostCreate (short ILI_LN, double ILI_QTY,
		  double ILI_UNT_PRC, double ILI_AMNT, double ILI_TX_AMNT, byte ILI_ENBL_AT_BLD, double ILI_DSCNT_1,
		  double ILI_DSCNT_2, double ILI_DSCNT_3, double ILI_DSCNT_4, double TTL_ILI_DSCNT, byte ILI_TX, Integer ILI_AD_CMPNY)
	      throws CreateException { }

}
