/*
 * com/ejb/ar/LocalArInvoicePaymentScheduleBean.java
 *
 * Created on March 03, 2004, 3:29 PM
 */

package com.ejb.ar;

import java.util.Collection;
import java.util.Date;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;

import com.util.AbstractEntityBean;
import com.util.Debug;

/**
 *
 * @author  Neil Andrew M. Ajero
 *
 * @ejb:bean name="ArInvoicePaymentScheduleEJB"
 *           display-name="InvoicePaymentSchedule Entity"
 *           type="CMP"
 *           cmp-version="2.x"
 *           view-type="local"
 *           local-jndi-name="omega-ejb/ArInvoicePaymentScheduleEJB"
 *           schema="ArInvoicePaymentSchedule"
 *           primkey-field="ipsCode"
 *
 * @ejb:pk class="java.lang.Integer"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="aruser"
 *                        role-link="aruserlink"
 *
 * @ejb:permission role-name="aruser"
 *
 * @ejb:interface local-class="com.ejb.ar.LocalArInvoicePaymentSchedule"
 *                local-extends="javax.ejb.EJBLocalObject"
 *
 * @ejb:home local-class="com.ejb.ar.LocalArInvoicePaymentScheduleHome"
 *           local-extends="javax.ejb.EJBLocalHome"
 *
 * @ejb:finder signature="Collection findByIpsLockAndInvCode(byte IPS_LCK, java.lang.Integer INV_CODE, java.lang.Integer IPS_AD_CMPNY)"
 *             query="SELECT OBJECT(ips) FROM ArInvoicePaymentSchedule ips WHERE ips.ipsLock = ?1 AND ips.arInvoice.invCode = ?2 AND ips.ipsAdCompany = ?3"
 *
 * @ejb:finder signature="Collection findOpenIpsByIpsLockAndCstCustomerCodeAndBrCode(byte IPS_LCK, java.lang.String CST_CSTMR_CODE, java.lang.Integer IPS_AD_BRNCH, java.lang.Integer IPS_AD_CMPNY)"
 *             query="SELECT OBJECT(ips) FROM ArInvoicePaymentSchedule ips WHERE ips.arInvoice.invPosted = 1 AND ips.ipsLock = ?1 AND ips.arInvoice.arCustomer.cstCustomerCode=?2 AND ips.ipsAmountDue > ips.ipsAmountPaid AND ips.arInvoice.invAdBranch = ?3 AND ips.ipsAdCompany = ?4"
 *
 * @jboss:query signature="Collection findOpenIpsByIpsLockAndCstCustomerCodeAndBrCode(byte IPS_LCK, java.lang.String CST_CSTMR_CODE, java.lang.Integer IPS_AD_BRNCH, java.lang.Integer IPS_AD_CMPNY)"
 *             query="SELECT OBJECT(ips) FROM ArInvoicePaymentSchedule ips WHERE ips.arInvoice.invPosted = 1 AND ips.ipsLock = ?1 AND ips.arInvoice.arCustomer.cstCustomerCode=?2 AND ips.ipsAmountDue > ips.ipsAmountPaid AND ips.arInvoice.invAdBranch = ?3 AND ips.ipsAdCompany = ?4 ORDER BY ips.arInvoice.invNumber, ips.ipsNumber ASC"
 *
 * @ejb:finder signature="Collection findOpenIpsByIpsLockAndCstCustomerCode(byte IPS_LCK, java.lang.String CST_CSTMR_CODE, java.lang.Integer IPS_AD_CMPNY)"
 *             query="SELECT OBJECT(ips) FROM ArInvoicePaymentSchedule ips WHERE ips.arInvoice.invPosted = 1 AND ips.ipsLock = ?1 AND ips.arInvoice.arCustomer.cstCustomerCode=?2 AND ips.ipsAmountDue > ips.ipsAmountPaid AND ips.ipsAdCompany = ?3"
 *
 * @jboss:query signature="Collection findOpenIpsByIpsLockAndCstCustomerCode(byte IPS_LCK, java.lang.String CST_CSTMR_CODE, java.lang.Integer IPS_AD_CMPNY)"
 *             query="SELECT OBJECT(ips) FROM ArInvoicePaymentSchedule ips WHERE ips.arInvoice.invPosted = 1 AND ips.ipsLock = ?1 AND ips.arInvoice.arCustomer.cstCustomerCode=?2 AND ips.ipsAmountDue > ips.ipsAmountPaid AND ips.ipsAdCompany = ?3 ORDER BY ips.ipsDueDate, ips.arInvoice.invNumber, ips.ipsNumber ASC"
 *
 * @ejb:finder signature="Collection findOpenIpsByIpsLockAndInvNumber(byte IPS_LOCK, java.lang.String INV_NMBR, java.lang.Integer IPS_AD_CMPNY)"
 *             query="SELECT OBJECT(ips) FROM ArInvoicePaymentSchedule ips WHERE ips.arInvoice.invPosted = 1 AND ips.ipsLock = ?1 AND ips.arInvoice.invNumber=?2 AND ips.ipsAmountDue > ips.ipsAmountPaid AND ips.ipsAdCompany = ?3"
 *
 * @jboss:query signature="Collection findOpenIpsByIpsLockAndInvNumber(byte IPS_LOCK, java.lang.String INV_NMBR, java.lang.Integer IPS_AD_CMPNY)"
 *             query="SELECT OBJECT(ips) FROM ArInvoicePaymentSchedule ips WHERE ips.arInvoice.invPosted = 1 AND ips.ipsLock = ?1 AND ips.arInvoice.invNumber=?2 AND ips.ipsAmountDue > ips.ipsAmountPaid AND ips.ipsAdCompany = ?3 ORDER BY ips.ipsDueDate"
 *
 * @ejb:finder signature="Collection findOpenIpsByIpsLockAndInvNumberAdBranchCompny(byte IPS_LOCK, java.lang.String INV_NMBR, java.lang.Integer INV_AD_BRNCH, java.lang.Integer IPS_AD_CMPNY)"
 *             query="SELECT OBJECT(ips) FROM ArInvoicePaymentSchedule ips WHERE ips.arInvoice.invPosted = 1 AND ips.ipsLock = ?1 AND ips.arInvoice.invNumber=?2 AND ips.ipsAmountDue > ips.ipsAmountPaid AND ips.arInvoice.invAdBranch = ?3 AND ips.ipsAdCompany = ?4"
 *
 * @jboss:query signature="Collection findOpenIpsByIpsLockAndInvNumberAdBranchCompny(byte IPS_LOCK, java.lang.String INV_NMBR, java.lang.Integer INV_AD_BRNCH, java.lang.Integer IPS_AD_CMPNY)"
 *             query="SELECT OBJECT(ips) FROM ArInvoicePaymentSchedule ips WHERE ips.arInvoice.invPosted = 1 AND ips.ipsLock = ?1 AND ips.arInvoice.invNumber=?2 AND ips.ipsAmountDue > ips.ipsAmountPaid AND ips.arInvoice.invAdBranch = ?3 AND ips.ipsAdCompany = ?4 ORDER BY ips.ipsDueDate"
 *
 * @ejb:finder signature="Collection findOpenIpsByInvNumber(java.lang.String INV_NMBR, java.lang.Integer INV_AD_BRNCH, java.lang.Integer IPS_AD_CMPNY)"
 *             query="SELECT OBJECT(ips) FROM ArInvoicePaymentSchedule ips WHERE ips.arInvoice.invPosted = 1 AND ips.arInvoice.invNumber = ?1 AND ips.ipsAmountDue > ips.ipsAmountPaid AND ips.arInvoice.invAdBranch = ?2 AND ips.ipsAdCompany = ?3"
 *
 * @jboss:query signature="Collection findOpenIpsByInvNumber(java.lang.String INV_NMBR, java.lang.Integer INV_AD_BRNCH, java.lang.Integer IPS_AD_CMPNY)"
 *             query="SELECT OBJECT(ips) FROM ArInvoicePaymentSchedule ips WHERE ips.arInvoice.invPosted = 1 AND ips.arInvoice.invNumber = ?1 AND ips.ipsAmountDue > ips.ipsAmountPaid AND ips.arInvoice.invAdBranch = ?2 AND ips.ipsAdCompany = ?3 ORDER BY ips.ipsDueDate"
 *
 * @ejb:finder signature="Collection findByCstCustomerCode(java.lang.String CST_CSTMR_CODE, java.lang.Integer IPS_AD_CMPNY)"
 * 			   query="SELECT OBJECT(ips) FROM ArInvoicePaymentSchedule ips WHERE ips.arInvoice.arCustomer.cstCustomerCode = ?1 AND ips.ipsAdCompany = ?2"
 *
 * @ejb:finder signature="Collection findByAdCompnyAll(java.lang.Integer IPS_AD_CMPNY)"
 * 			   query="SELECT OBJECT(ips) FROM ArInvoicePaymentSchedule ips WHERE ips.ipsAdCompany = ?1"
 *
 * @ejb:finder signature="Collection findByInvCodeAndAdCompny(java.lang.Integer INV_CODE, java.lang.Integer IPS_AD_CMPNY)"
 * 			   query="SELECT OBJECT(ips) FROM ArInvoicePaymentSchedule ips WHERE ips.arInvoice.invCode = ?1 AND ips.ipsAdCompany = ?2"
 *
 * @ejb:finder signature="Collection findByInvCodeAndAdBranchCompny(java.lang.Integer INV_CODE, java.lang.Integer INV_AD_BRNCH , java.lang.Integer IPS_AD_CMPNY)"
 * 			   query="SELECT OBJECT(ips) FROM ArInvoicePaymentSchedule ips WHERE ips.arInvoice.invCode = ?1 AND ips.arInvoice.invAdBranch = ?2 AND ips.ipsAdCompany = ?3"
 *
 * @ejb:finder signature="Collection findOverdueIpsByInvDateAndCstCustomerCode(java.util.Date INV_DT, java.lang.String CST_CSTMR_CODE, java.lang.Integer IPS_AD_CMPNY)"
 *             query="SELECT OBJECT(ips) FROM ArInvoicePaymentSchedule ips WHERE ips.ipsDueDate < ?1 AND ips.ipsAmountDue > ips.ipsAmountPaid AND ips.arInvoice.arCustomer.cstCustomerCode = ?2 AND ips.ipsAdCompany = ?3"
 *
 * @ejb:finder signature="Collection findOverdueIps(java.util.Date INV_DT, java.lang.Integer INV_AD_BRNCH, java.lang.Integer IPS_AD_CMPNY)"
 *             query="SELECT OBJECT(ips) FROM ArInvoicePaymentSchedule ips WHERE ips.arInvoice.invPosted = 1 AND ips.arInvoice.invInterest = 0 AND ips.ipsDueDate < ?1 AND ips.ipsAmountDue > ips.ipsAmountPaid AND ips.arInvoice.invAdBranch = ?2 AND ips.ipsAdCompany = ?3"
 *
 * @ejb:finder signature="Collection findOverdueIpsByNextRunDate2(java.util.Date INV_DT, java.lang.Integer INV_AD_BRNCH, java.lang.Integer IPS_AD_CMPNY)"
 *             query="SELECT OBJECT(ips) FROM ArInvoicePaymentSchedule ips WHERE ips.arInvoice.invPosted = 1 AND ips.ipsLock = 0 AND ips.arInvoice.invInterest = 0 AND ips.arInvoice.invInterestNextRunDate <= ?1 AND ips.ipsAmountDue > ips.ipsAmountPaid AND ips.arInvoice.invAdBranch = ?2 AND ips.ipsAdCompany = ?3"
 *
 * @ejb:finder signature="Collection findOverdueIpsByNextRunDate(java.util.Date INV_DT, java.lang.Integer INV_AD_BRNCH, java.lang.Integer IPS_AD_CMPNY)"
 *             query="SELECT OBJECT(ips) FROM ArInvoicePaymentSchedule ips WHERE ips.arInvoice.invPosted = 1 AND ips.arInvoice.arCustomer.cstAutoComputeInterest = 1 AND ips.ipsLock = 0 AND ips.arInvoice.invPosted = 1 AND ips.arInvoice.invInterest = 0 AND ips.arInvoice.invDisableInterest = 0 AND ips.arInvoice.invInterestNextRunDate IS NOT NULL AND ips.arInvoice.invInterestNextRunDate <= ?1 AND ips.ipsAmountDue > ips.ipsAmountPaid AND ips.arInvoice.invAdBranch = ?2 AND ips.ipsAdCompany = ?3"
 *
 * @ejb:finder signature="Collection findOverdueIpsByNextRunDateLimit(java.util.Date INV_DT, java.lang.Integer INV_AD_BRNCH, java.lang.Integer IPS_AD_CMPNY)"
 *             query="SELECT OBJECT(ips) FROM ArInvoicePaymentSchedule ips WHERE ips.arInvoice.invPosted = 1 AND ips.arInvoice.arCustomer.cstAutoComputeInterest = 1 AND ips.ipsLock = 0 AND ips.arInvoice.invPosted = 1 AND ips.arInvoice.invInterest = 0 AND ips.arInvoice.invDisableInterest = 0 AND ips.arInvoice.invInterestNextRunDate IS NOT NULL AND ips.arInvoice.invInterestNextRunDate <= ?1 AND ips.ipsAmountDue > ips.ipsAmountPaid AND ips.arInvoice.invAdBranch = ?2 AND ips.ipsAdCompany = ?3"
 *
 * @jboss:query signature="Collection findOverdueIpsByNextRunDateLimit(java.util.Date INV_DT, java.lang.Integer INV_AD_BRNCH, java.lang.Integer IPS_AD_CMPNY)"
 *             query="SELECT OBJECT(ips) FROM ArInvoicePaymentSchedule ips WHERE ips.arInvoice.invPosted = 1 AND ips.arInvoice.arCustomer.cstAutoComputeInterest = 1 AND ips.ipsLock = 0 AND ips.arInvoice.invPosted = 1 AND ips.arInvoice.invInterest = 0 AND ips.arInvoice.invDisableInterest = 0 AND ips.arInvoice.invInterestNextRunDate IS NOT NULL AND ips.arInvoice.invInterestNextRunDate <= ?1 AND ips.ipsAmountDue > ips.ipsAmountPaid AND ips.arInvoice.invAdBranch = ?2 AND ips.ipsAdCompany = ?3 ORDER BY ips.ipsCode DESC LIMIT 1"
 *
 * @ejb:finder signature="Collection findPastdueIpsByPenaltyDueDate(java.util.Date INV_DT, java.lang.Integer INV_AD_BRNCH, java.lang.Integer IPS_AD_CMPNY)"
 *             query="SELECT OBJECT(ips) FROM ArInvoicePaymentSchedule ips WHERE ips.arInvoice.invPosted = 1 AND ips.ipsInterestDue > 0 AND ips.arInvoice.arCustomer.cstAutoComputePenalty = 1 AND ips.ipsLock = 0 AND ips.ipsPenaltyDueDate < ?1 AND ips.ipsAmountDue > ips.ipsAmountPaid AND ips.arInvoice.invAdBranch = ?2 AND ips.ipsAdCompany = ?3"
 *
 * @ejb:finder signature="Collection findOpenIpsByInvHrDbReferenceIDAndIbName(java.lang.Integer DB_REF_ID, java.lang.String IB_NM, java.util.Date INV_DT, java.lang.Integer IPS_AD_CMPNY)"
 *             query="SELECT OBJECT(ips) FROM ArInvoicePaymentSchedule ips WHERE ips.ipsLock = 0 AND ips.arInvoice.invPosted = 1 AND ips.arInvoice.hrDeployedBranch.dbReferenceID IN (?1) AND ips.arInvoice.arInvoiceBatch.ibName = ?2 AND ips.ipsDueDate <= ?3 AND ips.ipsAmountDue > ips.ipsAmountPaid AND ips.ipsAdCompany = ?4"
 *
 * @jboss:query signature="Collection findOpenIpsByInvHrDbReferenceIDAndIbName(java.lang.Integer DB_REF_ID, java.lang.String IB_NM, java.util.Date INV_DT, java.lang.Integer IPS_AD_CMPNY)"
 *             query="SELECT OBJECT(ips) FROM ArInvoicePaymentSchedule ips WHERE ips.ipsLock = 0 AND ips.arInvoice.invPosted = 1 AND ips.arInvoice.hrDeployedBranch.dbReferenceID IN (?1) AND ips.arInvoice.arInvoiceBatch.ibName = ?2 AND ips.ipsDueDate <= ?3 AND ips.ipsAmountDue > ips.ipsAmountPaid AND ips.ipsAdCompany = ?4 ORDER BY ips.arInvoice.hrDeployedBranch.dbReferenceID, ips.arInvoice.arCustomer.cstCustomerCode, ips.arInvoice.invNumber, ips.ipsNumber ASC"
 *
 * @ejb:finder signature="Collection findOpenIpsByCstIbName(java.lang.String IB_NM, java.util.Date INV_DT, java.lang.Integer IPS_AD_CMPNY)"
 *             query="SELECT OBJECT(ips) FROM ArInvoicePaymentSchedule ips WHERE ips.ipsLock = 0 AND ips.arInvoice.invPosted = 1 AND ips.arInvoice.hrDeployedBranch IS NOT NULL AND ips.arInvoice.arInvoiceBatch.ibName IN (?1) AND ips.ipsDueDate <= ?2 AND ips.ipsAmountDue > ips.ipsAmountPaid AND ips.ipsAdCompany = ?3"
 *
 * @jboss:query signature="Collection findOpenIpsByCstIbName(java.lang.String IB_NM, java.util.Date INV_DT, java.lang.Integer IPS_AD_CMPNY)"
 *             query="SELECT OBJECT(ips) FROM ArInvoicePaymentSchedule ips WHERE ips.ipsLock = 0 AND ips.arInvoice.invPosted = 1 AND ips.arInvoice.hrDeployedBranch IS NOT NULL AND ips.arInvoice.arInvoiceBatch.ibName IN (?1) AND ips.ipsDueDate <= ?2 AND ips.ipsAmountDue > ips.ipsAmountPaid AND ips.ipsAdCompany = ?3 ORDER BY ips.arInvoice.hrDeployedBranch.dbReferenceID , ips.arInvoice.arCustomer.cstCustomerCode, ips.arInvoice.invNumber, ips.ipsNumber ASC"
 *
 *
 * @ejb:finder signature="Collection findOpenIpsByIpsLockAndIpsCode(byte IPS_LCK, java.lang.Integer IPS_CODE, java.lang.Integer IPS_AD_CMPNY)"
 *             query="SELECT OBJECT(ips) FROM ArInvoicePaymentSchedule ips WHERE ips.arInvoice.invPosted = 1 AND ips.ipsLock = ?1 AND ips.ipsCode=?2 AND ips.ipsAmountDue > ips.ipsAmountPaid AND ips.ipsAdCompany = ?3"
 *
 * @jboss:query signature="Collection findOpenIpsByIpsLockAndIpsCode(byte IPS_LCK, java.lang.Integer IPS_CODE, java.lang.Integer IPS_AD_CMPNY)"
 *             query="SELECT OBJECT(ips) FROM ArInvoicePaymentSchedule ips WHERE ips.arInvoice.invPosted = 1 AND ips.ipsLock = ?1 AND ips.ipsCode=?2 AND ips.ipsAmountDue > ips.ipsAmountPaid AND ips.ipsAdCompany = ?3 ORDER BY ips.ipsDueDate, ips.arInvoice.invNumber, ips.ipsNumber ASC"
 *
 *
 *
 * @ejb:select signature="Collection ejbSelectGeneric(java.lang.String jbossQl, java.lang.Object[] args)"
 *             result-type-mapping="Local"
 *             method-intf="LocalHome"
 *             query="SELECT OBJECT(ips) FROM ArInvoicePaymentSchedule ips"
 *
 * @jboss:query signature="Collection ejbSelectGeneric(java.lang.String jbossQl, java.lang.Object[] args)"
 *                         dynamic="true"
 *
 * @ejb:value-object match="*"
 *             name="ArInvoicePaymentSchedule"
 *
 * @jboss:persistence table-name="AR_INVC_PYMNT_SCHDL"
 *
 * @jboss:entity-command name="mysql-get-generated-keys"
 */


public abstract class ArInvoicePaymentScheduleBean extends AbstractEntityBean {


    // PERSITCENT METHODS

    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     * @ejb:pk-field
     *
     * @jboss:column-name name="IPS_CODE"
     *
     * @jboss:persistence auto-increment="true"
     **/
    public abstract Integer getIpsCode();
    public abstract void setIpsCode(Integer IPS_CODE);


    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="IPS_DUE_DT"
     **/
    public abstract Date getIpsDueDate();
     /**
      * @ejb:interface-method view-type="local"
      **/
    public abstract void setIpsDueDate(Date IPS_DUE_DT);


    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="IPS_NMBR"
     **/
    public abstract short getIpsNumber();
     /**
      * @ejb:interface-method view-type="local"
      **/
    public abstract void setIpsNumber(short IPS_NMBR);

    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="IPS_AMNT_DUE"
     **/
    public abstract double getIpsAmountDue();
     /**
      * @ejb:interface-method view-type="local"
      **/
    public abstract void setIpsAmountDue(double IPS_AMNT_DUE);

    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="IPS_AMNT_PD"
     **/
    public abstract double getIpsAmountPaid();
     /**
      * @ejb:interface-method view-type="local"
      **/
    public abstract void setIpsAmountPaid(double IPS_AMNT_PD);

    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="IPS_INT_DUE"
     **/
    public abstract double getIpsInterestDue();
     /**
      * @ejb:interface-method view-type="local"
      **/
    public abstract void setIpsInterestDue(double IPS_INT_DUE);

    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="IPS_LCK"
     **/
    public abstract byte getIpsLock();
     /**
      * @ejb:interface-method view-type="local"
      **/
    public abstract void setIpsLock(byte IPS_LCK);


    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="IPS_PNT_CTR"
     **/
    public abstract short getIpsPenaltyCounter();
     /**
      * @ejb:interface-method view-type="local"
      **/
    public abstract void setIpsPenaltyCounter(short IPS_PNT_CTR);

    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="IPS_PNT_DUE_DT"
     **/
    public abstract Date getIpsPenaltyDueDate();
     /**
      * @ejb:interface-method view-type="local"
      **/
    public abstract void setIpsPenaltyDueDate(Date IPS_PNT_DUE_DT);

    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="IPS_PNT_DUE"
     **/
    public abstract double getIpsPenaltyDue();
     /**
      * @ejb:interface-method view-type="local"
      **/
    public abstract void setIpsPenaltyDue(double IPS_PNT_DUE);

    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="IPS_PNT_PD"
     **/
    public abstract double getIpsPenaltyPaid();
     /**
      * @ejb:interface-method view-type="local"
      **/
    public abstract void setIpsPenaltyPaid(double IPS_PNT_PD);



    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="IPS_AD_CMPNY"
     **/
    public abstract Integer getIpsAdCompany();
     /**
      * @ejb:interface-method view-type="local"
      **/
    public abstract void setIpsAdCompany(Integer IPS_AD_CMPNY);


    // RELATIONSHIP METHODS

    /**
     * @ejb:interface-method view-type="local"
     * @ejb:relation name="invoice-invoicepaymentschedules"
     *               role-name="invoicepaymentschedule-has-one-invoice"
     *               cascade-delete="yes"
     *
     * @jboss:relation related-pk-field="invCode"
     *                 fk-column="AR_INVOICE"
     */
    public abstract LocalArInvoice getArInvoice();
    /**
     * @ejb:interface-method view-type="local"
     **/
    public abstract void setArInvoice(LocalArInvoice arInvoice);

    /**
     * @ejb:interface-method view-type="local"
     * @ejb:relation name="invoicepaymentschedule-appliedinvoices"
     *               role-name="invoicepaymentschedule-has-many-appliedinvoices"
     *
     */
    public abstract Collection getArAppliedInvoices();
    public abstract void setArAppliedInvoices(Collection arAppliedInvoices);

   /**
    * @jboss:dynamic-ql
    */
    public abstract Collection ejbSelectGeneric(java.lang.String jbossQl, java.lang.Object[] args)
       throws FinderException;


    // BUSINESS METHODS

   /**
    * @ejb:home-method view-type="local"
    */
    public Collection ejbHomeGetIpsByCriteria(java.lang.String jbossQl, java.lang.Object[] args)
       throws FinderException {

       return ejbSelectGeneric(jbossQl, args);
    }


    /**
	    * @ejb:home-method view-type="local"
	    */
	    public Collection ejbHomeGetCstByCriteria(java.lang.String jbossQl, java.lang.Object[] args)
	       throws FinderException {

	       return ejbSelectGeneric(jbossQl, args);
	    }





    /**
     * @ejb:interface-method view-type="local"
     **/
    public void addArAppliedInvoice(LocalArAppliedInvoice arAppliedInvoice) {

         Debug.print("ArInvoicePaymentScheduleBean addArAppliedInvoice");

         try {

            Collection arAppliedInvoices = getArAppliedInvoices();
	        arAppliedInvoices.add(arAppliedInvoice);

         } catch (Exception ex) {

           throw new EJBException(ex.getMessage());

         }
     }

    /**
     * @ejb:interface-method view-type="local"
     **/
    public void dropArAppliedInvoice(LocalArAppliedInvoice arAppliedInvoice) {

         Debug.print("ArInvoicePaymentScheduleBean dropArAppliedInvoice");

         try {

            Collection arAppliedInvoices = getArAppliedInvoices();
	        arAppliedInvoices.remove(arAppliedInvoice);

         } catch (Exception ex) {

           throw new EJBException(ex.getMessage());

         }
     }

    // ENTITY METHODS

    /**
     * @ejb:create-method view-type="local"
     **/
    public Integer ejbCreate(Integer IPS_CODE,
         Date IPS_DUE_DT, short IPS_NMBR,
         double IPS_AMNT_DUE, double IPS_AMNT_PD, byte IPS_LCK,
         short IPS_PNT_CTR, Date IPS_PNT_DUE_DT, double IPS_PNT_DUE, double IPS_PNT_PD,
		 Integer IPS_AD_CMPNY)
         throws CreateException {

         Debug.print("ArInvoicePaymentScheduleBean ejbCreate");

	     setIpsCode(IPS_CODE);
	     setIpsDueDate(IPS_DUE_DT);
	     setIpsNumber(IPS_NMBR);
	     setIpsAmountDue(IPS_AMNT_DUE);
	     setIpsAmountPaid(IPS_AMNT_PD);
	     setIpsLock(IPS_LCK);
	     setIpsPenaltyCounter(IPS_PNT_CTR);
	     setIpsPenaltyDueDate(IPS_PNT_DUE_DT);
	     setIpsPenaltyDue(IPS_PNT_DUE);
	     setIpsPenaltyPaid(IPS_PNT_PD);
	     setIpsAdCompany(IPS_AD_CMPNY);

         return null;
     }

    /**
     * @ejb:create-method view-type="local"
     **/
    public Integer ejbCreate(
         Date IPS_DUE_DT, short IPS_NMBR,
         double IPS_AMNT_DUE, double IPS_AMNT_PD, byte IPS_LCK,
         short IPS_PNT_CTR, Date IPS_PNT_DUE_DT, double IPS_PNT_DUE, double IPS_PNT_PD,
		 Integer IPS_AD_CMPNY)
         throws CreateException {

         Debug.print("ArInvoicePaymentScheduleBean ejbCreate");

	     setIpsDueDate(IPS_DUE_DT);
	     setIpsNumber(IPS_NMBR);
	     setIpsAmountDue(IPS_AMNT_DUE);
	     setIpsAmountPaid(IPS_AMNT_PD);
	     setIpsLock(IPS_LCK);
	     setIpsPenaltyCounter(IPS_PNT_CTR);
	     setIpsPenaltyDueDate(IPS_PNT_DUE_DT);
	     setIpsPenaltyDue(IPS_PNT_DUE);
	     setIpsPenaltyPaid(IPS_PNT_PD);
	     setIpsAdCompany(IPS_AD_CMPNY);

         return null;
     }

     public void ejbPostCreate(Integer IPS_CODE,
         Date IPS_DUE_DT, short IPS_NMBR,
         double IPS_AMNT_DUE, double IPS_AMNT_PD, byte IPS_LCK,
         short IPS_PNT_CTR, Date IPS_PNT_DUE_DT, double IPS_PNT_DUE, double IPS_PNT_PD,
		 Integer IPS_AD_CMPNY)
         throws CreateException { }

     public void ejbPostCreate(
         Date IPS_DUE_DT, short IPS_NMBR,
         double IPS_AMNT_DUE, double IPS_AMNT_PD, byte IPS_LCK,
         short IPS_PNT_CTR, Date IPS_PNT_DUE_DT, double IPS_PNT_DUE, double IPS_PNT_PD,
		 Integer IPS_AD_CMPNY)
         throws CreateException { }
}