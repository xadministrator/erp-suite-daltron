/*
 * com/ejb/ar/LocalArJobOrderAssignmentBean.java
 *
 * Created on January 3, 2019 05:20 PM
 */

package com.ejb.ar;

import java.util.Collection;

import javax.ejb.CreateException;
import javax.ejb.FinderException;

import com.util.AbstractEntityBean;
import com.util.Debug;

/**
 * 
 * @author  Ruben P. Lamberte
 * 
 * @ejb:bean name="ArJobOrderAssignmentEJB"
 *           display-name="Job Order Assignment Entity"
 *           type="CMP"
 *           cmp-version="2.x"
 *           view-type="local"
 *           local-jndi-name="omega-ejb/ArJobOrderAssignmentEJB"
 *           schema="ArJobOrderAssignment"
 *           primkey-field="jaCode"
 *       
 * @ejb:pk class="java.lang.Integer"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="apuser"
 *                        role-link="apuserlink"
 *
 * @ejb:permission role-name="apuser"
 *
 * @ejb:interface local-class="com.ejb.ar.LocalArJobOrderAssignment"
 *                local-extends="javax.ejb.EJBLocalObject"
 *
 * @ejb:home local-class="com.ejb.ar.LocalArJobOrderAssignmentHome"
 *           local-extends="javax.ejb.EJBLocalHome"
 * 
 * 
 * @ejb:finder signature="Collection findByJoCodeAndJaSo(java.lang.Integer JO_CODE, byte JA_SO, java.lang.Integer JA_AD_CMPNY)"
 *             query="SELECT OBJECT(ja) FROM ArJobOrderAssignment ja WHERE ja.arJobOrderLine.arJobOrder.joCode=?1 AND ja.jaSo=?2 AND ja.jaAdCompany = ?3"
 *
 * @jboss:query signature="Collection findByJoCodeAndJaSo(java.lang.Integer JA_CODE, byte JA_SO, java.lang.Integer JA_AD_CMPNY)"
 *             query="SELECT OBJECT(ja) FROM ArJobOrderAssignment ja WHERE ja.arJobOrderLine.arJobOrder.joCode=?1 AND ja.jaSo=?2 AND ja.jaAdCompany = ?3 ORDER BY ja.arPersonel.peIdNumber"
 * 
 * @ejb:finder signature="Collection findByJolCodeAndPeIdNumber(java.lang.Integer JOL_CODE, java.lang.String PE_ID_NUMBER, java.lang.Integer JA_AD_CMPNY)"
 *             query="SELECT OBJECT(ja) FROM ArJobOrderAssignment ja WHERE ja.arJobOrderLine.jolCode=?1 AND ja.arPersonel.peIdNumber = ?2 AND ja.jaAdCompany = ?3"
 * 
 * @ejb:finder signature="Collection findByJolCodeAndJaSo(java.lang.Integer JOL_CODE, byte JA_SO, java.lang.Integer JA_AD_CMPNY)"
 *             query="SELECT OBJECT(ja) FROM ArJobOrderAssignment ja WHERE ja.arJobOrderLine.jolCode=?1 AND ja.jaSo = ?2 AND ja.jaAdCompany = ?3"
 * 
 * 
 * @jboss:query signature="Collection ejbSelectGeneric(java.lang.String jbossQl, java.lang.Object[] args)"
 *                         dynamic="true"
 *
 * @ejb:value-object match="*"
 *             name="ArJobOrderAssignment"
 * 
 * @jboss:persistence table-name="AR_JB_ORDR_ASSGNMNT"
 *
 * @jboss:entity-command name="mysql-get-generated-keys"
 *
 */

public abstract class ArJobOrderAssignmentBean extends AbstractEntityBean {
	
	//	 Access methods for persistent fields
	
	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 * @ejb:pk-field
	 *
	 * @jboss:column-name name="JA_CODE"
	 *
	 * @jboss:persistence auto-increment="true"
	 **/
	public abstract java.lang.Integer getJaCode();
	public abstract void setJaCode(java.lang.Integer JA_CODE);
	
	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="JA_LN"
	 **/
	public abstract short getJaLine();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setJaLine(short JA_LN);
	
	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="JA_RMKS"
	 **/
	public abstract String getJaRemarks();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setJaRemarks(String JA_RMKS);
	
	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="JA_QTY"
	 **/
	public abstract double getJaQuantity();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setJaQuantity(double JA_QTY);
	
	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="JA_UNT_CST"
	 **/
	public abstract double getJaUnitCost();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setJaUnitCost(double JA_UNT_CST);
	
	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="JA_AMNT"
	 **/
	public abstract double getJaAmount();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setJaAmount(double JA_AMNT);
	
	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="JA_SO"
	 **/
	public abstract byte getJaSo();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setJaSo(byte JA_SO);
	
	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="JA_AD_CMPNY"
	 **/
	public abstract Integer getJaAdCompany();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setJaAdCompany(Integer JA_AD_CMPNY);
	
	//	 Access methods for relationship fields
	
	/**
	 * @ejb:interface-method view-type="local"
	 * @ejb:relation name="joborderline-joborderassignments"
	 *               role-name="joborderassignment-has-one-joborderline"
	 *               cascade-delete="yes"
	 *
	 * @jboss:relation related-pk-field="jolCode"
	 *                 fk-column="AR_JOB_ORDER_LINE"
	 */
	public abstract LocalArJobOrderLine getArJobOrderLine();
	 /**
     * @ejb:interface-method view-type="local"
     **/ 
	public abstract void setArJobOrderLine(LocalArJobOrderLine arJobOrderLine);
	
	/**
	 * @ejb:interface-method view-type="local"
	 * @ejb:relation name="personel-joborderassignments"
	 *               role-name="joborderassignment-has-one-personel"
	 *
	 * @jboss:relation related-pk-field="peCode"
	 *                 fk-column="AR_PERSONEL"
	 */
	public abstract LocalArPersonel getArPersonel();
	 /**
     * @ejb:interface-method view-type="local"
     **/ 
	public abstract void setArPersonel(LocalArPersonel arPersonel);
	
	/**
	 * @jboss:dynamic-ql
	 */
	public abstract Collection ejbSelectGeneric(java.lang.String jbossQl, java.lang.Object[] args)
	throws FinderException;
	
//	 BUSINESS METHODS
	
	/**
	* @ejb:home-method view-type="local"
	*/
	public Collection ejbHomeGetJaByCriteria(java.lang.String jbossQl, java.lang.Object[] args)
	   throws FinderException {
	   	
	   return ejbSelectGeneric(jbossQl, args);
	   
	}
	
//	 ENTITY METHODS
	
	/**
     * @ejb:create-method view-type="local"
     **/
    public Integer ejbCreate(Integer JA_CODE, short JA_LN, String JA_RMKS, double JA_QTY, double JA_UNT_CST, double JA_AMNT, byte JA_SO, Integer JA_AD_CMPNY)
        throws CreateException {
          
        Debug.print("ArJobOrderAssignment ejbCreate");
       
        setJaCode(JA_CODE);
        setJaLine(JA_LN);
        setJaRemarks(JA_RMKS);
        setJaQuantity(JA_QTY);
        setJaUnitCost(JA_UNT_CST);
        setJaAmount(JA_AMNT);
        setJaSo(JA_SO);
        setJaAdCompany(JA_AD_CMPNY);
               
        return null;
       
    }
    
    /**
     * @ejb:create-method view-type="local"
     **/
    public Integer ejbCreate(short JA_LN, String JA_RMKS, double JA_QTY, double JA_UNT_CST, double JA_AMNT, byte JA_SO, Integer JA_AD_CMPNY)
        throws CreateException {
          
        Debug.print("ArJobOrderAssignment ejbCreate");
       
        setJaLine(JA_LN);
        setJaRemarks(JA_RMKS);
        setJaQuantity(JA_QTY);
        setJaUnitCost(JA_UNT_CST);
        setJaAmount(JA_AMNT);
        setJaSo(JA_SO);
        setJaAdCompany(JA_AD_CMPNY);
               
        return null;
       
    }
    
    public void ejbPostCreate(Integer JA_CODE, short JA_LN, String JA_RMKS, double JA_QTY, double JA_UNT_CST, double JA_AMNT, byte JA_SO, Integer JA_AD_CMPNY)
    	throws CreateException { }
    
    public void ejbPostCreate(short JA_LN, String JA_RMKS, double JA_QTY, double JA_UNT_CST, double JA_AMNT, byte JA_SO, Integer JA_AD_CMPNY)
		throws CreateException { }
}