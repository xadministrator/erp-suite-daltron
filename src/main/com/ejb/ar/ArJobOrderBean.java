package com.ejb.ar;

import java.util.Collection;
import java.util.Date;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;

import com.util.AbstractEntityBean;
import com.util.Debug;

/**
 *
 * @ejb:bean name="ArJobOrderEJB"
 *           display-name="Job Order Entity"
 *           type="CMP"
 *           cmp-version="2.x"
 *           view-type="local"
 *           local-jndi-name="omega-ejb/ArJobOrderEJB"
 *           schema="ArJobOrder"
 *           primkey-field="joCode"
 *
 * @ejb:pk class="java.lang.Integer"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="aruser"
 *                        role-link="aruserlink"
 *
 * @ejb:permission role-name="aruser"
 *
 * @ejb:interface local-class="com.ejb.ar.LocalArJobOrder"
 *                local-extends="javax.ejb.EJBLocalObject"
 *
 * @ejb:home local-class="com.ejb.ar.LocalArJobOrderHome"
 *           local-extends="javax.ejb.EJBLocalHome"
 *
 * @ejb:finder signature="LocalArJobOrder findByJoDocumentNumberAndBrCode(java.lang.String JO_DCMNT_NMBR, java.lang.Integer JO_AD_BRNCH, java.lang.Integer JO_AD_CMPNY)"
 *           query="SELECT OBJECT(jo) FROM ArJobOrder jo  WHERE jo.joDocumentNumber = ?1 AND jo.joAdBranch = ?2 AND jo.joAdCompany = ?3"
 *
 * @ejb:finder signature="LocalArJobOrder findByJoDocumentNumberAndCstCustomerCode(java.lang.String JO_DCMNT_NMBR, java.lang.String CST_CSTMR_CODE, java.lang.Integer JO_AD_CMPNY)"
 *           query="SELECT OBJECT(jo) FROM ArJobOrder jo WHERE jo.joDocumentNumber = ?1 AND jo.arCustomer.cstCustomerCode = ?2 AND jo.joVoid = 0 AND jo.joAdCompany = ?3"
 *
 * @ejb:finder signature="Collection findPostedJoByCstCustomerCode(java.lang.String CST_CSTMR_CD, java.lang.Integer JO_AD_CMPNY)"
 * 			 query="SELECT OBJECT(jo) FROM ArJobOrder jo WHERE jo.joPosted = 1 AND jo.arCustomer.cstCustomerCode = ?1 AND jo.joAdCompany = ?2"
 *
 * @ejb:finder signature="Collection findJoPostedAndApprovedByDateRangeAndAdCompany(java.util.Date JO_DT_FRM, java.util.Date JO_DT_TO, java.lang.Integer JO_AD_CMPNY)"
 *           query="SELECT OBJECT(jo) FROM ArJobOrder jo  WHERE jo.joPosted = 1 AND jo.joApprovalStatus = 'APPROVED' AND jo.joDateApprovedRejected >= ?1 AND jo.joDateApprovedRejected <= ?2 AND jo.joAdCompany = ?3"
 *
 * @ejb:finder signature="Collection findPostedJoByBrCode(java.lang.Integer JO_AD_BRNCH, java.lang.Integer SO_AD_CMPNY)"
 * 			 query="SELECT OBJECT(jo) FROM ArJobOrder jo WHERE jo.joPosted = 1 AND jo.joVoid = 0 AND jo.joAdBranch = ?1 AND jo.joAdCompany = ?2"
 *
 *  @ejb:finder signature="Collection findPostedJoByMobileBrCode(java.lang.Integer JO_AD_BRNCH, java.lang.Integer JO_AD_CMPNY)"
 * 			 query="SELECT OBJECT(jo) FROM ArJobOrder jo WHERE jo.joPosted = 1 AND jo.joVoid = 0 AND jo.joMobile = 1 AND jo.joAdBranch = ?1 AND jo.joAdCompany = ?2"
 *
 * @ejb:finder signature="LocalArJobOrder findByJoDocumentNumberAndCstCustomerCodeAndBrCode(java.lang.String JO_DCMNT_NMBR, java.lang.String CST_CSTMR_CODE, java.lang.Integer JO_AD_BRNCH, java.lang.Integer JO_AD_CMPNY)"
 *           query="SELECT OBJECT(jo) FROM ArJobOrder jo  WHERE jo.joDocumentNumber = ?1 AND jo.arCustomer.cstCustomerCode = ?2 AND jo.joAdBranch = ?3 AND jo.joVoid = 0 AND jo.joAdCompany = ?4"
 *
 * @ejb:finder signature="Collection findDraftJoByBrCode(java.lang.Integer JO_AD_BRNCH, java.lang.Integer JO_AD_CMPNY)"
 *             query="SELECT OBJECT(jo) FROM ArJobOrder jo WHERE jo.joPosted = 0 AND jo.joApprovalStatus IS NULL AND jo.joVoid = 0 AND jo.joAdBranch = ?1 AND jo.joAdCompany = ?2"
 *
 * @ejb:select signature="Collection ejbSelectGeneric(java.lang.String jbossQl, java.lang.Object[] args)"
 *             result-type-mapping="Local"
 *             method-intf="LocalHome"
 *             query="SELECT OBJECT(jo) FROM ArJobOrder jo"
 *
 * @jboss:query signature="Collection ejbSelectGeneric(java.lang.String jbossQl, java.lang.Object[] args)"
 *                         dynamic="true"
 *
 * @ejb:value-object match="*"
 *             name="ArJobOrder"
 *
 * @jboss:persistence table-name="AR_JB_ORDR"
 *
 * @jboss:entity-command name="mysql-get-generated-keys"
 */

public abstract class ArJobOrderBean extends AbstractEntityBean {

    //  PERSISTENT METHODS

    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     * @ejb:pk-field
     *
     * @jboss:column-name name="JO_CODE"
     *
     * @jboss:persistence auto-increment="true"
     **/
    public abstract Integer getJoCode();
    public abstract void setJoCode(Integer JO_CODE);

     /**
	  * @ejb:persistent-field
	  * @ejb:interface-method view-type="local"
	  *
	  * @jboss:column-name name="JO_DT"
	  **/
	 public abstract Date getJoDate();
	 /**
	  * @ejb:interface-method view-type="local"
	  **/
	 public abstract void setJoDate(Date JO_DT);
	 
	 /**
	  * @ejb:persistent-field
	  * @ejb:interface-method view-type="local"
	  *
	  * @jboss:column-name name="JO_DCMNT_TYP"
	  **/
	 public abstract String getJoDocumentType();
	 /**
	  * @ejb:interface-method view-type="local"
	  **/
	 public abstract void setJoDocumentType(String JO_DCMNT_NMBR);
	 

	 /**
	  * @ejb:persistent-field
	  * @ejb:interface-method view-type="local"
	  *
	  * @jboss:column-name name="JO_DCMNT_NMBR"
	  **/
	 public abstract String getJoDocumentNumber();
	 /**
	  * @ejb:interface-method view-type="local"
	  **/
	 public abstract void setJoDocumentNumber(String JO_DCMNT_NMBR);

	 /**
	  * @ejb:persistent-field
	  * @ejb:interface-method view-type="local"
	  *
	  * @jboss:column-name name="JO_RFRNC_NMBR"
	  **/
	 public abstract String getJoReferenceNumber();
	 /**
	  * @ejb:interface-method view-type="local"
	  **/
	 public abstract void setJoReferenceNumber(String JO_RFRNC_NMBR);

	 /**
	  * @ejb:persistent-field
	  * @ejb:interface-method view-type="local"
	  *
	  * @jboss:column-name name="JO_TRNSCTN_TYP"
	  **/
	 public abstract String getJoTransactionType();
	 /**
	  * @ejb:interface-method view-type="local"
	  **/
	 public abstract void setJoTransactionType(String JO_TRNSCTN_TYP);




	 /**
	  * @ejb:persistent-field
	  * @ejb:interface-method view-type="local"
	  *
	  * @jboss:column-name name="JO_DESC"
	  **/
	 public abstract String getJoDescription();
	 /**
	  * @ejb:interface-method view-type="local"
	  **/
	 public abstract void setJoDescription(String JO_DESC);

	 /**
	  * @ejb:persistent-field
	  * @ejb:interface-method view-type="local"
	  *
	  * @jboss:column-name name="JO_BLL_TO"
	  **/
	 public abstract String getJoBillTo();
	 /**
	  * @ejb:interface-method view-type="local"
	  **/
	 public abstract void setJoBillTo(String JO_BLL_TO);

	 /**
	  * @ejb:persistent-field
	  * @ejb:interface-method view-type="local"
	  *
	  * @jboss:column-name name="JO_SHP_TO"
	  **/
	 public abstract String getJoShipTo();
	 /**
	  * @ejb:interface-method view-type="local"
	  **/
	 public abstract void setJoShipTo(String JO_SHP_TO);
	 
	 
	 /**
	  * @ejb:persistent-field
	  * @ejb:interface-method view-type="local"
	  *
	  * @jboss:column-name name="JO_TCHNCN"
	  **/
	 public abstract String getJoTechnician();
	 /**
	  * @ejb:interface-method view-type="local"
	  **/
	 public abstract void setJoTechnician(String JO_TCHNCN);

	 /**
	  * @ejb:persistent-field
	  * @ejb:interface-method view-type="local"
	  *
	  * @jboss:column-name name="JO_CNVRSN_DT"
	  **/
	 public abstract Date getJoConversionDate();
	 /**
	  * @ejb:interface-method view-type="local"
	  **/
	 public abstract void setJoConversionDate(Date JO_CNVRSN_DT);

	 /**
	  * @ejb:persistent-field
	  * @ejb:interface-method view-type="local"
	  *
	  * @jboss:column-name name="JO_CNVRSN_RT"
	  **/
	 public abstract double getJoConversionRate();
	 /**
	  * @ejb:interface-method view-type="local"
	  **/
	 public abstract void setJoConversionRate(double JO_CNVRSN_RT);

	 /**
	  * @ejb:persistent-field
	  * @ejb:interface-method view-type="local"
	  *
	  * @jboss:column-name name="JO_VD"
	  **/
	 public abstract byte getJoVoid();
	 /**
	  * @ejb:interface-method view-type="local"
	  **/
	 public abstract void setJoVoid(byte JO_VD);

	 /**
	  * @ejb:persistent-field
	  * @ejb:interface-method view-type="local"
	  *
	  * @jboss:column-name name="JO_MBL"
	  **/
	 public abstract byte getJoMobile();
	 /**
	  * @ejb:interface-method view-type="local"
	  **/
	 public abstract void setJoMobile(byte JO_MBL);

	 /**
	  * @ejb:persistent-field
	  * @ejb:interface-method view-type="local"
	  *
	  * @jboss:column-name name="JO_APPRVL_STATUS"
	  **/
	 public abstract String getJoApprovalStatus();
	 /**
	  * @ejb:interface-method view-type="local"
	  **/
	 public abstract void setJoApprovalStatus(String JO_APPRVL_STATUS);

	 /**
	  * @ejb:persistent-field
	  * @ejb:interface-method view-type="local"
	  *
	  * @jboss:column-name name="JO_PSTD"
	  **/
	 public abstract byte getJoPosted();
	 /**
	  * @ejb:interface-method view-type="local"
	  **/
	 public abstract void setJoPosted(byte JO_PSTD);

	 /**
	  * @ejb:persistent-field
	  * @ejb:interface-method view-type="local"
	  *
	  * @jboss:column-name name="JO_RSN_FR_RJCTN"
	  **/
	 public abstract String getJoReasonForRejection();
	 /**
	  * @ejb:interface-method view-type="local"
	  **/
	 public abstract void setJoReasonForRejection(String JO_RSN_FR_RJCTN);

	 /**
	  * @ejb:persistent-field
	  * @ejb:interface-method view-type="local"
	  *
	  * @jboss:column-name name="JO_CRTD_BY"
	  **/
	 public abstract String getJoCreatedBy();
	 /**
	  * @ejb:interface-method view-type="local"
	  **/
	 public abstract void setJoCreatedBy(String JO_CRTD_BY);

	 /**
	  * @ejb:persistent-field
	  * @ejb:interface-method view-type="local"
	  *
	  * @jboss:column-name name="JO_DT_CRTD"
	  **/
	 public abstract Date getJoDateCreated();
	 /**
	  * @ejb:interface-method view-type="local"
	  **/
	 public abstract void setJoDateCreated(Date JO_DT_CRTD);

	 /**
	  * @ejb:persistent-field
	  * @ejb:interface-method view-type="local"
	  *
	  * @jboss:column-name name="JO_LST_MDFD_BY"
	  **/
	 public abstract String getJoLastModifiedBy();
	 /**
	  * @ejb:interface-method view-type="local"
	  **/
	 public abstract void setJoLastModifiedBy(String JO_LST_MDFD_BY);

	 /**
	  * @ejb:persistent-field
	  * @ejb:interface-method view-type="local"
	  *
	  * @jboss:column-name name="JO_DT_LST_MDFD"
	  **/
	 public abstract Date getJoDateLastModified();
	 /**
	  * @ejb:interface-method view-type="local"
	  **/
	 public abstract void setJoDateLastModified(Date JO_DT_LST_MDFD);

	 /**
	  * @ejb:persistent-field
	  * @ejb:interface-method view-type="local"
	  *
	  * @jboss:column-name name="JO_APPRVD_RJCTD_BY"
	  **/
	 public abstract String getJoApprovedRejectedBy();
	 /**
	  * @ejb:interface-method view-type="local"
	  **/
	 public abstract void setJoApprovedRejectedBy(String JO_APPRVD_RJCTD_BY);

	 /**
	  * @ejb:persistent-field
	  * @ejb:interface-method view-type="local"
	  *
	  * @jboss:column-name name="JO_DT_APPRVD_RJCTD"
	  **/
	 public abstract Date getJoDateApprovedRejected();
	 /**
	  * @ejb:interface-method view-type="local"
	  **/
	 public abstract void setJoDateApprovedRejected(Date JO_DT_APPRVD_RJCTD);

	 /**
	  * @ejb:persistent-field
	  * @ejb:interface-method view-type="local"
	  *
	  * @jboss:column-name name="JO_PSTD_BY"
	  **/
	 public abstract String getJoPostedBy();
	 /**
	  * @ejb:interface-method view-type="local"
	  **/
	 public abstract void setJoPostedBy(String JO_PSTD_BY);

	 /**
	  * @ejb:persistent-field
	  * @ejb:interface-method view-type="local"
	  *
	  * @jboss:column-name name="JO_DT_PSTD"
	  **/
	 public abstract Date getJoDatePosted();
	 /**
	  * @ejb:interface-method view-type="local"
	  **/
	 public abstract void setJoDatePosted(Date JO_DT_PSTD);

	 /**
	  * @ejb:persistent-field
	  * @ejb:interface-method view-type="local"
	  *
	  * @jboss:column-name name="JO_LCK"
	  **/
	 public abstract byte getJoLock();
	 /**
	  * @ejb:interface-method view-type="local"
	  **/
	 public abstract void setJoLock(byte JO_LCK);

	 /**
	  * @ejb:persistent-field
	  * @ejb:interface-method view-type="local"
	  *
	  * @jboss:column-name name="JO_BO_LCK"
	  **/
	 public abstract byte getJoBoLock();
	 /**
	  * @ejb:interface-method view-type="local"
	  **/
	 public abstract void setJoBoLock(byte JO_BO_LCK);

	 /**
	  * @ejb:persistent-field
	  * @ejb:interface-method view-type="local"
	  *
	  * @jboss:column-name name="JO_MMO"
	  **/
	 public abstract String getJoMemo();
	 /**
	  * @ejb:interface-method view-type="local"
	  **/
	 public abstract void setJoMemo(String JO_MMO);
	 
	 
	 /**
	  * @ejb:persistent-field
	  * @ejb:interface-method view-type="local"
	  *
	  * @jboss:column-name name="JO_JB_ORDR_STTS"
	  **/
	 public abstract String getJoJobOrderStatus();
	 /**
	  * @ejb:interface-method view-type="local"
	  **/
	 public abstract void setJoJobOrderStatus(String JO_JB_ORDR_STTS);

	 /**
	  * @ejb:persistent-field
	  * @ejb:interface-method view-type="local"
	  *
	  * @jboss:column-name name="JO_ORDR_STTS"
	  **/
	 public abstract String getJoOrderStatus();
	 /**
	  * @ejb:interface-method view-type="local"
	  **/
	 public abstract void setJoOrderStatus(String JO_ORDR_STTS);


	 /**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="REPORT_PARAMETER"
	 **/
	public abstract String getReportParameter();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setReportParameter(String REPORT_PARAMETER);


	 /**
	  * @ejb:persistent-field
	  * @ejb:interface-method view-type="local"
	  *
	  * @jboss:column-name name="JO_AD_BRNCH"
	  **/
	 public abstract Integer getJoAdBranch();
	 /**
	  * @ejb:interface-method view-type="local"
	  **/
	 public abstract void setJoAdBranch(Integer JO_AD_BRNCH);

	 /**
	  * @ejb:persistent-field
	  * @ejb:interface-method view-type="local"
	  *
	  * @jboss:column-name name="JO_AD_CMPNY"
	  **/
	 public abstract Integer getJoAdCompany();
	 /**
	  * @ejb:interface-method view-type="local"
	  **/
	 public abstract void setJoAdCompany(Integer JO_AD_CMPNY);

	 // RELATIONSHIP FIELDS

	 /**
      * @ejb:interface-method view-type="local"
      * @ejb:relation name="customer-joborders"
      *               role-name="joborder-has-one-customer"
      *
      * @jboss:relation related-pk-field="cstCode"
      *                 fk-column="AR_CUSTOMER"
      */
     public abstract LocalArCustomer getArCustomer();
     /**
	  * @ejb:interface-method view-type="local"
	  **/
     public abstract void setArCustomer(LocalArCustomer arCustomer);

     /**
      * @ejb:interface-method view-type="local"
      * @ejb:relation name="paymentterm-joborders"
      *               role-name="salesorder-has-one-paymentterm"
      *
      * @jboss:relation related-pk-field="pytCode"
      *                 fk-column="AD_PAYMENT_TERM"
      */
     public abstract com.ejb.ad.LocalAdPaymentTerm getAdPaymentTerm();
     /**
	  * @ejb:interface-method view-type="local"
	  **/
     public abstract void setAdPaymentTerm(com.ejb.ad.LocalAdPaymentTerm adPaymentTerm);

     /**
      * @ejb:interface-method view-type="local"
      * @ejb:relation name="functionalcurrency-joborders"
      *               role-name="joborder-has-one-functionalcurrency"
      *
      * @jboss:relation related-pk-field="fcCode"
      *                 fk-column="GL_FUNCTIONAL_CURRENCY"
      */
     public abstract com.ejb.gl.LocalGlFunctionalCurrency getGlFunctionalCurrency();
     /**
	  * @ejb:interface-method view-type="local"
	  **/
     public abstract void setGlFunctionalCurrency(com.ejb.gl.LocalGlFunctionalCurrency glFunctionalCurrency);

     /**
      * @ejb:interface-method view-type="local"
      * @ejb:relation name="artaxcode-joborders"
      *               role-name="joborder-has-one-artaxcode"
      *
      * @jboss:relation related-pk-field="tcCode"
      *                 fk-column="AR_TAX_CODE"
      */
     public abstract LocalArTaxCode getArTaxCode();
     /**
	  * @ejb:interface-method view-type="local"
	  **/
     public abstract void setArTaxCode(LocalArTaxCode arTaxCode);
     
     /**
      * @ejb:interface-method view-type="local"
      * @ejb:relation name="jobordertype-joborders"
      *               role-name="joborder-has-one-jobordertype"
      *
      * @jboss:relation related-pk-field="jotCode"
      *                 fk-column="AR_JOB_ORDER_TYPE"
      */
     public abstract LocalArJobOrderType getArJobOrderType();
     /**
	  * @ejb:interface-method view-type="local"
	  **/
     public abstract void setArJobOrderType(LocalArJobOrderType arArJobOrderType);


     /**
      * @ejb:interface-method view-type="local"
      * @ejb:relation name="joborder-joborderlines"
      *               role-name="joborder-has-many-joborderlines"
      */
     public abstract Collection getArJobOrderLines();
     public abstract void setArJobOrderLines(Collection arJobOrderLines);


     /**
      * @ejb:interface-method view-type="local"
      * @ejb:relation name="joborder-adjustments"
      *               role-name="joborder-has-many-adjustment"
      */
     public abstract Collection getCmAdjustments();
     public abstract void setCmAdjustments(Collection cmAdjustments);


     /**
      * @ejb:interface-method view-type="local"
      * @ejb:relation name="salesperson-joborders"
      *               role-name="joborder-has-one-salesperson"
      *
      * @jboss:relation related-pk-field="slpCode"
      *                 fk-column="AR_SALESPERSON"
      */
     public abstract LocalArSalesperson getArSalesperson();
     /**
	  * @ejb:interface-method view-type="local"
	  **/
     public abstract void setArSalesperson(LocalArSalesperson arSalesperson);

     /**
      * @jboss:dynamic-ql
      */
      public abstract Collection ejbSelectGeneric(java.lang.String jbossQl, java.lang.Object[] args)
         throws FinderException;

      // BUSINESS METHODS

      /**
       * @ejb:home-method view-type="local"
       */
       public Collection ejbHomeGetJOByCriteria(java.lang.String jbossQl, java.lang.Object[] args)
          throws FinderException {

          return ejbSelectGeneric(jbossQl, args);
       }

      
      // ENTITY METHODS

      /**
       * @ejb:create-method view-type="local"
       **/
       public Integer ejbCreate(Integer JO_CODE, Date JO_DT, String JO_DCMNT_NMBR, String JO_RFRNC_NMBR, String JO_TRNSCTN_TYP,
    	        String JO_DESC, String JO_BLL_TO, String JO_SHP_TO, String JO_TCHNCN, Date JO_CNVRSN_DT, double JO_CNVRSN_RT,
    	        byte JO_VD, byte JO_MBL, String JO_APPRVL_STATUS, byte JO_PSTD, String JO_RSN_FR_RJCTN, String JO_CRTD_BY,
    	        Date JO_DT_CRTD, String JO_LST_MDFD_BY, Date JO_DT_LST_MDFD, String JO_APPRVD_RJCTD_BY,
    	        Date JO_DT_APPRVD_RJCTD, String JO_PSTD_BY, Date JO_DT_PSTD, byte JO_LCK, byte JO_BO_LCK,
    	        String JO_MMO, String JO_JB_ORDR_STTS, Integer JO_AD_BRNCH, Integer JO_AD_CMPNY)
	  		throws CreateException {

          Debug.print("ArJobOrderBean ejbCreate");

          setJoCode(JO_CODE);
          setJoDate(JO_DT);
          setJoDocumentNumber(JO_DCMNT_NMBR);
          setJoReferenceNumber(JO_RFRNC_NMBR);
          setJoTransactionType(JO_TRNSCTN_TYP);
          setJoDescription(JO_DESC);
          setJoBillTo(JO_BLL_TO);
          setJoShipTo(JO_SHP_TO);
          setJoTechnician(JO_TCHNCN);
          setJoConversionDate(JO_CNVRSN_DT);
          setJoConversionRate(JO_CNVRSN_RT);
          setJoVoid(JO_VD);
          setJoMobile(JO_MBL);
          setJoApprovalStatus(JO_APPRVL_STATUS);
          setJoPosted(JO_PSTD);
          setJoReasonForRejection(JO_RSN_FR_RJCTN);
          setJoCreatedBy(JO_CRTD_BY);
          setJoDateCreated(JO_DT_CRTD);
          setJoLastModifiedBy(JO_LST_MDFD_BY);
          setJoDateLastModified(JO_DT_LST_MDFD);
          setJoApprovedRejectedBy(JO_APPRVD_RJCTD_BY);
          setJoDateApprovedRejected(JO_DT_APPRVD_RJCTD);
          setJoPostedBy(JO_PSTD_BY);
          setJoDatePosted(JO_DT_PSTD);
          setJoLock(JO_LCK);
          setJoBoLock(JO_BO_LCK);
          setJoMemo(JO_MMO);
          setJoJobOrderStatus(JO_JB_ORDR_STTS);
          setJoAdBranch(JO_AD_BRNCH);
          setJoAdCompany(JO_AD_CMPNY);

          return null;

      }

      /**
       * @ejb:create-method view-type="local"
       **/
       public Integer ejbCreate(Date JO_DT, String JO_DCMNT_NMBR, String JO_RFRNC_NMBR, String JO_TRNSCTN_TYP,
    	        String JO_DESC, String JO_BLL_TO, String JO_SHP_TO, String JO_TCHNCN, Date JO_CNVRSN_DT, double JO_CNVRSN_RT,
    	        byte JO_VD, byte JO_MBL, String JO_APPRVL_STATUS, byte JO_PSTD, String JO_RSN_FR_RJCTN, String JO_CRTD_BY,
    	        Date JO_DT_CRTD, String JO_LST_MDFD_BY, Date JO_DT_LST_MDFD, String JO_APPRVD_RJCTD_BY,
    	        Date JO_DT_APPRVD_RJCTD, String JO_PSTD_BY, Date JO_DT_PSTD, byte JO_LCK, byte JO_BO_LCK,
    	        String JO_MMO, String JO_JB_ORDR_STTS, Integer JO_AD_BRNCH, Integer JO_AD_CMPNY)
	  		throws CreateException {

          Debug.print("ArJobOrderBean ejbCreate");

          setJoDate(JO_DT);
          setJoDocumentNumber(JO_DCMNT_NMBR);
          setJoReferenceNumber(JO_RFRNC_NMBR);
          setJoTransactionType(JO_TRNSCTN_TYP);
          setJoDescription(JO_DESC);
          setJoBillTo(JO_BLL_TO);
          setJoShipTo(JO_SHP_TO);
          setJoTechnician(JO_TCHNCN);
          setJoConversionDate(JO_CNVRSN_DT);
          setJoConversionRate(JO_CNVRSN_RT);
          setJoVoid(JO_VD);
          setJoMobile(JO_MBL);
          setJoApprovalStatus(JO_APPRVL_STATUS);
          setJoPosted(JO_PSTD);
          setJoReasonForRejection(JO_RSN_FR_RJCTN);
          setJoCreatedBy(JO_CRTD_BY);
          setJoDateCreated(JO_DT_CRTD);
          setJoLastModifiedBy(JO_LST_MDFD_BY);
          setJoDateLastModified(JO_DT_LST_MDFD);
          setJoApprovedRejectedBy(JO_APPRVD_RJCTD_BY);
          setJoDateApprovedRejected(JO_DT_APPRVD_RJCTD);
          setJoPostedBy(JO_PSTD_BY);
          setJoDatePosted(JO_DT_PSTD);
          setJoLock(JO_LCK);
          setJoBoLock(JO_BO_LCK);
          setJoMemo(JO_MMO);
          setJoJobOrderStatus(JO_JB_ORDR_STTS);
          setJoAdBranch(JO_AD_BRNCH);
          setJoAdCompany(JO_AD_CMPNY);

          return null;

      }

       public void ejbPostCreate(Integer JO_CODE, Date JO_DT, String JO_DCMNT_NMBR, String JO_RFRNC_NMBR, String JO_TRNSCTN_TYP,
    	          String JO_DESC, String JO_BLL_TO, String JO_TCHNCN, String JO_SHP_TO, Date JO_CNVRSN_DT, double JO_CNVRSN_RT,
    	          byte JO_VD, byte JO_MBL, String JO_APPRVL_STATUS, byte JO_PSTD, String JO_RSN_FR_RJCTN, String JO_CRTD_BY,
    	          Date JO_DT_CRTD, String JO_LST_MDFD_BY, Date JO_DT_LST_MDFD, String JO_APPRVD_RJCTD_BY,
    	          Date JO_DT_APPRVD_RJCTD, String JO_PSTD_BY, Date JO_DT_PSTD, byte JO_LCK, byte JO_BO_LCK,
    	          String JO_MMO, String JO_JB_ORDR_STTS,  Integer JO_AD_BRNCH, Integer JO_AD_CMPNY)
  	  		throws CreateException { }

       public void ejbPostCreate(Date JO_DT, String JO_DCMNT_NMBR, String JO_RFRNC_NMBR, String JO_TRNSCTN_TYP,
    	          String JO_DESC, String JO_BLL_TO, String JO_SHP_TO, String JO_TCHNCN, Date JO_CNVRSN_DT, double JO_CNVRSN_RT,
    	          byte JO_VD, byte JO_MBL, String JO_APPRVL_STATUS, byte JO_PSTD, String JO_RSN_FR_RJCTN, String JO_CRTD_BY,
    	          Date JO_DT_CRTD, String JO_LST_MDFD_BY, Date JO_DT_LST_MDFD, String JO_APPRVD_RJCTD_BY,
    	          Date JO_DT_APPRVD_RJCTD, String JO_PSTD_BY, Date JO_DT_PSTD, byte JO_LCK, byte JO_BO_LCK,
    	          String JO_MMO,  String JO_JB_ORDR_STTS, Integer JO_AD_BRNCH, Integer JO_AD_CMPNY)
  	  		throws CreateException { }
}
