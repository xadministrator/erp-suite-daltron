package com.ejb.ar;

import java.util.Collection;

import javax.ejb.CreateException;
import javax.ejb.FinderException;

import com.ejb.inv.LocalInvCosting;
import com.util.AbstractEntityBean;
import com.util.Debug;

/**
 * @ejb:bean name="ArJobOrderInvoiceLineEJB"
 *           display-name="Job Order Invoice Line Entity"
 *           type="CMP"
 *           cmp-version="2.x"
 *           view-type="local"
 *           local-jndi-name="omega-ejb/ArJobOrderInvoiceLineEJB"
 *           schema="ArJobOrderInvoiceLine"
 *           primkey-field="jilCode"
 *
 * @ejb:pk class="java.lang.Integer"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="aruser"
 *                        role-link="aruserlink"
 *
 * @ejb:permission role-name="aruser"
 *
 * @ejb:interface local-class="com.ejb.ar.LocalArJobOrderInvoiceLine"
 *                local-extends="javax.ejb.EJBLocalObject"
 *
 * @ejb:home local-class="com.ejb.ar.LocalArJobOrderInvoiceLineHome"
 *           local-extends="javax.ejb.EJBLocalHome"
 *
 * @ejb:finder signature = "Collection findByInvCode(java.lang.Integer INV_CODE, java.lang.Integer ILI_AD_CMPNY)"
 * 				query = "SELECT OBJECT(jil) FROM ArJobOrderInvoiceLine jil WHERE jil.arInvoice.invCode=?1 AND jil.jilAdCompany=?2"
 *
 * @ejb:finder signature="LocalArJobOrderInvoiceLine findByJolCodeAndInvCode(java.lang.Integer JIL_CODE, java.lang.Integer INV_CODE, java.lang.Integer JO_AD_CMPNY)"
 *           query="SELECT OBJECT(jil) FROM ArJobOrderInvoiceLine jil  WHERE jil.arJobOrderLine.jolCode = ?1 AND jil.arInvoice.invCode = ?2 AND jil.jilAdCompany = ?3"
 *
 *
 * @ejb:finder signature="Collection findUnpostedInvcByIiNameAndLocNameAndLessEqualDateAndInvAdBranch(java.lang.String II_NM, java.lang.String LOC_NM, java.util.Date INV_DT, java.lang.Integer INV_AD_BRNCH, java.lang.Integer JIL_AD_CMPNY)"
 * 			   query="SELECT OBJECT(jil) FROM ArJobOrderInvoiceLine jil WHERE jil.arInvoice.invPosted = 0 AND jil.arInvoice.invVoid = 0 AND jil.arInvoice.invCreditMemo = 0 AND jil.arJobOrderLine.invItemLocation.invItem.iiName = ?1 AND jil.arJobOrderLine.invItemLocation.invLocation.locName = ?2 AND jil.arInvoice.invDate <= ?3 AND jil.arInvoice.invAdBranch = ?4 AND jil.jilAdCompany = ?5"
 *
 * @ejb:finder signature="Collection findjolBySoCodeAndInvAdBranch(java.lang.Integer JO_CODE, java.lang.Integer INV_AD_BRNCH, java.lang.Integer JIL_AD_CMPNY)"
 * 			   query="SELECT OBJECT(jil) FROM ArJobOrderInvoiceLine jil WHERE jil.arInvoice.invVoid = 0 AND jil.arInvoice.invCreditMemo = 0 AND jil.arJobOrderLine.arJobOrder.joCode = ?1 AND jil.arInvoice.invAdBranch = ?2 AND jil.jilAdCompany = ?3"
 *
 * @ejb:finder signature="Collection findCommittedQtyByIiNameAndLocNameAndJoAdBranch(java.lang.String II_NM, java.lang.String LOC_NM, java.util.Date INV_DT_FRM, java.util.Date INV_DT_TO, java.lang.Integer JO_AD_BRNCH, java.lang.Integer JOL_AD_CMPNY)"
 * 			   query="SELECT OBJECT(jil) FROM ArJobOrderInvoiceLine jil WHERE jil.arJobOrderLine.invItemLocation.invItem.iiName = ?1 AND jil.arJobOrderLine.invItemLocation.invLocation.locName = ?2 AND jil.arInvoice.invDate >= ?3 AND jil.arInvoice.invDate <= ?4 AND jil.arInvoice.invAdBranch = ?5 AND jil.jilAdCompany = ?6"
 *
 * @ejb:finder signature="Collection findUnpostedJoInvByLocNameAndAdBranch(java.lang.String LOC_NM, java.lang.Integer INV_AD_BRNCH, java.lang.Integer JIL_AD_CMPNY)"
 * 			   query="SELECT OBJECT(jil) FROM ArJobOrderInvoiceLine jil WHERE jil.arInvoice.invPosted = 0 AND jil.arInvoice.invVoid = 0 AND jil.arJobOrderLine.invItemLocation.invLocation.locName = ?1 AND jil.arInvoice.invAdBranch = ?2 AND jil.jilAdCompany = ?3"
 *
 *  @ejb:finder signature="Collection findJoInvByIiCodeAndLocNameAndAdBranch(java.lang.Integer II_CODE, java.lang.String LOC_NM, java.lang.Integer INV_AD_BRNCH, java.lang.Integer JIL_AD_CMPNY)"
 * 			   query="SELECT OBJECT(jil) FROM ArJobOrderInvoiceLine jil WHERE jil.arInvoice.invVoid = 0 AND jil.arJobOrderLine.invItemLocation.invItem.iiCode=?1 AND jil.arJobOrderLine.invItemLocation.invLocation.locName = ?2 AND jil.arInvoice.invAdBranch = ?3 AND jil.jilAdCompany = ?4"
 *
 *
 * @ejb:select signature="Collection ejbSelectGeneric(java.lang.String jbossQl, java.lang.Object[] args)"
 *             result-type-mapping="Local"
 *             method-intf="LocalHome"
 *             query="SELECT OBJECT(jil) FROM ArJobOrderInvoiceLine jil"
 *
 * @jboss:query signature="Collection ejbSelectGeneric(java.lang.String jbossQl, java.lang.Object[] args)"
 *                         dynamic="true"
 *
 * @ejb:value-object match="*"
 *             name="ArJobOrderInvoiceLine"
 *
 * @jboss:persistence table-name="AR_JB_ORDR_INVC_LN"
 *
 * @jboss:entity-command name="mysql-get-generated-keys"
 *
 */

public abstract class ArJobOrderInvoiceLineBean extends AbstractEntityBean {

    //  PERSISTENT METHODS

    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     * @ejb:pk-field
     *
     * @jboss:column-name name="JIL_CODE"
     *
     * @jboss:persistence auto-increment="true"
     **/
    public abstract Integer getJilCode();
    public abstract void setJilCode(Integer JIL_CODE);

    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="JIL_QTY_DLVRD"
     **/
    public abstract double getJilQuantityDelivered();
    /**
     * @ejb:interface-method view-type="local"
     **/
    public abstract void setJilQuantityDelivered(double JIL_QTY_DLVRD);

    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="JIL_AMNT"
     **/
    public abstract double getJilAmount();
    /**
     * @ejb:interface-method view-type="local"
     **/
    public abstract void setJilAmount(double JIL_AMNT);

    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="JIL_TX_AMNT"
     **/
    public abstract double getJilTaxAmount();
    /**
     * @ejb:interface-method view-type="local"
     **/
    public abstract void setJilTaxAmount(double JIL_TX_AMNT);

    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="JIL_DSCNT_1"
     **/

    public abstract double getJilDiscount1();
    /**
     * @ejb:interface-method view-type="local"
     **/
    public abstract void setJilDiscount1(double JIL_DSCNT_1);

    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="JIL_DSCNT_2"
     **/
    public abstract double getJilDiscount2();
    /**
     * @ejb:interface-method view-type="local"
     **/
    public abstract void setJilDiscount2(double JIL_DSCNT_2);

    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="JIL_DSCNT_3"
     **/
    public abstract double getJilDiscount3();
    /**
     * @ejb:interface-method view-type="local"
     **/
    public abstract void setJilDiscount3(double JIL_DSCNT_3);

    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="JIL_DSCNT_4"
     **/
    public abstract double getJilDiscount4();
    /**
     * @ejb:interface-method view-type="local"
     **/
    public abstract void setJilDiscount4(double JIL_DSCNT_4);

    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="JIL_TTL_DSCNT"
     **/
    public abstract double getJilTotalDiscount();
    /**
     * @ejb:interface-method view-type="local"
     **/
    public abstract void setJilTotalDiscount(double JIL_TTL_DSCNT);


    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="JIL_IMEI"
     **/
    public abstract String getJilImei();
    /**
     * @ejb:interface-method view-type="local"
     **/
    public abstract void setJilImei(String JIL_IMEI);

    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="JIL_TX"
     **/
    public abstract byte getJilTax();
    /**
     * @ejb:interface-method view-type="local"
     **/
    public abstract void setJilTax(byte JIL_TX);


    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="JIL_AD_CMPNY"
     **/
    public abstract Integer getJilAdCompany();
    /**
     * @ejb:interface-method view-type="local"
     **/
    public abstract void setJilAdCompany(Integer JIL_AD_CMPNY);

    // RELATIONSHIP FIELDS

    /**
     * @ejb:interface-method view-type="local"
     * @ejb:relation name="joborderline-joborderinvoicelines"
     *               role-name="joborderinvoiceline-has-one-joborderline"
     *               cascade-delete="yes"
     *
     * @jboss:relation related-pk-field="jolCode"
     *                 fk-column="AR_JOB_ORDER_LINE"
     */
    public abstract LocalArJobOrderLine getArJobOrderLine();
    /**
     * @ejb:interface-method view-type="local"
     **/
    public abstract void setArJobOrderLine(LocalArJobOrderLine arJobOrderLine);

    /**
     * @ejb:interface-method view-type="local"
     * @ejb:relation name="invoice-joborderinvoicelines"
     *               role-name="joborderinvoiceline-has-one-invoice"
     *               cascade-delete="yes"
     *
     * @jboss:relation related-pk-field="invCode"
     *                 fk-column="AR_INVOICE"
     */
    public abstract LocalArInvoice getArInvoice();
    /**
     * @ejb:interface-method view-type="local"
     **/
    public abstract void setArInvoice(LocalArInvoice arInvoice);

    /**
     * @ejb:interface-method view-type="local"
	 * @ejb:relation name="joborderinvoiceline-costing"
     *               role-name="joborderinvoiceline-has-one-costing"
     */
    public abstract LocalInvCosting getInvCosting();
    /**
     * @ejb:interface-method view-type="local"
     **/
    public abstract void setInvCosting(LocalInvCosting invCosting);



    /**
     * @ejb:interface-method view-type="local"
     * @ejb:relation name="joborderinvoiceline-tags"
     *               role-name="joborderinvoiceline-has-many-tags"
     *
     */
    public abstract Collection getInvTags();
    public abstract void setInvTags(Collection invTags);


    /**
     * @jboss:dynamic-ql
     */
     public abstract Collection ejbSelectGeneric(java.lang.String jbossQl, java.lang.Object[] args)
        throws FinderException;

     // BUSINESS METHODS

     /**
      * @ejb:home-method view-type="local"
      */
      public Collection ejbHomeGetJobOrderInvoiceLineByCriteria(java.lang.String jbossQl, java.lang.Object[] args)
         throws FinderException {

         return ejbSelectGeneric(jbossQl, args);
      }

      // ENTITY METHODS

      /**
  	   * @ejb:create-method view-type="local"
  	   **/
      public Integer ejbCreate(Integer JIL_CODE, double JIL_QTY_DLVRD, double JIL_AMNT, double JIL_TX_AMNT,
    	        double JIL_DSCNT_1, double JIL_DSCNT_2, double JIL_DSCNT_3, double JIL_DSCNT_4, double TTL_JIL_DSCNT, String JIL_IMEI, byte JIL_TX,
    	            Integer JIL_AD_CMPNY)
      		throws CreateException {

          Debug.print("ArJobOrderInvoiceLineBean ejbCreate");

          setJilCode(JIL_CODE);
          setJilQuantityDelivered(JIL_QTY_DLVRD);
          setJilAmount(JIL_AMNT);
          setJilTaxAmount(JIL_TX_AMNT);
          setJilDiscount1(JIL_DSCNT_1);
          setJilDiscount2(JIL_DSCNT_2);
          setJilDiscount3(JIL_DSCNT_3);
          setJilDiscount4(JIL_DSCNT_4);
          setJilTotalDiscount(TTL_JIL_DSCNT);
          setJilImei(JIL_IMEI);
          setJilTax(JIL_TX);
          setJilAdCompany(JIL_AD_CMPNY);

          return null;
      }

      /**
  	   * @ejb:create-method view-type="local"
  	   **/
      public Integer ejbCreate(double JIL_QTY_DLVRD, double JIL_AMNT, double JIL_TX_AMNT, double JIL_DSCNT_1,
    	        double JIL_DSCNT_2, double JIL_DSCNT_3, double JIL_DSCNT_4, double TTL_JIL_DSCNT,  String JIL_IMEI,  byte JIL_TX,Integer JIL_AD_CMPNY)
      		throws CreateException {

          Debug.print("ArJobOrderInvoiceLineBean ejbCreate");

          setJilQuantityDelivered(JIL_QTY_DLVRD);
          setJilAmount(JIL_AMNT);
          setJilTaxAmount(JIL_TX_AMNT);
          setJilDiscount1(JIL_DSCNT_1);
          setJilDiscount2(JIL_DSCNT_2);
          setJilDiscount3(JIL_DSCNT_3);
          setJilDiscount4(JIL_DSCNT_4);
          setJilTotalDiscount(TTL_JIL_DSCNT);
          setJilImei(JIL_IMEI);
          setJilTax(JIL_TX);
          setJilAdCompany(JIL_AD_CMPNY);

          return null;
      }

      public void ejbPostCreate(Integer JIL_CODE, double JIL_QTY_DLVRD, double JIL_AMNT, double JIL_TX_AMNT,
  	        double JIL_DSCNT_1, double JIL_DSCNT_2, double JIL_DSCNT_3, double JIL_DSCNT_4, double TTL_JIL_DSCNT, String JIL_IMEI, byte JIL_TX,
            Integer JIL_AD_CMPNY)
		throws CreateException { }

      public void ejbPostCreate(double JIL_QTY_DLVRD, double JIL_AMNT, double JIL_TX_AMNT, double JIL_DSCNT_1,
  	        double JIL_DSCNT_2, double JIL_DSCNT_3, double JIL_DSCNT_4, double TTL_JIL_DSCNT,  String JIL_IMEI,  byte JIL_TX,Integer JIL_AD_CMPNY)
		throws CreateException { }

}
