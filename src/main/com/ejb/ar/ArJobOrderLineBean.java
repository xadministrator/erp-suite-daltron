package com.ejb.ar;

import java.util.Collection;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;

import com.ejb.inv.LocalInvItemLocation;
import com.ejb.inv.LocalInvUnitOfMeasure;
import com.ejb.inv.LocalInvBuildOrderLine;
import com.util.AbstractEntityBean;
import com.util.Debug;

 /**
  * @ejb:bean name="ArJobOrderLineEJB"
  *           display-name="Job Order Line Entity"
  *           type="CMP"
  *           cmp-version="2.x"
  *           view-type="local"
  *           local-jndi-name="omega-ejb/ArJobOrderLineEJB"
  *           schema="ArJobOrderLine"
  *           primkey-field="jolCode"
  *
  * @ejb:pk class="java.lang.Integer"
  *
  * @ejb:transaction type="Required"
  *
  * @ejb:security-role-ref role-name="aruser"
  *                        role-link="aruserlink"
  *
  * @ejb:permission role-name="aruser"
  *
  * @ejb:interface local-class="com.ejb.ar.LocalArJobOrderLine"
  *                local-extends="javax.ejb.EJBLocalObject"
  *
  * @ejb:home local-class="com.ejb.ar.LocalArJobOrderLineHome"
  *           local-extends="javax.ejb.EJBLocalHome"
  *
  * @jboss:query signature="Collection ejbSelectGeneric(java.lang.String jbossQl, java.lang.Object[] args)"
  *                         dynamic="true"
  *
  * @ejb:finder signature="Collection findOpenJolAll(java.lang.Integer JOL_AD_CMPNY)"
  * 			 query="SELECT OBJECT(jol) FROM ArJobOrderLine jol WHERE jol.jolAdCompany = ?1"
  *
  * @ejb:finder signature="Collection findBySalesOrderCode(java.lang.Integer JO_CODE, java.lang.Integer JOL_AD_CMPNY)"
  * 			 query="SELECT OBJECT(jol) FROM ArJobOrderLine jol WHERE jol.arJobOrder.joCode = ?1 AND jol.jolAdCompany = ?2"
  *
  * @ejb:finder signature="LocalArJobOrderLine findByJoCodeAndJolLineAndBrCode(java.lang.Integer JO_CODE, short JOL_LN, java.lang.Integer JO_AD_BRNCH, java.lang.Integer JOL_AD_CMPNY)"
  *             query="SELECT OBJECT(jol) FROM ArJobOrderLine jol  WHERE jol.arJobOrder.joCode=?1 AND jol.jolLine=?2 AND jol.arJobOrder.joAdBranch = ?3 AND jol.arJobOrder.joAdCompany = ?4" 
  *
  * @ejb:finder signature="Collection findCommittedQtyByIiNameAndLocNameAndJoAdBranch(java.lang.String II_NM, java.lang.String LOC_NM, java.util.Date JO_DT_FRM, java.util.Date JO_DT_TO, java.lang.Integer JO_AD_BRNCH, java.lang.Integer JOL_AD_CMPNY)"
  * 			   query="SELECT OBJECT(jol) FROM ArJobOrderLine jol WHERE jol.invItemLocation.invItem.iiName = ?1 AND jol.invItemLocation.invLocation.locName = ?2 AND jol.arJobOrder.joDate >= ?3 AND jol.arJobOrder.joDate <= ?4 AND jol.arJobOrder.joAdBranch = ?5 AND jol.jolAdCompany = ?6"
  *
  * @ejb:finder signature="Collection findCommittedQtyByIiNameAndLocNameAndWithoutDateAndJoAdBranch(java.lang.String II_NM, java.lang.String LOC_NM, java.lang.Integer SO_AD_BRNCH, java.lang.Integer SOL_AD_CMPNY)"
  * 			   query="SELECT OBJECT(jol) FROM ArJobOrderLine jol WHERE jol.invItemLocation.invItem.iiName = ?1 AND jol.invItemLocation.invLocation.locName = ?2 AND jol.arJobOrder.joAdBranch = ?3 AND jol.jolAdCompany = ?4"
  *
  * @ejb:value-object match="*"
  *             name="ArJobOrderLine"
  *
  * @jboss:persistence table-name="AR_JB_ORDR_LN"
  *
  * @jboss:entity-command name="mysql-get-generated-keys"
  *
  */

public abstract class ArJobOrderLineBean extends AbstractEntityBean{

    // PERSISTENT METHODS

    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     * @ejb:pk-field
     *
     * @jboss:column-name name="JOL_CODE"
     *
     * @jboss:persistence auto-increment="true"
     **/
    public abstract Integer getJolCode();
    public abstract void setJolCode(Integer JOL_CODE);

    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="JOL_LN"
     **/
    public abstract short getJolLine();
    /**
     * @ejb:interface-method view-type="local"
     **/
    public abstract void setJolLine(short JOL_LN);

    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="JOL_IDESC"
     **/
    public abstract String getJolLineIDesc();
    /**
     * @ejb:interface-method view-type="local"
     **/
    public abstract void setJolLineIDesc(String JOL_IDESC);


    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="JOL_QTY"
     **/
    public abstract double getJolQuantity();
    /**
     * @ejb:interface-method view-type="local"
     **/
    public abstract void setJolQuantity(double JOL_QTY);

    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="JOL_UNT_PRC"
     **/
    public abstract double getJolUnitPrice();
    /**
     * @ejb:interface-method view-type="local"
     **/
    public abstract void setJolUnitPrice(double JOL_UNT_PRC);

    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="JOL_AMNT"
     **/
    public abstract double getJolAmount();
    /**
     * @ejb:interface-method view-type="local"
     **/
    public abstract void setJolAmount(double JOL_AMNT);

    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="JOL_DSCNT_1"
     **/

    public abstract double getJolDiscount1();
    /**
     * @ejb:interface-method view-type="local"
     **/
    public abstract void setJolDiscount1(double JOL_DSCNT_1);

    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="JOL_DSCNT_2"
     **/
    public abstract double getJolDiscount2();
    /**
     * @ejb:interface-method view-type="local"
     **/
    public abstract void setJolDiscount2(double JOL_DSCNT_2);

    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="JOL_DSCNT_3"
     **/
    public abstract double getJolDiscount3();
    /**
     * @ejb:interface-method view-type="local"
     **/
    public abstract void setJolDiscount3(double JOL_DSCNT_3);

    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="JOL_DSCNT_4"
     **/
    public abstract double getJolDiscount4();
    /**
     * @ejb:interface-method view-type="local"
     **/
    public abstract void setJolDiscount4(double JOL_DSCNT_4);

    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="JOL_TTL_DSCNT"
     **/
    public abstract double getJolTotalDiscount();
    /**
     * @ejb:interface-method view-type="local"
     **/
    public abstract void setJolTotalDiscount(double JOL_TTL_DSCNT);

    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="JOL_RQST_QTY"
     **/
    public abstract double getJolRequestQuantity();
    /**
     * @ejb:interface-method view-type="local"
     **/
    public abstract void setJolRequestQuantity(double JOL_RQST_QTY);

    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="JOL_MISC"
     **/
    public abstract String getJolMisc();
    /**
     * @ejb:interface-method view-type="local"
     **/
    public abstract void setJolMisc(String JOL_MISC);

    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="JOL_TX"
     **/
    public abstract byte getJolTax();
    /**
     * @ejb:interface-method view-type="local"
     **/
    public abstract void setJolTax(byte JOL_TX);


    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="JOL_AD_CMPNY"
     **/
    public abstract Integer getJolAdCompany();
    /**
     * @ejb:interface-method view-type="local"
     **/
    public abstract void setJolAdCompany(Integer JOL_AD_CMPNY);

    // RELATIONSHIP FIELDS

    /**
     * @ejb:interface-method view-type="local"
     * @ejb:relation name="joborder-joborderlines"
     *               role-name="joborderline-has-one-joborder"
     *               cascade-delete="yes"
     *
     * @jboss:relation related-pk-field="joCode"
     *                 fk-column="AR_JOB_ORDER"
     */
    public abstract LocalArJobOrder getArJobOrder();
    /**
     * @ejb:interface-method view-type="local"
     **/
    public abstract void setArJobOrder(LocalArJobOrder arJobOrder);

    /**
     * @ejb:interface-method view-type="local"
     * @ejb:relation name="unitofmeasure-joborderlines"
     *               role-name="joborderline-has-one-unitofmeasure"
     *               cascade-delete="no"
     *
     * @jboss:relation related-pk-field="uomCode"
     *                 fk-column="INV_UNIT_OF_MEASURE"
     */
    public abstract LocalInvUnitOfMeasure getInvUnitOfMeasure();
    /**
     * @ejb:interface-method view-type="local"
     **/
    public abstract void setInvUnitOfMeasure(LocalInvUnitOfMeasure invUnitOfMeasure);

    /**
     * @ejb:interface-method view-type="local"
     * @ejb:relation name="itemlocation-joborderlines"
     *               role-name="joborderline-has-one-itemlocation"
     *               cascade-delete="no"
     *
     * @jboss:relation related-pk-field="ilCode"
     *                 fk-column="INV_ITEM_LOCATION"
     */
    public abstract LocalInvItemLocation getInvItemLocation();
    /**
     * @ejb:interface-method view-type="local"
     **/
    public abstract void setInvItemLocation(LocalInvItemLocation invItemLocation);

    /**
     * @ejb:interface-method view-type="local"
     * @ejb:relation name="joborderline-joborderassignments"
     *               role-name="joborderline-has-many-joborderassignments"
     */
    public abstract Collection getArJobOrderAssignments();
    
    public abstract void setArJobOrderAssignments(Collection arJobOrderAssignments);
    
    
    
    /**
     * @ejb:interface-method view-type="local"
     * @ejb:relation name="joborderline-joborderinvoicelines"
     *               role-name="joborderline-has-many-joborderinvoicelines"
     */
    public abstract Collection getArJobOrderInvoiceLines();
    public abstract void setArJobOrderInvoiceLines(Collection arJobOrderInvoiceLines);

    /**
     * @ejb:interface-method view-type="local"
     * @ejb:relation name="joborderline-buildorderlines"
     *               role-name="joborderline-has-many-buildorderlines"
     */
    public abstract Collection getInvBuildOrderLines();
    public abstract void setInvBuildOrderLines(Collection invBuildOrderLines);


    /**
    * @ejb:interface-method view-type="local"
    * @ejb:relation name="joborderline-tags"
    *               role-name="joborderline-has-many-tags"
    *
    */
   public abstract Collection getInvTags();
   public abstract void setInvTags(Collection invTags);

    /**
     * @jboss:dynamic-ql
     */
     public abstract Collection ejbSelectGeneric(java.lang.String jbossQl, java.lang.Object[] args)
        throws FinderException;

    // BUSINESS METHODS

     /**
      * @ejb:home-method view-type="local"
      */
      public Collection ejbHomeGetSalesOrderLineByCriteria(java.lang.String jbossQl, java.lang.Object[] args)
         throws FinderException {

         return ejbSelectGeneric(jbossQl, args);
      }

    

    // ENTITY METHODS

    /**
	 * @ejb:create-method view-type="local"
	 **/
      public Integer ejbCreate(Integer JOL_CODE, short JOL_LN, String JOL_IDESC, double JOL_QTY,
    	      double JOL_UNT_PRC, double JOL_AMNT, double JOL_DSCNT_1, double JOL_DSCNT_2, double JOL_DSCNT_3,
    	    double JOL_DSCNT_4, double TTL_JOL_DSCNT, double JOL_RQST_QTY, String JOL_MISC, byte JOL_TX, Integer JOL_AD_CMPNY)
    	throws CreateException {

        Debug.print("ArJobOrderLineBean ejbCreate");

        setJolCode(JOL_CODE);
        setJolLine(JOL_LN);
        setJolLineIDesc(JOL_IDESC);
        setJolQuantity(JOL_QTY);
        setJolUnitPrice(JOL_UNT_PRC);
        setJolAmount(JOL_AMNT);
        setJolDiscount1(JOL_DSCNT_1);
        setJolDiscount2(JOL_DSCNT_2);
        setJolDiscount3(JOL_DSCNT_3);
        setJolDiscount4(JOL_DSCNT_4);
        setJolTotalDiscount(TTL_JOL_DSCNT);
        setJolRequestQuantity(JOL_RQST_QTY);
        setJolMisc(JOL_MISC);
        setJolTax(JOL_TX);
        setJolAdCompany(JOL_AD_CMPNY);

        return null;
    }

    /**
	 * @ejb:create-method view-type="local"
	 **/
      public Integer ejbCreate(short JOL_LN, String JOL_IDESC, double JOL_QTY,
    	      double JOL_UNT_PRC, double JOL_AMNT, double JOL_DSCNT_1, double JOL_DSCNT_2, double JOL_DSCNT_3,
    	    double JOL_DSCNT_4, double TTL_JOL_DSCNT, double JOL_RQST_QTY, String JOL_MISC, byte JOL_TX, Integer JOL_AD_CMPNY)
    	throws CreateException {

        Debug.print("ArJobOrderLineBean ejbCreate");

        setJolLine(JOL_LN);
        setJolLineIDesc(JOL_IDESC);
        setJolQuantity(JOL_QTY);
        setJolUnitPrice(JOL_UNT_PRC);
        setJolAmount(JOL_AMNT);
        setJolDiscount1(JOL_DSCNT_1);
        setJolDiscount2(JOL_DSCNT_2);
        setJolDiscount3(JOL_DSCNT_3);
        setJolDiscount4(JOL_DSCNT_4);
        setJolTotalDiscount(TTL_JOL_DSCNT);
        setJolRequestQuantity(JOL_RQST_QTY);
        setJolMisc(JOL_MISC);
        setJolTax(JOL_TX);
        setJolAdCompany(JOL_AD_CMPNY);

        return null;
    }

    public void ejbPostCreate(Integer JOL_CODE, short JOL_LN, String JOL_IDESC, double JOL_QTY,
  	      double JOL_UNT_PRC, double JOL_AMNT, double JOL_DSCNT_1, double JOL_DSCNT_2, double JOL_DSCNT_3,
  	    double JOL_DSCNT_4, double TTL_JOL_DSCNT, double JOL_RQST_QTY, String JOL_MISC, byte JOL_TX, Integer JOL_AD_CMPNY)
    	throws CreateException { }

    public void ejbPostCreate(short JOL_LN, String JOL_IDESC, double JOL_QTY,
  	      double JOL_UNT_PRC, double JOL_AMNT, double JOL_DSCNT_1, double JOL_DSCNT_2, double JOL_DSCNT_3,
  	    double JOL_DSCNT_4, double TTL_JOL_DSCNT, double JOL_RQST_QTY, String JOL_MISC, byte JOL_TX, Integer JOL_AD_CMPNY)
    	throws CreateException { }
}
