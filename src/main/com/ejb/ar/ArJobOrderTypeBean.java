/*
 * com/ejb/ar/LocalArJobOrderTypeBean.java
 *
 * Created on January 10, 2019, 2:28 AM
 */

package com.ejb.ar;

import java.util.Collection;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;

import com.util.AbstractEntityBean;
import com.util.Debug;

/**
 *
 * @author  Ruben P. Lamberte
 *
 * @ejb:bean name="ArJobOrderTypeEJB"
 *           display-name="Job Order Type Entity"
 *           type="CMP"
 *           cmp-version="2.x"
 *           view-type="local"
 *           local-jndi-name="omega-ejb/ArJobOrderTypeEJB"
 *           schema="ArJobOrderType"
 *           primkey-field="jotCode"
 *
 * @ejb:pk class="java.lang.Integer"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="aruser"
 *                        role-link="aruserlink"
 *
 * @ejb:permission role-name="aruser"
 *
 * @ejb:interface local-class="com.ejb.ar.LocalArJobOrderType"
 *                local-extends="javax.ejb.EJBLocalObject"
 *
 * @ejb:home local-class="com.ejb.ar.LocalArJobOrderTypeHome"
 *           local-extends="javax.ejb.EJBLocalHome"
 *
 * @ejb:finder signature="Collection findEnabledJotAll(java.lang.Integer JOT_AD_CMPNY)"
 *             query="SELECT OBJECT(jot) FROM ArJobOrderType jot WHERE jot.jotEnable = 1 AND jot.jotAdCompany = ?1"
 *
 * @jboss:query signature="Collection findEnabledJotAll(java.lang.Integer JOT_AD_CMPNY)"
 *             query="SELECT OBJECT(jot) FROM ArJobOrderType jot WHERE jot.jotEnable = 1 AND jot.jotAdCompany = ?1 ORDER BY jot.jotName"
 *
 * @ejb:finder signature="LocalArJobOrderType findByJotName(java.lang.String JOT_NM, java.lang.Integer JOT_AD_CMPNY)"
 *             query="SELECT OBJECT(jot) FROM ArJobOrderType jot WHERE jot.jotName = ?1 AND jot.jotAdCompany = ?2"
 *
 * @ejb:finder signature="Collection findJotAll(java.lang.Integer JOT_AD_CMPNY)"
 *             query="SELECT OBJECT(jot) FROM ArJobOrderType jot WHERE jot.jotAdCompany = ?1"
 *
 * @jboss:query signature="Collection findJotAll(java.lang.Integer JOT_AD_CMPNY)"
 *             query="SELECT OBJECT(jot) FROM ArJobOrderType jot WHERE jot.jotAdCompany = ?1 ORDER BY jot.jotName"
 *             
 * @ejb:finder signature="Collection findByJotGlCoaJobOrderAccount(java.lang.Integer COA_CODE, java.lang.Integer JOT_AD_CMPNY)"
 *             query="SELECT OBJECT(jot) FROM ArJobOrderType jot WHERE jot.jotGlCoaJobOrderAccount=?1 AND jot.jotAdCompany = ?2"

 *
 * @ejb:select signature="Collection ejbSelectGeneric(java.lang.String jbossQl, java.lang.Object[] args)"
 *             result-type-mapping="Local"
 *             method-intf="LocalHome"
 *             query="SELECT OBJECT(jot) FROM ArJobOrderType jot"
 *
 * @jboss:query signature="Collection ejbSelectGeneric(java.lang.String jbossQl, java.lang.Object[] args)"
 *                         dynamic="true"
 * 
 * @ejb:value-object match="*"
 *             name="ArJobOrderType"
 *
 * @jboss:persistence table-name="AR_JB_ORDR_TYP"
 *
 * @jboss:entity-command name="mysql-get-generated-keys"
 */

public abstract class ArJobOrderTypeBean extends AbstractEntityBean {
    
    
    // PERSISTENT METHODS
    
    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     * @ejb:pk-field
     *
     * @jboss:column-name name="JOT_CODE"
     *
     * @jboss:persistence auto-increment="true"
     **/
    public abstract Integer getJotCode();
    public abstract void setJotCode(Integer JOT_CODE);
    
    
    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"   
     *
     * @jboss:column-name name="JOT_NM"
     **/
    public abstract String getJotName();
     /**
      * @ejb:interface-method view-type="local"
      **/ 
    public abstract void setJotName(String JOT_NM);
    
    
    /**
   	 * @ejb:persistent-field
   	 * @ejb:interface-method view-type="local"
   	 *
   	 * @jboss:column-name name="JOT_DCMNT_TYP"
   	 **/
   	public abstract String getJotDocumentType();
   	/**
   	 * @ejb:interface-method view-type="local"
   	 **/ 
   	public abstract void setJotDocumentType(String JOT_DCMNT_TYP);
    
   	
   	/**
   	 * @ejb:persistent-field
   	 * @ejb:interface-method view-type="local"
   	 *
   	 * @jboss:column-name name="JOT_RPRT_TYP"
   	 **/
   	public abstract String getJotReportType();
   	/**
   	 * @ejb:interface-method view-type="local"
   	 **/ 
   	public abstract void setJotReportType(String JOT_RPRT_TYP);
    

    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"   
     *
     * @jboss:column-name name="JOT_DESC"
     **/
    public abstract String getJotDescription();
     /**
      * @ejb:interface-method view-type="local"
      **/ 
    public abstract void setJotDescription(String JOT_DESC);
    
 
  


    
    
  
         
     /**
      * @ejb:persistent-field
      * @ejb:interface-method view-type="local"
      *
      * @jboss:column-name name="JOT_GL_COA_JB_ORDR_ACCNT"
      **/
     public abstract Integer getJotGlCoaJobOrderAccount();
     /**
      * @ejb:interface-method view-type="local"
      **/
     public abstract void setJotGlCoaJobOrderAccount(Integer JOT_GL_COA_JB_ORDR_ACCNT);
     
     
    
    
    
    
    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="JOT_ENBL"
     **/
    public abstract byte getJotEnable();
     /**
      * @ejb:interface-method view-type="local"
      **/ 
    public abstract void setJotEnable(byte JOT_ENBL);
    
   
    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="JOT_AD_CMPNY"
     **/
    public abstract Integer getJotAdCompany();
     /**
      * @ejb:interface-method view-type="local"
      **/ 
    public abstract void setJotAdCompany(Integer JOT_AD_CMPNY);
       
   
    // RELATIONSHIP METHODS
    
    /**
     * @ejb:interface-method view-type="local"
     * @ejb:relation name="artaxcode-jobordertypes"
     *               role-name="jobordertype-has-one-artaxcode"
     * 
     * @jboss:relation related-pk-field="tcCode"
     *                 fk-column="AR_TAX_CODE"
     */
    public abstract LocalArTaxCode getArTaxCode();
    /**
     * @ejb:interface-method view-type="local"
     **/ 
    public abstract void setArTaxCode(LocalArTaxCode arTaxCode);

    /**
     * @ejb:interface-method view-type="local"
     * @ejb:relation name="arwithholdingtaxcode-jobordertypes"
     *               role-name="jobordertype-has-one-arwithholdingtaxcode"
     * 
     * @jboss:relation related-pk-field="wtcCode"
     *                 fk-column="AR_WITHHOLDING_TAX_CODE"
     */
    public abstract LocalArWithholdingTaxCode getArWithholdingTaxCode();
    /**
     * @ejb:interface-method view-type="local"
     **/ 
    public abstract void setArWithholdingTaxCode(LocalArWithholdingTaxCode arWithholdingTaxCode);


    /**
     * @ejb:interface-method view-type="local"
     * @ejb:relation name="jobordertype-joborders"
     *               role-name="jobordertype-has-many-joborders"
     */
    public abstract Collection getArJobOrders();
    public abstract void setArJobOrders(Collection arJobOrders);
    
    
    
    /**
     * @jboss:dynamic-ql
     */
     public abstract Collection ejbSelectGeneric(java.lang.String jbossQl, java.lang.Object[] args)
        throws FinderException;
        
     
    // BUSINESS METHODS
    
    /**
     * @ejb:home-method view-type="local"
     */
     public Collection ejbHomeGetJotByCriteria(java.lang.String jbossQl, java.lang.Object[] args)
        throws FinderException {
        	
        return ejbSelectGeneric(jbossQl, args);
     }
     
    
    // ENTITY METHODS
    
    /**
     * @ejb:create-method view-type="local"
     **/
    public Integer ejbCreate(Integer JOT_CODE, 
         String JOT_NM, String JOT_DESC,  
         Integer JOT_GL_COA_JB_ORDR_ACCNT,
         byte JOT_ENBL,
         Integer JOT_AD_CMPNY)     	
         throws CreateException {	
     	
         Debug.print("ArJobOrderTypeBean ejbCreate");
     
	     setJotCode(JOT_CODE);
	     setJotName(JOT_NM);
	     setJotDescription(JOT_DESC);
	     setJotGlCoaJobOrderAccount(JOT_GL_COA_JB_ORDR_ACCNT);
	     setJotEnable(JOT_ENBL);
	     setJotAdCompany(JOT_AD_CMPNY);

         return null;
     }
     
    /**
     * @ejb:create-method view-type="local"
     **/
    public Integer ejbCreate(
    		 String JOT_NM, String JOT_DESC,  
             Integer JOT_GL_COA_JB_ORDR_ACCNT,
             byte JOT_ENBL,
             Integer JOT_AD_CMPNY)     	
         throws CreateException {	
     	
         Debug.print("ArJobOrderTypeBean ejbCreate");
     
         setJotName(JOT_NM);
	     setJotDescription(JOT_DESC);
	     setJotGlCoaJobOrderAccount(JOT_GL_COA_JB_ORDR_ACCNT);
	     setJotEnable(JOT_ENBL);
	     setJotAdCompany(JOT_AD_CMPNY);

         return null;
     }
     
     public void ejbPostCreate(Integer JOT_CODE, 
             String JOT_NM, String JOT_DESC,  
             Integer JOT_GL_COA_JB_ORDR_ACCNT,
             byte JOT_ENBL,
             Integer JOT_AD_CMPNY)      
         throws CreateException { }
      
     public void ejbPostCreate(
    		 String JOT_NM, String JOT_DESC,  
             Integer JOT_GL_COA_JB_ORDR_ACCNT,
             byte JOT_ENBL,
             Integer JOT_AD_CMPNY)
         throws CreateException { }     
}