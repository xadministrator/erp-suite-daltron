/*
 * com/ejb/ar/LocalArPdcBean.java
 *
 * Created on June 27, 2005 10:30 PM
 */

package com.ejb.ar;

import java.util.Collection;
import java.util.Date;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;

import com.util.AbstractEntityBean;
import com.util.Debug;

/**
 *
 * @author  Enrico C. Yap
 *
 * @ejb:bean name="ArPdcEJB"
 *           display-name="Pdc Entity"
 *           type="CMP"
 *           cmp-version="2.x"
 *           view-type="local"
 *           local-jndi-name="omega-ejb/ArPdcEJB"
 *           schema="ArPdc"
 *           primkey-field="pdcCode"
 *
 * @ejb:pk class="java.lang.Integer"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="aruser"
 *                        role-link="aruserlink"
 *
 * @ejb:permission role-name="aruser"
 *
 * @ejb:interface local-class="com.ejb.ar.LocalArPdc"
 *                local-extends="javax.ejb.EJBLocalObject"
 *
 * @ejb:home local-class="com.ejb.ar.LocalArPdcHome"
 *           local-extends="javax.ejb.EJBLocalHome"
 * 
 * @ejb:finder signature="Collection findPdcToGenerate(java.lang.Integer PDC_AD_CMPNY)"
 *             query="SELECT OBJECT(pdc) FROM ArPdc pdc WHERE pdc.pdcStatus='MATURED' AND pdc.pdcPosted=1 AND pdc.pdcAdCompany = ?1"
 *
 * @jboss:query signature="Collection findPdcToGenerate(java.lang.Integer PDC_AD_CMPNY)"
 *             query="SELECT OBJECT(pdc) FROM ArPdc pdc WHERE pdc.pdcStatus='MATURED' AND pdc.pdcPosted=1 AND pdc.pdcAdCompany = ?1"
 *
 * @ejb:finder signature="Collection findPdcToGenerateByBrCode(java.lang.Integer PDC_AD_BRNCH, java.lang.Integer PDC_AD_CMPNY)"
 *             query="SELECT OBJECT(pdc) FROM ArPdc pdc WHERE pdc.pdcStatus='MATURED' AND pdc.pdcPosted=1 AND pdc.pdcAdBranch = ?1 AND pdc.pdcAdCompany = ?2"
 * 
 * @jboss:query signature="Collection findPdcToGenerateByBrCode(java.lang.Integer PDC_AD_BRNCH, java.lang.Integer PDC_AD_CMPNY)"
 *             query="SELECT OBJECT(pdc) FROM ArPdc pdc WHERE pdc.pdcStatus='MATURED' AND pdc.pdcPosted=1 AND pdc.pdcAdBranch = ?1 AND pdc.pdcAdCompany = ?2"
 *
 * @ejb:finder signature="Collection findOpenPdcAll(java.lang.Integer PDC_AD_CMPNY)"
 *             query="SELECT OBJECT(pdc) FROM ArPdc pdc WHERE pdc.pdcStatus='OPEN' AND pdc.pdcAdCompany = ?1"
 * 
 * @jboss:query signature="Collection findOpenPdcAll(java.lang.Integer PDC_AD_CMPNY)"
 *             query="SELECT OBJECT(pdc) FROM ArPdc pdc WHERE pdc.pdcStatus='OPEN' AND pdc.pdcAdCompany = ?1"
 *
 * @ejb:finder signature="LocalArPdc findPdcByReferenceNumber(java.lang.String PDC_RFRNC_NMBR, java.lang.Integer PDC_AD_CMPNY)"
 *             query="SELECT OBJECT(pdc) FROM ArPdc pdc WHERE pdc.pdcReferenceNumber = ?1 AND pdc.pdcAdCompany = ?2"
 *             
 * @ejb:finder signature="Collection findPdcByPdcType(java.lang.Integer PDC_AD_CMPNY)"
 *             query="SELECT OBJECT(pdc) FROM ArPdc pdc WHERE pdc.adBankAccount IS NOT NULL AND pdc.pdcAdCompany = ?1"
 *
 *
 * @ejb:select signature="Collection ejbSelectGeneric(java.lang.String jbossQl, java.lang.Object[] args)"
 *             result-type-mapping="Local"
 *             method-intf="LocalHome"
 *             query="SELECT OBJECT(pdc) FROM ArPdc pdc"
 * 
 * @jboss:query signature="Collection ejbSelectGeneric(java.lang.String jbossQl, java.lang.Object[] args)"
 *                         dynamic="true"
 *
 * @ejb:value-object match="*"
 *             name="ArPdc"
 *
 * @jboss:persistence table-name="AR_PDC"
 *
 * @jboss:entity-command name="mysql-get-generated-keys"
 */

public abstract class ArPdcBean extends AbstractEntityBean {
    
    
    // PERSISTENT METHODS
    
	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 * @ejb:pk-field
	 *
	 * @jboss:column-name name="PDC_CODE"
	 *
	 * @jboss:persistence auto-increment="true"
	 **/
	public abstract Integer getPdcCode();         
	public abstract void setPdcCode(Integer PDC_CODE);

	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="PDC_STATUS"
	 **/
	public abstract String getPdcStatus();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setPdcStatus(String PDC_STATUS);

	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="PDC_LV_SHFT"
	 **/
	public abstract String getPdcLvShift();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setPdcLvShift(String PDC_LV_SHFT);     

	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="PDC_CHCK_NMBR"
	 **/
	public abstract String getPdcCheckNumber();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setPdcCheckNumber(String PDC_CHCK_NMBR);

	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="PDC_RFRNC_NMBR"
	 **/
	public abstract String getPdcReferenceNumber();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setPdcReferenceNumber(String PDC_RFRNC_NMBR);

	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="PDC_DT_RCVD"
	 **/
	public abstract Date getPdcDateReceived();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setPdcDateReceived(Date PDC_DT_RCVD);

	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="PDC_MTRTY_DT"
	 **/
	public abstract Date getPdcMaturityDate();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setPdcMaturityDate(Date PDC_MTRTY_DT);       

	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="PDC_DESC"
	 **/
	public abstract String getPdcDescription();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setPdcDescription(String PDC_DESC);

	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="PDC_CNCLLD"
	 **/
	public abstract byte getPdcCancelled();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setPdcCancelled(byte PDC_CNCLLD);

	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="PDC_AMNT"
	 **/
	public abstract double getPdcAmount();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setPdcAmount(double PDC_AMNT);

	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="PDC_CNVRSN_RT"
	 **/
	public abstract double getPdcConversionRate();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setPdcConversionRate(double PDC_CNVRSN_RT);       

	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="PDC_CNVRSN_DT"
	 **/
	public abstract Date getPdcConversionDate();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setPdcConversionDate(Date PDC_CNVRSN_DT);

	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="PDC_LV_FRGHT"
	 **/
	public abstract String getPdcLvFreight();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setPdcLvFreight(String PDC_LV_FRGHT);

	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="PDC_APPRVL_STATUS"
	 **/
	public abstract String getPdcApprovalStatus();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setPdcApprovalStatus(String PDC_APPRVL_STATUS);

	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="PDC_PSTD"
	 **/
	public abstract byte getPdcPosted();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setPdcPosted(byte PDC_PSTD);

	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="PDC_CRTD_BY"
	 **/
	public abstract String getPdcCreatedBy();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setPdcCreatedBy(String PDC_CRTD_BY);

	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="PDC_DT_CRTD"
	 **/
	public abstract Date getPdcDateCreated();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setPdcDateCreated(Date PDC_DT_CRTD);

	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="PDC_LST_MDFD_BY"
	 **/
	public abstract String getPdcLastModifiedBy();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setPdcLastModifiedBy(String PDC_LST_MDFD_BY);

	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="PDC_DT_LST_MDFD"
	 **/
	public abstract Date getPdcDateLastModified();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setPdcDateLastModified(Date PDC_DT_LST_MDFD);

	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="PDC_APPRVD_RJCTD_BY"
	 **/
	public abstract String getPdcApprovedRejectedBy();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setPdcApprovedRejectedBy(String PDC_APPRVD_RJCTD_BY);

	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="PDC_DT_APPRVD_RJCTD"
	 **/
	public abstract Date getPdcDateApprovedRejected();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setPdcDateApprovedRejected(Date PDC_DT_APPRVD_RJCTD);

	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="PDC_PSTD_BY"
	 **/
	public abstract String getPdcPostedBy();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setPdcPostedBy(String PDC_PSTD_BY);

	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="PDC_DT_PSTD"
	 **/
	public abstract Date getPdcDatePosted();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setPdcDatePosted(Date PDC_DT_PSTD);

	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="PDC_PRNTD"
	 **/
	public abstract byte getPdcPrinted();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setPdcPrinted(byte PDC_PRNTD);
	
	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="PDC_EFFCTVTY_DT"
	 **/
	public abstract Date getPdcEffectivityDate();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setPdcEffectivityDate(Date PDC_EFFCTVTY_DT);       

	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="PDC_AD_BRNCH"
	 **/
	public abstract Integer getPdcAdBranch();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setPdcAdBranch(Integer PDC_AD_BRNCH);

	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="PDC_AD_CMPNY"
	 **/
	public abstract Integer getPdcAdCompany();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setPdcAdCompany(Integer PDC_AD_CMPNY);

    // RELATIONSHIP METHODS    

     /**
      * @ejb:interface-method view-type="local"
      * @ejb:relation name="customer-pdcs"
      *               role-name="pdc-has-one-customer"
      *
      * @jboss:relation related-pk-field="cstCode"
      *                 fk-column="AR_CUSTOMER"
      */
     public abstract LocalArCustomer getArCustomer();
     public abstract void setArCustomer(LocalArCustomer arCustomer);

     /**
      * @ejb:interface-method view-type="local"
      * @ejb:relation name="arwithholdingtaxcode-pdcs"
      *               role-name="pdc-has-one-arwithholdingtaxcode"
      *
      * @jboss:relation related-pk-field="wtcCode"
      *                 fk-column="AR_WITHHOLDING_TAX_CODE"
      */
     public abstract LocalArWithholdingTaxCode getArWithholdingTaxCode();
     public abstract void setArWithholdingTaxCode(LocalArWithholdingTaxCode arWithholdingTaxCode);

     
     /**
      * @ejb:interface-method view-type="local"
      * @ejb:relation name="paymentterm-pdcs"
      *               role-name="pdc-has-one-paymentterm"
      *
      * @jboss:relation related-pk-field="pytCode"
      *                 fk-column="AD_PAYMENT_TERM"
      */
     public abstract com.ejb.ad.LocalAdPaymentTerm getAdPaymentTerm();
     public abstract void setAdPaymentTerm(com.ejb.ad.LocalAdPaymentTerm adPaymentTerm);

     /**
      * @ejb:interface-method view-type="local"
      * @ejb:relation name="functionalcurrency-pdcs"
      *               role-name="pdc-has-one-functionalcurrency"
      *
      * @jboss:relation related-pk-field="fcCode"
      *                 fk-column="GL_FUNCTIONAL_CURRENCY"
      */
     public abstract com.ejb.gl.LocalGlFunctionalCurrency getGlFunctionalCurrency();
     public abstract void setGlFunctionalCurrency(com.ejb.gl.LocalGlFunctionalCurrency glFunctionalCurrency);
     
     
     /**
      * @ejb:interface-method view-type="local"
      * @ejb:relation name="artaxcode-pdcs"
      *               role-name="pdc-has-one-artaxcode"
      *
      * @jboss:relation related-pk-field="tcCode"
      *                 fk-column="AR_TAX_CODE"
      */
     public abstract LocalArTaxCode getArTaxCode();
     public abstract void setArTaxCode(LocalArTaxCode arTaxCode);

     
 	/**
 	 * @ejb:interface-method view-type="local"
 	 * @ejb:relation name="bankaccount-pdcs"
 	 *               role-name="pdc-has-one-bankaccount"
 	 * 
 	 * @jboss:relation related-pk-field="baCode"
 	 *                 fk-column="AD_BANK_ACCOUNT"
 	 */
 	public abstract com.ejb.ad.LocalAdBankAccount getAdBankAccount();
 	public abstract void setAdBankAccount(com.ejb.ad.LocalAdBankAccount adBankAccount);
 	
	/**
	 * @ejb:interface-method view-type="local"
	 * @ejb:relation name="pdc-appliedinvoices"
	 *               role-name="pdc-has-many-appliedinvoices"
	 */
	public abstract Collection getArAppliedInvoices();
	public abstract void setArAppliedInvoices(Collection arAppliedInvoices);     

     /**
      * @ejb:interface-method view-type="local"
      * @ejb:relation name="pdc-invoicelines"
      *               role-name="pdc-has-many-invoicelines"
      */
     public abstract Collection getArInvoiceLines();
     public abstract void setArInvoiceLines(Collection arInvoiceLines);
     
     /**
      * @ejb:interface-method view-type="local"
      * @ejb:relation name="pdc-invoicelineitems"
      *               role-name="pdc-has-many-invoicelineitems"
      */
     public abstract Collection getArInvoiceLineItems();
     public abstract void setArInvoiceLineItems(Collection arInvoiceLineItems); 
     
   /**
    * @jboss:dynamic-ql
    */
    public abstract Collection ejbSelectGeneric(java.lang.String jbossQl, java.lang.Object[] args)
       throws FinderException;
       
    

   // BUSINESS METHODS
   
   /**
    * @ejb:home-method view-type="local"
    */
    public Collection ejbHomeGetPdcByCriteria(java.lang.String jbossQl, java.lang.Object[] args)
       throws FinderException {
       	
       return ejbSelectGeneric(jbossQl, args);
    }

	 /**
	  * @ejb:interface-method view-type="local"
	  **/
	 public void addArInvoiceLine(LocalArInvoiceLine arInvoiceLine) {
	
	      Debug.print("ArInvoiceBean addArInvoiceLine");
	  
	      try {
	  	
	      Collection arInvoiceLines = getArInvoiceLines();
		      arInvoiceLines.add(arInvoiceLine);
		     
	      } catch (Exception ex) {
	  	
	          throw new EJBException(ex.getMessage());
	     
	      }
	      
	 }
	
     /**
      * @ejb:interface-method view-type="local"
      **/ 
     public void dropArInvoiceLine(LocalArInvoiceLine arInvoiceLine) {
		
		  Debug.print("ArInvoiceBean dropArInvoiceLine");
		  
		  try {
		  	
		     Collection arInvoiceLines = getArInvoiceLines();
			     arInvoiceLines.remove(arInvoiceLine);
			     
		  } catch (Exception ex) {
		  	
		     throw new EJBException(ex.getMessage());
		     
		  }
		  
     }
     
     /**
	  * @ejb:interface-method view-type="local"
	  **/
	 public void addArInvoiceLineItem(LocalArInvoiceLineItem arInvoiceLineItem) {
	
	      Debug.print("ArInvoiceBean addArInvoiceLineItem");
	  
	      try {
	  	
	      Collection arInvoiceLineItems = getArInvoiceLineItems();
	      	arInvoiceLineItems.add(arInvoiceLineItem);
		     
	      } catch (Exception ex) {
	  	
	          throw new EJBException(ex.getMessage());
	     
	      }
	      
	 }
	
     /**
      * @ejb:interface-method view-type="local"
      **/ 
     public void dropArInvoiceLineItem(LocalArInvoiceLineItem arInvoiceLineItem) {
		
		  Debug.print("ArInvoiceBean dropArInvoiceLineItem");
		  
		  try {
		  	
		     Collection arInvoiceLineItems = getArInvoiceLineItems();
		     	arInvoiceLineItems.remove(arInvoiceLineItem);
			     
		  } catch (Exception ex) {
		  	
		     throw new EJBException(ex.getMessage());
		     
		  }
		  
     }
     
	 /**
	  * @ejb:interface-method view-type="local"
	  **/
	 public void addArAppliedInvoice(LocalArAppliedInvoice arAppliedInvoice) {
	
	      Debug.print("ArAppliedInvoiceBean addArAppliedInvoice");
	  
	      try {
	  	
	      Collection arAppliedInvoices = getArAppliedInvoices();
	      	arAppliedInvoices.add(arAppliedInvoice);
		     
	      } catch (Exception ex) {
	  	
	          throw new EJBException(ex.getMessage());
	     
	      }
	      
	 }
	
     /**
      * @ejb:interface-method view-type="local"
      **/ 
     public void dropArAppliedInvoice(LocalArAppliedInvoice arAppliedInvoice) {
		
		  Debug.print("ArAppliedInvoiceBean dropArAppliedInvoice");
		  
		  try {
		  	
		     Collection arAppliedInvoices = getArAppliedInvoices();
		     	arAppliedInvoices.remove(arAppliedInvoice);
			     
		  } catch (Exception ex) {
		  	
		     throw new EJBException(ex.getMessage());
		     
		  }
		  
     }
				         
     // ENTITY METHODS
    
     /**
      * @ejb:create-method view-type="local"
      **/
     public Integer ejbCreate(Integer PDC_CODE, 
     	 String PDC_STATUS, String PDC_LV_SHFT, String PDC_CHCK_NMBR, 
     	 String PDC_RFRNC_NMBR, Date PDC_DT_RCVD, Date PDC_MTRTY_DT, 
     	 String PDC_DESC, byte PDC_CNCLLD, double PDC_AMNT, double PDC_CNVRSN_RT, 
     	 Date PDC_CNVRSN_DT, String PDC_LV_FRGHT, String PDC_APPRVL_STATUS, 
     	 byte PDC_PSTD, String PDC_CRTD_BY, Date PDC_DT_CRTD, String PDC_LST_MDFD_BY,
     	 Date PDC_DT_LST_MDFD, String PDC_APPRVD_RJCTD_BY, Date PDC_DT_APPRVD_RJCTD,  String PDC_PSTD_BY, 
     	 Date PDC_DT_PSTD, byte PDC_PRNTD, Date PDC_EFFCTVTY_DT, Integer PDC_AD_BRNCH, Integer PDC_AD_CMPNY)
         throws CreateException {
           
         Debug.print("ArPdcBean ejbCreate");
        
         setPdcCode(PDC_CODE);
         setPdcStatus(PDC_STATUS);
         setPdcLvShift(PDC_LV_SHFT);
         setPdcCheckNumber(PDC_CHCK_NMBR);
         setPdcReferenceNumber(PDC_RFRNC_NMBR);
         setPdcDateReceived(PDC_DT_RCVD);	 
         setPdcMaturityDate(PDC_MTRTY_DT);         
         setPdcDescription(PDC_DESC);
         setPdcCancelled(PDC_CNCLLD);         
         setPdcAmount(PDC_AMNT);
         setPdcConversionRate(PDC_CNVRSN_RT);         
         setPdcConversionDate(PDC_CNVRSN_DT);
		 setPdcLvFreight(PDC_LV_FRGHT);
		 setPdcApprovalStatus(PDC_APPRVL_STATUS);
         setPdcPosted(PDC_PSTD);
         setPdcCreatedBy(PDC_CRTD_BY);
         setPdcDateCreated(PDC_DT_CRTD);
         setPdcLastModifiedBy(PDC_LST_MDFD_BY);
         setPdcDateLastModified(PDC_DT_LST_MDFD);
         setPdcApprovedRejectedBy(PDC_APPRVD_RJCTD_BY);
         setPdcDateApprovedRejected(PDC_DT_APPRVD_RJCTD);
         setPdcPostedBy(PDC_PSTD_BY);
         setPdcDatePosted(PDC_DT_PSTD);
         setPdcPrinted(PDC_PRNTD);
         setPdcEffectivityDate(PDC_EFFCTVTY_DT);
         setPdcAdBranch(PDC_AD_BRNCH);
         setPdcAdCompany(PDC_AD_CMPNY);
                
         return null;
        
     }
    
     /**
      * @ejb:create-method view-type="local"
      **/
     public Integer ejbCreate( 
     		String PDC_STATUS, String PDC_LV_SHFT, String PDC_CHCK_NMBR, 
        	 String PDC_RFRNC_NMBR, Date PDC_DT_RCVD, Date PDC_MTRTY_DT, 
        	 String PDC_DESC, byte PDC_CNCLLD, double PDC_AMNT, double PDC_CNVRSN_RT, 
        	 Date PDC_CNVRSN_DT, String PDC_LV_FRGHT, String PDC_APPRVL_STATUS, 
        	 byte PDC_PSTD, String PDC_CRTD_BY, Date PDC_DT_CRTD, String PDC_LST_MDFD_BY,
        	 Date PDC_DT_LST_MDFD, String PDC_APPRVD_RJCTD_BY, Date PDC_DT_APPRVD_RJCTD,  String PDC_PSTD_BY, 
        	 Date PDC_DT_PSTD, byte PDC_PRNTD, Date PDC_EFFCTVTY_DT, Integer PDC_AD_BRNCH, Integer PDC_AD_CMPNY)
         throws CreateException {
           
         Debug.print("ArPdcBean ejbCreate");
               
         setPdcStatus(PDC_STATUS);
         setPdcLvShift(PDC_LV_SHFT);
         setPdcCheckNumber(PDC_CHCK_NMBR);
         setPdcReferenceNumber(PDC_RFRNC_NMBR);
         setPdcDateReceived(PDC_DT_RCVD);	 
         setPdcMaturityDate(PDC_MTRTY_DT);         
         setPdcDescription(PDC_DESC);
         setPdcCancelled(PDC_CNCLLD);         
         setPdcAmount(PDC_AMNT);
         setPdcConversionRate(PDC_CNVRSN_RT);         
         setPdcConversionDate(PDC_CNVRSN_DT);
		 setPdcLvFreight(PDC_LV_FRGHT);
		 setPdcApprovalStatus(PDC_APPRVL_STATUS);
         setPdcPosted(PDC_PSTD);
         setPdcCreatedBy(PDC_CRTD_BY);
         setPdcDateCreated(PDC_DT_CRTD);
         setPdcLastModifiedBy(PDC_LST_MDFD_BY);
         setPdcDateLastModified(PDC_DT_LST_MDFD);
         setPdcApprovedRejectedBy(PDC_APPRVD_RJCTD_BY);
         setPdcDateApprovedRejected(PDC_DT_APPRVD_RJCTD);
         setPdcPostedBy(PDC_PSTD_BY);
         setPdcDatePosted(PDC_DT_PSTD);
         setPdcPrinted(PDC_PRNTD);
         setPdcEffectivityDate(PDC_EFFCTVTY_DT);
         setPdcAdBranch(PDC_AD_BRNCH);
         setPdcAdCompany(PDC_AD_CMPNY);
        
         return null;
        
     }
    
    
     public void ejbPostCreate(Integer PDC_CODE, 
        	 String PDC_STATUS, String PDC_LV_SHFT, String PDC_CHCK_NMBR, 
         	 String PDC_RFRNC_NMBR, Date PDC_DT_RCVD, Date PDC_MTRTY_DT, 
         	 String PDC_DESC, byte PDC_CNCLLD, double PDC_AMNT, double PDC_CNVRSN_RT, 
         	 Date PDC_CNVRSN_DT, String PDC_LV_FRGHT, String PDC_APPRVL_STATUS, 
         	 byte PDC_PSTD, String PDC_CRTD_BY, Date PDC_DT_CRTD, String PDC_LST_MDFD_BY,
         	 Date PDC_DT_LST_MDFD, String PDC_APPRVD_RJCTD_BY, Date PDC_DT_APPRVD_RJCTD,  String PDC_PSTD_BY, 
         	 Date PDC_DT_PSTD, byte PDC_PRNTD, Date PDC_EFFCTVTY_DT, Integer PDC_AD_BRNCH, Integer PDC_AD_CMPNY)
         throws CreateException { }
   
     public void ejbPostCreate(String PDC_STATUS, String PDC_LV_SHFT, String PDC_CHCK_NMBR, 
         	 String PDC_RFRNC_NMBR, Date PDC_DT_RCVD, Date PDC_MTRTY_DT, 
         	 String PDC_DESC, byte PDC_CNCLLD, double PDC_AMNT, double PDC_CNVRSN_RT, 
         	 Date PDC_CNVRSN_DT, String PDC_LV_FRGHT, String PDC_APPRVL_STATUS, 
         	 byte PDC_PSTD, String PDC_CRTD_BY, Date PDC_DT_CRTD, String PDC_LST_MDFD_BY,
         	 Date PDC_DT_LST_MDFD, String PDC_APPRVD_RJCTD_BY, Date PDC_DT_APPRVD_RJCTD,  String PDC_PSTD_BY, 
         	 Date PDC_DT_PSTD, byte PDC_PRNTD, Date PDC_EFFCTVTY_DT, Integer PDC_AD_BRNCH, Integer PDC_AD_CMPNY)
         throws CreateException { }
        
}

