package com.ejb.ar;

import java.util.Collection;
import java.util.Date;

import javax.ejb.CreateException;
import javax.ejb.FinderException;

import com.util.AbstractEntityBean;
import com.util.Debug;


/**
*
* @author  Ruben P. Lamberte
*
* @ejb:bean name="ArPersonelEJB"
*           display-name="Personel Entity"
*           type="CMP"
*           cmp-version="2.x"
*           view-type="local"
*           local-jndi-name="omega-ejb/ArPersonelEJB"
*           schema="ArPersonel"
*           primkey-field="peCode"
*
* @ejb:pk class="java.lang.Integer"
*
* @ejb:transaction type="Required"
*
* @ejb:security-role-ref role-name="aruser"
*                        role-link="aruserlink"
*
* @ejb:permission role-name="aruser"
*
* @ejb:interface local-class="com.ejb.ar.LocalArPersonel"
*                local-extends="javax.ejb.EJBLocalObject"
*
* @ejb:home local-class="com.ejb.ar.LocalArPersonelHome"
*           local-extends="javax.ejb.EJBLocalHome"
*
* @ejb:finder signature="Collection findPeAll(java.lang.Integer PE_AD_CMPNY)"
*             query="SELECT OBJECT(pe) FROM ArPersonel pe WHERE pe.peAdCompany = ?1"
*
* @jboss:query signature="Collection findPeAll(java.lang.Integer PE_AD_CMPNY)"
*             query="SELECT OBJECT(pe) FROM ArPersonel pe WHERE pe.peAdCompany = ?1" ORDER BY pe.peIdNumber"
*
* @ejb:finder signature="LocalArPersonel findByPeIdNumber(java.lang.String PE_ID_NMBR, java.lang.Integer PE_AD_CMPNY)"
*             query="SELECT OBJECT(pe) FROM ArPersonel pe WHERE pe.peIdNumber = ?1 AND pe.peAdCompany = ?2"
*             
* @ejb:finder signature="LocalArPersonel findByPeName(java.lang.String PE_NM, java.lang.Integer PE_AD_CMPNY)"
*             query="SELECT OBJECT(pe) FROM ArPersonel pe WHERE pe.peName = ?1 AND pe.peAdCompany = ?2"
*             
* @ejb:select signature="Collection ejbSelectGeneric(java.lang.String jbossQl, java.lang.Object[] args)"
*             result-type-mapping="Local"
*             method-intf="LocalHome"
*             query="SELECT OBJECT(pe) FROM ArPersonel pe"
*
* @jboss:query signature="Collection ejbSelectGeneric(java.lang.String jbossQl, java.lang.Object[] args)"
*                         dynamic="true"
*
*
* @ejb:value-object match="*"
*             name="ArPersonel"
*
* @jboss:persistence table-name="AR_PRSNL"
*
* @jboss:entity-command name="mysql-get-generated-keys"
*/
public abstract class ArPersonelBean extends AbstractEntityBean {


	// PERSISTENT METHODS

	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 * @ejb:pk-field
	 *
	 * @jboss:column-name name="PE_CODE"
	 *
	 * @jboss:persistence auto-increment="true"
	 **/
	public abstract Integer getPeCode();
	public abstract void setPeCode(Integer PE_CODE);

	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="PE_ID_NMBR"
	 **/
	public abstract String getPeIdNumber();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setPeIdNumber(String PE_ID_NMBR);

	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="PE_NM"
	 **/
	public abstract String getPeName();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setPeName(String PE_NM);


	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="PE_DESC"
	 **/
	public abstract String getPeDescription();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setPeDescription(String PE_DESC);


	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="PE_ADDRSS"
	 **/
	public abstract String getPeAddress();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setPeAddress(String PE_ADDRSS);
	
	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="PE_RT"
	 **/
	public abstract double getPeRate();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setPeRate(double PE_RT);



	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="PE_AD_CMPNY"
	 **/
	public abstract Integer getPeAdCompany();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setPeAdCompany(Integer PE_AD_CMPNY);

	// RELATIONSHIP METHODS

	
	  /**
     * @ejb:interface-method view-type="local"
     * @ejb:relation name="personel-personeltypes"
     *               role-name="personeltype-has-one-personel"
     * 
     * @jboss:relation related-pk-field="ptCode"
     *                 fk-column="AR_PERSONEL_TYPE"
     */	
    public abstract com.ejb.ar.LocalArPersonelType getArPersonelType();
    /**
     * @ejb:interface-method view-type="local"
     **/ 
    public abstract void setArPersonelType(com.ejb.ar.LocalArPersonelType arPersonelType);
	
	
	
	
	
	/**
     * @ejb:interface-method view-type="local"
     * @ejb:relation name="personel-joborderassignments"
     *               role-name="personel-has-many-joborderassignments"
     * 
     */
    public abstract Collection getArJobOrderAssignment();
    public abstract void setArJobOrderAssignment(Collection arJobOrderAssignment);
	
	// BUSINESS METHODS

    
    /**
     * @jboss:dynamic-ql
     */
     public abstract Collection ejbSelectGeneric(java.lang.String jbossQl, java.lang.Object[] args)
        throws FinderException;
        
    
    
    /**
     * @ejb:home-method view-type="local"
     */
     public Collection ejbHomeGetPeByCriteria(java.lang.String jbossQl, java.lang.Object[] args)
        throws FinderException {
        	
        return ejbSelectGeneric(jbossQl, args);
     }
     

	// ENTITY METHODS


    /**
     * @ejb:create-method view-type="local"
     **/
    public Integer ejbCreate(Integer PE_CODE, String PE_ID_NMBR, String PE_NM, String PE_DESC,
                 String PE_ADDRSS, double PE_RT, Integer PE_AD_CMPNY)
    throws CreateException {

      Debug.print("ArPersonel ejbCreate");

      setPeCode(PE_CODE);
      setPeIdNumber(PE_ID_NMBR);
      setPeName(PE_NM);
      setPeDescription(PE_DESC);
      setPeAddress(PE_ADDRSS);
      setPeRate(PE_RT);
      setPeAdCompany(PE_AD_CMPNY);

      return null;
    }



	/**
	 * @ejb:create-method view-type="local"
	 **/
	public Integer ejbCreate( String PE_ID_NMBR, String PE_NM, String PE_DESC,
            String PE_ADDRSS, double PE_RT, Integer PE_AD_CMPNY)
	throws CreateException {

		Debug.print("ArPersonel ejbCreate");



		setPeIdNumber(PE_ID_NMBR);
		setPeName(PE_NM);
		setPeDescription(PE_DESC);
		setPeAddress(PE_ADDRSS);
		setPeRate(PE_RT);  
		setPeAdCompany(PE_AD_CMPNY);
		return null;
	}


	public void ejbPostCreate(Integer PE_CODE,  String PE_ID_NMBR, String PE_NM, String PE_DESC,
            String PE_ADDRSS, double PE_RT,Integer PE_AD_CMPNY)
	throws CreateException { }


	public void ejbPostCreate( String PE_ID_NMBR, String PE_NM, String PE_DESC,
            String PE_ADDRSS, double PE_RT,Integer PE_AD_CMPNY)
	throws CreateException { }



}
