/*
 * com/ejb/ar/LocalArPersonelTypeBean.java
 *
 * Created on March 03, 2004, 2:11 PM
 */

package com.ejb.ar;

import java.util.Collection;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;

import com.util.AbstractEntityBean;
import com.util.Debug;

/**
 *
 * @author  Neil Andrew M. Ajero
 *
 * @ejb:bean name="ArPersonelTypeEJB"
 *           display-name="PersonelType Entity"
 *           type="CMP"
 *           cmp-version="2.x"
 *           view-type="local"
 *           local-jndi-name="omega-ejb/ArPersonelTypeEJB"
 *           schema="ArPersonelType"
 *           primkey-field="ptCode"
 *
 * @ejb:pk class="java.lang.Integer"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="aruser"
 *                        role-link="aruserlink"
 *
 * @ejb:permission role-name="aruser"
 *
 * @ejb:interface local-class="com.ejb.ar.LocalArPersonelType"
 *                local-extends="javax.ejb.EJBLocalObject"
 *
 * @ejb:home local-class="com.ejb.ar.LocalArPersonelTypeHome"
 *           local-extends="javax.ejb.EJBLocalHome"
 *
 *
 * @ejb:finder signature="Collection findPtAll(java.lang.Integer PT_AD_CMPNY)"
 *             query="SELECT OBJECT(pt) FROM ArPersonelType pt WHERE pt.ptAdCompany = ?1"
 *
 * @jboss:query signature="Collection findPtAll(java.lang.Integer PT_AD_CMPNY)"
 *             query="SELECT OBJECT(pt) FROM ArPersonelType pt WHERE pt.ptAdCompany = ?1 ORDER BY pt.ptName"
 *
 * @ejb:finder signature="LocalArPersonelType findByPtShortName(java.lang.String PT_NM, java.lang.Integer PT_AD_CMPNY)"
 *             query="SELECT OBJECT(pt) FROM ArPersonelType pt WHERE pt.ptName = ?1 AND pt.ptAdCompany = ?2"
 *
 * @ejb:finder signature="LocalArPersonelType findByPtName(java.lang.String PT_NM, java.lang.Integer PT_AD_CMPNY)"
 *             query="SELECT OBJECT(pt) FROM ArPersonelType pt WHERE pt.ptName = ?1 AND pt.ptAdCompany = ?2"
 * 
 * @ejb:select signature="Collection ejbSelectGeneric(java.lang.String jbossQl, java.lang.Object[] args)"
 *             result-type-mapping="Local"
 *             method-intf="LocalHome"
 *             query="SELECT OBJECT(pt) FROM ArPersonelType pt"
 *
 * @jboss:query signature="Collection ejbSelectGeneric(java.lang.String jbossQl, java.lang.Object[] args)"
 *                         dynamic="true"
 *
 * @ejb:value-object match="*"
 *             name="ArPersonelType"
 *
 * @jboss:persistence table-name="AR_PRSNL_TYP"
 *
 * @jboss:entity-command name="mysql-get-generated-keys"
 */

public abstract class ArPersonelTypeBean extends AbstractEntityBean {
    
    
    // PERSISTENT METHODS
    
    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     * @ejb:pk-field
     *
     * @jboss:column-name name="PT_CODE"
     *
     * @jboss:persistence auto-increment="true"
     **/
    public abstract Integer getPtCode();
    public abstract void setPtCode(Integer PT_CODE);
    
    
    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"   
     *
     * @jboss:column-name name="PT_SHRT_NM"
     **/
    public abstract String getPtShortName();
     /**
      * @ejb:interface-method view-type="local"
      **/ 
    public abstract void setPtShortName(String PT_SHRT_NM);
    
    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"   
     *
     * @jboss:column-name name="PT_NM"
     **/
    public abstract String getPtName();
     /**
      * @ejb:interface-method view-type="local"
      **/ 
    public abstract void setPtName(String PT_NM);
    

    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"   
     *
     * @jboss:column-name name="PT_DESC"
     **/
    public abstract String getPtDescription();
     /**
      * @ejb:interface-method view-type="local"
      **/ 
    public abstract void setPtDescription(String PT_DESC);
    
    
    /**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="PT_RT"
	 **/
	public abstract double getPtRate();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setPtRate(double PT_RT);
	
	
    
    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="PT_AD_CMPNY"
     **/
    public abstract Integer getPtAdCompany();
     /**
      * @ejb:interface-method view-type="local"
      **/ 
    public abstract void setPtAdCompany(Integer PT_AD_CMPNY);
    
   
    // RELATIONSHIP METHODS
    
   
    /**
     * @ejb:interface-method view-type="local"
     * @ejb:relation name="personel-personeltypes"
     *               role-name="personeltype-has-many-personel"
     * 
     */
    public abstract Collection getArPersonels();
    public abstract void setArPersonels(Collection arPersonels);    
     

    /**
     * @jboss:dynamic-ql
     */
     public abstract Collection ejbSelectGeneric(java.lang.String jbossQl, java.lang.Object[] args)
        throws FinderException;
        
     

    // BUSINESS METHODS
    
    /**
     * @ejb:home-method view-type="local"
     */
     public Collection ejbHomeGetPtByCriteria(java.lang.String jbossQl, java.lang.Object[] args)
        throws FinderException {
        	
        return ejbSelectGeneric(jbossQl, args);
     }
     
  

    // ENTITY METHODS
    
    /**
     * @ejb:create-method view-type="local"
     **/
    public Integer ejbCreate(Integer PT_CODE, 
         String PT_SHRT_NM, String PT_NM, String PT_DESC, double PT_RT,
		 Integer PT_AD_CMPNY)     	
         throws CreateException {	
     	
         Debug.print("ArPersonelTypeBean ejbCreate");
     
	     setPtCode(PT_CODE);
	     setPtShortName(PT_SHRT_NM);
	     setPtName(PT_NM);
	     setPtDescription(PT_DESC);   
	     setPtRate(PT_RT);
         setPtAdCompany(PT_AD_CMPNY);

         return null;
     }
     
    /**
     * @ejb:create-method view-type="local"
     **/
    public Integer ejbCreate(
		 String PT_SHRT_NM, String PT_NM, String PT_DESC, double PT_RT,
		 Integer PT_AD_CMPNY)     	
         throws CreateException {	
     	
         Debug.print("ArCustomerTypeBean ejbCreate");
     
         setPtShortName(PT_SHRT_NM);
	     setPtName(PT_NM);
	     setPtDescription(PT_DESC);   
	     setPtRate(PT_RT);
         setPtAdCompany(PT_AD_CMPNY);

         return null;
     }
     
     public void ejbPostCreate(Integer PT_CODE, 
             String PT_SHRT_NM, String PT_NM, String PT_DESC, double PT_RT,
    		 Integer PT_AD_CMPNY)     	
             throws CreateException {}
      
     public void ejbPostCreate(
    		 String PT_SHRT_NM, String PT_NM, String PT_DESC, double PT_RT,
    		 Integer PT_AD_CMPNY)     	
             throws CreateException {}     
}