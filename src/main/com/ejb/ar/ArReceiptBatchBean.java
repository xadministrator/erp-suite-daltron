/*
 * com/ejb/ar/LocalArReceiptBatchBean.java
 *
 * Created on May 20, 2004, 10:28 AM
 */
package com.ejb.ar;

import java.util.Collection;
import java.util.Date;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;

import com.util.AbstractEntityBean;
import com.util.Debug;

/**
 *
 * @author  Neil Andrew M.Ajero
 *
 * @ejb:bean name="ArReceiptBatchEJB"
 *           display-name="Receipt Batch Entity"
 *           type="CMP"
 *           cmp-version="2.x"
 *           view-type="local"
 *           local-jndi-name="omega-ejb/ArReceiptBatchEJB"
 *           schema="ArReceiptBatch"
 *           primkey-field="rbCode"
 *       
 * @ejb:pk class="Integer"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="aruser"
 *                        role-link="aruserlink"
 *
 * @ejb:permission role-name="aruser"
 *
 * @ejb:interface local-class="com.ejb.ar.LocalArReceiptBatch"
 *                local-extends="javax.ejb.EJBLocalObject"
 *
 * @ejb:home local-class="com.ejb.ar.LocalArReceiptBatchHome"
 *           local-extends="javax.ejb.EJBLocalHome"
 *
 * @ejb:finder signature="LocalArReceiptBatch findByRbName(java.lang.String RB_NM, java.lang.Integer IB_AD_BRNCH, java.lang.Integer RB_AD_CMPNY)"
 *             query="SELECT OBJECT(rb) FROM ArReceiptBatch rb WHERE rb.rbName=?1 AND rb.rbAdBranch=?2 AND rb.rbAdCompany=?3"
 * 
 * @ejb:finder signature="Collection findOpenRbByRbType(java.lang.String RB_TYP, java.lang.Integer AD_BRNCH, java.lang.Integer RB_AD_CMPNY)"
 *             query="SELECT OBJECT(rb) FROM ArReceiptBatch rb WHERE rb.rbType=?1 AND rb.rbStatus='OPEN' AND rb.rbAdBranch = ?2 AND rb.rbAdCompany=?3"
 *
 * @jboss:query signature="Collection findOpenRbByRbType(java.lang.String RB_TYP, java.lang.Integer AD_BRNCH, java.lang.Integer RB_AD_CMPNY)"
 *             query="SELECT OBJECT(rb) FROM ArReceiptBatch rb WHERE rb.rbType=?1 AND rb.rbStatus='OPEN' AND rb.rbAdBranch = ?2 AND rb.rbAdCompany=?3 ORDER BY rb.rbName"
 *
 * @ejb:finder signature="Collection findOpenRbAll(java.lang.Integer AD_BRNCH, java.lang.Integer RB_AD_CMPNY)"
 *             query="SELECT OBJECT(rb) FROM ArReceiptBatch rb WHERE rb.rbStatus='OPEN' AND rb.rbAdBranch = ?1 AND rb.rbAdCompany=?2"
 * 
 * @jboss:query signature="Collection findOpenRbAll(java.lang.Integer AD_BRNCH, java.lang.Integer RB_AD_CMPNY)"
 *             query="SELECT OBJECT(rb) FROM ArReceiptBatch rb WHERE rb.rbStatus='OPEN' AND rb.rbAdBranch = ?1 AND rb.rbAdCompany=?2 ORDER BY rb.rbName"
 *
 * @ejb:select signature="Collection ejbSelectGeneric(java.lang.String jbossQl, java.lang.Object[] args)"
 *             result-type-mapping="Local"
 *             method-intf="LocalHome"
 *             query="SELECT OBJECT(rb) FROM ArReceiptBatch rb"
 *
 * @jboss:query signature="Collection ejbSelectGeneric(java.lang.String jbossQl, java.lang.Object[] args)"
 *                         dynamic="true"
 *
 * @ejb:value-object match="*"
 *             name="ArReceiptBatch"
 *
 * @jboss:persistence table-name="AR_RCPT_BTCH"
 *
 * @jboss:entity-command name="mysql-get-generated-keys"
 *
 */

public abstract class ArReceiptBatchBean extends AbstractEntityBean {


   // Access methods for persistent fields

   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    * @ejb:pk-field
    *
    * @jboss:column-name name="RB_CODE"
    *
    * @jboss:persistence auto-increment="true"
    **/
   public abstract Integer getRbCode();
   public abstract void setRbCode(Integer RB_CODE);

   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="RB_NM"
    **/
   public abstract String getRbName();
   /**
    * @ejb:interface-method view-type="local"
    **/
   public abstract void setRbName(String RB_NM);

   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="RB_DESC"
    **/
   public abstract String getRbDescription();
   /**
    * @ejb:interface-method view-type="local"
    **/
   public abstract void setRbDescription(String RB_DESC);
   
   
   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="RB_STATUS"
    **/
   public abstract String getRbStatus();
   /**
    * @ejb:interface-method view-type="local"
    **/
   public abstract void setRbStatus(String RB_STATUS);
   
   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="RB_TYP"
    **/
   public abstract String getRbType();
   /**
    * @ejb:interface-method view-type="local"
    **/
   public abstract void setRbType(String RB_TYP);

   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="RB_DT_CRTD"
    **/
   public abstract Date getRbDateCreated();
   /**
    * @ejb:interface-method view-type="local"
    **/
   public abstract void setRbDateCreated(Date RB_DT_CRTD);
   
   
   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="RB_CRTD_BY"
    **/
   public abstract String getRbCreatedBy();
   /**
    * @ejb:interface-method view-type="local"
    **/
   public abstract void setRbCreatedBy(String RB_CRTD_BY);
   
   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="RB_AD_BRNCH"
    **/
   public abstract Integer getRbAdBranch();
   /**
    * @ejb:interface-method view-type="local"
    **/
   public abstract void setRbAdBranch(Integer RB_AD_BRNCH);

   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="RB_AD_CMPNY"
    **/
   public abstract Integer getRbAdCompany();
   /**
    * @ejb:interface-method view-type="local"
    **/
   public abstract void setRbAdCompany(Integer RB_AD_CMPNY);
   
   
   
   // Access methods for relationship fields

   /**
    * @ejb:interface-method view-type="local"
    * @ejb:relation name="receiptbatch-receipts"
    *               role-name="receiptbatch-has-many-receipts"
    */
   public abstract Collection getArReceipts();
   public abstract void setArReceipts(Collection arReceipts);
   
  
  /**
    * @jboss:dynamic-ql
    */
    public abstract Collection ejbSelectGeneric(java.lang.String jbossQl, java.lang.Object[] args)
       throws FinderException;

   // Business methods
   
   /**
    * @ejb:home-method view-type="local"
    */
    public Collection ejbHomeGetRbByCriteria(java.lang.String jbossQl, java.lang.Object[] args)
       throws FinderException {
       	
       return ejbSelectGeneric(jbossQl, args);
    }
      
   
   /**
    * @ejb:interface-method view-type="local"
    **/
   public void addArReceipt(LocalArReceipt arReceipt) {

      Debug.print("ArReceiptBatchBean addArReceipt");
      try {
         Collection arReceipts = getArReceipts();
	 arReceipts.add(arReceipt);
      } catch (Exception ex) {
         throw new EJBException(ex.getMessage());
      }
   }

   /**
    * @ejb:interface-method view-type="local"
    **/
   public void dropArReceipt(LocalArReceipt arReceipt) {

      Debug.print("ArReceiptBatchBean dropArReceipt");
      try {
         Collection arReceipts = getArReceipts();
	 arReceipts.remove(arReceipt);
      } catch (Exception ex) {
         throw new EJBException(ex.getMessage());
      }
   }
   
   // EntityBean methods

   /**
    * @ejb:create-method view-type="local"
    **/
   public Integer ejbCreate (Integer RB_CODE, String RB_NM, String RB_DESC,
      String RB_STATUS, String RB_TYP, Date RB_DT_CRTD, String RB_CRTD_BY,
	  Integer RB_AD_BRNCH, Integer RB_AD_CMPNY)
      throws CreateException {

      Debug.print("ArReceiptBatchBean ejbCreate");
      setRbCode(RB_CODE);
      setRbName(RB_NM);
      setRbDescription(RB_DESC);
      setRbStatus(RB_STATUS);
      setRbType(RB_TYP);
      setRbDateCreated(RB_DT_CRTD);
      setRbCreatedBy(RB_CRTD_BY);
      setRbAdBranch(RB_AD_BRNCH);
      setRbAdCompany(RB_AD_CMPNY);
      
      return null;
   }

   /**
    * @ejb:create-method view-type="local"
    **/
   public Integer ejbCreate (String RB_NM, String RB_DESC,
      String RB_STATUS, String RB_TYP, Date RB_DT_CRTD, String RB_CRTD_BY,
	  Integer RB_AD_BRNCH, Integer RB_AD_CMPNY)
      throws CreateException {

      Debug.print("ArReceiptBatchBean ejbCreate");

      setRbName(RB_NM);
      setRbDescription(RB_DESC);
      setRbStatus(RB_STATUS);
      setRbType(RB_TYP);
      setRbDateCreated(RB_DT_CRTD);
      setRbCreatedBy(RB_CRTD_BY);
      setRbAdBranch(RB_AD_BRNCH);
      setRbAdCompany(RB_AD_CMPNY);
      
      return null;
   }

   public void ejbPostCreate (Integer RB_CODE, String RB_NM, String RB_DESC,
      String RB_STATUS, String RB_TYP, Date RB_DT_CRTD, String RB_CRTD_BY,
	  Integer RB_AD_BRNCH, Integer RB_AD_CMPNY)
      throws CreateException { }

   public void ejbPostCreate (String RB_NM, String RB_DESC,
      String RB_STATUS, String RB_TYP, Date RB_DT_CRTD, String RB_CRTD_BY,
	  Integer RB_AD_BRNCH, Integer RB_AD_CMPNY)
      throws CreateException { }

} // ArReceiptBatchBean class
