/*
 * com/ejb/ar/LocalArReceiptBean.java
 *
 * Created on March 03, 2004, 04:05 PM
 */

package com.ejb.ar;

import java.util.Collection;
import java.util.Date;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;

import com.ejb.cm.LocalCmFundTransferReceipt;
import com.ejb.gl.LocalGlInvestorAccountBalance;
import com.ejb.hr.LocalHrPayrollPeriod;
import com.util.AbstractEntityBean;
import com.util.Debug;

/**
 *
 * @author  Neil Andrew M.Ajero
 *
 * @ejb:bean name="ArReceiptEJB"
 *           display-name="Receipt Entity"
 *           type="CMP"
 *           cmp-version="2.x"
 *           view-type="local"
 *           local-jndi-name="omega-ejb/ArReceiptEJB"
 *           schema="ArReceipt"
 *           primkey-field="rctCode"
 *
 * @ejb:pk class="java.lang.Integer"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="aruser"
 *                        role-link="aruserlink"
 *
 * @ejb:permission role-name="aruser"
 *
 * @ejb:interface local-class="com.ejb.ar.LocalArReceipt"
 *                local-extends="javax.ejb.EJBLocalObject"
 *
 * @ejb:home local-class="com.ejb.ar.LocalArReceiptHome"
 *           local-extends="javax.ejb.EJBLocalHome"
 *
 * @ejb:finder signature="LocalArReceipt findByRctNumberAndBrCode(java.lang.String RCT_NMBR, java.lang.Integer RCT_AD_BRNCH, java.lang.Integer RCT_AD_CMPNY)"
 *             query="SELECT OBJECT(rct) FROM ArReceipt rct WHERE rct.rctNumber = ?1 AND rct.rctAdBranch = ?2 AND rct.rctAdCompany = ?3"
 *
 * @ejb:finder signature="Collection findPostedRctByRctDateRange(java.util.Date RCT_DT_FRM, java.util.Date RCT_DT_TO, java.lang.Integer RCT_AD_CMPNY)"
 *             query="SELECT OBJECT(rct) FROM ArReceipt rct  WHERE rct.rctPosted = 1 AND rct.rctDate >= ?1 AND rct.rctDate <= ?2 AND rct.rctAdCompany = ?3"
 *
 * @jboss:query signature="Collection findPostedRctByRctDateRange(java.util.Date RCT_DT_FRM, java.util.Date RCT_DT_TO, java.lang.Integer RCT_AD_CMPNY)"
 *             query="SELECT OBJECT(rct) FROM ArReceipt rct  WHERE rct.rctPosted = 1 AND rct.rctDate >= ?1 AND rct.rctDate <= ?2 AND rct.rctAdCompany = ?3 ORDER BY rct.rctDate"
 *
 * @ejb:finder signature="LocalArReceipt findByReferenceNumberAndCompanyCode(java.lang.String INV_RFRNC_NMBR, java.lang.Integer INV_AD_CMPNY)"
 *             query="SELECT OBJECT(rct) FROM ArReceipt rct  WHERE rct.rctPosted = 0 AND rct.rctVoid = 0 AND rct.rctReferenceNumber = ?1 AND rct.rctAdCompany = ?2"
 *
 * @ejb:finder signature="LocalArReceipt findByPayfileReferenceNumberAndCompanyCode(java.lang.String RCT_PYFL_RFRNC_NMBR, java.lang.Integer INV_AD_CMPNY)"
 *             query="SELECT OBJECT(rct) FROM ArReceipt rct  WHERE rct.rctVoid = 0 AND rct.rctPayfileReferenceNumber = ?1 AND rct.rctAdCompany = ?2"
 *
 * @ejb:finder signature="Collection findVoidPostedRctByRctDateRange(java.util.Date RCT_DT_FRM, java.util.Date RCT_DT_TO, java.lang.Integer RCT_AD_CMPNY)"
 *             query="SELECT OBJECT(rct) FROM ArReceipt rct  WHERE rct.rctPosted = 1 AND rct.rctVoid = 1 AND rct.rctVoidPosted = 1 AND rct.rctDate >= ?1 AND rct.rctDate <= ?2 AND rct.rctAdCompany = ?3"
 *
 * @jboss:query signature="Collection findVoidPostedRctByRctDateRange(java.util.Date RCT_DT_FRM, java.util.Date RCT_DT_TO, java.lang.Integer RCT_AD_CMPNY)"
 *             query="SELECT OBJECT(rct) FROM ArReceipt rct  WHERE rct.rctPosted = 1 AND rct.rctVoid = 1 AND rct.rctVoidPosted = 1 AND rct.rctDate >= ?1 AND rct.rctDate <= ?2 AND rct.rctAdCompany = ?3 ORDER BY rct.rctDate"
 *
 * @ejb:finder signature="Collection findUnPostedRctByRctDateRangeAndSplCode(java.util.Date RCT_DT_FRM, java.util.Date RCT_DT_TO, java.lang.Integer SPL_CODE, java.lang.Integer RCT_AD_BRNCH, java.lang.Integer RCT_AD_CMPNY)"
 *             query="SELECT OBJECT(rct) FROM ArReceipt rct WHERE rct.rctVoid = 0 AND rct.rctPosted = 0 AND rct.rctDate >= ?1 AND rct.rctDate <= ?2 AND rct.arCustomer.apSupplier.splCode = ?3 AND rct.rctAdBranch = ?4 AND rct.rctAdCompany = ?5 ORDER BY rct.rctDate, rct.rctNumber"
 *
 * @jboss:query signature="Collection findUnPostedRctByRctDateRangeAndSplCode(java.util.Date RCT_DT_FRM, java.util.Date RCT_DT_TO, java.lang.Integer SPL_CODE, java.lang.Integer RCT_AD_BRNCH, java.lang.Integer RCT_AD_CMPNY)"
 *             query="SELECT OBJECT(rct) FROM ArReceipt rct WHERE rct.rctVoid = 0 AND rct.rctPosted = 0 AND rct.rctDate >= ?1 AND rct.rctDate <= ?2 AND rct.arCustomer.apSupplier.splCode = ?3 AND rct.rctAdBranch = ?4 AND rct.rctAdCompany = ?5 ORDER BY rct.rctDate, rct.rctNumber"
 *
 * @ejb:finder signature="Collection findPostedRctByRctDateRangeAndSplCode(java.util.Date RCT_DT_FRM, java.util.Date RCT_DT_TO, java.lang.Integer SPL_CODE, java.lang.Integer RCT_AD_BRNCH, java.lang.Integer RCT_AD_CMPNY)"
 *             query="SELECT OBJECT(rct) FROM ArReceipt rct WHERE rct.rctVoid = 0 AND rct.rctPosted = 1 AND rct.rctDate >= ?1 AND rct.rctDate <= ?2 AND rct.arCustomer.apSupplier.splCode = ?3 AND rct.rctAdBranch = ?4 AND rct.rctAdCompany = ?5 ORDER BY rct.rctDate, rct.rctNumber"
 *
 * @jboss:query signature="Collection findPostedRctByRctDateRangeAndSplCode(java.util.Date RCT_DT_FRM, java.util.Date RCT_DT_TO, java.lang.Integer SPL_CODE, java.lang.Integer RCT_AD_BRNCH, java.lang.Integer RCT_AD_CMPNY)"
 *             query="SELECT OBJECT(rct) FROM ArReceipt rct WHERE rct.rctVoid = 0 AND rct.rctPosted = 1 AND rct.rctDate >= ?1 AND rct.rctDate <= ?2 AND rct.arCustomer.apSupplier.splCode = ?3 AND rct.rctAdBranch = ?4 AND rct.rctAdCompany = ?5 ORDER BY rct.rctDate, rct.rctNumber"
 *
 *
 * @ejb:finder signature="Collection findPostedRctByDateAndBaName(java.util.Date DT, java.lang.String BA_NM, java.lang.Integer RCT_AD_CMPNY)"
 *             query="SELECT OBJECT(rct) FROM ArReceipt rct WHERE rct.rctDate <= ?1 AND rct.rctPosted = 1 AND rct.rctVoid = 0 AND rct.adBankAccount.baName = ?2  AND rct.rctAdCompany = ?3"
 *
 *
 * @ejb:finder signature="Collection findUnreconciledPostedRctByDateAndBaNameAndBrCode(java.util.Date DT, java.lang.String BA_NM, java.lang.Integer RCT_AD_BRNCH, java.lang.Integer RCT_AD_CMPNY)"
 *             query="SELECT OBJECT(rct) FROM ArReceipt rct WHERE rct.rctReconciled = 0 AND rct.rctDate <= ?1 AND rct.rctPosted = 1 AND rct.rctVoid = 0 AND rct.adBankAccount.baIsCashAccount = 0 AND rct.adBankAccount.baName = ?2 AND rct.rctAdBranch = ?3 AND rct.rctAdCompany = ?4"
 *
 * @jboss:query signature="Collection findUnreconciledPostedRctByDateAndBaNameAndBrCode(java.util.Date DT, java.lang.String BA_NM, java.lang.Integer RCT_AD_BRNCH, java.lang.Integer RCT_AD_CMPNY)"
 *             query="SELECT OBJECT(rct) FROM ArReceipt rct WHERE rct.rctReconciled = 0 AND rct.rctDate <= ?1 AND rct.rctPosted = 1 AND rct.rctVoid = 0 AND rct.adBankAccount.baIsCashAccount = 0 AND rct.adBankAccount.baName = ?2 AND rct.rctAdBranch = ?3 AND rct.rctAdCompany = ?4 ORDER BY rct.rctDate, rct.rctNumber"
 *
 * w
 * @ejb:finder signature="Collection findUnreconciledPostedCard1RctByDateAndBaNameAndBrCode(java.util.Date DT, java.lang.String BA_NM, java.lang.Integer RCT_AD_BRNCH, java.lang.Integer RCT_AD_CMPNY)"
 *             query="SELECT OBJECT(rct) FROM ArReceipt rct WHERE rct.rctReconciledCard1 = 0 AND rct.rctDate <= ?1 AND rct.rctPosted = 1 AND rct.rctVoid = 0 AND rct.adBankAccountCard1.baIsCashAccount = 0 AND rct.adBankAccountCard1.baName = ?2 AND rct.rctAdBranch = ?3 AND rct.rctAdCompany = ?4"
 *
 * @jboss:query signature="Collection findUnreconciledPostedCard1RctByDateAndBaNameAndBrCode(java.util.Date DT, java.lang.String BA_NM, java.lang.Integer RCT_AD_BRNCH, java.lang.Integer RCT_AD_CMPNY)"
 *             query="SELECT OBJECT(rct) FROM ArReceipt rct WHERE rct.rctReconciledCard1 = 0 AND rct.rctDate <= ?1 AND rct.rctPosted = 1 AND rct.rctVoid = 0 AND rct.adBankAccountCard1.baIsCashAccount = 0 AND rct.adBankAccountCard1.baName = ?2 AND rct.rctAdBranch = ?3 AND rct.rctAdCompany = ?4 ORDER BY rct.rctDate, rct.rctNumber"
 *
 * @ejb:finder signature="Collection findUnreconciledPostedCard2RctByDateAndBaNameAndBrCode(java.util.Date DT, java.lang.String BA_NM, java.lang.Integer RCT_AD_BRNCH, java.lang.Integer RCT_AD_CMPNY)"
 *             query="SELECT OBJECT(rct) FROM ArReceipt rct WHERE rct.rctReconciledCard2 = 0 AND rct.rctDate <= ?1 AND rct.rctPosted = 1 AND rct.rctVoid = 0 AND rct.adBankAccountCard2.baIsCashAccount = 0 AND rct.adBankAccountCard2.baName = ?2 AND rct.rctAdBranch = ?3 AND rct.rctAdCompany = ?4"
 *
 * @jboss:query signature="Collection findUnreconciledPostedCard2RctByDateAndBaNameAndBrCode(java.util.Date DT, java.lang.String BA_NM, java.lang.Integer RCT_AD_BRNCH, java.lang.Integer RCT_AD_CMPNY)"
 *             query="SELECT OBJECT(rct) FROM ArReceipt rct WHERE rct.rctReconciledCard2 = 0 AND rct.rctDate <= ?1 AND rct.rctPosted = 1 AND rct.rctVoid = 0 AND rct.adBankAccountCard2.baIsCashAccount = 0 AND rct.adBankAccountCard2.baName = ?2 AND rct.rctAdBranch = ?3 AND rct.rctAdCompany = ?4 ORDER BY rct.rctDate, rct.rctNumber"
 *
 * @ejb:finder signature="Collection findUnreconciledPostedCard3RctByDateAndBaNameAndBrCode(java.util.Date DT, java.lang.String BA_NM, java.lang.Integer RCT_AD_BRNCH, java.lang.Integer RCT_AD_CMPNY)"
 *             query="SELECT OBJECT(rct) FROM ArReceipt rct WHERE rct.rctReconciledCard3 = 0 AND rct.rctDate <= ?1 AND rct.rctPosted = 1 AND rct.rctVoid = 0 AND rct.adBankAccountCard3.baIsCashAccount = 0 AND rct.adBankAccountCard3.baName = ?2 AND rct.rctAdBranch = ?3 AND rct.rctAdCompany = ?4"
 *
 * @jboss:query signature="Collection findUnreconciledPostedCard3RctByDateAndBaNameAndBrCode(java.util.Date DT, java.lang.String BA_NM, java.lang.Integer RCT_AD_BRNCH, java.lang.Integer RCT_AD_CMPNY)"
 *             query="SELECT OBJECT(rct) FROM ArReceipt rct WHERE rct.rctReconciledCard3 = 0 AND rct.rctDate <= ?1 AND rct.rctPosted = 1 AND rct.rctVoid = 0 AND rct.adBankAccountCard3.baIsCashAccount = 0 AND rct.adBankAccountCard3.baName = ?2 AND rct.rctAdBranch = ?3 AND rct.rctAdCompany = ?4 ORDER BY rct.rctDate, rct.rctNumber"
 *
 * @ejb:finder signature="Collection findUnreconciledPostedChequeRctByDateAndBaNameAndBrCode(java.util.Date DT, java.lang.String BA_NM, java.lang.Integer RCT_AD_BRNCH, java.lang.Integer RCT_AD_CMPNY)"
 *             query="SELECT OBJECT(rct) FROM ArReceipt rct WHERE rct.rctReconciledCheque = 0 AND rct.rctDate <= ?1 AND rct.rctPosted = 1 AND rct.rctVoid = 0 AND rct.adBankAccount.baIsCashAccount = 0 AND rct.adBankAccount.baName = ?2 AND rct.rctAdBranch = ?3 AND rct.rctAdCompany = ?4"
 *
 * @jboss:query signature="Collection findUnreconciledPostedChequeRctByDateAndBaNameAndBrCode(java.util.Date DT, java.lang.String BA_NM, java.lang.Integer RCT_AD_BRNCH, java.lang.Integer RCT_AD_CMPNY)"
 *             query="SELECT OBJECT(rct) FROM ArReceipt rct WHERE rct.rctReconciledCheque = 0 AND rct.rctDate <= ?1 AND rct.rctPosted = 1 AND rct.rctVoid = 0 AND rct.adBankAccount.baIsCashAccount = 0 AND rct.adBankAccount.baName = ?2 AND rct.rctAdBranch = ?3 AND rct.rctAdCompany = ?4 ORDER BY rct.rctDate, rct.rctNumber"
 *
 *
 *
 *
 * @ejb:finder signature="Collection findUnreconciledPostedRctByDateAndBaName(java.util.Date DT, java.lang.String BA_NM, java.lang.Integer RCT_AD_CMPNY)"
 *             query="SELECT OBJECT(rct) FROM ArReceipt rct WHERE rct.rctReconciled = 0 AND rct.rctAmountCash > 0 AND rct.rctDate <= ?1 AND rct.rctPosted = 1 AND rct.rctVoid = 0 AND rct.adBankAccount.baIsCashAccount = 0 AND rct.adBankAccount.baName = ?2 AND rct.rctAdCompany = ?3"
 *
 * @jboss:query signature="Collection findUnreconciledPostedRctByDateAndBaName(java.util.Date DT, java.lang.String BA_NM, java.lang.Integer RCT_AD_CMPNY)"
 *             query="SELECT OBJECT(rct) FROM ArReceipt rct WHERE rct.rctReconciled = 0 AND rct.rctAmountCash > 0 AND rct.rctDate <= ?1 AND rct.rctPosted = 1 AND rct.rctVoid = 0 AND rct.adBankAccount.baIsCashAccount = 0 AND rct.adBankAccount.baName = ?2 AND rct.rctAdCompany = ?3 ORDER BY rct.rctDate, rct.rctNumber"
 *
 * @ejb:finder signature="Collection findUnreconciledPostedCard1RctByDateAndBaName(java.util.Date DT, java.lang.String BA_NM, java.lang.Integer RCT_AD_CMPNY)"
 *             query="SELECT OBJECT(rct) FROM ArReceipt rct WHERE rct.rctReconciledCard1 = 0 AND rct.rctAmountCard1 > 0 AND rct.rctDate <= ?1 AND rct.rctPosted = 1 AND rct.rctVoid = 0 AND rct.adBankAccountCard1.baIsCashAccount = 0 AND rct.adBankAccountCard1.baName = ?2 AND rct.rctAdCompany = ?3"
 *
 * @jboss:query signature="Collection findUnreconciledPostedCard1RctByDateAndBaName(java.util.Date DT, java.lang.String BA_NM, java.lang.Integer RCT_AD_CMPNY)"
 *             query="SELECT OBJECT(rct) FROM ArReceipt rct WHERE rct.rctReconciledCard1 = 0 AND rct.rctAmountCard1 > 0 AND rct.rctDate <= ?1 AND rct.rctPosted = 1 AND rct.rctVoid = 0 AND rct.adBankAccountCard1.baIsCashAccount = 0 AND rct.adBankAccountCard1.baName = ?2 AND rct.rctAdCompany = ?3 ORDER BY rct.rctDate, rct.rctNumber"
 *
 * @ejb:finder signature="Collection findUnreconciledPostedCard2RctByDateAndBaName(java.util.Date DT, java.lang.String BA_NM, java.lang.Integer RCT_AD_CMPNY)"
 *             query="SELECT OBJECT(rct) FROM ArReceipt rct WHERE rct.rctReconciledCard2 = 0 AND rct.rctAmountCard2 > 0 AND rct.rctDate <= ?1 AND rct.rctPosted = 1 AND rct.rctVoid = 0 AND rct.adBankAccountCard2.baIsCashAccount = 0 AND rct.adBankAccountCard2.baName = ?2 AND rct.rctAdCompany = ?3"
 *
 * @jboss:query signature="Collection findUnreconciledPostedCard2RctByDateAndBaName(java.util.Date DT, java.lang.String BA_NM, java.lang.Integer RCT_AD_CMPNY)"
 *             query="SELECT OBJECT(rct) FROM ArReceipt rct WHERE rct.rctReconciledCard2 = 0 AND rct.rctAmountCard2 > 0 AND rct.rctDate <= ?1 AND rct.rctPosted = 1 AND rct.rctVoid = 0 AND rct.adBankAccountCard2.baIsCashAccount = 0 AND rct.adBankAccountCard2.baName = ?2 AND rct.rctAdCompany = ?3 ORDER BY rct.rctDate, rct.rctNumber"
 *
 * @ejb:finder signature="Collection findUnreconciledPostedCard3RctByDateAndBaName(java.util.Date DT, java.lang.String BA_NM, java.lang.Integer RCT_AD_CMPNY)"
 *             query="SELECT OBJECT(rct) FROM ArReceipt rct WHERE rct.rctReconciledCard3 = 0 AND rct.rctAmountCard3 > 0 AND rct.rctDate <= ?1 AND rct.rctPosted = 1 AND rct.rctVoid = 0 AND rct.adBankAccountCard3.baIsCashAccount = 0 AND rct.adBankAccountCard3.baName = ?2 AND rct.rctAdCompany = ?3"
 *
 * @jboss:query signature="Collection findUnreconciledPostedCard3RctByDateAndBaName(java.util.Date DT, java.lang.String BA_NM, java.lang.Integer RCT_AD_CMPNY)"
 *             query="SELECT OBJECT(rct) FROM ArReceipt rct WHERE rct.rctReconciledCard3 = 0 AND rct.rctAmountCard3 > 0 AND rct.rctDate <= ?1 AND rct.rctPosted = 1 AND rct.rctVoid = 0 AND rct.adBankAccountCard3.baIsCashAccount = 0 AND rct.adBankAccountCard3.baName = ?2 AND rct.rctAdCompany = ?3 ORDER BY rct.rctDate, rct.rctNumber"
 *
 * @ejb:finder signature="Collection findUnreconciledPostedChequeRctByDateAndBaName(java.util.Date DT, java.lang.String BA_NM, java.lang.Integer RCT_AD_CMPNY)"
 *             query="SELECT OBJECT(rct) FROM ArReceipt rct WHERE rct.rctReconciledCheque = 0 AND rct.rctAmountCheque > 0 AND rct.rctDate <= ?1 AND rct.rctPosted = 1 AND rct.rctVoid = 0 AND rct.adBankAccount.baIsCashAccount = 0 AND rct.adBankAccount.baName = ?2 AND rct.rctAdCompany = ?3"
 *
 * @jboss:query signature="Collection findUnreconciledPostedChequeRctByDateAndBaName(java.util.Date DT, java.lang.String BA_NM, java.lang.Integer RCT_AD_CMPNY)"
 *             query="SELECT OBJECT(rct) FROM ArReceipt rct WHERE rct.rctReconciledCheque = 0 AND rct.rctAmountCheque > 0 AND rct.rctDate <= ?1 AND rct.rctPosted = 1 AND rct.rctVoid = 0 AND rct.adBankAccount.baIsCashAccount = 0 AND rct.adBankAccount.baName = ?2 AND rct.rctAdCompany = ?3 ORDER BY rct.rctDate, rct.rctNumber"
 *
 * @ejb:finder signature="Collection findByInvCodeAndRctVoid(java.lang.Integer INV_CODE, byte RCT_VD, java.lang.Integer RCT_AD_CMPNY)"
 *             query="SELECT OBJECT(rct) FROM ArReceipt rct, IN(rct.arAppliedInvoices) ai WHERE ai.arInvoicePaymentSchedule.arInvoice.invCode = ?1 AND rct.rctVoid = ?2 AND rct.rctAdCompany = ?3"
 *
 * @ejb:finder signature="Collection findByInvCodeAndRctVoidAndRctPosted(java.lang.Integer INV_CODE, byte RCT_VD, byte RCT_PSTD, java.lang.Integer RCT_AD_CMPNY)"
 *             query="SELECT OBJECT(rct) FROM ArReceipt rct, IN(rct.arAppliedInvoices) ai WHERE ai.arInvoicePaymentSchedule.arInvoice.invCode = ?1 AND rct.rctVoid = ?2 AND rct.rctPosted = ?3 AND rct.rctAdCompany = ?4"
 *
 * @ejb:finder signature="Collection findByRctPostedAndRctVoidAndRbName(byte RCT_PSTD, byte RCT_VD, java.lang.String RB_NM, java.lang.Integer RCT_AD_CMPNY)"
 *             query="SELECT OBJECT(rct) FROM ArReceipt rct WHERE rct.rctPosted=?1 AND rct.rctVoid = ?2 AND rct.arReceiptBatch.rbName = ?3 AND rct.rctAdCompany = ?4"
 *
 * @ejb:finder signature="Collection findPostedRctByRctTypeAndRctDateRange(java.lang.String RCT_TYP, java.util.Date RCT_DT_FRM, java.util.Date RCT_DT_TO, java.lang.Integer RCT_AD_CMPNY)"
 *             query="SELECT OBJECT(rct) FROM ArReceipt rct  WHERE rct.rctPosted = 1 AND rct.rctVoid = 0 AND rct.rctType = ?1 AND rct.rctDate >= ?2 AND rct.rctDate <= ?3 AND rct.rctAdCompany = ?4"
 *
 * @jboss:query signature="Collection findPostedRctByRctTypeAndRctDateRange(java.lang.String RCT_TYP, java.util.Date RCT_DT_FRM, java.util.Date RCT_DT_TO, java.lang.Integer RCT_AD_CMPNY)"
 *             query="SELECT OBJECT(rct) FROM ArReceipt rct  WHERE rct.rctPosted = 1 AND rct.rctVoid = 0 AND rct.rctType = ?1 AND rct.rctDate >= ?2 AND rct.rctDate <= ?3 AND rct.rctAdCompany = ?4 ORDER BY rct.rctDate"
 *
 *
 * @ejb:finder signature="Collection findPostedRctByRctTypeAndRctDateToAndCstCode(java.lang.String RCT_TYP, java.util.Date RCT_DT_TO, java.lang.String CST_CSTMR_CODE, java.lang.Integer RCT_AD_CMPNY)"
 *             query="SELECT OBJECT(rct) FROM ArReceipt rct  WHERE rct.rctPosted = 1 AND rct.rctVoid = 0 AND rct.rctType = ?1 AND rct.rctDate <= ?2 AND rct.arCustomer.cstCustomerCode = ?3 AND rct.rctAdCompany = ?4"
 *
 * @jboss:query signature="Collection findPostedRctByRctTypeAndRctDateToAndCstCode(java.lang.String RCT_TYP, java.util.Date RCT_DT_TO, java.lang.String CST_CSTMR_CODE, java.lang.Integer RCT_AD_CMPNY)"
 *             query="SELECT OBJECT(rct) FROM ArReceipt rct  WHERE rct.rctPosted = 1 AND rct.rctVoid = 0 AND rct.rctType = ?1 AND rct.rctDate <= ?2 AND rct.arCustomer.cstCustomerCode = ?3 AND rct.rctAdCompany = ?4 ORDER BY rct.rctDate"
 *
 * @ejb:finder signature="Collection findPostedRctByRctDateToAndCstCode(java.util.Date RCT_DT_TO, java.lang.String CST_CSTMR_CODE, java.lang.Integer RCT_AD_CMPNY)"
 *             query="SELECT OBJECT(rct) FROM ArReceipt rct  WHERE rct.rctPosted = 1 AND rct.rctVoid = 0 AND rct.rctDate <= ?1 AND rct.arCustomer.cstCustomerCode = ?2 AND rct.rctAdCompany = ?3"
 *
 * @jboss:query signature="Collection findPostedRctByRctDateToAndCstCode(java.util.Date RCT_DT_TO, java.lang.String CST_CSTMR_CODE, java.lang.Integer RCT_AD_CMPNY)"
 *             query="SELECT OBJECT(rct) FROM ArReceipt rct  WHERE rct.rctPosted = 1 AND rct.rctVoid = 0 AND rct.rctDate <= ?1 AND rct.arCustomer.cstCustomerCode = ?2 AND rct.rctAdCompany = ?3 ORDER BY rct.rctDate"
 *
 * @ejb:finder signature="Collection findNotVoidByCustomerCode(java.lang.String CST_CSTMR_CODE, java.lang.Integer RCT_AD_CMPNY)"
 *             query="SELECT OBJECT(rct) FROM ArReceipt rct  WHERE rct.arCustomer.cstCustomerCode = ?1 AND rct.rctVoid = 0 AND rct.rctAdCompany = ?2"
 *
 * @jboss:query signature="Collection findNotVoidByCustomerCode(java.lang.String CST_CSTMR_CODE, java.lang.Integer RCT_AD_CMPNY)"
 *             query="SELECT OBJECT(rct) FROM ArReceipt rct  WHERE rct.arCustomer.cstCustomerCode = ?1 AND rct.rctVoid = 0 AND rct.rctAdCompany = ?2 ORDER BY rct.rctNumber, rct.rctDate"
 *
 * @ejb:finder signature="Collection findByRctRfrncNmbrAndBrCode(java.lang.String RCT_RFRNC_NMBR, java.lang.Integer BR_CODE, java.lang.Integer RCT_AD_CMPNY)"
 *             query="SELECT OBJECT(rct) FROM ArReceipt rct  WHERE rct.rctReferenceNumber = ?1  AND rct.rctAdBranch = ?2 AND rct.rctAdCompany = ?3"
 *
 * @ejb:finder signature="Collection findByRbName(java.lang.String RB_NM, java.lang.Integer RCT_AD_CMPNY)"
 *             query="SELECT OBJECT(rct) FROM ArReceipt rct  WHERE rct.arReceiptBatch.rbName=?1 AND rct.rctAdCompany = ?2"
 *
 * @jboss:query signature="Collection findByRbName(java.lang.String RB_NM, java.lang.Integer RCT_AD_CMPNY)"
 *             query="SELECT OBJECT(rct) FROM ArReceipt rct  WHERE rct.arReceiptBatch.rbName=?1 AND rct.rctAdCompany = ?2 ORDER BY rct.rctNumber, rct.rctDate"
 *
 * @ejb:finder signature="Collection findDraftRctAll(java.lang.Integer RCT_AD_CMPNY)"
 *             query="SELECT OBJECT(rct) FROM ArReceipt rct WHERE rct.rctApprovalStatus IS NULL AND rct.rctVoid = 0 AND rct.rctAdCompany = ?1"
 *
 * @ejb:finder signature="Collection findDraftRctByBrCode(java.lang.Integer RCT_AD_BRNCH, java.lang.Integer RCT_AD_CMPNY)"
 *             query="SELECT OBJECT(rct) FROM ArReceipt rct WHERE rct.rctApprovalStatus IS NULL AND rct.rctVoid = 0 AND rct.rctAdBranch = ?1 AND rct.rctAdCompany = ?2"
 *
 * @ejb:finder signature="Collection findDraftRctByTypeBrCode(java.lang.String RCT_TYPE, java.lang.Integer RCT_AD_BRNCH, java.lang.Integer RCT_AD_CMPNY)"
 *             query="SELECT OBJECT(rct) FROM ArReceipt rct WHERE rct.rctApprovalStatus IS NULL AND rct.rctVoid = 0 AND rct.rctType=?1 AND rct.rctAdBranch = ?2 AND rct.rctAdCompany = ?3"
 *
 * @ejb:finder signature="Collection findPostedRctByRctTypeAndRctDateRangeAndCstNameAndTcType(java.lang.String RCT_TYP, java.util.Date RCT_DT_FRM, java.util.Date RCT_DT_TO, java.lang.String CST_NM, java.lang.String TC_TYP, java.lang.Integer RCT_AD_CMPNY)"
 *             query="SELECT OBJECT(rct) FROM ArReceipt rct  WHERE rct.rctPosted = 1 AND rct.rctVoid = 0 AND rct.rctType = ?1 AND rct.rctDate >= ?2 AND rct.rctDate <= ?3 AND rct.arCustomer.cstName = ?4 AND rct.arTaxCode.tcType = ?5 AND rct.rctAdCompany = ?6"
 *
 * @jboss:query signature="Collection findPostedRctByRctTypeAndRctDateRangeAndCstNameAndTcType(java.lang.String RCT_TYP, java.util.Date RCT_DT_FRM, java.util.Date RCT_DT_TO, java.lang.String CST_NM, java.lang.String TC_TYP, java.lang.Integer RCT_AD_CMPNY)"
 *             query="SELECT OBJECT(rct) FROM ArReceipt rct  WHERE rct.rctPosted = 1 AND rct.rctVoid = 0 AND rct.rctType = ?1 AND rct.rctDate >= ?2 AND rct.rctDate <= ?3 AND rct.arCustomer.cstName = ?4 AND rct.arTaxCode.tcType = ?5 AND rct.rctAdCompany = ?6 ORDER BY rct.rctDate"
 *
 * @ejb:finder signature="Collection findUnpostedRctByRctDateRange(java.util.Date RCT_DT_FRM, java.util.Date RCT_DT_TO, java.lang.Integer RCT_AD_CMPNY)"
 *             query="SELECT OBJECT(rct) FROM ArReceipt rct  WHERE rct.rctPosted = 0 AND rct.rctVoid = 0 AND rct.rctDate >= ?1 AND rct.rctDate <= ?2 AND rct.rctAdCompany = ?3"
 *
 * @jboss:query signature="Collection findUnpostedRctByRctDateRange(java.util.Date RCT_DT_FRM, java.util.Date RCT_DT_TO, java.lang.Integer RCT_AD_CMPNY)"
 *             query="SELECT OBJECT(rct) FROM ArReceipt rct  WHERE rct.rctPosted = 0 AND rct.rctVoid = 0 AND rct.rctDate >= ?1 AND rct.rctDate <= ?2 AND rct.rctAdCompany = ?3 ORDER BY rct.rctDate"
 *
 * @ejb:finder signature="Collection findUnpostedRctByRctDateRangeByBranch(java.util.Date RCT_DT_FRM, java.util.Date RCT_DT_TO, java.lang.Integer RCT_AD_BRNCH, java.lang.Integer RCT_AD_CMPNY)"
 *             query="SELECT OBJECT(rct) FROM ArReceipt rct  WHERE rct.rctPosted = 0 AND rct.rctVoid = 0 AND rct.rctDate >= ?1 AND rct.rctDate <= ?2 AND rct.rctAdBranch = ?3 AND rct.rctAdCompany = ?4"
 *
 * @jboss:query signature="Collection findUnpostedRctByRctDateRangeByBranch(java.util.Date RCT_DT_FRM, java.util.Date RCT_DT_TO, java.lang.Integer RCT_AD_BRNCH, java.lang.Integer RCT_AD_CMPNY)"
 *             query="SELECT OBJECT(rct) FROM ArReceipt rct  WHERE rct.rctPosted = 0 AND rct.rctVoid = 0 AND rct.rctDate >= ?1 AND rct.rctDate <= ?2 AND rct.rctAdBranch = ?3 AND rct.rctAdCompany = ?4 ORDER BY rct.rctDate"
 *
 * @ejb:finder signature="Collection findUnpostedRctByCstCustomerCode(java.lang.String CST_CSTMR_CODE, java.lang.Integer RCT_AD_CMPNY)"
 *             query="SELECT OBJECT(rct) FROM ArReceipt rct  WHERE rct.rctPosted = 0 AND rct.rctVoid = 0 AND rct.rctType = 'COLLECTION' AND rct.arCustomer.cstCustomerCode = ?1 AND rct.rctAdCompany = ?2"
 *
 * @jboss:query signature="Collection findUnpostedRctByCstCustomerCode(java.lang.String CST_CSTMR_CODE, java.lang.Integer RCT_AD_CMPNY)"
 *             query="SELECT OBJECT(rct) FROM ArReceipt rct  WHERE rct.rctPosted = 0 AND rct.rctVoid = 0 AND rct.rctType = 'COLLECTION' AND rct.arCustomer.cstCustomerCode = ?1 AND rct.rctAdCompany = ?2 ORDER BY rct.rctDate"
 *
 * @ejb:finder signature="Collection findUndepositedPostedRctByBaNameAndBrCode(java.lang.String BA_NM, java.lang.Integer RCT_AD_BRNCH, java.lang.Integer RCT_AD_CMPNY)"
 *             query="SELECT OBJECT(rct) FROM ArReceipt rct WHERE rct.rctLock = 0 AND rct.rctReconciled = 0 AND rct.rctPosted = 1 AND rct.rctVoid = 0 AND rct.adBankAccount.baName = ?1 AND rct.rctAdBranch = ?2 AND rct.rctAdCompany = ?3"
 *
 * @jboss:query signature="Collection findUndepositedPostedRctByBaNameAndBrCode(java.lang.String BA_NM, java.lang.Integer RCT_AD_BRNCH, java.lang.Integer RCT_AD_CMPNY)"
 *             query="SELECT OBJECT(rct) FROM ArReceipt rct WHERE rct.rctLock = 0 AND rct.rctReconciled = 0 AND rct.rctPosted = 1 AND rct.rctVoid = 0 AND rct.adBankAccount.baName = ?1 AND rct.rctAdBranch = ?2 AND rct.rctAdCompany = ?3 ORDER BY rct.rctDate, rct.rctNumber"
 *
 * @ejb:finder signature="Collection findUndepositedPostedRctByBaNameAndBrCodeAndRctDateRange(java.util.Date RCT_DT_FRM, java.util.Date RCT_DT_TO, java.lang.String BA_NM, java.lang.Integer RCT_AD_BRNCH, java.lang.Integer RCT_AD_CMPNY)"
 *             query="SELECT OBJECT(rct) FROM ArReceipt rct WHERE rct.rctLock = 0 AND rct.rctReconciled = 0 AND rct.rctPosted = 1 AND rct.rctVoid = 0 AND rct.rctDate >= ?1 AND rct.rctDate <= ?2 AND rct.adBankAccount.baName = ?3 AND rct.rctAdBranch = ?4 AND rct.rctAdCompany = ?5"
 *
 * @jboss:finder signature="Collection findUndepositedPostedRctByBaNameAndBrCodeAndRctDateRange(java.util.Date RCT_DT_FRM, java.util.Date RCT_DT_TO, java.lang.String BA_NM, java.lang.Integer RCT_AD_BRNCH, java.lang.Integer RCT_AD_CMPNY)"
 *             query="SELECT OBJECT(rct) FROM ArReceipt rct WHERE rct.rctLock = 0 AND rct.rctReconciled = 0 AND rct.rctPosted = 1 AND rct.rctVoid = 0 AND rct.rctDate >= ?1 AND rct.rctDate <= ?2 AND rct.adBankAccount.baName = ?3 AND rct.rctAdBranch = ?4 AND rct.rctAdCompany = ?5 ORDER BY rct.rctDate, rct.rctNumber"
 *
 * @ejb:finder signature="Collection findByRctDateAndBaName(java.util.Date RCT_DT, java.lang.String BA_NM, java.lang.Integer RCT_AD_CMPNY)"
 *			   query="SELECT OBJECT(rct) FROM ArReceipt rct WHERE rct.rctDate = ?1 AND rct.adBankAccount.baName LIKE ?2 AND rct.rctAdCompany = ?3"
 *
 * @ejb:finder signature="Collection findPostedRctByBeforeOrEqualRctDateAndRctTypeAndCstCustomerCode(java.util.Date RCT_DT, java.lang.String RCT_TYP, java.lang.String CST_CSTMR_CD, java.lang.Integer RCT_AD_CMPNY)"
 * 			   query="SELECT OBJECT(rct) FROM ArReceipt rct WHERE rct.rctPosted = 1 AND rct.rctDate <= ?1 AND rct.rctType = ?2 AND rct.arCustomer.cstCustomerCode = ?3 AND rct.rctAdCompany = ?4 ORDER BY rct.rctDate"
 *
 * @ejb:finder signature="Collection findOpenDepositEnabledPostedRctByCstCustomerCode (java.lang.String CST_CSTMR_CODE, java.lang.Integer RCT_AD_CMPNY)"
 *       	   query="SELECT OBJECT(rct) FROM ArReceipt rct WHERE rct.rctPosted = 1 AND rct.rctVoid = 0  AND rct.rctType = 'MISC' AND rct.rctCustomerDeposit = 1 AND rct.rctAmount > rct.rctAppliedDeposit AND rct.arCustomer.cstCustomerCode = ?1 AND rct.rctAdCompany = ?2"
 *
 * @jboss:query signature="Collection findOpenDepositEnabledPostedRctByCstCustomerCode (java.lang.String CST_CSTMR_CODE, java.lang.Integer RCT_AD_CMPNY)"
 *       	   query="SELECT OBJECT(rct) FROM ArReceipt rct WHERE rct.rctPosted = 1 AND rct.rctVoid = 0  AND rct.rctType = 'MISC' AND rct.rctCustomerDeposit = 1 AND rct.rctAmount > rct.rctAppliedDeposit AND rct.arCustomer.cstCustomerCode = ?1 AND rct.rctAdCompany = ?2 ORDER BY rct.rctDate"
 *
 * @ejb:finder signature="Collection findOpenDepositEnabledPostedRctByCstCustomerCodeOrderBySlp (java.util.Date RCT_DT, java.lang.String CST_CSTMR_CODE, java.lang.Integer RCT_AD_CMPNY)"
 *       	   query="SELECT OBJECT(rct) FROM ArReceipt rct WHERE rct.rctPosted = 1 AND rct.rctVoid = 0  AND rct.rctDate <= ?1 AND rct.rctType = 'MISC' AND rct.rctCustomerDeposit = 1 AND rct.rctAmount > rct.rctAppliedDeposit AND rct.arCustomer.cstCustomerCode = ?2 AND rct.rctAdCompany = ?3"
 *
 * @jboss:query signature="Collection findOpenDepositEnabledPostedRctByCstCustomerCodeOrderBySlp (java.util.Date RCT_DT, java.lang.String CST_CSTMR_CODE, java.lang.Integer RCT_AD_CMPNY)"
 *       	   query="SELECT OBJECT(rct) FROM ArReceipt rct WHERE rct.rctPosted = 1 AND rct.rctVoid = 0  AND rct.rctDate <= ?1 AND rct.rctType = 'MISC' AND rct.rctCustomerDeposit = 1 AND rct.rctAmount > rct.rctAppliedDeposit AND rct.arCustomer.cstCustomerCode = ?2 AND rct.rctAdCompany = ?3 ORDER BY rct.arSalesperson.slpSalespersonCode"
 *
 * @ejb:finder signature="Collection findAppliedDepositEnabledPostedRctByCstCustomerCode (java.lang.String CST_CSTMR_CODE, java.lang.Integer RCT_AD_CMPNY)"
 *       	   query="SELECT OBJECT(rct) FROM ArReceipt rct WHERE rct.rctPosted = 1 AND rct.rctVoid = 0  AND rct.rctType = 'MISC' AND rct.rctCustomerDeposit = 1 AND rct.rctAppliedDeposit > 0 AND rct.arCustomer.cstCustomerCode = ?1 AND rct.rctAdCompany = ?2"
 *
 * @jboss:query signature="Collection findAppliedDepositEnabledPostedRctByCstCustomerCode (java.lang.String CST_CSTMR_CODE, java.lang.Integer RCT_AD_CMPNY)"
 *       	   query="SELECT OBJECT(rct) FROM ArReceipt rct WHERE rct.rctPosted = 1 AND rct.rctVoid = 0  AND rct.rctType = 'MISC' AND rct.rctCustomerDeposit = 1 AND rct.rctAppliedDeposit > 0 AND rct.arCustomer.cstCustomerCode = ?1 AND rct.rctAdCompany = ?2 ORDER BY rct.rctDate DESC"
 *
 * @ejb:finder signature="Collection findByRctLvShift(java.lang.String RCT_LV_SHFT, java.lang.Integer RCT_AD_CMPNY)"
 *             query="SELECT OBJECT(rct) FROM ArReceipt rct  WHERE rct.rctLvShift = ?1 AND rct.rctAdCompany = ?2"
 *
 * @ejb:finder signature="Collection findRctByAdCompanyAll(java.lang.Integer RCT_AD_CMPNY)"
 *             query="SELECT OBJECT(rct) FROM ArReceipt rct WHERE rct.rctAdCompany = ?1"
 *
 * @ejb:finder signature="Collection findByRctTypeAndRbName(java.lang.String RCT_TYPE, java.lang.String RB_NM, java.lang.Integer RCT_AD_CMPNY)"
 *             query="SELECT OBJECT(rct) FROM ArReceipt rct  WHERE rct.rctType = ?1 AND rct.arReceiptBatch.rbName=?2 AND rct.rctAdCompany = ?3"
 *
 * @ejb:finder signature="LocalArReceipt findByRctDateAndRctNumberAndCstCustomerCodeAndBrCode(java.util.Date RCT_DT, java.lang.String RCT_NMBR, java.lang.String CST_CSTMR_CODE, java.lang.Integer RCT_AD_BRNCH, java.lang.Integer RCT_AD_CMPNY)"
 *             query="SELECT OBJECT(rct) FROM ArReceipt rct WHERE rct.rctDate = ?1 AND rct.rctNumber = ?2 AND rct.arCustomer.cstCustomerCode = ?3 AND rct.rctAdBranch = ?4 AND rct.rctAdCompany = ?5"
 *
 *
 *
 * @ejb:finder signature="Collection findPostedRctByBaNameAndRctDateRangeAndBrCode(java.lang.String BA_NM, java.util.Date RCT_DT_FRM, java.util.Date RCT_DT_TO, java.lang.Integer RCT_AD_BRNCH, java.lang.Integer RCT_AD_CMPNY)"
 *             query="SELECT OBJECT(rct) FROM ArReceipt rct  WHERE rct.rctLock = 0 AND rct.rctPosted = 1 AND rct.rctAmountCash > 0 AND rct.rctVoid = 0 AND rct.adBankAccount.baIsCashAccount = 0 AND rct.adBankAccount.baName = ?1 AND rct.rctDate >= ?2 AND rct.rctDate <= ?3 AND rct.rctAdBranch = ?4 AND rct.rctAdCompany = ?5"
 *
 * @jboss:query signature="Collection findPostedRctByBaNameAndRctDateRangeAndBrCode(java.lang.String BA_NM, java.util.Date RCT_DT_FRM, java.util.Date RCT_DT_TO, java.lang.Integer RCT_AD_BRNCH, java.lang.Integer RCT_AD_CMPNY)"
 *             query="SELECT OBJECT(rct) FROM ArReceipt rct  WHERE rct.rctLock = 0 AND rct.rctPosted = 1 AND rct.rctAmountCash > 0 AND rct.rctVoid = 0 AND rct.adBankAccount.baIsCashAccount = 0 AND rct.adBankAccount.baName = ?1 AND rct.rctDate >= ?2 AND rct.rctDate <= ?3 AND rct.rctAdBranch = ?4 AND rct.rctAdCompany = ?5 ORDER BY rct.rctDate"
 *
 *
 * @ejb:finder signature="Collection findPostedCard1RctByBaNameAndRctDateRangeAndBrCode(java.lang.String BA_NM, java.util.Date RCT_DT_FRM, java.util.Date RCT_DT_TO, java.lang.Integer RCT_AD_BRNCH, java.lang.Integer RCT_AD_CMPNY)"
 *             query="SELECT OBJECT(rct) FROM ArReceipt rct  WHERE rct.rctLock = 0 AND rct.rctPosted = 1 AND rct.rctAmountCard1 > 0 AND rct.rctVoid = 0 AND rct.adBankAccountCard1.baIsCashAccount = 0 AND rct.adBankAccountCard1.baName = ?1 AND rct.rctDate >= ?2 AND rct.rctDate <= ?3 AND rct.rctAdBranch = ?4 AND rct.rctAdCompany = ?5"
 *
 * @jboss:query signature="Collection findPostedCard1RctByBaNameAndRctDateRangeAndBrCode(java.lang.String BA_NM, java.util.Date RCT_DT_FRM, java.util.Date RCT_DT_TO, java.lang.Integer RCT_AD_BRNCH, java.lang.Integer RCT_AD_CMPNY)"
 *             query="SELECT OBJECT(rct) FROM ArReceipt rct  WHERE rct.rctLock = 0 AND rct.rctPosted = 1 AND rct.rctAmountCard1 > 0 AND rct.rctVoid = 0 AND rct.adBankAccountCard1.baIsCashAccount = 0 AND rct.adBankAccountCard1.baName = ?1 AND rct.rctDate >= ?2 AND rct.rctDate <= ?3 AND rct.rctAdBranch = ?4 AND rct.rctAdCompany = ?5 ORDER BY rct.rctDate"
 *
 * @ejb:finder signature="Collection findPostedCard2RctByBaNameAndRctDateRangeAndBrCode(java.lang.String BA_NM, java.util.Date RCT_DT_FRM, java.util.Date RCT_DT_TO, java.lang.Integer RCT_AD_BRNCH, java.lang.Integer RCT_AD_CMPNY)"
 *             query="SELECT OBJECT(rct) FROM ArReceipt rct  WHERE rct.rctLock = 0 AND rct.rctPosted = 1 AND rct.rctAmountCard2 > 0 AND rct.rctVoid = 0 AND rct.adBankAccountCard2.baIsCashAccount = 0 AND rct.adBankAccountCard2.baName = ?1 AND rct.rctDate >= ?2 AND rct.rctDate <= ?3 AND rct.rctAdBranch = ?4 AND rct.rctAdCompany = ?5"
 *
 * @jboss:query signature="Collection findPostedCard2RctByBaNameAndRctDateRangeAndBrCode(java.lang.String BA_NM, java.util.Date RCT_DT_FRM, java.util.Date RCT_DT_TO, java.lang.Integer RCT_AD_BRNCH, java.lang.Integer RCT_AD_CMPNY)"
 *             query="SELECT OBJECT(rct) FROM ArReceipt rct  WHERE rct.rctLock = 0 AND rct.rctPosted = 1 AND rct.rctAmountCard2 > 0 AND rct.rctVoid = 0 AND rct.adBankAccountCard2.baIsCashAccount = 0 AND rct.adBankAccountCard2.baName = ?1 AND rct.rctDate >= ?2 AND rct.rctDate <= ?3 AND rct.rctAdBranch = ?4 AND rct.rctAdCompany = ?5 ORDER BY rct.rctDate"
 *
 * @ejb:finder signature="Collection findPostedCard3RctByBaNameAndRctDateRangeAndBrCode(java.lang.String BA_NM, java.util.Date RCT_DT_FRM, java.util.Date RCT_DT_TO, java.lang.Integer RCT_AD_BRNCH, java.lang.Integer RCT_AD_CMPNY)"
 *             query="SELECT OBJECT(rct) FROM ArReceipt rct  WHERE rct.rctLock = 0 AND rct.rctPosted = 1 AND rct.rctAmountCard3 > 0 AND rct.rctVoid = 0 AND rct.adBankAccountCard3.baIsCashAccount = 0 AND rct.adBankAccountCard3.baName = ?1 AND rct.rctDate >= ?2 AND rct.rctDate <= ?3 AND rct.rctAdBranch = ?4 AND rct.rctAdCompany = ?5"
 *
 * @jboss:query signature="Collection findPostedCard3RctByBaNameAndRctDateRangeAndBrCode(java.lang.String BA_NM, java.util.Date RCT_DT_FRM, java.util.Date RCT_DT_TO, java.lang.Integer RCT_AD_BRNCH, java.lang.Integer RCT_AD_CMPNY)"
 *             query="SELECT OBJECT(rct) FROM ArReceipt rct  WHERE rct.rctLock = 0 AND rct.rctPosted = 1 AND rct.rctAmountCard3 > 0 AND rct.rctVoid = 0 AND rct.adBankAccountCard3.baIsCashAccount = 0 AND rct.adBankAccountCard3.baName = ?1 AND rct.rctDate >= ?2 AND rct.rctDate <= ?3 AND rct.rctAdBranch = ?4 AND rct.rctAdCompany = ?5 ORDER BY rct.rctDate"
 *
 * @ejb:finder signature="Collection findPostedChequeRctByBaNameAndRctDateRangeAndBrCode(java.lang.String BA_NM, java.util.Date RCT_DT_FRM, java.util.Date RCT_DT_TO, java.lang.Integer RCT_AD_BRNCH, java.lang.Integer RCT_AD_CMPNY)"
 *             query="SELECT OBJECT(rct) FROM ArReceipt rct  WHERE rct.rctLock = 0 AND rct.rctPosted = 1 AND rct.rctAmountCheque > 0 AND rct.rctVoid = 0 AND rct.adBankAccount.baIsCashAccount = 0 AND rct.adBankAccount.baName = ?1 AND rct.rctDate >= ?2 AND rct.rctDate <= ?3 AND rct.rctAdBranch = ?4 AND rct.rctAdCompany = ?5"
 *
 * @jboss:query signature="Collection findPostedChequeRctByBaNameAndRctDateRangeAndBrCode(java.lang.String BA_NM, java.util.Date RCT_DT_FRM, java.util.Date RCT_DT_TO, java.lang.Integer RCT_AD_BRNCH, java.lang.Integer RCT_AD_CMPNY)"
 *             query="SELECT OBJECT(rct) FROM ArReceipt rct  WHERE rct.rctLock = 0 AND rct.rctPosted = 1 AND rct.rctAmountCheque > 0 AND rct.rctVoid = 0 AND rct.adBankAccount.baIsCashAccount = 0 AND rct.adBankAccount.baName = ?1 AND rct.rctDate >= ?2 AND rct.rctDate <= ?3 AND rct.rctAdBranch = ?4 AND rct.rctAdCompany = ?5 ORDER BY rct.rctDate"
 *
 *
 *
 *
 *
 * @ejb:finder signature="Collection findReconciledPostedRctByBaNameAndRctDateRangeAndBrCode(java.lang.String BA_NM, java.util.Date RCT_DT_FRM, java.util.Date RCT_DT_TO, java.lang.Integer RCT_AD_BRNCH, java.lang.Integer RCT_AD_CMPNY)"
 *             query="SELECT OBJECT(rct) FROM ArReceipt rct  WHERE rct.rctLock = 0 AND rct.rctPosted = 1 AND rct.rctAmountCash > 0 AND rct.rctVoid = 0 AND rct.rctReconciled = 1 AND rct.adBankAccount.baIsCashAccount = 0 AND rct.adBankAccount.baName = ?1 AND rct.rctDate >= ?2 AND rct.rctDate <= ?3 AND rct.rctAdBranch = ?4 AND rct.rctAdCompany = ?5"
 *
 * @jboss:query signature="Collection findReconciledPostedRctByBaNameAndRctDateRangeAndBrCode(java.lang.String BA_NM, java.util.Date RCT_DT_FRM, java.util.Date RCT_DT_TO, java.lang.Integer RCT_AD_BRNCH, java.lang.Integer RCT_AD_CMPNY)"
 *             query="SELECT OBJECT(rct) FROM ArReceipt rct  WHERE rct.rctLock = 0 AND rct.rctPosted = 1 AND rct.rctAmountCash > 0 AND rct.rctVoid = 0 AND rct.rctReconciled = 1 AND rct.adBankAccount.baIsCashAccount = 0 AND rct.adBankAccount.baName = ?1 AND rct.rctDate >= ?2 AND rct.rctDate <= ?3 AND rct.rctAdBranch = ?4 AND rct.rctAdCompany = ?5 ORDER BY rct.rctDate"
 *
 * @ejb:finder signature="Collection findReconciledPostedCard1RctByBaNameAndRctDateRangeAndBrCode(java.lang.String BA_NM, java.util.Date RCT_DT_FRM, java.util.Date RCT_DT_TO, java.lang.Integer RCT_AD_BRNCH, java.lang.Integer RCT_AD_CMPNY)"
 *             query="SELECT OBJECT(rct) FROM ArReceipt rct  WHERE rct.rctLock = 0 AND rct.rctPosted = 1 AND rct.rctAmountCard1 > 0 AND rct.rctVoid = 0 AND rct.rctReconciledCard1 = 1 AND rct.adBankAccountCard1.baIsCashAccount = 0 AND rct.adBankAccountCard1.baName = ?1 AND rct.rctDate >= ?2 AND rct.rctDate <= ?3 AND rct.rctAdBranch = ?4 AND rct.rctAdCompany = ?5"
 *
 * @jboss:query signature="Collection findReconciledPostedCard1RctByBaNameAndRctDateRangeAndBrCode(java.lang.String BA_NM, java.util.Date RCT_DT_FRM, java.util.Date RCT_DT_TO, java.lang.Integer RCT_AD_BRNCH, java.lang.Integer RCT_AD_CMPNY)"
 *             query="SELECT OBJECT(rct) FROM ArReceipt rct  WHERE rct.rctLock = 0 AND rct.rctPosted = 1 AND rct.rctAmountCard1 > 0 AND rct.rctVoid = 0 AND rct.rctReconciledCard1 = 1 AND rct.adBankAccountCard1.baIsCashAccount = 0 AND rct.adBankAccountCard1.baName = ?1 AND rct.rctDate >= ?2 AND rct.rctDate <= ?3 AND rct.rctAdBranch = ?4 AND rct.rctAdCompany = ?5 ORDER BY rct.rctDate"
 *
 * @ejb:finder signature="Collection findReconciledPostedCard2RctByBaNameAndRctDateRangeAndBrCode(java.lang.String BA_NM, java.util.Date RCT_DT_FRM, java.util.Date RCT_DT_TO, java.lang.Integer RCT_AD_BRNCH, java.lang.Integer RCT_AD_CMPNY)"
 *             query="SELECT OBJECT(rct) FROM ArReceipt rct  WHERE rct.rctLock = 0 AND rct.rctPosted = 1 AND rct.rctAmountCard2 > 0 AND rct.rctVoid = 0 AND rct.rctReconciledCard2 = 1 AND rct.adBankAccountCard2.baIsCashAccount = 0 AND rct.adBankAccountCard2.baName = ?1 AND rct.rctDate >= ?2 AND rct.rctDate <= ?3 AND rct.rctAdBranch = ?4 AND rct.rctAdCompany = ?5"
 *
 * @jboss:query signature="Collection findReconciledPostedCard2RctByBaNameAndRctDateRangeAndBrCode(java.lang.String BA_NM, java.util.Date RCT_DT_FRM, java.util.Date RCT_DT_TO, java.lang.Integer RCT_AD_BRNCH, java.lang.Integer RCT_AD_CMPNY)"
 *             query="SELECT OBJECT(rct) FROM ArReceipt rct  WHERE rct.rctLock = 0 AND rct.rctPosted = 1 AND rct.rctAmountCard2 > 0 AND rct.rctVoid = 0 AND rct.rctReconciledCard2 = 1 AND rct.adBankAccountCard2.baIsCashAccount = 0 AND rct.adBankAccountCard2.baName = ?1 AND rct.rctDate >= ?2 AND rct.rctDate <= ?3 AND rct.rctAdBranch = ?4 AND rct.rctAdCompany = ?5 ORDER BY rct.rctDate"
 *
 * @ejb:finder signature="Collection findReconciledPostedCard3RctByBaNameAndRctDateRangeAndBrCode(java.lang.String BA_NM, java.util.Date RCT_DT_FRM, java.util.Date RCT_DT_TO, java.lang.Integer RCT_AD_BRNCH, java.lang.Integer RCT_AD_CMPNY)"
 *             query="SELECT OBJECT(rct) FROM ArReceipt rct  WHERE rct.rctLock = 0 AND rct.rctPosted = 1 AND rct.rctAmountCard3 > 0 AND rct.rctVoid = 0 AND rct.rctReconciledCard3 = 1 AND rct.adBankAccountCard3.baIsCashAccount = 0 AND rct.adBankAccountCard3.baName = ?1 AND rct.rctDate >= ?2 AND rct.rctDate <= ?3 AND rct.rctAdBranch = ?4 AND rct.rctAdCompany = ?5"
 *
 * @jboss:query signature="Collection findReconciledPostedCard3RctByBaNameAndRctDateRangeAndBrCode(java.lang.String BA_NM, java.util.Date RCT_DT_FRM, java.util.Date RCT_DT_TO, java.lang.Integer RCT_AD_BRNCH, java.lang.Integer RCT_AD_CMPNY)"
 *             query="SELECT OBJECT(rct) FROM ArReceipt rct  WHERE rct.rctLock = 0 AND rct.rctPosted = 1 AND rct.rctAmountCard3 > 0 AND rct.rctVoid = 0 AND rct.rctReconciledCard3 = 1 AND rct.adBankAccountCard3.baIsCashAccount = 0 AND rct.adBankAccountCard3.baName = ?1 AND rct.rctDate >= ?2 AND rct.rctDate <= ?3 AND rct.rctAdBranch = ?4 AND rct.rctAdCompany = ?5 ORDER BY rct.rctDate"
 *
 * @ejb:finder signature="Collection findReconciledPostedChequeRctByBaNameAndRctDateRangeAndBrCode(java.lang.String BA_NM, java.util.Date RCT_DT_FRM, java.util.Date RCT_DT_TO, java.lang.Integer RCT_AD_BRNCH, java.lang.Integer RCT_AD_CMPNY)"
 *             query="SELECT OBJECT(rct) FROM ArReceipt rct  WHERE rct.rctLock = 0 AND rct.rctPosted = 1 AND rct.rctAmountCheque > 0 AND rct.rctVoid = 0 AND rct.rctReconciledCheque = 1 AND rct.adBankAccount.baIsCashAccount = 0 AND rct.adBankAccount.baName = ?1 AND rct.rctDate >= ?2 AND rct.rctDate <= ?3 AND rct.rctAdBranch = ?4 AND rct.rctAdCompany = ?5"
 *
 * @jboss:query signature="Collection findReconciledPostedChequeRctByBaNameAndRctDateRangeAndBrCode(java.lang.String BA_NM, java.util.Date RCT_DT_FRM, java.util.Date RCT_DT_TO, java.lang.Integer RCT_AD_BRNCH, java.lang.Integer RCT_AD_CMPNY)"
 *             query="SELECT OBJECT(rct) FROM ArReceipt rct  WHERE rct.rctLock = 0 AND rct.rctPosted = 1 AND rct.rctAmountCheque > 0 AND rct.rctVoid = 0 AND rct.rctReconciledCheque = 1 AND rct.adBankAccount.baIsCashAccount = 0 AND rct.adBankAccount.baName = ?1 AND rct.rctDate >= ?2 AND rct.rctDate <= ?3 AND rct.rctAdBranch = ?4 AND rct.rctAdCompany = ?5 ORDER BY rct.rctDate"
 *
 *
 *
 * @ejb:finder signature="Collection findInvestorReceiptByIrabCode(java.lang.Integer IRAB_CODE, java.lang.Integer RCT_AD_CMPNY)"
 *             query="SELECT OBJECT(rct) FROM ArReceipt rct  WHERE rct.rctLock = 0 AND rct.rctPosted = 1 AND rct.rctVoid = 0 AND rct.glInvestorAccountBalance.irabCode = ?1 AND rct.rctAdCompany = ?2"
 *
 * @jboss:query signature="Collection findInvestorReceiptByIrabCode(java.lang.Integer IRAB_CODE, java.lang.Integer RCT_AD_CMPNY)"
 *             query="SELECT OBJECT(rct) FROM ArReceipt rct  WHERE rct.rctLock = 0 AND rct.rctPosted = 1 AND rct.rctVoid = 0 AND rct.glInvestorAccountBalance.irabCode = ?1 AND rct.rctAdCompany = ?2 ORDER BY rct.rctDate"
 *
 *
 *
 *
 * @ejb:finder signature="Collection findRctForPostingByBranch(java.lang.String RCT_TYP, java.lang.Integer RCT_AD_BRNCH, java.lang.Integer RCT_AD_CMPNY)"
 *             query="SELECT OBJECT(rct) FROM ArReceipt rct  WHERE  rct.rctType=?1 AND rct.rctPosted = 0 AND rct.rctVoid = 0 AND (rct.rctApprovalStatus='APPROVED' OR rct.rctApprovalStatus='N/A') AND rct.rctAdBranch=?2 AND rct.rctAdCompany=?3"
 *
 * @jboss:query signature="Collection findRctForPostingByBranch(java.lang.String RCT_TYP, java.lang.Integer RCT_AD_BRNCH, java.lang.Integer RCT_AD_CMPNY)"
 *             query="SELECT OBJECT(rct) FROM ArReceipt rct  WHERE  rct.rctType=?1 AND rct.rctPosted = 0 AND rct.rctVoid = 0 AND (rct.rctApprovalStatus='APPROVED' OR rct.rctApprovalStatus='N/A') AND rct.rctAdBranch=?2 AND rct.rctAdCompany=?3"
 *
 *
 * @ejb:finder signature="Collection findPostedTaxRctByRctDateRange(java.util.Date CHK_DT_FRM, java.util.Date CHK_DT_TO, java.lang.Integer CHK_AD_BRNCH, java.lang.Integer CHK_AD_CMPNY)"
 *             query="SELECT OBJECT(rct) FROM ArReceipt rct, IN(rct.arAppliedInvoices) ai WHERE rct.rctVoid = 0 AND ai.arInvoicePaymentSchedule.arInvoice.arTaxCode.tcType IN ('EXEMPT','ZERO-RATED','INCLUSIVE','EXCLUSIVE') AND rct.rctPosted = 1 AND rct.rctDate >= ?1 AND rct.rctDate <= ?2 AND rct.rctAdBranch = ?3 AND rct.rctAdCompany = ?4 ORDER BY rct.rctDate, rct.rctNumber"
 *
 * @jboss:query signature="Collection findPostedTaxRctByRctDateRange(java.util.Date CHK_DT_FRM, java.util.Date CHK_DT_TO, java.lang.Integer CHK_AD_BRNCH, java.lang.Integer CHK_AD_CMPNY)"
 *             query="SELECT OBJECT(rct) FROM ArReceipt rct, IN(rct.arAppliedInvoices) ai WHERE rct.rctVoid = 0 AND ai.arInvoicePaymentSchedule.arInvoice.arTaxCode.tcType IN ('EXEMPT','ZERO-RATED','INCLUSIVE','EXCLUSIVE') AND rct.rctPosted = 1 AND rct.rctDate >= ?1 AND rct.rctDate <= ?2 AND rct.rctAdBranch = ?3 AND rct.rctAdCompany = ?4 ORDER BY rct.rctDate, rct.rctNumber"
 *
 *
 * @ejb:finder signature="Collection findPostedTaxMiscRctByRctDateRange(java.util.Date CHK_DT_FRM, java.util.Date CHK_DT_TO, java.lang.Integer CHK_AD_BRNCH, java.lang.Integer CHK_AD_CMPNY)"
 *             query="SELECT OBJECT(rct) FROM ArReceipt rct WHERE rct.rctType = 'MISC' AND rct.rctVoid = 0 AND rct.arTaxCode.tcType IN ('EXEMPT','ZERO-RATED','INCLUSIVE','EXCLUSIVE') AND rct.rctPosted = 1 AND rct.rctDate >= ?1 AND rct.rctDate <= ?2 AND rct.rctAdBranch = ?3 AND rct.rctAdCompany = ?4 ORDER BY rct.rctDate, rct.rctNumber"
 *
 * @jboss:query signature="Collection findPostedTaxMiscRctByRctDateRange(java.util.Date CHK_DT_FRM, java.util.Date CHK_DT_TO, java.lang.Integer CHK_AD_BRNCH, java.lang.Integer CHK_AD_CMPNY)"
 *             query="SELECT OBJECT(rct) FROM ArReceipt rct WHERE rct.rctType = 'MISC' AND rct.rctVoid = 0 AND rct.arTaxCode.tcType IN ('EXEMPT','ZERO-RATED','INCLUSIVE','EXCLUSIVE') AND rct.rctPosted = 1 AND rct.rctDate >= ?1 AND rct.rctDate <= ?2 AND rct.rctAdBranch = ?3 AND rct.rctAdCompany = ?4 ORDER BY rct.rctDate, rct.rctNumber"
 *
 *
 * @ejb:select signature="Collection ejbSelectGeneric(java.lang.String jbossQl, java.lang.Object[] args)"
 *             result-type-mapping="Local"
 *             method-intf="LocalHome"
 *             query="SELECT OBJECT(rct) FROM ArReceipt rct"
 *
 * @jboss:query signature="Collection ejbSelectGeneric(java.lang.String jbossQl, java.lang.Object[] args)"
 *                         dynamic="true"
 *
 * @ejb:value-object match="*"
 *             name="ArReceipt"
 *
 * @jboss:persistence table-name="AR_RCPT"
 *
 * @jboss:entity-command name="mysql-get-generated-keys"
 */

public abstract class ArReceiptBean extends AbstractEntityBean {


	// PERSISTENT METHODS

	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 * @ejb:pk-field
	 *
	 * @jboss:column-name name="RCT_CODE"
	 *
	 * @jboss:persistence auto-increment="true"
	 **/
	public abstract Integer getRctCode();
	public abstract void setRctCode(Integer RCT_CODE);

	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="RCT_TYP"
	 **/
	public abstract String getRctType();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setRctType(String RCT_TYP);
	
	
	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="RCT_DCMNT_TYP"
	 **/
	public abstract String getRctDocumentType();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setRctDocumentType(String RCT_DCMNT_TYP);

	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="RCT_DESC"
	 **/
	public abstract String getRctDescription();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setRctDescription(String RCT_DESC);


	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="RCT_DT"
	 **/
	public abstract Date getRctDate();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setRctDate(Date RCT_DT);

	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="RCT_NMBR"
	 **/
	public abstract String getRctNumber();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setRctNumber(String RCT_NMBR);

	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="RCT_RFRNC_NMBR"
	 **/
	public abstract String getRctReferenceNumber();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setRctReferenceNumber(String RCT_RFRNC_NMBR);

	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="RCT_CHCK_NO"
	 **/
	public abstract String getRctCheckNo();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setRctCheckNo(String RCT_CHCK_NO);

	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="RCT_PYFL_RFRNC_NMBR"
	 **/
	public abstract String getRctPayfileReferenceNumber();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setRctPayfileReferenceNumber(String RCT_PYFL_RFRNC_NMBR);

	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="RCT_CHQ_NMBR"
	 **/
	public abstract String getRctChequeNumber();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setRctChequeNumber(String RCT_CHQ_NMBR);

	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="RCT_VCHR_NMBR"
	 **/
	public abstract String getRctVoucherNumber();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setRctVoucherNumber(String RCT_VCHR_NMBR);

	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="RCT_CRD_NMBR1"
	 **/
	public abstract String getRctCardNumber1();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setRctCardNumber1(String RCT_CRD_NMBR1);

	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="RCT_CRD_NMBR2"
	 **/
	public abstract String getRctCardNumber2();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setRctCardNumber2(String RCT_CRD_NMBR2);

	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="RCT_CRD_NMBR3"
	 **/
	public abstract String getRctCardNumber3();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setRctCardNumber3(String RCT_CRD_NMBR3);


	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="RCT_AMNT"
	 **/
	public abstract double getRctAmount();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setRctAmount(double RCT_AMNT);

	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="RCT_AMNT_CSH"
	 **/
	public abstract double getRctAmountCash();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setRctAmountCash(double RCT_AMNT_CSH);

	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="RCT_AMNT_CHQ"
	 **/
	public abstract double getRctAmountCheque();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setRctAmountCheque(double RCT_AMNT_CHQ);

	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="RCT_AMNT_VCHR"
	 **/
	public abstract double getRctAmountVoucher();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setRctAmountVoucher(double RCT_AMNT_VCHR);

	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="RCT_AMNT_CRD1"
	 **/
	public abstract double getRctAmountCard1();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setRctAmountCard1(double RCT_AMNT_CRD1);

	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="RCT_AMNT_CRD2"
	 **/
	public abstract double getRctAmountCard2();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setRctAmountCard2(double RCT_AMNT_CRD2);

	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="RCT_AMNT_CRD3"
	 **/
	public abstract double getRctAmountCard3();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setRctAmountCard3(double RCT_AMNT_CRD3);

	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="RCT_CNVRSN_DT"
	 **/
	public abstract Date getRctConversionDate();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setRctConversionDate(Date RCT_CNVRSN_DT);

	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="RCT_CNVRSN_RT"
	 **/
	public abstract double getRctConversionRate();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setRctConversionRate(double RCT_CNVRSN_RT);

	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="RCT_SLD_TO"
	 **/
	public abstract String getRctSoldTo();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setRctSoldTo(String RCT_SLD_TO);

	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="RCT_PYMNT_MTHD"
	 **/
	public abstract String getRctPaymentMethod();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setRctPaymentMethod(String RCT_PYMNT_MTHD);

	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="RCT_CSTMR_DPST"
	 **/
	public abstract byte getRctCustomerDeposit();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setRctCustomerDeposit(byte RCT_CSTMR_DPST);


	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="RCT_APPLD_DPST"
	 **/
	public abstract double getRctAppliedDeposit();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setRctAppliedDeposit(double RCT_APPLD_DPST);



	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="RCT_EXCSS_AMNT"
	 **/
	public abstract double getRctExcessAmount();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setRctExcessAmount(double RCT_EXCSS_AMNT);


	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="RCT_APPRVL_STATUS"
	 **/
	public abstract String getRctApprovalStatus();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setRctApprovalStatus(String RCT_APPRVL_STATUS);

	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="RCT_RSN_FR_RJCTN"
	 **/
	public abstract String getRctReasonForRejection();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setRctReasonForRejection(String RCT_RSN_FR_RJCTN);






	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="RCT_ENBL_ADVNC_PYMNT"
	 **/
	public abstract byte getRctEnableAdvancePayment();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setRctEnableAdvancePayment(byte RCT_ENBL_ADVNC_PYMNT);



	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="RCT_PSTD"
	 **/
	public abstract byte getRctPosted();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setRctPosted(byte RCT_PSTD);

	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="RCT_VD_APPRVL_STATUS"
	 **/
	public abstract String getRctVoidApprovalStatus();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setRctVoidApprovalStatus(String RCT_VD_APPRVL_STATUS);

	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="RCT_VD_PSTD"
	 **/
	public abstract byte getRctVoidPosted();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setRctVoidPosted(byte RCT_VD_PSTD);

	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="RCT_VD"
	 **/
	public abstract byte getRctVoid();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setRctVoid(byte RCT_VD);

	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="RCT_RCNCLD"
	 **/
	public abstract byte getRctReconciled();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setRctReconciled(byte RCT_RCNCLD);

	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="RCT_RCNCLD_CHQ"
	 **/
	public abstract byte getRctReconciledCheque();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setRctReconciledCheque(byte RCT_RCNCLD_CHQ);


	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="RCT_RCNCLD_CRD1"
	 **/
	public abstract byte getRctReconciledCard1();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setRctReconciledCard1(byte RCT_RCNCLD_CRD1);

	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="RCT_RCNCLD_CRD2"
	 **/
	public abstract byte getRctReconciledCard2();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setRctReconciledCard2(byte RCT_RCNCLD_CRD2);

	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="RCT_RCNCLD_CRD3"
	 **/
	public abstract byte getRctReconciledCard3();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setRctReconciledCard3(byte RCT_RCNCLD_CRD3);





	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="RCT_DT_RCNCLD"
	 **/
	public abstract Date getRctDateReconciled();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setRctDateReconciled(Date RCT_DT_RCNCLD);

	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="RCT_DT_RCNCLD_CHQ"
	 **/
	public abstract Date getRctDateReconciledCheque();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setRctDateReconciledCheque(Date RCT_DT_RCNCLD_CHQ);

	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="RCT_DT_RCNCLD_CRD1"
	 **/
	public abstract Date getRctDateReconciledCard1();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setRctDateReconciledCard1(Date RCT_DT_RCNCLD_CRD1);

	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="RCT_DT_RCNCLD_CRD2"
	 **/
	public abstract Date getRctDateReconciledCard2();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setRctDateReconciledCard2(Date RCT_DT_RCNCLD_CRD2);

	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="RCT_DT_RCNCLD_CRD3"
	 **/
	public abstract Date getRctDateReconciledCard3();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setRctDateReconciledCard3(Date RCT_DT_RCNCLD_CRD3);




	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="RCT_CRTD_BY"
	 **/
	public abstract String getRctCreatedBy();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setRctCreatedBy(String RCT_CRTD_BY);

	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="RCT_DT_CRTD"
	 **/
	public abstract Date getRctDateCreated();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setRctDateCreated(Date RCT_DT_CRTD);

	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="RCT_LST_MDFD_BY"
	 **/
	public abstract String getRctLastModifiedBy();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setRctLastModifiedBy(String RCT_LST_MDFD_BY);

	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="RCT_DT_LST_MDFD"
	 **/
	public abstract Date getRctDateLastModified();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setRctDateLastModified(Date RCT_DT_LST_MDFD);

	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="RCT_APPRVD_RJCTD_BY"
	 **/
	public abstract String getRctApprovedRejectedBy();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setRctApprovedRejectedBy(String RCT_APPRVD_RJCTD_BY);

	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="RCT_DT_APPRVD_RJCTD"
	 **/
	public abstract Date getRctDateApprovedRejected();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setRctDateApprovedRejected(Date RCT_DT_APPRVD_RJCTD);

	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="RCT_PSTD_BY"
	 **/
	public abstract String getRctPostedBy();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setRctPostedBy(String RCT_PSTD_BY);

	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="RCT_DT_PSTD"
	 **/
	public abstract Date getRctDatePosted();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setRctDatePosted(Date RCT_DT_PSTD);


	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="RCT_PRNTD"
	 **/
	public abstract byte getRctPrinted();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setRctPrinted(byte RCT_PRNTD);


	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="RCT_LV_SHFT"
	 **/
	public abstract String getRctLvShift();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setRctLvShift(String RCT_LV_SHFT);


	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="RCT_LCK"
	 **/
	public abstract byte getRctLock();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setRctLock(byte RCT_LCK);

	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="RCT_SBJCT_TO_CMMSSN"
	 **/
	public abstract byte getRctSubjectToCommission();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setRctSubjectToCommission(byte RCT_SBJCT_TO_CMMSSN);

	 /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="RCT_CST_NM"
     **/
    public abstract String getRctCustomerName();
    /**
     * @ejb:interface-method view-type="local"
     **/
    public abstract void setRctCustomerName(String RCT_CST_NM);

    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="RCT_CST_ADRSS"
     **/
    public abstract String getRctCustomerAddress();
    /**
     * @ejb:interface-method view-type="local"
     **/
    public abstract void setRctCustomerAddress(String RCT_CST_ADRSS);


    /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="RCT_INVTR_BGNNG_BLNC"
    **/
    public abstract byte getRctInvtrBeginningBalance();
    /**
     * @ejb:interface-method view-type="local"
     **/
    public abstract void setRctInvtrBeginningBalance(byte RCT_INVTR_BGNNG_BLNC);


    /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="RCT_INVTR_IF"
    **/
    public abstract byte getRctInvtrInvestorFund();
    /**
     * @ejb:interface-method view-type="local"
     **/
    public abstract void setRctInvtrInvestorFund(byte RCT_INVTR_IF);


	 /**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="RCT_INVTR_NXT_RN_DT"
	 **/
	 public abstract Date getRctInvtrNextRunDate();
	 /**
	  * @ejb:interface-method view-type="local"
	  **/
	 public abstract void setRctInvtrNextRunDate(Date RCT_INVTR_NXT_RN_DT);


	 /**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="REPORT_PARAMETER"
	 **/
	public abstract String getReportParameter();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setReportParameter(String REPORT_PARAMETER);

	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="RCT_AD_BRNCH"
	 **/
	public abstract Integer getRctAdBranch();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setRctAdBranch(Integer RCT_AD_BRNCH);

	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="RCT_AD_CMPNY"
	 **/
	public abstract Integer getRctAdCompany();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setRctAdCompany(Integer RCT_AD_CMPNY);


	// RELATIONSHIP METHODS

	/**
	 * @ejb:interface-method view-type="local"
	 * @ejb:relation name="payrollperiod-receipts"
	 *               role-name="receipt-has-one-payrollperiod"
	 *               cascade-delete="no"
	 *
	 * @jboss:relation related-pk-field="ppCode"
	 *                 fk-column="HR_PAYROLL_PERIOD"
	 */
	public abstract LocalHrPayrollPeriod getHrPayrollPeriod();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setHrPayrollPeriod(LocalHrPayrollPeriod hrPayrollPeriod);

	/**
	 * @ejb:interface-method view-type="local"
	 * @ejb:relation name="customer-receipts"
	 *               role-name="receipt-has-one-customer"
	 *
	 * @jboss:relation related-pk-field="cstCode"
	 *                 fk-column="AR_CUSTOMER"
	 */

	public abstract LocalArCustomer getArCustomer();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setArCustomer(LocalArCustomer arCustomer);

	/**
	 * @ejb:interface-method view-type="local"
	 * @ejb:relation name="arwithholdingtaxcode-receipts"
	 *               role-name="receipt-has-one-arwithholdingtaxcode"
	 *
	 * @jboss:relation related-pk-field="wtcCode"
	 *                 fk-column="AR_WITHHOLDING_TAX_CODE"
	 */
	public abstract LocalArWithholdingTaxCode getArWithholdingTaxCode();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setArWithholdingTaxCode(LocalArWithholdingTaxCode arWithholdingTaxCode);


	/**
	 * @ejb:interface-method view-type="local"
	 * @ejb:relation name="bankaccount-receipts"
	 *               role-name="receipt-has-one-bankaccount"
	 *
	 * @jboss:relation related-pk-field="baCode"
	 *                 fk-column="AD_BANK_ACCOUNT"
	 */
	public abstract com.ejb.ad.LocalAdBankAccount getAdBankAccount();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setAdBankAccount(com.ejb.ad.LocalAdBankAccount adBankAccount);

	/**
	 * @ejb:interface-method view-type="local"
	 * @ejb:relation name="bankaccountcard1-receipts"
	 *               role-name="receipt-has-one-bankaccount"
	 *
	 * @jboss:relation related-pk-field="baCode"
	 *                 fk-column="AD_BANK_ACCOUNT_CARD1"
	 */
	public abstract com.ejb.ad.LocalAdBankAccount getAdBankAccountCard1();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setAdBankAccountCard1(com.ejb.ad.LocalAdBankAccount adBankAccount);

	/**
	 * @ejb:interface-method view-type="local"
	 * @ejb:relation name="bankaccountcard2-receipts"
	 *               role-name="receipt-has-one-bankaccount"
	 *
	 * @jboss:relation related-pk-field="baCode"
	 *                 fk-column="AD_BANK_ACCOUNT_CARD2"
	 */
	public abstract com.ejb.ad.LocalAdBankAccount getAdBankAccountCard2();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setAdBankAccountCard2(com.ejb.ad.LocalAdBankAccount adBankAccount);

	/**
	 * @ejb:interface-method view-type="local"
	 * @ejb:relation name="bankaccountcard3-receipts"
	 *               role-name="receipt-has-one-bankaccount"
	 *
	 * @jboss:relation related-pk-field="baCode"
	 *                 fk-column="AD_BANK_ACCOUNT_CARD3"
	 */
	public abstract com.ejb.ad.LocalAdBankAccount getAdBankAccountCard3();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setAdBankAccountCard3(com.ejb.ad.LocalAdBankAccount adBankAccount);

	/**
	 * @ejb:interface-method view-type="local"
	 * @ejb:relation name="functionalcurrency-receipts"
	 *               role-name="receipt-has-one-functionalcurrency"
	 *
	 * @jboss:relation related-pk-field="fcCode"
	 *                 fk-column="GL_FUNCTIONAL_CURRENCY"
	 */
	public abstract com.ejb.gl.LocalGlFunctionalCurrency getGlFunctionalCurrency();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setGlFunctionalCurrency(com.ejb.gl.LocalGlFunctionalCurrency glFunctionalCurrency);


	/**
	 * @ejb:interface-method view-type="local"
	 * @ejb:relation name="artaxcode-receipts"
	 *               role-name="receipt-has-one-artaxcode"
	 *
	 * @jboss:relation related-pk-field="tcCode"
	 *                 fk-column="AR_TAX_CODE"
	 */
	public abstract LocalArTaxCode getArTaxCode();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setArTaxCode(LocalArTaxCode arTaxCode);

	/**
	 * @ejb:interface-method view-type="local"
	 * @ejb:relation name="receipt-ardistributionrecords"
	 *               role-name="receipt-has-many-ardistributionrecords"
	 *
	 */
	public abstract Collection getArDistributionRecords();
	public abstract void setArDistributionRecords(Collection arDistributionRecords);

	/**
	 * @ejb:interface-method view-type="local"
	 * @ejb:relation name="receipt-appliedinvoices"
	 *               role-name="receipt-has-many-appliedinvoices"
	 */
	public abstract Collection getArAppliedInvoices();
	public abstract void setArAppliedInvoices(Collection arAppliedInvoices);

	/**
	 * @ejb:interface-method view-type="local"
	 * @ejb:relation name="receipt-invoicelines"
	 *               role-name="receipt-has-many-invoicelines"
	 */
	public abstract Collection getArInvoiceLines();
	public abstract void setArInvoiceLines(Collection arInvoiceLines);

	/**
	 * @ejb:interface-method view-type="local"
	 * @ejb:relation name="receipt-invoicelineitems"
	 *               role-name="receipt-has-many-invoicelineitems"
	 */
	public abstract Collection getArInvoiceLineItems();
	public abstract void setArInvoiceLineItems(Collection arInvoiceLineItems);

	/**
	 * @ejb:interface-method view-type="local"
	 * @ejb:relation name="receiptbatch-receipts"
	 *               role-name="receipt-has-one-receiptbatch"
	 *
	 * @jboss:relation related-pk-field="rbCode"
	 *                 fk-column="AR_RECEIPT_BATCH"
	 */
	public abstract LocalArReceiptBatch getArReceiptBatch();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setArReceiptBatch(LocalArReceiptBatch arReceiptBatch);

	/**
	 * @ejb:interface-method view-type="local"
	 * @ejb:relation name="receipt-fundTransferReceipts"
	 *               role-name="receipt-has-many-fundTransferReceipts"
	 *
	 */
	public abstract Collection getCmFundTransferReceipts();
	public abstract void setCmFundTransferReceipts(Collection cmFundTransferReceipts);

	/**
	 * @ejb:interface-method view-type="local"
	 * @ejb:relation name="salesperson-receipts"
	 *               role-name="receipt-has-one-salesperson"
	 *
	 * @jboss:relation related-pk-field="slpCode"
	 *                 fk-column="AR_SALESPERSON"
	 */
	public abstract LocalArSalesperson getArSalesperson();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setArSalesperson(LocalArSalesperson arSalesperson);


        /**
	 * @ejb:interface-method view-type="local"
	 * @ejb:relation name="investorAccountBalance-receipts"
	 *               role-name="receipt-has-one-investorAccountBalance"
	 *
	 * @jboss:relation related-pk-field="irabCode"
            *                 fk-column="GL_INVESTOR_ACCOUNT_BALANCE"
"	 */
	public abstract LocalGlInvestorAccountBalance getGlInvestorAccountBalance();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setGlInvestorAccountBalance(LocalGlInvestorAccountBalance glInvestorAccountBalance);

	/**
	 * @jboss:dynamic-ql
	 */
	public abstract Collection ejbSelectGeneric(java.lang.String jbossQl, java.lang.Object[] args)
	throws FinderException;


	// BUSINESS METHODS

	/**
	 * @ejb:home-method view-type="local"
	 */
	public Collection ejbHomeGetRctByCriteria(java.lang.String jbossQl, java.lang.Object[] args)
	throws FinderException {

		return ejbSelectGeneric(jbossQl, args);
	}

	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public void addArDistributionRecord(LocalArDistributionRecord arDistributionRecord) {

		Debug.print("ArReceiptBean addArDistributionRecord");

		try {

			Collection arDistributionRecords = getArDistributionRecords();
			arDistributionRecords.add(arDistributionRecord);

		} catch (Exception ex) {

			throw new EJBException(ex.getMessage());

		}
	}

	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public void dropArDistributionRecord(LocalArDistributionRecord arDistributionRecord) {

		Debug.print("ArReceiptBean dropArDistributionRecord");

		try {

			Collection arDistributionRecords = getArDistributionRecords();
			arDistributionRecords.remove(arDistributionRecord);

		} catch (Exception ex) {

			throw new EJBException(ex.getMessage());

		}

	}

	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public void addArAppliedInvoice(LocalArAppliedInvoice arAppliedInvoice) {

		Debug.print("ArReceiptBean addArAppliedInvoice");

		try {

			Collection arAppliedInvoices = getArAppliedInvoices();
			arAppliedInvoices.add(arAppliedInvoice);

		} catch (Exception ex) {

			throw new EJBException(ex.getMessage());

		}

	}

	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public void dropArAppliedInvoice(LocalArAppliedInvoice arAppliedInvoice) {

		Debug.print("ArReceiptBean dropArAppliedInvoice");

		try {

			Collection arAppliedInvoices = getArAppliedInvoices();
			arAppliedInvoices.remove(arAppliedInvoice);

		} catch (Exception ex) {

			throw new EJBException(ex.getMessage());

		}

	}

	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public void addArInvoiceLine(LocalArInvoiceLine arInvoiceLine) {

		Debug.print("ArReceiptBean addArInvoiceLine");

		try {

			Collection arInvoiceLines = getArInvoiceLines();
			arInvoiceLines.add(arInvoiceLine);

		} catch (Exception ex) {

			throw new EJBException(ex.getMessage());

		}

	}

	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public void dropArInvoiceLine(LocalArInvoiceLine arInvoiceLine) {

		Debug.print("ArReceiptBean dropArInvoiceLine");

		try {

			Collection arInvoiceLines = getArInvoiceLines();
			arInvoiceLines.remove(arInvoiceLine);

		} catch (Exception ex) {

			throw new EJBException(ex.getMessage());

		}

	}

	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public void addArInvoiceLineItem(LocalArInvoiceLineItem arInvoiceLineItem) {

		Debug.print("ArReceiptBean addArInvoiceLineItem");

		try {

			Collection arInvoiceLineItems = getArInvoiceLineItems();
			arInvoiceLineItems.add(arInvoiceLineItem);

		} catch (Exception ex) {

			throw new EJBException(ex.getMessage());

		}

	}

	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public void dropArInvoiceLineItem(LocalArInvoiceLineItem arInvoiceLineItem) {

		Debug.print("ArReceiptBean dropArInvoiceLineItem");

		try {

			Collection arInvoiceLineItems = getArInvoiceLineItems();
			arInvoiceLineItems.remove(arInvoiceLineItem);

		} catch (Exception ex) {

			throw new EJBException(ex.getMessage());

		}

	}


	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public short getArDrNextLine() {

		Debug.print("ArReceiptBean getArDrNextLine");

		try {

			Collection arDistributionRecords = getArDistributionRecords();
			System.out.print("arDistributionRecords: "+arDistributionRecords);
			return (short)(arDistributionRecords.size() + 1);

		} catch (Exception ex) {

			throw new EJBException(ex.getMessage());

		}
	}

	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public void addCmFundTransferReceipt(LocalCmFundTransferReceipt cmFundTransferReceipt) {

		Debug.print("ArReceiptBean addCmFundTransferReceipt");

		try {

			Collection cmFundTransferReceipts = getCmFundTransferReceipts();
			cmFundTransferReceipts.add(cmFundTransferReceipt);

		} catch (Exception ex) {

			throw new EJBException(ex.getMessage());

		}
	}

	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public void dropCmFundTransferReceipt(LocalCmFundTransferReceipt cmFundTransferReceipt) {

		Debug.print("ArReceiptBean dropCmFundTransferReceipt");

		try {

			Collection cmFundTransferReceipts = getCmFundTransferReceipts();
			cmFundTransferReceipts.remove(cmFundTransferReceipt);

		} catch (Exception ex) {

			throw new EJBException(ex.getMessage());

		}

	}

	// ENTITY METHODS

	/**
	 * @ejb:create-method view-type="local"
	 **/
	public Integer ejbCreate(Integer RCT_CODE,
			String RCT_TYP, String RCT_DESC, Date RCT_DT, String RCT_NMBR,
			String RCT_RFRNC_NMBR, String RCT_CHCK_NO, String RCT_PYFL_RFRNC_NMBR,
			String RCT_CHQ_NMBR, String RCT_VCHR_NMBR, String RCT_CRD_NMBR1, String RCT_CRD_NMBR2, String RCT_CRD_NMBR3,
			double RCT_AMNT, double RCT_AMNT_CSH, double RCT_AMNT_CHQ, double RCT_AMNT_VCHR, double RCT_AMNT_CRD1, double RCT_AMNT_CRD2, double RCT_AMNT_CRD3,
			Date RCT_CNVRSN_DT, double RCT_CNVRSN_RT,
			String RCT_SLD_TO, String RCT_PYMNT_MTHD, byte RCT_CSTMR_DPST, double RCT_APPLD_DPST,
			String RCT_APPRVL_STATUS, String RCT_RSN_FR_RJCTN, byte RCT_PSTD,
			String RCT_VD_APPRVL_STATUS, byte RCT_VD_PSTD, byte RCT_VD,
			byte RCT_RCNCLD, byte RCT_RCNCLD_CHQ, byte RCT_RCNCLD_CRD1, byte RCT_RCNCLD_CRD2, byte RCT_RCNCLD_CRD3,
			Date RCT_DT_RCNCLD, Date RCT_DT_RCNCLD_CHQ, Date RCT_DT_RCNCLD_CRD1, Date RCT_DT_RCNCLD_CRD2, Date RCT_DT_RCNCLD_CRD3,
			String RCT_CRTD_BY, Date RCT_DT_CRTD, String RCT_LST_MDFD_BY, Date RCT_DT_LST_MDFD,
			String RCT_APPRVD_RJCTD_BY, Date RCT_DT_APPRVD_RJCTD, String RCT_PSTD_BY,
			Date RCT_DT_PSTD, byte RCT_PRNTD, String RCT_LV_SHFT, byte RCT_LCK,
			byte RCT_SBJCT_TO_CMMSSN, String RCT_CST_NM, String RCT_CST_ADRSS,
			byte RCT_INVTR_BGNNG_BLNC, byte RCT_INVTR_IF, Date RCT_INVTR_NXT_RN_DT,
			Integer RCT_AD_BRNCH, Integer RCT_AD_CMPNY)
	throws CreateException {

		Debug.print("ArReceiptBean ejbCreate");

		setRctCode(RCT_CODE);
		setRctType(RCT_TYP);
		setRctDescription(RCT_DESC);
		setRctDate(RCT_DT);
		setRctNumber(RCT_NMBR);
		setRctReferenceNumber(RCT_RFRNC_NMBR);
		setRctCheckNo(RCT_CHCK_NO);
		setRctPayfileReferenceNumber(RCT_PYFL_RFRNC_NMBR);
		setRctChequeNumber(RCT_CHQ_NMBR);
		setRctVoucherNumber(RCT_VCHR_NMBR);
		setRctCardNumber1(RCT_CRD_NMBR1);
		setRctCardNumber2(RCT_CRD_NMBR2);
		setRctCardNumber3(RCT_CRD_NMBR3);
		setRctAmount(RCT_AMNT);
		setRctAmountCash(RCT_AMNT_CSH);
		setRctAmountCheque(RCT_AMNT_CHQ);
		setRctAmountVoucher(RCT_AMNT_VCHR);
		setRctAmountCard1(RCT_AMNT_CRD1);
		setRctAmountCard2(RCT_AMNT_CRD2);
		setRctAmountCard3(RCT_AMNT_CRD3);
		setRctConversionDate(RCT_CNVRSN_DT);
		setRctConversionRate(RCT_CNVRSN_RT);
		setRctSoldTo(RCT_SLD_TO);
		setRctPaymentMethod(RCT_PYMNT_MTHD);
		setRctCustomerDeposit(RCT_CSTMR_DPST);
		setRctAppliedDeposit(RCT_APPLD_DPST);
		setRctApprovalStatus(RCT_APPRVL_STATUS);
		setRctReasonForRejection(RCT_RSN_FR_RJCTN);
		setRctPosted(RCT_PSTD);
		setRctVoidApprovalStatus(RCT_VD_APPRVL_STATUS);
		setRctVoidPosted(RCT_VD_PSTD);
		setRctVoid(RCT_VD);

		setRctReconciled(RCT_RCNCLD);
		setRctReconciledCheque(RCT_RCNCLD_CHQ);
		setRctReconciledCard1(RCT_RCNCLD_CRD1);
		setRctReconciledCard2(RCT_RCNCLD_CRD2);
		setRctReconciledCard3(RCT_RCNCLD_CRD3);
		setRctDateReconciled(RCT_DT_RCNCLD);
		setRctDateReconciledCheque(RCT_DT_RCNCLD_CHQ);
		setRctDateReconciledCard1(RCT_DT_RCNCLD_CRD1);
		setRctDateReconciledCard2(RCT_DT_RCNCLD_CRD2);
		setRctDateReconciledCard3(RCT_DT_RCNCLD_CRD3);


		setRctCreatedBy(RCT_CRTD_BY);
		setRctDateCreated(RCT_DT_CRTD);
		setRctLastModifiedBy(RCT_LST_MDFD_BY);
		setRctDateLastModified(RCT_DT_LST_MDFD);
		setRctApprovedRejectedBy(RCT_APPRVD_RJCTD_BY);
		setRctDateApprovedRejected(RCT_DT_APPRVD_RJCTD);
		setRctPostedBy(RCT_PSTD_BY);
		setRctDatePosted(RCT_DT_PSTD);
		setRctPrinted(RCT_PRNTD);
		setRctLvShift(RCT_LV_SHFT);
		setRctLock(RCT_LCK);
		setRctSubjectToCommission(RCT_SBJCT_TO_CMMSSN);
		setRctCustomerName(RCT_CST_NM);
		setRctCustomerAddress(RCT_CST_ADRSS);
                setRctInvtrBeginningBalance(RCT_INVTR_BGNNG_BLNC);
		setRctInvtrInvestorFund(RCT_INVTR_IF);
		setRctInvtrNextRunDate(RCT_INVTR_NXT_RN_DT);
		setRctAdBranch(RCT_AD_BRNCH);
		setRctAdCompany(RCT_AD_CMPNY);

		return null;

	}

	/**
	 * @ejb:create-method view-type="local"
	 **/
	public Integer ejbCreate(
			String RCT_TYP, String RCT_DESC, Date RCT_DT, String RCT_NMBR,
			String RCT_RFRNC_NMBR, String RCT_CHCK_NO, String RCT_PYFL_RFRNC_NMBR,
			String RCT_CHQ_NMBR, String RCT_VCHR_NMBR, String RCT_CRD_NMBR1, String RCT_CRD_NMBR2, String RCT_CRD_NMBR3,
			double RCT_AMNT, double RCT_AMNT_CSH, double RCT_AMNT_CHQ, double RCT_AMNT_VCHR, double RCT_AMNT_CRD1, double RCT_AMNT_CRD2, double RCT_AMNT_CRD3,
			Date RCT_CNVRSN_DT, double RCT_CNVRSN_RT,
			String RCT_SLD_TO, String RCT_PYMNT_MTHD, byte RCT_CSTMR_DPST, double RCT_APPLD_DPST,
			String RCT_APPRVL_STATUS, String RCT_RSN_FR_RJCTN, byte RCT_PSTD,
			String RCT_VD_APPRVL_STATUS, byte RCT_VD_PSTD, byte RCT_VD,
			byte RCT_RCNCLD, byte RCT_RCNCLD_CHQ, byte RCT_RCNCLD_CRD1, byte RCT_RCNCLD_CRD2, byte RCT_RCNCLD_CRD3,
			Date RCT_DT_RCNCLD, Date RCT_DT_RCNCLD_CHQ, Date RCT_DT_RCNCLD_CRD1, Date RCT_DT_RCNCLD_CRD2, Date RCT_DT_RCNCLD_CRD3,
			String RCT_CRTD_BY, Date RCT_DT_CRTD, String RCT_LST_MDFD_BY, Date RCT_DT_LST_MDFD,
			String RCT_APPRVD_RJCTD_BY, Date RCT_DT_APPRVD_RJCTD, String RCT_PSTD_BY,
			Date RCT_DT_PSTD, byte RCT_PRNTD, String RCT_LV_SHFT,  byte RCT_LCK,
			byte RCT_SBJCT_TO_CMMSSN, String RCT_CST_NM, String RCT_CST_ADRSS,
			byte RCT_INVTR_BGNNG_BLNC, byte RCT_INVTR_IF, Date RCT_INVTR_NXT_RN_DT,
			Integer RCT_AD_BRNCH, Integer RCT_AD_CMPNY)
	throws CreateException {

		Debug.print("ArReceiptBean ejbCreate");

		setRctType(RCT_TYP);
		setRctDescription(RCT_DESC);
		setRctDate(RCT_DT);
		setRctNumber(RCT_NMBR);
		setRctReferenceNumber(RCT_RFRNC_NMBR);
		setRctCheckNo(RCT_CHCK_NO);
		setRctPayfileReferenceNumber(RCT_PYFL_RFRNC_NMBR);
		setRctChequeNumber(RCT_CHQ_NMBR);
		setRctVoucherNumber(RCT_VCHR_NMBR);
		setRctCardNumber1(RCT_CRD_NMBR1);
		setRctCardNumber2(RCT_CRD_NMBR2);
		setRctCardNumber3(RCT_CRD_NMBR3);
		setRctAmount(RCT_AMNT);
		setRctAmountCash(RCT_AMNT_CSH);
		setRctAmountCheque(RCT_AMNT_CHQ);
		setRctAmountVoucher(RCT_AMNT_VCHR);
		setRctAmountCard1(RCT_AMNT_CRD1);
		setRctAmountCard2(RCT_AMNT_CRD2);
		setRctAmountCard3(RCT_AMNT_CRD3);
		setRctConversionDate(RCT_CNVRSN_DT);
		setRctConversionRate(RCT_CNVRSN_RT);
		setRctSoldTo(RCT_SLD_TO);
		setRctPaymentMethod(RCT_PYMNT_MTHD);
		setRctCustomerDeposit(RCT_CSTMR_DPST);
		setRctAppliedDeposit(RCT_APPLD_DPST);
		setRctApprovalStatus(RCT_APPRVL_STATUS);
		setRctReasonForRejection(RCT_RSN_FR_RJCTN);
		setRctPosted(RCT_PSTD);
		setRctVoidApprovalStatus(RCT_VD_APPRVL_STATUS);
		setRctVoidPosted(RCT_VD_PSTD);
		setRctVoid(RCT_VD);

		setRctReconciled(RCT_RCNCLD);
		setRctReconciledCheque(RCT_RCNCLD_CHQ);
		setRctReconciledCard1(RCT_RCNCLD_CRD1);
		setRctReconciledCard2(RCT_RCNCLD_CRD2);
		setRctReconciledCard3(RCT_RCNCLD_CRD3);
		setRctDateReconciled(RCT_DT_RCNCLD);
		setRctDateReconciledCheque(RCT_DT_RCNCLD_CHQ);
		setRctDateReconciledCard1(RCT_DT_RCNCLD_CRD1);
		setRctDateReconciledCard2(RCT_DT_RCNCLD_CRD2);
		setRctDateReconciledCard3(RCT_DT_RCNCLD_CRD3);

		setRctCreatedBy(RCT_CRTD_BY);
		setRctDateCreated(RCT_DT_CRTD);
		setRctLastModifiedBy(RCT_LST_MDFD_BY);
		setRctDateLastModified(RCT_DT_LST_MDFD);
		setRctApprovedRejectedBy(RCT_APPRVD_RJCTD_BY);
		setRctDateApprovedRejected(RCT_DT_APPRVD_RJCTD);
		setRctPostedBy(RCT_PSTD_BY);
		setRctDatePosted(RCT_DT_PSTD);
		setRctPrinted(RCT_PRNTD);
		setRctLvShift(RCT_LV_SHFT);
		setRctLock(RCT_LCK);
		setRctSubjectToCommission(RCT_SBJCT_TO_CMMSSN);
		setRctCustomerName(RCT_CST_NM);
		setRctCustomerAddress(RCT_CST_ADRSS);
        setRctInvtrBeginningBalance(RCT_INVTR_BGNNG_BLNC);
		setRctInvtrInvestorFund(RCT_INVTR_IF);
		setRctInvtrNextRunDate(RCT_INVTR_NXT_RN_DT);
		setRctAdBranch(RCT_AD_BRNCH);
		setRctAdCompany(RCT_AD_CMPNY);

		return null;

	}

	public void ejbPostCreate(Integer RCT_CODE,
			String RCT_TYP, String RCT_DESC, Date RCT_DT, String RCT_NMBR,
			String RCT_RFRNC_NMBR, String RCT_CHCK_NO,String RCT_PYFL_RFRNC_NMBR,
			String RCT_CHQ_NMBR, String RCT_VCHR_NMBR, String RCT_CRD_NMBR1, String RCT_CRD_NMBR2, String RCT_CRD_NMBR3,
			double RCT_AMNT, double RCT_AMNT_CSH, double RCT_AMNT_CHQ, double RCT_AMNT_VCHR, double RCT_AMNT_CRD1, double RCT_AMNT_CRD2, double RCT_AMNT_CRD3,
			Date RCT_CNVRSN_DT, double RCT_CNVRSN_RT,
			String RCT_SLD_TO, String RCT_PYMNT_MTHD, byte RCT_CSTMR_DPST, double RCT_APPLD_DPST,
			String RCT_APPRVL_STATUS, String RCT_RSN_FR_RJCTN, byte RCT_PSTD,
			String RCT_VD_APPRVL_STATUS, byte RCT_VD_PSTD, byte RCT_VD,
			byte RCT_RCNCLD, byte RCT_RCNCLD_CHQ, byte RCT_RCNCLD_CRD1, byte RCT_RCNCLD_CRD2, byte RCT_RCNCLD_CRD3,
			Date RCT_DT_RCNCLD, Date RCT_DT_RCNCLD_CHQ, Date RCT_DT_RCNCLD_CRD1, Date RCT_DT_RCNCLD_CRD2, Date RCT_DT_RCNCLD_CRD3,
			String RCT_CRTD_BY, Date RCT_DT_CRTD, String RCT_LST_MDFD_BY, Date RCT_DT_LST_MDFD,
			String RCT_APPRVD_RJCTD_BY, Date RCT_DT_APPRVD_RJCTD, String RCT_PSTD_BY,
			Date RCT_DT_PSTD, byte RCT_PRNTD, String RCT_LV_SHFT, byte RCT_LCK,
			byte RCT_SBJCT_TO_CMMSSN, String RCT_CST_NM, String RCT_CST_ADRSS,
			byte RCT_INVTR_BGNNG_BLNC, byte RCT_INVTR_IF, Date RCT_INVTR_NXT_RN_DT,
			Integer RCT_AD_BRNCH, Integer RCT_AD_CMPNY)
	throws CreateException { }

	public void ejbPostCreate(
			String RCT_TYP, String RCT_DESC, Date RCT_DT, String RCT_NMBR,
			String RCT_RFRNC_NMBR, String RCT_CHCK_NO,String RCT_PYFL_RFRNC_NMBR,
			String RCT_CHQ_NMBR, String RCT_VCHR_NMBR, String RCT_CRD_NMBR1, String RCT_CRD_NMBR2, String RCT_CRD_NMBR3,
			double RCT_AMNT, double RCT_AMNT_CSH, double RCT_AMNT_CHQ, double RCT_AMNT_VCHR, double RCT_AMNT_CRD1, double RCT_AMNT_CRD2, double RCT_AMNT_CRD3,
			Date RCT_CNVRSN_DT, double RCT_CNVRSN_RT,
			String RCT_SLD_TO, String RCT_PYMNT_MTHD, byte RCT_CSTMR_DPST, double RCT_APPLD_DPST,
			String RCT_APPRVL_STATUS, String RCT_RSN_FR_RJCTN, byte RCT_PSTD,
			String RCT_VD_APPRVL_STATUS, byte RCT_VD_PSTD, byte RCT_VD,
			byte RCT_RCNCLD, byte RCT_RCNCLD_CHQ, byte RCT_RCNCLD_CRD1, byte RCT_RCNCLD_CRD2, byte RCT_RCNCLD_CRD3,
			Date RCT_DT_RCNCLD, Date RCT_DT_RCNCLD_CHQ, Date RCT_DT_RCNCLD_CRD1, Date RCT_DT_RCNCLD_CRD2, Date RCT_DT_RCNCLD_CRD3,
			String RCT_CRTD_BY, Date RCT_DT_CRTD, String RCT_LST_MDFD_BY, Date RCT_DT_LST_MDFD,
			String RCT_APPRVD_RJCTD_BY, Date RCT_DT_APPRVD_RJCTD, String RCT_PSTD_BY,
			Date RCT_DT_PSTD, byte RCT_PRNTD, String RCT_LV_SHFT, byte RCT_LCK,
			byte RCT_SBJCT_TO_CMMSSN, String RCT_CST_NM, String RCT_CST_ADRSS,
			byte RCT_INVTR_BGNNG_BLNC, byte RCT_INVTR_IF, Date RCT_INVTR_NXT_RN_DT,
			Integer RCT_AD_BRNCH, Integer RCT_AD_CMPNY)
	throws CreateException { }

}

