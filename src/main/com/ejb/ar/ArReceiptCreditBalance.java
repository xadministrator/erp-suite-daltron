/*
 * com/ejb/ar/LocalArPdcBean.java
 *
 * Created on February 22, 2019 2:55 PM
 */

package com.ejb.ar;

import java.util.Collection;
import java.util.Date;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;

import com.util.AbstractEntityBean;
import com.util.Debug;

/**
 *
 * @author  Ruben P. Lamberte
 *
 * @ejb:bean name="ArReceiptCreditBalanceEJB"
 *           display-name="Receipt Credit Balance Entity"
 *           type="CMP"
 *           cmp-version="2.x"
 *           view-type="local"
 *           local-jndi-name="omega-ejb/ArReceiptCreditBalanceEJB"
 *           schema="ArReceiptCreditBalance"
 *           primkey-field="rcbCode"
 *
 * @ejb:pk class="java.lang.Integer"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="aruser"
 *                        role-link="aruserlink"
 *
 * @ejb:permission role-name="aruser"
 *
 * @ejb:interface local-class="com.ejb.ar.LocalArReceiptCreditBalance"
 *                local-extends="javax.ejb.EJBLocalObject"
 *
 * @ejb:home local-class="com.ejb.ar.LocalArReceiptCreditBalanceHome"
 *           local-extends="javax.ejb.EJBLocalHome"
 * 

 *
 * @ejb:select signature="Collection ejbSelectGeneric(java.lang.String jbossQl, java.lang.Object[] args)"
 *             result-type-mapping="Local"
 *             method-intf="LocalHome"
 *             query="SELECT OBJECT(rcb) FROM ArReceiptCreditBalance rcb"
 * 
 * @jboss:query signature="Collection ejbSelectGeneric(java.lang.String jbossQl, java.lang.Object[] args)"
 *                         dynamic="true"
 *
 * @ejb:value-object match="*"
 *             name="ArReceiptCreditBalance"
 *
 * @jboss:persistence table-name="AR_RCPT_CRDT_BLNC"
 *
 * @jboss:entity-command name="mysql-get-generated-keys"
 */

public abstract class ArReceiptCreditBalance extends AbstractEntityBean {
    
    
    // PERSISTENT METHODS
    
	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 * @ejb:pk-field
	 *
	 * @jboss:column-name name="RCB_CODE"
	 *
	 * @jboss:persistence auto-increment="true"
	 **/
	public abstract Integer getRcbCode();         
	public abstract void setRcbCode(Integer RCB_CODE);

	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="RCB_ADJ_CODE"
	 **/
	public abstract Integer getRcbAdjCode();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setRcbAdjCode(Integer RCB_ADJ_CODE);

	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="RCB_RCT_CODE"
	 **/
	public abstract Integer getRcbRctCode();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setRcbRctCode(Integer RCB_RCT_CODE);     

	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="RCB_AMNT"
	 **/
	public abstract Double getRcbAmount();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setRcbAmount(String RCB_AMNT);


     
   /**
    * @jboss:dynamic-ql
    */
    public abstract Collection ejbSelectGeneric(java.lang.String jbossQl, java.lang.Object[] args)
       throws FinderException;
       
    


				         
     // ENTITY METHODS
    
     /**
      * @ejb:create-method view-type="local"
      **/
     public Integer ejbCreate(Integer RCB_CODE, Integer RCB_ADJ_CODE, Integer RCB_RCT_CODE)
     	
         throws CreateException {
           
         Debug.print("ArReceiptCreditBalance ejbCreate");
        
         setRcbCode(RCB_CODE);
         setRcbAdjCode(RCB_ADJ_CODE);
         setRcbRctCode(RCB_RCT_CODE);
      
                
         return null;
        
     }
    
     /**
      * @ejb:create-method view-type="local"
      **/
     public Integer ejbCreate(Integer RCB_ADJ_CODE, Integer RCB_RCT_CODE)
         throws CreateException {
           
         Debug.print("ArReceiptCreditBalance ejbCreate");
               
         setRcbAdjCode(RCB_ADJ_CODE);
         setRcbRctCode(RCB_RCT_CODE);
         return null;
        
     }
    
    
     public void ejbPostCreate(Integer RCB_CODE, Integer RCB_ADJ_CODE, Integer RCB_RCT_CODE)
  	
             throws CreateException {}
   
     public void ejbPostCreate(Integer RCB_ADJ_CODE, Integer RCB_RCT_CODE)
             throws CreateException { }
        
}

