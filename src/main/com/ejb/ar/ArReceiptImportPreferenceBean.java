/*
 * com/ejb/ar/ArReceiptImportPreferenceBean.java
 *
 * Created on January 12, 2006, 10:45 AM
 */

package com.ejb.ar;

import java.util.Collection;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;

import com.util.AbstractEntityBean;
import com.util.Debug;

/**
 *
 * @author  Aliza D.J. Anos
 *
 * @ejb:bean name="ArReceiptImportPreferenceEJB"
 *           display-name="Receipt Import Preference Entity"
 *           type="CMP"
 *           cmp-version="2.x"
 *           view-type="local"
 *           local-jndi-name="omega-ejb/ArReceiptImportPreferenceEJB"
 *           schema="ArReceiptImportPreference"
 *           primkey-field="ripCode"
 *
 * @ejb:pk class="java.lang.Integer"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="aruser"
 *                        role-link="aruserlink"
 *
 * @ejb:permission role-name="aruser"
 *
 * @ejb:interface local-class="com.ejb.ar.LocalArReceiptImportPreference"
 *                local-extends="javax.ejb.EJBLocalObject"
 *
 * @ejb:home local-class="com.ejb.ar.LocalArReceiptImportPreferenceHome"
 *           local-extends="javax.ejb.EJBLocalHome"
 *
 * @ejb:finder signature="LocalArReceiptImportPreference findByRipType(java.lang.String RIP_TYP, java.lang.Integer RIP_AD_CMPNY)"
 *             query="SELECT OBJECT(rip) FROM ArReceiptImportPreference rip WHERE rip.ripType=?1 AND rip.ripAdCompany=?2"
 *
 * @ejb:select signature="Collection ejbSelectGeneric(java.lang.String jbossQl, java.lang.Object[] args)"
 *             result-type-mapping="Local"
 *             method-intf="LocalHome"
 *             query="SELECT OBJECT(rip) FROM ArReceiptImportPreference rip"
 *
 * @jboss:query signature="Collection ejbSelectGeneric(java.lang.String jbossQl, java.lang.Object[] args)"
 *                         dynamic="true"
 *
 * @ejb:value-object match="*"
 *             name="ArReceiptImportPreference"
 *
 * @jboss:persistence table-name="AR_RCPT_IMPRT_PRFRNC"
 *
 * @jboss:entity-command name="mysql-get-generated-keys"
 */

public abstract class ArReceiptImportPreferenceBean extends AbstractEntityBean {
	
	// PERSISTENT METHODS
	
	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 * @ejb:pk-field
	 *
	 * @jboss:column-name name="RIP_CODE"
	 *
	 * @jboss:persistence auto-increment="true"
	 **/
	public abstract Integer getRipCode();         
	public abstract void setRipCode(Integer RIP_CODE);
	
	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="RIP_TYP"
	 **/
	public abstract String getRipType();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setRipType(String RIP_TYP);
	
	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="RIP_IS_SMMRZD"
	 **/
	public abstract byte getRipIsSummarized();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setRipIsSummarized(byte RIP_IS_SMMRZD);
	
	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="RIP_CSTMR_CLMN"
	 **/
	public abstract short getRipCustomerColumn();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setRipCustomerColumn(short RIP_CSTMR_CLMN);
	
	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="RIP_CSTMR_FILE"
	 **/
	public abstract String getRipCustomerFile();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setRipCustomerFile(String RIP_CSTMR_FILE);
	
	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="RIP_DT_CLMN"
	 **/
	public abstract short getRipDateColumn();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setRipDateColumn(short RIP_DT_CLMN);
	
	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="RIP_DT_FILE"
	 **/
	public abstract String getRipDateFile();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setRipDateFile(String RIP_DT_FILE);
	
	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="RIP_RCPT_NMBR_CLMN"
	 **/
	public abstract short getRipReceiptNumberColumn();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setRipReceiptNumberColumn(short RIP_RCPT_NMBR_CLMN);
	
	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="RIP_RCPT_NMBR_FILE"
	 **/
	public abstract String getRipReceiptNumberFile();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setRipReceiptNumberFile(String RIP_RCPT_NMBR_FILE);
	
	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="RIP_RCPT_NMBR_LN_CLMN"
	 **/
	public abstract short getRipReceiptNumberLineColumn();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setRipReceiptNumberLineColumn(short RIP_RCPT_NMBR_LN_CLMN);
	
	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="RIP_RCPT_NMBR_LN_FILE"
	 **/
	public abstract String getRipReceiptNumberLineFile();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setRipReceiptNumberLineFile(String RIP_RCPT_NMBR_LN_FILE);
	
	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="RIP_RCPT_AMNT_CLMN"
	 **/
	public abstract short getRipReceiptAmountColumn();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setRipReceiptAmountColumn(short RIP_RCPT_AMNT_CLMN);
	
	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="RIP_RCPT_AMNT_FILE"
	 **/
	public abstract String getRipReceiptAmountFile();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setRipReceiptAmountFile(String RIP_RCPT_AMNT_FILE);
	
	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="RIP_TC_CLMN"
	 **/
	public abstract short getRipTcColumn();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setRipTcColumn(short RIP_TC_CLMN);
	
	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="RIP_TC_FILE"
	 **/
	public abstract String getRipTcFile();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setRipTcFile(String RIP_TC_FILE);
	
	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="RIP_WTC_CLMN"
	 **/
	public abstract short getRipWtcColumn();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setRipWtcColumn(short RIP_WTC_CLMN);
	
	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="RIP_WTC_FILE"
	 **/
	public abstract String getRipWtcFile();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setRipWtcFile(String RIP_WTC_FILE);
	
	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="RIP_ITM_NM_CLMN"
	 **/
	public abstract short getRipItemNameColumn();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setRipItemNameColumn(short RIP_ITM_NM_CLMN);
	
	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="RIP_ITM_NM_FILE"
	 **/
	public abstract String getRipItemNameFile();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setRipItemNameFile(String RIP_ITM_NM_FILE);
	
	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="RIP_LCTN_NM_CLMN"
	 **/
	public abstract short getRipLocationNameColumn();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setRipLocationNameColumn(short RIP_LCTN_NM_CLMN);
	
	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="RIP_LCTN_NM_FILE"
	 **/
	public abstract String getRipLocationNameFile();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setRipLocationNameFile(String RIP_LCTN_NM_FILE);
	
	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="RIP_UOM_NM_CLMN"
	 **/
	public abstract short getRipUomNameColumn();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setRipUomNameColumn(short RIP_UOM_NM_CLMN);
	
	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="RIP_UOM_NM_FILE"
	 **/
	public abstract String getRipUomNameFile();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setRipUomNameFile(String RIP_UOM_NM_FILE);
	
	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="RIP_QTY_CLMN"
	 **/
	public abstract short getRipQuantityColumn();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setRipQuantityColumn(short RIP_QTY_CLMN);
	
	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="RIP_QTY_FILE"
	 **/
	public abstract String getRipQuantityFile();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setRipQuantityFile(String RIP_QTY_FILE);
	
	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="RIP_UNT_PRC_CLMN"
	 **/
	public abstract short getRipUnitPriceColumn();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setRipUnitPriceColumn(short RIP_UNT_PRC_CLMN);
	
	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="RIP_UNT_PRC_FILE"
	 **/
	public abstract String getRipUnitPriceFile();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setRipUnitPriceFile(String RIP_UNT_PRC_FILE);
	
	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="RIP_TOTAL_CLMN"
	 **/
	public abstract short getRipTotalColumn();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setRipTotalColumn(short RIP_TOTAL_CLMN);
	
	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="RIP_TOTAL_FILE"
	 **/
	public abstract String getRipTotalFile();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setRipTotalFile(String RIP_TOTAL_FILE);
	
	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="RIP_MEMO_LN_CLMN"
	 **/
	public abstract short getRipMemoLineColumn();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setRipMemoLineColumn(short RIP_MEMO_LN_CLMN);
	
	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="RIP_MEMO_LN_FILE"
	 **/
	public abstract String getRipMemoLineFile();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setRipMemoLineFile(String RIP_MEMO_LN_FILE);
	
	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="RIP_INVC_NMBR_CLMN"
	 **/
	public abstract short getRipInvoiceNumberColumn();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setRipInvoiceNumberColumn(short RIP_INVC_NMBR_CLMN);
	
	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="RIP_INVC_NMBR_FILE"
	 **/
	public abstract String getRipInvoiceNumberFile();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setRipInvoiceNumberFile(String RIP_INVC_NMBR_FILE);
	
	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="RIP_APPLY_AMNT_CLMN"
	 **/
	public abstract short getRipApplyAmountColumn();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setRipApplyAmountColumn(short RIP_APPLY_AMNT_CLMN);
	
	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="RIP_APPLY_AMNT_FILE"
	 **/
	public abstract String getRipApplyAmountFile();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setRipApplyAmountFile(String RIP_APPLY_AMNT_FILE);
	
	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="RIP_APPLY_DSCNT_CLMN"
	 **/
	public abstract short getRipApplyDiscountColumn();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setRipApplyDiscountColumn(short RIP_APPLY_DSCNT_CLMN);
	
	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="RIP_APPLY_DSCNT_FILE"
	 **/
	public abstract String getRipApplyDiscountFile();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setRipApplyDiscountFile(String RIP_APPLY_DSCNT_FILE);
	
	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="RIP_AD_CMPNY"
	 **/
	public abstract Integer getRipAdCompany();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setRipAdCompany(Integer RIP_AD_CMPNY);
	
	
	//RELATIONSHIP METHODS
	
	/**
	 * @ejb:interface-method view-type="local"
	 * @ejb:relation name="receiptimportpreference-receiptimportpreferencelines"
	 *               role-name="receiptimportpreference-has-many-receiptimportpreferencelines"
	 * 
	 */
	public abstract Collection getArReceiptImportPreferenceLines();
	public abstract void setArReceiptImportPreferenceLines(Collection arReceiptImportPreferenceLines);
	
	
	/**
	 * @jboss:dynamic-ql
	 */
	public abstract Collection ejbSelectGeneric(java.lang.String jbossQl, java.lang.Object[] args)
	throws FinderException;
	
	//BUSINESS METHODS
	
	/**
	 * @ejb:home-method view-type="local"
	 */
	public Collection ejbHomeGetRipByCriteria(java.lang.String jbossQl, java.lang.Object[] args)
	throws FinderException {
		
		return ejbSelectGeneric(jbossQl, args);
	}
	
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public void addArReceiptImportPreferenceLine(LocalArReceiptImportPreferenceLine arReceiptImportPreferenceLine) {
		
		Debug.print("ArReceiptImportPreferenceBean addArReceiptImportPreferenceLine");
		
		try {
			
			Collection arReceiptImportPreferenceLines = getArReceiptImportPreferenceLines();
			arReceiptImportPreferenceLines.add(arReceiptImportPreferenceLine);
			
		} catch (Exception ex) {
			
			throw new EJBException(ex.getMessage());
			
		}
		
	}
	
	/**
	 * @ejb:interface-method view-type="local"
	 **/ 
	public void dropArReceiptImportPreferenceLine(LocalArReceiptImportPreferenceLine arReceiptImportPreferenceLine) {
		
		Debug.print("ArReceiptImportPreferenceBean dropArReceiptImportPreferenceLine");
		
		try {
			
			Collection arReceiptImportPreferenceLines = getArReceiptImportPreferenceLines();
			arReceiptImportPreferenceLines.remove(arReceiptImportPreferenceLine);
			
		} catch (Exception ex) {
			
			throw new EJBException(ex.getMessage());
			
		}
		
	}
	
	//ENTITY METHODS
	
	/**
	 * @ejb:create-method view-type="local"
	 **/
	public Integer ejbCreate(Integer RIP_CODE, String RIP_TYP, byte RIP_IS_SMMRZD,
			short RIP_CSTMR_CLMN, String RIP_CSTMR_FILE, short RIP_DT_CLMN, String RIP_DT_FILE,
			short RIP_RCPT_NMBR_CLMN, String RIP_RCPT_NMBR_FILE, short RIP_RCPT_NMBR_LN_CLMN, 
			String RIP_RCPT_NMBR_LN_FILE, short RIP_RCPT_AMNT_CLMN, String RIP_RCPT_AMNT_FILE,
			short RIP_TC_CLMN, String RIP_TC_FILE, short RIP_WTC_CLMN, String RIP_WTC_FILE,
			short RIP_ITM_NM_CLMN, String RIP_ITM_NM_FILE, short RIP_LCTN_NM_CLMN, String RIP_LCTN_NM_FILE,
			short RIP_UOM_NM_CLMN, String RIP_UOM_NM_FILE, short RIP_QTY_CLMN, String RIP_QTY_FILE,
			short RIP_UNT_PRC_CLMN, String RIP_UNT_PRC_FILE, short RIP_TOTAL_CLMN, String RIP_TOTAL_FILE,
			short RIP_MEMO_LN_CLMN, String RIP_MEMO_LN_FILE, short RIP_INVC_NMBR_CLMN, String RIP_INVC_NMBR_FILE,
			short RIP_APPLY_AMNT_CLMN, String RIP_APPLY_AMNT_FILE, short RIP_APPLY_DSCNT_CLMN, 
			String RIP_APPLY_DSCNT_FILE, Integer RIP_AD_CMPNY) 
	throws CreateException {
		
		Debug.print("ArReceiptImportPreferenceBean ejbCreate");
		
		setRipCode(RIP_CODE);
		setRipType(RIP_TYP);
		setRipIsSummarized(RIP_IS_SMMRZD);
		setRipCustomerColumn(RIP_CSTMR_CLMN);
		setRipCustomerFile(RIP_CSTMR_FILE);
		setRipDateColumn(RIP_DT_CLMN);
		setRipDateFile(RIP_DT_FILE);
		setRipReceiptNumberColumn(RIP_RCPT_NMBR_CLMN);
		setRipReceiptNumberFile(RIP_RCPT_NMBR_FILE);
		setRipReceiptNumberLineColumn(RIP_RCPT_NMBR_LN_CLMN);
		setRipReceiptNumberLineFile(RIP_RCPT_NMBR_LN_FILE);
		setRipReceiptAmountColumn(RIP_RCPT_AMNT_CLMN);
		setRipReceiptAmountFile(RIP_RCPT_AMNT_FILE);
		setRipTcColumn(RIP_TC_CLMN);
		setRipTcFile(RIP_TC_FILE);
		setRipWtcColumn(RIP_WTC_CLMN);
		setRipWtcFile(RIP_WTC_FILE);
		setRipItemNameColumn(RIP_ITM_NM_CLMN);
		setRipItemNameFile(RIP_ITM_NM_FILE);
		setRipLocationNameColumn(RIP_LCTN_NM_CLMN);
		setRipLocationNameFile(RIP_LCTN_NM_FILE);
		setRipUomNameColumn(RIP_UOM_NM_CLMN);
		setRipUomNameFile(RIP_UOM_NM_FILE);
		setRipQuantityColumn(RIP_QTY_CLMN);
		setRipQuantityFile(RIP_QTY_FILE);
		setRipUnitPriceColumn(RIP_UNT_PRC_CLMN);
		setRipUnitPriceFile(RIP_UNT_PRC_FILE);
		setRipTotalColumn(RIP_TOTAL_CLMN);
		setRipTotalFile(RIP_TOTAL_FILE);
		setRipMemoLineColumn(RIP_MEMO_LN_CLMN);
		setRipMemoLineFile(RIP_MEMO_LN_FILE);
		setRipInvoiceNumberColumn(RIP_INVC_NMBR_CLMN);
		setRipInvoiceNumberFile(RIP_INVC_NMBR_FILE);
		setRipApplyAmountColumn(RIP_APPLY_AMNT_CLMN);
		setRipApplyAmountFile(RIP_APPLY_AMNT_FILE);
		setRipApplyDiscountColumn(RIP_APPLY_DSCNT_CLMN);
		setRipApplyDiscountFile(RIP_APPLY_DSCNT_FILE);
		setRipAdCompany(RIP_AD_CMPNY);
		
		return null;
		
	}
	
	/**
	 * @ejb:create-method view-type="local"
	 **/
	public Integer ejbCreate(String RIP_TYP, byte RIP_IS_SMMRZD,
			short RIP_CSTMR_CLMN, String RIP_CSTMR_FILE, short RIP_DT_CLMN, String RIP_DT_FILE,
			short RIP_RCPT_NMBR_CLMN, String RIP_RCPT_NMBR_FILE, short RIP_RCPT_NMBR_LN_CLMN, 
			String RIP_RCPT_NMBR_LN_FILE, short RIP_RCPT_AMNT_CLMN, String RIP_RCPT_AMNT_FILE,
			short RIP_TC_CLMN, String RIP_TC_FILE, short RIP_WTC_CLMN, String RIP_WTC_FILE,
			short RIP_ITM_NM_CLMN, String RIP_ITM_NM_FILE, short RIP_LCTN_NM_CLMN, String RIP_LCTN_NM_FILE,
			short RIP_UOM_NM_CLMN, String RIP_UOM_NM_FILE, short RIP_QTY_CLMN, String RIP_QTY_FILE,
			short RIP_UNT_PRC_CLMN, String RIP_UNT_PRC_FILE, short RIP_TOTAL_CLMN, String RIP_TOTAL_FILE,
			short RIP_MEMO_LN_CLMN, String RIP_MEMO_LN_FILE, short RIP_INVC_NMBR_CLMN, String RIP_INVC_NMBR_FILE,
			short RIP_APPLY_AMNT_CLMN, String RIP_APPLY_AMNT_FILE, short RIP_APPLY_DSCNT_CLMN, 
			String RIP_APPLY_DSCNT_FILE, Integer RIP_AD_CMPNY) 
	throws CreateException {
		
		Debug.print("ArReceiptImportPreferenceBean ejbCreate");
		
		setRipType(RIP_TYP);
		setRipIsSummarized(RIP_IS_SMMRZD);
		setRipCustomerColumn(RIP_CSTMR_CLMN);
		setRipCustomerFile(RIP_CSTMR_FILE);
		setRipDateColumn(RIP_DT_CLMN);
		setRipDateFile(RIP_DT_FILE);
		setRipReceiptNumberColumn(RIP_RCPT_NMBR_CLMN);
		setRipReceiptNumberFile(RIP_RCPT_NMBR_FILE);
		setRipReceiptNumberLineColumn(RIP_RCPT_NMBR_LN_CLMN);
		setRipReceiptNumberLineFile(RIP_RCPT_NMBR_LN_FILE);
		setRipReceiptAmountColumn(RIP_RCPT_AMNT_CLMN);
		setRipReceiptAmountFile(RIP_RCPT_AMNT_FILE);
		setRipTcColumn(RIP_TC_CLMN);
		setRipTcFile(RIP_TC_FILE);
		setRipWtcColumn(RIP_WTC_CLMN);
		setRipWtcFile(RIP_WTC_FILE);
		setRipItemNameColumn(RIP_ITM_NM_CLMN);
		setRipItemNameFile(RIP_ITM_NM_FILE);
		setRipLocationNameColumn(RIP_LCTN_NM_CLMN);
		setRipLocationNameFile(RIP_LCTN_NM_FILE);
		setRipUomNameColumn(RIP_UOM_NM_CLMN);
		setRipUomNameFile(RIP_UOM_NM_FILE);
		setRipQuantityColumn(RIP_QTY_CLMN);
		setRipQuantityFile(RIP_QTY_FILE);
		setRipUnitPriceColumn(RIP_UNT_PRC_CLMN);
		setRipUnitPriceFile(RIP_UNT_PRC_FILE);
		setRipTotalColumn(RIP_TOTAL_CLMN);
		setRipTotalFile(RIP_TOTAL_FILE);
		setRipMemoLineColumn(RIP_MEMO_LN_CLMN);
		setRipMemoLineFile(RIP_MEMO_LN_FILE);
		setRipInvoiceNumberColumn(RIP_INVC_NMBR_CLMN);
		setRipInvoiceNumberFile(RIP_INVC_NMBR_FILE);
		setRipApplyAmountColumn(RIP_APPLY_AMNT_CLMN);
		setRipApplyAmountFile(RIP_APPLY_AMNT_FILE);
		setRipApplyDiscountColumn(RIP_APPLY_DSCNT_CLMN);
		setRipApplyDiscountFile(RIP_APPLY_DSCNT_FILE);
		setRipAdCompany(RIP_AD_CMPNY);
		
		return null;
		
	}
	
	public void ejbPostCreate(Integer RIP_CODE, String RIP_TYP, byte RIP_IS_SMMRZD,
			short RIP_CSTMR_CLMN, String RIP_CSTMR_FILE, short RIP_DT_CLMN, String RIP_DT_FILE,
			short RIP_RCPT_NMBR_CLMN, String RIP_RCPT_NMBR_FILE, short RIP_RCPT_NMBR_LN_CLMN, 
			String RIP_RCPT_NMBR_LN_FILE, short RIP_RCPT_AMNT_CLMN, String RIP_RCPT_AMNT_FILE,
			short RIP_TC_CLMN, String RIP_TC_FILE, short RIP_WTC_CLMN, String RIP_WTC_FILE,
			short RIP_ITM_NM_CLMN, String RIP_ITM_NM_FILE, short RIP_LCTN_NM_CLMN, String RIP_LCTN_NM_FILE,
			short RIP_UOM_NM_CLMN, String RIP_UOM_NM_FILE, short RIP_QTY_CLMN, String RIP_QTY_FILE,
			short RIP_UNT_PRC_CLMN, String RIP_UNT_PRC_FILE, short RIP_TOTAL_CLMN, String RIP_TOTAL_FILE,
			short RIP_MEMO_LN_CLMN, String RIP_MEMO_LN_FILE, short RIP_INVC_NMBR_CLMN, String RIP_INVC_NMBR_FILE,
			short RIP_APPLY_AMNT_CLMN, String RIP_APPLY_AMNT_FILE, short RIP_APPLY_DSCNT_CLMN, 
			String RIP_APPLY_DSCNT_FILE, Integer RIP_AD_CMPNY)
	throws CreateException { }
	
	public void ejbPostCreate(String RIP_TYP, byte RIP_IS_SMMRZD,
			short RIP_CSTMR_CLMN, String RIP_CSTMR_FILE, short RIP_DT_CLMN, String RIP_DT_FILE,
			short RIP_RCPT_NMBR_CLMN, String RIP_RCPT_NMBR_FILE, short RIP_RCPT_NMBR_LN_CLMN, 
			String RIP_RCPT_NMBR_LN_FILE, short RIP_RCPT_AMNT_CLMN, String RIP_RCPT_AMNT_FILE,
			short RIP_TC_CLMN, String RIP_TC_FILE, short RIP_WTC_CLMN, String RIP_WTC_FILE,
			short RIP_ITM_NM_CLMN, String RIP_ITM_NM_FILE, short RIP_LCTN_NM_CLMN, String RIP_LCTN_NM_FILE,
			short RIP_UOM_NM_CLMN, String RIP_UOM_NM_FILE, short RIP_QTY_CLMN, String RIP_QTY_FILE,
			short RIP_UNT_PRC_CLMN, String RIP_UNT_PRC_FILE, short RIP_TOTAL_CLMN, String RIP_TOTAL_FILE,
			short RIP_MEMO_LN_CLMN, String RIP_MEMO_LN_FILE, short RIP_INVC_NMBR_CLMN, String RIP_INVC_NMBR_FILE,
			short RIP_APPLY_AMNT_CLMN, String RIP_APPLY_AMNT_FILE, short RIP_APPLY_DSCNT_CLMN, 
			String RIP_APPLY_DSCNT_FILE, Integer RIP_AD_CMPNY)
	throws CreateException { }
}