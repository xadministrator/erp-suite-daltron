/*
 * com/ejb/ar/ArReceiptImportPreferenceBean.java
 *
 * Created on January 12, 2006, 11:54 AM
 */

package com.ejb.ar;

import java.util.Collection;

import javax.ejb.CreateException;
import javax.ejb.FinderException;

import com.util.AbstractEntityBean;
import com.util.Debug;

/**
 *
 * @author  Aliza D.J. Anos
 *
 * @ejb:bean name="ArReceiptImportPreferenceLineEJB"
 *           display-name="Receipt Import Preference Line Entity"
 *           type="CMP"
 *           cmp-version="2.x"
 *           view-type="local"
 *           local-jndi-name="omega-ejb/ArReceiptImportPreferenceLineEJB"
 *           schema="ArReceiptImportPreferenceLine"
 *           primkey-field="rilCode"
 *
 * @ejb:pk class="java.lang.Integer"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="aruser"
 *                        role-link="aruserlink"
 *
 * @ejb:permission role-name="aruser"
 *
 * @ejb:interface local-class="com.ejb.ar.LocalArReceiptImportPreferenceLine"
 *                local-extends="javax.ejb.EJBLocalObject"
 *
 * @ejb:home local-class="com.ejb.ar.LocalArReceiptImportPreferenceLineHome"
 *           local-extends="javax.ejb.EJBLocalHome"
 *
 * @ejb:select signature="Collection ejbSelectGeneric(java.lang.String jbossQl, java.lang.Object[] args)"
 *             result-type-mapping="Local"
 *             method-intf="LocalHome"
 *             query="SELECT OBJECT(ril) FROM ArReceiptImportPreferenceLine ril"
 *
 * @jboss:query signature="Collection ejbSelectGeneric(java.lang.String jbossQl, java.lang.Object[] args)"
 *                         dynamic="true"
 *
 * @ejb:value-object match="*"
 *             name="ArReceiptImportPreferenceLine"
 *
 * @jboss:persistence table-name="AR_RCPT_IMPRT_PRFRNC_LN"
 *
 * @jboss:entity-command name="mysql-get-generated-keys"
 */

public abstract class ArReceiptImportPreferenceLineBean extends AbstractEntityBean {
	
	//PERSISTENT METHODS
	
	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 * @ejb:pk-field
	 *
	 * @jboss:column-name name="RIL_CODE"
	 *
	 * @jboss:persistence auto-increment="true"
	 **/
	public abstract Integer getRilCode();         
	public abstract void setRilCode(Integer RIL_CODE);
	
	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="RIL_TYP"
	 **/
	public abstract String getRilType();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setRilType(String RIL_TYP);
	
	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="RIL_NM"
	 **/
	public abstract String getRilName();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setRilName(String RIL_NM);
	
	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="RIL_GL_ACCNT_NUM"
	 **/
	public abstract String getRilGlAccountNumber();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setRilGlAccountNumber(String RIL_GL_ACCNT_NUM);
	
	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="RIL_BNK_ACCNT_NM"
	 **/
	public abstract String getRilBankAccountName();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setRilBankAccountName(String RIL_BNK_ACCNT_NM);
	
	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="RIL_CLMN"
	 **/
	public abstract short getRilColumn();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setRilColumn(short RIL_CLMN);
	
	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="RIL_FILE"
	 **/
	public abstract String getRilFile();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setRilFile(String RIL_FILE);
	
	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="RIL_AMNT_CLMN"
	 **/
	public abstract short getRilAmountColumn();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setRilAmountColumn(short RIL_AMNT_CLMN);
	
	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="RIL_AD_CMPNY"
	 **/
	public abstract Integer getRilAdCompany();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setRilAdCompany(Integer RIL_AD_CMPNY);
	
	
	//RELATIONSHIP METHODS
	
	/**
	 * @ejb:interface-method view-type="local"
	 * @ejb:relation name="receiptimportpreference-receiptimportpreferencelines"
	 *               role-name="receiptimportpreferenceline-has-one-receiptimportpreference"
	 *               cascade-delete="yes"
	 *
	 * @jboss:relation related-pk-field="ripCode"
	 *                 fk-column="AR_RECEIPT_IMPORT_PREFERENCE"
	 */
	public abstract LocalArReceiptImportPreference getArReceiptImportPreference();
	public abstract void setArReceiptImportPreference(LocalArReceiptImportPreference arReceiptImportPreferences);
	
	
	/**
	 * @jboss:dynamic-ql
	 */
	public abstract Collection ejbSelectGeneric(java.lang.String jbossQl, java.lang.Object[] args)
	throws FinderException;
	
	
	//BUSINESS METHODS
	
	/**
	 * @ejb:home-method view-type="local"
	 */
	public Collection ejbHomeGetRilByCriteria(java.lang.String jbossQl, java.lang.Object[] args)
	throws FinderException {
		
		return ejbSelectGeneric(jbossQl, args);
	}
	
	
	//ENTITY METHODS
	
	/**
	 * @ejb:create-method view-type="local"
	 **/
	public Integer ejbCreate(Integer RIL_CODE, String RIL_TYP, String RIL_NM, 
			String RIL_GL_ACCNT_NUM, String RIL_BNK_ACCNT_NM, short RIL_CLMN, 
			String RIL_FILE, short RIL_AMNT_CLMN, Integer RIL_AD_CMPNY)
	throws CreateException {
		
		Debug.print("ArReceiptImportPreferenceLineBean ejbCreate");
		
		setRilCode(RIL_CODE);
		setRilType(RIL_TYP);
		setRilName(RIL_NM);
		setRilGlAccountNumber(RIL_GL_ACCNT_NUM);
		setRilBankAccountName(RIL_BNK_ACCNT_NM);
		setRilColumn(RIL_CLMN);
		setRilFile(RIL_FILE);
		setRilAmountColumn(RIL_AMNT_CLMN);
		setRilAdCompany(RIL_AD_CMPNY);
		
		return null;
		
	}
	
	/**
	 * @ejb:create-method view-type="local"
	 **/
	public Integer ejbCreate(String RIL_TYP, String RIL_NM, 
			String RIL_GL_ACCNT_NUM, String RIL_BNK_ACCNT_NM, short RIL_CLMN, 
			String RIL_FILE, short RIL_AMNT_CLMN, Integer RIL_AD_CMPNY)
	throws CreateException {
		
		Debug.print("ArReceiptImportPreferenceLineBean ejbCreate");
		
		setRilType(RIL_TYP);
		setRilName(RIL_NM);
		setRilGlAccountNumber(RIL_GL_ACCNT_NUM);
		setRilBankAccountName(RIL_BNK_ACCNT_NM);
		setRilColumn(RIL_CLMN);
		setRilFile(RIL_FILE);
		setRilAmountColumn(RIL_AMNT_CLMN);
		setRilAdCompany(RIL_AD_CMPNY);
		
		return null;
		
	}
	
	public void ejbPostCreate(Integer RIL_CODE, String RIL_TYP, String RIL_NM, 
			String RIL_GL_ACCNT_NUM, String RIL_BNK_ACCNT_NM, short RIL_CLMN, 
			String RIL_FILE, short RIL_AMNT_CLMN, Integer RIL_AD_CMPNY)
	throws CreateException { }
	
	public void ejbPostCreate(String RIL_TYP, String RIL_NM, 
			String RIL_GL_ACCNT_NUM, String RIL_BNK_ACCNT_NM, short RIL_CLMN, 
			String RIL_FILE, short RIL_AMNT_CLMN, Integer RIL_AD_CMPNY)
	throws CreateException { }
	
}