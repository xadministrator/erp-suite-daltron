package com.ejb.ar;

import java.util.Collection;
import java.util.Date;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;

import com.util.AbstractEntityBean;
import com.util.Debug;

/**
 *
 * @ejb:bean name="ArSalesOrderEJB"
 *           display-name="Sales Order Entity"
 *           type="CMP"
 *           cmp-version="2.x"
 *           view-type="local"
 *           local-jndi-name="omega-ejb/ArSalesOrderEJB"
 *           schema="ArSalesOrder"
 *           primkey-field="soCode"
 *
 * @ejb:pk class="java.lang.Integer"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="aruser"
 *                        role-link="aruserlink"
 *
 * @ejb:permission role-name="aruser"
 *
 * @ejb:interface local-class="com.ejb.ar.LocalArSalesOrder"
 *                local-extends="javax.ejb.EJBLocalObject"
 *
 * @ejb:home local-class="com.ejb.ar.LocalArSalesOrderHome"
 *           local-extends="javax.ejb.EJBLocalHome"
 *
 * @ejb:finder signature="LocalArSalesOrder findBySoDocumentNumberAndBrCode(java.lang.String SO_DCMNT_NMBR, java.lang.Integer SO_AD_BRNCH, java.lang.Integer SO_AD_CMPNY)"
 *           query="SELECT OBJECT(so) FROM ArSalesOrder so  WHERE so.soDocumentNumber = ?1 AND so.soAdBranch = ?2 AND so.soAdCompany = ?3"
 *
 * @ejb:finder signature="LocalArSalesOrder findBySoDocumentNumber(java.lang.String SO_DCMNT_NMBR, java.lang.Integer SO_AD_CMPNY)"
 *           query="SELECT OBJECT(so) FROM ArSalesOrder so  WHERE so.soDocumentNumber = ?1 AND so.soAdCompany = ?2"
 *
 * @ejb:finder signature="LocalArSalesOrder findBySoDocumentNumberAndCstCustomerCode(java.lang.String SO_DCMNT_NMBR, java.lang.String CST_CSTMR_CODE, java.lang.Integer SO_AD_CMPNY)"
 *           query="SELECT OBJECT(so) FROM ArSalesOrder so  WHERE so.soDocumentNumber = ?1 AND so.arCustomer.cstCustomerCode = ?2 AND so.soVoid = 0 AND so.soAdCompany = ?3"
 *
 * @ejb:finder signature="Collection findPostedSoByCstCustomerCode(java.lang.String CST_CSTMR_CD, java.lang.Integer SO_AD_CMPNY)"
 * 			 query="SELECT OBJECT(so) FROM ArSalesOrder so WHERE so.soPosted = 1 AND so.arCustomer.cstCustomerCode = ?1 AND so.soAdCompany = ?2"
 *
 * @ejb:finder signature="Collection findSoPostedAndApprovedByDateRangeAndAdCompany(java.util.Date SO_DT_FRM, java.util.Date SO_DT_TO, java.lang.Integer SO_AD_CMPNY)"
 *           query="SELECT OBJECT(so) FROM ArSalesOrder so  WHERE so.soPosted = 1 AND so.soApprovalStatus = 'APPROVED' AND so.soDateApprovedRejected >= ?1 AND so.soDateApprovedRejected <= ?2 AND so.soAdCompany = ?3"
 *
 * @ejb:finder signature="Collection findPostedSoByBrCode(java.lang.Integer SO_AD_BRNCH, java.lang.Integer SO_AD_CMPNY)"
 * 			 query="SELECT OBJECT(so) FROM ArSalesOrder so WHERE so.soPosted = 1 AND so.soVoid = 0 AND so.soAdBranch = ?1 AND so.soAdCompany = ?2"
 *
 *  @ejb:finder signature="Collection findPostedSoByMobileBrCode(java.lang.Integer SO_AD_BRNCH, java.lang.Integer SO_AD_CMPNY)"
 * 			 query="SELECT OBJECT(so) FROM ArSalesOrder so WHERE so.soPosted = 1 AND so.soVoid = 0 AND so.soMobile = 1 AND so.soAdBranch = ?1 AND so.soAdCompany = ?2"
 *
 * @ejb:finder signature="LocalArSalesOrder findBySoDocumentNumberAndCstCustomerCodeAndBrCode(java.lang.String SO_DCMNT_NMBR, java.lang.String CST_CSTMR_CODE, java.lang.Integer SO_AD_BRNCH, java.lang.Integer SO_AD_CMPNY)"
 *           query="SELECT OBJECT(so) FROM ArSalesOrder so  WHERE so.soDocumentNumber = ?1 AND so.arCustomer.cstCustomerCode = ?2 AND so.soAdBranch = ?3 AND so.soVoid = 0 AND so.soAdCompany = ?4"
 *
 * @ejb:finder signature="Collection findDraftSoByBrCode(java.lang.Integer SO_AD_BRNCH, java.lang.Integer SO_AD_CMPNY)"
 *             query="SELECT OBJECT(so) FROM ArSalesOrder so WHERE so.soPosted = 0 AND so.soApprovalStatus IS NULL AND so.soVoid = 0 AND so.soAdBranch = ?1 AND so.soAdCompany = ?2"
 *
 * @ejb:finder signature="Collection findDraftSoByTransactionStatus(java.lang.String SO_TRNSCTN_STTS, java.lang.Integer SO_AD_BRNCH, java.lang.Integer SO_AD_CMPNY)"
 *             query="SELECT OBJECT(so) FROM ArSalesOrder so WHERE so.soPosted = 0 AND so.soApprovalStatus IS NULL AND so.soVoid = 0 AND so.soTransactionStatus = ?1 AND so.soAdBranch = ?2 AND so.soAdCompany = ?3"
 *   
 * @ejb:select signature="Collection ejbSelectGeneric(java.lang.String jbossQl, java.lang.Object[] args)"
 *             result-type-mapping="Local"
 *             method-intf="LocalHome"
 *             query="SELECT OBJECT(so) FROM ArSalesOrder so"
 *
 * @jboss:query signature="Collection ejbSelectGeneric(java.lang.String jbossQl, java.lang.Object[] args)"
 *                         dynamic="true"
 *
 * @ejb:value-object match="*"
 *             name="ArSalesOrder"
 *
 * @jboss:persistence table-name="AR_SLS_ORDR"
 *
 * @jboss:entity-command name="mysql-get-generated-keys"
 */

public abstract class ArSalesOrderBean extends AbstractEntityBean {

    //  PERSISTENT METHODS

    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     * @ejb:pk-field
     *
     * @jboss:column-name name="SO_CODE"
     *
     * @jboss:persistence auto-increment="true"
     **/
    public abstract Integer getSoCode();
    public abstract void setSoCode(Integer SO_CODE);

     /**
	  * @ejb:persistent-field
	  * @ejb:interface-method view-type="local"
	  *
	  * @jboss:column-name name="SO_DT"
	  **/
	 public abstract Date getSoDate();
	 /**
	  * @ejb:interface-method view-type="local"
	  **/
	 public abstract void setSoDate(Date SO_DT);
	 
	 /**
	  * @ejb:persistent-field
	  * @ejb:interface-method view-type="local"
	  *
	  * @jboss:column-name name="SO_DCMNT_TYP"
	  **/
	 public abstract String getSoDocumentType();
	 /**
	  * @ejb:interface-method view-type="local"
	  **/
	 public abstract void setSoDocumentType(String SO_DCMNT_TYP);

	 /**
	  * @ejb:persistent-field
	  * @ejb:interface-method view-type="local"
	  *
	  * @jboss:column-name name="SO_DCMNT_NMBR"
	  **/
	 public abstract String getSoDocumentNumber();
	 /**
	  * @ejb:interface-method view-type="local"
	  **/
	 public abstract void setSoDocumentNumber(String SO_DCMNT_NMBR);

	 /**
	  * @ejb:persistent-field
	  * @ejb:interface-method view-type="local"
	  *
	  * @jboss:column-name name="SO_RFRNC_NMBR"
	  **/
	 public abstract String getSoReferenceNumber();
	 /**
	  * @ejb:interface-method view-type="local"
	  **/
	 public abstract void setSoReferenceNumber(String SO_RFRNC_NMBR);

	 /**
	  * @ejb:persistent-field
	  * @ejb:interface-method view-type="local"
	  *
	  * @jboss:column-name name="SO_TRNSCTN_TYP"
	  **/
	 public abstract String getSoTransactionType();
	 /**
	  * @ejb:interface-method view-type="local"
	  **/
	 public abstract void setSoTransactionType(String SO_TRNSCTN_TYP);

	 /**
	  * @ejb:persistent-field
	  * @ejb:interface-method view-type="local"
	  *
	  * @jboss:column-name name="SO_TRNSCTN_STTS"
	  **/
	 public abstract String getSoTransactionStatus();
	 /**
	  * @ejb:interface-method view-type="local"
	  **/
	 public abstract void setSoTransactionStatus(String SO_TRNSCTN_STTS);


	 /**
	  * @ejb:persistent-field
	  * @ejb:interface-method view-type="local"
	  *
	  * @jboss:column-name name="SO_DESC"
	  **/
	 public abstract String getSoDescription();
	 /**
	  * @ejb:interface-method view-type="local"
	  **/
	 public abstract void setSoDescription(String SO_DESC);

	 /**
	  * @ejb:persistent-field
	  * @ejb:interface-method view-type="local"
	  *
	  * @jboss:column-name name="SO_BLL_TO"
	  **/
	 public abstract String getSoBillTo();
	 /**
	  * @ejb:interface-method view-type="local"
	  **/
	 public abstract void setSoBillTo(String SO_BLL_TO);

	 /**
	  * @ejb:persistent-field
	  * @ejb:interface-method view-type="local"
	  *
	  * @jboss:column-name name="SO_SHP_TO"
	  **/
	 public abstract String getSoShipTo();
	 /**
	  * @ejb:interface-method view-type="local"
	  **/
	 public abstract void setSoShipTo(String SO_SHP_TO);

	 /**
	  * @ejb:persistent-field
	  * @ejb:interface-method view-type="local"
	  *
	  * @jboss:column-name name="SO_CNVRSN_DT"
	  **/
	 public abstract Date getSoConversionDate();
	 /**
	  * @ejb:interface-method view-type="local"
	  **/
	 public abstract void setSoConversionDate(Date SO_CNVRSN_DT);

	 /**
	  * @ejb:persistent-field
	  * @ejb:interface-method view-type="local"
	  *
	  * @jboss:column-name name="SO_CNVRSN_RT"
	  **/
	 public abstract double getSoConversionRate();
	 /**
	  * @ejb:interface-method view-type="local"
	  **/
	 public abstract void setSoConversionRate(double SO_CNVRSN_RT);

	 /**
	  * @ejb:persistent-field
	  * @ejb:interface-method view-type="local"
	  *
	  * @jboss:column-name name="SO_VD"
	  **/
	 public abstract byte getSoVoid();
	 /**
	  * @ejb:interface-method view-type="local"
	  **/
	 public abstract void setSoVoid(byte SO_VD);

	 /**
	  * @ejb:persistent-field
	  * @ejb:interface-method view-type="local"
	  *
	  * @jboss:column-name name="SO_MBL"
	  **/
	 public abstract byte getSoMobile();
	 /**
	  * @ejb:interface-method view-type="local"
	  **/
	 public abstract void setSoMobile(byte SO_MBL);

	 
	 /**
	  * @ejb:persistent-field
	  * @ejb:interface-method view-type="local"
	  *
	  * @jboss:column-name name="SO_SHPPNG_LN"
	  **/
	 public abstract String getSoShippingLine();
	 /**
	  * @ejb:interface-method view-type="local"
	  **/
	 public abstract void setSoShippingLine(String SO_SHPPNG_LN);
	 
	 
	 /**
	  * @ejb:persistent-field
	  * @ejb:interface-method view-type="local"
	  *
	  * @jboss:column-name name="SO_PRT"
	  **/
	 public abstract String getSoPort();
	 /**
	  * @ejb:interface-method view-type="local"
	  **/
	 public abstract void setSoPort(String SO_PRT);

	 
	 
	 /**
	  * @ejb:persistent-field
	  * @ejb:interface-method view-type="local"
	  *
	  * @jboss:column-name name="SO_APPRVL_STATUS"
	  **/
	 public abstract String getSoApprovalStatus();
	 /**
	  * @ejb:interface-method view-type="local"
	  **/
	 public abstract void setSoApprovalStatus(String SO_APPRVL_STATUS);

	 /**
	  * @ejb:persistent-field
	  * @ejb:interface-method view-type="local"
	  *
	  * @jboss:column-name name="SO_PSTD"
	  **/
	 public abstract byte getSoPosted();
	 /**
	  * @ejb:interface-method view-type="local"
	  **/
	 public abstract void setSoPosted(byte SO_PSTD);

	 /**
	  * @ejb:persistent-field
	  * @ejb:interface-method view-type="local"
	  *
	  * @jboss:column-name name="SO_RSN_FR_RJCTN"
	  **/
	 public abstract String getSoReasonForRejection();
	 /**
	  * @ejb:interface-method view-type="local"
	  **/
	 public abstract void setSoReasonForRejection(String SO_RSN_FR_RJCTN);

	 /**
	  * @ejb:persistent-field
	  * @ejb:interface-method view-type="local"
	  *
	  * @jboss:column-name name="SO_CRTD_BY"
	  **/
	 public abstract String getSoCreatedBy();
	 /**
	  * @ejb:interface-method view-type="local"
	  **/
	 public abstract void setSoCreatedBy(String SO_CRTD_BY);

	 /**
	  * @ejb:persistent-field
	  * @ejb:interface-method view-type="local"
	  *
	  * @jboss:column-name name="SO_DT_CRTD"
	  **/
	 public abstract Date getSoDateCreated();
	 /**
	  * @ejb:interface-method view-type="local"
	  **/
	 public abstract void setSoDateCreated(Date SO_DT_CRTD);

	 /**
	  * @ejb:persistent-field
	  * @ejb:interface-method view-type="local"
	  *
	  * @jboss:column-name name="SO_LST_MDFD_BY"
	  **/
	 public abstract String getSoLastModifiedBy();
	 /**
	  * @ejb:interface-method view-type="local"
	  **/
	 public abstract void setSoLastModifiedBy(String SO_LST_MDFD_BY);

	 /**
	  * @ejb:persistent-field
	  * @ejb:interface-method view-type="local"
	  *
	  * @jboss:column-name name="SO_DT_LST_MDFD"
	  **/
	 public abstract Date getSoDateLastModified();
	 /**
	  * @ejb:interface-method view-type="local"
	  **/
	 public abstract void setSoDateLastModified(Date SO_DT_LST_MDFD);

	 /**
	  * @ejb:persistent-field
	  * @ejb:interface-method view-type="local"
	  *
	  * @jboss:column-name name="SO_APPRVD_RJCTD_BY"
	  **/
	 public abstract String getSoApprovedRejectedBy();
	 /**
	  * @ejb:interface-method view-type="local"
	  **/
	 public abstract void setSoApprovedRejectedBy(String SO_APPRVD_RJCTD_BY);

	 /**
	  * @ejb:persistent-field
	  * @ejb:interface-method view-type="local"
	  *
	  * @jboss:column-name name="SO_DT_APPRVD_RJCTD"
	  **/
	 public abstract Date getSoDateApprovedRejected();
	 /**
	  * @ejb:interface-method view-type="local"
	  **/
	 public abstract void setSoDateApprovedRejected(Date SO_DT_APPRVD_RJCTD);

	 /**
	  * @ejb:persistent-field
	  * @ejb:interface-method view-type="local"
	  *
	  * @jboss:column-name name="SO_PSTD_BY"
	  **/
	 public abstract String getSoPostedBy();
	 /**
	  * @ejb:interface-method view-type="local"
	  **/
	 public abstract void setSoPostedBy(String SO_PSTD_BY);

	 /**
	  * @ejb:persistent-field
	  * @ejb:interface-method view-type="local"
	  *
	  * @jboss:column-name name="SO_DT_PSTD"
	  **/
	 public abstract Date getSoDatePosted();
	 /**
	  * @ejb:interface-method view-type="local"
	  **/
	 public abstract void setSoDatePosted(Date SO_DT_PSTD);

	 /**
	  * @ejb:persistent-field
	  * @ejb:interface-method view-type="local"
	  *
	  * @jboss:column-name name="SO_LCK"
	  **/
	 public abstract byte getSoLock();
	 /**
	  * @ejb:interface-method view-type="local"
	  **/
	 public abstract void setSoLock(byte SO_LCK);

	 /**
	  * @ejb:persistent-field
	  * @ejb:interface-method view-type="local"
	  *
	  * @jboss:column-name name="SO_BO_LCK"
	  **/
	 public abstract byte getSoBoLock();
	 /**
	  * @ejb:interface-method view-type="local"
	  **/
	 public abstract void setSoBoLock(byte SO_BO_LCK);

	 /**
	  * @ejb:persistent-field
	  * @ejb:interface-method view-type="local"
	  *
	  * @jboss:column-name name="SO_MMO"
	  **/
	 public abstract String getSoMemo();
	 /**
	  * @ejb:interface-method view-type="local"
	  **/
	 public abstract void setSoMemo(String SO_MMO);

	 /**
	  * @ejb:persistent-field
	  * @ejb:interface-method view-type="local"
	  *
	  * @jboss:column-name name="SO_ORDR_STTS"
	  **/
	 public abstract String getSoOrderStatus();
	 /**
	  * @ejb:interface-method view-type="local"
	  **/
	 public abstract void setSoOrderStatus(String SO_ORDR_STTS);


	 /**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="REPORT_PARAMETER"
	 **/
	public abstract String getReportParameter();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setReportParameter(String REPORT_PARAMETER);


	 /**
	  * @ejb:persistent-field
	  * @ejb:interface-method view-type="local"
	  *
	  * @jboss:column-name name="SO_AD_BRNCH"
	  **/
	 public abstract Integer getSoAdBranch();
	 /**
	  * @ejb:interface-method view-type="local"
	  **/
	 public abstract void setSoAdBranch(Integer SO_AD_BRNCH);

	 /**
	  * @ejb:persistent-field
	  * @ejb:interface-method view-type="local"
	  *
	  * @jboss:column-name name="SO_AD_CMPNY"
	  **/
	 public abstract Integer getSoAdCompany();
	 /**
	  * @ejb:interface-method view-type="local"
	  **/
	 public abstract void setSoAdCompany(Integer SO_AD_CMPNY);

	 // RELATIONSHIP FIELDS

	 /**
      * @ejb:interface-method view-type="local"
      * @ejb:relation name="customer-salesorder"
      *               role-name="salesorder-has-one-customer"
      *
      * @jboss:relation related-pk-field="cstCode"
      *                 fk-column="AR_CUSTOMER"
      */
     public abstract LocalArCustomer getArCustomer();
     public abstract void setArCustomer(LocalArCustomer arCustomer);

     /**
      * @ejb:interface-method view-type="local"
      * @ejb:relation name="paymentterm-salesorder"
      *               role-name="salesorder-has-one-paymentterm"
      *
      * @jboss:relation related-pk-field="pytCode"
      *                 fk-column="AD_PAYMENT_TERM"
      */
     public abstract com.ejb.ad.LocalAdPaymentTerm getAdPaymentTerm();
     public abstract void setAdPaymentTerm(com.ejb.ad.LocalAdPaymentTerm adPaymentTerm);

     /**
      * @ejb:interface-method view-type="local"
      * @ejb:relation name="functionalcurrency-salesorder"
      *               role-name="salesorder-has-one-functionalcurrency"
      *
      * @jboss:relation related-pk-field="fcCode"
      *                 fk-column="GL_FUNCTIONAL_CURRENCY"
      */
     public abstract com.ejb.gl.LocalGlFunctionalCurrency getGlFunctionalCurrency();
     public abstract void setGlFunctionalCurrency(com.ejb.gl.LocalGlFunctionalCurrency glFunctionalCurrency);

     /**
      * @ejb:interface-method view-type="local"
      * @ejb:relation name="artaxcode-salesorder"
      *               role-name="salesorder-has-one-artaxcode"
      *
      * @jboss:relation related-pk-field="tcCode"
      *                 fk-column="AR_TAX_CODE"
      */
     public abstract LocalArTaxCode getArTaxCode();
     public abstract void setArTaxCode(LocalArTaxCode arTaxCode);

     /**
      * @ejb:interface-method view-type="local"
      * @ejb:relation name="salesorder-salesorderline"
      *               role-name="salesorder-has-many-salesorderline"
      */
     public abstract Collection getArSalesOrderLines();
     public abstract void setArSalesOrderLines(Collection arSalesOrderLines);


     /**
      * @ejb:interface-method view-type="local"
      * @ejb:relation name="salesorder-adjustments"
      *               role-name="salesorder-has-many-adjustment"
      */
     public abstract Collection getCmAdjustments();
     public abstract void setCmAdjustments(Collection cmAdjustments);


     
     /**
      * @ejb:interface-method view-type="local"
      * @ejb:relation name="salesorder-deliveries"
      *               role-name="salesorder-has-many-delivery"
      */	
     public abstract Collection getArDeliveries();
     public abstract void setArDeliveries(Collection arDeliveries);
     
     
     /**
      * @ejb:interface-method view-type="local"
      * @ejb:relation name="salesperson-salesorder"
      *               role-name="salesorder-has-one-salesperson"
      *
      * @jboss:relation related-pk-field="slpCode"
      *                 fk-column="AR_SALESPERSON"
      */
     public abstract LocalArSalesperson getArSalesperson();
     public abstract void setArSalesperson(LocalArSalesperson arSalesperson);

     
     
     /**
      * @jboss:dynamic-ql
      */
      public abstract Collection ejbSelectGeneric(java.lang.String jbossQl, java.lang.Object[] args)
         throws FinderException;

      // BUSINESS METHODS

      /**
       * @ejb:home-method view-type="local"
       */
       public Collection ejbHomeGetSOByCriteria(java.lang.String jbossQl, java.lang.Object[] args)
          throws FinderException {

          return ejbSelectGeneric(jbossQl, args);
       }

       /**
  	    * @ejb:interface-method view-type="local"
  	    **/
  	   public void addArSalesOrderLine(LocalArSalesOrderLine arSalesOrderLine) {

	        Debug.print("ArSalesOrderBean addArSalesOrderLine");

	        try {

	  	        Collection arSalesOrderLines = getArSalesOrderLines();
	  	        arSalesOrderLines.add(arSalesOrderLine);

	        } catch (Exception ex) {

	            throw new EJBException(ex.getMessage());

	        }

  	   }

       /**
        * @ejb:interface-method view-type="local"
        **/
        public void dropArSalesOrderLine(LocalArSalesOrderLine arSalesOrderLine) {

  		  Debug.print("ArSalesOrderBean dropArSalesOrderLine");

  		  try {

  		     Collection arSalesOrderLines = getArSalesOrderLines();
  		     arSalesOrderLines.remove(arSalesOrderLine);

  		  } catch (Exception ex) {

  		     throw new EJBException(ex.getMessage());

  		  }

       }

      // ENTITY METHODS

      /**
       * @ejb:create-method view-type="local"
       **/
      public Integer ejbCreate(Integer SO_CODE, Date SO_DT, String SO_DCMNT_NMBR, String SO_RFRNC_NMBR, String SO_TRNSCTN_TYP,
	  		String SO_DESC, String SO_SHPPNG_LN, String SO_PRT, String SO_BLL_TO, String SO_SHP_TO, Date SO_CNVRSN_DT, double SO_CNVRSN_RT,
	  		byte SO_VD, byte SO_MBL, String SO_APPRVL_STATUS, byte SO_PSTD, String SO_RSN_FR_RJCTN, String SO_CRTD_BY,
	  		Date SO_DT_CRTD, String SO_LST_MDFD_BY, Date SO_DT_LST_MDFD, String SO_APPRVD_RJCTD_BY,
	  		Date SO_DT_APPRVD_RJCTD, String SO_PSTD_BY, Date SO_DT_PSTD, byte SO_LCK, byte SO_BO_LCK,
	  		String SO_MMO, String SO_TRNSCTN_STTS, Integer SO_AD_BRNCH, Integer SO_AD_CMPNY)
	  		throws CreateException {

          Debug.print("ArSalesOrderBean ejbCreate");

          setSoCode(SO_CODE);
          setSoDate(SO_DT);
          setSoDocumentNumber(SO_DCMNT_NMBR);
          setSoReferenceNumber(SO_RFRNC_NMBR);
          setSoTransactionType(SO_TRNSCTN_TYP);
          setSoDescription(SO_DESC);
          setSoShippingLine(SO_SHPPNG_LN);
          setSoPort(SO_PRT);
          setSoBillTo(SO_BLL_TO);
          setSoShipTo(SO_SHP_TO);
          setSoConversionDate(SO_CNVRSN_DT);
          setSoConversionRate(SO_CNVRSN_RT);
          setSoVoid(SO_VD);
          setSoMobile(SO_MBL);
          setSoApprovalStatus(SO_APPRVL_STATUS);
          setSoPosted(SO_PSTD);
          setSoReasonForRejection(SO_RSN_FR_RJCTN);
          setSoCreatedBy(SO_CRTD_BY);
          setSoDateCreated(SO_DT_CRTD);
          setSoLastModifiedBy(SO_LST_MDFD_BY);
          setSoDateLastModified(SO_DT_LST_MDFD);
          setSoApprovedRejectedBy(SO_APPRVD_RJCTD_BY);
          setSoDateApprovedRejected(SO_DT_APPRVD_RJCTD);
          setSoPostedBy(SO_PSTD_BY);
          setSoDatePosted(SO_DT_PSTD);
          setSoLock(SO_LCK);
          setSoBoLock(SO_BO_LCK);
          setSoMemo(SO_MMO);
          setSoTransactionStatus(SO_TRNSCTN_STTS);
          setSoAdBranch(SO_AD_BRNCH);
          setSoAdCompany(SO_AD_CMPNY);

          return null;

      }

      /**
       * @ejb:create-method view-type="local"
       **/
      public Integer ejbCreate(Date SO_DT, String SO_DCMNT_NMBR, String SO_RFRNC_NMBR, String SO_TRNSCTN_TYP,
	  		String SO_DESC, String SO_SHPPNG_LN, String SO_PRT, String SO_BLL_TO, String SO_SHP_TO, Date SO_CNVRSN_DT, double SO_CNVRSN_RT,
	  		byte SO_VD, byte SO_MBL, String SO_APPRVL_STATUS, byte SO_PSTD, String SO_RSN_FR_RJCTN, String SO_CRTD_BY,
	  		Date SO_DT_CRTD, String SO_LST_MDFD_BY, Date SO_DT_LST_MDFD, String SO_APPRVD_RJCTD_BY,
	  		Date SO_DT_APPRVD_RJCTD, String SO_PSTD_BY, Date SO_DT_PSTD, byte SO_LCK, byte SO_BO_LCK,
	  		String SO_MMO, String SO_TRNSCTN_STTS, Integer SO_AD_BRNCH, Integer SO_AD_CMPNY)
	  		throws CreateException {

          Debug.print("ArSalesOrderBean ejbCreate");

          setSoDate(SO_DT);
          setSoDocumentNumber(SO_DCMNT_NMBR);
          setSoReferenceNumber(SO_RFRNC_NMBR);
          setSoTransactionType(SO_TRNSCTN_TYP);
          setSoDescription(SO_DESC);
          setSoShippingLine(SO_SHPPNG_LN);
          setSoPort(SO_PRT);
          setSoBillTo(SO_BLL_TO);
          setSoShipTo(SO_SHP_TO);
          setSoConversionDate(SO_CNVRSN_DT);
          setSoConversionRate(SO_CNVRSN_RT);
          setSoVoid(SO_VD);
          setSoMobile(SO_MBL);
          setSoApprovalStatus(SO_APPRVL_STATUS);
          setSoPosted(SO_PSTD);
          setSoReasonForRejection(SO_RSN_FR_RJCTN);
          setSoCreatedBy(SO_CRTD_BY);
          setSoDateCreated(SO_DT_CRTD);
          setSoLastModifiedBy(SO_LST_MDFD_BY);
          setSoDateLastModified(SO_DT_LST_MDFD);
          setSoApprovedRejectedBy(SO_APPRVD_RJCTD_BY);
          setSoDateApprovedRejected(SO_DT_APPRVD_RJCTD);
          setSoPostedBy(SO_PSTD_BY);
          setSoDatePosted(SO_DT_PSTD);
          setSoLock(SO_LCK);
          setSoBoLock(SO_BO_LCK);
          setSoMemo(SO_MMO);
          setSoTransactionStatus(SO_TRNSCTN_STTS);
          setSoAdBranch(SO_AD_BRNCH);
          setSoAdCompany(SO_AD_CMPNY);

          return null;

      }

      public void ejbPostCreate(Integer SO_CODE, Date SO_DT, String SO_DCMNT_NMBR, String SO_RFRNC_NMBR, String SO_TRNSCTN_TYP,
  	  		String SO_DESC, String SO_SHPPNG_LN, String SO_PRT, String SO_BLL_TO, String SO_SHP_TO, Date SO_CNVRSN_DT, double SO_CNVRSN_RT,
  	  		byte SO_VD, byte SO_MBL, String SO_APPRVL_STATUS, byte SO_PSTD, String SO_RSN_FR_RJCTN, String SO_CRTD_BY,
  	  		Date SO_DT_CRTD, String SO_LST_MDFD_BY, Date SO_DT_LST_MDFD, String SO_APPRVD_RJCTD_BY,
  	  		Date SO_DT_APPRVD_RJCTD, String SO_PSTD_BY, Date SO_DT_PSTD, byte SO_LCK, byte SO_BO_LCK,
  	  		String SO_MMO, String SO_TRNSCTN_STTS, Integer SO_AD_BRNCH, Integer SO_AD_CMPNY)
  	  		throws CreateException { }

      public void ejbPostCreate(Date SO_DT, String SO_DCMNT_NMBR, String SO_RFRNC_NMBR, String SO_TRNSCTN_TYP,
  	  		String SO_DESC, String SO_SHPPNG_LN, String SO_PRT, String SO_BLL_TO, String SO_SHP_TO, Date SO_CNVRSN_DT, double SO_CNVRSN_RT,
  	  		byte SO_VD, byte SO_MBL, String SO_APPRVL_STATUS, byte SO_PSTD, String SO_RSN_FR_RJCTN, String SO_CRTD_BY,
  	  		Date SO_DT_CRTD, String SO_LST_MDFD_BY, Date SO_DT_LST_MDFD, String SO_APPRVD_RJCTD_BY,
  	  		Date SO_DT_APPRVD_RJCTD, String SO_PSTD_BY, Date SO_DT_PSTD, byte SO_LCK, byte SO_BO_LCK,
  	  		String SO_MMO, String SO_TRNSCTN_STTS, Integer SO_AD_BRNCH, Integer SO_AD_CMPNY)
  	  		throws CreateException { }
}
