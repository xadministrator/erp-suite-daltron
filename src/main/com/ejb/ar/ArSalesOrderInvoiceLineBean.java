package com.ejb.ar;

import java.util.Collection;

import javax.ejb.CreateException;
import javax.ejb.FinderException;

import com.ejb.inv.LocalInvCosting;
import com.util.AbstractEntityBean;
import com.util.Debug;

/**
 * @ejb:bean name="ArSalesOrderInvoiceLineEJB"
 *           display-name="Sales Order Invoice Line Entity"
 *           type="CMP"
 *           cmp-version="2.x"
 *           view-type="local"
 *           local-jndi-name="omega-ejb/ArSalesOrderInvoiceLineEJB"
 *           schema="ArSalesOrderInvoiceLine"
 *           primkey-field="silCode"
 *
 * @ejb:pk class="java.lang.Integer"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="aruser"
 *                        role-link="aruserlink"
 *
 * @ejb:permission role-name="aruser"
 *
 * @ejb:interface local-class="com.ejb.ar.LocalArSalesOrderInvoiceLine"
 *                local-extends="javax.ejb.EJBLocalObject"
 *
 * @ejb:home local-class="com.ejb.ar.LocalArSalesOrderInvoiceLineHome"
 *           local-extends="javax.ejb.EJBLocalHome"
 *
 * @ejb:finder signature = "Collection findByInvCode(java.lang.Integer INV_CODE, java.lang.Integer ILI_AD_CMPNY)"
 * 				query = "SELECT OBJECT(sil) FROM ArSalesOrderInvoiceLine sil WHERE sil.arInvoice.invCode=?1 AND sil.silAdCompany=?2"
 *
 * @ejb:finder signature="LocalArSalesOrderInvoiceLine findBySolCodeAndInvCode(java.lang.Integer SIL_CODE, java.lang.Integer INV_CODE, java.lang.Integer SO_AD_CMPNY)"
 *           query="SELECT OBJECT(sil) FROM ArSalesOrderInvoiceLine sil  WHERE sil.arSalesOrderLine.solCode = ?1 AND sil.arInvoice.invCode = ?2 AND sil.silAdCompany = ?3"
 *
 *
 * @ejb:finder signature="Collection findUnpostedInvcByIiNameAndLocNameAndLessEqualDateAndInvAdBranch(java.lang.String II_NM, java.lang.String LOC_NM, java.util.Date INV_DT, java.lang.Integer INV_AD_BRNCH, java.lang.Integer SIL_AD_CMPNY)"
 * 			   query="SELECT OBJECT(sil) FROM ArSalesOrderInvoiceLine sil WHERE sil.arInvoice.invPosted = 0 AND sil.arInvoice.invVoid = 0 AND sil.arInvoice.invCreditMemo = 0 AND sil.arSalesOrderLine.invItemLocation.invItem.iiName = ?1 AND sil.arSalesOrderLine.invItemLocation.invLocation.locName = ?2 AND sil.arInvoice.invDate <= ?3 AND sil.arInvoice.invAdBranch = ?4 AND sil.silAdCompany = ?5"
 *
 * @ejb:finder signature="Collection findSolBySoCodeAndInvAdBranch(java.lang.Integer SO_CODE, java.lang.Integer INV_AD_BRNCH, java.lang.Integer SIL_AD_CMPNY)"
 * 			   query="SELECT OBJECT(sil) FROM ArSalesOrderInvoiceLine sil WHERE sil.arInvoice.invVoid = 0 AND sil.arInvoice.invCreditMemo = 0 AND sil.arSalesOrderLine.arSalesOrder.soCode = ?1 AND sil.arInvoice.invAdBranch = ?2 AND sil.silAdCompany = ?3"
 *
 * @ejb:finder signature="Collection findCommittedQtyByIiNameAndLocNameAndSoAdBranch(java.lang.String II_NM, java.lang.String LOC_NM, java.util.Date INV_DT_FRM, java.util.Date INV_DT_TO, java.lang.Integer SO_AD_BRNCH, java.lang.Integer SOL_AD_CMPNY)"
 * 			   query="SELECT OBJECT(sil) FROM ArSalesOrderInvoiceLine sil WHERE sil.arSalesOrderLine.invItemLocation.invItem.iiName = ?1 AND sil.arSalesOrderLine.invItemLocation.invLocation.locName = ?2 AND sil.arInvoice.invDate >= ?3 AND sil.arInvoice.invDate <= ?4 AND sil.arInvoice.invAdBranch = ?5 AND sil.silAdCompany = ?6"
 *
 * @ejb:finder signature="Collection findUnpostedSoInvByLocNameAndAdBranch(java.lang.String LOC_NM, java.lang.Integer INV_AD_BRNCH, java.lang.Integer SIL_AD_CMPNY)"
 * 			   query="SELECT OBJECT(sil) FROM ArSalesOrderInvoiceLine sil WHERE sil.arInvoice.invPosted = 0 AND sil.arInvoice.invVoid = 0 AND sil.arSalesOrderLine.invItemLocation.invLocation.locName = ?1 AND sil.arInvoice.invAdBranch = ?2 AND sil.silAdCompany = ?3"
 *
 *  @ejb:finder signature="Collection findSoInvByIiCodeAndLocNameAndAdBranch(java.lang.Integer II_CODE, java.lang.String LOC_NM, java.lang.Integer INV_AD_BRNCH, java.lang.Integer SIL_AD_CMPNY)"
 * 			   query="SELECT OBJECT(sil) FROM ArSalesOrderInvoiceLine sil WHERE sil.arInvoice.invVoid = 0 AND sil.arSalesOrderLine.invItemLocation.invItem.iiCode=?1 AND sil.arSalesOrderLine.invItemLocation.invLocation.locName = ?2 AND sil.arInvoice.invAdBranch = ?3 AND sil.silAdCompany = ?4"
 *
 *
 * @ejb:select signature="Collection ejbSelectGeneric(java.lang.String jbossQl, java.lang.Object[] args)"
 *             result-type-mapping="Local"
 *             method-intf="LocalHome"
 *             query="SELECT OBJECT(sil) FROM ArSalesOrderInvoiceLine sil"
 *
 * @jboss:query signature="Collection ejbSelectGeneric(java.lang.String jbossQl, java.lang.Object[] args)"
 *                         dynamic="true"
 *
 * @ejb:value-object match="*"
 *             name="ArSalesOrderInvoiceLine"
 *
 * @jboss:persistence table-name="AR_SLS_ORDR_INVC_LN"
 *
 * @jboss:entity-command name="mysql-get-generated-keys"
 *
 */

public abstract class ArSalesOrderInvoiceLineBean extends AbstractEntityBean {

    //  PERSISTENT METHODS

    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     * @ejb:pk-field
     *
     * @jboss:column-name name="SIL_CODE"
     *
     * @jboss:persistence auto-increment="true"
     **/
    public abstract Integer getSilCode();
    public abstract void setSilCode(Integer SIL_CODE);

    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="SIL_QTY_DLVRD"
     **/
    public abstract double getSilQuantityDelivered();
    /**
     * @ejb:interface-method view-type="local"
     **/
    public abstract void setSilQuantityDelivered(double SIL_QTY_DLVRD);

    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="SIL_AMNT"
     **/
    public abstract double getSilAmount();
    /**
     * @ejb:interface-method view-type="local"
     **/
    public abstract void setSilAmount(double SIL_AMNT);

    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="SIL_TX_AMNT"
     **/
    public abstract double getSilTaxAmount();
    /**
     * @ejb:interface-method view-type="local"
     **/
    public abstract void setSilTaxAmount(double SIL_TX_AMNT);

    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="SIL_DSCNT_1"
     **/

    public abstract double getSilDiscount1();
    /**
     * @ejb:interface-method view-type="local"
     **/
    public abstract void setSilDiscount1(double SIL_DSCNT_1);

    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="SIL_DSCNT_2"
     **/
    public abstract double getSilDiscount2();
    /**
     * @ejb:interface-method view-type="local"
     **/
    public abstract void setSilDiscount2(double SIL_DSCNT_2);

    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="SIL_DSCNT_3"
     **/
    public abstract double getSilDiscount3();
    /**
     * @ejb:interface-method view-type="local"
     **/
    public abstract void setSilDiscount3(double SIL_DSCNT_3);

    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="SIL_DSCNT_4"
     **/
    public abstract double getSilDiscount4();
    /**
     * @ejb:interface-method view-type="local"
     **/
    public abstract void setSilDiscount4(double SIL_DSCNT_4);

    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="SIL_TTL_DSCNT"
     **/
    public abstract double getSilTotalDiscount();
    /**
     * @ejb:interface-method view-type="local"
     **/
    public abstract void setSilTotalDiscount(double SIL_TTL_DSCNT);


    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="SIL_IMEI"
     **/
    public abstract String getSilImei();
    /**
     * @ejb:interface-method view-type="local"
     **/
    public abstract void setSilImei(String SIL_IMEI);

    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="SIL_TX"
     **/
    public abstract byte getSilTax();
    /**
     * @ejb:interface-method view-type="local"
     **/
    public abstract void setSilTax(byte SIL_TX);


    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="SIL_AD_CMPNY"
     **/
    public abstract Integer getSilAdCompany();
    /**
     * @ejb:interface-method view-type="local"
     **/
    public abstract void setSilAdCompany(Integer SIL_AD_CMPNY);

    // RELATIONSHIP FIELDS

    /**
     * @ejb:interface-method view-type="local"
     * @ejb:relation name="salesorderline-salesorderinvoiceline"
     *               role-name="salesorderinvoiceline-has-one-salesorderline"
     *               cascade-delete="yes"
     *
     * @jboss:relation related-pk-field="solCode"
     *                 fk-column="AR_SALES_ORDER_LINE"
     */
    public abstract LocalArSalesOrderLine getArSalesOrderLine();
    /**
     * @ejb:interface-method view-type="local"
     **/
    public abstract void setArSalesOrderLine(LocalArSalesOrderLine arSalesOrderLine);

    /**
     * @ejb:interface-method view-type="local"
     * @ejb:relation name="invoice-salesorderinvoiceline"
     *               role-name="salesorderinvoiceline-has-one-invoice"
     *               cascade-delete="yes"
     *
     * @jboss:relation related-pk-field="invCode"
     *                 fk-column="AR_INVOICE"
     */
    public abstract LocalArInvoice getArInvoice();
    /**
     * @ejb:interface-method view-type="local"
     **/
    public abstract void setArInvoice(LocalArInvoice arInvoice);

    /**
     * @ejb:interface-method view-type="local"
	 * @ejb:relation name="salesorderinvoiceline-costing"
     *               role-name="salesorderinvoiceline-has-one-costing"
     */
    public abstract LocalInvCosting getInvCosting();
    /**
     * @ejb:interface-method view-type="local"
     **/
    public abstract void setInvCosting(LocalInvCosting invCosting);



    /**
     * @ejb:interface-method view-type="local"
     * @ejb:relation name="salesorderinvoiceline-tags"
     *               role-name="salesorderinvoiceline-has-many-tags"
     *
     */
    public abstract Collection getInvTags();
    public abstract void setInvTags(Collection invTags);


    /**
     * @jboss:dynamic-ql
     */
     public abstract Collection ejbSelectGeneric(java.lang.String jbossQl, java.lang.Object[] args)
        throws FinderException;

     // BUSINESS METHODS

     /**
      * @ejb:home-method view-type="local"
      */
      public Collection ejbHomeGetSalesOrderInvoiceLineByCriteria(java.lang.String jbossQl, java.lang.Object[] args)
         throws FinderException {

         return ejbSelectGeneric(jbossQl, args);
      }

      // ENTITY METHODS

      /**
  	   * @ejb:create-method view-type="local"
  	   **/
      public Integer ejbCreate(Integer SIL_CODE, double SIL_QTY_DLVRD, double SIL_AMNT, double SIL_TX_AMNT,
      	double SIL_DSCNT_1, double SIL_DSCNT_2, double SIL_DSCNT_3, double SIL_DSCNT_4, double TTL_SIL_DSCNT, String SIL_IMEI, byte SIL_TX,
            Integer SIL_AD_CMPNY)
      		throws CreateException {

          Debug.print("ArSalesOrderInvoiceLineBean ejbCreate");

          setSilCode(SIL_CODE);
          setSilQuantityDelivered(SIL_QTY_DLVRD);
          setSilAmount(SIL_AMNT);
          setSilTaxAmount(SIL_TX_AMNT);
          setSilDiscount1(SIL_DSCNT_1);
          setSilDiscount2(SIL_DSCNT_2);
          setSilDiscount3(SIL_DSCNT_3);
          setSilDiscount4(SIL_DSCNT_4);
          setSilTotalDiscount(TTL_SIL_DSCNT);
          setSilImei(SIL_IMEI);
          setSilTax(SIL_TX);
          setSilAdCompany(SIL_AD_CMPNY);

          return null;
      }

      /**
  	   * @ejb:create-method view-type="local"
  	   **/
      public Integer ejbCreate(double SIL_QTY_DLVRD, double SIL_AMNT, double SIL_TX_AMNT, double SIL_DSCNT_1,
      	double SIL_DSCNT_2, double SIL_DSCNT_3, double SIL_DSCNT_4, double TTL_SIL_DSCNT,  String SIL_IMEI,  byte SIL_TX,Integer SIL_AD_CMPNY)
      		throws CreateException {

          Debug.print("ArSalesOrderInvoiceLineBean ejbCreate");

          setSilQuantityDelivered(SIL_QTY_DLVRD);
          setSilAmount(SIL_AMNT);
          setSilTaxAmount(SIL_TX_AMNT);
          setSilDiscount1(SIL_DSCNT_1);
          setSilDiscount2(SIL_DSCNT_2);
          setSilDiscount3(SIL_DSCNT_3);
          setSilDiscount4(SIL_DSCNT_4);
          setSilTotalDiscount(TTL_SIL_DSCNT);
          setSilImei(SIL_IMEI);
          setSilTax(SIL_TX);
          setSilAdCompany(SIL_AD_CMPNY);

          return null;
      }

      public void ejbPostCreate(Integer SIL_CODE, double SIL_QTY_DLVRD, double SIL_AMNT, double SIL_TX_AMNT,
      	double SIL_DSCNT_1, double SIL_DSCNT_2, double SIL_DSCNT_3, double SIL_DSCNT_4, double TTL_SIL_DSCNT, String SIL_IMEI, byte SIL_TX,
        Integer SIL_AD_CMPNY)
		throws CreateException { }

      public void ejbPostCreate(double SIL_QTY_DLVRD, double SIL_AMNT, double SIL_TX_AMNT, double SIL_DSCNT_1,
      	double SIL_DSCNT_2, double SIL_DSCNT_3, double SIL_DSCNT_4, double TTL_SIL_DSCNT, String SIL_IMEI, byte SIL_TX,  Integer SIL_AD_CMPNY)
		throws CreateException { }

}
