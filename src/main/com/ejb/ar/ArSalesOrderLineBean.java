package com.ejb.ar;

import java.util.Collection;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;

import com.ejb.inv.LocalInvItemLocation;
import com.ejb.inv.LocalInvUnitOfMeasure;
import com.ejb.inv.LocalInvBuildOrderLine;
import com.util.AbstractEntityBean;
import com.util.Debug;

 /**
  * @ejb:bean name="ArSalesOrderLineEJB"
  *           display-name="Sales Order Line Entity"
  *           type="CMP"
  *           cmp-version="2.x"
  *           view-type="local"
  *           local-jndi-name="omega-ejb/ArSalesOrderLineEJB"
  *           schema="ArSalesOrderLine"
  *           primkey-field="solCode"
  *
  * @ejb:pk class="java.lang.Integer"
  *
  * @ejb:transaction type="Required"
  *
  * @ejb:security-role-ref role-name="aruser"
  *                        role-link="aruserlink"
  *
  * @ejb:permission role-name="aruser"
  *
  * @ejb:interface local-class="com.ejb.ar.LocalArSalesOrderLine"
  *                local-extends="javax.ejb.EJBLocalObject"
  *
  * @ejb:home local-class="com.ejb.ar.LocalArSalesOrderLineHome"
  *           local-extends="javax.ejb.EJBLocalHome"
  *
  * @jboss:query signature="Collection ejbSelectGeneric(java.lang.String jbossQl, java.lang.Object[] args)"
  *                         dynamic="true"
  *
  * @ejb:finder signature="Collection findOpenSolAll(java.lang.Integer SOL_AD_CMPNY)"
  * 			 query="SELECT OBJECT(sol) FROM ArSalesOrderLine sol WHERE sol.solAdCompany = ?1"
  *
  * @ejb:finder signature="Collection findBySalesOrderCode(java.lang.Integer SO_CODE, java.lang.Integer SOL_AD_CMPNY)"
  * 			 query="SELECT OBJECT(sol) FROM ArSalesOrderLine sol WHERE sol.arSalesOrder.soCode = ?1 AND sol.solAdCompany = ?2"
  *
  *
  * @ejb:finder signature="Collection findCommittedQtyByIiNameAndLocNameAndSoAdBranch(java.lang.String II_NM, java.lang.String LOC_NM, java.util.Date SO_DT_FRM, java.util.Date SO_DT_TO, java.lang.Integer SO_AD_BRNCH, java.lang.Integer SOL_AD_CMPNY)"
  * 			   query="SELECT OBJECT(sol) FROM ArSalesOrderLine sol WHERE sol.invItemLocation.invItem.iiName = ?1 AND sol.invItemLocation.invLocation.locName = ?2 AND sol.arSalesOrder.soDate >= ?3 AND sol.arSalesOrder.soDate <= ?4 AND sol.arSalesOrder.soAdBranch = ?5 AND sol.solAdCompany = ?6"
  *
  * @ejb:finder signature="Collection findCommittedQtyByIiNameAndLocNameAndWithoutDateAndSoAdBranch(java.lang.String II_NM, java.lang.String LOC_NM, java.lang.Integer SO_AD_BRNCH, java.lang.Integer SOL_AD_CMPNY)"
  * 			   query="SELECT OBJECT(sol) FROM ArSalesOrderLine sol WHERE sol.invItemLocation.invItem.iiName = ?1 AND sol.invItemLocation.invLocation.locName = ?2 AND sol.arSalesOrder.soAdBranch = ?3 AND sol.solAdCompany = ?4"
  *
  * @ejb:value-object match="*"
  *             name="ArSalesOrderLine"
  *
  * @jboss:persistence table-name="AR_SLS_ORDR_LN"
  *
  * @jboss:entity-command name="mysql-get-generated-keys"
  *
  */

public abstract class ArSalesOrderLineBean extends AbstractEntityBean{

    // PERSISTENT METHODS

    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     * @ejb:pk-field
     *
     * @jboss:column-name name="SOL_CODE"
     *
     * @jboss:persistence auto-increment="true"
     **/
    public abstract Integer getSolCode();
    public abstract void setSolCode(Integer SOL_CODE);

    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="SOL_LN"
     **/
    public abstract short getSolLine();
    /**
     * @ejb:interface-method view-type="local"
     **/
    public abstract void setSolLine(short SOL_LN);

    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="SOL_IDESC"
     **/
    public abstract String getSolLineIDesc();
    /**
     * @ejb:interface-method view-type="local"
     **/
    public abstract void setSolLineIDesc(String SOL_IDESC);


    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="SOL_QTY"
     **/
    public abstract double getSolQuantity();
    /**
     * @ejb:interface-method view-type="local"
     **/
    public abstract void setSolQuantity(double SOL_QTY);

    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="SOL_UNT_PRC"
     **/
    public abstract double getSolUnitPrice();
    /**
     * @ejb:interface-method view-type="local"
     **/
    public abstract void setSolUnitPrice(double SOL_UNT_PRC);

    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="SOL_AMNT"
     **/
    public abstract double getSolAmount();
    /**
     * @ejb:interface-method view-type="local"
     **/
    public abstract void setSolAmount(double SOL_AMNT);

    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="SOL_DSCNT_1"
     **/

    public abstract double getSolDiscount1();
    /**
     * @ejb:interface-method view-type="local"
     **/
    public abstract void setSolDiscount1(double SOL_DSCNT_1);

    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="SOL_DSCNT_2"
     **/
    public abstract double getSolDiscount2();
    /**
     * @ejb:interface-method view-type="local"
     **/
    public abstract void setSolDiscount2(double SOL_DSCNT_2);

    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="SOL_DSCNT_3"
     **/
    public abstract double getSolDiscount3();
    /**
     * @ejb:interface-method view-type="local"
     **/
    public abstract void setSolDiscount3(double SOL_DSCNT_3);

    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="SOL_DSCNT_4"
     **/
    public abstract double getSolDiscount4();
    /**
     * @ejb:interface-method view-type="local"
     **/
    public abstract void setSolDiscount4(double SOL_DSCNT_4);

    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="SOL_TTL_DSCNT"
     **/
    public abstract double getSolTotalDiscount();
    /**
     * @ejb:interface-method view-type="local"
     **/
    public abstract void setSolTotalDiscount(double SOL_TTL_DSCNT);

    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="SOL_RQST_QTY"
     **/
    public abstract double getSolRequestQuantity();
    /**
     * @ejb:interface-method view-type="local"
     **/
    public abstract void setSolRequestQuantity(double SOL_RQST_QTY);

    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="SOL_MISC"
     **/
    public abstract String getSolMisc();
    /**
     * @ejb:interface-method view-type="local"
     **/
    public abstract void setSolMisc(String SOL_MISC);

    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="SOL_TX"
     **/
    public abstract byte getSolTax();
    /**
     * @ejb:interface-method view-type="local"
     **/
    public abstract void setSolTax(byte SOL_TX);


    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="SOL_AD_CMPNY"
     **/
    public abstract Integer getSolAdCompany();
    /**
     * @ejb:interface-method view-type="local"
     **/
    public abstract void setSolAdCompany(Integer SOL_AD_CMPNY);

    // RELATIONSHIP FIELDS

    /**
     * @ejb:interface-method view-type="local"
     * @ejb:relation name="salesorder-salesorderline"
     *               role-name="salesorderline-has-one-salesorder"
     *               cascade-delete="yes"
     *
     * @jboss:relation related-pk-field="soCode"
     *                 fk-column="AR_SALES_ORDER"
     */
    public abstract LocalArSalesOrder getArSalesOrder();
    public abstract void setArSalesOrder(LocalArSalesOrder arSalesOrder);

    /**
     * @ejb:interface-method view-type="local"
     * @ejb:relation name="unitofmeasure-salesorderline"
     *               role-name="salesorderline-has-one-unitofmeasure"
     *               cascade-delete="no"
     *
     * @jboss:relation related-pk-field="uomCode"
     *                 fk-column="INV_UNIT_OF_MEASURE"
     */
    public abstract LocalInvUnitOfMeasure getInvUnitOfMeasure();
    public abstract void setInvUnitOfMeasure(LocalInvUnitOfMeasure invUnitOfMeasure);

    /**
     * @ejb:interface-method view-type="local"
     * @ejb:relation name="itemlocation-salesorderline"
     *               role-name="salesorderline-has-one-itemlocation"
     *               cascade-delete="no"
     *
     * @jboss:relation related-pk-field="ilCode"
     *                 fk-column="INV_ITEM_LOCATION"
     */
    public abstract LocalInvItemLocation getInvItemLocation();
    public abstract void setInvItemLocation(LocalInvItemLocation invItemLocation);

    /**
     * @ejb:interface-method view-type="local"
     * @ejb:relation name="salesorderline-salesorderinvoiceline"
     *               role-name="salesorderline-has-many-salesorderinvoiceline"
     */
    public abstract Collection getArSalesOrderInvoiceLines();
    public abstract void setArSalesOrderInvoiceLines(Collection arSalesOrderInvoiceLines);

    /**
     * @ejb:interface-method view-type="local"
     * @ejb:relation name="salesorderline-buildorderlines"
     *               role-name="salesorderline-has-many-buildorderlines"
     */
    public abstract Collection getInvBuildOrderLines();
    public abstract void setInvBuildOrderLines(Collection invBuildOrderLines);


    /**
    * @ejb:interface-method view-type="local"
    * @ejb:relation name="salesorderline-tags"
    *               role-name="salesorderline-has-many-tags"
    *
    */
   public abstract Collection getInvTags();
   public abstract void setInvTags(Collection invTags);

    /**
     * @jboss:dynamic-ql
     */
     public abstract Collection ejbSelectGeneric(java.lang.String jbossQl, java.lang.Object[] args)
        throws FinderException;

    // BUSINESS METHODS

     /**
      * @ejb:home-method view-type="local"
      */
      public Collection ejbHomeGetSalesOrderLineByCriteria(java.lang.String jbossQl, java.lang.Object[] args)
         throws FinderException {

         return ejbSelectGeneric(jbossQl, args);
      }

      /**
 	    * @ejb:interface-method view-type="local"
 	    **/
 	   public void addArSalesOrderInvoiceLine(LocalArSalesOrderInvoiceLine arSalesOrderInvoiceLine) {

	        Debug.print("ArSalesOrderLineBean addArSalesOrderInvoiceLine");

	        try {

	  	        Collection arSalesOrderInvoiceLines = getArSalesOrderInvoiceLines();
	  	        arSalesOrderInvoiceLines.add(arSalesOrderInvoiceLine);

	        } catch (Exception ex) {

	            throw new EJBException(ex.getMessage());

	        }

 	   }

      /**
       * @ejb:interface-method view-type="local"
       **/
       public void dropArSalesOrderInvoiceLine(LocalArSalesOrderInvoiceLine arSalesOrderInvoiceLine) {

 		  Debug.print("ArSalesOrderLineBean dropArSalesOrderInvoiceLine");

 		  try {

 		     Collection arSalesOrderInvoiceLines = getArSalesOrderInvoiceLines();
 		     arSalesOrderInvoiceLines.remove(arSalesOrderInvoiceLine);

 		  } catch (Exception ex) {

 		     throw new EJBException(ex.getMessage());

 		  }

      }

       /**
 	    * @ejb:interface-method view-type="local"
 	    **/
 	   public void addInvBuildOrderLine(LocalInvBuildOrderLine invBuildOrderLine) {

	        Debug.print("ArSalesOrderLineBean addInvBuildOrderLine");

	        try {

	  	        Collection invBuildOrderLines = getInvBuildOrderLines();
	  	        invBuildOrderLines.add(invBuildOrderLine);

	        } catch (Exception ex) {

	            throw new EJBException(ex.getMessage());

	        }

 	   }

     /**
       * @ejb:interface-method view-type="local"
       **/
       public void dropInvBuildOrderLine(LocalInvBuildOrderLine invBuildOrderLine) {

 		  Debug.print("ArSalesOrderLineBean dropInvBuildOrderLine");

 		  try {

 			 Collection invBuildOrderLines = getInvBuildOrderLines();
 		     invBuildOrderLines.remove(invBuildOrderLine);

 		  } catch (Exception ex) {

 		     throw new EJBException(ex.getMessage());

 		  }

      }

    // ENTITY METHODS

    /**
	 * @ejb:create-method view-type="local"
	 **/
    public Integer ejbCreate(Integer SOL_CODE, short SOL_LN, String SOL_IDESC, double SOL_QTY,
    	double SOL_UNT_PRC, double SOL_AMNT, double SOL_DSCNT_1, double SOL_DSCNT_2, double SOL_DSCNT_3,
		double SOL_DSCNT_4, double TTL_SOL_DSCNT, double SOL_RQST_QTY, String SOL_MISC, byte SOL_TX, Integer SOL_AD_CMPNY)
    	throws CreateException {

        Debug.print("ArSalesOrderLineBean ejbCreate");

        setSolCode(SOL_CODE);
        setSolLine(SOL_LN);
        setSolLineIDesc(SOL_IDESC);
        setSolQuantity(SOL_QTY);
        setSolUnitPrice(SOL_UNT_PRC);
        setSolAmount(SOL_AMNT);
        setSolDiscount1(SOL_DSCNT_1);
        setSolDiscount2(SOL_DSCNT_2);
        setSolDiscount3(SOL_DSCNT_3);
        setSolDiscount4(SOL_DSCNT_4);
        setSolTotalDiscount(TTL_SOL_DSCNT);
        setSolRequestQuantity(SOL_RQST_QTY);
        setSolMisc(SOL_MISC);
        setSolTax(SOL_TX);
        setSolAdCompany(SOL_AD_CMPNY);

        return null;
    }

    /**
	 * @ejb:create-method view-type="local"
	 **/
    public Integer ejbCreate(short SOL_LN, String SOL_IDESC, double SOL_QTY,
    	double SOL_UNT_PRC, double SOL_AMNT, double SOL_DSCNT_1, double SOL_DSCNT_2, double SOL_DSCNT_3,
		double SOL_DSCNT_4, double TTL_SOL_DSCNT, double SOL_RQST_QTY, String SOL_MISC, byte SOL_TX, Integer SOL_AD_CMPNY)
    	throws CreateException {

        Debug.print("ArSalesOrderLineBean ejbCreate");

        setSolLine(SOL_LN);
        setSolLineIDesc(SOL_IDESC);
        setSolQuantity(SOL_QTY);
        setSolUnitPrice(SOL_UNT_PRC);
        setSolAmount(SOL_AMNT);
        setSolDiscount1(SOL_DSCNT_1);
        setSolDiscount2(SOL_DSCNT_2);
        setSolDiscount3(SOL_DSCNT_3);
        setSolDiscount4(SOL_DSCNT_4);
        setSolTotalDiscount(TTL_SOL_DSCNT);
        setSolRequestQuantity(SOL_RQST_QTY);
        setSolMisc(SOL_MISC);
        setSolTax(SOL_TX);
        setSolAdCompany(SOL_AD_CMPNY);

        return null;
    }

    public void ejbPostCreate(Integer SOL_CODE, short SOL_LN, String SOL_IDESC, double SOL_QTY,
    	double SOL_UNT_PRC, double SOL_AMNT, double SOL_DSCNT_1, double SOL_DSCNT_2, double SOL_DSCNT_3,
		double SOL_DSCNT_4, double TTL_SOL_DSCNT, double SOL_RQST_QTY, String SOL_MISC, byte SOL_TX, Integer SOL_AD_CMPNY)
    	throws CreateException { }

    public void ejbPostCreate(short SOL_LN, String SOL_IDESC, double SOL_QTY,
    	double SOL_UNT_PRC, double SOL_AMNT, double SOL_DSCNT_1, double SOL_DSCNT_2, double SOL_DSCNT_3,
		double SOL_DSCNT_4, double TTL_SOL_DSCNT, double SOL_RQST_QTY, String SOL_MISC, byte SOL_TX, Integer SOL_AD_CMPNY)
    	throws CreateException { }
}
