/*
 * com/ejb/ar/LocalArSalespersonBean.java
 *
 * Created on January 31, 2006, 2:00 PM
 */

package com.ejb.ar;

import java.util.Collection;
import java.util.Date;

import javax.ejb.CreateException;
import javax.ejb.EJBException;

import com.ejb.ad.LocalAdBranchSalesperson;
import com.util.AbstractEntityBean;
import com.util.Debug;

/**
 *
 * @author  Franco Antonio R. Roig
 *
 * @ejb:bean name="ArSalespersonEJB"
 *           display-name="Salesperson Entity"
 *           type="CMP"
 *           cmp-version="2.x"
 *           view-type="local"
 *           local-jndi-name="omega-ejb/ArSalespersonEJB"
 *           schema="ArSalesperson"
 *           primkey-field="slpCode"
 *
 * @ejb:pk class="java.lang.Integer"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="aruser"
 *                        role-link="aruserlink"
 *
 * @ejb:permission role-name="aruser"
 *
 * @ejb:interface local-class="com.ejb.ar.LocalArSalesperson"
 *                local-extends="javax.ejb.EJBLocalObject"
 *
 * @ejb:home local-class="com.ejb.ar.LocalArSalespersonHome"
 *           local-extends="javax.ejb.EJBLocalHome"
 *
 * @ejb:finder signature="Collection findSlpAll(java.lang.Integer SLP_AD_CMPNY)"
 *             query="SELECT OBJECT(slp) FROM ArSalesperson slp WHERE slp.slpAdCompany = ?1"
 *
 * @jboss:query signature="Collection findSlpAll(java.lang.Integer SLP_AD_CMPNY)"
 *             query="SELECT OBJECT(slp) FROM ArSalesperson slp WHERE slp.slpAdCompany = ?1 ORDER BY slp.slpSalespersonCode"
 *
 * @ejb:finder signature="LocalArSalesperson findBySlpSalespersonCode(java.lang.String SLP_SLSPRSN_CODE, java.lang.Integer SLP_AD_CMPNY)"
 *             query="SELECT OBJECT(slp) FROM ArSalesperson slp WHERE slp.slpSalespersonCode = ?1 AND slp.slpAdCompany = ?2"
 *
 * @ejb:finder signature="Collection findSlpByBrCode(java.lang.Integer SLP_AD_BRNCH, java.lang.Integer SLP_AD_CMPNY)"
 *             query="SELECT OBJECT(slp) FROM ArSalesperson slp, IN(slp.adBranchSalespersons) bslp WHERE bslp.adBranch.brCode = ?1 AND slp.slpAdCompany = ?2"
 * 
 * @ejb:value-object match="*"
 *             name="ArSalesperson"
 *
 * @jboss:persistence table-name="AR_SLSPRSN"
 *
 * @jboss:entity-command name="mysql-get-generated-keys"
 */

public abstract class ArSalespersonBean extends AbstractEntityBean {
	
	
	// PERSISTENT METHODS
	
	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 * @ejb:pk-field
	 *
	 * @jboss:column-name name="SLP_CODE"
	 *
	 * @jboss:persistence auto-increment="true"
	 **/
	public abstract Integer getSlpCode();
	public abstract void setSlpCode(Integer SLP_CODE);
	
	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"   
	 *
	 * @jboss:column-name name="SLP_SLSPRSN_CODE"
	 **/
	public abstract String getSlpSalespersonCode();
	/**
	 * @ejb:interface-method view-type="local"
	 **/ 
	public abstract void setSlpSalespersonCode(String SLP_SLSPRSN_CODE);
	
	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"   
	 *
	 * @jboss:column-name name="SLP_NM"
	 **/
	public abstract String getSlpName();
	/**
	 * @ejb:interface-method view-type="local"
	 **/ 
	public abstract void setSlpName(String SLP_NM);    

	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="SLP_ENTRY_DT"
	 **/
	public abstract Date getSlpEntryDate();
	/**
	 * @ejb:interface-method view-type="local"
	 **/ 
	public abstract void setSlpEntryDate(Date SLP_ENTRY_DT);
	
	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="SLP_ADDRSS"
	 **/
	public abstract String getSlpAddress();
	/**
	 * @ejb:interface-method view-type="local"
	 **/ 
	public abstract void setSlpAddress(String SLP_ADDRSS);
	
	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="SLP_PHN"
	 **/
	public abstract String getSlpPhone();
	/**
	 * @ejb:interface-method view-type="local"
	 **/ 
	public abstract void setSlpPhone(String SLP_PHN);
		
	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="SLP_MBL_PHN"
	 **/
	public abstract String getSlpMobilePhone();
	/**
	 * @ejb:interface-method view-type="local"
	 **/ 
	public abstract void setSlpMobilePhone(String SLP_MBL_PHN); 
	
	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="SLP_EML"
	 **/
	public abstract String getSlpEmail();
	/**
	 * @ejb:interface-method view-type="local"
	 **/ 
	public abstract void setSlpEmail(String SLP_EML); 
	
	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="SLP_AD_CMPNY"
	 **/
	public abstract Integer getSlpAdCompany();
	/**
	 * @ejb:interface-method view-type="local"
	 **/ 
	public abstract void setSlpAdCompany(Integer SLP_AD_CMPNY);
	
	
	// RELATIONSHIP METHODS
	
    /**
     * @ejb:interface-method view-type="local"
     * @ejb:relation name="salesperson-branchsalesperson"
     *               role-name="salesperson-has-many-branchsalesperson"
     */
    public abstract Collection getAdBranchSalespersons();
    public abstract void setAdBranchSalespersons(Collection adBranchSalespersons); 

	/**
	 * @ejb:interface-method view-type="local"
	 * @ejb:relation name="salesperson-invoices"
	 *               role-name="salesperson-has-many-invoices"
	 * 
	 */
	public abstract Collection getArInvoices();
	public abstract void setArInvoices(Collection arInvoices);    
	
	/**
	 * @ejb:interface-method view-type="local"
	 * @ejb:relation name="salesperson-receipts"
	 *               role-name="salesperson-has-many-receipts"
	 * 
	 */
	public abstract Collection getArReceipts();
	public abstract void setArReceipts(Collection arReceipts);

    /**
     * @ejb:interface-method view-type="local"
     * @ejb:relation name="salesperson-customers"
     *               role-name="salesperson-has-many-customers"
     * 
     */
    public abstract Collection getArCustomers();
    public abstract void setArCustomers(Collection arCustomer);    
    
    /**
     * @ejb:interface-method view-type="local"
     * @ejb:relation name="salesperson-joborders"
     *               role-name="salesperson-has-many-joborders"
     *
     */
    public abstract Collection getArJobOrders();
    public abstract void setArJobOrders(Collection arJobOrders); 
    
    /**
     * @ejb:interface-method view-type="local"
     * @ejb:relation name="salesperson-salesorder"
     *               role-name="salesperson-has-many-salesorders"
     *
     */
    public abstract Collection getArSalesOrders();
    public abstract void setArSalesOrders(Collection arSalesOrders);

    // BUSINESS METHODS
    
    /**
     * @ejb:interface-method view-type="local"
     **/
    public void addAdBranchSalesperson(LocalAdBranchSalesperson adBranchSalesperson) {

        Debug.print("ArSalespersonBean addAdBranchSalesperson");
  
        try {
  	
	        Collection adBranchSalespersons = getAdBranchSalespersons();
	        adBranchSalespersons.add(adBranchSalesperson);
	     
        } catch (Exception ex) {
  	
            throw new EJBException(ex.getMessage());
     
        }
      
   }
	
   /**
    * @ejb:interface-method view-type="local"
    **/ 
    public void dropAdBranchSalesperson(LocalAdBranchSalesperson adBranchSalesperson) {
		
		  Debug.print("ArSalespersonBean dropAdBranchSalesperson");
		  
		  try {
		  	
		     Collection adBranchSalespersons = getAdBranchSalespersons();
		     adBranchSalespersons.remove(adBranchSalesperson);
			     
		  } catch (Exception ex) {
		  	
		     throw new EJBException(ex.getMessage());
		     
		  }
		  
   }

	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public void addArInvoice(LocalArInvoice arInvoice) {
		
		Debug.print("ArSalespersonBean addArInvoice");
		
		try {
			
			Collection arInvoices = getArInvoices();
			arInvoices.add(arInvoice);
			
		} catch (Exception ex) {
			
			throw new EJBException(ex.getMessage());
			
		}   
	}
	
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public void dropArInvoice(LocalArInvoice arInvoice) {
		
		Debug.print("ArSalespersonBean dropArInvoice");
		
		try {
			
			Collection arInvoices = getArInvoices();
			arInvoices.remove(arInvoice);
			
		} catch (Exception ex) {
			
			throw new EJBException(ex.getMessage());
			
		}   
	}
	
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public void addArReceipt(LocalArReceipt arReceipt) {
		
		Debug.print("ArSalespersonBean addArReceipt");
		
		try {
			
			Collection arReceipts = getArReceipts();
			arReceipts.add(arReceipt);
			
		} catch (Exception ex) {
			
			throw new EJBException(ex.getMessage());
			
		}   
	}
	
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public void dropArReceipt(LocalArReceipt arReceipt) {
		
		Debug.print("ArSalespersonBean dropArReceipt");
		
		try {
			
			Collection arReceipts = getArReceipts();
			arReceipts.remove(arReceipt);
			
		} catch (Exception ex) {
			
			throw new EJBException(ex.getMessage());
			
		} 
		
	}

    /**
     * @ejb:interface-method view-type="local"
     **/
    public void addArCustomer(LocalArCustomer arCustomer) {

         Debug.print("ArSalespersonBean addArCustomer");
      
         try {
      	
            Collection arCustomers = getArCustomers();
	        arCustomers.add(arCustomer);
	          
         } catch (Exception ex) {
      	
           throw new EJBException(ex.getMessage());
      
         }   
     }

    /**
     * @ejb:interface-method view-type="local"
     **/
    public void dropArCustomer(LocalArCustomer arCustomer) {

         Debug.print("ArSalespersonBean dropArCustomer");
      
         try {
      	
            Collection arCustomers = getArCustomers();
	        arCustomers.remove(arCustomer);
	      
         } catch (Exception ex) {
      	
           throw new EJBException(ex.getMessage());
         
         }   
     }
    
    /**
     * @ejb:interface-method view-type="local"
     **/
    public void addArSalesOrder(LocalArSalesOrder arSalesOrder) {

         Debug.print("ArSalespersonBean addArSalesOrder");
      
         try {
      	
            Collection arSalesOrders = getArSalesOrders();
            arSalesOrders.add(arSalesOrder);
	          
         } catch (Exception ex) {
      	
           throw new EJBException(ex.getMessage());
      
         }   
     }

    /**
     * @ejb:interface-method view-type="local"
     **/
    public void dropArSalesOrder(LocalArSalesOrder arSalesOrder) {

         Debug.print("ArSalespersonBean dropArSalesOrder");
      
         try {
      	
            Collection arSalesOrders = getArSalesOrders();
            arSalesOrders.remove(arSalesOrder);
	      
         } catch (Exception ex) {
      	
           throw new EJBException(ex.getMessage());
         
         }   
     }

	// ENTITY METHODS
	
	/**
	 * @ejb:create-method view-type="local"
	 **/
	public Integer ejbCreate(Integer SLP_CODE, String SLP_SLSPRSN_CODE, String SLP_NM, Date SLP_ENTRY_DT, 
							 String SLP_ADDRSS, String SLP_PHN, String SLP_MBL_PHN, String SLP_EML,
							 Integer SLP_AD_CMPNY)
	throws CreateException {	
		
		Debug.print("ArSalespersonBean ejbCreate");
		
		setSlpCode(SLP_CODE);
		setSlpSalespersonCode(SLP_SLSPRSN_CODE);
		setSlpName(SLP_NM);
		setSlpEntryDate(SLP_ENTRY_DT);
		setSlpAddress(SLP_ADDRSS);
		setSlpPhone(SLP_PHN);
		setSlpMobilePhone(SLP_MBL_PHN);
		setSlpEmail(SLP_EML);
		setSlpAdCompany(SLP_AD_CMPNY);
		return null;
	}
	
	/**
	 * @ejb:create-method view-type="local"
	 **/
	public Integer ejbCreate(String SLP_SLSPRSN_CODE, String SLP_NM, Date SLP_ENTRY_DT, 
							 String SLP_ADDRSS, String SLP_PHN, String SLP_MBL_PHN, String SLP_EML,
							 Integer SLP_AD_CMPNY)     	
	throws CreateException {	
		
		Debug.print("ArSalespersonBean ejbCreate");
		
		setSlpSalespersonCode(SLP_SLSPRSN_CODE);
		setSlpName(SLP_NM);
		setSlpEntryDate(SLP_ENTRY_DT);
		setSlpAddress(SLP_ADDRSS);
		setSlpPhone(SLP_PHN);
		setSlpMobilePhone(SLP_MBL_PHN);
		setSlpEmail(SLP_EML);
		setSlpAdCompany(SLP_AD_CMPNY);
		return null;
	}
	
	public void ejbPostCreate(Integer SLP_CODE, String SLP_SLSPRSN_CODE, String SLP_NM, Date SLP_ENTRY_DT, 
			 String SLP_ADDRSS, String SLP_PHN, String SLP_MBL_PHN, String SLP_EML,
			 Integer SLP_AD_CMPNY)      
	throws CreateException { }

	public void ejbPostCreate(String SLP_SLSPRSN_CODE, String SLP_NM, Date SLP_ENTRY_DT, 
			 String SLP_ADDRSS, String SLP_PHN, String SLP_MBL_PHN, String SLP_EML,
			 Integer SLP_AD_CMPNY)      
	throws CreateException { }

}
