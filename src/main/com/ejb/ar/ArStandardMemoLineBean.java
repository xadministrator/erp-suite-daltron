/*
 * com/ejb/ar/LocalArStandardMemoLineBean.java
 *
 * Created on March 03, 2004, 3:53 PM
 */

package com.ejb.ar;

import java.util.Collection;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;

import com.ejb.ad.LocalAdBranchStandardMemoLine;
import com.ejb.ar.LocalArStandardMemoLineClass;
import com.util.AbstractEntityBean;
import com.util.Debug;

/**
 *
 * @author  Neil Andrew M. Ajero
 *
 * @ejb:bean name="ArStandardMemoLineEJB"
 *           display-name="StandardMemoLine Entity"
 *           type="CMP"
 *           cmp-version="2.x"
 *           view-type="local"
 *           local-jndi-name="omega-ejb/ArStandardMemoLineEJB"
 *           schema="ArStandardMemoLine"
 *           primkey-field="smlCode"
 *
 * @ejb:pk class="java.lang.Integer"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="aruser"
 *                        role-link="aruserlink"
 *
 * @ejb:permission role-name="aruser"
 *
 * @ejb:interface local-class="com.ejb.ar.LocalArStandardMemoLine"
 *                local-extends="javax.ejb.EJBLocalObject"
 *
 * @ejb:home local-class="com.ejb.ar.LocalArStandardMemoLineHome"
 *           local-extends="javax.ejb.EJBLocalHome"
 *
 * @ejb:finder signature="Collection findEnabledSmlAll(java.lang.Integer SML_AD_BRNCH, java.lang.Integer SML_AD_CMPNY)"
 *             query="SELECT OBJECT(sml) FROM ArStandardMemoLine sml, IN(sml.adBranchStandardMemoLines)bsml WHERE sml.smlEnable = 1 AND bsml.adBranch.brCode = ?1 AND sml.smlAdCompany = ?2"
 * 
 * @jboss:query signature="Collection findEnabledSmlAll(java.lang.Integer SML_AD_BRNCH, java.lang.Integer SML_AD_CMPNY)"
 *             query="SELECT OBJECT(sml) FROM ArStandardMemoLine sml, IN(sml.adBranchStandardMemoLines)bsml WHERE sml.smlEnable = 1 AND bsml.adBranch.brCode = ?1 AND sml.smlAdCompany = ?2 ORDER BY sml.smlName"
 *
 * @ejb:finder signature="LocalArStandardMemoLine findBySmlName(java.lang.String SML_NM, java.lang.Integer SML_AD_CMPNY)"
 *             query="SELECT OBJECT(sml) FROM ArStandardMemoLine sml WHERE sml.smlName = ?1 AND sml.smlAdCompany = ?2"
 *
 * @ejb:finder signature="Collection findSmlAll(java.lang.Integer SML_AD_CMPNY)"
 *             query="SELECT OBJECT(sml) FROM ArStandardMemoLine sml WHERE sml.smlAdCompany = ?1"
 *
 * @ejb:finder signature="Collection findSmlBySmlNewAndUpdated(java.lang.Integer BR_CODE, java.lang.Integer AD_CMPNY, char NEW, char UPDATED, char DOWNLOADED_UPDATED)"
 * 			   query="SELECT DISTINCT OBJECT(sml) FROM ArStandardMemoLine sml, IN(sml.adBranchStandardMemoLines) bsml  WHERE (bsml.bsmlStandardMemoLineDownloadStatus = ?3 OR bsml.bsmlStandardMemoLineDownloadStatus = ?4 OR bsml.bsmlStandardMemoLineDownloadStatus = ?5) AND bsml.adBranch.brCode = ?1 AND bsml.bsmlAdCompany = ?2"
 * 
 * @jboss:query signature="Collection findSmlAll(java.lang.Integer SML_AD_CMPNY)"
 *             query="SELECT OBJECT(sml) FROM ArStandardMemoLine sml WHERE sml.smlAdCompany = ?1 ORDER BY sml.smlName"
 *
 * @ejb:finder signature="LocalArStandardMemoLine findBySmlNameAndBrCode(java.lang.String SML_NM, java.lang.Integer BR_CODE, java.lang.Integer SML_AD_CMPNY)"
 *			   query="SELECT OBJECT(sml) FROM ArStandardMemoLine sml, IN(sml.adBranchStandardMemoLines) bsml WHERE sml.smlName = ?1 AND bsml.adBranch.brCode = ?2 AND sml.smlAdCompany = ?3"
 *
 * @ejb:select signature="Collection ejbSelectGeneric(java.lang.String jbossQl, java.lang.Object[] args)"
 *             result-type-mapping="Local"
 *             method-intf="LocalHome"
 *             query="SELECT OBJECT(sml) FROM ArStandardMemoLine sml"
 *
 * @jboss:query signature="Collection ejbSelectGeneric(java.lang.String jbossQl, java.lang.Object[] args)"
 *                         dynamic="true"
 *
 * @ejb:value-object match="*"
 *             name="ArStandardMemoLine"
 *
 * @jboss:persistence table-name="AR_STNDRD_MMO_LN"
 *
 * @jboss:entity-command name="mysql-get-generated-keys"
 */

public abstract class ArStandardMemoLineBean extends AbstractEntityBean {
	
	
	// PERSITCENT METHODS
	
	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 * @ejb:pk-field
	 *
	 * @jboss:column-name name="SML_CODE"
	 *
	 * @jboss:persistence auto-increment="true"
	 **/
	public abstract Integer getSmlCode();
	public abstract void setSmlCode(Integer SML_CODE);
	
	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"   
	 *
	 * @jboss:column-name name="SML_TYP"
	 **/
	public abstract String getSmlType();
	/**
	 * @ejb:interface-method view-type="local"
	 **/ 
	public abstract void setSmlType(String SML_TYP);
	
	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"   
	 *
	 * @jboss:column-name name="SML_NM"
	 **/
	public abstract String getSmlName();
	/**
	 * @ejb:interface-method view-type="local"
	 **/ 
	public abstract void setSmlName(String SML_NM);    
	
	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"   
	 *
	 * @jboss:column-name name="SML_DESC"
	 **/
	public abstract String getSmlDescription();
	/**
	 * @ejb:interface-method view-type="local"
	 **/ 
	public abstract void setSmlDescription(String SML_DESC);
	
	
	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"   
	 *							
	 * @jboss:column-name name="SML_WP_PRDCT_ID"
	 **/
	public abstract String getSmlWordPressProductID();
	/**
	 * @ejb:interface-method view-type="local"
	 **/ 
	public abstract void setSmlWordPressProductID(String SML_WP_PRDCT_ID);
	
	
	
	
	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"   
	 *
	 * @jboss:column-name name="SML_UNT_PRC"
	 **/
	public abstract double getSmlUnitPrice();
	/**
	 * @ejb:interface-method view-type="local"
	 **/ 
	public abstract void setSmlUnitPrice(double SML_UNT_PRC);
	
	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"   
	 *
	 * @jboss:column-name name="SML_TX"
	 **/
	public abstract byte getSmlTax();
	/**
	 * @ejb:interface-method view-type="local"
	 **/ 
	public abstract void setSmlTax(byte SML_TX);
	
	
	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"   
	 *
	 * @jboss:column-name name="SML_ENBL"
	 **/
	
	public abstract byte getSmlEnable();
	/**
	 * @ejb:interface-method view-type="local"
	 **/ 
	public abstract void setSmlEnable(byte SML_ENBL);
	
	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"   
	 *
	 * @jboss:column-name name="SML_SBJCT_TO_CMMSSN"
	 **/
	public abstract byte getSmlSubjectToCommission();
	/**
	 * @ejb:interface-method view-type="local"
	 **/ 
	public abstract void setSmlSubjectToCommission(byte SML_SBJCT_TO_CMMSSN);
	
	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"   
	 *
	 * @jboss:column-name name="SML_INTRM_ACCNT"
	 **/
	public abstract Integer getSmlInterimAccount();
	/**
	 * @ejb:interface-method view-type="local"
	 **/ 
	public abstract void setSmlInterimAccount(Integer SML_INTRM_ACCNT);

	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"   
	 *
	 * @jboss:column-name name="SML_UNT_OF_MSR"
	 **/
	public abstract String getSmlUnitOfMeasure();
	/**
	 * @ejb:interface-method view-type="local"
	 **/ 
	public abstract void setSmlUnitOfMeasure(String SML_UNT_PRC);
	
	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="SML_GL_COA_RCVBL_ACCNT"
	 **/
	public abstract Integer getSmlGlCoaReceivableAccount();
	/**
	 * @ejb:interface-method view-type="local"
	 **/ 
	public abstract void setSmlGlCoaReceivableAccount(Integer SML_GL_COA_RCVBL_ACCNT); 
				
	
	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="SML_GL_COA_RVNUE_ACCNT"
	 **/
	public abstract Integer getSmlGlCoaRevenueAccount();
	/**
	 * @ejb:interface-method view-type="local"
	 **/ 
	public abstract void setSmlGlCoaRevenueAccount(Integer SML_GL_COA_RVNUE_ACCNT);
	
	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"   
	 *
	 * @jboss:column-name name="SML_AD_CMPNY"
	 **/
	public abstract Integer getSmlAdCompany();
	/**
	 * @ejb:interface-method view-type="local"
	 **/ 
	public abstract void setSmlAdCompany(Integer SML_AD_CMPNY);
	
	
	// RELATIONSHIP METHODS
	
	/**
	 * @ejb:interface-method view-type="local"
	 * @ejb:relation name="chartofaccount-standardmemolines"
	 *               role-name="standardmemoline-has-one-chartofaccount"
	 * 
	 * @jboss:relation related-pk-field="coaCode"
	 *                 fk-column="GL_CHART_OF_ACCOUNT"
	 */
	public abstract com.ejb.gl.LocalGlChartOfAccount getGlChartOfAccount();
	public abstract void setGlChartOfAccount(com.ejb.gl.LocalGlChartOfAccount glChartOfAccount);
	
	public abstract void setArInvoiceLines(Collection arInvoiceLines); 
	
	/**
	 * @ejb:interface-method view-type="local"
	 * @ejb:relation name="standardmemoline-branchstandardmemoline"
	 *               role-name="standardmemoline-has-many-branchstandardmemoline"
	 */
	public abstract Collection getAdBranchStandardMemoLines();
	/**
	 * @ejb:interface-method view-type="local"
	 * @ejb:relation name="standardmemoline-invoicelines"
	 *               role-name="standardmemoline-has-many-invoicelines"
	 */
	public abstract Collection getArInvoiceLines();
	public abstract void setAdBranchStandardMemoLines(Collection adBranchStandardMemoLine);
        
        /**
	 * @ejb:interface-method view-type="local"
	 * @ejb:relation name="standardmemoline-standardmemolineclasses"
	 *               role-name="standardmemoline-has-many-standardmemolineclasses"
	 */
	public abstract Collection getArStandardMemoLineClasses();
	public abstract void setArStandardMemoLineClasses(Collection arStandardMemoLineClasses);
	
	
	/**
	 * @jboss:dynamic-ql
	 */
	public abstract Collection ejbSelectGeneric(java.lang.String jbossQl, java.lang.Object[] args)
	throws FinderException;
	
	
	
	// BUSINESS METHODS
	
	/**
	 * @ejb:home-method view-type="local"
	 */
	public Collection ejbHomeGetSmlByCriteria(java.lang.String jbossQl, java.lang.Object[] args)
	throws FinderException {
		
		return ejbSelectGeneric(jbossQl, args);
	}
	
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public void addArInvoiceLine(LocalArInvoiceLine arInvoiceLine) {
		
		Debug.print("ArStandardMemoLineBean addArInvoiceLine");
		
		try {
			
			Collection arInvoiceLines = getArInvoiceLines();
			arInvoiceLines.add(arInvoiceLine);
			
		} catch (Exception ex) {
			
			throw new EJBException(ex.getMessage());
			
		}   
	}
	
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public void dropArInvoiceLine(LocalArInvoiceLine arInvoiceLine) {
		
		Debug.print("ArStandardMemoLineBean dropArInvoiceLine");
		
		try {
			
			Collection arInvoiceLines = getArInvoiceLines();
			arInvoiceLines.remove(arInvoiceLine);
			
		} catch (Exception ex) {
			
			throw new EJBException(ex.getMessage());
			
		}   
	}
	
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public void addAdBranchStandardMemoLine(LocalAdBranchStandardMemoLine adBranchStandardMemoLine) {
		
		Debug.print("ArStandardMemoLineBean addAdBranchStandardMemoLine");
		
		try {
			
			Collection adBranchStandardMemoLines = getAdBranchStandardMemoLines();
			adBranchStandardMemoLines.add(adBranchStandardMemoLine);
			
		} catch (Exception ex) {
			
			throw new EJBException(ex.getMessage());
			
		}   
	}
	
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public void dropAdBranchStandardMemoLine(LocalAdBranchStandardMemoLine adBranchStandardMemoLine) {
		
		Debug.print("ArStandardMemoLineBean dropAdBranchStandardMemoLine");
		
		try {
			
			Collection adBranchStandardMemoLines = getAdBranchStandardMemoLines();
			adBranchStandardMemoLines.remove(adBranchStandardMemoLine);
			
		} catch (Exception ex) {
			
			throw new EJBException(ex.getMessage());
			
		}   
	}    
        
        
        /**
	 * @ejb:interface-method view-type="local"
	 **/
	public void addArStandardMemoLineClass(LocalArStandardMemoLineClass arStandardMemoLineClass) {
		
		Debug.print("ArStandardMemoLineBean addArStandardMemoLineClass");
		
		try {
			
			Collection arStandardMemoLineClasses = getArStandardMemoLineClasses();
			arStandardMemoLineClasses.add(arStandardMemoLineClass);
			
		} catch (Exception ex) {
			
			throw new EJBException(ex.getMessage());
			
		}   
	}
	
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public void dropArStandardMemoLineClass(LocalArStandardMemoLineClass arStandardMemoLineClass) {
		
		Debug.print("ArStandardMemoLineBean dropArStandardMemoLineClass");
		
		try {
			
			Collection arStandardMemoLineClasses = getArStandardMemoLineClasses();
			arStandardMemoLineClasses.remove(arStandardMemoLineClass);
			
		} catch (Exception ex) {
			
			throw new EJBException(ex.getMessage());
			
		}   
	}
	
	// ENTITY METHODS
	
	/**
	 * @ejb:create-method view-type="local"
	 **/
	public Integer ejbCreate(Integer SML_CODE, 
			String SML_TYP, String SML_NM, String SML_DESC, String SML_WP_PRDCT_ID,
			double SML_UNT_PRC, byte SML_TX,
			byte SML_ENBL, byte SML_SBJCT_TO_CMMSSN, String SML_UNT_OF_MSR, Integer SML_INTRM_ACCNT, Integer SML_GL_COA_RCVBL_ACCNT, Integer SML_GL_COA_RVNUE_ACCNT, Integer SML_AD_CMPNY)     	
	throws CreateException {	
		
		Debug.print("ArStandardMemoLineBean ejbCreate");
		
		setSmlCode(SML_CODE);
		setSmlType(SML_TYP);
		setSmlName(SML_NM);
		setSmlDescription(SML_DESC);
		setSmlWordPressProductID(SML_WP_PRDCT_ID);
		setSmlUnitPrice(SML_UNT_PRC);
		setSmlTax(SML_TX);
		setSmlEnable(SML_ENBL);  
		setSmlSubjectToCommission(SML_SBJCT_TO_CMMSSN);
		setSmlUnitOfMeasure(SML_UNT_OF_MSR);
		setSmlInterimAccount(SML_INTRM_ACCNT);
		setSmlGlCoaReceivableAccount(SML_GL_COA_RCVBL_ACCNT);
		setSmlGlCoaRevenueAccount(SML_GL_COA_RVNUE_ACCNT);
		setSmlAdCompany(SML_AD_CMPNY);
		
		return null;
	}
	
	/**
	 * @ejb:create-method view-type="local"
	 **/
	public Integer ejbCreate( 
			String SML_TYP, String SML_NM, String SML_DESC, String SML_WP_PRDCT_ID,
			double SML_UNT_PRC, byte SML_TX,
			byte SML_ENBL, byte SML_SBJCT_TO_CMMSSN, String SML_UNT_OF_MSR, Integer SML_INTRM_ACCNT, Integer SML_GL_COA_RCVBL_ACCNT, Integer SML_GL_COA_RVNUE_ACCNT, Integer SML_AD_CMPNY)     	
	throws CreateException {	
		
		Debug.print("ArStandardMemoLineBean ejbCreate");
		
		setSmlType(SML_TYP);
		setSmlName(SML_NM);
		setSmlDescription(SML_DESC);
		setSmlWordPressProductID(SML_WP_PRDCT_ID);
		setSmlUnitPrice(SML_UNT_PRC);
		setSmlTax(SML_TX);
		setSmlEnable(SML_ENBL);
		setSmlSubjectToCommission(SML_SBJCT_TO_CMMSSN);
		setSmlUnitOfMeasure(SML_UNT_OF_MSR);
		setSmlInterimAccount(SML_INTRM_ACCNT);
		setSmlGlCoaReceivableAccount(SML_GL_COA_RCVBL_ACCNT);
		setSmlGlCoaRevenueAccount(SML_GL_COA_RVNUE_ACCNT);
		setSmlAdCompany(SML_AD_CMPNY);
		
		return null;
	}
	
	public void ejbPostCreate(Integer SML_CODE, 
			String SML_TYP, String SML_NM, String SML_DESC, String SML_WP_PRDCT_ID,
			double SML_UNT_PRC, byte SML_TX,
			byte SML_ENBL, byte SML_SBJCT_TO_CMMSSN, String SML_UNT_OF_MSR, Integer SML_INTRM_ACCNT, Integer SML_GL_COA_RCVBL_ACCNT, Integer SML_GL_COA_RVNUE_ACCNT, Integer SML_AD_CMPNY)      
	throws CreateException { }
	
	public void ejbPostCreate( 
			String SML_TYP, String SML_NM, String SML_DESC, String SML_WP_PRDCT_ID,
			double SML_UNT_PRC, byte SML_TX,
			byte SML_ENBL, byte SML_SBJCT_TO_CMMSSN, String SML_UNT_OF_MSR, Integer SML_INTRM_ACCNT, Integer SML_GL_COA_RCVBL_ACCNT, Integer SML_GL_COA_RVNUE_ACCNT, Integer SML_AD_CMPNY)
	throws CreateException { }     
}