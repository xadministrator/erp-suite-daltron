/*
 * com/ejb/ar/LocalArStandardMemoLineClassBean.java
 *
 * Created on April 27, 2018, 12:10 AM
 */

package com.ejb.ar;

import java.util.Collection;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;

import com.ejb.ar.LocalArStandardMemoLineClass;
import com.util.AbstractEntityBean;
import com.util.Debug;
import java.util.Date;


/**
 *
 * @author  Ruben P. Lamberte
 *
 * @ejb:bean name="ArStandardMemoLineClassEJB"
 *           display-name="StandardMemoLineClass Entity"
 *           type="CMP"
 *           cmp-version="2.x"
 *           view-type="local"
 *           local-jndi-name="omega-ejb/ArStandardMemoLineClassEJB"
 *           schema="ArStandardMemoLineClass"
 *           primkey-field="smcCode"
 *
 * @ejb:pk class="java.lang.Integer"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="aruser"
 *                        role-link="aruserlink"
 *
 * @ejb:permission role-name="aruser"
 *
 * @ejb:interface local-class="com.ejb.ar.LocalArStandardMemoLineClass"
 *                local-extends="javax.ejb.EJBLocalObject"
 *
 * @ejb:home local-class="com.ejb.ar.LocalArStandardMemoLineClassHome"
 *           local-extends="javax.ejb.EJBLocalHome"
 *
 * @ejb:finder signature="Collection findSmcAll(java.lang.Integer SMC_AD_CMPNY)"
 *             query="SELECT OBJECT(smc) FROM ArStandardMemoLineClass smc WHERE smc.smcAdCompany = ?1"
 * 
 * @jboss:query signature="Collection findSmcAll(java.lang.Integer SMC_AD_CMPNY)"
 *             query="SELECT OBJECT(smc) FROM ArStandardMemoLineClass smc WHERE smc.smcAdCompany = ?1 ORDER BY smc.smcCode"
 
 * @ejb:finder signature="Collection findSmcByCcCode(java.lang.Integer CC_CODE, java.lang.Integer SMC_AD_CMPNY)"
 *             query="SELECT OBJECT(smc) FROM ArStandardMemoLineClass smc WHERE smc.arCustomerClass.ccCode = ?1 AND smc.smcAdCompany = ?2"
 *
 * @ejb:finder signature="Collection findSmcByCcName(java.lang.String CC_NM, java.lang.Integer SMC_AD_CMPNY)"
 * 	       query="SELECT OBJECT(smc) FROM ArStandardMemoLineClass smc WHERE smc.arCustomerClass.ccName = ?1 AND smc.smcAdCompany = ?2"
 *
 * @ejb:finder signature="LocalArStandardMemoLineClass findSmcByCcNameSmlNameCstCstmrCodeAdBrnch(java.lang.String CC_NM, java.lang.String SML_NM, java.lang.String CST_CSTMR_CODE, java.lang.Integer AD_BRNCH, java.lang.Integer SML_AD_CMPNY)"
 *             query="SELECT OBJECT(smc) FROM ArStandardMemoLineClass smc WHERE smc.arCustomerClass.ccName = ?1 AND smc.arStandardMemoLine.smlName = ?2 AND smc.arCustomer.cstCustomerCode = ?3 AND smc.smcAdBranch = ?4 AND smc.smcAdCompany = ?5"
 *
 * @ejb:finder signature="LocalArStandardMemoLineClass findSmcByCcNameSmlNameAdBrnch(java.lang.String CC_NM, java.lang.String SML_NM, java.lang.Integer AD_BRNCH, java.lang.Integer SML_AD_CMPNY)"
 *             query="SELECT OBJECT(smc) FROM ArStandardMemoLineClass smc WHERE smc.arCustomerClass.ccName = ?1 AND smc.arStandardMemoLine.smlName = ?2 AND smc.arCustomer IS NULL AND smc.smcAdBranch = ?3 AND smc.smcAdCompany = ?4"
 *
 * @ejb:select signature="Collection ejbSelectGeneric(java.lang.String jbossQl, java.lang.Object[] args)"
 *             result-type-mapping="Local"
 *             method-intf="LocalHome"
 *             query="SELECT OBJECT(smc) FROM ArStandardMemoLineClass smc"
 *
 * @jboss:query signature="Collection ejbSelectGeneric(java.lang.String jbossQl, java.lang.Object[] args)"
 *                         dynamic="true"
 *
 * @ejb:value-object match="*"
 *             name="ArStandardMemoLineClass"
 *
 * @jboss:persistence table-name="AR_STNDRD_MMO_LN_CLSS"
 *
 * @jboss:entity-command name="mysql-get-generated-keys"
 */
public abstract class ArStandardMemoLineClassBean extends AbstractEntityBean {
    
    // PERSITCENT METHODS
	
    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     * @ejb:pk-field
     *
     * @jboss:column-name name="SMC_CODE"
     *
     * @jboss:persistence auto-increment="true"
     **/
    public abstract Integer getSmcCode();
    public abstract void setSmcCode(Integer SMC_CODE);
	
    
    /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"   
    *
    * @jboss:column-name name="SMC_UNT_PRC"
    **/
    public abstract double getSmcUnitPrice();
    /**
     * @ejb:interface-method view-type="local"
     **/ 
    public abstract void setSmcUnitPrice(double SMC_UNT_PRC);
        
    
    /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"   
    *
    * @jboss:column-name name="SMC_SML_DESC"
    **/
    public abstract String getSmcStandardMemoLineDescription();
    /**
     * @ejb:interface-method view-type="local"
     **/ 
    public abstract void setSmcStandardMemoLineDescription(String SMC_SML_DESC);
    
    
    
    /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"   
    *
    * @jboss:column-name name="SMC_CRTD_BY"
    **/
    public abstract String getSmcCreatedBy();
    /**
     * @ejb:interface-method view-type="local"
     **/ 
    public abstract void setSmcCreatedBy(String SMC_CRTD_BY);
   
   
    /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="SMC_DT_CRTD"
    **/
    public abstract Date getSmcDateCreated();
    /**
     * @ejb:interface-method view-type="local"
     **/
    public abstract void setSmcDateCreated(Date SMC_DT_CRTD);
    
    /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"   
    *
    * @jboss:column-name name="SMC_MDFD_BY"
    **/
    public abstract String getSmcModifiedBy();
    /**
     * @ejb:interface-method view-type="local"
     **/ 
    public abstract void setSmcModifiedBy(String SMC_MDFD_BY);
   
    /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="SMC_DT_MDFD"
    **/
    public abstract Date getSmcDateModified();
    /**
     * @ejb:interface-method view-type="local"
     **/
    public abstract void setSmcDateModified(Date SMC_DT_MDFD);
    
    
    
    /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="SMC_AD_BRNCH"
    **/
    public abstract Integer getSmcAdBranch();
    /**
     * @ejb:interface-method view-type="local"
     **/
    public abstract void setSmcAdBranch(Integer SMC_AD_BRNCH);

    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="SMC_AD_CMPNY"
     **/
    public abstract Integer getSmcAdCompany();
    /**
     * @ejb:interface-method view-type="local"
     **/
    public abstract void setSmcAdCompany(Integer SMC_AD_CMPNY);
    
    
    // RELATIONSHIP METHODS
    
    
    /**
    * @ejb:interface-method view-type="local"
    * @ejb:relation name="standardmemoline-standardmemolineclasses"
    *               role-name="standardmemolineclass-has-one-standardmemoline"
    *
    * @jboss:relation related-pk-field="smlCode"
    *                 fk-column="AR_STANDARD_MEMO_LINE"
    */
    public abstract LocalArStandardMemoLine getArStandardMemoLine();
    /**
     * @ejb:interface-method view-type="local"
     **/
    public abstract void setArStandardMemoLine(LocalArStandardMemoLine arStandardMemoLine);
    
    
    /**
    * @ejb:interface-method view-type="local"
    * @ejb:relation name="customer-standardmemolineclasses"
    *               role-name="standardmemolineclass-has-one-customer"
    *
    * @jboss:relation related-pk-field="cstCode"
    *                 fk-column="AR_CUSTOMER"
    */
    public abstract LocalArCustomer getArCustomer();
    /**
     * @ejb:interface-method view-type="local"
     **/
    public abstract void setArCustomer(LocalArCustomer arCustomer);

    /**
    * @ejb:interface-method view-type="local"
    * @ejb:relation name="customerclass-standardmemolineclasses"
    *               role-name="standardmemolineclass-has-one-customerclass"
    *
    * @jboss:relation related-pk-field="ccCode"
    *                 fk-column="AR_CUSTOMER_CLASS"
    */
    public abstract LocalArCustomerClass getArCustomerClass();
    /**
     * @ejb:interface-method view-type="local"
     **/
    public abstract void setArCustomerClass(LocalArCustomerClass arCustomerClass);
    
    
    /**
    * @jboss:dynamic-ql
    */
    public abstract Collection ejbSelectGeneric(java.lang.String jbossQl, java.lang.Object[] args)
    throws FinderException;
    
    
    // BUSINESS METHODS
	
    /**
     * @ejb:home-method view-type="local"
     */
    public Collection ejbHomeGetSmcByCriteria(java.lang.String jbossQl, java.lang.Object[] args)
    throws FinderException {

            return ejbSelectGeneric(jbossQl, args);
    }
    
    
    
    // ENTITY METHODS
	
    /**
     * @ejb:create-method view-type="local"
     **/
    public Integer ejbCreate(Integer SMC_CODE, 
                    double SMC_UNT_PRC, String SMC_SML_DESC, String SMC_CRTD_BY, Date SMC_CRTD_DT, String SMC_MDFD_BY, Date SMC_DT_MDFD, int SMC_AD_BRNCH, int SMC_AD_CMPNY)     	
    throws CreateException {	

            Debug.print("ArStandardMemoLineClassBean ejbCreate");

            setSmcCode(SMC_CODE);
            setSmcUnitPrice(SMC_UNT_PRC);
            setSmcStandardMemoLineDescription(SMC_SML_DESC);
            setSmcCreatedBy(SMC_CRTD_BY);
            setSmcDateCreated(SMC_CRTD_DT);
            setSmcModifiedBy(SMC_MDFD_BY);
            setSmcDateModified(SMC_DT_MDFD);
            setSmcAdBranch(SMC_AD_BRNCH);
            setSmcAdCompany(SMC_AD_CMPNY);


            return null;
    }

    /**
     * @ejb:create-method view-type="local"
     **/
    public Integer ejbCreate( 
                     double SMC_UNT_PRC, String SMC_SML_DESC, String SMC_CRTD_BY, Date SMC_CRTD_DT, String SMC_MDFD_BY, Date SMC_DT_MDFD, int SMC_AD_BRNCH, int SMC_AD_CMPNY)     	
            throws CreateException {	

            Debug.print("ArStandardMemoLineClassBean ejbCreate");

            setSmcUnitPrice(SMC_UNT_PRC);
            setSmcStandardMemoLineDescription(SMC_SML_DESC);
            setSmcCreatedBy(SMC_CRTD_BY);
            setSmcDateCreated(SMC_CRTD_DT);
            setSmcModifiedBy(SMC_MDFD_BY);
            setSmcDateModified(SMC_DT_MDFD);
            setSmcAdBranch(SMC_AD_BRNCH);
            setSmcAdCompany(SMC_AD_CMPNY);

            return null;
    }

    public void ejbPostCreate(Integer SMC_CODE, 
                    double SMC_UNT_PRC, String SMC_SML_DESC, String SMC_CRTD_BY, Date SMC_CRTD_DT, String SMC_MDFD_BY, Date SMC_DT_MDFD, int SMC_AD_BRNCH, int SMC_AD_CMPNY)     	
    throws CreateException { }

    public void ejbPostCreate( double SMC_UNT_PRC, String SMC_SML_DESC, String SMC_CRTD_BY, Date SMC_CRTD_DT, String SMC_MDFD_BY, Date SMC_DT_MDFD, int SMC_AD_BRNCH, int SMC_AD_CMPNY)     	
           throws CreateException { }  
    
    
}
