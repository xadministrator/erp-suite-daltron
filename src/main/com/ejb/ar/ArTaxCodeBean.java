/*
 * com/ejb/ar/LocalArTaxCodeBean.java
 *
 * Created on March 03, 2004, 2:18 PM
 */

package com.ejb.ar;

import java.util.Collection;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;

import com.ejb.ad.LocalAdBranchArTaxCode;
import com.util.AbstractEntityBean;
import com.util.Debug;

/**
 *
 * @author  Neil Andrew M. Ajero
 *
 * @ejb:bean name="ArTaxCodeEJB"
 *           display-name="TaxCode Entity"
 *           type="CMP"
 *           cmp-version="2.x"
 *           view-type="local"
 *           local-jndi-name="omega-ejb/ArTaxCodeEJB"
 *           schema="ArTaxCode"
 *           primkey-field="tcCode"
 *
 * @ejb:pk class="java.lang.Integer"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="aruser"
 *                        role-link="aruserlink"
 *
 * @ejb:permission role-name="aruser"
 *
 * @ejb:interface local-class="com.ejb.ar.LocalArTaxCode"
 *                local-extends="javax.ejb.EJBLocalObject"
 *
 * @ejb:home local-class="com.ejb.ar.LocalArTaxCodeHome"
 *           local-extends="javax.ejb.EJBLocalHome"
 *
 * @ejb:finder signature="Collection findTcAll(java.lang.Integer TC_AD_CMPNY)"
 *             query="SELECT OBJECT(tc) FROM ArTaxCode tc WHERE tc.tcAdCompany = ?1"
 *
 * @jboss:query signature="Collection findTcAll(java.lang.Integer TC_AD_CMPNY)"
 *             query="SELECT OBJECT(tc) FROM ArTaxCode tc WHERE tc.tcAdCompany = ?1 ORDER BY tc.tcName"
 *
 * @ejb:finder signature="Collection findEnabledTcAll(java.lang.Integer TC_AD_CMPNY)"
 *             query="SELECT OBJECT(tc) FROM ArTaxCode tc WHERE tc.tcEnable = 1 AND tc.tcAdCompany = ?1"
 *
 * @jboss:query signature="Collection findEnabledTcAll(java.lang.Integer TC_AD_CMPNY)"
 *             query="SELECT OBJECT(tc) FROM ArTaxCode tc WHERE tc.tcEnable = 1 AND tc.tcAdCompany = ?1 ORDER BY tc.tcName"
 *
 * @ejb:finder signature="LocalArTaxCode findByTcName(java.lang.String TC_NM, java.lang.Integer TC_AD_CMPNY)"
 *             query="SELECT OBJECT(tc) FROM ArTaxCode tc WHERE tc.tcName = ?1 AND tc.tcAdCompany = ?2"
 * 
 * @ejb:select signature="Collection ejbSelectGeneric(java.lang.String jbossQl, java.lang.Object[] args)"
 *             result-type-mapping="Local"
 *             method-intf="LocalHome"
 *             query="SELECT OBJECT(tc) FROM ArTaxCode tc"
 *
 * @jboss:query signature="Collection ejbSelectGeneric(java.lang.String jbossQl, java.lang.Object[] args)"
 *                         dynamic="true"
 *
 * @ejb:value-object match="*"
 *             name="ArTaxCode"
 *
 * @jboss:persistence table-name="AR_TX_CD"
 *
 * @jboss:entity-command name="mysql-get-generated-keys"
 */

public abstract class ArTaxCodeBean extends AbstractEntityBean {
	
	
	// PERSITCENT METHODS
	
	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 * @ejb:pk-field
	 *
	 * @jboss:column-name name="TC_CODE"
	 *
	 * @jboss:persistence auto-increment="true"
	 **/
	public abstract Integer getTcCode();
	public abstract void setTcCode(Integer TC_CODE);
	
	
	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"   
	 *
	 * @jboss:column-name name="TC_NM"
	 **/
	public abstract String getTcName();
	/**
	 * @ejb:interface-method view-type="local"
	 **/ 
	public abstract void setTcName(String TC_NM);
	
	
	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"   
	 *
	 * @jboss:column-name name="TC_DESC"
	 **/
	public abstract String getTcDescription();
	/**
	 * @ejb:interface-method view-type="local"
	 **/ 
	public abstract void setTcDescription(String TC_DESC);
	
	
	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"   
	 *
	 * @jboss:column-name name="TC_TYP"
	 **/
	public abstract String getTcType();
	/**
	 * @ejb:interface-method view-type="local"
	 **/ 
	public abstract void setTcType(String TC_TYP);
	
	
	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"   
	 *
	 * @jboss:column-name name="TC_INTRM_ACCNT"
	 **/
	public abstract Integer getTcInterimAccount();
	/**
	 * @ejb:interface-method view-type="local"
	 **/ 
	public abstract void setTcInterimAccount(Integer TC_INTRM_ACCNT);
	
	
	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"   
	 *
	 * @jboss:column-name name="TC_RT"
	 **/
	public abstract double getTcRate();
	/**
	 * @ejb:interface-method view-type="local"
	 **/ 
	public abstract void setTcRate(double TC_RT);
	
	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="TC_ENBL"
	 **/
	public abstract byte getTcEnable();
	/**
	 * @ejb:interface-method view-type="local"
	 **/ 
	public abstract void setTcEnable(byte TC_ENBL);
	
	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="TC_AD_CMPNY"
	 **/
	public abstract Integer getTcAdCompany();
	/**
	 * @ejb:interface-method view-type="local"
	 **/ 
	public abstract void setTcAdCompany(Integer TC_AD_CMPNY);
	
	
	// RELATIONSHIP METHODS
	
	/**
	 * @ejb:interface-method view-type="local"
	 * @ejb:relation name="chartofaccount-artaxcodes"
	 *               role-name="artaxcode-has-one-chartofaccount"
	 * 
	 * @jboss:relation related-pk-field="coaCode"
	 *                 fk-column="GL_CHART_OF_ACCOUNT"
	 */
	public abstract com.ejb.gl.LocalGlChartOfAccount getGlChartOfAccount();
	public abstract void setGlChartOfAccount(com.ejb.gl.LocalGlChartOfAccount glChartOfAccount);
	
	
	/**
	 * @ejb:interface-method view-type="local"
	 * @ejb:relation name="artaxcode-customerclasses"
	 *               role-name="artaxcode-has-many-customerclasses"
	 * 
	 */
	public abstract Collection getArCustomerClasses();
	public abstract void setArCustomerClasses(Collection arCustomerClasses);    
	
	
	/**
	 * @ejb:interface-method view-type="local"
	 * @ejb:relation name="artaxcode-jobordertypes"
	 *               role-name="artaxcode-has-many-jobordertypes"
	 * 
	 */
	public abstract Collection getArJobOrderTypes();
	public abstract void setArJobOrderTypes(Collection arJobOrderTypes); 
	
	/**
	 * @ejb:interface-method view-type="local"
	 * @ejb:relation name="artaxcode-invoices"
	 *               role-name="artaxcode-has-many-invoices"
	 * 
	 */
	public abstract Collection getArInvoices();
	public abstract void setArInvoices(Collection arInvoices); 
	
	/**
	 * @ejb:interface-method view-type="local"
	 * @ejb:relation name="artaxcode-receipts"
	 *               role-name="artaxcode-has-many-receipts"
	 * 
	 */
	public abstract Collection getArReceipts();
	public abstract void setArReceipts(Collection arReceipts);
	
	/**
	 * @ejb:interface-method view-type="local"
	 * @ejb:relation name="artaxcode-joborders"
	 *               role-name="artaxcode-has-many-joborders"
	 * 
	 */
	public abstract Collection getArJobOrders();
	public abstract void setArJobOrders(Collection arJobOrders); 
	
	/**
	 * @ejb:interface-method view-type="local"
	 * @ejb:relation name="artaxcode-pdcs"
	 *               role-name="artaxcode-has-many-pdcs"
	 * 
	 */
	public abstract Collection getArPdcs();
	public abstract void setArPdcs(Collection arPdcs); 
	
	
	
	/**
	 * @ejb:interface-method view-type="local"
	 * @ejb:relation name="artaxcode-salesorder"
	 *               role-name="artaxcode-has-many-salesorder"
	 * 
	 */
	public abstract Collection getArSalesOrders();
	public abstract void setArSalesOrders(Collection arSalesOrders);
	
	
	/**
     * @ejb:interface-method view-type="local"
     * @ejb:relation name="artaxcode-branchartaxcode"
     *               role-name="artaxcode-has-many-branchartaxcode"
     */
    public abstract Collection getAdBranchArTaxCodes();
    public abstract void setAdBranchArTaxCodes(Collection adBranchArTaxCodes); 

	
	/**
	 * @jboss:dynamic-ql
	 */
	public abstract Collection ejbSelectGeneric(java.lang.String jbossQl, java.lang.Object[] args)
	throws FinderException;
	
	
	
	// BUSINESS METHODS
	
	/**
	 * @ejb:home-method view-type="local"
	 */
	public Collection ejbHomeGetTcByCriteria(java.lang.String jbossQl, java.lang.Object[] args)
	throws FinderException {
		
		return ejbSelectGeneric(jbossQl, args);
	}
	
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public void addArCustomerClass(LocalArCustomerClass arCustomerClass) {
		
		Debug.print("ArTaxCodeBean addArCustomerClass");
		
		try {
			
			Collection arCustomerClasses = getArCustomerClasses();
			arCustomerClasses.add(arCustomerClass);
			
		} catch (Exception ex) {
			
			throw new EJBException(ex.getMessage());
			
		}   
	}
	
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public void dropArCustomerClass(LocalArCustomerClass arCustomerClass) {
		
		Debug.print("ArTaxCodeBean dropArCustomerClass");
		
		try {
			
			Collection arCustomerClasses = getArCustomerClasses();
			arCustomerClasses.remove(arCustomerClass);
			
		} catch (Exception ex) {
			
			throw new EJBException(ex.getMessage());
			
		}   
	}
	
	
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public void addArInvoice(LocalArInvoice arInvoice) {
		
		Debug.print("ArTaxCodeBean addArInvoice");
		
		try {
			
			Collection arInvoices = getArInvoices();
			arInvoices.add(arInvoice);
			
		} catch (Exception ex) {
			
			throw new EJBException(ex.getMessage());
			
		}   
	}
	
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public void dropArInvoice(LocalArInvoice arInvoice) {
		
		Debug.print("ArTaxCodeBean dropArInvoice");
		
		try {
			
			Collection arInvoices = getArInvoices();
			arInvoices.remove(arInvoice);
			
		} catch (Exception ex) {
			
			throw new EJBException(ex.getMessage());
			
		}   
	}
	
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public void addArReceipt(LocalArReceipt arReceipt) {
		
		Debug.print("ArTaxCodeBean addArReceipt");
		
		try {
			
			Collection arReceipts = getArReceipts();
			arReceipts.add(arReceipt);
			
		} catch (Exception ex) {
			
			throw new EJBException(ex.getMessage());
			
		}   
	}
	
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public void dropArReceipt(LocalArReceipt arReceipt) {
		
		Debug.print("ArTaxCodeBean dropArReceipt");
		
		try {
			
			Collection arReceipts = getArReceipts();
			arReceipts.remove(arReceipt);
			
		} catch (Exception ex) {
			
			throw new EJBException(ex.getMessage());
			
		} 
		
	}
	
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public void addArPdc(LocalArPdc arPdc) {
		
		Debug.print("ArTaxCodeBean addArPdc");
		
		try {
			
			Collection arPdcs = getArPdcs();
			arPdcs.add(arPdc);
			
		} catch (Exception ex) {
			
			throw new EJBException(ex.getMessage());
			
		}   
	}
	
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public void dropArPdc(LocalArPdc arPdc) {
		
		Debug.print("ArTaxCodeBean dropArPdc");
		
		try {
			
			Collection arPdcs = getArPdcs();
			arPdcs.remove(arPdc);
			
		} catch (Exception ex) {
			
			throw new EJBException(ex.getMessage());
			
		}   
	}        
	
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public void addArSalesOrder(LocalArSalesOrder arSalesOrder) {
		
		Debug.print("ArTaxCodeBean addArSalesOrder");
		
		try {
			
			Collection arSalesOrders = getArSalesOrders();
			arSalesOrders.add(arSalesOrder);
			
		} catch (Exception ex) {
			
			throw new EJBException(ex.getMessage());
			
		}   
	}
	
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public void dropArSalesOrder(LocalArSalesOrder arSalesOrder) {
		
		Debug.print("ArTaxCodeBean dropArSalesOrder");
		
		try {
			
			Collection arSalesOrders = getArSalesOrders();
			arSalesOrders.remove(arSalesOrder);
			
		} catch (Exception ex) {
			
			throw new EJBException(ex.getMessage());
			
		}   
	}
	
	
	
	/**
     * @ejb:interface-method view-type="local"
     **/
    public void addAdBranchArTaxCode(LocalAdBranchArTaxCode adBranchArTaxCode) {

         Debug.print("ArTaxCodeBean addAdBranchArTaxCode");
   
         try {
   	
	        Collection adBranchArTaxCodes = getAdBranchArTaxCodes();

	        adBranchArTaxCodes.add(adBranchArTaxCode);
	        
         } catch (Exception ex) {
   	
             throw new EJBException(ex.getMessage());
      
         }
       
    }
 	
    /**
     * @ejb:interface-method view-type="local"
     **/ 
     public void dropAdBranchArTaxCode(LocalAdBranchArTaxCode adBranchArTaxCode) {
 		
 		  Debug.print("ArTaxCodeBean dropAdBranchArTaxCode");
 		  
 		  try {
 		  	
 		        Collection adBranchArTaxCodes = getAdBranchArTaxCodes();

 		        adBranchArTaxCodes.remove(adBranchArTaxCode);

 			     
 		  } catch (Exception ex) {
 		  	
 		     throw new EJBException(ex.getMessage());
 		     
 		  }
 		  
    }
     
     
     
	
	// ENTITY METHODS
	
	/**
	 * @ejb:create-method view-type="local"
	 **/
	public Integer ejbCreate(Integer TC_CODE, 
			String TC_NM, String TC_DESC, String TC_TYP, Integer TC_INTRM_ACCNT,
			double TC_RT, byte TC_ENBL, Integer TC_AD_CMPNY)     	
	throws CreateException {	
		
		Debug.print("ArTaxCodeBean ejbCreate");
		
		setTcCode(TC_CODE);
		setTcName(TC_NM);
		setTcDescription(TC_DESC); 
		setTcType(TC_TYP);
		setTcInterimAccount(TC_INTRM_ACCNT);
		setTcRate(TC_RT);  
		setTcEnable(TC_ENBL);	 
		setTcAdCompany(TC_AD_CMPNY);
		
		return null;
	}
	
	/**
	 * @ejb:create-method view-type="local"
	 **/
	public Integer ejbCreate(
			String TC_NM, String TC_DESC, String TC_TYP, Integer TC_INTRM_ACCNT,
			double TC_RT, byte TC_ENBL, Integer TC_AD_CMPNY)     	
	throws CreateException {	
		
		Debug.print("ArTaxCodeBean ejbCreate");
		
		setTcName(TC_NM);
		setTcDescription(TC_DESC); 
		setTcType(TC_TYP);
		setTcInterimAccount(TC_INTRM_ACCNT);
		setTcRate(TC_RT);  
		setTcEnable(TC_ENBL);
		setTcAdCompany(TC_AD_CMPNY);
		
		return null;
	}
	
	public void ejbPostCreate(Integer TC_CODE, 
			String TC_NM, String TC_DESC, String TC_TYP, Integer TC_INTRM_ACCNT, 
			double TC_RT, byte TC_ENBL, Integer TC_AD_CMPNY)      
	throws CreateException { }
	
	public void ejbPostCreate(
			String TC_NM, String TC_DESC, String TC_TYP, Integer TC_INTRM_ACCNT, 
			double TC_RT, byte TC_ENBL, Integer TC_AD_CMPNY)
	throws CreateException { }     
}