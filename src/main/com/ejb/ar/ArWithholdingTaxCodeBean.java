/*
 * com/ejb/ar/LocalArWithholdingTaxCodeBean.java
 *
 * Created on March 03, 2004, 2:26 PM
 */

package com.ejb.ar;

import java.util.Collection;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;

import com.ejb.ad.LocalAdPreference;
import com.util.AbstractEntityBean;
import com.util.Debug;

/**
 *
 * @author  Neil Andrew M. Ajero
 *
 * @ejb:bean name="ArWithholdingTaxCodeEJB"
 *           display-name="WithholdingTaxCode Entity"
 *           type="CMP"
 *           cmp-version="2.x"
 *           view-type="local"
 *           local-jndi-name="omega-ejb/ArWithholdingTaxCodeEJB"
 *           schema="ArWithholdingTaxCode"
 *           primkey-field="wtcCode"
 *
 * @ejb:pk class="java.lang.Integer"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="aruser"
 *                        role-link="aruserlink"
 *
 * @ejb:permission role-name="aruser"
 *
 * @ejb:interface local-class="com.ejb.ar.LocalArWithholdingTaxCode"
 *                local-extends="javax.ejb.EJBLocalObject"
 *
 * @ejb:home local-class="com.ejb.ar.LocalArWithholdingTaxCodeHome"
 *           local-extends="javax.ejb.EJBLocalHome"
 *
 * @ejb:finder signature="Collection findEnabledWtcAll(java.lang.Integer WTC_AD_CMPNY)"
 *             query="SELECT OBJECT(wtc) FROM ArWithholdingTaxCode wtc WHERE wtc.wtcEnable = 1 AND wtc.wtcAdCompany = ?1"
 *
 * @jboss:query signature="Collection findEnabledWtcAll(java.lang.Integer WTC_AD_CMPNY)"
 *             query="SELECT OBJECT(wtc) FROM ArWithholdingTaxCode wtc WHERE wtc.wtcEnable = 1 AND wtc.wtcAdCompany = ?1 ORDER BY wtc.wtcName"
 *
 * @ejb:finder signature="LocalApWithholdingTaxCode findByWtcName(java.lang.String WTC_NM, java.lang.Integer WTC_AD_CMPNY)"
 *             query="SELECT OBJECT(wtc) FROM ArWithholdingTaxCode wtc WHERE wtc.wtcName = ?1 AND wtc.wtcAdCompany = ?2"
 *
 * @ejb:finder signature="Collection findWtcAll(java.lang.Integer WTC_AD_CMPNY)"
 *             query="SELECT OBJECT(wtc) FROM ArWithholdingTaxCode wtc WHERE wtc.wtcAdCompany = ?1"
 *
 * @jboss:query signature="Collection findWtcAll(java.lang.Integer WTC_AD_CMPNY)"
 *             query="SELECT OBJECT(wtc) FROM ArWithholdingTaxCode wtc WHERE wtc.wtcAdCompany = ?1 ORDER BY wtc.wtcName"
 *
 * @ejb:select signature="Collection ejbSelectGeneric(java.lang.String jbossQl, java.lang.Object[] args)"
 *             result-type-mapping="Local"
 *             method-intf="LocalHome"
 *             query="SELECT OBJECT(wtc) FROM ArWithholdingTaxCode wtc"
 *
 * @jboss:query signature="Collection ejbSelectGeneric(java.lang.String jbossQl, java.lang.Object[] args)"
 *                         dynamic="true"
 *
 * @ejb:value-object match="*"
 *             name="ArWithholdingTaxCode"
 *
 * @jboss:persistence table-name="AR_WTHHLDNG_TX_CD"
 *
 * @jboss:entity-command name="mysql-get-generated-keys"
 */

public abstract class ArWithholdingTaxCodeBean extends AbstractEntityBean {
    
    
    // PERSITCENT METHODS
    
    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     * @ejb:pk-field
     *
     * @jboss:column-name name="WTC_CODE"
     *
     * @jboss:persistence auto-increment="true"
     **/
    public abstract Integer getWtcCode();
    public abstract void setWtcCode(Integer WTC_CODE);
    
    
    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"   
     *
     * @jboss:column-name name="WTC_NM"
     **/
    public abstract String getWtcName();
     /**
      * @ejb:interface-method view-type="local"
      **/ 
    public abstract void setWtcName(String WTC_NM);
    

    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"   
     *
     * @jboss:column-name name="WTC_DESC"
     **/
    public abstract String getWtcDescription();
     /**
      * @ejb:interface-method view-type="local"
      **/ 
    public abstract void setWtcDescription(String WTC_DESC);

    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"   
     *
     * @jboss:column-name name="WTC_RT"
     **/
    public abstract double getWtcRate();
     /**
      * @ejb:interface-method view-type="local"
      **/ 
    public abstract void setWtcRate(double WTC_RT);
    
    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="WTC_ENBL"
     **/
    public abstract byte getWtcEnable();
     /**
      * @ejb:interface-method view-type="local"
      **/ 
    public abstract void setWtcEnable(byte WTC_ENBL);
    
    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="WTC_AD_CMPNY"
     **/
    public abstract Integer getWtcAdCompany();
     /**
      * @ejb:interface-method view-type="local"
      **/ 
    public abstract void setWtcAdCompany(Integer WTC_AD_CMPNY);
    
   
    // RELATIONSHIP METHODS
    
    /**
     * @ejb:interface-method view-type="local"
     * @ejb:relation name="chartofaccount-arwithholdingtaxcodes"
     *               role-name="arwithholdingtaxcode-has-one-chartofaccount"
     * 
     * @jboss:relation related-pk-field="coaCode"
     *                 fk-column="GL_CHART_OF_ACCOUNT"
     */
    public abstract com.ejb.gl.LocalGlChartOfAccount getGlChartOfAccount();
    public abstract void setGlChartOfAccount(com.ejb.gl.LocalGlChartOfAccount glChartOfAccount);


    /**
     * @ejb:interface-method view-type="local"
     * @ejb:relation name="arwithholdingtaxcode-customerclasses"
     *               role-name="arwithholdingtaxcode-has-many-customerclasses"
     * 
     */
    public abstract Collection getArCustomerClasses();
    public abstract void setArCustomerClasses(Collection arCustomerClasses);  
    
    /**
     * @ejb:interface-method view-type="local"
     * @ejb:relation name="arwithholdingtaxcode-jobordertypes"
     *               role-name="arwithholdingtaxcode-has-many-jobordertypes"
     * 
     */
    public abstract Collection getArJobOrderTypes();
    public abstract void setArJobOrderTypes(Collection arJobOrderTypes); 

    /**
     * @ejb:interface-method view-type="local"
     * @ejb:relation name="arwithholdingtaxcode-invoices"
     *               role-name="arwithholdingtaxcode-has-many-invoices"
     * 
     */
    public abstract Collection getArInvoices();
    public abstract void setArInvoices(Collection arInvoices); 

    /**
     * @ejb:interface-method view-type="local"
     * @ejb:relation name="arwithholdingtaxcode-receipts"
     *               role-name="arwithholdingtaxcode-has-many-receipts"
     * 
     */
    public abstract Collection getArReceipts();
    public abstract void setArReceipts(Collection arReceipts);
    
    /**
     * @ejb:interface-method view-type="local"
     * @ejb:relation name="arwithholdingtaxcode-preferences"
     *               role-name="arwithholdingtaxcode-has-many-preferences"
     * 
     */
    public abstract Collection getAdPreferences();
    public abstract void setAdPreferences(Collection adPreferences);
    
    /**
     * @ejb:interface-method view-type="local"
     * @ejb:relation name="arwithholdingtaxcode-pdcs"
     *               role-name="arwithholdingtaxcode-has-many-pdcs"
     * 
     */
    public abstract Collection getArPdcs();
    public abstract void setArPdcs(Collection arPdcs);    

    /**
     * @jboss:dynamic-ql
     */
     public abstract Collection ejbSelectGeneric(java.lang.String jbossQl, java.lang.Object[] args)
        throws FinderException;
        
     

    // BUSINESS METHODS
    
    /**
     * @ejb:home-method view-type="local"
     */
     public Collection ejbHomeGetWtcByCriteria(java.lang.String jbossQl, java.lang.Object[] args)
        throws FinderException {
        	
        return ejbSelectGeneric(jbossQl, args);
     }
     
    /**
     * @ejb:interface-method view-type="local"
     **/
    public void addArCustomerClass(LocalArCustomerClass arCustomerClass) {

         Debug.print("ArWithholdingTaxCodeBean addArCustomerClass");
      
         try {
      	
            Collection arCustomerClasses = getArCustomerClasses();
	        arCustomerClasses.add(arCustomerClass);
	          
         } catch (Exception ex) {
      	
           throw new EJBException(ex.getMessage());
      
         }   
     }

    /**
     * @ejb:interface-method view-type="local"
     **/
    public void dropArCustomerClass(LocalArCustomerClass arCustomerClass) {

         Debug.print("ArWithholdingTaxCodeBean dropArCustomerClass");
      
         try {
      	
            Collection arCustomerClasses = getArCustomerClasses();
	        arCustomerClasses.remove(arCustomerClass);
	      
         } catch (Exception ex) {
      	
           throw new EJBException(ex.getMessage());
         
         }   
     }
     

   /**
     * @ejb:interface-method view-type="local"
     **/
    public void addArInvoice(LocalArInvoice arInvoice) {

         Debug.print("ArWithholdingTaxCodeBean addArInvoice");
      
         try {
      	
            Collection arInvoices = getArInvoices();
	        arInvoices.add(arInvoice);
	          
         } catch (Exception ex) {
      	
           throw new EJBException(ex.getMessage());
      
         }   
     }

    /**
     * @ejb:interface-method view-type="local"
     **/
    public void dropArInvoice(LocalArInvoice arInvoice) {

         Debug.print("ArWithholdingTaxCodeBean dropArInvoice");
      
         try {
      	
            Collection arInvoices = getArInvoices();
	        arInvoices.remove(arInvoice);
	      
         } catch (Exception ex) {
      	
           throw new EJBException(ex.getMessage());
         
         }   
     }
     
    /**
     * @ejb:interface-method view-type="local"
     **/
    public void addArReceipt(LocalArReceipt arReceipt) {

         Debug.print("ArWithholdingTaxCodeBean addArReceipt");
      
         try {
      	
            Collection arReceipts = getArReceipts();
	        arReceipts.add(arReceipt);
	          
         } catch (Exception ex) {
      	
           throw new EJBException(ex.getMessage());
      
         }   
     }

    /**
     * @ejb:interface-method view-type="local"
     **/
    public void dropArReceipt(LocalArReceipt arReceipt) {

         Debug.print("ArWithholdingTaxCodeBean dropArReceipt");
      
         try {
      	
            Collection arReceipts = getArReceipts();
	        arReceipts.remove(arReceipt);
	      
         } catch (Exception ex) {
      	
           throw new EJBException(ex.getMessage());
         
         } 
           
     }
                
    /**
     * @ejb:interface-method view-type="local"
     **/
    public void addAdPreference(LocalAdPreference adPreference) {

         Debug.print("ArWithholdingTaxCodeBean addAdPreference");
      
         try {
      	
            Collection adPreferences = getAdPreferences();
	        adPreferences.add(adPreference);
	          
         } catch (Exception ex) {
      	
           throw new EJBException(ex.getMessage());
      
         }   
     }

    /**
     * @ejb:interface-method view-type="local"
     **/
    public void dropAdPreference(LocalAdPreference adPreference) {

         Debug.print("ArWithholdingTaxCodeBean dropAdPreference");
      
         try {
      	
            Collection adPreferences = getAdPreferences();
            adPreferences.remove(adPreference);
	      
         } catch (Exception ex) {
      	
           throw new EJBException(ex.getMessage());
         
         } 
           
     }
    
    /**
     * @ejb:interface-method view-type="local"
     **/
    public void addArPdc(LocalArPdc arPdc) {

         Debug.print("ArWithholdingTaxCodeBean addArPdc");
      
         try {
      	
            Collection arPdcs = getArPdcs();
            arPdcs.add(arPdc);
	          
         } catch (Exception ex) {
      	
           throw new EJBException(ex.getMessage());
      
         }   
     }

    /**
     * @ejb:interface-method view-type="local"
     **/
    public void dropArPdc(LocalArPdc arPdc) {

         Debug.print("ArWithholdingTaxCodeBean dropArPdc");
      
         try {
      	
            Collection arPdcs = getArPdcs();
            arPdcs.remove(arPdc);
	      
         } catch (Exception ex) {
      	
           throw new EJBException(ex.getMessage());
         
         }   
     }    

    // ENTITY METHODS
    
    /**
     * @ejb:create-method view-type="local"
     **/
    public Integer ejbCreate(Integer WTC_CODE, 
         String WTC_NM, String WTC_DESC, double WTC_RT, byte WTC_ENBL,
		 Integer WTC_AD_CMPNY)     	
         throws CreateException {	
     	
         Debug.print("ArWithholdingTaxCodeBean ejbCreate");
     
	     setWtcCode(WTC_CODE);
	     setWtcName(WTC_NM);
	     setWtcDescription(WTC_DESC); 
	     setWtcRate(WTC_RT);  
         setWtcEnable(WTC_ENBL);	
         setWtcAdCompany(WTC_AD_CMPNY);

         return null;
     }
     
    /**
     * @ejb:create-method view-type="local"
     **/
    public Integer ejbCreate(
         String WTC_NM, String WTC_DESC, double WTC_RT, byte WTC_ENBL,
		 Integer WTC_AD_CMPNY)     	
         throws CreateException {	
     	
         Debug.print("ArWithholdingTaxCodeBean ejbCreate");
     
	     setWtcName(WTC_NM);
	     setWtcDescription(WTC_DESC); 
	     setWtcRate(WTC_RT);  
         setWtcEnable(WTC_ENBL);
         setWtcAdCompany(WTC_AD_CMPNY);

         return null;
     }
     
     public void ejbPostCreate(Integer WTC_CODE, 
         String WTC_NM, String WTC_DESC, double WTC_RT, byte WTC_ENBL,
		 Integer WTC_AD_CMPNY)      
         throws CreateException { }
      
     public void ejbPostCreate(
         String WTC_NM, String WTC_DESC, double WTC_RT, byte WTC_ENBL,
		 Integer WTC_AD_CMPNY)
         throws CreateException { }     
}