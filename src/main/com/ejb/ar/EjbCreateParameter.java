package com.ejb.ar;

public class EjbCreateParameter {
	public Integer aI_CODE;
	public double aI_APPLY_AMNT;
	public double aI_CRDTBL_W_TX;
	public double aI_DSCNT_AMNT;
	public double aI_APPLD_DPST;
	public double aI_ALLCTD_PYMNT_AMNT;
	public Integer aI_AD_CMPNY;
	public double aI_FRX_GN_LSS;

	public EjbCreateParameter(Integer aI_CODE, double aI_APPLY_AMNT,
			double aI_CRDTBL_W_TX, double aI_DSCNT_AMNT, double aI_APPLD_DPST,
			double aI_ALLCTD_PYMNT_AMNT, Integer aI_AD_CMPNY,
			double aI_FRX_GN_LSS) {
		this.aI_CODE = aI_CODE;
		this.aI_APPLY_AMNT = aI_APPLY_AMNT;
		this.aI_CRDTBL_W_TX = aI_CRDTBL_W_TX;
		this.aI_DSCNT_AMNT = aI_DSCNT_AMNT;
		this.aI_APPLD_DPST = aI_APPLD_DPST;
		this.aI_ALLCTD_PYMNT_AMNT = aI_ALLCTD_PYMNT_AMNT;
		this.aI_AD_CMPNY = aI_AD_CMPNY;
		this.aI_FRX_GN_LSS = aI_FRX_GN_LSS;
	}
}