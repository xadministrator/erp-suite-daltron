/*
 * com/ejb/cm/LocalCmAdjustmentBean.java
 *
 * Created on October 23, 2003, 03:04 PM
 */

package com.ejb.cm;

import java.util.Collection;
import java.util.Date;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;

import com.ejb.ar.LocalArAppliedCredit;
import com.ejb.ar.LocalArReceipt;
import com.ejb.hr.LocalHrPayrollPeriod;
import com.util.AbstractEntityBean;
import com.util.Debug;

/**
 *
 * @author  Dennis M. Hilario
 *
 * @ejb:bean name="CmAdjustmentEJB"
 *           display-name="Adjustment Entity"
 *           type="CMP"
 *           cmp-version="2.x"
 *           view-type="local"
 *           local-jndi-name="omega-ejb/CmAdjustmentEJB"
 *           schema="CmAdjustment"
 *           primkey-field="adjCode"
 *
 * @ejb:pk class="java.lang.Integer"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="cmuser"
 *                        role-link="cmuserlink"
 *
 * @ejb:permission role-name="cmuser"
 *
 * @ejb:interface local-class="com.ejb.cm.LocalCmAdjustment"
 *                local-extends="javax.ejb.EJBLocalObject"
 *
 * @ejb:home local-class="com.ejb.cm.LocalCmAdjustmentHome"
 *           local-extends="javax.ejb.EJBLocalHome"
 *
 * @ejb:finder signature="Collection findUnreconciledPostedAdjByDateAndBaNameAndAdjTypeAndBrCode(java.util.Date DT, java.lang.String BA_NM, java.lang.String ADJ_TYP, java.lang.Integer ADJ_AD_BRNCH, java.lang.Integer ADJ_AD_CMPNY)"
 *             query="SELECT OBJECT(adj) FROM CmAdjustment adj WHERE adj.adjDate <= ?1 AND adj.adBankAccount.baName = ?2 AND adj.adjType = ?3 AND adj.adjReconciled = 0 AND adj.adjPosted = 1 AND adj.adjVoid = 0 AND adj.adjAdBranch = ?4 AND adj.adjAdCompany = ?5"
 *
 * @jboss:query signature="Collection findUnreconciledPostedAdjByDateAndBaNameAndAdjTypeAndBrCode(java.util.Date DT, java.lang.String BA_NM, java.lang.String ADJ_TYP, java.lang.Integer ADJ_AD_BRNCH, java.lang.Integer ADJ_AD_CMPNY)"
 *             query="SELECT OBJECT(adj) FROM CmAdjustment adj WHERE adj.adjDate <= ?1 AND adj.adBankAccount.baName = ?2 AND adj.adjType = ?3 AND adj.adjReconciled = 0 AND adj.adjPosted = 1 AND adj.adjVoid = 0 AND adj.adjAdBranch = ?4 AND adj.adjAdCompany = ?5 ORDER BY adj.adjDate"
 *
 *
 * @ejb:finder signature="Collection findPostedAdjByDateAndBaNameAndAdjType(java.util.Date DT, java.lang.String BA_NM, java.lang.String ADJ_TYP, java.lang.Integer ADJ_AD_CMPNY)"
 *             query="SELECT OBJECT(adj) FROM CmAdjustment adj WHERE adj.adjDate <= ?1 AND adj.adBankAccount.baName = ?2 AND adj.adjType = ?3 AND adj.adjPosted = 1 AND adj.adjVoid = 0 AND adj.adjAdCompany = ?4"
 *
 *
 * @ejb:finder signature="Collection findUnreconciledPostedAdjByDateAndBaNameAndAdjType(java.util.Date DT, java.lang.String BA_NM, java.lang.String ADJ_TYP, java.lang.Integer ADJ_AD_CMPNY)"
 *             query="SELECT OBJECT(adj) FROM CmAdjustment adj WHERE adj.adjDate <= ?1 AND adj.adBankAccount.baName = ?2 AND adj.adjType = ?3 AND adj.adjReconciled = 0 AND adj.adjPosted = 1 AND adj.adjVoid = 0 AND adj.adjAdCompany = ?4"
 *
 * @jboss:query signature="Collection findUnreconciledPostedAdjByDateAndBaNameAndAdjType(java.util.Date DT, java.lang.String BA_NM, java.lang.String ADJ_TYP, java.lang.Integer ADJ_AD_CMPNY)"
 *             query="SELECT OBJECT(adj) FROM CmAdjustment adj WHERE adj.adjDate <= ?1 AND adj.adBankAccount.baName = ?2 AND adj.adjType = ?3 AND adj.adjReconciled = 0 AND adj.adjPosted = 1 AND adj.adjVoid = 0 AND adj.adjAdCompany = ?4 ORDER BY adj.adjDate"
 *
 * @ejb:finder signature="LocalCmAdjustment findAdjByReferenceNumber(java.lang.String ADJ_RFRNC_NMBR, java.lang.Integer ADJ_AD_CMPNY)"
 *             query="SELECT OBJECT(adj) FROM CmAdjustment adj WHERE adj.adjReferenceNumber = ?1 AND adj.adjVoid = 0 AND adj.adjAdCompany = ?2"
 *
 * @ejb:finder signature="Collection findPostedAdjByAdjDateRange(java.util.Date ADJ_DT_FRM, java.util.Date ADJ_DT_TO, java.lang.Integer ADJ_AD_CMPNY)"
 *             query="SELECT OBJECT(adj) FROM CmAdjustment adj WHERE adj.adjVoid = 0 AND adj.adjPosted = 1 AND adj.adjDate >= ?1 AND adj.adjDate <= ?2 AND adj.adjAdCompany = ?3"
 *
 * @jboss:query signature="Collection findPostedAdjByAdjDateRange(java.util.Date ADJ_DT_FRM, java.util.Date ADJ_DT_TO, java.lang.Integer ADJ_AD_CMPNY)"
 *             query="SELECT OBJECT(adj) FROM CmAdjustment adj WHERE adj.adjPosted = 1 AND adj.adjVoid = 0 AND adj.adjPosted = 1 AND adj.adjDate >= ?1 AND adj.adjDate <= ?2 AND adj.adjAdCompany = ?3 ORDER BY adj.adjDate"
 *
 * @ejb:finder signature="Collection findPostedAdjByCustomerCode(java.lang.String CST_CSTMR_CODE, java.lang.Integer ADJ_AD_CMPNY)"
 *             query="SELECT OBJECT(adj) FROM CmAdjustment adj WHERE adj.adjPosted = 1 AND adj.adjVoid = 0 AND adj.adjType = 'ADVANCE' AND adj.arCustomer.cstCustomerCode = ?1 AND adj.adjAdCompany = ?2"
 *
 * @jboss:query signature="Collection findPostedAdjByCustomerCode(java.lang.String CST_CSTMR_CODE, java.lang.Integer ADJ_AD_CMPNY)"
 *             query="SELECT OBJECT(adj) FROM CmAdjustment adj WHERE adj.adjVoid = 0 AND adj.adjType = 'ADVANCE' AND adj.arCustomer.cstCustomerCode = ?1 AND adj.adjAdCompany = ?2"
 *
 * @ejb:finder signature="Collection findAdjAll(java.lang.Integer ADJ_AD_CMPNY)"
 *             query="SELECT OBJECT(adj) FROM CmAdjustment adj WHERE adj.adjAdCompany = ?1"
 *
 * @jboss:query signature="Collection findAdjAll(java.lang.Integer ADJ_AD_CMPNY)"
 *             query="SELECT OBJECT(adj) FROM CmAdjustment adj WHERE adj.adjAdCompany = ?1 ORDER BY adj.adjType"
 *
 * @ejb:finder signature="Collection findDraftAdjAll(java.lang.Integer ADJ_AD_CMPNY)"
 *             query="SELECT OBJECT(adj) FROM CmAdjustment adj WHERE adj.adjApprovalStatus IS NULL AND adj.adjAdCompany = ?1"
 *
 * @ejb:finder signature="Collection findDraftAdjByBrCode(java.lang.Integer ADJ_AD_BRNCH, java.lang.Integer ADJ_AD_CMPNY)"
 *             query="SELECT OBJECT(adj) FROM CmAdjustment adj WHERE adj.adjApprovalStatus IS NULL AND adj.adjAdBranch = ?1 AND adj.adjAdCompany = ?2"
 *
 * @ejb:finder signature="Collection findUnpostedCmAdjByCmAdjDateRange(java.util.Date ADJ_DT_FRM, java.util.Date ADJ_DT_TO, java.lang.Integer ADJ_AD_CMPNY)"
 *             query="SELECT OBJECT(adj) FROM CmAdjustment adj  WHERE adj.adjPosted = 0 AND adj.adjVoid = 0 AND adj.adjDate >= ?1 AND adj.adjDate <= ?2 AND adj.adjAdCompany = ?3"
 *
 * @jboss:query signature="Collection findUnpostedCmAdjByCmAdjDateRange(java.util.Date ADJ_DT_FRM, java.util.Date ADJ_DT_TO, java.lang.Integer ADJ_AD_CMPNY)"
 *             query="SELECT OBJECT(adj) FROM CmAdjustment adj  WHERE adj.adjPosted = 0 AND adj.adjVoid = 0 AND adj.adjDate >= ?1 AND adj.adjDate <= ?2 AND adj.adjAdCompany = ?3 ORDER BY adj.adjDate"
 *
 * @ejb:finder signature="LocalCmAdjustment findByAdjDocumentNumberAndBrCode(java.lang.String ADJ_DCMNT_NMBR, java.lang.Integer ADJ_AD_BRNCH, java.lang.Integer ADJ_AD_CMPNY)"
 *             query="SELECT OBJECT(adj) FROM CmAdjustment adj  WHERE adj.adjDocumentNumber = ?1 AND adj.adjAdBranch = ?2 AND adj.adjAdCompany = ?3"
 *
 * @ejb:finder signature="Collection findPostedAdjByBaNameAndAdjDateRangeAndAdjTypeAndBrCode(java.lang.String BA_NM, java.util.Date ADJ_DT_FRM, java.util.Date ADJ_DT_TO, java.lang.String ADJ_TYP, java.lang.Integer ADJ_AD_BRNCH, java.lang.Integer ADJ_AD_CMPNY)"
 *             query="SELECT OBJECT(adj) FROM CmAdjustment adj  WHERE adj.adjPosted = 1 AND adj.adjVoid = 0 AND adj.adBankAccount.baIsCashAccount = 0 AND adj.adBankAccount.baName = ?1 AND adj.adjDate >= ?2 AND adj.adjDate <= ?3 AND adj.adjType = ?4 AND adj.adjAdBranch = ?5 AND adj.adjAdCompany = ?6"
 *
 * @jboss:query signature="Collection findPostedAdjByBaNameAndAdjDateRangeAndAdjTypeAndBrCode(java.lang.String BA_NM, java.util.Date ADJ_DT_FRM, java.util.Date ADJ_DT_TO, java.lang.String ADJ_TYP, java.lang.Integer ADJ_AD_BRNCH, java.lang.Integer ADJ_AD_CMPNY)"
 *             query="SELECT OBJECT(adj) FROM CmAdjustment adj  WHERE adj.adjPosted = 1 AND adj.adjVoid = 0 AND adj.adBankAccount.baIsCashAccount = 0 AND adj.adBankAccount.baName = ?1 AND adj.adjDate >= ?2 AND adj.adjDate <= ?3 AND adj.adjType = ?4 AND adj.adjAdBranch = ?5 AND adj.adjAdCompany = ?6 ORDER BY adj.adjDate"
 *
 * @ejb:finder signature="Collection findDraftAdjByBaNameAndAdjDateRangeAndAdjTypeAndBrCode(java.lang.String BA_NM, java.util.Date ADJ_DT_FRM, java.util.Date ADJ_DT_TO, java.lang.String ADJ_TYP, java.lang.Integer ADJ_AD_BRNCH, java.lang.Integer ADJ_AD_CMPNY)"
 *             query="SELECT OBJECT(adj) FROM CmAdjustment adj  WHERE adj.adjPosted = 0 AND adj.adjApprovalStatus IS NULL AND adj.adjVoid = 0 AND adj.adBankAccount.baIsCashAccount = 0 AND adj.adBankAccount.baName = ?1 AND adj.adjDate >= ?2 AND adj.adjDate <= ?3 AND adj.adjType = ?4 AND adj.adjAdBranch = ?5 AND adj.adjAdCompany = ?6"
 *
 * @jboss:query signature="Collection findDraftAdjByBaNameAndAdjDateRangeAndAdjTypeAndBrCode(java.lang.String BA_NM, java.util.Date ADJ_DT_FRM, java.util.Date ADJ_DT_TO, java.lang.String ADJ_TYP, java.lang.Integer ADJ_AD_BRNCH, java.lang.Integer ADJ_AD_CMPNY)"
 *             query="SELECT OBJECT(adj) FROM CmAdjustment adj  WHERE adj.adjPosted = 0 AND adj.adjApprovalStatus IS NULL AND adj.adjVoid = 0 AND adj.adBankAccount.baIsCashAccount = 0 AND adj.adBankAccount.baName = ?1 AND adj.adjDate >= ?2 AND adj.adjDate <= ?3 AND adj.adjType = ?4 AND adj.adjAdBranch = ?5 AND adj.adjAdCompany = ?6 ORDER BY adj.adjDate"
 *
 * @ejb:finder signature="Collection findReconciledPostedAdjByBaNameAndAdjDateRangeAndAdjTypeAndBrCode(java.lang.String BA_NM, java.util.Date ADJ_DT_FRM, java.util.Date ADJ_DT_TO, java.lang.String ADJ_TYP, java.lang.Integer ADJ_AD_BRNCH, java.lang.Integer ADJ_AD_CMPNY)"
 *             query="SELECT OBJECT(adj) FROM CmAdjustment adj  WHERE adj.adjPosted = 1 AND adj.adjVoid = 0 AND adj.adjReconciled = 1 AND adj.adBankAccount.baIsCashAccount = 0 AND adj.adBankAccount.baName = ?1 AND adj.adjDate >= ?2 AND adj.adjDate <= ?3 AND adj.adjType = ?4 AND adj.adjAdBranch = ?5 AND adj.adjAdCompany = ?6"
 *
 * @jboss:query signature="Collection findReconciledPostedAdjByBaNameAndAdjDateRangeAndAdjTypeAndBrCode(java.lang.String BA_NM, java.util.Date ADJ_DT_FRM, java.util.Date ADJ_DT_TO, java.lang.String ADJ_TYP, java.lang.Integer ADJ_AD_BRNCH, java.lang.Integer ADJ_AD_CMPNY)"
 *             query="SELECT OBJECT(adj) FROM CmAdjustment adj  WHERE adj.adjPosted = 1 AND adj.adjVoid = 0 AND adj.adjReconciled = 1 AND adj.adBankAccount.baIsCashAccount = 0 AND adj.adBankAccount.baName = ?1 AND adj.adjDate >= ?2 AND adj.adjDate <= ?3 AND adj.adjType = ?4 AND adj.adjAdBranch = ?5 AND adj.adjAdCompany = ?6 ORDER BY adj.adjDate"
 *
 * @ejb:finder signature="LocalCmAdjustment findAdjByAdjDateAndPpCodeAndCstCodeAndBaName(java.util.Date ADJ_DT, java.lang.Integer PP_CODE, java.lang.Integer CST_CODE, java.lang.String BA_NM, java.lang.Integer ADJ_AD_CMPNY)"
 *             query="SELECT OBJECT(adj) FROM CmAdjustment adj WHERE adj.adjVoid = 0  AND adj.adjDate = ?1 AND adj.hrPayrollPeriod.ppCode = ?2 AND adj.arCustomer.cstCode = ?3 AND adj.adBankAccount.baName = ?4 AND  adj.adjAdCompany = ?5"
 *
 * @ejb:select signature="Collection ejbSelectGeneric(java.lang.String jbossQl, java.lang.Object[] args)"
 *             result-type-mapping="Local"
 *             method-intf="LocalHome"
 *             query="SELECT OBJECT(adj) FROM CmAdjustment adj"
 *
 * @jboss:query signature="Collection ejbSelectGeneric(java.lang.String jbossQl, java.lang.Object[] args)"
 *                         dynamic="true"
 *
 * @ejb:value-object match="*"
 *             name="CmAdjustment"
 *
 * @jboss:persistence table-name="CM_ADJSTMNT"
 *
 * @jboss:entity-command name="mysql-get-generated-keys"
 */

public abstract class CmAdjustmentBean extends AbstractEntityBean {


    // PERSISTENT METHODS

    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     * @ejb:pk-field
     *
     * @jboss:column-name name="ADJ_CODE"
     *
     * @jboss:persistence auto-increment="true"
     **/
    public abstract Integer getAdjCode();
    public abstract void setAdjCode(Integer ADJ_CODE);

    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="ADJ_TYP"
     **/
     public abstract String getAdjType();
     /**
      * @ejb:interface-method view-type="local"
      **/
     public abstract void setAdjType(String ADJ_TYP);

    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="ADJ_DT"
     **/
     public abstract Date getAdjDate();
     /**
      * @ejb:interface-method view-type="local"
      **/
     public abstract void setAdjDate(Date ADJ_DT);

     /**
      * @ejb:persistent-field
      * @ejb:interface-method view-type="local"
      *
      * @jboss:column-name name="ADJ_DCMNT_NMBR"
      **/
     public abstract String getAdjDocumentNumber();
     /**
      * @ejb:interface-method view-type="local"
      **/
     public abstract void setAdjDocumentNumber(String ADJ_DCMNT_NMBR);

    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="ADJ_RFRNC_NMBR"
     **/
     public abstract String getAdjReferenceNumber();
     /**
      * @ejb:interface-method view-type="local"
      **/
     public abstract void setAdjReferenceNumber(String ADJ_RFRNC_NMBR);


     /**
      * @ejb:persistent-field
      * @ejb:interface-method view-type="local"
      *
      * @jboss:column-name name="ADJ_CHCK_NMBR"
      **/
      public abstract String getAdjCheckNumber();
      /**
       * @ejb:interface-method view-type="local"
       **/
      public abstract void setAdjCheckNumber(String ADJ_CHCK_NMBR);

     /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="ADJ_AMNT"
     **/
     public abstract double getAdjAmount();
     /**
      * @ejb:interface-method view-type="local"
      **/
     public abstract void setAdjAmount(double ADJ_AMNT);

     /**
      * @ejb:persistent-field
      * @ejb:interface-method view-type="local"
      *
      * @jboss:column-name name="ADJ_AMNT_APPLD"
      **/
      public abstract double getAdjAmountApplied();
      /**
       * @ejb:interface-method view-type="local"
       **/
      public abstract void setAdjAmountApplied(double ADJ_AMNT_APPLD);
      
      
   

    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="ADJ_CNVRSN_DT"
     **/
     public abstract Date getAdjConversionDate();
     /**
      * @ejb:interface-method view-type="local"
      **/
     public abstract void setAdjConversionDate(Date ADJ_CNVRSN_DT);

    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="ADJ_CNVRSN_RT"
     **/
     public abstract double getAdjConversionRate();
     /**
      * @ejb:interface-method view-type="local"
      **/
     public abstract void setAdjConversionRate(double ADJ_CNVRSN_RT);

    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="ADJ_MM"
     **/
     public abstract String getAdjMemo();
     /**
      * @ejb:interface-method view-type="local"
      **/
     public abstract void setAdjMemo(String ADJ_MM);



     /**
 	 * @ejb:persistent-field
 	 * @ejb:interface-method view-type="local"
 	 *
 	 * @jboss:column-name name="ADJ_VD_APPRVL_STATUS"
 	 **/
 	public abstract String getAdjVoidApprovalStatus();
 	/**
 	 * @ejb:interface-method view-type="local"
 	 **/
 	public abstract void setAdjVoidApprovalStatus(String ADJ_VD_APPRVL_STATUS);

 	/**
 	 * @ejb:persistent-field
 	 * @ejb:interface-method view-type="local"
 	 *
 	 * @jboss:column-name name="ADJ_VD_PSTD"
 	 **/
 	public abstract byte getAdjVoidPosted();
 	/**
 	 * @ejb:interface-method view-type="local"
 	 **/
 	public abstract void setAdjVoidPosted(byte ADJ_VD_PSTD);


    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="ADJ_VOID"
     **/
     public abstract byte getAdjVoid();
     /**
      * @ejb:interface-method view-type="local"
      **/
     public abstract void setAdjVoid(byte ADJ_VOID);

     /**
      * @ejb:persistent-field
      * @ejb:interface-method view-type="local"
      *
      * @jboss:column-name name="ADJ_RFND"
      **/
      public abstract byte getAdjRefund();
      /**
       * @ejb:interface-method view-type="local"
       **/
      public abstract void setAdjRefund(byte ADJ_RFND);

      /**
       * @ejb:persistent-field
       * @ejb:interface-method view-type="local"
       *
       * @jboss:column-name name="ADJ_RFND_AMNT"
       **/
       public abstract double getAdjRefundAmount();
       /**
        * @ejb:interface-method view-type="local"
        **/
       public abstract void setAdjRefundAmount(double ADJ_RFND_AMNT);

       /**
        * @ejb:persistent-field
        * @ejb:interface-method view-type="local"
        *
        * @jboss:column-name name="ADJ_RFND_RFRNC_NMBR"
        **/
        public abstract String getAdjRefundReferenceNumber();
        /**
         * @ejb:interface-method view-type="local"
         **/
        public abstract void setAdjRefundReferenceNumber(String ADJ_RFND_RFRNC_NMBR);


     /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="ADJ_RCNCLD"
     **/
     public abstract byte getAdjReconciled();
     /**
      * @ejb:interface-method view-type="local"
      **/
     public abstract void setAdjReconciled(byte ADJ_RCNCLD);

     /**
      * @ejb:persistent-field
      * @ejb:interface-method view-type="local"
      *
      * @jboss:column-name name="ADJ_DT_RCNCLD"
      **/
     public abstract Date getAdjDateReconciled();
     /**
      * @ejb:interface-method view-type="local"
      **/
     public abstract void setAdjDateReconciled(Date ADJ_DT_RCNCLD);

     /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="ADJ_APPRVL_STATUS"
     **/
     public abstract String getAdjApprovalStatus();
     /**
      * @ejb:interface-method view-type="local"
      **/
     public abstract void setAdjApprovalStatus(String ADJ_APPRVL_STATUS);

     /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="ADJ_PSTD"
     **/
     public abstract byte getAdjPosted();
     /**
      * @ejb:interface-method view-type="local"
      **/
     public abstract void setAdjPosted(byte ADJ_PSTD);

     /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="ADJ_CRTD_BY"
     **/
     public abstract String getAdjCreatedBy();
     /**
      * @ejb:interface-method view-type="local"
      **/
     public abstract void setAdjCreatedBy(String ADJ_CRTD_BY);

    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="ADJ_DT_CRTD"
     **/
     public abstract Date getAdjDateCreated();
     /**
      * @ejb:interface-method view-type="local"
      **/
     public abstract void setAdjDateCreated(Date ADJ_DT_CRTD);

    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="ADJ_LST_MDFD_BY"
     **/
     public abstract String getAdjLastModifiedBy();
     /**
      * @ejb:interface-method view-type="local"
      **/
     public abstract void setAdjLastModifiedBy(String ADJ_LST_MDFD_BY);

    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="ADJ_DT_LST_MDFD"
     **/
     public abstract Date getAdjDateLastModified();
     /**
      * @ejb:interface-method view-type="local"
      **/
     public abstract void setAdjDateLastModified(Date ADJ_DT_LST_MDFD);

    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="ADJ_APPRVD_RJCTD_BY"
     **/
     public abstract String getAdjApprovedRejectedBy();
     /**
      * @ejb:interface-method view-type="local"
      **/
     public abstract void setAdjApprovedRejectedBy(String ADJ_APPRVD_RJCTD_BY);

    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="ADJ_DT_APPRVD_RJCTD"
     **/
     public abstract Date getAdjDateApprovedRejected();
     /**
      * @ejb:interface-method view-type="local"
      **/
     public abstract void setAdjDateApprovedRejected(Date ADJ_DT_APPRVD_RJCTD);

    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="ADJ_PSTD_BY"
     **/
     public abstract String getAdjPostedBy();
     /**
      * @ejb:interface-method view-type="local"
      **/
     public abstract void setAdjPostedBy(String ADJ_PSTD_BY);

    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="ADJ_DT_PSTD"
     **/
     public abstract Date getAdjDatePosted();
     /**
      * @ejb:interface-method view-type="local"
      **/
     public abstract void setAdjDatePosted(Date ADJ_DT_PSTD);


     /**
      * @ejb:persistent-field
      * @ejb:interface-method view-type="local"
      *
      * @jboss:column-name name="ADJ_PRNTD"
      **/
      public abstract byte getAdjPrinted();
      /**
       * @ejb:interface-method view-type="local"
       **/
      public abstract void setAdjPrinted(byte ADJ_PRNTD);




     /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="ADJ_RSN_FR_RJCTN"
     **/
     public abstract String getAdjReasonForRejection();
     /**
      * @ejb:interface-method view-type="local"
      **/
     public abstract void setAdjReasonForRejection(String ADJ_RSN_FR_RJCTN);

     /**
      * @ejb:persistent-field
      * @ejb:interface-method view-type="local"
      *
      * @jboss:column-name name="ADJ_AD_BRNCH"
      **/
      public abstract Integer getAdjAdBranch();
      /**
       * @ejb:interface-method view-type="local"
       **/
      public abstract void setAdjAdBranch(Integer ADJ_AD_BRNCH);

     /**
      * @ejb:persistent-field
      * @ejb:interface-method view-type="local"
      *
      * @jboss:column-name name="ADJ_AD_CMPNY"
      **/
      public abstract Integer getAdjAdCompany();
      /**
       * @ejb:interface-method view-type="local"
       **/
      public abstract void setAdjAdCompany(Integer ADJ_AD_CMPNY);


    // RELATIONSHIP METHODS

     /**
      * @ejb:interface-method view-type="local"
      * @ejb:relation name="adjustment-cmdistributionrecords"
      *               role-name="adjustment-has-many-cmdistributionrecords"
      *
      */
     public abstract Collection getCmDistributionRecords();
     public abstract void setCmDistributionRecords(Collection cmDistributionRecords);


     /**
      * @ejb:interface-method view-type="local"
      * @ejb:relation name="adjustment-appliedcredits"
      *               role-name="adjustment-has-many-appliedcredits"
      *
      */
     public abstract Collection getArAppliedCredits();
     public abstract void setArAppliedCredits(Collection arAppliedCredits);


     /**
      * @ejb:interface-method view-type="local"
      * @ejb:relation name="bankaccount-adjustments"
      *               role-name="adjustment-has-one-bankaccount"
      *               cascade-delete="no"
      *
      * @jboss:relation related-pk-field="baCode"
      *                 fk-column="AD_BANK_ACCOUNT"
      */
     public abstract com.ejb.ad.LocalAdBankAccount getAdBankAccount();
     public abstract void setAdBankAccount(com.ejb.ad.LocalAdBankAccount adBankAccount);


     /**
      * @ejb:interface-method view-type="local"
      * @ejb:relation name="customer-adjustments"
      *               role-name="adjustment-has-one-customer"
      *               cascade-delete="no"
      *
      * @jboss:relation related-pk-field="cstCode"
      *                 fk-column="AR_CUSTOMER"
      */
     public abstract com.ejb.ar.LocalArCustomer getArCustomer();
     public abstract void setArCustomer(com.ejb.ar.LocalArCustomer arCustomer);


     /**
      * @ejb:interface-method view-type="local"
      * @ejb:relation name="salesorder-adjustments"
      *               role-name="adjustment-has-one-salesorder"
      *               cascade-delete="no"
      *
      * @jboss:relation related-pk-field="soCode"
      *                 fk-column="AR_SALES_ORDER"
      */
     public abstract com.ejb.ar.LocalArSalesOrder getArSalesOrder();
     /**
 	 * @ejb:interface-method view-type="local"
 	 **/
     public abstract void setArSalesOrder(com.ejb.ar.LocalArSalesOrder arSalesOrder);
     
     /**
      * @ejb:interface-method view-type="local"
      * @ejb:relation name="joborder-adjustments"
      *               role-name="adjustment-has-one-joborder"
      *               cascade-delete="no"
      *
      * @jboss:relation related-pk-field="joCode"
      *                 fk-column="AR_JOB_ORDER"
      */
     public abstract com.ejb.ar.LocalArJobOrder getArJobOrder();
     /**
 	 * @ejb:interface-method view-type="local"
 	 **/
     public abstract void setArJobOrder(com.ejb.ar.LocalArJobOrder arJobOrder);



     /**
 	 * @ejb:interface-method view-type="local"
 	 * @ejb:relation name="payrollperiod-adjustments"
 	 *               role-name="adjustment-has-one-payrollperiod"
 	 *               cascade-delete="no"
 	 *
 	 * @jboss:relation related-pk-field="ppCode"
 	 *                 fk-column="HR_PAYROLL_PERIOD"
 	 */
 	public abstract LocalHrPayrollPeriod getHrPayrollPeriod();
 	/**
 	 * @ejb:interface-method view-type="local"
 	 **/
 	public abstract void setHrPayrollPeriod(LocalHrPayrollPeriod hrPayrollPeriod);


    /**
     * @jboss:dynamic-ql
     */
     public abstract Collection ejbSelectGeneric(java.lang.String jbossQl, java.lang.Object[] args)
         throws FinderException;


     // BUSINESS METHODS

     /**
      * @ejb:home-method view-type="local"
      */
     public Collection ejbHomeGetAdjByCriteria(java.lang.String jbossQl, java.lang.Object[] args)
        throws FinderException {

        return ejbSelectGeneric(jbossQl, args);
     }

     /**
      * @ejb:interface-method view-type="local"
      **/
     public void addArAppliedCredit(LocalArAppliedCredit arAppliedCredit) {

          Debug.print("CmAdjustmentBean addArAppliedCredit");

          try {

             Collection arAppliedCredits = getArAppliedCredits();
             arAppliedCredits.add(arAppliedCredit);

          } catch (Exception ex) {

            throw new EJBException(ex.getMessage());

          }
     }

     /**
      * @ejb:interface-method view-type="local"
      **/
     public void dropArAppliedCredit(LocalArAppliedCredit arAppliedCredit) {

          Debug.print("CmAdjustmentBean dropArAppliedCredit");

          try {

             Collection arAppliedCredits = getArAppliedCredits();
             arAppliedCredits.remove(arAppliedCredit);

          } catch (Exception ex) {

            throw new EJBException(ex.getMessage());

          }

     }


     /**
      * @ejb:interface-method view-type="local"
      **/
     public void addCmDistributionRecord(LocalCmDistributionRecord cmDistributionRecord) {

          Debug.print("CmAdjustmentBean addCmDistributionRecord");

          try {

             Collection cmDistributionRecords = getCmDistributionRecords();
	         cmDistributionRecords.add(cmDistributionRecord);

          } catch (Exception ex) {

            throw new EJBException(ex.getMessage());

          }
     }

     /**
      * @ejb:interface-method view-type="local"
      **/
     public void dropCmDistributionRecord(LocalCmDistributionRecord cmDistributionRecord) {

          Debug.print("CmAdjustmentBean dropCmDistributionRecord");

          try {

             Collection cmDistributionRecords = getCmDistributionRecords();
	         cmDistributionRecords.remove(cmDistributionRecord);

          } catch (Exception ex) {

            throw new EJBException(ex.getMessage());

          }

     }

    /**
     * @ejb:interface-method view-type="local"
     **/
    public short getCmDrNextLine() {

       Debug.print("CmAdjustmentBean getCmDrNextLine");

       try {

          Collection cmDistributionRecords = getCmDistributionRecords();
	      return (short)(cmDistributionRecords.size() + 1);

       } catch (Exception ex) {

          throw new EJBException(ex.getMessage());

       }
    }


     // ENTITY METHODS

     /**
      * @ejb:create-method view-type="local"
      **/
     public Integer ejbCreate(Integer ADJ_CODE, String ADJ_TYP, Date ADJ_DT,
     	 String ADJ_DCMNT_NMBR, String ADJ_RFRNC_NMBR, String ADJ_CHCK_NMBR, double ADJ_AMNT, double ADJ_AMNT_APPLD, Date ADJ_CNVRSN_DT,
         double ADJ_CNVRSN_RT, String ADJ_MM,

         String ADJ_VD_APPRVL_STATUS, byte ADJ_VD_PSTD, byte ADJ_VOID,

         byte ADJ_RFND,double ADJ_RFND_AMNT, String ADJ_RFND_RFRNC_NMBR,
         byte ADJ_RCNCLD, Date ADJ_DT_RCNCLD,
         String ADJ_APPRVL_STATUS, byte ADJ_PSTD,
         String ADJ_CRTD_BY, Date ADJ_DT_CRTD, String ADJ_LST_MDFD_BY,
         Date ADJ_DT_LST_MDFD, String ADJ_APPRVD_RJCTD_BY,
         Date ADJ_DT_APPRVD_RJCTD, String ADJ_PSTD_BY, Date ADJ_DT_PSTD, String ADJ_RSN_FR_RJCTN,
		 Integer ADJ_AD_BRNCH, Integer ADJ_AD_CMPNY)
         throws CreateException {

         Debug.print("CmAdjustmentBean ejbCreate");

         setAdjCode(ADJ_CODE);
         setAdjType(ADJ_TYP);
         setAdjDate(ADJ_DT);
         setAdjDocumentNumber(ADJ_DCMNT_NMBR);
         setAdjReferenceNumber(ADJ_RFRNC_NMBR);
         setAdjCheckNumber(ADJ_CHCK_NMBR);
         setAdjAmount(ADJ_AMNT);
         setAdjAmountApplied(ADJ_AMNT_APPLD);
         setAdjConversionDate(ADJ_CNVRSN_DT);
         setAdjConversionRate(ADJ_CNVRSN_RT);
         setAdjMemo(ADJ_MM);
         setAdjVoidApprovalStatus(ADJ_VD_APPRVL_STATUS);
         setAdjVoidPosted(ADJ_VD_PSTD);
         setAdjVoid(ADJ_VOID);
         setAdjRefund(ADJ_RFND);
         setAdjReconciled(ADJ_RCNCLD);
         setAdjDateReconciled(ADJ_DT_RCNCLD);
         setAdjApprovalStatus(ADJ_APPRVL_STATUS);
         setAdjPosted(ADJ_PSTD);
         setAdjCreatedBy(ADJ_CRTD_BY);
         setAdjDateCreated(ADJ_DT_CRTD);
         setAdjLastModifiedBy(ADJ_LST_MDFD_BY);
         setAdjDateLastModified(ADJ_DT_LST_MDFD);
         setAdjApprovedRejectedBy(ADJ_APPRVD_RJCTD_BY);
         setAdjDateApprovedRejected(ADJ_DT_APPRVD_RJCTD);
         setAdjPostedBy(ADJ_PSTD_BY);
         setAdjDatePosted(ADJ_DT_PSTD);
         setAdjReasonForRejection(ADJ_RSN_FR_RJCTN);
         setAdjAdBranch(ADJ_AD_BRNCH);
         setAdjAdCompany(ADJ_AD_CMPNY);

         return null;

     }

     /**
      * @ejb:create-method view-type="local"
      **/
     public Integer ejbCreate(String ADJ_TYP, Date ADJ_DT,
     	 String ADJ_DCMNT_NMBR, String ADJ_RFRNC_NMBR, String ADJ_CHCK_NMBR, double ADJ_AMNT, double ADJ_AMNT_APPLD, Date ADJ_CNVRSN_DT,
         double ADJ_CNVRSN_RT, String ADJ_MM,

         String ADJ_VD_APPRVL_STATUS, byte ADJ_VD_PSTD, byte ADJ_VOID,

         byte ADJ_RFND,double ADJ_RFND_AMNT, String ADJ_RFND_RFRNC_NMBR,
         byte ADJ_RCNCLD, Date ADJ_DT_RCNCLD,
         String ADJ_APPRVL_STATUS, byte ADJ_PSTD,
         String ADJ_CRTD_BY, Date ADJ_DT_CRTD, String ADJ_LST_MDFD_BY,
         Date ADJ_DT_LST_MDFD, String ADJ_APPRVD_RJCTD_BY,
         Date ADJ_DT_APPRVD_RJCTD, String ADJ_PSTD_BY, Date ADJ_DT_PSTD, String ADJ_RSN_FR_RJCTN,
		 Integer ADJ_AD_BRNCH, Integer ADJ_AD_CMPNY)
         throws CreateException {

         Debug.print("CmAdjustmentBean ejbCreate");

         setAdjType(ADJ_TYP);
         setAdjDate(ADJ_DT);
         setAdjDocumentNumber(ADJ_DCMNT_NMBR);
         setAdjReferenceNumber(ADJ_RFRNC_NMBR);
         setAdjCheckNumber(ADJ_CHCK_NMBR);
         setAdjAmount(ADJ_AMNT);
         setAdjAmountApplied(ADJ_AMNT_APPLD);
         setAdjConversionDate(ADJ_CNVRSN_DT);
         setAdjConversionRate(ADJ_CNVRSN_RT);
         setAdjMemo(ADJ_MM);
         setAdjVoidApprovalStatus(ADJ_VD_APPRVL_STATUS);
         setAdjVoidPosted(ADJ_VD_PSTD);
         setAdjVoid(ADJ_VOID);
         setAdjRefund(ADJ_RFND);
         setAdjReconciled(ADJ_RCNCLD);
         setAdjDateReconciled(ADJ_DT_RCNCLD);
         setAdjApprovalStatus(ADJ_APPRVL_STATUS);
         setAdjPosted(ADJ_PSTD);
         setAdjCreatedBy(ADJ_CRTD_BY);
         setAdjDateCreated(ADJ_DT_CRTD);
         setAdjLastModifiedBy(ADJ_LST_MDFD_BY);
         setAdjDateLastModified(ADJ_DT_LST_MDFD);
         setAdjApprovedRejectedBy(ADJ_APPRVD_RJCTD_BY);
         setAdjDateApprovedRejected(ADJ_DT_APPRVD_RJCTD);
         setAdjPostedBy(ADJ_PSTD_BY);
         setAdjDatePosted(ADJ_DT_PSTD);
         setAdjReasonForRejection(ADJ_RSN_FR_RJCTN);
         setAdjAdBranch(ADJ_AD_BRNCH);
         setAdjAdCompany(ADJ_AD_CMPNY);

         return null;

     }


     public void ejbPostCreate(Integer ADJ_CODE, String ADJ_TYP, Date ADJ_DT,
     	 String ADJ_DCMNT_NMBR, String ADJ_RFRNC_NMBR, String ADJ_CHCK_NMBR, double ADJ_AMNT, double ADJ_AMNT_APPLD, Date ADJ_CNVRSN_DT,
         double ADJ_CNVRSN_RT, String ADJ_MM,

         String ADJ_VD_APPRVL_STATUS, byte ADJ_VD_PSTD, byte ADJ_VOID,

         byte ADJ_RFND,double ADJ_RFND_AMNT, String ADJ_RFND_RFRNC_NMBR,
         byte ADJ_RCNCLD, Date ADJ_DT_RCNCLD,
         String ADJ_APPRVL_STATUS, byte ADJ_PSTD,
         String ADJ_CRTD_BY, Date ADJ_DT_CRTD, String ADJ_LST_MDFD_BY,
         Date ADJ_DT_LST_MDFD, String ADJ_APPRVD_RJCTD_BY,
         Date ADJ_DT_APPRVD_RJCTD, String ADJ_PSTD_BY, Date ADJ_DT_PSTD, String ADJ_RSN_FR_RJCTN,
		 Integer ADJ_AD_BRNCH, Integer ADJ_AD_CMPNY)
         throws CreateException { }

     public void ejbPostCreate(String ADJ_TYP, Date ADJ_DT,
     	 String ADJ_DCMNT_NMBR, String ADJ_RFRNC_NMBR, String ADJ_CHCK_NMBR, double ADJ_AMNT, double ADJ_AMNT_APPLD, Date ADJ_CNVRSN_DT,
         double ADJ_CNVRSN_RT,
         String ADJ_MM,

         String ADJ_VD_APPRVL_STATUS, byte ADJ_VD_PSTD,
         byte ADJ_VOID, byte ADJ_RFND,
         double ADJ_RFND_AMNT, String ADJ_RFND_RFRNC_NMBR,
         byte ADJ_RCNCLD, Date ADJ_DT_RCNCLD,
         String ADJ_APPRVL_STATUS, byte ADJ_PSTD,

         String ADJ_CRTD_BY, Date ADJ_DT_CRTD,
         String ADJ_LST_MDFD_BY, Date ADJ_DT_LST_MDFD,

         String ADJ_APPRVD_RJCTD_BY, Date ADJ_DT_APPRVD_RJCTD,
         String ADJ_PSTD_BY, Date ADJ_DT_PSTD, String ADJ_RSN_FR_RJCTN,
		 Integer ADJ_AD_BRNCH, Integer ADJ_AD_CMPNY)
         throws CreateException { }

}

