/*
 * com/ejb/cm/LocalCmDistributionRecordBean.java
 *
 * Created on October 23, 2003, 03:44 PM
 */

package com.ejb.cm;

import javax.ejb.CreateException;

import com.util.AbstractEntityBean;
import com.util.Debug;

/**
 *
 * @author  Dennis Hilario
 *
 * @ejb:bean name="CmDistributionRecordEJB"
 *           display-name="Distribution Record Entity"
 *           type="CMP"
 *           cmp-version="2.x"
 *           view-type="local"
 *           local-jndi-name="omega-ejb/CmDistributionRecordEJB"
 *           schema="CmDistributionRecord"
 *           primkey-field="drCode"
 *
 * @ejb:pk class="java.lang.Integer"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="cmuser"
 *                        role-link="cmuserlink"
 *
 * @ejb:permission role-name="cmuser"
 *
 * @ejb:interface local-class="com.ejb.cm.LocalCmDistributionRecord"
 *                local-extends="javax.ejb.EJBLocalObject"
 *
 * @ejb:home local-class="com.ejb.cm.LocalCmDistributionRecordHome"
 *           local-extends="javax.ejb.EJBLocalHome"
 *
 * @ejb:finder signature="Collection findByDrReversalAndDrImportedAndFtCode(byte DR_RVRSL, byte DR_IMPRTD, java.lang.Integer FT_CODE, java.lang.Integer DR_AD_CMPNY)"
 *             query="SELECT OBJECT(dr) FROM CmFundTransfer ft, IN(ft.cmDistributionRecords) dr WHERE dr.drReversal=?1  AND  dr.drImported=?2 AND ft.ftCode=?3 AND dr.drAdCompany = ?4"
 *
 * @ejb:finder signature="Collection findByDrReversalAndDrImportedAndAdjCode(byte DR_RVRSL, byte DR_IMPRTD, java.lang.Integer ADJ_CODE, java.lang.Integer DR_AD_CMPNY)"
 *             query="SELECT OBJECT(dr) FROM CmAdjustment adj, IN(adj.cmDistributionRecords) dr WHERE dr.drReversal=?1  AND  dr.drImported=?2 AND adj.adjCode=?3 AND dr.drAdCompany = ?4"
 *
 * @ejb:finder signature="Collection findByFtCode(java.lang.Integer FT_CODE, java.lang.Integer DR_AD_CMPNY)"
 *             query="SELECT OBJECT(dr) FROM CmFundTransfer ft, IN(ft.cmDistributionRecords) dr WHERE ft.ftCode=?1 AND dr.drAdCompany = ?2"
 *
 * @jboss:query signature="Collection findByFtCode(java.lang.Integer FT_CODE, java.lang.Integer DR_AD_CMPNY)"
 *             query="SELECT OBJECT(dr) FROM CmFundTransfer ft, IN(ft.cmDistributionRecords) dr WHERE ft.ftCode=?1 AND dr.drAdCompany = ?2 ORDER BY dr.drDebit DESC"
 *
 * @ejb:finder signature="Collection findByAdjCode(java.lang.Integer ADJ_CODE, java.lang.Integer DR_AD_CMPNY)"
 *             query="SELECT OBJECT(dr) FROM CmAdjustment adj, IN(adj.cmDistributionRecords) dr WHERE adj.adjCode=?1 AND dr.drAdCompany = ?2"
 *
 * @jboss:query signature="Collection findByAdjCode(java.lang.Integer ADJ_CODE, java.lang.Integer DR_AD_CMPNY)"
 *             query="SELECT OBJECT(dr) FROM CmAdjustment adj, IN(adj.cmDistributionRecords) dr WHERE adj.adjCode=?1 AND dr.drAdCompany = ?2 ORDER BY dr.drDebit DESC"
 *
 * @ejb:finder signature="Collection findUnpostedFtByDateRangeAndCoaAccountNumber(java.util.Date FT_DT_FRM, java.util.Date FT_DT_TO, java.lang.Integer COA_CODE, java.lang.Integer FT_AD_BRNCH, java.lang.Integer DR_AD_CMPNY)"
 *             query="SELECT OBJECT(dr) FROM CmFundTransfer ft, IN(ft.cmDistributionRecords) dr WHERE ft.ftVoid=0 AND ft.ftPosted=0 AND ft.ftDate>=?1 AND ft.ftDate<=?2 AND dr.glChartOfAccount.coaCode=?3 AND ft.ftAdBranch=?4 AND dr.drAdCompany=?5"
 *
 * @jboss:query signature="Collection findUnpostedFtByDateRangeAndCoaAccountNumber(java.util.Date FT_DT_FRM, java.util.Date FT_DT_TO, java.lang.Integer COA_CODE, java.lang.Integer FT_AD_BRNCH, java.lang.Integer DR_AD_CMPNY)"
 *             query="SELECT OBJECT(dr) FROM CmFundTransfer ft, IN(ft.cmDistributionRecords) dr WHERE ft.ftVoid=0 AND ft.ftPosted=0 AND ft.ftDate>=?1 AND ft.ftDate<=?2 AND dr.glChartOfAccount.coaCode=?3 AND ft.ftAdBranch=?4 AND dr.drAdCompany=?5 ORDER BY dr.drLine"
 *
 * @ejb:finder signature="Collection findUnpostedAdjByDateRangeAndCoaAccountNumber(java.util.Date ADJ_DT_FRM, java.util.Date ADJ_DT_TO, java.lang.Integer COA_CODE, java.lang.Integer ADJ_AD_BRNCH, java.lang.Integer DR_AD_CMPNY)"
 *             query="SELECT OBJECT(dr) FROM CmAdjustment adj, IN(adj.cmDistributionRecords) dr WHERE adj.adjVoid=0 AND adj.adjPosted=0 AND adj.adjDate>=?1 AND adj.adjDate<=?2 AND dr.glChartOfAccount.coaCode=?3 AND adj.adjAdBranch=?4 AND dr.drAdCompany=?5"
 *
 * @jboss:query signature="Collection findUnpostedAdjByDateRangeAndCoaAccountNumber(java.util.Date ADJ_DT_FRM, java.util.Date ADJ_DT_TO, java.lang.Integer COA_CODE, java.lang.Integer ADJ_AD_BRNCH, java.lang.Integer DR_AD_CMPNY)"
 *             query="SELECT OBJECT(dr) FROM CmAdjustment adj, IN(adj.cmDistributionRecords) dr WHERE adj.adjVoid=0 AND adj.adjPosted=0 AND adj.adjDate>=?1 AND adj.adjDate<=?2 AND dr.glChartOfAccount.coaCode=?3 AND adj.adjAdBranch=?4 AND dr.drAdCompany=?5 ORDER BY dr.drLine"
 *   
 * @ejb:finder signature="Collection findUnpostedAdjByDrCoaAccountNumberAndBrCode(java.lang.Integer COA_CODE, java.lang.Integer ADJ_AD_BRNCH, java.lang.Integer DR_AD_CMPNY)"
 *             query="SELECT OBJECT(dr) FROM CmAdjustment adj, IN(adj.cmDistributionRecords) dr WHERE adj.adjPosted=0 AND dr.glChartOfAccount.coaCode=?1 AND adj.adjAdBranch = ?2 AND dr.drAdCompany = ?3"
 *
 * @ejb:finder signature="Collection findUnpostedFtByDrCoaAccountNumberAndBrCode(java.lang.Integer COA_CODE, java.lang.Integer FT_AD_BRNCH, java.lang.Integer DR_AD_CMPNY)"
 *             query="SELECT OBJECT(dr) FROM CmFundTransfer ft, IN(ft.cmDistributionRecords) dr WHERE ft.ftPosted=0 AND dr.glChartOfAccount.coaCode=?1 AND ft.ftAdBranch = ?2 AND dr.drAdCompany = ?3"
 *
 * @ejb:finder signature="Collection findAdjByDateAndCoaAccountNumberAndCurrencyAndAdjPosted(java.util.Date ADJ_DT, java.lang.Integer COA_CODE, java.lang.Integer FC_CODE, byte ADJ_PSTD, java.lang.Integer DR_AD_CMPNY)"
 *             query="SELECT OBJECT(dr) FROM CmAdjustment adj, IN(adj.cmDistributionRecords) dr WHERE adj.adjDate<=?1 AND dr.glChartOfAccount.coaCode=?2 AND adj.adBankAccount.glFunctionalCurrency.fcCode=?3 AND adj.adjPosted=?4 AND adj.adjVoid=0 AND dr.drAdCompany=?5"
 *             
 * @jboss:query signature="Collection findAdjByDateAndCoaAccountNumberAndCurrencyAndAdjPosted(java.util.Date ADJ_DT, java.lang.Integer COA_CODE, java.lang.Integer FC_CODE, byte ADJ_PSTD, java.lang.Integer DR_AD_CMPNY)"
 *             query="SELECT OBJECT(dr) FROM CmAdjustment adj, IN(adj.cmDistributionRecords) dr WHERE adj.adjDate<=?1 AND dr.glChartOfAccount.coaCode=?2 AND adj.adBankAccount.glFunctionalCurrency.fcCode=?3 AND adj.adjPosted=?4 AND adj.adjVoid=0 AND dr.drAdCompany=?5 ORDER BY adj.adjDate, dr.drLine"
 * 
 * @ejb:finder signature="Collection findFtByDateAndCoaAccountNumberAndCurrencyAndFtPosted(java.util.Date FT_DT, java.lang.Integer COA_CODE, java.lang.Integer FC_CODE, byte FT_PSTD, java.lang.Integer DR_AD_CMPNY)"
 *             query="SELECT OBJECT(dr) FROM CmFundTransfer ft, IN(ft.cmDistributionRecords) dr, AdBankAccount ba WHERE ft.ftDate<=?1 AND dr.glChartOfAccount.coaCode=?2 AND ft.ftAdBaAccountFrom=ba.baCode AND ba.glFunctionalCurrency.fcCode=?3 AND ft.ftPosted=?4 AND ft.ftVoid=0 AND dr.drAdCompany=?5"
 *             
 * @jboss:query signature="Collection findFtByDateAndCoaAccountNumberAndCurrencyAndFtPosted(java.util.Date FT_DT, java.lang.Integer COA_CODE, java.lang.Integer FC_CODE, byte FT_PSTD, java.lang.Integer DR_AD_CMPNY)"
 *             query="SELECT OBJECT(dr) FROM CmFundTransfer ft, IN(ft.cmDistributionRecords) dr, AdBankAccount ba WHERE ft.ftDate<=?1 AND dr.glChartOfAccount.coaCode=?2 AND ft.ftAdBaAccountFrom=ba.baCode AND ba.glFunctionalCurrency.fcCode=?3 AND ft.ftPosted=?4 AND ft.ftVoid=0 AND dr.drAdCompany=?5 ORDER BY ft.ftDate, dr.drLine"
 * 
 * @ejb:finder signature="Collection findUnpostedFtByDateAndCoaAccountNumber(java.util.Date FT_DT_TO, java.lang.Integer COA_CODE, java.lang.Integer FT_AD_BRNCH, java.lang.Integer DR_AD_CMPNY)"
 *             query="SELECT OBJECT(dr) FROM CmFundTransfer ft, IN(ft.cmDistributionRecords) dr WHERE ft.ftVoid=0 AND ft.ftPosted=0 AND ft.ftDate<=?1 AND dr.glChartOfAccount.coaCode=?2 AND ft.ftAdBranch=?3 AND dr.drAdCompany=?4"
 *
 * @jboss:query signature="Collection findUnpostedFtByDateAndCoaAccountNumber(java.util.Date FT_DT_TO, java.lang.Integer COA_CODE, java.lang.Integer FT_AD_BRNCH, java.lang.Integer DR_AD_CMPNY)"
 *             query="SELECT OBJECT(dr) FROM CmFundTransfer ft, IN(ft.cmDistributionRecords) dr WHERE ft.ftVoid=0 AND ft.ftPosted=0 AND ft.ftDate<=?1 AND dr.glChartOfAccount.coaCode=?2 AND ft.ftAdBranch=?3 AND dr.drAdCompany=?4 ORDER BY dr.drLine"
 *
 * @ejb:finder signature="Collection findUnpostedAdjByDateAndCoaAccountNumber(java.util.Date ADJ_DT_TO, java.lang.Integer COA_CODE, java.lang.Integer ADJ_AD_BRNCH, java.lang.Integer DR_AD_CMPNY)"
 *             query="SELECT OBJECT(dr) FROM CmAdjustment adj, IN(adj.cmDistributionRecords) dr WHERE adj.adjVoid=0 AND adj.adjPosted=0 AND adj.adjDate<=?1 AND dr.glChartOfAccount.coaCode=?2 AND adj.adjAdBranch=?3 AND dr.drAdCompany=?4"
 *
 * @jboss:query signature="Collection findUnpostedAdjByDateAndCoaAccountNumber(java.util.Date ADJ_DT_TO, java.lang.Integer COA_CODE, java.lang.Integer ADJ_AD_BRNCH, java.lang.Integer DR_AD_CMPNY)"
 *             query="SELECT OBJECT(dr) FROM CmAdjustment adj, IN(adj.cmDistributionRecords) dr WHERE adj.adjVoid=0 AND adj.adjPosted=0 AND adj.adjDate<=?1 AND dr.glChartOfAccount.coaCode=?2 AND adj.adjAdBranch=?3 AND dr.drAdCompany=?4 ORDER BY dr.drLine"
 * 
 * @ejb:value-object match="*"
 *             name="CmDistributionRecord"
 *
 * @jboss:persistence table-name="CM_DSTRBTN_RCRD"
 *
 * @jboss:entity-command name="mysql-get-generated-keys"
 */

public abstract class CmDistributionRecordBean extends AbstractEntityBean {
    
    
    // PERSISTENT METHODS
    
    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     * @ejb:pk-field
     *
     * @jboss:column-name name="DR_CODE"
     *
     * @jboss:persistence auto-increment="true"
     **/
    public abstract Integer getDrCode();
    public abstract void setDrCode(Integer DR_CODE);
     
    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="DR_LN"
     **/
     public abstract short getDrLine();
     /**
      * @ejb:interface-method view-type="local"
      **/         
     public abstract void setDrLine(short DR_LN);

    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="DR_CLSS"
     **/
     public abstract String getDrClass();    
     /**
      * @ejb:interface-method view-type="local"
      **/     
     public abstract void setDrClass(String DR_CLSS);   

    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="DR_AMNT"
     **/
     public abstract double getDrAmount();    
     /**
      * @ejb:interface-method view-type="local"
      **/ 
     public abstract void setDrAmount(double DR_AMNT);     
     
    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="DR_DBT"
     **/
     public abstract byte getDrDebit();    
     /**
      * @ejb:interface-method view-type="local"
      **/     
     public abstract void setDrDebit(byte DR_DBT);
     
    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="DR_RVRSL"
     **/
     public abstract byte getDrReversal();    
     /**
      * @ejb:interface-method view-type="local"
      **/     
     public abstract void setDrReversal(byte DR_RVRSL);     
     
    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="DR_IMPRTD"
     **/
     public abstract byte getDrImported();    
     /**
      * @ejb:interface-method view-type="local"
      **/     
     public abstract void setDrImported(byte DR_IMPRTD);          
       
     /**
      * @ejb:persistent-field
      * @ejb:interface-method view-type="local"
      *
      * @jboss:column-name name="DR_AD_CMPNY"
      **/
      public abstract Integer getDrAdCompany();    
      /**
       * @ejb:interface-method view-type="local"
       **/     
      public abstract void setDrAdCompany(Integer DR_AD_CMPNY);

     
     // RELATIONSHIP METHODS    
     
     /**
      * @ejb:interface-method view-type="local"
      * @ejb:relation name="fundtransfer-cmdistributionrecords"
      *               role-name="cmdistributionrecord-has-one-fundtransfer"
      *				  cascade-delete="yes"
      *
      * @jboss:relation related-pk-field="ftCode"
      *                 fk-column="CM_FUND_TRANSFER"
      */
     public abstract LocalCmFundTransfer getCmFundTransfer();
     public abstract void setCmFundTransfer(LocalCmFundTransfer cmFundTransfer);

     /**
      * @ejb:interface-method view-type="local"
      * @ejb:relation name="adjustment-cmdistributionrecords"
      *               role-name="cmdistributionrecord-has-one-adjustment"
      *				  cascade-delete="yes"
      * @jboss:relation related-pk-field="adjCode"
      *                 fk-column="CM_ADJUSTMENT"
      */
     public abstract LocalCmAdjustment getCmAdjustment();
     public abstract void setCmAdjustment(LocalCmAdjustment cmAdjustment);
     
     /**
      * @ejb:interface-method view-type="local"
      * @ejb:relation name="chartofaccount-cmdistributionrecords"
      *               role-name="cmdistributionrecord-has-one-chartofaccount"
      *
      * @jboss:relation related-pk-field="coaCode"
      *                 fk-column="GL_CHART_OF_ACCOUNT"
      */
     public abstract com.ejb.gl.LocalGlChartOfAccount getGlChartOfAccount();
     public abstract void setGlChartOfAccount(com.ejb.gl.LocalGlChartOfAccount glChartOfAccount);
                       
     // ENTITY METHODS
    
     /**
      * @ejb:create-method view-type="local"
      **/
     public Integer ejbCreate(Integer DR_CODE, short DR_LN, String DR_CLSS, 
         double DR_AMNT, byte DR_DBT, byte DR_RVRSL, byte DR_IMPRTD, Integer DR_AD_CMPNY)
         throws CreateException {
           
         Debug.print("CmDistributionRecordBean ejbCreate");
        
         setDrLine(DR_LN);
         setDrClass(DR_CLSS);
         setDrAmount(DR_AMNT);
         setDrDebit(DR_DBT);
         setDrReversal(DR_RVRSL);
         setDrImported(DR_IMPRTD);
         setDrAdCompany(DR_AD_CMPNY);
                
         return null;
        
     }
    
     /**
      * @ejb:create-method view-type="local"
      **/
     public Integer ejbCreate(short DR_LN, String DR_CLSS,
         double DR_AMNT, byte DR_DBT, byte DR_RVRSL, byte DR_IMPRTD, Integer DR_AD_CMPNY)
         throws CreateException {
           
         Debug.print("CmDistributionRecordBean ejbCreate");
        
         setDrLine(DR_LN);
         setDrClass(DR_CLSS);
         setDrAmount(DR_AMNT);
         setDrDebit(DR_DBT);
         setDrReversal(DR_RVRSL);
         setDrImported(DR_IMPRTD);  
         setDrAdCompany(DR_AD_CMPNY);
        
         return null;
        
     }
    
    
     public void ejbPostCreate(Integer DR_CODE, short DR_LN, String DR_CLSS,
         double DR_AMNT, byte DR_DBT, byte DR_RVRSL, byte DR_IMPRTD, Integer DR_AD_CMPNY)
         throws CreateException { }
   
     public void ejbPostCreate(short DR_LN, String DR_CLSS,
         double DR_AMNT, byte DR_DBT, byte DR_RVRSL, byte DR_IMPRTD, Integer DR_AD_CMPNY)
         throws CreateException { }
        
}

