/*
 * com/ejb/ap/LocalCmFundTransferBean.java
 *
 * Created on October 23, 2003, 03:29 PM
 */

package com.ejb.cm;

import java.util.Collection;
import java.util.Date;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;

import com.util.AbstractEntityBean;
import com.util.Debug;

/**
 *
 * @author  Dennis M. Hilario
 *
 * @ejb:bean name="CmFundTransferEJB"
 *           display-name="Fund Transfer Entity"
 *           type="CMP"
 *           cmp-version="2.x"
 *           view-type="local"
 *           local-jndi-name="omega-ejb/CmFundTransferEJB"
 *           schema="CmFundTransfer"
 *           primkey-field="ftCode"
 *
 * @ejb:pk class="java.lang.Integer"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="cmuser"
 *                        role-link="cmuserlink"
 *
 * @ejb:permission role-name="cmuser"
 *
 * @ejb:interface local-class="com.ejb.cm.LocalCmFundTransfer"
 *                local-extends="javax.ejb.EJBLocalObject"
 *
 * @ejb:home local-class="com.ejb.cm.LocalCmFundTransferHome"
 *           local-extends="javax.ejb.EJBLocalHome"
 *
 * @ejb:finder signature="Collection findUnreconciledPostedFtAccountToByDateAndBaCodeAndBrCode(java.util.Date DT, java.lang.Integer BA_CODE, java.lang.Integer FT_AD_BRNCH, java.lang.Integer FT_AD_CMPNY)"
 *             query="SELECT OBJECT(ft) FROM CmFundTransfer ft WHERE ft.ftDate <= ?1 AND ft.ftAdBaAccountTo = ?2 AND ft.ftAccountToReconciled = 0 AND ft.ftPosted = 1 AND ft.ftVoid = 0 AND ft.ftAdBranch = ?3 AND ft.ftAdCompany = ?4"
 *
 * @jboss:query signature="Collection findUnreconciledPostedFtAccountToByDateAndBaCodeAndBrCode(java.util.Date DT, java.lang.Integer BA_CODE, java.lang.Integer FT_AD_BRNCH, java.lang.Integer FT_AD_CMPNY)"
 *             query="SELECT OBJECT(ft) FROM CmFundTransfer ft WHERE ft.ftDate <= ?1 AND ft.ftAdBaAccountTo = ?2 AND ft.ftAccountToReconciled = 0 AND ft.ftPosted = 1 AND ft.ftVoid = 0 AND ft.ftAdBranch = ?3 AND ft.ftAdCompany = ?4 ORDER BY ft.ftDate"
 *
 * @ejb:finder signature="Collection findUnreconciledPostedFtAccountToByDateAndBaCode(java.util.Date DT, java.lang.Integer BA_CODE, java.lang.Integer FT_AD_CMPNY)"
 *             query="SELECT OBJECT(ft) FROM CmFundTransfer ft WHERE ft.ftDate <= ?1 AND ft.ftAdBaAccountTo = ?2 AND ft.ftAccountToReconciled = 0 AND ft.ftPosted = 1 AND ft.ftVoid = 0 AND ft.ftAdCompany = ?3"
 *
 * @ejb:finder signature="Collection findPostedFtAccountToByDateAndBaCode(java.util.Date DT, java.lang.Integer BA_CODE, java.lang.Integer FT_AD_CMPNY)"
 *             query="SELECT OBJECT(ft) FROM CmFundTransfer ft WHERE ft.ftDate <= ?1 AND ft.ftAdBaAccountTo = ?2 AND ft.ftPosted = 1 AND ft.ftVoid = 0 AND ft.ftAdCompany = ?3"
 *
 *
 * @jboss:query signature="Collection findUnreconciledPostedFtAccountToByDateAndBaCode(java.util.Date DT, java.lang.Integer BA_CODE, java.lang.Integer FT_AD_CMPNY)"
 *             query="SELECT OBJECT(ft) FROM CmFundTransfer ft WHERE ft.ftDate <= ?1 AND ft.ftAdBaAccountTo = ?2 AND ft.ftAccountToReconciled = 0 AND ft.ftPosted = 1 AND ft.ftVoid = 0 AND ft.ftAdCompany = ?3 ORDER BY ft.ftDate"
 *
 * @ejb:finder signature="Collection findUnreconciledPostedFtAccountFromByDateAndBaCodeAndBrCode(java.util.Date DT, java.lang.Integer BA_CODE, java.lang.Integer FT_AD_BRNCH, java.lang.Integer FT_AD_CMPNY)"
 *             query="SELECT OBJECT(ft) FROM CmFundTransfer ft WHERE ft.ftDate <= ?1 AND ft.ftAdBaAccountFrom = ?2 AND ft.ftAccountFromReconciled = 0 AND ft.ftPosted = 1 AND ft.ftVoid = 0 AND ft.ftAdBranch = ?3 AND ft.ftAdCompany = ?4"
 *
 * @jboss:query signature="Collection findUnreconciledPostedFtAccountFromByDateAndBaCodeAndBrCode(java.util.Date DT, java.lang.Integer BA_CODE, java.lang.Integer FT_AD_BRNCH, java.lang.Integer FT_AD_CMPNY)"
 *             query="SELECT OBJECT(ft) FROM CmFundTransfer ft WHERE ft.ftDate <= ?1 AND ft.ftAdBaAccountFrom = ?2 AND ft.ftAccountFromReconciled = 0 AND ft.ftPosted = 1 AND ft.ftVoid = 0 AND ft.ftAdBranch = ?3 AND ft.ftAdCompany = ?4 ORDER BY ft.ftDate"
 *  
 * @ejb:finder signature="Collection findUnreconciledPostedFtAccountFromByDateAndBaCode(java.util.Date DT, java.lang.Integer BA_CODE, java.lang.Integer FT_AD_CMPNY)"
 *             query="SELECT OBJECT(ft) FROM CmFundTransfer ft WHERE ft.ftDate <= ?1 AND ft.ftAdBaAccountFrom = ?2 AND ft.ftAccountFromReconciled = 0 AND ft.ftPosted = 1 AND ft.ftVoid = 0 AND ft.ftAdCompany = ?3"
 *
 * @jboss:query signature="Collection findUnreconciledPostedFtAccountFromByDateAndBaCode(java.util.Date DT, java.lang.Integer BA_CODE, java.lang.Integer FT_AD_CMPNY)"
 *             query="SELECT OBJECT(ft) FROM CmFundTransfer ft WHERE ft.ftDate <= ?1 AND ft.ftAdBaAccountFrom = ?2 AND ft.ftAccountFromReconciled = 0 AND ft.ftPosted = 1 AND ft.ftVoid = 0 AND ft.ftAdCompany = ?3 ORDER BY ft.ftDate"
 *
 * @ejb:finder signature="Collection findPostedFtAccountFromByDateAndBaCode(java.util.Date DT, java.lang.Integer BA_CODE, java.lang.Integer FT_AD_CMPNY)"
 *             query="SELECT OBJECT(ft) FROM CmFundTransfer ft WHERE ft.ftDate <= ?1 AND ft.ftAdBaAccountFrom = ?2 AND ft.ftPosted = 1 AND ft.ftVoid = 0 AND ft.ftAdCompany = ?3"
 *
 *
 * @ejb:finder signature="Collection findPostedFtByFtDateRange(java.util.Date FT_DT_FRM, java.util.Date FT_DT_TO, java.lang.Integer FT_AD_CMPNY)"
 *             query="SELECT OBJECT(ft) FROM CmFundTransfer ft WHERE ft.ftPosted = 1 AND  ft.ftDate >= ?1 AND ft.ftDate <= ?2 AND ft.ftAdCompany = ?3"
 *
 * @jboss:query signature="Collection findPostedFtByFtDateRange(java.util.Date FT_DT_FRM, java.util.Date FT_DT_TO, java.lang.Integer FT_AD_CMPNY)"
 *             query="SELECT OBJECT(ft) FROM CmFundTransfer ft WHERE ft.ftPosted = 1 AND  ft.ftDate >= ?1 AND ft.ftDate <= ?2 AND ft.ftAdCompany = ?3 ORDER BY ft.ftDate"
 *
 * @ejb:finder signature="Collection findByAdBankAccountFrom(java.lang.Integer BA_CODE, java.lang.Integer FT_AD_CMPNY)"
 *             query="SELECT OBJECT(ft) FROM CmFundTransfer ft WHERE ft.ftAdBaAccountFrom =?1 AND ft.ftAdCompany = ?2"
 *
 * @ejb:finder signature="Collection findByAdBankAccountTo(java.lang.Integer BA_CODE, java.lang.Integer FT_AD_CMPNY)"
 *             query="SELECT OBJECT(ft) FROM CmFundTransfer ft WHERE ft.ftAdBaAccountTo =?1 AND ft.ftAdCompany = ?2"
 * 
 * @ejb:finder signature="Collection findDraftFtAll(java.lang.Integer FT_AD_CMPNY)"
 *             query="SELECT OBJECT(ft) FROM CmFundTransfer ft WHERE ft.ftApprovalStatus IS NULL AND ft.ftAdCompany = ?1"
 * 
 * @ejb:finder signature="Collection findDraftFtByBrCode(java.lang.Integer FT_AD_BRNCH, java.lang.Integer FT_AD_CMPNY)"
 *             query="SELECT OBJECT(ft) FROM CmFundTransfer ft WHERE ft.ftApprovalStatus IS NULL AND ft.ftAdBranch = ?1 AND ft.ftAdCompany = ?2"
 *
 * @ejb:finder signature="Collection findUnpostedCmFtByCmFtDateRange(java.util.Date FT_DT_FRM, java.util.Date FT_DT_TO, java.lang.Integer FT_AD_CMPNY)"
 *             query="SELECT OBJECT(ft) FROM CmFundTransfer ft  WHERE ft.ftPosted = 0 AND ft.ftVoid = 0 AND ft.ftDate >= ?1 AND ft.ftDate <= ?2 AND ft.ftAdCompany = ?3"
 *
 * @jboss:query signature="Collection findUnpostedCmFtByCmFtDateRange(java.util.Date FT_DT_FRM, java.util.Date FT_DT_TO, java.lang.Integer FT_AD_CMPNY)"
 *             query="SELECT OBJECT(ft) FROM CmFundTransfer ft  WHERE ft.ftPosted = 0 AND ft.ftVoid = 0 AND ft.ftDate >= ?1 AND ft.ftDate <= ?2 AND ft.ftAdCompany = ?3 ORDER BY ft.ftDate" 
 *
 * @ejb:finder signature="LocalCmFundTransfer findByFtDocumentNumberAndBrCode(java.lang.String FT_DCMNT_NMBR, java.lang.Integer FT_AD_BRNCH, java.lang.Integer FT_AD_CMPNY)"
 *             query="SELECT OBJECT(ft) FROM CmFundTransfer ft  WHERE ft.ftDocumentNumber = ?1 AND ft.ftAdBranch = ?2 AND ft.ftAdCompany = ?3"
 *
 *
 * @ejb:finder signature="Collection findPostedFtAccountToByBaCodeAndDateRangeAndBrCode(java.lang.Integer BA_CODE, java.util.Date FT_DT_FRM, java.util.Date FT_DT_TO, java.lang.Integer FT_AD_BRNCH, java.lang.Integer FT_AD_CMPNY)"
 *             query="SELECT OBJECT(ft) FROM CmFundTransfer ft WHERE ft.ftPosted = 1 AND ft.ftVoid = 0 AND ft.ftAdBaAccountTo = ?1 AND ft.ftDate >= ?2 AND ft.ftDate <= ?3 AND ft.ftAdBranch = ?4 AND ft.ftAdCompany = ?5"
 *
 * @jboss:query signature="Collection findPostedFtAccountToByBaCodeAndDateRangeAndBrCode(java.lang.Integer BA_CODE, java.util.Date FT_DT_FRM, java.util.Date FT_DT_TO, java.lang.Integer FT_AD_BRNCH, java.lang.Integer FT_AD_CMPNY)"
 *             query="SELECT OBJECT(ft) FROM CmFundTransfer ft WHERE ft.ftPosted = 1 AND ft.ftVoid = 0 AND ft.ftAdBaAccountTo = ?1 AND ft.ftDate >= ?2 AND ft.ftDate <= ?3 AND ft.ftAdBranch = ?4 AND ft.ftAdCompany = ?5 ORDER BY ft.ftDate"
 *
 * @ejb:finder signature="Collection findPostedFtAccountFromByBaCodeAndDateRangeAndBrCode(java.lang.Integer BA_CODE, java.util.Date FT_DT_FRM, java.util.Date FT_DT_TO, java.lang.Integer FT_AD_BRNCH, java.lang.Integer FT_AD_CMPNY)"
 *             query="SELECT OBJECT(ft) FROM CmFundTransfer ft WHERE ft.ftPosted = 1 AND ft.ftVoid = 0 AND ft.ftAdBaAccountFrom = ?1 AND ft.ftDate >= ?2 AND ft.ftDate <= ?3 AND ft.ftAdBranch = ?4 AND ft.ftAdCompany = ?5"
 *
 * @jboss:query signature="Collection findPostedFtAccountFromByBaCodeAndDateRangeAndBrCode(java.lang.Integer BA_CODE, java.util.Date FT_DT_FRM, java.util.Date FT_DT_TO, java.lang.Integer FT_AD_BRNCH, java.lang.Integer FT_AD_CMPNY)"
 *             query="SELECT OBJECT(ft) FROM CmFundTransfer ft WHERE ft.ftPosted = 1 AND ft.ftVoid = 0 AND ft.ftAdBaAccountFrom = ?1 AND ft.ftDate >= ?2 AND ft.ftDate <= ?3 AND ft.ftAdBranch = ?4 AND ft.ftAdCompany = ?5 ORDER BY ft.ftDate"
 *
 * @ejb:finder signature="Collection findReconciledPostedFtAccountToByBaCodeAndDateRangeAndBrCode(java.lang.Integer BA_CODE, java.util.Date FT_DT_FRM, java.util.Date FT_DT_TO, java.lang.Integer FT_AD_BRNCH, java.lang.Integer FT_AD_CMPNY)"
 *             query="SELECT OBJECT(ft) FROM CmFundTransfer ft WHERE ft.ftPosted = 1 AND ft.ftVoid = 0 AND ft.ftAccountToReconciled = 1 AND ft.ftAdBaAccountTo = ?1 AND ft.ftDate >= ?2 AND ft.ftDate <= ?3 AND ft.ftAdBranch = ?4 AND ft.ftAdCompany = ?5"
 *
 * @jboss:query signature="Collection findReconciledPostedFtAccountToByBaCodeAndDateRangeAndBrCode(java.lang.Integer BA_CODE, java.util.Date FT_DT_FRM, java.util.Date FT_DT_TO, java.lang.Integer FT_AD_BRNCH, java.lang.Integer FT_AD_CMPNY)"
 *             query="SELECT OBJECT(ft) FROM CmFundTransfer ft WHERE ft.ftPosted = 1 AND ft.ftVoid = 0 AND ft.ftAccountToReconciled = 1 AND ft.ftAdBaAccountTo = ?1 AND ft.ftDate >= ?2 AND ft.ftDate <= ?3 AND ft.ftAdBranch = ?4 AND ft.ftAdCompany = ?5 ORDER BY ft.ftDate"
 *
 * @ejb:finder signature="Collection findReconciledPostedFtAccountFromByBaCodeAndDateRangeAndBrCode(java.lang.Integer BA_CODE, java.util.Date FT_DT_FRM, java.util.Date FT_DT_TO, java.lang.Integer FT_AD_BRNCH, java.lang.Integer FT_AD_CMPNY)"
 *             query="SELECT OBJECT(ft) FROM CmFundTransfer ft WHERE ft.ftPosted = 1 AND ft.ftVoid = 0 AND ft.ftAccountFromReconciled = 1 AND ft.ftAdBaAccountFrom = ?1 AND ft.ftDate >= ?2 AND ft.ftDate <= ?3 AND ft.ftAdBranch = ?4 AND ft.ftAdCompany = ?5"
 *
 * @jboss:query signature="Collection findReconciledPostedFtAccountFromByBaCodeAndDateRangeAndBrCode(java.lang.Integer BA_CODE, java.util.Date FT_DT_FRM, java.util.Date FT_DT_TO, java.lang.Integer FT_AD_BRNCH, java.lang.Integer FT_AD_CMPNY)"
 *             query="SELECT OBJECT(ft) FROM CmFundTransfer ft WHERE ft.ftPosted = 1 AND ft.ftVoid = 0 AND ft.ftAccountFromReconciled = 1 AND ft.ftAdBaAccountFrom = ?1 AND ft.ftDate >= ?2 AND ft.ftDate <= ?3 AND ft.ftAdBranch = ?4 AND ft.ftAdCompany = ?5 ORDER BY ft.ftDate"
 *
 * @ejb:select signature="Collection ejbSelectGeneric(java.lang.String jbossQl, java.lang.Object[] args)"
 *             result-type-mapping="Local"
 *             method-intf="LocalHome"
 *             query="SELECT OBJECT(ft) FROM CmFundTransfer ft"
 *
 * @jboss:query signature="Collection ejbSelectGeneric(java.lang.String jbossQl, java.lang.Object[] args)"
 *                         dynamic="true"
 *
 * @ejb:value-object match="*"
 *             name="CmFundTransfer"
 *
 * @jboss:persistence table-name="CM_FND_TRNSFR"
 *
 * @jboss:entity-command name="mysql-get-generated-keys"
 */

public abstract class CmFundTransferBean extends AbstractEntityBean {
    
    
    // PERSISTENT METHODS
    
    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     * @ejb:pk-field
     *
     * @jboss:column-name name="FT_CODE"
     *
     * @jboss:persistence auto-increment="true"
     **/
    public abstract Integer getFtCode();         
    public abstract void setFtCode(Integer FT_CODE);
     
    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="FT_DT"
     **/
     public abstract Date getFtDate();
     /**
      * @ejb:interface-method view-type="local"
      **/
     public abstract void setFtDate(Date FT_DT);
     
     /**
      * @ejb:persistent-field
      * @ejb:interface-method view-type="local"
      *
      * @jboss:column-name name="FT_DCMNT_NMBR"
      **/
     public abstract String getFtDocumentNumber();
     /**
      * @ejb:interface-method view-type="local"
      **/
     public abstract void setFtDocumentNumber(String FT_DCMNT_NMBR);
    
    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="FT_RFRNC_NMBR"
     **/
     public abstract String getFtReferenceNumber();
     /**
      * @ejb:interface-method view-type="local"
      **/
     public abstract void setFtReferenceNumber(String FT_RFRNC_NMBR);
     
     /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="FT_MM"
     **/
     public abstract String getFtMemo();
     /**
      * @ejb:interface-method view-type="local"
      **/
     public abstract void setFtMemo(String FT_MM);

    
     /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="FT_AD_BA_ACCNT_FRM"
     **/
     public abstract Integer getFtAdBaAccountFrom();
     /**
      * @ejb:interface-method view-type="local"
      **/
     public abstract void setFtAdBaAccountFrom(Integer FT_AD_BA_ACCNT_FRM);
     
     
     /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="FT_AD_BA_ACCNT_TO"
     **/
     public abstract Integer getFtAdBaAccountTo();
     /**
      * @ejb:interface-method view-type="local"
      **/
     public abstract void setFtAdBaAccountTo(Integer FT_AD_BA_ACCNT_TO);

     /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="FT_AMNT"
     **/
     public abstract double getFtAmount();
     /**
      * @ejb:interface-method view-type="local"
      **/
     public abstract void setFtAmount(double FT_AMNT);

    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="FT_CNVRSN_DT"
     **/
     public abstract Date getFtConversionDate();
     /**
      * @ejb:interface-method view-type="local"
      **/
     public abstract void setFtConversionDate(Date FT_CNVRSN_DT);

    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="FT_CNVRSN_RT_FRM"
     **/
     public abstract double getFtConversionRateFrom();
     /**
      * @ejb:interface-method view-type="local"
      **/
     public abstract void setFtConversionRateFrom(double FT_CNVRSN_RT_FRM);
     
    
    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="FT_ACCNT_FRM_RCNCLD"
     **/
     public abstract byte getFtAccountFromReconciled();
     /**
      * @ejb:interface-method view-type="local"
      **/
     public abstract void setFtAccountFromReconciled(byte FT_ACCNT_FRM_RCNCLD);
     
     
     /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="FT_ACCNT_TO_RCNCLD"
     **/
     public abstract byte getFtAccountToReconciled();
     /**
      * @ejb:interface-method view-type="local"
      **/
     public abstract void setFtAccountToReconciled(byte FT_ACCNT_TO_RCNCLD);
     
     
     /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="FT_VOID"
     **/
     public abstract byte getFtVoid();
     /**
      * @ejb:interface-method view-type="local"
      **/
     public abstract void setFtVoid(byte BIL_VOID);
     
     /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="FT_APPRVL_STATUS"
     **/
     public abstract String getFtApprovalStatus();
     /**
      * @ejb:interface-method view-type="local"
      **/
     public abstract void setFtApprovalStatus(String FT_APPRVL_STATUS); 

     /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="FT_PSTD"
     **/
     public abstract byte getFtPosted();
     /**
      * @ejb:interface-method view-type="local"
      **/
     public abstract void setFtPosted(byte FT_PSTD);

     /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="FT_CRTD_BY"
     **/
     public abstract String getFtCreatedBy();
     /**
      * @ejb:interface-method view-type="local"
      **/
     public abstract void setFtCreatedBy(String FT_CRTD_BY);

    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="FT_DT_CRTD"
     **/
     public abstract Date getFtDateCreated();
     /**
      * @ejb:interface-method view-type="local"
      **/
     public abstract void setFtDateCreated(Date FT_DT_CRTD);

    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="FT_LST_MDFD_BY"
     **/
     public abstract String getFtLastModifiedBy();
     /**
      * @ejb:interface-method view-type="local"
      **/
     public abstract void setFtLastModifiedBy(String FT_LST_MDFD_BY);

    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="FT_DT_LST_MDFD"
     **/
     public abstract Date getFtDateLastModified();
     /**
      * @ejb:interface-method view-type="local"
      **/
     public abstract void setFtDateLastModified(Date FT_DT_LST_MDFD);

    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="FT_APPRVD_RJCTD_BY"
     **/
     public abstract String getFtApprovedRejectedBy();
     /**
      * @ejb:interface-method view-type="local"
      **/
     public abstract void setFtApprovedRejectedBy(String FT_APPRVD_RJCTD_BY);

    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="FT_DT_APPRVD_RJCTD"
     **/
     public abstract Date getFtDateApprovedRejected();
     /**
      * @ejb:interface-method view-type="local"
      **/
     public abstract void setFtDateApprovedRejected(Date FT_DT_APPRVD_RJCTD);

    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="FT_PSTD_BY"
     **/
     public abstract String getFtPostedBy();
     /**
      * @ejb:interface-method view-type="local"
      **/
     public abstract void setFtPostedBy(String FT_PSTD_BY);

    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="FT_DT_PSTD"
     **/
     public abstract Date getFtDatePosted();
     /**
      * @ejb:interface-method view-type="local"
      **/
     public abstract void setFtDatePosted(Date FT_DT_PSTD);
     
     /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="FT_RSN_FR_RJCTN"
     **/
     public abstract String getFtReasonForRejection();
     /**
      * @ejb:interface-method view-type="local"
      **/
     public abstract void setFtReasonForRejection(String FT_RSN_FR_RJCTN);  
     
     /**
      * @ejb:persistent-field
      * @ejb:interface-method view-type="local"
      *
      * @jboss:column-name name="FT_TYP"
      **/
      public abstract String getFtType();
      /**
       * @ejb:interface-method view-type="local"
       **/
      public abstract void setFtType(String FT_TYP);  

      /**
       * @ejb:persistent-field
       * @ejb:interface-method view-type="local"
       *
       * @jboss:column-name name="FT_CNVRSN_RT_TO"
       **/
      public abstract double getFtConversionRateTo();
      /**
       * @ejb:interface-method view-type="local"
       **/
      public abstract void setFtConversionRateTo(double FT_CNVRSN_RT_TO);      
      
      /**
       * @ejb:persistent-field
       * @ejb:interface-method view-type="local"
       *
       * @jboss:column-name name="FT_ACCNT_FRM_DT_RCNCLD"
       **/
       public abstract Date getFtAccountFromDateReconciled();
       /**
        * @ejb:interface-method view-type="local"
        **/
       public abstract void setFtAccountFromDateReconciled(Date FT_ACCNT_FRM_DT_RCNCLD);
       
       
       /**
       * @ejb:persistent-field
       * @ejb:interface-method view-type="local"
       *
       * @jboss:column-name name="FT_ACCNT_TO_DT_RCNCLD"
       **/
       public abstract Date getFtAccountToDateReconciled();
       /**
        * @ejb:interface-method view-type="local"
        **/
       public abstract void setFtAccountToDateReconciled(Date FT_ACCNT_TO_DT_RCNCLD);
       
      
     /**
      * @ejb:persistent-field
      * @ejb:interface-method view-type="local"
      *
      * @jboss:column-name name="FT_AD_BRNCH"
      **/
      public abstract Integer getFtAdBranch();
      /**
       * @ejb:interface-method view-type="local"
       **/
      public abstract void setFtAdBranch(Integer FT_AD_BRNCH);
     
     /**
      * @ejb:persistent-field
      * @ejb:interface-method view-type="local"
      *
      * @jboss:column-name name="FT_AD_CMPNY"
      **/
      public abstract Integer getFtAdCompany();
      /**
       * @ejb:interface-method view-type="local"
       **/
      public abstract void setFtAdCompany(Integer FT_AD_CMPNY);  


    // RELATIONSHIP METHODS    

     /**
      * @ejb:interface-method view-type="local"
      * @ejb:relation name="fundtransfer-cmdistributionrecords"
      *               role-name="fundtransfer-has-many-cmdistributionrecords"
      * 
      */
     public abstract Collection getCmDistributionRecords();
     public abstract void setCmDistributionRecords(Collection cmDistributionRecords);
     
     /**
      * @ejb:interface-method view-type="local"
      * @ejb:relation name="fundTransfer-fundTransferReceipts"
      *               role-name="fundtransfer-has-many-fundReceipts"
      * 
      */
     public abstract Collection getCmFundTransferReceipts();
     public abstract void setCmFundTransferReceipts(Collection cmFundTransferReceipts);

    /**
     * @jboss:dynamic-ql
     */
     public abstract Collection ejbSelectGeneric(java.lang.String jbossQl, java.lang.Object[] args)
         throws FinderException;

     
     // BUSINESS METHODS
     
     /**
      * @ejb:home-method view-type="local"
      */
     public Collection ejbHomeGetFtByCriteria(java.lang.String jbossQl, java.lang.Object[] args)
        throws FinderException {
       	
        return ejbSelectGeneric(jbossQl, args);
     }     

    /**
     * @ejb:interface-method view-type="local"
     **/
    public void addCmDistributionRecord(LocalCmDistributionRecord cmDistributionRecord) {

         Debug.print("CmFundTransferBean addCmDistributionRecord");
     
         try {
     	
            Collection cmDistributionRecords = getCmDistributionRecords();
	         cmDistributionRecords.add(cmDistributionRecord);
	          
         } catch (Exception ex) {
     	
           throw new EJBException(ex.getMessage());
     
         }   
    }

    /**
     * @ejb:interface-method view-type="local"
     **/
    public void dropCmDistributionRecord(LocalCmDistributionRecord cmDistributionRecord) {

         Debug.print("CmFundTransferBean dropCmDistributionRecord");
     
         try {
     	
            Collection cmDistributionRecords = getCmDistributionRecords();
	         cmDistributionRecords.remove(cmDistributionRecord);
	      
         } catch (Exception ex) {
     	
           throw new EJBException(ex.getMessage());
        
         }
            
    }    			    

    /**
     * @ejb:interface-method view-type="local"
     **/
    public short getCmDrNextLine() {

       Debug.print("CmFundTransferBean getCmDrNextLine");
      
       try {
      	
          Collection cmDistributionRecords = getCmDistributionRecords();
	      return (short)(cmDistributionRecords.size() + 1);
	     
       } catch (Exception ex) {
      	
          throw new EJBException(ex.getMessage());
         
       }
    }
    
    /**
     * @ejb:interface-method view-type="local"
     **/
    public void addCmFundTransferReceipt(LocalCmFundTransferReceipt cmFundTransferReceipt) {

         Debug.print("CmFundTransferBean addCmFundTransferReceipt");
     
         try {
     	
            Collection cmFundTransferReceipts = getCmFundTransferReceipts();
            cmFundTransferReceipts.add(cmFundTransferReceipt);
	          
         } catch (Exception ex) {
     	
           throw new EJBException(ex.getMessage());
     
         }   
    }

    /**
     * @ejb:interface-method view-type="local"
     **/
    public void dropCmFundTransferReceipt(LocalCmFundTransferReceipt cmFundTransferReceipt) {

         Debug.print("CmFundTransferBean dropCmFundTransferReceipt");
     
         try {
     	
            Collection cmFundTransferReceipts = getCmFundTransferReceipts();
            cmFundTransferReceipts.remove(cmFundTransferReceipt);
	      
         } catch (Exception ex) {
     	
           throw new EJBException(ex.getMessage());
        
         }
            
    }      
    
     // ENTITY METHODS
    
     /**
      * @ejb:create-method view-type="local"
      **/
     public Integer ejbCreate(Integer FT_CODE, Date FT_DT, 
         String FT_DCMNT_NMBR, String FT_RFRNC_NMBR, String FT_MM, Integer FT_AD_BA_ACCNT_FRM,
         Integer FT_AD_BA_ACCNT_TO, double FT_AMNT, Date FT_CNVRSN_DT,
         double FT_CNVRSN_RT_FRM, byte FT_VOID, byte FT_ACCNT_FRM_RCNCLD, 
         byte FT_ACCNT_TO_RCNCLD, String FT_APPRVL_STATUS, byte FT_PSTD, 
         String FT_CRTD_BY, Date FT_DT_CRTD, String FT_LST_MDFD_BY, 
         Date FT_DT_LST_MDFD, String FT_APPRVD_RJCTD_BY, 
         Date FT_DT_APPRVD_RJCTD, String FT_PSTD_BY, Date FT_DT_PSTD, String FT_RSN_FR_RJCTN, String FT_TYP,
         double FT_CNVRSN_RT_TO, Date FT_ACCNT_FRM_DT_RCNCLD, Date FT_ACCNT_TO_DT_RCNCLD, Integer FT_AD_BRNCH, Integer FT_AD_CMPNY)
         throws CreateException {
           
         Debug.print("CmFundTransferBean ejbCreate");
        
         setFtCode(FT_CODE);
         setFtDate(FT_DT);
         setFtDocumentNumber(FT_DCMNT_NMBR);
         setFtReferenceNumber(FT_RFRNC_NMBR);
         setFtMemo(FT_MM);
         setFtAdBaAccountFrom(FT_AD_BA_ACCNT_FRM);
         setFtAdBaAccountTo(FT_AD_BA_ACCNT_TO);
         setFtAmount(FT_AMNT);
         setFtConversionDate(FT_CNVRSN_DT);
         setFtConversionRateFrom(FT_CNVRSN_RT_FRM);         
         setFtVoid(FT_VOID);
         setFtAccountFromReconciled(FT_ACCNT_FRM_RCNCLD);
         setFtAccountToReconciled(FT_ACCNT_TO_RCNCLD);
         setFtApprovalStatus(FT_APPRVL_STATUS);
         setFtPosted(FT_PSTD);
         setFtCreatedBy(FT_CRTD_BY);
         setFtDateCreated(FT_DT_CRTD);
         setFtLastModifiedBy(FT_LST_MDFD_BY);
         setFtDateLastModified(FT_DT_LST_MDFD);
         setFtApprovedRejectedBy(FT_APPRVD_RJCTD_BY);
         setFtDateApprovedRejected(FT_DT_APPRVD_RJCTD);
         setFtPostedBy(FT_PSTD_BY);
         setFtDatePosted(FT_DT_PSTD);
         setFtReasonForRejection(FT_RSN_FR_RJCTN);
         setFtType(FT_TYP);
         setFtConversionRateTo(FT_CNVRSN_RT_TO);
         setFtAccountFromDateReconciled(FT_ACCNT_FRM_DT_RCNCLD);
         setFtAccountToDateReconciled(FT_ACCNT_TO_DT_RCNCLD);
         setFtAdBranch(FT_AD_BRNCH);
         setFtAdCompany(FT_AD_CMPNY);
                
         return null;
        
     }
    
     /**
      * @ejb:create-method view-type="local"
      **/
     public Integer ejbCreate(Date FT_DT, 
     	 String FT_DCMNT_NMBR, String FT_RFRNC_NMBR, String FT_MM, Integer FT_AD_BA_ACCNT_FRM,
         Integer FT_AD_BA_ACCNT_TO, double FT_AMNT, Date FT_CNVRSN_DT,
         double FT_CNVRSN_RT_FRM, byte FT_VOID, byte FT_ACCNT_FRM_RCNCLD, 
         byte FT_ACCNT_TO_RCNCLD, String FT_APPRVL_STATUS, byte FT_PSTD, 
         String FT_CRTD_BY, Date FT_DT_CRTD, String FT_LST_MDFD_BY, 
         Date FT_DT_LST_MDFD, String FT_APPRVD_RJCTD_BY, 
         Date FT_DT_APPRVD_RJCTD, String FT_PSTD_BY, Date FT_DT_PSTD, String FT_RSN_FR_RJCTN, String FT_TYP,
         double FT_CNVRSN_RT_TO, Date FT_ACCNT_FRM_DT_RCNCLD, Date FT_ACCNT_TO_DT_RCNCLD, Integer FT_AD_BRNCH, Integer FT_AD_CMPNY)
         throws CreateException {
           
         Debug.print("CmFundTransferBean ejbCreate");
               
         setFtDate(FT_DT);
         setFtDocumentNumber(FT_DCMNT_NMBR);
         setFtReferenceNumber(FT_RFRNC_NMBR);
         setFtMemo(FT_MM);
         setFtAdBaAccountFrom(FT_AD_BA_ACCNT_FRM);
         setFtAdBaAccountTo(FT_AD_BA_ACCNT_TO);
         setFtAmount(FT_AMNT);
         setFtConversionDate(FT_CNVRSN_DT);
         setFtConversionRateFrom(FT_CNVRSN_RT_FRM);         
         setFtVoid(FT_VOID);
         setFtAccountFromReconciled(FT_ACCNT_FRM_RCNCLD);
         setFtAccountToReconciled(FT_ACCNT_TO_RCNCLD);
         setFtApprovalStatus(FT_APPRVL_STATUS);
         setFtPosted(FT_PSTD);
         setFtCreatedBy(FT_CRTD_BY);
         setFtDateCreated(FT_DT_CRTD);
         setFtLastModifiedBy(FT_LST_MDFD_BY);
         setFtDateLastModified(FT_DT_LST_MDFD);
         setFtApprovedRejectedBy(FT_APPRVD_RJCTD_BY);
         setFtDateApprovedRejected(FT_DT_APPRVD_RJCTD);
         setFtPostedBy(FT_PSTD_BY);
         setFtDatePosted(FT_DT_PSTD);
         setFtReasonForRejection(FT_RSN_FR_RJCTN);
         setFtType(FT_TYP);
         setFtConversionRateTo(FT_CNVRSN_RT_TO);
         setFtAccountFromDateReconciled(FT_ACCNT_FRM_DT_RCNCLD);
         setFtAccountToDateReconciled(FT_ACCNT_TO_DT_RCNCLD);
         setFtAdBranch(FT_AD_BRNCH);
         setFtAdCompany(FT_AD_CMPNY);
        
         return null;
        
     }
    
    
     public void ejbPostCreate(Integer FT_CODE, Date FT_DT, 
     	 String FT_DCMNT_NMBR, String FT_RFRNC_NMBR, String FT_MM, Integer FT_AD_BA_ACCNT_FRM,
         Integer FT_AD_BA_ACCNT_TO, double FT_AMNT, Date FT_CNVRSN_DT,
         double FT_CNVRSN_RT_FRM, byte FT_VOID, byte FT_ACCNT_FRM_RCNCLD, 
         byte FT_ACCNT_TO_RCNCLD, String FT_APPRVL_STATUS, byte FT_PSTD, 
         String FT_CRTD_BY, Date FT_DT_CRTD, String FT_LST_MDFD_BY, 
         Date FT_DT_LST_MDFD, String FT_APPRVD_RJCTD_BY, 
         Date FT_DT_APPRVD_RJCTD, String FT_PSTD_BY, Date FT_DT_PSTD, String FT_RSN_FR_RJCTN, String FT_TYP,
         double FT_CNVRSN_RT_TO, Date FT_ACCNT_FRM_DT_RCNCLD, Date FT_ACCNT_TO_DT_RCNCLD, Integer FT_AD_BRNCH, Integer FT_AD_CMPNY)
         throws CreateException { }
   
     public void ejbPostCreate(Date FT_DT, 
     	 String FT_DCMNT_NMBR, String FT_RFRNC_NMBR, String FT_MM, Integer FT_AD_BA_ACCNT_FRM,
         Integer FT_AD_BA_ACCNT_TO, double FT_AMNT, Date FT_CNVRSN_DT,
         double FT_CNVRSN_RT_FRM, byte FT_VOID, byte FT_ACCNT_FRM_RCNCLD, 
         byte FT_ACCNT_TO_RCNCLD, String FT_APPRVL_STATUS, byte FT_PSTD, 
         String FT_CRTD_BY, Date FT_DT_CRTD, String FT_LST_MDFD_BY, 
         Date FT_DT_LST_MDFD, String FT_APPRVD_RJCTD_BY, 
         Date FT_DT_APPRVD_RJCTD, String FT_PSTD_BY, Date FT_DT_PSTD, String FT_RSN_FR_RJCTN, String FT_TYP,
         double FT_CNVRSN_RT_TO, Date FT_ACCNT_FRM_DT_RCNCLD, Date FT_ACCNT_TO_DT_RCNCLD, Integer FT_AD_BRNCH, Integer FT_AD_CMPNY)
         throws CreateException { }
        
}

