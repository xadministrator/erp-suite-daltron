/*
 * com/ejb/ap/LocalCmFundTransferReceiptBean.java
 *
 * Created on December 01, 2005 1:11 PM
 */

package com.ejb.cm;

import java.util.Collection;

import javax.ejb.CreateException;
import javax.ejb.FinderException;

import com.ejb.ar.LocalArReceipt;
import com.util.AbstractEntityBean;
import com.util.Debug;

/**
 *
 * @author  Farrah S. Garing
 *
 * @ejb:bean name="CmFundTransferReceiptEJB"
 *           display-name="Fund Transfer Receipt Entity"
 *           type="CMP"
 *           cmp-version="2.x"
 *           view-type="local"
 *           local-jndi-name="omega-ejb/CmFundTransferReceiptEJB"
 *           schema="CmFundTransferReceipt"
 *           primkey-field="ftrCode"
 *
 * @ejb:pk class="java.lang.Integer"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="cmuser"
 *                        role-link="cmuserlink"
 *
 * @ejb:permission role-name="cmuser"
 *
 * @ejb:interface local-class="com.ejb.cm.LocalCmFundTransferReceipt"
 *                local-extends="javax.ejb.EJBLocalObject"
 *
 * @ejb:home local-class="com.ejb.cm.LocalCmFundTransferReceiptHome"
 *           local-extends="javax.ejb.EJBLocalHome"
 *
 * @ejb:select signature="Collection ejbSelectGeneric(java.lang.String jbossQl, java.lang.Object[] args)"
 *             result-type-mapping="Local"
 *             method-intf="LocalHome"
 *             query="SELECT OBJECT(ftr) FROM CmFundTransferReceipt ftr"
 *
 * @jboss:query signature="Collection ejbSelectGeneric(java.lang.String jbossQl, java.lang.Object[] args)"
 *                         dynamic="true"
 * 
 * @ejb:value-object match="*"
 *             name="CmFundTransferReceipt"
 *
 * @jboss:persistence table-name="CM_FND_TRNSFR_RCPT"
 *
 * @jboss:entity-command name="mysql-get-generated-keys"
 */

public abstract class CmFundTransferReceiptBean extends AbstractEntityBean {
    
    
    // PERSISTENT METHODS
    
    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     * @ejb:pk-field
     *
     * @jboss:column-name name="FTR_CODE"
     *
     * @jboss:persistence auto-increment="true"
     **/
    public abstract Integer getFtrCode();
    /**
     * @ejb:interface-method view-type="local"
     **/
    public abstract void setFtrCode(Integer FTR_CODE);

     /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="FTR_AMNT_DPSTD"
     **/
     public abstract double getFtrAmountDeposited();
     /**
      * @ejb:interface-method view-type="local"
      **/
     public abstract void setFtrAmountDeposited(double FTR_AMNT_DPSTD);
     
     /**
      * @ejb:persistent-field
      * @ejb:interface-method view-type="local"
      *
      * @jboss:column-name name="FTR_AD_CMPNY"
      **/
      public abstract Integer getFtrAdCompany();
      /**
       * @ejb:interface-method view-type="local"
       **/
      public abstract void setFtrAdCompany(Integer FTR_AD_CMPNY);  


    // RELATIONSHIP METHODS    

      /**
       * @ejb:interface-method view-type="local"
       * @ejb:relation name="receipt-fundTransferReceipts"
       *               role-name="fundTransferReceipt-has-one-receipt"
       *
       * @jboss:relation related-pk-field="rctCode"
       *                 fk-column="AR_RECEIPT"
       */
      public abstract LocalArReceipt getArReceipt();
      /**
       * @ejb:interface-method view-type="local"
       **/
      public abstract void setArReceipt(LocalArReceipt arReceipt);
      
      /**
       * @ejb:interface-method view-type="local"
       * @ejb:relation name="fundTransfer-fundTransferReceipts"
       *               role-name="fundTransferReceipt-has-one-fundTransfer"
       *               cascade-delete="yes"
       * 
       * @jboss:relation related-pk-field="ftCode"
       *                 fk-column="CM_FUND_TRANSFER"
       */
      public abstract LocalCmFundTransfer getCmFundTransfer();
      /**
       * @ejb:interface-method view-type="local"
       **/
      public abstract void setCmFundTransfer(LocalCmFundTransfer cmFundTransfer);  
      
      /**
       * @jboss:dynamic-ql
       */
       public abstract Collection ejbSelectGeneric(java.lang.String jbossQl, java.lang.Object[] args)
           throws FinderException;
       
     // BUSINESS METHODS

       /**
        * @ejb:home-method view-type="local"
        */
       public Collection ejbHomeGetFtrByCriteria(java.lang.String jbossQl, java.lang.Object[] args)
          throws FinderException {
         	
          return ejbSelectGeneric(jbossQl, args);
       }     

     // ENTITY METHODS
    
     /**
      * @ejb:create-method view-type="local"
      **/
     public Integer ejbCreate(Integer FTR_CODE, double FTR_AMNT_DPSTD, Integer FTR_AD_CMPNY)
         throws CreateException {
           
         Debug.print("CmFundTransferReceiptBean ejbCreate");
        
         setFtrCode(FTR_CODE);
         setFtrAmountDeposited(FTR_AMNT_DPSTD);
         setFtrAdCompany(FTR_AD_CMPNY);
                
         return null;
        
     }
    
     /**
      * @ejb:create-method view-type="local"
      **/
     public Integer ejbCreate( double FTR_AMNT_DPSTD, Integer FTR_AD_CMPNY)
         throws CreateException {
           
         Debug.print("CmFundTransferReceiptBean ejbCreate");
               
         setFtrAmountDeposited(FTR_AMNT_DPSTD);
         setFtrAdCompany(FTR_AD_CMPNY);
        
         return null;
        
     }
    
    
     public void ejbPostCreate(Integer FTR_CODE, double FTR_AMNT_DPSTD, Integer FTR_AD_CMPNY)
         throws CreateException { }
   
     public void ejbPostCreate(double FTR_AMNT_DPST, Integer FTR_AD_CMPNY)
         throws CreateException { }
        
}

