package com.ejb.exception;

import com.util.Debug;

public class AdPRFCoaGlPosDineInChargeAccountNotFoundException extends Exception {

   public AdPRFCoaGlPosDineInChargeAccountNotFoundException() {
      Debug.print("AdPRFCoaGlPosDineInChargeAccountNotFoundException Constructor");
   }

   public AdPRFCoaGlPosDineInChargeAccountNotFoundException(String msg) {
      super(msg);
      Debug.print("AdPRFCoaGlPosDineInChargeAccountNotFoundException Constructor");
   }
}
