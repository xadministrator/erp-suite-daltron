package com.ejb.exception;

import com.util.Debug;

public class GlACVLastPeriodIncorrectException extends Exception {

   public GlACVLastPeriodIncorrectException() {
      Debug.print("GlACVLastPeriodIncorrectException Constructor");
   }

   public GlACVLastPeriodIncorrectException(String msg) {
      super(msg);
      Debug.print("GlACVLastPeriodIncorrectException Constructor");
   }
}
