package com.ejb.exception;

import com.util.Debug;

public class GlRepDISNoRecordFoundException extends Exception {

   public GlRepDISNoRecordFoundException() {
      Debug.print("GlRepDISNoRecordFoundException Constructor");
   }

   public GlRepDISNoRecordFoundException(String msg) {
      super(msg);
      Debug.print("GlRepDISNoRecordFoundException Constructor");
   }
}
