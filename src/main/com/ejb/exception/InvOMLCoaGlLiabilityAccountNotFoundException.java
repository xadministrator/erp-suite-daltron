package com.ejb.exception;

import com.util.Debug;

public class InvOMLCoaGlLiabilityAccountNotFoundException extends Exception {

   public InvOMLCoaGlLiabilityAccountNotFoundException() {
      Debug.print("InvOMLCoaGlLiabilityAccountNotFoundException Constructor");
   }

   public InvOMLCoaGlLiabilityAccountNotFoundException(String msg) {
      super(msg);
      Debug.print("InvOMLCoaGlLiabilityAccountNotFoundException Constructor");
   }
}
