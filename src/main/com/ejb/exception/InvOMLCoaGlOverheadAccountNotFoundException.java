package com.ejb.exception;

import com.util.Debug;

public class InvOMLCoaGlOverheadAccountNotFoundException extends Exception {

   public InvOMLCoaGlOverheadAccountNotFoundException() {
      Debug.print("InvOMLCoaGlOverheadAccountNotFoundException Constructor");
   }

   public InvOMLCoaGlOverheadAccountNotFoundException(String msg) {
      super(msg);
      Debug.print("InvOMLCoaGlOverheadAccountNotFoundException Constructor");
   }
}
