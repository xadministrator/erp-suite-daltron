package com.ejb.exception;

import com.util.Debug;

public class InvOverHeadCostAccountNotFoundException extends Exception {

   public InvOverHeadCostAccountNotFoundException() {
      Debug.print("InvOverHeadCostAccountNotFoundException Constructor");
   }

   public InvOverHeadCostAccountNotFoundException(String msg) {
      super(msg);
      Debug.print("InvOverHeadCostAccountNotFoundException Constructor");
   }
}
