package com.ejb.genfld;

import javax.ejb.CreateException;

import com.util.AbstractEntityBean;
import com.util.Debug;

/**
 * @ejb:bean name="GenChildRangeEJB"
 *           display-name="Child Range Entity"
 *           type="CMP"
 *           cmp-version="2.x"
 *           view-type="local"
 *           local-jndi-name="omega-ejb/GenChildRangeEJB"
 *           schema="GenChildRange"
 *           primkey-field="crCode"
 *       
 * @ejb:pk class="java.lang.Integer"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="gluser"
 *                        role-link="gluserlink"
 *
 * @ejb:permission role-name="gluser"
 *
 * @ejb:interface local-class="com.ejb.genfld.LocalGenChildRange"
 *                local-extends="javax.ejb.EJBLocalObject"
 *
 * @ejb:home local-class="com.ejb.genfld.LocalGenChildRangeHome"
 *           local-extends="javax.ejb.EJBLocalHome"
 *
 * @jboss:persistence table-name="GEN_CHLD_RNG"
 *
 * @jboss:entity-command name="mysql-get-generated-keys"
 *
 */

public abstract class GenChildRangeBean extends AbstractEntityBean {

  
   // Access methods for persistent fields

   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    * @ejb:pk-field
    *
    * @jboss:column-name name="CR_CODE"
    *
    * @jboss:persistence auto-increment="true"
    **/
   public abstract java.lang.Integer getCrCode();
   public abstract void setCrCode(java.lang.Integer CR_CODE);

   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="CR_LW"
    **/
   public abstract int getCrLow();
   public abstract void setCrLow(int CR_LW);

   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="CR_HGH"
    **/
   public abstract int getCrHigh();
   public abstract void setCrHigh(int CR_HGH);

   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="CR_RNG_TYP"
    **/
   public abstract char getCrRangeType();
   public abstract void setCrRangeType(char CR_RNG_TYP);
   
   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="CR_AD_CMPNY"
    **/
   public abstract Integer getCrAdCompany();
   public abstract void setCrAdCompany(Integer CR_AD_CMPNY);

   // No access methods for relationship fields

   /**
    * @ejb:interface-method view-type="local"
    * @ejb:relation name="valuesetvalue-childranges"
    *               role-name="childrange-has-one-valuesetvalue"
    *
    * @jboss:relation related-pk-field="vsvCode"
    *                 fk-column="GEN_VALUE_SET_VALUE"
    */
   public abstract LocalGenValueSetValue getGenValueSetValue();
   public abstract void setGenValueSetValue(LocalGenValueSetValue genValueSetValue);

   // No Business methods

   // EntityBean methods

   /**
    * @ejb:create-method view-type="local"
    **/
   public java.lang.Integer ejbCreate (java.lang.Integer CR_CODE, int CR_LW, int CR_HGH,
      char CR_RNG_TYP, Integer CR_AD_CMPNY)
      throws CreateException {

      Debug.print("GenChildRangeBean ejbCreate");
      setCrCode(CR_CODE);
      setCrLow(CR_LW);
      setCrHigh(CR_HGH);
      setCrRangeType(CR_RNG_TYP);
      setCrAdCompany(CR_AD_CMPNY);
      
      return null;
   }

   public void ejbPostCreate (java.lang.Integer CR_CODE, int CR_LW, int CR_HGH,
      char CR_RNG_TYP, Integer CR_AD_CMPNY)
      throws CreateException { }

} // GenChldRngBean class
