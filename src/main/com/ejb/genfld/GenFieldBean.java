package com.ejb.genfld;

import java.util.Collection;

import javax.ejb.CreateException;
import javax.ejb.EJBException;

import com.util.AbstractEntityBean;
import com.util.Debug;

/**
 * @ejb:bean name="GenFieldEJB"
 *           display-name="Generic Field Entity"
 *           type="CMP"
 *           cmp-version="2.x"
 *           view-type="local"
 *           local-jndi-name="omega-ejb/GenFieldEJB"
 *           schema="GenField"
 *           primkey-field="flCode"
 *       
 * @ejb:pk class="java.lang.Integer"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="gluser"
 *                        role-link="gluserlink"
 *
 * @ejb:permission role-name="gluser"
 *
 * @ejb:interface local-class="com.ejb.genfld.LocalGenField"
 *                local-extends="javax.ejb.EJBLocalObject"
 *
 * @ejb:home local-class="com.ejb.genfld.LocalGenFieldHome"
 *           local-extends="javax.ejb.EJBLocalHome"
 *
 * @ejb:finder signature="Collection findFlAllEnabled(java.lang.Integer FL_AD_CMPNY)"
 *             query="SELECT OBJECT(fl) FROM GenField fl WHERE fl.flEnable=1 AND fl.flAdCompany = ?1"
 *
 * @jboss:query signature="Collection findFlAllEnabled(java.lang.Integer FL_AD_CMPNY)"
 *             query="SELECT OBJECT(fl) FROM GenField fl WHERE fl.flEnable=1 AND fl.flAdCompany = ?1 ORDER BY fl.flName"
 *
 * @ejb:finder signature="LocalGenField findByFlName(java.lang.String FL_NM, java.lang.Integer FL_AD_CMPNY)"
 *             query="SELECT OBJECT(fl) FROM GenField fl WHERE fl.flName=?1 AND fl.flAdCompany = ?2"
 *
 * @ejb:finder signature="LocalGenField findByFlNumberOfSegment(short FL_NMBR_OF_NMBR, java.lang.Integer FL_AD_CMPNY)"
 *             query="SELECT OBJECT(fl) FROM GenField fl WHERE fl.flNumberOfSegment=?1 AND fl.flAdCompany = ?2"
 *
 * @jboss:persistence table-name="GEN_FLD"
 *
 * @jboss:entity-command name="mysql-get-generated-keys"
 *
 */

public abstract class GenFieldBean extends AbstractEntityBean {

   
   // Access methods for persistent fields

   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    * @ejb:pk-field
    *
    * @jboss:column-name name="FL_CODE"
    *
    * @jboss:persistence auto-increment="true"
    **/
   public abstract java.lang.Integer getFlCode();
   public abstract void setFlCode(java.lang.Integer FL_CODE);

   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="FL_NM"
    **/
   public abstract java.lang.String getFlName();
   public abstract void setFlName(java.lang.String FL_NM);

   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="FL_DESC"
    **/
   public abstract java.lang.String getFlDescription();
   public abstract void setFlDescription(java.lang.String FL_DESC);

   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="FL_SGMNT_SPRTR"
    **/
   public abstract char getFlSegmentSeparator();
   public abstract void setFlSegmentSeparator(char FL_SGMNT_SPRTR);

   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="FL_NMBR_OF_SGMNT"
    **/
   public abstract short getFlNumberOfSegment();
   public abstract void setFlNumberOfSegment(short FL_NMBR_OF_SGMNT);

   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="FL_FRZ_RLLP"
    **/
   public abstract byte getFlFreezeRollup();
   public abstract void setFlFreezeRollup(byte FL_FRZ_RLLP);

   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="FL_ENBL"
    **/
   public abstract byte getFlEnable();
   public abstract void setFlEnable(byte FL_ENBL);
   
   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="FL_AD_CMPNY"
    **/
   public abstract Integer getFlAdCompany();
   public abstract void setFlAdCompany(Integer FL_AD_CMPNY);

   // Access methods for relationship fields

   /**
    * @ejb:interface-method view-type="local"
    * @ejb:relation name="field-segments"
    *               role-name="field-has-many-segments"
    */
   public abstract Collection getGenSegments();
   public abstract void setGenSegments(Collection genSegments); 
   
   /**
    * @ejb:interface-method view-type="local"
    * @ejb:relation name="field-companies"
    *               role-name="field-has-many-companies"
    */
   public abstract Collection getAdCompanies();
   public abstract void setAdCompanies(Collection adCompanies);
   
   

   // Business methods

   /**
    * @ejb:interface-method view-type="local"
    **/
   public void addGenSegment(LocalGenSegment genSegment) {

      Debug.print("GenFieldBean addGenSegment");
      try {
         Collection genSegments = getGenSegments();
	     genSegments.add(genSegment);
      } catch (Exception ex) {
         throw new EJBException(ex.getMessage());
      }
   }

   /**
    * @ejb:interface-method view-type="local"
    **/
   public void dropGenSegment(LocalGenSegment genSegment) {

      Debug.print("GenFieldBean dropGenSegment");
      try {
         Collection genSegments = getGenSegments();
	     genSegments.remove(genSegment);
      } catch (Exception ex) {
         throw new EJBException(ex.getMessage());
      }
   }
   
   /**
    * @ejb:interface-method view-type="local"
    **/
   public void addAdCompany(com.ejb.ad.LocalAdCompany adCompany) {

      Debug.print("GenFieldBean addAdCompany");
      try {
         Collection adCompanies = getAdCompanies();
	 adCompanies.add(adCompany);
      } catch (Exception ex) {
         throw new EJBException(ex.getMessage());
      }
   }

   /**
    * @ejb:interface-method view-type="local"
    **/
   public void dropAdCompany(com.ejb.ad.LocalAdCompany adCompany) {
   
      Debug.print("GenFieldBean dropAdCompany");
      try {
         Collection adCompanies = getAdCompanies();
	 adCompanies.remove(adCompany);
      } catch (Exception ex) {
         throw new EJBException(ex.getMessage());
      }
   }

   // EntityBean methods

   /**
    * @ejb:create-method view-type="local"
    **/
   public java.lang.Integer ejbCreate (java.lang.Integer FL_CODE, java.lang.String FL_NM, java.lang.String FL_DESC,
      char FL_SGMNT_SPRTR, short FL_NMBR_OF_SGMNT, 
      byte FL_FRZ_RLLP, byte FL_ENBL, Integer FL_AD_CMPNY)
      throws CreateException {

      Debug.print("GenFieldBean ejbCreate");
      setFlCode(FL_CODE);
      setFlName(FL_NM);
      setFlDescription(FL_DESC);
      setFlSegmentSeparator(FL_SGMNT_SPRTR);
      setFlNumberOfSegment(FL_NMBR_OF_SGMNT);
      setFlFreezeRollup(FL_FRZ_RLLP);
      setFlEnable(FL_ENBL);
      setFlAdCompany(FL_AD_CMPNY);
      
      return null;
   }
   
   /**
    * @ejb:create-method view-type="local"
    **/
   public java.lang.Integer ejbCreate (java.lang.String FL_NM, java.lang.String FL_DESC,
      char FL_SGMNT_SPRTR, short FL_NMBR_OF_SGMNT, 
      byte FL_FRZ_RLLP, byte FL_ENBL, Integer FL_AD_CMPNY)
      throws CreateException {

      Debug.print("GenFieldBean ejbCreate");
      
      setFlName(FL_NM);
      setFlDescription(FL_DESC);
      setFlSegmentSeparator(FL_SGMNT_SPRTR);
      setFlNumberOfSegment(FL_NMBR_OF_SGMNT);
      setFlFreezeRollup(FL_FRZ_RLLP);
      setFlEnable(FL_ENBL);
      setFlAdCompany(FL_AD_CMPNY);
      
      return null;
   }

   public void ejbPostCreate (java.lang.Integer FL_CODE, java.lang.String FL_NM, java.lang.String FL_DESC,
      char FL_SGMNT_SPRTR, short FL_NMBR_OF_SGMNT, 
      byte FL_FRZ_RLLP, byte FL_ENBL, Integer FL_AD_CMPNY)
      throws CreateException { }
  
   public void ejbPostCreate (java.lang.String FL_NM, java.lang.String FL_DESC,
        char FL_SGMNT_SPRTR, short FL_NMBR_OF_SGMNT, 
        byte FL_FRZ_RLLP, byte FL_ENBL, Integer FL_AD_CMPNY)
        throws CreateException { }

} // GenFldBean class
