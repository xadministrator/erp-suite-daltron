package com.ejb.genfld;

import java.util.Collection;

import javax.ejb.CreateException;
import javax.ejb.EJBException;

import com.util.AbstractEntityBean;
import com.util.Debug;

/**
 * @ejb:bean name="GenQualifierEJB"
 *           display-name="Qualifier Entity"
 *           type="CMP"
 *           cmp-version="2.x"
 *           view-type="local"
 *           local-jndi-name="omega-ejb/GenQualifierEJB"
 *           schema="GenQualifier"
 *           primkey-field="qlCode"
 *       
 * @ejb:pk class="java.lang.Integer"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="gluser"
 *                        role-link="gluserlink"
 *
 * @ejb:permission role-name="gluser"
 *
 * @ejb:interface local-class="com.ejb.genfld.LocalGenQualifier"
 *                local-extends="javax.ejb.EJBLocalObject"
 *
 * @ejb:home local-class="com.ejb.genfld.LocalGenQualifierHome"
 *           local-extends="javax.ejb.EJBLocalHome"
 *
 * @ejb:finder signature="Collection findQlfrAll(java.lang.Integer QL_AD_CMPNY)"
 *             query="SELECT OBJECT(ql) FROM GenQualifier ql WHERE ql.qlAdCompany = ?1"
 *
 * @ejb:finder signature="LocalGenQualifier findByQlAccountType(java.lang.String QL_ACCNT_TYP, java.lang.Integer QL_AD_CMPNY)"
 *             query="SELECT OBJECT(ql) FROM GenQualifier ql WHERE ql.qlAccountType = ?1 AND ql.qlAdCompany = ?2"
 *
 * @jboss:persistence table-name="GEN_QLFR"
 *
 * @jboss:entity-command name="mysql-get-generated-keys"
 *
 */

public abstract class GenQualifierBean extends AbstractEntityBean {

  
   // Access methods for persistent fields

   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    * @ejb:pk-field
    *
    * @jboss:column-name name="QL_CODE"
    *
    * @jboss:persistence auto-increment="true"
    **/
   public abstract java.lang.Integer getQlCode();
   public abstract void setQlCode(java.lang.Integer QL_CODE);

   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="QL_ACCNT_TYP"
    **/
   public abstract java.lang.String getQlAccountType();
   /**
	* @ejb:interface-method view-type="local"
	**/ 
   public abstract void setQlAccountType(java.lang.String QL_ACCNT_TYP);

   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="QL_BDGTNG_ALLWD"
    **/
   public abstract byte getQlBudgetingAllowed();
   /**
	* @ejb:interface-method view-type="local"
	**/ 
   public abstract void setQlBudgetingAllowed(byte QL_BDGTNG_ALLWD);

   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="QL_PSTNG_ALLWD"
    **/
   public abstract byte getQlPostingAllowed();
   /**
	* @ejb:interface-method view-type="local"
	**/ 
   public abstract void setQlPostingAllowed(byte QL_PSTNG_ALLWD);
   
   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="QL_AD_CMPNY"
    **/
   public abstract Integer getQlAdCompany();
   /**
	* @ejb:interface-method view-type="local"
	**/ 
   public abstract void setQlAdCompany(Integer QL_AD_CMPNY);

   // Access methods for relationship fields

   /**
    * @ejb:interface-method view-type="local"
    * @ejb:relation name="qualifier-valuesetvalues"
    *               role-name="qualifier-has-many-valuesetvalues"
    */
   public abstract Collection getGenValueSetValues();
   public abstract void setGenValueSetValues(Collection genValueSetValues);

   // Business methods

   /**
    * @ejb:interface-method view-type="local"
    **/
   public void addGenValueSetValue(LocalGenValueSetValue genValueSetValue) {

      Debug.print("GenQualifierBean addGenValueSetValue");
      try {
         Collection genValueSetValues = getGenValueSetValues();
	 genValueSetValues.add(genValueSetValue);
      } catch (Exception ex) {
         throw new EJBException(ex.getMessage());
      }
   }

   /**
    * @ejb:interface-method view-type="local"
    **/
   public void dropGenValueSetValue(LocalGenValueSetValue genValueSetValue) {

      Debug.print("GenQualifierBean dropGenValueSetValue");
      try {
         Collection genValueSetValues = getGenValueSetValues();
	 genValueSetValues.remove(genValueSetValue);
      } catch (Exception ex) {
         throw new EJBException(ex.getMessage());
      }
   }

   // EntityBean methods

   /**
    * @ejb:create-method view-type="local"
    **/
   public java.lang.Integer ejbCreate (java.lang.Integer QL_CODE, java.lang.String QL_ACCNT_TYP,
      byte QL_BDGTNG_ALLWD, byte QL_PSTNG_ALLWD, Integer QL_AD_CMPNY)
      throws CreateException {

      Debug.print("GenQlfrBean ejbCreate");
      setQlCode(QL_CODE);
      setQlAccountType(QL_ACCNT_TYP);
      setQlBudgetingAllowed(QL_BDGTNG_ALLWD);
      setQlPostingAllowed(QL_PSTNG_ALLWD);
      setQlAdCompany(QL_AD_CMPNY);
      return null;
   }
   
   /**
    * @ejb:create-method view-type="local"
    **/
   public java.lang.Integer ejbCreate (java.lang.String QL_ACCNT_TYP,
      byte QL_BDGTNG_ALLWD, byte QL_PSTNG_ALLWD, Integer QL_AD_CMPNY)
      throws CreateException {

      Debug.print("GenQlfrBean ejbCreate");
      
      setQlAccountType(QL_ACCNT_TYP);
      setQlBudgetingAllowed(QL_BDGTNG_ALLWD);
      setQlPostingAllowed(QL_PSTNG_ALLWD);
      setQlAdCompany(QL_AD_CMPNY);
      return null;
   }
   
   public void ejbPostCreate (java.lang.String QL_ACCNT_TYP,
        byte QL_BDGTNG_ALLWD, byte QL_PSTNG_ALLWD, Integer QL_AD_CMPNY)
        throws CreateException { }

   public void ejbPostCreate (java.lang.Integer QL_CODE, java.lang.String QL_ACCNT_TYP,
      byte QL_BDGTNG_ALLWD, byte QL_PSTNG_ALLWD, Integer QL_AD_CMPNY)
      throws CreateException { }

} // GenQlfrBean class
