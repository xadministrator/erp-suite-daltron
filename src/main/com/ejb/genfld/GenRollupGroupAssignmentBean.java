package com.ejb.genfld;

import javax.ejb.CreateException;

import com.util.AbstractEntityBean;
import com.util.Debug;

/**
 * @ejb:bean name="GenRollupGroupAssignmentEJB"
 *           display-name="Rollup Group Assignment Entity"
 *           type="CMP"
 *           cmp-version="2.x"
 *           view-type="local"
 *           local-jndi-name="omega-ejb/GenRollupGroupAssignmentEJB"
 *           schema="GenRollupGroupAssignment"
 *           primkey-field="rlgaCode"
 *       
 * @ejb:pk class="java.lang.Integer"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="gluser"
 *                        role-link="gluserlink"
 *
 * @ejb:permission role-name="gluser"
 *
 * @ejb:interface local-class="com.ejb.genfld.LocalGenRollupGroupAssignment"
 *                local-extends="javax.ejb.EJBLocalObject"
 *
 * @ejb:home local-class="com.ejb.genfld.LocalGenRollupGroupAssignmentHome"
 *           local-extends="javax.ejb.EJBLocalHome"
 *
 * @jboss:persistence table-name="GEN_RLLP_GRP_ASSGNMNT"
 *
 * @jboss:entity-command name="mysql-get-generated-keys"
 *
 */

public abstract class GenRollupGroupAssignmentBean extends AbstractEntityBean {

  
   // Access methods for persistent fields

   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    * @ejb:pk-field
    *
    * @jboss:column-name name="RLGA_CODE"
    *
    * @jboss:persistence auto-increment="true"
    **/
   public abstract java.lang.Integer getRlgaCode();
   public abstract void setRlgaCode(java.lang.Integer RLGA_CODE);

   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="RLGA_AD_CMPNY"
    **/
   public abstract Integer getRlgaAdCompany();
   /**
	* @ejb:interface-method view-type="local"
	**/ 
   public abstract void setRlgaAdCompany(Integer RLGA_AD_CMPNY);
   
   // Access methods for relationship fields

   /**
    * @ejb:interface-method view-type="local"
    * @ejb:relation name="valuesetvalue-rollupgroupassignments"
    *               role-name="rollupgroupassignment-has-one-valuesetvalue"
    *
    * @jboss:relation related-pk-field="vsvCode"
    *                 fk-column="GEN_VALUE_SET_VALUE"
    */
   public abstract LocalGenValueSetValue getGenValueSetValue();
   public abstract void setGenValueSetValue(LocalGenValueSetValue genValueSetValue);

   /**
    * @ejb:interface-method view-type="local"
    * @ejb:relation name="rollupgroup-rollupgroupassignments"
    *               role-name="rollupgroupassignment-has-one-rollupgroup"
    *
    * @jboss:relation related-pk-field="rlgCode"
    *                 fk-column="GEN_ROLLUP_GROUP"
    */
   public abstract LocalGenRollupGroup getGenRollupGroup();
   public abstract void setGenRollupGroup(LocalGenRollupGroup genRollupGroup);

   // EntityBean methods

   /**
    * @ejb:create-method view-type="local"
    **/
   public java.lang.Integer ejbCreate (java.lang.Integer RLGA_CODE, Integer RLGA_AD_CMPNY)
      throws CreateException {

      Debug.print("GenRollupGroupAssignment ejbCreate");
      setRlgaCode(RLGA_CODE);
      setRlgaAdCompany(RLGA_AD_CMPNY);
      
      return null;
   }

   public void ejbPostCreate (java.lang.Integer RLGA_CODE, Integer RLGA_AD_CMPNY) 
      throws CreateException { }

} // GenRollupGroupAssignmentBean class
