package com.ejb.genfld;

import java.util.Collection;

import javax.ejb.CreateException;
import javax.ejb.EJBException;

import com.util.AbstractEntityBean;
import com.util.Debug;

/**
 * @ejb:bean name="GenRollupGroupEJB"
 *           display-name="Rollup Group Entity"
 *           type="CMP"
 *           cmp-version="2.x"
 *           view-type="local"
 *           local-jndi-name="omega-ejb/GenRollupGroupEJB"
 *           schema="GenRollupGroup"
 *           primkey-field="rlgCode"
 *       
 * @ejb:pk class="java.lang.Integer"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="gluser"
 *                        role-link="gluserlink"
 *
 * @ejb:permission role-name="gluser"
 *
 * @ejb:interface local-class="com.ejb.genfld.LocalGenRollupGroup"
 *                local-extends="javax.ejb.EJBLocalObject"
 *
 * @ejb:home local-class="com.ejb.genfld.LocalGenRollupGroupHome"
 *           local-extends="javax.ejb.EJBLocalHome"
 *
 * @jboss:persistence table-name="GEN_RLLP_GRP"
 *
 * @jboss:entity-command name="mysql-get-generated-keys"
 *
 */

public abstract class GenRollupGroupBean extends AbstractEntityBean {

   
   // Access methods for persistent fields

   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    * @ejb:pk-field
    *
    * @jboss:column-name name="RLG_CODE"
    *
    * @jboss:persistence auto-increment="true"
    **/
   public abstract java.lang.Integer getRlgCode();
   public abstract void setRlgCode(java.lang.Integer RLG_CODE);

   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="RLG_NM"
    **/
   public abstract java.lang.String getRlgName();
   public abstract void setRlgName(java.lang.String RLG_NM);

   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="RLG_DESC"
    **/
   public abstract java.lang.String getRlgDescription();
   public abstract void setRlgDescription(java.lang.String RLG_DESC);
   
   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="RLG_AD_CMPNY"
    **/
   public abstract java.lang.Integer getRlgAdCompany();
   public abstract void setRlgAdCompany(java.lang.Integer RLG_AD_CMPNY);

   // Access methods for relationship fields

   /**
    * @ejb:interface-method view-type="local"
    * @ejb:relation name="rollupgroup-rollupgroupassignments"
    *               role-name="rollupgroup-has-many-rollupgroupassignments"
    */
   public abstract Collection getGenRollupGroupAssignments();
   public abstract void setGenRollupGroupAssignments(Collection genRollupGroupAssignments);

   // Business methods

   /**
    * @ejb:interface-method view-type="local"
    **/
   public void addGenRollupGroupAssignment(LocalGenRollupGroupAssignment genRollupGroupAssignment) {

      Debug.print("GenRollupGroupBean addGenRollupGroupAssignment");
      try {
         Collection genRollupGroupAssignments = getGenRollupGroupAssignments();
	 genRollupGroupAssignments.add(genRollupGroupAssignment);
      } catch (Exception ex) {
         throw new EJBException(ex.getMessage());
      }
   }

   /**
    * @ejb:interface-method view-type="local"
    **/
   public void dropGenRollupGroupAssignment(LocalGenRollupGroupAssignment genRollupGroupAssignment) {
      
      Debug.print("GenRollupGroupBean dropGenRollupGroupAssignment");
      try {
         Collection genRollupGroupAssignments = getGenRollupGroupAssignments();
	 genRollupGroupAssignments.remove(genRollupGroupAssignment);
      } catch (Exception ex) {
         throw new EJBException(ex.getMessage());
      }
   }

   // EntityBean methods

   /**
    * @ejb:create-method view-type="local"
    **/
   public java.lang.Integer ejbCreate (java.lang.Integer RLG_CODE, java.lang.String RLG_NM, java.lang.String RLG_DESC,
   	  Integer RLG_AD_CMPNY)
      throws CreateException {

      Debug.print("GenRollupGroupBean ejbCreate");
      setRlgCode(RLG_CODE);
      setRlgName(RLG_NM);
      setRlgDescription(RLG_DESC);
      setRlgAdCompany(RLG_AD_CMPNY);
      
      return null;
   }

   public void ejbPostCreate (java.lang.Integer RLG_CODE, java.lang.String RLG_NM, java.lang.String RLG_DESC,
   	  Integer RLG_AD_CMPNY)
      throws CreateException { }

} // GenRllpGrpBean class
