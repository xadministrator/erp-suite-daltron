package com.ejb.genfld;

import javax.ejb.CreateException;

import com.util.AbstractEntityBean;
import com.util.Debug;

/**
 * @ejb:bean name="GenSegmentEJB"
 *           display-name="Segment Entity"
 *           type="CMP"
 *           cmp-version="2.x"
 *           view-type="local"
 *           local-jndi-name="omega-ejb/GenSegmentEJB"
 *           schema="GenSegment"
 *           primkey-field="sgCode"
 *       
 * @ejb:pk class="java.lang.Integer"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="gluser"
 *                        role-link="gluserlink"
 *
 * @ejb:permission role-name="gluser"
 *
 * @ejb:interface local-class="com.ejb.genfld.LocalGenSegment"
 *                local-extends="javax.ejb.EJBLocalObject"
 *
 * @ejb:home local-class="com.ejb.genfld.LocalGenSegmentHome"
 *           local-extends="javax.ejb.EJBLocalHome"
 *
 * @ejb:finder signature="LocalGenSegment findByFlCodeAndSegmentType(java.lang.Integer FL_CODE, char SG_SGMNT_TYP, java.lang.Integer SG_AD_CMPNY)"
 *             query="SELECT OBJECT(sg) FROM GenField fl, IN(fl.genSegments) sg WHERE fl.flCode=?1 AND sg.sgSegmentType=?2 AND sg.sgAdCompany = ?3"
 *
 * @ejb:finder signature="LocalGenSegment findByFlCodeAndVsName(java.lang.Integer FL_CODE, java.lang.String VS_NM, java.lang.Integer SG_AD_CMPNY)"
 *             query="SELECT OBJECT(sg) FROM GenSegment sg WHERE sg.genField.flCode=?1 AND sg.genValueSet.vsName=?2 AND sg.sgAdCompany = ?3"
 *
 * @ejb:finder signature="Collection findByFlCode(java.lang.Integer FL_CODE, java.lang.Integer SG_AD_CMPNY)"
 *             query="SELECT OBJECT(sg) FROM GenField fl, IN(fl.genSegments) sg WHERE fl.flCode=?1 AND sg.sgAdCompany = ?2"
 *
 * @jboss:query signature="Collection findByFlCode(java.lang.Integer FL_CODE, java.lang.Integer SG_AD_CMPNY)"
 *             query="SELECT OBJECT(sg) FROM GenField fl, IN(fl.genSegments) sg WHERE fl.flCode=?1 AND sg.sgAdCompany = ?2 ORDER BY sg.sgSegmentNumber"
 *
 * @ejb:finder signature="LocalGenSegment findByVsCode(java.lang.Integer VS_CODE, java.lang.Integer SG_AD_CMPNY)"
 *             query="SELECT OBJECT(sg) FROM GenValueSet vs, IN(vs.genSegments) sg WHERE vs.vsCode=?1 AND sg.sgAdCompany = ?2"
 * 
 * @ejb:finder signature="LocalGenSegment findByFlCodeAndSgSegmentNumber(java.lang.Integer FL_CODE, short SG_SGMNT_NMBR, java.lang.Integer SG_AD_CMPNY)"
 *             query="SELECT OBJECT(sg) FROM GenField fl, IN(fl.genSegments) sg WHERE fl.flCode=?1 AND sg.sgSegmentNumber=?2 AND sg.sgAdCompany = ?3"
 *
 * @ejb:value-object match="*"
 *             name="GenSegment"
 *
 * @jboss:persistence table-name="GEN_SGMNT"
 *
 * @jboss:entity-command name="mysql-get-generated-keys"
 *
 */

public abstract class GenSegmentBean extends AbstractEntityBean {

   
   // Access methods for persistent fields

   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    * @ejb:pk-field
    *
    * @jboss:column-name name="SG_CODE"
    *
    * @jboss:persistence auto-increment="true"
    **/
   public abstract java.lang.Integer getSgCode();
   public abstract void setSgCode(java.lang.Integer SG_CODE);

   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="SG_NM"
    **/
   public abstract java.lang.String getSgName();
     /**
      * @ejb:interface-method view-type="local"
      **/  
   public abstract void setSgName(java.lang.String SG_NM);

   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="SG_DESC"
    **/
   public abstract java.lang.String getSgDescription();
     /**
      * @ejb:interface-method view-type="local"
      **/  
   public abstract void setSgDescription(java.lang.String SG_DESC);

   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="SG_MAX_SZ"
    **/
   public abstract short getSgMaxSize();
     /**
      * @ejb:interface-method view-type="local"
      **/  
   public abstract void setSgMaxSize(short SG_MAX_SZ);

   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="SG_DFLT_VL"
    **/
   public abstract java.lang.String getSgDefaultValue();
     /**
      * @ejb:interface-method view-type="local"
      **/  
   public abstract void setSgDefaultValue(java.lang.String SG_DFLT_VL);

   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="SG_SGMNT_TYP"
    **/
   public abstract char getSgSegmentType();
     /**
      * @ejb:interface-method view-type="local"
      **/  
   public abstract void setSgSegmentType(char SG_SGMNT_TYP);

   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="SG_SGMNT_NMBR"
    **/
   public abstract short getSgSegmentNumber();
     /**
      * @ejb:interface-method view-type="local"
      **/  
   public abstract void setSgSegmentNumber(short SG_SGMNT_NMBR);
   
   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="SG_AD_CMPNY"
    **/
   public abstract Integer getSgAdCompany();
     /**
      * @ejb:interface-method view-type="local"
      **/  
   public abstract void setSgAdCompany(Integer SG_AD_CMPNY);

   // Access methods for relationship fields

   /**
    * @ejb:interface-method view-type="local"
    * @ejb:relation name="field-segments"
    *               role-name="segment-has-one-field"
    *
    * @jboss:relation related-pk-field="flCode"
    *                 fk-column="GEN_FIELD"
    */
   public abstract LocalGenField getGenField();
   public abstract void setGenField(LocalGenField genField);

   /**
    * @ejb:interface-method view-type="local"
    * @ejb:relation name="valueset-segments"
    *               role-name="segment-has-one-valueset"
    *
    * @jboss:relation related-pk-field="vsCode"
    *                 fk-column="GEN_VALUE_SET"
    */
   public abstract LocalGenValueSet getGenValueSet();
   public abstract void setGenValueSet(LocalGenValueSet genValueSet);

   // No Business methods

   // EntityBean methods

   /**
    * @ejb:create-method view-type="local"
    **/
   public java.lang.Integer ejbCreate (java.lang.Integer SG_CODE, java.lang.String SG_NM, java.lang.String SG_DESC,
      short SG_MAX_SZ, java.lang.String SG_DFLT_VL, char SG_SGMNT_TYP,
      short SG_SGMNT_NMBR, Integer SG_AD_CMPNY)
      throws CreateException {

      Debug.print("GenSegmentBean ejbCreate");
      setSgCode(SG_CODE);
      setSgName(SG_NM);
      setSgDescription(SG_DESC);
      setSgMaxSize(SG_MAX_SZ);
      setSgDefaultValue(SG_DFLT_VL);
      setSgSegmentType(SG_SGMNT_TYP);
      setSgSegmentNumber(SG_SGMNT_NMBR);
      setSgAdCompany(SG_AD_CMPNY);      
      return null;
   }
   
   /**
    * @ejb:create-method view-type="local"
    **/
   public java.lang.Integer ejbCreate (java.lang.String SG_NM, java.lang.String SG_DESC,
      short SG_MAX_SZ, java.lang.String SG_DFLT_VL, char SG_SGMNT_TYP,
      short SG_SGMNT_NMBR, Integer SG_AD_CMPNY)
      throws CreateException {

      Debug.print("GenSegmentBean ejbCreate");
      
      setSgName(SG_NM);
      setSgDescription(SG_DESC);
      setSgMaxSize(SG_MAX_SZ);
      setSgDefaultValue(SG_DFLT_VL);
      setSgSegmentType(SG_SGMNT_TYP);
      setSgSegmentNumber(SG_SGMNT_NMBR);
      setSgAdCompany(SG_AD_CMPNY);      
      return null;
   }

   public void ejbPostCreate (java.lang.Integer SG_CODE, java.lang.String SG_NM, java.lang.String SG_DESC,
      short SG_MAX_SZ, java.lang.String SG_DFLT_VL, char SG_SGMNT_TYP,
      short SG_SGMNT_NMBR, Integer SG_AD_CMPNY)
      throws CreateException { }
   
   public void ejbPostCreate (java.lang.String SG_NM, java.lang.String SG_DESC,
        short SG_MAX_SZ, java.lang.String SG_DFLT_VL, char SG_SGMNT_TYP,
        short SG_SGMNT_NMBR, Integer SG_AD_CMPNY)
        throws CreateException { }

} // GenSgmntBean class
