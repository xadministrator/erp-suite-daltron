package com.ejb.genfld;

import java.util.Collection;

import javax.ejb.CreateException;
import javax.ejb.EJBException;

import com.util.AbstractEntityBean;
import com.util.Debug;

/**
 * @ejb:bean name="GenValueSetEJB"
 *           display-name="Value Set Entity"
 *           type="CMP"
 *           cmp-version="2.x"
 *           view-type="local"
 *           local-jndi-name="omega-ejb/GenValueSetEJB"
 *           schema="GenValueSet"
 *           primkey-field="vsCode"
 *       
 * @ejb:pk class="java.lang.Integer"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="gluser"
 *                        role-link="gluserlink"
 *
 * @ejb:permission role-name="gluser"
 *
 * @ejb:interface local-class="com.ejb.genfld.LocalGenValueSet"
 *                local-extends="javax.ejb.EJBLocalObject"
 *
 * @ejb:home local-class="com.ejb.genfld.LocalGenValueSetHome"
 *           local-extends="javax.ejb.EJBLocalHome"
 *
 * @ejb:finder signature="Collection findVsAll(java.lang.Integer VS_AD_CMPNY)"
 *             query="SELECT OBJECT(vs) FROM GenValueSet vs WHERE vs.vsAdCompany = ?1"
 *
 * @jboss:query signature="Collection findVsAll(java.lang.Integer VS_AD_CMPNY)"
 *             query="SELECT OBJECT(vs) FROM GenValueSet vs, IN(vs.genSegments) sg WHERE vs.vsAdCompany = ?1 ORDER BY sg.sgSegmentNumber"
 * 
 * @ejb:finder signature="LocalGenValueSet findByVsName(java.lang.String VS_NM, java.lang.Integer VS_AD_CMPNY)"
 *             query="SELECT OBJECT(vs) FROM GenValueSet vs WHERE vs.vsName = ?1 AND vs.vsAdCompany = ?2"
 * 
 * @ejb:finder signature="LocalGenValueSet findBySegmentType(char SG_SGMNT_TYP, java.lang.Integer VS_AD_CMPNY)"
 *             query="SELECT OBJECT(vs) FROM GenValueSet vs, IN(vs.genSegments) sg WHERE sg.sgSegmentType = ?1 AND vs.vsAdCompany = ?2"
 *
 * @jboss:persistence table-name="GEN_VL_ST"
 *
 * @jboss:entity-command name="mysql-get-generated-keys"
 *
 */

public abstract class GenValueSetBean extends AbstractEntityBean {

  
   // Acesss methods for persistent fields

   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    * @ejb:pk-field
    *
    * @jboss:column-name name="VS_CODE"
    *
    * @jboss:persistence auto-increment="true"
    **/
   public abstract java.lang.Integer getVsCode();
   public abstract void setVsCode(java.lang.Integer VS_CODE);

   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="VS_NM"
    **/
   public abstract java.lang.String getVsName();
   /**
	* @ejb:interface-method view-type="local"
	**/ 
   public abstract void setVsName(java.lang.String VS_NM);

   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="VS_DESC"
    **/
   public abstract java.lang.String getVsDescription();
   /**
	* @ejb:interface-method view-type="local"
	**/ 
   public abstract void setVsDescription(java.lang.String VS_DESC);

   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="VS_ENBL"
    **/
   public abstract byte getVsEnable();
   /**
	* @ejb:interface-method view-type="local"
	**/ 
   public abstract void setVsEnable(byte VS_ENBL);
   
   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="VS_AD_CMPNY"
    **/
   public abstract Integer getVsAdCompany();
   /**
	* @ejb:interface-method view-type="local"
	**/ 
   public abstract void setVsAdCompany(Integer VS_AD_CMPNY);

   // Access methods for relationship fields

   /**
    * @ejb:interface-method view-type="local"
    * @ejb:relation name="valueset-segments"
    *               role-name="valueset-has-many-segments"
    */
   public abstract Collection getGenSegments();
   public abstract void setGenSegments(Collection genSegments);

   /**
    * @ejb:interface-method view-type="local"
    * @ejb:relation name="valueset-valuesetvalues"
    *               role-name="valueset-has-many-valuesetvalues"
    */
   public abstract Collection getGenValueSetValues();
   public abstract void setGenValueSetValues(Collection genSegments);

   // Select methods

   // Business methods

   /**
    * @ejb:interface-method view-type="local"
    **/
   public void addGenSegment(LocalGenSegment genSegment) {

      Debug.print("GenValueSetBean addGenSegment");
      try {
         Collection genSegments = getGenSegments();
	 genSegments.add(genSegment);
      } catch (Exception ex) {
         throw new EJBException(ex.getMessage());
      }
   }

   /**
    * @ejb:interface-method view-type="local"
    **/
   public void dropGenSegment(LocalGenSegment genSegment) {

      Debug.print("GenValueSetBean dropGenSegment");
      try {
         Collection genSegments = getGenSegments();
	 genSegments.remove(genSegment);
      } catch (Exception ex) {
         throw new EJBException(ex.getMessage());
      }
   }

   /**
    * @ejb:interface-method view-type="local"
    **/
   public void addGenValueSetValue(LocalGenValueSetValue genValueSetValue) {

      Debug.print("GenValueSetBean addGenValueSetValue");
      try {
         Collection genValueSetValues = getGenValueSetValues();
	 genValueSetValues.add(genValueSetValue);
      } catch (Exception ex) {
         throw new EJBException(ex.getMessage());
      }
   }

   /**
    * @ejb:interface-method view-type="local"
    **/
   public void dropGenValueSetValue(LocalGenValueSetValue genValueSetValue) {

      Debug.print("GenValueSetBean dropGenValueSetValue");
      try {
         Collection genValueSetValues = getGenValueSetValues();
	 genValueSetValues.remove(genValueSetValue);
      } catch (Exception ex) {
         throw new EJBException(ex.getMessage());
      }
   } 

   // EntityBean methods

   /**
    * @ejb:create-method view-type="local"
    **/
   public java.lang.Integer ejbCreate (java.lang.Integer VS_CODE, java.lang.String VS_NM, java.lang.String VS_DESC,
      byte VS_ENBL, Integer VS_AD_CMPNY)
      throws CreateException {

      Debug.print("GenValueSetBean ejbCreate");
      setVsCode(VS_CODE);
      setVsName(VS_NM);
      setVsDescription(VS_DESC);
      setVsEnable(VS_ENBL);
      setVsAdCompany(VS_AD_CMPNY);
      
      return null;
   }
   
   /**
    * @ejb:create-method view-type="local"
    **/
   public java.lang.Integer ejbCreate (java.lang.String VS_NM, java.lang.String VS_DESC,
      byte VS_ENBL, Integer VS_AD_CMPNY)
      throws CreateException {

      Debug.print("GenValueSetBean ejbCreate");
      
      setVsName(VS_NM);
      setVsDescription(VS_DESC);
      setVsEnable(VS_ENBL);
      setVsAdCompany(VS_AD_CMPNY);
      
      return null;
   }

   public void ejbPostCreate (java.lang.Integer VS_CODE, java.lang.String VS_NM, java.lang.String VS_DESC,
      byte VS_ENBL, Integer VS_AD_CMPNY)
      throws CreateException { }
   
   
   public void ejbPostCreate (java.lang.String VS_NM, java.lang.String VS_DESC,
        byte VS_ENBL, Integer VS_AD_CMPNY)
        throws CreateException { }

} // GenVlStBean class
