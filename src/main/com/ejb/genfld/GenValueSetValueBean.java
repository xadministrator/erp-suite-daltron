package com.ejb.genfld;

import java.util.Collection;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;

import com.util.AbstractEntityBean;
import com.util.Debug;

/**
 * @ejb:bean name="GenValueSetValueEJB"
 *           display-name="Value Set Value Entity"
 *           type="CMP"
 *           cmp-version="2.x"
 *           view-type="local"
 *           local-jndi-name="omega-ejb/GenValueSetValueEJB"
 *           schema="GenValueSetValue"
 *           primkey-field="vsvCode"
 *       
 * @ejb:pk class="java.lang.Integer"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="gluser"
 *                        role-link="gluserlink"
 *
 * @ejb:permission role-name="gluser"
 *
 * @ejb:interface local-class="com.ejb.genfld.LocalGenValueSetValue"
 *                local-extends="javax.ejb.EJBLocalObject"
 *
 * @ejb:home local-class="com.ejb.genfld.LocalGenValueSetValueHome"
 *           local-extends="javax.ejb.EJBLocalHome"
 *
 * @ejb:finder signature="LocalGenValueSetValue findByVsCodeAndVsvValue(java.lang.Integer VS_CODE, java.lang.String VSV_VL, java.lang.Integer VSV_AD_CMPNY)"
 *             query="SELECT OBJECT(vsv) FROM GenValueSet vs, IN(vs.genValueSetValues) vsv WHERE vs.vsCode=?1 AND vsv.vsvValue=?2 AND vsv.vsvAdCompany = ?3"
 *
 * @ejb:finder signature="LocalGenValueSetValue findByVsvValueAndVsName(java.lang.String VSV_VL, java.lang.String VS_NM, java.lang.Integer VSV_AD_CMPNY)"
 *             query="SELECT OBJECT(vsv) FROM GenValueSet vs, IN(vs.genValueSetValues) vsv WHERE vsv.vsvValue=?1 AND vs.vsName=?2 AND vsv.vsvAdCompany = ?3"
 *
 * @ejb:finder signature="Collection findByVsName(java.lang.String VS_NM, java.lang.Integer VSV_AD_CMPNY)"
 *             query="SELECT OBJECT(vsv) FROM GenValueSet vs, IN(vs.genValueSetValues) vsv WHERE vs.vsName=?1 AND vsv.vsvAdCompany = ?2"
 *
 * @jboss:query signature="Collection findByVsName(java.lang.String VS_NM, java.lang.Integer VSV_AD_CMPNY)"
 *             query="SELECT OBJECT(vsv) FROM GenValueSet vs, IN(vs.genValueSetValues) vsv WHERE vs.vsName=?1 AND vsv.vsvAdCompany = ?2 ORDER BY vsv.vsvValue"
 *
 * @ejb:finder signature="Collection findByVsvDescriptionAndVsName(java.lang.String VSV_DESC, java.lang.String VS_NM, java.lang.Integer VSV_AD_CMPNY)"
 *             query="SELECT OBJECT(vsv) FROM GenValueSet vs, IN(vs.genValueSetValues) vsv WHERE vsv.vsvDescription LIKE CONCAT('%',CONCAT(?1,'%')) AND vs.vsName=?2 AND vsv.vsvParent=0 AND vsv.vsvAdCompany = ?3"
 *
 * @jboss:query signature="Collection findByVsvDescriptionAndVsName(java.lang.String VSV_DESC, java.lang.String VS_NM, java.lang.Integer VSV_AD_CMPNY)"
 *             query="SELECT OBJECT(vsv) FROM GenValueSet vs, IN(vs.genValueSetValues) vsv WHERE vsv.vsvDescription LIKE CONCAT('%',CONCAT(?1,'%')) AND vs.vsName=?2 AND vsv.vsvParent=0 AND vsv.vsvAdCompany = ?3 ORDER BY vsv.vsvValue"
 * 
 * @ejb:finder signature="Collection findByVsvValueRangeAndVsName(java.lang.String VSV_VL_FRM, java.lang.String VSV_VL_TO, java.lang.String VS_NM, java.lang.Integer VSV_AD_CMPNY)"
 *             query="SELECT OBJECT(vsv) FROM GenValueSet vs, IN(vs.genValueSetValues) vsv WHERE vsv.vsvValue BETWEEN ?1 AND ?2 AND vs.vsName=?3 AND vsv.vsvAdCompany = ?4"
 * 
 * @jboss:query signature="Collection findByVsvValueRangeAndVsName(java.lang.String VSV_VL_FRM, java.lang.String VSV_VL_TO, java.lang.String VS_NM, java.lang.Integer VSV_AD_CMPNY)"
 *             query="SELECT OBJECT(vsv) FROM GenValueSet vs, IN(vs.genValueSetValues) vsv WHERE vsv.vsvValue BETWEEN ?1 AND ?2 AND vs.vsName=?3 AND vsv.vsvAdCompany = ?4 ORDER BY vsv.vsvValue"
 * 
 * @ejb:finder signature="LocalGenValueSetValue findByVsCodeAndVsvDescription(java.lang.Integer VS_CODE, java.lang.String VSV_DESC, java.lang.Integer VSV_AD_CMPNY)"
 *             query="SELECT OBJECT(vsv) FROM GenValueSet vs, IN(vs.genValueSetValues) vsv WHERE vs.vsCode=?1 AND vsv.vsvDescription LIKE ?2 AND vsv.vsvAdCompany = ?3"
 * 
 * @ejb:finder signature="Collection findByQlAccountType(java.lang.String QL_ACCNT_TYP, java.lang.Integer VSV_AD_CMPNY)"
 *             query="SELECT OBJECT(vsv) FROM GenQualifier ql, IN(ql.genValueSetValues) vsv WHERE ql.qlAccountType=?1 AND vsv.vsvParent=0 AND vsv.vsvAdCompany = ?2" 
 *
 * @ejb:select signature="Collection ejbSelectGeneric(java.lang.String jbossQl, java.lang.Object[] args)"
 *             result-type-mapping="Local"
 *             method-intf="LocalHome"
 *             query="SELECT OBJECT(vsv) FROM GenValueSetValue vsv"
 *
 * @jboss:query signature="Collection ejbSelectGeneric(java.lang.String jbossQl, java.lang.Object[] args)"
 *                         dynamic="true"
 *
 * @jboss:persistence table-name="GEN_VL_ST_VL"
 *
 * @jboss:entity-command name="mysql-get-generated-keys"
 *
 */

public abstract class GenValueSetValueBean extends AbstractEntityBean {

   
   // Access methods for persistent fields

   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    * @ejb:pk-field
    *
    * @jboss:column-name name="VSV_CODE"
    *
    * @jboss:persistence auto-increment="true"
    **/
   public abstract java.lang.Integer getVsvCode();
   public abstract void setVsvCode(java.lang.Integer VSV_CODE);

   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="VSV_VL"
    **/
   public abstract java.lang.String getVsvValue();
   /**
	* @ejb:interface-method view-type="local"
	**/ 
   public abstract void setVsvValue(java.lang.String VSV_VL);

   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="VSV_DESC"
    **/
   public abstract java.lang.String getVsvDescription();
   /**
	* @ejb:interface-method view-type="local"
	**/ 
   public abstract void setVsvDescription(java.lang.String VSV_DESC);

   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="VSV_PRNT"
    **/
   public abstract byte getVsvParent();
   /**
	* @ejb:interface-method view-type="local"
	**/ 
   public abstract void setVsvParent(byte VSV_PRNT);

   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="VSV_LVL"
    **/
   public abstract short getVsvLevel();
   /**
	* @ejb:interface-method view-type="local"
	**/ 
   public abstract void setVsvLevel(short VSV_LVL);

   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="VSV_ENBL"
    **/
   public abstract byte getVsvEnable();
   /**
	* @ejb:interface-method view-type="local"
	**/ 
   public abstract void setVsvEnable(byte VSV_ENBL);
   
   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="VSV_AD_CMPNY"
    **/
   public abstract Integer getVsvAdCompany();
   /**
	* @ejb:interface-method view-type="local"
	**/ 
   public abstract void setVsvAdCompany(Integer VSV_AD_CMPNY);

   // Access methods for relationship fields
   
   /**
    * @ejb:interface-method view-type="local"
    * @ejb:relation name="valuesetvalue-childranges"
    *               role-name="valuesetvalue-has-many-childranges"
    */
   public abstract Collection getGenChildRanges();
   public abstract void setGenChildRanges(Collection genChildRanges);

   /**
    * @ejb:interface-method view-type="local"
    * @ejb:relation name="valueset-valuesetvalues"
    *               role-name="valuesetvalue-has-one-valueset"
    *
    * @jboss:relation related-pk-field="vsCode"
    *                 fk-column="GEN_VALUE_SET"
    */
   public abstract LocalGenValueSet getGenValueSet();
   /**
	* @ejb:interface-method view-type="local"
	**/
   public abstract void setGenValueSet(LocalGenValueSet genValueSet); 

   /**
    * @ejb:interface-method view-type="local"
    * @ejb:relation name="qualifier-valuesetvalues"
    *               role-name="valuesetvalue-has-one-qualifier"
    *
    * @jboss:relation related-pk-field="qlCode"
    *                 fk-column="GEN_QUALIFIER"
    */
   public abstract LocalGenQualifier getGenQualifier();
   /**
	* @ejb:interface-method view-type="local"
	**/
   public abstract void setGenQualifier(LocalGenQualifier genQualifier);

   /**
    * @ejb:interface-method view-type="local"
    * @ejb:relation name="valuesetvalue-rollupgroupassignments"
    *               role-name="valuesetvalue-has-many-rollupgroupassignments"
    */
   public abstract Collection getGenRollupGroupAssignments();
   public abstract void setGenRollupGroupAssignments(Collection genRollupGroupAssignments);

   /**
    * @jboss:dynamic-ql
    */
    public abstract Collection ejbSelectGeneric(java.lang.String jbossQl, java.lang.Object[] args)
       throws FinderException;
       
    

   // BUSINESS METHODS
   
   /**
    * @ejb:home-method view-type="local"
    */
    public Collection ejbHomeGetVsvByCriteria(java.lang.String jbossQl, java.lang.Object[] args)
       throws FinderException {
       	
       return ejbSelectGeneric(jbossQl, args);
    }
    

   /**
    * @ejb:interface-method view-type="local"
    **/
   public void addGenChildRange(LocalGenChildRange genChildRange) {

      Debug.print("GenValueSetValueBean addGenChildRange");
      try {
         Collection genChildRanges = getGenChildRanges();
	 genChildRanges.add(genChildRange);
      } catch (Exception ex) {
         throw new EJBException(ex.getMessage());
      }
   }

   /**
    * @ejb:interface-method view-type="local"
    **/
   public void dropGenChildRange(LocalGenChildRange genChildRange) {
   
      Debug.print("GenValueSetValueBean dropGenChildRange");
      try {
         Collection genChildRanges = getGenChildRanges();
	 genChildRanges.remove(genChildRange);
      } catch (Exception ex) {
         throw new EJBException(ex.getMessage());
      }
   }

   /**
    * @ejb:interface-method view-type="local"
    **/
   public void addGenRollupGroupAssignment(LocalGenRollupGroupAssignment genRollupGroupAssignment) {

      Debug.print("GenValueSetValueBean addGenRollupGroupAssignment");
      try {
         Collection genRollupGroupAssignments = getGenRollupGroupAssignments();
	 genRollupGroupAssignments.add(genRollupGroupAssignment);
      } catch (Exception ex) {
         throw new EJBException(ex.getMessage());
      }
   }

   /**
    * @ejb:interface-method view-type="local"
    **/
   public void dropGenRollupGroupAssignment(LocalGenRollupGroupAssignment genRollupGroupAssignment) {
   
      Debug.print("GenValueSetValueBean dropGenRollupGroupAssignment");
      try {
         Collection genRollupGroupAssignments = getGenRollupGroupAssignments();
	 genRollupGroupAssignments.remove(genRollupGroupAssignment);
      } catch (Exception ex) {
         throw new EJBException(ex.getMessage());
      }
   }

   // EntityBean methods

   /**
    * @ejb:create-method view-type="local"
    **/
   public java.lang.Integer ejbCreate (java.lang.Integer VSV_CODE, java.lang.String VSV_VL, java.lang.String VSV_DESC,
      byte VSV_PRNT, short VSV_LVL, byte VSV_ENBL, Integer VSV_AD_CMPNY)
      throws CreateException {

      Debug.print("GenValueSetValueBean ejbCreate");
      setVsvCode(VSV_CODE);
      setVsvValue(VSV_VL);
      setVsvDescription(VSV_DESC);
      setVsvParent(VSV_PRNT);
      setVsvLevel(VSV_LVL);
      setVsvEnable(VSV_ENBL);
      setVsvAdCompany(VSV_AD_CMPNY);
      
      return null;
   }

   /**
    * @ejb:create-method view-type="local"
    **/
   public java.lang.Integer ejbCreate (java.lang.String VSV_VL, java.lang.String VSV_DESC,
      byte VSV_PRNT, short VSV_LVL, byte VSV_ENBL, Integer VSV_AD_CMPNY)
      throws CreateException {

      Debug.print("GenValueSetValueBean ejbCreate");

      setVsvValue(VSV_VL);
      setVsvDescription(VSV_DESC);
      setVsvParent(VSV_PRNT);
      setVsvLevel(VSV_LVL);
      setVsvEnable(VSV_ENBL);
      setVsvAdCompany(VSV_AD_CMPNY);
      
      return null;
   }

   public void ejbPostCreate (java.lang.Integer VSV_CODE, java.lang.String VSV_VL, java.lang.String VSV_DESC,
      byte VSV_PRNT, short VSV_LVL, byte VSV_ENBL, Integer VSV_AD_CMPNY)
      throws CreateException { }

   public void ejbPostCreate (java.lang.String VSV_VL, java.lang.String VSV_DESC,
      byte VSV_PRNT, short VSV_LVL, byte VSV_ENBL, Integer VSV_AD_CMPNY)
      throws CreateException { }

} // GenValueSetValueBean class
