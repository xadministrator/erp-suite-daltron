package com.ejb.gl;

import javax.ejb.CreateException;

import com.util.AbstractEntityBean;
import com.util.Debug;

/**
 * @ejb:bean name="GlAccountRangeEJB"
 *           display-name="Account Range Entity"
 *           type="CMP"
 *           cmp-version="2.x"
 *           view-type="local"
 *           local-jndi-name="omega-ejb/GlAccountRangeEJB"
 *           schema="GlAccountRange"
 *           primkey-field="arCode"
 *       
 * @ejb:pk class="java.lang.Integer"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="gluser"
 *                        role-link="gluserlink"
 *
 * @ejb:permission role-name="gluser"
 *
 * @ejb:interface local-class="com.ejb.gl.LocalGlAccountRange"
 *                local-extends="javax.ejb.EJBLocalObject"
 *
 * @ejb:home local-class="com.ejb.gl.LocalGlAccountRangeHome"
 *           local-extends="javax.ejb.EJBLocalHome"
 *
 * @jboss:persistence table-name="GL_ACCNT_RNG"
 *
 * @jboss:entity-command name="mysql-get-generated-keys"
 *
 */

public abstract class GlAccountRangeBean extends AbstractEntityBean {

   private static final int IDGEN_START = (int)System.currentTimeMillis();
   private static int idgen = IDGEN_START;

   // Access methods for persistent fields

   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    * @ejb:pk-field
    *
    * @jboss:column-name name="AR_CODE"
    *
    * @jboss:persistence auto-increment="true"
    **/
   public abstract java.lang.Integer getArCode();
   public abstract void setArCode(java.lang.Integer AR_CODE);

   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="AR_LN"
    **/
   public abstract short getArLine();
   /**
    * @ejb:interface-method view-type="local"
    **/
   public abstract void setArLine(short AR_LN);

   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="AR_ACCNT_LW"
    **/
   public abstract java.lang.String getArAccountLow();
   /**
    * @ejb:interface-method view-type="local"
    **/
   public abstract void setArAccountLow(java.lang.String AR_ACCNT_LW);

   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="AR_ACCNT_HGH"
    **/
   public abstract java.lang.String getArAccountHigh();
   /**
    * @ejb:interface-method view-type="local"
    **/
   public abstract void setArAccountHigh(java.lang.String AR_ACCNT_HGH);
   
   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="AR_AD_CMPNY"
    **/
   public abstract java.lang.Integer getArAdCompany();
   /**
    * @ejb:interface-method view-type="local"
    **/
   public abstract void setArAdCompany(java.lang.Integer AR_AD_CMPNY);

   // Access methods for relationship fields

   /**
    * @ejb:interface-method view-type="local"
    * @ejb:relation name="organization-accountranges"
    *               role-name="accountrange-has-one-organization"
    *               cascade-delete="yes"
    *
    * @jboss:relation related-pk-field="orgCode"
    *                 fk-column="GL_ORGANIZATION"
    */
   public abstract LocalGlOrganization getGlOrganization();
   public abstract void setGlOrganization(LocalGlOrganization glOrganization);

   // EntityBean methods

   /**
    * @ejb:create-method view-type="local"
    **/
   public java.lang.Integer ejbCreate (java.lang.Integer AR_CODE, short AR_LN, java.lang.String AR_ACCNT_LW,
      java.lang.String AR_ACCNT_HGH, java.lang.Integer AR_AD_CMPNY)
      throws CreateException {

      Debug.print("GlAccountRangeBean ejbCreate");
      setArCode(AR_CODE);
      setArLine(AR_LN);
      setArAccountLow(AR_ACCNT_LW);
      setArAccountHigh(AR_ACCNT_HGH);
      setArAdCompany(AR_AD_CMPNY);
      
      return null;
   }

   /**
    * @ejb:create-method view-type="local"
    **/
    public java.lang.Integer ejbCreate (short AR_LN, java.lang.String AR_ACCNT_LW,
      java.lang.String AR_ACCNT_HGH, java.lang.Integer AR_AD_CMPNY)
      throws CreateException {

      Debug.print("GlAccountRangeBean ejbCreate");

      setArLine(AR_LN);
      setArAccountLow(AR_ACCNT_LW);
      setArAccountHigh(AR_ACCNT_HGH);
      setArAdCompany(AR_AD_CMPNY);
      
      return null;
   }

   public void ejbPostCreate (java.lang.Integer AR_CODE, short AR_LN, java.lang.String AR_ACCNT_LW,
      java.lang.String AR_ACCNT_HGH, java.lang.Integer AR_AD_CMPNY)
      throws CreateException { }

    public void ejbPostCreate (short AR_LN, java.lang.String AR_ACCNT_LW,
      java.lang.String AR_ACCNT_HGH, java.lang.Integer AR_AD_CMPNY)
      throws CreateException { }

} // GlAccountRangeBean class
