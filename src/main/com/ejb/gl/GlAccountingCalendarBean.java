package com.ejb.gl;

import java.util.Collection;

import javax.ejb.CreateException;
import javax.ejb.EJBException;

import com.util.AbstractEntityBean;
import com.util.Debug;

/**
 * @ejb:bean name="GlAccountingCalendarEJB"
 *           display-name="Accounting Calendar Entity"
 *           type="CMP"
 *           cmp-version="2.x"
 *           view-type="local"
 *           local-jndi-name="omega-ejb/GlAccountingCalendarEJB"
 *           schema="GlAccountingCalendar"
 *           primkey-field="acCode"
 *       
 * @ejb:pk class="java.lang.Integer"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="gluser"
 *                        role-link="gluserlink"
 *
 * @ejb:permission role-name="gluser"
 *
 * @ejb:interface local-class="com.ejb.gl.LocalGlAccountingCalendar"
 *                local-extends="javax.ejb.EJBLocalObject"
 *
 * @ejb:home local-class="com.ejb.gl.LocalGlAccountingCalendarHome"
 *           local-extends="javax.ejb.EJBLocalHome"
 *
 * @ejb:finder signature="Collection findAcAll(java.lang.Integer AC_AD_CMPNY)"
 *             query="SELECT OBJECT(ac) FROM GlAccountingCalendar ac WHERE ac.acAdCompany = ?1"
 *
 * @jboss:query signature="Collection findAcAll(java.lang.Integer AC_AD_CMPNY)"
 *             query="SELECT OBJECT(ac) FROM GlAccountingCalendar ac WHERE ac.acAdCompany = ?1 ORDER BY ac.acName"
 *
 * @ejb:finder signature="Collection findAcAllWithAcv(java.lang.Integer AC_AD_CMPNY)"
 *             query="SELECT DISTINCT OBJECT(ac) FROM GlAccountingCalendar ac, IN(ac.glAccountingCalendarValues) acv WHERE acv.acvCode > 0 AND ac.acAdCompany = ?1"
 *
 * @ejb:finder signature="LocalGlAccountingCalendar findByAcName(java.lang.String AC_NM, java.lang.Integer AC_AD_CMPNY)"
 *             query="SELECT OBJECT(ac) FROM GlAccountingCalendar ac WHERE ac.acName = ?1 AND ac.acAdCompany = ?2"
 *
 * @ejb:finder signature="LocalGlAccountingCalendar findByAcYear(java.lang.Integer AC_YR, java.lang.Integer AC_AD_CMPNY)"
 *             query="SELECT OBJECT(ac) FROM GlAccountingCalendar ac WHERE ac.acYear = ?1 AND ac.acAdCompany = ?2"
 *
 * @jboss:persistence table-name="GL_ACCNTNG_CLNDR"
 *
 * @jboss:entity-command name="mysql-get-generated-keys"
 *
 */

public abstract class GlAccountingCalendarBean extends AbstractEntityBean {


   // Access methods for persistent fields

   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    * @ejb:pk-field
    *
    * @jboss:column-name name="AC_CODE"
    *
    * @jboss:persistence auto-increment="true"
    **/
   public abstract java.lang.Integer getAcCode();
   public abstract void setAcCode(java.lang.Integer AC_CODE);

   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="AC_NM"
    **/
   public abstract java.lang.String getAcName();
   /**
    * @ejb:interface-method view-type="local"
    **/
   public abstract void setAcName(java.lang.String AC_NM);

   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="AC_DESC"
    **/
   public abstract java.lang.String getAcDescription();
   /**
    * @ejb:interface-method view-type="local"
    **/
   public abstract void setAcDescription(java.lang.String AC_DESC);
   
   
   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="AC_YR"
    **/
   public abstract java.lang.Integer getAcYear();
   /**
    * @ejb:interface-method view-type="local"
    **/
   public abstract void setAcYear(java.lang.Integer AC_YR);
   
   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="AC_AD_CMPNY"
    **/
   public abstract java.lang.Integer getAcAdCompany();
   /**
    * @ejb:interface-method view-type="local"
    **/
   public abstract void setAcAdCompany(java.lang.Integer AC_AD_CMPNY);

   // Access methods for relationship fields

   /**
    * @ejb:interface-method view-type="local"
    * @ejb:relation name="accountingcalendar-setofbooks"
    *               role-name="accountingcalendar-has-many-setofbooks"
    */
   public abstract Collection getGlSetOfBooks();
   public abstract void setGlSetOfBooks(Collection glSetOfBooks);

   /**
    * @ejb:interface-method view-type="local"
    * @ejb:relation name="accountingcalendar-accountingcalendarvalues"
    *               role-name="accountingcalendar-has-many-accountingcalendarvalues"
    */
   public abstract Collection getGlAccountingCalendarValues();
   public abstract void setGlAccountingCalendarValues(Collection glAccountingCalendarValues);

   /**
    * @ejb:interface-method view-type="local"
    * @ejb:relation name="periodtype-accountingcalendars"
    *               role-name="accountingcalendar-has-one-periodtype"
    *
    * @jboss:relation related-pk-field="ptCode"
    *                 fk-column="GL_PERIOD_TYPE"
    */
   public abstract LocalGlPeriodType getGlPeriodType();
   public abstract void setGlPeriodType(LocalGlPeriodType glPeriodType);

   // Business methods

   /**
    * @ejb:interface-method view-type="local"
    **/
   public void addGlSetOfBook(LocalGlSetOfBook glSetOfBook) {

      Debug.print("GlAccountingCalendarBean addGlSetOfBook");
      try {
         Collection glSetOfBooks = getGlSetOfBooks();
	 glSetOfBooks.add(glSetOfBook);
      } catch (Exception ex) {
         throw new EJBException(ex.getMessage());
      }
   }

   /**
    * @ejb:interface-method view-type="local"
    **/
   public void dropGlSetOfBook(LocalGlSetOfBook glSetOfBook) {

      Debug.print("GlAccountingCalendarBean dropGlSetOfBook");
      try {
         Collection glSetOfBooks = getGlSetOfBooks();
	 glSetOfBooks.remove(glSetOfBook);
      } catch (Exception ex) {
         throw new EJBException(ex.getMessage());
      }
   }

   /**
    * @ejb:interface-method view-type="local"
    **/
   public void addGlAccountingCalendarValue(LocalGlAccountingCalendarValue glAccountingCalendarValue) {

      Debug.print("GlAccountingCaledarBean addGlAccountingCalendarValue");
      try {
         Collection glAccountingCalendarValues = getGlAccountingCalendarValues();
	 glAccountingCalendarValues.add(glAccountingCalendarValue);
      } catch (Exception ex) {
         throw new EJBException(ex.getMessage());
      }
   }

   /**
    * @ejb:interface-method view-type="local"
    **/
   public void dropGlAccountingCalendarValue(LocalGlAccountingCalendarValue glAccountingCalendarValue) {

      Debug.print("GlAccountingCalendarBean dropGlAccountingCalendarValue");
      try {
         Collection glAccountingCalendarValues = getGlAccountingCalendarValues();
	 glAccountingCalendarValues.remove(glAccountingCalendarValue);
      } catch (Exception ex) {
         throw new EJBException(ex.getMessage());
      }
   }

   // EntityBean methods

   /**
    * @ejb:create-method view-type="local"
    **/
   public java.lang.Integer ejbCreate (java.lang.Integer AC_CODE,
      java.lang.String AC_NM, java.lang.String AC_DESC, java.lang.Integer AC_YR, java.lang.Integer AC_AD_CMPNY)
      throws CreateException {

      Debug.print("GlAccountingCalendarBean ejbCreate");
      
      setAcCode(AC_CODE);
      setAcName(AC_NM);
      setAcDescription(AC_DESC);
      setAcYear(AC_YR);
      setAcAdCompany(AC_AD_CMPNY);
      
      return null;
   }

   /**
    * @ejb:create-method view-type="local"
    **/
   public java.lang.Integer ejbCreate (
      java.lang.String AC_NM, java.lang.String AC_DESC, java.lang.Integer AC_YR, java.lang.Integer AC_AD_CMPNY)
      throws CreateException {

      Debug.print("GlAccountingCalendarBean ejbCreate");
      
      setAcName(AC_NM);
      setAcDescription(AC_DESC);
      setAcYear(AC_YR);
      setAcAdCompany(AC_AD_CMPNY);
      
      return null;
   }

   public void ejbPostCreate (java.lang.Integer AC_CODE,
      java.lang.String AC_NM, java.lang.String AC_DESC, java.lang.Integer AC_YR, java.lang.Integer AC_AD_CMPNY)
      throws CreateException  { }
  
   public void ejbPostCreate (
      java.lang.String AC_NM, java.lang.String AC_DESC, java.lang.Integer AC_YR, java.lang.Integer AC_AD_CMPNY)
      throws CreateException  { }
  
} // GlAccountingCalendarBean class
