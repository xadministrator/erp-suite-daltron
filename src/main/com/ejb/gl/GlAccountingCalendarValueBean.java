package com.ejb.gl;

import java.util.Collection;
import java.util.Date;

import javax.ejb.CreateException;
import javax.ejb.EJBException;

import com.util.AbstractEntityBean;
import com.util.Debug;

/**
 * @ejb:bean name="GlAccountingCalendarValueEJB"
 *           display-name="Accounting Calendar Value Entity"
 *           type="CMP"
 *           cmp-version="2.x"
 *           view-type="local"
 *           local-jndi-name="omega-ejb/GlAccountingCalendarValueEJB"
 *           schema="GlAccountingCalendarValue"
 *           primkey-field="acvCode"
 *       
 * @ejb:pk class="java.lang.Integer"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="gluser"
 *                        role-link="gluserlink"
 *
 * @ejb:permission role-name="gluser"
 *
 * @ejb:interface local-class="com.ejb.gl.LocalGlAccountingCalendarValue"
 *                local-extends="javax.ejb.EJBLocalObject"
 *
 * @ejb:home local-class="com.ejb.gl.LocalGlAccountingCalendarValueHome"
 *           local-extends="javax.ejb.EJBLocalHome"
 *
 * @ejb:finder signature="LocalGlAccountingCalendarValue findByAcCodeAndAcvPeriodPrefix(java.lang.Integer AC_CODE, java.lang.String ACV_PRD_PRFX, java.lang.Integer ACV_AD_CMPNY)"
 *             query="SELECT OBJECT(acv) FROM GlAccountingCalendar ac, IN(ac.glAccountingCalendarValues) acv WHERE ac.acCode=?1 AND acv.acvPeriodPrefix=?2 AND acv.acvAdCompany = ?3"
 *
 * @ejb:finder signature="LocalGlAccountingCalendarValue findByAcCodeAndAcvPeriodNumber(java.lang.Integer AC_CODE, short ACV_PRD_NMBR, java.lang.Integer ACV_AD_CMPNY)"
 *             query="SELECT OBJECT(acv) FROM GlAccountingCalendar ac, IN(ac.glAccountingCalendarValues) acv WHERE ac.acCode=?1 AND acv.acvPeriodNumber=?2 AND acv.acvAdCompany = ?3"
 *
 * @ejb:finder signature="Collection findByAcCodeAndPtPeriodPerYear(java.lang.Integer AC_CODE, short PT_PRD_PER_YR, java.lang.Integer ACV_AD_CMPNY)"
 *             query="SELECT OBJECT(acv) FROM GlPeriodType pt, IN(pt.glAccountingCalendars) ac, IN(ac.glAccountingCalendarValues) acv WHERE ac.acCode=?1 AND acv.acvPeriodNumber  > 0 AND acv.acvPeriodNumber <= ?2 AND acv.acvAdCompany = ?3"
 *
 * @jboss:query signature="Collection findByAcCodeAndPtPeriodPerYear(java.lang.Integer AC_CODE, short PT_PRD_PER_YR, java.lang.Integer ACV_AD_CMPNY)"
 *             query="SELECT OBJECT(acv) FROM GlPeriodType pt, IN(pt.glAccountingCalendars) ac, IN(ac.glAccountingCalendarValues) acv WHERE ac.acCode=?1 AND acv.acvPeriodNumber  > 0 AND acv.acvPeriodNumber <= ?2 AND acv.acvAdCompany = ?3"
 *
 * @ejb:finder signature="Collection findByAcCodeAndAcvStatus(java.lang.Integer AC_CODE, char ACV_STATUS, java.lang.Integer ACV_AD_CMPNY)"
 *             query="SELECT OBJECT(acv) FROM GlAccountingCalendar ac, IN(ac.glAccountingCalendarValues) acv WHERE ac.acCode=?1 AND acv.acvStatus=?2 AND acv.acvAdCompany = ?3 ORDER BY acv.acvPeriodNumber"
 *
 * @jboss:query signature="Collection findByAcCodeAndAcvStatus(java.lang.Integer AC_CODE, char ACV_STATUS, java.lang.Integer ACV_AD_CMPNY)"
 *             query="SELECT OBJECT(acv) FROM GlAccountingCalendar ac, IN(ac.glAccountingCalendarValues) acv WHERE ac.acCode=?1 AND acv.acvStatus=?2 AND acv.acvAdCompany = ?3 ORDER BY acv.acvPeriodNumber"
 *
 * @ejb:finder signature="LocalGlAccountingCalendarValue findByAcCodeAndDate(java.lang.Integer AC_CODE, java.util.Date DT, java.lang.Integer ACV_AD_CMPNY)"
 *             query="SELECT OBJECT(acv) FROM GlAccountingCalendar ac, IN(ac.glAccountingCalendarValues) acv WHERE ac.acCode=?1 AND acv.acvDateFrom <= ?2 AND acv.acvDateTo >= ?2 AND acv.acvAdCompany = ?3"
 *
 * @jboss:query signature="LocalGlAccountingCalendarValue findByAcCodeAndDate(java.lang.Integer AC_CODE, java.util.Date DT, java.lang.Integer ACV_AD_CMPNY)"
 *             query="SELECT OBJECT(acv) FROM GlAccountingCalendar ac, IN(ac.glAccountingCalendarValues) acv WHERE ac.acCode=?1 AND acv.acvDateFrom <= ?2 AND acv.acvDateTo >= ?2 AND acv.acvAdCompany = ?3"
 *
 * @ejb:finder signature="Collection findAcvByAcCodeAndAcvPeriodNumberAndAcvStatus(java.lang.Integer AC_CODE, short ACV_PRD_NMBR, char ACV_STATUS, java.lang.Integer ACV_AD_CMPNY)"
 *             query="SELECT OBJECT(acv) FROM GlAccountingCalendar ac, IN(ac.glAccountingCalendarValues) acv WHERE ac.acCode=?1 AND acv.acvPeriodNumber < ?2 AND acv.acvStatus=?3 AND acv.acvAdCompany = ?4"
 *
 * @ejb:finder signature="Collection findOpenAndFutureEntryAcvByAcCode(java.lang.Integer AC_CODE, char ACV_STATUS1, char ACV_STATUS2, java.lang.Integer ACV_AD_CMPNY)"
 *             query="SELECT OBJECT(acv) FROM GlAccountingCalendar ac, IN(ac.glAccountingCalendarValues) acv WHERE ac.acCode=?1 AND (acv.acvStatus=?2 OR acv.acvStatus=?3) AND acv.acvAdCompany = ?4"
 *
 * @jboss:query signature="Collection findOpenAndFutureEntryAcvByAcCode(java.lang.Integer AC_CODE, char ACV_STATUS1, char ACV_STATUS2, java.lang.Integer ACV_AD_CMPNY)"
 *             query="SELECT OBJECT(acv) FROM GlAccountingCalendar ac, IN(ac.glAccountingCalendarValues) acv WHERE ac.acCode=?1 AND (acv.acvStatus=?2 OR acv.acvStatus=?3) AND acv.acvAdCompany = ?4 ORDER BY acv.acvPeriodNumber"
 *
 * @ejb:finder signature="Collection findCurrentAndFutureAcvByAcCodeAndCurrentDate(java.lang.Integer AC_CODE, java.util.Date ACV_DT, java.lang.Integer ACV_AD_CMPNY)"
 *             query="SELECT OBJECT(acv) FROM GlAccountingCalendar ac, IN(ac.glAccountingCalendarValues) acv WHERE ac.acCode=?1 AND acv.acvDateTo > ?2 AND acv.acvAdCompany = ?3"
 *
 * @ejb:finder signature="Collection findByAcCodeAndAcvQuarterNumber(java.lang.Integer AC_CODE, short ACV_QRTR_NMBR, java.lang.Integer ACV_AD_CMPNY)"
 *             query="SELECT OBJECT(acv) FROM GlAccountingCalendar ac, IN(ac.glAccountingCalendarValues) acv WHERE ac.acCode=?1 AND acv.acvQuarter=?2 AND acv.acvAdCompany = ?3"
 *
 * @jboss:query signature="Collection findByAcCodeAndAcvQuarterNumber(java.lang.Integer AC_CODE, short ACV_QRTR_NMBR, java.lang.Integer ACV_AD_CMPNY)"
 *             query="SELECT OBJECT(acv) FROM GlAccountingCalendar ac, IN(ac.glAccountingCalendarValues) acv WHERE ac.acCode=?1 AND acv.acvQuarter=?2 AND acv.acvAdCompany = ?3 ORDER BY acv.acvPeriodNumber"
 *
 * @ejb:finder signature="Collection findClosedAndPermanentlyClosedAcvByAcCode(java.lang.Integer AC_CODE, char ACV_STATUS1, char ACV_STATUS2, java.lang.Integer ACV_AD_CMPNY)"
 *             query="SELECT OBJECT(acv) FROM GlAccountingCalendar ac, IN(ac.glAccountingCalendarValues) acv WHERE ac.acCode=?1 AND (acv.acvStatus=?2 OR acv.acvStatus=?3) AND acv.acvAdCompany = ?4 ORDER BY acv.acvPeriodNumber"
 *
 * @jboss:query signature="Collection findClosedAndPermanentlyClosedAcvByAcCode(java.lang.Integer AC_CODE, char ACV_STATUS1, char ACV_STATUS2, java.lang.Integer ACV_AD_CMPNY)"
 *             query="SELECT OBJECT(acv) FROM GlAccountingCalendar ac, IN(ac.glAccountingCalendarValues) acv WHERE ac.acCode=?1 AND (acv.acvStatus=?2 OR acv.acvStatus=?3) AND acv.acvAdCompany = ?4 ORDER BY acv.acvPeriodNumber"
 *
 * @ejb:finder signature="Collection findClosedAndPermanentlyClosedAcvByAcCodeAndBackwardAcvDate(java.lang.Integer AC_CODE, java.util.Date ACV_DT, char ACV_STATUS1, char ACV_STATUS2, java.lang.Integer ACV_AD_CMPNY)"
 *             query="SELECT OBJECT(acv) FROM GlAccountingCalendar ac, IN(ac.glAccountingCalendarValues) acv WHERE ac.acCode=?1 AND acv.acvDateTo<?2 AND (acv.acvStatus=?3 OR acv.acvStatus=?4) AND acv.acvAdCompany = ?5"
 *
 * @ejb:finder signature="Collection findClosedAndPermanentlyClosedAcvByAcCodeAndForwardAcvDate(java.lang.Integer AC_CODE, java.util.Date ACV_DT, char ACV_STATUS1, char ACV_STATUS2, java.lang.Integer ACV_AD_CMPNY)"
 *             query="SELECT OBJECT(acv) FROM GlAccountingCalendar ac, IN(ac.glAccountingCalendarValues) acv WHERE ac.acCode=?1 AND acv.acvDateFrom>?2 AND (acv.acvStatus=?3 OR acv.acvStatus=?4) AND acv.acvAdCompany = ?5"
 *
 * @ejb:finder signature="Collection findSubsequentAcvByAcCodeAndAcvPeriodNumber(java.lang.Integer AC_CODE, short ACV_PRD_NMBR, java.lang.Integer ACV_AD_CMPNY)"
 *             query="SELECT OBJECT(acv) FROM GlAccountingCalendar ac, IN(ac.glAccountingCalendarValues) acv WHERE ac.acCode=?1 AND acv.acvPeriodNumber>?2) AND acv.acvAdCompany = ?3"
 *
 * @jboss:query signature="Collection findSubsequentAcvByAcCodeAndAcvPeriodNumber(java.lang.Integer AC_CODE, short ACV_PRD_NMBR, java.lang.Integer ACV_AD_CMPNY)"
 *             query="SELECT OBJECT(acv) FROM GlAccountingCalendar ac, IN(ac.glAccountingCalendarValues) acv WHERE ac.acCode=?1 AND acv.acvPeriodNumber>?2 AND acv.acvAdCompany = ?3 ORDER BY acv.acvPeriodNumber"
 *
 * @ejb:finder signature="Collection findReportableAcvByAcCodeAndAcvStatus(java.lang.Integer AC_CODE, char ACV_STATUS1, char ACV_STATUS2, char ACV_STATUS3, java.lang.Integer ACV_AD_CMPNY)"
 *             query="SELECT OBJECT(acv) FROM GlAccountingCalendar ac, IN(ac.glAccountingCalendarValues) acv WHERE ac.acCode=?1 AND (acv.acvStatus=?2 OR acv.acvStatus=?3 OR acv.acvStatus=?4) AND acv.acvAdCompany = ?5 ORDER BY acv.acvPeriodNumber"
 *
 * @jboss:query signature="Collection findReportableAcvByAcCodeAndAcvStatus(java.lang.Integer AC_CODE, char ACV_STATUS1, char ACV_STATUS2, char ACV_STATUS3, java.lang.Integer ACV_AD_CMPNY)"
 *             query="SELECT OBJECT(acv) FROM GlAccountingCalendar ac, IN(ac.glAccountingCalendarValues) acv WHERE ac.acCode=?1 AND (acv.acvStatus=?2 OR acv.acvStatus=?3 OR acv.acvStatus=?4) AND acv.acvAdCompany = ?5 ORDER BY acv.acvPeriodNumber"
 *
 * @ejb:finder signature="Collection findByAcCode(java.lang.Integer AC_CODE, java.lang.Integer ACV_AD_CMPNY)"
 *             query="SELECT OBJECT(acv) FROM GlAccountingCalendar ac, IN(ac.glAccountingCalendarValues) acv WHERE ac.acCode=?1 AND acv.acvAdCompany = ?2"
 *
 * @jboss:query signature="Collection findByAcCode(java.lang.Integer AC_CODE, java.lang.Integer ACV_AD_CMPNY)"
 *             query="SELECT OBJECT(acv) FROM GlAccountingCalendar ac, IN(ac.glAccountingCalendarValues) acv WHERE ac.acCode=?1 AND acv.acvAdCompany = ?2 ORDER BY acv.acvPeriodNumber"
 *
 * @jboss:persistence table-name="GL_ACCNTNG_CLNDR_VL"
 *
 * @jboss:entity-command name="mysql-get-generated-keys"
 *
 */

public abstract class GlAccountingCalendarValueBean extends AbstractEntityBean {


   // Access methods for persistent fields

   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    * @ejb:pk-field
    *
    * @jboss:column-name name="ACV_CODE"
    *
    * @jboss:persistence auto-increment="true"
    **/
   public abstract java.lang.Integer getAcvCode();
   public abstract void setAcvCode(java.lang.Integer ACV_CODE);

   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="ACV_PRD_PRFX"
    **/
   public abstract java.lang.String getAcvPeriodPrefix();
   public abstract void setAcvPeriodPrefix(java.lang.String ACV_PRD_PRFX);

   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="ACV_QRTR"
    **/
   public abstract short getAcvQuarter();
   public abstract void setAcvQuarter(short ACV_QRTR);

   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="ACV_PRD_NMBR"
    **/
   public abstract short getAcvPeriodNumber();
   public abstract void setAcvPeriodNumber(short ACV_PRD_NMBR);

   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="ACV_DT_FRM"
    **/
   public abstract Date getAcvDateFrom();
   /**
    * @ejb:interface-method view-type="local"
    **/
   public abstract void setAcvDateFrom(Date ACV_DT_FRM);

   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="ACV_DT_TO"
    **/
   public abstract Date getAcvDateTo();
   public abstract void setAcvDateTo(Date ACV_DT_TO);

   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="ACV_STATUS"
    **/
   public abstract char getAcvStatus();
   /**
    * @ejb:interface-method view-type="local"
    **/
   public abstract void setAcvStatus(char ACV_STATUS);

   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="ACV_DT_OPND"
    **/
   public abstract Date getAcvDateOpened();
   /**
    * @ejb:interface-method view-type="local"
    **/
   public abstract void setAcvDateOpened(Date ACV_DT_OPND);

   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="ACV_DT_CLSD"
    **/
   public abstract Date getAcvDateClosed();
   /**
    * @ejb:interface-method view-type="local"
    **/
   public abstract void setAcvDateClosed(Date ACV_DT_CLSD);

   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="ACV_DT_PRMNNTLY_CLSD"
    **/
   public abstract Date getAcvDatePermanentlyClosed();
   /**
    * @ejb:interface-method view-type="local"
    **/
   public abstract void setAcvDatePermanentlyClosed(Date ACV_DT_PRMNNTLY_CLSD);

   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="ACV_DT_FTR_ENTRD"
    **/
   public abstract Date getAcvDateFutureEntered();
   /**
    * @ejb:interface-method view-type="local"
    **/
   public abstract void setAcvDateFutureEntered(Date ACV_DT_FTR_ENTRD);
   
   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="ACV_AD_CMPNY"
    **/
   public abstract Integer getAcvAdCompany();
   /**
    * @ejb:interface-method view-type="local"
    **/
   public abstract void setAcvAdCompany(Integer ACV_AD_CMPNY);

   // Access methods for relationship fields

   /**
    * @ejb:interface-method view-type="local"
    * @ejb:relation name="accountingcalendar-accountingcalendarvalues"
    *               role-name="accountingcalendarvalue-has-one-accountingcalendar"
    *               cascade-delete="yes"
    *
    * @jboss:relation related-pk-field="acCode"
    *                 fk-column="GL_ACCOUNTING_CALENDAR"
    */
   public abstract LocalGlAccountingCalendar getGlAccountingCalendar();
   /**
    * @ejb:interface-method view-type="local"
    **/
   public abstract void setGlAccountingCalendar(LocalGlAccountingCalendar glAccountingCalendar);

      
   /**
    * @ejb:interface-method view-type="local"
    * @ejb:relation name="accountingcalendarvalue-chartofaccountbalances"
    *               role-name="accountingcalendarvalue-has-many-chartofaccountbalances"
    */
   public abstract Collection getGlChartOfAccountBalances();
   public abstract void setGlChartOfAccountBalances(Collection glChartOfAccountBalances);
   
   
   /**
    * @ejb:interface-method view-type="local"
    * @ejb:relation name="accountingcalendarvalue-investoraccountbalances"
    *               role-name="accountingcalendarvalue-has-many-investoraccountbalances"
    */
   public abstract Collection getGlInvestorAccountBalances();
   public abstract void setGlInvestorAccountBalances(Collection glInvestorAccountBalances);
   
   
   /**
    * @ejb:interface-method view-type="local"
    * @ejb:relation name="accountingcalendarvalue-budgetamountperiods"
    *               role-name="accountingcalendarvalue-has-many-budgetamountperiods"
    */
   public abstract Collection getGlBudgetAmountPeriods();
   public abstract void setGlBudgetAmountPeriods(Collection glBudgetAmountPeriods);

   // Business methods
      
   /**
    * @ejb:interface-method view-type="local"
    **/
   public void addGlChartOfAccountBalance(LocalGlChartOfAccountBalance glChartOfAccountBalance) {

      Debug.print("GlAccountingCalendarValueBean addGlChartOfAccountBalance");
      try {
         Collection glChartOfAccountBalances = getGlChartOfAccountBalances();
	     glChartOfAccountBalances.add(glChartOfAccountBalance);
      } catch (Exception ex) {
         throw new EJBException(ex.getMessage());
      }
   }

   /**
    * @ejb:interface-method view-type="local"
    **/
   public void dropGlChartOfAccountBalance(LocalGlChartOfAccountBalance glChartOfAccountBalance) {
   
      Debug.print("GlAccountingCalendarValueBean dropGlChartOfAccountBalance");
      try {
         Collection glChartOfAccountBalances = getGlChartOfAccountBalances();
	     glChartOfAccountBalances.remove(glChartOfAccountBalance);
      } catch (Exception ex) {
         throw new EJBException(ex.getMessage());
      }
   }
   
   
   /**
    * @ejb:interface-method view-type="local"
    **/
   public void addGlInvestorAccountBalance(LocalGlInvestorAccountBalance glInvestorAccountBalance) {

      Debug.print("GlAccountingCalendarValueBean addGlInvestorAccountBalance");
      try {
         Collection glInvestorAccountBalances = getGlInvestorAccountBalances();
         glInvestorAccountBalances.add(glInvestorAccountBalance);
      } catch (Exception ex) {
         throw new EJBException(ex.getMessage());
      }
   }

   /**
    * @ejb:interface-method view-type="local"
    **/
   public void dropGlInvestorAccountBalance(LocalGlInvestorAccountBalance glInvestorAccountBalance) {
   
      Debug.print("GlAccountingCalendarValueBean dropGlInvestorAccountBalance");
      try {
         Collection glInvestorAccountBalances = getGlChartOfAccountBalances();
         glInvestorAccountBalances.remove(glInvestorAccountBalance);
      } catch (Exception ex) {
         throw new EJBException(ex.getMessage());
      }
   }
   
   
   
   
   
   
   
   
   
   /**
    * @ejb:interface-method view-type="local"
    **/
   public void addGlBudgetAmountPeriod(LocalGlBudgetAmountPeriod glBudgetAmountPeriod) {

      Debug.print("GlAccountingCalendarValueBean addGlBudgetAmountPeriod");
      try {
         Collection glBudgetAmountPeriods = getGlBudgetAmountPeriods();
	     glBudgetAmountPeriods.add(glBudgetAmountPeriod);
      } catch (Exception ex) {
         throw new EJBException(ex.getMessage());
      }
   }

   /**
    * @ejb:interface-method view-type="local"
    **/
   public void dropGlBudgetAmountPeriod(LocalGlBudgetAmountPeriod glBudgetAmountPeriod) {
   
      Debug.print("GlAccountingCalendarValueBean dropGlBudgetAmountPeriod");
      try {
         Collection glBudgetAmountPeriods = getGlBudgetAmountPeriods();
	     glBudgetAmountPeriods.remove(glBudgetAmountPeriod);
      } catch (Exception ex) {
         throw new EJBException(ex.getMessage());
      }
   }

   // EntityBean methods

   /**
    * @ejb:create-method view-type="local"
    **/
   public java.lang.Integer ejbCreate (java.lang.Integer ACV_CODE,
      java.lang.String ACV_PRD_PRFX, short ACV_QRTR, short ACV_PRD_NMBR,
      Date ACV_DT_FRM, Date ACV_DT_TO,
      char ACV_STATUS, Date ACV_DT_OPND, Date ACV_DT_CLSD,
      Date ACV_DT_PRMNNTLY_CLSD, Date ACV_DT_FTR_ENTRD, Integer ACV_AD_CMPNY)
      throws CreateException {

      Debug.print("GlAccountingCalendarValueBean ejbCreate");
      setAcvCode(ACV_CODE);
      setAcvPeriodPrefix(ACV_PRD_PRFX);
      setAcvQuarter(ACV_QRTR);
      setAcvPeriodNumber(ACV_PRD_NMBR);
      setAcvDateFrom(ACV_DT_FRM);
      setAcvDateTo(ACV_DT_TO);
      setAcvStatus(ACV_STATUS);
      setAcvDateOpened(ACV_DT_OPND);
      setAcvDateClosed(ACV_DT_CLSD);
      setAcvDatePermanentlyClosed(ACV_DT_PRMNNTLY_CLSD);
      setAcvDateFutureEntered(ACV_DT_FTR_ENTRD);
      setAcvAdCompany(ACV_AD_CMPNY);
      
      return null;
   }

   /**
    * @ejb:create-method view-type="local"
    **/
   public java.lang.Integer ejbCreate (
      java.lang.String ACV_PRD_PRFX, short ACV_QRTR, short ACV_PRD_NMBR,
      Date ACV_DT_FRM, Date ACV_DT_TO,
      char ACV_STATUS, Date ACV_DT_OPND, Date ACV_DT_CLSD,
      Date ACV_DT_PRMNNTLY_CLSD, Date ACV_DT_FTR_ENTRD, Integer ACV_AD_CMPNY)
      throws CreateException {
      
      Debug.print("GlAccountingCalendarValueBean ejbCreate");

      setAcvPeriodPrefix(ACV_PRD_PRFX);
      setAcvQuarter(ACV_QRTR);
      setAcvPeriodNumber(ACV_PRD_NMBR);
      setAcvDateFrom(ACV_DT_FRM);
      setAcvDateTo(ACV_DT_TO);
      setAcvStatus(ACV_STATUS);
      setAcvDateOpened(ACV_DT_OPND);
      setAcvDateClosed(ACV_DT_CLSD);
      setAcvDatePermanentlyClosed(ACV_DT_PRMNNTLY_CLSD);
      setAcvDateFutureEntered(ACV_DT_FTR_ENTRD);
      setAcvAdCompany(ACV_AD_CMPNY);
      
      return null;
   }   
      
   public void ejbPostCreate (java.lang.Integer ACV_CODE,
      java.lang.String ACV_PRD_PRFX, short ACV_QRTR, short ACV_PRD_NMBR,
      Date ACV_DT_FRM, Date ACV_DT_TO,
      char ACV_STATUS, Date ACV_DT_OPND, Date ACV_DT_CLSD,
      Date ACV_DT_PRMNNTLY_CLSD, Date ACV_DT_FTR_ENTRD, Integer ACV_AD_CMPNY)
      throws CreateException { }

   public void ejbPostCreate (
      java.lang.String ACV_PRD_PRFX, short ACV_QRTR, short ACV_PRD_NMBR,
      Date ACV_DT_FRM, Date ACV_DT_TO,
      char ACV_STATUS, Date ACV_DT_OPND, Date ACV_DT_CLSD,
      Date ACV_DT_PRMNNTLY_CLSD, Date ACV_DT_FTR_ENTRD, Integer ACV_AD_CMPNY)
      throws CreateException { }

} //GlAccountingCalendarValueBean class
