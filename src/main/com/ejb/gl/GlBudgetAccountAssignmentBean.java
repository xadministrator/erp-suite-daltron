package com.ejb.gl;

import javax.ejb.CreateException;

import com.util.AbstractEntityBean;
import com.util.Debug;

/**
 * @ejb:bean name="GlBudgetAccountAssignmentEJB"
 *           display-name="Budget Account Assignment Entity"
 *           type="CMP"
 *           cmp-version="2.x"
 *           view-type="local"
 *           local-jndi-name="omega-ejb/GlBudgetAccountAssignmentEJB"
 *           schema="GlBudgetAccountAssignment"
 *           primkey-field="baaCode"
 *       
 * @ejb:pk class="java.lang.Integer"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="gluser"
 *                        role-link="gluserlink"
 *
 * @ejb:permission role-name="gluser"
 *
 * @ejb:interface local-class="com.ejb.gl.LocalGlBudgetAccountAssignment"
 *                local-extends="javax.ejb.EJBLocalObject"
 *
 * @ejb:home local-class="com.ejb.gl.LocalGlBudgetAccountAssignmentHome"
 *                local-extends="javax.ejb.EJBLocalHome"
 * 
 * @ejb:finder signature="Collection findByBoCode(java.lang.Integer BO_CODE, java.lang.Integer BAA_AD_CMPNY)"
 *             query="SELECT OBJECT(baa) FROM GlBudgetOrganization bo, IN(bo.glBudgetAccountAssignments) baa WHERE bo.boCode = ?1 AND baa.baaAdCompany = ?2"
 *
 * @jboss:query signature="Collection findByBoCode(java.lang.Integer BO_CODE, java.lang.Integer BAA_AD_CMPNY)"
 *             query="SELECT OBJECT(baa) FROM GlBudgetOrganization bo, IN(bo.glBudgetAccountAssignments) baa WHERE bo.boCode = ?1 AND baa.baaAdCompany = ?2 ORDER BY baa.baaCode"
 *
 * @ejb:value-object match="*"
 *             name="GlBudgetAccountAssignment"
 *
 * @jboss:persistence table-name="GL_BDGT_ACCNT_ASSGNMNT"
 *
 * @jboss:entity-command name="mysql-get-generated-keys"
 *
 */

public abstract class GlBudgetAccountAssignmentBean extends AbstractEntityBean {


   // Access methods for persistent fields

   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    * @ejb:pk-field
    *
    * @jboss:column-name name="BAA_CODE"
    *
    * @jboss:persistence auto-increment="true"
    **/
   public abstract java.lang.Integer getBaaCode();
   public abstract void setBaaCode(java.lang.Integer BAA_CODE);

   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="BAA_ACCNT_FRM"
    **/
   public abstract java.lang.String getBaaAccountFrom();
   /**
    * @ejb:interface-method view-type="local"
    **/   
   public abstract void setBaaAccountFrom(java.lang.String BAA_ACCNT_FRM);

   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="BAA_ACCNT_TO"
    **/
   public abstract java.lang.String getBaaAccountTo();
   /**
    * @ejb:interface-method view-type="local"
    **/   
   public abstract void setBaaAccountTo(java.lang.String BAA_ACCNT_TO);
   
   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="BAA_TYP"
    **/
   public abstract java.lang.String getBaaType();
   /**
    * @ejb:interface-method view-type="local"
    **/   
   public abstract void setBaaType(java.lang.String BAA_TYP);
   
   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="BAA_AD_CMPNY"
    **/
   public abstract java.lang.Integer getBaaAdCompany();
   /**
    * @ejb:interface-method view-type="local"
    **/   
   public abstract void setBaaAdCompany(java.lang.Integer BAA_AD_CMPNY);
   

   // Access methods for relationship fields 

   /**
    * @ejb:interface-method view-type="local"
    * @ejb:relation name="budgetorganization-budgetaccountassignments"
    *               role-name="budgetaccountassignment-has-one-budgetorganization"
    * 
    * @jboss:relation related-pk-field="boCode"
    *                 fk-column="GL_BUDGET_ORGANIZATION"
    */
   public abstract LocalGlBudgetOrganization getGlBudgetOrganization();
   public abstract void setGlBudgetOrganization(LocalGlBudgetOrganization glBudgetOrganization); 

   // No Business methods

   // EntityBean methods 

   /**
    * @ejb:create-method view-type="local"
    **/
   public java.lang.Integer ejbCreate (java.lang.Integer BAA_CODE, java.lang.String BAA_ACCNT_FRM, 
      java.lang.String BAA_ACCNT_TO, java.lang.String BAA_TYP, java.lang.Integer BAA_AD_CMPNY)
      throws CreateException {

      Debug.print("GlBudgetAccountAssignment ejbCreate");
      setBaaCode(BAA_CODE);
      setBaaAccountFrom(BAA_ACCNT_FRM);
      setBaaAccountTo(BAA_ACCNT_TO);
      setBaaType(BAA_TYP);
      setBaaAdCompany(BAA_AD_CMPNY);
      
      return null;
   }
   
   /**
    * @ejb:create-method view-type="local"
    **/
   public java.lang.Integer ejbCreate (java.lang.String BAA_ACCNT_FRM, 
        java.lang.String BAA_ACCNT_TO, java.lang.String BAA_TYP, java.lang.Integer BAA_AD_CMPNY)
        throws CreateException {

	   	Debug.print("GlBudgetAccountAssignment ejbCreate");
	    setBaaAccountFrom(BAA_ACCNT_FRM);
	    setBaaAccountTo(BAA_ACCNT_TO);
	    setBaaType(BAA_TYP);
	    setBaaAdCompany(BAA_AD_CMPNY);
	    
        return null;
     }
   
   public void ejbPostCreate (java.lang.Integer BAA_CODE, java.lang.String BAA_ACCNT_FRM, 
        java.lang.String BAA_ACCNT_TO, java.lang.String BAA_TYP, java.lang.Integer BAA_AD_CMPNY)
      throws CreateException { }
   
   public void ejbPostCreate (java.lang.String BAA_ACCNT_FRM, 
        java.lang.String BAA_ACCNT_TO, java.lang.String BAA_TYP, java.lang.Integer BAA_AD_CMPNY)
        throws CreateException { }

}     
