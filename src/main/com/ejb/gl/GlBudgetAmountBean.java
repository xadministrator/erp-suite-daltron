package com.ejb.gl;

import java.util.Collection;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;

import com.util.AbstractEntityBean;
import com.util.Debug;

/**
 * @ejb:bean name="GlBudgetAmountEJB"
 *           display-name="Budget Amount Entity"
 *           type="CMP"
 *           cmp-version="2.x"
 *           view-type="local"
 *           local-jndi-name="omega-ejb/GlBudgetAmountEJB"
 *           schema="GlBudgetAmount"
 *           primkey-field="bgaCode"
 *       
 * @ejb:pk class="java.lang.Integer"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="gluser"
 *                        role-link="gluserlink"
 *
 * @ejb:permission role-name="gluser"
 *
 * @ejb:interface local-class="com.ejb.gl.LocalGlBudgetAmount"
 *                local-extends="javax.ejb.EJBLocalObject"
 *
 * @ejb:home local-class="com.ejb.gl.LocalGlBudgetAmountHome"
 *                local-extends="javax.ejb.EJBLocalHome"
 * 
 * @ejb:finder signature="LocalGlBudgetAmount findByBoNameAndBgtName(java.lang.String BO_NM, java.lang.String BGT_NM, java.lang.Integer BGA_AD_CMPNY)"
 *             query="SELECT OBJECT(bga) FROM GlBudgetAmount bga WHERE bga.glBudgetOrganization.boName = ?1 AND bga.glBudget.bgtName = ?2 AND bga.bgaAdCompany = ?3"
 * 
 * @ejb:finder signature="LocalGlBudgetAmount findByBoNameAndBgtNameAndCoaAccountNumber(java.lang.String BO_NM, java.lang.String BGT_NM, java.lang.String COA_ACCNT_NMBR, java.lang.Integer BGA_AD_CMPNY)"
 *             query="SELECT OBJECT(bga) FROM GlBudgetAmount bga, In(bga.glBudgetAmountCoas) bc WHERE bga.glBudgetOrganization.boName = ?1 AND bga.glBudget.bgtName = ?2 AND bc.glChartOfAccount.coaAccountNumber = ?3 AND bga.bgaAdCompany = ?4"
 * 
 * @ejb:select signature="Collection ejbSelectGeneric(java.lang.String jbossQl, java.lang.Object[] args)"
 *             result-type-mapping="Local"
 *             method-intf="LocalHome"
 *             query="SELECT OBJECT(bga) FROM GlBudgetAmount bga"
 *
 * @jboss:query signature="Collection ejbSelectGeneric(java.lang.String jbossQl, java.lang.Object[] args)"
 *                         dynamic="true"
 * 
 * @ejb:value-object match="*"
 *             name="GlBudgetAmount"
 *
 * @jboss:persistence table-name="GL_BDGT_AMNT"
 *
 * @jboss:entity-command name="mysql-get-generated-keys"
 *
 */

public abstract class GlBudgetAmountBean extends AbstractEntityBean {


   // Access methods for persistent fields

   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    * @ejb:pk-field
    *
    * @jboss:column-name name="BGA_CODE"
    *
    * @jboss:persistence auto-increment="true"
    **/
   public abstract java.lang.Integer getBgaCode();
   public abstract void setBgaCode(java.lang.Integer BGA_CODE);

   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="BGA_CRTD_BY"
    **/
   public abstract java.lang.String getBgaCreatedBy();
   /**
    * @ejb:interface-method view-type="local"
    **/   
   public abstract void setBgaCreatedBy(java.lang.String BGA_CRTD_BY);

   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="BGA_DT_CRTD"
    **/
   public abstract java.util.Date getBgaDateCreated();
   /**
    * @ejb:interface-method view-type="local"
    **/   
   public abstract void setBgaDateCreated(java.util.Date BGA_DT_CRTD);
   
   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="BGA_LST_MDFD_BY"
    **/
   public abstract java.lang.String getBgaLastModifiedBy();
   /**
    * @ejb:interface-method view-type="local"
    **/   
   public abstract void setBgaLastModifiedBy(java.lang.String BGA_LST_MDFD_BY);
   
   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="BGA_DT_LST_MDFD"
    **/
   public abstract java.util.Date getBgaDateLastModified();
   /**
    * @ejb:interface-method view-type="local"
    **/   
   public abstract void setBgaDateLastModified(java.util.Date BGA_DT_LST_MDFD);
   
   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="BGA_AD_CMPNY"
    **/
   public abstract java.lang.Integer getBgaAdCompany();
   /**
    * @ejb:interface-method view-type="local"
    **/   
   public abstract void setBgaAdCompany(java.lang.Integer BGA_AD_CMPNY);

   // Access methods for relationship fields
   
   /**
    * @ejb:interface-method view-type="local"
    * @ejb:relation name="budget-budgetamounts"
    *               role-name="budgetamount-has-one-budget"
    * 
    * @jboss:relation related-pk-field="bgtCode"
    *                 fk-column="GL_BUDGET"
    */
   public abstract LocalGlBudget getGlBudget();
   public abstract void setGlBudget(LocalGlBudget glBudget); 
   
   /**
    * @ejb:interface-method view-type="local"
    * @ejb:relation name="budgetorganization-budgetamounts"
    *               role-name="budgetamount-has-one-budgetorganization"
    * 
    * @jboss:relation related-pk-field="boCode"
    *                 fk-column="GL_BUDGET_ORGANIZATION"
    */
   public abstract LocalGlBudgetOrganization getGlBudgetOrganization();
   public abstract void setGlBudgetOrganization(LocalGlBudgetOrganization glBudgetOrganization); 
   

   /**
    * @ejb:interface-method view-type="local"
    * @ejb:relation name="budgetamount-budgetamountcoas"
    *               role-name="budgetamount-has-many-budgetamountcoas"
    */
   public abstract Collection getGlBudgetAmountCoas();
   public abstract void setGlBudgetAmountCoas(Collection glBudgetAmountCoas);
   
   /**
    * @jboss:dynamic-ql
    */
    public abstract Collection ejbSelectGeneric(java.lang.String jbossQl, java.lang.Object[] args)
       throws FinderException;

   // Business methods
   
   /**
    * @ejb:home-method view-type="local"
    */
    public Collection ejbHomeGetBgaByCriteria(java.lang.String jbossQl, java.lang.Object[] args)
       throws FinderException {
       	
       return ejbSelectGeneric(jbossQl, args);
    }

   // Business methods

   /**
    * @ejb:interface-method view-type="local"
    **/
   public void addGlBudgetAmountCoa(LocalGlBudgetAmountCoa glBudgetAmountCoa) {

      Debug.print("GlBudgetAmountBean addGlBudgetAmountCoa");
      try {
         Collection glBudgetAmountCoas = getGlBudgetAmountCoas();
         glBudgetAmountCoas.add(glBudgetAmountCoa);
      } catch (Exception ex) {
         throw new EJBException(ex.getMessage());
      }
   }

   /**
    * @ejb:interface-method view-type="local"
    **/
   public void dropGlBudgetAmountCoa(LocalGlBudgetAmountCoa glBudgetAmountCoa) {

      Debug.print("GlBudgetAmountBean dropGlBudgetAmountCoa");
      try {
         Collection glBudgetAmountCoas = getGlBudgetAmountCoas();
         glBudgetAmountCoas.remove(glBudgetAmountCoa);
      } catch (Exception ex) {
         throw new EJBException(ex.getMessage());
      }
   }
 
   // EntityBean methods 

   /**
    * @ejb:create-method view-type="local"
    **/
   public java.lang.Integer ejbCreate (java.lang.Integer BGA_CODE, java.lang.String BGA_CRTD_BY, 
      java.util.Date BGA_DT_CRTD, java.lang.String BGA_LST_MDFD_BY, java.util.Date BGA_DT_LST_MDFD,
	  java.lang.Integer BGA_AD_CMPNY)
      throws CreateException {

      Debug.print("GlBudgetAmount ejbCreate");
      setBgaCode(BGA_CODE);
      setBgaCreatedBy(BGA_CRTD_BY);
      setBgaDateCreated(BGA_DT_CRTD);
      setBgaLastModifiedBy(BGA_LST_MDFD_BY);
      setBgaDateLastModified(BGA_DT_LST_MDFD);
      setBgaAdCompany(BGA_AD_CMPNY);
      
      return null;
   }
   
   /**
    * @ejb:create-method view-type="local"
    **/
   public java.lang.Integer ejbCreate (java.lang.String BGA_CRTD_BY, 
        java.util.Date BGA_DT_CRTD, java.lang.String BGA_LST_MDFD_BY, java.util.Date BGA_DT_LST_MDFD,
		java.lang.Integer BGA_AD_CMPNY)
        throws CreateException {

        Debug.print("GlBudgetAmount ejbCreate");
        setBgaCreatedBy(BGA_CRTD_BY);
        setBgaDateCreated(BGA_DT_CRTD);
        setBgaLastModifiedBy(BGA_LST_MDFD_BY);
        setBgaDateLastModified(BGA_DT_LST_MDFD);
        setBgaAdCompany(BGA_AD_CMPNY);
        
        return null;
     }
   
   public void ejbPostCreate (java.lang.Integer BGA_CODE, java.lang.String BGA_CRTD_BY, 
        java.util.Date BGA_DT_CRTD, java.lang.String BGA_LST_MDFD_BY, java.util.Date BGA_DT_LST_MDFD,
		java.lang.Integer BGA_AD_CMPNY)
      throws CreateException { }
   
   public void ejbPostCreate (java.lang.String BGA_CRTD_BY, 
        java.util.Date BGA_DT_CRTD, java.lang.String BGA_LST_MDFD_BY, java.util.Date BGA_DT_LST_MDFD,
		java.lang.Integer BGA_AD_CMPNY)
        throws CreateException { }

}     
