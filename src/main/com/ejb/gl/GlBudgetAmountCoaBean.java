package com.ejb.gl;

import java.util.Collection;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;

import com.util.AbstractEntityBean;
import com.util.Debug;

/**
 * @ejb:bean name="GlBudgetAmountCoaEJB"
 *           display-name="Budget Amount Coa Entity"
 *           type="CMP"
 *           cmp-version="2.x"
 *           view-type="local"
 *           local-jndi-name="omega-ejb/GlBudgetAmountCoaEJB"
 *           schema="GlBudgetAmountCoa"
 *           primkey-field="bcCode"
 *       
 * @ejb:pk class="java.lang.Integer"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="gluser"
 *                        role-link="gluserlink"
 *
 * @ejb:permission role-name="gluser"
 *
 * @ejb:interface local-class="com.ejb.gl.LocalGlBudgetAmountCoa"
 *                local-extends="javax.ejb.EJBLocalObject"
 *
 * @ejb:home local-class="com.ejb.gl.LocalGlBudgetAmountCoaHome"
 *                local-extends="javax.ejb.EJBLocalHome"
 * 
 * @ejb:finder signature="LocalGlBudgetAmountCoa findByCoaCodeAndBgtCode(java.lang.Integer COA_CODE, java.lang.Integer BGT_CODE, java.lang.Integer BC_AD_CMPNY)"
 *             query="SELECT OBJECT(bc) FROM GlBudgetAmountCoa bc WHERE bc.glChartOfAccount.coaCode = ?1 AND bc.glBudgetAmount.glBudget.bgtCode = ?2 AND bc.bcAdCompany = ?3"
 * 
 * @ejb:select signature="Collection ejbSelectGeneric(java.lang.String jbossQl, java.lang.Object[] args)"
 *             result-type-mapping="Local"
 *             method-intf="LocalHome"
 *             query="SELECT OBJECT(bc) FROM GlBudgetAmountCoa bc"
 *
 * @jboss:query signature="Collection ejbSelectGeneric(java.lang.String jbossQl, java.lang.Object[] args)"
 *                         dynamic="true"
 * 
 * @ejb:value-object match="*"
 *             name="GlBudgetAmountCoa"
 *
 * @jboss:persistence table-name="GL_BDGT_AMNT_COA"
 *
 * @jboss:entity-command name="mysql-get-generated-keys"
 *
 */

public abstract class GlBudgetAmountCoaBean extends AbstractEntityBean {


   // Access methods for persistent fields

   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    * @ejb:pk-field
    *
    * @jboss:column-name name="BC_CODE"
    *
    * @jboss:persistence auto-increment="true"
    **/
   public abstract java.lang.Integer getBcCode();
   public abstract void setBcCode(java.lang.Integer BC_CODE);

   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="BC_RL_AMNT"
    **/
   public abstract double getBcRuleAmount();
   /**
    * @ejb:interface-method view-type="local"
    **/   
   public abstract void setBcRuleAmount(double BC_RL_AMNT);
   
   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="BC_RL_PRCNTG1"
    **/
   public abstract double getBcRulePercentage1();
   /**
    * @ejb:interface-method view-type="local"
    **/   
   public abstract void setBcRulePercentage1(double BC_RL_PRCNTG1);
   
   /**
   * @ejb:persistent-field
   * @ejb:interface-method view-type="local"
   *
   * @jboss:column-name name="BC_RL_PRCNTG2"
   **/
  public abstract double getBcRulePercentage2();
  /**
   * @ejb:interface-method view-type="local"
   **/   
  public abstract void setBcRulePercentage2(double BC_RL_PRCNTG2);
   
   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="BC_DESC"
    **/
   public abstract java.lang.String getBcDescription();
   /**
    * @ejb:interface-method view-type="local"
    **/   
   public abstract void setBcDescription(java.lang.String BC_DESC);

   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="BC_RL_TYP"
    **/
   public abstract java.lang.String getBcRuleType();
   /**
    * @ejb:interface-method view-type="local"
    **/   
   public abstract void setBcRuleType(java.lang.String BC_RL_TYP);
   
   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="BC_AD_CMPNY"
    **/
   public abstract java.lang.Integer getBcAdCompany();
   /**
    * @ejb:interface-method view-type="local"
    **/   
   public abstract void setBcAdCompany(java.lang.Integer BC_AD_CMPNY);

   // Access methods for relationship fields 

   /**
    * @ejb:interface-method view-type="local"
    * @ejb:relation name="budgetamount-budgetamountcoas"
    *               role-name="budgetamountcoa-has-one-budgetamount"
    * 
    * @jboss:relation related-pk-field="bgaCode"
    *                 fk-column="GL_BUDGET_AMOUNT"
    */
   public abstract LocalGlBudgetAmount getGlBudgetAmount();
   public abstract void setGlBudgetAmount(LocalGlBudgetAmount glBudgetAmount);
   
   /**
    * @ejb:interface-method view-type="local"
    * @ejb:relation name="chartofaccount-budgetamountcoas"
    *               role-name="budgetamountcoa-has-one-chartofaccount"
    * 
    * @jboss:relation related-pk-field="coaCode"
    *                 fk-column="GL_CHART_OF_ACCOUNT"
    */
   public abstract LocalGlChartOfAccount getGlChartOfAccount();
   public abstract void setGlChartOfAccount(LocalGlChartOfAccount glChartOfAccount); 
   
   /**
    * @ejb:interface-method view-type="local"
    * @ejb:relation name="budgetamountcoa-budgetamountperiods"
    *               role-name="budgetamountcoa-has-many-budgetamountperiods"
    */
   public abstract Collection getGlBudgetAmountPeriods();
   public abstract void setGlBudgetAmountPeriods(Collection glBudgetAmountPeriods);

   // Business methods
   
   /**
    * @jboss:dynamic-ql
    */
    public abstract Collection ejbSelectGeneric(java.lang.String jbossQl, java.lang.Object[] args)
       throws FinderException;

   // Business methods
   
   /**
    * @ejb:home-method view-type="local"
    */
    public Collection ejbHomeGetBcByCriteria(java.lang.String jbossQl, java.lang.Object[] args)
       throws FinderException {
       	
       return ejbSelectGeneric(jbossQl, args);
    }
   
   /**
    * @ejb:interface-method view-type="local"
    **/
   public void addGlBudgetAmountPeriod(LocalGlBudgetAmountPeriod glBudgetAmountPeriod) {

      Debug.print("GlBudgetAmountCoaBean addGlBudgetAmountPeriod");
      try {
         Collection glBudgetAmountPeriods = getGlBudgetAmountPeriods();
         glBudgetAmountPeriods.add(glBudgetAmountPeriod);
      } catch (Exception ex) {
         throw new EJBException(ex.getMessage());
      }
   }

   /**
    * @ejb:interface-method view-type="local"
    **/
   public void dropGlBudgetAmountPeriod(LocalGlBudgetAmountPeriod glBudgetAmountPeriod) {

      Debug.print("GlBudgetAmountCoaBean dropGlBudgetAmountPeriod");
      try {
         Collection glBudgetAmountPeriods = getGlBudgetAmountPeriods();
         glBudgetAmountPeriods.remove(glBudgetAmountPeriod);
      } catch (Exception ex) {
         throw new EJBException(ex.getMessage());
      }
   }

   // EntityBean methods 

   /**
    * @ejb:create-method view-type="local"
    **/
   public java.lang.Integer ejbCreate (java.lang.Integer BC_CODE, double BC_RL_AMNT, double BC_RL_PRCNTG1, double BC_RL_PRCNTG2, java.lang.String BC_DESC, java.lang.String BC_TYP,
   		java.lang.Integer BC_AD_CMPNY)
      throws CreateException {

      Debug.print("GlBudgetAmountCoa ejbCreate");
      setBcCode(BC_CODE);
      setBcRuleAmount(BC_RL_AMNT);
      setBcRulePercentage1(BC_RL_PRCNTG1);
      setBcRulePercentage2(BC_RL_PRCNTG2);
      setBcDescription(BC_DESC);
      setBcRuleType(BC_TYP);
      setBcAdCompany(BC_AD_CMPNY);
      
      return null;
   }
   
   /**
    * @ejb:create-method view-type="local"
    **/
   public java.lang.Integer ejbCreate (double BC_RL_AMNT, double BC_RL_PRCNTG1, double BC_RL_PRCNTG2,java.lang.String BC_DESC, java.lang.String BC_TYP,
   		java.lang.Integer BC_AD_CMPNY)
        throws CreateException {

	   	Debug.print("GlBudgetAmountCoa ejbCreate");
	    setBcRuleAmount(BC_RL_AMNT);
	    setBcRulePercentage1(BC_RL_PRCNTG1);
	    setBcRulePercentage2(BC_RL_PRCNTG2);
	    setBcDescription(BC_DESC);
	    setBcRuleType(BC_TYP);
	    setBcAdCompany(BC_AD_CMPNY);
	    
        return null;
     }
   
   public void ejbPostCreate (java.lang.Integer BC_CODE, double BC_RL_AMNT, double BC_RL_PRCNTG1, double BC_RL_PRCNTG2,java.lang.String BC_DESC, java.lang.String BC_TYP,
   		java.lang.Integer BC_AD_CMPNY)
      throws CreateException { }
   
   public void ejbPostCreate (double BC_RL_AMNT, double BC_RL_PRCNTG1, double BC_RL_PRCNTG2,java.lang.String BC_DESC, java.lang.String BC_TYP,
   		java.lang.Integer BC_AD_CMPNY)
        throws CreateException { }

}     
