package com.ejb.gl;

import javax.ejb.CreateException;

import com.util.AbstractEntityBean;
import com.util.Debug;

/**
 * @ejb:bean name="GlBudgetAmountPeriodEJB"
 *           display-name="Budget Amount Period Entity"
 *           type="CMP"
 *           cmp-version="2.x"
 *           view-type="local"
 *           local-jndi-name="omega-ejb/GlBudgetAmountPeriodEJB"
 *           schema="GlBudgetAmountPeriod"
 *           primkey-field="bapCode"
 *       
 * @ejb:pk class="java.lang.Integer"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="gluser"
 *                        role-link="gluserlink"
 *
 * @ejb:permission role-name="gluser"
 *
 * @ejb:interface local-class="com.ejb.gl.LocalGlBudgetAmountPeriod"
 *                local-extends="javax.ejb.EJBLocalObject"
 *
 * @ejb:home local-class="com.ejb.gl.LocalGlBudgetAmountPeriodHome"
 *                local-extends="javax.ejb.EJBLocalHome"
 * 
 * @ejb:finder signature="LocalGlBudgetAmountPeriod findByBcCodeAndAcvCode(java.lang.Integer BC_CODE, java.lang.Integer ACV_CODE, java.lang.Integer BAP_AD_CMPNY)"
 *             query="SELECT OBJECT(bap) FROM GlBudgetAmountPeriod bap WHERE bap.glBudgetAmountCoa.bcCode = ?1 AND bap.glAccountingCalendarValue.acvCode = ?2 AND bap.bapAdCompany = ?3"
 * 
 * @ejb:value-object match="*"
 *             name="GlBudgetAmountPeriod"
 *
 * @jboss:persistence table-name="GL_BDGT_AMNT_PRD"
 *
 * @jboss:entity-command name="mysql-get-generated-keys"
 *
 */

public abstract class GlBudgetAmountPeriodBean extends AbstractEntityBean {


   // Access methods for persistent fields

   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    * @ejb:pk-field
    *
    * @jboss:column-name name="BAP_CODE"
    *
    * @jboss:persistence auto-increment="true"
    **/
   public abstract java.lang.Integer getBapCode();
   public abstract void setBapCode(java.lang.Integer BAP_CODE);

   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="BAP_AMNT"
    **/
   public abstract double getBapAmount();
   /**
    * @ejb:interface-method view-type="local"
    **/   
   public abstract void setBapAmount(double BAP_AMNT);
   
   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="BAP_AD_CMPNY"
    **/
   public abstract Integer getBapAdCompany();
   /**
    * @ejb:interface-method view-type="local"
    **/   
   public abstract void setBapAdCompany(Integer BAP_AD_CMPNY);

   // Access methods for relationship fields 

   /**
    * @ejb:interface-method view-type="local"
    * @ejb:relation name="budgetamountcoa-budgetamountperiods"
    *               role-name="budgetamountperiod-has-one-budgetamountcoa"
    *               cascade-delete="yes"
    * 
    * @jboss:relation related-pk-field="bcCode"
    *                 fk-column="GL_BUDGET_AMOUNT_COA"
    */
   public abstract LocalGlBudgetAmountCoa getGlBudgetAmountCoa();
   public abstract void setGlBudgetAmountCoa(LocalGlBudgetAmountCoa glBudgetAmountCoa);
   
   /**
    * @ejb:interface-method view-type="local"
    * @ejb:relation name="accountingcalendarvalue-budgetamountperiods"
    *               role-name="budgetamountperiod-has-one-accountingcalendarvalue"
    * 
    * @jboss:relation related-pk-field="acvCode"
    *                 fk-column="GL_ACCOUNTING_CALENDAR_VALUE"
    */
   public abstract LocalGlAccountingCalendarValue getGlAccountingCalendarValue();
   public abstract void setGlAccountingCalendarValue(LocalGlAccountingCalendarValue glAccountingCalendarValue); 
   

   // No Business methods

   // EntityBean methods 

   /**
    * @ejb:create-method view-type="local"
    **/
   public java.lang.Integer ejbCreate (java.lang.Integer BAP_CODE, double BAP_AMNT, Integer BAP_AD_CMPNY)
      throws CreateException {

      Debug.print("GlBudgetAmountPeriod ejbCreate");
      setBapCode(BAP_CODE);
      setBapAmount(BAP_AMNT);
      setBapAdCompany(BAP_AD_CMPNY);
      
      return null;
   }
   
   /**
    * @ejb:create-method view-type="local"
    **/
   public java.lang.Integer ejbCreate (double BAP_AMNT, Integer BAP_AD_CMPNY)
        throws CreateException {

	   	Debug.print("GlBudgetAmountPeriod ejbCreate");
	    setBapAmount(BAP_AMNT);
	    setBapAdCompany(BAP_AD_CMPNY);
	    
        return null;
     }
   
   public void ejbPostCreate (java.lang.Integer BAP_CODE, double BAP_AMNT, Integer BAP_AD_CMPNY)
      throws CreateException { }
   
   public void ejbPostCreate (double BAP_AMNT, Integer BAP_AD_CMPNY)
        throws CreateException { }

}     
