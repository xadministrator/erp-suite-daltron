package com.ejb.gl;

import java.util.Collection;

import javax.ejb.CreateException;
import javax.ejb.EJBException;

import com.util.AbstractEntityBean;
import com.util.Debug;

/**
 * @ejb:bean name="GlBudgetEJB"
 *           display-name="Budget Entity"
 *           type="CMP"
 *           cmp-version="2.x"
 *           view-type="local"
 *           local-jndi-name="omega-ejb/GlBudgetEJB"
 *           schema="GlBudget"
 *           primkey-field="bgtCode"
 *       
 * @ejb:pk class="java.lang.Integer"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="gluser"
 *                        role-link="gluserlink"
 *
 * @ejb:permission role-name="gluser"
 *
 * @ejb:interface local-class="com.ejb.gl.LocalGlBudget"
 *                local-extends="javax.ejb.EJBLocalObject"
 *
 * @ejb:home local-class="com.ejb.gl.LocalGlBudgetHome"
 *                local-extends="javax.ejb.EJBLocalHome"
 * 
 * @ejb:finder signature="Collection findBgtAll(java.lang.Integer BGT_AD_CMPNY)"
 *             query="SELECT OBJECT(bgt) FROM GlBudget bgt WHERE bgt.bgtAdCompany = ?1"
 *
 * @jboss:query signature="Collection findBgtAll(java.lang.Integer BGT_AD_CMPNY)"
 *             query="SELECT OBJECT(bgt) FROM GlBudget bgt WHERE bgt.bgtAdCompany = ?1 ORDER BY bgt.bgtName"
 * 
 * @ejb:finder signature="LocalGlBudget findByBgtName(java.lang.String BGT_NM, java.lang.Integer BGT_AD_CMPNY)"
 *             query="SELECT OBJECT(bgt) FROM GlBudget bgt WHERE bgt.bgtName = ?1 AND bgt.bgtAdCompany = ?2"
 * 
 * @ejb:finder signature="LocalGlBudget findByBgtStatus(java.lang.String BGT_STATUS, java.lang.Integer BGT_AD_CMPNY)"
 *             query="SELECT OBJECT(bgt) FROM GlBudget bgt WHERE bgt.bgtStatus = ?1 AND bgt.bgtAdCompany = ?2"
 * 
 * @ejb:value-object match="*"
 *             name="GlBudget"
 *
 * @jboss:persistence table-name="GL_BDGT"
 *
 * @jboss:entity-command name="mysql-get-generated-keys"
 *
 */

public abstract class GlBudgetBean extends AbstractEntityBean {


   // Access methods for persistent fields

   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    * @ejb:pk-field
    *
    * @jboss:column-name name="BGT_CODE"
    *
    * @jboss:persistence auto-increment="true"
    **/
   public abstract java.lang.Integer getBgtCode();
   public abstract void setBgtCode(java.lang.Integer BGT_CODE);

   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="BGT_NM"
    **/
   public abstract java.lang.String getBgtName();
   /**
    * @ejb:interface-method view-type="local"
    **/   
   public abstract void setBgtName(java.lang.String BGT_NM);

   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="BGT_DESC"
    **/
   public abstract java.lang.String getBgtDescription();
   /**
    * @ejb:interface-method view-type="local"
    **/   
   public abstract void setBgtDescription(java.lang.String BGT_DESC);
   
   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="BGT_STATUS"
    **/
   public abstract java.lang.String getBgtStatus();
   /**
    * @ejb:interface-method view-type="local"
    **/   
   public abstract void setBgtStatus(java.lang.String BGT_STATUS);
   
   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="BGT_FRST_PRD"
    **/
   public abstract java.lang.String getBgtFirstPeriod();
   /**
    * @ejb:interface-method view-type="local"
    **/   
   public abstract void setBgtFirstPeriod(java.lang.String BGT_FRST_PRD);
   
   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="BGT_LST_PRD"
    **/
   public abstract java.lang.String getBgtLastPeriod();
   /**
    * @ejb:interface-method view-type="local"
    **/   
   public abstract void setBgtLastPeriod(java.lang.String BGT_LST_PRD);
   
   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="BGT_AD_CMPNY"
    **/
   public abstract java.lang.Integer getBgtAdCompany();
   /**
    * @ejb:interface-method view-type="local"
    **/   
   public abstract void setBgtAdCompany(java.lang.Integer BGT_AD_CMPNY);

   // Access methods for relationship fields

   /**
    * @ejb:interface-method view-type="local"
    * @ejb:relation name="budget-budgetamounts"
    *               role-name="budget-has-many-budgetamounts"
    */
   public abstract Collection getGlBudgetAmounts();
   public abstract void setGlBudgetAmounts(Collection glBudgetAmounts);

   // Business methods

   /**
    * @ejb:interface-method view-type="local"
    **/
   public void addGlBudgetAmount(LocalGlBudgetAmount glBudgetAmount) {

      Debug.print("GlBudgetBean addGlBudgetAmount");
      try {
         Collection glBudgetAmounts = getGlBudgetAmounts();
         glBudgetAmounts.add(glBudgetAmount);
      } catch (Exception ex) {
         throw new EJBException(ex.getMessage());
      }
   }

   /**
    * @ejb:interface-method view-type="local"
    **/
   public void dropGlBudgetAmount(LocalGlBudgetAmount glBudgetAmount) {

      Debug.print("GlBudgetBean dropGlBudgetAmount");
      try {
         Collection glBudgetAmounts = getGlBudgetAmounts();
         glBudgetAmounts.remove(glBudgetAmount);
      } catch (Exception ex) {
         throw new EJBException(ex.getMessage());
      }
   }
 
   // EntityBean methods 

   /**
    * @ejb:create-method view-type="local"
    **/
   public java.lang.Integer ejbCreate (java.lang.Integer BGT_CODE, java.lang.String BGT_NM, 
      java.lang.String BGT_DESC, java.lang.String BGT_STATUS, java.lang.String BGT_FRST_PRD, 
	  java.lang.String BGT_LST_PRD, java.lang.Integer BGT_AD_CMPNY)
      throws CreateException {

      Debug.print("GlBudget ejbCreate");
      setBgtCode(BGT_CODE);
      setBgtName(BGT_NM);
      setBgtDescription(BGT_DESC);
      setBgtStatus(BGT_STATUS);
      setBgtFirstPeriod(BGT_FRST_PRD);
      setBgtLastPeriod(BGT_LST_PRD);
      setBgtAdCompany(BGT_AD_CMPNY);
	  
      return null;
   }
   
   /**
    * @ejb:create-method view-type="local"
    **/
   public java.lang.Integer ejbCreate (java.lang.String BGT_NM, 
        java.lang.String BGT_DESC, java.lang.String BGT_STATUS, java.lang.String BGT_FRST_PRD, 
  	  java.lang.String BGT_LST_PRD, java.lang.Integer BGT_AD_CMPNY)
        throws CreateException {

        Debug.print("GlBudget ejbCreate");
        setBgtName(BGT_NM);
        setBgtDescription(BGT_DESC);
        setBgtStatus(BGT_STATUS);
        setBgtFirstPeriod(BGT_FRST_PRD);
        setBgtLastPeriod(BGT_LST_PRD);
        setBgtAdCompany(BGT_AD_CMPNY);
        
        return null;
     }
   
   public void ejbPostCreate (java.lang.Integer BGT_CODE, java.lang.String BGT_NM, 
   	  java.lang.String BGT_DESC, java.lang.String BGT_STATUS, java.lang.String BGT_FRST_PRD, 
	  java.lang.String BGT_LST_PRD, java.lang.Integer BGT_AD_CMPNY)
      throws CreateException { }
   
   public void ejbPostCreate (java.lang.String BGT_NM, 
      java.lang.String BGT_DESC, java.lang.String BGT_STATUS, java.lang.String BGT_FRST_PRD, 
  	  java.lang.String BGT_LST_PRD, java.lang.Integer BGT_AD_CMPNY)
        throws CreateException { }

}     
