package com.ejb.gl;

import java.util.Collection;

import javax.ejb.CreateException;
import javax.ejb.EJBException;

import com.util.AbstractEntityBean;
import com.util.Debug;

/**
 * @ejb:bean name="GlBudgetOrganizationEJB"
 *           display-name="Budget Organization Entity"
 *           type="CMP"
 *           cmp-version="2.x"
 *           view-type="local"
 *           local-jndi-name="omega-ejb/GlBudgetOrganizationEJB"
 *           schema="GlBudgetOrganization"
 *           primkey-field="boCode"
 *       
 * @ejb:pk class="java.lang.Integer"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="gluser"
 *                        role-link="gluserlink"
 *
 * @ejb:permission role-name="gluser"
 *
 * @ejb:interface local-class="com.ejb.gl.LocalGlBudgetOrganization"
 *                local-extends="javax.ejb.EJBLocalObject"
 *
 * @ejb:home local-class="com.ejb.gl.LocalGlBudgetOrganizationHome"
 *                local-extends="javax.ejb.EJBLocalHome"
 * 
 * @ejb:finder signature="Collection findBoAll(java.lang.Integer BO_AD_CMPNY)"
 *             query="SELECT OBJECT(bo) FROM GlBudgetOrganization bo WHERE bo.boAdCompany = ?1"
 *
 * @jboss:query signature="Collection findBoAll(java.lang.Integer BO_AD_CMPNY)"
 *             query="SELECT OBJECT(bo) FROM GlBudgetOrganization bo WHERE bo.boAdCompany = ?1 ORDER BY bo.boName"
 * 
 * @ejb:finder signature="LocalGlBudgetOrganization findByBoName(java.lang.String BO_NM, java.lang.Integer BO_AD_CMPNY)"
 *             query="SELECT OBJECT(bo) FROM GlBudgetOrganization bo WHERE bo.boName = ?1 AND bo.boAdCompany = ?2"
 * 
 * @ejb:value-object match="*"
 *             name="GlBudgetOrganization"
 *
 * @jboss:persistence table-name="GL_BDGT_ORGNZTN"
 *
 * @jboss:entity-command name="mysql-get-generated-keys"
 *
 */

public abstract class GlBudgetOrganizationBean extends AbstractEntityBean {


   // Access methods for persistent fields

   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    * @ejb:pk-field
    *
    * @jboss:column-name name="BO_CODE"
    *
    * @jboss:persistence auto-increment="true"
    **/
   public abstract java.lang.Integer getBoCode();
   public abstract void setBoCode(java.lang.Integer BO_CODE);

   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="BO_NM"
    **/
   public abstract java.lang.String getBoName();
   /**
    * @ejb:interface-method view-type="local"
    **/   
   public abstract void setBoName(java.lang.String BO_NM);

   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="BO_DESC"
    **/
   public abstract java.lang.String getBoDescription();
   /**
    * @ejb:interface-method view-type="local"
    **/   
   public abstract void setBoDescription(java.lang.String BO_DESC);
   
   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="BO_SGMNT_ORDR"
    **/
   public abstract java.lang.String getBoSegmentOrder();
   /**
    * @ejb:interface-method view-type="local"
    **/   
   public abstract void setBoSegmentOrder(java.lang.String BO_SGMNT_ORDR);
   
   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="BO_PSSWRD"
    **/
   public abstract java.lang.String getBoPassword();
   /**
    * @ejb:interface-method view-type="local"
    **/   
   public abstract void setBoPassword(java.lang.String BO_PSSWRD);
   
   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="BO_AD_CMPNY"
    **/
   public abstract java.lang.Integer getBoAdCompany();
   /**
    * @ejb:interface-method view-type="local"
    **/   
   public abstract void setBoAdCompany(java.lang.Integer BO_AD_CMPNY);

   // Access methods for relationship fields
   

   /**
    * @ejb:interface-method view-type="local"
    * @ejb:relation name="budgetorganization-budgetamounts"
    *               role-name="budgetorganization-has-many-budgetamounts"
    */
   public abstract Collection getGlBudgetAmounts();
   public abstract void setGlBudgetAmounts(Collection glBudgetAmounts);
   
   /**
    * @ejb:interface-method view-type="local"
    * @ejb:relation name="budgetorganization-budgetaccountassignments"
    *               role-name="budgetorganization-has-many-budgetaccountassignments"
    */
   public abstract Collection getGlBudgetAccountAssignments();
   public abstract void setGlBudgetAccountAssignments(Collection glBudgetAccountAssignments);

   // Business methods

   /**
    * @ejb:interface-method view-type="local"
    **/
   public void addGlBudgetAmount(LocalGlBudgetAmount glBudgetAmount) {

      Debug.print("GlBudgetOrganizationBean addGlBudgetAmount");
      try {
         Collection glBudgetAmounts = getGlBudgetAmounts();
         glBudgetAmounts.add(glBudgetAmount);
      } catch (Exception ex) {
         throw new EJBException(ex.getMessage());
      }
   }

   /**
    * @ejb:interface-method view-type="local"
    **/
   public void dropGlBudgetAmount(LocalGlBudgetAmount glBudgetAmount) {

      Debug.print("GlBudgetOrganixationBean dropGlBudgetAmount");
      try {
         Collection glBudgetAmounts = getGlBudgetAmounts();
         glBudgetAmounts.remove(glBudgetAmount);
      } catch (Exception ex) {
         throw new EJBException(ex.getMessage());
      }
   }
   
   /**
    * @ejb:interface-method view-type="local"
    **/
   public void addGlBudgetAccountAssignment(LocalGlBudgetAccountAssignment glBudgetAccountAssignment) {

      Debug.print("GlBudgetOrganizationBean addGlBudgetAccountAssignment");
      try {
         Collection glBudgetAccountAssignments = getGlBudgetAccountAssignments();
         glBudgetAccountAssignments.add(glBudgetAccountAssignment);
      } catch (Exception ex) {
         throw new EJBException(ex.getMessage());
      }
   }

   /**
    * @ejb:interface-method view-type="local"
    **/
   public void dropGlBudgetAccountAssignment(LocalGlBudgetAccountAssignment glBudgetAccountAssignment) {

      Debug.print("GlBudgetOrganizationBean dropGlBudgetAccountAssignment");
      try {
         Collection glBudgetAccountAssignments = getGlBudgetAccountAssignments();
         glBudgetAccountAssignments.remove(glBudgetAccountAssignment);
      } catch (Exception ex) {
         throw new EJBException(ex.getMessage());
      }
   }
 
   // EntityBean methods 

   /**
    * @ejb:create-method view-type="local"
    **/
   public java.lang.Integer ejbCreate (java.lang.Integer BO_CODE, java.lang.String BO_NM, 
      java.lang.String BO_DESC, java.lang.String BO_SGMNT_ORDR, java.lang.String BO_PSSWRD,
	  java.lang.Integer BO_AD_CMPNY)
      throws CreateException {

      Debug.print("GlBudgetOrganization ejbCreate");
      setBoCode(BO_CODE);
      setBoName(BO_NM);
      setBoDescription(BO_DESC);
      setBoSegmentOrder(BO_SGMNT_ORDR);
      setBoPassword(BO_PSSWRD);
      setBoAdCompany(BO_AD_CMPNY);
      
      return null;
   }
   
   /**
    * @ejb:create-method view-type="local"
    **/
   public java.lang.Integer ejbCreate (java.lang.String BO_NM, 
        java.lang.String BO_DESC, java.lang.String BO_SGMNT_ORDR, java.lang.String BO_PSSWRD,
		java.lang.Integer BO_AD_CMPNY)
        throws CreateException {

	   	Debug.print("GlBudgetOrganization ejbCreate");
	   	setBoName(BO_NM);
	   	setBoDescription(BO_DESC);
	   	setBoSegmentOrder(BO_SGMNT_ORDR);
	   	setBoPassword(BO_PSSWRD);
	   	setBoAdCompany(BO_AD_CMPNY);
	   	
        return null;
     }
   
   public void ejbPostCreate (java.lang.Integer BO_CODE, java.lang.String BO_NM, 
        java.lang.String BO_DESC, java.lang.String BO_SGMNT_ORDR, java.lang.String BO_PSSWRD,
		java.lang.Integer BO_AD_CMPNY)
      throws CreateException { }
   
   public void ejbPostCreate (java.lang.String BO_NM, 
        java.lang.String BO_DESC, java.lang.String BO_SGMNT_ORDR, java.lang.String BO_PSSWRD,
		java.lang.Integer BO_AD_CMPNY)
        throws CreateException { }

}     
