package com.ejb.gl;

import javax.ejb.CreateException;

import com.util.AbstractEntityBean;
import com.util.Debug;

/**
 * @ejb:bean name="GlChartOfAccountBalanceEJB"
 *           display-name="Chart Of Account Balance Entity"
 *           type="CMP"
 *           cmp-version="2.x"
 *           view-type="local"
 *           local-jndi-name="omega-ejb/GlChartOfAccountBalanceEJB"
 *           schema="GlChartOfAccountBalance"
 *           primkey-field="coabCode"
 *       
 * @ejb:pk class="java.lang.Integer"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="gluser"
 *                        role-link="gluserlink"
 *
 * @ejb:permission role-name="gluser"
 *
 * @ejb:interface local-class="com.ejb.gl.LocalGlChartOfAccountBalance"
 *                local-extends="javax.ejb.EJBLocalObject"
 *
 * @ejb:home local-class="com.ejb.gl.LocalGlChartOfAccountBalanceHome"
 *           local-extends="javax.ejb.EJBLocalHome"
 *
 * @ejb:finder signature="LocalGlChartOfAccountBalance findByAcvCodeAndCoaCode(java.lang.Integer ACV_CODE, java.lang.Integer COA_CODE, java.lang.Integer COAB_AD_CMPNY)"
 *             query="SELECT OBJECT(coab) FROM GlAccountingCalendarValue acv, IN(acv.glChartOfAccountBalances) coab WHERE acv.acvCode=?1 AND coab.glChartOfAccount.coaCode=?2 AND coab.coabAdCompany = ?3"
 *
 * @ejb:finder signature="Collection findByAcvCodeAndCoaSegment1(java.lang.Integer ACV_CODE, java.lang.String COA_SEGMENT1, java.lang.Integer COAB_AD_CMPNY)"
 *             query="SELECT OBJECT(coab) FROM GlAccountingCalendarValue acv, IN(acv.glChartOfAccountBalances) coab WHERE acv.acvCode=?1 AND coab.glChartOfAccount.coaSegment5=?2 AND coab.coabAdCompany = ?3"
 *
 * @ejb:finder signature="LocalGlChartOfAccountBalance findByOtEffectiveDateAndCoaCode(java.util.Date OT_EFFCTV_DT, java.lang.Integer COA_CODE, java.lang.Integer COAB_AD_CMPNY)"
 *             query="SELECT OBJECT(coab) FROM GlChartOfAccount coa, IN(coa.glChartOfAccountBalances) coab WHERE coa.coaCode=?2 AND coab.glAccountingCalendarValue.acvDateFrom<=?1 AND coab.glAccountingCalendarValue.acvDateTo>=?1 AND coab.coabAdCompany = ?3"
 *
 * @jboss:query signature="LocalGlChartOfAccountBalance findByOtEffectiveDateAndCoaCode(java.util.Date OT_EFFCTV_DT, java.lang.Integer COA_CODE, java.lang.Integer COAB_AD_CMPNY)"
 *              query="SELECT OBJECT(coab) FROM GlChartOfAccount coa, IN(coa.glChartOfAccountBalances) coab WHERE coa.coaCode=?2 AND coab.glAccountingCalendarValue.acvDateFrom<=?1 AND coab.glAccountingCalendarValue.acvDateTo>=?1 AND coab.coabAdCompany = ?3"
 *
 * @ejb:finder signature="LocalGlChartOfAccountBalance findByCoaCodeAndAcvCode(java.lang.Integer COA_CODE, java.lang.Integer ACV_CODE, java.lang.Integer COAB_AD_CMPNY)"
 *             query="SELECT OBJECT(coab) FROM GlChartOfAccount coa, IN(coa.glChartOfAccountBalances) coab WHERE coa.coaCode=?1 AND coab.glAccountingCalendarValue.acvCode =?2 AND coab.coabAdCompany = ?3"
 *
 * @jboss:persistence table-name="GL_COA_BLNC"
 *
 * @jboss:entity-command name="mysql-get-generated-keys"
 *
 */

public abstract class GlChartOfAccountBalanceBean extends AbstractEntityBean {


   // Access methods for persistent fields

   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    * @ejb:pk-field
    *
    * @jboss:column-name name="COAB_CODE"
    *
    * @jboss:persistence auto-increment="true"
    **/
   public abstract java.lang.Integer getCoabCode();
   public abstract void setCoabCode(java.lang.Integer COAB_CODE);

   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="COAB_TTL_DBT"
    **/
   public abstract double getCoabTotalDebit();
   /**
    * @ejb:interface-method view-type="local"
    **/
   public abstract void setCoabTotalDebit(double COAB_TTL_DBT);
   
   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="COAB_TTL_CRDT"
    **/
   public abstract double getCoabTotalCredit();
   /**
    * @ejb:interface-method view-type="local"
    **/
   public abstract void setCoabTotalCredit(double COAB_TTL_CRDT);
   
   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="COAB_BEG_BLNC"
    **/
   public abstract double getCoabBeginningBalance();
   /**
    * @ejb:interface-method view-type="local"
    **/
   public abstract void setCoabBeginningBalance(double COAB_BEG_BLNC);
   
   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="COAB_END_BLNC"
    **/
   public abstract double getCoabEndingBalance();
   /**
    * @ejb:interface-method view-type="local"
    **/
   public abstract void setCoabEndingBalance(double COAB_END_BLNC);
   
   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="COAB_AD_CMPNY"
    **/
   public abstract Integer getCoabAdCompany();
   /**
    * @ejb:interface-method view-type="local"
    **/
   public abstract void setCoabAdCompany(Integer COAB_AD_CMPNY);

   // Access methods for relationship fields

   /**
    * @ejb:interface-method view-type="local"
    * @ejb:relation name="chartofaccount-chartofaccountbalances"
    *               role-name="chartofaccountbalance-has-one-chartofaccount"
    *               cascade-delete="yes"
    *
    * @jboss:relation related-pk-field="coaCode"
    *                 fk-column="GL_CHART_OF_ACCOUNT"
    */
   public abstract LocalGlChartOfAccount getGlChartOfAccount();
   /**
    * @ejb:interface-method view-type="local"
    **/
   public abstract void setGlChartOfAccount(LocalGlChartOfAccount glChartOfAccount);

   /**
    * @ejb:interface-method view-type="local"
    * @ejb:relation name="accountingcalendarvalue-chartofaccountbalances"
    *               role-name="chartofaccountbalance-has-one-accountingcalendarvalue"
    *
    * @jboss:relation related-pk-field="acvCode"
    *                 fk-column="GL_ACCOUNTING_CALENDAR_VALUE"
    */
   public abstract LocalGlAccountingCalendarValue getGlAccountingCalendarValue();
   /**
    * @ejb:interface-method view-type="local"
    **/
   public abstract void setGlAccountingCalendarValue(LocalGlAccountingCalendarValue glAccountingCalendarValue);

   // Business methods

   // EntityBean methods

   /**
    * @ejb:create-method view-type="local"
    **/
   public java.lang.Integer ejbCreate (java.lang.Integer COAB_CODE, double COAB_TTL_DBT,
      double COAB_TTL_CRDT, double COAB_BEG_BLNC, double COAB_END_BLNC, Integer COAB_AD_CMPNY)
      throws CreateException {

      Debug.print("GlChartOfAccountBalance ejbCreate");
      setCoabCode(COAB_CODE);
      setCoabTotalDebit(COAB_TTL_DBT);
      setCoabTotalCredit(COAB_TTL_CRDT);
      setCoabBeginningBalance(COAB_BEG_BLNC);
      setCoabEndingBalance(COAB_END_BLNC);
      setCoabAdCompany(COAB_AD_CMPNY);
      
      return null;
   }
   
   /**
    * @ejb:create-method view-type="local"
    **/
   public java.lang.Integer ejbCreate (double COAB_TTL_DBT,
      double COAB_TTL_CRDT, double COAB_BEG_BLNC, double COAB_END_BLNC, Integer COAB_AD_CMPNY)
      throws CreateException {

      Debug.print("GlChartOfAccountBalance ejbCreate");
      setCoabTotalDebit(COAB_TTL_DBT);
      setCoabTotalCredit(COAB_TTL_CRDT);
      setCoabBeginningBalance(COAB_BEG_BLNC);
      setCoabEndingBalance(COAB_END_BLNC);
      setCoabAdCompany(COAB_AD_CMPNY);
      
      return null;
   }
   
   public void ejbPostCreate (java.lang.Integer COAB_CODE, double COAB_TTL_DBT,
      double COAB_TTL_CRDT, double COAB_BEG_BLNC, double COAB_END_BLNC, Integer COAB_AD_CMPNY)
      throws CreateException { }

   public void ejbPostCreate (double COAB_TTL_DBT,
      double COAB_TTL_CRDT, double COAB_BEG_BLNC, double COAB_END_BLNC, Integer COAB_AD_CMPNY)
      throws CreateException { }

} // GlChartOfAccountBalance class
