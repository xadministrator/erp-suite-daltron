package com.ejb.gl;

import java.util.Collection;
import java.util.Date;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;

import com.ejb.ad.LocalAdBranchCoa;
import com.ejb.gl.LocalGlForexLedger;
import com.util.AbstractEntityBean;
import com.util.Debug;

/**
 * @ejb:bean name="GlChartOfAccountEJB"
 *           display-name="Chart Of Account Entity"
 *           type="CMP"
 *           cmp-version="2.x"
 *           view-type="local"
 *           local-jndi-name="omega-ejb/GlChartOfAccountEJB"
 *           schema="GlChartOfAccount"
 *           primkey-field="coaCode"
 *
 * @ejb:pk class="java.lang.Integer"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="gluser"
 *                        role-link="gluserlink"
 *
 * @ejb:permission role-name="gluser"
 *
 * @ejb:interface local-class="com.ejb.gl.LocalGlChartOfAccount"
 *                local-extends="javax.ejb.EJBLocalObject"
 *
 * @ejb:home local-class="com.ejb.gl.LocalGlChartOfAccountHome"
 *           local-extends="javax.ejb.EJBLocalHome"
 *
 * @ejb:finder signature="LocalGlChartOfAccount findByCoaAccountNumber(java.lang.String COA_ACCNT_NMBR, java.lang.Integer COA_AD_CMPNY)"
 *             query="SELECT OBJECT(coa) FROM GlChartOfAccount coa WHERE coa.coaAccountNumber=?1 AND coa.coaAdCompany = ?2"
 *
 * @ejb:finder signature="LocalGlChartOfAccount findByCoaAccountNumberAndBranchCode(java.lang.String COA_ACCNT_NMBR, java.lang.Integer COA_AD_BRNCH, java.lang.Integer COA_AD_CMPNY)"
 *             query="SELECT OBJECT(coa) FROM GlChartOfAccount coa, In(coa.adBranchCoas) bcoa WHERE coa.coaAccountNumber=?1 AND bcoa.adBranch.brCode=?2 AND coa.coaAdCompany = ?3"
 *
 * @ejb:finder signature="Collection findCoaAllEnabled(java.util.Date CURR_DATE, java.lang.Integer COA_AD_CMPNY)"
 *             query="SELECT OBJECT(coa) FROM GlChartOfAccount coa WHERE coa.coaEnable=1 AND ((coa.coaDateFrom <= ?1 AND coa.coaDateTo >= ?1) OR (coa.coaDateFrom <= ?1 AND coa.coaDateTo IS NULL)) AND coa.coaAdCompany = ?2"
 *
 * @jboss:query signature="Collection findCoaAllEnabled(java.util.Date CURR_DATE, java.lang.Integer COA_AD_CMPNY)"
 *             query="SELECT OBJECT(coa) FROM GlChartOfAccount coa WHERE coa.coaEnable=1 AND ((coa.coaDateFrom <= ?1 AND coa.coaDateTo >= ?1) OR (coa.coaDateFrom <= ?1 AND coa.coaDateTo IS NULL)) AND coa.coaAdCompany = ?2"
 *
 * @ejb:finder signature="Collection findCoaAll(java.lang.Integer COA_AD_CMPNY)"
 *             query="SELECT OBJECT(coa) FROM GlChartOfAccount coa WHERE coa.coaAdCompany = ?1 ORDER BY coa.coaAccountNumber"
 *
 * @jboss:query signature="Collection findCoaAll(java.lang.Integer COA_AD_CMPNY)"
 *             query="SELECT OBJECT(coa) FROM GlChartOfAccount coa WHERE coa.coaAdCompany = ?1 ORDER BY coa.coaAccountNumber"
 *
 * @ejb:finder signature="LocalGlChartOfAccount findByCoaCodeAndBranchCode(java.lang.Integer COA_CODE, java.lang.Integer COA_AD_BRNCH, java.lang.Integer COA_AD_CMPNY)"
 * 			   query="SELECT OBJECT(coa) FROM GlChartOfAccount coa, In(coa.adBranchCoas) bcoa WHERE coa.coaCode=?1 AND bcoa.adBranch.brCode=?2 AND coa.coaAdCompany = ?3"
 *
 * @ejb:finder signature="LocalGlChartOfAccount findByCoaCode(java.lang.Integer COA_CODE, java.lang.Integer COA_AD_CMPNY)"
 * 			   query="SELECT OBJECT(coa) FROM GlChartOfAccount coa WHERE coa.coaCode=?1 AND coa.coaAdCompany = ?2"
 *
 * @ejb:select signature="Collection ejbSelectGeneric(java.lang.String jbossQl, java.lang.Object[] args)"
 *             result-type-mapping="Local"
 *             method-intf="LocalHome"
 *             query="SELECT OBJECT(coa) FROM GlChartOfAccount coa"
 *
 * @jboss:query signature="Collection ejbSelectGeneric(java.lang.String jbossQl, java.lang.Object[] args)"
 *                         dynamic="true"
 *
 * @jboss:persistence table-name="GL_COA"
 *
 * @jboss:entity-command name="mysql-get-generated-keys"
 *
 */

public abstract class GlChartOfAccountBean extends AbstractEntityBean {


	// Access methods for persistent fields

	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 * @ejb:pk-field
	 *
	 * @jboss:column-name name="COA_CODE"
	 *
	 * @jboss:persistence auto-increment="true"
	 **/
	public abstract java.lang.Integer getCoaCode();
	public abstract void setCoaCode(java.lang.Integer COA_CODE);

	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="COA_ACCNT_NMBR"
	 **/
	public abstract java.lang.String getCoaAccountNumber();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setCoaAccountNumber(java.lang.String COA_ACCNT_NMBR);

	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="COA_ACCNT_DESC"
	 **/
	public abstract java.lang.String getCoaAccountDescription();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setCoaAccountDescription(java.lang.String COA_ACCNT_DESC);

	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="COA_ACCNT_TYP"
	 **/
	public abstract java.lang.String getCoaAccountType();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setCoaAccountType(java.lang.String COA_ACCNT_TYP);

	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="COA_TX_TYP"
	 **/
	public abstract java.lang.String getCoaTaxType();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setCoaTaxType(java.lang.String COA_TX_TYP);

	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="COA_CIT_CTGRY"
	 **/
	public abstract java.lang.String getCoaCitCategory();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setCoaCitCategory(java.lang.String COA_CIT_CTGRY);


	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="COA_SAW_CTGRY"
	 **/
	public abstract java.lang.String getCoaSawCategory();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setCoaSawCategory(java.lang.String COA_SAW_CTGRY);






	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="COA_IIT_CTGRY"
	 **/
	public abstract java.lang.String getCoaIitCategory();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setCoaIitCategory(java.lang.String COA_IIT_CTGRY);


	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="COA_DT_FRM"
	 **/
	public abstract Date getCoaDateFrom();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setCoaDateFrom(Date COA_DT_FRM);

	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="COA_DT_TO"
	 **/
	public abstract Date getCoaDateTo();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setCoaDateTo(Date COA_DT_TO);

	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="COA_SGMNT1"
	 **/
	public abstract String getCoaSegment1();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setCoaSegment1(String COA_SGMNT1);

	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="COA_SGMNT2"
	 **/
	public abstract String getCoaSegment2();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setCoaSegment2(String COA_SGMNT2);

	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="COA_SGMNT3"
	 **/
	public abstract String getCoaSegment3();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setCoaSegment3(String COA_SGMNT3);

	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="COA_SGMNT4"
	 **/
	public abstract String getCoaSegment4();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setCoaSegment4(String COA_SGMNT4);

	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="COA_SGMNT5"
	 **/
	public abstract String getCoaSegment5();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setCoaSegment5(String COA_SGMNT5);

	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="COA_SGMNT6"
	 **/
	public abstract String getCoaSegment6();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setCoaSegment6(String COA_SGMNT6);

	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="COA_SGMNT7"
	 **/
	public abstract String getCoaSegment7();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setCoaSegment7(String COA_SGMNT7);

	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="COA_SGMNT8"
	 **/
	public abstract String getCoaSegment8();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setCoaSegment8(String COA_SGMNT8);

	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="COA_SGMNT9"
	 **/
	public abstract String getCoaSegment9();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setCoaSegment9(String COA_SGMNT9);

	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="COA_SGMNT10"
	 **/
	public abstract String getCoaSegment10();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setCoaSegment10(String COA_SGMNT10);

	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="COA_ENBL"
	 **/
	public abstract byte getCoaEnable();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setCoaEnable(byte COA_ENBL);

	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="COA_FR_RVLTN"
	 **/
	public abstract byte getCoaForRevaluation();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setCoaForRevaluation(byte COA_FR_RVLTN);
	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="COA_AP_TAG"
	 **/
	public abstract byte getCoaApTag();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setCoaApTag(byte COA_FR_RVLTN);

	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="COA_AD_CMPNY"
	 **/
	public abstract Integer getCoaAdCompany();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setCoaAdCompany(Integer COA_AD_CMPNY);

	// Access methods for relationship fields

	/**
	 * @ejb:interface-method view-type="local"
	 * @ejb:relation name="chartofaccount-suspenseaccounts"
	 *               role-name="chartofaccount-has-many-suspenseaccounts"
	 */
	public abstract Collection getGlSuspenseAccounts();
	public abstract void setGlSuspenseAccounts(Collection glSuspenseAccounts);

	/**
	 * @ejb:interface-method view-type="local"
	 * @ejb:relation name="chartofaccount-chartofaccountbalances"
	 *               role-name="chartofaccount-has-many-chartofaccountbalances"
	 */
	public abstract Collection getGlChartOfAccountBalances();
	public abstract void setGlChartOfAccountBalances(Collection glChartOfAccountBalances);

	/**
	 * @ejb:interface-method view-type="local"
	 * @ejb:relation name="chartofaccount-recurringjournallines"
	 *               role-name="chartofaccount-has-many-recurringjournallines"
	 */
	public abstract Collection getGlRecurringJournalLines();
	public abstract void setGlRecurringJournalLines(Collection glRecurringJournalLines);

	/**
	 * @ejb:interface-method view-type="local"
	 * @ejb:relation name="chartofaccount-journallines"
	 *               role-name="chartofaccount-has-many-journallines"
	 */
	public abstract Collection getGlJournalLines();
	public abstract void setGlJournalLines(Collection glJournalLines);

	/**
	 * @ejb:interface-method view-type="local"
	 * @ejb:relation name="chartofaccount-apdistributionrecords"
	 *               role-name="chartofaccount-has-many-apdistributionrecords"
	 */
	public abstract Collection getApDistributionRecords();
	public abstract void setApDistributionRecords(Collection apDistributionRecords);


	/**
	 * @ejb:interface-method view-type="local"
	 * @ejb:relation name="chartofaccount-cmdistributionrecords"
	 *               role-name="chartofaccount-has-many-cmdistributionrecords"
	 */
	public abstract Collection getCmDistributionRecords();
	public abstract void setCmDistributionRecords(Collection cmDistributionRecords);

	/**
	 * @ejb:interface-method view-type="local"
	 * @ejb:relation name="chartofaccount-aptaxcodes"
	 *               role-name="chartofaccount-has-many-aptaxcodes"
	 */
	public abstract Collection getApTaxCodes();
	public abstract void setApTaxCodes(Collection apTaxCodes);

	/**
	 * @ejb:interface-method view-type="local"
	 * @ejb:relation name="chartofaccount-withholdingtaxcodes"
	 *               role-name="chartofaccount-has-many-withholdingtaxcodes"
	 */
	public abstract Collection getApWithholdingTaxCodes();
	public abstract void setApWithholdingTaxCodes(Collection apWithholdingTaxCodes);

	/**
	 * @ejb:interface-method view-type="local"
	 * @ejb:relation name="chartofaccount-artaxcodes"
	 *               role-name="chartofaccount-has-many-artaxcodes"
	 */
	public abstract Collection getArTaxCodes();
	public abstract void setArTaxCodes(Collection arTaxCodes);

	/**
	 * @ejb:interface-method view-type="local"
	 * @ejb:relation name="chartofaccount-arwithholdingtaxcodes"
	 *               role-name="chartofaccount-has-many-arwithholdingtaxcodes"
	 */
	public abstract Collection getArWithholdingTaxCodes();
	public abstract void setArWithholdingTaxCodes(Collection arWithholdingTaxCodes);

	/**
	 * @ejb:interface-method view-type="local"
	 * @ejb:relation name="chartofaccount-ardistributionrecords"
	 *               role-name="chartofaccount-has-many-ardistributionrecords"
	 */
	public abstract Collection getArDistributionRecords();
	public abstract void setArDistributionRecords(Collection arDistributionRecords);

	/**
	 * @ejb:interface-method view-type="local"
	 * @ejb:relation name="chartofaccount-standardmemolines"
	 *               role-name="chartofaccount-has-many-standardmemolines"
	 */
	public abstract Collection getArStandardMemoLines();
	public abstract void setArStandardMemoLines(Collection arStandardMemoLines);

	/**
	 * @ejb:interface-method view-type="local"
	 * @ejb:relation name="chartofaccount-approvalcoalines"
	 *               role-name="chartofaccount-has-many-approvalcoalines"
	 */
	public abstract Collection getAdApprovalCoaLines();
	public abstract void setAdApprovalCoaLines(Collection adApprovalCoaLines);

	/**
	 * @ejb:interface-method view-type="local"
	 * @ejb:relation name="chartofaccount-budgetamountcoas"
	 *               role-name="chartofaccount-has-many-budgetamountcoas"
	 */
	public abstract Collection getGlBudgetAmountCoas();
	public abstract void setGlBudgetAmountCoas(Collection glBudgetAmountCoas);

	/**
	 * @ejb:interface-method view-type="local"
	 * @ejb:relation name="chartofaccount-adjustments"
	 *               role-name="chartofaccount-has-many-adjustments"
	 */
	public abstract Collection getInvAdjustments();
	public abstract void setInvAdjustments(Collection invAdjustments);

	/**
	 * @ejb:interface-method view-type="local"
	 * @ejb:relation name="chartofaccount-invdistributionrecords"
	 *               role-name="chartofaccount-has-many-invdistributionrecords"
	 */
	public abstract Collection getInvDistributionRecords();
	public abstract void setInvDistributionRecords(Collection invDistributionRecords);

	/**
	 * @ejb:interface-method view-type="local"
	 * @ejb:relation name="chartofaccount-paymentterms"
	 *               role-name="chartofaccount-has-many-paymentterms"
	 */
	public abstract Collection getAdPaymentTerms();
	public abstract void setAdPaymentTerms(Collection adPaymentTerms);

	/**
	 * @ejb:interface-method view-type="local"
	 * @ejb:relation name="chartofaccount-branchcoa"
	 *               role-name="chartofaccount-has-many-branchcoa"
	 */
	public abstract Collection getAdBranchCoas();
	public abstract void setAdBranchCoas(Collection adBranchCoas);

	/**
	 * @ejb:interface-method view-type="local"
	 * @ejb:relation name="functionalcurrency-chartofaccounts"
	 *               role-name="chartofaccount-has-one-functionalcurrency"
	 *               cascade-delete="no"
	 *
	 * @jboss:relation related-pk-field="fcCode"
	 *                 fk-column="GL_FUNCTIONAL_CURRENCY"
	 */
	public abstract LocalGlFunctionalCurrency getGlFunctionalCurrency();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setGlFunctionalCurrency(LocalGlFunctionalCurrency glFunctionalCurrency);

	/**
	 * @ejb:interface-method view-type="local"
	 * @ejb:relation name="chartofaccount-forexledgers"
	 *               role-name="chartofaccount-has-many-forexledgers"
	 */
	public abstract Collection getGlForexLedgers();
	public abstract void setGlForexLedgers(Collection glForexLedgers);


	/**
	 * @ejb:interface-method view-type="local"
	 * @ejb:relation name="chartofaccount-reportvalues"
	 *               role-name="chartofaccount-has-many-reportvalues"
	 */
	public abstract Collection getGlReportValues();
	public abstract void setGlReportValues(Collection glReportValues);



	/**
	 * @ejb:interface-method view-type="local"
	 * @ejb:relation name="chartofaccount-branches"
	 *               role-name="chartofaccount-has-many-branches"
	 */
	public abstract Collection getAdBranches();
	public abstract void setAdBranches(Collection adBranches);


	/**
	 * @jboss:dynamic-ql
	 */
	public abstract Collection ejbSelectGeneric(java.lang.String jbossQl, java.lang.Object[] args)
	throws FinderException;

	// Business methods

	/**
	 * @ejb:home-method view-type="local"
	 */
	public Collection ejbHomeGetCoaByCriteria(java.lang.String jbossQl, java.lang.Object[] args)
	throws FinderException {

		return ejbSelectGeneric(jbossQl, args);
	}

	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public void addGlSuspenseAccount(LocalGlSuspenseAccount glSuspenseAccount) {

		Debug.print("GlChartOfAccountBean addGlSuspenseAccount");
		try {
			Collection glSuspenseAccounts = getGlSuspenseAccounts();
			glSuspenseAccounts.add(glSuspenseAccount);
		} catch (Exception ex) {
			throw new EJBException(ex.getMessage());
		}
	}

	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public void dropGlSuspenseAccount(LocalGlSuspenseAccount glSuspenseAccount) {

		Debug.print("GlChartOfAccountBean dropGlSuspenseAccount");
		try {
			Collection glSuspenseAccounts = getGlSuspenseAccounts();
			glSuspenseAccounts.remove(glSuspenseAccount);
		} catch (Exception ex) {
			throw new EJBException(ex.getMessage());
		}
	}

	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public void addGlChartOfAccountBalance(LocalGlChartOfAccountBalance glChartOfAccountBalance) {

		Debug.print("GlAccountingCalendarValueBean addGlChartOfAccountBalance");
		try {
			Collection glChartOfAccountBalances = getGlChartOfAccountBalances();
			glChartOfAccountBalances.add(glChartOfAccountBalance);
		} catch (Exception ex) {
			throw new EJBException(ex.getMessage());
		}
	}

	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public void dropGlChartOfAccountBalance(LocalGlChartOfAccountBalance glChartOfAccountBalance) {

		Debug.print("GlAccountingCalendarValueBean dropGlChartOfAccountBalance");
		try {
			Collection glChartOfAccountBalances = getGlChartOfAccountBalances();
			glChartOfAccountBalances.remove(glChartOfAccountBalance);
		} catch (Exception ex) {
			throw new EJBException(ex.getMessage());
		}
	}

	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public void addGlRecurringJournalLine(LocalGlRecurringJournalLine glRecurringJournalLine) {

		Debug.print("GlChartOfAccountBean addGlRecurringJournalLine");
		try {
			Collection glRecurringJournalLines = getGlRecurringJournalLines();
			glRecurringJournalLines.add(glRecurringJournalLine);
		} catch (Exception ex) {
			throw new EJBException(ex.getMessage());
		}
	}

	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public void dropGlRecurringJournalLine(LocalGlRecurringJournalLine glRecurringJournalLine) {

		Debug.print("GlChartOfAccountBean dropGlRecurringJournalLine");
		try {
			Collection glRecurringJournalLines = getGlRecurringJournalLines();
			glRecurringJournalLines.remove(glRecurringJournalLine);
		} catch (Exception ex) {
			throw new EJBException(ex.getMessage());
		}
	}

	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public void addGlJournalLine(LocalGlJournalLine glJournalLine) {

		Debug.print("GlChartOfAccountBean addGlJournalLine");
		try {
			Collection glJournalLines = getGlJournalLines();
			glJournalLines.add(glJournalLine);
		} catch (Exception ex) {
			throw new EJBException(ex.getMessage());
		}
	}

	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public void dropGlJournalLine(LocalGlJournalLine glJournalLine) {

		Debug.print("GlChartOfAccountBean dropGlJournalLine");
		try {
			Collection glJournalLines = getGlJournalLines();
			glJournalLines.remove(glJournalLine);
		} catch (Exception ex) {
			throw new EJBException(ex.getMessage());
		}
	}


	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public void addCmDistributionRecord(com.ejb.cm.LocalCmDistributionRecord cmDistributionRecord) {

		Debug.print("GlChartOfAccountBean addCmDistributionRecord");
		try {
			Collection cmDistributionRecords = getCmDistributionRecords();
			cmDistributionRecords.add(cmDistributionRecord);
		} catch (Exception ex) {
			throw new EJBException(ex.getMessage());
		}
	}

	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public void dropCmDistributionRecord(com.ejb.cm.LocalCmDistributionRecord cmDistributionRecord) {

		Debug.print("GlChartOfAccountBean dropCmDistributionRecord");
		try {
			Collection cmDistributionRecords = getCmDistributionRecords();
			cmDistributionRecords.remove(cmDistributionRecord);
		} catch (Exception ex) {
			throw new EJBException(ex.getMessage());
		}
	}


	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public void addApTaxCode(com.ejb.ap.LocalApTaxCode apTaxCode) {

		Debug.print("GlChartOfAccountBean addApTaxCode");
		try {
			Collection apTaxCodes = getApTaxCodes();
			apTaxCodes.add(apTaxCode);
		} catch (Exception ex) {
			throw new EJBException(ex.getMessage());
		}
	}

	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public void dropApTaxCode(com.ejb.ap.LocalApTaxCode apTaxCode) {

		Debug.print("GlChartOfAccountBean dropApTaxCode");
		try {
			Collection apTaxCodes = getApTaxCodes();
			apTaxCodes.remove(apTaxCode);
		} catch (Exception ex) {
			throw new EJBException(ex.getMessage());
		}
	}

	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public void addApWithholdingTaxCode(com.ejb.ap.LocalApWithholdingTaxCode apWithholdingTaxCode) {

		Debug.print("GlChartOfAccountBean addApWithholdingTaxCode");
		try {
			Collection apWithholdingTaxCodes = getApWithholdingTaxCodes();
			apWithholdingTaxCodes.add(apWithholdingTaxCode);
		} catch (Exception ex) {
			throw new EJBException(ex.getMessage());
		}
	}

	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public void dropApWithholdingTaxCode(com.ejb.ap.LocalApWithholdingTaxCode apWithholdingTaxCode) {

		Debug.print("GlChartOfAccountBean dropApWithholdingTaxCode");
		try {
			Collection apWithholdingTaxCodes = getApWithholdingTaxCodes();
			apWithholdingTaxCodes.remove(apWithholdingTaxCode);
		} catch (Exception ex) {
			throw new EJBException(ex.getMessage());
		}
	}

	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public void addApDistributionRecord(com.ejb.ap.LocalApDistributionRecord apDistributionRecord) {

		Debug.print("GlChartOfAccountBean addApDistributionRecord");
		try {
			Collection apDistributionRecords = getApDistributionRecords();
			apDistributionRecords.add(apDistributionRecord);
		} catch (Exception ex) {
			throw new EJBException(ex.getMessage());
		}
	}

	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public void dropApDistributionRecord(com.ejb.ap.LocalApDistributionRecord apDistributionRecord) {

		Debug.print("GlChartOfAccountBean dropApDistributionRecord");
		try {
			Collection apDistributionRecords = getApDistributionRecords();
			apDistributionRecords.remove(apDistributionRecord);
		} catch (Exception ex) {
			throw new EJBException(ex.getMessage());
		}
	}

	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public void addArTaxCode(com.ejb.ar.LocalArTaxCode arTaxCode) {

		Debug.print("GlChartOfAccountBean addArTaxCode");
		try {
			Collection arTaxCodes = getArTaxCodes();
			arTaxCodes.add(arTaxCode);
		} catch (Exception ex) {
			throw new EJBException(ex.getMessage());
		}
	}

	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public void dropArTaxCode(com.ejb.ar.LocalArTaxCode arTaxCode) {

		Debug.print("GlChartOfAccountBean dropArTaxCode");
		try {
			Collection arTaxCodes = getArTaxCodes();
			arTaxCodes.remove(arTaxCode);
		} catch (Exception ex) {
			throw new EJBException(ex.getMessage());
		}
	}

	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public void addArWithholdingTaxCode(com.ejb.ar.LocalArWithholdingTaxCode arWithholdingTaxCode) {

		Debug.print("GlChartOfAccountBean addArWithholdingTaxCode");
		try {
			Collection arWithholdingTaxCodes = getArWithholdingTaxCodes();
			arWithholdingTaxCodes.add(arWithholdingTaxCode);
		} catch (Exception ex) {
			throw new EJBException(ex.getMessage());
		}
	}

	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public void dropArWithholdingTaxCode(com.ejb.ar.LocalArWithholdingTaxCode arWithholdingTaxCode) {

		Debug.print("GlChartOfAccountBean dropArWithholdingTaxCode");
		try {
			Collection arWithholdingTaxCodes = getArWithholdingTaxCodes();
			arWithholdingTaxCodes.remove(arWithholdingTaxCode);
		} catch (Exception ex) {
			throw new EJBException(ex.getMessage());
		}
	}

	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public void addArDistributionRecord(com.ejb.ar.LocalArDistributionRecord arDistributionRecord) {

		Debug.print("GlChartOfAccountBean addArDistributionRecord");
		try {
			Collection arDistributionRecords = getArDistributionRecords();
			arDistributionRecords.add(arDistributionRecord);
		} catch (Exception ex) {
			throw new EJBException(ex.getMessage());
		}
	}

	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public void dropArDistributionRecord(com.ejb.ar.LocalArDistributionRecord arDistributionRecord) {

		Debug.print("GlChartOfAccountBean dropArDistributionRecord");
		try {
			Collection arDistributionRecords = getArDistributionRecords();
			arDistributionRecords.remove(arDistributionRecord);
		} catch (Exception ex) {
			throw new EJBException(ex.getMessage());
		}
	}

	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public void addArStandardMemoLine(com.ejb.ar.LocalArStandardMemoLine arStandardMemoLine) {

		Debug.print("GlChartOfAccountBean addArStandardMemoLine");
		try {
			Collection arStandardMemoLines = getArStandardMemoLines();
			arStandardMemoLines.add(arStandardMemoLine);
		} catch (Exception ex) {
			throw new EJBException(ex.getMessage());
		}
	}

	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public void dropArStandardMemoLine(com.ejb.ar.LocalArStandardMemoLine arStandardMemoLine) {

		Debug.print("GlChartOfAccountBean dropArStandardMemoLine");
		try {
			Collection arStandardMemoLines = getArStandardMemoLines();
			arStandardMemoLines.remove(arStandardMemoLine);
		} catch (Exception ex) {
			throw new EJBException(ex.getMessage());
		}
	}

	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public void addAdApprovalCoaLine(com.ejb.ad.LocalAdApprovalCoaLine adApprovalCoaLine) {

		Debug.print("GlChartOfAccountBean addAdApprovalCoaLine");
		try {
			Collection adApprovalCoaLines = getAdApprovalCoaLines();
			adApprovalCoaLines.add(adApprovalCoaLine);
		} catch (Exception ex) {
			throw new EJBException(ex.getMessage());
		}
	}

	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public void dropAdApprovalCoaLine(com.ejb.ad.LocalAdApprovalCoaLine adApprovalCoaLine) {

		Debug.print("GlChartOfAccountBean dropAdApprovalCoaLine");
		try {
			Collection adApprovalCoaLines = getAdApprovalCoaLines();
			adApprovalCoaLines.remove(adApprovalCoaLine);
		} catch (Exception ex) {
			throw new EJBException(ex.getMessage());
		}
	}

	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public void addGlBudgetAmountCoa(LocalGlBudgetAmountCoa glBudgetAmountCoa) {

		Debug.print("GlChartOfAccountBean addGlBudgetAmountCoa");
		try {
			Collection glBudgetAmountCoas = getGlBudgetAmountCoas();
			glBudgetAmountCoas.add(glBudgetAmountCoa);
		} catch (Exception ex) {
			throw new EJBException(ex.getMessage());
		}
	}

	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public void dropGlBudgetAmountCoa(LocalGlBudgetAmountCoa glBudgetAmountCoa) {

		Debug.print("GlChartOfAccountBean dropGlBudgetAmountCoa");
		try {
			Collection glBudgetAmountCoas = getGlBudgetAmountCoas();
			glBudgetAmountCoas.remove(glBudgetAmountCoa);
		} catch (Exception ex) {
			throw new EJBException(ex.getMessage());
		}
	}

	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public void addInvAdjustment(com.ejb.inv.LocalInvAdjustment invAdjustment) {

		Debug.print("GlChartOfAccountBean addInvAdjustment");
		try {
			Collection invAdjustments = getInvAdjustments();
			invAdjustments.add(invAdjustment);
		} catch (Exception ex) {
			throw new EJBException(ex.getMessage());
		}
	}

	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public void dropInvAdjustment(com.ejb.inv.LocalInvAdjustment invAdjustment) {

		Debug.print("GlChartOfAccountBean dropInvAdjustment");
		try {
			Collection invAdjustments = getInvAdjustments();
			invAdjustments.remove(invAdjustment);
		} catch (Exception ex) {
			throw new EJBException(ex.getMessage());
		}
	}

	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public void addInvDistributionRecord(com.ejb.inv.LocalInvDistributionRecord invDistributionRecord) {

		Debug.print("GlChartOfAccountBean addInvDistributionRecord");
		try {
			Collection invDistributionRecords = getInvDistributionRecords();
			invDistributionRecords.add(invDistributionRecord);
		} catch (Exception ex) {
			throw new EJBException(ex.getMessage());
		}
	}

	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public void dropInvDistributionRecord(com.ejb.inv.LocalInvDistributionRecord invDistributionRecord) {

		Debug.print("GlChartOfAccountBean dropInvDistributionRecord");
		try {
			Collection invDistributionRecords = getInvDistributionRecords();
			invDistributionRecords.remove(invDistributionRecord);
		} catch (Exception ex) {
			throw new EJBException(ex.getMessage());
		}
	}

	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public void addAdPaymentTerm(com.ejb.ad.LocalAdPaymentTerm adPaymentTerm) {

		Debug.print("GlChartOfAccountBean addAdPaymentTerm");
		try {
			Collection adPaymentTerms = getAdPaymentTerms();
			adPaymentTerms.add(adPaymentTerm);
		} catch (Exception ex) {
			throw new EJBException(ex.getMessage());
		}
	}

	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public void dropAdPaymentTerm(com.ejb.ad.LocalAdPaymentTerm adPaymentTerm) {

		Debug.print("GlChartOfAccountBean dropAdPaymentTerm");
		try {
			Collection adPaymentTerms = getAdPaymentTerms();
			adPaymentTerms.remove(adPaymentTerm);
		} catch (Exception ex) {
			throw new EJBException(ex.getMessage());
		}
	}

	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public void addAdBranchCoa(LocalAdBranchCoa adBranchCoa) {

		Debug.print("GlChartOfAccountBean addAdBranchCoa");

		try {

			Collection adBranchCoas = getAdBranchCoas();
			adBranchCoas.add(adBranchCoa);

		} catch (Exception ex) {

			throw new EJBException(ex.getMessage());

		}

	}

	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public void dropAdBranchCoa(LocalAdBranchCoa adBranchCoa) {

		Debug.print("GlChartOfAccountBean dropAdBranchCoa");

		try {

			Collection adBranchCoas = getAdBranchCoas();
			adBranchCoas.remove(adBranchCoa);

		} catch (Exception ex) {

			throw new EJBException(ex.getMessage());

		}

	}

	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public void addGlForexLedger(LocalGlForexLedger glForexLedger) {

		Debug.print("GlChartOfAccountBean addGlForexLedger");

		try {

			Collection glForexLedgers = getGlForexLedgers();
			glForexLedgers.add(glForexLedger);

		} catch (Exception ex) {

			throw new EJBException(ex.getMessage());

		}

	}

	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public void dropGlForexLedger(LocalGlForexLedger glForexLedger) {

		Debug.print("GlChartOfAccountBean dropGlForexLedger");

		try {

			Collection glForexLedgers = getGlForexLedgers();
			glForexLedgers.remove(glForexLedger);

		} catch (Exception ex) {

			throw new EJBException(ex.getMessage());

		}

	}

	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public void addAdBranch(com.ejb.ad.LocalAdBranch adBranch) {

		Debug.print("GlChartOfAccountBean addAdBranch");
		try {
			Collection adBranches = getAdBranches();
			adBranches.add(adBranch);
		} catch (Exception ex) {
			throw new EJBException(ex.getMessage());
		}
	}

	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public void dropAdBranch(com.ejb.ad.LocalAdBranch adBranch) {

		Debug.print("GlChartOfAccountBean dropAdBranch");
		try {
			Collection adBranches = getAdBranches();
			adBranches.remove(adBranch);
		} catch (Exception ex) {
			throw new EJBException(ex.getMessage());
		}
	}

	// EntityBean methods

	/**
	 * @ejb:create-method view-type="local"
	 **/
	public java.lang.Integer ejbCreate (java.lang.Integer COA_CODE,
			java.lang.String COA_ACCNT_NMBR, java.lang.String COA_ACCNT_DESC,
			java.lang.String COA_ACCNT_TYP, java.lang.String COA_TX_TYP,
			String COA_CIT_CTGRY, String COA_SAW_CTGRY, String COA_IIT_CTGRY,
			Date COA_DT_FRM, Date COA_DT_TO,
			String COA_SGMNT1, String COA_SGMNT2, String COA_SGMNT3, String COA_SGMNT4, String COA_SGMNT5,
			String COA_SGMNT6, String COA_SGMNT7, String COA_SGMNT8, String COA_SGMNT9, String COA_SGMNT10,
			byte COA_ENBL, byte COA_FR_RVLTN, Integer COA_AD_CMPNY)
	throws CreateException {

		Debug.print("GlChartOfAccountBean ejbCreate");
		setCoaCode(COA_CODE);
		setCoaAccountNumber(COA_ACCNT_NMBR);
		setCoaAccountDescription(COA_ACCNT_DESC);
		setCoaAccountType(COA_ACCNT_TYP);
		setCoaTaxType(COA_TX_TYP);
		setCoaCitCategory(COA_CIT_CTGRY);
		setCoaSawCategory(COA_SAW_CTGRY);
		setCoaIitCategory(COA_IIT_CTGRY);
		setCoaDateFrom(COA_DT_FRM);
		setCoaDateTo(COA_DT_TO);
		setCoaSegment1(COA_SGMNT1);
		setCoaSegment2(COA_SGMNT2);
		setCoaSegment3(COA_SGMNT3);
		setCoaSegment4(COA_SGMNT4);
		setCoaSegment5(COA_SGMNT5);
		setCoaSegment6(COA_SGMNT6);
		setCoaSegment7(COA_SGMNT7);
		setCoaSegment8(COA_SGMNT8);
		setCoaSegment9(COA_SGMNT9);
		setCoaSegment10(COA_SGMNT10);
		setCoaEnable(COA_ENBL);
		setCoaForRevaluation(COA_FR_RVLTN);
		setCoaAdCompany(COA_AD_CMPNY);

		return null;
	}

	/**
	 * @ejb:create-method view-type="local"
	 **/
	public java.lang.Integer ejbCreate (
			java.lang.String COA_ACCNT_NMBR, java.lang.String COA_ACCNT_DESC,
			java.lang.String COA_ACCNT_TYP, java.lang.String COA_TX_TYP,
			String COA_CIT_CTGRY, String COA_SAW_CTGRY,String COA_IIT_CTGRY,
			Date COA_DT_FRM, Date COA_DT_TO,
			String COA_SGMNT1, String COA_SGMNT2, String COA_SGMNT3, String COA_SGMNT4, String COA_SGMNT5,
			String COA_SGMNT6, String COA_SGMNT7, String COA_SGMNT8, String COA_SGMNT9, String COA_SGMNT10,
			byte COA_ENBL, byte COA_FR_RVLTN, Integer COA_AD_CMPNY)
	throws CreateException {

		Debug.print("GlChartOfAccountBean ejbCreate");

		setCoaAccountNumber(COA_ACCNT_NMBR);
		setCoaAccountDescription(COA_ACCNT_DESC);
		setCoaAccountType(COA_ACCNT_TYP);
		setCoaTaxType(COA_TX_TYP);
		setCoaCitCategory(COA_CIT_CTGRY);
		setCoaSawCategory(COA_SAW_CTGRY);
		setCoaIitCategory(COA_IIT_CTGRY);
		setCoaDateFrom(COA_DT_FRM);
		setCoaDateTo(COA_DT_TO);
		setCoaSegment1(COA_SGMNT1);
		setCoaSegment2(COA_SGMNT2);
		setCoaSegment3(COA_SGMNT3);
		setCoaSegment4(COA_SGMNT4);
		setCoaSegment5(COA_SGMNT5);
		setCoaSegment6(COA_SGMNT6);
		setCoaSegment7(COA_SGMNT7);
		setCoaSegment8(COA_SGMNT8);
		setCoaSegment9(COA_SGMNT9);
		setCoaSegment10(COA_SGMNT10);
		setCoaEnable(COA_ENBL);
		setCoaForRevaluation(COA_FR_RVLTN);
		setCoaAdCompany(COA_AD_CMPNY);

		return null;
	}

	public void ejbPostCreate (java.lang.Integer COA_CODE,
			java.lang.String COA_ACCNT_NMBR, java.lang.String COA_ACCNT_DESC,
			java.lang.String COA_ACCNT_TYP, java.lang.String COA_TX_TYP,
			String COA_CIT_CTGRY, String COA_SAW_CTGRY, String COA_IIT_CTGRY,
			Date COA_DT_FRM, Date COA_DT_TO,
			String COA_SGMNT1, String COA_SGMNT2, String COA_SGMNT3, String COA_SGMNT4, String COA_SGMNT5,
			String COA_SGMNT6, String COA_SGMNT7, String COA_SGMNT8, String COA_SGMNT9, String COA_SGMNT10,
			byte COA_ENBL, byte COA_FR_RVLTN, Integer COA_AD_CMPNY)
	throws CreateException { }

	public void ejbPostCreate (
			java.lang.String COA_ACCNT_NMBR, java.lang.String COA_ACCNT_DESC,
			java.lang.String COA_ACCNT_TYP, java.lang.String COA_TX_TYP,
			String COA_CIT_CTGRY, String COA_SAW_CTGRY, String COA_IIT_CTGRY,
			Date COA_DT_FRM, Date COA_DT_TO,
			String COA_SGMNT1, String COA_SGMNT2, String COA_SGMNT3, String COA_SGMNT4, String COA_SGMNT5,
			String COA_SGMNT6, String COA_SGMNT7, String COA_SGMNT8, String COA_SGMNT9, String COA_SGMNT10,
			byte COA_ENBL, byte COA_FR_RVLTN, Integer COA_AD_CMPNY)
	throws CreateException { }

} // GlChartOfAccountBean class
