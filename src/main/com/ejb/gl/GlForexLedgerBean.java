/*
 * Created on May 10, 2006
 *
 */
package com.ejb.gl;

import java.util.Date;

import javax.ejb.CreateException;

import com.util.AbstractEntityBean;
import com.util.Debug;

/**
 * @ejb:bean name="GlForexLedgerEJB"
 *           display-name="FOREX Ledger Entity"
 *           type="CMP"
 *           cmp-version="2.x"
 *           view-type="local"
 *           local-jndi-name="omega-ejb/GlForexLedgerEJB"
 *           schema="GlForexLedger"
 *           primkey-field="frlCode"
 *       
 * @ejb:pk class="java.lang.Integer"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="gluser"
 *                        role-link="gluserlink"
 *
 * @ejb:permission role-name="gluser"
 *
 * @ejb:interface local-class="com.ejb.gl.LocalGlForexLedger"
 *                local-extends="javax.ejb.EJBLocalObject"
 *
 * @ejb:home local-class="com.ejb.gl.LocalGlForexLedgerHome"
 *           local-extends="javax.ejb.EJBLocalHome"
 *
 * @ejb:finder signature="Collection findLatestGlFrlByFrlDateAndByCoaCode(java.util.Date FRL_DT, java.lang.Integer COA_CODE, java.lang.Integer AD_CMPNY)"
 * 			   query="SELECT OBJECT(frl) FROM GlForexLedger frl WHERE frl.frlDate<=?1 AND frl.glChartOfAccount.coaCode =?2 AND frl.frlAdCompany =?3"
 * 
 * @jboss:query signature="Collection findLatestGlFrlByFrlDateAndByCoaCode(java.util.Date FRL_DT, java.lang.Integer COA_CODE, java.lang.Integer AD_CMPNY)"
 * 			   query="SELECT OBJECT(frl) FROM GlForexLedger frl WHERE frl.frlDate<=?1 AND frl.glChartOfAccount.coaCode =?2 AND frl.frlAdCompany =?3 ORDER BY frl.frlDate DESC, frl.frlLine DESC"
 *
 * @ejb:finder signature="Collection findByGreaterThanFrlDateAndCoaCode(java.util.Date FRL_DT, java.lang.Integer COA_CODE, java.lang.Integer AD_CMPNY)"
 *             query="SELECT OBJECT(frl) FROM GlForexLedger frl WHERE frl.frlDate>?1 AND frl.glChartOfAccount.coaCode =?2 AND frl.frlAdCompany =?3"
 *
 * @ejb:finder signature="Collection findLatestGlFrlByFrlTypeAndFrlDateAndCoaCode(java.lang.String FRL_TYP, java.util.Date FRL_DT, java.lang.Integer COA_CODE, java.lang.Integer AD_CMPNY)"
 *             query="SELECT OBJECT(frl) FROM GlForexLedger frl WHERE frl.frlType=?1 AND frl.frlDate<=?2 AND frl.glChartOfAccount.coaCode =?3 AND frl.frlAdCompany =?4"
 * 
 * @jboss:query signature="Collection findLatestGlFrlByFrlTypeAndFrlDateAndCoaCode(java.lang.String FRL_TYP, java.util.Date FRL_DT, java.lang.Integer COA_CODE, java.lang.Integer AD_CMPNY)"
 * 			   query="SELECT OBJECT(frl) FROM GlForexLedger frl WHERE frl.frlType=?1 AND frl.frlDate<=?2 AND frl.glChartOfAccount.coaCode =?3 AND frl.frlAdCompany =?4 ORDER BY frl.frlDate DESC, frl.frlLine DESC" 
 *
 * @ejb:finder signature="Collection findByFrlDateFromAndFrlDateToAndCoaCode(java.util.Date FRL_DT_FRM, java.util.Date FRL_DT_TO, java.lang.Integer COA_CODE, java.lang.Integer AD_CMPNY)"
 *             query="SELECT OBJECT(frl) FROM GlForexLedger frl WHERE frl.frlDate>=?1 AND frl.frlDate<=?2 AND frl.glChartOfAccount.coaCode =?3 AND frl.frlAdCompany =?4"

 *  @ejb:value-object match="*"
 *             name="GlForexLedger"
 * 
 * @jboss:persistence table-name="GL_FRX_LDGR"
 *
 * @jboss:entity-command name="mysql-get-generated-keys"
 *
 */
public abstract class GlForexLedgerBean extends AbstractEntityBean {
	
	// Access methods for persistent fields
	
	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 * @ejb:pk-field
	 *
	 * @jboss:column-name name="FRL_CODE"
	 *
	 * @jboss:persistence auto-increment="true"
	 **/
	public abstract Integer getFrlCode();
	public abstract void setFrlCode(Integer FRL_CODE);
	
	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="FRL_DT"
	 **/
	public abstract Date getFrlDate();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setFrlDate(Date FRL_DT);
	
	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="FRL_LN"
	 **/
	public abstract Integer getFrlLine();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setFrlLine(Integer FRL_LN);
	
	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="FRL_TYP"
	 **/
	public abstract String getFrlType();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setFrlType(String FRL_TYP);

	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="FRL_AMNT"
	 **/
	public abstract double getFrlAmount();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setFrlAmount(double FRL_AMNT);

	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="FRL_RT"
	 **/
	public abstract double getFrlRate();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setFrlRate(double FRL_RT);

	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="FRL_BLNC"
	 **/
	public abstract double getFrlBalance();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setFrlBalance(double FRL_BLNC);

	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="FRL_FRX_GN_LSS"
	 **/
	public abstract double getFrlForexGainLoss();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setFrlForexGainLoss(double FRL_FRX_GN_LSS);
	
	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="FRL_AD_CMPNY"
	 **/
	public abstract java.lang.Integer getFrlAdCompany();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setFrlAdCompany(Integer FRL_AD_CMPNY);
	
	// Access methods for relationship fields   

	/**
	 * @ejb:interface-method view-type="local"
	 * @ejb:relation name="chartofaccount-forexledgers"
	 *               role-name="forexledger-has-one-chartofaccount"
	 *               cascade-delete="yes"
	 *
	 * @jboss:relation related-pk-field="coaCode"
	 *                 fk-column="GL_CHART_OF_ACCOUNT"
	 */
	public abstract LocalGlChartOfAccount getGlChartOfAccount();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setGlChartOfAccount(LocalGlChartOfAccount glChartOfAccount);
	
	// EntityBean methods
	
	/**
	 * @ejb:create-method view-type="local"
	 **/
	public Integer ejbCreate (Integer FRL_CODE, Date FRL_DT, Integer FRL_LN, String FRL_TYP, double FRL_AMNT, 
		double FRL_RT, double FRL_BLNC, double FRL_FRX_GN_LSS, Integer FRL_AD_CMPNY)
	throws CreateException {
		
		Debug.print("GlForexLedgerBean ejbCreate");
		setFrlCode(FRL_CODE);
		setFrlDate(FRL_DT);
		setFrlLine(FRL_LN);
		setFrlType(FRL_TYP);
		setFrlAmount(FRL_AMNT);
		setFrlRate(FRL_RT);
		setFrlBalance(FRL_BLNC);
		setFrlForexGainLoss(FRL_FRX_GN_LSS);
		setFrlAdCompany(FRL_AD_CMPNY);
		
		return null;
	}
	
	/**
	 * @ejb:create-method view-type="local"
	 **/
	public Integer ejbCreate (Date FRL_DT, Integer FRL_LN, String FRL_TYP, double FRL_AMNT, double FRL_RT,
		double FRL_BLNC, double FRL_FRX_GN_LSS, Integer FRL_AD_CMPNY)
	throws CreateException {
		
		Debug.print("GlForexLedgerBean ejbCreate");
		setFrlDate(FRL_DT);
		setFrlLine(FRL_LN);
		setFrlType(FRL_TYP);
		setFrlAmount(FRL_AMNT);		
		setFrlRate(FRL_RT);
		setFrlBalance(FRL_BLNC);
		setFrlForexGainLoss(FRL_FRX_GN_LSS);
		setFrlAdCompany(FRL_AD_CMPNY);
		
		return null;
	}
	
	public void ejbPostCreate(Integer FRL_CODE, Date FRL_DT, Integer FRL_LN, String FRL_TYP,
		double FRL_AMNT, double FRL_RT,double FRL_BLNC, double FRL_FRX_GN_LSS, Integer FRL_AD_CMPNY)      
	throws CreateException { }

	public void ejbPostCreate(Date FRL_DT, Integer FRL_LN, String FRL_TYP, double FRL_AMNT, double FRL_RT,
		double FRL_BLNC, double FRL_FRX_GN_LSS, Integer FRL_AD_CMPNY)      
	throws CreateException { }

}// GlForexLedgerBean Class
