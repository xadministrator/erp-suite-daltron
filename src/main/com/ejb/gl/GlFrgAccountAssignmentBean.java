package com.ejb.gl;

import javax.ejb.CreateException;

import com.util.AbstractEntityBean;
import com.util.Debug;

/**
 * @ejb:bean name="GlFrgAccountAssignmentEJB"
 *           display-name="Row Entity"
 *           type="CMP"
 *           cmp-version="2.x"
 *           view-type="local"
 *           local-jndi-name="omega-ejb/GlFrgAccountAssignmentEJB"
 *           schema="GlFrgAccountAssignment"
 *           primkey-field="aaCode"
 *       
 * @ejb:pk class="java.lang.Integer"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="gluser"
 *                        role-link="gluserlink"
 *
 * @ejb:permission role-name="gluser"
 *
 * @ejb:interface local-class="com.ejb.gl.LocalGlFrgAccountAssignment"
 *                local-extends="javax.ejb.EJBLocalObject"
 *
 * @ejb:home local-class="com.ejb.gl.LocalGlFrgAccountAssignmentHome"
 *           local-extends="javax.ejb.EJBLocalHome"
 *
 * @ejb:finder signature="Collection findByRowCode(java.lang.Integer ROW_CODE, java.lang.Integer AA_AD_CMPNY)"
 *             query="SELECT OBJECT(aa) FROM GlFrgRow row, IN(row.glFrgAccountAssignments) aa WHERE row.rowCode = ?1 AND aa.aaAdCompany = ?2"
 *
 * @jboss:query signature="Collection findByRowCode(java.lang.Integer ROW_CODE, java.lang.Integer AA_AD_CMPNY)"
 *             query="SELECT OBJECT(aa) FROM GlFrgRow row, IN(row.glFrgAccountAssignments) aa WHERE row.rowCode = ?1 AND aa.aaAdCompany = ?2 ORDER BY aa.aaCode"
 *
 * @ejb:value-object match="*"
 *             name="GlFrgAccountAssignment"
 *
 * @jboss:persistence table-name="GL_FRG_ACCNT_ASSGNMNT"
 *
 * @jboss:entity-command name="mysql-get-generated-keys"
 *
 */

public abstract class GlFrgAccountAssignmentBean extends AbstractEntityBean {


   // Access methods for persistent fields

   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    * @ejb:pk-field
    *
    * @jboss:column-name name="AA_CODE"
    *
    * @jboss:persistence auto-increment="true"
    **/
   public abstract Integer getAaCode();
   public abstract void setAaCode(Integer AA_CODE);
   
   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="AA_ACCNT_LOW"
    **/
   public abstract String getAaAccountLow();
     /**
      * @ejb:interface-method view-type="local"
      **/
   public abstract void setAaAccountLow(String AA_ACCNT_LOW);
   
   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="AA_ACCNT_HGH"
    **/
   public abstract String getAaAccountHigh();
     /**
      * @ejb:interface-method view-type="local"
      **/
   public abstract void setAaAccountHigh(String AA_ACCNT_HGH);
   
   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="AA_ACTVTY_TYP"
    **/
   public abstract String getAaActivityType();
     /**
      * @ejb:interface-method view-type="local"
      **/
   public abstract void setAaActivityType(String AA_ACTVTY_TYP);
   
   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="AA_AD_CMPNY"
    **/
   public abstract Integer getAaAdCompany();
     /**
      * @ejb:interface-method view-type="local"
      **/
   public abstract void setAaAdCompany(Integer AA_AD_CMPNY);
   
   
   // Access methods for relationship fields
   
   /**
    * @ejb:interface-method view-type="local"
    * @ejb:relation name="frgrow-frgaccountassignments"
    *               role-name="frgaccountassignment-has-one-frgrow"
    *
    * @jboss:relation related-pk-field="rowCode"
    *                 fk-column="GL_FRG_ROW"
    */
   public abstract LocalGlFrgRow getGlFrgRow();
   public abstract void setGlFrgRow(LocalGlFrgRow glFrgRow);
      

   // Business methods
   
   
   
   // EntityBean methods

   /**
    * @ejb:create-method view-type="local"
    **/
   public java.lang.Integer ejbCreate (Integer AA_CODE, String AA_ACCNT_LOW,
      String AA_ACCNT_HGH, String AA_ACTVTY_TYP, Integer AA_AD_CMPNY)
      throws CreateException {

      Debug.print("GlFrgAccountAssignmentBean ejbCreate");
      setAaCode(AA_CODE);
      setAaAccountLow(AA_ACCNT_LOW);
      setAaAccountHigh(AA_ACCNT_HGH);
      setAaActivityType(AA_ACTVTY_TYP);
      setAaAdCompany(AA_AD_CMPNY);
      
      return null;
   }
   
   /**
    * @ejb:create-method view-type="local"
    **/
   public java.lang.Integer ejbCreate (String AA_ACCNT_LOW,
      String AA_ACCNT_HGH, String AA_ACTVTY_TYP, Integer AA_AD_CMPNY) 
      throws CreateException {

      Debug.print("GlFrgAccountAssignmentBean ejbCreate");
      setAaAccountLow(AA_ACCNT_LOW);
      setAaAccountHigh(AA_ACCNT_HGH);
      setAaActivityType(AA_ACTVTY_TYP);
      setAaAdCompany(AA_AD_CMPNY);
      
      return null;
   }
   
   public void ejbPostCreate (Integer AA_CODE, String AA_ACCNT_LOW,
      String AA_ACCNT_HGH, String AA_ACTVTY_TYP, Integer AA_AD_CMPNY) 
      throws CreateException { }

   public void ejbPostCreate (String AA_ACCNT_LOW,
      String AA_ACCNT_HGH, String AA_ACTVTY_TYP, Integer AA_AD_CMPNY) 
      throws CreateException { }

} // GlFrgAccountAssignmentBean class
