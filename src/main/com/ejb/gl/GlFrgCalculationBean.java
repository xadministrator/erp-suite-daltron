package com.ejb.gl;

import javax.ejb.CreateException;

import com.util.AbstractEntityBean;
import com.util.Debug;

/**
 * @ejb:bean name="GlFrgCalculationEJB"
 *           display-name="Row Entity"
 *           type="CMP"
 *           cmp-version="2.x"
 *           view-type="local"
 *           local-jndi-name="omega-ejb/GlFrgCalculationEJB"
 *           schema="GlFrgCalculation"
 *           primkey-field="calCode"
 *       
 * @ejb:pk class="java.lang.Integer"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="gluser"
 *                        role-link="gluserlink"
 *
 * @ejb:permission role-name="gluser"
 *
 * @ejb:interface local-class="com.ejb.gl.LocalGlFrgCalculation"
 *                local-extends="javax.ejb.EJBLocalObject"
 *
 * @ejb:home local-class="com.ejb.gl.LocalGlFrgCalculationHome"
 *           local-extends="javax.ejb.EJBLocalHome"
 *
 * @ejb:finder signature="Collection findByRowCodeAndCalType(java.lang.Integer ROW_CODE, java.lang.String CAL_TYP, java.lang.Integer CAL_AD_CMPNY)"
 *             query="SELECT OBJECT(cal) FROM GlFrgRow row, IN(row.glFrgCalculations) cal WHERE row.rowCode = ?1 AND cal.calType = ?2 AND cal.calAdCompany = ?3"
 *
 * @jboss:query signature="Collection findByRowCodeAndCalType(java.lang.Integer ROW_CODE, java.lang.String CAL_TYP, java.lang.Integer CAL_AD_CMPNY)"
 *             query="SELECT OBJECT(cal) FROM GlFrgRow row, IN(row.glFrgCalculations) cal WHERE row.rowCode = ?1 AND cal.calType = ?2 AND cal.calAdCompany = ?3 ORDER BY cal.calSequenceNumber"
 *
 * @ejb:finder signature="Collection findByColCode(java.lang.Integer ROW_CODE, java.lang.Integer CAL_AD_CMPNY)"
 *             query="SELECT OBJECT(cal) FROM GlFrgColumn col, IN(col.glFrgCalculations) cal WHERE col.colCode = ?1 AND cal.calAdCompany = ?2"
 *
 * @jboss:query signature="Collection findByColCode(java.lang.Integer ROW_CODE, java.lang.Integer CAL_AD_CMPNY)"
 *             query="SELECT OBJECT(cal) FROM GlFrgColumn col, IN(col.glFrgCalculations) cal WHERE col.colCode = ?1 AND cal.calAdCompany = ?2 ORDER BY cal.calSequenceNumber"
 *
 * @ejb:finder signature="LocalGlFrgCalculation findByRowCodeAndCalSequenceNumberAndCalType(java.lang.Integer ROW_CODE, int CAL_SQNC_NMBR, java.lang.String CAL_TYP, java.lang.Integer CAL_AD_CMPNY)"
 *             query="SELECT OBJECT(cal) FROM GlFrgRow row, IN(row.glFrgCalculations) cal WHERE row.rowCode = ?1 AND cal.calSequenceNumber = ?2 AND cal.calType = ?3 AND cal.calAdCompany = ?4"
 *
 * @ejb:finder signature="LocalGlFrgCalculation findByColCodeAndCalSequenceNumber(java.lang.Integer COL_CODE, int CAL_SQNC_NMBR, java.lang.Integer CAL_AD_CMPNY)"
 *             query="SELECT OBJECT(cal) FROM GlFrgColumn col, IN(col.glFrgCalculations) cal WHERE col.colCode = ?1 AND cal.calSequenceNumber = ?2 AND cal.calAdCompany = ?3"
 *
 * @ejb:value-object match="*"
 *             name="GlFrgCalculation"
 *
 * @jboss:persistence table-name="GL_FRG_CLCLTN"
 *
 * @jboss:entity-command name="mysql-get-generated-keys"
 *
 */

public abstract class GlFrgCalculationBean extends AbstractEntityBean {


   // Access methods for persistent fields

   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    * @ejb:pk-field
    *
    * @jboss:column-name name="CAL_CODE"
    *
    * @jboss:persistence auto-increment="true"
    **/
   public abstract Integer getCalCode();
   public abstract void setCalCode(Integer CAL_CODE);
   
   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="CAL_SQNC_NMBR"
    **/
   public abstract int getCalSequenceNumber();
     /**
      * @ejb:interface-method view-type="local"
      **/
   public abstract void setCalSequenceNumber(int CAL_SQNC_NMBR);
   
   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="CAL_OPRTR"
    **/
   public abstract String getCalOperator();
     /**
      * @ejb:interface-method view-type="local"
      **/
   public abstract void setCalOperator(String CAL_OPRTR);
         
   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="CAL_TYP"
    **/
   public abstract String getCalType();
     /**
      * @ejb:interface-method view-type="local"
      **/
   public abstract void setCalType(String CAL_TYP);

   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="CAL_CNSTNT"
    **/
   public abstract double getCalConstant();
     /**
      * @ejb:interface-method view-type="local"
      **/
   public abstract void setCalConstant(double CAL_CNSTNT);
   
   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="CAL_ROW_COL_NM"
    **/
   public abstract String getCalRowColName();
     /**
      * @ejb:interface-method view-type="local"
      **/
   public abstract void setCalRowColName(String CAL_ROW_COL_NM);
   
   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="CAL_SQNC_LW"
    **/
   public abstract int getCalSequenceLow();
     /**
      * @ejb:interface-method view-type="local"
      **/
   public abstract void setCalSequenceLow(int CAL_SQNC_LW);
   
   
   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="CAL_SQNC_HGH"
    **/
   public abstract int getCalSequenceHigh();
     /**
      * @ejb:interface-method view-type="local"
      **/
   public abstract void setCalSequenceHigh(int CAL_SQNC_HGH);
   
   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="CAL_AD_CMPNY"
    **/
   public abstract Integer getCalAdCompany();
     /**
      * @ejb:interface-method view-type="local"
      **/
   public abstract void setCalAdCompany(Integer CAL_AD_CMPNY);
   
   
   // Access methods for relationship fields
   
   /**
    * @ejb:interface-method view-type="local"
    * @ejb:relation name="frgrow-frgcalculations"
    *               role-name="frgcalculation-has-one-frgrow"
    *
    * @jboss:relation related-pk-field="rowCode"
    *                 fk-column="GL_FRG_ROW"
    */
   public abstract LocalGlFrgRow getGlFrgRow();
   public abstract void setGlFrgRow(LocalGlFrgRow glFrgRow);
   
   
   /**
    * @ejb:interface-method view-type="local"
    * @ejb:relation name="frgcolumn-frgcalculations"
    *               role-name="frgcalculation-has-one-frgcolumn"
    *
    * @jboss:relation related-pk-field="colCode"
    *                 fk-column="GL_FRG_COLUMN"
    */
   public abstract LocalGlFrgColumn getGlFrgColumn();
   public abstract void setGlFrgColumn(LocalGlFrgColumn glFrgColumn);
   
   
      

   // Business methods
   
   
   
   // EntityBean methods

   /**
    * @ejb:create-method view-type="local"
    **/
   public java.lang.Integer ejbCreate (Integer CAL_CODE, int CAL_SQNC_NMBR,
      String CAL_OPRTR, String CAL_TYP, double CAL_CNSTNT, String CAL_ROW_COL_NM, int CAL_SQNC_LW, 
      int CAL_SQNC_HGH, Integer CAL_AD_CMPNY)
      throws CreateException {

      Debug.print("GlFrgCalculationBean ejbCreate");
      setCalCode(CAL_CODE);
      setCalSequenceNumber(CAL_SQNC_NMBR);
      setCalOperator(CAL_OPRTR);
      setCalType(CAL_TYP);
      setCalConstant(CAL_CNSTNT);
      setCalRowColName(CAL_ROW_COL_NM);
      setCalSequenceLow(CAL_SQNC_LW);
      setCalSequenceHigh(CAL_SQNC_HGH);
      setCalAdCompany(CAL_AD_CMPNY);
      
      return null;
   }
   
   /**
    * @ejb:create-method view-type="local"
    **/
   public java.lang.Integer ejbCreate (int CAL_SQNC_NMBR,
      String CAL_OPRTR, String CAL_TYP, double CAL_CNSTNT, String CAL_ROW_COL_NM, int CAL_SQNC_LW, 
      int CAL_SQNC_HGH, Integer CAL_AD_CMPNY) 
      throws CreateException {

      Debug.print("GlFrgCalculationBean ejbCreate");
      setCalSequenceNumber(CAL_SQNC_NMBR);
      setCalOperator(CAL_OPRTR);
      setCalType(CAL_TYP);
      setCalConstant(CAL_CNSTNT);
      setCalRowColName(CAL_ROW_COL_NM);
      setCalSequenceLow(CAL_SQNC_LW);
      setCalSequenceHigh(CAL_SQNC_HGH);
      setCalAdCompany(CAL_AD_CMPNY);
      
      return null;
   }
   
   public void ejbPostCreate (Integer CAL_CODE, int CAL_SQNC_NMBR,
      String CAL_OPRTR, String CAL_TYP, double CAL_CNSTNT, String CAL_ROW_COL_NM, int CAL_SQNC_LW, 
      int CAL_SQNC_HGH, Integer CAL_AD_CMPNY) 
      throws CreateException { }

   public void ejbPostCreate (int CAL_SQNC_NMBR,
      String CAL_OPRTR, String CAL_TYP, double CAL_CNSTNT, String CAL_ROW_COL_NM, int CAL_SQNC_LW, 
      int CAL_SQNC_HGH, Integer CAL_AD_CMPNY) 
      throws CreateException { }

} // GlFrgCalculationBean class
