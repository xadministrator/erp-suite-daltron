package com.ejb.gl;

import java.util.Collection;

import javax.ejb.CreateException;
import javax.ejb.EJBException;

import com.util.AbstractEntityBean;
import com.util.Debug;

/**
 * @ejb:bean name="GlFrgColumnEJB"
 *           display-name="Column Entity"
 *           type="CMP"
 *           cmp-version="2.x"
 *           view-type="local"
 *           local-jndi-name="omega-ejb/GlFrgColumnEJB"
 *           schema="GlFrgColumn"
 *           primkey-field="colCode"
 *       
 * @ejb:pk class="java.lang.Integer"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="gluser"
 *                        role-link="gluserlink"
 *
 * @ejb:permission role-name="gluser"
 *
 * @ejb:interface local-class="com.ejb.gl.LocalGlFrgColumn"
 *                local-extends="javax.ejb.EJBLocalObject"
 *
 * @ejb:home local-class="com.ejb.gl.LocalGlFrgColumnHome"
 *           local-extends="javax.ejb.EJBLocalHome"
 *
 * @ejb:finder signature="Collection findByFrName(java.lang.String FR_NM, java.lang.Integer COL_AD_CMPNY)"
 *             query="SELECT OBJECT(col) FROM GlFrgColumnSet cs, IN(cs.glFrgColumns) col, IN(cs.glFrgFinancialReports) fr WHERE fr.frName = ?1 AND col.colAdCompany = ?2"
 *
 * @jboss:query signature="Collection findByFrName(java.lang.String FR_NM, java.lang.Integer COL_AD_CMPNY)"
 *             query="SELECT OBJECT(col) FROM GlFrgColumnSet cs, IN(cs.glFrgColumns) col, IN(cs.glFrgFinancialReports) fr WHERE fr.frName = ?1 AND col.colAdCompany = ?2 ORDER BY col.colSequenceNumber"
 *
 * @ejb:finder signature="Collection findByCsName(java.lang.String CS_NM, java.lang.Integer COL_AD_CMPNY)"
 *             query="SELECT OBJECT(col) FROM GlFrgColumnSet cs, IN(cs.glFrgColumns) col WHERE cs.csName = ?1 AND col.colAdCompany = ?2"
 *
 * @jboss:query signature="Collection findByCsName(java.lang.String CS_NM, java.lang.Integer COL_AD_CMPNY)"
 *             query="SELECT OBJECT(col) FROM GlFrgColumnSet cs, IN(cs.glFrgColumns) col WHERE cs.csName = ?1 AND col.colAdCompany = ?2 ORDER BY col.colSequenceNumber"
 *
 * @ejb:finder signature="LocalGlFrgColumn findByColNameAndCsName(java.lang.String COL_NM, java.lang.String CS_NM, java.lang.Integer COL_AD_CMPNY)"
 *             query="SELECT OBJECT(col) FROM GlFrgColumnSet cs, IN(cs.glFrgColumns) col WHERE col.colName = ?1 AND cs.csName = ?2 AND col.colAdCompany = ?3"
 *
 * @ejb:finder signature="LocalGlFrgColumn findByColumnNameAndCsCode(java.lang.String COL_NM, java.lang.Integer CS_CODE, java.lang.Integer COL_AD_CMPNY)"
 *             query="SELECT OBJECT(col) FROM GlFrgColumnSet cs, IN(cs.glFrgColumns) col WHERE col.colName=?1 AND cs.csCode=?2 AND col.colAdCompany = ?3"
 *
 * @ejb:finder signature="LocalGlFrgColumn findBySequenceNumberAndCsCode(int COL_SQNC_NMBR, java.lang.Integer CS_CODE, java.lang.Integer COL_AD_CMPNY)"
 *             query="SELECT OBJECT(col) FROM GlFrgColumnSet cs, IN(cs.glFrgColumns) col WHERE col.colSequenceNumber=?1 AND cs.csCode=?2 AND col.colAdCompany = ?3"
 *
 * @ejb:finder signature="Collection findByCsCode(java.lang.Integer CS_CODE, java.lang.Integer COL_AD_CMPNY)"
 *             query="SELECT OBJECT(col) FROM GlFrgColumnSet cs, IN(cs.glFrgColumns) col WHERE cs.csCode = ?1 AND col.colAdCompany = ?2"
 *
 * @jboss:query signature="Collection findByCsCode(java.lang.Integer CS_CODE, java.lang.Integer COL_AD_CMPNY)"
 *             query="SELECT OBJECT(col) FROM GlFrgColumnSet cs, IN(cs.glFrgColumns) col WHERE cs.csCode = ?1 AND col.colAdCompany = ?2 ORDER BY col.colSequenceNumber"
 *
 * @ejb:finder signature="Collection findByColumnSetAndColSequenceNumber(java.lang.Integer CS_CODE, int COL_SQNC_NMBR, java.lang.Integer COL_AD_CMPNY)"
 *             query="SELECT OBJECT(col) FROM GlFrgColumnSet cs, IN(cs.glFrgColumns) col WHERE cs.csCode = ?1 AND col.colSequenceNumber < ?2 AND col.colAdCompany = ?3"
 *
 * @jboss:query signature="Collection findByColumnSetAndColSequenceNumber(java.lang.Integer CS_CODE, int COL_SQNC_NMBR, java.lang.Integer COL_AD_CMPNY)"
 *             query="SELECT OBJECT(col) FROM GlFrgColumnSet cs, IN(cs.glFrgColumns) col WHERE cs.csCode = ?1 AND col.colSequenceNumber < ?2 AND col.colAdCompany = ?3 ORDER BY col.colSequenceNumber"
 *
 * @ejb:finder signature="Collection findColumnAll(java.lang.Integer COL_AD_CMPNY)"
 *             query="SELECT OBJECT(col) FROM GlFrgColumn col WHERE col.colAdCompany = ?1"
 *
 * @ejb:value-object match="*"
 *             name="GlFrgColumn"
 *
 * @jboss:persistence table-name="GL_FRG_CLMN"
 *
 * @jboss:entity-command name="mysql-get-generated-keys"
 *
 */

public abstract class GlFrgColumnBean extends AbstractEntityBean {


   // Access methods for persistent fields

   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    * @ejb:pk-field
    *
    * @jboss:column-name name="COL_CODE"
    *
    * @jboss:persistence auto-increment="true"
    **/
   public abstract Integer getColCode();
   public abstract void setColCode(Integer COL_CODE);
   
   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="COL_NM"
    **/
   public abstract String getColName();
     /**
      * @ejb:interface-method view-type="local"
      **/
   public abstract void setColName(String COL_NM);
   
   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="COL_PSTN"
    **/
   public abstract int getColPosition();
     /**
      * @ejb:interface-method view-type="local"
      **/
   public abstract void setColPosition(int COL_PSTN);

   

   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="COL_SQNC_NMBR"
    **/
   public abstract int getColSequenceNumber();
     /**
      * @ejb:interface-method view-type="local"
      **/
   public abstract void setColSequenceNumber(int COL_SQNC_NMBR);
   
   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="COL_FRMT_MSK"
    **/
   public abstract String getColFormatMask();
     /**
      * @ejb:interface-method view-type="local"
      **/
   public abstract void setColFormatMask(String COL_FRMT_MSK);
   
   
   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="COL_FCTR"
    **/
   public abstract String getColFactor();
     /**
      * @ejb:interface-method view-type="local"
      **/
   public abstract void setColFactor(String COL_FCTR);
   
   
   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="COL_AMNT_TYP"
    **/
   public abstract String getColAmountType();
     /**
      * @ejb:interface-method view-type="local"
      **/
   public abstract void setColAmountType(String COL_AMNT_TYP);
   
   
   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="COL_OFFST"
    **/
   public abstract int getColOffset();
     /**
      * @ejb:interface-method view-type="local"
      **/
   public abstract void setColOffset(int COL_OFFST);
   
   
   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="COL_OVRRD_ROW_CALC"
    **/
   public abstract byte getColOverrideRowCalculation();
     /**
      * @ejb:interface-method view-type="local"
      **/
   public abstract void setColOverrideRowCalculation(byte COL_OVRRD_ROW_CALC);
   
   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="COL_AD_CMPNY"
    **/
   public abstract Integer getColAdCompany();
     /**
      * @ejb:interface-method view-type="local"
      **/
   public abstract void setColAdCompany(Integer COL_AD_CMPNY);
   

   // Access methods for relationship fields
   
   /**
    * @ejb:interface-method view-type="local"
    * @ejb:relation name="frgcolumnset-frgcolumns"
    *               role-name="frgcolumn-has-one-frgcolumnset"
    *
    * @jboss:relation related-pk-field="csCode"
    *                 fk-column="GL_FRG_COLUMN_SET"
    */
   public abstract LocalGlFrgColumnSet getGlFrgColumnSet();
   public abstract void setGlFrgColumnSet(LocalGlFrgColumnSet glFrgColumnSet);
   
   /**
    * @ejb:interface-method view-type="local"
    * @ejb:relation name="frgcolumn-frgcalculations"
    *               role-name="frgcolumn-has-many-frgcalculations"
    */
   public abstract Collection getGlFrgCalculations();
   public abstract void setGlFrgCalculations(Collection glFrgCalculations);
   
   /**
	 * @ejb:interface-method view-type="local"
	 * @ejb:relation name="functionalcurrency-frgcolumns"
	 *               role-name="frgcolumn-has-one-functionalcurrency"
	 *               cascade-delete="no"
	 *
	 * @jboss:relation related-pk-field="fcCode"
	 *                 fk-column="GL_FUNCTIONAL_CURRENCY"
	 */
	public abstract LocalGlFunctionalCurrency getGlFunctionalCurrency();
	public abstract void setGlFunctionalCurrency(LocalGlFunctionalCurrency glFunctionalCurrency);
   
   // Business methods  
   
   /**
    * @ejb:interface-method view-type="local"
    **/
   public void addGlFrgCalculation(LocalGlFrgCalculation glFrgCalculation) {

      Debug.print("GlFrgColumnBean addGlFrgCalculation");
      
      try {
      	
         Collection glFrgCalculations = getGlFrgCalculations();
	 		glFrgCalculations.add(glFrgCalculation);
	 		
      } catch (Exception ex) {
      	
         throw new EJBException(ex.getMessage());
         
      }
   }

   /**
    * @ejb:interface-method view-type="local"
    **/
   public void dropGlFrgCalculation(LocalGlFrgCalculation glFrgCalculation) {

      Debug.print("GlFrgColumnBean dropGlFrgCalculation");
      
      try {
      	
         Collection glFrgCalculations = getGlFrgCalculations();
	 		glFrgCalculations.remove(glFrgCalculation);
	 		
      } catch (Exception ex) {
      	
         throw new EJBException(ex.getMessage());
         
      }
   }
   
   // EntityBean methods

   /**
    * @ejb:create-method view-type="local"
    **/
   public java.lang.Integer ejbCreate (Integer COL_CODE, String COL_NM, int COL_PSTN,
      int COL_SQNC_NMBR, String COL_FRMT_MSK, String COL_FCTR, String COL_AMNT_TYP,
      int COL_OFFST, byte COL_OVRRD_ROW_CALC, Integer COL_AD_CMPNY)
      throws CreateException {

      Debug.print("GlFrgColumnBean ejbCreate");
      setColCode(COL_CODE);
      setColName(COL_NM);
      setColPosition(COL_PSTN);
      setColSequenceNumber(COL_SQNC_NMBR);
      setColFormatMask(COL_FRMT_MSK);
      setColFactor(COL_FCTR);
      setColAmountType(COL_AMNT_TYP);
      setColOffset(COL_OFFST);
      setColOverrideRowCalculation(COL_OVRRD_ROW_CALC);
      setColAdCompany(COL_AD_CMPNY);
      
      return null;
   }
   
   /**
    * @ejb:create-method view-type="local"
    **/
   public java.lang.Integer ejbCreate (String COL_NM, int COL_PSTN,
      int COL_SQNC_NMBR, String COL_FRMT_MSK, String COL_FCTR, String COL_AMNT_TYP,
      int COL_OFFST, byte COL_OVRRD_ROW_CALC, Integer COL_AD_CMPNY) 
      throws CreateException {

      Debug.print("GlFrgColumnBean ejbCreate");
      setColName(COL_NM);
      setColPosition(COL_PSTN);
      setColSequenceNumber(COL_SQNC_NMBR);
      setColFormatMask(COL_FRMT_MSK);
      setColFactor(COL_FCTR);
      setColAmountType(COL_AMNT_TYP);
      setColOffset(COL_OFFST);
      setColOverrideRowCalculation(COL_OVRRD_ROW_CALC);
      setColAdCompany(COL_AD_CMPNY);
      
      return null;
   }
   
   public void ejbPostCreate (Integer COL_CODE, String COL_NM, int COL_PSTN,
      int COL_SQNC_NMBR, String COL_FRMT_MSK, String COL_FCTR, String COL_AMNT_TYP,
      int COL_OFFST, byte COL_OVRRD_ROW_CALC, Integer COL_AD_CMPNY) 
      throws CreateException { }

   public void ejbPostCreate (String COL_NM, int COL_PSTN,
      int COL_SQNC_NMBR, String COL_FRMT_MSK, String COL_FCTR, String COL_AMNT_TYP,
      int COL_OFFST, byte COL_OVRRD_ROW_CALC, Integer COL_AD_CMPNY) 
      throws CreateException { }

} // GlFrgColumnBean class
