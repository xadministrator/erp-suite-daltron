package com.ejb.gl;

import java.util.Collection;

import javax.ejb.CreateException;
import javax.ejb.EJBException;

import com.util.AbstractEntityBean;
import com.util.Debug;

/**
 * @ejb:bean name="GlFrgColumnSetEJB"
 *           display-name="Column Set Entity"
 *           type="CMP"
 *           cmp-version="2.x"
 *           view-type="local"
 *           local-jndi-name="omega-ejb/GlFrgColumnSetEJB"
 *           schema="GlFrgColumnSet"
 *           primkey-field="csCode"
 *       
 * @ejb:pk class="java.lang.Integer"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="gluser"
 *                        role-link="gluserlink"
 *
 * @ejb:permission role-name="gluser"
 *
 * @ejb:interface local-class="com.ejb.gl.LocalGlFrgColumnSet"
 *                local-extends="javax.ejb.EJBLocalObject"
 *
 * @ejb:home local-class="com.ejb.gl.LocalGlFrgColumnSetHome"
 *           local-extends="javax.ejb.EJBLocalHome"
 *
 * @ejb:finder signature="Collection findCsAll(java.lang.Integer CS_AD_CMPNY)"
 *             query="SELECT OBJECT(cs) FROM GlFrgColumnSet cs WHERE cs.csAdCompany = ?1"
 *
 * @jboss:query signature="Collection findCsAll(java.lang.Integer CS_AD_CMPNY)"
 *             query="SELECT OBJECT(cs) FROM GlFrgColumnSet cs WHERE cs.csAdCompany = ?1 ORDER BY cs.csName"
 *
 * @ejb:finder signature="LocalGlFrgColumnSet findByCsName(java.lang.String CS_NM, java.lang.Integer CS_AD_CMPNY)"
 *             query="SELECT OBJECT(Cs) FROM GlFrgColumnSet cs WHERE cs.csName = ?1 AND cs.csAdCompany = ?2"
 *
 * @ejb:value-object match="*"
 *             name="GlFrgColumnSet"
 *
 * @jboss:persistence table-name="GL_FRG_CLMN_ST"
 *
 * @jboss:entity-command name="mysql-get-generated-keys"
 *
 */

public abstract class GlFrgColumnSetBean extends AbstractEntityBean {


   // Access methods for persistent fields

   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    * @ejb:pk-field
    *
    * @jboss:column-name name="CS_CODE"
    *
    * @jboss:persistence auto-increment="true"
    **/
   public abstract Integer getCsCode();
   public abstract void setCsCode(Integer CS_CODE);

   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="CS_NM"
    **/
   public abstract String getCsName();
     /**
      * @ejb:interface-method view-type="local"
      **/  
   public abstract void setCsName(String CS_NM);

   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="CS_DESC"
    **/
   public abstract String getCsDescription();
     /**
      * @ejb:interface-method view-type="local"
      **/  
   public abstract void setCsDescription(String CS_DESC);
   
   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="CS_AD_CMPNY"
    **/
   public abstract Integer getCsAdCompany();
     /**
      * @ejb:interface-method view-type="local"
      **/  
   public abstract void setCsAdCompany(Integer CS_AD_CMPNY);


   // Access methods for relationship fields

   /**
    * @ejb:interface-method view-type="local"
    * @ejb:relation name="frgcolumnset-frgfinancialreports"
    *               role-name="frgcolumnset-has-many-frgfinancialreports"
    */
   public abstract Collection getGlFrgFinancialReports();
   public abstract void setGlFrgFinancialReports(Collection glFrgFinancialReports);
   
   /**
    * @ejb:interface-method view-type="local"
    * @ejb:relation name="frgcolumnset-frgcolumns"
    *               role-name="frgcolumnset-has-many-frgcolumns"
    */
   public abstract Collection getGlFrgColumns();
   public abstract void setGlFrgColumns(Collection glFrgColumns);

   // Business methods
   
   /**
    * @ejb:interface-method view-type="local"
    **/
   public void addGlFrgFinancialReport(LocalGlFrgFinancialReport glFrgFinancialReport) {

      Debug.print("GlFrgColumnSetBean addGlFrgFinancialReport");
      
      try {
      	
         Collection glFrgFinancialReports = getGlFrgFinancialReports();
	 		glFrgFinancialReports.add(glFrgFinancialReport);
	 		
      } catch (Exception ex) {
      	
         throw new EJBException(ex.getMessage());
         
      }
   }

   /**
    * @ejb:interface-method view-type="local"
    **/
   public void dropGlFrgFinancialReport(LocalGlFrgFinancialReport glFrgFinancialReport) {

      Debug.print("GlFrgColumnSetBean dropGlFrgFinancialReport");
      
      try {
      	
         Collection glFrgFinancialReports = getGlFrgFinancialReports();
	 		glFrgFinancialReports.remove(glFrgFinancialReport);
	 		
      } catch (Exception ex) {
      	
         throw new EJBException(ex.getMessage());
         
      }
   }
   
   
   /**
    * @ejb:interface-method view-type="local"
    **/
   public void addGlFrgColumn(LocalGlFrgColumn glFrgColumn) {

      Debug.print("GlFrgColumnSetBean addGlFrgColumn");
      
      try {
      	
         Collection glFrgColumns = getGlFrgColumns();
	 		glFrgColumns.add(glFrgColumn);
	 		
      } catch (Exception ex) {
      	
         throw new EJBException(ex.getMessage());
         
      }
   }

   /**
    * @ejb:interface-method view-type="local"
    **/
   public void dropGlFrgColumn(LocalGlFrgColumn glFrgColumn) {

      Debug.print("GlFrgColumnSetBean dropGlFrgColumn");
      
      try {
      	
         Collection glFrgColumns = getGlFrgColumns();
	 		glFrgColumns.remove(glFrgColumn);
	 		
      } catch (Exception ex) {
      	
         throw new EJBException(ex.getMessage());
         
      }
   }
   
   // EntityBean methods

   /**
    * @ejb:create-method view-type="local"
    **/
   public java.lang.Integer ejbCreate (Integer CS_CODE, String CS_NM, String CS_DESC,
   		Integer CS_AD_CMPNY) 
      throws CreateException {

      Debug.print("GlFrgColumnSetBean ejbCreate");
      setCsCode(CS_CODE);
      setCsName(CS_NM);
      setCsDescription(CS_DESC);
      setCsAdCompany(CS_AD_CMPNY);
      
      return null;
   }
   
   /**
    * @ejb:create-method view-type="local"
    **/
   public java.lang.Integer ejbCreate (String CS_NM, String CS_DESC,
   		Integer CS_AD_CMPNY) 
      throws CreateException {

      Debug.print("GlFrgColumnSetBean ejbCreate");
      setCsName(CS_NM);
      setCsDescription(CS_DESC);
      setCsAdCompany(CS_AD_CMPNY);
      
      return null;
   }
   
   public void ejbPostCreate (Integer CS_CODE, String CS_NM, String CS_DESC,
   		Integer CS_AD_CMPNY) 
      throws CreateException { }

   public void ejbPostCreate (String CS_NM, String CS_DESC,
   		Integer CS_AD_CMPNY) 
      throws CreateException { }

} // GlFrgColumnSetBean class
