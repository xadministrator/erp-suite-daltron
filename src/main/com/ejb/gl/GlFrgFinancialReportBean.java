package com.ejb.gl;

import javax.ejb.CreateException;

import com.util.AbstractEntityBean;
import com.util.Debug;

/**
 * @ejb:bean name="GlFrgFinancialReportEJB"
 *           display-name="Financial Report Entity"
 *           type="CMP"
 *           cmp-version="2.x"
 *           view-type="local"
 *           local-jndi-name="omega-ejb/GlFrgFinancialReportEJB"
 *           schema="GlFrgFinancialReport"
 *           primkey-field="frCode"
 *       
 * @ejb:pk class="java.lang.Integer"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="gluser"
 *                        role-link="gluserlink"
 *
 * @ejb:permission role-name="gluser"
 *
 * @ejb:interface local-class="com.ejb.gl.LocalGlFrgFinancialReport"
 *                local-extends="javax.ejb.EJBLocalObject"
 *
 * @ejb:home local-class="com.ejb.gl.LocalGlFrgFinancialReportHome"
 *           local-extends="javax.ejb.EJBLocalHome"
 *
 * @ejb:finder signature="LocalGlFrgFinancialReport findByFrName(java.lang.String FR_NM, java.lang.Integer FR_AD_CMPNY)"
 *             query="SELECT OBJECT(fr) FROM GlFrgFinancialReport fr WHERE fr.frName = ?1 AND fr.frAdCompany = ?2"
 *
 * @jboss:query signature="LocalGlFrgFinancialReport findByFrName(java.lang.String FR_NM, java.lang.Integer FR_AD_CMPNY)"
 *             query="SELECT OBJECT(fr) FROM GlFrgFinancialReport fr WHERE fr.frName = ?1 AND fr.frAdCompany = ?2 ORDER BY fr.frCode"
 *
 * @ejb:finder signature="Collection findFrAll(java.lang.Integer FR_AD_CMPNY)"
 *             query="SELECT OBJECT(fr) FROM GlFrgFinancialReport fr WHERE fr.frAdCompany = ?1"
 *
 * @jboss:query signature="Collection findFrAll(java.lang.Integer FR_AD_CMPNY)"
 *             query="SELECT OBJECT(fr) FROM GlFrgFinancialReport fr WHERE fr.frAdCompany = ?1 ORDER BY fr.frName"
 *
 * @ejb:value-object match="*"
 *             name="GlFrgFinancialReport"
 *
 * @jboss:persistence table-name="GL_FRG_FNNCL_RPRT"
 *
 * @jboss:entity-command name="mysql-get-generated-keys"
 *
 */

public abstract class GlFrgFinancialReportBean extends AbstractEntityBean {


   // Access methods for persistent fields

   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    * @ejb:pk-field
    *
    * @jboss:column-name name="FR_CODE"
    *
    * @jboss:persistence auto-increment="true"
    **/
   public abstract Integer getFrCode();
   public abstract void setFrCode(Integer FR_CODE);

   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="FR_NM"
    **/
   public abstract String getFrName();
     /**
      * @ejb:interface-method view-type="local"
      **/  
   public abstract void setFrName(String FR_NM);

   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="FR_DESC"
    **/
   public abstract String getFrDescription();
     /**
      * @ejb:interface-method view-type="local"
      **/  
   public abstract void setFrDescription(String FR_DESC);

   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="FR_TTLE"
    **/
   public abstract String getFrTitle();
     /**
      * @ejb:interface-method view-type="local"
      **/  
   public abstract void setFrTitle(String FR_TTLE);
   
   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="FR_FNT_SZ"
    **/
   public abstract int getFrFontSize();
    /**
     * @ejb:interface-method view-type="local"
     **/
   public abstract void setFrFontSize(int FR_FNT_SZ);
   
   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="FR_FNT_STYL"
    **/
   public abstract String getFrFontStyle();
    /**
     * @ejb:interface-method view-type="local"
     **/
   public abstract void setFrFontStyle(String FR_FNT_STYL);
   
   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="FR_HRZNTL_ALGN"
    **/
   public abstract String getFrHorizontalAlign();
     /**
      * @ejb:interface-method view-type="local"
      **/
   public abstract void setFrHorizontalAlign(String FR_HRZNTL_ALGN);
  
   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="FR_AD_CMPNY"
    **/
   public abstract Integer getFrAdCompany();
     /**
      * @ejb:interface-method view-type="local"
      **/  
   public abstract void setFrAdCompany(Integer FR_AD_CMPNY);

   // Access methods for relationship fields

   /**
    * @ejb:interface-method view-type="local"
    * @ejb:relation name="frgrowset-frgfinancialreports"
    *               role-name="frgfinancialreport-has-one-frgrowset"
    *
    * @jboss:relation related-pk-field="rsCode"
    *                 fk-column="GL_FRG_ROW_SET"
    */
   public abstract LocalGlFrgRowSet getGlFrgRowSet();
   public abstract void setGlFrgRowSet(LocalGlFrgRowSet glFrgRowSet);
   
   /**
    * @ejb:interface-method view-type="local"
    * @ejb:relation name="frgcolumnset-frgfinancialreports"
    *               role-name="frgfinancialreport-has-one-frgcolumnset"
    *
    * @jboss:relation related-pk-field="csCode"
    *                 fk-column="GL_FRG_COLUMN_SET"
    */
   public abstract LocalGlFrgColumnSet getGlFrgColumnSet();
   public abstract void setGlFrgColumnSet(LocalGlFrgColumnSet glFrgColumnSet);

   // Business methods
   
   // EntityBean methods

   /**
    * @ejb:create-method view-type="local"
    **/
   public java.lang.Integer ejbCreate (Integer FR_CODE, String FR_NM, String FR_DESC,
      String FR_TTLE, int FR_FNT_SZ, String FR_FNT_STYL, String FR_HRZNTL_ALGN, Integer FR_AD_CMPNY) 
      throws CreateException {

      Debug.print("GlFrgFinancialReportBean ejbCreate");
      setFrCode(FR_CODE);
      setFrName(FR_NM);
      setFrDescription(FR_DESC);
      setFrTitle(FR_TTLE);
      setFrFontSize(FR_FNT_SZ);
      setFrFontStyle(FR_FNT_STYL);
      setFrHorizontalAlign(FR_HRZNTL_ALGN);
      setFrAdCompany(FR_AD_CMPNY);
      
      return null;
   }
   
   /**
    * @ejb:create-method view-type="local"
    **/
   public java.lang.Integer ejbCreate (String FR_NM, String FR_DESC,
      String FR_TTLE, int FR_FNT_SZ, String FR_FNT_STYL, String FR_HRZNTL_ALGN, Integer FR_AD_CMPNY) 
      throws CreateException {

      Debug.print("GlFrgFinancialReportBean ejbCreate");
      setFrName(FR_NM);
      setFrDescription(FR_DESC);
      setFrTitle(FR_TTLE);
      setFrFontSize(FR_FNT_SZ);
      setFrFontStyle(FR_FNT_STYL);
      setFrHorizontalAlign(FR_HRZNTL_ALGN);
      setFrAdCompany(FR_AD_CMPNY);
      
      return null;
   }
   
   public void ejbPostCreate (Integer FR_CODE, String FR_NM, String FR_DESC,
      String FR_TTLE, int FR_FNT_SZ, String FR_FNT_STYL, String FR_HRZNTL_ALGN, Integer FR_AD_CMPNY) 
      throws CreateException { }

   public void ejbPostCreate (String FR_NM, String FR_DESC,
      String FR_TTLE, int FR_FNT_SZ, String FR_FNT_STYL, String FR_HRZNTL_ALGN, Integer FR_AD_CMPNY) 
      throws CreateException { }

} // GlFrgFinancialReportBean class
