package com.ejb.gl;

import java.util.Collection;

import javax.ejb.CreateException;
import javax.ejb.EJBException;

import com.util.AbstractEntityBean;
import com.util.Debug;

/**
 * @ejb:bean name="GlFrgRowEJB"
 *           display-name="Row Entity"
 *           type="CMP"
 *           cmp-version="2.x"
 *           view-type="local"
 *           local-jndi-name="omega-ejb/GlFrgRowEJB"
 *           schema="GlFrgRow"
 *           primkey-field="rowCode"
 *       
 * @ejb:pk class="java.lang.Integer"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="gluser"
 *                        role-link="gluserlink"
 *
 * @ejb:permission role-name="gluser"
 *
 * @ejb:interface local-class="com.ejb.gl.LocalGlFrgRow"
 *                local-extends="javax.ejb.EJBLocalObject"
 *
 * @ejb:home local-class="com.ejb.gl.LocalGlFrgRowHome"
 *           local-extends="javax.ejb.EJBLocalHome"
 *
 * @ejb:finder signature="Collection findByFrName(java.lang.String FR_NM, java.lang.Integer ROW_AD_CMPNY)"
 *             query="SELECT OBJECT(row) FROM GlFrgRowSet rs, IN(rs.glFrgRows) row, IN(rs.glFrgFinancialReports) fr WHERE fr.frName = ?1 AND row.rowAdCompany = ?2"
 *
 * @jboss:query signature="Collection findByFrName(java.lang.String FR_NM, java.lang.Integer ROW_AD_CMPNY)"
 *             query="SELECT OBJECT(row) FROM GlFrgRowSet rs, IN(rs.glFrgRows) row, IN(rs.glFrgFinancialReports) fr WHERE fr.frName = ?1 AND row.rowAdCompany = ?2 ORDER BY row.rowLineNumber"
 *
 * @ejb:finder signature="Collection findByRsName(java.lang.String RS_NM, java.lang.Integer ROW_AD_CMPNY)"
 *             query="SELECT OBJECT(row) FROM GlFrgRowSet rs, IN(rs.glFrgRows) row WHERE rs.rsName = ?1 AND row.rowAdCompany = ?2"
 *
 * @jboss:query signature="Collection findByRsName(java.lang.String RS_NM, java.lang.Integer ROW_AD_CMPNY)"
 *             query="SELECT OBJECT(row) FROM GlFrgRowSet rs, IN(rs.glFrgRows) row WHERE rs.rsName = ?1 AND row.rowAdCompany = ?2 ORDER BY row.rowLineNumber"
 *
 * @ejb:finder signature="LocalGlFrgRow findByRowNameAndRsName(java.lang.String ROW_NM, java.lang.String RS_NM, java.lang.Integer ROW_AD_CMPNY)"
 *             query="SELECT OBJECT(row) FROM GlFrgRowSet rs, IN(rs.glFrgRows) row WHERE row.rowName = ?1 AND rs.rsName = ?2 AND row.rowAdCompany = ?3"
 * 
 * @ejb:finder signature="LocalGlFrgRow findByRowNameAndRsCode(java.lang.String ROW_NM, java.lang.Integer RS_CODE, java.lang.Integer ROW_AD_CMPNY)"
 *             query="SELECT OBJECT(row) FROM GlFrgRowSet rs, IN(rs.glFrgRows) row WHERE row.rowName=?1 AND rs.rsCode=?2 AND row.rowAdCompany = ?3"
 *
 * @ejb:finder signature="LocalGlFrgRow findByRowLineNumberAndRsCode(int ROW_LN_NMBR, java.lang.Integer RS_CODE, java.lang.Integer ROW_AD_CMPNY)"
 *             query="SELECT OBJECT(row) FROM GlFrgRowSet rs, IN(rs.glFrgRows) row WHERE row.rowLineNumber=?1 AND rs.rsCode=?2 AND row.rowAdCompany = ?3"
 *
 * @ejb:finder signature="Collection findByRsCode(java.lang.Integer RS_CODE, java.lang.Integer ROW_AD_CMPNY)"
 *             query="SELECT OBJECT(row) FROM GlFrgRowSet rs, IN(rs.glFrgRows) row WHERE rs.rsCode = ?1 AND row.rowAdCompany = ?2"
 *
 * @jboss:query signature="Collection findByRsCode(java.lang.Integer RS_CODE, java.lang.Integer ROW_AD_CMPNY)"
 *             query="SELECT OBJECT(row) FROM GlFrgRowSet rs, IN(rs.glFrgRows) row WHERE rs.rsCode = ?1 AND row.rowAdCompany = ?2 ORDER BY row.rowLineNumber"
 *
 * @ejb:finder signature="Collection findByRowSetAndRowLineNumber(java.lang.Integer RS_CODE, int RW_LN_NMBR, java.lang.Integer ROW_AD_CMPNY)"
 *             query="SELECT OBJECT(row) FROM GlFrgRowSet rs, IN(rs.glFrgRows) row WHERE rs.rsCode = ?1 AND row.rowLineNumber < ?2 AND row.rowAdCompany = ?3"
 *
 * @jboss:query signature="Collection findByRowSetAndRowLineNumber(java.lang.Integer RS_CODE, int RW_LN_NMBR, java.lang.Integer ROW_AD_CMPNY)"
 *             query="SELECT OBJECT(row) FROM GlFrgRowSet rs, IN(rs.glFrgRows) row WHERE rs.rsCode = ?1 AND row.rowLineNumber < ?2 AND row.rowAdCompany = ?3 ORDER BY row.rowLineNumber"
 *
 * @ejb:finder signature="Collection findRowAll(java.lang.Integer ROW_AD_CMPNY)"
 *             query="SELECT OBJECT(row) FROM GlFrgRow row WHERE row.rowAdCompany = ?1"
 *
 * @ejb:value-object match="*"
 *             name="GlFrgRow"
 * 
 * @jboss:persistence table-name="GL_FRG_RW"
 *
 * @jboss:entity-command name="mysql-get-generated-keys"
 *
 */

public abstract class GlFrgRowBean extends AbstractEntityBean {


   // Access methods for persistent fields

   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    * @ejb:pk-field
    *
    * @jboss:column-name name="ROW_CODE"
    *
    * @jboss:persistence auto-increment="true"
    **/
   public abstract Integer getRowCode();
   public abstract void setRowCode(Integer ROW_CODE);
   
   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="ROW_LN_NMBR"
    **/
   public abstract int getRowLineNumber();
     /**
      * @ejb:interface-method view-type="local"
      **/
   public abstract void setRowLineNumber(int ROW_LN_NMBR);

   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="ROW_NM"
    **/
   public abstract String getRowName();
     /**
      * @ejb:interface-method view-type="local"
      **/
   public abstract void setRowName(String ROW_NM);

   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="ROW_INDNT"
    **/
   public abstract int getRowIndent();
     /**
      * @ejb:interface-method view-type="local"
      **/
   public abstract void setRowIndent(int ROW_INDNT);
   
   
   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="ROW_LN_TO_SKP_BFR"
    **/
   public abstract int getRowLineToSkipBefore();
     /**
      * @ejb:interface-method view-type="local"
      **/
   public abstract void setRowLineToSkipBefore(int ROW_LN_TO_SKP_BFR);
   
   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="ROW_LN_TO_SKP_AFTR"
    **/
   public abstract int getRowLineToSkipAfter();
     /**
      * @ejb:interface-method view-type="local"
      **/
   public abstract void setRowLineToSkipAfter(int ROW_LN_TO_SKP_AFTR);
   
   
   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="ROW_UNDRLN_CHAR_BFR"
    **/
   public abstract int getRowUnderlineCharacterBefore();
     /**
      * @ejb:interface-method view-type="local"
      **/
   public abstract void setRowUnderlineCharacterBefore(int ROW_UNDRLN_CHAR_BFR);
   
   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="ROW_UNDRLN_CHAR_AFTR"
    **/
   public abstract int getRowUnderlineCharacterAfter();
     /**
      * @ejb:interface-method view-type="local"
      **/
   public abstract void setRowUnderlineCharacterAfter(int ROW_UNDRLN_CHAR_AFTR);
   
   
   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="ROW_PG_BRK_BFR"
    **/
   public abstract int getRowPageBreakBefore();
     /**
      * @ejb:interface-method view-type="local"
      **/
   public abstract void setRowPageBreakBefore(int ROW_PG_BRK_BFR);
   
   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="ROW_PG_BRK_AFTR"
    **/
   public abstract int getRowPageBreakAfter();
     /**
      * @ejb:interface-method view-type="local"
      **/
   public abstract void setRowPageBreakAfter(int ROW_PG_BRK_AFTR);
   
   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="ROW_OVRRD_COL_CALC"
    **/
   public abstract byte getRowOverrideColumnCalculation();
     /**
      * @ejb:interface-method view-type="local"
      **/
   public abstract void setRowOverrideColumnCalculation(byte ROW_OVRRD_COL_CALC);
   
   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="ROW_HD_RW"
    **/
   public abstract byte getRowHideRow();
     /**
      * @ejb:interface-method view-type="local"
      **/
   public abstract void setRowHideRow(byte ROW_HD_RW);
   
   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="ROW_FNT_STYL"
    **/
   public abstract String getRowFontStyle();
     /**
      * @ejb:interface-method view-type="local"
      **/
   public abstract void setRowFontStyle(String ROW_FNT_STYL);
   
   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="ROW_HRZNTL_ALGN"
    **/
   public abstract String getRowHorizontalAlign();
     /**
      * @ejb:interface-method view-type="local"
      **/
   public abstract void setRowHorizontalAlign(String ROW_HRZNTL_ALGN);
   
   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="ROW_AD_CMPNY"
    **/
   public abstract Integer getRowAdCompany();
     /**
      * @ejb:interface-method view-type="local"
      **/
   public abstract void setRowAdCompany(Integer ROW_AD_CMPNY);


   // Access methods for relationship fields
   
   /**
    * @ejb:interface-method view-type="local"
    * @ejb:relation name="frgrowset-frgrows"
    *               role-name="frgrow-has-one-frgrowset"
    *
    * @jboss:relation related-pk-field="rsCode"
    *                 fk-column="GL_FRG_ROW_SET"
    */
   public abstract LocalGlFrgRowSet getGlFrgRowSet();
   public abstract void setGlFrgRowSet(LocalGlFrgRowSet glFrgRowSet);

   /**
    * @ejb:interface-method view-type="local"
    * @ejb:relation name="frgrow-frgaccountassignments"
    *               role-name="frgrow-has-many-frgaccountassignments"
    */
   public abstract Collection getGlFrgAccountAssignments();
   public abstract void setGlFrgAccountAssignments(Collection glFrgAccountAssignments);
   
   /**
    * @ejb:interface-method view-type="local"
    * @ejb:relation name="frgrow-frgcalculations"
    *               role-name="frgrow-has-many-frgcalculations"
    */
   public abstract Collection getGlFrgCalculations();
   public abstract void setGlFrgCalculations(Collection glFrgCalculations);
   

   // Business methods
   
   /**
    * @ejb:interface-method view-type="local"
    **/
   public void addGlFrgAccountAssignment(LocalGlFrgAccountAssignment glFrgAccountAssignment) {

      Debug.print("GlFrgRowBean addGlFrgAccountAssignment");
      
      try {
      	
         Collection glFrgAccountAssignments = getGlFrgAccountAssignments();
	 		glFrgAccountAssignments.add(glFrgAccountAssignment);
	 		
      } catch (Exception ex) {
      	
         throw new EJBException(ex.getMessage());
         
      }
   }

   /**
    * @ejb:interface-method view-type="local"
    **/
   public void dropGlFrgAccountAssignment(LocalGlFrgAccountAssignment glFrgAccountAssignment) {

      Debug.print("GlFrgRowBean dropGlFrgAccountAssignment");
      
      try {
      	
         Collection glFrgAccountAssignments = getGlFrgAccountAssignments();
	 		glFrgAccountAssignments.remove(glFrgAccountAssignment);
	 		
      } catch (Exception ex) {
      	
         throw new EJBException(ex.getMessage());
         
      }
   }
   
   
   /**
    * @ejb:interface-method view-type="local"
    **/
   public void addGlFrgCalculation(LocalGlFrgCalculation glFrgCalculation) {

      Debug.print("GlFrgRowBean addGlFrgCalculation");
      
      try {
      	
         Collection glFrgCalculations = getGlFrgCalculations();
	 		glFrgCalculations.add(glFrgCalculation);
	 		
      } catch (Exception ex) {
      	
         throw new EJBException(ex.getMessage());
         
      }
   }

   /**
    * @ejb:interface-method view-type="local"
    **/
   public void dropGlFrgCalculation(LocalGlFrgCalculation glFrgCalculation) {

      Debug.print("GlFrgRowBean dropGlFrgCalculation");
      
      try {
      	
         Collection glFrgCalculations = getGlFrgCalculations();
	 		glFrgCalculations.remove(glFrgCalculation);
	 		
      } catch (Exception ex) {
      	
         throw new EJBException(ex.getMessage());
         
      }
   }
   
   // EntityBean methods

   /**
    * @ejb:create-method view-type="local"
    **/
   public java.lang.Integer ejbCreate (Integer ROW_CODE, int ROW_LN_NMBR,
      String ROW_NM, int ROW_INDNT, int ROW_LN_TO_SKP_BFR, int ROW_LN_TO_SKP_AFTR,
      int ROW_UNDRLN_CHAR_BFR, int ROW_UNDRLN_CHAR_AFTR, int ROW_PG_BRK_BFR, int ROW_PG_BRK_AFTR,
      byte ROW_OVRRD_COL_CALC, byte ROW_HD_RW, String ROW_FNT_STYL, String ROW_HRZNTL_ALGN, Integer ROW_AD_CMPNY)
      throws CreateException {

      Debug.print("GlFrgRowBean ejbCreate");
      setRowCode(ROW_CODE);
      setRowLineNumber(ROW_LN_NMBR);
      setRowName(ROW_NM);
      setRowIndent(ROW_INDNT);
      setRowLineToSkipBefore(ROW_LN_TO_SKP_BFR);
      setRowLineToSkipAfter(ROW_LN_TO_SKP_AFTR);
      setRowUnderlineCharacterBefore(ROW_UNDRLN_CHAR_BFR);
      setRowUnderlineCharacterAfter(ROW_UNDRLN_CHAR_AFTR);
      setRowPageBreakBefore(ROW_PG_BRK_BFR);
      setRowPageBreakAfter(ROW_PG_BRK_AFTR);
      setRowOverrideColumnCalculation(ROW_OVRRD_COL_CALC);
      setRowHideRow(ROW_HD_RW);
      setRowFontStyle(ROW_FNT_STYL);
      setRowHorizontalAlign(ROW_HRZNTL_ALGN);
      setRowAdCompany(ROW_AD_CMPNY);
      
      return null;
   }
   
   /**
    * @ejb:create-method view-type="local"
    **/
   public java.lang.Integer ejbCreate (int ROW_LN_NMBR,
      String ROW_NM, int ROW_INDNT, int ROW_LN_TO_SKP_BFR, int ROW_LN_TO_SKP_AFTR,
      int ROW_UNDRLN_CHAR_BFR, int ROW_UNDRLN_CHAR_AFTR, int ROW_PG_BRK_BFR, int ROW_PG_BRK_AFTR,
      byte ROW_OVRRD_COL_CALC, byte ROW_HD_RW, String ROW_FNT_STYL, String ROW_HRZNTL_ALGN, Integer ROW_AD_CMPNY) 
      throws CreateException {

      Debug.print("GlFrgRowBean ejbCreate");
      setRowLineNumber(ROW_LN_NMBR);
      setRowName(ROW_NM);
      setRowIndent(ROW_INDNT);
      setRowLineToSkipBefore(ROW_LN_TO_SKP_BFR);
      setRowLineToSkipAfter(ROW_LN_TO_SKP_AFTR);
      setRowUnderlineCharacterBefore(ROW_UNDRLN_CHAR_BFR);
      setRowUnderlineCharacterAfter(ROW_UNDRLN_CHAR_AFTR);
      setRowPageBreakBefore(ROW_PG_BRK_BFR);
      setRowPageBreakAfter(ROW_PG_BRK_AFTR);
      setRowOverrideColumnCalculation(ROW_OVRRD_COL_CALC);
      setRowHideRow(ROW_HD_RW);
      setRowFontStyle(ROW_FNT_STYL);
      setRowHorizontalAlign(ROW_HRZNTL_ALGN);
      setRowAdCompany(ROW_AD_CMPNY);
      
      return null;
   }
   
   public void ejbPostCreate (Integer ROW_CODE, int ROW_LN_NMBR,
      String ROW_NM, int ROW_INDNT, int ROW_LN_TO_SKP_BFR, int ROW_LN_TO_SKP_AFTR,
      int ROW_UNDRLN_CHAR_BFR, int ROW_UNDRLN_CHAR_AFTR, int ROW_PG_BRK_BFR, int ROW_PG_BRK_AFTR,
      byte ROW_OVRRD_COL_CALC, byte ROW_HD_RW, String ROW_FNT_STYL, String ROW_HRZNTL_ALGN, Integer ROW_AD_CMPNY) 
      throws CreateException { }

   public void ejbPostCreate (int ROW_LN_NMBR,
      String ROW_NM, int ROW_INDNT, int ROW_LN_TO_SKP_BFR, int ROW_LN_TO_SKP_AFTR,
      int ROW_UNDRLN_CHAR_BFR, int ROW_UNDRLN_CHAR_AFTR, int ROW_PG_BRK_BFR, int ROW_PG_BRK_AFTR,
      byte ROW_OVRRD_COL_CALC, byte ROW_HD_RW, String ROW_FNT_STYL, String ROW_HRZNTL_ALGN, Integer ROW_AD_CMPNY) 
      throws CreateException { }

} // GlFrgRowBean class
