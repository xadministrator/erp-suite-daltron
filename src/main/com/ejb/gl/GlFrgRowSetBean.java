package com.ejb.gl;

import java.util.Collection;

import javax.ejb.CreateException;
import javax.ejb.EJBException;

import com.util.AbstractEntityBean;
import com.util.Debug;

/**
 * @ejb:bean name="GlFrgRowSetEJB"
 *           display-name="Row Set Entity"
 *           type="CMP"
 *           cmp-version="2.x"
 *           view-type="local"
 *           local-jndi-name="omega-ejb/GlFrgRowSetEJB"
 *           schema="GlFrgRowSet"
 *           primkey-field="rsCode"
 *       
 * @ejb:pk class="java.lang.Integer"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="gluser"
 *                        role-link="gluserlink"
 *
 * @ejb:permission role-name="gluser"
 *
 * @ejb:interface local-class="com.ejb.gl.LocalGlFrgRowSet"
 *                local-extends="javax.ejb.EJBLocalObject"
 *
 * @ejb:home local-class="com.ejb.gl.LocalGlFrgRowSetHome"
 *           local-extends="javax.ejb.EJBLocalHome"
 *
 * @ejb:finder signature="Collection findRsAll(java.lang.Integer RS_AD_CMPNY)"
 *             query="SELECT OBJECT(rs) FROM GlFrgRowSet rs WHERE rs.rsAdCompany = ?1"
 *
 * @jboss:query signature="Collection findRsAll(java.lang.Integer RS_AD_CMPNY)"
 *             query="SELECT OBJECT(rs) FROM GlFrgRowSet rs WHERE rs.rsAdCompany = ?1 ORDER BY rs.rsName"
 *
 * @ejb:finder signature="LocalGlFrgRowSet findByRsName(java.lang.String RS_NM, java.lang.Integer RS_AD_CMPNY)"
 *             query="SELECT OBJECT(rs) FROM GlFrgRowSet rs WHERE rs.rsName = ?1 AND rs.rsAdCompany = ?2"
 *
 * @ejb:value-object match="*"
 *             name="GlFrgRowSet"
 *
 * @jboss:persistence table-name="GL_FRG_RW_ST"
 *
 * @jboss:entity-command name="mysql-get-generated-keys"
 *
 */

public abstract class GlFrgRowSetBean extends AbstractEntityBean {


   // Access methods for persistent fields

   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    * @ejb:pk-field
    *
    * @jboss:column-name name="RS_CODE"
    *
    * @jboss:persistence auto-increment="true"
    **/
   public abstract Integer getRsCode();
   public abstract void setRsCode(Integer RS_CODE);

   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="RS_NM"
    **/
   public abstract String getRsName();
     /**
      * @ejb:interface-method view-type="local"
      **/  
   public abstract void setRsName(String RS_NM);

   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="RS_DESC"
    **/
   public abstract String getRsDescription();
     /**
      * @ejb:interface-method view-type="local"
      **/  
   public abstract void setRsDescription(String RS_DESC);
   
   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="RS_AD_CMPNY"
    **/
   public abstract Integer getRsAdCompany();
     /**
      * @ejb:interface-method view-type="local"
      **/  
   public abstract void setRsAdCompany(Integer RS_AD_CMPNY);


   // Access methods for relationship fields

   /**
    * @ejb:interface-method view-type="local"
    * @ejb:relation name="frgrowset-frgfinancialreports"
    *               role-name="frgrowset-has-many-frgfinancialreports"
    */
   public abstract Collection getGlFrgFinancialReports();
   public abstract void setGlFrgFinancialReports(Collection glFrgFinancialReports);
   
   /**
    * @ejb:interface-method view-type="local"
    * @ejb:relation name="frgrowset-frgrows"
    *               role-name="frgrowset-has-many-frgrows"
    */
   public abstract Collection getGlFrgRows();
   public abstract void setGlFrgRows(Collection glFrgRows);

   // Business methods
   
   /**
    * @ejb:interface-method view-type="local"
    **/
   public void addGlFrgFinancialReport(LocalGlFrgFinancialReport glFrgFinancialReport) {

      Debug.print("GlFrgRowSetBean addGlFrgFinancialReport");
      
      try {
      	
         Collection glFrgFinancialReports = getGlFrgFinancialReports();
	 		glFrgFinancialReports.add(glFrgFinancialReport);
	 		
      } catch (Exception ex) {
      	
         throw new EJBException(ex.getMessage());
         
      }
   }

   /**
    * @ejb:interface-method view-type="local"
    **/
   public void dropGlFrgFinancialReport(LocalGlFrgFinancialReport glFrgFinancialReport) {

      Debug.print("GlFrgRowSetBean dropGlFrgFinancialReport");
      
      try {
      	
         Collection glFrgFinancialReports = getGlFrgFinancialReports();
	 		glFrgFinancialReports.remove(glFrgFinancialReport);
	 		
      } catch (Exception ex) {
      	
         throw new EJBException(ex.getMessage());
         
      }
   }
   
   
   /**
    * @ejb:interface-method view-type="local"
    **/
   public void addGlFrgRow(LocalGlFrgRow glFrgRow) {

      Debug.print("GlFrgRowSetBean addGlFrgRow");
      
      try {
      	
         Collection glFrgRows = getGlFrgRows();
	 		glFrgRows.add(glFrgRow);
	 		
      } catch (Exception ex) {
      	
         throw new EJBException(ex.getMessage());
         
      }
   }

   /**
    * @ejb:interface-method view-type="local"
    **/
   public void dropGlFrgRow(LocalGlFrgRow glFrgRow) {

      Debug.print("GlFrgRowSetBean dropGlFrgRow");
      
      try {
      	
         Collection glFrgRows = getGlFrgRows();
	 		glFrgRows.remove(glFrgRow);
	 		
      } catch (Exception ex) {
      	
         throw new EJBException(ex.getMessage());
         
      }
   }
   
   // EntityBean methods

   /**
    * @ejb:create-method view-type="local"
    **/
   public java.lang.Integer ejbCreate (Integer RS_CODE, String RS_NM, String RS_DESC,
   		Integer RS_AD_CMPNY) 
      throws CreateException {

      Debug.print("GlFrgRowSetBean ejbCreate");
      setRsCode(RS_CODE);
      setRsName(RS_NM);
      setRsDescription(RS_DESC);
      setRsAdCompany(RS_AD_CMPNY);
      
      return null;
   }
   
   /**
    * @ejb:create-method view-type="local"
    **/
   public java.lang.Integer ejbCreate (String RS_NM, String RS_DESC,
   		Integer RS_AD_CMPNY) 
      throws CreateException {

      Debug.print("GlFrgRowSetBean ejbCreate");
      setRsName(RS_NM);
      setRsDescription(RS_DESC);
      setRsAdCompany(RS_AD_CMPNY);
      
      return null;
   }
   
   public void ejbPostCreate (Integer RS_CODE, String RS_NM, String RS_DESC,
   		Integer RS_AD_CMPNY) 
      throws CreateException { }

   public void ejbPostCreate (String RS_NM, String RS_DESC,
   		Integer RS_AD_CMPNY) 
      throws CreateException { }

} // GlFrgRowSetBean class
