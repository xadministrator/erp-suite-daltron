package com.ejb.gl;

import java.util.Collection;
import java.util.Date;

import javax.ejb.CreateException;
import javax.ejb.EJBException;

import com.ejb.ar.LocalArSalesOrder;
import com.util.AbstractEntityBean;
import com.util.Debug;

/**
 * @ejb:bean name="GlFunctionalCurrencyEJB"
 *           display-name="Functional Currency Entity"
 *           type="CMP"
 *           cmp-version="2.x"
 *           view-type="local"
 *           local-jndi-name="omega-ejb/GlFunctionalCurrencyEJB"
 *           schema="GlFunctionalCurrency"
 *           primkey-field="fcCode"
 *       
 * @ejb:pk class="java.lang.Integer"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="gluser"
 *                        role-link="gluserlink"
 *
 * @ejb:permission role-name="gluser"
 *
 * @ejb:interface local-class="com.ejb.gl.LocalGlFunctionalCurrency"
 *                local-extends="javax.ejb.EJBLocalObject"
 *
 * @ejb:home local-class="com.ejb.gl.LocalGlFunctionalCurrencyHome"
 *           local-extends="javax.ejb.EJBLocalHome"
 *
 * @ejb:finder signature="Collection findFcAllEnabled(java.util.Date CURR_DATE, java.lang.Integer FC_AD_CMPNY)"
 *             query="SELECT OBJECT(fc) FROM GlFunctionalCurrency fc WHERE fc.fcEnable=1 AND ((fc.fcDateFrom <= ?1 AND fc.fcDateTo >= ?1) OR (fc.fcDateFrom <= ?1 AND fc.fcDateTo IS NULL)) AND fc.fcAdCompany = ?2"
 *
 * @jboss:query signature="Collection findFcAllEnabled(java.util.Date CURR_DATE, java.lang.Integer FC_AD_CMPNY)"
 *             query="SELECT OBJECT(fc) FROM GlFunctionalCurrency fc WHERE fc.fcEnable=1 AND ((fc.fcDateFrom <= ?1 AND fc.fcDateTo >= ?1) OR (fc.fcDateFrom <= ?1 AND fc.fcDateTo IS NULL)) AND fc.fcAdCompany = ?2 ORDER BY fc.fcName"
 *
 * @ejb:finder signature="LocalGlFunctionalCurrency findByFcName(java.lang.String FC_NM, java.lang.Integer FC_AD_CMPNY)"
 *             query="SELECT OBJECT(fc) FROM GlFunctionalCurrency fc WHERE fc.fcName=?1 AND fc.fcAdCompany = ?2"
 *
 * @ejb:finder signature="Collection findFcAll(java.lang.Integer FC_AD_CMPNY)"
 *             query="SELECT OBJECT(fc) FROM GlFunctionalCurrency fc WHERE fc.fcAdCompany = ?1"
 *
 * @jboss:query signature="Collection findFcAll(java.lang.Integer FC_AD_CMPNY)"
 *             query="SELECT OBJECT(fc) FROM GlFunctionalCurrency fc WHERE fc.fcAdCompany = ?1 ORDER BY fc.fcName"
 *
 * @jboss:persistence table-name="GL_FNCTNL_CRRNCY"
 *
 * @jboss:entity-command name="mysql-get-generated-keys"
 * 
 */

public abstract class GlFunctionalCurrencyBean extends AbstractEntityBean {
	
	
	// Access methods for persistent fields
	
	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 * @ejb:pk-field
	 *
	 * @jboss:column-name name="FC_CODE"
	 *
	 * @jboss:persistence auto-increment="true"
	 **/
	public abstract java.lang.Integer getFcCode();
	public abstract void setFcCode(java.lang.Integer FC_CODE);
	
	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="FC_NM"
	 **/
	public abstract java.lang.String getFcName();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setFcName(java.lang.String FC_NM);
	
	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="FC_DESC"
	 **/
	public abstract java.lang.String getFcDescription();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setFcDescription(java.lang.String FC_DESC);
	
	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="FC_CNTRY"
	 **/
	public abstract java.lang.String getFcCountry();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setFcCountry(java.lang.String FC_CNTRY);
	
	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="FC_SYMBL"
	 **/
	public abstract char getFcSymbol();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setFcSymbol(char FC_SYMBL);
	
	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="FC_PRCSN"
	 **/
	public abstract short getFcPrecision();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setFcPrecision(short FC_PRCSN);
	
	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="FC_EXTNDD_PRCSN"
	 **/
	public abstract short getFcExtendedPrecision();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setFcExtendedPrecision(short FC_EXTNDD_PRCSN);
	
	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="FC_MIN_ACCNT_UNT"
	 **/
	public abstract double getFcMinimumAccountUnit();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setFcMinimumAccountUnit(double FC_MIN_ACCNT_UNT);
	
	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="FC_DT_FRM"
	 **/
	public abstract Date getFcDateFrom();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setFcDateFrom(Date FC_DT_FRM);
	
	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="FC_DT_TO"
	 **/
	public abstract Date getFcDateTo();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setFcDateTo(Date FC_DT_TO);
	
	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="FC_ENBL"
	 **/
	public abstract byte getFcEnable();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setFcEnable(byte FC_ENBL);
	
	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="FC_AD_CMPNY"
	 **/
	public abstract Integer getFcAdCompany();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setFcAdCompany(Integer FC_AD_CMPNY);
	
	// Access methods for relationship fields
	
	/**
	 * @ejb:interface-method view-type="local"
	 * @ejb:relation name="functionalcurrency-functionalcurrencyrates"
	 *               role-name="functionalcurrency-has-many-functionalcurrencyrates"
	 */
	public abstract Collection getGlFunctionalCurrencyRates();
	public abstract void setGlFunctionalCurrencyRates(Collection glFunctionalCurrencyRates);
	
	/**
	 * @ejb:interface-method view-type="local"
	 * @ejb:relation name="functionalcurrency-journals"
	 *               role-name="functionalcurrency-has-many-journals"
	 */
	public abstract Collection getGlJournals();
	public abstract void setGlJournals(Collection glJournals); 
	
	
	/**
	 * @ejb:interface-method view-type="local"
	 * @ejb:relation name="functionalcurrency-bankaccounts"
	 *               role-name="functionalcurrency-has-many-bankaccounts"
	 */
	public abstract Collection getAdBankAccounts();
	public abstract void setAdBankAccounts(Collection adBankAccounts);
	
	/**
	 * @ejb:interface-method view-type="local"
	 * @ejb:relation name="functionalcurrency-companies"
	 *               role-name="functionalcurrency-has-many-companies"
	 */
	public abstract Collection getAdCompanies();
	public abstract void setAdCompanies(Collection adCompanies);
	
	/**
	 * @ejb:interface-method view-type="local"
	 * @ejb:relation name="functionalcurrency-recurringvouchers"
	 *               role-name="functionalcurrency-has-many-recurringvouchers"
	 * 
	 */
	public abstract Collection getApRecurringVouchers();
	public abstract void setApRecurringVouchers(Collection apRecurringVouchers);
	
	/**
	 * @ejb:interface-method view-type="local"
	 * @ejb:relation name="functionalcurrency-vouchers"
	 *               role-name="functionalcurrency-has-many-vouchers"
	 * 
	 */
	public abstract Collection getApVouchers();
	public abstract void setApVouchers(Collection apVouchers);  
	
	/**
	 * @ejb:interface-method view-type="local"
	 * @ejb:relation name="functionalcurrency-checks"
	 *               role-name="functionalcurrency-has-many-checks"
	 * 
	 */
	public abstract Collection getApChecks();
	public abstract void setApChecks(Collection apChecks);
	
	/**
	 * @ejb:interface-method view-type="local"
	 * @ejb:relation name="functionalcurrency-invoices"
	 *               role-name="functionalcurrency-has-many-invoices"
	 * 
	 */
	public abstract Collection getArInvoices();
	public abstract void setArInvoices(Collection arInvoices);  
	
	/**
	 * @ejb:interface-method view-type="local"
	 * @ejb:relation name="functionalcurrency-receipts"
	 *               role-name="functionalcurrency-has-many-receipts"
	 * 
	 */
	public abstract Collection getArReceipts();
	public abstract void setArReceipts(Collection arReceipts);
	
	/**
	 * @ejb:interface-method view-type="local"
	 * @ejb:relation name="functionalcurrency-purchaseorders"
	 *               role-name="functionalcurrency-has-many-purchaseorders"
	 * 
	 */
	public abstract Collection getApPurchaseOrders();
	public abstract void setApPurchaseOrders(Collection apPurchaseOrders);
	
	/**
	 * @ejb:interface-method view-type="local"
	 * @ejb:relation name="functionalcurrency-pdcs"
	 *               role-name="functionalcurrency-has-many-pdcs"
	 * 
	 */
	public abstract Collection getArPdcs();
	public abstract void setArPdcs(Collection arPdcs);   
	
	/**
	 * @ejb:interface-method view-type="local"
	 * @ejb:relation name="functionalcurrency-salesorder"
	 *               role-name="functionalcurrency-has-many-salesorder"
	 * 
	 */
	public abstract Collection getArSalesOrders();
	public abstract void setArSalesOrders(Collection arSalesOrders);    
	
	/**
	 * @ejb:interface-method view-type="local"
	 * @ejb:relation name="functionalcurrency-purchaserequisitions"
	 *               role-name="functionalcurrency-has-many-purchaserequisitions"
	 * 
	 */
	public abstract Collection getApPurchaseRequisitions();
	public abstract void setApPurchaseRequisitions(Collection apPurchaseRequisitions);
	
	
	/**
	 * @ejb:interface-method view-type="local"
	 * @ejb:relation name="functionalcurrency-joborders"
	 *               role-name="functionalcurrency-has-many-joborders"
	 * 
	 */
	public abstract Collection getArJobOrders();
	public abstract void setArJobOrders(Collection arJobOrders);

	/**
	 * @ejb:interface-method view-type="local"
	 * @ejb:relation name="functionalcurrency-chartofaccounts"
	 *               role-name="functionalcurrency-has-many-chartofaccounts"
	 * 
	 */
	public abstract Collection getGlChartOfAccounts();
	public abstract void setGlChartOfAccounts(Collection GlChartOfAccount);  
	
	/**
	 * @ejb:interface-method view-type="local"
	 * @ejb:relation name="functionalcurrency-frgcolumns"
	 *               role-name="functionalcurrency-has-many-frgcolumns"
	 * 
	 */
	public abstract Collection getGlFrgColumns();
	public abstract void setGlFrgColumns(Collection glFrgColumns);    
	
	// Business methods
	
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public void addGlFunctionalCurrencyRate(LocalGlFunctionalCurrencyRate glFunctionalCurrencyRate) {
		
		Debug.print("GlFunctionalCurrencyBean addGlFunctionalCurrencyRate");
		try {
			Collection glFunctionalCurrencyRates = getGlFunctionalCurrencyRates();
			glFunctionalCurrencyRates.add(glFunctionalCurrencyRate);
		} catch (Exception ex) {
			throw new EJBException(ex.getMessage());
		}
	}
	
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public void dropGlFunctionalCurrencyRate(LocalGlFunctionalCurrencyRate glFunctionalCurrencyRate) {
		
		Debug.print("GlFunctionalCurrencyRateBean dropGlFunctionalCurrencyRate");
		try {
			Collection glFunctionalCurrencyRates = getGlFunctionalCurrencyRates();
			glFunctionalCurrencyRates.remove(glFunctionalCurrencyRate);
		} catch (Exception ex) {
			throw new EJBException(ex.getMessage());
		}
	}
	
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public void addGlJournal(LocalGlJournal glJournal) {
		
		Debug.print("GlFunctionalCurrencyRateBean addGlJournal");
		try {
			Collection glJournals = getGlJournals();
			glJournals.add(glJournal);
		} catch (Exception ex) {
			throw new EJBException(ex.getMessage());
		}
	}
	
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public void dropGlJournal(LocalGlJournal glJournal) {
		
		Debug.print("GlFunctionalCurrencyRateBean dropGlJournal");
		try {
			Collection glJournals = getGlJournals();
			glJournals.remove(glJournal); 
		} catch (Exception ex) {
			throw new EJBException(ex.getMessage());
		}
	}
	
	
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public void addAdBankAccount(com.ejb.ad.LocalAdBankAccount adBankAccount) {
		
		Debug.print("GlFunctionalCurrencyBean addAdBankAccount");
		try {
			Collection adBankAccounts = getAdBankAccounts();
			adBankAccounts.add(adBankAccount);
		} catch (Exception ex) {
			throw new EJBException(ex.getMessage());
		}
	}
	
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public void dropAdBankAccount(com.ejb.ad.LocalAdBankAccount adBankAccount) {
		
		Debug.print("GlFunctionalCurrencyBean dropAdBankAccount");
		try {
			Collection adBankAccounts = getAdBankAccounts();
			adBankAccounts.remove(adBankAccount);
		} catch (Exception ex) {
			throw new EJBException(ex.getMessage());
		}
	}
	
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public void addAdCompany(com.ejb.ad.LocalAdCompany adCompany) {
		
		Debug.print("GlFunctionalCurrencyBean addAdCompany");
		try {
			Collection adCompanies = getAdCompanies();
			adCompanies.add(adCompany);
		} catch (Exception ex) {
			throw new EJBException(ex.getMessage());
		}
	}
	
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public void dropAdCompany(com.ejb.ad.LocalAdCompany adCompany) {
		
		Debug.print("GlFunctionalCurrencyBean dropAdCompany");
		try {
			Collection adCompanies = getAdCompanies();
			adCompanies.remove(adCompany);
		} catch (Exception ex) {
			throw new EJBException(ex.getMessage());
		}
	}
	
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public void addApCheck(com.ejb.ap.LocalApCheck apCheck) {
		
		Debug.print("GlFunctionalCurrencyBean addApCheck");
		
		try {
			
			Collection apChecks = getApChecks();
			apChecks.add(apCheck);
			
		} catch (Exception ex) {
			
			throw new EJBException(ex.getMessage());
			
		}   
	}
	
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public void dropApCheck(com.ejb.ap.LocalApCheck apCheck) {
		
		Debug.print("GlFunctionalCurrencyBean dropApCheck");
		
		try {
			
			Collection apChecks = getApChecks();
			apChecks.remove(apCheck);
			
		} catch (Exception ex) {
			
			throw new EJBException(ex.getMessage());
			
		} 
		
	}
	
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public void addApRecurringVoucher(com.ejb.ap.LocalApRecurringVoucher apRecurringVoucher) {
		
		Debug.print("GlFunctionalCurrencyBean addApRecurringVoucher");
		
		try {
			
			Collection apRecurringVouchers = getApRecurringVouchers();
			apRecurringVouchers.add(apRecurringVoucher);
			
		} catch (Exception ex) {
			
			throw new EJBException(ex.getMessage());
			
		}   
	}
	
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public void dropApRecurringVoucher(com.ejb.ap.LocalApRecurringVoucher apRecurringVoucher) {
		
		Debug.print("GlFunctionalCurrencyBean dropApRecurringVoucher");
		
		try {
			
			Collection apRecurringVouchers = getApRecurringVouchers();
			apRecurringVouchers.remove(apRecurringVoucher);
			
		} catch (Exception ex) {
			
			throw new EJBException(ex.getMessage());
			
		}   
	}     
	
	
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public void addApVoucher(com.ejb.ap.LocalApVoucher apVoucher) {
		
		Debug.print("GlFunctionalCurrencyBean addApVoucher");
		
		try {
			
			Collection apVouchers = getApVouchers();
			apVouchers.add(apVoucher);
			
		} catch (Exception ex) {
			
			throw new EJBException(ex.getMessage());
			
		}   
	}
	
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public void dropApVoucher(com.ejb.ap.LocalApVoucher apVoucher) {
		
		Debug.print("GlFunctionalCurrencyBean dropApVoucher");
		
		try {
			
			Collection apVouchers = getApVouchers();
			apVouchers.remove(apVoucher);
			
		} catch (Exception ex) {
			
			throw new EJBException(ex.getMessage());
			
		}   
	}     
	
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public void addArInvoice(com.ejb.ar.LocalArInvoice arInvoice) {
		
		Debug.print("GlFunctionalCurrencyBean addArInvoice");
		
		try {
			
			Collection arInvoices = getArInvoices();
			arInvoices.add(arInvoice);
			
		} catch (Exception ex) {
			
			throw new EJBException(ex.getMessage());
			
		}   
	}
	
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public void dropArInvoice(com.ejb.ar.LocalArInvoice arInvoice) {
		
		Debug.print("GlFunctionalCurrencyBean dropArInvoice");
		
		try {
			
			Collection arInvoices = getArInvoices();
			arInvoices.remove(arInvoice);
			
		} catch (Exception ex) {
			
			throw new EJBException(ex.getMessage());
			
		}   
	}
	
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public void addArReceipt(com.ejb.ar.LocalArReceipt arReceipt) {
		
		Debug.print("GlFunctionalCurrencyBean addArReceipt");
		
		try {
			Collection arReceipts = getArReceipts();
			arReceipts.add(arReceipt);

		} catch (Exception ex) {
			
			throw new EJBException(ex.getMessage());
			
		}   
	}
	
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public void dropArReceipt(com.ejb.ar.LocalArReceipt arReceipt) {
		
		Debug.print("GlFunctionalCurrencyBean dropArReceipt");
		
		try {
			
			Collection arReceipts = getArReceipts();
			arReceipts.remove(arReceipt);
			
		} catch (Exception ex) {
			
			throw new EJBException(ex.getMessage());
			
		}   
	}
	
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public void addApPurchaseOrder(com.ejb.ap.LocalApPurchaseOrder apPurchaseOrder) {
		
		Debug.print("GlFunctionalCurrencyBean addApPurchaseOrder");
		
		try {
			
			Collection apPurchaseOrders = getApPurchaseOrders();
			apPurchaseOrders.add(apPurchaseOrder);
			
		} catch (Exception ex) {
			
			throw new EJBException(ex.getMessage());
			
		}   
	}
	
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public void dropApPurchaseOrder(com.ejb.ap.LocalApPurchaseOrder apPurchaseOrder) {
		
		Debug.print("GlFunctionalCurrencyBean dropApPurchaseOrder");
		
		try {
			
			Collection apPurchaseOrders = getApPurchaseOrders();
			apPurchaseOrders.remove(apPurchaseOrder);
			
		} catch (Exception ex) {
			
			throw new EJBException(ex.getMessage());
			
		}   
	}
	
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public void addArPdc(com.ejb.ar.LocalArPdc arPdc) {
		
		Debug.print("GlFunctionalCurrencyBean addArPdc");
		
		try {
			
			Collection arPdcs = getArPdcs();
			arPdcs.add(arPdc);
			
		} catch (Exception ex) {
			
			throw new EJBException(ex.getMessage());
			
		}   
	}
	
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public void dropArPdc(com.ejb.ar.LocalArPdc arPdc) {
		
		Debug.print("GlFunctionalCurrencyBean dropArPdc");
		
		try {
			
			Collection arPdcs = getArPdcs();
			arPdcs.remove(arPdc);
			
		} catch (Exception ex) {
			
			throw new EJBException(ex.getMessage());
			
		}   
		
	}   
	
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public void addArSalesOrder(LocalArSalesOrder arSalesOrder) {
		
		Debug.print("GlFunctionalCurrencyBean addArSalesOrder");
		
		try {
			
			Collection arSalesOrders = getArSalesOrders();
			arSalesOrders.add(arSalesOrder);
			
		} catch (Exception ex) {
			
			throw new EJBException(ex.getMessage());
			
		}   
		
	}
	
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public void dropArSalesOrder(LocalArSalesOrder arSalesOrder) {
		
		Debug.print("GlFunctionalCurrencyBean dropArSalesOrder");
		
		try {
			
			Collection arSalesOrders = getArSalesOrders();
			arSalesOrders.remove(arSalesOrder);
			
		} catch (Exception ex) {
			
			throw new EJBException(ex.getMessage());
			
		}
		
		
	}      
	
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public void addApPurchaseRequisition(com.ejb.ap.LocalApPurchaseRequisition apPurchaseRequisition) {
		
		Debug.print("GlFunctionalCurrencyBean addApPurchaseRequisition");
		
		try {
			
			Collection apPurchaseRequisitions = getApPurchaseRequisitions();
			apPurchaseRequisitions.add(apPurchaseRequisition);
			
		} catch (Exception ex) {
			
			throw new EJBException(ex.getMessage());
			
		}   
	}
	
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public void dropApPurchaseRequisition(com.ejb.ap.LocalApPurchaseRequisition apPurchaseRequisition) {
		
		Debug.print("GlFunctionalCurrencyBean dropApPurchaseRequisition");
		
		try {
			
			Collection apPurchaseRequisitions = getApPurchaseRequisitions();
			apPurchaseRequisitions.remove(apPurchaseRequisition);
			
		} catch (Exception ex) {
			
			throw new EJBException(ex.getMessage());
			
		}   
	}

	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public void addGlChartOfAccount(LocalGlChartOfAccount glChartOfAccount) {
		
		Debug.print("GlFunctionalCurrencyBean addArSalesOrder");
		
		try {
			
			Collection glChartOfAccounts = getGlChartOfAccounts();
			glChartOfAccounts.add(glChartOfAccount);
			
		} catch (Exception ex) {
			
			throw new EJBException(ex.getMessage());
			
		}   
		
	}
	
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public void dropGlChartOfAccount(LocalGlChartOfAccount glChartOfAccount) {
		
		Debug.print("GlFunctionalCurrencyBean dropGlChartOfAccount");
		
		try {
			
			Collection glChartOfAccounts = getGlChartOfAccounts();
			glChartOfAccounts.remove(glChartOfAccount);
			
		} catch (Exception ex) {
			
			throw new EJBException(ex.getMessage());
			
		}
		
		
	}	
	
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public void addGlFrgColumn(LocalGlFrgColumn glFrgColumn) {
		
		Debug.print("GlFunctionalCurrencyBean addGlFrgColumn");
		
		try {
			
			Collection glFrgColumns = getGlFrgColumns();
			glFrgColumns.add(glFrgColumn);
			
		} catch (Exception ex) {
			
			throw new EJBException(ex.getMessage());
			
		}   
		
	}
	
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public void dropGlFrgColumn(LocalGlFrgColumn glFrgColumn) {
		
		Debug.print("GlFunctionalCurrencyBean dropGlFrgColumn");
		
		try {
			
			Collection glFrgColumns = getGlFrgColumns();
			glFrgColumns.remove(glFrgColumn);
			
		} catch (Exception ex) {
			
			throw new EJBException(ex.getMessage());
			
		}
		
	}
	
	// EntityBean methods
	
	/**
	 * @ejb:create-method view-type="local"
	 **/
	public java.lang.Integer ejbCreate (java.lang.Integer FC_CODE, java.lang.String FC_NM,
			java.lang.String FC_DESC, java.lang.String FC_CNTRY, char FC_SYMBL, short FC_PRCSN,
			short FC_EXTNDD_PRCSN, double FC_MIN_ACCNT_UNT, Date FC_DT_FRM,
			Date FC_DT_TO, byte FC_ENBL, Integer FC_AD_CMPNY)
	throws CreateException {
		
		Debug.print("GlFunctionalCurrencyBean ejbCreate");
		setFcCode(FC_CODE);
		setFcName(FC_NM);
		setFcDescription(FC_DESC);
		setFcCountry(FC_CNTRY);
		setFcSymbol(FC_SYMBL);
		setFcPrecision(FC_PRCSN);
		setFcExtendedPrecision(FC_EXTNDD_PRCSN);
		setFcMinimumAccountUnit(FC_MIN_ACCNT_UNT);
		setFcDateFrom(FC_DT_FRM);
		setFcDateTo(FC_DT_TO);
		setFcEnable(FC_ENBL);
		setFcAdCompany(FC_AD_CMPNY);
		
		return null;
	}
	
	/**
	 * @ejb:create-method view-type="local"
	 **/
	public java.lang.Integer ejbCreate (java.lang.String FC_NM,
			java.lang.String FC_DESC, java.lang.String FC_CNTRY, char FC_SYMBL, short FC_PRCSN,
			short FC_EXTNDD_PRCSN, double FC_MIN_ACCNT_UNT, Date FC_DT_FRM,
			Date FC_DT_TO, byte FC_ENBL, Integer FC_AD_CMPNY)
	throws CreateException {
		
		Debug.print("GlFunctionalCurrencyBean ejbCreate");
		
		setFcName(FC_NM);
		setFcDescription(FC_DESC);
		setFcCountry(FC_CNTRY);
		setFcSymbol(FC_SYMBL);
		setFcPrecision(FC_PRCSN);
		setFcExtendedPrecision(FC_EXTNDD_PRCSN);
		setFcMinimumAccountUnit(FC_MIN_ACCNT_UNT);
		setFcDateFrom(FC_DT_FRM);
		setFcDateTo(FC_DT_TO);
		setFcEnable(FC_ENBL);
		setFcAdCompany(FC_AD_CMPNY);
		
		return null;
	}
	
	public void ejbPostCreate (java.lang.Integer FC_CODE, java.lang.String FC_NM,
			java.lang.String FC_DESC, java.lang.String FC_CNTRY, char FC_SYMBL, short FC_PRCSN,
			short FC_EXTNDD_PRCSN, double FC_MIN_ACCNT_UNT, Date FC_DT_FRM,
			Date FC_DT_TO, byte FC_ENBL, Integer FC_AD_CMPNY)
	throws CreateException { }
	
	public void ejbPostCreate (java.lang.String FC_NM,
			java.lang.String FC_DESC, java.lang.String FC_CNTRY, char FC_SYMBL, short FC_PRCSN,
			short FC_EXTNDD_PRCSN, double FC_MIN_ACCNT_UNT, Date FC_DT_FRM,
			Date FC_DT_TO, byte FC_ENBL, Integer FC_AD_CMPNY)
	throws CreateException { }
	
} // GlFunctionalCurrencyBean class
