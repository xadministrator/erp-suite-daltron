package com.ejb.gl;

import java.util.Collection;
import java.util.Date;

import javax.ejb.CreateException;
import javax.ejb.FinderException;

import com.util.AbstractEntityBean;
import com.util.Debug;

/**
 * @ejb:bean name="GlFunctionalCurrencyRateEJB"
 *           display-name="Functional Currency Rate Entity"
 *           type="CMP"
 *           cmp-version="2.x"
 *           view-type="local"
 *           local-jndi-name="omega-ejb/GlFunctionalCurrencyRateEJB"
 *           schema="GlFunctionalCurrencyRate"
 *           primkey-field="frCode"
 *       
 * @ejb:pk class="java.lang.Integer"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="gluser"
 *                        role-link="gluserlink"
 *
 * @ejb:permission role-name="gluser"
 *
 * @ejb:interface local-class="com.ejb.gl.LocalGlFunctionalCurrencyRate"
 *                local-extends="javax.ejb.EJBLocalObject"
 *
 * @ejb:home local-class="com.ejb.gl.LocalGlFunctionalCurrencyRateHome"
 *           local-extends="javax.ejb.EJBLocalHome"
 *
 * @ejb:finder signature="Collection findByFrDate(java.util.Date FR_DT, java.lang.Integer FR_AD_CMPNY)"
 *             query="SELECT OBJECT(fr) FROM GlFunctionalCurrencyRate fr WHERE fr.frDate=?1 AND fr.frAdCompany = ?2"
 *
 * @ejb:finder signature="Collection findFcrAll(java.lang.Integer FR_AD_CMPNY)"
 *             query="SELECT OBJECT(fr) FROM GlFunctionalCurrencyRate fr WHERE fr.frAdCompany = ?1"
 *
 * @jboss:query signature="Collection findFcrAll(java.lang.Integer FR_AD_CMPNY)"
 *             query="SELECT OBJECT(fr) FROM GlFunctionalCurrencyRate fr WHERE fr.frAdCompany = ?1 ORDER BY fr.frDate"
 *
 * @ejb:finder signature="LocalGlFunctionalCurrencyRate findByFcCodeAndDate(java.lang.Integer FC_CODE, java.util.Date FR_DT, java.lang.Integer FR_AD_CMPNY)"
 *             query="SELECT OBJECT(fr) FROM GlFunctionalCurrency fc, IN(fc.glFunctionalCurrencyRates) fr WHERE fc.fcCode=?1 AND fr.frDate=?2 AND fr.frAdCompany = ?3"
 *
 * @ejb:finder signature="Collection findPriorByFcCodeAndDate(java.lang.Integer FC_CODE, java.util.Date FR_DT, java.lang.Integer FR_AD_CMPNY)"
 *             query="SELECT OBJECT(fr) FROM GlFunctionalCurrency fc, IN(fc.glFunctionalCurrencyRates) fr WHERE fc.fcCode=?1 AND fr.frDate<?2 AND fr.frAdCompany = ?3"
 *
 * @jboss:query signature="Collection findPriorByFcCodeAndDate(java.lang.Integer FC_CODE, java.util.Date FR_DT, java.lang.Integer FR_AD_CMPNY)"
 *             query="SELECT OBJECT(fr) FROM GlFunctionalCurrency fc, IN(fc.glFunctionalCurrencyRates) fr WHERE fc.fcCode=?1 AND fr.frDate<?2 AND fr.frAdCompany = ?3 ORDER BY fr.frDate DESC"
 * 
 * 
 * @ejb:finder signature="Collection findPriorByFcCode(java.lang.Integer FC_CODE, java.lang.Integer FR_AD_CMPNY)"
 *             query="SELECT OBJECT(fr) FROM GlFunctionalCurrencyRate fr WHERE fr.glFunctionalCurrency.fcCode = ?1 AND fr.frAdCompany = ?2"
 *
 * @jboss:query signature="Collection findPriorByFcCode(java.lang.Integer FC_CODE, java.lang.Integer FR_AD_CMPNY)"
 *             query="SELECT OBJECT(fr) FROM GlFunctionalCurrencyRate fr WHERE fr.glFunctionalCurrency.fcCode = ?1 AND fr.frAdCompany = ?2 ORDER BY fr.frDate DESC LIMIT 1"
 * 
 * 
 * @ejb:select signature="Collection ejbSelectGeneric(java.lang.String jbossQl, java.lang.Object[] args)"
 *             result-type-mapping="Local"
 *             method-intf="LocalHome"
 *             query="SELECT OBJECT(fr) FROM GlFunctionalCurrencyRate fr"
 *
 * @jboss:query signature="Collection ejbSelectGeneric(java.lang.String jbossQl, java.lang.Object[] args)"
 *                         dynamic="true"
 *
 * @jboss:persistence table-name="GL_FC_RT"
 *
 * @jboss:entity-command name="mysql-get-generated-keys"
 *
 */

public abstract class GlFunctionalCurrencyRateBean extends AbstractEntityBean {


   // Access methods for persistent fields

   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    * @ejb:pk-field
    *
    * @jboss:column-name name="FR_CODE"
    *
    * @jboss:persistence auto-increment="true"
    **/
   public abstract java.lang.Integer getFrCode();
   public abstract void setFrCode(java.lang.Integer FR_CODE);

   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="FR_X_TO_USD"
    **/
   public abstract double getFrXToUsd();
   /**
    * @ejb:interface-method view-type="local"
    **/
   public abstract void setFrXToUsd(double FR_X_TO_USD);

   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="FR_DT"
    **/
   public abstract Date getFrDate();
   /**
    * @ejb:interface-method view-type="local"
    **/
   public abstract void setFrDate(Date FR_DT);
   
   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="FR_AD_CMPNY"
    **/
   public abstract Integer getFrAdCompany();
   /**
    * @ejb:interface-method view-type="local"
    **/
   public abstract void setFrAdCompany(Integer FR_AD_CMPNY);

   // Access methods for relationship fields

   /**
    * @ejb:interface-method view-type="local"
    * @ejb:relation name="functionalcurrency-functionalcurrencyrates"
    *               role-name="functionalcurrencyrate-has-one-functionalcurrency"
    *
    * @jboss:relation related-pk-field="fcCode"
    *                 fk-column="GL_FUNCTIONAL_CURRENCY"
    */
   public abstract LocalGlFunctionalCurrency getGlFunctionalCurrency();
   public abstract void setGlFunctionalCurrency(LocalGlFunctionalCurrency glFunctionalCurrency);

   // Business methods
   
   /**
    * @jboss:dynamic-ql
    */
    public abstract Collection ejbSelectGeneric(java.lang.String jbossQl, java.lang.Object[] args)
       throws FinderException;
   
   // Business methods
   
   /**
    * @ejb:home-method view-type="local"
    */
    public Collection ejbHomeGetFrByCriteria(java.lang.String jbossQl, java.lang.Object[] args)
       throws FinderException {
       	
       return ejbSelectGeneric(jbossQl, args);
    }

   /**
    * @ejb:create-method view-type="local"
    **/
   public java.lang.Integer ejbCreate (java.lang.Integer FR_CODE, double FR_X_TO_USD, 
      Date FR_DT, Integer FR_AD_CMPNY)
      throws CreateException {

      Debug.print("GlFunctionalCurrencyRateBean ejbCreate");
      setFrCode(FR_CODE);
      setFrXToUsd(FR_X_TO_USD);
      setFrDate(FR_DT);
      setFrAdCompany(FR_AD_CMPNY);
      
      return null;
   }

   /**
    * @ejb:create-method view-type="local"
    **/
    public java.lang.Integer ejbCreate (double FR_X_TO_USD,
       Date FR_DT, Integer FR_AD_CMPNY)
       throws CreateException {
       
       Debug.print("GlFunctionalCurrencyRateBean ejbCreate");
       System.out.println("ejbCreate: " + FR_X_TO_USD);
       setFrXToUsd(FR_X_TO_USD);
       setFrDate(FR_DT);
       setFrAdCompany(FR_AD_CMPNY);
       
       return null;
    }

   public void ejbPostCreate (java.lang.Integer FR_CODE, double FR_X_TO_USD,
      Date FR_DT, Integer FR_AD_CMPNY)
      throws CreateException { }

   public void ejbPostCreate (double FR_X_TO_USD,
      Date FR_DT, Integer FR_AD_CMPNY)
      throws CreateException { }

} // GlFunctionalCurrencyRateBean class
