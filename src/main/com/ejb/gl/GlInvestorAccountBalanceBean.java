package com.ejb.gl;

import javax.ejb.CreateException;

import com.ejb.ap.LocalApSupplier;
import com.ejb.ar.LocalArCustomer;
import com.util.AbstractEntityBean;
import com.util.Debug;
import java.util.Collection;

/**
 * @ejb:bean name="GlInvestorAccountBalanceEJB"
 *           display-name="Investor Account Balance Entity"
 *           type="CMP"
 *           cmp-version="2.x"
 *           view-type="local"
 *           local-jndi-name="omega-ejb/GlInvestorAccountBalanceEJB"
 *           schema="GlInvestorAccountBalance"
 *           primkey-field="irabCode"
 *       
 * @ejb:pk class="java.lang.Integer"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="gluser"
 *                        role-link="gluserlink"
 *
 * @ejb:permission role-name="gluser"
 *
 * @ejb:interface local-class="com.ejb.gl.LocalGlInvestorAccountBalance"
 *                local-extends="javax.ejb.EJBLocalObject"
 *
 * @ejb:home local-class="com.ejb.gl.LocalGlInvestorAccountBalanceHome"
 *           local-extends="javax.ejb.EJBLocalHome"
 *
 * @ejb:finder signature="LocalGlInvestorAccountBalance findByAcvCodeAndSplCode(java.lang.Integer ACV_CODE, java.lang.Integer SPL_CODE, java.lang.Integer IRAB_AD_CMPNY)"
 *             query="SELECT OBJECT(irab) FROM GlAccountingCalendarValue acv, IN(acv.glInvestorAccountBalances) irab WHERE acv.acvCode=?1 AND irab.apSupplier.splCode=?2 AND irab.irabAdCompany = ?3"
 *  
 * @ejb:finder signature="Collection findBonusAndInterestByAcCodeAndSplCode(java.lang.Integer AC_CODE, java.lang.Integer SPL_CODE, java.lang.Integer IRAB_AD_CMPNY)"
 *             query="SELECT OBJECT(irab) FROM GlInvestorAccountBalance irab WHERE irab.irabInterest = 0 AND irab.irabBonus = 0  AND irab.irabEndingBalance > 0 AND irab.glAccountingCalendarValue.glAccountingCalendar.acCode =?1 AND irab.apSupplier.splCode = ?2 AND irab.irabAdCompany = ?3"
 *
 * @ejb:finder signature="Collection findByAcvCode(java.lang.Integer ACV_CODE, java.lang.Integer IRAB_AD_CMPNY)"
 *             query="SELECT OBJECT(irab) FROM GlAccountingCalendarValue acv, IN(acv.glInvestorAccountBalances) irab WHERE acv.acvCode=?1 AND irab.irabAdCompany = ?2"
 * 
 * @ejb:finder signature="Collection findByAcvCodeBonusAndInterest(java.lang.Integer ACV_CODE, java.lang.Integer IRAB_AD_CMPNY)"
 *             query="SELECT OBJECT(irab) FROM GlAccountingCalendarValue acv, IN(acv.glInvestorAccountBalances) irab WHERE acv.acvCode=?1 AND irab.irabInterest = 0 AND irab.irabBeginningBalance > 0 AND irab.irabAdCompany = ?2"
 *
 * @ejb:finder signature="LocalGlInvestorAccountBalance findByOtEffectiveDateAndSplCode(java.util.Date OT_EFFCTV_DT, java.lang.Integer SPL_CODE, java.lang.Integer IRAB_AD_CMPNY)"
 *             query="SELECT OBJECT(irab) FROM ApSupplier spl, IN(spl.glInvestorAccountBalances) irab WHERE spl.splCode=?2 AND irab.glAccountingCalendarValue.acvDateFrom<=?1 AND irab.glAccountingCalendarValue.acvDateTo>=?1 AND irab.irabAdCompany = ?3"
 *
 * @jboss:query signature="LocalGlInvestorAccountBalance findByOtEffectiveDateAndSplCode(java.util.Date OT_EFFCTV_DT, java.lang.Integer SPL_CODE, java.lang.Integer IRAB_AD_CMPNY)"
 *              query="SELECT OBJECT(irab) FROM ApSupplier spl, IN(spl.glInvestorAccountBalances) irab WHERE spl.splCode=?2 AND irab.glAccountingCalendarValue.acvDateFrom<=?1 AND irab.glAccountingCalendarValue.acvDateTo>=?1 AND irab.irabAdCompany = ?3"
 *
 * @ejb:finder signature="LocalGlInvestorAccountBalance findBySplCodeAndAcvCode(java.lang.Integer SPL_CODE, java.lang.Integer ACV_CODE, java.lang.Integer IRAB_AD_CMPNY)"
 *             query="SELECT OBJECT(irab) FROM ApSupplier spl, IN(spl.glInvestorAccountBalances) irab WHERE spl.splCode=?1 AND irab.glAccountingCalendarValue.acvCode =?2 AND irab.irabAdCompany = ?3"
 *
 * @jboss:persistence table-name="GL_INVTR_BLNC"
 *
 * @jboss:entity-command name="mysql-get-generated-keys"
 *
 */

public abstract class GlInvestorAccountBalanceBean extends AbstractEntityBean {


   // Access methods for persistent fields

   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    * @ejb:pk-field
    *
    * @jboss:column-name name="IRAB_CODE"
    *
    * @jboss:persistence auto-increment="true"
    **/
   public abstract java.lang.Integer getIrabCode();
   public abstract void setIrabCode(java.lang.Integer IRAB_CODE);

   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="IRAB_TTL_DBT"
    **/
   public abstract double getIrabTotalDebit();
   /**
    * @ejb:interface-method view-type="local"
    **/
   public abstract void setIrabTotalDebit(double IRAB_TTL_DBT);
   
   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="IRAB_TTL_CRDT"
    **/
   public abstract double getIrabTotalCredit();
   /**
    * @ejb:interface-method view-type="local"
    **/
   public abstract void setIrabTotalCredit(double IRAB_TTL_CRDT);
	 
   
     /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="IRAB_BNS"
    **/
    public abstract byte getIrabBonus();
    /**
     * @ejb:interface-method view-type="local"
     **/
    public abstract void setIrabBonus(byte IRAB_BNS);
   
   
   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="IRAB_INT"
    **/
    public abstract byte getIrabInterest();
    /**
     * @ejb:interface-method view-type="local"
     **/
    public abstract void setIrabInterest(byte IRAB_INT);

    
   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="IRAB_TTL_BNS"
    **/
   public abstract double getIrabTotalBonus();
   /**
    * @ejb:interface-method view-type="local"
    **/
   public abstract void setIrabTotalBonus(double IRAB_TTL_BNS);
   
   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="IRAB_TTL_INT"
    **/
   public abstract double getIrabTotalInterest();
   /**
    * @ejb:interface-method view-type="local"
    **/
   public abstract void setIrabTotalInterest(double IRAB_TTL_INT);
   
   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="IRAB_MNTHLY_BNS_RT"
    **/
   public abstract double getIrabMonthlyBonusRate();
   /**
    * @ejb:interface-method view-type="local"
    **/
   public abstract void setIrabMonthlyBonusRate(double IRAB_MNTHLY_BNS_RT);
   
   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="IRAB_MNTHLY_INT_RT"
    **/
   public abstract double getIrabMonthlyInterestRate();
   /**
    * @ejb:interface-method view-type="local"
    **/
   public abstract void setIrabMonthlyInterestRate(double IRAB_MNTHLY_INT_RT);
   
   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="IRAB_BEG_BLNC"
    **/
   public abstract double getIrabBeginningBalance();
   /**
    * @ejb:interface-method view-type="local"
    **/
   public abstract void setIrabBeginningBalance(double IRAB_BEG_BLNC);
   
   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="IRAB_END_BLNC"
    **/
   public abstract double getIrabEndingBalance();
   /**
    * @ejb:interface-method view-type="local"
    **/
   public abstract void setIrabEndingBalance(double IRAB_END_BLNC);
   
   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="IRAB_AD_CMPNY"
    **/
   public abstract Integer getIrabAdCompany();
   /**
    * @ejb:interface-method view-type="local"
    **/
   public abstract void setIrabAdCompany(Integer IRAB_AD_CMPNY);

   // Access methods for relationship fields


   /**
	 * @ejb:interface-method view-type="local"
	 * @ejb:relation name="supplier-investoraccountbalances"
	 *               role-name="investoraccountbalance-has-one-supplier"
	 *
	 * @jboss:relation related-pk-field="splCode"
	 *                 fk-column="AP_SUPPLIER"
	 */
	
	public abstract LocalApSupplier getApSupplier();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setApSupplier(LocalApSupplier apSupplier);
	
	
	
	
   /**
    * @ejb:interface-method view-type="local"
    * @ejb:relation name="accountingcalendarvalue-investoraccountbalances"
    *               role-name="investoraccountbalance-has-one-accountingcalendarvalue"
    *
    * @jboss:relation related-pk-field="acvCode"
    *                 fk-column="GL_ACCOUNTING_CALENDAR_VALUE"
    */
   public abstract LocalGlAccountingCalendarValue getGlAccountingCalendarValue();
   /**
    * @ejb:interface-method view-type="local"
    **/
   public abstract void setGlAccountingCalendarValue(LocalGlAccountingCalendarValue glAccountingCalendarValue);

   
   /**
    * @ejb:interface-method view-type="local"
    * @ejb:relation name="investorAccountBalance-receipts"
    *               role-name="investorAccountBalance-has-many-receipts"
    * 
    */
   public abstract Collection getArReceipts();
   public abstract void setArReceipts(Collection arReceipts);
   
   
   
   
   
   // Business methods

   // EntityBean methods

   /**
    * @ejb:create-method view-type="local"
    **/
   public java.lang.Integer ejbCreate (java.lang.Integer IRAB_CODE, 
		   double IRAB_TTL_DBT, double IRAB_TTL_CRDT,
		   byte IRAB_BNS, byte IRAB_INT,
		   double IRAB_TTL_BNS, double IRAB_TTL_INT,
                   double IRAB_MNTHLY_BNS_RT, double IRAB_MNTHLY_INT_RT,
		   double IRAB_BEG_BLNC, double IRAB_END_BLNC, 
		   Integer IRAB_AD_CMPNY)
      throws CreateException {

      Debug.print("GlInvestorAccountBalance ejbCreate");
      setIrabCode(IRAB_CODE);
      setIrabTotalDebit(IRAB_TTL_DBT);
      setIrabTotalCredit(IRAB_TTL_CRDT);
      setIrabBonus(IRAB_BNS);
      setIrabInterest(IRAB_INT);
      setIrabTotalBonus(IRAB_TTL_BNS);
      setIrabTotalInterest(IRAB_TTL_INT);
      setIrabMonthlyBonusRate(IRAB_MNTHLY_BNS_RT);
      setIrabMonthlyInterestRate(IRAB_MNTHLY_INT_RT);
      setIrabBeginningBalance(IRAB_BEG_BLNC);
      setIrabEndingBalance(IRAB_END_BLNC);
      setIrabAdCompany(IRAB_AD_CMPNY);
      
      return null;
   }
   
   /**
    * @ejb:create-method view-type="local"
    **/
   public java.lang.Integer ejbCreate (
		   double IRAB_TTL_DBT, double IRAB_TTL_CRDT,
		   byte IRAB_BNS, byte IRAB_INT,
		   double IRAB_TTL_BNS, double IRAB_TTL_INT, 
                   double IRAB_MNTHLY_BNS_RT, double IRAB_MNTHLY_INT_RT,
		   double IRAB_BEG_BLNC, double IRAB_END_BLNC, 
		   Integer IRAB_AD_CMPNY)
      throws CreateException {

      Debug.print("GlInvestorAccountBalance ejbCreate");
      setIrabTotalDebit(IRAB_TTL_DBT);
      setIrabTotalCredit(IRAB_TTL_CRDT);
      setIrabBonus(IRAB_BNS);
      setIrabInterest(IRAB_INT);
      setIrabTotalBonus(IRAB_TTL_BNS);
      setIrabTotalInterest(IRAB_TTL_INT);
      setIrabMonthlyBonusRate(IRAB_MNTHLY_BNS_RT);
      setIrabMonthlyInterestRate(IRAB_MNTHLY_INT_RT);
      setIrabBeginningBalance(IRAB_BEG_BLNC);
      setIrabEndingBalance(IRAB_END_BLNC);
      setIrabAdCompany(IRAB_AD_CMPNY);
      
      return null;
   }
   
   public void ejbPostCreate (java.lang.Integer IRAB_CODE, 
		   double IRAB_TTL_DBT, double IRAB_TTL_CRDT,
		   byte IRAB_BNS, byte IRAB_INT,
		   double IRAB_TTL_BNS, double IRAB_TTL_INT, 
                   double IRAB_MNTHLY_BNS_RT, double IRAB_MNTHLY_INT_RT,
		   double IRAB_BEG_BLNC, double IRAB_END_BLNC, 
		   Integer IRAB_AD_CMPNY)
      throws CreateException { }

   public void ejbPostCreate (
		   double IRAB_TTL_DBT, double IRAB_TTL_CRDT,
		   byte IRAB_BNS, byte IRAB_INT,
		   double IRAB_TTL_BNS, double IRAB_TTL_INT, 
                   double IRAB_MNTHLY_BNS_RT, double IRAB_MNTHLY_INT_RT,
		   double IRAB_BEG_BLNC, double IRAB_END_BLNC, 
		   Integer IRAB_AD_CMPNY)
      throws CreateException { }

} // GlInvestorAccountBalance class
