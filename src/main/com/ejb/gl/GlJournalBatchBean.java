package com.ejb.gl;

import java.util.Collection;
import java.util.Date;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;

import com.util.AbstractEntityBean;
import com.util.Debug;

/**
 * @ejb:bean name="GlJournalBatchEJB"
 *           display-name="Journal Batch Entity"
 *           type="CMP"
 *           cmp-version="2.x"
 *           view-type="local"
 *           local-jndi-name="omega-ejb/GlJournalBatchEJB"
 *           schema="GlJournalBatch"
 *           primkey-field="jbCode"
 *       
 * @ejb:pk class="Integer"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="gluser"
 *                        role-link="gluserlink"
 *
 * @ejb:permission role-name="gluser"
 *
 * @ejb:interface local-class="com.ejb.gl.LocalGlJournalBatch"
 *                local-extends="javax.ejb.EJBLocalObject"
 *
 * @ejb:home local-class="com.ejb.gl.LocalGlJournalBatchHome"
 *           local-extends="javax.ejb.EJBLocalHome"
 *
 * @ejb:finder signature="LocalGlJournalBatch findByJbName(java.lang.String JB_NM, java.lang.Integer JB_AD_BRNCH, java.lang.Integer JB_AD_CMPNY)"
 *             query="SELECT OBJECT(jb) FROM GlJournalBatch jb WHERE jb.jbName=?1 AND jb.jbAdBranch=?2 AND jb.jbAdCompany = ?3"
 * 
 * @ejb:finder signature="Collection findOpenJbAll(java.lang.Integer JB_AD_BRNCH, java.lang.Integer JB_AD_CMPNY)"
 *             query="SELECT OBJECT(jb) FROM GlJournalBatch jb WHERE jb.jbStatus='OPEN' AND jb.jbAdBranch = ?1 AND jb.jbAdCompany = ?2"
 *
 * @ejb:finder signature="Collection findOpenJbAll(java.lang.Integer JB_AD_BRNCH, java.lang.Integer JB_AD_CMPNY)"
 *             query="SELECT OBJECT(jb) FROM GlJournalBatch jb WHERE jb.jbStatus='OPEN' AND jb.jbAdBranch = ?1 AND jb.jbAdCompany = ?2"
 *
 * @ejb:select signature="Collection ejbSelectGeneric(java.lang.String jbossQl, java.lang.Object[] args)"
 *             result-type-mapping="Local"
 *             method-intf="LocalHome"
 *             query="SELECT OBJECT(jb) FROM GlJournalBatch jb"
 *
 * @jboss:query signature="Collection ejbSelectGeneric(java.lang.String jbossQl, java.lang.Object[] args)"
 *                         dynamic="true"
 *
 * @ejb:value-object match="*"
 *             name="GlJournalBatch"
 *
 * @jboss:persistence table-name="GL_JRNL_BTCH"
 *
 * @jboss:entity-command name="mysql-get-generated-keys"
 *
 */

public abstract class GlJournalBatchBean extends AbstractEntityBean {


   // Access methods for persistent fields

   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    * @ejb:pk-field
    *
    * @jboss:column-name name="JB_CODE"
    *
    * @jboss:persistence auto-increment="true"
    **/
   public abstract Integer getJbCode();
   public abstract void setJbCode(Integer JB_CODE);

   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="JB_NM"
    **/
   public abstract String getJbName();
   /**
    * @ejb:interface-method view-type="local"
    **/
   public abstract void setJbName(String JB_NM);

   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="JB_DESC"
    **/
   public abstract String getJbDescription();
   /**
    * @ejb:interface-method view-type="local"
    **/
   public abstract void setJbDescription(String JB_DESC);
   
   
   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="JB_STATUS"
    **/
   public abstract String getJbStatus();
   /**
    * @ejb:interface-method view-type="local"
    **/
   public abstract void setJbStatus(String JB_STATUS);

   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="JB_DT_CRTD"
    **/
   public abstract Date getJbDateCreated();
   /**
    * @ejb:interface-method view-type="local"
    **/
   public abstract void setJbDateCreated(Date JB_DT_CRTD);
   
   
   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="JB_CRTD_BY"
    **/
   public abstract String getJbCreatedBy();
   /**
    * @ejb:interface-method view-type="local"
    **/
   public abstract void setJbCreatedBy(String JB_CRTD_BY);

   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="JB_AD_BRNCH"
    **/
   public abstract Integer getJbAdBranch();
   /**
    * @ejb:interface-method view-type="local"
    **/
   public abstract void setJbAdBranch(Integer JB_AD_BRNCH);

   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="JB_AD_CMPNY"
    **/
   public abstract Integer getJbAdCompany();
   /**
    * @ejb:interface-method view-type="local"
    **/
   public abstract void setJbAdCompany(Integer JB_AD_CMPNY);
   
   
   
   // Access methods for relationship fields

   /**
    * @ejb:interface-method view-type="local"
    * @ejb:relation name="journalbatch-journals"
    *               role-name="journalbatch-has-many-journals"
    */
   public abstract Collection getGlJournals();
   public abstract void setGlJournals(Collection glJournals);
   
   /**
    * @ejb:interface-method view-type="local"
    * @ejb:relation name="journalbatch-recurringjournal"
    *               role-name="journalbatch-has-many-recurringjournal"
    */
   public abstract Collection getGlRecurringJournals();
   public abstract void setGlRecurringJournals(Collection glRecurringJournals);
   
  
  /**
    * @jboss:dynamic-ql
    */
    public abstract Collection ejbSelectGeneric(java.lang.String jbossQl, java.lang.Object[] args)
       throws FinderException;

   // Business methods
   
   /**
    * @ejb:home-method view-type="local"
    */
    public Collection ejbHomeGetJbByCriteria(java.lang.String jbossQl, java.lang.Object[] args)
       throws FinderException {
       	
       return ejbSelectGeneric(jbossQl, args);
    }
      
   
   /**
    * @ejb:interface-method view-type="local"
    **/
   public void addGlJournal(LocalGlJournal glJournal) {

      Debug.print("GlJournalBatchBean addGlJournal");
      try {
         Collection glJournals = getGlJournals();
	 glJournals.add(glJournal);
      } catch (Exception ex) {
         throw new EJBException(ex.getMessage());
      }
   }

   /**
    * @ejb:interface-method view-type="local"
    **/
   public void dropGlJournal(LocalGlJournal glJournal) {

      Debug.print("GlJournalBatchBean dropGlJournal");
      try {
         Collection glJournals = getGlJournals();
	 glJournals.remove(glJournal);
      } catch (Exception ex) {
         throw new EJBException(ex.getMessage());
      }
   }
   
   /**
    * @ejb:interface-method view-type="local"
    **/
   public void addGlRecurringJournal(LocalGlRecurringJournal glRecurringJournal) {

      Debug.print("GlJournalBatchBean addGlRecurringJournal");
      try {
         Collection glRecurringJournals = getGlRecurringJournals();
	 glRecurringJournals.add(glRecurringJournal);
      } catch (Exception ex) {
         throw new EJBException(ex.getMessage());
      }
   }

   /**
    * @ejb:interface-method view-type="local"
    **/
   public void dropGlRecurringJournal(LocalGlRecurringJournal glRecurringJournal) {

      Debug.print("GlJournalBatchBean dropGlRecurringJournal");
      try {
         Collection glRecurringJournals = getGlRecurringJournals();
	 glRecurringJournals.remove(glRecurringJournal);
      } catch (Exception ex) {
         throw new EJBException(ex.getMessage());
      }
   }
   
   // EntityBean methods

   /**
    * @ejb:create-method view-type="local"
    **/
   public Integer ejbCreate (Integer JB_CODE, String JB_NM, String JB_DESC,
      String JB_STATUS, Date JB_DT_CRTD, String JB_CRTD_BY, 
	  Integer JB_AD_BRNCH, Integer JB_AD_CMPNY)
      throws CreateException {

      Debug.print("GlJournalBatchBean ejbCreate");
      setJbCode(JB_CODE);
      setJbName(JB_NM);
      setJbDescription(JB_DESC);
      setJbStatus(JB_STATUS);
      setJbDateCreated(JB_DT_CRTD);
      setJbCreatedBy(JB_CRTD_BY);
      setJbAdBranch(JB_AD_BRNCH);
      setJbAdCompany(JB_AD_CMPNY);
      
      return null;
   }

   /**
    * @ejb:create-method view-type="local"
    **/
   public Integer ejbCreate (String JB_NM, String JB_DESC,
      String JB_STATUS, Date JB_DT_CRTD, String JB_CRTD_BY, 
	  Integer JB_AD_BRNCH, Integer JB_AD_CMPNY)
      throws CreateException {

      Debug.print("GlJournalBatchBean ejbCreate");

      setJbName(JB_NM);
      setJbDescription(JB_DESC);
      setJbStatus(JB_STATUS);
      setJbDateCreated(JB_DT_CRTD);
      setJbCreatedBy(JB_CRTD_BY);
      setJbAdBranch(JB_AD_BRNCH);
      setJbAdCompany(JB_AD_CMPNY);
      
      return null;
   }

   public void ejbPostCreate (Integer JB_CODE, String JB_NM, String JB_DESC,
      String JB_STATUS, Date JB_DT_CRTD, String JB_CRTD_BY, 
	  Integer JB_AD_BRNCH, Integer JB_AD_CMPNY)
      throws CreateException { }

   public void ejbPostCreate (String JB_NM, String JB_DESC,
      String JB_STATUS, Date JB_DT_CRTD, String JB_CRTD_BY, 
	  Integer JB_AD_BRNCH, Integer JB_AD_CMPNY)
      throws CreateException { }

} // GlJournalBatchBean class
