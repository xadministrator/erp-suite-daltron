package com.ejb.gl;
import java.util.Collection;
import java.util.Date;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;


import com.util.AbstractEntityBean;
import com.util.Debug;



/**
 * @ejb:bean name="GlJournalEJB"
 *           display-name="Journal Entity"
 *           type="CMP"
 *           view-type="local"
 *           cmp-version="2.x"
 *           local-jndi-name="omega-ejb/GlJournalEJB"
 *           schema="GlJournal"
 *           primkey-field="jrCode"
 *       
 * @ejb:pk class="java.lang.Integer"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="gluser"
 *                        role-link="gluserlink"
 *
 * @ejb:permission role-name="gluser"
 *
 * @ejb:interface local-class="com.ejb.gl.LocalGlJournal"
 *                local-extends="javax.ejb.EJBLocalObject"
 *
 * @ejb:home local-class="com.ejb.gl.LocalGlJournalHome"
 *           local-extends="javax.ejb.EJBLocalHome"
 *
 * @ejb:finder signature="LocalGlJournal findByJrName(java.lang.String JR_NM, java.lang.Integer JR_AD_CMPNY)"
 *             query="SELECT OBJECT(jr) FROM GlJournal jr WHERE jr.jrName=?1 AND jr.jrAdCompany = ?2"
 *             
 * @ejb:finder signature="LocalGlJournal findByJrNameAndBrCode(java.lang.String JR_NM, java.lang.Integer JR_AD_BRNCH, java.lang.Integer JR_AD_CMPNY)"
 *             query="SELECT OBJECT(jr) FROM GlJournal jr WHERE jr.jrName=?1 AND jr.jrAdBranch = ?2 AND jr.jrAdCompany = ?3"
 *
 * @ejb:finder signature="LocalGlJournal findByJrReferenceNumber(java.lang.String JR_NM, java.lang.Integer JR_AD_CMPNY)"
 *             query="SELECT OBJECT(jr) FROM GlJournal jr WHERE jr.jrReferenceNumber=?1 AND jr.jrAdCompany = ?2"
 *             
 * @ejb:finder signature="LocalGlJournal findByJrReferenceNumberAndBrCode(java.lang.String JR_NM, java.lang.Integer JR_AD_BRNCH, java.lang.Integer JR_AD_CMPNY)"
 *             query="SELECT OBJECT(jr) FROM GlJournal jr WHERE jr.jrReferenceNumber=?1 AND jr.jrAdBranch = ?2 AND jr.jrAdCompany = ?3"
 *
 * @ejb:finder signature="LocalGlJournal findByJrDocumentNumberAndJsNameAndBrCode(java.lang.String JR_DCMNT_NMBR, java.lang.String JS_NM, java.lang.Integer JR_AD_BRNCH, java.lang.Integer JR_AD_CMPNY)"
 *             query="SELECT OBJECT(jr) FROM GlJournal jr WHERE jr.jrDocumentNumber=?1 AND jr.glJournalSource.jsName=?2 AND jr.jrAdBranch = ?3 AND jr.jrAdCompany = ?4"
 *
 * @ejb:finder signature="Collection findReversibleJrByJrReversalDateAndBrCode(java.util.Date CRRNT_DT, java.lang.Integer JR_AD_BRNCH, java.lang.Integer JR_AD_CMPNY)"
 *             query="SELECT OBJECT(jr) FROM GlJournal jr WHERE jr.jrReversed=0 AND jr.jrPosted=1 AND jr.jrDateReversal=?1 AND jr.jrAdBranch = ?2 AND jr.jrAdCompany = ?3"
 *
 * @ejb:finder signature="Collection findByJrPostedAndJbName(byte JR_PSTD, java.lang.String JB_NM, java.lang.Integer JR_AD_CMPNY)"
 *             query="SELECT OBJECT(jr) FROM GlJournal jr WHERE jr.jrPosted=?1 AND jr.glJournalBatch.jbName = ?2 AND jr.jrAdCompany = ?3"
 * 
 * @ejb:finder signature="Collection findByJbName(java.lang.String JB_NM, java.lang.Integer JR_AD_CMPNY)"
 *             query="SELECT OBJECT(jr) FROM GlJournal jr WHERE jr.glJournalBatch.jbName=?1 AND jr.jrAdCompany = ?2"
 * 
 * @jboss:query signature="Collection findByJbName(java.lang.String JB_NM, java.lang.Integer JR_AD_CMPNY)"
 *             query="SELECT OBJECT(jr) FROM GlJournal jr WHERE jr.glJournalBatch.jbName=?1 AND jr.jrAdCompany = ?2 ORDER BY jr.jrDocumentNumber, jr.jrEffectiveDate"
 * 
 * @ejb:finder signature="Collection findDraftJrAll(java.lang.Integer JR_AD_CMPNY)"
 *             query="SELECT OBJECT(jr) FROM GlJournal jr WHERE jr.jrApprovalStatus IS NULL AND jr.jrAdCompany = ?1"
 * 
 * @ejb:finder signature="Collection findDraftJrByBrCode(java.lang.Integer JR_AD_BRNCH, java.lang.Integer JR_AD_CMPNY)"
 *             query="SELECT OBJECT(jr) FROM GlJournal jr WHERE jr.jrApprovalStatus IS NULL AND jr.jrAdBranch = ?1 AND jr.jrAdCompany = ?2"
 *
 * @ejb:finder signature="Collection findUnpostedJrByJrDateRange(java.util.Date JR_EFFCTV_DT_FRM, java.util.Date JR_EFFCTV_DT_TO, java.lang.Integer JR_AD_CMPNY)"
 *             query="SELECT OBJECT(jr) FROM GlJournal jr  WHERE jr.jrPosted = 0 AND jr.jrEffectiveDate >= ?1 AND jr.jrEffectiveDate <= ?2 AND jr.jrAdCompany = ?3"
 *
 * @jboss:query signature="Collection findUnpostedJrByJrDateRange(java.util.Date JR_EFFCTV_DT_FRM, java.util.Date JR_EFFCTV_DT_TO, java.lang.Integer JR_AD_CMPNY)"
 *             query="SELECT OBJECT(jr) FROM GlJournal jr  WHERE jr.jrPosted = 0 AND jr.jrEffectiveDate >= ?1 AND jr.jrEffectiveDate <= ?2 AND jr.jrAdCompany = ?3 ORDER BY jr.jrEffectiveDate"
 *
 *
 * @ejb:select signature="Collection ejbSelectGeneric(java.lang.String jbossQl, java.lang.Object[] args)"
 *             result-type-mapping="Local"
 *             method-intf="LocalHome"
 *             query="SELECT OBJECT(jr) FROM GlJournal jr"
 *
 * @jboss:query signature="Collection ejbSelectGeneric(java.lang.String jbossQl, java.lang.Object[] args)"
 *                         dynamic="true"
 *
 * @ejb:value-object match="*"
 *             name="GlJournal"
 *
 * @jboss:persistence table-name="GL_JRNL"
 *
 * @jboss:entity-command name="mysql-get-generated-keys"
 *
 */


public abstract class GlJournalBean extends AbstractEntityBean {


   // Access methods for persistent fields

   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    * @ejb:pk-field
    *
    * @jboss:column-name name="JR_CODE"
    *
    * @jboss:persistence auto-increment="true"
    **/
   public abstract java.lang.Integer getJrCode();
   public abstract void setJrCode(java.lang.Integer JR_CODE);

   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="JR_NM"
    **/
   public abstract java.lang.String getJrName();
   /**
    * @ejb:interface-method view-type="local"
    **/
   public abstract void setJrName(java.lang.String JR_NM);

   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="JR_DESC"
    **/
   public abstract java.lang.String getJrDescription();
   /**
    * @ejb:interface-method view-type="local"
    **/
   public abstract void setJrDescription(java.lang.String JR_DESC);

   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="JR_EFFCTV_DT"
    **/
   public abstract Date getJrEffectiveDate();
   /**
    * @ejb:interface-method view-type="local"
    **/
   public abstract void setJrEffectiveDate(Date JR_EFFCTV_DT);
   
   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="JR_CNTRL_TTL"
    **/
   public abstract double getJrControlTotal();
   /**
    * @ejb:interface-method view-type="local"
    **/
   public abstract void setJrControlTotal(double JR_CNTRL_TTL);

   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="JR_DT_RVRSL"
    **/
   public abstract Date getJrDateReversal();
   /**
    * @ejb:interface-method view-type="local"
    **/
   public abstract void setJrDateReversal(Date JR_DT_RVRSL);

   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="JR_DCMNT_NMBR"
    **/
   public abstract String getJrDocumentNumber();
   /**
    * @ejb:interface-method view-type="local"
    **/
   public abstract void setJrDocumentNumber(String JR_DCMNT_NMBR);

   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="JR_CNVRSN_DT"
    **/
   public abstract Date getJrConversionDate();
   /**
    * @ejb:interface-method view-type="local"
    **/
   public abstract void setJrConversionDate(Date JR_CNVRSN_DT);

   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="JR_CNVRSN_RT"
    **/
   public abstract double getJrConversionRate();
   /**
    * @ejb:interface-method view-type="local"
    **/
   public abstract void setJrConversionRate(double JR_CNVRSN_RT);
   
   
   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="JR_APPRVL_STATUS"
    **/
   public abstract String getJrApprovalStatus();
   /**
    * @ejb:interface-method view-type="local"
    **/
   public abstract void setJrApprovalStatus(String JR_APPRVL_STATUS);
   
   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="JR_RSN_FR_RJCTN"
    **/
   public abstract String getJrReasonForRejection();
   /**
    * @ejb:interface-method view-type="local"
    **/
   public abstract void setJrReasonForRejection(String JR_RSN_FR_RJCTN);

   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="JR_FND_STATUS"
    **/
   public abstract char getJrFundStatus();
   /**
    * @ejb:interface-method view-type="local"
    **/   
   public abstract void setJrFundStatus(char JR_FND_STATUS);
   
   
   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="JR_PSTD"
    **/
   public abstract byte getJrPosted();
   /**
    * @ejb:interface-method view-type="local"
    **/
   public abstract void setJrPosted(byte JR_PSTD);
   
   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="JR_RVRSD"
    **/
   public abstract byte getJrReversed();
   /**
    * @ejb:interface-method view-type="local"
    **/
   public abstract void setJrReversed(byte JR_RVRSD);
         
   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="JR_CRTD_BY"
    **/
   public abstract String getJrCreatedBy();
   /**
    * @ejb:interface-method view-type="local"
    **/
   public abstract void setJrCreatedBy(String JR_CRTD_BY);
   
   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="JR_DT_CRTD"
    **/
   public abstract Date getJrDateCreated();
   /**
    * @ejb:interface-method view-type="local"
    **/
   public abstract void setJrDateCreated(Date JR_DT_CRTD);
   
   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="JR_LST_MDFD_BY"
    **/
   public abstract String getJrLastModifiedBy();
   /**
    * @ejb:interface-method view-type="local"
    **/
   public abstract void setJrLastModifiedBy(String JR_LST_MDFD_BY);
   
   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="JR_DT_LST_MDFD"
    **/
   public abstract Date getJrDateLastModified();
   /**
    * @ejb:interface-method view-type="local"
    **/
   public abstract void setJrDateLastModified(Date JR_DT_LST_MDFD);
   
   
   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="JR_APPRVD_RJCTD_BY"
    **/
   public abstract String getJrApprovedRejectedBy();
   /**
    * @ejb:interface-method view-type="local"
    **/
   public abstract void setJrApprovedRejectedBy(String JR_APPRVD_RJCTD_BY);
   
      
   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="JR_DT_APPRVD_RJCTD"
    **/
   public abstract Date getJrDateApprovedRejected();
   /**
    * @ejb:interface-method view-type="local"
    **/
   public abstract void setJrDateApprovedRejected(Date JR_DT_APPRVD_RJCTD);
   
   
   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="JR_PSTD_BY"
    **/
   public abstract String getJrPostedBy();
   /**
    * @ejb:interface-method view-type="local"
    **/
   public abstract void setJrPostedBy(String JR_PSTD_BY);
   
   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="JR_DT_PSTD"
    **/
   public abstract Date getJrDatePosted();
   /**
    * @ejb:interface-method view-type="local"
    **/
   public abstract void setJrDatePosted(Date JR_DT_PSTD);
   
   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="JR_TIN"
    **/
   public abstract String getJrTin();
   /**
    * @ejb:interface-method view-type="local"
    **/
   public abstract void setJrTin(String JR_TIN);
   
   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="JR_SUB_LDGR"
    **/
   public abstract String getJrSubLedger();
   /**
    * @ejb:interface-method view-type="local"
    **/
   public abstract void setJrSubLedger(String JR_SUB_LDGR);
   
   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="JR_PRNTD"
    **/
   public abstract byte getJrPrinted();
   /**
    * @ejb:interface-method view-type="local"
    **/
   public abstract void setJrPrinted(byte JR_PRNTD);
   
   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="JR_RFRNC_NMBR"
    **/
   public abstract String getJrReferenceNumber();
   /**
    * @ejb:interface-method view-type="local"
    **/
   public abstract void setJrReferenceNumber(String JR_RFRNC_NMBR);
   
   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="JR_AD_BRNCH"
    **/
   public abstract Integer getJrAdBranch();
   /**
    * @ejb:interface-method view-type="local"
    **/
   public abstract void setJrAdBranch(Integer JR_AD_BRNCH);

   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="JR_AD_CMPNY"
    **/
   public abstract Integer getJrAdCompany();
   /**
    * @ejb:interface-method view-type="local"
    **/
   public abstract void setJrAdCompany(Integer JR_AD_CMPNY);
   

   // Access methods for relationship fields

   /**
    * @ejb:interface-method view-type="local"
    * @ejb:relation name="journal-journallines"
    *               role-name="journal-has-many-journallines"
    */
   public abstract Collection getGlJournalLines();
   public abstract void setGlJournalLines(Collection glJournalLines);
      
   /**
    * @ejb:interface-method view-type="local"
    * @ejb:relation name="documentsequenceassignment-journals"
    *               role-name="journal-has-one-documentsequenceassignment"
    *
    * @jboss:relation related-pk-field="dsaCode"
    *                 fk-column="AD_DOCUMENT_SEQUENCE_ASSIGNMENT"
    */
   public abstract LocalAdDocumentSequenceAssignment getAdDocumentSequenceAssignment();
   public abstract void setAdDocumentSequenceAssignment(LocalAdDocumentSequenceAssignment adDocumentSequenceAssignment);

   /**
    * @ejb:interface-method view-type="local"
    * @ejb:relation name="journalcategory-journals"
    *               role-name="journal-has-one-journalcategory"
    *
    * @jboss:relation related-pk-field="jcCode"
    *                 fk-column="GL_JOURNAL_CATEGORY"
    */
   public abstract LocalGlJournalCategory getGlJournalCategory();
   /**
	 * @ejb:interface-method view-type="local"
	 **/
   public abstract void setGlJournalCategory(LocalGlJournalCategory glJournalCategory);
   
   /**
    * @ejb:interface-method view-type="local"
    * @ejb:relation name="journalsource-journals"
    *               role-name="journal-has-one-journalsource"
    *
    * @jboss:relation related-pk-field="jsCode"
    *                 fk-column="GL_JOURNAL_SOURCE"
    */
   public abstract LocalGlJournalSource getGlJournalSource();
   /**
	 * @ejb:interface-method view-type="local"
	 **/
   public abstract void setGlJournalSource(LocalGlJournalSource glJournalSource);
   
   /**
    * @ejb:interface-method view-type="local"
    * @ejb:relation name="functionalcurrency-journals"
    *               role-name="journal-has-one-functionalcurrency"
    *
    * @jboss:relation related-pk-field="fcCode"
    *                 fk-column="GL_FUNCTIONAL_CURRENCY"
    */
   public abstract LocalGlFunctionalCurrency getGlFunctionalCurrency();
   /**
	 * @ejb:interface-method view-type="local"
	 **/
   public abstract void setGlFunctionalCurrency(LocalGlFunctionalCurrency glFunctionalCurrency);
   
   
  /**
    * @ejb:interface-method view-type="local"
    * @ejb:relation name="journalbatch-journals"
    *               role-name="journal-has-one-journalbatch"
    *
    * @jboss:relation related-pk-field="jbCode"
    *                 fk-column="GL_JOURNAL_BATCH"
    */
   public abstract LocalGlJournalBatch getGlJournalBatch();
   /**
	 * @ejb:interface-method view-type="local"
	 **/
   public abstract void setGlJournalBatch(LocalGlJournalBatch glJournalBatch);
   
   /**
    * @jboss:dynamic-ql
    */
    public abstract Collection ejbSelectGeneric(java.lang.String jbossQl, java.lang.Object[] args)
       throws FinderException;

   // Business methods
   
   /**
    * @ejb:home-method view-type="local"
    */
    public Collection ejbHomeGetJrByCriteria(java.lang.String jbossQl, java.lang.Object[] args)
       throws FinderException {
       	
       return ejbSelectGeneric(jbossQl, args);
    }

   /**
    * @ejb:interface-method view-type="local"
    **/
   public void addGlJournalLine(LocalGlJournalLine glJournalLine) {

      Debug.print("GlJournalBean addGlJournalLine");
      try {
         Collection glJournalLines = getGlJournalLines();
	 glJournalLines.add(glJournalLine);
      } catch (Exception ex) {
         throw new EJBException(ex.getMessage());
      }
   }

   /**
    * @ejb:interface-method view-type="local"
    **/
   public void dropGlJournalLine(LocalGlJournalLine glJournalLine) {

      Debug.print("GlJournalBean dropGlJournalLine");
      try {
         Collection glJournalLines = getGlJournalLines();
	 glJournalLines.remove(glJournalLine);
      } catch (Exception ex) {
         throw new EJBException(ex.getMessage());
      }
   }
   
   // EntityBean methods

   /**
    * @ejb:create-method view-type="local"
    **/
   public java.lang.Integer ejbCreate (java.lang.Integer JR_CODE, java.lang.String JR_NM, 
      java.lang.String JR_DESC, Date JR_EFFCTV_DT, double JR_CNTRL_TTL,
      Date JR_DT_RVRSL, String JR_DCMNT_NMBR,
      Date JR_CNVRSN_DT, double JR_CNVRSN_RT, String JR_APPRVL_STATUS, String JR_RSN_FR_RJCTN,
      char JR_FND_STATUS, byte JR_PSTD, byte JR_RVRSD, String JR_CRTD_BY, Date JR_DT_CRTD,
      String JR_LST_MDFD_BY, Date JR_DT_LST_MDFD,
      String JR_APPRVD_RJCTD_BY, Date JR_DT_APPRVD_RJCTD,
      String JR_PSTD_BY, Date JR_DT_PSTD, String JR_TIN, String JR_SUB_LDGR, byte JR_PRNTD,
	  String JR_RFRNC_NMBR, 
	  Integer JR_AD_BRNCH, Integer JR_AD_CMPNY)
      throws CreateException {

      Debug.print("GlJournalBean ejbCreate");
      setJrCode(JR_CODE);
      setJrName(JR_NM);
      setJrDescription(JR_DESC);
      setJrEffectiveDate(JR_EFFCTV_DT);
      setJrControlTotal(JR_CNTRL_TTL);      
      setJrDateReversal(JR_DT_RVRSL);
      setJrDocumentNumber(JR_DCMNT_NMBR);
      setJrConversionDate(JR_CNVRSN_DT);
      setJrConversionRate(JR_CNVRSN_RT);
      setJrApprovalStatus(JR_APPRVL_STATUS);
      setJrReasonForRejection(JR_RSN_FR_RJCTN);
      setJrFundStatus(JR_FND_STATUS);
      setJrPosted(JR_PSTD);
      setJrReversed(JR_RVRSD);
      setJrCreatedBy(JR_CRTD_BY);
      setJrDateCreated(JR_DT_CRTD);
      setJrLastModifiedBy(JR_LST_MDFD_BY);
      setJrDateLastModified(JR_DT_LST_MDFD);
      setJrApprovedRejectedBy(JR_APPRVD_RJCTD_BY);
      setJrDateApprovedRejected(JR_DT_APPRVD_RJCTD);
      setJrPostedBy(JR_PSTD_BY);
      setJrDatePosted(JR_DT_PSTD);
      setJrTin(JR_TIN);
      setJrSubLedger(JR_SUB_LDGR);
      setJrPrinted(JR_PRNTD);
      setJrReferenceNumber(JR_RFRNC_NMBR);
      setJrAdBranch(JR_AD_BRNCH);
      setJrAdCompany(JR_AD_CMPNY);
      
      return null;
   }

   /**
    * @ejb:create-method view-type="local"
    **/
   public java.lang.Integer ejbCreate (java.lang.String JR_NM, 
      java.lang.String JR_DESC, Date JR_EFFCTV_DT, double JR_CNTRL_TTL,
      Date JR_DT_RVRSL, String JR_DCMNT_NMBR,
      Date JR_CNVRSN_DT, double JR_CNVRSN_RT, String JR_APPRVL_STATUS, String JR_RSN_FR_RJCTN,
      char JR_FND_STATUS, byte JR_PSTD, byte JR_RVRSD, String JR_CRTD_BY, Date JR_DT_CRTD,
      String JR_LST_MDFD_BY, Date JR_DT_LST_MDFD,
      String JR_APPRVD_RJCTD_BY, Date JR_DT_APPRVD_RJCTD,
      String JR_PSTD_BY, Date JR_DT_PSTD, String JR_TIN, String JR_SUB_LDGR, byte JR_PRNTD,
	  String JR_RFRNC_NMBR,
	  Integer JR_AD_BRNCH, Integer JR_AD_CMPNY)
      throws CreateException {

      Debug.print("GlJournalBean ejbCreate");

      setJrName(JR_NM);
      setJrDescription(JR_DESC);
      setJrEffectiveDate(JR_EFFCTV_DT);
      setJrControlTotal(JR_CNTRL_TTL);      
      setJrDateReversal(JR_DT_RVRSL);
      setJrDocumentNumber(JR_DCMNT_NMBR);
      setJrConversionDate(JR_CNVRSN_DT);
      setJrConversionRate(JR_CNVRSN_RT);
      setJrApprovalStatus(JR_APPRVL_STATUS);
      setJrReasonForRejection(JR_RSN_FR_RJCTN);
      setJrFundStatus(JR_FND_STATUS);
      setJrPosted(JR_PSTD);
      setJrReversed(JR_RVRSD);
      setJrCreatedBy(JR_CRTD_BY);
      setJrDateCreated(JR_DT_CRTD);
      setJrLastModifiedBy(JR_LST_MDFD_BY);
      setJrDateLastModified(JR_DT_LST_MDFD);
      setJrApprovedRejectedBy(JR_APPRVD_RJCTD_BY);
      setJrDateApprovedRejected(JR_DT_APPRVD_RJCTD);
      setJrPostedBy(JR_PSTD_BY);
      setJrDatePosted(JR_DT_PSTD);
      setJrTin(JR_TIN);
      setJrSubLedger(JR_SUB_LDGR);
      setJrPrinted(JR_PRNTD);
      setJrReferenceNumber(JR_RFRNC_NMBR);
      setJrAdBranch(JR_AD_BRNCH);
      setJrAdCompany(JR_AD_CMPNY);
      
      return null;
   }

   public void ejbPostCreate (java.lang.Integer JR_CODE, java.lang.String JR_NM, 
      java.lang.String JR_DESC, Date JR_EFFCTV_DT, double JR_CNTRL_TTL,
      Date JR_DT_RVRSL, String JR_DCMNT_NMBR,
      Date JR_CNVRSN_DT, double JR_CNVRSN_RT, String JR_APPRVL_STATUS, String JR_RSN_FR_RJCTN,
      char JR_FND_STATUS, byte JR_PSTD, byte JR_RVRSD, String JR_CRTD_BY, Date JR_DT_CRTD,
      String JR_LST_MDFD_BY, Date JR_DT_LST_MDFD,
      String JR_APPRVD_RJCTD_BY, Date JR_DT_APPRVD_RJCTD,
      String JR_PSTD_BY, Date JR_DT_PSTD, String JR_TIN, String JR_SUB_LDGR, byte JR_PRNTD,
	  String JR_RFRNC_NMBR, 
	  Integer JR_AD_BRNCH, Integer JR_AD_CMPNY)
      throws CreateException { }

   public void ejbPostCreate (java.lang.String JR_NM, 
      java.lang.String JR_DESC, Date JR_EFFCTV_DT, double JR_CNTRL_TTL,
      Date JR_DT_RVRSL, String JR_DCMNT_NMBR,
      Date JR_CNVRSN_DT, double JR_CNVRSN_RT, String JR_APPRVL_STATUS, String JR_RSN_FR_RJCTN,
      char JR_FND_STATUS, byte JR_PSTD, byte JR_RVRSD, String JR_CRTD_BY, Date JR_DT_CRTD,
      String JR_LST_MDFD_BY, Date JR_DT_LST_MDFD,
      String JR_APPRVD_RJCTD_BY, Date JR_DT_APPRVD_RJCTD,
      String JR_PSTD_BY, Date JR_DT_PSTD, String JR_TIN, String JR_SUB_LDGR, byte JR_PRNTD,
	  String JR_RFRNC_NMBR, 
	  Integer JR_AD_BRNCH, Integer JR_AD_CMPNY)
      throws CreateException { }

} // GlJournalBean class
