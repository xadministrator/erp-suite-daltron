package com.ejb.gl;

import java.util.Collection;

import javax.ejb.CreateException;
import javax.ejb.EJBException;

import com.util.AbstractEntityBean;
import com.util.Debug;

/**
 * @ejb:bean name="GlJournalCategoryEJB"
 *           display-name="Journal Category Entity"
 *           type="CMP"
 *           cmp-version="2.x"
 *           view-type="local"
 *           local-jndi-name="omega-ejb/GlJournalCategoryEJB"
 *           schema="GlJournalCategory"
 *           primkey-field="jcCode"
 *       
 * @ejb:pk class="java.lang.Integer"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="gluser"
 *                        role-link="gluserlink"
 *
 * @ejb:permission role-name="gluser"
 *
 * @ejb:interface local-class="com.ejb.gl.LocalGlJournalCategory"
 *                local-extends="javax.ejb.EJBLocalObject"
 *
 * @ejb:home local-class="com.ejb.gl.LocalGlJournalCategoryHome"
 *           local-extends="javax.ejb.EJBLocalHome"
 *
 * @ejb:finder signature="Collection findJcAll(java.lang.Integer JC_AD_CMPNY)"
 *             query="SELECT OBJECT(jc) FROM GlJournalCategory jc WHERE jc.jcAdCompany = ?1"
 *
 * @jboss:query signature="Collection findJcAll(java.lang.Integer JC_AD_CMPNY)"
 *             query="SELECT OBJECT(jc) FROM GlJournalCategory jc WHERE jc.jcAdCompany = ?1 ORDER BY jc.jcName"
 *
 * @ejb:finder signature="LocalGlJournalCategory findByJcName(java.lang.String JC_NM, java.lang.Integer JC_AD_CMPNY)"
 *             query="SELECT OBJECT(jc) FROM GlJournalCategory jc WHERE jc.jcName=?1 AND jc.jcAdCompany = ?2"
 *
 * @jboss:persistence table-name="GL_JRNL_CTGRY"
 *
 * @jboss:entity-command name="mysql-get-generated-keys"
 *
 */

public abstract class GlJournalCategoryBean extends AbstractEntityBean {

   // Access methods for persistent fields

   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    * @ejb:pk-field
    *
    * @jboss:column-name name="JC_CODE"
    *
    * @jboss:persistence auto-increment="true"
    **/
   public abstract java.lang.Integer getJcCode();
   public abstract void setJcCode(java.lang.Integer JC_CODE);

   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="JC_NM"
    **/
   public abstract java.lang.String getJcName();
   /**
    * @ejb:interface-method view-type="local"
    **/
   public abstract void setJcName(java.lang.String JC_NM);

   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="JC_DESC"
    **/
   public abstract java.lang.String getJcDescription();
   /**
    * @ejb:interface-method view-type="local"
    **/
   public abstract void setJcDescription(java.lang.String JC_DESC);

   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="JC_RVRSL_MTHD"
    **/
   public abstract char getJcReversalMethod();
   /**
    * @ejb:interface-method view-type="local"
    **/
   public abstract void setJcReversalMethod(char JC_RVRSL_MTHD);
   
   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="JC_AD_CMPNY"
    **/
   public abstract Integer getJcAdCompany();
   /**
    * @ejb:interface-method view-type="local"
    **/
   public abstract void setJcAdCompany(Integer JC_AD_CMPNY);

   // Access methods for relationship fields

   /**
    * @ejb:interface-method view-type="local"
    * @ejb:relation name="journalcategory-suspenseaccounts"
    *               role-name="journalcategory-has-many-suspenseaccounts"
    */
   public abstract Collection getGlSuspenseAccounts();
   public abstract void setGlSuspenseAccounts(Collection glSuspenseAccounts);

   /**
    * @ejb:interface-method view-type="local"
    * @ejb:relation name="journalcategory-journals"
    *               role-name="journalcategory-has-many-journals"
    */
   public abstract Collection getGlJournals();
   public abstract void setGlJournals(Collection glJournals);

   /**
    * @ejb:interface-method view-type="local"
    * @ejb:relation name="journalcategory-recurringjournals"
    *               role-name="journalcategory-has-many-recurringjournals"
    */
   public abstract Collection getGlRecurringJournals();
   public abstract void setGlRecurringJournals(Collection glRecurringJournals);

   //Business methods

   /**
    * @ejb:interface-method view-type="local"
    **/
   public void addGlSuspenseAccount(LocalGlSuspenseAccount glSuspenseAccount) {

      Debug.print("GlJournalCategoryBean addGlSuspenseAccount");
      try {
         Collection glSuspenseAccounts = getGlSuspenseAccounts();
	 glSuspenseAccounts.add(glSuspenseAccount);
      } catch (Exception ex) {
         throw new EJBException(ex.getMessage());
      }
   }

   /**
    * @ejb:interface-method view-type="local"
    **/
   public void dropGlSuspenseAccount(LocalGlSuspenseAccount glSuspenseAccount) {

      Debug.print("GlJournalCategoryBean dropGlSuspenseAccount");
      try {
         Collection glSuspenseAccounts = getGlSuspenseAccounts();
	 glSuspenseAccounts.remove(glSuspenseAccount);
      } catch (Exception ex) {
         throw new EJBException(ex.getMessage());
      }
   }

   /**
    * @ejb:interface-method view-type="local"
    **/
   public void addGlJournal(LocalGlJournal glJournal) {

      Debug.print("GlJournalCategoryBean addGlJournal");
      try {
         Collection glJournals = getGlJournals();
	 glJournals.add(glJournal);
      } catch (Exception ex) {
         throw new EJBException(ex.getMessage());
      }
   }

   /**
    * @ejb:interface-method view-type="local"
    **/
   public void dropGlJournal(LocalGlJournal glJournal) {

      Debug.print("GlJournalCategoryBean dropGlJournal");
      try {
         Collection glJournals = getGlJournals();
	 glJournals.remove(glJournal);
      } catch (Exception ex) {
         throw new EJBException(ex.getMessage());
      }
   }

   /**
    * @ejb:interface-method view-type="local"
    **/
   public void addGlRecurringJournal(LocalGlRecurringJournal glRecurringJournal) {

      Debug.print("GlJournalCategoryBean addGlRecurringJournal");
      try {
         Collection glRecurringJournals = getGlRecurringJournals();
	 glRecurringJournals.add(glRecurringJournal);
      } catch (Exception ex) {
         throw new EJBException(ex.getMessage());
      }
   }
   
   /**
    * @ejb:interface-method view-type="local"
    **/
   public void dropGlRecurringJournal(LocalGlRecurringJournal glRecurringJournal) {

      Debug.print("GlJournalCategoryBean dropGlRecurringJournal");
      try {
         Collection glRecurringJournals = getGlRecurringJournals();
	 glRecurringJournals.remove(glRecurringJournal);
      } catch (Exception ex) {
         throw new EJBException(ex.getMessage());
      }
   }

   // EntityBean methods

  /**
   * @ejb:create-method view-type="local"
   **/
  public java.lang.Integer ejbCreate (java.lang.Integer JC_CODE, java.lang.String JC_NM,
      java.lang.String JC_DESC, char JC_RVRSL_MTHD, Integer JC_AD_CMPNY)
      throws CreateException {

      Debug.print("GlJournalCategoryBean ejbCreate");
      setJcCode(JC_CODE);
      setJcName(JC_NM);
      setJcDescription(JC_DESC);
      setJcReversalMethod(JC_RVRSL_MTHD);
      setJcAdCompany(JC_AD_CMPNY);
      
      return null;
   }

  /**
    * @ejb:create-method view-type="local"
    **/
   public java.lang.Integer ejbCreate (java.lang.String JC_NM,
      java.lang.String JC_DESC, char JC_RVRSL_MTHD, Integer JC_AD_CMPNY)
      throws CreateException {

      Debug.print("GlJournalCategoryBean ejbCreate");
      
      setJcName(JC_NM);
      setJcDescription(JC_DESC);
      setJcReversalMethod(JC_RVRSL_MTHD);
      setJcAdCompany(JC_AD_CMPNY);
      
      return null;
   }   
  
   public void ejbPostCreate (java.lang.Integer JC_CODE, java.lang.String JC_NM,
      java.lang.String JC_DESC, char JC_RVRSL_MTHD, Integer JC_AD_CMPNY)
      throws CreateException { }

   public void ejbPostCreate (java.lang.String JC_NM,
      java.lang.String JC_DESC, char JC_RVRSL_MTHD, Integer JC_AD_CMPNY)     
      throws CreateException { }

} // GlJournalCategoryBean class
