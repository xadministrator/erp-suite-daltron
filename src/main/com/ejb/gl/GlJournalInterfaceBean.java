package com.ejb.gl;

import java.util.Collection;
import java.util.Date;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;

import com.util.AbstractEntityBean;
import com.util.Debug;

/**
 * @ejb:bean name="GlJournalInterfaceEJB"
 *           display-name="Journal Interface Entity"
 *           type="CMP"
 *           cmp-version="2.x"
 *           view-type="local"
 *           local-jndi-name="omega-ejb/GlJournalInterfaceEJB"
 *           schema="GlJournalInterface"
 *           primkey-field="jriCode"
 *       
 * @ejb:pk class="java.lang.Integer"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="gluser"
 *                        role-link="gluserlink"
 *
 * @ejb:permission role-name="gluser"
 *
 * @ejb:interface local-class="com.ejb.gl.LocalGlJournalInterface"
 *                local-extends="javax.ejb.EJBLocalObject"
 *
 * @ejb:home local-class="com.ejb.gl.LocalGlJournalInterfaceHome"
 *           local-extends="javax.ejb.EJBLocalHome"
 *
 * @ejb:finder signature="Collection findByJsNameAndDateRange(java.lang.String JS_NM, java.util.Date DT_FRM, java.util.Date DT_TO, java.lang.Integer JRI_AD_CMPNY)"
 *             query="SELECT OBJECT(jri) FROM GlJournalInterface jri WHERE jri.jriJournalSource=?1 AND jri.jriDate >= ?2 AND jri.jriDate <= ?3 AND jri.jriAdCompany = ?4"
 *
 * @jboss:query signature="Collection findByJsNameAndDateRange(java.lang.String JS_NM, java.util.Date DT_FRM, java.util.Date DT_TO, java.lang.Integer JRI_AD_CMPNY)"
 *             query="SELECT OBJECT(jri) FROM GlJournalInterface jri WHERE jri.jriJournalSource=?1 AND jri.jriEffectiveDate >= ?2 AND jri.jriEffectiveDate <= ?3 AND jri.jriAdCompany = ?4 ORDER BY jri.jriEffectiveDate"
 *
 * @ejb:finder signature="LocalGlJournalInterface findByJriName(java.lang.String JRI_NM, java.lang.Integer JRI_AD_CMPNY)"
 *             query="SELECT OBJECT(jri) FROM GlJournalInterface jri WHERE jri.jriName = ?1 AND jri.jriAdCompany = ?2"
 *
 * @ejb:select signature="Collection ejbSelectGeneric(java.lang.String jbossQl, java.lang.Object[] args)"
 *             result-type-mapping="Local"
 *             method-intf="LocalHome"
 *             query="SELECT OBJECT(jri) FROM GlJournalInterface jri"
 *
 * @jboss:query signature="Collection ejbSelectGeneric(java.lang.String jbossQl, java.lang.Object[] args)"
 *                         dynamic="true"
 *
 * @ejb:value-object match="*"
 *             name="GlJournalInterface"
 *
 * @jboss:persistence table-name="GL_JRNL_INTRFC"
 *
 * @jboss:entity-command name="mysql-get-generated-keys"
 *
 */

public abstract class GlJournalInterfaceBean extends AbstractEntityBean {


   // Access methods for persistent fields

   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    * @ejb:pk-field
    *
    * @jboss:column-name name="JRI_CODE"
    *
    * @jboss:persistence auto-increment="true"
    **/
   public abstract java.lang.Integer getJriCode();
   public abstract void setJriCode(java.lang.Integer JRI_CODE);

   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="JRI_NM"
    **/
   public abstract java.lang.String getJriName();
   /**
    * @ejb:interface-method view-type="local"
    **/
   public abstract void setJriName(java.lang.String JRI_NM);

   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="JRI_DESC"
    **/
   public abstract java.lang.String getJriDescription();
   /**
    * @ejb:interface-method view-type="local"
    **/
   public abstract void setJriDescription(java.lang.String JRI_DESC);

   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="JRI_EFFCTV_DT"
    **/
   public abstract Date getJriEffectiveDate();
   /**
    * @ejb:interface-method view-type="local"
    **/
   public abstract void setJriEffectiveDate(Date JRI_EFFCTV_DT);
   
   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="JRI_JRNL_CTGRY"
    **/
   public abstract String getJriJournalCategory();
   /**
    * @ejb:interface-method view-type="local"
    **/
   public abstract void setJriJournalCategory(String JRI_JRNL_CTGRY);
   
   
   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="JRI_JRNL_SRC"
    **/
   public abstract String getJriJournalSource();
   /**
    * @ejb:interface-method view-type="local"
    **/
   public abstract void setJriJournalSource(String JRI_JRNL_SRC);
   
   
   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="JRI_FNCTNL_CRRNCY"
    **/
   public abstract String getJriFunctionalCurrency();
   /**
    * @ejb:interface-method view-type="local"
    **/
   public abstract void setJriFunctionalCurrency(String JRI_FNCTNL_CRRNCY);
   
   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="JRI_DT_RVRSL"
    **/
   public abstract Date getJriDateReversal();
   /**
    * @ejb:interface-method view-type="local"
    **/
   public abstract void setJriDateReversal(Date JRI_DT_RVRSL);

   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="JRI_DCMNT_NMBR"
    **/
   public abstract String getJriDocumentNumber();
   /**
    * @ejb:interface-method view-type="local"
    **/
   public abstract void setJriDocumentNumber(String JRI_DCMNT_NMBR);

   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="JRI_CNVRSN_DT"
    **/
   public abstract Date getJriConversionDate();
   /**
    * @ejb:interface-method view-type="local"
    **/
   public abstract void setJriConversionDate(Date JRI_CNVRSN_DT);

   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="JRI_CNVRSN_RT"
    **/
   public abstract double getJriConversionRate();
   /**
    * @ejb:interface-method view-type="local"
    **/
   public abstract void setJriConversionRate(double JRI_CNVRSN_RT);

   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="JRI_FND_STATUS"
    **/
   public abstract char getJriFundStatus();
   /**
    * @ejb:interface-method view-type="local"
    **/   
   public abstract void setJriFundStatus(char JRI_FND_STATUS);
   
   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="JRI_RVRSD"
    **/
   public abstract byte getJriReversed();
   /**
    * @ejb:interface-method view-type="local"
    **/
   public abstract void setJriReversed(byte JRI_RVRSD);
   
   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="JRI_TIN"
    **/
   public abstract String getJriTin();
   /**
    * @ejb:interface-method view-type="local"
    **/
   public abstract void setJriTin(String JRI_TIN);
   
   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="JRI_SUB_LDGR"
    **/
   public abstract String getJriSubLedger();
   /**
    * @ejb:interface-method view-type="local"
    **/
   public abstract void setJriSubLedger(String JRI_SUB_LDGR);
   
   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="JRI_RFRNC_NMBR"
    **/
   public abstract String getJriReferenceNumber();
   /**
    * @ejb:interface-method view-type="local"
    **/
   public abstract void setJriReferenceNumber(String JRI_RFRNC_NMBR);
   
   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="JRI_AD_BRNCH"
    **/
   public abstract Integer getJriAdBranch();
   /**
    * @ejb:interface-method view-type="local"
    **/
   public abstract void setJriAdBranch(Integer JRI_AD_BRNCH);
   
   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="JRI_AD_CMPNY"
    **/
   public abstract Integer getJriAdCompany();
   /**
    * @ejb:interface-method view-type="local"
    **/
   public abstract void setJriAdCompany(Integer JRI_AD_CMPNY);

   // Access methods for relationship fields

   /**
    * @ejb:interface-method view-type="local"
    * @ejb:relation name="journalinterface-journallineinterfaces"
    *               role-name="journalinterface-has-many-journallineinterfaces"
    */
   public abstract Collection getGlJournalLineInterfaces();
   public abstract void setGlJournalLineInterfaces(Collection glJournalLineInterfaces);

   /**
	* @jboss:dynamic-ql
	*/
   public abstract Collection ejbSelectGeneric(java.lang.String jbossQl, java.lang.Object[] args)
   throws FinderException;      
	
	// BUSINESS METHODS
	
   /**
	* @ejb:home-method view-type="local"
	*/
   public Collection ejbHomeGetJriByCriteria(java.lang.String jbossQl, java.lang.Object[] args)
   throws FinderException {
	
   return ejbSelectGeneric(jbossQl, args);
	
   }  

   /**
    * @ejb:interface-method view-type="local"
    **/
   public void addGlJournalLineInterface(LocalGlJournalLineInterface GlJournalLineInterface) {

      Debug.print("GlJournalInterfaceBean addGlJournalLineInterface");
      
      try {
      	
         Collection GlJournalLineInterfaces = getGlJournalLineInterfaces();
	     GlJournalLineInterfaces.add(GlJournalLineInterface);
	     
      } catch (Exception ex) {
      	
         throw new EJBException(ex.getMessage());
         
      }
   }

   /**
    * @ejb:interface-method view-type="local"
    **/
   public void dropGlJournalLineInterface(LocalGlJournalLineInterface GlJournalLineInterface) {

      Debug.print("GlJournalInterfaceBean dropGlJournalLineInterface");
      
      try {
      	
         Collection GlJournalLineInterfaces = getGlJournalLineInterfaces();
	 	 GlJournalLineInterfaces.remove(GlJournalLineInterface);
	 	 
      } catch (Exception ex) {
      	
         throw new EJBException(ex.getMessage());
         
      }
   }

   // EntityBean methods

   /**
    * @ejb:create-method view-type="local"
    **/
   public java.lang.Integer ejbCreate (java.lang.Integer JRI_CODE, java.lang.String JRI_NM, 
      java.lang.String JRI_DESC, Date JRI_EFFCTV_DT, String JRI_JRNL_CTGRY,
      String JRI_JRNL_SRC, String JRI_FNCTNL_CRRNCY,  
      Date JRI_DT_RVRSL, String JRI_DCMNT_NMBR, Date JRI_CNVRSN_DT, double JRI_CNVRSN_RT, 
      char JRI_FND_STATUS, byte JRI_RVRSD, String JRI_TIN, String JRI_SUB_LDGR, String JRI_RFRNC_NMBR,
	  Integer JRI_AD_BRNCH, Integer JRI_AD_CMPNY)
      throws CreateException {

      Debug.print("GlJournalInterfaceBean ejbCreate");
      setJriCode(JRI_CODE);
      setJriName(JRI_NM);
      setJriDescription(JRI_DESC);
      setJriEffectiveDate(JRI_EFFCTV_DT);
      setJriJournalCategory(JRI_JRNL_CTGRY);
      setJriJournalSource(JRI_JRNL_SRC);
      setJriFunctionalCurrency(JRI_FNCTNL_CRRNCY);
      setJriDateReversal(JRI_DT_RVRSL);
      setJriDocumentNumber(JRI_DCMNT_NMBR);
      setJriConversionDate(JRI_CNVRSN_DT);
      setJriConversionRate(JRI_CNVRSN_RT);
      setJriFundStatus(JRI_FND_STATUS);
      setJriReversed(JRI_RVRSD);
      setJriTin(JRI_TIN);
      setJriSubLedger(JRI_SUB_LDGR);
      setJriReferenceNumber(JRI_RFRNC_NMBR);
      setJriAdBranch(JRI_AD_BRNCH);
      setJriAdCompany(JRI_AD_CMPNY);
      
      return null;
   }

   /**
    * @ejb:create-method view-type="local"
    **/
   public java.lang.Integer ejbCreate (java.lang.String JRI_NM, 
      java.lang.String JRI_DESC, Date JRI_EFFCTV_DT, String JRI_JRNL_CTGRY,
      String JRI_JRNL_SRC, String JRI_FNCTNL_CRRNCY,  
      Date JRI_DT_RVRSL, String JRI_DCMNT_NMBR, Date JRI_CNVRSN_DT, double JRI_CNVRSN_RT, 
      char JRI_FND_STATUS, byte JRI_RVRSD, String JRI_TIN, String JRI_SUB_LDGR, String JRI_RFRNC_NMBR,
      Integer JRI_AD_BRNCH, Integer JRI_AD_CMPNY)
      throws CreateException {

      Debug.print("GlJournalInterfaceBean ejbCreate");

      setJriName(JRI_NM);
      setJriDescription(JRI_DESC);
      setJriEffectiveDate(JRI_EFFCTV_DT);
      setJriJournalCategory(JRI_JRNL_CTGRY);
      setJriJournalSource(JRI_JRNL_SRC);
      setJriFunctionalCurrency(JRI_FNCTNL_CRRNCY);
      setJriDateReversal(JRI_DT_RVRSL);
      setJriDocumentNumber(JRI_DCMNT_NMBR);
      setJriConversionDate(JRI_CNVRSN_DT);
      setJriConversionRate(JRI_CNVRSN_RT);
      setJriFundStatus(JRI_FND_STATUS);
      setJriReversed(JRI_RVRSD);
      setJriTin(JRI_TIN);
      setJriSubLedger(JRI_SUB_LDGR);
      setJriReferenceNumber(JRI_RFRNC_NMBR);
      setJriAdBranch(JRI_AD_BRNCH);
      setJriAdCompany(JRI_AD_CMPNY);
      
      return null;
   }

   public void ejbPostCreate (java.lang.Integer JRI_CODE, java.lang.String JRI_NM, 
      java.lang.String JRI_DESC, Date JRI_EFFCTV_DT, String JRI_JRNL_CTGRY,
      String JRI_JRNL_SRC, String JRI_FNCTNL_CRRNCY, 
      Date JRI_DT_RVRSL, String JRI_DCMNT_NMBR, Date JRI_CNVRSN_DT, double JRI_CNVRSN_RT, 
      char JRI_FND_STATUS, byte JRI_RVRSD, String JRI_TIN, String JRI_SUB_LDGR, String JRI_RFRNC_NMBR,
      Integer JRI_AD_BRNCH, Integer JRI_AD_CMPNY)
      throws CreateException { }

   public void ejbPostCreate (java.lang.String JRI_NM, 
      java.lang.String JRI_DESC, Date JRI_EFFCTV_DT, String JRI_JRNL_CTGRY,
      String JRI_JRNL_SRC, String JRI_FNCTNL_CRRNCY, 
      Date JRI_DT_RVRSL, String JRI_DCMNT_NMBR, Date JRI_CNVRSN_DT, double JRI_CNVRSN_RT, 
      char JRI_FND_STATUS, byte JRI_RVRSD, String JRI_TIN, String JRI_SUB_LDGR, String JRI_RFRNC_NMBR,
      Integer JRI_AD_BRNCH, Integer JRI_AD_CMPNY)
      throws CreateException { }

} // GlJournalInterfaceBean class
