package com.ejb.gl;

import javax.ejb.CreateException;

import com.util.AbstractEntityBean;
import com.util.Debug;

/**
 * @ejb:bean name="GlJournalLineEJB"
 *           display-name="Journal Line Entity"
 *           type="CMP"
 *           cmp-version="2.x"
 *           view-type="local"
 *           local-jndi-name="omega-ejb/GlJournalLineEJB"
 *           schema="GlJournalLine"
 *           primkey-field="jlCode"
 *       
 * @ejb:pk class="java.lang.Integer"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="gluser"
 *                        role-link="gluserlink"
 *
 * @ejb:permission role-name="gluser"
 *
 * @ejb:interface local-class="com.ejb.gl.LocalGlJournalLine"
 *                local-extends="javax.ejb.EJBLocalObject"
 *
 * @ejb:home local-class="com.ejb.gl.LocalGlJournalLineHome"
 *           local-extends="javax.ejb.EJBLocalHome"
 *
 * @ejb:finder signature="Collection findUnpostedJlByJrEffectiveDateAndCoaCode(java.util.Date JR_DT, java.lang.Integer COA_CODE, java.lang.Integer JL_AD_CMPNY)"
 *             query="SELECT OBJECT(jl) FROM GlJournal jr, IN(jr.glJournalLines) jl WHERE jr.jrPosted = 0 AND jr.jrEffectiveDate <= ?1 AND jl.glChartOfAccount.coaCode = ?2 AND jl.jlAdCompany = ?3"
 *
 * @jboss:query signature="Collection findUnpostedJlByJrEffectiveDateAndCoaCode(java.util.Date JR_DT, java.lang.Integer COA_CODE, java.lang.Integer JL_AD_CMPNY)"
 *             query="SELECT OBJECT(jl) FROM GlJournal jr, IN(jr.glJournalLines) jl WHERE jr.jrPosted = 0 AND jr.jrEffectiveDate <= ?1 AND jl.glChartOfAccount.coaCode = ?2 AND jl.jlAdCompany = ?3"
 *
 * @ejb:finder signature="Collection findPostedJlByJrEffectiveDateAndCoaAccountNumberRange(java.lang.String COA_ACCNT_NMBR_FROM, java.lang.String COA_ACCNT_NMBR_TO, java.lang.Integer JL_AD_CMPNY)"
 *             query="SELECT OBJECT(jl) FROM GlJournal jr, IN(jr.glJournalLines) jl WHERE jr.jrPosted = 1 AND jl.glChartOfAccount.coaAccountNumber BETWEEN ?1 AND ?2 AND jl.jlAdCompany = ?3"
 *
 * @jboss:query signature="Collection findPostedJlByJrEffectiveDateAndCoaAccountNumberRange(java.lang.String COA_ACCNT_NMBR_FROM, java.lang.String COA_ACCNT_NMBR_TO, java.lang.Integer JL_AD_CMPNY)"
 *             query="SELECT OBJECT(jl) FROM GlJournal jr, IN(jr.glJournalLines) jl WHERE jr.jrPosted = 1 AND jl.glChartOfAccount.coaAccountNumber BETWEEN ?1 AND ?2 AND jl.jlAdCompany = ?3 ORDER BY jl.glChartOfAccount.coaAccountNumber, jr.jrEffectiveDate"
 *
 *
 * @ejb:finder signature="Collection findUnpostedJlByJrEffectiveDateRangeAndCoaCode(java.util.Date JR_DT_FRM, java.util.Date JR_DT_TO, java.lang.Integer COA_CODE, java.lang.Integer JL_AD_CMPNY)"
 *             query="SELECT OBJECT(jl) FROM GlJournal jr, IN(jr.glJournalLines) jl WHERE jr.jrPosted = 0 AND jr.jrEffectiveDate >= ?1 AND jr.jrEffectiveDate <= ?2 AND jl.glChartOfAccount.coaCode = ?3 AND jl.jlAdCompany = ?4"
 *
 * @jboss:query signature="Collection findUnpostedJlByJrEffectiveDateRangeAndCoaCode(java.util.Date JR_DT_FRM, java.util.Date JR_DT_TO, java.lang.Integer COA_CODE, java.lang.Integer JL_AD_CMPNY)"
 *             query="SELECT OBJECT(jl) FROM GlJournal jr, IN(jr.glJournalLines) jl WHERE jr.jrPosted = 0 AND jr.jrEffectiveDate >= ?1 AND jr.jrEffectiveDate <= ?2 AND jl.glChartOfAccount.coaCode = ?3 AND jl.jlAdCompany = ?4"
 * 
 * @ejb:finder signature="Collection findUnpostedJlByJrEffectiveDateAndCoaCodeAndJsName(java.util.Date JR_DT, java.lang.Integer COA_CODE, java.lang.String JS_NM, java.lang.Integer JL_AD_CMPNY)"
 *             query="SELECT OBJECT(jl) FROM GlJournal jr, IN(jr.glJournalLines) jl WHERE jr.jrPosted = 0 AND jr.jrEffectiveDate <= ?1 AND jl.glChartOfAccount.coaCode = ?2 AND jl.glJournal.glJournalSource.jsName=?3 AND jl.jlAdCompany = ?4"
 *
 * @jboss:query signature="Collection findUnpostedJlByJrEffectiveDateAndCoaCodeAndJsName(java.util.Date JR_DT, java.lang.Integer COA_CODE, java.lang.String JS_NM, java.lang.Integer JL_AD_CMPNY)"
 *             query="SELECT OBJECT(jl) FROM GlJournal jr, IN(jr.glJournalLines) jl WHERE jr.jrPosted = 0 AND jr.jrEffectiveDate <= ?1 AND jl.glChartOfAccount.coaCode = ?2 AND jl.glJournal.glJournalSource.jsName=?3 AND jl.jlAdCompany = ?4"
 *
 * @ejb:finder signature="Collection findPostedJlByJrEffectiveDateRangeAndCoaCode(java.util.Date JR_DT_FRM, java.util.Date JR_DT_TO, java.lang.Integer COA_CODE, java.lang.Integer JL_AD_CMPNY)"
 *             query="SELECT OBJECT(jl) FROM GlJournal jr, IN(jr.glJournalLines) jl WHERE jr.jrPosted = 1 AND jr.jrEffectiveDate >= ?1 AND jr.jrEffectiveDate <= ?2 AND jl.glChartOfAccount.coaCode = ?3 AND jl.jlAdCompany = ?4 ORDER BY jr.jrEffectiveDate, jr.jrDocumentNumber"
 *
 * @jboss:query signature="Collection findPostedJlByJrEffectiveDateRangeAndCoaCode(java.util.Date JR_DT_FRM, java.util.Date JR_DT_TO, java.lang.Integer COA_CODE, java.lang.Integer JL_AD_CMPNY)"
 *             query="SELECT OBJECT(jl) FROM GlJournal jr, IN(jr.glJournalLines) jl WHERE jr.jrPosted = 1 AND jr.jrEffectiveDate >= ?1 AND jr.jrEffectiveDate <= ?2 AND jl.glChartOfAccount.coaCode = ?3 AND jl.jlAdCompany = ?4 ORDER BY jr.jrEffectiveDate, jr.jrDocumentNumber"
 *
 * @ejb:finder signature="Collection findPostedJlByJrEffectiveDateAndCoaCodeAndJsName(java.util.Date JR_DT, java.lang.Integer COA_CODE, java.lang.String JS_NM, java.lang.Integer JL_AD_CMPNY)"
 *             query="SELECT OBJECT(jl) FROM GlJournal jr, IN(jr.glJournalLines) jl WHERE jr.jrPosted = 1 AND jr.jrEffectiveDate <= ?1 AND jl.glChartOfAccount.coaCode = ?2 AND jl.glJournal.glJournalSource.jsName=?3 AND jl.jlAdCompany = ?4"
 *
 * @jboss:query signature="Collection findPostedJlByJrEffectiveDateAndCoaCodeAndJsName(java.util.Date JR_DT, java.lang.Integer COA_CODE, java.lang.String JS_NM, java.lang.Integer JL_AD_CMPNY)"
 *             query="SELECT OBJECT(jl) FROM GlJournal jr, IN(jr.glJournalLines) jl WHERE jr.jrPosted = 1 AND jr.jrEffectiveDate <= ?1 AND jl.glChartOfAccount.coaCode = ?2 AND jl.glJournal.glJournalSource.jsName=?3 AND jl.jlAdCompany = ?4"
 *
 * @ejb:finder signature="Collection findByJrEffectiveDateRangeAndCoaCode(java.util.Date JR_DT_FRM, java.util.Date JR_DT_TO, java.lang.Integer COA_CODE, java.lang.Integer JL_AD_CMPNY)"
 *             query="SELECT OBJECT(jl) FROM GlJournal jr, IN(jr.glJournalLines) jl WHERE jr.jrEffectiveDate >= ?1 AND jr.jrEffectiveDate <= ?2 AND jl.glChartOfAccount.coaCode = ?3 AND jl.jlAdCompany = ?4 ORDER BY jr.jrEffectiveDate, jr.jrDocumentNumber"
 *
 * @jboss:query signature="Collection findByJrEffectiveDateRangeAndCoaCode(java.util.Date JR_DT_FRM, java.util.Date JR_DT_TO, java.lang.Integer COA_CODE, java.lang.Integer JL_AD_CMPNY)"
 *             query="SELECT OBJECT(jl) FROM GlJournal jr, IN(jr.glJournalLines) jl WHERE jr.jrEffectiveDate >= ?1 AND jr.jrEffectiveDate <= ?2 AND jl.glChartOfAccount.coaCode = ?3 AND jl.jlAdCompany = ?4 ORDER BY jr.jrEffectiveDate, jr.jrDocumentNumber"
 * 
 * @ejb:finder signature="Collection findByJrEffectiveDateAndCoaCodeAndJsName(java.util.Date JR_DT, java.lang.Integer COA_CODE, java.lang.String JS_NM, java.lang.Integer JL_AD_CMPNY)"
 *             query="SELECT OBJECT(jl) FROM GlJournal jr, IN(jr.glJournalLines) jl WHERE jr.jrEffectiveDate <= ?1 AND jl.glChartOfAccount.coaCode = ?2 AND jl.glJournal.glJournalSource.jsName=?3 AND jl.jlAdCompany = ?4"
 *
 * @jboss:query signature="Collection findByJrEffectiveDateAndCoaCodeAndJsName(java.util.Date JR_DT, java.lang.Integer COA_CODE, java.lang.String JS_NM, java.lang.Integer JL_AD_CMPNY)"
 *             query="SELECT OBJECT(jl) FROM GlJournal jr, IN(jr.glJournalLines) jl WHERE jr.jrEffectiveDate <= ?1 AND jl.glChartOfAccount.coaCode = ?2 AND jl.glJournal.glJournalSource.jsName=?3 AND jl.jlAdCompany = ?4"
 *
 * @ejb:finder signature="Collection findByJrCode(java.lang.Integer JR_CODE, java.lang.Integer JL_AD_CMPNY)"
 *             query="SELECT OBJECT(jl) FROM GlJournal jr, IN(jr.glJournalLines) jl WHERE jr.jrCode = ?1 AND jl.jlAdCompany = ?2"
 *
 * @jboss:query signature="Collection findByJrCode(java.lang.Integer JR_CODE, java.lang.Integer JR_AD_CMPNY)"
 *             query="SELECT OBJECT(jl) FROM GlJournal jr, IN(jr.glJournalLines) jl WHERE jr.jrCode = ?1 AND jl.jlAdCompany = ?2 ORDER BY jl.jlDebit DESC"
 *
 * @ejb:finder signature="Collection findManualJrByEffectiveDateRangeAndCoaCode(java.util.Date JR_DT_FRM, java.util.Date JR_DT_TO, java.lang.Integer COA_CODE, java.lang.Integer JR_AD_CMPNY)"
 * 			   query="SELECT OBJECT(jl) FROM GlJournal jr, IN(jr.glJournalLines) jl WHERE jr.glJournalSource.jsName='MANUAL' AND jr.jrPosted=1 AND jr.jrEffectiveDate>=?1 AND jr.jrEffectiveDate<=?2 AND jl.glChartOfAccount.coaCode=?3 AND jl.jlAdCompany=?4"
 * 
 * @jboss:query signature="Collection findManualJrByEffectiveDateRangeAndCoaCode(java.util.Date JR_DT_FRM, java.util.Date JR_DT_TO, java.lang.Integer COA_CODE, java.lang.Integer JR_AD_CMPNY)"
 * 			   query="SELECT OBJECT(jl) FROM GlJournal jr, IN(jr.glJournalLines) jl WHERE jr.glJournalSource.jsName='MANUAL' AND jr.jrPosted=1 AND jr.jrEffectiveDate>=?1 AND jr.jrEffectiveDate<=?2 AND jl.glChartOfAccount.coaCode=?3 AND jl.jlAdCompany=?4 ORDER BY jr.jrEffectiveDate, jr.jrDocumentNumber"
 * 
 * @ejb:finder signature="Collection findUnpostedJrByJlCoaAccountNumberAndBrCode(java.lang.Integer COA_CODE, java.lang.Integer JR_AD_BRNCH, java.lang.Integer JL_AD_CMPNY)"
 *             query="SELECT OBJECT(jl) FROM GlJournal jr, IN(jr.glJournalLines) jl WHERE jr.jrPosted=0 AND jl.glChartOfAccount.coaCode=?1 AND jr.jrAdBranch = ?2 AND jl.jlAdCompany = ?3"
 * 
 * 
 * @ejb:finder signature="Collection findJrByDateAndCoaAccountNumberAndCurrencyAndJrPosted(java.util.Date JR_DT, java.lang.Integer COA_CODE, java.lang.Integer FC_CODE, byte JR_PSTD, java.lang.Integer JL_AD_CMPNY)"
 *             query="SELECT OBJECT(jl) FROM GlJournal jr, IN(jr.glJournalLines) jl WHERE jr.glJournalSource.jsName='MANUAL' AND jr.jrEffectiveDate<=?1 AND jl.glChartOfAccount.coaCode=?2 AND jr.glFunctionalCurrency.fcCode=?3 AND jr.jrPosted=?4 AND jr.jrReversed=0 AND jl.jlAdCompany=?5"
 *
 * @jboss:query signature="Collection findJrByDateAndCoaAccountNumberAndCurrencyAndJrPosted(java.util.Date JR_DT, java.lang.Integer COA_CODE, java.lang.Integer FC_CODE, byte JR_PSTD, java.lang.Integer JL_AD_CMPNY)"
 *             query="SELECT OBJECT(jl) FROM GlJournal jr, IN(jr.glJournalLines) jl WHERE jr.glJournalSource.jsName='MANUAL' AND jr.jrEffectiveDate<=?1 AND jl.glChartOfAccount.coaCode=?2 AND jr.glFunctionalCurrency.fcCode=?3 AND jr.jrPosted=?4 AND jr.jrReversed=0 AND jl.jlAdCompany=?5 ORDER BY jr.jrEffectiveDate, jr.jrDocumentNumber"
 *
 * @ejb:finder signature="Collection findJrByDateAndCoaAccountNumberAndCurrencyAndJrPostedAndJsName(java.util.Date JR_DT, java.lang.Integer COA_CODE, java.lang.Integer FC_CODE, byte JR_PSTD, java.lang.String JS_NM, java.lang.Integer JL_AD_CMPNY)"
 *             query="SELECT OBJECT(jl) FROM GlJournal jr, IN(jr.glJournalLines) jl WHERE jr.jrEffectiveDate<=?1 AND jl.glChartOfAccount.coaCode=?2 AND jr.glFunctionalCurrency.fcCode=?3 AND jr.jrPosted=?4 AND jr.jrReversed=0 AND jl.glJournal.glJournalSource.jsName=?5 AND jl.jlAdCompany=?6"
 *
 * @jboss:query signature="Collection findJrByDateAndCoaAccountNumberAndCurrencyAndJrPostedAndJsName(java.util.Date JR_DT, java.lang.Integer COA_CODE, java.lang.Integer FC_CODE, byte JR_PSTD, java.lang.String JS_NM, java.lang.Integer JL_AD_CMPNY)"
 *             query="SELECT OBJECT(jl) FROM GlJournal jr, IN(jr.glJournalLines) jl WHERE jr.jrEffectiveDate<=?1 AND jl.glChartOfAccount.coaCode=?2 AND jr.glFunctionalCurrency.fcCode=?3 AND jr.jrPosted=?4 AND jr.jrReversed=0 AND jl.glJournal.glJournalSource.jsName=?5 AND jl.jlAdCompany=?6 ORDER BY jr.jrEffectiveDate, jr.jrDocumentNumber"
 *
 * @ejb:value-object match="*"
 *             name="GlJournalLine"
 *
 * @jboss:persistence table-name="GL_JRNL_LN"
 *
 * @jboss:entity-command name="mysql-get-generated-keys"
 *
 */

public abstract class GlJournalLineBean extends AbstractEntityBean {


   // Access methods for persistent fields

   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    * @ejb:pk-field
    *
    * @jboss:column-name name="JL_CODE"
    *
    * @jboss:persistence auto-increment="true"
    **/
   public abstract java.lang.Integer getJlCode();
   public abstract void setJlCode(java.lang.Integer JL_CODE);

   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="JL_LN_NMBR"
    **/
   public abstract short getJlLineNumber();
   public abstract void setJlLineNumber(short JL_LN_NMBR);

   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="JL_DBT"
    **/
   public abstract byte getJlDebit();
   /**
    * @ejb:interface-method view-type="local"
    **/
   public abstract void setJlDebit(byte JL_DBT);

   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="JL_AMNT"
    **/
   public abstract double getJlAmount();
   /**
    * @ejb:interface-method view-type="local"
    **/
   public abstract void setJlAmount(double JL_AMNT);

   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="JL_DESC"
    **/
   public abstract String getJlDescription();
   /**
    * @ejb:interface-method view-type="local"
    **/
   public abstract void setJlDescription(String JL_DESC);
   
   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="JL_AD_CMPNY"
    **/
   public abstract Integer getJlAdCompany();
   /**
    * @ejb:interface-method view-type="local"
    **/
   public abstract void setJlAdCompany(Integer JL_AD_CMPNY);

   // Access methods for relationship fields

   /**
    * @ejb:interface-method view-type="local"
    * @ejb:relation name="journal-journallines"
    *               role-name="journalline-has-one-journal"
    *               cascade-delete="yes"
    *
    * @jboss:relation related-pk-field="jrCode"
    *                 fk-column="GL_JOURNAL"
    */
   public abstract LocalGlJournal getGlJournal();
   /**
	 * @ejb:interface-method view-type="local"
	 **/
   public abstract void setGlJournal(LocalGlJournal glJournal);

   /**
    * @ejb:interface-method view-type="local"
    * @ejb:relation name="chartofaccount-journallines"
    *               role-name="journalline-has-one-chartofaccount"
    *
    * @jboss:relation related-pk-field="coaCode"
    *                 fk-column="GL_CHART_OF_ACCOUNT"
    */
   public abstract LocalGlChartOfAccount getGlChartOfAccount();
   /**
	 * @ejb:interface-method view-type="local"
	 **/
   public abstract void setGlChartOfAccount(LocalGlChartOfAccount glChartOfAccount);

   // Business methods

   // EntityBean methods

   /**
    * @ejb:create-method view-type="local"
    **/
   public java.lang.Integer ejbCreate (java.lang.Integer JL_CODE, short JL_LN_NMBR, byte JL_DBT, double JL_AMNT,
   		String JL_DESC, Integer JL_AD_CMPNY)
      throws CreateException {

      Debug.print("glJournalLineBean ejbCreate");
      setJlCode(JL_CODE);
      setJlLineNumber(JL_LN_NMBR);
      setJlDebit(JL_DBT);
      setJlAmount(JL_AMNT);
      setJlDescription(JL_DESC);
      setJlAdCompany(JL_AD_CMPNY);
      
      return null;
   }

   /**
    * @ejb:create-method view-type="local"
    **/
   public java.lang.Integer ejbCreate (short JL_LN_NMBR, byte JL_DBT, double JL_AMNT, String JL_DESC,
   		Integer JL_AD_CMPNY)
      throws CreateException {

      Debug.print("glJournalLineBean ejbCreate");
      
      setJlLineNumber(JL_LN_NMBR);
      setJlDebit(JL_DBT);
      setJlAmount(JL_AMNT);
      setJlDescription(JL_DESC);
      setJlAdCompany(JL_AD_CMPNY);
      
      return null;
   }

   public void ejbPostCreate (java.lang.Integer JL_CODE, short JL_LN_NMBR, byte JL_DBT, double JL_AMNT,
   		String JL_DESC, Integer JL_AD_CMPNY)
      throws CreateException { }

   public void ejbPostCreate (short JL_LN_NMBR, byte JL_DBT, double JL_AMNT, String JL_DESC, Integer JL_AD_CMPNY)
      throws CreateException { }

} // GlJournalLineBean class
