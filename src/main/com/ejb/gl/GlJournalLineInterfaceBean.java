package com.ejb.gl;

import javax.ejb.CreateException;

import com.util.AbstractEntityBean;
import com.util.Debug;

/**
 * @ejb:bean name="GlJournalLineInterfaceEJB"
 *           display-name="Journal Line Interface Entity"
 *           type="CMP"
 *           cmp-version="2.x"
 *           view-type="local"
 *           local-jndi-name="omega-ejb/GlJournalLineInterfaceEJB"
 *           schema="GlJournalLineInterface"
 *           primkey-field="jliCode"
 *       
 * @ejb:pk class="java.lang.Integer"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="gluser"
 *                        role-link="gluserlink"
 *
 * @ejb:permission role-name="gluser"
 *
 * @ejb:interface local-class="com.ejb.gl.LocalGlJournalLineInterface"
 *                local-extends="javax.ejb.EJBLocalObject"
 *
 * @ejb:home local-class="com.ejb.gl.LocalGlJournalLineInterfaceHome"
 *           local-extends="javax.ejb.EJBLocalHome"
 *
 * @ejb:value-object match="*"
 *             name="GlJournalLineInterface"
 * 
 * @ejb:finder signature="Collection findJliByJliCoaAccountNumber(java.lang.String JLI_COA_ACCNT_NMBR, java.lang.Integer JLI_AD_CMPNY)"
 *             query="SELECT OBJECT(jli) FROM GlJournalLineInterface jli WHERE jli.jliCoaAccountNumber = ?1 AND jli.jliAdCompany = ?2"
 *
 * @jboss:persistence table-name="GL_JRNL_LN_INTRFC"
 *
 * @jboss:entity-command name="mysql-get-generated-keys"
 *
 */

public abstract class GlJournalLineInterfaceBean extends AbstractEntityBean {


   // Access methods for persistent fields

   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    * @ejb:pk-field
    *
    * @jboss:column-name name="JLI_CODE"
    *
    * @jboss:persistence auto-increment="true"
    **/
   public abstract java.lang.Integer getJliCode();
   public abstract void setJliCode(java.lang.Integer JLI_CODE);

   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="JLI_LN_NMBR"
    **/
   public abstract short getJliLineNumber();
   public abstract void setJliLineNumber(short JLI_LN_NMBR);

   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="JLI_DBT"
    **/
   public abstract byte getJliDebit();
   /**
    * @ejb:interface-method view-type="local"
    **/
   public abstract void setJliDebit(byte JLI_DBT);

   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="JLI_AMNT"
    **/
   public abstract double getJliAmount();
   /**
    * @ejb:interface-method view-type="local"
    **/
   public abstract void setJliAmount(double JLI_AMNT);
   
   
   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="JLI_COA_ACCNT_NMBR"
    **/
   public abstract String getJliCoaAccountNumber();
   /**
    * @ejb:interface-method view-type="local"
    **/
   public abstract void setJliCoaAccountNumber(String JLI_COA_ACCNT_NMBR);
   
   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="JLI_AD_CMPNY"
    **/
   public abstract Integer getJliAdCompany();
   /**
    * @ejb:interface-method view-type="local"
    **/
   public abstract void setJliAdCompany(Integer JLI_AD_CMPNY);  
   

   // Access methods for relationship fields

   /**
    * @ejb:interface-method view-type="local"
    * @ejb:relation name="journalinterface-journallineinterfaces"
    *               role-name="journallineinterface-has-one-journalinterface"
    *               cascade-delete="yes"
    *
    * @jboss:relation related-pk-field="jriCode"
    *                 fk-column="GL_JOURNAL_INTERFACE"
    */
   public abstract LocalGlJournalInterface getGlJournalInterface();
   public abstract void setGlJournalInterface(LocalGlJournalInterface glJournalInterface);

   // Business methods

   // EntityBean methods

   /**
    * @ejb:create-method view-type="local"
    **/
   public java.lang.Integer ejbCreate (java.lang.Integer JLI_CODE, short JLI_LN_NMBR,
      byte JLI_DBT, double JLI_AMNT, String JLI_COA_ACCNT_NMBR, Integer JLI_AD_CMPNY)
      throws CreateException {

      Debug.print("glJournalLineInterfaceBean ejbCreate");
      setJliCode(JLI_CODE);
      setJliLineNumber(JLI_LN_NMBR);
      setJliDebit(JLI_DBT);
      setJliAmount(JLI_AMNT);
      setJliCoaAccountNumber(JLI_COA_ACCNT_NMBR);
      setJliAdCompany(JLI_AD_CMPNY);
      
      return null;
   }

   /**
    * @ejb:create-method view-type="local"
    **/
   public java.lang.Integer ejbCreate (short JLI_LN_NMBR,
      byte JLI_DBT, double JLI_AMNT, String JLI_COA_ACCNT_NMBR, Integer JLI_AD_CMPNY)
      throws CreateException {

      Debug.print("glJournalLineInterfaceBean ejbCreate");
      
      setJliLineNumber(JLI_LN_NMBR);
      setJliDebit(JLI_DBT);
      setJliAmount(JLI_AMNT);
      setJliCoaAccountNumber(JLI_COA_ACCNT_NMBR);
      setJliAdCompany(JLI_AD_CMPNY);
      
      return null;
   }

   public void ejbPostCreate (java.lang.Integer JLI_CODE, short JLI_LN_NMBR,
      byte JLI_DBT, double JLI_AMNT, String JLI_COA_ACCNT_NMBR, Integer JLI_AD_CMPNY)
      throws CreateException { }

   public void ejbPostCreate (short JLI_LN_NMBR,
      byte JLI_DBT, double JLI_AMNT, String JLI_COA_ACCNT_NMBR, Integer JLI_AD_CMPNY)
      throws CreateException { }

} // GlJournalLineInterfaceBean class
