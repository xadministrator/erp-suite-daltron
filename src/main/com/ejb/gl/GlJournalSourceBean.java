package com.ejb.gl;

import java.util.Collection;

import javax.ejb.CreateException;
import javax.ejb.EJBException;

import com.util.AbstractEntityBean;
import com.util.Debug;

/**
 * @ejb:bean name="GlJournalSourceEJB"
 *           display-name="Journal Source Entity"
 *           type="CMP"
 *           cmp-version="2.x"
 *           view-type="local"
 *           local-jndi-name="omega-ejb/GlJournalSourceEJB"
 *           schema="GlJournalSource"
 *           primkey-field="jsCode"
 *       
 * @ejb:pk class="java.lang.Integer"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="gluser"
 *                        role-link="gluserlink"
 *
 * @ejb:permission role-name="gluser"
 *
 * @ejb:interface local-class="com.ejb.gl.LocalGlJournalSource"
 *                local-extends="javax.ejb.EJBLocalObject"
 *
 * @ejb:home local-class="com.ejb.gl.LocalGlJournalSourceHome"
 *           local-extends="javax.ejb.EJBLocalHome"
 *
 * @ejb:finder signature="Collection findJsAll(java.lang.Integer JS_AD_CMPNY)"
 *             query="SELECT OBJECT(js) FROM GlJournalSource js WHERE js.jsAdCompany = ?1"
 *
 * @jboss:query signature="Collection findJsAll(java.lang.Integer JS_AD_CMPNY)"
 *             query="SELECT OBJECT(js) FROM GlJournalSource js WHERE js.jsAdCompany = ?1 ORDER BY js.jsName"
 *
 * @ejb:finder signature="LocalGlJournalSource findByJsName(java.lang.String JS_NM, java.lang.Integer JS_AD_CMPNY)"
 *             query="SELECT OBJECT(js) FROM GlJournalSource js WHERE js.jsName=?1 AND js.jsAdCompany = ?2"
 *
 * @jboss:persistence table-name="GL_JRNL_SRC"
 *
 * @jboss:entity-command name="mysql-get-generated-keys"
 *
 */

public abstract class GlJournalSourceBean extends AbstractEntityBean {


   // Access methods for persistent fields

   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    * @ejb:pk-field
    *
    * @jboss:column-name name="JS_CODE"
    *
    * @jboss:persistence auto-increment="true"
    **/
   public abstract java.lang.Integer getJsCode();
   public abstract void setJsCode(java.lang.Integer JS_CODE);

   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="JS_NM"
    **/
   public abstract java.lang.String getJsName();
   /**
    * @ejb:interface-method view-type="local"
    **/
   public abstract void setJsName(java.lang.String JS_NM);

   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="JS_DESC"
    **/
   public abstract java.lang.String getJsDescription();
   /**
    * @ejb:interface-method view-type="local"
    **/
   public abstract void setJsDescription(java.lang.String JS_DESC);

   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="JS_FRZ_JRNL"
    **/
   public abstract byte getJsFreezeJournal();
   /**
    * @ejb:interface-method view-type="local"
    **/
   public abstract void setJsFreezeJournal(byte JS_FRZ_JRNL);

   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="JS_JRNL_APPRVL"
    **/
   public abstract byte getJsJournalApproval();
   /**
    * @ejb:interface-method view-type="local"
    **/
   public abstract void setJsJournalApproval(byte JS_JRNL_APPRVL);

   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="JS_EFFCTV_DT_RL"
    **/
   public abstract char getJsEffectiveDateRule();
   /**
    * @ejb:interface-method view-type="local"
    **/
   public abstract void setJsEffectiveDateRule(char JS_EFFCTV_DT_RL);
   
   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="JS_AD_CMPNY"
    **/
   public abstract Integer getJsAdCompany();
   /**
    * @ejb:interface-method view-type="local"
    **/
   public abstract void setJsAdCompany(Integer JS_AD_CMPNY);

   // Access methods for relationship fields

   /**
    * @ejb:interface-method view-type="local"
    * @ejb:relation name="journalsource-suspenseaccounts"
    *               role-name="journalsource-has-many-suspenseaccounts"
    */
   public abstract Collection getGlSuspenseAccounts();
   public abstract void setGlSuspenseAccounts(Collection glSuspenseAccounts);

   /**
    * @ejb:interface-method view-type="local"
    * @ejb:relation name="journalsource-journals"
    *               role-name="journalsource-has-many-journals"
    */
   public abstract Collection getGlJournals();
   public abstract void setGlJournals(Collection glJournals);

   // Business methods

   /**
    * @ejb:interface-method view-type="local"
    **/
   public void addGlSuspenseAccount(LocalGlSuspenseAccount glSuspenseAccount) {

      Debug.print("GlJournalSourceBean addGlSuspenseAccount");
      try {
         Collection glSuspenseAccounts = getGlSuspenseAccounts();
	 glSuspenseAccounts.add(glSuspenseAccount);
      } catch (Exception ex) {
         throw new EJBException(ex.getMessage());
      }
   }

   /**
    * @ejb:interface-method view-type="local"
    **/
   public void dropGlSuspenseAccount(LocalGlSuspenseAccount glSuspenseAccount) {

      Debug.print("GlJournalSourceBean dropGlSuspenseAccount");
      try {
         Collection glSuspenseAccounts = getGlSuspenseAccounts();
	 glSuspenseAccounts.remove(glSuspenseAccount);
      } catch (Exception ex) {
         throw new EJBException(ex.getMessage());
      }
   }

   /**
    * @ejb:interface-method view-type="local"
    **/
   public void addGlJournal(LocalGlJournal glJournal) {

      Debug.print("GlJournalSourceBean addGlJournal");
      try {
         Collection glJournals = getGlJournals();
	 glJournals.add(glJournal);
      } catch (Exception ex) {
         throw new EJBException(ex.getMessage());
      }
   }

   /**
    * @ejb:interface-method view-type="local"
    **/
   public void dropGlJournal(LocalGlJournal glJournal) {
   
      Debug.print("GlJournalSourceBean dropGlJournal");
      try {
         Collection glJournals = getGlJournals();
	 glJournals.remove(glJournal);
      } catch (Exception ex) {
         throw new EJBException(ex.getMessage());
      }
   }

   // EntityBean methods

   /**
    * @ejb:create-method view-type="local"
    **/
  public java.lang.Integer ejbCreate (java.lang.Integer JS_CODE, java.lang.String JS_NM,
      java.lang.String JS_DESC, byte JS_FRZ_JRNL,
      byte JS_JRNL_APPRVL, char JS_EFFCTV_DT_RL, Integer JS_AD_CMPNY)
      throws CreateException {

      Debug.print("GlJournalSourceBean ejbCreate");
      setJsCode(JS_CODE);
      setJsName(JS_NM);
      setJsDescription(JS_DESC);
      setJsFreezeJournal(JS_FRZ_JRNL);
      setJsJournalApproval(JS_JRNL_APPRVL);
      setJsEffectiveDateRule(JS_EFFCTV_DT_RL);
      setJsAdCompany(JS_AD_CMPNY);
      
      return null;
   }

  /**
    * @ejb:create-method view-type="local"
    **/
   public java.lang.Integer ejbCreate (java.lang.String JS_NM,
   	java.lang.String JS_DESC, byte JS_FRZ_JRNL,
	byte JS_JRNL_APPRVL, char JS_EFFCTV_DT_RL, Integer JS_AD_CMPNY)
	throws CreateException {

	Debug.print("GlJournalSourceBean ejbCreate");
	
	setJsName(JS_NM);
	setJsDescription(JS_DESC);
	setJsFreezeJournal(JS_FRZ_JRNL);
	setJsJournalApproval(JS_JRNL_APPRVL);
	setJsEffectiveDateRule(JS_EFFCTV_DT_RL);
	setJsAdCompany(JS_AD_CMPNY);
	
	return null;
   }
						     

   public void ejbPostCreate (java.lang.Integer JS_CODE, java.lang.String JS_NM,
      java.lang.String JS_DESC, byte JS_FRZ_JRNL,
      byte JS_JRNL_APPRVL, char JS_EFFCTV_DT_RL, Integer JS_AD_CMPNY)
      throws CreateException { }

   public void ejbPostCreate (java.lang.String JS_NM,
         java.lang.String JS_DESC, byte JS_FRZ_JRNL,
	 byte JS_JRNL_APPRVL, char JS_EFFCTV_DT_RL, Integer JS_AD_CMPNY)
	 throws CreateException { }
		     
} // GlJournalSourceBean class
