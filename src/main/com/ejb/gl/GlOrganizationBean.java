package com.ejb.gl;

import java.util.Collection;

import javax.ejb.CreateException;
import javax.ejb.EJBException;

import com.util.AbstractEntityBean;
import com.util.Debug;

/**
 * @ejb:bean name="GlOrganizationEJB"
 *           display-name="Organization Entity"
 *           type="CMP"
 *           cmp-version="2.x"
 *           view-type="local"
 *           local-jndi-name="omega-ejb/GlOrganizationEJB"
 *           schema="GlOrganization"
 *           primkey-field="orgCode"
 *       
 * @ejb:pk class="java.lang.Integer"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="gluser"
 *                        role-link="gluserlink"
 *
 * @ejb:permission role-name="gluser"
 *
 * @ejb:interface local-class="com.ejb.gl.LocalGlOrganization"
 *                local-extends="javax.ejb.EJBLocalObject"
 *
 * @ejb:home local-class="com.ejb.gl.LocalGlOrganizationHome"
 *           local-extends="javax.ejb.EJBLocalHome"
 *
 * @ejb:finder signature="Collection findByMasterCode(java.lang.Integer MSTR_CODE, java.lang.Integer ORG_AD_CMPNY)"
 *             query="SELECT OBJECT(org) FROM GlOrganization org WHERE org.orgMasterCode = ?1 AND org.orgAdCompany = ?2"
 *
 * @ejb:finder signature="Collection findOrgAll(java.lang.Integer ORG_AD_CMPNY)"
 *             query="SELECT OBJECT(org) FROM GlOrganization org WHERE org.orgAdCompany = ?1"
 *
 * @ejb:finder signature="LocalGlOrganization findByOrgName(java.lang.String ORG_NM, java.lang.Integer ORG_AD_CMPNY)"
 *             query="SELECT OBJECT(org) FROM GlOrganization org WHERE org.orgName=?1 AND org.orgAdCompany = ?2"
 *
 * @jboss:persistence table-name="GL_ORGNZTN"
 *
 * @jboss:entity-command name="mysql-get-generated-keys"
 *
 */

public abstract class GlOrganizationBean extends AbstractEntityBean {

   
   // Access methods for persistent fields

   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    * @ejb:pk-field
    *
    * @jboss:column-name name="ORG_CODE"
    *
    * @jboss:persistence auto-increment="true"
    **/
   public abstract java.lang.Integer getOrgCode();
   public abstract void setOrgCode(java.lang.Integer ORG_CODE);

   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="ORG_NM"
    **/
   public abstract java.lang.String getOrgName();
   /**
    * @ejb:interface-method view-type="local"
    **/
   public abstract void setOrgName(java.lang.String ORG_NM);

   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="ORG_DESC"
    **/
   public abstract java.lang.String getOrgDescription();
   /**
    * @ejb:interface-method view-type="local"
    **/
   public abstract void setOrgDescription(java.lang.String ORG_DESC);

   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="ORG_MSTR_CODE"
    **/
   public abstract java.lang.Integer getOrgMasterCode();
   /**
    * @ejb:interface-method view-type="local"
    **/
   public abstract void setOrgMasterCode(java.lang.Integer ORG_MSTR_CODE);
   
   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="ORG_AD_CMPNY"
    **/
   public abstract java.lang.Integer getOrgAdCompany();
   /**
    * @ejb:interface-method view-type="local"
    **/
   public abstract void setOrgAdCompany(java.lang.Integer ORG_AD_CMPNY);

   // Access methiods for relationship fields

   /**
    * @ejb:interface-method view-type="local"
    * @ejb:relation name="organization-accountranges"
    *               role-name="organization-has-many-accountranges"
    */
   public abstract Collection getGlAccountRanges();
   public abstract void setGlAccountRanges(Collection glAccountRanges);

   /**
    * @ejb:interface-method view-type="local"
    * @ejb:relation name="organization-responsibilities"
    *               role-name="organization-has-many-responsibilities"
    */
   public abstract Collection getGlResponsibilities();
   public abstract void setGlResponsibilities(Collection glResponsibilities);

   // Business methods

   /**
    * @ejb:interface-method view-type="local"
    **/
   public void addGlAccountRange(LocalGlAccountRange glAccountRange) {

      Debug.print("GlOrganizationBean addGlAccountRange");
      try {
         Collection glAccountRanges = getGlAccountRanges();
	 glAccountRanges.add(glAccountRange);
      } catch (Exception ex) {
         throw new EJBException(ex.getMessage());
      }
   }

   /**
    * @ejb:interface-method view-type="local"
    **/
   public void dropGlAccountRange(LocalGlAccountRange glAccountRange) {

      Debug.print("GlOrganizationBean dropGlAccountRange");
      try {
        Collection glAccountRanges = getGlAccountRanges();
	glAccountRanges.remove(glAccountRange);
      } catch (Exception ex) {
         throw new EJBException(ex.getMessage());
      }
   }
   
   /**
    * @ejb:interface-method view-type="local"
    **/
   public void addGlResponsibility(LocalGlResponsibility glResponsibility) {

      Debug.print("GlOrganizationBean addGlResponsiblity");
      try {
         Collection glResponsibilities = getGlResponsibilities();
	 glResponsibilities.add(glResponsibility);
      } catch (Exception ex) {
         throw new EJBException(ex.getMessage());
      }
   }

   /**
    * @ejb:interface-method view-type="local"
    **/
   public void dropGlResponsibility(LocalGlResponsibility glResponsibility) {

      Debug.print("GlOrganizationBean dropGlResponsibility");
      try {
         Collection glResponsibilities = getGlResponsibilities();
	 glResponsibilities.remove(glResponsibility);
      } catch (Exception ex) {
         throw new EJBException(ex.getMessage());
      }
   }

   // EntityBean methods

   /**
    * @ejb:create-method view-type="local"
    **/
   public java.lang.Integer ejbCreate (java.lang.Integer ORG_CODE, java.lang.String ORG_NM, 
      java.lang.String ORG_DESC, java.lang.Integer ORG_MSTR_CODE, java.lang.Integer ORG_AD_CMPNY)
      throws CreateException {

      Debug.print("GlOrganizationBean ejbCreate");
      setOrgCode(ORG_CODE);
      setOrgName(ORG_NM);
      setOrgDescription(ORG_DESC);
      setOrgMasterCode(ORG_MSTR_CODE);
      setOrgAdCompany(ORG_AD_CMPNY);
      
      return null;
   }

   /**
    * @ejb:create-method view-type="local"
    **/
   public java.lang.Integer ejbCreate (java.lang.String ORG_NM,
      java.lang.String ORG_DESC, java.lang.Integer ORG_MSTR_CODE, java.lang.Integer ORG_AD_CMPNY)
      throws CreateException {

      Debug.print("GlOrganizationBean ejbCreate");
      
      setOrgName(ORG_NM);
      setOrgDescription(ORG_DESC);
      setOrgMasterCode(ORG_MSTR_CODE);
      setOrgAdCompany(ORG_AD_CMPNY);
      
      return null;
   }

   public void ejbPostCreate (java.lang.Integer ORG_CODE, java.lang.String ORG_NM,
      java.lang.String ORG_DESC, java.lang.Integer ORG_MSTR_CODE, java.lang.Integer ORG_AD_CMPNY)
      throws CreateException { }

   public void ejbPostCreate (java.lang.String ORG_NM,
      java.lang.String ORG_DESC, java.lang.Integer ORG_MSTR_CODE, java.lang.Integer ORG_AD_CMPNY)
      throws CreateException { }

} // GlOrganizationBean class
