package com.ejb.gl;

import java.util.Collection;

import javax.ejb.CreateException;
import javax.ejb.EJBException;

import com.util.AbstractEntityBean;
import com.util.Debug;

/**
 * @ejb:bean name="GlPeriodTypeEJB"
 *           display-name="Period Type Entity"
 *           type="CMP"
 *           cmp-version="2.x"
 *           view-type="local"
 *           local-jndi-name="omega-ejb/GlPeriodTypeEJB"
 *           schema="GlPeriodType"
 *           primkey-field="ptCode"
 *       
 * @ejb:pk class="java.lang.Integer"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="gluser"
 *                        role-link="gluserlink"
 *
 * @ejb:permission role-name="gluser"
 *
 * @ejb:interface local-class="com.ejb.gl.LocalGlPeriodType"
 *                local-extends="javax.ejb.EJBLocalObject"
 *
 * @ejb:home local-class="com.ejb.gl.LocalGlPeriodTypeHome"
 *           local-extends="javax.ejb.EJBLocalHome"
 *
 * @ejb:finder signature="Collection findPtAll(java.lang.Integer PT_AD_CMPNY)"
 *             query="SELECT OBJECT(pt) FROM GlPeriodType pt WHERE pt.ptAdCompany = ?1"
 *
 * @jboss:query signature="Collection findPtAll(java.lang.Integer PT_AD_CMPNY)"
 *             query="SELECT OBJECT(pt) FROM GlPeriodType pt WHERE pt.ptAdCompany = ?1 ORDER BY pt.ptName"
 *
 * @ejb:finder signature="LocalGlPeriodType findByPtName(java.lang.String PT_NM, java.lang.Integer PT_AD_CMPNY)"
 *             query="SELECT OBJECT(pt) FROM GlPeriodType pt WHERE pt.ptName=?1 AND pt.ptAdCompany = ?2"
 *
 * @jboss:persistence table-name="GL_PRD_TYP"
 *
 * @jboss:entity-command name="mysql-get-generated-keys"
 *
 */

public abstract class GlPeriodTypeBean extends AbstractEntityBean {

   // Access methods for persistent fields

   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    * @ejb:pk-field
    *
    * @jboss:column-name name="PT_CODE"
    *
    * @jboss:persistence auto-increment="true"
    **/
   public abstract java.lang.Integer getPtCode();
   public abstract void setPtCode(java.lang.Integer PT_CODE);

   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="PT_NM"
    **/
   public abstract java.lang.String getPtName();
   /**
    * @ejb:interface-method view-type="local"
    **/
   public abstract void setPtName(java.lang.String PT_NM);

   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="PT_DESC"
    **/
   public abstract java.lang.String getPtDescription();
   /**
    * @ejb:interface-method view-type="local"
    **/
   public abstract void setPtDescription(java.lang.String PT_DESC);

   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="PT_PRD_PER_YR"
    **/
   public abstract short getPtPeriodPerYear();
   /**
    * @ejb:interface-method view-type="local"
    **/
   public abstract void setPtPeriodPerYear(short PT_PRD_PER_YR);

   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="PT_YR_TYP"
    **/
   public abstract char getPtYearType();
   /**
    * @ejb:interface-method view-type="local"
    **/
   public abstract void setPtYearType(char PT_YR_TYP);
   
   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="PT_AD_CMPNY"
    **/
   public abstract Integer getPtAdCompany();
   /**
    * @ejb:interface-method view-type="local"
    **/
   public abstract void setPtAdCompany(Integer PT_AD_CMPNY);

   // Access methods for relationship fields

   /**
    * @ejb:interface-method view-type="local"
    * @ejb:relation name="periodtype-accountingcalendars"
    *               role-name="periodtype-has-many-accountingcalendars"
    */
   public abstract Collection getGlAccountingCalendars();
   public abstract void setGlAccountingCalendars(Collection glAccountingCalendars);

   // Business methods

   /**
    * @ejb:interface-method view-type="local"
    **/
   public void addGlAccountingCalendar(LocalGlAccountingCalendar glAccountingCalendar) {

      Debug.print("GlPeriodTypeBean addGlAccountingCalendar");
      try {
         Collection glAccountingCalendars = getGlAccountingCalendars();
	 glAccountingCalendars.add(glAccountingCalendar);
      } catch (Exception ex) {
         throw new EJBException(ex.getMessage());
      }
   }

   /**
    * @ejb:interface-method view-type="local"
    **/
   public void dropGlAccountingCalendar(LocalGlAccountingCalendar glAccountingCalendar) {
   
      Debug.print("GlPeriodTypeBean dropGlAccountingCalendar");
      try {
         Collection glAccountingCalendars = getGlAccountingCalendars();
	 glAccountingCalendars.remove(glAccountingCalendar);
      } catch (Exception ex) {
         throw new EJBException(ex.getMessage());
      }
   }

   // EntityBean methods

   /**
    * @ejb:create-method view-type="local"
    **/
  public java.lang.Integer ejbCreate (java.lang.Integer PT_CODE, java.lang.String PT_NM,
      java.lang.String PT_DESC, short PT_PRD_PER_YR, char PT_YR_TYP, Integer PT_AD_CMPNY)
      throws CreateException {

      Debug.print("GlPeriodTypeBean ejbCreate");
      setPtCode(PT_CODE);
      setPtName(PT_NM);
      setPtDescription(PT_DESC);
      setPtPeriodPerYear(PT_PRD_PER_YR);
      setPtYearType(PT_YR_TYP);
      setPtAdCompany(PT_AD_CMPNY);
      
      return null;
   }

  /**
    * @ejb:create-method view-type="local"
    **/
   public java.lang.Integer ejbCreate (java.lang.String PT_NM,
      java.lang.String PT_DESC, short PT_PRD_PER_YR, char PT_YR_TYP, Integer PT_AD_CMPNY)
      throws CreateException {

      Debug.print("GlPeriodTypeBean ejbCreate");
      
      setPtName(PT_NM);
      setPtDescription(PT_DESC);
      setPtPeriodPerYear(PT_PRD_PER_YR);
      setPtYearType(PT_YR_TYP);
      setPtAdCompany(PT_AD_CMPNY);
      
      return null;
   }
   
   public void ejbPostCreate (java.lang.Integer PT_CODE, java.lang.String PT_NM,
      java.lang.String PT_DESC, short PT_PRD_PER_YR, char PT_YR_TYP, Integer PT_AD_CMPNY)
      throws CreateException { }

   public void ejbPostCreate (java.lang.String PT_NM,
      java.lang.String PT_DESC, short PT_PRD_PER_YR, char PT_YR_TYP, Integer PT_AD_CMPNY)
      throws CreateException { }

} // GlPeriodTypeBean class
