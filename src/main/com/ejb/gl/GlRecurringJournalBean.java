package com.ejb.gl;

import java.util.Collection;
import java.util.Date;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;

import com.util.AbstractEntityBean;
import com.util.Debug;

/**
 * @ejb:bean name="GlRecurringJournalEJB"
 *           display-name="Recurring Journal Entity"
 *           type="CMP"
 *           cmp-version="2.x"
 *           view-type="local"
 *           local-jndi-name="omega-ejb/GlRecurringJournalEJB"
 *           schema="GlRecurringJournal"
 *           primkey-field="rjCode"
 *       
 * @ejb:pk class="java.lang.Integer"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="gluser"
 *                        role-link="gluserlink"
 *
 * @ejb:permission role-name="gluser"
 *
 * @ejb:interface local-class="com.ejb.gl.LocalGlRecurringJournal"
 *                local-extends="javax.ejb.EJBLocalObject"
 *
 * @ejb:home local-class="com.ejb.gl.LocalGlRecurringJournalHome"
 *           local-extends="javax.ejb.EJBLocalHome"
 *
 * @ejb:finder signature="Collection findRjAll(java.lang.Integer RJ_AD_CMPNY)"
 *             query="SELECT OBJECT(rj) FROM GlRecurringJournal rj WHERE rj.rjAdCompany = ?1"
 *
 * @ejb:finder signature="LocalGlRecurringJournal findByRjName(java.lang.String RJ_NM, java.lang.Integer RJ_AD_CMPNY)"
 *             query="SELECT OBJECT(rj) FROM GlRecurringJournal rj WHERE rj.rjName=?1 AND rj.rjAdCompany = ?2"
 *
 * @ejb:finder signature="Collection findRjToGenerateByAdUsrNameAndDateAndBrCode(java.lang.String USR_NM, java.util.Date DT, java.lang.Integer RJ_AD_BRNCH, java.lang.Integer RJ_AD_CMPNY)"
 *             query="SELECT OBJECT(rj) FROM GlRecurringJournal rj WHERE (rj.rjUserName1=?1 OR rj.rjUserName2=?1 OR rj.rjUserName3=?1 OR rj.rjUserName4=?1 OR rj.rjUserName5=?1) AND rj.rjNextRunDate <= ?2 AND rj.rjAdBranch = ?3 AND rj.rjAdCompany = ?4"
 *
 * @jboss:query signature="Collection findRjToGenerateByAdUsrNameAndDateAndBrCode(java.lang.String USR_NM, java.util.Date DT, java.lang.Integer RJ_AD_BRNCH, java.lang.Integer RJ_AD_CMPNY)"
 *             query="SELECT OBJECT(rj) FROM GlRecurringJournal rj WHERE (rj.rjUserName1=?1 OR rj.rjUserName2=?1 OR rj.rjUserName3=?1 OR rj.rjUserName4=?1 OR rj.rjUserName5=?1) AND rj.rjNextRunDate <= ?2 AND rj.rjAdBranch = ?3 AND rj.rjAdCompany = ?4"
 *
 * @ejb:finder signature="Collection findByUserName1(java.lang.String USR_NM, java.lang.Integer RJ_AD_CMPNY)"
 *             query="SELECT OBJECT(rj) FROM GlRecurringJournal rj WHERE rj.rjUserName1 = ?1 AND rj.rjAdCompany = ?2"
 * 
 * @ejb:finder signature="Collection findByUserName2(java.lang.String USR_NM, java.lang.Integer RJ_AD_CMPNY)"
 *             query="SELECT OBJECT(rj) FROM GlRecurringJournal rj WHERE rj.rjUserName2 = ?1 AND rj.rjAdCompany = ?2"
 * 
 * @ejb:finder signature="Collection findByUserName3(java.lang.String USR_NM, java.lang.Integer RJ_AD_CMPNY)"
 *             query="SELECT OBJECT(rj) FROM GlRecurringJournal rj WHERE rj.rjUserName3 = ?1 AND rj.rjAdCompany = ?2"
 * 
 * @ejb:finder signature="Collection findByUserName4(java.lang.String USR_NM, java.lang.Integer RJ_AD_CMPNY)"
 *             query="SELECT OBJECT(rj) FROM GlRecurringJournal rj WHERE rj.rjUserName4 = ?1 AND rj.rjAdCompany = ?2"
 * 
 * @ejb:finder signature="Collection findByUserName5(java.lang.String USR_NM, java.lang.Integer RJ_AD_CMPNY)"
 *             query="SELECT OBJECT(rj) FROM GlRecurringJournal rj WHERE rj.rjUserName5 = ?1 AND rj.rjAdCompany = ?2"
 * 
 * @ejb:finder signature="Collection findByJbName(java.lang.String JB_NM, java.lang.Integer RJ_AD_CMPNY)"
 *             query="SELECT OBJECT(rj) FROM GlRecurringJournal rj WHERE rj.glJournalBatch.jbName=?1 AND rj.rjAdCompany = ?2"
 * 
 * @ejb:select signature="Collection ejbSelectGeneric(java.lang.String jbossQl, java.lang.Object[] args)"
 *             result-type-mapping="Local"
 *             method-intf="LocalHome"
 *             query="SELECT OBJECT(rj) FROM GlRecurringJournal rj"
 *
 * @jboss:query signature="Collection ejbSelectGeneric(java.lang.String jbossQl, java.lang.Object[] args)"
 *                         dynamic="true"
 *
 * @ejb:value-object match="*"
 *             name="GlRecurringJournal"
 *
 * @jboss:persistence table-name="GL_RCRRNG_JRNL"
 *
 * @jboss:entity-command name="mysql-get-generated-keys"
 *
 */

public abstract class GlRecurringJournalBean extends AbstractEntityBean {


   // Access methods for persistent fields

   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    * @ejb:pk-field
    *
    * @jboss:column-name name="RJ_CODE"
    *
    * @jboss:persistence auto-increment="true"
    **/
   public abstract java.lang.Integer getRjCode();
   public abstract void setRjCode(java.lang.Integer RJ_CODE);

   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="RJ_NM"
    **/
   public abstract java.lang.String getRjName();
   /**
    * @ejb:interface-method view-type="local"
    **/
   public abstract void setRjName(java.lang.String RJ_NM);

   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="RJ_DESC"
    **/
   public abstract java.lang.String getRjDescription();
   /**
    * @ejb:interface-method view-type="local"
    **/
   public abstract void setRjDescription(java.lang.String RJ_DESC);
   
   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="RJ_USR_NM1"
    **/
   public abstract String getRjUserName1();
   /**
    * @ejb:interface-method view-type="local"
    **/
   public abstract void setRjUserName1(String RJ_USR_NM1);
   
   
   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="RJ_USR_NM2"
    **/
   public abstract String getRjUserName2();
   /**
    * @ejb:interface-method view-type="local"
    **/
   public abstract void setRjUserName2(String RJ_USR_NM2);
   
   
   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="RJ_USR_NM3"
    **/
   public abstract String getRjUserName3();
   /**
    * @ejb:interface-method view-type="local"
    **/
   public abstract void setRjUserName3(String RJ_USR_NM3);
   
   
   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="RJ_USR_NM4"
    **/
   public abstract String getRjUserName4();
   /**
    * @ejb:interface-method view-type="local"
    **/
   public abstract void setRjUserName4(String RJ_USR_NM4);
   
   
   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="RJ_USR_NM5"
    **/
   public abstract String getRjUserName5();
   /**
    * @ejb:interface-method view-type="local"
    **/
   public abstract void setRjUserName5(String RJ_USR_NM5);
   
   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="RJ_SCHDL"
    **/
   public abstract String getRjSchedule();
   /**
    * @ejb:interface-method view-type="local"
    **/
   public abstract void setRjSchedule(String RJ_SCHDL);
   
   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="RJ_NXT_RN_DT"
    **/
   public abstract Date getRjNextRunDate();
   /**
    * @ejb:interface-method view-type="local"
    **/
   public abstract void setRjNextRunDate(Date RJ_NXT_RN_DT);
   
   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="RJ_LST_RN_DT"
    **/
   public abstract Date getRjLastRunDate();
   /**
    * @ejb:interface-method view-type="local"
    **/
   public abstract void setRjLastRunDate(Date RJ_LST_RN_DT);
   
   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="RJ_AD_BRNCH"
    **/
   public abstract Integer getRjAdBranch();
   /**
    * @ejb:interface-method view-type="local"
    **/
   public abstract void setRjAdBranch(Integer RJ_AD_BRNCH);

   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="RJ_AD_CMPNY"
    **/
   public abstract Integer getRjAdCompany();
   /**
    * @ejb:interface-method view-type="local"
    **/
   public abstract void setRjAdCompany(Integer RJ_AD_CMPNY);
      
   // Access methods for relationship fields

   /**
    * @ejb:interface-method view-type="local"
    * @ejb:relation name="recurringjournal-recurringjournallines"
    *               role-name="recurringjournal-has-many-recurringjournallines"
    */
   public abstract Collection getGlRecurringJournalLines();
   public abstract void setGlRecurringJournalLines(Collection glRecurringJournalLines);

   /**
    * @ejb:interface-method view-type="local"
    * @ejb:relation name="journalcategory-recurringjournals"
    *               role-name="recurringjournal-has-one-journalcategory"
    *
    * @jboss:relation related-pk-field="jcCode"
    *                 fk-column="GL_JOURNAL_CATEGORY"
    */
   public abstract LocalGlJournalCategory getGlJournalCategory();
   public abstract void setGlJournalCategory(LocalGlJournalCategory glJournalCategory);
   
   /**
    * @ejb:interface-method view-type="local"
    * @ejb:relation name="journalbatch-recurringjournal"
    *               role-name="recurringjournal-has-one-journalbatch"
    *
    * @jboss:relation related-pk-field="jbCode"
    *                 fk-column="GL_JOURNAL_BATCH"
    */
   public abstract LocalGlJournalBatch getGlJournalBatch();
   public abstract void setGlJournalBatch(LocalGlJournalBatch glJournalBatch);
   
   /**
    * @jboss:dynamic-ql
    */
    public abstract Collection ejbSelectGeneric(java.lang.String jbossQl, java.lang.Object[] args)
       throws FinderException;

   // Business methods
   
   /**
    * @ejb:home-method view-type="local"
    */
    public Collection ejbHomeGetRjByCriteria(java.lang.String jbossQl, java.lang.Object[] args)
       throws FinderException {
       	
       return ejbSelectGeneric(jbossQl, args);
    }

   // Business methods

   /**
    * @ejb:interface-method view-type="local"
    **/
   public void addGlRecurringJournalLine(LocalGlRecurringJournalLine glRecurringJournalLine) {

      Debug.print("GlRecurringJournalBean addGlRecurringJournalLine");
      try {
         Collection glRecurringJournalLines = getGlRecurringJournalLines();
	 glRecurringJournalLines.add(glRecurringJournalLine);
      } catch (Exception ex) {
         throw new EJBException(ex.getMessage());
      }
   }

   /**
    * @ejb:interface-method view-type="local"
    **/
   public void dropGlRecurringJournalLine(LocalGlRecurringJournalLine glRecurringJournalLine) {

      Debug.print("GlRecurringJournalBean dropGlRecurringJournalLine");
      try {
         Collection glRecurringJournalLines = getGlRecurringJournalLines();
	 glRecurringJournalLines.remove(glRecurringJournalLine);
      } catch (Exception ex) {
         throw new EJBException(ex.getMessage());
      }
   }

   // EntityBean methods

   /**
    * @ejb:create-method view-type="local"
    **/
   public java.lang.Integer ejbCreate (java.lang.Integer RJ_CODE, java.lang.String RJ_NM, java.lang.String RJ_DESC,
      String RJ_USR_NM1, String RJ_USR_NM2, String RJ_USR_NM3, 
      String RJ_USR_NM4, String RJ_USR_NM5, String RJ_SCHDL, Date RJ_NXT_RN_DT,
      Date RJ_LST_RN_DT, Integer RJ_AD_BRNCH, Integer RJ_AD_CMPNY)
      throws CreateException {

      Debug.print("GlRecurringJournalBean ejbCreate");
      setRjCode(RJ_CODE);
      setRjName(RJ_NM);
      setRjDescription(RJ_DESC);
      setRjUserName1(RJ_USR_NM1);
      setRjUserName2(RJ_USR_NM2);
      setRjUserName3(RJ_USR_NM3);
      setRjUserName4(RJ_USR_NM4);
      setRjUserName5(RJ_USR_NM5);
      setRjSchedule(RJ_SCHDL);
      setRjNextRunDate(RJ_NXT_RN_DT);
      setRjLastRunDate(RJ_LST_RN_DT);
      setRjAdBranch(RJ_AD_BRNCH);
      setRjAdCompany(RJ_AD_CMPNY);
      
      return null;
   }

   /**
    * @ejb:create-method view-type="local"
    **/
   public java.lang.Integer ejbCreate (java.lang.String RJ_NM, java.lang.String RJ_DESC,
      String RJ_USR_NM1, String RJ_USR_NM2, String RJ_USR_NM3, 
      String RJ_USR_NM4, String RJ_USR_NM5, String RJ_SCHDL, Date RJ_NXT_RN_DT,
      Date RJ_LST_RN_DT, Integer RJ_AD_BRNCH, Integer RJ_AD_CMPNY)
      throws CreateException {

      Debug.print("GlRecurringJournalBean ejbCreate");
      
      setRjName(RJ_NM);
      setRjDescription(RJ_DESC);
      setRjUserName1(RJ_USR_NM1);
      setRjUserName2(RJ_USR_NM2);
      setRjUserName3(RJ_USR_NM3);
      setRjUserName4(RJ_USR_NM4);
      setRjUserName5(RJ_USR_NM5);
      setRjSchedule(RJ_SCHDL);
      setRjNextRunDate(RJ_NXT_RN_DT);
      setRjLastRunDate(RJ_LST_RN_DT);
      setRjAdBranch(RJ_AD_BRNCH);
      setRjAdCompany(RJ_AD_CMPNY);
      
      return null;
   }
   
   public void ejbPostCreate (java.lang.Integer RJ_CODE, java.lang.String RJ_NM, java.lang.String RJ_DESC,
      String RJ_USR_NM1, String RJ_USR_NM2, String RJ_USR_NM3, 
      String RJ_USR_NM4, String RJ_USR_NM5, String RJ_SCHDL, Date RJ_NXT_RN_DT,
      Date RJ_LST_RN_DT, Integer RJ_AD_BRNCH, Integer RJ_AD_CMPNY)
     throws CreateException { }

   public void ejbPostCreate (java.lang.String RJ_NM, java.lang.String RJ_DESC,
      String RJ_USR_NM1, String RJ_USR_NM2, String RJ_USR_NM3, 
      String RJ_USR_NM4, String RJ_USR_NM5, String RJ_SCHDL, Date RJ_NXT_RN_DT,
      Date RJ_LST_RN_DT, Integer RJ_AD_BRNCH, Integer RJ_AD_CMPNY)
     throws CreateException { }
} // GlRecurringJournalBean.java
