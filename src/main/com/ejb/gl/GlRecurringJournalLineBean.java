package com.ejb.gl;

import javax.ejb.CreateException;

import com.util.AbstractEntityBean;
import com.util.Debug;

/**
 * @ejb:bean name="GlRecurringJournalLineEJB"
 *           display-name="Recurring Journal Line Entity"
 *           type="CMP"
 *           cmp-version="2.x"
 *           view-type="local"
 *           local-jndi-name="omega-ejb/GlRecurringJournalLineEJB"
 *           schema="GlRecurringJournalLine"
 *           primkey-field="rjlCode"
 *       
 * @ejb:pk class="java.lang.Integer"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="gluser"
 *                        role-link="gluserlink"
 *
 * @ejb:permission role-name="gluser"
 *
 * @ejb:interface local-class="com.ejb.gl.LocalGlRecurringJournalLine"
 *                local-extends="javax.ejb.EJBLocalObject"
 *
 * @ejb:home local-class="com.ejb.gl.LocalGlRecurringJournalLineHome"
 *           local-extends="javax.ejb.EJBLocalHome"
 *
 * @ejb:finder signature="Collection findRjlAll(java.lang.Integer RJL_AD_CMPNY)"
 *             query="SELECT OBJECT(rjl) FROM GlRecurringJournalLine rjl WHERE rjl.rjlAdCompany = ?1"
 *
 * @ejb:finder signature="LocalGlRecurringJournalLine findByRjlLineNumber(short RJL_LN_NMBR, java.lang.Integer RJL_AD_CMPNY)"
 *             query="SELECT OBJECT(rjl) FROM GlRecurringJournalLine rjl WHERE rjl.rjlLineNumber=?1 AND rjl.rjlAdCompany = ?2"
 *
 * @ejb:finder signature="Collection findByRjCode(java.lang.Integer RJ_CODE, java.lang.Integer RJL_AD_CMPNY)"
 *             query="SELECT OBJECT(rjl) FROM GlRecurringJournal rj, IN(rj.glRecurringJournalLines) rjl WHERE rj.rjCode = ?1 AND rjl.rjlAdCompany = ?2"
 *
 * @jboss:query signature="Collection findByRjCode(java.lang.Integer RJ_CODE, java.lang.Integer RJL_AD_CMPNY)"
 *             query="SELECT OBJECT(rjl) FROM GlRecurringJournal rj, IN(rj.glRecurringJournalLines) rjl WHERE rj.rjCode = ?1 AND rjl.rjlAdCompany = ?2 ORDER BY rjl.rjlDebit DESC"
 *
 * @ejb:value-object match="*"
 *             name="GlRecurringJournalLine"
 *
 * @jboss:persistence table-name="GL_RCRRNG_JRNL_LN"
 *
 * @jboss:entity-command name="mysql-get-generated-keys"
 *
 */

public abstract class GlRecurringJournalLineBean extends AbstractEntityBean {

      
   // Access methods for persistent fields

   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    * @ejb:pk-field
    *
    * @jboss:column-name name="RJL_CODE"
    *
    * @jboss:persistence auto-increment="true"
    **/
   public abstract java.lang.Integer getRjlCode();
   public abstract void setRjlCode(java.lang.Integer RJL_CODE);

   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="RJL_LN_NMBR"
    **/
   public abstract short getRjlLineNumber();
   /**
    * @ejb:interface-method view-type="local"
    **/
   public abstract void setRjlLineNumber(short RJL_LN_NMBR);

   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="RJL_DBT"
    **/
   public abstract byte getRjlDebit();
   /**
    * @ejb:interface-method view-type="local"
    **/
   public abstract void setRjlDebit(byte RJL_DBT);

   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="RJL_AMNT"
    **/
   public abstract double getRjlAmount();
   /**
    * @ejb:interface-method view-type="local"
    **/
   public abstract void setRjlAmount(double RJL_AMNT);
   
   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="RJL_AD_CMPNY"
    **/
   public abstract Integer getRjlAdCompany();
   /**
    * @ejb:interface-method view-type="local"
    **/
   public abstract void setRjlAdCompany(Integer RJL_AD_CMPNY);

   // Access methods for relationship fields

   /**
    * @ejb:interface-method view-type="local"
    * @ejb:relation name="recurringjournal-recurringjournallines"
    *               role-name="recurringjournalline-has-one-recurringjournal"
    *               cascade-delete="yes"
    *
    * @jboss:relation related-pk-field="rjCode"
    *                 fk-column="GL_RECURRING_JOURNAL"
    */
   public abstract LocalGlRecurringJournal getGlRecurringJournal();
   public abstract void setGlRecurringJournal(LocalGlRecurringJournal glRecurringJournal);

   /**
    * @ejb:interface-method view-type="local"
    * @ejb:relation name="chartofaccount-recurringjournallines"
    *               role-name="recurringjournalline-has-one-chartofaccount"
    *
    * @jboss:relation related-pk-field="coaCode"
    *                 fk-column="GL_CHART_OF_ACCOUNT"
    */
   public abstract LocalGlChartOfAccount getGlChartOfAccount();
   public abstract void setGlChartOfAccount(LocalGlChartOfAccount glChartOfAccount);

   // No Business methods

   // EntityBean methods

   /**
    * @ejb:create-method view-type="local"
    **/
   public java.lang.Integer ejbCreate (java.lang.Integer RJL_CODE, short RJL_LN_NMBR,
      byte RJL_DBT, double RJL_AMNT, Integer RJL_AD_CMPNY)
      throws CreateException {
      
      Debug.print("GlRecurringJournalLineBean ejbCreate");
      setRjlCode(RJL_CODE);
      setRjlLineNumber(RJL_LN_NMBR);
      setRjlDebit(RJL_DBT);
      setRjlAmount(RJL_AMNT);
      setRjlAdCompany(RJL_AD_CMPNY);
      
      return null;
   }
   
   /**
    * @ejb:create-method view-type="local"
    **/
   public java.lang.Integer ejbCreate (short RJL_LN_NMBR,
      byte RJL_DBT, double RJL_AMNT, Integer RJL_AD_CMPNY)
      throws CreateException {
      
      Debug.print("GlRecurringJournalLineBean ejbCreate");
      
      setRjlLineNumber(RJL_LN_NMBR);
      setRjlDebit(RJL_DBT);
      setRjlAmount(RJL_AMNT);
      setRjlAdCompany(RJL_AD_CMPNY);
      
      return null;
   }

   public void ejbPostCreate (java.lang.Integer RJL_CODE, short RJL_LN_NMBR,
      byte RJL_DBT, double RJL_AMNT, Integer RJL_AD_CMPNY)
      throws CreateException { }
   
   public void ejbPostCreate (short RJL_LN_NMBR,
      byte RJL_DBT, double RJL_AMNT, Integer RJL_AD_CMPNY)
      throws CreateException { }

} // GlRecurringJournalLineBean class
