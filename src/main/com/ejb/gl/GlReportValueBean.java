package com.ejb.gl;

import java.util.Collection;

import javax.ejb.CreateException;
import javax.ejb.EJBException;

import com.ejb.ar.LocalArInvoice;
import com.util.AbstractEntityBean;
import com.util.Debug;

/**
 * @ejb:bean name="GlReportValueEJB"
 *           display-name="Report Value Entity"
 *           type="CMP"
 *           cmp-version="2.x"
 *           view-type="local"
 *           local-jndi-name="omega-ejb/GlReportValueEJB"
 *           schema="GlReportValue"
 *           primkey-field="rvCode"
 *
 * @ejb:pk class="java.lang.Integer"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="gluser"
 *                        role-link="gluserlink"
 *
 * @ejb:permission role-name="gluser"
 *
 * @ejb:interface local-class="com.ejb.gl.LocalGlReportValue"
 *                local-extends="javax.ejb.EJBLocalObject"
 *
 * @ejb:home local-class="com.ejb.gl.LocalGlReportValueHome"
 *           local-extends="javax.ejb.EJBLocalHome"
 *
 * @ejb:finder signature="Collection findRvAll(java.lang.Integer PT_AD_CMPNY)"
 *             query="SELECT OBJECT(rv) FROM GlReportValue rv WHERE rv.rvAdCompany = ?1"
 *
 * @jboss:query signature="Collection findRvAll(java.lang.Integer PT_AD_CMPNY)"
 *             query="SELECT OBJECT(rv) FROM GlReportValue rv WHERE rv.rvAdCompany = ?1 ORDER BY rv.rvType"
 *
 * @ejb:finder signature="Collection findRvByRvType(java.lang.String RV_TYP, java.lang.Integer PT_AD_CMPNY)"
 *             query="SELECT OBJECT(rv) FROM GlReportValue rv WHERE rv.rvType = ?1 AND rv.rvAdCompany = ?2"
 *
 * @jboss:query signature="Collection findRvByRvType(java.lang.String RV_TYP, java.lang.Integer PT_AD_CMPNY)"
 *             query="SELECT OBJECT(rv) FROM GlReportValue rv WHERE rv.rvType = ?1 AND rv.rvAdCompany = ?2 ORDER BY rv.rvType"
 *
 * @ejb:value-object match="*"
 *             name="GlReportValue"
 *
 * @jboss:persistence table-name="GL_RPRT_VL"
 *
 * @jboss:entity-command name="mysql-get-generated-keys"
 *
 */

public abstract class GlReportValueBean extends AbstractEntityBean {

   // Access methods for persistent fields

   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    * @ejb:pk-field
    *
    * @jboss:column-name name="RV_CODE"
    *
    * @jboss:persistence auto-increment="true"
    **/
   public abstract java.lang.Integer getRvCode();
   public abstract void setRvCode(java.lang.Integer RV_CODE);

   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="RV_TYP"
    **/
   public abstract java.lang.String getRvType();
   /**
    * @ejb:interface-method view-type="local"
    **/
   public abstract void setRvType(java.lang.String RV_TYP);

   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="RV_PRMTR"
    **/
   public abstract java.lang.String getRvParameter();
   /**
    * @ejb:interface-method view-type="local"
    **/
   public abstract void setRvParameter(java.lang.String RV_PRMTR);

   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="RV_DESC"
    **/
   public abstract java.lang.String getRvDescription();
   /**
    * @ejb:interface-method view-type="local"
    **/
   public abstract void setRvDescription(java.lang.String RV_DESC);

   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="RV_DFLT_VL"
    **/
   public abstract java.lang.String getRvDefaultValue();
   /**
    * @ejb:interface-method view-type="local"
    **/
   public abstract void setRvDefaultValue(java.lang.String RV_DFLT_VL);

   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="RV_AD_CMPNY"
    **/
   public abstract Integer getRvAdCompany();
   /**
    * @ejb:interface-method view-type="local"
    **/
   public abstract void setRvAdCompany(Integer RV_AD_CMPNY);

   // Access methods for relationship fields

   /**
    * @ejb:interface-method view-type="local"
    * @ejb:relation name="chartofaccount-reportvalues"
    *               role-name="reportvalue-has-one-chartofaccount"
    *               cascade-delete="yes"
    *
    * @jboss:relation related-pk-field="coaCode"
    *                 fk-column="GL_CHART_OF_ACCOUNT"
    */
   public abstract LocalGlChartOfAccount getGlChartOfAccount();
   /**
    * @ejb:interface-method view-type="local"
    **/
   public abstract void setGlChartOfAccount(LocalGlChartOfAccount glChartOfAccount);

   // EntityBean methods

   /**
    * @ejb:create-method view-type="local"
    **/
  public java.lang.Integer ejbCreate (java.lang.Integer RV_CODE, java.lang.String RV_TYP,
      java.lang.String RV_PRMTR, String RV_DESC, String RV_DFLT_VL, Integer RV_AD_CMPNY)
      throws CreateException {

      Debug.print("GlReportValueBean ejbCreate");
      setRvCode(RV_CODE);
      setRvType(RV_TYP);
      setRvParameter(RV_PRMTR);
      setRvDescription(RV_DESC);
      setRvDefaultValue(RV_DFLT_VL);
      setRvAdCompany(RV_AD_CMPNY);

      return null;
   }

  /**
    * @ejb:create-method view-type="local"
    **/
  	public java.lang.Integer ejbCreate (java.lang.String RV_TYP,
	      java.lang.String RV_PRMTR, String RV_DESC, String RV_DFLT_VL, Integer RV_AD_CMPNY)
      throws CreateException {

      Debug.print("GlReportValueBean ejbCreate");
      setRvType(RV_TYP);
      setRvParameter(RV_PRMTR);
      setRvDescription(RV_DESC);
      setRvDefaultValue(RV_DFLT_VL);
      setRvAdCompany(RV_AD_CMPNY);

      return null;
   }

   public void ejbPostCreate (java.lang.Integer RV_CODE, java.lang.String RV_TYP,
		      java.lang.String RV_PRMTR, String RV_DESC, String RV_DFLT_VL, Integer RV_AD_CMPNY)
      throws CreateException { }

   public void ejbPostCreate (java.lang.String RV_TYP,
		      java.lang.String RV_PRMTR, String RV_DESC, String RV_DFLT_VL, Integer RV_AD_CMPNY)
      throws CreateException { }

} // GlPeriodTypeBean class
