package com.ejb.gl;

import javax.ejb.CreateException;

import com.util.AbstractEntityBean;
import com.util.Debug;

/**
 * @ejb:bean name="GlResponsibilityEJB"
 *           display-name="Responsibility Entity"
 *           type="CMP"
 *           cmp-version="2.x"
 *           view-type="local"
 *           local-jndi-name="omega-ejb/GlResponsibilityEJB"
 *           schema="GlResponsibility"
 *           primkey-field="resCode"
 *       
 * @ejb:pk class="java.lang.Integer"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="gluser"
 *                        role-link="gluserlink"
 *
 * @ejb:permission role-name="gluser"
 *
 * @ejb:interface local-class="com.ejb.gl.LocalGlResponsibility"
 *                local-extends="javax.ejb.EJBLocalObject"
 *
 * @ejb:home local-class="com.ejb.gl.LocalGlResponsibilityHome"
 *           local-extends="javax.ejb.EJBLocalHome"
 *
 * @jboss:persistence table-name="GL_RSPNSBLTY"
 * 
 */

public abstract class GlResponsibilityBean extends AbstractEntityBean {

   // Access methods for persistent fields

   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    * @ejb:pk-field
    *
    * @jboss:column-name name="RES_CODE"
    *
    **/
   public abstract java.lang.Integer getResCode();
   public abstract void setResCode(java.lang.Integer RES_CODE);

   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="RES_ENBL"
    **/
   public abstract byte getResEnable();
   /**
    * @ejb:interface-method view-type="local"
    **/
   public abstract void setResEnable(byte RES_ENBL);
   
   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="RES_AD_CMPNY"
    **/
   public abstract Integer getResAdCompany();
   /**
    * @ejb:interface-method view-type="local"
    **/
   public abstract void setResAdCompany(Integer RES_AD_CMPNY);

   // Access methods for relationship fields 

   /**
    * @ejb:interface-method view-type="local"
    * @ejb:relation name="organization-responsibilities"
    *               role-name="responsibility-has-one-organization"
    *               cascade-delete="yes"
    *
    * @jboss:relation related-pk-field="orgCode"
    *                 fk-column="GL_ORGANIZATION"
    */
   public abstract LocalGlOrganization getGlOrganization();
   public abstract void setGlOrganization(LocalGlOrganization glOrganization);

   // No Business methods

   // EntityBean methods

   /**
    * @ejb:create-method view-type="local"
    **/
   public java.lang.Integer ejbCreate (java.lang.Integer RES_CODE, byte RES_ENBL,
   		Integer RES_AD_CMPNY)
      throws CreateException {

      Debug.print("GlResponsibilityBean ejbCreate");
      setResCode(RES_CODE);
      setResEnable(RES_ENBL);
      setResAdCompany(RES_AD_CMPNY);
      
      return null;
   }

   public void ejbPostCreate (java.lang.Integer RES_CODE, byte RES_ENBL,
   		Integer RES_AD_CMPNY)
      throws CreateException { }

} // GlResponsibilityBean class
