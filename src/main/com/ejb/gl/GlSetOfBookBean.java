package com.ejb.gl;

import javax.ejb.CreateException;

import com.util.AbstractEntityBean;
import com.util.Debug;

/**
 * @ejb:bean name="GlSetOfBookEJB"
 *           display-name="Set Of Book Entity"
 *           type="CMP"
 *           cmp-version="2.x"
 *           view-type="local"
 *           local-jndi-name="omega-ejb/GlSetOfBookEJB"
 *           schema="GlSetOfBook"
 *           primkey-field="sobCode"
 *       
 * @ejb:pk class="java.lang.Integer"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="gluser"
 *                        role-link="gluserlink"
 *
 * @ejb:permission role-name="gluser"
 *
 * @ejb:interface local-class="com.ejb.gl.LocalGlSetOfBook"
 *                local-extends="javax.ejb.EJBLocalObject"
 *
 * @ejb:home local-class="com.ejb.gl.LocalGlSetOfBookHome"
 *           local-extends="javax.ejb.EJBLocalHome"
 *
 * @ejb:finder signature="Collection findSobAll(java.lang.Integer SOB_AD_CMPNY)"
 *             query="SELECT OBJECT(sob) FROM GlSetOfBook sob WHERE sob.sobAdCompany = ?1"
 *
 * @jboss:query signature="Collection findSobAll(java.lang.Integer SOB_AD_CMPNY)"
 *             query="SELECT OBJECT(sob) FROM GlSetOfBook sob WHERE sob.sobAdCompany = ?1 ORDER BY sob.glAccountingCalendar.acYear"
 *
 * @ejb:finder signature="LocalGlSetOfBook findByAcCode(java.lang.Integer AC_CODE, java.lang.Integer SOB_AD_CMPNY)"
 *             query="SELECT OBJECT(sob) FROM GlSetOfBook sob WHERE sob.glAccountingCalendar.acCode=?1 AND sob.sobAdCompany = ?2"
 *
 * @ejb:finder signature="LocalGlSetOfBook findByTcCode(java.lang.Integer TC_CODE, java.lang.Integer SOB_AD_CMPNY)"
 *             query="SELECT OBJECT(sob) FROM GlSetOfBook sob WHERE sob.glTransactionCalendar.tcCode=?1 AND sob.sobAdCompany = ?2"
 * 
 * @ejb:finder signature="LocalGlSetOfBook findLatestTransactionalCalendar(java.lang.Integer SOB_AD_CMPNY)"
 *             query="SELECT OBJECT(sob) FROM GlSetOfBook sob WHERE sob.sobAdCompany = ?1"
 *
 * @ejb:query signature="LocalGlSetOfBook findLatestTransactionalCalendar(java.lang.Integer SOB_AD_CMPNY)"
 *             query="SELECT OBJECT(sob) FROM GlSetOfBook sob WHERE sob.sobAdCompany = ?1 ORDER BY sob.glTransactionCalendar.tcCode DESC LIMIT 1"
 *  
 * @ejb:finder signature="LocalGlSetOfBook findByDate(java.util.Date DT, java.lang.Integer SOB_AD_CMPNY)"
 *             query="SELECT OBJECT(sob) FROM GlSetOfBook sob, IN(sob.glAccountingCalendar.glAccountingCalendarValues) acv WHERE acv.acvDateFrom <= ?1 AND acv.acvDateTo >= ?1 AND sob.sobAdCompany = ?2"
 *
 * @jboss:query signature="LocalGlSetOfBook findByDate(java.util.Date DT, java.lang.Integer SOB_AD_CMPNY)"
 *             query="SELECT OBJECT(sob) FROM GlSetOfBook sob, IN(sob.glAccountingCalendar.glAccountingCalendarValues) acv WHERE acv.acvDateFrom <= ?1 AND acv.acvDateTo >= ?1 AND sob.sobAdCompany = ?2"
 *
 * @ejb:finder signature="Collection findBySobYearEndClosed(byte SOB_YR_END_CLSD, java.lang.Integer SOB_AD_CMPNY)"
 *             query="SELECT OBJECT(sob) FROM GlSetOfBook sob WHERE sob.sobYearEndClosed = ?1 AND sob.sobAdCompany = ?2"
 *
 * @jboss:query signature="Collection findBySobYearEndClosed(byte SOB_YR_END_CLSD, java.lang.Integer SOB_AD_CMPNY)"
 *             query="SELECT OBJECT(sob) FROM GlSetOfBook sob WHERE sob.sobYearEndClosed = ?1 AND sob.sobAdCompany = ?2 ORDER BY sob.glAccountingCalendar.acYear"
 *
 * @ejb:finder signature="Collection findByAcvPeriodPrefixAndDate(java.lang.String ACV_PRD_PFX, java.util.Date DT, java.lang.Integer SOB_AD_CMPNY)"
 *             query="SELECT OBJECT(sob) FROM GlSetOfBook sob, IN(sob.glAccountingCalendar.glAccountingCalendarValues) acv WHERE acv.acvPeriodPrefix = ?1 AND acv.acvDateTo >= ?2 AND sob.sobAdCompany = ?3"
 *
 * @jboss:query signature="Collection findByAcvPeriodPrefixAndDate(java.lang.String ACV_PRD_PFX, java.util.Date DT, java.lang.Integer SOB_AD_CMPNY)"
 *             query="SELECT OBJECT(sob) FROM GlSetOfBook sob, IN(sob.glAccountingCalendar.glAccountingCalendarValues) acv WHERE acv.acvPeriodPrefix = ?1 AND acv.acvDateTo >= ?2 AND sob.sobAdCompany = ?3 ORDER BY sob.glAccountingCalendar.acYear"
 * 
 * @ejb:finder signature="Collection findSubsequentSobByAcYear(java.lang.Integer AC_YR, java.lang.Integer SOB_AD_CMPNY)"
 *             query="SELECT OBJECT(sob) FROM GlSetOfBook sob WHERE sob.glAccountingCalendar.acYear > ?1 AND sob.sobAdCompany = ?2"
 * 
 * @jboss:query signature="Collection findSubsequentSobByAcYear(java.lang.Integer AC_YR, java.lang.Integer SOB_AD_CMPNY)"
 *             query="SELECT OBJECT(sob) FROM GlSetOfBook sob WHERE sob.glAccountingCalendar.acYear > ?1 AND sob.sobAdCompany = ?2 ORDER BY sob.glAccountingCalendar.acYear"
 *
 *  @ejb:finder signature="Collection findPrecedingSobByAcYear(java.lang.Integer AC_YR, java.lang.Integer SOB_AD_CMPNY)"
 *             query="SELECT OBJECT(sob) FROM GlSetOfBook sob WHERE sob.glAccountingCalendar.acYear < ?1 AND sob.sobAdCompany = ?2"
 * 
 * @jboss:query signature="Collection findPrecedingSobByAcYear(java.lang.Integer AC_YR, java.lang.Integer SOB_AD_CMPNY)"
 *             query="SELECT OBJECT(sob) FROM GlSetOfBook sob WHERE sob.glAccountingCalendar.acYear < ?1 AND sob.sobAdCompany = ?2 ORDER BY sob.glAccountingCalendar.acYear DESC"
 * 
 * @ejb:finder signature="LocalGlSetOfBook findByAcYear(java.lang.Integer AC_YR, java.lang.Integer SOB_AD_CMPNY)"
 *             query="SELECT OBJECT(sob) FROM GlSetOfBook sob WHERE sob.glAccountingCalendar.acYear = ?1 AND sob.sobAdCompany = ?2"
 * 
 * @ejb:finder signature="Collection findBySobYearEndClosedAndGreaterThanAcYear(byte SOB_YR_END_CLSD, java.lang.Integer AC_YR, java.lang.Integer SOB_AD_CMPNY)"
 *             query="SELECT OBJECT(sob) FROM GlSetOfBook sob WHERE sob.sobYearEndClosed = ?1 AND sob.glAccountingCalendar.acYear > ?2 AND sob.sobAdCompany = ?3"
 * 
 * @jboss:query signature="Collection findBySobYearEndClosedAndGreaterThanAcYear(byte SOB_YR_END_CLSD, java.lang.Integer AC_YR, java.lang.Integer SOB_AD_CMPNY)"
 *             query="SELECT OBJECT(sob) FROM GlSetOfBook sob WHERE sob.sobYearEndClosed = ?1 AND sob.glAccountingCalendar.acYear > ?2 AND sob.sobAdCompany = ?3 ORDER BY sob.glAccountingCalendar.acYear"
 *
 * @jboss:persistence table-name="GL_SOB"
 *
 * @jboss:entity-command name="mysql-get-generated-keys"
 *
 */

public abstract class GlSetOfBookBean extends AbstractEntityBean {

   
   // Access methods for persistent fields

   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    * @ejb:pk-field
    *
    * @jboss:column-name name="SOB_CODE"
    *
    * @jboss:persistence auto-increment="true"
    **/
   public abstract java.lang.Integer getSobCode();
   public abstract void setSobCode(java.lang.Integer SOB_CODE);

   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="SOB_YR_END_CLSD"
    **/
   public abstract byte getSobYearEndClosed();
   /**
    * @ejb:interface-method view-type="local"
    **/
   public abstract void setSobYearEndClosed(byte SOB_YR_END_CLSD);
   
   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="SOB_AD_CMPNY"
    **/
   public abstract Integer getSobAdCompany();
   /**
    * @ejb:interface-method view-type="local"
    **/
   public abstract void setSobAdCompany(Integer SOB_AD_CMPNY);


   // Access methods for relationship fields

   /**
    * @ejb:interface-method view-type="local"
    * @ejb:relation name="accountingcalendar-setofbooks"
    *               role-name="setofbook-has-one-accountingcalendar"
    *
    * @jboss:relation related-pk-field="acCode"
    *                 fk-column="GL_ACCOUNTING_CALENDAR"
    */
   public abstract LocalGlAccountingCalendar getGlAccountingCalendar();
   /**
    * @ejb:interface-method view-type="local"
    **/
   public abstract void setGlAccountingCalendar(LocalGlAccountingCalendar glAccountingCalendar);


   /**
    * @ejb:interface-method view-type="local"
    * @ejb:relation name="transactioncalendar-setofbooks"
    *               role-name="setofbook-has-one-transactioncalendar"
    *
    * @jboss:relation related-pk-field="tcCode"
    *                 fk-column="GL_TRANSACTION_CALENDAR"
    */
   public abstract LocalGlTransactionCalendar getGlTransactionCalendar();
   /**
    * @ejb:interface-method view-type="local"
    **/
   public abstract void setGlTransactionCalendar(LocalGlTransactionCalendar glTransactionCalendar);
   
   

   // No Business methods

   // EntityBean methods

   /**
    * @ejb:create-method view-type="local"
    **/
  public java.lang.Integer ejbCreate (java.lang.Integer SOB_CODE, byte SOB_YR_END_CLSD,
  		Integer SOB_AD_CMPNY)
      throws CreateException {

      Debug.print("GlSetOfBookBean ejbCreate");
      
      setSobCode(SOB_CODE);
      setSobYearEndClosed(SOB_YR_END_CLSD);
      setSobAdCompany(SOB_AD_CMPNY);
      
      return null;
   }
   
  /**
    * @ejb:create-method view-type="local"
    **/
   public java.lang.Integer ejbCreate (byte SOB_YR_END_CLSD,
   		Integer SOB_AD_CMPNY)
      throws CreateException {
      
      Debug.print("GlSetOfBookBean ejbCreate");
      
      setSobYearEndClosed(SOB_YR_END_CLSD);
      setSobAdCompany(SOB_AD_CMPNY);
      
      return null;
   }   
   
   public void ejbPostCreate (java.lang.Integer SOB_CODE, byte SOB_YR_END_CLSD,
   		Integer SOB_AD_CMPNY)
      throws CreateException { }

   public void ejbPostCreate (byte SOB_YR_END_CLSD,
   		Integer SOB_AD_CMPNY)
      throws CreateException { }

} // GlSetOfBookBean class
