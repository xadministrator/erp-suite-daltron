package com.ejb.gl;

import java.util.Collection;
import java.util.Date;

import javax.ejb.CreateException;
import javax.ejb.EJBException;

import com.util.AbstractEntityBean;
import com.util.Debug;

/**
 * @ejb:bean name="GlStaticReportEJB"
 *           display-name="Static Report Entity"
 *           type="CMP"
 *           cmp-version="2.x"
 *           view-type="local"
 *           local-jndi-name="omega-ejb/GlStaticReportEJB"
 *           schema="GlStaticReport"
 *           primkey-field="srCode"
 *       
 * @ejb:pk class="java.lang.Integer"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="gluser"
 *                        role-link="gluserlink"
 *
 * @ejb:permission role-name="gluser"
 *
 * @ejb:interface local-class="com.ejb.gl.LocalGlStaticReport"
 *                local-extends="javax.ejb.EJBLocalObject"
 *
 * @ejb:home local-class="com.ejb.gl.LocalGlStaticReportHome"
 *           local-extends="javax.ejb.EJBLocalHome"
 * 
 * 
 * @ejb:finder signature="Collection findSrAll(java.lang.Integer SR_AD_CMPNY)"
 *             query="SELECT OBJECT(sr) FROM GlStaticReport sr WHERE sr.srAdCompany = ?1"
 * 
 * @jboss:query signature="Collection findSrAll(java.lang.Integer SR_AD_CMPNY)"
 *             query="SELECT OBJECT(sr) FROM GlStaticReport sr WHERE sr.srAdCompany = ?1 ORDER BY sr.srName"
 * 
 * @ejb:finder signature="LocalGlStaticReport findBySrName(java.lang.String SR_NM, java.lang.Integer SR_AD_CMPNY)"
 *             query="SELECT OBJECT(sr) FROM GlStaticReport sr WHERE sr.srName = ?1 AND sr.srAdCompany = ?2"
 * 
 * @ejb:value-object match="*"
 *             name="GlStaticReport"
 * 
 * @jboss:persistence table-name="GL_STTC_RPRT"
 *
 * @jboss:entity-command name="mysql-get-generated-keys"
 *
 */

public abstract class GlStaticReportBean extends AbstractEntityBean {


   // Access methods for persistent fields

   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    * @ejb:pk-field
    *
    * @jboss:column-name name="SR_CODE"
    *
    * @jboss:persistence auto-increment="true"
    **/
   public abstract java.lang.Integer getSrCode();
   /**
    * @ejb:interface-method view-type="local"
    **/
   public abstract void setSrCode(java.lang.Integer SR_CODE);

   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="SR_NM"
    *
    **/
   public abstract java.lang.String getSrName();
   /**
    * @ejb:interface-method view-type="local"
    **/
   public abstract void setSrName(java.lang.String SR_NM);

   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="SR_DESC"
    *
    **/
   public abstract java.lang.String getSrDescription();
   /**
    * @ejb:interface-method view-type="local"
    **/   
   public abstract void setSrDescription(java.lang.String SR_DESC);

   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="SR_FL_NM"
    *
    **/
   public abstract java.lang.String getSrFileName();
   /**
    * @ejb:interface-method view-type="local"
    **/
   public abstract void setSrFileName(java.lang.String SR_FL_NM);

   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="SR_DT_FRM"
    *
    **/
   public abstract Date getSrDateFrom();
   /**
    * @ejb:interface-method view-type="local"
    **/
   public abstract void setSrDateFrom(Date SR_DT_FRM);


   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="SR_DT_TO"
    *
    **/
   public abstract Date getSrDateTo();
   /**
    * @ejb:interface-method view-type="local"
    **/
   public abstract void setSrDateTo(Date SR_DT_TO);

   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="SR_AD_CMPNY"
    **/
   public abstract Integer getSrAdCompany();
   public abstract void setSrAdCompany(Integer SR_AD_CMPNY);
   
   // Access methods for relationship fields

   /**
    * @ejb:interface-method view-type="local"
    * @ejb:relation name="staticreport-userstaticreports"
    *               role-name="staticreport-has-many-userstaticreports"
    */
   public abstract Collection getGlUserStaticReports();
   public abstract void setGlUserStaticReports(Collection glUserStaticReports);

   
   // Business methods
   
   /**
    * @ejb:interface-method view-type="local"
    **/
   public void addGlUserStaticReport(LocalGlUserStaticReport glUserStaticReport) {

      Debug.print("GlStaticReportBean addGlSuspenseAccount");
      try {
         Collection glUserStaticReports = getGlUserStaticReports();
         glUserStaticReports.add(glUserStaticReport);
      } catch (Exception ex) {
         throw new EJBException(ex.getMessage());
      }
   }

   /**
    * @ejb:interface-method view-type="local"
    **/
   public void dropGlUserStaticReport(LocalGlUserStaticReport glUserStaticReport) {
   
      Debug.print("GlStaticReportBean dropGlUserStaticReport");
      try {
         Collection glUserStaticReports = getGlUserStaticReports();
         glUserStaticReports.remove(glUserStaticReport);
      } catch (Exception ex) {
         throw new EJBException(ex.getMessage());
      }
   }

   // EntityBean methods

   /**
    * @ejb:create-method view-type="local"
    **/
   public java.lang.Integer ejbCreate (Integer SR_CODE,
   		String SR_NM,
   		String SR_DESC,
   		String SR_FL_NM,
   		Date SR_DT_FRM,
		Date SR_DT_TO,
		Integer SR_AD_CMPNY)
      throws CreateException {

      Debug.print("GlStaticReportBean ejbCreate");
      setSrCode(SR_CODE);
      setSrName(SR_NM);
	  setSrDescription(SR_DESC);
	  setSrFileName(SR_FL_NM);
	  setSrDateFrom(SR_DT_FRM);
	  setSrDateTo(SR_DT_TO);
	  setSrAdCompany(SR_AD_CMPNY);

      return null;
   }

   /**
    * @ejb:create-method view-type="local"
    **/
   public java.lang.Integer ejbCreate (String SR_NM,
   		String SR_DESC,
   		String SR_FL_NM,
   		Date SR_DT_FRM,
		Date SR_DT_TO,
		Integer SR_AD_CMPNY)
      throws CreateException {

      Debug.print("GlStaticReportBean ejbCreate");

      setSrName(SR_NM);
	  setSrDescription(SR_DESC);
	  setSrFileName(SR_FL_NM);
	  setSrDateFrom(SR_DT_FRM);
	  setSrDateTo(SR_DT_TO);
	  setSrAdCompany(SR_AD_CMPNY);
      
      return null;
   }

   public void ejbPostCreate (Integer SR_CODE,
   		String SR_NM,
   		String SR_DESC,
   		String SR_FL_NM,
   		Date SR_DT_FRM,
		Date SR_DT_TO,
		Integer SR_AD_CMPNY)
      throws CreateException { }

   public void ejbPostCreate (String SR_NM,
   		String SR_DESC,
   		String SR_FL_NM,
   		Date SR_DT_FRM,
		Date SR_DT_TO,
		Integer SR_AD_CMPNY)
      throws CreateException { }

} // GlStaticReportBean class
