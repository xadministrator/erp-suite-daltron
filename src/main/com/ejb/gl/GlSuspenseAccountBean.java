package com.ejb.gl;

import javax.ejb.CreateException;

import com.util.AbstractEntityBean;
import com.util.Debug;

/**
 * @ejb:bean name="GlSuspenseAccountEJB"
 *           display-name="Suspense Account Entity"
 *           type="CMP"
 *           cmp-version="2.x"
 *           view-type="local"
 *           local-jndi-name="omega-ejb/GlSuspenseAccountEJB"
 *           schema="GlSuspenseAccount"
 *           primkey-field="saCode"
 *       
 * @ejb:pk class="java.lang.Integer"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="gluser"
 *                        role-link="gluserlink"
 *
 * @ejb:permission role-name="gluser"
 *
 * @ejb:interface local-class="com.ejb.gl.LocalGlSuspenseAccount"
 *                local-extends="javax.ejb.EJBLocalObject"
 *
 * @ejb:home local-class="com.ejb.gl.LocalGlSuspenseAccountHome"
 *           local-extends="javax.ejb.EJBLocalHome"
 *
 * @ejb:finder signature="Collection findSaAll(java.lang.Integer SA_AD_CMPNY)"
 *             query="SELECT OBJECT(sa) FROM GlSuspenseAccount sa WHERE sa.saAdCompany = ?1"
 *
 * @jboss:query signature="Collection findSaAll(java.lang.Integer SA_AD_CMPNY)"
 *             query="SELECT OBJECT(sa) FROM GlSuspenseAccount sa WHERE sa.saAdCompany = ?1 ORDER BY sa.saName"
 *
 * @ejb:finder signature="LocalGlSuspenseAccount findBySaName(java.lang.String SA_NM, java.lang.Integer SA_AD_CMPNY)"
 *             query="SELECT OBJECT(sa) FROM GlSuspenseAccount sa WHERE sa.saName=?1 AND sa.saAdCompany = ?2"
 *
 * @ejb:finder signature="LocalGlSuspenseAccount findByJsNameAndJcName(java.lang.String JS_NM, java.lang.String JC_NM, java.lang.Integer SA_AD_CMPNY)"
 *             query="SELECT OBJECT(sa) FROM GlSuspenseAccount sa WHERE sa.glJournalSource.jsName=?1 AND sa.glJournalCategory.jcName=?2 AND sa.saAdCompany = ?3"
 *
 * @jboss:persistence table-name="GL_SSPNS_ACCNT"
 *
 * @jboss:entity-command name="mysql-get-generated-keys"
 *
 */

public abstract class GlSuspenseAccountBean extends AbstractEntityBean {


   // Access methods for persistent fields

   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    * @ejb:pk-field
    *
    * @jboss:column-name name="SA_CODE"
    *
    * @jboss:persistence auto-increment="true"
    **/
   public abstract java.lang.Integer getSaCode();
   public abstract void setSaCode(java.lang.Integer SA_CODE);

   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="SA_NM"
    **/
   public abstract java.lang.String getSaName();
   /**
    * @ejb:interface-method view-type="local"
    **/
   public abstract void setSaName(java.lang.String SA_NM);

   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="SA_DESC"
    **/
   public abstract java.lang.String getSaDescription();
   /**
    * @ejb:interface-method view-type="local"
    **/
   public abstract void setSaDescription(java.lang.String SA_DESC);
   
   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="SA_AD_CMPNY"
    **/
   public abstract java.lang.Integer getSaAdCompany();
   /**
    * @ejb:interface-method view-type="local"
    **/
   public abstract void setSaAdCompany(java.lang.Integer SA_AD_CMPNY);

   // Access methods for relationship fields

   /**
    * @ejb:interface-method view-type="local"
    * @ejb:relation name="chartofaccount-suspenseaccounts"
    *               role-name="suspenseaccount-has-one-chartofaccount"
    *
    * @jboss:relation related-pk-field="coaCode"
    *                 fk-column="GL_CHART_OF_ACCOUNT"
    */
   public abstract LocalGlChartOfAccount getGlChartOfAccount();
   public abstract void setGlChartOfAccount(LocalGlChartOfAccount glChartOfAccount);

   /**
    * @ejb:interface-method view-type="local"
    * @ejb:relation name="journalcategory-suspenseaccounts"
    *               role-name="suspenseaccount-has-one-journalcategory"
    *
    * @jboss:relation related-pk-field="jcCode"
    *                 fk-column="GL_JOURNAL_CATEGORY"
    */
   public abstract LocalGlJournalCategory getGlJournalCategory();
   public abstract void setGlJournalCategory(LocalGlJournalCategory glJournalCategory);

   /**
    * @ejb:interface-method view-type="local"
    * @ejb:relation name="journalsource-suspenseaccounts"
    *               role-name="suspenseaccount-has-one-journalsource"
    *
    * @jboss:relation related-pk-field="jsCode"
    *                 fk-column="GL_JOURNAL_SOURCE"
    */
   public abstract LocalGlJournalSource getGlJournalSource();
   public abstract void setGlJournalSource(LocalGlJournalSource glJournalSource);

   // Business methods
   
   // EntityBean methods

   /**
    * @ejb:create-method view-type="local"
    **/
   public java.lang.Integer ejbCreate (java.lang.Integer SA_CODE, java.lang.String SA_NM,
      java.lang.String SA_DESC, java.lang.Integer SA_AD_CMPNY) throws CreateException {

      Debug.print("GlSuspenseAccountBean ejbCreate");
      
      setSaCode(SA_CODE);
      setSaName(SA_NM);
      setSaDescription(SA_DESC);
      setSaAdCompany(SA_AD_CMPNY);
      
      return null;
   }

   /**
    * @ejb:create-method view-type="local"
    **/
   public java.lang.Integer ejbCreate (java.lang.String SA_NM, java.lang.String SA_DESC, 
   		java.lang.Integer SA_AD_CMPNY)
   	throws CreateException{

	Debug.print("GlSuspenseAccount Bean ejbCreate");
	
	setSaName(SA_NM);
	setSaDescription(SA_DESC);
	setSaAdCompany(SA_AD_CMPNY);
	
	return null;
   }

   public void ejbPostCreate (java.lang.Integer SA_CODE, java.lang.String SA_NM,
      java.lang.String SA_DESC, java.lang.Integer SA_AD_CMPNY) throws CreateException { }

   public void ejbPostCreate (java.lang.String SA_NM, java.lang.String SA_DESC, 
   		java.lang.Integer SA_AD_CMPNY)
      throws CreateException{ }

} // GlSuspenseAccountBean class
