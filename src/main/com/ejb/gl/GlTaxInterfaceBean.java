package com.ejb.gl;

import java.util.Collection;
import java.util.Date;

import javax.ejb.CreateException;
import javax.ejb.FinderException;

import com.util.AbstractEntityBean;
import com.util.Debug;

/**
 * @ejb:bean name="GlTaxInterfaceEJB"
 *           display-name="Tax Interface Entity"
 *           type="CMP"
 *           cmp-version="2.x"
 *           view-type="local"
 *           local-jndi-name="omega-ejb/GlTaxInterfaceEJB"
 *           schema="GlTaxInterface"
 *           primkey-field="tiCode"
 *       
 * @ejb:pk class="java.lang.Integer"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="gluser"
 *                        role-link="gluserlink"
 *
 * @ejb:permission role-name="gluser"
 *
 * @ejb:interface local-class="com.ejb.gl.LocalGlTaxInterface"
 *                local-extends="javax.ejb.EJBLocalObject"
 *
 * @ejb:home local-class="com.ejb.gl.LocalGlTaxInterfaceHome"
 *           local-extends="javax.ejb.EJBLocalHome"
 * 
 * @ejb:finder signature="LocalGlTaxInterface findByTiDocumentTypeAndTxlCode(java.lang.String TI_DCMNT_TYP, java.lang.Integer TI_TXL_CODE, java.lang.Integer TI_AD_CMPNY)"
 *             query="SELECT OBJECT(ti) FROM GlTaxInterface ti WHERE ti.tiDocumentType=?1 AND ti.tiTxlCode=?2 AND ti.tiAdCompany = ?3" 
 *
 * @ejb:finder signature="LocalGlTaxInterface findByTiDocumentTypeAndTxnCode(java.lang.String TI_DCMNT_TYP, java.lang.Integer TI_TXN_CODE, java.lang.Integer TI_AD_CMPNY)"
 *             query="SELECT OBJECT(ti) FROM GlTaxInterface ti WHERE ti.tiDocumentType=?1 AND ti.tiTxnCode=?2 AND ti.tiAdCompany = ?3" 
 *
 * @ejb:finder signature="Collection findAllByTiDocumentTypeAndTxnCode(java.lang.String TI_DCMNT_TYP, java.lang.Integer TI_TXN_CODE, java.lang.Integer TI_AD_CMPNY)"
 *             query="SELECT OBJECT(ti) FROM GlTaxInterface ti WHERE ti.tiDocumentType=?1 AND ti.tiTxnCode=?2 AND ti.tiAdCompany = ?3" 
 *
 * @ejb:finder signature="Collection findByTiSourceAndTax(java.lang.String TI_SRC, java.util.Date TI_TXN_DT_FRM, java.util.Date TI_TXN_DT_TO, java.lang.Integer TI_AD_CMPNY)"
 *             query="SELECT OBJECT(ti) FROM GlTaxInterface ti WHERE ti.tiTcCode IS NOT NULL AND ti.tiSource=?1 AND ti.tiTxnDate >= ?2 AND ti.tiTxnDate <= ?3 AND ti.tiAdCompany = ?4"
 * 
 * @jboss:query signature="Collection findByTiSourceAndTax(java.lang.String TI_SRC, java.util.Date TI_TXN_DT_FRM, java.util.Date TI_TXN_DT_TO, java.lang.Integer TI_AD_CMPNY)"
 *             query="SELECT OBJECT(ti) FROM GlTaxInterface ti WHERE ti.tiTcCode IS NOT NULL AND ti.tiSource=?1 AND ti.tiTxnDate >= ?2 AND ti.tiTxnDate <= ?3  AND ti.tiAdCompany = ?4 ORDER BY ti.tiSlSubledgerCode, ti.tiSlTin"
 * 
 * @ejb:select signature="Collection ejbSelectGeneric(java.lang.String jbossQl, java.lang.Object[] args)"
 *             result-type-mapping="Local"
 *             method-intf="LocalHome"
 *             query="SELECT OBJECT(ti) FROM GlTaxInterface ti"
 *
 * @jboss:query signature="Collection ejbSelectGeneric(java.lang.String jbossQl, java.lang.Object[] args)"
 *                         dynamic="true"
 * 
 * @ejb:value-object match="*"
 *             name="GlTaxInterface"
 * 
 * @ejb:select signature="Collection ejbSelectGeneric(java.lang.String jbossQl, java.lang.Object[] args)"
 *             result-type-mapping="Local"
 *             method-intf="LocalHome"
 *             query="SELECT OBJECT(ti) FROM GlTaxInterface ti"
 *
 * @jboss:query signature="Collection ejbSelectGeneric(java.lang.String jbossQl, java.lang.Object[] args)"
 *                         dynamic="true"
 *
 * @jboss:persistence table-name="GL_TX_INTRFC"
 *
 * @jboss:entity-command name="mysql-get-generated-keys"
 *
 */

public abstract class GlTaxInterfaceBean extends AbstractEntityBean {

	
   // Access methods for persistent fields

   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    * @ejb:pk-field
    *
    * @jboss:column-name name="TI_CODE"
    *
    * @jboss:persistence auto-increment="true"
    **/
   public abstract Integer getTiCode();
   public abstract void setTiCode(Integer TI_CODE);

   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="TI_DCMNT_TYP"
    **/
   public abstract String getTiDocumentType();
   /**
    * @ejb:interface-method view-type="local"
    **/
   public abstract void setTiDocumentType(String TI_DCMNT_TYP);
   
   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="TI_SRC"
    **/
   public abstract String getTiSource();
   /**
    * @ejb:interface-method view-type="local"
    **/
   public abstract void setTiSource(String TI_SRC);

   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="TI_NET_AMNT"
    **/
   public abstract double getTiNetAmount();
   /**
    * @ejb:interface-method view-type="local"
    **/
   public abstract void setTiNetAmount(double TI_NET_AMNT);
   
   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="TI_TX_AMNT"
    **/
   public abstract double getTiTaxAmount();
   /**
    * @ejb:interface-method view-type="local"
    **/
   public abstract void setTiTaxAmount(double TI_TX_AMNT);
   
   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="TI_SLS_AMNT"
    **/
   public abstract double getTiSalesAmount();
   /**
    * @ejb:interface-method view-type="local"
    **/
   public abstract void setTiSalesAmount(double TI_SLS_AMNT);
   
   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="TI_SRVCS_AMNT"
    **/
   public abstract double getTiServicesAmount();
   /**
    * @ejb:interface-method view-type="local"
    **/
   public abstract void setTiServicesAmount(double TI_SRVCS_AMNT);
   
   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="TI_CPTL_GDS_AMNT"
    **/
   public abstract double getTiCapitalGoodsAmount();
   /**
    * @ejb:interface-method view-type="local"
    **/
   public abstract void setTiCapitalGoodsAmount(double TI_CPTL_GDS_AMNT);
   
   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="TI_OTHR_CPTL_GDS_AMNT"
    **/
   public abstract double getTiOtherCapitalGoodsAmount();
   /**
    * @ejb:interface-method view-type="local"
    **/
   public abstract void setTiOtherCapitalGoodsAmount(double TI_OTHR_CPTL_GDS_AMNT);
   
   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="TI_TXN_CODE"
    **/
   public abstract Integer getTiTxnCode();
   /**
    * @ejb:interface-method view-type="local"
    **/
   public abstract void setTiTxnCode(Integer TI_TXN_CODE);
   
   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="TI_TXN_DT"
    **/
   public abstract Date getTiTxnDate();
   /**
    * @ejb:interface-method view-type="local"
    **/
   public abstract void setTiTxnDate(Date TI_TXN_DT);
      
   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="TI_TXN_DCMNT_NMBR"
    **/
   public abstract String getTiTxnDocumentNumber();
   /**
    * @ejb:interface-method view-type="local"
    **/
   public abstract void setTiTxnDocumentNumber(String TI_TXN_DCMNT_NMBR);
   
   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="TI_TXN_RFRNC_NMBR"
    **/
   public abstract String getTiTxnReferenceNumber();
   /**
    * @ejb:interface-method view-type="local"
    **/
   public abstract void setTiTxnReferenceNumber(String TI_TXN_RFRNC_NMBR);
   
   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="TI_TX_EXMPT"
    **/
   public abstract double getTiTaxExempt();
   /**
    * @ejb:interface-method view-type="local"
    **/
   public abstract void setTiTaxExempt(double TI_TX_EXMPT);
   
   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="TI_TX_ZR_RTD"
    **/
   public abstract double getTiTaxZeroRated();
   /**
    * @ejb:interface-method view-type="local"
    **/
   public abstract void setTiTaxZeroRated(double TI_TX_ZR_RTD);
   
   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="TI_TXL_CODE"
    **/
   public abstract Integer getTiTxlCode();
   /**
    * @ejb:interface-method view-type="local"
    **/
   public abstract void setTiTxlCode(Integer TI_TXL_CODE);
   
   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="TI_TXL_COA_CODE"
    **/
   public abstract Integer getTiTxlCoaCode();
   /**
    * @ejb:interface-method view-type="local"
    **/
   public abstract void setTiTxlCoaCode(Integer TI_TXL_COA_CODE);
   
   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="TI_TC_CODE"
    **/
   public abstract Integer getTiTcCode();
   /**
    * @ejb:interface-method view-type="local"
    **/
   public abstract void setTiTcCode(Integer TI_TC_CODE);
   
   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="TI_WTC_CODE"
    **/
   public abstract Integer getTiWtcCode();
   /**
    * @ejb:interface-method view-type="local"
    **/
   public abstract void setTiWtcCode(Integer TI_WTC_CODE);
      
   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="TI_SL_CODE"
    **/
   public abstract Integer getTiSlCode();
   /**
    * @ejb:interface-method view-type="local"
    **/
   public abstract void setTiSlCode(Integer TI_SL_CODE);
   
   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="TI_SL_TIN"
    **/
   public abstract String getTiSlTin();
   /**
    * @ejb:interface-method view-type="local"
    **/
   public abstract void setTiSlTin(String TI_SL_TIN);
   
   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="TI_SL_SBLDGR_CODE"
    **/
   public abstract String getTiSlSubledgerCode();
   /**
    * @ejb:interface-method view-type="local"
    **/
   public abstract void setTiSlSubledgerCode(String TI_SL_SBLDGR_CODE);
   
   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="TI_SL_NM"
    **/
   public abstract String getTiSlName();
   /**
    * @ejb:interface-method view-type="local"
    **/
   public abstract void setTiSlName(String TI_SL_NM);
   
   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="TI_SL_ADDRSS"
    **/
   public abstract String getTiSlAddress();
   /**
    * @ejb:interface-method view-type="local"
    **/
   public abstract void setTiSlAddress(String TI_SL_ADDRSS);
   
   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="TI_SL_ADDRSS2"
    **/
   public abstract String getTiSlAddress2();
   /**
    * @ejb:interface-method view-type="local"
    **/
   public abstract void setTiSlAddress2(String TI_SL_ADDRSS2);
   
   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="TI_IS_AR_DCMNT"
    **/
   public abstract byte getTiIsArDocument();
   /**
    * @ejb:interface-method view-type="local"
    **/
   public abstract void setTiIsArDocument(byte TI_IS_AR_DCMNT);
   
   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="TI_AD_BRNCH"
    **/
   public abstract Integer getTiAdBranch();
   /**
    * @ejb:interface-method view-type="local"
    **/
   public abstract void setTiAdBranch(Integer TI_AD_BRNCH);
   
   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="TI_AD_CMPNY"
    **/
   public abstract Integer getTiAdCompany();
   /**
    * @ejb:interface-method view-type="local"
    **/
   public abstract void setTiAdCompany(Integer TI_AD_CMPNY);
   
   /**
    * @jboss:dynamic-ql
    */
    public abstract Collection ejbSelectGeneric(java.lang.String jbossQl, java.lang.Object[] args)
       throws FinderException;

   //Business methods
    
   /**
	* @ejb:home-method view-type="local"
	*/
	public Collection ejbHomeGetTiByCriteria(java.lang.String jbossQl, java.lang.Object[] args)
	   throws FinderException {
	   	
	   return ejbSelectGeneric(jbossQl, args);
	}
	
   // EntityBean methods

   /**
    * @ejb:create-method view-type="local"
    **/
   public java.lang.Integer ejbCreate (Integer TI_CODE, String TI_DCMNT_TYP, String TI_SRC, double TI_NET_AMNT, double TI_TX_AMNT,
		   double TI_SLS_AMNT, double TI_SRVCS_AMNT, double TI_CPTL_GDS_AMNT, double TI_OTHR_CPTL_GDS_AMNT,
   	  Integer TI_TXN_CODE, Date TI_TXN_DT, String TI_TXN_DCMNT_NMBR, String TI_TXN_RFRNC_NMBR,
   	  double TI_TX_EXMPT, double TI_TX_ZR_RTD,
	  Integer TI_TXL_CODE, Integer TI_TXL_COA_CODE, Integer TI_TC_CODE, Integer TI_WTC_CODE, 
	  Integer TI_SL_CODE, String TI_SL_TIN, String TI_SL_SBLDGR_CODE, String TI_SL_NM, String TI_SL_ADDRSS, String TI_SL_ADDRSS2,
	  byte TI_IS_AR_DCMNT, Integer TI_AD_BRNCH, Integer TI_AD_CMPNY)
      throws CreateException {

      Debug.print("GlTaxInterfaceBean ejbCreate");
            
      setTiCode(TI_CODE);
	  setTiDocumentType(TI_DCMNT_TYP);
	  setTiSource(TI_SRC);
      setTiNetAmount(TI_NET_AMNT);
      setTiTaxAmount(TI_TX_AMNT);
      setTiSalesAmount(TI_SLS_AMNT);
      setTiServicesAmount(TI_SRVCS_AMNT);
      setTiCapitalGoodsAmount(TI_CPTL_GDS_AMNT);
      setTiOtherCapitalGoodsAmount(TI_OTHR_CPTL_GDS_AMNT);
      setTiTxnCode(TI_TXN_CODE);
      setTiTxnDate(TI_TXN_DT);
      setTiTxnDocumentNumber(TI_TXN_DCMNT_NMBR);
      setTiTxnReferenceNumber(TI_TXN_RFRNC_NMBR);
      setTiTaxExempt(TI_TX_EXMPT);
      setTiTaxZeroRated(TI_TX_ZR_RTD);
      setTiTxlCode(TI_TXL_CODE);
      setTiTxlCoaCode(TI_TXL_COA_CODE);
      setTiTcCode(TI_TC_CODE);
      setTiWtcCode(TI_WTC_CODE);
      setTiSlCode(TI_SL_CODE);
      setTiSlTin(TI_SL_TIN);
      setTiSlSubledgerCode(TI_SL_SBLDGR_CODE);
      setTiSlName(TI_SL_NM);
      setTiSlAddress(TI_SL_ADDRSS);
      setTiSlAddress2(TI_SL_ADDRSS2);
      setTiIsArDocument(TI_IS_AR_DCMNT);
      setTiAdBranch(TI_AD_BRNCH);
      setTiAdCompany(TI_AD_CMPNY);
      
      return null;
   }

   /**
    * @ejb:create-method view-type="local"
    **/
   public java.lang.Integer ejbCreate (String TI_DCMNT_TYP, String TI_SRC,double TI_NET_AMNT, double TI_TX_AMNT, 
		   double TI_SLS_AMNT, double TI_SRVCS_AMNT, double TI_CPTL_GDS_AMNT, double TI_OTHR_CPTL_GDS_AMNT,
      Integer TI_TXN_CODE, Date TI_TXN_DT, String TI_TXN_DCMNT_NMBR, String TI_TXN_RFRNC_NMBR,
      double TI_TX_EXMPT, double TI_TX_ZR_RTD,
	  Integer TI_TXL_CODE, Integer TI_TXL_COA_CODE, Integer TI_TC_CODE, Integer TI_WTC_CODE, 
	  Integer TI_SL_CODE, String TI_SL_TIN, String TI_SL_SBLDGR_CODE, String TI_SL_NM, String TI_SL_ADDRSS, String TI_SL_ADDRSS2,
	  byte TI_IS_AR_DCMNT, Integer TI_AD_BRNCH, Integer TI_AD_CMPNY)
      throws CreateException {

      Debug.print("GlTaxInterfaceBean ejbCreate");

      setTiDocumentType(TI_DCMNT_TYP);
      setTiSource(TI_SRC);
      setTiNetAmount(TI_NET_AMNT);
      setTiTaxAmount(TI_TX_AMNT);
      setTiSalesAmount(TI_SLS_AMNT);
      setTiServicesAmount(TI_SRVCS_AMNT);
      setTiCapitalGoodsAmount(TI_CPTL_GDS_AMNT);
      setTiOtherCapitalGoodsAmount(TI_OTHR_CPTL_GDS_AMNT);
      setTiTxnCode(TI_TXN_CODE);
      setTiTxnDate(TI_TXN_DT);
      setTiTxnDocumentNumber(TI_TXN_DCMNT_NMBR);
      setTiTxnReferenceNumber(TI_TXN_RFRNC_NMBR);
      setTiTaxExempt(TI_TX_EXMPT);
      setTiTaxZeroRated(TI_TX_ZR_RTD);
      setTiTxlCode(TI_TXL_CODE);
      setTiTxlCoaCode(TI_TXL_COA_CODE);
      setTiTcCode(TI_TC_CODE);
      setTiWtcCode(TI_WTC_CODE);
      setTiSlCode(TI_SL_CODE);
      setTiSlTin(TI_SL_TIN);
      setTiSlSubledgerCode(TI_SL_SBLDGR_CODE);
      setTiSlName(TI_SL_NM);
      setTiSlAddress(TI_SL_ADDRSS);
      setTiSlAddress2(TI_SL_ADDRSS2);
      setTiIsArDocument(TI_IS_AR_DCMNT);
      setTiAdBranch(TI_AD_BRNCH);
      setTiAdCompany(TI_AD_CMPNY);
      
      return null;
   }

   public void ejbPostCreate (Integer TI_CODE, String TI_DCMNT_TYP, String TI_SRC, double TI_NET_AMNT, double TI_TX_AMNT, 
		   double TI_SLS_AMNT, double TI_SRVCS_AMNT, double TI_CPTL_GDS_AMNT, double TI_OTHR_CPTL_GDS_AMNT,
      Integer TI_TXN_CODE, Date TI_TXN_DT, String TI_TXN_DCMNT_NMBR, String TI_TXN_RFRNC_NMBR,
      double TI_TX_EXMPT, double TI_TX_ZR_RTD,
	  Integer TI_TXL_CODE, Integer TI_TXL_COA_CODE, Integer TI_TC_CODE, Integer TI_WTC_CODE,
	  Integer TI_SL_CODE, String TI_SL_TIN, String TI_SL_SBLDGR_CODE, String TI_SL_NM, String TI_SL_ADDRSS, String TI_SL_ADDRSS2,
	  byte TI_IS_AR_DCMNT, Integer TI_AD_BRNCH, Integer TI_AD_CMPNY)
      throws CreateException { }

   public void ejbPostCreate (String TI_DCMNT_TYP, String TI_SRC, double TI_NET_AMNT, double TI_TX_AMNT, 
		   double TI_SLS_AMNT, double TI_SRVCS_AMNT, double TI_CPTL_GDS_AMNT, double TI_OTHR_CPTL_GDS_AMNT,
      Integer TI_TXN_CODE, Date TI_TXN_DT, String TI_TXN_DCMNT_NMBR, String TI_TXN_RFRNC_NMBR,
      double TI_TX_EXMPT, double TI_TX_ZR_RTD,
	  Integer TI_TXL_CODE, Integer TI_TXL_COA_CODE, Integer TI_TC_CODE, Integer TI_WTC_CODE,
	  Integer TI_SL_CODE, String TI_SL_TIN, String TI_SL_SBLDGR_CODE, String TI_SL_NM, String TI_SL_ADDRSS, String TI_SL_ADDRSS2,
	  byte TI_IS_AR_DCMNT, Integer TI_AD_BRNCH, Integer TI_AD_CMPNY)
      throws CreateException { }

} // GlTaxInterfaceBean class
