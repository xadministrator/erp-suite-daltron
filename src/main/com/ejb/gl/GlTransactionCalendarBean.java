package com.ejb.gl;

import java.util.Collection;

import javax.ejb.CreateException;
import javax.ejb.EJBException;

import com.util.AbstractEntityBean;
import com.util.Debug;

/**
 * @ejb:bean name="GlTransactionCalendarEJB"
 *           display-name="Transaction Calendar Entity"
 *           type="CMP"
 *           cmp-version="2.x"
 *           view-type="local"
 *           local-jndi-name="omega-ejb/GlTransactionCalendarEJB"
 *           schema="GlTransactionCalendar"
 *           primkey-field="tcCode"
 *       
 * @ejb:pk class="java.lang.Integer"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="gluser"
 *                        role-link="gluserlink"
 *
 * @ejb:permission role-name="gluser"
 *
 * @ejb:interface local-class="com.ejb.gl.LocalGlTransactionCalendar"
 *                local-extends="javax.ejb.EJBLocalObject"
 *
 * @ejb:home local-class="com.ejb.gl.LocalGlTransactionCalendarHome"
 *           local-extends="javax.ejb.EJBLocalHome"
 *
 * @ejb:finder signature="Collection findTcAll(java.lang.Integer TC_AD_CMPNY)"
 *             query="SELECT OBJECT(tc) FROM GlTransactionCalendar tc WHERE tc.tcAdCompany = ?1"
 *
 * @jboss:query signature="Collection findTcAll(java.lang.Integer TC_AD_CMPNY)"
 *             query="SELECT OBJECT(tc) FROM GlTransactionCalendar tc WHERE tc.tcAdCompany = ?1 ORDER BY tc.tcName"
 *
 * @ejb:finder signature="Collection findTcAllWithTcv(java.lang.Integer TC_AD_CMPNY)"
 *             query="SELECT DISTINCT OBJECT(tc) FROM GlTransactionCalendar tc, IN(tc.glTransactionCalendarValues) tcv WHERE tcv.tcvCode > 0 AND tc.tcAdCompany = ?1"
 *
 * @ejb:finder signature="LocalGlTransactionCalendar findByTcName(java.lang.String TC_NM, java.lang.Integer TC_AD_CMPNY)"
 *             query="SELECT OBJECT(tc) FROM GlTransactionCalendar tc WHERE tc.tcName=?1 AND tc.tcAdCompany = ?2"
 *
 * @jboss:persistence table-name="GL_TRNSCTN_CLNDR"
 *
 * @jboss:entity-command name="mysql-get-generated-keys"
 *
 */

public abstract class GlTransactionCalendarBean extends AbstractEntityBean {


   // Access methods for persistent fields

   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    * @ejb:pk-field
    *
    * @jboss:column-name name="TC_CODE"
    *
    * @jboss:persistence auto-increment="true"
    **/
   public abstract java.lang.Integer getTcCode();
   public abstract void setTcCode(java.lang.Integer TC_CODE);

   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="TC_NM"
    **/
   public abstract java.lang.String getTcName();
   /**
    * @ejb:interface-method view-type="local"
    **/
   public abstract void setTcName(java.lang.String TC_NM);

   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="TC_DESC"
    **/
   public abstract java.lang.String getTcDescription();
   /**
    * @ejb:interface-method view-type="local"
    **/
   public abstract void setTcDescription(java.lang.String TC_DESC);
   
   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="TC_AD_CMPNY"
    **/
   public abstract java.lang.Integer getTcAdCompany();
   /**
    * @ejb:interface-method view-type="local"
    **/
   public abstract void setTcAdCompany(java.lang.Integer TC_AD_CMPNY);

   // Access methods for relationship fields

   /**
    * @ejb:interface-method view-type="local"
    * @ejb:relation name="transactioncalendar-setofbooks"
    *               role-name="transactioncalendar-has-many-setofbooks"
    */
   public abstract Collection getGlSetOfBooks();
   public abstract void setGlSetOfBooks(Collection glSetOfBooks);

   /**
    * @ejb:interface-method view-type="local"
    * @ejb:relation name="transactioncalendar-transactioncalendarvalues"
    *               role-name="transactioncalendar-has-many-transactioncalendarvalues"
    */
   public abstract Collection getGlTransactionCalendarValues();
   public abstract void setGlTransactionCalendarValues(Collection glTransactionClendarValues);

   // Business methods

   /**
    * @ejb:interface-method view-type="local"
    **/
   public void addGlSetOfBook(LocalGlSetOfBook glSetOfBook) {
   
      Debug.print("GlTransactionCalendarBean addGlSetOfBook");
      try {
         Collection glSetOfBooks = getGlSetOfBooks();
	 glSetOfBooks.add(glSetOfBook);
      } catch (Exception ex) {
         throw new EJBException(ex.getMessage());
      }
   }

   /**
    * @ejb:interface-method view-type="local"
    **/
   public void dropGlSetOfBook(LocalGlSetOfBook glSetOfBook) {

      Debug.print("GlTransactionCalendarBean dropGlSetOfBook");
      try {
         Collection glSetOfBooks = getGlSetOfBooks();
	 glSetOfBooks.remove(glSetOfBook);
      } catch (Exception ex) {
         throw new EJBException(ex.getMessage());
      }
   }

   /**
    * @ejb:interface-method view-type="local"
    **/
   public void addGlTransactionCalendarValue(LocalGlTransactionCalendarValue glTransactionCalendarValue) {
   
      Debug.print("GlTransactionCalenanBean addGlTransactionCalendarValue");
      try {
         Collection glTransactionCalendarValues = getGlTransactionCalendarValues();
         glTransactionCalendarValues.add(glTransactionCalendarValue);
      } catch (Exception ex) {
         throw new EJBException(ex.getMessage());
      }
   }

   /**
    * @ejb:interface-method view-type="local"
    **/
   public void dropGlTransactionCalendarValue(LocalGlTransactionCalendarValue glTransactionCalendarValue) {
   
      Debug.print("GlTransactionCalendarBean dropGlTransactionCalendarValue");
      try {
         Collection glTransactionCalendarValues = getGlTransactionCalendarValues();
	 glTransactionCalendarValues.remove(glTransactionCalendarValue);
      } catch (Exception ex) {
         throw new EJBException(ex.getMessage());
      }
   }
 
   // EntityBean methods

   /**
    * @ejb:create-method view-type="local"
    **/
   public java.lang.Integer ejbCreate (java.lang.Integer TC_CODE,
      java.lang.String TC_NM, java.lang.String TC_DESC, java.lang.Integer TC_AD_CMPNY) 
      throws CreateException {

      Debug.print("GlTransactionCalendarBean ejbCreate");
      
      setTcCode(TC_CODE);
      setTcName(TC_NM);
      setTcDescription(TC_DESC);
      setTcAdCompany(TC_AD_CMPNY);
      
      return null;
   }
 
   /**
    * @ejb:create-method view-type="local"
    **/
   public java.lang.Integer ejbCreate (
      java.lang.String TC_NM, java.lang.String TC_DESC, java.lang.Integer TC_AD_CMPNY)
      throws CreateException {
      
      Debug.print("GlTransactionCalendarBean ejbCreate");
     
      setTcName(TC_NM);
      setTcDescription(TC_DESC);
      setTcAdCompany(TC_AD_CMPNY);
      
      return null;
   }

   public void ejbPostCreate (java.lang.Integer TC_CODE,
      java.lang.String TC_NM, java.lang.String TC_DESC, java.lang.Integer TC_AD_CMPNY) 
   	  throws CreateException { }

   public void ejbPostCreate (
      java.lang.String TC_NM, java.lang.String TC_DESC, java.lang.Integer TC_AD_CMPNY) 
      throws CreateException { }

} // GlTransactionCalendarBean class
