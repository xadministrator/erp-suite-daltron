package com.ejb.gl;

import java.util.Date;

import javax.ejb.CreateException;

import com.util.AbstractEntityBean;
import com.util.Debug;

/**
 * @ejb:bean name="GlTransactionCalendarValueEJB"
 *           display-name="Transaction Calendar Value Entity"
 *           type="CMP"
 *           cmp-version="2.x"
 *           view-type="local"
 *           local-jndi-name="omega-ejb/GlTransactionCalendarValueEJB"
 *           schema="GlTransactionCalendarValue"
 *           primkey-field="tcvCode"
 *       
 * @ejb:pk class="java.lang.Integer"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="gluser"
 *                        role-link="gluserlink"
 *
 * @ejb:permission role-name="gluser"
 *
 * @ejb:interface local-class="com.ejb.gl.LocalGlTransactionCalendarValue"
 *                local-extends="javax.ejb.EJBLocalObject"
 *
 * @ejb:home local-class="com.ejb.gl.LocalGlTransactionCalendarValueHome"
 *           local-extends="javax.ejb.EJBLocalHome"
 *
 * @ejb:finder signature="LocalGlTransactionCalendarValue findByTcCodeAndTcvDate(java.lang.Integer TC_CODE, java.util.Date TCV_DT, java.lang.Integer TCV_AD_CMPNY)"
 *             query="SELECT OBJECT(tcv) FROM GlTransactionCalendar tc, IN(tc.glTransactionCalendarValues) tcv WHERE tc.tcCode=?1 AND tcv.tcvDate=?2 AND tcv.tcvAdCompany=?3"
 *
 * @jboss:persistence table-name="GL_TRNSCTN_CLNDR_VL"
 *
 * @jboss:entity-command name="mysql-get-generated-keys"
 *
 */

public abstract class GlTransactionCalendarValueBean extends AbstractEntityBean {


   // Access methods for persistent fields

   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    * @ejb:pk-field
    *
    * @jboss:column-name name="TCV_CODE"
    *
    * @jboss:persistence auto-increment="true"
    **/
   public abstract java.lang.Integer getTcvCode();
   public abstract void setTcvCode(java.lang.Integer TCV_CODE);

   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="TCV_DT"
    **/
   public abstract Date getTcvDate();
   public abstract void setTcvDate(Date TCV_DT);

   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="TCV_DY_OF_WK"
    **/
   public abstract short getTcvDayOfWeek();
   public abstract void setTcvDayOfWeek(short TCV_DY_OF_WK);

   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="TCV_BSNSS_DY"
    **/
   public abstract byte getTcvBusinessDay();
   /**
    * @ejb:interface-method view-type="local"
    **/
   public abstract void setTcvBusinessDay(byte TCV_BSNSS_DY);
   
   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="TCV_AD_CMPNY"
    **/
   public abstract Integer getTcvAdCompany();
   /**
    * @ejb:interface-method view-type="local"
    **/
   public abstract void setTcvAdCompany(Integer TCV_AD_CMPNY);
   
   
   // Access methods for relationship fields

   /**
    * @ejb:interface-method view-type="local"
    * @ejb:relation name="transactioncalendar-transactioncalendarvalues"
    *               role-name="transactioncalendarvalue-has-one-transactioncalendar"
    *               cascade-delete="yes"
    *
    * @jboss:relation related-pk-field="tcCode"
    *                 fk-column="GL_TRANSACTION_CALENDAR"
    */
   public abstract LocalGlTransactionCalendar getGlTransactionCalendar();
   /**
    * @ejb:interface-method view-type="local"
    **/
   public abstract void setGlTransactionCalendar(LocalGlTransactionCalendar glTransactionCalendar);

   // EntityBean methods

   /**
    * @ejb:create-method view-type="local"
    **/
  public java.lang.Integer ejbCreate (java.lang.Integer TCV_CODE,
      Date TCV_DT, short TCV_DY_OF_WK, byte TCV_BSNSS_DY, Integer TCV_AD_CMPNY)
      throws CreateException {

      Debug.print("GlTransactionCalendarValueBean ejbCreate");
      
      setTcvCode(TCV_CODE);
      setTcvDate(TCV_DT);
      setTcvDayOfWeek(TCV_DY_OF_WK);
      setTcvBusinessDay(TCV_BSNSS_DY);
      setTcvAdCompany(TCV_AD_CMPNY);
      
      return null;
   }

  /**
    * @ejb:create-method view-type="local"
    **/
   public java.lang.Integer ejbCreate (
      Date TCV_DT, short TCV_DY_OF_WK, byte TCV_BSNSS_DY, Integer TCV_AD_CMPNY)
      throws CreateException {

      Debug.print("GlTransactionCalendarValueBean ejbCreate");
     
      setTcvDate(TCV_DT);
      setTcvDayOfWeek(TCV_DY_OF_WK);
      setTcvBusinessDay(TCV_BSNSS_DY);
      setTcvAdCompany(TCV_AD_CMPNY);
      
      return null;
   }

   public void ejbPostCreate (java.lang.Integer TCV_CODE,
      Date TCV_DT, short TCV_DY_OF_WK, byte TCV_BSNSS_DY, Integer TCV_AD_CMPNY)
      throws CreateException { }

    public void ejbPostCreate (
      Date TCV_DT, short TCV_DY_OF_WK, byte TCV_BSNSS_DY, Integer TCV_AD_CMPNY)
      throws CreateException { }

} // GlTransactionCalendarValueBean class
