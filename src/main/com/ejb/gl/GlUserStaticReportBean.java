package com.ejb.gl;

import javax.ejb.CreateException;

import com.ejb.ad.LocalAdUser;
import com.util.AbstractEntityBean;
import com.util.Debug;

/**
 * @ejb:bean name="GlUserStaticReportEJB"
 *           display-name="User Static Report Entity"
 *           type="CMP"
 *           cmp-version="2.x"
 *           view-type="local"
 *           local-jndi-name="omega-ejb/GlUserStaticReportEJB"
 *           schema="GlUserStaticReport"
 *           primkey-field="ustrCode"
 *       
 * @ejb:pk class="java.lang.Integer"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="gluser"
 *                        role-link="gluserlink"
 *
 * @ejb:permission role-name="gluser"
 *
 * @ejb:interface local-class="com.ejb.gl.LocalGlUserStaticReport"
 *                local-extends="javax.ejb.EJBLocalObject"
 *
 * @ejb:home local-class="com.ejb.gl.LocalGlUserStaticReportHome"
 *           local-extends="javax.ejb.EJBLocalHome"
 * 
 * @ejb:finder signature="Collection findBySrCode(java.lang.Integer SR_CODE, java.lang.Integer USTR_AD_CMPNY)"
 *             query="SELECT OBJECT(ustr) FROM GlUserStaticReport ustr WHERE ustr.glStaticReport.srCode = ?1 AND ustr.ustrAdCompany = ?2"
 *
 * @jboss:query signature="Collection findBySrCode(java.lang.Integer SR_CODE, java.lang.Integer USTR_AD_CMPNY)"
 *             query="SELECT OBJECT(ustr) FROM GlUserStaticReport ustr WHERE ustr.glStaticReport.srCode = ?1 AND ustr.ustrAdCompany = ?2 ORDER BY ustr.adUser.usrCode"
 * 
 * @ejb:finder signature="Collection findByUsrCode(java.lang.Integer USR_CODE, java.lang.Integer USTR_AD_CMPNY)"
 *             query="SELECT OBJECT(ustr) FROM GlUserStaticReport ustr WHERE ustr.adUser.usrCode = ?1 AND ustr.ustrAdCompany = ?2"
 *
 * @jboss:query signature="Collection findByUsrCode(java.lang.Integer USR_CODE, java.lang.Integer USTR_AD_CMPNY)"
 *             query="SELECT OBJECT(ustr) FROM GlUserStaticReport ustr WHERE ustr.adUser.usrCode = ?1 AND ustr.ustrAdCompany = ?2 ORDER BY ustr.adUser.usrName"
 * 
 * @ejb:finder signature="LocalGlUserStaticReport findBySrCodeAndUsrCode(java.lang.Integer SR_CODE, java.lang.Integer USR_NM, java.lang.Integer USTR_AD_CMPNY)"
 *             query="SELECT OBJECT(ustr) FROM GlUserStaticReport ustr WHERE ustr.glStaticReport.srCode=?1 AND ustr.adUser.usrCode=?2 AND ustr.ustrAdCompany = ?3"
 * 
 * @ejb:value-object match="*"
 *             name="GlUserStaticReport"
 *
 * @jboss:persistence table-name="GL_USR_STTC_RPRT"
 *
 * @jboss:entity-command name="mysql-get-generated-keys"
 *
 */

public abstract class GlUserStaticReportBean extends AbstractEntityBean {


   // Access methods for persistent fields

   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    * @ejb:pk-field
    *
    * @jboss:column-name name="USTR_CODE"
    *
    * @jboss:persistence auto-increment="true"
    **/
   public abstract java.lang.Integer getUstrCode();
   /**
    * @ejb:interface-method view-type="local"
    **/
   public abstract void setUstrCode(java.lang.Integer USTR_CODE);

   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="USTR_AD_CMPNY"
    **/
   public abstract Integer getUstrAdCompany();
   /**
    * @ejb:interface-method view-type="local"
    **/
   public abstract void setUstrAdCompany(Integer USTR_AD_CMPNY);
   
   // Access methods for relationship fields

   /**
    * @ejb:interface-method view-type="local"
    * @ejb:relation name="staticreport-userstaticreports"
    *               role-name="userstaticreport-has-one-staticreport"
    *               cascade-delete="yes"
    *
    * @jboss:relation related-pk-field="srCode"
    *                 fk-column="GL_STATIC_REPORT"
    */
   public abstract LocalGlStaticReport getGlStaticReport();
   public abstract void setGlStaticReport(LocalGlStaticReport glStaticReport);

   /**
    * @ejb:interface-method view-type="local"
    * @ejb:relation name="user-userstaticreports"
    *               role-name="userstaticreport-has-one-user"
    *               cascade-delete="yes"
    *
    * @jboss:relation related-pk-field="usrCode"
    *                 fk-column="AD_USER"
    */
   public abstract LocalAdUser getAdUser();
   public abstract void setAdUser(LocalAdUser adUser);
   
   
   
   // Business methods
   

   // EntityBean methods

   /**
    * @ejb:create-method view-type="local"
    **/
   public java.lang.Integer ejbCreate (java.lang.Integer USTR_AD_CMNY)

      throws CreateException {

      Debug.print("GlStaticReportBean ejbCreate");
	  setUstrAdCompany(USTR_AD_CMNY);

      return null;
   }   

   public void ejbPostCreate (java.lang.Integer USTR_AD_CMNY)
      throws CreateException { }

} // GlUserStaticReportBean class
