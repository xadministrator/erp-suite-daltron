/*
 * com/ejb/hr/LocalHrDeployedBranchBean.java
 *
 * Created on February 21, 2003, 12:55 PM
 */

package com.ejb.hr;

import java.util.Collection;
import java.util.Date;

import javax.ejb.CreateException;
import javax.ejb.EJBException;

import com.ejb.ar.LocalArAppliedInvoice;
import com.ejb.ar.LocalArInvoice;
import com.ejb.ar.LocalArInvoiceBatch;
import com.ejb.cm.LocalCmAdjustment;
import com.util.AbstractEntityBean;
import com.util.Debug;

/**
 * @ejb:bean name="HrDeployedBranchEJB"
 *           display-name="Deployed Branch Entity"
 *           type="CMP"
 *           cmp-version="2.x"
 *           view-type="local"
 *           local-jndi-name="omega-ejb/HrDeployedBranchEJB"
 *           schema="HrDeployedBranch"
 *           primkey-field="dbCode"
 *       
 * @ejb:pk class="java.lang.Integer"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="hruser"
 *                        role-link="hruserlink"
 *
 * @ejb:permission role-name="hruser"
 *
 * @ejb:interface local-class="com.ejb.hr.LocalHrDeployedBranch"
 *                local-extends="javax.ejb.EJBLocalObject"
 *
 * @ejb:home local-class="com.ejb.hr.LocalHrDeployedBranchHome"
 *           local-extends="javax.ejb.EJBLocalHome"
 *
 * @ejb:finder signature="Collection findDbAll(java.lang.Integer DB_AD_CMPNY)"
 *             query="SELECT OBJECT(db) FROM HrDeployedBranch db WHERE db.dbAdCompany = ?1"
 *
 * @jboss:query signature="Collection findDbAll(java.lang.Integer DB_AD_CMPNY)"
 *             query="SELECT OBJECT(db) FROM HrDeployedBranch db WHERE db.dbAdCompany = ?1 ORDER BY db.dbClientCode"
 *
 * @ejb:finder signature="LocalHrDeployedBranch findByReferenceID(java.lang.Integer DB_REF_ID, java.lang.Integer DB_AD_CMPNY)"
 *             query="SELECT OBJECT(db) FROM HrDeployedBranch db WHERE db.dbReferenceID=?1 AND db.dbAdCompany=?2"
 *
 * @ejb:finder signature="LocalHrDeployedBranch findByDbClientName(java.lang.String DB_CLNT_NM, java.lang.Integer DB_AD_CMPNY)"
 *             query="SELECT OBJECT(db) FROM HrDeployedBranch db WHERE db.dbClientName=?1 AND db.dbAdCompany=?2"
 *  
 * @ejb:value-object match="*"
 * 						name="HrDeployedBranch"
 * 
 * @jboss:persistence table-name="HR_DPLYD_BRNCH"
 *
 * @jboss:entity-command name="mysql-get-generated-keys"
 *
 */

public abstract class HrDeployedBranchBean extends AbstractEntityBean {


   // Access methods for persistent fields

   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    * @ejb:pk-field
    *
    * @jboss:column-name name="DB_CODE"
    *
    * @jboss:persistence auto-increment="true"
    **/
   public abstract java.lang.Integer getDbCode();
   public abstract void setDbCode(java.lang.Integer DB_CODE);
   
   /**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="DB_RFRNC_ID"
	 **/
	public abstract Integer getDbReferenceID();
	/**
	 * @ejb:interface-method view-type="local"
	 **/ 
	public abstract void setDbReferenceID(Integer DB_RFRNC_ID);
	
	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"   
	 *
	 * @jboss:column-name name="DB_CLNT_CODE"
	 **/
	public abstract String getDbClientCode();
	/**
	 * @ejb:interface-method view-type="local"
	 **/ 
	public abstract void setDbClientCode(String DB_CLNT_CODE);
   
   /**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"   
	 *
	 * @jboss:column-name name="DB_CLNT_NM"
	 **/
	public abstract String getDbClientName();
	/**
	 * @ejb:interface-method view-type="local"
	 **/ 
	public abstract void setDbClientName(String DB_CLNT_NM);
	
	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"   
	 *
	 * @jboss:column-name name="DB_TIN"
	 **/
	public abstract String getDbTin();
	/**
	 * @ejb:interface-method view-type="local"
	 **/ 
	public abstract void setDbTin(String DB_TIN);
	
	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"   
	 *
	 * @jboss:column-name name="DB_ADDRSS"
	 **/
	public abstract String getDbAddress();
	/**
	 * @ejb:interface-method view-type="local"
	 **/ 
	public abstract void setDbAddress(String DB_ADDRSS);
	
	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"   
	 *
	 * @jboss:column-name name="DB_MNGNG_BRNCH"
	 **/
	public abstract String getDbManagingBranch();
	/**
	 * @ejb:interface-method view-type="local"
	 **/ 
	public abstract void setDbManagingBranch(String DB_MNGNG_BRNCH);

   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="DB_AD_CMPNY"
    **/
   public abstract Integer getDbAdCompany();
   /**
    * @ejb:interface-method view-type="local"
    **/
   public abstract void setDbAdCompany(Integer DB_AD_CMPNY);
   
   
   // RELATIONSHIP METHODS 
   
   /**
	 * @ejb:interface-method view-type="local"
	 * @ejb:relation name="deployedbranch-invoices"
	 *               role-name="deployedbranch-has-many-invoices"
	 * 
	 */
	public abstract Collection getArInvoices();
	public abstract void setArInvoices(Collection arInvoices);
	
	
	/**
	 * @ejb:interface-method view-type="local"
	 * @ejb:relation name="deployedbranch-employees"
	 *               role-name="deployedbranch-has-many-employees"
	 * 
	 */
	public abstract Collection getHrEmployees();
	public abstract void setHrEmployees(Collection hrEmployees);
	
	
	
	/**
	 * @ejb:interface-method view-type="local"
	 * @ejb:relation name="deployedbranch-customers"
	 *               role-name="deployedbranch-has-many-customers"
	 * 
	 */
	public abstract Collection getArCustomers();
	public abstract void setArCustomers(Collection arCustomers);
	
	
	// BUSINESS METHODS
	


	
	
   // EntityBean methods

   /**
    * @ejb:create-method view-type="local"
    **/
  public java.lang.Integer ejbCreate (java.lang.Integer DB_CODE, 
		  java.lang.Integer DB_RFRNC_ID, String DB_CLNT_CODE, String DB_CLNT_NM, 
		  String DB_TIN, String DB_ADDRSS,  String DB_MNGNG_BRNCH, 
		  Integer DB_AD_CMPNY)
      throws CreateException {

      Debug.print("HrDeployedBranchBean ejbCreate");
      
      setDbCode(DB_CODE);
      setDbReferenceID(DB_RFRNC_ID);
      setDbClientCode(DB_CLNT_CODE);
      setDbClientName(DB_CLNT_NM);
      setDbTin(DB_TIN);
      setDbAddress(DB_ADDRSS);
      setDbManagingBranch(DB_MNGNG_BRNCH);
      setDbAdCompany(DB_AD_CMPNY);
      
      return null;
   }

  /**
    * @ejb:create-method view-type="local"
    **/
   public java.lang.Integer ejbCreate (
		   java.lang.Integer DB_RFRNC_ID, String DB_CLNT_CODE, String DB_CLNT_NM,
		   String DB_TIN, String DB_ADDRSS,  String DB_MNGNG_BRNCH, 
		   Integer DB_AD_CMPNY)
      throws CreateException {

      Debug.print("HrDeployedBranchBean ejbCreate");
     
      setDbReferenceID(DB_RFRNC_ID);
      setDbClientCode(DB_CLNT_CODE);
      setDbClientName(DB_CLNT_NM);
      setDbTin(DB_TIN);
      setDbAddress(DB_ADDRSS);
      setDbManagingBranch(DB_MNGNG_BRNCH);
      setDbAdCompany(DB_AD_CMPNY);

      return null;
   }

   public void ejbPostCreate (java.lang.Integer DB_CODE, 
			  java.lang.Integer DB_RFRNC_ID, String DB_CLNT_CODE, String DB_CLNT_NM,
			  String DB_TIN, String DB_ADDRSS,  String DB_MNGNG_BRNCH, 
			  Integer DB_AD_CMPNY)
      throws CreateException { }

    public void ejbPostCreate (
    		java.lang.Integer DB_RFRNC_ID, String DB_CLNT_CODE, String DB_CLNT_NM,
    		String DB_TIN, String DB_ADDRSS,  String DB_MNGNG_BRNCH, 
    		Integer DB_AD_CMPNY)
      throws CreateException { }

} // HrDeployedBranchBean class
