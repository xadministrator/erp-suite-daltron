/*
 * com/ejb/hr/LocalHrEmployeeBean.java
 *
 * Created on February 21, 2003, 12:55 PM
 */

package com.ejb.hr;

import java.util.Collection;
import java.util.Date;

import javax.ejb.CreateException;
import javax.ejb.EJBException;

import com.util.AbstractEntityBean;
import com.util.Debug;

/**
 * @ejb:bean name="HrEmployeeEJB"
 *           display-name="Employee Entity"
 *           type="CMP"
 *           cmp-version="2.x"
 *           view-type="local"
 *           local-jndi-name="omega-ejb/HrEmployeeEJB"
 *           schema="HrEmployee"
 *           primkey-field="empCode"
 *       
 * @ejb:pk class="java.lang.Integer"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="hruser"
 *                        role-link="hruserlink"
 *
 * @ejb:permission role-name="hruser"
 *
 * @ejb:interface local-class="com.ejb.hr.LocalHrEmployee"
 *                local-extends="javax.ejb.EJBLocalObject"
 *
 * @ejb:home local-class="com.ejb.hr.LocalHrEmployeeHome"
 *           local-extends="javax.ejb.EJBLocalHome"
 *
 * @ejb:finder signature="Collection findEmpAll(java.lang.Integer EMP_AD_CMPNY)"
 *             query="SELECT OBJECT(emp) FROM HrEmployee emp WHERE emp.empAdCompany = ?1"
 *
 * @jboss:query signature="Collection findEmpAll(java.lang.Integer EMP_AD_CMPNY)"
 *             query="SELECT OBJECT(emp) FROM HrEmployee emp WHERE emp.empAdCompany = ?1 ORDER BY emp.empEmployeeNumber"
 *
 * @ejb:finder signature="Collection findEmpAllEmpNumberAndBioNo(java.lang.Integer EMP_AD_CMPNY)"
 *             query="SELECT OBJECT(emp) FROM HrEmployee emp WHERE emp.empEmployeeNumber IS NOT NULL AND emp.empBioNumber IS NOT NULL AND emp.empAdCompany = ?1"
 *
 * @jboss:query signature="Collection findEmpAllEmpNumberAndBioNo(java.lang.Integer EMP_AD_CMPNY)"
 *             query="SELECT OBJECT(emp) FROM HrEmployee emp WHERE emp.empEmployeeNumber IS NOT NULL AND emp.empBioNumber IS NOT NULL AND emp.empAdCompany = ?1 ORDER BY emp.empBioNumber"
 *
 * @ejb:finder signature="LocalHrEmployee findByReferenceID(java.lang.Integer EMP_REF_ID, java.lang.Integer EMP_AD_CMPNY)"
 *             query="SELECT OBJECT(emp) FROM HrEmployee emp WHERE emp.empReferenceID=?1 AND emp.empAdCompany=?2"
 *  
 * @ejb:finder signature="LocalHrEmployee findByEmpEmployeeNumber(java.lang.String EMP_NMBR, java.lang.Integer EMP_AD_CMPNY)"
 *             query="SELECT OBJECT(emp) FROM HrEmployee emp WHERE emp.empEmployeeNumber=?1 AND emp.empAdCompany=?2"
 * 
 * @ejb:finder signature="LocalHrEmployee findByEmpBioNumber(java.lang.String EMP_BIO_NMBR, java.lang.Integer EMP_AD_CMPNY)"
 *             query="SELECT OBJECT(emp) FROM HrEmployee emp WHERE emp.empBioNumber=?1 AND emp.empAdCompany=?2"
 *             
 * @ejb:value-object match="*"
 * 						name="HrEmployee"
 * 
 * @jboss:persistence table-name="HR_EMPLYEE"
 *
 * @jboss:entity-command name="mysql-get-generated-keys"
 *
 */

public abstract class HrEmployeeBean extends AbstractEntityBean {


   // Access methods for persistent fields

   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    * @ejb:pk-field
    *
    * @jboss:column-name name="EMP_CODE"
    *
    * @jboss:persistence auto-increment="true"
    **/
   public abstract java.lang.Integer getEmpCode();
   public abstract void setEmpCode(java.lang.Integer EMP_CODE);
   
   /**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="EMP_RFRNC_ID"
	 **/
	public abstract Integer getEmpReferenceID();
	/**
	 * @ejb:interface-method view-type="local"
	 **/ 
	public abstract void setEmpReferenceID(Integer EMP_RFRNC_ID);
	
	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"   
	 *
	 * @jboss:column-name name="EMP_EMPLYEE_NMBR"
	 **/
	public abstract String getEmpEmployeeNumber();
	/**
	 * @ejb:interface-method view-type="local"
	 **/ 
	public abstract void setEmpEmployeeNumber(String EMP_EMPLYEE_NMBR);
	
	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"   
	 *
	 * @jboss:column-name name="EMP_BIO_NMBR"
	 **/
	public abstract String getEmpBioNumber();
	/**
	 * @ejb:interface-method view-type="local"
	 **/ 
	public abstract void setEmpBioNumber(String EMP_BIO_NMBR);

   /**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"   
	 *
	 * @jboss:column-name name="EMP_FRST_NM"
	 **/
	public abstract String getEmpFirstName();
	/**
	 * @ejb:interface-method view-type="local"
	 **/ 
	public abstract void setEmpFirstName(String EMP_FRST_NM);
	
	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"   
	 *
	 * @jboss:column-name name="EMP_MDDL_NM"
	 **/
	public abstract String getEmpMiddleName();
	/**
	 * @ejb:interface-method view-type="local"
	 **/ 
	public abstract void setEmpMiddleName(String EMP_MDDL_NM);
	
	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"   
	 *
	 * @jboss:column-name name="EMP_LST_NM"
	 **/
	public abstract String getEmpLastName();
	/**
	 * @ejb:interface-method view-type="local"
	 **/ 
	public abstract void setEmpLastName(String EMP_LST_NM);
	
	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"   
	 *
	 * @jboss:column-name name="EMP_CRRNT_JB_PSTN"
	 **/
	public abstract String getEmpCurrentJobPosition();
	/**
	 * @ejb:interface-method view-type="local"
	 **/ 
	public abstract void setEmpCurrentJobPosition(String EMP_CRRNT_JB_PSTN);
	
	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"   
	 *
	 * @jboss:column-name name="EMP_MNGNG_BRNCH"
	 **/
	public abstract String getEmpManagingBranch();
	/**
	 * @ejb:interface-method view-type="local"
	 **/ 
	public abstract void setEmpManagingBranch(String EMP_MNGNG_BRNCH);


   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="EMP_AD_CMPNY"
    **/
   public abstract Integer getEmpAdCompany();
   /**
    * @ejb:interface-method view-type="local"
    **/
   public abstract void setEmpAdCompany(Integer EMP_AD_CMPNY);
   
   
   // RELATIONSHIP METHODS
   
   
   /**
	 * @ejb:interface-method view-type="local"
	 * @ejb:relation name="deployedbranch-employees"
	 *               role-name="employee-has-one-deployedbranch"
	 *
	 * @jboss:relation related-pk-field="dbCode"
	 *                 fk-column="HR_DEPLOYED_BRANCH"
	 */
	public abstract LocalHrDeployedBranch getHrDeployedBranch();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setHrDeployedBranch(LocalHrDeployedBranch hrDeployedBranch);
	
	
	/**
	 * @ejb:interface-method view-type="local"
	 * @ejb:relation name="employee-customers"
	 *               role-name="employee-has-many-customers"
	 * 
	 */
	public abstract Collection getArCustomers();
	public abstract void setArCustomers(Collection arCustomers);
	
	
	
	// BUSINESS METHODS
	


	
	
   // EntityBean methods

   /**
    * @ejb:create-method view-type="local"
    **/
  public java.lang.Integer ejbCreate (java.lang.Integer EMP_CODE, 
		  java.lang.Integer EMP_RFRNC_ID, String EMP_EMPLYEE_NMBR, String EMP_BIO_NMBR,  
		  String EMP_FRST_NM, String EMP_MDDL_NM, String EMP_LST_NM, String EMP_CRRNT_JB_PSTN, String EMP_MNGNG_BRNCH,
		  Integer EMP_AD_CMPNY)
      throws CreateException {

      Debug.print("HrEmployeeBean ejbCreate");
      
      setEmpCode(EMP_CODE);
      setEmpReferenceID(EMP_RFRNC_ID);
      setEmpEmployeeNumber(EMP_EMPLYEE_NMBR);
      setEmpBioNumber(EMP_BIO_NMBR);
      setEmpFirstName(EMP_FRST_NM);
      setEmpMiddleName(EMP_MDDL_NM);
      setEmpLastName(EMP_LST_NM);
      setEmpCurrentJobPosition(EMP_CRRNT_JB_PSTN);
      setEmpManagingBranch(EMP_MNGNG_BRNCH);
      setEmpAdCompany(EMP_AD_CMPNY);
      
      return null;
   }

  /**
    * @ejb:create-method view-type="local"
    **/
   public java.lang.Integer ejbCreate (
		   java.lang.Integer EMP_RFRNC_ID, String EMP_EMPLYEE_NMBR, String EMP_BIO_NMBR,  
			  String EMP_FRST_NM, String EMP_MDDL_NM, String EMP_LST_NM, String EMP_CRRNT_JB_PSTN, String EMP_MNGNG_BRNCH,
			  Integer EMP_AD_CMPNY)
      throws CreateException {

      Debug.print("HrEmployeeBean ejbCreate");
     
      setEmpReferenceID(EMP_RFRNC_ID);
      setEmpEmployeeNumber(EMP_EMPLYEE_NMBR);
      setEmpBioNumber(EMP_BIO_NMBR);
      setEmpFirstName(EMP_FRST_NM);
      setEmpMiddleName(EMP_MDDL_NM);
      setEmpLastName(EMP_LST_NM);
      setEmpCurrentJobPosition(EMP_CRRNT_JB_PSTN);
      setEmpManagingBranch(EMP_MNGNG_BRNCH);
      setEmpAdCompany(EMP_AD_CMPNY);

      return null;
   }

   public void ejbPostCreate (java.lang.Integer EMP_CODE, 
			  java.lang.Integer EMP_RFRNC_ID, String EMP_EMPLYEE_NMBR, String EMP_BIO_NMBR,  
			  String EMP_FRST_NM, String EMP_MDDL_NM, String EMP_LST_NM, String EMP_CRRNT_JB_PSTN, String EMP_MNGNG_BRNCH, 
			  Integer EMP_AD_CMPNY)
      throws CreateException { }

    public void ejbPostCreate (
    		java.lang.Integer EMP_RFRNC_ID, String EMP_EMPLYEE_NMBR, String EMP_BIO_NMBR,  
			  String EMP_FRST_NM, String EMP_MDDL_NM, String EMP_LST_NM, String EMP_CRRNT_JB_PSTN, String EMP_MNGNG_BRNCH, 
			  Integer EMP_AD_CMPNY)
      throws CreateException { }

} // HrEmployeeBean class
