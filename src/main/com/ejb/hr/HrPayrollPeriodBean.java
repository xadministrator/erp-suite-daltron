/*
 * com/ejb/hr/LocalHrPayrollPeriodBean.java
 *
 * Created on February 21, 2003, 12:55 PM
 */

package com.ejb.hr;

import java.util.Collection;
import java.util.Date;

import javax.ejb.CreateException;
import javax.ejb.EJBException;

import com.ejb.ar.LocalArAppliedInvoice;
import com.ejb.ar.LocalArInvoice;
import com.ejb.ar.LocalArInvoiceBatch;
import com.ejb.ar.LocalArReceipt;
import com.ejb.cm.LocalCmAdjustment;
import com.util.AbstractEntityBean;
import com.util.Debug;

/**
 * @ejb:bean name="HrPayrollPeriodEJB"
 *           display-name="Payroll Period Entity"
 *           type="CMP"
 *           cmp-version="2.x"
 *           view-type="local"
 *           local-jndi-name="omega-ejb/HrPayrollPeriodEJB"
 *           schema="HrPayrollPeriod"
 *           primkey-field="ppCode"
 *       
 * @ejb:pk class="java.lang.Integer"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="hruser"
 *                        role-link="hruserlink"
 *
 * @ejb:permission role-name="hruser"
 *
 * @ejb:interface local-class="com.ejb.hr.LocalHrPayrollPeriod"
 *                local-extends="javax.ejb.EJBLocalObject"
 *
 * @ejb:home local-class="com.ejb.hr.LocalHrPayrollPeriodHome"
 *           local-extends="javax.ejb.EJBLocalHome"
 *
 * @ejb:finder signature="Collection findPpAll(java.lang.Integer PP_AD_CMPNY)"
 *             query="SELECT OBJECT(pp) FROM HrPayrollPeriod pp WHERE pp.ppAdCompany = ?1"
 *
 * @jboss:query signature="Collection findPpAll(java.lang.Integer PP_AD_CMPNY)"
 *             query="SELECT OBJECT(pp) FROM HrPayrollPeriod pp WHERE pp.ppAdCompany = ?1 ORDER BY pp.ppName"
 *
 * @ejb:finder signature="LocalHrPayrollPeriod findByReferenceID(java.lang.Integer PP_REF_ID, java.lang.Integer PP_AD_CMPNY)"
 *             query="SELECT OBJECT(pp) FROM HrPayrollPeriod pp WHERE pp.ppReferenceID=?1 AND pp.ppAdCompany=?2"
 *  
 * @ejb:finder signature="LocalHrPayrollPeriod findByPpName(java.lang.String PP_NM, java.lang.Integer PP_AD_CMPNY)"
 *             query="SELECT OBJECT(pp) FROM HrPayrollPeriod pp WHERE pp.ppName=?1 AND pp.ppAdCompany=?2" 
 *  
 * @ejb:value-object match="*"
 * 						name="HrPayrollPeriod"
 * 
 * @jboss:persistence table-name="HR_PYRLL_PRD"
 *
 * @jboss:entity-command name="mysql-get-generated-keys"
 *
 */

public abstract class HrPayrollPeriodBean extends AbstractEntityBean {


   // Access methods for persistent fields

   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    * @ejb:pk-field
    *
    * @jboss:column-name name="PP_CODE"
    *
    * @jboss:persistence auto-increment="true"
    **/
   public abstract java.lang.Integer getPpCode();
   public abstract void setPpCode(java.lang.Integer PP_CODE);
   
   /**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="PP_RFRNC_ID"
	 **/
	public abstract Integer getPpReferenceID();
	/**
	 * @ejb:interface-method view-type="local"
	 **/ 
	public abstract void setPpReferenceID(Integer PP_RFRNC_ID);
	
	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"   
	 *
	 * @jboss:column-name name="PP_TYP"
	 **/
	public abstract String getPpType();
	/**
	 * @ejb:interface-method view-type="local"
	 **/ 
	public abstract void setPpType(String PP_TYP);
   
   /**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"   
	 *
	 * @jboss:column-name name="PP_NM"
	 **/
	public abstract String getPpName();
	/**
	 * @ejb:interface-method view-type="local"
	 **/ 
	public abstract void setPpName(String PP_NM);

   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="PP_DT_FRM"
    **/
   public abstract Date getPpDateFrom();
   /**
	 * @ejb:interface-method view-type="local"
	 **/
   public abstract void setPpDateFrom(Date PP_DT_FRM);
   
   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="PP_DT_TO"
    **/
   public abstract Date getPpDateTo();
   /**
	 * @ejb:interface-method view-type="local"
	 **/
   public abstract void setPpDateTo(Date PP_DT_TO);
   
   /**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"   
	 *
	 * @jboss:column-name name="PP_STAT"
	 **/
	public abstract String getPpStatus();
	/**
	 * @ejb:interface-method view-type="local"
	 **/ 
	public abstract void setPpStatus(String PP_STAT);

   
   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="PP_AD_CMPNY"
    **/
   public abstract Integer getPpAdCompany();
   /**
    * @ejb:interface-method view-type="local"
    **/
   public abstract void setPpAdCompany(Integer PP_AD_CMPNY);
   
   
   // RELATIONSHIP METHODS 
   
   /**
    * @ejb:interface-method view-type="local"
    * @ejb:relation name="payrollperiod-receipts"
    *               role-name="payrollperiod-has-many-receipts"
    */
   public abstract Collection getArReceipts();
   public abstract void setArReceipts(Collection arReceipts);

	   
   
   /**
    * @ejb:interface-method view-type="local"
    * @ejb:relation name="payrollperiod-adjustments"
    *               role-name="customer-has-many-adjustment"
    * 
    */
   public abstract Collection getCmAdjustments();
   public abstract void setCmAdjustments(Collection cmAdjustments);
	   
	
	// BUSINESS METHODS
	

/**
    * @ejb:interface-method view-type="local"
    **/
   public void addArReceipt(LocalArReceipt arReceipt) {

      Debug.print("HrPayrollPeriodBean addArReceipt");
      try {
         Collection arReceipts = getArReceipts();
         arReceipts.add(arReceipt);
      } catch (Exception ex) {
         throw new EJBException(ex.getMessage());
      }
   }

   /**
    * @ejb:interface-method view-type="local"
    **/
   public void dropArReceipt(LocalArReceipt arReceipt) {

      Debug.print("HrPayrollPeriodBean dropArReceipt");
      try {
         Collection arReceipts = getArReceipts();
         arReceipts.remove(arReceipt);
      } catch (Exception ex) {
         throw new EJBException(ex.getMessage());
      }
   }
   
   
   
   /**
    * @ejb:interface-method view-type="local"
    **/
   public void addCmAdjustment(LocalCmAdjustment cmAdjustment) {

        Debug.print("HrPayrollPeriodBean addCmAdjustment");
     
        try {
     	
           Collection cmAdjustments = getCmAdjustments();
           cmAdjustments.add(cmAdjustment);
	          
        } catch (Exception ex) {
     	
          throw new EJBException(ex.getMessage());
     
        }   
    }

   /**
    * @ejb:interface-method view-type="local"
    **/
   public void dropCmAdjustment(LocalCmAdjustment cmAdjustment) {

        Debug.print("HrPayrollPeriodBean Drop dropCmAdjustment");
     
        try {
     	
           Collection cmAdjustments = getCmAdjustments();
           cmAdjustments.remove(cmAdjustment);
	      
        } catch (Exception ex) {
     	
          throw new EJBException(ex.getMessage());
        
        }   
    }
	
	
   // EntityBean methods

   /**
    * @ejb:create-method view-type="local"
    **/
  public java.lang.Integer ejbCreate (java.lang.Integer PP_CODE, 
		  java.lang.Integer PP_RFRNC_ID, String PP_TYP, String PP_NM, Date PP_DT_FRM, Date PP_DT_TO, 
		  String PP_STAT, Integer PP_AD_CMPNY)
      throws CreateException {

      Debug.print("HrPayrollPeriodBean ejbCreate");
      
      setPpCode(PP_CODE);
      setPpReferenceID(PP_RFRNC_ID);
      setPpType(PP_TYP);
      setPpName(PP_NM);
      setPpDateFrom(PP_DT_FRM);
      setPpDateTo(PP_DT_TO);
      setPpStatus(PP_STAT);
      setPpAdCompany(PP_AD_CMPNY);
      
      return null;
   }

  /**
    * @ejb:create-method view-type="local"
    **/
   public java.lang.Integer ejbCreate (
		   java.lang.Integer PP_RFRNC_ID, String PP_TYP, String PP_NM, Date PP_DT_FRM, Date PP_DT_TO, 
			  String PP_STAT, Integer PP_AD_CMPNY)
      throws CreateException {

      Debug.print("HrPayrollPeriodBean ejbCreate");
     
      setPpReferenceID(PP_RFRNC_ID);
      setPpType(PP_TYP);
      setPpName(PP_NM);
      setPpDateFrom(PP_DT_FRM);
      setPpDateTo(PP_DT_TO);
      setPpStatus(PP_STAT);
      setPpAdCompany(PP_AD_CMPNY);

      return null;
   }

   public void ejbPostCreate (java.lang.Integer PP_CODE, 
			  java.lang.Integer PP_RFRNC_ID, String PP_TYP, String PP_NM, Date PP_DT_FRM, Date PP_DT_TO, 
			  String PP_STAT, Integer PP_AD_CMPNY)
      throws CreateException { }

    public void ejbPostCreate (
    		java.lang.Integer PP_RFRNC_ID, String PP_TYP, String PP_NM, Date PP_DT_FRM, Date PP_DT_TO, 
			  String PP_STAT, Integer PP_AD_CMPNY)
      throws CreateException { }

} // HrPayrollPeriodBean class
