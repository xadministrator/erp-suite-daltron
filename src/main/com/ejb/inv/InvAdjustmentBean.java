package com.ejb.inv;

import java.util.Collection;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;

import com.ejb.ap.LocalApSupplier;
import com.util.AbstractEntityBean;
import com.util.Debug;

/**
 * @ejb:bean name="InvAdjustmentEJB"
 *           display-name="Adjustment Entity"
 *           type="CMP"
 *           cmp-version="2.x"
 *           view-type="local"
 *           local-jndi-name="omega-ejb/InvAdjustmentEJB"
 *           schema="InvAdjustment"
 *           primkey-field="adjCode"
 *       
 * @ejb:pk class="java.lang.Integer"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="invuser"
 *                        role-link="invuserlink"
 *
 * @ejb:permission role-name="invuser"
 *
 * @ejb:interface local-class="com.ejb.inv.LocalInvAdjustment"
 *                local-extends="javax.ejb.EJBLocalObject"
 *
 * @ejb:home local-class="com.ejb.inv.LocalInvAdjustmentHome"
 *           local-extends="javax.ejb.EJBLocalHome"
 * 
 * @ejb:select signature="Collection ejbSelectGeneric(java.lang.String jbossQl, java.lang.Object[] args)"
 *             result-type-mapping="Local"
 *             method-intf="LocalHome"
 *             query="SELECT OBJECT(adj) FROM InvAdjusment adj"
 *
 * @jboss:query signature="Collection ejbSelectGeneric(java.lang.String jbossQl, java.lang.Object[] args)"
 *                         dynamic="true"
 * 
 * @ejb:finder signature="Collection findPostedAdjByAdjDateRange(java.util.Date ADJ_DT_FRM, java.util.Date ADJ_DT_TO, java.lang.Integer ADJ_AD_CMPNY)"
 *             query="SELECT OBJECT(adj) FROM InvAdjustment adj WHERE adj.adjPosted = 1 AND adj.adjDate >= ?1 AND adj.adjDate <= ?2 AND adj.adjAdCompany = ?3"
 *
 * @jboss:query signature="Collection findPostedAdjByAdjDateRange(java.util.Date ADJ_DT_FRM, java.util.Date ADJ_DT_TO, java.lang.Integer ADJ_AD_CMPNY)"
 *             query="SELECT OBJECT(adj) FROM InvAdjustment adj WHERE adj.adjPosted = 1 AND adj.adjDate >= ?1 AND adj.adjDate <= ?2 AND adj.adjAdCompany = ?3 ORDER BY adj.adjDate"
 * 
 * @ejb:finder signature="Collection findDraftAdjAll(java.lang.Integer ADJ_AD_CMPNY)"
 *             query="SELECT OBJECT(adj) FROM InvAdjustment adj WHERE adj.adjApprovalStatus IS NULL AND adj.adjAdCompany = ?1"
 * 
 * @ejb:finder signature="Collection findDraftAdjByBrCode(java.lang.Integer ADJ_AD_BRNCH, java.lang.Integer ADJ_AD_CMPNY)"
 *             query="SELECT OBJECT(adj) FROM InvAdjustment adj WHERE adj.adjApprovalStatus IS NULL AND adj.adjAdBranch = ?1 AND adj.adjAdCompany = ?2"
 *
 * @ejb:finder signature="Collection findUnpostedInvAdjByInvAdjDateRange(java.util.Date ADJ_DT_FRM, java.util.Date ADJ_DT_TO, java.lang.Integer ADJ_AD_CMPNY)"
 *             query="SELECT OBJECT(adj) FROM InvAdjustment adj  WHERE adj.adjPosted = 0 AND adj.adjDate >= ?1 AND adj.adjDate <= ?2 AND adj.adjAdCompany = ?3"
 *
 * @jboss:query signature="Collection findUnpostedInvAdjByInvAdjDateRange(java.util.Date ADJ_DT_FRM, java.util.Date ADJ_DT_TO, java.lang.Integer ADJ_AD_CMPNY)"
 *             query="SELECT OBJECT(adj) FROM InvAdjustment adj  WHERE adj.adjPosted = 0 AND adj.adjDate >= ?1 AND adj.adjDate <= ?2 AND adj.adjAdCompany = ?3 ORDER BY adj.adjDate" 
 *
 * @ejb:finder signature="Collection findUnpostedInvAdjByInvAdjDateRangeByBranch(java.util.Date ADJ_DT_FRM, java.util.Date ADJ_DT_TO, java.lang.Integer ADJ_AD_BRNCH, java.lang.Integer ADJ_AD_CMPNY)"
 *             query="SELECT OBJECT(adj) FROM InvAdjustment adj  WHERE adj.adjPosted = 0 AND adj.adjDate >= ?1 AND adj.adjDate <= ?2 AND adj.adjAdBranch = ?3 AND adj.adjAdCompany = ?4"
 *
 * @jboss:query signature="Collection findUnpostedInvAdjByInvAdjDateRangeByBranch(java.util.Date ADJ_DT_FRM, java.util.Date ADJ_DT_TO, java.lang.Integer ADJ_AD_BRNCH, java.lang.Integer ADJ_AD_CMPNY)"
 *             query="SELECT OBJECT(adj) FROM InvAdjustment adj  WHERE adj.adjPosted = 0 AND adj.adjDate >= ?1 AND adj.adjDate <= ?2 AND adj.adjAdBranch = ?3 AND adj.adjAdCompany = ?4 ORDER BY adj.adjDate" 
 *
 * @ejb:finder signature="LocalInvAdjustment findByAdjDocumentNumberAndBrCode(java.lang.String ADJ_DCMNT_NMBR, java.lang.Integer ADJ_AD_BRNCH, java.lang.Integer ADJ_AD_CMPNY)"
 *             query="SELECT OBJECT(adj) FROM InvAdjustment adj  WHERE adj.adjDocumentNumber = ?1 AND adj.adjAdBranch = ?2 AND adj.adjAdCompany = ?3"
 *
 * @ejb:finder signature="LocalInvAdjustment findByAdjReferenceNumberAndAdjDateAndBrCode(java.lang.String ADJ_RFRNC_NMBR, java.util.Date ADJ_DT, java.lang.Integer AD_BRNCH, java.lang.Integer AD_CMPNY)"
 * 			   query="SELECT OBJECT(adj) FROM InvAdjustment adj WHERE adj.adjReferenceNumber = ?1 AND adj.adjDate = ?2 AND adj.adjAdBranch = ?3 AND adj.adjAdCompany = ?4"
 * 
 * @ejb:finder signature="Collection findByAdjReferenceNumber(java.lang.String ADJ_DCMNT_NMBR, java.lang.Integer AD_BRNCH, java.lang.Integer AD_CMPNY)"
 * 			   query="SELECT OBJECT(adj) FROM InvAdjustment adj WHERE adj.adjDocumentNumber = ?1  AND adj.adjAdBranch = ?2 AND adj.adjAdCompany = ?3"
 * 
 * 
 * @ejb:finder signature="Collection findUnpostedAdjByAdjCoaAccountNumberAndBrCode(java.lang.Integer COA_CODE, java.lang.Integer ADJ_AD_BRNCH, java.lang.Integer ADJ_AD_CMPNY)"
 *             query="SELECT OBJECT(adj) FROM InvAdjustment adj WHERE adj.adjPosted=0 AND adj.glChartOfAccount.coaCode=?1 AND adj.adjAdBranch = ?2 AND adj.adjAdCompany = ?3"
 *  
 * @ejb:finder signature="Collection findAdjRequestToGenerateByAdCompanyAndBrCode(java.lang.Integer ADJ_AD_BRNCH, java.lang.Integer ADJ_AD_CMPNY)"
 *             query="SELECT OBJECT(adj) FROM InvAdjustment adj WHERE adj.adjGenerated=0 AND adj.adjType='REQUEST' AND (adj.adjApprovalStatus='APPROVED' OR adj.adjApprovalStatus='N/A') AND adj.adjPosted=1 AND adj.adjAdBranch = ?1 AND adj.adjAdCompany = ?2"
 * 
 * @ejb:value-object match="*"
 *             name="InvAdjustment"
 *
 * @jboss:persistence table-name="INV_ADJSTMNT"
 *
 * @jboss:entity-command name="mysql-get-generated-keys"
 *
 */

public abstract class InvAdjustmentBean extends AbstractEntityBean {
	
//	 Access methods for persistent fields

	   /**
	    * @ejb:persistent-field
	    * @ejb:interface-method view-type="local"
	    * @ejb:pk-field
	    *
	    * @jboss:column-name name="ADJ_CODE"
	    *
	    * @jboss:persistence auto-increment="true"
	    **/
	   public abstract java.lang.Integer getAdjCode();
	   public abstract void setAdjCode(java.lang.Integer ADJ_CODE);
	   
	   /**
	    * @ejb:persistent-field
	    * @ejb:interface-method view-type="local"
	    *
	    * @jboss:column-name name="ADJ_DCMNT_NMBR"
	    **/
	   public abstract java.lang.String getAdjDocumentNumber();
	   /**
	    * @ejb:interface-method view-type="local"
	    **/
	   public abstract void setAdjDocumentNumber(java.lang.String ADJ_DCMNT_NMBR);

	   /**
	    * @ejb:persistent-field
	    * @ejb:interface-method view-type="local"
	    *
	    * @jboss:column-name name="ADJ_RFRNC_NMBR"
	    **/
	   public abstract java.lang.String getAdjReferenceNumber();
	   /**
	    * @ejb:interface-method view-type="local"
	    **/
	   public abstract void setAdjReferenceNumber(java.lang.String ADJ_RFRNC_NMBR);
	   
	   /**
	    * @ejb:persistent-field
	    * @ejb:interface-method view-type="local"
	    *
	    * @jboss:column-name name="ADJ_DESC"
	    **/
	   public abstract java.lang.String getAdjDescription();
	   /**
	    * @ejb:interface-method view-type="local"
	    **/
	   public abstract void setAdjDescription(java.lang.String ADJ_DESC);	   
	   
	   /**
	    * @ejb:persistent-field
	    * @ejb:interface-method view-type="local"
	    *
	    * @jboss:column-name name="ADJ_DT"
	    **/
	   public abstract java.util.Date getAdjDate();
	   /**
	    * @ejb:interface-method view-type="local"
	    **/
	   public abstract void setAdjDate(java.util.Date ADJ_DT);
	   
	   /**
	    * @ejb:persistent-field
	    * @ejb:interface-method view-type="local"
	    *
	    * @jboss:column-name name="ADJ_TYP"
	    **/
	   public abstract String getAdjType();
	   /**
	    * @ejb:interface-method view-type="local"
	    **/
	   public abstract void setAdjType(String ADJ_TYP);
	   
	   /**
	    * @ejb:persistent-field
	    * @ejb:interface-method view-type="local"
	    *
	    * @jboss:column-name name="ADJ_NT_BY"
	    **/
	   public abstract String getAdjNotedBy();
	   /**
	    * @ejb:interface-method view-type="local"
	    **/
	   public abstract void setAdjNotedBy(String ADJ_NT_BY);
	   
	   /**
	    * @ejb:persistent-field
	    * @ejb:interface-method view-type="local"
	    *
	    * @jboss:column-name name="ADJ_APPRVL_STATUS"
	    **/
	   public abstract java.lang.String getAdjApprovalStatus();
	   /**
	    * @ejb:interface-method view-type="local"
	    **/
	   public abstract void setAdjApprovalStatus(java.lang.String ADJ_APPRVL_STATUS);
	   
	   /**
	    * @ejb:persistent-field
	    * @ejb:interface-method view-type="local"
	    *
	    * @jboss:column-name name="ADJ_PSTD"
	    **/
	   public abstract byte getAdjPosted();
	   /**
	    * @ejb:interface-method view-type="local"
	    **/
	   public abstract void setAdjPosted(byte ADJ_PSTD);
	   
	   /**
	    * @ejb:persistent-field
	    * @ejb:interface-method view-type="local"
	    *
	    * @jboss:column-name name="ADJ_GNRTD"
	    **/
	   public abstract byte getAdjGenerated();
	   /**
	    * @ejb:interface-method view-type="local"
	    **/
	   public abstract void setAdjGenerated(byte ADJ_GNRTD);
	   
	   /**
	    * @ejb:persistent-field
	    * @ejb:interface-method view-type="local"
	    *
	    * @jboss:column-name name="ADJ_CRTD_BY"
	    **/
	   public abstract String getAdjCreatedBy();
	   /**
	    * @ejb:interface-method view-type="local"
	    **/
	   public abstract void setAdjCreatedBy(String ADJ_CRTD_BY);
	   
	   /**
	    * @ejb:persistent-field
	    * @ejb:interface-method view-type="local"
	    *
	    * @jboss:column-name name="ADJ_DT_CRTD"
	    **/
	   public abstract java.util.Date getAdjDateCreated();
	   /**
	    * @ejb:interface-method view-type="local"
	    **/
	   public abstract void setAdjDateCreated(java.util.Date ADJ_DT_CRTD);
	   
	   /**
	    * @ejb:persistent-field
	    * @ejb:interface-method view-type="local"
	    *
	    * @jboss:column-name name="ADJ_LST_MDFD_BY"
	    **/
	   public abstract String getAdjLastModifiedBy();
	   /**
	    * @ejb:interface-method view-type="local"
	    **/
	   public abstract void setAdjLastModifiedBy(String ADJ_LST_MDFD_BY);
	   
	   /**
	    * @ejb:persistent-field
	    * @ejb:interface-method view-type="local"
	    *
	    * @jboss:column-name name="ADJ_DT_LST_MDFD"
	    **/
	   public abstract java.util.Date getAdjDateLastModified();
	   /**
	    * @ejb:interface-method view-type="local"
	    **/
	   public abstract void setAdjDateLastModified(java.util.Date ADJ_DT_LST_MDFD);
	   
	   /**
	    * @ejb:persistent-field
	    * @ejb:interface-method view-type="local"
	    *
	    * @jboss:column-name name="ADJ_APPRVD_RJCTD_BY"
	    **/
	   public abstract String getAdjApprovedRejectedBy();
	   /**
	    * @ejb:interface-method view-type="local"
	    **/
	   public abstract void setAdjApprovedRejectedBy(String ADJ_APPRVD_RJCTD_BY);
	   
	   /**
	    * @ejb:persistent-field
	    * @ejb:interface-method view-type="local"
	    *
	    * @jboss:column-name name="ADJ_DT_APPRVD_RJCTD"
	    **/
	   public abstract java.util.Date getAdjDateApprovedRejected();
	   /**
	    * @ejb:interface-method view-type="local"
	    **/
	   public abstract void setAdjDateApprovedRejected(java.util.Date ADJ_DT_APPRVD_RJCTD);
	   
	   /**
	    * @ejb:persistent-field
	    * @ejb:interface-method view-type="local"
	    *
	    * @jboss:column-name name="ADJ_PSTD_BY"
	    **/
	   public abstract String getAdjPostedBy();
	   /**
	    * @ejb:interface-method view-type="local"
	    **/
	   public abstract void setAdjPostedBy(String ADJ_PSTD_BY);
	   
	   /**
	    * @ejb:persistent-field
	    * @ejb:interface-method view-type="local"
	    *
	    * @jboss:column-name name="ADJ_DT_PSTD"
	    **/
	   public abstract java.util.Date getAdjDatePosted();
	   /**
	    * @ejb:interface-method view-type="local"
	    **/
	   public abstract void setAdjDatePosted(java.util.Date ADJ_DT_PSTD);
	   
	   /**
	     * @ejb:persistent-field
	     * @ejb:interface-method view-type="local"
	     *
	     * @jboss:column-name name="ADJ_RSN_FR_RJCTN"
	     **/
       public abstract String getAdjReasonForRejection();
       /**
        * @ejb:interface-method view-type="local"
        **/
       public abstract void setAdjReasonForRejection(String ADJ_RSN_FR_RJCTN);  
       
	   /**
	    * @ejb:persistent-field
	    * @ejb:interface-method view-type="local"
	    *
	    * @jboss:column-name name="ADJ_IS_CST_VRNC"
	    **/
	   public abstract byte getAdjIsCostVariance();
	   /**
	    * @ejb:interface-method view-type="local"
	    **/
	   public abstract void setAdjIsCostVariance(byte ADJ_IS_CST_VRNC);
	   
	   /**
	    * @ejb:persistent-field
	    * @ejb:interface-method view-type="local"
	    *
	    * @jboss:column-name name="ADJ_VD"
	    **/
	   public abstract byte getAdjVoid();
	   /**
	    * @ejb:interface-method view-type="local"
	    **/
	   public abstract void setAdjVoid(byte ADJ_VD);
       
       /**
        * @ejb:persistent-field
        * @ejb:interface-method view-type="local"
        *
        * @jboss:column-name name="ADJ_AD_BRNCH"
        **/
       public abstract Integer getAdjAdBranch();
       /**
        * @ejb:interface-method view-type="local"
        **/
       public abstract void setAdjAdBranch(Integer ADJ_AD_BRNCH); 
       
       /**
        * @ejb:persistent-field
        * @ejb:interface-method view-type="local"
        *
        * @jboss:column-name name="ADJ_AD_CMPNY"
        **/
       public abstract Integer getAdjAdCompany();
       /**
        * @ejb:interface-method view-type="local"
        **/
       public abstract void setAdjAdCompany(Integer ADJ_AD_CMPNY);  

	   // Access methods for relationship fields
       
		   /**
		 * @ejb:interface-method view-type="local"
		 * @ejb:relation name="supplier-adjustments"
		 *               role-name="adjustment-has-one-supplier"
		 * 
		 * @jboss:relation related-pk-field="splCode" fk-column="AP_SUPPLIER"
		 */
		public abstract LocalApSupplier getApSupplier();
		/**
		 * @ejb:interface-method view-type="local"
		 */
		public abstract void setApSupplier(LocalApSupplier apSupplier);
   	
	   
	   /**
	    * @ejb:interface-method view-type="local"
	    * @ejb:relation name="adjustment-adjustmentlines"
	    *               role-name="adjustment-has-many-adjustmentlines"
	    */
	   public abstract Collection getInvAdjustmentLines();
	   public abstract void setInvAdjustmentLines(Collection invAdjustmentLines);
	   
	   /**
	    * @ejb:interface-method view-type="local"
	    * @ejb:relation name="adjustment-invdistributionrecords"
	    *               role-name="adjustment-has-many-invdistributionrecords"
	    */
	   public abstract Collection getInvDistributionRecords();
	   public abstract void setInvDistributionRecords(Collection invDistributionRecords);
	   
	   /**
	    * @ejb:interface-method view-type="local"
	    * @ejb:relation name="chartofaccount-adjustments"
	    *               role-name="adjustment-has-one-chartofaccount"
	    *               cascade-delete="no"
	    *
	    * @jboss:relation related-pk-field="coaCode"
	    *                 fk-column="GL_CHART_OF_ACCOUNT"
	    */
	   public abstract com.ejb.gl.LocalGlChartOfAccount getGlChartOfAccount();
       /**
        * @ejb:interface-method view-type="local"
        **/
	   public abstract void setGlChartOfAccount(com.ejb.gl.LocalGlChartOfAccount glChartOfAccount);

		/**
		 * @jboss:dynamic-ql
		 */
		public abstract Collection ejbSelectGeneric(java.lang.String jbossQl, java.lang.Object[] args)
		throws FinderException;
		
		// BUSINESS METHODS
		
		/**
		 * @ejb:home-method view-type="local"
		 */
		public Collection ejbHomeGetAdjByCriteria(java.lang.String jbossQl, java.lang.Object[] args)
		throws FinderException {
			
			return ejbSelectGeneric(jbossQl, args);
			
		}
	   
	   /**
	    * @ejb:interface-method view-type="local"
	    **/
	   public void addInvAdjustmentLine(LocalInvAdjustmentLine invAdjustmentLine) {

	      Debug.print("InvAdjustmentBean addInvAdjustmentLine");
	      try {
	         Collection invAdjustmentLines = getInvAdjustmentLines();
	         invAdjustmentLines.add(invAdjustmentLine);
	      } catch (Exception ex) {
	         throw new EJBException(ex.getMessage());
	      }
	   }

	   /**
	    * @ejb:interface-method view-type="local"
	    **/
	   public void dropInvAdjustmentLine(LocalInvAdjustmentLine invAdjustmentLine) {
	   
	      Debug.print("InvAdjustmentBean dropInvAdjustmentLine");
	      try {
	         Collection invAdjustmentLines = getInvAdjustmentLines();
	         invAdjustmentLines.remove(invAdjustmentLine);
	      } catch (Exception ex) {
	         throw new EJBException(ex.getMessage());
	      }
	   }

	   /**
	    * @ejb:interface-method view-type="local"
	    **/
	   public void addInvDistributionRecord(LocalInvDistributionRecord invDistributionRecord) {

	      Debug.print("InvAdjustmentBean addInvDistributionRecord");
	      try {
	         Collection invDistributionRecords = getInvDistributionRecords();
	         invDistributionRecords.add(invDistributionRecord);
	      } catch (Exception ex) {
	         throw new EJBException(ex.getMessage());
	      }
	   }

	   /**
	    * @ejb:interface-method view-type="local"
	    **/
	   public void dropInvDistributionRecord(LocalInvDistributionRecord invDistributionRecord) {

	      Debug.print("InvAdjustmentBean dropInvDistributionRecord");
	      try {
	         Collection invDistributionRecords = getInvDistributionRecords();
	         invDistributionRecords.remove(invDistributionRecord);
	      } catch (Exception ex) {
	         throw new EJBException(ex.getMessage());
	      }
	   }
	   
	   /**
	    * @ejb:interface-method view-type="local"
	    **/
	   public short getInvDrNextLine() {

	       Debug.print("InvAdjustmentBean getInvDrNextLine");
	       try {
	          Collection invDistributionRecords = getInvDistributionRecords();
		      return (short)(invDistributionRecords.size() + 1);		    
	       } catch (Exception ex) {	      	
	          throw new EJBException(ex.getMessage());	         
	       }
	    }  

	   // EntityBean methods

	   /**
	    * @ejb:create-method view-type="local"
	    **/
	   
	   
	   public java.lang.Integer ejbCreate (java.lang.Integer ADJ_CODE, java.lang.String ADJ_DCMNT_NMBR, java.lang.String ADJ_RFRNC_NMBR,
	      String ADJ_DESC, java.util.Date ADJ_DT, java.lang.String ADJ_TYP, java.lang.String ADJ_APPRVL_STATUS,
		  byte ADJ_PSTD, java.lang.String ADJ_CRTD_BY, java.util.Date ADJ_DT_CRTD, 
		  java.lang.String ADJ_LST_MDFD_BY, java.util.Date ADJ_DT_LST_MDFD, 
		  java.lang.String ADJ_APPRVD_RJCTD_BY, java.util.Date ADJ_DT_APPRVD_RJCTD, 
		  java.lang.String ADJ_PSTD_BY, java.util.Date ADJ_DT_PSTD, String ADJ_NT_BY, String ADJ_RSN_FR_RJCTN, byte ADJ_IS_CST_VRNC, byte ADJ_VD,
		  Integer ADJ_AD_BRNCH, Integer ADJ_AD_CMPNY)
	      throws CreateException {

	      Debug.print("invAdjustmentBean ejbCreate");
	      
	      setAdjCode(ADJ_CODE);
	      setAdjDocumentNumber(ADJ_DCMNT_NMBR);
	      setAdjReferenceNumber(ADJ_RFRNC_NMBR);
	      setAdjDescription(ADJ_DESC);
	      setAdjDate(ADJ_DT);
	      setAdjType(ADJ_TYP);
	      setAdjApprovalStatus(ADJ_APPRVL_STATUS);
	      setAdjPosted(ADJ_PSTD);
	      setAdjCreatedBy(ADJ_CRTD_BY);
	      setAdjDateCreated(ADJ_DT_CRTD);
	      setAdjLastModifiedBy(ADJ_LST_MDFD_BY);
	      setAdjDateLastModified(ADJ_DT_LST_MDFD);
	      setAdjApprovedRejectedBy(ADJ_APPRVD_RJCTD_BY);
	      setAdjDateApprovedRejected(ADJ_DT_APPRVD_RJCTD);
	      setAdjPostedBy(ADJ_PSTD_BY);
	      setAdjDatePosted(ADJ_DT_PSTD);
	      setAdjNotedBy(ADJ_NT_BY);
	      setAdjReasonForRejection(ADJ_RSN_FR_RJCTN);
	      setAdjIsCostVariance(ADJ_IS_CST_VRNC);
	      setAdjVoid(ADJ_VD);
	      setAdjAdBranch(ADJ_AD_BRNCH);
	      setAdjAdCompany(ADJ_AD_CMPNY);
	      
	      return null;
	   }  

	   // EntityBean methods

	   /**
	    * @ejb:create-method view-type="local"
	    **/
	   
	   
	   public java.lang.Integer ejbCreate (java.lang.String ADJ_DCMNT_NMBR, java.lang.String ADJ_RFRNC_NMBR,
	      String ADJ_DESC, java.util.Date ADJ_DT, java.lang.String ADJ_TYP, java.lang.String ADJ_APPRVL_STATUS,
	      byte ADJ_PSTD, byte ADJ_GNRTD, java.lang.String ADJ_CRTD_BY, java.util.Date ADJ_DT_CRTD, 
		  java.lang.String ADJ_LST_MDFD_BY, java.util.Date ADJ_DT_LST_MDFD, 
		  java.lang.String ADJ_APPRVD_RJCTD_BY, java.util.Date ADJ_DT_APPRVD_RJCTD, 
		  java.lang.String ADJ_PSTD_BY, java.util.Date ADJ_DT_PSTD, String ADJ_NT_BY, String ADJ_RSN_FR_RJCTN, byte ADJ_IS_CST_VRNC, byte ADJ_VD,
		  Integer ADJ_AD_BRNCH, Integer ADJ_AD_CMPNY)
	      throws CreateException {

	      Debug.print("invAdjustmentBean ejbCreate");
	      
	      setAdjDocumentNumber(ADJ_DCMNT_NMBR);
	      setAdjReferenceNumber(ADJ_RFRNC_NMBR);
	      setAdjDescription(ADJ_DESC);
	      setAdjDate(ADJ_DT);
	      setAdjType(ADJ_TYP);
	      setAdjApprovalStatus(ADJ_APPRVL_STATUS);
	      setAdjPosted(ADJ_PSTD);
	      setAdjGenerated(ADJ_GNRTD);
	      setAdjCreatedBy(ADJ_CRTD_BY);
	      setAdjDateCreated(ADJ_DT_CRTD);
	      setAdjLastModifiedBy(ADJ_LST_MDFD_BY);
	      setAdjDateLastModified(ADJ_DT_LST_MDFD);
	      setAdjApprovedRejectedBy(ADJ_APPRVD_RJCTD_BY);
	      setAdjDateApprovedRejected(ADJ_DT_APPRVD_RJCTD);
	      setAdjPostedBy(ADJ_PSTD_BY);
	      setAdjDatePosted(ADJ_DT_PSTD);
	      setAdjNotedBy(ADJ_NT_BY);
	      setAdjReasonForRejection(ADJ_RSN_FR_RJCTN);
	      setAdjIsCostVariance(ADJ_IS_CST_VRNC);
	      setAdjVoid(ADJ_VD);
	      setAdjAdBranch(ADJ_AD_BRNCH);
	      setAdjAdCompany(ADJ_AD_CMPNY);
	      
	      return null;
	   }

	   /**
	    * @ejb:create-method view-type="local"
	    **/
	   public java.lang.Integer ejbCreate (java.lang.String ADJ_DCMNT_NMBR, java.lang.String ADJ_RFRNC_NMBR,
	      String ADJ_DESC, java.util.Date ADJ_DT, java.lang.String ADJ_TYP, java.lang.String ADJ_APPRVL_STATUS,
		  byte ADJ_PSTD, java.lang.String ADJ_CRTD_BY, java.util.Date ADJ_DT_CRTD, 
		  java.lang.String ADJ_LST_MDFD_BY, java.util.Date ADJ_DT_LST_MDFD, 
		  java.lang.String ADJ_APPRVD_RJCTD_BY, java.util.Date ADJ_DT_APPRVD_RJCTD, 
		  java.lang.String ADJ_PSTD_BY, java.util.Date ADJ_DT_PSTD, String ADJ_NT_BY, String ADJ_RSN_FR_RJCTN, byte ADJ_IS_CST_VRNC, byte ADJ_VD,
		  Integer ADJ_AD_BRNCH, Integer ADJ_AD_CMPNY)
	      throws CreateException {

	      Debug.print("invAdjustmentBean ejbCreate");
	      
	      setAdjDocumentNumber(ADJ_DCMNT_NMBR);
	      setAdjReferenceNumber(ADJ_RFRNC_NMBR);
	      setAdjDescription(ADJ_DESC);
	      setAdjDate(ADJ_DT);
	      setAdjType(ADJ_TYP);
	      setAdjApprovalStatus(ADJ_APPRVL_STATUS);
	      setAdjPosted(ADJ_PSTD);
	      setAdjCreatedBy(ADJ_CRTD_BY);
	      setAdjDateCreated(ADJ_DT_CRTD);
	      setAdjLastModifiedBy(ADJ_LST_MDFD_BY);
	      setAdjDateLastModified(ADJ_DT_LST_MDFD);
	      setAdjApprovedRejectedBy(ADJ_APPRVD_RJCTD_BY);
	      setAdjDateApprovedRejected(ADJ_DT_APPRVD_RJCTD);
	      setAdjPostedBy(ADJ_PSTD_BY);
	      setAdjDatePosted(ADJ_DT_PSTD);
	      setAdjNotedBy(ADJ_NT_BY);
	      setAdjReasonForRejection(ADJ_RSN_FR_RJCTN);
	      setAdjIsCostVariance(ADJ_IS_CST_VRNC);
	      setAdjVoid(ADJ_VD);
	      setAdjAdBranch(ADJ_AD_BRNCH);
	      setAdjAdCompany(ADJ_AD_CMPNY);
	      
	      return null;
	   }

	   public void ejbPostCreate (java.lang.Integer ADJ_CODE, java.lang.String ADJ_DCMNT_NMBR, java.lang.String ADJ_RFRNC_NMBR,
	      String ADJ_DESC, java.util.Date ADJ_DT, java.lang.String ADJ_TYP, java.lang.String ADJ_APPRVL_STATUS,
		  byte ADJ_PSTD, java.lang.String ADJ_CRTD_BY, java.util.Date ADJ_DT_CRTD, 
		  java.lang.String ADJ_LST_MDFD_BY, java.util.Date ADJ_DT_LST_MDFD, 
		  java.lang.String ADJ_APPRVD_RJCTD_BY, java.util.Date ADJ_DT_APPRVD_RJCTD, 
		  java.lang.String ADJ_PSTD_BY, java.util.Date ADJ_DT_PSTD, java.lang.String ADJ_NT_BY, String ADJ_RSN_FR_RJCTN, byte ADJ_IS_CST_VRNC, byte ADJ_VD,
		  Integer ADJ_AD_BRNCH, Integer ADJ_AD_CMPNY)
	      throws CreateException { }

	   public void ejbPostCreate (java.lang.String ADJ_DCMNT_NMBR, java.lang.String ADJ_RFRNC_NMBR,
	      String ADJ_DESC, java.util.Date ADJ_DT, java.lang.String ADJ_TYP, java.lang.String ADJ_APPRVL_STATUS,
	      byte ADJ_PSTD, byte ADJ_GNRTD, java.lang.String ADJ_CRTD_BY, java.util.Date ADJ_DT_CRTD, 
		  java.lang.String ADJ_LST_MDFD_BY, java.util.Date ADJ_DT_LST_MDFD, 
		  java.lang.String ADJ_APPRVD_RJCTD_BY, java.util.Date ADJ_DT_APPRVD_RJCTD, 
		  java.lang.String ADJ_PSTD_BY, java.util.Date ADJ_DT_PSTD, java.lang.String ADJ_NT_BY, String ADJ_RSN_FR_RJCTN, byte ADJ_IS_CST_VRNC, byte ADJ_VD,
		  Integer ADJ_AD_BRNCH, Integer ADJ_AD_CMPNY)
	      throws CreateException { }

	   public void ejbPostCreate (java.lang.String ADJ_DCMNT_NMBR, java.lang.String ADJ_RFRNC_NMBR,
	      String ADJ_DESC, java.util.Date ADJ_DT, java.lang.String ADJ_TYP, java.lang.String ADJ_APPRVL_STATUS,
		  byte ADJ_PSTD, java.lang.String ADJ_CRTD_BY, java.util.Date ADJ_DT_CRTD, 
		  java.lang.String ADJ_LST_MDFD_BY, java.util.Date ADJ_DT_LST_MDFD, 
		  java.lang.String ADJ_APPRVD_RJCTD_BY, java.util.Date ADJ_DT_APPRVD_RJCTD, 
		  java.lang.String ADJ_PSTD_BY, java.util.Date ADJ_DT_PSTD, java.lang.String ADJ_NT_BY, String ADJ_RSN_FR_RJCTN, byte ADJ_IS_CST_VRNC, byte ADJ_VD,
		  Integer ADJ_AD_BRNCH, Integer ADJ_AD_CMPNY)
	      throws CreateException { }
	
}
