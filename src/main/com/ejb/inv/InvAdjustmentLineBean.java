package com.ejb.inv;


import java.util.Collection;
import java.util.Date;

import javax.ejb.CreateException;
import javax.ejb.FinderException;

import com.util.AbstractEntityBean;
import com.util.Debug;

/**
 * @ejb:bean name="InvAdjusmentLineEJB"
 *           display-name="Adjustment Line Entity"
 *           type="CMP"
 *           cmp-version="2.x"
 *           view-type="local"
 *           local-jndi-name="omega-ejb/InvAdjustmentLineEJB"
 *           schema="InvAdjustmentLine"
 *           primkey-field="alCode"
 *       
 * @ejb:pk class="java.lang.Integer"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="invuser"
 *                        role-link="invuserlink"
 *
 * @ejb:permission role-name="invuser"
 *
 * @ejb:interface local-class="com.ejb.inv.LocalInvAdjustmentLine"
 *                local-extends="javax.ejb.EJBLocalObject"
 *
 * @ejb:home local-class="com.ejb.inv.LocalInvAdjustmentLineHome"
 *           local-extends="javax.ejb.EJBLocalHome"
 *
 * @ejb:finder signature = "LocalInvAdjustmentLine findByAdjCodeAndIlCode(java.lang.Integer ADJ_CODE, java.lang.Integer IL_CODE, java.lang.Integer AL_AD_CMPNY)"
 * 				query = "SELECT OBJECT(al) FROM InvAdjustmentLine al WHERE al.invAdjustment.adjCode=?1 AND al.invItemLocation.ilCode=?2 AND al.alAdCompany=?3"
 *
  * @ejb:finder signature = "LocalInvAdjustmentLine findByAlCode(java.lang.Integer AL_CODE, java.lang.Integer AL_AD_CMPNY)"
 * 				query = "SELECT OBJECT(al) FROM InvAdjustmentLine al WHERE al.alCode=?1 AND al.alAdCompany=?2"
 *
 * @ejb:finder signature = "Collection findNegativeAlByAdjPostedAndIlCodeAndBrCode(byte ADJ_PSTD, java.lang.Integer IL_CODE, java.lang.Integer ADJ_AD_BRNCH, java.lang.Integer AL_AD_CMPNY)"
 * 				query = "SELECT OBJECT(al) FROM InvAdjustmentLine al WHERE al.alAdjustQuantity < 0 AND al.invAdjustment.adjPosted=?1 AND al.invItemLocation.ilCode=?2 AND al.invAdjustment.adjAdBranch=?3 AND al.alAdCompany=?4"
 * 
 * @ejb:finder signature="Collection findUnvoidAndIsCostVarianceGreaterThanAdjDateAndIlCodeAndBrCode(java.util.Date ADJ_DT, java.lang.Integer IL_CODE, java.lang.Integer ADJ_AD_BRNCH, java.lang.Integer ADJ_AD_CMPNY)"
 *             query="SELECT OBJECT(al) FROM InvAdjustmentLine al  WHERE al.invAdjustment.adjVoid = 0 AND al.invAdjustment.adjIsCostVariance = 1 AND al.invAdjustment.adjDate > ?1 AND al.invItemLocation.ilCode = ?2 AND al.invAdjustment.adjAdBranch = ?3 AND al.alAdCompany = ?4"
 *
 * @ejb:finder signature="Collection findPostedAdjByLessOrEqualDateAndAdjAdBranch(java.lang.Integer ADJ_AD_BRNCH, java.lang.Integer AL_AD_CMPNY)"
 * 			   query="SELECT OBJECT(al) FROM InvAdjustmentLine al WHERE al.invAdjustment.adjPosted = 1 AND al.invAdjustment.adjVoid = 0  AND al.invAdjustment.adjAdBranch = ?1 AND al.alAdCompany = ?2"          
 *
 * @ejb:finder signature="Collection findUnpostedAdjByIiNameAndLocNameAndLessEqualDateAndAdjAdBranch(java.lang.String II_NM, java.lang.String LOC_NM, java.util.Date ADJ_DT, java.lang.Integer ADJ_AD_BRNCH, java.lang.Integer AL_AD_CMPNY)"
 * 			   query="SELECT OBJECT(al) FROM InvAdjustmentLine al WHERE al.invAdjustment.adjPosted = 0 AND al.invAdjustment.adjVoid = 0 AND al.invItemLocation.invItem.iiName = ?1 AND al.invItemLocation.invLocation.locName = ?2 AND al.invAdjustment.adjDate <= ?3 AND al.invAdjustment.adjAdBranch = ?4 AND al.alAdCompany = ?5"          
 * 
 * @ejb:finder signature="Collection findUnpostedAdjByLocNameAndAdBranch(java.lang.String LOC_NM, java.lang.Integer ADJ_AD_BRNCH, java.lang.Integer AL_AD_CMPNY)"
 * 			   query="SELECT OBJECT(al) FROM InvAdjustmentLine al WHERE al.invAdjustment.adjPosted = 0 AND al.invAdjustment.adjVoid = 0 AND al.invItemLocation.invLocation.locName = ?1 AND al.invAdjustment.adjAdBranch = ?2 AND al.alAdCompany = ?3"
 * 
 * @ejb:finder signature="Collection findUnpostedReference(java.lang.String ADJ_RFRNC_NMBR, java.lang.Integer ADJ_AD_BRNCH, java.lang.Integer AL_AD_CMPNY)"
 * 			   query="SELECT OBJECT(al) FROM InvAdjustmentLine al WHERE al.invAdjustment.adjPosted = 0 AND  al.invAdjustment.adjReferenceNumber = ?1 AND al.invAdjustment.adjAdBranch = ?2 AND al.alAdCompany = ?3"       
 *           
 * @ejb:finder signature="Collection findByAlVoidAndAdjCode(byte ADJ_VD, java.lang.Integer ADJ_CODE, java.lang.Integer DR_AD_CMPNY)"
 *             query="SELECT OBJECT(al) FROM InvAdjustment adj, IN(adj.invAdjustmentLines) al WHERE al.alVoid = ?1 AND adj.adjCode = ?2 AND al.alAdCompany = ?3"
 *             
 * @ejb:finder signature="Collection findStockOnHand(java.lang.Integer DR_AD_CMPNY, java.util.Date ADJ_DT)"
 *             query="SELECT OBJECT(al) FROM InvAdjustment adj, IN(adj.invAdjustmentLines) al WHERE al.alVoid = 0 AND al.alAdCompany = ?1 AND al.invAdjustment.adjDate < ?2"
 *             
 * @ejb:finder signature="Collection findGeneral(java.lang.Integer DR_AD_CMPNY, java.util.Date ADJ_DT)"
 *             query="SELECT OBJECT(al) FROM InvAdjustment adj, IN(adj.invAdjustmentLines) al WHERE al.alVoid = 0 AND al.alAdCompany = ?1 AND al.invAdjustment.adjType = 'GENERAL' AND al.invAdjustment.adjPosted = 1 AND al.invAdjustment.adjDate = ?2"
 *
 *@ejb:finder signature="Collection findPostedIssuance(java.lang.Integer DR_AD_CMPNY, java.util.Date ADJ_DT)"
 *             query="SELECT OBJECT(al) FROM InvAdjustment adj, IN(adj.invAdjustmentLines) al WHERE al.alVoid = 0 AND al.alAdCompany = ?1 AND al.invAdjustment.adjType = 'ISSUANCE' AND al.invAdjustment.adjPosted = 1 AND al.invAdjustment.adjDate = ?2"
 *   
 *@ejb:finder signature="Collection findUnPostedIssuance(java.lang.Integer DR_AD_CMPNY)"
 *             query="SELECT OBJECT(al) FROM InvAdjustment adj, IN(adj.invAdjustmentLines) al WHERE al.alVoid = 0 AND al.alAdCompany = ?1 AND al.invAdjustment.adjType = 'ISSUANCE' AND al.invAdjustment.adjPosted = 0"
 *  
 * @ejb:finder signature="Collection findUnpostedByItem(java.lang.Integer INV_ITEM_LOCATION, java.lang.Integer ADJ_AD_BRNCH, java.lang.Integer AL_AD_CMPNY)"
 * 			   query="SELECT OBJECT(al) FROM InvAdjustmentLine al WHERE al.invAdjustment.adjPosted = 0 AND al.invAdjustment.adjVoid = 0 AND al.invAdjustment.adjType = 'ISSUANCE' AND al.invItemLocation.invItem.iiCode = ?1 AND al.invAdjustment.adjAdBranch = ?2 AND al.alAdCompany = ?3"          
 * 
 * @ejb:select signature="Collection ejbSelectGeneric(java.lang.String jbossQl, java.lang.Object[] args)"
 *             result-type-mapping="Local"
 *             method-intf="LocalHome"
 *             query="SELECT OBJECT(inv) FROM InvAdjustment inv"
 *
 * @jboss:query signature="Collection ejbSelectGeneric(java.lang.String jbossQl, java.lang.Object[] args)"
 *                         dynamic="true"
 *
 * @ejb:value-object match="*"
 *             name="InvAdjustmentLine"
 *
 * @jboss:persistence table-name="INV_ADJSTMNT_LN"
 *
 * @jboss:entity-command name="mysql-get-generated-keys"
 *
 */

public abstract class InvAdjustmentLineBean extends AbstractEntityBean {
	
//	 Access methods for persistent fields

	   /**
	    * @ejb:persistent-field
	    * @ejb:interface-method view-type="local"
	    * @ejb:pk-field
	    *
	    * @jboss:column-name name="AL_CODE"
	    *
	    * @jboss:persistence auto-increment="true"
	    **/
	   public abstract java.lang.Integer getAlCode();
	   public abstract void setAlCode(java.lang.Integer AL_CODE);

	   /**
	    * @ejb:persistent-field
	    * @ejb:interface-method view-type="local"
	    *
	    * @jboss:column-name name="AL_UNT_CST"
	    **/
	   public abstract double getAlUnitCost();
	   /**
	    * @ejb:interface-method view-type="local"
	    **/
	   public abstract void setAlUnitCost(double AL_UNT_CST);
	   
	   
	   /**
	    * @ejb:persistent-field
	    * @ejb:interface-method view-type="local"
	    *
	    * @jboss:column-name name="AL_QC_NUM"
	    **/
	   public abstract String getAlQcNumber();
	   /**
	    * @ejb:interface-method view-type="local"
	    **/
	   public abstract void setAlQcNumber(String AL_QC_NUM);
	   
	   /**
	    * @ejb:persistent-field
	    * @ejb:interface-method view-type="local"
	    *
	    * @jboss:column-name name="AL_QC_EXPRY_DT"
	    **/
	   public abstract java.util.Date getAlQcExpiryDate();
	   /**
	    * @ejb:interface-method view-type="local"
	    **/
	   public abstract void setAlQcExpiryDate(java.util.Date AL_QC_EXPRY_DT);

	   /**
	    * @ejb:persistent-field
	    * @ejb:interface-method view-type="local"
	    *
	    * @jboss:column-name name="AL_ADJST_QTY"
	    **/
	   public abstract double getAlAdjustQuantity();
	   /**
	    * @ejb:interface-method view-type="local"
	    **/
	   public abstract void setAlAdjustQuantity(double AL_ADJST_QTY);
	    
	   /**
	    * @ejb:persistent-field
	    * @ejb:interface-method view-type="local"
	    *
	    * @jboss:column-name name="AL_SRVD"
	    **/
	   public abstract double getAlServed();
	   /**
	    * @ejb:interface-method view-type="local"
	    **/
	   public abstract void setAlServed(double AL_SRVD);
	   
	   
	   
	   /**
	    * @ejb:persistent-field
	    * @ejb:interface-method view-type="local"
	    *
	    * @jboss:column-name name="AL_VD"
	    **/
	   public abstract byte getAlVoid();
	   /**
	    * @ejb:interface-method view-type="local"
	    **/
	   public abstract void setAlVoid(byte AL_VD);
	   /**
	    * @ejb:persistent-field
	    * @ejb:interface-method view-type="local"
	    *
	    * @jboss:column-name name="AL_ADJST_EXPRY_DT"
	    **/
	   public abstract String getAlMisc();
	   
	   /**
	    * @ejb:interface-method view-type="local"
	    **/
	   public abstract void setAlMisc(String AL_MISC);
	   /**
	    * @ejb:persistent-field
	    * @ejb:interface-method view-type="local"
	    *
	    * @jboss:column-name name="AL_AD_CMPNY"
	    **/
	   public abstract Integer getAlAdCompany();
	   /**
	    * @ejb:interface-method view-type="local"
	    **/
	   public abstract void setAlAdCompany(Integer AL_AD_CMPNY);
	   
	   // Access methods for relationship fields

	   /**
	    * @ejb:interface-method view-type="local"
	    * @ejb:relation name="adjustment-adjustmentlines"
	    *               role-name="adjustmentline-has-one-adjustment"
	    *               cascade-delete="yes"
	    *
	    * @jboss:relation related-pk-field="adjCode"
	    *                 fk-column="INV_ADJUSTMENT"
	    */
	   public abstract LocalInvAdjustment getInvAdjustment();
	   /**
	    * @ejb:interface-method view-type="local"
	    **/
	   public abstract void setInvAdjustment(LocalInvAdjustment invAdjustment);
	   
	   /**
	    * @ejb:interface-method view-type="local"
	    * @ejb:relation name="unitofmeasure-adjustmentlines"
	    *               role-name="adjustmentline-has-one-unitofmeasure"
	    *               cascade-delete="no"
	    *
	    * @jboss:relation related-pk-field="uomCode"
	    *                 fk-column="INV_UNIT_OF_MEASURE"
	    */
	   public abstract LocalInvUnitOfMeasure getInvUnitOfMeasure();
	   /**
	    * @ejb:interface-method view-type="local"
	    **/
	   public abstract void setInvUnitOfMeasure(LocalInvUnitOfMeasure invUnitOfMeasure);
	   
	   /**
	    * @ejb:interface-method view-type="local"
	    * @ejb:relation name="itemlocation-adjustmentlines"
	    *               role-name="adjustmentline-has-one-itemlocation"
	    *               cascade-delete="no"
	    *
	    * @jboss:relation related-pk-field="ilCode"
	    *                 fk-column="INV_ITEM_LOCATION"
	    */
	   public abstract LocalInvItemLocation getInvItemLocation();
	   /**
	    * @ejb:interface-method view-type="local"
	    **/
	   public abstract void setInvItemLocation(LocalInvItemLocation invItemLocation);	  

	   /**
	    * @ejb:interface-method view-type="local"
	    * @ejb:relation name="adjustmentline-costing"
	    *               role-name="adjustmentline-has-one-costing"
	    */
	   public abstract LocalInvCosting getInvCosting();
	   /**
	    * @ejb:interface-method view-type="local"
	    **/
	   public abstract void setInvCosting(LocalInvCosting invCosting);
	   
	   /**
	    * @ejb:interface-method view-type="local"
	    * @ejb:relation name="adjustmentline-tags"
	    *               role-name="adjustmentline-has-many-tags"
	    *               
	    */				
	   public abstract Collection getInvTags();
	   public abstract void setInvTags(Collection invTags);
	
		/**
		 * @jboss:dynamic-ql
		 */
		public abstract Collection ejbSelectGeneric(java.lang.String jbossQl, java.lang.Object[] args)
		throws FinderException;
		
	   // Business methods

		
		/**
		 * @ejb:home-method view-type="local"
		 */
		public Collection ejbHomeGetInvByCriteria(java.lang.String jbossQl, java.lang.Object[] args)
		throws FinderException {
			
			return ejbSelectGeneric(jbossQl,args);
		} 	   
	   // EntityBean methods

	   /**
	    * @ejb:create-method view-type="local"
	    **/
	   public java.lang.Integer ejbCreate (java.lang.Integer AL_CODE, double AL_UNT_CST, String AL_QC_NUM, Date AL_QC_EXPRY_DT,
	   	  double AL_ADJST_QTY, double AL_SRVD, byte AL_VD, Integer AL_AD_CMPNY)
	      throws CreateException {

	      Debug.print("invAdjustmentLineBean ejbCreate");
	      
	      setAlCode(AL_CODE);
	      setAlUnitCost(AL_UNT_CST);
	      setAlQcNumber(AL_QC_NUM);
	      setAlQcExpiryDate(AL_QC_EXPRY_DT);
	      setAlAdjustQuantity(AL_ADJST_QTY);
	      setAlServed(AL_SRVD);
	      setAlVoid(AL_VD);
	      setAlAdCompany(AL_AD_CMPNY);

	      return null;
	   }

	   /**
	    * @ejb:create-method view-type="local"
	    **/
	   public java.lang.Integer ejbCreate (double AL_UNT_CST, String AL_QC_NUM, Date AL_QC_EXPRY_DT,
	   	  double AL_ADJST_QTY,double AL_SRVD, byte AL_VD, Integer AL_AD_CMPNY)
	      throws CreateException {

	      Debug.print("invAdjustmentLineBean ejbCreate");
	      
	      setAlUnitCost(AL_UNT_CST);
	      setAlQcNumber(AL_QC_NUM);
	      setAlQcExpiryDate(AL_QC_EXPRY_DT);
	      setAlAdjustQuantity(AL_ADJST_QTY);
	      setAlServed(AL_SRVD);
	      setAlVoid(AL_VD);
	      setAlAdCompany(AL_AD_CMPNY);
	      
	      return null;
	   }

	   public void ejbPostCreate (java.lang.Integer AL_CODE, double AL_UNT_CST, String AL_QC_NUM, Date AL_QC_EXPRY_DT,
		  double AL_ADJST_QTY,double AL_SRVD, byte AL_VD, Integer AL_AD_CMPNY)
	      throws CreateException { }

	   public void ejbPostCreate (double AL_UNT_CST, String AL_QC_NUM, Date AL_QC_EXPRY_DT,
		  double AL_ADJST_QTY,double AL_SRVD,byte AL_VD, Integer AL_AD_CMPNY)
	      throws CreateException { }
	
}
