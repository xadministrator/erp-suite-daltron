package com.ejb.inv;

import java.util.Collection;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;

import com.util.AbstractEntityBean;
import com.util.Debug;

/**
 * @ejb:bean name="InvAssemblyTransferEJB"
 *           display-name="Assembly Transfer Entity"
 *           type="CMP"
 *           cmp-version="2.x"
 *           view-type="local"
 *           local-jndi-name="omega-ejb/InvAssemblyTransferEJB"
 *           schema="InvAssemblyTransfer"
 *           primkey-field="atrCode"
 *       
 * @ejb:pk class="java.lang.Integer"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="invuser"
 *                        role-link="invuserlink"
 *
 * @ejb:permission role-name="invuser"
 *
 * @ejb:interface local-class="com.ejb.inv.LocalInvAssemblyTransfer"
 *                local-extends="javax.ejb.EJBLocalObject"
 *
 * @ejb:home local-class="com.ejb.inv.LocalInvAssemblyTransferHome"
 *           local-extends="javax.ejb.EJBLocalHome"
 *
 * @ejb:finder signature="LocalInvAssemblyTransfer findByAtrDocumentNumberAndBrCode(java.lang.String ATR_DCMNT_NMBR, java.lang.Integer ATR_AD_BRNCH, java.lang.Integer ATR_AD_CMPNY)"
 *             query="SELECT OBJECT(atr) FROM InvAssemblyTransfer atr  WHERE atr.atrDocumentNumber = ?1 AND atr.atrAdBranch = ?2 AND atr.atrAdCompany = ?3"
 *
 * @ejb:select signature="Collection ejbSelectGeneric(java.lang.String jbossQl, java.lang.Object[] args)"
 *             result-type-mapping="Local"
 *             method-intf="LocalHome"
 *             query="SELECT OBJECT(atr) FROM InvAssemblyTransfer atr"
 *
 * @ejb:finder signature="Collection findUnpostedAtrByAtrDateRange(java.util.Date ATR_DT_FRM, java.util.Date ATR_DT_TO, java.lang.Integer ATR_AD_CMPNY)"
 *             query="SELECT OBJECT(atr) FROM InvAssemblyTransfer atr  WHERE atr.atrPosted = 0 AND atr.atrVoid = 0 AND atr.atrDate >= ?1 AND atr.atrDate <= ?2 AND atr.atrAdCompany = ?3"
 *
 * @jboss:query signature="Collection findUnpostedAtrByAtrDateRange(java.util.Date ATR_DT_FRM, java.util.Date ATR_DT_TO, java.lang.Integer ATR_AD_CMPNY)"
 *             query="SELECT OBJECT(atr) FROM InvAssemblyTransfer atr  WHERE atr.atrPosted = 0 AND atr.atrVoid = 0 AND atr.atrDate >= ?1 AND atr.atrDate <= ?2 AND atr.atrAdCompany = ?3 ORDER BY atr.atrDate"
 * 
 * @jboss:query signature="Collection ejbSelectGeneric(java.lang.String jbossQl, java.lang.Object[] args)"
 *                         dynamic="true"
 * 
 * @ejb:value-object match="*"
 *             name="InvAssemblyTransfer"
 *
 * @jboss:persistence table-name="INV_ASSMBLY_TRNSFR"
 *
 * @jboss:entity-command name="mysql-get-generated-keys"
 *
 */

public abstract class InvAssemblyTransferBean extends AbstractEntityBean {
	
      // Access methods for persistent fields

	  /**
	   * @ejb:persistent-field
	   * @ejb:interface-method view-type="local"
	   * @ejb:pk-field
	   *
	   * @jboss:column-name name="ATR_CODE"
	   *
	   * @jboss:persistence auto-increment="true"
	   **/
	  public abstract java.lang.Integer getAtrCode();
	  public abstract void setAtrCode(java.lang.Integer ATR_CODE);
	   
	  /**
	   * @ejb:persistent-field
	   * @ejb:interface-method view-type="local"
	   *
	   * @jboss:column-name name="ATR_DT"
	   **/
	  public abstract java.util.Date getAtrDate();
	  /**
	   * @ejb:interface-method view-type="local"
	   **/
	  public abstract void setAtrDate(java.util.Date ATR_DT);

	  /**
	   * @ejb:persistent-field
	   * @ejb:interface-method view-type="local"
	   *
	   * @jboss:column-name name="ATR_DCMNT_NMBR"
	   **/
	  public abstract java.lang.String getAtrDocumentNumber();
	  /**
	   * @ejb:interface-method view-type="local"
	   **/
	  public abstract void setAtrDocumentNumber(java.lang.String ATR_DCMNT_NMBR);
	   
	  /**
	   * @ejb:persistent-field
	   * @ejb:interface-method view-type="local"
	   *
	   * @jboss:column-name name="ATR_RFRNC_NMBR"
	   **/
	  public abstract java.lang.String getAtrReferenceNumber();
	  /**
	   * @ejb:interface-method view-type="local"
	   **/
	  public abstract void setAtrReferenceNumber(java.lang.String ATR_RFRNC_NMBR);
	   
	  /**
	   * @ejb:persistent-field
	   * @ejb:interface-method view-type="local"
	   *
	   * @jboss:column-name name="ATR_DESC"
	   **/
	  public abstract java.lang.String getAtrDescription();
	  /**
	   * @ejb:interface-method view-type="local"
	   **/
	  public abstract void setAtrDescription(java.lang.String ATR_DESC);	   
	   
	  /**
	   * @ejb:persistent-field
	   * @ejb:interface-method view-type="local"
	   *
	   * @jboss:column-name name="ATR_VOID"
	   **/
	  public abstract byte getAtrVoid();
	  /**
	   * @ejb:interface-method view-type="local"
	   **/
	  public abstract void setAtrVoid(byte ATR_VOID);
	   
	  /**
	   * @ejb:persistent-field
	   * @ejb:interface-method view-type="local"
	   *
	   * @jboss:column-name name="ATR_APPRVL_STATUS"
	   **/
	  public abstract java.lang.String getAtrApprovalStatus();
	  /**
	   * @ejb:interface-method view-type="local"
	   **/
	  public abstract void setAtrApprovalStatus(java.lang.String ATR_APPRVL_STATUS);
	   
	  /**
	   * @ejb:persistent-field
	   * @ejb:interface-method view-type="local"
	   *
	   * @jboss:column-name name="ATR_PSTD"
	   **/
	  public abstract byte getAtrPosted();
	  /**
	   * @ejb:interface-method view-type="local"
	   **/
	  public abstract void setAtrPosted(byte ATR_PSTD);
	   
	  /**
	   * @ejb:persistent-field
	   * @ejb:interface-method view-type="local"
	   *
	   * @jboss:column-name name="ATR_CRTD_BY"
	   **/
	  public abstract String getAtrCreatedBy();
	  /**
	   * @ejb:interface-method view-type="local"
	   **/
	  public abstract void setAtrCreatedBy(String ATR_CRTD_BY);
	   
	  /**
	   * @ejb:persistent-field
	   * @ejb:interface-method view-type="local"
	   *
	   * @jboss:column-name name="ATR_DT_CRTD"
	   **/
	  public abstract java.util.Date getAtrDateCreated();
	  /**
	   * @ejb:interface-method view-type="local"
	   **/
	  public abstract void setAtrDateCreated(java.util.Date ATR_DT_CRTD);
	   
	  /**
	   * @ejb:persistent-field
	   * @ejb:interface-method view-type="local"
	   *
	   * @jboss:column-name name="ATR_LST_MDFD_BY"
	   **/
	  public abstract String getAtrLastModifiedBy();
	  /**
	   * @ejb:interface-method view-type="local"
	   **/
	  public abstract void setAtrLastModifiedBy(String ATR_LST_MDFD_BY);
	   
	  /**
	   * @ejb:persistent-field
	   * @ejb:interface-method view-type="local"
	   *
	   * @jboss:column-name name="ATR_DT_LST_MDFD"
	   **/
	  public abstract java.util.Date getAtrDateLastModified();
	  /**
	   * @ejb:interface-method view-type="local"
	   **/
	  public abstract void setAtrDateLastModified(java.util.Date ATR_DT_LST_MDFD);
	   
	  /**
	   * @ejb:persistent-field
	   * @ejb:interface-method view-type="local"
	   *
	   * @jboss:column-name name="ATR_APPRVD_RJCTD_BY"
	   **/
	  public abstract String getAtrApprovedRejectedBy();
	  /**
	   * @ejb:interface-method view-type="local"
	   **/
	  public abstract void setAtrApprovedRejectedBy(String ATR_APPRVD_RJCTD_BY);
	   
	  /**
	   * @ejb:persistent-field
	   * @ejb:interface-method view-type="local"
	   *
	   * @jboss:column-name name="ATR_DT_APPRVD_RJCTD"
	   **/
	  public abstract java.util.Date getAtrDateApprovedRejected();
	  /**
	   * @ejb:interface-method view-type="local"
	   **/
	  public abstract void setAtrDateApprovedRejected(java.util.Date ATR_DT_APPRVD_RJCTD);
	   
	  /**
	   * @ejb:persistent-field
	   * @ejb:interface-method view-type="local"
	   *
	   * @jboss:column-name name="ATR_PSTD_BY"
	   **/
	  public abstract String getAtrPostedBy();
	  /**
	   * @ejb:interface-method view-type="local"
	   **/
	  public abstract void setAtrPostedBy(String ATR_PSTD_BY);
	   
	  /**
	   * @ejb:persistent-field
	   * @ejb:interface-method view-type="local"
	   *
	   * @jboss:column-name name="ATR_DT_PSTD"
	   **/
	  public abstract java.util.Date getAtrDatePosted();
	  /**
	   * @ejb:interface-method view-type="local"
	   **/
	  public abstract void setAtrDatePosted(java.util.Date ATR_DT_PSTD);
	   
	  /**
	   * @ejb:persistent-field
	   * @ejb:interface-method view-type="local"
	   *
	   * @jboss:column-name name="ATR_RSN_FR_RJCTN"
	   **/
      public abstract String getAtrReasonForRejection();
      /**
       * @ejb:interface-method view-type="local"
       **/
      public abstract void setAtrReasonForRejection(String ATR_RSN_FR_RJCTN);  
       
      /**
       * @ejb:persistent-field
       * @ejb:interface-method view-type="local"
       *
       * @jboss:column-name name="ATR_AD_BRNCH"
       **/
      public abstract Integer getAtrAdBranch();
      /**
       * @ejb:interface-method view-type="local"
       **/
      public abstract void setAtrAdBranch(Integer ATR_AD_BRNCH);  
      
      /**
       * @ejb:persistent-field
       * @ejb:interface-method view-type="local"
       *
       * @jboss:column-name name="ATR_AD_CMPNY"
       **/
      public abstract Integer getAtrAdCompany();
      /**
       * @ejb:interface-method view-type="local"
       **/
      public abstract void setAtrAdCompany(Integer ATR_AD_CMPNY);  

	  // Access methods for relationship fields 
	   
	  /**
	   * @ejb:interface-method view-type="local"
	   * @ejb:relation name="assemblytransfer-assemblytransferlines"
	   *               role-name="assemblytransfer-has-many-assemblytransferlines"
	   **/
	  public abstract Collection getInvAssemblyTransferLines();
	  public abstract void setInvAssemblyTransferLines(Collection invAssemblyTransferLines);
	   
	  /**
	   * @ejb:interface-method view-type="local"
	   * @ejb:relation name="assemblytransfer-invdistributionrecords"
	   *               role-name="assemblytransfer-has-many-invdistributionrecords"
	   **/
	  public abstract Collection getInvDistributionRecords();
	  public abstract void setInvDistributionRecords(Collection invDistributionRecords);
	  
	   /**
		 * @jboss:dynamic-ql
		 */
		public abstract Collection ejbSelectGeneric(java.lang.String jbossQl, java.lang.Object[] args)
		throws FinderException;
		
		// BUSINESS METHODS
		
		/**
		 * @ejb:home-method view-type="local"
		 */
		public Collection ejbHomeGetAtrByCriteria(java.lang.String jbossQl, java.lang.Object[] args)
		throws FinderException {
			
			return ejbSelectGeneric(jbossQl, args);
			
		}
		
		
	  /**
	   * @ejb:interface-method view-type="local"
	   **/
      public void addInvAssemblyTransferLine(LocalInvAssemblyTransferLine invAssemblyTransferLine) {

	     Debug.print("InvAssemblyTransferBean addInvAssemblyTransferLine");
	     try {
	        Collection invAssemblyTransferLines = getInvAssemblyTransferLines();
	        invAssemblyTransferLines.add(invAssemblyTransferLine);
	     } catch (Exception ex) {
	         throw new EJBException(ex.getMessage());
	     }
	  }

	  /**
	   * @ejb:interface-method view-type="local"
	   **/
	  public void dropInvAssemblyTransferLine(LocalInvAssemblyTransferLine invAssemblyTransferLine) {
	   
	     Debug.print("InvAssemblyTransferBean dropInvAssemblyTransferLine");
	     try {
	        Collection invAssemblyTransferLines = getInvAssemblyTransferLines();
	        invAssemblyTransferLines.remove(invAssemblyTransferLine);
	     } catch (Exception ex) {
	        throw new EJBException(ex.getMessage());
	     }
	  }

	  /**
	   * @ejb:interface-method view-type="local"
	   **/
	  public void addInvDistributionRecord(LocalInvDistributionRecord invDistributionRecord) {

	     Debug.print("InvAssemblyTransferBean addInvDistributionRecord");
	     try {
	        Collection invDistributionRecords = getInvDistributionRecords();
	        invDistributionRecords.add(invDistributionRecord);
	     } catch (Exception ex) {
	        throw new EJBException(ex.getMessage());
	     }
	  }

	  /**
	   * @ejb:interface-method view-type="local"
	   **/
	  public void dropInvDistributionRecord(LocalInvDistributionRecord invDistributionRecord) {

	  	 Debug.print("InvAdjustmentBean dropInvDistributionRecord");
	     try {
	        Collection invDistributionRecords = getInvDistributionRecords();
	        invDistributionRecords.remove(invDistributionRecord);
	     } catch (Exception ex) {
	        throw new EJBException(ex.getMessage());
	     }
	  }
	   
	  /**
	   * @ejb:interface-method view-type="local"
	   **/
	  public short getInvDrNextLine() {

         Debug.print("InvAssemblyTransferBean getInvDrNextLine");
	     try {
	        Collection invDistributionRecords = getInvDistributionRecords();
	        return (short)(invDistributionRecords.size() + 1);		    
	     } catch (Exception ex) {	      	
	        throw new EJBException(ex.getMessage());	         
	     }
	  } 
	  
	  // EntityBean methods

	  /**
	   * @ejb:create-method view-type="local"
	   **/
	  public java.lang.Integer ejbCreate (java.lang.Integer ATR_CODE, java.util.Date ATR_DT,
	  	 java.lang.String ATR_DCMNT_NMBR, java.lang.String ATR_RFRNC_NMBR, String ATR_DESC,
	  	 byte ATR_VOID, java.lang.String ATR_APPRVL_STATUS, byte ATR_PSTD,
	  	 java.lang.String ATR_CRTD_BY, java.util.Date ATR_DT_CRTD,
		 java.lang.String ATR_LST_MDFD_BY, java.util.Date ATR_DT_LST_MDFD, 
		 java.lang.String ATR_APPRVD_RJCTD_BY, java.util.Date ATR_DT_APPRVD_RJCTD, 
		 java.lang.String ATR_PSTD_BY, java.util.Date ATR_DT_PSTD, String ATR_RSN_FR_RJCTN,
		 Integer ATR_AD_BRNCH, Integer ATR_AD_CMPNY)
	     throws CreateException {

	     Debug.print("invAssemblyTransferBean ejbCreate");
	      
	     setAtrCode(ATR_CODE);
	     setAtrDate(ATR_DT);
	     setAtrDocumentNumber(ATR_DCMNT_NMBR);
	     setAtrReferenceNumber(ATR_RFRNC_NMBR);
	     setAtrDescription(ATR_DESC);
	     setAtrVoid(ATR_VOID);
	     setAtrApprovalStatus(ATR_APPRVL_STATUS);
	     setAtrPosted(ATR_PSTD);
	     setAtrCreatedBy(ATR_CRTD_BY);
	     setAtrDateCreated(ATR_DT_CRTD);
	     setAtrLastModifiedBy(ATR_LST_MDFD_BY);
	     setAtrDateLastModified(ATR_DT_LST_MDFD);
	     setAtrApprovedRejectedBy(ATR_APPRVD_RJCTD_BY);
	     setAtrDateApprovedRejected(ATR_DT_APPRVD_RJCTD);
	     setAtrPostedBy(ATR_PSTD_BY);
	     setAtrDatePosted(ATR_DT_PSTD);
	     setAtrReasonForRejection(ATR_RSN_FR_RJCTN);
	     setAtrAdBranch(ATR_AD_BRNCH);
	     setAtrAdCompany(ATR_AD_CMPNY);
	     
	     return null;
	  }

	  /**
	   * @ejb:create-method view-type="local"
	   **/
	  public java.lang.Integer ejbCreate (java.util.Date ATR_DT,
		 java.lang.String ATR_DCMNT_NMBR, java.lang.String ATR_RFRNC_NMBR, String ATR_DESC,
		 byte ATR_VOID, java.lang.String ATR_APPRVL_STATUS, byte ATR_PSTD,
		 java.lang.String ATR_CRTD_BY, java.util.Date ATR_DT_CRTD,
		 java.lang.String ATR_LST_MDFD_BY, java.util.Date ATR_DT_LST_MDFD, 
		 java.lang.String ATR_APPRVD_RJCTD_BY, java.util.Date ATR_DT_APPRVD_RJCTD, 
		 java.lang.String ATR_PSTD_BY, java.util.Date ATR_DT_PSTD, String ATR_RSN_FR_RJCTN,
		 Integer ATR_AD_BRNCH, Integer ATR_AD_CMPNY)
	     throws CreateException {

	     Debug.print("invAdjustmentBean ejbCreate");
	      
	     setAtrDate(ATR_DT);
	     setAtrDocumentNumber(ATR_DCMNT_NMBR);
	     setAtrReferenceNumber(ATR_RFRNC_NMBR);
	     setAtrDescription(ATR_DESC);
	     setAtrVoid(ATR_VOID);
	     setAtrApprovalStatus(ATR_APPRVL_STATUS);
	     setAtrPosted(ATR_PSTD);
	     setAtrCreatedBy(ATR_CRTD_BY);
	     setAtrDateCreated(ATR_DT_CRTD);
	     setAtrLastModifiedBy(ATR_LST_MDFD_BY);
	     setAtrDateLastModified(ATR_DT_LST_MDFD);
	     setAtrApprovedRejectedBy(ATR_APPRVD_RJCTD_BY);
	     setAtrDateApprovedRejected(ATR_DT_APPRVD_RJCTD);
	     setAtrPostedBy(ATR_PSTD_BY);
	     setAtrDatePosted(ATR_DT_PSTD);
	     setAtrReasonForRejection(ATR_RSN_FR_RJCTN);
	     setAtrAdBranch(ATR_AD_BRNCH);
	     setAtrAdCompany(ATR_AD_CMPNY);
	      
	     return null;
	  }

	  public void ejbPostCreate (java.lang.Integer ATR_CODE, java.util.Date ATR_DT,
		 java.lang.String ATR_DCMNT_NMBR, java.lang.String ATR_RFRNC_NMBR, String ATR_DESC,
		 byte ATR_VOID, java.lang.String ATR_APPRVL_STATUS, byte ATR_PSTD,
		 java.lang.String ATR_CRTD_BY, java.util.Date ATR_DT_CRTD,
		 java.lang.String ATR_LST_MDFD_BY, java.util.Date ATR_DT_LST_MDFD, 
		 java.lang.String ATR_APPRVD_RJCTD_BY, java.util.Date ATR_DT_APPRVD_RJCTD, 
		 java.lang.String ATR_PSTD_BY, java.util.Date ATR_DT_PSTD, String ATR_RSN_FR_RJCTN,
		 Integer ATR_AD_BRNCH, Integer ATR_AD_CMPNY)
	     throws CreateException { }

	  public void ejbPostCreate (java.util.Date ATR_DT,
		 java.lang.String ATR_DCMNT_NMBR, java.lang.String ATR_RFRNC_NMBR, String ATR_DESC,
		 byte ATR_VOID, java.lang.String ATR_APPRVL_STATUS, byte ATR_PSTD,
		 java.lang.String ATR_CRTD_BY, java.util.Date ATR_DT_CRTD,
		 java.lang.String ATR_LST_MDFD_BY, java.util.Date ATR_DT_LST_MDFD, 
		 java.lang.String ATR_APPRVD_RJCTD_BY, java.util.Date ATR_DT_APPRVD_RJCTD, 
		 java.lang.String ATR_PSTD_BY, java.util.Date ATR_DT_PSTD, String ATR_RSN_FR_RJCTN,
		 Integer ATR_AD_BRNCH, Integer ATR_AD_CMPNY)
	     throws CreateException { }
	
}
