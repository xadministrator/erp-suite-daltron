package com.ejb.inv;

import java.util.Collection;

import javax.ejb.CreateException;

import com.util.AbstractEntityBean;
import com.util.Debug;

/**
 * @ejb:bean name="InvAssemblyTransferLineEJB"
 *           display-name="Assembly Transfer Line Entity"
 *           type="CMP"
 *           cmp-version="2.x"
 *           view-type="local"
 *           local-jndi-name="omega-ejb/InvAssemblyTransferLineEJB"
 *           schema="InvAssemblyTransferLine"
 *           primkey-field="atlCode"
 *       
 * @ejb:pk class="java.lang.Integer"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="invuser"
 *                        role-link="invuserlink"
 *
 * @ejb:permission role-name="invuser"
 *
 * @ejb:interface local-class="com.ejb.inv.LocalInvAssemblyTransferLine"
 *                local-extends="javax.ejb.EJBLocalObject"
 *
 * @ejb:home local-class="com.ejb.inv.LocalInvAssemblyTransferLineHome"
 *           local-extends="javax.ejb.EJBLocalHome"
 *
 * @ejb:finder signature="Collection findUnpostedAtrByLocNameAndAdBranch(java.lang.String LOC_NM, java.lang.Integer ATR_AD_BRNCH, java.lang.Integer ATL_AD_CMPNY)"
 * 			   query="SELECT OBJECT(atl) FROM InvAssemblyTransferLine atl WHERE atl.invAssemblyTransfer.atrPosted = 0 AND atl.invAssemblyTransfer.atrVoid = 0 AND atl.invBuildOrderLine.invItemLocation.invLocation.locName = ?1 AND atl.invAssemblyTransfer.atrAdBranch = ?2 AND atl.atlAdCompany = ?3"
 * 
 * @ejb:finder signature="Collection findByIiCodeAndLocNameAndAdBranch(java.lang.Integer II_CODE, java.lang.String LOC_NM, java.lang.Integer ATR_AD_BRNCH, java.lang.Integer ATL_AD_CMPNY)"
 * 			   query="SELECT OBJECT(atl) FROM InvAssemblyTransferLine atl WHERE atl.invAssemblyTransfer.atrVoid = 0 AND atl.invBuildOrderLine.invItemLocation.invItem.iiCode=?1 AND atl.invBuildOrderLine.invItemLocation.invLocation.locName = ?2 AND atl.invAssemblyTransfer.atrAdBranch = ?3 AND atl.atlAdCompany = ?4"
 *
 * @ejb:value-object match="*"
 *             name="InvAssemblyTransferLine"
 *
 * @jboss:persistence table-name="INV_ASSMBLY_TRNSFR_LN"
 *
 * @jboss:entity-command name="mysql-get-generated-keys"
 *
 */

public abstract class InvAssemblyTransferLineBean extends AbstractEntityBean {
	
      // Access methods for persistent fields

	  /**
	   * @ejb:persistent-field
	   * @ejb:interface-method view-type="local"
	   * @ejb:pk-field
	   *
	   * @jboss:column-name name="ATL_CODE"
	   *
	   * @jboss:persistence auto-increment="true"
	   **/
	  public abstract java.lang.Integer getAtlCode();
	  public abstract void setAtlCode(java.lang.Integer ATL_CODE);

	  /**
	   * @ejb:persistent-field
	   * @ejb:interface-method view-type="local"
	   *
	   * @jboss:column-name name="ATL_ASSMBL_QTY"
	   **/
	  public abstract double getAtlAssembleQuantity();
	  /**
	   * @ejb:interface-method view-type="local"
	   **/
	  public abstract void setAtlAssembleQuantity(double ATL_ASSMBL_QTY);
	   
	  /**
	   * @ejb:persistent-field
	   * @ejb:interface-method view-type="local"
	   *
	   * @jboss:column-name name="ATL_ASSMBL_COST"
	   **/
	  public abstract double getAtlAssembleCost();
	  /**
	   * @ejb:interface-method view-type="local"
	   **/
	  public abstract void setAtlAssembleCost(double ATL_ASSMBL_COST);
	  
	  /**
	   * @ejb:persistent-field
	   * @ejb:interface-method view-type="local"
	   *
	   * @jboss:column-name name="ATL_AD_CMPNY"
	   **/
	  public abstract Integer getAtlAdCompany();
	  /**
	   * @ejb:interface-method view-type="local"
	   **/
	  public abstract void setAtlAdCompany(Integer ATL_AD_CMPNY);
	   
	  // Access methods for relationship fields
      
	  /**
	   * @ejb:interface-method view-type="local"
	   * @ejb:relation name="assemblytransfer-assemblytransferlines"
	   *               role-name="assemblytransferline-has-one-assemblytransfer"
	   *               cascade-delete="yes"
	   *
	   * @jboss:relation related-pk-field="atrCode"
	   *                 fk-column="INV_ASSEMBLY_TRANSFER"
	   **/
	  public abstract LocalInvAssemblyTransfer getInvAssemblyTransfer();
	  public abstract void setInvAssemblyTransfer(LocalInvAssemblyTransfer invAssemblyTransfer);
	  
	  /**
	   * @ejb:interface-method view-type="local"
	   * @ejb:relation name="buildorderline-assemblytransferlines"
	   *               role-name="assemblytransferline-has-one-buildorderline"
	   *               cascade-delete="no"
	   *
	   * @jboss:relation related-pk-field="bolCode"
	   *                 fk-column="INV_BUILD_ORDER_LINE"
	   **/
	  public abstract LocalInvBuildOrderLine getInvBuildOrderLine();
	  public abstract void setInvBuildOrderLine(LocalInvBuildOrderLine invBuildOrderLine);
	  	  
	  /**
	   * @ejb:interface-method view-type="local"
	   * @ejb:relation name="assemblytransferline-costing"
	   *               role-name="assemblytransferline-has-many-costings"
	   **/
	  public abstract Collection getInvCostings();
	  public abstract void setInvCostings(Collection invCostings);
	  
	  // Business methods	   
	    
	  // EntityBean methods

	  /**
	    * @ejb:create-method view-type="local"
	    **/
	  public java.lang.Integer ejbCreate (java.lang.Integer ATL_CODE, 
	          double ATL_ASSMBL_QTY, double ATL_ASSMBL_COST, Integer ATL_AD_CMPNY)
	     throws CreateException {

	     Debug.print("invAssemblyTransferLineBean ejbCreate");
	      
	     setAtlCode(ATL_CODE);
	     setAtlAssembleQuantity(ATL_ASSMBL_QTY);
	     setAtlAssembleCost(ATL_ASSMBL_COST);
	     setAtlAdCompany(ATL_AD_CMPNY);

	     return null;
	  }

	  /**
	   * @ejb:create-method view-type="local"
	   **/
	  public java.lang.Integer ejbCreate (double ATL_ASSMBL_QTY, double ATL_ASSMBL_COST, Integer ATL_AD_CMPNY)
	     throws CreateException {

	     Debug.print("invAssemblyTransferLineBean ejbCreate");
	     
	     setAtlAssembleQuantity(ATL_ASSMBL_QTY);
	     setAtlAssembleCost(ATL_ASSMBL_COST);
	     setAtlAdCompany(ATL_AD_CMPNY);
	      
	     return null;
	  }

	  public void ejbPostCreate (java.lang.Integer ATL_CODE, 
	          double ATL_ASSMBL_QTY, double ATL_ASSMBL_COST, Integer ATL_AD_CMPNY)
	     throws CreateException { }

	  public void ejbPostCreate (double ATL_ASSMBL_QTY, double ATL_ASSMBL_COST, Integer ATL_AD_CMPNY)
	     throws CreateException { }
	
}
