/*
 * com/ejb/inv/LocalInvBillOfMaterialBean.java
 *
 * Created on May 20, 2004 02:48 PM
 */

package com.ejb.inv;

import javax.ejb.CreateException;

import com.util.AbstractEntityBean;
import com.util.Debug;

/**
 *
 * @author  Enrico C. Yap
 *
 * @ejb:bean name="InvBillOfMaterialEJB"
 *           display-name="Bill Of Material Entity"
 *           type="CMP"
 *           cmp-version="2.x"
 *           view-type="local"
 *           local-jndi-name="omega-ejb/InvBillOfMaterialEJB"
 *           schema="InvBillOfMaterial"
 *           primkey-field="bomCode"
 *
 * @ejb:pk class="java.lang.Integer"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="invuser"
 *                        role-link="invuserlink"
 *
 * @ejb:permission role-name="invuser"
 *
 * @ejb:interface local-class="com.ejb.inv.LocalInvBillOfMaterial"
 *                local-extends="javax.ejb.EJBLocalObject"
 *
 * @ejb:home local-class="com.ejb.inv.LocalInvBillOfMaterialHome"
 *           local-extends="javax.ejb.EJBLocalHome"
 * 
 * @ejb:finder signature="Collection findByBomIiName(java.lang.String BOM_II_NM, java.lang.Integer BOM_AD_CMPNY)"
 *             query="SELECT OBJECT(bom) FROM InvBillOfMaterial bom WHERE bom.bomIiName=?1 AND bom.bomAdCompany=?2"
 * 
 * @ejb:finder signature="Collection findByBomLocName(java.lang.String BOM_LOC_NM, java.lang.Integer BOM_AD_CMPNY)"
 *             query="SELECT OBJECT(bom) FROM InvBillOfMaterial bom WHERE bom.bomLocName=?1 AND bom.bomAdCompany=?2"
 * 
 * @ejb:finder signature="Collection findByBomCodeAndBomDownloadStatus(java.lang.Integer BOM_CODE, char BOM_DWNLD_STATUS, java.lang.Integer BOM_AD_CMPNY)"
 *             query="SELECT OBJECT(bom) FROM InvBillOfMaterial bom WHERE bom.bomCode = ?1 AND bom.bomDownloadStatus = ?2 AND bom.bomAdCompany = ?3"
 * 
 * @ejb:finder signature="Collection findByBomNewAndUpdated(java.lang.Integer BOM_AD_CMPNY, char NEW, char UPDATED, char DOWNLOADED_UPDATED)"
 *             query="SELECT OBJECT(bom) FROM InvBillOfMaterial bom WHERE bom.bomAdCompany = ?1 AND (bom.bomDownloadStatus = ?2 OR bom.bomDownloadStatus = ?3 OR bom.bomDownloadStatus = ?4)"
 *
 * @ejb:finder signature="LocalInvBillOfMaterial findByBomIiNameAndAssemblyItem(java.lang.String BOM_II_NM, java.lang.String INV_ITM, java.lang.Integer BOM_AD_CMPNY)"
 *             query="SELECT OBJECT(bom) FROM InvBillOfMaterial bom WHERE bom.bomIiName=?1 AND bom.invItem.iiName=?2 AND bom.bomAdCompany=?3"
 * 
 * @ejb:finder signature="LocalInvBillOfMaterial findByBomIiNameAndLocNameAndAssemblyItem(java.lang.String BOM_II_NM, java.lang.String BOM_LOC_NM, java.lang.String INV_ITM, java.lang.Integer BOM_AD_CMPNY)"
 *             query="SELECT OBJECT(bom) FROM InvBillOfMaterial bom WHERE bom.bomIiName=?1 AND bom.bomLocName=?2 AND bom.invItem.iiName=?3 AND bom.bomAdCompany=?4"
 *  
 * @ejb:finder signature="Collection findByBomIiNameAndLocNameAndBrCode(java.lang.String BOM_II_NM, java.lang.String BOM_LOC_NM, java.lang.Integer BOM_AD_CMPNY)"
 *             query="SELECT OBJECT(bom) FROM InvBillOfMaterial bom WHERE bom.bomIiName=?1 AND bom.bomLocName=?2 AND bom.bomAdCompany=?3"
 * 
 * @ejb:finder signature="Collection findByBomIiName(java.lang.String BOM_II_NM, java.lang.Integer BOM_AD_CMPNY)"
 *             query="SELECT OBJECT(bom) FROM InvBillOfMaterial bom WHERE bom.bomIiName=?1 AND bom.bomAdCompany=?2"
 * 
 * @ejb:finder signature="Collection findAll(java.lang.Integer BOM_AD_CMPNY)"
 *             query="SELECT OBJECT(bom) FROM InvBillOfMaterial bom WHERE bom.bomAdCompany=?1"
 * 
 * @ejb:value-object match="*"
 *             name="InvBillOfMaterial"
 *
 * @jboss:persistence table-name="INV_BLL_OF_MTRL"
 *
 * @jboss:entity-command name="mysql-get-generated-keys"
 */

public abstract class InvBillOfMaterialBean extends AbstractEntityBean {
	
	
	// PERSISTENT METHODS
	
	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 * @ejb:pk-field
	 *
	 * @jboss:column-name name="BOM_CODE"
	 *
	 * @jboss:persistence auto-increment="true"
	 **/
	public abstract Integer getBomCode();         
	public abstract void setBomCode(Integer BOM_CODE);
	
	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="BOM_II_NM"
	 **/
	public abstract String getBomIiName();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setBomIiName(String BOM_II_NM);
	
	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="BOM_LOC_NM"
	 **/
	public abstract String getBomLocName();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setBomLocName(String BOM_LOC_NM);
	
	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="BOM_QNTTY_NDD"
	 **/
	public abstract double getBomQuantityNeeded();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setBomQuantityNeeded(double BOM_QNTTY_NDD);
	
	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="BOM_DWNLD_STATUS"
	 **/
	public abstract char getBomDownloadStatus();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setBomDownloadStatus(char BOM_DWNLD_STATUS);
	
	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="BOM_AD_CMPNY"
	 **/
	public abstract Integer getBomAdCompany();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setBomAdCompany(Integer BOM_AD_CMPNY);
	
	
	// RELATIONSHIP METHODS    
	
	/**
	 * @ejb:interface-method view-type="local"
	 * @ejb:relation name="item-billofmaterials"
	 *               role-name="billofmaterial-has-one-item"
	 *               cascade-delete="no"
	 *
	 * @jboss:relation related-pk-field="iiCode"
	 *                 fk-column="INV_ITEM"
	 */
	public abstract LocalInvItem getInvItem();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setInvItem(LocalInvItem invItem);
	
	 /**
	  * @ejb:interface-method view-type="local"
	  * @ejb:relation name="unitofmeasure-billofmaterials"
	  *               role-name="billofmaterials-has-one-unitofmeasure" 
	  * 
	  *  @jboss:relation related-pk-field="uomCode"
      *                 fk-column="INV_UNIT_OF_MEASURE"	   	  
	  */ 	  
	   public abstract LocalInvUnitOfMeasure getInvUnitOfMeasure();
	   public abstract void setInvUnitOfMeasure(LocalInvUnitOfMeasure invUnitOfMeasure);
	
	// BUSINESS METHODS
	
	// ENTITY METHODS
	
	/**
	 * @ejb:create-method view-type="local"
	 **/
	public Integer ejbCreate(Integer BOM_CODE, String BOM_II_NM, String BOM_LOC_NM, double BOM_QNTTY_NDD,
			char BOM_DWNLD_STATUS, Integer BOM_AD_CMPNY)
		throws CreateException {
		
		Debug.print("InvBillOfMaterialBean ejbCreate");
		
		setBomCode(BOM_CODE);
		setBomIiName(BOM_II_NM);
		setBomLocName(BOM_LOC_NM);
		setBomQuantityNeeded(BOM_QNTTY_NDD);
		setBomDownloadStatus(BOM_DWNLD_STATUS);
		setBomAdCompany(BOM_AD_CMPNY);
		
		return null;
		
	}
	
	/**
	 * @ejb:create-method view-type="local"
	 **/
	public Integer ejbCreate(String BOM_II_NM, String BOM_LOC_NM, double BOM_QNTTY_NDD,
			char BOM_DWNLD_STATUS, Integer BOM_AD_CMPNY)
		throws CreateException {
		
		Debug.print("InvBillOfMaterialBean ejbCreate");
		
		setBomIiName(BOM_II_NM);
		setBomLocName(BOM_LOC_NM);
		setBomQuantityNeeded(BOM_QNTTY_NDD);
		setBomDownloadStatus(BOM_DWNLD_STATUS);
		setBomAdCompany(BOM_AD_CMPNY);
		
		return null;
		
	}
	
	public void ejbPostCreate(Integer BOM_CODE, String BOM_II_NM, String BOM_LOC_NM, double BOM_QNTTY_NDD,
			char BOM_DWNLD_STATUS, Integer BOM_AD_CMPNY)
		throws CreateException { }
	
	public void ejbPostCreate(String BOM_II_NM, String BOM_LOC_NM, double BOM_QNTTY_NDD,
			char BOM_DWNLD_STATUS, Integer BOM_AD_CMPNY)
		throws CreateException { }
	
}

