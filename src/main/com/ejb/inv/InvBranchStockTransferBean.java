package com.ejb.inv;

import java.util.Collection;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;

import com.ejb.ad.LocalAdBranch;
import com.util.AbstractEntityBean;
import com.util.Debug;

/**
 * @ejb:bean name="InvBranchStockTransferEJB"
 *           display-name="Branch Stock Transfer Entity"
 *           type="CMP"
 *           cmp-version="2.x"
 *           view-type="local"
 *           local-jndi-name="omega-ejb/InvBranchStockTransferEJB"
 *           schema="InvBranchStockTransfer"
 *           primkey-field="bstCode"
 *       
 * @ejb:pk class="java.lang.Integer"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="invuser"
 *                        role-link="invuserlink"
 *
 * @ejb:permission role-name="invuser"
 *
 * @ejb:interface local-class="com.ejb.inv.LocalInvBranchStockTransfer"
 *                local-extends="javax.ejb.EJBLocalObject"
 *
 * @ejb:home local-class="com.ejb.inv.LocalInvBranchStockTransferHome"
 *           local-extends="javax.ejb.EJBLocalHome" 
 * 
 * @ejb:finder signature="LocalInvBranchStockTransfer findByBstNumberAndBrCode(java.lang.String BST_NMBR, java.lang.Integer BST_AD_BRNCH, java.lang.Integer BST_AD_CMPNY)"
 *           query="SELECT OBJECT(bst) FROM InvBranchStockTransfer bst  WHERE bst.bstVoid = 0 AND bst.bstNumber = ?1 AND bst.bstAdBranch = ?2 AND bst.bstAdCompany = ?3" 
 * 
 * @ejb:finder signature="LocalInvBranchStockTransfer findByBstNumberAndAdCompany(java.lang.String BST_NMBR, java.lang.Integer BST_AD_CMPNY)"
 *           query="SELECT OBJECT(bst) FROM InvBranchStockTransfer bst  WHERE bst.bstVoid = 0 AND bst.bstNumber = ?1 AND bst.bstAdCompany = ?2"
 *           
 * @ejb:finder signature="LocalInvBranchStockTransfer findAllByBstNumberAndBrCode(java.lang.String BST_NMBR, java.lang.Integer BST_AD_BRNCH, java.lang.Integer BST_AD_CMPNY)"
 *           query="SELECT OBJECT(bst) FROM InvBranchStockTransfer bst  WHERE bst.bstNumber = ?1 AND bst.bstAdBranch = ?2 AND bst.bstAdCompany = ?3"
 * 
 * @ejb:finder signature="Collection findBstByBstTransferOutNumberAndBstAdBranchAndBstAdCompany(java.lang.String BST_OUT_NMBR, java.lang.Integer BST_AD_BRNCH, java.lang.Integer BST_AD_CMPNY)"
 *             query="SELECT OBJECT(bst) FROM InvBranchStockTransfer bst WHERE bst.bstVoid = 0 AND bst.bstPosted = 1 AND bst.bstType = 'IN' AND bst.bstTransferOutNumber = ?1 AND bst.bstAdBranch = ?2 AND bst.bstAdCompany = ?3"
 *  
 * @ejb:finder signature="LocalInvBranchStockTransfer findInByBstOutNumberAndBrCode(java.lang.String BST_NMBR, java.lang.Integer BST_AD_BRNCH, java.lang.Integer BST_AD_CMPNY)"
 *           query="SELECT OBJECT(bst) FROM InvBranchStockTransfer bst  WHERE bst.bstVoid = 0 AND bst.bstType = 'IN' AND bst.bstTransferOutNumber = ?1 AND bst.bstAdBranch = ?2 AND bst.bstAdCompany = ?3"
 * 
 * @jboss:query signature="Collection ejbSelectGeneric(java.lang.String jbossQl, java.lang.Object[] args)"
 *                         dynamic="true" 
 * 
 * @ejb:finder signature="Collection findUnpostedBstByBstDateRange(java.util.Date BST_DT_FRM, java.util.Date BST_DT_TO, java.lang.Integer BST_AD_CMPNY)"
 *             query="SELECT OBJECT(bst) FROM InvBranchStockTransfer bst  WHERE bst.bstVoid = 0 AND bst.bstPosted = 0 AND bst.bstDate >= ?1 AND bst.bstDate <= ?2 AND bst.bstAdCompany = ?3"
 *
 * @jboss:query signature="Collection findUnpostedBstByBstDateRange(java.util.Date BST_DT_FRM, java.util.Date BST_DT_TO, java.lang.Integer BST_AD_CMPNY)"
 *             query="SELECT OBJECT(bst) FROM InvBranchStockTransfer bst  WHERE bst.bstVoid = 0 AND bst.bstPosted = 0 AND bst.bstDate >= ?1 AND bst.bstDate <= ?2 AND bst.bstAdCompany = ?3 ORDER BY bst.bstDate"
 * 
 * @ejb:finder signature="Collection findBstByAdBranchAndAdCompany(java.lang.Integer BST_AD_BRNCH, java.lang.Integer BST_AD_CMPNY)"
 *             query="SELECT OBJECT(bst) FROM InvBranchStockTransfer bst  WHERE bst.bstVoid = 0 AND bst.bstAdBranch = ?1 AND bst.bstAdCompany = ?2"
 *
 * @ejb:finder signature="LocalInvBranchStockTransfer findBstByBstNumberAndAdBranchAndAdCompany(java.lang.String BST_NMBR, java.lang.Integer BST_AD_BRNCH, java.lang.Integer BST_AD_CMPNY)"
 *             query="SELECT OBJECT(bst) FROM InvBranchStockTransfer bst  WHERE bst.bstVoid = 0 AND bst.bstNumber = ?1 AND bst.adBranch.brCode = ?2 AND bst.bstAdCompany = ?3"
 *
 * @ejb:finder signature="Collection findPostedIncomingBstByAdBranchAndBstAdCompany(java.lang.Integer AD_BRNCH, java.lang.Integer BST_AD_CMPNY)"
 *             query="SELECT OBJECT(bst) FROM InvBranchStockTransfer bst WHERE bst.bstVoid = 0 AND bst.bstPosted = 1 AND bst.bstType = 'OUT' AND bst.adBranch.brCode= ?1 AND bst.bstAdCompany = ?2"
 * 
 * @ejb:finder signature="Collection findPostedIncomingBstByAdBranchAndBstAdCompany(java.lang.Integer AD_BRNCH, java.lang.Integer BST_AD_CMPNY)"
 *             query="SELECT OBJECT(bst) FROM InvBranchStockTransfer bst WHERE bst.bstVoid = 0 AND bst.bstPosted = 1 AND bst.bstType = 'OUT' AND bst.adBranch.brCode= ?1 AND bst.bstAdCompany = ?2"
 * 
 *                      
 * @ejb:finder signature="Collection findBstByBstTypeAndAdBranchAndAdCompany(java.lang.String BST_TYP, java.lang.Integer BST_AD_BRNCH, java.lang.Integer BST_AD_CMPNY)"
 *             query="SELECT OBJECT(bst) FROM InvBranchStockTransfer bst WHERE bst.bstType=?1 AND bst.bstVoid = 0 AND (bst.bstApprovalStatus='N/A' OR bst.bstApprovalStatus='APPROVED') AND bst.bstLock=0 AND bst.bstPosted=1 AND bst.adBranch.brCode=?2 AND bst.bstAdCompany=?3"
 *              
 * @ejb:finder signature="Collection findBstByBstTypeAndTransferOrderNumberAndAdBranchAndAdCompany(java.lang.String BST_TYP, java.lang.String BST_TRNSFR_ORDR_NMBR, java.lang.Integer BST_AD_BRNCH, java.lang.Integer BST_AD_CMPNY)"
 *             query="SELECT OBJECT(bst) FROM InvBranchStockTransfer bst WHERE bst.bstType=?1 AND bst.bstTransferOrderNumber=?2 AND bst.bstVoid = 0 AND bst.bstLock=0 AND bst.adBranch.brCode=?3 AND bst.bstAdCompany=?4"
 * 
 * @ejb:finder signature="Collection findDraftBstOrderByBrCode(java.lang.Integer BST_AD_BRNCH, java.lang.Integer BST_AD_CMPNY)"
 *             query="SELECT OBJECT(bst) FROM InvBranchStockTransfer bst WHERE (bst.bstType = 'ORDER' OR bst.bstType = 'REGULAR' OR bst.bstType = 'EMERGENCY') AND bst.bstApprovalStatus IS NULL AND bst.bstAdBranch = ?1 AND bst.bstAdCompany = ?2"
 * 
 * @ejb:finder signature="Collection findDraftBstOutByBrCode(java.lang.Integer BST_AD_BRNCH, java.lang.Integer BST_AD_CMPNY)"
 *             query="SELECT OBJECT(bst) FROM InvBranchStockTransfer bst WHERE bst.bstType = 'OUT' AND bst.bstApprovalStatus IS NULL AND bst.bstAdBranch = ?1 AND bst.bstAdCompany = ?2"
 * 
 * @ejb:finder signature="Collection findDraftBstInByBrCode(java.lang.Integer BST_AD_BRNCH, java.lang.Integer BST_AD_CMPNY)"
 *             query="SELECT OBJECT(bst) FROM InvBranchStockTransfer bst WHERE bst.bstType = 'IN' AND bst.bstApprovalStatus IS NULL AND bst.bstAdBranch = ?1 AND bst.bstAdCompany = ?2"
 *
 * @ejb:finder signature="Collection findPostedBstOrderByBrCode(java.lang.Integer BST_AD_BRNCH, java.lang.Integer BST_AD_CMPNY)"
 *             query="SELECT OBJECT(bst) FROM InvBranchStockTransfer bst WHERE bst.bstType = 'ORDER' AND bst.bstApprovalStatus = 'N/A' AND bst.bstPosted = 1 AND bst.bstAdBranch = ?1 AND bst.bstAdCompany = ?2"
 * 
 * @ejb:value-object match="*"
 *             name="InvBranchStockTransfer"
 * 
 * @jboss:persistence table-name="INV_BRNCH_STCK_TRNSFR"
 *
 * @jboss:entity-command name="mysql-get-generated-keys"
 *
 */

public abstract class InvBranchStockTransferBean extends AbstractEntityBean {
	
	/**
	* @ejb:persistent-field
	* @ejb:interface-method view-type="local"
	* @ejb:pk-field
	*
	* @jboss:column-name name="BST_CODE"
	*
	* @jboss:persistence auto-increment="true"
	**/
   public abstract java.lang.Integer getBstCode();
   public abstract void setBstCode(java.lang.Integer BST_CODE);
   
   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="BST_DT"
    **/
   public abstract java.util.Date getBstDate();
   /**
    * @ejb:interface-method view-type="local"
    **/
   public abstract void setBstDate(java.util.Date BST_DT);
   
   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="BST_TYP"
    **/
   public abstract java.lang.String getBstType();
   /**
    * @ejb:interface-method view-type="local"
    **/
   public abstract void setBstType(java.lang.String BST_TYP);
   
   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="BST_NMBR"
    **/
   public abstract java.lang.String getBstNumber();
   /**
    * @ejb:interface-method view-type="local"
    **/
   public abstract void setBstNumber(java.lang.String BST_NMBR);
   
   
   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="BST_TRNSFR_OUT_NMBR"
    **/
   public abstract java.lang.String getBstTransferOutNumber();
   /**
    * @ejb:interface-method view-type="local"
    **/
   public abstract void setBstTransferOutNumber(java.lang.String BST_TRNSFR_OUT_NMBR);
   
   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="BST_TRNSFR_ORDER_NMBR"
    **/
   public abstract java.lang.String getBstTransferOrderNumber();
   /**
    * @ejb:interface-method view-type="local"
    **/
   public abstract void setBstTransferOrderNumber(java.lang.String BST_TRNSFR_ORDER_NMBR);
   
   
   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="BST_DSCRPTN"
    **/
   public abstract java.lang.String getBstDescription();
   /**
    * @ejb:interface-method view-type="local"
    **/
   public abstract void setBstDescription(java.lang.String BST_DSCRPTN);
   
   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="BST_APPRVL_STATUS"
    **/
   public abstract java.lang.String getBstApprovalStatus();
   /**
    * @ejb:interface-method view-type="local"
    **/
   public abstract void setBstApprovalStatus(java.lang.String BST_APPRVL_STATUS);
	
   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="BST_PSTD"
    **/
   public abstract byte getBstPosted();
   /**
    * @ejb:interface-method view-type="local"
    **/
   public abstract void setBstPosted(byte BST_PSTD);
   
   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="BST_RSN_FR_RJCTN"
    **/
   public abstract java.lang.String getBstReasonForRejection();
   /**
    * @ejb:interface-method view-type="local"
    **/
   public abstract void setBstReasonForRejection(java.lang.String BST_RSN_FR_RJCTN);
   
   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="BST_CRTD_BY"
    **/
   public abstract java.lang.String getBstCreatedBy();
   /**
    * @ejb:interface-method view-type="local"
    **/
   public abstract void setBstCreatedBy(java.lang.String BST_CRTD_BY);
   
   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="BST_DT_CRTD"
    **/
   public abstract java.util.Date getBstDateCreated();
   /**
    * @ejb:interface-method view-type="local"
    **/
   public abstract void setBstDateCreated(java.util.Date BST_DT_CRTD);
   
   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="BST_LST_MDFD_BY"
    **/
   public abstract java.lang.String getBstLastModifiedBy();
   /**
    * @ejb:interface-method view-type="local"
    **/
   public abstract void setBstLastModifiedBy(java.lang.String BST_LST_MDFD_BY);
   
   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="BST_DT_LST_MDFD"
    **/
   public abstract java.util.Date getBstDateLastModified();
   /**
    * @ejb:interface-method view-type="local"
    **/
   public abstract void setBstDateLastModified(java.util.Date BST_DT_LST_MDFD);
   
   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="BST_APPRVD_RJCTD_BY"
    **/
   public abstract java.lang.String getBstApprovedRejectedBy();
   /**
    * @ejb:interface-method view-type="local"
    **/
   public abstract void setBstApprovedRejectedBy(java.lang.String BST_APPRVD_RJCTD_BY);
   
   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="BST_DT_APPRVD_RJCTD"
    **/
   public abstract java.util.Date getBstDateApprovedRejected();
   /**
    * @ejb:interface-method view-type="local"
    **/
   public abstract void setBstDateApprovedRejected(java.util.Date BST_DT_APPRVD_RJCTD);
   
   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="BST_PSTD_BY"
    **/
   public abstract java.lang.String getBstPostedBy();
   /**
    * @ejb:interface-method view-type="local"
    **/
   public abstract void setBstPostedBy(java.lang.String BST_PSTD_BY);
   
   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="BST_DT_PSTD"
    **/
   public abstract java.util.Date getBstDatePosted();
   /**
    * @ejb:interface-method view-type="local"
    **/
   public abstract void setBstDatePosted(java.util.Date BST_DT_PSTD);
    
   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="BST_LCK"
    **/
   public abstract byte getBstLock();
   /**
    * @ejb:interface-method view-type="local"
    **/
   public abstract void setBstLock(byte BST_PSTD);
   
   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="BST_VOID"
    **/
   public abstract byte getBstVoid();
   /**
    * @ejb:interface-method view-type="local"
    **/
   public abstract void setBstVoid(byte BST_VOID);

   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="BST_AD_BRNCH"
    **/
   public abstract java.lang.Integer getBstAdBranch();
   /**
    * @ejb:interface-method view-type="local"
    **/
   public abstract void setBstAdBranch(java.lang.Integer BST_AD_BRNCH);

   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="BST_AD_CMPNY"
    **/
   public abstract java.lang.Integer getBstAdCompany();
   /**
    * @ejb:interface-method view-type="local"
    **/
   public abstract void setBstAdCompany(java.lang.Integer BST_AD_CMPNY);

   
   // Access methods for relationship fields
 
   /**
    * @ejb:interface-method view-type="local"
    * @ejb:relation name="location-branchstocktransfers"
    *               role-name="branchstocktransfer-has-one-location"
    *               cascade-delete="no"
    *
    * @jboss:relation related-pk-field="locCode"
    *                 fk-column="INV_LOCATION"
    */
   public abstract LocalInvLocation getInvLocation();
   /**
    * @ejb:interface-method view-type="local"
    **/
   public abstract void setInvLocation(LocalInvLocation invLocation);   
   
   /**
    * @ejb:interface-method view-type="local"
    * @ejb:relation name="branch-branchstocktransfers"
    *               role-name="branchstocktransfer-has-one-adbranch"
    *               cascade-delete="no"
    *
    * @jboss:relation related-pk-field="brCode"
    *                 fk-column="AD_BRANCH"
    */
   public abstract LocalAdBranch getAdBranch();
   public abstract void setAdBranch(LocalAdBranch adBranch);
   
   /**
    * @ejb:interface-method view-type="local"
    * @ejb:relation name="branchstocktransfer-branchstocktransferlines"
    *               role-name="branchstocktransfer-has-many-branchstocktransferlines"
    */
   public abstract Collection getInvBranchStockTransferLines();
   public abstract void setInvBranchStockTransferLines(Collection invBranchStockTranferLines);
   
   
   
   /**
    * @ejb:interface-method view-type="local"
    * @ejb:relation name="branchstocktransfer-invdistributionrecords"
    *               role-name="branchstocktransfer-has-many-invdistributionrecords"
    */
   public abstract Collection getInvDistributionRecords();
   public abstract void setInvDistributionRecords(Collection invDistributionRecords);
   
   /**
    * @jboss:dynamic-ql
    */
   public abstract Collection ejbSelectGeneric(java.lang.String jbossQl, java.lang.Object[] args)
   throws FinderException;
 

   //BUSINESS METHODS
 
   /**
	 * @ejb:home-method view-type="local"
	 */
   public Collection ejbHomeGetBstByCriteria(java.lang.String jbossQl, java.lang.Object[] args)
   throws FinderException {
		
       return ejbSelectGeneric(jbossQl, args);
		
   }
   
   /**
    * @ejb:interface-method view-type="local"
    **/
   public void addInvBranchStockTransferLine(LocalInvBranchStockTransferLine invBranchStockTransferLine) {

      Debug.print("InvBranchStockTransferBean addBranchInvStockTransferLine");
      try {
         Collection invBranchStockTransferLines = getInvBranchStockTransferLines();
         invBranchStockTransferLines.add(invBranchStockTransferLine);
      } catch (Exception ex) {
         throw new EJBException(ex.getMessage());
      }
   }

   /**
    * @ejb:interface-method view-type="local"
    **/
   public void dropInvBranchStockTransferLine(LocalInvBranchStockTransferLine invBranchStockTransferLine) {
   
      Debug.print("InvBranchStockTransferBean dropInvBranchStockTransferLine");
      try {
         Collection invBranchStockTransferLines = getInvBranchStockTransferLines();
         invBranchStockTransferLines.remove(invBranchStockTransferLine);
      } catch (Exception ex) {
         throw new EJBException(ex.getMessage());
      }
   }
   
   /**
    * @ejb:interface-method view-type="local"
    **/
   public void addInvDistributionRecord(LocalInvDistributionRecord invDistributionRecord) {

      Debug.print("InvBranchStockTransferBean addInvDistributionRecord");
      try {
         Collection invDistributionRecords = getInvDistributionRecords();
         invDistributionRecords.add(invDistributionRecord);
      } catch (Exception ex) {
         throw new EJBException(ex.getMessage());
      }
   }

   /**
    * @ejb:interface-method view-type="local"
    **/
   public void dropInvDistributionRecord(LocalInvDistributionRecord invDistributionRecord) {

      Debug.print("InvBranchStockTransferBean dropInvDistributionRecord");
      try {
         Collection invDistributionRecords = getInvDistributionRecords();
         invDistributionRecords.remove(invDistributionRecord);
      } catch (Exception ex) {
         throw new EJBException(ex.getMessage());
      }
   }

   /**
    * @ejb:interface-method view-type="local"
    **/
   public short getInvDrNextLine() {

       Debug.print("InvBranchStockTransferBean getInvDrNextLine");
       try {
          Collection invDistributionRecords = getInvDistributionRecords();
	      return (short)(invDistributionRecords.size() + 1);		    
       } catch (Exception ex) {	      	
          throw new EJBException(ex.getMessage());	         
       }
    }
   // EntityBean methods
   
   /**
    * @ejb:create-method view-type="local"
    **/
   public java.lang.Integer ejbCreate (java.lang.Integer BST_CODE, java.util.Date BST_DT, java.lang.String BST_TYP,
   		java.lang.String BST_NMBR, java.lang.String BST_TRNSFR_OUT_NMBR, java.lang.String BST_TRNSFR_ORDER_NMBR, java.lang.String BST_DSCRPTN,
		java.lang.String BST_APPRVL_STATUS, byte BST_PSTD, java.lang.String BST_RSN_FR_RJCTN,
		java.lang.String BST_CRTD_BY, java.util.Date BST_DT_CRTD, java.lang.String BST_LST_MDFD_BY,
		java.util.Date BST_DT_LST_MDFD, java.lang.String BST_APPRVD_RJCTD_BY, java.util.Date BST_DT_APPRVD_RJCTD,
		java.lang.String BST_PSTD_BY, java.util.Date BST_DT_PSTD, byte BST_LCK, byte BST_VOID, java.lang.Integer BST_AD_BRNCH,
		java.lang.Integer BST_AD_CMPNY) 
   	   throws CreateException {
       
       Debug.print("InvBranchStockTransferBean ejbCreate");
       
       setBstCode(BST_CODE);
       setBstDate(BST_DT);
       setBstType(BST_TYP);
       setBstNumber(BST_NMBR);
       setBstTransferOutNumber(BST_TRNSFR_OUT_NMBR);
       setBstTransferOrderNumber(BST_TRNSFR_ORDER_NMBR);
       
       setBstDescription(BST_DSCRPTN);
       setBstApprovalStatus(BST_APPRVL_STATUS);
       setBstPosted(BST_PSTD);
       setBstReasonForRejection(BST_RSN_FR_RJCTN);
       setBstCreatedBy(BST_CRTD_BY);
       setBstDateCreated(BST_DT_CRTD);
       setBstLastModifiedBy(BST_LST_MDFD_BY);
       setBstDateLastModified(BST_DT_LST_MDFD);
       setBstApprovedRejectedBy(BST_APPRVD_RJCTD_BY);
       setBstDateApprovedRejected(BST_DT_APPRVD_RJCTD);
       setBstPostedBy(BST_PSTD_BY);
       setBstDatePosted(BST_DT_PSTD);
       setBstLock(BST_LCK);
       setBstVoid(BST_VOID);
       setBstAdBranch(BST_AD_BRNCH);
       setBstAdCompany(BST_AD_CMPNY);
       
       return null;
   }
   
   /**
    * @ejb:create-method view-type="local"
    **/
   public java.lang.Integer ejbCreate (java.util.Date BST_DT, java.lang.String BST_TYP, java.lang.String BST_NMBR,
		java.lang.String BST_TRNSFR_OUT_NMBR, java.lang.String BST_TRNSFR_ORDER_NMBR, java.lang.String BST_DSCRPTN, java.lang.String BST_APPRVL_STATUS,
		byte BST_PSTD, java.lang.String BST_RSN_FR_RJCTN, java.lang.String BST_CRTD_BY, java.util.Date BST_DT_CRTD,
		java.lang.String BST_LST_MDFD_BY, java.util.Date BST_DT_LST_MDFD, java.lang.String BST_APPRVD_RJCTD_BY,
		java.util.Date BST_DT_APPRVD_RJCTD, java.lang.String BST_PSTD_BY, java.util.Date BST_DT_PSTD, byte BST_LCK, byte BST_VOID,
		java.lang.Integer BST_AD_BRNCH, java.lang.Integer BST_AD_CMPNY) 
   	   throws CreateException {
       
       Debug.print("InvBranchStockTransferBean ejbCreate");
       
       setBstDate(BST_DT);
       setBstType(BST_TYP);
       setBstNumber(BST_NMBR);
       setBstTransferOutNumber(BST_TRNSFR_OUT_NMBR);
       setBstTransferOrderNumber(BST_TRNSFR_ORDER_NMBR);
       setBstDescription(BST_DSCRPTN);
       setBstApprovalStatus(BST_APPRVL_STATUS);
       setBstPosted(BST_PSTD);
       setBstReasonForRejection(BST_RSN_FR_RJCTN);
       setBstCreatedBy(BST_CRTD_BY);
       setBstDateCreated(BST_DT_CRTD);
       setBstLastModifiedBy(BST_LST_MDFD_BY);
       setBstDateLastModified(BST_DT_LST_MDFD);
       setBstApprovedRejectedBy(BST_APPRVD_RJCTD_BY);
       setBstDateApprovedRejected(BST_DT_APPRVD_RJCTD);
       setBstPostedBy(BST_PSTD_BY);
       setBstDatePosted(BST_DT_PSTD);
       setBstLock(BST_LCK);
       setBstVoid(BST_VOID);
       setBstAdBranch(BST_AD_BRNCH);
       setBstAdCompany(BST_AD_CMPNY);
       
       return null;
       
   }
   
   
   
   public void ejbPostCreate (java.lang.Integer BST_CODE, java.util.Date BST_DT, java.lang.String BST_TYP,
   		java.lang.String BST_NMBR, java.lang.String BST_TRNSFR_OUT_NMBR, java.lang.String BST_TRNSFR_ORDER_NMBR, java.lang.String BST_DSCRPTN,
		java.lang.String BST_APPRVL_STATUS, byte BST_PSTD, java.lang.String BST_RSN_FR_RJCTN,
		java.lang.String BST_CRTD_BY, java.util.Date BST_DT_CRTD, java.lang.String BST_LST_MDFD_BY,
		java.util.Date BST_DT_LST_MDFD, java.lang.String BST_APPRVD_RJCTD_BY, java.util.Date BST_DT_APPRVD_RJCTD,
		java.lang.String BST_PSTD_BY, java.util.Date BST_DT_PSTD, byte BST_LCK, byte BST_VOID, java.lang.Integer BST_AD_BRNCH,
		java.lang.Integer BST_AD_CMPNY) 
   	   throws CreateException { }
   
   public void ejbPostCreate ( java.util.Date BST_DT, java.lang.String BST_TYP, java.lang.String BST_NMBR,
		java.lang.String BST_TRNSFR_OUT_NMBR, java.lang.String BST_TRNSFR_ORDER_NMBR, java.lang.String BST_DSCRPTN, java.lang.String BST_APPRVL_STATUS,
		byte BST_PSTD, java.lang.String BST_RSN_FR_RJCTN, java.lang.String BST_CRTD_BY, java.util.Date BST_DT_CRTD,
		java.lang.String BST_LST_MDFD_BY, java.util.Date BST_DT_LST_MDFD, java.lang.String BST_APPRVD_RJCTD_BY,
		java.util.Date BST_DT_APPRVD_RJCTD, java.lang.String BST_PSTD_BY, java.util.Date BST_DT_PSTD, byte BST_LCK, byte BST_VOID,
		java.lang.Integer BST_AD_BRNCH, java.lang.Integer BST_AD_CMPNY) 
   	   throws CreateException { }
}