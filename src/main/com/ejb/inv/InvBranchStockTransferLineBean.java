package com.ejb.inv;

import java.util.Collection;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;

import com.util.AbstractEntityBean;
import com.util.Debug;

/**
 * @ejb:bean name="InvBranchStockTransferLineEJB"
 *           display-name="Branch Stock Transfer Line Entity"
 *           type="CMP"
 *           cmp-version="2.x"
 *           view-type="local"
 *           local-jndi-name="omega-ejb/InvBranchStockTransferLineEJB"
 *           schema="InvBranchStockTransferLine"
 *           primkey-field="bslCode"
 *
 * @ejb:pk class="java.lang.Integer"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="invuser"
 *                        role-link="invuserlink"
 *
 * @ejb:permission role-name="invuser"
 *
 * @ejb:interface local-class="com.ejb.inv.LocalInvBranchStockTransferLine"
 *                local-extends="javax.ejb.EJBLocalObject"
 *
 * @ejb:home local-class="com.ejb.inv.LocalInvBranchStockTransferLineHome"
 *           local-extends="javax.ejb.EJBLocalHome"
 *
 * @ejb:finder signature = "Collection findByBstCode(java.lang.Integer BST_CODE, java.lang.Integer BSL_AD_CMPNY)"
 * 				query = "SELECT OBJECT(bsl) FROM InvBranchStockTransferLine bsl WHERE bsl.invBranchStockTransfer.bstCode=?1 AND bsl.bslAdCompany=?2"
 *
 * @ejb:finder signature = "Collection findByBstTransferOutNumberAndIiNameAndLocNameAndBrCode(java.lang.String BST_TRNSFR_OUT_NMBR, java.lang.String INV_ITEM, java.lang.String INV_LOCATION, java.lang.Integer BST_AD_BRNCH, java.lang.Integer BSL_AD_CMPNY)"
 * 				query = "SELECT OBJECT(bsl) FROM InvBranchStockTransferLine bsl WHERE bsl.invBranchStockTransfer.bstTransferOutNumber=?1 AND bsl.invItemLocation.invItem.iiName=?2 AND bsl.invItemLocation.invLocation.locName=?3 AND bsl.invBranchStockTransfer.bstAdBranch=?4 AND bsl.bslAdCompany=?5"
 *
 * @ejb:finder signature = "Collection findByBstTransferOrderNumberAndIiNameAndLocNameAndBrCode(java.lang.String BST_TRNSFR_ORDR_NMBR, java.lang.String INV_ITEM, java.lang.String INV_LOCATION, java.lang.Integer BST_AD_BRNCH, java.lang.Integer BSL_AD_CMPNY)"
 * 				query = "SELECT OBJECT(bsl) FROM InvBranchStockTransferLine bsl WHERE bsl.invBranchStockTransfer.bstTransferOrderNumber=?1 AND bsl.invItemLocation.invItem.iiName=?2 AND bsl.invItemLocation.invLocation.locName=?3 AND bsl.invBranchStockTransfer.bstAdBranch=?4 AND bsl.bslAdCompany=?5"
 *
 * @ejb:finder signature = "Collection findOutBslByBstPostedAndIlCodeAndBrCode(byte BST_PSTD, java.lang.Integer IL_CODE, java.lang.Integer BST_AD_BRNCH, java.lang.Integer BSL_AD_CMPNY)"
 * 				query = "SELECT OBJECT(bsl) FROM InvBranchStockTransferLine bsl WHERE bsl.invBranchStockTransfer.bstType = 'OUT' AND bsl.invBranchStockTransfer.bstPosted=?1 AND bsl.invItemLocation.ilCode=?2 AND bsl.invBranchStockTransfer.bstAdBranch=?3 AND bsl.bslAdCompany=?4"
 *
 * @ejb:finder signature = "Collection findInBslByBstPostedAndLocCodeAndIiCodeAndAdBrCode(byte BST_PSTD, java.lang.Integer LOC_CODE, java.lang.Integer II_CODE, java.lang.Integer BST_AD_BRNCH, java.lang.Integer BSL_AD_CMPNY)"
 * 				query = "SELECT OBJECT(bsl) FROM InvBranchStockTransferLine bsl WHERE bsl.invBranchStockTransfer.bstType = 'IN' AND bsl.invBranchStockTransfer.bstPosted=?1 AND bsl.invBranchStockTransfer.invLocation.locCode=?2 AND bsl.invItemLocation.invItem.iiCode=?3 AND bsl.invBranchStockTransfer.adBranch.brCode=?4 AND bsl.bslAdCompany=?5"
 *
 * @ejb:finder signature = "Collection findInBslByBstTransferOutNumberAndItemLocAndAdBranchAndAdCompany(java.lang.String BST_TRNSFR_OUT_NMBR, java.lang.Integer IL_CODE, java.lang.Integer BST_AD_BRNCH, java.lang.Integer BSL_AD_CMPNY)"
 * 				query = "SELECT OBJECT(bsl) FROM InvBranchStockTransferLine bsl WHERE bsl.invBranchStockTransfer.bstVoid = 0 AND bsl.invBranchStockTransfer.bstPosted = 1 AND bsl.invBranchStockTransfer.bstType = 'IN' AND bsl.invBranchStockTransfer.bstTransferOutNumber=?1 AND bsl.invItemLocation.ilCode=?2 AND bsl.invBranchStockTransfer.bstAdBranch=?3 AND bsl.bslAdCompany=?4"
 *
 *
 *
 * @ejb:finder signature="Collection findPostedIncomingBslByBstTransferOutNumberAndBstAdBranchAndBstAdCompany(java.lang.String BST_OUT_NMBR, java.lang.Integer BST_AD_BRNCH, java.lang.Integer BST_AD_CMPNY)"
 *             query="SELECT OBJECT(bsl) FROM InvBranchStockTransferLine bsl WHERE bsl.invBranchStockTransfer.bstVoid = 0 AND bsl.invBranchStockTransfer.bstPosted = 1 AND bsl.invBranchStockTransfer.bstType = 'OUT' AND bsl.invBranchStockTransfer.bstTransferOutNumber = ?1 AND bsl.invBranchStockTransfer.bstAdBranch = ?2 AND bsl.invBranchStockTransfer.bstAdCompany = ?3"
 *
 * @ejb:finder signature = "Collection findByAdCompanyAll(java.lang.Integer BSL_AD_CMPNY)"
 * 				query = "SELECT OBJECT(bsl) FROM InvBranchStockTransferLine bsl WHERE bsl.bslAdCompany=?1"
 *
 *
 * @ejb:finder signature="Collection findUnpostedBstByIiNameAndLocNameAndLessEqualDateAndBstAdBranch(java.lang.String II_NM, java.lang.String LOC_NM, java.util.Date BST_DT, java.lang.Integer BST_AD_BRNCH, java.lang.Integer BSL_AD_CMPNY)"
 * 			   query="SELECT OBJECT(bsl) FROM InvBranchStockTransferLine bsl WHERE bsl.invBranchStockTransfer.bstPosted = 0 AND bsl.invBranchStockTransfer.bstVoid = 0 AND bsl.invItemLocation.invItem.iiName = ?1 AND bsl.invItemLocation.invLocation.locName = ?2 AND bsl.invBranchStockTransfer.bstDate <= ?3 AND bsl.invBranchStockTransfer.bstAdBranch = ?4 AND bsl.bslAdCompany = ?5"
 *
 * @ejb:finder signature="Collection findQtyTransferedByBstTranferOutNumberAndIiCode(java.lang.String BST_TRNSFR_OUT_NMBR, java.lang.String II_NAME, java.lang.String LOC_NAME, java.lang.Integer BSL_AD_CMPNY)"
 * 			   query="SELECT OBJECT(bsl) FROM InvBranchStockTransferLine bsl WHERE bsl.invBranchStockTransfer.bstType = 'IN' AND bsl.invBranchStockTransfer.bstTransferOutNumber = ?1 AND bsl.invItemLocation.invItem.iiName = ?2 AND bsl.invItemLocation.invLocation.locName = ?3 AND bsl.bslAdCompany = ?4"
 *
 * @ejb:finder signature="Collection findUnpostedBstByLocNameAndAdBranch(java.lang.String LOC_NM, java.lang.Integer BST_AD_BRNCH, java.lang.Integer BSL_AD_CMPNY)"
 * 			   query="SELECT OBJECT(bsl) FROM InvBranchStockTransferLine bsl WHERE bsl.invBranchStockTransfer.bstPosted = 0 AND bsl.invBranchStockTransfer.bstVoid = 0 AND bsl.invItemLocation.invLocation.locName = ?1 AND bsl.invBranchStockTransfer.bstAdBranch = ?2 AND bsl.bslAdCompany = ?3"
 *
 * @ejb:finder signature="Collection findByIiCodeAndLocNameAndAdBranch(java.lang.Integer II_CODE, java.lang.String LOC_NM, java.lang.Integer BST_AD_BRNCH, java.lang.Integer BSL_AD_CMPNY)"
 * 			   query="SELECT OBJECT(bsl) FROM InvBranchStockTransferLine bsl WHERE bsl.invBranchStockTransfer.bstVoid = 0 AND bsl.invItemLocation.invItem.iiCode=?1 AND bsl.invItemLocation.invLocation.locName = ?2 AND bsl.invBranchStockTransfer.bstAdBranch = ?3 AND bsl.bslAdCompany = ?4"
 *
 * @jboss:query signature="Collection ejbSelectGeneric(java.lang.String jbossQl, java.lang.Object[] args)"
 *                         dynamic="true"
 *
 * @ejb:value-object match="*"
 *             name="InvBranchStockTransferLine"
 *
 * @jboss:persistence table-name="INV_BRNCH_STCK_TRNSFR_LN"
 *
 * @jboss:entity-command name="mysql-get-generated-keys"
 *
 */

public abstract class InvBranchStockTransferLineBean extends AbstractEntityBean {

        // access methods for persistent fields

        /**
     	 * @ejb:persistent-field
     	 * @ejb:interface-method view-type="local"
     	 * @ejb:pk-field
     	 *
     	 * @jboss:column-name name="BSL_CODE"
     	 *
     	 * @jboss:persistence auto-increment="true"
     	 **/
    	public abstract java.lang.Integer getBslCode();
    	public abstract void setBslCode(java.lang.Integer BSL_CODE);

        /**
	     * @ejb:persistent-field
	     * @ejb:interface-method view-type="local"
	     *
	     * @jboss:column-name name="BSL_QTY"
	     **/
    	public abstract double getBslQuantity();
	    /**
	     * @ejb:interface-method view-type="local"
	     **/
	    public abstract void setBslQuantity(double BSL_QTY);

	    /**
	     * @ejb:persistent-field
	     * @ejb:interface-method view-type="local"
	     *
	     * @jboss:column-name name="BSL_QTY_RCVD"
	     **/
	    public abstract double getBslQuantityReceived();
	    /**
	     * @ejb:interface-method view-type="local"
	     **/
	    public abstract void setBslQuantityReceived(double BSL_QTY_RCVD);

	    /**
	     * @ejb:persistent-field
	     * @ejb:interface-method view-type="local"
	     *
	     * @jboss:column-name name="BSL_UNT_CST"
	     **/
    	public abstract double getBslUnitCost();
	    /**
	     * @ejb:interface-method view-type="local"
	     **/
	    public abstract void setBslUnitCost(double BSL_UNT_CST);

	    /**
	     * @ejb:persistent-field
	     * @ejb:interface-method view-type="local"
	     *
	     * @jboss:column-name name="BSL_AMNT"
	     **/
    	public abstract double getBslAmount();
	    /**
	     * @ejb:interface-method view-type="local"
	     **/
	    public abstract void setBslAmount(double BSL_AMNT);

	    /**
	     * @ejb:persistent-field
	     * @ejb:interface-method view-type="local"
	     *
	     * @jboss:column-name name="BSL_AD_CMPNY"
	     **/
    	public abstract Integer getBslAdCompany();
    	/**
	     * @ejb:interface-method view-type="local"
	     **/
	    public abstract void setBslMisc(String BSL_MISC);

	    /**
	     * @ejb:persistent-field
	     * @ejb:interface-method view-type="local"
	     *
	     * @jboss:column-name name="BSL_EXPRY_DT"
	     **/
    	public abstract String getBslMisc();
	    /**
	     * @ejb:interface-method view-type="local"
	     **/
	    public abstract void setBslAdCompany(java.lang.Integer BSL_AD_CMPNY);


	    // Access methods for relationship fields
	    /**
	     * @ejb:interface-method view-type="local"
	     * @ejb:relation name="branchstocktransfer-branchstocktransferlines"
	     *               role-name="branchstocktransferline-has-one-branchstocktransfer"
	     *               cascade-delete="yes"
	     *
	     * @jboss:relation related-pk-field="bstCode"
	     *                 fk-column="INV_BRANCH_STOCK_TRANSFER"
	     */
	    public abstract LocalInvBranchStockTransfer getInvBranchStockTransfer();
	    /**
	     * @ejb:interface-method view-type="local"
	     **/
	    public abstract void setInvBranchStockTransfer(LocalInvBranchStockTransfer invBranchStockTransfer);

	    /**
	     * @ejb:interface-method view-type="local"
	     * @ejb:relation name="unitofmeasure-branchstocktransferlines"
	     *               role-name="branchstocktransferline-has-one-unitofmeasure"
	     *               cascade-delete="no"
	     *
	     * @jboss:relation related-pk-field="uomCode"
	     *                 fk-column="INV_UNIT_OF_MEASURE"
	     */
	    public abstract LocalInvUnitOfMeasure getInvUnitOfMeasure();
	    /**
	     * @ejb:interface-method view-type="local"
	     **/
	    public abstract void setInvUnitOfMeasure(LocalInvUnitOfMeasure invUnitOfMeasure);

	    /**
	     * @ejb:interface-method view-type="local"
	     * @ejb:relation name="itemlocation-branchstocktransferlines"
	     *               role-name="branchstocktransferline-has-one-itemlocation"
	     *               cascade-delete="no"
	     *
	     * @jboss:relation related-pk-field="ilCode"
	     *                 fk-column="INV_ITEM_LOCATION"
	     */
	    public abstract LocalInvItemLocation getInvItemLocation();
	    /**
	     * @ejb:interface-method view-type="local"
	     **/
	    public abstract void setInvItemLocation(LocalInvItemLocation invItemLocation);

	    /**
	     * @ejb:interface-method view-type="local"
  	     * @ejb:relation name="branchstocktransferline-costings"
	     *               role-name="branchstocktransferline-has-many-costings"
	    */
	    public abstract Collection getInvCostings();
	    public abstract void setInvCostings(Collection invCostings);

	    /**
	     * @ejb:interface-method view-type="local"
	     * @ejb:relation name="branchstocktransferline-tags"
	     *               role-name="branchstocktransferline-has-many-tags"
	     *
	     */
	    public abstract Collection getInvTags();
	    public abstract void setInvTags(Collection invTags);
	    /**
		* @jboss:dynamic-ql
		*/
	   public abstract Collection ejbSelectGeneric(java.lang.String jbossQl, java.lang.Object[] args)
	   throws FinderException;

	    // Business methods

	    /**
		 * @ejb:home-method view-type="local"
	     **/
	    public Collection ejbHomeGetBstlByCriteria(java.lang.String jbossQl, java.lang.Object[] args)
	    throws FinderException {

	        return ejbSelectGeneric(jbossQl, args);

	    }

	    /**
		 * @ejb:interface-method view-type="local"
		 **/
		public void addInvCosting(LocalInvCosting invCosting) {

			Debug.print("InvBranchStockTransferLineBean addInvCosting");

			try {

				Collection invCostings = getInvCostings();
				invCostings.add(invCosting);

			} catch (Exception ex) {

				throw new EJBException(ex.getMessage());

			}

		}

		/**
		 * @ejb:interface-method view-type="local"
		 **/
		public void dropInvCosting(LocalInvCosting invCosting) {

			Debug.print("InvBranchStockTransferLineBean dropInvCosting");

			try {

				Collection invCostings = getInvCostings();
				invCostings.remove(invCosting);

			} catch (Exception ex) {

				throw new EJBException(ex.getMessage());

			}

		}

	    // EntityBean methods

	    /**
 	     * @ejb:create-method view-type="local"
	     **/
	    public java.lang.Integer ejbCreate (java.lang.Integer BSL_CODE, double BSL_QTY, double BSL_QTY_RCVD, double BSL_UNT_CST, double BSL_AMNT, java.lang.Integer BSL_AD_CMPNY)
	       throws CreateException {

	        Debug.print("InvBranchStockTransferLineBean ejbCreate");

	        setBslCode(BSL_CODE);
	        setBslQuantity(BSL_QTY);
	        setBslQuantityReceived(BSL_QTY_RCVD);
	        setBslUnitCost(BSL_UNT_CST);
	        setBslAmount(BSL_AMNT);
	        setBslAdCompany(BSL_AD_CMPNY);

	        return null;
	    }

	    /**
 	     * @ejb:create-method view-type="local"
	     **/
	    public java.lang.Integer ejbCreate (double BSL_QTY, double BSL_QTY_RCVD, double BSL_UNT_CST, double BSL_AMNT, java.lang.Integer BSL_AD_CMPNY)
		throws CreateException {

		Debug.print("InvBranchStockTransferLineBean ejbCreate");

		setBslQuantity(BSL_QTY);
		setBslQuantityReceived(BSL_QTY_RCVD);
		setBslUnitCost(BSL_UNT_CST);
		setBslAmount(BSL_AMNT);
		setBslAdCompany(BSL_AD_CMPNY);

		return null;
		}

	    public void ejbPostCreate (java.lang.Integer BSL_CODE, double BSL_QTY, double BSL_QTY_RCVD, double BSL_UNT_CST, double BSL_AMNT, java.lang.Integer BSL_AD_CMPNY) throws CreateException { }

	    public void ejbPostCreate (double BSL_QTY, double BSL_QTY_RCVD, double BSL_UNT_CST, double BSL_AMNT, java.lang.Integer BSL_AD_CMPNY) throws CreateException { }


}



