package com.ejb.inv;

import java.util.Collection;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;

import com.util.AbstractEntityBean;
import com.util.Debug;

/**
 * @ejb:bean name="InvBuildOrderEJB"
 *           display-name="Build Order Entity"
 *           type="CMP"
 *           cmp-version="2.x"
 *           view-type="local"
 *           local-jndi-name="omega-ejb/InvBuildOrderEJB"
 *           schema="InvBuildOrder"
 *           primkey-field="borCode"
 *       
 * @ejb:pk class="java.lang.Integer"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="invuser"
 *                        role-link="invuserlink"
 *
 * @ejb:permission role-name="invuser"
 *
 * @ejb:interface local-class="com.ejb.inv.LocalInvBuildOrder"
 *                local-extends="javax.ejb.EJBLocalObject"
 *
 * @ejb:home local-class="com.ejb.inv.LocalInvBuildOrderHome"
 *           local-extends="javax.ejb.EJBLocalHome"
 * 
 * @ejb:select signature="Collection ejbSelectGeneric(java.lang.String jbossQl, java.lang.Object[] args)"
 *             result-type-mapping="Local"
 *             method-intf="LocalHome"
 *             query="SELECT OBJECT(bor) FROM InvBuildOrder bor"
 *
 * @jboss:query signature="Collection ejbSelectGeneric(java.lang.String jbossQl, java.lang.Object[] args)"
 *                         dynamic="true"
 *
 * @ejb:finder signature="LocalInvBuildOrder findByBorDocumentNumberAndBrCode(java.lang.String BOR_DCMNT_NMBR, java.lang.Integer BOR_AD_BRNCH, java.lang.Integer BOR_AD_CMPNY)"
 *             query="SELECT OBJECT(bor) FROM InvBuildOrder bor WHERE bor.borDocumentNumber = ?1 AND bor.borAdBranch = ?2 AND bor.borAdCompany = ?3"
 *
 * @ejb:value-object match="*"
 *             name="InvBuildOrder"
 *
 * @jboss:persistence table-name="INV_BLD_ORDR"
 *
 * @jboss:entity-command name="mysql-get-generated-keys"
 *
 */

public abstract class InvBuildOrderBean extends AbstractEntityBean {
	
//	 Access methods for persistent fields

	   /**
	    * @ejb:persistent-field
	    * @ejb:interface-method view-type="local"
	    * @ejb:pk-field
	    *
	    * @jboss:column-name name="BOR_CODE"
	    *
	    * @jboss:persistence auto-increment="true"
	    **/
	   public abstract java.lang.Integer getBorCode();
	   public abstract void setBorCode(java.lang.Integer BOR_CODE);

	   /**
	    * @ejb:persistent-field
	    * @ejb:interface-method view-type="local"
	    *
	    * @jboss:column-name name="BOR_DT"
	    **/
	   public abstract java.util.Date getBorDate();
	   /**
	    * @ejb:interface-method view-type="local"
	    **/
	   public abstract void setBorDate(java.util.Date BOR_DT);
	   
	   /**
	    * @ejb:persistent-field
	    * @ejb:interface-method view-type="local"
	    *
	    * @jboss:column-name name="BOR_DCMNT_NMBR"
	    **/
	   public abstract java.lang.String getBorDocumentNumber();
	   /**
	    * @ejb:interface-method view-type="local"
	    **/
	   public abstract void setBorDocumentNumber(java.lang.String BOR_DCMNT_NMBR);	   
	   
	   /**
	    * @ejb:persistent-field
	    * @ejb:interface-method view-type="local"
	    *
	    * @jboss:column-name name="BOR_RFRNC_NMBR"
	    **/
	   public abstract java.lang.String getBorReferenceNumber();
	   /**
	    * @ejb:interface-method view-type="local"
	    **/
	   public abstract void setBorReferenceNumber(java.lang.String BOR_RFRNC_NMBR);
	   
	   /**
	    * @ejb:persistent-field
	    * @ejb:interface-method view-type="local"
	    *
	    * @jboss:column-name name="BOR_DESC"
	    **/
	   public abstract String getBorDescription();
	   /**
	    * @ejb:interface-method view-type="local"
	    **/
	   public abstract void setBorDescription(String BOR_DESC);
	   
	   /**
	    * @ejb:persistent-field
	    * @ejb:interface-method view-type="local"
	    *
	    * @jboss:column-name name="BOR_VOID"
	    **/
	   public abstract byte getBorVoid();
	   /**
	    * @ejb:interface-method view-type="local"
	    **/
	   public abstract void setBorVoid(byte BOR_VOID);
	   /**
	    * @ejb:persistent-field
	    * @ejb:interface-method view-type="local"
	    *
	    * @jboss:column-name name="BOR_APPRVL_STATUS"
	    **/
	   public abstract java.lang.String getBorApprovalStatus();
	   /**
	    * @ejb:interface-method view-type="local"
	    **/
	   public abstract void setBorApprovalStatus(java.lang.String BOR_APPRVL_STATUS);
	   
	   /**
	    * @ejb:persistent-field
	    * @ejb:interface-method view-type="local"
	    *
	    * @jboss:column-name name="BOR_PSTD"
	    **/
	   public abstract byte getBorPosted();
	   /**
	    * @ejb:interface-method view-type="local"
	    **/
	   public abstract void setBorPosted(byte BOR_PSTD);
	   
	   
	   /**
	    * @ejb:persistent-field
	    * @ejb:interface-method view-type="local"
	    *
	    * @jboss:column-name name="BOR_CRTD_BY"
	    **/
	   public abstract String getBorCreatedBy();
	   /**
	    * @ejb:interface-method view-type="local"
	    **/
	   public abstract void setBorCreatedBy(String BOR_CRTD_BY);
	   
	   /**
	    * @ejb:persistent-field
	    * @ejb:interface-method view-type="local"
	    *
	    * @jboss:column-name name="BOR_DT_CRTD"
	    **/
	   public abstract java.util.Date getBorDateCreated();
	   /**
	    * @ejb:interface-method view-type="local"
	    **/
	   public abstract void setBorDateCreated(java.util.Date BOR_DT_CRTD);
	   
	   /**
	    * @ejb:persistent-field
	    * @ejb:interface-method view-type="local"
	    *
	    * @jboss:column-name name="BOR_LST_MDFD_BY"
	    **/
	   public abstract String getBorLastModifiedBy();
	   /**
	    * @ejb:interface-method view-type="local"
	    **/
	   public abstract void setBorLastModifiedBy(String BOR_LST_MDFD_BY);
	   
	   /**
	    * @ejb:persistent-field
	    * @ejb:interface-method view-type="local"
	    *
	    * @jboss:column-name name="BOR_DT_LST_MDFD"
	    **/
	   public abstract java.util.Date getBorDateLastModified();
	   /**
	    * @ejb:interface-method view-type="local"
	    **/
	   public abstract void setBorDateLastModified(java.util.Date BOR_DT_LST_MDFD);
	   
	   /**
	    * @ejb:persistent-field
	    * @ejb:interface-method view-type="local"
	    *
	    * @jboss:column-name name="BOR_APPRVD_RJCTD_BY"
	    **/
	   public abstract String getBorApprovedRejectedBy();
	   /**
	    * @ejb:interface-method view-type="local"
	    **/
	   public abstract void setBorApprovedRejectedBy(String BOR_APPRVD_RJCTD_BY);
	   
	   /**
	    * @ejb:persistent-field
	    * @ejb:interface-method view-type="local"
	    *
	    * @jboss:column-name name="BOR_DT_APPRVD_RJCTD"
	    **/
	   public abstract java.util.Date getBorDateApprovedRejected();
	   /**
	    * @ejb:interface-method view-type="local"
	    **/
	   public abstract void setBorDateApprovedRejected(java.util.Date BOR_DT_APPRVD_RJCTD);
	   
	   /**
	    * @ejb:persistent-field
	    * @ejb:interface-method view-type="local"
	    *
	    * @jboss:column-name name="BOR_PSTD_BY"
	    **/
	   public abstract String getBorPostedBy();
	   /**
	    * @ejb:interface-method view-type="local"
	    **/
	   public abstract void setBorPostedBy(String BOR_PSTD_BY);
	   
	   /**
	    * @ejb:persistent-field
	    * @ejb:interface-method view-type="local"
	    *
	    * @jboss:column-name name="BOR_DT_PSTD"
	    **/
	   public abstract java.util.Date getBorDatePosted();
	   /**
	    * @ejb:interface-method view-type="local"
	    **/
	   public abstract void setBorDatePosted(java.util.Date BOR_DT_PSTD);
	   
	   /**
	     * @ejb:persistent-field
	     * @ejb:interface-method view-type="local"
	     *
	     * @jboss:column-name name="BOR_RSN_FR_RJCTN"
	     **/
      public abstract String getBorReasonForRejection();
      /**
       * @ejb:interface-method view-type="local"
       **/
      public abstract void setBorReasonForRejection(String BOR_RSN_FR_RJCTN);  
      
	   /**
	    * @ejb:persistent-field
	    * @ejb:interface-method view-type="local"
	    *
	    * @jboss:column-name name="BOR_SO_NMBR"
	    **/
	   public abstract java.lang.String getBorSoNumber();
	   /**
	    * @ejb:interface-method view-type="local"
	    **/
	   public abstract void setBorSoNumber(java.lang.String BOR_SO_NMBR);	   

	   /**
       * @ejb:persistent-field
       * @ejb:interface-method view-type="local"
       *
       * @jboss:column-name name="BOR_AD_BRNCH"
       **/
      public abstract Integer getBorAdBranch();
      /**
       * @ejb:interface-method view-type="local"
       **/
      public abstract void setBorAdBranch(Integer BOR_AD_BRNCH);  
      
      /**
       * @ejb:persistent-field
       * @ejb:interface-method view-type="local"
       *
       * @jboss:column-name name="BOR_AD_CMPNY"
       **/
      public abstract Integer getBorAdCompany();
      /**
       * @ejb:interface-method view-type="local"
       **/
      public abstract void setBorAdCompany(Integer BOR_AD_CMPNY);  

	   // Access methods for relationship fields
	   
	   /**
	    * @ejb:interface-method view-type="local"
	    * @ejb:relation name="buildorder-buildordelines"
	    *               role-name="buildorder-has-many-buildorderlines"
	    */
	   public abstract Collection getInvBuildOrderLines();
	   public abstract void setInvBuildOrderLines(Collection invBuildOrderLines);
	   		
		/**
		 * @jboss:dynamic-ql
		 */
		public abstract Collection ejbSelectGeneric(java.lang.String jbossQl, java.lang.Object[] args)
		throws FinderException;
		
		// BUSINESS METHODS
		
		/**
		 * @ejb:home-method view-type="local"
		 */
		public Collection ejbHomeGetBorByCriteria(java.lang.String jbossQl, java.lang.Object[] args)
		throws FinderException {
			
			return ejbSelectGeneric(jbossQl, args);
			
		}
			   
	   /**
	    * @ejb:interface-method view-type="local"
	    **/
	   public void addInvBuildOrderLine(LocalInvBuildOrderLine invBuildOrderLine) {

	      Debug.print("InvBuildOrderBean addInvBuildOrderLine");
	      try {
	         Collection invBuildOrderLines = getInvBuildOrderLines();
	         invBuildOrderLines.add(invBuildOrderLine);
	      } catch (Exception ex) {
	         throw new EJBException(ex.getMessage());
	      }
	   }

	   /**
	    * @ejb:interface-method view-type="local"
	    **/
	   public void dropInvBuildOrderLine(LocalInvBuildOrderLine invBuildOrderLine) {
	   
	      Debug.print("InvBuildOrderBean dropInvBuildOrderLine");
	      try {
	         Collection invBuildOrderLines = getInvBuildOrderLines();
	         invBuildOrderLines.remove(invBuildOrderLine);
	      } catch (Exception ex) {
	         throw new EJBException(ex.getMessage());
	      }
	   }



	   // EntityBean methods

	   /**
	    * @ejb:create-method view-type="local"
	    **/
	   
	   
	   public java.lang.Integer ejbCreate (java.lang.Integer BOR_CODE, java.util.Date BOR_DT,
	      String BOR_DCMNT_NMBR,String BOR_RFRNC_NMBR,String BOR_DESC,byte BOR_VOID,java.lang.String BOR_APPRVL_STATUS,
		  java.lang.String BOR_PSTD_BY,java.lang.String  BOR_CRTD_BY, java.util.Date BOR_DT_CRTD, 
		  java.lang.String BOR_LST_MDFD_BY, java.util.Date BOR_DT_LST_MDFD, 
		  java.lang.String BOR_APPRVD_RJCTD_BY, java.util.Date BOR_DT_APPRVD_RJCTD, 
		  byte BOR_PSTD, java.util.Date BOR_DT_PSTD,java.lang.String BOR_RSN_FR_RJCTN,
		  java.lang.String BOR_SO_NMBR, Integer BOR_AD_BRNCH, Integer BOR_AD_CMPNY)
	      throws CreateException {

	      Debug.print("invBuildOrderBean ejbCreate");
	      
	      setBorCode(BOR_CODE);
	      setBorDate(BOR_DT);
	      setBorDocumentNumber(BOR_DCMNT_NMBR);
	      setBorReferenceNumber(BOR_RFRNC_NMBR);
	      setBorDescription(BOR_DESC);
	      setBorVoid(BOR_VOID);
	      setBorApprovalStatus(BOR_APPRVL_STATUS);
	      setBorPosted(BOR_PSTD);
	      setBorCreatedBy(BOR_CRTD_BY);
	      setBorDateCreated(BOR_DT_CRTD);
	      setBorLastModifiedBy(BOR_LST_MDFD_BY);
	      setBorDateLastModified(BOR_DT_LST_MDFD);
	      setBorApprovedRejectedBy(BOR_APPRVD_RJCTD_BY);
	      setBorDateApprovedRejected(BOR_DT_APPRVD_RJCTD);
	      setBorPostedBy(BOR_PSTD_BY);
	      setBorDatePosted(BOR_DT_PSTD);
	      setBorReasonForRejection(BOR_RSN_FR_RJCTN);
	      setBorSoNumber(BOR_SO_NMBR);
	      setBorAdBranch(BOR_AD_BRNCH);
	      setBorAdCompany(BOR_AD_CMPNY);
	      
	      return null;
	   }

	   /**
	    * @ejb:create-method view-type="local"
	    **/
	   public java.lang.Integer ejbCreate (java.util.Date BOR_DT,
		      String BOR_DCMNT_NMBR,String BOR_RFRNC_NMBR,String BOR_DESC,byte BOR_VOID,java.lang.String BOR_APPRVL_STATUS,
			  byte BOR_PSTD,java.lang.String  BOR_CRTD_BY, java.util.Date BOR_DT_CRTD, 
			  java.lang.String BOR_LST_MDFD_BY, java.util.Date BOR_DT_LST_MDFD, 
			  java.lang.String BOR_APPRVD_RJCTD_BY, java.util.Date BOR_DT_APPRVD_RJCTD, 
			  java.lang.String BOR_PSTD_BY, java.util.Date BOR_DT_PSTD,java.lang.String BOR_RSN_FR_RJCTN,
			  java.lang.String BOR_SO_NMBR, Integer BOR_AD_BRNCH, Integer BOR_AD_CMPNY)
	      throws CreateException {

	      Debug.print("invBuildOrderBean ejbCreate");
	      
	      
	      setBorDate(BOR_DT);
	      setBorDocumentNumber(BOR_DCMNT_NMBR);
	      setBorReferenceNumber(BOR_RFRNC_NMBR);
	      setBorDescription(BOR_DESC);
	      setBorVoid(BOR_VOID);
	      setBorApprovalStatus(BOR_APPRVL_STATUS);
	      setBorPostedBy(BOR_PSTD_BY);
	      setBorCreatedBy(BOR_CRTD_BY);
	      setBorDateCreated(BOR_DT_CRTD);
	      setBorLastModifiedBy(BOR_LST_MDFD_BY);
	      setBorDateLastModified(BOR_DT_LST_MDFD);
	      setBorApprovedRejectedBy(BOR_APPRVD_RJCTD_BY);
	      setBorDateApprovedRejected(BOR_DT_APPRVD_RJCTD);
	      setBorPostedBy(BOR_PSTD_BY);
	      setBorDatePosted(BOR_DT_PSTD);
	      setBorReasonForRejection(BOR_RSN_FR_RJCTN);
	      setBorSoNumber(BOR_SO_NMBR);
	      setBorAdBranch(BOR_AD_BRNCH);
	      setBorAdCompany(BOR_AD_CMPNY);
	      
	      return null;
	   }

	   public void ejbPostCreate (java.lang.Integer BOR_CODE, java.util.Date BOR_DT,
		      String BOR_DCMNT_NMBR,String BOR_RFRNC_NMBR,String BOR_DESC,byte BOR_VOID,java.lang.String BOR_APPRVL_STATUS,
			  java.lang.String BOR_PSTD_BY,java.lang.String  BOR_CRTD_BY, java.util.Date BOR_DT_CRTD, 
			  java.lang.String BOR_LST_MDFD_BY, java.util.Date BOR_DT_LST_MDFD, 
			  java.lang.String BOR_APPRVD_RJCTD_BY, java.util.Date BOR_DT_APPRVD_RJCTD, 
			  byte BOR_PSTD, java.util.Date BOR_DT_PSTD,java.lang.String BOR_RSN_FR_RJCTN,
			  java.lang.String BOR_SO_NMBR, Integer BOR_AD_BRNCH, Integer BOR_AD_CMPNY)
	      throws CreateException { }

	   public void ejbPostCreate (java.util.Date BOR_DT,
		      String BOR_DCMNT_NMBR,String BOR_RFRNC_NMBR,String BOR_DESC,byte BOR_VOID,java.lang.String BOR_APPRVL_STATUS,
			  byte BOR_PSTD,java.lang.String  BOR_CRTD_BY, java.util.Date BOR_DT_CRTD, 
			  java.lang.String BOR_LST_MDFD_BY, java.util.Date BOR_DT_LST_MDFD, 
			  java.lang.String BOR_APPRVD_RJCTD_BY, java.util.Date BOR_DT_APPRVD_RJCTD, 
			  java.lang.String BOR_PSTD_BY, java.util.Date BOR_DT_PSTD,java.lang.String BOR_RSN_FR_RJCTN,
			  java.lang.String BOR_SO_NMBR, Integer BOR_AD_BRNCH, Integer BOR_AD_CMPNY)
	      throws CreateException { }
	
}
