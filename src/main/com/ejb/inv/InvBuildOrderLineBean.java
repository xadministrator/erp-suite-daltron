package com.ejb.inv;

import java.util.Collection;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;

import com.ejb.ar.LocalArJobOrderLine;
import com.ejb.ar.LocalArSalesOrderLine;
import com.util.AbstractEntityBean;
import com.util.Debug;

/**
 * @ejb:bean name="InvBuildOrderLineEJB"
 *           display-name="Build Order Line Entity"
 *           type="CMP"
 *           cmp-version="2.x"
 *           view-type="local"
 *           local-jndi-name="omega-ejb/InvBuildOrderLineEJB"
 *           schema="InvBuildOrderLine"
 *           primkey-field="bolCode"
 *       
 * @ejb:pk class="java.lang.Integer"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="invuser"
 *                        role-link="invuserlink"
 *
 * @ejb:permission role-name="invuser"
 *
 * @ejb:interface local-class="com.ejb.inv.LocalInvBuildOrderLine"
 *                local-extends="javax.ejb.EJBLocalObject"
 *
 * @ejb:home local-class="com.ejb.inv.LocalInvBuildOrderLineHome"
 *           local-extends="javax.ejb.EJBLocalHome"
 *           
 * @jboss:query signature="Collection ejbSelectGeneric(java.lang.String jbossQl, java.lang.Object[] args)"
 *                         dynamic="true"          
 *
 * @ejb:finder signature="LocalInvBuildOrderLine findPostedBorByIiNameAndLocNameAndBorDocumentNumber(java.lang.String BOL_IL_II_NM, java.lang.String BOL_IL_LOC_NM, java.lang.String BOL_BOR_DCMNT_NMBR, java.lang.Integer BOL_AD_CMPNY)"
 *             query="SELECT OBJECT(bol) FROM InvBuildOrderLine bol WHERE bol.invItemLocation.invItem.iiName = ?1 AND bol.invItemLocation.invLocation.locName = ?2 AND bol.invBuildOrder.borDocumentNumber = ?3 AND bol.bolAdCompany = ?4 AND bol.invBuildOrder.borPosted = 1 AND bol.invBuildOrder.borVoid = 0"
 *
 * @ejb:finder signature="Collection findCompleteBor(java.lang.Integer BOL_AD_BRNCH, java.lang.Integer BOL_AD_CMPNY)"
 *             query="SELECT OBJECT(bol) FROM InvBuildOrderLine bol WHERE bol.invBuildOrder.borPosted = 1 AND bol.invBuildOrder.borVoid = 0 AND bol.bolLock = 0 AND bol.bolQuantityAvailable > 0 AND bol.invBuildOrder.borAdBranch = ?1 AND bol.bolAdCompany = ?2"
 * 
 * @jboss:query signature="Collection findCompleteBor(java.lang.Integer BOL_AD_BRNCH, java.lang.Integer BOL_AD_CMPNY)"
 *             query="SELECT OBJECT(bol) FROM InvBuildOrderLine bol WHERE bol.invBuildOrder.borPosted = 1 AND bol.invBuildOrder.borVoid = 0 AND bol.bolLock = 0 AND bol.bolQuantityAvailable > 0 AND bol.invBuildOrder.borAdBranch = ?1 AND bol.bolAdCompany = ?2 ORDER BY bol.invBuildOrder.borDate"
 * 
 *
 * @ejb:value-object match="*"
 *             name="InvBuildOrderLine"
 *
 * @jboss:persistence table-name="INV_BLD_ORDR_LN"
 *
 * @jboss:entity-command name="mysql-get-generated-keys"
 *
 */

public abstract class InvBuildOrderLineBean extends AbstractEntityBean {
	
//	 Access methods for persistent fields

	   /**
	    * @ejb:persistent-field
	    * @ejb:interface-method view-type="local"
	    * @ejb:pk-field
	    *
	    * @jboss:column-name name="BOL_CODE"
	    *
	    * @jboss:persistence auto-increment="true"
	    **/
	   public abstract java.lang.Integer getBolCode();
	   public abstract void setBolCode(java.lang.Integer BOL_CODE);

	   /**
	    * @ejb:persistent-field
	    * @ejb:interface-method view-type="local"
	    *
	    * @jboss:column-name name="BOL_QTY_RQRD"
	    **/
	   public abstract double getBolQuantityRequired();
	   /**
	    * @ejb:interface-method view-type="local"
	    **/
	   public abstract void setBolQuantityRequired(double BOL_QTY_RQRD);

	   /**
	    * @ejb:persistent-field
	    * @ejb:interface-method view-type="local"
	    *
	    * @jboss:column-name name="BOL_QTY_ASSMBLD"
	    **/
	   public abstract double getBolQuantityAssembled();
	   /**
	    * @ejb:interface-method view-type="local"
	    **/
	   public abstract void setBolQuantityAssembled(double BOL_QTY_ASSMBLD);
	   
	   /**
	    * @ejb:persistent-field
	    * @ejb:interface-method view-type="local"
	    *
	    * @jboss:column-name name="BOL_QTY_AVLBL"
	    **/
	   public abstract double getBolQuantityAvailable();
	   /**
	    * @ejb:interface-method view-type="local"
	    **/
	   public abstract void setBolQuantityAvailable(double BOL_QTY_AVLBL);
	   
	   /**
	    * @ejb:persistent-field
	    * @ejb:interface-method view-type="local"
	    *
	    * @jboss:column-name name="BOL_LCK"
	    **/
	   public abstract double getBolLock();
	   /**
	    * @ejb:interface-method view-type="local"
	    **/
	   public abstract void setBolLock(double BOL_LCK);
	   
	   /**
	    * @ejb:persistent-field
	    * @ejb:interface-method view-type="local"
	    *
	    * @jboss:column-name name="BOL_EXPRY_DT"
	    **/
	   public abstract String getBolMisc();
	   /**
	    * @ejb:interface-method view-type="local"
	    **/
	   public abstract void setBolMisc(String BOL_EXPRY_DT);
	   
	   /**
	    * @ejb:persistent-field
	    * @ejb:interface-method view-type="local"
	    *
	    * @jboss:column-name name="BOL_AD_CMPNY"
	    **/
	   public abstract Integer getBolAdCompany();
	   /**
	    * @ejb:interface-method view-type="local"
	    **/
	   public abstract void setBolAdCompany(Integer BOL_AD_CMPNY);
	   
	   // Access methods for relationship fields

	   /**
	    * @ejb:interface-method view-type="local"
	    * @ejb:relation name="salesorderline-buildorderlines"
	    *               role-name="buildorderlines-has-one-salesorderline"
	    *               cascade-delete="yes"
	    *
	    * @jboss:relation related-pk-field="solCode"
	    *                 fk-column="AR_SALES_ORDER_LINE"
	    */
	   public abstract LocalArSalesOrderLine getArSalesOrderLine();
	   /**
	    * @ejb:interface-method view-type="local"
	    **/
	   public abstract void setArSalesOrderLine(LocalArSalesOrderLine arSalesOrderLine);
	   
	   /**
	    * @ejb:interface-method view-type="local"
	    * @ejb:relation name="joborderline-buildorderlines"
	    *               role-name="buildorderlines-has-one-joborderline"
	    *               cascade-delete="yes"
	    *
	    * @jboss:relation related-pk-field="jolCode"
	    *                 fk-column="AR_JOB_ORDER_LINE"
	    */
	   public abstract LocalArJobOrderLine getArJobOrderLine();
	   /**
	    * @ejb:interface-method view-type="local"
	    **/
	   public abstract void setArJobOrderLine(LocalArJobOrderLine arJobOrderLine);

	   /**
	    * @ejb:interface-method view-type="local"
	    * @ejb:relation name="buildorder-buildordelines"
	    *               role-name="buildordelines-has-one-buildorder"
	    *               cascade-delete="yes"
	    *
	    * @jboss:relation related-pk-field="borCode"
	    *                 fk-column="INV_BUILD_ORDER"
	    */
	   public abstract LocalInvBuildOrder getInvBuildOrder();
	   public abstract void setInvBuildOrder(LocalInvBuildOrder invBuildOrder);
	   
	    /**
		  * @ejb:interface-method view-type="local"
		  * @ejb:relation name="buildordeline-buildorderstocks"
		  *               role-name="buildorderline-has-many-buildorderstocks"
		  */
	   public abstract Collection getInvBuildOrderStocks();
	   public abstract void setInvBuildOrderStocks(Collection invBuildOrderStocks);
	   
	   
	   /**
	    * @ejb:interface-method view-type="local"
	    * @ejb:relation name="itemlocation-buildorderlines"
	    *               role-name="buildorderline-has-one-itemlocation"
	    *               cascade-delete="no"
	    *
	    * @jboss:relation related-pk-field="ilCode"
	    *                 fk-column="INV_ITEM_LOCATION"
	    */
	   public abstract LocalInvItemLocation getInvItemLocation();
	   public abstract void setInvItemLocation(LocalInvItemLocation invItemLocation);	  
	   
	   /**
		  * @ejb:interface-method view-type="local"
		  * @ejb:relation name="buildorderline-assemblytransferlines"
		  *               role-name="buildorderline-has-many-assemblytransferlines"
		  */
	   public abstract Collection getInvAssemblyTransferLines();
	   public abstract void setInvAssemblyTransferLines(Collection invAssemblyTransferLines);
	   

	   /**
	 	 * @jboss:dynamic-ql
	 	 */
	 	public abstract Collection ejbSelectGeneric(java.lang.String jbossQl, java.lang.Object[] args)
	 	throws FinderException;     
	 	
	 	// BUSINESS METHODS
	 	
	 	/**
	 	 * @ejb:home-method view-type="local"
	 	 */
	 	public Collection ejbHomeGetIncompleteBolByCriteria(java.lang.String jbossQl, java.lang.Object[] args)
	 	throws FinderException {
	 		
	 		return ejbSelectGeneric(jbossQl, args);
	 		
	 	}	   
	   
	   /**
	    * @ejb:interface-method view-type="local"
	    **/
	   public void addInvBuildOrderStock(LocalInvBuildOrderStock invBuildOrderStock) {

	      Debug.print("InvBuildOrderLineBean addInvBuildOrderStock");
	      try {
	         Collection invBuildOrderStocks = getInvBuildOrderStocks();
	         invBuildOrderStocks.add(invBuildOrderStock);
	      } catch (Exception ex) {
	         throw new EJBException(ex.getMessage());
	      }
	   }

	   /**
	    * @ejb:interface-method view-type="local"
	    **/
	   public void dropInvBuildOrderStock(LocalInvBuildOrderStock invBuildOrderStock) {
	   
	      Debug.print("InvBuildOrderLineBean dropInvBuildOrderStock");
	      try {
	      	Collection invBuildOrderStocks = getInvBuildOrderStocks();
	      	invBuildOrderStocks.remove(invBuildOrderStock);
	      } catch (Exception ex) {
	         throw new EJBException(ex.getMessage());
	      }
	   }
	   
	   /**
	    * @ejb:interface-method view-type="local"
	    **/
	   public void addInvAssemblyTransferLine(LocalInvAssemblyTransferLine invAssemblyTransferLine) {

	      Debug.print("InvBuildOrderLineBean addInvAssemblyTransferLine");
	      try {
	         Collection invAssemblyTransferLines = getInvAssemblyTransferLines();
	         invAssemblyTransferLines.add(invAssemblyTransferLine);
	      } catch (Exception ex) {
	         throw new EJBException(ex.getMessage());
	      }
	   }

	   /**
	    * @ejb:interface-method view-type="local"
	    **/
	   public void dropInvAssemblyTransferLine(LocalInvAssemblyTransferLine invAssemblyTransferLine) {
	   
	      Debug.print("InvBuildOrderLineBean dropInvAssemblyTransferLine");
	      try {
	      	Collection invAssemblyTransferLines = getInvAssemblyTransferLines();
	      	invAssemblyTransferLines.remove(invAssemblyTransferLine);
	      } catch (Exception ex) {
	         throw new EJBException(ex.getMessage());
	      }
	   }
	   
	   // EntityBean methods

	   /**
	    * @ejb:create-method view-type="local"
	    **/
	   public java.lang.Integer ejbCreate (java.lang.Integer BOL_CODE, double BOL_QTY_RQRD,
	   	  double BOL_QTY_ASSMBLD, double BOL_QTY_AVLBL, Integer BOL_AD_CMPNY)
	      throws CreateException {

	      Debug.print("invBuildOrderLineBean ejbCreate");
	      
	      setBolCode(BOL_CODE);
	      setBolQuantityRequired(BOL_QTY_RQRD);
	      setBolQuantityAssembled(BOL_QTY_ASSMBLD);
	      setBolQuantityAvailable(BOL_QTY_AVLBL);
	      setBolAdCompany(BOL_AD_CMPNY);

	      return null;
	   }

	   /**
	    * @ejb:create-method view-type="local"
	    **/
	   public java.lang.Integer ejbCreate (double BOL_QTY_RQRD,
		   	  double BOL_QTY_ASSMBLD, double BOL_QTY_AVLBL, Integer BOL_AD_CMPNY)
	      throws CreateException {

	      Debug.print("invBuildOrderLineBean ejbCreate");
	      
	      setBolQuantityRequired(BOL_QTY_RQRD);
	      setBolQuantityAssembled(BOL_QTY_ASSMBLD);
	      setBolQuantityAvailable(BOL_QTY_AVLBL);
	      setBolAdCompany(BOL_AD_CMPNY);
	      return null;
	   }

	   public void ejbPostCreate (java.lang.Integer BOL_CODE, double BOL_QTY_RQRD,
		   	  double BOL_QTY_ASSMBLD, double BOL_QTY_AVLBL, Integer BOL_AD_CMPNY)
	      throws CreateException { }

	   public void ejbPostCreate (double BOL_QTY_RQRD,
		   	  double BOL_QTY_ASSMBLD, double BOL_QTY_AVLBL, Integer BOL_AD_CMPNY)
	      throws CreateException { }
	
}
