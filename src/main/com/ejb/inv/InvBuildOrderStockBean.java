package com.ejb.inv;

import java.util.Collection;

import javax.ejb.CreateException;
import javax.ejb.EJBException;

import com.util.AbstractEntityBean;
import com.util.Debug;

/**
 * @ejb:bean name="InvBuildOrderStockEJB"
 *           display-name="Build Order Stock Entity"
 *           type="CMP"
 *           cmp-version="2.x"
 *           view-type="local"
 *           local-jndi-name="omega-ejb/InvBuildOrderStockEJB"
 *           schema="InvBuildOrderStock"
 *           primkey-field="bosCode"
 *       
 * @ejb:pk class="java.lang.Integer"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="invuser"
 *                        role-link="invuserlink"
 *
 * @ejb:permission role-name="invuser"
 *
 * @ejb:interface local-class="com.ejb.inv.LocalInvBuildOrderStock"
 *                local-extends="javax.ejb.EJBLocalObject"
 *
 * @ejb:home local-class="com.ejb.inv.LocalInvBuildOrderStockHome"
 *           local-extends="javax.ejb.EJBLocalHome"
 *           
 * @ejb:finder signature="Collection findIncompleteBosByBolCode(java.lang.Integer BOL_CODE, java.lang.Integer SI_AD_CMPNY)"
 *             query="SELECT OBJECT(bos) FROM InvBuildOrderStock bos WHERE bos.invBuildOrderLine.bolCode = ?1 AND bos.bosQuantityRequired > bos.bosQuantityIssued AND bos.bosLock = 0 AND bos.bosAdCompany = ?2"
 *
 * @ejb:value-object match="*"
 *             name="InvBuildOrderStock"
 *
 * @jboss:persistence table-name="INV_BLD_ORDR_STCK"
 *
 * @jboss:entity-command name="mysql-get-generated-keys"
 *
 */

public abstract class InvBuildOrderStockBean extends AbstractEntityBean {
	
//	 Access methods for persistent fields

	   /**
	    * @ejb:persistent-field
	    * @ejb:interface-method view-type="local"
	    * @ejb:pk-field
	    *
	    * @jboss:column-name name="BOS_CODE"
	    *
	    * @jboss:persistence auto-increment="true"
	    **/
	   public abstract java.lang.Integer getBosCode();
	   public abstract void setBosCode(java.lang.Integer BOS_CODE);

	   /**
	    * @ejb:persistent-field
	    * @ejb:interface-method view-type="local"
	    *
	    * @jboss:column-name name="BOS_QTY_RQRD"
	    **/
	   public abstract double getBosQuantityRequired();
	   /**
	    * @ejb:interface-method view-type="local"
	    **/
	   public abstract void setBosQuantityRequired(double BOS_QTY_RQRD);

	   /**
	    * @ejb:persistent-field
	    * @ejb:interface-method view-type="local"
	    *
	    * @jboss:column-name name="BOS_QTY_ISSUED"
	    **/
	   public abstract double getBosQuantityIssued();
	   /**
	    * @ejb:interface-method view-type="local"
	    **/
	   public abstract void setBosQuantityIssued(double BOS_QTY_ISSUED);
	   
	   
	   /**
	    * @ejb:persistent-field
	    * @ejb:interface-method view-type="local"
	    *
	    * @jboss:column-name name="BOS_LCK"
	    **/
	   public abstract byte getBosLock();
	   /**
	    * @ejb:interface-method view-type="local"
	    **/
	   public abstract void setBosLock(byte BOS_LCK);
	   
	   /**
	    * @ejb:persistent-field
	    * @ejb:interface-method view-type="local"
	    *
	    * @jboss:column-name name="BOS_AD_CMPNY"
	    **/
	   public abstract Integer getBosAdCompany();
	   /**
	    * @ejb:interface-method view-type="local"
	    **/
	   public abstract void setBosAdCompany(Integer BOS_AD_CMPNY);
	   
	   // Access methods for relationship fields

	   /**
	    * @ejb:interface-method view-type="local"
	    * @ejb:relation name="buildordeline-buildorderstocks"
	    *               role-name="buildorderstocks-has-one-buildordeline"
	    *               cascade-delete="yes"
	    *
	    * @jboss:relation related-pk-field="bolCode"
	    *                 fk-column="INV_BUILD_ORDER_LINE"
	    */
	   public abstract LocalInvBuildOrderLine getInvBuildOrderLine();
	   public abstract void setInvBuildOrderLine(LocalInvBuildOrderLine invBuildOrderLine);
	   
	   
	   /**
	    * @ejb:interface-method view-type="local"
	    * @ejb:relation name="item-buildorderstocks"
	    *               role-name="buildorderstock-has-one-item"
	    *               cascade-delete="no"
	    *
	    * @jboss:relation related-pk-field="iiCode"
	    *                 fk-column="INV_ITEM"
	    */
	   public abstract LocalInvItem getInvItem();
	   public abstract void setInvItem(LocalInvItem invItem);
	   
	   /**
	    * @ejb:interface-method view-type="local"
	    * @ejb:relation name="buildorderstock-stockissuancelines"
	    *               role-name="buildorderstock-has-many-stockissuancelines"
	    */
	   public abstract Collection getInvStockIssuanceLines();
	   public abstract void setInvStockIssuanceLines(Collection invStockIssuanceLines);
	   

	   // Business methods	   
	   
	   /**
	    * @ejb:interface-method view-type="local"
	    **/
	   public void addInvStockIssuanceLine(LocalInvStockIssuanceLine invStockIssuanceLine) {

	      Debug.print("InvBuildOrderStockBean addInvStockIssuanceLine");
	      try {
	         Collection invStockIssuanceLines = getInvStockIssuanceLines();
	         invStockIssuanceLines.add(invStockIssuanceLine);
	      } catch (Exception ex) {
	         throw new EJBException(ex.getMessage());
	      }
	   }

	   /**
	    * @ejb:interface-method view-type="local"
	    **/
	   public void dropInvStockIssuanceLine(LocalInvStockIssuanceLine invStockIssuanceLine) {
	   
	      Debug.print("InvBuildOrderStockBean dropInvStockIssuanceLine");
	      try {
	      	Collection invStockIssuanceLines = getInvStockIssuanceLines();
	      	invStockIssuanceLines.remove(invStockIssuanceLine);
	      } catch (Exception ex) {
	         throw new EJBException(ex.getMessage());
	      }
	   }
	   // EntityBean methods

	   /**
	    * @ejb:create-method view-type="local"
	    **/
	   public java.lang.Integer ejbCreate (java.lang.Integer BOS_CODE, double BOS_QTY_RQRD,
	   	  double BOS_QTY_ISSUED, byte BOS_LCK, Integer BOS_AD_CMPNY)
	      throws CreateException {

	      Debug.print("invBuildOrderStockBean ejbCreate");
	      
	      setBosCode(BOS_CODE);
	      setBosQuantityRequired(BOS_QTY_RQRD);
	      setBosQuantityIssued(BOS_QTY_ISSUED);
	      setBosLock(BOS_LCK);
	      setBosAdCompany(BOS_AD_CMPNY);

	      return null;
	   }

	   /**
	    * @ejb:create-method view-type="local"
	    **/
	   public java.lang.Integer ejbCreate (double BOS_QTY_RQRD, 
		   	  double BOS_QTY_ISSUED, byte BOS_LCK, Integer BOS_AD_CMPNY)
	      throws CreateException {

	      Debug.print("invBuildOrderStockBean ejbCreate");
	      
	      setBosQuantityRequired(BOS_QTY_RQRD);
	      setBosQuantityIssued(BOS_QTY_ISSUED);
	      setBosLock(BOS_LCK);
	      setBosAdCompany(BOS_AD_CMPNY);
	      return null;
	   }

	   public void ejbPostCreate (java.lang.Integer BOS_CODE, double BOS_QTY_RQRD, 
		   	  double BOS_QTY_ISSUED, byte BOS_LCK, Integer BOS_AD_CMPNY)
	      throws CreateException { }

	   public void ejbPostCreate (double BOS_QTY_RQRD,
		   	  double BOS_QTY_ISSUED, byte BOS_LCK, Integer BOS_AD_CMPNY)
	      throws CreateException { }
	
}
