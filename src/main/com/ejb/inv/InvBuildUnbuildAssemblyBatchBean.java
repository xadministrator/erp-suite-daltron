/*
 * com/ejb/ar/LocalInvBuildUnbuildAssemblyBatchBean.java
 *
 * Created on May 20, 2004, 10:28 AM
 */
package com.ejb.inv;

import java.util.Collection;
import java.util.Date;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;

import com.util.AbstractEntityBean;
import com.util.Debug;

/**
 *
 * @author  Clint L. Arrogante
 *
 * @ejb:bean name="InvBuildUnbuildAssemblyBatchEJB"
 *           display-name="InvBuildUnbuildAssembly Batch Entity"
 *           type="CMP"
 *           cmp-version="2.x"
 *           view-type="local"
 *           local-jndi-name="omega-ejb/InvBuildUnbuildAssemblyBatchEJB"
 *           schema="InvBuildUnbuildAssemblyBatch"
 *           primkey-field="bbCode"
 *       
 * @ejb:pk class="Integer"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="invuser"
 *                        role-link="invuserlink"
 *
 * @ejb:permission role-name="invuser"
 *
 * @ejb:interface local-class="com.ejb.inv.LocalInvBuildUnbuildAssemblyBatch"
 *                local-extends="javax.ejb.EJBLocalObject"
 *
 * @ejb:home local-class="com.ejb.inv.LocalInvBuildUnbuildAssemblyBatchHome"
 *           local-extends="javax.ejb.EJBLocalHome"
 *
 * @ejb:finder signature="LocalInvBuildUnbuildAssemblyBatch findByBbName(java.lang.String BB_NM, java.lang.Integer BB_AD_BRNCH, java.lang.Integer BB_AD_CMPNY)"
 *             query="SELECT OBJECT(bb) FROM InvBuildUnbuildAssemblyBatch bb WHERE bb.bbName=?1 AND bb.bbAdBranch = ?2 AND bb.bbAdCompany=?3"
 * 
 * @ejb:finder signature="Collection findOpenBbByBbType(java.lang.String BB_TYP, java.lang.Integer BB_AD_BRNCH, java.lang.Integer BB_AD_CMPNY)"
 *             query="SELECT OBJECT(bb) FROM InvBuildUnbuildAssemblyBatch bb WHERE bb.bbType=?1 AND bb.bbStatus='OPEN' AND bb.bbAdBranch=?2 AND bb.bbAdCompany=?3"
 *
 * @jboss:query signature="Collection findOpenBbByBbType(java.lang.String BB_TYP, java.lang.Integer BB_AD_BRNCH, java.lang.Integer BB_AD_CMPNY)"
 *             query="SELECT OBJECT(bb) FROM InvBuildUnbuildAssemblyBatch bb WHERE bb.bbType=?1 AND bb.bbStatus='OPEN' AND bb.bbAdBranch=?2 AND bb.bbAdCompany=?3 ORDER BY bb.bbName"
 *
 * @ejb:finder signature="Collection findOpenBbAll(java.lang.Integer AD_BRNCH, java.lang.Integer BB_AD_CMPNY)"
 *             query="SELECT OBJECT(bb) FROM InvBuildUnbuildAssemblyBatch bb WHERE bb.bbStatus='OPEN' AND bb.bbAdBranch=?1 AND bb.bbAdCompany=?2"
 * 
 * @jboss:query signature="Collection findOpenBbAll(java.lang.Integer AD_BRNCH, java.lang.Integer BB_AD_CMPNY)"
 *             query="SELECT OBJECT(bb) FROM InvBuildUnbuildAssemblyBatch bb WHERE bb.bbStatus='OPEN' AND bb.bbAdBranch=?1 AND bb.bbAdCompany=?2 ORDER BY bb.bbName"
 *
 * @ejb:select signature="Collection ejbSelectGeneric(java.lang.String jbossQl, java.lang.Object[] args)"
 *             result-type-mapping="Local"
 *             method-intf="LocalHome"
 *             query="SELECT OBJECT(bb) FROM InvBuildUnbuildAssemblyBatch bb"
 *
 * @jboss:query signature="Collection ejbSelectGeneric(java.lang.String jbossQl, java.lang.Object[] args)"
 *                         dynamic="true"
 *
 * @ejb:value-object match="*"
 *             name="InvBuildUnbuildAssemblyBatch"
 *
 * @jboss:persistence table-name="INV_BUA_BTCH"
 *
 * @jboss:entity-command name="mysql-get-generated-keys"
 *
 */

public abstract class InvBuildUnbuildAssemblyBatchBean extends AbstractEntityBean {


   // Access methods for persistent fields

   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    * @ejb:pk-field
    *
    * @jboss:column-name name="BB_CODE"
    *
    * @jboss:persistence auto-increment="true"
    **/
   public abstract Integer getBbCode();
   public abstract void setBbCode(Integer BB_CODE);

   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="BB_NM"
    **/
   public abstract String getBbName();
   /**
    * @ejb:interface-method view-type="local"
    **/
   public abstract void setBbName(String BB_NM);

   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="BB_DESC"
    **/
   public abstract String getBbDescription();
   /**
    * @ejb:interface-method view-type="local"
    **/
   public abstract void setBbDescription(String BB_DESC);
   
   
   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="BB_STATUS"
    **/
   public abstract String getBbStatus();
   /**
    * @ejb:interface-method view-type="local"
    **/
   public abstract void setBbStatus(String BB_STATUS);
   
   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="BB_TYP"
    **/
   public abstract String getBbType();
   /**
    * @ejb:interface-method view-type="local"
    **/
   public abstract void setBbType(String BB_TYP);

   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="BB_DT_CRTD"
    **/
   public abstract Date getBbDateCreated();
   /**
    * @ejb:interface-method view-type="local"
    **/
   public abstract void setBbDateCreated(Date BB_DT_CRTD);
   
   
   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="BB_CRTD_BY"
    **/
   public abstract String getBbCreatedBy();
   /**
    * @ejb:interface-method view-type="local"
    **/
   public abstract void setBbCreatedBy(String BB_CRTD_BY);
   
   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="BB_AD_BRNCH"
    **/
   public abstract Integer getBbAdBranch();
   /**
    * @ejb:interface-method view-type="local"
    **/
   public abstract void setBbAdBranch(Integer BB_AD_BRNCH);   
   
   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="BB_AD_CMPNY"
    **/
   public abstract Integer getBbAdCompany();
   /**
    * @ejb:interface-method view-type="local"
    **/
   public abstract void setBbAdCompany(Integer BB_AD_CMPNY);
   
   // Access methods for relationship fields

   /**
    * @ejb:interface-method view-type="local"
    * @ejb:relation name="buildunbuildassemblybatch-buildunbuildassemblies"
    *               role-name="buildunbuildassemblybatch-has-many-buildunbuildassemblies"
    */
   public abstract Collection getInvBuildUnbuildAssemblies();
   public abstract void setInvBuildUnbuildAssemblies(Collection invBuildUnbuildAssemblies);
   
  
  /**
    * @jboss:dynamic-ql
    */
    public abstract Collection ejbSelectGeneric(java.lang.String jbossQl, java.lang.Object[] args)
       throws FinderException;

   // Business methods
   
   /**
    * @ejb:home-method view-type="local"
    */
    public Collection ejbHomeGetBbByCriteria(java.lang.String jbossQl, java.lang.Object[] args)
       throws FinderException {
       	
       return ejbSelectGeneric(jbossQl, args);
    }
      
   
   /**
    * @ejb:interface-method view-type="local"
    **/
   public void addInvBuildUnbuildAssembly(LocalInvBuildUnbuildAssembly InvBuildUnbuildAssembly) {

      Debug.print("InvBuildUnbuildAssemblyBatchBean addInvBuildUnbuildAssembly");
      try {
         Collection invBuildUnbuildAssemblies = getInvBuildUnbuildAssemblies();
         invBuildUnbuildAssemblies.add(InvBuildUnbuildAssembly);
	 
      } catch (Exception ex) {
         throw new EJBException(ex.getMessage());
      }
   }

   /**
    * @ejb:interface-method view-type="local"
    **/
   public void dropInvBuildUnbuildAssembly(LocalInvBuildUnbuildAssembly InvBuildUnbuildAssembly) {

      Debug.print("InvBuildUnbuildAssemblyBatchBean dropArInvoice");
      try {
    	  
    	  Collection invBuildUnbuildAssemblies = getInvBuildUnbuildAssemblies();
          invBuildUnbuildAssemblies.add(InvBuildUnbuildAssembly);
 	 
      } catch (Exception ex) {
         throw new EJBException(ex.getMessage());
      }
   }
   
   // EntityBean methods

   /**
    * @ejb:create-method view-type="local"
    **/
   public Integer ejbCreate (Integer BB_CODE, String BB_NM, String BB_DESC,
      String BB_STATUS, String BB_TYP, Date BB_DT_CRTD, String BB_CRTD_BY,
	  Integer BB_AD_BRNCH, Integer BB_AD_CMPNY)
      throws CreateException {

      Debug.print("InvBuildUnbuildAssemblyBatchBean ejbCreate");
      setBbCode(BB_CODE);
      setBbName(BB_NM);
      setBbDescription(BB_DESC);
      setBbStatus(BB_STATUS);
      setBbType(BB_TYP);
      setBbDateCreated(BB_DT_CRTD);
      setBbCreatedBy(BB_CRTD_BY);
      setBbAdBranch(BB_AD_BRNCH);
      setBbAdCompany(BB_AD_CMPNY);
      
      return null;
   }

   /**
    * @ejb:create-method view-type="local"
    **/
   public Integer ejbCreate (String BB_NM, String BB_DESC,
      String BB_STATUS, String BB_TYP, Date BB_DT_CRTD, String BB_CRTD_BY,
	  Integer BB_AD_BRNCH, Integer BB_AD_CMPNY)
      throws CreateException {

      Debug.print("InvBuildUnbuildAssemblyBatchBean ejbCreate");

      setBbName(BB_NM);
      setBbDescription(BB_DESC);
      setBbStatus(BB_STATUS);
      setBbType(BB_TYP);
      setBbDateCreated(BB_DT_CRTD);
      setBbCreatedBy(BB_CRTD_BY);
      setBbAdBranch(BB_AD_BRNCH);
      setBbAdCompany(BB_AD_CMPNY);
      
      return null;
   }

   public void ejbPostCreate (Integer BB_CODE, String BB_NM, String BB_DESC,
      String BB_STATUS, String BB_TYP, Date BB_DT_CRTD, String BB_CRTD_BY,
	  Integer BB_AD_BRNCH, Integer BB_AD_CMPNY)
      throws CreateException { }

   public void ejbPostCreate (String BB_NM, String BB_DESC,
      String BB_STATUS, String BB_TYP, Date BB_DT_CRTD, String BB_CRTD_BY,
	  Integer BB_AD_BRNCH, Integer BB_AD_CMPNY)
      throws CreateException { }

} // InvBuildUnbuildAssemblyBatchBean class
