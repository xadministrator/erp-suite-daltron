/*
 * com/ejb/inv/LocalInvBuildUnbuildAssemblyBean.java
 *
 * Created on May 20, 2004 04:35 PM
 */

package com.ejb.inv;


import java.util.Collection;
import java.util.Date;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;

import com.ejb.ar.LocalArCustomer;
import com.util.AbstractEntityBean;
import com.util.Debug;

/**
 *
 * @author  Enrico C. Yap
 *
 * @ejb:bean name="InvBuildUnbuildAssemblyEJB"
 *           display-name="Build Unbuild Assembly Entity"
 *           type="CMP"
 *           cmp-version="2.x"
 *           view-type="local"
 *           local-jndi-name="omega-ejb/InvBuildUnbuildAssemblyEJB"
 *           schema="InvBuildUnbuildAssembly"
 *           primkey-field="buaCode"
 *
 * @ejb:pk class="java.lang.Integer"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="invuser"
 *                        role-link="invuserlink"
 *
 * @ejb:permission role-name="invuser"
 *
 * @ejb:interface local-class="com.ejb.inv.LocalInvBuildUnbuildAssembly"
 *                local-extends="javax.ejb.EJBLocalObject"
 *
 * @ejb:home local-class="com.ejb.inv.LocalInvBuildUnbuildAssemblyHome"
 *           local-extends="javax.ejb.EJBLocalHome"
 * 
 * @ejb:select signature="Collection ejbSelectGeneric(java.lang.String jbossQl, java.lang.Object[] args)"
 *             result-type-mapping="Local"
 *             method-intf="LocalHome"
 *             query="SELECT OBJECT(bua) FROM InvBuildUnbuildAssembly bua"
 *
 * @jboss:query signature="Collection ejbSelectGeneric(java.lang.String jbossQl, java.lang.Object[] args)"
 *                         dynamic="true"
 * 
 * @ejb:finder signature="Collection findPostedBuaByBuaDateRange(java.util.Date BUA_DT_FRM, java.util.Date BUA_DT_TO, java.lang.Integer BUA_AD_CMPNY)"
 *             query="SELECT OBJECT(bua) FROM InvBuildUnbuildAssembly bua WHERE bua.buaPosted = 1 AND bua.buaDate >= ?1 AND bua.buaDate <= ?2 AND bua.buaAdCompany = ?3"
 *
 * @jboss:query signature="Collection findPostedBuaByBuaDateRange(java.util.Date BUA_DT_FRM, java.util.Date BUA_DT_TO, java.lang.Integer BUA_AD_CMPNY)"
 *             query="SELECT OBJECT(bua) FROM InvBuildUnbuildAssembly bua WHERE bua.buaPosted = 1 AND bua.buaDate >= ?1 AND bua.buaDate <= ?2 AND bua.buaAdCompany = ?3 ORDER BY bua.buaDate"
 *
 * @ejb:finder signature="Collection findDraftBuaAll(java.lang.Integer BUA_AD_CMPNY)"
 *             query="SELECT OBJECT(bua) FROM InvBuildUnbuildAssembly bua WHERE bua.buaApprovalStatus IS NULL AND bua.buaAdCompany = ?1"
 * 
 * @ejb:finder signature="Collection findDraftBuaByBrCode(java.lang.Integer BUA_AD_BRNCH, java.lang.Integer BUA_AD_CMPNY)"
 *             query="SELECT OBJECT(bua) FROM InvBuildUnbuildAssembly bua WHERE bua.buaApprovalStatus IS NULL AND bua.buaReceiving = 1 AND bua.buaAdBranch = ?1 AND bua.buaAdCompany = ?2"
 *
 * @ejb:finder signature="Collection findDraftOrderBuaByBrCode(java.lang.Integer BUA_AD_BRNCH, java.lang.Integer BUA_AD_CMPNY)"
 *             query="SELECT OBJECT(bua) FROM InvBuildUnbuildAssembly bua WHERE bua.buaApprovalStatus IS NULL AND bua.buaReceiving = 0 AND bua.buaAdBranch = ?1 AND bua.buaAdCompany = ?2"
 *
 * @ejb:finder signature="Collection findUnpostedBuaByBuaDateRange(java.util.Date BUA_DT_FRM, java.util.Date BUA_DT_TO, java.lang.Integer BUA_AD_CMPNY)"
 *             query="SELECT OBJECT(bua) FROM InvBuildUnbuildAssembly bua  WHERE bua.buaPosted = 0 AND bua.buaVoid = 0 AND bua.buaDate >= ?1 AND bua.buaDate <= ?2 AND bua.buaAdCompany = ?3"
 *
 * @jboss:query signature="Collection findUnpostedBuaByBuaDateRange(java.util.Date BUA_DT_FRM, java.util.Date BUA_DT_TO, java.lang.Integer BUA_AD_CMPNY)"
 *             query="SELECT OBJECT(bua) FROM InvBuildUnbuildAssembly bua  WHERE bua.buaPosted = 0 AND bua.buaVoid = 0 AND bua.buaDate >= ?1 AND bua.buaDate <= ?2 AND bua.buaAdCompany = ?3 ORDER BY bua.buaDate"
 *
 * @ejb:finder signature="Collection findUnpostedBuaByBuaDueDateRange(java.util.Date BUA_DUE_DT_FRM, java.util.Date BUA_DUE_DT_TO, java.lang.Integer BUA_AD_CMPNY)"
 *             query="SELECT OBJECT(bua) FROM InvBuildUnbuildAssembly bua  WHERE bua.buaPosted = 0 AND bua.buaReceiving = 0 AND bua.buaVoid = 0 AND bua.buaDueDate >= ?1 AND bua.buaDueDate <= ?2 AND bua.buaAdCompany = ?3"
 *
 * @jboss:query signature="Collection findUnpostedBuaByBuaDueDateRange(java.util.Date BUA_DUE_DT_FRM, java.util.Date BUA_DUE_DT_TO, java.lang.Integer BUA_AD_CMPNY)"
 *             query="SELECT OBJECT(bua) FROM InvBuildUnbuildAssembly bua  WHERE bua.buaPosted = 0 AND bua.buaReceiving = 0 AND bua.buaVoid = 0 AND bua.buaDueDate >= ?1 AND bua.buaDueDate <= ?2 AND bua.buaAdCompany = ?3 ORDER BY bua.buaDate"
 * 
 * @ejb:finder signature="LocalInvBuildUnbuildAssembly findByBuaRcvBuaNumberAndBuaReceivingAndCstCustomerCodeAndBrCode(java.lang.String BUA_RCV_PO_NMBR, byte BUA_RCVNG, java.lang.String CST_CSTMR_CODE, java.lang.Integer BUA_AD_BRNCH, java.lang.Integer BUA_AD_CMPNY)"
 *             query="SELECT OBJECT(bua) FROM InvBuildUnbuildAssembly bua WHERE bua.buaDocumentNumber = ?1 AND bua.buaReceiving = ?2 AND bua.arCustomer.cstCustomerCode = ?3 AND bua.buaAdBranch = ?4 AND bua.buaAdCompany = ?5"
 *                        
 * @ejb:finder signature="LocalInvBuildUnbuildAssembly findByBuaDocumentNumberAndBrCode(java.lang.String BUA_DCMNT_NMBR, java.lang.Integer BUA_AD_BRNCH, java.lang.Integer BUA_AD_CMPNY)"
 *             query="SELECT OBJECT(bua) FROM InvBuildUnbuildAssembly bua  WHERE bua.buaDocumentNumber = ?1 AND bua.buaAdBranch = ?2 AND bua.buaAdCompany = ?3"
 *                        
 * @ejb:finder signature="LocalInvBuildUnbuildAssembly findByBuaDocumentNumberAndBuaReceivingBrCode(java.lang.String BUA_DCMNT_NMBR, byte BUA_RCVNG, java.lang.Integer BUA_AD_BRNCH, java.lang.Integer BUA_AD_CMPNY)"
 *             query="SELECT OBJECT(bua) FROM InvBuildUnbuildAssembly bua  WHERE bua.buaDocumentNumber = ?1 AND bua.buaReceiving = ?2 AND bua.buaAdBranch = ?3 AND bua.buaAdCompany = ?3"
 *
 * @ejb:finder signature="Collection findByBuaRcvBuaNumberAndCstCustomerCodeAndBrCode(java.lang.String BUA_RCV_PO_NMBR, java.lang.String CST_CSTMR_CODE, java.lang.Integer BUA_AD_BRNCH, java.lang.Integer BUA_AD_CMPNY)"
 *             query="SELECT OBJECT(bua) FROM InvBuildUnbuildAssembly bua WHERE bua.buaReceiving = 1 AND bua.buaReceiveBoNumber = ?1  AND bua.arCustomer.cstCustomerCode = ?2 AND bua.buaAdBranch = ?3 AND bua.buaAdCompany = ?4"
 *            
 * 
 * @ejb:value-object match="*"
 *             name="InvBuildUnbuildAssembly"
 *
 * @jboss:persistence table-name="INV_BLD_UNBLD_ASSMBLY"
 *
 * @jboss:entity-command name="mysql-get-generated-keys"
 */

public abstract class InvBuildUnbuildAssemblyBean extends AbstractEntityBean {
	
	
	// PERSISTENT METHODS
	
	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 * @ejb:pk-field
	 *
	 * @jboss:column-name name="BUA_CODE"
	 *
	 * @jboss:persistence auto-increment="true"
	 **/
	public abstract Integer getBuaCode();         
	public abstract void setBuaCode(Integer BUA_CODE);
	
	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 * 
	 * @jboss:column-name name="BUA_RCVNG"
	 */
	public abstract byte getBuaReceiving();

	/**
	 * @ejb:interface-method view-type="local"
	 */
	public abstract void setBuaReceiving(byte BUA_RCVNG);

	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 * 
	 * @jboss:column-name name="BUA_TYP"
	 */
	public abstract String getBuaType();

	/**
	 * @ejb:interface-method view-type="local"
	 */
	public abstract void setBuaType(String BUA_TYP);
	
	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="BUA_DCMNT_NMBR"
	 **/
	public abstract String getBuaDocumentNumber();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setBuaDocumentNumber(String BUA_DCMNT_NMBR);
	
	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="BUA_RFRNC_NMBR"
	 **/
	public abstract String getBuaReferenceNumber();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setBuaReferenceNumber(String BUA_RFRNC_NMBR);
	
	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="BUA_RCV_BO_NMBR"
	 **/
	public abstract String getBuaReceiveBoNumber();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setBuaReceiveBoNumber(String BUA_RCV_BO_NMBR);
	
	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 * 
	 * @jboss:column-name name="BUA_LCK"
	 */
	public abstract byte getBuaLock();

	/**
	 * @ejb:interface-method view-type="local"
	 */
	public abstract void setBuaLock(byte BUA_LCK);
	
	
	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="BUA_DT"
	 **/
	public abstract Date getBuaDate();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setBuaDate(Date BUA_DT);
	
	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="BUA_DUE_DT"
	 **/
	public abstract Date getBuaDueDate();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setBuaDueDate(Date BUA_DUE_DT);
	
	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="BUA_DESC"
	 **/
	public abstract String getBuaDescription();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setBuaDescription(String BUA_DESC);
	
	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="BUA_VD"
	 **/
	public abstract byte getBuaVoid();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setBuaVoid(byte BUA_VD);  
	
	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="BUA_APPRVL_STATUS"
	 **/
	public abstract String getBuaApprovalStatus();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setBuaApprovalStatus(String BUA_APPRVL_STATUS);
	
	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="BUA_PSTD"
	 **/
	public abstract byte getBuaPosted();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setBuaPosted(byte BUA_PSTD);
	
	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="BUA_CRTD_BY"
	 **/
	public abstract String getBuaCreatedBy();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setBuaCreatedBy(String BUA_CRTD_BY);
	
	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="BUA_DT_CRTD"
	 **/
	public abstract java.util.Date getBuaDateCreated();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setBuaDateCreated(java.util.Date BUA_DT_CRTD);
	
	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="BUA_LST_MDFD_BY"
	 **/
	public abstract String getBuaLastModifiedBy();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setBuaLastModifiedBy(String BUA_LST_MDFD_BY);
	
	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="BUA_DT_LST_MDFD"
	 **/
	public abstract java.util.Date getBuaDateLastModified();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setBuaDateLastModified(java.util.Date BUA_DT_LST_MDFD);
	
	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="BUA_APPRVD_RJCTD_BY"
	 **/
	public abstract String getBuaApprovedRejectedBy();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setBuaApprovedRejectedBy(String BUA_APPRVD_RJCTD_BY);
	
	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="BUA_DT_APPRVD_RJCTD"
	 **/
	public abstract java.util.Date getBuaDateApprovedRejected();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setBuaDateApprovedRejected(java.util.Date BUA_DT_APPRVD_RJCTD);
	
	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="BUA_PSTD_BY"
	 **/
	public abstract String getBuaPostedBy();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setBuaPostedBy(String BUA_PSTD_BY);
	
	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="BUA_DT_PSTD"
	 **/
	public abstract java.util.Date getBuaDatePosted();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setBuaDatePosted(java.util.Date BUA_DT_PSTD); 
	
	 /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="BUA_RSN_FR_RJCTN"
     **/
    public abstract String getBuaReasonForRejection();
    /**
     * @ejb:interface-method view-type="local"
     **/
    public abstract void setBuaReasonForRejection(String BUA_RSN_FR_RJCTN); 
    
    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="BUA_AD_BRNCH"
     **/
    public abstract Integer getBuaAdBranch();
    /**
     * @ejb:interface-method view-type="local"
     **/
    public abstract void setBuaAdBranch(Integer BUA_AD_BRNCH); 
    
    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="BUA_AD_CMPNY"
     **/
    public abstract Integer getBuaAdCompany();
    /**
     * @ejb:interface-method view-type="local"
     **/
    public abstract void setBuaAdCompany(Integer BUA_AD_CMPNY); 
	
	// RELATIONSHIP METHODS
    
    
    /**
	 * @ejb:interface-method view-type="local"
	 * @ejb:relation name="customer-buildunbuildassemblies"
	 *               role-name="buildunbuildassembly-has-one-customer"
	 *
	 * @jboss:relation related-pk-field="cstCode"
	 *                 fk-column="AR_CUSTOMER"
	 */
	public abstract LocalArCustomer getArCustomer();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setArCustomer(LocalArCustomer arCustomer);
	
    /**
	 * @ejb:interface-method view-type="local"
	 * @ejb:relation name="buildunbuildassemblybatch-buildunbuildassemblies"
	 *               role-name="buildunbuildassembly-has-one-buildunbuildassemblybatch"
	 *
	 * @jboss:relation related-pk-field="bbCode"
	 *                 fk-column="INV_BUA_BATCH"
	 */
	public abstract LocalInvBuildUnbuildAssemblyBatch getInvBuildUnbuildAssemblyBatch();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setInvBuildUnbuildAssemblyBatch(LocalInvBuildUnbuildAssemblyBatch invBuildUnbuildAssemblyBatch);
	

	
	/**
	 * @ejb:interface-method view-type="local"
	 * @ejb:relation name="buildunbuildassembly-buildunbuildassemblylines"
	 *               role-name="buildunbuildassembly-has-many-buildunbuildassemblylines"
	 */
	public abstract Collection getInvBuildUnbuildAssemblyLines();
	public abstract void setInvBuildUnbuildAssemblyLines(Collection invBuildUnbuildAssemblyLines);     
	
	/**
	 * @ejb:interface-method view-type="local"
	 * @ejb:relation name="buildunbuildassembly-invdistributionrecords"
	 *               role-name="buildunbuildassembly-has-many-invdistributionrecords"
	 */
	public abstract Collection getInvDistributionRecords();
	public abstract void setInvDistributionRecords(Collection invDistributionRecords);     
	
	/**
	 * @jboss:dynamic-ql
	 */
	public abstract Collection ejbSelectGeneric(java.lang.String jbossQl, java.lang.Object[] args)
	throws FinderException;
	
	// BUSINESS METHODS
	
	/**
	 * @ejb:home-method view-type="local"
	 */
	public Collection ejbHomeGetBuaByCriteria(java.lang.String jbossQl, java.lang.Object[] args)
	throws FinderException {
		
		return ejbSelectGeneric(jbossQl, args);
		
	}
	
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public void addInvBuildUnbuildAssemblyLine(LocalInvBuildUnbuildAssemblyLine invBuildUnbuildAssemblyLine) {
		
		Debug.print("InvBuildUnbuildAssemblyBean addInvBuildUnbuildAssemblyLine");
		
		try {
			
			Collection invBuildUnbuildAssemblyLines = getInvBuildUnbuildAssemblyLines();
			invBuildUnbuildAssemblyLines.add(invBuildUnbuildAssemblyLine);
			
		} catch (Exception ex) {
			
			throw new EJBException(ex.getMessage());
			
		}   
		
	}
	
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public void dropInvBuildUnbuildAssemblyLine(LocalInvBuildUnbuildAssemblyLine invBuildUnbuildAssemblyLine) {
		
		Debug.print("InvBuildUnbuildAssemblyBean dropInvBuildUnbuildAssemblyLine");
		
		try {
			
			Collection invBuildUnbuildAssemblyLines = getInvBuildUnbuildAssemblyLines();
			invBuildUnbuildAssemblyLines.remove(invBuildUnbuildAssemblyLine);
			
		} catch (Exception ex) {
			
			throw new EJBException(ex.getMessage());
			
		}
		
	}
	
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public void addInvDistributionRecord(LocalInvDistributionRecord invDistributionRecord) {
		
		Debug.print("InvBuildUnbuildAssemblyBean addInvDistributionRecord");
		
		try {
			
			Collection invDistributionRecords = getInvDistributionRecords();
			invDistributionRecords.add(invDistributionRecord);
			
		} catch (Exception ex) {
			
			throw new EJBException(ex.getMessage());
			
		}
		
	}
	
	/**
	 * @ejb:interface-method view-type="local"
	 **/ 
	public void dropInvDistributionRecord(LocalInvDistributionRecord invDistributionRecord) {
		
		Debug.print("InvBuildUnbuildAssemblyBean dropInvDistributionRecord");
		
		try {
			
			Collection invDistributionRecords = getInvDistributionRecords();
			invDistributionRecords.remove(invDistributionRecord);
			
		} catch (Exception ex) {
			
			throw new EJBException(ex.getMessage());
			
		}
		
	} 
	
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public short getInvDrNextLine() {
		
		Debug.print("InvBuildUnbuildAssemblyBean getInvDrNextLine");
		try {
			Collection invDistributionRecords = getInvDistributionRecords();
			return (short)(invDistributionRecords.size() + 1);		    
		} catch (Exception ex) {	      	
			throw new EJBException(ex.getMessage());	         
		}
	}       
	
	// ENTITY METHODS
	
	/**
	 * @ejb:create-method view-type="local"
	 **/
	public Integer ejbCreate(Integer BUA_CODE, byte BUA_RCVNG, String BUA_TYP, String BUA_DCMNT_NMBR, String BUA_RFRNC_NMBR, String BUA_RCV_BO_NMBR, byte BUA_LCK, 
			Date BUA_DT, Date BUA_DUE_DT,String BUA_DESC, byte BUA_VD, String BUA_APPRVL_STATUS,
			byte BUA_PSTD, String BUA_CRTD_BY, Date BUA_DT_CRTD, 
			String BUA_LST_MDFD_BY, Date BUA_DT_LST_MDFD, 
			String BUA_APPRVD_RJCTD_BY, Date BUA_DT_APPRVD_RJCTD, 
			String BUA_PSTD_BY, Date BUA_DT_PSTD, String BUA_RSN_FR_RJCTN,
			Integer BUA_AD_BRNCH, Integer BUA_AD_CMPNY)
	throws CreateException {
		
		Debug.print("InvBuildUnbuildAssemblyBean ejbCreate");
		
		setBuaCode(BUA_CODE);
		setBuaReceiving(BUA_RCVNG);
		setBuaType(BUA_TYP);
		setBuaDocumentNumber(BUA_DCMNT_NMBR);
		setBuaReferenceNumber(BUA_RFRNC_NMBR);
		setBuaReceiveBoNumber(BUA_RCV_BO_NMBR);
		setBuaLock(BUA_LCK);
		setBuaDate(BUA_DT);
		setBuaDueDate(BUA_DUE_DT);
		setBuaDescription(BUA_DESC);
		setBuaVoid(BUA_VD);
		setBuaApprovalStatus(BUA_APPRVL_STATUS);
		setBuaPosted(BUA_PSTD);
		setBuaCreatedBy(BUA_CRTD_BY);
		setBuaDateCreated(BUA_DT_CRTD);
		setBuaLastModifiedBy(BUA_LST_MDFD_BY);
		setBuaDateLastModified(BUA_DT_LST_MDFD);
		setBuaApprovedRejectedBy(BUA_APPRVD_RJCTD_BY);
		setBuaDateApprovedRejected(BUA_DT_APPRVD_RJCTD);
		setBuaPostedBy(BUA_PSTD_BY);
		setBuaDatePosted(BUA_DT_PSTD);
		setBuaReasonForRejection(BUA_RSN_FR_RJCTN);
		setBuaAdBranch(BUA_AD_BRNCH);
		setBuaAdCompany(BUA_AD_CMPNY);
		
		return null;
		
	}
	
	/**
	 * @ejb:create-method view-type="local"
	 **/
	public Integer ejbCreate(byte BUA_RCVNG, String BUA_TYP, String BUA_DCMNT_NMBR, String BUA_RFRNC_NMBR, String BUA_RCV_BO_NMBR, byte BUA_LCK,
			Date BUA_DT, Date BUA_DUE_DT, String BUA_DESC, byte BUA_VD, String BUA_APPRVL_STATUS,
			byte BUA_PSTD, String BUA_CRTD_BY, Date BUA_DT_CRTD, 
			String BUA_LST_MDFD_BY, Date BUA_DT_LST_MDFD, 
			String BUA_APPRVD_RJCTD_BY, Date BUA_DT_APPRVD_RJCTD, 
			String BUA_PSTD_BY, Date BUA_DT_PSTD, String BUA_RSN_FR_RJCTN,
			Integer BUA_AD_BRNCH, Integer BUA_AD_CMPNY)
	throws CreateException {
		
		Debug.print("InvBuildUnbuildAssemblyBean ejbCreate");
		
		setBuaReceiving(BUA_RCVNG);
		setBuaType(BUA_TYP);
		setBuaDocumentNumber(BUA_DCMNT_NMBR);
		setBuaReferenceNumber(BUA_RFRNC_NMBR);
		setBuaReceiveBoNumber(BUA_RCV_BO_NMBR);
		setBuaLock(BUA_LCK);
		setBuaDate(BUA_DT);
		setBuaDueDate(BUA_DUE_DT);
		setBuaDescription(BUA_DESC);
		setBuaVoid(BUA_VD);
		setBuaApprovalStatus(BUA_APPRVL_STATUS);
		setBuaPosted(BUA_PSTD);
		setBuaCreatedBy(BUA_CRTD_BY);
		setBuaDateCreated(BUA_DT_CRTD);
		setBuaLastModifiedBy(BUA_LST_MDFD_BY);
		setBuaDateLastModified(BUA_DT_LST_MDFD);
		setBuaApprovedRejectedBy(BUA_APPRVD_RJCTD_BY);
		setBuaDateApprovedRejected(BUA_DT_APPRVD_RJCTD);
		setBuaPostedBy(BUA_PSTD_BY);
		setBuaDatePosted(BUA_DT_PSTD);
		setBuaReasonForRejection(BUA_RSN_FR_RJCTN);
		setBuaAdBranch(BUA_AD_BRNCH);
		setBuaAdCompany(BUA_AD_CMPNY);
		
		return null;
		
	}
	
	
	public void ejbPostCreate(Integer BUA_CODE, byte BUA_RCVNG, String BUA_TYP, String BUA_DCMNT_NMBR, String BUA_RFRNC_NMBR, String BUA_RCV_BO_NMBR, byte BUA_LCK, 
			Date BUA_DT, Date BUA_DUE_DT, String BUA_DESC, byte BUA_VD, String BUA_APPRVL_STATUS,
			byte BUA_PSTD, String BUA_CRTD_BY, Date BUA_DT_CRTD, 
			String BUA_LST_MDFD_BY, Date BUA_DT_LST_MDFD, 
			String BUA_APPRVD_RJCTD_BY, Date BUA_DT_APPRVD_RJCTD, 
			String BUA_PSTD_BY, Date BUA_DT_PSTD, String BUA_RSN_FR_RJCTN,
			Integer BUA_AD_BRNCH, Integer BUA_AD_CMPNY)
	throws CreateException { }
	
	public void ejbPostCreate(byte BUA_RCVNG, String BUA_TYP, String BUA_DCMNT_NMBR, String BUA_RFRNC_NMBR, String BUA_RCV_BO_NMBR, byte BUA_LCK,
			Date BUA_DT, Date BUA_DUE_DT, String BUA_DESC, byte BUA_VD, String BUA_APPRVL_STATUS,
			byte BUA_PSTD, String BUA_CRTD_BY, Date BUA_DT_CRTD, 
			String BUA_LST_MDFD_BY, Date BUA_DT_LST_MDFD, 
			String BUA_APPRVD_RJCTD_BY, Date BUA_DT_APPRVD_RJCTD, 
			String BUA_PSTD_BY, Date BUA_DT_PSTD, String BUA_RSN_FR_RJCTN,
			Integer BUA_AD_BRNCH, Integer BUA_AD_CMPNY)
	throws CreateException { }
	
}

