package com.ejb.inv;

import java.util.Collection;

import javax.ejb.CreateException;
import javax.ejb.FinderException;

import com.util.AbstractEntityBean;
import com.util.Debug;

/**
 * @ejb:bean name="InvBuildUnbuildAssemblyLineEJB"
 *           display-name="Build Unbuild Assembly Line Entity"
 *           type="CMP"
 *           cmp-version="2.x"
 *           view-type="local"
 *           local-jndi-name="omega-ejb/InvBuildUnbuildAssemblyLineEJB"
 *           schema="InvBuildUnbuildAssemblyLine"
 *           primkey-field="blCode"
 *       
 * @ejb:pk class="java.lang.Integer"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="invuser"
 *                        role-link="invuserlink"
 *
 * @ejb:permission role-name="invuser"
 *
 * @ejb:interface local-class="com.ejb.inv.LocalInvBuildUnbuildAssemblyLine"
 *                local-extends="javax.ejb.EJBLocalObject"
 *
 * @ejb:home local-class="com.ejb.inv.LocalInvBuildUnbuildAssemblyLineHome"
 *           local-extends="javax.ejb.EJBLocalHome"
 * 
 * @ejb:finder signature = "Collection findNegativeBlByBuaPostedAndIlCodeAndBrCode(byte BUA_PSTD, java.lang.Integer IL_CODE, java.lang.Integer BUA_AD_BRNCH, java.lang.Integer BL_AD_CMPNY)"
 * 				query = "SELECT OBJECT(bl) FROM InvBuildUnbuildAssemblyLine bl WHERE bl.blBuildQuantity < 0 AND bl.invBuildUnbuildAssembly.buaPosted=?1 AND bl.invItemLocation.ilCode=?2 AND bl.invBuildUnbuildAssembly.buaAdBranch=?3 AND bl.blAdCompany=?4" *
 *
 * @jboss:query signature="Collection ejbSelectGeneric(java.lang.String jbossQl, java.lang.Object[] args)"
 *                         dynamic="true"
 *                         
 * @ejb:finder signature="Collection findUnpostedBuaByIiNameAndLocNameAndLessEqualDateAndBuaAdBranch(java.lang.String II_NM, java.lang.String LOC_NM, java.util.Date BUA_DT, java.lang.Integer BUA_AD_BRNCH, java.lang.Integer BL_AD_CMPNY)"
 * 			   query="SELECT OBJECT(bl) FROM InvBuildUnbuildAssemblyLine bl WHERE bl.invBuildUnbuildAssembly.buaPosted = 0 AND bl.invBuildUnbuildAssembly.buaReceiving = 1 AND bl.invBuildUnbuildAssembly.buaVoid = 0 AND bl.invItemLocation.invItem.iiName = ?1 AND bl.invItemLocation.invLocation.locName = ?2 AND bl.invBuildUnbuildAssembly.buaDate <= ?3 AND bl.invBuildUnbuildAssembly.buaAdBranch = ?4 AND bl.blAdCompany = ?5"          
 * 
 * @ejb:finder signature="Collection findUnpostedBuaByLocNameAndAdBranch(java.lang.String LOC_NM, java.lang.Integer BUA_AD_BRNCH, java.lang.Integer BL_AD_CMPNY)"
 * 			   query="SELECT OBJECT(bl) FROM InvBuildUnbuildAssemblyLine bl WHERE bl.invBuildUnbuildAssembly.buaPosted = 0 AND bl.invBuildUnbuildAssembly.buaReceiving = 1 AND bl.invBuildUnbuildAssembly.buaVoid = 0 AND bl.invItemLocation.invLocation.locName = ?1 AND bl.invBuildUnbuildAssembly.buaAdBranch = ?2 AND bl.blAdCompany = ?3"
 *
 * @ejb:finder signature="Collection findByBlBlCode(java.lang.Integer BL_CODE, java.lang.Integer BL_AD_CMPNY)"
 *             query="SELECT OBJECT(bl) FROM InvBuildUnbuildAssemblyLine bl WHERE bl.blBlCode = ?1 AND bl.blAdCompany = ?2"
 * 
 * @ejb:finder signature="Collection findByIiCodeLocNameAndAdBranch(java.lang.Integer II_CODE, java.lang.String LOC_NM, java.lang.Integer BUA_AD_BRNCH, java.lang.Integer BL_AD_CMPNY)"
 * 			   query="SELECT OBJECT(bl) FROM InvBuildUnbuildAssemblyLine bl WHERE bl.invBuildUnbuildAssembly.buaVoid = 0 AND bl.invItemLocation.invItem.iiCode=?1 AND bl.invItemLocation.invLocation.locName = ?2 AND bl.invBuildUnbuildAssembly.buaAdBranch = ?3 AND bl.blAdCompany = ?4"
 * 
 * @ejb:finder signature="Collection findByBuaRcvBuaNumberAndBuaReceivingAndCstCustomerCodeAndBrCode(java.lang.String BUA_RCV_PO_NMBR, java.lang.String CST_CSTMR_CODE, java.lang.Integer BUA_AD_BRNCH, java.lang.Integer BUA_AD_CMPNY)"
 * 			   query="SELECT OBJECT(bl) FROM InvBuildUnbuildAssemblyLine bl WHERE bl.invBuildUnbuildAssembly.buaVoid = 0 AND bl.invBuildUnbuildAssembly.buaReceiving = 1 AND bl.invBuildUnbuildAssembly.buaReceiveBoNumber = ?1 AND bl.invBuildUnbuildAssembly.arCustomer.cstCustomerCode = ?2 AND bl.invBuildUnbuildAssembly.buaAdBranch = ?3 AND bl.invBuildUnbuildAssembly.buaAdCompany = ?4"
 * 
 * @ejb:value-object match="*"
 *             name="InvBuildUnbuildAssemblyLine"
 *
 * @jboss:persistence table-name="INV_BLD_UNBLD_ASSMBLY_LN"
 *
 * @jboss:entity-command name="mysql-get-generated-keys"
 *
 */

public abstract class InvBuildUnbuildAssemblyLineBean extends AbstractEntityBean {
	
//	Access methods for persistent fields
	
	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 * @ejb:pk-field
	 *
	 * @jboss:column-name name="BL_CODE"
	 *
	 * @jboss:persistence auto-increment="true"
	 **/
	public abstract Integer getBlCode();
	public abstract void setBlCode(Integer BL_CODE);
	
	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="BL_BLD_QTY"
	 **/
	public abstract double getBlBuildQuantity();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setBlBuildQuantity(double BL_BLD_QTY);
	
	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="BL_BLD_SPCFC_GRVTY"
	 **/
	public abstract double getBlBuildSpecificGravity();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setBlBuildSpecificGravity(double BL_BLD_SPCFC_GRVTY);
	
	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="BL_BLD_STD_FLL_SZ"
	 **/
	public abstract double getBlBuildStandardFillSize();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setBlBuildStandardFillSize(double BL_BLD_STD_FLL_SZ);
	
	/**
	    * @ejb:persistent-field
	    * @ejb:interface-method view-type="local"
	    *
	    * @jboss:column-name name="BL_BL_CODE"
	    **/
	   public abstract Integer getBlBlCode();
	   /**
	    * @ejb:interface-method view-type="local"
	    **/
	   public abstract void setBlBlCode(Integer BL_BL_CODE);
	   
	
	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="BL_EXPRY_DT"
	 **/
	public abstract String getBlMisc();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setBlMisc(String BL_EXPRY_DT);
	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="BL_AD_CMPNY"
	 **/
	public abstract Integer getBlAdCompany();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setBlAdCompany(Integer BL_AD_CMPNY);
	
	// Access methods for relationship fields
	
	/**
	 * @ejb:interface-method view-type="local"
	 * @ejb:relation name="buildunbuildassembly-buildunbuildassemblylines"
	 *               role-name="buildunbuildassemblyline-has-one-buildunbuildassembly"
	 *               cascade-delete="yes"
	 *
	 * @jboss:relation related-pk-field="buaCode"
	 *                 fk-column="INV_BUILD_UNBUILD_ASSEMBLY"
	 */
	public abstract LocalInvBuildUnbuildAssembly getInvBuildUnbuildAssembly();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setInvBuildUnbuildAssembly(LocalInvBuildUnbuildAssembly invBuildUnBuildAssembly);
	
	/**
	 * @ejb:interface-method view-type="local"
	 * @ejb:relation name="unitofmeasure-buildunbuildassemblylines"
	 *               role-name="buildunbuildassemblyline-has-one-unitofmeasure"
	 *
	 * @jboss:relation related-pk-field="uomCode"
	 *                 fk-column="INV_UNIT_OF_MEASURE"
	 */
	public abstract LocalInvUnitOfMeasure getInvUnitOfMeasure();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setInvUnitOfMeasure(LocalInvUnitOfMeasure invUnitOfMeasure);
	
	/**
	 * @ejb:interface-method view-type="local"
	 * @ejb:relation name="itemlocation-buildunbuildassemblylines"
	 *               role-name="buildunbuildassemblyline-has-one-itemlocation"
	 *
	 * @jboss:relation related-pk-field="ilCode"
	 *                 fk-column="INV_ITEM_LOCATION"
	 */
	public abstract LocalInvItemLocation getInvItemLocation();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setInvItemLocation(LocalInvItemLocation invItemLocation);	  
	
	/**
	 * @ejb:interface-method view-type="local"
	 * @ejb:relation name="buildunbuildassemblyline-costings"
	 *               role-name="buildunbuildassemblyline-has-many-costings"
	 */
	public abstract Collection getInvCostings();
	public abstract void setInvCostings(Collection invCostings);
	
	/**
	    * @ejb:interface-method view-type="local"
	    * @ejb:relation name="buildunbuildassemblyline-tags"
	    *               role-name="buildunbuildassemblyline-has-many-tags"
	    *               
	    */
	   public abstract Collection getInvTags();
	   public abstract void setInvTags(Collection invTags);
		
	/**
	* @jboss:dynamic-ql
	*/
	public abstract Collection ejbSelectGeneric(java.lang.String jbossQl, java.lang.Object[] args)
	   throws FinderException;	   
  
   // Business methods	   

	/**
	* @ejb:home-method view-type="local"
	*/
	public Collection ejbHomeGetBolByCriteria(java.lang.String jbossQl, java.lang.Object[] args)
	   throws FinderException {
	   	
	   return ejbSelectGeneric(jbossQl, args);
	   
	}	 
		
	// EntityBean methods
	
	/**
	 * @ejb:create-method view-type="local"
	 **/
	public Integer ejbCreate (Integer BL_CODE, double BL_BLD_QTY, double BL_BLD_SPCFC_GRVTY, double BL_BLD_STD_FLL_SZ, Integer BL_BL_CODE, Integer BL_AD_CMPNY)
	throws CreateException {
		
		Debug.print("invBuildUnbuildAssemblyLineBean ejbCreate");
		
		setBlCode(BL_CODE);
		setBlBuildQuantity(BL_BLD_QTY);
		setBlBuildSpecificGravity(BL_BLD_SPCFC_GRVTY);
		setBlBuildStandardFillSize(BL_BLD_STD_FLL_SZ);
		setBlBlCode(BL_BL_CODE);
		setBlAdCompany(BL_AD_CMPNY);

		return null;
	}
	
	/**
	 * @ejb:create-method view-type="local"
	 **/
	public Integer ejbCreate (double BL_BLD_QTY, double BL_BLD_SPCFC_GRVTY, double BL_BLD_STD_FLL_SZ, Integer BL_BL_CODE, Integer BL_AD_CMPNY)
	throws CreateException {
		
		Debug.print("invBuildUnbuildAssemblyLineBean ejbCreate");
		
		setBlBuildQuantity(BL_BLD_QTY);
		setBlBuildSpecificGravity(BL_BLD_SPCFC_GRVTY);
		setBlBuildStandardFillSize(BL_BLD_STD_FLL_SZ);
		setBlBlCode(BL_BL_CODE);
		setBlAdCompany(BL_AD_CMPNY);

		return null;
	}
	
	public void ejbPostCreate (Integer BL_CODE, double BL_BLD_QTY, double BL_BLD_SPCFC_GRVTY, double BL_BLD_STD_FLL_SZ, Integer BL_BL_CODE, Integer BL_AD_CMPNY)
	throws CreateException { }
	
	public void ejbPostCreate (double BL_BLD_QTY, double BL_BLD_SPCFC_GRVTY, double BL_BLD_STD_FLL_SZ, Integer BL_BL_CODE, Integer BL_AD_CMPNY)
	throws CreateException { }
	
}
