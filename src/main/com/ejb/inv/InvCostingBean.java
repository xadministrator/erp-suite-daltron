package com.ejb.inv;

import java.util.Collection;
import java.util.Date;

import javax.ejb.CreateException;
import javax.ejb.FinderException;

import com.ejb.ap.LocalApPurchaseOrderLine;
import com.ejb.ap.LocalApVoucherLineItem;
import com.ejb.ar.LocalArInvoiceLineItem;
import com.ejb.ar.LocalArJobOrderInvoiceLine;
import com.ejb.ar.LocalArSalesOrderInvoiceLine;
import com.util.AbstractEntityBean;
import com.util.Debug;

/**
 * @ejb:bean name="InvCostingEJB"
 *           display-name="Costing Entity"
 *           type="CMP"
 *           cmp-version="2.x"
 *           view-type="local"
 *           local-jndi-name="omega-ejb/InvCostingEJB"
 *           schema="InvCosting"
 *           primkey-field="cstCode"
 *       
 * @ejb:pk class="java.lang.Integer"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="invuser"
 *                        role-link="invuserlink"
 *
 * @ejb:permission role-name="invuser"
 *
 * @ejb:interface local-class="com.ejb.inv.LocalInvCosting"
 *                local-extends="javax.ejb.EJBLocalObject"
 *
 * @ejb:home local-class="com.ejb.inv.LocalInvCostingHome"
 *           local-extends="javax.ejb.EJBLocalHome"
 *  
 *             
 * @jboss:query signature="LocalInvCosting ejbSelectGetLatestAverageCost(java.util.Date CST_DT, java.lang.String II_NM, java.lang.String LOC_NM, java.lang.Integer CST_AD_BRNCH, java.lang.Integer CST_AD_CMPNY)"
 *                         query="SELECT OBJECT(cst) FROM InvCosting cst WHERE cst.cstDate <= ?1 AND cst.invItemLocation.invItem.iiName=?2 AND cst.invItemLocation.invLocation.locName=?3 AND cst.cstRemainingQuantity > 0 AND cst.cstAdBranch = ?4 AND cst.cstAdCompany = ?5 ORDER BY cst.cstDate DESC, cst.cstDateToLong DESC, cst.cstLineNumber DESC LIMIT 1"
 *                      
 * @jboss:query signature="LocalInvCosting ejbSelectByMaxCstLineNumberAndCstDateLessThanEqualCstDateAndIiNameAndLocName(java.util.Date CST_DT, java.lang.String II_NM, java.lang.String LOC_NM, java.lang.Integer CST_AD_BRNCH, java.lang.Integer CST_AD_CMPNY)"
 *                         query="SELECT OBJECT(cst) FROM InvCosting cst WHERE cst.cstDate <= ?1 AND cst.invItemLocation.invItem.iiName=?2 AND cst.invItemLocation.invLocation.locName=?3 AND cst.cstAdBranch = ?4 AND cst.cstAdCompany = ?5 ORDER BY cst.cstDate DESC, cst.cstDateToLong DESC, cst.cstLineNumber DESC LIMIT 1"
 * 
 * @jboss:query signature="LocalInvCosting ejbSelectByMaxCstLineNumberAndCstDateLessThanEqualCstDateAndIiNameAndLocNameAndFifoRemainingQuantity(java.util.Date CST_DT, java.lang.String II_NM, java.lang.String LOC_NM, java.lang.Integer CST_AD_BRNCH, java.lang.Integer CST_AD_CMPNY)"
 *                         query="SELECT OBJECT(cst) FROM InvCosting cst WHERE cst.cstRemainingLifoQuantity > 0 AND cst.cstDate <= ?1 AND cst.invItemLocation.invItem.iiName=?2 AND cst.invItemLocation.invLocation.locName=?3 AND cst.cstAdBranch = ?4 AND cst.cstAdCompany = ?5 ORDER BY cst.cstDate DESC, cst.cstLineNumber DESC LIMIT 1"
 *                       
 * @jboss:query signature="LocalInvCosting ejbSelectByMaxCstLineNumberAndCstDateToLongAndIiNameAndLocName(long CST_DT_TO_LNG, java.lang.String II_NM, java.lang.String LOC_NM, java.lang.Integer CST_AD_BRNCH, java.lang.Integer CST_AD_CMPNY)"
 *                         query="SELECT OBJECT(cst) FROM InvCosting cst WHERE cst.cstDateToLong=?1 AND cst.invItemLocation.invItem.iiName=?2 AND cst.invItemLocation.invLocation.locName=?3 AND cst.cstAdBranch = ?4 AND cst.cstAdCompany = ?5 ORDER BY cst.cstDate DESC, cst.cstLineNumber DESC, cst.cstDateToLong DESC LIMIT 1"
 *
 * @jboss:query signature="LocalInvCosting ejbSelectByMaxCstLineNumberAndIiNameAndLocName(java.lang.String II_NM, java.lang.String LOC_NM, java.lang.Integer CST_AD_BRNCH, java.lang.Integer CST_AD_CMPNY)"
 *                         query="SELECT OBJECT(cst) FROM InvCosting cst WHERE cst.invItemLocation.invItem.iiName=?1 AND cst.invItemLocation.invLocation.locName=?2 AND cst.cstAdBranch = ?3 AND cst.cstAdCompany = ?4 ORDER BY cst.cstDate DESC, cst.cstDateToLong DESC, cst.cstLineNumber DESC LIMIT 1"
 *                         
 * @jboss:query signature="LocalInvCosting ejbSelectByMaxCstLineNumberAndRemainingQuantityNotEqualToZeroAndIiNameAndLocName(java.lang.String II_NM, java.lang.String LOC_NM, java.lang.Integer CST_AD_BRNCH, java.lang.Integer CST_AD_CMPNY)"
 *                         query="SELECT OBJECT(cst) FROM InvCosting cst WHERE cst.invItemLocation.invItem.iiName=?1 AND cst.invItemLocation.invLocation.locName=?2 AND cst.cstRemainingQuantity <> 0 AND cst.cstAdBranch = ?3 AND cst.cstAdCompany = ?4 ORDER BY cst.cstDate DESC, cst.cstLineNumber DESC LIMIT 1"
 *
 * @jboss:query signature="LocalInvCosting ejbSelectByMaxCstLineNumberAndMaxCstDateToLongByLessThanCstDateAndIiNameAndLocName(java.util.Date CST_DT, java.lang.String II_NM, java.lang.String LOC_NM, java.lang.Integer CST_AD_BRNCH, java.lang.Integer CST_AD_CMPNY)"
 *                         query="SELECT OBJECT(cst) FROM InvCosting cst WHERE cst.cstDate < ?1 AND cst.invItemLocation.invItem.iiName=?2 AND cst.invItemLocation.invLocation.locName=?3 AND cst.cstAdBranch = ?4 AND cst.cstAdCompany = ?5 ORDER BY cst.cstDate DESC, cst.cstLineNumber DESC LIMIT 1"
 *
 *                         
 * @jboss:query signature="LocalInvCosting ejbSelectByMaxCstLineNumberAndCstDateLessThanEqualCstDateAndIlCode(java.util.Date CST_DT, java.lang.Integer IL_CODE, java.lang.Integer CST_AD_BRNCH, java.lang.Integer CST_AD_CMPNY)"
 *                         query="SELECT OBJECT(cst) FROM InvCosting cst WHERE cst.cstDate <= ?1 AND cst.invItemLocation.ilCode=?2 AND cst.cstAdBranch = ?3 AND cst.cstAdCompany = ?4 ORDER BY cst.cstDate DESC, cst.cstLineNumber DESC LIMIT 1"
 *                                                                               
 * @jboss:query signature="LocalInvCosting ejbSelectByMaxCstDateToLongByLessThanEqualCstDateAndRemainingQuantityNotEqualToZeroAndIlCodeAndMaxLineNumber(java.util.Date CST_DT, java.lang.Integer IL_CODE, java.lang.Integer CST_AD_BRNCH, java.lang.Integer CST_AD_CMPNY)"
 *             query="SELECT OBJECT(cst) FROM InvCosting cst WHERE cst.cstDate <= ?1 AND  cst.cstRemainingQuantity <> 0 AND cst.invItemLocation.ilCode=?2  AND cst.cstAdBranch = ?3 AND cst.cstAdCompany = ?4 ORDER BY cst.cstDate DESC, cst.cstLineNumber DESC LIMIT 1"
 *                       
 * @ejb:finder signature="LocalInvCosting findByCstDateAndIlCode(java.util.Date CST_DT, java.lang.Integer IL_CODE, java.lang.Integer CST_AD_BRNCH, java.lang.Integer CST_AD_CMPNY)"
 *             query="SELECT OBJECT(cst) FROM InvCosting cst WHERE cst.cstDate=?1 AND cst.invItemLocation.ilCode=?2 AND cst.cstAdBranch = ?3 AND cst.cstAdCompany = ?4" 
 * 
 * @ejb:finder signature="Collection findByIiNameAndLocNameAndAdjLineCode(java.util.Date CST_DT, java.lang.String II_NM, java.lang.String LOC_NM, java.lang.Integer AL_CODE, java.lang.Integer CST_AD_BRNCH, java.lang.Integer CST_AD_CMPNY)"
 *             query="SELECT OBJECT(cst) FROM InvCosting cst WHERE cst.cstDate<?1 AND cst.invItemLocation.invItem.iiName=?2 AND cst.invItemLocation.invLocation.locName=?3 AND cst.invAdjustmentLine.alCode=?4 AND cst.cstAdBranch=?5 AND cst.cstAdCompany=?6" 
 * 
 * @ejb:finder signature="LocalInvCosting findByCstLineNumberAndCstDateAndIlCode(int CST_LN_NMBR, java.util.Date CST_DT, java.lang.Integer IL_CODE, java.lang.Integer CST_AD_BRNCH, java.lang.Integer CST_AD_CMPNY)"
 *             query="SELECT OBJECT(cst) FROM InvCosting cst WHERE cst.cstLineNumber=?1 AND cst.cstDate=?2 AND cst.invItemLocation.ilCode=?3 AND cst.cstAdBranch = ?4 AND cst.cstAdCompany = ?5"
 * 
 * @ejb:finder signature="Collection findByGreaterThanCstDateAndIiNameAndLocName(java.util.Date CST_DT, java.lang.String II_NM, java.lang.String LOC_NM, java.lang.Integer CST_AD_BRNCH, java.lang.Integer CST_AD_CMPNY)"
 *             query="SELECT OBJECT(cst) FROM InvCosting cst WHERE cst.cstDate > ?1 AND cst.invItemLocation.invItem.iiName=?2 AND cst.invItemLocation.invLocation.locName=?3 AND cst.cstAdBranch = ?4 AND cst.cstAdCompany = ?5 ORDER BY cst.cstDate, cst.cstLineNumber"
 * 
 * @ejb:finder signature="Collection findByCstDateAndIiNameAndLocName(java.util.Date CST_DT, java.lang.String II_NM, java.lang.String LOC_NM, java.lang.Integer CST_AD_BRNCH, java.lang.Integer CST_AD_CMPNY)"
 *             query="SELECT OBJECT(cst) FROM InvCosting cst WHERE cst.cstDate = ?1 AND cst.invItemLocation.invItem.iiName=?2 AND cst.invItemLocation.invLocation.locName=?3 AND cst.cstAdBranch = ?4 AND cst.cstAdCompany = ?5"
 *
 * @ejb:finder signature="Collection findByCstDateAndIiNameAndLocName2(java.util.Date CST_DT, java.lang.String II_NM, java.lang.String LOC_NM, java.lang.Integer CST_AD_BRNCH, java.lang.Integer CST_AD_CMPNY)"
 *             query="SELECT OBJECT(cst) FROM InvCosting cst WHERE cst.cstDate <= ?1 AND cst.invItemLocation.invItem.iiName=?2 AND cst.invItemLocation.invLocation.locName=?3 AND cst.cstAdBranch = ?4 AND cst.cstAdCompany = ?5 AND cst.cstAdjustQuantity > 0"
 *
 * @ejb:finder signature="LocalInvCosting findLastReceiveDateByCstDateAndIiNameAndLocName(java.util.Date CST_DT, java.lang.String II_NM, java.lang.String LOC_NM, java.lang.Integer CST_AD_BRNCH, java.lang.Integer CST_AD_CMPNY)"
 *             query="SELECT OBJECT(cst) FROM InvCosting cst WHERE cst.cstDate <= ?1 AND cst.invItemLocation.invItem.iiName=?2 AND cst.invItemLocation.invLocation.locName=?3 AND cst.cstAdBranch = ?4 AND cst.cstAdCompany = ?5 AND (cst.cstAdjustQuantity > 0 OR cst.cstQuantityReceived > 0)"
 *
 * @jboss:query signature="LocalInvCosting findLastReceiveDateByCstDateAndIiNameAndLocName(java.util.Date CST_DT, java.lang.String II_NM, java.lang.String LOC_NM, java.lang.Integer CST_AD_BRNCH, java.lang.Integer CST_AD_CMPNY)"
 *             query="SELECT OBJECT(cst) FROM InvCosting cst WHERE cst.cstDate <= ?1 AND cst.invItemLocation.invItem.iiName=?2 AND cst.invItemLocation.invLocation.locName=?3 AND cst.cstAdBranch = ?4 AND cst.cstAdCompany = ?5 AND (cst.cstAdjustQuantity > 0 OR cst.cstQuantityReceived > 0) ORDER BY cst.cstDate DESC LIMIT 1"
 *             
 * @ejb:finder signature="Collection findByCstDateGeneralOnlyByMonth(java.util.Date CST_DT_FRM,java.util.Date CST_DT_TO,java.lang.Integer CST_AD_CMPNY)"
 *             query="SELECT OBJECT(cst) FROM InvCosting cst WHERE cst.cstDate >= ?1 AND cst.cstDate <= ?2 AND cst.cstAdCompany = ?3 AND cst.cstAdjustQuantity > 0 ORDER BY cst.invAdjustmentLine.invAdjustment.adjCode,cst.invAdjustmentLine.invItemLocation.invItem.iiName"
 *             
 * @ejb:finder signature="Collection findByCstDateAndIiNameAndLocName3(java.util.Date CST_DT, java.lang.String II_NM, java.lang.String LOC_NM, java.lang.Integer CST_AD_BRNCH, java.lang.Integer CST_AD_CMPNY)"
 *             query="SELECT OBJECT(cst) FROM InvCosting cst WHERE cst.cstDate <= ?1 AND cst.invItemLocation.invItem.iiName=?2 AND cst.invItemLocation.invLocation.locName=?3 AND cst.cstAdBranch = ?4 AND cst.cstAdCompany = ?5 AND cst.cstAdjustQuantity < 0"
 *
 * @ejb:finder signature="Collection findNegTxnByGreaterThanCstDateAndIiNameAndLocName(java.util.Date CST_DT, java.lang.String II_NM, java.lang.String LOC_NM, java.lang.Integer CST_AD_BRNCH, java.lang.Integer CST_AD_CMPNY)"
 *             query="SELECT OBJECT(cst) FROM InvCosting cst WHERE (cst.cstAssemblyQuantity < 0 OR cst.cstAdjustQuantity < 0 OR cst.cstQuantitySold > 0) AND cst.cstDate > ?1 AND cst.invItemLocation.invItem.iiName=?2 AND cst.invItemLocation.invLocation.locName=?3 AND cst.cstAdBranch = ?4 AND cst.cstAdCompany = ?5"
 * 
 * @ejb:finder signature="Collection findByCstDateFromAndCstDateToAndIlCode(java.util.Date CST_DT_FRM, java.util.Date CST_DT_TO, java.lang.Integer IL_CODE, java.lang.Integer CST_AD_BRNCH, java.lang.Integer CST_AD_CMPNY)"
 *             query="SELECT OBJECT(cst) FROM InvCosting cst WHERE cst.cstDate >= ?1 AND cst.cstDate <= ?2 AND cst.invItemLocation.ilCode=?3 AND cst.cstAdBranch = ?4 AND cst.cstAdCompany = ?5"
 *             
 * @ejb:finder signature="Collection findQtySoldByCstDateFromAndCstDateToAndIlCode(java.util.Date CST_DT_FRM, java.util.Date CST_DT_TO, java.lang.Integer IL_CODE, java.lang.Integer CST_AD_BRNCH, java.lang.Integer CST_AD_CMPNY)"
 *             query="SELECT OBJECT(cst) FROM InvCosting cst WHERE (cst.arInvoiceLineItem.arReceipt.rctVoid = 0 OR cst.arInvoiceLineItem.arInvoice.invVoid=0 OR cst.arSalesOrderInvoiceLine.arInvoice.invVoid=0 OR cst.arJobOrderInvoiceLine.arInvoice.invVoid=0) AND cst.cstQuantitySold > 0 AND cst.cstDate >= ?1 AND cst.cstDate <= ?2 AND cst.invItemLocation.ilCode=?3 AND cst.cstAdBranch = ?4 AND cst.cstAdCompany = ?5"
 *             
 * @jboss:query signature="Collection findQtySoldByCstDateFromAndCstDateToAndIlCode(java.util.Date CST_DT_FRM, java.util.Date CST_DT_TO, java.lang.Integer IL_CODE, java.lang.Integer CST_AD_BRNCH, java.lang.Integer CST_AD_CMPNY)"
 *             query="SELECT OBJECT(cst) FROM InvCosting cst WHERE (cst.arInvoiceLineItem.arReceipt.rctVoid = 0 OR cst.arInvoiceLineItem.arInvoice.invVoid=0 OR cst.arSalesOrderInvoiceLine.arInvoice.invVoid=0 OR cst.arJobOrderInvoiceLine.arInvoice.invVoid=0) AND cst.cstQuantitySold > 0 AND cst.cstDate >= ?1 AND cst.cstDate <= ?2 AND cst.invItemLocation.ilCode=?3 AND cst.cstAdBranch = ?4 AND cst.cstAdCompany = ?5 ORDER BY cst.cstDate, cst.cstLineNumber"
 * 
 * @ejb:finder signature="LocalInvCosting findNegativeRemainingQuantityByIlCodeAndBrCode(java.lang.Integer IL_CODE, java.lang.Integer CST_AD_BRNCH, java.lang.Integer CST_AD_CMPNY)"
 *             query="SELECT OBJECT(cst) FROM InvCosting cst WHERE cst.cstRemainingQuantity < 0 AND cst.invItemLocation.ilCode=?1 AND cst.cstAdBranch = ?2 AND cst.cstAdCompany = ?3"
 * 
 * @jboss:query signature="LocalInvCosting findNegativeRemainingQuantityByIlCodeAndBrCode(java.lang.Integer IL_CODE, java.lang.Integer CST_AD_BRNCH, java.lang.Integer CST_AD_CMPNY)"
 *             query="SELECT OBJECT(cst) FROM InvCosting cst WHERE cst.cstRemainingQuantity < 0 AND cst.invItemLocation.ilCode=?1 AND cst.cstAdBranch = ?2 AND cst.cstAdCompany = ?3 ORDER BY cst.cstDateToLong"             
 * 
 * @ejb:finder signature="LocalInvCosting findByCstDateAndIiNameAndLocNameLimit1(java.util.Date CST_DT, java.lang.String II_NM, java.lang.String LOC_NM, java.lang.Integer CST_AD_BRNCH, java.lang.Integer CST_AD_CMPNY)"
 *             query="SELECT OBJECT(cst) FROM InvCosting cst WHERE cst.cstDate = ?1 AND cst.invItemLocation.invItem.iiName=?2 AND cst.invItemLocation.invLocation.locName=?3 AND cst.cstAdBranch = ?4 AND cst.cstAdCompany = ?5"
 *
 * @jboss:query signature="LocalInvCosting findByCstDateAndIiNameAndLocNameLimit1(java.util.Date CST_DT, java.lang.String II_NM, java.lang.String LOC_NM, java.lang.Integer CST_AD_BRNCH, java.lang.Integer CST_AD_CMPNY)"
 *             query="SELECT OBJECT(cst) FROM InvCosting cst WHERE cst.cstDate = ?1 AND cst.invItemLocation.invItem.iiName=?2 AND cst.invItemLocation.invLocation.locName=?3 AND cst.cstAdBranch = ?4 AND cst.cstAdCompany = ?5 ORDER BY cst.cstDate DESC, cst.cstLineNumber DESC, cst.cstDateToLong DESC LIMIT 1"
 *
 * @ejb:finder signature="Collection findFifoRemainingQuantityByLessThanOrEqualCstDateAndIlCodeAndBrCode(java.util.Date CST_DT, java.lang.Integer IL_CODE, java.lang.Integer CST_AD_BRNCH, java.lang.Integer CST_AD_CMPNY)"
 * 			   query="SELECT OBJECT(cst) FROM InvCosting cst WHERE cst.cstRemainingLifoQuantity > 0 AND cst.cstDate <= ?1 AND cst.invItemLocation.ilCode=?2 AND cst.cstAdBranch=?3 AND cst.cstAdCompany=?4 ORDER BY cst.cstDate"
 *
 * @ejb:finder signature="Collection findFifoRemainingQuantityByLessThanOrEqualCstDateAndIlCodeAndBrCode2(java.util.Date CST_DT, java.lang.Integer IL_CODE, java.lang.Integer CST_AD_BRNCH, java.lang.Integer CST_AD_CMPNY)"
 * 			   query="SELECT OBJECT(cst) FROM InvCosting cst WHERE cst.cstRemainingLifoQuantity > 0 AND cst.cstDate <= ?1 AND cst.invItemLocation.ilCode=?2 AND cst.cstAdBranch=?3 AND cst.cstAdCompany=?4 ORDER BY cst.cstDate"
 * 
 * @jboss:query signature="Collection findFifoRemainingQuantityByLessThanOrEqualCstDateAndIlCodeAndBrCode(java.util.Date CST_DT, java.lang.Integer IL_CODE, java.lang.Integer CST_AD_BRNCH, java.lang.Integer CST_AD_CMPNY)"
 * 			   query="SELECT OBJECT(cst) FROM InvCosting cst WHERE cst.cstRemainingLifoQuantity > 0 AND cst.cstDate <= ?1 AND cst.invItemLocation.ilCode=?2 AND cst.cstAdBranch=?3 AND cst.cstAdCompany=?4 ORDER BY cst.cstDateToLong, cst.cstLineNumber"
 * 
 * @ejb:finder signature="Collection findByPriorEqualCstDateToAndIlCode(java.util.Date CST_DT_TO, java.lang.Integer IL_CODE, java.lang.Integer CST_AD_BRNCH, java.lang.Integer CST_AD_CMPNY)"
 *             query="SELECT OBJECT(cst) FROM InvCosting cst WHERE cst.cstDate <= ?1 AND cst.invItemLocation.ilCode=?2 AND cst.cstAdBranch = ?3 AND cst.cstAdCompany = ?4"
 *             
 * @ejb:select signature="Collection ejbSelectGeneric(java.lang.String jbossQl, java.lang.Object[] args)"
 *             result-type-mapping="Local"
 *             method-intf="LocalHome"
 *             query="SELECT OBJECT(cst) FROM InvCosting cst"
 *
 * @jboss:query signature="Collection ejbSelectGeneric(java.lang.String jbossQl, java.lang.Object[] args)"
 *                         dynamic="true"
 *
 * @ejb:value-object match="*"
 *             name="InvCosting"
 *
 * @jboss:persistence table-name="INV_CSTNG"
 *
 * @jboss:entity-command name="mysql-get-generated-keys"
 *
 */


public abstract class InvCostingBean extends AbstractEntityBean {
	
//	 Access methods for persistent fields

	   /**
	    * @ejb:persistent-field
	    * @ejb:interface-method view-type="local"
	    * @ejb:pk-field
	    *
	    * @jboss:column-name name="CST_CODE"
	    *
	    * @jboss:persistence auto-increment="true"
	    **/
	   public abstract java.lang.Integer getCstCode();
	   public abstract void setCstCode(java.lang.Integer CST_CODE);

	   /**
	    * @ejb:persistent-field
	    * @ejb:interface-method view-type="local"
	    *
	    * @jboss:column-name name="CST_DT"
	    **/
	   public abstract java.util.Date getCstDate();
	   /**
	    * @ejb:interface-method view-type="local"
	    **/
	   public abstract void setCstDate(java.util.Date CST_DT);
	   
	   /**
	    * @ejb:persistent-field
	    * @ejb:interface-method view-type="local"
	    *
	    * @jboss:column-name name="CST_DT_TO_LNG"
	    **/
	   public abstract long getCstDateToLong();
	   /**
	    * @ejb:interface-method view-type="local"
	    **/
	   public abstract void setCstDateToLong(long CST_DT_TO_LNG);

	   /**
	    * @ejb:persistent-field
	    * @ejb:interface-method view-type="local"
	    *
	    * @jboss:column-name name="CST_LN_NMBR"
	    **/
	   public abstract int getCstLineNumber();
	   /**
	    * @ejb:interface-method view-type="local"
	    **/
	   public abstract void setCstLineNumber(int CST_LN_NMBR);
	   
	   /**
	    * @ejb:persistent-field
	    * @ejb:interface-method view-type="local"
	    *
	    * @jboss:column-name name="CST_QTY_RCVD"
	    **/
	   public abstract double getCstQuantityReceived();
	   /**
	    * @ejb:interface-method view-type="local"
	    **/
	   public abstract void setCstQuantityReceived(double CST_QTY_RCVD);
	   
	   /**
	    * @ejb:persistent-field
	    * @ejb:interface-method view-type="local"
	    *
	    * @jboss:column-name name="CST_ITM_CST"
	    **/
	   public abstract double getCstItemCost();
	   /**
	    * @ejb:interface-method view-type="local"
	    **/
	   public abstract void setCstItemCost(double CST_ITM_CST);
	   
	   /**
	    * @ejb:persistent-field
	    * @ejb:interface-method view-type="local"
	    *
	    * @jboss:column-name name="CST_ASSMBLY_QTY"
	    **/
	   public abstract double getCstAssemblyQuantity();
	   /**
	    * @ejb:interface-method view-type="local"
	    **/
	   public abstract void setCstAssemblyQuantity(double CST_ASSMBLY_QTY);
	   
	   /**
	    * @ejb:persistent-field
	    * @ejb:interface-method view-type="local"
	    *
	    * @jboss:column-name name="CST_ASSMBLY_CST"
	    **/
	   public abstract double getCstAssemblyCost();
	   /**
	    * @ejb:interface-method view-type="local"
	    **/
	   public abstract void setCstAssemblyCost(double CST_ASSMBLY_CST);
	   
	   /**
	    * @ejb:persistent-field
	    * @ejb:interface-method view-type="local"
	    *
	    * @jboss:column-name name="CST_ADJST_QTY"
	    **/
	   public abstract double getCstAdjustQuantity();
	   /**
	    * @ejb:interface-method view-type="local"
	    **/
	   public abstract void setCstAdjustQuantity(double CST_ADJST_QTY);
	   
	   /**
	    * @ejb:persistent-field
	    * @ejb:interface-method view-type="local"
	    *
	    * @jboss:column-name name="CST_ADJST_CST"
	    **/
	   public abstract double getCstAdjustCost();
	   /**
	    * @ejb:interface-method view-type="local"
	    **/
	   public abstract void setCstAdjustCost(double CST_ADJST_CST);
	   
	   /**
	    * @ejb:persistent-field
	    * @ejb:interface-method view-type="local"
	    *
	    * @jboss:column-name name="CST_QTY_SLD"
	    **/
	   public abstract double getCstQuantitySold();
	   /**
	    * @ejb:interface-method view-type="local"
	    **/
	   public abstract void setCstQuantitySold(double CST_QTY_SLD);
	   
	   /**
	    * @ejb:persistent-field
	    * @ejb:interface-method view-type="local"
	    *
	    * @jboss:column-name name="CST_CST_OF_SLS"
	    **/
	   public abstract double getCstCostOfSales();
	   /**
	    * @ejb:interface-method view-type="local"
	    **/
	   public abstract void setCstCostOfSales(double CST_CST_OF_SLS);
	   
	   /**
	    * @ejb:persistent-field
	    * @ejb:interface-method view-type="local"
	    *
	    * @jboss:column-name name="CST_RMNNG_QTY"
	    **/
	   public abstract double getCstRemainingQuantity();
	   /**
	    * @ejb:interface-method view-type="local"
	    **/
	   public abstract void setCstRemainingQuantity(double CST_RMNNG_QTY);
	   
	   /**
	    * @ejb:persistent-field
	    * @ejb:interface-method view-type="local"
	    *
	    * @jboss:column-name name="CST_RMNNG_VL"
	    **/
	   public abstract double getCstRemainingValue();
	   /**
	    * @ejb:interface-method view-type="local"
	    **/
	   public abstract void setCstRemainingValue(double CST_RMNNG_VL);
	   
	   /**
	    * @ejb:persistent-field
	    * @ejb:interface-method view-type="local"
	    *
	    * @jboss:column-name name="CST_VRNC_QTY"
	    **/
	   public abstract double getCstVarianceQuantity();
	   /**
	    * @ejb:interface-method view-type="local"
	    **/
	   public abstract void setCstVarianceQuantity(double CST_VRNC_QTY);
	   
	   /**
	    * @ejb:persistent-field
	    * @ejb:interface-method view-type="local"
	    *
	    * @jboss:column-name name="CST_VRNC_VL"
	    **/
	   public abstract double getCstVarianceValue();
	   /**
	    * @ejb:interface-method view-type="local"
	    **/
	   public abstract void setCstVarianceValue(double CST_VRNC_VL);	   
	   
	   /**
	    * @ejb:persistent-field
	    * @ejb:interface-method view-type="local"
	    *
	    * @jboss:column-name name="CST_RMNNG_LIFO_QTY"
	    **/
	   public abstract double getCstRemainingLifoQuantity();
	   /**
	    * @ejb:interface-method view-type="local"
	    **/
	   public abstract void setCstRemainingLifoQuantity(double CST_RMNNG_LIFO_QTY);
	   
	   /**
	    * @ejb:persistent-field
	    * @ejb:interface-method view-type="local"
	    *
	    * @jboss:column-name name="CST_RMNNG_LIFO_OUT_QTY"
	    **/
	   public abstract double getCstRemainingLifoOutQuantity();
	   /**
	    * @ejb:interface-method view-type="local"
	    **/
	   public abstract void setCstRemainingLifoOutQuantity(double CST_RMNNG_LIFO_OUT_QTY);

	   /**
	    * @ejb:persistent-field
	    * @ejb:interface-method view-type="local"
	    *
	    * @jboss:column-name name="CST_EXPRY_DT"
	    **/
	   public abstract String getCstExpiryDate();
	   /**
	    * @ejb:interface-method view-type="local"
	    **/
	   public abstract void setCstExpiryDate(String CST_EXPRY_DT);
	   
	   /**
	    * @ejb:persistent-field
	    * @ejb:interface-method view-type="local"
	    *
	    * @jboss:column-name name="CST_QC_NM"
	    **/
	   public abstract String getCstQCNumber();
	   /**
	    * @ejb:interface-method view-type="local"
	    **/
	   public abstract void setCstQCNumber(String CST_QC_NM);
	   
	   /**
	    * @ejb:persistent-field
	    * @ejb:interface-method view-type="local"
	    *
	    * @jboss:column-name name="CST_QC_EXPRY_DT"
	    **/
	   public abstract Date getCstQCExpiryDate();
	   /**
	    * @ejb:interface-method view-type="local"
	    **/
	   public abstract void setCstQCExpiryDate(Date CST_QC_EXPRY_DT);
	   
	   
	  
	   /**
	    * @ejb:persistent-field
	    * @ejb:interface-method view-type="local"
	    *
	    * @jboss:column-name name="CST_AD_BRNCH"
	    **/
	   public abstract Integer getCstAdBranch();
	   /**
	    * @ejb:interface-method view-type="local"
	    **/
	   public abstract void setCstAdBranch(Integer CST_AD_BRNCH);
	   
	   /**
	    * @ejb:persistent-field
	    * @ejb:interface-method view-type="local"
	    *
	    * @jboss:column-name name="CST_AD_CMPNY"
	    **/
	   public abstract Integer getCstAdCompany();
	   /**
	    * @ejb:interface-method view-type="local"
	    **/
	   public abstract void setCstAdCompany(Integer CST_AD_CMPNY);	   

	   // Access methods for relationship fields

	   /**
	    * @ejb:interface-method view-type="local"
	    * @ejb:relation name="itemlocation-costings"
	    *               role-name="costing-has-one-itemlocation"
	    *               cascade-delete="no"
	    *
	    * @jboss:relation related-pk-field="ilCode"
	    *                 fk-column="INV_ITEM_LOCATION"
	    */
	   public abstract LocalInvItemLocation getInvItemLocation();
	   /**
	    * @ejb:interface-method view-type="local"
	    **/	 
	   public abstract void setInvItemLocation(LocalInvItemLocation invItemLocation);
	   
	   /**
	    * @ejb:interface-method view-type="local"
	    * @ejb:relation name="stockissuanceline-costing"
	    *               role-name="costing-has-one-stockissuanceline"
	    *               cascade-delete="no"
	    *
	    * @jboss:relation related-pk-field="silCode"
	    *                 fk-column="INV_STOCK_ISSUANCE_LINE"
	    */
	   public abstract LocalInvStockIssuanceLine getInvStockIssuanceLine();
	   /**
	    * @ejb:interface-method view-type="local"
	    **/
	   public abstract void setInvStockIssuanceLine(LocalInvStockIssuanceLine invStockIssuanceLine);
	   
	   /**
	    * @ejb:interface-method view-type="local"
	    * @ejb:relation name="adjustmentline-costing"
	    *               role-name="costing-has-one-adjustmentline"
	    *               cascade-delete="yes"
	    *
	    * @jboss:relation related-pk-field="alCode"
	    *                 fk-column="INV_ADJUSTMENT_LINE"
	    */
	   public abstract LocalInvAdjustmentLine getInvAdjustmentLine();
	   /**
	    * @ejb:interface-method view-type="local"
	    **/
	   public abstract void setInvAdjustmentLine(LocalInvAdjustmentLine invAdjustmentLine);
	   
	   /**
	    * @ejb:interface-method view-type="local"
	    * @ejb:relation name="assemblytransferline-costing"
	    *               role-name="costing-has-one-assemblytransferline"
	    *               cascade-delete="yes"
	    *
	    * @jboss:relation related-pk-field="atlCode"
	    *                 fk-column="INV_ASSEMBLY_TRANSFER_LINE"
	    */
	   public abstract LocalInvAssemblyTransferLine getInvAssemblyTransferLine();
	   /**
	    * @ejb:interface-method view-type="local"
	    **/
	   public abstract void setInvAssemblyTransferLine(LocalInvAssemblyTransferLine invAssemblyTransferLine);
	   
	   /**
	    * @ejb:interface-method view-type="local"
	    * @ejb:relation name="buildunbuildassemblyline-costings"
	    *               role-name="costing-has-one-buildunbuildassemblyline"
	    *               cascade-delete="no"
	    *
	    * @jboss:relation related-pk-field="blCode"
	    *                 fk-column="INV_BUILD_UNBUILD_ASSEMBLY_LINE"
	    */
	   public abstract LocalInvBuildUnbuildAssemblyLine getInvBuildUnbuildAssemblyLine();
	   /**
	    * @ejb:interface-method view-type="local"
	    **/	   
	   public abstract void setInvBuildUnbuildAssemblyLine(LocalInvBuildUnbuildAssemblyLine invBuildUnbuildAssemblyLine);
	   	   
	   /**
	    * @ejb:interface-method view-type="local"
	    * @ejb:relation name="voucherlineitem-costings"
	    *               role-name="costing-has-one-voucherlineitem"
	    *               cascade-delete="yes"
	    *
	    * @jboss:relation related-pk-field="vliCode"
	    *                 fk-column="AP_VOUCHER_LINE_ITEM"
	    */
	   public abstract LocalApVoucherLineItem getApVoucherLineItem();
	   /**
	    * @ejb:interface-method view-type="local"
	    **/
	   public abstract void setApVoucherLineItem(LocalApVoucherLineItem apVoucherLineItem);
	   
	   /**
	    * @ejb:interface-method view-type="local"
	    * @ejb:relation name="invoicelineitem-costings"
	    *               role-name="costing-has-one-invoicelineitem"
	    *               cascade-delete="yes"
	    *
	    * @jboss:relation related-pk-field="iliCode"
	    *                 fk-column="AR_INVOICE_LINE_ITEM"
	    */
	   public abstract LocalArInvoiceLineItem getArInvoiceLineItem();
	   /**
	    * @ejb:interface-method view-type="local"
	    **/
	   public abstract void setArInvoiceLineItem(LocalArInvoiceLineItem arInvoiceLineItem);
	   
	   /**
	    * @ejb:interface-method view-type="local"
	    * @ejb:relation name="purchaseorderline-costing"
	    *               role-name="costing-has-one-purchaseorderline"
	    *               cascade-delete="yes"
	    *
	    * @jboss:relation related-pk-field="plCode"
	    *                 fk-column="AP_PURCHASE_ORDER_LINE"
	    */
	   public abstract LocalApPurchaseOrderLine getApPurchaseOrderLine();
	   /**
	    * @ejb:interface-method view-type="local"
	    **/
	   public abstract void setApPurchaseOrderLine(LocalApPurchaseOrderLine apPurchaseOrderLine);
	   
	   /**
	    * @ejb:interface-method view-type="local"
	    * @ejb:relation name="stocktransferline-costings"
	    *               role-name="costing-has-one-stocktransferline"
	    *               cascade-delete="yes"
	    *
	    * @jboss:relation related-pk-field="stlCode"
	    *                 fk-column="INV_STOCK_TRANSFER_LINE"
	    */
	   public abstract LocalInvStockTransferLine getInvStockTransferLine();
	   /**
	    * @ejb:interface-method view-type="local"
	    **/
	   public abstract void setInvStockTransferLine(LocalInvStockTransferLine invStockTransferLine);
	   
	   
	   /**
	    * @ejb:interface-method view-type="local"
	    * @ejb:relation name="branchstocktransferline-costings"
	    *               role-name="costing-has-one-branchstocktransferline"
	    *               cascade-delete="yes"
	    *
	    * @jboss:relation related-pk-field="bslCode"
	    *                 fk-column="INV_BRANCH_STOCK_TRANSFER_LINE"
	    */
	   public abstract LocalInvBranchStockTransferLine getInvBranchStockTransferLine();
	   /**
	    * @ejb:interface-method view-type="local"
	    **/
	   public abstract void setInvBranchStockTransferLine(LocalInvBranchStockTransferLine invBranchStockTransferLine);
	   	   
	   /**
	    * @ejb:interface-method view-type="local"
	    * @ejb:relation name="salesorderinvoiceline-costing"
	    *               role-name="costing-has-one-salesorderinvoiceline"
	    *               cascade-delete="yes"
	    *
	    * @jboss:relation related-pk-field="silCode"
	    *                 fk-column="AR_SALES_ORDER_INVOICE_LINE"
	    */
	   public abstract LocalArSalesOrderInvoiceLine getArSalesOrderInvoiceLine();
	   /**
	    * @ejb:interface-method view-type="local"
	    **/
	   public abstract void setArSalesOrderInvoiceLine(LocalArSalesOrderInvoiceLine arSalesOrderInvoiceLine);
	   
	   
	   /**
	    * @ejb:interface-method view-type="local"
	    * @ejb:relation name="joborderinvoiceline-costing"
	    *               role-name="costing-has-one-joborderinvoiceline"
	    *               cascade-delete="yes"
	    *
	    * @jboss:relation related-pk-field="jilCode"
	    *                 fk-column="AR_JOB_ORDER_INVOICE_LINE"
	    */
	   public abstract LocalArJobOrderInvoiceLine getArJobOrderInvoiceLine();
	   /**
	    * @ejb:interface-method view-type="local"
	    **/
	   public abstract void setArJobOrderInvoiceLine(LocalArJobOrderInvoiceLine arJobOrderInvoiceLine);
	   
	
		   		
	   public abstract LocalInvCosting ejbSelectByMaxCstLineNumberAndCstDateToLongAndIiNameAndLocName(long CST_DT_TO_LNG,
		   		java.lang.String II_NM, java.lang.String LOC_NM, java.lang.Integer CST_AD_BRNCH, java.lang.Integer CST_AD_CMPNY)
		   		throws FinderException;

	    public abstract LocalInvCosting ejbSelectGetLatestAverageCost( 
                Date CST_DT, java.lang.String II_NM, java.lang.String LOC_NM, java.lang.Integer CST_AD_BRNCH, java.lang.Integer CST_AD_CMPNY)
                throws FinderException;
       
  
	   public abstract LocalInvCosting ejbSelectByMaxCstLineNumberAndCstDateLessThanEqualCstDateAndIiNameAndLocName( 
		   		Date CST_DT, java.lang.String II_NM, java.lang.String LOC_NM, java.lang.Integer CST_AD_BRNCH, java.lang.Integer CST_AD_CMPNY)
		   		throws FinderException;
	   
	   public abstract LocalInvCosting ejbSelectByMaxCstLineNumberAndCstDateLessThanEqualCstDateAndIiNameAndLocNameAndFifoRemainingQuantity( 
		   		Date CST_DT, java.lang.String II_NM, java.lang.String LOC_NM, java.lang.Integer CST_AD_BRNCH, java.lang.Integer CST_AD_CMPNY)
		   		throws FinderException;
	   
	   


	   public abstract LocalInvCosting ejbSelectByMaxCstLineNumberAndIiNameAndLocName( 
		   		java.lang.String II_NM, java.lang.String LOC_NM, java.lang.Integer CST_AD_BRNCH, java.lang.Integer CST_AD_CMPNY)
		   		throws FinderException;
	   
	   public abstract LocalInvCosting ejbSelectByMaxCstLineNumberAndRemainingQuantityNotEqualToZeroAndIiNameAndLocName( 
		   		java.lang.String II_NM, java.lang.String LOC_NM, java.lang.Integer CST_AD_BRNCH, java.lang.Integer CST_AD_CMPNY)
		   		throws FinderException;
	   
	   public abstract LocalInvCosting ejbSelectByMaxCstLineNumberAndMaxCstDateToLongByLessThanCstDateAndIiNameAndLocName( 
			   Date CST_DT, java.lang.String II_NM, java.lang.String LOC_NM, java.lang.Integer CST_AD_BRNCH, java.lang.Integer CST_AD_CMPNY)
		   		throws FinderException;
	   
	   /****************************************************/
	   
	   public abstract LocalInvCosting ejbSelectByMaxCstDateToLongByLessThanEqualCstDateAndRemainingQuantityNotEqualToZeroAndIlCodeAndMaxLineNumber( 
			   Date CST_DT, java.lang.Integer IL_CODE, java.lang.Integer CST_AD_BRNCH, java.lang.Integer CST_AD_CMPNY)
		   		throws FinderException;
	   
	   public abstract LocalInvCosting ejbSelectByMaxCstLineNumberAndCstDateLessThanEqualCstDateAndIlCode( 
			   Date CST_DT, java.lang.Integer IL_CODE, java.lang.Integer CST_AD_BRNCH, java.lang.Integer CST_AD_CMPNY)
		   		throws FinderException;
	  

	   		
	  /**
	    * @jboss:dynamic-ql
	    */
	    public abstract Collection ejbSelectGeneric(java.lang.String jbossQl, java.lang.Object[] args)
	       throws FinderException;

	   // Business methods
	   
	   /**
	    * @ejb:home-method view-type="local"
	    */
	    public Collection ejbHomeGetCstByCriteria(java.lang.String jbossQl, java.lang.Object[] args)
	       throws FinderException {
	       	
	       return ejbSelectGeneric(jbossQl, args);
	    }
	   	    
	     /**
	      * @ejb:home-method view-type="local"
	      */
	     public LocalInvCosting ejbHomeGetByMaxCstDateToLongAndMaxCstLineNumberAndIiNameAndLocName(java.lang.String II_NM, java.lang.String LOC_NM, java.lang.Integer CST_AD_BRNCH, java.lang.Integer CST_AD_CMPNY)
	        throws FinderException {
	        
	     	try {	     	
	     	
	     	   return ejbSelectByMaxCstLineNumberAndIiNameAndLocName(II_NM, LOC_NM, CST_AD_BRNCH, CST_AD_CMPNY);
		        
	     	} catch (NullPointerException ex) {

	     		throw new FinderException(ex.getMessage());
	     		
	     	}
	        
	     }
	     
	     /**
	      * @ejb:home-method view-type="local"
	      */
	     public LocalInvCosting ejbHomeGetByMaxCstDateToLongAndMaxCstLineNumberAndRemainingQuantityNotEqualToZeroAndIiNameAndLocName(java.lang.String II_NM, java.lang.String LOC_NM, java.lang.Integer CST_AD_BRNCH, java.lang.Integer CST_AD_CMPNY)
	        throws FinderException {
	        
	     	try {	     	
	     	
	     	    return ejbSelectByMaxCstLineNumberAndRemainingQuantityNotEqualToZeroAndIiNameAndLocName(II_NM, LOC_NM, CST_AD_BRNCH, CST_AD_CMPNY);
		        
	     	} catch (NullPointerException ex) {

	     		throw new FinderException(ex.getMessage());
	     		
	     	}
	        
	     }
	     
	     /**
	      * @ejb:home-method view-type="local"
	      */
	     public LocalInvCosting ejbHomeGetByMaxCstDateToLongAndMaxCstLineNumberAndLessThanEqualCstDateAndIiNameAndLocName(java.util.Date CST_DT, java.lang.String II_NM, java.lang.String LOC_NM, java.lang.Integer CST_AD_BRNCH, java.lang.Integer CST_AD_CMPNY)
	        throws FinderException {
	        
	     	try {	     	

	     		return ejbSelectByMaxCstLineNumberAndCstDateLessThanEqualCstDateAndIiNameAndLocName(CST_DT, II_NM, LOC_NM, CST_AD_BRNCH, CST_AD_CMPNY);
		        
	     	} catch (NullPointerException ex) {

	     		throw new FinderException(ex.getMessage());
	     		
	     	}
	        
	     }
	     
	      /**
          * @ejb:home-method view-type="local"
          */
	     public LocalInvCosting ejbHomeGetLatestAverageCost(java.util.Date CST_DT, java.lang.String II_NM, java.lang.String LOC_NM, java.lang.Integer CST_AD_BRNCH, java.lang.Integer CST_AD_CMPNY)
            throws FinderException {
            
            try {           

                return ejbSelectGetLatestAverageCost(CST_DT, II_NM, LOC_NM, CST_AD_BRNCH, CST_AD_CMPNY);
                
            } catch (NullPointerException ex) {

                throw new FinderException(ex.getMessage());
                
            }
            
         }
	     
	     
	     /**
	      * @ejb:home-method view-type="local"
	      */
	     public LocalInvCosting ejbHomeGetByMaxCstDateToLongAndMaxCstLineNumberAndLessThanEqualCstDateAndIiNameAndLocNameAndFifoRemainingQuantity(java.util.Date CST_DT, java.lang.String II_NM, java.lang.String LOC_NM, java.lang.Integer CST_AD_BRNCH, java.lang.Integer CST_AD_CMPNY)
	        throws FinderException {
	        
	     	try {	     	

	     		return ejbSelectByMaxCstLineNumberAndCstDateLessThanEqualCstDateAndIiNameAndLocNameAndFifoRemainingQuantity(CST_DT, II_NM, LOC_NM, CST_AD_BRNCH, CST_AD_CMPNY);
		        
	     	} catch (NullPointerException ex) {

	     		throw new FinderException(ex.getMessage());
	     		
	     	}
	        
	     }
	     
	     
	     
		
	
	     /**
	      * @ejb:home-method view-type="local"
	      */
	     public LocalInvCosting ejbHomeGetByMaxCstDateToLongAndMaxCstLineNumberAndLessThanCstDateAndIiNameAndLocName(java.util.Date CST_DT, java.lang.String II_NM, java.lang.String LOC_NM, java.lang.Integer CST_AD_BRNCH, java.lang.Integer CST_AD_CMPNY)
	        throws FinderException {
	        
	     	try {	     	
	     		      	    
	     		return ejbSelectByMaxCstLineNumberAndMaxCstDateToLongByLessThanCstDateAndIiNameAndLocName(CST_DT, II_NM, LOC_NM, CST_AD_BRNCH, CST_AD_CMPNY);
		        
	     	} catch (NullPointerException ex) {

	     		throw new FinderException(ex.getMessage());
	     		
	     	}
	        
	     }
	     
	     /**
	      * @ejb:home-method view-type="local"
	      */
	     public LocalInvCosting ejbHomeGetByMaxCstLineNumberAndCstDateToLongAndIiNameAndLocName(long CST_DT_TO_LNG, java.lang.String II_NM, java.lang.String LOC_NM, java.lang.Integer CST_AD_BRNCH, java.lang.Integer CST_AD_CMPNY)
	        throws FinderException {

	     	try {	     	
	     		      	          	    
		         return ejbSelectByMaxCstLineNumberAndCstDateToLongAndIiNameAndLocName(CST_DT_TO_LNG, II_NM, LOC_NM, CST_AD_BRNCH, CST_AD_CMPNY);
		        
	     	} catch (NullPointerException ex) {

	     		throw new FinderException(ex.getMessage());
	     		
	     	}
	        
	     }
	     
	 
	     
	     /********************************************************************************************/
	     
	   
	     
	     
	     /**
	      * @ejb:home-method view-type="local"
	      */
	     public LocalInvCosting ejbHomeGetByMaxCstDateToLongAndMaxCstLineNumberAndLessThanEqualCstDateAndIlCode(java.util.Date CST_DT, java.lang.Integer IL_CODE, java.lang.Integer CST_AD_BRNCH, java.lang.Integer CST_AD_CMPNY)
	        throws FinderException {
	        
	     	try {	     	

	     		return ejbSelectByMaxCstLineNumberAndCstDateLessThanEqualCstDateAndIlCode(CST_DT, IL_CODE, CST_AD_BRNCH, CST_AD_CMPNY);
		        
	     	} catch (NullPointerException ex) {

	     		throw new FinderException(ex.getMessage());
	     		
	     	}
	        
	     }
	     
	     /**
	      * @ejb:home-method view-type="local" 
	      */
	     public LocalInvCosting ejbHomeGetByMaxCstDateToLongAndMaxCstLineNumberAndLessThanEqualCstDateAndRemainingQuantityNotEqualToZeroAndIlCode(java.util.Date CST_DT, java.lang.Integer IL_CODE, java.lang.Integer CST_AD_BRNCH, java.lang.Integer CST_AD_CMPNY)
	        throws FinderException {
	        
	     	try {
	     		 return ejbSelectByMaxCstDateToLongByLessThanEqualCstDateAndRemainingQuantityNotEqualToZeroAndIlCodeAndMaxLineNumber(CST_DT, IL_CODE, CST_AD_BRNCH, CST_AD_CMPNY);
		        
	     	} catch (NullPointerException ex) {

	     		throw new FinderException(ex.getMessage());
	     		
	     	}
	        
	     }
	     
	 
	     
	   
	   
	   // EntityBean methods

	   /**
	    * @ejb:create-method view-type="local"
	    **/
	   public java.lang.Integer ejbCreate (java.lang.Integer CST_CODE, java.util.Date CST_DT, long CST_DT_TO_LNG,
	   	  int CST_LN_NMBR, double CST_QTY_RCVD, double CST_ITM_CST, double CST_ASSMBLY_QTY, 
		  double CST_ASSMBLY_CST, double CST_ADJST_QTY, double CST_ADJST_CST, 
		  double CST_QTY_SLD, double CST_CST_OF_SLS, double CST_RMNNG_QTY, double CST_RMNNG_VL, double CST_VRNC_QTY,
		  double CST_VRNC_VL, double CST_RMNNG_LIFO_QTY, Integer CST_AD_BRNCH, Integer CST_AD_CMPNY)
	      throws CreateException {

	      Debug.print("invCostingBean ejbCreate");
	      setCstCode(CST_CODE);
	      setCstDate(CST_DT);
	      setCstDateToLong(CST_DT_TO_LNG);
	      setCstLineNumber(CST_LN_NMBR);
	      setCstQuantityReceived(CST_QTY_RCVD);
	      setCstItemCost(CST_ITM_CST);
	      setCstAssemblyQuantity(CST_ASSMBLY_QTY);
	      setCstAssemblyCost(CST_ASSMBLY_CST);
	      setCstAdjustQuantity(CST_ADJST_QTY);
	      setCstAdjustCost(CST_ADJST_CST);
	      setCstQuantitySold(CST_QTY_SLD);
	      setCstCostOfSales(CST_CST_OF_SLS);
	      setCstRemainingQuantity(CST_RMNNG_QTY);
	      setCstRemainingValue(CST_RMNNG_VL);
	      setCstVarianceQuantity(CST_VRNC_QTY);
	      setCstVarianceValue(CST_VRNC_VL);
	      setCstRemainingLifoQuantity(CST_RMNNG_LIFO_QTY);
	      setCstAdBranch(CST_AD_BRNCH);
	      setCstAdCompany(CST_AD_CMPNY);
	      return null;
	   }

	   /**
	    * @ejb:create-method view-type="local"
	    **/
	   public java.lang.Integer ejbCreate (java.util.Date CST_DT, long CST_DT_TO_LNG,
	   	  int CST_LN_NMBR, double CST_QTY_RCVD, double CST_ITM_CST, double CST_ASSMBLY_QTY, 
		  double CST_ASSMBLY_CST, double CST_ADJST_QTY, double CST_ADJST_CST, 
		  double CST_QTY_SLD, double CST_CST_OF_SLS, double CST_RMNNG_QTY, double CST_RMNNG_VL, double CST_VRNC_QTY,
		  double CST_VRNC_VL, double CST_RMNNG_LIFO_QTY, Integer CST_AD_BRNCH, Integer CST_AD_CMPNY)
	      throws CreateException {

	      Debug.print("invCostingBean ejbCreate");
	      setCstDate(CST_DT);
	      setCstDateToLong(CST_DT_TO_LNG);
	      setCstLineNumber(CST_LN_NMBR);
	      setCstQuantityReceived(CST_QTY_RCVD);
	      setCstItemCost(CST_ITM_CST);
	      setCstAssemblyQuantity(CST_ASSMBLY_QTY);
	      setCstAssemblyCost(CST_ASSMBLY_CST);
	      setCstAdjustQuantity(CST_ADJST_QTY);
	      setCstAdjustCost(CST_ADJST_CST);
	      setCstQuantitySold(CST_QTY_SLD);
	      setCstCostOfSales(CST_CST_OF_SLS);
	      setCstRemainingQuantity(CST_RMNNG_QTY);
	      setCstRemainingValue(CST_RMNNG_VL);
	      setCstVarianceQuantity(CST_VRNC_QTY);
	      setCstVarianceValue(CST_VRNC_VL);
	      setCstRemainingLifoQuantity(CST_RMNNG_LIFO_QTY);
	      setCstAdBranch(CST_AD_BRNCH);
	      setCstAdCompany(CST_AD_CMPNY);
	      return null;
	   }

	   public void ejbPostCreate (java.lang.Integer CST_CODE, java.util.Date CST_DT, long CST_DT_TO_LNG,
		   	  int CST_LN_NMBR, double CST_QTY_RCVD, double CST_ITM_CST, double CST_ASSMBLY_QTY, 
			  double CST_ASSMBLY_CST, double CST_ADJST_QTY, double CST_ADJST_CST, 
			  double CST_QTY_SLD, double CST_CST_OF_SLS, double CST_RMNNG_QTY, double CST_RMNNG_VL, double CST_VRNC_QTY,
			  double CST_VRNC_VL, double CST_RMNNG_LIFO_QTY, Integer CST_AD_BRNCH, Integer CST_AD_CMPNY)
	      throws CreateException { }

	   public void ejbPostCreate (java.util.Date CST_DT, long CST_DT_TO_LNG,
		   	  int CST_LN_NMBR, double CST_QTY_RCVD, double CST_ITM_CST, double CST_ASSMBLY_QTY, 
			  double CST_ASSMBLY_CST, double CST_ADJST_QTY, double CST_ADJST_CST, 
			  double CST_QTY_SLD, double CST_CST_OF_SLS, double CST_RMNNG_QTY, double CST_RMNNG_VL, double CST_VRNC_QTY,
			  double CST_VRNC_VL, double CST_RMNNG_LIFO_QTY, Integer CST_AD_BRNCH, Integer CST_AD_CMPNY)
	      throws CreateException { }
	
}
