package com.ejb.inv;

import java.util.Collection;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;

import com.ejb.gl.LocalGlChartOfAccount;
import com.ejb.inv.LocalInvTag;
import com.util.AbstractEntityBean;
import com.util.Debug;

/**
 * @ejb:bean name="InvDepreciationLedgerBeanEJB"
 *           display-name="DepreciationLedger Entity"
 *           type="CMP"
 *           cmp-version="2.x"
 *           view-type="local"
 *           local-jndi-name="omega-ejb/InvDepreciationLedgerBeanEJB"
 *           schema="InvDepreciationLedger"
 *           primkey-field="dlCode"
 *       
 * @ejb:pk class="java.lang.Integer"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="invuser"
 *                        role-link="invuserlink"
 *
 * @ejb:permission role-name="invuser"
 *
 * @ejb:interface local-class="com.ejb.inv.LocalInvDepreciationLedger"
 *                local-extends="javax.ejb.EJBLocalObject"
 *
 * @ejb:home local-class="com.ejb.inv.LocalInvDepreciationLedgerHome"
 *           local-extends="javax.ejb.EJBLocalHome"
 * 
 *                                              
 * @ejb:select signature="Collection ejbSelectGeneric(java.lang.String jbossQl, java.lang.Object[] args)"
 *             result-type-mapping="Local"
 *             method-intf="LocalHome"
 *             query="SELECT OBJECT(dl) FROM InvDepreciationLedgerBean dl"
 *
 * @jboss:query signature="Collection ejbSelectGeneric(java.lang.String jbossQl, java.lang.Object[] args)"
 *                         dynamic="true"
 *            
 * @ejb:finder signature="LocalInvDepreciationLedger findExistingDLbyDateAndInvTag(java.util.Date DL_DT, java.lang.Integer TG_CD, java.lang.Integer TG_AD_CMPNY)"
 *             query="SELECT OBJECT(dl) FROM InvDepreciationLedger dl WHERE dl.dlDate=?1 AND dl.invTag.tgCode=?2 AND dl.dlAdCompany=?3" 
 *              
 * @ejb:value-object match="*"
 *             name="InvDepreciationLedger"
 *
 * @jboss:persistence table-name="INV_DPRCTN_LDGR"
 *
 * @jboss:entity-command name="mysql-get-generated-keys"
 *
 */

public abstract class InvDepreciationLedgerBean extends AbstractEntityBean {
	
//	 Access methods for persistent fields

	   /**
	    * @ejb:persistent-field
	    * @ejb:interface-method view-type="local"
	    * @ejb:pk-field
	    *
	    * @jboss:column-name name="DL_CODE"
	    *
	    * @jboss:persistence auto-increment="true"
	    **/
	   public abstract java.lang.Integer getDlCode();
	   public abstract void setDlCode(java.lang.Integer DL_CODE);
	   
       
       /**
        * @ejb:persistent-field
        * @ejb:interface-method view-type="local"
        *
        * @jboss:column-name name="DL_DT"
        **/
       public abstract java.util.Date getDlDate();
       /**
        * @ejb:interface-method view-type="local"
        **/
       public abstract void setDlDate(java.util.Date DL_DT);
       
       
       /**
	    * @ejb:persistent-field
	    * @ejb:interface-method view-type="local"
	    *
	    * @jboss:column-name name="DL_ACQSTN_CST"
	    **/
	   public abstract double getDlAcquisitionCost();
	   /**
	    * @ejb:interface-method view-type="local"
	    **/
	   public abstract void setDlAcquisitionCost(double DL_ACQSTN_CST);
	   
	   
	   /**
	    * @ejb:persistent-field
	    * @ejb:interface-method view-type="local"
	    *
	    * @jboss:column-name name="DL_DPRCTN_AMT"
	    **/
	   public abstract double getDlDepreciationAmount();
	   /**
	    * @ejb:interface-method view-type="local"
	    **/
	   public abstract void setDlDepreciationAmount(double DL_DPRCTN_AMT);
	   
	   
	   /**
	    * @ejb:persistent-field
	    * @ejb:interface-method view-type="local"
	    *
	    * @jboss:column-name name="DL_MNTH_LF_SPN"
	    **/
	   public abstract double getDlMonthLifeSpan();
	   /**
	    * @ejb:interface-method view-type="local"
	    **/
	   public abstract void setDlMonthLifeSpan(double DL_MNTH_LF_SPN);
	   
	   
	   /**
	    * @ejb:persistent-field
	    * @ejb:interface-method view-type="local"
	    *
	    * @jboss:column-name name="DL_CRRNT_BLNC"
	    **/
	   public abstract double getDlCurrentBalance();
	   /**
	    * @ejb:interface-method view-type="local"
	    **/
	   public abstract void setDlCurrentBalance(double DL_CRRNT_BLNC);
	   
       
       /**
        * @ejb:persistent-field
        * @ejb:interface-method view-type="local"
        *
        * @jboss:column-name name="DL_AD_CMPNY"
        **/
       public abstract Integer getDlAdCompany();
       /**
        * @ejb:interface-method view-type="local"
        **/
       public abstract void setDlAdCompany(Integer DL_AD_CMPNY);  

    // RELATIONSHIP

      /**
	    * @ejb:interface-method view-type="local"
	    * @ejb:relation name="depreciationledger-tags"
	    *               role-name="depreciationledger-has-one-tag"
	    *				cascade-delete="yes"
	    *
	    *
	    * @jboss:relation related-pk-field="tgCode"
	    *                 fk-column="INV_TAG"              
	    */
	   public abstract LocalInvTag getInvTag();
	   /**
	    * @ejb:interface-method view-type="local"
	    **/
	   public abstract void setInvTag(LocalInvTag invTag);
       

	   /**
		 * @jboss:dynamic-ql
		 */
		public abstract Collection ejbSelectGeneric(java.lang.String jbossQl, java.lang.Object[] args)
		throws FinderException;
		
		// BUSINESS METHODS

	    /**
		 * @ejb:home-method view-type="local"
		 */
		public Collection ejbHomeGetTgByCriteria(java.lang.String jbossQl, java.lang.Object[] args)
		throws FinderException {
			
			return ejbSelectGeneric(jbossQl, args);
			
		}
		
	   // EntityBean methods

	   /**
	    * @ejb:create-method view-type="local"
	    **/
	   
	   
	   public java.lang.Integer ejbCreate (Integer DL_CODE, java.util.Date DL_DT, Double DL_ACQSTN_CST, Double DL_DPRCTN_AMT, Double DL_MNTH_LF_SPN, Double DL_CRRNT_BLNC, Integer DL_AD_CMPNY)
	      throws CreateException {

	      Debug.print("InvDepreciationLedgerBean ejbCreate");
	      
	      setDlCode(DL_CODE);
	      setDlDate(DL_DT);
	      setDlAcquisitionCost(DL_ACQSTN_CST);
	      setDlDepreciationAmount(DL_DPRCTN_AMT);
	      setDlMonthLifeSpan(DL_MNTH_LF_SPN);
	      setDlCurrentBalance(DL_CRRNT_BLNC);
	      setDlAdCompany(DL_AD_CMPNY);

	      return null;
	   }
	   
	   /**
	    * @ejb:create-method view-type="local"
	    **/
	   
	   
	   public java.lang.Integer ejbCreate (java.util.Date DL_DT, Double DL_ACQSTN_CST, Double DL_DPRCTN_AMT, Double DL_MNTH_LF_SPN, Double DL_CRRNT_BLNC, Integer DL_AD_CMPNY)
	      throws CreateException {

	      Debug.print("InvDepreciationLedgerBean ejbCreate");
	      
	      setDlDate(DL_DT);
	      setDlAcquisitionCost(DL_ACQSTN_CST);
	      setDlDepreciationAmount(DL_DPRCTN_AMT);
	      setDlMonthLifeSpan(DL_MNTH_LF_SPN);
	      setDlCurrentBalance(DL_CRRNT_BLNC);
	      setDlAdCompany(DL_AD_CMPNY);

	      return null;
	   }
	   

	   public void ejbPostCreate (Integer DL_CODE, java.util.Date DL_DT, Double DL_ACQSTN_CST, Double DL_DPRCTN_AMT, Double DL_MNTH_LF_SPN, Double DL_CRRNT_BLNC, Integer DL_AD_CMPNY)
	      throws CreateException { }

	   public void ejbPostCreate (java.util.Date DL_DT, Double DL_ACQSTN_CST, Double DL_DPRCTN_AMT, Double DL_MNTH_LF_SPN, Double DL_CRRNT_BLNC, Integer DL_AD_CMPNY)
	      throws CreateException { }
	
}
