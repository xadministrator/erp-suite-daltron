package com.ejb.inv;

import javax.ejb.CreateException;

import com.util.AbstractEntityBean;
import com.util.Debug;

/**
 * @ejb:bean name="InvDistributionRecordEJB"
 *           display-name="Distribution Record Entity"
 *           type="CMP"
 *           cmp-version="2.x"
 *           view-type="local"
 *           local-jndi-name="omega-ejb/InvDistributionRecordEJB"
 *           schema="InvDistributionRecord"
 *           primkey-field="drCode"
 *       
 * @ejb:pk class="java.lang.Integer"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="invuser"
 *                        role-link="invuserlink"
 *
 * @ejb:permission role-name="invuser"
 *
 * @ejb:interface local-class="com.ejb.inv.LocalInvDistributionRecord"
 *                local-extends="javax.ejb.EJBLocalObject"
 *
 * @ejb:home local-class="com.ejb.inv.LocalInvDistributionRecordHome"
 *           local-extends="javax.ejb.EJBLocalHome"
 * 
 * @ejb:finder signature="LocalInvDistributionRecord findByDrClassAndAdjCode(java.lang.String DR_CLSS, java.lang.Integer ADJ_CODE, java.lang.Integer DR_AD_CMPNY)"
 *             query="SELECT OBJECT(dr) FROM InvDistributionRecord dr WHERE dr.drClass=?1 AND dr.invAdjustment.adjCode=?2 AND dr.drAdCompany=?3"
 * 
 * @ejb:finder signature="LocalInvDistributionRecord findByAdjCodeCredit(java.lang.Integer ADJ_CODE, java.lang.Integer DR_AD_CMPNY)"
 *             query="SELECT OBJECT(dr) FROM InvDistributionRecord dr WHERE dr.invAdjustment.adjCode=?1 AND dr.drAdCompany=?2 and dr.drDebit=0 "
 * 
 * @ejb:finder signature="Collection findImportableDrByAdjCode(java.lang.Integer ADJ_CODE, java.lang.Integer DR_AD_CMPNY)"
 *             query="SELECT OBJECT(dr) FROM InvAdjustment adj, IN(adj.invDistributionRecords) dr WHERE dr.drImported = 0 AND adj.adjCode = ?1 AND dr.drAdCompany=?2"
 *
 * @ejb:finder signature="Collection findImportableDrByAtrCode(java.lang.Integer ATR_CODE, java.lang.Integer DR_AD_CMPNY)"
 *             query="SELECT OBJECT(dr) FROM InvAssemblyTransfer atr, IN(atr.invDistributionRecords) dr WHERE dr.drImported = 0 AND atr.atrCode = ?1 AND dr.drAdCompany=?2"
 * 
 * @jboss:query signature="Collection findImportableDrByAtrCode(java.lang.Integer ATR_CODE, java.lang.Integer DR_AD_CMPNY)"
 *             query="SELECT OBJECT(dr) FROM InvAssemblyTransfer atr, IN(atr.invDistributionRecords) dr WHERE dr.drImported = 0 AND atr.atrCode = ?1 AND dr.drAdCompany=?2 ORDER BY dr.drLine"
 *
 * @ejb:finder signature="Collection findImportableDrByBuaCode(java.lang.Integer BUA_CODE, java.lang.Integer DR_AD_CMPNY)"
 *             query="SELECT OBJECT(dr) FROM InvBuildUnbuildAssembly bua, IN(bua.invDistributionRecords) dr WHERE dr.drImported = 0 AND bua.buaCode = ?1 AND dr.drAdCompany=?2"
 *
 * @jboss:query signature="Collection findImportableDrByBuaCode(java.lang.Integer BUA_CODE, java.lang.Integer DR_AD_CMPNY)"
 *             query="SELECT OBJECT(dr) FROM InvBuildUnbuildAssembly bua, IN(bua.invDistributionRecords) dr WHERE dr.drImported = 0 AND bua.buaCode = ?1 AND dr.drAdCompany=?2 ORDER BY dr.drLine"
 *
 * @ejb:finder signature="Collection findImportableDrByOhCode(java.lang.Integer OH_CODE, java.lang.Integer OH_AD_CMPNY)"
 *             query="SELECT OBJECT(dr) FROM InvOverhead oh, IN(oh.invDistributionRecords) dr WHERE dr.drImported = 0 AND oh.ohCode = ?1 AND oh.ohAdCompany = ?2"
 *
 * @jboss:query signature="Collection findImportableDrByOhCode(java.lang.Integer OH_CODE, java.lang.Integer OH_AD_CMPNY)"
 *             query="SELECT OBJECT(dr) FROM InvOverhead oh, IN(oh.invDistributionRecords) dr WHERE dr.drImported = 0 AND oh.ohCode = ?1 AND oh.ohAdCompany = ?2 ORDER BY dr.drLine"
 *             
 * @ejb:finder signature="Collection findImportableDrBySiCode(java.lang.Integer SI_CODE, java.lang.Integer SI_AD_CMPNY)"
 *             query="SELECT OBJECT(dr) FROM InvStockIssuance si, IN(si.invDistributionRecords) dr WHERE dr.drImported = 0 AND si.siCode = ?1 AND si.siAdCompany = ?2"
 *             
 * @ejb:finder signature="Collection findImportableDrBySiCode(java.lang.Integer SI_CODE, java.lang.Integer SI_AD_CMPNY)"
 *             query="SELECT OBJECT(dr) FROM InvStockIssuance si, IN(si.invDistributionRecords) dr WHERE dr.drImported = 0 AND si.siCode = ?1 AND si.siAdCompany = ?2 ORDER BY dr.drLine"
 * 
 * @ejb:finder signature="Collection findImportableDrByStCode(java.lang.Integer ST_CODE, java.lang.Integer DR_AD_CMPNY)"
 *             query="SELECT OBJECT(dr) FROM InvStockTransfer st, IN(st.invDistributionRecords) dr WHERE dr.drImported = 0 AND st.stCode = ?1 AND dr.drAdCompany=?2"
 * 
 * @jboss:query signature="Collection findImportableDrByStCode(java.lang.Integer ST_CODE, java.lang.Integer DR_AD_CMPNY)"
 *             query="SELECT OBJECT(dr) FROM InvStockTransfer st, IN(st.invDistributionRecords) dr WHERE dr.drImported = 0 AND st.stCode = ?1 AND dr.drAdCompany=?2 ORDER BY dr.drLine"
 *             
 * @ejb:finder signature="Collection findImportableDrByBstCode(java.lang.Integer BST_CODE, java.lang.Integer DR_AD_CMPNY)"
 *             query="SELECT OBJECT(dr) FROM InvDistributionRecord dr WHERE dr.drImported = 0 AND dr.invBranchStockTransfer.bstCode = ?1 AND dr.drAdCompany=?2"
 *               
 * @jboss:query signature="Collection findImportableDrByBstCode(java.lang.Integer BST_CODE, java.lang.Integer DR_AD_CMPNY)"
 *             query="SELECT OBJECT(dr) FROM InvDistributionRecord dr WHERE dr.drImported = 0 AND dr.invBranchStockTransfer.bstCode = ?1 AND dr.drAdCompany=?2 ORDER BY dr.drLine"
 *
 * @ejb:finder signature="Collection findByAdjCode(java.lang.Integer ADJ_CODE, java.lang.Integer DR_AD_CMPNY)"
 *             query="SELECT OBJECT(dr) FROM InvAdjustment adj, IN(adj.invDistributionRecords) dr WHERE adj.adjCode =?1 AND dr.drAdCompany=?2"
 *
 * @jboss:query signature="Collection findByAdjCode(java.lang.Integer ADJ_CODE, java.lang.Integer DR_AD_CMPNY)"
 *             query="SELECT OBJECT(dr) FROM InvAdjustment adj, IN(adj.invDistributionRecords) dr WHERE adj.adjCode =?1 AND dr.drAdCompany=?2 ORDER BY dr.drDebit DESC"
 *
 * @ejb:finder signature="Collection findByBuaCode(java.lang.Integer BUA_CODE, java.lang.Integer DR_AD_CMPNY)"
 *             query="SELECT OBJECT(dr) FROM InvBuildUnbuildAssembly bua, IN(bua.invDistributionRecords) dr WHERE bua.buaCode =?1 AND dr.drAdCompany=?2"
 *
 * @jboss:query signature="Collection findByBuaCode(java.lang.Integer BUA_CODE, java.lang.Integer DR_AD_CMPNY)"
 *             query="SELECT OBJECT(dr) FROM InvBuildUnbuildAssembly bua, IN(bua.invDistributionRecords) dr WHERE bua.buaCode =?1 AND dr.drAdCompany=?2 ORDER BY dr.drDebit DESC"
 *
 * @ejb:finder signature="Collection findByOhCode(java.lang.Integer OH_CODE, java.lang.Integer DR_AD_CMPNY)"
 *             query="SELECT OBJECT(dr) FROM InvOverhead oh, IN(oh.invDistributionRecords) dr WHERE oh.ohCode =?1 AND dr.drAdCompany=?2"
 *
 * @jboss:query signature="Collection findByOhCode(java.lang.Integer OH_CODE, java.lang.Integer DR_AD_CMPNY)"
 *             query="SELECT OBJECT(dr) FROM InvOverhead oh, IN(oh.invDistributionRecords) dr WHERE oh.ohCode =?1 AND dr.drAdCompany=?2 ORDER BY dr.drDebit DESC"
 * 
 * @ejb:finder signature="Collection findBySiCode(java.lang.Integer SI_CODE, java.lang.Integer DR_AD_CMPNY)"
 *             query="SELECT OBJECT(dr) FROM InvStockIssuance si, IN(si.invDistributionRecords) dr WHERE si.siCode =?1 AND dr.drAdCompany=?2"
 * 
 * @jboss:query signature="Collection findBySiCode(java.lang.Integer SI_CODE, java.lang.Integer DR_AD_CMPNY)"
 *             query="SELECT OBJECT(dr) FROM InvStockIssuance si, IN(si.invDistributionRecords) dr WHERE si.siCode =?1 AND dr.drAdCompany=?2 ORDER BY dr.drDebit DESC"
 * 
 * @ejb:finder signature="Collection findByAtrCode(java.lang.Integer ATR_CODE, java.lang.Integer DR_AD_CMPNY)"
 *             query="SELECT OBJECT(dr) FROM InvAssemblyTransfer atr, IN(atr.invDistributionRecords) dr WHERE atr.atrCode =?1 AND dr.drAdCompany=?2"
 * 
 * @jboss:query signature="Collection findByAtrCode(java.lang.Integer ATR_CODE, java.lang.Integer DR_AD_CMPNY)"
 *             query="SELECT OBJECT(dr) FROM InvAssemblyTransfer atr, IN(atr.invDistributionRecords) dr WHERE atr.atrCode =?1 AND dr.drAdCompany=?2 ORDER BY dr.drDebit DESC"
 * 
 * @ejb:finder signature="Collection findByStCode(java.lang.Integer ST_CODE, java.lang.Integer DR_AD_CMPNY)"
 *             query="SELECT OBJECT(dr) FROM InvStockTransfer st, IN(st.invDistributionRecords) dr WHERE st.stCode =?1 AND dr.drAdCompany=?2"
 * 
 * @jboss:query signature="Collection findByStCode(java.lang.Integer ST_CODE, java.lang.Integer DR_AD_CMPNY)"
 *             query="SELECT OBJECT(dr) FROM InvStockTransfer st, IN(st.invDistributionRecords) dr WHERE st.stCode =?1 AND dr.drAdCompany=?2 ORDER BY dr.drDebit DESC"
 *
 * @ejb:finder signature="Collection findUnpostedAdjByDateRangeAndCoaAccountNumber(java.util.Date ADJ_DT_FRM, java.util.Date ADJ_DT_TO, java.lang.Integer COA_CODE, java.lang.Integer ADJ_AD_BRNCH, java.lang.Integer DR_AD_CMPNY)"
 *             query="SELECT OBJECT(dr) FROM InvAdjustment adj, IN(adj.invDistributionRecords) dr WHERE adj.adjVoid=0 AND adj.adjPosted=0 AND adj.adjDate>=?1 AND adj.adjDate<=?2 AND dr.invChartOfAccount.coaCode=?3 AND adj.adjAdBranch=?4 AND dr.drAdCompany=?5"
 *
 * @jboss:query signature="Collection findUnpostedAdjByDateRangeAndCoaAccountNumber(java.util.Date ADJ_DT_FRM, java.util.Date ADJ_DT_TO, java.lang.Integer COA_CODE, java.lang.Integer ADJ_AD_BRNCH, java.lang.Integer DR_AD_CMPNY)"
 *             query="SELECT OBJECT(dr) FROM InvAdjustment adj, IN(adj.invDistributionRecords) dr WHERE adj.adjVoid=0 AND adj.adjPosted=0 AND adj.adjDate>=?1 AND adj.adjDate<=?2 AND dr.invChartOfAccount.coaCode=?3 AND adj.adjAdBranch=?4 AND dr.drAdCompany=?5 ORDER BY dr.drLine"
 *
 * @ejb:finder signature="Collection findUnpostedBuaByDateRangeAndCoaAccountNumber(java.util.Date BUA_DT_FRM, java.util.Date BUA_DT_TO, java.lang.Integer COA_CODE, java.lang.Integer BUA_AD_BRNCH, java.lang.Integer DR_AD_CMPNY)"
 *             query="SELECT OBJECT(dr) FROM InvBuildUnbuildAssembly bua, IN(bua.invDistributionRecords) dr WHERE bua.buaVoid=0 AND bua.buaPosted=0 AND bua.buaDate>=?1 AND bua.buaDate<=?2 AND dr.invChartOfAccount.coaCode=?3 AND bua.buaAdBranch=?4 AND dr.drAdCompany=?5"
 *
 * @jboss:query signature="Collection findUnpostedBuaByDateRangeAndCoaAccountNumber(java.util.Date BUA_DT_FRM, java.util.Date BUA_DT_TO, java.lang.Integer COA_CODE, java.lang.Integer BUA_AD_BRNCH, java.lang.Integer DR_AD_CMPNY)"
 *             query="SELECT OBJECT(dr) FROM InvBuildUnbuildAssembly bua, IN(bua.invDistributionRecords) dr WHERE bua.buaVoid=0 AND bua.buaPosted=0 AND bua.buaDate>=?1 AND bua.buaDate<=?2 AND dr.invChartOfAccount.coaCode=?3 AND bua.buaAdBranch=?4 AND dr.drAdCompany=?5 ORDER BY dr.drLine"
 *
 * @ejb:finder signature="Collection findUnpostedOhByDateRangeAndCoaAccountNumber(java.util.Date OH_DT_FRM, java.util.Date OH_DT_TO, java.lang.Integer COA_CODE, java.lang.Integer OH_AD_BRNCH, java.lang.Integer DR_AD_CMPNY)"
 *             query="SELECT OBJECT(dr) FROM InvOverhead oh, IN(oh.invDistributionRecords) dr WHERE oh.ohPosted=0 AND oh.ohDate>=?1 AND oh.ohDate<=?2 AND dr.invChartOfAccount.coaCode=?3 AND oh.ohAdBranch=?4 AND dr.drAdCompany=?5"
 *
 * @jboss:query signature="Collection findUnpostedOhByDateRangeAndCoaAccountNumber(java.util.Date OH_DT_FRM, java.util.Date OH_DT_TO, java.lang.Integer COA_CODE, java.lang.Integer OH_AD_BRNCH, java.lang.Integer DR_AD_CMPNY)"
 *             query="SELECT OBJECT(dr) FROM InvOverhead oh, IN(oh.invDistributionRecords) dr WHERE oh.ohPosted=0 AND oh.ohDate>=?1 AND oh.ohDate<=?2 AND dr.invChartOfAccount.coaCode=?3 AND oh.ohAdBranch=?4 AND dr.drAdCompany=?5 ORDER BY dr.drLine"
 * 
 * @ejb:finder signature="Collection findUnpostedSiByDateRangeAndCoaAccountNumber(java.util.Date SI_DT_FRM, java.util.Date SI_DT_TO, java.lang.Integer COA_CODE, java.lang.Integer SI_AD_BRNCH, java.lang.Integer DR_AD_CMPNY)"
 *             query="SELECT OBJECT(dr) FROM InvStockIssuance si, IN(si.invDistributionRecords) dr WHERE si.siVoid=0 AND si.siPosted=0 AND si.siDate>=?1 AND si.siDate<=?2 AND dr.invChartOfAccount.coaCode=?3 AND si.siAdBranch=?4 AND dr.drAdCompany=?5"
 * 
 * @jboss:query signature="Collection findUnpostedSiByDateRangeAndCoaAccountNumber(java.util.Date SI_DT_FRM, java.util.Date SI_DT_TO, java.lang.Integer COA_CODE, java.lang.Integer SI_AD_BRNCH, java.lang.Integer DR_AD_CMPNY)"
 *             query="SELECT OBJECT(dr) FROM InvStockIssuance si, IN(si.invDistributionRecords) dr WHERE si.siVoid=0 AND si.siPosted=0 AND si.siDate>=?1 AND si.siDate<=?2 AND dr.invChartOfAccount.coaCode=?3 AND si.siAdBranch=?4 AND dr.drAdCompany=?5 ORDER BY dr.drLine"
 * 
 * @ejb:finder signature="Collection findUnpostedAtrByDateRangeAndCoaAccountNumber(java.util.Date ATR_DT_FRM, java.util.Date ATR_DT_TO, java.lang.Integer COA_CODE, java.lang.Integer ATR_AD_BRNCH, java.lang.Integer DR_AD_CMPNY)"
 *             query="SELECT OBJECT(dr) FROM InvAssemblyTransfer atr, IN(atr.invDistributionRecords) dr WHERE atr.atrVoid=0 AND atr.atrPosted=0 AND atr.atrDate>=?1 AND atr.atrDate<=?2 AND dr.invChartOfAccount.coaCode=?3 AND atr.atrAdBranch=?4 AND dr.drAdCompany=?5"
 * 
 * @jboss:query signature="Collection findUnpostedAtrByDateRangeAndCoaAccountNumber(java.util.Date ATR_DT_FRM, java.util.Date ATR_DT_TO, java.lang.Integer COA_CODE, java.lang.Integer ATR_AD_BRNCH, java.lang.Integer DR_AD_CMPNY)"
 *             query="SELECT OBJECT(dr) FROM InvAssemblyTransfer atr, IN(atr.invDistributionRecords) dr WHERE atr.atrVoid=0 AND atr.atrPosted=0 AND atr.atrDate>=?1 AND atr.atrDate<=?2 AND dr.invChartOfAccount.coaCode=?3 AND atr.atrAdBranch=?4 AND dr.drAdCompany=?5 ORDER BY dr.drLine"
 * 
 * @ejb:finder signature="Collection findUnpostedStByDateRangeAndCoaAccountNumber(java.util.Date ST_DT_FRM, java.util.Date ST_DT_TO, java.lang.Integer COA_CODE, java.lang.Integer ST_AD_BRNCH, java.lang.Integer DR_AD_CMPNY)"
 *             query="SELECT OBJECT(dr) FROM InvStockTransfer st, IN(st.invDistributionRecords) dr WHERE st.stPosted=0 AND st.stDate>=?1 AND st.stDate<=?2 AND dr.invChartOfAccount.coaCode=?3 AND st.stAdBranch=?4 AND dr.drAdCompany=?5"
 * 
 * @jboss:query signature="Collection findUnpostedStByDateRangeAndCoaAccountNumber(java.util.Date ST_DT_FRM, java.util.Date ST_DT_TO, java.lang.Integer COA_CODE, java.lang.Integer ST_AD_BRNCH, java.lang.Integer DR_AD_CMPNY)"
 *             query="SELECT OBJECT(dr) FROM InvStockTransfer st, IN(st.invDistributionRecords) dr WHERE st.stPosted=0 AND st.stDate>=?1 AND st.stDate<=?2 AND dr.invChartOfAccount.coaCode=?3 AND st.stAdBranch=?4 AND dr.drAdCompany=?5 ORDER BY dr.drLine"
 *
 * @ejb:finder signature="Collection findByBstCode(java.lang.Integer BST_CODE, java.lang.Integer DR_AD_CMPNY)"
 *             query="SELECT OBJECT(dr) FROM InvBranchStockTransfer bst, IN(bst.invDistributionRecords) dr WHERE bst.bstCode = ?1 AND dr.drAdCompany=?2"
 * 
 * @jboss:query signature="Collection findByBstCode(java.lang.Integer ST_CODE, java.lang.Integer DR_AD_CMPNY)"
 *             query="SELECT OBJECT(dr) FROM InvBranchStockTransfer bst, IN(bst.invDistributionRecords) dr WHERE bst.bstCode = ?1 AND dr.drAdCompany=?2 ORDER BY dr.drDebit DESC"
 * 

 * @ejb:finder signature="Collection findUnpostedAdjByDrCoaAccountNumberAndBrCode(java.lang.Integer COA_CODE, java.lang.Integer ADJ_AD_BRNCH, java.lang.Integer DR_AD_CMPNY)"
 *             query="SELECT OBJECT(dr) FROM InvAdjustment adj, IN(adj.invDistributionRecords) dr WHERE adj.adjPosted=0 AND dr.invChartOfAccount.coaCode=?1 AND adj.adjAdBranch = ?2 AND dr.drAdCompany = ?3"
 *
 * @ejb:finder signature="Collection findUnpostedBuaByDrCoaAccountNumberAndBrCode(java.lang.Integer COA_CODE, java.lang.Integer BUA_AD_BRNCH, java.lang.Integer DR_AD_CMPNY)"
 *             query="SELECT OBJECT(dr) FROM InvBuildUnbuildAssembly bua, IN(bua.invDistributionRecords) dr WHERE bua.buaPosted=0 AND dr.invChartOfAccount.coaCode=?1 AND bua.buaAdBranch = ?2 AND dr.drAdCompany = ?3"
 * 
 * @ejb:finder signature="Collection findUnpostedSiByDrCoaAccountNumberAndBrCode(java.lang.Integer COA_CODE, java.lang.Integer SI_AD_BRNCH, java.lang.Integer DR_AD_CMPNY)"
 *             query="SELECT OBJECT(dr) FROM InvStockIssuance si, IN(si.invDistributionRecords) dr WHERE si.siPosted=0 AND dr.invChartOfAccount.coaCode=?1 AND si.siAdBranch = ?2 AND dr.drAdCompany = ?3"
 *
 * @ejb:finder signature="Collection findUnpostedAtrByDrCoaAccountNumberAndBrCode(java.lang.Integer COA_CODE, java.lang.Integer ATR_AD_BRNCH, java.lang.Integer DR_AD_CMPNY)"
 *             query="SELECT OBJECT(dr) FROM InvAssemblyTransfer atr, IN(atr.invDistributionRecords) dr WHERE atr.atrPosted=0 AND dr.invChartOfAccount.coaCode=?1 AND atr.atrAdBranch = ?2 AND dr.drAdCompany = ?3"
 *
 * @ejb:finder signature="Collection findUnpostedStByDrCoaAccountNumberAndBrCode(java.lang.Integer COA_CODE, java.lang.Integer ST_AD_BRNCH, java.lang.Integer DR_AD_CMPNY)"
 *             query="SELECT OBJECT(dr) FROM InvStockTransfer st, IN(st.invDistributionRecords) dr WHERE st.stPosted=0 AND dr.invChartOfAccount.coaCode=?1 AND st.stAdBranch = ?2 AND dr.drAdCompany = ?3"
 * 
 * @ejb:finder signature="Collection findUnpostedBstByDrCoaAccountNumberAndBrCode(java.lang.Integer COA_CODE, java.lang.Integer BST_AD_BRNCH, java.lang.Integer DR_AD_CMPNY)"
 *             query="SELECT OBJECT(dr) FROM InvBranchStockTransfer bst, IN(bst.invDistributionRecords) dr WHERE bst.bstPosted=0 AND dr.invChartOfAccount.coaCode=?1 AND bst.bstAdBranch = ?2 AND dr.drAdCompany = ?3"
 *
 * @ejb:finder signature="Collection findUnpostedOhByDrCoaAccountNumberAndBrCode(java.lang.Integer COA_CODE, java.lang.Integer OH_AD_BRNCH, java.lang.Integer DR_AD_CMPNY)"
 *             query="SELECT OBJECT(dr) FROM InvOverhead oh, IN(oh.invDistributionRecords) dr WHERE oh.ohPosted=0 AND dr.invChartOfAccount.coaCode=?1 AND oh.ohAdBranch = ?2 AND dr.drAdCompany = ?3"
 *
 * @ejb:finder signature="Collection findImportableDrByDrReversedAndAdjCode(byte DR_RVRSD, java.lang.Integer ADJ_CODE, java.lang.Integer DR_AD_CMPNY)"
 *             query="SELECT OBJECT(dr) FROM InvAdjustment adj, IN(adj.invDistributionRecords) dr WHERE dr.drImported = 0 AND dr.drReversal = ?1 AND adj.adjCode = ?2 AND dr.drAdCompany = ?3"
 *             
 * @ejb:finder signature="Collection findUnpostedBstByDateRangeAndCoaAccountNumber(java.util.Date BST_DT_FRM, java.util.Date BST_DT_TO, java.lang.Integer COA_CODE, java.lang.Integer BST_AD_BRNCH, java.lang.Integer DR_AD_CMPNY)"
 *             query="SELECT OBJECT(dr) FROM InvBranchStockTransfer bst, IN(bst.invDistributionRecords) dr WHERE bst.bstVoid=0 AND bst.bstPosted=0 AND bst.bstDate>=?1 AND bst.bstDate<=?2 AND dr.invChartOfAccount.coaCode=?3 AND bst.bstAdBranch=?4 AND dr.drAdCompany=?5"
 * 
 * @jboss:query signature="Collection findUnpostedBstByDateRangeAndCoaAccountNumber(java.util.Date BST_DT_FRM, java.util.Date BST_DT_TO, java.lang.Integer COA_CODE, java.lang.Integer BST_AD_BRNCH, java.lang.Integer DR_AD_CMPNY)"
 *             query="SELECT OBJECT(dr) FROM InvBranchStockTransfer bst, IN(bst.invDistributionRecords) dr WHERE bst.bstVoid=0 AND bst.bstPosted=0 AND bst.bstDate>=?1 AND bst.bstDate<=?2 AND dr.invChartOfAccount.coaCode=?3 AND bst.bstAdBranch=?4 AND dr.drAdCompany=?5 ORDER BY dr.drLine"
 * 
 *            
 * @ejb:finder signature="Collection findUnpostedAdjByDateAndCoaAccountNumber(java.util.Date ADJ_DT_TO, java.lang.Integer COA_CODE, java.lang.Integer ADJ_AD_BRNCH, java.lang.Integer DR_AD_CMPNY)"
 *             query="SELECT OBJECT(dr) FROM InvAdjustment adj, IN(adj.invDistributionRecords) dr WHERE adj.adjVoid=0 AND adj.adjPosted=0 AND adj.adjDate<=?1 AND dr.invChartOfAccount.coaCode=?2 AND adj.adjAdBranch=?3 AND dr.drAdCompany=?4"
 *
 * @jboss:query signature="Collection findUnpostedAdjByDateAndCoaAccountNumber(java.util.Date ADJ_DT_TO, java.lang.Integer COA_CODE, java.lang.Integer ADJ_AD_BRNCH, java.lang.Integer DR_AD_CMPNY)"
 *             query="SELECT OBJECT(dr) FROM InvAdjustment adj, IN(adj.invDistributionRecords) dr WHERE adj.adjVoid=0 AND adj.adjPosted=0 AND adj.adjDate<=?1 AND dr.invChartOfAccount.coaCode=?2 AND adj.adjAdBranch=?3 AND dr.drAdCompany=?4 ORDER BY dr.drLine"
 *
 * @ejb:finder signature="Collection findUnpostedBuaByDateAndCoaAccountNumber(java.util.Date BUA_DT_TO, java.lang.Integer COA_CODE, java.lang.Integer BUA_AD_BRNCH, java.lang.Integer DR_AD_CMPNY)"
 *             query="SELECT OBJECT(dr) FROM InvBuildUnbuildAssembly bua, IN(bua.invDistributionRecords) dr WHERE bua.buaVoid=0 AND bua.buaPosted=0 AND bua.buaDate<=?1 AND dr.invChartOfAccount.coaCode=?2 AND bua.buaAdBranch=?3 AND dr.drAdCompany=?4"
 *
 * @jboss:query signature="Collection findUnpostedBuaByDateAndCoaAccountNumber(java.util.Date BUA_DT_TO, java.lang.Integer COA_CODE, java.lang.Integer BUA_AD_BRNCH, java.lang.Integer DR_AD_CMPNY)"
 *             query="SELECT OBJECT(dr) FROM InvBuildUnbuildAssembly bua, IN(bua.invDistributionRecords) dr WHERE bua.buaVoid=0 AND bua.buaPosted=0 AND bua.buaDate<=?1 AND dr.invChartOfAccount.coaCode=?2 AND bua.buaAdBranch=?3 AND dr.drAdCompany=?4 ORDER BY dr.drLine"
 *
 * @ejb:finder signature="Collection findUnpostedOhByDateAndCoaAccountNumber(java.util.Date OH_DT_TO, java.lang.Integer COA_CODE, java.lang.Integer OH_AD_BRNCH, java.lang.Integer DR_AD_CMPNY)"
 *             query="SELECT OBJECT(dr) FROM InvOverhead oh, IN(oh.invDistributionRecords) dr WHERE oh.ohPosted=0 AND oh.ohDate<=?1 AND dr.invChartOfAccount.coaCode=?2 AND oh.ohAdBranch=?3 AND dr.drAdCompany=?4"
 *
 * @jboss:query signature="Collection findUnpostedOhByDateAndCoaAccountNumber(java.util.Date OH_DT_TO, java.lang.Integer COA_CODE, java.lang.Integer OH_AD_BRNCH, java.lang.Integer DR_AD_CMPNY)"
 *             query="SELECT OBJECT(dr) FROM InvOverhead oh, IN(oh.invDistributionRecords) dr WHERE oh.ohPosted=0 AND oh.ohDate<=?1 AND dr.invChartOfAccount.coaCode=?2 AND oh.ohAdBranch=?3 AND dr.drAdCompany=?4 ORDER BY dr.drLine"
 * 
 * @ejb:finder signature="Collection findUnpostedSiByDateAndCoaAccountNumber(java.util.Date SI_DT_TO, java.lang.Integer COA_CODE, java.lang.Integer SI_AD_BRNCH, java.lang.Integer DR_AD_CMPNY)"
 *             query="SELECT OBJECT(dr) FROM InvStockIssuance si, IN(si.invDistributionRecords) dr WHERE si.siVoid=0 AND si.siPosted=0 AND si.siDate<=?1 AND dr.invChartOfAccount.coaCode=?2 AND si.siAdBranch=?3 AND dr.drAdCompany=?4"
 * 
 * @jboss:query signature="Collection findUnpostedSiByDateAndCoaAccountNumber(java.util.Date SI_DT_TO, java.lang.Integer COA_CODE, java.lang.Integer SI_AD_BRNCH, java.lang.Integer DR_AD_CMPNY)"
 *             query="SELECT OBJECT(dr) FROM InvStockIssuance si, IN(si.invDistributionRecords) dr WHERE si.siVoid=0 AND si.siPosted=0 AND si.siDate<=?1 AND dr.invChartOfAccount.coaCode=?2 AND si.siAdBranch=?3 AND dr.drAdCompany=?4 ORDER BY dr.drLine"
 * 
 * @ejb:finder signature="Collection findUnpostedAtrByDateAndCoaAccountNumber(java.util.Date ATR_DT_TO, java.lang.Integer COA_CODE, java.lang.Integer ATR_AD_BRNCH, java.lang.Integer DR_AD_CMPNY)"
 *             query="SELECT OBJECT(dr) FROM InvAssemblyTransfer atr, IN(atr.invDistributionRecords) dr WHERE atr.atrVoid=0 AND atr.atrPosted=0 AND atr.atrDate<=?1 AND dr.invChartOfAccount.coaCode=?2 AND atr.atrAdBranch=?3 AND dr.drAdCompany=?4"
 * 
 * @jboss:query signature="Collection findUnpostedAtrByDateAndCoaAccountNumber(java.util.Date ATR_DT_TO, java.lang.Integer COA_CODE, java.lang.Integer ATR_AD_BRNCH, java.lang.Integer DR_AD_CMPNY)"
 *             query="SELECT OBJECT(dr) FROM InvAssemblyTransfer atr, IN(atr.invDistributionRecords) dr WHERE atr.atrVoid=0 AND atr.atrPosted=0 AND atr.atrDate<=?1 AND dr.invChartOfAccount.coaCode=?2 AND atr.atrAdBranch=?3 AND dr.drAdCompany=?4 ORDER BY dr.drLine"
 * 
 * @ejb:finder signature="Collection findUnpostedStByDateAndCoaAccountNumber(java.util.Date ST_DT_TO, java.lang.Integer COA_CODE, java.lang.Integer ST_AD_BRNCH, java.lang.Integer DR_AD_CMPNY)"
 *             query="SELECT OBJECT(dr) FROM InvStockTransfer st, IN(st.invDistributionRecords) dr WHERE st.stPosted=0 AND st.stDate<=?1 AND dr.invChartOfAccount.coaCode=?2 AND st.stAdBranch=?3 AND dr.drAdCompany=?4"
 * 
 * @jboss:query signature="Collection findUnpostedStByDateAndCoaAccountNumber(java.util.Date ST_DT_TO, java.lang.Integer COA_CODE, java.lang.Integer ST_AD_BRNCH, java.lang.Integer DR_AD_CMPNY)"
 *             query="SELECT OBJECT(dr) FROM InvStockTransfer st, IN(st.invDistributionRecords) dr WHERE st.stPosted=0 AND st.stDate<=?1 AND dr.invChartOfAccount.coaCode=?2 AND st.stAdBranch=?3 AND dr.drAdCompany=?4 ORDER BY dr.drLine"
 * 
 * @ejb:finder signature="Collection findUnpostedBstByDateAndCoaAccountNumber(java.util.Date BST_DT_TO, java.lang.Integer COA_CODE, java.lang.Integer BST_AD_BRNCH, java.lang.Integer DR_AD_CMPNY)"
 *             query="SELECT OBJECT(dr) FROM InvBranchStockTransfer bst, IN(bst.invDistributionRecords) dr WHERE bst.bstVoid=0 AND bst.bstPosted=0 AND bst.bstDate<=?1 AND dr.invChartOfAccount.coaCode=?2 AND bst.bstAdBranch=?3 AND dr.drAdCompany=?4"
 * 
 * @jboss:query signature="Collection findUnpostedBstByDateAndCoaAccountNumber(java.util.Date BST_DT_TO, java.lang.Integer COA_CODE, java.lang.Integer BST_AD_BRNCH, java.lang.Integer DR_AD_CMPNY)"
 *             query="SELECT OBJECT(dr) FROM InvBranchStockTransfer bst, IN(bst.invDistributionRecords) dr WHERE bst.bstVoid=0 AND bst.bstPosted=0 AND bst.bstDate<=?1 AND dr.invChartOfAccount.coaCode=?2 AND bst.bstAdBranch=?3 AND dr.drAdCompany=?4 ORDER BY dr.drLine"
 * 
 * @ejb:value-object match="*"
 *             name="InvDistributionRecord"
 *
 * @jboss:persistence table-name="INV_DSTRBTN_RCRD"
 *
 * @jboss:entity-command name="mysql-get-generated-keys"
 *
 */

public abstract class InvDistributionRecordBean extends AbstractEntityBean {

//	 Access methods for persistent fields

	   /**
	    * @ejb:persistent-field
	    * @ejb:interface-method view-type="local"
	    * @ejb:pk-field
	    *
	    * @jboss:column-name name="DR_CODE"
	    *
	    * @jboss:persistence auto-increment="true"
	    **/
	   public abstract java.lang.Integer getDrCode();
	   public abstract void setDrCode(java.lang.Integer DR_CODE);

	   /**
	    * @ejb:persistent-field
	    * @ejb:interface-method view-type="local"
	    *
	    * @jboss:column-name name="DR_LN"
	    **/
	   public abstract short getDrLine();
	   /**
	    * @ejb:interface-method view-type="local"
	    **/
	   public abstract void setDrLine(short DR_LN);

	   /**
	    * @ejb:persistent-field
	    * @ejb:interface-method view-type="local"
	    *
	    * @jboss:column-name name="DR_CLSS"
	    **/
	   public abstract java.lang.String getDrClass();
	   /**
	    * @ejb:interface-method view-type="local"
	    **/
	   public abstract void setDrClass(java.lang.String DR_CLSS);
	   
	   /**
	    * @ejb:persistent-field
	    * @ejb:interface-method view-type="local"
	    *
	    * @jboss:column-name name="DR_AMNT"
	    **/
	   public abstract double getDrAmount();
	   /**
	    * @ejb:interface-method view-type="local"
	    **/
	   public abstract void setDrAmount(double DR_AMNT);
	   
	   /**
	    * @ejb:persistent-field
	    * @ejb:interface-method view-type="local"
	    *
	    * @jboss:column-name name="DR_DBT"
	    **/
	   public abstract byte getDrDebit();
	   /**
	    * @ejb:interface-method view-type="local"
	    **/
	   public abstract void setDrDebit(byte DR_DBT);
	   
	   /**
	    * @ejb:persistent-field
	    * @ejb:interface-method view-type="local"
	    *
	    * @jboss:column-name name="DR_RVRSL"
	    **/
	   public abstract byte getDrReversal();
	   /**
	    * @ejb:interface-method view-type="local"
	    **/
	   public abstract void setDrReversal(byte DR_RVRSL);
	   
	   /**
	    * @ejb:persistent-field
	    * @ejb:interface-method view-type="local"
	    *
	    * @jboss:column-name name="DR_IMPRTD"
	    **/
	   public abstract byte getDrImported();
	   /**
	    * @ejb:interface-method view-type="local"
	    **/
	   public abstract void setDrImported(byte DR_IMPRTD);
	   
	   /**
	    * @ejb:persistent-field
	    * @ejb:interface-method view-type="local"
	    *
	    * @jboss:column-name name="DR_AD_CMPNY"
	    **/
	   public abstract Integer getDrAdCompany();
	   /**
	    * @ejb:interface-method view-type="local"
	    **/
	   public abstract void setDrAdCompany(Integer DR_AD_CMPNY);

	   // Access methods for relationship fields
	   
	   /**
	    * @ejb:interface-method view-type="local"
	    * @ejb:relation name="adjustment-invdistributionrecords"
	    *               role-name="invdistributionrecord-has-one-adjustment"
	    *               cascade-delete="yes"
	    *
	    * @jboss:relation related-pk-field="adjCode"
	    *                 fk-column="INV_ADJUSTMENT"
	    */
	   public abstract LocalInvAdjustment getInvAdjustment();
	   /**
		 * @ejb:interface-method view-type="local"
		 **/
	   public abstract void setInvAdjustment(LocalInvAdjustment invAdjustment);
	   
	   /**
	    * @ejb:interface-method view-type="local"
	    * @ejb:relation name="buildunbuildassembly-invdistributionrecords"
	    *               role-name="invdistributionrecord-has-one-buildunbuildassembly"
	    *               cascade-delete="yes"
	    *
	    * @jboss:relation related-pk-field="buaCode"
	    *                 fk-column="INV_BUILD_UNBUILD_ASSEMBLY"
	    */
	   public abstract LocalInvBuildUnbuildAssembly getInvBuildUnbuildAssembly();
	   /**
		 * @ejb:interface-method view-type="local"
		 **/
	   public abstract void setInvBuildUnbuildAssembly(LocalInvBuildUnbuildAssembly invBuildUnbuildAssembly);
	   
	   /**
	    * @ejb:interface-method view-type="local"
	    * @ejb:relation name="chartofaccount-invdistributionrecords"
	    *               role-name="invdistributionrecord-has-one-chartofaccount"
	    *               cascade-delete="no"
	    *
	    * @jboss:relation related-pk-field="coaCode"
	    *                 fk-column="GL_CHART_OF_ACCOUNT"
	    */
	   public abstract com.ejb.gl.LocalGlChartOfAccount getInvChartOfAccount();
	   /**
		 * @ejb:interface-method view-type="local"
		 **/
	   public abstract void setInvChartOfAccount(com.ejb.gl.LocalGlChartOfAccount glChartOfAccount);
	   
	   /**
	    * @ejb:interface-method view-type="local"
	    * @ejb:relation name="overhead-invdistributionrecords"
	    *               role-name="invdistributionrecord-has-one-overhead"
	    *               cascade-delete="yes"
	    *
	    * @jboss:relation related-pk-field="ohCode"
	    *                 fk-column="INV_OVERHEAD"
	    */
	   public abstract LocalInvOverhead getInvOverhead();
	   /**
		 * @ejb:interface-method view-type="local"
		 **/
	   public abstract void setInvOverhead(LocalInvOverhead invOverhead);
	   
	   
	   /**
	    * @ejb:interface-method view-type="local"
	    * @ejb:relation name="stockissuance-invdistributionrecords"
	    *               role-name="invdistributionrecord-has-one-stockissuance"
	    *               cascade-delete="yes"
	    *
	    * @jboss:relation related-pk-field="siCode"
	    *                 fk-column="INV_STOCK_ISSUANCE"
	    */
	   public abstract LocalInvStockIssuance getInvStockIssuance();
	   /**
		 * @ejb:interface-method view-type="local"
		 **/
	   public abstract void setInvStockIssuance(LocalInvStockIssuance invStockIssuance);
	   
	   
	   /**
	    * @ejb:interface-method view-type="local"
	    * @ejb:relation name="assemblytransfer-invdistributionrecords"
	    *               role-name="invdistributionrecord-has-one-assemblytransfer"
	    *               cascade-delete="yes"
	    *
	    * @jboss:relation related-pk-field="atrCode"
	    *                 fk-column="INV_ASSEMBLY_TRANSFER"
	    */
	   public abstract LocalInvAssemblyTransfer getInvAssemblyTransfer();
	   /**
		 * @ejb:interface-method view-type="local"
		 **/
	   public abstract void setInvAssemblyTransfer(LocalInvAssemblyTransfer invAssemblyTransfer);
	   
	   /**
	    * @ejb:interface-method view-type="local"
	    * @ejb:relation name="stocktransfer-invdistributionrecords"
	    *               role-name="invdistributionrecord-has-one-stocktransfer"
	    *               cascade-delete="yes"
	    *
	    * @jboss:relation related-pk-field="stCode"
	    *                 fk-column="INV_STOCK_TRANSFER"
	    */
	   public abstract LocalInvStockTransfer getInvStockTransfer();
	   /**
		 * @ejb:interface-method view-type="local"
		 **/
	   public abstract void setInvStockTransfer(LocalInvStockTransfer invStockTransfer);
	   
	   /**
	    * @ejb:interface-method view-type="local"
	    * @ejb:relation name="branchstocktransfer-invdistributionrecords"
	    *               role-name="invdistributionrecord-has-one-branchstocktransfer"
	    *               cascade-delete="yes"
	    *
	    * @jboss:relation related-pk-field="bstCode"
	    *                 fk-column="INV_BRANCH_STOCK_TRANSFER"
	    */
	   public abstract LocalInvBranchStockTransfer getInvBranchStockTransfer();
	   /**
		 * @ejb:interface-method view-type="local"
		 **/
	   public abstract void setInvBranchStockTransfer(LocalInvBranchStockTransfer invBranchStockTransfer);
	   
	   // Business methods
	   
	   // EntityBean methods

	   /**
	    * @ejb:create-method view-type="local"
	    **/
	   public java.lang.Integer ejbCreate (java.lang.Integer DR_CODE, short DR_LN,
	   	  java.lang.String DR_CLSS, byte DR_DBT, double DR_AMNT, byte DR_RVRSL, 
		  byte DR_IMPRTD, Integer DR_AD_CMPNY)
	      throws CreateException {

	      Debug.print("InvDistributionRecordBean ejbCreate");
	      setDrCode(DR_CODE);
	      setDrLine(DR_LN);
	      setDrClass(DR_CLSS);
	      setDrDebit(DR_DBT);
	      setDrAmount(DR_AMNT);	      
	      setDrReversal(DR_RVRSL);
	      setDrImported(DR_IMPRTD);
	      setDrAdCompany(DR_AD_CMPNY);
	      return null;
	   }

	   /**
	    * @ejb:create-method view-type="local"
	    **/
	   public java.lang.Integer ejbCreate (short DR_LN,
	   	  java.lang.String DR_CLSS, byte DR_DBT, double DR_AMNT, byte DR_RVRSL, 
		  byte DR_IMPRTD, Integer DR_AD_CMPNY)
	      throws CreateException {

	      Debug.print("InvDistributionRecordBean ejbCreate");
	      setDrLine(DR_LN);
	      setDrClass(DR_CLSS);
	      setDrDebit(DR_DBT);
	      setDrAmount(DR_AMNT);	      
	      setDrReversal(DR_RVRSL);
	      setDrImported(DR_IMPRTD);
	      setDrAdCompany(DR_AD_CMPNY);
	      return null;
	   }

	   public void ejbPostCreate (java.lang.Integer DR_CODE, short DR_LN,
		   	  java.lang.String DR_CLSS, byte DR_DBT, double DR_AMNT, byte DR_RVRSL, 
			  byte DR_IMPRTD, Integer DR_AD_CMPNY)
	      throws CreateException { }

	   public void ejbPostCreate (short DR_LN,
		   	  java.lang.String DR_CLSS, byte DR_DBT, double DR_AMNT, byte DR_RVRSL, 
			  byte DR_IMPRTD, Integer DR_AD_CMPNY)
	      throws CreateException { }
	
}
