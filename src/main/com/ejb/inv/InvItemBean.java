/*
 * com/ejb/inv/LocalInvItemBean.java
 *
 * Created on May 20, 2004 11:07 AM
 */

package com.ejb.inv;

import java.util.Collection;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;

import com.ejb.ap.LocalApSupplier;
import com.ejb.ar.LocalArCustomer;
import com.util.AbstractEntityBean;
import com.util.Debug;

/**
 *
 * @author  Enrico C. Yap
 *
 * @ejb:bean name="InvItemEJB"
 *           display-name="Item Entity"
 *           type="CMP"
 *           cmp-version="2.x"
 *           view-type="local"
 *           local-jndi-name="omega-ejb/InvItemEJB"
 *           schema="InvItem"
 *           primkey-field="iiCode"
 *
 * @ejb:pk class="java.lang.Integer"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="invuser"
 *                        role-link="invuserlink"
 *
 * @ejb:permission role-name="invuser"
 *
 * @ejb:interface local-class="com.ejb.inv.LocalInvItem"
 *                local-extends="javax.ejb.EJBLocalObject"
 *
 * @ejb:home local-class="com.ejb.inv.LocalInvItemHome"
 *           local-extends="javax.ejb.EJBLocalHome"
 *
 * @ejb:finder signature="LocalInvItem findByIiName(java.lang.String II_NM, java.lang.Integer II_AD_CMPNY)"
 *             query="SELECT OBJECT(ii) FROM InvItem ii WHERE ii.iiName=?1 AND ii.iiAdCompany=?2"
 *
 * @ejb:finder signature="LocalInvItem findByIiCode(java.lang.Integer II_CD, java.lang.Integer II_AD_CMPNY)"
 *             query="SELECT OBJECT(ii) FROM InvItem ii WHERE ii.iiCode=?1 AND ii.iiAdCompany=?2"
 *
 * @ejb:finder signature="LocalInvItem findByPartNumber(java.lang.String II_PRT_NMBR, java.lang.Integer II_AD_CMPNY)"
 *             query="SELECT OBJECT(ii) FROM InvItem ii WHERE ii.iiPartNumber=?1 AND ii.iiAdCompany=?2"
 *
 * @ejb:finder signature="LocalInvItem findByIiNameAndIiAdLvCategory(java.lang.String II_NM, java.lang.String II_AD_LV_CTGRY, java.lang.Integer II_AD_CMPNY)"
 *             query="SELECT OBJECT(ii) FROM InvItem ii WHERE ii.iiName=?1 AND ii.iiAdLvCategory=?2 AND ii.iiAdCompany=?3"
 *
 * @ejb:finder signature="Collection findAssemblyIiAll(java.lang.Integer II_AD_CMPNY)"
 *             query="SELECT OBJECT(ii) FROM InvItem ii WHERE ii.iiClass = 'Assembly' AND ii.iiAdCompany=?1"
 *
 * @ejb:finder signature="Collection findStockIiAll(java.lang.Integer II_AD_CMPNY)"
 *             query="SELECT OBJECT(ii) FROM InvItem ii WHERE ii.iiClass = 'Stock' AND ii.iiAdCompany=?1"
 *
 * @ejb:finder signature="Collection findFixedAssetIiAll(java.lang.Integer II_AD_CMPNY)"
 *             query="SELECT OBJECT(ii) FROM InvItem ii WHERE ii.iiFixedAsset = 1 AND ii.iiEnable = 1 AND ii.iiAdCompany=?1"
 *
 * @ejb:finder signature="Collection findFixedAssetIiByDateAcquired(java.util.Date II_DT_ACQRD_TO, java.lang.Integer II_AD_CMPNY)"
 *             query="SELECT OBJECT(ii) FROM InvItem ii WHERE ii.iiFixedAsset = 1 AND ii.iiEnable = 1 AND ii.iiDateAcquired <= ?1 AND ii.iiAdCompany=?2"
 *
 * @ejb:finder signature="Collection findEnabledIiAll(java.lang.Integer II_AD_CMPNY)"
 *             query="SELECT OBJECT(ii) FROM InvItem ii WHERE ii.iiEnable = 1 AND ii.iiAdCompany=?1"
 *
 * @jboss:query signature="Collection findEnabledIiAll(java.lang.Integer II_AD_CMPNY)"
 *             query="SELECT OBJECT(ii) FROM InvItem ii WHERE ii.iiEnable = 1 AND ii.iiAdCompany=?1 ORDER BY ii.iiName"
 *
 * @ejb:finder signature="Collection findEnabledIiByLocName(java.lang.String LOC_NM, java.lang.Integer II_AD_CMPNY)"
 *             query="SELECT OBJECT(ii) FROM InvItem ii, IN(ii.invItemLocations) il WHERE il.invLocation.locName = ?1 AND ii.iiEnable = 1 AND ii.iiAdCompany=?2 ORDER BY ii.iiName"
 *
 * @jboss:query signature="Collection findEnabledIiByLocName(java.lang.String LOC_NM, java.lang.Integer II_AD_CMPNY)"
 *             query="SELECT OBJECT(ii) FROM InvItem ii, IN(ii.invItemLocations) il WHERE il.invLocation.locName = ?1 AND ii.iiEnable = 1 AND ii.iiAdCompany=?2 ORDER BY ii.iiName"
 *
 * @ejb:finder signature="Collection findEnabledIiByIiAdLvCategory(java.lang.String II_AD_LV_CTGRY, java.lang.Integer II_AD_CMPNY)"
 *             query="SELECT OBJECT(ii) FROM InvItem ii WHERE ii.iiAdLvCategory = ?1 AND ii.iiEnable = 1 AND ii.iiAdCompany = ?2"
 *
 * @jboss:query signature="Collection findEnabledIiByIiAdLvCategory(java.lang.String II_AD_LV_CTGRY, java.lang.Integer II_AD_CMPNY)"
 *             query="SELECT OBJECT(ii) FROM InvItem ii WHERE ii.iiAdLvCategory = ?1 AND ii.iiEnable = 1 AND ii.iiAdCompany = ?2 ORDER BY ii.iiName"
 *
 *
 * @ejb:finder signature="Collection findEnabledIiByIiAdLvCategoryAndIiClass(java.lang.String II_AD_LV_CTGRY, java.lang.String II_CLSS, java.lang.Integer II_AD_CMPNY)"
 *             query="SELECT OBJECT(ii) FROM InvItem ii WHERE ii.iiAdLvCategory = ?1 AND ii.iiEnable = 1 AND ii.iiClass=?2 AND ii.iiAdCompany = ?3 ORDER BY ii.iiName"
 *
 * @ejb:finder signature="Collection findIiAll(java.lang.Integer II_AD_CMPNY)"
 *             query="SELECT OBJECT(ii) FROM InvItem ii WHERE ii.iiAdCompany=?1"
 *
 * @ejb:finder signature="Collection findIiByUmcAdLvClass(java.lang.String UOM_AD_LV_CLSS, java.lang.Integer II_AD_CMPNY)"
 *             query="SELECT DISTINCT OBJECT(ii) FROM InvItem ii, IN(ii.invUnitOfMeasureConversions) umc WHERE umc.invUnitOfMeasure.uomAdLvClass = ?1 AND ii.iiAdCompany = ?2"
 *
 * @jboss:query signature="Collection findIiByUmcAdLvClass(java.lang.String UOM_AD_LV_CLSS, java.lang.Integer II_AD_CMPNY)"
 *             query="SELECT DISTINCT OBJECT(ii) FROM InvItem ii, IN(ii.invUnitOfMeasureConversions) umc WHERE umc.invUnitOfMeasure.uomAdLvClass = ?1 AND ii.iiAdCompany = ?2 ORDER BY ii.iiName"
 *
 * @ejb:finder signature="Collection findByIiDoneness(java.lang.String II_DNNSS, java.lang.Integer II_AD_CMPNY)"
 *             query="SELECT OBJECT(ii) FROM InvItem ii  WHERE ii.iiDoneness = ?1 AND ii.iiAdCompany = ?2"
 *
 * @ejb:finder signature="Collection findIiByIiNewAndUpdated(java.lang.Integer BR_CODE, java.lang.String II_IL_LOCATION, java.lang.Integer AD_CMPNY, char NEW, char UPDATED, char DOWNLOADED_UPDATED)"
 *             query="SELECT DISTINCT OBJECT(ii) FROM InvItem ii, IN(ii.invItemLocations) il, IN(il.adBranchItemLocations) bil WHERE (bil.bilItemDownloadStatus = ?4 OR bil.bilItemDownloadStatus = ?5 OR bil.bilItemDownloadStatus = ?6) AND ii.iiRetailUom IS NOT NULL AND bil.adBranch.brCode = ?1 AND bil.invItemLocation.invLocation.locLvType = ?2 AND bil.bilAdCompany = ?3"
 *
 * @ejb:finder signature="Collection findIiByIiNewAndUpdated(java.lang.Integer BR_CODE, java.lang.Integer AD_CMPNY, char NEW, char UPDATED, char DOWNLOADED_UPDATED)"
 *             query="SELECT DISTINCT OBJECT(ii) FROM InvItem ii, IN(ii.invItemLocations) il, IN(il.adBranchItemLocations) bil WHERE (bil.bilItemDownloadStatus = ?3 OR bil.bilItemDownloadStatus = ?4 OR bil.bilItemDownloadStatus = ?5) AND ii.iiRetailUom IS NOT NULL AND bil.adBranch.brCode = ?1 AND bil.bilAdCompany = ?2"
 *
 * @jboss:query signature="Collection ejbSelectGeneric(java.lang.String jbossQl, java.lang.Object[] args)"
 *                         dynamic="true"
 *
 * @ejb:value-object match="*"
 *             name="InvItem"
 *
 * @jboss:persistence table-name="INV_ITM"
 *
 * @jboss:entity-command name="mysql-get-generated-keys"
 */

public abstract class InvItemBean extends AbstractEntityBean {


    // PERSISTENT METHODS

    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     * @ejb:pk-field
     *
     * @jboss:column-name name="II_CODE"
     *
     * @jboss:persistence auto-increment="true"
     **/
    public abstract Integer getIiCode();
    public abstract void setIiCode(Integer II_CODE);

    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="II_NM"
     **/
     public abstract String getIiName();
     /**
      * @ejb:interface-method view-type="local"
      **/
     public abstract void setIiName(String II_NM);

    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="II_DESC"
     **/
     public abstract String getIiDescription();
     /**
      * @ejb:interface-method view-type="local"
      **/
     public abstract void setIiDescription(String II_DESC);

	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="II_PRT_NMBR"
	 **/
	 public abstract String getIiPartNumber();
	 /**
	  * @ejb:interface-method view-type="local"
	  **/
	 public abstract void setIiPartNumber(String II_PRT_NMBR);

	 /**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="II_SHRT_NM"
	 **/
	 public abstract String getIiShortName();
	 /**
	  * @ejb:interface-method view-type="local"
	  **/
	 public abstract void setIiShortName(String II_SHRT_NM);

	 /**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="II_BR_CD_1"
	 **/
	 public abstract String getIiBarCode1();
	 /**
	  * @ejb:interface-method view-type="local"
	  **/
	 public abstract void setIiBarCode1(String II_BR_CD_1);

	 /**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="II_BR_CD_2"
	 **/
	 public abstract String getIiBarCode2();
	 /**
	  * @ejb:interface-method view-type="local"
	  **/
	 public abstract void setIiBarCode2(String II_BR_CD_2);


	 /**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="II_BR_CD_3"
	 **/
	 public abstract String getIiBarCode3();
	 /**
	  * @ejb:interface-method view-type="local"
	  **/
	 public abstract void setIiBarCode3(String II_BR_CD_3);



	 /**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="II_BRND"
	 **/
	 public abstract String getIiBrand();
	 /**
	  * @ejb:interface-method view-type="local"
	  **/
	 public abstract void setIiBrand(String II_BRND);


    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="II_CLSS"
     **/
     public abstract String getIiClass();
     /**
      * @ejb:interface-method view-type="local"
      **/
     public abstract void setIiClass(String II_CLSS);

    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="II_AD_LV_CTGRY"
     **/
     public abstract String getIiAdLvCategory();
     /**
      * @ejb:interface-method view-type="local"
      **/
     public abstract void setIiAdLvCategory(String II_AD_LV_CTGRY);

    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="II_CST_MTHD"
     **/
     public abstract String getIiCostMethod();
     /**
      * @ejb:interface-method view-type="local"
      **/
     public abstract void setIiCostMethod(String II_CST_MTHD);

     /**
      * @ejb:persistent-field
      * @ejb:interface-method view-type="local"
      *
      * @jboss:column-name name="II_UNT_CST"
      **/
      public abstract double getIiUnitCost();
      /**
       * @ejb:interface-method view-type="local"
       **/
      public abstract void setIiUnitCost(double II_UNT_CST);

    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="II_SLS_PRC"
     **/
     public abstract double getIiSalesPrice();
     /**
      * @ejb:interface-method view-type="local"
      **/
     public abstract void setIiSalesPrice(double II_SLS_PRC);

    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="II_ENBL"
     **/
     public abstract byte getIiEnable();
     /**
      * @ejb:interface-method view-type="local"
      **/
     public abstract void setIiEnable(byte II_ENBL);

     /**
      * @ejb:persistent-field
      * @ejb:interface-method view-type="local"
      *
      * @jboss:column-name name="II_ENBL_AT_BLD"
      **/
      public abstract byte getIiEnableAutoBuild();
      /**
       * @ejb:interface-method view-type="local"
       **/
      public abstract void setIiEnableAutoBuild(byte II_ENBL_AT_BLD);

      /**
       * @ejb:persistent-field
       * @ejb:interface-method view-type="local"
       *
       * @jboss:column-name name="II_DNNSS"
       **/
      public abstract String getIiDoneness();
      /**
       * @ejb:interface-method view-type="local"
       **/
      public abstract void setIiDoneness(String II_DNNSS);

      /**
       * @ejb:persistent-field
       * @ejb:interface-method view-type="local"
       *
       * @jboss:column-name name="II_SDNGS"
       **/
      public abstract String getIiSidings();
      /**
       * @ejb:interface-method view-type="local"
       **/
      public abstract void setIiSidings(String II_SDNGS);

      /**
       * @ejb:persistent-field
       * @ejb:interface-method view-type="local"
       *
       * @jboss:column-name name="II_RMRKS"
       **/
      public abstract String getIiRemarks();
      /**
       * @ejb:interface-method view-type="local"
       **/
      public abstract void setIiRemarks(String II_RMRKS);

      /**
       * @ejb:persistent-field
       * @ejb:interface-method view-type="local"
       *
       * @jboss:column-name name="II_SRVC_CHRG"
       **/
      public abstract byte getIiServiceCharge();
      /**
       * @ejb:interface-method view-type="local"
       **/
      public abstract void setIiServiceCharge(byte II_SRVC_CHRG);

      /**
       * @ejb:persistent-field
       * @ejb:interface-method view-type="local"
       *
       * @jboss:column-name name="II_VS_ITM"
       **/
      public abstract byte getIiVirtualStore();
      /**
       * @ejb:interface-method view-type="local"
       **/
      public abstract void setIiVirtualStore(byte II_VS_ITM);

      /**
       * @ejb:persistent-field
       * @ejb:interface-method view-type="local"
       *
       * @jboss:column-name name="II_DN_IN_CHRG"
       **/
      public abstract byte getIiNonInventoriable();
      /**
       * @ejb:interface-method view-type="local"
       **/
      public abstract void setIiNonInventoriable(byte II_DN_IN_CHRG);


      /**
       * @ejb:persistent-field
       * @ejb:interface-method view-type="local"
       *
       * @jboss:column-name name="II_SRVCS"
       **/
      public abstract byte getIiServices();
      /**
       * @ejb:interface-method view-type="local"
       **/
      public abstract void setIiServices(byte II_SRVCS);
      
      /**
       * @ejb:persistent-field
       * @ejb:interface-method view-type="local"
       *
       * @jboss:column-name name="II_JB_SRVCS"
       **/
      public abstract byte getIiJobServices();
      /**
       * @ejb:interface-method view-type="local"
       **/
      public abstract void setIiJobServices(byte II_JB_SRVCS);


      /**
       * @ejb:persistent-field
       * @ejb:interface-method view-type="local"
       *
       * @jboss:column-name name="II_IS_VAT_RLF"
       **/
      public abstract byte getIiIsVatRelief();
      /**
       * @ejb:interface-method view-type="local"
       **/
      public abstract void setIiIsVatRelief(byte II_IS_VAT_RLF);


      /**
       * @ejb:persistent-field
       * @ejb:interface-method view-type="local"
       *
       * @jboss:column-name name="II_IS_TX"
       **/
       public abstract byte getIiIsTax();
       /**
        * @ejb:interface-method view-type="local"
        **/
       public abstract void setIiIsTax(byte II_IS_TX);

       /**
        * @ejb:persistent-field
        * @ejb:interface-method view-type="local"
        *
        * @jboss:column-name name="II_IS_PRJCT"
        **/
        public abstract byte getIiIsProject();
        /**
         * @ejb:interface-method view-type="local"
         **/
        public abstract void setIiIsProject(byte II_IS_PRJCT);


      /**
       * @ejb:persistent-field
       * @ejb:interface-method view-type="local"
       *
       * @jboss:column-name name="II_PRCNT_MRKP"
       **/
      public abstract double getIiPercentMarkup();
      /**
       * @ejb:interface-method view-type="local"
       **/
      public abstract void setIiPercentMarkup(double II_PRCNT_MRKP);

      /**
       * @ejb:persistent-field
       * @ejb:interface-method view-type="local"
       *
       * @jboss:column-name name="II_SHPPNG_CST"
       **/
      public abstract double getIiShippingCost();
      /**
       * @ejb:interface-method view-type="local"
       **/
      public abstract void setIiShippingCost(double II_SHPPNG_CST);


      /**
       * @ejb:persistent-field
       * @ejb:interface-method view-type="local"
       *
       * @jboss:column-name name="II_SPCFC_GRVTY"
       **/
      public abstract double getIiSpecificGravity();
      /**
       * @ejb:interface-method view-type="local"
       **/
      public abstract void setIiSpecificGravity(double II_SPCFC_GRVTY);

      /**
       * @ejb:persistent-field
       * @ejb:interface-method view-type="local"
       *
       * @jboss:column-name name="II_STNDRD_FLL_SZ"
       **/
      public abstract double getIiStandardFillSize();
      /**
       * @ejb:interface-method view-type="local"
       **/
      public abstract void setIiStandardFillSize(double II_STNDRD_FLL_SZ);


      /**
       * @ejb:persistent-field
       * @ejb:interface-method view-type="local"
       *
       * @jboss:column-name name="II_YLD"
       **/
      public abstract double getIiYield();
      /**
       * @ejb:interface-method view-type="local"
       **/
      public abstract void setIiYield(double II_YLD);

      /**
       * @ejb:persistent-field
       * @ejb:interface-method view-type="local"
       *
       * @jboss:column-name name="II_LBR_CST"
       **/
      public abstract double getIiLaborCost();
      /**
       * @ejb:interface-method view-type="local"
       **/
      public abstract void setIiLaborCost(double II_LBR_CST);

      /**
       * @ejb:persistent-field
       * @ejb:interface-method view-type="local"
       *
       * @jboss:column-name name="II_PWR_CST"
       **/
      public abstract double getIiPowerCost();
      /**
       * @ejb:interface-method view-type="local"
       **/
      public abstract void setIiPowerCost(double II_PWR_CST);

      /**
       * @ejb:persistent-field
       * @ejb:interface-method view-type="local"
       *
       * @jboss:column-name name="II_OHD_CST"
       **/
      public abstract double getIiOverHeadCost();
      /**
       * @ejb:interface-method view-type="local"
       **/
      public abstract void setIiOverHeadCost(double II_OHD_CST);

      /**
       * @ejb:persistent-field
       * @ejb:interface-method view-type="local"
       *
       * @jboss:column-name name="II_FRGHT_CST"
       **/
      public abstract double getIiFreightCost();
      /**
       * @ejb:interface-method view-type="local"
       **/
      public abstract void setIiFreightCost(double II_FRGHT_CST);

      /**
       * @ejb:persistent-field
       * @ejb:interface-method view-type="local"
       *
       * @jboss:column-name name="II_FXD_CST"
       **/
      public abstract double getIiFixedCost();
      /**
       * @ejb:interface-method view-type="local"
       **/
      public abstract void setIiFixedCost(double II_FXD_CST);


      /**
       * @ejb:persistent-field
       * @ejb:interface-method view-type="local"
       *
       * @jboss:column-name name="II_STD_LST_PRCNTG"
       **/
      public abstract double getIiLossPercentage();
      /**
       * @ejb:interface-method view-type="local"
       **/
      public abstract void setIiLossPercentage(double II_STD_LST_PRCNTG);


      /**
       * @ejb:persistent-field
       * @ejb:interface-method view-type="local"
       *
       * @jboss:column-name name="II_MRKP_VL"
       **/
      public abstract double getIiMarkupValue();
      /**
       * @ejb:interface-method view-type="local"
       **/
      public abstract void setIiMarkupValue(double II_MRKP_VL);

      /**
       * @ejb:persistent-field
       * @ejb:interface-method view-type="local"
       *
       * @jboss:column-name name="II_MRKT"
       **/
      public abstract String getIiMarket();
      /**
       * @ejb:interface-method view-type="local"
       **/
      public abstract void setIiMarket(String II_MRKT);

      /**
       * @ejb:persistent-field
       * @ejb:interface-method view-type="local"
       *
       * @jboss:column-name name="II_ENBL_PO"
       **/
      public abstract byte getIiEnablePo();
      /**
       * @ejb:interface-method view-type="local"
       **/
      public abstract void setIiEnablePo(byte II_ENBL_PO);


      /**
       * @ejb:persistent-field
       * @ejb:interface-method view-type="local"
       *
       * @jboss:column-name name="II_PO_CYCL"
       **/
      public abstract short getIiPoCycle();
      /**
       * @ejb:interface-method view-type="local"
       **/
      public abstract void setIiPoCycle(short II_PO_CYCL);

      /**
       * @ejb:persistent-field
       * @ejb:interface-method view-type="local"
      *
      * @jboss:column-name name="II_RETAIL_UOM"
      **/
      public abstract Integer getIiRetailUom();
      /**
       * @ejb:interface-method view-type="local"
       **/
      public abstract void setIiRetailUom(Integer II_RETAIL_UOM);

      /**
       * @ejb:persistent-field
       * @ejb:interface-method view-type="local"
      *
      * @jboss:column-name name="II_UMC_PCKGNG"
      **/
      public abstract String getIiUmcPackaging();
      /**
       * @ejb:interface-method view-type="local"
       **/
      public abstract void setIiUmcPackaging(String II_UMC_PCKGNG);

      /**
       * @ejb:persistent-field
       * @ejb:interface-method view-type="local"
      *
      * @jboss:column-name name="II_OPN_PRDCT"
      **/
      public abstract byte getIiOpenProduct();
      /**
       * @ejb:interface-method view-type="local"
       **/
      public abstract void setIiOpenProduct(byte II_OPN_PRDCT);

      /**
       * @ejb:persistent-field
       * @ejb:interface-method view-type="local"
      *
      * @jboss:column-name name="II_FXD_ASST"
      **/
      public abstract byte getIiFixedAsset();
      /**
       * @ejb:interface-method view-type="local"
       **/
      public abstract void setIiFixedAsset(byte II_FXD_ASST);

	   /**
	    * @ejb:persistent-field
	    * @ejb:interface-method view-type="local"
	    *
	    * @jboss:column-name name="II_DT_ACQRD"
	    **/
	   public abstract java.util.Date getIiDateAcquired();
	   /**
	    * @ejb:interface-method view-type="local"
	    **/
	   public abstract void setIiDateAcquired(java.util.Date II_DT_ACQRD);

	   /**
       * @ejb:persistent-field
       * @ejb:interface-method view-type="local"
      *
      * @jboss:column-name name="II_DFLT_LCTN"
      **/
      public abstract Integer getIiDefaultLocation();
      /**
       * @ejb:interface-method view-type="local"
       **/
      public abstract void setIiDefaultLocation(Integer II_DFLT_LCTN);

      /**
       * @ejb:persistent-field
       * @ejb:interface-method view-type="local"
      *
      * @jboss:column-name name="II_TRC_MSC"
      **/
      public abstract byte getIiTraceMisc();
      /**
       * @ejb:interface-method view-type="local"
       **/
      public abstract void setIiTraceMisc(byte II_TRC_MSC);


      /**
       * @ejb:persistent-field
       * @ejb:interface-method view-type="local"
      *
      * @jboss:column-name name="II_SC_SNDY"
      **/
      public abstract byte getIiScSunday();
      /**
       * @ejb:interface-method view-type="local"
       **/
      public abstract void setIiScSunday(byte II_SC_SNDY);

      /**
       * @ejb:persistent-field
       * @ejb:interface-method view-type="local"
      *
      * @jboss:column-name name="II_SC_MNDY"
      **/
      public abstract byte getIiScMonday();
      /**
       * @ejb:interface-method view-type="local"
       **/
      public abstract void setIiScMonday(byte II_SC_MNDY);

      /**
       * @ejb:persistent-field
       * @ejb:interface-method view-type="local"
      *
      * @jboss:column-name name="II_SC_TSDY"
      **/
      public abstract byte getIiScTuesday();
      /**
       * @ejb:interface-method view-type="local"
       **/
      public abstract void setIiScTuesday(byte II_SC_TSDY);

      /**
       * @ejb:persistent-field
       * @ejb:interface-method view-type="local"
      *
      * @jboss:column-name name="II_SC_WDNSDY"
      **/
      public abstract byte getIiScWednesday();
      /**
       * @ejb:interface-method view-type="local"
       **/
      public abstract void setIiScWednesday(byte II_SC_WDNSDY);

      /**
       * @ejb:persistent-field
       * @ejb:interface-method view-type="local"
      *
      * @jboss:column-name name="II_SC_THRSDY"
      **/
      public abstract byte getIiScThursday();
      /**
       * @ejb:interface-method view-type="local"
       **/
      public abstract void setIiScThursday(byte II_SC_THRSDY);

      /**
       * @ejb:persistent-field
       * @ejb:interface-method view-type="local"
      *
      * @jboss:column-name name="II_SC_FRDY"
      **/
      public abstract byte getIiScFriday();
      /**
       * @ejb:interface-method view-type="local"
       **/
      public abstract void setIiScFriday(byte II_SC_FRDY);

      /**
       * @ejb:persistent-field
       * @ejb:interface-method view-type="local"
      *
      * @jboss:column-name name="II_SC_STRDY"
      **/
      public abstract byte getIiScSaturday();
      /**
       * @ejb:interface-method view-type="local"
       **/
      public abstract void setIiScSaturday(byte II_SC_STRDY);


      /**
       * @ejb:persistent-field
       * @ejb:interface-method view-type="local"
      *
      * @jboss:column-name name="II_ACQSTN_CST"
      **/
      public abstract double getIiAcquisitionCost();
      /**
       * @ejb:interface-method view-type="local"
       **/
      public abstract void setIiAcquisitionCost(double II_ACQSTN_CST);
      /**
       * @ejb:persistent-field
       * @ejb:interface-method view-type="local"
      *
      * @jboss:column-name name="II_LF_SPN"
      **/
      public abstract double getIiLifeSpan();
      /**
       * @ejb:interface-method view-type="local"
       **/
      public abstract void setIiLifeSpan(double II_LF_SPN);
      /**
       * @ejb:persistent-field
       * @ejb:interface-method view-type="local"
      *
      * @jboss:column-name name="II_RSDL_VL"
      **/
      public abstract double getIiResidualValue();
      /**
       * @ejb:interface-method view-type="local"
       **/
      public abstract void setIiResidualValue(double II_RSDL_VL);
      /**
       * @ejb:persistent-field
       * @ejb:interface-method view-type="local"
      *
      * @jboss:column-name name="II_GL_COA_FXD_ASST_ACCNT"
      **/
      public abstract String getGlCoaFixedAssetAccount();
      /**
       * @ejb:interface-method view-type="local"
       **/
      public abstract void setGlCoaFixedAssetAccount(String II_GL_COA_FXD_ASST_ACCNT);

      /**
       * @ejb:persistent-field
       * @ejb:interface-method view-type="local"
      *
      * @jboss:column-name name="II_GL_COA_DPRCTN_ACCNT"
      **/
      public abstract String getGlCoaDepreciationAccount();
      /**
       * @ejb:interface-method view-type="local"
       **/
      public abstract void setGlCoaDepreciationAccount(String II_GL_COA_DPRCTN_ACCNT);

      /**
       * @ejb:persistent-field
       * @ejb:interface-method view-type="local"
      *
      * @jboss:column-name name="II_GL_ACCMLTD_DPRCTN_ACCNT"
      **/
      public abstract String getGlCoaAccumulatedDepreciationAccount();
      /**
       * @ejb:interface-method view-type="local"
       **/
      public abstract void setGlCoaAccumulatedDepreciationAccount(String II_GL_ACCMLTD_DPRCTN_ACCNT);

      /**
       * @ejb:persistent-field
       * @ejb:interface-method view-type="local"
      *
      * @jboss:column-name name="II_GL_LBR_CST_ACCNT"
      **/
      public abstract String getGlCoaLaborCostAccount();
      /**
       * @ejb:interface-method view-type="local"
       **/
      public abstract void setGlCoaLaborCostAccount(String II_GL_LBR_CST_ACCNT);

      /**
       * @ejb:persistent-field
       * @ejb:interface-method view-type="local"
      *
      * @jboss:column-name name="II_GL_PWR_CST_ACCNT"
      **/
      public abstract String getGlCoaPowerCostAccount();
      /**
       * @ejb:interface-method view-type="local"
       **/
      public abstract void setGlCoaPowerCostAccount(String II_GL_PWR_CST_ACCNT);

      /**
       * @ejb:persistent-field
       * @ejb:interface-method view-type="local"
      *
      * @jboss:column-name name="II_GL_OHD_CST_ACCNT"
      **/
      public abstract String getGlCoaOverHeadCostAccount();
      /**
       * @ejb:interface-method view-type="local"
       **/
      public abstract void setGlCoaOverHeadCostAccount(String II_GL_OHD_CST_ACCNT);

      /**
       * @ejb:persistent-field
       * @ejb:interface-method view-type="local"
      *
      * @jboss:column-name name="II_GL_FRGHT_CST_ACCNT"
      **/
      public abstract String getGlCoaFreightCostAccount();
      /**
       * @ejb:interface-method view-type="local"
       **/
      public abstract void setGlCoaFreightCostAccount(String II_GL_FRGHT_CST_ACCNT);

      /**
       * @ejb:persistent-field
       * @ejb:interface-method view-type="local"
      *
      * @jboss:column-name name="II_GL_FXD_CST_ACCNT"
      **/
      public abstract String getGlCoaFixedCostAccount();
      /**
       * @ejb:interface-method view-type="local"
       **/
      public abstract void setGlCoaFixedCostAccount(String II_GL_FXD_CST_ACCNT);

      /**
       * @ejb:persistent-field
       * @ejb:interface-method view-type="local"
      *
      * @jboss:column-name name="II_CNDTN"
      **/
      public abstract String getIiCondition();
      /**
       * @ejb:interface-method view-type="local"
       **/
      public abstract void setIiCondition(String II_CNDTN);

      /**
       * @ejb:persistent-field
       * @ejb:interface-method view-type="local"
      *
      * @jboss:column-name name="II_TX_CD"
      **/
      public abstract String getIiTaxCode();
      /**
       * @ejb:interface-method view-type="local"
       **/
      public abstract void setIiTaxCode(String II_TX_CD);
      /**
       * @ejb:persistent-field
       * @ejb:interface-method view-type="local"
      *
      * @jboss:column-name name="II_AD_CMPNY"
      **/
      public abstract Integer getIiAdCompany();
      /**
       * @ejb:interface-method view-type="local"
       **/
      public abstract void setIiAdCompany(Integer II_AD_CMPNY);
      
      
      /**
       * @ejb:persistent-field
       * @ejb:interface-method view-type="local"
      *
      * @jboss:column-name name="II_CRTD_BY"
      **/
      public abstract String getIiCreatedBy();
      /**
       * @ejb:interface-method view-type="local"
       **/
      public abstract void setIiCreatedBy(String II_CRTD_BY);
      
      /**
       * @ejb:persistent-field
       * @ejb:interface-method view-type="local"
      *
      * @jboss:column-name name="II_DT_CRTD"
      **/
      public abstract java.util.Date getIiDateCreated();
      /**
       * @ejb:interface-method view-type="local"
       **/
      public abstract void setIiDateCreated(java.util.Date II_CRTD_BY);
      
      
      /**
       * @ejb:persistent-field
       * @ejb:interface-method view-type="local"
      *
      * @jboss:column-name name="II_LST_MDFD_BY"
      **/
      public abstract String getIiLastModifiedBy();
      /**
       * @ejb:interface-method view-type="local"
       **/
      public abstract void setIiLastModifiedBy(String II_LST_MDFD_BY);
      
      /**
       * @ejb:persistent-field
       * @ejb:interface-method view-type="local"
      *
      * @jboss:column-name name="II_DT_LST_MDFD"
      **/
      public abstract java.util.Date getIiDateLastModified();
      /**
       * @ejb:interface-method view-type="local"
       **/
      public abstract void setIiDateLastModified(java.util.Date II_DT_LST_MDFD);
      


    // RELATIONSHIP METHODS

     /**
      * @ejb:interface-method view-type="local"
      * @ejb:relation name="unitofmeasure-items"
      *               role-name="item-has-one-unitofmeasure"
      *               cascade-delete="no"
      *
      * @jboss:relation related-pk-field="uomCode"
      *                 fk-column="INV_UNIT_OF_MEASURE"
      */
     public abstract LocalInvUnitOfMeasure getInvUnitOfMeasure();
     /**
      * @ejb:interface-method view-type="local"
      **/
     public abstract void setInvUnitOfMeasure(LocalInvUnitOfMeasure invUnitOfMeasure);

     /**
      * @ejb:interface-method view-type="local"
      * @ejb:relation name="item-billofmaterials"
      *               role-name="item-has-many-billofmaterials"
      *
      */
     public abstract Collection getInvBillOfMaterials();
     public abstract void setInvBillOfMaterials(Collection invBillOfMaterials);

     /**
      * @ejb:interface-method view-type="local"
      * @ejb:relation name="item-itemlocations"
      *               role-name="item-has-many-itemlocations"
      */
     public abstract Collection getInvItemLocations();
     public abstract void setInvItemLocations(Collection invItemLocations);

     /**
      * @ejb:interface-method view-type="local"
      * @ejb:relation name="item-transactionalbudget"
      *               role-name="item-has-many-transactionalbudget"
      */
     public abstract Collection getInvTransactionalBudget();
     public abstract void setInvTransactionalBudget(Collection invTransactionalBudget);

     /**
      * @ejb:interface-method view-type="local"
      * @ejb:relation name="item-buildorderstocks"
      *               role-name="item-has-many-buildorderstocks"
      *
      */
     public abstract Collection getInvBuildOrderStocks();
     public abstract void setInvBuildOrderStocks(Collection invBuildOrderStocks);

     /**
       * @ejb:interface-method view-type="local"
       * @ejb:relation name="item-stocktransferlines"
       *               role-name="item-has-many-stocktransferlines"
       */
     public abstract Collection getInvStockTransferLines();
     public abstract void setInvStockTransferLines(Collection invStockTransferLines);

     /**
      * @ejb:interface-method view-type="local"
      * @ejb:relation name="supplier-items"
      *               role-name="item-has-one-supplier"
      *               cascade-delete="no"
      *
      * @jboss:relation related-pk-field="splCode"
      *                 fk-column="AP_SUPPLIER"
      */
     public abstract LocalApSupplier getApSupplier();
     /**
      * @ejb:interface-method view-type="local"
      **/
     public abstract void setApSupplier(LocalApSupplier apSupplier);

     /**
      * @ejb:interface-method view-type="local"
      * @ejb:relation name="customer-items"
      *               role-name="item-has-one-customer"
      *               cascade-delete="no"
      *
      * @jboss:relation related-pk-field="cstCode"
      *                 fk-column="AR_CUSTOMER"
      */
     public abstract LocalArCustomer getArCustomer();
     /**
      * @ejb:interface-method view-type="local"
      **/
     public abstract void setArCustomer(LocalArCustomer arCustomer);


     /**
      * @ejb:interface-method view-type="local"
      * @ejb:relation name="item-pricelevels"
      *               role-name="item-has-many-pricelevels"
      */
     public abstract Collection getInvPriceLevels();
     public abstract void setInvPriceLevels(Collection invPriceLevels);
     
     
   
       /**
      * @ejb:interface-method view-type="local"
      * @ejb:relation name="item-pricelevelsdate"
      *               role-name="item-has-many-pricelevelsdate"
      */
     public abstract Collection getInvPriceLevelsDate();
     public abstract void setInvPriceLevelsDate(Collection invPriceLevelsDate);

     /**
      * @ejb:interface-method view-type="local"
      * @ejb:relation name="item-unitofmeasureconversions"
      *               role-name="item-has-many-unitofmeasureconversions"
      */
     public abstract Collection getInvUnitOfMeasureConversions();
     public abstract void setInvUnitOfMeasureConversions(Collection invUnitOfMeasureConversions);

 	/**
 	 * @jboss:dynamic-ql
 	 */
 	public abstract Collection ejbSelectGeneric(java.lang.String jbossQl, java.lang.Object[] args)
 	throws FinderException;

 	// BUSINESS METHODS

 	/**
 	 * @ejb:home-method view-type="local"
 	 */
 	public Collection ejbHomeGetIiByCriteria(java.lang.String jbossQl, java.lang.Object[] args)
 	throws FinderException {

 		return ejbSelectGeneric(jbossQl, args);

 	}

     /**
      * @ejb:interface-method view-type="local"
      **/
     public void addInvBillOfMaterial(LocalInvBillOfMaterial invBillOfMaterial) {

          Debug.print("InvItemBean addInvBillOfMaterial");

          try {

             Collection invBillOfMaterials = getInvBillOfMaterials();
             invBillOfMaterials.add(invBillOfMaterial);

          } catch (Exception ex) {

            throw new EJBException(ex.getMessage());

          }
     }

     /**
      * @ejb:interface-method view-type="local"
      **/
     public void dropInvBillOfMaterial(LocalInvBillOfMaterial invBillOfMaterial) {

          Debug.print("InvItemBean dropInvBillOfMaterial");

          try {

             Collection invBillOfMaterials = getInvBillOfMaterials();
	         invBillOfMaterials.remove(invBillOfMaterial);

          } catch (Exception ex) {

            throw new EJBException(ex.getMessage());

          }

     }

	 /**
	  * @ejb:interface-method view-type="local"
	  **/
	 public void addInvItemLocation(LocalInvItemLocation invItemLocation) {

	      Debug.print("InvItemBean addInvItemLocation");

	      try {

	      Collection invItemLocations = getInvItemLocations();
		      invItemLocations.add(invItemLocation);

	      } catch (Exception ex) {

	          throw new EJBException(ex.getMessage());

	      }

	 }

     /**
      * @ejb:interface-method view-type="local"
      **/
     public void dropInvItemLocation(LocalInvItemLocation invItemLocation) {

		  Debug.print("InvItemBean dropInvItemLocation");

		  try {

		     Collection invItemLocations = getInvItemLocations();
		     invItemLocations.remove(invItemLocation);

		  } catch (Exception ex) {

		     throw new EJBException(ex.getMessage());

		  }

     }


     /**
	  * @ejb:interface-method view-type="local"
	  **/
	 public void addInvBuildOrderStock(LocalInvBuildOrderStock invBuildOrderStock) {

	      Debug.print("InvItemBean addInvBuildOrderStock");

	      try {

	      Collection invBuildOrderStocks = getInvBuildOrderStocks();
		      invBuildOrderStocks.add(invBuildOrderStock);

	      } catch (Exception ex) {

	          throw new EJBException(ex.getMessage());

	      }

	 }

     /**
      * @ejb:interface-method view-type="local"
      **/
     public void dropInvBuildOrderStock(LocalInvBuildOrderStock invBuildOrderStock) {

		  Debug.print("InvItemBean dropInvBuildOrderStock");

		  try {

		     Collection invBuildOrderStocks = getInvBuildOrderStocks();
		     invBuildOrderStocks.remove(invBuildOrderStock);

		  } catch (Exception ex) {

		     throw new EJBException(ex.getMessage());

		  }

     }

     /**
	  * @ejb:interface-method view-type="local"
	  **/
	 public void addInvStockTransferLine(LocalInvStockTransferLine invStockTransferLine) {

	    Debug.print("InvItemLocationBean addInvStockTransferLine");
	    try {
	       Collection invStockTransferLines = getInvStockTransferLines();
	       invStockTransferLines.add(invStockTransferLine);
	    } catch (Exception ex) {
	       throw new EJBException(ex.getMessage());
	    }
	 }

	 /**
	  * @ejb:interface-method view-type="local"
	  **/
	 public void dropInvStockTransferLine(LocalInvStockTransferLine invStockTransferLine) {

	    Debug.print("InvItemBean dropInvStockTransferLine");
	    try {
	       Collection invStockTransferLines = getInvStockTransferLines();
	       invStockTransferLines.remove(invStockTransferLine);
	    } catch (Exception ex) {
	       throw new EJBException(ex.getMessage());
	    }
	 }

	 /**
	  * @ejb:interface-method view-type="local"
	  **/
	 public void addInvPriceLevel(LocalInvPriceLevel invPriceLevel) {

	      Debug.print("InvItemBean addInvPriceLevel");

	      try {

	      Collection invPriceLevels = getInvPriceLevels();
		      invPriceLevels.add(invPriceLevel);

	      } catch (Exception ex) {

	          throw new EJBException(ex.getMessage());

	      }

	 }

     /**
      * @ejb:interface-method view-type="local"
      **/
     public void dropInvPriceLevel(LocalInvPriceLevel invPriceLevel) {

		  Debug.print("InvItemBean dropInvPriceLevel");

		  try {

		     Collection invPriceLevels = getInvPriceLevels();
		     invPriceLevels.remove(invPriceLevel);

		  } catch (Exception ex) {

		     throw new EJBException(ex.getMessage());

		  }

     }

	 /**
	  * @ejb:interface-method view-type="local"
	  **/
	 public void addInvUnitOfMeasureConversion(LocalInvUnitOfMeasureConversion invUnitOfMeasureConversion) {

	    Debug.print("InvUnitOfMeasureConversionBean addInvUnitOfMeasureConversion");

	    try {

	       Collection invUnitOfMeasureConversions = getInvUnitOfMeasureConversions();
	       invUnitOfMeasureConversions.add(invUnitOfMeasureConversion);

	    } catch (Exception ex) {

	       throw new EJBException(ex.getMessage());

	    }
	 }

	 /**
	  * @ejb:interface-method view-type="local"
	  **/
	 public void dropInvUnitOfMeasureConversion(LocalInvUnitOfMeasureConversion invUnitOfMeasureConversion) {

	    Debug.print("InvUnitOfMeasureConversionBean dropInvUnitOfMeasureConversion");

	    try {

	       Collection invUnitOfMeasureConversions = getInvUnitOfMeasureConversions();
	       invUnitOfMeasureConversions.remove(invUnitOfMeasureConversion);

	    } catch (Exception ex) {

	       throw new EJBException(ex.getMessage());

	    }
	 }

     // ENTITY METHODS

     /**
      * @ejb:create-method view-type="local"
      **/
     public Integer ejbCreate(Integer II_CODE, String II_NM, String II_DESC,
     		String II_PRT_NMBR, String II_SHRT_NM, String II_BR_CD_1, String II_BR_CD_2, String II_BR_CD_3, String II_BRND, String II_CLSS, String II_AD_LV_CTGRY,
			String II_CST_MTHD, double II_UNT_CST, double II_SLS_PRC, byte II_ENBL, byte II_VS_ITM,
			byte II_ENBL_AT_BLD, String II_DNNSS, String II_SDNGS, String II_RMRKS,
			byte II_SRVC_CHRG, byte II_DN_IN_CHRG, byte II_SRVCS, byte II_JB_SRVCS,
			byte II_IS_VAT_RLF, byte II_IS_TX, byte II_IS_PRJCT,
			double II_PRCNT_MRKP, double II_SHPPNG_CST, double II_SPCFC_GRVTY, double II_STNDRD_FLL_SZ,double II_YLD,
			double II_LBR_CST, double II_PWR_CST, double II_OHD_CST, double II_FRGHT_CST, double II_FXD_CST,
			String II_GL_LBR_CST_ACCNT, String II_GL_PWR_CST_ACCNT, String II_GL_OHD_CST_ACCNT, String II_GL_FRGHT_CST_ACCNT, String II_GL_FXD_CST_ACCNT,

			double II_STD_LST_PRCNTG, double II_MRKP_VAL,
			String II_MRKT, byte II_ENBL_PO, short II_PO_CYCL, String II_UMC_PCKGNG, Integer II_RETAIL_UOM, byte II_OPN_PRDCT,
			byte II_FXD_ASST, java.util.Date II_DT_ACQRD, Integer II_DFLT_LCTN, Integer II_AD_CMPNY)
         throws CreateException {

         Debug.print("InvItemBean ejbCreate");

         setIiCode(II_CODE);
         setIiName(II_NM);
         setIiDescription(II_DESC);
         setIiPartNumber(II_PRT_NMBR);
         setIiShortName(II_SHRT_NM);
         setIiBarCode1(II_BR_CD_1);
         setIiBarCode2(II_BR_CD_2);
         setIiBarCode3(II_BR_CD_3);
         setIiBrand(II_BRND);
         setIiClass(II_CLSS);
         setIiAdLvCategory(II_AD_LV_CTGRY);
		 setIiCostMethod(II_CST_MTHD);
		 setIiUnitCost(II_UNT_CST);
         setIiSalesPrice(II_SLS_PRC);
         setIiEnable(II_ENBL);
         setIiVirtualStore(II_VS_ITM);
         setIiEnableAutoBuild(II_ENBL_AT_BLD);
         setIiDoneness(II_DNNSS);
         setIiSidings(II_SDNGS);
         setIiRemarks(II_RMRKS);
         setIiServiceCharge(II_SRVC_CHRG);
         setIiNonInventoriable(II_DN_IN_CHRG);
         setIiServices(II_SRVCS);
         setIiJobServices(II_JB_SRVCS);

         setIiIsVatRelief(II_IS_VAT_RLF);
         setIiIsTax(II_IS_TX);
         setIiIsProject(II_IS_PRJCT);

         setIiPercentMarkup(II_PRCNT_MRKP);
         setIiShippingCost(II_SHPPNG_CST);
         setIiSpecificGravity(II_SPCFC_GRVTY);
         setIiStandardFillSize(II_STNDRD_FLL_SZ);
         setIiYield(II_YLD);

         setIiLaborCost(II_LBR_CST);
         setIiPowerCost(II_PWR_CST);
         setIiOverHeadCost(II_OHD_CST);
         setIiFreightCost(II_FRGHT_CST);
         setIiFreightCost(II_FRGHT_CST);
         setIiFixedCost(II_FXD_CST);

         setGlCoaLaborCostAccount(II_GL_LBR_CST_ACCNT);
         setGlCoaPowerCostAccount(II_GL_PWR_CST_ACCNT);
         setGlCoaOverHeadCostAccount(II_GL_OHD_CST_ACCNT);
         setGlCoaFreightCostAccount(II_GL_FRGHT_CST_ACCNT);
         setGlCoaFixedCostAccount(II_GL_FXD_CST_ACCNT);

         setIiLossPercentage(II_STD_LST_PRCNTG);
         setIiMarkupValue(II_MRKP_VAL);
         setIiMarket(II_MRKT);
         setIiEnablePo(II_ENBL_PO);
         setIiPoCycle(II_PO_CYCL);
         setIiUmcPackaging(II_UMC_PCKGNG);
         setIiRetailUom(II_RETAIL_UOM);
         setIiOpenProduct(II_OPN_PRDCT);
         setIiFixedAsset(II_FXD_ASST);
         setIiDateAcquired(II_DT_ACQRD);
         setIiDefaultLocation(II_DFLT_LCTN);
         setIiAdCompany(II_AD_CMPNY);

         return null;

     }


     /**
      * @ejb:create-method view-type="local"
      **/
     public Integer ejbCreate(String II_NM, String II_DESC,
     		String II_PRT_NMBR, String II_SHRT_NM, String II_BR_CD_1, String II_BR_CD_2, String II_BR_CD_3, String II_BRND, String II_CLSS, String II_AD_LV_CTGRY,
			String II_CST_MTHD, double II_UNT_CST, double II_SLS_PRC, byte II_ENBL, byte II_VS_ITM,
			byte II_ENBL_AT_BLD, String II_DNNSS, String II_SDNGS, String II_RMRKS,
			byte II_SRVC_CHRG, byte II_DN_IN_CHRG,  byte II_SRVCS, byte II_JB_SRVCS, byte II_IS_VAT_RLF, byte II_IS_TX, byte II_IS_PRJCT,
			double II_PRCNT_MRKP, double II_SHPPNG_CST, double II_SPCFC_GRVTY, double II_STNDRD_FLL_SZ,double II_YLD,
			double II_LBR_CST, double II_PWR_CST, double II_OHD_CST, double II_FRGHT_CST, double II_FXD_CST,
			String II_GL_LBR_CST_ACCNT, String II_GL_PWR_CST_ACCNT, String II_GL_OHD_CST_ACCNT, String II_GL_FRGHT_CST_ACCNT, String II_GL_FXD_CST_ACCNT,

			double II_STD_LST_PRCNTG, double II_MRKP_VAL,
			String II_MRKT, byte II_ENBL_PO, short II_PO_CYCL, String II_UMC_PCKGNG, Integer II_RETAIL_UOM, byte II_OPN_PRDCT,
			byte II_FXD_ASST, java.util.Date II_DT_ACQRD, Integer II_DFLT_LCTN, String II_TX_CD,
			byte II_SC_SNDY, byte II_SC_MNDY, byte II_SC_TSDY, byte II_SC_WDNSDY, byte II_SC_THRSDY, byte II_SC_FRDY, byte II_SC_STRDY,
			Integer II_AD_CMPNY)
         throws CreateException {

         Debug.print("InvItemBean ejbCreate");

         setIiName(II_NM);
         setIiDescription(II_DESC);
         setIiPartNumber(II_PRT_NMBR);
         setIiShortName(II_SHRT_NM);
         setIiBarCode1(II_BR_CD_1);
         setIiBarCode2(II_BR_CD_2);
         setIiBarCode3(II_BR_CD_3);
         setIiBrand(II_BRND);
         setIiClass(II_CLSS);
         setIiAdLvCategory(II_AD_LV_CTGRY);
		 setIiCostMethod(II_CST_MTHD);
		 setIiUnitCost(II_UNT_CST);
         setIiSalesPrice(II_SLS_PRC);
         setIiEnable(II_ENBL);
         setIiVirtualStore(II_VS_ITM);
         setIiEnableAutoBuild(II_ENBL_AT_BLD);
         setIiDoneness(II_DNNSS);
         setIiSidings(II_SDNGS);
         setIiRemarks(II_RMRKS);
         setIiServiceCharge(II_SRVC_CHRG);
         setIiNonInventoriable(II_DN_IN_CHRG);
         setIiServices(II_SRVCS);
         setIiJobServices(II_JB_SRVCS);
         setIiIsVatRelief(II_IS_VAT_RLF);
         setIiIsTax(II_IS_TX);
         setIiIsProject(II_IS_PRJCT);

         setIiPercentMarkup(II_PRCNT_MRKP);
         setIiShippingCost(II_SHPPNG_CST);
         setIiSpecificGravity(II_SPCFC_GRVTY);
         setIiStandardFillSize(II_STNDRD_FLL_SZ);
         setIiYield(II_YLD);

         setIiLaborCost(II_LBR_CST);
         setIiPowerCost(II_PWR_CST);
         setIiOverHeadCost(II_OHD_CST);
         setIiFreightCost(II_FRGHT_CST);
         setIiFreightCost(II_FRGHT_CST);
         setIiFixedCost(II_FXD_CST);

         setGlCoaLaborCostAccount(II_GL_LBR_CST_ACCNT);
         setGlCoaPowerCostAccount(II_GL_PWR_CST_ACCNT);
         setGlCoaOverHeadCostAccount(II_GL_OHD_CST_ACCNT);
         setGlCoaFreightCostAccount(II_GL_FRGHT_CST_ACCNT);
         setGlCoaFixedCostAccount(II_GL_FXD_CST_ACCNT);

         setIiLossPercentage(II_STD_LST_PRCNTG);
         setIiMarkupValue(II_MRKP_VAL);
         setIiMarket(II_MRKT);
         setIiEnablePo(II_ENBL_PO);
         setIiPoCycle(II_PO_CYCL);
         setIiUmcPackaging(II_UMC_PCKGNG);
         setIiRetailUom(II_RETAIL_UOM);
         setIiOpenProduct(II_OPN_PRDCT);
         setIiFixedAsset(II_FXD_ASST);
         setIiDateAcquired(II_DT_ACQRD);
         setIiDefaultLocation(II_DFLT_LCTN);

         setIiScSunday(II_SC_SNDY);
         setIiScMonday(II_SC_MNDY);
         setIiScTuesday(II_SC_TSDY);
         setIiScWednesday(II_SC_WDNSDY);
         setIiScThursday(II_SC_THRSDY);
         setIiScFriday(II_SC_FRDY);
         setIiScSaturday(II_SC_STRDY);


         setIiAdCompany(II_AD_CMPNY);

         return null;

     }

     /**
      * @ejb:create-method view-type="local"
      **/
     public Integer ejbCreate(String II_NM, String II_DESC,
     		String II_PRT_NMBR, String II_SHRT_NM, String II_BR_CD_1, String II_BR_CD_2, String II_BR_CD_3, String II_BRND, String II_CLSS, String II_AD_LV_CTGRY,
			String II_CST_MTHD, double II_UNT_CST, double II_SLS_PRC, byte II_ENBL, byte II_VS_ITM,
			byte II_ENBL_AT_BLD, String II_DNNSS, String II_SDNGS, String II_RMRKS,
			byte II_SRVC_CHRG, byte II_DN_IN_CHRG,  byte II_SRVCS, byte II_JB_SRVCS, byte II_IS_VAT_RLF, byte II_IS_TX, byte II_IS_PRJCT,
			double II_PRCNT_MRKP, double II_SHPPNG_CST, double II_SPCFC_GRVTY, double II_STNDRD_FLL_SZ, double II_YLD,
			double II_LBR_CST, double II_PWR_CST, double II_OHD_CST, double II_FRGHT_CST, double II_FXD_CST,
			String II_GL_LBR_CST_ACCNT, String II_GL_PWR_CST_ACCNT, String II_GL_OHD_CST_ACCNT, String II_GL_FRGHT_CST_ACCNT, String II_GL_FXD_CST_ACCNT,

			double II_STD_LST_PRCNTG, double II_MRKP_VAL,
			String II_MRKT, byte II_ENBL_PO, short II_PO_CYCL, String II_UMC_PCKGNG, Integer II_RETAIL_UOM, byte II_OPN_PRDCT,
			byte II_FXD_ASST, java.util.Date II_DT_ACQRD, Integer II_DFLT_LCTN, byte II_TRC_MSC,
			byte II_SC_SNDY, byte II_SC_MNDY, byte II_SC_TSDY, byte II_SC_WDNSDY, byte II_SC_THRSDY, byte II_SC_FRDY, byte II_SC_STRDY,
			double II_ACQSTN_CST, double II_LF_SPN, double II_RSDL_VL, String II_GL_COA_FXD_ASST_ACCNT, String II_GL_COA_DPRCTN_ACCNT,
			String II_GL_ACCMLTD_DPRCTN_ACCNT,

			String II_CNDTN, String II_TX_CD, Integer II_AD_CMPNY)
         throws CreateException {

         Debug.print("InvItemBean ejbCreate");

         setIiName(II_NM);
         setIiDescription(II_DESC);
         setIiPartNumber(II_PRT_NMBR);
         setIiShortName(II_SHRT_NM);
         setIiBarCode1(II_BR_CD_1);
         setIiBarCode2(II_BR_CD_2);
         setIiBarCode3(II_BR_CD_3);
         setIiBrand(II_BRND);

         setIiClass(II_CLSS);
         setIiAdLvCategory(II_AD_LV_CTGRY);
		 setIiCostMethod(II_CST_MTHD);
		 setIiUnitCost(II_UNT_CST);
         setIiSalesPrice(II_SLS_PRC);
         setIiEnable(II_ENBL);
         setIiVirtualStore(II_VS_ITM);
         setIiEnableAutoBuild(II_ENBL_AT_BLD);
         setIiDoneness(II_DNNSS);
         setIiSidings(II_SDNGS);
         setIiRemarks(II_RMRKS);
         setIiServiceCharge(II_SRVC_CHRG);
         setIiNonInventoriable(II_DN_IN_CHRG);
         setIiServices(II_SRVCS);
         setIiJobServices(II_JB_SRVCS);

         setIiIsVatRelief(II_IS_VAT_RLF);
         setIiIsTax(II_IS_TX);
         setIiIsProject(II_IS_PRJCT);

         setIiPercentMarkup(II_PRCNT_MRKP);
         setIiShippingCost(II_SHPPNG_CST);
         setIiSpecificGravity(II_SPCFC_GRVTY);
         setIiStandardFillSize(II_STNDRD_FLL_SZ);
         setIiYield(II_YLD);

         setIiLaborCost(II_LBR_CST);
         setIiPowerCost(II_PWR_CST);
         setIiOverHeadCost(II_OHD_CST);
         setIiFreightCost(II_FRGHT_CST);
         setIiFreightCost(II_FRGHT_CST);
         setIiFixedCost(II_FXD_CST);

         setGlCoaLaborCostAccount(II_GL_LBR_CST_ACCNT);
         setGlCoaPowerCostAccount(II_GL_PWR_CST_ACCNT);
         setGlCoaOverHeadCostAccount(II_GL_OHD_CST_ACCNT);
         setGlCoaFreightCostAccount(II_GL_FRGHT_CST_ACCNT);
         setGlCoaFixedCostAccount(II_GL_FXD_CST_ACCNT);

         setIiAcquisitionCost(II_ACQSTN_CST);
         setIiLifeSpan(II_LF_SPN);
         setIiResidualValue(II_RSDL_VL);
         setGlCoaFixedAssetAccount(II_GL_COA_FXD_ASST_ACCNT);
         setGlCoaDepreciationAccount(II_GL_COA_DPRCTN_ACCNT);
         setGlCoaAccumulatedDepreciationAccount(II_GL_ACCMLTD_DPRCTN_ACCNT);

         setIiLossPercentage(II_STD_LST_PRCNTG);
         setIiMarkupValue(II_MRKP_VAL);
         setIiMarket(II_MRKT);
         setIiEnablePo(II_ENBL_PO);
         setIiPoCycle(II_PO_CYCL);
         setIiUmcPackaging(II_UMC_PCKGNG);
         setIiRetailUom(II_RETAIL_UOM);
         setIiOpenProduct(II_OPN_PRDCT);
         setIiFixedAsset(II_FXD_ASST);
         setIiDateAcquired(II_DT_ACQRD);
         setIiDefaultLocation(II_DFLT_LCTN);
         setIiTraceMisc(II_TRC_MSC);

         setIiScSunday(II_SC_SNDY);
         setIiScMonday(II_SC_MNDY);
         setIiScTuesday(II_SC_TSDY);
         setIiScWednesday(II_SC_WDNSDY);
         setIiScThursday(II_SC_THRSDY);
         setIiScFriday(II_SC_FRDY);
         setIiScSaturday(II_SC_STRDY);
         setIiCondition(II_CNDTN);
         setIiTaxCode(II_TX_CD);
         setIiAdCompany(II_AD_CMPNY);

         return null;

     }

     public void ejbPostCreate(Integer II_CODE, String II_NM, String II_DESC,
     		String II_PRT_NMBR,  String II_SHRT_NM, String II_BR_CD_1, String II_BR_CD_2, String II_BR_CD_3, String II_BRND, String II_CLSS, String II_AD_LV_CTGRY,
			String II_CST_MTHD, double II_UNT_CST, double II_SLS_PRC, byte II_ENBL, byte II_VS_ITM,
			byte II_ENBL_AT_BLD, String II_DNNSS, String II_SDNGS, String II_RMRKS,
			byte II_SRVC_CHRG, byte II_DN_IN_CHRG,  byte II_SRVCS, byte II_JB_SRVCS, byte II_IS_VAT_RLF, byte II_IS_TX, byte II_IS_PRJCT,
			double II_PRCNT_MRKP, double II_SHPPNG_CST, double II_SPCFC_GRVTY, double II_STNDRD_FLL_SZ,double II_YLD,
			double II_LBR_CST, double II_PWR_CST, double II_OHD_CST, double II_FRGHT_CST, double II_FXD_CST,
			String II_GL_LBR_CST_ACCNT, String II_GL_PWR_CST_ACCNT, String II_GL_OHD_CST_ACCNT, String II_GL_FRGHT_CST_ACCNT, String II_GL_FXD_CST_ACCNT,
			double II_STD_LST_PRCNTG, double II_MRKP_VAL,
			String II_MRKT, byte II_ENBL_PO, short II_PO_CYCL, String II_UMC_PCKGNG, Integer II_RETAIL_UOM, byte II_OPN_PRDCT,
			byte II_FXD_ASST, java.util.Date II_DT_ACQRD, Integer II_DFLT_LCTN, Integer II_AD_CMPNY)
         throws CreateException { }

     public void ejbPostCreate(String II_NM, String II_DESC,
     		String II_PRT_NMBR,  String II_SHRT_NM, String II_BR_CD_1, String II_BR_CD_2, String II_BR_CD_3, String II_BRND, String II_CLSS, String II_AD_LV_CTGRY,
			String II_CST_MTHD, double II_UNT_CST, double II_SLS_PRC, byte II_ENBL, byte II_VS_ITM,
			byte II_ENBL_AT_BLD, String II_DNNSS, String II_SDNGS, String II_RMRKS,
			byte II_SRVC_CHRG, byte II_DN_IN_CHRG,  byte II_SRVCS, byte II_JB_SRVCS, byte II_IS_VAT_RLF, byte II_IS_TX, byte II_IS_PRJCT,
			double II_PRCNT_MRKP, double II_SHPPNG_CST, double II_SPCFC_GRVTY, double II_STNDRD_FLL_SZ,double II_YLD,
			double II_LBR_CST, double II_PWR_CST, double II_OHD_CST, double II_FRGHT_CST, double II_FXD_CST,
			String II_GL_LBR_CST_ACCNT, String II_GL_PWR_CST_ACCNT, String II_GL_OHD_CST_ACCNT, String II_GL_FRGHT_CST_ACCNT, String II_GL_FXD_CST_ACCNT,
			double II_STD_LST_PRCNTG, double II_MRKP_VAL,
			String II_MRKT, byte II_ENBL_PO, short II_PO_CYCL, String II_UMC_PCKGNG, Integer II_RETAIL_UOM, byte II_OPN_PRDCT,
			byte II_FXD_ASST, java.util.Date II_DT_ACQRD, Integer II_DFLT_LCTN, String II_TX_CD,
			byte II_SC_SNDY, byte II_SC_MNDY, byte II_SC_TSDY, byte II_SC_WDNSDY, byte II_SC_THRSDY, byte II_SC_FRDY, byte II_SC_STRDY,
			Integer II_AD_CMPNY)
         throws CreateException { }

     public void ejbPostCreate(String II_NM, String II_DESC,
      		String II_PRT_NMBR,  String II_SHRT_NM, String II_BR_CD_1, String II_BR_CD_2, String II_BR_CD_3, String II_BRND, String II_CLSS, String II_AD_LV_CTGRY,
 			String II_CST_MTHD, double II_UNT_CST, double II_SLS_PRC, byte II_ENBL, byte II_VS_ITM,
 			byte II_ENBL_AT_BLD, String II_DNNSS, String II_SDNGS, String II_RMRKS,
 			byte II_SRVC_CHRG, byte II_DN_IN_CHRG,   byte II_SRVCS, byte II_JB_SRVCS,  byte II_IS_VAT_RLF, byte II_IS_TX, byte II_IS_PRJCT,
 			double II_PRCNT_MRKP, double II_SHPPNG_CST, double II_SPCFC_GRVTY, double II_STNDRD_FLL_SZ,double II_YLD,
 			double II_LBR_CST, double II_PWR_CST, double II_OHD_CST, double II_FRGHT_CST, double II_FXD_CST,
 			String II_GL_LBR_CST_ACCNT, String II_GL_PWR_CST_ACCNT, String II_GL_OHD_CST_ACCNT, String II_GL_FRGHT_CST_ACCNT, String II_GL_FXD_CST_ACCNT,
 			double II_STD_LST_PRCNTG, double II_MRKP_VAL,
 			String II_MRKT, byte II_ENBL_PO, short II_PO_CYCL, String II_UMC_PCKGNG, Integer II_RETAIL_UOM, byte II_OPN_PRDCT,
 			byte II_FXD_ASST, java.util.Date II_DT_ACQRD, Integer II_DFLT_LCTN, byte II_TRC_MSC,
 			byte II_SC_SNDY, byte II_SC_MNDY, byte II_SC_TSDY, byte II_SC_WDNSDY, byte II_SC_THRSDY, byte II_SC_FRDY, byte II_SC_STRDY,
 			double II_ACQSTN_CST, double II_LF_SPN, double II_DPRCTN_CST, String II_GL_COA_FXD_ASST_ACCNT, String II_GL_COA_DPRCTN_ACCNT,
			String II_GL_ACCMLTD_DPRCTN_ACCNT,
			String II_CNDTN, String II_TX_CD, Integer II_AD_CMPNY)
          throws CreateException { }


}

