package com.ejb.inv;

import java.util.Collection;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;

import com.ejb.ad.LocalAdBranchItemLocation;
import com.ejb.ap.LocalApPurchaseOrderLine;
import com.ejb.ap.LocalApPurchaseRequisitionLine;
import com.ejb.ap.LocalApVoucherLineItem;
import com.ejb.ar.LocalArInvoiceLineItem;
import com.ejb.ar.LocalArSalesOrderLine;
import com.ejb.pm.LocalPmProjecting;
import com.util.AbstractEntityBean;
import com.util.Debug;

/**
 * @ejb:bean name="InvItemLocationEJB"
 *           display-name="Item Location Entity"
 *           type="CMP"
 *           cmp-version="2.x"
 *           view-type="local"
 *           local-jndi-name="omega-ejb/InvItemLocationEJB"
 *           schema="InvItemLocation"
 *           primkey-field="ilCode"
 *       
 * @ejb:pk class="java.lang.Integer"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="invuser"
 *                        role-link="invuserlink"
 *
 * @ejb:permission role-name="invuser"
 *
 * @ejb:interface local-class="com.ejb.inv.LocalInvItemLocation"
 *                local-extends="javax.ejb.EJBLocalObject"
 *
 * @ejb:home local-class="com.ejb.inv.LocalInvItemLocationHome"
 *           local-extends="javax.ejb.EJBLocalHome"
 * 
 * @ejb:finder signature="LocalInvItemLocation findByLocNameAndIiName(java.lang.String LOC_NM, java.lang.String II_NM, java.lang.Integer II_AD_CMPNY)"
 *             query="SELECT OBJECT(il) FROM InvItemLocation il WHERE il.invLocation.locName=?1 AND il.invItem.iiName=?2 AND il.ilAdCompany=?3"
 * 
 * @ejb:finder signature="Collection findByIiClassAndLocNameAndIiAdLvCategory(java.lang.String II_CLSS, java.lang.String LOC_NM, java.lang.String II_AD_LV_CTGRY, java.lang.Integer II_AD_CMPNY)"
 *             query="SELECT OBJECT(il) FROM InvItemLocation il WHERE il.invItem.iiClass=?1 AND il.invLocation.locName=?2 AND il.invItem.iiAdLvCategory=?3 AND il.ilAdCompany=?4"
 * 
 * @jboss:query signature="Collection findByIiClassAndLocNameAndIiAdLvCategory(java.lang.String II_CLSS, java.lang.String LOC_NM,  java.lang.String II_AD_LV_CTGRY, java.lang.Integer II_AD_CMPNY)"
 *             query="SELECT OBJECT(il) FROM InvItemLocation il WHERE il.invItem.iiClass=?1 AND il.invLocation.locName=?2 AND il.invItem.iiAdLvCategory=?3 AND il.ilAdCompany=?4 ORDER BY il.invItem.iiName"
 * 
 * @ejb:finder signature="Collection findByIlAll(java.lang.Integer II_AD_CMPNY)"
 *             query="SELECT OBJECT(il) FROM InvItemLocation il WHERE il.ilAdCompany=?1"
 * 
 * @jboss:query signature="Collection findByIlAll(java.lang.Integer II_AD_CMPNY)"
 *             query="SELECT OBJECT(il) FROM InvItemLocation il WHERE il.ilAdCompany=?1"
 * 
 * @ejb:finder signature="Collection findByLocName(java.lang.String LOC_NM, java.lang.Integer II_AD_CMPNY)"
 *             query="SELECT OBJECT(il) FROM InvItemLocation il WHERE il.invLocation.locName=?1 AND il.ilAdCompany=?2"
 * 
 * @jboss:query signature="Collection findByLocName(java.lang.String LOC_NM, java.lang.Integer II_AD_CMPNY)"
 *             query="SELECT OBJECT(il) FROM InvItemLocation il WHERE il.invLocation.locName=?1 AND il.ilAdCompany=?2 ORDER BY il.invItem.iiName"
 * 
 * @ejb:finder signature="Collection findItemByLocNameAndIiAdLvCategory(java.lang.String LOC_NM, java.lang.String II_AD_LV_CTGRY, java.lang.Integer IL_AD_CMPNY)"
 *             query="SELECT OBJECT(il) FROM InvItemLocation il WHERE il.invLocation.locName=?1 AND il.invItem.iiAdLvCategory=?2 AND il.ilAdCompany = ?3"
 * 
 * @jboss:query signature="Collection findItemByLocNameAndIiAdLvCategory(java.lang.String LOC_NM, java.lang.String II_AD_LV_CTGRY, java.lang.Integer IL_AD_CMPNY)"
 *             query="SELECT OBJECT(il) FROM InvItemLocation il WHERE il.invLocation.locName=?1 AND il.invItem.iiAdLvCategory=?2 AND il.ilAdCompany = ?3ORDER BY il.invItem.iiName"
 * 
 * @ejb:finder signature="Collection findByIlGlCoaSalesAccount(java.lang.Integer COA_CODE, java.lang.Integer IL_AD_CMPNY)"
 *             query="SELECT OBJECT(il) FROM InvItemLocation il WHERE il.ilGlCoaSalesAccount  = ?1 AND il.ilAdCompany = ?2"
 *
 * @ejb:finder signature="Collection findByIlGlCoaSalesReturnAccount(java.lang.Integer COA_CODE, java.lang.Integer IL_AD_CMPNY)"
 *             query="SELECT OBJECT(il) FROM InvItemLocation il WHERE il.ilGlCoaSalesReturnAccount  = ?1 AND il.ilAdCompany = ?2"
 * 
 * @ejb:finder signature="Collection findByIlGlCoaInventoryAccount(java.lang.Integer COA_CODE, java.lang.Integer IL_AD_CMPNY)"
 *             query="SELECT OBJECT(il) FROM InvItemLocation il WHERE il.ilGlCoaInventoryAccount  = ?1 AND il.ilAdCompany = ?2"
 * 
 * @ejb:finder signature="Collection findByIlGlCoaCostOfSalesAccount(java.lang.Integer COA_CODE, java.lang.Integer IL_AD_CMPNY)"
 *             query="SELECT OBJECT(il) FROM InvItemLocation il WHERE il.ilGlCoaCostOfSalesAccount  = ?1 AND il.ilAdCompany = ?2"
 * 
 * @ejb:finder signature="Collection findByIiNameAndIiAdLvCategory(java.lang.String II_NM, java.lang.String II_AD_LV_CTGRY, java.lang.Integer II_AD_CMPNY)"
 * 			   query="SELECT OBJECT(il) FROM InvItemLocation il WHERE il.invItem.iiName=?1 AND il.invItem.iiAdLvCategory=?2 AND il.ilAdCompany=?3"
 * 
 * @jboss:query signature="Collection findByIiNameAndIiAdLvCategory(java.lang.String II_NM, java.lang.String II_AD_LV_CTGRY, java.lang.Integer II_AD_CMPNY)"
 * 			   query="SELECT OBJECT(il) FROM InvItemLocation il WHERE il.invItem.iiName=?1 AND il.invItem.iiAdLvCategory=?2 AND il.ilAdCompany=?3"
 * 
 * @ejb:finder signature="Collection findByIiName(java.lang.String II_NM, java.lang.Integer II_AD_CMPNY)"
 * 			   query="SELECT OBJECT(il) FROM InvItemLocation il WHERE il.invItem.iiName=?1 AND il.ilAdCompany=?2"
 * 
 * @jboss:query signature="Collection findByIiName(java.lang.String II_NM, java.lang.Integer II_AD_CMPNY)"
 * 			   query="SELECT OBJECT(il) FROM InvItemLocation il WHERE il.invItem.iiName=?1 AND il.ilAdCompany=?2" 
 *
 * @ejb:finder signature="Collection findByAdBranch(java.lang.Integer AD_BRNCH, java.lang.Integer II_AD_CMPNY)"
 * 			   query="SELECT OBJECT(il) FROM InvItemLocation il, IN(il.adBranchItemLocations)bil WHERE bil.adBranch.brCode=?1 AND il.ilAdCompany=?2"
 * 
 * @jboss:query signature="Collection findByAdBranch(java.lang.Integer AD_BRNCH, java.lang.Integer II_AD_CMPNY)"
 * 			   query="SELECT OBJECT(il) FROM InvItemLocation il, IN(il.adBranchItemLocations)bil WHERE bil.adBranch.brCode=?1 AND il.ilAdCompany=?2 ORDER BY il.invItem.iiName" 
 *
 * @ejb:finder signature="Collection findItemByLocNameAndIiAdLvCategoryAndAdBranch(java.lang.String LOC_NM, java.lang.String II_AD_LV_CTGRY, java.lang.Integer AD_BRNCH, java.lang.Integer IL_AD_CMPNY)"
 *             query="SELECT OBJECT(il) FROM InvItemLocation il, IN(il.adBranchItemLocations)bil WHERE il.invItem.iiEnable=1 AND il.invItem.iiNonInventoriable=0 AND il.invLocation.locName=?1 AND il.invItem.iiAdLvCategory=?2 AND bil.adBranch.brCode=?3 AND il.ilAdCompany=?4"
 * 
 * @jboss:query signature="Collection findItemByLocNameAndIiAdLvCategoryAndAdBranch(java.lang.String LOC_NM, java.lang.String II_AD_LV_CTGRY, java.lang.Integer AD_BRNCH, java.lang.Integer IL_AD_CMPNY)"
 *             query="SELECT OBJECT(il) FROM InvItemLocation il, IN(il.adBranchItemLocations)bil WHERE il.invItem.iiEnable=1 AND il.invItem.iiNonInventoriable=0 AND il.invLocation.locName=?1 AND il.invItem.iiAdLvCategory=?2 AND bil.adBranch.brCode=?3 AND il.ilAdCompany=?4 ORDER BY il.invItem.iiName"
 * 
 * @ejb:finder signature="Collection findByIiNameAndAdBranch(java.lang.String II_NM, java.lang.Integer AD_BRNCH, java.lang.Integer II_AD_CMPNY)"
 *             query="SELECT OBJECT(il) FROM InvItemLocation il, IN(il.adBranchItemLocations)bil WHERE il.invItem.iiName=?1 AND bil.adBranch.brCode=?2 AND il.ilAdCompany=?3"
 *
 * @ejb:finder signature="Collection findByIlReorderPointAndAdBranch(java.lang.Integer IL_RRDR_PNT, java.lang.Integer AD_BRNCH, java.lang.Integer II_AD_CMPNY)"
 *             query="SELECT OBJECT(il) FROM InvItemLocation il, IN(il.adBranchItemLocations)bil WHERE il.ilReorderPoint > ?1 AND bil.adBranch.brCode=?2 AND il.ilAdCompany=?3"
 *
 * @ejb:finder signature="LocalInvItemLocation findByIiNameAndLocName(java.lang.String II_NM, java.lang.String LOC_NM, java.lang.Integer II_AD_CMPNY)"
 *             query="SELECT OBJECT(il) FROM InvItemLocation il WHERE il.invItem.iiName = ?1 AND il.invLocation.locName = ?2 AND il.ilAdCompany = ?3"
 *
 * @ejb:finder signature="LocalInvItemLocation findByIiNameAndLocNameAndAdBranch(java.lang.String II_NM, java.lang.String LOC_NM, java.lang.Integer AD_BRNCH, java.lang.Integer II_AD_CMPNY)"
 *             query="SELECT OBJECT(il) FROM InvItemLocation il, IN(il.adBranchItemLocations)bil WHERE il.invItem.iiName = ?1 AND il.invLocation.locName = ?2 AND bil.adBranch.brCode=?3 AND  il.ilAdCompany = ?4"
 *
 * @ejb:finder signature="LocalInvItemLocation findByIiCodeAndLocName(java.lang.Integer II_CD, java.lang.String LOC_NM, java.lang.Integer II_AD_CMPNY)"
 *             query="SELECT OBJECT(il) FROM InvItemLocation il WHERE il.invItem.iiCode = ?1 AND il.invLocation.locName = ?2 AND il.ilAdCompany = ?3"
 *
 * @ejb:finder signature="Collection findByIlGlCoaWipAccount(java.lang.Integer COA_CODE, java.lang.Integer IL_AD_CMPNY)"
 *             query="SELECT OBJECT(il) FROM InvItemLocation il WHERE il.ilGlCoaWipAccount  = ?1 AND il.ilAdCompany = ?2"
 * 
 * @ejb:finder signature="Collection findByIlGlCoaAccruedInventoryAccount(java.lang.Integer COA_CODE, java.lang.Integer IL_AD_CMPNY)"
 *             query="SELECT OBJECT(il) FROM InvItemLocation il WHERE il.ilGlCoaAccruedInventoryAccount  = ?1 AND il.ilAdCompany = ?2"
 * 
 * @ejb:select signature="Collection ejbSelectGeneric(java.lang.String jbossQl, java.lang.Object[] args)"
 *             result-type-mapping="Local"
 *             method-intf="LocalHome"
 *             query="SELECT OBJECT(il) FROM InvItemLocation il"
 *
 * @jboss:query signature="Collection ejbSelectGeneric(java.lang.String jbossQl, java.lang.Object[] args)"
 *                         dynamic="true"
 *
 * @ejb:value-object match="*"
 *             name="InvItemLocation"
 *
 * @jboss:persistence table-name="INV_ITM_LCTN"
 *
 * @jboss:entity-command name="mysql-get-generated-keys"
 *
 */

public abstract class InvItemLocationBean extends AbstractEntityBean {
	
	//	 Access methods for persistent fields
	
	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 * @ejb:pk-field
	 *
	 * @jboss:column-name name="IL_CODE"
	 *
	 * @jboss:persistence auto-increment="true"
	 **/
	public abstract java.lang.Integer getIlCode();
	public abstract void setIlCode(java.lang.Integer IL_CODE);
	
	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="IL_RCK"
	 **/
	public abstract String getIlRack();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setIlRack(String IL_RCK);
	
	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="IL_BN"
	 **/
	public abstract String getIlBin();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setIlBin(String IL_BN);

	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="IL_RRDR_PNT"
	 **/
	public abstract double getIlReorderPoint();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setIlReorderPoint(double IL_RRDR_PNT);
	
	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="IL_RRDR_QTY"
	 **/
	public abstract double getIlReorderQuantity();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setIlReorderQuantity(double IL_RRDR_QTY);
	
	
	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="IL_RRDR_LVL"
	 **/
	public abstract double getIlReorderLevel();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setIlReorderLevel(double IL_RRDR_LVL);
	
	
	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="IL_CMMTTD_QTY"
	 **/
	public abstract double getIlCommittedQuantity();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setIlCommittedQuantity(double IL_CMMTTD_QTY);	   
	
	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="IL_GL_COA_SLS_ACCNT"
	 **/
	public abstract java.lang.Integer getIlGlCoaSalesAccount();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setIlGlCoaSalesAccount(java.lang.Integer IL_GL_COA_SLS_ACCNT);
	
	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="IL_GL_COA_INVNTRY_ACCNT"
	 **/
	public abstract java.lang.Integer getIlGlCoaInventoryAccount();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setIlGlCoaInventoryAccount(java.lang.Integer IL_GL_COA_INVNTRY_ACCNT);
	
	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="IL_GL_CST_OF_SLS_ACCNT"
	 **/
	public abstract java.lang.Integer getIlGlCoaCostOfSalesAccount();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setIlGlCoaCostOfSalesAccount(java.lang.Integer IL_GL_CST_OF_SLS_ACCNT);
	
	
	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="IL_GL_COA_WIP_ACCNT"
	 **/
	public abstract java.lang.Integer getIlGlCoaWipAccount();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setIlGlCoaWipAccount(java.lang.Integer IL_GL_COA_WIP_ACCNT);
	
	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="IL_GL_COA_ACCRD_INVNTRY_ACCNT"
	 **/
	public abstract java.lang.Integer getIlGlCoaAccruedInventoryAccount();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setIlGlCoaAccruedInventoryAccount(java.lang.Integer IL_GL_COA_ACCRD_INVNTRY_ACCNT);
	
	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="IL_GL_COA_SLS_RTRN_ACCNT"
	 **/
	public abstract java.lang.Integer getIlGlCoaSalesReturnAccount();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setIlGlCoaSalesReturnAccount(java.lang.Integer IL_GL_COA_SLS_RTRN_ACCNT);
	
	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="IL_SBJCT_TO_CMMSSN"
	 **/
	public abstract byte getIlSubjectToCommission();      
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setIlSubjectToCommission(byte IL_SBJCT_TO_CMMSSN); 

	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="IL_AD_CMPNY"
	 **/
	public abstract java.lang.Integer getIlAdCompany();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setIlAdCompany(java.lang.Integer IL_AD_CMPNY);
	
	// Access methods for relationship fields
	
	/**
	 * @ejb:interface-method view-type="local"
	 * @ejb:relation name="location-itemlocations"
	 *               role-name="itemlocation-has-one-location"
	 *               cascade-delete="yes"
	 *
	 * @jboss:relation related-pk-field="locCode"
	 *                 fk-column="INV_LOCATION"
	 */
	public abstract LocalInvLocation getInvLocation();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setInvLocation(LocalInvLocation invLocation);
	
	/**
	 * @ejb:interface-method view-type="local"
	 * @ejb:relation name="item-itemlocations"
	 *               role-name="itemlocation-has-one-item"
	 *               cascade-delete="yes"
	 *
	 * @jboss:relation related-pk-field="iiCode"
	 *                 fk-column="INV_ITEM"
	 */
	public abstract LocalInvItem getInvItem();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setInvItem(LocalInvItem invItem);
	
	/**
	 * @ejb:interface-method view-type="local"
	 * @ejb:relation name="itemlocation-adjustmentlines"
	 *               role-name="itemlocation-has-many-adjustmentlines"
	 */
	public abstract Collection getInvAdjustmentLines();
	public abstract void setInvAdjustmentLines(Collection invAdjustmentLines);
	
	/**
	 * @ejb:interface-method view-type="local"
	 * @ejb:relation name="itemlocation-tags"
	 *               role-name="itemlocation-has-many-tags"
	 */
	public abstract Collection getInvTags();
	public abstract void setInvTags(Collection invTags);
	
	/**
	 * @ejb:interface-method view-type="local"
	 * @ejb:relation name="itemlocation-physicalinventorylines"
	 *               role-name="itemlocation-has-many-physicalinventorylines"
	 */
	public abstract Collection getInvPhysicalInventoryLines();
	public abstract void setInvPhysicalInventoryLines(Collection invPhysicalInventoryLines);
	
	/**
	 * @ejb:interface-method view-type="local"
	 * @ejb:relation name="itemlocation-buildunbuildassemblylines"
	 *               role-name="itemlocation-has-many-buildunbuildassemblylines"
	 */
	public abstract Collection getInvBuildUnbuildAssemblyLines();
	public abstract void setInvBuildUnbuildAssemblyLines(Collection invBuildUnbuildAssemblyLines);
	
	/**
	 * @ejb:interface-method view-type="local"
	 * @ejb:relation name="itemlocation-costings"
	 *               role-name="itemlocation-has-many-costings"
	 */
	public abstract Collection getInvCostings();
	public abstract void setInvCostings(Collection invCostings);
	
	/**
	 * @ejb:interface-method view-type="local"
	 * @ejb:relation name="itemlocation-projectings"
	 *               role-name="itemlocation-has-many-projectings"
	 */
	public abstract Collection getPmProjectings();
	public abstract void setPmProjectings(Collection pmProjectings);
	
	/**
	 * @ejb:interface-method view-type="local"
	 * @ejb:relation name="itemlocation-voucherlineitems"
	 *               role-name="itemlocation-has-many-voucherlineitems"
	 */
	public abstract Collection getApVoucherLineItems();
	public abstract void setApVoucherLineItems(Collection apVoucherLineItems);
	
	/**
	 * @ejb:interface-method view-type="local"
	 * @ejb:relation name="itemlocation-invoicelineitems"
	 *               role-name="itemlocation-has-many-invoicelineitems"
	 */
	public abstract Collection getArInvoiceLineItems();
	public abstract void setArInvoiceLineItems(Collection arInvoiceLineItems);
	
	/**
	 * @ejb:interface-method view-type="local"
	 * @ejb:relation name="itemlocation-buildorderlines"
	 *               role-name="itemlocation-has-many-buildorderlines"
	 */
	public abstract Collection getInvBuildOrderLines();
	public abstract void setInvBuildOrderLines(Collection invBuildOrderLines);
	
	
	/**
	 * @ejb:interface-method view-type="local"
	 * @ejb:relation name="itemlocation-stockissuancelines"
	 *               role-name="itemlocation-has-many-stockissuancelines"
	 */
	public abstract Collection getInvStockIssuanceLines();
	public abstract void setInvStockIssuanceLines(Collection invStockIssuanceLines);
	
	/**
	 * @ejb:interface-method view-type="local"
	 * @ejb:relation name="itemlocation-purchaseorderlines"
	 *               role-name="itemlocation-has-many-purchaseorderlines"
	 */
	public abstract Collection getApPurchaseOrderLines();
	public abstract void setApPurchaseOrderLines(Collection apPurchaseOrderLines);	   
	
	/**
	 * @ejb:interface-method view-type="local"
	 * @ejb:relation name="itemlocation-salesorderline"
	 *               role-name="itemlocation-has-many-salesorderline"
	 */
	public abstract Collection getArSalesOrderLines();
	public abstract void setArSalesOrderLines(Collection arSalesOrderLines);
	
	/**
	 * @ejb:interface-method view-type="local"
	 * @ejb:relation name="itemlocation-branchitemlocation"
	 *               role-name="itemlocation-has-many-branchitemlocation"
	 */
	public abstract Collection getAdBranchItemLocations();
	public abstract void setAdBranchItemLocations(Collection adBranchItemLocations);
	
	/**
	 * @ejb:interface-method view-type="local"
	 * @ejb:relation name="itemlocation-purchaserequisitionlines"
	 *               role-name="itemlocation-has-many-purchaserequisitionlines"
	 */
	public abstract Collection getApPurchaseRequisitionLines();
	public abstract void setApPurchaseRequisitionLines(Collection apPurchaseRequisitionLines);	
	
	/**
	 * @ejb:interface-method view-type="local"
	 * @ejb:relation name="itemlocation-joborderlines"
	 *               role-name="itemlocation-has-many-joborderlines"
	 */
	public abstract Collection getArJobOrderLines();
	public abstract void setArJobOrderLines(Collection arJobOrderLines);
	
	
	/**
     * @ejb:interface-method view-type="local"
     * @ejb:relation name="itemlocation-branchstocktransferlines"
     *               role-name="itemlocation-has-many-branchstocktransferlines;"
     */
    public abstract Collection getInvBranchStockTransferLines();
    public abstract void setInvBranchStockTransferLines(Collection invBranchStockTransferLines);
    
    /**
	 * @ejb:interface-method view-type="local"
	 * @ejb:relation name="itemlocation-lineitems"
	 *               role-name="itemlocation-has-many-lineitems"
	 */
	public abstract Collection getInvLineItems();
	public abstract void setInvLineItems(Collection invLineItems);
	

	/**
	 * @jboss:dynamic-ql
	 */
	public abstract Collection ejbSelectGeneric(java.lang.String jbossQl, java.lang.Object[] args)
	throws FinderException;
	
	// BUSINESS METHODS
	
	/**
	 * @ejb:home-method view-type="local"
	 */
	public Collection ejbHomeGetIlByCriteria(java.lang.String jbossQl, java.lang.Object[] args)
	throws FinderException {
		
		return ejbSelectGeneric(jbossQl, args);
	}
	
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public void addInvAdjustmentLine(LocalInvAdjustmentLine invAdjustmentLine) {
		
		Debug.print("InvItemLocationBean addInvAdjustmentLine");
		try {
			Collection invAdjustmentLines = getInvAdjustmentLines();
			invAdjustmentLines.add(invAdjustmentLine);
		} catch (Exception ex) {
			throw new EJBException(ex.getMessage());
		}
	}
	
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public void dropInvAdjustmentLine(LocalInvAdjustmentLine invAdjustmentLine) {
		
		Debug.print("InvItemLocationBean dropInvAdjustmentLine");
		try {
			Collection invAdjustmentLines = getInvAdjustmentLines();
			invAdjustmentLines.remove(invAdjustmentLine);
		} catch (Exception ex) {
			throw new EJBException(ex.getMessage());
		}
	}
	
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public void addInvPhysicalInventoryLine(LocalInvPhysicalInventoryLine invPhysicalInventoryLine) {
		
		Debug.print("InvItemLocationBean addInvPhysicalInventoryLine");
		try {
			Collection invPhysicalInventoryLines = getInvPhysicalInventoryLines();
			invPhysicalInventoryLines.add(invPhysicalInventoryLine);
		} catch (Exception ex) {
			throw new EJBException(ex.getMessage());
		}
	}
	
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public void dropInvPhysicalInventoryLine(LocalInvPhysicalInventoryLine invPhysicalInventoryLine) {
		
		Debug.print("InvItemLocationBean dropInvPhysicalInventoryLine");
		try {
			Collection invPhysicalInventoryLines = getInvPhysicalInventoryLines();
			invPhysicalInventoryLines.remove(invPhysicalInventoryLine);
		} catch (Exception ex) {
			throw new EJBException(ex.getMessage());
		}
	}
	
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public void addInvBuildUnbuildAssemblyLine(LocalInvBuildUnbuildAssemblyLine invBuildUnbuildAssemblyLine) {
		
		Debug.print("InvItemLocationBean addInvBuildUnbuildAssemblyLine");
		try {
			Collection invBuildUnbuildAssemblyLines = getInvBuildUnbuildAssemblyLines();
			invBuildUnbuildAssemblyLines.add(invBuildUnbuildAssemblyLine);
		} catch (Exception ex) {
			throw new EJBException(ex.getMessage());
		}
	}
	
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public void dropInvBuildUnbuildAssemblyLine(LocalInvBuildUnbuildAssemblyLine invBuildUnbuildAssemblyLine) {
		
		Debug.print("InvItemLocationBean dropInvBuildUnbuildAssemblyLine");
		try {
			Collection invBuildUnbuildAssemblyLines = getInvBuildUnbuildAssemblyLines();
			invBuildUnbuildAssemblyLines.remove(invBuildUnbuildAssemblyLine);
		} catch (Exception ex) {
			throw new EJBException(ex.getMessage());
		}
	}
	
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public void addInvCosting(LocalInvCosting invCosting) {
		
		Debug.print("InvItemLocationBean addInvCosting");
		try {
			Collection invCostings = getInvCostings();
			invCostings.add(invCosting);
		} catch (Exception ex) {
			throw new EJBException(ex.getMessage());
		}
	}	   
	
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public void dropInvCosting(LocalInvCosting invCosting) {
		
		Debug.print("InvItemLocationBean dropInvCosting");
		try {
			Collection invCostings = getInvCostings();
			invCostings.remove(invCosting);
		} catch (Exception ex) {
			throw new EJBException(ex.getMessage());
		}
	}
	
	
	
	
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public void addPmProjecting(LocalPmProjecting pmProjecting) {
		
		Debug.print("InvItemLocationBean addPmProjecting");
		try {
			Collection pmProjectings = getPmProjectings();
			pmProjectings.add(pmProjecting);
		} catch (Exception ex) {
			throw new EJBException(ex.getMessage());
		}
	}	   
	
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public void dropPmProjecting(LocalPmProjecting pmProjecting) {
		
		Debug.print("InvItemLocationBean dropPmProjecting");
		try {
			Collection pmProjectings = getPmProjectings();
			pmProjectings.remove(pmProjecting);
		} catch (Exception ex) {
			throw new EJBException(ex.getMessage());
		}
	}
	
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public void addApVoucherLineItem(LocalApVoucherLineItem apVoucherLineItem) {
		
		Debug.print("InvItemLocationBean addApVoucherLineItem");
		try {
			Collection apVoucherLineItems = getApVoucherLineItems();
			apVoucherLineItems.add(apVoucherLineItem);
		} catch (Exception ex) {
			throw new EJBException(ex.getMessage());
		}
	}	   
	
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public void dropApVoucherLineItem(LocalApVoucherLineItem apVoucherLineItem) {
		
		Debug.print("InvItemLocationBean dropApVoucherLineItem");
		try {
			Collection apVoucherLineItems = getApVoucherLineItems();
			apVoucherLineItems.remove(apVoucherLineItem);
		} catch (Exception ex) {
			throw new EJBException(ex.getMessage());
		}
	}
	
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public void addArInvoiceLineItem(LocalArInvoiceLineItem arInvoiceLineItem) {
		
		Debug.print("InvItemLocationBean addArInvoiceLineItem");
		
		try {
			
			Collection arInvoiceLineItems = getArInvoiceLineItems();
			arInvoiceLineItems.add(arInvoiceLineItem);
			
		} catch (Exception ex) {
			
			throw new EJBException(ex.getMessage());
			
		}   
	}
	
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public void dropArInvoiceLineItem(LocalArInvoiceLineItem arInvoiceLineItem) {
		
		Debug.print("InvItemLocationBean dropArInvoiceLineItem");
		
		try {
			
			Collection arInvoiceLineItems = getArInvoiceLineItems();
			arInvoiceLineItems.remove(arInvoiceLineItem);
			
		} catch (Exception ex) {
			
			throw new EJBException(ex.getMessage());
			
		}
		
	}
	
	
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public void addInvBuildOrderLine(LocalInvBuildOrderLine invBuildOrderLine) {
		
		Debug.print("InvItemLocationBean addInvBuildOrderLine");
		try {
			Collection invBuildOrderLines = getInvBuildOrderLines();
			invBuildOrderLines.add(invBuildOrderLine);
		} catch (Exception ex) {
			throw new EJBException(ex.getMessage());
		}
	}
	
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public void dropInvBuildOrderLine(LocalInvBuildOrderLine invBuildOrderLine) {
		
		Debug.print("InvItemLocationBean dropInvBuildOrderLine");
		try {
			Collection invBuildOrderLines = getInvBuildOrderLines();
			invBuildOrderLines.remove(invBuildOrderLine);
		} catch (Exception ex) {
			throw new EJBException(ex.getMessage());
		}
	}
	
	
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public void addInvStockIssuanceLine(LocalInvStockIssuanceLine invStockIssuanceLine) {
		
		Debug.print("InvItemLocationBean addInvStockIssuanceLine");
		try {
			Collection invStockIssuanceLines = getInvStockIssuanceLines();
			invStockIssuanceLines.add(invStockIssuanceLine);
		} catch (Exception ex) {
			throw new EJBException(ex.getMessage());
		}
	}
	
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public void dropInvStockIssuanceLine(LocalInvStockIssuanceLine invStockIssuanceLine) {
		
		Debug.print("InvItemLocationBean dropInvStockIssuanceLine");
		try {
			Collection invStockIssuanceLines = getInvStockIssuanceLines();
			invStockIssuanceLines.remove(invStockIssuanceLine);
		} catch (Exception ex) {
			throw new EJBException(ex.getMessage());
		}
	}
	
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public void addApPurchaseOrderLine(LocalApPurchaseOrderLine apPurchaseOrderLine) {
		
		Debug.print("InvItemLocationBean addApPurchaseOrderLine");
		
		try {
			
			Collection apPurchaseOrderLines = getApPurchaseOrderLines();
			apPurchaseOrderLines.add(apPurchaseOrderLine);
			
		} catch (Exception ex) {
			
			throw new EJBException(ex.getMessage());
			
		}   
	}
	
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public void dropApPurchaseOrderLine(LocalApPurchaseOrderLine apPurchaseOrderLine) {
		
		Debug.print("InvItemLocationBean dropApPurchaseOrderLine");
		
		try {
			
			Collection apPurchaseOrderLines = getApPurchaseOrderLines();
			apPurchaseOrderLines.remove(apPurchaseOrderLine);
			
		} catch (Exception ex) {
			
			throw new EJBException(ex.getMessage());
			
		}
		
	}	  
	
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public void addArSalesOrderLine(LocalArSalesOrderLine arSalesOrderLine) {
		
		Debug.print("InvItemLocation addArSalesOrderLine");
		
		try {
			
			Collection arSalesOrderLines = getArSalesOrderLines();
			arSalesOrderLines.add(arSalesOrderLine);
			
		} catch (Exception ex) {
			
			throw new EJBException(ex.getMessage());
			
		}
		
	}
	
	/**
	 * @ejb:interface-method view-type="local"
	 **/ 
	public void dropArSalesOrderLine(LocalArSalesOrderLine arSalesOrderLine) {
		
		Debug.print("InvItemLocation dropArSalesOrderLine");
		
		try {
			
			Collection arSalesOrderLines = getArSalesOrderLines();
			arSalesOrderLines.remove(arSalesOrderLine);
			
		} catch (Exception ex) {
			
			throw new EJBException(ex.getMessage());
			
		}
		
	}

	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public void addAdBranchItemLocation(LocalAdBranchItemLocation adBranchItemLocation) {
		
		Debug.print("InvItemLocation addAdBranchItemLocation");
		
		try {
			
			Collection adBranchItemLocations = getAdBranchItemLocations();
			adBranchItemLocations.add(adBranchItemLocation);
			
		} catch (Exception ex) {
			
			throw new EJBException(ex.getMessage());
			
		}
		
	}
	
	/**
	 * @ejb:interface-method view-type="local"
	 **/ 
	public void dropAdBranchItemLocation(LocalAdBranchItemLocation adBranchItemLocation) {
		
		Debug.print("InvItemLocation dropAdBranchItemLocation");
		
		try {
			
			Collection adBranchItemLocations = getAdBranchItemLocations();
			adBranchItemLocations.remove(adBranchItemLocation);
			
		} catch (Exception ex) {
			
			throw new EJBException(ex.getMessage());
			
		}
		
	}
	
	
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public void addApPurchaseRequisitionLine(LocalApPurchaseRequisitionLine apPurchaseRequisitionLine) {
		
		Debug.print("InvItemLocationBean addApPurchaseRequisitionLine");
		
		try {
			
			Collection apPurchaseRequisitionLines = getApPurchaseRequisitionLines();
			apPurchaseRequisitionLines.add(apPurchaseRequisitionLine);
			
		} catch (Exception ex) {
			
			throw new EJBException(ex.getMessage());
			
		}   
	}
	
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public void dropApPurchaseRequisitionLine(LocalApPurchaseRequisitionLine apPurchaseRequisitionLine) {
		
		Debug.print("InvItemLocationBean dropApPurchaseRequisitionLine");
		
		try {
			
			Collection apPurchaseRequisitionLines = getApPurchaseRequisitionLines();
			apPurchaseRequisitionLines.remove(apPurchaseRequisitionLine);
			
		} catch (Exception ex) {
			
			throw new EJBException(ex.getMessage());
			
		}
		
	}
	
	/**
     * @ejb:interface-method view-type="local"
     **/
    public void addInvBranchStockTransferLine(LocalInvBranchStockTransferLine invBranchStockTransferLine) {

         Debug.print("InvUnitOfMeasureBean addInvBranchStockTransferLine");
     
         try {
     	
            Collection invBranchStockTransferLines = getInvBranchStockTransferLines();
            invBranchStockTransferLines.add(invBranchStockTransferLine);
	          
         } catch (Exception ex) {
     	
           throw new EJBException(ex.getMessage());
     
         }   
    }

    /**
     * @ejb:interface-method view-type="local"
     **/
    public void dropInvBranchStockTransferLine(LocalInvBranchStockTransferLine invBranchStockTransferLine) {

         Debug.print("InvUnitOfMeasureBean dropInvBranchStockTransferLine");
     
         try {
     	
            Collection invBranchStockTransferLines = getInvBranchStockTransferLines();
            invBranchStockTransferLines.remove(invBranchStockTransferLine);
	      
         } catch (Exception ex) {
     	
           throw new EJBException(ex.getMessage());
        
         }
            
    }
    
    /**
	 * @ejb:interface-method view-type="local"
	 **/
	public void addInvLineItem(LocalInvLineItem invLineItem) {
		
		Debug.print("InvItemLocationBean addInvLineItem");
		try {
			Collection invLineItems = getInvLineItems();
			invLineItems.add(invLineItem);
		} catch (Exception ex) {
			throw new EJBException(ex.getMessage());
		}
	}	   
	
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public void dropInvLineItem(LocalInvLineItem invLineItem) {
		
		Debug.print("InvItemLocationBean dropInvLineItem");
		try {
			Collection invLineItems = getInvLineItems();
			invLineItems.remove(invLineItem);
		} catch (Exception ex) {
			throw new EJBException(ex.getMessage());
		}
	}
	
	
	// EntityBean methods
	
	/**
	 * @ejb:create-method view-type="local"
	 **/
	public java.lang.Integer ejbCreate (java.lang.Integer IL_CODE, 
			String IL_RCK, String IL_BN,
			double IL_RRDR_PNT, double IL_RRDR_QTY, double IL_RRDR_LVL, 
			double IL_CMMTTD_QTY, 
	        java.lang.Integer IL_GL_COA_SALES_ACCOUNT, java.lang.Integer IL_GL_COA_INVENTORY_ACCOUNT, 
			java.lang.Integer IL_GL_COA_COST_OF_SALES_ACCOUNT, java.lang.Integer IL_GL_COA_WIP_ACCNT, 
			Integer IL_GL_COA_ACCRD_INVNTRY_ACCNT, Integer IL_GL_COA_SLS_RTRN_ACCNT,
			byte IL_SBJCT_TO_CMMSSN, java.lang.Integer IL_AD_CMPNY)
	throws CreateException {
		
	    Debug.print("InvItemLocationBean ejbCreate");
		setIlCode(IL_CODE);
		setIlRack(IL_RCK);
		setIlBin(IL_BN);
		setIlReorderPoint(IL_RRDR_PNT);
		setIlReorderQuantity(IL_RRDR_QTY);
		setIlReorderLevel(IL_RRDR_LVL);
		setIlCommittedQuantity(IL_CMMTTD_QTY);
		setIlGlCoaSalesAccount(IL_GL_COA_SALES_ACCOUNT);
		setIlGlCoaInventoryAccount(IL_GL_COA_INVENTORY_ACCOUNT);
		setIlGlCoaCostOfSalesAccount(IL_GL_COA_COST_OF_SALES_ACCOUNT);
		setIlGlCoaWipAccount(IL_GL_COA_WIP_ACCNT);
		setIlGlCoaAccruedInventoryAccount(IL_GL_COA_ACCRD_INVNTRY_ACCNT);
		setIlGlCoaSalesReturnAccount(IL_GL_COA_SLS_RTRN_ACCNT);
		setIlSubjectToCommission(IL_SBJCT_TO_CMMSSN);
		setIlAdCompany(IL_AD_CMPNY);
		return null;
	}
	
	/**
	 * @ejb:create-method view-type="local"
	 **/
	public java.lang.Integer ejbCreate (
			String IL_RCK, String IL_BN,
			double IL_RRDR_PNT, double IL_RRDR_QTY, double IL_RRDR_LVL, 
	        double IL_CMMTTD_QTY, java.lang.Integer IL_GL_COA_SALES_ACCOUNT, 
			java.lang.Integer IL_GL_COA_INVENTORY_ACCOUNT, 
			java.lang.Integer IL_GL_COA_COST_OF_SALES_ACCOUNT, java.lang.Integer IL_GL_COA_WIP_ACCNT, Integer IL_GL_COA_ACCRD_INVNTRY_ACCNT, Integer IL_GL_COA_SLS_RTRN_ACCNT,
			byte IL_SBJCT_TO_CMMSSN, java.lang.Integer IL_AD_CMPNY)
	throws CreateException {
		
		Debug.print("InvItemLocationBean ejbCreate");
		
		setIlRack(IL_RCK);
		setIlBin(IL_BN);
		setIlReorderPoint(IL_RRDR_PNT);
		setIlReorderQuantity(IL_RRDR_QTY);
		setIlReorderLevel(IL_RRDR_LVL);
		setIlCommittedQuantity(IL_CMMTTD_QTY);
		setIlGlCoaSalesAccount(IL_GL_COA_SALES_ACCOUNT);
		setIlGlCoaInventoryAccount(IL_GL_COA_INVENTORY_ACCOUNT);
		setIlGlCoaCostOfSalesAccount(IL_GL_COA_COST_OF_SALES_ACCOUNT);
		setIlGlCoaWipAccount(IL_GL_COA_WIP_ACCNT);
		setIlGlCoaAccruedInventoryAccount(IL_GL_COA_ACCRD_INVNTRY_ACCNT);
		setIlGlCoaSalesReturnAccount(IL_GL_COA_SLS_RTRN_ACCNT);
		setIlSubjectToCommission(IL_SBJCT_TO_CMMSSN);
		setIlAdCompany(IL_AD_CMPNY);
		return null;
	}
	
	public void ejbPostCreate (java.lang.Integer IL_CODE, 
			String IL_RCK, String IL_BN,
			double IL_RRDR_PNT, double IL_RRDR_QTY, double IL_RRDR_LVL, 
			double IL_CMMTTD_QTY, java.lang.Integer IL_GL_COA_SALES_ACCOUNT, 
			java.lang.Integer IL_GL_COA_INVENTORY_ACCOUNT, 
			java.lang.Integer IL_GL_COA_COST_OF_SALES_ACCOUNT, java.lang.Integer IL_GL_COA_WIP_ACCNT, Integer IL_GL_COA_ACCRD_INVNTRY_ACCNT, Integer IL_GL_COA_SLS_RTRN_ACCNT,
			byte IL_SBJCT_TO_CMMSSN, java.lang.Integer IL_AD_CMPNY)
	throws CreateException { }
	
	public void ejbPostCreate (
			String IL_RCK, String IL_BN,
			double IL_RRDR_PNT, double IL_RRDR_QTY, double IL_RRDR_LVL, 
			double IL_CMMTTD_QTY, java.lang.Integer IL_GL_COA_SALES_ACCOUNT, 
			java.lang.Integer IL_GL_COA_INVENTORY_ACCOUNT, 
			java.lang.Integer IL_GL_COA_COST_OF_SALES_ACCOUNT, java.lang.Integer IL_GL_COA_WIP_ACCNT, Integer IL_GL_COA_ACCRD_INVNTRY_ACCNT, Integer IL_GL_COA_SLS_RTRN_ACCNT,
			byte IL_SBJCT_TO_CMMSSN, java.lang.Integer IL_AD_CMPNY)
	throws CreateException { }
	
}
