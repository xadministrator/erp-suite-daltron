package com.ejb.inv;

import java.util.Collection;

import javax.ejb.CreateException;
import javax.ejb.FinderException;

import com.ejb.inv.LocalInvLineItemTemplate;
import com.ejb.inv.LocalInvItemLocation;
import com.ejb.inv.LocalInvUnitOfMeasure;
import com.util.AbstractEntityBean;
import com.util.Debug;

/**
 * @ejb:bean name="InvLineItemEJB"
 *           display-name="Line Item Entity"
 *           type="CMP"
 *           cmp-version="2.x"
 *           view-type="local"
 *           local-jndi-name="omega-ejb/InvLineItemEJB"
 *           schema="InvLineItem"
 *           primkey-field="liCode"
 *       
 * @ejb:pk class="java.lang.Integer"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="invuser"
 *                        role-link="invuserlink"
 *
 * @ejb:permission role-name="invuser"
 *
 * @ejb:interface local-class="com.ejb.inv.LocalInvLineItem"
 *                local-extends="javax.ejb.EJBLocalObject"
 *
 * @ejb:home local-class="com.ejb.inv.LocalInvLineItemHome"
 *           local-extends="javax.ejb.EJBLocalHome"
 *
 * @ejb:select signature="Collection ejbSelectGeneric(java.lang.String jbossQl, java.lang.Object[] args)"
 *             result-type-mapping="Local"
 *             method-intf="LocalHome"
 *             query="SELECT OBJECT(li) FROM InvLineItem li"
 * 
 * @jboss:query signature="Collection ejbSelectGeneric(java.lang.String jbossQl, java.lang.Object[] args)"
 *                         dynamic="true"
 *
 * @ejb:value-object match="*"
 *             name="InvLineItem"
 *
 * @jboss:persistence table-name="INV_LN_ITM"
 *
 * @jboss:entity-command name="mysql-get-generated-keys"
 *
 */

public abstract class InvLineItemBean extends AbstractEntityBean {
	
	// Access methods for persistent fields
	
	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 * @ejb:pk-field
	 *
	 * @jboss:column-name name="LI_CODE"
	 *
	 * @jboss:table-name auto-increment="true"
	 **/
	public abstract java.lang.Integer getLiCode();
	public abstract void setLiCode(java.lang.Integer LI_CODE);
	
	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="LI_LN"
	 **/
	public abstract short getLiLine();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setLiLine(short LI_LN);
	
	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="LI_AD_CMPNY"
	 **/
	public abstract Integer getLiAdCompany();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setLiAdCompany(Integer LI_AD_CMPNY);
	
	
	// Access methods for relationship fields
	
	/**
	 * @ejb:interface-method view-type="local"
	 * @ejb:relation name="lineItemTemplate-lineitems"
	 *               role-name="lineitem-has-onelineItemTemplate"
	 *               cascade-delete="yes"
	 *
	 * @jboss:relation related-pk-field="litCode"
	 *                 fk-column="INV_LINE_ITEM_TEMPLATE"
	 */
	public abstract LocalInvLineItemTemplate getInvLineItemTemplate();
	public abstract void setInvLineItemTemplate(LocalInvLineItemTemplate invLienItemTemplate);
	
	/**
	 * @ejb:interface-method view-type="local"
	 * @ejb:relation name="unitofmeasure-lineitems"
	 *               role-name="lineitem-has-one-unitofmeasure"
	 *               cascade-delete="no"
	 *
	 * @jboss:relation related-pk-field="uomCode"
	 *                 fk-column="INV_UNIT_OF_MEASURE"
	 */
	public abstract LocalInvUnitOfMeasure getInvUnitOfMeasure();
	public abstract void setInvUnitOfMeasure(LocalInvUnitOfMeasure invUnitOfMeasure);
	
	/**
	 * @ejb:interface-method view-type="local"
	 * @ejb:relation name="itemlocation-lineitems"
	 *               role-name="lineitem-has-one-itemlocation"
	 *               cascade-delete="no"
	 *
	 * @jboss:relation related-pk-field="ilCode"
	 *                 fk-column="INV_ITEM_LOCATION"
	 */
	public abstract LocalInvItemLocation getInvItemLocation();
	public abstract void setInvItemLocation(LocalInvItemLocation invItemLocation);	  
	
	/**
	 * @jboss:dynamic-ql
	 */
	public abstract Collection ejbSelectGeneric(java.lang.String jbossQl, java.lang.Object[] args)
	throws FinderException;
	
	
	// BUSINESS METHODS
	
	/**
	 * @ejb:home-method view-type="local"
	 */
	public Collection ejbHomeGetLiByCriteria(java.lang.String jbossQl, java.lang.Object[] args)
	throws FinderException {
		
		return ejbSelectGeneric(jbossQl, args);
		
	}
	
	
	// EntityBean methods
	
	/**
	 * @ejb:create-method view-type="local"
	 **/
	public java.lang.Integer ejbCreate (java.lang.Integer LI_CODE, short LI_LN, Integer LI_AD_CMPNY)
	throws CreateException {
		
		Debug.print("InvLineItemBean ejbCreate");
		setLiCode(LI_CODE);
		setLiLine(LI_LN);
		setLiAdCompany(LI_AD_CMPNY);
		
		return null;
	}
	
	/**
	 * @ejb:create-method view-type="local"
	 **/
	public java.lang.Integer ejbCreate (short LI_LN, Integer LI_AD_CMPNY)
	throws CreateException {
		
		Debug.print("InvLineItemBean ejbCreate");
		setLiLine(LI_LN);
		setLiAdCompany(LI_AD_CMPNY);
		
		return null;
	}
	
	public void ejbPostCreate (java.lang.Integer LI_CODE, short LI_LN, Integer LI_AD_CMPNY)
	throws CreateException { }
	
	public void ejbPostCreate (short LI_LN, Integer LI_AD_CMPNY)
	throws CreateException { }
	
}
