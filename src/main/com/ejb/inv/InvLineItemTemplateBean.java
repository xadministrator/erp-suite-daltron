/*
 * com/ejb/inv/LocalInvLineItemTemplateBean.java
 *
 * Created on October 30, 2005, 09:36 AM
 */

package com.ejb.inv;

import java.util.Collection;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;

import com.ejb.ap.LocalApSupplier;
import com.ejb.ar.LocalArCustomer;
import com.util.AbstractEntityBean;
import com.util.Debug;

/**
 *
 * @author  Jolly Martin
 *
 * @ejb:bean name="InvLineItemTemplateEJB"
 *           display-name="Line Item Template Entity"
 *           type="CMP"
 *           cmp-version="2.x"
 *           view-type="local"
 *           local-jndi-name="omega-ejb/InvLineItemTemplateEJB"
 *           schema="InvLineItemTemplate"
 *           primkey-field="litCode"
 *
 * @ejb:pk class="java.lang.Integer"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="invuser"
 *                        role-link="invuserlink"
 *
 * @ejb:permission role-name="invuser"
 *
 * @ejb:interface local-class="com.ejb.inv.LocalInvLineItemTemplate"
 *                local-extends="javax.ejb.EJBLocalObject"
 *
 * @ejb:home local-class="com.ejb.inv.LocalInvLineItemTemplateHome"
 *           local-extends="javax.ejb.EJBLocalHome"
 *
 * @ejb:finder signature="LocalInvLineItemTemplate findByLitName(java.lang.String LIT_NM, java.lang.Integer LIT_AD_CMPNY)"
 *             query="SELECT OBJECT(lit) FROM InvLineItemTemplate lit WHERE lit.litName = ?1 AND lit.litAdCompany = ?2"
 * 
 * @ejb:finder signature="Collection findLitAll(java.lang.Integer LIT_AD_CMPNY)"
 *             query="SELECT OBJECT(lit) FROM InvLineItemTemplate lit WHERE lit.litAdCompany = ?1"
 *
 * @jboss:query signature="Collection findLitAll(java.lang.Integer LIT_AD_CMPNY)"
 *             query="SELECT OBJECT(lit) FROM InvLineItemTemplate lit WHERE lit.litAdCompany = ?1 ORDER BY lit.litName"
 *
 * @jboss:query signature="Collection ejbSelectGeneric(java.lang.String jbossQl, java.lang.Object[] args)"
 *                         dynamic="true"
 * 
 * @ejb:value-object match="*"
 *             name="InvLineItemTemplate"
 *
 * @jboss:persistence table-name="INV_LN_ITM_TMPLT"
 *
 * @jboss:entity-command name="mysql-get-generated-keys"
 *  
 */

public abstract class InvLineItemTemplateBean extends AbstractEntityBean {
	
	// PERSISTENT METHODS
	
	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 * @ejb:pk-field
	 *
	 * @jboss:column-name name="LIT_CODE"
	 *
	 * @jboss:table-name auto-increment="true"
	 **/
	public abstract Integer getLitCode();         
	public abstract void setLitCode(Integer LIT_CODE);
	
	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="LIT_NM"
	 **/
	public abstract String getLitName();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setLitName(String LIT_NM);
	
	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="LIT_DESC"
	 **/
	public abstract String getLitDescription();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setLitDescription(String Lit_DESC);
	
	
	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="LIT_AD_CMPNY"
	 **/
	public abstract Integer getLitAdCompany();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setLitAdCompany(Integer LIT_AD_CMPNY);
	
	// RELATIONSHIP METHODS    
	
	/**
	 * @ejb:interface-method view-type="local"
	 * @ejb:relation name="lineItemTemplate-customers"
	 *               role-name="lineItemTemplate-has-many-customers"
	 *
	 */
	public abstract Collection getArCustomers();
	public abstract void setArCustomers(Collection arCustomers); 
	
	/**
	 * @ejb:interface-method view-type="local"
	 * @ejb:relation name="lineItemTemplate-suppliers"
	 *               role-name="lineItemTemplate-has-many-suppliers"
	 *
	 */
	public abstract Collection getApSuppliers();
	public abstract void setApSuppliers(Collection apSuppliers); 
	
	/**
	 * @ejb:interface-method view-type="local"
	 * @ejb:relation name="lineItemTemplate-lineitems"
	 *               role-name="lineItemTemplate-has-many-lineitems"
	 */
	public abstract Collection getInvLineItems();
	public abstract void setInvLineItems(Collection invLineItems); 
	
	/**
 	 * @jboss:dynamic-ql
 	 */
 	public abstract Collection ejbSelectGeneric(java.lang.String jbossQl, java.lang.Object[] args)
 	throws FinderException;     
 	
	// BUSINESS METHODS
	
	/**
 	 * @ejb:home-method view-type="local"
 	 */
 	public Collection ejbHomeGetLitByCriteria(java.lang.String jbossQl, java.lang.Object[] args)
 	throws FinderException {
 		
 		return ejbSelectGeneric(jbossQl, args);
 		
 	} 
 	
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public void addArCustomer(LocalArCustomer arCustomer) {
		
		Debug.print("InvLineItemTemplateBean addArCustomer");
		
		try {
			
			Collection arCustomers = getArCustomers();
			arCustomers.add(arCustomer);
			
		} catch (Exception ex) {
			
			throw new EJBException(ex.getMessage());
			
		}
		
	}
	
	/**
	 * @ejb:interface-method view-type="local"
	 **/ 
	public void dropArCustomer(LocalArCustomer arCustomer) {
		
		Debug.print("InvLineItemTemplateBean dropArCustomer");
		
		try {
			
			Collection arCustomers = getArCustomers();
			arCustomers.remove(arCustomer);
			
		} catch (Exception ex) {
			
			throw new EJBException(ex.getMessage());
			
		}
		
	} 
	
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public void addApSupplier(LocalApSupplier apSupplier) {
		
		Debug.print("InvLineItemTemplateBean addApSupplier");
		
		try {
			
			Collection apSuppliers = getApSuppliers();
			apSuppliers.add(apSupplier);
			
		} catch (Exception ex) {
			
			throw new EJBException(ex.getMessage());
			
		}
		
	}
	
	/**
	 * @ejb:interface-method view-type="local"
	 **/ 
	public void dropApSupplier(LocalApSupplier apSupplier) {
		
		Debug.print("InvLineItemTemplateBean dropApSupplier");
		
		try {
			
			Collection apSuppliers = getApSuppliers();
			apSuppliers.remove(apSupplier);
			
		} catch (Exception ex) {
			
			throw new EJBException(ex.getMessage());
			
		}
		
	}
	
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public void addInvLineItem(LocalInvLineItem invLineItem) {
		
		Debug.print("InvLineItemTemplateBean addInvLineItem");
		
		try {
			
			Collection invLineItems = getInvLineItems();
			invLineItems.add(invLineItem);
			
		} catch (Exception ex) {
			
			throw new EJBException(ex.getMessage());
			
		}
		
	}
	
	/**
	 * @ejb:interface-method view-type="local"
	 **/ 
	public void dropInvLineItem(LocalInvLineItem invLineItem) {
		
		Debug.print("InvLineItemTemplateBean dropInvLineItem");
		
		try {
			
			Collection invLineItems = getInvLineItems();
			invLineItems.remove(invLineItem);
			
		} catch (Exception ex) {
			
			throw new EJBException(ex.getMessage());
			
		}
		
	}
	
	// ENTITY METHODS
	
	/**
	 * @ejb:create-method view-type="local"
	 **/
	public Integer ejbCreate(Integer LIT_CODE, String LIT_NM, 
			String LIT_DESC, Integer LIT_AD_CMPNY)
	throws CreateException {
		
		Debug.print("InvLineItemTemplateBean ejbCreate");
		
		setLitCode(LIT_CODE);
		setLitName(LIT_NM);
		setLitDescription(LIT_DESC);
		setLitAdCompany(LIT_AD_CMPNY);
		
		return null;
		
	}
	
	/**
	 * @ejb:create-method view-type="local"
	 **/
	public Integer ejbCreate(String LIT_NM, 
			String LIT_DESC, Integer LIT_AD_CMPNY)
	throws CreateException {
		
		Debug.print("InvLineItemTemplateBean ejbCreate");
		
		setLitName(LIT_NM);
		setLitDescription(LIT_DESC);
		setLitAdCompany(LIT_AD_CMPNY);
		
		return null;
		
	}
	
	
	public void ejbPostCreate(Integer LIT_CODE, String LIT_NM, 
			String LIT_DESC, Integer LIT_AD_CMPNY)
	throws CreateException { }
	
	public void ejbPostCreate(String LIT_NM, 
			String LIT_DESC, Integer LIT_AD_CMPNY)
	throws CreateException { }
	
}

