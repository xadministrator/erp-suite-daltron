package com.ejb.inv;

import java.util.Collection;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;

import com.util.AbstractEntityBean;
import com.util.Debug;

/**
 * @ejb:bean name="InvLocationEJB"
 *           display-name="Location Entity"
 *           type="CMP"
 *           cmp-version="2.x"
 *           view-type="local"
 *           local-jndi-name="omega-ejb/InvLocationEJB"
 *           schema="InvLocation"
 *           primkey-field="locCode"
 *       
 * @ejb:pk class="java.lang.Integer"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="invuser"
 *                        role-link="invuserlink"
 *
 * @ejb:permission role-name="invuser"
 *
 * @ejb:interface local-class="com.ejb.inv.LocalInvLocation"
 *                local-extends="javax.ejb.EJBLocalObject"
 *
 * @ejb:home local-class="com.ejb.inv.LocalInvLocationHome"
 *           local-extends="javax.ejb.EJBLocalHome"
 *
 * @ejb:finder signature="Collection findLocAll(java.lang.Integer LOC_AD_CMPNY)"
 *             query="SELECT OBJECT(loc) FROM InvLocation loc WHERE loc.locAdCompany=?1 ORDER BY loc.locName"
 *
 * @jboss:query signature="Collection findLocAll(java.lang.Integer LOC_AD_CMPNY)"
 *             query="SELECT OBJECT(loc) FROM InvLocation loc WHERE loc.locAdCompany=?1 ORDER BY loc.locName"
 * 
 * @ejb:finder signature="LocalInvLocation findByLocName(java.lang.String LOC_NM, java.lang.Integer LOC_AD_CMPNY)"
 *             query="SELECT OBJECT(loc) FROM InvLocation loc WHERE loc.locName=?1 AND loc.locAdCompany=?2"
 * 
 * @ejb:finder signature="Collection findByLocAdLvType(java.lang.String LOC_LV_TYP, java.lang.Integer LOC_AD_CMPNY)"
 *             query="SELECT OBJECT(loc) FROM InvLocation loc WHERE loc.locLvType=?1 AND loc.locAdCompany=?2"
 *             
 * @ejb:finder signature="Collection findLocByLocNewAndUpdated(java.lang.Integer BR_CODE, java.lang.Integer AD_CMPNY, char NEW, char UPDATED, char DOWNLOADED_UPDATED)"
 *             query="SELECT DISTINCT OBJECT(loc) FROM InvLocation loc, IN(loc.invItemLocations) il, IN(il.adBranchItemLocations) bil WHERE (bil.bilLocationDownloadStatus = ?3 OR bil.bilLocationDownloadStatus = ?4 OR bil.bilLocationDownloadStatus = ?5) AND bil.adBranch.brCode = ?1 AND bil.bilAdCompany = ?2"
 * 
 * @jboss:query signature="Collection ejbSelectGeneric(java.lang.String jbossQl, java.lang.Object[] args)"
 *                         dynamic="true"
 *
 * @ejb:value-object match="*"
 *             name="InvLocation"
 *
 * @jboss:persistence table-name="INV_LCTN"
 *
 * @jboss:entity-command name="mysql-get-generated-keys"
 *
 */

public abstract class InvLocationBean extends AbstractEntityBean {
	
//	 Access methods for persistent fields

	   /**
	    * @ejb:persistent-field
	    * @ejb:interface-method view-type="local"
	    * @ejb:pk-field
	    *
	    * @jboss:column-name name="LOC_CODE"
	    *
	    * @jboss:persistence auto-increment="true"
	    **/
	   public abstract java.lang.Integer getLocCode();
	   public abstract void setLocCode(java.lang.Integer LOC_CODE);
	   /**
	    * @ejb:persistent-field
	    * @ejb:interface-method view-type="local"
	    *
	    * @jboss:column-name name="LOC_NM"
	    **/
	   
	   public abstract java.lang.String getLocName();
	   /**
	    * @ejb:interface-method view-type="local"
	    **/
	   public abstract void setLocName(java.lang.String LOC_NM);

	   /**
	    * @ejb:persistent-field
	    * @ejb:interface-method view-type="local"
	    *
	    * @jboss:column-name name="LOC_DESC"
	    **/
	   public abstract java.lang.String getLocDescription();
	   /**
	    * @ejb:interface-method view-type="local"
	    **/
	   public abstract void setLocDescription(java.lang.String LOC_DESC);
	   
	   /**
	    * @ejb:persistent-field
	    * @ejb:interface-method view-type="local"
	    *
	    * @jboss:column-name name="LOC_LV_TYP"
	    **/
	   public abstract java.lang.String getLocLvType();
	   /**
	    * @ejb:interface-method view-type="local"
	    **/
	   public abstract void setLocLvType(java.lang.String LOC_LV_TYP);
	   
	   /**
	    * @ejb:persistent-field
	    * @ejb:interface-method view-type="local"
	    *
	    * @jboss:column-name name="LOC_DPRTMNT"
	    **/
	   public abstract java.lang.String getLocDepartment();
	   /**
	    * @ejb:interface-method view-type="local"
	    **/
	   public abstract void setLocDepartment(java.lang.String LOC_DPRTMNT);
	   
	   /**
	    * @ejb:persistent-field
	    * @ejb:interface-method view-type="local"
	    *
	    * @jboss:column-name name="LOC_PSTN"
	    **/
	   public abstract java.lang.String getLocPosition();
	   /**
	    * @ejb:interface-method view-type="local"
	    **/
	   public abstract void setLocPosition(java.lang.String LOC_PSTN);
	   
	   /**
	    * @ejb:persistent-field
	    * @ejb:interface-method view-type="local"
	    *
	    * @jboss:column-name name="LOC_DT_HRD"
	    **/
	   public abstract java.lang.String getLocDateHired();
	   /**
	    * @ejb:interface-method view-type="local"
	    **/
	   public abstract void setLocDateHired(java.lang.String LOC_DT_HRD);
	   
	   /**
	    * @ejb:persistent-field
	    * @ejb:interface-method view-type="local"
	    *
	    * @jboss:column-name name="LOC_EMPLYMNT_STTS"
	    **/
	   public abstract java.lang.String getLocEmploymentStatus();
	   /**
	    * @ejb:interface-method view-type="local"
	    **/
	   public abstract void setLocEmploymentStatus(java.lang.String LOC_EMPLYMNT_STTS);
	   /**
	    * @ejb:persistent-field
	    * @ejb:interface-method view-type="local"
	    *
	    * @jboss:column-name name="LOC_BRNCH"
	    **/
	   public abstract java.lang.String getLocBranch();
	   /**
	    * @ejb:interface-method view-type="local"
	    **/
	   public abstract void setLocBranch(java.lang.String LOC_BRNCH);
	   
	   /**
	    * @ejb:persistent-field
	    * @ejb:interface-method view-type="local"
	    *
	    * @jboss:column-name name="LOC_ADDRSS"
	    **/
	   public abstract java.lang.String getLocAddress();
	   /**
	    * @ejb:interface-method view-type="local"
	    **/
	   public abstract void setLocAddress(java.lang.String LOC_ADDRSS);
	   
	   /**
	    * @ejb:persistent-field
	    * @ejb:interface-method view-type="local"
	    *
	    * @jboss:column-name name="LOC_CNTCT_PRSN"
	    **/
	   public abstract java.lang.String getLocContactPerson();
	   /**
	    * @ejb:interface-method view-type="local"
	    **/
	   public abstract void setLocContactPerson(java.lang.String LOC_CNTCT_PRSN);
	   
	   /**
	    * @ejb:persistent-field
	    * @ejb:interface-method view-type="local"
	    *
	    * @jboss:column-name name="LOC_CNTCT_NMBR"
	    **/
	   public abstract java.lang.String getLocContactNumber();
	   /**
	    * @ejb:interface-method view-type="local"
	    **/
	   public abstract void setLocContactNumber(java.lang.String LOC_CNTCT_NMBR);
	   
	   /**
	    * @ejb:persistent-field
	    * @ejb:interface-method view-type="local"
	    *
	    * @jboss:column-name name="LOC_EML_ADDRSS"
	    **/
	   public abstract java.lang.String getLocEmailAddress();
	   /**
	    * @ejb:interface-method view-type="local"
	    **/
	   public abstract void setLocEmailAddress(java.lang.String LOC_EML_ADDRSS);
	   
	   /**
	    * @ejb:persistent-field
	    * @ejb:interface-method view-type="local"
	    *
	    * @jboss:column-name name="LOC_AD_CMPNY"
	    **/
	   public abstract java.lang.Integer getLocAdCompany();
	   /**
	    * @ejb:interface-method view-type="local"
	    **/
	   public abstract void setLocAdCompany(java.lang.Integer LOC_AD_CMPNY);

	   // Access methods for relationship fields
	   
	   /**
	    * @ejb:interface-method view-type="local"
	    * @ejb:relation name="location-itemlocations"
	    *               role-name="location-has-many-itemlocations"
	    */
	   public abstract Collection getInvItemLocations();
	   public abstract void setInvItemLocations(Collection invItemLocations);
	   
	   /**
	    * @ejb:interface-method view-type="local"
	    * @ejb:relation name="location-physicalinventories"
	    *               role-name="location-has-many-physicalinventories"
	    */
	   public abstract Collection getInvPhysicalInventories();
	   public abstract void setInvPhysicalInventories(Collection invPhysicalInventories);	   
	   
	   /**
	    * @ejb:interface-method view-type="local"
	    * @ejb:relation name="location-branchstocktransfers"
	    *               role-name="location-has-many-branchstocktransfers"
	    */
	   public abstract Collection getInvBranchStockTransfers();
	   public abstract void setInvBranchStockTransfers(Collection invBranchStockTransfers);
	   
		/**
		 * @jboss:dynamic-ql
		 */
		public abstract Collection ejbSelectGeneric(java.lang.String jbossQl, java.lang.Object[] args)
		throws FinderException;	   

	   // Business methods
		
		/**
		 * @ejb:home-method view-type="local"
		 */
		public Collection ejbHomeGetLocByCriteria(java.lang.String jbossQl, java.lang.Object[] args)
		throws FinderException {
			
			return ejbSelectGeneric(jbossQl, args);
			
		}   		
	   
	   /**
	    * @ejb:interface-method view-type="local"
	    **/
	   public void addInvItemLocation(LocalInvItemLocation invItemLocation) {

	      Debug.print("InvItemLocationBean addInvItemLocation");
	      try {
	         Collection invItemLocations = getInvItemLocations();
	         invItemLocations.add(invItemLocation);
	      } catch (Exception ex) {
	         throw new EJBException(ex.getMessage());
	      }
	   }

	   /**
	    * @ejb:interface-method view-type="local"
	    **/
	   public void dropInvItemLocation(LocalInvItemLocation invItemLocation) {

	      Debug.print("InvLocationBean dropInvItemLocation");
	      try {
	         Collection invItemLocations = getInvItemLocations();
	         invItemLocations.remove(invItemLocation);
	      } catch (Exception ex) {
	         throw new EJBException(ex.getMessage());
	      }
	   }
	   
	   /**
	    * @ejb:interface-method view-type="local"
	    **/
	   public void addInvBranchStockTransfer(LocalInvBranchStockTransfer invBranchStockTransfer) {

	      Debug.print("InvBranchStockTransfersBean addInvBranchStockTransfer");
	      try {
	         Collection invBranchStockTransfers = getInvBranchStockTransfers();
	         invBranchStockTransfers.add(invBranchStockTransfer);
	      } catch (Exception ex) {
	         throw new EJBException(ex.getMessage());
	      }
	   }

	   /**
	    * @ejb:interface-method view-type="local"
	    **/
	   public void dropInvBranchStockTransfer(LocalInvBranchStockTransfer invBranchStockTransfer) {

	      Debug.print("InvBranchStockTransferBean dropInvBranchStockTransfer");
	      try {
	         Collection invBranchStockTransfers = getInvBranchStockTransfers();
	         invBranchStockTransfers.remove(invBranchStockTransfer);
	      } catch (Exception ex) {
	         throw new EJBException(ex.getMessage());
	      }
	   }
	   
	   /**
	    * @ejb:interface-method view-type="local"
	    **/
	   public void addInvPhysicalInventory(LocalInvPhysicalInventory invPhysicalInventory) {

	      Debug.print("InvLocationBean addInvPhysicalInventory");
	      try {
	         Collection invPhysicalInventories = getInvPhysicalInventories();
	         invPhysicalInventories.add(invPhysicalInventory);
	      } catch (Exception ex) {
	         throw new EJBException(ex.getMessage());
	      }
	   }

	   /**
	    * @ejb:interface-method view-type="local"
	    **/
	   public void dropInvPhysicalInventory(LocalInvPhysicalInventory invPhysicalInventory) {

	      Debug.print("InvLocationBean dropInvPhysicalInventory");
	      try {
	         Collection invPhysicalInventories = getInvPhysicalInventories();
	         invPhysicalInventories.remove(invPhysicalInventory);
	      } catch (Exception ex) {
	         throw new EJBException(ex.getMessage());
	      }
	   }	   
	   
	   // EntityBean methods

	   /**
	    * @ejb:create-method view-type="local"
	    **/
	   public java.lang.Integer ejbCreate (java.lang.Integer LOC_CODE, java.lang.String LOC_NM,
	   	  java.lang.String LOC_DESC, java.lang.String LOC_LV_TYP, java.lang.String LOC_ADDRSS,
		  java.lang.String LOC_CNTCT_PRSN, java.lang.String LOC_CNTCT_NMBR,
		  java.lang.String LOC_EML_ADDRSS, java.lang.String LOC_BRNCH, java.lang.String LOC_DPRTMNT, 
		  java.lang.String LOC_PSTN, java.lang.String LOC_DT_HRD, java.lang.String LOC_EMPLYMNT_STTS,
		  java.lang.Integer LOC_AD_CMPNY)
		  throws CreateException {

	      Debug.print("InvLocationBean ejbCreate");
	      setLocCode(LOC_CODE);
	      setLocName(LOC_NM);
	      setLocDescription(LOC_DESC);
	      setLocLvType(LOC_LV_TYP);
	      setLocAddress(LOC_ADDRSS);
	      setLocContactPerson(LOC_CNTCT_PRSN);
	      setLocContactNumber(LOC_CNTCT_NMBR);
	      setLocEmailAddress(LOC_EML_ADDRSS);
	      setLocBranch(LOC_BRNCH);
	      setLocDepartment(LOC_DPRTMNT);
	      setLocPosition(LOC_PSTN);
	      setLocDateHired(LOC_DT_HRD);
	      setLocEmploymentStatus(LOC_EMPLYMNT_STTS);
	      setLocAdCompany(LOC_AD_CMPNY);
	      
	      return null;
	   }

	   /**
	    * @ejb:create-method view-type="local"
	    **/
	   public java.lang.Integer ejbCreate (java.lang.Integer LOC_CODE, java.lang.String LOC_NM,
	   	  java.lang.String LOC_DESC, java.lang.String LOC_LV_TYP, java.lang.String LOC_ADDRSS,
		  java.lang.String LOC_CNTCT_PRSN, java.lang.String LOC_CNTCT_NMBR,
		  java.lang.String LOC_EML_ADDRSS,
		  java.lang.Integer LOC_AD_CMPNY)
		  throws CreateException {

	      Debug.print("InvLocationBean ejbCreate");
	      setLocCode(LOC_CODE);
	      setLocName(LOC_NM);
	      setLocDescription(LOC_DESC);
	      setLocLvType(LOC_LV_TYP);
	      setLocAddress(LOC_ADDRSS);
	      setLocContactPerson(LOC_CNTCT_PRSN);
	      setLocContactNumber(LOC_CNTCT_NMBR);
	      setLocEmailAddress(LOC_EML_ADDRSS);
	      setLocAdCompany(LOC_AD_CMPNY);
	      
	      return null;
	   }
	   
	   /**
	    * @ejb:create-method view-type="local"
	    **/
	   public java.lang.Integer ejbCreate (java.lang.String LOC_NM,
	   	  java.lang.String LOC_DESC, java.lang.String LOC_LV_TYP, java.lang.String LOC_ADDRSS,
		  java.lang.String LOC_CNTCT_PRSN, java.lang.String LOC_CNTCT_NMBR,
		  java.lang.String LOC_EML_ADDRSS,
		  java.lang.Integer LOC_AD_CMPNY)
	      throws CreateException {

	      Debug.print("InvLocationBean ejbCreate");
	      setLocName(LOC_NM);
	      setLocDescription(LOC_DESC);
	      setLocLvType(LOC_LV_TYP);
	      setLocAddress(LOC_ADDRSS);
	      setLocContactPerson(LOC_CNTCT_PRSN);
	      setLocContactNumber(LOC_CNTCT_NMBR);
	      setLocEmailAddress(LOC_EML_ADDRSS);
	      setLocAdCompany(LOC_AD_CMPNY);
	      return null;
	   }
	   
	   /**
	    * @ejb:create-method view-type="local"
	    **/
	   public java.lang.Integer ejbCreate (java.lang.String LOC_NM,
	   	  java.lang.String LOC_DESC, java.lang.String LOC_LV_TYP, java.lang.String LOC_ADDRSS,
		  java.lang.String LOC_CNTCT_PRSN, java.lang.String LOC_CNTCT_NMBR,
		  java.lang.String LOC_EML_ADDRSS, java.lang.String LOC_BRNCH, java.lang.String LOC_DPRTMNT, 
		  java.lang.String LOC_PSTN, java.lang.String LOC_DT_HRD, java.lang.String LOC_EMPLYMNT_STTS, 
		  java.lang.Integer LOC_AD_CMPNY)
	      throws CreateException {

	      Debug.print("InvLocationBean ejbCreate");
	      setLocName(LOC_NM);
	      setLocDescription(LOC_DESC);
	      setLocLvType(LOC_LV_TYP);
	      setLocAddress(LOC_ADDRSS);
	      setLocContactPerson(LOC_CNTCT_PRSN);
	      setLocContactNumber(LOC_CNTCT_NMBR);
	      setLocEmailAddress(LOC_EML_ADDRSS);
	      setLocBranch(LOC_BRNCH);
	      setLocDepartment(LOC_DPRTMNT);
	      setLocPosition(LOC_PSTN);
	      setLocDateHired(LOC_DT_HRD);
	      setLocEmploymentStatus(LOC_EMPLYMNT_STTS);
	      setLocAdCompany(LOC_AD_CMPNY);
	      return null;
	   }

	   public void ejbPostCreate (java.lang.Integer LOC_CODE, java.lang.String LOC_NM,
		   	  java.lang.String LOC_DESC, java.lang.String LOC_LV_TYP, java.lang.String LOC_ADDRSS,
			  java.lang.String LOC_CNTCT_PRSN, java.lang.String LOC_CNTCT_NMBR,
			  java.lang.String LOC_EML_ADDRSS,
			  java.lang.Integer LOC_AD_CMPNY)
	      throws CreateException { }

	   public void ejbPostCreate (java.lang.String LOC_NM,
		   	  java.lang.String LOC_DESC, java.lang.String LOC_LV_TYP, java.lang.String LOC_ADDRSS,
			  java.lang.String LOC_CNTCT_PRSN, java.lang.String LOC_CNTCT_NMBR,
			  java.lang.String LOC_EML_ADDRSS,
			  java.lang.Integer LOC_AD_CMPNY)
	      throws CreateException { }
	   
	   public void ejbPostCreate (java.lang.Integer LOC_CODE, java.lang.String LOC_NM,
			   java.lang.String LOC_DESC, java.lang.String LOC_LV_TYP, java.lang.String LOC_ADDRSS,
			   java.lang.String LOC_CNTCT_PRSN, java.lang.String LOC_CNTCT_NMBR,
			   java.lang.String LOC_EML_ADDRSS, java.lang.String LOC_BRNCH, java.lang.String LOC_DPRTMNT,
			   java.lang.String LOC_PSTN, java.lang.String LOC_DT_HRD, java.lang.String LOC_EMPLYMNT_STTS,
			   java.lang.Integer LOC_AD_CMPNY)
	   throws CreateException { }

	   public void ejbPostCreate (java.lang.String LOC_NM,
			   java.lang.String LOC_DESC, java.lang.String LOC_LV_TYP, java.lang.String LOC_ADDRSS,
			   java.lang.String LOC_CNTCT_PRSN, java.lang.String LOC_CNTCT_NMBR,
			   java.lang.String LOC_EML_ADDRSS, java.lang.String LOC_BRNCH, java.lang.String LOC_DPRTMNT,
			   java.lang.String LOC_PSTN, java.lang.String LOC_DT_HRD, java.lang.String LOC_EMPLYMNT_STTS,
			   java.lang.Integer LOC_AD_CMPNY)
	   throws CreateException { }
	
}
