package com.ejb.inv;

import java.util.Collection;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;

import com.util.AbstractEntityBean;
import com.util.Debug;

/**
 * @ejb:bean name="InvOverheadEJB"
 *           display-name="Overhead Entity"
 *           type="CMP"
 *           cmp-version="2.x"
 *           view-type="local"
 *           local-jndi-name="omega-ejb/InvOverheadEJB"
 *           schema="InvOverhead"
 *           primkey-field="ohCode"
 *       
 * @ejb:pk class="java.lang.Integer"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="invuser"
 *                        role-link="invuserlink"
 *
 * @ejb:permission role-name="invuser"
 *
 * @ejb:interface local-class="com.ejb.inv.LocalInvOverhead"
 *                local-extends="javax.ejb.EJBLocalObject"
 *
 * @ejb:home local-class="com.ejb.inv.LocalInvOverheadHome"
 *           local-extends="javax.ejb.EJBLocalHome"
 * 
 * @ejb:finder signature="Collection findUnpostedOhByOhDateRange(java.util.Date OH_DT_FRM, java.util.Date OH_DT_TO, java.lang.Integer OH_AD_CMPNY)"
 *             query="SELECT OBJECT(oh) FROM InvOverhead oh  WHERE oh.ohPosted = 0 AND oh.ohDate >= ?1 AND oh.ohDate <= ?2 AND oh.ohAdCompany = ?3"
 *
 * @jboss:query signature="Collection findUnpostedOhByOhDateRange(java.util.Date OH_DT_FRM, java.util.Date OH_DT_TO, java.lang.Integer OH_AD_CMPNY)"
 *             query="SELECT OBJECT(oh) FROM InvOverhead oh  WHERE oh.ohPosted = 0 AND oh.ohDate >= ?1 AND oh.ohDate <= ?2 AND oh.ohAdCompany = ?3 ORDER BY oh.ohDate"
 * 
 * @ejb:finder signature="LocalInvOverhead findByOhDocumentNumberAndBrCode(java.lang.String OH_DCMNT_NMBR, java.lang.Integer OH_AD_BRNCH, java.lang.Integer OH_AD_CMPNY)"
 *             query="SELECT OBJECT(oh) FROM InvOverhead oh  WHERE oh.ohDocumentNumber = ?1 AND oh.ohAdBranch = ?2 AND oh.ohAdCompany = ?3"
 *
 * @ejb:select signature="Collection ejbSelectGeneric(java.lang.String jbossQl, java.lang.Object[] args)"
 *             result-type-mapping="Local"
 *             method-intf="LocalHome"
 *             query="SELECT OBJECT(oh) FROM InvOverhead oh"
 *
 * @jboss:query signature="Collection ejbSelectGeneric(java.lang.String jbossQl, java.lang.Object[] args)"
 *                         dynamic="true"
 * 
 * @ejb:value-object match="*"
 *             name="InvOverhead"
 *
 * @jboss:persistence table-name="INV_OVRHD"
 *
 * @jboss:entity-command name="mysql-get-generated-keys"
 *
 */

public abstract class InvOverheadBean extends AbstractEntityBean {
	
//	 Access methods for persistent fields

	   /**
	    * @ejb:persistent-field
	    * @ejb:interface-method view-type="local"
	    * @ejb:pk-field
	    *
	    * @jboss:column-name name="OH_CODE"
	    *
	    * @jboss:persistence auto-increment="true"
	    **/
	   public abstract java.lang.Integer getOhCode();
	   public abstract void setOhCode(java.lang.Integer OH_CODE);
	   
       /**
	    * @ejb:persistent-field
	    * @ejb:interface-method view-type="local"
	    *
	    * @jboss:column-name name="OH_DT"
	    **/
	   public abstract java.util.Date getOhDate();
	   /**
	    * @ejb:interface-method view-type="local"
	    **/
	   public abstract void setOhDate(java.util.Date OH_DT);
	   
	   /**
	    * @ejb:persistent-field
	    * @ejb:interface-method view-type="local"
	    *
	    * @jboss:column-name name="OH_DCMNT_NMBR"
	    **/
	   public abstract java.lang.String getOhDocumentNumber();
	   /**
	    * @ejb:interface-method view-type="local"
	    **/
	   public abstract void setOhDocumentNumber(java.lang.String OH_DCMNT_NMBR);
	   

	   /**
	    * @ejb:persistent-field
	    * @ejb:interface-method view-type="local"
	    *
	    * @jboss:column-name name="OH_RFRNC_NMBR"
	    **/
	   public abstract java.lang.String getOhReferenceNumber();
	   /**
	    * @ejb:interface-method view-type="local"
	    **/
	   public abstract void setOhReferenceNumber(java.lang.String OH_RFRNC_NMBR);
	   
	   /**
	    * @ejb:persistent-field
	    * @ejb:interface-method view-type="local"
	    *
	    * @jboss:column-name name="OH_DESC"
	    **/
	   public abstract java.lang.String getOhDescription();
	   /**
	    * @ejb:interface-method view-type="local"
	    **/
	   public abstract void setOhDescription(java.lang.String OH_DESC);	   
	   	   
	   /**
	    * @ejb:persistent-field
	    * @ejb:interface-method view-type="local"
	    *
	    * @jboss:column-name name="OH_LV_SHFT"
	    **/
	   public abstract String getOhLvShift();
	   /**
	    * @ejb:interface-method view-type="local"
	    **/
	   public abstract void setOhLvShift(String OH_LV_SHFT);
	   
	   /**
	    * @ejb:persistent-field
	    * @ejb:interface-method view-type="local"
	    *
	    * @jboss:column-name name="OH_PSTD"
	    **/
	   public abstract byte getOhPosted();
	   /**
	    * @ejb:interface-method view-type="local"
	    **/
	   public abstract void setOhPosted(byte OH_PSTD);
	   
	   /**
	    * @ejb:persistent-field
	    * @ejb:interface-method view-type="local"
	    *
	    * @jboss:column-name name="OH_CRTD_BY"
	    **/
	   public abstract String getOhCreatedBy();
	   /**
	    * @ejb:interface-method view-type="local"
	    **/
	   public abstract void setOhCreatedBy(String OH_CRTD_BY);
	   
	   /**
	    * @ejb:persistent-field
	    * @ejb:interface-method view-type="local"
	    *
	    * @jboss:column-name name="OH_DT_CRTD"
	    **/
	   public abstract java.util.Date getOhDateCreated();
	   /**
	    * @ejb:interface-method view-type="local"
	    **/
	   public abstract void setOhDateCreated(java.util.Date OH_DT_CRTD);
	   
	   /**
	    * @ejb:persistent-field
	    * @ejb:interface-method view-type="local"
	    *
	    * @jboss:column-name name="OH_LST_MDFD_BY"
	    **/
	   public abstract String getOhLastModifiedBy();
	   /**
	    * @ejb:interface-method view-type="local"
	    **/
	   public abstract void setOhLastModifiedBy(String OH_LST_MDFD_BY);
	   
	   /**
	    * @ejb:persistent-field
	    * @ejb:interface-method view-type="local"
	    *
	    * @jboss:column-name name="OH_DT_LST_MDFD"
	    **/
	   public abstract java.util.Date getOhDateLastModified();
	   /**
	    * @ejb:interface-method view-type="local"
	    **/
	   public abstract void setOhDateLastModified(java.util.Date OH_DT_LST_MDFD);
	   
	   /**
	    * @ejb:persistent-field
	    * @ejb:interface-method view-type="local"
	    *
	    * @jboss:column-name name="OH_PSTD_BY"
	    **/
	   public abstract String getOhPostedBy();
	   /**
	    * @ejb:interface-method view-type="local"
	    **/
	   public abstract void setOhPostedBy(String OH_PSTD_BY);
	   
	   /**
	    * @ejb:persistent-field
	    * @ejb:interface-method view-type="local"
	    *
	    * @jboss:column-name name="OH_DT_PSTD"
	    **/
	   public abstract java.util.Date getOhDatePosted();
	   /**
	    * @ejb:interface-method view-type="local"
	    **/
	   public abstract void setOhDatePosted(java.util.Date OH_DT_PSTD);
	   
	   /**
	    * @ejb:persistent-field
	    * @ejb:interface-method view-type="local"
	    *
	    * @jboss:column-name name="OH_AD_BRNCH"
	    **/
	   public abstract Integer getOhAdBranch();
	   /**
	    * @ejb:interface-method view-type="local"
	    **/
	   public abstract void setOhAdBranch(Integer OH_AD_BRNCH);
	   
	   /**
	    * @ejb:persistent-field
	    * @ejb:interface-method view-type="local"
	    *
	    * @jboss:column-name name="OH_AD_CMPNY"
	    **/
	   public abstract Integer getOhAdCompany();
	   /**
	    * @ejb:interface-method view-type="local"
	    **/
	   public abstract void setOhAdCompany(Integer OH_AD_CMPNY);
	   
	   // Access methods for relationship fields
	   
	   /**
	    * @ejb:interface-method view-type="local"
	    * @ejb:relation name="overhead-overheadlines"
	    *               role-name="overhead-has-many-overheadlines"
	    */
	   public abstract Collection getInvOverheadLines();
	   public abstract void setInvOverheadLines(Collection invOverheadLines);
	   
	   /**
	    * @ejb:interface-method view-type="local"
	    * @ejb:relation name="overhead-invdistributionrecords"
	    *               role-name="overhead-has-many-invdistributionrecords"
	    */
	   public abstract Collection getInvDistributionRecords();
	   public abstract void setInvDistributionRecords(Collection invDistributionRecords);
	   
		/**
		 * @jboss:dynamic-ql
		 */
		public abstract Collection ejbSelectGeneric(java.lang.String jbossQl, java.lang.Object[] args)
		throws FinderException;
		
		// BUSINESS METHODS
		
		/**
		 * @ejb:home-method view-type="local"
		 */
		public Collection ejbHomeGetOhByCriteria(java.lang.String jbossQl, java.lang.Object[] args)
		throws FinderException {
			
			return ejbSelectGeneric(jbossQl, args);
			
		}	   
	   		
	   /**
	    * @ejb:interface-method view-type="local"
	    **/
	   public void addInvOverheadLine(LocalInvOverheadLine invOverheadLine) {

	      Debug.print("InvOverheadBean addInvOverheadLine");
	      try {
	         Collection invOverheadLines = getInvOverheadLines();
	         invOverheadLines.add(invOverheadLine);
	      } catch (Exception ex) {
	         throw new EJBException(ex.getMessage());
	      }
	   }

	   /**
	    * @ejb:interface-method view-type="local"
	    **/
	   public void dropInvOverheadLine(LocalInvOverheadLine invOverheadLine) {
	   
	      Debug.print("InvOverheadBean dropInvOverheadLine");
	      try {
	         Collection invOverheadLines = getInvOverheadLines();
	         invOverheadLines.remove(invOverheadLine);
	      } catch (Exception ex) {
	         throw new EJBException(ex.getMessage());
	      }
	   }

	   /**
	    * @ejb:interface-method view-type="local"
	    **/
	   public void addInvDistributionRecord(LocalInvDistributionRecord invDistributionRecord) {

	      Debug.print("InvOverheadBean addInvDistributionRecord");
	      try {
	         Collection invDistributionRecords = getInvDistributionRecords();
	         invDistributionRecords.add(invDistributionRecord);
	      } catch (Exception ex) {
	         throw new EJBException(ex.getMessage());
	      }
	   }

	   /**
	    * @ejb:interface-method view-type="local"
	    **/
	   public void dropInvDistributionRecord(LocalInvDistributionRecord invDistributionRecord) {

	      Debug.print("InvOverheadBean dropInvDistributionRecord");
	      try {
	         Collection invDistributionRecords = getInvDistributionRecords();
	         invDistributionRecords.remove(invDistributionRecord);
	      } catch (Exception ex) {
	         throw new EJBException(ex.getMessage());
	      }
	   }
	   
	   /**
	    * @ejb:interface-method view-type="local"
	    **/
	   public short getInvDrNextLine() {

	       Debug.print("InvOverheadBean getInvDrNextLine");
	       try {
	          Collection invDistributionRecords = getInvDistributionRecords();
		      return (short)(invDistributionRecords.size() + 1);		    
	       } catch (Exception ex) {	      	
	          throw new EJBException(ex.getMessage());	         
	       }
	    }  

	   // EntityBean methods

	   /**
	    * @ejb:create-method view-type="local"
	    **/
	   	   
	   public java.lang.Integer ejbCreate (java.lang.Integer OH_CODE, java.util.Date OH_DT, 
	   	  java.lang.String OH_DCMNT_NMBR, java.lang.String OH_RFRNC_NMBR, String OH_DESC, java.lang.String OH_LV_SHFT,
	      byte OH_PSTD, java.lang.String OH_CRTD_BY, java.util.Date OH_DT_CRTD, 
		  java.lang.String OH_LST_MDFD_BY, java.util.Date OH_DT_LST_MDFD,  
		  java.lang.String OH_PSTD_BY, java.util.Date OH_DT_PSTD, Integer OH_AD_BRNCH, java.lang.Integer OH_AD_CMPNY)
	      throws CreateException {

	      Debug.print("invOverheadBean ejbCreate");
	      
	      setOhCode(OH_CODE);
	      setOhDate(OH_DT);
	      setOhDocumentNumber(OH_DCMNT_NMBR);
	      setOhReferenceNumber(OH_RFRNC_NMBR);
	      setOhDescription(OH_DESC);
	      setOhLvShift(OH_LV_SHFT);
	      setOhPosted(OH_PSTD);
	      setOhCreatedBy(OH_CRTD_BY);
	      setOhDateCreated(OH_DT_CRTD);
	      setOhLastModifiedBy(OH_LST_MDFD_BY);
	      setOhDateLastModified(OH_DT_LST_MDFD);
	      setOhPostedBy(OH_PSTD_BY);
	      setOhDatePosted(OH_DT_PSTD);
	      setOhAdBranch(OH_AD_BRNCH);
	      setOhAdCompany(OH_AD_CMPNY);

	      return null;
	   }

	   /**
	    * @ejb:create-method view-type="local"
	    **/
	   public java.lang.Integer ejbCreate (java.util.Date OH_DT, 
	   	  java.lang.String OH_DCMNT_NMBR, java.lang.String OH_RFRNC_NMBR, String OH_DESC, java.lang.String OH_LV_SHFT,
	      byte OH_PSTD, java.lang.String OH_CRTD_BY, java.util.Date OH_DT_CRTD, 
		  java.lang.String OH_LST_MDFD_BY, java.util.Date OH_DT_LST_MDFD,  
		  java.lang.String OH_PSTD_BY, java.util.Date OH_DT_PSTD, Integer OH_AD_BRNCH, java.lang.Integer OH_AD_CMPNY)
	      throws CreateException {

	      Debug.print("invOverheadBean ejbCreate");
	      
	      setOhDate(OH_DT);
	      setOhDocumentNumber(OH_DCMNT_NMBR);
	      setOhReferenceNumber(OH_RFRNC_NMBR);
	      setOhDescription(OH_DESC);
	      setOhLvShift(OH_LV_SHFT);
	      setOhPosted(OH_PSTD);
	      setOhCreatedBy(OH_CRTD_BY);
	      setOhDateCreated(OH_DT_CRTD);
	      setOhLastModifiedBy(OH_LST_MDFD_BY);
	      setOhDateLastModified(OH_DT_LST_MDFD);
	      setOhPostedBy(OH_PSTD_BY);
	      setOhDatePosted(OH_DT_PSTD);
	      setOhAdBranch(OH_AD_BRNCH);
	      setOhAdCompany(OH_AD_CMPNY);
	      
	      return null;
	      
	   }

	   public void ejbPostCreate (java.lang.Integer OH_CODE, java.util.Date OH_DT, 
	   	  java.lang.String OH_DCMNT_NMBR,  java.lang.String OH_RFRNC_NMBR, String OH_DESC, java.lang.String OH_LV_SHFT,
	      byte OH_PSTD, java.lang.String OH_CRTD_BY, java.util.Date OH_DT_CRTD, 
		  java.lang.String OH_LST_MDFD_BY, java.util.Date OH_DT_LST_MDFD,  
		  java.lang.String OH_PSTD_BY, java.util.Date OH_DT_PSTD, Integer OH_AD_BRNCH, java.lang.Integer OH_AD_CMPNY)	      
	      throws CreateException { }

	   public void ejbPostCreate (java.util.Date OH_DT, 
	   	  java.lang.String OH_DCMNT_NMBR, java.lang.String OH_RFRNC_NMBR, String OH_DESC, java.lang.String OH_LV_SHFT,
	      byte OH_PSTD, java.lang.String OH_CRTD_BY, java.util.Date OH_DT_CRTD, 
		  java.lang.String OH_LST_MDFD_BY, java.util.Date OH_DT_LST_MDFD,  
		  java.lang.String OH_PSTD_BY, java.util.Date OH_DT_PSTD, Integer OH_AD_BRNCH, java.lang.Integer OH_AD_CMPNY)	      
	      throws CreateException { }
	
}
