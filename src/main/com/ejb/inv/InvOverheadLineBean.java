package com.ejb.inv;

import java.util.Collection;

import javax.ejb.CreateException;
import javax.ejb.FinderException;

import com.util.AbstractEntityBean;
import com.util.Debug;

/**
 * @ejb:bean name="InvOverheadLineEJB"
 *           display-name="Overhead Line Entity"
 *           type="CMP"
 *           cmp-version="2.x"
 *           view-type="local"
 *           local-jndi-name="omega-ejb/InvOverheadLineEJB"
 *           schema="InvOverheadLine"
 *           primkey-field="ohlCode"
 *       
 * @ejb:pk class="java.lang.Integer"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="invuser"
 *                        role-link="invuserlink"
 *
 * @ejb:permission role-name="invuser"
 *
 * @ejb:interface local-class="com.ejb.inv.LocalInvOverheadLine"
 *                local-extends="javax.ejb.EJBLocalObject"
 *
 * @ejb:home local-class="com.ejb.inv.LocalInvOverheadLineHome"
 *           local-extends="javax.ejb.EJBLocalHome"
 * 
 * @ejb:select signature="Collection ejbSelectGeneric(java.lang.String jbossQl, java.lang.Object[] args)"
 *             result-type-mapping="Local"
 *             method-intf="LocalHome"
 *             query="SELECT OBJECT(ohl) FROM InvOverheadLine ohl"
 *
 * @jboss:query signature="Collection ejbSelectGeneric(java.lang.String jbossQl, java.lang.Object[] args)"
 *                         dynamic="true"
 *
 * @ejb:value-object match="*"
 *             name="InvOverheadLine"
 *
 * @jboss:persistence table-name="INV_OVRHD_LN"
 *
 * @jboss:entity-command name="mysql-get-generated-keys"
 *
 */

public abstract class InvOverheadLineBean extends AbstractEntityBean {
	
//	 Access methods for persistent fields

	   /**
	    * @ejb:persistent-field
	    * @ejb:interface-method view-type="local"
	    * @ejb:pk-field
	    *
	    * @jboss:column-name name="OHL_CODE"
	    *
	    * @jboss:persistence auto-increment="true"
	    **/
	   public abstract java.lang.Integer getOhlCode();
	   public abstract void setOhlCode(java.lang.Integer OHL_CODE);

	   /**
	    * @ejb:persistent-field
	    * @ejb:interface-method view-type="local"
	    *
	    * @jboss:column-name name="OHL_UNT_CST"
	    **/
	   public abstract double getOhlUnitCost();
	   /**
	    * @ejb:interface-method view-type="local"
	    **/
	   public abstract void setOhlUnitCost(double OHL_UNT_CST);

	   /**
	    * @ejb:persistent-field
	    * @ejb:interface-method view-type="local"
	    *
	    * @jboss:column-name name="OHL_QTY"
	    **/
	   public abstract double getOhlQuantity();
	   /**
	    * @ejb:interface-method view-type="local"
	    **/
	   public abstract void setOhlQuantity(double OHL_QTY);
	   
	   
	   /**
	    * @ejb:persistent-field
	    * @ejb:interface-method view-type="local"
	    *
	    * @jboss:column-name name="OHL_AD_CMPNY"
	    **/
	   public abstract Integer getOhlAdCompany();
	   /**
	    * @ejb:interface-method view-type="local"
	    **/
	   public abstract void setOhlAdCompany(Integer OHL_AD_CMPNY);
	   
	   // Access methods for relationship fields

	   /**
	    * @ejb:interface-method view-type="local"
	    * @ejb:relation name="overhead-overheadlines"
	    *               role-name="overheadline-has-one-overhead"
	    *               cascade-delete="yes"
	    *
	    * @jboss:relation related-pk-field="ohCode"
	    *                 fk-column="INV_OVERHEAD"
	    */
	   public abstract LocalInvOverhead getInvOverhead();
	   public abstract void setInvOverhead(LocalInvOverhead invOverhead);
	   
	   /**
	    * @ejb:interface-method view-type="local"
	    * @ejb:relation name="overheadmemoline-overheadlines"
	    *               role-name="overheadline-has-one-overheadmemoline"
	    *               cascade-delete="no"
	    *
	    * @jboss:relation related-pk-field="omlCode"
	    *                 fk-column="INV_OVERHEAD_MEMO_LINE"
	    */
	   public abstract LocalInvOverheadMemoLine getInvOverheadMemoLine();
	   public abstract void setInvOverheadMemoLine(LocalInvOverheadMemoLine invOverheadMemoLine);
	   
	   /**
	    * @ejb:interface-method view-type="local"
	    * @ejb:relation name="unitofmeasure-overheadlines"
	    *               role-name="overheadline-has-one-unitofmeasure"
	    *               cascade-delete="no"
	    *
	    * @jboss:relation related-pk-field="uomCode"
	    *                 fk-column="INV_UNIT_OF_MEASURE"
	    */
	   public abstract LocalInvUnitOfMeasure getInvUnitOfMeasure();
	   public abstract void setInvUnitOfMeasure(LocalInvUnitOfMeasure invUnitOfMeasure);
	   
	   /**
		 * @jboss:dynamic-ql
		 */
		public abstract Collection ejbSelectGeneric(java.lang.String jbossQl, java.lang.Object[] args)
		throws FinderException;
		
		// BUSINESS METHODS
		
		/**
		 * @ejb:home-method view-type="local"
		 */
		public Collection ejbHomeGetOhlByCriteria(java.lang.String jbossQl, java.lang.Object[] args)
		throws FinderException {
			
			return ejbSelectGeneric(jbossQl, args);
			
		}
	   	  
	   // EntityBean methods

	   /**
	    * @ejb:create-method view-type="local"
	    **/
	   public java.lang.Integer ejbCreate (java.lang.Integer OHL_CODE, double OHL_UNT_CST,
	   	  double OHL_QTY, Integer OHL_AD_CMPNY)
	      throws CreateException {

	      Debug.print("invOverheadLineBean ejbCreate");
	      
	      setOhlCode(OHL_CODE);
	      setOhlUnitCost(OHL_UNT_CST);
	      setOhlQuantity(OHL_QTY);
	      setOhlAdCompany(OHL_AD_CMPNY);
	      
	      return null;
	   }

	   /**
	    * @ejb:create-method view-type="local"
	    **/
	   public java.lang.Integer ejbCreate (double OHL_UNT_CST,
		  double OHL_QTY, Integer OHL_AD_CMPNY)
	      throws CreateException {

	      Debug.print("invOverheadLineBean ejbCreate");
	      
	      setOhlUnitCost(OHL_UNT_CST);
	      setOhlQuantity(OHL_QTY);
	      setOhlAdCompany(OHL_AD_CMPNY);

	      return null;
	   }

	   public void ejbPostCreate (java.lang.Integer OHL_CODE, double OHL_UNT_CST,
			  double OHL_QTY, Integer OHL_AD_CMPNY)
	      throws CreateException { }

	   public void ejbPostCreate (double OHL_UNT_CST,
			  double OHL_QTY, Integer OHL_AD_CMPNY)
	      throws CreateException { }
	
}
