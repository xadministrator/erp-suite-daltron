package com.ejb.inv;

import java.util.Collection;

import javax.ejb.CreateException;
import javax.ejb.EJBException;

import com.ejb.ad.LocalAdBranchOverheadMemoLine;
import com.util.AbstractEntityBean;
import com.util.Debug;

/**
 * @ejb:bean name="InvOverheadMemoLineEJB"
 *           display-name="Overhead Memo Line Entity"
 *           type="CMP"
 *           cmp-version="2.x"
 *           view-type="local"
 *           local-jndi-name="omega-ejb/InvOverheadMemoLineEJB"
 *           schema="InvOverheadMemoLine"
 *           primkey-field="omlCode"
 *       
 * @ejb:pk class="java.lang.Integer"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="invuser"
 *                        role-link="invuserlink"
 *
 * @ejb:permission role-name="invuser"
 *
 * @ejb:interface local-class="com.ejb.inv.LocalInvOverheadMemoLine"
 *                local-extends="javax.ejb.EJBLocalObject"
 *
 * @ejb:home local-class="com.ejb.inv.LocalInvOverheadMemoLineHome"
 *           local-extends="javax.ejb.EJBLocalHome"
 * 
 * @ejb:finder signature="Collection findOmlAll(java.lang.Integer AD_CMPNY)"
 *             query="SELECT OBJECT(oml) FROM InvOverheadMemoLine oml WHERE oml.omlAdCompany = ?1"
 * 
 * @ejb:finder signature="Collection findOmlByBranch(java.lang.Integer AD_BRNCH, java.lang.Integer AD_CMPNY)"
 *             query="SELECT OBJECT(oml) FROM InvOverheadMemoLine oml, IN(oml.adBranchOverheadMemoLines) boml WHERE boml.adBranch.brCode = ?1 and oml.omlAdCompany = ?2" 
 *
 * @ejb:finder signature="LocalInvOverheadMemoLine findByOmlName(java.lang.String OML_NM, java.lang.Integer AD_CMPNY)"
 *             query="SELECT OBJECT(oml) FROM InvOverheadMemoLine oml WHERE oml.omlName=?1 AND oml.omlAdCompany = ?2"
 *
 * @ejb:finder signature="Collection findByOmlGlCoaOverheadAccount(java.lang.Integer COA_CODE, java.lang.Integer AD_CMPNY)"
 *             query="SELECT OBJECT(oml) FROM InvOverheadMemoLine oml WHERE oml.omlGlCoaOverheadAccount  = ?1 AND oml.omlAdCompany = ?2"
 * 
 * @ejb:finder signature="Collection findByOmlGlCoaLiabilityAccount(java.lang.Integer COA_CODE, java.lang.Integer AD_CMPNY)"
 *             query="SELECT OBJECT(oml) FROM InvOverheadMemoLine oml WHERE oml.omlGlCoaLiabilityAccount  = ?1 AND oml.omlAdCompany = ?2"
 * 
 * @ejb:value-object match="*"
 *             name="InvOverheadMemoLine"
 *
 * @jboss:persistence table-name="INV_OVRHD_MMO_LN"
 *
 * @jboss:entity-command name="mysql-get-generated-keys"
 *
 */

public abstract class InvOverheadMemoLineBean extends AbstractEntityBean {

//	 Access methods for persistent fields

	   /**
	    * @ejb:persistent-field
	    * @ejb:interface-method view-type="local"
	    * @ejb:pk-field
	    *
	    * @jboss:column-name name="OML_CODE"
	    *
	    * @jboss:persistence auto-increment="true"
	    **/
	   public abstract java.lang.Integer getOmlCode();
	   public abstract void setOmlCode(java.lang.Integer OML_CODE);

	   /**
	    * @ejb:persistent-field
	    * @ejb:interface-method view-type="local"
	    *
	    * @jboss:column-name name="OML_NM"
	    **/
	   public abstract String getOmlName();
	   /**
	    * @ejb:interface-method view-type="local"
	    **/
	   public abstract void setOmlName(String OML_NM);

	   /**
	    * @ejb:persistent-field
	    * @ejb:interface-method view-type="local"
	    *
	    * @jboss:column-name name="OML_DESC"
	    **/
	   public abstract String getOmlDescription();
	   /**
	    * @ejb:interface-method view-type="local"
	    **/
	   public abstract void setOmlDescription(String OML_DESC);
	   
	   /**
	    * @ejb:persistent-field
	    * @ejb:interface-method view-type="local"
	    *
	    * @jboss:column-name name="OML_UNT_CST"
	    **/
	   public abstract double getOmlUnitCost();
	   /**
	    * @ejb:interface-method view-type="local"
	    **/
	   public abstract void setOmlUnitCost(double OML_UNT_CST);	   
	   
	   /**
	    * @ejb:persistent-field
	    * @ejb:interface-method view-type="local"
	    *
	    * @jboss:column-name name="OML_GL_COA_OVRHD_ACCNT"
	    **/
	   public abstract java.lang.Integer getOmlGlCoaOverheadAccount();
	   /**
	    * @ejb:interface-method view-type="local"
	    **/
	   public abstract void setOmlGlCoaOverheadAccount(java.lang.Integer OML_GL_COA_OVRHD_ACCNT);
	   
	   /**
	    * @ejb:persistent-field
	    * @ejb:interface-method view-type="local"
	    *
	    * @jboss:column-name name="OML_GL_COA_LBLTY_ACCNT"
	    **/
	   public abstract java.lang.Integer getOmlGlCoaLiabilityAccount();
	   /**
	    * @ejb:interface-method view-type="local"
	    **/
	   public abstract void setOmlGlCoaLiabilityAccount(java.lang.Integer OML_GL_COA_LBLTY_ACCNT);
	   
	   
	   /**
	    * @ejb:persistent-field
	    * @ejb:interface-method view-type="local"
	    *
	    * @jboss:column-name name="OML_AD_CMPNY"
	    **/
	   public abstract java.lang.Integer getOmlAdCompany();
	   /**
	    * @ejb:interface-method view-type="local"
	    **/
	   public abstract void setOmlAdCompany(java.lang.Integer OML_AD_CMPNY);
	   
	   // Access methods for relationship fields
	   
	   /**
	    * @ejb:interface-method view-type="local"
	    * @ejb:relation name="unitofmeasure-overheadmemolines"
	    *               role-name="overheadmemoline-has-one-unitofmeasure"
	    *               cascade-delete="yes"
	    *
	    * @jboss:relation related-pk-field="uomCode"
	    *                 fk-column="INV_UNIT_OF_MEASURE"
	    */
	   public abstract LocalInvUnitOfMeasure getInvUnitOfMeasure();
	   public abstract void setInvUnitOfMeasure(LocalInvUnitOfMeasure invUnitOfMeasure);
	   
	   /**
	    * @ejb:interface-method view-type="local"
	    * @ejb:relation name="overheadmemoline-overheadlines"
	    *               role-name="overheadmemoline-has-many-overheadlines;"
	    */
	   public abstract Collection getInvOverheadLines();
	   public abstract void setInvOverheadLines(Collection invOverheadLines);

	   /**
	    * @ejb:interface-method view-type="local"
	    * @ejb:relation name="overheadmemoline-branchomls"
	    * 				role-name="overheadmemoline-has-many-branchomls"
	    * 
	    */
	   public abstract Collection getAdBranchOverheadMemoLines();
	   public abstract void setAdBranchOverheadMemoLines(Collection adBranchOmls);

	   
	   // Business methods
	   
	   /**
	    * @ejb:interface-method view-type="local"
	    **/
	   public void addInvOverheadLine(LocalInvOverheadLine invOverheadLine) {
	
	        Debug.print("InvOverheadMemoLineBean addInvOverheadLine");
	  
	        try {
	  	
	        Collection invOverheadLines = getInvOverheadLines();
	        invOverheadLines.add(invOverheadLine);
		     
	        } catch (Exception ex) {
	  	
	            throw new EJBException(ex.getMessage());
	     
	        }
	      
	   }
	
       /**
        * @ejb:interface-method view-type="local"
        **/ 
       public void dropInvOverheadLine(LocalInvOverheadLine invOverheadLine) {
		
		    Debug.print("InvOverheadMemoLineBean dropInvOverheadLine");
		  
		    try {
		  	
		       Collection invOverheadLines = getInvOverheadLines();
		       invOverheadLines.remove(invOverheadLine);
			     
		    } catch (Exception ex) {
		  	
		       throw new EJBException(ex.getMessage());
		     
		    }
		  
       }

	   /**
	    * @ejb:interface-method view-type="local"
	    **/
	   public void addAdBranchOverheadMemoLine(LocalAdBranchOverheadMemoLine adBranchOml) {
	
	        Debug.print("InvOverheadMemoLineBean addAdBranchOverheadMemoLine");
	  
	        try {
	  	
	        	Collection adBranchOmls = getAdBranchOverheadMemoLines();
	        	adBranchOmls.add(adBranchOml);
		     
	        } catch (Exception ex) {
	  	
	            throw new EJBException(ex.getMessage());
	     
	        }
	      
	   }
	
       /**
        * @ejb:interface-method view-type="local"
        **/ 
       public void dropAdBranchOverheadMemoLine(LocalAdBranchOverheadMemoLine adBranchOml) {
		
		    Debug.print("InvOverheadMemoLineBean dropAdBranchOverheadMemoLine");
		  
		    try {
		  	
		    	Collection adBranchOmls = getAdBranchOverheadMemoLines();
		    	adBranchOmls.remove(adBranchOml);
			     
		    } catch (Exception ex) {
		  	
		       throw new EJBException(ex.getMessage());
		     
		    }
		  
       }
       
       
       // ENTITY METHODS
       
	   /**
	    * @ejb:create-method view-type="local"
	    **/
	   public java.lang.Integer ejbCreate (java.lang.Integer OML_CODE, String OML_NM,
	   	  String OML_DESC, double OML_UNT_CST, java.lang.Integer OML_GL_COA_OVRHD_ACCOUNT, 
		  java.lang.Integer OML_GL_COA_LBLTY_ACCOUNT, Integer OML_AD_CMPNY)
	      throws CreateException {

	      Debug.print("InvOverheadMemoLineBean ejbCreate");
	      setOmlCode(OML_CODE);
	      setOmlName(OML_NM);
	      setOmlDescription(OML_DESC);
	      setOmlUnitCost(OML_UNT_CST);
	      setOmlGlCoaOverheadAccount(OML_GL_COA_OVRHD_ACCOUNT);
	      setOmlGlCoaLiabilityAccount(OML_GL_COA_LBLTY_ACCOUNT);
	      setOmlAdCompany(OML_AD_CMPNY);

	      return null;
	      
	   }

	   /**
	    * @ejb:create-method view-type="local"
	    **/
	   public java.lang.Integer ejbCreate (String OML_NM,
		   	  String OML_DESC, double OML_UNT_CST, java.lang.Integer OML_GL_COA_OVRHD_ACCOUNT, 
			  java.lang.Integer OML_GL_COA_LBLTY_ACCOUNT, Integer OML_AD_CMPNY)
	      throws CreateException {

	      Debug.print("InvOverheadMemoLineBean ejbCreate");
	      
	      setOmlName(OML_NM);
	      setOmlDescription(OML_DESC);
	      setOmlUnitCost(OML_UNT_CST);
	      setOmlGlCoaOverheadAccount(OML_GL_COA_OVRHD_ACCOUNT);
	      setOmlGlCoaLiabilityAccount(OML_GL_COA_LBLTY_ACCOUNT);
	      setOmlAdCompany(OML_AD_CMPNY);
	      
	      return null;
	      
	   }

	   public void ejbPostCreate (java.lang.Integer OML_CODE, String OML_NM,
		   	  String OML_DESC, double OML_UNT_CST, java.lang.Integer OML_GL_COA_OVRHD_ACCOUNT, 
			  java.lang.Integer OML_GL_COA_LBLTY_ACCOUNT, Integer OML_AD_CMPNY)
	      throws CreateException { }

	   public void ejbPostCreate (String OML_NM,
		   	  String OML_DESC, double OML_UNT_CST, java.lang.Integer OML_GL_COA_OVRHD_ACCOUNT, 
			  java.lang.Integer OML_GL_COA_LBLTY_ACCOUNT, Integer OML_AD_CMPNY)
	      throws CreateException { }
	
}
