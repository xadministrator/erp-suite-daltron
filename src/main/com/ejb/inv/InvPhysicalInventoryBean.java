/*
 * com/ejb/inv/LocalInvPhysicalInventoryBean.java
 *
 * Created on May 20, 2004 05:55 PM
 */

package com.ejb.inv;

import java.util.Collection;
import java.util.Date;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;

import com.util.AbstractEntityBean;
import com.util.Debug;
	
/**
 *
 * @author  Enrico C. Yap
 *
 * @ejb:bean name="InvPhysicalInventoryEJB"
 *           display-name="Physical Inventory Entity"
 *           type="CMP"
 *           cmp-version="2.x"
 *           view-type="local"
 *           local-jndi-name="omega-ejb/InvPhysicalInventoryEJB"
 *           schema="InvPhysicalInventory"
 *           primkey-field="piCode"
 *
 * @ejb:pk class="java.lang.Integer"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="invuser"
 *                        role-link="invuserlink"
 *
 * @ejb:permission role-name="invuser"
 *
 * @ejb:interface local-class="com.ejb.inv.LocalInvPhysicalInventory"
 *                local-extends="javax.ejb.EJBLocalObject"
 *
 * @ejb:home local-class="com.ejb.inv.LocalInvPhysicalInventoryHome"
 *           local-extends="javax.ejb.EJBLocalHome"
 * 
 * @ejb:finder signature="LocalInvPhysicalInventory findByPiDate(java.util.Date PI_DT, java.lang.Integer PI_AD_CMPNY)"
 *             query="SELECT OBJECT(pi) FROM InvPhysicalInventory pi WHERE pi.piDate=?1 AND pi.piAdCompany=?2"
 * 
 * @ejb:finder signature="LocalInvPhysicalInventory findByPiDateAndLocNameAndBrCode(java.util.Date PI_DT, java.lang.String LOC_NM, java.lang.Integer PI_AD_BRNCH, java.lang.Integer PI_AD_CMPNY)"
 *             query="SELECT OBJECT(pi) FROM InvPhysicalInventory pi WHERE pi.piDate=?1 AND pi.invLocation.locName=?2 AND pi.piAdBranch = ?3 AND pi.piAdCompany=?4"
 *
 * @ejb:finder signature="LocalInvPhysicalInventory findByPiDateAndLocNameAndCategoryName(java.util.Date PI_DT, java.lang.String LOC_NM, java.lang.String CTGRY_NM, java.lang.Integer PI_AD_CMPNY)"
 *             query="SELECT OBJECT(pi) FROM InvPhysicalInventory pi WHERE pi.piDate=?1 AND pi.invLocation.locName=?2 AND pi.piAdLvCategory=?3 AND pi.piAdCompany = ?4"
 * 
 * @ejb:finder signature="LocalInvPhysicalInventory findByPiDateAndLocNameAndCategoryNameAndBrCode(java.util.Date PI_DT, java.lang.String LOC_NM, java.lang.String CTGRY_NM, java.lang.Integer PI_AD_BRNCH, java.lang.Integer PI_AD_CMPNY)"
 *             query="SELECT OBJECT(pi) FROM InvPhysicalInventory pi WHERE pi.piDate=?1 AND pi.invLocation.locName=?2 AND pi.piAdLvCategory=?3 AND pi.piAdBranch = ?4 AND pi.piAdCompany = ?5"
 * 
 * @ejb:finder signature="Collection findByPiAdLvCategory(java.lang.String PI_AD_LV_CTGRY, java.lang.Integer PI_AD_CMPNY)"
 *             query="SELECT OBJECT(pi) FROM InvPhysicalInventory pi WHERE pi.piAdLvCategory=?1 AND pi.piAdCompany=?2"
 * 
 * @ejb:select signature="Collection ejbSelectGeneric(java.lang.String jbossQl, java.lang.Object[] args)"
 *             result-type-mapping="Local"
 *             method-intf="LocalHome"
 *             query="SELECT OBJECT(pi) FROM InvPhysicalInventory pi"
 *
 * @jboss:query signature="Collection ejbSelectGeneric(java.lang.String jbossQl, java.lang.Object[] args)"
 *                         dynamic="true"
 *
 * @ejb:value-object match="*"
 *             name="InvPhysicalInventory"
 *
 * @jboss:persistence table-name="INV_PHYSCL_INVNTRY"
 *
 * @jboss:entity-command name="mysql-get-generated-keys"
 */

public abstract class InvPhysicalInventoryBean extends AbstractEntityBean {
    
    
    // PERSISTENT METHODS
    
    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     * @ejb:pk-field
     *
     * @jboss:column-name name="PI_CODE"
     *
     * @jboss:persistence auto-increment="true"
     **/
    public abstract Integer getPiCode();         
    public abstract void setPiCode(Integer PI_CODE);
     
    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="PI_DT"
     **/
     public abstract Date getPiDate();
     /**
      * @ejb:interface-method view-type="local"
      **/
     public abstract void setPiDate(Date PI_DT);
     
	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="PI_RFRNC_NMBR"
	 **/
	 public abstract String getPiReferenceNumber();
	 /**
	  * @ejb:interface-method view-type="local"
	  **/
	 public abstract void setPiReferenceNumber(String PI_RFRNC_NMBR);
      
    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="PI_DESC"
     **/
     public abstract String getPiDescription();
     /**
      * @ejb:interface-method view-type="local"
      **/
     public abstract void setPiDescription(String PI_DESC);
     	
	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="PI_CRTD_BY"
	 **/
	public abstract String getPiCreatedBy();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setPiCreatedBy(String PI_CRTD_BY);
	
	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="PI_DT_CRTD"
	 **/
	public abstract java.util.Date getPiDateCreated();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setPiDateCreated(java.util.Date PI_DT_CRTD);
	
	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="PI_LST_MDFD_BY"
	 **/
	public abstract String getPiLastModifiedBy();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setPiLastModifiedBy(String PI_LST_MDFD_BY);
	
	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="PI_DT_LST_MDFD"
	 **/
	public abstract java.util.Date getPiDateLastModified();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setPiDateLastModified(java.util.Date PI_DT_LST_MDFD);
	
	/**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="PI_AD_LV_CTGRY"
     **/
     public abstract String getPiAdLvCategory();
     /**
      * @ejb:interface-method view-type="local"
      **/
     public abstract void setPiAdLvCategory(String PI_AD_LV_CTGRY);
     
     /**
      * @ejb:persistent-field
      * @ejb:interface-method view-type="local"
      *
      * @jboss:column-name name="PI_VRNC_ADJSTD"
      **/
      public abstract byte getPiVarianceAdjusted();
      /**
       * @ejb:interface-method view-type="local"
       **/
      public abstract void setPiVarianceAdjusted(byte PI_VRNC_ADJSTD);
      
      /**
       * @ejb:persistent-field
       * @ejb:interface-method view-type="local"
       *
       * @jboss:column-name name="PI_WSTG_ADJSTD"
       **/
       public abstract byte getPiWastageAdjusted();
       /**
        * @ejb:interface-method view-type="local"
        **/
       public abstract void setPiWastageAdjusted(byte PI_WSTG_ADJSTD);
     
       /**
        * @ejb:persistent-field
        * @ejb:interface-method view-type="local"
        *
        * @jboss:column-name name="PI_AD_BRNCH"
        **/
       public abstract Integer getPiAdBranch();
       /**
        * @ejb:interface-method view-type="local"
        **/
       public abstract void setPiAdBranch(Integer PI_AD_BRNCH);
       
       /**
        * @ejb:persistent-field
        * @ejb:interface-method view-type="local"
        *
        * @jboss:column-name name="PI_AD_CMPNY"
        **/
       public abstract Integer getPiAdCompany();
       /**
        * @ejb:interface-method view-type="local"
        **/
       public abstract void setPiAdCompany(Integer PI_AD_CMPNY);
	
    // RELATIONSHIP METHODS    
    
     /**
      * @ejb:interface-method view-type="local"
      * @ejb:relation name="location-physicalinventories"
      *               role-name="physicalinventories-has-one-location"
      *               cascade-delete="no"
      * 
      * @jboss:relation related-pk-field="locCode"
      *                 fk-column="INV_LOCATION"
      *                 
      */
     public abstract LocalInvLocation getInvLocation();
     /**
      * @ejb:interface-method view-type="local"
      **/
     public abstract void setInvLocation(LocalInvLocation invLocation);    

     /**
      * @ejb:interface-method view-type="local"
      * @ejb:relation name="physicalinventory-physicalinventorylines"
      *               role-name="physicalinventory-has-many-physicalinventorylines"
      *               
      */
     public abstract Collection getInvPhysicalInventoryLines();
     public abstract void setInvPhysicalInventoryLines(Collection invPhysicalInventories);

	 /**
	  * @jboss:dynamic-ql
	  */
	 public abstract Collection ejbSelectGeneric(java.lang.String jbossQl, java.lang.Object[] args)
	     throws FinderException;
		
	 // BUSINESS METHODS
		
	 /**
	  * @ejb:home-method view-type="local"
	  */
	 public Collection ejbHomeGetPiByCriteria(java.lang.String jbossQl, java.lang.Object[] args)
	     throws FinderException {
			
	     return ejbSelectGeneric(jbossQl, args);
			
	 }
     
     /**
      * @ejb:interface-method view-type="local"
      **/
     public void addInvPhysicalInventoryLine(LocalInvPhysicalInventoryLine invPhysicalInventoryLine) {

          Debug.print("InvPhysicalInventoryBean addInvPhysicalInventoryLine");
      
          try {
      	
             Collection invPhysicalInventoryLines = getInvPhysicalInventoryLines();
             invPhysicalInventoryLines.add(invPhysicalInventoryLine);
	          
          } catch (Exception ex) {
      	
            throw new EJBException(ex.getMessage());
      
          }   
     }

     /**
      * @ejb:interface-method view-type="local"
      **/
     public void dropInvPhysicalInventoryLine(LocalInvPhysicalInventoryLine invPhysicalInventoryLine) {

          Debug.print("InvPhysicalInventoryBean dropInvPhysicalInventoryLine");
      
          try {
      	
             Collection invPhysicalInventoryLines = getInvPhysicalInventoryLines();
             invPhysicalInventoryLines.remove(invPhysicalInventoryLine);
	      
          } catch (Exception ex) {
      	
            throw new EJBException(ex.getMessage());
         
          }
             
     }     
   
    // ENTITY METHODS
    
     /**
      * @ejb:create-method view-type="local"
      **/
     public Integer ejbCreate(Integer PI_CODE, Date PI_DT,
     	 String PI_RFRNC_NMBR, String PI_DESC, String PI_CRTD_BY, 
     	 Date PI_DT_CRTD, String PI_LST_MDFD_BY, Date PI_DT_LST_MDFD, String PI_AD_LV_CTGRY,
     	 byte PI_VRNC_ADJSTD, byte PI_WSTG_ADJSTD,
		 Integer PI_AD_BRNCH, Integer PI_AD_CMPNY)
         throws CreateException {
           
         Debug.print("InvPhysicalInventoryBean ejbCreate");
        
         setPiCode(PI_CODE);
         setPiDate(PI_DT);
         setPiReferenceNumber(PI_RFRNC_NMBR);
         setPiDescription(PI_DESC);
		 setPiCreatedBy(PI_CRTD_BY);
		 setPiDateCreated(PI_DT_CRTD);
		 setPiLastModifiedBy(PI_LST_MDFD_BY);
		 setPiDateLastModified(PI_DT_LST_MDFD);
		 setPiAdLvCategory(PI_AD_LV_CTGRY);
		 setPiVarianceAdjusted(PI_VRNC_ADJSTD);
		 setPiWastageAdjusted(PI_WSTG_ADJSTD);
		 setPiAdBranch(PI_AD_BRNCH);
		 setPiAdCompany(PI_AD_CMPNY);

         return null;
        
     }
    
     /**
      * @ejb:create-method view-type="local"
      **/
     public Integer ejbCreate(Date PI_DT,
     	 String PI_RFRNC_NMBR, String PI_DESC, String PI_CRTD_BY, 
     	 Date PI_DT_CRTD, String PI_LST_MDFD_BY, Date PI_DT_LST_MDFD, String PI_AD_LV_CTGRY,
     	 byte PI_VRNC_ADJSTD, byte PI_WSTG_ADJSTD,
		 Integer PI_AD_BRNCH, Integer PI_AD_CMPNY)
         throws CreateException {
           
         Debug.print("InvPhysicalInventoryBean ejbCreate");
               
         setPiDate(PI_DT);
         setPiReferenceNumber(PI_RFRNC_NMBR);
         setPiDescription(PI_DESC);
		 setPiCreatedBy(PI_CRTD_BY);
		 setPiDateCreated(PI_DT_CRTD);
		 setPiLastModifiedBy(PI_LST_MDFD_BY);
		 setPiDateLastModified(PI_DT_LST_MDFD);
		 setPiAdLvCategory(PI_AD_LV_CTGRY);
		 setPiVarianceAdjusted(PI_VRNC_ADJSTD);
		 setPiWastageAdjusted(PI_WSTG_ADJSTD);
		 setPiAdBranch(PI_AD_BRNCH);
		 setPiAdCompany(PI_AD_CMPNY);

         return null;
        
     }
        
     public void ejbPostCreate(Integer PI_CODE, Date PI_DT,
     	 String PI_RFRNC_NMBR, String PI_DESC, String PI_CRTD_BY, 
     	 Date PI_DT_CRTD, String PI_LST_MDFD_BY, Date PI_DT_LST_MDFD, String PI_AD_LV_CTGRY,
     	 byte PI_VRNC_ADJSTD, byte PI_WSTG_ADJSTD,
		 Integer PI_AD_BRNCH, Integer PI_AD_CMPNY)
         throws CreateException { }
   
     public void ejbPostCreate(Date PI_DT,
     	 String PI_RFRNC_NMBR, String PI_DESC, String PI_CRTD_BY, 
     	 Date PI_DT_CRTD, String PI_LST_MDFD_BY, Date PI_DT_LST_MDFD, String PI_AD_LV_CTGRY,
     	 byte PI_VRNC_ADJSTD, byte PI_WSTG_ADJSTD,
		 Integer PI_AD_BRNCH, Integer PI_AD_CMPNY)
         throws CreateException { }
        
}

