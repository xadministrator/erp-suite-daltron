/*
 * com/ejb/inv/LocalInvPhysicalInventoryLineBean.java
 *
 * Created on May 20, 2004 06:18 PM
 */

package com.ejb.inv;

import java.util.Collection;

import javax.ejb.CreateException;

import com.util.AbstractEntityBean;
import com.util.Debug;

/**
 *
 * @author  Enrico C. Yap
 *
 * @ejb:bean name="InvPhysicalInventoryLineEJB"
 *           display-name="Physical Inventory Line Entity"
 *           type="CMP"
 *           cmp-version="2.x"
 *           view-type="local"
 *           local-jndi-name="omega-ejb/InvPhysicalInventoryLineEJB"
 *           schema="InvPhysicalInventoryLine"
 *           primkey-field="pilCode"
 *
 * @ejb:pk class="java.lang.Integer"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="invuser"
 *                        role-link="invuserlink"
 *
 * @ejb:permission role-name="invuser"
 *
 * @ejb:interface local-class="com.ejb.inv.LocalInvPhysicalInventoryLine"
 *                local-extends="javax.ejb.EJBLocalObject"
 *
 * @ejb:home local-class="com.ejb.inv.LocalInvPhysicalInventoryLineHome"
 *           local-extends="javax.ejb.EJBLocalHome"
 *
 * @ejb:finder signature="Collection findByPiDateAndLocName(java.util.Date PI_DT, java.lang.String LOC_NM, java.lang.Integer PIL_AD_CMPNY)"
 *             query="SELECT OBJECT(pil) FROM InvPhysicalInventoryLine pil WHERE pil.invPhysicalInventory.piDate=?1 AND pil.invItemLocation.invLocation.locName=?2 and pil.pilAdCompany=?3"
 *
 * @jboss:query signature="Collection findByPiDateAndLocName(java.util.Date PI_DT, java.lang.String LOC_NM, java.lang.Integer PIL_AD_CMPNY)"
 *             query="SELECT OBJECT(pil) FROM InvPhysicalInventoryLine pil WHERE pil.invPhysicalInventory.piDate=?1 AND pil.invItemLocation.invLocation.locName=?2 and pil.pilAdCompany=?3 ORDER BY pil.invItemLocation.invItem.iiName"
 *
 * @ejb:finder signature="LocalInvPhysicalInventoryLine findByPiCodeAndIlCode(java.lang.Integer PI_CODE, java.lang.Integer LOC_CODE, java.lang.Integer PIL_AD_CMPNY)"
 *             query="SELECT OBJECT(pil) FROM InvPhysicalInventoryLine pil WHERE pil.invPhysicalInventory.piCode=?1 AND pil.invItemLocation.ilCode=?2 and pil.pilAdCompany=?3"
 *
 * @ejb:finder signature="Collection findByPiCode(java.lang.Integer PI_CODE, java.lang.Integer PIL_AD_CMPNY)"
 *             query="SELECT OBJECT(pil) FROM InvPhysicalInventoryLine pil WHERE pil.invPhysicalInventory.piCode=?1 AND pil.pilAdCompany = ?2 ORDER BY pil.invItemLocation.invItem.iiName"
 *
 * @jboss:query signature="Collection findByPiCode(java.lang.Integer PI_CODE, java.lang.Integer PIL_AD_CMPNY)"
 *             query="SELECT OBJECT(pil) FROM InvPhysicalInventoryLine pil WHERE pil.invPhysicalInventory.piCode=?1 AND pil.pilAdCompany = ?2 ORDER BY pil.invItemLocation.invItem.iiName"
 *
 *
 * @ejb:value-object match="*"
 *             name="InvPhysicalInventoryLine"
 *
 * @jboss:persistence table-name="INV_PHYSCL_INVNTRY_LN"
 *
 * @jboss:entity-command name="mysql-get-generated-keys"
 */

public abstract class InvPhysicalInventoryLineBean extends AbstractEntityBean {


    // PERSISTENT METHODS

    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     * @ejb:pk-field
     *
     * @jboss:column-name name="PIL_CODE"
     *
     * @jboss:persistence auto-increment="true"
     **/
    public abstract Integer getPilCode();
    public abstract void setPilCode(Integer PIL_CODE);

    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="PIL_ENDNG_INVNTRY"
     **/
     public abstract double getPilEndingInventory();
     /**
      * @ejb:interface-method view-type="local"
      **/
     public abstract void setPilEndingInventory(double PIL_ENDNG_INVNTRY);


     /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="PIL_WSTG"
     **/
     public abstract double getPilWastage();
     /**
      * @ejb:interface-method view-type="local"
      **/
     public abstract void setPilWastage(double PIL_WSTG);

     /**
      * @ejb:persistent-field
      * @ejb:interface-method view-type="local"
      *
      * @jboss:column-name name="PIL_VRNC"
      **/
      public abstract double getPilVariance();
      /**
       * @ejb:interface-method view-type="local"
       **/
      public abstract void setPilVariance(double PIL_VRNC);

	   /**
	    * @ejb:persistent-field
	    * @ejb:interface-method view-type="local"
	    *
	    * @jboss:column-name name="PIL_EXPRY_DT"
	    **/
	   public abstract String getPilMisc();

	   /**
	    * @ejb:interface-method view-type="local"
	    **/
	   public abstract void setPilMisc(String PIL_EXPRY_DT);

     /**
      * @ejb:persistent-field
      * @ejb:interface-method view-type="local"
      *
      * @jboss:column-name name="PIL_AD_CMPNY"
      **/
      public abstract Integer getPilAdCompany();
      /**
       * @ejb:interface-method view-type="local"
       **/
      public abstract void setPilAdCompany(Integer PIL_AD_CMPNY);

    // RELATIONSHIP METHODS

     /**
      * @ejb:interface-method view-type="local"
      * @ejb:relation name="physicalinventory-physicalinventorylines"
      *               role-name="physicalinventorylines-has-one-physicalinventory"
      *               cascade-delete="yes"
      *
      * @jboss:relation related-pk-field="piCode"
      *                 fk-column="INV_PHYSICAL_INVENTORY"
      */
     public abstract LocalInvPhysicalInventory getInvPhysicalInventory();
     /**
      * @ejb:interface-method view-type="local"
      **/
     public abstract void setInvPhysicalInventory(LocalInvPhysicalInventory invPhysicalInventory);

     /**
      * @ejb:interface-method view-type="local"
      * @ejb:relation name="itemlocation-physicalinventorylines"
      *               role-name="physicalinventorylines-has-one-itemlocation"
      *               cascade-delete="no"
      *
      * @jboss:relation related-pk-field="ilCode"
      *                 fk-column="INV_ITEM_LOCATION"
      */
     public abstract LocalInvItemLocation getInvItemLocation();
     /**
      * @ejb:interface-method view-type="local"
      **/
     public abstract void setInvItemLocation(LocalInvItemLocation invItemLocation);

     /**
      * @ejb:interface-method view-type="local"
      * @ejb:relation name="unitofmeasure-physicalinventorylines"
      *               role-name="physicalinventorylines-has-one-unitofmeasure"
      *
      * @jboss:relation related-pk-field="uomCode"
      *                 fk-column="INV_UNIT_OF_MEASURE"
      */
     public abstract LocalInvUnitOfMeasure getInvUnitOfMeasure();
     /**
      * @ejb:interface-method view-type="local"
      **/
     public abstract void setInvUnitOfMeasure(LocalInvUnitOfMeasure invUnitOfMeasure);


     // BUSINESS METHODS


     /**
 	 * @ejb:interface-method view-type="local"
 	 * @ejb:relation name="physicalinventoryline-tags"
 	 *               role-name="physicalinventoryline-has-many-tag"
 	 */
 	public abstract Collection getInvTags();
 	public abstract void setInvTags(Collection intTags);

    // ENTITY METHODS

     /**
      * @ejb:create-method view-type="local"
      **/
     public Integer ejbCreate(Integer PIL_CODE, double PIL_ENDNG_INVNTRY,
    	 double PIL_WSTG, double PIL_VRNC, String PIL_EXPRY_DT,
     		Integer PIL_AD_CMPNY)
         throws CreateException {

         Debug.print("InvPhysicalInventoryLineBean ejbCreate");

         setPilCode(PIL_CODE);
         setPilEndingInventory(PIL_ENDNG_INVNTRY);
         setPilWastage(PIL_WSTG);
         setPilVariance(PIL_VRNC);
         setPilMisc(PIL_EXPRY_DT);
         setPilAdCompany(PIL_AD_CMPNY);

         return null;

     }

     /**
      * @ejb:create-method view-type="local"
      **/
     public Integer ejbCreate(double PIL_ENDNG_INVNTRY,
     	 double PIL_WSTG, double PIL_VRNC, String PIL_EXPRY_DT,
     		Integer PIL_AD_CMPNY)
         throws CreateException {

         Debug.print("InvPhysicalInventoryLineBean ejbCreate");

         setPilEndingInventory(PIL_ENDNG_INVNTRY);
         setPilWastage(PIL_WSTG);
         setPilVariance(PIL_VRNC);
         setPilMisc(PIL_EXPRY_DT);
         setPilAdCompany(PIL_AD_CMPNY);

         return null;

     }

     public void ejbPostCreate(Integer PIL_CODE, double PIL_ENDNG_INVNTRY,
     	 double PIL_WSTG, double PIL_VRNC, String PIL_EXPRY_DT,
     		Integer PIL_AD_CMPNY)
         throws CreateException { }

     public void ejbPostCreate(double PIL_ENDNG_INVNTRY,
         double PIL_WSTG, double PIL_VRNC, String PIL_EXPRY_DT,
     		Integer PIL_AD_CMPNY)
         throws CreateException { }

}

