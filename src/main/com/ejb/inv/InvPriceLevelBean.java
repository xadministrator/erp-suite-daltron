package com.ejb.inv;

import java.util.Collection;

import javax.ejb.CreateException;
import javax.ejb.FinderException;

import com.util.AbstractEntityBean;
import com.util.Debug;

/**
 * @ejb:bean name="InvPriceLevelEJB"
 *           display-name="Price Level Entity"
 *           type="CMP"
 *           cmp-version="2.x"
 *           view-type="local"
 *           local-jndi-name="omega-ejb/InvPriceLevelEJB"
 *           schema="InvPriceLevel"
 *           primkey-field="plCode"
 *       
 * @ejb:pk class="java.lang.Integer"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="invuser"
 *                        role-link="invuserlink"
 *
 * @ejb:permission role-name="invuser"
 *
 * @ejb:interface local-class="com.ejb.inv.LocalInvPriceLevel"
 *                local-extends="javax.ejb.EJBLocalObject"
 *
 * @ejb:home local-class="com.ejb.inv.LocalInvPriceLevelHome"
 *           local-extends="javax.ejb.EJBLocalHome"
 * 
 * @ejb:finder signature="Collection findByIiCode(java.lang.Integer II_CODE, java.lang.Integer PL_AD_CMPNY)"
 *             query="SELECT OBJECT(pl) FROM InvPriceLevel pl WHERE pl.invItem.iiCode=?1 AND pl.plAdCompany=?2"
 * 
 * @ejb:finder signature="Collection findByPlAdLvPriceLevel(java.lang.String PL_AD_LV_PRC_LVL, java.lang.Integer PL_AD_CMPNY)"
 *             query="SELECT OBJECT(pl) FROM InvPriceLevel pl WHERE pl.plAdLvPriceLevel=?1 AND pl.plAdCompany=?2"
 * 
 * @ejb:finder signature="LocalInvPriceLevel findByIiNameAndAdLvPriceLevel(java.lang.String II_NM, java.lang.String PL_AD_LV_PRC_LVL, java.lang.Integer PL_AD_CMPNY)"
 *             query="SELECT OBJECT(pl) FROM InvPriceLevel pl WHERE pl.invItem.iiName=?1 AND pl.plAdLvPriceLevel=?2 AND pl.plAdCompany=?3"
 *
 * @ejb:finder signature="Collection findByPlCodeAndPlDownloadStatus(java.lang.Integer PL_CODE, char PL_DWNLD_STATUS, java.lang.Integer PL_AD_CMPNY)"
 *             query="SELECT OBJECT(pl) FROM InvPriceLevel pl WHERE pl.plCode = ?1 AND pl.plDownloadStatus = ?2 AND pl.plAdCompany = ?3"
 * 
 * @ejb:finder signature="Collection findPlByPlNewAndUpdated(java.lang.Integer PL_AD_CMPNY, char NEW, char UPDATED, char DOWNLOADED_UPDATED)"
 *             query="SELECT OBJECT(pl) FROM InvPriceLevel pl WHERE pl.plAdCompany = ?1 AND (pl.plDownloadStatus = ?2 OR pl.plDownloadStatus = ?3 OR pl.plDownloadStatus = ?4)"
 *
 * @jboss:query signature="Collection ejbSelectGeneric(java.lang.String jbossQl, java.lang.Object[] args)"
 *                         dynamic="true"
 *
 * @ejb:value-object match="*"
 *             name="InvPriceLevel"
 *
 * @jboss:persistence table-name="INV_PRC_LVL"
 *
 * @jboss:entity-command name="mysql-get-generated-keys"
 *
 */

public abstract class InvPriceLevelBean extends AbstractEntityBean {
	
//	 Access methods for persistent fields

	   /**
	    * @ejb:persistent-field
	    * @ejb:interface-method view-type="local"
	    * @ejb:pk-field
	    *
	    * @jboss:column-name name="PL_CODE"
	    *
	    * @jboss:persistence auto-increment="true"
	    **/
	   public abstract java.lang.Integer getPlCode();
	   public abstract void setPlCode(java.lang.Integer PL_CODE);

	   /**
	    * @ejb:persistent-field
	    * @ejb:interface-method view-type="local"
	    *
	    * @jboss:column-name name="PL_AMNT"
	    **/
	   public abstract double getPlAmount();

	   /**
	    * @ejb:interface-method view-type="local"
	    **/
	   public abstract void setPlAmount(double PL_AMNT);
	   
	   /**
	    * @ejb:persistent-field
	    * @ejb:interface-method view-type="local"
	    *
	    * @jboss:column-name name="PL_MRGN"
	    **/
	   public abstract double getPlMargin();

	   /**
	    * @ejb:interface-method view-type="local"
	    **/
	   public abstract void setPlMargin(double PL_MRGN);
	   
	   /**
	    * @ejb:persistent-field
	    * @ejb:interface-method view-type="local"
	    *
	    * @jboss:column-name name="PL_PRCNT_MRKUP"
	    **/
	   public abstract double getPlPercentMarkup();

	   /**
	    * @ejb:interface-method view-type="local"
	    **/
	   public abstract void setPlPercentMarkup(double PL_PRCNT_MRKUP);
	   
	   
	   /**
	    * @ejb:persistent-field
	    * @ejb:interface-method view-type="local"
	    *
	    * @jboss:column-name name="PL_SHPPNG_CST"
	    **/
	   public abstract double getPlShippingCost();

	   /**
	    * @ejb:interface-method view-type="local"
	    **/
	   public abstract void setPlShippingCost(double PL_SHPPNG_CST);

	   /**
	    * @ejb:persistent-field
	    * @ejb:interface-method view-type="local"
	    *
	    * @jboss:column-name name="PL_AD_LV_PRC_LVL"
	    **/
	   public abstract java.lang.String getPlAdLvPriceLevel();
	   
	   /**
	    * @ejb:interface-method view-type="local"
	    **/
	   public abstract void setPlAdLvPriceLevel(java.lang.String PL_AD_LV_PRC_LVL);
	   
	   /**
	    * @ejb:persistent-field
	    * @ejb:interface-method view-type="local"
	    *
	    * @jboss:column-name name="PL_DWNLD_STATUS"
	    **/
	   public abstract char getPlDownloadStatus();
	   
	   /**
	    * @ejb:interface-method view-type="local"
	    **/
	   public abstract void setPlDownloadStatus(char PL_DWNLD_STATUS);
	     
	   /**
	    * @ejb:persistent-field
	    * @ejb:interface-method view-type="local"
	    *
	    * @jboss:column-name name="PL_AD_CMPNY"
	    **/
	   public abstract java.lang.Integer getPlAdCompany();

	   /**
	    * @ejb:interface-method view-type="local"
	    **/
	   public abstract void setPlAdCompany(java.lang.Integer PL_AD_CMPNY);

	   // Access methods for relationship fields
	   
	   /**
	    * @ejb:interface-method view-type="local"
	    * @ejb:relation name="item-pricelevels"
	    *               role-name="pricelevel-has-one-item"
	    *               cascade-delete="no"
	    *
	    * @jboss:relation related-pk-field="iiCode"
	    *                 fk-column="INV_ITEM"
	    */
	   public abstract LocalInvItem getInvItem();
	   /**
		 * @ejb:interface-method view-type="local"
		 **/
	   public abstract void setInvItem(LocalInvItem invItem);
	   
		/**
		 * @jboss:dynamic-ql
		 */
		public abstract Collection ejbSelectGeneric(java.lang.String jbossQl, java.lang.Object[] args)
		throws FinderException;	   

	   // Business methods
		
		/**
		 * @ejb:home-method view-type="local"
		 */
		public Collection ejbHomeGetPlByCriteria(java.lang.String jbossQl, java.lang.Object[] args)
		throws FinderException {
			
			return ejbSelectGeneric(jbossQl, args);
			
		}   		
	    
	   // EntityBean methods

	   /**
	    * @ejb:create-method view-type="local"
	    **/
	   public java.lang.Integer ejbCreate (java.lang.Integer PL_CODE, double PL_AMNT, double PL_MRGN,
	      double PL_PRCNT_MRKUP, double PL_SHPPNG_CST,
	   	  java.lang.String PL_AD_LV_PRC_LVL, char PL_DWNLD_STATUS, java.lang.Integer PL_AD_CMPNY)
	      throws CreateException {

	      Debug.print("InvPriceLevelBean ejbCreate");
	      setPlCode(PL_CODE);
	      setPlAmount(PL_AMNT);
	      setPlMargin(PL_MRGN);
	      setPlPercentMarkup(PL_PRCNT_MRKUP);
	      setPlShippingCost(PL_SHPPNG_CST);
	      setPlAdLvPriceLevel(PL_AD_LV_PRC_LVL);
	      setPlDownloadStatus(PL_DWNLD_STATUS);
	      setPlAdCompany(PL_AD_CMPNY);
	      return null;
	   }

	   /**
	    * @ejb:create-method view-type="local"
	    **/
	   public java.lang.Integer ejbCreate (double PL_AMNT, double PL_MRGN,
			   double PL_PRCNT_MRKUP, double PL_SHPPNG_CST,
		   	  java.lang.String PL_AD_LV_PRC_LVL, char PL_DWNLD_STATUS, java.lang.Integer PL_AD_CMPNY)
	      throws CreateException {

	      Debug.print("InvPriceLevelBean ejbCreate");
	      setPlAmount(PL_AMNT);
	      setPlMargin(PL_MRGN);
	      setPlPercentMarkup(PL_PRCNT_MRKUP);
	      setPlShippingCost(PL_SHPPNG_CST);
	      setPlAdLvPriceLevel(PL_AD_LV_PRC_LVL);
	      setPlDownloadStatus(PL_DWNLD_STATUS);
	      setPlAdCompany(PL_AD_CMPNY);
	      return null;
	   }

	   public void ejbPostCreate (java.lang.Integer PL_CODE, double PL_AMNT, double PL_MRGN,
			   double PL_PRCNT_MRKUP, double PL_SHPPNG_CST,
		   	  java.lang.String PL_AD_LV_PRC_LVL, char PL_DWNLD_STATUS, java.lang.Integer PL_AD_CMPNY)
	      throws CreateException { }

	   public void ejbPostCreate (double PL_AMNT, double PL_MRGN,
			   double PL_PRCNT_MRKUP, double PL_SHPPNG_CST,
		   	  java.lang.String PL_AD_LV_PRC_LVL, char PL_DWNLD_STATUS, java.lang.Integer PL_AD_CMPNY)
	      throws CreateException { }
	
}
