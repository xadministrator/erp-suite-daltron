package com.ejb.inv;

import java.util.Collection;

import javax.ejb.CreateException;
import javax.ejb.FinderException;

import com.util.AbstractEntityBean;
import com.util.Debug;

/**
 * @ejb:bean name="InvPriceLevelDateEJB"
 *           display-name="Price Level Entity"
 *           type="CMP"
 *           cmp-version="2.x"
 *           view-type="local"	
 *           local-jndi-name="omega-ejb/InvPriceLevelDateEJB"
 *           schema="InvPriceLevelDate"
 *           primkey-field="pdCode"
 *       
 * @ejb:pk class="java.lang.Integer"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="invuser"
 *                        role-link="invuserlink"
 *
 * @ejb:permission role-name="invuser"
 *
 * @ejb:interface local-class="com.ejb.inv.LocalInvPriceLevelDate"
 *                local-extends="javax.ejb.EJBLocalObject"
 *
 * @ejb:home local-class="com.ejb.inv.LocalInvPriceLevelDateHome"
 *           local-extends="javax.ejb.EJBLocalHome"	
 * 
 * @ejb:finder signature="Collection findByIiCode(java.lang.Integer II_CODE, java.lang.Integer PD_AD_CMPNY)"
 *             query="SELECT OBJECT(pd) FROM InvPriceLevelDate pd WHERE pd.invItem.iiCode=?1 AND pd.pdAdCompany=?2"
 * 
 * @ejb:finder signature="Collection findByPdCode(java.lang.Integer PD_CODE, java.lang.Integer PD_AD_CMPNY)"
 *             query="SELECT OBJECT(pd) FROM InvPriceLevelDate pd WHERE pd.pdCode = ?1  AND pd.pdAdCompany = ?2"
 * 
 * @ejb:finder signature="Collection findPdAll(java.lang.Integer PD_AD_CMPNY)"
 *             query="SELECT OBJECT(pd) FROM InvPriceLevelDate pd WHERE pd.pdAdCompany = ?1"
 *
 * @ejb:finder signature="Collection findPdByIiCodeAndPdDateAndPdStatus(java.lang.String PD_STATUS, java.lang.Integer II_CODE, java.util.Date PD_DT_FRM, java.util.Date PD_DT_TO, java.lang.Integer PD_AD_CMPNY)"
 *             query="SELECT OBJECT(pd) FROM InvPriceLevelDate pd WHERE pd.pdStatus = ?1 AND pd.invItem.iiCode = ?2 AND pd.pdDateFrom >= ?3 AND pd.pdDateTo <= ?4 AND pd.pdAdCompany = ?5"
 *
 * @ejb:finder signature="Collection findPdByIiCodeAndPdPriceLevel(java.lang.String PD_STATUS, java.lang.Integer II_CODE, java.lang.String PD_AD_LV_PRC_LVL, java.lang.Integer PD_AD_CMPNY)"
 *             query="SELECT OBJECT(pd) FROM InvPriceLevelDate pd WHERE pd.pdStatus = ?1 AND pd.invItem.iiCode = ?2 AND pd.pdAdLvPriceLevel >= ?3 AND pd.pdAdCompany = ?4"
 *
 * @ejb:finder signature="LocalInvPriceLevelDate findPdByPdDescription(java.lang.String PD_DESC, java.lang.Integer II_CODE, java.lang.Integer PD_AD_CMPNY)"
 *             query="SELECT OBJECT(pd) FROM InvPriceLevelDate pd WHERE pd.pdDesc = ?1 AND pd.invItem.iiCode = ?2  AND pd.pdAdCompany = ?3"
 *
 * @jboss:query signature="Collection ejbSelectGeneric(java.lang.String jbossQl, java.lang.Object[] args)"
 *                         dynamic="true"
 *
 * @ejb:value-object match="*"
 *             name="InvPriceLevelDate"
 *
 * @jboss:persistence table-name="INV_PRC_LVL_DT"
 *
 * @jboss:entity-command name="mysql-get-generated-keys"
 *
 */

public abstract class InvPriceLevelDateBean extends AbstractEntityBean {
	
//	 Access methods for persistent fields

	   /**
	    * @ejb:persistent-field
	    * @ejb:interface-method view-type="local"
	    * @ejb:pk-field
	    *
	    * @jboss:column-name name="PD_CODE"
	    *
	    * @jboss:persistence auto-increment="true"
	    **/
	   public abstract java.lang.Integer getPdCode();
	   public abstract void setPdCode(java.lang.Integer PD_CODE);





	   /**
	    * @ejb:persistent-field
	    * @ejb:interface-method view-type="local"
	    *
	    * @jboss:column-name name="PD_DESC"
	    **/
	   public abstract java.lang.String getPdDesc();
	   
	   /**
	    * @ejb:interface-method view-type="local"
	    **/
	   public abstract void setPdDesc(java.lang.String PD_DESC);
	   





	   /**
	    * @ejb:persistent-field
	    * @ejb:interface-method view-type="local"
	    *
	    * @jboss:column-name name="PD_AMNT"
	    **/
	   public abstract double getPdAmount();

	   /**
	    * @ejb:interface-method view-type="local"
	    **/
	   public abstract void setPdAmount(double PD_AMNT);
	   
	   /**
	    * @ejb:persistent-field
	    * @ejb:interface-method view-type="local"
	    *
	    * @jboss:column-name name="PD_MRGN"
	    **/
	   public abstract double getPdMargin();

	   /**
	    * @ejb:interface-method view-type="local"
	    **/
	   public abstract void setPdMargin(double PD_MRGN);
	   
	   /**
	    * @ejb:persistent-field
	    * @ejb:interface-method view-type="local"
	    *
	    * @jboss:column-name name="PD_PRCNT_MRKUP"
	    **/
	   public abstract double getPdPercentMarkup();

	   /**
	    * @ejb:interface-method view-type="local"
	    **/
	   public abstract void setPdPercentMarkup(double PD_PRCNT_MRKUP);
	   
	   
	   /**
	    * @ejb:persistent-field
	    * @ejb:interface-method view-type="local"
	    *
	    * @jboss:column-name name="PD_SHPPNG_CST"
	    **/
	   public abstract double getPdShippingCost();

	   /**
	    * @ejb:interface-method view-type="local"
	    **/
	   public abstract void setPdShippingCost(double PD_SHPPNG_CST);

	   /**
	    * @ejb:persistent-field
	    * @ejb:interface-method view-type="local"
	    *
	    * @jboss:column-name name="PD_AD_LV_PRC_LVL"
	    **/
	   public abstract java.lang.String getPdAdLvPriceLevel();
	   
	   /**
	    * @ejb:interface-method view-type="local"
	    **/
	   public abstract void setPdAdLvPriceLevel(java.lang.String PD_AD_LV_PRC_LVL);
	   
	   
	    /**
	    * @ejb:persistent-field
	    * @ejb:interface-method view-type="local"
	    *
	    * @jboss:column-name name="PD_DT_FRM"
	    **/
	   public abstract java.util.Date getPdDateFrom();
	   
	   /**
	    * @ejb:interface-method view-type="local"
	    **/
	   public abstract void setPdDateFrom(java.util.Date PD_DT_FRM);
	   
	   
	   /**
	    * @ejb:persistent-field
	    * @ejb:interface-method view-type="local"
	    *
	    * @jboss:column-name name="PD_DT_TO"
	    **/
	   public abstract java.util.Date getPdDateTo();
	   
	   /**
	    * @ejb:interface-method view-type="local"
	    **/
	   public abstract void setPdDateTo(java.util.Date PD_DT_TO);
	   
	   
	    /**
	    * @ejb:persistent-field
	    * @ejb:interface-method view-type="local"
	    *
	    * @jboss:column-name name="PD_STATUS"
	    **/
	   public abstract java.lang.String getPdStatus();
	   
	   /**
	    * @ejb:interface-method view-type="local"
	    **/
	   public abstract void setPdStatus(java.lang.String PD_STATUS);
	   
	   
	     
	   /**
	    * @ejb:persistent-field
	    * @ejb:interface-method view-type="local"
	    *
	    * @jboss:column-name name="PD_AD_CMPNY"
	    **/
	   public abstract java.lang.Integer getPdAdCompany();

	   /**
	    * @ejb:interface-method view-type="local"
	    **/
	   public abstract void setPdAdCompany(java.lang.Integer PD_AD_CMPNY);

	   // Access methods for relationship fields
	   
	   /**
	    * @ejb:interface-method view-type="local"
	    * @ejb:relation name="item-pricelevelsdate"
	    *               role-name="priceleveldate-has-one-item"
	    *               cascade-delete="no"
	    *
	    * @jboss:relation related-pk-field="iiCode"
	    *                 fk-column="INV_ITEM"
	    */
	   public abstract LocalInvItem getInvItem();
	   /**
		 * @ejb:interface-method view-type="local"
		 **/
	   public abstract void setInvItem(LocalInvItem invItem);
	   
		/**
		 * @jboss:dynamic-ql
		 */
		public abstract Collection ejbSelectGeneric(java.lang.String jbossQl, java.lang.Object[] args)
		throws FinderException;	   

	   // Business methods
		
		/**
		 * @ejb:home-method view-type="local"
		 */
		public Collection ejbHomeGetPdByCriteria(java.lang.String jbossQl, java.lang.Object[] args)
		throws FinderException {
			
			return ejbSelectGeneric(jbossQl, args);
			
		}   		
	    
	   // EntityBean methods

	   /**
	    * @ejb:create-method view-type="local"
	    **/
	   public java.lang.Integer ejbCreate (java.lang.Integer PD_CODE, java.lang.String PD_DESC, double PD_AMNT, double PD_MRGN,
	      double PD_PRCNT_MRKUP, double PD_SHPPNG_CST,
	   	  java.lang.String PD_AD_LV_PRC_LVL, java.util.Date PD_DT_FRM, java.util.Date PD_DT_TO, java.lang.String PD_STATUS, java.lang.Integer PD_AD_CMPNY)
	      throws CreateException {

	      Debug.print("InvPriceLevelDateBean ejbCreate");
	      setPdCode(PD_CODE);
	      setPdDesc(PD_DESC);
	      setPdAmount(PD_AMNT);
	      setPdMargin(PD_MRGN);
	      setPdPercentMarkup(PD_PRCNT_MRKUP);
	      setPdShippingCost(PD_SHPPNG_CST);
	      setPdAdLvPriceLevel(PD_AD_LV_PRC_LVL);
	      setPdDateFrom(PD_DT_FRM);
	      setPdDateTo(PD_DT_TO);
	      setPdStatus(PD_STATUS);
	      setPdAdCompany(PD_AD_CMPNY);
	      return null;
	   }

	   /**
	    * @ejb:create-method view-type="local"
	    **/
	  public java.lang.Integer ejbCreate (java.lang.String PD_DESC, double PD_AMNT, double PD_MRGN,
	      double PD_PRCNT_MRKUP, double PD_SHPPNG_CST,
	   	  java.lang.String PD_AD_LV_PRC_LVL, java.util.Date PD_DT_FRM, java.util.Date PD_DT_TO, java.lang.String PD_STATUS, java.lang.Integer PD_AD_CMPNY)
	      throws CreateException {

	      Debug.print("InvPriceLevelDateBean ejbCreate");
	      
	      setPdDesc(PD_DESC);
	      setPdAmount(PD_AMNT);
	      setPdMargin(PD_MRGN);
	      setPdPercentMarkup(PD_PRCNT_MRKUP);
	      setPdShippingCost(PD_SHPPNG_CST);
	      setPdAdLvPriceLevel(PD_AD_LV_PRC_LVL);
	      setPdDateFrom(PD_DT_FRM);
	      setPdDateTo(PD_DT_TO);
	      setPdStatus(PD_STATUS);
	      setPdAdCompany(PD_AD_CMPNY);
	      return null;
	  }

	   public void ejbPostCreate(java.lang.Integer PD_CODE, java.lang.String PD_DESC, double PD_AMNT, double PD_MRGN,
	      double PD_PRCNT_MRKUP, double PD_SHPPNG_CST,
	   	  java.lang.String PD_AD_LV_PRC_LVL, java.util.Date PD_DT_FRM, java.util.Date PD_DT_TO, java.lang.String PD_STATUS, java.lang.Integer PD_AD_CMPNY)
	      throws CreateException { }

	   public void ejbPostCreate(java.lang.String PD_DESC, double PD_AMNT, double PD_MRGN,
	      double PD_PRCNT_MRKUP, double PD_SHPPNG_CST,
	   	  java.lang.String PD_AD_LV_PRC_LVL, java.util.Date PD_DT_FRM, java.util.Date PD_DT_TO, java.lang.String PD_STATUS, java.lang.Integer PD_AD_CMPNY)
	      throws CreateException { }
	
}
