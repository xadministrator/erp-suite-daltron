package com.ejb.inv;

import java.util.Collection;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;

import com.util.AbstractEntityBean;
import com.util.Debug;

/**
 * @ejb:bean name="InvStockIssuanceEJB"
 *           display-name="Stock Issuance Entity"
 *           type="CMP"
 *           cmp-version="2.x"
 *           view-type="local"
 *           local-jndi-name="omega-ejb/InvStockIssuanceEJB"
 *           schema="InvStockIssuance"
 *           primkey-field="siCode"
 *       
 * @ejb:pk class="java.lang.Integer"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="invuser"
 *                        role-link="invuserlink"
 *
 * @ejb:permission role-name="invuser"
 *
 * @ejb:interface local-class="com.ejb.inv.LocalInvStockIssuance"
 *                local-extends="javax.ejb.EJBLocalObject"
 *
 * @ejb:home local-class="com.ejb.inv.LocalInvStockIssuanceHome"
 *           local-extends="javax.ejb.EJBLocalHome"
 * 
 * @ejb:select signature="Collection ejbSelectGeneric(java.lang.String jbossQl, java.lang.Object[] args)"
 *             result-type-mapping="Local"
 *             method-intf="LocalHome"
 *             query="SELECT OBJECT(si) FROM InvStockIssuance si"
 *
 * @jboss:query signature="Collection ejbSelectGeneric(java.lang.String jbossQl, java.lang.Object[] args)"
 *                         dynamic="true"
 * 
 * @ejb:finder signature="LocalInvStockIssuance findBySiDocumentNumberAndBrCode(java.lang.String SI_DCMNT_NMBR, java.lang.Integer SI_AD_BRNCH, java.lang.Integer SI_AD_CMPNY)"
 *             query="SELECT OBJECT(si) FROM InvStockIssuance si  WHERE si.siDocumentNumber = ?1 AND si.siAdBranch = ?2AND si.siAdCompany = ?3"
 * 
 * @ejb:finder signature="Collection findUnpostedSiBySiDateRange(java.util.Date SI_DT_FRM, java.util.Date SI_DT_TO, java.lang.Integer SI_AD_CMPNY)"
 *             query="SELECT OBJECT(si) FROM InvStockIssuance si  WHERE si.siPosted = 0 AND si.siVoid = 0 AND si.siDate >= ?1 AND si.siDate <= ?2 AND si.siAdCompany = ?3"
 *
 * @jboss:query signature="Collection findUnpostedSiBySiDateRange(java.util.Date SI_DT_FRM, java.util.Date SI_DT_TO, java.lang.Integer SI_AD_CMPNY)"
 *             query="SELECT OBJECT(si) FROM InvStockIssuance si  WHERE si.siPosted = 0 AND si.siVoid = 0 AND si.siDate >= ?1 AND si.siDate <= ?2 AND si.siAdCompany = ?3 ORDER BY si.siDate"
 * 
 * @ejb:value-object match="*"
 *             name="InvStockIssuance"
 *
 * @jboss:persistence table-name="INV_STCK_ISSNC"
 *
 * @jboss:entity-command name="mysql-get-generated-keys"
 *
 */

public abstract class InvStockIssuanceBean extends AbstractEntityBean {
	
//	 Access methods for persistent fields

	   /**
	    * @ejb:persistent-field
	    * @ejb:interface-method view-type="local"
	    * @ejb:pk-field
	    *
	    * @jboss:column-name name="SI_CODE"
	    *
	    * @jboss:persistence auto-increment="true"
	    **/
	   public abstract java.lang.Integer getSiCode();
	   public abstract void setSiCode(java.lang.Integer SI_CODE);
	   
	   /**
	    * @ejb:persistent-field
	    * @ejb:interface-method view-type="local"
	    *
	    * @jboss:column-name name="SI_DT"
	    **/
	   public abstract java.util.Date getSiDate();
	   /**
	    * @ejb:interface-method view-type="local"
	    **/
	   public abstract void setSiDate(java.util.Date SI_DT);
	   
	   /**
	     * @ejb:persistent-field
	     * @ejb:interface-method view-type="local"
	     *
	     * @jboss:column-name name="SI_DCMNT_NMBR"
	     **/
      public abstract String getSiDocumentNumber();
      /**
       * @ejb:interface-method view-type="local"
       **/
      public abstract void setSiDocumentNumber(String SI_DCMNT_NMBR); 

	   /**
	    * @ejb:persistent-field
	    * @ejb:interface-method view-type="local"
	    *
	    * @jboss:column-name name="SI_RFRNC_NMBR"
	    **/
	   public abstract java.lang.String getSiReferenceNumber();
	   /**
	    * @ejb:interface-method view-type="local"
	    **/
	   public abstract void setSiReferenceNumber(java.lang.String SI_RFRNC_NMBR);
	   
	   /**
	    * @ejb:persistent-field
	    * @ejb:interface-method view-type="local"
	    *
	    * @jboss:column-name name="SI_DESC"
	    **/
	   public abstract java.lang.String getSiDescription();
	   /**
	    * @ejb:interface-method view-type="local"
	    **/
	   public abstract void setSiDescription(java.lang.String SI_DESC);	   
	   
	   /**
	    * @ejb:persistent-field
	    * @ejb:interface-method view-type="local"
	    *
	    * @jboss:column-name name="SI_VOID"
	    **/
	   public abstract byte getSiVoid();
	   /**
	    * @ejb:interface-method view-type="local"
	    **/
	   public abstract void setSiVoid(byte SI_VOID);
	   
	   /**
	    * @ejb:persistent-field
	    * @ejb:interface-method view-type="local"
	    *
	    * @jboss:column-name name="SI_APPRVL_STATUS"
	    **/
	   public abstract java.lang.String getSiApprovalStatus();
	   /**
	    * @ejb:interface-method view-type="local"
	    **/
	   public abstract void setSiApprovalStatus(java.lang.String SI_APPRVL_STATUS);
	   
	   /**
	    * @ejb:persistent-field
	    * @ejb:interface-method view-type="local"
	    *
	    * @jboss:column-name name="SI_PSTD"
	    **/
	   public abstract byte getSiPosted();
	   /**
	    * @ejb:interface-method view-type="local"
	    **/
	   public abstract void setSiPosted(byte SI_PSTD);
	   
	   /**
	    * @ejb:persistent-field
	    * @ejb:interface-method view-type="local"
	    *
	    * @jboss:column-name name="SI_CRTD_BY"
	    **/
	   public abstract String getSiCreatedBy();
	   /**
	    * @ejb:interface-method view-type="local"
	    **/
	   public abstract void setSiCreatedBy(String SI_CRTD_BY);
	   
	   /**
	    * @ejb:persistent-field
	    * @ejb:interface-method view-type="local"
	    *
	    * @jboss:column-name name="SI_DT_CRTD"
	    **/
	   public abstract java.util.Date getSiDateCreated();
	   /**
	    * @ejb:interface-method view-type="local"
	    **/
	   public abstract void setSiDateCreated(java.util.Date SI_DT_CRTD);
	   
	   /**
	    * @ejb:persistent-field
	    * @ejb:interface-method view-type="local"
	    *
	    * @jboss:column-name name="SI_LST_MDFD_BY"
	    **/
	   public abstract String getSiLastModifiedBy();
	   /**
	    * @ejb:interface-method view-type="local"
	    **/
	   public abstract void setSiLastModifiedBy(String SI_LST_MDFD_BY);
	   
	   /**
	    * @ejb:persistent-field
	    * @ejb:interface-method view-type="local"
	    *
	    * @jboss:column-name name="SI_DT_LST_MDFD"
	    **/
	   public abstract java.util.Date getSiDateLastModified();
	   /**
	    * @ejb:interface-method view-type="local"
	    **/
	   public abstract void setSiDateLastModified(java.util.Date SI_DT_LST_MDFD);
	   
	   /**
	    * @ejb:persistent-field
	    * @ejb:interface-method view-type="local"
	    *
	    * @jboss:column-name name="SI_APPRVD_RJCTD_BY"
	    **/
	   public abstract String getSiApprovedRejectedBy();
	   /**
	    * @ejb:interface-method view-type="local"
	    **/
	   public abstract void setSiApprovedRejectedBy(String SI_APPRVD_RJCTD_BY);
	   
	   /**
	    * @ejb:persistent-field
	    * @ejb:interface-method view-type="local"
	    *
	    * @jboss:column-name name="SI_DT_APPRVD_RJCTD"
	    **/
	   public abstract java.util.Date getSiDateApprovedRejected();
	   /**
	    * @ejb:interface-method view-type="local"
	    **/
	   public abstract void setSiDateApprovedRejected(java.util.Date SI_DT_APPRVD_RJCTD);
	   
	   /**
	    * @ejb:persistent-field
	    * @ejb:interface-method view-type="local"
	    *
	    * @jboss:column-name name="SI_PSTD_BY"
	    **/
	   public abstract String getSiPostedBy();
	   /**
	    * @ejb:interface-method view-type="local"
	    **/
	   public abstract void setSiPostedBy(String SI_PSTD_BY);
	   
	   /**
	    * @ejb:persistent-field
	    * @ejb:interface-method view-type="local"
	    *
	    * @jboss:column-name name="SI_DT_PSTD"
	    **/
	   public abstract java.util.Date getSiDatePosted();
	   /**
	    * @ejb:interface-method view-type="local"
	    **/
	   public abstract void setSiDatePosted(java.util.Date SI_DT_PSTD);
	   
	   /**
	     * @ejb:persistent-field
	     * @ejb:interface-method view-type="local"
	     *
	     * @jboss:column-name name="SI_RSN_FR_RJCTN"
	     **/
      public abstract String getSiReasonForRejection();
      /**
       * @ejb:interface-method view-type="local"
       **/
      public abstract void setSiReasonForRejection(String SI_RSN_FR_RJCTN);  
	   
      /**
       * @ejb:persistent-field
       * @ejb:interface-method view-type="local"
       *
       * @jboss:column-name name="SI_AD_BRNCH"
       **/
      public abstract Integer getSiAdBranch();
      /**
       * @ejb:interface-method view-type="local"
       **/
      public abstract void setSiAdBranch(Integer SI_AD_BRNCH); 
      
      /**
       * @ejb:persistent-field
       * @ejb:interface-method view-type="local"
       *
       * @jboss:column-name name="SI_AD_CMPNY"
       **/
      public abstract Integer getSiAdCompany();
      /**
       * @ejb:interface-method view-type="local"
       **/
      public abstract void setSiAdCompany(Integer SI_AD_CMPNY); 
	   
	 
	   // Access methods for relationship fields
	   
	   /**
	    * @ejb:interface-method view-type="local"
	    * @ejb:relation name="stockissuance-stockissuancelines"
	    *               role-name="stockissuance-has-many-stockissuancelines"
	    */
	   public abstract Collection getInvStockIssuanceLines();
	   public abstract void setInvStockIssuanceLines(Collection invStockIssuanceLines);
	   
	   /**
	    * @ejb:interface-method view-type="local"
	    * @ejb:relation name="stockissuance-invdistributionrecords"
	    *               role-name="stockissuance-has-many-invdistributionrecords"
	    */
	   public abstract Collection getInvDistributionRecords();
	   public abstract void setInvDistributionRecords(Collection invDistributionRecords);

	
	   /**
		 * @jboss:dynamic-ql
		 */
		public abstract Collection ejbSelectGeneric(java.lang.String jbossQl, java.lang.Object[] args)
		throws FinderException;
		
		// BUSINESS METHODS
		
		/**
		 * @ejb:home-method view-type="local"
		 */
		public Collection ejbHomeGetSiByCriteria(java.lang.String jbossQl, java.lang.Object[] args)
		throws FinderException {
			
			return ejbSelectGeneric(jbossQl, args);
			
		}
		
	   
	   /**
	    * @ejb:interface-method view-type="local"
	    **/
	   public void addInvStockIssuanceLine(LocalInvStockIssuanceLine invStockIssunaceLine) {

	      Debug.print("InvStockIssuanceBean addInvStockIssuanceLine");
	      try {
	         Collection invStockIssuanceLines = getInvStockIssuanceLines();
	         invStockIssuanceLines.add(invStockIssunaceLine);
	      } catch (Exception ex) {
	         throw new EJBException(ex.getMessage());
	      }
	   }

	   /**
	    * @ejb:interface-method view-type="local"
	    **/
	   public void dropInvStockIssuanceLine(LocalInvStockIssuanceLine invStockIssunaceLine) {
	   
	      Debug.print("InvStockIssuanceBean dropInvStockIssuanceLine");
	      try {
	         Collection invStockIssuanceLines = getInvStockIssuanceLines();
	         invStockIssuanceLines.remove(invStockIssunaceLine);
	      } catch (Exception ex) {
	         throw new EJBException(ex.getMessage());
	      }
	   }
	   
	   
	   /**
	    * @ejb:interface-method view-type="local"
	    **/
	   public void addInvDistributionRecord(LocalInvDistributionRecord invDistributionRecord) {

	      Debug.print("InvStockIssuanceBean addInvDistributionRecord");
	      try {
	         Collection invDistributionRecords = getInvDistributionRecords();
	         invDistributionRecords.add(invDistributionRecord);
	      } catch (Exception ex) {
	         throw new EJBException(ex.getMessage());
	      }
	   }

	   /**
	    * @ejb:interface-method view-type="local"
	    **/
	   public void dropInvDistributionRecord(LocalInvDistributionRecord invDistributionRecord) {
	   
	      Debug.print("InvStockIssuanceBean dropInvDistributionRecord");
	      try {
	         Collection invDistributionRecords = getInvDistributionRecords();
	         invDistributionRecords.remove(invDistributionRecord);
	      } catch (Exception ex) {
	         throw new EJBException(ex.getMessage());
	      }
	   }
	   
	   /**
	    * @ejb:interface-method view-type="local"
	    **/
	   public short getInvDrNextLine() {

	       Debug.print("InvStockIssuanceBean getInvDrNextLine");
	       try {
	          Collection arDistributionRecords = getInvDistributionRecords();
		      return (short)(arDistributionRecords.size() + 1);		    
	       } catch (Exception ex) {	      	
	          throw new EJBException(ex.getMessage());	         
	       }
	    }  

	   // EntityBean methods

	   /**
	    * @ejb:create-method view-type="local"
	    **/
	   public java.lang.Integer ejbCreate (java.lang.Integer SI_CODE, java.util.Date SI_DT, java.lang.String SI_DCMNT_NMBR,
	   		  java.lang.String SI_RFRNC_NMBR, String SI_DESC, byte SI_VOID, java.lang.String SI_APPRVL_STATUS,
			  byte SI_PSTD, java.lang.String SI_CRTD_BY, java.util.Date SI_DT_CRTD, 
			  java.lang.String SI_LST_MDFD_BY, java.util.Date SI_DT_LST_MDFD, 
			  java.lang.String SI_APPRVD_RJCTD_BY, java.util.Date SI_DT_APPRVD_RJCTD, 
			  java.lang.String SI_PSTD_BY, java.util.Date SI_DT_PSTD, String SI_RSN_FR_RJCTN, 
			  Integer SI_AD_BRNCH, Integer SI_AD_CMPNY)
	      throws CreateException {

	      Debug.print("invStockIssuanceBean ejbCreate");
	      
	      setSiCode(SI_CODE);
	      setSiDate(SI_DT);
	      setSiDocumentNumber(SI_DCMNT_NMBR);
	      setSiReferenceNumber(SI_RFRNC_NMBR);
	      setSiDescription(SI_DESC);
	      setSiVoid(SI_VOID);
	      setSiApprovalStatus(SI_APPRVL_STATUS);
	      setSiPosted(SI_PSTD);
	      setSiCreatedBy(SI_CRTD_BY);
	      setSiDateCreated(SI_DT_CRTD);
	      setSiLastModifiedBy(SI_LST_MDFD_BY);
	      setSiDateLastModified(SI_DT_LST_MDFD);
	      setSiApprovedRejectedBy(SI_APPRVD_RJCTD_BY);
	      setSiDateApprovedRejected(SI_DT_APPRVD_RJCTD);
	      setSiPostedBy(SI_PSTD_BY);
	      setSiDatePosted(SI_DT_PSTD);
	      setSiReasonForRejection(SI_RSN_FR_RJCTN);
	      setSiAdBranch(SI_AD_BRNCH);
	      setSiAdCompany(SI_AD_CMPNY);
	      
	      return null;
	   }

	   /**
	    * @ejb:create-method view-type="local"
	    **/
	   public java.lang.Integer ejbCreate (java.util.Date SI_DT, java.lang.String SI_DCMNT_NMBR,
	   		  java.lang.String SI_RFRNC_NMBR, String SI_DESC, byte SI_VOID, java.lang.String SI_APPRVL_STATUS,
			  byte SI_PSTD, java.lang.String SI_CRTD_BY, java.util.Date SI_DT_CRTD, 
			  java.lang.String SI_LST_MDFD_BY, java.util.Date SI_DT_LST_MDFD, 
			  java.lang.String SI_APPRVD_RJCTD_BY, java.util.Date SI_DT_APPRVD_RJCTD, 
			  java.lang.String SI_PSTD_BY, java.util.Date SI_DT_PSTD, String SI_RSN_FR_RJCTN, 
			  Integer SI_AD_BRNCH, Integer SI_AD_CMPNY)
	      throws CreateException {

	      Debug.print("invStockIssuanceBean ejbCreate");
	      
	      setSiDate(SI_DT);
	      setSiDocumentNumber(SI_DCMNT_NMBR);
	      setSiReferenceNumber(SI_RFRNC_NMBR);
	      setSiDescription(SI_DESC);
	      setSiVoid(SI_VOID);
	      setSiApprovalStatus(SI_APPRVL_STATUS);
	      setSiPosted(SI_PSTD);
	      setSiCreatedBy(SI_CRTD_BY);
	      setSiDateCreated(SI_DT_CRTD);
	      setSiLastModifiedBy(SI_LST_MDFD_BY);
	      setSiDateLastModified(SI_DT_LST_MDFD);
	      setSiApprovedRejectedBy(SI_APPRVD_RJCTD_BY);
	      setSiDateApprovedRejected(SI_DT_APPRVD_RJCTD);
	      setSiPostedBy(SI_PSTD_BY);
	      setSiDatePosted(SI_DT_PSTD);
	      setSiReasonForRejection(SI_RSN_FR_RJCTN);
	      setSiAdBranch(SI_AD_BRNCH);
	      setSiAdCompany(SI_AD_CMPNY);
	     
	      return null;
	   }

	   public void ejbPostCreate (java.lang.Integer SI_CODE, java.util.Date SI_DT, java.lang.String SI_DCMNT_NMBR,
	   		  java.lang.String SI_RFRNC_NMBR, String SI_DESC, byte SI_VOID, java.lang.String SI_APPRVL_STATUS,
			  byte SI_PSTD, java.lang.String SI_CRTD_BY, java.util.Date SI_DT_CRTD, 
			  java.lang.String SI_LST_MDFD_BY, java.util.Date SI_DT_LST_MDFD, 
			  java.lang.String SI_APPRVD_RJCTD_BY, java.util.Date SI_DT_APPRVD_RJCTD, 
			  java.lang.String SI_PSTD_BY, java.util.Date SI_DT_PSTD, String SI_RSN_FR_RJCTN, 
			  Integer SI_AD_BRNCH, Integer SI_AD_CMPNY)
	      throws CreateException { }

	   public void ejbPostCreate (java.util.Date SI_DT, java.lang.String SI_DCMNT_NMBR,
	   		  java.lang.String SI_RFRNC_NMBR, String SI_DESC, byte SI_VOID, java.lang.String SI_APPRVL_STATUS,
			  byte SI_PSTD, java.lang.String SI_CRTD_BY, java.util.Date SI_DT_CRTD, 
			  java.lang.String SI_LST_MDFD_BY, java.util.Date SI_DT_LST_MDFD, 
			  java.lang.String SI_APPRVD_RJCTD_BY, java.util.Date SI_DT_APPRVD_RJCTD, 
			  java.lang.String SI_PSTD_BY, java.util.Date SI_DT_PSTD, String SI_RSN_FR_RJCTN, 
			  Integer SI_AD_BRNCH, Integer SI_AD_CMPNY)
	      throws CreateException { }
	
}
