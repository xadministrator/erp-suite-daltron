package com.ejb.inv;


import javax.ejb.CreateException;

import com.util.AbstractEntityBean;
import com.util.Debug;

   /**
* @ejb:bean name="InvStockIssuanceLineEJB"
*           display-name="Stock Issuance Line Entity"
*           type="CMP"
*           cmp-version="2.x"
*           view-type="local"
*           local-jndi-name="omega-ejb/InvStockIssuanceLineEJB"
*           schema="InvStockIssuanceLine"
*           primkey-field="silCode"
*       
* @ejb:pk class="java.lang.Integer"
*
* @ejb:transaction type="Required"
*
* @ejb:security-role-ref role-name="invuser"
*                        role-link="invuserlink"
*
* @ejb:permission role-name="invuser"
*
* @ejb:interface local-class="com.ejb.inv.LocalInvStockIssuanceLine"
*                local-extends="javax.ejb.EJBLocalObject"
*
* @ejb:home local-class="com.ejb.inv.LocalInvStockIssuanceLineHome"
*           local-extends="javax.ejb.EJBLocalHome"
* 
* @ejb:finder signature = "Collection findBySiPostedAndIlCodeAndBrCode(byte SI_PSTD, java.lang.Integer IL_CODE, java.lang.Integer SI_AD_BRNCH, java.lang.Integer SIL_AD_CMPNY)"
* 				query = "SELECT OBJECT(sil) FROM InvStockIssuanceLine sil WHERE sil.invStockIssuance.siPosted=?1 AND sil.invItemLocation.ilCode=?2 AND sil.invStockIssuance.siAdBranch=?3 AND sil.silAdCompany=?4"  
*
* @ejb:finder signature="Collection findUnpostedSiByLocNameAndAdBranch(java.lang.String LOC_NM, java.lang.Integer SI_AD_BRNCH, java.lang.Integer SIL_AD_CMPNY)"
* 			   query="SELECT OBJECT(sil) FROM InvStockIssuanceLine sil WHERE sil.invStockIssuance.siPosted = 0 AND sil.invStockIssuance.siVoid = 0 AND sil.invItemLocation.invLocation.locName = ?1 AND sil.invStockIssuance.siAdBranch = ?2 AND sil.silAdCompany = ?3"
* 
** @ejb:finder signature="Collection findByIiCodeAndLocNameAndAdBranch(java.lang.Integer II_CODE, java.lang.String LOC_NM, java.lang.Integer SI_AD_BRNCH, java.lang.Integer SIL_AD_CMPNY)"
* 			   query="SELECT OBJECT(sil) FROM InvStockIssuanceLine sil WHERE sil.invStockIssuance.siVoid = 0 AND sil.invItemLocation.invItem.iiCode=?1 AND sil.invItemLocation.invLocation.locName = ?2 AND sil.invStockIssuance.siAdBranch = ?3 AND sil.silAdCompany = ?4"
*
* @ejb:value-object match="*"
*             name="InvStockIssuanceLine"
*
* @jboss:persistence table-name="INV_STCK_ISSNC_LN"
*
* @jboss:entity-command name="mysql-get-generated-keys"
*
*/

public abstract class InvStockIssuanceLineBean extends AbstractEntityBean {
	
//	 Access methods for persistent fields

	   /**
	    * @ejb:persistent-field
	    * @ejb:interface-method view-type="local"
	    * @ejb:pk-field
	    *
	    * @jboss:column-name name="SIL_CODE"
	    *
	    * @jboss:persistence auto-increment="true"
	    **/
	   public abstract java.lang.Integer getSilCode();
	   public abstract void setSilCode(java.lang.Integer SIL_CODE);
	   
	   /**
	    * @ejb:persistent-field
	    * @ejb:interface-method view-type="local"
	    *
	    * @jboss:column-name name="SIL_ISSUE_QTY"
	    **/
	   public abstract double getSilIssueQuantity();
	   /**
	    * @ejb:interface-method view-type="local"
	    **/
	   public abstract void setSilIssueQuantity(double SIL_ISSUE_QTY);
	   
	   /**
	    * @ejb:persistent-field
	    * @ejb:interface-method view-type="local"
	    *
	    * @jboss:column-name name="SIL_ISSUE_COST"
	    *
	    **/
	   public abstract double getSilIssueCost();
	   /**
	    * @ejb:interface-method view-type="local"
	    **/
	   public abstract void setSilIssueCost(double SIL_ISSUE_COST);
	   
	   /**
	    * @ejb:persistent-field
	    * @ejb:interface-method view-type="local"
	    *
	    * @jboss:column-name name="SIL_ASSMBLY_COST"
	    *
	    **/
	   public abstract double getSilAssemblyCost();
	   /**
	    * @ejb:interface-method view-type="local"
	    **/
	   public abstract void setSilAssemblyCost(double SIL_ASSMBLY_COST);

	   /**
	    * @ejb:persistent-field
	    * @ejb:interface-method view-type="local"
	    *
	    * @jboss:column-name name="SIL_UNT_COST"
	    *
	    **/
	   public abstract double getSilUnitCost();
	   /**
	    * @ejb:interface-method view-type="local"
	    **/
	   public abstract void setSilUnitCost(double SIL_UNT_COST);

	   /**
	    * @ejb:persistent-field
	    * @ejb:interface-method view-type="local"
	    *
	    * @jboss:column-name name="SIL_AD_CMPNY"
	    **/
	   public abstract java.lang.Integer getSilAdCompany();
	   /**
	    * @ejb:interface-method view-type="local"
	    **/
	   public abstract void setSilAdCompany(java.lang.Integer SIL_AD_CMPNY);
	   
	   // Access methods for relationship fields
	
	   /**
	    * @ejb:interface-method view-type="local"
	    * @ejb:relation name="stockissuance-stockissuancelines"
	    *               role-name="stockissuanceline-has-one-stockissuance"
	    *               cascade-delete="yes"
	    *
	    * @jboss:relation related-pk-field="siCode"
	    *                 fk-column="INV_STOCK_ISSUANCE"
	    */
	   public abstract LocalInvStockIssuance getInvStockIssuance();
	   public abstract void setInvStockIssuance(LocalInvStockIssuance invStockIssuance);
	   
	   /**
	    * @ejb:interface-method view-type="local"
	    * @ejb:relation name="buildorderstock-stockissuancelines"
	    *               role-name="stockissuanceline-has-one-buildorderstock"
	    *               cascade-delete="no"
	    *
	    * @jboss:relation related-pk-field="bosCode"
	    *                 fk-column="INV_BUILD_ORDER_STOCK"
	    */
	   public abstract LocalInvBuildOrderStock getInvBuildOrderStock();
	   public abstract void setInvBuildOrderStock(LocalInvBuildOrderStock invBuildOrderStock);
	   
	   /**
	    * @ejb:interface-method view-type="local"
	    * @ejb:relation name="stockissuanceline-costing"
	    *               role-name="stockissuanceline-has-one-costing"            
	    */
	   public abstract LocalInvCosting getInvCosting();
	   public abstract void setInvCosting(LocalInvCosting invCosting);
	   
	   /**
	    * @ejb:interface-method view-type="local"
	    * @ejb:relation name="itemlocation-stockissuancelines"
	    *               role-name="stockissuanceline-has-one-itemlocation"
	    *               cascade-delete="no"
	    *
	    * @jboss:relation related-pk-field="ilCode"
	    *                 fk-column="INV_ITEM_LOCATION"
	    */
	   public abstract LocalInvItemLocation getInvItemLocation();
	   public abstract void setInvItemLocation(LocalInvItemLocation invItemLocation);
	   
	   /**
	    * @ejb:interface-method view-type="local"
	    * @ejb:relation name="unitofmeasure-stockissuancelines"
	    *               role-name="stockissuancelines-has-one-unitofmeasure"
	    * 
	    *  @jboss:relation related-pk-field="uomCode"
        *                 fk-column="INV_UNIT_OF_MEASURE"
	    */
	   public abstract LocalInvUnitOfMeasure getInvUnitOfMeasure();
	   public abstract void setInvUnitOfMeasure(LocalInvUnitOfMeasure invUnitOfMeasure);  
	   
	   
	   
	   // Business methods	   
	   	   
	   // EntityBean methods
	
	   /**
	    * @ejb:create-method view-type="local"
	    **/
	   public java.lang.Integer ejbCreate (java.lang.Integer SIL_CODE,double SIL_ISSUE_QTY, double SIL_ISSUE_COST, 
	   	  double SIL_ASSMBLY_COST, double SIL_UNT_COST, Integer SIL_AD_CMPNY)
	      throws CreateException {
	
	      Debug.print("invStockIssuanceLineBean ejbCreate");
	      
	      setSilCode(SIL_CODE);
	      setSilIssueQuantity(SIL_ISSUE_QTY);
	      setSilIssueCost(SIL_ISSUE_COST);
	      setSilAssemblyCost(SIL_ASSMBLY_COST);
	      setSilUnitCost(SIL_UNT_COST);
	      setSilAdCompany(SIL_AD_CMPNY);
	
	      return null;
	   }
	
	   /**
	    * @ejb:create-method view-type="local"
	    **/
	   public java.lang.Integer ejbCreate (double SIL_ISSUE_QTY, double SIL_ISSUE_COST,
	      double SIL_ASSMBLY_COST, double SIL_UNT_COST, Integer SIL_AD_CMPNY)
	   throws CreateException {
	       
	       Debug.print("invStockIssuanceLineBean ejbCreate");
	       
	       setSilIssueQuantity(SIL_ISSUE_QTY);
	       setSilIssueCost(SIL_ISSUE_COST);
	       setSilAssemblyCost(SIL_ASSMBLY_COST);
	       setSilUnitCost(SIL_UNT_COST);
	       setSilAdCompany(SIL_AD_CMPNY);
	       
	       return null;
	   }
	
	   public void ejbPostCreate (java.lang.Integer SIL_CODE,double SIL_ISSUE_QTY, double SIL_ISSUE_COST, 
	   	  double SIL_ASSMBLY_COST, double SIL_UNT_COST, Integer SIL_AD_CMPNY)
	      throws CreateException { }
	
	   public void ejbPostCreate (double SIL_ISSUE_QTY, double SIL_ISSUE_COST, 
	   	  double SIL_ASSMBLY_COST, double SIL_UNT_COST, Integer SIL_AD_CMPNY)
	      throws CreateException { }  
	   	
	  
}
