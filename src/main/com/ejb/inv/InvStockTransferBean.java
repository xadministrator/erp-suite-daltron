package com.ejb.inv;

import java.util.Collection;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;

import com.util.AbstractEntityBean;
import com.util.Debug;

/**
 * @ejb:bean name="InvStockTransferEJB"
 *           display-name="Stock Transfer Entity"
 *           type="CMP"
 *           cmp-version="2.x"
 *           view-type="local"
 *           local-jndi-name="omega-ejb/InvStockTransferEJB"
 *           schema="InvStockTransfer"
 *           primkey-field="stCode"
 *       
 * @ejb:pk class="java.lang.Integer"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="invuser"
 *                        role-link="invuserlink"
 *
 * @ejb:permission role-name="invuser"
 *
 * @ejb:interface local-class="com.ejb.inv.LocalInvStockTransfer"
 *                local-extends="javax.ejb.EJBLocalObject"
 *
 * @ejb:home local-class="com.ejb.inv.LocalInvStockTransferHome"
 *           local-extends="javax.ejb.EJBLocalHome" 
 * 
 * @ejb:finder signature="LocalInvStockTransfer findByStDocumentNumberAndBrCode(java.lang.String ST_DCMNT_NMBR, java.lang.Integer ST_AD_BRNCH, java.lang.Integer ST_AD_CMPNY)"
 *           query="SELECT OBJECT(st) FROM InvStockTransfer st  WHERE st.stDocumentNumber = ?1 AND st.stAdBranch = ?2 AND st.stAdCompany = ?3"
 * 
 * @jboss:query signature="Collection ejbSelectGeneric(java.lang.String jbossQl, java.lang.Object[] args)"
 *                         dynamic="true" 
 * 
 * @ejb:finder signature="Collection findUnpostedStByStDateRange(java.util.Date ST_DT_FRM, java.util.Date ST_DT_TO, java.lang.Integer ST_AD_CMPNY)"
 *             query="SELECT OBJECT(st) FROM InvStockTransfer st  WHERE st.stPosted = 0 AND st.stDate >= ?1 AND st.stDate <= ?2 AND st.stAdCompany = ?3"
 *
 * @jboss:query signature="Collection findUnpostedStByStDateRange(java.util.Date ST_DT_FRM, java.util.Date ST_DT_TO, java.lang.Integer ST_AD_CMPNY)"
 *             query="SELECT OBJECT(st) FROM InvStockTransfer st  WHERE st.stPosted = 0 AND st.stDate >= ?1 AND st.stDate <= ?2 AND st.stAdCompany = ?3 ORDER BY st.stDate"
 * 
 * @ejb:finder signature="Collection findUnpostedSt(java.lang.Integer ST_AD_CMPNY)"
 *             query="SELECT OBJECT(st) FROM InvStockTransfer st  WHERE st.stPosted = 0 AND st.stAdCompany = ?1"
 *
 * @jboss:query signature="Collection findUnpostedSt(java.lang.Integer ST_AD_CMPNY)"
 *             query="SELECT OBJECT(st) FROM InvStockTransfer st  WHERE st.stPosted = 0 AND st.stAdCompany = ?1 ORDER BY st.stDate"
 * 
 * @ejb:value-object match="*"
 *             name="InvStockTransfer"
 *
 * @jboss:persistence table-name="INV_STCK_TRNSFR"
 *
 * @jboss:entity-command name="mysql-get-generated-keys"
 *
 */

public abstract class InvStockTransferBean extends AbstractEntityBean {
    
    	//	 Access methods for persistent fields
    
	   /**
		* @ejb:persistent-field
		* @ejb:interface-method view-type="local"
		* @ejb:pk-field
		*
		* @jboss:column-name name="ST_CODE"
		*
		* @jboss:persistence auto-increment="true"
		**/
	   public abstract java.lang.Integer getStCode();
	   public abstract void setStCode(java.lang.Integer ST_CODE);
	   
	   /**
	    * @ejb:persistent-field
	    * @ejb:interface-method view-type="local"
	    *
	    * @jboss:column-name name="ST_DCMNT_NMBR"
	    **/
	   public abstract java.lang.String getStDocumentNumber();
	   /**
	    * @ejb:interface-method view-type="local"
	    **/
	   public abstract void setStDocumentNumber(java.lang.String ST_DCMNT_NMBR);
	   
	   /**
	    * @ejb:persistent-field
	    * @ejb:interface-method view-type="local"
	    *
	    * @jboss:column-name name="ST_RFRNC_NMBR"
	    **/
	   public abstract java.lang.String getStReferenceNumber();
	   /**
	    * @ejb:interface-method view-type="local"
	    **/
	   public abstract void setStReferenceNumber(java.lang.String ST_RFRNC_NMBR);
	   
	   /**
	    * @ejb:persistent-field
	    * @ejb:interface-method view-type="local"
	    *
	    * @jboss:column-name name="ST_DESC"
	    **/
	   public abstract java.lang.String getStDescription();
	   /**
	    * @ejb:interface-method view-type="local"
	    **/
	   public abstract void setStDescription(java.lang.String ST_DESC);
	   
	   /**
	    * @ejb:persistent-field
	    * @ejb:interface-method view-type="local"
	    *
	    * @jboss:column-name name="ST_DT"
	    **/
	   public abstract java.util.Date getStDate();
	   /**
	    * @ejb:interface-method view-type="local"
	    **/
	   public abstract void setStDate(java.util.Date ST_DT);
	   
	   /**
	    * @ejb:persistent-field
	    * @ejb:interface-method view-type="local"
	    *
	    * @jboss:column-name name="ST_APPRVL_STATUS"
	    **/
	   public abstract java.lang.String getStApprovalStatus();
	   /**
	    * @ejb:interface-method view-type="local"
	    **/
	   public abstract void setStApprovalStatus(java.lang.String ST_APPRVL_STATUS);
	   
	   /**
	    * @ejb:persistent-field
	    * @ejb:interface-method view-type="local"
	    *
	    * @jboss:column-name name="ST_PSTD"
	    **/
	   public abstract byte getStPosted();
	   /**
	    * @ejb:interface-method view-type="local"
	    **/
	   public abstract void setStPosted(byte ST_PSTD);
	   
	   /**
	    * @ejb:persistent-field
	    * @ejb:interface-method view-type="local"
	    *
	    * @jboss:column-name name="ST_CRTD_BY"
	    **/
	   public abstract java.lang.String getStCreatedBy();
	   /**
	    * @ejb:interface-method view-type="local"
	    **/
	   public abstract void setStCreatedBy(java.lang.String ST_CRTD_BY);
	   
	   /**
	    * @ejb:persistent-field
	    * @ejb:interface-method view-type="local"
	    *
	    * @jboss:column-name name="ST_DT_CRTD"
	    **/
	   public abstract java.util.Date getStDateCreated();
	   /**
	    * @ejb:interface-method view-type="local"
	    **/
	   public abstract void setStDateCreated(java.util.Date ST_DT_CRTD);
	   
	   /**
	    * @ejb:persistent-field
	    * @ejb:interface-method view-type="local"
	    *
	    * @jboss:column-name name="ST_LST_MDFD_BY"
	    **/
	   public abstract java.lang.String getStLastModifiedBy();
	   /**
	    * @ejb:interface-method view-type="local"
	    **/
	   public abstract void setStLastModifiedBy(java.lang.String ST_LST_MDFD_BY);
	   
	   /**
	    * @ejb:persistent-field
	    * @ejb:interface-method view-type="local"
	    *
	    * @jboss:column-name name="ST_DT_LST_MDFD"
	    **/
	   public abstract java.util.Date getStDateLastModified();
	   /**
	    * @ejb:interface-method view-type="local"
	    **/
	   public abstract void setStDateLastModified(java.util.Date ST_DT_LST_MDFD);
	   
	   /**
	    * @ejb:persistent-field
	    * @ejb:interface-method view-type="local"
	    *
	    * @jboss:column-name name="ST_APPRVD_RJCTD_BY"
	    **/
	   public abstract java.lang.String getStApprovedRejectedBy();
	   /**
	    * @ejb:interface-method view-type="local"
	    **/
	   public abstract void setStApprovedRejectedBy(java.lang.String ST_APPRVD_RJCTD_BY);
	   
	   /**
	    * @ejb:persistent-field
	    * @ejb:interface-method view-type="local"
	    *
	    * @jboss:column-name name="ST_DT_APPRVD_RJCTD"
	    **/
	   public abstract java.util.Date getStDateApprovedRejected();
	   /**
	    * @ejb:interface-method view-type="local"
	    **/
	   public abstract void setStDateApprovedRejected(java.util.Date ST_DT_APPRVD_RJCTD);
	   
	   /**
	    * @ejb:persistent-field
	    * @ejb:interface-method view-type="local"
	    *
	    * @jboss:column-name name="ST_PSTD_BY"
	    **/
	   public abstract java.lang.String getStPostedBy();
	   /**
	    * @ejb:interface-method view-type="local"
	    **/
	   public abstract void setStPostedBy(java.lang.String ST_PSTD_BY);
	   
	   /**
	    * @ejb:persistent-field
	    * @ejb:interface-method view-type="local"
	    *
	    * @jboss:column-name name="ST_DT_PSTD"
	    **/
	   public abstract java.util.Date getStDatePosted();
	   /**
	    * @ejb:interface-method view-type="local"
	    **/
	   public abstract void setStDatePosted(java.util.Date ST_DT_PSTD);
	   
	   /**
	    * @ejb:persistent-field
	    * @ejb:interface-method view-type="local"
	    *
	    * @jboss:column-name name="ST_RSN_FR_RJCTN"
	    **/
	   public abstract java.lang.String getStReasonForRejection();
	   /**
	    * @ejb:interface-method view-type="local"
	    **/
	   public abstract void setStReasonForRejection(java.lang.String ST_RSN_FR_RJCTN);
	   
	   /**
	    * @ejb:persistent-field
	    * @ejb:interface-method view-type="local"
	    *
	    * @jboss:column-name name="ST_AD_BRNCH"
	    **/
	   public abstract Integer getStAdBranch();
	   /**
	    * @ejb:interface-method view-type="local"
	    **/
	   public abstract void setStAdBranch(Integer ST_AD_BRNCH);
	   
	   /**
	    * @ejb:persistent-field
	    * @ejb:interface-method view-type="local"
	    *
	    * @jboss:column-name name="ST_AD_CMPNY"
	    **/
	   public abstract Integer getStAdCompany();
	   /**
	    * @ejb:interface-method view-type="local"
	    **/
	   public abstract void setStAdCompany(Integer ST_AD_CMPNY);
	   
	   // access methods for relationship fields
	   
	   /**
	    * @ejb:interface-method view-type="local"
	    * @ejb:relation name="stocktransfer-stocktransferlines"
	    *               role-name="stocktransfer-has-many-stocktransferlines"
	    */
	   public abstract Collection getInvStockTransferLines();
	   public abstract void setInvStockTransferLines(Collection invStockTranferLines);
	   
	   /**
	    * @ejb:interface-method view-type="local"
	    * @ejb:relation name="stocktransfer-invdistributionrecords"
	    *               role-name="stocktransfer-has-many-invdistributionrecords"
	    */
	   public abstract Collection getInvDistributionRecords();
	   public abstract void setInvDistributionRecords(Collection invDistributionRecords);
	   
	   /**
		* @jboss:dynamic-ql
		*/
	   public abstract Collection ejbSelectGeneric(java.lang.String jbossQl, java.lang.Object[] args)
	   throws FinderException;
	   
	   //	 BUSINESS METHODS
		
	   /**
		 * @ejb:home-method view-type="local"
		 */
	   public Collection ejbHomeGetStByCriteria(java.lang.String jbossQl, java.lang.Object[] args)
	   throws FinderException {
			
	       return ejbSelectGeneric(jbossQl, args);
			
	   }
	   
	   /**
	    * @ejb:interface-method view-type="local"
	    **/
	   public void addInvStockTransferLine(LocalInvStockTransferLine invStockTransferLine) {

	      Debug.print("InvStockTransferBean addInvStockTransferLine");
	      try {
	         Collection invStockTransferLines = getInvStockTransferLines();
	         invStockTransferLines.add(invStockTransferLine);
	      } catch (Exception ex) {
	         throw new EJBException(ex.getMessage());
	      }
	   }

	   /**
	    * @ejb:interface-method view-type="local"
	    **/
	   public void dropInvStockTransferLine(LocalInvStockTransferLine invStockTransferLine) {
	   
	      Debug.print("InvStockTransferBean dropInvStockTransferLine");
	      try {
	         Collection invStockTransferLines = getInvStockTransferLines();
	         invStockTransferLines.remove(invStockTransferLine);
	      } catch (Exception ex) {
	         throw new EJBException(ex.getMessage());
	      }
	   }
	   
	   /**
	    * @ejb:interface-method view-type="local"
	    **/
	   public void addInvDistributionRecord(LocalInvDistributionRecord invDistributionRecord) {

	      Debug.print("InvStockTransferBean addInvDistributionRecord");
	      try {
	         Collection invDistributionRecords = getInvDistributionRecords();
	         invDistributionRecords.add(invDistributionRecord);
	      } catch (Exception ex) {
	         throw new EJBException(ex.getMessage());
	      }
	   }

	   /**
	    * @ejb:interface-method view-type="local"
	    **/
	   public void dropInvDistributionRecord(LocalInvDistributionRecord invDistributionRecord) {

	      Debug.print("InvStockTransferBean dropInvDistributionRecord");
	      try {
	         Collection invDistributionRecords = getInvDistributionRecords();
	         invDistributionRecords.remove(invDistributionRecord);
	      } catch (Exception ex) {
	         throw new EJBException(ex.getMessage());
	      }
	   }
	   
	   /**
	    * @ejb:interface-method view-type="local"
	    **/
	   public short getInvDrNextLine() {

	       Debug.print("InvStockTransferBean getInvDrNextLine");
	       try {
	          Collection arDistributionRecords = getInvDistributionRecords();
		      return (short)(arDistributionRecords.size() + 1);		    
	       } catch (Exception ex) {	      	
	          throw new EJBException(ex.getMessage());	         
	       }
	    }
	   
	   // EntityBean methods
	   
	   /**
	    * @ejb:create-method view-type="local"
	    **/
	   public java.lang.Integer ejbCreate (java.lang.Integer ST_CODE, java.lang.String ST_DCMNT_NMBR, 
	       java.lang.String ST_RFRNC_NMBR, java.lang.String ST_DESC, java.util.Date ST_DT, 
	       java.lang.String ST_APPRVL_STATUS, byte ST_PSTD, java.lang.String ST_CRTD_BY, java.util.Date ST_DT_CRTD,
	       java.lang.String ST_LST_MDFD_BY, java.util.Date ST_DT_LST_MDFD, java.lang.String ST_APPRVD_RJCTD_BY,
	       java.util.Date ST_DT_APPRVD_RJCTD, java.lang.String ST_PSTD_BY, java.util.Date ST_DT_PSTD, 
	       java.lang.String ST_RSN_FR_RJCTN, Integer ST_AD_BRNCH, Integer ST_AD_CMPNY) 
	   	   throws CreateException {
	       
	       Debug.print("invStockTransferBean ejbCreate");
	       
	       setStCode(ST_CODE);
	       setStDocumentNumber(ST_DCMNT_NMBR);
	       setStReferenceNumber(ST_RFRNC_NMBR);
	       setStDescription(ST_DESC);
	       setStDate(ST_DT);
	       setStApprovalStatus(ST_APPRVL_STATUS);
	       setStPosted(ST_PSTD);
	       setStCreatedBy(ST_CRTD_BY);
	       setStDateCreated(ST_DT_CRTD);
	       setStLastModifiedBy(ST_LST_MDFD_BY);
	       setStDateLastModified(ST_DT_LST_MDFD);
	       setStApprovedRejectedBy(ST_APPRVD_RJCTD_BY);
	       setStDateApprovedRejected(ST_DT_APPRVD_RJCTD);
	       setStPostedBy(ST_PSTD_BY);
	       setStDatePosted(ST_DT_PSTD);
	       setStReasonForRejection(ST_RSN_FR_RJCTN);
	       setStAdBranch(ST_AD_BRNCH);
	       setStAdCompany(ST_AD_CMPNY);
	       
	       return null;
	   }
	   
	   /**
	    * @ejb:create-method view-type="local"
	    **/
	   public java.lang.Integer ejbCreate (java.lang.String ST_DCMNT_NMBR, java.lang.String ST_RFRNC_NMBR, 
	       java.lang.String ST_DESC, java.util.Date ST_DT, java.lang.String ST_APPRVL_STATUS, byte ST_PSTD, 
	       java.lang.String ST_CRTD_BY, java.util.Date ST_DT_CRTD, java.lang.String ST_LST_MDFD_BY, 
	       java.util.Date ST_DT_LST_MDFD, java.lang.String ST_APPRVD_RJCTD_BY, java.util.Date ST_DT_APPRVD_RJCTD, 
	       java.lang.String ST_PSTD_BY, java.util.Date ST_DT_PSTD, java.lang.String ST_RSN_FR_RJCTN, 
	       Integer ST_AD_BRNCH, Integer ST_AD_CMPNY) 
	   	   throws CreateException {
	       
	       Debug.print("invStockTransferBean ejbCreate");
	       	       
	       setStDocumentNumber(ST_DCMNT_NMBR);
	       setStReferenceNumber(ST_RFRNC_NMBR);
	       setStDescription(ST_DESC);
	       setStDate(ST_DT);
	       setStApprovalStatus(ST_APPRVL_STATUS);
	       setStPosted(ST_PSTD);
	       setStCreatedBy(ST_CRTD_BY);
	       setStDateCreated(ST_DT_CRTD);
	       setStLastModifiedBy(ST_LST_MDFD_BY);
	       setStDateLastModified(ST_DT_LST_MDFD);
	       setStApprovedRejectedBy(ST_APPRVD_RJCTD_BY);
	       setStDateApprovedRejected(ST_DT_APPRVD_RJCTD);
	       setStPostedBy(ST_PSTD_BY);
	       setStDatePosted(ST_DT_PSTD);
	       setStReasonForRejection(ST_RSN_FR_RJCTN);
	       setStAdBranch(ST_AD_BRNCH);
	       setStAdCompany(ST_AD_CMPNY);
	       
	       return null;
	   }
	   
	   public void ejbPostCreate (java.lang.Integer ST_CODE, java.lang.String ST_DCMNT_NMBR, 
	       java.lang.String ST_RFRNC_NMBR, java.lang.String ST_DESC, java.util.Date ST_DT, 
	       java.lang.String ST_APPRVL_STATUS, byte ST_PSTD, java.lang.String ST_CRTD_BY, java.util.Date ST_DT_CRTD,
	       java.lang.String ST_LST_MDFD_BY, java.util.Date ST_DT_LST_MDFD, java.lang.String ST_APPRVD_RJCTD_BY,
	       java.util.Date ST_DT_APPRVD_RJCTD, java.lang.String ST_PSTD_BY, java.util.Date ST_DT_PSTD, 
	       java.lang.String ST_RSN_FR_RJCTN, Integer ST_AD_BRNCH, Integer ST_AD_CMPNY) 
	   	   throws CreateException { }
	   
	   public void ejbPostCreate (java.lang.String ST_DCMNT_NMBR, java.lang.String ST_RFRNC_NMBR, 
	       java.lang.String ST_DESC, java.util.Date ST_DT, java.lang.String ST_APPRVL_STATUS, byte ST_PSTD, 
	       java.lang.String ST_CRTD_BY, java.util.Date ST_DT_CRTD, java.lang.String ST_LST_MDFD_BY, 
	       java.util.Date ST_DT_LST_MDFD, java.lang.String ST_APPRVD_RJCTD_BY, java.util.Date ST_DT_APPRVD_RJCTD, 
	       java.lang.String ST_PSTD_BY, java.util.Date ST_DT_PSTD, java.lang.String ST_RSN_FR_RJCTN, 
	       Integer ST_AD_BRNCH, Integer ST_AD_CMPNY) 
	   	   throws CreateException { }
}
