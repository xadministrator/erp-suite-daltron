package com.ejb.inv;

import java.util.Collection;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;

import com.util.AbstractEntityBean;
import com.util.Debug;

/**
 * @ejb:bean name="InvStockTransferLineEJB"
 *           display-name="Stock Transfer Line Entity"
 *           type="CMP"
 *           cmp-version="2.x"
 *           view-type="local"
 *           local-jndi-name="omega-ejb/InvStockTransferLineEJB"
 *           schema="InvStockTransferLine"
 *           primkey-field="stlCode"
 *
 * @ejb:pk class="java.lang.Integer"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="invuser"
 *                        role-link="invuserlink"
 *
 * @ejb:permission role-name="invuser"
 *
 * @ejb:interface local-class="com.ejb.inv.LocalInvStockTransferLine"
 *                local-extends="javax.ejb.EJBLocalObject"
 *
 * @ejb:home local-class="com.ejb.inv.LocalInvStockTransferLineHome"
 *           local-extends="javax.ejb.EJBLocalHome"
 *
 * @ejb:finder signature = "Collection findByStCode(java.lang.Integer ST_CODE, java.lang.Integer STL_AD_CMPNY)"
 * 				query = "SELECT OBJECT(stl) FROM InvStockTransferLine stl WHERE stl.invStockTransfer.stCode=?1 AND stl.stlAdCompany=?2"
 *
 * @jboss:query signature = "Collection findByStCode(java.lang.Integer ST_CODE, java.lang.Integer STL_AD_CMPNY)"
 * 				query = "SELECT OBJECT(stl) FROM InvStockTransferLine stl WHERE stl.invStockTransfer.stCode=?1 AND stl.stlAdCompany=?2 ORDER BY stl.invItem.iiAdLvCategory"
 *
 * @ejb:finder signature = "Collection findByStPostedAndStlLocationFromAndIiCodeAndBrCode(byte ST_PSTD, java.lang.Integer STL_LCTN_FRM, java.lang.Integer II_CODE, java.lang.Integer ST_AD_BRNCH, java.lang.Integer STL_AD_CMPNY)"
 * 				query = "SELECT OBJECT(stl) FROM InvStockTransferLine stl WHERE stl.invStockTransfer.stPosted=?1 AND stl.stlLocationFrom=?2 AND stl.invItem.iiCode=?3 AND stl.invStockTransfer.stAdBranch=?4 AND stl.stlAdCompany=?5"
 *
 * @ejb:finder signature="Collection findUnpostedStByLocCodeAndAdBranch(java.lang.Integer LOC_CODE, java.lang.Integer ST_AD_BRNCH, java.lang.Integer STL_AD_CMPNY)"
 * 			    query = "SELECT OBJECT(stl) FROM InvStockTransferLine stl WHERE stl.invStockTransfer.stPosted = 0 AND stl.stlLocationFrom=?1 AND stl.invStockTransfer.stAdBranch = ?2 AND stl.stlAdCompany = ?3"
 *
 * * @ejb:finder signature="Collection findByIiCodeAndLocCodeAndAdBranch(java.lang.Integer II_CODE, java.lang.Integer LOC_CODE, java.lang.Integer ST_AD_BRNCH, java.lang.Integer STL_AD_CMPNY)"
 * 			    query = "SELECT OBJECT(stl) FROM InvStockTransferLine stl WHERE stl.invItem.iiCode=?1 AND stl.stlLocationFrom=?2 AND stl.invStockTransfer.stAdBranch = ?3 AND stl.stlAdCompany = ?4"
 *
 * @ejb:finder signature="Collection findUnpostedStByLocToCodeAndAdBranch(java.lang.Integer LOC_CODE, java.lang.Integer ST_AD_BRNCH, java.lang.Integer STL_AD_CMPNY)"
 * 			    query = "SELECT OBJECT(stl) FROM InvStockTransferLine stl WHERE stl.invStockTransfer.stPosted = 0 AND stl.stlLocationTo=?1 AND stl.invStockTransfer.stAdBranch = ?2 AND stl.stlAdCompany = ?3"
 *
 *
 * @jboss:query signature="Collection ejbSelectGeneric(java.lang.String jbossQl, java.lang.Object[] args)"
 *                         dynamic="true"
 *
 *
 * @ejb:finder signature="Collection findUnpostedStlByIiNameAndLocCodeAndLessEqualDateAndStAdBranch(java.lang.String II_NM, java.lang.Integer LOC_CODE, java.util.Date ST_DT, java.lang.Integer ST_AD_BRNCH, java.lang.Integer STL_AD_CMPNY)"
 * 			   query="SELECT OBJECT(stl) FROM InvStockTransferLine stl WHERE stl.invStockTransfer.stPosted = 0 AND stl.invItem.iiName = ?1 AND (stl.stlLocationFrom = ?2 OR stl.stlLocationTo = ?2) AND stl.invStockTransfer.stDate <= ?3 AND stl.invStockTransfer.stAdBranch=?4 AND stl.stlAdCompany = ?5"
 *
 *
 * @ejb:value-object match="*"
 *             name="InvStockTransferLine"
 *
 * @jboss:persistence table-name="INV_STCK_TRNSFR_LN"
 *
 * @jboss:entity-command name="mysql-get-generated-keys"
 *
 */

public abstract class InvStockTransferLineBean extends AbstractEntityBean {

    // access methods for persistent fields

        /**
     	 * @ejb:persistent-field
     	 * @ejb:interface-method view-type="local"
     	 * @ejb:pk-field
     	 *
     	 * @jboss:column-name name="STL_CODE"
     	 *
     	 * @jboss:persistence auto-increment="true"
     	 **/
    	public abstract java.lang.Integer getStlCode();
    	public abstract void setStlCode(java.lang.Integer STL_CODE);

        /**
	     * @ejb:persistent-field
	     * @ejb:interface-method view-type="local"
	     *
	     * @jboss:column-name name="STL_LCTN_FRM"
	     **/
    	public abstract java.lang.Integer getStlLocationFrom();
	    /**
	     * @ejb:interface-method view-type="local"
	     **/
	    public abstract void setStlLocationFrom(java.lang.Integer STL_LCTN_FRM);

	    /**
	     * @ejb:persistent-field
	     * @ejb:interface-method view-type="local"
	     *
	     * @jboss:column-name name="STL_LCTN_TO"
	     **/
    	public abstract java.lang.Integer getStlLocationTo();
	    /**
	     * @ejb:interface-method view-type="local"
	     **/
	    public abstract void setStlLocationTo(java.lang.Integer STL_LCTN_TO);

	    /**
	     * @ejb:persistent-field
	     * @ejb:interface-method view-type="local"
	     *
	     * @jboss:column-name name="STL_UNT_CST"
	     **/
    	public abstract double getStlUnitCost();
	    /**
	     * @ejb:interface-method view-type="local"
	     **/
	    public abstract void setStlUnitCost(double STL_UNT_CST);

	    /**
	     * @ejb:persistent-field
	     * @ejb:interface-method view-type="local"
	     *
	     * @jboss:column-name name="STL_QTY_DLVRD"
	     **/
    	public abstract double getStlQuantityDelivered();
	    /**
	     * @ejb:interface-method view-type="local"
	     **/
	    public abstract void setStlQuantityDelivered(double STL_QTY_DLVRD);

	    /**
	     * @ejb:persistent-field
	     * @ejb:interface-method view-type="local"
	     *
	     * @jboss:column-name name="STL_AMNT"
	     **/
    	public abstract double getStlAmount();
	    /**
	     * @ejb:interface-method view-type="local"
	     **/
	    public abstract void setStlAmount(double STL_AMNT);

	    /**
	     * @ejb:persistent-field
	     * @ejb:interface-method view-type="local"
	     *
	     * @jboss:column-name name="STL_EXPRY_DT"
	     **/
    	public abstract String getStlMisc();
	    /**
	     * @ejb:interface-method view-type="local"
	     **/
	    public abstract void setStlMisc(String STL_MISC);
	    /**
	     * @ejb:persistent-field
	     * @ejb:interface-method view-type="local"
	     *
	     * @jboss:column-name name="STL_AD_CMPNY"
	     **/
    	public abstract Integer getStlAdCompany();
	    /**
	     * @ejb:interface-method view-type="local"
	     **/
	    public abstract void setStlAdCompany(Integer STL_AD_CMPNY);

	    // Access methods for relationship fields

	    /**
	     * @ejb:interface-method view-type="local"
	     * @ejb:relation name="stocktransfer-stocktransferlines"
	     *               role-name="stocktransferline-has-one-stocktransfer"
	     *               cascade-delete="yes"
	     *
	     * @jboss:relation related-pk-field="stCode"
	     *                 fk-column="INV_STOCK_TRANSFER"
	     */
	    public abstract LocalInvStockTransfer getInvStockTransfer();
	    public abstract void setInvStockTransfer(LocalInvStockTransfer invStockTransfer);

	    /**
	     * @ejb:interface-method view-type="local"
	     * @ejb:relation name="unitofmeasure-stocktransferlines"
	     *               role-name="stocktransferline-has-one-unitofmeasure"
	     *               cascade-delete="no"
	     *
	     * @jboss:relation related-pk-field="uomCode"
	     *                 fk-column="INV_UNIT_OF_MEASURE"
	     */
	    public abstract LocalInvUnitOfMeasure getInvUnitOfMeasure();
	    public abstract void setInvUnitOfMeasure(LocalInvUnitOfMeasure invUnitOfMeasure);

	    /**
	     * @ejb:interface-method view-type="local"
  	     * @ejb:relation name="stocktransferline-costings"
	     *               role-name="stocktransferline-has-many-costings"
	    */
	    public abstract Collection getInvCostings();
	    public abstract void setInvCostings(Collection invCostings);

	    /**
	     * @ejb:interface-method view-type="local"
	     * @ejb:relation name="item-stocktransferlines"
	     *               role-name="stocktransferline-has-one-item"
	     *               cascade-delete="no"
	     *
	     * @jboss:relation related-pk-field="iiCode"
	     *                 fk-column="INV_ITEM"
	    */
	    public abstract LocalInvItem getInvItem();
	    public abstract void setInvItem(LocalInvItem invItem);


	    /**
	     * @ejb:interface-method view-type="local"
	     * @ejb:relation name="stocktransferline-tags"
	     *               role-name="stocktransferline-has-many-tags"
	     *
	     */
	    public abstract Collection getInvTags();
	    public abstract void setInvTags(Collection invTags);




	    /**
		* @jboss:dynamic-ql
		*/
	   public abstract Collection ejbSelectGeneric(java.lang.String jbossQl, java.lang.Object[] args)
	   throws FinderException;

	    // Business methods

	    /**
		 * @ejb:home-method view-type="local"
	     **/
	    public Collection ejbHomeGetStlByCriteria(java.lang.String jbossQl, java.lang.Object[] args)
	    throws FinderException {

	        return ejbSelectGeneric(jbossQl, args);

	    }

	    /**
		 * @ejb:interface-method view-type="local"
		 **/
		public void addInvCosting(LocalInvCosting invCosting) {

			Debug.print("InvStockTransferLineBean addInvCosting");

			try {

				Collection invCostings = getInvCostings();
				invCostings.add(invCosting);

			} catch (Exception ex) {

				throw new EJBException(ex.getMessage());

			}

		}

		/**
		 * @ejb:interface-method view-type="local"
		 **/
		public void dropInvCosting(LocalInvCosting invCosting) {

			Debug.print("InvStockTransferLineBean dropInvCosting");

			try {

				Collection invCostings = getInvCostings();
				invCostings.remove(invCosting);

			} catch (Exception ex) {

				throw new EJBException(ex.getMessage());

			}

		}

	    // EntityBean methods

	    /**
 	     * @ejb:create-method view-type="local"
	     **/
	    public java.lang.Integer ejbCreate (java.lang.Integer STL_CODE, java.lang.Integer STL_LCTN_FRM,
           java.lang.Integer STL_LCTN_TO, double STL_UNT_CST, double STL_QTY_DLVRD, double STL_AMNT,
           java.lang.Integer STL_AD_CMPNY)
	       throws CreateException {

	        Debug.print("invStockTransferLineBean ejbCreate");

	        setStlCode(STL_CODE);
	        setStlLocationFrom(STL_LCTN_FRM);
	        setStlLocationTo(STL_LCTN_TO);
	        setStlUnitCost(STL_UNT_CST);
	        setStlQuantityDelivered(STL_QTY_DLVRD);
	        setStlAmount(STL_AMNT);
	        setStlAdCompany(STL_AD_CMPNY);

	        return null;
	    }

	    /**
 	     * @ejb:create-method view-type="local"
	     **/
	    public java.lang.Integer ejbCreate (java.lang.Integer STL_LCTN_FRM, java.lang.Integer STL_LCTN_TO,
	       double STL_UNT_CST, double STL_QTY_DLVRD, double STL_AMNT, java.lang.Integer STL_AD_CMPNY)
	       throws CreateException {

	        Debug.print("invStockTransferLineBean ejbCreate");

	        setStlLocationFrom(STL_LCTN_FRM);
	        setStlLocationTo(STL_LCTN_TO);
	        setStlUnitCost(STL_UNT_CST);
	        setStlQuantityDelivered(STL_QTY_DLVRD);
	        setStlAmount(STL_AMNT);
	        setStlAdCompany(STL_AD_CMPNY);

	        return null;
	    }

	    public void ejbPostCreate (java.lang.Integer STL_CODE, java.lang.Integer STL_LCTN_FRM,
	       java.lang.Integer STL_LCTN_TO, double STL_UNT_CST, double STL_QTY_DLVRD, double STL_AMNT,
	       java.lang.Integer STL_AD_CMPNY)
	       throws CreateException { }

	    public void ejbPostCreate (java.lang.Integer STL_LCTN_FRM, java.lang.Integer STL_LCTN_TO,
 	       double STL_UNT_CST, double STL_QTY_DLVRD, double STL_AMNT, java.lang.Integer STL_AD_CMPNY)
 	       throws CreateException { }
}
