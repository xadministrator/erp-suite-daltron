package com.ejb.inv;

import java.util.Collection;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;

import com.util.AbstractEntityBean;
import com.ejb.ad.LocalAdUser;
import com.ejb.ap.LocalApPurchaseRequisitionLine;
import com.ejb.ap.LocalApVoucherLineItem;
import com.ejb.ap.LocalApPurchaseOrderLine;
import com.ejb.ar.LocalArInvoiceLineItem;
import com.ejb.ar.LocalArJobOrderInvoiceLine;
import com.ejb.ar.LocalArJobOrderLine;
import com.ejb.ar.LocalArSalesOrderInvoiceLine;
import com.ejb.ar.LocalArSalesOrderLine;
import com.ejb.inv.LocalInvBranchStockTransferLine;
import com.ejb.inv.LocalInvAdjustmentLine;
import com.util.Debug;

/**
 * @ejb:bean name="InvTagEJB"
 *           display-name="Tag Entity"
 *           type="CMP"
 *           cmp-version="2.x"
 *           view-type="local"
 *           local-jndi-name="omega-ejb/InvTagEJB"
 *           schema="InvTag"
 *           primkey-field="tgCode"
 *
 * @ejb:pk class="java.lang.Integer"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="invuser"
 *                        role-link="invuserlink"
 *
 * @ejb:permission role-name="invuser"
 *
 * @ejb:interface local-class="com.ejb.inv.LocalInvTag"
 *                local-extends="javax.ejb.EJBLocalObject"
 *
 * @ejb:home local-class="com.ejb.inv.LocalInvTagHome"
 *           local-extends="javax.ejb.EJBLocalHome"
 *
 * @ejb:finder signature="Collection findByTgCode(java.lang.Integer TG_CODE, java.lang.Integer TG_AD_CMPNY)"
 *             query="SELECT OBJECT(tg) FROM InvTag tg WHERE tg.tgCode=?1 AND tg.tgAdCompany=?2"
 *
 * @ejb:finder signature="Collection findByTgSpecs(java.lang.String TG_SPCS, java.lang.Integer TG_AD_CMPNY)"
 *             query="SELECT OBJECT(tg) FROM InvTag tg WHERE tg.tgSpecs=?1 AND tg.tgAdCompany=?2"
 *
 * @ejb:finder signature="Collection findByTgPropertyCode(java.lang.String TG_PRPRTY_CD, java.lang.Integer TG_AD_CMPNY)"
 *             query="SELECT OBJECT(tg) FROM InvTag tg WHERE tg.tgPropertyCode=?1 AND tg.tgAdCompany=?2"
 *
 * @ejb:finder signature="Collection findByPlCode(java.lang.Integer AP_PURCHASE_ORDER_LINE, java.lang.Integer TG_AD_CMPNY)"
 *             query="SELECT OBJECT(tg) FROM InvTag tg WHERE tg.apPurchaseOrderLine.plCode=?1 AND tg.tgAdCompany=?2"
 *
 * @ejb:finder signature="Collection findAllByPlCodeNotNull(java.lang.Integer TG_AD_CMPNY)"
 *             query="SELECT OBJECT(tg) FROM InvTag tg WHERE tg.apPurchaseOrderLine.plCode IS NOT NULL AND tg.tgAdCompany=?1"
 *
 * @ejb:finder signature="Collection findAllPlReceiving(java.lang.Integer TG_AD_CMPNY)"
 *             query="SELECT OBJECT(tg) FROM InvTag tg WHERE tg.apPurchaseOrderLine.plCode IS NOT NULL AND tg.apPurchaseOrderLine.apPurchaseOrder.poReceiving = 1 AND tg.tgAdCompany=?1"
 *
 * @ejb:finder signature="Collection findAllPlReceivingByItem(java.lang.String II_NM, java.lang.Integer TG_AD_CMPNY)"
 *             query="SELECT OBJECT(tg) FROM InvTag tg WHERE tg.apPurchaseOrderLine.apPurchaseOrder.poReceiving=1 AND tg.apPurchaseOrderLine.invItemLocation.invItem.iiName=?1 AND tg.tgAdCompany=?2 ORDER BY tg.tgSerialNumber ASC"
 *
 * @ejb:finder signature="Collection findByTgDocumentNumber(java.lang.String TG_DCMNT_NMBR, java.lang.Integer TG_AD_CMPNY)"
 *             query="SELECT OBJECT(tg) FROM InvTag tg WHERE tg.tgDocumentNumber=?1 AND tg.tgAdCompany=?2"
 *
 * @ejb:finder signature="LocalInvTag findByPlCodeTemp(java.lang.Integer AP_PURCHASE_ORDER_LINE, java.lang.Integer TG_AD_CMPNY)"
 *             query="SELECT OBJECT(tg) FROM InvTag tg WHERE tg.apPurchaseOrderLine.plCode=?1 AND tg.tgAdCompany=?2"
 *
 *
 * @ejb:finder signature="Collection findByPrlCode(java.lang.Integer AP_PURCHASE_REQUISITION_LINE, java.lang.Integer TG_AD_CMPNY)"
 *             query="SELECT OBJECT(tg) FROM InvTag tg WHERE tg.apPurchaseRequisitionLine.prlCode=?1 AND tg.tgAdCompany=?2"
 *
 * @ejb:finder signature="Collection findByBslCode(java.lang.Integer INV_BRANCH_STOCK_TRANSFER_LINE, java.lang.Integer TG_AD_CMPNY)"
 *             query="SELECT OBJECT(tg) FROM InvTag tg WHERE tg.invBranchStockTransferLine.bslCode=?1 AND tg.tgAdCompany=?2"
 *
 * @ejb:finder signature="Collection findByAlCode(java.lang.Integer INV_ADJUSTMENT_LINE, java.lang.Integer TG_AD_CMPNY)"
 *             query="SELECT OBJECT(tg) FROM InvTag tg WHERE tg.invAdjustmentLine.alCode=?1 AND tg.tgAdCompany=?2"
 *
 * @ejb:finder signature="Collection findTgAll(java.lang.Integer TG_AD_CMPNY)"
 *             query="SELECT OBJECT(tg) FROM InvTag tg WHERE tg.tgAdCompany=?1"
 *
 * @ejb:finder signature="Collection findByLatestDateFromAdjLine(java.util.Date TG_TXN_DT, java.lang.Integer ITM_LCTN_CD, java.lang.Integer AD_BRNCH_CD, java.lang.Integer TG_AD_CMPNY, java.lang.Integer LIMIT)"
 * 			   query="SELECT OBJECT(tg) FROM InvTag tg WHERE tg.tgTransactionDate<=?1 AND tg.invAdjustmentLine.invItemLocation.ilCode=?2 AND tg.invAdjustmentLine.invAdjustment.adjAdBranch=?3 AND tg.tgAdCompany=?4 DESC LIMIT ?5"
 *
 * @jboss:query signature="Collection findByLatestDateFromAdjLine(java.util.Date TG_TXN_DT, java.lang.Integer ITM_LCTN_CD, java.lang.Integer AD_BRNCH_CD, java.lang.Integer TG_AD_CMPNY, java.lang.Integer LIMIT)"
 * 			   query="SELECT OBJECT(tg) FROM InvTag tg WHERE tg.tgTransactionDate<=?1 AND tg.invAdjustmentLine.invItemLocation.ilCode=?2 AND tg.invAdjustmentLine.invAdjustment.adjAdBranch=?3 AND tg.tgAdCompany=?4 ORDER BY tg.tgTransactionDate DESC LIMIT ?5"
 *
 *
 * @ejb:finder signature="Collection findByLatestDateFromPOLine(java.util.Date TG_TXN_DT, java.lang.Integer ITM_LCTN_CD, java.lang.Integer AD_BRNCH_CD, java.lang.Integer TG_AD_CMPNY, java.lang.Integer LIMIT)"
 * 			   query="SELECT OBJECT(tg) FROM InvTag tg WHERE tg.tgTransactionDate<=?1 AND tg.apPurchaseOrderLine.invItemLocation.ilCode=?2 AND tg.apPurchaseOrderLine.apPurchaseOrder.poAdBranch=?3 AND tg.tgAdCompany=?4 DESC LIMIT ?5"
 *
 * @jboss:query signature="Collection findByLatestDateFromPOLine(java.util.Date TG_TXN_DT, java.lang.Integer ITM_LCTN_CD, java.lang.Integer AD_BRNCH_CD, java.lang.Integer TG_AD_CMPNY, java.lang.Integer LIMIT)"
 * 			   query="SELECT OBJECT(tg) FROM InvTag tg WHERE tg.tgTransactionDate<=?1 AND tg.apPurchaseOrderLine.invItemLocation.ilCode=?2 AND tg.apPurchaseOrderLine.apPurchaseOrder.poAdBranch=?3 AND tg.tgAdCompany=?4 ORDER BY tg.tgTransactionDate DESC LIMIT ?5"
 *
 *
 * @ejb:finder signature="Collection findByLatestDate(java.util.Date TG_TXN_DT, java.lang.Integer TG_AD_CMPNY, java.lang.Integer LIMIT)"
 * 			   query="SELECT OBJECT(tg) FROM InvTag tg WHERE tg.tgTransactionDate<=?1 AND tg.tgAdCompany=?2 DESC LIMIT ?3"
 *
 * @jboss:query signature="Collection findByLatestDate(java.util.Date TG_TXN_DT, java.lang.Integer TG_AD_CMPNY, java.lang.Integer LIMIT)"
 * 			   query="SELECT OBJECT(tg) FROM InvTag tg WHERE tg.tgTransactionDate<=?1 AND tg.tgAdCompany=?2 ORDER BY tg.tgTransactionDate DESC LIMIT ?3"
 *
 * @ejb:finder signature="Collection findByLessThanOrEqualToTransactionDateAndItemLocationAndAdBranchFromAdjLine(java.util.Date TG_TXN_DT, java.lang.Integer ITM_LCTN_CD, java.lang.Integer AD_BRNCH_CD, java.lang.Integer TG_AD_CMPNY)"
 *             query="SELECT OBJECT(tg) FROM InvTag tg WHERE tg.invAdjustmentLine IS NOT NULL AND tg.tgTransactionDate<=?1 AND ((tg.invAdjustmentLine.invItemLocation.ilCode=?2 AND tg.invAdjustmentLine.invAdjustment.adjAdBranch=?3)) AND tg.tgAdCompany=?4 ORDER BY tg.tgPropertyCode DESC"
 *
 * @ejb:finder signature="Collection findByLessThanOrEqualToTransactionDateAndItemLocationAndAdBranchFromPOLine(java.util.Date TG_TXN_DT, java.lang.Integer ITM_LCTN_CD, java.lang.Integer AD_BRNCH_CD, java.lang.Integer TG_AD_CMPNY)"
 *             query="SELECT OBJECT(tg) FROM InvTag tg WHERE tg.apPurchaseOrderLine IS NOT NULL AND tg.tgTransactionDate<=?1 AND ((tg.apPurchaseOrderLine.invItemLocation.ilCode=?2 AND tg.apPurchaseOrderLine.apPurchaseOrder.poAdBranch=?3)) AND tg.tgAdCompany=?4 ORDER BY tg.tgPropertyCode DESC"
 *
 * @ejb:finder signature="Collection findByLessThanOrEqualToTransactionDateAndItemLocationAndAdBranchFromPORecevingLine(java.util.Date TG_TXN_DT, java.lang.Integer ITM_LCTN_CD, java.lang.Integer AD_BRNCH_CD, java.lang.Integer TG_AD_CMPNY)"
 *             query="SELECT OBJECT(tg) FROM InvTag tg WHERE tg.apPurchaseOrderLine IS NOT NULL AND ((tg.apPurchaseOrderLine.apPurchaseOrder.poReceiving=1 AND tg.apPurchaseOrderLine.invItemLocation.ilCode=?2 AND tg.apPurchaseOrderLine.apPurchaseOrder.poAdBranch=?3)) AND tg.tgAdCompany=?4 ORDER BY tg.tgPropertyCode DESC"
 *
 * @ejb:finder signature="Collection findByTgPropertyCodeAndItemLocNameFromPOLine(java.lang.String TG_PRPRTY_CD, java.lang.String ITM_LCTN_NM, java.lang.Integer AD_BRNCH_CD, java.lang.Integer TG_AD_CMPNY)"
 *             query="SELECT OBJECT(tg) FROM InvTag tg WHERE tg.tgPropertyCode=?1 AND (tg.apPurchaseOrderLine.invItemLocation.invLocation.locName=?2 AND tg.apPurchaseOrderLine.apPurchaseOrder.poAdBranch=?3) AND tg.tgAdCompany=?4"
 *
 * @ejb:finder signature="Collection findAllInByTgSerialNumberAndAdBranchAndAdCompany(java.lang.String TG_SRL_NMBR, java.lang.Integer AD_BRNCH_CD, java.lang.Integer TG_AD_CMPNY)"
 *             query="SELECT OBJECT(tg) FROM InvTag tg WHERE tg.tgSerialNumber=?1 AND tg.tgType='IN' AND tg.apPurchaseOrderLine.apPurchaseOrder.poAdBranch=?2 AND tg.tgAdCompany=?3"
 *
 * @ejb:finder signature="Collection findAllOutByTgSerialNumberAndAdBranchAndAdCompany(java.lang.String TG_SRL_NMBR, java.lang.Integer AD_BRNCH_CD, java.lang.Integer TG_AD_CMPNY)"
 *             query="SELECT OBJECT(tg) FROM InvTag tg WHERE tg.tgSerialNumber=?1 AND tg.tgType='OUT' AND tg.tgAdCompany=?3"
 *
 * @ejb:finder signature="Collection findByTgPropertyCodeAndItemLocNameFromAdjLine(java.lang.String TG_PRPRTY_CD, java.lang.String ITM_LCTN_NM, java.lang.Integer AD_BRNCH_CD, java.lang.Integer TG_AD_CMPNY)"
 *             query="SELECT OBJECT(tg) FROM InvTag tg WHERE tg.tgPropertyCode=?1 AND (tg.invAdjustmentLine.invItemLocation.invLocation.locName=?2 AND tg.invAdjustmentLine.invAdjustment.adjAdBranch=?3) AND tg.tgAdCompany=?4"
 *
 * @ejb:finder signature="Collection findTgSerialNumberByPlCode(java.lang.String TG_SRL_NMBR, java.lang.Integer PL_CODE )"
 *             query="SELECT OBJECT(tg) FROM InvTag tg WHERE tg.tgSerialNumber=?1 AND tg.apPurchaseOrderLine.plCode=?2"
 * 
 * @ejb:finder signature="Collection findTgSerialNumberBySilCode(java.lang.String TG_SRL_NMBR, java.lang.Integer SIL_CODE )"
 *             query="SELECT OBJECT(tg) FROM InvTag tg WHERE tg.tgSerialNumber=?1 AND tg.arSalesOrderInvoiceLine.silCode=?2"
 *   
 * @ejb:finder signature="Collection findTgSerialNumberByIliCode(java.lang.String TG_SRL_NMBR, java.lang.Integer ILI_CODE )"
 *             query="SELECT OBJECT(tg) FROM InvTag tg WHERE tg.tgSerialNumber=?1 AND tg.arInvoiceLineItem.iliCode=?2"
 * 
 * @ejb:finder signature="Collection findTgSerialNumberByJilCode(java.lang.String TG_SRL_NMBR, java.lang.Integer JIL_CODE )"
 *             query="SELECT OBJECT(tg) FROM InvTag tg WHERE tg.tgSerialNumber=?1 AND tg.arJobOrderInvoiceLine.jilCode=?2"
 *
 * @ejb:select signature="Collection ejbSelectGeneric(java.lang.String jbossQl, java.lang.Object[] args)"
 *             result-type-mapping="Local"
 *             method-intf="LocalHome"
 *             query="SELECT OBJECT(tg) FROM InvTag tg"
 *
 * @jboss:query signature="Collection ejbSelectGeneric(java.lang.String jbossQl, java.lang.Object[] args)"
 *                         dynamic="true"
 *
 *
 * @ejb:value-object match="*"
 *             name="InvTag"
 *
 * @jboss:persistence table-name="INV_TAG"
 *
 * @jboss:entity-command name="mysql-get-generated-keys"
 *
 */

public abstract class InvTagBean extends AbstractEntityBean {

//	 Access methods for persistent fields

	   /**
	    * @ejb:persistent-field
	    * @ejb:interface-method view-type="local"
	    * @ejb:pk-field
	    *
	    * @jboss:column-name name="TG_CODE"
	    *
	    * @jboss:persistence auto-increment="true"
	    **/
	   public abstract java.lang.Integer getTgCode();
	   public abstract void setTgCode(java.lang.Integer TG_CODE);

	   /**
        * @ejb:persistent-field
        * @ejb:interface-method view-type="local"
        *
        * @jboss:column-name name="TG_PRPRTY_CODE"
        **/
       public abstract String getTgPropertyCode();
       /**
        * @ejb:interface-method view-type="local"
        **/
       public abstract void setTgPropertyCode(String TG_PRPRTY_CODE);

       /**
        * @ejb:persistent-field
        * @ejb:interface-method view-type="local"
        *
        * @jboss:column-name name="TG_SRL_NMBR"
        **/
       public abstract String getTgSerialNumber();
       /**
        * @ejb:interface-method view-type="local"
        **/
       public abstract void setTgSerialNumber(String TG_SRL_NMBR);

       /**
        * @ejb:persistent-field
        * @ejb:interface-method view-type="local"
        *
        * @jboss:column-name name="TG_DCMNT_NMBR"
        **/
       public abstract String getTgDocumentNumber();
       /**
        * @ejb:interface-method view-type="local"
        **/
       public abstract void setTgDocumentNumber(String TG_DCMNT_NMBR);

       /**
        * @ejb:persistent-field
        * @ejb:interface-method view-type="local"
        *
        * @jboss:column-name name="TG_SPCS"
        **/
       public abstract String getTgSpecs();
       /**
        * @ejb:interface-method view-type="local"
        **/
       public abstract void setTgSpecs(String TG_SPCS);

       /**
        * @ejb:persistent-field
        * @ejb:interface-method view-type="local"
        *
        * @jboss:column-name name="TG_EXPRY_DT"
        **/
       public abstract java.util.Date getTgExpiryDate();
       /**
        * @ejb:interface-method view-type="local"
        **/
       public abstract void setTgExpiryDate(java.util.Date TG_EXPRY_DT);

       /**
        * @ejb:persistent-field
        * @ejb:interface-method view-type="local"
        *
        * @jboss:column-name name="TG_TXN_DT"
        **/
       public abstract java.util.Date getTgTransactionDate();
       /**
        * @ejb:interface-method view-type="local"
        **/
       public abstract void setTgTransactionDate(java.util.Date TG_TXN_DT);

       /**
        * @ejb:persistent-field
        * @ejb:interface-method view-type="local"
        *
        * @jboss:column-name name="TG_TYP"
        **/
       public abstract String getTgType();
       /**
        * @ejb:interface-method view-type="local"
        **/
       public abstract void setTgType(String TG_TYP);

       /**
        * @ejb:persistent-field
        * @ejb:interface-method view-type="local"
        *
        * @jboss:column-name name="TG_AD_CMPNY"
        **/
       public abstract Integer getTgAdCompany();
       /**
        * @ejb:interface-method view-type="local"
        **/
       public abstract void setTgAdCompany(Integer TG_AD_CMPNY);

	   // Access methods for relationship fields


       /**
	    * @ejb:interface-method view-type="local"
	    * @ejb:relation name="purchaserequisitionline-tags"
	    *               role-name="tag-has-one-purchaserequisitionline"
	    *				cascade-delete="no"
	    *
	    *
	    * @jboss:relation related-pk-field="prlCode"
	    *                 fk-column="AP_PURCHASE_REQUISITION_LINE"
	    */
	   public abstract LocalApPurchaseRequisitionLine getApPurchaseRequisitionLine();
	   /**
	    * @ejb:interface-method view-type="local"
	    **/
	   public abstract void setApPurchaseRequisitionLine(LocalApPurchaseRequisitionLine apPurchaseRequisitionLine);
	   
	   
	   /**
	    * @ejb:interface-method view-type="local"
	    * @ejb:relation name="joborderline-tags"
	    *               role-name="tag-has-one-joborderline"
	    *				cascade-delete="no"
	    *
	    *
	    * @jboss:relation related-pk-field="jolCode"
	    *                 fk-column="AR_JOB_ORDER_LINE"
	    */
	   public abstract LocalArJobOrderLine getArJobOrderLine();
	   /**
	    * @ejb:interface-method view-type="local"
	    **/
	   public abstract void setArJobOrderLine(LocalArJobOrderLine arJobOrderLine);

	   /**
	    * @ejb:interface-method view-type="local"
	    * @ejb:relation name="purchaseorderline-tags"
	    *               role-name="tag-has-one-purchaseorderline"
	    *				cascade-delete="yes"
	    *
	    *
	    * @jboss:relation related-pk-field="plCode"
	    *                 fk-column="AP_PURCHASE_ORDER_LINE"
	    */
	   public abstract LocalApPurchaseOrderLine getApPurchaseOrderLine();
	   /**
	    * @ejb:interface-method view-type="local"
	    **/
	   public abstract void setApPurchaseOrderLine(LocalApPurchaseOrderLine apPurchaseOrderLine);



	   /**
	    * @ejb:interface-method view-type="local"
	    * @ejb:relation name="invoicelineitem-tags"
	    *               role-name="tag-has-one-invoicelineitem"
	    *				cascade-delete="yes"
	    *
	    *
	    * @jboss:relation related-pk-field="iliCode"
	    *                 fk-column="AR_INVOICE_LINE_ITEM"
	    */
	   public abstract LocalArInvoiceLineItem getArInvoiceLineItem();
	   /**
	    * @ejb:interface-method view-type="local"
	    **/
	   public abstract void setArInvoiceLineItem(LocalArInvoiceLineItem arInvoiceLineItem);






	   /**
	    * @ejb:interface-method view-type="local"
	    * @ejb:relation name="user-tags"
	    *               role-name="tag-has-one-user"
	    *				cascade-delete="yes"
	    *
	    *
	    * @jboss:relation related-pk-field="usrCode"
	    *                 fk-column="AD_USER"
	    */
	   public abstract LocalAdUser getAdUser();
	   /**
	    * @ejb:interface-method view-type="local"
	    **/
	   public abstract void setAdUser(LocalAdUser adUser);

	   /**
	     * @ejb:interface-method view-type="local"
	     * @ejb:relation name="branchstocktransferline-tags"
	     *               role-name="tag-has-one-branchstocktransferline"
         *   	      	 cascade-delete="yes"
	     *
	     * @jboss:relation related-pk-field="bslCode"
	     *                 fk-column="INV_BRANCH_STOCK_TRANSFER_LINE"
	     */
	   public abstract LocalInvBranchStockTransferLine getInvBranchStockTransferLine();
	   /**
	    * @ejb:interface-method view-type="local"
	    **/
	   public abstract void setInvBranchStockTransferLine(LocalInvBranchStockTransferLine invBranchStockTransferLine);

	    /**
	     * @ejb:interface-method view-type="local"
	     * @ejb:relation name="adjustmentline-tags"
	     *               role-name="tag-has-one-adjustmentline"
	     *   	      	 cascade-delete="yes"
	     *
	     * @jboss:relation related-pk-field="alCode"
	     *                 fk-column="INV_ADJUSTMENT_LINE"
	     */
	   public abstract LocalInvAdjustmentLine getInvAdjustmentLine();
	   /**
	    * @ejb:interface-method view-type="local"
	    **/
	   public abstract void setInvAdjustmentLine(LocalInvAdjustmentLine invAdjustmentLine);


	   /**
	      * @ejb:interface-method view-type="local"
	      * @ejb:relation name="depreciationledger-tags"
	      *               role-name="tag-has-many-depreciationledgers"
	      *
	      */
	     public abstract Collection getInvDepreciationLedgers();
	     public abstract void setInvDepreciationLedgers(Collection invDepreciationLedger);



	     /**
		    * @ejb:interface-method view-type="local"
		    * @ejb:relation name="buildunbuildassemblyline-tags"
		    *               role-name="tag-has-one-buildunbuildassemblyline"
		    *               cascade-delete="yes"
		    *
		    * @jboss:relation related-pk-field="blCode"
		    *                 fk-column="INV_BUILD_UNBUILD_ASSEMBLY_LINE"
		    */
		   public abstract LocalInvBuildUnbuildAssemblyLine getInvBuildUnbuildAssemblyLine();
		   /**
		    * @ejb:interface-method view-type="local"
		    **/
		   public abstract void setInvBuildUnbuildAssemblyLine(LocalInvBuildUnbuildAssemblyLine invBuildUnbuildAssemblyLine);


		   /**
		    * @ejb:interface-method view-type="local"
		    * @ejb:relation name="itemlocation-tags"
		    *               role-name="tag-has-one-itemlocation"
		    *               cascade-delete="no"
		    *
		    * @jboss:relation related-pk-field="ilCode"
		    *                 fk-column="INV_ITEM_LOCATION"
		    */
		   public abstract LocalInvItemLocation getInvItemLocation();
		   /**
		    * @ejb:interface-method view-type="local"
		    **/
		   public abstract void setInvItemLocation(LocalInvItemLocation invItemLocation);


		   /**
		    * @ejb:interface-method view-type="local"
		    * @ejb:relation name="salesorderline-tags"
		    *               role-name="tag-has-one-salesorderline"
		    *               cascade-delete="no"
		    *
		    * @jboss:relation related-pk-field="solCode"
		    *                 fk-column="AR_SALES_ORDER_LINE"
		    */
		   public abstract LocalArSalesOrderLine getArSalesOrderLine();
		   /**
		    * @ejb:interface-method view-type="local"
		    **/
		   public abstract void setArSalesOrderLine(LocalArSalesOrderLine arSalesOrderLine);


		   /**
		    * @ejb:interface-method view-type="local"
		    * @ejb:relation name="salesorderinvoiceline-tags"
		    *               role-name="tag-has-one-salesorderinvoiceline"
		    *               cascade-delete="no"
		    *
		    * @jboss:relation related-pk-field="silCode"
		    *                 fk-column="AR_SALES_ORDER_INVOICE_LINE"
		    */
		   public abstract LocalArSalesOrderInvoiceLine getArSalesOrderInvoiceLine();
		   /**
		    * @ejb:interface-method view-type="local"
		    **/
		   public abstract void setArSalesOrderInvoiceLine(LocalArSalesOrderInvoiceLine arSalesOrderInvoiceLine);
		   
		   /**
		    * @ejb:interface-method view-type="local"
		    * @ejb:relation name="joborderinvoiceline-tags"
		    *               role-name="tag-has-one-joborderinvoiceline"
		    *               cascade-delete="no"
		    *
		    * @jboss:relation related-pk-field="jilCode"
		    *                 fk-column="AR_JOB_ORDER_INVOICE_LINE"
		    */
		   public abstract LocalArJobOrderInvoiceLine getArJobOrderInvoiceLine();
		   /**
		    * @ejb:interface-method view-type="local"
		    **/
		   public abstract void setArJobOrderInvoiceLine(LocalArJobOrderInvoiceLine arJobOrderInvoiceLine);



		   /**
		    * @ejb:interface-method view-type="local"
		    * @ejb:relation name="stocktransferline-tags"
		    *               role-name="tag-has-one-stocktransferline"
		    *               cascade-delete="no"
		    *
		    * @jboss:relation related-pk-field="stlCode"
		    *                 fk-column="INV_STOCK_TRANSFER_LINE"
		    */
		   public abstract LocalInvStockTransferLine getInvStockTransferLine();
		   /**
		    * @ejb:interface-method view-type="local"
		    **/
		   public abstract void setInvStockTransferLine(LocalInvStockTransferLine invStockTransferLine);


		   /**
		    * @ejb:interface-method view-type="local"
		    * @ejb:relation name="voucherlineitem-tags"
		    *               role-name="tag-has-one-voucherlineitem"
		    *               cascade-delete="no"
		    *
		    * @jboss:relation related-pk-field="vliCode"
		    *                 fk-column="AP_VOUCHER_LINE_ITEM"
		    */
		   public abstract LocalApVoucherLineItem getApVoucherLineItem();
		   /**
		    * @ejb:interface-method view-type="local"
		    **/
		   public abstract void setApVoucherLineItem(LocalApVoucherLineItem apVoucherLineItem);



		   /**
		    * @ejb:interface-method view-type="local"
		    * @ejb:relation name="physicalinventoryline-tags"
		    *               role-name="tag-has-one-physicalinventoryline"
		    *               cascade-delete="no"
		    *
		    * @jboss:relation related-pk-field="pilCode"
		    *                 fk-column="INV_PHYSICAL_INVENTORY_LINE"
		    */
		   public abstract LocalInvPhysicalInventoryLine getInvPhysicalInventoryLine();
		   /**
		    * @ejb:interface-method view-type="local"
		    **/
		   public abstract void setInvPhysicalInventoryLine(LocalInvPhysicalInventoryLine invPhysicalInventoryLine);


	    // /**
	    //  * @ejb:interface-method view-type="local"
	    //  * @ejb:relation name="buildunbuildassemblyline-tags"
	    //  *               role-name="tag-has-many-buildunbuildassemblyline"
	   //   *
	    //  */
	  //   public abstract Collection getInvBuildUnbuildAssemblyLines();
	   //  public abstract void setInvBuildUnbuildAssemblyLines(Collection invBuildUnbuildAssemblyLine);


	   /**
		 * @jboss:dynamic-ql
		 */
		public abstract Collection ejbSelectGeneric(java.lang.String jbossQl, java.lang.Object[] args)
		throws FinderException;

		// BUSINESS METHODS

		/**
		 * @ejb:home-method view-type="local"
		 */
		public Collection ejbHomeGetTgByCriteria(java.lang.String jbossQl, java.lang.Object[] args)
		throws FinderException {

			return ejbSelectGeneric(jbossQl, args);

		}

		/**
		  * @ejb:interface-method view-type="local"
		  **/
		 public void addInvDepreciationLedger(LocalInvDepreciationLedger invDepreciationLedger) {

		      Debug.print("InvTagBean addInvDepreciationLedger");

		      try {

		      Collection invDepreciationLedgers = getInvDepreciationLedgers();
		      invDepreciationLedgers.add(invDepreciationLedger);

		      } catch (Exception ex) {

		          throw new EJBException(ex.getMessage());

		      }

		 }

	     /**
	      * @ejb:interface-method view-type="local"
	      **/
	     public void dropInvDepreciationLedger(LocalInvDepreciationLedger invDepreciationLedger) {

			  Debug.print("InvTagBean dropInvDepreciationLedger");

			  try {

			     Collection invDepreciationLedgers = getInvDepreciationLedgers();
			     invDepreciationLedgers.remove(invDepreciationLedger);

			  } catch (Exception ex) {

			     throw new EJBException(ex.getMessage());

			  }

	     }



	   // EntityBean methods

	   /**
	    * @ejb:create-method view-type="local"
	    **/


	   public java.lang.Integer ejbCreate (Integer TG_CODE, String TG_PRPRTY_CODE, String TG_SRL_NMBR,
			   String TG_DCMNT_NMBR, java.util.Date TG_EXPRY_DT, String TG_SPECS, Integer TG_CMPNY,
			   java.util.Date TG_TXN_DT, String TG_TYP)
	      throws CreateException {

	      Debug.print("invTagBean ejbCreate");

	      setTgCode(TG_CODE);
	      setTgPropertyCode(TG_PRPRTY_CODE);
	      setTgSerialNumber(TG_SRL_NMBR);
	      setTgDocumentNumber(TG_DCMNT_NMBR);
	      setTgExpiryDate(TG_EXPRY_DT);
	      setTgSpecs(TG_SPECS);
	      setTgAdCompany(TG_CMPNY);
	      setTgTransactionDate(TG_TXN_DT);
	      setTgType(TG_TYP);

	      return null;
	   }

	   /**
	    * @ejb:create-method view-type="local"
	    **/


	   public java.lang.Integer ejbCreate (String TG_PRPRTY_CODE, String TG_SRL_NMBR,
			   String TG_DCMNT_NMBR, java.util.Date TG_EXPRY_DT, String TG_SPECS, Integer TG_CMPNY,
			   java.util.Date TG_TXN_DT, String TG_TYP)
	      throws CreateException {

	      Debug.print("invTagBean ejbCreate");

	      setTgPropertyCode(TG_PRPRTY_CODE);
	      setTgSerialNumber(TG_SRL_NMBR);
	      setTgDocumentNumber(TG_DCMNT_NMBR);
	      setTgExpiryDate(TG_EXPRY_DT);
	      setTgSpecs(TG_SPECS);
	      setTgAdCompany(TG_CMPNY);
	      setTgTransactionDate(TG_TXN_DT);
	      setTgType(TG_TYP);
	      return null;
	   }


	   public void ejbPostCreate (Integer TG_CODE, String TG_PRPRTY_CODE, String TG_SRL_NMBR,
			   String TG_DCMNT_NMBR, java.util.Date TG_EXPRY_DT, String TG_SPECS, Integer TG_CMPNY,
			   java.util.Date TG_TXN_DT, String TG_TYP)
	      throws CreateException { }

	   public void ejbPostCreate (String TG_PRPRTY_CODE, String TG_SRL_NMBR,
			   String TG_DCMNT_NMBR, java.util.Date TG_EXPRY_DT, String TG_SPECS, Integer TG_CMPNY,
			   java.util.Date TG_TXN_DT, String TG_TYP)
	      throws CreateException { }

}
