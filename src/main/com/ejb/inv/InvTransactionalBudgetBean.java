/*
 * com/ejb/inv/LocalInvTransactionalBudgetBean.java
 *
 * Created on April 17, 2006 05:20 PM
 */

package com.ejb.inv;

import java.util.Collection;

import javax.ejb.CreateException;
import javax.ejb.FinderException;

import com.util.AbstractEntityBean;
import com.util.Debug;

/**
 * 
 * @author  Aliza D.J. Anos
 * 
 * @ejb:bean name="InvTransactionalBudgetEJB"
 *           display-name="Transactional Budget Entity"
 *           type="CMP"
 *           cmp-version="2.x"
 *           view-type="local"
 *           local-jndi-name="omega-ejb/InvTransactionalBudgetEJB"
 *           schema="InvTransactionalBudget"
 *           primkey-field="tbCode"
 *       
 * @ejb:pk class="java.lang.Integer"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="invuser"
 *                        role-link="invuserlink"
 *
 * @ejb:permission role-name="invuser"
 *
 * @ejb:interface local-class="com.ejb.inv.LocalInvTransactionalBudget"
 *                local-extends="javax.ejb.EJBLocalObject"
 *
 * @ejb:home local-class="com.ejb.inv.LocalInvTransactionalBudgetHome"
 *           local-extends="javax.ejb.EJBLocalHome"
 * 
 * 
 * @ejb:finder signature="Collection findByTbCode(java.lang.Integer TB_CODE, java.lang.Integer TB_AD_CMPNY)"
 *             query="SELECT OBJECT(tb) FROM InvTransactionalBudget tb WHERE tb.tbCode=?1 AND tb.tbAdCompany=?2"
 *             
 * @ejb:finder signature="Collection findByTbItemNameAll(java.lang.String TB_NAME)"
 *             query="SELECT OBJECT(tb) FROM InvTransactionalBudget tb WHERE tb.tbName=?1"   
 *                      
 * @ejb:finder signature="Collection findByTbTotalAmountAndYear(java.lang.Double TB_TTL_AMNT, java.lang.Integer TB_YR)"
 *             query="SELECT OBJECT(tb) FROM InvTransactionalBudget tb WHERE tb.tbTotalAmount=?1 AND tb.tbYear=?2"    
 *                     
 * @ejb:finder signature="LocalInvTransactionalBudget findByTbInvItem(java.lang.Integer INV_ITEM)"
 *             query="SELECT OBJECT(tb) FROM InvTransactionalBudget tb WHERE tb.invItem.iiCode=?1"
 *             
 * @ejb:finder signature="LocalInvTransactionalBudget findByTbItemName(java.lang.String TB_NAME)"
 *             query="SELECT OBJECT(tb) FROM InvTransactionalBudget tb WHERE tb.tbName=?1"
 *             
 * @ejb:finder signature="LocalInvTransactionalBudget findByTbItemNameAndDepartment(java.lang.String TB_NAME, java.lang.Integer AD_LOOK_UP_VALUE,java.lang.Integer TB_AD_CMPNY )"
 *             query="SELECT OBJECT(tb) FROM InvTransactionalBudget tb WHERE tb.tbName=?1 AND tb.tbAdLookupValue=?2 AND tb.tbAdCompany=?3"
 *             
 * @ejb:finder signature="LocalInvTransactionalBudget findByTbDepartmentCode(java.lang.Integer AD_LOOK_UP_VALUE, java.lang.Integer TB_AD_CMPNY)"
 *             query="SELECT OBJECT(tb) FROM InvTransactionalBudget tb WHERE tb.tbAdLookupValue=?1 AND tb.tbAdCompany=?2"  
 *
 * @ejb:finder signature="LocalInvTransactionalBudget findByTbCodeAndDepartmentCode(java.lang.Integer TB_CODE, java.lang.Integer AD_LOOK_UP_VALUE, java.lang.Integer TB_AD_CMPNY)"
 *             query="SELECT OBJECT(tb) FROM InvTransactionalBudget tb WHERE tb.tbCode=?1 AND tb.tbAdLookupValue=?2 AND tb.tbAdCompany=?3"  
 *                        
 * @ejb:finder signature="LocalInvTransactionalBudget findByTbItemNameAndTbDeptAndTbYear(java.lang.String TB_NAME, java.lang.Integer AD_LOOK_UP_VALUE, java.lang.Integer TB_YR, java.lang.Integer TB_AD_CMPNY )"
 *             query="SELECT OBJECT(tb) FROM InvTransactionalBudget tb WHERE tb.tbName=?1 AND tb.tbAdLookupValue=?2 AND tb.tbYear=?3 AND tb.tbAdCompany=?4"
 *                        
 * @ejb:finder signature="LocalInvTransactionalBudget findByTbYear(java.lang.Integer TB_YR, java.lang.Integer TB_AD_CMPNY)"
 *             query="SELECT OBJECT(tb) FROM InvTransactionalBudget tb WHERE tb.tbYear=?1 AND tb.tbAdCompany=?2" 
 *                      
 *  
 * @jboss:query signature="Collection ejbSelectGeneric(java.lang.String jbossQl, java.lang.Object[] args)"
 *                         dynamic="true"
 *
 * @ejb:value-object match="*"
 *             name="InvTransactionalBudget"
 * 
 * @jboss:persistence table-name="INV_TRNSCTNL_BDGT"
 *
 * @jboss:entity-command name="mysql-get-generated-keys"
 *
 */

public abstract class InvTransactionalBudgetBean extends AbstractEntityBean {
	
	//	 Access methods for persistent fields
	
	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 * @ejb:pk-field
	 *
	 * @jboss:column-name name="TB_CODE"
	 *
	 * @jboss:persistence auto-increment="true"
	 **/
	public abstract java.lang.Integer getTbCode();
	public abstract void setTbCode(java.lang.Integer TB_CODE);
	
	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="TB_NAME"
	 **/
	public abstract String getTbName();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setTbName(String TB_NAME);
	
	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="TB_DESC"
	 **/
	public abstract String getTbDesc();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setTbDesc(String TB_DESC);
	
	
	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="TB_QNTTY_JAN"
	 **/
	public abstract double getTbQuantityJan();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setTbQuantityJan(double TB_QNTTY_JAN);
	
	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="TB_QNTTY_FEB"
	 **/
	public abstract double getTbQuantityFeb();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setTbQuantityFeb(double TB_QNTTY_FEB);
	
	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="TB_QNTTY_MRCH"
	 **/
	public abstract double getTbQuantityMrch();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setTbQuantityMrch(double TB_QNTTY_MRCH);
	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="TB_QNTTY_APRL"
	 **/
	public abstract double getTbQuantityAprl();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setTbQuantityAprl(double TB_QNTTY_Aprl);
	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="TB_QNTTY_MAY"
	 **/
	public abstract double getTbQuantityMay();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setTbQuantityMay(double TB_QNTTY_MAY);
	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="TB_QNTTY_JUN"
	 **/
	public abstract double getTbQuantityJun();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setTbQuantityJun(double TB_QNTTY_JUN);
	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="TB_QNTTY_JUL"
	 **/
	public abstract double getTbQuantityJul();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setTbQuantityJul(double TB_QNTTY_JUL);
	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="TB_QNTTY_AUG"
	 **/
	public abstract double getTbQuantityAug();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setTbQuantityAug(double TB_QNTTY_AUG);
	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="TB_QNTTY_SEP"
	 **/
	public abstract double getTbQuantitySep();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setTbQuantitySep(double TB_QNTTY_SEP);
	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="TB_QNTTY_OCT"
	 **/
	public abstract double getTbQuantityOct();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setTbQuantityOct(double TB_QNTTY_OCT);
	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="TB_QNTTY_NOV"
	 **/
	public abstract double getTbQuantityNov();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setTbQuantityNov(double TB_QNTTY_NOV);
	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="TB_QNTTY_DEC"
	 **/
	public abstract double getTbQuantityDec();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setTbQuantityDec(double TB_QNTTY_DEC);
	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="TB_AMNT"
	 **/
	public abstract double getTbAmount();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setTbAmount(double TB_AMNT);
	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="TB_TTL_AMNT"
	 **/
	public abstract double getTbTotalAmount();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setTbTotalAmount(double TB_TTL_AMNT);
	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="TB_YR"
	 **/
	public abstract Integer getTbYear();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setTbYear(Integer TB_YR);
	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="TB_AD_CMPNY"
	 **/
	public abstract Integer getTbAdCompany();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setTbAdCompany(Integer TB_AD_CMPNY);
	
	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="AD_LOOK_UP_VALUE"
	 **/
	public abstract Integer getTbAdLookupValue();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setTbAdLookupValue(Integer AD_LOOK_UP_VALUE);
	
	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="INV_UNIT_OF_MEASURE"
	 **/
	public abstract Integer getInvUnitOfMeasureCode();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setInvUnitOfMeasureCode(Integer INV_UNIT_OF_MEASURE);
	
	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="INV_ITEM"
	 **/
	public abstract Integer getInvUnitCode();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setInvUnitCode(Integer INV_ITEM);
	
	//	 Access methods for relationship fields
	
	/**
	 * @ejb:interface-method view-type="local"
	 * @ejb:relation name="unitofmeasure-transactionalbudget"
	 *               role-name="transactionalbudget-has-one-unitofmeasure"
	 *               cascade-delete="no"
	 *               
	 *
	 * @jboss:relation related-pk-field="uomCode"
	 *                 fk-column="INV_UNIT_OF_MEASURE"
	 */
	public abstract LocalInvUnitOfMeasure getInvUnitOfMeasure();
    /**
     * @ejb:interface-method view-type="local"
     **/
    public abstract void setInvUnitOfMeasure(LocalInvUnitOfMeasure invUnitOfMeasure);
    
		
	/**
	 * @ejb:interface-method view-type="local"
	 * @ejb:relation name="item-transactionalbudget"
	 *               role-name="transactionalbudget-has-one-item"
	 *               
	 *				 
	 * @jboss:relation related-pk-field="iiCode"
	 *                 fk-column="INV_ITEM"
	 */
	public abstract LocalInvItem getInvItem();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setInvItem(LocalInvItem invItem);
	
		
	/**
	 * @jboss:dynamic-ql
	 */
	public abstract Collection ejbSelectGeneric(java.lang.String jbossQl, java.lang.Object[] args)
	throws FinderException;
	
//	 BUSINESS METHODS
	
	/**
	* @ejb:home-method view-type="local"
	*/
	public Collection ejbHomeGetTbByCriteria(java.lang.String jbossQl, java.lang.Object[] args)
	   throws FinderException {
	   	
	   return ejbSelectGeneric(jbossQl, args);
	   
	}
	
//	 ENTITY METHODS
	
	/**
     * @ejb:create-method view-type="local"
     **/
    public Integer ejbCreate(Integer TB_CODE, String TB_NAME, String TB_DESC, 
    		double TB_QNTTY_JAN, double TB_QNTTY_FEB, double TB_QNTTY_MRCH, 
    		double TB_QNTTY_APRL, double TB_QNTTY_MAY, double TB_QNTTY_JUN, 
    		double TB_QNTTY_JUL, double TB_QNTTY_AUG, double TB_QNTTY_SEP, 
    		double TB_QNTTY_OCT, double TB_QNTTY_NOV, double TB_QNTTY_DEC,
    		double TB_AMNT, double TB_TTL_AMNT, Integer TB_YR, Integer TB_AD_CMPNY   
    		)
        throws CreateException {
          
        Debug.print("InvTransactionalBudget ejbCreate");
       
        setTbCode(TB_CODE);
        setTbName(TB_NAME);
        setTbDesc(TB_DESC);
        
        setTbQuantityJan(TB_QNTTY_JAN);
        setTbQuantityFeb(TB_QNTTY_FEB);
        setTbQuantityMrch(TB_QNTTY_MRCH);
        setTbQuantityAprl(TB_QNTTY_APRL);
        setTbQuantityMay(TB_QNTTY_MAY);
        setTbQuantityJun(TB_QNTTY_JUN);
        setTbQuantityJul(TB_QNTTY_JUL);
        setTbQuantityAug(TB_QNTTY_AUG);
        setTbQuantitySep(TB_QNTTY_SEP);
        setTbQuantityOct(TB_QNTTY_OCT);
        setTbQuantityNov(TB_QNTTY_NOV);
        setTbQuantityDec(TB_QNTTY_DEC);
        setTbAmount(TB_AMNT);
        setTbTotalAmount(TB_TTL_AMNT);
        setTbYear(TB_YR);
        setTbAdCompany(TB_AD_CMPNY);
        return null;
       
    }
    
    /**
     * @ejb:create-method view-type="local"
     **/
    public Integer ejbCreate(String TB_NAME, String TB_DESC, 
    		double TB_QNTTY_JAN, double TB_QNTTY_FEB, double TB_QNTTY_MRCH, 
    		double TB_QNTTY_APRL, double TB_QNTTY_MAY, double TB_QNTTY_JUN, 
    		double TB_QNTTY_JUL, double TB_QNTTY_AUG, double TB_QNTTY_SEP, 
    		double TB_QNTTY_OCT, double TB_QNTTY_NOV, double TB_QNTTY_DEC,
    		double TB_AMNT, double TB_TTL_AMNT, Integer TB_YR, Integer TB_AD_CMPNY   
    		)
        throws CreateException {
          
        Debug.print("InvTransactionalBudget ejbCreate");
       
        setTbName(TB_NAME);
        setTbDesc(TB_DESC);
       
        setTbQuantityJan(TB_QNTTY_JAN);
        setTbQuantityFeb(TB_QNTTY_FEB);
        setTbQuantityMrch(TB_QNTTY_MRCH);
        setTbQuantityAprl(TB_QNTTY_APRL);
        setTbQuantityMay(TB_QNTTY_MAY);
        setTbQuantityJun(TB_QNTTY_JUN);
        setTbQuantityJul(TB_QNTTY_JUL);
        setTbQuantityAug(TB_QNTTY_AUG);
        setTbQuantitySep(TB_QNTTY_SEP);
        setTbQuantityOct(TB_QNTTY_OCT);
        setTbQuantityNov(TB_QNTTY_NOV);
        setTbQuantityDec(TB_QNTTY_DEC);
        setTbAmount(TB_AMNT);
        setTbTotalAmount(TB_TTL_AMNT);
        setTbYear(TB_YR);
        setTbAdCompany(TB_AD_CMPNY);
        return null;
       
    }
    
    public void ejbPostCreate(Integer TB_CODE, String TB_NAME, String TB_DESC, 
    		double TB_QNTTY_JAN, double TB_QNTTY_FEB, double TB_QNTTY_MRCH, 
    		double TB_QNTTY_APRL, double TB_QNTTY_MAY, double TB_QNTTY_JUN, 
    		double TB_QNTTY_JUL, double TB_QNTTY_AUG, double TB_QNTTY_SEP, 
    		double TB_QNTTY_OCT, double TB_QNTTY_NOV, double TB_QNTTY_DEC,
    		double TB_AMNT, double TB_TTL_AMNT, Integer TB_YR, Integer TB_AD_CMPNY)
    	throws CreateException { }
    
    public void ejbPostCreate(String TB_NAME, String TB_DESC, 
    		double TB_QNTTY_JAN, double TB_QNTTY_FEB, double TB_QNTTY_MRCH, 
    		double TB_QNTTY_APRL, double TB_QNTTY_MAY, double TB_QNTTY_JUN, 
    		double TB_QNTTY_JUL, double TB_QNTTY_AUG, double TB_QNTTY_SEP, 
    		double TB_QNTTY_OCT, double TB_QNTTY_NOV, double TB_QNTTY_DEC,
    		double TB_AMNT, double TB_TTL_AMNT, Integer TB_YR, Integer TB_AD_CMPNY)
		throws CreateException { }
}