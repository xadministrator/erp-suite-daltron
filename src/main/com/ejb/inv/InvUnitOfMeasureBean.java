/*
 * com/ejb/inv/LocalInvUnitOfMeasureBean.java
 *
 * Created on May 20, 2004 03:00 PM
 */

package com.ejb.inv;

import java.util.Collection;

import javax.ejb.CreateException;
import javax.ejb.EJBException;

import com.ejb.ap.LocalApPurchaseOrderLine;
import com.ejb.ap.LocalApPurchaseRequisitionLine;
import com.ejb.ap.LocalApVoucherLineItem;
import com.ejb.ar.LocalArInvoiceLineItem;
import com.ejb.ar.LocalArSalesOrderLine;
import com.util.AbstractEntityBean;
import com.util.Debug;
	
/**
 *
 * @author  Enrico C. Yap
 *
 * @ejb:bean name="InvUnitOfMeasureEJB"
 *           display-name="Unit Of Measure Entity"
 *           type="CMP"
 *           cmp-version="2.x"
 *           view-type="local"
 *           local-jndi-name="omega-ejb/InvUnitOfMeasureEJB"
 *           schema="InvUnitOfMeasure"
 *           primkey-field="uomCode"
 *
 * @ejb:pk class="java.lang.Integer"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="invuser"
 *                        role-link="invuserlink"
 *
 * @ejb:permission role-name="invuser"
 *
 * @ejb:interface local-class="com.ejb.inv.LocalInvUnitOfMeasure"
 *                local-extends="javax.ejb.EJBLocalObject"
 *
 * @ejb:home local-class="com.ejb.inv.LocalInvUnitOfMeasureHome"
 *           local-extends="javax.ejb.EJBLocalHome"
 * 
 * @ejb:finder signature="Collection findUomAll(java.lang.Integer UOM_AD_CMPNY)"
 *             query="SELECT OBJECT(uom) FROM InvUnitOfMeasure uom WHERE uom.uomAdCompany = ?1"
 * 
 * @jboss:query signature="Collection findUomAll(java.lang.Integer UOM_AD_CMPNY)"
 *             query="SELECT OBJECT(uom) FROM InvUnitOfMeasure uom WHERE uom.uomAdCompany = ?1 ORDER BY uom.uomAdLvClass, uom.uomName"
 * 
 * @ejb:finder signature="Collection findEnabledUomAll(java.lang.Integer UOM_AD_CMPNY)"
 *             query="SELECT OBJECT(uom) FROM InvUnitOfMeasure uom WHERE uom.uomEnable = 1 AND uom.uomAdCompany = ?1"
 *
 * @jboss:query signature="Collection findEnabledUomAll(java.lang.Integer UOM_AD_CMPNY)"
 *             query="SELECT OBJECT(uom) FROM InvUnitOfMeasure uom WHERE uom.uomEnable = 1 AND uom.uomAdCompany = ?1 ORDER BY uom.uomName"
 * 
 * @ejb:finder signature="LocalInvUnitOfMeasure findByUomName(java.lang.String UOM_NM, java.lang.Integer UOM_AD_CMPNY)"
 *             query="SELECT OBJECT(uom) FROM InvUnitOfMeasure uom WHERE uom.uomName=?1 AND uom.uomAdCompany = ?2"
 * 
 * @ejb:finder signature="LocalUnitOfMeasure findByUomShortName(java.lang.String UOM_SHRT_NM, java.lang.Integer UOM_AD_CMPNY)"
 *             query="SELECT OBJECT(uom) FROM InvUnitOfMeasure uom WHERE uom.uomShortName = ?1 AND uom.uomAdCompany = ?2"
 * 
 * @ejb:finder signature="LocalUnitOfMeasure findByUomAdLvClassAndBaseUnitEqualsTrue(java.lang.String UOM_AD_LV_CLSS, java.lang.Integer UOM_AD_CMPNY)"
 *             query="SELECT OBJECT(uom) FROM InvUnitOfMeasure uom WHERE uom.uomAdLvClass = ?1 AND uom.uomBaseUnit = 1 AND uom.uomAdCompany = ?2"
 *
 * @ejb:finder signature="Collection findByUomAdLvClass(java.lang.String UOM_AD_LV_CLSS, java.lang.Integer UOM_AD_CMPNY)"
 *             query="SELECT OBJECT(uom) FROM InvUnitOfMeasure uom WHERE uom.uomAdLvClass=?1 AND uom.uomAdCompany = ?2 ORDER BY uom.uomName"
 *
 * @jboss:query signature="Collection findByUomAdLvClass(java.lang.String UOM_AD_LV_CLSS, java.lang.Integer UOM_AD_CMPNY)"
 *             query="SELECT OBJECT(uom) FROM InvUnitOfMeasure uom WHERE uom.uomAdLvClass=?1 AND uom.uomAdCompany = ?2 ORDER BY uom.uomName"
 * 
 * @ejb:finder signature="Collection findByUomCodeAndUomDownloadStatus(java.lang.Integer UOM_CODE, char UOM_DWNLD_STATUS, java.lang.Integer UOM_AD_CMPNY)"
 *             query="SELECT OBJECT(uom) FROM InvUnitOfMeasure uom WHERE uom.uomCode = ?1 AND uom.uomDownloadStatus = ?2 AND uom.uomAdCompany = ?3"
 *
 * @ejb:finder signature="Collection findEnabledUomByUomNewAndUpdated(java.lang.Integer UOM_AD_CMPNY, char NEW, char UPDATED, char DOWNLOADED_UPDATED)"
 *             query="SELECT OBJECT(uom) FROM InvUnitOfMeasure uom WHERE uom.uomEnable = 1 AND uom.uomAdCompany = ?1 AND (uom.uomDownloadStatus = ?2 OR uom.uomDownloadStatus = ?3 OR uom.uomDownloadStatus = ?4)"
 * 
 * @ejb:finder signature="LocalUnitOfMeasure findByUomNameAndUomAdLvClass(java.lang.String UOM_NM, java.lang.String UOM_AD_LV_CLSS, java.lang.Integer UOM_AD_CMPNY)"
 *             query="SELECT OBJECT(uom) FROM InvUnitOfMeasure uom WHERE uom.uomName = ?1 AND uom.uomAdLvClass = ?2 AND uom.uomAdCompany = ?3"
 * 
 * @ejb:value-object match="*"
 *             name="InvUnitOfMeasure"
 *
 * @jboss:persistence table-name="INV_UNT_OF_MSR"
 *
 * @jboss:entity-command name="mysql-get-generated-keys"
 */

public abstract class InvUnitOfMeasureBean extends AbstractEntityBean {
    
    
    // PERSISTENT METHODS
    
    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     * @ejb:pk-field
     *
     * @jboss:column-name name="UOM_CODE"
     *
     * @jboss:persistence auto-increment="true"
     **/
    public abstract Integer getUomCode();         
    public abstract void setUomCode(Integer UOM_CODE);
     
    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="UOM_NM"
     **/
     public abstract String getUomName();
     /**
      * @ejb:interface-method view-type="local"
      **/
     public abstract void setUomName(String UOM_NM);

    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="UOM_DESC"
     **/
     public abstract String getUomDescription();
     /**
      * @ejb:interface-method view-type="local"
      **/
     public abstract void setUomDescription(String UOM_DESC);
     
	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="UOM_SHRT_NM"
	 **/
	 public abstract String getUomShortName();
	 /**
	  * @ejb:interface-method view-type="local"
	  **/
	 public abstract void setUomShortName(String UOM_SHRT_NM);
      
    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="UOM_AD_LV_CLSS"
     **/
     public abstract String getUomAdLvClass();
     /**
      * @ejb:interface-method view-type="local"
      **/
     public abstract void setUomAdLvClass(String UOM_AD_LV_CLSS);
              
    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="UOM_CNVRSN_FCTR"
     **/
     public abstract double getUomConversionFactor();
     /**
      * @ejb:interface-method view-type="local"
      **/
     public abstract void setUomConversionFactor(double UOM_CNVRSN_FCTR);
     
     /**
      * @ejb:persistent-field
      * @ejb:interface-method view-type="local"
      *
      * @jboss:column-name name="UOM_BS_UNT"
      **/
      public abstract byte getUomBaseUnit();
      /**
       * @ejb:interface-method view-type="local"
       **/
      public abstract void setUomBaseUnit(byte UOM_BS_UNT);
     
    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     *
     * @jboss:column-name name="UOM_ENBL"
     **/
     public abstract byte getUomEnable();
     /**
      * @ejb:interface-method view-type="local"
      **/
     public abstract void setUomEnable(byte UOM_ENBL);
     
     /**
      * @ejb:persistent-field
      * @ejb:interface-method view-type="local"
      *
      * @jboss:column-name name="UOM_DWNLD_STATUS"
      **/
     public abstract char getUomDownloadStatus();
     /**
      * @ejb:interface-method view-type="local"
      **/
     public abstract void setUomDownloadStatus(char UOM_DWNLD_STATUS);
 	
     /**
      * @ejb:persistent-field
      * @ejb:interface-method view-type="local"
      *
      * @jboss:column-name name="UOM_AD_CMPNY"
      **/
      public abstract Integer getUomAdCompany();
      /**
       * @ejb:interface-method view-type="local"
       **/
      public abstract void setUomAdCompany(Integer UOM_AD_CMPNY);
     
     
    // RELATIONSHIP METHODS    

     /**
      * @ejb:interface-method view-type="local"
      * @ejb:relation name="unitofmeasure-items"
      *               role-name="unitofmeasure-has-many-items"
      * 
      */
     public abstract Collection getInvItems();
     public abstract void setInvItems(Collection invItems);

     /**
      * @ejb:interface-method view-type="local"
      * @ejb:relation name="unitofmeasure-buildunbuildassemblylines"
      *               role-name="unitofmeasure-has-many-buildunbuildassemblylines"
      */
     public abstract Collection getInvBuildUnbuildAssemblyLines();
     public abstract void setInvBuildUnbuildAssemblyLines(Collection buildUnbuildAssemblyLines);
     
     /**
      * @ejb:interface-method view-type="local"
      * @ejb:relation name="unitofmeasure-adjustmentlines"
      *               role-name="unitofmeasure-has-many-adjusmentlines;"
      */
     public abstract Collection getInvAdjustmentLines();
     public abstract void setInvAdjustmentLines(Collection invAdjustmentLines);
     
     /**
      * @ejb:interface-method view-type="local"
      * @ejb:relation name="unitofmeasure-voucherlineitems"
      *               role-name="unitofmeasure-has-many-voucherlineitems;"
      */
     public abstract Collection getApVoucherLineItems();
     public abstract void setApVoucherLineItems(Collection apVoucherLineItems);
     
     /**
      * @ejb:interface-method view-type="local"
      * @ejb:relation name="unitofmeasure-invoicelineitems"
      *               role-name="unitofmeasure-has-many-invoicelineitems;"
      */
     public abstract Collection getArInvoiceLineItems();
     public abstract void setArInvoiceLineItems(Collection arInvoiceLineItems);
     
     /**
      * @ejb:interface-method view-type="local"
      * @ejb:relation name="unitofmeasure-overheadmemolines"
      *               role-name="unitofmeasure-has-many-overheadmemolines;"
      */
     public abstract Collection getInvOverheadMemoLines();
     public abstract void setInvOverheadMemoLines(Collection invOverheadMemoLines);
     
     /**
      * @ejb:interface-method view-type="local"
      * @ejb:relation name="unitofmeasure-overheadlines"
      *               role-name="unitofmeasure-has-many-overheadlines;"
      */
     public abstract Collection getInvOverheadLines();
     public abstract void setInvOverheadLines(Collection invOverheadLines);
     
     /**
      * @ejb:interface-method view-type="local"
      * @ejb:relation name="unitofmeasure-purchaseorderlines"
      *               role-name="unitofmeasure-has-many-purchaseorderlines;"
      */
     public abstract Collection getApPurchaseOrderLines();
     public abstract void setApPurchaseOrderLines(Collection apPurchaseOrderLines);
     
     /**
      * @ejb:interface-method view-type="local"
      * @ejb:relation name="unitofmeasure-transactionalbudget"
      *               role-name="unitofmeasure-has-many-transactionalbudget;"
      */
     public abstract Collection getInvTransactionalBudget();
     public abstract void setInvTransactionalBudget(Collection invTransactionalBudget);
     /**
      * @ejb:interface-method view-type="local"
      * @ejb:relation name="unitofmeasure-stocktransferlines"
      *               role-name="unitofmeasure-has-many-stocktransferlines;"
      */
     public abstract Collection getInvStockTransferLines();
     public abstract void setInvStockTransferLines(Collection invStockTransferLines);
     
     /**
      * @ejb:interface-method view-type="local"
      * @ejb:relation name="unitofmeasure-salesorderline"
      *               role-name="unitofmeasure-has-many-salesorderline"
      */
     public abstract Collection getArSalesOrderLines();
     public abstract void setArSalesOrderLines(Collection arSalesOrderLines);
     
     /**
      * @ejb:interface-method view-type="local"
      * @ejb:relation name="unitofmeasure-purchaserequisitionlines"
      *               role-name="unitofmeasure-has-many-purchaserequisitionlines"
      */
     public abstract Collection getApPurchaseRequisitionLines();
     public abstract void setApPurchaseRequisitionLines(Collection apPurchaseRequisitionLines);
     
     /**
      * @ejb:interface-method view-type="local"
      * @ejb:relation name="unitofmeasure-joborderlines"
      *               role-name="unitofmeasure-has-many-joborderlines"
      */
     public abstract Collection getArJobOrderLines();
     public abstract void setArJobOrderLines(Collection arJobOrderLines);
     
     /**
      * @ejb:interface-method view-type="local"
      * @ejb:relation name="unitofmeasure-branchstocktransferlines"
      *               role-name="unitofmeasure-has-many-branchstocktransferlines;"
      */
     public abstract Collection getInvBranchStockTransferLines();
     public abstract void setInvBranchStockTransferLines(Collection invBranchStockTransferLines);

	 /**
      * @ejb:interface-method view-type="local"
      * @ejb:relation name="unitofmeasure-unitofmeasureconversions"
      *               role-name="unitofmeasure-has-many-unitofmeasureconversions"
      */
     public abstract Collection getInvUnitOfMeasureConversions();
     public abstract void setInvUnitOfMeasureConversions(Collection invUnitOfMeasureConversions);
 
     /**
 	 * @ejb:interface-method view-type="local"
 	 * @ejb:relation name="unitofmeasure-lineitems"
 	 *               role-name="unitofmeasure-has-many-lineitems"
 	 */
 	public abstract Collection getInvLineItems();
 	public abstract void setInvLineItems(Collection invLineItems);
 	
 	/**
	  * @ejb:interface-method view-type="local"
	  * @ejb:relation name="unitofmeasure-physicalinventorylines"
	  *               role-name="unitofmeasure-has-many-physicalinventorylines"
	  */
	   public abstract Collection getInvPhysicalInventoryLines();
	   public abstract void setInvPhysicalInventoryLines(Collection invPhysicalInventoryLines);

	  /**
	   * @ejb:interface-method view-type="local"
	   * @ejb:relation name="unitofmeasure-stockissuancelines"
	   *               role-name="unitofmeasure-has-many-stockissuancelines"
	   */
	   public abstract Collection getInvStockIssuanceLines();
	   public abstract void setInvStockIssuanceLines(Collection invStockIssuanceLines);

	   /**
	    * @ejb:interface-method view-type="local"
	    * @ejb:relation name="unitofmeasure-billofmaterials"
	    *               role-name="unitofmeasure-has-many-billofmaterials"
	    */
	   public abstract Collection getInvBillOfMaterials();
	   public abstract void setInvBillOfMaterials(Collection invBillOfMaterials);

   // BUSINESS METHODS
   
     /**
      * @ejb:interface-method view-type="local"
      **/
     public void addInvItem(LocalInvItem invItem) {

          Debug.print("InvUnitOfMeasureBean addInvItem");
      
          try {
      	
             Collection invItems = getInvItems();
             invItems.add(invItem);
	          
          } catch (Exception ex) {
      	
            throw new EJBException(ex.getMessage());
      
          }   
     }

     /**
      * @ejb:interface-method view-type="local"
      **/
     public void dropInvItem(LocalInvItem invItem) {

          Debug.print("InvUnitOfMeasureBean dropInvItem");
      
          try {
      	
             Collection invItems = getInvItems();
             invItems.remove(invItem);
	      
          } catch (Exception ex) {
      	
            throw new EJBException(ex.getMessage());
         
          }
             
     }
     
	 /**
	  * @ejb:interface-method view-type="local"
	  **/
	 public void addInvBuildUnbuildAssemblyLine(LocalInvBuildUnbuildAssemblyLine invBuildUnbuildAssemblyLine) {
	
	      Debug.print("InvUnitOfMeasureBean addInvBuildUnbuildAssemblyLine");
	  
	      try {
	  	
	      Collection invBuildUnbuildAssemblyLines = getInvBuildUnbuildAssemblyLines();
	      invBuildUnbuildAssemblyLines.add(invBuildUnbuildAssemblyLine);
		     
	      } catch (Exception ex) {
	  	
	          throw new EJBException(ex.getMessage());
	     
	      }
	      
	 }
	
     /**
      * @ejb:interface-method view-type="local"
      **/ 
     public void dropInvBuildUnbuildAssemblyLine(LocalInvBuildUnbuildAssemblyLine invBuildUnbuildAssemblyLine) {
		
		  Debug.print("InvUnitOfMeasureBean dropInvBuildUnbuildAssemblyLine");
		  
		  try {
		  	
		     Collection invBuildUnbuildAssemblyLines = getInvBuildUnbuildAssemblyLines();
		     invBuildUnbuildAssemblyLines.remove(invBuildUnbuildAssemblyLine);
			     
		  } catch (Exception ex) {
		  	
		     throw new EJBException(ex.getMessage());
		     
		  }
		  
     }
     
     /**
      * @ejb:interface-method view-type="local"
      **/
     public void addInvAdjustmentLine(LocalInvAdjustmentLine invAdjustmentLine) {

          Debug.print("InvUnitOfMeasureBean addInvAdjustmentLine");
      
          try {
      	
             Collection invAdjustmentLines = getInvAdjustmentLines();
             invAdjustmentLines.add(invAdjustmentLine);
	          
          } catch (Exception ex) {
      	
            throw new EJBException(ex.getMessage());
      
          }   
     }

     /**
      * @ejb:interface-method view-type="local"
      **/
     public void dropInvAdjustmentLine(LocalInvAdjustmentLine invAdustmentLine) {

          Debug.print("InvUnitOfMeasureBean dropInvAdjustmentLine");
      
          try {
      	
             Collection invAdjustmentLines = getInvAdjustmentLines();
             invAdjustmentLines.remove(invAdustmentLine);
	      
          } catch (Exception ex) {
      	
            throw new EJBException(ex.getMessage());
         
          }
             
     }
     
     /**
      * @ejb:interface-method view-type="local"
      **/
     public void addApVoucherLineItem(LocalApVoucherLineItem apVoucherLineItem) {

          Debug.print("InvUnitOfMeasureBean addApVoucherLineItem");
      
          try {
      	
             Collection apVoucherLineItems = getApVoucherLineItems();
             apVoucherLineItems.add(apVoucherLineItem);
	          
          } catch (Exception ex) {
      	
            throw new EJBException(ex.getMessage());
      
          }   
     }

     /**
      * @ejb:interface-method view-type="local"
      **/
     public void dropApVoucherLineItem(LocalApVoucherLineItem apVoucherLineItem) {

          Debug.print("InvUnitOfMeasureBean dropApVoucherLineItem");
      
          try {
      	
             Collection apVoucherLineItems = getApVoucherLineItems();
             apVoucherLineItems.remove(apVoucherLineItem);
	      
          } catch (Exception ex) {
      	
            throw new EJBException(ex.getMessage());
         
          }
             
     }
     
     /**
      * @ejb:interface-method view-type="local"
      **/
     public void addArInvoiceLineItem(LocalArInvoiceLineItem arInvoiceLineItem) {

          Debug.print("InvUnitOfMeasureBean addArInvoiceLineItem");
      
          try {
      	
             Collection arInvoiceLineItems = getArInvoiceLineItems();
             	arInvoiceLineItems.add(arInvoiceLineItem);
	          
          } catch (Exception ex) {
      	
            throw new EJBException(ex.getMessage());
      
          }   
     }

     /**
      * @ejb:interface-method view-type="local"
      **/
     public void dropArInvoiceLineItem(LocalArInvoiceLineItem arInvoiceLineItem) {

          Debug.print("InvUnitOfMeasureBean dropArInvoiceLineItem");
      
          try {
      	
             Collection arInvoiceLineItems = getArInvoiceLineItems();
             	arInvoiceLineItems.remove(arInvoiceLineItem);
	      
          } catch (Exception ex) {
      	
            throw new EJBException(ex.getMessage());
         
          }
             
     }
     
     /**
      * @ejb:interface-method view-type="local"
      **/
     public void addInvOverheadMemoLine(LocalInvOverheadMemoLine invOverheadMemoLine) {

          Debug.print("InvUnitOfMeasureBean addInvOverheadMemoLine");
      
          try {
      	
             Collection invOverheadMemoLines = getInvOverheadMemoLines();
             	invOverheadMemoLines.add(invOverheadMemoLine);
	          
          } catch (Exception ex) {
      	
            throw new EJBException(ex.getMessage());
      
          }   
     }

     /**
      * @ejb:interface-method view-type="local"
      **/
     public void dropInvOverheadMemoLine(LocalInvOverheadMemoLine invOverheadMemoLine) {

          Debug.print("InvUnitOfMeasureBean dropInvOverheadMemoLine");
      
          try {
      	
             Collection invOverheadMemoLines = getInvOverheadMemoLines();
             	invOverheadMemoLines.remove(invOverheadMemoLine);
	      
          } catch (Exception ex) {
      	
            throw new EJBException(ex.getMessage());
         
          }
             
     }
     
     /**
      * @ejb:interface-method view-type="local"
      **/
     public void addInvOverheadLine(LocalInvOverheadLine invOverheadLine) {

          Debug.print("InvUnitOfMeasureBean addInvOverheadLine");
      
          try {
      	
             Collection invOverheadLines = getInvOverheadLines();
             	invOverheadLines.add(invOverheadLine);
	          
          } catch (Exception ex) {
      	
            throw new EJBException(ex.getMessage());
      
          }   
     }

     /**
      * @ejb:interface-method view-type="local"
      **/
     public void dropInvOverheadLine(LocalInvOverheadMemoLine invOverheadLine) {

          Debug.print("InvUnitOfMeasureBean dropInvOverheadLine");
      
          try {
      	
             Collection invOverheadLines = getInvOverheadLines();
             	invOverheadLines.remove(invOverheadLine);
	      
          } catch (Exception ex) {
      	
            throw new EJBException(ex.getMessage());
         
          }
             
     }
     
     /**
      * @ejb:interface-method view-type="local"
      **/
     public void addApPurchaseOrderLine(LocalApPurchaseOrderLine apPurchaseOrderLine) {

          Debug.print("InvUnitOfMeasureBean addApPurchaseOrderLine");
      
          try {
      	
             Collection apPurchaseOrderLines = getApPurchaseOrderLines();
             apPurchaseOrderLines.add(apPurchaseOrderLine);
	          
          } catch (Exception ex) {
      	
            throw new EJBException(ex.getMessage());
      
          }   
     }

     /**
      * @ejb:interface-method view-type="local"
      **/
     public void dropApPurchaseOrderLine(LocalApPurchaseOrderLine apPurchaseOrderLine) {

          Debug.print("InvUnitOfMeasureBean dropApPurchaseOrderLine");
      
          try {
      	
             Collection apPurchaseOrderLines = getApPurchaseOrderLines();
             apPurchaseOrderLines.remove(apPurchaseOrderLine);
	      
          } catch (Exception ex) {
      	
            throw new EJBException(ex.getMessage());
         
          }
             
     } 
     
     /**
      * @ejb:interface-method view-type="local"
      **/
     public void addInvStockTransferLine(LocalInvStockTransferLine invStockTransferLine) {

          Debug.print("InvUnitOfMeasureBean addInvStockTransferLine");
      
          try {
      	
             Collection invStockTransferLines = getInvStockTransferLines();
             invStockTransferLines.add(invStockTransferLine);
	          
          } catch (Exception ex) {
      	
            throw new EJBException(ex.getMessage());
      
          }   
     }

     /**
      * @ejb:interface-method view-type="local"
      **/
     public void dropInvStockTransferLine(LocalInvStockTransferLine invStockTransferLine) {

          Debug.print("InvUnitOfMeasureBean dropInvStockTransferLine");
      
          try {
      	
             Collection invStockTransferLines = getInvStockTransferLines();
             invStockTransferLines.remove(invStockTransferLine);
	      
          } catch (Exception ex) {
      	
            throw new EJBException(ex.getMessage());
         
          }
             
     }
     
     /**
	   * @ejb:interface-method view-type="local"
	   **/
	  public void addArSalesOrderLine(LocalArSalesOrderLine arSalesOrderLine) {
	
	       Debug.print("InvUnitOfMeasureBean addArSalesOrderLine");
	  
	       try {
	  	
	  	       Collection arSalesOrderLines = getArSalesOrderLines();
	  	       arSalesOrderLines.add(arSalesOrderLine);
		     
	       } catch (Exception ex) {
	  	
	           throw new EJBException(ex.getMessage());
	     
	       }
	      
	  }
	
     /**
      * @ejb:interface-method view-type="local"
      **/ 
     public void dropArSalesOrderLine(LocalArSalesOrderLine arSalesOrderLine) {
		
         Debug.print("InvUnitOfMeasureBean dropArSalesOrderLine");
		  
		  try {
		  	
		     Collection arSalesOrderLines = getArSalesOrderLines();
		     arSalesOrderLines.remove(arSalesOrderLine);
			     
		  } catch (Exception ex) {
		  	
		     throw new EJBException(ex.getMessage());
		     
		  }
		  
    }
    
    
    /**
      * @ejb:interface-method view-type="local"
      **/
     public void addApPurchaseRequisitionLine(LocalApPurchaseRequisitionLine apPurchaseRequisitionLine) {

          Debug.print("InvUnitOfMeasureBean addApPurchaseRequisitionLine");
      
          try {
      	
             Collection apPurchaseRequisitionLines = getApPurchaseRequisitionLines();
             apPurchaseRequisitionLines.add(apPurchaseRequisitionLine);
	          
          } catch (Exception ex) {
      	
            throw new EJBException(ex.getMessage());
      
          }   
     }

     /**
      * @ejb:interface-method view-type="local"
      **/
     public void dropApPurchaseRequisitionLine(LocalApPurchaseRequisitionLine apPurchaseRequisitionLine) {

          Debug.print("InvUnitOfMeasureBean dropApPurchaseRequisitionLine");
      
          try {
      	
             Collection apPurchaseRequisitionLines = getApPurchaseRequisitionLines();
             apPurchaseRequisitionLines.remove(apPurchaseRequisitionLine);
	      
          } catch (Exception ex) {
      	
            throw new EJBException(ex.getMessage());
         
          }
             
     } 
     
     /**
      * @ejb:interface-method view-type="local"
      **/
     public void addInvBranchStockTransferLine(LocalInvBranchStockTransferLine invBranchStockTransferLine) {

          Debug.print("InvUnitOfMeasureBean addInvBranchStockTransferLine");
      
          try {
      	
             Collection invBranchStockTransferLines = getInvBranchStockTransferLines();
             invBranchStockTransferLines.add(invBranchStockTransferLine);
	          
          } catch (Exception ex) {
      	
            throw new EJBException(ex.getMessage());
      
          }   
     }
     
     /**
      * @ejb:interface-method view-type="local"
      **/
     public void addInvBillOfMaterial(LocalInvBillOfMaterial invBillOfMaterial) {

          Debug.print("InvUnitOfMeasureBean addInvBillOfMaterial");
      
          try {
      	
             Collection invBillOfMaterials = getInvBillOfMaterials();
             invBillOfMaterials.add(invBillOfMaterial);
	          
          } catch (Exception ex) {
      	
            throw new EJBException(ex.getMessage());
      
          }   
     }
     
     /**
      * @ejb:interface-method view-type="local"
      **/
     public void dropInvBillOfMaterial(LocalInvBillOfMaterial invBillOfMaterial) {

          Debug.print("InvUnitOfMeasureBean addInvBillOfMaterial");
      
          try {
      	
             Collection invBillOfMaterials = getInvBillOfMaterials();
             invBillOfMaterials.remove(invBillOfMaterial);
	          
          } catch (Exception ex) {
      	
            throw new EJBException(ex.getMessage());
      
          }   
     }
     
     /**
      * @ejb:interface-method view-type="local"
      **/
     public void addInvStockIssuanceLine(LocalInvStockIssuanceLine invStockIssuanceLine) {

          Debug.print("InvUnitOfMeasureBean addInvStockIssuanceLine");
      
          try {
      	
             Collection invStockIssuanceLines = getInvStockIssuanceLines();
             invStockIssuanceLines.add(invStockIssuanceLine);
	          
          } catch (Exception ex) {
      	
            throw new EJBException(ex.getMessage());
      
          }   
     }
     
     /**
      * @ejb:interface-method view-type="local"
      **/
     public void dropInvStockIssuanceLine(LocalInvStockIssuanceLine invStockIssuanceLine) {

          Debug.print("InvUnitOfMeasureBean addInvStockIssuanceLine");
      
          try {
      	
             Collection invStockIssuanceLines = getInvStockIssuanceLines();
             invStockIssuanceLines.remove(invStockIssuanceLine);
	          
          } catch (Exception ex) {
      	
            throw new EJBException(ex.getMessage());
      
          }   
     }
     
     /**
      * @ejb:interface-method view-type="local"
      **/
     public void addInvPhysicalInventoryLine(LocalInvPhysicalInventoryLine invPhysicalInventoryLine) {

          Debug.print("InvUnitOfMeasureBean addInvPhysicalInventoryLine");
      
          try {
      	
             Collection invPhysicalInventoryLines = getInvPhysicalInventoryLines();
             invPhysicalInventoryLines.add(invPhysicalInventoryLine);
	          
          } catch (Exception ex) {
      	
            throw new EJBException(ex.getMessage());
      
          }   
     }
     
     /**
      * @ejb:interface-method view-type="local"
      **/
     public void dropInvPhysicalInventoryLine(LocalInvPhysicalInventoryLine invPhysicalInventoryLine) {

          Debug.print("InvUnitOfMeasureBean addInvPhysicalInventoryLine");
      
          try {
      	
             Collection invPhysicalInventoryLines = getInvPhysicalInventoryLines();
             invPhysicalInventoryLines.remove(invPhysicalInventoryLine);
	          
          } catch (Exception ex) {
      	
            throw new EJBException(ex.getMessage());
      
          }   
     }

     /**
      * @ejb:interface-method view-type="local"
      **/
     public void dropInvBranchStockTransferLine(LocalInvBranchStockTransferLine invBranchStockTransferLine) {

          Debug.print("InvUnitOfMeasureBean dropInvBranchStockTransferLine");
      
          try {
      	
             Collection invBranchStockTransferLines = getInvBranchStockTransferLines();
             invBranchStockTransferLines.remove(invBranchStockTransferLine);
	      
          } catch (Exception ex) {
      	
            throw new EJBException(ex.getMessage());
         
          }
             
     }
     
     /**
	   * @ejb:interface-method view-type="local"
	   **/
	  public void addInvUnitOfMeasureConversion(LocalInvUnitOfMeasureConversion invUnitOfMeasureConversion) {
	
	       Debug.print("InvUnitOfMeasureConversionBean addInvUnitOfMeasureConversion");
	  
	       try {
	  	
	  	       Collection invUnitOfMeasureConversions = getInvUnitOfMeasureConversions();
	  	       invUnitOfMeasureConversions.add(invUnitOfMeasureConversion);
		     
	       } catch (Exception ex) {
	  	
	           throw new EJBException(ex.getMessage());
	     
	       }
	      
	  }
	
    /**
     * @ejb:interface-method view-type="local"
     **/ 
    public void dropInvUnitOfMeasureConversion(LocalInvUnitOfMeasureConversion invUnitOfMeasureConversion) {
		
        Debug.print("InvUnitOfMeasureConversionBean dropInvUnitOfMeasureConversion");
		  
		  try {
		  	
		     Collection invUnitOfMeasureConversions = getInvUnitOfMeasureConversions();
		     invUnitOfMeasureConversions.remove(invUnitOfMeasureConversion);
			     
		  } catch (Exception ex) {
		  	
		     throw new EJBException(ex.getMessage());
		     
		  }
		  
   }
    
    /**
	 * @ejb:interface-method view-type="local"
	 **/
	public void addInvLineItem(LocalInvLineItem invLineItem) {
		
		Debug.print("InvUnitOfMeasureBean addInvLineItem");
		
		try {
			
			Collection invLineItems = getInvLineItems();
			invLineItems.add(invLineItem);
			
		} catch (Exception ex) {
			
			throw new EJBException(ex.getMessage());
			
		}
		
	}	   
	
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public void dropInvLineItem(LocalInvLineItem invLineItem) {
		
		Debug.print("InvUnitOfMeasureBean dropInvLineItem");
		
		try {
			
			Collection invLineItems = getInvLineItems();
			invLineItems.remove(invLineItem);
			
		} catch (Exception ex) {
			
			throw new EJBException(ex.getMessage());
			
		}
		
	}
     
     // ENTITY METHODS
    
     /**
      * @ejb:create-method view-type="local"
      **/
     public Integer ejbCreate(Integer UOM_CODE, String UOM_NM, 
         String UOM_DESC, String UOM_SHRT_NM, String UOM_AD_LV_CLSS, double UOM_CNVRSN_FCTR, 
         byte UOM_BS_UNT, byte UOM_ENBL, char UOM_DWNLD_STATUS, Integer UOM_AD_CMPNY)
         throws CreateException {
           
         Debug.print("InvUnitOfMeasureBean ejbCreate");
        
         setUomCode(UOM_CODE);
         setUomName(UOM_NM);
         setUomDescription(UOM_DESC);
         setUomShortName(UOM_SHRT_NM);
         setUomAdLvClass(UOM_AD_LV_CLSS);
		 setUomConversionFactor(UOM_CNVRSN_FCTR);
         setUomBaseUnit(UOM_BS_UNT);
         setUomEnable(UOM_ENBL);
         setUomDownloadStatus(UOM_DWNLD_STATUS);
         setUomAdCompany(UOM_AD_CMPNY);
        
         return null;
        
     }
    
     /**
      * @ejb:create-method view-type="local"
      **/
     public Integer ejbCreate(String UOM_NM, String UOM_DESC, 
     		String UOM_SHRT_NM, String UOM_AD_LV_CLSS, double UOM_CNVRSN_FCTR, 
	        byte UOM_BS_UNT, byte UOM_ENBL, char UOM_DWNLD_STATUS, Integer UOM_AD_CMPNY)
         throws CreateException {
           
         Debug.print("InvUnitOfMeasureBean ejbCreate");
               
         setUomName(UOM_NM);
         setUomDescription(UOM_DESC);
         setUomShortName(UOM_SHRT_NM);
         setUomAdLvClass(UOM_AD_LV_CLSS);
		 setUomConversionFactor(UOM_CNVRSN_FCTR);
         setUomBaseUnit(UOM_BS_UNT);
         setUomEnable(UOM_ENBL);
         setUomDownloadStatus(UOM_DWNLD_STATUS);
         setUomAdCompany(UOM_AD_CMPNY);
        
         return null;
        
     }
    
    
     public void ejbPostCreate(Integer UOM_CODE, String UOM_NM, 
            String UOM_DESC, String UOM_SHRT_NM, String UOM_AD_LV_CLSS, double UOM_CNVRSN_FCTR, 
	         byte UOM_BS_UNT, byte UOM_ENBL, char UOM_DWNLD_STATUS, Integer UOM_AD_CMPNY)
         throws CreateException { }
   
     public void ejbPostCreate(String UOM_NM, String UOM_DESC, 
     		String UOM_SHRT_NM, String UOM_AD_LV_CLSS, double UOM_CNVRSN_FCTR, 
	        byte UOM_BS_UNT, byte UOM_ENBL, char UOM_DWNLD_STATUS, Integer UOM_AD_CMPNY)
         throws CreateException { }
        
}

