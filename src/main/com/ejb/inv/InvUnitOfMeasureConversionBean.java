/*
 * com/ejb/inv/LocalInvUnitOfMeasureBean.java
 *
 * Created on May 20, 2004 03:00 PM
 */

package com.ejb.inv;

import javax.ejb.CreateException;

import com.util.AbstractEntityBean;
import com.util.Debug;
	
/**
 *
 * @author  Rey B.Limosenero
 *
 * @ejb:bean name="InvUnitOfMeasureConversionEJB"
 *           display-name="Unit Of Measure Conversion Entity"
 *           type="CMP"
 *           cmp-version="2.x"
 *           view-type="local"
 *           local-jndi-name="omega-ejb/InvUnitOfMeasureConversionEJB"
 *           schema="InvUnitOfMeasureConversion"
 *           primkey-field="umcCode"
 *
 * @ejb:pk class="java.lang.Integer"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="invuser"
 *                        role-link="invuserlink"
 *
 * @ejb:permission role-name="invuser"
 *
 * @ejb:interface local-class="com.ejb.inv.LocalInvUnitOfMeasureConversion"
 *                local-extends="javax.ejb.EJBLocalObject"
 *
 * @ejb:home local-class="com.ejb.inv.LocalInvUnitOfMeasureConversionHome"
 *           local-extends="javax.ejb.EJBLocalHome"
 * 
 * @ejb:finder signature="Collection findUmcAll(java.lang.Integer UMC_AD_CMPNY)"
 *             query="SELECT OBJECT(umc) FROM InvUnitOfMeasureConversion umc WHERE umc.umcAdCompany = ?1"
 * 
 * @jboss:query signature="Collection findUmcAll(java.lang.Integer UMC_AD_CMPNY)"
 *             query="SELECT OBJECT(umc) FROM InvUnitOfMeasureConversion umc WHERE umc.umcAdCompany = ?1"
 * 
 * @ejb:finder signature="LocalInvUnitOfMeasureConversion findUmcByIiNameAndUomName(java.lang.String II_NM, java.lang.String UOM_NM, java.lang.Integer UMC_AD_CMPNY)"
 *             query="SELECT OBJECT(umc) FROM InvUnitOfMeasureConversion umc WHERE umc.invItem.iiName = ?1 AND umc.invUnitOfMeasure.uomName = ?2 AND umc.umcAdCompany = ?3"
 *
 * @ejb:finder signature="LocalInvUnitOfMeasureConversion findUmcByUmcBaseUnitAndIiName(java.lang.String II_NM, java.lang.Integer UMC_AD_CMPNY)"
 *             query="SELECT OBJECT(umc) FROM InvUnitOfMeasureConversion umc WHERE umc.umcBaseUnit=1 AND umc.invItem.iiName = ?1 AND umc.umcAdCompany = ?2"
 *             
 * @ejb:finder signature="Collection findUmcByUmcNewAndUpdated(java.lang.Integer UOM_AD_CMPNY, char NEW, char UPDATED, char DOWNLOADED_UPDATED)"
 *             query="SELECT OBJECT(umc) FROM InvUnitOfMeasureConversion umc WHERE umc.umcAdCompany = ?1 AND (umc.umcDownloadStatus = ?2 OR umc.umcDownloadStatus = ?3 OR umc.umcDownloadStatus = ?4)"         
 *   
 * @ejb:finder signature="LocalInvUnitOfMeasureConversion findUmcByUomEnableAndByIiNameAndUomName(java.lang.String II_NM, java.lang.String UOM_NM, java.lang.Integer UMC_AD_CMPNY)"
 *             query="SELECT OBJECT(umc) FROM InvUnitOfMeasureConversion umc WHERE umc.invUnitOfMeasure.uomEnable=1 AND umc.invItem.iiName = ?1 AND umc.invUnitOfMeasure.uomName = ?2 AND umc.umcAdCompany = ?3"
 *
 * @ejb:value-object match="*"
 *             name="InvUnitOfMeasureConversion"
 *
 * @jboss:persistence table-name="INV_UNT_OF_MSR_CNVRSN"
 *
 * @jboss:entity-command name="mysql-get-generated-keys"
 */

public abstract class InvUnitOfMeasureConversionBean extends AbstractEntityBean {
    
    
    // PERSISTENT METHODS
    
    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     * @ejb:pk-field
     *
     * @jboss:column-name name="UMC_CODE"
     *
     * @jboss:persistence auto-increment="true"
     **/
    public abstract Integer getUmcCode();         
    public abstract void setUmcCode(Integer UMC_CODE);
     
    /**
     * @ejb:persistent-field
     * @ejb:interface-method view-type="local"
     * @ejb:pk-field
     *
     * @jboss:column-name name="UMC_CNVRSN_FCTR"
     **/
     public abstract double getUmcConversionFactor();
     /**
      * @ejb:interface-method view-type="local"
      **/
     public abstract void setUmcConversionFactor(double UMC_CNVRSN_FCTR);
     
     /**
      * @ejb:persistent-field
      * @ejb:interface-method view-type="local"
      * @ejb:pk-field
      *
      * @jboss:column-name name="UMC_BS_UNT"
      **/
      public abstract byte getUmcBaseUnit();
      /**
       * @ejb:interface-method view-type="local"
       **/
      public abstract void setUmcBaseUnit(byte UMC_BS_UNT);
      
      /**
       * @ejb:persistent-field
       * @ejb:interface-method view-type="local"
       * @ejb:pk-field
       *
       * @jboss:column-name name="UMC_DWNLD_STATUS"
       **/
       public abstract char getUmcDownloadStatus();
       /**
        * @ejb:interface-method view-type="local"
        **/
       public abstract void setUmcDownloadStatus(char UMC_DWNLD_STATUS);

    	
     /**
      * @ejb:persistent-field
      * @ejb:interface-method view-type="local"
      *
      * @jboss:column-name name="UMC_AD_CMPNY"
      **/
      public abstract Integer getUmcAdCompany();
      /**
       * @ejb:interface-method view-type="local"
       **/
      public abstract void setUmcAdCompany(Integer UMC_AD_CMPNY);

     
      // RELATIONSHIP METHODS    
      
      /**
       * @ejb:interface-method view-type="local"
       * @ejb:relation name="item-unitofmeasureconversions"
       *               role-name="unitofmeasureconversion-has-one-item"
       *			   cascade-delete="yes"
       *
       * @jboss:relation related-pk-field="iiCode"
       *                 fk-column="INV_ITEM"
       */
      public abstract LocalInvItem getInvItem();
      /**
       * @ejb:interface-method view-type="local"
       **/
      public abstract void setInvItem(LocalInvItem invItem);
      
      /**
       * @ejb:interface-method view-type="local"
       * @ejb:relation name="unitofmeasure-unitofmeasureconversions"
       *               role-name="unitofmeasureconversion-has-one-unitofmeasure"
       * 			   cascade-delete="yes"
       * 
       * @jboss:relation related-pk-field="uomCode"
       *                 fk-column="INV_UNIT_OF_MEASURE"
       */
      public abstract LocalInvUnitOfMeasure getInvUnitOfMeasure();
      /**
       * @ejb:interface-method view-type="local"
       **/
      public abstract void setInvUnitOfMeasure(LocalInvUnitOfMeasure invUnitOfMeasure);
     
     // BUSINESS METHODS
     
    // ENTITY METHODS
    
     /**
      * @ejb:create-method view-type="local"
      **/
     public Integer ejbCreate(Integer UMC_CODE, double UMC_CNVRSN_FCTR, byte UMC_BS_UNT, Integer UMC_AD_CMPNY)
         throws CreateException {
           
         Debug.print("InvUnitOfMeasureConversionBean ejbCreate");
        
         setUmcCode(UMC_CODE);
         setUmcConversionFactor(UMC_CNVRSN_FCTR);
         setUmcBaseUnit(UMC_BS_UNT);
         setUmcAdCompany(UMC_AD_CMPNY);
         
         
         return null;
        
     }
    
     /**
      * @ejb:create-method view-type="local"
      **/
     public Integer ejbCreate(double UMC_CNVRSN_FCTR, byte UMC_BS_UNT, Integer UMC_AD_CMPNY)
         throws CreateException {
           
         Debug.print("InvUnitOfMeasureConversionBean ejbCreate");
               
         setUmcConversionFactor(UMC_CNVRSN_FCTR);
         setUmcBaseUnit(UMC_BS_UNT);
         setUmcAdCompany(UMC_AD_CMPNY);
         
         return null;
        
     }
    
    
     public void ejbPostCreate(Integer UMC_CODE, double UMC_CNVRSN_FCTR, byte UMC_BS_UNT, Integer UMC_AD_CMPNY)
         throws CreateException { }
   
     public void ejbPostCreate(double UMC_CNVRSN_FCTR, byte UMC_BS_UNT, Integer UMC_AD_CMPNY)
         throws CreateException { }
        
}

