/*
 * com/ejb/pm/LocalHrProjectBean.java
 *
 * Created on February 21, 2003, 12:55 PM
 */

package com.ejb.pm;

import java.util.Collection;
import java.util.Date;

import javax.ejb.CreateException;
import javax.ejb.EJBException;

import com.ejb.ar.LocalArAppliedInvoice;
import com.ejb.ar.LocalArInvoice;
import com.ejb.ar.LocalArInvoiceBatch;
import com.ejb.cm.LocalCmAdjustment;
import com.util.AbstractEntityBean;
import com.util.Debug;

/**
 * @ejb:bean name="PmContractEJB"
 *           display-name="Contract Entity"
 *           type="CMP"
 *           cmp-version="2.x"
 *           view-type="local"
 *           local-jndi-name="omega-ejb/PmContractEJB"
 *           schema="PmContract"
 *           primkey-field="ctrCode"
 *       
 * @ejb:pk class="java.lang.Integer"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="pmuser"
 *                        role-link="pmuserlink"
 *
 * @ejb:permission role-name="pmuser"
 *
 * @ejb:interface local-class="com.ejb.pm.LocalPmContract"
 *                local-extends="javax.ejb.EJBLocalObject"
 *
 * @ejb:home local-class="com.ejb.pm.LocalPmContractHome"
 *           local-extends="javax.ejb.EJBLocalHome"
 *
 * @ejb:finder signature="Collection findCtrAll(java.lang.Integer CTR_AD_CMPNY)"
 *             query="SELECT OBJECT(ctr) FROM PmContract ctr WHERE ctr.ctrAdCompany = ?1"
 *
 * @jboss:query signature="Collection findCtrAll(java.lang.Integer CTR_AD_CMPNY)"
 *             query="SELECT OBJECT(ctr) FROM PmContract ctr WHERE ctr.ctrAdCompany = ?1 ORDER BY ctr.ctrCode"
 * 
 * @ejb:finder signature="LocalPmContract findCtrByReferenceID(java.lang.Integer CTR_REF_ID, java.lang.Integer CTR_AD_CMPNY)"
 *             query="SELECT OBJECT(ctr) FROM PmContract ctr WHERE ctr.ctrReferenceID=?1 AND ctr.ctrAdCompany=?2"
 *                          
 * @ejb:value-object match="*"
 * 						name="PmContract"
 * 
 * @jboss:persistence table-name="PM_CNTRCT"
 *
 * @jboss:entity-command name="mysql-get-generated-keys"
 *
 */

public abstract class PmContractBean extends AbstractEntityBean {


   // Access methods for persistent fields
	
	/**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    * @ejb:pk-field
    *
    * @jboss:column-name name="CTR_CODE"
    *
    * @jboss:persistence auto-increment="true"
    **/
   public abstract java.lang.Integer getCtrCode();
   public abstract void setCtrCode(java.lang.Integer CTR_CODE);
   
   /**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="CTR_RFRNC_ID"
	 **/
	public abstract Integer getCtrReferenceID();
	/**
	 * @ejb:interface-method view-type="local"
	 **/ 
	public abstract void setCtrReferenceID(Integer CTR_RFRNC_ID);

	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"   
	 *
	 * @jboss:column-name name="CTR_CLNT_ID"
	 **/
	public abstract String getCtrClientID();
	/**
	 * @ejb:interface-method view-type="local"
	 **/ 
	public abstract void setCtrClientID(String CTR_CLNT_ID);
	

   
   /**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"   
	 *
	 * @jboss:column-name name="CTR_PRC"
	 **/
	public abstract Double getCtrPrice();
	/**
	 * @ejb:interface-method view-type="local"
	 **/ 
	public abstract void setCtrPrice(Double CTR_PRC);

   
   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="CTR_AD_CMPNY"
    **/
   public abstract Integer getCtrAdCompany();
   /**
    * @ejb:interface-method view-type="local"
    **/
   public abstract void setCtrAdCompany(Integer CTR_AD_CMPNY);
   
   
   // RELATIONSHIP METHODS
   
   /**
	 * @ejb:interface-method view-type="local"
	 * @ejb:relation name="contract-contractterms"
	 *               role-name="contract-has-many-contractterms"
	 * 
	 */
	public abstract Collection getPmContractTerms();
	public abstract void setPmContractTerms(Collection pmContractTerms);
   

	   
	
	/**
	 * @ejb:interface-method view-type="local"
	 * @ejb:relation name="contract-projecttypetypes"
	 *               role-name="contract-has-many-projecttypetypes"
	 * 
	 */
	public abstract Collection getPmProjectTypeTypes();
	public abstract void setPmProjectTypeTypes(Collection pmProjectTypeTypes);
	
	// BUSINESS METHODS
	
	
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public void addPmContractTerm(LocalPmContractTerm pmContractTerm) {
		
		Debug.print("PmContractBean addPmContractTerm");
		
		try {
			
			Collection pmContractTerms = getPmContractTerms();
			pmContractTerms.add(pmContractTerm);
			
		} catch (Exception ex) {
			
			throw new EJBException(ex.getMessage());
			
		}   
	}
	
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public void dropPmContractTerm(LocalPmContractTerm pmContractTerm) {
		
		Debug.print("PmContractBean dropPmContractTerm");
		
		try {
			
			Collection pmContractTerms = getPmContractTerms();
			pmContractTerms.remove(pmContractTerm);
			
		} catch (Exception ex) {
			
			throw new EJBException(ex.getMessage());
			
		}   
	}
	
	
	
	
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public void addPmProjectTypeType(LocalPmProjectTypeType pmProjectTypeType) {
		
		Debug.print("PmContractBean addPmProjectTypeType");
		
		try {
			
			Collection pmProjectTypeTypes = getPmProjectTypeTypes();
			pmProjectTypeTypes.add(pmProjectTypeType);
			
		} catch (Exception ex) {
			
			throw new EJBException(ex.getMessage());
			
		}   
	}
	
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public void dropPmProjectTypeType(LocalPmProjectTypeType pmProjectTypeType) {
		
		Debug.print("PmContractBean dropPmProjectTypeType");
		
		try {
			
			Collection pmProjectTypeTypes = getPmProjectTypeTypes();
			pmProjectTypeTypes.remove(pmProjectTypeType);
			
		} catch (Exception ex) {
			
			throw new EJBException(ex.getMessage());
			
		}   
	}

	
	
   // EntityBean methods

   /**
    * @ejb:create-method view-type="local"
    **/
  public java.lang.Integer ejbCreate (java.lang.Integer CTR_RFRNC_ID, 
		  String CTR_CLNT_ID, Double CTR_PRC, 
		  Integer CTR_AD_CMPNY)
      throws CreateException {

      Debug.print("PmContractBean ejbCreate");
      
      setCtrReferenceID(CTR_RFRNC_ID);
      setCtrClientID(CTR_CLNT_ID);
      setCtrPrice(CTR_PRC);
      setCtrAdCompany(CTR_AD_CMPNY);
      
      return null;
   }

  

   public void ejbPostCreate (java.lang.Integer CTR_RFRNC_ID, 
			  String CTR_CLNT_ID, Double CTR_PRC, 
			  Integer CTR_AD_CMPNY)
      throws CreateException { }



} // HrPayrollPeriodBean class
