/*
 * com/ejb/pm/LocalHrProjectBean.java
 *
 * Created on February 21, 2003, 12:55 PM
 */

package com.ejb.pm;


import java.util.Collection;

import javax.ejb.CreateException;
import com.util.AbstractEntityBean;
import com.util.Debug;

/**
 * @ejb:bean name="PmContractTermEJB"
 *           display-name="Contract Entity"
 *           type="CMP"
 *           cmp-version="2.x"
 *           view-type="local"
 *           local-jndi-name="omega-ejb/PmContractTermEJB"
 *           schema="PmContractTerm"
 *           primkey-field="ctCode"
 *       
 * @ejb:pk class="java.lang.Integer"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="pmuser"
 *                        role-link="pmuserlink"
 *
 * @ejb:permission role-name="pmuser"
 *
 * @ejb:interface local-class="com.ejb.pm.LocalPmContractTerm"
 *                local-extends="javax.ejb.EJBLocalObject"
 *
 * @ejb:home local-class="com.ejb.pm.LocalPmContractTermHome"
 *           local-extends="javax.ejb.EJBLocalHome"
 *
 * @ejb:finder signature="Collection findCtAll(java.lang.Integer CT_AD_CMPNY)"
 *             query="SELECT OBJECT(ct) FROM PmContractTerm ct WHERE ct.ctAdCompany = ?1"
 *
 * @jboss:query signature="Collection findCtAll(java.lang.Integer CT_AD_CMPNY)"
 *             query="SELECT OBJECT(ct) FROM PmContractTerm ct WHERE ct.ctAdCompany = ?1 ORDER BY ct.ctCode"
 * 
 * @ejb:finder signature="LocalPmContractTerm findCtByCtrCodeAndCtTermDescription(java.lang.Integer CTR_CODE, java.lang.String CT_TRM_NM, java.lang.Integer CT_AD_CMPNY)"
 *             query="SELECT OBJECT(ct) FROM PmContractTerm ct WHERE ct.pmContract.ctrCode = ?1 AND ct.ctTermDescription = ?2 AND ct.ctAdCompany=?3"
 *              
 * @ejb:finder signature="LocalPmContractTerm findCtByReferenceID(java.lang.Integer CT_REF_ID, java.lang.Integer CT_AD_CMPNY)"
 *             query="SELECT OBJECT(ct) FROM PmContractTerm ct WHERE ct.ctReferenceID=?1 AND ct.ctAdCompany=?2"
 *                         
 * @ejb:finder signature="LocalPmContractTerm findCtByReferenceIDAndCtrByReferenceID(java.lang.Integer CT_REF_ID, java.lang.Integer CTR_REF_ID, java.lang.Integer CT_AD_CMPNY)"
 *             query="SELECT OBJECT(ct) FROM PmContractTerm ct WHERE ct.ctReferenceID = ?1 AND ct.pmContract.ctrReferenceID = ?2  AND ct.ctAdCompany=?3"
 * 
 * @ejb:value-object match="*"
 * 						name="PmContractTerm"
 * 
 * @jboss:persistence table-name="PM_CNTRCT_TRM"
 *
 * @jboss:entity-command name="mysql-get-generated-keys"
 *
 */

public abstract class PmContractTermBean extends AbstractEntityBean {


   // Access methods for persistent fields

	/**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    * @ejb:pk-field
    *
    * @jboss:column-name name="CT_CODE"
    *
    * @jboss:persistence auto-increment="true"
    **/
   public abstract java.lang.Integer getCtCode();
   public abstract void setCtCode(java.lang.Integer CT_CODE);
   
   /**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="CT_RFRNC_ID"
	 **/
	public abstract Integer getCtReferenceID();
	/**
	 * @ejb:interface-method view-type="local"
	 **/ 
	public abstract void setCtReferenceID(Integer CT_RFRNC_ID);

	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"   
	 *
	 * @jboss:column-name name="CT_TRM_DESC"
	 **/
	public abstract String getCtTermDescription();
	/**
	 * @ejb:interface-method view-type="local"
	 **/ 
	public abstract void setCtTermDescription(String CT_TRM_DESC);
	

   
   /**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"   
	 *
	 * @jboss:column-name name="CT_VL"
	 **/
	public abstract Double getCtValue();
	/**
	 * @ejb:interface-method view-type="local"
	 **/ 
	public abstract void setCtValue(Double CT_VL);

   
   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="CT_AD_CMPNY"
    **/
   public abstract Integer getCtAdCompany();
   /**
    * @ejb:interface-method view-type="local"
    **/
   public abstract void setCtAdCompany(Integer CT_AD_CMPNY);
   
   
   // RELATIONSHIP METHODS
   
   /**
	 * @ejb:interface-method view-type="local"
	 * @ejb:relation name="contract-contractterms"
	 *               role-name="contractterm-has-one-contract"
	 *
	 * @jboss:relation related-pk-field="ctrCode"
	 *                 fk-column="PM_CONTRACT"
	 */
	public abstract LocalPmContract getPmContract();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setPmContract(LocalPmContract pmContract);
   

	/**
	 * @ejb:interface-method view-type="local"
	 * @ejb:relation name="contractterm-projectphases"
	 *               role-name="contractterm-has-many-projectphases"
	 * 
	 */
	public abstract Collection getPmProjectPhases();
	public abstract void setPmProjectPhases(Collection pmProjectPhases);
	
	
	/**
	 * @ejb:interface-method view-type="local"
	 * @ejb:relation name="contractterm-invoices"
	 *               role-name="contractterm-has-many-invoices"
	 * 
	 */
	public abstract Collection getArInvoices();
	public abstract void setArInvoices(Collection arInvoices);
		
	
	// BUSINESS METHODS
	

	
	
   // EntityBean methods

   /**
    * @ejb:create-method view-type="local"
    **/
  public java.lang.Integer ejbCreate (java.lang.Integer CT_RFRNC_ID, 
		  String CT_TRM_DESC, Double CT_VL, 
		  Integer CT_AD_CMPNY)
      throws CreateException {

      Debug.print("PmContractTermBean ejbCreate");
      
      setCtReferenceID(CT_RFRNC_ID);
      setCtTermDescription(CT_TRM_DESC);
      setCtValue(CT_VL);
      setCtAdCompany(CT_AD_CMPNY);
      
      return null;
   }

  

   public void ejbPostCreate (java.lang.Integer CT_RFRNC_ID, 
			  String CT_TRM_DESC, Double CT_VL, 
			  Integer CT_AD_CMPNY)
      throws CreateException { }



} // HrPayrollPeriodBean class
