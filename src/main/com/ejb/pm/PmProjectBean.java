/*
 * com/ejb/pm/LocalHrProjectBean.java
 *
 * Created on February 21, 2003, 12:55 PM
 */

package com.ejb.pm;

import java.util.Collection;
import java.util.Date;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;

import com.ejb.ar.LocalArAppliedInvoice;
import com.ejb.ar.LocalArInvoice;
import com.ejb.ar.LocalArInvoiceBatch;
import com.ejb.cm.LocalCmAdjustment;
import com.util.AbstractEntityBean;
import com.util.Debug;

/**
 * @ejb:bean name="PmProjectEJB"
 *           display-name="Project Entity"
 *           type="CMP"
 *           cmp-version="2.x"
 *           view-type="local"
 *           local-jndi-name="omega-ejb/PmProjectEJB"
 *           schema="PmProject"
 *           primkey-field="prjCode"
 *       
 * @ejb:pk class="java.lang.Integer"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="pmuser"
 *                        role-link="pmuserlink"
 *
 * @ejb:permission role-name="pmuser"
 *
 * @ejb:interface local-class="com.ejb.pm.LocalPmProject"
 *                local-extends="javax.ejb.EJBLocalObject"
 *
 * @ejb:home local-class="com.ejb.pm.LocalPmProjectHome"
 *           local-extends="javax.ejb.EJBLocalHome"
 *
 * @ejb:finder signature="Collection findPrjAll(java.lang.Integer PRJ_AD_CMPNY)"
 *             query="SELECT OBJECT(prj) FROM PmProject prj WHERE prj.prjAdCompany = ?1"
 *
 * @jboss:query signature="Collection findPrjAll(java.lang.Integer PRJ_AD_CMPNY)"
 *             query="SELECT OBJECT(prj) FROM PmProject prj WHERE prj.prjAdCompany = ?1 ORDER BY prj.prjProjectCode"
 *
 * @ejb:finder signature="LocalPmProject findByPrjName(java.lang.String PRJ_NM, java.lang.Integer PRJ_AD_CMPNY)"
 *             query="SELECT OBJECT(prj) FROM PmProject prj WHERE prj.prjName=?1 AND prj.prjAdCompany=?2"
 * 
 * @ejb:finder signature="LocalPmProject findByPrjProjectCode(java.lang.String PRJ_PRJCT_CODE, java.lang.Integer PRJ_AD_CMPNY)"
 *             query="SELECT OBJECT(prj) FROM PmProject prj WHERE prj.prjProjectCode=?1 AND prj.prjAdCompany=?2"
 * 
 * @ejb:finder signature="LocalPmProject findPrjByReferenceID(java.lang.Integer PRJ_REF_ID, java.lang.Integer PRJ_AD_CMPNY)"
 *             query="SELECT OBJECT(prj) FROM PmProject prj WHERE prj.prjReferenceID=?1 AND prj.prjAdCompany=?2"
 *  
 * @jboss:query signature="Collection ejbSelectGeneric(java.lang.String jbossQl, java.lang.Object[] args)"
 *                         dynamic="true"
 *          
 * @ejb:value-object match="*"
 * 						name="PmProject"
 * 
 * @jboss:persistence table-name="PM_PRJCT"
 *
 * @jboss:entity-command name="mysql-get-generated-keys"
 *
 */

public abstract class PmProjectBean extends AbstractEntityBean {


   // Access methods for persistent fields

	/**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    * @ejb:pk-field
    *
    * @jboss:column-name name="PRJ_CODE"
    *
    * @jboss:persistence auto-increment="true"
    **/
   public abstract java.lang.Integer getPrjCode();
   public abstract void setPrjCode(java.lang.Integer PRJ_CODE);
	   
   /**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="PRJ_RFRNC_ID"
	 **/
	public abstract Integer getPrjReferenceID();
	/**
	 * @ejb:interface-method view-type="local"
	 **/ 
	public abstract void setPrjReferenceID(Integer PRJ_RFRNC_ID);
   
	
	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"   
	 *
	 * @jboss:column-name name="PRJ_PRJCT_CODE"
	 **/
	public abstract String getPrjProjectCode();
	/**
	 * @ejb:interface-method view-type="local"
	 **/ 
	public abstract void setPrjProjectCode(String PRJ_PRJCT_CODE);
   
   
	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"   
	 *
	 * @jboss:column-name name="PRJ_NM"
	 **/
	public abstract String getPrjName();
	/**
	 * @ejb:interface-method view-type="local"
	 **/ 
	public abstract void setPrjName(String PRJ_NM);
	
	
	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"   
	 *
	 * @jboss:column-name name="PRJ_CLNT_ID"
	 **/
	public abstract String getPrjClientID();
	/**
	 * @ejb:interface-method view-type="local"
	 **/ 
	public abstract void setPrjClientID(String PRJ_CLNT_ID);
	

   
   /**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"   
	 *
	 * @jboss:column-name name="PRJ_STAT"
	 **/
	public abstract String getPrjStatus();
	/**
	 * @ejb:interface-method view-type="local"
	 **/ 
	public abstract void setPrjStatus(String PRJ_STAT);

   
   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="PRJ_AD_CMPNY"
    **/
   public abstract Integer getPrjAdCompany();
   /**
    * @ejb:interface-method view-type="local"
    **/
   public abstract void setPrjAdCompany(Integer PRJ_AD_CMPNY);
   
   
   // RELATIONSHIP METHODS
   
   
   /**
	 * @ejb:interface-method view-type="local"
	 * @ejb:relation name="project-invoices"
	 *               role-name="project-has-many-invoices"
	 * 
	 */
	public abstract Collection getArInvoices();
	public abstract void setArInvoices(Collection arInvoices);
	
	/**
	 * @ejb:interface-method view-type="local"
	 * @ejb:relation name="project-projectings"
	 *               role-name="project-has-many-projectings"
	 * 
	 */
	public abstract Collection getPmProjectings();
	public abstract void setPmProjectings(Collection pmProjectings);
	
	/**
	 * @ejb:interface-method view-type="local"
	 * @ejb:relation name="project-voucherlineitems"
	 *               role-name="project-has-many-voucherlineitems"
	 * 
	 */
	public abstract Collection getApVoucherLineItems();
	public abstract void setApVoucherLineItems(Collection apVoucherLineItems);
	
	/**
	 * @ejb:interface-method view-type="local"
	 * @ejb:relation name="project-projecttypetypes"
	 *               role-name="project-has-many-projecttypetypes"
	 * 
	 */
	public abstract Collection getPmProjectTypeTypes();
	public abstract void setPmProjectTypeTypes(Collection pmProjectTypeTypes);
	
	
	/**
	 * @ejb:interface-method view-type="local"
	 * @ejb:relation name="project-projecttimerecords"
	 *               role-name="project-has-many-projecttimerecords"
	 * 
	 */
	public abstract Collection getPmProjectTimeRecords();
	public abstract void setPmProjectTimeRecords(Collection pmProjectTimeRecords);
	
	   
	
	/**
 	 * @jboss:dynamic-ql
 	 */
 	public abstract Collection ejbSelectGeneric(java.lang.String jbossQl, java.lang.Object[] args)
 	throws FinderException;     
 	
 	// BUSINESS METHODS
 	
 	/**
 	 * @ejb:home-method view-type="local"
 	 */
 	public Collection ejbHomeGetPrjByCriteria(java.lang.String jbossQl, java.lang.Object[] args)
 	throws FinderException {
 		
 		return ejbSelectGeneric(jbossQl, args);
 		
 	}
	

	
	
   // EntityBean methods

   /**
    * @ejb:create-method view-type="local"
    **/
  public java.lang.Integer ejbCreate (java.lang.Integer PRJ_RFRNC_ID, 
		  String PRJ_PRJCT_CODE, String PRJ_NM, String PRJ_CLNT_ID, String PRJ_STAT, 
		  Integer PRJ_AD_CMPNY)
      throws CreateException {

      Debug.print("PmProjectBean ejbCreate");
      
      setPrjReferenceID(PRJ_RFRNC_ID);
      setPrjProjectCode(PRJ_PRJCT_CODE);
      setPrjName(PRJ_NM);
      setPrjClientID(PRJ_CLNT_ID);
      setPrjStatus(PRJ_STAT);
      setPrjAdCompany(PRJ_AD_CMPNY);
      
      return null;
   }

  

   public void ejbPostCreate (java.lang.Integer PRJ_RFRNC_ID, 
			  String PRJ_PRJCT_CODE, String PRJ_NM, String PRJ_CLNT_ID, 
			  String PRJ_STAT, Integer PRJ_AD_CMPNY)
      throws CreateException { }



} // HrPayrollPeriodBean class
