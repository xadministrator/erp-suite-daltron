/*
 * com/ejb/pm/LocalHrProjectPhaseBean.java
 *
 * Created on February 21, 2003, 12:55 PM
 */

package com.ejb.pm;

import java.util.Collection;
import java.util.Date;

import javax.ejb.CreateException;
import javax.ejb.EJBException;

import com.util.AbstractEntityBean;
import com.util.Debug;

/**
 * @ejb:bean name="PmProjectPhaseEJB"
 *           display-name="ProjectPhase Entity"
 *           type="CMP"
 *           cmp-version="2.x"
 *           view-type="local"
 *           local-jndi-name="omega-ejb/PmProjectPhaseEJB"
 *           schema="PmProjectPhase"
 *           primkey-field="ppCode"
 *
 * @ejb:pk class="java.lang.Integer"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="pmuser"
 *                        role-link="pmuserlink"
 *
 * @ejb:permission role-name="pmuser"
 *
 * @ejb:interface local-class="com.ejb.pm.LocalPmProjectPhase"
 *                local-extends="javax.ejb.EJBLocalObject"
 *
 * @ejb:home local-class="com.ejb.pm.LocalPmProjectPhaseHome"
 *           local-extends="javax.ejb.EJBLocalHome"
 *
 * @ejb:finder signature="Collection findPpAll(java.lang.Integer PP_AD_CMPNY)"
 *             query="SELECT OBJECT(pp) FROM PmProjectPhase pp WHERE pp.ppAdCompany = ?1"
 *
 * @jboss:query signature="Collection findPpAll(java.lang.Integer PP_AD_CMPNY)"
 *             query="SELECT OBJECT(pp) FROM PmProjectPhase pp WHERE pp.ppAdCompany = ?1 ORDER BY pp.ppName"
 *             
 * @ejb:finder signature="LocalPmProjectPhase findPpByPrjProjectCodeAndPtProjectTypeCodeAndPpName(java.lang.String PRJ_PRJCT_CODE, java.lang.String PT_PRJCT_TYP_CODE, java.lang.String PP_NM, java.lang.Integer PP_AD_CMPNY)"
 *             query="SELECT OBJECT(pp) FROM PmProjectPhase pp WHERE pp.pmProjectTypeType.pmProject.prjProjectCode = ?1 AND pp.pmProjectTypeType.pmProjectType.ptProjectTypeCode = ?2 AND pp.ppName = ?3 AND pp.ppAdCompany = ?4"
 *
 * @ejb:finder signature="LocalPmProjectPhase findByPpCodeAndPrjCode(java.lang.Integer PP_CODE, java.lang.Integer PRJ_CODE , java.lang.Integer PP_AD_CMPNY)"
 *             query="SELECT OBJECT(pp) FROM PmProjectPhase pp WHERE pp.ppCode = ?1 AND pp.pmProjectTypeType.pmProject.prjCode = ?2 AND pp.ppAdCompany = ?3"
 *
 * @ejb:finder signature="Collection findPpByPrjProjectCodeAndPtProjectTypeCode(java.lang.String PRJ_PRJCT_CODE, java.lang.String PT_PRJCT_TYP_CODE, java.lang.Integer PP_AD_CMPNY)"
 *             query="SELECT OBJECT(pp) FROM PmProjectPhase pp WHERE pp.pmProjectTypeType.pmProject.prjProjectCode = ?1 AND pp.pmProjectTypeType.pmProjectType.ptProjectTypeCode = ?2 AND pp.ppAdCompany = ?3"
 *
 * @jboss:query signature="Collection findPpByPrjProjectCodeAndPtProjectTypeCode(java.lang.String PRJ_PRJCT_CODE, java.lang.String PT_PRJCT_TYP_CODE, java.lang.Integer PP_AD_CMPNY)"
 *             query="SELECT OBJECT(pp) FROM PmProjectPhase pp WHERE pp.pmProjectTypeType.pmProject.prjProjectCode = ?1 AND pp.pmProjectTypeType.pmProjectType.ptProjectTypeCode = ?2 AND pp.ppAdCompany = ?3 ORDER BY pp.ppName"
 *
 * @ejb:finder signature="LocalPmProjectPhase findPpByReferenceID(java.lang.Integer PP_REF_ID, java.lang.Integer DB_AD_CMPNY)"
 *             query="SELECT OBJECT(pp) FROM PmProjectPhase pp WHERE pp.ppReferenceID=?1 AND pp.ppAdCompany=?2"
 * 
 * @ejb:finder signature="LocalPmProjectPhase findPpByReferenceIDAndPttByReferenceID(java.lang.Integer PP_REF_ID, java.lang.Integer PTT_REF_ID, java.lang.Integer DB_AD_CMPNY)"
 *             query="SELECT OBJECT(pp) FROM PmProjectPhase pp WHERE pp.ppReferenceID = ?1 AND pp.pmProjectTypeType.pttReferenceID = ?2 AND pp.ppAdCompany=?3"
 * 
 * 
 * 
 * @ejb:value-object match="*"
 * 						name="PmProjectPhase"
 *
 * @jboss:persistence table-name="PM_PRJCT_PHSE"
 *
 * @jboss:entity-command name="mysql-get-generated-keys"
 *
 */

public abstract class PmProjectPhaseBean extends AbstractEntityBean {


   // Access methods for persistent fields


	/**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    * @ejb:pk-field
    *
    * @jboss:column-name name="PP_CODE"
    *
    * @jboss:persistence auto-increment="true"
    **/
   public abstract java.lang.Integer getPpCode();
   public abstract void setPpCode(java.lang.Integer PP_CODE);

   /**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="PP_RFRNC_ID"
	 **/
	public abstract Integer getPpReferenceID();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setPpReferenceID(Integer PP_RFRNC_ID);


   /**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="PP_NM"
	 **/
	public abstract String getPpName();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setPpName(String PP_NM);


   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="PP_AD_CMPNY"
    **/
   public abstract Integer getPpAdCompany();
   /**
    * @ejb:interface-method view-type="local"
    **/
   public abstract void setPpAdCompany(Integer PP_AD_CMPNY);


   // RELATIONSHIP METHODS


   /**
	 * @ejb:interface-method view-type="local"
	 * @ejb:relation name="projecttypetype-projectphases"
	 *               role-name="projectphase-has-one-projecttypetype"
	 *
	 * @jboss:relation related-pk-field="pttCode"
	 *                 fk-column="PM_PROJECT_TYPE_TYPE"
	 */
	public abstract LocalPmProjectTypeType getPmProjectTypeType();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setPmProjectTypeType(LocalPmProjectTypeType pmProjectTypeType);


	/**
	 * @ejb:interface-method view-type="local"
	 * @ejb:relation name="contractterm-projectphases"
	 *               role-name="projectphase-has-one-contractterm"
	 *
	 * @jboss:relation related-pk-field="ctCode"
	 *                 fk-column="PM_CONTRACT_TERM"
	 */
	public abstract LocalPmContractTerm getPmContractTerm();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setPmContractTerm(LocalPmContractTerm pmContractTerm);

   /**
	 * @ejb:interface-method view-type="local"
	 * @ejb:relation name="projectphase-invoices"
	 *               role-name="projectphase-has-many-invoices"
	 *
	 */
	public abstract Collection getArInvoices();
	public abstract void setArInvoices(Collection arInvoices);
	
	/**
	 * @ejb:interface-method view-type="local"
	 * @ejb:relation name="projectphase-projectings"
	 *               role-name="projectphase-has-many-projectings"
	 *
	 */
	public abstract Collection getPmProjectings();
	public abstract void setPmProjectings(Collection pmProjectings);


	/**
	 * @ejb:interface-method view-type="local"
	 * @ejb:relation name="projectphase-voucherlineitems"
	 *               role-name="projectphase-has-many-voucherlineitems"
	 *
	 */
	public abstract Collection getApVoucherLineItems();
	public abstract void setApVoucherLineItems(Collection apVoucherLineItems);

	// BUSINESS METHODS
	

	
	
   // EntityBean methods

   /**
    * @ejb:create-method view-type="local"
    **/
  public java.lang.Integer ejbCreate (java.lang.Integer PP_RFRNC_ID, String PP_NM,
		  Integer PP_AD_CMPNY)
      throws CreateException {

      Debug.print("PmProjectPhaseBean ejbCreate");

      setPpReferenceID(PP_RFRNC_ID);
      setPpName(PP_NM);
      setPpAdCompany(PP_AD_CMPNY);

      return null;
   }



   public void ejbPostCreate (java.lang.Integer PP_RFRNC_ID, String PP_NM,
			  Integer PP_AD_CMPNY)
      throws CreateException { }



} // PmProjectPhaseBean class
