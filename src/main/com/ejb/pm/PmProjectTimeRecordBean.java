/*
 * com/ejb/pm/LocalPmProjectTimeRecord.java
 *
 * Created on September 06, 2018, 5:11 PM
 */

package com.ejb.pm;

import javax.ejb.CreateException;

import com.util.AbstractEntityBean;
import com.util.Debug;

/**
 * @ejb:bean name="PmProjectTimeRecordEJB"
 *           display-name="Contract Entity"
 *           type="CMP"
 *           cmp-version="2.x"
 *           view-type="local"
 *           local-jndi-name="omega-ejb/PmProjectTimeRecordEJB"
 *           schema="PmProjectTimeRecord"
 *           primkey-field="ptrCode"
 *       
 * @ejb:pk class="java.lang.Integer"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="pmuser"
 *                        role-link="pmuserlink"
 *
 * @ejb:permission role-name="pmuser"
 *
 * @ejb:interface local-class="com.ejb.pm.LocalPmProjectTimeRecord"
 *                local-extends="javax.ejb.EJBLocalObject"
 *
 * @ejb:home local-class="com.ejb.pm.LocalPmProjectTimeRecordHome"
 *           local-extends="javax.ejb.EJBLocalHome"
 *
 * @ejb:finder signature="Collection findPtrAll(java.lang.Integer PTR_AD_CMPNY)"
 *             query="SELECT OBJECT(ptr) FROM PmProjectTimeRecord ptr WHERE ptr.ptrAdCompany = ?1"
 *
 * @jboss:query signature="Collection findPtrAll(java.lang.Integer PTR_AD_CMPNY)"
 *             query="SELECT OBJECT(ptr) FROM PmProjectTimeRecord ptr WHERE ptr.ptrAdCompany = ?1 ORDER BY ptr.ptrCode"
 * 
 * @ejb:finder signature="LocalPmProjectTimeRecord findPtrByPrjReferenceIDAndUsrReferenceID(java.lang.Integer PRJ_REF_ID, java.lang.Integer USR_REF_ID, java.lang.Integer PTR_AD_CMPNY)"
 *             query="SELECT OBJECT(ptr) FROM PmProjectTimeRecord ptr WHERE ptr.pmProject.prjReferenceID = ?1 AND ptr.pmUser.usrReferenceID = ?2 AND ptr.ptrAdCompany=?3"
 *                        
 *                          
 *                          
 * @ejb:value-object match="*"
 * 						name="PmProjectTimeRecord"
 * 
 * @jboss:persistence table-name="PM_PRJCT_TM_RCRD"
 *
 * @jboss:entity-command name="mysql-get-generated-keys"
 *
 */

public abstract class PmProjectTimeRecordBean extends AbstractEntityBean {


   // Access methods for persistent fields
	
	/**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    * @ejb:pk-field
    *
    * @jboss:column-name name="PTR_CODE"
    *
    * @jboss:persistence auto-increment="true"
    **/
   public abstract java.lang.Integer getPtrCode();
   public abstract void setPtrCode(java.lang.Integer PTR_CODE);
   
	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"   
	 *
	 * @jboss:column-name name="PTR_REG_RT"
	 **/
	public abstract Double getPtrRegularRate();
	/**
	 * @ejb:interface-method view-type="local"
	 **/ 
	public abstract void setPtrRegularRate(Double PTR_REG_RT);
	
	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"   
	 *
	 * @jboss:column-name name="PTR_OT_RT"
	 **/
	public abstract Double getPtrOvertimeRate();
	/**
	 * @ejb:interface-method view-type="local"
	 **/ 
	public abstract void setPtrOvertimeRate(Double PTR_OT_RT);
	
	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"   
	 *
	 * @jboss:column-name name="PTR_REG_HRS"
	 **/
	public abstract Double getPtrRegularHours();
	/**
	 * @ejb:interface-method view-type="local"
	 **/ 
	public abstract void setPtrRegularHours(Double PTR_REG_HRS);
	
	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"   
	 *
	 * @jboss:column-name name="PTR_OT_HRS"
	 **/
	public abstract Double getPtrOvertimeHours();
	/**
	 * @ejb:interface-method view-type="local"
	 **/ 
	public abstract void setPtrOvertimeHours(Double PTR_OT_HRS);
	
	
   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="PTR_AD_CMPNY"
    **/
   public abstract Integer getPtrAdCompany();
   /**
    * @ejb:interface-method view-type="local"
    **/
   public abstract void setPtrAdCompany(Integer PTR_AD_CMPNY);
   
   
   // RELATIONSHIP METHODS
   
   
   /**
	 * @ejb:interface-method view-type="local"
	 * @ejb:relation name="project-projecttimerecords"
	 *               role-name="projecttimerecord-has-one-project"
	 *
	 * @jboss:relation related-pk-field="prjCode"
	 *                 fk-column="PM_PROJECT"
	 */
	public abstract LocalPmProject getPmProject();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setPmProject(LocalPmProject pmProject);
	
	
	/**
	 * @ejb:interface-method view-type="local"
	 * @ejb:relation name="user-projecttimerecords"
	 *               role-name="projecttimerecord-has-one-user"
	 *
	 * @jboss:relation related-pk-field="usrCode"
	 *                 fk-column="PM_USER"
	 */
	public abstract LocalPmUser getPmUser();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setPmUser(LocalPmUser pmUser);
	
  
	
	// BUSINESS METHODS
	
	
	
	
	
   // EntityBean methods

   /**
    * @ejb:create-method view-type="local"
    **/
  public java.lang.Integer ejbCreate (
		 Double PTR_REG_RT,  Double PTR_OT_RT, Double PTR_REG_HRS,  Double PTR_OT_HRS, 
		  Integer PTR_AD_CMPNY)
      throws CreateException {

      Debug.print("PmProjectTimeRecordBean ejbCreate");
      
      setPtrRegularRate(PTR_REG_RT);
      setPtrOvertimeRate(PTR_OT_RT);
      setPtrRegularHours(PTR_REG_HRS);
      setPtrOvertimeHours(PTR_OT_HRS);
      
      setPtrAdCompany(PTR_AD_CMPNY);
      
      return null;
   }

  

   public void ejbPostCreate (
			 Double PTR_REG_RT,  Double PTR_OT_RT, Double PTR_REG_HRS,  Double PTR_OT_HRS, 
			  Integer PTR_AD_CMPNY)
      throws CreateException { }



} // PmProjectTimeRecordBean class
