/*
 * com/ejb/pm/LocalHrProjectTypeBean.java
 *
 * Created on February 21, 2003, 12:55 PM
 */

package com.ejb.pm;

import java.util.Collection;

import javax.ejb.CreateException;
import javax.ejb.FinderException;

import com.util.AbstractEntityBean;
import com.util.Debug;

/**
 * @ejb:bean name="PmProjectTypeEJB"
 *           display-name="ProjectType Entity"
 *           type="CMP"
 *           cmp-version="2.x"
 *           view-type="local"
 *           local-jndi-name="omega-ejb/PmProjectTypeEJB"
 *           schema="PmProjectType"
 *           primkey-field="ptCode"
 *       
 * @ejb:pk class="java.lang.Integer"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="pmuser"
 *                        role-link="pmuserlink"
 *
 * @ejb:permission role-name="pmuser"
 *
 * @ejb:interface local-class="com.ejb.pm.LocalPmProjectType"
 *                local-extends="javax.ejb.EJBLocalObject"
 *
 * @ejb:home local-class="com.ejb.pm.LocalPmProjectTypeHome"
 *           local-extends="javax.ejb.EJBLocalHome"
 *
 * @ejb:finder signature="Collection findPtAll(java.lang.Integer PT_AD_CMPNY)"
 *             query="SELECT OBJECT(pt) FROM PmProjectType pt WHERE pt.ptAdCompany = ?1"
 *
 * @jboss:query signature="Collection findPtAll(java.lang.Integer PT_AD_CMPNY)"
 *             query="SELECT OBJECT(pt) FROM PmProjectType pt WHERE pt.ptAdCompany = ?1 ORDER BY pt.ptProjectTypeCode"
 *
 * @ejb:finder signature="LocalPmProjectType findPtByReferenceID(java.lang.Integer PT_REF_ID, java.lang.Integer PT_AD_CMPNY)"
 *             query="SELECT OBJECT(pt) FROM PmProjectType pt WHERE pt.ptReferenceID=?1 AND pt.ptAdCompany=?2"
 * 
 * 
 * @jboss:query signature="Collection ejbSelectGeneric(java.lang.String jbossQl, java.lang.Object[] args)"
 *                         dynamic="true"
 *             
 * @ejb:value-object match="*"
 * 						name="PmProjectType"
 * 
 * @jboss:persistence table-name="PM_PRJCT_TYP"
 *
 * @jboss:entity-command name="mysql-get-generated-keys"
 *
 */

public abstract class PmProjectTypeBean extends AbstractEntityBean {


   // Access methods for persistent fields

	/**
	    * @ejb:persistent-field
	    * @ejb:interface-method view-type="local"
	    * @ejb:pk-field
	    *
	    * @jboss:column-name name="PT_CODE"
	    *
	    * @jboss:persistence auto-increment="true"
	    **/
	   public abstract java.lang.Integer getPtCode();
	   public abstract void setPtCode(java.lang.Integer PT_CODE);
	   
	   /**
		 * @ejb:persistent-field
		 * @ejb:interface-method view-type="local"
		 *
		 * @jboss:column-name name="PT_RFRNC_ID"
		 **/
		public abstract Integer getPtReferenceID();
		/**
		 * @ejb:interface-method view-type="local"
		 **/ 
		public abstract void setPtReferenceID(Integer PT_RFRNC_ID);
   
	
	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"   
	 *
	 * @jboss:column-name name="PT_PRJCT_TYP_CODE"
	 **/
	public abstract String getPtProjectTypeCode();
	/**
	 * @ejb:interface-method view-type="local"
	 **/ 
	public abstract void setPtProjectTypeCode(String PT_PRJCT_TYP_CODE);
   
   
	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"   
	 *
	 * @jboss:column-name name="PT_VL"
	 **/
	public abstract String getPtValue();
	/**
	 * @ejb:interface-method view-type="local"
	 **/ 
	public abstract void setPtValue(String PT_VL);
	

   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="PT_AD_CMPNY"
    **/
   public abstract Integer getPtAdCompany();
   /**
    * @ejb:interface-method view-type="local"
    **/
   public abstract void setPtAdCompany(Integer PT_AD_CMPNY);
   
   
   // RELATIONSHIP METHODS
   

   /**
	 * @ejb:interface-method view-type="local"
	 * @ejb:relation name="projecttype-projecttypetypes"
	 *               role-name="projecttype-has-many-projecttypetypes"
	 * 
	 */
	public abstract Collection getPmProjectTypeTypes();
	public abstract void setPmProjectTypeTypes(Collection pmProjectTypeTypes);
	
	
	/**
	 * @jboss:dynamic-ql
	 */
	public abstract Collection ejbSelectGeneric(java.lang.String jbossQl, java.lang.Object[] args)
	throws FinderException;	   

   // Business methods
	
	/**
	 * @ejb:home-method view-type="local"
	 */
	public Collection ejbHomeGetProjTypByCriteria(java.lang.String jbossQl, java.lang.Object[] args)
	throws FinderException {
		
		return ejbSelectGeneric(jbossQl, args);
		
	}
	

	
	
   // EntityBean methods

   /**
    * @ejb:create-method view-type="local"
    **/
  public java.lang.Integer ejbCreate (java.lang.Integer PT_RFRNC_ID, 
		  String PT_PTYPCT_CODE, String PT_VL,
		  Integer PT_AD_CMPNY)
      throws CreateException {

      Debug.print("PmProjectTypeBean ejbCreate");
      
      setPtReferenceID(PT_RFRNC_ID);
      setPtProjectTypeCode(PT_PTYPCT_CODE);
      setPtValue(PT_VL);
      setPtAdCompany(PT_AD_CMPNY);
      
      return null;
   }

  

   public void ejbPostCreate (java.lang.Integer PT_RFRNC_ID, 
			  String PT_PTYPCT_CODE, String PT_VL,
			  Integer PT_AD_CMPNY)
      throws CreateException { }



} // HrPayrollPeriodBean class
