/*
 * com/ejb/pm/LocalPmProjectTypeTypeBean.java
 *
 * Created on September 03, 2018, 12:55 PM
 */

package com.ejb.pm;

import java.util.Collection;

import javax.ejb.CreateException;
import javax.ejb.FinderException;

import com.util.AbstractEntityBean;
import com.util.Debug;

/**
 * @ejb:bean name="PmProjectTypeTypeEJB"
 *           display-name="ProjectTypeType Entity"
 *           type="CMP"
 *           cmp-version="2.x"
 *           view-type="local"
 *           local-jndi-name="omega-ejb/PmProjectTypeTypeEJB"
 *           schema="PmProjectTypeType"
 *           primkey-field="pttCode"
 *       
 * @ejb:pk class="java.lang.Integer"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="pmuser"
 *                        role-link="pmuserlink"
 *
 * @ejb:permission role-name="pmuser"
 *
 * @ejb:interface local-class="com.ejb.pm.LocalPmProjectTypeType"
 *                local-extends="javax.ejb.EJBLocalObject"
 *
 * @ejb:home local-class="com.ejb.pm.LocalPmProjectTypeTypeHome"
 *           local-extends="javax.ejb.EJBLocalHome"
 *
 *
 * @ejb:finder signature="Collection findPttPrjAll(java.lang.Integer PTT_AD_CMPNY)"
 *             query="SELECT OBJECT(ptt) FROM PmProjectTypeType ptt WHERE ptt.pmProject IS NOT NULL AND ptt.pttAdCompany = ?1"
 *
 * @jboss:query signature="Collection findPttPrjAll(java.lang.Integer PTT_AD_CMPNY)"
 *             query="SELECT OBJECT(ptt) FROM PmProjectTypeType ptt WHERE ptt.pmProject IS NOT NULL AND ptt.pttAdCompany = ?1 ORDER BY ptt.pttCode"
 *             
 *             
 * @ejb:finder signature="Collection findPttByPrjProjectCode(java.lang.String PRJ_PRJCT_CODE, java.lang.Integer PTT_AD_CMPNY)"
 *             query="SELECT OBJECT(ptt) FROM PmProjectTypeType ptt WHERE ptt.pmProject.prjProjectCode = ?1 AND ptt.pttAdCompany = ?2"
 *
 * @jboss:query signature="Collection findPttByPrjProjectCode(java.lang.String PRJ_PRJCT_CODE, java.lang.Integer PTT_AD_CMPNY)"
 *             query="SELECT OBJECT(ptt) FROM PmProjectTypeType ptt WHERE ptt.pmProject.prjProjectCode = ?1 AND ptt.pttAdCompany = ?2 ORDER BY ptt.pmProject.prjProjectCode"
 *                         
 * @ejb:finder signature="LocalPmProjectTypeType findPttByPrjProjectCodeAndPtProjectTypeCode(java.lang.String PRJ_PRJCT_CODE, java.lang.String PT_PRJCT_TYP_CODE, java.lang.Integer PTT_AD_CMPNY)"
 *             query="SELECT OBJECT(ptt) FROM PmProjectTypeType ptt WHERE ptt.pmProject.prjProjectCode = ?1 AND ptt.pmProjectType.ptProjectTypeCode = ?2 AND ptt.pttAdCompany = ?3"
 *
 * @ejb:finder signature="LocalPmProjectTypeType findPttByReferenceID(java.lang.Integer PTT_REF_ID, java.lang.Integer PTT_AD_CMPNY)"
 *             query="SELECT OBJECT(ptt) FROM PmProjectTypeType ptt WHERE ptt.pttReferenceID=?1 AND ptt.pttAdCompany=?2"
 * 
 * @ejb:finder signature="LocalPmProjectTypeType findPttByReferenceIDAndPrjByReferenceID(java.lang.Integer PTT_REF_ID, java.lang.Integer PRJ_REF_ID, java.lang.Integer PTT_AD_CMPNY)"
 *             query="SELECT OBJECT(ptt) FROM PmProjectTypeType ptt WHERE ptt.pttReferenceID = ?1 AND ptt.pmProject.prjReferenceID = ?2 AND ptt.pttAdCompany=?3"
 * 
 * @ejb:select signature="Collection ejbSelectGeneric(java.lang.String jbossQl, java.lang.Object[] args)"
 *             result-type-mapping="Local"
 *             method-intf="LocalHome"
 *             query="SELECT OBJECT(ptt) FROM PmProjectTypeType ptt"
 *
 * @jboss:query signature="Collection ejbSelectGeneric(java.lang.String jbossQl, java.lang.Object[] args)"
 *                         dynamic="true"
 *           
 * @ejb:value-object match="*"
 * 						name="PmProjectTypeType"
 * 
 * @jboss:persistence table-name="PM_PRJCT_TYP_TYP"
 *
 * @jboss:entity-command name="mysql-get-generated-keys"
 *
 */

public abstract class PmProjectTypeTypeBean extends AbstractEntityBean {


   // Access methods for persistent fields

	/**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    * @ejb:pk-field
    *
    * @jboss:column-name name="PTT_CODE"
    *
    * @jboss:persistence auto-increment="true"
    **/
   public abstract java.lang.Integer getPttCode();
   public abstract void setPttCode(java.lang.Integer PTT_CODE);
	   
   /**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="PTT_RFRNC_ID"
	 **/
	public abstract Integer getPttReferenceID();
	/**
	 * @ejb:interface-method view-type="local"
	 **/ 
	public abstract void setPttReferenceID(Integer PTT_RFRNC_ID);

   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="PTT_AD_CMPNY"
    **/
   public abstract Integer getPttAdCompany();
   /**
    * @ejb:interface-method view-type="local"
    **/
   public abstract void setPttAdCompany(Integer PTT_AD_CMPNY);
   
   
   // RELATIONSHIP METHODS
   
   /**
	 * @ejb:interface-method view-type="local"
	 * @ejb:relation name="project-projecttypetypes"
	 *               role-name="projecttypetype-has-one-project"
	 *
	 * @jboss:relation related-pk-field="prjCode"
	 *                 fk-column="PM_PROJECT"
	 */
	public abstract LocalPmProject getPmProject();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setPmProject(LocalPmProject pmProject);
	
	
	/**
	 * @ejb:interface-method view-type="local"
	 * @ejb:relation name="contract-projecttypetypes"
	 *               role-name="projecttypetype-has-one-contract"
	 *
	 * @jboss:relation related-pk-field="ctrCode"
	 *                 fk-column="PM_CONTRACT"
	 */
	public abstract LocalPmContract getPmContract();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setPmContract(LocalPmContract pmContract);
	


	/**
	 * @ejb:interface-method view-type="local"
	 * @ejb:relation name="projecttype-projecttypetypes"
	 *               role-name="projecttypetype-has-one-projecttype"
	 *
	 * @jboss:relation related-pk-field="ptCode"
	 *                 fk-column="PM_PROJECT_TYPE"
	 */
	public abstract LocalPmProjectType getPmProjectType();
	/**
	 * @ejb:interface-method view-type="local"
	 **/
	public abstract void setPmProjectType(LocalPmProjectType pmProjectType);
	
	
	
	/**
	 * @ejb:interface-method view-type="local"
	 * @ejb:relation name="projecttypetype-invoices"
	 *               role-name="projecttypetype-has-many-invoices"
	 * 
	 */
	public abstract Collection getArInvoices();
	public abstract void setArInvoices(Collection arInvoices);
	
	/**
	 * @ejb:interface-method view-type="local"
	 * @ejb:relation name="projecttypetype-projectings"
	 *               role-name="projecttypetype-has-many-projectings"
	 * 
	 */
	public abstract Collection getPmProjectings();
	public abstract void setPmProjectings(Collection pmProjectings);

	/**
	 * @ejb:interface-method view-type="local"
	 * @ejb:relation name="projecttypetype-voucherlineitems"
	 *               role-name="projecttypetype-has-many-voucherlineitems"
	 *
	 */
	public abstract Collection getApVoucherLineItems();
	public abstract void setApVoucherLineItems(Collection apVoucherLineItems);

	/**
	 * @ejb:interface-method view-type="local"
	 * @ejb:relation name="projecttypetype-projectphases"
	 *               role-name="projecttypetype-has-many-projectphases"
	 *
	 */
	public abstract Collection getPmProjectPhases();
	public abstract void setPmProjectPhases(Collection pmProjectPhases);
	
	/**
	 * @ejb:interface-method view-type="local"
	 * @ejb:relation name="projecttypetype-branchprojecttypetype"
	 *               role-name="projecttypetype-has-many-branchprojecttypetype"
	 */
	public abstract Collection getAdBranchProjectTypeTypes();
	public abstract void setAdBranchProjectTypeTypes(Collection adBranchProjectTypeTypes);
	

	/**
	 * @jboss:dynamic-ql
	 */
	public abstract Collection ejbSelectGeneric(java.lang.String jbossQl, java.lang.Object[] args)
	throws FinderException;
	
	// BUSINESS METHODS
	
	/**
	 * @ejb:home-method view-type="local"
	 */
	public Collection ejbHomeGetPttByCriteria(java.lang.String jbossQl, java.lang.Object[] args)
	throws FinderException {
		
		return ejbSelectGeneric(jbossQl, args);
	}
	
	
	

	
	
   // EntityBean methods

   /**
    * @ejb:create-method view-type="local"
    **/
  public java.lang.Integer ejbCreate (java.lang.Integer PTT_RFRNC_ID, 
		  Integer PTT_AD_CMPNY)
      throws CreateException {

      Debug.print("PmProjectTypeTypeBean ejbCreate");
      
      setPttReferenceID(PTT_RFRNC_ID);
      setPttAdCompany(PTT_AD_CMPNY);
      
      return null;
   }

  

   public void ejbPostCreate (java.lang.Integer PTT_RFRNC_ID, 
			  Integer PTT_AD_CMPNY)
      throws CreateException { }



} // PmProjectTypeType class
