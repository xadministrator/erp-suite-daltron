package com.ejb.pm;

import java.util.Collection;
import java.util.Date;

import javax.ejb.CreateException;
import javax.ejb.FinderException;

import com.ejb.ap.LocalApVoucherLineItem;
import com.ejb.inv.LocalInvCosting;
import com.ejb.inv.LocalInvItemLocation;
import com.util.AbstractEntityBean;
import com.util.Debug;

/**
 * @ejb:bean name="PmProjectingEJB"
 *           display-name="Projecting Entity"
 *           type="CMP"
 *           cmp-version="2.x"
 *           view-type="local"
 *           local-jndi-name="omega-ejb/PmProjectingEJB"
 *           schema="PmProjecting"
 *           primkey-field="pjtCode"
 *       
 * @ejb:pk class="java.lang.Integer"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="pmuser"
 *                        role-link="pmuserlink"
 *
 * @ejb:permission role-name="pmuser"
 *
 * @ejb:interface local-class="com.ejb.pm.LocalPmProjecting"
 *                local-extends="javax.ejb.EJBLocalObject"
 *
 * @ejb:home local-class="com.ejb.pm.LocalPmProjectingHome"
 *           local-extends="javax.ejb.EJBLocalHome"
 *  
*         
 * @ejb:select signature="Collection ejbSelectGeneric(java.lang.String jbossQl, java.lang.Object[] args)"
 *             result-type-mapping="Local"
 *             method-intf="LocalHome"
 *             query="SELECT OBJECT(pjt) FROM PmProjecting pjt"
 *             
 *             
 *  @jboss:query signature="LocalInvCosting ejbSelectByMaxPrjLineNumberAndPrjDateToLongAndIiNameAndLocNameAndPrjName(long PJT_DT_TO_LNG, java.lang.String II_NM, java.lang.String LOC_NM, java.lang.String PRJ_NM, java.lang.Integer PJT_AD_BRNCH, java.lang.Integer PJT_AD_CMPNY)"
 *                         query="SELECT OBJECT(pjt) FROM PmProjecting pjt WHERE pjt.pjtDateToLong=?1 AND pjt.invItemLocation.invItem.iiName=?2 AND pjt.invItemLocation.invLocation.locName=?3 AND pjt.pmProject.prjProjectCode=?4 AND pjt.pjtAdBranch = ?5 AND pjt.pjtAdCompany = ?6 ORDER BY pjt.pjtDate DESC, pjt.pjtLineNumber DESC, pjt.pjtDateToLong DESC LIMIT 1"
 * 
 *
 * @jboss:query signature="Collection ejbSelectGeneric(java.lang.String jbossQl, java.lang.Object[] args)"
 *                         dynamic="true"
 *
 * @ejb:value-object match="*"
 *             name="PmProjecting"
 *
 * @jboss:persistence table-name="PM_PRJCTNG"
 *
 * @jboss:entity-command name="mysql-get-generated-keys"
 *
 */


public abstract class PmProjectingBean extends AbstractEntityBean {
	
//	 Access methods for persistent fields

	   /**
	    * @ejb:persistent-field
	    * @ejb:interface-method view-type="local"
	    * @ejb:pk-field
	    *
	    * @jboss:column-name name="PJT_CODE"
	    *
	    * @jboss:persistence auto-increment="true"
	    **/
	   public abstract java.lang.Integer getPjtCode();
	   public abstract void setPjtCode(java.lang.Integer PJT_CODE);

	   /**
	    * @ejb:persistent-field
	    * @ejb:interface-method view-type="local"
	    *
	    * @jboss:column-name name="PJT_DT"
	    **/
	   public abstract java.util.Date getPjtDate();
	   /**
	    * @ejb:interface-method view-type="local"
	    **/
	   public abstract void setPjtDate(java.util.Date PJT_DT);
	   
	   /**
	    * @ejb:persistent-field
	    * @ejb:interface-method view-type="local"
	    *
	    * @jboss:column-name name="PJT_DT_TO_LNG"
	    **/
	   public abstract long getPjtDateToLong();
	   /**
	    * @ejb:interface-method view-type="local"
	    **/
	   public abstract void setPjtDateToLong(long PJT_DT_TO_LNG);

	   /**
	    * @ejb:persistent-field
	    * @ejb:interface-method view-type="local"
	    *
	    * @jboss:column-name name="PJT_LN_NMBR"
	    **/
	   public abstract int getPjtLineNumber();
	   /**
	    * @ejb:interface-method view-type="local"
	    **/
	   public abstract void setPjtLineNumber(int PJT_LN_NMBR);
	   
	   
	   /**
	    * @ejb:persistent-field
	    * @ejb:interface-method view-type="local"
	    *
	    * @jboss:column-name name="PJT_PRJCT_CST"
	    **/
	   public abstract double getPjtProjectCost();
	   /**
	    * @ejb:interface-method view-type="local"
	    **/
	   public abstract void setPjtProjectCost(double PJT_PRJCT_CST);
	   
	   
	  
	   /**
	    * @ejb:persistent-field
	    * @ejb:interface-method view-type="local"
	    *
	    * @jboss:column-name name="PJT_AD_BRNCH"
	    **/
	   public abstract Integer getPjtAdBranch();
	   /**
	    * @ejb:interface-method view-type="local"
	    **/
	   public abstract void setPjtAdBranch(Integer PJT_AD_BRNCH);
	   
	   /**
	    * @ejb:persistent-field
	    * @ejb:interface-method view-type="local"
	    *
	    * @jboss:column-name name="PJT_AD_CMPNY"
	    **/
	   public abstract Integer getPjtAdCompany();
	   /**
	    * @ejb:interface-method view-type="local"
	    **/
	   public abstract void setPjtAdCompany(Integer PJT_AD_CMPNY);	   

	   // Access methods for relationship fields

	   /**
	    * @ejb:interface-method view-type="local"
	    * @ejb:relation name="itemlocation-projectings"
	    *               role-name="projecting-has-one-itemlocation"
	    *               cascade-delete="no"
	    *
	    * @jboss:relation related-pk-field="ilCode"
	    *                 fk-column="INV_ITEM_LOCATION"
	    */
	   public abstract LocalInvItemLocation getInvItemLocation();
	   /**
	    * @ejb:interface-method view-type="local"
	    **/	 
	   public abstract void setInvItemLocation(LocalInvItemLocation invItemLocation);

	   	   
	   /**
	    * @ejb:interface-method view-type="local"
	    * @ejb:relation name="voucherlineitem-projectings"
	    *               role-name="projecting-has-one-voucherlineitem"
	    *               cascade-delete="yes"
	    *
	    * @jboss:relation related-pk-field="vliCode"
	    *                 fk-column="AP_VOUCHER_LINE_ITEM"
	    */
	   public abstract LocalApVoucherLineItem getApVoucherLineItem();
	   /**
	    * @ejb:interface-method view-type="local"
	    **/
	   public abstract void setApVoucherLineItem(LocalApVoucherLineItem apVoucherLineItem);
	   
	   
	   
	 //PMA

 		/**
 		 * @ejb:interface-method view-type="local"
 		 * @ejb:relation name="project-projectings"
 		 *               role-name="projecting-has-one-project"
 		 *
 		 * @jboss:relation related-pk-field="prjCode"
 		 *                 fk-column="PM_PROJECT"
 		 */
 		public abstract LocalPmProject getPmProject();
 		/**
 		 * @ejb:interface-method view-type="local"
 		 **/
 		public abstract void setPmProject(LocalPmProject pmProject);


 		/**
 		 * @ejb:interface-method view-type="local"
 		 * @ejb:relation name="projecttypetype-projectings"
 		 *               role-name="projecting-has-one-projecttypetype"
 		 *
 		 * @jboss:relation related-pk-field="pttCode"
 		 *                 fk-column="PM_PROJECT_TYPE_TYPE"
 		 */
 		public abstract LocalPmProjectTypeType getPmProjectTypeType();
 		/**
 		 * @ejb:interface-method view-type="local"
 		 **/
 		public abstract void setPmProjectTypeType(LocalPmProjectTypeType pmProjectTypeType);


 		/**
 		 * @ejb:interface-method view-type="local"
 		 * @ejb:relation name="projectphase-projectings"
 		 *               role-name="projecting-has-one-projectphase"
 		 *
 		 * @jboss:relation related-pk-field="ppCode"
 		 *                 fk-column="PM_PROJECT_PHASE"
 		 */
 		public abstract LocalPmProjectPhase getPmProjectPhase();
 		/**
 		 * @ejb:interface-method view-type="local"
 		 **/
 		public abstract void setPmProjectPhase(LocalPmProjectPhase pmProjectPhase);

	  /**
	    * @jboss:dynamic-ql
	    */
	    public abstract Collection ejbSelectGeneric(java.lang.String jbossQl, java.lang.Object[] args)
	       throws FinderException;

	   // Business methods
	   
	   /**
	    * @ejb:home-method view-type="local"
	    */
	    public Collection ejbHomeGetPjtByCriteria(java.lang.String jbossQl, java.lang.Object[] args)
	       throws FinderException {
	       	
	       return ejbSelectGeneric(jbossQl, args);
	    }
	   	
	    
	    
	    public abstract LocalPmProjecting ejbSelectByMaxPrjLineNumberAndPrjDateToLongAndIiNameAndLocNameAndPrjName(long PJT_DT_TO_LNG,
		   		java.lang.String II_NM, java.lang.String LOC_NM, java.lang.String PRJ_NM, java.lang.Integer PJT_AD_BRNCH, java.lang.Integer PJT_AD_CMPNY)
		   		throws FinderException;
	    
	    
	    /**
	      * @ejb:home-method view-type="local"
	      */
	     public LocalPmProjecting ejbHomeGetByMaxPrjLineNumberAndPrjDateToLongAndIiNameAndLocNameAndPrjName(long PJT_DT_TO_LNG, java.lang.String II_NM, java.lang.String LOC_NM, java.lang.String PRJ_NM, java.lang.Integer PJT_AD_BRNCH, java.lang.Integer PJT_AD_CMPNY)
	        throws FinderException {

	     	try {	     	
	     		      	          	    
		        return ejbSelectByMaxPrjLineNumberAndPrjDateToLongAndIiNameAndLocNameAndPrjName(PJT_DT_TO_LNG, II_NM, LOC_NM, PRJ_NM, PJT_AD_BRNCH, PJT_AD_CMPNY);
		        
	     	} catch (NullPointerException ex) {

	     		throw new FinderException(ex.getMessage());
	     		
	     	}
	        
	     }
	     
	      
	   
	   // EntityBean methods

	   /**
	    * @ejb:create-method view-type="local"
	    **/
	   public java.lang.Integer ejbCreate (java.lang.Integer PJT_CODE, java.util.Date PJT_DT, long PJT_DT_TO_LNG,
			   	  int PJT_LN_NMBR, double PJT_PRJCT_CST, Integer PJT_AD_BRNCH, Integer PJT_AD_CMPNY)
	      throws CreateException {

	      Debug.print("PmProjectingBean ejbCreate");
	      setPjtCode(PJT_CODE);
	      setPjtDate(PJT_DT);
	      setPjtDateToLong(PJT_DT_TO_LNG);
	      setPjtLineNumber(PJT_LN_NMBR);
	      setPjtProjectCost(PJT_PRJCT_CST);
	      setPjtAdBranch(PJT_AD_BRNCH);
	      setPjtAdCompany(PJT_AD_CMPNY);
	      return null;
	   }

	   /**
	    * @ejb:create-method view-type="local"
	    **/
	   public java.lang.Integer ejbCreate (java.util.Date PJT_DT, long PJT_DT_TO_LNG,
	   	  int PJT_LN_NMBR, double PJT_PRJCT_CST, Integer PJT_AD_BRNCH, Integer PJT_AD_CMPNY)
	      throws CreateException {

	      Debug.print("PmProjectingBean ejbCreate");
	      setPjtDate(PJT_DT);
	      setPjtDateToLong(PJT_DT_TO_LNG);
	      setPjtLineNumber(PJT_LN_NMBR);
	      setPjtProjectCost(PJT_PRJCT_CST);
	      setPjtAdBranch(PJT_AD_BRNCH);
	      setPjtAdCompany(PJT_AD_CMPNY);
	      return null;
	   }

	   public void ejbPostCreate (java.lang.Integer PJT_CODE, java.util.Date PJT_DT, long PJT_DT_TO_LNG,
			   	  int PJT_LN_NMBR, double PJT_PRJCT_CST, Integer PJT_AD_BRNCH, Integer PJT_AD_CMPNY)
	      throws CreateException { }

	   public void ejbPostCreate (java.util.Date PJT_DT, long PJT_DT_TO_LNG,
			   	  int PJT_LN_NMBR, double PJT_PRJCT_CST, Integer PJT_AD_BRNCH, Integer PJT_AD_CMPNY)
	      throws CreateException { }
	
}
