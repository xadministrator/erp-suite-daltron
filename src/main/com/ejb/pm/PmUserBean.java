/*
 * com/ejb/pm/LocalPmUser.java
 *
 * Created on September 06, 2018, 5:11 PM
 */

package com.ejb.pm;

import java.util.Collection;

import javax.ejb.CreateException;

import com.util.AbstractEntityBean;
import com.util.Debug;

/**
 * @ejb:bean name="PmUserEJB"
 *           display-name="User Entity"
 *           type="CMP"
 *           cmp-version="2.x"
 *           view-type="local"
 *           local-jndi-name="omega-ejb/PmUserEJB"
 *           schema="PmUser"
 *           primkey-field="usrCode"
 *       
 * @ejb:pk class="java.lang.Integer"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="pmuser"
 *                        role-link="pmuserlink"
 *
 * @ejb:permission role-name="pmuser"
 *
 * @ejb:interface local-class="com.ejb.pm.LocalPmUser"
 *                local-extends="javax.ejb.EJBLocalObject"
 *
 * @ejb:home local-class="com.ejb.pm.LocalPmUserHome"
 *           local-extends="javax.ejb.EJBLocalHome"
 *
 * @ejb:finder signature="Collection findUsrAll(java.lang.Integer USR_AD_CMPNY)"
 *             query="SELECT OBJECT(usr) FROM PmUser usr WHERE usr.usrAdCompany = ?1"
 *
 * @jboss:query signature="Collection findUsrAll(java.lang.Integer USR_AD_CMPNY)"
 *             query="SELECT OBJECT(usr) FROM PmUser usr WHERE usr.usrAdCompany = ?1 ORDER BY usr.usrCode"
 * 
 * @ejb:finder signature="LocalPmUser findUsrByReferenceID(java.lang.Integer USR_REF_ID, java.lang.Integer USR_AD_CMPNY)"
 *             query="SELECT OBJECT(usr) FROM PmUser usr WHERE usr.usrReferenceID=?1 AND usr.usrAdCompany=?2"
 * 
 * @ejb:finder signature="LocalPmUser findUsrByEmployeeNumber(java.lang.String USR_EMP_NMBR, java.lang.Integer USR_AD_CMPNY)"
 *             query="SELECT OBJECT(usr) FROM PmUser usr WHERE usr.usrEmployeeNumber = ?1 AND usr.usrAdCompany=?2"
 *
 * @ejb:finder signature="Collection findAllUsrEmployeeNumber(java.lang.Integer USR_AD_CMPNY)"
 *             query="SELECT OBJECT(usr) FROM PmUser usr WHERE usr.usrEmployeeNumber IS NOT NULL AND usr.usrAdCompany = ?1"
 *
 * @jboss:query signature="Collection findAllUsrEmployeeNumber(java.lang.Integer USR_AD_CMPNY)"
 *             query="SELECT OBJECT(usr) FROM PmUser usr WHERE usr.usrEmployeeNumber IS NOT NULL AND usr.usrAdCompany = ?1 ORDER BY usr.usrEmployeeNumber"
 *                          
 * @ejb:value-object match="*"
 * 						name="PmUser"
 * 
 * @jboss:persistence table-name="PM_USR"
 *
 * @jboss:entity-command name="mysql-get-generated-keys"
 *
 */

public abstract class PmUserBean extends AbstractEntityBean {


   // Access methods for persistent fields
	
	/**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    * @ejb:pk-field
    *
    * @jboss:column-name name="USR_CODE"
    *
    * @jboss:persistence auto-increment="true"
    **/
   public abstract java.lang.Integer getUsrCode();
   public abstract void setUsrCode(java.lang.Integer USR_CODE);
   
   /**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"
	 *
	 * @jboss:column-name name="USR_RFRNC_ID"
	 **/
	public abstract Integer getUsrReferenceID();
	/**
	 * @ejb:interface-method view-type="local"
	 **/ 
	public abstract void setUsrReferenceID(Integer USR_RFRNC_ID);

	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"   
	 *
	 * @jboss:column-name name="USR_NM"
	 **/
	public abstract String getUsrName();
	/**
	 * @ejb:interface-method view-type="local"
	 **/ 
	public abstract void setUsrName(String USR_NM);
	
	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"   
	 *
	 * @jboss:column-name name="USR_USR_NM"
	 **/
	public abstract String getUsrUserName();
	/**
	 * @ejb:interface-method view-type="local"
	 **/ 
	public abstract void setUsrUserName(String USR_USR_NM);
	
	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"   
	 *
	 * @jboss:column-name name="USR_EMP_NMBR"
	 **/
	public abstract String getUsrEmployeeNumber();
	/**
	 * @ejb:interface-method view-type="local"
	 **/ 
	public abstract void setUsrEmployeeNumber(String USR_EMP_NMBR);
	
	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"   
	 *
	 * @jboss:column-name name="USR_PSTN"
	 **/
	public abstract String getUsrPosition();
	/**
	 * @ejb:interface-method view-type="local"
	 **/ 
	public abstract void setUsrPosition(String USR_PSTN);
	
	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"   
	 *
	 * @jboss:column-name name="USR_REG_RT"
	 **/
	public abstract Double getUsrRegularRate();
	/**
	 * @ejb:interface-method view-type="local"
	 **/ 
	public abstract void setUsrRegularRate(Double USR_REG_RT);
	
	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"   
	 *
	 * @jboss:column-name name="USR_OT_RT"
	 **/
	public abstract Double getUsrOvertimeRate();
	/**
	 * @ejb:interface-method view-type="local"
	 **/ 
	public abstract void setUsrOvertimeRate(Double USR_OT_RT);
	
	/**
	 * @ejb:persistent-field
	 * @ejb:interface-method view-type="local"   
	 *
	 * @jboss:column-name name="USR_STAT"
	 **/
	public abstract String getUsrStatus();
	/**
	 * @ejb:interface-method view-type="local"
	 **/ 
	public abstract void setUsrStatus(String USR_STAT);

   
   /**
    * @ejb:persistent-field
    * @ejb:interface-method view-type="local"
    *
    * @jboss:column-name name="USR_AD_CMPNY"
    **/
   public abstract Integer getUsrAdCompany();
   /**
    * @ejb:interface-method view-type="local"
    **/
   public abstract void setUsrAdCompany(Integer USR_AD_CMPNY);
   
   
   // RELATIONSHIP METHODS
   
  
   /**
	 * @ejb:interface-method view-type="local"
	 * @ejb:relation name="user-projecttimerecords"
	 *               role-name="user-has-many-projecttimerecords"
	 * 
	 */
	public abstract Collection getPmProjectTimeRecords();
	public abstract void setPmProjectTimeRecords(Collection pmProjectTimeRecords);
	
	
	/**
	 * @ejb:interface-method view-type="local"
	 * @ejb:relation name="pmuser-customers"
	 *               role-name="user-has-many-customers"
	 * 
	 */
	public abstract Collection getArCustomers();
	public abstract void setArCustomers(Collection arCustomers);
	
	
	/**
	 * @ejb:interface-method view-type="local"
	 * @ejb:relation name="pmuser-adusers"
	 *               role-name="pmuser-has-many-adusers"
	 * 
	 */
	public abstract Collection getAdUsers();
	public abstract void setAdUsers(Collection adUsers);
	
	
	// BUSINESS METHODS
	
	
	
	
	
   // EntityBean methods

   /**
    * @ejb:create-method view-type="local"
    **/
  public java.lang.Integer ejbCreate (java.lang.Integer USR_RFRNC_ID, 
		  String USR_NM, String USR_USR_NM, String USR_EMP_NMBR, String USR_PSTN, Double USR_REG_RT,  Double USR_OT_RT, String USR_STAT,
		  Integer USR_AD_CMPNY)
      throws CreateException {

      Debug.print("PmUserBean ejbCreate");
      
      setUsrReferenceID(USR_RFRNC_ID);
      setUsrName(USR_NM);
      setUsrUserName(USR_USR_NM);
      setUsrEmployeeNumber(USR_EMP_NMBR);
      setUsrPosition(USR_PSTN);
      setUsrRegularRate(USR_REG_RT);
      setUsrOvertimeRate(USR_OT_RT);
      setUsrStatus(USR_STAT);
      
      setUsrAdCompany(USR_AD_CMPNY);
      
      return null;
   }

  

   public void ejbPostCreate (java.lang.Integer USR_RFRNC_ID, 
			  String USR_NM, String USR_USR_NM, String USR_EMP_NMBR, String USR_PSTN, Double USR_REG_RT,  Double USR_OT_RT, String USR_STAT,
			  Integer USR_AD_CMPNY)
      throws CreateException { }



} // PmUserBean class
