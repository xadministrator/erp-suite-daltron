
/*
 * AdAgingBucketControllerBean.java
 *
 * Created on June 9, 2003, 4:21 PM
 *
 * @author  Neil Andrew M. Ajero
 *
 */

package com.ejb.txn;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;
import javax.ejb.RemoveException;
import javax.naming.NamingException;

import com.ejb.ad.LocalAdAgingBucket;
import com.ejb.ad.LocalAdAgingBucketHome;
import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.exception.GlobalRecordAlreadyAssignedException;
import com.ejb.exception.GlobalRecordAlreadyDeletedException;
import com.ejb.exception.GlobalRecordAlreadyExistException;
import com.util.AbstractSessionBean;
import com.util.AdAgingBucketDetails;
import com.util.Debug;
import com.util.EJBHomeFactory;

/**
 * @ejb:bean name="AdAgingBucketControllerEJB"
 *           display-name="Used for entering aging buckets"
 *           type="Stateless"
 *           view-type="remote"
 *           jndi-name="ejb/AdAgingBucketControllerEJB"
 *
 * @ejb:interface remote-class="com.ejb.txn.AdAgingBucketController"
 *                extends="javax.ejb.EJBObject"
 *
 * @ejb:home remote-class="com.ejb.txn.AdAgingBucketControllerHome"
 *           extends="javax.ejb.EJBHome"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="aduser"
 *                        role-link="aduserlink"
 *
 * @ejb:permission role-name="aduser"
 * 
*/

public class AdAgingBucketControllerBean extends AbstractSessionBean {
    
    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList getAdAbAll(Integer AD_CMPNY) 
        throws GlobalNoRecordFoundException {
                    
        Debug.print("AdAgingBucketControllerBean getAdAbAll");

        LocalAdAgingBucketHome adAgingBucketHome = null;

        Collection adAgingBuckets = null;

        LocalAdAgingBucket adAgingBucket = null;

        ArrayList list = new ArrayList();
        
        // Initialize EJB Home

        try {
            
            adAgingBucketHome = (LocalAdAgingBucketHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdAgingBucketHome.JNDI_NAME, LocalAdAgingBucketHome.class);

        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }

        try {
            
            adAgingBuckets = adAgingBucketHome.findAbAll(AD_CMPNY);
            
        } catch (FinderException ex) {
            
        } catch (Exception ex) {
            
            throw new EJBException(ex.getMessage());
        }

        if (adAgingBuckets.isEmpty()) {
        	
            throw new GlobalNoRecordFoundException();
        	
        }
        
        Iterator i = adAgingBuckets.iterator();
               
        while (i.hasNext()) {
        	
        	adAgingBucket = (LocalAdAgingBucket)i.next();
                                                                        
        	AdAgingBucketDetails details = new AdAgingBucketDetails();
        		details.setAbCode(adAgingBucket.getAbCode());        		
                details.setAbName(adAgingBucket.getAbName());
                details.setAbDescription(adAgingBucket.getAbDescription());
                details.setAbType(adAgingBucket.getAbType());
                details.setAbEnable(adAgingBucket.getAbEnable());
                                                 	
        	list.add(details);
        }
        
        return list;
            
    }
    
    /**
     * @ejb:interface-method view-type="remote"
     **/    
    public void addAdAbEntry(com.util.AdAgingBucketDetails details, Integer AD_CMPNY) 
        throws GlobalRecordAlreadyExistException {
                    
        Debug.print("AdAgingBucketControllerBean addAdAbEntry");
        
        LocalAdAgingBucketHome adAgingBucketHome = null;
               
        LocalAdAgingBucket adAgingBucket = null;
                
        ArrayList list = new ArrayList();
        
        // Initialize EJB Home
        
        try {
            
            adAgingBucketHome = (LocalAdAgingBucketHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdAgingBucketHome.JNDI_NAME, LocalAdAgingBucketHome.class);
                                                      
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try { 
            
            adAgingBucket = adAgingBucketHome.findByAbName(details.getAbName(), AD_CMPNY);
            
            throw new GlobalRecordAlreadyExistException();
                                             
         } catch (GlobalRecordAlreadyExistException ex) {
        	
        	throw ex;
        	
         } catch (FinderException ex) {
        	 	
         } catch (Exception ex) {
            
            throw new EJBException(ex.getMessage());
            
         }
                            
        try {
        	
        	// create new aging bucket
        	
        	adAgingBucket = adAgingBucketHome.create(details.getAbName(), 
        	        details.getAbDescription(), details.getAbType(),
        	        details.getAbEnable(), AD_CMPNY); 
        	        
        } catch (Exception ex) {
        	
        	getSessionContext().setRollbackOnly();
            throw new EJBException(ex.getMessage());
        	
        }    
        
    }
    
    
    
    /**
     * @ejb:interface-method view-type="remote"
     **/
    public void updateAdAbEntry(com.util.AdAgingBucketDetails details, Integer AD_CMPNY) 
        throws GlobalRecordAlreadyExistException { 
                    
        Debug.print("AdAgingBucketControllerBean updateAdAbEntry");
        
        LocalAdAgingBucketHome adAgingBucketHome = null;                
                               
        LocalAdAgingBucket adAgingBucket = null;        
                
        ArrayList list = new ArrayList();
        
        // Initialize EJB Home
        
        try {

            adAgingBucketHome = (LocalAdAgingBucketHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdAgingBucketHome.JNDI_NAME, LocalAdAgingBucketHome.class);           

        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try { 
            
            LocalAdAgingBucket adExistingAgingBucket = adAgingBucketHome.findByAbName(details.getAbName(), AD_CMPNY);
            
            if (!adExistingAgingBucket.getAbCode().equals(details.getAbCode())) {
            
                 throw new GlobalRecordAlreadyExistException();
                 
            }
                                             
        } catch (GlobalRecordAlreadyExistException ex) {
        	
            throw ex;
        	
        } catch (FinderException ex) {
        	 	
        } catch (Exception ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
                        
        try {
        	        	
        	// find and update aging bucket
        	
        	adAgingBucket = adAgingBucketHome.findByPrimaryKey(details.getAbCode());
        	    	
                adAgingBucket.setAbName(details.getAbName());
                adAgingBucket.setAbDescription(details.getAbDescription());
                adAgingBucket.setAbType(details.getAbType());
                adAgingBucket.setAbEnable(details.getAbEnable());
                        	        	                            	
        } catch (Exception ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
    }
    
   /**
    * @ejb:interface-method view-type="remote"
    **/
    public void deleteAdAbEntry(Integer AB_CODE, Integer AD_CMPNY) 
        throws GlobalRecordAlreadyDeletedException,
               GlobalRecordAlreadyAssignedException {

      Debug.print("AdAgingBucketControllerBean deleteAdAbEntry");

      LocalAdAgingBucket adAgingBucket = null;
      LocalAdAgingBucketHome adAgingBucketHome = null;

      // Initialize EJB Home
        
      try {

          adAgingBucketHome = (LocalAdAgingBucketHome)EJBHomeFactory.
              lookUpLocalHome(LocalAdAgingBucketHome.JNDI_NAME, LocalAdAgingBucketHome.class);           
      
      } catch (NamingException ex) {

          throw new EJBException(ex.getMessage());

      }                
      
      try {
      	
         adAgingBucket = adAgingBucketHome.findByPrimaryKey(AB_CODE);
         
      } catch (FinderException ex) {
      	
         throw new GlobalRecordAlreadyDeletedException();
         
      } catch (Exception ex) {
      	
         throw new EJBException(ex.getMessage()); 
         
      }
      
      try {

          if (!adAgingBucket.getAdAgingBucketValues().isEmpty()) {
      
             throw new GlobalRecordAlreadyAssignedException();
             
          }
          
      } catch (GlobalRecordAlreadyAssignedException ex) {                        
         
          throw ex;
          
      } catch (Exception ex) {
       
            throw new EJBException(ex.getMessage());
           
      }                         
         
      try {
         	
	      adAgingBucket.remove();
	    
	  } catch (RemoveException ex) {
	 	
	      getSessionContext().setRollbackOnly();	    
	      throw new EJBException(ex.getMessage());
	    
	  } catch (Exception ex) {
	 	
	      getSessionContext().setRollbackOnly();	    
	      throw new EJBException(ex.getMessage());
	    
	  }	      
      
   }    
    
    // SessionBean methods

    /**
     * @ejb:create-method view-type="remote"
     **/
    public void ejbCreate() throws CreateException {

       Debug.print("AdAgingBucketControllerBean ejbCreate");
      
    }
}
