
/*
 * AdAgingBucketValueControllerBean.java
 *
 * Created on June 11, 2003, 9:59 AM
 * @author  Neil Andrew M. Ajero
 */

package com.ejb.txn;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;
import javax.ejb.RemoveException;
import javax.naming.NamingException;

import com.ejb.ad.LocalAdAgingBucket;
import com.ejb.ad.LocalAdAgingBucketHome;
import com.ejb.ad.LocalAdAgingBucketValue;
import com.ejb.ad.LocalAdAgingBucketValueHome;
import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.exception.GlobalRecordAlreadyDeletedException;
import com.ejb.exception.GlobalRecordAlreadyExistException;
import com.util.AbstractSessionBean;
import com.util.AdAgingBucketDetails;
import com.util.AdAgingBucketValueDetails;
import com.util.Debug;
import com.util.EJBHomeFactory;

/**
 * @ejb:bean name="AdAgingBucketValueControllerEJB"
 *           display-name="Used for entering aging bucket values"
 *           type="Stateless"
 *           view-type="remote"
 *           jndi-name="ejb/AdAgingBucketValueControllerEJB"
 *
 * @ejb:interface remote-class="com.ejb.txn.AdAgingBucketValueController"
 *                extends="javax.ejb.EJBObject"
 *
 * @ejb:home remote-class="com.ejb.txn.AdAgingBucketValueControllerHome"
 *           extends="javax.ejb.EJBHome"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="aduser"
 *                        role-link="aduserlink"
 *
 * @ejb:permission role-name="aduser"
 * 
*/

public class AdAgingBucketValueControllerBean extends AbstractSessionBean {
    
    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList getAdAvByAbCode(Integer AB_CODE, Integer AD_CMPNY) 
        throws GlobalNoRecordFoundException {
                    
        Debug.print("AdAgingBucketValueControllerBean getAdAvByAbCode");

        LocalAdAgingBucketValueHome adAgingBucketValueHome = null;

        Collection adAgingBucketValues = null;

        LocalAdAgingBucketValue adAgingBucketValue = null;

        ArrayList list = new ArrayList();
        
        // Initialize EJB Home

        try {
            
            adAgingBucketValueHome = (LocalAdAgingBucketValueHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdAgingBucketValueHome.JNDI_NAME, LocalAdAgingBucketValueHome.class);

        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }

        try {
            
            adAgingBucketValues = adAgingBucketValueHome.findByAbCode(AB_CODE, AD_CMPNY);
            
        } catch (FinderException ex) {
            
        } catch (Exception ex) {
            
            throw new EJBException(ex.getMessage());
        }

        if (adAgingBucketValues.isEmpty()) {
        	
            throw new GlobalNoRecordFoundException();
        	
        }
        
        Iterator i = adAgingBucketValues.iterator();
               
        while (i.hasNext()) {
        	
        	adAgingBucketValue = (LocalAdAgingBucketValue)i.next();
                                                                        
        	AdAgingBucketValueDetails details = new AdAgingBucketValueDetails();
        		details.setAvCode(adAgingBucketValue.getAvCode());        		
                details.setAvSequenceNumber(adAgingBucketValue.getAvSequenceNumber());
                details.setAvType(adAgingBucketValue.getAvType());
                details.setAvDaysFrom(adAgingBucketValue.getAvDaysFrom());
                details.setAvDaysTo(adAgingBucketValue.getAvDaysTo());
                details.setAvDescription(adAgingBucketValue.getAvDescription());
                                                        	
        	list.add(details);
        }
        
        return list;
            
    }
    
    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public com.util.AdAgingBucketDetails getAdAbByAbCode(Integer AB_CODE, Integer AD_CMPNY) {
                    
        Debug.print("AdAgingBucketValueControllerBean getAdAbByAbCode");
        		    	         	   	        	    
        LocalAdAgingBucketHome adAgingBucketHome = null;
                                       
        // Initialize EJB Home
        
        try {
            
            adAgingBucketHome = (LocalAdAgingBucketHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdAgingBucketHome.JNDI_NAME, LocalAdAgingBucketHome.class);              
            
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {
        	
        	LocalAdAgingBucket adAgingBucket = adAgingBucketHome.findByPrimaryKey(AB_CODE);
   
        	AdAgingBucketDetails details = new AdAgingBucketDetails();
        		details.setAbCode(adAgingBucket.getAbCode());        		
                details.setAbName(adAgingBucket.getAbName());
                details.setAbDescription(adAgingBucket.getAbDescription());
                details.setAbType(adAgingBucket.getAbType());
                details.setAbEnable(adAgingBucket.getAbEnable());
                        
        	return details;
        	       	
        } catch (Exception ex) {
        	
        	throw new EJBException(ex.getMessage());
        	
        }
        
	}      
	
    /**
     * @ejb:interface-method view-type="remote"
     **/    
    public void addAdAvEntry(com.util.AdAgingBucketValueDetails details, Integer AB_CODE, Integer AD_CMPNY) 
        throws GlobalRecordAlreadyExistException {
               
                    
        Debug.print("AdAgingBucketValueControllerBean addAdAvEntry");
        
        LocalAdAgingBucketValueHome adAgingBucketValueHome = null;
        LocalAdAgingBucketHome adAgingBucketHome = null;
               
        LocalAdAgingBucketValue adAgingBucketValue = null;
        LocalAdAgingBucket adAgingBucket = null;
        
        // Initialize EJB Home
        
        try {
            
            adAgingBucketHome = (LocalAdAgingBucketHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdAgingBucketHome.JNDI_NAME, LocalAdAgingBucketHome.class);
            adAgingBucketValueHome = (LocalAdAgingBucketValueHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdAgingBucketValueHome.JNDI_NAME, LocalAdAgingBucketValueHome.class);                
                                                      
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
                                   
        try {

            adAgingBucketValue = adAgingBucketValueHome.
              findByAvSequenceNumberAndAbCode(details.getAvSequenceNumber(), AB_CODE, AD_CMPNY);
        
            throw new GlobalRecordAlreadyExistException();
                                             
        } catch (GlobalRecordAlreadyExistException ex) {
        	
           throw ex;
        	
        } catch (FinderException ex) {
        	 	
        } catch (Exception ex) {
            
           throw new EJBException(ex.getMessage());
            
        }
        
        try {
        	
        	// create new aging bucket values
        	
        	adAgingBucketValue = adAgingBucketValueHome.create(details.getAvSequenceNumber(), 
        	        details.getAvType(), details.getAvDaysFrom(), details.getAvDaysTo(), 
        	        details.getAvDescription(), AD_CMPNY); 
        	        
        	adAgingBucket = adAgingBucketHome.findByPrimaryKey(AB_CODE);
		      	adAgingBucket.addAdAgingBucketValue(adAgingBucketValue);            	        
        	        
        } catch (Exception ex) {
        	
        	getSessionContext().setRollbackOnly();
        	
            throw new EJBException(ex.getMessage());
        	
        }        
                                    	                                        
    }	
    
    /**
     * @ejb:interface-method view-type="remote"
     **/    
    public void updateAdAvEntry(com.util.AdAgingBucketValueDetails details, Integer AB_CODE, Integer AD_CMPNY) 
        throws GlobalRecordAlreadyExistException {
               
                    
        Debug.print("AdAgingBucketValueControllerBean updateArAvEntry");
        
        LocalAdAgingBucketValueHome adAgingBucketValueHome = null;
        LocalAdAgingBucketHome adAgingBucketHome = null;
               
        LocalAdAgingBucketValue adAgingBucketValue = null;
        LocalAdAgingBucket adAgingBucket = null;
        
        // Initialize EJB Home
        
        try {
            
            adAgingBucketHome = (LocalAdAgingBucketHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdAgingBucketHome.JNDI_NAME, LocalAdAgingBucketHome.class);
            adAgingBucketValueHome = (LocalAdAgingBucketValueHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdAgingBucketValueHome.JNDI_NAME, LocalAdAgingBucketValueHome.class);                
                                                      
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }                                  

        try {

            adAgingBucketValue = adAgingBucketValueHome.
              findByAvSequenceNumberAndAbCode(details.getAvSequenceNumber(), AB_CODE, AD_CMPNY);
              
            if(adAgingBucketValue != null &&
                !adAgingBucketValue.getAvCode().equals(details.getAvCode()))  {  
        
               throw new GlobalRecordAlreadyExistException();
             
            } 
                                             
        } catch (GlobalRecordAlreadyExistException ex) {
        	
           throw ex;
        	
        } catch (FinderException ex) {
        	 	
        } catch (Exception ex) {
            
           throw new EJBException(ex.getMessage());
            
        }

        // Find and Update Aging Bucket Value 
               
        try {
        	
        	adAgingBucketValue = adAgingBucketValueHome.findByPrimaryKey(details.getAvCode());
        		
                adAgingBucketValue.setAvSequenceNumber(details.getAvSequenceNumber());
                adAgingBucketValue.setAvType(details.getAvType());
                adAgingBucketValue.setAvDaysFrom(details.getAvDaysFrom());
                adAgingBucketValue.setAvDaysTo(details.getAvDaysTo());
                adAgingBucketValue.setAvDescription(details.getAvDescription());

        	adAgingBucket = adAgingBucketHome.findByPrimaryKey(AB_CODE);
		      	adAgingBucket.addAdAgingBucketValue(adAgingBucketValue); 
      	        	                            	
        } catch (Exception ex) {
            
            throw new EJBException(ex.getMessage());
            
        }           	
                                                	                                        
    }	 
    
   /**
    * @ejb:interface-method view-type="remote"
    **/
    public void deleteAdAvEntry(Integer AV_CODE, Integer AD_CMPNY) 
        throws GlobalRecordAlreadyDeletedException {               

      Debug.print("AdAgingBucketValueControllerBean deleteAdAvEntry");

      LocalAdAgingBucketValue adAgingBucketValue = null;
      LocalAdAgingBucketValueHome adAgingBucketValueHome = null;

      // Initialize EJB Home
        
      try {

          adAgingBucketValueHome = (LocalAdAgingBucketValueHome)EJBHomeFactory.
              lookUpLocalHome(LocalAdAgingBucketValueHome.JNDI_NAME, LocalAdAgingBucketValueHome.class);           
      
      } catch (NamingException ex) {

          throw new EJBException(ex.getMessage());

      }                
      
      try {
      	
         adAgingBucketValue = adAgingBucketValueHome.findByPrimaryKey(AV_CODE);
         
      } catch (FinderException ex) {
      	
         throw new GlobalRecordAlreadyDeletedException();
         
      } catch (Exception ex) {
      	
         throw new EJBException(ex.getMessage()); 
         
      }
      
      try {
         	
	      adAgingBucketValue.remove();
	    
	  } catch (RemoveException ex) {
	 	
	      getSessionContext().setRollbackOnly();	    
	      throw new EJBException(ex.getMessage());
	    
	  } catch (Exception ex) {
	 	
	      getSessionContext().setRollbackOnly();	    
	      throw new EJBException(ex.getMessage());
	    
	  }	      
      
   }         
    
    // SessionBean methods

    /**
     * @ejb:create-method view-type="remote"
     **/
    public void ejbCreate() throws CreateException {

       Debug.print("AdAgingBucketValueControllerBean ejbCreate");
      
    }
}
