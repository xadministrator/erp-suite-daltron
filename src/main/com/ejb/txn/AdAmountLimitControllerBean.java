
/*
 * AdAmountLimitControllerBean.java
 *
 * Created on March 22, 2004, 8:16 PM
 *
 * @author  Neil Andrew M. Ajero
 */

package com.ejb.txn;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;
import javax.ejb.RemoveException;
import javax.naming.NamingException;

import com.ejb.ad.LocalAdAmountLimit;
import com.ejb.ad.LocalAdAmountLimitHome;
import com.ejb.ad.LocalAdApprovalCoaLine;
import com.ejb.ad.LocalAdApprovalCoaLineHome;
import com.ejb.ad.LocalAdApprovalDocument;
import com.ejb.ad.LocalAdApprovalDocumentHome;
import com.ejb.ad.LocalAdCompany;
import com.ejb.ad.LocalAdCompanyHome;
import com.ejb.ad.LocalAdLookUpValue;
import com.ejb.ad.LocalAdLookUpValueHome;
import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.exception.GlobalRecordAlreadyDeletedException;
import com.ejb.exception.GlobalRecordAlreadyExistException;
import com.ejb.gl.LocalGlChartOfAccount;
import com.ejb.gl.LocalGlChartOfAccountHome;
import com.util.AbstractSessionBean;
import com.util.AdAmountLimitDetails;
import com.util.AdApprovalDocumentDetails;
import com.util.AdModApprovalCoaLineDetails;
import com.util.Debug;
import com.util.EJBHomeFactory;

/**
 * @ejb:bean name="AdAmountLimitControllerEJB"
 *           display-name="Used for entering amount limits"
 *           type="Stateless"
 *           view-type="remote"
 *           jndi-name="ejb/AdAmountLimitControllerEJB"
 *
 * @ejb:interface remote-class="com.ejb.txn.AdAmountLimitController"
 *                extends="javax.ejb.EJBObject"
 *
 * @ejb:home remote-class="com.ejb.txn.AdAmountLimitControllerHome"
 *           extends="javax.ejb.EJBHome"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="aduser"
 *                        role-link="aduserlink"
 *
 * @ejb:permission role-name="aduser"
 * 
*/

public class AdAmountLimitControllerBean extends AbstractSessionBean{
	
	
	
	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public ArrayList getAdLvDEPARTMENT(Integer AD_CMPNY) {
		
		Debug.print("AdAmountLimitControllerBean getAdLvDEPARTMENT");
		
		LocalAdLookUpValueHome adLookUpValueHome = null;               
		
		ArrayList list = new ArrayList();
		
		// Initialize EJB Home
		
		try {
			
			adLookUpValueHome = (LocalAdLookUpValueHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdLookUpValueHome.JNDI_NAME, LocalAdLookUpValueHome.class);
			
		} catch (NamingException ex) {
			
			throw new EJBException(ex.getMessage());
			
		}        
		
		try {
			
			Collection adLookUpValues = adLookUpValueHome.findByLuName("AD DEPARTMENT", AD_CMPNY);
			
			Iterator i = adLookUpValues.iterator();
			
			while (i.hasNext()) {
				
				LocalAdLookUpValue adLookUpValue = (LocalAdLookUpValue)i.next();
				
				list.add(adLookUpValue.getLvName());
				
			}
			
			return list;
			
		} catch (Exception ex) {
			
			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());
			
		}
		
	}
	
	
    
    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList getAdCalByAdcCode(Integer ADC_CODE, Integer AD_CMPNY) 
        throws GlobalNoRecordFoundException {
                    
        Debug.print("AdAmountLimitControllerBean getAdCalByAdcCode");

        LocalAdAmountLimitHome adAmountLimitHome = null;

        ArrayList list = new ArrayList();
        
        // Initialize EJB Home

        try {
            
            adAmountLimitHome = (LocalAdAmountLimitHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdAmountLimitHome.JNDI_NAME, LocalAdAmountLimitHome.class);

        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }

	    try {
	            
	        Collection adAmountLimits = adAmountLimitHome.findByAdcCode(ADC_CODE, AD_CMPNY);

	        if (adAmountLimits.isEmpty()) {
	        	
	            throw new GlobalNoRecordFoundException();
	        	
	        }
	        
	        Iterator i = adAmountLimits.iterator();
	               
	        while (i.hasNext()) {
	        	
	        	LocalAdAmountLimit adAmountLimit = (LocalAdAmountLimit)i.next();
	                                                                        
	        	AdAmountLimitDetails details = new AdAmountLimitDetails();
	        		details.setCalCode(adAmountLimit.getCalCode());
	        		details.setCalDept(adAmountLimit.getCalDept());
	                details.setCalAmountLimit(adAmountLimit.getCalAmountLimit());
	                details.setCalAndOr(adAmountLimit.getCalAndOr());
	                                                        	
	        	list.add(details);
	        }
	        
	        return list;
            
        } catch (GlobalNoRecordFoundException ex) {
	    	
	    	throw ex;    
	        
        } catch (Exception ex) {
        	
        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());
        	
        }
            
    } 
    
    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public com.util.AdApprovalDocumentDetails getAdAdcByAdcCode(Integer ADC_CODE, Integer AD_CMPNY) {
                    
        Debug.print("AdAmountLimitControllerBean getAdAdcByAdcCode");
        		    	         	   	        	    
        LocalAdApprovalDocumentHome adApprovalDocumentHome = null;
                                       
        // Initialize EJB Home
        
        try {
            
            adApprovalDocumentHome = (LocalAdApprovalDocumentHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdApprovalDocumentHome.JNDI_NAME, LocalAdApprovalDocumentHome.class);              
            
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {
        	
        	LocalAdApprovalDocument adApprovalDocument = adApprovalDocumentHome.findByPrimaryKey(ADC_CODE);
   
        	AdApprovalDocumentDetails details = new AdApprovalDocumentDetails();
        		details.setAdcCode(adApprovalDocument.getAdcCode());        		
                details.setAdcType(adApprovalDocument.getAdcType());
                        
        	return details;
        	       	
        } catch (Exception ex) {
        	
        	throw new EJBException(ex.getMessage());
        	
        }
        
	}
	
    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList getAdCalByAclCode(Integer ACL_CODE, Integer AD_CMPNY) 
        throws GlobalNoRecordFoundException {
                    
        Debug.print("AdAmountLimitControllerBean getAdCalByAclCode");

        LocalAdAmountLimitHome adAmountLimitHome = null;

        ArrayList list = new ArrayList();
        
        // Initialize EJB Home

        try {
            
            adAmountLimitHome = (LocalAdAmountLimitHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdAmountLimitHome.JNDI_NAME, LocalAdAmountLimitHome.class);

        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }

	    try {
	            
	        Collection adAmountLimits = adAmountLimitHome.findByAclCode(ACL_CODE, AD_CMPNY);

	        if (adAmountLimits.isEmpty()) {
	        	
	            throw new GlobalNoRecordFoundException();
	        	
	        }
	        
	        Iterator i = adAmountLimits.iterator();
	               
	        while (i.hasNext()) {
	        	
	        	LocalAdAmountLimit adAmountLimit = (LocalAdAmountLimit)i.next();
	                                                                        
	        	AdAmountLimitDetails details = new AdAmountLimitDetails();
	        		details.setCalCode(adAmountLimit.getCalCode());    
	        		details.setCalDept(adAmountLimit.getCalDept());
	                details.setCalAmountLimit(adAmountLimit.getCalAmountLimit());
	                details.setCalAndOr(adAmountLimit.getCalAndOr());
	                                                        	
	        	list.add(details);
	        }
	        
	        return list;
            
        } catch (GlobalNoRecordFoundException ex) {
	    	
	    	throw ex;    
	        
        } catch (Exception ex) {
        	
        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());
        	
        }
            
    } 
    
    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public com.util.AdModApprovalCoaLineDetails getAdAclByAclCode(Integer ACL_CODE, Integer AD_CMPNY) {
                    
        Debug.print("AdAmountLimitControllerBean getAdAclByAclCode");
        		    	         	   	        	    
        LocalAdApprovalCoaLineHome adApprovalCoaLineHome = null;
        LocalGlChartOfAccountHome glChartOfAccountHome = null;
                      
        // Initialize EJB Home
        
        try {
            
            adApprovalCoaLineHome = (LocalAdApprovalCoaLineHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdApprovalCoaLineHome.JNDI_NAME, LocalAdApprovalCoaLineHome.class);
            glChartOfAccountHome = (LocalGlChartOfAccountHome)EJBHomeFactory.
                lookUpLocalHome(LocalGlChartOfAccountHome.JNDI_NAME, LocalGlChartOfAccountHome.class);

        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {
        	
        	LocalAdApprovalCoaLine adApprovalCoaLine = adApprovalCoaLineHome.findByPrimaryKey(ACL_CODE);
        	
        	AdModApprovalCoaLineDetails mdetails = new AdModApprovalCoaLineDetails();
        	mdetails.setAclCode(adApprovalCoaLine.getAclCode());  
        	
        	LocalGlChartOfAccount glChartOfAccount = glChartOfAccountHome.findByPrimaryKey(adApprovalCoaLine.getGlChartOfAccount().getCoaCode());
        	
        	mdetails.setAclCoaAccountNumber(glChartOfAccount.getCoaAccountNumber());
        	mdetails.setAclCoaDescription(glChartOfAccount.getCoaAccountDescription());       		 
        	
        	return mdetails;
        	
        } catch (Exception ex) {
        	
        	throw new EJBException(ex.getMessage());
        	
        }
        
	}  	      
	
    /**
     * @ejb:interface-method view-type="remote"
     **/    
    public void addAdCalEntry(com.util.AdAmountLimitDetails details, Integer ADC_CODE, Integer ACL_CODE, Integer AD_CMPNY) 
        throws GlobalRecordAlreadyExistException {
     
        Debug.print("AdAmountLimitControllerBean addAdCalEntry");
        
        LocalAdAmountLimitHome adAmountLimitHome = null;
        LocalAdApprovalDocumentHome adApprovalDocumentHome = null;
        LocalAdApprovalCoaLineHome adApprovalCoaLineHome = null;
        
        // Initialize EJB Home
        
        try {
            
            adApprovalDocumentHome = (LocalAdApprovalDocumentHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdApprovalDocumentHome.JNDI_NAME, LocalAdApprovalDocumentHome.class);
            adApprovalCoaLineHome = (LocalAdApprovalCoaLineHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdApprovalCoaLineHome.JNDI_NAME, LocalAdApprovalCoaLineHome.class);                
            adAmountLimitHome = (LocalAdAmountLimitHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdAmountLimitHome.JNDI_NAME, LocalAdAmountLimitHome.class);                
                                                      
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {
	               
            LocalAdAmountLimit adAmountLimit = null;
        	
        	try {
        		
        		if (ADC_CODE != null) {

		            adAmountLimit = adAmountLimitHome.
		            		findByCalDeptAndCalAmountLimitAndAdcCode(details.getCalDept(), details.getCalAmountLimit(), ADC_CODE, AD_CMPNY);
	                                             
	                  throw new GlobalRecordAlreadyExistException();
	                  
	            } else {
	            	
		            adAmountLimit = adAmountLimitHome.
		              findByCalAmountLimitAndAclCode(details.getCalAmountLimit(), ACL_CODE, AD_CMPNY);	            	
	            	
	                  throw new GlobalRecordAlreadyExistException();
	                  
	            }
	            	                              
            } catch (GlobalRecordAlreadyExistException ex) {
	        	
	        	throw ex;
                  
            } catch (FinderException ex) {
            	
            }

        	// create new amount limit
        	
        	adAmountLimit = adAmountLimitHome.create(
        			details.getCalDept(), details.getCalAmountLimit(), details.getCalAndOr(), AD_CMPNY); 
        	        
        	if (ADC_CODE != null) {
        	        
 		       	LocalAdApprovalDocument adApprovalDocument = adApprovalDocumentHome.findByPrimaryKey(ADC_CODE);
			      	adApprovalDocument.addAdAmountLimit(adAmountLimit); 
			      	
			} else {
				
				LocalAdApprovalCoaLine adApprovalCoaLine = adApprovalCoaLineHome.findByPrimaryKey(ACL_CODE);
					adApprovalCoaLine.addAdAmountLimit(adAmountLimit);
					
			}
		      	
		} catch (GlobalRecordAlreadyExistException ex) {
        	
        	getSessionContext().setRollbackOnly();
        	throw ex;      	           	        
        	        
        } catch (Exception ex) {
        	
        	Debug.printStackTrace(ex);
        	getSessionContext().setRollbackOnly();
        	throw new EJBException(ex.getMessage());
        	
        }
            
    } 	
    
    /**
     * @ejb:interface-method view-type="remote"
     **/    
    public void updateAdCalEntry(com.util.AdAmountLimitDetails details, Integer ADC_CODE, Integer ACL_CODE, Integer AD_CMPNY) 
        throws GlobalRecordAlreadyExistException {
      
        Debug.print("AdAmountLimitControllerBean updateAdCalEntry");
        
        LocalAdAmountLimitHome adAmountLimitHome = null;
        LocalAdApprovalDocumentHome adApprovalDocumentHome = null;
        LocalAdApprovalCoaLineHome adApprovalCoaLineHome = null;
        
        // Initialize EJB Home
        
        try {
            
            adApprovalDocumentHome = (LocalAdApprovalDocumentHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdApprovalDocumentHome.JNDI_NAME, LocalAdApprovalDocumentHome.class);
            adApprovalCoaLineHome = (LocalAdApprovalCoaLineHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdApprovalCoaLineHome.JNDI_NAME, LocalAdApprovalCoaLineHome.class);                
            adAmountLimitHome = (LocalAdAmountLimitHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdAmountLimitHome.JNDI_NAME, LocalAdAmountLimitHome.class);                
                                                      
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }                                  

        try { 	
        	               
            LocalAdAmountLimit adAmountLimit = null;
        	
        	try {
        		
        		if (ADC_CODE != null) {

		            adAmountLimit = adAmountLimitHome.
		            		findByCalDeptAndCalAmountLimitAndAdcCode(details.getCalDept(), details.getCalAmountLimit(), ADC_CODE, AD_CMPNY);
		              
		        } else {
		        	
		            adAmountLimit = adAmountLimitHome.
		              findByCalAmountLimitAndAclCode(details.getCalAmountLimit(), ACL_CODE, AD_CMPNY);
		              		        	
		        }
	              
	            if(adAmountLimit != null &&
	                !adAmountLimit.getCalCode().equals(details.getCalCode()))  {  
	        
	               throw new GlobalRecordAlreadyExistException();
	             
	            } 
                                             
            } catch (GlobalRecordAlreadyExistException ex) {
	        	
	        	throw ex;
                  
            } catch (FinderException ex) {
            	
            }

           // Find and Update Amount Limit 
        	
        	adAmountLimit = adAmountLimitHome.findByPrimaryKey(details.getCalCode());
        		adAmountLimit.setCalDept(details.getCalDept());
                adAmountLimit.setCalAmountLimit(details.getCalAmountLimit());
                adAmountLimit.setCalAndOr(details.getCalAndOr());

        	if (ADC_CODE != null) {
        	        
 		       	LocalAdApprovalDocument adApprovalDocument = adApprovalDocumentHome.findByPrimaryKey(ADC_CODE);
			      	adApprovalDocument.addAdAmountLimit(adAmountLimit); 
			      	
			} else {
				
				LocalAdApprovalCoaLine adApprovalCoaLine = adApprovalCoaLineHome.findByPrimaryKey(ACL_CODE);
					adApprovalCoaLine.addAdAmountLimit(adAmountLimit);
					
			}
      	        	                            	
        } catch (GlobalRecordAlreadyExistException ex) {
        	
        	getSessionContext().setRollbackOnly();
        	throw ex;      	           	        
        	        
        } catch (Exception ex) {
        	
        	Debug.printStackTrace(ex);
        	getSessionContext().setRollbackOnly();
        	throw new EJBException(ex.getMessage());
        	
        }
            
    }	 
    
   /**
    * @ejb:interface-method view-type="remote"
    **/
    public void deleteAdCalEntry(Integer CAL_CODE, Integer AD_CMPNY) 
        throws GlobalRecordAlreadyDeletedException {               

      Debug.print("AdAmountLimitControllerBean deleteAdCalEntry");

      LocalAdAmountLimitHome adAmountLimitHome = null;

      // Initialize EJB Home
        
      try {

          adAmountLimitHome = (LocalAdAmountLimitHome)EJBHomeFactory.
              lookUpLocalHome(LocalAdAmountLimitHome.JNDI_NAME, LocalAdAmountLimitHome.class);           
      
      } catch (NamingException ex) {

          throw new EJBException(ex.getMessage());

      }                
      
      try {
	         
	       LocalAdAmountLimit adAmountLimit = adAmountLimitHome.findByPrimaryKey(CAL_CODE);

	       adAmountLimit.remove();
	      
	  } catch (FinderException ex) {
	      	
	      throw new GlobalRecordAlreadyDeletedException();

	  } catch (RemoveException ex) {
	 	
	      getSessionContext().setRollbackOnly();	    
	      throw new EJBException(ex.getMessage());
	    
	  } catch (Exception ex) {
	 	
	      getSessionContext().setRollbackOnly();	    
	      throw new EJBException(ex.getMessage());
	    
	  }	      
      
   }
   
   /**
    * @ejb:interface-method view-type="remote"
    * @jboss:method-attributes read-only="true"
    **/
    public short getGlFcPrecisionUnit(Integer AD_CMPNY) {

       Debug.print("AdAmountLimitControllerBean getGlFcPrecisionUnit");

      
       LocalAdCompanyHome adCompanyHome = null;
      
     
       // Initialize EJB Home
        
       try {
            
           adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
              lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);       
            
       } catch (NamingException ex) {
            
           throw new EJBException(ex.getMessage());
            
       }

       try {
       	
         LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
            
         return  adCompany.getGlFunctionalCurrency().getFcPrecision();
                                     
       } catch (Exception ex) {
       	 
       	 Debug.printStackTrace(ex);
         throw new EJBException(ex.getMessage());
         
       }

    }            
    
    // SessionBean methods

    /**
     * @ejb:create-method view-type="remote"
     **/
    public void ejbCreate() throws CreateException {

       Debug.print("AdAmountLimitControllerBean ejbCreate");
      
    }
}
