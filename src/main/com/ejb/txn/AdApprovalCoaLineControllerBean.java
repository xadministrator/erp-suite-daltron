
/*
 * AdApprovalCoaLineControllerBean.java
 *
 * Created on March 23, 2004, 6:13 PM
 *
 * @author  Neil Andrew M. Ajero
 */

package com.ejb.txn;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;
import javax.ejb.RemoveException;
import javax.naming.NamingException;

import com.ejb.ad.LocalAdApprovalCoaLine;
import com.ejb.ad.LocalAdApprovalCoaLineHome;
import com.ejb.ad.LocalAdApprovalDocument;
import com.ejb.ad.LocalAdApprovalDocumentHome;
import com.ejb.ad.LocalAdCompany;
import com.ejb.ad.LocalAdCompanyHome;
import com.ejb.exception.GlobalAccountNumberInvalidException;
import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.exception.GlobalRecordAlreadyDeletedException;
import com.ejb.exception.GlobalRecordAlreadyExistException;
import com.ejb.gl.LocalGlChartOfAccount;
import com.ejb.gl.LocalGlChartOfAccountHome;
import com.util.AbstractSessionBean;
import com.util.AdApprovalDocumentDetails;
import com.util.AdModApprovalCoaLineDetails;
import com.util.Debug;
import com.util.EJBHomeFactory;

/**
 * @ejb:bean name="AdApprovalCoaLineControllerEJB"
 *           display-name="Used for assigning coa to approval document"
 *           type="Stateless"
 *           view-type="remote"
 *           jndi-name="ejb/AdApprovalCoaLineControllerEJB"
 *
 * @ejb:interface remote-class="com.ejb.txn.AdApprovalCoaLineController"
 *                extends="javax.ejb.EJBObject"
 *
 * @ejb:home remote-class="com.ejb.txn.AdApprovalCoaLineControllerHome"
 *           extends="javax.ejb.EJBHome"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="aduser"
 *                        role-link="aduserlink"
 *
 * @ejb:permission role-name="aduser"
 * 
*/

public class AdApprovalCoaLineControllerBean extends AbstractSessionBean {
    
    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList getAdAclAll(Integer AD_CMPNY) 
        throws GlobalNoRecordFoundException {
                    
        Debug.print("AdApprovalCoaLineControllerBean getAdAclAll");

        LocalAdApprovalCoaLineHome adApprovalCoaLineHome = null;
        LocalGlChartOfAccountHome glChartOfAccountHome = null;

        ArrayList list = new ArrayList();
        
        // Initialize EJB Home

        try {
            
            adApprovalCoaLineHome = (LocalAdApprovalCoaLineHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdApprovalCoaLineHome.JNDI_NAME, LocalAdApprovalCoaLineHome.class);
            glChartOfAccountHome = (LocalGlChartOfAccountHome)EJBHomeFactory.
                lookUpLocalHome(LocalGlChartOfAccountHome.JNDI_NAME, LocalGlChartOfAccountHome.class);

        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
  
	    try {
	            
	        Collection adApprovalCoaLines = adApprovalCoaLineHome.findAclAll(AD_CMPNY);

	        if (adApprovalCoaLines.isEmpty()) {
	        	
	            throw new GlobalNoRecordFoundException();
	        	
	        }
	        
	        Iterator i = adApprovalCoaLines.iterator();
	               
	        while (i.hasNext()) {
	        	
	        	LocalAdApprovalCoaLine adApprovalCoaLine = (LocalAdApprovalCoaLine)i.next();
	        	
	        	AdModApprovalCoaLineDetails mdetails = new AdModApprovalCoaLineDetails();
	        	mdetails.setAclCode(adApprovalCoaLine.getAclCode());
	        	
	        	// get account number
	        	
	        	LocalGlChartOfAccount glChartOfAccount = glChartOfAccountHome.findByPrimaryKey(adApprovalCoaLine.getGlChartOfAccount().getCoaCode());
	        	
	        	mdetails.setAclCoaAccountNumber(glChartOfAccount.getCoaAccountNumber());
	        	mdetails.setAclCoaDescription(glChartOfAccount.getCoaAccountDescription());       		 
	        	
	        	list.add(mdetails);
	        }
	        
	        return list;
            
        } catch (GlobalNoRecordFoundException ex) {
	    	
	    	throw ex;    
	        
        } catch (Exception ex) {
        	
        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());
        	
        }
            
    } 
    
    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public com.util.AdApprovalDocumentDetails getAdAdcByAdcCode(Integer ADC_CODE, Integer AD_CMPNY) {
                    
        Debug.print("AdApprovalCoaLineControllerBean getAdAdcByAdcCode");
        		    	         	   	        	    
        LocalAdApprovalDocumentHome adApprovalDocumentHome = null;
                                       
        // Initialize EJB Home
        
        try {
            
            adApprovalDocumentHome = (LocalAdApprovalDocumentHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdApprovalDocumentHome.JNDI_NAME, LocalAdApprovalDocumentHome.class);              
            
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {
        	
        	LocalAdApprovalDocument adApprovalDocument = adApprovalDocumentHome.findByPrimaryKey(ADC_CODE);
   
        	AdApprovalDocumentDetails details = new AdApprovalDocumentDetails();      		
                details.setAdcType(adApprovalDocument.getAdcType());
                        
        	return details;
        	       	
        } catch (Exception ex) {
        	
        	throw new EJBException(ex.getMessage());
        	
        }
        
	}      
	
    /**
     * @ejb:interface-method view-type="remote"
     **/    
    public void addAdAclEntry(String COA_ACCNT_NMBR, Integer AD_CMPNY) 
        throws GlobalRecordAlreadyExistException,
               GlobalAccountNumberInvalidException {
     
        Debug.print("AdApprovalCoaLineControllerBean addAdAclEntry");
        
        LocalAdApprovalCoaLineHome adApprovalCoaLineHome = null;
        LocalGlChartOfAccountHome glChartOfAccountHome = null;
        
        // Initialize EJB Home
        
        try {
            
            adApprovalCoaLineHome = (LocalAdApprovalCoaLineHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdApprovalCoaLineHome.JNDI_NAME, LocalAdApprovalCoaLineHome.class);
            glChartOfAccountHome = (LocalGlChartOfAccountHome)EJBHomeFactory.
                lookUpLocalHome(LocalGlChartOfAccountHome.JNDI_NAME, LocalGlChartOfAccountHome.class);                
                                                      
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
                                   
        try {
	               
            LocalAdApprovalCoaLine adApprovalCoaLine = null;
            LocalGlChartOfAccount glChartOfAccount = null;
            
            // Check if valid account number
            
            try {
            	
            	glChartOfAccount = glChartOfAccountHome.findByCoaAccountNumber(COA_ACCNT_NMBR, AD_CMPNY);
            	
            } catch (FinderException ex) {
            	
            	throw new GlobalAccountNumberInvalidException();
            	
            }
            
            // Check if there is already a COA line with this account number
            
            try {
            	
            	adApprovalCoaLine = adApprovalCoaLineHome.findByCoaAccountNumber(COA_ACCNT_NMBR, AD_CMPNY);
            	throw new GlobalRecordAlreadyExistException();
            	
            } catch (FinderException ex) {
            	
            }
            

        	// create new amount limit
        	
        	adApprovalCoaLine = adApprovalCoaLineHome.create(AD_CMPNY); 
        	glChartOfAccount.addAdApprovalCoaLine(adApprovalCoaLine);
		      	
		} catch (GlobalAccountNumberInvalidException ex) {
        	
        	getSessionContext().setRollbackOnly();
        	throw ex;  
        	
        } catch (GlobalRecordAlreadyExistException ex) {
        	
        	getSessionContext().setRollbackOnly();
        	throw ex;
		      	        	        
        } catch (Exception ex) {
        	
        	Debug.printStackTrace(ex);
        	getSessionContext().setRollbackOnly();
        	throw new EJBException(ex.getMessage());
        	
        }
            
    } 	
    
    /**
     * @ejb:interface-method view-type="remote"
     **/    
    public void updateAdAclEntry(com.util.AdApprovalCoaLineDetails details, String COA_ACCNT_NMBR, Integer AD_CMPNY) 
        throws GlobalRecordAlreadyExistException,
               GlobalAccountNumberInvalidException {
      
        Debug.print("AdApprovalCoaLineControllerBean updateAdCalEntry");
        
        LocalGlChartOfAccountHome glChartOfAccountHome = null;
        LocalAdApprovalCoaLineHome adApprovalCoaLineHome = null;        
        
        // Initialize EJB Home
        
        try {
            
            glChartOfAccountHome = (LocalGlChartOfAccountHome)EJBHomeFactory.
                lookUpLocalHome(LocalGlChartOfAccountHome.JNDI_NAME, LocalGlChartOfAccountHome.class);
            adApprovalCoaLineHome = (LocalAdApprovalCoaLineHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdApprovalCoaLineHome.JNDI_NAME, LocalAdApprovalCoaLineHome.class);                
                                                      
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }                                  

        try { 	
        	               
            LocalAdApprovalCoaLine adApprovalCoaLine = null;
            LocalGlChartOfAccount glChartOfAccount = null;            
        	
        	try {

	            adApprovalCoaLine = adApprovalCoaLineHome.findByAclCodeAndCoaAccountNumber(details.getAclCode(), COA_ACCNT_NMBR, AD_CMPNY);
				throw new GlobalRecordAlreadyExistException();

            } catch (FinderException ex) {            	
            	
            }
            
            try {
            
                
            	glChartOfAccount = glChartOfAccountHome.findByCoaAccountNumber(COA_ACCNT_NMBR, AD_CMPNY);
            	
            } catch (FinderException ex) {
            	
            	throw new GlobalAccountNumberInvalidException();
            	
            }

           // Find and Update Coa Line
           
            adApprovalCoaLine = adApprovalCoaLineHome.findByPrimaryKey(details.getAclCode());

        	glChartOfAccount.addAdApprovalCoaLine(adApprovalCoaLine);
      	        	                            	
        } catch (GlobalRecordAlreadyExistException ex) {
        	
        	getSessionContext().setRollbackOnly();
        	throw ex;
        	
        } catch (GlobalAccountNumberInvalidException ex) {

         	getSessionContext().setRollbackOnly();
         	throw ex;
        	        
        } catch (Exception ex) {
        	
        	Debug.printStackTrace(ex);
        	getSessionContext().setRollbackOnly();
        	throw new EJBException(ex.getMessage());
        	
        }
            
    }	 
    
   /**
    * @ejb:interface-method view-type="remote"
    **/
    public void deleteAdAclEntry(Integer ACL_CODE, Integer AD_CMPNY) 
        throws GlobalRecordAlreadyDeletedException {               

      Debug.print("AdApprovalCoaLineControllerBean deleteAdAclEntry");

      LocalAdApprovalCoaLineHome adApprovalCoaLineHome = null;

      // Initialize EJB Home
        
      try {

          adApprovalCoaLineHome = (LocalAdApprovalCoaLineHome)EJBHomeFactory.
              lookUpLocalHome(LocalAdApprovalCoaLineHome.JNDI_NAME, LocalAdApprovalCoaLineHome.class);           
      
      } catch (NamingException ex) {

          throw new EJBException(ex.getMessage());

      }                
      
      try {
	         
	       LocalAdApprovalCoaLine adApprovalCoaLine = adApprovalCoaLineHome.findByPrimaryKey(ACL_CODE);

	       adApprovalCoaLine.remove();
	      
	  } catch (FinderException ex) {
	      	
	      throw new GlobalRecordAlreadyDeletedException();

	  } catch (RemoveException ex) {
	 	
	      getSessionContext().setRollbackOnly();	    
	      throw new EJBException(ex.getMessage());
	    
	  } catch (Exception ex) {
	 	
	      getSessionContext().setRollbackOnly();	    
	      throw new EJBException(ex.getMessage());
	    
	  }	      
      
   }
   
   /**
    * @ejb:interface-method view-type="remote"
    * @jboss:method-attributes read-only="true"
    **/
    public short getGlFcPrecisionUnit(Integer AD_CMPNY) {

       Debug.print("AdApprovalCoaLineControllerBean getGlFcPrecisionUnit");

      
       LocalAdCompanyHome adCompanyHome = null;
      
     
       // Initialize EJB Home
        
       try {
            
           adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
              lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);       
            
       } catch (NamingException ex) {
            
           throw new EJBException(ex.getMessage());
            
       }

       try {
       	
         LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
            
         return  adCompany.getGlFunctionalCurrency().getFcPrecision();
                                     
       } catch (Exception ex) {
       	 
       	 Debug.printStackTrace(ex);
         throw new EJBException(ex.getMessage());
         
       }

    }            
    
    // SessionBean methods

    /**
     * @ejb:create-method view-type="remote"
     **/
    public void ejbCreate() throws CreateException {

       Debug.print("AdApprovalCoaLineControllerBean ejbCreate");
      
    }
}
