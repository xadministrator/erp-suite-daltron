package com.ejb.txn;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.naming.NamingException;

import com.ejb.ad.LocalAdApprovalDocument;
import com.ejb.ad.LocalAdApprovalDocumentHome;
import com.ejb.exception.GlobalNoRecordFoundException;
import com.util.AbstractSessionBean;
import com.util.AdApprovalDocumentDetails;
import com.util.Debug;
import com.util.EJBHomeFactory;

/**
 * @ejb:bean name="AdApprovalDocumentControllerEJB"
 *           display-name="Used for editing approval document"
 *           type="Stateless"
 *           view-type="remote"
 *           jndi-name="ejb/AdApprovalDocumentControllerEJB"
 *
 * @ejb:interface remote-class="com.ejb.txn.AdApprovalDocumentController"
 *                extends="javax.ejb.EJBObject"
 *
 * @ejb:home remote-class="com.ejb.txn.AdApprovalDocumentControllerHome"
 *           extends="javax.ejb.EJBHome"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="aduser"
 *                        role-link="aduserlink"
 *
 * @ejb:permission role-name="aduser"
 * 
*/

public class AdApprovalDocumentControllerBean extends AbstractSessionBean {

    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList getAdAdcAll(Integer AD_CMPNY)
        throws GlobalNoRecordFoundException {
                    
        Debug.print("AdApprovalControllerBean getAdAdcAll");

        LocalAdApprovalDocumentHome adApprovalDocumentHome = null;

        ArrayList list = new ArrayList();
        
        // Initialize EJB Home

        try {

            adApprovalDocumentHome = (LocalAdApprovalDocumentHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdApprovalDocumentHome.JNDI_NAME, LocalAdApprovalDocumentHome.class);

        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {
        	
        	Collection adApprovalDocuments = adApprovalDocumentHome.findAdcAll(AD_CMPNY);
	
	        if (adApprovalDocuments.isEmpty()) {
	        	
	            throw new GlobalNoRecordFoundException();
            
            }
              
            Iterator i = adApprovalDocuments.iterator();
	               
	        while (i.hasNext()) {
	        	
	        	LocalAdApprovalDocument adApprovalDocument = (LocalAdApprovalDocument)i.next();
	            
				AdApprovalDocumentDetails details = new AdApprovalDocumentDetails();
				    details.setAdcCode(adApprovalDocument.getAdcCode());
				    details.setAdcType(adApprovalDocument.getAdcType());
				    details.setAdcPrintOption(adApprovalDocument.getAdcPrintOption());
				    details.setAdcAllowDuplicate(adApprovalDocument.getAdcAllowDuplicate());
				    details.setAdcTrackDuplicate(adApprovalDocument.getAdcTrackDuplicate());
				    details.setAdcEnableCreditLimitChecking(adApprovalDocument.getAdcEnableCreditLimitChecking());
			    
			    list.add(details);    
			}
  
	        return list;
	        
	    } catch (GlobalNoRecordFoundException ex) {
	    	
	    	throw ex;    
	        
        } catch (Exception ex) {
        	
        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());
        	
        }
            
    }    
    
    /**
     * @ejb:interface-method view-type="remote"
     **/
    public void updateAdAdcEntry(com.util.AdApprovalDocumentDetails details, Integer AD_CMPNY) {
                    
        Debug.print("AdApprovalControllerBean updateAdAdcEntry");
        
        LocalAdApprovalDocumentHome adApprovalDocumentHome = null;                
                
        // Initialize EJB Home
        
        try {

            adApprovalDocumentHome = (LocalAdApprovalDocumentHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdApprovalDocumentHome.JNDI_NAME, LocalAdApprovalDocumentHome.class);           

        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {
        	
        	LocalAdApprovalDocument adApprovalDocument = null;
        	
			// update approval document setup
			
			adApprovalDocument = adApprovalDocumentHome.findByPrimaryKey(details.getAdcCode());
				
			    adApprovalDocument.setAdcType(details.getAdcType());
			    adApprovalDocument.setAdcPrintOption(details.getAdcPrintOption());
			    adApprovalDocument.setAdcAllowDuplicate(details.getAdcAllowDuplicate());
			    adApprovalDocument.setAdcTrackDuplicate(details.getAdcTrackDuplicate());
			    
			    if (details.getAdcType().equalsIgnoreCase("AR INVOICE"))
			    	adApprovalDocument.setAdcEnableCreditLimitChecking(details.getAdcEnableCreditLimitChecking());
			    
        } catch (Exception ex) {
        	
        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());
        	
        }
            
    }    
    
    // SessionBean methods

    /**
     * @ejb:create-method view-type="remote"
     **/
    public void ejbCreate() throws CreateException {

        Debug.print("AdApprovalControllerBean ejbCreate");
      
    }
    
}

