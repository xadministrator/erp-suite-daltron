package com.ejb.txn;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.naming.NamingException;

import com.ejb.ad.LocalAdApprovalDocument;
import com.ejb.ad.LocalAdApprovalDocumentHome;
import com.ejb.ad.LocalAdApprovalQueue;
import com.ejb.ap.LocalApCanvass;
import com.ejb.ap.LocalApCanvassHome;
import com.ejb.ap.LocalApPurchaseRequisition;
import com.ejb.ap.LocalApPurchaseRequisitionHome;
import com.ejb.ap.LocalApPurchaseRequisitionLine;
import com.ejb.exception.GlobalNoRecordFoundException;
import com.util.AbstractSessionBean;
import com.util.AdApprovalDocumentDetails;
import com.util.AdModApprovalQueueDetails;
import com.util.Debug;
import com.util.EJBCommon;
import com.util.EJBHomeFactory;

/**
 * @ejb:bean name="AdApprovalDocumentEmailNotificationControllerEJB"
 *           display-name="Used for editing approval document"
 *           type="Stateless"
 *           view-type="remote"
 *           jndi-name="ejb/AdApprovalDocumentEmailNotificationControllerEJB"
 *
 * @ejb:interface remote-class="com.ejb.txn.AdApprovalDocumentEmailNotificationController"
 *                extends="javax.ejb.EJBObject"
 *
 * @ejb:home remote-class="com.ejb.txn.AdApprovalDocumentEmailNotificationControllerHome"
 *           extends="javax.ejb.EJBHome"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="aduser"
 *                        role-link="aduserlink"
 *
 * @ejb:permission role-name="aduser"
 * 
*/

public class AdApprovalDocumentEmailNotificationControllerBean extends AbstractSessionBean {

	/**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
	public void sendPurchaseRequisitionEmail(LocalAdApprovalQueue adApprovalQueue, Integer AD_CMPNY) {

		Debug.print("AdApprovalDocumentEmailNotificationControllerBean sendPurchaseRequisitionEmail");


		LocalApPurchaseRequisitionHome apPurchaseRequisitionHome = null;
		LocalApCanvassHome apCanvassHome = null;


		// Initialize EJB Home

		try {


			apPurchaseRequisitionHome = (LocalApPurchaseRequisitionHome)EJBHomeFactory.
			lookUpLocalHome(LocalApPurchaseRequisitionHome.JNDI_NAME, LocalApPurchaseRequisitionHome.class);

			apCanvassHome = (LocalApCanvassHome)EJBHomeFactory.
					lookUpLocalHome(LocalApCanvassHome.JNDI_NAME, LocalApCanvassHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}

		StringBuilder composedEmail = new StringBuilder();
		LocalApPurchaseRequisition apPurchaseRequisition = null;

		try {

			apPurchaseRequisition = apPurchaseRequisitionHome.findByPrimaryKey(adApprovalQueue.getAqDocumentCode());

			HashMap hm = new HashMap();

			Iterator i = apPurchaseRequisition.getApPurchaseRequisitionLines().iterator();

			while(i.hasNext()) {

				LocalApPurchaseRequisitionLine apPurchaseRequisitionLine = (LocalApPurchaseRequisitionLine)i.next();

				Collection apCanvasses = apCanvassHome.findByPrlCodeAndCnvPo(
						apPurchaseRequisitionLine.getPrlCode(), EJBCommon.TRUE, AD_CMPNY);

				Iterator j = apCanvasses.iterator();


				while(j.hasNext()) {

					LocalApCanvass apCanvass = (LocalApCanvass)j.next();


					if (hm.containsKey(apCanvass.getApSupplier().getSplSupplierCode())){
						AdModApprovalQueueDetails adModApprovalQueueExistingDetails = (AdModApprovalQueueDetails)
		        				 hm.get(apCanvass.getApSupplier().getSplSupplierCode());

						adModApprovalQueueExistingDetails.setAqSupplierCode(apCanvass.getApSupplier().getSplSupplierCode());
						adModApprovalQueueExistingDetails.setAqSupplierName(apCanvass.getApSupplier().getSplName());
						adModApprovalQueueExistingDetails.setAqAmount(adModApprovalQueueExistingDetails.getAqAmount() +  apCanvass.getCnvAmount());

					} else {

						AdModApprovalQueueDetails adModApprovalQueueNewDetails = new AdModApprovalQueueDetails();

						adModApprovalQueueNewDetails.setAqSupplierCode(apCanvass.getApSupplier().getSplSupplierCode());
						adModApprovalQueueNewDetails.setAqSupplierName(apCanvass.getApSupplier().getSplName());
						adModApprovalQueueNewDetails.setAqAmount(apCanvass.getCnvAmount());

						hm.put(apCanvass.getApSupplier().getSplSupplierCode(), adModApprovalQueueNewDetails);
					}
				}
			}


			Set set = hm.entrySet();

			composedEmail.append("<table border='1' style='width:100%'>");

			composedEmail.append("<tr>");
			composedEmail.append("<th> VENDOR </th>");
			composedEmail.append("<th> AMOUNT </th>");
			composedEmail.append("</tr>");


			Iterator x = set.iterator();

			while(x.hasNext()) {


				Map.Entry me = (Map.Entry)x.next();

				AdModApprovalQueueDetails adModApprovalQueueDetails = (AdModApprovalQueueDetails)
							me.getValue();

				composedEmail.append("<tr>");
				composedEmail.append("<td>");
				composedEmail.append(adModApprovalQueueDetails.getAqSupplierName());
				composedEmail.append("</td>");
				composedEmail.append("<td>");
				composedEmail.append(adModApprovalQueueDetails.getAqAmount());
				composedEmail.append("</td>");
				composedEmail.append("</tr>");

			}

			composedEmail.append("</table>");




		} catch (Exception ex) {

			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());

		}


		String emailTo = adApprovalQueue.getAdUser().getUsrEmailAddress();
		Properties props = new Properties();
		/*
		 * GMAIL SETTINGS
		 * props.put("mail.smtp.host", "smtp.gmail.com");
		props.put("mail.smtp.socketFactory.port", "465");
		props.put("mail.smtp.socketFactory.class",
				"javax.net.ssl.SSLSocketFactory");
		props.put("mail.smtp.auth", "true");
		props.put("mail.smtp.port", "465");

		Session session = Session.getDefaultInstance(props,
		new javax.mail.Authenticator() {
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication("chrisr@daltron.net.pg","Sh0wc453");
			}
		});*/

		props.put("mail.smtp.host", "180.150.253.101");
		props.put("mail.smtp.socketFactory.port", "25");
		props.put("mail.smtp.auth", "false");
		props.put("mail.smtp.port", "25");

		Session session = Session.getDefaultInstance(props, null);

		try {

			Message message = new MimeMessage(session);
			message.setFrom(new InternetAddress("ofs-notifcation@daltron.net.pg"));
			message.setRecipients(Message.RecipientType.TO,
					InternetAddress.parse(emailTo));


			message.setRecipients(Message.RecipientType.BCC,
					InternetAddress.parse("cromero@wrcpng.com"));

			message.setSubject("DALTRON - OFS - PURCHASE REQUISTION APPROVAL PR #:"+apPurchaseRequisition.getPrNumber());
			
			System.out.println("adApprovalQueue.getAqRequesterName()="+adApprovalQueue.getAqRequesterName());
			System.out.println("adApprovalQueue.getAqDocumentNumber()="+adApprovalQueue.getAqDocumentNumber());
			System.out.println("adApprovalQueue.getAdUser().getUsrDescription()="+adApprovalQueue.getAdUser().getUsrDescription());

			message.setContent(
		              "Dear Mr/Mrs,"+adApprovalQueue.getAdUser().getUsrDescription()+"<br><br>"+
		              "A purchase request was raised by "+adApprovalQueue.getAqRequesterName()+" for your approval.<br>"+
		              "PR Number: "+adApprovalQueue.getAqDocumentNumber()+".<br>"+
		              "Delivery Period: "+EJBCommon.convertSQLDateToString(apPurchaseRequisition.getPrDeliveryPeriod())+".<br>"+
		              "Description: "+apPurchaseRequisition.getPrDescription()+".<br>"+
		              composedEmail.toString() +

		              "Please click the link <a href=\"http://180.150.253.99:8080/daltron\">http://180.150.253.99:8080/daltron</a>.<br><br><br>"+

		              "This is an automated message and was sent from a notification-only email address.<br><br>"+
		              "Please do not reply to this message.<br><br>",
		             "text/html");


			Transport.send(message);
			adApprovalQueue.setAqEmailSent(EJBCommon.TRUE);

			System.out.println("Done");

		} catch (MessagingException e) {
			throw new RuntimeException(e);
		}

		
	}
    // SessionBean methods

    /**
     * @ejb:create-method view-type="remote"
     **/
    public void ejbCreate() throws CreateException {

        Debug.print("AdApprovalDocumentEmailNotificationControllerBean ejbCreate");
      
    }
    
}

