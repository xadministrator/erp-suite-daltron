package com.ejb.txn;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.naming.NamingException;

import com.ejb.ad.LocalAdApproval;
import com.ejb.ad.LocalAdApprovalHome;
import com.util.AbstractSessionBean;
import com.util.AdApprovalDetails;
import com.util.Debug;
import com.util.EJBHomeFactory;

/**
 * @ejb:bean name="AdApprovalSetupControllerEJB"
 *           display-name="Used for editing approval setup"
 *           type="Stateless"
 *           view-type="remote"
 *           jndi-name="ejb/AdApprovalSetupControllerEJB"
 *
 * @ejb:interface remote-class="com.ejb.txn.AdApprovalSetupController"
 *                extends="javax.ejb.EJBObject"
 *
 * @ejb:home remote-class="com.ejb.txn.AdApprovalSetupControllerHome"
 *           extends="javax.ejb.EJBHome"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="aduser"
 *                        role-link="aduserlink"
 *
 * @ejb:permission role-name="aduser"
 * 
*/

public class AdApprovalSetupControllerBean extends AbstractSessionBean {

    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public com.util.AdApprovalDetails getAdApr(Integer AD_CMPNY) {
                    
        Debug.print("AdApprovalControllerBean getAdApr");

        LocalAdApprovalHome adApprovalHome = null;
        LocalAdApproval adApproval = null;
        
        // Initialize EJB Home

        try {

            adApprovalHome = (LocalAdApprovalHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdApprovalHome.JNDI_NAME, LocalAdApprovalHome.class);

        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {
        	
	        adApproval = adApprovalHome.findByAprAdCompany(AD_CMPNY);
	
			AdApprovalDetails details = new AdApprovalDetails();
			    details.setAprEnableGlJournal(adApproval.getAprEnableGlJournal());
			    details.setAprEnableApVoucher(adApproval.getAprEnableApVoucher());
			    details.setAprEnableApVoucherDepartment(adApproval.getAprEnableApVoucherDepartment());
			    details.setAprEnableApDebitMemo(adApproval.getAprEnableApDebitMemo());
			    details.setAprEnableApCheck(adApproval.getAprEnableApCheck());
			    details.setAprEnableArInvoice(adApproval.getAprEnableArInvoice());
			    details.setAprEnableArCreditMemo(adApproval.getAprEnableArCreditMemo());
			    details.setAprEnableArReceipt(adApproval.getAprEnableArReceipt());
			    details.setAprEnableCmFundTransfer(adApproval.getAprEnableCmFundTransfer());
			    details.setAprEnableCmAdjustment(adApproval.getAprEnableCmAdjustment());
			    details.setAprApprovalQueueExpiration(adApproval.getAprApprovalQueueExpiration());
	            details.setAprEnableInvAdjustment(adApproval.getAprEnableInvAdjustment());
	            details.setAprEnableInvBuild(adApproval.getAprEnableInvBuild());
	            
	            details.setAprEnableApPurReq(adApproval.getAprEnableApPurReq());
	            details.setAprEnableApCanvass(adApproval.getAprEnableApCanvass());

	            details.setAprEnableInvAdjustmentRequest(adApproval.getAprEnableInvAdjustmentRequest());

	            details.setAprEnableInvBranchStockTransferOrder(adApproval.getAprEnableInvBranchStockTransferOrder());
	            details.setAprEnableApCheckPaymentRequest(adApproval.getAprEnableApCheckPaymentRequest());
	            details.setAprEnableApCheckPaymentRequestDepartment(adApproval.getAprEnableApCheckPaymentRequestDepartment());
	            details.setAprEnableInvStockTransfer(adApproval.getAprEnableInvStockTransfer());	
	            details.setAprEnableApPurchaseOrder(adApproval.getAprEnableApPurchaseOrder());
	            details.setAprEnableApReceivingItem(adApproval.getAprEnableApReceivingItem());
	            details.setAprEnableInvBranchStockTransfer(adApproval.getAprEnableInvBranchStockTransfer());
	            details.setAprEnableArSalesOrder(adApproval.getAprEnableArSalesOrder());
	            details.setAprEnableArCustomer(adApproval.getAprEnableArCustomer());
	            
		        
	        return details;
	        
        } catch (Exception ex) {
        	
        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());
        	
        }
            
    }    
    
    /**
     * @ejb:interface-method view-type="remote"
     **/
    public Integer saveAdAprEntry(com.util.AdApprovalDetails details, Integer AD_CMPNY) {
                    
        Debug.print("AdApprovalControllerBean saveAdAprEntry");
        
        LocalAdApprovalHome adApprovalHome = null;                
                
        // Initialize EJB Home
        
        try {

            adApprovalHome = (LocalAdApprovalHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdApprovalHome.JNDI_NAME, LocalAdApprovalHome.class);           

        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {
        	
        	LocalAdApproval adApproval = null;
        	
			// update approval setup
			
			adApproval = adApprovalHome.findByAprAdCompany(AD_CMPNY);
				
			    adApproval.setAprEnableGlJournal(details.getAprEnableGlJournal());
			    adApproval.setAprEnableApVoucher(details.getAprEnableApVoucher());
			    adApproval.setAprEnableApVoucherDepartment(details.getAprEnableApVoucherDepartment());
			    adApproval.setAprEnableApDebitMemo(details.getAprEnableApDebitMemo());
			    adApproval.setAprEnableApCheck(details.getAprEnableApCheck());
			    adApproval.setAprEnableArInvoice(details.getAprEnableArInvoice());
			    adApproval.setAprEnableArCreditMemo(details.getAprEnableArCreditMemo());
			    adApproval.setAprEnableArReceipt(details.getAprEnableArReceipt());
			    adApproval.setAprEnableCmFundTransfer(details.getAprEnableCmFundTransfer());
			    adApproval.setAprEnableCmAdjustment(details.getAprEnableCmAdjustment());
			    adApproval.setAprApprovalQueueExpiration(details.getAprApprovalQueueExpiration());
			    adApproval.setAprEnableInvAdjustment(details.getAprEnableInvAdjustment());
			    adApproval.setAprEnableInvBuild(details.getAprEnableInvBuild());
			    
			    adApproval.setAprEnableApPurReq(details.getAprEnableApPurReq());
			    adApproval.setAprEnableApCanvass(details.getAprEnableApCanvass());
			    adApproval.setAprEnableInvAdjustmentRequest(details.getAprEnableInvAdjustmentRequest());	    
			    adApproval.setAprEnableApCheckPaymentRequest(details.getAprEnableApCheckPaymentRequest());
			    adApproval.setAprEnableApCheckPaymentRequestDepartment(details.getAprEnableApCheckPaymentRequestDepartment());
			    adApproval.setAprEnableInvBranchStockTransferOrder(details.getAprEnableInvBranchStockTransferOrder());
			    adApproval.setAprEnableInvStockTransfer(details.getAprEnableInvStockTransfer());
			    adApproval.setAprEnableApPurchaseOrder(details.getAprEnableApPurchaseOrder());	
			    adApproval.setAprEnableApReceivingItem(details.getAprEnableApReceivingItem());
			    adApproval.setAprEnableInvBranchStockTransfer(details.getAprEnableInvBranchStockTransfer());
			    adApproval.setAprEnableArSalesOrder(details.getAprEnableArSalesOrder());
			    adApproval.setAprEnableArCustomer(details.getAprEnableArCustomer());
			    
			return adApproval.getAprCode();
			    
        } catch (Exception ex) {
        	
        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());
        	
        }
            
    }    
    
    // SessionBean methods

    /**
     * @ejb:create-method view-type="remote"
     **/
    public void ejbCreate() throws CreateException {

        Debug.print("AdApprovalControllerBean ejbCreate");
      
    }
    
}

