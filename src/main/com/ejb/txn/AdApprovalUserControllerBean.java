
/*
 * AdApprovalUserControllerBean.java
 *
 * Created on March 24, 2004, 8:04 PM
 *
 * @author  Enrico C. Yap
 */

package com.ejb.txn;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;
import javax.ejb.RemoveException;
import javax.naming.NamingException;

import com.ejb.ad.LocalAdAmountLimit;
import com.ejb.ad.LocalAdAmountLimitHome;
import com.ejb.ad.LocalAdApprovalUser;
import com.ejb.ad.LocalAdApprovalUserHome;
import com.ejb.ad.LocalAdCompany;
import com.ejb.ad.LocalAdCompanyHome;
import com.ejb.ad.LocalAdUser;
import com.ejb.ad.LocalAdUserHome;
import com.ejb.exception.AdAURequesterMustBeEnteredOnceException;
import com.ejb.exception.AdAUUserCannotBeARequesterException;
import com.ejb.exception.AdAUUserCannotBeAnApproverException;
import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.exception.GlobalRecordAlreadyDeletedException;
import com.ejb.exception.GlobalRecordAlreadyExistException;
import com.util.AbstractSessionBean;
import com.util.AdAmountLimitDetails;
import com.util.AdModApprovalUserDetails;
import com.util.Debug;
import com.util.EJBHomeFactory;

/**
 * @ejb:bean name="AdApprovalUserControllerEJB"
 *           display-name="Used for entering amount limits"
 *           type="Stateless"
 *           view-type="remote"
 *           jndi-name="ejb/AdApprovalUserControllerEJB"
 *
 * @ejb:interface remote-class="com.ejb.txn.AdApprovalUserController"
 *                extends="javax.ejb.EJBObject"
 *
 * @ejb:home remote-class="com.ejb.txn.AdApprovalUserControllerHome"
 *           extends="javax.ejb.EJBHome"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="aduser"
 *                        role-link="aduserlink"
 *
 * @ejb:permission role-name="aduser"
 * 
*/

public class AdApprovalUserControllerBean extends AbstractSessionBean {
    
    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList getAdUsrAll(Integer AD_CMPNY) {
                    
        Debug.print("AdApprovalUserControllerBean getAdUsrAll");
        
        LocalAdUserHome adUserHome = null;
                
        LocalAdUser adUser = null;
        
        Collection adUsers = null;
        
        ArrayList list = new ArrayList();
        
        // Initialize EJB Home
        
        try {
            
            adUserHome = (LocalAdUserHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdUserHome.JNDI_NAME, LocalAdUserHome.class);
            
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {
            
            adUsers = adUserHome.findUsrAll(AD_CMPNY);
            
        } catch (FinderException ex) {
            
        } catch (Exception ex) {
            
            throw new EJBException(ex.getMessage());
        }
        
        if (adUsers.isEmpty()) {
        	
            return null;
        	
        }
        
        Iterator i = adUsers.iterator();
               
        while (i.hasNext()) {
        	
            adUser = (LocalAdUser)i.next();
        	
            list.add(adUser.getUsrName());
        	
        }
        
        return list;
            
    }      
    
    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList getAdAuByCalCode(Integer CAL_CODE, Integer AD_CMPNY) 
        throws GlobalNoRecordFoundException {
                    
        Debug.print("AdApprovalUserControllerBean getAdAuByCalCode");

        LocalAdApprovalUserHome adApprovalUserHome = null;

        ArrayList list = new ArrayList();
        
        // Initialize EJB Home

        try {
            
            adApprovalUserHome = (LocalAdApprovalUserHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdApprovalUserHome.JNDI_NAME, LocalAdApprovalUserHome.class);

        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }

	    try {
	            
	        Collection adApprovalUsers = adApprovalUserHome.findByCalCode(CAL_CODE, AD_CMPNY);

	        if (adApprovalUsers.isEmpty()) {
	        	
	            throw new GlobalNoRecordFoundException();
	        	
	        }
	        
	        Iterator i = adApprovalUsers.iterator();
	               
	        while (i.hasNext()) {
	        	
	        	LocalAdApprovalUser adApprovalUser = (LocalAdApprovalUser)i.next();
	                                                                        
 	        	AdModApprovalUserDetails mdetails = new AdModApprovalUserDetails();
	        		mdetails.setAuCode(adApprovalUser.getAuCode());
	        		mdetails.setAuLevel(adApprovalUser.getAuLevel());
                    mdetails.setAuType(adApprovalUser.getAuType());
                    mdetails.setAuUsrName(adApprovalUser.getAdUser().getUsrName());
                    mdetails.setAuOr(adApprovalUser.getAuOr());
	                                                        	
	        	list.add(mdetails);
	        }
	        
	        return list;
            
        } catch (GlobalNoRecordFoundException ex) {
	    	
	    	throw ex;    
	        
        } catch (Exception ex) {
        	
        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());
        	
        }
            
    } 
    
    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public com.util.AdAmountLimitDetails getAdCalByCalCode(Integer CAL_CODE, Integer AD_CMPNY) {
                    
        Debug.print("AdApprovalUserControllerBean getAdCalByCalCode");
        		    	         	   	        	    
        LocalAdAmountLimitHome adAmountLimitHome = null;
                                       
        // Initialize EJB Home
        
        try {
            
            adAmountLimitHome = (LocalAdAmountLimitHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdAmountLimitHome.JNDI_NAME, LocalAdAmountLimitHome.class);              
            
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {
        	
        	LocalAdAmountLimit adAmountLimit = adAmountLimitHome.findByPrimaryKey(CAL_CODE);
   
        	AdAmountLimitDetails details = new AdAmountLimitDetails();
        		details.setCalCode(adAmountLimit.getCalCode());        		
                details.setCalAmountLimit(adAmountLimit.getCalAmountLimit());
                details.setCalAndOr(adAmountLimit.getCalAndOr());
                        
        	return details;
        	       	
        } catch (Exception ex) {
        	
        	throw new EJBException(ex.getMessage());
        	
        }
        
	}      
	
    /**
     * @ejb:interface-method view-type="remote"
     **/    
    public void addAdAuEntry(com.util.AdApprovalUserDetails details, Integer CAL_CODE, String USR_NM, Integer AD_CMPNY) 
        throws GlobalRecordAlreadyExistException,
               AdAURequesterMustBeEnteredOnceException,
               AdAUUserCannotBeAnApproverException {
     
        Debug.print("AdApprovalUserControllerBean addAdAuEntry");
        
        LocalAdApprovalUserHome adApprovalUserHome = null;
        LocalAdAmountLimitHome adAmountLimitHome = null;
        LocalAdUserHome adUserHome = null;
        
        // Initialize EJB Home
        
        try {
            
            adAmountLimitHome = (LocalAdAmountLimitHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdAmountLimitHome.JNDI_NAME, LocalAdAmountLimitHome.class);
            adApprovalUserHome = (LocalAdApprovalUserHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdApprovalUserHome.JNDI_NAME, LocalAdApprovalUserHome.class);
            adUserHome = (LocalAdUserHome)EJBHomeFactory.
            	lookUpLocalHome(LocalAdUserHome.JNDI_NAME, LocalAdUserHome.class);
                                                      
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
                                   
        try {
	               
            LocalAdApprovalUser adApprovalUser = null;
            LocalAdAmountLimit adAmountLimit = adAmountLimitHome.findByPrimaryKey(CAL_CODE);
        	
        	try {

	            adApprovalUser = adApprovalUserHome.
	              findByUsrNameAndCalCode(USR_NM, CAL_CODE, AD_CMPNY);
                                             
                  throw new GlobalRecordAlreadyExistException();
                  
            } catch (FinderException ex) {
            	
            }
            
            if (details.getAuType().equals("REQUESTER")) {            
            
	            try {
	            		            	
	            	if (adAmountLimit.getAdApprovalDocument() != null) {

	            		System.out.println("details.getAuType()="+details.getAuType());
	            		System.out.println("adAmountLimit.getAdApprovalDocument().getAdcCode()="+adAmountLimit.getAdApprovalDocument().getAdcCode());
	            		System.out.println("USR_NM="+USR_NM);
	            		System.out.println("adAmountLimit.getCalDept()="+adAmountLimit.getCalDept());
	            		System.out.println("adAmountLimit.getCalAmountLimit()="+adAmountLimit.getCalAmountLimit());
	            		
	            		
		        		adApprovalUser = adApprovalUserHome.findByAdcCodeAndApprovalTypeAndUsrNameAndAclDept(details.getAuType(),
		        			adAmountLimit.getAdApprovalDocument().getAdcCode(), USR_NM, adAmountLimit.getCalDept(), adAmountLimit.getCalAmountLimit(), AD_CMPNY);
		        		System.out.println("adApprovalUser="+adApprovalUser.getAuCode());
		        			throw new AdAURequesterMustBeEnteredOnceException();
		        			
		            } else {
		            	
		            	adApprovalUser = adApprovalUserHome.findByAclCodeAndApprovalTypeAndUsrName(details.getAuType(),
		            		adAmountLimit.getAdApprovalCoaLine().getAclCode(), USR_NM, AD_CMPNY);
		            		
		            		throw new AdAURequesterMustBeEnteredOnceException();
		            	
		            }
	            	
	            } catch (FinderException ex) {
	            	
	            }
            
            } else {
            	
            	try {
            		
            		if (adAmountLimit.getAdApprovalDocument() != null) {

	            		adApprovalUser = adApprovalUserHome.findByAdcCodeAndUsrNameAndRequesterTypeLessThanAmountLimit(
	            			USR_NM, "REQUESTER", adAmountLimit.getCalAmountLimit(),
	            			adAmountLimit.getAdApprovalDocument().getAdcCode(), AD_CMPNY);
	            			
	            			throw new AdAUUserCannotBeAnApproverException();
	            			
	                } else {
	                	
	                	adApprovalUser = adApprovalUserHome.findByAclCodeAndUsrNameAndRequesterTypeLessThanAmountLimit(
	                		USR_NM, "REQUESTER", adAmountLimit.getCalAmountLimit(),
	                		adAmountLimit.getAdApprovalCoaLine().getAclCode(), AD_CMPNY);
	                		
	                		throw new AdAUUserCannotBeAnApproverException();
	                		
	                }
	                	                         		            		            		
            	} catch (FinderException ex) {
            		
            	}
            	
            }
            
        	// create new amount limit
        	
        	adApprovalUser = adApprovalUserHome.create(details.getAuLevel(), details.getAuType(), details.getAuOr(), AD_CMPNY); 
        	        
		    adAmountLimit.addAdApprovalUser(adApprovalUser);
		      	
		    LocalAdUser adUser = adUserHome.findByUsrName(USR_NM, AD_CMPNY);
		        adUser.addAdApprovalUser(adApprovalUser);
		        		      	
		} catch (GlobalRecordAlreadyExistException ex) {
        	
        	getSessionContext().setRollbackOnly();
        	throw ex;      	           	        
        	
		} catch (AdAURequesterMustBeEnteredOnceException ex) {
        	
        	getSessionContext().setRollbackOnly();
        	throw ex;      	           	                	
        	
		} catch (AdAUUserCannotBeAnApproverException ex) {
        	
        	getSessionContext().setRollbackOnly();
        	throw ex;      	           	                	        	
        	
        } catch (Exception ex) {
        	
        	Debug.printStackTrace(ex);
        	getSessionContext().setRollbackOnly();
        	throw new EJBException(ex.getMessage());
        	
        }
            
    } 	
    
    /**
     * @ejb:interface-method view-type="remote"
     **/    
    public void updateAdAuEntry(com.util.AdApprovalUserDetails details, Integer CAL_CODE, String USR_NM, Integer AD_CMPNY) 
        throws GlobalRecordAlreadyExistException,
               AdAURequesterMustBeEnteredOnceException,
               AdAUUserCannotBeAnApproverException,
               AdAUUserCannotBeARequesterException {

        Debug.print("AdApprovalUserControllerBean updateAdAuEntry");
        
        LocalAdApprovalUserHome adApprovalUserHome = null;
        LocalAdAmountLimitHome adAmountLimitHome = null;
        LocalAdUserHome adUserHome = null;
        
        // Initialize EJB Home
        
        try {
            
            adAmountLimitHome = (LocalAdAmountLimitHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdAmountLimitHome.JNDI_NAME, LocalAdAmountLimitHome.class);
            adApprovalUserHome = (LocalAdApprovalUserHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdApprovalUserHome.JNDI_NAME, LocalAdApprovalUserHome.class);
            adUserHome = (LocalAdUserHome)EJBHomeFactory.
            	lookUpLocalHome(LocalAdUserHome.JNDI_NAME, LocalAdUserHome.class);
                                                      
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }                                  

        try { 	
        	               
            LocalAdApprovalUser adApprovalUser = null;
            LocalAdAmountLimit adAmountLimit = adAmountLimitHome.findByPrimaryKey(CAL_CODE);
        	
        	try {

	            adApprovalUser = adApprovalUserHome.
	              findByUsrNameAndCalCode(USR_NM, CAL_CODE, AD_CMPNY);
	              
	            if(adApprovalUser != null &&
	                !adApprovalUser.getAuCode().equals(details.getAuCode()))  {  
	        
	               throw new GlobalRecordAlreadyExistException();
	             
	            } 
                                             
            } catch (FinderException ex) {
            	
            }
            
            if (details.getAuType().equals("REQUESTER")) {            
            
	            try {
	            	
	            	if (adAmountLimit.getAdApprovalDocument() != null) {
           	            		            		            	
		        		adApprovalUser = adApprovalUserHome.findByAdcCodeAndApprovalTypeAndUsrNameAndAclDept(details.getAuType(),
		        			adAmountLimit.getAdApprovalDocument().getAdcCode(), USR_NM, adAmountLimit.getCalDept(), adAmountLimit.getCalAmountLimit(), AD_CMPNY);
		        	
		        			throw new AdAURequesterMustBeEnteredOnceException();
		        			
		            } else {
		            	
		        		adApprovalUser = adApprovalUserHome.findByAclCodeAndApprovalTypeAndUsrName(details.getAuType(),
		        			adAmountLimit.getAdApprovalCoaLine().getAclCode(), USR_NM, AD_CMPNY);
		        	
		        			throw new AdAURequesterMustBeEnteredOnceException();		            	
		            	
		            }
	            	
	            } catch (FinderException ex) {
	            	
	            }
	            
	            try {
	            	
	            	if (adAmountLimit.getAdApprovalDocument() != null) {

		            	adApprovalUser = adApprovalUserHome.findByAdcCodeAndUsrNameAndApproverTypeLessThanAmountLimit(
	            			USR_NM, "APPROVER", adAmountLimit.getCalAmountLimit(),
	            			adAmountLimit.getAdApprovalDocument().getAdcCode(), AD_CMPNY);
	            			
	            			throw new AdAUUserCannotBeARequesterException();
	            			
	                } else {
	                	
		            	adApprovalUser = adApprovalUserHome.findByAclCodeAndUsrNameAndApproverTypeLessThanAmountLimit(
	            			USR_NM, "APPROVER", adAmountLimit.getCalAmountLimit(),
	            			adAmountLimit.getAdApprovalCoaLine().getAclCode(), AD_CMPNY);
	            			
	            			throw new AdAUUserCannotBeARequesterException();	                	
	                	
	                }
            		            		            		
            	} catch (FinderException ex) {
            			            	
	            }
            
            } else {
            	
            	try {
            		
            		if (adAmountLimit.getAdApprovalDocument() != null) {
            		
	            		adApprovalUser = adApprovalUserHome.findByAdcCodeAndUsrNameAndRequesterTypeLessThanAmountLimit(
	            			USR_NM, "REQUESTER", adAmountLimit.getCalAmountLimit(),
	            			adAmountLimit.getAdApprovalDocument().getAdcCode(), AD_CMPNY);
	            			
	            			throw new AdAUUserCannotBeAnApproverException();
	            			
	            	} else {
	            		
	            		adApprovalUser = adApprovalUserHome.findByAclCodeAndUsrNameAndRequesterTypeLessThanAmountLimit(
	            			USR_NM, "REQUESTER", adAmountLimit.getCalAmountLimit(),
	            			adAmountLimit.getAdApprovalCoaLine().getAclCode(), AD_CMPNY);
	            			
	            			throw new AdAUUserCannotBeAnApproverException();	            		
	            		
	            	}
            		            		            		
            	} catch (FinderException ex) {
            		
            	}
            	
            }

           // Find and Update Amount Limit 
        	
        	adApprovalUser = adApprovalUserHome.findByPrimaryKey(details.getAuCode());
        		
        	System.out.println("-------------------->details.getAuLevel()="+details.getAuLevel());
        	
        	adApprovalUser.setAuLevel(details.getAuLevel());
            adApprovalUser.setAuType(details.getAuType());
            adApprovalUser.setAuOr(details.getAuOr());

		    adAmountLimit.addAdApprovalUser(adApprovalUser); 
		      	
		    LocalAdUser adUser = adUserHome.findByUsrName(USR_NM, AD_CMPNY);
		        adUser.addAdApprovalUser(adApprovalUser);
      	        	                            	
        } catch (GlobalRecordAlreadyExistException ex) {
        	
        	getSessionContext().setRollbackOnly();
        	throw ex;      	           	        
        	
		} catch (AdAURequesterMustBeEnteredOnceException ex) {
        	
        	getSessionContext().setRollbackOnly();
        	throw ex;      	   
        	
		} catch (AdAUUserCannotBeAnApproverException ex) {
        	
        	getSessionContext().setRollbackOnly();
        	throw ex;      	           	                  
        	
		} catch (AdAUUserCannotBeARequesterException ex) {
        	
        	getSessionContext().setRollbackOnly();
        	throw ex;      	           	                          	      	        	                	    	
        	        
        } catch (Exception ex) {
        	
        	Debug.printStackTrace(ex);
        	getSessionContext().setRollbackOnly();
        	throw new EJBException(ex.getMessage());
        	
        }
            
    }	 
    
   /**
    * @ejb:interface-method view-type="remote"
    **/
    public void deleteAdAuEntry(Integer AU_CODE, Integer AD_CMPNY) 
        throws GlobalRecordAlreadyDeletedException {               

      Debug.print("AdApprovalUserControllerBean deleteAdAuEntry");

      LocalAdApprovalUserHome adApprovalUserHome = null;

      // Initialize EJB Home
        
      try {

          adApprovalUserHome = (LocalAdApprovalUserHome)EJBHomeFactory.
              lookUpLocalHome(LocalAdApprovalUserHome.JNDI_NAME, LocalAdApprovalUserHome.class);           
      
      } catch (NamingException ex) {

          throw new EJBException(ex.getMessage());

      }                
      
      try {
	         
	       LocalAdApprovalUser adApprovalUser = adApprovalUserHome.findByPrimaryKey(AU_CODE);

	       adApprovalUser.remove();
	      
	  } catch (FinderException ex) {
	      	
	      throw new GlobalRecordAlreadyDeletedException();

	  } catch (RemoveException ex) {
	 	
	      getSessionContext().setRollbackOnly();	    
	      throw new EJBException(ex.getMessage());
	    
	  } catch (Exception ex) {
	 	
	      getSessionContext().setRollbackOnly();	    
	      throw new EJBException(ex.getMessage());
	    
	  }	      
      
   }
   
   /**
    * @ejb:interface-method view-type="remote"
    * @jboss:method-attributes read-only="true"
    **/
    public short getGlFcPrecisionUnit(Integer AD_CMPNY) {

       Debug.print("AdApprovalUserControllerBean getGlFcPrecisionUnit");
      
       LocalAdCompanyHome adCompanyHome = null;
           
       // Initialize EJB Home
        
       try {
            
           adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
              lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);       
            
       } catch (NamingException ex) {
            
           throw new EJBException(ex.getMessage());
            
       }

       try {
       	
         LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
            
         return  adCompany.getGlFunctionalCurrency().getFcPrecision();
                                     
       } catch (Exception ex) {
       	 
       	 Debug.printStackTrace(ex);
         throw new EJBException(ex.getMessage());
         
       }

    }    
   
    // SessionBean methods

    /**
     * @ejb:create-method view-type="remote"
     **/
    public void ejbCreate() throws CreateException {

       Debug.print("AdApprovalUserControllerBean ejbCreate");
      
    }
}
