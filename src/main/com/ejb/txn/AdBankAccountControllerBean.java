
/*
 * AdBankAccountControllerBean.java
 *
 * Created on May 15, 2003, 2:26 PM
 *
 * @author  Neil Andrew M. Ajero
 */

package com.ejb.txn;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;
import javax.naming.NamingException;

import com.ejb.ad.LocalAdBank;
import com.ejb.ad.LocalAdBankAccount;
import com.ejb.ad.LocalAdBankAccountBalance;
import com.ejb.ad.LocalAdBankAccountBalanceHome;
import com.ejb.ad.LocalAdBankAccountHome;
import com.ejb.ad.LocalAdBankHome;
import com.ejb.ad.LocalAdBranch;
import com.ejb.ad.LocalAdBranchBankAccount;
import com.ejb.ad.LocalAdBranchBankAccountHome;
import com.ejb.ad.LocalAdBranchCustomer;
import com.ejb.ad.LocalAdBranchDocumentSequenceAssignment;
import com.ejb.ad.LocalAdBranchDocumentSequenceAssignmentHome;
import com.ejb.ad.LocalAdBranchHome;
import com.ejb.ad.LocalAdBranchResponsibility;
import com.ejb.ad.LocalAdBranchResponsibilityHome;
import com.ejb.ad.LocalAdCompany;
import com.ejb.ad.LocalAdCompanyHome;
import com.ejb.ad.LocalAdResponsibility;
import com.ejb.ad.LocalAdResponsibilityHome;
import com.ejb.ar.LocalArStandardMemoLine;
import com.ejb.cm.LocalCmFundTransferHome;
import com.ejb.exception.AdBACoaGlAccountReceiptNotFoundException;
import com.ejb.exception.AdBACoaGlAdjustmentAccountNotFoundException;
import com.ejb.exception.AdBACoaGlAdvanceAccountNotFoundException;
import com.ejb.exception.AdBACoaGlBankChargeAccountNotFoundException;
import com.ejb.exception.AdBACoaGlCashAccountNotFoundException;
import com.ejb.exception.AdBACoaGlCashDiscountNotFoundException;
import com.ejb.exception.AdBACoaGlClearingAccountNotFoundException;
import com.ejb.exception.AdBACoaGlInterestAccountNotFoundException;
import com.ejb.exception.AdBACoaGlSalesDiscountNotFoundException;
import com.ejb.exception.AdBACoaGlUnappliedCheckNotFoundException;
import com.ejb.exception.AdBACoaGlUnappliedReceiptNotFoundException;
import com.ejb.exception.ArCCCoaGlEarnedInterestAccountNotFoundException;
import com.ejb.exception.ArCCCoaGlReceivableAccountNotFoundException;
import com.ejb.exception.ArCCCoaGlRevenueAccountNotFoundException;
import com.ejb.exception.ArCCCoaGlUnEarnedInterestAccountNotFoundException;
import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.exception.GlobalRecordAlreadyAssignedException;
import com.ejb.exception.GlobalRecordAlreadyDeletedException;
import com.ejb.exception.GlobalRecordAlreadyExistException;
import com.ejb.exception.InvILCoaGlWipAccountNotFoundException;
import com.ejb.gl.LocalGlChartOfAccount;
import com.ejb.gl.LocalGlChartOfAccountHome;
import com.ejb.gl.LocalGlFunctionalCurrency;
import com.ejb.gl.LocalGlFunctionalCurrencyHome;
import com.util.AbstractSessionBean;
import com.util.AdBankAccountDetails;
import com.util.AdBranchDetails;
import com.util.AdModBankAccountDetails;
import com.util.AdModBranchBankAccountDetails;
import com.util.AdModBranchCustomerDetails;
import com.util.AdModBranchDocumentSequenceAssignmentDetails;
import com.util.AdResponsibilityDetails;
import com.util.ArCustomerDetails;
import com.util.Debug;
import com.util.EJBCommon;
import com.util.EJBHomeFactory;
import com.util.GlModFunctionalCurrencyDetails;

/**
 * @ejb:bean name="AdBankAccountControllerEJB"
 *           display-name="Used for entering banks"
 *           type="Stateless"
 *           view-type="remote"
 *           jndi-name="ejb/AdBankAccountControllerEJB"
 *
 * @ejb:interface remote-class="com.ejb.txn.AdBankAccountController"
 *                extends="javax.ejb.EJBObject"
 *
 * @ejb:home remote-class="com.ejb.txn.AdBankAccountControllerHome"
 *           extends="javax.ejb.EJBHome"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="aruser"
 *                        role-link="aruserlink"
 *
 * @ejb:permission role-name="aruser"
 * 
*/

public class AdBankAccountControllerBean extends AbstractSessionBean {
    
    
    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList getAdBaAll(Integer AD_CMPNY) 
        throws GlobalNoRecordFoundException {
                    
        Debug.print("AdBankAccountControllerBean getAdBnkAll");
        
        LocalAdBankAccountHome adBankAccountHome = null;
        LocalGlChartOfAccountHome glChartOfAccountHome = null;
        LocalAdBankAccountBalanceHome adBankAccountBalanceHome = null; 
        
        Collection adBankAccounts = null;
        
        LocalAdBankAccount adBankAccount = null;
        
        ArrayList list = new ArrayList();
        
        // Initialize EJB Home
        
        try {
            
            adBankAccountHome = (LocalAdBankAccountHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdBankAccountHome.JNDI_NAME, LocalAdBankAccountHome.class);
            glChartOfAccountHome = (LocalGlChartOfAccountHome)EJBHomeFactory.
                lookUpLocalHome(LocalGlChartOfAccountHome.JNDI_NAME, LocalGlChartOfAccountHome.class);
            adBankAccountBalanceHome = (LocalAdBankAccountBalanceHome)EJBHomeFactory.
            	lookUpLocalHome(LocalAdBankAccountBalanceHome.JNDI_NAME, LocalAdBankAccountBalanceHome.class);
            
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {
        	
        	try {
        		
        		adBankAccounts = adBankAccountHome.findBaAll(AD_CMPNY);
        		
        	} catch (FinderException ex) {
        		
        	}
        	
        	if (adBankAccounts.isEmpty()) {
        		
        		throw new GlobalNoRecordFoundException();
        		
        	}
        	
        	LocalGlChartOfAccount glCashChartOfAccount = null;
        	LocalGlChartOfAccount glOnAccountReceiptChartOfAccount = null;
        	LocalGlChartOfAccount glUnappliedReceiptChartOfAccount = null;
        	LocalGlChartOfAccount glUnappliedCheckChartOfAccount = null;
        	LocalGlChartOfAccount glBankChargeChartOfAccount = null;       
        	LocalGlChartOfAccount glClearingChartOfAccount = null;
        	LocalGlChartOfAccount glInterestChartOfAccount = null;
        	LocalGlChartOfAccount glAdjustmentChartOfAccount = null;
        	LocalGlChartOfAccount glCashDiscountChartOfAccount = null;		
        	LocalGlChartOfAccount glSalesDiscountChartOfAccount = null;
        	LocalGlChartOfAccount glAdvanceChartOfAccount = null;	
        	
        	Iterator i = adBankAccounts.iterator();
        	
        	while (i.hasNext()) {
        		
        		adBankAccount = (LocalAdBankAccount)i.next();       
        		
        		AdModBankAccountDetails mdetails = new AdModBankAccountDetails();
        		mdetails.setBaCode(adBankAccount.getBaCode());
        		mdetails.setBaName(adBankAccount.getBaName());
        		mdetails.setBaDescription(adBankAccount.getBaDescription());
        		mdetails.setBaAccountType(adBankAccount.getBaAccountType());
        		mdetails.setBaAccountNumber(adBankAccount.getBaAccountNumber());
        		mdetails.setBaAccountUse(adBankAccount.getBaAccountUse());								
        		
        		// Get Cash Account Number with Description
        		
        		if (adBankAccount.getBaCoaGlCashAccount() != null) {
        			
        			try {
        				
        				glCashChartOfAccount = 
        					glChartOfAccountHome.findByPrimaryKey(adBankAccount.getBaCoaGlCashAccount());
        				
        				mdetails.setBaCoaCashAccount(glCashChartOfAccount.getCoaAccountNumber());
        				mdetails.setBaCashAccountDescription(glCashChartOfAccount.getCoaAccountDescription());
        				
        			} catch (FinderException ex) {
        				
        			}
        			
        		}
        		
        		// Get On-Account Receipt Account Number with Description
        		
        		if (adBankAccount.getBaCoaGlOnAccountReceipt() != null) {
        			
        			try {
        				
        				glOnAccountReceiptChartOfAccount = 
        					glChartOfAccountHome.findByPrimaryKey(adBankAccount.getBaCoaGlOnAccountReceipt());
        				
        				mdetails.setBaCoaOnAccountReceipt(glOnAccountReceiptChartOfAccount.getCoaAccountNumber());
        				mdetails.setBaOnAccountReceiptDescription(glOnAccountReceiptChartOfAccount.getCoaAccountDescription());
        				
        			} catch (FinderException ex) {
        				
        			}
        			
        		}
        		
        		if (adBankAccount.getBaCoaGlUnappliedReceipt() != null) {
        			
        			// Get Unapplied Receipt Account Number with Description
        			
        			try {
        				
        				glUnappliedReceiptChartOfAccount = 
        					glChartOfAccountHome.findByPrimaryKey(adBankAccount.getBaCoaGlUnappliedReceipt());
        				
        				mdetails.setBaCoaUnappliedReceipt(glUnappliedReceiptChartOfAccount.getCoaAccountNumber());
        				mdetails.setBaUnappliedReceiptDescription(glUnappliedReceiptChartOfAccount.getCoaAccountDescription());			    
        				
        			} catch (FinderException ex) {
        				
        			}
        			
        		}
        		
        		// Get Bank Charge Account Number with Description                
        		
        		if (adBankAccount.getBaCoaGlBankChargeAccount() != null) {
        			
        			try {
        				
        				glBankChargeChartOfAccount = 
        					glChartOfAccountHome.findByPrimaryKey(adBankAccount.getBaCoaGlBankChargeAccount());
        				
        				mdetails.setBaCoaBankChargeAccount(glBankChargeChartOfAccount.getCoaAccountNumber());
        				mdetails.setBaBankChargeAccountDescription(glBankChargeChartOfAccount.getCoaAccountDescription());
        				
        			} catch (FinderException ex) {
        				
        			}
        			
        		}
        		
        		// Get Clearing Account Number with Description
        		
        		if (adBankAccount.getBaCoaGlClearingAccount() != null) {
        			
        			try {
        				
        				glClearingChartOfAccount = 
        					glChartOfAccountHome.findByPrimaryKey(adBankAccount.getBaCoaGlClearingAccount());
        				
        				mdetails.setBaCoaClearingAccount(glClearingChartOfAccount.getCoaAccountNumber());
        				mdetails.setBaClearingAccountDescription(glClearingChartOfAccount.getCoaAccountDescription());
        				
        			} catch (FinderException ex) {
        				
        			}
        			
        		}
        		
        		// Get Interest Account Number with Description
        		
        		if (adBankAccount.getBaCoaGlInterestAccount() != null) {
        			
        			try {
        				
        				glInterestChartOfAccount = 
        					glChartOfAccountHome.findByPrimaryKey(adBankAccount.getBaCoaGlInterestAccount());
        				
        				mdetails.setBaCoaInterestAccount(glInterestChartOfAccount.getCoaAccountNumber());
        				mdetails.setBaInterestAccountDescription(glInterestChartOfAccount.getCoaAccountDescription());
        				
        			} catch (FinderException ex) {
        				
        			}
        			
        			
        		}
        		
        		
        		// Get Adjustment Account Number with Description
        		
        		if (adBankAccount.getBaCoaGlAdjustmentAccount() != null) {
        			
        			try {
        				
        				glAdjustmentChartOfAccount = 
        					glChartOfAccountHome.findByPrimaryKey(adBankAccount.getBaCoaGlAdjustmentAccount());
        				
        				mdetails.setBaCoaAdjustmentAccount(glAdjustmentChartOfAccount.getCoaAccountNumber());
        				mdetails.setBaAdjustmentAccountDescription(glAdjustmentChartOfAccount.getCoaAccountDescription());
        				
        			} catch (FinderException ex) {
        				
        			}		                   
        			
        		}
        		
        		// Get Cash Discount Account Number with Description
        		
        		if (adBankAccount.getBaCoaGlCashDiscount() != null) {
        			
        			try {
        				
        				glCashDiscountChartOfAccount = 
        					glChartOfAccountHome.findByPrimaryKey(adBankAccount.getBaCoaGlCashDiscount());
        				
        				mdetails.setBaCoaCashDiscount(glCashDiscountChartOfAccount.getCoaAccountNumber());
        				mdetails.setBaCashDiscountDescription(glCashDiscountChartOfAccount.getCoaAccountDescription());
        				
        			} catch (FinderException ex) {
        				
        			} 		    			    		    
        			
        		}
        		
        		// Get Sales Discount Account Number with Description
        		
        		if (adBankAccount.getBaCoaGlSalesDiscount() != null) {
        			
        			try {
        				
        				glSalesDiscountChartOfAccount = 
        					glChartOfAccountHome.findByPrimaryKey(adBankAccount.getBaCoaGlSalesDiscount());
        				
        				mdetails.setBaCoaSalesDiscount(glSalesDiscountChartOfAccount.getCoaAccountNumber());
        				mdetails.setBaSalesDiscountDescription(glSalesDiscountChartOfAccount.getCoaAccountDescription());
        				
        			} catch (FinderException ex) {
        				
        			} 
        			
        		}
        		
        		// Get Advance Account Number with Description
        		
        		if (adBankAccount.getBaCoaGlAdvanceAccount() != null) {
        			
        			try {
        				
        				glAdvanceChartOfAccount = 
        					glChartOfAccountHome.findByPrimaryKey(adBankAccount.getBaCoaGlAdvanceAccount());
        				
        				mdetails.setBaCoaAdvanceAccount(glAdvanceChartOfAccount.getCoaAccountNumber());
        				mdetails.setBaAdvanceAccountDescription(glAdvanceChartOfAccount.getCoaAccountDescription());
        				
        			} catch (FinderException ex) {
        				
        			} 
        			
        		}
        		
        		
        		
        		// Get Unapplied Check Account Number with Description
        		
        		if (adBankAccount.getBaCoaGlUnappliedCheck() != null) {
        			
        			try {
        				
        				glUnappliedCheckChartOfAccount = 
        					glChartOfAccountHome.findByPrimaryKey(adBankAccount.getBaCoaGlUnappliedCheck());
        				
        				mdetails.setBaCoaUnappliedCheck(glUnappliedCheckChartOfAccount.getCoaAccountNumber());
        				mdetails.setBaUnappliedCheckDescription(glUnappliedCheckChartOfAccount.getCoaAccountDescription());			    			    
        				
        			} catch (FinderException ex) {
        				
        			}
        			
        		}
        		
    			// get latest bank account balance for current bank account
    			
        		double bankAccountBalance = 0;
				
    			Collection adBankAccountBalances = adBankAccountBalanceHome.findByBaCodeAndType(adBankAccount.getBaCode(), "BOOK", AD_CMPNY);

    			if (!adBankAccountBalances.isEmpty()) {
    				
    				// get last check
    				
    				ArrayList adBankAccountBalanceList = new ArrayList(adBankAccountBalances);
    				
    				LocalAdBankAccountBalance adBankAccountBalance = (LocalAdBankAccountBalance)adBankAccountBalanceList.get(adBankAccountBalanceList.size() - 1);
    			
    				bankAccountBalance = adBankAccountBalance.getBabBalance();
    				
    			}

        		mdetails.setBaAvailableBalance(bankAccountBalance);
        		mdetails.setBaFloatBalance(adBankAccount.getBaFloatBalance());
        		mdetails.setBaNextCheckNumber(adBankAccount.getBaNextCheckNumber());
        		mdetails.setBaEnable(adBankAccount.getBaEnable());
        		mdetails.setBaAccountNumberShow(adBankAccount.getBaAccountNumberShow());
        		mdetails.setBaAccountNumberTop(adBankAccount.getBaAccountNumberTop());
        		mdetails.setBaAccountNumberLeft(adBankAccount.getBaAccountNumberLeft());
        		mdetails.setBaAccountNameShow(adBankAccount.getBaAccountNameShow());
        		mdetails.setBaAccountNameTop(adBankAccount.getBaAccountNameTop());
        		mdetails.setBaAccountNameLeft(adBankAccount.getBaAccountNameLeft());
        		mdetails.setBaNumberShow(adBankAccount.getBaNumberShow());
        		mdetails.setBaNumberTop(adBankAccount.getBaNumberTop());
        		mdetails.setBaNumberLeft(adBankAccount.getBaNumberLeft());
        		mdetails.setBaDateShow(adBankAccount.getBaDateShow());
        		mdetails.setBaDateTop(adBankAccount.getBaDateTop());
        		mdetails.setBaDateLeft(adBankAccount.getBaDateLeft());
        		mdetails.setBaPayeeShow(adBankAccount.getBaPayeeShow());
        		mdetails.setBaPayeeTop(adBankAccount.getBaPayeeTop());
        		mdetails.setBaPayeeLeft(adBankAccount.getBaPayeeLeft());
        		mdetails.setBaAmountShow(adBankAccount.getBaAmountShow());
        		mdetails.setBaAmountTop(adBankAccount.getBaAmountTop());
        		mdetails.setBaAmountLeft(adBankAccount.getBaAmountLeft());
        		mdetails.setBaWordAmountShow(adBankAccount.getBaWordAmountShow());
        		mdetails.setBaWordAmountTop(adBankAccount.getBaWordAmountTop());
        		mdetails.setBaWordAmountLeft(adBankAccount.getBaWordAmountLeft());
        		mdetails.setBaCurrencyShow(adBankAccount.getBaCurrencyShow());
        		mdetails.setBaCurrencyTop(adBankAccount.getBaCurrencyTop());
        		mdetails.setBaCurrencyLeft(adBankAccount.getBaCurrencyLeft());
        		mdetails.setBaAddressShow(adBankAccount.getBaAddressShow());
        		mdetails.setBaAddressTop(adBankAccount.getBaAddressTop());
        		mdetails.setBaAddressLeft(adBankAccount.getBaAddressLeft());
        		mdetails.setBaMemoShow(adBankAccount.getBaMemoShow());
        		mdetails.setBaMemoTop(adBankAccount.getBaMemoTop());
        		mdetails.setBaMemoLeft(adBankAccount.getBaMemoLeft());
        		mdetails.setBadocNumberShow(adBankAccount.getBadocNumberShow());
        		mdetails.setBadocNumberTop(adBankAccount.getBadocNumberTop());
        		mdetails.setBadocNumberLeft(adBankAccount.getBadocNumberLeft());
        		mdetails.setBaFontSize(adBankAccount.getBaFontSize());
        		mdetails.setBaFontStyle(adBankAccount.getBaFontStyle());
        		mdetails.setBaBankName(adBankAccount.getAdBank().getBnkName());
        		mdetails.setBaFcName(adBankAccount.getGlFunctionalCurrency().getFcName());
        		mdetails.setBaIsCashAccount(adBankAccount.getBaIsCashAccount());
        		
        		list.add(mdetails);
        	}
        	
        	return list;
        	
        } catch (GlobalNoRecordFoundException ex) {
        	
        	throw ex;
        	
        } catch (Exception ex) {
            
            ex.printStackTrace();
        	throw new EJBException(ex.getMessage());
        }
            
    }
    
    
    
    
    
    /**
     * @ejb:interface-method view-type="remote"
     **/    
    public void addAdBaEntry(com.util.AdModBankAccountDetails mdetails, 
        String BA_COA_GL_CSH_ACCNT, 
        String BA_COA_GL_ON_ACCNT_RCPT, 
        String BA_COA_GL_UNPPLD_RCPT, 
        String BA_COA_GL_BNK_CHRG_ACCNT, 
        String BA_COA_GL_CLRNG_ACCNT, 
        String BA_COA_GL_INTRST_ACCNT, 
        String BA_COA_GL_ADJSTMNT_ACCNT, 
        String BA_COA_GL_CSH_DSCNT, 
        String BA_COA_GL_SLS_DSCNT, 
        String BA_COA_GL_UNPPLD_CHK,
        String BA_COA_GL_ADVNC_ACCNT,
        String BNK_NM, String FC_NM, ArrayList branchList,Integer AD_CMPNY) 
        
        throws GlobalRecordAlreadyExistException,
               AdBACoaGlCashAccountNotFoundException,
               AdBACoaGlAccountReceiptNotFoundException,
               AdBACoaGlUnappliedReceiptNotFoundException,
               AdBACoaGlUnappliedCheckNotFoundException,
               AdBACoaGlBankChargeAccountNotFoundException,
               AdBACoaGlClearingAccountNotFoundException,
               AdBACoaGlCashDiscountNotFoundException,
               AdBACoaGlSalesDiscountNotFoundException,
               AdBACoaGlInterestAccountNotFoundException,
               AdBACoaGlAdjustmentAccountNotFoundException,
               AdBACoaGlAdvanceAccountNotFoundException{
                    
        Debug.print("AdBankAccountControllerBean addAdBaEntry");
        
        LocalAdBankAccountHome adBankAccountHome = null;
        LocalGlChartOfAccountHome glChartOfAccountHome = null;
        LocalAdBankHome adBankHome = null;
        LocalGlFunctionalCurrencyHome glFunctionalCurrencyHome = null;
        LocalAdBranchBankAccountHome adBranchBankAccountHome = null;
        LocalAdBranchHome adBranchHome = null;
                
        ArrayList list = new ArrayList();
        
        LocalAdBankAccount adBankAccount = null;
        LocalAdBranchBankAccount adBranchBankAccount = null;
        LocalAdBranch adBranch = null;
   
        // Initialize EJB Home
        
        try {
            
            adBankAccountHome = (LocalAdBankAccountHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdBankAccountHome.JNDI_NAME, LocalAdBankAccountHome.class);
            glChartOfAccountHome = (LocalGlChartOfAccountHome)EJBHomeFactory.
                lookUpLocalHome(LocalGlChartOfAccountHome.JNDI_NAME, LocalGlChartOfAccountHome.class);
            adBankHome = (LocalAdBankHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdBankHome.JNDI_NAME, LocalAdBankHome.class);
            glFunctionalCurrencyHome = (LocalGlFunctionalCurrencyHome)EJBHomeFactory.
                lookUpLocalHome(LocalGlFunctionalCurrencyHome.JNDI_NAME, LocalGlFunctionalCurrencyHome.class);
            adBranchBankAccountHome = (LocalAdBranchBankAccountHome)EJBHomeFactory.
            	lookUpLocalHome(LocalAdBranchBankAccountHome.JNDI_NAME, LocalAdBranchBankAccountHome.class);
            adBranchHome = (LocalAdBranchHome)EJBHomeFactory.
        		lookUpLocalHome(LocalAdBranchHome.JNDI_NAME, LocalAdBranchHome.class);
                         
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
       
        
        System.out.println("--------------------->1");
        
        // get glChartOfAccount to validate accounts 
        
        LocalGlChartOfAccount glCashChartOfAccount = null;
		LocalGlChartOfAccount glBankChargeChartOfAccount = null;       
		LocalGlChartOfAccount glInterestChartOfAccount = null;
		LocalGlChartOfAccount glAdjustmentChartOfAccount = null;	
		LocalGlChartOfAccount glSalesDiscountChartOfAccount = null;	
		LocalGlChartOfAccount glAdvanceAccountChartOfAccount = null;
		
		
		LocalGlChartOfAccount glCashDiscountChartOfAccount = null;	
		LocalGlChartOfAccount glOnAccountReceiptChartOfAccount = null;
		LocalGlChartOfAccount glUnappliedReceiptChartOfAccount = null;
		LocalGlChartOfAccount glUnappliedCheckChartOfAccount = null;
		
		
		
		LocalGlChartOfAccount glClearingChartOfAccount = null;
		if (BA_COA_GL_CSH_ACCNT != null && BA_COA_GL_CSH_ACCNT.length() > 0) {
		
	        try {
	       
	            glCashChartOfAccount = glChartOfAccountHome.findByCoaAccountNumber(
	               BA_COA_GL_CSH_ACCNT, AD_CMPNY);
	               
	        } catch (FinderException ex) {
	        
	            throw new AdBACoaGlCashAccountNotFoundException();
	            
	        } catch (Exception ex) {
	       
	            throw new EJBException(ex.getMessage());
	           
	        }
	        
    	}
    	
    	
    	if (BA_COA_GL_BNK_CHRG_ACCNT != null && BA_COA_GL_BNK_CHRG_ACCNT.length() > 0) {
    		
	        try {
	       
	            glBankChargeChartOfAccount = glChartOfAccountHome.findByCoaAccountNumber(
	               BA_COA_GL_BNK_CHRG_ACCNT, AD_CMPNY);
	               
	        } catch (FinderException ex) {
	        
	            throw new AdBACoaGlBankChargeAccountNotFoundException();
	            
	        } catch (Exception ex) {
	       
	            throw new EJBException(ex.getMessage());
	           
	        }  
	        
    	}
    	
    	
    	
    	if (BA_COA_GL_INTRST_ACCNT != null && BA_COA_GL_INTRST_ACCNT.length() > 0) {
    		
        
	        try {
	       
	            glInterestChartOfAccount = glChartOfAccountHome.findByCoaAccountNumber(
	               BA_COA_GL_INTRST_ACCNT, AD_CMPNY);
	               
	        } catch (FinderException ex) {
	        
	            throw new AdBACoaGlInterestAccountNotFoundException();
	            
	        } catch (Exception ex) {
	       
	            throw new EJBException(ex.getMessage());
	           
	        }
        
    	}
    	
    	if (BA_COA_GL_ADJSTMNT_ACCNT != null && BA_COA_GL_ADJSTMNT_ACCNT.length() > 0) {
    		
	        try {
	       
	            glAdjustmentChartOfAccount = glChartOfAccountHome.findByCoaAccountNumber(
	               BA_COA_GL_ADJSTMNT_ACCNT, AD_CMPNY);
	               
	        } catch (FinderException ex) {
	        
	            throw new AdBACoaGlAdjustmentAccountNotFoundException();
	            
	        } catch (Exception ex) {
	       
	            throw new EJBException(ex.getMessage());
	           
	        }                      
	        
	    }

        
      	if (BA_COA_GL_SLS_DSCNT != null && BA_COA_GL_SLS_DSCNT.length() > 0) {

	        try {
	       
	            glSalesDiscountChartOfAccount = glChartOfAccountHome.findByCoaAccountNumber(
	               BA_COA_GL_SLS_DSCNT, AD_CMPNY);
	               
	        } catch (FinderException ex) {
	        
	            throw new AdBACoaGlSalesDiscountNotFoundException();
	            
	        } catch (Exception ex) {
	       
	            throw new EJBException(ex.getMessage());
	           
	        } 
        
    	}
    	
    	
    	
    	
    	if (BA_COA_GL_ADVNC_ACCNT != null && BA_COA_GL_ADVNC_ACCNT.length() > 0) {
    		
	        try {
	       
	        	glAdvanceAccountChartOfAccount = glChartOfAccountHome.findByCoaAccountNumber(
	        			BA_COA_GL_ADVNC_ACCNT, AD_CMPNY);
	               
	        } catch (FinderException ex) {
	        
	            throw new AdBACoaGlAdvanceAccountNotFoundException();
	            
	        } catch (Exception ex) {
	       
	            throw new EJBException(ex.getMessage());
	           
	        }  
        
    	}
        
        System.out.println("--------------------->2");
        
        try {
        	
        	// create new bank account
        	
        	adBankAccount = adBankAccountHome.create(mdetails.getBaName(), mdetails.getBaDescription(),
	        	mdetails.getBaAccountType(), mdetails.getBaAccountNumber(), mdetails.getBaAccountUse(), 
	        	glCashChartOfAccount != null ? glCashChartOfAccount.getCoaCode() : null, 
	        	glOnAccountReceiptChartOfAccount != null ? glOnAccountReceiptChartOfAccount.getCoaCode() : null, 
	        	glUnappliedReceiptChartOfAccount != null ? glUnappliedReceiptChartOfAccount.getCoaCode() : null, 
	        	glBankChargeChartOfAccount != null ? glBankChargeChartOfAccount.getCoaCode() : null, 
	        	glClearingChartOfAccount != null ? glClearingChartOfAccount.getCoaCode() : null, 
	        	glInterestChartOfAccount != null ? glInterestChartOfAccount.getCoaCode() : null,
	        	glAdjustmentChartOfAccount != null ? glAdjustmentChartOfAccount.getCoaCode() : null, 
	        	glCashDiscountChartOfAccount != null ? glCashDiscountChartOfAccount.getCoaCode() : null,
	        	glSalesDiscountChartOfAccount != null ? glSalesDiscountChartOfAccount.getCoaCode() : null, 
	        	glAdvanceAccountChartOfAccount != null ? glAdvanceAccountChartOfAccount.getCoaCode() : null,
	        	glUnappliedCheckChartOfAccount != null ? glUnappliedCheckChartOfAccount.getCoaCode() : null, 
	        	
	        			mdetails.getBaFloatBalance(), null, 0d, mdetails.getBaNextCheckNumber(), mdetails.getBaEnable(),
	        	mdetails.getBaAccountNumberShow(), mdetails.getBaAccountNumberTop(), mdetails.getBaAccountNumberLeft(),
		        mdetails.getBaAccountNameShow(), mdetails.getBaAccountNameTop(), mdetails.getBaAccountNameLeft(),
		        mdetails.getBaNumberShow(), mdetails.getBaNumberTop(), mdetails.getBaNumberLeft(), 
		        mdetails.getBaDateShow(), mdetails.getBaDateTop(), mdetails.getBaDateLeft(),
		        mdetails.getBaPayeeShow(), mdetails.getBaPayeeTop(), mdetails.getBaPayeeLeft(),
		        mdetails.getBaAmountShow(), mdetails.getBaAmountTop(), mdetails.getBaAmountLeft(),
		        mdetails.getBaWordAmountShow(), mdetails.getBaWordAmountTop(), mdetails.getBaWordAmountLeft(), 
		        mdetails.getBaCurrencyShow(), mdetails.getBaCurrencyTop(), mdetails.getBaCurrencyLeft(),
		        mdetails.getBaAddressShow(), mdetails.getBaAddressTop(), mdetails.getBaAddressLeft(), 
		        mdetails.getBaMemoShow(), mdetails.getBaMemoTop(), mdetails.getBaMemoLeft(), 
		        mdetails.getBadocNumberShow(), mdetails.getBadocNumberTop(), mdetails.getBadocNumberLeft(), mdetails.getBaFontSize(), mdetails.getBaFontStyle(),
		        mdetails.getBaIsCashAccount(), AD_CMPNY);   	        

	        	LocalAdBank adBank = adBankHome.findByBnkName(BNK_NM, AD_CMPNY);
		      	adBank.addAdBankAccount(adBankAccount); 
		      	
		      	LocalGlFunctionalCurrency glFunctionalCurrency = glFunctionalCurrencyHome.findByFcName(FC_NM, AD_CMPNY);
		      	glFunctionalCurrency.addAdBankAccount(adBankAccount);
		      	
                	       
        } catch (Exception ex) {
        	
        	getSessionContext().setRollbackOnly();
            throw new EJBException(ex.getMessage());
        	
        } 
        
        System.out.println("--------------------->3");
        
        
     // add branch dsa
        
        try{
        	
        	System.out.println("--------------------->4");
            
  	      Iterator i = branchList.iterator();
  	      System.out.println("branchList="+branchList.size());
  	    System.out.println("--------------------->5");
        
  	      while(i.hasNext()) {	          	          
  	    	System.out.println("--------------------->6");
  	           
  	    	AdModBranchBankAccountDetails brBaDetails = (AdModBranchBankAccountDetails)i.next();
  	    	
  	    	Integer glCashChrtOfAccntCode = null;
  	    	Integer glBankChargeChrtOfAccntCode = null;
  	    	Integer glInterestChrtOfAccntCode = null;
  	    	Integer glAdjustmnetChrtOfAccntCode = null;
  	    	Integer glSalesDiscountChrtOfAccntCode = null;
  	    	Integer glAdvanceChrtOfAccntCode = null;
  	    	
  	    	
  	    	System.out.println("brBaDetails.getBbaCashAccountNumber()="+brBaDetails.getBbaCashAccountNumber());	
  	    	System.out.println("brBaDetails.getBbaBankChargeAccountNumber()="+brBaDetails.getBbaBankChargeAccountNumber());	
  	    	System.out.println("brBaDetails.getBbaInterestAccountNumber()="+brBaDetails.getBbaInterestAccountNumber());	
  	    	System.out.println("brBaDetails.getBbaAdjustmentAccountNumber()="+brBaDetails.getBbaAdjustmentAccountNumber());	
  	    	System.out.println("brBaDetails.getBbaSalesDiscountAccountNumber()="+brBaDetails.getBbaSalesDiscountAccountNumber());	
  	    	
  	    	
  	    	if(brBaDetails.getBbaCashAccountNumber() != null && brBaDetails.getBbaCashAccountNumber().length() > 0){
  	    		glCashChartOfAccount = glChartOfAccountHome.findByCoaAccountNumber(
	            		brBaDetails.getBbaCashAccountNumber(), AD_CMPNY);
  	    	}
  	    	
  	    	if(brBaDetails.getBbaBankChargeAccountNumber() != null && brBaDetails.getBbaBankChargeAccountNumber().length() > 0){
  	    		glBankChargeChartOfAccount = glChartOfAccountHome.findByCoaAccountNumber(
	    				brBaDetails.getBbaBankChargeAccountNumber(), AD_CMPNY);
  	    	}
  	    	
  	    	if(brBaDetails.getBbaInterestAccountNumber() != null && brBaDetails.getBbaInterestAccountNumber().length() > 0){
  	    		glInterestChartOfAccount = glChartOfAccountHome.findByCoaAccountNumber(
	    				brBaDetails.getBbaInterestAccountNumber(), AD_CMPNY);

  	    	}

  	    	if(brBaDetails.getBbaAdjustmentAccountNumber() != null && brBaDetails.getBbaAdjustmentAccountNumber().length() > 0){
  	    		glAdjustmentChartOfAccount = glChartOfAccountHome.findByCoaAccountNumber(
	    				brBaDetails.getBbaAdjustmentAccountNumber(), AD_CMPNY);


  	    	}
  	    	
  	    	if(brBaDetails.getBbaSalesDiscountAccountNumber() != null && brBaDetails.getBbaSalesDiscountAccountNumber().length() > 0){
  	    		glSalesDiscountChartOfAccount = glChartOfAccountHome.findByCoaAccountNumber(
	    				brBaDetails.getBbaSalesDiscountAccountNumber(), AD_CMPNY);

  	    	}
			
  	    	if(brBaDetails.getBbaAdvanceAccountNumber() != null && brBaDetails.getBbaAdvanceAccountNumber().length() > 0){
  	    		glAdvanceAccountChartOfAccount = glChartOfAccountHome.findByCoaAccountNumber(
						brBaDetails.getBbaAdvanceAccountNumber(), AD_CMPNY);


  	    	}
			
			
		     
		 glCashChrtOfAccntCode = glCashChartOfAccount.getCoaCode();
  	     glBankChargeChrtOfAccntCode = glBankChargeChartOfAccount.getCoaCode();
  	     glInterestChrtOfAccntCode = glInterestChartOfAccount.getCoaCode();
  	     glAdjustmnetChrtOfAccntCode = glAdjustmentChartOfAccount.getCoaCode();
  	     glSalesDiscountChrtOfAccntCode = glSalesDiscountChartOfAccount.getCoaCode();
  	     glAdvanceChrtOfAccntCode = glAdvanceAccountChartOfAccount.getCoaCode();
			
		     
  	          adBranchBankAccount = adBranchBankAccountHome.create(
  	        		glCashChrtOfAccntCode, 
  	        		glBankChargeChrtOfAccntCode,
  	        		glInterestChrtOfAccntCode,
  	        		glAdjustmnetChrtOfAccntCode,
  	        		glSalesDiscountChrtOfAccntCode,
  	        		glAdvanceChrtOfAccntCode,
	        		'N',AD_CMPNY);      	                   


  	        adBankAccount.addAdBranchBankAccount(adBranchBankAccount);
  	          adBranch = adBranchHome.findByPrimaryKey(brBaDetails.getBbaBranchCode());
  	          adBranch.addAdBranchBankAccount(adBranchBankAccount);
  	          
  	      }
        
        } catch (Exception ex) {
            
        	  getSessionContext().setRollbackOnly();
            throw new EJBException(ex.getMessage());
        	
        }   
        
  
        
    }
    
    
    
    /**
     * @ejb:interface-method view-type="remote"
     **/
    public void updateAdBaEntry(com.util.AdModBankAccountDetails details, 
            String BA_COA_GL_CSH_ACCNT, 
            String BA_COA_GL_ON_ACCNT_RCPT, 
            String BA_COA_GL_UNPPLD_RCPT, 
            String BA_COA_GL_BNK_CHRG_ACCNT, 
            String BA_COA_GL_CLRNG_ACCNT, 
            String BA_COA_GL_INTRST_ACCNT, 
            String BA_COA_GL_ADJSTMNT_ACCNT, 
            String BA_COA_GL_CSH_DSCNT, 
            String BA_COA_GL_SLS_DSCNT, 
            String BA_COA_GL_UNPPLD_CHK,
            String BA_COA_GL_ADVNC_ACCNT,
            String BNK_NM, String FC_NM, String RS_NM, ArrayList branchList,Integer AD_CMPNY) 
        
        throws GlobalRecordAlreadyExistException,
               AdBACoaGlCashAccountNotFoundException,
               AdBACoaGlAccountReceiptNotFoundException,
               AdBACoaGlUnappliedReceiptNotFoundException,
               AdBACoaGlUnappliedCheckNotFoundException,
               AdBACoaGlBankChargeAccountNotFoundException,
               AdBACoaGlClearingAccountNotFoundException,
               AdBACoaGlCashDiscountNotFoundException,
               AdBACoaGlSalesDiscountNotFoundException,
               AdBACoaGlInterestAccountNotFoundException,
               AdBACoaGlAdjustmentAccountNotFoundException,
    			AdBACoaGlAdvanceAccountNotFoundException{
                    
        Debug.print("AdBankAccountControllerBean updateAdBaEntry");
        
        LocalAdBankAccountHome adBankAccountHome = null;
        LocalGlChartOfAccountHome glChartOfAccountHome = null;
        LocalAdBankHome adBankHome = null;
        LocalGlFunctionalCurrencyHome glFunctionalCurrencyHome = null;
        LocalAdBranchBankAccountHome adBranchBankAccountHome = null;
        LocalAdBranchBankAccount adBranchBankAccount = null;
        LocalAdBranchHome adBranchHome = null;

        LocalAdBankAccount adBankAccount = null;
        LocalAdBranch adBranch = null;
                
        ArrayList list = new ArrayList();
        
        // Initialize EJB Home
        
        try {
            
            
            adBankAccountHome = (LocalAdBankAccountHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdBankAccountHome.JNDI_NAME, LocalAdBankAccountHome.class);
            glChartOfAccountHome = (LocalGlChartOfAccountHome)EJBHomeFactory.
                lookUpLocalHome(LocalGlChartOfAccountHome.JNDI_NAME, LocalGlChartOfAccountHome.class);
            adBankHome = (LocalAdBankHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdBankHome.JNDI_NAME, LocalAdBankHome.class);
            glFunctionalCurrencyHome = (LocalGlFunctionalCurrencyHome)EJBHomeFactory.
                lookUpLocalHome(LocalGlFunctionalCurrencyHome.JNDI_NAME, LocalGlFunctionalCurrencyHome.class);
            adBranchBankAccountHome = (LocalAdBranchBankAccountHome)EJBHomeFactory.
            	lookUpLocalHome(LocalAdBranchBankAccountHome.JNDI_NAME, LocalAdBranchBankAccountHome.class);	
            adBranchHome = (LocalAdBranchHome)EJBHomeFactory.
        		lookUpLocalHome(LocalAdBranchHome.JNDI_NAME, LocalAdBranchHome.class);
                          
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
       
        
        try { 
            
            LocalAdBankAccount adExistingBankAccount = adBankAccountHome.findByBaName(details.getBaName(), AD_CMPNY);
            
            if (!adExistingBankAccount.getBaCode().equals(details.getBaCode())) {
            
                 throw new GlobalRecordAlreadyExistException();
                 
            }
                                             
        } catch (GlobalRecordAlreadyExistException ex) {
        	
            throw ex;
        	
        } catch (FinderException ex) {
        	 	
        } catch (Exception ex) {
            
            throw new EJBException(ex.getMessage());
            
        }

        // get glChartOfAccount to validate accounts 
        
        LocalGlChartOfAccount glCashChartOfAccount = null;
        LocalGlChartOfAccount glOnAccountReceiptChartOfAccount = null;
		LocalGlChartOfAccount glUnappliedReceiptChartOfAccount = null;
		LocalGlChartOfAccount glUnappliedCheckChartOfAccount = null;
		LocalGlChartOfAccount glBankChargeChartOfAccount = null;       
		LocalGlChartOfAccount glClearingChartOfAccount = null;
		LocalGlChartOfAccount glCashDiscountChartOfAccount = null;		
		LocalGlChartOfAccount glSalesDiscountChartOfAccount = null;			
		LocalGlChartOfAccount glInterestChartOfAccount = null;
		LocalGlChartOfAccount glAdjustmentChartOfAccount = null;
		LocalGlChartOfAccount glAdvanceChartOfAccount = null;
		
		System.out.println("BA_COA_GL_CSH_ACCNT="+BA_COA_GL_CSH_ACCNT);
		
		
        if (BA_COA_GL_CSH_ACCNT != null && BA_COA_GL_CSH_ACCNT.length() > 0) {
		
	        try {
	       
	            glCashChartOfAccount = glChartOfAccountHome.findByCoaAccountNumber(
	               BA_COA_GL_CSH_ACCNT, AD_CMPNY);
	               
	        } catch (FinderException ex) {
	        
	            throw new AdBACoaGlCashAccountNotFoundException();
	            
	        } catch (Exception ex) {
	       
	            throw new EJBException(ex.getMessage());
	           
	        }
	        
    	}
    	
    
    	
    
    	
    	
    	if (BA_COA_GL_BNK_CHRG_ACCNT != null && BA_COA_GL_BNK_CHRG_ACCNT.length() > 0) {
    		
	        try {
	       
	            glBankChargeChartOfAccount = glChartOfAccountHome.findByCoaAccountNumber(
	               BA_COA_GL_BNK_CHRG_ACCNT, AD_CMPNY);
	               
	        } catch (FinderException ex) {
	        
	            throw new AdBACoaGlBankChargeAccountNotFoundException();
	            
	        } catch (Exception ex) {
	       
	            throw new EJBException(ex.getMessage());
	           
	        }  
	        
    	}
    
    	if (BA_COA_GL_INTRST_ACCNT != null && BA_COA_GL_INTRST_ACCNT.length() > 0) {
    		
        
	        try {
	       
	            glInterestChartOfAccount = glChartOfAccountHome.findByCoaAccountNumber(
	               BA_COA_GL_INTRST_ACCNT, AD_CMPNY);
	               
	        } catch (FinderException ex) {
	        
	            throw new AdBACoaGlInterestAccountNotFoundException();
	            
	        } catch (Exception ex) {
	       
	            throw new EJBException(ex.getMessage());
	           
	        }
        
    	}
    	
    	if (BA_COA_GL_ADJSTMNT_ACCNT != null && BA_COA_GL_ADJSTMNT_ACCNT.length() > 0) {
    		
	        try {
	       
	            glAdjustmentChartOfAccount = glChartOfAccountHome.findByCoaAccountNumber(
	               BA_COA_GL_ADJSTMNT_ACCNT, AD_CMPNY);
	               
	        } catch (FinderException ex) {
	        
	            throw new AdBACoaGlAdjustmentAccountNotFoundException();
	            
	        } catch (Exception ex) {
	       
	            throw new EJBException(ex.getMessage());
	           
	        }                      
	        
	    }

        
      	
      	if (BA_COA_GL_SLS_DSCNT != null && BA_COA_GL_SLS_DSCNT.length() > 0) {

	        try {
	       
	            glSalesDiscountChartOfAccount = glChartOfAccountHome.findByCoaAccountNumber(
	               BA_COA_GL_SLS_DSCNT, AD_CMPNY);
	               
	        } catch (FinderException ex) {
	        
	            throw new AdBACoaGlSalesDiscountNotFoundException();
	            
	        } catch (Exception ex) {
	       
	            throw new EJBException(ex.getMessage());
	           
	        } 
        
    	}
      	
      	if (BA_COA_GL_ADVNC_ACCNT != null && BA_COA_GL_ADVNC_ACCNT.length() > 0) {

	        try {
	       
	        	glAdvanceChartOfAccount = glChartOfAccountHome.findByCoaAccountNumber(
	        			BA_COA_GL_ADVNC_ACCNT, AD_CMPNY);
	               
	        } catch (FinderException ex) {
	        
	            throw new AdBACoaGlAdvanceAccountNotFoundException();
	            
	        } catch (Exception ex) {
	       
	            throw new EJBException(ex.getMessage());
	           
	        } 
        
    	}
    	
    	

               
        try {
        	        	
        	// find and update bank account
        	System.out.println("details.getBaCode()="+details.getBaCode());
        	adBankAccount = adBankAccountHome.findByPrimaryKey(details.getBaCode());
        	
        	System.out.println("details.getBaName()="+details.getBaName());
        	System.out.println("details.getBaDescription()="+details.getBaDescription());
        	
        	
				adBankAccount.setBaName(details.getBaName());
				
				adBankAccount.setBaDescription(details.getBaDescription());
				adBankAccount.setBaAccountType(details.getBaAccountType());
				adBankAccount.setBaAccountNumber(details.getBaAccountNumber());
				adBankAccount.setBaAccountUse(details.getBaAccountUse());
				
				adBankAccount.setBaCoaGlCashAccount(glCashChartOfAccount != null ? glCashChartOfAccount.getCoaCode() : null);
				adBankAccount.setBaCoaGlBankChargeAccount(glBankChargeChartOfAccount != null ? glBankChargeChartOfAccount.getCoaCode() : null);
				adBankAccount.setBaCoaGlInterestAccount(glInterestChartOfAccount != null ? glInterestChartOfAccount.getCoaCode() : null);
				adBankAccount.setBaCoaGlAdjustmentAccount(glAdjustmentChartOfAccount != null ? glAdjustmentChartOfAccount.getCoaCode() : null);
				adBankAccount.setBaCoaGlSalesDiscount(glSalesDiscountChartOfAccount != null ? glSalesDiscountChartOfAccount.getCoaCode() : null);
				adBankAccount.setBaCoaGlAdvanceAccount(glAdvanceChartOfAccount != null ? glAdvanceChartOfAccount.getCoaCode() : null);
				
				adBankAccount.setBaCoaGlCashDiscount(glCashDiscountChartOfAccount != null ? glCashDiscountChartOfAccount.getCoaCode() : null);
				adBankAccount.setBaCoaGlClearingAccount(glClearingChartOfAccount != null ? glClearingChartOfAccount.getCoaCode() : null);
				adBankAccount.setBaCoaGlUnappliedReceipt(glUnappliedReceiptChartOfAccount != null ? glUnappliedReceiptChartOfAccount.getCoaCode() : null);
				adBankAccount.setBaCoaGlOnAccountReceipt(glOnAccountReceiptChartOfAccount != null ? glOnAccountReceiptChartOfAccount.getCoaCode() : null);
				adBankAccount.setBaCoaGlUnappliedCheck(glUnappliedCheckChartOfAccount != null ? glUnappliedCheckChartOfAccount.getCoaCode() : null);				
//				
				//adBankAccount.setBaAvailableBalance(details.getBaAvailableBalance());
				adBankAccount.setBaFloatBalance(details.getBaFloatBalance());
				adBankAccount.setBaNextCheckNumber(details.getBaNextCheckNumber());
				adBankAccount.setBaEnable(details.getBaEnable());
				adBankAccount.setBaAccountNumberShow(details.getBaAccountNumberShow());
		        adBankAccount.setBaAccountNumberTop(details.getBaAccountNumberTop());
		        adBankAccount.setBaAccountNumberLeft(details.getBaAccountNumberLeft());
		        adBankAccount.setBaAccountNameShow(details.getBaAccountNameShow());
		        adBankAccount.setBaAccountNameTop(details.getBaAccountNameTop());
		        adBankAccount.setBaAccountNameLeft(details.getBaAccountNameLeft());
		        adBankAccount.setBaNumberShow(details.getBaNumberShow());
		        adBankAccount.setBaNumberTop(details.getBaNumberTop());
		        adBankAccount.setBaNumberLeft(details.getBaNumberLeft());
		        adBankAccount.setBaDateShow(details.getBaDateShow());
		        adBankAccount.setBaDateTop(details.getBaDateTop());
		        adBankAccount.setBaDateLeft(details.getBaDateLeft());
		        adBankAccount.setBaPayeeShow(details.getBaPayeeShow());
		        adBankAccount.setBaPayeeTop(details.getBaPayeeTop());
		        adBankAccount.setBaPayeeLeft(details.getBaPayeeLeft());
		        adBankAccount.setBaAmountShow(details.getBaAmountShow());
		        adBankAccount.setBaAmountTop(details.getBaAmountTop());
		        adBankAccount.setBaAmountLeft(details.getBaAmountLeft());
		        adBankAccount.setBaWordAmountShow(details.getBaWordAmountShow());
		        adBankAccount.setBaWordAmountTop(details.getBaWordAmountTop());
		        adBankAccount.setBaWordAmountLeft(details.getBaWordAmountLeft());
		        adBankAccount.setBaCurrencyShow(details.getBaCurrencyShow());
		        adBankAccount.setBaCurrencyTop(details.getBaCurrencyTop());
		        adBankAccount.setBaCurrencyLeft(details.getBaCurrencyLeft());
		        adBankAccount.setBaAddressShow(details.getBaAddressShow());
		        adBankAccount.setBaAddressTop(details.getBaAddressTop());
		        adBankAccount.setBaAddressLeft(details.getBaAddressLeft());
		        adBankAccount.setBaMemoShow(details.getBaMemoShow());
		        adBankAccount.setBaMemoTop(details.getBaMemoTop());
		        adBankAccount.setBaMemoLeft(details.getBaMemoLeft());
		        adBankAccount.setBadocNumberShow(details.getBadocNumberShow());
		        adBankAccount.setBadocNumberTop(details.getBadocNumberTop());
		        adBankAccount.setBadocNumberLeft(details.getBadocNumberLeft());
		        adBankAccount.setBaFontSize(details.getBaFontSize());
		        adBankAccount.setBaFontStyle(details.getBaFontStyle());
		        adBankAccount.setBaIsCashAccount(details.getBaIsCashAccount());
		        
		        try {	
				
		        	LocalAdBank adBank = adBankHome.findByBnkName(BNK_NM, AD_CMPNY);
		        	adBank.addAdBankAccount(adBankAccount); 
		        
		        } catch (FinderException ex) {
		        	
			    }
			    
			    try {	
				
		        	LocalGlFunctionalCurrency glFunctionalCurrency = glFunctionalCurrencyHome.findByFcName(FC_NM, AD_CMPNY);
		      		glFunctionalCurrency.addAdBankAccount(adBankAccount); 
		        
		        } catch (FinderException ex) {
		        	
			    }

      	        	                            	
        } catch (Exception ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
       
      
        try{
            
  	      Collection adBranchBBAs = adBranchBankAccountHome.findBbaByBaCodeAndRsName(adBankAccount.getBaCode(), RS_NM, AD_CMPNY);
  	      
  	      Iterator i = adBranchBBAs.iterator();
  	      
  	      //remove all adBranchDSA lines
  	      while(i.hasNext()) {
  	      	
  	    	  
  	      	adBranchBankAccount = (LocalAdBranchBankAccount) i.next();
  	      	
  	      	adBankAccount.dropAdBranchBankAccount(adBranchBankAccount); 
  	      	System.out.println("adBranchBankAccount.getAdBranch().getBrCode()="+adBranchBankAccount.getAdBranch().getBrCode());
  	      	adBranch = adBranchHome.findByPrimaryKey(adBranchBankAccount.getAdBranch().getBrCode());
  	      	adBranch.dropAdBranchBankAccount(adBranchBankAccount);
  	      	adBranchBankAccount.remove();
  	      }
  	      
  	      
   	
        	System.out.println("--------------------->4");
            
  	      Iterator x = branchList.iterator();
  	      System.out.println("branchList="+branchList.size());
  	    System.out.println("--------------------->5");
        
  	      while(x.hasNext()) {	          	          
  	    	System.out.println("--------------------->6");
  	           
  	    	AdModBranchBankAccountDetails brBaDetails = (AdModBranchBankAccountDetails)x.next();
  	    	
  	    	/*Integer glCashChrtOfAccntCode = null;
  	    	Integer glBankChargeChrtOfAccntCode = null;
  	    	Integer glInterestChrtOfAccntCode = null;
  	    	Integer glAdjustmnetChrtOfAccntCode = null;
  	    	Integer glSalesDiscountChrtOfAccntCode = null;
  	    	Integer glAdvanceChrtOfAccntCode = null;*/
  	    	
  	    	
  	    	System.out.println("brBaDetails.getBbaCashAccountNumber()="+brBaDetails.getBbaCashAccountNumber());	
  	    	System.out.println("brBaDetails.getBbaBankChargeAccountNumber()="+brBaDetails.getBbaBankChargeAccountNumber());	
  	    	System.out.println("brBaDetails.getBbaInterestAccountNumber()="+brBaDetails.getBbaInterestAccountNumber());	
  	    	System.out.println("brBaDetails.getBbaAdjustmentAccountNumber()="+brBaDetails.getBbaAdjustmentAccountNumber());	
  	    	System.out.println("brBaDetails.getBbaSalesDiscountAccountNumber()="+brBaDetails.getBbaSalesDiscountAccountNumber());	
  	    	System.out.println("brBaDetails.getBbaAdvanceAccountNumber()="+brBaDetails.getBbaAdvanceAccountNumber());	
  	    	
  	    	LocalGlChartOfAccount glCashCOA = null;
  	    	LocalGlChartOfAccount glBankCOA = null;
  	    	LocalGlChartOfAccount glInterestCOA = null;
  	    	LocalGlChartOfAccount glAdjustmentCOA = null;
  	    	LocalGlChartOfAccount glSalesDiscountCOA = null;
  	    	LocalGlChartOfAccount glAdvanceCOA = null;

  	    	if(brBaDetails.getBbaCashAccountNumber() != null && brBaDetails.getBbaCashAccountNumber().length() > 0){
  	    		
  	    		try {
  	  		       
  	    			glCashCOA = glChartOfAccountHome.findByCoaAccountNumber(
  	  	    				brBaDetails.getBbaCashAccountNumber(), AD_CMPNY);
  		               
  		        } catch (FinderException ex) {
  		        
  		            throw new AdBACoaGlCashAccountNotFoundException();
  		            
  		        }
  	    		
  	    	}

  	    	if(brBaDetails.getBbaBankChargeAccountNumber() != null && brBaDetails.getBbaBankChargeAccountNumber().length() > 0){
  	    		
  	    		try {
  	  		       
  	    			glBankCOA = glChartOfAccountHome.findByCoaAccountNumber(
  	  	    				brBaDetails.getBbaBankChargeAccountNumber(), AD_CMPNY);
  		               
  		        } catch (FinderException ex) {
  		        
  		            throw new AdBACoaGlBankChargeAccountNotFoundException();
  		            
  		        }
  	    		
  	    	}
  	    	
  	    	
  	    	if(brBaDetails.getBbaInterestAccountNumber() != null && brBaDetails.getBbaInterestAccountNumber().length() > 0){
  	    		
  	    		try {
  	   		       
  	    			glInterestCOA = glChartOfAccountHome.findByCoaAccountNumber(
  		    				brBaDetails.getBbaInterestAccountNumber(), AD_CMPNY);
  		               
  		        } catch (FinderException ex) {
  		        
  		            throw new AdBACoaGlInterestAccountNotFoundException();
  		            
  		        }
  	    		
  	    	}
  	    	


  	    	if(brBaDetails.getBbaAdjustmentAccountNumber() != null && brBaDetails.getBbaAdjustmentAccountNumber().length() > 0){
  	    		
  	    		try {
  	   		       
  	    			glAdjustmentCOA = glChartOfAccountHome.findByCoaAccountNumber(
  		    				brBaDetails.getBbaAdjustmentAccountNumber(), AD_CMPNY);

  		               
  		        } catch (FinderException ex) {
  		        
  		            throw new AdBACoaGlAdjustmentAccountNotFoundException();
  		            
  		        }
  	    	}
  	    	
  	    	
  	    	
  	    	if(brBaDetails.getBbaSalesDiscountAccountNumber() != null && brBaDetails.getBbaSalesDiscountAccountNumber().length() > 0){
  	    		
  	    		try {
  	   		       
  	    			glSalesDiscountCOA= glChartOfAccountHome.findByCoaAccountNumber(
  		    				brBaDetails.getBbaSalesDiscountAccountNumber(), AD_CMPNY);
  			
  		               
  		        } catch (FinderException ex) {
  		        
  		            throw new AdBACoaGlSalesDiscountNotFoundException();
  		            
  		        }
  	    	}
  	    	
  	    	
  	    	
  	    	if(brBaDetails.getBbaAdvanceAccountNumber() != null && brBaDetails.getBbaAdvanceAccountNumber().length() > 0){
  	    		
  	    		try {
  	   		       
  	    			glAdvanceCOA = glChartOfAccountHome.findByCoaAccountNumber(
  	  	    				brBaDetails.getBbaAdvanceAccountNumber(), AD_CMPNY);
  	  		
  		               
  		        } catch (FinderException ex) {
  		        
  		            throw new AdBACoaGlAdvanceAccountNotFoundException();
  		            
  		        }
  	    	}
  	    	
  	    	
  	    			
  	    					
  	    	
  	          adBranchBankAccount = adBranchBankAccountHome.create(
  	       
  	        		glCashCOA != null ? glCashCOA.getCoaCode() : null,
  	        		glBankCOA != null ? glBankCOA.getCoaCode() : null,
  	        		glInterestCOA != null ? glInterestCOA.getCoaCode() : null,
  	        		glAdjustmentCOA != null ? glAdjustmentCOA.getCoaCode() : null,
  	        		glSalesDiscountCOA != null ? glSalesDiscountCOA.getCoaCode() : null,
  	        		glAdvanceCOA != null ? glAdvanceCOA.getCoaCode() : null,
	        		'N',AD_CMPNY);      	                   


  	        adBankAccount.addAdBranchBankAccount(adBranchBankAccount);
  	          adBranch = adBranchHome.findByPrimaryKey(brBaDetails.getBbaBranchCode());
  	          adBranch.addAdBranchBankAccount(adBranchBankAccount);
  	          
  	      }
        
        } catch (Exception ex) {
            
        	  getSessionContext().setRollbackOnly();
            throw new EJBException(ex.getMessage());
        	
        } 
        
    }

   /**
    * @ejb:interface-method view-type="remote"
    * @jboss:method-attributes read-only="true"
    **/
    public short getGlFcPrecisionUnit(Integer AD_CMPNY) {

       Debug.print("AdBankAccountControllerBean getGlFcPrecisionUnit");

      
       LocalAdCompanyHome adCompanyHome = null;
      
     
       // Initialize EJB Home
        
       try {
            
           adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
              lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);       
            
       } catch (NamingException ex) {
            
           throw new EJBException(ex.getMessage());
            
       }

       try {
       	
         LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
            
         return  adCompany.getGlFunctionalCurrency().getFcPrecision();
                                     
       } catch (Exception ex) {
       	 
       	 Debug.printStackTrace(ex);
         throw new EJBException(ex.getMessage());
         
       }

    }
    
    /**
     * @ejb:interface-method view-type="remote"
     **/
    public void deleteAdBaEntry(Integer BA_CODE, Integer AD_CMPNY) 
        throws GlobalRecordAlreadyAssignedException,
                 GlobalRecordAlreadyDeletedException {
                    
        Debug.print("AdBankAccountControllerBean deleteAdBaEntry");
        
        LocalAdBankAccountHome adBankAccountHome = null; 
        LocalCmFundTransferHome cmFundTransferHome = null;
        LocalAdBankAccount adBankAccount = null;           
               
        // Initialize EJB Home
        
        try {
            
            adBankAccountHome = (LocalAdBankAccountHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdBankAccountHome.JNDI_NAME, LocalAdBankAccountHome.class);  
            cmFundTransferHome = (LocalCmFundTransferHome)EJBHomeFactory.
                lookUpLocalHome(LocalCmFundTransferHome.JNDI_NAME, LocalCmFundTransferHome.class);   
            
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }

	    try {
         
            adBankAccount = adBankAccountHome.findByPrimaryKey(BA_CODE);

	    } catch (FinderException ex) {
	    	
	         throw new GlobalRecordAlreadyDeletedException();
	    
	    }
	    
	    try {
	    	
	    	Collection fundTransfers1 = cmFundTransferHome.findByAdBankAccountFrom(adBankAccount.getBaCode(), AD_CMPNY);
	    	Collection fundTransfers2 = cmFundTransferHome.findByAdBankAccountTo(adBankAccount.getBaCode(), AD_CMPNY);

    	    if(!adBankAccount.getCmAdjustments().isEmpty() ||
    	        !adBankAccount.getApSuppliers().isEmpty() ||
    	    	!adBankAccount.getApSupplierTypes().isEmpty() ||
				!adBankAccount.getArCustomers().isEmpty() ||
				!adBankAccount.getArCustomerTypes().isEmpty() ||
				!adBankAccount.getApChecks().isEmpty() ||
				!adBankAccount.getArReceipts().isEmpty() ||
				!fundTransfers1.isEmpty() ||
				!fundTransfers2.isEmpty()) {
    		
    		    throw new GlobalRecordAlreadyAssignedException();
    		
    	    }
    	    
    	    adBankAccount.remove();        	

        		                 	
        } catch (GlobalRecordAlreadyAssignedException ex) {
        	
        	 throw ex;
        	
        } catch (Exception ex) {
        	
        	 throw new EJBException(ex.getMessage());
        	
        }
        
	}

    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList getAdBnkAll(Integer AD_CMPNY) {
                    
        Debug.print("AdBankAccountControllerBean getAdBnkAll");
        
        LocalAdBankHome adBankHome = null;
        
        Collection adBanks = null;
        
        LocalAdBank adBank = null;
        
        ArrayList list = new ArrayList();
        
        // Initialize EJB Home
        
        try {
            
            adBankHome = (LocalAdBankHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdBankHome.JNDI_NAME, LocalAdBankHome.class);
            
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {
            
            adBanks = adBankHome.findEnabledBnkAll(AD_CMPNY);
            
        } catch (FinderException ex) {
            
        } catch (Exception ex) {
            
            throw new EJBException(ex.getMessage());
        }
        
        if (adBanks.isEmpty()) {
        	
        	return null;
        	
        }
        
        Iterator i = adBanks.iterator();
               
        while (i.hasNext()) {
        	
        	adBank = (LocalAdBank)i.next();
        	
        	list.add(adBank.getBnkName());
        	
        }
        
        return list;
            
    }

    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList getGlFcAllWithDefault(Integer AD_CMPNY) {
                    
        Debug.print("AdBankAccountControllerBean getGlFcAllWithDefault");
        
        LocalGlFunctionalCurrencyHome glFunctionalCurrencyHome = null;
        LocalAdCompanyHome adCompanyHome = null;
        
        Collection glFunctionalCurrencies = null;
        
        LocalGlFunctionalCurrency glFunctionalCurrency = null;
        LocalAdCompany adCompany = null;
        
        
        ArrayList list = new ArrayList();
        
        // Initialize EJB Home
        
        try {
            
            glFunctionalCurrencyHome = (LocalGlFunctionalCurrencyHome)EJBHomeFactory.
                lookUpLocalHome(LocalGlFunctionalCurrencyHome.JNDI_NAME, LocalGlFunctionalCurrencyHome.class);
            adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
            
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {
            
            adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
	               
            glFunctionalCurrencies = glFunctionalCurrencyHome.findFcAllEnabled(
            	EJBCommon.getGcCurrentDateWoTime().getTime(), AD_CMPNY);
            
        } catch (Exception ex) {
            
            throw new EJBException(ex.getMessage());
        }
        
        if (glFunctionalCurrencies.isEmpty()) {
        	
        	return null;
        	
        }
        
        Iterator i = glFunctionalCurrencies.iterator();
               
        while (i.hasNext()) {
        	
        	glFunctionalCurrency = (LocalGlFunctionalCurrency)i.next();
        	
        	GlModFunctionalCurrencyDetails mdetails = new GlModFunctionalCurrencyDetails(
        	    glFunctionalCurrency.getFcCode(), glFunctionalCurrency.getFcName(),
        	    adCompany.getGlFunctionalCurrency().getFcName().equals(glFunctionalCurrency.getFcName()) ?
        	       EJBCommon.TRUE : EJBCommon.FALSE);
        	
        	list.add(mdetails);
        	
        }
        
        return list;
            
    }
    
    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/   
    public ArrayList getAdBrResAll(Integer RS_CODE, Integer AD_CMPNY) 
    	throws GlobalNoRecordFoundException{
        
        Debug.print("AdBankAccountControllerBean getAdBrResAll");
        
        LocalAdBranchResponsibilityHome adBranchResponsibilityHome = null;
        LocalAdBranchHome adBranchHome = null;
        
        LocalAdBranchResponsibility adBranchResponsibility = null;
        LocalAdBranch adBranch = null;
        
        Collection adBranchResponsibilities = null;
        
        ArrayList list = new ArrayList();
        
        // Initialize EJB Home
        
        try {
            
            adBranchResponsibilityHome = (LocalAdBranchResponsibilityHome)EJBHomeFactory.
            lookUpLocalHome(LocalAdBranchResponsibilityHome.JNDI_NAME, LocalAdBranchResponsibilityHome.class);
            adBranchHome = (LocalAdBranchHome)EJBHomeFactory.
            lookUpLocalHome(LocalAdBranchHome.JNDI_NAME, LocalAdBranchHome.class);
            
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {
            
            adBranchResponsibilities = adBranchResponsibilityHome.findByAdResponsibility(RS_CODE, AD_CMPNY);
            
        } catch (FinderException ex) {
            
        } catch (Exception ex) {
            
            throw new EJBException(ex.getMessage());
        }
        
        if (adBranchResponsibilities.isEmpty()) {
            
            throw new GlobalNoRecordFoundException();
            
        }
        
        try {
            
            Iterator i = adBranchResponsibilities.iterator();
            
            while(i.hasNext()) {
                
                adBranchResponsibility = (LocalAdBranchResponsibility)i.next();
                
                adBranch = adBranchResponsibility.getAdBranch();
                
                AdBranchDetails details = new AdBranchDetails();
                
                details.setBrCode(adBranch.getBrCode());
                details.setBrName(adBranch.getBrName());
                
                list.add(details);
                
            }	               
            
        } catch (Exception ex) {
            
            throw new EJBException(ex.getMessage());
        }
        
        return list;
        
    }
    
    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/   
    public ArrayList getAdBrBaAll(Integer BA_CODE, Integer AD_CMPNY) 
    	throws GlobalNoRecordFoundException{
        
        Debug.print("AdBankAccountControllerBean getAdBrBaAll");
        
        LocalAdBranchBankAccountHome adBranchBankAccountHome  = null;
        LocalAdBranchHome adBranchHome = null;
        LocalGlChartOfAccountHome glChartOfAccountHome = null;
        
        LocalAdBranchBankAccount adBranchBankAccount = null;
        LocalAdBranch adBranch = null;
        LocalGlChartOfAccount glChartOfAccount = null;
        
        Collection adBranchBankAccounts = null;
        
        ArrayList list = new ArrayList();
       
        // Initialize EJB Home
        
        try {
            
            adBranchBankAccountHome = (LocalAdBranchBankAccountHome)EJBHomeFactory.
            	lookUpLocalHome(LocalAdBranchBankAccountHome.JNDI_NAME, LocalAdBranchBankAccountHome.class);
            adBranchHome = (LocalAdBranchHome)EJBHomeFactory.
            	lookUpLocalHome(LocalAdBranchHome.JNDI_NAME, LocalAdBranchHome.class);
            glChartOfAccountHome = (LocalGlChartOfAccountHome)EJBHomeFactory.
    				lookUpLocalHome(LocalGlChartOfAccountHome.JNDI_NAME, LocalGlChartOfAccountHome.class);

            
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {
        	System.out.println("BA_CODE="+BA_CODE);
            adBranchBankAccounts = adBranchBankAccountHome.findBbaByBaCode(BA_CODE, AD_CMPNY);
            
        } catch (FinderException ex) {
        	System.out.println("1");
        } catch (Exception ex) {
        	System.out.println("2");
            throw new EJBException(ex.getMessage());
        }
        
        if (adBranchBankAccounts.isEmpty()) {
        	System.out.println("3");
            throw new GlobalNoRecordFoundException();
            
        }
        
        System.out.println("adBranchBankAccounts.size()="+adBranchBankAccounts.size());
        try {
            
            Iterator i = adBranchBankAccounts.iterator();
            
            while(i.hasNext()) {
                
                adBranchBankAccount = (LocalAdBranchBankAccount)i.next();
                System.out.println("adBranchBankAccount.getAdBranch().getBrCode()="+adBranchBankAccount.getAdBranch().getBrCode());
                adBranch = adBranchHome.findByPrimaryKey(adBranchBankAccount.getAdBranch().getBrCode());                              
                
                
                //AdModBranchDocumentSequenceAssignmentDetails details = new AdModBranchDocumentSequenceAssignmentDetails();
                
                AdModBranchBankAccountDetails details = new AdModBranchBankAccountDetails();
                
                //AdBranchDetails details = new AdBranchDetails();
                details.setBbaBranchCode(adBranch.getBrCode());
                details.setBbaBranchName(adBranch.getBrName());
                
                if(adBranchBankAccount.getBbaGlCoaCashAccount() != null){
                	glChartOfAccount = glChartOfAccountHome.findByPrimaryKey(adBranchBankAccount.getBbaGlCoaCashAccount());
                    details.setBbaCashAccountNumber(glChartOfAccount.getCoaAccountNumber());
                    details.setBbaCashAccountDescription(glChartOfAccount.getCoaAccountDescription());
                    
                }
                
                if(adBranchBankAccount.getBbaGlCoaBankChargeAccount() != null){
                	glChartOfAccount = glChartOfAccountHome.findByPrimaryKey(adBranchBankAccount.getBbaGlCoaBankChargeAccount());
                    details.setBbaBankChargeAccountNumber(glChartOfAccount.getCoaAccountNumber());
                    details.setBbaBankChargeAccountDescription(glChartOfAccount.getCoaAccountDescription());
                    
                }
                
                if(adBranchBankAccount.getBbaGlCoaInterestAccount() != null){
                	glChartOfAccount = glChartOfAccountHome.findByPrimaryKey(adBranchBankAccount.getBbaGlCoaInterestAccount());
                    details.setBbaInterestAccountNumber(glChartOfAccount.getCoaAccountNumber());
                    details.setBbaInterestAccountDescription(glChartOfAccount.getCoaAccountDescription());
                    
                }
                
                if(adBranchBankAccount.getBbaGlCoaAdjustmentAccount() != null){
                	glChartOfAccount = glChartOfAccountHome.findByPrimaryKey(adBranchBankAccount.getBbaGlCoaAdjustmentAccount());
                    details.setBbaAdjustmentAccountNumber(glChartOfAccount.getCoaAccountNumber());
                    details.setBbaAdjustmentAccountDescription(glChartOfAccount.getCoaAccountDescription());
                    
                }
                
                if(adBranchBankAccount.getBbaGlCoaSalesDiscountAccount() != null){
                	glChartOfAccount = glChartOfAccountHome.findByPrimaryKey(adBranchBankAccount.getBbaGlCoaSalesDiscountAccount());
                    details.setBbaSalesDiscountAccountNumber(glChartOfAccount.getCoaAccountNumber());
                    details.setBbaSalesDiscountAccountDescription(glChartOfAccount.getCoaAccountDescription());
                    
                }
                
                
                if(adBranchBankAccount.getBbaGlCoaAdvanceAccount() != null){
                	
                	glChartOfAccount = glChartOfAccountHome.findByPrimaryKey(adBranchBankAccount.getBbaGlCoaAdvanceAccount());
                    details.setBbaAdvanceAccountNumber(glChartOfAccount.getCoaAccountNumber());
                    details.setBbaAdvanceAccountDescription(glChartOfAccount.getCoaAccountDescription());
                	
                }
                
                
	        	list.add(details);
	           
                
            }
            
        } catch (FinderException ex) {
            
        } catch (Exception ex) {
            
            throw new EJBException(ex.getMessage());
        }
        
        return list;
        
       /* ArrayList branchAndGlAccnts = new ArrayList();
        branchAndGlAccnts.add(branchList);
        branchAndGlAccnts.add(glCoaAccntList);
        
        return branchAndGlAccnts;*/
        
    }
    
    
    
    
    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/   
    public ArrayList getAdBrDsaAll(Integer DSA_CODE, Integer AD_CMPNY) 
    	   throws GlobalNoRecordFoundException{
        
        Debug.print("AdDocumentSequenceAssignmentControllerBean getAdBrDsaAll");
        
        LocalAdBranchDocumentSequenceAssignmentHome adBranchDsaHome  = null;
        LocalAdBranchHome adBranchHome = null;
        
        LocalAdBranchDocumentSequenceAssignment adBranchDsa = null;
        LocalAdBranch adBranch = null;
        
        Collection adBranchDsas = null;
        
        ArrayList list = new ArrayList();
        
        // Initialize EJB Home
        
        try {
            
            adBranchDsaHome = (LocalAdBranchDocumentSequenceAssignmentHome)EJBHomeFactory.
            		lookUpLocalHome(LocalAdBranchDocumentSequenceAssignmentHome.JNDI_NAME, LocalAdBranchDocumentSequenceAssignmentHome.class);
            adBranchHome = (LocalAdBranchHome)EJBHomeFactory.
            		lookUpLocalHome(LocalAdBranchHome.JNDI_NAME, LocalAdBranchHome.class);
            
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {
            
            adBranchDsas = adBranchDsaHome.findBdsByDsaCode(DSA_CODE, AD_CMPNY);
            
        } catch (FinderException ex) {
            
        } catch (Exception ex) {
            
            throw new EJBException(ex.getMessage());
        }
        
        if (adBranchDsas.isEmpty()) {
            
            throw new GlobalNoRecordFoundException();
            
        }
        
        try {
            
            Iterator i = adBranchDsas.iterator();
            
            while(i.hasNext()) {
                
                adBranchDsa = (LocalAdBranchDocumentSequenceAssignment)i.next();
                
                adBranch = adBranchHome.findByPrimaryKey(adBranchDsa.getAdBranch().getBrCode());
                
                AdModBranchDocumentSequenceAssignmentDetails details = new AdModBranchDocumentSequenceAssignmentDetails();
                
                details.setBrCode(adBranch.getBrCode());
                details.setBdsNextSequence(adBranchDsa.getBdsNextSequence());               
                
                list.add(details);
                
            }
            
        } catch (FinderException ex) {
            
        } catch (Exception ex) {
            
            throw new EJBException(ex.getMessage());
        }
        
        return list;
        
    }
    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public AdResponsibilityDetails getAdRsByRsCode(Integer RS_CODE) 
    	throws GlobalNoRecordFoundException {
        
        Debug.print("AdBankAccountControllerBean getAdRsByRsCode");
        
        LocalAdResponsibilityHome adResHome = null;
        LocalAdResponsibility adRes = null;
        
        try {
            adResHome = (LocalAdResponsibilityHome)EJBHomeFactory.
            lookUpLocalHome(LocalAdResponsibilityHome.JNDI_NAME, LocalAdResponsibilityHome.class);
        } catch (NamingException ex) {
            
        }
        
        try {
            adRes = adResHome.findByPrimaryKey(RS_CODE);    		
        } catch (FinderException ex) {
            
        }
        
        AdResponsibilityDetails details = new AdResponsibilityDetails();
        details.setRsName(adRes.getRsName());
        
        return details;
    }
    
    // SessionBean methods

    /**
     * @ejb:create-method view-type="remote"
     **/
    public void ejbCreate() throws CreateException {

       Debug.print("AdBankAccountControllerBean ejbCreate");
      
    }
}
