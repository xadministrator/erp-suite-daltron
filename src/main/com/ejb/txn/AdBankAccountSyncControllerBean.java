
/*
 * AdBankAccountSyncControllerBean.java
 *
 * Created Sometime in December 2007
 *
 * @author  BONBON
 */

package com.ejb.txn;

import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.SessionBean;
import javax.ejb.SessionContext;
import javax.naming.NamingException;

import com.ejb.gl.LocalGlChartOfAccountHome;
import com.ejb.gl.LocalGlChartOfAccount;
import com.ejb.ad.LocalAdBranch;
import com.ejb.ad.LocalAdBranchBankAccount;
import com.ejb.ad.LocalAdBranchBankAccountHome;
import com.ejb.ad.LocalAdBranchHome;
import com.ejb.ad.LocalAdBankAccount;
import com.ejb.ad.LocalAdBankAccountHome;
import com.util.Debug;
import com.util.EJBCommon;
import com.util.EJBHomeFactory;

/**
 * @ejb:bean name="AdBankAccountSyncControllerBean"
 *           type="Stateless"
 *           view-type="service-endpoint"
 *
 * @wsee:port-component name="AdBankAccountSync"
 *
 * @jboss:port-component uri="omega-ejb/AdBankAccountSyncWS"
 *
 * @ejb:interface service-endpoint-class="com.ejb.txn.AdBankAccountSyncWS"
 *
*/

public class AdBankAccountSyncControllerBean implements SessionBean {

	private SessionContext ctx;

	/**
     * @ejb:interface-method
     **/
    public int getAdBankAccountAllNewLength(String BR_BRNCH_CODE, Integer AD_CMPNY) {

    	Debug.print("AdBankAccountSyncControllerBean getAdBankAccountAllNewLength");

    	LocalAdBankAccountHome adBankAccountHome = null;
    	LocalAdBranchHome adBranchHome = null;

    	// Initialize EJB Home

        try {

        	adBankAccountHome = (LocalAdBankAccountHome)EJBHomeFactory.
            	lookUpLocalHome(LocalAdBankAccountHome.JNDI_NAME, LocalAdBankAccountHome.class);
        	adBranchHome = (LocalAdBranchHome)EJBHomeFactory.
        		lookUpLocalHome(LocalAdBranchHome.JNDI_NAME, LocalAdBranchHome.class);

        } catch (NamingException ex) {

            throw new EJBException(ex.getMessage());

        }

        try {

        	System.out.println("test : " + BR_BRNCH_CODE  + "  "  + AD_CMPNY);
        	LocalAdBranch adBranch = adBranchHome.findByBrBranchCode(BR_BRNCH_CODE, AD_CMPNY);

        	Collection adBankAccounts = adBankAccountHome.findBaByBaNewAndUpdated(adBranch.getBrCode(), AD_CMPNY, 'N', 'N', 'N');

        	return adBankAccounts.size();

        } catch (Exception ex) {

        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());

        }
    }

    /**
     * @ejb:interface-method
     **/
    public int getAdBankAccountAllUpdatedLength(String BR_BRNCH_CODE, Integer AD_CMPNY) {

    	Debug.print("AdBankAccountSyncControllerBean getAdBankAccountAllUpdatedLength");

    	LocalAdBankAccountHome adBankAccountHome = null;
    	LocalAdBranchHome adBranchHome = null;

    	// Initialize EJB Home

        try {

        	adBankAccountHome = (LocalAdBankAccountHome)EJBHomeFactory.
            	lookUpLocalHome(LocalAdBankAccountHome.JNDI_NAME, LocalAdBankAccountHome.class);
        	adBranchHome = (LocalAdBranchHome)EJBHomeFactory.
        		lookUpLocalHome(LocalAdBranchHome.JNDI_NAME, LocalAdBranchHome.class);

        } catch (NamingException ex) {

            throw new EJBException(ex.getMessage());

        }

        try {

        	LocalAdBranch adBranch = adBranchHome.findByBrBranchCode(BR_BRNCH_CODE, AD_CMPNY);

        	Collection adBankAccounts = adBankAccountHome.findBaByBaNewAndUpdated(adBranch.getBrCode(), AD_CMPNY, 'U','U','X');

        	return adBankAccounts.size();

        } catch (Exception ex) {

        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());

        }
    }

	/**
     * @ejb:interface-method
     **/
    public String[] getAdBankAccountAllNewAndUpdated(String BR_BRNCH_CODE, Integer AD_CMPNY) {

    	Debug.print("AdBankAccountSyncControllerBean getAdBankAccountAllNewAndUpdated");

    	LocalAdBankAccountHome adBankAccountHome = null;
    	LocalAdBranchBankAccount adBranchBankAccount = null;
    	LocalAdBranchHome adBranchHome = null;

    	// Initialize EJB Home

        try {

        	adBankAccountHome = (LocalAdBankAccountHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdBankAccountHome.JNDI_NAME, LocalAdBankAccountHome.class);
        	adBranchHome = (LocalAdBranchHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdBranchHome.JNDI_NAME, LocalAdBranchHome.class);

        } catch (NamingException ex) {

            throw new EJBException(ex.getMessage());

        }

        try {
        	LocalAdBranch adBranch = adBranchHome.findByBrBranchCode(BR_BRNCH_CODE, AD_CMPNY);
        	Collection adBankAccounts = adBankAccountHome.findBaByBaNewAndUpdated(adBranch.getBrCode(), AD_CMPNY, 'N', 'N', 'N');
        	Collection adUpdatedBankAccounts = adBankAccountHome.findBaByBaNewAndUpdated(adBranch.getBrCode(), AD_CMPNY, 'U', 'U', 'X');

        	String[] results = new String[adBankAccounts.size() + adUpdatedBankAccounts.size()];

        	Iterator i = adBankAccounts.iterator();
        	int ctr = 0;
	        while (i.hasNext()) {

	        	LocalAdBankAccount adBankAccount = (LocalAdBankAccount)i.next();
	        	results[ctr] = bankAccountRowEncode(adBankAccount);
	        	ctr++;
	        }

	        i = adUpdatedBankAccounts.iterator();
	        while (i.hasNext()) {

	        	LocalAdBankAccount adBankAccount = (LocalAdBankAccount)i.next();
	        	results[ctr] = bankAccountRowEncode(adBankAccount);
	        	ctr++;

	        }
	        return results;

        } catch (Exception ex) {

        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());

        }
    }

    /**
     * @ejb:interface-method
     **/
    public int setAdBankAccountsAllNewAndUpdatedSuccessConfirmation(String BR_BRNCH_CODE, Integer AD_CMPNY) {

    	Debug.print("AdBankAccountSyncControllerBean setAdBankAccountAllNewAndUpdatedSuccessConfirmation");

    	LocalAdBranchBankAccountHome adBranchBankAccountHome = null;
    	LocalAdBranchBankAccount adBranchBankAccount = null;
    	LocalAdBranchHome adBranchHome = null;

    	// Initialize EJB Home

        try {

        	adBranchBankAccountHome = (LocalAdBranchBankAccountHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdBranchBankAccountHome.JNDI_NAME, LocalAdBranchBankAccountHome.class);
        	adBranchHome = (LocalAdBranchHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdBranchHome.JNDI_NAME, LocalAdBranchHome.class);


        } catch (NamingException ex) {

            throw new EJBException(ex.getMessage());

        }

        try {
        	LocalAdBranch adBranch = adBranchHome.findByBrBranchCode(BR_BRNCH_CODE, AD_CMPNY);

        	Collection adBranchBankAccounts = adBranchBankAccountHome.findBBaByBaNewAndUpdated(adBranch.getBrCode(), AD_CMPNY, 'N', 'U', 'X');

        	Iterator i = adBranchBankAccounts.iterator();
	        while (i.hasNext()) {

	        	adBranchBankAccount = (LocalAdBranchBankAccount)i.next();
	        	adBranchBankAccount.setBbaDownloadStatus('D');

	        }

        } catch (Exception ex) {

        	ctx.setRollbackOnly();
        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());

        }

        return 0;

    }

    private String bankAccountRowEncode(LocalAdBankAccount adBankAccount) {

    	char separator = EJBCommon.SEPARATOR;
    	StringBuffer tempResult = new StringBuffer();
    	String encodedResult = new String();

    	// Start separator
    	tempResult.append(separator);

    	// Primary Key
    	tempResult.append(adBankAccount.getBaCode());
    	tempResult.append(separator);

    	// Name
    	tempResult.append(adBankAccount.getBaName());
    	tempResult.append(separator);

    	// Description
    	tempResult.append(adBankAccount.getBaDescription());
    	tempResult.append(separator);

    	// GL CASH ACCOUNT
    	try{
    		LocalGlChartOfAccountHome glChartOfAccountHome = (LocalGlChartOfAccountHome)EJBHomeFactory.
    		lookUpLocalHome(LocalGlChartOfAccountHome.JNDI_NAME, LocalGlChartOfAccountHome.class);
    		try {
        	LocalGlChartOfAccount glChartOfAccount =   glChartOfAccountHome.findByCoaCode(adBankAccount.getBaCoaGlCashAccount(), adBankAccount.getBaAdCompany());
        	tempResult.append(glChartOfAccount.getCoaAccountNumber());
        	tempResult.append(separator);
    		} catch (Exception ex) {
    			tempResult.append("Error");
            	tempResult.append(separator);
            	Debug.printStackTrace(ex);
            	throw new EJBException(ex.getMessage());

            }
    	} catch (NamingException ex) {
    		tempResult.append("Error");
        	tempResult.append(separator);
            throw new EJBException(ex.getMessage());
        }



    	// remove unwanted chars from encodedResult;
    	encodedResult = tempResult.toString();
    	encodedResult = encodedResult.replace("\"", " ");
    	encodedResult = encodedResult.replace("'", " ");
    	encodedResult = encodedResult.replace(";", " ");
    	encodedResult = encodedResult.replace("\\", " ");
    	encodedResult = encodedResult.replace("|", " ");

    	return encodedResult;

    }

    public void ejbCreate() throws CreateException {

       Debug.print("ApBankAccountEntryControllerBean ejbCreate");

    }

    public void ejbRemove() {};

    public void ejbActivate() {}
    public void ejbPassivate() {}

    public void setSessionContext(SessionContext ctx) {}

}