
/*
 * AdBankControllerBean.java
 *
 * Created on May 13, 2003, 9:13 AM
 *
 * @author  Neil Andrew M. Ajero
 */

package com.ejb.txn;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;
import javax.naming.NamingException;

import com.ejb.ad.LocalAdBank;
import com.ejb.ad.LocalAdBankHome;
import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.exception.GlobalRecordAlreadyAssignedException;
import com.ejb.exception.GlobalRecordAlreadyDeletedException;
import com.ejb.exception.GlobalRecordAlreadyExistException;
import com.util.AbstractSessionBean;
import com.util.AdBankDetails;
import com.util.Debug;
import com.util.EJBHomeFactory;

/**
 * @ejb:bean name="AdBankControllerEJB"
 *           display-name="Used for entering banks"
 *           type="Stateless"
 *           view-type="remote"
 *           jndi-name="ejb/AdBankControllerEJB"
 *
 * @ejb:interface remote-class="com.ejb.txn.AdBankController"
 *                extends="javax.ejb.EJBObject"
 *
 * @ejb:home remote-class="com.ejb.txn.AdBankControllerHome"
 *           extends="javax.ejb.EJBHome"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="aruser"
 *                        role-link="aruserlink"
 *
 * @ejb:permission role-name="aruser"
 * 
*/

public class AdBankControllerBean extends AbstractSessionBean {
    
    
    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList getAdBnkAll(Integer AD_CMPNY) 
        throws GlobalNoRecordFoundException {
                    
        Debug.print("AdBankControllerBean getAdBnkAll");
        
        LocalAdBankHome adBankHome = null;
        
        Collection adBanks = null;
        
        LocalAdBank adBank = null;
        
        ArrayList list = new ArrayList();
        
        // Initialize EJB Home
        
        try {
            
            adBankHome = (LocalAdBankHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdBankHome.JNDI_NAME, LocalAdBankHome.class);
            
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {
            
            adBanks = adBankHome.findBnkAll(AD_CMPNY);
            
        } catch (FinderException ex) {
            
        } catch (Exception ex) {
            
            throw new EJBException(ex.getMessage());
        }
        
        if (adBanks.isEmpty()) {
        	
            throw new GlobalNoRecordFoundException();
        	
        }
        
        Iterator i = adBanks.iterator();
               
        while (i.hasNext()) {
        	
        	adBank = (LocalAdBank)i.next();
        
                
        	AdBankDetails details = new AdBankDetails();
	    		details.setBnkCode(adBank.getBnkCode());        		
	            details.setBnkName(adBank.getBnkName());
	            details.setBnkBranch(adBank.getBnkBranch());
	            details.setBnkNumber(adBank.getBnkNumber());
	            details.setBnkInstitution(adBank.getBnkInstitution());
	            details.setBnkDescription(adBank.getBnkDescription());
	            details.setBnkAddress(adBank.getBnkAddress());
	            details.setBnkEnable(adBank.getBnkEnable());
                                                        	
        	list.add(details);
        }
        
        return list;
            
    }
    
    /**
     * @ejb:interface-method view-type="remote"
     **/    
    public void addAdBnkEntry(com.util.AdBankDetails details, Integer AD_CMPNY) 
        throws GlobalRecordAlreadyExistException {
                    
        Debug.print("AdBankControllerBean addAdBnkEntry");
        
        LocalAdBankHome adBankHome = null;

        LocalAdBank adBank = null;
                
        ArrayList list = new ArrayList();
        
        // Initialize EJB Home
        
        try {
            
            adBankHome = (LocalAdBankHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdBankHome.JNDI_NAME, LocalAdBankHome.class);
                          
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try { 
            
           adBank = adBankHome.findByBnkName(details.getBnkName(), AD_CMPNY);
            
           throw new GlobalRecordAlreadyExistException();
                                             
        } catch (GlobalRecordAlreadyExistException ex) {
        	
           throw ex;
        	
        } catch (FinderException ex) {
        	 	
        } catch (Exception ex) {
            
           throw new EJBException(ex.getMessage());
            
        }
               
        try {
        	
        	// create new bank
        	
        	adBank = adBankHome.create(details.getBnkName(), details.getBnkBranch(),
        	   details.getBnkNumber(), details.getBnkInstitution(), details.getBnkDescription(),
        	   details.getBnkAddress(), details.getBnkEnable(), AD_CMPNY);   	        
                	       
        } catch (Exception ex) {
        	
        	getSessionContext().setRollbackOnly();
            throw new EJBException(ex.getMessage());
        	
        }    
        
    }
    
    
    
    /**
     * @ejb:interface-method view-type="remote"
     **/
    public void updateAdBnkEntry(com.util.AdBankDetails details, Integer AD_CMPNY) 
        throws GlobalRecordAlreadyExistException {
                    
        Debug.print("AdBankControllerBean updateAdBnkEntry");
        
        LocalAdBankHome adBankHome = null;

        LocalAdBank adBank = null;
                
        ArrayList list = new ArrayList();
        
        // Initialize EJB Home
        
        try {
            
            adBankHome = (LocalAdBankHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdBankHome.JNDI_NAME, LocalAdBankHome.class);
                          
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try { 
            
            LocalAdBank adExistingBank = adBankHome.findByBnkName(details.getBnkName(), AD_CMPNY);
            
            if (!adExistingBank.getBnkCode().equals(details.getBnkCode())) {
            
                 throw new GlobalRecordAlreadyExistException();
                 
            }
                                             
        } catch (GlobalRecordAlreadyExistException ex) {
        	
            throw ex;
        	
        } catch (FinderException ex) {
        	 	
        } catch (Exception ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
               
        try {
        	        	
        	// find and update bank
        	
        	adBank = adBankHome.findByPrimaryKey(details.getBnkCode());
        		
                adBank.setBnkName(details.getBnkName());
                adBank.setBnkBranch(details.getBnkBranch());
                adBank.setBnkNumber(details.getBnkNumber());
                adBank.setBnkInstitution(details.getBnkInstitution());
                adBank.setBnkDescription(details.getBnkDescription());
                adBank.setBnkAddress(details.getBnkAddress());
                adBank.setBnkEnable(details.getBnkEnable());
      	        	                            	
        } catch (Exception ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
    }
    

    /**
     * @ejb:interface-method view-type="remote"
     **/
    public void deleteAdBankEntry(Integer BNK_CODE, Integer AD_CMPNY) 
        throws GlobalRecordAlreadyAssignedException,
                 GlobalRecordAlreadyDeletedException {
                    
        Debug.print("AdBankControllerBean deleteAdBankEntry");
        
        LocalAdBankHome adBankHome = null; 
        
        Collection adBankAccounts = null; 
        
        LocalAdBank adBank = null;           
               
        // Initialize EJB Home
        
        try {
            
            adBankHome = (LocalAdBankHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdBankHome.JNDI_NAME, LocalAdBankHome.class);                      
            
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }

	    try {
         
            adBank = adBankHome.findByPrimaryKey(BNK_CODE);

	    } catch (FinderException ex) {
	    	
	         throw new GlobalRecordAlreadyDeletedException();
	    
	    }
	    
	    try {
        	       	  	       	
    	    adBankAccounts = adBank.getAdBankAccounts();
    	
    	    if (!adBankAccounts.isEmpty()) {
    		
    		    throw new GlobalRecordAlreadyAssignedException();
    		
    	    }
    	    
    	    adBank.remove();        	

        		                 	
        } catch (GlobalRecordAlreadyAssignedException ex) {
        	
        	 throw new GlobalRecordAlreadyAssignedException();
        	
        } catch (Exception ex) {
        	
        	 throw new EJBException(ex.getMessage());
        	
        }
        
	}
    
        
    // SessionBean methods

    /**
     * @ejb:create-method view-type="remote"
     **/
    public void ejbCreate() throws CreateException {

       Debug.print("AdBankControllerBean ejbCreate");
      
    }
}
