
/*
 * AdBranchControllerBean.java
 *
 * Created on October 24, 2005, 07:10 PM
 * @author  Aliza D.J. Anos
 */

package com.ejb.txn;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;
import javax.ejb.RemoveException;
import javax.naming.NamingException;

import com.ejb.ad.LocalAdBranch;
import com.ejb.ad.LocalAdBranchHome;
import com.ejb.ad.LocalAdCompany;
import com.ejb.ad.LocalAdCompanyHome;
import com.ejb.exception.AdBRBranchCodeAlreadyExistsException;
import com.ejb.exception.AdBRBranchNameAlreadyExistsException;
import com.ejb.exception.AdBRHeadQuarterAlreadyExistsException;
import com.ejb.exception.GlobalAccountNumberInvalidException;
import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.exception.GlobalRecordAlreadyAssignedException;
import com.ejb.exception.GlobalRecordAlreadyDeletedException;
import com.ejb.gl.LocalGlChartOfAccount;
import com.ejb.gl.LocalGlChartOfAccountHome;
import com.util.AbstractSessionBean;
import com.util.AdModBranchDetails;
import com.util.Debug;
import com.util.EJBCommon;
import com.util.EJBHomeFactory;

/**
 * @ejb:bean name="AdBranchControllerEJB"
 *           display-name="Used for entering branches"
 *           type="Stateless"
 *           view-type="remote"
 *           jndi-name="ejb/AdBranchControllerEJB"
 *
 * @ejb:interface remote-class="com.ejb.txn.AdBranchController"
 *                extends="javax.ejb.EJBObject"
 *
 * @ejb:home remote-class="com.ejb.txn.AdBranchControllerHome"
 *           extends="javax.ejb.EJBHome"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="aruser"
 *                        role-link="aruserlink"
 *
 * @ejb:permission role-name="aruser"
 * 
*/

public class AdBranchControllerBean extends AbstractSessionBean {

	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/

	public ArrayList getAdBrAll(Integer AD_CMPNY) 
	throws GlobalNoRecordFoundException {

		Debug.print("AdBranchControllerBean getAdBrAll");

		LocalAdBranchHome adBranchHome = null;
		LocalAdBranch adBranch = null;

		Collection adBranches = null;

		ArrayList list = new ArrayList();

		// Initialize EJB Home

		try {

			adBranchHome = (LocalAdBranchHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdBranchHome.JNDI_NAME, LocalAdBranchHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}

		try {

			adBranches = adBranchHome.findBrAll(AD_CMPNY);

		} catch (FinderException ex) {

		} catch (Exception ex) {

			throw new EJBException(ex.getMessage());
		}

		if (adBranches.isEmpty()) {

			throw new GlobalNoRecordFoundException();

		}

		Iterator i = adBranches.iterator();

		while(i.hasNext()) {

			adBranch = (LocalAdBranch)i.next();

			AdModBranchDetails mdetails = new AdModBranchDetails();
			mdetails.setBrCode(adBranch.getBrCode()); 
			mdetails.setBrBranchCode(adBranch.getBrBranchCode()); 
			mdetails.setBrName(adBranch.getBrName()); 
			mdetails.setBrDescription(adBranch.getBrDescription()); 
			mdetails.setBrType(adBranch.getBrType()); 
			mdetails.setBrHeadQuarter(adBranch.getBrHeadQuarter()); 
			mdetails.setBrAddress(adBranch.getBrAddress());
			mdetails.setBrContactPerson(adBranch.getBrContactPerson());
			mdetails.setBrContactNumber(adBranch.getBrContactNumber());
			mdetails.setBrCoaSegment(adBranch.getBrCoaSegment());
			mdetails.setBrDownloadStatus(adBranch.getBrDownloadStatus());
			mdetails.setBrApplyShipping(adBranch.getBrApplyShipping());
			mdetails.setBrPercentMarkup(adBranch.getBrPercentMarkup());

			if(adBranch.getBrApplyShipping() == EJBCommon.TRUE) {
				mdetails.setBrGlCoaAccountNumber(adBranch.getGlChartOfAccount().getCoaAccountNumber());
				mdetails.setBrGlCoaAccountDescription(adBranch.getGlChartOfAccount().getCoaAccountDescription());
			}

			list.add(mdetails);

		}

		return list;

	}

	/**
	 * @ejb:interface-method view-type="remote"
	 **/    
	public void addAdBrEntry(com.util.AdBranchDetails details, String BR_GL_COA_ACCNT_NMBR, Integer AD_CMPNY) 
	throws AdBRHeadQuarterAlreadyExistsException,
	AdBRBranchCodeAlreadyExistsException,
	AdBRBranchNameAlreadyExistsException,
	GlobalAccountNumberInvalidException {

		Debug.print("AdBranchControllerBean addAdBrEntry");

		LocalAdBranchHome adBranchHome = null;
		LocalAdBranch adBranch = null;
		LocalGlChartOfAccountHome glChartOfAccountHome = null;
		LocalGlChartOfAccount glAccount = null;

		ArrayList list = new ArrayList();

		// Initialize EJB Home

		try {

			adBranchHome = (LocalAdBranchHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdBranchHome.JNDI_NAME, LocalAdBranchHome.class);
			glChartOfAccountHome = (LocalGlChartOfAccountHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlChartOfAccountHome.JNDI_NAME, LocalGlChartOfAccountHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}

		//check if branch code already exists

		try { 

			adBranch = adBranchHome.findByBrBranchCode(details.getBrBranchCode(), AD_CMPNY);

			throw new AdBRBranchCodeAlreadyExistsException();

		} catch (AdBRBranchCodeAlreadyExistsException ex) {

			throw ex;

		} catch (FinderException ex) {

		} catch (Exception ex) {

			throw new EJBException(ex.getMessage());

		}

		//check if branch name already exists

		try { 

			adBranch = adBranchHome.findByBrName(details.getBrName(), AD_CMPNY);

			throw new AdBRBranchNameAlreadyExistsException();

		} catch (AdBRBranchNameAlreadyExistsException ex) {

			throw ex;

		} catch (FinderException ex) {

		} catch (Exception ex) {

			throw new EJBException(ex.getMessage());

		}

		if(details.getBrHeadQuarter() == EJBCommon.TRUE) {

			try {

				adBranch = adBranchHome.findByBrHeadQuarter(AD_CMPNY);

				throw new AdBRHeadQuarterAlreadyExistsException();

			} catch (AdBRHeadQuarterAlreadyExistsException ex) {

				throw ex;

			} catch (FinderException ex) {

			} catch (Exception ex) {

				throw new EJBException(ex.getMessage());

			}

		}

		try {

			try {

				if (BR_GL_COA_ACCNT_NMBR != null && BR_GL_COA_ACCNT_NMBR.length() > 0) {

					glAccount = glChartOfAccountHome.findByCoaAccountNumber(BR_GL_COA_ACCNT_NMBR, AD_CMPNY);

				}

			} catch (FinderException ex) {

				throw new GlobalAccountNumberInvalidException();

			}

			// create new branch

			adBranch = adBranchHome.create(details.getBrBranchCode(), details.getBrName(), 
					details.getBrDescription(), details.getBrType(), details.getBrHeadQuarter(), 
					details.getBrAddress(), details.getBrContactPerson(), details.getBrContactNumber(), 'N', 
					details.getBrApplyShipping(), details.getBrPercentMarkup(), AD_CMPNY); 

			if (glAccount != null) {

				glAccount.addAdBranch(adBranch);

			}

		} catch (GlobalAccountNumberInvalidException ex) {

			getSessionContext().setRollbackOnly();
			throw ex;        	

		} catch (Exception ex) {

			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());

		}    

	}

	/**
	 * @ejb:interface-method view-type="remote"
	 **/
	public void updateAdBrEntry(com.util.AdBranchDetails details, String BR_GL_COA_ACCNT_NMBR, Integer AD_CMPNY) 
	throws AdBRHeadQuarterAlreadyExistsException,
	AdBRBranchCodeAlreadyExistsException, 
	AdBRBranchNameAlreadyExistsException,
	GlobalAccountNumberInvalidException { 

		Debug.print("AdBranchControllerBean updateAdBrEntry");

		LocalAdBranchHome adBranchHome = null;
		LocalAdBranch adBranch = null;
		LocalGlChartOfAccountHome glChartOfAccountHome = null;
		LocalGlChartOfAccount glAccount = null;

		ArrayList list = new ArrayList();

		// Initialize EJB Home

		try {

			adBranchHome = (LocalAdBranchHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdBranchHome.JNDI_NAME, LocalAdBranchHome.class);
			glChartOfAccountHome = (LocalGlChartOfAccountHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlChartOfAccountHome.JNDI_NAME, LocalGlChartOfAccountHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}

		try { 

			LocalAdBranch arExistingBranch = adBranchHome.findByBrBranchCode(details.getBrBranchCode(), AD_CMPNY);

			if (!arExistingBranch.getBrCode().equals(details.getBrCode())) {

				throw new AdBRBranchCodeAlreadyExistsException();

			}

		} catch (AdBRBranchCodeAlreadyExistsException ex) {

			throw ex;

		} catch (FinderException ex) {

		} catch (Exception ex) {

			throw new EJBException(ex.getMessage());

		}

		try { 

			LocalAdBranch arExistingBranch = adBranchHome.findByBrName(details.getBrName(), AD_CMPNY);

			if (!arExistingBranch.getBrCode().equals(details.getBrCode())) {

				throw new AdBRBranchNameAlreadyExistsException();

			}

		} catch (AdBRBranchNameAlreadyExistsException ex) {

			throw ex;

		} catch (FinderException ex) {

		} catch (Exception ex) {

			throw new EJBException(ex.getMessage());

		}

		if(details.getBrHeadQuarter() == EJBCommon.TRUE) {

			try {

				LocalAdBranch adBranchHeadQuarter = adBranchHome.findByBrHeadQuarter(AD_CMPNY);

				if (!adBranchHeadQuarter.getBrCode().equals(details.getBrCode())) {

					throw new AdBRHeadQuarterAlreadyExistsException();

				}

			} catch (AdBRHeadQuarterAlreadyExistsException ex) {

				throw ex;

			} catch (FinderException ex) {

			} catch (Exception ex) {

				throw new EJBException(ex.getMessage());

			}


		}

		try {

			try {

				if (BR_GL_COA_ACCNT_NMBR != null && BR_GL_COA_ACCNT_NMBR.length() > 0) {

					glAccount = glChartOfAccountHome.findByCoaAccountNumber(BR_GL_COA_ACCNT_NMBR, AD_CMPNY);

				}

			} catch (FinderException ex) {

				throw new GlobalAccountNumberInvalidException();

			}

			// find and update branch

			adBranch = adBranchHome.findByPrimaryKey(details.getBrCode());

			adBranch.setBrBranchCode(details.getBrBranchCode());
			adBranch.setBrName(details.getBrName());
			adBranch.setBrDescription(details.getBrDescription());
			adBranch.setBrType(details.getBrType());
			adBranch.setBrHeadQuarter(details.getBrHeadQuarter());
			adBranch.setBrAddress(details.getBrAddress());
			adBranch.setBrContactPerson(details.getBrContactPerson());
			adBranch.setBrContactNumber(details.getBrContactNumber());

			if (adBranch.getBrDownloadStatus()=='N'){

				adBranch.setBrDownloadStatus('U');

			}else if (adBranch.getBrDownloadStatus()=='D'){

				adBranch.setBrDownloadStatus('X');

			}

			adBranch.setBrApplyShipping(details.getBrApplyShipping());
			adBranch.setBrPercentMarkup(details.getBrPercentMarkup());

			if (glAccount != null) {

				glAccount.addAdBranch(adBranch);

			} else if (glAccount == null && adBranch.getGlChartOfAccount() != null){

				adBranch.getGlChartOfAccount().dropAdBranch(adBranch);

			}

		} catch (GlobalAccountNumberInvalidException ex) {

			getSessionContext().setRollbackOnly();
			ex.printStackTrace();
			throw ex;        	

		} catch (Exception ex) {

			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());

		}

	}

	/**
	 * @ejb:interface-method view-type="remote"
	 **/
	public void deleteAdBrEntry(Integer BR_CODE, Integer AD_CMPNY) 
	throws GlobalRecordAlreadyDeletedException,
	GlobalRecordAlreadyAssignedException {

		Debug.print("AdBranchControllerBean deleteAdBrEntry");

		LocalAdBranch adBranch = null;
		LocalAdBranchHome adBranchHome = null;

		// Initialize EJB Home

		try {

			adBranchHome = (LocalAdBranchHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdBranchHome.JNDI_NAME, LocalAdBranchHome.class);           

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}                

		try {

			adBranch = adBranchHome.findByPrimaryKey(BR_CODE);

		} catch (FinderException ex) {

			throw new GlobalRecordAlreadyDeletedException();

		} catch (Exception ex) {

			throw new EJBException(ex.getMessage()); 

		}

		try {

			if (!adBranch.getAdBranchResponsibilities().isEmpty() || !adBranch.getAdBranchBankAccounts().isEmpty() ||
					!adBranch.getAdBranchCoas().isEmpty() || !adBranch.getAdBranchCustomer().isEmpty() ||
					!adBranch.getAdBranchDocumentSequenceAssignments().isEmpty() || !adBranch.getAdBranchItemLocation().isEmpty() ||
					!adBranch.getAdBranchOverheadMemoLines().isEmpty() || !adBranch.getAdBranchStandardMemoLine().isEmpty() ||
					!adBranch.getAdBranchSupplier().isEmpty() || !adBranch.getInvBranchStockTransfers().isEmpty()) {

				throw new GlobalRecordAlreadyAssignedException();

			}

		} catch (GlobalRecordAlreadyAssignedException ex) {                        

			throw ex;

		} catch (Exception ex) {

			throw new EJBException(ex.getMessage());

		}

		try {

			adBranch = adBranchHome.findByPrimaryKey(BR_CODE);

			if (adBranch.getBrDownloadStatus()=='D' || adBranch.getBrDownloadStatus()=='X'){
				throw new GlobalRecordAlreadyAssignedException();
			}

		} catch (GlobalRecordAlreadyAssignedException ex){
			throw ex;
		} catch (FinderException ex){

		}

		try {

			adBranch.remove();

		} catch (RemoveException ex) {

			getSessionContext().setRollbackOnly();

			throw new EJBException(ex.getMessage());

		} catch (Exception ex) {

			getSessionContext().setRollbackOnly();

			throw new EJBException(ex.getMessage());

		}	      

	}   

	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public short getGlFcPrecisionUnit(Integer AD_CMPNY) {

		Debug.print("AdBranchControllerBean getGlFcPrecisionUnit");

		LocalAdCompanyHome adCompanyHome = null;


		// Initialize EJB Home

		try {

			adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);       

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}

		try {

			LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);

			return  adCompany.getGlFunctionalCurrency().getFcPrecision();

		} catch (Exception ex) {

			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());

		}

	}

	// SessionBean methods

	/**
	 * @ejb:create-method view-type="remote"
	 **/
	public void ejbCreate() throws CreateException {

		Debug.print("AdBranchControllerBean ejbCreate");

	}
}
