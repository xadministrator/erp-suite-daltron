
/*
 * AdBranchResponsibilityControllerBean.java
 *
 * Created on October 21, 2005, 11:26 AM
 *
 * @author  Franco Antonio R. Roig
 */

package com.ejb.txn;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;
import javax.naming.NamingException;

import com.ejb.ad.LocalAdBranchResponsibility;
import com.ejb.ad.LocalAdBranchResponsibilityHome;
import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.exception.GlobalRecordAlreadyAssignedException;
import com.ejb.exception.GlobalRecordAlreadyDeletedException;
import com.ejb.exception.GlobalRecordAlreadyExistException;
import com.util.AbstractSessionBean;
import com.util.AdBranchResponsibilityDetails;
import com.util.Debug;
import com.util.EJBHomeFactory;

/**
 * @ejb:bean name="AdBranchResponsibilityControllerEJB"
 *           display-name="Used for entering branch responsibility"
 *           type="Stateless"
 *           view-type="remote"
 *           jndi-name="ejb/AdBranchResponsibilityControllerEJB"
 *
 * @ejb:interface remote-class="com.ejb.txn.AdBranchResponsibilityController"
 *                extends="javax.ejb.EJBObject"
 *
 * @ejb:home remote-class="com.ejb.txn.AdBranchResponsibilityControllerHome"
 *           extends="javax.ejb.EJBHome"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="aduser"
 *                        role-link="aduserlink"
 *
 * @ejb:permission role-name="aduser"
 * 
*/

public class AdBranchResponsibilityControllerBean extends AbstractSessionBean {
    
    
    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList getAdBrnchRspnsbltyAll(int resCode, Integer AD_CMPNY) 
        throws GlobalNoRecordFoundException {
                    
        Debug.print("AdBranchResponsibilityController getAdBrnchRspnsbltyAll");
        
        LocalAdBranchResponsibilityHome adBrResHome = null;
        LocalAdBranchResponsibility adBrRes = null;

        
        Collection adBranches = null;
        ArrayList list = new ArrayList();
        
        // Initialize EJB Home
        
        try {
            
        	adBrResHome = (LocalAdBranchResponsibilityHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdBranchResponsibilityHome.JNDI_NAME, LocalAdBranchResponsibilityHome.class);
            
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {
            
        	adBranches = adBrResHome.findByAdResponsibility(new Integer(resCode), AD_CMPNY);
            
        } catch (FinderException ex) {
            
        } catch (Exception ex) {
            throw new EJBException(ex.getMessage());
        }
        
        if (adBranches.isEmpty()) {
            throw new GlobalNoRecordFoundException();
        	
        }

    	Iterator i = adBranches.iterator();
    	
    	while ( i.hasNext() ) {
    		adBrRes = (LocalAdBranchResponsibility)i.next();
    		AdBranchResponsibilityDetails details = new AdBranchResponsibilityDetails();
    		    		
    		details.setBrsCode(adBrRes.getAdBranch().getBrCode());
    		details.setBrsAdCompany(adBrRes.getBrsAdCompany());
    		list.add(details);
    	}
        
    	
        return list;
            
    }
    
    /**
     * @ejb:interface-method view-type="remote"
     **/    
    public void addAdBranchResponsibility(AdBranchResponsibilityDetails details, Integer AD_CMPNY) 
        throws GlobalRecordAlreadyExistException {
                    
        Debug.print("AdBranchResponsibilityControllerBean addAdBranchResponsibility");
        
        LocalAdBranchResponsibilityHome adBrResHome = null;
        LocalAdBranchResponsibility adBrRes = null;
                
        ArrayList list = new ArrayList();
        
        // Initialize EJB Home
        
        try {
            
        	adBrResHome = (LocalAdBranchResponsibilityHome)EJBHomeFactory.
            lookUpLocalHome(LocalAdBranchResponsibilityHome.JNDI_NAME, LocalAdBranchResponsibilityHome.class);
                         
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try { 
           
           adBrRes = adBrResHome.findByPrimaryKey(details.getBrsCode());           
            
           throw new GlobalRecordAlreadyExistException();
                                             
        } catch (GlobalRecordAlreadyExistException ex) {
        	
           throw ex;
        	
        } catch (FinderException ex) {
        	 	
        } catch (Exception ex) {
            
           throw new EJBException(ex.getMessage());
            
        }
               
        try {
        	
        	// create new branch responsibility
        	
        	adBrRes = adBrResHome.create(details.getBrsCode(), details.getBrsAdCompany());   	        
                	       
        } catch (Exception ex) {
        	
        	getSessionContext().setRollbackOnly();
            throw new EJBException(ex.getMessage());
        	
        }    
        
    }
    
    
    
    /**
     * @ejb:interface-method view-type="remote"
     **/
    public void updateAdBranchResponsibility(AdBranchResponsibilityDetails details, Integer AD_CMPNY) 
        throws GlobalRecordAlreadyExistException {
                    
        Debug.print("AdBranchResponsibilityControllerBean addAdBranchResponsibility");
        
        LocalAdBranchResponsibilityHome adBrResHome = null;
        LocalAdBranchResponsibility adBrRes = null;
                
        ArrayList list = new ArrayList();
        
        // Initialize EJB Home
        
        try {
            
        	adBrResHome = (LocalAdBranchResponsibilityHome)EJBHomeFactory.
            lookUpLocalHome(LocalAdBranchResponsibilityHome.JNDI_NAME, LocalAdBranchResponsibilityHome.class);
                          
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try { 
            
            LocalAdBranchResponsibility adExistingBrRes = adBrResHome.findByPrimaryKey(details.getBrsCode());
            
            if (!adExistingBrRes.getBrsCode().equals(details.getBrsCode())) {
            
                 throw new GlobalRecordAlreadyExistException();
                 
            }
                                             
        } catch (GlobalRecordAlreadyExistException ex) {
        	
            throw ex;
        	
        } catch (FinderException ex) {
        	 	
        } catch (Exception ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
               
        try {
        	        	
        	// find and update branch responsibility
        	
        	adBrRes = adBrResHome.findByPrimaryKey(details.getBrsCode());
        		
        	adBrRes.setBrsAdCompany(details.getBrsAdCompany());
      	        	                            	
        } catch (Exception ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
    }
    

    /**
     * @ejb:interface-method view-type="remote"
     **/
    public void deleteAdBranchResponsibility(Integer BRS_CODE, Integer AD_CMPNY) 
        throws GlobalRecordAlreadyAssignedException,
                 GlobalRecordAlreadyDeletedException {
                    
        Debug.print("AdBranchResponsibilityControllerBean deleteAdBranchResponsibility");
        
        LocalAdBranchResponsibilityHome adBrResHome = null;
        LocalAdBranchResponsibility adBrRes = null;
               
        // Initialize EJB Home
        
        try {
            
        	adBrResHome = (LocalAdBranchResponsibilityHome)EJBHomeFactory.
            lookUpLocalHome(LocalAdBranchResponsibilityHome.JNDI_NAME, LocalAdBranchResponsibilityHome.class);
            
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }

	    try {
         
            adBrRes = adBrResHome.findByPrimaryKey(BRS_CODE);

	    } catch (FinderException ex) {
	    	
	         throw new GlobalRecordAlreadyDeletedException();
	    
	    }
	    
	    try {
	    	
    	    adBrRes.remove();
        		                 	        	
        } catch (Exception ex) {
        	
        	 throw new EJBException(ex.getMessage());
        	
        }
        
	}
    
        
    // SessionBean methods

    /**
     * @ejb:create-method view-type="remote"
     **/
    public void ejbCreate() throws CreateException {

       Debug.print("AdBranchResponsibilityControllerBean ejbCreate");
      
    }
}
