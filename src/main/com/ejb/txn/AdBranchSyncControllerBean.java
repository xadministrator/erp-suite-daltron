
/*
 * AdBranchSyncControllerBean
 *
 * Created on February 2, 2006, 2:44 PM
 *
 * @author  Franco Antonio R. Roig
 */

package com.ejb.txn;

import java.util.Collection;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.SessionBean;
import javax.ejb.SessionContext;
import javax.naming.NamingException;

import com.ejb.ad.LocalAdBranch;
import com.ejb.ad.LocalAdBranchHome;
import com.util.Debug;
import com.util.EJBCommon;
import com.util.EJBHomeFactory;

/**
 * 
 * @ejb:bean name="AdBranchSyncControllerBean"
 * 			 type="Stateless"
 * 			 view-type="service-endpoint"
 * 
 * @wsee:port-component name ="AdBranchSync"
 * 
 * @jboss:port-component uri="omega-ejb/AdBranchSyncWS"
 * 
 * @ejb:interface service-endpoint-class="com.ejb.txn.AdBranchSyncWS"
 * 
 **/

public class AdBranchSyncControllerBean implements SessionBean {
	
	private SessionContext ctx;
	
	/**
     * @ejb:interface-method
     **/
    public String[] getAdBranchAll(Integer AD_CMPNY) {    	
    
    	Debug.print("AdBranchSyncControllerBean getAdBranchAll");
    	
    	LocalAdBranchHome adBranchHome = null;
    	LocalAdBranch adBranch = null;
    	
    	 try {
            
        	adBranchHome = (LocalAdBranchHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdBranchHome.JNDI_NAME, LocalAdBranchHome.class);
            
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {
        	
        	Collection adBranches = adBranchHome.findBrAll(AD_CMPNY);
        	
        	String[] results = new String[adBranches.size()];
        	
        	Iterator i = adBranches.iterator();
        	int ctr = 0;
	        while (i.hasNext()) {
	        	
	        	adBranch = (LocalAdBranch)i.next();
	        	results[ctr] = branchRowEncode(adBranch);
	        	ctr++;
	        	
	        }

	        return results;
        	
        } catch (Exception ex) {
        	
        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());
        	
        }
    	
    }
    
    /**
     * @ejb:interface-method
     **/
    public String[] getAdBranchAllWithBranchName(Integer AD_CMPNY) {    	
        
    	Debug.print("AdBranchSyncControllerBean getAdBranchAllWithBranchName");
    	
    	LocalAdBranchHome adBranchHome = null;
    	LocalAdBranch adBranch = null;
    	
    	 try {
            
        	adBranchHome = (LocalAdBranchHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdBranchHome.JNDI_NAME, LocalAdBranchHome.class);
            
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {
        	
        	Collection adBranches = adBranchHome.findBrAll(AD_CMPNY);
        	
        	String[] results = new String[adBranches.size()];
        	
        	Iterator i = adBranches.iterator();
        	int ctr = 0;
	        while (i.hasNext()) {
	        	
	        	adBranch = (LocalAdBranch)i.next();
	        	results[ctr] = branchRowEncodeWithBranchName(adBranch);
	        	ctr++;
	        	
	        }

	        return results;
        	
        } catch (Exception ex) {
        	
        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());
        	
        }
    	
    }
    
    
    /**
     * @ejb:interface-method
     **/
    public int setAdBranchAllSuccessConfirmation(Integer AD_CMPNY) {    	
    
    	System.out.println("AdBranchSyncControllerBean setAdBranchAllSuccessConfirmation");    	    
        
    	LocalAdBranchHome adBranchHome = null;
    	LocalAdBranch adBranch = null;
    	
    	try {
            
    		adBranchHome = (LocalAdBranchHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdBranchHome.JNDI_NAME, LocalAdBranchHome.class);
            
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {
        	
        	Collection adBranches = adBranchHome.findBrAll(AD_CMPNY);
        	
        	Iterator i = adBranches.iterator();        	
	        while (i.hasNext()) {
	        	
	        	adBranch = (LocalAdBranch)i.next();
	        	adBranch.setBrDownloadStatus('D');
	        	
	        }
        	
        } catch (Exception ex) {
        	
        	ctx.setRollbackOnly();
        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());
        	
        }
        
        return 0;
    }        
    
    private String branchRowEncode(LocalAdBranch adBranch){
    	
    	char separator = EJBCommon.SEPARATOR;
    	StringBuffer encodedResult = new StringBuffer();

    	// Start separator
    	encodedResult.append(separator);
    	
    	// Primary Key / OPOS: Branch Code
    	encodedResult.append(adBranch.getBrCode());
    	encodedResult.append(separator);
    	
    	// Branch Code / OPOS: Br Branch Code
    	encodedResult.append(adBranch.getBrBranchCode());
    	encodedResult.append(separator);
    	
    	// End separator
    	encodedResult.append(separator);
    
    	return encodedResult.toString();
    }
    
	private String branchRowEncodeWithBranchName(LocalAdBranch adBranch){
	    	
	    	char separator = EJBCommon.SEPARATOR;
	    	StringBuffer encodedResult = new StringBuffer();
	
	    	// Start separator
	    	encodedResult.append(separator);
	    	
	    	// Primary Key / OPOS: Branch Code
	    	encodedResult.append(adBranch.getBrCode());
	    	encodedResult.append(separator);
	    	
	    	// Branch Code / OPOS: Br Branch Code
	    	encodedResult.append(adBranch.getBrBranchCode());
	    	encodedResult.append(separator);
	    	
	    	// Branch Name / OPOS: Br Branch Name
	    	encodedResult.append(adBranch.getBrName());
	    	encodedResult.append(separator);
	    	
	    	// End separator
	    	encodedResult.append(separator);
	    
	    	return encodedResult.toString();
	    }
    
    public void ejbCreate() throws CreateException {

       System.out.println("AdBranchSyncControllerBean ejbCreate");
      
    }
    
    public void ejbRemove() {};

    public void ejbActivate() {}
    public void ejbPassivate() {}

    public void setSessionContext(SessionContext ctx) {}

}