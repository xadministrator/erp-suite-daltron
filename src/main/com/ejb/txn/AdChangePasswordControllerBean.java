
/*
 * AdChangePasswordControllerBean.java
 *
 * Created on June 17, 2003, 9:54 AM
 *
 * @author  Neil Andrew M. Ajero
 *
 */

package com.ejb.txn;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;
import javax.naming.NamingException;
//import java.util.Base64;
import sun.misc.BASE64Encoder;

import com.ejb.ad.LocalAdUser;
import com.ejb.ad.LocalAdUserHome;
import com.ejb.exception.AdUSRPasswordInvalidException;
import com.util.AbstractSessionBean;
import com.util.Debug;
import com.util.EJBHomeFactory;

/**
 * @ejb:bean name="AdChangePasswordControllerEJB"
 *           display-name="Used for entering aging buckets"
 *           type="Stateless"
 *           view-type="remote"
 *           jndi-name="ejb/AdChangePasswordControllerEJB"
 *
 * @ejb:interface remote-class="com.ejb.txn.AdChangePasswordController"
 *                extends="javax.ejb.EJBObject"
 *
 * @ejb:home remote-class="com.ejb.txn.AdChangePasswordControllerHome"
 *           extends="javax.ejb.EJBHome"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="aduser"
 *                        role-link="aduserlink"
 *
 * @ejb:permission role-name="aduser"
 * 
*/

public class AdChangePasswordControllerBean extends AbstractSessionBean {
    
    /**
     * @ejb:interface-method view-type="remote"
     **/
    public void executeChangePassword(String USR_PSSWRD, String USR_NEW_PSSWRD, 
        Integer USR_CODE, Integer AD_CMPNY) 
        throws AdUSRPasswordInvalidException {
                    
        Debug.print("AdChangePasswordControllerBean executeChangePassword");

        LocalAdUserHome adUserHome = null;

        LocalAdUser adUser = null;
        
        // Initialize EJB Home

        try {
            
            adUserHome = (LocalAdUserHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdUserHome.JNDI_NAME, LocalAdUserHome.class);

        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }

        try {
            
            adUser = adUserHome.findByPrimaryKey(USR_CODE);
            
            if(!adUser.getUsrPassword().equals(this.encryptPassword(USR_PSSWRD))) {
            	
            	throw new AdUSRPasswordInvalidException();
            	
            } else {
            	
            	adUser.setUsrPassword(this.encryptPassword(USR_NEW_PSSWRD));
            	
            }
            
        } catch (AdUSRPasswordInvalidException ex) {  
        
            throw ex;  
            
        } catch (FinderException ex) {
            
        } catch (Exception ex) {
            
            throw new EJBException(ex.getMessage());
        }
        
    }
   
    // private method
    
	private String encryptPassword(String password) {
		
		Debug.print("AdChangePasswordControllerBean encryptPassword");
		
		String encPassword = new String();
		
		try {
			
			String keyString = "Some things are better left unread.";
			byte key[] = keyString.getBytes("UTF8");
			
			// encode password to utf-8
			byte utf8[] = password.getBytes("UTF8");
			
			int length = key.length;
			if(utf8.length < key.length)
				length = utf8.length;
			
			// encrypt
			byte data[] = new byte[length];
			for(int i =0; i< length; i++)
			{
			   data[i] = (byte)(utf8[i] ^ key[i]);
			}
			
			
			
			
				//old encoder code
			
			BASE64Encoder encoder = new BASE64Encoder();
			encPassword = encoder.encode(data);
			
			// new encoder code for java 8
		//	encPassword = Base64.getEncoder().encodeToString(data);
			
		} catch (Exception ex) {	
			
			Debug.printStackTrace(ex);
            throw new EJBException(ex.getMessage());
            
		}
		
		return encPassword;
		
	}

    
    // SessionBean methods

    /**
     * @ejb:create-method view-type="remote"
     **/
    public void ejbCreate() throws CreateException {

       Debug.print("AdUserControllerBean ejbCreate");
      
    }
}
