
/*
 * AdCompanyControllerBean.java
 *
 * Created on March 15, 2004, 11:37 AM
 *
 * @author  Neil Andrew M. Ajero
 */

package com.ejb.txn;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.naming.NamingException;

import com.ejb.ad.LocalAdCompany;
import com.ejb.ad.LocalAdCompanyHome;
import com.ejb.genfld.LocalGenField;
import com.ejb.genfld.LocalGenFieldHome;
import com.ejb.gl.LocalGlFunctionalCurrency;
import com.ejb.gl.LocalGlFunctionalCurrencyHome;
import com.util.AbstractSessionBean;
import com.util.AdModCompanyDetails;
import com.util.Debug;
import com.util.EJBHomeFactory;

/**
 * @ejb:bean name="AdCompanyControllerEJB"
 *           display-name="Used for editing company"
 *           type="Stateless"
 *           view-type="remote"
 *           jndi-name="ejb/AdCompanyControllerEJB"
 *
 * @ejb:interface remote-class="com.ejb.txn.AdCompanyController"
 *                extends="javax.ejb.EJBObject"
 *
 * @ejb:home remote-class="com.ejb.txn.AdCompanyControllerHome"
 *           extends="javax.ejb.EJBHome"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="aduser"
 *                        role-link="aduserlink"
 *
 * @ejb:permission role-name="aduser"
 * 
*/

public class AdCompanyControllerBean extends AbstractSessionBean {

    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList getGlFcAll(Integer AD_CMPNY) {
                    
        Debug.print("ArCustomerEntryControllerBean getGlFcAll");
        
        LocalGlFunctionalCurrencyHome glFunctionalCurrencyHome = null;

        ArrayList list = new ArrayList();
        
        // Initialize EJB Home
        
        try {
            
            glFunctionalCurrencyHome = (LocalGlFunctionalCurrencyHome)EJBHomeFactory.
                lookUpLocalHome(LocalGlFunctionalCurrencyHome.JNDI_NAME, LocalGlFunctionalCurrencyHome.class);
            
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {
            
            Collection glFunctionalCurrencies = glFunctionalCurrencyHome.findFcAllEnabled(new java.util.Date(), AD_CMPNY);
	        
	        Iterator i = glFunctionalCurrencies.iterator();
	               
	        while (i.hasNext()) {
	        	
	        	LocalGlFunctionalCurrency glFunctionalCurrency = (LocalGlFunctionalCurrency)i.next();
	        	
	        	list.add(glFunctionalCurrency.getFcName());
	        	
	        }
	        
	        return list;
          
        } catch (Exception ex) {
        	
        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());
        	
        }
            
    }
    
    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList getGenFlAll(Integer AD_CMPNY) {
                    
        Debug.print("ArCustomerEntryControllerBean getGenFlAll");
        
        LocalGenFieldHome genFieldHome = null;

        ArrayList list = new ArrayList();
        
        // Initialize EJB Home
        
        try {
            
            genFieldHome = (LocalGenFieldHome)EJBHomeFactory.
                lookUpLocalHome(LocalGenFieldHome.JNDI_NAME, LocalGenFieldHome.class);
            
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {
            
            Collection genFields = genFieldHome.findFlAllEnabled(AD_CMPNY);
	        
	        Iterator i = genFields.iterator();
	               
	        while (i.hasNext()) {
	        	
	        	LocalGenField genField = (LocalGenField)i.next();
	        	
	        	list.add(genField.getFlName());
	        	
	        }
	        
	        return list;
          
        } catch (Exception ex) {
        	
        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());
        	
        }
            
    }

    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public com.util.AdModCompanyDetails getArCmp(Integer AD_CMPNY) {
                    
        Debug.print("AdCompanyControllerBean getArCmp");

        LocalAdCompanyHome adCompanyHome = null;
        LocalAdCompany adCompany = null;
        
        // Initialize EJB Home

        try {

            adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);

        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {
        	
	        adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
	
			AdModCompanyDetails mdetails = new AdModCompanyDetails();
			    mdetails.setCmpName(adCompany.getCmpName());
			    mdetails.setCmpTaxPayerName(adCompany.getCmpTaxPayerName());
			    mdetails.setCmpContact(adCompany.getCmpContact());  
			    mdetails.setCmpDescription(adCompany.getCmpDescription());
			    mdetails.setCmpAddress(adCompany.getCmpAddress());
			    mdetails.setCmpCity(adCompany.getCmpCity());
			    mdetails.setCmpZip(adCompany.getCmpZip());
			    mdetails.setCmpCountry(adCompany.getCmpCountry());
			    mdetails.setCmpPhone(adCompany.getCmpPhone());
			    mdetails.setCmpFax(adCompany.getCmpFax());
			    mdetails.setCmpEmail(adCompany.getCmpEmail());
			    mdetails.setCmpTin(adCompany.getCmpTin());
			    
			    mdetails.setCmpMailSectionNo(adCompany.getCmpMailSectionNo());
			    mdetails.setCmpMailLotNo(adCompany.getCmpMailLotNo());
			    mdetails.setCmpMailStreet(adCompany.getCmpMailStreet());
			    mdetails.setCmpMailPoBox(adCompany.getCmpMailPoBox());
			    mdetails.setCmpMailCountry(adCompany.getCmpMailCountry());
			    mdetails.setCmpMailProvince(adCompany.getCmpMailProvince());
			    mdetails.setCmpMailPostOffice(adCompany.getCmpMailPostOffice());
			    mdetails.setCmpMailCareOff(adCompany.getCmpMailCareOff());
			    mdetails.setCmpTaxPeriodFrom(adCompany.getCmpTaxPeriodFrom());
			    mdetails.setCmpTaxPeriodTo(adCompany.getCmpTaxPeriodTo());
			    mdetails.setCmpPublicOfficeName(adCompany.getCmpPublicOfficeName());
			    mdetails.setCmpDateAppointment(adCompany.getCmpDateAppointment());
			    
			    mdetails.setCmpRevenueOffice(adCompany.getCmpRevenueOffice());
			    mdetails.setCmpFiscalYearEnding(adCompany.getCmpFiscalYearEnding());
			    mdetails.setCmpIndustry(adCompany.getCmpIndustry());
			    mdetails.setCmpRetainedEarnings(adCompany.getCmpRetainedEarnings());
			    mdetails.setCmpGlFcName(adCompany.getGlFunctionalCurrency().getFcName());
			    mdetails.setCmpGenFlName(adCompany.getGenField().getFlName());
			    
	        return mdetails;
	        
        } catch (Exception ex) {
        	
        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());
        	
        }
            
    }    
    
    /**
     * @ejb:interface-method view-type="remote"
     **/
    public void saveArCmpEntry(com.util.AdCompanyDetails details, String FC_NM, String FL_NM, Integer AD_CMPNY) {
                    
        Debug.print("AdCompanyControllerBean saveArCmpEntry");
        
        LocalAdCompanyHome adCompanyHome = null;
        LocalGlFunctionalCurrencyHome glFunctionalCurrencyHome = null; 
        LocalGenFieldHome genFieldHome = null;               
                
        // Initialize EJB Home
        
        try {

            adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);  
            glFunctionalCurrencyHome = (LocalGlFunctionalCurrencyHome)EJBHomeFactory.
                lookUpLocalHome(LocalGlFunctionalCurrencyHome.JNDI_NAME, LocalGlFunctionalCurrencyHome.class);           
            genFieldHome = (LocalGenFieldHome)EJBHomeFactory.
                lookUpLocalHome(LocalGenFieldHome.JNDI_NAME, LocalGenFieldHome.class);           

        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {
        	
        	LocalAdCompany adCompany = null;
        	
			// update company
			
			adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
				
			    adCompany.setCmpName(details.getCmpName());
			    adCompany.setCmpTaxPayerName(details.getCmpTaxPayerName());
			    adCompany.setCmpContact(details.getCmpContact());  
			    adCompany.setCmpDescription(details.getCmpDescription());
			    adCompany.setCmpAddress(details.getCmpAddress());
			    adCompany.setCmpCity(details.getCmpCity());
			    adCompany.setCmpZip(details.getCmpZip());
			    adCompany.setCmpCountry(details.getCmpCountry());
			    adCompany.setCmpPhone(details.getCmpPhone());
			    adCompany.setCmpFax(details.getCmpFax());
			    adCompany.setCmpEmail(details.getCmpEmail());
			    adCompany.setCmpTin(details.getCmpTin());
			    
			    adCompany.setCmpMailSectionNo(details.getCmpMailSectionNo());
			    adCompany.setCmpMailLotNo(details.getCmpMailLotNo());
			    adCompany.setCmpMailStreet(details.getCmpMailStreet());
			    adCompany.setCmpMailPoBox(details.getCmpMailPoBox());
			    adCompany.setCmpMailCountry(details.getCmpMailCountry());
			    adCompany.setCmpMailProvince(details.getCmpMailProvince());
			    adCompany.setCmpMailPostOffice(details.getCmpMailPostOffice());
			    adCompany.setCmpMailCareOff(details.getCmpMailCareOff());
			    adCompany.setCmpTaxPeriodFrom(details.getCmpTaxPeriodFrom());
			    adCompany.setCmpTaxPeriodTo(details.getCmpTaxPeriodTo());
			    adCompany.setCmpPublicOfficeName(details.getCmpPublicOfficeName());
			    adCompany.setCmpDateAppointment(details.getCmpDateAppointment());
			    adCompany.setCmpRevenueOffice(details.getCmpRevenueOffice());
			    adCompany.setCmpFiscalYearEnding(details.getCmpFiscalYearEnding());
			    adCompany.setCmpIndustry(details.getCmpIndustry());
			    adCompany.setCmpRetainedEarnings(details.getCmpRetainedEarnings());
			    
			LocalGlFunctionalCurrency glFunctionalCurrency = glFunctionalCurrencyHome.findByFcName(FC_NM, AD_CMPNY);
		        glFunctionalCurrency.addAdCompany(adCompany);
		        
		    LocalGenField genField = genFieldHome.findByFlName(FL_NM, AD_CMPNY);
		        genField.addAdCompany(adCompany);

        } catch (Exception ex) {
        	
        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());
        	
        }
            
    }    
    
    // SessionBean methods

    /**
     * @ejb:create-method view-type="remote"
     **/
    public void ejbCreate() throws CreateException {

        Debug.print("AdCompanyControllerBean ejbCreate");
      
    }
    
}

