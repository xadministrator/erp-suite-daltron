
/*
 * AdConsoleControllerBean.java
 *
 * Created on March 02, 2005, 10:01 AM
 *
 * @author  Dennis M. Hilario
 *
 */

package com.ejb.txn;

import java.util.ArrayList;
import java.util.Collection;
import java.util.GregorianCalendar;
import java.util.Iterator;
import java.util.StringTokenizer;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;
import javax.naming.NamingException;

import sun.misc.BASE64Encoder;




//import java.util.Base64;
import com.ejb.ad.LocalAdApprovalDocumentHome;
import com.ejb.ad.LocalAdApprovalHome;
import com.ejb.ad.LocalAdBranch;
import com.ejb.ad.LocalAdBranchHome;
import com.ejb.ad.LocalAdBranchResponsibility;
import com.ejb.ad.LocalAdBranchResponsibilityHome;
import com.ejb.ad.LocalAdBranchCoa;
import com.ejb.ad.LocalAdBranchCoaHome;
import com.ejb.ad.LocalAdCompany;
import com.ejb.ad.LocalAdCompanyHome;
import com.ejb.ad.LocalAdFormFunction;
import com.ejb.ad.LocalAdFormFunctionHome;
import com.ejb.ad.LocalAdFormFunctionResponsibility;
import com.ejb.ad.LocalAdFormFunctionResponsibilityHome;
import com.ejb.ad.LocalAdLookUp;
import com.ejb.ad.LocalAdLookUpHome;
import com.ejb.ad.LocalAdLookUpValue;
import com.ejb.ad.LocalAdLookUpValueHome;
import com.ejb.ad.LocalAdPaymentSchedule;
import com.ejb.ad.LocalAdPaymentScheduleHome;
import com.ejb.ad.LocalAdPaymentTerm;
import com.ejb.ad.LocalAdPaymentTermHome;
import com.ejb.ad.LocalAdPreference;
import com.ejb.ad.LocalAdPreferenceHome;
import com.ejb.ad.LocalAdResponsibility;
import com.ejb.ad.LocalAdResponsibilityHome;
import com.ejb.ad.LocalAdStoredProcedure;
import com.ejb.ad.LocalAdStoredProcedureHome;
import com.ejb.ad.LocalAdUser;
import com.ejb.ad.LocalAdUserHome;
import com.ejb.ad.LocalAdUserResponsibility;
import com.ejb.ad.LocalAdUserResponsibilityHome;
import com.ejb.ap.LocalApSupplierClass;
import com.ejb.ap.LocalApSupplierClassHome;
import com.ejb.ap.LocalApTaxCode;
import com.ejb.ap.LocalApTaxCodeHome;
import com.ejb.ap.LocalApWithholdingTaxCode;
import com.ejb.ap.LocalApWithholdingTaxCodeHome;
import com.ejb.ar.LocalArAutoAccounting;
import com.ejb.ar.LocalArAutoAccountingHome;
import com.ejb.ar.LocalArAutoAccountingSegment;
import com.ejb.ar.LocalArAutoAccountingSegmentHome;
import com.ejb.ar.LocalArCustomerClass;
import com.ejb.ar.LocalArCustomerClassHome;
import com.ejb.ar.LocalArStandardMemoLine;
import com.ejb.ar.LocalArStandardMemoLineHome;
import com.ejb.ar.LocalArTaxCode;
import com.ejb.ar.LocalArTaxCodeHome;
import com.ejb.ar.LocalArWithholdingTaxCode;
import com.ejb.ar.LocalArWithholdingTaxCodeHome;
import com.ejb.exception.GlobalRecordAlreadyExistException;
import com.ejb.exception.GlobalRecordInvalidException;
import com.ejb.genfld.LocalGenField;
import com.ejb.genfld.LocalGenFieldHome;
import com.ejb.genfld.LocalGenQualifier;
import com.ejb.genfld.LocalGenQualifierHome;
import com.ejb.genfld.LocalGenSegment;
import com.ejb.genfld.LocalGenSegmentHome;
import com.ejb.genfld.LocalGenValueSet;
import com.ejb.genfld.LocalGenValueSetHome;
import com.ejb.genfld.LocalGenValueSetValue;
import com.ejb.genfld.LocalGenValueSetValueHome;
import com.ejb.gl.LocalAdApplication;
import com.ejb.gl.LocalAdApplicationHome;
import com.ejb.gl.LocalAdDocumentCategory;
import com.ejb.gl.LocalAdDocumentCategoryHome;
import com.ejb.gl.LocalAdDocumentSequence;
import com.ejb.gl.LocalAdDocumentSequenceAssignment;
import com.ejb.gl.LocalAdDocumentSequenceAssignmentHome;
import com.ejb.gl.LocalAdDocumentSequenceHome;
import com.ejb.gl.LocalGlAccountRange;
import com.ejb.gl.LocalGlAccountRangeHome;
import com.ejb.gl.LocalGlChartOfAccount;
import com.ejb.gl.LocalGlChartOfAccountHome;
import com.ejb.gl.LocalGlFunctionalCurrency;
import com.ejb.gl.LocalGlFunctionalCurrencyHome;
import com.ejb.gl.LocalGlInvestorAccountBalanceHome;
import com.ejb.gl.LocalGlJournalCategoryHome;
import com.ejb.gl.LocalGlJournalSourceHome;
import com.ejb.gl.LocalGlOrganization;
import com.ejb.gl.LocalGlOrganizationHome;
import com.ejb.gl.LocalGlPeriodTypeHome;
import com.ejb.gl.LocalGlResponsibility;
import com.ejb.gl.LocalGlResponsibilityHome;
import com.util.AbstractSessionBean;
import com.util.Debug;
import com.util.EJBCommon;
import com.util.EJBHomeFactory;
import com.util.GenSegmentDetails;
import com.util.GenValueSetValueDetails;
import com.util.GlChartOfAccountDetails;
import com.util.GlRepGeneralLedgerDetails;
import com.ejb.gl.LocalGlJournalBatchHome;
import com.ejb.ar.LocalArInvoiceBatchHome;
import com.ejb.ar.LocalArReceiptBatchHome;
import com.ejb.ap.LocalApVoucherBatchHome;
import com.ejb.ap.LocalApCheckBatchHome;

/**
 * @ejb:bean name="AdConsoleControllerEJB"
 *           display-name="Used for admin console"
 *           type="Stateless"
 *           view-type="remote"
 *           jndi-name="ejb/AdConsoleControllerEJB"
 *
 * @ejb:interface remote-class="com.ejb.txn.AdConsoleController"
 *                extends="javax.ejb.EJBObject"
 *
 * @ejb:home remote-class="com.ejb.txn.AdConsoleControllerHome"
 *           extends="javax.ejb.EJBHome"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="aduser"
 *                        role-link="aduserlink"
 *
 * @ejb:permission role-name="aduser"
 *
*/

public class AdConsoleControllerBean extends AbstractSessionBean {

	/**
     * @ejb:interface-method view-type="remote"
     **/
    public void executeAdConsole(String CMP_NM, String CMP_SHRT_NM, String CMP_WLCM_NT, short FL_NMBR_OF_SGMNT,
    		String FL_SGMNT_SPRTR, String CMP_RTND_EARNNGS, ArrayList genSegmentList, ArrayList genValueSetValueList, ArrayList glCoaList)
    	throws GlobalRecordAlreadyExistException,
    	GlobalRecordInvalidException {

        Debug.print("AdConsoleControllerBean executeAdConsole");

        LocalAdCompanyHome adCompanyHome = null;
        LocalGenFieldHome genFieldHome = null;
        LocalGenValueSetHome genValueSetHome = null;
        LocalGenSegmentHome genSegmentHome = null;
        LocalGenValueSetValueHome genValueSetValueHome = null;
        LocalGenQualifierHome genQualifierHome = null;
        LocalGlChartOfAccountHome glChartOfAccountHome = null;
        LocalAdApplicationHome adApplicationHome = null;
        LocalAdApprovalHome adApprovalHome = null;
        LocalAdApprovalDocumentHome adApprovalDocumentHome = null;
        LocalAdDocumentCategoryHome adDocumentCategoryHome = null;
        LocalAdDocumentSequenceHome adDocumentSequenceHome = null;
        LocalAdDocumentSequenceAssignmentHome adDocumentSequenceAssignmentHome = null;
        LocalAdUserHome adUserHome = null;
        LocalAdResponsibilityHome adResponsibilityHome = null;
        LocalAdFormFunctionResponsibilityHome adFormFunctionResponsibilityHome = null;
        LocalAdUserResponsibilityHome adUserResponsibilityHome = null;
        LocalAdFormFunctionHome adFormFunctionHome = null;
        LocalAdLookUpHome adLookUpHome = null;
        LocalAdLookUpValueHome adLookUpValueHome = null;
        LocalAdPreferenceHome adPreferenceHome = null;
        LocalAdStoredProcedureHome adStoredProcedureHome = null;
        LocalAdPaymentTermHome adPaymentTermHome = null;
        LocalAdPaymentScheduleHome adPaymentScheduleHome = null;
        LocalAdBranchHome adBranchHome = null;
        LocalAdBranchResponsibilityHome adBranchResponsibilityHome = null;
        LocalAdBranchCoaHome adBranchCoaHome = null;
        LocalApTaxCodeHome apTaxCodeHome = null;
        LocalApWithholdingTaxCodeHome apWithholdingTaxCodeHome = null;
        LocalApSupplierClassHome apSupplierClassHome = null;
        LocalArAutoAccountingHome arAutoAccountingHome = null;
        LocalArAutoAccountingSegmentHome arAutoAccountingSegmentHome = null;
        LocalArTaxCodeHome arTaxCodeHome = null;
        LocalArWithholdingTaxCodeHome arWithholdingTaxCodeHome = null;
        LocalArCustomerClassHome arCustomerClassHome = null;
        LocalArStandardMemoLineHome arStandardMemoLineHome = null;
        LocalGlOrganizationHome glOrganizationHome = null;
        LocalGlAccountRangeHome glAccountRangeHome = null;
        LocalGlResponsibilityHome glResponsibilityHome = null;
        LocalGlPeriodTypeHome glPeriodTypeHome = null;
        LocalGlFunctionalCurrencyHome glFunctionalCurrencyHome = null;
        LocalGlJournalCategoryHome glJournalCategoryHome = null;
        LocalGlJournalSourceHome glJournalSourceHome = null;


        LocalGlJournalBatchHome glJournalBatchHome = null;
        LocalArInvoiceBatchHome arInvoiceBatchHome = null;
        LocalArReceiptBatchHome arReceiptBatchHome = null;
        LocalApVoucherBatchHome apVoucherBatchHome = null;
        LocalApCheckBatchHome apCheckBatchHome = null;

        // Initialize EJB Home

        try {
        	glJournalBatchHome = (LocalGlJournalBatchHome)EJBHomeFactory.
                    lookUpLocalHome(LocalGlJournalBatchHome.JNDI_NAME, LocalGlJournalBatchHome.class);
        	apVoucherBatchHome = (LocalApVoucherBatchHome)EJBHomeFactory.
                    lookUpLocalHome(LocalApVoucherBatchHome.JNDI_NAME, LocalApVoucherBatchHome.class);
        	arReceiptBatchHome = (LocalArReceiptBatchHome)EJBHomeFactory.
                    lookUpLocalHome(LocalArReceiptBatchHome.JNDI_NAME, LocalArReceiptBatchHome.class);
        	apVoucherBatchHome = (LocalApVoucherBatchHome)EJBHomeFactory.
                    lookUpLocalHome(LocalApVoucherBatchHome.JNDI_NAME, LocalApVoucherBatchHome.class);
        	apCheckBatchHome = (LocalApCheckBatchHome)EJBHomeFactory.
                    lookUpLocalHome(LocalApCheckBatchHome.JNDI_NAME, LocalApCheckBatchHome.class);

            adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
            genFieldHome = (LocalGenFieldHome)EJBHomeFactory.
                lookUpLocalHome(LocalGenFieldHome.JNDI_NAME, LocalGenFieldHome.class);
            genValueSetHome = (LocalGenValueSetHome)EJBHomeFactory.
                lookUpLocalHome(LocalGenValueSetHome.JNDI_NAME, LocalGenValueSetHome.class);
            genSegmentHome = (LocalGenSegmentHome)EJBHomeFactory.
                lookUpLocalHome(LocalGenSegmentHome.JNDI_NAME, LocalGenSegmentHome.class);
            genValueSetValueHome = (LocalGenValueSetValueHome)EJBHomeFactory.
                lookUpLocalHome(LocalGenValueSetValueHome.JNDI_NAME, LocalGenValueSetValueHome.class);
            genQualifierHome = (LocalGenQualifierHome)EJBHomeFactory.
                lookUpLocalHome(LocalGenQualifierHome.JNDI_NAME, LocalGenQualifierHome.class);
            glChartOfAccountHome = (LocalGlChartOfAccountHome)EJBHomeFactory.
            	lookUpLocalHome(LocalGlChartOfAccountHome.JNDI_NAME, LocalGlChartOfAccountHome.class);
            adApplicationHome = (LocalAdApplicationHome)EJBHomeFactory.
        		lookUpLocalHome(LocalAdApplicationHome.JNDI_NAME, LocalAdApplicationHome.class);
            adApprovalHome = (LocalAdApprovalHome)EJBHomeFactory.
    			lookUpLocalHome(LocalAdApprovalHome.JNDI_NAME, LocalAdApprovalHome.class);
            adApprovalDocumentHome = (LocalAdApprovalDocumentHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdApprovalDocumentHome.JNDI_NAME, LocalAdApprovalDocumentHome.class);
            adDocumentCategoryHome = (LocalAdDocumentCategoryHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdDocumentCategoryHome.JNDI_NAME, LocalAdDocumentCategoryHome.class);
            adDocumentSequenceHome = (LocalAdDocumentSequenceHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdDocumentSequenceHome.JNDI_NAME, LocalAdDocumentSequenceHome.class);
            adDocumentSequenceAssignmentHome = (LocalAdDocumentSequenceAssignmentHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdDocumentSequenceAssignmentHome.JNDI_NAME, LocalAdDocumentSequenceAssignmentHome.class);
            adUserHome = (LocalAdUserHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdUserHome.JNDI_NAME, LocalAdUserHome.class);
            adResponsibilityHome = (LocalAdResponsibilityHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdResponsibilityHome.JNDI_NAME, LocalAdResponsibilityHome.class);
            adFormFunctionResponsibilityHome = (LocalAdFormFunctionResponsibilityHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdFormFunctionResponsibilityHome.JNDI_NAME, LocalAdFormFunctionResponsibilityHome.class);
            adUserResponsibilityHome = (LocalAdUserResponsibilityHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdUserResponsibilityHome.JNDI_NAME, LocalAdUserResponsibilityHome.class);
            adFormFunctionHome = (LocalAdFormFunctionHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdFormFunctionHome.JNDI_NAME, LocalAdFormFunctionHome.class);
            adLookUpHome = (LocalAdLookUpHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdLookUpHome.JNDI_NAME, LocalAdLookUpHome.class);
            adLookUpValueHome = (LocalAdLookUpValueHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdLookUpValueHome.JNDI_NAME, LocalAdLookUpValueHome.class);
            adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);
            adStoredProcedureHome = (LocalAdStoredProcedureHome)EJBHomeFactory.
    				lookUpLocalHome(LocalAdStoredProcedureHome.JNDI_NAME, LocalAdStoredProcedureHome.class);


            adPaymentTermHome = (LocalAdPaymentTermHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdPaymentTermHome.JNDI_NAME, LocalAdPaymentTermHome.class);
            adPaymentScheduleHome = (LocalAdPaymentScheduleHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdPaymentScheduleHome.JNDI_NAME, LocalAdPaymentScheduleHome.class);
            adBranchHome = (LocalAdBranchHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdBranchHome.JNDI_NAME, LocalAdBranchHome.class);
            adBranchCoaHome = (LocalAdBranchCoaHome)EJBHomeFactory.
       			lookUpLocalHome(LocalAdBranchCoaHome.JNDI_NAME, LocalAdBranchCoaHome.class);
            adBranchResponsibilityHome = (LocalAdBranchResponsibilityHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdBranchResponsibilityHome.JNDI_NAME, LocalAdBranchResponsibilityHome.class);
            apTaxCodeHome = (LocalApTaxCodeHome)EJBHomeFactory.
				lookUpLocalHome(LocalApTaxCodeHome.JNDI_NAME, LocalApTaxCodeHome.class);
            apWithholdingTaxCodeHome = (LocalApWithholdingTaxCodeHome)EJBHomeFactory.
				lookUpLocalHome(LocalApWithholdingTaxCodeHome.JNDI_NAME, LocalApWithholdingTaxCodeHome.class);
            apSupplierClassHome = (LocalApSupplierClassHome)EJBHomeFactory.
				lookUpLocalHome(LocalApSupplierClassHome.JNDI_NAME, LocalApSupplierClassHome.class);
            arAutoAccountingHome = (LocalArAutoAccountingHome)EJBHomeFactory.
				lookUpLocalHome(LocalArAutoAccountingHome.JNDI_NAME, LocalArAutoAccountingHome.class);
            arAutoAccountingSegmentHome = (LocalArAutoAccountingSegmentHome)EJBHomeFactory.
				lookUpLocalHome(LocalArAutoAccountingSegmentHome.JNDI_NAME, LocalArAutoAccountingSegmentHome.class);
            arTaxCodeHome = (LocalArTaxCodeHome)EJBHomeFactory.
				lookUpLocalHome(LocalArTaxCodeHome.JNDI_NAME, LocalArTaxCodeHome.class);
            arWithholdingTaxCodeHome = (LocalArWithholdingTaxCodeHome)EJBHomeFactory.
				lookUpLocalHome(LocalArWithholdingTaxCodeHome.JNDI_NAME, LocalArWithholdingTaxCodeHome.class);
            arCustomerClassHome = (LocalArCustomerClassHome)EJBHomeFactory.
				lookUpLocalHome(LocalArCustomerClassHome.JNDI_NAME, LocalArCustomerClassHome.class);
            arStandardMemoLineHome = (LocalArStandardMemoLineHome)EJBHomeFactory.
				lookUpLocalHome(LocalArStandardMemoLineHome.JNDI_NAME, LocalArStandardMemoLineHome.class);
			glOrganizationHome = (LocalGlOrganizationHome)EJBHomeFactory.
				lookUpLocalHome(LocalGlOrganizationHome.JNDI_NAME, LocalGlOrganizationHome.class);
			glAccountRangeHome = (LocalGlAccountRangeHome)EJBHomeFactory.
				lookUpLocalHome(LocalGlAccountRangeHome.JNDI_NAME, LocalGlAccountRangeHome.class);
			glResponsibilityHome = (LocalGlResponsibilityHome)EJBHomeFactory.
				lookUpLocalHome(LocalGlResponsibilityHome.JNDI_NAME, LocalGlResponsibilityHome.class);
			glPeriodTypeHome = (LocalGlPeriodTypeHome)EJBHomeFactory.
				lookUpLocalHome(LocalGlPeriodTypeHome.JNDI_NAME, LocalGlPeriodTypeHome.class);
			glFunctionalCurrencyHome = (LocalGlFunctionalCurrencyHome)EJBHomeFactory.
				lookUpLocalHome(LocalGlFunctionalCurrencyHome.JNDI_NAME, LocalGlFunctionalCurrencyHome.class);
			glJournalCategoryHome = (LocalGlJournalCategoryHome)EJBHomeFactory.
				lookUpLocalHome(LocalGlJournalCategoryHome.JNDI_NAME, LocalGlJournalCategoryHome.class);
			glJournalSourceHome = (LocalGlJournalSourceHome)EJBHomeFactory.
				lookUpLocalHome(LocalGlJournalSourceHome.JNDI_NAME, LocalGlJournalSourceHome.class);

        } catch (NamingException ex) {

            throw new EJBException(ex.getMessage());

        }


        try {

        	try {

        		LocalAdCompany adExistingCompany = adCompanyHome.findByCmpName(CMP_NM);

        		throw new GlobalRecordAlreadyExistException();

        	} catch (FinderException ex) { }

        	try {

        		LocalAdCompany adExistingCompany = adCompanyHome.findByCmpShortName(CMP_SHRT_NM);

        		throw new GlobalRecordAlreadyExistException();

        	} catch (FinderException ex) { }

        	if (CMP_RTND_EARNNGS == null) throw new GlobalRecordInvalidException();


        	// company

        	LocalAdCompany adCompany = adCompanyHome.create(CMP_NM, CMP_SHRT_NM, null, null, CMP_WLCM_NT, null,
        	    null, null, null, null, null, null, null, null,
        	    null, null, null, null, null, null, null, null, null, null, null, null,
        	    null, null, null, CMP_RTND_EARNNGS);

        	Integer CMP_CODE = adCompany.getCmpCode();

        	// fields & segments

        	LocalGenField genField = genFieldHome.create(CMP_NM, CMP_NM, FL_SGMNT_SPRTR.charAt(0), FL_NMBR_OF_SGMNT, EJBCommon.FALSE, EJBCommon.TRUE, CMP_CODE);

        	genField.addAdCompany(adCompany);

        	LocalGenQualifier genAssetQualifier = genQualifierHome.create("ASSET", EJBCommon.TRUE, EJBCommon.TRUE, CMP_CODE);
    		LocalGenQualifier genLiabilityQualifier = genQualifierHome.create("LIABILITY", EJBCommon.TRUE, EJBCommon.TRUE, CMP_CODE);
    		LocalGenQualifier genEquityQualifier = genQualifierHome.create("OWNERS EQUITY", EJBCommon.TRUE, EJBCommon.TRUE, CMP_CODE);
    		LocalGenQualifier genRevenueQualifier = genQualifierHome.create("REVENUE", EJBCommon.TRUE, EJBCommon.TRUE, CMP_CODE);
    		LocalGenQualifier genExpenseQualifier = genQualifierHome.create("EXPENSE", EJBCommon.TRUE, EJBCommon.TRUE, CMP_CODE);

        	Iterator segmentIter = genSegmentList.iterator();

        	while (segmentIter.hasNext()) {

        		GenSegmentDetails segmentDetails = (GenSegmentDetails)segmentIter.next();

        		LocalGenValueSet genValueSet = genValueSetHome.create(segmentDetails.getSgName(), segmentDetails.getSgDescription(), EJBCommon.TRUE, CMP_CODE);

        		LocalGenSegment genSegment = genSegmentHome.create(segmentDetails.getSgName(), segmentDetails.getSgDescription(), segmentDetails.getSgMaxSize(), "0",
        			segmentDetails.getSgSegmentType(), segmentDetails.getSgSegmentNumber(), CMP_CODE);

        		genField.addGenSegment(genSegment);
        		genValueSet.addGenSegment(genSegment);

        		Iterator vsvIter = genValueSetValueList.iterator();

        		while (vsvIter.hasNext()) {

        			GenValueSetValueDetails vsvDetails = (GenValueSetValueDetails)vsvIter.next();

        			if (vsvDetails.getVsvSegmentNumber() == segmentDetails.getSgSegmentNumber()) {

        				LocalGenValueSetValue genValueSetValue = genValueSetValueHome.create(vsvDetails.getVsvValue(), vsvDetails.getVsvDescription(), EJBCommon.FALSE, (short)0, EJBCommon.TRUE, CMP_CODE);
        				genValueSet.addGenValueSetValue(genValueSetValue);

        				if (segmentDetails.getSgSegmentType() == 'N') {

	        				if (vsvDetails.getVsvAccountType().equals("ASSET")) {

	        					genAssetQualifier.addGenValueSetValue(genValueSetValue);

	        				} else if (vsvDetails.getVsvAccountType().equals("LIABILITY")) {

	        					genLiabilityQualifier.addGenValueSetValue(genValueSetValue);

	        				} else if (vsvDetails.getVsvAccountType().equals("OWNERS EQUITY")) {

	        					genEquityQualifier.addGenValueSetValue(genValueSetValue);

	        				} else if (vsvDetails.getVsvAccountType().equals("REVENUE")) {

	        					genRevenueQualifier.addGenValueSetValue(genValueSetValue);

	        				} else if (vsvDetails.getVsvAccountType().equals("EXPENSE")) {

	        					genExpenseQualifier.addGenValueSetValue(genValueSetValue);

	        				}

	        				}

        			}

        		}

        	}

        	// branch
        	LocalAdBranch adBranch = adBranchHome.create("HQ", "Head Office", "", "", (byte)1, "", "", "", 'N', (byte)0, 0, CMP_CODE);
        	Integer AD_BRANCH = adBranch.getBrCode();

        	// coa

        	LocalGlChartOfAccount glFirstCoa = null;

        	Iterator coaIter = glCoaList.iterator();

        	while (coaIter.hasNext()) {

        		GlChartOfAccountDetails coaDetails = (GlChartOfAccountDetails)coaIter.next();

        		String COA_DESC = "";
        		String COA_ACCNT_TYP = "";

        		StringTokenizer st = new StringTokenizer(coaDetails.getCoaAccountNumber(), FL_SGMNT_SPRTR);
        		Collection genSegments = genSegmentHome.findByFlCode(genField.getFlCode(), CMP_CODE);
      	        Iterator j = genSegments.iterator();
      	        int a = 0;
    	        while (st.hasMoreTokens()) {
    	            LocalGenSegment genSegment = (LocalGenSegment) j.next();

    	            String tknVal = st.nextToken();
    	            System.out.println("toekn is : " +  tknVal);
    	            LocalGenValueSetValue genValueSetValue = genValueSetValueHome.findByVsCodeAndVsvValue(genSegment.getGenValueSet().getVsCode(), tknVal, CMP_CODE);

    	            System.out.println("gen qualified is : " +  genValueSetValue.getGenQualifier());
    	            if (genSegment.getSgSegmentType() == 'N') COA_ACCNT_TYP = genValueSetValue.getGenQualifier().getQlAccountType();

    	            System.out.println("pass a");
    	            COA_DESC = COA_DESC + genValueSetValue.getVsvDescription();
    	            System.out.println("pass b");
			        if (st.hasMoreTokens()) {

			        	COA_DESC = COA_DESC + FL_SGMNT_SPRTR;

			        }
    		        a++;
    		    }
    	        System.out.println("pass 1");
        		LocalGlChartOfAccount glChartOfAccount = glChartOfAccountHome.create(
        				coaDetails.getCoaAccountNumber(),
        				COA_DESC,
        				COA_ACCNT_TYP,
        				null,
        				null,
        				null,
        				null,
						coaDetails.getCoaDateFrom(),
						coaDetails.getCoaDateTo(),
						coaDetails.getCoaSegment1(),
						coaDetails.getCoaSegment2(),
						coaDetails.getCoaSegment3(),
						coaDetails.getCoaSegment4(),
						coaDetails.getCoaSegment5(),
						coaDetails.getCoaSegment6(),
						coaDetails.getCoaSegment7(),
						coaDetails.getCoaSegment8(),
						coaDetails.getCoaSegment9(),
						coaDetails.getCoaSegment10(), EJBCommon.TRUE, EJBCommon.TRUE, CMP_CODE);

        	      if (glFirstCoa == null) glFirstCoa = glChartOfAccount;

        	      // branch mapping

        	      LocalAdBranchCoa adBranchCoa = adBranchCoaHome.create(adCompany.getCmpCode());
        	      glChartOfAccount.addAdBranchCoa(adBranchCoa);
        	      adBranch.addAdBranchCoa(adBranchCoa);

        	}
        	  System.out.println("pass 3");
        	// document

        	LocalAdApplication adApplicationGl = adApplicationHome.create("OMEGA GENERAL LEDGER", "OMEGA GENERAL LEDGER", EJBCommon.TRUE, CMP_CODE);
        	LocalAdApplication adApplicationAp = adApplicationHome.create("OMEGA PAYABLES", "OMEGA PAYABLES", EJBCommon.TRUE, CMP_CODE);
        	LocalAdApplication adApplicationAr = adApplicationHome.create("OMEGA RECEIVABLES", "OMEGA RECEIVABLES", EJBCommon.TRUE, CMP_CODE);
        	LocalAdApplication adApplicationCm = adApplicationHome.create("OMEGA CASH MANAGEMENT", "OMEGA CASH MANAGEMENT", EJBCommon.TRUE, CMP_CODE);
        	LocalAdApplication adApplicationAd = adApplicationHome.create("OMEGA ADMINISTRATION", "OMEGA ADMINISTRATION", EJBCommon.TRUE, CMP_CODE);
        	LocalAdApplication adApplicationInv = adApplicationHome.create("OMEGA INVENTORY", "OMEGA INVENTORY", EJBCommon.TRUE, CMP_CODE);
        	LocalAdApplication adApplicationHr = adApplicationHome.create("OMEGA HUMAN RESOURCE", "OMEGA HUMAN RESOURCE", EJBCommon.FALSE, CMP_CODE);
        	LocalAdApplication adApplicationPm = adApplicationHome.create("OMEGA PROJECT MANAGEMENT", "OMEGA PROJECT MANAGEMENT", EJBCommon.FALSE, CMP_CODE);


        	adApprovalHome.create(EJBCommon.FALSE, EJBCommon.FALSE,  EJBCommon.FALSE,
        			EJBCommon.FALSE, EJBCommon.FALSE,
					EJBCommon.FALSE, EJBCommon.FALSE, EJBCommon.FALSE, EJBCommon.FALSE,
					EJBCommon.FALSE, EJBCommon.FALSE,
					EJBCommon.FALSE, (short)3, EJBCommon.FALSE, EJBCommon.FALSE,EJBCommon.FALSE,EJBCommon.FALSE,
					EJBCommon.FALSE, EJBCommon.FALSE,
					EJBCommon.FALSE, EJBCommon.FALSE, EJBCommon.FALSE,
					EJBCommon.FALSE, EJBCommon.FALSE, EJBCommon.FALSE, EJBCommon.FALSE, CMP_CODE);

        	adApprovalDocumentHome.create("GL JOURNAL", "PRINT ANYTIME", EJBCommon.TRUE, EJBCommon.FALSE, EJBCommon.FALSE, CMP_CODE);

        	adApprovalDocumentHome.create("AP PURCHASE REQUISITION", "PRINT ANYTIME", EJBCommon.TRUE, EJBCommon.FALSE, EJBCommon.FALSE, CMP_CODE);
        	adApprovalDocumentHome.create("AP CANVASS", "PRINT ANYTIME", EJBCommon.TRUE, EJBCommon.FALSE, EJBCommon.FALSE, CMP_CODE);
        	adApprovalDocumentHome.create("AP PURCHASE ORDER", "PRINT ANYTIME", EJBCommon.TRUE, EJBCommon.FALSE, EJBCommon.FALSE, CMP_CODE);
        	adApprovalDocumentHome.create("AP RECEIVING ITEM", "PRINT ANYTIME", EJBCommon.TRUE, EJBCommon.FALSE, EJBCommon.FALSE, CMP_CODE);
        	adApprovalDocumentHome.create("AP VOUCHER", "PRINT ANYTIME", EJBCommon.TRUE, EJBCommon.FALSE, EJBCommon.FALSE, CMP_CODE);
        	adApprovalDocumentHome.create("AP DEBIT MEMO", "PRINT ANYTIME", EJBCommon.TRUE, EJBCommon.FALSE, EJBCommon.FALSE, CMP_CODE);
        	adApprovalDocumentHome.create("AP CHECK", "PRINT ANYTIME", EJBCommon.TRUE, EJBCommon.FALSE, EJBCommon.FALSE, CMP_CODE);
        	adApprovalDocumentHome.create("AP CHECK PAYMENT REQUEST", "PRINT ANYTIME", EJBCommon.TRUE, EJBCommon.FALSE, EJBCommon.FALSE, CMP_CODE);

        	adApprovalDocumentHome.create("AR SALES ORDER", "PRINT ANYTIME", EJBCommon.TRUE, EJBCommon.FALSE, EJBCommon.FALSE, CMP_CODE);
        	adApprovalDocumentHome.create("AR INVOICE", "PRINT ANYTIME", EJBCommon.TRUE, EJBCommon.FALSE, EJBCommon.FALSE, CMP_CODE);
        	adApprovalDocumentHome.create("AR CREDIT MEMO", "PRINT ANYTIME", EJBCommon.TRUE, EJBCommon.FALSE, EJBCommon.FALSE, CMP_CODE);
        	adApprovalDocumentHome.create("AR RECEIPT", "PRINT ANYTIME", EJBCommon.TRUE, EJBCommon.FALSE, EJBCommon.FALSE, CMP_CODE);
        	adApprovalDocumentHome.create("AR STATEMENT OF ACCOUNT", "PRINT ANYTIME", EJBCommon.TRUE, EJBCommon.FALSE, EJBCommon.FALSE, CMP_CODE);
        	adApprovalDocumentHome.create("AR ACKNOWLEDGEMENT RECEIPT", "PRINT ANYTIME", EJBCommon.TRUE, EJBCommon.FALSE, EJBCommon.FALSE, CMP_CODE);

        	adApprovalDocumentHome.create("CM ADJUSTMENT", "PRINT ANYTIME", EJBCommon.TRUE, EJBCommon.FALSE, EJBCommon.FALSE, CMP_CODE);
        	adApprovalDocumentHome.create("CM FUND TRANSFER", "PRINT ANYTIME", EJBCommon.TRUE, EJBCommon.FALSE, EJBCommon.FALSE, CMP_CODE);

        	adApprovalDocumentHome.create("INV ADJUSTMENT REQUEST", "PRINT ANYTIME", EJBCommon.TRUE, EJBCommon.FALSE, EJBCommon.FALSE, CMP_CODE);
        	adApprovalDocumentHome.create("INV ADJUSTMENT", "PRINT ANYTIME", EJBCommon.TRUE, EJBCommon.FALSE, EJBCommon.FALSE, CMP_CODE);
        	adApprovalDocumentHome.create("INV BRANCH STOCK TRANSFER ORDER", "PRINT ANYTIME", EJBCommon.TRUE, EJBCommon.FALSE, EJBCommon.FALSE, CMP_CODE);
        	adApprovalDocumentHome.create("INV BRANCH STOCK TRANSFER-OUT", "PRINT ANYTIME", EJBCommon.TRUE, EJBCommon.FALSE, EJBCommon.FALSE, CMP_CODE);
        	adApprovalDocumentHome.create("INV BRANCH STOCK TRANSFER-IN", "PRINT ANYTIME", EJBCommon.TRUE, EJBCommon.FALSE, EJBCommon.FALSE, CMP_CODE);
        	adApprovalDocumentHome.create("INV STOCK TRANSFER", "PRINT ANYTIME", EJBCommon.TRUE, EJBCommon.FALSE, EJBCommon.FALSE, CMP_CODE);
        	adApprovalDocumentHome.create("INV BUILD ASSEMBLY", "PRINT ANYTIME", EJBCommon.TRUE, EJBCommon.FALSE, EJBCommon.FALSE, CMP_CODE);
        	adApprovalDocumentHome.create("INV BUILD ASSEMBLY ORDER", "PRINT ANYTIME", EJBCommon.TRUE, EJBCommon.FALSE, EJBCommon.FALSE, CMP_CODE);

        	adApprovalDocumentHome.create("INV OVERHEAD", "PRINT ANYTIME", EJBCommon.TRUE, EJBCommon.FALSE, EJBCommon.FALSE, CMP_CODE);
        	adApprovalDocumentHome.create("INV BUILD ORDER", "PRINT ANYTIME", EJBCommon.TRUE, EJBCommon.FALSE, EJBCommon.FALSE, CMP_CODE);
        	adApprovalDocumentHome.create("INV STOCK ISSUANCE", "PRINT ANYTIME", EJBCommon.TRUE, EJBCommon.FALSE, EJBCommon.FALSE, CMP_CODE);
        	adApprovalDocumentHome.create("INV ASSEMBLY TRANSFER", "PRINT ANYTIME", EJBCommon.TRUE, EJBCommon.FALSE, EJBCommon.FALSE, CMP_CODE);
        	adApprovalDocumentHome.create("INV TRANSACTIONAL BUDGET", "PRINT ANYTIME", EJBCommon.TRUE, EJBCommon.FALSE, EJBCommon.FALSE, CMP_CODE);


        	LocalAdDocumentCategory adDocumentCategory = adDocumentCategoryHome.create("GL JOURNAL", "JOURNAL", CMP_CODE);
        	LocalAdDocumentSequence adDocumentSequence = adDocumentSequenceHome.create("GL JOURNAL DOCUMENT", new GregorianCalendar(2000, 0, 1, 0, 0, 0).getTime(), new GregorianCalendar(2050, 0, 1, 0, 0, 0).getTime(), 'A', "1", CMP_CODE);
        	LocalAdDocumentSequenceAssignment adDocumentSequenceAssignment = adDocumentSequenceAssignmentHome.create(new Integer(0), "JV000001", CMP_CODE);
        	adDocumentCategory.addAdDocumentSequenceAssignment(adDocumentSequenceAssignment);
        	adDocumentSequence.addAdDocumentSequenceAssignment(adDocumentSequenceAssignment);
        	adApplicationGl.addAdDocumentSequence(adDocumentSequence);
        	adApplicationGl.addAdDocumentCategory(adDocumentCategory);


        	adDocumentCategory = adDocumentCategoryHome.create("AP PURCHASE REQUISITION", "PURCHASE REQUISITION", CMP_CODE);
        	adDocumentSequence = adDocumentSequenceHome.create("AP PURCHASE REQUISITION DOCUMENT", new GregorianCalendar(2000, 0, 1, 0, 0, 0).getTime(), new GregorianCalendar(2050, 0, 1, 0, 0, 0).getTime(), 'A', "1", CMP_CODE);
        	adDocumentSequenceAssignment = adDocumentSequenceAssignmentHome.create(new Integer(0), "PR000001", CMP_CODE);
        	adDocumentCategory.addAdDocumentSequenceAssignment(adDocumentSequenceAssignment);
        	adDocumentSequence.addAdDocumentSequenceAssignment(adDocumentSequenceAssignment);
        	adApplicationAp.addAdDocumentSequence(adDocumentSequence);
        	adApplicationAp.addAdDocumentCategory(adDocumentCategory);
        	adDocumentCategory = adDocumentCategoryHome.create("AP CANVASS", "CANVASS", CMP_CODE);
        	adDocumentSequence = adDocumentSequenceHome.create("AP CANVASS DOCUMENT", new GregorianCalendar(2000, 0, 1, 0, 0, 0).getTime(), new GregorianCalendar(2050, 0, 1, 0, 0, 0).getTime(), 'A', "1", CMP_CODE);
        	adDocumentSequenceAssignment = adDocumentSequenceAssignmentHome.create(new Integer(0), "CS000001", CMP_CODE);
        	adDocumentCategory.addAdDocumentSequenceAssignment(adDocumentSequenceAssignment);
        	adDocumentSequence.addAdDocumentSequenceAssignment(adDocumentSequenceAssignment);
        	adApplicationAp.addAdDocumentSequence(adDocumentSequence);
        	adApplicationAp.addAdDocumentCategory(adDocumentCategory);
        	adDocumentCategory = adDocumentCategoryHome.create("AP PURCHASE ORDER", "PURCHASE ORDER", CMP_CODE);
        	adDocumentSequence = adDocumentSequenceHome.create("AP PURCHASE ORDER DOCUMENT", new GregorianCalendar(2000, 0, 1, 0, 0, 0).getTime(), new GregorianCalendar(2050, 0, 1, 0, 0, 0).getTime(), 'A', "1", CMP_CODE);
        	adDocumentSequenceAssignment = adDocumentSequenceAssignmentHome.create(new Integer(0), "PO000001", CMP_CODE);
        	adDocumentCategory.addAdDocumentSequenceAssignment(adDocumentSequenceAssignment);
        	adDocumentSequence.addAdDocumentSequenceAssignment(adDocumentSequenceAssignment);
        	adApplicationAp.addAdDocumentSequence(adDocumentSequence);
        	adApplicationAp.addAdDocumentCategory(adDocumentCategory);
        	adDocumentCategory = adDocumentCategoryHome.create("AP RECEIVING ITEM", "RECEIVING ITEM", CMP_CODE);
        	adDocumentSequence = adDocumentSequenceHome.create("AP RECEIVING ITEM DOCUMENT", new GregorianCalendar(2000, 0, 1, 0, 0, 0).getTime(), new GregorianCalendar(2050, 0, 1, 0, 0, 0).getTime(), 'A', "1", CMP_CODE);
        	adDocumentSequenceAssignment = adDocumentSequenceAssignmentHome.create(new Integer(0), "RI000001", CMP_CODE);
        	adDocumentCategory.addAdDocumentSequenceAssignment(adDocumentSequenceAssignment);
        	adDocumentSequence.addAdDocumentSequenceAssignment(adDocumentSequenceAssignment);
        	adApplicationAp.addAdDocumentSequence(adDocumentSequence);
        	adApplicationAp.addAdDocumentCategory(adDocumentCategory);
        	adDocumentCategory = adDocumentCategoryHome.create("AP VOUCHER", "VOUCHER", CMP_CODE);
        	adDocumentSequence = adDocumentSequenceHome.create("AP VOUCHER DOCUMENT", new GregorianCalendar(2000, 0, 1, 0, 0, 0).getTime(), new GregorianCalendar(2050, 0, 1, 0, 0, 0).getTime(), 'A', "1", CMP_CODE);
        	adDocumentSequenceAssignment = adDocumentSequenceAssignmentHome.create(new Integer(0), "APV000001", CMP_CODE);
        	adDocumentCategory.addAdDocumentSequenceAssignment(adDocumentSequenceAssignment);
        	adDocumentSequence.addAdDocumentSequenceAssignment(adDocumentSequenceAssignment);
        	adApplicationAp.addAdDocumentSequence(adDocumentSequence);
        	adApplicationAp.addAdDocumentCategory(adDocumentCategory);
        	adDocumentCategory = adDocumentCategoryHome.create("AP DEBIT MEMO", "DEBIT MEMO", CMP_CODE);
        	adDocumentSequence = adDocumentSequenceHome.create("AP DEBIT MEMO DOCUMENT", new GregorianCalendar(2000, 0, 1, 0, 0, 0).getTime(), new GregorianCalendar(2050, 0, 1, 0, 0, 0).getTime(), 'A', "1", CMP_CODE);
        	adDocumentSequenceAssignment = adDocumentSequenceAssignmentHome.create(new Integer(0), "DM00001", CMP_CODE);
        	adDocumentCategory.addAdDocumentSequenceAssignment(adDocumentSequenceAssignment);
        	adDocumentSequence.addAdDocumentSequenceAssignment(adDocumentSequenceAssignment);
        	adApplicationAp.addAdDocumentSequence(adDocumentSequence);
        	adApplicationAp.addAdDocumentCategory(adDocumentCategory);
        	adDocumentCategory = adDocumentCategoryHome.create("AP CHECK", "CHECK", CMP_CODE);
        	adDocumentSequence = adDocumentSequenceHome.create("AP CHECK DOCUMENT", new GregorianCalendar(2000, 0, 1, 0, 0, 0).getTime(), new GregorianCalendar(2050, 0, 1, 0, 0, 0).getTime(), 'A', "1", CMP_CODE);
        	adDocumentSequenceAssignment = adDocumentSequenceAssignmentHome.create(new Integer(0), "CV000001", CMP_CODE);
        	adDocumentCategory.addAdDocumentSequenceAssignment(adDocumentSequenceAssignment);
        	adDocumentSequence.addAdDocumentSequenceAssignment(adDocumentSequenceAssignment);
        	adApplicationAp.addAdDocumentSequence(adDocumentSequence);
        	adApplicationAp.addAdDocumentCategory(adDocumentCategory);
        	adDocumentCategory = adDocumentCategoryHome.create("AP CHECK PAYMENT REQUEST", "CHECK PAYMENT REQUEST", CMP_CODE);
        	adDocumentSequence = adDocumentSequenceHome.create("AP CHECK PAYMENT REQUEST DOCUMENT", new GregorianCalendar(2000, 0, 1, 0, 0, 0).getTime(), new GregorianCalendar(2050, 0, 1, 0, 0, 0).getTime(), 'A', "1", CMP_CODE);
        	adDocumentSequenceAssignment = adDocumentSequenceAssignmentHome.create(new Integer(0), "CPR000001", CMP_CODE);
        	adDocumentCategory.addAdDocumentSequenceAssignment(adDocumentSequenceAssignment);
        	adDocumentSequence.addAdDocumentSequenceAssignment(adDocumentSequenceAssignment);
        	adApplicationAp.addAdDocumentSequence(adDocumentSequence);
        	adApplicationAp.addAdDocumentCategory(adDocumentCategory);


        	adDocumentCategory = adDocumentCategoryHome.create("AR SALES ORDER", "SALES ORDER", CMP_CODE);
        	adDocumentSequence = adDocumentSequenceHome.create("AR SALES ORDER DOCUMENT", new GregorianCalendar(2000, 0, 1, 0, 0, 0).getTime(), new GregorianCalendar(2050, 0, 1, 0, 0, 0).getTime(), 'A', "1", CMP_CODE);
        	adDocumentSequenceAssignment = adDocumentSequenceAssignmentHome.create(new Integer(0), "SO000001", CMP_CODE);
        	adDocumentCategory.addAdDocumentSequenceAssignment(adDocumentSequenceAssignment);
        	adDocumentSequence.addAdDocumentSequenceAssignment(adDocumentSequenceAssignment);
        	adApplicationInv.addAdDocumentSequence(adDocumentSequence);
        	adApplicationInv.addAdDocumentCategory(adDocumentCategory);
        	
        	adDocumentCategory = adDocumentCategoryHome.create("AR JOB ORDER", "JOB ORDER", CMP_CODE);
        	adDocumentSequence = adDocumentSequenceHome.create("AR JOB ORDER DOCUMENT", new GregorianCalendar(2000, 0, 1, 0, 0, 0).getTime(), new GregorianCalendar(2050, 0, 1, 0, 0, 0).getTime(), 'A', "1", CMP_CODE);
        	adDocumentSequenceAssignment = adDocumentSequenceAssignmentHome.create(new Integer(0), "JO000001", CMP_CODE);
        	adDocumentCategory.addAdDocumentSequenceAssignment(adDocumentSequenceAssignment);
        	adDocumentSequence.addAdDocumentSequenceAssignment(adDocumentSequenceAssignment);
        	adApplicationInv.addAdDocumentSequence(adDocumentSequence);
        	adApplicationInv.addAdDocumentCategory(adDocumentCategory);
        	
        	
        	adDocumentCategory = adDocumentCategoryHome.create("AR INVOICE", "INVOICE", CMP_CODE);
        	adDocumentSequence = adDocumentSequenceHome.create("AR INVOICE DOCUMENT", new GregorianCalendar(2000, 0, 1, 0, 0, 0).getTime(), new GregorianCalendar(2050, 0, 1, 0, 0, 0).getTime(), 'A', "1", CMP_CODE);
        	adDocumentSequenceAssignment = adDocumentSequenceAssignmentHome.create(new Integer(0), "SI000001", CMP_CODE);
        	adDocumentCategory.addAdDocumentSequenceAssignment(adDocumentSequenceAssignment);
        	adDocumentSequence.addAdDocumentSequenceAssignment(adDocumentSequenceAssignment);
        	adApplicationAr.addAdDocumentSequence(adDocumentSequence);
        	adApplicationAr.addAdDocumentCategory(adDocumentCategory);
        	adDocumentCategory = adDocumentCategoryHome.create("AR CREDIT MEMO", "CREDIT MEMO", CMP_CODE);
        	adDocumentSequence = adDocumentSequenceHome.create("AR CREDIT MEMO DOCUMENT", new GregorianCalendar(2000, 0, 1, 0, 0, 0).getTime(), new GregorianCalendar(2050, 0, 1, 0, 0, 0).getTime(), 'A', "1", CMP_CODE);
        	adDocumentSequenceAssignment = adDocumentSequenceAssignmentHome.create(new Integer(0), "CM000001", CMP_CODE);
        	adDocumentCategory.addAdDocumentSequenceAssignment(adDocumentSequenceAssignment);
        	adDocumentSequence.addAdDocumentSequenceAssignment(adDocumentSequenceAssignment);
        	adApplicationAr.addAdDocumentSequence(adDocumentSequence);
        	adApplicationAr.addAdDocumentCategory(adDocumentCategory);
        	adDocumentCategory = adDocumentCategoryHome.create("AR RECEIPT", "RECEIPT", CMP_CODE);
        	adDocumentSequence = adDocumentSequenceHome.create("AR RECEIPT DOCUMENT", new GregorianCalendar(2000, 0, 1, 0, 0, 0).getTime(), new GregorianCalendar(2050, 0, 1, 0, 0, 0).getTime(), 'A', "1", CMP_CODE);
        	adDocumentSequenceAssignment = adDocumentSequenceAssignmentHome.create(new Integer(0), "OR000001", CMP_CODE);
        	adDocumentCategory.addAdDocumentSequenceAssignment(adDocumentSequenceAssignment);
        	adDocumentSequence.addAdDocumentSequenceAssignment(adDocumentSequenceAssignment);
        	adApplicationAr.addAdDocumentSequence(adDocumentSequence);
        	adApplicationAr.addAdDocumentCategory(adDocumentCategory);
        	adDocumentCategory = adDocumentCategoryHome.create("AR STATEMENT OF ACCOUNT", "STATEMENT OF ACCOUNT", CMP_CODE);
        	adDocumentSequence = adDocumentSequenceHome.create("AR STATEMENT OF ACCOUNT DOCUMENT", new GregorianCalendar(2000, 0, 1, 0, 0, 0).getTime(), new GregorianCalendar(2050, 0, 1, 0, 0, 0).getTime(), 'A', "1", CMP_CODE);
        	adDocumentSequenceAssignment = adDocumentSequenceAssignmentHome.create(new Integer(0), "SOA000001", CMP_CODE);
        	adDocumentCategory.addAdDocumentSequenceAssignment(adDocumentSequenceAssignment);
        	adDocumentSequence.addAdDocumentSequenceAssignment(adDocumentSequenceAssignment);
        	adApplicationAr.addAdDocumentSequence(adDocumentSequence);
        	adApplicationAr.addAdDocumentCategory(adDocumentCategory);
        	adDocumentCategory = adDocumentCategoryHome.create("AR ACKNOWLEDGEMENT RECEIPT", "ACKNOWLEDGEMENT RECEIPT", CMP_CODE);
        	adDocumentSequence = adDocumentSequenceHome.create("AR ACKNOWLEDGEMENT RECEIPT DOCUMENT", new GregorianCalendar(2000, 0, 1, 0, 0, 0).getTime(), new GregorianCalendar(2050, 0, 1, 0, 0, 0).getTime(), 'A', "1", CMP_CODE);
        	adDocumentSequenceAssignment = adDocumentSequenceAssignmentHome.create(new Integer(0), "AR000001", CMP_CODE);
        	adDocumentCategory.addAdDocumentSequenceAssignment(adDocumentSequenceAssignment);
        	adDocumentSequence.addAdDocumentSequenceAssignment(adDocumentSequenceAssignment);
        	adApplicationAr.addAdDocumentSequence(adDocumentSequence);
        	adApplicationAr.addAdDocumentCategory(adDocumentCategory);

        	adDocumentCategory = adDocumentCategoryHome.create("AR DELIVERY", "DELIVERY", CMP_CODE);
        	adDocumentSequence = adDocumentSequenceHome.create("AR DELIVERY DOCUMENT", new GregorianCalendar(2000, 0, 1, 0, 0, 0).getTime(), new GregorianCalendar(2050, 0, 1, 0, 0, 0).getTime(), 'A', "1", CMP_CODE);
        	adDocumentSequenceAssignment = adDocumentSequenceAssignmentHome.create(new Integer(0), "000001", CMP_CODE);
        	adDocumentCategory.addAdDocumentSequenceAssignment(adDocumentSequenceAssignment);
        	adDocumentSequence.addAdDocumentSequenceAssignment(adDocumentSequenceAssignment);
        	adApplicationAr.addAdDocumentSequence(adDocumentSequence);
        	adApplicationAr.addAdDocumentCategory(adDocumentCategory);


        	adDocumentCategory = adDocumentCategoryHome.create("CM ADJUSTMENT", "ADJUSTMENT", CMP_CODE);
        	adDocumentSequence = adDocumentSequenceHome.create("CM ADJUSTMENT DOCUMENT", new GregorianCalendar(2000, 0, 1, 0, 0, 0).getTime(), new GregorianCalendar(2050, 0, 1, 0, 0, 0).getTime(), 'A', "1", CMP_CODE);
        	adDocumentSequenceAssignment = adDocumentSequenceAssignmentHome.create(new Integer(0), "CA000001", CMP_CODE);
        	adDocumentCategory.addAdDocumentSequenceAssignment(adDocumentSequenceAssignment);
        	adDocumentSequence.addAdDocumentSequenceAssignment(adDocumentSequenceAssignment);
        	adApplicationCm.addAdDocumentSequence(adDocumentSequence);
        	adApplicationCm.addAdDocumentCategory(adDocumentCategory);
        	adDocumentCategory = adDocumentCategoryHome.create("CM FUND TRANSFER", "FUND TRANSFER", CMP_CODE);
        	adDocumentSequence = adDocumentSequenceHome.create("CM FUND TRANSFER DOCUMENT", new GregorianCalendar(2000, 0, 1, 0, 0, 0).getTime(), new GregorianCalendar(2050, 0, 1, 0, 0, 0).getTime(), 'A', "1", CMP_CODE);
        	adDocumentSequenceAssignment = adDocumentSequenceAssignmentHome.create(new Integer(0), "FT000001", CMP_CODE);
        	adDocumentCategory.addAdDocumentSequenceAssignment(adDocumentSequenceAssignment);
        	adDocumentSequence.addAdDocumentSequenceAssignment(adDocumentSequenceAssignment);
        	adApplicationCm.addAdDocumentSequence(adDocumentSequence);
        	adApplicationCm.addAdDocumentCategory(adDocumentCategory);


        	adDocumentCategory = adDocumentCategoryHome.create("INV ADJUSTMENT REQUEST", "ADJUSTMENT REQUEST", CMP_CODE);
        	adDocumentSequence = adDocumentSequenceHome.create("INV ADJUSTMENT REQUEST DOCUMENT", new GregorianCalendar(2000, 0, 1, 0, 0, 0).getTime(), new GregorianCalendar(2050, 0, 1, 0, 0, 0).getTime(), 'A', "1", CMP_CODE);
        	adDocumentSequenceAssignment = adDocumentSequenceAssignmentHome.create(new Integer(0), "IAR000001", CMP_CODE);
        	adDocumentCategory.addAdDocumentSequenceAssignment(adDocumentSequenceAssignment);
        	adDocumentSequence.addAdDocumentSequenceAssignment(adDocumentSequenceAssignment);
        	adApplicationInv.addAdDocumentSequence(adDocumentSequence);
        	adApplicationInv.addAdDocumentCategory(adDocumentCategory);
        	adDocumentCategory = adDocumentCategoryHome.create("INV ADJUSTMENT", "ADJUSTMENT", CMP_CODE);
        	adDocumentSequence = adDocumentSequenceHome.create("INV ADJUSTMENT DOCUMENT", new GregorianCalendar(2000, 0, 1, 0, 0, 0).getTime(), new GregorianCalendar(2050, 0, 1, 0, 0, 0).getTime(), 'A', "1", CMP_CODE);
        	adDocumentSequenceAssignment = adDocumentSequenceAssignmentHome.create(new Integer(0), "IA000001", CMP_CODE);
        	adDocumentCategory.addAdDocumentSequenceAssignment(adDocumentSequenceAssignment);
        	adDocumentSequence.addAdDocumentSequenceAssignment(adDocumentSequenceAssignment);
        	adApplicationInv.addAdDocumentSequence(adDocumentSequence);
        	adApplicationInv.addAdDocumentCategory(adDocumentCategory);
        	adDocumentCategory = adDocumentCategoryHome.create("INV BRANCH STOCK TRANSFER ORDER", "BRANCH STOCK TRANSFER ORDER", CMP_CODE);
        	adDocumentSequence = adDocumentSequenceHome.create("INV BRANCH STOCK TRANSFER ORDER DOCUMENT", new GregorianCalendar(2000, 0, 1, 0, 0, 0).getTime(), new GregorianCalendar(2050, 0, 1, 0, 0, 0).getTime(), 'A', "1", CMP_CODE);
        	adDocumentSequenceAssignment = adDocumentSequenceAssignmentHome.create(new Integer(0), "BSOS000001", CMP_CODE);
        	adDocumentCategory.addAdDocumentSequenceAssignment(adDocumentSequenceAssignment);
        	adDocumentSequence.addAdDocumentSequenceAssignment(adDocumentSequenceAssignment);
        	adApplicationInv.addAdDocumentSequence(adDocumentSequence);
        	adApplicationInv.addAdDocumentCategory(adDocumentCategory);
        	adDocumentCategory = adDocumentCategoryHome.create("INV BRANCH STOCK TRANSFER-OUT", "BRANCH STOCK TRANSFER-OUT", CMP_CODE);
        	adDocumentSequence = adDocumentSequenceHome.create("INV BRANCH STOCK TRANSFER-OUT DOCUMENT", new GregorianCalendar(2000, 0, 1, 0, 0, 0).getTime(), new GregorianCalendar(2050, 0, 1, 0, 0, 0).getTime(), 'A', "1", CMP_CODE);
        	adDocumentSequenceAssignment = adDocumentSequenceAssignmentHome.create(new Integer(0), "BSTO000001", CMP_CODE);
        	adDocumentCategory.addAdDocumentSequenceAssignment(adDocumentSequenceAssignment);
        	adDocumentSequence.addAdDocumentSequenceAssignment(adDocumentSequenceAssignment);
        	adApplicationInv.addAdDocumentSequence(adDocumentSequence);
        	adApplicationInv.addAdDocumentCategory(adDocumentCategory);
        	adDocumentCategory = adDocumentCategoryHome.create("INV BRANCH STOCK TRANSFER-IN", "BRANCH STOCK TRANSFER-IN", CMP_CODE);
        	adDocumentSequence = adDocumentSequenceHome.create("INV BRANCH STOCK TRANSFER-IN DOCUMENT", new GregorianCalendar(2000, 0, 1, 0, 0, 0).getTime(), new GregorianCalendar(2050, 0, 1, 0, 0, 0).getTime(), 'A', "1", CMP_CODE);
        	adDocumentSequenceAssignment = adDocumentSequenceAssignmentHome.create(new Integer(0), "BSTI000001", CMP_CODE);
        	adDocumentCategory.addAdDocumentSequenceAssignment(adDocumentSequenceAssignment);
        	adDocumentSequence.addAdDocumentSequenceAssignment(adDocumentSequenceAssignment);
        	adApplicationInv.addAdDocumentSequence(adDocumentSequence);
        	adApplicationInv.addAdDocumentCategory(adDocumentCategory);
        	adDocumentCategory = adDocumentCategoryHome.create("INV STOCK TRANSFER", "STOCK TRANSFER", CMP_CODE);
        	adDocumentSequence = adDocumentSequenceHome.create("INV STOCK TRANSFER DOCUMENT", new GregorianCalendar(2000, 0, 1, 0, 0, 0).getTime(), new GregorianCalendar(2050, 0, 1, 0, 0, 0).getTime(), 'A', "1", CMP_CODE);
        	adDocumentSequenceAssignment = adDocumentSequenceAssignmentHome.create(new Integer(0), "STR000001", CMP_CODE);
        	adDocumentCategory.addAdDocumentSequenceAssignment(adDocumentSequenceAssignment);
        	adDocumentSequence.addAdDocumentSequenceAssignment(adDocumentSequenceAssignment);
        	adApplicationInv.addAdDocumentSequence(adDocumentSequence);
        	adApplicationInv.addAdDocumentCategory(adDocumentCategory);
        	adDocumentCategory = adDocumentCategoryHome.create("INV BUILD ASSEMBLY ORDER", "BUILD ASSEMBLY", CMP_CODE);
        	adDocumentSequence = adDocumentSequenceHome.create("INV BUILD ASSEMBLY ORDER DOCUMENT", new GregorianCalendar(2000, 0, 1, 0, 0, 0).getTime(), new GregorianCalendar(2050, 0, 1, 0, 0, 0).getTime(), 'A', "1", CMP_CODE);
        	adDocumentSequenceAssignment = adDocumentSequenceAssignmentHome.create(new Integer(0), "BO000001", CMP_CODE);
        	adDocumentCategory.addAdDocumentSequenceAssignment(adDocumentSequenceAssignment);
        	adDocumentSequence.addAdDocumentSequenceAssignment(adDocumentSequenceAssignment);
        	adApplicationInv.addAdDocumentSequence(adDocumentSequence);
        	adApplicationInv.addAdDocumentCategory(adDocumentCategory);
        	adDocumentCategory = adDocumentCategoryHome.create("INV BUILD ASSEMBLY", "BUILD ASSEMBLY", CMP_CODE);
        	adDocumentSequence = adDocumentSequenceHome.create("INV BUILD ASSEMBLY DOCUMENT", new GregorianCalendar(2000, 0, 1, 0, 0, 0).getTime(), new GregorianCalendar(2050, 0, 1, 0, 0, 0).getTime(), 'A', "1", CMP_CODE);
        	adDocumentSequenceAssignment = adDocumentSequenceAssignmentHome.create(new Integer(0), "BUA000001", CMP_CODE);
        	adDocumentCategory.addAdDocumentSequenceAssignment(adDocumentSequenceAssignment);
        	adDocumentSequence.addAdDocumentSequenceAssignment(adDocumentSequenceAssignment);
        	adApplicationInv.addAdDocumentSequence(adDocumentSequence);
        	adApplicationInv.addAdDocumentCategory(adDocumentCategory);
        	adDocumentCategory = adDocumentCategoryHome.create("INV OVERHEAD", "OVERHEAD", CMP_CODE);
        	adDocumentSequence = adDocumentSequenceHome.create("INV OVERHEAD DOCUMENT", new GregorianCalendar(2000, 0, 1, 0, 0, 0).getTime(), new GregorianCalendar(2050, 0, 1, 0, 0, 0).getTime(), 'A', "1", CMP_CODE);
        	adDocumentSequenceAssignment = adDocumentSequenceAssignmentHome.create(new Integer(0), "OVH000001", CMP_CODE);
        	adDocumentCategory.addAdDocumentSequenceAssignment(adDocumentSequenceAssignment);
        	adDocumentSequence.addAdDocumentSequenceAssignment(adDocumentSequenceAssignment);
        	adApplicationInv.addAdDocumentSequence(adDocumentSequence);
        	adApplicationInv.addAdDocumentCategory(adDocumentCategory);
        	adDocumentCategory = adDocumentCategoryHome.create("INV BUILD ORDER", "BUILD ORDER", CMP_CODE);
        	adDocumentSequence = adDocumentSequenceHome.create("INV BUILD ORDER DOCUMENT", new GregorianCalendar(2000, 0, 1, 0, 0, 0).getTime(), new GregorianCalendar(2050, 0, 1, 0, 0, 0).getTime(), 'A', "1", CMP_CODE);
        	adDocumentSequenceAssignment = adDocumentSequenceAssignmentHome.create(new Integer(0), "BOR000001", CMP_CODE);
        	adDocumentCategory.addAdDocumentSequenceAssignment(adDocumentSequenceAssignment);
        	adDocumentSequence.addAdDocumentSequenceAssignment(adDocumentSequenceAssignment);
        	adApplicationInv.addAdDocumentSequence(adDocumentSequence);
        	adApplicationInv.addAdDocumentCategory(adDocumentCategory);
        	adDocumentCategory = adDocumentCategoryHome.create("INV STOCK ISSUANCE", "STOCK ISSUANCE", CMP_CODE);
        	adDocumentSequence = adDocumentSequenceHome.create("INV STOCK ISSUANCE DOCUMENT", new GregorianCalendar(2000, 0, 1, 0, 0, 0).getTime(), new GregorianCalendar(2050, 0, 1, 0, 0, 0).getTime(), 'A', "1", CMP_CODE);
        	adDocumentSequenceAssignment = adDocumentSequenceAssignmentHome.create(new Integer(0), "SI000001", CMP_CODE);
        	adDocumentCategory.addAdDocumentSequenceAssignment(adDocumentSequenceAssignment);
        	adDocumentSequence.addAdDocumentSequenceAssignment(adDocumentSequenceAssignment);
        	adApplicationInv.addAdDocumentSequence(adDocumentSequence);
        	adApplicationInv.addAdDocumentCategory(adDocumentCategory);
        	adDocumentCategory = adDocumentCategoryHome.create("INV ASSEMBLY TRANSFER", "ASSEMBLY TRANSFER", CMP_CODE);
        	adDocumentSequence = adDocumentSequenceHome.create("INV ASSEMBLY TRANSFER DOCUMENT", new GregorianCalendar(2000, 0, 1, 0, 0, 0).getTime(), new GregorianCalendar(2050, 0, 1, 0, 0, 0).getTime(), 'A', "1", CMP_CODE);
        	adDocumentSequenceAssignment = adDocumentSequenceAssignmentHome.create(new Integer(0), "ATR000001", CMP_CODE);
        	adDocumentCategory.addAdDocumentSequenceAssignment(adDocumentSequenceAssignment);
        	adDocumentSequence.addAdDocumentSequenceAssignment(adDocumentSequenceAssignment);
        	adApplicationInv.addAdDocumentSequence(adDocumentSequence);
        	adApplicationInv.addAdDocumentCategory(adDocumentCategory);
        	adDocumentCategory = adDocumentCategoryHome.create("INV TRANSACTIONAL BUDGET", "TRANSACTIONAL BUDGET", CMP_CODE);
        	adDocumentSequence = adDocumentSequenceHome.create("INV TRANSACTIONAL BUDGET DOCUMENT", new GregorianCalendar(2000, 0, 1, 0, 0, 0).getTime(), new GregorianCalendar(2050, 0, 1, 0, 0, 0).getTime(), 'A', "1", CMP_CODE);
        	adDocumentSequenceAssignment = adDocumentSequenceAssignmentHome.create(new Integer(0), "TB000001", CMP_CODE);
        	adDocumentCategory.addAdDocumentSequenceAssignment(adDocumentSequenceAssignment);
        	adDocumentSequence.addAdDocumentSequenceAssignment(adDocumentSequenceAssignment);
        	adApplicationInv.addAdDocumentSequence(adDocumentSequence);
        	adApplicationInv.addAdDocumentCategory(adDocumentCategory);

        	// responsibility
        	// branch responsibility
        	LocalAdResponsibility adResponsibility = adResponsibilityHome.create("Super", "Super",  new GregorianCalendar(2000, 0, 1, 0, 0, 0).getTime(), new GregorianCalendar(2050, 0, 1, 0, 0, 0).getTime(), CMP_CODE);
        	LocalAdBranchResponsibility adBranchResponsibility = adBranchResponsibilityHome.create(CMP_CODE);
        	adBranch.addAdBranchResponsibility(adBranchResponsibility);
        	adResponsibility.addAdBranchResponsibility(adBranchResponsibility);

        	LocalAdResponsibility adResponsibilitySysAdmin = adResponsibilityHome.create("Sys Admin", "System Admin",  new GregorianCalendar(2000, 0, 1, 0, 0, 0).getTime(), new GregorianCalendar(2050, 0, 1, 0, 0, 0).getTime(), CMP_CODE);
        	adBranchResponsibility = adBranchResponsibilityHome.create(CMP_CODE);
        	adBranch.addAdBranchResponsibility(adBranchResponsibility);
        	adResponsibilitySysAdmin.addAdBranchResponsibility(adBranchResponsibility);

        	LocalAdResponsibility adResponsibilityAccnt = adResponsibilityHome.create("Accountant", "Accountant",  new GregorianCalendar(2000, 0, 1, 0, 0, 0).getTime(), new GregorianCalendar(2050, 0, 1, 0, 0, 0).getTime(), CMP_CODE);
        	adBranchResponsibility = adBranchResponsibilityHome.create(CMP_CODE);
        	adBranch.addAdBranchResponsibility(adBranchResponsibility);
        	adResponsibilityAccnt.addAdBranchResponsibility(adBranchResponsibility);

        	LocalAdResponsibility adResponsibilityGlClerk = adResponsibilityHome.create("Accounting Clerk", "Accounting Clerk",  new GregorianCalendar(2000, 0, 1, 0, 0, 0).getTime(), new GregorianCalendar(2050, 0, 1, 0, 0, 0).getTime(), CMP_CODE);
        	adBranchResponsibility = adBranchResponsibilityHome.create(CMP_CODE);
        	adBranch.addAdBranchResponsibility(adBranchResponsibility);
        	adResponsibilityGlClerk.addAdBranchResponsibility(adBranchResponsibility);

        	LocalAdResponsibility adResponsibilityApClerk = adResponsibilityHome.create("AP Clerk", "AP Clerk",  new GregorianCalendar(2000, 0, 1, 0, 0, 0).getTime(), new GregorianCalendar(2050, 0, 1, 0, 0, 0).getTime(), CMP_CODE);
        	adBranchResponsibility = adBranchResponsibilityHome.create(CMP_CODE);
        	adBranch.addAdBranchResponsibility(adBranchResponsibility);
        	adResponsibilityApClerk.addAdBranchResponsibility(adBranchResponsibility);

        	LocalAdResponsibility adResponsibilityArClerk = adResponsibilityHome.create("AR Clerk", "AR Clerk",  new GregorianCalendar(2000, 0, 1, 0, 0, 0).getTime(), new GregorianCalendar(2050, 0, 1, 0, 0, 0).getTime(), CMP_CODE);
        	adBranchResponsibility = adBranchResponsibilityHome.create(CMP_CODE);
        	adBranch.addAdBranchResponsibility(adBranchResponsibility);
        	adResponsibilityArClerk.addAdBranchResponsibility(adBranchResponsibility);

        	LocalAdResponsibility adResponsibilityKeeper = adResponsibilityHome.create("Bookkeeper", "Bookkeeper",  new GregorianCalendar(2000, 0, 1, 0, 0, 0).getTime(), new GregorianCalendar(2050, 0, 1, 0, 0, 0).getTime(), CMP_CODE);
        	adBranchResponsibility = adBranchResponsibilityHome.create(CMP_CODE);
        	adBranch.addAdBranchResponsibility(adBranchResponsibility);
        	adResponsibilityKeeper.addAdBranchResponsibility(adBranchResponsibility);

        	LocalAdResponsibility adResponsibilityFinance = adResponsibilityHome.create("Finance Officer", "Finance Officer",  new GregorianCalendar(2000, 0, 1, 0, 0, 0).getTime(), new GregorianCalendar(2050, 0, 1, 0, 0, 0).getTime(), CMP_CODE);
        	adBranchResponsibility = adBranchResponsibilityHome.create(CMP_CODE);
        	adBranch.addAdBranchResponsibility(adBranchResponsibility);
        	adResponsibilityFinance.addAdBranchResponsibility(adBranchResponsibility);

        	// form function

        	LocalAdFormFunction adFormFunction = adFormFunctionHome.findByPrimaryKey(new Integer(1));
        	ArrayList list = new ArrayList();
        	list.add(adResponsibility);
        	list.add(adResponsibilityAccnt);
        	list.add(adResponsibilityGlClerk);
        	list.add(adResponsibilityKeeper);
        	list.add(adResponsibilityFinance);
        	this.setFormFunctionResponsibility(list, adFormFunction, CMP_CODE); System.out.println("1");

        	adFormFunction = adFormFunctionHome.findByPrimaryKey(new Integer(2));
        	list = new ArrayList();
        	list.add(adResponsibility);
        	list.add(adResponsibilityAccnt);
        	list.add(adResponsibilityGlClerk);
        	list.add(adResponsibilityKeeper);
        	list.add(adResponsibilityFinance);
        	this.setFormFunctionResponsibility(list, adFormFunction, CMP_CODE); System.out.println("2");

        	adFormFunction = adFormFunctionHome.findByPrimaryKey(new Integer(3));
        	list = new ArrayList();
        	list.add(adResponsibility);
        	list.add(adResponsibilityAccnt);
        	list.add(adResponsibilityGlClerk);
        	list.add(adResponsibilityKeeper);
        	list.add(adResponsibilityFinance);
        	this.setFormFunctionResponsibility(list, adFormFunction, CMP_CODE); System.out.println("3");

			adFormFunction = adFormFunctionHome.findByPrimaryKey(new Integer(4));
			list = new ArrayList();
        	list.add(adResponsibility);
        	list.add(adResponsibilityAccnt);
        	list.add(adResponsibilityGlClerk);
        	list.add(adResponsibilityKeeper);
        	list.add(adResponsibilityFinance);
        	this.setFormFunctionResponsibility(list, adFormFunction, CMP_CODE); System.out.println("4");

			adFormFunction = adFormFunctionHome.findByPrimaryKey(new Integer(5));
			list = new ArrayList();
        	list.add(adResponsibility);
        	list.add(adResponsibilityAccnt);
        	list.add(adResponsibilityGlClerk);
        	list.add(adResponsibilityKeeper);
        	list.add(adResponsibilityFinance);
        	this.setFormFunctionResponsibility(list, adFormFunction, CMP_CODE); System.out.println("5");

			adFormFunction = adFormFunctionHome.findByPrimaryKey(new Integer(6));
			list = new ArrayList();
        	list.add(adResponsibility);
        	list.add(adResponsibilityAccnt);
        	list.add(adResponsibilityGlClerk);
        	list.add(adResponsibilityKeeper);
        	list.add(adResponsibilityFinance);
        	this.setFormFunctionResponsibility(list, adFormFunction, CMP_CODE); System.out.println("6");

			adFormFunction = adFormFunctionHome.findByPrimaryKey(new Integer(7));
			list = new ArrayList();
        	list.add(adResponsibility);
        	list.add(adResponsibilityKeeper);
        	list.add(adResponsibilityFinance);
        	this.setFormFunctionResponsibility(list, adFormFunction, CMP_CODE); System.out.println("7");

			adFormFunction = adFormFunctionHome.findByPrimaryKey(new Integer(8));
			list = new ArrayList();
        	list.add(adResponsibility);
        	list.add(adResponsibilityAccnt);
        	list.add(adResponsibilityGlClerk);
        	list.add(adResponsibilityKeeper);
        	list.add(adResponsibilityFinance);
        	this.setFormFunctionResponsibility(list, adFormFunction, CMP_CODE); System.out.println("8");

			adFormFunction = adFormFunctionHome.findByPrimaryKey(new Integer(9));
			list = new ArrayList();
        	list.add(adResponsibility);
        	list.add(adResponsibilityAccnt);
        	list.add(adResponsibilityGlClerk);
        	list.add(adResponsibilityKeeper);
        	list.add(adResponsibilityFinance);
        	this.setFormFunctionResponsibility(list, adFormFunction, CMP_CODE); System.out.println("9");

			adFormFunction = adFormFunctionHome.findByPrimaryKey(new Integer(10));
			list = new ArrayList();
        	list.add(adResponsibility);
        	list.add(adResponsibilityAccnt);
        	list.add(adResponsibilityGlClerk);
        	list.add(adResponsibilityKeeper);
        	list.add(adResponsibilityFinance);
        	this.setFormFunctionResponsibility(list, adFormFunction, CMP_CODE); System.out.println("10");

			adFormFunction = adFormFunctionHome.findByPrimaryKey(new Integer(11));
			list = new ArrayList();
        	list.add(adResponsibility);
        	list.add(adResponsibilityAccnt);
        	list.add(adResponsibilityGlClerk);
        	list.add(adResponsibilityKeeper);
        	list.add(adResponsibilityFinance);
        	this.setFormFunctionResponsibility(list, adFormFunction, CMP_CODE); System.out.println("11");

			adFormFunction = adFormFunctionHome.findByPrimaryKey(new Integer(12));
			list = new ArrayList();
        	list.add(adResponsibility);
        	list.add(adResponsibilityAccnt);
        	list.add(adResponsibilityGlClerk);
        	list.add(adResponsibilityKeeper);
        	list.add(adResponsibilityFinance);
        	this.setFormFunctionResponsibility(list, adFormFunction, CMP_CODE);

			adFormFunction = adFormFunctionHome.findByPrimaryKey(new Integer(13));
			list = new ArrayList();
        	list.add(adResponsibility);
        	list.add(adResponsibilityAccnt);
        	list.add(adResponsibilityGlClerk);
        	list.add(adResponsibilityKeeper);
        	list.add(adResponsibilityFinance);
        	this.setFormFunctionResponsibility(list, adFormFunction, CMP_CODE);

			adFormFunction = adFormFunctionHome.findByPrimaryKey(new Integer(14));
			list = new ArrayList();
        	list.add(adResponsibility);
        	list.add(adResponsibilityAccnt);
        	list.add(adResponsibilityGlClerk);
        	list.add(adResponsibilityKeeper);
        	list.add(adResponsibilityFinance);
        	this.setFormFunctionResponsibility(list, adFormFunction, CMP_CODE);

			adFormFunction = adFormFunctionHome.findByPrimaryKey(new Integer(15));
			list = new ArrayList();
        	list.add(adResponsibility);
        	list.add(adResponsibilityAccnt);
        	list.add(adResponsibilityGlClerk);
        	list.add(adResponsibilityKeeper);
        	list.add(adResponsibilityFinance);
        	this.setFormFunctionResponsibility(list, adFormFunction, CMP_CODE);

			adFormFunction = adFormFunctionHome.findByPrimaryKey(new Integer(22));
			list = new ArrayList();
        	list.add(adResponsibility);
        	list.add(adResponsibilityFinance);
        	this.setFormFunctionResponsibility(list, adFormFunction, CMP_CODE);

			adFormFunction = adFormFunctionHome.findByPrimaryKey(new Integer(23));
			list = new ArrayList();
        	list.add(adResponsibility);
        	list.add(adResponsibilityFinance);
        	this.setFormFunctionResponsibility(list, adFormFunction, CMP_CODE);

			adFormFunction = adFormFunctionHome.findByPrimaryKey(new Integer(24));
			list = new ArrayList();
        	list.add(adResponsibility);
        	list.add(adResponsibilityFinance);
        	this.setFormFunctionResponsibility(list, adFormFunction, CMP_CODE);

			adFormFunction = adFormFunctionHome.findByPrimaryKey(new Integer(25));
			list = new ArrayList();
        	list.add(adResponsibility);
        	list.add(adResponsibilityFinance);
        	this.setFormFunctionResponsibility(list, adFormFunction, CMP_CODE);

			adFormFunction = adFormFunctionHome.findByPrimaryKey(new Integer(27));
			list = new ArrayList();
        	list.add(adResponsibility);
        	list.add(adResponsibilityFinance);
        	this.setFormFunctionResponsibility(list, adFormFunction, CMP_CODE);

			adFormFunction = adFormFunctionHome.findByPrimaryKey(new Integer(33));
			list = new ArrayList();
        	list.add(adResponsibility);
        	list.add(adResponsibilityFinance);
        	this.setFormFunctionResponsibility(list, adFormFunction, CMP_CODE);

			adFormFunction = adFormFunctionHome.findByPrimaryKey(new Integer(34));
			list = new ArrayList();
        	list.add(adResponsibility);
        	list.add(adResponsibilityFinance);
        	this.setFormFunctionResponsibility(list, adFormFunction, CMP_CODE);

			adFormFunction = adFormFunctionHome.findByPrimaryKey(new Integer(35));
			list = new ArrayList();
        	list.add(adResponsibility);
        	list.add(adResponsibilityFinance);
        	this.setFormFunctionResponsibility(list, adFormFunction, CMP_CODE);

			adFormFunction = adFormFunctionHome.findByPrimaryKey(new Integer(37));
			list = new ArrayList();
        	list.add(adResponsibility);
        	list.add(adResponsibilityAccnt);
        	list.add(adResponsibilityGlClerk);
        	list.add(adResponsibilityKeeper);
        	list.add(adResponsibilityFinance);
        	this.setFormFunctionResponsibility(list, adFormFunction, CMP_CODE);

			adFormFunction = adFormFunctionHome.findByPrimaryKey(new Integer(42));
			list = new ArrayList();
        	list.add(adResponsibility);
        	list.add(adResponsibilityFinance);
        	this.setFormFunctionResponsibility(list, adFormFunction, CMP_CODE);

			adFormFunction = adFormFunctionHome.findByPrimaryKey(new Integer(43));
			list = new ArrayList();
        	list.add(adResponsibility);
        	list.add(adResponsibilityFinance);
        	this.setFormFunctionResponsibility(list, adFormFunction, CMP_CODE);

			adFormFunction = adFormFunctionHome.findByPrimaryKey(new Integer(45));
			list = new ArrayList();
        	list.add(adResponsibility);
        	list.add(adResponsibilityFinance);
        	this.setFormFunctionResponsibility(list, adFormFunction, CMP_CODE);

			adFormFunction = adFormFunctionHome.findByPrimaryKey(new Integer(46));
			list = new ArrayList();
        	list.add(adResponsibility);
        	list.add(adResponsibilityFinance);
        	this.setFormFunctionResponsibility(list, adFormFunction, CMP_CODE);

			adFormFunction = adFormFunctionHome.findByPrimaryKey(new Integer(47));
			list = new ArrayList();
        	list.add(adResponsibility);
        	list.add(adResponsibilityFinance);
        	this.setFormFunctionResponsibility(list, adFormFunction, CMP_CODE);

			adFormFunction = adFormFunctionHome.findByPrimaryKey(new Integer(53));
			list = new ArrayList();
        	list.add(adResponsibility);
        	list.add(adResponsibilityFinance);
        	this.setFormFunctionResponsibility(list, adFormFunction, CMP_CODE);

			adFormFunction = adFormFunctionHome.findByPrimaryKey(new Integer(54));
			list = new ArrayList();
        	list.add(adResponsibility);
        	list.add(adResponsibilityFinance);
        	this.setFormFunctionResponsibility(list, adFormFunction, CMP_CODE);

			adFormFunction = adFormFunctionHome.findByPrimaryKey(new Integer(55));
			list = new ArrayList();
        	list.add(adResponsibility);
        	list.add(adResponsibilityFinance);
        	this.setFormFunctionResponsibility(list, adFormFunction, CMP_CODE);

			adFormFunction = adFormFunctionHome.findByPrimaryKey(new Integer(56));
			list = new ArrayList();
        	list.add(adResponsibility);
        	list.add(adResponsibilityFinance);
        	this.setFormFunctionResponsibility(list, adFormFunction, CMP_CODE);

			adFormFunction = adFormFunctionHome.findByPrimaryKey(new Integer(57));
			list = new ArrayList();
        	list.add(adResponsibility);
        	list.add(adResponsibilityFinance);
        	this.setFormFunctionResponsibility(list, adFormFunction, CMP_CODE);

			adFormFunction = adFormFunctionHome.findByPrimaryKey(new Integer(58));
			list = new ArrayList();
        	list.add(adResponsibility);
        	list.add(adResponsibilityFinance);
        	this.setFormFunctionResponsibility(list, adFormFunction, CMP_CODE);

			adFormFunction = adFormFunctionHome.findByPrimaryKey(new Integer(59));
			list = new ArrayList();
        	list.add(adResponsibility);
        	list.add(adResponsibilityFinance);
        	this.setFormFunctionResponsibility(list, adFormFunction, CMP_CODE);

			adFormFunction = adFormFunctionHome.findByPrimaryKey(new Integer(60));
			list = new ArrayList();
        	list.add(adResponsibility);
        	list.add(adResponsibilityFinance);
        	this.setFormFunctionResponsibility(list, adFormFunction, CMP_CODE);

			adFormFunction = adFormFunctionHome.findByPrimaryKey(new Integer(61));
			list = new ArrayList();
        	list.add(adResponsibility);
        	list.add(adResponsibilityFinance);
        	this.setFormFunctionResponsibility(list, adFormFunction, CMP_CODE);

			adFormFunction = adFormFunctionHome.findByPrimaryKey(new Integer(62));
			list = new ArrayList();
        	list.add(adResponsibility);
        	list.add(adResponsibilityFinance);
        	this.setFormFunctionResponsibility(list, adFormFunction, CMP_CODE);

			adFormFunction = adFormFunctionHome.findByPrimaryKey(new Integer(63));
			list = new ArrayList();
        	list.add(adResponsibility);
        	list.add(adResponsibilityFinance);
        	this.setFormFunctionResponsibility(list, adFormFunction, CMP_CODE);

			adFormFunction = adFormFunctionHome.findByPrimaryKey(new Integer(64));
			list = new ArrayList();
        	list.add(adResponsibility);
        	list.add(adResponsibilityFinance);
        	this.setFormFunctionResponsibility(list, adFormFunction, CMP_CODE);

			adFormFunction = adFormFunctionHome.findByPrimaryKey(new Integer(65));
			list = new ArrayList();
        	list.add(adResponsibility);
        	list.add(adResponsibilityFinance);
        	this.setFormFunctionResponsibility(list, adFormFunction, CMP_CODE);

			adFormFunction = adFormFunctionHome.findByPrimaryKey(new Integer(66));
			list = new ArrayList();
        	list.add(adResponsibility);
        	list.add(adResponsibilityFinance);
        	this.setFormFunctionResponsibility(list, adFormFunction, CMP_CODE);

			adFormFunction = adFormFunctionHome.findByPrimaryKey(new Integer(67));
			list = new ArrayList();
        	list.add(adResponsibility);
        	list.add(adResponsibilityFinance);
        	this.setFormFunctionResponsibility(list, adFormFunction, CMP_CODE);

			adFormFunction = adFormFunctionHome.findByPrimaryKey(new Integer(68));
			list = new ArrayList();
        	list.add(adResponsibility);
        	list.add(adResponsibilityFinance);
        	this.setFormFunctionResponsibility(list, adFormFunction, CMP_CODE);

			adFormFunction = adFormFunctionHome.findByPrimaryKey(new Integer(69));
			list = new ArrayList();
        	list.add(adResponsibility);
        	list.add(adResponsibilityFinance);
        	this.setFormFunctionResponsibility(list, adFormFunction, CMP_CODE);

        	adFormFunction = adFormFunctionHome.findByPrimaryKey(new Integer(70));
        	list = new ArrayList();
        	list.add(adResponsibility);
        	list.add(adResponsibilityFinance);
        	this.setFormFunctionResponsibility(list, adFormFunction, CMP_CODE);

        	adFormFunction = adFormFunctionHome.findByPrimaryKey(new Integer(71));
        	list = new ArrayList();
        	list.add(adResponsibility);
        	list.add(adResponsibilityFinance);
        	this.setFormFunctionResponsibility(list, adFormFunction, CMP_CODE);

        	adFormFunction = adFormFunctionHome.findByPrimaryKey(new Integer(72));
        	list = new ArrayList();
        	list.add(adResponsibility);
        	list.add(adResponsibilityFinance);
        	this.setFormFunctionResponsibility(list, adFormFunction, CMP_CODE);

        	adFormFunction = adFormFunctionHome.findByPrimaryKey(new Integer(73));
        	list = new ArrayList();
        	list.add(adResponsibility);
        	list.add(adResponsibilityFinance);
        	this.setFormFunctionResponsibility(list, adFormFunction, CMP_CODE);

        	adFormFunction = adFormFunctionHome.findByPrimaryKey(new Integer(74));
        	list = new ArrayList();
        	list.add(adResponsibility);
        	list.add(adResponsibilityFinance);
        	this.setFormFunctionResponsibility(list, adFormFunction, CMP_CODE);

        	adFormFunction = adFormFunctionHome.findByPrimaryKey(new Integer(75));
        	list = new ArrayList();
        	list.add(adResponsibility);
        	list.add(adResponsibilityFinance);
        	this.setFormFunctionResponsibility(list, adFormFunction, CMP_CODE);

        	adFormFunction = adFormFunctionHome.findByPrimaryKey(new Integer(76));
        	list = new ArrayList();
        	list.add(adResponsibility);
        	list.add(adResponsibilityFinance);
        	this.setFormFunctionResponsibility(list, adFormFunction, CMP_CODE);

        	adFormFunction = adFormFunctionHome.findByPrimaryKey(new Integer(501));
			list = new ArrayList();
        	list.add(adResponsibility);
        	list.add(adResponsibilityAccnt);
        	list.add(adResponsibilityGlClerk);
        	list.add(adResponsibilityFinance);
        	this.setFormFunctionResponsibility(list, adFormFunction, CMP_CODE);

			adFormFunction = adFormFunctionHome.findByPrimaryKey(new Integer(502));
			list = new ArrayList();
        	list.add(adResponsibility);
        	list.add(adResponsibilityAccnt);
        	list.add(adResponsibilityGlClerk);
        	list.add(adResponsibilityFinance);
        	this.setFormFunctionResponsibility(list, adFormFunction, CMP_CODE);

			adFormFunction = adFormFunctionHome.findByPrimaryKey(new Integer(503));
			list = new ArrayList();
        	list.add(adResponsibility);
        	list.add(adResponsibilityAccnt);
        	list.add(adResponsibilityGlClerk);
        	list.add(adResponsibilityFinance);
        	this.setFormFunctionResponsibility(list, adFormFunction, CMP_CODE);

			adFormFunction = adFormFunctionHome.findByPrimaryKey(new Integer(504));
			list = new ArrayList();
        	list.add(adResponsibility);
        	list.add(adResponsibilityAccnt);
        	list.add(adResponsibilityGlClerk);
        	list.add(adResponsibilityFinance);
        	this.setFormFunctionResponsibility(list, adFormFunction, CMP_CODE);

			adFormFunction = adFormFunctionHome.findByPrimaryKey(new Integer(505));
			list = new ArrayList();
        	list.add(adResponsibility);
        	list.add(adResponsibilityAccnt);
        	list.add(adResponsibilityGlClerk);
        	list.add(adResponsibilityFinance);
        	this.setFormFunctionResponsibility(list, adFormFunction, CMP_CODE);

			adFormFunction = adFormFunctionHome.findByPrimaryKey(new Integer(506));
			list = new ArrayList();
        	list.add(adResponsibility);
        	list.add(adResponsibilityAccnt);
        	list.add(adResponsibilityGlClerk);
        	list.add(adResponsibilityFinance);
        	this.setFormFunctionResponsibility(list, adFormFunction, CMP_CODE);

			adFormFunction = adFormFunctionHome.findByPrimaryKey(new Integer(507));
			list = new ArrayList();
        	list.add(adResponsibility);
        	list.add(adResponsibilityAccnt);
        	list.add(adResponsibilityGlClerk);
        	list.add(adResponsibilityFinance);
        	this.setFormFunctionResponsibility(list, adFormFunction, CMP_CODE);

			adFormFunction = adFormFunctionHome.findByPrimaryKey(new Integer(508));
			list = new ArrayList();
        	list.add(adResponsibility);
        	list.add(adResponsibilityAccnt);
        	list.add(adResponsibilityGlClerk);
        	list.add(adResponsibilityFinance);
        	this.setFormFunctionResponsibility(list, adFormFunction, CMP_CODE);

			adFormFunction = adFormFunctionHome.findByPrimaryKey(new Integer(509));
			list = new ArrayList();
        	list.add(adResponsibility);
        	list.add(adResponsibilityAccnt);
        	list.add(adResponsibilityGlClerk);
        	list.add(adResponsibilityFinance);
        	this.setFormFunctionResponsibility(list, adFormFunction, CMP_CODE);

			adFormFunction = adFormFunctionHome.findByPrimaryKey(new Integer(510));
			list = new ArrayList();
        	list.add(adResponsibility);
        	list.add(adResponsibilityAccnt);
        	list.add(adResponsibilityGlClerk);
        	list.add(adResponsibilityFinance);
        	this.setFormFunctionResponsibility(list, adFormFunction, CMP_CODE);

			adFormFunction = adFormFunctionHome.findByPrimaryKey(new Integer(511));
			list = new ArrayList();
        	list.add(adResponsibility);
        	list.add(adResponsibilityAccnt);
        	list.add(adResponsibilityGlClerk);
        	list.add(adResponsibilityKeeper);
        	list.add(adResponsibilityFinance);
        	this.setFormFunctionResponsibility(list, adFormFunction, CMP_CODE);

        	adFormFunction = adFormFunctionHome.findByPrimaryKey(new Integer(512));
        	list = new ArrayList();
        	list.add(adResponsibility);
        	list.add(adResponsibilityAccnt);
        	list.add(adResponsibilityGlClerk);
        	list.add(adResponsibilityFinance);
        	this.setFormFunctionResponsibility(list, adFormFunction, CMP_CODE);

			adFormFunction = adFormFunctionHome.findByPrimaryKey(new Integer(513));
			list = new ArrayList();
        	list.add(adResponsibility);
        	list.add(adResponsibilityAccnt);
        	list.add(adResponsibilityGlClerk);
        	list.add(adResponsibilityFinance);
        	this.setFormFunctionResponsibility(list, adFormFunction, CMP_CODE);

			adFormFunction = adFormFunctionHome.findByPrimaryKey(new Integer(515));
			list = new ArrayList();
        	list.add(adResponsibility);
        	list.add(adResponsibilityAccnt);
        	list.add(adResponsibilityGlClerk);
        	list.add(adResponsibilityFinance);
        	this.setFormFunctionResponsibility(list, adFormFunction, CMP_CODE);

        	adFormFunction = adFormFunctionHome.findByPrimaryKey(new Integer(516));
			list = new ArrayList();
        	list.add(adResponsibility);
        	list.add(adResponsibilityAccnt);
        	list.add(adResponsibilityGlClerk);
        	list.add(adResponsibilityFinance);
        	this.setFormFunctionResponsibility(list, adFormFunction, CMP_CODE);

        	adFormFunction = adFormFunctionHome.findByPrimaryKey(new Integer(517));
			list = new ArrayList();
        	list.add(adResponsibility);
        	list.add(adResponsibilityAccnt);
        	list.add(adResponsibilityGlClerk);
        	list.add(adResponsibilityFinance);
        	this.setFormFunctionResponsibility(list, adFormFunction, CMP_CODE);

        	adFormFunction = adFormFunctionHome.findByPrimaryKey(new Integer(518));
			list = new ArrayList();
        	list.add(adResponsibility);
        	list.add(adResponsibilityAccnt);
        	list.add(adResponsibilityGlClerk);
        	list.add(adResponsibilityFinance);
        	this.setFormFunctionResponsibility(list, adFormFunction, CMP_CODE);

        	adFormFunction = adFormFunctionHome.findByPrimaryKey(new Integer(519));
        	list = new ArrayList();
        	list.add(adResponsibility);
        	list.add(adResponsibilityAccnt);
        	list.add(adResponsibilityKeeper);
        	list.add(adResponsibilityFinance);
        	this.setFormFunctionResponsibility(list, adFormFunction, CMP_CODE);



        	adFormFunction = adFormFunctionHome.findByPrimaryKey(new Integer(1002));
			list = new ArrayList();
        	list.add(adResponsibility);
        	list.add(adResponsibilitySysAdmin);
        	this.setFormFunctionResponsibility(list, adFormFunction, CMP_CODE);

			adFormFunction = adFormFunctionHome.findByPrimaryKey(new Integer(1003));
			list = new ArrayList();
        	list.add(adResponsibility);
        	list.add(adResponsibilitySysAdmin);
        	this.setFormFunctionResponsibility(list, adFormFunction, CMP_CODE);

			adFormFunction = adFormFunctionHome.findByPrimaryKey(new Integer(1004));
			list = new ArrayList();
        	list.add(adResponsibility);
        	list.add(adResponsibilityFinance);
        	this.setFormFunctionResponsibility(list, adFormFunction, CMP_CODE);

			adFormFunction = adFormFunctionHome.findByPrimaryKey(new Integer(1005));
			list = new ArrayList();
        	list.add(adResponsibility);
        	list.add(adResponsibilityFinance);
        	this.setFormFunctionResponsibility(list, adFormFunction, CMP_CODE);

			adFormFunction = adFormFunctionHome.findByPrimaryKey(new Integer(1006));
			list = new ArrayList();
        	list.add(adResponsibility);
        	list.add(adResponsibilitySysAdmin);
        	this.setFormFunctionResponsibility(list, adFormFunction, CMP_CODE);

			adFormFunction = adFormFunctionHome.findByPrimaryKey(new Integer(1007));
			list = new ArrayList();
        	list.add(adResponsibility);
        	list.add(adResponsibilitySysAdmin);
        	this.setFormFunctionResponsibility(list, adFormFunction, CMP_CODE);

			adFormFunction = adFormFunctionHome.findByPrimaryKey(new Integer(1008));
			list = new ArrayList();
        	list.add(adResponsibility);
        	this.setFormFunctionResponsibility(list, adFormFunction, CMP_CODE);

			adFormFunction = adFormFunctionHome.findByPrimaryKey(new Integer(1009));
			list = new ArrayList();
        	list.add(adResponsibility);
        	this.setFormFunctionResponsibility(list, adFormFunction, CMP_CODE);

			adFormFunction = adFormFunctionHome.findByPrimaryKey(new Integer(1010));
			list = new ArrayList();
        	list.add(adResponsibility);
        	list.add(adResponsibilityFinance);
        	this.setFormFunctionResponsibility(list, adFormFunction, CMP_CODE);

			adFormFunction = adFormFunctionHome.findByPrimaryKey(new Integer(1011));
			list = new ArrayList();
        	list.add(adResponsibility);
        	list.add(adResponsibilityFinance);
        	this.setFormFunctionResponsibility(list, adFormFunction, CMP_CODE);

			adFormFunction = adFormFunctionHome.findByPrimaryKey(new Integer(1012));
			list = new ArrayList();
        	list.add(adResponsibility);
        	list.add(adResponsibilityFinance);
        	this.setFormFunctionResponsibility(list, adFormFunction, CMP_CODE);

			adFormFunction = adFormFunctionHome.findByPrimaryKey(new Integer(1013));
			list = new ArrayList();
        	list.add(adResponsibility);
        	list.add(adResponsibilitySysAdmin);
        	this.setFormFunctionResponsibility(list, adFormFunction, CMP_CODE);

			adFormFunction = adFormFunctionHome.findByPrimaryKey(new Integer(1014));
			list = new ArrayList();
        	list.add(adResponsibility);
        	list.add(adResponsibilitySysAdmin);
        	this.setFormFunctionResponsibility(list, adFormFunction, CMP_CODE);

			adFormFunction = adFormFunctionHome.findByPrimaryKey(new Integer(1015));
			list = new ArrayList();
        	list.add(adResponsibility);
        	list.add(adResponsibilitySysAdmin);
        	this.setFormFunctionResponsibility(list, adFormFunction, CMP_CODE);

			adFormFunction = adFormFunctionHome.findByPrimaryKey(new Integer(1016));
			list = new ArrayList();
        	list.add(adResponsibility);
        	list.add(adResponsibilitySysAdmin);
        	this.setFormFunctionResponsibility(list, adFormFunction, CMP_CODE);

			adFormFunction = adFormFunctionHome.findByPrimaryKey(new Integer(1017));
			list = new ArrayList();
        	list.add(adResponsibility);
        	list.add(adResponsibilitySysAdmin);
        	this.setFormFunctionResponsibility(list, adFormFunction, CMP_CODE);

			adFormFunction = adFormFunctionHome.findByPrimaryKey(new Integer(1018));
			list = new ArrayList();
        	list.add(adResponsibility);
        	list.add(adResponsibilityFinance);
        	this.setFormFunctionResponsibility(list, adFormFunction, CMP_CODE);

			adFormFunction = adFormFunctionHome.findByPrimaryKey(new Integer(1019));
			list = new ArrayList();
        	list.add(adResponsibility);
        	list.add(adResponsibilitySysAdmin);
        	list.add(adResponsibilityAccnt);
        	list.add(adResponsibilityGlClerk);
        	list.add(adResponsibilityApClerk);
        	list.add(adResponsibilityArClerk);
        	list.add(adResponsibilityKeeper);
        	list.add(adResponsibilityFinance);
        	this.setFormFunctionResponsibility(list, adFormFunction, CMP_CODE);

			adFormFunction = adFormFunctionHome.findByPrimaryKey(new Integer(1020));
			list = new ArrayList();
        	list.add(adResponsibility);
        	list.add(adResponsibilitySysAdmin);
        	this.setFormFunctionResponsibility(list, adFormFunction, CMP_CODE);

			adFormFunction = adFormFunctionHome.findByPrimaryKey(new Integer(1021));
			list = new ArrayList();
        	list.add(adResponsibility);
        	list.add(adResponsibilityFinance);
        	this.setFormFunctionResponsibility(list, adFormFunction, CMP_CODE);

			adFormFunction = adFormFunctionHome.findByPrimaryKey(new Integer(1022));
			list = new ArrayList();
        	list.add(adResponsibility);
        	list.add(adResponsibilityFinance);
        	this.setFormFunctionResponsibility(list, adFormFunction, CMP_CODE);

			adFormFunction = adFormFunctionHome.findByPrimaryKey(new Integer(1023));
			list = new ArrayList();
        	list.add(adResponsibility);
        	list.add(adResponsibilityFinance);
        	this.setFormFunctionResponsibility(list, adFormFunction, CMP_CODE);

			adFormFunction = adFormFunctionHome.findByPrimaryKey(new Integer(1024));
			list = new ArrayList();
        	list.add(adResponsibility);
        	list.add(adResponsibilityFinance);
        	this.setFormFunctionResponsibility(list, adFormFunction, CMP_CODE);

			adFormFunction = adFormFunctionHome.findByPrimaryKey(new Integer(1025));
			list = new ArrayList();
        	list.add(adResponsibility);
        	list.add(adResponsibilityFinance);
        	this.setFormFunctionResponsibility(list, adFormFunction, CMP_CODE);

			adFormFunction = adFormFunctionHome.findByPrimaryKey(new Integer(1026));
			list = new ArrayList();
        	list.add(adResponsibility);
        	list.add(adResponsibilitySysAdmin);
        	this.setFormFunctionResponsibility(list, adFormFunction, CMP_CODE);

        	adFormFunction = adFormFunctionHome.findByPrimaryKey(new Integer(1027));
			list = new ArrayList();
        	list.add(adResponsibility);
        	list.add(adResponsibilitySysAdmin);
        	this.setFormFunctionResponsibility(list, adFormFunction, CMP_CODE);

        	adFormFunction = adFormFunctionHome.findByPrimaryKey(new Integer(1501));
			list = new ArrayList();
        	list.add(adResponsibility);
        	list.add(adResponsibilitySysAdmin);
        	this.setFormFunctionResponsibility(list, adFormFunction, CMP_CODE);

        	adFormFunction = adFormFunctionHome.findByPrimaryKey(new Integer(1502));
			list = new ArrayList();
        	list.add(adResponsibility);
        	list.add(adResponsibilityAccnt);
        	list.add(adResponsibilityFinance);
        	this.setFormFunctionResponsibility(list, adFormFunction, CMP_CODE);

        	adFormFunction = adFormFunctionHome.findByPrimaryKey(new Integer(1503));
			list = new ArrayList();
        	list.add(adResponsibility);
        	list.add(adResponsibilityAccnt);
        	list.add(adResponsibilityGlClerk);
        	list.add(adResponsibilityFinance);
        	this.setFormFunctionResponsibility(list, adFormFunction, CMP_CODE);

        	adFormFunction = adFormFunctionHome.findByPrimaryKey(new Integer(1504));
        	list = new ArrayList();
        	list.add(adResponsibility);
        	list.add(adResponsibilitySysAdmin);
        	this.setFormFunctionResponsibility(list, adFormFunction, CMP_CODE);

        	adFormFunction = adFormFunctionHome.findByPrimaryKey(new Integer(1505));
        	list = new ArrayList();
        	list.add(adResponsibility);
        	list.add(adResponsibilitySysAdmin);
        	this.setFormFunctionResponsibility(list, adFormFunction, CMP_CODE);

        	adFormFunction = adFormFunctionHome.findByPrimaryKey(new Integer(1506));
        	list = new ArrayList();
        	list.add(adResponsibility);
        	list.add(adResponsibilitySysAdmin);
        	this.setFormFunctionResponsibility(list, adFormFunction, CMP_CODE);

        	adFormFunction = adFormFunctionHome.findByPrimaryKey(new Integer(1507));
        	list = new ArrayList();
        	list.add(adResponsibility);
        	list.add(adResponsibilitySysAdmin);
        	this.setFormFunctionResponsibility(list, adFormFunction, CMP_CODE);

        	adFormFunction = adFormFunctionHome.findByPrimaryKey(new Integer(1508));
        	list = new ArrayList();
        	list.add(adResponsibility);
        	list.add(adResponsibilitySysAdmin);
        	this.setFormFunctionResponsibility(list, adFormFunction, CMP_CODE);







			adFormFunction = adFormFunctionHome.findByPrimaryKey(new Integer(2001));
			list = new ArrayList();
        	list.add(adResponsibility);
        	list.add(adResponsibilityFinance);
        	this.setFormFunctionResponsibility(list, adFormFunction, CMP_CODE);

			adFormFunction = adFormFunctionHome.findByPrimaryKey(new Integer(2002));
			list = new ArrayList();
        	list.add(adResponsibility);
        	list.add(adResponsibilityFinance);
        	this.setFormFunctionResponsibility(list, adFormFunction, CMP_CODE);

			adFormFunction = adFormFunctionHome.findByPrimaryKey(new Integer(2003));
			list = new ArrayList();
        	list.add(adResponsibility);
        	list.add(adResponsibilityAccnt);
        	list.add(adResponsibilityArClerk);
        	list.add(adResponsibilityKeeper);
        	list.add(adResponsibilityFinance);
        	this.setFormFunctionResponsibility(list, adFormFunction, CMP_CODE);

			adFormFunction = adFormFunctionHome.findByPrimaryKey(new Integer(2004));
			list = new ArrayList();
        	list.add(adResponsibility);
        	list.add(adResponsibilityAccnt);
        	list.add(adResponsibilityArClerk);
        	list.add(adResponsibilityKeeper);
        	list.add(adResponsibilityFinance);
        	this.setFormFunctionResponsibility(list, adFormFunction, CMP_CODE);

			adFormFunction = adFormFunctionHome.findByPrimaryKey(new Integer(2005));
			list = new ArrayList();
        	list.add(adResponsibility);
        	list.add(adResponsibilityAccnt);
        	list.add(adResponsibilityArClerk);
        	list.add(adResponsibilityKeeper);
        	list.add(adResponsibilityFinance);
        	this.setFormFunctionResponsibility(list, adFormFunction, CMP_CODE);

			adFormFunction = adFormFunctionHome.findByPrimaryKey(new Integer(2006));
			list = new ArrayList();
        	list.add(adResponsibility);
        	list.add(adResponsibilityAccnt);
        	list.add(adResponsibilityArClerk);
        	list.add(adResponsibilityKeeper);
        	list.add(adResponsibilityFinance);
        	this.setFormFunctionResponsibility(list, adFormFunction, CMP_CODE);

			adFormFunction = adFormFunctionHome.findByPrimaryKey(new Integer(2007));
			list = new ArrayList();
        	list.add(adResponsibility);
        	list.add(adResponsibilityAccnt);
        	list.add(adResponsibilityArClerk);
        	list.add(adResponsibilityKeeper);
        	list.add(adResponsibilityFinance);
        	this.setFormFunctionResponsibility(list, adFormFunction, CMP_CODE);

			adFormFunction = adFormFunctionHome.findByPrimaryKey(new Integer(2008));
			list = new ArrayList();
        	list.add(adResponsibility);
        	list.add(adResponsibilityAccnt);
        	list.add(adResponsibilityArClerk);
        	list.add(adResponsibilityKeeper);
        	list.add(adResponsibilityFinance);
        	this.setFormFunctionResponsibility(list, adFormFunction, CMP_CODE);

			adFormFunction = adFormFunctionHome.findByPrimaryKey(new Integer(2009));
			list = new ArrayList();
        	list.add(adResponsibility);
        	list.add(adResponsibilityAccnt);
        	list.add(adResponsibilityArClerk);
        	list.add(adResponsibilityKeeper);
        	list.add(adResponsibilityFinance);
        	this.setFormFunctionResponsibility(list, adFormFunction, CMP_CODE);

			adFormFunction = adFormFunctionHome.findByPrimaryKey(new Integer(2010));
			list = new ArrayList();
        	list.add(adResponsibility);
        	list.add(adResponsibilityAccnt);
        	list.add(adResponsibilityArClerk);
        	list.add(adResponsibilityKeeper);
        	list.add(adResponsibilityFinance);
        	this.setFormFunctionResponsibility(list, adFormFunction, CMP_CODE);

			adFormFunction = adFormFunctionHome.findByPrimaryKey(new Integer(2011));
			list = new ArrayList();
        	list.add(adResponsibility);
        	list.add(adResponsibilityAccnt);
        	list.add(adResponsibilityArClerk);
        	list.add(adResponsibilityKeeper);
        	list.add(adResponsibilityFinance);
        	this.setFormFunctionResponsibility(list, adFormFunction, CMP_CODE);

			adFormFunction = adFormFunctionHome.findByPrimaryKey(new Integer(2012));
			list = new ArrayList();
        	list.add(adResponsibility);
        	list.add(adResponsibilityAccnt);
        	list.add(adResponsibilityArClerk);
        	list.add(adResponsibilityKeeper);
        	list.add(adResponsibilityFinance);
        	this.setFormFunctionResponsibility(list, adFormFunction, CMP_CODE);

			adFormFunction = adFormFunctionHome.findByPrimaryKey(new Integer(2013));
			list = new ArrayList();
        	list.add(adResponsibility);
        	list.add(adResponsibilityAccnt);
        	list.add(adResponsibilityArClerk);
        	list.add(adResponsibilityKeeper);
        	list.add(adResponsibilityFinance);
        	this.setFormFunctionResponsibility(list, adFormFunction, CMP_CODE);

        	adFormFunction = adFormFunctionHome.findByPrimaryKey(new Integer(2014));
			list = new ArrayList();
        	list.add(adResponsibility);
        	list.add(adResponsibilityAccnt);
        	list.add(adResponsibilityArClerk);
        	list.add(adResponsibilityKeeper);
        	list.add(adResponsibilityFinance);
        	this.setFormFunctionResponsibility(list, adFormFunction, CMP_CODE);

        	adFormFunction = adFormFunctionHome.findByPrimaryKey(new Integer(2015));
			list = new ArrayList();
        	list.add(adResponsibility);
        	list.add(adResponsibilityAccnt);
        	list.add(adResponsibilityArClerk);
        	list.add(adResponsibilityKeeper);
        	list.add(adResponsibilityFinance);
        	this.setFormFunctionResponsibility(list, adFormFunction, CMP_CODE);

        	adFormFunction = adFormFunctionHome.findByPrimaryKey(new Integer(2016));
			list = new ArrayList();
        	list.add(adResponsibility);
        	list.add(adResponsibilityAccnt);
        	list.add(adResponsibilityArClerk);
        	list.add(adResponsibilityKeeper);
        	list.add(adResponsibilityFinance);
        	this.setFormFunctionResponsibility(list, adFormFunction, CMP_CODE);

        	adFormFunction = adFormFunctionHome.findByPrimaryKey(new Integer(2017));
			list = new ArrayList();
			list.add(adResponsibility);
        	list.add(adResponsibilityAccnt);
        	list.add(adResponsibilityApClerk);
        	list.add(adResponsibilityKeeper);
        	list.add(adResponsibilityFinance);
        	this.setFormFunctionResponsibility(list, adFormFunction, CMP_CODE);

        	adFormFunction = adFormFunctionHome.findByPrimaryKey(new Integer(2018));
			list = new ArrayList();
			list.add(adResponsibility);
        	list.add(adResponsibilityAccnt);
        	list.add(adResponsibilityApClerk);
        	list.add(adResponsibilityKeeper);
        	list.add(adResponsibilityFinance);
        	this.setFormFunctionResponsibility(list, adFormFunction, CMP_CODE);

        	adFormFunction = adFormFunctionHome.findByPrimaryKey(new Integer(2020));
			list = new ArrayList();
			list.add(adResponsibility);
        	list.add(adResponsibilityAccnt);
        	list.add(adResponsibilityApClerk);
        	list.add(adResponsibilityKeeper);
        	list.add(adResponsibilityFinance);
        	this.setFormFunctionResponsibility(list, adFormFunction, CMP_CODE);

        	adFormFunction = adFormFunctionHome.findByPrimaryKey(new Integer(2019));
			list = new ArrayList();
			list.add(adResponsibility);
        	list.add(adResponsibilityAccnt);
        	list.add(adResponsibilityApClerk);
        	list.add(adResponsibilityKeeper);
        	list.add(adResponsibilityFinance);
        	this.setFormFunctionResponsibility(list, adFormFunction, CMP_CODE);

        	adFormFunction = adFormFunctionHome.findByPrimaryKey(new Integer(2020));
			list = new ArrayList();
			list.add(adResponsibility);
        	list.add(adResponsibilityAccnt);
        	list.add(adResponsibilityApClerk);
        	list.add(adResponsibilityKeeper);
        	list.add(adResponsibilityFinance);
        	this.setFormFunctionResponsibility(list, adFormFunction, CMP_CODE);
        	
        	adFormFunction = adFormFunctionHome.findByPrimaryKey(new Integer(2021));
			list = new ArrayList();
			list.add(adResponsibility);
        	list.add(adResponsibilityAccnt);
        	list.add(adResponsibilityApClerk);
        	list.add(adResponsibilityKeeper);
        	list.add(adResponsibilityFinance);
        	this.setFormFunctionResponsibility(list, adFormFunction, CMP_CODE);
        	
        	adFormFunction = adFormFunctionHome.findByPrimaryKey(new Integer(2022));
			list = new ArrayList();
			list.add(adResponsibility);
        	list.add(adResponsibilityAccnt);
        	list.add(adResponsibilityApClerk);
        	list.add(adResponsibilityKeeper);
        	list.add(adResponsibilityFinance);
        	this.setFormFunctionResponsibility(list, adFormFunction, CMP_CODE);
        	
        	adFormFunction = adFormFunctionHome.findByPrimaryKey(new Integer(2023));
			list = new ArrayList();
			list.add(adResponsibility);
        	list.add(adResponsibilityAccnt);
        	list.add(adResponsibilityApClerk);
        	list.add(adResponsibilityKeeper);
        	list.add(adResponsibilityFinance);
        	this.setFormFunctionResponsibility(list, adFormFunction, CMP_CODE);
        	
        	adFormFunction = adFormFunctionHome.findByPrimaryKey(new Integer(2024));
			list = new ArrayList();
			list.add(adResponsibility);
        	list.add(adResponsibilityAccnt);
        	list.add(adResponsibilityApClerk);
        	list.add(adResponsibilityKeeper);
        	list.add(adResponsibilityFinance);
        	this.setFormFunctionResponsibility(list, adFormFunction, CMP_CODE);

			adFormFunction = adFormFunctionHome.findByPrimaryKey(new Integer(2301));
			list = new ArrayList();
        	list.add(adResponsibility);
        	list.add(adResponsibilityFinance);
        	this.setFormFunctionResponsibility(list, adFormFunction, CMP_CODE);

			adFormFunction = adFormFunctionHome.findByPrimaryKey(new Integer(2302));
			list = new ArrayList();
        	list.add(adResponsibility);
        	list.add(adResponsibilityFinance);
        	this.setFormFunctionResponsibility(list, adFormFunction, CMP_CODE);

			adFormFunction = adFormFunctionHome.findByPrimaryKey(new Integer(2303));
			list = new ArrayList();
        	list.add(adResponsibility);
        	list.add(adResponsibilityFinance);
        	this.setFormFunctionResponsibility(list, adFormFunction, CMP_CODE);

			adFormFunction = adFormFunctionHome.findByPrimaryKey(new Integer(2305));
			list = new ArrayList();
        	list.add(adResponsibility);
        	list.add(adResponsibilityFinance);
        	this.setFormFunctionResponsibility(list, adFormFunction, CMP_CODE);

			adFormFunction = adFormFunctionHome.findByPrimaryKey(new Integer(2306));
			list = new ArrayList();
        	list.add(adResponsibility);
        	list.add(adResponsibilityFinance);
        	this.setFormFunctionResponsibility(list, adFormFunction, CMP_CODE);

			adFormFunction = adFormFunctionHome.findByPrimaryKey(new Integer(2307));
			list = new ArrayList();	
        	list.add(adResponsibility);
        	list.add(adResponsibilityAccnt);
        	list.add(adResponsibilityArClerk);
        	list.add(adResponsibilityKeeper);
        	list.add(adResponsibilityFinance);
        	this.setFormFunctionResponsibility(list, adFormFunction, CMP_CODE);

			adFormFunction = adFormFunctionHome.findByPrimaryKey(new Integer(2308));
			list = new ArrayList();
        	list.add(adResponsibility);
        	list.add(adResponsibilityAccnt);
        	list.add(adResponsibilityArClerk);
        	list.add(adResponsibilityKeeper);
        	list.add(adResponsibilityFinance);
        	this.setFormFunctionResponsibility(list, adFormFunction, CMP_CODE);

			adFormFunction = adFormFunctionHome.findByPrimaryKey(new Integer(2309));
			list = new ArrayList();
        	list.add(adResponsibility);
        	list.add(adResponsibilityAccnt);
        	list.add(adResponsibilityArClerk);
        	list.add(adResponsibilityKeeper);
        	list.add(adResponsibilityFinance);
        	this.setFormFunctionResponsibility(list, adFormFunction, CMP_CODE);

			adFormFunction = adFormFunctionHome.findByPrimaryKey(new Integer(2310));
			list = new ArrayList();
        	list.add(adResponsibility);
        	list.add(adResponsibilityAccnt);
        	list.add(adResponsibilityArClerk);
        	list.add(adResponsibilityKeeper);
        	list.add(adResponsibilityFinance);
        	this.setFormFunctionResponsibility(list, adFormFunction, CMP_CODE);

			adFormFunction = adFormFunctionHome.findByPrimaryKey(new Integer(2311));
			list = new ArrayList();
        	list.add(adResponsibility);
        	list.add(adResponsibilityAccnt);
        	list.add(adResponsibilityArClerk);
        	list.add(adResponsibilityKeeper);
        	list.add(adResponsibilityFinance);
        	this.setFormFunctionResponsibility(list, adFormFunction, CMP_CODE);

			adFormFunction = adFormFunctionHome.findByPrimaryKey(new Integer(2312));
			list = new ArrayList();
        	list.add(adResponsibility);
        	list.add(adResponsibilityAccnt);
        	list.add(adResponsibilityArClerk);
        	list.add(adResponsibilityKeeper);
        	list.add(adResponsibilityFinance);
        	this.setFormFunctionResponsibility(list, adFormFunction, CMP_CODE);

			adFormFunction = adFormFunctionHome.findByPrimaryKey(new Integer(2313));
			list = new ArrayList();
        	list.add(adResponsibility);
        	list.add(adResponsibilityAccnt);
        	list.add(adResponsibilityArClerk);
        	list.add(adResponsibilityKeeper);
        	list.add(adResponsibilityFinance);
        	this.setFormFunctionResponsibility(list, adFormFunction, CMP_CODE);

			adFormFunction = adFormFunctionHome.findByPrimaryKey(new Integer(2314));
			list = new ArrayList();
        	list.add(adResponsibility);
        	list.add(adResponsibilityAccnt);
        	list.add(adResponsibilityArClerk);
        	list.add(adResponsibilityKeeper);
        	list.add(adResponsibilityFinance);
        	this.setFormFunctionResponsibility(list, adFormFunction, CMP_CODE);

			adFormFunction = adFormFunctionHome.findByPrimaryKey(new Integer(2315));
			list = new ArrayList();
        	list.add(adResponsibility);
        	list.add(adResponsibilityAccnt);
        	list.add(adResponsibilityArClerk);
        	list.add(adResponsibilityKeeper);
        	list.add(adResponsibilityFinance);
        	this.setFormFunctionResponsibility(list, adFormFunction, CMP_CODE);

			adFormFunction = adFormFunctionHome.findByPrimaryKey(new Integer(2316));
			list = new ArrayList();
        	list.add(adResponsibility);
        	list.add(adResponsibilityAccnt);
        	list.add(adResponsibilityArClerk);
        	list.add(adResponsibilityKeeper);
        	list.add(adResponsibilityFinance);
        	this.setFormFunctionResponsibility(list, adFormFunction, CMP_CODE);

			adFormFunction = adFormFunctionHome.findByPrimaryKey(new Integer(2317));
			list = new ArrayList();
        	list.add(adResponsibility);
        	list.add(adResponsibilityAccnt);
        	list.add(adResponsibilityArClerk);
        	list.add(adResponsibilityKeeper);
        	list.add(adResponsibilityFinance);
        	this.setFormFunctionResponsibility(list, adFormFunction, CMP_CODE);

        	adFormFunction = adFormFunctionHome.findByPrimaryKey(new Integer(2318));
			list = new ArrayList();
        	list.add(adResponsibility);
        	list.add(adResponsibilityAccnt);
        	list.add(adResponsibilityArClerk);
        	list.add(adResponsibilityKeeper);
        	list.add(adResponsibilityFinance);
        	this.setFormFunctionResponsibility(list, adFormFunction, CMP_CODE);
        	
        	adFormFunction = adFormFunctionHome.findByPrimaryKey(new Integer(2319));
			list = new ArrayList();
        	list.add(adResponsibility);
        	list.add(adResponsibilityAccnt);
        	list.add(adResponsibilityArClerk);
        	list.add(adResponsibilityKeeper);
        	list.add(adResponsibilityFinance);
        	this.setFormFunctionResponsibility(list, adFormFunction, CMP_CODE);
        	
        	adFormFunction = adFormFunctionHome.findByPrimaryKey(new Integer(2320));
			list = new ArrayList();
        	list.add(adResponsibility);
        	list.add(adResponsibilityAccnt);
        	list.add(adResponsibilityArClerk);
        	list.add(adResponsibilityKeeper);
        	list.add(adResponsibilityFinance);
        	this.setFormFunctionResponsibility(list, adFormFunction, CMP_CODE);
        	
        	adFormFunction = adFormFunctionHome.findByPrimaryKey(new Integer(2321));
			list = new ArrayList();
        	list.add(adResponsibility);
        	list.add(adResponsibilityAccnt);
        	list.add(adResponsibilityArClerk);
        	list.add(adResponsibilityKeeper);
        	list.add(adResponsibilityFinance);
        	this.setFormFunctionResponsibility(list, adFormFunction, CMP_CODE);
        	
        	adFormFunction = adFormFunctionHome.findByPrimaryKey(new Integer(2322));
			list = new ArrayList();
        	list.add(adResponsibility);
        	list.add(adResponsibilityAccnt);
        	list.add(adResponsibilityArClerk);
        	list.add(adResponsibilityKeeper);
        	list.add(adResponsibilityFinance);
        	this.setFormFunctionResponsibility(list, adFormFunction, CMP_CODE);

        	adFormFunction = adFormFunctionHome.findByPrimaryKey(new Integer(2501));
			list = new ArrayList();
        	list.add(adResponsibility);
        	list.add(adResponsibilityAccnt);
        	list.add(adResponsibilityArClerk);
        	list.add(adResponsibilityKeeper);
        	list.add(adResponsibilityFinance);
        	this.setFormFunctionResponsibility(list, adFormFunction, CMP_CODE);

			adFormFunction = adFormFunctionHome.findByPrimaryKey(new Integer(2502));
			list = new ArrayList();
        	list.add(adResponsibility);
        	list.add(adResponsibilityAccnt);
        	list.add(adResponsibilityArClerk);
        	list.add(adResponsibilityKeeper);
        	list.add(adResponsibilityFinance);
        	this.setFormFunctionResponsibility(list, adFormFunction, CMP_CODE);

			adFormFunction = adFormFunctionHome.findByPrimaryKey(new Integer(2503));
			list = new ArrayList();
        	list.add(adResponsibility);
        	list.add(adResponsibilityAccnt);
        	list.add(adResponsibilityArClerk);
        	list.add(adResponsibilityKeeper);
        	list.add(adResponsibilityFinance);
        	this.setFormFunctionResponsibility(list, adFormFunction, CMP_CODE);

			adFormFunction = adFormFunctionHome.findByPrimaryKey(new Integer(2504));
			list = new ArrayList();
        	list.add(adResponsibility);
        	list.add(adResponsibilityAccnt);
        	list.add(adResponsibilityArClerk);
        	list.add(adResponsibilityFinance);
        	this.setFormFunctionResponsibility(list, adFormFunction, CMP_CODE);

			adFormFunction = adFormFunctionHome.findByPrimaryKey(new Integer(2505));
			list = new ArrayList();
        	list.add(adResponsibility);
        	list.add(adResponsibilityAccnt);
        	list.add(adResponsibilityArClerk);
        	list.add(adResponsibilityFinance);
        	this.setFormFunctionResponsibility(list, adFormFunction, CMP_CODE);

			adFormFunction = adFormFunctionHome.findByPrimaryKey(new Integer(2506));
			list = new ArrayList();
        	list.add(adResponsibility);
        	list.add(adResponsibilityAccnt);
        	list.add(adResponsibilityArClerk);
        	list.add(adResponsibilityFinance);
        	this.setFormFunctionResponsibility(list, adFormFunction, CMP_CODE);

			adFormFunction = adFormFunctionHome.findByPrimaryKey(new Integer(2507));
			list = new ArrayList();
        	list.add(adResponsibility);
        	list.add(adResponsibilityAccnt);
        	list.add(adResponsibilityArClerk);
        	list.add(adResponsibilityFinance);
        	this.setFormFunctionResponsibility(list, adFormFunction, CMP_CODE);

			adFormFunction = adFormFunctionHome.findByPrimaryKey(new Integer(2508));
			list = new ArrayList();
        	list.add(adResponsibility);
        	list.add(adResponsibilityAccnt);
        	list.add(adResponsibilityArClerk);
        	list.add(adResponsibilityFinance);
        	this.setFormFunctionResponsibility(list, adFormFunction, CMP_CODE);

			adFormFunction = adFormFunctionHome.findByPrimaryKey(new Integer(2509));
			list = new ArrayList();
        	list.add(adResponsibility);
        	list.add(adResponsibilityAccnt);
        	list.add(adResponsibilityArClerk);
        	list.add(adResponsibilityFinance);
        	this.setFormFunctionResponsibility(list, adFormFunction, CMP_CODE);

			adFormFunction = adFormFunctionHome.findByPrimaryKey(new Integer(2510));
			list = new ArrayList();
        	list.add(adResponsibility);
        	list.add(adResponsibilityAccnt);
        	list.add(adResponsibilityArClerk);
        	list.add(adResponsibilityFinance);
        	this.setFormFunctionResponsibility(list, adFormFunction, CMP_CODE);

			adFormFunction = adFormFunctionHome.findByPrimaryKey(new Integer(2511));
			list = new ArrayList();
        	list.add(adResponsibility);
        	list.add(adResponsibilityAccnt);
        	list.add(adResponsibilityArClerk);
        	list.add(adResponsibilityKeeper);
        	list.add(adResponsibilityFinance);
        	this.setFormFunctionResponsibility(list, adFormFunction, CMP_CODE);

			adFormFunction = adFormFunctionHome.findByPrimaryKey(new Integer(2512));
			list = new ArrayList();
        	list.add(adResponsibility);
        	list.add(adResponsibilityAccnt);
        	list.add(adResponsibilityArClerk);
        	list.add(adResponsibilityKeeper);
        	list.add(adResponsibilityFinance);
        	this.setFormFunctionResponsibility(list, adFormFunction, CMP_CODE);

        	adFormFunction = adFormFunctionHome.findByPrimaryKey(new Integer(2513));
        	list = new ArrayList();
        	list.add(adResponsibility);
        	list.add(adResponsibilityAccnt);
        	list.add(adResponsibilityArClerk);
        	list.add(adResponsibilityFinance);
        	this.setFormFunctionResponsibility(list, adFormFunction, CMP_CODE);

        	adFormFunction = adFormFunctionHome.findByPrimaryKey(new Integer(2514));
        	list = new ArrayList();
        	list.add(adResponsibility);
        	list.add(adResponsibilityAccnt);
        	list.add(adResponsibilityArClerk);
        	list.add(adResponsibilityFinance);
        	this.setFormFunctionResponsibility(list, adFormFunction, CMP_CODE);

        	adFormFunction = adFormFunctionHome.findByPrimaryKey(new Integer(2515));
        	list = new ArrayList();
        	list.add(adResponsibility);
        	list.add(adResponsibilityAccnt);
        	list.add(adResponsibilityArClerk);
        	list.add(adResponsibilityFinance);
        	this.setFormFunctionResponsibility(list, adFormFunction, CMP_CODE);

        	adFormFunction = adFormFunctionHome.findByPrimaryKey(new Integer(2516));
        	list = new ArrayList();
        	list.add(adResponsibility);
        	list.add(adResponsibilityAccnt);
        	list.add(adResponsibilityArClerk);
        	list.add(adResponsibilityFinance);
        	this.setFormFunctionResponsibility(list, adFormFunction, CMP_CODE);

        	adFormFunction = adFormFunctionHome.findByPrimaryKey(new Integer(2517));
        	list = new ArrayList();
        	list.add(adResponsibility);
        	list.add(adResponsibilityAccnt);
        	list.add(adResponsibilityArClerk);
        	list.add(adResponsibilityFinance);
        	this.setFormFunctionResponsibility(list, adFormFunction, CMP_CODE);

        	adFormFunction = adFormFunctionHome.findByPrimaryKey(new Integer(2518));
        	list = new ArrayList();
        	list.add(adResponsibility);
        	list.add(adResponsibilityAccnt);
        	list.add(adResponsibilityArClerk);
        	list.add(adResponsibilityFinance);
        	this.setFormFunctionResponsibility(list, adFormFunction, CMP_CODE);

        	adFormFunction = adFormFunctionHome.findByPrimaryKey(new Integer(2519));
        	list = new ArrayList();
        	list.add(adResponsibility);
        	list.add(adResponsibilityAccnt);
        	list.add(adResponsibilityArClerk);
        	list.add(adResponsibilityFinance);
        	this.setFormFunctionResponsibility(list, adFormFunction, CMP_CODE);

        	adFormFunction = adFormFunctionHome.findByPrimaryKey(new Integer(2520));
        	list = new ArrayList();
        	list.add(adResponsibility);
        	list.add(adResponsibilityAccnt);
        	list.add(adResponsibilityArClerk);
        	list.add(adResponsibilityFinance);
        	this.setFormFunctionResponsibility(list, adFormFunction, CMP_CODE);

        	adFormFunction = adFormFunctionHome.findByPrimaryKey(new Integer(2521));
        	list = new ArrayList();
        	list.add(adResponsibility);
        	list.add(adResponsibilityAccnt);
        	list.add(adResponsibilityArClerk);
        	list.add(adResponsibilityKeeper);
        	list.add(adResponsibilityFinance);
        	this.setFormFunctionResponsibility(list, adFormFunction, CMP_CODE);

        	adFormFunction = adFormFunctionHome.findByPrimaryKey(new Integer(2522));
        	list = new ArrayList();
        	list.add(adResponsibility);
        	list.add(adResponsibilityAccnt);
        	list.add(adResponsibilityArClerk);
        	list.add(adResponsibilityKeeper);
        	list.add(adResponsibilityFinance);
        	this.setFormFunctionResponsibility(list, adFormFunction, CMP_CODE);

        	adFormFunction = adFormFunctionHome.findByPrimaryKey(new Integer(2523));
        	list = new ArrayList();
        	list.add(adResponsibility);
        	list.add(adResponsibilityAccnt);
        	list.add(adResponsibilityArClerk);
        	list.add(adResponsibilityKeeper);
        	list.add(adResponsibilityFinance);
        	this.setFormFunctionResponsibility(list, adFormFunction, CMP_CODE);

        	adFormFunction = adFormFunctionHome.findByPrimaryKey(new Integer(2524));
        	list = new ArrayList();
        	list.add(adResponsibility);
        	list.add(adResponsibilityAccnt);
        	list.add(adResponsibilityArClerk);
        	list.add(adResponsibilityKeeper);
        	list.add(adResponsibilityFinance);
        	this.setFormFunctionResponsibility(list, adFormFunction, CMP_CODE);

        	adFormFunction = adFormFunctionHome.findByPrimaryKey(new Integer(2525));
        	list = new ArrayList();
        	list.add(adResponsibility);
        	list.add(adResponsibilityAccnt);
        	list.add(adResponsibilityArClerk);
        	list.add(adResponsibilityKeeper);
        	list.add(adResponsibilityFinance);
        	this.setFormFunctionResponsibility(list, adFormFunction, CMP_CODE);

        	adFormFunction = adFormFunctionHome.findByPrimaryKey(new Integer(2526));
        	list = new ArrayList();
        	list.add(adResponsibility);
        	list.add(adResponsibilityAccnt);
        	list.add(adResponsibilityArClerk);
        	list.add(adResponsibilityKeeper);
        	list.add(adResponsibilityFinance);
        	this.setFormFunctionResponsibility(list, adFormFunction, CMP_CODE);

			adFormFunction = adFormFunctionHome.findByPrimaryKey(new Integer(2527));
			list = new ArrayList();
        	list.add(adResponsibility);
        	list.add(adResponsibilityAccnt);
        	list.add(adResponsibilityArClerk);
        	list.add(adResponsibilityKeeper);
        	list.add(adResponsibilityFinance);
        	this.setFormFunctionResponsibility(list, adFormFunction, CMP_CODE);
        	
        	adFormFunction = adFormFunctionHome.findByPrimaryKey(new Integer(2528));
			list = new ArrayList();
        	list.add(adResponsibility);
        	list.add(adResponsibilityAccnt);
        	list.add(adResponsibilityArClerk);
        	list.add(adResponsibilityKeeper);
        	list.add(adResponsibilityFinance);
        	this.setFormFunctionResponsibility(list, adFormFunction, CMP_CODE);
        	
        	
        	adFormFunction = adFormFunctionHome.findByPrimaryKey(new Integer(2529));
			list = new ArrayList();
        	list.add(adResponsibility);
        	list.add(adResponsibilityAccnt);
        	list.add(adResponsibilityArClerk);
        	list.add(adResponsibilityKeeper);
        	list.add(adResponsibilityFinance);
        	this.setFormFunctionResponsibility(list, adFormFunction, CMP_CODE);
        	
        	adFormFunction = adFormFunctionHome.findByPrimaryKey(new Integer(2530));
			list = new ArrayList();
        	list.add(adResponsibility);
        	list.add(adResponsibilityAccnt);
        	list.add(adResponsibilityArClerk);
        	list.add(adResponsibilityKeeper);
        	list.add(adResponsibilityFinance);
        	this.setFormFunctionResponsibility(list, adFormFunction, CMP_CODE);


        	adFormFunction = adFormFunctionHome.findByPrimaryKey(new Integer(3001));
			list = new ArrayList();
        	list.add(adResponsibility);
        	list.add(adResponsibilityFinance);
        	this.setFormFunctionResponsibility(list, adFormFunction, CMP_CODE);

			adFormFunction = adFormFunctionHome.findByPrimaryKey(new Integer(3002));
			list = new ArrayList();
        	list.add(adResponsibility);
        	list.add(adResponsibilityFinance);
        	this.setFormFunctionResponsibility(list, adFormFunction, CMP_CODE);

			adFormFunction = adFormFunctionHome.findByPrimaryKey(new Integer(3003));
			list = new ArrayList();
        	list.add(adResponsibility);
        	list.add(adResponsibilityAccnt);
        	list.add(adResponsibilityApClerk);
        	list.add(adResponsibilityKeeper);
        	list.add(adResponsibilityFinance);
        	this.setFormFunctionResponsibility(list, adFormFunction, CMP_CODE);

			adFormFunction = adFormFunctionHome.findByPrimaryKey(new Integer(3004));
			list = new ArrayList();
        	list.add(adResponsibility);
        	list.add(adResponsibilityAccnt);
        	list.add(adResponsibilityApClerk);
        	list.add(adResponsibilityKeeper);
        	list.add(adResponsibilityFinance);
        	this.setFormFunctionResponsibility(list, adFormFunction, CMP_CODE);

			adFormFunction = adFormFunctionHome.findByPrimaryKey(new Integer(3005));
			list = new ArrayList();
        	list.add(adResponsibility);
        	list.add(adResponsibilityAccnt);
        	list.add(adResponsibilityApClerk);
        	list.add(adResponsibilityKeeper);
        	list.add(adResponsibilityFinance);
        	this.setFormFunctionResponsibility(list, adFormFunction, CMP_CODE);

			adFormFunction = adFormFunctionHome.findByPrimaryKey(new Integer(3006));
			list = new ArrayList();
        	list.add(adResponsibility);
        	list.add(adResponsibilityAccnt);
        	list.add(adResponsibilityApClerk);
        	list.add(adResponsibilityKeeper);
        	list.add(adResponsibilityFinance);
        	this.setFormFunctionResponsibility(list, adFormFunction, CMP_CODE);

			adFormFunction = adFormFunctionHome.findByPrimaryKey(new Integer(3007));
			list = new ArrayList();
        	list.add(adResponsibility);
        	list.add(adResponsibilityAccnt);
        	list.add(adResponsibilityApClerk);
        	list.add(adResponsibilityKeeper);
        	list.add(adResponsibilityFinance);
        	this.setFormFunctionResponsibility(list, adFormFunction, CMP_CODE);

			adFormFunction = adFormFunctionHome.findByPrimaryKey(new Integer(3008));
			list = new ArrayList();
        	list.add(adResponsibility);
        	list.add(adResponsibilityAccnt);
        	list.add(adResponsibilityApClerk);
        	list.add(adResponsibilityKeeper);
        	list.add(adResponsibilityFinance);
        	this.setFormFunctionResponsibility(list, adFormFunction, CMP_CODE);

			adFormFunction = adFormFunctionHome.findByPrimaryKey(new Integer(3009));
			list = new ArrayList();
        	list.add(adResponsibility);
        	list.add(adResponsibilityAccnt);
        	list.add(adResponsibilityApClerk);
        	list.add(adResponsibilityKeeper);
        	list.add(adResponsibilityFinance);
        	this.setFormFunctionResponsibility(list, adFormFunction, CMP_CODE);

			adFormFunction = adFormFunctionHome.findByPrimaryKey(new Integer(3010));
			list = new ArrayList();
        	list.add(adResponsibility);
        	list.add(adResponsibilityAccnt);
        	list.add(adResponsibilityApClerk);
        	list.add(adResponsibilityKeeper);
        	list.add(adResponsibilityFinance);
        	this.setFormFunctionResponsibility(list, adFormFunction, CMP_CODE);

			adFormFunction = adFormFunctionHome.findByPrimaryKey(new Integer(3011));
			list = new ArrayList();
        	list.add(adResponsibility);
        	list.add(adResponsibilityAccnt);
        	list.add(adResponsibilityApClerk);
        	list.add(adResponsibilityKeeper);
        	list.add(adResponsibilityFinance);
        	this.setFormFunctionResponsibility(list, adFormFunction, CMP_CODE);

			adFormFunction = adFormFunctionHome.findByPrimaryKey(new Integer(3012));
			list = new ArrayList();
        	list.add(adResponsibility);
        	list.add(adResponsibilityAccnt);
        	list.add(adResponsibilityApClerk);
        	list.add(adResponsibilityKeeper);
        	list.add(adResponsibilityFinance);
        	this.setFormFunctionResponsibility(list, adFormFunction, CMP_CODE);

			adFormFunction = adFormFunctionHome.findByPrimaryKey(new Integer(3013));
			list = new ArrayList();
        	list.add(adResponsibility);
        	list.add(adResponsibilityAccnt);
        	list.add(adResponsibilityApClerk);
        	list.add(adResponsibilityKeeper);
        	list.add(adResponsibilityFinance);
        	this.setFormFunctionResponsibility(list, adFormFunction, CMP_CODE);

			adFormFunction = adFormFunctionHome.findByPrimaryKey(new Integer(3014));
			list = new ArrayList();
        	list.add(adResponsibility);
        	list.add(adResponsibilityAccnt);
        	list.add(adResponsibilityApClerk);
        	list.add(adResponsibilityKeeper);
        	list.add(adResponsibilityFinance);
        	this.setFormFunctionResponsibility(list, adFormFunction, CMP_CODE);

			adFormFunction = adFormFunctionHome.findByPrimaryKey(new Integer(3015));
			list = new ArrayList();
        	list.add(adResponsibility);
        	list.add(adResponsibilityAccnt);
        	list.add(adResponsibilityApClerk);
        	list.add(adResponsibilityKeeper);
        	list.add(adResponsibilityFinance);
        	this.setFormFunctionResponsibility(list, adFormFunction, CMP_CODE);

			adFormFunction = adFormFunctionHome.findByPrimaryKey(new Integer(3016));
			list = new ArrayList();
        	list.add(adResponsibility);
        	list.add(adResponsibilityAccnt);
        	list.add(adResponsibilityApClerk);
        	list.add(adResponsibilityKeeper);
        	list.add(adResponsibilityFinance);
        	this.setFormFunctionResponsibility(list, adFormFunction, CMP_CODE);

			adFormFunction = adFormFunctionHome.findByPrimaryKey(new Integer(3017));
			list = new ArrayList();
        	list.add(adResponsibility);
        	list.add(adResponsibilityAccnt);
        	list.add(adResponsibilityApClerk);
        	list.add(adResponsibilityKeeper);
        	list.add(adResponsibilityFinance);
        	this.setFormFunctionResponsibility(list, adFormFunction, CMP_CODE);

			adFormFunction = adFormFunctionHome.findByPrimaryKey(new Integer(3018));
			list = new ArrayList();
        	list.add(adResponsibility);
        	list.add(adResponsibilityAccnt);
        	list.add(adResponsibilityApClerk);
        	list.add(adResponsibilityKeeper);
        	list.add(adResponsibilityFinance);
        	this.setFormFunctionResponsibility(list, adFormFunction, CMP_CODE);

			adFormFunction = adFormFunctionHome.findByPrimaryKey(new Integer(3019));
			list = new ArrayList();
        	list.add(adResponsibility);
        	list.add(adResponsibilityAccnt);
        	list.add(adResponsibilityApClerk);
        	list.add(adResponsibilityKeeper);
        	list.add(adResponsibilityFinance);
        	this.setFormFunctionResponsibility(list, adFormFunction, CMP_CODE);

			adFormFunction = adFormFunctionHome.findByPrimaryKey(new Integer(3020));
			list = new ArrayList();
        	list.add(adResponsibility);
        	list.add(adResponsibilityAccnt);
        	list.add(adResponsibilityApClerk);
        	list.add(adResponsibilityKeeper);
        	list.add(adResponsibilityFinance);
        	this.setFormFunctionResponsibility(list, adFormFunction, CMP_CODE);

			adFormFunction = adFormFunctionHome.findByPrimaryKey(new Integer(3021));
			list = new ArrayList();
        	list.add(adResponsibility);
        	list.add(adResponsibilityAccnt);
        	list.add(adResponsibilityApClerk);
        	list.add(adResponsibilityKeeper);
        	list.add(adResponsibilityFinance);
        	this.setFormFunctionResponsibility(list, adFormFunction, CMP_CODE);

			adFormFunction = adFormFunctionHome.findByPrimaryKey(new Integer(3022));
			list = new ArrayList();
        	list.add(adResponsibility);
        	list.add(adResponsibilityAccnt);
        	list.add(adResponsibilityApClerk);
        	list.add(adResponsibilityKeeper);
        	list.add(adResponsibilityFinance);
        	this.setFormFunctionResponsibility(list, adFormFunction, CMP_CODE);

			adFormFunction = adFormFunctionHome.findByPrimaryKey(new Integer(3023));
			list = new ArrayList();
        	list.add(adResponsibility);
        	list.add(adResponsibilityAccnt);
        	list.add(adResponsibilityApClerk);
        	list.add(adResponsibilityKeeper);
        	list.add(adResponsibilityFinance);
        	this.setFormFunctionResponsibility(list, adFormFunction, CMP_CODE);

			adFormFunction = adFormFunctionHome.findByPrimaryKey(new Integer(3024));
			list = new ArrayList();
        	list.add(adResponsibility);
        	list.add(adResponsibilityAccnt);
        	list.add(adResponsibilityApClerk);
        	list.add(adResponsibilityKeeper);
        	list.add(adResponsibilityFinance);
        	this.setFormFunctionResponsibility(list, adFormFunction, CMP_CODE);

			adFormFunction = adFormFunctionHome.findByPrimaryKey(new Integer(3025));
			list = new ArrayList();
        	list.add(adResponsibility);
        	list.add(adResponsibilityAccnt);
        	list.add(adResponsibilityApClerk);
        	list.add(adResponsibilityKeeper);
        	list.add(adResponsibilityFinance);
        	this.setFormFunctionResponsibility(list, adFormFunction, CMP_CODE);

			adFormFunction = adFormFunctionHome.findByPrimaryKey(new Integer(3026));
			list = new ArrayList();
        	list.add(adResponsibility);
        	list.add(adResponsibilityAccnt);
        	list.add(adResponsibilityApClerk);
        	list.add(adResponsibilityKeeper);
        	list.add(adResponsibilityFinance);
        	this.setFormFunctionResponsibility(list, adFormFunction, CMP_CODE);

        	adFormFunction = adFormFunctionHome.findByPrimaryKey(new Integer(3027));
        	list = new ArrayList();
        	list.add(adResponsibility);
        	list.add(adResponsibilityAccnt);
        	list.add(adResponsibilityApClerk);
        	list.add(adResponsibilityKeeper);
        	list.add(adResponsibilityFinance);
        	this.setFormFunctionResponsibility(list, adFormFunction, CMP_CODE);

        	adFormFunction = adFormFunctionHome.findByPrimaryKey(new Integer(3028));
        	list = new ArrayList();
        	list.add(adResponsibility);
        	list.add(adResponsibilityAccnt);
        	list.add(adResponsibilityApClerk);
        	list.add(adResponsibilityKeeper);
        	list.add(adResponsibilityFinance);
        	this.setFormFunctionResponsibility(list, adFormFunction, CMP_CODE);

        	adFormFunction = adFormFunctionHome.findByPrimaryKey(new Integer(3029));
        	list = new ArrayList();
        	list.add(adResponsibility);
        	list.add(adResponsibilityAccnt);
        	list.add(adResponsibilityApClerk);
        	list.add(adResponsibilityKeeper);
        	list.add(adResponsibilityFinance);
        	this.setFormFunctionResponsibility(list, adFormFunction, CMP_CODE);

        	adFormFunction = adFormFunctionHome.findByPrimaryKey(new Integer(3030));
			list = new ArrayList();
        	list.add(adResponsibility);
        	list.add(adResponsibilityAccnt);
        	list.add(adResponsibilityApClerk);
        	list.add(adResponsibilityKeeper);
        	list.add(adResponsibilityFinance);
        	this.setFormFunctionResponsibility(list, adFormFunction, CMP_CODE);

        	adFormFunction = adFormFunctionHome.findByPrimaryKey(new Integer(3031));
        	list = new ArrayList();
        	list.add(adResponsibility);
        	list.add(adResponsibilityAccnt);
        	list.add(adResponsibilityApClerk);
        	list.add(adResponsibilityKeeper);
        	list.add(adResponsibilityFinance);
        	this.setFormFunctionResponsibility(list, adFormFunction, CMP_CODE);

        	adFormFunction = adFormFunctionHome.findByPrimaryKey(new Integer(3032));
        	list = new ArrayList();
        	list.add(adResponsibility);
        	list.add(adResponsibilityAccnt);
        	list.add(adResponsibilityApClerk);
        	list.add(adResponsibilityKeeper);
        	list.add(adResponsibilityFinance);
        	this.setFormFunctionResponsibility(list, adFormFunction, CMP_CODE);

        	adFormFunction = adFormFunctionHome.findByPrimaryKey(new Integer(3033));
        	list = new ArrayList();
        	list.add(adResponsibility);
        	list.add(adResponsibilityAccnt);
        	list.add(adResponsibilityApClerk);
        	list.add(adResponsibilityKeeper);
        	list.add(adResponsibilityFinance);
        	this.setFormFunctionResponsibility(list, adFormFunction, CMP_CODE);

        	adFormFunction = adFormFunctionHome.findByPrimaryKey(new Integer(3034));
        	list = new ArrayList();
        	list.add(adResponsibility);
        	list.add(adResponsibilityAccnt); 
        	list.add(adResponsibilityApClerk);
        	list.add(adResponsibilityKeeper);
        	list.add(adResponsibilityFinance);
        	this.setFormFunctionResponsibility(list, adFormFunction, CMP_CODE);

			adFormFunction = adFormFunctionHome.findByPrimaryKey(new Integer(3301));
			list = new ArrayList();
        	list.add(adResponsibility);
        	list.add(adResponsibilityFinance);
        	this.setFormFunctionResponsibility(list, adFormFunction, CMP_CODE);

			adFormFunction = adFormFunctionHome.findByPrimaryKey(new Integer(3302));
			list = new ArrayList();
        	list.add(adResponsibility);
        	list.add(adResponsibilityFinance);
        	this.setFormFunctionResponsibility(list, adFormFunction, CMP_CODE);

			adFormFunction = adFormFunctionHome.findByPrimaryKey(new Integer(3303));
			list = new ArrayList();
        	list.add(adResponsibility);
        	list.add(adResponsibilityFinance);
        	this.setFormFunctionResponsibility(list, adFormFunction, CMP_CODE);

			adFormFunction = adFormFunctionHome.findByPrimaryKey(new Integer(3304));
			list = new ArrayList();
        	list.add(adResponsibility);
        	list.add(adResponsibilityFinance);
        	this.setFormFunctionResponsibility(list, adFormFunction, CMP_CODE);

			adFormFunction = adFormFunctionHome.findByPrimaryKey(new Integer(3501));
			list = new ArrayList();
        	list.add(adResponsibility);
        	list.add(adResponsibilityAccnt);
        	list.add(adResponsibilityApClerk);
        	list.add(adResponsibilityKeeper);
        	list.add(adResponsibilityFinance);
        	this.setFormFunctionResponsibility(list, adFormFunction, CMP_CODE);

			adFormFunction = adFormFunctionHome.findByPrimaryKey(new Integer(3502));
			list = new ArrayList();
        	list.add(adResponsibility);
        	list.add(adResponsibilityAccnt);
        	list.add(adResponsibilityApClerk);
        	list.add(adResponsibilityKeeper);
        	list.add(adResponsibilityFinance);
        	this.setFormFunctionResponsibility(list, adFormFunction, CMP_CODE);

			adFormFunction = adFormFunctionHome.findByPrimaryKey(new Integer(3503));
			list = new ArrayList();
        	list.add(adResponsibility);
        	list.add(adResponsibilityAccnt);
        	list.add(adResponsibilityApClerk);
        	list.add(adResponsibilityKeeper);
        	list.add(adResponsibilityFinance);
        	this.setFormFunctionResponsibility(list, adFormFunction, CMP_CODE);

			adFormFunction = adFormFunctionHome.findByPrimaryKey(new Integer(3504));
			list = new ArrayList();
        	list.add(adResponsibility);
        	list.add(adResponsibilityAccnt);
        	list.add(adResponsibilityApClerk);
        	list.add(adResponsibilityKeeper);
        	list.add(adResponsibilityFinance);
        	this.setFormFunctionResponsibility(list, adFormFunction, CMP_CODE);

			adFormFunction = adFormFunctionHome.findByPrimaryKey(new Integer(3505));
			list = new ArrayList();
        	list.add(adResponsibility);
        	list.add(adResponsibilityAccnt);
        	list.add(adResponsibilityApClerk);
        	list.add(adResponsibilityFinance);
        	this.setFormFunctionResponsibility(list, adFormFunction, CMP_CODE);

			adFormFunction = adFormFunctionHome.findByPrimaryKey(new Integer(3506));
			list = new ArrayList();
        	list.add(adResponsibility);
        	list.add(adResponsibilityAccnt);
        	list.add(adResponsibilityApClerk);
        	list.add(adResponsibilityFinance);
        	this.setFormFunctionResponsibility(list, adFormFunction, CMP_CODE);

			adFormFunction = adFormFunctionHome.findByPrimaryKey(new Integer(3507));
			list = new ArrayList();
        	list.add(adResponsibility);
        	list.add(adResponsibilityAccnt);
        	list.add(adResponsibilityApClerk);
        	list.add(adResponsibilityFinance);
        	this.setFormFunctionResponsibility(list, adFormFunction, CMP_CODE);

			adFormFunction = adFormFunctionHome.findByPrimaryKey(new Integer(3508));
			list = new ArrayList();
        	list.add(adResponsibility);
        	list.add(adResponsibilityAccnt);
        	list.add(adResponsibilityApClerk);
        	list.add(adResponsibilityFinance);
        	this.setFormFunctionResponsibility(list, adFormFunction, CMP_CODE);

			adFormFunction = adFormFunctionHome.findByPrimaryKey(new Integer(3509));
			list = new ArrayList();
        	list.add(adResponsibility);
        	list.add(adResponsibilityAccnt);
        	list.add(adResponsibilityApClerk);
        	list.add(adResponsibilityFinance);
        	this.setFormFunctionResponsibility(list, adFormFunction, CMP_CODE);

			adFormFunction = adFormFunctionHome.findByPrimaryKey(new Integer(3510));
			list = new ArrayList();
        	list.add(adResponsibility);
        	list.add(adResponsibilityAccnt);
        	list.add(adResponsibilityApClerk);
        	list.add(adResponsibilityFinance);
        	this.setFormFunctionResponsibility(list, adFormFunction, CMP_CODE);

			adFormFunction = adFormFunctionHome.findByPrimaryKey(new Integer(3511));
			list = new ArrayList();
        	list.add(adResponsibility);
        	list.add(adResponsibilityAccnt);
        	list.add(adResponsibilityApClerk);
        	list.add(adResponsibilityKeeper);
        	list.add(adResponsibilityFinance);
        	this.setFormFunctionResponsibility(list, adFormFunction, CMP_CODE);

			adFormFunction = adFormFunctionHome.findByPrimaryKey(new Integer(3512));
			list = new ArrayList();
        	list.add(adResponsibility);
        	list.add(adResponsibilityAccnt);
        	list.add(adResponsibilityApClerk);
        	list.add(adResponsibilityKeeper);
        	list.add(adResponsibilityFinance);
        	this.setFormFunctionResponsibility(list, adFormFunction, CMP_CODE);

        	adFormFunction = adFormFunctionHome.findByPrimaryKey(new Integer(3513));
			list = new ArrayList();
        	list.add(adResponsibility);
        	list.add(adResponsibilityAccnt);
        	list.add(adResponsibilityApClerk);
        	list.add(adResponsibilityFinance);
        	this.setFormFunctionResponsibility(list, adFormFunction, CMP_CODE);

        	adFormFunction = adFormFunctionHome.findByPrimaryKey(new Integer(3514));
			list = new ArrayList();
        	list.add(adResponsibility);
        	list.add(adResponsibilityAccnt);
        	list.add(adResponsibilityApClerk);
        	list.add(adResponsibilityFinance);
        	this.setFormFunctionResponsibility(list, adFormFunction, CMP_CODE);

        	adFormFunction = adFormFunctionHome.findByPrimaryKey(new Integer(3515));
			list = new ArrayList();
        	list.add(adResponsibility);
        	list.add(adResponsibilityAccnt);
        	list.add(adResponsibilityApClerk);
        	list.add(adResponsibilityFinance);
        	this.setFormFunctionResponsibility(list, adFormFunction, CMP_CODE);

        	adFormFunction = adFormFunctionHome.findByPrimaryKey(new Integer(3516));
			list = new ArrayList();
        	list.add(adResponsibility);
        	list.add(adResponsibilityAccnt);
        	list.add(adResponsibilityApClerk);
        	list.add(adResponsibilityFinance);
        	this.setFormFunctionResponsibility(list, adFormFunction, CMP_CODE);

			adFormFunction = adFormFunctionHome.findByPrimaryKey(new Integer(3517));
			list = new ArrayList();
        	list.add(adResponsibility);
        	list.add(adResponsibilityAccnt);
        	list.add(adResponsibilityApClerk);
        	list.add(adResponsibilityKeeper);
        	list.add(adResponsibilityFinance);
        	this.setFormFunctionResponsibility(list, adFormFunction, CMP_CODE);

        	adFormFunction = adFormFunctionHome.findByPrimaryKey(new Integer(3518));
        	list = new ArrayList();
        	list.add(adResponsibility);
        	list.add(adResponsibilityAccnt);
        	list.add(adResponsibilityApClerk);
        	list.add(adResponsibilityKeeper);
        	list.add(adResponsibilityFinance);
        	this.setFormFunctionResponsibility(list, adFormFunction, CMP_CODE);

        	adFormFunction = adFormFunctionHome.findByPrimaryKey(new Integer(3519));
        	list = new ArrayList();
        	list.add(adResponsibility);
        	list.add(adResponsibilityAccnt);
        	list.add(adResponsibilityApClerk);
        	list.add(adResponsibilityKeeper);
        	list.add(adResponsibilityFinance);
        	this.setFormFunctionResponsibility(list, adFormFunction, CMP_CODE);

        	adFormFunction = adFormFunctionHome.findByPrimaryKey(new Integer(3520));
        	list = new ArrayList();
        	list.add(adResponsibility);
        	list.add(adResponsibilityAccnt);
        	list.add(adResponsibilityApClerk);
        	list.add(adResponsibilityKeeper);
        	list.add(adResponsibilityFinance);
        	this.setFormFunctionResponsibility(list, adFormFunction, CMP_CODE);

        	adFormFunction = adFormFunctionHome.findByPrimaryKey(new Integer(3521));
        	list = new ArrayList();
        	list.add(adResponsibility);
        	list.add(adResponsibilityAccnt);
        	list.add(adResponsibilityApClerk);
        	list.add(adResponsibilityKeeper);
        	list.add(adResponsibilityFinance);
        	this.setFormFunctionResponsibility(list, adFormFunction, CMP_CODE);

        	adFormFunction = adFormFunctionHome.findByPrimaryKey(new Integer(3522));
        	list = new ArrayList();
        	list.add(adResponsibility);
        	list.add(adResponsibilityAccnt);
        	list.add(adResponsibilityApClerk);
        	list.add(adResponsibilityKeeper);
        	list.add(adResponsibilityFinance);
        	this.setFormFunctionResponsibility(list, adFormFunction, CMP_CODE);

        	adFormFunction = adFormFunctionHome.findByPrimaryKey(new Integer(3523));
        	list = new ArrayList();
        	list.add(adResponsibility);
        	list.add(adResponsibilityAccnt);
        	list.add(adResponsibilityApClerk);
        	list.add(adResponsibilityKeeper);
        	list.add(adResponsibilityFinance);
        	this.setFormFunctionResponsibility(list, adFormFunction, CMP_CODE);

        	adFormFunction = adFormFunctionHome.findByPrimaryKey(new Integer(4001));
        	list = new ArrayList();
        	list.add(adResponsibility);
        	list.add(adResponsibilityAccnt);
        	list.add(adResponsibilityKeeper);
        	list.add(adResponsibilityFinance);
        	this.setFormFunctionResponsibility(list, adFormFunction, CMP_CODE);

			adFormFunction = adFormFunctionHome.findByPrimaryKey(new Integer(4002));
			list = new ArrayList();
        	list.add(adResponsibility);
        	list.add(adResponsibilityAccnt);
        	list.add(adResponsibilityKeeper);
        	list.add(adResponsibilityFinance);
        	this.setFormFunctionResponsibility(list, adFormFunction, CMP_CODE);

			adFormFunction = adFormFunctionHome.findByPrimaryKey(new Integer(4003));
			list = new ArrayList();
        	list.add(adResponsibility);
        	list.add(adResponsibilityAccnt);
        	list.add(adResponsibilityKeeper);
        	list.add(adResponsibilityFinance);
        	this.setFormFunctionResponsibility(list, adFormFunction, CMP_CODE);

			adFormFunction = adFormFunctionHome.findByPrimaryKey(new Integer(4004));
			list = new ArrayList();
        	list.add(adResponsibility);
        	list.add(adResponsibilityAccnt);
        	list.add(adResponsibilityKeeper);
        	list.add(adResponsibilityFinance);
        	this.setFormFunctionResponsibility(list, adFormFunction, CMP_CODE);

			adFormFunction = adFormFunctionHome.findByPrimaryKey(new Integer(4005));
			list = new ArrayList();
        	list.add(adResponsibility);
        	list.add(adResponsibilityAccnt);
        	list.add(adResponsibilityKeeper);
        	list.add(adResponsibilityFinance);
        	this.setFormFunctionResponsibility(list, adFormFunction, CMP_CODE);

			adFormFunction = adFormFunctionHome.findByPrimaryKey(new Integer(4006));
			list = new ArrayList();
        	list.add(adResponsibility);
        	list.add(adResponsibilityAccnt);
        	list.add(adResponsibilityKeeper);
        	list.add(adResponsibilityFinance);
        	this.setFormFunctionResponsibility(list, adFormFunction, CMP_CODE);

			adFormFunction = adFormFunctionHome.findByPrimaryKey(new Integer(4007));
			list = new ArrayList();
        	list.add(adResponsibility);
        	list.add(adResponsibilityAccnt);
        	list.add(adResponsibilityKeeper);
        	list.add(adResponsibilityFinance);
        	this.setFormFunctionResponsibility(list, adFormFunction, CMP_CODE);

			adFormFunction = adFormFunctionHome.findByPrimaryKey(new Integer(4008));
			list = new ArrayList();
        	list.add(adResponsibility);
        	list.add(adResponsibilityAccnt);
        	list.add(adResponsibilityKeeper);
        	list.add(adResponsibilityFinance);
        	this.setFormFunctionResponsibility(list, adFormFunction, CMP_CODE);

			adFormFunction = adFormFunctionHome.findByPrimaryKey(new Integer(4009));
			list = new ArrayList();
        	list.add(adResponsibility);
        	list.add(adResponsibilityAccnt);
        	list.add(adResponsibilityKeeper);
        	list.add(adResponsibilityFinance);
        	this.setFormFunctionResponsibility(list, adFormFunction, CMP_CODE);

			adFormFunction = adFormFunctionHome.findByPrimaryKey(new Integer(4010));
			list = new ArrayList();
        	list.add(adResponsibility);
        	list.add(adResponsibilityAccnt);
        	list.add(adResponsibilityKeeper);
        	list.add(adResponsibilityFinance);
        	this.setFormFunctionResponsibility(list, adFormFunction, CMP_CODE);

			adFormFunction = adFormFunctionHome.findByPrimaryKey(new Integer(4011));
			list = new ArrayList();
        	list.add(adResponsibility);
        	list.add(adResponsibilityAccnt);
        	list.add(adResponsibilityKeeper);
        	list.add(adResponsibilityFinance);
        	this.setFormFunctionResponsibility(list, adFormFunction, CMP_CODE);

			adFormFunction = adFormFunctionHome.findByPrimaryKey(new Integer(4012));
			list = new ArrayList();
        	list.add(adResponsibility);
        	list.add(adResponsibilityAccnt);
        	list.add(adResponsibilityKeeper);
        	list.add(adResponsibilityFinance);
        	this.setFormFunctionResponsibility(list, adFormFunction, CMP_CODE);

			adFormFunction = adFormFunctionHome.findByPrimaryKey(new Integer(4013));
			list = new ArrayList();
        	list.add(adResponsibility);
        	list.add(adResponsibilityAccnt);
        	list.add(adResponsibilityKeeper);
        	list.add(adResponsibilityFinance);
        	this.setFormFunctionResponsibility(list, adFormFunction, CMP_CODE);

			adFormFunction = adFormFunctionHome.findByPrimaryKey(new Integer(4501));
			list = new ArrayList();
        	list.add(adResponsibility);
        	list.add(adResponsibilityAccnt);
        	list.add(adResponsibilityKeeper);
        	list.add(adResponsibilityFinance);
        	this.setFormFunctionResponsibility(list, adFormFunction, CMP_CODE);

			adFormFunction = adFormFunctionHome.findByPrimaryKey(new Integer(4502));
			list = new ArrayList();
        	list.add(adResponsibility);
        	list.add(adResponsibilityAccnt);
        	list.add(adResponsibilityKeeper);
        	list.add(adResponsibilityFinance);
        	this.setFormFunctionResponsibility(list, adFormFunction, CMP_CODE);

			adFormFunction = adFormFunctionHome.findByPrimaryKey(new Integer(4503));
			list = new ArrayList();
        	list.add(adResponsibility);
        	list.add(adResponsibilityAccnt);
        	list.add(adResponsibilityKeeper);
        	list.add(adResponsibilityFinance);
        	this.setFormFunctionResponsibility(list, adFormFunction, CMP_CODE);

			adFormFunction = adFormFunctionHome.findByPrimaryKey(new Integer(4504));
			list = new ArrayList();
        	list.add(adResponsibility);
        	list.add(adResponsibilityAccnt);
        	list.add(adResponsibilityKeeper);
        	list.add(adResponsibilityFinance);
        	this.setFormFunctionResponsibility(list, adFormFunction, CMP_CODE);

			adFormFunction = adFormFunctionHome.findByPrimaryKey(new Integer(4505));
			list = new ArrayList();
        	list.add(adResponsibility);
        	list.add(adResponsibilityAccnt);
        	list.add(adResponsibilityKeeper);
        	list.add(adResponsibilityFinance);
        	this.setFormFunctionResponsibility(list, adFormFunction, CMP_CODE);

			adFormFunction = adFormFunctionHome.findByPrimaryKey(new Integer(4506));
			list = new ArrayList();
        	list.add(adResponsibility);
        	list.add(adResponsibilityAccnt);
        	list.add(adResponsibilityKeeper);
        	list.add(adResponsibilityFinance);
        	this.setFormFunctionResponsibility(list, adFormFunction, CMP_CODE);

			adFormFunction = adFormFunctionHome.findByPrimaryKey(new Integer(4507));
			list = new ArrayList();
        	list.add(adResponsibility);
        	list.add(adResponsibilityAccnt);
        	list.add(adResponsibilityKeeper);
        	list.add(adResponsibilityFinance);
        	this.setFormFunctionResponsibility(list, adFormFunction, CMP_CODE);

			adFormFunction = adFormFunctionHome.findByPrimaryKey(new Integer(4508));
			list = new ArrayList();
        	list.add(adResponsibility);
        	list.add(adResponsibilityAccnt);
        	list.add(adResponsibilityKeeper);
        	list.add(adResponsibilityFinance);
        	this.setFormFunctionResponsibility(list, adFormFunction, CMP_CODE);

        	adFormFunction = adFormFunctionHome.findByPrimaryKey(new Integer(4509));
			list = new ArrayList();
        	list.add(adResponsibility);
        	list.add(adResponsibilityAccnt);
        	list.add(adResponsibilityKeeper);
        	list.add(adResponsibilityFinance);
        	this.setFormFunctionResponsibility(list, adFormFunction, CMP_CODE);

        	adFormFunction = adFormFunctionHome.findByPrimaryKey(new Integer(4510));
			list = new ArrayList();
        	list.add(adResponsibility);
        	list.add(adResponsibilityAccnt);
        	list.add(adResponsibilityKeeper);
        	list.add(adResponsibilityFinance);
        	this.setFormFunctionResponsibility(list, adFormFunction, CMP_CODE);

        	adFormFunction = adFormFunctionHome.findByPrimaryKey(new Integer(4511));
			list = new ArrayList();
        	list.add(adResponsibility);
        	list.add(adResponsibilityAccnt);
        	list.add(adResponsibilityKeeper);
        	list.add(adResponsibilityFinance);
        	this.setFormFunctionResponsibility(list, adFormFunction, CMP_CODE);

			adFormFunction = adFormFunctionHome.findByPrimaryKey(new Integer(5001));
			list = new ArrayList();
        	list.add(adResponsibility);
        	list.add(adResponsibilityAccnt);
        	list.add(adResponsibilityKeeper);
        	list.add(adResponsibilityFinance);
        	this.setFormFunctionResponsibility(list, adFormFunction, CMP_CODE);

			adFormFunction = adFormFunctionHome.findByPrimaryKey(new Integer(5002));
			list = new ArrayList();
        	list.add(adResponsibility);
        	list.add(adResponsibilityAccnt);
        	list.add(adResponsibilityKeeper);
        	list.add(adResponsibilityFinance);
        	this.setFormFunctionResponsibility(list, adFormFunction, CMP_CODE);

			adFormFunction = adFormFunctionHome.findByPrimaryKey(new Integer(5003));
			list = new ArrayList();
        	list.add(adResponsibility);
        	list.add(adResponsibilityAccnt);
        	list.add(adResponsibilityKeeper);
        	list.add(adResponsibilityFinance);
        	this.setFormFunctionResponsibility(list, adFormFunction, CMP_CODE);

			adFormFunction = adFormFunctionHome.findByPrimaryKey(new Integer(5004));
			list = new ArrayList();
        	list.add(adResponsibility);
        	list.add(adResponsibilityAccnt);
        	list.add(adResponsibilityKeeper);
        	list.add(adResponsibilityFinance);
        	this.setFormFunctionResponsibility(list, adFormFunction, CMP_CODE);

			adFormFunction = adFormFunctionHome.findByPrimaryKey(new Integer(5005));
			list = new ArrayList();
        	list.add(adResponsibility);
        	list.add(adResponsibilityAccnt);
        	list.add(adResponsibilityKeeper);
        	list.add(adResponsibilityFinance);
        	this.setFormFunctionResponsibility(list, adFormFunction, CMP_CODE);

			adFormFunction = adFormFunctionHome.findByPrimaryKey(new Integer(5006));
			list = new ArrayList();
        	list.add(adResponsibility);
        	list.add(adResponsibilityAccnt);
        	list.add(adResponsibilityKeeper);
        	list.add(adResponsibilityFinance);
        	this.setFormFunctionResponsibility(list, adFormFunction, CMP_CODE);

			adFormFunction = adFormFunctionHome.findByPrimaryKey(new Integer(5007));
			list = new ArrayList();
        	list.add(adResponsibility);
        	list.add(adResponsibilityAccnt);
        	list.add(adResponsibilityKeeper);
        	list.add(adResponsibilityFinance);
        	this.setFormFunctionResponsibility(list, adFormFunction, CMP_CODE);

			adFormFunction = adFormFunctionHome.findByPrimaryKey(new Integer(5008));
			list = new ArrayList();
        	list.add(adResponsibility);
        	list.add(adResponsibilityAccnt);
        	list.add(adResponsibilityKeeper);
        	list.add(adResponsibilityFinance);
        	this.setFormFunctionResponsibility(list, adFormFunction, CMP_CODE);

			adFormFunction = adFormFunctionHome.findByPrimaryKey(new Integer(5009));
			list = new ArrayList();
        	list.add(adResponsibility);
        	list.add(adResponsibilityAccnt);
        	list.add(adResponsibilityKeeper);
        	list.add(adResponsibilityFinance);
        	this.setFormFunctionResponsibility(list, adFormFunction, CMP_CODE);

			adFormFunction = adFormFunctionHome.findByPrimaryKey(new Integer(5010));
			list = new ArrayList();
        	list.add(adResponsibility);
        	list.add(adResponsibilityAccnt);
        	list.add(adResponsibilityKeeper);
        	list.add(adResponsibilityFinance);
        	this.setFormFunctionResponsibility(list, adFormFunction, CMP_CODE);

        	adFormFunction = adFormFunctionHome.findByPrimaryKey(new Integer(5011));
        	list = new ArrayList();
        	list.add(adResponsibility);
        	list.add(adResponsibilityAccnt);
        	list.add(adResponsibilityKeeper);
        	list.add(adResponsibilityFinance);
        	this.setFormFunctionResponsibility(list, adFormFunction, CMP_CODE);

        	adFormFunction = adFormFunctionHome.findByPrimaryKey(new Integer(5012));
        	list = new ArrayList();
        	list.add(adResponsibility);
        	list.add(adResponsibilityAccnt);
        	list.add(adResponsibilityKeeper);
        	list.add(adResponsibilityFinance);
        	this.setFormFunctionResponsibility(list, adFormFunction, CMP_CODE);

        	adFormFunction = adFormFunctionHome.findByPrimaryKey(new Integer(5013));
        	list = new ArrayList();
        	list.add(adResponsibility);
        	list.add(adResponsibilityAccnt);
        	list.add(adResponsibilityKeeper);
        	list.add(adResponsibilityFinance);
        	this.setFormFunctionResponsibility(list, adFormFunction, CMP_CODE);

        	adFormFunction = adFormFunctionHome.findByPrimaryKey(new Integer(5014));
        	list = new ArrayList();
        	list.add(adResponsibility);
        	list.add(adResponsibilityAccnt);
        	list.add(adResponsibilityKeeper);
        	list.add(adResponsibilityFinance);
        	this.setFormFunctionResponsibility(list, adFormFunction, CMP_CODE);

        	adFormFunction = adFormFunctionHome.findByPrimaryKey(new Integer(5015));
        	list = new ArrayList();
        	list.add(adResponsibility);
        	list.add(adResponsibilityAccnt);
        	list.add(adResponsibilityKeeper);
        	list.add(adResponsibilityFinance);
        	this.setFormFunctionResponsibility(list, adFormFunction, CMP_CODE);

        	adFormFunction = adFormFunctionHome.findByPrimaryKey(new Integer(5016));
        	list = new ArrayList();
        	list.add(adResponsibility);
        	list.add(adResponsibilityAccnt);
        	list.add(adResponsibilityKeeper);
        	list.add(adResponsibilityFinance);
        	this.setFormFunctionResponsibility(list, adFormFunction, CMP_CODE);

        	adFormFunction = adFormFunctionHome.findByPrimaryKey(new Integer(5017));
        	list = new ArrayList();
        	list.add(adResponsibility);
        	list.add(adResponsibilityAccnt);
        	list.add(adResponsibilityKeeper);
        	list.add(adResponsibilityFinance);
        	this.setFormFunctionResponsibility(list, adFormFunction, CMP_CODE);

        	adFormFunction = adFormFunctionHome.findByPrimaryKey(new Integer(5018));
        	list = new ArrayList();
        	list.add(adResponsibility);
        	list.add(adResponsibilityAccnt);
        	list.add(adResponsibilityKeeper);
        	list.add(adResponsibilityFinance);
        	this.setFormFunctionResponsibility(list, adFormFunction, CMP_CODE);

        	adFormFunction = adFormFunctionHome.findByPrimaryKey(new Integer(5019));
        	list = new ArrayList();
        	list.add(adResponsibility);
        	list.add(adResponsibilityAccnt);
        	list.add(adResponsibilityKeeper);
        	list.add(adResponsibilityFinance);
        	this.setFormFunctionResponsibility(list, adFormFunction, CMP_CODE);

        	adFormFunction = adFormFunctionHome.findByPrimaryKey(new Integer(5020));
        	list = new ArrayList();
        	list.add(adResponsibility);
        	list.add(adResponsibilityAccnt);
        	list.add(adResponsibilityKeeper);
        	list.add(adResponsibilityFinance);
        	this.setFormFunctionResponsibility(list, adFormFunction, CMP_CODE);

        	adFormFunction = adFormFunctionHome.findByPrimaryKey(new Integer(5021));
        	list = new ArrayList();
        	list.add(adResponsibility);
        	list.add(adResponsibilityAccnt);
        	list.add(adResponsibilityKeeper);
        	list.add(adResponsibilityFinance);
        	this.setFormFunctionResponsibility(list, adFormFunction, CMP_CODE);

        	adFormFunction = adFormFunctionHome.findByPrimaryKey(new Integer(5022));
        	list = new ArrayList();
        	list.add(adResponsibility);
        	list.add(adResponsibilityAccnt);
        	list.add(adResponsibilityKeeper);
        	list.add(adResponsibilityFinance);
        	this.setFormFunctionResponsibility(list, adFormFunction, CMP_CODE);

        	adFormFunction = adFormFunctionHome.findByPrimaryKey(new Integer(5023));
        	list = new ArrayList();
        	list.add(adResponsibility);
        	list.add(adResponsibilityAccnt);
        	list.add(adResponsibilityKeeper);
        	list.add(adResponsibilityFinance);
        	this.setFormFunctionResponsibility(list, adFormFunction, CMP_CODE);

        	adFormFunction = adFormFunctionHome.findByPrimaryKey(new Integer(5024));
        	list = new ArrayList();
        	list.add(adResponsibility);
        	list.add(adResponsibilityAccnt);
        	list.add(adResponsibilityKeeper);
        	list.add(adResponsibilityFinance);
        	this.setFormFunctionResponsibility(list, adFormFunction, CMP_CODE);

        	adFormFunction = adFormFunctionHome.findByPrimaryKey(new Integer(5301));
        	list = new ArrayList();
        	list.add(adResponsibility);
        	list.add(adResponsibilityFinance);
        	this.setFormFunctionResponsibility(list, adFormFunction, CMP_CODE);

			adFormFunction = adFormFunctionHome.findByPrimaryKey(new Integer(5302));
			list = new ArrayList();
        	list.add(adResponsibility);
        	list.add(adResponsibilityFinance);
        	this.setFormFunctionResponsibility(list, adFormFunction, CMP_CODE);

			adFormFunction = adFormFunctionHome.findByPrimaryKey(new Integer(5303));
			list = new ArrayList();
        	list.add(adResponsibility);
        	list.add(adResponsibilityFinance);
        	this.setFormFunctionResponsibility(list, adFormFunction, CMP_CODE);

			adFormFunction = adFormFunctionHome.findByPrimaryKey(new Integer(5304));
			list = new ArrayList();
        	list.add(adResponsibility);
        	list.add(adResponsibilityFinance);
        	this.setFormFunctionResponsibility(list, adFormFunction, CMP_CODE);

			adFormFunction = adFormFunctionHome.findByPrimaryKey(new Integer(5305));
			list = new ArrayList();
        	list.add(adResponsibility);
        	list.add(adResponsibilityFinance);
        	this.setFormFunctionResponsibility(list, adFormFunction, CMP_CODE);

			adFormFunction = adFormFunctionHome.findByPrimaryKey(new Integer(5306));
			list = new ArrayList();
        	list.add(adResponsibility);
        	list.add(adResponsibilityFinance);
        	this.setFormFunctionResponsibility(list, adFormFunction, CMP_CODE);

			adFormFunction = adFormFunctionHome.findByPrimaryKey(new Integer(5307));
			list = new ArrayList();
        	list.add(adResponsibility);
        	list.add(adResponsibilityFinance);
        	this.setFormFunctionResponsibility(list, adFormFunction, CMP_CODE);

        	adFormFunction = adFormFunctionHome.findByPrimaryKey(new Integer(5308));
        	list = new ArrayList();
        	list.add(adResponsibility);
        	list.add(adResponsibilityFinance);
        	this.setFormFunctionResponsibility(list, adFormFunction, CMP_CODE);

			adFormFunction = adFormFunctionHome.findByPrimaryKey(new Integer(5309));
			list = new ArrayList();
        	list.add(adResponsibility);
        	list.add(adResponsibilityFinance);
        	this.setFormFunctionResponsibility(list, adFormFunction, CMP_CODE);

        	adFormFunction = adFormFunctionHome.findByPrimaryKey(new Integer(5310));
        	list = new ArrayList();
        	list.add(adResponsibility);
        	list.add(adResponsibilityFinance);
        	this.setFormFunctionResponsibility(list, adFormFunction, CMP_CODE);

        	adFormFunction = adFormFunctionHome.findByPrimaryKey(new Integer(5311));
        	list = new ArrayList();
        	list.add(adResponsibility);
        	list.add(adResponsibilityFinance);
        	this.setFormFunctionResponsibility(list, adFormFunction, CMP_CODE);

        	adFormFunction = adFormFunctionHome.findByPrimaryKey(new Integer(5312));
        	list = new ArrayList();
        	list.add(adResponsibility);
        	list.add(adResponsibilityFinance);
        	this.setFormFunctionResponsibility(list, adFormFunction, CMP_CODE);

        	adFormFunction = adFormFunctionHome.findByPrimaryKey(new Integer(5313));
        	list = new ArrayList();
        	list.add(adResponsibility);
        	list.add(adResponsibilityFinance);
        	this.setFormFunctionResponsibility(list, adFormFunction, CMP_CODE);

			adFormFunction = adFormFunctionHome.findByPrimaryKey(new Integer(5501));
			list = new ArrayList();
        	list.add(adResponsibility);
        	list.add(adResponsibilityAccnt);
        	list.add(adResponsibilityFinance);
        	this.setFormFunctionResponsibility(list, adFormFunction, CMP_CODE);

			adFormFunction = adFormFunctionHome.findByPrimaryKey(new Integer(5502));
			list = new ArrayList();
        	list.add(adResponsibility);
        	list.add(adResponsibilityAccnt);
        	list.add(adResponsibilityFinance);
        	this.setFormFunctionResponsibility(list, adFormFunction, CMP_CODE);

			adFormFunction = adFormFunctionHome.findByPrimaryKey(new Integer(5503));
			list = new ArrayList();
        	list.add(adResponsibility);
        	list.add(adResponsibilityAccnt);
        	list.add(adResponsibilityFinance);
        	this.setFormFunctionResponsibility(list, adFormFunction, CMP_CODE);

			adFormFunction = adFormFunctionHome.findByPrimaryKey(new Integer(5504));
			list = new ArrayList();
        	list.add(adResponsibility);
        	list.add(adResponsibilityAccnt);
        	list.add(adResponsibilityFinance);
        	this.setFormFunctionResponsibility(list, adFormFunction, CMP_CODE);

			adFormFunction = adFormFunctionHome.findByPrimaryKey(new Integer(5505));
			list = new ArrayList();
        	list.add(adResponsibility);
        	list.add(adResponsibilityAccnt);
        	list.add(adResponsibilityFinance);
        	this.setFormFunctionResponsibility(list, adFormFunction, CMP_CODE);

        	adFormFunction = adFormFunctionHome.findByPrimaryKey(new Integer(5506));
        	list = new ArrayList();
        	list.add(adResponsibility);
        	list.add(adResponsibilityAccnt);
        	list.add(adResponsibilityFinance);
        	this.setFormFunctionResponsibility(list, adFormFunction, CMP_CODE);

        	adFormFunction = adFormFunctionHome.findByPrimaryKey(new Integer(5507));
        	list = new ArrayList();
        	list.add(adResponsibility);
        	list.add(adResponsibilityAccnt);
        	list.add(adResponsibilityFinance);
        	this.setFormFunctionResponsibility(list, adFormFunction, CMP_CODE);

        	adFormFunction = adFormFunctionHome.findByPrimaryKey(new Integer(5508));
        	list = new ArrayList();
        	list.add(adResponsibility);
        	list.add(adResponsibilityAccnt);
        	list.add(adResponsibilityKeeper);
        	list.add(adResponsibilityFinance);
        	this.setFormFunctionResponsibility(list, adFormFunction, CMP_CODE);

        	adFormFunction = adFormFunctionHome.findByPrimaryKey(new Integer(5509));
        	list = new ArrayList();
        	list.add(adResponsibility);
        	list.add(adResponsibilityAccnt);
        	list.add(adResponsibilityKeeper);
        	list.add(adResponsibilityFinance);
        	this.setFormFunctionResponsibility(list, adFormFunction, CMP_CODE);

        	adFormFunction = adFormFunctionHome.findByPrimaryKey(new Integer(5510));
        	list = new ArrayList();
        	list.add(adResponsibility);
        	list.add(adResponsibilityAccnt);
        	list.add(adResponsibilityKeeper);
        	list.add(adResponsibilityFinance);
        	this.setFormFunctionResponsibility(list, adFormFunction, CMP_CODE);

        	adFormFunction = adFormFunctionHome.findByPrimaryKey(new Integer(5511));
        	list = new ArrayList();
        	list.add(adResponsibility);
        	list.add(adResponsibilityAccnt);
        	list.add(adResponsibilityKeeper);
        	list.add(adResponsibilityFinance);
        	this.setFormFunctionResponsibility(list, adFormFunction, CMP_CODE);

        	adFormFunction = adFormFunctionHome.findByPrimaryKey(new Integer(5512));
        	list = new ArrayList();
        	list.add(adResponsibility);
        	list.add(adResponsibilityAccnt);
        	list.add(adResponsibilityKeeper);
        	list.add(adResponsibilityFinance);
        	this.setFormFunctionResponsibility(list, adFormFunction, CMP_CODE);

        	adFormFunction = adFormFunctionHome.findByPrimaryKey(new Integer(5513));
        	list = new ArrayList();
        	list.add(adResponsibility);
        	list.add(adResponsibilityAccnt);
        	list.add(adResponsibilityKeeper);
        	list.add(adResponsibilityFinance);
        	this.setFormFunctionResponsibility(list, adFormFunction, CMP_CODE);

        	adFormFunction = adFormFunctionHome.findByPrimaryKey(new Integer(5514));
        	list = new ArrayList();
        	list.add(adResponsibility);
        	list.add(adResponsibilityAccnt);
        	list.add(adResponsibilityKeeper);
        	list.add(adResponsibilityFinance);
        	this.setFormFunctionResponsibility(list, adFormFunction, CMP_CODE);

        	adFormFunction = adFormFunctionHome.findByPrimaryKey(new Integer(5515));
        	list = new ArrayList();
        	list.add(adResponsibility);
        	list.add(adResponsibilityAccnt);
        	list.add(adResponsibilityKeeper);
        	list.add(adResponsibilityFinance);
        	this.setFormFunctionResponsibility(list, adFormFunction, CMP_CODE);

        	adFormFunction = adFormFunctionHome.findByPrimaryKey(new Integer(5516));
        	list = new ArrayList();
        	list.add(adResponsibility);
        	list.add(adResponsibilityAccnt);
        	list.add(adResponsibilityKeeper);
        	list.add(adResponsibilityFinance);
        	this.setFormFunctionResponsibility(list, adFormFunction, CMP_CODE);

        	adFormFunction = adFormFunctionHome.findByPrimaryKey(new Integer(5517));
        	list = new ArrayList();
        	list.add(adResponsibility);
        	list.add(adResponsibilityAccnt);
        	list.add(adResponsibilityKeeper);
        	list.add(adResponsibilityFinance);
        	this.setFormFunctionResponsibility(list, adFormFunction, CMP_CODE);

        	adFormFunction = adFormFunctionHome.findByPrimaryKey(new Integer(5518));
        	list = new ArrayList();
        	list.add(adResponsibility);
        	list.add(adResponsibilityAccnt);
        	list.add(adResponsibilityKeeper);
        	list.add(adResponsibilityFinance);
        	this.setFormFunctionResponsibility(list, adFormFunction, CMP_CODE);

        	adFormFunction = adFormFunctionHome.findByPrimaryKey(new Integer(5519));
        	list = new ArrayList();
        	list.add(adResponsibility);
        	list.add(adResponsibilityAccnt);
        	list.add(adResponsibilityKeeper);
        	list.add(adResponsibilityFinance);
        	this.setFormFunctionResponsibility(list, adFormFunction, CMP_CODE);

        	adFormFunction = adFormFunctionHome.findByPrimaryKey(new Integer(5520));
        	list = new ArrayList();
        	list.add(adResponsibility);
        	list.add(adResponsibilityAccnt);
        	list.add(adResponsibilityKeeper);
        	list.add(adResponsibilityFinance);
        	this.setFormFunctionResponsibility(list, adFormFunction, CMP_CODE);

        	adFormFunction = adFormFunctionHome.findByPrimaryKey(new Integer(5521));
        	list = new ArrayList();
        	list.add(adResponsibility);
        	list.add(adResponsibilityAccnt);
        	list.add(adResponsibilityKeeper);
        	list.add(adResponsibilityFinance);
        	this.setFormFunctionResponsibility(list, adFormFunction, CMP_CODE);

        	adFormFunction = adFormFunctionHome.findByPrimaryKey(new Integer(5522));
        	list = new ArrayList();
        	list.add(adResponsibility);
        	list.add(adResponsibilityAccnt);
        	list.add(adResponsibilityKeeper);
        	list.add(adResponsibilityFinance);
        	this.setFormFunctionResponsibility(list, adFormFunction, CMP_CODE);

        	adFormFunction = adFormFunctionHome.findByPrimaryKey(new Integer(5523));
        	list = new ArrayList();
        	list.add(adResponsibility);
        	list.add(adResponsibilityAccnt);
        	list.add(adResponsibilityKeeper);
        	list.add(adResponsibilityFinance);
        	this.setFormFunctionResponsibility(list, adFormFunction, CMP_CODE);

        	adFormFunction = adFormFunctionHome.findByPrimaryKey(new Integer(5524));
        	list = new ArrayList();
        	list.add(adResponsibility);
        	list.add(adResponsibilityAccnt);
        	list.add(adResponsibilityKeeper);
        	list.add(adResponsibilityFinance);
        	this.setFormFunctionResponsibility(list, adFormFunction, CMP_CODE);

        	adFormFunction = adFormFunctionHome.findByPrimaryKey(new Integer(5525));
        	list = new ArrayList();
        	list.add(adResponsibility);
        	list.add(adResponsibilityAccnt);
        	list.add(adResponsibilityKeeper);
        	list.add(adResponsibilityFinance);
        	this.setFormFunctionResponsibility(list, adFormFunction, CMP_CODE); System.out.println("441");

        	adFormFunction = adFormFunctionHome.findByPrimaryKey(new Integer(5527));
        	list = new ArrayList();
        	list.add(adResponsibility);
        	list.add(adResponsibilityAccnt);
        	list.add(adResponsibilityKeeper);
        	list.add(adResponsibilityFinance);
        	this.setFormFunctionResponsibility(list, adFormFunction, CMP_CODE); System.out.println("442");

        	adFormFunction = adFormFunctionHome.findByPrimaryKey(new Integer(5528));
        	list = new ArrayList();
        	list.add(adResponsibility);
        	list.add(adResponsibilityAccnt);
        	list.add(adResponsibilityKeeper);
        	list.add(adResponsibilityFinance);
        	this.setFormFunctionResponsibility(list, adFormFunction, CMP_CODE); System.out.println("443");

        	adFormFunction = adFormFunctionHome.findByPrimaryKey(new Integer(5529));
        	list = new ArrayList();
        	list.add(adResponsibility);
        	list.add(adResponsibilityAccnt);
        	list.add(adResponsibilityKeeper);
        	list.add(adResponsibilityFinance);
        	this.setFormFunctionResponsibility(list, adFormFunction, CMP_CODE); System.out.println("444");

        	adFormFunction = adFormFunctionHome.findByPrimaryKey(new Integer(5530));
        	list = new ArrayList();
        	list.add(adResponsibility);
        	list.add(adResponsibilityAccnt);
        	list.add(adResponsibilityKeeper);
        	list.add(adResponsibilityFinance);
        	this.setFormFunctionResponsibility(list, adFormFunction, CMP_CODE); System.out.println("445");

        	adFormFunction = adFormFunctionHome.findByPrimaryKey(new Integer(5531));
        	list = new ArrayList();
        	list.add(adResponsibility);
        	list.add(adResponsibilityAccnt);
        	list.add(adResponsibilityKeeper);
        	list.add(adResponsibilityFinance);
        	this.setFormFunctionResponsibility(list, adFormFunction, CMP_CODE); System.out.println("446");



        	adFormFunction = adFormFunctionHome.findByPrimaryKey(new Integer(6000));
        	list = new ArrayList();
        	list.add(adResponsibility);
        	list.add(adResponsibilityAccnt);
        	list.add(adResponsibilityKeeper);
        	list.add(adResponsibilityFinance);
        	this.setFormFunctionResponsibility(list, adFormFunction, CMP_CODE); System.out.println("445");


        	adFormFunction = adFormFunctionHome.findByPrimaryKey(new Integer(7000));
        	list = new ArrayList();
        	list.add(adResponsibility);
        	list.add(adResponsibilityAccnt);
        	list.add(adResponsibilityKeeper);
        	list.add(adResponsibilityFinance);
        	this.setFormFunctionResponsibility(list, adFormFunction, CMP_CODE); System.out.println("445");


        	// user

        	LocalAdUser adUser = adUserHome.create("admin", "default", "Administrator", " ", "", (byte)0, (byte)0,  this.encryptPassword("admin"), (short)2, (short)0, (short)0, (short)0, new GregorianCalendar(2000, 0, 1, 0, 0, 0).getTime(), new GregorianCalendar(2050, 0, 1, 0, 0, 0).getTime(), CMP_CODE);
        	LocalAdUserResponsibility adUserResponsibility = adUserResponsibilityHome.create(new GregorianCalendar(2000, 0, 1, 0, 0, 0).getTime(), new GregorianCalendar(2050, 0, 1, 0, 0, 0).getTime(), CMP_CODE);
        	adUser.addAdUserResponsibility(adUserResponsibility);
        	adResponsibility.addAdUserResponsibility(adUserResponsibility);
        	Integer USR_CODE = adUser.getUsrCode();

        	// lookup

        	LocalAdLookUp adLookUp = adLookUpHome.create("AR FREIGHT", "AR FREIGHT", CMP_CODE);
        	LocalAdLookUpValue adLookUpValue = adLookUpValueHome.create("DHL", "DHL", null, EJBCommon.TRUE, 'N',CMP_CODE);
        	adLookUp.addAdLookUpValue(adLookUpValue);

        	adLookUpHome.create("AD DEPARTMENT", "AD DEPARTMENT", CMP_CODE);




        	adLookUpHome.create("AR REGION", "AR REGION", CMP_CODE);
        	adLookUpHome.create("AR CUSTOMER BATCH - SOA", "AR CUSTOMER BATCH - SOA", CMP_CODE);
        	adLookUpHome.create("AR CUSTOMER DEPARTMENT - SOA", "AR CUSTOMER DEPARTMENT - SOA", CMP_CODE);
        	adLookUpHome.create("AR REPORT TYPE - STATEMENT OF ACCOUNT", "AR REPORT TYPE - STATEMENT OF ACCOUNT", CMP_CODE);
        	adLookUpHome.create("AR REPORT TYPE - SALES REGISTER", "AR REPORT TYPE - SALES REGISTER", CMP_CODE);
        	adLookUpHome.create("AR REPORT TYPE - JOB ORDER", "AR REPORT TYPE - JOB ORDER", CMP_CODE);
            adLookUpHome.create("AR INVOICE ITEM PRINT TYPE", "AR INVOICE ITEM PRINT TYPE", CMP_CODE);
            adLookUpHome.create("AR INVOICE MEMO LINE PRINT TYPE", "AR INVOICE MEMO LINE PRINT TYPE", CMP_CODE);
            adLookUpHome.create("AR INVOICE SO MATCHED PRINT TYPE", "AR INVOICE SO MATCHED PRINT TYPE", CMP_CODE);
            adLookUpHome.create("AR INVOICE SO MATCHED PRINT TYPE", "AR INVOICE SO MATCHED PRINT TYPE", CMP_CODE);
            adLookUpHome.create("AR SALES ORDER PRINT TYPE", "AR SALES ORDER PRINT TYPE", CMP_CODE);


            adLookUpHome.create("AR SALES ORDER TYPE", "AR SALES ORDER TYPE", CMP_CODE);
            
            adLookUpHome.create("AR JOB ORDER - TECHNICIANS", "AR JOB ORDER - TECHNICIANS", CMP_CODE);
         

        	adLookUpHome.create("AR PRINT INVOICE PARAMETER", "AR PRINT INVOICE PARAMETER", CMP_CODE);
        	adLookUpHome.create("AR PRINT CREDIT MEMO PARAMETER", "AR PRINT INVOICE PARAMETER", CMP_CODE);
        	adLookUpHome.create("AR PRINT SALES ORDER PARAMETER", "AR PRINT SALES ORDER PARAMETER", CMP_CODE);
        	adLookUpHome.create("AR PRINT RECEIPT PARAMETER", "AR PRINT RECEIPT PARAMETER", CMP_CODE);
        	adLookUpHome.create("AR PRINT MISC RECEIPT PARAMETER", "AR PRINT MISC RECEIPT PARAMETER", CMP_CODE);

            adLookUpHome.create("AP PR TYPE", "AP PR TYPE", CMP_CODE);
        	adLookUpHome.create("INV LOCATION TYPE", "INV LOCATION TYPE", CMP_CODE);
        	adLookUpHome.create("INV ITEM CATEGORY", "INV ITEM CATEGORY", CMP_CODE);
        	adLookUpHome.create("INV UNIT OF MEASURE CLASS", "INV UNIT OF MEASURE CLASS", CMP_CODE);
        	adLookUpHome.create("INV SHIFT", "INV SHIFT", CMP_CODE);
        	adLookUpHome.create("INV DONENESS", "INV DONENESS", CMP_CODE);
        	adLookUpHome.create("INV PRICE LEVEL", "INV PRICE LEVEL", CMP_CODE);
        	adLookUpHome.create("INV REPORT TYPE - INVENTORY LIST", "INV REPORT TYPE - INVENTORY LIST", CMP_CODE);
        	adLookUpHome.create("INV REPORT TYPE - BUILD UNBUILD ASSEMBLY ORDER", "INV REPORT TYPE - BUILD UNBUILD ASSEMBLY ORDER", CMP_CODE);

        	adLookUpHome.create("GL REPORT TYPE - GENERAL LEDGER", "GL REPORT TYPE - GENERAL LEDGER", CMP_CODE);
        	adLookUpHome.create("AR CUSTOMER DEPARTMENT - SOA", "AR CUSTOMER DEPARTMENT - SOA", CMP_CODE);

        	adLookUpHome.create("GL REPORT TYPE - MONTHLY VAT RETURN", "GL REPORT TYPE - MONTHLY VAT RETURN", CMP_CODE);
        	adLookUpHome.create("GL REPORT TYPE - QUARTERLY VAT RETURN", "GL REPORT TYPE - QUARTERLY VAT RETURN", CMP_CODE);
            adLookUpHome.create("GL REPORT TYPE - INCOME TAX WITHHELD", "GL REPORT TYPE - INCOME TAX WITHHELD", CMP_CODE);

            //document sequence assignment type
            adLookUpHome.create("GL JOURNAL DOCUMENT TYPE", "GL JOURNAL DOCUMENT TYPE", CMP_CODE);
            adLookUpHome.create("AP PURCHASE REQUISITION DOCUMENT TYPE", "AP PURCHASE REQUISITION DOCUMENT TYPE", CMP_CODE);
            adLookUpHome.create("AP CANVASS DOCUMENT TYPE", "AP CANVASS DOCUMENT TYPE", CMP_CODE);
            adLookUpHome.create("AP PURCHASE ORDER DOCUMENT TYPE", "AP PURCHASE ORDER DOCUMENT TYPE", CMP_CODE);
            adLookUpHome.create("AP RECEIVING ITEM DOCUMENT TYPE", "AP RECEIVING ITEM DOCUMENT TYPE", CMP_CODE);
            adLookUpHome.create("AP VOUCHER DOCUMENT TYPE", "AP VOUCHER DOCUMENT TYPE", CMP_CODE);
            adLookUpHome.create("AP DEBIT MEMO DOCUMENT TYPE", "AP DEBIT MEMO DOCUMENT TYPE", CMP_CODE);
            adLookUpHome.create("AP CHECK DOCUMENT TYPE", "AP CHECK DOCUMENT TYPE", CMP_CODE);
            adLookUpHome.create("AP CHECK PAYMENT REQUEST DOCUMENT TYPE", "AP CHECK PAYMENT REQUEST DOCUMENT TYPE", CMP_CODE);
            adLookUpHome.create("AR SALES ORDER DOCUMENT TYPE", "AR SALES ORDER DOCUMENT TYPE", CMP_CODE);
            adLookUpHome.create("AR JOB ORDER DOCUMENT TYPE", "AR JOB ORDER DOCUMENT TYPE", CMP_CODE);
            adLookUpHome.create("AR INVOICE DOCUMENT TYPE", "AR INVOICE DOCUMENT TYPE", CMP_CODE);
            adLookUpHome.create("AR CREDIT MEMO DOCUMENT TYPE", "AR CREDIT MEMO DOCUMENT TYPE", CMP_CODE);
            adLookUpHome.create("AR RECEIPT DOCUMENT TYPE", "AR RECEIPT DOCUMENT TYPE", CMP_CODE);
            adLookUpHome.create("AR STATEMENT OF ACCOUNT DOCUMENT TYPE", "AR STATEMENT OF ACCOUNT DOCUMENT TYPE", CMP_CODE);
            adLookUpHome.create("AR ACKNOWLEDGEMENT RECEIPT DOCUMENT TYPE", "AR ACKNOWLEDGEMENT RECEIPT DOCUMENT TYPE", CMP_CODE);
            adLookUpHome.create("CM ADJUSTMENT DOCUMENT TYPE", "CM ADJUSTMENT DOCUMENT TYPE", CMP_CODE);
            adLookUpHome.create("CM FUND TRANSFER DOCUMENT TYPE", "CM FUND TRANSFER DOCUMENT TYPE", CMP_CODE);
            adLookUpHome.create("INV ADJUSTMENT REQUEST DOCUMENT TYPE", "INV ADJUSTMENT REQUEST DOCUMENT TYPE", CMP_CODE);
            adLookUpHome.create("INV ADJUSTMENT DOCUMENT TYPE", "INV ADJUSTMENT DOCUMENT TYPE", CMP_CODE);
            adLookUpHome.create("INV BRANCH STOCK TRANSFER ORDER DOCUMENT TYPE", "INV BRANCH STOCK TRANSFER ORDER DOCUMENT TYPE", CMP_CODE);
            adLookUpHome.create("INV BRANCH STOCK TRANSFER-OUT DOCUMENT TYPE", "INV BRANCH STOCK TRANSFER-OUT DOCUMENT TYPE", CMP_CODE);
            adLookUpHome.create("INV BRANCH STOCK TRANSFER-IN DOCUMENT TYPE", "INV BRANCH STOCK TRANSFER-IN DOCUMENT TYPE", CMP_CODE);
            adLookUpHome.create("INV STOCK TRANSFER DOCUMENT TYPE", "INV STOCK TRANSFER DOCUMENT TYPE", CMP_CODE);
            adLookUpHome.create("INV BUILD ASSEMBLY ORDER DOCUMENT TYPE", "INV BUILD ASSEMBLY ORDER DOCUMENT TYPE", CMP_CODE);
            adLookUpHome.create("INV BUILD ASSEMBLY DOCUMENT TYPE", "INV BUILD ASSEMBLY DOCUMENT TYPE", CMP_CODE);
            adLookUpHome.create("INV OVERHEAD DOCUMENT TYPE", "INV OVERHEAD DOCUMENT TYPE", CMP_CODE);
            adLookUpHome.create("INV BUILD ORDER DOCUMENT TYPE", "INV BUILD ORDER DOCUMENT TYPE", CMP_CODE);
            adLookUpHome.create("INV STOCK ISSUANCE DOCUMENT TYPE", "INV STOCK ISSUANCE DOCUMENT TYPE", CMP_CODE);
            adLookUpHome.create("INV ASSEMBLY TRANSFER DOCUMENT TYPE", "INV ASSEMBLY TRANSFER DOCUMENT TYPE", CMP_CODE);
            adLookUpHome.create("INV TRANSACTIONAL BUDGET DOCUMENT TYPE", "INV TRANSACTIONAL BUDGET DOCUMENT TYPE", CMP_CODE);

            
             //job order staus 
            
            LocalAdLookUp adLokkUpJobOrderStatus = adLookUpHome.create("AR JOB ORDER STATUS", "AR JOB ORDER STATUS", CMP_CODE);
            
         
            
            
            
        	LocalAdLookUp adLookUpProvinces = adLookUpHome.create("GL PROVINCES - MONTHLY VAT RETURN", "GL PROVINCES - MONTHLY VAT RETURN", CMP_CODE);
        	
        	
        	
        	
        	

        	LocalAdLookUpValue adLookUpProvinces1 = adLookUpValueHome.create("CENTRAL","CENTRAL",null, EJBCommon.TRUE, 'N',CMP_CODE);
        	LocalAdLookUpValue adLookUpProvinces2 = adLookUpValueHome.create("CHIMBU","CHIMBU",null, EJBCommon.TRUE, 'N',CMP_CODE);
        	LocalAdLookUpValue adLookUpProvinces3 = adLookUpValueHome.create("EAST NEW BRITAIN","EAST NEW BRITAIN",null, EJBCommon.TRUE, 'N',CMP_CODE);
        	LocalAdLookUpValue adLookUpProvinces4 = adLookUpValueHome.create("EAST SEPIK","EAST SEPIK",null, EJBCommon.TRUE, 'N',CMP_CODE);
        	LocalAdLookUpValue adLookUpProvinces5 = adLookUpValueHome.create("EASTERN HIGHLANDS","EASTERN HIGHLANDS",null, EJBCommon.TRUE, 'N',CMP_CODE);
        	LocalAdLookUpValue adLookUpProvinces6 = adLookUpValueHome.create("ENGA","ENGA",null, EJBCommon.TRUE, 'N',CMP_CODE);
        	LocalAdLookUpValue adLookUpProvinces7 = adLookUpValueHome.create("GULF","GULF",null, EJBCommon.TRUE, 'N',CMP_CODE);
        	LocalAdLookUpValue adLookUpProvinces8 = adLookUpValueHome.create("MADANG","MADANG",null, EJBCommon.TRUE, 'N',CMP_CODE);
        	LocalAdLookUpValue adLookUpProvinces9 = adLookUpValueHome.create("MANUS","MANUS",null, EJBCommon.TRUE, 'N',CMP_CODE);
        	LocalAdLookUpValue adLookUpProvinces10 = adLookUpValueHome.create("MILNE BAY","MILNE BAY",null, EJBCommon.TRUE, 'N',CMP_CODE);
        	LocalAdLookUpValue adLookUpProvinces11 = adLookUpValueHome.create("MOROBE","MOROBE",null, EJBCommon.TRUE, 'N',CMP_CODE);
        	LocalAdLookUpValue adLookUpProvinces12 = adLookUpValueHome.create("N.C.D.","N.C.D.",null, EJBCommon.TRUE, 'N',CMP_CODE);
        	LocalAdLookUpValue adLookUpProvinces13 = adLookUpValueHome.create("NEW IRELAND","NEW IRELAND",null, EJBCommon.TRUE, 'N',CMP_CODE);
        	LocalAdLookUpValue adLookUpProvinces14 = adLookUpValueHome.create("ORO","ORO",null, EJBCommon.TRUE, 'N',CMP_CODE);
        	LocalAdLookUpValue adLookUpProvinces15 = adLookUpValueHome.create("SANDAUN","SANDAUN",null, EJBCommon.TRUE, 'N',CMP_CODE);
        	LocalAdLookUpValue adLookUpProvinces16 = adLookUpValueHome.create("SOUTHERN HIGHLANDS","SOUTHERN HIGHLANDS",null, EJBCommon.TRUE, 'N',CMP_CODE);
        	LocalAdLookUpValue adLookUpProvinces17 = adLookUpValueHome.create("WEST NEW BRITAIN","WEST NEW BRITAIN",null, EJBCommon.TRUE, 'N',CMP_CODE);
        	LocalAdLookUpValue adLookUpProvinces18 = adLookUpValueHome.create("WESTERN HIGHLANDS","WESTERN HIGHLANDS",null, EJBCommon.TRUE, 'N',CMP_CODE);
        	LocalAdLookUpValue adLookUpProvinces19 = adLookUpValueHome.create("WESTERN PROVINCE","WESTERN PROVINCE",null, EJBCommon.TRUE, 'N',CMP_CODE);

        	adLookUpProvinces.addAdLookUpValue(adLookUpProvinces1);
        	adLookUpProvinces.addAdLookUpValue(adLookUpProvinces2);
        	adLookUpProvinces.addAdLookUpValue(adLookUpProvinces3);
        	adLookUpProvinces.addAdLookUpValue(adLookUpProvinces4);
        	adLookUpProvinces.addAdLookUpValue(adLookUpProvinces5);
        	adLookUpProvinces.addAdLookUpValue(adLookUpProvinces6);
        	adLookUpProvinces.addAdLookUpValue(adLookUpProvinces7);
        	adLookUpProvinces.addAdLookUpValue(adLookUpProvinces8);
        	adLookUpProvinces.addAdLookUpValue(adLookUpProvinces9);
        	adLookUpProvinces.addAdLookUpValue(adLookUpProvinces10);
        	adLookUpProvinces.addAdLookUpValue(adLookUpProvinces11);
        	adLookUpProvinces.addAdLookUpValue(adLookUpProvinces12);
        	adLookUpProvinces.addAdLookUpValue(adLookUpProvinces13);
        	adLookUpProvinces.addAdLookUpValue(adLookUpProvinces14);
        	adLookUpProvinces.addAdLookUpValue(adLookUpProvinces15);
        	adLookUpProvinces.addAdLookUpValue(adLookUpProvinces16);
        	adLookUpProvinces.addAdLookUpValue(adLookUpProvinces17);
        	adLookUpProvinces.addAdLookUpValue(adLookUpProvinces18);
        	adLookUpProvinces.addAdLookUpValue(adLookUpProvinces19);


        	GlRepGeneralLedgerDetails glDetails = new GlRepGeneralLedgerDetails();

        	String[] iitNameList = glDetails.getIITNameList();
        	LocalAdLookUp adLookUpIitCategory = adLookUpHome.create("GL COA CATEGORY - IIT", "GL COA CATEGORY - IIT", CMP_CODE);

        	for(int x=0;x<iitNameList.length;x++){
        		String iitName = iitNameList[x];
        		LocalAdLookUpValue adLookUpValueAccountCategory = adLookUpValueHome.create(iitName,iitName,null, EJBCommon.TRUE, 'N',CMP_CODE);

        		adLookUpIitCategory.addAdLookUpValue(adLookUpValueAccountCategory);
        	}


        	/*

        	LocalAdLookUpValue adLookUpValueAccountCategory1 = adLookUpValueHome.create("Ten","Ten",null, EJBCommon.TRUE, 'N',CMP_CODE);
        	LocalAdLookUpValue adLookUpValueAccountCategory2 = adLookUpValueHome.create("Twenty","Twenty",null, EJBCommon.TRUE, 'N',CMP_CODE);
        	LocalAdLookUpValue adLookUpValueAccountCategory3 = adLookUpValueHome.create("Thirty","Thirty",null, EJBCommon.TRUE, 'N',CMP_CODE);

        	adLookUpIitCategory.addAdLookUpValue(adLookUpValueAccountCategory1);
        	adLookUpIitCategory.addAdLookUpValue(adLookUpValueAccountCategory2);
        	adLookUpIitCategory.addAdLookUpValue(adLookUpValueAccountCategory3);

        	*/

        	String[] sawNameList = glDetails.getSAWNameList();
        	LocalAdLookUp adLookUpSawCategory = adLookUpHome.create("GL COA CATEGORY - SAW","GL COA CATEGORY - SAW", CMP_CODE);

        	for(int x=0;x<sawNameList.length;x++){
        		String sawName = sawNameList[x];
        		LocalAdLookUpValue adLookUpValueAccountCategory = adLookUpValueHome.create(sawName,sawName,null, EJBCommon.TRUE, 'N',CMP_CODE);

        		adLookUpSawCategory.addAdLookUpValue(adLookUpValueAccountCategory);
        	}



        	String[] citNameList = glDetails.getCITNameList();
        	LocalAdLookUp adLookUpCitCategory = adLookUpHome.create("GL COA CATEGORY - CIT", "GL COA CATEGORY - CIT", CMP_CODE);


        	for(int x=0;x<citNameList.length;x++){
        		String citName = citNameList[x];
        		LocalAdLookUpValue adLookUpValueAccountCategory = adLookUpValueHome.create(citName,citName,null, EJBCommon.TRUE, 'N',CMP_CODE);

        		adLookUpCitCategory.addAdLookUpValue(adLookUpValueAccountCategory);
        	}

        	/*
        	LocalAdLookUpValue adLookUpValueAccountCategory10 = adLookUpValueHome.create("Ten","Ten",null, EJBCommon.TRUE, 'N',CMP_CODE);
        	LocalAdLookUpValue adLookUpValueAccountCategory11 = adLookUpValueHome.create("Eleven","Eleven",null, EJBCommon.TRUE, 'N',CMP_CODE);
        	LocalAdLookUpValue adLookUpValueAccountCategory12 = adLookUpValueHome.create("Twelve","Twelve",null, EJBCommon.TRUE, 'N',CMP_CODE);
        	LocalAdLookUpValue adLookUpValueAccountCategory13 = adLookUpValueHome.create("Thirteen","Thirteen",null, EJBCommon.TRUE, 'N',CMP_CODE);
        	LocalAdLookUpValue adLookUpValueAccountCategory14 = adLookUpValueHome.create("Fourteen","Fourteen",null, EJBCommon.TRUE, 'N',CMP_CODE);
        	LocalAdLookUpValue adLookUpValueAccountCategory15 = adLookUpValueHome.create("Fifteen","Fifteen",null, EJBCommon.TRUE, 'N',CMP_CODE);
        	LocalAdLookUpValue adLookUpValueAccountCategory16 = adLookUpValueHome.create("Sixteen","Sixteen",null, EJBCommon.TRUE, 'N',CMP_CODE);
        	LocalAdLookUpValue adLookUpValueAccountCategory17 = adLookUpValueHome.create("Seventeen","Seventeen",null, EJBCommon.TRUE, 'N',CMP_CODE);
        	LocalAdLookUpValue adLookUpValueAccountCategory18 = adLookUpValueHome.create("Eighteen","Eighteen",null, EJBCommon.TRUE, 'N',CMP_CODE);
        	LocalAdLookUpValue adLookUpValueAccountCategory19 = adLookUpValueHome.create("Nineteen","Nineteen",null, EJBCommon.TRUE, 'N',CMP_CODE);
        	LocalAdLookUpValue adLookUpValueAccountCategory20 = adLookUpValueHome.create("Twenty","Twenty",null, EJBCommon.TRUE, 'N',CMP_CODE);
        	LocalAdLookUpValue adLookUpValueAccountCategory100 = adLookUpValueHome.create("OneHundred","OneHundred",null, EJBCommon.TRUE, 'N',CMP_CODE);
        	LocalAdLookUpValue adLookUpValueAccountCategory101 = adLookUpValueHome.create("OneHundredandOne","OneHundredandOne",null, EJBCommon.TRUE, 'N',CMP_CODE);
        	LocalAdLookUpValue adLookUpValueAccountCategory102 = adLookUpValueHome.create("OneHundredandTwo","OneHundredandTwo",null, EJBCommon.TRUE, 'N',CMP_CODE);
        	LocalAdLookUpValue adLookUpValueAccountCategory103 = adLookUpValueHome.create("OneHundredandThree","OneHundredandThree",null, EJBCommon.TRUE, 'N',CMP_CODE);
        	LocalAdLookUpValue adLookUpValueAccountCategory104 = adLookUpValueHome.create("OneHundredandFour","OneHundredandFour",null, EJBCommon.TRUE, 'N',CMP_CODE);
        	LocalAdLookUpValue adLookUpValueAccountCategory105 = adLookUpValueHome.create("OneHundredandFive","OneHundredandFive",null, EJBCommon.TRUE, 'N',CMP_CODE);
        	LocalAdLookUpValue adLookUpValueAccountCategory106 = adLookUpValueHome.create("OneHundredandSix","OneHundredandSix",null, EJBCommon.TRUE, 'N',CMP_CODE);
        	LocalAdLookUpValue adLookUpValueAccountCategory107 = adLookUpValueHome.create("OneHundredandSeven","OneHundredandSeven",null, EJBCommon.TRUE, 'N',CMP_CODE);
        	LocalAdLookUpValue adLookUpValueAccountCategory108 = adLookUpValueHome.create("OneHundredandEight","OneHundredandEight",null, EJBCommon.TRUE, 'N',CMP_CODE);
        	LocalAdLookUpValue adLookUpValueAccountCategory109 = adLookUpValueHome.create("OneHundredandNine","OneHundredandNine",null, EJBCommon.TRUE, 'N',CMP_CODE);
        	LocalAdLookUpValue adLookUpValueAccountCategory110 = adLookUpValueHome.create("OneHundredandTen","OneHundredandTen",null, EJBCommon.TRUE, 'N',CMP_CODE);
        	LocalAdLookUpValue adLookUpValueAccountCategory111 = adLookUpValueHome.create("OneHundredandEleven","OneHundredandEleven",null, EJBCommon.TRUE, 'N',CMP_CODE);
        	LocalAdLookUpValue adLookUpValueAccountCategory112 = adLookUpValueHome.create("OneHundredandTwelve","OneHundredandTwelve",null, EJBCommon.TRUE, 'N',CMP_CODE);
        	LocalAdLookUpValue adLookUpValueAccountCategory113 = adLookUpValueHome.create("OneHundredandThirteen","OneHundredandThirteen",null, EJBCommon.TRUE, 'N',CMP_CODE);
        	LocalAdLookUpValue adLookUpValueAccountCategory114 = adLookUpValueHome.create("OneHundredandFourteen","OneHundredandFourteen",null, EJBCommon.TRUE, 'N',CMP_CODE);
        	LocalAdLookUpValue adLookUpValueAccountCategory115 = adLookUpValueHome.create("OneHundredandFifteen","OneHundredandFifteen",null, EJBCommon.TRUE, 'N',CMP_CODE);
        	LocalAdLookUpValue adLookUpValueAccountCategory116 = adLookUpValueHome.create("OneHundredandSixteen","OneHundredandSixteen",null, EJBCommon.TRUE, 'N',CMP_CODE);
        	LocalAdLookUpValue adLookUpValueAccountCategory117 = adLookUpValueHome.create("OneHundredandSeventeen","OneHundredandSeventeen",null, EJBCommon.TRUE, 'N',CMP_CODE);
        	LocalAdLookUpValue adLookUpValueAccountCategory118 = adLookUpValueHome.create("OneHundredandEighteen","OneHundredandEighteen",null, EJBCommon.TRUE, 'N',CMP_CODE);
        	LocalAdLookUpValue adLookUpValueAccountCategory119 = adLookUpValueHome.create("OneHundredandNineteen","OneHundredandNineteen",null, EJBCommon.TRUE, 'N',CMP_CODE);
        	LocalAdLookUpValue adLookUpValueAccountCategory120 = adLookUpValueHome.create("OneHundredandTwenty","OneHundredandTwenty",null, EJBCommon.TRUE, 'N',CMP_CODE);
        	LocalAdLookUpValue adLookUpValueAccountCategory121 = adLookUpValueHome.create("OneHundredandTwentyOne","OneHundredandTwentyOne",null, EJBCommon.TRUE, 'N',CMP_CODE);
        	LocalAdLookUpValue adLookUpValueAccountCategory122 = adLookUpValueHome.create("OneHundredandTwentyTwo","OneHundredandTwentyTwo",null, EJBCommon.TRUE, 'N',CMP_CODE);
        	LocalAdLookUpValue adLookUpValueAccountCategory123 = adLookUpValueHome.create("OneHundredandTwentyThree","OneHundredandTwentyThree",null, EJBCommon.TRUE, 'N',CMP_CODE);
        	LocalAdLookUpValue adLookUpValueAccountCategory124 = adLookUpValueHome.create("OneHundredandTwentyFour","OneHundredandTwentyFour",null, EJBCommon.TRUE, 'N',CMP_CODE);
        	LocalAdLookUpValue adLookUpValueAccountCategory125 = adLookUpValueHome.create("OneHundredandTwentyFive","OneHundredandTwentyFive",null, EJBCommon.TRUE, 'N',CMP_CODE);
        	LocalAdLookUpValue adLookUpValueAccountCategory126 = adLookUpValueHome.create("OneHundredandTwentySix","OneHundredandTwentySix",null, EJBCommon.TRUE, 'N',CMP_CODE);
        	LocalAdLookUpValue adLookUpValueAccountCategory127 = adLookUpValueHome.create("OneHundredandTwentySeven","OneHundredandTwentySeven",null, EJBCommon.TRUE, 'N',CMP_CODE);
        	LocalAdLookUpValue adLookUpValueAccountCategory128 = adLookUpValueHome.create("OneHundredandTwentyEight","OneHundredandTwentyEight",null, EJBCommon.TRUE, 'N',CMP_CODE);
        	LocalAdLookUpValue adLookUpValueAccountCategory129 = adLookUpValueHome.create("OneHundredandTwentyNine","OneHundredandTwentyNine",null, EJBCommon.TRUE, 'N',CMP_CODE);
        	LocalAdLookUpValue adLookUpValueAccountCategory130 = adLookUpValueHome.create("OneHundredandThirty","OneHundredandThirty",null, EJBCommon.TRUE, 'N',CMP_CODE);
        	LocalAdLookUpValue adLookUpValueAccountCategory131 = adLookUpValueHome.create("OneHundredandThirtyOne","OneHundredandThirtyOne",null, EJBCommon.TRUE, 'N',CMP_CODE);
        	LocalAdLookUpValue adLookUpValueAccountCategory132 = adLookUpValueHome.create("OneHundredandThirtyTwo","OneHundredandThirtyTwo",null, EJBCommon.TRUE, 'N',CMP_CODE);
        	LocalAdLookUpValue adLookUpValueAccountCategory133 = adLookUpValueHome.create("OneHundredandThirtyThree","OneHundredandThirtyThree",null, EJBCommon.TRUE, 'N',CMP_CODE);
        	LocalAdLookUpValue adLookUpValueAccountCategory200 = adLookUpValueHome.create("TwoHundred","TwoHundred",null, EJBCommon.TRUE, 'N',CMP_CODE);
        	LocalAdLookUpValue adLookUpValueAccountCategory201 = adLookUpValueHome.create("TwoHundredandOne","TwoHundredandOne",null, EJBCommon.TRUE, 'N',CMP_CODE);
        	LocalAdLookUpValue adLookUpValueAccountCategory202 = adLookUpValueHome.create("TwoHundredandTwo","TwoHundredandTwo",null, EJBCommon.TRUE, 'N',CMP_CODE);
        	LocalAdLookUpValue adLookUpValueAccountCategory203 = adLookUpValueHome.create("TwoHundredandThree","TwoHundredandThree",null, EJBCommon.TRUE, 'N',CMP_CODE);
        	LocalAdLookUpValue adLookUpValueAccountCategory204 = adLookUpValueHome.create("TwoHundredandFour","TwoHundredandFour",null, EJBCommon.TRUE, 'N',CMP_CODE);
        	LocalAdLookUpValue adLookUpValueAccountCategory205 = adLookUpValueHome.create("TwoHundredandFive","TwoHundredandFive",null, EJBCommon.TRUE, 'N',CMP_CODE);
        	LocalAdLookUpValue adLookUpValueAccountCategory206 = adLookUpValueHome.create("TwoHundredandSix","TwoHundredandSix",null, EJBCommon.TRUE, 'N',CMP_CODE);
        	LocalAdLookUpValue adLookUpValueAccountCategory207 = adLookUpValueHome.create("TwoHundredandSeven","TwoHundredandSeven",null, EJBCommon.TRUE, 'N',CMP_CODE);
        	LocalAdLookUpValue adLookUpValueAccountCategory208 = adLookUpValueHome.create("TwoHundredandEight","TwoHundredandEight",null, EJBCommon.TRUE, 'N',CMP_CODE);
        	LocalAdLookUpValue adLookUpValueAccountCategory209 = adLookUpValueHome.create("TwoHundredandNine","TwoHundredandNine",null, EJBCommon.TRUE, 'N',CMP_CODE);
        	LocalAdLookUpValue adLookUpValueAccountCategory210 = adLookUpValueHome.create("TwoHundredandTen","TwoHundredandTen",null, EJBCommon.TRUE, 'N',CMP_CODE);
        	LocalAdLookUpValue adLookUpValueAccountCategory211 = adLookUpValueHome.create("TwoHundredandEleven","TwoHundredandEleven",null, EJBCommon.TRUE, 'N',CMP_CODE);
        	LocalAdLookUpValue adLookUpValueAccountCategory300 = adLookUpValueHome.create("ThreeHundred","ThreeHundred",null, EJBCommon.TRUE, 'N',CMP_CODE);
        	LocalAdLookUpValue adLookUpValueAccountCategory301 = adLookUpValueHome.create("ThreeHundredandOne","ThreeHundredandOne",null, EJBCommon.TRUE, 'N',CMP_CODE);
        	LocalAdLookUpValue adLookUpValueAccountCategory302 = adLookUpValueHome.create("ThreeHundredandTwo","ThreeHundredandTwo",null, EJBCommon.TRUE, 'N',CMP_CODE);
        	LocalAdLookUpValue adLookUpValueAccountCategory303 = adLookUpValueHome.create("ThreeHundredandThree","ThreeHundredandThree",null, EJBCommon.TRUE, 'N',CMP_CODE);
        	LocalAdLookUpValue adLookUpValueAccountCategory304 = adLookUpValueHome.create("ThreeHundredandFour","ThreeHundredandFour",null, EJBCommon.TRUE, 'N',CMP_CODE);
        	LocalAdLookUpValue adLookUpValueAccountCategory305 = adLookUpValueHome.create("ThreeHundredandFive","ThreeHundredandFive",null, EJBCommon.TRUE, 'N',CMP_CODE);
        	LocalAdLookUpValue adLookUpValueAccountCategory306 = adLookUpValueHome.create("ThreeHundredandSix","ThreeHundredandSix",null, EJBCommon.TRUE, 'N',CMP_CODE);
        	LocalAdLookUpValue adLookUpValueAccountCategory307 = adLookUpValueHome.create("ThreeHundredandSeven","ThreeHundredandSeven",null, EJBCommon.TRUE, 'N',CMP_CODE);
        	LocalAdLookUpValue adLookUpValueAccountCategory308 = adLookUpValueHome.create("ThreeHundredandEight","ThreeHundredandEight",null, EJBCommon.TRUE, 'N',CMP_CODE);
        	LocalAdLookUpValue adLookUpValueAccountCategory309 = adLookUpValueHome.create("ThreeHundredandNine","ThreeHundredandNine",null, EJBCommon.TRUE, 'N',CMP_CODE);
        	LocalAdLookUpValue adLookUpValueAccountCategory310 = adLookUpValueHome.create("ThreeHundredandTen","ThreeHundredandTen",null, EJBCommon.TRUE, 'N',CMP_CODE);
        	LocalAdLookUpValue adLookUpValueAccountCategory500 = adLookUpValueHome.create("FiveHundred","FiveHundred",null, EJBCommon.TRUE, 'N',CMP_CODE);
        	LocalAdLookUpValue adLookUpValueAccountCategory501 = adLookUpValueHome.create("FiveHundredandOne","FiveHundredandOne",null, EJBCommon.TRUE, 'N',CMP_CODE);
        	LocalAdLookUpValue adLookUpValueAccountCategory502 = adLookUpValueHome.create("FiveHundredandTwo","FiveHundredandTwo",null, EJBCommon.TRUE, 'N',CMP_CODE);
        	LocalAdLookUpValue adLookUpValueAccountCategory503 = adLookUpValueHome.create("FiveHundredandThree","FiveHundredandThree",null, EJBCommon.TRUE, 'N',CMP_CODE);
        	LocalAdLookUpValue adLookUpValueAccountCategory504 = adLookUpValueHome.create("FiveHundredandFour","FiveHundredandFour",null, EJBCommon.TRUE, 'N',CMP_CODE);
        	LocalAdLookUpValue adLookUpValueAccountCategory520 = adLookUpValueHome.create("FiveHundredandTwenty","FiveHundredandTwenty",null, EJBCommon.TRUE, 'N',CMP_CODE);
        	LocalAdLookUpValue adLookUpValueAccountCategory521 = adLookUpValueHome.create("FiveHundredandTwentyOne","FiveHundredandTwentyOne",null, EJBCommon.TRUE, 'N',CMP_CODE);
        	LocalAdLookUpValue adLookUpValueAccountCategory522 = adLookUpValueHome.create("FiveHundredandTwentyTwo","FiveHundredandTwentyTwo",null, EJBCommon.TRUE, 'N',CMP_CODE);
        	LocalAdLookUpValue adLookUpValueAccountCategory523 = adLookUpValueHome.create("FiveHundredandTwentyThree","FiveHundredandTwentyThree",null, EJBCommon.TRUE, 'N',CMP_CODE);
        	LocalAdLookUpValue adLookUpValueAccountCategory524 = adLookUpValueHome.create("FiveHundredandTwentyFour","FiveHundredandTwentyFour",null, EJBCommon.TRUE, 'N',CMP_CODE);
        	LocalAdLookUpValue adLookUpValueAccountCategory540 = adLookUpValueHome.create("FiveHundredandForty","FiveHundredandForty",null, EJBCommon.TRUE, 'N',CMP_CODE);
        	LocalAdLookUpValue adLookUpValueAccountCategory541 = adLookUpValueHome.create("FiveHundredandFortyOne","FiveHundredandFortyOne",null, EJBCommon.TRUE, 'N',CMP_CODE);
        	LocalAdLookUpValue adLookUpValueAccountCategory542 = adLookUpValueHome.create("FiveHundredandFortyTwo","FiveHundredandFortyTwo",null, EJBCommon.TRUE, 'N',CMP_CODE);
        	LocalAdLookUpValue adLookUpValueAccountCategory543 = adLookUpValueHome.create("FiveHundredandFortyThree","FiveHundredandFortyThree",null, EJBCommon.TRUE, 'N',CMP_CODE);
        	LocalAdLookUpValue adLookUpValueAccountCategory544 = adLookUpValueHome.create("FiveHundredandFortyFour","FiveHundredandFortyFour",null, EJBCommon.TRUE, 'N',CMP_CODE);
        	LocalAdLookUpValue adLookUpValueAccountCategory560 = adLookUpValueHome.create("FiveHundredandSixty","FiveHundredandSixty",null, EJBCommon.TRUE, 'N',CMP_CODE);
        	LocalAdLookUpValue adLookUpValueAccountCategory561 = adLookUpValueHome.create("FiveHundredandSixtyOne","FiveHundredandSixtyOne",null, EJBCommon.TRUE, 'N',CMP_CODE);
        	LocalAdLookUpValue adLookUpValueAccountCategory562 = adLookUpValueHome.create("FiveHundredandSixtyTwo","FiveHundredandSixtyTwo",null, EJBCommon.TRUE, 'N',CMP_CODE);
        	LocalAdLookUpValue adLookUpValueAccountCategory563 = adLookUpValueHome.create("FiveHundredandSixtyThree","FiveHundredandSixtyThree",null, EJBCommon.TRUE, 'N',CMP_CODE);
        	LocalAdLookUpValue adLookUpValueAccountCategory564 = adLookUpValueHome.create("FiveHundredandSixtyFour","FiveHundredandSixtyFour",null, EJBCommon.TRUE, 'N',CMP_CODE);


        	adLookUpAccountCategory.addAdLookUpValue(adLookUpValueAccountCategory10);
        	adLookUpAccountCategory.addAdLookUpValue(adLookUpValueAccountCategory11);
        	adLookUpAccountCategory.addAdLookUpValue(adLookUpValueAccountCategory12);
        	adLookUpAccountCategory.addAdLookUpValue(adLookUpValueAccountCategory13);
        	adLookUpAccountCategory.addAdLookUpValue(adLookUpValueAccountCategory14);
        	adLookUpAccountCategory.addAdLookUpValue(adLookUpValueAccountCategory15);
        	adLookUpAccountCategory.addAdLookUpValue(adLookUpValueAccountCategory16);
        	adLookUpAccountCategory.addAdLookUpValue(adLookUpValueAccountCategory17);
        	adLookUpAccountCategory.addAdLookUpValue(adLookUpValueAccountCategory18);
        	adLookUpAccountCategory.addAdLookUpValue(adLookUpValueAccountCategory19);
        	adLookUpAccountCategory.addAdLookUpValue(adLookUpValueAccountCategory20);
        	adLookUpAccountCategory.addAdLookUpValue(adLookUpValueAccountCategory100);
        	adLookUpAccountCategory.addAdLookUpValue(adLookUpValueAccountCategory101);
        	adLookUpAccountCategory.addAdLookUpValue(adLookUpValueAccountCategory102);
        	adLookUpAccountCategory.addAdLookUpValue(adLookUpValueAccountCategory103);
        	adLookUpAccountCategory.addAdLookUpValue(adLookUpValueAccountCategory104);
        	adLookUpAccountCategory.addAdLookUpValue(adLookUpValueAccountCategory105);
        	adLookUpAccountCategory.addAdLookUpValue(adLookUpValueAccountCategory106);
        	adLookUpAccountCategory.addAdLookUpValue(adLookUpValueAccountCategory107);
        	adLookUpAccountCategory.addAdLookUpValue(adLookUpValueAccountCategory108);
        	adLookUpAccountCategory.addAdLookUpValue(adLookUpValueAccountCategory109);
        	adLookUpAccountCategory.addAdLookUpValue(adLookUpValueAccountCategory110);
        	adLookUpAccountCategory.addAdLookUpValue(adLookUpValueAccountCategory111);
        	adLookUpAccountCategory.addAdLookUpValue(adLookUpValueAccountCategory112);
        	adLookUpAccountCategory.addAdLookUpValue(adLookUpValueAccountCategory113);
        	adLookUpAccountCategory.addAdLookUpValue(adLookUpValueAccountCategory114);
        	adLookUpAccountCategory.addAdLookUpValue(adLookUpValueAccountCategory115);
        	adLookUpAccountCategory.addAdLookUpValue(adLookUpValueAccountCategory116);
        	adLookUpAccountCategory.addAdLookUpValue(adLookUpValueAccountCategory117);
        	adLookUpAccountCategory.addAdLookUpValue(adLookUpValueAccountCategory118);
        	adLookUpAccountCategory.addAdLookUpValue(adLookUpValueAccountCategory119);
        	adLookUpAccountCategory.addAdLookUpValue(adLookUpValueAccountCategory120);
        	adLookUpAccountCategory.addAdLookUpValue(adLookUpValueAccountCategory121);
        	adLookUpAccountCategory.addAdLookUpValue(adLookUpValueAccountCategory122);
        	adLookUpAccountCategory.addAdLookUpValue(adLookUpValueAccountCategory123);
        	adLookUpAccountCategory.addAdLookUpValue(adLookUpValueAccountCategory124);
        	adLookUpAccountCategory.addAdLookUpValue(adLookUpValueAccountCategory125);
        	adLookUpAccountCategory.addAdLookUpValue(adLookUpValueAccountCategory126);
        	adLookUpAccountCategory.addAdLookUpValue(adLookUpValueAccountCategory127);
        	adLookUpAccountCategory.addAdLookUpValue(adLookUpValueAccountCategory128);
        	adLookUpAccountCategory.addAdLookUpValue(adLookUpValueAccountCategory129);
        	adLookUpAccountCategory.addAdLookUpValue(adLookUpValueAccountCategory130);
        	adLookUpAccountCategory.addAdLookUpValue(adLookUpValueAccountCategory131);
        	adLookUpAccountCategory.addAdLookUpValue(adLookUpValueAccountCategory132);
        	adLookUpAccountCategory.addAdLookUpValue(adLookUpValueAccountCategory133);
        	adLookUpAccountCategory.addAdLookUpValue(adLookUpValueAccountCategory200);
        	adLookUpAccountCategory.addAdLookUpValue(adLookUpValueAccountCategory201);
        	adLookUpAccountCategory.addAdLookUpValue(adLookUpValueAccountCategory202);
        	adLookUpAccountCategory.addAdLookUpValue(adLookUpValueAccountCategory203);
        	adLookUpAccountCategory.addAdLookUpValue(adLookUpValueAccountCategory204);
        	adLookUpAccountCategory.addAdLookUpValue(adLookUpValueAccountCategory205);
        	adLookUpAccountCategory.addAdLookUpValue(adLookUpValueAccountCategory206);
        	adLookUpAccountCategory.addAdLookUpValue(adLookUpValueAccountCategory207);
        	adLookUpAccountCategory.addAdLookUpValue(adLookUpValueAccountCategory208);
        	adLookUpAccountCategory.addAdLookUpValue(adLookUpValueAccountCategory209);
        	adLookUpAccountCategory.addAdLookUpValue(adLookUpValueAccountCategory210);
        	adLookUpAccountCategory.addAdLookUpValue(adLookUpValueAccountCategory211);
        	adLookUpAccountCategory.addAdLookUpValue(adLookUpValueAccountCategory300);
        	adLookUpAccountCategory.addAdLookUpValue(adLookUpValueAccountCategory301);
        	adLookUpAccountCategory.addAdLookUpValue(adLookUpValueAccountCategory302);
        	adLookUpAccountCategory.addAdLookUpValue(adLookUpValueAccountCategory303);
        	adLookUpAccountCategory.addAdLookUpValue(adLookUpValueAccountCategory304);
        	adLookUpAccountCategory.addAdLookUpValue(adLookUpValueAccountCategory305);
        	adLookUpAccountCategory.addAdLookUpValue(adLookUpValueAccountCategory306);
        	adLookUpAccountCategory.addAdLookUpValue(adLookUpValueAccountCategory307);
        	adLookUpAccountCategory.addAdLookUpValue(adLookUpValueAccountCategory308);
        	adLookUpAccountCategory.addAdLookUpValue(adLookUpValueAccountCategory309);
        	adLookUpAccountCategory.addAdLookUpValue(adLookUpValueAccountCategory310);
        	adLookUpAccountCategory.addAdLookUpValue(adLookUpValueAccountCategory500);
        	adLookUpAccountCategory.addAdLookUpValue(adLookUpValueAccountCategory501);
        	adLookUpAccountCategory.addAdLookUpValue(adLookUpValueAccountCategory502);
        	adLookUpAccountCategory.addAdLookUpValue(adLookUpValueAccountCategory503);
        	adLookUpAccountCategory.addAdLookUpValue(adLookUpValueAccountCategory504);
        	adLookUpAccountCategory.addAdLookUpValue(adLookUpValueAccountCategory520);
        	adLookUpAccountCategory.addAdLookUpValue(adLookUpValueAccountCategory521);
        	adLookUpAccountCategory.addAdLookUpValue(adLookUpValueAccountCategory522);
        	adLookUpAccountCategory.addAdLookUpValue(adLookUpValueAccountCategory523);
        	adLookUpAccountCategory.addAdLookUpValue(adLookUpValueAccountCategory524);
        	adLookUpAccountCategory.addAdLookUpValue(adLookUpValueAccountCategory540);
        	adLookUpAccountCategory.addAdLookUpValue(adLookUpValueAccountCategory541);
        	adLookUpAccountCategory.addAdLookUpValue(adLookUpValueAccountCategory542);
        	adLookUpAccountCategory.addAdLookUpValue(adLookUpValueAccountCategory543);
        	adLookUpAccountCategory.addAdLookUpValue(adLookUpValueAccountCategory544);
        	adLookUpAccountCategory.addAdLookUpValue(adLookUpValueAccountCategory560);
        	adLookUpAccountCategory.addAdLookUpValue(adLookUpValueAccountCategory561);
        	adLookUpAccountCategory.addAdLookUpValue(adLookUpValueAccountCategory562);
        	adLookUpAccountCategory.addAdLookUpValue(adLookUpValueAccountCategory563);
        	adLookUpAccountCategory.addAdLookUpValue(adLookUpValueAccountCategory564);


        	*/

        	// preference

        	LocalAdPreference adPreference = adPreferenceHome.create(
        			EJBCommon.TRUE,

        			(short)4, (short)4, (short)4, "VOUCHER","COLLECTION",
        			EJBCommon.TRUE, EJBCommon.TRUE, EJBCommon.TRUE, EJBCommon.TRUE, EJBCommon.TRUE, EJBCommon.TRUE, EJBCommon.TRUE, EJBCommon.TRUE, EJBCommon.TRUE,
        			"USE SL POSTING","USE SL POSTING", "USE SL POSTING", (short)4,(short)3,
        			(short)2, "USE SL POSTING", "USE POSTING", EJBCommon.FALSE, EJBCommon.FALSE,
					"PAYMENT", "COLLECTION", EJBCommon.FALSE, "CURRENT DATE",  null,
					null, null, null, EJBCommon.FALSE, EJBCommon.TRUE,
					EJBCommon.TRUE, EJBCommon.FALSE, null, EJBCommon.FALSE, null,
					EJBCommon.FALSE, EJBCommon.FALSE, EJBCommon.TRUE, glFirstCoa.getCoaCode(), "AP DISTRIBUTION RECORD", glFirstCoa.getCoaCode(),
					glFirstCoa.getCoaCode(), glFirstCoa.getCoaCode(), glFirstCoa.getCoaCode(), glFirstCoa.getCoaCode(),"NONE",
					"USD", "AR DISTRIBUTION RECORD", glFirstCoa.getCoaCode(), EJBCommon.FALSE, 0d,
					30, 30, EJBCommon.FALSE, EJBCommon.TRUE, EJBCommon.TRUE, EJBCommon.TRUE, EJBCommon.TRUE,
					EJBCommon.FALSE, EJBCommon.TRUE, EJBCommon.TRUE, EJBCommon.FALSE, EJBCommon.TRUE,
					EJBCommon.TRUE, EJBCommon.TRUE, EJBCommon.FALSE, EJBCommon.FALSE, null, null, null, null, EJBCommon.FALSE, null,null, null, null, null,
					CMP_CODE);

        	//stored proc

        	LocalAdStoredProcedure adStoredProcedure = adStoredProcedureHome.create(
                                EJBCommon.FALSE, "{ call sp_GlRepGeneralLedger(?,?,?,?,?,?,?,?) }",
        			EJBCommon.FALSE, "{ call sp_GlRepTrialBalance(?,?,?,?,?,?,?) }",
        			EJBCommon.FALSE, "{ call sp_GlRepIncomeStatement(?,?,?,?,?,?,?) }",
        			EJBCommon.FALSE, "{ call sp_GlRepBalanceSheet(?,?,?,?,?,?,?) }",
                                EJBCommon.FALSE, "{ call sp_StatementOfAccount(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?}",

        			CMP_CODE);

        	// payment term

        	LocalAdPaymentTerm adPaymentTerm = adPaymentTermHome.create("IMMEDIATE", "IMMEDIATE", 100,0, EJBCommon.TRUE, EJBCommon.FALSE, EJBCommon.FALSE, EJBCommon.FALSE, null, "DEFAULT", CMP_CODE);
        	LocalAdPaymentSchedule adPaymentSchedule = adPaymentScheduleHome.create((short)1, 100, (short)0, CMP_CODE);
        	adPaymentTerm.addAdPaymentSchedule(adPaymentSchedule);
        	adPaymentTerm = adPaymentTermHome.create("7 Days Net", "7 Days Net Term", 100,0, EJBCommon.TRUE, EJBCommon.FALSE, EJBCommon.FALSE, EJBCommon.FALSE, null, "DEFAULT", CMP_CODE);
        	adPaymentSchedule = adPaymentScheduleHome.create((short)1, 100, (short)7, CMP_CODE);
        	adPaymentTerm.addAdPaymentSchedule(adPaymentSchedule);
        	adPaymentTerm = adPaymentTermHome.create("10 Days Net", "10 Days Net Term", 100,0, EJBCommon.TRUE, EJBCommon.FALSE, EJBCommon.FALSE, EJBCommon.FALSE, null, "DEFAULT", CMP_CODE);
        	adPaymentSchedule = adPaymentScheduleHome.create((short)1, 100, (short)10, CMP_CODE);
        	adPaymentTerm.addAdPaymentSchedule(adPaymentSchedule);
        	adPaymentTerm = adPaymentTermHome.create("15 Days Net", "15 Days Net Term", 100,0, EJBCommon.TRUE, EJBCommon.FALSE, EJBCommon.FALSE, EJBCommon.FALSE, null, "DEFAULT", CMP_CODE);
        	adPaymentSchedule = adPaymentScheduleHome.create((short)1, 100, (short)15, CMP_CODE);
        	adPaymentTerm.addAdPaymentSchedule(adPaymentSchedule);
        	adPaymentTerm = adPaymentTermHome.create("30 Days Net", "30 Days Net Term", 100,0, EJBCommon.TRUE, EJBCommon.FALSE, EJBCommon.FALSE, EJBCommon.FALSE, null, "DEFAULT", CMP_CODE);
        	adPaymentSchedule = adPaymentScheduleHome.create((short)1, 100, (short)30, CMP_CODE);
        	adPaymentTerm.addAdPaymentSchedule(adPaymentSchedule);
        	adPaymentTerm = adPaymentTermHome.create("45 Days Net", "45 Days Net Term", 100,0, EJBCommon.TRUE, EJBCommon.FALSE, EJBCommon.FALSE, EJBCommon.FALSE, null, "DEFAULT", CMP_CODE);
        	adPaymentSchedule = adPaymentScheduleHome.create((short)1, 100, (short)45, CMP_CODE);
        	adPaymentTerm.addAdPaymentSchedule(adPaymentSchedule);
        	adPaymentTerm = adPaymentTermHome.create("90 Days Net", "90 Days Net Term", 100,0, EJBCommon.TRUE, EJBCommon.FALSE, EJBCommon.FALSE, EJBCommon.FALSE, null, "DEFAULT", CMP_CODE);
        	adPaymentSchedule = adPaymentScheduleHome.create((short)1, 100, (short)90, CMP_CODE);
        	adPaymentTerm.addAdPaymentSchedule(adPaymentSchedule);
        	adPaymentTerm = adPaymentTermHome.create("COD", "Cash On Delivery", 100,0, EJBCommon.TRUE, EJBCommon.FALSE, EJBCommon.FALSE, EJBCommon.FALSE, null, "DEFAULT", CMP_CODE);
        	adPaymentSchedule = adPaymentScheduleHome.create((short)1, 100, (short)0, CMP_CODE);
        	adPaymentTerm.addAdPaymentSchedule(adPaymentSchedule);
        	adPaymentTerm = adPaymentTermHome.create("COA PDC", "COD Post Dated Check,0", 100,0, EJBCommon.TRUE, EJBCommon.FALSE, EJBCommon.FALSE, EJBCommon.FALSE, null, "DEFAULT", CMP_CODE);
        	adPaymentSchedule = adPaymentScheduleHome.create((short)1, 100, (short)25, CMP_CODE);
        	adPaymentTerm.addAdPaymentSchedule(adPaymentSchedule);

        	adPaymentTerm = adPaymentTermHome.create("12 Months to Pay", "12 Months to Pay", 100,0, EJBCommon.TRUE, EJBCommon.FALSE, EJBCommon.FALSE, EJBCommon.FALSE, null, "MONTHLY", CMP_CODE);
        	adPaymentSchedule = adPaymentScheduleHome.create((short)1, 100, (short)25, CMP_CODE);
        	adPaymentTerm.addAdPaymentSchedule(adPaymentSchedule);

        	adPaymentTerm = adPaymentTermHome.create("24 Months to Pay", "24 Months to Pay", 100,0, EJBCommon.TRUE, EJBCommon.FALSE, EJBCommon.FALSE, EJBCommon.FALSE, null, "MONTHLY", CMP_CODE);
        	adPaymentSchedule = adPaymentScheduleHome.create((short)1, 100, (short)25, CMP_CODE);
        	adPaymentTerm.addAdPaymentSchedule(adPaymentSchedule);

        	adPaymentTerm = adPaymentTermHome.create("36 Months to Pay", "36 Months to Pay", 100,0, EJBCommon.TRUE, EJBCommon.FALSE, EJBCommon.FALSE, EJBCommon.FALSE, null, "MONTHLY", CMP_CODE);
        	adPaymentSchedule = adPaymentScheduleHome.create((short)1, 100, (short)25, CMP_CODE);
        	adPaymentTerm.addAdPaymentSchedule(adPaymentSchedule);

        	adPaymentTerm = adPaymentTermHome.create("48 Months to Pay", "48 Months to Pay", 100,0, EJBCommon.TRUE, EJBCommon.FALSE, EJBCommon.FALSE, EJBCommon.FALSE, null, "MONTHLY", CMP_CODE);
        	adPaymentSchedule = adPaymentScheduleHome.create((short)1, 100, (short)25, CMP_CODE);
        	adPaymentTerm.addAdPaymentSchedule(adPaymentSchedule);

        	adPaymentTerm = adPaymentTermHome.create("60 Months to Pay", "60 Months to Pay", 100,0, EJBCommon.TRUE, EJBCommon.FALSE, EJBCommon.FALSE, EJBCommon.FALSE, null, "MONTHLY", CMP_CODE);
        	adPaymentSchedule = adPaymentScheduleHome.create((short)1, 100, (short)25, CMP_CODE);
        	adPaymentTerm.addAdPaymentSchedule(adPaymentSchedule);

        	// tax code

        	LocalApTaxCode apInclusiveTaxCode = apTaxCodeHome.create("VAT INCLUSIVE", "INPUT TAX", "INCLUSIVE", 12, EJBCommon.TRUE, CMP_CODE);
        	glFirstCoa.addApTaxCode(apInclusiveTaxCode);
        	LocalApTaxCode apTaxCode = apTaxCodeHome.create("VAT EXCLUSIVE", "INPUT TAX", "EXCLUSIVE", 12, EJBCommon.TRUE, CMP_CODE);
        	glFirstCoa.addApTaxCode(apTaxCode);
        	apTaxCode = apTaxCodeHome.create("ZERO-RATED", "INPUT TAX", "ZERO-RATED", 0, EJBCommon.TRUE, CMP_CODE);
        	glFirstCoa.addApTaxCode(apTaxCode);
        	apTaxCode = apTaxCodeHome.create("EXEMPT", "INPUT TAX", "EXEMPT", 0, EJBCommon.TRUE, CMP_CODE);
        	apTaxCode = apTaxCodeHome.create("NONE", "INPUT TAX", "NONE", 0, EJBCommon.TRUE, CMP_CODE);

        	LocalApWithholdingTaxCode apNoneWithholdingTaxCode = apWithholdingTaxCodeHome.create("NONE", "NONE", 0, EJBCommon.TRUE, CMP_CODE);
        	LocalApWithholdingTaxCode apWithholdingTaxCode = apWithholdingTaxCodeHome.create("WC 010", "EWT-professional/talent fees paid to juridical persons/individuals", 10, EJBCommon.TRUE, CMP_CODE);
        	glFirstCoa.addApWithholdingTaxCode(apWithholdingTaxCode);
        	apWithholdingTaxCode = apWithholdingTaxCodeHome.create("WC 100", "EWT- rentals : real/personal properties, poles,satellites & transmission, facilities, billboard", 5, EJBCommon.TRUE, CMP_CODE);
        	glFirstCoa.addApWithholdingTaxCode(apWithholdingTaxCode);
        	apWithholdingTaxCode = apWithholdingTaxCodeHome.create("WC 110", "EWT- cinematographic film rentals", 5, EJBCommon.TRUE, CMP_CODE);
        	glFirstCoa.addApWithholdingTaxCode(apWithholdingTaxCode);
        	apWithholdingTaxCode = apWithholdingTaxCodeHome.create("WC 120", "EWT- prime contractors/sub-contractors", 2, EJBCommon.TRUE, CMP_CODE);
        	glFirstCoa.addApWithholdingTaxCode(apWithholdingTaxCode);
        	apWithholdingTaxCode = apWithholdingTaxCodeHome.create("WC 140", "EWT- gross commission or service fees of custom, insurance, stock, real estate, immigration & commercial brokers & fees of agents of professional ente", 10, EJBCommon.TRUE, CMP_CODE);
        	glFirstCoa.addApWithholdingTaxCode(apWithholdingTaxCode);
        	apWithholdingTaxCode = apWithholdingTaxCodeHome.create("WC 156", "EWT- payments made by credit card companies", 0.005, EJBCommon.TRUE, CMP_CODE);
        	glFirstCoa.addApWithholdingTaxCode(apWithholdingTaxCode);
        	apWithholdingTaxCode = apWithholdingTaxCodeHome.create("WC 157", "EWT-  payments made by government offices on their local purchase of goods & services from local/resident suppliers", 2, EJBCommon.TRUE, CMP_CODE);
        	glFirstCoa.addApWithholdingTaxCode(apWithholdingTaxCode);
        	apWithholdingTaxCode = apWithholdingTaxCodeHome.create("WC 158", "EWT-  Income   payments   made by top 10,000  private corporations to their local/resident supplier of goods", 1, EJBCommon.TRUE, CMP_CODE);
        	glFirstCoa.addApWithholdingTaxCode(apWithholdingTaxCode);
        	apWithholdingTaxCode = apWithholdingTaxCodeHome.create("WC 160", "EWT-  Income   payments   made by top 10,000  private corporations to their local/resident supplier of services", 2, EJBCommon.TRUE, CMP_CODE);
        	glFirstCoa.addApWithholdingTaxCode(apWithholdingTaxCode);
        	apWithholdingTaxCode = apWithholdingTaxCodeHome.create("WC 515", "EWT-  commission,rebates, discounts & other similar considerations paid/granted to independent & exclusive distributors, medical/technical & sales rep", 10, EJBCommon.TRUE, CMP_CODE);
        	glFirstCoa.addApWithholdingTaxCode(apWithholdingTaxCode);
        	apWithholdingTaxCode = apWithholdingTaxCodeHome.create("WC 535", "EWT - payments made by pre-need companies to funeral parlors", 1, EJBCommon.TRUE, CMP_CODE);
        	glFirstCoa.addApWithholdingTaxCode(apWithholdingTaxCode);
        	apWithholdingTaxCode = apWithholdingTaxCodeHome.create("WC 540", "EWT- Tolling fee paid to refineries", 5, EJBCommon.TRUE, CMP_CODE);
        	glFirstCoa.addApWithholdingTaxCode(apWithholdingTaxCode);
        	apWithholdingTaxCode = apWithholdingTaxCodeHome.create("WC 610", "EWT- Income payments made to suppliers of agricultural products", 1, EJBCommon.TRUE, CMP_CODE);
        	glFirstCoa.addApWithholdingTaxCode(apWithholdingTaxCode);
        	apWithholdingTaxCode = apWithholdingTaxCodeHome.create("WC 630", "EWT- Income payments on purchases of minerals, mineral products & quarry resources", 1, EJBCommon.TRUE, CMP_CODE);
        	glFirstCoa.addApWithholdingTaxCode(apWithholdingTaxCode);

        	LocalApSupplierClass apSupplierClass = apSupplierClassHome.create("Officers and Employees", "Officers and Employees", glFirstCoa.getCoaCode(), glFirstCoa.getCoaCode(), 0d, 0d, EJBCommon.TRUE, EJBCommon.FALSE, EJBCommon.FALSE, EJBCommon.FALSE, EJBCommon.FALSE, null, null, CMP_CODE);
        	apNoneWithholdingTaxCode.addApSupplierClass(apSupplierClass);
        	apTaxCode.addApSupplierClass(apSupplierClass);
        	apSupplierClass = apSupplierClassHome.create("Govt Institution", "Government Institution", glFirstCoa.getCoaCode(), glFirstCoa.getCoaCode(), 0d, 0d, EJBCommon.TRUE, EJBCommon.FALSE, EJBCommon.FALSE, EJBCommon.FALSE, EJBCommon.FALSE, null, null, CMP_CODE);
        	apNoneWithholdingTaxCode.addApSupplierClass(apSupplierClass);
        	apTaxCode.addApSupplierClass(apSupplierClass);
        	apSupplierClass = apSupplierClassHome.create("Others", "Others", glFirstCoa.getCoaCode(), glFirstCoa.getCoaCode(), 0d, 0d, EJBCommon.TRUE, EJBCommon.FALSE, EJBCommon.FALSE, EJBCommon.FALSE, EJBCommon.FALSE,null, null, CMP_CODE);
        	apNoneWithholdingTaxCode.addApSupplierClass(apSupplierClass);
        	apTaxCode.addApSupplierClass(apSupplierClass);
        	apSupplierClass = apSupplierClassHome.create("Partners", "Partners", glFirstCoa.getCoaCode(), glFirstCoa.getCoaCode(), 0d, 0d, EJBCommon.TRUE, EJBCommon.FALSE, EJBCommon.FALSE, EJBCommon.FALSE, EJBCommon.FALSE,null, null, CMP_CODE);
        	apNoneWithholdingTaxCode.addApSupplierClass(apSupplierClass);
        	apTaxCode.addApSupplierClass(apSupplierClass);
        	apSupplierClass = apSupplierClassHome.create("Trade", "Trade Vendors", glFirstCoa.getCoaCode(), glFirstCoa.getCoaCode(), 0d, 0d, EJBCommon.TRUE, EJBCommon.FALSE, EJBCommon.FALSE, EJBCommon.FALSE, EJBCommon.FALSE,null, null, CMP_CODE);
        	apNoneWithholdingTaxCode.addApSupplierClass(apSupplierClass);
        	apInclusiveTaxCode.addApSupplierClass(apSupplierClass);
        	apSupplierClass = apSupplierClassHome.create("Investors", "Investors", glFirstCoa.getCoaCode(), glFirstCoa.getCoaCode(), 0d, 0d, EJBCommon.TRUE, EJBCommon.FALSE, EJBCommon.FALSE, EJBCommon.FALSE, EJBCommon.FALSE,null, null, CMP_CODE);
        	apNoneWithholdingTaxCode.addApSupplierClass(apSupplierClass);
        	apTaxCode.addApSupplierClass(apSupplierClass);
        	apSupplierClass = apSupplierClassHome.create("Investment", "Investment", glFirstCoa.getCoaCode(), glFirstCoa.getCoaCode(), 0d, 0d, EJBCommon.TRUE, EJBCommon.FALSE, EJBCommon.FALSE, EJBCommon.FALSE, EJBCommon.FALSE,null, null, CMP_CODE);
        	apNoneWithholdingTaxCode.addApSupplierClass(apSupplierClass);
        	apTaxCode.addApSupplierClass(apSupplierClass);


        	LocalArAutoAccounting arAutoAccounting = arAutoAccountingHome.create("REVENUE", CMP_CODE);
        	for (int i=0; i<genSegmentList.size(); i++) {

        		LocalArAutoAccountingSegment arAutoAccountingSegment = arAutoAccountingSegmentHome.create((short)(i+1), "AR STANDARD MEMO LINE", CMP_CODE);
        		arAutoAccounting.addArAutoAccountingSegment(arAutoAccountingSegment);

        	}

        	Integer nullInteger = null;

        	LocalArTaxCode arInclusiveTaxCode = arTaxCodeHome.create("VAT INCLUSIVE", "OUTPUT TAX", "INCLUSIVE", nullInteger, 12, EJBCommon.TRUE, CMP_CODE);
        	glFirstCoa.addArTaxCode(arInclusiveTaxCode);
        	LocalArTaxCode arTaxCode = arTaxCodeHome.create("VAT EXCLUSIVE", "OUTPUT TAX", "EXCLUSIVE", nullInteger, 12, EJBCommon.TRUE, CMP_CODE);
        	glFirstCoa.addArTaxCode(arTaxCode);
        	arTaxCode = arTaxCodeHome.create("ZERO-RATED", "INPUT TAX", "ZERO-RATED", nullInteger, 0, EJBCommon.TRUE, CMP_CODE);
        	glFirstCoa.addArTaxCode(arTaxCode);
        	arTaxCode = arTaxCodeHome.create("EXEMPT", "OUTPUT TAX", "EXEMPT", nullInteger, 0, EJBCommon.TRUE, CMP_CODE);
        	arTaxCode = arTaxCodeHome.create("NONE", "OUTPUT TAX", "NONE", nullInteger, 0, EJBCommon.TRUE, CMP_CODE);

        	LocalArWithholdingTaxCode arNoneWithholdingTaxCode = arWithholdingTaxCodeHome.create("NONE", "NONE", 0, EJBCommon.TRUE, CMP_CODE);
        	LocalArWithholdingTaxCode arWithholdingTaxCode = arWithholdingTaxCodeHome.create("WC 010", "EWT-professional/talent fees paid to juridical persons/individuals", 10, EJBCommon.TRUE, CMP_CODE);
        	glFirstCoa.addArWithholdingTaxCode(arWithholdingTaxCode);

        	LocalArCustomerClass arCustomerClass = arCustomerClassHome.create("Trade-Cons", "Trade-Consultancy",null,null,null, 0d, 0, (short)0, (short)0, null, EJBCommon.FALSE, glFirstCoa.getCoaCode(), null, null, null, null, null, EJBCommon.TRUE, EJBCommon.TRUE, EJBCommon.TRUE, EJBCommon.TRUE,0, CMP_CODE);
        	arNoneWithholdingTaxCode.addArCustomerClass(arCustomerClass);
        	arInclusiveTaxCode.addArCustomerClass(arCustomerClass);
        	arCustomerClass = arCustomerClassHome.create("Trade-Acct", "Trade-Accounting",null,null,null, 0d, 0, (short)0, (short)0, null, EJBCommon.FALSE, glFirstCoa.getCoaCode(), null, null, null, null, null, EJBCommon.TRUE, EJBCommon.TRUE, EJBCommon.TRUE, EJBCommon.TRUE,0, CMP_CODE);
        	arNoneWithholdingTaxCode.addArCustomerClass(arCustomerClass);
        	arInclusiveTaxCode.addArCustomerClass(arCustomerClass);
        	arCustomerClass = arCustomerClassHome.create("Trade-Audit", "Trade-Audit",null,null,null, 0d, 0, (short)0, (short)0, null, EJBCommon.FALSE, glFirstCoa.getCoaCode(), null, null, null, null, null, EJBCommon.TRUE, EJBCommon.TRUE, EJBCommon.TRUE, EJBCommon.TRUE, 0,CMP_CODE);
        	arNoneWithholdingTaxCode.addArCustomerClass(arCustomerClass);
        	arInclusiveTaxCode.addArCustomerClass(arCustomerClass);
        	arCustomerClass = arCustomerClassHome.create("Non-Trade", "Non-Trade",null,null,null, 0d, 0, (short)0, (short)0, null, EJBCommon.FALSE, glFirstCoa.getCoaCode(), null, null, null, null, null, EJBCommon.TRUE, EJBCommon.TRUE, EJBCommon.TRUE, EJBCommon.TRUE, 0,CMP_CODE);
        	arNoneWithholdingTaxCode.addArCustomerClass(arCustomerClass);
        	arTaxCode.addArCustomerClass(arCustomerClass);
        	arCustomerClass = arCustomerClassHome.create("Trade-Spcl", "Trade-Special Projects",null,null,null, 0d, 0, (short)0, (short)0, null, EJBCommon.FALSE, glFirstCoa.getCoaCode(), null, null, null, null, null, EJBCommon.TRUE, EJBCommon.TRUE, EJBCommon.TRUE, EJBCommon.TRUE, 0,CMP_CODE);
        	arNoneWithholdingTaxCode.addArCustomerClass(arCustomerClass);
        	arInclusiveTaxCode.addArCustomerClass(arCustomerClass);
        	arCustomerClass = arCustomerClassHome.create("Investors", "Investors",null,null,null, 0d, 0, (short)0, (short)0, null, EJBCommon.FALSE, glFirstCoa.getCoaCode(), null, null, null, null, null, EJBCommon.TRUE, EJBCommon.TRUE, EJBCommon.TRUE, EJBCommon.TRUE, 0,CMP_CODE);
        	arNoneWithholdingTaxCode.addArCustomerClass(arCustomerClass);
        	arInclusiveTaxCode.addArCustomerClass(arCustomerClass);
        	arCustomerClass = arCustomerClassHome.create("Officers and Employees", "Officers and Employees",null,null,null, 0d, 0, (short)0, (short)0, null, EJBCommon.FALSE, glFirstCoa.getCoaCode(), null, null, null, null, null, EJBCommon.TRUE, EJBCommon.TRUE, EJBCommon.TRUE, EJBCommon.TRUE, 0,CMP_CODE);
        	arNoneWithholdingTaxCode.addArCustomerClass(arCustomerClass);
        	arInclusiveTaxCode.addArCustomerClass(arCustomerClass);





        	LocalArStandardMemoLine arStandardMemoLine = arStandardMemoLineHome.create("LINE", "Engagement Fee", "Project Engagement Fee","", 0, EJBCommon.TRUE, EJBCommon.TRUE, EJBCommon.FALSE, null, null, glFirstCoa.getCoaCode(), glFirstCoa.getCoaCode(), CMP_CODE);
        	glFirstCoa.addArStandardMemoLine(arStandardMemoLine);
        	arStandardMemoLine = arStandardMemoLineHome.create("LINE", "Consultancy", "Consultancy Fee","", 0, EJBCommon.TRUE, EJBCommon.TRUE, EJBCommon.FALSE, null, null, glFirstCoa.getCoaCode(), glFirstCoa.getCoaCode(), CMP_CODE);
        	glFirstCoa.addArStandardMemoLine(arStandardMemoLine);
        	arStandardMemoLine = arStandardMemoLineHome.create("LINE", "Miscellaneous", "Miscellaneous","", 0, EJBCommon.TRUE, EJBCommon.TRUE, EJBCommon.FALSE, null, null, glFirstCoa.getCoaCode(), glFirstCoa.getCoaCode(), CMP_CODE);
        	glFirstCoa.addArStandardMemoLine(arStandardMemoLine);
        	arStandardMemoLine = arStandardMemoLineHome.create("LINE", "Retainer - Accounting", "Retainer Fee - Accounting","", 0, EJBCommon.TRUE, EJBCommon.TRUE, EJBCommon.FALSE, null, null, glFirstCoa.getCoaCode(), glFirstCoa.getCoaCode(), CMP_CODE);
        	glFirstCoa.addArStandardMemoLine(arStandardMemoLine);
        	arStandardMemoLine = arStandardMemoLineHome.create("LINE", "Retainers - Audit", "Retainers Fee - Audit","", 0, EJBCommon.TRUE, EJBCommon.TRUE, EJBCommon.FALSE, null, null, glFirstCoa.getCoaCode(), glFirstCoa.getCoaCode(), CMP_CODE);
        	glFirstCoa.addArStandardMemoLine(arStandardMemoLine);

        	arStandardMemoLine = arStandardMemoLineHome.create("LINE", "Beginning Balance", "Beginning Balance","", 0, EJBCommon.TRUE, EJBCommon.TRUE, EJBCommon.FALSE, null, null, glFirstCoa.getCoaCode(), glFirstCoa.getCoaCode(), CMP_CODE);
        	glFirstCoa.addArStandardMemoLine(arStandardMemoLine);

        	arStandardMemoLine = arStandardMemoLineHome.create("LINE", "Loan Interest", "Loan Interest","", 0, EJBCommon.TRUE, EJBCommon.TRUE, EJBCommon.FALSE, null, null, glFirstCoa.getCoaCode(), glFirstCoa.getCoaCode(), CMP_CODE);
        	glFirstCoa.addArStandardMemoLine(arStandardMemoLine);

        	LocalGlOrganization glOrganization = glOrganizationHome.create("ACCOUNTING HEAD", "ACCOUNTING HEAD", null, CMP_CODE);

        	segmentIter = genSegmentList.iterator();

        	String accountFrom = "";
	        String accountTo = "";

        	while (segmentIter.hasNext()) {

    		    GenSegmentDetails details =
    		        (GenSegmentDetails)segmentIter.next();

    		    for (int j=0; j<details.getSgMaxSize(); j++) {

    		    	accountFrom = accountFrom + "0";
    		    	accountTo = accountTo + "Z";

    		    }

    		    if (segmentIter.hasNext()) {

    		    	accountFrom = accountFrom + FL_SGMNT_SPRTR;
    		    	accountTo = accountTo + FL_SGMNT_SPRTR;

    		    }

        	}

        	//LocalGlJournalBatch glJournalBatch = glJournalBatchHome.create("JOURNAL BATCH", "JOURNAL BATCH", "OPEN", EJBCommon.getGcCurrentDateWoTime(), USR_CODE, AD_BRANCH, CMP_CODE);
            //LocalArInvoiceBatch arInvoiceBatch = arInvoiceBatchHome.create("JOURNAL BATCH", "JOURNAL BATCH", "OPEN", "INVOICE", EJBCommon.getGcCurrentDateWoTime(), USR_CODE, AD_BRANCH, CMP_CODE);
            //arInvoiceBatch = arInvoiceBatchHome.create("JOURNAL BATCH", "CREDIT MEMO BATCH", "OPEN", "CREDIT MEMO", EJBCommon.getGcCurrentDateWoTime(), USR_CODE, AD_BRANCH, CMP_CODE);
            //LocalArReceiptBatch arReceiptBatch = null;
            //LocalApVoucherBatch apVoucherBatch = null;
            //LocalApCheckBatch apCheckBatch = null;



        	LocalGlAccountRange glAccountRange = glAccountRangeHome.create((short)1, accountFrom, accountTo, CMP_CODE);
        	glOrganization.addGlAccountRange(glAccountRange);

        	// gl responsibility

        	LocalGlResponsibility glResponsibility = glResponsibilityHome.create(adResponsibility.getRsCode(), EJBCommon.TRUE, CMP_CODE);
        	glOrganization.addGlResponsibility(glResponsibility);

        	glResponsibility = glResponsibilityHome.create(adResponsibilitySysAdmin.getRsCode(), EJBCommon.TRUE, CMP_CODE);
        	glOrganization.addGlResponsibility(glResponsibility);

        	glResponsibility = glResponsibilityHome.create(adResponsibilityAccnt.getRsCode(), EJBCommon.TRUE, CMP_CODE);
        	glOrganization.addGlResponsibility(glResponsibility);

        	glResponsibility = glResponsibilityHome.create(adResponsibilityGlClerk.getRsCode(), EJBCommon.TRUE, CMP_CODE);
        	glOrganization.addGlResponsibility(glResponsibility);

        	glResponsibility = glResponsibilityHome.create(adResponsibilityApClerk.getRsCode(), EJBCommon.TRUE, CMP_CODE);
        	glOrganization.addGlResponsibility(glResponsibility);

        	glResponsibility = glResponsibilityHome.create(adResponsibilityArClerk.getRsCode(), EJBCommon.TRUE, CMP_CODE);
        	glOrganization.addGlResponsibility(glResponsibility);

        	glResponsibility = glResponsibilityHome.create(adResponsibilityKeeper.getRsCode(), EJBCommon.TRUE, CMP_CODE);
        	glOrganization.addGlResponsibility(glResponsibility);

        	glResponsibility = glResponsibilityHome.create(adResponsibilityFinance.getRsCode(), EJBCommon.TRUE, CMP_CODE);
        	glOrganization.addGlResponsibility(glResponsibility);

        	glPeriodTypeHome.create("CORPORATE", "CORPORATE", (short)12, 'C', CMP_CODE);

        	LocalGlFunctionalCurrency glFunctionalCurrency = glFunctionalCurrencyHome.create("PHP", "PESO", "PHILIPPINES", 'P', (short)2, (short)2, 0.01, new GregorianCalendar(2000, 0, 1, 0, 0, 0).getTime(), new GregorianCalendar(2050, 0, 1, 0, 0, 0).getTime(), EJBCommon.TRUE, CMP_CODE);
        	glFunctionalCurrency.addAdCompany(adCompany);
        	glFunctionalCurrencyHome.create("USD", "DOLLARS", "UNITED STATED", '$', (short)2, (short)2, 0.01, new GregorianCalendar(2000, 0, 1, 0, 0, 0).getTime(), new GregorianCalendar(2050, 0, 1, 0, 0, 0).getTime(), EJBCommon.TRUE, CMP_CODE);
        	glFunctionalCurrencyHome.create("PGK", "KINA", "PAPUA NEW GUINEA", 'K', (short)2, (short)2, 0.01, new GregorianCalendar(2000, 0, 1, 0, 0, 0).getTime(), new GregorianCalendar(2050, 0, 1, 0, 0, 0).getTime(), EJBCommon.TRUE, CMP_CODE);
        	// journal

        	glJournalSourceHome.create("MANUAL", "MANUAL", EJBCommon.FALSE, EJBCommon.TRUE, 'F', CMP_CODE);
        	glJournalSourceHome.create("RECURRING", "RECURRING JOURNALS", EJBCommon.FALSE, EJBCommon.TRUE, 'F', CMP_CODE);
        	glJournalSourceHome.create("JOURNAL REVERSAL", "JOURNAL REVERSAL", EJBCommon.TRUE, EJBCommon.TRUE, 'F', CMP_CODE);
        	glJournalSourceHome.create("ACCOUNTS RECEIVABLES", "ACCOUNT RECEIVABLES SYSTEM", EJBCommon.FALSE, EJBCommon.FALSE, 'F', CMP_CODE);
        	glJournalSourceHome.create("ACCOUNTS PAYABLES", "ACCOUNTS PAYABLES", EJBCommon.FALSE, EJBCommon.FALSE, 'F', CMP_CODE);
        	glJournalSourceHome.create("CASH MANAGEMENT", "CASH MANAGEMENT", EJBCommon.FALSE, EJBCommon.FALSE, 'F', CMP_CODE);
        	glJournalSourceHome.create("INVENTORY", "INVENTORY", EJBCommon.FALSE, EJBCommon.FALSE, 'F', CMP_CODE);
        //	glJournalSourceHome.create("PAYROLL", "PAYROLL", EJBCommon.FALSE, EJBCommon.FALSE, 'F', CMP_CODE);
        	glJournalSourceHome.create("REVALUATION", "REVALUATION JOURNALS", EJBCommon.FALSE, EJBCommon.FALSE, 'F', CMP_CODE);

        	glJournalCategoryHome.create("GENERAL", "GENERAL", 'S', CMP_CODE);
        	glJournalCategoryHome.create("BANK CHARGES", "BANK CHARGES", 'S', CMP_CODE);
        	glJournalCategoryHome.create("CREDIT MEMOS", "CREDIT MEMOS", 'S', CMP_CODE);
        	glJournalCategoryHome.create("SALES INVOICES", "SALES INVOICES", 'S', CMP_CODE);
        	glJournalCategoryHome.create("SALES RECEIPTS", "SALES RECEIPTS", 'S', CMP_CODE);
        	glJournalCategoryHome.create("DEPOSITS", "DEPOSITS", 'S', CMP_CODE);
        	glJournalCategoryHome.create("VOUCHERS", "VOUCHERS", 'S', CMP_CODE);
        	glJournalCategoryHome.create("DEBIT MEMOS", "DEBIT MEMOS", 'S', CMP_CODE);
        	glJournalCategoryHome.create("CHECKS", "CHECKS", 'S', CMP_CODE);
        	glJournalCategoryHome.create("FINANCE CHARGES", "FINANCE CHARGES", 'S', CMP_CODE);
        	glJournalCategoryHome.create("FUND TRANSFERS", "FUND TRANSFERS", 'S', CMP_CODE);
        	glJournalCategoryHome.create("BANK ADJUSTMENTS", "BANK ADJUSTMENTS", 'S', CMP_CODE);
        	glJournalCategoryHome.create("INVENTORY ADJUSTMENTS", "INVENTORY ADJUSTMENTS", 'S', CMP_CODE);
        	glJournalCategoryHome.create("INVENTORY ASSEMBLIES", "INVENTORY ASSEMBLIES", 'S', CMP_CODE);
        	glJournalCategoryHome.create("STOCK ISSUANCES", "STOCK ISSUANCES", 'S', CMP_CODE);
        	glJournalCategoryHome.create("ASSEMBLY TRANSFERS", "ASSEMBLY TRANSFERS", 'S', CMP_CODE);
        	glJournalCategoryHome.create("RECEIVING ITEMS", "RECEIVING ITEMS", 'S', CMP_CODE);
        	glJournalCategoryHome.create("STOCK TRANSFERS", "STOCK TRANSFERS", 'S', CMP_CODE);
        	glJournalCategoryHome.create("BRANCH STOCK TRANSFERS", "BRANCH STOCK TRANSFERS", 'S', CMP_CODE);

        } catch (GlobalRecordAlreadyExistException ex) {

        	getSessionContext().setRollbackOnly();
            throw ex;

        } catch (GlobalRecordInvalidException ex) {

        	getSessionContext().setRollbackOnly();
            throw ex;

        } catch (Exception ex) {

        	ex.printStackTrace();
        	getSessionContext().setRollbackOnly();
            throw new EJBException(ex.getMessage());

        }

    }

// private method

    private void setFormFunctionResponsibility(ArrayList list, LocalAdFormFunction adFormFunction, Integer CMP_CODE) {

    	Debug.print("AdConsoleControllerBean setFormFunctionResponsibility");

    	LocalAdFormFunctionResponsibilityHome adFormFunctionResponsibilityHome = null;

    	try {

    		adFormFunctionResponsibilityHome = (LocalAdFormFunctionResponsibilityHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdFormFunctionResponsibilityHome.JNDI_NAME, LocalAdFormFunctionResponsibilityHome.class);

    	} catch (NamingException ex) {

    		throw new EJBException(ex.getMessage());

    	}

    	try{
    		System.out.println("list---"+list);
    		System.out.println("list.size()="+list.size());
        	Iterator i = list.iterator();

        	while(i.hasNext()) {

        		LocalAdFormFunctionResponsibility adFormFunctionResponsibility = adFormFunctionResponsibilityHome.create("F", CMP_CODE);

        		// form function
        		//System.out.println("1");
        		adFormFunction.addAdFormFunctionResponsibility(adFormFunctionResponsibility);

        		// responsibility

        		LocalAdResponsibility adResponsibility = (LocalAdResponsibility)i.next();
        		adResponsibility.addAdFormFunctionResponsibility(adFormFunctionResponsibility);

        	}

    	} catch (Exception ex) {

			Debug.printStackTrace(ex);
			getSessionContext().setRollbackOnly();
            throw new EJBException(ex.getMessage());

		}



    }

	private String encryptPassword(String password) {

		Debug.print("AdConsoleControllerBean encryptPassword");

		String encPassword = new String();

		try {

			String keyString = "Some things are better left unread.";
			byte key[] = keyString.getBytes("UTF8");

			// encode password to utf-8
			byte utf8[] = password.getBytes("UTF8");

			int length = key.length;
			if(utf8.length < key.length)
				length = utf8.length;

			// encrypt
			byte data[] = new byte[length];
			for(int i =0; i< length; i++)
			{
			   data[i] = (byte)(utf8[i] ^ key[i]);
			}

			// encode byte to base64 old
			BASE64Encoder encoder = new BASE64Encoder();
			encPassword = encoder.encode(data);

			// new encoder for java 8
		//	encPassword = Base64.getEncoder().encodeToString(data);

		} catch (Exception ex) {

			Debug.printStackTrace(ex);
            throw new EJBException(ex.getMessage());

		}

		return encPassword;

	}

    // SessionBean methods

    /**
     * @ejb:create-method view-type="remote"
     **/
    public void ejbCreate() throws CreateException {

       Debug.print("AdConsoleControllerBean ejbCreate");

    }
}
