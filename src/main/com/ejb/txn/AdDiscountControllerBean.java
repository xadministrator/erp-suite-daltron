
/*
 * AdDiscountControllerBean.java
 *
 * Created on June 05, 2003, 9:05 PM
 * @author  Enrico C. Yap
 */

package com.ejb.txn;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;
import javax.ejb.RemoveException;
import javax.naming.NamingException;

import com.ejb.ad.LocalAdDiscount;
import com.ejb.ad.LocalAdDiscountHome;
import com.ejb.ad.LocalAdPaymentSchedule;
import com.ejb.ad.LocalAdPaymentScheduleHome;
import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.exception.GlobalRecordAlreadyDeletedException;
import com.ejb.exception.GlobalRecordAlreadyExistException;
import com.util.AbstractSessionBean;
import com.util.AdDiscountDetails;
import com.util.AdPaymentScheduleDetails;
import com.util.Debug;
import com.util.EJBHomeFactory;

/**
 * @ejb:bean name="AdDiscountControllerEJB"
 *           display-name="Used for entering discounts"
 *           type="Stateless"
 *           view-type="remote"
 *           jndi-name="ejb/AdDiscountControllerEJB"
 *
 * @ejb:interface remote-class="com.ejb.txn.AdDiscountController"
 *                extends="javax.ejb.EJBObject"
 *
 * @ejb:home remote-class="com.ejb.txn.AdDiscountControllerHome"
 *           extends="javax.ejb.EJBHome"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="aruser"
 *                        role-link="aruserlink"
 *
 * @ejb:permission role-name="aruser"
 * 
*/

public class AdDiscountControllerBean extends AbstractSessionBean {
    
    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList getAdDscByPsCode(Integer PS_CODE, Integer AD_CMPNY) 
        throws GlobalNoRecordFoundException {
                    
        Debug.print("AdDiscountControllerBean getAdDscByPsCode");

        LocalAdDiscountHome adDiscountHome = null;

        Collection adDiscounts = null;

        LocalAdDiscount adDiscount = null;

        ArrayList list = new ArrayList();
        
        // Initialize EJB Home

        try {
            
            adDiscountHome = (LocalAdDiscountHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdDiscountHome.JNDI_NAME, LocalAdDiscountHome.class);

        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }

        try {
            
            adDiscounts = adDiscountHome.findByPsCode(PS_CODE, AD_CMPNY);
            
        } catch (FinderException ex) {
            
        } catch (Exception ex) {
            
            throw new EJBException(ex.getMessage());
        }

        if (adDiscounts.isEmpty()) {
        	
            throw new GlobalNoRecordFoundException();
        	
        }
        
        Iterator i = adDiscounts.iterator();
               
        while (i.hasNext()) {
        	
        	adDiscount = (LocalAdDiscount)i.next();
        	System.out.println(adDiscount.getDscCode());
            System.out.println("adDiscount.getDscDiscountPercent(): " + adDiscount.getDscDiscountPercent());                                    
        	AdDiscountDetails details = new AdDiscountDetails();
        		details.setDscCode(adDiscount.getDscCode());        		
                details.setDscDiscountPercent(adDiscount.getDscDiscountPercent());
                details.setDscPaidWithinDay(adDiscount.getDscPaidWithinDay());
                                                        	
        	list.add(details);
        }
        
        return list;
            
    }
    
    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public com.util.AdPaymentScheduleDetails getAdPsByPsCode(Integer PS_CODE, Integer AD_CMPNY) {
                    
        Debug.print("AdDiscountControllerBean getAdPsByPsCode");
        		    	         	   	        	    
        LocalAdPaymentScheduleHome adPaymentScheduleHome = null;
                                       
        // Initialize EJB Home
        
        try {
            
            adPaymentScheduleHome = (LocalAdPaymentScheduleHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdPaymentScheduleHome.JNDI_NAME, LocalAdPaymentScheduleHome.class);              
            
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {
        	
        	LocalAdPaymentSchedule adPaymentSchedule = adPaymentScheduleHome.findByPrimaryKey(PS_CODE);
   
        	AdPaymentScheduleDetails details = new AdPaymentScheduleDetails();
        		details.setPsCode(adPaymentSchedule.getPsCode());        		
                details.setPsLineNumber(adPaymentSchedule.getPsLineNumber());
                details.setPsRelativeAmount(adPaymentSchedule.getPsRelativeAmount());
                details.setPsDueDay(adPaymentSchedule.getPsDueDay());
                        
        	return details;
        	       	
        } catch (Exception ex) {
        	
        	throw new EJBException(ex.getMessage());
        	
        }
        
	}      
	
    /**
     * @ejb:interface-method view-type="remote"
     **/    
    public void addAdDscEntry(com.util.AdDiscountDetails details, Integer PS_CODE, Integer AD_CMPNY) 
        throws GlobalRecordAlreadyExistException {
               
                    
        Debug.print("AdDiscountControllerBean addAdDscEntry");
        
        LocalAdDiscountHome adDiscountHome = null;
        LocalAdPaymentScheduleHome adPaymentScheduleHome = null;
               
        LocalAdPaymentSchedule adPaymentSchedule = null;
        LocalAdDiscount adDiscount = null;
        
        Collection adDiscounts = null;
                
        ArrayList list = new ArrayList();
        
        // Initialize EJB Home
        
        try {
            
            adPaymentScheduleHome = (LocalAdPaymentScheduleHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdPaymentScheduleHome.JNDI_NAME, LocalAdPaymentScheduleHome.class);
            adDiscountHome = (LocalAdDiscountHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdDiscountHome.JNDI_NAME, LocalAdDiscountHome.class);                
                                                      
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
                                   
        try {

            adDiscount = adDiscountHome.findByPsCodeAndDscPaidWithinDate(PS_CODE, details.getDscPaidWithinDay(), AD_CMPNY);
        
        	throw new GlobalRecordAlreadyExistException();
        	
        } catch (GlobalRecordAlreadyExistException ex) {
        	
        	throw ex;
        	
        } catch (FinderException ex) {
        	 	
        } catch (Exception ex) {
            
           throw new EJBException(ex.getMessage());
            
        }                	
                        	                	        
        try {
        	
        	// create new discount
        	
        	adDiscount = adDiscountHome.create(details.getDscDiscountPercent(), 
        	        details.getDscPaidWithinDay(), AD_CMPNY); 
        	        
            adPaymentSchedule = adPaymentScheduleHome.findByPrimaryKey(PS_CODE);        	        
            adPaymentSchedule.addAdDiscount(adDiscount);              	        
        	        
        } catch (Exception ex) {
        	
        	getSessionContext().setRollbackOnly();
        	
            throw new EJBException(ex.getMessage());
        	
        }        
                                    	                                        
    }	
    
    /**
     * @ejb:interface-method view-type="remote"
     **/    
    public void updateAdDscEntry(com.util.AdDiscountDetails details, Integer PS_CODE, Integer AD_CMPNY) 
        throws GlobalRecordAlreadyExistException {
               
                    
        Debug.print("AdDiscountControllerBean updateAdDscEntry");
        
        LocalAdDiscountHome adDiscountHome = null;
        LocalAdPaymentScheduleHome adPaymentScheduleHome = null;
               
        LocalAdPaymentSchedule adPaymentSchedule = null;
        LocalAdDiscount adDiscount = null;
        
        Collection adDiscounts = null;
                
        ArrayList list = new ArrayList();
        
        // Initialize EJB Home
        
        try {
            
            adPaymentScheduleHome = (LocalAdPaymentScheduleHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdPaymentScheduleHome.JNDI_NAME, LocalAdPaymentScheduleHome.class);
            adDiscountHome = (LocalAdDiscountHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdDiscountHome.JNDI_NAME, LocalAdDiscountHome.class);                
                                                      
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }                                   

        try { 
            
            LocalAdDiscount arExistingDiscount = adDiscountHome.findByPsCodeAndDscPaidWithinDate(PS_CODE, details.getDscPaidWithinDay(), AD_CMPNY);
            
            if (!arExistingDiscount.getDscCode().equals(details.getDscCode())) {
            
                 throw new GlobalRecordAlreadyExistException();
                 
            }
        	
        } catch (GlobalRecordAlreadyExistException ex) {
        	
        	throw ex;
        	
        } catch (FinderException ex) {
        	 	
        } catch (Exception ex) {
            
            throw new EJBException(ex.getMessage());
            
        }   

        try {
        	
        	// find and update payment schedule
        	
        	adDiscount = adDiscountHome.findByPrimaryKey(details.getDscCode()); 

		    adDiscount.setDscDiscountPercent(details.getDscDiscountPercent());
            adDiscount.setDscPaidWithinDay(details.getDscPaidWithinDay());

            adPaymentSchedule = adPaymentScheduleHome.findByPrimaryKey(PS_CODE);        	        
            adPaymentSchedule.addAdDiscount(adDiscount);
            
        } catch (Exception ex) {
        	
        	getSessionContext().setRollbackOnly();
        	
            throw new EJBException(ex.getMessage());
        	
        }            	
                                                	                                        
    }	 
    
   /**
    * @ejb:interface-method view-type="remote"
    **/
    public void deleteAdDscEntry(Integer DSC_CODE, Integer AD_CMPNY) 
        throws GlobalRecordAlreadyDeletedException {               

      Debug.print("AdDiscountControllerBean deleteAdDscEntry");

      LocalAdDiscount adDiscount = null;
      LocalAdDiscountHome adDiscountHome = null;

      // Initialize EJB Home
        
      try {

          adDiscountHome = (LocalAdDiscountHome)EJBHomeFactory.
              lookUpLocalHome(LocalAdDiscountHome.JNDI_NAME, LocalAdDiscountHome.class);           
      
      } catch (NamingException ex) {

          throw new EJBException(ex.getMessage());

      }                
      
      try {
      	
         adDiscount = adDiscountHome.findByPrimaryKey(DSC_CODE);
         
      } catch (FinderException ex) {
      	
         throw new GlobalRecordAlreadyDeletedException();
         
      } catch (Exception ex) {
      	
         throw new EJBException(ex.getMessage()); 
         
      }
      
      try {
         	
	      adDiscount.remove();
	    
	  } catch (RemoveException ex) {
	 	
	      getSessionContext().setRollbackOnly();
	    
	      throw new EJBException(ex.getMessage());
	    
	  } catch (Exception ex) {
	 	
	      getSessionContext().setRollbackOnly();
	    
	      throw new EJBException(ex.getMessage());
	    
	  }	      
      
   }         
    
    // SessionBean methods

    /**
     * @ejb:create-method view-type="remote"
     **/
    public void ejbCreate() throws CreateException {

       Debug.print("AdDiscountControllerBean ejbCreate");
      
    }
}
