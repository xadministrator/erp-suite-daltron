
/*
 * AdDocumentCategoryControllerBean.java
 *
 * Created on May 26, 2003, 8:49 AM
 * @author  Enrico C. Yap
 */

package com.ejb.txn;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;
import javax.ejb.RemoveException;
import javax.naming.NamingException;

import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.exception.GlobalRecordAlreadyAssignedException;
import com.ejb.exception.GlobalRecordAlreadyDeletedException;
import com.ejb.exception.GlobalRecordAlreadyExistException;
import com.ejb.gl.LocalAdApplication;
import com.ejb.gl.LocalAdApplicationHome;
import com.ejb.gl.LocalAdDocumentCategory;
import com.ejb.gl.LocalAdDocumentCategoryHome;
import com.util.AbstractSessionBean;
import com.util.AdModDocumentCategoryDetails;
import com.util.Debug;
import com.util.EJBHomeFactory;

/**
 * @ejb:bean name="AdDocumentCategoryControllerEJB"
 *           display-name="Used for entering document categories"
 *           type="Stateless"
 *           view-type="remote"
 *           jndi-name="ejb/AdDocumentCategoryControllerEJB"
 *
 * @ejb:interface remote-class="com.ejb.txn.AdDocumentCategoryController"
 *                extends="javax.ejb.EJBObject"
 *
 * @ejb:home remote-class="com.ejb.txn.AdDocumentCategoryControllerHome"
 *           extends="javax.ejb.EJBHome"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="gluser"
 *                        role-link="gluserlink"
 *
 * @ejb:permission role-name="gluser"
 * 
*/

public class AdDocumentCategoryControllerBean extends AbstractSessionBean {
    
    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
	public ArrayList getAdAppAll(Integer AD_CMPNY)
    	throws GlobalNoRecordFoundException {
                    
        Debug.print("AdDocumentCategoryControllerBean getAdAppAll");
        
        LocalAdApplicationHome adApplicationHome = null;
        
        Collection adApplications = null;
        
        LocalAdApplication adApplication = null;
        
        ArrayList list = new ArrayList();
        
        // Initialize EJB Home
        
        try {
            
            adApplicationHome = (LocalAdApplicationHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdApplicationHome.JNDI_NAME, LocalAdApplicationHome.class);
            
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {
            
            adApplications = adApplicationHome.findAppAll(AD_CMPNY);
            
        } catch (FinderException ex) {
            
        } catch (Exception ex) {
            
            throw new EJBException(ex.getMessage());
        }
        
        if (adApplications.isEmpty()) {
        	
        	throw new GlobalNoRecordFoundException();
        	
        }
        
        Iterator i = adApplications.iterator();
               
        while (i.hasNext()) {
        	
        	adApplication = (LocalAdApplication)i.next();
        	
        	list.add(adApplication.getAppName());
        	
        }
        
        return list;
            
    }
    
    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
	public ArrayList getAdDcAll(Integer AD_CMPNY) 
        throws GlobalNoRecordFoundException {
                    
        Debug.print("AdDocumentCategoryControllerBean getAdDcAll");

        LocalAdDocumentCategoryHome adDocumentCategoryHome = null;

        LocalAdDocumentCategory adDocumentCategory = null;

        Collection adDocumentCategories = null;

        ArrayList list = new ArrayList();
        
        // Initialize EJB Home

        try {
            
            adDocumentCategoryHome = (LocalAdDocumentCategoryHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdDocumentCategoryHome.JNDI_NAME, LocalAdDocumentCategoryHome.class);
                          
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }

        try {
            
            adDocumentCategories = adDocumentCategoryHome.findDcAll(AD_CMPNY);
            
        } catch (FinderException ex) {
            
        } catch (Exception ex) {
            
            throw new EJBException(ex.getMessage());
        }

        if (adDocumentCategories.isEmpty()) {
        	
            throw new GlobalNoRecordFoundException();
        	
        }
        
        Iterator i = adDocumentCategories.iterator();
               
        while (i.hasNext()) {
        	
        	adDocumentCategory = (LocalAdDocumentCategory)i.next();
                                                                        
        	AdModDocumentCategoryDetails mdetails = new AdModDocumentCategoryDetails(
        		adDocumentCategory.getDcCode(), adDocumentCategory.getDcName(), 
        		adDocumentCategory.getDcDescription(), adDocumentCategory.getAdApplication().getAppName());
                                                        	
        	list.add(mdetails);
        }
        
        return list;
            
    }
    
    /**
     * @ejb:interface-method view-type="remote"
     **/    
	public void addAdDcEntry(com.util.AdDocumentCategoryDetails details, String DC_APP_NM, Integer AD_CMPNY) 
        throws GlobalRecordAlreadyExistException {
                    
        Debug.print("AdDocumentCategoryControllerBean addAdDcEntry");
        
        LocalAdDocumentCategoryHome adDocumentCategoryHome = null;
        LocalAdApplicationHome adApplicationHome = null;
               
        LocalAdDocumentCategory adDocumentCategory = null;
        LocalAdApplication adApplication = null;
                
        ArrayList list = new ArrayList();
        
        // Initialize EJB Home
        
        try {
            
            adDocumentCategoryHome = (LocalAdDocumentCategoryHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdDocumentCategoryHome.JNDI_NAME, LocalAdDocumentCategoryHome.class);
            adApplicationHome = (LocalAdApplicationHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdApplicationHome.JNDI_NAME, LocalAdApplicationHome.class);                
                                                      
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try { 
            
            adDocumentCategory = adDocumentCategoryHome.findByDcName(details.getDcName(), AD_CMPNY);
            
            throw new GlobalRecordAlreadyExistException();
                                             
         } catch (GlobalRecordAlreadyExistException ex) {
        	
        	throw ex;
        	
         } catch (FinderException ex) {
        	 	
         } catch (Exception ex) {
            
            throw new EJBException(ex.getMessage());
            
         }
                            
        try {
        	
        	// create new document category
        	
        	adDocumentCategory = adDocumentCategoryHome.create(details.getDcCode(),
        	   details.getDcName(), details.getDcDescription(), AD_CMPNY); 
        	        
        	   adApplication = adApplicationHome.findByAppName(DC_APP_NM, AD_CMPNY);
        	   adApplication.addAdDocumentCategory(adDocumentCategory);        	          	        
                
	       
        } catch (Exception ex) {
        	
            throw new EJBException(ex.getMessage());
        	
        }    
        
    }    
        
    /**
     * @ejb:interface-method view-type="remote"
     **/
	public void updateAdDcEntry(com.util.AdDocumentCategoryDetails details, String DC_APP_NM, Integer AD_CMPNY) 
        throws GlobalRecordAlreadyExistException {
                    
        Debug.print("AdDocumentCategoryControllerBean updateAdDcEntry");
        
        LocalAdDocumentCategoryHome adDocumentCategoryHome = null;
        LocalAdApplicationHome adApplicationHome = null;
               
        LocalAdDocumentCategory adDocumentCategory = null;
        LocalAdApplication adApplication = null;
                
        ArrayList list = new ArrayList();
        
        // Initialize EJB Home
        
        try {
            
            adDocumentCategoryHome = (LocalAdDocumentCategoryHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdDocumentCategoryHome.JNDI_NAME, LocalAdDocumentCategoryHome.class);
            adApplicationHome = (LocalAdApplicationHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdApplicationHome.JNDI_NAME, LocalAdApplicationHome.class);                
                         
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try { 
            
            LocalAdDocumentCategory adExistingDocumentCategory = adDocumentCategoryHome.findByDcName(details.getDcName(), AD_CMPNY);
            
            if (!adExistingDocumentCategory.getDcCode().equals(details.getDcCode())) {
            
                 throw new GlobalRecordAlreadyExistException();
                 
            }
                                             
        } catch (GlobalRecordAlreadyExistException ex) {
        	
            throw ex;
        	
        } catch (FinderException ex) {
        	 	
        } catch (Exception ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
                        
        try {
        	        	
        	// find and update document category
        	
        	adDocumentCategory = adDocumentCategoryHome.findByPrimaryKey(details.getDcCode());
        	    	
                adDocumentCategory.setDcName(details.getDcName());
                adDocumentCategory.setDcDescription(details.getDcDescription());
                
        	    adApplication = adApplicationHome.findByAppName(DC_APP_NM, AD_CMPNY);
        	    adApplication.addAdDocumentCategory(adDocumentCategory);        	                          
        	        	                            	
        } catch (Exception ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
    }
    
   /**
    * @ejb:interface-method view-type="remote"
    **/
	public void deleteAdDcEntry(Integer DC_CODE, Integer AD_CMPNY) 
        throws GlobalRecordAlreadyDeletedException,
               GlobalRecordAlreadyAssignedException {

        Debug.print("AdDocumentCategoryControllerBean deleteAdDcEntry");

        LocalAdDocumentCategory adDocumentCategory = null;
        LocalAdDocumentCategoryHome adDocumentCategoryHome = null;

        // Initialize EJB Home
        
        try {

            adDocumentCategoryHome = (LocalAdDocumentCategoryHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdDocumentCategoryHome.JNDI_NAME, LocalAdDocumentCategoryHome.class);           
      
        } catch (NamingException ex) {

            throw new EJBException(ex.getMessage());

        }                
      
        try {
      	
           adDocumentCategory = adDocumentCategoryHome.findByPrimaryKey(DC_CODE);
         
        } catch (FinderException ex) {
      	
           throw new GlobalRecordAlreadyDeletedException();
         
        } catch (Exception ex) {
      	
           throw new EJBException(ex.getMessage()); 
         
        }
      
        try {

            if (!adDocumentCategory.getAdDocumentSequenceAssignments().isEmpty()) {
      
               throw new GlobalRecordAlreadyAssignedException();
             
            }
          
        } catch (GlobalRecordAlreadyAssignedException ex) {                        
         
            throw ex;
          
        } catch (Exception ex) {
       
            throw new EJBException(ex.getMessage());
           
        }                         
         
        try {
         	
	        adDocumentCategory.remove();
	    
	    } catch (RemoveException ex) {
	 	
	        getSessionContext().setRollbackOnly();
	    
	        throw new EJBException(ex.getMessage());
	    
	    } catch (Exception ex) {
	 	
	        getSessionContext().setRollbackOnly();
	    
	        throw new EJBException(ex.getMessage());
	    
	    }	      
      
    }    
    
    // SessionBean methods

    /**
     * @ejb:create-method view-type="remote"
     **/
     public void ejbCreate() throws CreateException {

       Debug.print("AdDocumentCategoryControllerBean ejbCreate");
      
    }
}
