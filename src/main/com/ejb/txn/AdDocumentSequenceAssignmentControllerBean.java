package com.ejb.txn;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;
import javax.ejb.RemoveException;
import javax.naming.NamingException;

import com.ejb.ad.LocalAdBranch;
import com.ejb.ad.LocalAdBranchDocumentSequenceAssignment;
import com.ejb.ad.LocalAdBranchDocumentSequenceAssignmentHome;
import com.ejb.ad.LocalAdBranchHome;
import com.ejb.ad.LocalAdBranchResponsibility;
import com.ejb.ad.LocalAdBranchResponsibilityHome;
import com.ejb.ad.LocalAdResponsibility;
import com.ejb.ad.LocalAdResponsibilityHome;
import com.ejb.exception.AdDCDocumentCategoryAlreadyExistException;
import com.ejb.exception.AdDCNoDocumentCategoryFoundException;
import com.ejb.exception.AdDSADocumentSequenceAssignmentAlreadyAssignedException;
import com.ejb.exception.AdDSADocumentSequenceAssignmentAlreadyDeletedException;
import com.ejb.exception.AdDSANoDocumentSequenceAssignmentFoundException;
import com.ejb.exception.AdDSNoDocumentSequenceFoundException;
import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.gl.LocalAdDocumentCategory;
import com.ejb.gl.LocalAdDocumentCategoryHome;
import com.ejb.gl.LocalAdDocumentSequence;
import com.ejb.gl.LocalAdDocumentSequenceAssignment;
import com.ejb.gl.LocalAdDocumentSequenceAssignmentHome;
import com.ejb.gl.LocalAdDocumentSequenceHome;
import com.util.AbstractSessionBean;
import com.util.AdBranchDetails;
import com.util.AdDocumentCategoryDetails;
import com.util.AdDocumentSequenceAssignmentDetails;
import com.util.AdDocumentSequenceDetails;
import com.util.AdModBranchDocumentSequenceAssignmentDetails;
import com.util.AdModDocumentSequenceAssignmentDetails;
import com.util.AdResponsibilityDetails;
import com.util.Debug;
import com.util.EJBCommon;
import com.util.EJBHomeFactory;

/**
 * @ejb:bean name="AdDocumentSequenceAssignmentControllerEJB"
 *           display-name="Used for document sequence assignment"
 *           type="Stateless"
 *           view-type="remote"
 *           jndi-name="ejb/AdDocumentSequenceAssignmentControllerEJB"
 *
 * @ejb:interface remote-class="com.ejb.txn.AdDocumentSequenceAssignmentController"
 *                extends="javax.ejb.EJBObject"
 *
 * @ejb:home remote-class="com.ejb.txn.AdDocumentSequenceAssignmentControllerHome"
 *           extends="javax.ejb.EJBHome"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="gluser"
 *                        role-link="gluserlink"
 *
 * @ejb:permission role-name="gluser"
 *
*/

public class AdDocumentSequenceAssignmentControllerBean extends AbstractSessionBean {

   /*******************************************************************
      Business methods:

      (1) getAdDcAll - returns an ArrayList of all DC

      (2) getAdDsAll - returns an ArrayList of all DS

      (3) getAdDsaAll - return an ArrayList of all DSA

      (3) addAdDsaEntry - add a DSA into the DB

      (4) updateAdDsaEntry - updates a DSA into the DB

      (5) deleteAdDsaEntry - deletes a DSA into the DB only if its not
      			    yet assigned

   *********************************************************************/

   /**
   * @ejb:interface-method view-type="remote"
   * @jboss:method-attributes read-only="true"
   **/
   public ArrayList getAdDcAll(Integer AD_CMPNY)
      throws AdDCNoDocumentCategoryFoundException {

      Debug.print("AdDocumentSequenceAssignmentControllerBean getAdDcAll");

      /****************************************************
         Gets all Document Categories
       ****************************************************/

      ArrayList dcAllList = new ArrayList();
      Collection adDocumentCategories = null;
      
      LocalAdDocumentCategoryHome adDocumentCategoryHome = null;
      
      // Initialize EJB Home
        
      try {
            
          adDocumentCategoryHome = (LocalAdDocumentCategoryHome)EJBHomeFactory.
              lookUpLocalHome(LocalAdDocumentCategoryHome.JNDI_NAME, LocalAdDocumentCategoryHome.class);
            
      } catch (NamingException ex) {
            
          throw new EJBException(ex.getMessage());
            
      }

      try {
         adDocumentCategories = adDocumentCategoryHome.findDcAll(AD_CMPNY);
      } catch (FinderException ex) {
         throw new EJBException(ex.getMessage());
      } catch (Exception ex) {
         throw new EJBException(ex.getMessage());
      }

      if (adDocumentCategories.size() == 0)
         throw new AdDCNoDocumentCategoryFoundException();
         
      Iterator i = adDocumentCategories.iterator();
      while (i.hasNext()) {
         LocalAdDocumentCategory adDocumentCategory = (LocalAdDocumentCategory) i.next();
	 AdDocumentCategoryDetails details = new AdDocumentCategoryDetails(
	    adDocumentCategory.getDcCode(), adDocumentCategory.getDcName(),
	    adDocumentCategory.getDcDescription());
         dcAllList.add(details);
      }

      return dcAllList;
   }

   /**
   * @ejb:interface-method view-type="remote"
   * @jboss:method-attributes read-only="true"
   **/
   public ArrayList getAdDsAll(Integer AD_CMPNY)
      throws AdDSNoDocumentSequenceFoundException {
      
      Debug.print("AdDocumentSequenceAssignmentControllerBean getAdDsAll");

      /****************************************************
         Gets all Document Sequences
      ****************************************************/

      ArrayList dsAllList = new ArrayList();
      Collection adDocumentSequences = null;
      GregorianCalendar gcCurrDate = new GregorianCalendar();
      gcCurrDate.set(gcCurrDate.get(Calendar.YEAR), gcCurrDate.get(Calendar.MONTH), gcCurrDate.get(Calendar.DATE), 0, 0, 0);
      gcCurrDate.set(Calendar.MILLISECOND, 0);
      
      LocalAdDocumentSequenceHome adDocumentSequenceHome = null;
      
      // Initialize EJB Home
        
      try {
            
          adDocumentSequenceHome = (LocalAdDocumentSequenceHome)EJBHomeFactory.
              lookUpLocalHome(LocalAdDocumentSequenceHome.JNDI_NAME, LocalAdDocumentSequenceHome.class);
            
      } catch (NamingException ex) {
            
          throw new EJBException(ex.getMessage());
            
      }
      
      try {
      	 adDocumentSequences = adDocumentSequenceHome.findDsEnabled(new Date(gcCurrDate.getTime().getTime()), AD_CMPNY);
      } catch (FinderException ex) {
         throw new EJBException(ex.getMessage());
      } catch (Exception ex) {
         throw new EJBException(ex.getMessage());
      }

      if (adDocumentSequences.size() == 0)
         throw new AdDSNoDocumentSequenceFoundException();
	
      Iterator i = adDocumentSequences.iterator();
      dsAllList.clear();
      while (i.hasNext()) {
      	LocalAdDocumentSequence adDocumentSequence = (LocalAdDocumentSequence) i.next();
      	AdDocumentSequenceDetails details = new AdDocumentSequenceDetails(
      			adDocumentSequence.getDsCode(), adDocumentSequence.getDsSequenceName(),
				adDocumentSequence.getDsDateFrom(), adDocumentSequence.getDsDateTo(), 
				adDocumentSequence.getDsNumberingType(), adDocumentSequence.getDsInitialValue());
      	dsAllList.add(details);
      }
      
      return dsAllList;
   }

   /**
   * @ejb:interface-method view-type="remote"
   * @jboss:method-attributes read-only="true"
   **/
   public ArrayList getAdDsaAll(Integer AD_CMPNY)
      throws AdDSANoDocumentSequenceAssignmentFoundException {

      Debug.print("AdDocumentSequenceAssignmentControllerBean getAdDsaAll");

      /****************************************************
         Gets all Document Sequence Assignments
	 including the document category name and
	 document sequence name of the DS
      ****************************************************/

      ArrayList dsaAllList = new ArrayList();
      Collection adDocumentSequenceAssignments = null;
      
      LocalAdDocumentSequenceAssignmentHome adDocumentSequenceAssignmentHome = null;
      
      // Initialize EJB Home
        
      try {
            
          adDocumentSequenceAssignmentHome = (LocalAdDocumentSequenceAssignmentHome)EJBHomeFactory.
              lookUpLocalHome(LocalAdDocumentSequenceAssignmentHome.JNDI_NAME, LocalAdDocumentSequenceAssignmentHome.class);
            
      } catch (NamingException ex) {
            
          throw new EJBException(ex.getMessage());
            
      }

      try {
         adDocumentSequenceAssignments = adDocumentSequenceAssignmentHome.findDsaAll(AD_CMPNY);
      } catch (FinderException ex) {
         throw new EJBException(ex.getMessage());
      } catch (Exception ex) {
         throw new EJBException(ex.getMessage());
      }

      if (adDocumentSequenceAssignments.size() == 0)
         throw new AdDSANoDocumentSequenceAssignmentFoundException();

      Iterator i = adDocumentSequenceAssignments.iterator();
      while (i.hasNext()) {
         LocalAdDocumentSequenceAssignment adDocumentSequenceAssignment = (LocalAdDocumentSequenceAssignment) i.next();
            AdModDocumentSequenceAssignmentDetails details = new AdModDocumentSequenceAssignmentDetails(
	    adDocumentSequenceAssignment.getDsaCode(), adDocumentSequenceAssignment.getDsaSobCode(),
	    adDocumentSequenceAssignment.getDsaNextSequence(), 
	    adDocumentSequenceAssignment.getAdDocumentCategory().getDcName(),
	    adDocumentSequenceAssignment.getAdDocumentSequence().getDsSequenceName());
            dsaAllList.add(details);
      }

      return dsaAllList;
   }
   

   /**
   * @ejb:interface-method view-type="remote"
   **/
   public void updateAdDSASequenceByCode(Integer DSA_CODE, String DSA_NXT_SQNC,Integer AD_CMPNY)
		   throws AdDCNoDocumentCategoryFoundException,
		   AdDSNoDocumentSequenceFoundException,
		   AdDCDocumentCategoryAlreadyExistException {
      
	   LocalAdDocumentSequenceAssignment adDocumentSequenceAssignment = null;
	   LocalAdDocumentSequenceAssignmentHome adDocumentSequenceAssignmentHome = null;
	   
	   	try {
           
	   		System.out.println("finding");
	          adDocumentSequenceAssignmentHome = (LocalAdDocumentSequenceAssignmentHome)EJBHomeFactory.
	              lookUpLocalHome(LocalAdDocumentSequenceAssignmentHome.JNDI_NAME, LocalAdDocumentSequenceAssignmentHome.class);
	        
	                
	      } catch (NamingException ex) {
	            
	          throw new EJBException(ex.getMessage());
	            
	      }
	   	
	   	
	    try {
	    //	adDocumentSequenceAssignment = adDocumentSequenceAssignmentHome.findDsaByCode(DSA_CODE,AD_CMPNY);
	    	adDocumentSequenceAssignment = adDocumentSequenceAssignmentHome.findByPrimaryKey(DSA_CODE);
		      
	    	
	    	
	    	
	    	System.out.println(adDocumentSequenceAssignment.getDsaNextSequence() + " the seq");
	    	System.out.println("to change" +  DSA_NXT_SQNC);
	    	
	    	
	    	
	    	
	    	
	    	
	    } catch (FinderException ex) {
	         throw new EJBException(ex.getMessage());
	      
	    } catch (Exception ex) {
	         throw new EJBException(ex.getMessage());
	      
	    }
	    
	    
	    try {
	         adDocumentSequenceAssignment.setDsaNextSequence(DSA_NXT_SQNC);
	    } catch (Exception ex) {
	         getSessionContext().setRollbackOnly();
	         throw new EJBException(ex.getMessage());
	    }
	   
   }
   
   
   
   

   /**
   * @ejb:interface-method view-type="remote"
   **/
   public void addAdDsaEntry(AdDocumentSequenceAssignmentDetails details, String DC_NM, String DS_NM, ArrayList branchList, Integer AD_CMPNY)
      throws AdDCNoDocumentCategoryFoundException,
      AdDSNoDocumentSequenceFoundException,
      AdDCDocumentCategoryAlreadyExistException {

      Debug.print("AdDocumentSequenceAssignmentControllerBean addAdDsaEntry");

      /*******************************************************
         Adds a DSA
      *******************************************************/

      LocalAdDocumentCategory adDocumentCategory = null;
      LocalAdDocumentSequence adDocumentSequence = null;
      LocalAdDocumentSequenceAssignment adDocumentSequenceAssignment = null;
      LocalAdBranchDocumentSequenceAssignment adBranchDsa = null;
      LocalAdBranch adBranch = null;
      
      LocalAdDocumentSequenceAssignmentHome adDocumentSequenceAssignmentHome = null;
      LocalAdDocumentSequenceHome adDocumentSequenceHome = null;
      LocalAdDocumentCategoryHome adDocumentCategoryHome = null;
      LocalAdBranchDocumentSequenceAssignmentHome adBranchDsaHome = null;
      LocalAdBranchHome adBranchHome = null;
      
      // Initialize EJB Home
        
      try {
            
          adDocumentSequenceAssignmentHome = (LocalAdDocumentSequenceAssignmentHome)EJBHomeFactory.
              lookUpLocalHome(LocalAdDocumentSequenceAssignmentHome.JNDI_NAME, LocalAdDocumentSequenceAssignmentHome.class);
          adDocumentSequenceHome = (LocalAdDocumentSequenceHome)EJBHomeFactory.
              lookUpLocalHome(LocalAdDocumentSequenceHome.JNDI_NAME, LocalAdDocumentSequenceHome.class);
          adDocumentCategoryHome = (LocalAdDocumentCategoryHome)EJBHomeFactory.
              lookUpLocalHome(LocalAdDocumentCategoryHome.JNDI_NAME, LocalAdDocumentCategoryHome.class);
          adBranchDsaHome = (LocalAdBranchDocumentSequenceAssignmentHome)EJBHomeFactory.
          	lookUpLocalHome(LocalAdBranchDocumentSequenceAssignmentHome.JNDI_NAME, LocalAdBranchDocumentSequenceAssignmentHome.class);
          adBranchHome = (LocalAdBranchHome)EJBHomeFactory.
        	lookUpLocalHome(LocalAdBranchHome.JNDI_NAME, LocalAdBranchHome.class);
                
      } catch (NamingException ex) {
            
          throw new EJBException(ex.getMessage());
            
      }

      try {
         adDocumentCategory = adDocumentCategoryHome.findByDcName(DC_NM, AD_CMPNY);
      } catch (FinderException ex) {
         throw new AdDCNoDocumentCategoryFoundException();
      } catch (Exception ex) {
         throw new EJBException(ex.getMessage());
      }
      
      try {
         adDocumentSequence = adDocumentSequenceHome.findByDsName(DS_NM, AD_CMPNY);
      } catch (FinderException ex) {
         throw new AdDSNoDocumentSequenceFoundException();
      } catch (Exception ex) {
         throw new EJBException(ex.getMessage());
      }

      try {
         adDocumentSequenceAssignment = adDocumentSequenceAssignmentHome.create(
	    new Integer(1), adDocumentSequence.getDsInitialValue(), AD_CMPNY);
      } catch (Exception ex) {
         getSessionContext().setRollbackOnly();
	 throw new EJBException(ex.getMessage());
      }

      if(adDocumentCategory.getAdDocumentSequenceAssignments().size() == 0) { 
         try{
            adDocumentCategory.addAdDocumentSequenceAssignment(adDocumentSequenceAssignment);
	 } catch (Exception e) {
	    getSessionContext().setRollbackOnly();
	    throw new EJBException(e.getMessage());
         }
      } else {
         getSessionContext().setRollbackOnly();
	 throw new AdDCDocumentCategoryAlreadyExistException();
      }

      try {
         adDocumentSequence.addAdDocumentSequenceAssignment(adDocumentSequenceAssignment);
      } catch (Exception ex) {
         getSessionContext().setRollbackOnly();
         throw new EJBException(ex.getMessage());
      } 
      
      // add branch dsa
      
      try {
      
	      Iterator i = branchList.iterator();
	      
	      while(i.hasNext()) {	          	          
	          
	          AdModBranchDocumentSequenceAssignmentDetails brDsaDetails = (AdModBranchDocumentSequenceAssignmentDetails)i.next();
	          adBranchDsa = adBranchDsaHome.create(brDsaDetails.getBdsNextSequence(), AD_CMPNY);	          	                   
	          
	          adDocumentSequenceAssignment.addAdBranchDocumentSequenceAssignments(adBranchDsa);
	          
	          adBranch = adBranchHome.findByPrimaryKey(brDsaDetails.getBrCode());
	          adBranch.addAdBranchDocumentSequenceAssignments(adBranchDsa);
	          
	      }
      
      } catch (Exception ex) {
          
      	  getSessionContext().setRollbackOnly();
          throw new EJBException(ex.getMessage());
      	
      }    
      

   }

   /**
   * @ejb:interface-method view-type="remote"
   **/
   public void updateAdDsaEntry(AdDocumentSequenceAssignmentDetails details, String DC_NM, String DS_NM, 
      String RS_NM, ArrayList branchList, Integer AD_CMPNY)
      throws AdDCNoDocumentCategoryFoundException,
      AdDSNoDocumentSequenceFoundException,
      AdDCDocumentCategoryAlreadyExistException,
      AdDSADocumentSequenceAssignmentAlreadyDeletedException {

      Debug.print("AdDocumentSequenceAssignmentControllerBean updateAdDsaEntry");

      /********************************************************
         Updates a particular document sequence
      ********************************************************/

      LocalAdDocumentCategory adDocumentCategory = null;
      LocalAdDocumentSequence adDocumentSequence = null;
      LocalAdBranchDocumentSequenceAssignment adBranchDsa = null;
      LocalAdBranch adBranch = null;
      
      LocalAdDocumentSequenceAssignmentHome adDocumentSequenceAssignmentHome = null;
      LocalAdDocumentSequenceHome adDocumentSequenceHome = null;
      LocalAdDocumentCategoryHome adDocumentCategoryHome = null;
      LocalAdBranchDocumentSequenceAssignmentHome adBranchDsaHome = null;
      LocalAdBranchHome adBranchHome = null;
      
      // Initialize EJB Home
        
      try {
            
          adDocumentSequenceAssignmentHome = (LocalAdDocumentSequenceAssignmentHome)EJBHomeFactory.
              lookUpLocalHome(LocalAdDocumentSequenceAssignmentHome.JNDI_NAME, LocalAdDocumentSequenceAssignmentHome.class);
          adDocumentSequenceHome = (LocalAdDocumentSequenceHome)EJBHomeFactory.
              lookUpLocalHome(LocalAdDocumentSequenceHome.JNDI_NAME, LocalAdDocumentSequenceHome.class);
          adDocumentCategoryHome = (LocalAdDocumentCategoryHome)EJBHomeFactory.
              lookUpLocalHome(LocalAdDocumentCategoryHome.JNDI_NAME, LocalAdDocumentCategoryHome.class);
          adBranchDsaHome = (LocalAdBranchDocumentSequenceAssignmentHome)EJBHomeFactory.
          	lookUpLocalHome(LocalAdBranchDocumentSequenceAssignmentHome.JNDI_NAME, LocalAdBranchDocumentSequenceAssignmentHome.class);
          adBranchHome = (LocalAdBranchHome)EJBHomeFactory.
        	lookUpLocalHome(LocalAdBranchHome.JNDI_NAME, LocalAdBranchHome.class);
                
      } catch (NamingException ex) {
            
          throw new EJBException(ex.getMessage());
            
      }

      try {
         adDocumentCategory = adDocumentCategoryHome.findByDcName(DC_NM, AD_CMPNY);
      } catch (FinderException ex) {
         throw new AdDCNoDocumentCategoryFoundException();
      } catch (Exception ex) {
         throw new EJBException(ex.getMessage());
      }

      try {
         adDocumentSequence = adDocumentSequenceHome.findByDsName(DS_NM, AD_CMPNY);
      } catch (FinderException ex) {
         throw new AdDSNoDocumentSequenceFoundException();
      } catch (Exception ex){
         throw new EJBException(ex.getMessage());
      }

      LocalAdDocumentSequenceAssignment adDocumentSequenceAssignment = null;

      try {
         adDocumentSequenceAssignment = adDocumentSequenceAssignmentHome.findByPrimaryKey(details.getDsaCode());
      } catch (FinderException ex) {
         throw new AdDSADocumentSequenceAssignmentAlreadyDeletedException();
      } catch (Exception ex) {
         throw new EJBException(ex.getMessage());
      }


      try {
         adDocumentSequenceAssignment.setDsaNextSequence(details.getDsaNextSequence());
      } catch (Exception ex) {
         getSessionContext().setRollbackOnly();
         throw new EJBException(ex.getMessage());
      }

      Collection adDocumentSequenceAssignments = adDocumentCategory.getAdDocumentSequenceAssignments();
      
      if(adDocumentSequenceAssignments.size() == 0) {
         try {
            adDocumentCategory.addAdDocumentSequenceAssignment(adDocumentSequenceAssignment);
         } catch (Exception ex) {
            getSessionContext().setRollbackOnly();
            throw new EJBException(ex.getMessage());
         }
      } else if(adDocumentCategory.getAdDocumentSequenceAssignments().size() > 0) {
         Iterator i = adDocumentSequenceAssignments.iterator();
	 boolean dsaFound = false;
	 while (i.hasNext()) {
	    LocalAdDocumentSequenceAssignment adDocumentSequenceAssignment2 = (LocalAdDocumentSequenceAssignment) i.next();
	    if (adDocumentSequenceAssignment2.getAdDocumentCategory().getDcCode().equals(
	       adDocumentSequenceAssignment.getAdDocumentCategory().getDcCode())) {
	          try {
		     adDocumentCategory.addAdDocumentSequenceAssignment(adDocumentSequenceAssignment);
		     dsaFound = true;
		  } catch (Exception ex) {
		     getSessionContext().setRollbackOnly();
		     throw new EJBException(ex.getMessage());
		  }
	       }
	 }
	 if (!dsaFound) {
	    getSessionContext().setRollbackOnly();
	    throw new AdDCDocumentCategoryAlreadyExistException();
	 }
      }

      try {
         adDocumentSequence.addAdDocumentSequenceAssignment(adDocumentSequenceAssignment);
      } catch (Exception ex) {
         getSessionContext().setRollbackOnly();
         throw new EJBException(ex.getMessage());
      }
      
      // update branch dsa
      
      try {
      
	      Collection adBranchDSAs = adBranchDsaHome.findBdsByDsaCodeAndRsName(adDocumentSequenceAssignment.getDsaCode(), RS_NM, AD_CMPNY);
	      
	      Iterator i = adBranchDSAs.iterator();
	      
	      //remove all adBranchDSA lines
	      while(i.hasNext()) {
	      	
	      	adBranchDsa = (LocalAdBranchDocumentSequenceAssignment) i.next();
	      	
	      	adDocumentSequenceAssignment.dropAdBranchDocumentSequenceAssignments(adBranchDsa);
	      	
	      	adBranch = adBranchHome.findByPrimaryKey(adBranchDsa.getAdBranch().getBrCode());
	      	adBranch.dropAdBranchDocumentSequenceAssignments(adBranchDsa);
	      	adBranchDsa.remove();
	      }
	      
	      //add adBranchDSA lines
	      
	      Iterator brListIter = branchList.iterator();
	      
	      while(brListIter.hasNext()) {
	          
	          AdModBranchDocumentSequenceAssignmentDetails brDsaDetails = (AdModBranchDocumentSequenceAssignmentDetails)brListIter.next();
	          adBranchDsa = adBranchDsaHome.create(brDsaDetails.getBdsNextSequence(), AD_CMPNY);
	          
	          adDocumentSequenceAssignment.addAdBranchDocumentSequenceAssignments(adBranchDsa);
	          
	          adBranch = adBranchHome.findByPrimaryKey(brDsaDetails.getBrCode());
	          adBranch.addAdBranchDocumentSequenceAssignments(adBranchDsa);
	                	
	      }
	 
      } catch (Exception ex) {
          
      	  getSessionContext().setRollbackOnly();
          throw new EJBException(ex.getMessage());
      	
      }    
   }

   /**
   * @ejb:interface-method view-type="remote"
   **/
   public void deleteAdDsaEntry(Integer DSA_CODE, Integer AD_CMPNY) 
      throws AdDSADocumentSequenceAssignmentAlreadyAssignedException,
      AdDSADocumentSequenceAssignmentAlreadyDeletedException {

      Debug.print("AdDocumentSequenceAssignmentControllerBean deleteAdDsaEntry");

      /********************************************************
         Deletes a DSA but only if is still not assigned
       *******************************************************/

      LocalAdDocumentSequenceAssignment adDocumentSequenceAssignment = null;
      
      LocalAdDocumentSequenceAssignmentHome adDocumentSequenceAssignmentHome = null;
            
      // Initialize EJB Home
        
      try {
            
          adDocumentSequenceAssignmentHome = (LocalAdDocumentSequenceAssignmentHome)EJBHomeFactory.
              lookUpLocalHome(LocalAdDocumentSequenceAssignmentHome.JNDI_NAME, LocalAdDocumentSequenceAssignmentHome.class);
                      
      } catch (NamingException ex) {
            
          throw new EJBException(ex.getMessage());
            
      }

      try {
         adDocumentSequenceAssignment = adDocumentSequenceAssignmentHome.findByPrimaryKey(DSA_CODE);
      } catch (FinderException ex) {
         throw new AdDSADocumentSequenceAssignmentAlreadyDeletedException();
      } catch (Exception ex) {
         throw new EJBException(ex.getMessage()); 
      }

      if (hasRelation(adDocumentSequenceAssignment, AD_CMPNY))
         throw new AdDSADocumentSequenceAssignmentAlreadyAssignedException();
      else {
         try {
	    adDocumentSequenceAssignment.remove();
	 } catch (RemoveException ex) {
	    getSessionContext().setRollbackOnly();
	    throw new EJBException(ex.getMessage());
	 } catch (Exception ex) {
	    getSessionContext().setRollbackOnly();
	    throw new EJBException(ex.getMessage());
	 }
      }
   }
   
   /**
    * @ejb:interface-method view-type="remote"
    * @jboss:method-attributes read-only="true"
    **/   
   public ArrayList getAdBrResAll(Integer RS_CODE, Integer AD_CMPNY) 
   	   throws GlobalNoRecordFoundException{
       
       Debug.print("AdDocumentSequenceAssignmentControllerBean getAdBrResAll");
       
       LocalAdBranchResponsibilityHome adBranchResponsibilityHome = null;
       LocalAdBranchHome adBranchHome = null;
       
       LocalAdBranchResponsibility adBranchResponsibility = null;
       LocalAdBranch adBranch = null;
       
       Collection adBranchResponsibilities = null;
       
       ArrayList list = new ArrayList();
       
       // Initialize EJB Home
       
       try {
           
           adBranchResponsibilityHome = (LocalAdBranchResponsibilityHome)EJBHomeFactory.
           lookUpLocalHome(LocalAdBranchResponsibilityHome.JNDI_NAME, LocalAdBranchResponsibilityHome.class);
           adBranchHome = (LocalAdBranchHome)EJBHomeFactory.
           lookUpLocalHome(LocalAdBranchHome.JNDI_NAME, LocalAdBranchHome.class);
           
       } catch (NamingException ex) {
           
           throw new EJBException(ex.getMessage());
           
       }
       
       try {
           
           adBranchResponsibilities = adBranchResponsibilityHome.findByAdResponsibility(RS_CODE, AD_CMPNY);
           
       } catch (FinderException ex) {
           
       } catch (Exception ex) {
           
           throw new EJBException(ex.getMessage());
       }
       
       if (adBranchResponsibilities.isEmpty()) {
           
           throw new GlobalNoRecordFoundException();
           
       }
       
       try {
           
           Iterator i = adBranchResponsibilities.iterator();
           
           while(i.hasNext()) {
               
               adBranchResponsibility = (LocalAdBranchResponsibility)i.next();
               
               adBranch = adBranchResponsibility.getAdBranch();
               
               AdBranchDetails details = new AdBranchDetails();
               
               details.setBrCode(adBranch.getBrCode());
               details.setBrName(adBranch.getBrName());
               
               list.add(details);
               
           }	               
           
       } catch (Exception ex) {
           
           throw new EJBException(ex.getMessage());
       }
       
       return list;
       
   }
   
   /**
    * @ejb:interface-method view-type="remote"
    * @jboss:method-attributes read-only="true"
    **/   
   public ArrayList getAdBrDsaAll(Integer DSA_CODE, Integer AD_CMPNY) 
   	   throws GlobalNoRecordFoundException{
       
       Debug.print("AdDocumentSequenceAssignmentControllerBean getAdBrDsaAll");
       
       LocalAdBranchDocumentSequenceAssignmentHome adBranchDsaHome  = null;
       LocalAdBranchHome adBranchHome = null;
       
       LocalAdBranchDocumentSequenceAssignment adBranchDsa = null;
       LocalAdBranch adBranch = null;
       
       Collection adBranchDsas = null;
       
       ArrayList list = new ArrayList();
       
       // Initialize EJB Home
       
       try {
           
           adBranchDsaHome = (LocalAdBranchDocumentSequenceAssignmentHome)EJBHomeFactory.
           		lookUpLocalHome(LocalAdBranchDocumentSequenceAssignmentHome.JNDI_NAME, LocalAdBranchDocumentSequenceAssignmentHome.class);
           adBranchHome = (LocalAdBranchHome)EJBHomeFactory.
           		lookUpLocalHome(LocalAdBranchHome.JNDI_NAME, LocalAdBranchHome.class);
           
       } catch (NamingException ex) {
           
           throw new EJBException(ex.getMessage());
           
       }
       
       try {
           
           adBranchDsas = adBranchDsaHome.findBdsByDsaCode(DSA_CODE, AD_CMPNY);
           
       } catch (FinderException ex) {
           
       } catch (Exception ex) {
           
           throw new EJBException(ex.getMessage());
       }
       
       if (adBranchDsas.isEmpty()) {
           
           throw new GlobalNoRecordFoundException();
           
       }
       
       try {
           
           Iterator i = adBranchDsas.iterator();
           
           while(i.hasNext()) {
               
               adBranchDsa = (LocalAdBranchDocumentSequenceAssignment)i.next();
               
               adBranch = adBranchHome.findByPrimaryKey(adBranchDsa.getAdBranch().getBrCode());
               
               AdModBranchDocumentSequenceAssignmentDetails details = new AdModBranchDocumentSequenceAssignmentDetails();
               
               details.setBrCode(adBranch.getBrCode());
               System.out.println("adBranchDsa.getBdsNextSequence()="+adBranchDsa.getBdsNextSequence());
               details.setBdsNextSequence(adBranchDsa.getBdsNextSequence());               
               
               list.add(details);
               
           }
           
       } catch (FinderException ex) {
           
       } catch (Exception ex) {
           
           throw new EJBException(ex.getMessage());
       }
       
       return list;
       
   }
   
   /**
    * @ejb:interface-method view-type="remote"
    * @jboss:method-attributes read-only="true"
    **/
   public AdResponsibilityDetails getAdRsByRsCode(Integer RS_CODE) 
   	   throws GlobalNoRecordFoundException {
       
       Debug.print("AdDocumentSequenceAssignmentControllerBean getAdRsByRsCode");
       
       LocalAdResponsibilityHome adResHome = null;
       LocalAdResponsibility adRes = null;
       
       try {
           adResHome = (LocalAdResponsibilityHome)EJBHomeFactory.
           lookUpLocalHome(LocalAdResponsibilityHome.JNDI_NAME, LocalAdResponsibilityHome.class);
       } catch (NamingException ex) {
              throw new EJBException(ex.getMessage());
       }
       
       try {
           adRes = adResHome.findByPrimaryKey(RS_CODE);    		
       } catch (FinderException ex) {
           
       }
       
       AdResponsibilityDetails details = new AdResponsibilityDetails();
       details.setRsName(adRes.getRsName());
       
       return details;
   }
   
     /**
    * @ejb:interface-method view-type="remote"
    * @jboss:method-attributes read-only="true"
    **/
   public String getDocumentNumberSequence(String DOCUMENT_NAME, Integer BR_CODE, Integer AD_CMPNY){
        Debug.print("AdDocumentSequenceAssignmentControllerBean getDocumentNumberSequence");

       LocalAdBranchDocumentSequenceAssignmentHome adBranchDocumentSequenceAssignmentHome = null;
       LocalAdDocumentSequenceAssignmentHome adDocumentSequenceAssignmentHome = null;
       
       String documentNextSequence = "";
       
       
       try {
           adBranchDocumentSequenceAssignmentHome = (LocalAdBranchDocumentSequenceAssignmentHome)EJBHomeFactory.
           lookUpLocalHome(LocalAdBranchDocumentSequenceAssignmentHome.JNDI_NAME, LocalAdBranchDocumentSequenceAssignmentHome.class);
           
           adDocumentSequenceAssignmentHome = (LocalAdDocumentSequenceAssignmentHome)EJBHomeFactory.
           lookUpLocalHome(LocalAdDocumentSequenceAssignmentHome.JNDI_NAME, LocalAdDocumentSequenceAssignmentHome.class);
           
       } catch (NamingException ex) {
             throw new EJBException(ex.getMessage());
       }
       
       try{
       
           LocalAdDocumentSequenceAssignment adDocumentSequenceAssignment =adDocumentSequenceAssignmentHome.findByDcName(DOCUMENT_NAME, AD_CMPNY);
       
           documentNextSequence = adDocumentSequenceAssignment.getDsaNextSequence();
           try{
           
                LocalAdBranchDocumentSequenceAssignment adBranchDocumentSequenceAssignment = adBranchDocumentSequenceAssignmentHome.findBdsByDsaCodeAndBrCode(adDocumentSequenceAssignment.getDsaCode(), BR_CODE, AD_CMPNY);
  
                documentNextSequence = adBranchDocumentSequenceAssignment.getBdsNextSequence();
                
                adBranchDocumentSequenceAssignment.setBdsNextSequence(EJBCommon.incrementStringNumber(documentNextSequence));
                
           }catch(FinderException ex){
                
                adDocumentSequenceAssignment.setDsaNextSequence(EJBCommon.incrementStringNumber(documentNextSequence));
            
           }
        

       } catch (Exception ex) {
           
           throw new EJBException(ex.getMessage());
       }
       
       
       return documentNextSequence;
       
       
   
   }
   
   // SessionBean methods
   
   /**
    * @ejb:create-method view-type="remote"
    **/
   public void ejbCreate() throws CreateException {
       
       Debug.print("AdDocumentSequenceAssignmentControllerBean ejbCreate");
       
   }
   
   //  methods
   
   boolean hasRelation(LocalAdDocumentSequenceAssignment adDocumentSequenceAssignment, Integer AD_CMPNY) {
       
       Debug.print("AdDocumentSequenceAssignmentControllerBean hasRelation");
       
       Collection glJournals = null;
       
       /**********************************************************
        Check if has relation with GlJournal
        ***********************************************************/
       
       try {
           glJournals =
               adDocumentSequenceAssignment.getGlJournals();
       } catch (Exception ex) {
       }
       
       if (glJournals.size() != 0) return true;
       else return false;
       
   }
   
   
   
}
