package com.ejb.txn;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;
import javax.ejb.RemoveException;
import javax.naming.NamingException;

import com.ejb.exception.AdAPPNoApplicationFoundException;
import com.ejb.exception.AdDSDocumentSequenceAlreadyAssignedException;
import com.ejb.exception.AdDSDocumentSequenceAlreadyDeletedException;
import com.ejb.exception.AdDSDocumentSequenceAlreadyExistException;
import com.ejb.exception.AdDSNoDocumentSequenceFoundException;
import com.ejb.gl.LocalAdApplication;
import com.ejb.gl.LocalAdApplicationHome;
import com.ejb.gl.LocalAdDocumentSequence;
import com.ejb.gl.LocalAdDocumentSequenceHome;
import com.util.AbstractSessionBean;
import com.util.AdApplicationDetails;
import com.util.AdDocumentSequenceDetails;
import com.util.AdModDocumentSequenceDetails;
import com.util.Debug;
import com.util.EJBHomeFactory;

/**
 * @ejb:bean name="AdDocumentSequenceControllerEJB"
 *           display-name="Used for document sequence"
 *           type="Stateless"
 *           view-type="remote"
 *           jndi-name="ejb/AdDocumentSequenceControllerEJB"
 *
 * @ejb:interface remote-class="com.ejb.txn.AdDocumentSequenceController"
 *                extends="javax.ejb.EJBObject"
 *
 * @ejb:home remote-class="com.ejb.txn.AdDocumentSequenceControllerHome"
 *           extends="javax.ejb.EJBHome"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="gluser"
 *                        role-link="gluserlink"
 *
 * @ejb:permission role-name="gluser"
 * 
*/

public class AdDocumentSequenceControllerBean extends AbstractSessionBean {


   /*******************************************************************
      Business methods:

      (1) getAdAppAll - returns an ArrayList of all APP

      (2) getAdDsAll - returns an ArrayList of all DS

      (3) addAdDsEntry - add a DS into the DB

      (4) updateAdDsEntry - updates a DS into the DB only if its not
      			    yet assigned

      (5) deleteAdDsEntry - deletes a DS into the DB only if its not
      			    yet assigned

   *********************************************************************/

   /**
   * @ejb:interface-method view-type="remote"
   * @jboss:method-attributes read-only="true"
   **/
   public ArrayList getAdAppAll(Integer AD_CMPNY)
      throws AdAPPNoApplicationFoundException {

      Debug.print("AdDocumentSequenceControllerBean getAdAppAll");

      /****************************************************
         Gets all Applications
       ****************************************************/
       
      LocalAdApplicationHome adApplicationHome = null;

      ArrayList appAllList = new ArrayList();
      Collection adApplications = null;
      
      // Initialize EJB Home
        
      try {
            
          adApplicationHome = (LocalAdApplicationHome)EJBHomeFactory.
              lookUpLocalHome(LocalAdApplicationHome.JNDI_NAME, LocalAdApplicationHome.class);
            
      } catch (NamingException ex) {
            
          throw new EJBException(ex.getMessage());
            
      }

      try {
         adApplications = adApplicationHome.findAppAll(AD_CMPNY);
      } catch (FinderException ex) {
         throw new EJBException(ex.getMessage());
      } catch (Exception ex) {
         throw new EJBException(ex.getMessage());
      }

      if (adApplications.size() == 0)
         throw new AdAPPNoApplicationFoundException();
         
      Iterator i = adApplications.iterator();
      while (i.hasNext()) {
         LocalAdApplication adApplication = (LocalAdApplication) i.next();
	 AdApplicationDetails details = new AdApplicationDetails(
	    adApplication.getAppCode(), adApplication.getAppName(),
	    adApplication.getAppDescription());
         appAllList.add(details);
      }

      return appAllList;
   }

   /**
   * @ejb:interface-method view-type="remote"
   * @jboss:method-attributes read-only="true"
   **/
   public ArrayList getAdDsAll(Integer AD_CMPNY)
      throws AdDSNoDocumentSequenceFoundException {

      Debug.print("AdDocumentSequenceControllerBean getAdDsAll");

      /****************************************************
         Gets all Documents Sequences including the
	 application name of the DS
      ****************************************************/

      ArrayList dsAllList = new ArrayList();
      Collection adDocumentSequences = null;
      
      LocalAdDocumentSequenceHome adDocumentSequenceHome = null;

           
      // Initialize EJB Home
        
      try {
            
          adDocumentSequenceHome = (LocalAdDocumentSequenceHome)EJBHomeFactory.
              lookUpLocalHome(LocalAdDocumentSequenceHome.JNDI_NAME, LocalAdDocumentSequenceHome.class);
            
      } catch (NamingException ex) {
            
          throw new EJBException(ex.getMessage());
            
      }

      try {
         adDocumentSequences = adDocumentSequenceHome.findDsAll(AD_CMPNY);
      } catch (FinderException ex) {
         throw new EJBException(ex.getMessage());
      } catch (Exception ex) {
         throw new EJBException(ex.getMessage());
      }

      if (adDocumentSequences.size() == 0)
         throw new AdDSNoDocumentSequenceFoundException();

      Iterator i = adDocumentSequences.iterator();
      while (i.hasNext()) {
         LocalAdDocumentSequence adDocumentSequence = (LocalAdDocumentSequence) i.next();
            AdModDocumentSequenceDetails details = new AdModDocumentSequenceDetails(
	    adDocumentSequence.getDsCode(), adDocumentSequence.getDsSequenceName(),
	    adDocumentSequence.getDsDateFrom(), adDocumentSequence.getDsDateTo(),
	    adDocumentSequence.getDsNumberingType(), adDocumentSequence.getDsInitialValue(),
	    adDocumentSequence.getAdApplication().getAppName());
            dsAllList.add(details);
      }

      return dsAllList;
   }

   /**
   * @ejb:interface-method view-type="remote"
   **/
   public void addAdDsEntry(AdDocumentSequenceDetails details, String APP_NM, Integer AD_CMPNY)
      throws AdAPPNoApplicationFoundException,
      AdDSDocumentSequenceAlreadyExistException {

      Debug.print("AdDocumentSequenceControllerBean addAdDsEntry");

      /*******************************************************
         Adds a DS
      *******************************************************/
      
      LocalAdApplication adApplication = null;
      LocalAdDocumentSequence adDocumentSequence = null;
      
      LocalAdDocumentSequenceHome adDocumentSequenceHome = null;
      LocalAdApplicationHome adApplicationHome = null;

           
      // Initialize EJB Home
        
      try {
            
          adDocumentSequenceHome = (LocalAdDocumentSequenceHome)EJBHomeFactory.
              lookUpLocalHome(LocalAdDocumentSequenceHome.JNDI_NAME, LocalAdDocumentSequenceHome.class);
          adApplicationHome = (LocalAdApplicationHome)EJBHomeFactory.
              lookUpLocalHome(LocalAdApplicationHome.JNDI_NAME, LocalAdApplicationHome.class);    
                          
      } catch (NamingException ex) {
            
          throw new EJBException(ex.getMessage());
            
      }

      try {
         adApplication = adApplicationHome.findByAppName(APP_NM, AD_CMPNY);
      } catch (FinderException ex) {
         throw new AdAPPNoApplicationFoundException();
      } catch (Exception ex) {
         throw new EJBException(ex.getMessage());
      }
      
      try {
      	
         adDocumentSequenceHome.findByDsName(details.getDsSequenceName(), AD_CMPNY);
         
         getSessionContext().setRollbackOnly();
         throw new AdDSDocumentSequenceAlreadyExistException();
      	
      } catch (FinderException ex) {
      	
      	      	
      }
         

      try {
         adDocumentSequence = adDocumentSequenceHome.create(
	    details.getDsSequenceName(), details.getDsDateFrom(),
	    details.getDsDateTo(), details.getDsNumberingType(),
	    details.getDsInitialValue(), AD_CMPNY);
      } catch (Exception ex) {
         getSessionContext().setRollbackOnly();
	 throw new EJBException(ex.getMessage());
      }

      try {
         adApplication.addAdDocumentSequence(adDocumentSequence);
      } catch (Exception ex) {
         getSessionContext().setRollbackOnly();
         throw new EJBException(ex.getMessage());
      }

   }

   /**
   * @ejb:interface-method view-type="remote"
   **/
   public void updateAdDsEntry(AdDocumentSequenceDetails details, String APP_NM, Integer AD_CMPNY)
      throws AdAPPNoApplicationFoundException,
      AdDSDocumentSequenceAlreadyExistException,
      AdDSDocumentSequenceAlreadyAssignedException,
      AdDSDocumentSequenceAlreadyDeletedException {

      Debug.print("AdDocumentSequenceControllerBean updateAdDsEntry");

      /********************************************************
         Updates a particular document sequence only if it is
	 not assigned yet
      ********************************************************/

      LocalAdApplication adApplication = null;
      
      LocalAdDocumentSequenceHome adDocumentSequenceHome = null;
      LocalAdApplicationHome adApplicationHome = null;

           
      // Initialize EJB Home
        
      try {
            
          adDocumentSequenceHome = (LocalAdDocumentSequenceHome)EJBHomeFactory.
              lookUpLocalHome(LocalAdDocumentSequenceHome.JNDI_NAME, LocalAdDocumentSequenceHome.class);
          adApplicationHome = (LocalAdApplicationHome)EJBHomeFactory.
              lookUpLocalHome(LocalAdApplicationHome.JNDI_NAME, LocalAdApplicationHome.class);    
                          
      } catch (NamingException ex) {
            
          throw new EJBException(ex.getMessage());
            
      }

      try {
         adApplication = adApplicationHome.findByAppName(APP_NM, AD_CMPNY);
      } catch (FinderException ex) {
         throw new AdAPPNoApplicationFoundException();
      } catch (Exception ex) {
         throw new EJBException(ex.getMessage());
      }

      LocalAdDocumentSequence adDocumentSequence = null;

      try {
         adDocumentSequence = adDocumentSequenceHome.findByPrimaryKey(
	    details.getDsCode());
      } catch (FinderException ex) {
         throw new AdDSDocumentSequenceAlreadyDeletedException();
      } catch (Exception ex) {
         throw new EJBException(ex.getMessage());
      }

      if (hasRelation(adDocumentSequence, AD_CMPNY))
         throw new AdDSDocumentSequenceAlreadyAssignedException();
      else {
       
	 LocalAdDocumentSequence adDocumentSequence2 = null;

         try {
	    adDocumentSequence2 = 
	       adDocumentSequenceHome.findByDsName(details.getDsSequenceName(), AD_CMPNY);
	 } catch (FinderException ex) {
	    try {
	       adDocumentSequence.setDsSequenceName(details.getDsSequenceName());
	       adDocumentSequence.setDsDateFrom(details.getDsDateFrom());
	       adDocumentSequence.setDsDateTo(details.getDsDateTo());
	       adDocumentSequence.setDsNumberingType(details.getDsNumberingType());
	       adDocumentSequence.setDsInitialValue(details.getDsInitialValue());
	    } catch (Exception e) {
	       getSessionContext().setRollbackOnly();
	       throw new EJBException(ex.getMessage());
	    }

	    try {
               adApplication.addAdDocumentSequence(adDocumentSequence);
            } catch (Exception e) {
               getSessionContext().setRollbackOnly();
               throw new EJBException(ex.getMessage());
            }
	 } catch (Exception ex) {
	    throw new EJBException(ex.getMessage());
	 }

	 if(adDocumentSequence2 != null && !adDocumentSequence.getDsCode().equals(adDocumentSequence2.getDsCode())) {
	    getSessionContext().setRollbackOnly();
	    throw new AdDSDocumentSequenceAlreadyExistException();
	 } else
	    if (adDocumentSequence2 != null && adDocumentSequence.getDsCode().equals(adDocumentSequence2.getDsCode())) {
	       try {
                  adDocumentSequence.setDsDateFrom(details.getDsDateFrom());
	          adDocumentSequence.setDsDateTo(details.getDsDateTo());
	          adDocumentSequence.setDsNumberingType(details.getDsNumberingType());
	          adDocumentSequence.setDsInitialValue(details.getDsInitialValue());
               } catch (Exception ex) {
	          getSessionContext().setRollbackOnly();
		  throw new EJBException(ex.getMessage());
	       }

	       try {
                  adApplication.addAdDocumentSequence(adDocumentSequence);
	       } catch (Exception ex) {
	          getSessionContext().setRollbackOnly();
	          throw new EJBException(ex.getMessage());
	       }
	 }
      }
   }

   /**
   * @ejb:interface-method view-type="remote"
   **/
   public void deleteAdDsEntry(Integer DS_CODE, Integer AD_CMPNY) 
      throws AdDSDocumentSequenceAlreadyAssignedException,
      AdDSDocumentSequenceAlreadyDeletedException {

      Debug.print("AdDocumentSequenceControllerBean deleteAdDEntry");

      /********************************************************
         Deletes a DS but only if is still not assigned
       *******************************************************/

      LocalAdDocumentSequence adDocumentSequence = null;
      
      LocalAdDocumentSequenceHome adDocumentSequenceHome = null;
                
      // Initialize EJB Home
        
      try {
            
          adDocumentSequenceHome = (LocalAdDocumentSequenceHome)EJBHomeFactory.
              lookUpLocalHome(LocalAdDocumentSequenceHome.JNDI_NAME, LocalAdDocumentSequenceHome.class);
                                
      } catch (NamingException ex) {
            
          throw new EJBException(ex.getMessage());
            
      }

      try {
         adDocumentSequence = adDocumentSequenceHome.findByPrimaryKey(DS_CODE);
      } catch (FinderException ex) {
         throw new AdDSDocumentSequenceAlreadyDeletedException();
      } catch (Exception ex) {
         throw new EJBException(ex.getMessage()); 
      }

      if (hasRelation(adDocumentSequence, AD_CMPNY))
         throw new AdDSDocumentSequenceAlreadyAssignedException();
      else {
         try {
	    adDocumentSequence.remove();
	 } catch (RemoveException ex) {
	    getSessionContext().setRollbackOnly();
	    throw new EJBException(ex.getMessage());
	 } catch (Exception ex) {
	    getSessionContext().setRollbackOnly();
	    throw new EJBException(ex.getMessage());
	 }
      }
   }

   // SessionBean methods

   /**
    * @ejb:create-method view-type="remote"
    **/
   public void ejbCreate() throws CreateException {

      Debug.print("AdDocumentSequenceControllerBean ejbCreate");

   }

   // private methods

   private boolean hasRelation(LocalAdDocumentSequence adDocumentSequence, Integer AD_CMPNY) {

      Debug.print("AdDocumentSequenceControllerBean hasRelation");

      Collection adDocumentSequenceAssignments = null;

      /**********************************************************
         Check if has relation with AdDocumentSequenceAssignment
      ***********************************************************/

      try {
         adDocumentSequenceAssignments =
            adDocumentSequence.getAdDocumentSequenceAssignments();
      } catch (Exception ex) {
      }

      if (adDocumentSequenceAssignments.size() != 0) return true;
      else return false;

   }

}
