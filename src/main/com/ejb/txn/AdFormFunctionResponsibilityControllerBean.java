
/*
 * AdFormFunctionResponsibilityControllerBean.java
 *
 * Created on May 28, 2004, 8:46 AM
 * 
 * @author  Neil Andrew M. Ajero
 */

package com.ejb.txn;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;
import javax.naming.NamingException;

import com.ejb.ad.LocalAdFormFunction;
import com.ejb.ad.LocalAdFormFunctionHome;
import com.ejb.ad.LocalAdFormFunctionResponsibility;
import com.ejb.ad.LocalAdFormFunctionResponsibilityHome;
import com.ejb.ad.LocalAdResponsibility;
import com.ejb.ad.LocalAdResponsibilityHome;
import com.util.AbstractSessionBean;
import com.util.AdModFormFunctionResponsibilityDetails;
import com.util.AdResponsibilityDetails;
import com.util.Debug;
import com.util.EJBHomeFactory;

/**
 * @ejb:bean name="AdFormFunctionResponsibilityControllerEJB"
 *           display-name="Used for entering payment schedules"
 *           type="Stateless"
 *           view-type="remote"
 *           jndi-name="ejb/AdFormFunctionResponsibilityControllerEJB"
 *
 * @ejb:interface remote-class="com.ejb.txn.AdFormFunctionResponsibilityController"
 *                extends="javax.ejb.EJBObject"
 *
 * @ejb:home remote-class="com.ejb.txn.AdFormFunctionResponsibilityControllerHome"
 *           extends="javax.ejb.EJBHome"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="aruser"
 *                        role-link="aruserlink"
 *
 * @ejb:permission role-name="aruser"
 * 
*/

public class AdFormFunctionResponsibilityControllerBean extends AbstractSessionBean {
    
    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
     public AdModFormFunctionResponsibilityDetails getAdFrByRsCodeAndFfCode(Integer RS_CODE, Integer FF_CODE, Integer AD_CMPNY) {
                    
        Debug.print("AdFormFunctionResponsibilityControllerBean getAdFrByRsCodeAndFfCode");

        LocalAdFormFunctionResponsibilityHome adFormFunctionResponsibilityHome = null;

        LocalAdFormFunctionResponsibility adFormFunctionResponsibility = null;

        ArrayList list = new ArrayList();
        
        // Initialize EJB Home

        try {
            
            adFormFunctionResponsibilityHome = (LocalAdFormFunctionResponsibilityHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdFormFunctionResponsibilityHome.JNDI_NAME, LocalAdFormFunctionResponsibilityHome.class);

        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }

        try {
            
        	adFormFunctionResponsibility = adFormFunctionResponsibilityHome.findByRsCodeAndFfCode(RS_CODE, FF_CODE, AD_CMPNY);
        	
        	AdModFormFunctionResponsibilityDetails mdetails = new AdModFormFunctionResponsibilityDetails();
    		mdetails.setFrCode(adFormFunctionResponsibility.getFrCode());        		
            mdetails.setFrParameter(adFormFunctionResponsibility.getFrParameter());
            mdetails.setFrFfCode(adFormFunctionResponsibility.getAdFormFunction().getFfCode());
            mdetails.setFrFfFormFunctionName(adFormFunctionResponsibility.getAdFormFunction().getFfName());
            
            return mdetails;
            
        } catch (FinderException ex) {
        	
        	return null;
            
        } catch (Exception ex) {
            
            throw new EJBException(ex.getMessage());
        }        
            
    }
    
    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
     public com.util.AdResponsibilityDetails getAdRsByRsCode(Integer RS_CODE, Integer AD_CMPNY) {
                    
        Debug.print("AdFormFunctionResponsibilityControllerBean getAdRsByRsCode");
        		    	         	   	        	    
        LocalAdResponsibilityHome adResponsibilityHome = null;
                                       
        // Initialize EJB Home
        
        try {
            
            adResponsibilityHome = (LocalAdResponsibilityHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdResponsibilityHome.JNDI_NAME, LocalAdResponsibilityHome.class);              
            
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {
        	
        	LocalAdResponsibility adResponsibility = adResponsibilityHome.findByPrimaryKey(RS_CODE);
   
        	AdResponsibilityDetails details = new AdResponsibilityDetails();
        		details.setRsCode(adResponsibility.getRsCode());        		
                details.setRsName(adResponsibility.getRsName());
                details.setRsDescription(adResponsibility.getRsDescription());
                        
        	return details;
        	       	
        } catch (Exception ex) {
        	
        	throw new EJBException(ex.getMessage());
        	
        }
        
	}      
	
    /**
     * @ejb:interface-method view-type="remote"
     **/    
     public void saveAdFrEntry(ArrayList list, Integer RS_CODE, Integer AD_CMPNY){

        Debug.print("AdFormFunctionResponsibilityControllerBean saveAdFrEntry");
        
        LocalAdFormFunctionResponsibilityHome adFormFunctionResponsibilityHome = null;
        LocalAdResponsibilityHome adResponsibilityHome = null;
        LocalAdFormFunctionHome adFormFunctionHome = null;
                       
        // Initialize EJB Home
        
        try {
            
            adResponsibilityHome = (LocalAdResponsibilityHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdResponsibilityHome.JNDI_NAME, LocalAdResponsibilityHome.class);
            adFormFunctionResponsibilityHome = (LocalAdFormFunctionResponsibilityHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdFormFunctionResponsibilityHome.JNDI_NAME, LocalAdFormFunctionResponsibilityHome.class);                
            adFormFunctionHome = (LocalAdFormFunctionHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdFormFunctionHome.JNDI_NAME, LocalAdFormFunctionHome.class);                                                      
                
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
                                   
        try {
        
            LocalAdFormFunctionResponsibility adFormFunctionResponsibility = null;

        	// delete all existing form function responsibility  
        	
        	Collection formFunctions = adFormFunctionResponsibilityHome.findByRsCode(RS_CODE, AD_CMPNY);

        	Iterator i = formFunctions.iterator();
        	
        	while(i.hasNext()) {

        		adFormFunctionResponsibility = (LocalAdFormFunctionResponsibility)i.next();
        		
        		i.remove();
        		
        		adFormFunctionResponsibility.remove();
        		
        	}

        	// create new form function responsibility
        	
        	i = list.iterator();
        	
        	while(i.hasNext()) {
        	
        	    AdModFormFunctionResponsibilityDetails mdetails = (AdModFormFunctionResponsibilityDetails)i.next();
       		    
        		adFormFunctionResponsibility = adFormFunctionResponsibilityHome.create(mdetails.getFrParameter(), AD_CMPNY);

                // add form function
                
                LocalAdFormFunction adFormFunction = adFormFunctionHome.findByPrimaryKey(mdetails.getFrFfCode());
                adFormFunction.addAdFormFunctionResponsibility(adFormFunctionResponsibility);
                
                // add responsibility
                
                LocalAdResponsibility adResposibility = adResponsibilityHome.findByPrimaryKey(RS_CODE);
                adResposibility.addAdFormFunctionResponsibility(adFormFunctionResponsibility);

        	}    	

        } catch (Exception ex) {
        	
        	getSessionContext().setRollbackOnly();
            throw new EJBException(ex.getMessage());
        	
        }        
                                    	                                        
    }	
    
    // SessionBean methods

    /**
     * @ejb:create-method view-type="remote"
     **/
     public void ejbCreate() throws CreateException {

       Debug.print("AdFormFunctionResponsibilityControllerBean ejbCreate");
      
    }
}
