
/*
 * AdLogControllerBean.java
 *
 * Created on January 16, 20109, 12:15 PM
 *
 * @author  Ruben P. Lamberte
 */
 
package com.ejb.txn;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;
import javax.naming.NamingException;

import com.ejb.ad.LocalAdBranch;
import com.ejb.ad.LocalAdBranchHome;
import com.ejb.ad.LocalAdBranchSalesperson;
import com.ejb.ad.LocalAdBranchSalespersonHome;
import com.ejb.ad.LocalAdLog;
import com.ejb.ad.LocalAdLogHome;
import com.ejb.ar.LocalArSalesperson;
import com.ejb.ar.LocalArSalespersonHome;
import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.exception.GlobalRecordAlreadyAssignedException;
import com.ejb.exception.GlobalRecordAlreadyDeletedException;
import com.ejb.exception.GlobalRecordAlreadyExistException;
import com.util.AbstractSessionBean;
import com.util.AdBranchDetails;
import com.util.AdLogDetails;
import com.util.Debug;
import com.util.EJBHomeFactory;

/**
 * @ejb:bean name="AdLogControllerEJB"
 *           display-name="Viewing Logs"
 *           type="Stateless"
 *           view-type="remote"
 *           jndi-name="ejb/AdLogControllerEJB"
 *
 * @ejb:interface remote-class="com.ejb.txn.AdLogController"
 *                extends="javax.ejb.EJBObject"
 *
 * @ejb:home remote-class="com.ejb.txn.AdLogControllerHome"
 *           extends="javax.ejb.EJBHome"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="aruser"
 *                        role-link="aruserlink"
 *
 * @ejb:permission role-name="aruser"
 * 
*/

public class AdLogControllerBean extends AbstractSessionBean {

  
    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList getAdLogAllByLogMdlAndLogMdlKy(String LOG_MDL, Integer LOG_MDL_KY, Integer AD_CMPNY) 
    	throws GlobalNoRecordFoundException{
    	
    	Debug.print("AdLogModuleControllerBean getAdLogAllByLogMdlAndLogMdlKy");

    	 LocalAdLogHome adLogHome = null;

         ArrayList list = new ArrayList();
         
         // Initialize EJB Home

         try {

         	adLogHome = (LocalAdLogHome)EJBHomeFactory.
                 lookUpLocalHome(LocalAdLogHome.JNDI_NAME, LocalAdLogHome.class);

         } catch (NamingException ex) {
             
             throw new EJBException(ex.getMessage());
             
         }
         
         
         System.out.println("MODULE: "+ LOG_MDL);
         System.out.println("MODULE KEY : "+ LOG_MDL_KY);
      
         try {
	        	
 	        Collection adLogs = adLogHome.findByLogMdlAndLogMdlKy(LOG_MDL, LOG_MDL_KY);
 	
 	        if (adLogs.isEmpty()) {
 	
 	           return new ArrayList();
 	        	
 	        }
 	            
 	        Iterator i = adLogs.iterator();
 	               
 	        while (i.hasNext()) {
 	        	
 	        	LocalAdLog adLog = (LocalAdLog)i.next();
      
 	        	AdLogDetails details = new AdLogDetails();
 	        	
 	        	details.setLogCode(adLog.getLogCode());
 	        	details.setLogDate(adLog.getLogDate());
 	        	details.setLogUsername(adLog.getLogUsername());
 	        	details.setLogAction(adLog.getLogAction());
 	        	details.setLogModule(adLog.getLogModule());
 	        	details.setLogDescription(adLog.getLogDescription());
         		list.add(details);
 	        		
 	        }              
 	                                                        		        		        
 	        return list;
 	        
        
         	        	
         } catch (Exception ex) {
         	
         	Debug.printStackTrace(ex);
         	throw new EJBException(ex.getMessage());
         	
         }
    	
    }
    
    
    /**
     * @ejb:interface-method view-type="remote"
     **/    
    public void addAdLogEntry(com.util.AdLogDetails details,  Integer AD_CMPNY) 
        throws GlobalRecordAlreadyExistException {
                    
        Debug.print("AdLogControllerBean addAdLogEntry");
        
        
        
        LocalAdLogHome adLogHome = null;
        
      
    
        
        // Initialize EJB Home
        
        try {
            
        	adLogHome = (LocalAdLogHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdLogHome.JNDI_NAME, LocalAdLogHome.class);
          
                                         
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {
        	
        	LocalAdLog adLog = null;
          
	     
	         
	    	// create new salesperson
	    				    	        	        	    	
        	adLog = adLogHome.create(details.getLogDate(),details.getLogUsername(), details.getLogAction(), details.getLogDescription(), details.getLogModule(), details.getLogModuleKey());
            
          

	    	        	    		        	    		    		    			        	        
     
        	
        } catch (Exception ex) {
        	
        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());
        	
        }
            
    }    
    
    
    /**
     * @ejb:interface-method view-type="remote"
     **/    
    public void addAdLogEntry(Integer LOG_MDL_KY, String LOG_MDL, java.util.Date LOG_DT, String LOG_USRNM, String LOG_ACTN, String LOG_DESC ,  Integer AD_CMPNY) 
        throws GlobalRecordAlreadyExistException {
                    
        Debug.print("AdLogControllerBean addAdLogEntry");
        
        
        
        LocalAdLogHome adLogHome = null;
        
      
    
        
        // Initialize EJB Home
        
        try {
            
        	adLogHome = (LocalAdLogHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdLogHome.JNDI_NAME, LocalAdLogHome.class);
          
                                         
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {
        	
        	LocalAdLog adLog = null;
          
	     
	         
	    	// create new salesperson
	    				    	        	        	    	
        	adLog = adLogHome.create(LOG_DT,LOG_USRNM, LOG_ACTN, LOG_DESC,LOG_MDL, LOG_MDL_KY);
            
          

	    	        	    		        	    		    		    			        	        
     
        	
        } catch (Exception ex) {
        	
        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());
        	
        }
            
    }    
   
    
    /**
     * @ejb:create-method view-type="remote"
     **/
    public void ejbCreate() throws CreateException {

        Debug.print("AdLogControllerBean ejbCreate");
      
    }
    
}

