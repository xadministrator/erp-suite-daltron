
/*
 * AdLookUpValueControllerBean.java
 *
 * Created on May 21, 2003, 4:07 PM
 *
 * @author  Neil Andrew M. Ajero
 */

package com.ejb.txn;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;
import javax.ejb.RemoveException;
import javax.naming.NamingException;

import com.ejb.ad.LocalAdLookUp;
import com.ejb.ad.LocalAdLookUpHome;
import com.ejb.ad.LocalAdLookUpValue;
import com.ejb.ad.LocalAdLookUpValueHome;
import com.ejb.ar.LocalArCustomer;
import com.ejb.ar.LocalArCustomerHome;
import com.ejb.ar.LocalArInvoice;
import com.ejb.ar.LocalArInvoiceHome;
import com.ejb.ar.LocalArReceipt;
import com.ejb.ar.LocalArReceiptHome;
import com.ejb.exception.AdLUNoLookUpFoundException;
import com.ejb.exception.AdLVMasterCodeNotFoundException;
import com.ejb.exception.AdLVNoLookUpValueFoundException;
import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.exception.GlobalRecordAlreadyAssignedException;
import com.ejb.exception.GlobalRecordAlreadyDeletedException;
import com.ejb.exception.GlobalRecordAlreadyExistException;
import com.ejb.inv.LocalInvItem;
import com.ejb.inv.LocalInvItemHome;
import com.ejb.inv.LocalInvLocation;
import com.ejb.inv.LocalInvLocationHome;
import com.ejb.inv.LocalInvPhysicalInventory;
import com.ejb.inv.LocalInvPhysicalInventoryHome;
import com.ejb.inv.LocalInvPriceLevel;
import com.ejb.inv.LocalInvPriceLevelHome;
import com.ejb.inv.LocalInvUnitOfMeasure;
import com.ejb.inv.LocalInvUnitOfMeasureHome;
import com.util.AbstractSessionBean;
import com.util.AdLookUpValueDetails;
import com.util.Debug;
import com.util.EJBHomeFactory;

/**
 * @ejb:bean name="AdLookUpValueControllerEJB"
 *           display-name="Used for entering banks"
 *           type="Stateless"
 *           view-type="remote"
 *           jndi-name="ejb/AdLookUpValueControllerEJB"
 *
 * @ejb:interface remote-class="com.ejb.txn.AdLookUpValueController"
 *                extends="javax.ejb.EJBObject"
 *
 * @ejb:home remote-class="com.ejb.txn.AdLookUpValueControllerHome"
 *           extends="javax.ejb.EJBHome"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="aduser"
 *                        role-link="aduserlink"
 *
 * @ejb:permission role-name="aduser"
 * 
*/

public class AdLookUpValueControllerBean extends AbstractSessionBean {
    
    
    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList getAdLuAll(Integer AD_CMPNY) 
        throws GlobalNoRecordFoundException {
                    
        Debug.print("AdLookUpValueControllerBean getAdLuAll");
        
        LocalAdLookUpHome adLookUpHome = null;
        
        Collection adLookUps = null;
        
        LocalAdLookUp adLookUp = null;
        
        ArrayList list = new ArrayList();
        
        // Initialize EJB Home
        
        try {
            
            adLookUpHome = (LocalAdLookUpHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdLookUpHome.JNDI_NAME, LocalAdLookUpHome.class);
            
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {
            
            adLookUps = adLookUpHome.findLuAll(AD_CMPNY);
            
        } catch (FinderException ex) {
            
        } catch (Exception ex) {
            
            throw new EJBException(ex.getMessage());
        }
        
        if (adLookUps.isEmpty()) {
        	
            throw new GlobalNoRecordFoundException();
        	
        }
        
        Iterator i = adLookUps.iterator();
               
        while (i.hasNext()) {
        	
        	adLookUp = (LocalAdLookUp)i.next();
        	
        	list.add(adLookUp.getLuName());
        	
        }
        
        return list;
            
    }

   /**
    * @ejb:interface-method view-type="remote"
    * @jboss:method-attributes read-only="true"
    **/
   public ArrayList getAdLvByLuName(String LU_NM, Integer AD_CMPNY)
      throws AdLVNoLookUpValueFoundException,
             AdLUNoLookUpFoundException {

      Debug.print("AdLookUpValueControllerBean getAdLvByLuName");
     
      LocalAdLookUpValueHome adLookUpValueHome = null;
      LocalAdLookUpHome adLookUpHome = null;

      ArrayList lvAllList = new ArrayList();
      Collection adLookUpValues = null;
      
      
      // Initialize EJB Home
        
      try {
            
          adLookUpValueHome = (LocalAdLookUpValueHome)EJBHomeFactory.
             lookUpLocalHome(LocalAdLookUpValueHome.JNDI_NAME, LocalAdLookUpValueHome.class);
          adLookUpHome = (LocalAdLookUpHome)EJBHomeFactory.
             lookUpLocalHome(LocalAdLookUpHome.JNDI_NAME, LocalAdLookUpHome.class);
            
      } catch (NamingException ex) {
            
          throw new EJBException(ex.getMessage());
            
      }
      
      try {
      	
      	 adLookUpHome.findByLookUpName(LU_NM, AD_CMPNY);
      	 
      } catch (FinderException ex) {
      	
      	  throw new AdLUNoLookUpFoundException();
      	
      } catch (Exception ex) {
      	
         throw new EJBException(ex.getMessage());
         
      }

      try {
      	
         adLookUpValues = adLookUpValueHome.findByLuName(LU_NM, AD_CMPNY);
                  
      } catch (Exception ex) {
      	
         throw new EJBException(ex.getMessage());
         
      }
      
      if (adLookUpValues.size() == 0) {
      	
      	throw new AdLVNoLookUpValueFoundException();
      	
      }
 
      Iterator i = adLookUpValues.iterator();
      
      while (i.hasNext()) {
      	
         LocalAdLookUpValue adLookUpValue = (LocalAdLookUpValue) i.next();
	       
		 AdLookUpValueDetails details = new AdLookUpValueDetails(
		    adLookUpValue.getLvCode(), adLookUpValue.getLvName(), 
		    adLookUpValue.getLvDescription(), adLookUpValue.getLvMasterCode(),
		    adLookUpValue.getLvEnable(), adLookUpValue.getLvDownloadStatus(), adLookUpValue.getLvAdCompany());

	     lvAllList.add(details);
      
      }

      return lvAllList;
   }
    
    /**
     * @ejb:interface-method view-type="remote"
     **/    
    public void addAdLvEntry(com.util.AdLookUpValueDetails details, String LU_NM, Integer AD_CMPNY) 
        throws GlobalRecordAlreadyExistException,
               AdLVMasterCodeNotFoundException {
                    
        Debug.print("AdLookUpValueControllerBean addAdLvEntry");
        
        LocalAdLookUpValueHome adLookUpValueHome = null;
        LocalAdLookUpHome adLookUpHome = null;
        LocalInvItemHome invItemHome = null;
        LocalInvPriceLevelHome invPriceLevelHome = null;

        LocalAdLookUpValue adLookUpValue = null;
                
        ArrayList list = new ArrayList();
        
        // Initialize EJB Home
        
        try {
            
            adLookUpValueHome = (LocalAdLookUpValueHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdLookUpValueHome.JNDI_NAME, LocalAdLookUpValueHome.class);
            adLookUpHome = (LocalAdLookUpHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdLookUpHome.JNDI_NAME, LocalAdLookUpHome.class);
            invItemHome = (LocalInvItemHome)EJBHomeFactory.
            	lookUpLocalHome(LocalInvItemHome.JNDI_NAME, LocalInvItemHome.class);
            invPriceLevelHome = (LocalInvPriceLevelHome)EJBHomeFactory.
        		lookUpLocalHome(LocalInvPriceLevelHome.JNDI_NAME, LocalInvPriceLevelHome.class);

        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
    
        try { 
            
           adLookUpValue = adLookUpValueHome.findByLuNameAndLvName(LU_NM, details.getLvName(), AD_CMPNY);
           	
           	 throw new GlobalRecordAlreadyExistException();
                           
        } catch (GlobalRecordAlreadyExistException ex) {
        	
           throw ex;
        	
        } catch (FinderException ex) {
        	 	
        } catch (Exception ex) {
            
           throw new EJBException(ex.getMessage());
            
        }
        
        /*try { 
        
	        // check if the input master code exist
	        
	        if (details.getLvMasterCode() != null) {
	        
		        LocalAdLookUpValue existingMasterCode = 
		           adLookUpValueHome.findByLvCodeAndLuName(details.getLvMasterCode(), LU_NM, AD_CMPNY);
		        
		    }

        	
        } catch (FinderException ex) {
        	        	
        	throw new AdLVMasterCodeNotFoundException();
        	 	
        } catch (Exception ex) {
            
           throw new EJBException(ex.getMessage());
            
        }*/        
        
        
        
               
        try {
        	
        	// create new look up value
        	
        	adLookUpValue = adLookUpValueHome.create(details.getLvName(), 
        	   details.getLvDescription(), details.getLvMasterCode(), 
        	   details.getLvEnable(), 'N',  AD_CMPNY); 
        	   
        	LocalAdLookUp adLookUp = adLookUpHome.findByLookUpName(LU_NM, AD_CMPNY);
		      	adLookUp.addAdLookUpValue(adLookUpValue);     
		      	
		    // if lookup value is INV PRICE LEVELS then add in AdPriceLevelBean
		    if (LU_NM.equals("INV PRICE LEVEL")) {
		    	
		    	// loop in InvItem
		    	Collection invItems = invItemHome.findIiAll(AD_CMPNY);
		    	Iterator i = invItems.iterator();
		    	
		    	while (i.hasNext()) {
		    		
		    		LocalInvItem invItem = (LocalInvItem)i.next();
		    		
		    		LocalInvPriceLevel invPriceLevel = invPriceLevelHome.create(invItem.getIiSalesPrice(), 0d, invItem.getIiPercentMarkup(), invItem.getIiShippingCost(), adLookUpValue.getLvName(), 'N', AD_CMPNY);
		    		
		    		invItem.addInvPriceLevel(invPriceLevel);
		    		
		    	}
		    	
		    }
                	       
        } catch (Exception ex) {
        	
        	getSessionContext().setRollbackOnly();
            throw new EJBException(ex.getMessage());
        	
        }    
        
    }
    
    
    
    /**
     * @ejb:interface-method view-type="remote"
     **/
    public void updateAdLvEntry(com.util.AdLookUpValueDetails details, String LU_NM, Integer AD_CMPNY) 
        throws GlobalRecordAlreadyExistException,
               AdLVMasterCodeNotFoundException {
                    
        Debug.print("AdLookUpValueControllerBean updateAdLvEntry");
        
        LocalAdLookUpValueHome adLookUpValueHome = null;
        LocalAdLookUpHome adLookUpHome = null;

        LocalAdLookUpValue adLookUpValue = null;
        LocalAdLookUp adLookUp = null;
                
        LocalInvItemHome invItemHome = null;
        LocalInvPriceLevelHome invPriceLevelHome = null;
        LocalArCustomerHome arCustomerHome = null;
        LocalArInvoiceHome arInvoiceHome = null;
        LocalInvLocationHome invLocationHome = null;
        LocalInvUnitOfMeasureHome invUnitOfMeasureHome = null;
        LocalInvPhysicalInventoryHome invPhysicalInventoryHome = null;
        LocalArReceiptHome arReceiptHome = null;
        
        // ArrayList list = new ArrayList();
        
        // Initialize EJB Home
        
        try {
            
            adLookUpValueHome = (LocalAdLookUpValueHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdLookUpValueHome.JNDI_NAME, LocalAdLookUpValueHome.class);
            adLookUpHome = (LocalAdLookUpHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdLookUpHome.JNDI_NAME, LocalAdLookUpHome.class);
            invItemHome = (LocalInvItemHome)EJBHomeFactory.
            	lookUpLocalHome(LocalInvItemHome.JNDI_NAME, LocalInvItemHome.class);
            invPriceLevelHome = (LocalInvPriceLevelHome)EJBHomeFactory.
        		lookUpLocalHome(LocalInvPriceLevelHome.JNDI_NAME, LocalInvPriceLevelHome.class);
            arCustomerHome = (LocalArCustomerHome)EJBHomeFactory.
    			lookUpLocalHome(LocalArCustomerHome.JNDI_NAME, LocalArCustomerHome.class);
            
            arInvoiceHome = (LocalArInvoiceHome)EJBHomeFactory.
            	lookUpLocalHome(LocalArInvoiceHome.JNDI_NAME, LocalArInvoiceHome.class);
            invLocationHome = (LocalInvLocationHome)EJBHomeFactory.
            	lookUpLocalHome(LocalInvLocationHome.JNDI_NAME, LocalInvLocationHome.class);
            invUnitOfMeasureHome = (LocalInvUnitOfMeasureHome)EJBHomeFactory.
            	lookUpLocalHome(LocalInvUnitOfMeasureHome.JNDI_NAME, LocalInvUnitOfMeasureHome.class);
            invPhysicalInventoryHome = (LocalInvPhysicalInventoryHome)EJBHomeFactory.
            	lookUpLocalHome(LocalInvPhysicalInventoryHome.JNDI_NAME, LocalInvPhysicalInventoryHome.class);
            arReceiptHome = (LocalArReceiptHome)EJBHomeFactory.
            	lookUpLocalHome(LocalArReceiptHome.JNDI_NAME, LocalArReceiptHome.class);   
            
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }

        try { 
            
            adLookUpValue = adLookUpValueHome.findByLuNameAndLvName(LU_NM, details.getLvName(), AD_CMPNY);
            
            if (!adLookUpValue.getLvCode().equals(details.getLvCode())) {
            
                 throw new GlobalRecordAlreadyExistException();
                 
            }
                                             
        } catch (GlobalRecordAlreadyExistException ex) {
        	
            throw ex;
        	
        } catch (FinderException ex) {
        	 	
        } catch (Exception ex) {
            
            throw new EJBException(ex.getMessage());
            
        }

        /*try { 
        
	        // check if the input master code exist
	        
	        if (details.getLvMasterCode() != null) {
	        
		        LocalAdLookUpValue existingMasterCode = 
		           adLookUpValueHome.findByLvCodeAndLuName(details.getLvMasterCode(), LU_NM, AD_CMPNY);
		        
		    }

        	
        } catch (FinderException ex) {
        	        	
        	throw new AdLVMasterCodeNotFoundException();
        	 	
        } catch (Exception ex) {
            
           throw new EJBException(ex.getMessage());
            
        } */
               
        try {
        	
        	// find and update look up value
        	
        	adLookUpValue = adLookUpValueHome.findByPrimaryKey(details.getLvCode());
        	
        	String originalLvName = adLookUpValue.getLvName();
        	
        	adLookUpValue.setLvName(details.getLvName());
        	adLookUpValue.setLvDescription(details.getLvDescription());
        	adLookUpValue.setLvMasterCode(details.getLvMasterCode());
        	adLookUpValue.setLvEnable(details.getLvEnable());
        	adLookUpValue.setLvDownloadStatus('U');
        	
        	adLookUp = adLookUpHome.findByLookUpName(LU_NM, AD_CMPNY);
        	adLookUp.addAdLookUpValue(adLookUpValue); 
        	 
  	    	if (LU_NM.equals("AR FREIGHT")) {
        		
        		// update invoices
        		Collection arInvoices = arInvoiceHome.findByInvLvFreight(originalLvName, AD_CMPNY);
        		Iterator i = arInvoices.iterator();
        		
        		while(i.hasNext()) {
        			
        			LocalArInvoice arInvoice = (LocalArInvoice)i.next();
        			arInvoice.setInvLvFreight(details.getLvName());
        			
        		}
        		
        	} else if (LU_NM.equals("AR REGION")) {
        		
        		// update customers
        		Collection arCustomers = arCustomerHome.findByCstRegion(originalLvName, AD_CMPNY);
        		Iterator i = arCustomers.iterator();
        		
        		while(i.hasNext()) {
        			
        			LocalArCustomer arCustomer = (LocalArCustomer)i.next();
        			arCustomer.setCstAdLvRegion(details.getLvName());
        			
        		}
        		
        	} else if (LU_NM.equals("INV LOCATION TYPE")) {
        		
        		// update locations
        		Collection invLocations = invLocationHome.findByLocAdLvType(originalLvName, AD_CMPNY);
        		Iterator i = invLocations.iterator();
        		
        		while(i.hasNext()) {
        			
        			LocalInvLocation invLocation = (LocalInvLocation)i.next();
        			invLocation.setLocLvType(details.getLvName());
        			
        		}
        		
        	} else if (LU_NM.equals("INV ITEM CATEGORY")) {
        		
        		// update items
        		Collection invItems = invItemHome.findEnabledIiByIiAdLvCategory(originalLvName, AD_CMPNY);
        		Iterator i = invItems.iterator();
        		
        		while(i.hasNext()) {
        			
        			LocalInvItem invItem = (LocalInvItem)i.next();
        			invItem.setIiAdLvCategory(details.getLvName());
        			
        		}
        		
        		// update physical inventories
        		Collection invPhysicalInventories = invPhysicalInventoryHome.findByPiAdLvCategory(originalLvName, AD_CMPNY);
        		i = invPhysicalInventories.iterator();
        		
        		while(i.hasNext()) {
        			
        			LocalInvPhysicalInventory invPhysicalInventory = (LocalInvPhysicalInventory)i.next();
        			invPhysicalInventory.setPiAdLvCategory(details.getLvName());
        			
        		}  
        		
        	} else if (LU_NM.equals("INV UNIT OF MEASURE CLASS")) {
        		
        		// update unit measures
        		Collection invUnitOfMeasures = invUnitOfMeasureHome.findByUomAdLvClass(originalLvName, AD_CMPNY);
        		Iterator i = invUnitOfMeasures.iterator();
        		
        		while(i.hasNext()) {
        			
        			LocalInvUnitOfMeasure invUnitOfMeasure = (LocalInvUnitOfMeasure)i.next();
        			invUnitOfMeasure.setUomAdLvClass(details.getLvName());
        			
        		}
        		
        	} else if (LU_NM.equals("INV SHIFT")) {
        		
        		// update invoices
        		Collection arInvoices = arInvoiceHome.findByInvLvShift(originalLvName, AD_CMPNY);
        		Iterator i = arInvoices.iterator();
        		
        		while(i.hasNext()) {
        			
        			LocalArInvoice arInvoice = (LocalArInvoice)i.next();
        			arInvoice.setInvLvShift(details.getLvName());
        			
        		}
        		
        		// update receipts
        		Collection arReceipts = arReceiptHome.findByRctLvShift(originalLvName, AD_CMPNY);
        		i = arReceipts.iterator();
        		
        		while(i.hasNext()) {
        			
        			LocalArReceipt arReceipt = (LocalArReceipt)i.next();
        			arReceipt.setRctLvShift(details.getLvName());
        			
        		}
        		
        	} else if (LU_NM.equals("INV DONENESS")) {

        		// update items
        		Collection invItems = invItemHome.findByIiDoneness(originalLvName, AD_CMPNY);
        		Iterator i = invItems.iterator();
        		
        		while(i.hasNext()) {
        			
        			LocalInvItem invItem = (LocalInvItem)i.next();
        			invItem.setIiDoneness(details.getLvName());
        			
        		}
        		
        	} else if (LU_NM.equals("INV PRICE LEVEL")) {
        		
        		// update price levels and customer deal price
        		Collection invPriceLevels = invPriceLevelHome.findByPlAdLvPriceLevel(originalLvName, AD_CMPNY);
        		Iterator i = invPriceLevels.iterator();
        		
        		while (i.hasNext()) {
        			
        			LocalInvPriceLevel invPriceLevel = (LocalInvPriceLevel)i.next();
        			
        			invPriceLevel.setPlAdLvPriceLevel(details.getLvName());
        			
        		}
        		
        		Collection arCustomers = arCustomerHome.findByCstDealPrice(originalLvName, AD_CMPNY);
        		i = arCustomers.iterator();
        		
        		while (i.hasNext()) {
        			
        			LocalArCustomer arCustomer = (LocalArCustomer)i.next();
        			
        			arCustomer.setCstDealPrice(details.getLvName());
        			
        		}
        		
        	}
        	
        } catch (Exception ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
    }
    
    /**
    * @ejb:interface-method view-type="remote"
    **/
    public void deleteAdLvEntry(Integer LV_CODE, Integer AD_CMPNY) 
        throws GlobalRecordAlreadyDeletedException,
               GlobalRecordAlreadyAssignedException {               

      Debug.print("AdLookUpValueControllerBean deleteAdLvEntry");

      LocalAdLookUpValue adLookUpValue = null;
      LocalAdLookUpValueHome adLookUpValueHome = null;
      LocalArInvoiceHome arInvoiceHome = null;
      LocalInvItemHome invItemHome =null;
      LocalInvLocationHome invLocationHome = null;
      LocalInvUnitOfMeasureHome invUnitOfMeasureHome = null;
      LocalInvPhysicalInventoryHome invPhysicalInventoryHome = null;
      LocalInvPriceLevelHome invPriceLevelHome = null;
      LocalArCustomerHome arCustomerHome = null;
      LocalArReceiptHome arReceiptHome = null;
      
      

      // Initialize EJB Home
        
      try {

          adLookUpValueHome = (LocalAdLookUpValueHome)EJBHomeFactory.
              lookUpLocalHome(LocalAdLookUpValueHome.JNDI_NAME, LocalAdLookUpValueHome.class);
          arInvoiceHome = (LocalArInvoiceHome)EJBHomeFactory.
              lookUpLocalHome(LocalArInvoiceHome.JNDI_NAME, LocalArInvoiceHome.class);
          invItemHome = (LocalInvItemHome)EJBHomeFactory.
              lookUpLocalHome(LocalInvItemHome.JNDI_NAME, LocalInvItemHome.class);
          invLocationHome = (LocalInvLocationHome)EJBHomeFactory.
              lookUpLocalHome(LocalInvLocationHome.JNDI_NAME, LocalInvLocationHome.class);
          invUnitOfMeasureHome = (LocalInvUnitOfMeasureHome)EJBHomeFactory.
              lookUpLocalHome(LocalInvUnitOfMeasureHome.JNDI_NAME, LocalInvUnitOfMeasureHome.class);
          invPhysicalInventoryHome = (LocalInvPhysicalInventoryHome)EJBHomeFactory.
              lookUpLocalHome(LocalInvPhysicalInventoryHome.JNDI_NAME, LocalInvPhysicalInventoryHome.class);
          
          arCustomerHome = (LocalArCustomerHome)EJBHomeFactory.
              lookUpLocalHome(LocalArCustomerHome.JNDI_NAME, LocalArCustomerHome.class);   
          invPriceLevelHome = (LocalInvPriceLevelHome)EJBHomeFactory.
          	  lookUpLocalHome(LocalInvPriceLevelHome.JNDI_NAME, LocalInvPriceLevelHome.class);   
          arReceiptHome = (LocalArReceiptHome)EJBHomeFactory.
      	  	lookUpLocalHome(LocalArReceiptHome.JNDI_NAME, LocalArReceiptHome.class);   

      
      } catch (NamingException ex) {

          throw new EJBException(ex.getMessage());

      }                
      
      try {
      	
         adLookUpValue = adLookUpValueHome.findByPrimaryKey(LV_CODE);
         
      } catch (FinderException ex) {
      	
         throw new GlobalRecordAlreadyDeletedException();
         
      } catch (Exception ex) {
      	
         throw new EJBException(ex.getMessage()); 
         
      }
      
      try {
      
	      Collection arInvoices1 = arInvoiceHome.findByInvLvFreight(adLookUpValue.getLvName(), AD_CMPNY);
	      Collection arInvoices2 = arInvoiceHome.findByInvLvShift(adLookUpValue.getLvName(), AD_CMPNY);
	      Collection arReceipt = arReceiptHome.findByRctLvShift(adLookUpValue.getLvName(), AD_CMPNY);
	      Collection invItems1 = invItemHome.findEnabledIiByIiAdLvCategory(adLookUpValue.getLvName(), AD_CMPNY);
	      Collection invItems2 = invItemHome.findByIiDoneness(adLookUpValue.getLvName(), AD_CMPNY);
	      Collection invLocations = invLocationHome.findByLocAdLvType(adLookUpValue.getLvName(), AD_CMPNY);
	      Collection invUnitOfMeasures = invUnitOfMeasureHome.findByUomAdLvClass(adLookUpValue.getLvName(), AD_CMPNY);
	      Collection invPhysicalInventories = invPhysicalInventoryHome.findByPiAdLvCategory(adLookUpValue.getLvName(), AD_CMPNY);
	      
	      Collection arCustomers1 = arCustomerHome.findByCstDealPrice(adLookUpValue.getLvName(), AD_CMPNY);
	      
	      Collection arCustomers2 = arCustomerHome.findByCstRegion(adLookUpValue.getLvName(), AD_CMPNY);
	      
	      if (!arInvoices1.isEmpty() || !arInvoices2.isEmpty() || !arReceipt.isEmpty() || 
	              !invItems1.isEmpty() || !invItems2.isEmpty() || !invLocations.isEmpty() ||
		          !invUnitOfMeasures.isEmpty() || !invPhysicalInventories.isEmpty() || 
		          !arCustomers1.isEmpty() || !arCustomers2.isEmpty()) {
			  
			      throw new GlobalRecordAlreadyAssignedException();
			  
	      }
	      
	      if (arCustomers1.isEmpty()) {
	      	
	      	Collection invPriceLevels = invPriceLevelHome.findByPlAdLvPriceLevel(adLookUpValue.getLvName(), AD_CMPNY);
      		Iterator i = invPriceLevels.iterator();
      		
      		while (i.hasNext()) {
      			
      			LocalInvPriceLevel invPriceLevel = (LocalInvPriceLevel)i.next();
      			try {
      				
      				invPriceLevel.remove();
      				
      			}catch (RemoveException ex){
      				
      				getSessionContext().setRollbackOnly();
      			    
      				throw new EJBException(ex.getMessage());
      			}
      			
      			
      		}
	      	
	      } 
      
      } catch (FinderException ex) {
      	
      }
      
      try {
         	
	      adLookUpValue.remove();
	    
	  } catch (RemoveException ex) {
	 	
	      getSessionContext().setRollbackOnly();
	    
	      throw new EJBException(ex.getMessage());
	    
	  } catch (Exception ex) {
	 	
	      getSessionContext().setRollbackOnly();
	    
	      throw new EJBException(ex.getMessage());
	    
	  }	      
      
   }    
        
    // SessionBean methods

    /**
     * @ejb:create-method view-type="remote"
     **/
    public void ejbCreate() throws CreateException {

       Debug.print("AdLookUpValueControllerBean ejbCreate");
      
    }
}
