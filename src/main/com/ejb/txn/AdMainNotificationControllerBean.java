package com.ejb.txn;

import java.rmi.RemoteException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Iterator;
import java.util.StringTokenizer;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;
import javax.naming.NamingException;

import com.ejb.ad.LocalAdAmountLimit;
import com.ejb.ad.LocalAdAmountLimitHome;
import com.ejb.ad.LocalAdApproval;
import com.ejb.ad.LocalAdApprovalDocument;
import com.ejb.ad.LocalAdApprovalDocumentHome;
import com.ejb.ad.LocalAdApprovalHome;
import com.ejb.ad.LocalAdApprovalQueue;
import com.ejb.ad.LocalAdApprovalQueueHome;
import com.ejb.ad.LocalAdApprovalUser;
import com.ejb.ad.LocalAdApprovalUserHome;
import com.ejb.ad.LocalAdBankAccount;
import com.ejb.ad.LocalAdBankAccountBalance;
import com.ejb.ad.LocalAdBankAccountBalanceHome;
import com.ejb.ad.LocalAdBankAccountHome;
import com.ejb.ad.LocalAdBranchArTaxCode;
import com.ejb.ad.LocalAdBranchArTaxCodeHome;
import com.ejb.ad.LocalAdBranchBankAccount;
import com.ejb.ad.LocalAdBranchBankAccountHome;
import com.ejb.ad.LocalAdBranchCustomerHome;
import com.ejb.ad.LocalAdBranchDocumentSequenceAssignment;
import com.ejb.ad.LocalAdBranchDocumentSequenceAssignmentHome;
import com.ejb.ad.LocalAdBranchItemLocation;
import com.ejb.ad.LocalAdBranchItemLocationHome;
import com.ejb.ad.LocalAdBranchStandardMemoLine;
import com.ejb.ad.LocalAdBranchStandardMemoLineHome;
import com.ejb.ad.LocalAdCompany;
import com.ejb.ad.LocalAdCompanyHome;
import com.ejb.ad.LocalAdDiscount;
import com.ejb.ad.LocalAdDiscountHome;
import com.ejb.ad.LocalAdPaymentSchedule;
import com.ejb.ad.LocalAdPaymentScheduleHome;
import com.ejb.ad.LocalAdPaymentTerm;
import com.ejb.ad.LocalAdPaymentTermHome;
import com.ejb.ad.LocalAdPreference;
import com.ejb.ad.LocalAdPreferenceHome;
import com.ejb.ad.LocalAdUser;
import com.ejb.ad.LocalAdUserHome;
import com.ejb.ap.LocalApCheck;
import com.ejb.ap.LocalApCheckHome;
import com.ejb.ap.LocalApPurchaseOrderHome;
import com.ejb.ap.LocalApPurchaseOrderLineHome;
import com.ejb.ap.LocalApPurchaseRequisitionHome;
import com.ejb.ap.LocalApRecurringVoucherHome;
import com.ejb.ap.LocalApSupplier;
import com.ejb.ap.LocalApTaxCode;
import com.ejb.ap.LocalApVoucher;
import com.ejb.ap.LocalApVoucherHome;
import com.ejb.ap.LocalApVoucherLineItem;
import com.ejb.ar.LocalArAppliedInvoice;
import com.ejb.ar.LocalArAppliedInvoiceHome;
import com.ejb.ar.LocalArAutoAccountingSegment;
import com.ejb.ar.LocalArAutoAccountingSegmentHome;
import com.ejb.ar.LocalArCustomer;
import com.ejb.ar.LocalArCustomerBalance;
import com.ejb.ar.LocalArCustomerBalanceHome;
import com.ejb.ar.LocalArCustomerHome;
import com.ejb.ar.LocalArDistributionRecord;
import com.ejb.ar.LocalArDistributionRecordHome;
import com.ejb.ar.LocalArInvoice;
import com.ejb.ar.LocalArInvoiceBatch;
import com.ejb.ar.LocalArInvoiceBatchHome;
import com.ejb.ar.LocalArInvoiceHome;
import com.ejb.ar.LocalArInvoiceLine;
import com.ejb.ar.LocalArInvoiceLineHome;
import com.ejb.ar.LocalArInvoiceLineItem;
import com.ejb.ar.LocalArInvoiceLineItemHome;
import com.ejb.ar.LocalArInvoicePaymentSchedule;
import com.ejb.ar.LocalArInvoicePaymentScheduleHome;
import com.ejb.ar.LocalArJobOrder;
import com.ejb.ar.LocalArJobOrderHome;
import com.ejb.ar.LocalArPdc;
import com.ejb.ar.LocalArPdcHome;
import com.ejb.ar.LocalArReceipt;
import com.ejb.ar.LocalArReceiptBatch;
import com.ejb.ar.LocalArReceiptBatchHome;
import com.ejb.ar.LocalArReceiptHome;
import com.ejb.ar.LocalArSalesOrder;
import com.ejb.ar.LocalArSalesOrderHome;
import com.ejb.ar.LocalArSalesOrderInvoiceLine;
import com.ejb.ar.LocalArSalesOrderInvoiceLineHome;
import com.ejb.ar.LocalArSalesOrderLine;
import com.ejb.ar.LocalArSalesOrderLineHome;
import com.ejb.ar.LocalArSalesperson;
import com.ejb.ar.LocalArSalespersonHome;
import com.ejb.ar.LocalArStandardMemoLine;
import com.ejb.ar.LocalArStandardMemoLineHome;
import com.ejb.ar.LocalArTaxCode;
import com.ejb.ar.LocalArTaxCodeHome;
import com.ejb.ar.LocalArWithholdingTaxCode;
import com.ejb.ar.LocalArWithholdingTaxCodeHome;
import com.ejb.cm.LocalCmAdjustmentHome;
import com.ejb.cm.LocalCmFundTransferHome;
import com.ejb.exception.AdPRFCoaGlVarianceAccountNotFoundException;
import com.ejb.exception.ArINVAmountExceedsCreditLimitException;
import com.ejb.exception.GlJREffectiveDateNoPeriodExistException;
import com.ejb.exception.GlJREffectiveDatePeriodClosedException;
import com.ejb.exception.GlobalBranchAccountNumberInvalidException;
import com.ejb.exception.GlobalConversionDateNotExistException;
import com.ejb.exception.GlobalDocumentNumberNotUniqueException;
import com.ejb.exception.GlobalExpiryDateNotFoundException;
import com.ejb.exception.GlobalInvItemLocationNotFoundException;
import com.ejb.exception.GlobalInventoryDateException;
import com.ejb.exception.GlobalJournalNotBalanceException;
import com.ejb.exception.GlobalMiscInfoIsRequiredException;
import com.ejb.exception.GlobalNoApprovalApproverFoundException;
import com.ejb.exception.GlobalNoApprovalRequesterFoundException;
import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.exception.GlobalPaymentTermInvalidException;
import com.ejb.exception.GlobalRecordAlreadyAssignedException;
import com.ejb.exception.GlobalRecordAlreadyDeletedException;
import com.ejb.exception.GlobalTransactionAlreadyApprovedException;
import com.ejb.exception.GlobalTransactionAlreadyPendingException;
import com.ejb.exception.GlobalTransactionAlreadyPostedException;
import com.ejb.exception.GlobalTransactionAlreadyVoidException;
import com.ejb.exception.GlobalTransactionAlreadyVoidPostedException;
import com.ejb.genfld.LocalGenField;
import com.ejb.gl.LocalAdDocumentSequenceAssignment;
import com.ejb.gl.LocalAdDocumentSequenceAssignmentHome;
import com.ejb.gl.LocalGlAccountingCalendarValue;
import com.ejb.gl.LocalGlAccountingCalendarValueHome;
import com.ejb.gl.LocalGlChartOfAccount;
import com.ejb.gl.LocalGlChartOfAccountBalance;
import com.ejb.gl.LocalGlChartOfAccountBalanceHome;
import com.ejb.gl.LocalGlChartOfAccountHome;
import com.ejb.gl.LocalGlForexLedger;
import com.ejb.gl.LocalGlForexLedgerHome;
import com.ejb.gl.LocalGlFunctionalCurrency;
import com.ejb.gl.LocalGlFunctionalCurrencyHome;
import com.ejb.gl.LocalGlFunctionalCurrencyRate;
import com.ejb.gl.LocalGlFunctionalCurrencyRateHome;
import com.ejb.gl.LocalGlInvestorAccountBalance;
import com.ejb.gl.LocalGlInvestorAccountBalanceHome;
import com.ejb.gl.LocalGlJournal;
import com.ejb.gl.LocalGlJournalBatch;
import com.ejb.gl.LocalGlJournalBatchHome;
import com.ejb.gl.LocalGlJournalCategory;
import com.ejb.gl.LocalGlJournalCategoryHome;
import com.ejb.gl.LocalGlJournalHome;
import com.ejb.gl.LocalGlJournalLine;
import com.ejb.gl.LocalGlJournalLineHome;
import com.ejb.gl.LocalGlJournalSource;
import com.ejb.gl.LocalGlJournalSourceHome;
import com.ejb.gl.LocalGlRecurringJournalHome;
import com.ejb.gl.LocalGlSetOfBook;
import com.ejb.gl.LocalGlSetOfBookHome;
import com.ejb.gl.LocalGlSuspenseAccount;
import com.ejb.gl.LocalGlSuspenseAccountHome;
import com.ejb.inv.LocalInvAdjustment;
import com.ejb.inv.LocalInvAdjustmentHome;
import com.ejb.inv.LocalInvAdjustmentLine;
import com.ejb.inv.LocalInvAdjustmentLineHome;
import com.ejb.inv.LocalInvBillOfMaterial;
import com.ejb.inv.LocalInvBranchStockTransfer;
import com.ejb.inv.LocalInvBranchStockTransferHome;
import com.ejb.inv.LocalInvBranchStockTransferLine;
import com.ejb.inv.LocalInvBranchStockTransferLineHome;
import com.ejb.inv.LocalInvBuildUnbuildAssemblyHome;
import com.ejb.inv.LocalInvCosting;
import com.ejb.inv.LocalInvCostingHome;
import com.ejb.inv.LocalInvDistributionRecord;
import com.ejb.inv.LocalInvDistributionRecordHome;
import com.ejb.inv.LocalInvItem;
import com.ejb.inv.LocalInvItemHome;
import com.ejb.inv.LocalInvItemLocation;
import com.ejb.inv.LocalInvItemLocationHome;
import com.ejb.inv.LocalInvUnitOfMeasure;
import com.ejb.inv.LocalInvUnitOfMeasureConversion;
import com.ejb.inv.LocalInvUnitOfMeasureConversionHome;
import com.ejb.inv.LocalInvUnitOfMeasureHome;
import com.ejb.ap.LocalApPurchaseOrderLine;
import com.ejb.ap.LocalApVoucherPaymentScheduleHome;
import com.ejb.ap.LocalApVoucherPaymentSchedule;
import com.ejb.inv.LocalInvStockTransferHome;
import com.util.AbstractSessionBean;
import com.util.ApModVoucherPaymentScheduleDetails;
import com.util.ArInvoiceDetails;
import com.util.ArModInvoiceDetails;
import com.util.ArModInvoiceLineDetails;
import com.util.ArModInvoiceLineItemDetails;
import com.util.ArModSalesOrderLineDetails;
import com.util.ArReceiptDetails;
import com.util.Debug;
import com.util.EJBCommon;
import com.util.EJBHomeFactory;
import com.util.GlInvestorAccountBalanceDetails;
import com.util.InvModBranchStockTransferDetails;
import com.ejb.txn.ArStandardMemoLineControllerBean;
import com.ejb.txn.ArStandardMemoLineControllerHome;



















import org.joda.time.DateTime;
import org.joda.time.Days;
import org.joda.time.LocalDate;

/**
 * @ejb:bean name="AdMainNotificationControllerEJB"
 *           display-name="Used for displaying announcement and notification in the main page"
 *           type="Stateless"
 *           view-type="remote"
 *           jndi-name="ejb/AdMainNotificationControllerEJB"
 *
 * @ejb:interface remote-class="com.ejb.txn.AdMainNotificationController"
 *                extends="javax.ejb.EJBObject"
 *
 * @ejb:home remote-class="com.ejb.txn.AdMainNotificationControllerHome"
 *           extends="javax.ejb.EJBHome"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="gluser"
 *                        role-link="gluserlink"
 *
 * @ejb:permission role-name="gluser"
 *
*/

public class AdMainNotificationControllerBean extends AbstractSessionBean{


	/**
     * @ejb:interface-method view-type="remote"
     **/
     public int generateLoan(String USER_NM, Integer AD_BRNCH, Integer AD_CMPNY)
                 throws GlobalNoRecordFoundException,GlJREffectiveDatePeriodClosedException{

        Debug.print("AdMainNotificationControllerBean generateLoan");

        LocalArInvoiceBatchHome arInvoiceBatchHome = null;
        LocalApVoucherHome apVoucherHome = null;

        ArStandardMemoLineControllerHome arStandardMemoLineControllerHome = null;

        ArInvoiceEntryControllerHome homeINV = null;
        ArInvoiceEntryController ejbINV = null;

        LocalArCustomerHome arCustomerHome = null;


        int noOfSubmitted = 0;
        // Initialize EJB Home

        try {


            arInvoiceBatchHome =  (LocalArInvoiceBatchHome)EJBHomeFactory.
                    lookUpLocalHome(LocalArInvoiceBatchHome.JNDI_NAME, LocalArInvoiceBatchHome.class);
            apVoucherHome =  (LocalApVoucherHome)EJBHomeFactory.
            		lookUpLocalHome(LocalApVoucherHome.JNDI_NAME, LocalApVoucherHome.class);
            arCustomerHome =  (LocalArCustomerHome)EJBHomeFactory.
            		lookUpLocalHome(LocalArCustomerHome.JNDI_NAME, LocalArCustomerHome.class);


            homeINV = (ArInvoiceEntryControllerHome)com.util.EJBHomeFactory.
                    lookUpHome("ejb/ArInvoiceEntryControllerEJB", ArInvoiceEntryControllerHome.class);

        } catch (NamingException ex) {

            throw new EJBException(ex.getMessage());

        }



            try {
				ejbINV = homeINV.create();
			} catch (RemoteException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (CreateException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}




        try {


          Collection vouLoans = apVoucherHome.findVouForLoanGeneration(AD_BRNCH, AD_CMPNY);

          Iterator x = vouLoans.iterator();

          int loansGenerated = 1;

          while(x.hasNext()){

        	  	LocalApVoucher apVoucher = (LocalApVoucher)x.next();

        	  	Collection vLis =apVoucher.getApVoucherLineItems();

        	  	Iterator y = vLis.iterator();

        	  	ArrayList iliList = new ArrayList();

        	  	ArModInvoiceLineItemDetails mdetails = new ArModInvoiceLineItemDetails();

        	  	String II_NM = "";
        	  	String LOC_NM = "";
        	  	String UOM_NM = "";

        	  	int ctr = 1;

        	  	while(y.hasNext()){
        	  		LocalApVoucherLineItem apVoucherLineItem = (LocalApVoucherLineItem)y.next();

        	  		mdetails.setIliLine((short)ctr);
                    mdetails.setIliIiName(apVoucherLineItem.getInvItemLocation().getInvItem().getIiName());
                    mdetails.setIliLocName(apVoucherLineItem.getInvItemLocation().getInvLocation().getLocName());
                    mdetails.setIliQuantity(apVoucherLineItem.getVliQuantity());
                    mdetails.setIliUomName(apVoucherLineItem.getInvUnitOfMeasure().getUomName());
                    mdetails.setIliUnitPrice(apVoucher.getVouAmountDue());
                    mdetails.setIliAmount(apVoucher.getVouAmountDue());
                    mdetails.setIliEnableAutoBuild((byte)0);
                    mdetails.setIliDiscount1(0d);
                    mdetails.setIliDiscount2(0d);
                    mdetails.setIliDiscount3(0d);
                    mdetails.setIliDiscount4(0d);
                    mdetails.setIliTotalDiscount(0d);
                    //mdetails.setIliMisc(arILIList.getMisc());

                    ctr++;
                    iliList.add(mdetails);


        	  	}


                 LocalArCustomer arCustomer =
     	  			 	arCustomerHome.findByCstBySupplierCode(apVoucher.getApSupplier().getSplCode(), AD_CMPNY);

 	             String customerCode = arCustomer.getCstCustomerCode();
 	             String referenceNumber = apVoucher.getVouDocumentNumber();



                 ArModInvoiceDetails details = new ArModInvoiceDetails();

                 details.setInvCode(null);
                 details.setInvType("ITEMS");
                 details.setInvDate(apVoucher.getVouDate());
                 details.setInvNumber(null);
                 details.setInvReferenceNumber(referenceNumber);
                 details.setInvVoid(EJBCommon.FALSE);
                 details.setInvDescription("SYSTEM GENERATED LOAN FOR VOUCHER: "+ referenceNumber);
                 details.setInvBillToAddress("");
                 details.setInvBillToContact("");
                 details.setInvBillToAltContact("");
                 details.setInvBillToPhone("");
                 details.setInvBillingHeader("");
                 details.setInvBillingFooter("");
                 details.setInvBillingHeader2("");
                 details.setInvBillingFooter2("");
                 details.setInvBillingHeader3(null);
                 details.setInvBillingFooter3(null);
                 details.setInvBillingSignatory("");
                 details.setInvSignatoryTitle("");
                 details.setInvShipToAddress("");
                 details.setInvShipToContact("");
                 details.setInvShipToAltContact("");
                 details.setInvShipToPhone("");
                 details.setInvLvFreight("");
                 details.setInvShipDate(EJBCommon.convertStringToSQLDate(null));
                 details.setInvConversionDate(EJBCommon.convertStringToSQLDate(null));
                 details.setInvConversionRate(1);
                 details.setInvDebitMemo((byte)0);
                 details.setInvSubjectToCommission((byte)0);
                 details.setInvClientPO("");
                 details.setInvEffectivityDate(apVoucher.getVouDate());
                 details.setInvRecieveDate(EJBCommon.convertStringToSQLDate(""));
                 details.setInvCreatedBy(USER_NM);
                 details.setInvDateCreated(new java.util.Date());
                 details.setInvLastModifiedBy(USER_NM);
                 details.setInvDateLastModified(new java.util.Date());

                 details.setInvDisableInterest(EJBCommon.TRUE);
                 details.setInvInterest(EJBCommon.FALSE);
                 details.setInvInterestReferenceNumber(null);
                 details.setInvInterestAmount(0d);
                 details.setInvInterestCreatedBy(null);
                 details.setInvInterestDateCreated(null);

                 //assign batch name , if not existed, create one
                 System.out.println("genreated invoice: " );
                 Integer INV_CODE = ejbINV.saveArInvIliEntry(details, apVoucher.getAdPaymentTerm2().getPytName(), "NONE", "NONE", "PHP", customerCode, null, iliList, true /*set "false" if posted*/, null, "", AD_BRNCH, AD_CMPNY);

                 loansGenerated++;

                 apVoucher.setVouLoanGenerated(EJBCommon.TRUE);

          }

          return loansGenerated;

        } catch(GlJREffectiveDatePeriodClosedException gx){
            getSessionContext().setRollbackOnly();
            throw new GlJREffectiveDatePeriodClosedException();


        } catch (Exception ex) {
            getSessionContext().setRollbackOnly();
          Debug.printStackTrace(ex);
           throw new EJBException(ex.getMessage());

        }

     }


     /**
      * @ejb:interface-method view-type="remote"
      **/
      public int executeSalesOrderTransactionStatus(String SO_TRNSCTN_STTS, Integer AD_BRNCH, Integer AD_CMPNY) {

         Debug.print("AdMainNotificationControllerBean executeSalesOrderTransactionStatus");


         LocalArSalesOrderHome arSalesOrderHome = null;
      


         // Initialize EJB Home

         try {

      
         	
            arSalesOrderHome = (LocalArSalesOrderHome)EJBHomeFactory.
                    lookUpLocalHome(LocalArSalesOrderHome.JNDI_NAME, LocalArSalesOrderHome.class);
         
           

         } catch (NamingException ex) {

             throw new EJBException(ex.getMessage());

         }

         try {

         	
         	
         	
         	return arSalesOrderHome.findDraftSoByTransactionStatus(SO_TRNSCTN_STTS, AD_BRNCH, AD_CMPNY).size();

           
            

         } catch (Exception ex) {

         	 Debug.printStackTrace(ex);
            throw new EJBException(ex.getMessage());

         }

     
      }


   /**
   * @ejb:interface-method view-type="remote"
   **/
   public int executeGlRjbUserNotification(Integer USR_CODE, Integer AD_BRNCH, Integer AD_CMPNY) {

      Debug.print("AdMainNotificationControllerBean executeGLRjbUserNotification");

      LocalAdUserHome adUserHome = null;
      LocalGlRecurringJournalHome glRecurringJournalHome = null;

      // Initialize EJB Home

      try {

          adUserHome = (LocalAdUserHome)EJBHomeFactory.
              lookUpLocalHome(LocalAdUserHome.JNDI_NAME, LocalAdUserHome.class);
          glRecurringJournalHome = (LocalGlRecurringJournalHome)EJBHomeFactory.
              lookUpLocalHome(LocalGlRecurringJournalHome.JNDI_NAME, LocalGlRecurringJournalHome.class);

      } catch (NamingException ex) {

          throw new EJBException(ex.getMessage());

      }

      try {

         LocalAdUser adUser = adUserHome.findByPrimaryKey(USR_CODE);

         Collection glRecurringJournals = glRecurringJournalHome.findRjToGenerateByAdUsrNameAndDateAndBrCode(adUser.getUsrName(), EJBCommon.getGcCurrentDateWoTime().getTime(), AD_BRNCH, AD_CMPNY);

         return glRecurringJournals.size();


      } catch (FinderException ex) {

         return 0;

      } catch (Exception ex) {

      	 Debug.printStackTrace(ex);
         throw new EJBException(ex.getMessage());

      }

   }



   /**
    * @ejb:interface-method view-type="remote"
    * @jboss:method-attributes read-only="true"
    **/
   public byte getAdPrfEnableArInvoiceInterestGeneration(Integer AD_CMPNY) {

       Debug.print("AdMainNotificationControllerBean getAdPrfEnableArInvoiceInterestGeneration");

       LocalAdPreferenceHome adPreferenceHome = null;


       // Initialize EJB Home

       try {

           adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
           lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);

       } catch (NamingException ex) {

           throw new EJBException(ex.getMessage());

       }


       try {

           LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);

           return adPreference.getPrfEnableArInvoiceInterestGeneration();

       } catch (Exception ex) {

           Debug.printStackTrace(ex);
           throw new EJBException(ex.getMessage());

       }

   }


   /**
   * @ejb:interface-method view-type="remote"
   **/
   public int executeGlJrForReversal(Integer AD_BRNCH, Integer AD_CMPNY) {

      Debug.print("AdMainNotificationControllerBean executeGlJrForReversal");

      LocalGlJournalHome glJournalHome = null;

      // Initialize EJB Home

      try {

          glJournalHome = (LocalGlJournalHome)EJBHomeFactory.
              lookUpLocalHome(LocalGlJournalHome.JNDI_NAME, LocalGlJournalHome.class);

      } catch (NamingException ex) {

          throw new EJBException(ex.getMessage());

      }

	  try {

	      Collection glJournals = glJournalHome.findReversibleJrByJrReversalDateAndBrCode(EJBCommon.getGcCurrentDateWoTime().getTime(), AD_BRNCH, AD_CMPNY);

	      return glJournals.size();

	  } catch (Exception ex) {

	      throw new EJBException(ex.getMessage());

	  }

    }



   /**
   * @ejb:interface-method view-type="remote"
   **/
   public int executeArSalesOrdersForProcessing(Integer AD_BRNCH, Integer AD_CMPNY) {

      Debug.print("AdMainNotificationControllerBean executeArSalesOrdersForProcessing");

      LocalArSalesOrderHome arSalesOrderHome = null;

      // Initialize EJB Home

      try {

    	  arSalesOrderHome = (LocalArSalesOrderHome)EJBHomeFactory.
              lookUpLocalHome(LocalArSalesOrderHome.JNDI_NAME, LocalArSalesOrderHome.class);

      } catch (NamingException ex) {

          throw new EJBException(ex.getMessage());

      }

	  try {

		  int count=0;
	      Collection salesOrdersForProcessing = arSalesOrderHome.findPostedSoByBrCode(AD_BRNCH, AD_CMPNY);
			System.out.println("pass 1");
	      Iterator i=salesOrdersForProcessing.iterator();
	      while (i.hasNext()) {
	      	System.out.println("pass 2");
	    	  LocalArSalesOrder arSalesOrder = (LocalArSalesOrder)i.next();
	    	  if ((arSalesOrder.getSoApprovalStatus().equals("APPROVED") || arSalesOrder.getSoApprovalStatus().equals("N/A")) &&
	    			  arSalesOrder.getSoLock()==(byte)(0)){
	    			  	System.out.println("pass 3");
	    			   count++;
	    	   }
	      }

	      return count;

	  } catch (Exception ex) {

	      throw new EJBException(ex.getMessage());

	  }

    }
   
   /**
    * @ejb:interface-method view-type="remote"
    **/
    public int executeArJobOrdersForProcessing(Integer AD_BRNCH, Integer AD_CMPNY) {

	       Debug.print("AdMainNotificationControllerBean executeArJobOrdersForProcessing");
	
	       LocalArJobOrderHome arJobOrderHome = null;
	
	       // Initialize EJB Home
	
	       try {
	
	    	   arJobOrderHome = (LocalArJobOrderHome)EJBHomeFactory.
	               lookUpLocalHome(LocalArJobOrderHome.JNDI_NAME, LocalArJobOrderHome.class);
	
	       } catch (NamingException ex) {
	
	           throw new EJBException(ex.getMessage());
	
	       }

	 	  try {
	
	 		  int count=0;
	 	      Collection jobOrdersForProcessing = arJobOrderHome.findPostedJoByBrCode(AD_BRNCH, AD_CMPNY);
	
	 	      Iterator i=jobOrdersForProcessing.iterator();
	 	      while (i.hasNext()) {
	 	    	  LocalArJobOrder arJobOrder = (LocalArJobOrder)i.next();
	 	    	  if ((arJobOrder.getJoApprovalStatus().equals("APPROVED") || arJobOrder.getJoApprovalStatus().equals("N/A")) &&
	 	    			 arJobOrder.getJoLock()==(byte)(0)) count++;
	 	      }
	
	 	      return count;
	
	 	  } catch (Exception ex) {
	
	 	      throw new EJBException(ex.getMessage());
	
	 	  }

     }


    /**
     * @ejb:interface-method view-type="remote"
     **/
    public int executeApRvNumberOfVouchersToGenerate(Integer USR_CODE, Integer AD_BRNCH, Integer AD_CMPNY) {

    	Debug.print("AdMainNotificationControllerBean executeApRvNumberOfVouchersToGenerate");

        LocalAdUserHome adUserHome = null;
        LocalApRecurringVoucherHome apRecurringVoucherHome = null;

        // Initialize EJB Home

        try {

            adUserHome = (LocalAdUserHome)EJBHomeFactory.
               lookUpLocalHome(LocalAdUserHome.JNDI_NAME, LocalAdUserHome.class);
            apRecurringVoucherHome = (LocalApRecurringVoucherHome)EJBHomeFactory.
               lookUpLocalHome(LocalApRecurringVoucherHome.JNDI_NAME, LocalApRecurringVoucherHome.class);

        } catch (NamingException ex) {

           throw new EJBException(ex.getMessage());

        }

        try {

           LocalAdUser adUser = adUserHome.findByPrimaryKey(USR_CODE);

           Collection apRecurringVouchers = apRecurringVoucherHome.findRvToGenerateByAdUsrCodeAndDate(adUser.getUsrCode(), EJBCommon.getGcCurrentDateWoTime().getTime(), AD_BRNCH, AD_CMPNY);

           return apRecurringVouchers.size();


        } catch (FinderException ex) {

           return 0;

        } catch (Exception ex) {

      	   Debug.printStackTrace(ex);
           throw new EJBException(ex.getMessage());

        }

    }



    /**
     * @ejb:interface-method view-type="remote"
     **/
    public int executeApPrNumberOfPurchaseRequisitionsToGenerate(Integer USR_CODE, Integer AD_BRNCH, Integer AD_CMPNY) {

    	Debug.print("AdMainNotificationControllerBean executeApPrNumberOfPurchaseRequisitionsToGenerate");

        LocalAdUserHome adUserHome = null;
        LocalApPurchaseRequisitionHome apPurchaseRequisitionHome = null;

        // Initialize EJB Home

        try {

            adUserHome = (LocalAdUserHome)EJBHomeFactory.
               lookUpLocalHome(LocalAdUserHome.JNDI_NAME, LocalAdUserHome.class);
            apPurchaseRequisitionHome = (LocalApPurchaseRequisitionHome)EJBHomeFactory.
                    lookUpLocalHome(LocalApPurchaseRequisitionHome.JNDI_NAME, LocalApPurchaseRequisitionHome.class);

        } catch (NamingException ex) {

           throw new EJBException(ex.getMessage());

        }

        try {

           LocalAdUser adUser = adUserHome.findByPrimaryKey(USR_CODE);

           Collection apRecurringPurchaseRequisitions = apPurchaseRequisitionHome.findPrToGenerateByAdUsrCodeAndDate(adUser.getUsrCode(), EJBCommon.getGcCurrentDateWoTime().getTime(), AD_BRNCH, AD_CMPNY);

           return apRecurringPurchaseRequisitions.size();


        } catch (FinderException ex) {

           return 0;

        } catch (Exception ex) {

      	   Debug.printStackTrace(ex);
           throw new EJBException(ex.getMessage());

        }

    }



    /**
     * @ejb:interface-method view-type="remote"
     **/
    public int executeApPurchaseReqForCanvass(Integer AD_BRNCH, Integer AD_CMPNY) {

    	Debug.print("AdMainNotificationControllerBean executeApPurchaseReqForCanvass");

        LocalApPurchaseRequisitionHome apPurchaseRequisitionHome = null;

        // Initialize EJB Home

        try {

            apPurchaseRequisitionHome = (LocalApPurchaseRequisitionHome)EJBHomeFactory.
               lookUpLocalHome(LocalApPurchaseRequisitionHome.JNDI_NAME, LocalApPurchaseRequisitionHome.class);

        } catch (NamingException ex) {

           throw new EJBException(ex.getMessage());

        }

        try {

           Collection apPurchaseReqForCanvass = apPurchaseRequisitionHome.findPostedAndCanvassUnPostedPr(AD_CMPNY);
           System.out.println("apPurchaseReqForCanvass.size() = "+apPurchaseReqForCanvass.size());


           return apPurchaseReqForCanvass.size();


        } catch (FinderException ex) {

           return 0;

        } catch (Exception ex) {

      	   Debug.printStackTrace(ex);
           throw new EJBException(ex.getMessage());

        }

    }



    /**
     * @ejb:interface-method view-type="remote"
     **/
    public int executeApCanvassRejected(Integer AD_BRNCH, String PR_LST_MDFD_BY, Integer AD_CMPNY) {

    	Debug.print("AdMainNotificationControllerBean executeApCanvassRejected");

        LocalApPurchaseRequisitionHome apPurchaseRequisitionHome = null;

        // Initialize EJB Home

        try {

            apPurchaseRequisitionHome = (LocalApPurchaseRequisitionHome)EJBHomeFactory.
               lookUpLocalHome(LocalApPurchaseRequisitionHome.JNDI_NAME, LocalApPurchaseRequisitionHome.class);

        } catch (NamingException ex) {

           throw new EJBException(ex.getMessage());

        }

        try {

           Collection apCanvassRejected = apPurchaseRequisitionHome.findRejectedCanvassByBrCodeAndPrLastModifiedBy(PR_LST_MDFD_BY, AD_CMPNY);
           System.out.println("apCanvassRejected.size() = "+apCanvassRejected.size());


           return apCanvassRejected.size();


        } catch (FinderException ex) {

           return 0;

        } catch (Exception ex) {

      	   Debug.printStackTrace(ex);
           throw new EJBException(ex.getMessage());

        }

    }


    /**
     * @ejb:interface-method view-type="remote"
     **/
    public int executeApPurchaseOrderRejected(Integer AD_BRNCH, String PR_CRTD_BY, Integer AD_CMPNY) {

    	Debug.print("AdMainNotificationControllerBean executeApPurchaseOrderRejected");

        LocalApPurchaseOrderHome arPurchaseOrderHome = null;

        // Initialize EJB Home

        try {

        	arPurchaseOrderHome = (LocalApPurchaseOrderHome)EJBHomeFactory.
               lookUpLocalHome(LocalApPurchaseOrderHome.JNDI_NAME, LocalApPurchaseOrderHome.class);

        } catch (NamingException ex) {

           throw new EJBException(ex.getMessage());

        }

        try {

           Collection apPurchaseOrdersRejected = arPurchaseOrderHome.findRejectedPoByBrCodeAndPoCreatedBy(AD_BRNCH, PR_CRTD_BY, AD_CMPNY);
           System.out.println("apPurchaseOrdersRejected.size() = "+apPurchaseOrdersRejected.size());


           return apPurchaseOrdersRejected.size();


        } catch (FinderException ex) {

           return 0;

        } catch (Exception ex) {

      	   Debug.printStackTrace(ex);
           throw new EJBException(ex.getMessage());

        }

    }


    /**
     * @ejb:interface-method view-type="remote"
     **/
    public int executeApPurchaseOrderReceivingRejected(Integer AD_BRNCH, String PR_CRTD_BY, Integer AD_CMPNY) {

    	Debug.print("AdMainNotificationControllerBean executeApPurchaseOrderReceivingRejected");

        LocalApPurchaseOrderHome arPurchaseOrderHome = null;

        // Initialize EJB Home

        try {

        	arPurchaseOrderHome = (LocalApPurchaseOrderHome)EJBHomeFactory.
               lookUpLocalHome(LocalApPurchaseOrderHome.JNDI_NAME, LocalApPurchaseOrderHome.class);

        } catch (NamingException ex) {

           throw new EJBException(ex.getMessage());

        }

        try {

           Collection apReceivings = arPurchaseOrderHome.findRejectedRiByBrCodeAndPoCreatedBy(AD_BRNCH, PR_CRTD_BY, AD_CMPNY);
           System.out.println("apReceivings.size() = "+apReceivings.size());


           return apReceivings.size();


        } catch (FinderException ex) {

           return 0;

        } catch (Exception ex) {

      	   Debug.printStackTrace(ex);
           throw new EJBException(ex.getMessage());

        }

    }

    /**
     * @ejb:interface-method view-type="remote"
     **/
    public int executeApPurchaseReqRejected(Integer AD_BRNCH, String PR_CRTD_BY, Integer AD_CMPNY) {

    	Debug.print("AdMainNotificationControllerBean executeApPurchaseReqRejected");

        LocalApPurchaseRequisitionHome apPurchaseRequisitionHome = null;

        // Initialize EJB Home

        try {

            apPurchaseRequisitionHome = (LocalApPurchaseRequisitionHome)EJBHomeFactory.
               lookUpLocalHome(LocalApPurchaseRequisitionHome.JNDI_NAME, LocalApPurchaseRequisitionHome.class);

        } catch (NamingException ex) {

           throw new EJBException(ex.getMessage());

        }

        try {

           Collection apPurchaseRequisitionsRejected = apPurchaseRequisitionHome.findRejectedPrByBrCodeAndPrCreatedBy(AD_BRNCH, PR_CRTD_BY, AD_CMPNY);
           System.out.println("apPurchaseRequisitionsRejected.size() = "+apPurchaseRequisitionsRejected.size());


           return apPurchaseRequisitionsRejected.size();


        } catch (FinderException ex) {

           return 0;

        } catch (Exception ex) {

      	   Debug.printStackTrace(ex);
           throw new EJBException(ex.getMessage());

        }

    }


    /**
     * @ejb:interface-method view-type="remote"
     **/
    public int executeApChecksForRelease(Integer AD_BRNCH, String PR_CRTD_BY, Integer AD_CMPNY) {

    	Debug.print("AdMainNotificationControllerBean executeApChecksForRelease");

        LocalApCheckHome apCheckHome = null;

        // Initialize EJB Home

        try {

        	apCheckHome = (LocalApCheckHome)EJBHomeFactory.
               lookUpLocalHome(LocalApCheckHome.JNDI_NAME, LocalApCheckHome.class);

        } catch (NamingException ex) {

           throw new EJBException(ex.getMessage());

        }

        try {

           Collection apChecks = apCheckHome.findChecksForReleaseByBrCode(AD_BRNCH, AD_CMPNY);
           System.out.println("apChecks.size() = "+apChecks.size());


           return apChecks.size();


        } catch (FinderException ex) {

           return 0;

        } catch (Exception ex) {

      	   Debug.printStackTrace(ex);
           throw new EJBException(ex.getMessage());

        }

    }



    /**
     * @ejb:interface-method view-type="remote"
     **/
    public int executeApPaymentRequestDraft(Integer AD_BRNCH, Integer AD_CMPNY) {

    	Debug.print("AdMainNotificationControllerBean executeApPaymentRequestRejected");

        LocalApVoucherHome apVoucherHome = null;

        // Initialize EJB Home

        try {

        	apVoucherHome = (LocalApVoucherHome)EJBHomeFactory.
               lookUpLocalHome(LocalApVoucherHome.JNDI_NAME, LocalApVoucherHome.class);

        } catch (NamingException ex) {

           throw new EJBException(ex.getMessage());

        }

        try {

           Collection apVouchers = apVoucherHome.findDraftRequestVouByBrCode(AD_BRNCH, AD_CMPNY);


           return apVouchers.size();


        } catch (FinderException ex) {

           return 0;

        } catch (Exception ex) {

      	   Debug.printStackTrace(ex);
           throw new EJBException(ex.getMessage());

        }

    }

    /**
     * @ejb:interface-method view-type="remote"
     **/
    public int executeApPaymentRequestGeneration(Integer AD_BRNCH, Integer AD_CMPNY) {

    	Debug.print("AdMainNotificationControllerBean executeApPaymentRequestRejected");

        LocalApVoucherHome apVoucherHome = null;

        // Initialize EJB Home

        try {

        	apVoucherHome = (LocalApVoucherHome)EJBHomeFactory.
               lookUpLocalHome(LocalApVoucherHome.JNDI_NAME, LocalApVoucherHome.class);

        } catch (NamingException ex) {

           throw new EJBException(ex.getMessage());

        }

        try {

           Collection apVouchers = apVoucherHome.findForGenerationRequestVouByBrCode(AD_BRNCH, AD_CMPNY);


           return apVouchers.size();


        } catch (FinderException ex) {

           return 0;

        } catch (Exception ex) {

      	   Debug.printStackTrace(ex);
           throw new EJBException(ex.getMessage());

        }

    }

    /**
     * @ejb:interface-method view-type="remote"
     **/
    public int executeApPaymentRequestProcessing(Integer AD_BRNCH, Integer AD_CMPNY) {

    	Debug.print("AdMainNotificationControllerBean executeApPaymentRequestRejected");

        LocalApVoucherHome apVoucherHome = null;

        // Initialize EJB Home

        try {

        	apVoucherHome = (LocalApVoucherHome)EJBHomeFactory.
               lookUpLocalHome(LocalApVoucherHome.JNDI_NAME, LocalApVoucherHome.class);

        } catch (NamingException ex) {

           throw new EJBException(ex.getMessage());

        }

        try {

           Collection apVouchers = apVoucherHome.findForProcessingRequestVouByBrCode(AD_BRNCH, AD_CMPNY);


           return apVouchers.size();


        } catch (FinderException ex) {

           return 0;

        } catch (Exception ex) {

      	   Debug.printStackTrace(ex);
           throw new EJBException(ex.getMessage());

        }

    }

    /**
     * @ejb:interface-method view-type="remote"
     **/
    public int executeApPaymentsForProcessing(Integer AD_BRNCH, Integer AD_CMPNY) {

    	Debug.print("AdMainNotificationControllerBean executeApPaymentsForProcessing");

		LocalApVoucherPaymentScheduleHome apVoucherPaymentScheduleHome = null;
		LocalAdDiscountHome adDiscountHome = null;
		LocalAdPreferenceHome adPreferenceHome = null;
		LocalApPurchaseOrderHome apPurchaseOrderHome = null;

		ArrayList list = new ArrayList();
		int count=0;

		// Initialize EJB Home

		try {

		apVoucherPaymentScheduleHome = (LocalApVoucherPaymentScheduleHome)EJBHomeFactory.
		    lookUpLocalHome(LocalApVoucherPaymentScheduleHome.JNDI_NAME, LocalApVoucherPaymentScheduleHome.class);
		adDiscountHome = (LocalAdDiscountHome)EJBHomeFactory.
		    lookUpLocalHome(LocalAdDiscountHome.JNDI_NAME, LocalAdDiscountHome.class);
		adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
		    lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);
		apPurchaseOrderHome = (LocalApPurchaseOrderHome)EJBHomeFactory.
			lookUpLocalHome(LocalApPurchaseOrderHome.JNDI_NAME, LocalApPurchaseOrderHome.class);

		} catch (NamingException ex) {

		throw new EJBException(ex.getMessage());

		}

		try {

			Collection apVoucherPaymentSchedules =
			    apVoucherPaymentScheduleHome.findOpenVpsByVpsLock(EJBCommon.FALSE, AD_BRNCH, AD_CMPNY);


			Iterator i = apVoucherPaymentSchedules.iterator();

			while (i.hasNext()) {

				LocalApVoucherPaymentSchedule apVoucherPaymentSchedule =
				   (LocalApVoucherPaymentSchedule)i.next();

			    // verification if vps is already closed
				if (apVoucherPaymentSchedule.getVpsAmountDue() == apVoucherPaymentSchedule.getVpsAmountPaid()) continue;


				count++;

			}
			return count;

		}
		catch (Exception e) {
			// TODO: handle exception
			return 0;
		}
    }

    /**
     * @ejb:interface-method view-type="remote"
     **/
    public int executeApPaymentRequestRejected(Integer AD_BRNCH, String VOU_CRTD_BY, Integer AD_CMPNY) {

    	Debug.print("AdMainNotificationControllerBean executeApPaymentRequestRejected");

        LocalApVoucherHome apVoucherHome = null;

        // Initialize EJB Home

        try {

        	apVoucherHome = (LocalApVoucherHome)EJBHomeFactory.
               lookUpLocalHome(LocalApVoucherHome.JNDI_NAME, LocalApVoucherHome.class);

        } catch (NamingException ex) {

           throw new EJBException(ex.getMessage());

        }

        try {

           Collection apVouchers = apVoucherHome.findByVouRejectedCPRByByBrCodeAndVouCreatedBy(AD_BRNCH, VOU_CRTD_BY, AD_CMPNY);


           return apVouchers.size();


        } catch (FinderException ex) {

           return 0;

        } catch (Exception ex) {

      	   Debug.printStackTrace(ex);
           throw new EJBException(ex.getMessage());

        }

    }




    /**
     * @ejb:interface-method view-type="remote"
     **/
    public int executeApVoucherForProcessing(Integer AD_BRNCH, Integer AD_CMPNY) {

    	Debug.print("AdMainNotificationControllerBean executeApPurchaseOrderProcessing");

        LocalApPurchaseOrderLineHome apPurchaseOrderLineHome = null;

        // Initialize EJB Home

        try {

        	apPurchaseOrderLineHome = (LocalApPurchaseOrderLineHome)EJBHomeFactory.
               lookUpLocalHome(LocalApPurchaseOrderLineHome.JNDI_NAME, LocalApPurchaseOrderLineHome.class);

        } catch (NamingException ex) {

           throw new EJBException(ex.getMessage());

        }

        try {

           Collection apPurchaseOrderLines = apPurchaseOrderLineHome.findRrForProcessing(AD_BRNCH, AD_CMPNY);

           Iterator i = apPurchaseOrderLines.iterator();

           java.util.HashMap hm = new java.util.HashMap();
           while (i.hasNext()) {

        	   LocalApPurchaseOrderLine apPurchaseOrderLine = (LocalApPurchaseOrderLine)i.next();
        	   hm.put(apPurchaseOrderLine.getApPurchaseOrder().getPoCode(), apPurchaseOrderLine.getApPurchaseOrder().getPoCode());
           }

           return hm.size();


        } catch (FinderException ex) {

           return 0;

        } catch (Exception ex) {

      	   Debug.printStackTrace(ex);
           throw new EJBException(ex.getMessage());

        }

    }


    /**
     * @ejb:interface-method view-type="remote"
     **/
    public int executeApChecksRejected(Integer AD_BRNCH, String PO_CRTD_BY, Integer AD_CMPNY) {

    	Debug.print("AdMainNotificationControllerBean executeApChecksRejected");

        LocalApCheckHome apCheckHome = null;

        // Initialize EJB Home

        try {

        	apCheckHome = (LocalApCheckHome)EJBHomeFactory.
               lookUpLocalHome(LocalApCheckHome.JNDI_NAME, LocalApCheckHome.class);

        } catch (NamingException ex) {

           throw new EJBException(ex.getMessage());

        }

        try {


           Collection apChecksRejected = apCheckHome.findRejectedChkByBrCodeAndChkCreatedBy(AD_BRNCH, PO_CRTD_BY, AD_CMPNY);
           System.out.println("apChecksRejected.size() = "+apChecksRejected.size());

           return apChecksRejected.size();


        } catch (FinderException ex) {

           return 0;

        } catch (Exception ex) {

      	   Debug.printStackTrace(ex);
           throw new EJBException(ex.getMessage());

        }

    }


    /**
     * @ejb:interface-method view-type="remote"
     **/
    public int executeApDirectChecksRejected(Integer AD_BRNCH, String PO_CRTD_BY, Integer AD_CMPNY) {

    	Debug.print("AdMainNotificationControllerBean executeApDirectChecksRejected");

        LocalApCheckHome apCheckHome = null;

        // Initialize EJB Home

        try {

        	apCheckHome = (LocalApCheckHome)EJBHomeFactory.
               lookUpLocalHome(LocalApCheckHome.JNDI_NAME, LocalApCheckHome.class);

        } catch (NamingException ex) {

           throw new EJBException(ex.getMessage());

        }

        try {


           Collection apDirectChecksRejected = apCheckHome.findRejectedDirectChkByBrCodeAndChkCreatedBy(AD_BRNCH, PO_CRTD_BY, AD_CMPNY);
           System.out.println("apDirectChecksRejected.size() = "+apDirectChecksRejected.size());

           return apDirectChecksRejected.size();


        } catch (FinderException ex) {

           return 0;

        } catch (Exception ex) {

      	   Debug.printStackTrace(ex);
           throw new EJBException(ex.getMessage());

        }

    }

    /**
     * @ejb:interface-method view-type="remote"
     **/
    public int executeApVoucherRejected(Integer AD_BRNCH, String PO_CRTD_BY, Integer AD_CMPNY) {

    	Debug.print("AdMainNotificationControllerBean executeApVoucherRejected");

        LocalApVoucherHome apVoucherHome = null;

        // Initialize EJB Home

        try {

        	apVoucherHome = (LocalApVoucherHome)EJBHomeFactory.
               lookUpLocalHome(LocalApVoucherHome.JNDI_NAME, LocalApVoucherHome.class);

        } catch (NamingException ex) {

           throw new EJBException(ex.getMessage());

        }

        try {


           Collection apVouchersRejected = apVoucherHome.findRejectedVouByBrCodeAndPoCreatedBy(AD_BRNCH, PO_CRTD_BY, AD_CMPNY);
           System.out.println("apVouchersRejected.size() = "+apVouchersRejected.size());

           return apVouchersRejected.size();


        } catch (FinderException ex) {

           return 0;

        } catch (Exception ex) {

      	   Debug.printStackTrace(ex);
           throw new EJBException(ex.getMessage());

        }

    }
    /**
     * @ejb:interface-method view-type="remote"
     **/
    public int executeApPurchaseOrdersForGeneration(Integer AD_BRNCH, Integer AD_CMPNY) {

    	Debug.print("AdMainNotificationControllerBean executeApPurchaseOrdersForGeneration");

        LocalApPurchaseRequisitionHome apPurchaseRequisitionHome = null;

        // Initialize EJB Home

        try {

            apPurchaseRequisitionHome = (LocalApPurchaseRequisitionHome)EJBHomeFactory.
               lookUpLocalHome(LocalApPurchaseRequisitionHome.JNDI_NAME, LocalApPurchaseRequisitionHome.class);

        } catch (NamingException ex) {

           throw new EJBException(ex.getMessage());

        }

        try {

           Collection apPurchaseRequisitions = apPurchaseRequisitionHome.findUnPostedGeneratedPr(AD_CMPNY);
           System.out.println("apPurchaseRequisitions.size() = "+apPurchaseRequisitions.size());


           return apPurchaseRequisitions.size();


        } catch (FinderException ex) {

           return 0;

        } catch (Exception ex) {

      	   Debug.printStackTrace(ex);
           throw new EJBException(ex.getMessage());

        }

    }


    /**
     * @ejb:interface-method view-type="remote"
     **/
    public int executeInvAdjustmentRequestToGenerate(Integer AD_BRNCH, Integer AD_CMPNY) {

    	Debug.print("AdMainNotificationControllerBean executeInvAdjustmentRequestToGenerate");

        LocalInvAdjustmentHome invAdjustmentHome = null;

        // Initialize EJB Home

        try {

        	invAdjustmentHome = (LocalInvAdjustmentHome)EJBHomeFactory.
               lookUpLocalHome(LocalInvAdjustmentHome.JNDI_NAME, LocalInvAdjustmentHome.class);

        } catch (NamingException ex) {

           throw new EJBException(ex.getMessage());

        }

        try {

           Collection invAdjustmentReqs = invAdjustmentHome.findAdjRequestToGenerateByAdCompanyAndBrCode(AD_BRNCH,AD_CMPNY);
           System.out.println("invAdjustmentReqs.size() = "+invAdjustmentReqs.size());


           return invAdjustmentReqs.size();


        } catch (FinderException ex) {

           return 0;

        } catch (Exception ex) {

      	   Debug.printStackTrace(ex);
           throw new EJBException(ex.getMessage());

        }

    }



    /**
     * @ejb:interface-method view-type="remote"
     **/
    public int executeApPurchaseOrdersForPrinting(Integer AD_BRNCH, Integer AD_CMPNY) {

    	Debug.print("AdMainNotificationControllerBean executeApPurchaseOrdersForPrinting");

        LocalApPurchaseOrderHome apPurchaseOrderHome = null;

        // Initialize EJB Home

        try {

        	apPurchaseOrderHome = (LocalApPurchaseOrderHome)EJBHomeFactory.
               lookUpLocalHome(LocalApPurchaseOrderHome.JNDI_NAME, LocalApPurchaseOrderHome.class);

        } catch (NamingException ex) {

           throw new EJBException(ex.getMessage());

        }

        try {

           Collection apPurchaseOrders = apPurchaseOrderHome.findPOforPrint(AD_BRNCH, AD_CMPNY);
           System.out.println("apPurchaseOrder.size() = "+apPurchaseOrders.size());


           return apPurchaseOrders.size();


        } catch (FinderException ex) {

           return 0;

        } catch (Exception ex) {

      	   Debug.printStackTrace(ex);
           throw new EJBException(ex.getMessage());

        }

    }


    /**
     * @ejb:interface-method view-type="remote"
     **/
    public int executeArCustomerRejected(Integer AD_BRNCH, String CST_CRTD_BY, Integer AD_CMPNY) {

    	Debug.print("AdMainNotificationControllerBean executeArCustomerRejected");


        LocalArCustomerHome arCustomerHome = null;

        // Initialize EJB Home

        try {

     	   arCustomerHome = (LocalArCustomerHome)EJBHomeFactory.
               lookUpLocalHome(LocalArCustomerHome.JNDI_NAME, LocalArCustomerHome.class);

        } catch (NamingException ex) {

           throw new EJBException(ex.getMessage());

        }

        try {

           System.out.println("Created by : " + CST_CRTD_BY);
           Collection arCustomers = arCustomerHome.findByCstRejectedByByBrCodeAndCstLastModifiedBy(AD_BRNCH, CST_CRTD_BY, AD_CMPNY);

           System.out.println("arCustomers.size()="+arCustomers.size());
           return arCustomers.size();


        } catch (FinderException ex) {

           return 0;

        } catch (Exception ex) {

      	   Debug.printStackTrace(ex);
           throw new EJBException(ex.getMessage());

        }

    }

    /**
     * @ejb:interface-method view-type="remote"
     **/
    public int executeArPdcNumberOfInvoicesToGenerate(Integer USR_CODE, Integer AD_BRNCH, Integer AD_CMPNY) {

    	Debug.print("AdMainNotificationControllerBean executeArPdcNumberOfInvoicesToGenerate");

        LocalArPdcHome arPdcHome = null;

        // Initialize EJB Home

        try {

            arPdcHome = (LocalArPdcHome)EJBHomeFactory.
               lookUpLocalHome(LocalArPdcHome.JNDI_NAME, LocalArPdcHome.class);

        } catch (NamingException ex) {

           throw new EJBException(ex.getMessage());

        }

        try {

        	Collection openPdcs = arPdcHome.findOpenPdcAll(AD_CMPNY);

        	Date current = EJBCommon.getGcCurrentDateWoTime().getTime();

        	Iterator i = openPdcs.iterator();


        	while(i.hasNext()) {

        		LocalArPdc arPdc = (LocalArPdc)i.next();

        		if (arPdc.getPdcMaturityDate().equals(current) || arPdc.getPdcMaturityDate().before(current)) {

        			arPdc.setPdcStatus("MATURED");

        		}

        	}

        } catch (FinderException ex) {

        }

        try {

           Collection arPdcs = arPdcHome.findPdcToGenerateByBrCode(AD_BRNCH, AD_CMPNY);

           return arPdcs.size();


        } catch (FinderException ex) {

           return 0;

        } catch (Exception ex) {

      	   Debug.printStackTrace(ex);
           throw new EJBException(ex.getMessage());

        }

    }

    /**
     * @ejb:interface-method view-type="remote"
     **/
     public int executeArInvoiceForPosting(String invoiceType, Integer AD_BRNCH, Integer AD_CMPNY) {

        Debug.print("AdMainNotificationControllerBean executeArInvoiceForPosting");

        LocalArInvoiceHome arInvoiceHome = null;
        LocalArInvoiceLineHome arInvoiceLineHome =null;
        LocalArInvoiceLineItemHome arInvoiceLineItemHome= null;

        // Initialize EJB Home

        try {

            arInvoiceHome = (LocalArInvoiceHome)EJBHomeFactory.
                lookUpLocalHome(LocalArInvoiceHome.JNDI_NAME, LocalArInvoiceHome.class);
            arInvoiceLineHome = (LocalArInvoiceLineHome)EJBHomeFactory.
                    lookUpLocalHome(LocalArInvoiceLineHome.JNDI_NAME, LocalArInvoiceLineHome.class);
            arInvoiceLineItemHome = (LocalArInvoiceLineItemHome)EJBHomeFactory.
                    lookUpLocalHome(LocalArInvoiceLineItemHome.JNDI_NAME, LocalArInvoiceLineItemHome.class);

        } catch (NamingException ex) {

            throw new EJBException(ex.getMessage());

        }

        try {

        	 Collection arInvoices = arInvoiceHome.findInvForPostingByBranch(AD_BRNCH, AD_CMPNY);

        	 int totalSize = 0;

   		  Iterator i = arInvoices.iterator();

   		  while (i.hasNext()) {

   		  	  LocalArInvoice arInvoice = (LocalArInvoice)i.next();
   		  	  String invType = "";


   		  	  //confirm if it is item or memo line
   		  	  Collection ar_inv_ln = arInvoiceLineHome.findInvoiceLineByInvCodeAndAdCompany(arInvoice.getInvCode(),AD_CMPNY);

   		  	  if(ar_inv_ln.size()>0){
   		  		invType = "MEMO LINES";
   		  	  }

   		  	  Collection ar_inv_ln_itm = arInvoiceLineItemHome.findByInvCode(arInvoice.getInvCode(), AD_CMPNY);

   		  	  if(ar_inv_ln_itm.size()>0){
   		  		invType = "ITEMS";
   		  	  }


   		  	  //search if invoice type have value.filter item and memo line

	  		 System.out.println("1-" + invType + " 2:" + invoiceType);
	  		 if(invType.equals(invoiceType)){
	  			totalSize++;
	  		 }




   		  }










        	 System.out.println("arInvoices.size()="+totalSize);

           return totalSize;

        } catch (Exception ex) {

        	 Debug.printStackTrace(ex);
           throw new EJBException(ex.getMessage());

        }

     }


     /**
      * @ejb:interface-method view-type="remote"
      **/
      public int executeArInvoiceCreditMemoForPosting(Integer AD_BRNCH, Integer AD_CMPNY) {

         Debug.print("AdMainNotificationControllerBean executeArInvoiceForPosting");

         LocalArInvoiceHome arInvoiceHome = null;

         // Initialize EJB Home

         try {

             arInvoiceHome = (LocalArInvoiceHome)EJBHomeFactory.
                 lookUpLocalHome(LocalArInvoiceHome.JNDI_NAME, LocalArInvoiceHome.class);

         } catch (NamingException ex) {

             throw new EJBException(ex.getMessage());

         }

         try {

         	 Collection arInvoices = arInvoiceHome.findInvCreditMemoForPostingByBranch(AD_BRNCH, AD_CMPNY);

         	 System.out.println("arInvoicesCreditMemo.size()="+arInvoices.size());

            return arInvoices.size();

         } catch (Exception ex) {

         	 Debug.printStackTrace(ex);
            throw new EJBException(ex.getMessage());

         }

      }



      /**
       * @ejb:interface-method view-type="remote"
       **/
       public int executeArReceiptForPosting(String RCT_TYP, Integer AD_BRNCH, Integer AD_CMPNY) {

          Debug.print("AdMainNotificationControllerBean executeArReceiptForPosting");

          LocalArReceiptHome arReceiptHome = null;

          // Initialize EJB Home

          try {

              arReceiptHome = (LocalArReceiptHome)EJBHomeFactory.
                  lookUpLocalHome(LocalArReceiptHome.JNDI_NAME, LocalArReceiptHome.class);

          } catch (NamingException ex) {

              throw new EJBException(ex.getMessage());

          }

          try {

          	 Collection arReceipts	 = arReceiptHome.findRctForPostingByBranch(RCT_TYP, AD_BRNCH, AD_CMPNY);

          	 System.out.println("arReceipts.size()="+arReceipts.size());

             return arReceipts.size();

          } catch (Exception ex) {

          	 Debug.printStackTrace(ex);
             throw new EJBException(ex.getMessage());

          }

       }











  /**
   * @ejb:interface-method view-type="remote"
   **/
   public int executeAdDocumentToApprove(String AQ_DCMNT, Integer USR_CODE, Integer AD_BRNCH, Integer AD_CMPNY) {

      Debug.print("AdMainNotificationControllerBean executeAdDocumentToApprove");

      LocalAdApprovalQueueHome adApprovalQueueHome = null;

      // Initialize EJB Home

      try {

          adApprovalQueueHome = (LocalAdApprovalQueueHome)EJBHomeFactory.
              lookUpLocalHome(LocalAdApprovalQueueHome.JNDI_NAME, LocalAdApprovalQueueHome.class);

      } catch (NamingException ex) {

          throw new EJBException(ex.getMessage());

      }

      try {

      	 Collection adApprovalQueues = adApprovalQueueHome.findByAqDocumentAndUsrCode(AQ_DCMNT, USR_CODE, AD_BRNCH, AD_CMPNY);

         return adApprovalQueues.size();

      } catch (Exception ex) {

      	 Debug.printStackTrace(ex);
         throw new EJBException(ex.getMessage());

      }

   }

    /**
   * @ejb:interface-method view-type="remote"
   **/
   public int executeInvStockTransferUnposted(Integer AD_CMPNY) {

       Debug.print("AdMainNotificationControllerBean executeInvStockTransferUnposted");
       LocalInvStockTransferHome invStockTransferHome = null;

       try {

          invStockTransferHome = (LocalInvStockTransferHome)EJBHomeFactory.
              lookUpLocalHome(LocalInvStockTransferHome.JNDI_NAME, LocalInvStockTransferHome.class);

      } catch (NamingException ex) {

          throw new EJBException(ex.getMessage());

      }
    try {

      	 Collection invStockTransfers = invStockTransferHome.findUnpostedSt(AD_CMPNY);

         return invStockTransfers.size();

      } catch (Exception ex) {

      	 Debug.printStackTrace(ex);
         throw new EJBException(ex.getMessage());

      }
   }







   /**
    * @ejb:interface-method view-type="remote"
    **/
    public int executeArOverDueInvoices(Integer AD_BRNCH, Integer AD_CMPNY) {

       Debug.print("AdMainNotificationControllerBean executeArOverDueInvoices");

       LocalArInvoicePaymentScheduleHome arInvoicePaymentScheduleHome = null;

       // Initialize EJB Home

       try {

    	   arInvoicePaymentScheduleHome = (LocalArInvoicePaymentScheduleHome)EJBHomeFactory.
               lookUpLocalHome(LocalArInvoicePaymentScheduleHome.JNDI_NAME, LocalArInvoicePaymentScheduleHome.class);

       } catch (NamingException ex) {

           throw new EJBException(ex.getMessage());

       }

       try {


      	 Collection overDueInvoices = arInvoicePaymentScheduleHome.findOverdueIpsByNextRunDate(EJBCommon.convertStringToSQLDate(EJBCommon.convertSQLDateToString(new Date()) ), AD_BRNCH, AD_CMPNY);
/*
    	   int count = 0;

    	 Collection overDueInvoices = arInvoicePaymentScheduleHome.findOverdueIpsByNextRunDate2(EJBCommon.convertStringToSQLDate(EJBCommon.convertSQLDateToString(new Date()) ), AD_BRNCH, AD_CMPNY);

    	 Iterator i = overDueInvoices.iterator();


    	 while(i.hasNext()) {

    		 LocalArInvoicePaymentSchedule arInvoicePaymentSchedule =(LocalArInvoicePaymentSchedule)i.next();


    		 if(arInvoicePaymentSchedule.getIpsLock()==EJBCommon.TRUE ) {
    			 continue;
    		 }

    		 if(arInvoicePaymentSchedule.getArInvoice().getInvInterest()==EJBCommon.TRUE ) {
    			 continue;
    		 }

    		 if(arInvoicePaymentSchedule.getArInvoice().getInvDisableInterest()==EJBCommon.TRUE ) {
    			 continue;
    		 }

    		 if(arInvoicePaymentSchedule.getArInvoice().getInvInterestNextRunDate()== null ) {
    			 continue;
    		 }

    		 count++;
    	 }


*/


       	 System.out.println("overDueInvoices.size()="+overDueInvoices.size());
       	 return overDueInvoices.size();

       } catch (Exception ex) {

       	 Debug.printStackTrace(ex);
          throw new EJBException(ex.getMessage());

       }

    }

    /**
     * @ejb:interface-method view-type="remote"
     **/
     public int executeArInvestorBonusAndInterest(Integer AD_BRNCH, Integer AD_CMPNY) {

    	  Debug.print("AdMainNotificationControllerBean executeArInvestorBonusInterest");

    	  LocalGlInvestorAccountBalanceHome glInvestorAccountBalanceHome = null;
          LocalGlSetOfBookHome glSetOfBookHome = null;
          LocalGlAccountingCalendarValueHome glAccountingCalendarValueHome = null;
           LocalArReceiptHome arReceiptHome = null;
            LocalApCheckHome apCheckHome = null;

          // Initialize EJB Home

          try {


          	glInvestorAccountBalanceHome = (LocalGlInvestorAccountBalanceHome)EJBHomeFactory.
                    lookUpLocalHome(LocalGlInvestorAccountBalanceHome.JNDI_NAME, LocalGlInvestorAccountBalanceHome.class);

          	glSetOfBookHome = (LocalGlSetOfBookHome)EJBHomeFactory.
                      lookUpLocalHome(LocalGlSetOfBookHome.JNDI_NAME, LocalGlSetOfBookHome.class);

          	glAccountingCalendarValueHome = (LocalGlAccountingCalendarValueHome)EJBHomeFactory.
                      lookUpLocalHome(LocalGlAccountingCalendarValueHome.JNDI_NAME, LocalGlAccountingCalendarValueHome.class);

                arReceiptHome =  (LocalArReceiptHome)EJBHomeFactory.
		        	   lookUpLocalHome(LocalArReceiptHome.JNDI_NAME, LocalArReceiptHome.class);

                apCheckHome =  (LocalApCheckHome)EJBHomeFactory.
		        	   lookUpLocalHome(LocalApCheckHome.JNDI_NAME, LocalApCheckHome.class);
          } catch (NamingException ex) {

              throw new EJBException(ex.getMessage());

          }

          try {

            LocalGlSetOfBook glJournalSetOfBook = null;
            Date dateNow = DateTime.now().withTimeAtStartOfDay().toDate();
             try {

                glJournalSetOfBook = glSetOfBookHome.findByDate(dateNow, AD_CMPNY);

             } catch (FinderException ex) {

                // throw new GlJREffectiveDateNoPeriodExistException();

                //temporary,
                return  0;
             }

             System.out.println(glJournalSetOfBook.getGlAccountingCalendar().getAcCode());

              LocalGlAccountingCalendarValue glAccountingCalendarValue =
                   glAccountingCalendarValueHome.findByAcCodeAndDate(
                           glJournalSetOfBook.getGlAccountingCalendar().getAcCode(),dateNow, AD_CMPNY);

              int supplierToGenerate = 0;


              System.out.println(glAccountingCalendarValue.getAcvDateTo()+" = "+ dateNow +" ="+glAccountingCalendarValue.getAcvDateTo().compareTo(new Date()));


               Collection glInvestorAccountBalancesSupplierList =  glInvestorAccountBalanceHome.findByAcvCode(
                                    glAccountingCalendarValue.getAcvCode(), AD_CMPNY);

               Iterator i = glInvestorAccountBalancesSupplierList.iterator();
               //loop for multiple supplier investors
               while(i.hasNext()){
                   LocalGlInvestorAccountBalance glInvestorAccountBalancesPerSupplier = (LocalGlInvestorAccountBalance) i.next();

                   Date dateNowEndOfTheMonth = glInvestorAccountBalancesPerSupplier.getGlAccountingCalendarValue().getAcvDateTo();

                   int supplierCode = glInvestorAccountBalancesPerSupplier.getApSupplier().getSplCode();



                   Collection glInvestorAccountBalances =
                            glInvestorAccountBalanceHome.findBonusAndInterestByAcCodeAndSplCode(
                                            glJournalSetOfBook.getGlAccountingCalendar().getAcCode(),supplierCode , AD_CMPNY);

                   Iterator j = glInvestorAccountBalances.iterator();

                   //loop for every investor account balance per supplier
                   int monthsUngenerated = 0;
                   while(j.hasNext()){
                      LocalGlInvestorAccountBalance glInvestorAccountBalance = (LocalGlInvestorAccountBalance) j.next();

                      Date acvDateFrom = glInvestorAccountBalance.getGlAccountingCalendarValue().getAcvDateFrom();
                      Date acvDateTo = glInvestorAccountBalance.getGlAccountingCalendarValue().getAcvDateTo();


                     int CHECK_DATE_GAP = Days.daysBetween(new LocalDate(acvDateTo), new LocalDate(dateNowEndOfTheMonth)).getDays();

                     //This will check if lines is not exceeding in current date
                     if(CHECK_DATE_GAP < 0){
                            System.out.println("Date exceed. Loop will now exit");
                            break;
                      }else{
                         monthsUngenerated++;
                     }


                   }

                   if(monthsUngenerated>0){
                       supplierToGenerate++;
                   }
               }



                return supplierToGenerate;

          } catch (Exception ex) {

          	 Debug.printStackTrace(ex);
             throw new EJBException(ex.getMessage());

          }


     }



    /**
     * @ejb:interface-method view-type="remote"
     **/
     public int executeArGenerateAccruedInterestIS(Integer AD_BRNCH, Integer AD_CMPNY) {

        Debug.print("AdMainNotificationControllerBean executeArGenerateAccruedInterestIS");


        LocalApCheckHome apCheckHome = null;

        // Initialize EJB Home

        try {

        	apCheckHome = (LocalApCheckHome)EJBHomeFactory.
                lookUpLocalHome(LocalApCheckHome.JNDI_NAME, LocalApCheckHome.class);

        } catch (NamingException ex) {

            throw new EJBException(ex.getMessage());

        }

        try {

            int countToGenerate = 0;
            Collection inscribedStocks = apCheckHome.findDirectChkForAccruedInterestISGeneration(AD_BRNCH, AD_CMPNY);

            Iterator x = inscribedStocks.iterator();

            while(x.hasNext()){

                LocalApCheck apCheck = (LocalApCheck)x.next();

                Date maturityDate = apCheck.getChkInvtMaturityDate();
                Date nextRunDate = apCheck.getChkInvtNextRunDate();

                int CHECK_MATURITY_DATE_GAP = Days.daysBetween(new LocalDate(nextRunDate),new LocalDate(maturityDate)).getDays();

                   //This will check if lines is not exceeding in maturity date
                if(CHECK_MATURITY_DATE_GAP < 0){
                    System.out.println("maturity date gap " + CHECK_MATURITY_DATE_GAP);
                    continue;
                }

                int CHECK_NEXTRUN_DATE_GAP = Days.daysBetween( new LocalDate(nextRunDate),new LocalDate(new Date())).getDays();

                   //This will check if lines is not exceeding in current date
                if(CHECK_NEXTRUN_DATE_GAP < 0){
                      System.out.println("nextrundate date gap " + CHECK_NEXTRUN_DATE_GAP);
                    continue;
                }

                countToGenerate++;
            }

            System.out.println("inscribedStocks.size()="+inscribedStocks.size());
            System.out.println("Count to generate = " + countToGenerate);


            return countToGenerate;

        } catch (Exception ex) {

        	 Debug.printStackTrace(ex);
           throw new EJBException(ex.getMessage());

        }

     }






     /**
      * @ejb:interface-method view-type="remote"
      **/
      public int executeApGenerateLoan(Integer AD_BRNCH, Integer AD_CMPNY) {

         Debug.print("AdMainNotificationControllerBean executeApGenerateLoan");


         LocalApVoucherHome apVoucherHome = null;

         // Initialize EJB Home

         try {

        	 apVoucherHome = (LocalApVoucherHome)EJBHomeFactory.
                 lookUpLocalHome(LocalApVoucherHome.JNDI_NAME, LocalApVoucherHome.class);

         } catch (NamingException ex) {

             throw new EJBException(ex.getMessage());

         }

         try {


             Collection generateLoans = apVoucherHome.findVouForLoanGeneration(AD_BRNCH, AD_CMPNY);


             return generateLoans.size();

         } catch (Exception ex) {

         	 Debug.printStackTrace(ex);
            throw new EJBException(ex.getMessage());

         }

      }




     /**
      * @ejb:interface-method view-type="remote"
      **/
      public int executeArGenerateAccruedInterestTB(Integer AD_BRNCH, Integer AD_CMPNY) {

         Debug.print("AdMainNotificationControllerBean executeArGenerateAccruedInterestTB");


         LocalApCheckHome apCheckHome = null;

         // Initialize EJB Home

         try {

         	apCheckHome = (LocalApCheckHome)EJBHomeFactory.
                 lookUpLocalHome(LocalApCheckHome.JNDI_NAME, LocalApCheckHome.class);

         } catch (NamingException ex) {

             throw new EJBException(ex.getMessage());

         }

         try {


         	 Collection treasuryBills = apCheckHome.findDirectChkForAccruedInterestTBGeneration(AD_BRNCH, AD_CMPNY);

         	 System.out.println("treasuryBills.size()="+treasuryBills.size());
         	 return treasuryBills.size();

         } catch (Exception ex) {

         	 Debug.printStackTrace(ex);
            throw new EJBException(ex.getMessage());

         }

      }


    /**
     * @ejb:interface-method view-type="remote"
     **/
     public int executeArPastDueInvoices(Integer AD_BRNCH, Integer AD_CMPNY) {

        Debug.print("AdMainNotificationControllerBean executeArPastDueInvoices");

        LocalArInvoicePaymentScheduleHome arInvoicePaymentScheduleHome = null;

        // Initialize EJB Home

        try {

     	   arInvoicePaymentScheduleHome = (LocalArInvoicePaymentScheduleHome)EJBHomeFactory.
                lookUpLocalHome(LocalArInvoicePaymentScheduleHome.JNDI_NAME, LocalArInvoicePaymentScheduleHome.class);

        } catch (NamingException ex) {

            throw new EJBException(ex.getMessage());

        }

        try {

        	System.out.println("EJBCommon.convertStringToSQLDate(EJBCommon.convertSQLDateToString(new Date()) )="+EJBCommon.convertStringToSQLDate(EJBCommon.convertSQLDateToString(new Date()) ));


        	 Collection executeArPastDueInvoices = arInvoicePaymentScheduleHome.findPastdueIpsByPenaltyDueDate(EJBCommon.convertStringToSQLDate(EJBCommon.convertSQLDateToString(new Date()) ), AD_BRNCH, AD_CMPNY);

        	 System.out.println("executeArPastDueInvoices.size()="+executeArPastDueInvoices.size());
        	 return executeArPastDueInvoices.size();

        } catch (Exception ex) {

        	 Debug.printStackTrace(ex);
           throw new EJBException(ex.getMessage());

        }

     }

 /**
  * @ejb:interface-method view-type="remote"
  **/
  public int executeInvBSTOrdersUnserved(Integer AD_BRNCH, Integer AD_CMPNY) {

     Debug.print("AdMainNotificationControllerBean executeInvBSTOrdersUnserved");

     LocalInvBranchStockTransferHome invBranchStockTransferHome = null;
     LocalInvBranchStockTransferLineHome invBranchStockTransferLineHome = null;

     // Initialize EJB Home

     try {

	  	 invBranchStockTransferHome = (LocalInvBranchStockTransferHome)EJBHomeFactory.
	  			 lookUpLocalHome(LocalInvBranchStockTransferHome.JNDI_NAME, LocalInvBranchStockTransferHome.class);

		 invBranchStockTransferLineHome = (LocalInvBranchStockTransferLineHome)EJBHomeFactory.
				 lookUpLocalHome(LocalInvBranchStockTransferLineHome.JNDI_NAME, LocalInvBranchStockTransferLineHome.class);

     } catch (NamingException ex) {

         throw new EJBException(ex.getMessage());

     }

     try {

     	 Collection invBranchStockTransfers = invBranchStockTransferHome.findBstByBstTypeAndAdBranchAndAdCompany("ORDER", AD_BRNCH, AD_CMPNY);
     	 Iterator i = invBranchStockTransfers.iterator();
     	 ArrayList list = new ArrayList();
     	 while (i.hasNext()) {

			LocalInvBranchStockTransfer invBranchStockTransfer = (LocalInvBranchStockTransfer)i.next();
			boolean isBstBreak = false;
    		Collection invBranchStockTransferLines = invBranchStockTransfer.getInvBranchStockTransferLines();
    		Iterator j = invBranchStockTransferLines.iterator();
    		while (j.hasNext()) {

    			LocalInvBranchStockTransferLine invBranchStockTransferLine = (LocalInvBranchStockTransferLine)j.next();
    			Collection invServedBsts = null;

    			if (invBranchStockTransfer.getBstType().equals("ORDER") || invBranchStockTransfer.getBstType().equals("REGULAR") || invBranchStockTransfer.getBstType().equals("EMERGENCY")) {
    				invServedBsts = invBranchStockTransferLineHome.findByBstTransferOrderNumberAndIiNameAndLocNameAndBrCode(
    						invBranchStockTransfer.getBstNumber(), invBranchStockTransferLine.getInvItemLocation().getInvItem().getIiName(),
    						invBranchStockTransferLine.getInvItemLocation().getInvLocation().getLocName(), invBranchStockTransfer.getAdBranch().getBrCode(), AD_CMPNY);
    			} else if (invBranchStockTransfer.getBstType().equals("OUT")) {
    				invServedBsts = invBranchStockTransferLineHome.findByBstTransferOutNumberAndIiNameAndLocNameAndBrCode(
    						invBranchStockTransfer.getBstNumber(), invBranchStockTransferLine.getInvItemLocation().getInvItem().getIiName(),
    						invBranchStockTransferLine.getInvItemLocation().getInvLocation().getLocName(), invBranchStockTransfer.getAdBranch().getBrCode(), AD_CMPNY);
    			}
    			System.out.println("invServed-" + invServedBsts.size());
    			double totalServed = 0;
    			Iterator k = invServedBsts.iterator();
    			while (k.hasNext()) {

    				LocalInvBranchStockTransferLine invServedBst = (LocalInvBranchStockTransferLine)k.next();
    				totalServed += invServedBst.getBslQuantity();

    			}

    			if (invServedBsts.size() == 0 || totalServed != invBranchStockTransferLine.getBslQuantity()) {
    				InvModBranchStockTransferDetails mdetails = new InvModBranchStockTransferDetails();
					mdetails.setBstCode(invBranchStockTransfer.getBstCode());
					mdetails.setBstType(invBranchStockTransfer.getBstType());
					mdetails.setBstDate(invBranchStockTransfer.getBstDate());
					mdetails.setBstNumber(invBranchStockTransfer.getBstNumber());
					mdetails.setBstTransferOutNumber(invBranchStockTransfer.getBstTransferOutNumber());
					mdetails.setBstTransferOrderNumber(invBranchStockTransfer.getBstTransferOrderNumber());

					list.add(mdetails);
	        		isBstBreak = true;
    			}
    			if (isBstBreak) break;

    		}

    		if (isBstBreak) continue;

     	 }

        return list.size();

     } catch (Exception ex) {

     	 Debug.printStackTrace(ex);
     	 return 0;

     }

  }

 /**
  * @ejb:interface-method view-type="remote"
  **/
  public int executeChecksForPrinting(Integer AD_BRNCH, Integer AD_CMPNY) {

     Debug.print("AdMainNotificationControllerBean executeChecksForPrinting");

     LocalApCheckHome apCheckHome = null;

     // Initialize EJB Home

     try {

    	 apCheckHome = (LocalApCheckHome)EJBHomeFactory.
             lookUpLocalHome(LocalApCheckHome.JNDI_NAME, LocalApCheckHome.class);

     } catch (NamingException ex) {

         throw new EJBException(ex.getMessage());

     }

     try {

     	 Collection apCheckForPrinting = apCheckHome.findChecksForPrintingByBrCode(AD_BRNCH, AD_CMPNY);

        return apCheckForPrinting.size();

     } catch (Exception ex) {

     	 Debug.printStackTrace(ex);
        throw new EJBException(ex.getMessage());

     }

  }


  /**
   * @ejb:interface-method view-type="remote"
   **/
   public int executeBuildUnbuildAssembliesOrderInDue(Integer AD_BRNCH, Integer AD_CMPNY) {

      Debug.print("AdMainNotificationControllerBean executeBuildUnbuildAssembliesOrderInDue");

      LocalInvBuildUnbuildAssemblyHome invBuildUnbuildAssemblyHome = null;

      // Initialize EJB Home

      try {

    	  invBuildUnbuildAssemblyHome = (LocalInvBuildUnbuildAssemblyHome)EJBHomeFactory.
              lookUpLocalHome(LocalInvBuildUnbuildAssemblyHome.JNDI_NAME, LocalInvBuildUnbuildAssemblyHome.class);

      } catch (NamingException ex) {

          throw new EJBException(ex.getMessage());

      }

      try {

    	  Date startingDate = new java.util.Date();
    	  GregorianCalendar gc = new GregorianCalendar();
    	  gc.setTime(startingDate);
    	  gc.add(Calendar.WEEK_OF_MONTH, 2);
    	  Date result = gc.getTime();

    	  SimpleDateFormat format = new SimpleDateFormat("yyyy-mm-dd");


      	Collection unbuildAssemblies = invBuildUnbuildAssemblyHome.findUnpostedBuaByBuaDueDateRange(format.parse(format.format(startingDate)), result, AD_CMPNY);
      	System.out.println("unbuildAssemblies="+unbuildAssemblies.size());
      	System.out.println("startingDate="+startingDate);
      	 System.out.println("result="+result);
         return unbuildAssemblies.size();

      } catch (Exception ex) {

      	 Debug.printStackTrace(ex);
         throw new EJBException(ex.getMessage());

      }

   }

   /**
    * @ejb:interface-method view-type="remote"
    **/
    public int executeAdDocumentInDraft(String DCMNT_TYP, Integer AD_BRNCH, Integer AD_CMPNY) {

       Debug.print("AdMainNotificationControllerBean executeAdDocumentInDraft");

       LocalGlJournalHome glJournalHome = null;
       LocalApVoucherHome apVoucherHome = null;
       LocalApCheckHome apCheckHome = null;
       LocalArInvoiceHome arInvoiceHome = null;
       LocalArSalesOrderHome arSalesOrderHome = null;
       LocalArJobOrderHome arJobOrderHome = null;

       LocalArReceiptHome arReceiptHome = null;
       LocalCmAdjustmentHome cmAdjustmentHome = null;
       LocalCmFundTransferHome cmFundTransferHome = null;
       LocalInvAdjustmentHome invAdjustmentHome = null;
       LocalInvBuildUnbuildAssemblyHome invBuildUnbuildAssemblyHome = null;
       LocalApPurchaseOrderHome apPurchaseOrderHome = null;
       LocalApPurchaseRequisitionHome apPurchaseRequisitionHome = null;
       LocalInvBranchStockTransferHome invBranchStockTransferHome = null;


       // Initialize EJB Home

       try {

       	  glJournalHome = (LocalGlJournalHome)EJBHomeFactory.
             lookUpLocalHome(LocalGlJournalHome.JNDI_NAME, LocalGlJournalHome.class);
          apVoucherHome = (LocalApVoucherHome)EJBHomeFactory.
             lookUpLocalHome(LocalApVoucherHome.JNDI_NAME, LocalApVoucherHome.class);
          apCheckHome = (LocalApCheckHome)EJBHomeFactory.
             lookUpLocalHome(LocalApCheckHome.JNDI_NAME, LocalApCheckHome.class);
          arInvoiceHome = (LocalArInvoiceHome)EJBHomeFactory.
             lookUpLocalHome(LocalArInvoiceHome.JNDI_NAME, LocalArInvoiceHome.class);
          arSalesOrderHome = (LocalArSalesOrderHome)EJBHomeFactory.
                  lookUpLocalHome(LocalArSalesOrderHome.JNDI_NAME, LocalArSalesOrderHome.class);
          arJobOrderHome = (LocalArJobOrderHome)EJBHomeFactory.
                  lookUpLocalHome(LocalArJobOrderHome.JNDI_NAME, LocalArJobOrderHome.class);

          arReceiptHome = (LocalArReceiptHome)EJBHomeFactory.
             lookUpLocalHome(LocalArReceiptHome.JNDI_NAME, LocalArReceiptHome.class);
          cmAdjustmentHome = (LocalCmAdjustmentHome)EJBHomeFactory.
             lookUpLocalHome(LocalCmAdjustmentHome.JNDI_NAME, LocalCmAdjustmentHome.class);
          cmFundTransferHome = (LocalCmFundTransferHome)EJBHomeFactory.
             lookUpLocalHome(LocalCmFundTransferHome.JNDI_NAME, LocalCmFundTransferHome.class);
          invAdjustmentHome = (LocalInvAdjustmentHome)EJBHomeFactory.
          	 lookUpLocalHome(LocalInvAdjustmentHome.JNDI_NAME, LocalInvAdjustmentHome.class);
          invBuildUnbuildAssemblyHome = (LocalInvBuildUnbuildAssemblyHome)EJBHomeFactory.
        	 lookUpLocalHome(LocalInvBuildUnbuildAssemblyHome.JNDI_NAME, LocalInvBuildUnbuildAssemblyHome.class);
          apPurchaseOrderHome = (LocalApPurchaseOrderHome)EJBHomeFactory.
     	 	lookUpLocalHome(LocalApPurchaseOrderHome.JNDI_NAME, LocalApPurchaseOrderHome.class);
          apPurchaseRequisitionHome = (LocalApPurchaseRequisitionHome)EJBHomeFactory.
   	 		lookUpLocalHome(LocalApPurchaseRequisitionHome.JNDI_NAME, LocalApPurchaseRequisitionHome.class);
          invBranchStockTransferHome = (LocalInvBranchStockTransferHome)EJBHomeFactory.
   	 		lookUpLocalHome(LocalInvBranchStockTransferHome.JNDI_NAME, LocalInvBranchStockTransferHome.class);
         

       } catch (NamingException ex) {

           throw new EJBException(ex.getMessage());

       }

       try {

           if (DCMNT_TYP.equals("GL JOURNAL")) {

               return (glJournalHome.findDraftJrByBrCode(AD_BRNCH, AD_CMPNY)).size();

           } else if (DCMNT_TYP.equals("AP CHECK PAYMENT REQUEST")) {

			   return (apVoucherHome.findDraftVouByVouTypeAndVouDebitMemoAndBrCode("REQUEST", EJBCommon.FALSE, AD_BRNCH, AD_CMPNY)).size();


           } else if (DCMNT_TYP.equals("AP VOUCHER")) {
        	   System.out.print("AP VOUCHER="+(apVoucherHome.findDraftVouAndVouDebitMemoAndBrCode(EJBCommon.FALSE, AD_BRNCH, AD_CMPNY)).size());
			   return (apVoucherHome.findDraftVouAndVouDebitMemoAndBrCode(EJBCommon.FALSE, AD_BRNCH, AD_CMPNY)).size();

           } else if (DCMNT_TYP.equals("AP DEBIT MEMO")) {

			   return (apVoucherHome.findDraftVouByVouTypeAndVouDebitMemoAndBrCode("DEBIT MEMO", EJBCommon.TRUE, AD_BRNCH, AD_CMPNY)).size();

           } else if (DCMNT_TYP.equals("AP CHECK")) {

			   return (apCheckHome.findDraftChkByBrCode(AD_BRNCH, AD_CMPNY)).size();

           } else if (DCMNT_TYP.equals("AP DIRECT CHECK")) {

			   return (apCheckHome.findDraftDirectChkByBrCode(AD_BRNCH, AD_CMPNY)).size();
			   
           } else if (DCMNT_TYP.equals("AR SALES ORDER")) {

        	   return ( arSalesOrderHome.findDraftSoByBrCode(AD_BRNCH, AD_CMPNY)).size();
        	   
           } else if (DCMNT_TYP.equals("AR JOB ORDER")) {

        	   return ( arJobOrderHome.findDraftJoByBrCode(AD_BRNCH, AD_CMPNY)).size();
        	   
           } else if (DCMNT_TYP.equals("AR INVOICE ITEMS")) {

        	   return ( arInvoiceHome.findDraftInvByInvCreditMemoAndBrCode("ITEMS", EJBCommon.FALSE, AD_BRNCH, AD_CMPNY)).size();

           } else if (DCMNT_TYP.equals("AR INVOICE MEMO LINES")) {

        	   return ( arInvoiceHome.findDraftInvByInvCreditMemoAndBrCode("MEMO LINES", EJBCommon.FALSE, AD_BRNCH, AD_CMPNY)).size();
        	   
           } else if (DCMNT_TYP.equals("AR INVOICE SO MATCHED")) {

        	   return ( arInvoiceHome.findDraftInvByInvCreditMemoAndBrCode("SO MATCHED", EJBCommon.FALSE, AD_BRNCH, AD_CMPNY)).size();

           } else if (DCMNT_TYP.equals("AR INVOICE JO MATCHED")) {

        	   return ( arInvoiceHome.findDraftInvByInvCreditMemoAndBrCode("JO MATCHED", EJBCommon.FALSE, AD_BRNCH, AD_CMPNY)).size();

           } else if (DCMNT_TYP.equals("AR CREDIT MEMO ITEMS")) {

        	   return ( arInvoiceHome.findDraftInvByInvCreditMemoAndBrCode("ITEMS", EJBCommon.TRUE, AD_BRNCH, AD_CMPNY)).size();

           } else if (DCMNT_TYP.equals("AR CREDIT MEMO MEMO LINES")) {

        	   return ( arInvoiceHome.findDraftInvByInvCreditMemoAndBrCode("MEMO LINES",EJBCommon.TRUE, AD_BRNCH, AD_CMPNY)).size();

           } else if (DCMNT_TYP.equals("AR RECEIPT COLLECTION")) {

			   return (arReceiptHome.findDraftRctByTypeBrCode("COLLECTION",AD_BRNCH, AD_CMPNY)).size();

           }else if (DCMNT_TYP.equals("AR RECEIPT MISC")) {

    		   return (arReceiptHome.findDraftRctByTypeBrCode("MISC",AD_BRNCH, AD_CMPNY)).size();

           } else if (DCMNT_TYP.equals("CM ADJUSTMENT")) {

			   return (cmAdjustmentHome.findDraftAdjByBrCode(AD_BRNCH, AD_CMPNY)).size();

           } else if (DCMNT_TYP.equals("CM FUND TRANSFER")) {

			   return (cmFundTransferHome.findDraftFtByBrCode(AD_BRNCH, AD_CMPNY)).size();

           } else if (DCMNT_TYP.equals("INV ADJUSTMENT")) {

           	   return (invAdjustmentHome.findDraftAdjByBrCode(AD_BRNCH, AD_CMPNY)).size();

           } else if (DCMNT_TYP.equals("INV BRANCH STOCK TRANSFER ORDER DRAFT")) {

           	   return (invBranchStockTransferHome.findDraftBstOrderByBrCode(AD_BRNCH, AD_CMPNY)).size();

           } else if (DCMNT_TYP.equals("INV BRANCH STOCK TRANSFER OUT DRAFT")) {

               return invBranchStockTransferHome.findDraftBstOutByBrCode(AD_BRNCH, AD_CMPNY).size();

           } else if (DCMNT_TYP.equals("INV BRANCH STOCK TRANSFER OUT INCOMING")) {

        	   ArrayList incomingBSTOut = new ArrayList();
        	   Iterator i = invBranchStockTransferHome.findPostedIncomingBstByAdBranchAndBstAdCompany(AD_BRNCH, AD_CMPNY).iterator();
        	   while (i.hasNext()) {
        		   LocalInvBranchStockTransfer invBranchStockTransfer = (LocalInvBranchStockTransfer)i.next();
        		   if (invBranchStockTransfer.getBstLock()==(byte)0) incomingBSTOut.add(invBranchStockTransfer);
        	   }

               return incomingBSTOut.size();

           } else if (DCMNT_TYP.equals("INV BRANCH STOCK TRANSFER IN DRAFT")) {

               return invBranchStockTransferHome.findDraftBstInByBrCode(AD_BRNCH, AD_CMPNY).size();

           } else if (DCMNT_TYP.equals("INV BUILD ASSEMBLY DRAFT")) {

               return (invBuildUnbuildAssemblyHome.findDraftBuaByBrCode(AD_BRNCH, AD_CMPNY)).size();

           } else if (DCMNT_TYP.equals("INV BUILD ASSEMBLY ORDER DRAFT")) {

               return (invBuildUnbuildAssemblyHome.findDraftOrderBuaByBrCode(AD_BRNCH, AD_CMPNY)).size();

           } else if (DCMNT_TYP.equals("INV BUILD ASSEMBLY DUE")) {

               return (invBuildUnbuildAssemblyHome.findDraftBuaByBrCode(AD_BRNCH, AD_CMPNY)).size();

           } else if (DCMNT_TYP.equals("AP PURCHASE ORDER")) {

                return (apPurchaseOrderHome.findDraftPoByBrCode(AD_BRNCH, AD_CMPNY)).size();

           } else if (DCMNT_TYP.equals("AP RECEIVING ITEM")) {

               return (apPurchaseOrderHome.findDraftRiByBrCode(AD_BRNCH, AD_CMPNY)).size();

           } else if (DCMNT_TYP.equals("AP PURCHASE REQUISITION")) {

               return (apPurchaseRequisitionHome.findDraftPrByBrCode(AD_BRNCH, AD_CMPNY)).size();

		   }

           /*else if (DCMNT_TYP.equals("AP CANVASS")) {

               return (apPurchaseRequisitionHome.findDraftPrByBrCode(AD_BRNCH, AD_CMPNY)).size();

           }*/

           return 0;

       } catch (Exception ex) {

       	 Debug.printStackTrace(ex);
          throw new EJBException(ex.getMessage());

       }

    }


   // Session Methods

   /**
    * @ejb:create-method view-type="remote"
    **/
   public void ejbCreate()
      throws CreateException {

      Debug.print("AdMainNotificationControllerBean ejbCreate");


   }

}
