
/*
 * AdPaymentScheduleControllerBean.java
 *
 * Created on May 29, 2003, 2:27 PM
 * @author  Enrico C. Yap
 */

package com.ejb.txn;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;
import javax.ejb.RemoveException;
import javax.naming.NamingException;

import com.ejb.ad.LocalAdPaymentSchedule;
import com.ejb.ad.LocalAdPaymentScheduleHome;
import com.ejb.ad.LocalAdPaymentTerm;
import com.ejb.ad.LocalAdPaymentTermHome;
import com.ejb.exception.AdPSRelativeAmountLessPytBaseAmountException;
import com.ejb.exception.AdPSRelativeAmountOverPytBaseAmountException;
import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.exception.GlobalRecordAlreadyDeletedException;
import com.util.AbstractSessionBean;
import com.util.AdPaymentScheduleDetails;
import com.util.AdPaymentTermDetails;
import com.util.Debug;
import com.util.EJBHomeFactory;
import com.util.EJBCommon;

/**
 * @ejb:bean name="AdPaymentScheduleControllerEJB"
 *           display-name="Used for entering payment schedules"
 *           type="Stateless"
 *           view-type="remote"
 *           jndi-name="ejb/AdPaymentScheduleControllerEJB"
 *
 * @ejb:interface remote-class="com.ejb.txn.AdPaymentScheduleController"
 *                extends="javax.ejb.EJBObject"
 *
 * @ejb:home remote-class="com.ejb.txn.AdPaymentScheduleControllerHome"
 *           extends="javax.ejb.EJBHome"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="aruser"
 *                        role-link="aruserlink"
 *
 * @ejb:permission role-name="aruser"
 * 
*/

public class AdPaymentScheduleControllerBean extends AbstractSessionBean {
    
    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList getAdPsByPytCode(Integer PYT_CODE, Integer AD_CMPNY) 
        throws GlobalNoRecordFoundException {
                    
        Debug.print("AdPaymentScheduleControllerBean getAdPsByPytCode");

        LocalAdPaymentScheduleHome adPaymentScheduleHome = null;

        Collection adPaymentSchedules = null;

        LocalAdPaymentSchedule adPaymentSchedule = null;

        ArrayList list = new ArrayList();
        
        // Initialize EJB Home

        try {
            
            adPaymentScheduleHome = (LocalAdPaymentScheduleHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdPaymentScheduleHome.JNDI_NAME, LocalAdPaymentScheduleHome.class);

        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }

        try {
            
            adPaymentSchedules = adPaymentScheduleHome.findByPytCode(PYT_CODE, AD_CMPNY);
            
        } catch (FinderException ex) {
            
        } catch (Exception ex) {
            
            throw new EJBException(ex.getMessage());
        }

        if (adPaymentSchedules.isEmpty()) {
        	
            throw new GlobalNoRecordFoundException();
        	
        }
        
        Iterator i = adPaymentSchedules.iterator();
               
        while (i.hasNext()) {
        	
        	adPaymentSchedule = (LocalAdPaymentSchedule)i.next();
                                                                        
        	AdPaymentScheduleDetails details = new AdPaymentScheduleDetails();
        		details.setPsCode(adPaymentSchedule.getPsCode());        		
                details.setPsLineNumber(adPaymentSchedule.getPsLineNumber());
                details.setPsRelativeAmount(adPaymentSchedule.getPsRelativeAmount());
                details.setPsDueDay(adPaymentSchedule.getPsDueDay());
                                                        	
        	list.add(details);
        }
        
        return list;
            
    }
    
    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public com.util.AdPaymentTermDetails getAdPytByPytCode(Integer PYT_CODE, Integer AD_CMPNY) {
                    
        Debug.print("AdPaymentScheduleControllerBean getAdPytByPytCode");
        		    	         	   	        	    
        LocalAdPaymentTermHome adPaymentTermHome = null;
                                       
        // Initialize EJB Home
        
        try {
            
            adPaymentTermHome = (LocalAdPaymentTermHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdPaymentTermHome.JNDI_NAME, LocalAdPaymentTermHome.class);              
            
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {
        	
        	LocalAdPaymentTerm adPaymentTerm = adPaymentTermHome.findByPrimaryKey(PYT_CODE);
   
        	AdPaymentTermDetails details = new AdPaymentTermDetails();
        		details.setPytCode(adPaymentTerm.getPytCode());        		
                details.setPytName(adPaymentTerm.getPytName());
                details.setPytDescription(adPaymentTerm.getPytDescription());
                details.setPytBaseAmount(adPaymentTerm.getPytBaseAmount());
                details.setPytEnable(adPaymentTerm.getPytEnable());
                        
        	return details;
        	       	
        } catch (Exception ex) {
        	
        	throw new EJBException(ex.getMessage());
        	
        }
        
	}      
	
    /**
     * @ejb:interface-method view-type="remote"
     **/    
    public void addAdPsEntry(ArrayList psList, Integer PYT_CODE, Integer AD_CMPNY) 
        throws 	AdPSRelativeAmountOverPytBaseAmountException,
        		AdPSRelativeAmountLessPytBaseAmountException {
               
                    
        Debug.print("AdPaymentScheduleControllerBean addAdPsEntry");
        
        LocalAdPaymentScheduleHome adPaymentScheduleHome = null;
        LocalAdPaymentSchedule adPaymentSchedule = null;
        LocalAdPaymentTermHome adPaymentTermHome = null;
        LocalAdPaymentTerm adPaymentTerm = null;
        
        ArrayList list = new ArrayList();
        
        // Initialize EJB Home
        
        try {
            
            adPaymentTermHome = (LocalAdPaymentTermHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdPaymentTermHome.JNDI_NAME, LocalAdPaymentTermHome.class);
            adPaymentScheduleHome = (LocalAdPaymentScheduleHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdPaymentScheduleHome.JNDI_NAME, LocalAdPaymentScheduleHome.class);                
                                                      
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
            
        Collection adPaymentSchedules = null;
        
        try {

            adPaymentSchedules = adPaymentScheduleHome.findByPytCode(PYT_CODE, AD_CMPNY);
        
        } catch (Exception ex) {
        	
        	throw new EJBException(ex.getMessage());
        	
        }        	
        
        double TOTAL_RELATIVE_AMOUNT = 0.0d;    
        
        Iterator i = adPaymentSchedules.iterator();
               
        while (i.hasNext()) {
        	
        	adPaymentSchedule = (LocalAdPaymentSchedule)i.next();
                  
            TOTAL_RELATIVE_AMOUNT += adPaymentSchedule.getPsRelativeAmount(); 
            
        }
        
        try {
        	
        	i = psList.iterator();
        	
        	while(i.hasNext()) {
        		
        		AdPaymentScheduleDetails details = (AdPaymentScheduleDetails)i.next();
        		
        		if(details.getPsCode() == null) {
        			
        			TOTAL_RELATIVE_AMOUNT += details.getPsRelativeAmount();
        			
        		} else {
        	
        			adPaymentSchedule = adPaymentScheduleHome.findByPrimaryKey(details.getPsCode());
        			
        			TOTAL_RELATIVE_AMOUNT += details.getPsRelativeAmount() - adPaymentSchedule.getPsRelativeAmount();;
        			
        		}
        		
        	}
        	
        	TOTAL_RELATIVE_AMOUNT = EJBCommon.roundIt(TOTAL_RELATIVE_AMOUNT, (short)6);
        	
        	adPaymentTerm = adPaymentTermHome.findByPrimaryKey(PYT_CODE);
        	
        	if (TOTAL_RELATIVE_AMOUNT > adPaymentTerm.getPytBaseAmount()) {
	        	
	            throw new AdPSRelativeAmountOverPytBaseAmountException();
	        	
			} else if (TOTAL_RELATIVE_AMOUNT < adPaymentTerm.getPytBaseAmount()) {
	        	
	            throw new AdPSRelativeAmountLessPytBaseAmountException();
	        	
			}
        	
			i = psList.iterator();
			
        	while(i.hasNext()) {
        		
        		AdPaymentScheduleDetails details = (AdPaymentScheduleDetails)i.next();
        		
        		// create
        		if(details.getPsCode() == null) {
        		
        			adPaymentSchedule = adPaymentScheduleHome.create(details.getPsLineNumber(), 
                	        details.getPsRelativeAmount(), details.getPsDueDay(), AD_CMPNY); 
                	 
        			adPaymentTerm.addAdPaymentSchedule(adPaymentSchedule);
        			
                // update
        		} else {
        	
        			adPaymentSchedule = adPaymentScheduleHome.findByPrimaryKey(details.getPsCode());
        			
        			adPaymentSchedule.setPsLineNumber(details.getPsLineNumber());
        			adPaymentSchedule.setPsRelativeAmount(details.getPsRelativeAmount());
                    adPaymentSchedule.setPsDueDay(details.getPsDueDay());                
                    
        		}
        		
        		//adPaymentTerm.addAdPaymentSchedule(adPaymentSchedule);
        	
        	}
        		
        } catch (AdPSRelativeAmountOverPytBaseAmountException ex) {
        	
        	throw ex;
        	
        } catch (AdPSRelativeAmountLessPytBaseAmountException ex) {
        	
        	throw ex;
        	
        } catch (Exception ex) {
        	
        	getSessionContext().setRollbackOnly();
        	
            throw new EJBException(ex.getMessage());
        	
        }        
                               	                                        
    }	
    
   /**
    * @ejb:interface-method view-type="remote"
    **/
    public void deleteAdPsEntry(Integer PS_CODE, Integer AD_CMPNY) 
        throws GlobalRecordAlreadyDeletedException {               

      Debug.print("AdPaymentScheduleControllerBean deleteAdPsEntry");

      LocalAdPaymentSchedule adPaymentSchedule = null;
      LocalAdPaymentScheduleHome adPaymentScheduleHome = null;

      // Initialize EJB Home
        
      try {

          adPaymentScheduleHome = (LocalAdPaymentScheduleHome)EJBHomeFactory.
              lookUpLocalHome(LocalAdPaymentScheduleHome.JNDI_NAME, LocalAdPaymentScheduleHome.class);           
      
      } catch (NamingException ex) {

          throw new EJBException(ex.getMessage());

      }                
      
      try {
      	
         adPaymentSchedule = adPaymentScheduleHome.findByPrimaryKey(PS_CODE);
         
      } catch (FinderException ex) {
      	
         throw new GlobalRecordAlreadyDeletedException();
         
      } catch (Exception ex) {
      	
         throw new EJBException(ex.getMessage()); 
         
      }
      
      try {
         	
	      adPaymentSchedule.remove();
	    
	  } catch (RemoveException ex) {
	 	
	      getSessionContext().setRollbackOnly();
	    
	      throw new EJBException(ex.getMessage());
	    
	  } catch (Exception ex) {
	 	
	      getSessionContext().setRollbackOnly();
	    
	      throw new EJBException(ex.getMessage());
	    
	  }	      
      
   }         
    
    // SessionBean methods

    /**
     * @ejb:create-method view-type="remote"
     **/
    public void ejbCreate() throws CreateException {

       Debug.print("AdPaymentScheduleControllerBean ejbCreate");
      
    }
}
