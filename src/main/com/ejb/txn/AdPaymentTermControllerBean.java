/*
 * AdPaymentTermControllerBean.java
 *
 * Created on May 27, 2003, 2:29 PM
 * @author  Enrico C. Yap
 */

package com.ejb.txn;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;
import javax.ejb.RemoveException;
import javax.naming.NamingException;

import com.ejb.ad.LocalAdPaymentSchedule;
import com.ejb.ad.LocalAdPaymentScheduleHome;
import com.ejb.ad.LocalAdPaymentTerm;
import com.ejb.ad.LocalAdPaymentTermHome;
import com.ejb.exception.GlobalAccountNumberInvalidException;
import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.exception.GlobalRecordAlreadyAssignedException;
import com.ejb.exception.GlobalRecordAlreadyDeletedException;
import com.ejb.exception.GlobalRecordAlreadyExistException;
import com.ejb.gl.LocalGlChartOfAccount;
import com.ejb.gl.LocalGlChartOfAccountHome;
import com.util.AbstractSessionBean;
import com.util.AdModPaymentTermDetails;
import com.util.Debug;
import com.util.EJBCommon;
import com.util.EJBHomeFactory;


/**
 * @ejb:bean name="AdPaymentTermControllerEJB"
 *           display-name="Used for entering payment terms"
 *           type="Stateless"
 *           view-type="remote"
 *           jndi-name="ejb/AdPaymentTermControllerEJB"
 *
 * @ejb:interface remote-class="com.ejb.txn.AdPaymentTermController"
 *                extends="javax.ejb.EJBObject"
 *
 * @ejb:home remote-class="com.ejb.txn.AdPaymentTermControllerHome"
 *           extends="javax.ejb.EJBHome"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="aruser"
 *                        role-link="aruserlink"
 *
 * @ejb:permission role-name="aruser"
 * 
*/

public class AdPaymentTermControllerBean extends AbstractSessionBean {
    
    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList getAdPytAll(Integer AD_CMPNY) 
        throws GlobalNoRecordFoundException {
                    
        Debug.print("AdPaymentTermControllerBean getAdPytAll");

        LocalAdPaymentTermHome adPaymentTermHome = null;

        ArrayList list = new ArrayList();
        
        // Initialize EJB Home

        try {
            
            adPaymentTermHome = (LocalAdPaymentTermHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdPaymentTermHome.JNDI_NAME, LocalAdPaymentTermHome.class);

        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {
        	
        	Collection adPaymentTerms = adPaymentTermHome.findPytAll(AD_CMPNY);
        	
        	if (adPaymentTerms.isEmpty()) {
        		
        		Debug.print("Empty");
        		
        	}
        	
        	if (adPaymentTerms.isEmpty()) {
        		
        		throw new GlobalNoRecordFoundException();
        		
        	}
        	
        	Iterator i = adPaymentTerms.iterator();
        	
        	while (i.hasNext()) {
        		
        		LocalAdPaymentTerm adPaymentTerm = (LocalAdPaymentTerm)i.next();
        		
        		AdModPaymentTermDetails mdetails = new AdModPaymentTermDetails();
        		mdetails.setPytCode(adPaymentTerm.getPytCode());        		
        		mdetails.setPytName(adPaymentTerm.getPytName());
        		mdetails.setPytDescription(adPaymentTerm.getPytDescription());
        		mdetails.setPytBaseAmount(adPaymentTerm.getPytBaseAmount());
        		mdetails.setPytMonthlyInterestRate(adPaymentTerm.getPytMonthlyInterestRate());
        		mdetails.setPytEnable(adPaymentTerm.getPytEnable());
        		mdetails.setPytEnableRebate(adPaymentTerm.getPytEnableRebate());
        		mdetails.setPytEnableInterest(adPaymentTerm.getPytEnableInterest());
        		mdetails.setPytDiscountOnInvoice(adPaymentTerm.getPytDiscountOnInvoice());
        		mdetails.setPytScheduleBasis(adPaymentTerm.getPytScheduleBasis());
        		
        		if(adPaymentTerm.getPytDiscountOnInvoice() == EJBCommon.TRUE) {
        			mdetails.setPtGlCoaAccountNumber(adPaymentTerm.getGlChartOfAccount().getCoaAccountNumber());
        			mdetails.setPtGlCoaAccountDescription(adPaymentTerm.getGlChartOfAccount().getCoaAccountDescription());
        			mdetails.setPytDiscountDescription(adPaymentTerm.getPytDiscountDescription());
        		}
        		
        		list.add(mdetails);
        	}
        	
        	return list;
          
        } catch (GlobalNoRecordFoundException ex) {
        	
        	throw ex;
        	        	
        } catch (Exception ex) {
        	
        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());
        	
        }
        
    }
    
    /**
     * @ejb:interface-method view-type="remote"
     **/    
    public void addAdPytEntry(com.util.AdPaymentTermDetails details, String PT_GL_COA_ACCNT_NMBR, Integer AD_CMPNY) 
        throws GlobalRecordAlreadyExistException,
		 	   GlobalAccountNumberInvalidException {
                    
        Debug.print("AdPaymentTermControllerBean addAdPytEntry");
        
        LocalAdPaymentTermHome adPaymentTermHome = null;
        LocalAdPaymentTerm adPaymentTerm = null;
        LocalGlChartOfAccountHome glChartOfAccountHome = null;
		LocalGlChartOfAccount glAccount = null;
		LocalAdPaymentScheduleHome adPaymentScheduleHome = null;
		LocalAdPaymentSchedule adPaymentSchedule = null;
		
        ArrayList list = new ArrayList();
        
        // Initialize EJB Home
        
        try {
            
            adPaymentTermHome = (LocalAdPaymentTermHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdPaymentTermHome.JNDI_NAME, LocalAdPaymentTermHome.class);
            glChartOfAccountHome = (LocalGlChartOfAccountHome)EJBHomeFactory.
            	lookUpLocalHome(LocalGlChartOfAccountHome.JNDI_NAME, LocalGlChartOfAccountHome.class);
            adPaymentScheduleHome = (LocalAdPaymentScheduleHome)EJBHomeFactory.
            	lookUpLocalHome(LocalAdPaymentScheduleHome.JNDI_NAME, LocalAdPaymentScheduleHome.class);
                                                      
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        try{
        	
        	try { 
        		
        		adPaymentTerm = adPaymentTermHome.findByPytName(details.getPytName(), AD_CMPNY);
        		
        		throw new GlobalRecordAlreadyExistException();
        		
        	} catch (FinderException ex) {
        		
        	}
        	
        	try {
        		
        		if (PT_GL_COA_ACCNT_NMBR != null && PT_GL_COA_ACCNT_NMBR.length() > 0) {
        			
        			glAccount = glChartOfAccountHome.findByCoaAccountNumber(PT_GL_COA_ACCNT_NMBR, AD_CMPNY);
        			
        		}
        		
        	} catch (FinderException ex) {
        		
        		throw new GlobalAccountNumberInvalidException();
        		           
        	}
        	        	
        	try {
        		
        		// create new payment term
        		
        		adPaymentTerm = adPaymentTermHome.create(details.getPytName(), 
        				details.getPytDescription(), details.getPytBaseAmount(), details.getPytMonthlyInterestRate(),
						details.getPytEnable(),details.getPytEnableRebate(),details.getPytEnableInterest(), details.getPytDiscountOnInvoice(), 
						details.getPytDiscountDescription(), details.getPytScheduleBasis(), AD_CMPNY); 
        		
        		if (glAccount != null) {
        			
        			glAccount.addAdPaymentTerm(adPaymentTerm);
        			
        		}
        		
        		// create payment schedule
        		short lineNumber = 1;
        		short dueDay = 0;
        		
        		adPaymentSchedule = adPaymentScheduleHome.create(lineNumber, details.getPytBaseAmount(), dueDay, AD_CMPNY);
        		
        		adPaymentTerm.addAdPaymentSchedule(adPaymentSchedule);
        		
        	} catch (Exception ex) {
        		
        		throw new EJBException(ex.getMessage());
        		
        	}  
        	
        } catch (GlobalRecordAlreadyExistException ex) {
        		
        	getSessionContext().setRollbackOnly();
        	throw ex;
        		
        } catch (GlobalAccountNumberInvalidException ex) {
        	
        	getSessionContext().setRollbackOnly();
        	throw ex;        	
        	
        } catch (Exception ex) {
        	
        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());
        	
        }
        
    }
    
    /**
     * @ejb:interface-method view-type="remote"
     **/
    public void updateAdPytEntry(com.util.AdPaymentTermDetails details, String PT_GL_COA_ACCNT_NMBR, Integer AD_CMPNY) 
        throws GlobalRecordAlreadyExistException, 
        	   GlobalAccountNumberInvalidException { 
                    
        Debug.print("AdPaymentTermControllerBean updateAdPytEntry");
        
        LocalAdPaymentTermHome adPaymentTermHome = null;                
        LocalAdPaymentTerm adPaymentTerm = null;   
        LocalGlChartOfAccountHome glChartOfAccountHome = null;
        LocalGlChartOfAccount glAccount = null;
             
                
        ArrayList list = new ArrayList();
        
        // Initialize EJB Home
        
        try {

            adPaymentTermHome = (LocalAdPaymentTermHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdPaymentTermHome.JNDI_NAME, LocalAdPaymentTermHome.class);           
            glChartOfAccountHome = (LocalGlChartOfAccountHome)EJBHomeFactory.
        		lookUpLocalHome(LocalGlChartOfAccountHome.JNDI_NAME, LocalGlChartOfAccountHome.class);	

        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {
        	try { 
        		
        		LocalAdPaymentTerm arExistingPaymentTerm = adPaymentTermHome.findByPytName(details.getPytName(), AD_CMPNY);
        		
        		if (!arExistingPaymentTerm.getPytCode().equals(details.getPytCode())) {
        			
        			throw new GlobalRecordAlreadyExistException();
        			
        		}
        		
        	} catch (FinderException ex) {
        		
        	}
        	
        	try {
        		
        		if (PT_GL_COA_ACCNT_NMBR != null && PT_GL_COA_ACCNT_NMBR.length() > 0) {
        			
        			glAccount = glChartOfAccountHome.findByCoaAccountNumber(PT_GL_COA_ACCNT_NMBR, AD_CMPNY);
        			
        		}
        		
        	} catch (FinderException ex) {
        		
        		throw new GlobalAccountNumberInvalidException();
        		           
        	}
        	
        	try {
        		
        		// find and update payment term
        		
        		adPaymentTerm = adPaymentTermHome.findByPrimaryKey(details.getPytCode());
        		
        		adPaymentTerm.setPytName(details.getPytName());
        		adPaymentTerm.setPytDescription(details.getPytDescription());
        		adPaymentTerm.setPytBaseAmount(details.getPytBaseAmount());
        		adPaymentTerm.setPytMonthlyInterestRate(details.getPytMonthlyInterestRate());
        		adPaymentTerm.setPytEnable(details.getPytEnable());
        		adPaymentTerm.setPytEnableRebate(details.getPytEnableRebate());
        		adPaymentTerm.setPytEnableInterest(details.getPytEnableInterest());
        		adPaymentTerm.setPytDiscountOnInvoice(details.getPytDiscountOnInvoice());
        		adPaymentTerm.setPytDiscountDescription(details.getPytDiscountDescription());
        		adPaymentTerm.setPytScheduleBasis(details.getPytScheduleBasis());
        		
        		if (glAccount != null) {
        			
        			glAccount.addAdPaymentTerm(adPaymentTerm);
        			
        		} else if (glAccount == null && adPaymentTerm.getGlChartOfAccount() != null){
        			
        			adPaymentTerm.getGlChartOfAccount().dropAdPaymentTerm(adPaymentTerm);
        			
        		}
        		
        	} catch (Exception ex) {
        		
        		throw new EJBException(ex.getMessage());
        		
        	}
        	
        } catch (GlobalRecordAlreadyExistException ex) {
        	
        	getSessionContext().setRollbackOnly();        	
        	throw ex;
        	
        } catch (GlobalAccountNumberInvalidException ex) {
        	
        	getSessionContext().setRollbackOnly();
        	ex.printStackTrace();
        	throw ex;        	
        	
        } catch (Exception ex) {
        	
        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());
        	
        }
    
    }
    
   /**
    * @ejb:interface-method view-type="remote"
    **/
    public void deleteAdPytEntry(Integer PYT_CODE, Integer AD_CMPNY) 
        throws GlobalRecordAlreadyDeletedException,
               GlobalRecordAlreadyAssignedException {

      Debug.print("AdPaymentTermControllerBean deleteAdPytEntry");

      LocalAdPaymentTerm adPaymentTerm = null;
      LocalAdPaymentTermHome adPaymentTermHome = null;

      // Initialize EJB Home
        
      try {

          adPaymentTermHome = (LocalAdPaymentTermHome)EJBHomeFactory.
              lookUpLocalHome(LocalAdPaymentTermHome.JNDI_NAME, LocalAdPaymentTermHome.class);           
      
      } catch (NamingException ex) {

          throw new EJBException(ex.getMessage());

      }                
      
      try {
      	
         adPaymentTerm = adPaymentTermHome.findByPrimaryKey(PYT_CODE);
         
      } catch (FinderException ex) {
      	
         throw new GlobalRecordAlreadyDeletedException();
         
      } catch (Exception ex) {
      	
         throw new EJBException(ex.getMessage()); 
         
      }
      
      try {

          if (!adPaymentTerm.getApVouchers().isEmpty() ||
          	  !adPaymentTerm.getApRecurringVouchers().isEmpty() ||
			  !adPaymentTerm.getApSuppliers().isEmpty() ||
			  !adPaymentTerm.getArCustomers().isEmpty() ||
			  !adPaymentTerm.getArInvoices().isEmpty()||
			  !adPaymentTerm.getApPurchaseOrders().isEmpty()||
			  !adPaymentTerm.getArPdcs().isEmpty()||
			  !adPaymentTerm.getArSalesOrders().isEmpty()) {
              
             throw new GlobalRecordAlreadyAssignedException();
             
          }
      } catch (GlobalRecordAlreadyAssignedException ex) {                        
         
          throw ex;
          
      } catch (Exception ex) {
       
            throw new EJBException(ex.getMessage());
           
      }                         
         
      try {
         	
	      adPaymentTerm.remove();
	    
	  } catch (RemoveException ex) {
	 	
	      getSessionContext().setRollbackOnly();
	    
	      throw new EJBException(ex.getMessage());
	    
	  } catch (Exception ex) {
	 	
	      getSessionContext().setRollbackOnly();
	    
	      throw new EJBException(ex.getMessage());
	    
	  }	      
      
   }    
    
    // SessionBean methods

    /**
     * @ejb:create-method view-type="remote"
     **/
    public void ejbCreate() throws CreateException {

       Debug.print("AdPaymentTermControllerBean ejbCreate");
      
    }
}
