
/*
 * AdPreferenceControllerBean.java
 *
 * Created on June 12, 2003, 10:21 AM
 *
 * @author  Neil Andrew M. Ajero
 *
 */

package com.ejb.txn;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;
import javax.naming.NamingException;
import com.ejb.ad.LocalAdBranch;
import com.ejb.ad.LocalAdBranchHome;
import com.ejb.ad.LocalAdCompany;
import com.ejb.ad.LocalAdCompanyHome;
import com.ejb.ad.LocalAdPreference;
import com.ejb.ad.LocalAdPreferenceHome;
import com.ejb.ar.LocalArTaxCode;
import com.ejb.ar.LocalArTaxCodeHome;
import com.ejb.ar.LocalArWithholdingTaxCode;
import com.ejb.ar.LocalArWithholdingTaxCodeHome;
import com.ejb.exception.AdPRFCoaGlAccruedVatAccountNotFoundException;
import com.ejb.exception.AdPRFCoaGlCustomerDepositAccountNotFoundException;
import com.ejb.exception.AdPRFCoaGlPOSAdjustmentAccountNotFoundException;
import com.ejb.exception.AdPRFCoaGlPettyCashAccountNotFoundException;
import com.ejb.exception.AdPRFCoaGlPosDineInChargeAccountNotFoundException;
import com.ejb.exception.AdPRFCoaGlPosDiscountAccountNotFoundException;
import com.ejb.exception.AdPRFCoaGlPosGiftCertificateAccountNotFoundException;
import com.ejb.exception.AdPRFCoaGlPosServiceChargeAccountNotFoundException;
import com.ejb.exception.AdPRFCoaGlVarianceAccountNotFoundException;
import com.ejb.exception.AdPRFCoaInvPreparedFoodTaxAccountNotFoundException;
import com.ejb.exception.AdPRFCoaInvSalesTaxAccountNotFoundException;
import com.ejb.exception.GlobalRecordAlreadyExistException;
import com.ejb.gl.LocalGlChartOfAccount;
import com.ejb.gl.LocalGlChartOfAccountHome;
import com.ejb.gl.LocalGlFunctionalCurrency;
import com.ejb.gl.LocalGlFunctionalCurrencyHome;
import com.ejb.inv.LocalInvLocation;
import com.ejb.inv.LocalInvLocationHome;
import com.util.AbstractSessionBean;
import com.util.AdModPreferenceDetails;
import com.util.Debug;
import com.util.EJBCommon;
import com.util.EJBHomeFactory;

/**
 * @ejb:bean name="AdPreferenceControllerEJB"
 *           display-name="Used for updating preferences"
 *           type="Stateless"
 *           view-type="remote"
 *           jndi-name="ejb/AdPreferenceControllerEJB"
 *
 * @ejb:interface remote-class="com.ejb.txn.AdPreferenceController"
 *                extends="javax.ejb.EJBObject"
 *
 * @ejb:home remote-class="com.ejb.txn.AdPreferenceControllerHome"
 *           extends="javax.ejb.EJBHome"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="aduser"
 *                        role-link="aduserlink"
 *
 * @ejb:permission role-name="aduser"
 *
*/

public class AdPreferenceControllerBean extends AbstractSessionBean {



	/**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public byte getAdPrfAdDisableMultipleLogin(String CMP_SHRT_NM) {

        Debug.print("AdPreferenceControllerBean getAdPrfAdDisableMultipleLogin");

        LocalAdPreferenceHome adPreferenceHome = null;
        LocalAdCompanyHome adCompanyHome = null;

        // Initialize EJB Home

        try {

            adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
            lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);

            adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
                    lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);


        } catch (NamingException ex) {

            throw new EJBException(ex.getMessage());

        }


        try {
        	System.out.println("CMP_SHRT_NM="+CMP_SHRT_NM);
        	LocalAdCompany adCompany = adCompanyHome.findByCmpShortName(CMP_SHRT_NM);
        	System.out.println("adCompany.getCmpCode()="+adCompany.getCmpCode());
            LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(adCompany.getCmpCode());
            System.out.println("adPreference.getPrfAdDisableMultipleLogin()="+adPreference.getPrfAdDisableMultipleLogin());
            return adPreference.getPrfAdDisableMultipleLogin();

        } catch (Exception ex) {

            Debug.printStackTrace(ex);
            throw new EJBException(ex.getMessage());

        }

    }



	/**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public short getGlFcPrecisionUnit(Integer AD_CMPNY) {

        Debug.print("AdPreferenceControllerBean getGlFcPrecisionUnit");


        LocalAdCompanyHome adCompanyHome = null;


        // Initialize EJB Home

        try {

            adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
            lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);

        } catch (NamingException ex) {

            throw new EJBException(ex.getMessage());

        }

        try {

            LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);

            return  adCompany.getGlFunctionalCurrency().getFcPrecision();

        } catch (Exception ex) {

            Debug.printStackTrace(ex);
            throw new EJBException(ex.getMessage());

        }

    }


    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public com.util.AdModPreferenceDetails getAdPrf(Integer AD_CMPNY) {

        Debug.print("AdPreferenceControllerBean getAdPrf");

        LocalAdPreferenceHome adPreferenceHome = null;
        LocalGlChartOfAccountHome glChartOfAccountHome = null;
        LocalAdPreference adPreference = null;


        // Initialize EJB Home

        try {

            adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);
            glChartOfAccountHome = (LocalGlChartOfAccountHome)EJBHomeFactory.
                lookUpLocalHome(LocalGlChartOfAccountHome.JNDI_NAME, LocalGlChartOfAccountHome.class);

        } catch (NamingException ex) {

            throw new EJBException(ex.getMessage());

        }

        LocalGlChartOfAccount glAccruedVatAccount = null;
        LocalGlChartOfAccount glPettyCashAccount = null;
        LocalGlChartOfAccount glPosAdjustmentAccount = null;
        LocalGlChartOfAccount glGeneralAdjustmentAccount = null;
        LocalGlChartOfAccount glIssuanceAdjustmentAccount = null;
        LocalGlChartOfAccount glProductionAdjustmentAccount = null;
        LocalGlChartOfAccount glWastageAdjustmentAccount = null;
        LocalGlChartOfAccount glPosDiscountAccount = null;
        LocalGlChartOfAccount glPosGiftCertificateAccount = null;
        LocalGlChartOfAccount glPosServiceChargeAccount = null;
        LocalGlChartOfAccount glPosDineInChargeAccount = null;
        LocalGlChartOfAccount glCoaCustomerDepositAccount = null;

        try {

	        adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);

			AdModPreferenceDetails mdetails = new AdModPreferenceDetails();
            mdetails.setPrfAllowSuspensePosting(adPreference.getPrfAllowSuspensePosting());
            mdetails.setPrfGlJournalLineNumber(adPreference.getPrfGlJournalLineNumber());
            mdetails.setPrfApJournalLineNumber(adPreference.getPrfApJournalLineNumber());
            mdetails.setPrfArInvoiceLineNumber(adPreference.getPrfArInvoiceLineNumber());
            mdetails.setPrfApWTaxRealization(adPreference.getPrfApWTaxRealization());
            mdetails.setPrfArWTaxRealization(adPreference.getPrfArWTaxRealization());
            mdetails.setPrfEnableGlJournalBatch(adPreference.getPrfEnableGlJournalBatch());
            mdetails.setPrfEnableGlRecomputeCoaBalance(adPreference.getPrfEnableGlRecomputeCoaBalance());

            mdetails.setPrfEnableApVoucherBatch(adPreference.getPrfEnableApVoucherBatch());
            mdetails.setPrfEnableApPOBatch(adPreference.getPrfEnableApPOBatch());
            mdetails.setPrfEnableApCheckBatch(adPreference.getPrfEnableApCheckBatch());
            mdetails.setPrfEnableArInvoiceBatch(adPreference.getPrfEnableArInvoiceBatch());
            mdetails.setPrfEnableArInvoiceInterestGeneration(adPreference.getPrfEnableArInvoiceInterestGeneration());
            mdetails.setPrfEnableArReceiptBatch(adPreference.getPrfEnableArReceiptBatch());
            mdetails.setPrfEnableArMiscReceiptBatch(adPreference.getPrfEnableArMiscReceiptBatch());
            mdetails.setPrfApGlPostingType(adPreference.getPrfApGlPostingType());
            mdetails.setPrfArGlPostingType(adPreference.getPrfArGlPostingType());
            mdetails.setPrfCmGlPostingType(adPreference.getPrfCmGlPostingType());
            mdetails.setPrfCmUseBankForm(adPreference.getPrfCmUseBankForm());
            mdetails.setPrfInvInventoryLineNumber(adPreference.getPrfInvInventoryLineNumber());
            mdetails.setPrfInvQuantityPrecisionUnit(adPreference.getPrfInvQuantityPrecisionUnit());
            mdetails.setPrfInvCostPrecisionUnit(adPreference.getPrfInvCostPrecisionUnit());
            mdetails.setPrfInvGlPostingType(adPreference.getPrfInvGlPostingType());
            mdetails.setPrfGlPostingType(adPreference.getPrfGlPostingType());
            mdetails.setPrfApFindCheckDefaultType(adPreference.getPrfApFindCheckDefaultType());
            mdetails.setPrfArFindReceiptDefaultType(adPreference.getPrfArFindReceiptDefaultType());
            mdetails.setPrfInvEnableShift(adPreference.getPrfInvEnableShift());
            mdetails.setPrfEnableInvBUABatch(adPreference.getPrfEnableInvBUABatch());
            mdetails.setPrfApUseAccruedVat(adPreference.getPrfApUseAccruedVat());
            mdetails.setPrfApDefaultCheckDate(adPreference.getPrfApDefaultCheckDate());
            mdetails.setPrfApDefaultChecker(adPreference.getPrfApDefaultChecker());
            mdetails.setPrfApDefaultApprover(adPreference.getPrfApDefaultApprover());
            mdetails.setPrfWtcName(adPreference.getArWithholdingTaxCode() != null ? adPreference.getArWithholdingTaxCode().getWtcName() : null);

            if (adPreference.getPrfApGlCoaAccruedVatAccount() != null && !adPreference.getPrfApGlCoaAccruedVatAccount().equals(new Integer(0))) {

	        	glAccruedVatAccount = glChartOfAccountHome.findByPrimaryKey(adPreference.getPrfApGlCoaAccruedVatAccount());

	        	mdetails.setPrfApGlCoaAccruedVatAccountNumber(glAccruedVatAccount.getCoaAccountNumber());
	            mdetails.setPrfApGlCoaAccruedVatAccountDescription(glAccruedVatAccount.getCoaAccountDescription());

            }

            if (adPreference.getPrfApGlCoaPettyCashAccount() != null && !adPreference.getPrfApGlCoaPettyCashAccount().equals(new Integer(0))) {

            	glPettyCashAccount = glChartOfAccountHome.findByPrimaryKey(adPreference.getPrfApGlCoaPettyCashAccount());

	        	mdetails.setPrfApGlCoaPettyCashAccountNumber(glPettyCashAccount.getCoaAccountNumber());
	            mdetails.setPrfApGlCoaPettyCashAccountDescription(glPettyCashAccount.getCoaAccountDescription());

    	    }

	        mdetails.setPrfApUseSupplierPulldown(adPreference.getPrfApUseSupplierPulldown());
	        mdetails.setPrfArUseCustomerPulldown(adPreference.getPrfArUseCustomerPulldown());
	        mdetails.setPrfApAutoGenerateSupplierCode(adPreference.getPrfApAutoGenerateSupplierCode());
	        mdetails.setPrfArAutoGenerateCustomerCode(adPreference.getPrfArAutoGenerateCustomerCode());
	        mdetails.setPrfApNextSupplierCode(adPreference.getPrfApNextSupplierCode());
	        mdetails.setPrfArNextCustomerCode(adPreference.getPrfArNextCustomerCode());
	        mdetails.setPrfApReferenceNumberValidation(adPreference.getPrfApReferenceNumberValidation());
	        mdetails.setPrfInvEnablePosIntegration(adPreference.getPrfInvEnablePosIntegration());
	        mdetails.setPrfInvEnablePosAutoPostUpload(adPreference.getPrfInvEnablePosAutoPostUpload());
	        mdetails.setPrfApCheckVoucherDataSource(adPreference.getPrfApCheckVoucherDataSource());
	        mdetails.setPrfApDefaultPrTax(adPreference.getPrfApDefaultPrTax());
	        mdetails.setPrfApDefaultPrCurrency(adPreference.getPrfApDefaultPrCurrency());
	        mdetails.setPrfArSalesInvoiceDataSource(adPreference.getPrfArSalesInvoiceDataSource());
	        mdetails.setPrfArAutoComputeCogs(adPreference.getPrfArAutoComputeCogs());

	        mdetails.setPrfArMonthlyInterestRate(adPreference.getPrfArMonthlyInterestRate());
	        mdetails.setPrfApAgingBucket(adPreference.getPrfApAgingBucket());
	        mdetails.setPrfArAgingBucket(adPreference.getPrfArAgingBucket());
	        mdetails.setPrfArAllowPriorDate(adPreference.getPrfArAllowPriorDate());
	        mdetails.setPrfApShowPrCost(adPreference.getPrfApShowPrCost());
	        mdetails.setPrfApDebitMemoOverrideCost(adPreference.getPrfApDebitMemoOverrideCost());
	        mdetails.setPrfGlYearEndCloseRestriction(adPreference.getPrfGlYearEndCloseRestriction());
	        mdetails.setPrfArEnablePaymentTerm(adPreference.getPrfArEnablePaymentTerm());
	        mdetails.setPrfArDisableSalesPrice(adPreference.getPrfArDisableSalesPrice());
	        mdetails.setPrfInvItemLocationShowAll(adPreference.getPrfInvItemLocationShowAll());
	        mdetails.setPrfInvItemLocationAddByItemList(adPreference.getPrfInvItemLocationAddByItemList());
	        mdetails.setPrfArValidateCustomerEmail(adPreference.getPrfArValidateCustomerEmail());
	        mdetails.setPrfArCheckInsufficientStock(adPreference.getPrfArCheckInsufficientStock());
	        mdetails.setPrfArDetailedReceivable(adPreference.getPrfArDetailedReceivable());
	        mdetails.setPrfAdDisableMultipleLogin(adPreference.getPrfAdDisableMultipleLogin());
	        mdetails.setPrfAdEnableEmailNotification(adPreference.getPrfAdEnableEmailNotification());
	        
	        mdetails.setPrfArSoSalespersonRequired(adPreference.getPrfArSoSalespersonRequired());
	        mdetails.setPrfArInvcSalespersonRequired(adPreference.getPrfArInvcSalespersonRequired());
	        
	        mdetails.setPrfMailHost(adPreference.getPrfMailHost());
	        mdetails.setPrfMailSocketFactoryPort(adPreference.getPrfMailSocketFactoryPort());
	        mdetails.setPrfMailPort(adPreference.getPrfMailPort());
	        mdetails.setPrfMailFrom(adPreference.getPrfMailFrom());
	        mdetails.setPrfMailAuthenticator(adPreference.getPrfMailAuthenticator());
	        mdetails.setPrfMailPassword(adPreference.getPrfMailPassword());
	        mdetails.setPrfMailTo(adPreference.getPrfMailTo());
	        mdetails.setPrfMailCc(adPreference.getPrfMailCc());
	        mdetails.setPrfMailBcc(adPreference.getPrfMailBcc());
	        mdetails.setPrfMailConfig(adPreference.getPrfMailConfig());
	        mdetails.setPrfCentralWarehouseBranch(adPreference.getPrfCentralWarehouseBranch());
	        mdetails.setPrfCentralWarehouseLocation(adPreference.getPrfCentralWarehouseLocation());



	        if(adPreference.getPrfInvEnablePosIntegration() == EJBCommon.TRUE || adPreference.getPrfInvPosAdjustmentAccount()!=null) {

	        	glPosAdjustmentAccount = glChartOfAccountHome.findByPrimaryKey(adPreference.getPrfInvPosAdjustmentAccount() == null ? new Integer(1) :adPreference.getPrfInvPosAdjustmentAccount());
	        	mdetails.setPrfInvGlCoaPosAdjustmentAccountNumber(glPosAdjustmentAccount.getCoaAccountNumber());
	        	mdetails.setPrfInvGlCoaPosAdjustmentAccountDescription(glPosAdjustmentAccount.getCoaAccountDescription());


	        }
	        if (adPreference.getPrfInvGeneralAdjustmentAccount() != null && !adPreference.getPrfInvGeneralAdjustmentAccount().equals(new Integer(0))) {

        	glGeneralAdjustmentAccount = glChartOfAccountHome.findByPrimaryKey(adPreference.getPrfInvGeneralAdjustmentAccount() == null ? new Integer(1) : adPreference.getPrfInvGeneralAdjustmentAccount());
        	mdetails.setPrfInvGlCoaGeneralAdjustmentAccountNumber(glGeneralAdjustmentAccount.getCoaAccountNumber());
        	mdetails.setPrfInvGlCoaGeneralAdjustmentAccountDescription(glGeneralAdjustmentAccount.getCoaAccountDescription());

	        }

	        if (adPreference.getPrfInvIssuanceAdjustmentAccount() != null && !adPreference.getPrfInvIssuanceAdjustmentAccount().equals(new Integer(0))) {

        	glIssuanceAdjustmentAccount = glChartOfAccountHome.findByPrimaryKey(adPreference.getPrfInvIssuanceAdjustmentAccount() == null ? new Integer(1) :adPreference.getPrfInvIssuanceAdjustmentAccount());
        	mdetails.setPrfInvGlCoaIssuanceAdjustmentAccountNumber(glIssuanceAdjustmentAccount.getCoaAccountNumber());
        	mdetails.setPrfInvGlCoaIssuanceAdjustmentAccountDescription(glIssuanceAdjustmentAccount.getCoaAccountDescription());

	        }

	        if (adPreference.getPrfInvProductionAdjustmentAccount() != null && !adPreference.getPrfInvProductionAdjustmentAccount().equals(new Integer(0))) {

        	glProductionAdjustmentAccount = glChartOfAccountHome.findByPrimaryKey(adPreference.getPrfInvProductionAdjustmentAccount() == null ? new Integer(1) :adPreference.getPrfInvProductionAdjustmentAccount());
        	mdetails.setPrfInvGlCoaProductionAdjustmentAccountNumber(glProductionAdjustmentAccount.getCoaAccountNumber());
        	mdetails.setPrfInvGlCoaProductionAdjustmentAccountDescription(glProductionAdjustmentAccount.getCoaAccountDescription());

	        }

	        if (adPreference.getPrfInvWastageAdjustmentAccount() != null && !adPreference.getPrfInvWastageAdjustmentAccount().equals(new Integer(0))) {

        	glWastageAdjustmentAccount = glChartOfAccountHome.findByPrimaryKey(adPreference.getPrfInvWastageAdjustmentAccount() == null ? new Integer(1) :adPreference.getPrfInvWastageAdjustmentAccount());
        	mdetails.setPrfInvGlCoaWastageAdjustmentAccountNumber(glWastageAdjustmentAccount.getCoaAccountNumber());
        	mdetails.setPrfInvGlCoaWastageAdjustmentAccountDescription(glWastageAdjustmentAccount.getCoaAccountDescription());

	        }

	        if (adPreference.getPrfMiscPosDiscountAccount() != null && !adPreference.getPrfMiscPosDiscountAccount().equals(new Integer(0))) {

	        	glPosDiscountAccount = glChartOfAccountHome.findByPrimaryKey(adPreference.getPrfMiscPosDiscountAccount() == null ? new Integer(1) :adPreference.getPrfMiscPosDiscountAccount());
	        	mdetails.setPrfMiscGlCoaPosDiscountAccountNumber(glPosDiscountAccount.getCoaAccountNumber());
	        	mdetails.setPrfMiscGlCoaPosDiscountAccountDescription(glPosDiscountAccount.getCoaAccountDescription());

	        }

	        if (adPreference.getPrfMiscPosGiftCertificateAccount() != null && !adPreference.getPrfMiscPosGiftCertificateAccount().equals(new Integer(0))) {

	        	glPosGiftCertificateAccount = glChartOfAccountHome.findByPrimaryKey(adPreference.getPrfMiscPosGiftCertificateAccount() == null ? new Integer(1) :adPreference.getPrfMiscPosGiftCertificateAccount());
	        	mdetails.setPrfMiscGlCoaPosGiftCertificateAccountNumber(glPosGiftCertificateAccount.getCoaAccountNumber());
	        	mdetails.setPrfMiscGlCoaPosGiftCertificateAccountDescription(glPosGiftCertificateAccount.getCoaAccountDescription());

	        }

	        if (adPreference.getPrfMiscPosServiceChargeAccount() != null && !adPreference.getPrfMiscPosServiceChargeAccount().equals(new Integer(0))) {

	        	glPosServiceChargeAccount = glChartOfAccountHome.findByPrimaryKey(adPreference.getPrfMiscPosServiceChargeAccount() == null ? new Integer(1) :adPreference.getPrfMiscPosServiceChargeAccount());
	        	mdetails.setPrfMiscGlCoaPosServiceChargeAccountNumber(glPosServiceChargeAccount.getCoaAccountNumber());
	        	mdetails.setPrfMiscGlCoaPosServiceChargeAccountDescription(glPosServiceChargeAccount.getCoaAccountDescription());

	        }

	        if (adPreference.getPrfMiscPosDineInChargeAccount() != null && !adPreference.getPrfMiscPosDineInChargeAccount().equals(new Integer(0))) {

	        	glPosDineInChargeAccount = glChartOfAccountHome.findByPrimaryKey(adPreference.getPrfMiscPosDineInChargeAccount() == null ? new Integer(1) :adPreference.getPrfMiscPosDineInChargeAccount());
	        	mdetails.setPrfMiscGlCoaPosDineInChargeAccountNumber(glPosDineInChargeAccount.getCoaAccountNumber());
	        	mdetails.setPrfMiscGlCoaPosDineInChargeAccountDescription(glPosDineInChargeAccount.getCoaAccountDescription());

	        }

	        if (adPreference.getPrfArGlCoaCustomerDepositAccount() != null && !adPreference.getPrfArGlCoaCustomerDepositAccount().equals(new Integer(0))) {

	        	glCoaCustomerDepositAccount = glChartOfAccountHome.findByPrimaryKey(adPreference.getPrfArGlCoaCustomerDepositAccount() == null ? new Integer(1) :adPreference.getPrfArGlCoaCustomerDepositAccount());
	        	mdetails.setPrfArGlCoaCustomerDepositAccountNumber(glCoaCustomerDepositAccount.getCoaAccountNumber());
	        	mdetails.setPrfArGlCoaCustomerDepositAccountDescription(glCoaCustomerDepositAccount.getCoaAccountDescription());

	        }

	        if (adPreference.getPrfInvGlCoaVarianceAccount() != null && !adPreference.getPrfInvGlCoaVarianceAccount().equals(new Integer(0))) {

	        	try {

	        		LocalGlChartOfAccount glCoaVarianceAccount = glChartOfAccountHome.findByPrimaryKey(adPreference.getPrfInvGlCoaVarianceAccount() == null ? new Integer(1) : adPreference.getPrfInvGlCoaVarianceAccount());
	        		mdetails.setPrfInvGlCoaVarianceAccountNumber(glCoaVarianceAccount.getCoaAccountNumber());
	        		mdetails.setPrfInvGlCoaVarianceAccountDescription(glCoaVarianceAccount.getCoaAccountDescription());

	        	} catch (FinderException ex) {

	        	}

	        }

	       /* if (adPreference.getPrfInvSalesTaxCodeAccount() != null && !adPreference.getPrfInvSalesTaxCodeAccount().equals(new Integer(0))) {

	        	try {

	        		LocalGlChartOfAccount glCoaSalesTaxAccount = glChartOfAccountHome.findByPrimaryKey(
	        				adPreference.getPrfInvSalesTaxCodeAccount());
	        		mdetails.setPrfInvGlCoaSalesTaxAccountNumber(glCoaSalesTaxAccount.getCoaAccountNumber());
	        		mdetails.setPrfInvGlCoaSalesTaxAccountDescription(glCoaSalesTaxAccount.getCoaAccountDescription());

	        	} catch (FinderException ex) {

	        	}

	        }

	        if (adPreference.getPrfInvPreparedFoodTaxCodeAccount() != null &&
	        		!adPreference.getPrfInvPreparedFoodTaxCodeAccount().equals(new Integer(0))) {

	        	try {

	        		LocalGlChartOfAccount glCoaPreparedFoodTaxAccount = glChartOfAccountHome.findByPrimaryKey(
	        				adPreference.getPrfInvPreparedFoodTaxCodeAccount());
	        		mdetails.setPrfInvGlCoaPreparedFoodTaxAccountNumber(glCoaPreparedFoodTaxAccount.getCoaAccountNumber());
	        		mdetails.setPrfInvGlCoaPreparedFoodTaxAccountDescription(glCoaPreparedFoodTaxAccount.getCoaAccountDescription());

	        	} catch (FinderException ex) {

	        	}

	        }*/

	        return mdetails;

        } catch (Exception ex) {

        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());

        }

    }

    /**
     * @ejb:interface-method view-type="remote"
     **/
    public void saveAdPrfEntry(com.util.AdModPreferenceDetails mdetails, String WTC_NM, Integer AD_CMPNY)
        throws GlobalRecordAlreadyExistException,
        AdPRFCoaGlAccruedVatAccountNotFoundException,
        AdPRFCoaGlPettyCashAccountNotFoundException,
        AdPRFCoaGlPOSAdjustmentAccountNotFoundException,
        AdPRFCoaGlPosDiscountAccountNotFoundException,
        AdPRFCoaGlPosGiftCertificateAccountNotFoundException,
        AdPRFCoaGlPosServiceChargeAccountNotFoundException,
        AdPRFCoaGlPosDineInChargeAccountNotFoundException,
        AdPRFCoaGlCustomerDepositAccountNotFoundException {

        Debug.print("AdPreferenceControllerBean saveAdPrfEntry");

        LocalAdPreferenceHome adPreferenceHome = null;
        LocalArWithholdingTaxCodeHome arWithholdingTaxCodeHome = null;
        LocalGlChartOfAccountHome glChartOfAccountHome = null;

        // Initialize EJB Home

        try {Debug.print("try 1!");

            adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);
            arWithholdingTaxCodeHome = (LocalArWithholdingTaxCodeHome)EJBHomeFactory.
            	lookUpLocalHome(LocalArWithholdingTaxCodeHome.JNDI_NAME, LocalArWithholdingTaxCodeHome.class);
            glChartOfAccountHome = (LocalGlChartOfAccountHome)EJBHomeFactory.
            	lookUpLocalHome(LocalGlChartOfAccountHome.JNDI_NAME, LocalGlChartOfAccountHome.class);

        } catch (NamingException ex) {

            throw new EJBException(ex.getMessage());

        }

        LocalGlChartOfAccount glAccruedVatAccount = null;
        LocalGlChartOfAccount glPettyCashAccount = null;
        LocalGlChartOfAccount glPosAdjustmentAccount = null;
        LocalGlChartOfAccount glGeneralAdjustmentAccount = null;
        LocalGlChartOfAccount glIssuanceAdjustmentAccount = null;
        LocalGlChartOfAccount glProductionAdjustmentAccount = null;
        LocalGlChartOfAccount glWastageAdjustmentAccount = null;
        LocalGlChartOfAccount glPosDiscountAccount = null;
        LocalGlChartOfAccount glPosGiftCertificateAccount = null;
        LocalGlChartOfAccount glPosServiceChargeAccount = null;
        LocalGlChartOfAccount glPosDineInChargeAccount = null;
        LocalGlChartOfAccount glCoaCustomerDepositAccount = null;

        try {Debug.print("try 2!");

        	// update preference

        	LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);

                adPreference.setPrfAllowSuspensePosting(mdetails.getPrfAllowSuspensePosting());
                adPreference.setPrfGlJournalLineNumber(mdetails.getPrfGlJournalLineNumber());
                adPreference.setPrfApJournalLineNumber(mdetails.getPrfApJournalLineNumber());
                adPreference.setPrfArInvoiceLineNumber(mdetails.getPrfArInvoiceLineNumber());
                adPreference.setPrfApWTaxRealization(mdetails.getPrfApWTaxRealization());
                adPreference.setPrfArWTaxRealization(mdetails.getPrfArWTaxRealization());
                adPreference.setPrfEnableGlJournalBatch(mdetails.getPrfEnableGlJournalBatch());
                adPreference.setPrfEnableGlRecomputeCoaBalance(mdetails.getPrfEnableGlRecomputeCoaBalance());

                adPreference.setPrfEnableApVoucherBatch(mdetails.getPrfEnableApVoucherBatch());
                adPreference.setPrfEnableApPOBatch(mdetails.getPrfEnableApPOBatch());
                adPreference.setPrfEnableApCheckBatch(mdetails.getPrfEnableApCheckBatch());
                adPreference.setPrfEnableArInvoiceBatch(mdetails.getPrfEnableArInvoiceBatch());
                adPreference.setPrfEnableArInvoiceInterestGeneration(mdetails.getPrfEnableArInvoiceInterestGeneration());
                adPreference.setPrfEnableArReceiptBatch(mdetails.getPrfEnableArReceiptBatch());
                adPreference.setPrfEnableArMiscReceiptBatch(mdetails.getPrfEnableArMiscReceiptBatch());
                adPreference.setPrfApGlPostingType(mdetails.getPrfApGlPostingType());
                adPreference.setPrfArGlPostingType(mdetails.getPrfArGlPostingType());
                adPreference.setPrfCmGlPostingType(mdetails.getPrfCmGlPostingType());
                adPreference.setPrfCmUseBankForm(mdetails.getPrfCmUseBankForm());
                adPreference.setPrfInvInventoryLineNumber(mdetails.getPrfInvInventoryLineNumber());
                adPreference.setPrfInvQuantityPrecisionUnit(mdetails.getPrfInvQuantityPrecisionUnit());
                adPreference.setPrfInvCostPrecisionUnit(mdetails.getPrfInvCostPrecisionUnit());
                adPreference.setPrfInvGlPostingType(mdetails.getPrfInvGlPostingType());
                adPreference.setPrfGlPostingType(mdetails.getPrfGlPostingType());
                adPreference.setPrfApFindCheckDefaultType(mdetails.getPrfApFindCheckDefaultType());
                adPreference.setPrfArFindReceiptDefaultType(mdetails.getPrfArFindReceiptDefaultType());
                adPreference.setPrfInvEnableShift(mdetails.getPrfInvEnableShift());
                adPreference.setPrfEnableInvBUABatch(mdetails.getPrfEnableInvBUABatch());
                adPreference.setPrfApUseAccruedVat(mdetails.getPrfApUseAccruedVat());
                adPreference.setPrfApDefaultCheckDate(mdetails.getPrfApDefaultCheckDate());
                adPreference.setPrfApDefaultChecker(mdetails.getPrfApDefaultChecker());
                adPreference.setPrfApDefaultApprover(mdetails.getPrfApDefaultApprover());
                adPreference.setPrfApUseSupplierPulldown(mdetails.getPrfApUseSupplierPulldown());
                adPreference.setPrfArUseCustomerPulldown(mdetails.getPrfArUseCustomerPulldown());
                adPreference.setPrfApAutoGenerateSupplierCode(mdetails.getPrfApAutoGenerateSupplierCode());
                adPreference.setPrfArAutoGenerateCustomerCode(mdetails.getPrfArAutoGenerateCustomerCode());
                adPreference.setPrfApReferenceNumberValidation(mdetails.getPrfApReferenceNumberValidation());
                adPreference.setPrfApCheckVoucherDataSource(mdetails.getPrfApCheckVoucherDataSource());
                adPreference.setPrfApDefaultPrTax(mdetails.getPrfApDefaultPrTax());
                adPreference.setPrfApDefaultPrCurrency(mdetails.getPrfApDefaultPrCurrency());
                adPreference.setPrfArSalesInvoiceDataSource(mdetails.getPrfArSalesInvoiceDataSource());
                adPreference.setPrfArAutoComputeCogs(mdetails.getPrfArAutoComputeCogs());

                adPreference.setPrfArMonthlyInterestRate(mdetails.getPrfArMonthlyInterestRate());
                adPreference.setPrfApAgingBucket(mdetails.getPrfApAgingBucket());
                adPreference.setPrfArAgingBucket(mdetails.getPrfArAgingBucket());
                adPreference.setPrfArAllowPriorDate(mdetails.getPrfArAllowPriorDate());
                adPreference.setPrfApShowPrCost(mdetails.getPrfApShowPrCost());
                adPreference.setPrfApDebitMemoOverrideCost(mdetails.getPrfApDebitMemoOverrideCost());
                adPreference.setPrfGlYearEndCloseRestriction(mdetails.getPrfGlYearEndCloseRestriction());
                adPreference.setPrfArEnablePaymentTerm(mdetails.getPrfArEnablePaymentTerm());
                adPreference.setPrfArDisableSalesPrice(mdetails.getPrfArDisableSalesPrice());
                adPreference.setPrfInvItemLocationShowAll(mdetails.getPrfInvItemLocationShowAll());
                adPreference.setPrfInvItemLocationAddByItemList(mdetails.getPrfInvItemLocationAddByItemList());
                adPreference.setPrfArValidateCustomerEmail(mdetails.getPrfArValidateCustomerEmail());
                adPreference.setPrfArCheckInsufficientStock(mdetails.getPrfArCheckInsufficientStock());
                adPreference.setPrfArDetailedReceivable(mdetails.getPrfArDetailedReceivable());
                adPreference.setPrfAdDisableMultipleLogin(mdetails.getPrfAdDisableMultipleLogin());
                adPreference.setPrfAdEnableEmailNotification(mdetails.getPrfAdEnableEmailNotification());
                
                adPreference.setPrfArSoSalespersonRequired(mdetails.getPrfArSoSalespersonRequired());
                adPreference.setPrfArInvcSalespersonRequired(mdetails.getPrfArInvcSalespersonRequired());
    	       
                
                adPreference.setPrfMailHost(mdetails.getPrfMailHost());
                adPreference.setPrfMailSocketFactoryPort(mdetails.getPrfMailSocketFactoryPort());
                adPreference.setPrfMailPort(mdetails.getPrfMailPort());
                adPreference.setPrfMailFrom(mdetails.getPrfMailFrom());
                adPreference.setPrfMailAuthenticator(mdetails.getPrfMailAuthenticator());
                adPreference.setPrfMailPassword(mdetails.getPrfMailPassword());
                adPreference.setPrfMailTo(mdetails.getPrfMailTo());
                adPreference.setPrfMailCc(mdetails.getPrfMailCc());
                adPreference.setPrfMailBcc(mdetails.getPrfMailBcc());
                adPreference.setPrfMailConfig(mdetails.getPrfMailConfig());
                adPreference.setPrfCentralWarehouseBranch(mdetails.getPrfCentralWarehouseBranch());
                adPreference.setPrfCentralWarehouseLocation(mdetails.getPrfCentralWarehouseLocation());

                if(mdetails.getPrfApAutoGenerateSupplierCode() == EJBCommon.TRUE) {
                    adPreference.setPrfApNextSupplierCode(mdetails.getPrfApNextSupplierCode());
                } else if(mdetails.getPrfApAutoGenerateSupplierCode() == EJBCommon.FALSE) {
                    adPreference.setPrfApNextSupplierCode(null);
                }

                if(mdetails.getPrfArAutoGenerateCustomerCode() == EJBCommon.TRUE) {
                    adPreference.setPrfArNextCustomerCode(mdetails.getPrfArNextCustomerCode());
                } else if(mdetails.getPrfArAutoGenerateCustomerCode() == EJBCommon.FALSE) {
                    adPreference.setPrfArNextCustomerCode(null);
                }

                if (mdetails.getPrfApGlCoaAccruedVatAccountNumber() != null && mdetails.getPrfApGlCoaAccruedVatAccountNumber().length() > 0) {

        	        try {Debug.print("try 3!");

        	        	glAccruedVatAccount = glChartOfAccountHome.findByCoaAccountNumber(mdetails.getPrfApGlCoaAccruedVatAccountNumber(), AD_CMPNY);
        	        	adPreference.setPrfApGlCoaAccruedVatAccount(glAccruedVatAccount.getCoaCode());

        	        } catch (FinderException ex) {

        	        	throw new AdPRFCoaGlAccruedVatAccountNotFoundException();

        	        }

                } else {

                	adPreference.setPrfApGlCoaAccruedVatAccount(null);

                }

                if (mdetails.getPrfApGlCoaPettyCashAccountNumber() != null && mdetails.getPrfApGlCoaPettyCashAccountNumber().length() > 0) {

        	        try {Debug.print("try 4!");

        	        	glPettyCashAccount = glChartOfAccountHome.findByCoaAccountNumber(mdetails.getPrfApGlCoaPettyCashAccountNumber(), AD_CMPNY);
        	        	adPreference.setPrfApGlCoaPettyCashAccount(glPettyCashAccount.getCoaCode());

        	        } catch (FinderException ex) {

        	        	throw new AdPRFCoaGlPettyCashAccountNotFoundException();

        	        }

                } else {

                	adPreference.setPrfApGlCoaPettyCashAccount(null);

                }

                if (mdetails.getPrfInvGlCoaGeneralAdjustmentAccountNumber() != null && mdetails.getPrfInvGlCoaGeneralAdjustmentAccountNumber().length() > 0) {

        	        try {Debug.print("try 5!");

        	        	glGeneralAdjustmentAccount = glChartOfAccountHome.findByCoaAccountNumber(mdetails.getPrfInvGlCoaGeneralAdjustmentAccountNumber(), AD_CMPNY);
        	        	Debug.print("try 5.1!");
        	        	adPreference.setPrfInvGeneralAdjustmentAccount(glGeneralAdjustmentAccount.getCoaCode());

        	        } catch (FinderException ex) {

        	        	ex.printStackTrace();

        	        }

                } else {

                	adPreference.setPrfInvGeneralAdjustmentAccount(null);

                }

                if (mdetails.getPrfInvGlCoaIssuanceAdjustmentAccountNumber() != null && mdetails.getPrfInvGlCoaIssuanceAdjustmentAccountNumber().length() > 0) {

        	        try {Debug.print("try 6!");

        	        	glIssuanceAdjustmentAccount = glChartOfAccountHome.findByCoaAccountNumber(mdetails.getPrfInvGlCoaIssuanceAdjustmentAccountNumber(), AD_CMPNY);
        	        	adPreference.setPrfInvIssuanceAdjustmentAccount(glIssuanceAdjustmentAccount.getCoaCode());

        	        } catch (FinderException ex) {

        	        	ex.printStackTrace();

        	        }

                } else {

                	adPreference.setPrfInvIssuanceAdjustmentAccount(null);

                }

                if (mdetails.getPrfInvGlCoaProductionAdjustmentAccountNumber() != null && mdetails.getPrfInvGlCoaProductionAdjustmentAccountNumber().length() > 0) {

        	        try {Debug.print("try 7!");

        	        	glProductionAdjustmentAccount = glChartOfAccountHome.findByCoaAccountNumber(mdetails.getPrfInvGlCoaProductionAdjustmentAccountNumber(), AD_CMPNY);
        	        	adPreference.setPrfInvProductionAdjustmentAccount(glProductionAdjustmentAccount.getCoaCode());

        	        } catch (FinderException ex) {

        	        	ex.printStackTrace();

        	        }

                } else {

                	adPreference.setPrfInvProductionAdjustmentAccount(null);

                }

                if (mdetails.getPrfInvGlCoaWastageAdjustmentAccountNumber() != null && mdetails.getPrfInvGlCoaWastageAdjustmentAccountNumber().length() > 0) {

        	        try {Debug.print("try 8!");

        	        	glWastageAdjustmentAccount = glChartOfAccountHome.findByCoaAccountNumber(mdetails.getPrfInvGlCoaWastageAdjustmentAccountNumber(), AD_CMPNY);
        	        	adPreference.setPrfInvWastageAdjustmentAccount(glWastageAdjustmentAccount.getCoaCode());

        	        } catch (FinderException ex) {

        	        	ex.printStackTrace();

        	        }

                } else {

                	adPreference.setPrfInvProductionAdjustmentAccount(null);

                }

                if (adPreference.getArWithholdingTaxCode() != null) {
            		adPreference.getArWithholdingTaxCode().dropAdPreference(adPreference);
            	}

                if (WTC_NM != null && WTC_NM.length() > 0) {

                	LocalArWithholdingTaxCode arWithholdingTaxCode = arWithholdingTaxCodeHome.findByWtcName(WTC_NM, AD_CMPNY);
                	arWithholdingTaxCode.addAdPreference(adPreference);

                }

                adPreference.setPrfInvEnablePosAutoPostUpload(mdetails.getPrfInvEnablePosAutoPostUpload());
                adPreference.setPrfInvEnablePosIntegration(mdetails.getPrfInvEnablePosIntegration());
                if(adPreference.getPrfInvEnablePosIntegration() == EJBCommon.TRUE) {

                	try {Debug.print("try 9!");

	                	glPosAdjustmentAccount = glChartOfAccountHome.findByCoaAccountNumber(mdetails.getPrfInvGlCoaPosAdjustmentAccountNumber(), AD_CMPNY);
	                	adPreference.setPrfInvPosAdjustmentAccount(glPosAdjustmentAccount.getCoaCode());

                	} catch(FinderException ex) {

                		throw new AdPRFCoaGlPOSAdjustmentAccountNotFoundException();

                	}

                } else {

                	adPreference.setPrfInvPosAdjustmentAccount(null);

                }


                if (mdetails.getPrfInvGlCoaAdjustmentRequestAccountNumber()!=null) {
	            	glPosAdjustmentAccount = glChartOfAccountHome.findByCoaAccountNumber(mdetails.getPrfInvGlCoaAdjustmentRequestAccountNumber(), AD_CMPNY);
	            	adPreference.setPrfInvPosAdjustmentAccount(glPosAdjustmentAccount.getCoaCode());
                }

                if (mdetails.getPrfMiscGlCoaPosDiscountAccountNumber() != null && mdetails.getPrfMiscGlCoaPosDiscountAccountNumber().length() > 0) {

        	        try {Debug.print("try 10!");

        	        	glPosDiscountAccount = glChartOfAccountHome.findByCoaAccountNumber(mdetails.getPrfMiscGlCoaPosDiscountAccountNumber(), AD_CMPNY);
        	        	adPreference.setPrfMiscPosDiscountAccount(glPosDiscountAccount.getCoaCode());

        	        } catch (FinderException ex) {

        	        	throw new AdPRFCoaGlPosDiscountAccountNotFoundException();

        	        }

                } else {

                	adPreference.setPrfMiscPosDiscountAccount(null);

                }

                if (mdetails.getPrfMiscGlCoaPosGiftCertificateAccountNumber() != null && mdetails.getPrfMiscGlCoaPosGiftCertificateAccountNumber().length() > 0) {

        	        try {Debug.print("try 11!");

        	        	glPosGiftCertificateAccount = glChartOfAccountHome.findByCoaAccountNumber(mdetails.getPrfMiscGlCoaPosGiftCertificateAccountNumber(), AD_CMPNY);
        	        	adPreference.setPrfMiscPosGiftCertificateAccount(glPosGiftCertificateAccount.getCoaCode());

        	        } catch (FinderException ex) {

        	        	throw new AdPRFCoaGlPosGiftCertificateAccountNotFoundException();

        	        }

                } else {

                	adPreference.setPrfMiscPosGiftCertificateAccount(null);

                }

                if (mdetails.getPrfMiscGlCoaPosServiceChargeAccountNumber() != null && mdetails.getPrfMiscGlCoaPosServiceChargeAccountNumber().length() > 0) {

        	        try {Debug.print("try 12!");

        	        	glPosServiceChargeAccount = glChartOfAccountHome.findByCoaAccountNumber(mdetails.getPrfMiscGlCoaPosServiceChargeAccountNumber(), AD_CMPNY);
        	        	adPreference.setPrfMiscPosServiceChargeAccount(glPosServiceChargeAccount.getCoaCode());

        	        } catch (FinderException ex) {

        	        	throw new AdPRFCoaGlPosServiceChargeAccountNotFoundException();

        	        }

                } else {

                	adPreference.setPrfMiscPosServiceChargeAccount(null);

                }

                if (mdetails.getPrfMiscGlCoaPosDineInChargeAccountNumber() != null && mdetails.getPrfMiscGlCoaPosDineInChargeAccountNumber().length() > 0) {

        	        try {Debug.print("try 13!");

        	        	glPosDineInChargeAccount = glChartOfAccountHome.findByCoaAccountNumber(mdetails.getPrfMiscGlCoaPosDineInChargeAccountNumber(), AD_CMPNY);
        	        	adPreference.setPrfMiscPosDineInChargeAccount(glPosDineInChargeAccount.getCoaCode());

        	        } catch (FinderException ex) {

        	        	throw new AdPRFCoaGlPosDineInChargeAccountNotFoundException();

        	        }

                } else {

                	adPreference.setPrfMiscPosDineInChargeAccount(null);

                }

                if (mdetails.getPrfArGlCoaCustomerDepositAccountNumber() != null && mdetails.getPrfArGlCoaCustomerDepositAccountNumber().length() > 0) {

        	        try {Debug.print("try 14!");

        	        	glCoaCustomerDepositAccount = glChartOfAccountHome.findByCoaAccountNumber(mdetails.getPrfArGlCoaCustomerDepositAccountNumber(), AD_CMPNY);
        	        	adPreference.setPrfArGlCoaCustomerDepositAccount(glCoaCustomerDepositAccount.getCoaCode());

        	        } catch (FinderException ex) {

        	        	throw new AdPRFCoaGlCustomerDepositAccountNotFoundException();

        	        }

                } else {

                	adPreference.setPrfArGlCoaCustomerDepositAccount(null);

                }

                if (mdetails.getPrfInvGlCoaVarianceAccountNumber() != null && mdetails.getPrfInvGlCoaVarianceAccountNumber().length() > 0) {

        	        try {Debug.print("try 15!");

        	        	LocalGlChartOfAccount glCoaVarianceAccount = glChartOfAccountHome.findByCoaAccountNumber(
        	        			mdetails.getPrfInvGlCoaVarianceAccountNumber(), AD_CMPNY);
        	        	adPreference.setPrfInvGlCoaVarianceAccount(glCoaVarianceAccount.getCoaCode());

        	        } catch (FinderException ex) {

        	        	throw new AdPRFCoaGlVarianceAccountNotFoundException();

        	        }

                } else {

                	adPreference.setPrfInvGlCoaVarianceAccount(null);

                }

               /* if (mdetails.getPrfInvGlCoaSalesTaxAccountNumber() != null && mdetails.getPrfInvGlCoaSalesTaxAccountNumber().length() > 0) {

                	try {

                		LocalGlChartOfAccount glCoaSalesTaxAccount = glChartOfAccountHome.findByCoaAccountNumber(
                				mdetails.getPrfInvGlCoaSalesTaxAccountNumber(), AD_CMPNY);
                		adPreference.setPrfInvSalesTaxCodeAccount(glCoaSalesTaxAccount.getCoaCode());

                	} catch (FinderException ex) {

                		throw new AdPRFCoaInvSalesTaxAccountNotFoundException();

                	}

                } else {

                	adPreference.setPrfInvSalesTaxCodeAccount(null);

                }

                if (mdetails.getPrfInvGlCoaPreparedFoodTaxAccountNumber() != null &&
                		mdetails.getPrfInvGlCoaPreparedFoodTaxAccountNumber().length() > 0) {

                	try {

                		LocalGlChartOfAccount glCoaPreparedFoodTaxAccount = glChartOfAccountHome.findByCoaAccountNumber(
                				mdetails.getPrfInvGlCoaPreparedFoodTaxAccountNumber(), AD_CMPNY);
                		adPreference.setPrfInvPreparedFoodTaxCodeAccount(glCoaPreparedFoodTaxAccount.getCoaCode());

                	} catch (FinderException ex) {

                		throw new AdPRFCoaInvPreparedFoodTaxAccountNotFoundException();

                	}

                } else {

                	adPreference.setPrfInvPreparedFoodTaxCodeAccount(null);

                }*/

        } catch (AdPRFCoaGlAccruedVatAccountNotFoundException ex) {

        	getSessionContext().setRollbackOnly();
        	throw ex;

        } catch (AdPRFCoaGlPettyCashAccountNotFoundException ex) {

        	getSessionContext().setRollbackOnly();
        	throw ex;

        } catch (AdPRFCoaGlPOSAdjustmentAccountNotFoundException ex) {

        	getSessionContext().setRollbackOnly();
        	throw ex;

        } catch (AdPRFCoaGlPosDiscountAccountNotFoundException ex) {

        	getSessionContext().setRollbackOnly();
        	throw ex;

        } catch (AdPRFCoaGlPosGiftCertificateAccountNotFoundException ex) {

        	getSessionContext().setRollbackOnly();
        	throw ex;

        } catch (AdPRFCoaGlPosServiceChargeAccountNotFoundException ex) {

        	getSessionContext().setRollbackOnly();
        	throw ex;

        } catch (AdPRFCoaGlPosDineInChargeAccountNotFoundException ex) {

        	getSessionContext().setRollbackOnly();
        	throw ex;

        } catch (AdPRFCoaGlCustomerDepositAccountNotFoundException ex) {

        	getSessionContext().setRollbackOnly();
        	throw ex;

        } catch (Exception ex) {
        	System.out.print("ERROR! - " + ex);
        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());

        }

    }

     /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList getArWtcAll(Integer AD_CMPNY) {

        Debug.print("AdPreferenceControllerBean getArWtcAll");

        LocalArWithholdingTaxCodeHome arWithholdingTaxCodeHome = null;

        ArrayList list = new ArrayList();

        // Initialize EJB Home

        try {

            arWithholdingTaxCodeHome = (LocalArWithholdingTaxCodeHome)EJBHomeFactory.
                lookUpLocalHome(LocalArWithholdingTaxCodeHome.JNDI_NAME, LocalArWithholdingTaxCodeHome.class);

        } catch (NamingException ex) {

            throw new EJBException(ex.getMessage());

        }

        try {

        	Collection arWithholdingTaxCodes = arWithholdingTaxCodeHome.findEnabledWtcAll(AD_CMPNY);

        	Iterator i = arWithholdingTaxCodes.iterator();

        	while (i.hasNext()) {

        		LocalArWithholdingTaxCode arWithholdingTaxCode = (LocalArWithholdingTaxCode)i.next();

        		list.add(arWithholdingTaxCode.getWtcName());

        	}

        	return list;

        } catch (Exception ex) {

        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());

        }

    }

    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList getArTcAll(Integer AD_CMPNY) {

        Debug.print("AdPreferenceControllerBean getArTcAll");

        LocalArTaxCodeHome arTaxCodeHome = null;

        ArrayList list = new ArrayList();

        // Initialize EJB Home

        try {

        	arTaxCodeHome = (LocalArTaxCodeHome)EJBHomeFactory.
                lookUpLocalHome(LocalArTaxCodeHome.JNDI_NAME, LocalArTaxCodeHome.class);

        } catch (NamingException ex) {

            throw new EJBException(ex.getMessage());

        }

        try {

        	Collection arTaxCodes = arTaxCodeHome.findEnabledTcAll(AD_CMPNY);

        	Iterator i = arTaxCodes.iterator();

        	while (i.hasNext()) {

        		LocalArTaxCode arTaxCode = (LocalArTaxCode)i.next();

        		list.add(arTaxCode.getTcName());

        	}

        	return list;

        } catch (Exception ex) {

        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());

        }

    }

    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList getGlFcAll(Integer AD_CMPNY) {

        Debug.print("AdPreferenceControllerBean getGlFcAll");

        LocalGlFunctionalCurrencyHome glFunctionalCurrencyHome = null;

        ArrayList list = new ArrayList();

        // Initialize EJB Home

        try {

            glFunctionalCurrencyHome = (LocalGlFunctionalCurrencyHome)EJBHomeFactory.
                lookUpLocalHome(LocalGlFunctionalCurrencyHome.JNDI_NAME, LocalGlFunctionalCurrencyHome.class);

        } catch (NamingException ex) {

            throw new EJBException(ex.getMessage());

        }

        try {

            Collection glFunctionalCurrencies = glFunctionalCurrencyHome.findFcAllEnabled(new java.util.Date(), AD_CMPNY);

	        Iterator i = glFunctionalCurrencies.iterator();

	        while (i.hasNext()) {

	        	LocalGlFunctionalCurrency glFunctionalCurrency = (LocalGlFunctionalCurrency)i.next();

	        	list.add(glFunctionalCurrency.getFcName());

	        }

	        return list;

        } catch (Exception ex) {

        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());

        }

    }
    
    
    
    /**
   * @ejb:interface-method view-type="remote"
   * @jboss:method-attributes read-only="true"
   **/
    public ArrayList getAdBranchAll(Integer AD_CMPNY) {

        Debug.print("AdPreferenceControllerBean getAdBranchAll");

        LocalAdBranchHome adBranchHome = null;

        ArrayList list = new ArrayList();

        // Initialize EJB Home

        try {

            adBranchHome = (LocalAdBranchHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdBranchHome.JNDI_NAME, LocalAdBranchHome.class);

        } catch (NamingException ex) {

            throw new EJBException(ex.getMessage());

        }

        try {

            
            Collection adBranches = adBranchHome.findBrAll(AD_CMPNY);

            Iterator i = adBranches.iterator();

            while (i.hasNext()) {

                LocalAdBranch adBranch = (LocalAdBranch)i.next();

                list.add(adBranch.getBrBranchCode());

            }

            return list;

        } catch (Exception ex) {

            Debug.printStackTrace(ex);
            throw new EJBException(ex.getMessage());

        }

    }
    
    
      /**
   * @ejb:interface-method view-type="remote"
   * @jboss:method-attributes read-only="true"
   **/
    public ArrayList getInvLocationAll(Integer AD_CMPNY) {

        Debug.print("AdPreferenceControllerBean getInvLocationAll");

        LocalInvLocationHome invLocationHome = null;

        ArrayList list = new ArrayList();

        // Initialize EJB Home

        try {

            invLocationHome = (LocalInvLocationHome)EJBHomeFactory.
                lookUpLocalHome(LocalInvLocationHome.JNDI_NAME, LocalInvLocationHome.class);

        } catch (NamingException ex) {

            throw new EJBException(ex.getMessage());

        }

        try {

            
            Collection invLocations = invLocationHome.findLocAll(AD_CMPNY);

            Iterator i = invLocations.iterator();

            while (i.hasNext()) {

                LocalInvLocation invLocation = (LocalInvLocation)i.next();

                list.add(invLocation.getLocName());

            }

            return list;

        } catch (Exception ex) {

            Debug.printStackTrace(ex);
            throw new EJBException(ex.getMessage());

        }

    }


    // SessionBean methods

    /**
     * @ejb:create-method view-type="remote"
     **/
    public void ejbCreate() throws CreateException {

       Debug.print("AdPreferenceControllerBean ejbCreate");

    }
}
