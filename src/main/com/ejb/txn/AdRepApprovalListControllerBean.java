
/*
 * AdRepApprovalListControllerBean.java
 *
 * Created on Jun 22, 2005, 04:48 PM
 *
 * @author  Jolly T. Martin
 */

package com.ejb.txn;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.naming.NamingException;

import com.ejb.ad.LocalAdAmountLimit;
import com.ejb.ad.LocalAdApproval;
import com.ejb.ad.LocalAdApprovalCoaLineHome;
import com.ejb.ad.LocalAdApprovalDocument;
import com.ejb.ad.LocalAdApprovalDocumentHome;
import com.ejb.ad.LocalAdApprovalHome;
import com.ejb.ad.LocalAdApprovalUser;
import com.ejb.ad.LocalAdCompany;
import com.ejb.ad.LocalAdCompanyHome;
import com.ejb.exception.GlobalNoRecordFoundException;
import com.util.AbstractSessionBean;
import com.util.AdApprovalDetails;
import com.util.AdCompanyDetails;
import com.util.AdRepApprovalListDetails;
import com.util.Debug;
import com.util.EJBHomeFactory;

/**
 * @ejb:bean name="AdRepApprovalListControllerEJB"
 *           display-name="Used for viewing approval lists"
 *           type="Stateless"
 *           view-type="remote"
 *           jndi-name="ejb/AdRepApprovalListControllerEJB"
 *
 * @ejb:interface remote-class="com.ejb.txn.AdRepApprovalListController"
 *                extends="javax.ejb.EJBObject"
 *
 * @ejb:home remote-class="com.ejb.txn.AdRepApprovalListControllerHome"
 *           extends="javax.ejb.EJBHome"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="aduser"
 *                        role-link="aduserlink"
 *
 * @ejb:permission role-name="aduser"
 * 
*/

public class AdRepApprovalListControllerBean extends AbstractSessionBean {

	
	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public ArrayList getAdApprovalDocumentAll(Integer AD_CMPNY) {
		
		Debug.print("AdRepApprovalListControllerBean getAdApprovalDocumentAll");
		
		LocalAdApprovalDocumentHome adApprovalDocumentHome = null;               
		
		ArrayList list = new ArrayList();
		
		// Initialize EJB Home
		
		try {
			
			adApprovalDocumentHome = (LocalAdApprovalDocumentHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdApprovalDocumentHome.JNDI_NAME, LocalAdApprovalDocumentHome.class);
			
		} catch (NamingException ex) {
			
			throw new EJBException(ex.getMessage());
			
		}        
		
		try {
			
			Collection adApprovalDocuments = adApprovalDocumentHome.findAdcAll(AD_CMPNY);
			
			Iterator i = adApprovalDocuments.iterator();
			
			while (i.hasNext()) {
				
				LocalAdApprovalDocument adApprovalDocument = (LocalAdApprovalDocument)i.next();
				
				list.add(adApprovalDocument.getAdcType());
				
			}
			
			return list;
			
		} catch (Exception ex) {
			
			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());
			
		}
		
	}
	
    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList executeAdRepApprovalList(HashMap criteria, Integer AD_CMPNY) 
        throws GlobalNoRecordFoundException {
                    
        Debug.print("AdRepApprovalListControllerBean executeAdRepApprovalList");
        
        LocalAdApprovalDocumentHome adApprovalDocumentHome = null;
        LocalAdApprovalHome adApprovalHome = null;
        LocalAdApprovalCoaLineHome adApprovalCoaLineHome = null;
        
        ArrayList list = new ArrayList();
        
        //initialized EJB Home
        
        try {
            
        	adApprovalDocumentHome = (LocalAdApprovalDocumentHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdApprovalDocumentHome.JNDI_NAME, LocalAdApprovalDocumentHome.class);
        	adApprovalHome = (LocalAdApprovalHome)EJBHomeFactory.
            	lookUpLocalHome(LocalAdApprovalHome.JNDI_NAME, LocalAdApprovalHome.class);
        	adApprovalCoaLineHome = (LocalAdApprovalCoaLineHome)EJBHomeFactory.
			   	lookUpLocalHome(LocalAdApprovalCoaLineHome.JNDI_NAME, LocalAdApprovalCoaLineHome.class);
           
        } catch (NamingException ex) 	{
            
            throw new EJBException(ex.getMessage());
            
        }
        
		try { 
           
		  StringBuffer jbossQl = new StringBuffer();
		  jbossQl.append("SELECT OBJECT(adc) FROM AdApprovalDocument adc ");
		  
	      boolean firstArgument = true;
	      short ctr = 0;
		  int criteriaSize = criteria.size();	      
	      
	      Object obj[];	      
		
		  // Allocate the size of the object parameter
		
	      if (criteria.containsKey("documentType")) {
	      	
	      	 criteriaSize--;
	      	 
	      }
	      
	      obj = new Object[criteriaSize];
		       	      
		  if (criteria.containsKey("documentType")) {
		  	
		  	 if (!firstArgument) {
		  	 
		  	    jbossQl.append("AND ");	
		  	 	
		     } else {
		     	
		     	firstArgument = false;
		     	jbossQl.append("WHERE ");
		     	
		     }
		     
		  	 jbossQl.append("adc.adcType LIKE '%" + (String)criteria.get("documentType") + "%' ");
		  	 
		  }
		 				  
		  if (!firstArgument) {
      	 	jbossQl.append("AND ");
      	  } else {
      	 	firstArgument = false;
      	 	jbossQl.append("WHERE ");
      	  }

		  jbossQl.append("adc.adcAdCompany=" + AD_CMPNY + " ORDER BY adc.adcType ");
  	
		  // approval details
		  
		  AdApprovalDetails adApprovalDetails = this.getApprovalDetails(AD_CMPNY);
		  
		  // approval documents
		  
		  Collection adApprovalDocuments = adApprovalDocumentHome.getAdcByCriteria(jbossQl.toString(), obj);	         
		  
		  if (adApprovalDocuments.size() == 0)
		     throw new GlobalNoRecordFoundException();
		  
		  Iterator itrAD = adApprovalDocuments.iterator();
		
		  while (itrAD.hasNext()) {
		  	
		  	  LocalAdApprovalDocument adApprovalDocument = (LocalAdApprovalDocument)itrAD.next();		
		  	  
		  	  // approval enable
		  	  
		  	  boolean enable = this.checkDocumentEnable(adApprovalDetails, adApprovalDocument.getAdcType());
		  	  
		  	  // amount limit/s
		  	  
		  	  Collection amountLimits = adApprovalDocument.getAdAmountLimits();
		  	  
		  	  Iterator itrAL = amountLimits.iterator();
		  	  
		  	  while(itrAL.hasNext()) {
		  	  	
		  	  	LocalAdAmountLimit adAmountLimit = (LocalAdAmountLimit)itrAL.next();
		  	  	
		  	  	// approval users
		  	  	
		  	  	Collection adApprovalUsers = adAmountLimit.getAdApprovalUsers();
		  	  	
		  	  	Iterator itrAU = adApprovalUsers.iterator();
		  	  	
		  	  	while(itrAU.hasNext()) {
		  	  		
		  	  		LocalAdApprovalUser adApprovalUser = (LocalAdApprovalUser)itrAU.next();
		  	  		
		  	  		AdRepApprovalListDetails details = new AdRepApprovalListDetails();
		  	  		
		  	  		details.setAlDocumentType(adApprovalDocument.getAdcType());
		  	  		details.setAlAmount(adAmountLimit.getCalAmountLimit());
		  	  		details.setAlUserName(adApprovalUser.getAdUser().getUsrName());
		  	  		details.setAlUserType(adApprovalUser.getAuType());
		  	  		details.setAlEnable(enable);
		  	  		details.setAlAndOr(adAmountLimit.getCalAndOr());
		  	  		
		  	  		list.add(details);
		  	  		
		  	  	}
		  	  	
		  	  }
		  	  
		  }
			     
		  return list;
		      
	  } catch (GlobalNoRecordFoundException ex) {
	  	 
	  	  throw ex;
	  	
	  } catch (Exception ex) {
	  	

	  	  ex.printStackTrace();
	  	  throw new EJBException(ex.getMessage());
	  	
	  }
        
    }
    
	/**
	* @ejb:interface-method view-type="remote"
	* @jboss:method-attributes read-only="true"
	**/
	public com.util.AdCompanyDetails getAdCompany(Integer AD_CMPNY) {
	
	  Debug.print("AdRepApprovalListControllerBean getAdCompany");      
	  
	  LocalAdCompanyHome adCompanyHome = null;
	        
	  // Initialize EJB Home
	    
	  try {
	        
	       adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
	           lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
	        
	  } catch (NamingException ex) {
	        
	      throw new EJBException(ex.getMessage());
	        
	  }
	
	  try {
	      
	     LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
	     
	     AdCompanyDetails details = new AdCompanyDetails();
	     details.setCmpName(adCompany.getCmpName());
	     
	     return details;  	
	  	       	
	  } catch (Exception ex) {
	  	
	  	 Debug.printStackTrace(ex);
	  	 throw new EJBException(ex.getMessage());
	  	  
	  }
	  
	}    

	// private method
	
	private com.util.AdApprovalDetails getApprovalDetails(Integer AD_CMPNY) {
		
		LocalAdApprovalHome adApprovalHome = null;
		
		// Initialize EJB Home
		
		try {
			
			adApprovalHome = (LocalAdApprovalHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdApprovalHome.JNDI_NAME, LocalAdApprovalHome.class);
			
		} catch (NamingException ex) {
			
			throw new EJBException(ex.getMessage());
			
		}
		
		try {
			
			LocalAdApproval adApproval = adApprovalHome.findByAprAdCompany(AD_CMPNY);
			
			AdApprovalDetails details = new AdApprovalDetails();
			details.setAprEnableApCheck(adApproval.getAprEnableApCheck());
			details.setAprEnableApDebitMemo(adApproval.getAprEnableApDebitMemo());
			details.setAprEnableApVoucher(adApproval.getAprEnableApVoucher());
			details.setAprEnableArCreditMemo(adApproval.getAprEnableArCreditMemo());
			details.setAprEnableArInvoice(adApproval.getAprEnableArInvoice());
			details.setAprEnableArReceipt(adApproval.getAprEnableArReceipt());
			details.setAprEnableCmAdjustment(adApproval.getAprEnableCmAdjustment());
			details.setAprEnableCmFundTransfer(adApproval.getAprEnableCmFundTransfer());
			details.setAprEnableGlJournal(adApproval.getAprEnableGlJournal());
			details.setAprEnableInvAdjustment(adApproval.getAprEnableInvAdjustment());
			details.setAprEnableInvBuild(adApproval.getAprEnableInvBuild());
						
			return details;  	
			
		} catch (Exception ex) {
			
			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());
			
		}
		
	}
	
	private boolean checkDocumentEnable(AdApprovalDetails details, String documentType) {
		
		boolean enable = false;
		
		if(documentType.equals("GL JOURNAL")){
			
			if(details.getAprEnableGlJournal()==1)
				enable=true;
			
		}
			
		if(documentType.equals("AP VOUCHER")){
			
			if(details.getAprEnableApVoucher()==1)
				enable=true;
			
		}
		
		if(documentType.equals("AP DEBIT MEMO")){
			
			if(details.getAprEnableApDebitMemo()==1)
				enable=true;
			
		}
		
		if(documentType.equals("AP CHECK")){
			
			if(details.getAprEnableApCheck()==1)
				enable=true;
			
		}
		
		if(documentType.equals("AR INVOICE")){
			
			if(details.getAprEnableArInvoice()==1)
				enable=true;
			
		}
		
		if(documentType.equals("AR CREDIT MEMO")){
			
			if(details.getAprEnableArCreditMemo()==1)
				enable=true;
			
		}
		
		if(documentType.equals("AR RECEIPT")){
			
			if(details.getAprEnableArReceipt()==1)
				enable=true;
			
		}
		
		if(documentType.equals("CM ADJUSTMENT")){
			
			if(details.getAprEnableCmAdjustment()==1)
				enable=true;
			
		}
		
		if(documentType.equals("CM FUND TRANSFER")){
			
			if(details.getAprEnableCmFundTransfer()==1)
				enable=true;
			
		}
		
		if(documentType.equals("INV ADJUSTMENT")){
			
			if(details.getAprEnableInvAdjustment()==1)
				enable=true;
			
		}
		
		if(documentType.equals("INV BUILD ASSEMBLY")){
			
			if(details.getAprEnableInvBuild()==1)
				enable=true;
			
		}
		
		return enable;
			
	}
	
	
    // SessionBean methods

    /**
     * @ejb:create-method view-type="remote"
     **/
    public void ejbCreate() throws CreateException {

       Debug.print("AdRepApprovalListControllerBean ejbCreate");
      
    }
}
