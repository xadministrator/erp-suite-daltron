
/*
 * AdRepBankAccountListControllerBean.java
 *
 * Created on March 07, 2005, 02:55 PM
 *
 * @author  Neil Andrew M. Ajero
 */

package com.ejb.txn;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;
import javax.naming.NamingException;

import com.ejb.ad.LocalAdBankAccount;
import com.ejb.ad.LocalAdBankAccountBalance;
import com.ejb.ad.LocalAdBankAccountBalanceHome;
import com.ejb.ad.LocalAdBankAccountHome;
import com.ejb.ad.LocalAdBranch;
import com.ejb.ad.LocalAdBranchHome;
import com.ejb.ad.LocalAdBranchResponsibility;
import com.ejb.ad.LocalAdBranchResponsibilityHome;
import com.ejb.ad.LocalAdCompany;
import com.ejb.ad.LocalAdCompanyHome;
import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.gl.LocalGlChartOfAccount;
import com.ejb.gl.LocalGlChartOfAccountHome;
import com.util.AbstractSessionBean;
import com.util.AdBranchDetails;
import com.util.AdCompanyDetails;
import com.util.AdRepBankAccountListDetails;
import com.util.Debug;
import com.util.EJBHomeFactory;

/**
 * @ejb:bean name="AdRepBankAccountListControllerEJB"
 *           display-name="Used for viewing bank account lists"
 *           type="Stateless"
 *           view-type="remote"
 *           jndi-name="ejb/AdRepBankAccountListControllerEJB"
 *
 * @ejb:interface remote-class="com.ejb.txn.AdRepBankAccountListController"
 *                extends="javax.ejb.EJBObject"
 *
 * @ejb:home remote-class="com.ejb.txn.AdRepBankAccountListControllerHome"
 *           extends="javax.ejb.EJBHome"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="gluser"
 *                        role-link="gluserlink"
 *
 * @ejb:permission role-name="gluser"
 * 
*/

public class AdRepBankAccountListControllerBean extends AbstractSessionBean {


    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList executeAdRepBankAccountList(HashMap criteria, ArrayList branchList, String ORDER_BY, Integer AD_CMPNY) 
        throws GlobalNoRecordFoundException {
                    
        Debug.print("AdRepBankAccountListControllerBean executeAdRepBankAccountList");
        
        LocalAdBankAccountHome adBankAccountHome = null;
        LocalGlChartOfAccountHome glChartOfAccountHome = null;
        LocalAdBankAccountBalanceHome adBankAccountBalanceHome = null;
        
        LocalGlChartOfAccount glChartOfAccount = null;
        
        ArrayList list = new ArrayList();
        
        //initialized EJB Home
        
        try {
            
        	adBankAccountHome = (LocalAdBankAccountHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdBankAccountHome.JNDI_NAME, LocalAdBankAccountHome.class);
        	glChartOfAccountHome = (LocalGlChartOfAccountHome)EJBHomeFactory.
            	lookUpLocalHome(LocalGlChartOfAccountHome.JNDI_NAME, LocalGlChartOfAccountHome.class);
        	adBankAccountBalanceHome = (LocalAdBankAccountBalanceHome)EJBHomeFactory.
            	lookUpLocalHome(LocalAdBankAccountBalanceHome.JNDI_NAME, LocalAdBankAccountBalanceHome.class);

        } catch (NamingException ex) 	{
            
            throw new EJBException(ex.getMessage());
            
        }
        
		try { 
          
		  StringBuffer jbossQl = new StringBuffer();
		  jbossQl.append("SELECT DISTINCT OBJECT(ba) FROM AdBankAccount ba ");
		  
	      boolean firstArgument = true;
	      short ctr = 0;
		  int criteriaSize = criteria.size();	      
		  
		  if (branchList.isEmpty()) {
		  	
		  	throw new GlobalNoRecordFoundException();
		  	
		  }
		  else {
		  	
		  	jbossQl.append(", in (ba.adBranchBankAccounts) bba WHERE bba.adBranch.brCode in (");
		  	
		  	boolean firstLoop = true;
		  	
		  	Iterator j = branchList.iterator();
		  	
		  	while(j.hasNext()) {
		  		
		  		if(firstLoop == false) { 
		  			jbossQl.append(", "); 
		  		}
		  		else { 
		  			firstLoop = false; 
		  		}
		  		
		  		AdBranchDetails mdetails = (AdBranchDetails) j.next();
		  		
		  		jbossQl.append(mdetails.getBrCode());
		  		
		  	}
		  	
		  	jbossQl.append(") ");
		  	
		  	firstArgument = false;
		  	
		  }
		  
	      Object obj[];	      
		
		  // Allocate the size of the object parameter
		
	      if (criteria.containsKey("bankName")) {
	      	
	      	 criteriaSize--;
	      	 
	      }
	      
	      if (criteria.containsKey("accountName")) {
	      	
	      	 criteriaSize--;
	      	 
	      }

	      obj = new Object[criteriaSize];
		       	      
		  if (criteria.containsKey("bankName")) {
		  	
		  	 if (!firstArgument) {
		  	 
		  	    jbossQl.append("AND ");	
		  	 	
		     } else {
		     	
		     	firstArgument = false;
		     	jbossQl.append("WHERE ");
		     	
		     }
		     
		  	 jbossQl.append("ba.adBank.bnkName LIKE '%" + (String)criteria.get("bankName") + "%' ");
		  	 
		  }
		  
		  if (criteria.containsKey("accountName")) {
		  	
		  	 if (!firstArgument) {
		  	 
		  	    jbossQl.append("AND ");	
		  	 	
		     } else {
		     	
		     	firstArgument = false;
		     	jbossQl.append("WHERE ");
		     	
		     }
		     
		  	 jbossQl.append("ba.baName LIKE '%" + (String)criteria.get("accountName") + "%' ");
		  	 
		  }
		  
		  if (criteria.containsKey("accountUse")) {
	       	
	       	  if (!firstArgument) {
	       	  	
	       	     jbossQl.append("AND ");
	       	     
	       	  } else {
	       	  	
	       	  	 firstArgument = false;
	       	  	 jbossQl.append("WHERE ");
	       	  	 
	       	  }
	       	  
	       	  jbossQl.append("ba.baAccountUse=?" + (ctr+1) + " ");
	       	  obj[ctr] = (String)criteria.get("accountUse");
	       	  ctr++;
	       	  
	      }
		  
	      if (!firstArgument) {		       	  	
	   	     jbossQl.append("AND ");		       	     
	   	  } else {		       	  	
	   	  	 firstArgument = false;
	   	  	 jbossQl.append("WHERE ");		       	  	 
	   	  }	     
	   	  
	   	  jbossQl.append("ba.baAdCompany=" + AD_CMPNY + " ");

		  String orderBy = null;
          
          if (ORDER_BY.equals("BANK NAME")) {
	      	 
	      	  orderBy = "ba.adBank.bnkName";
	      	  
	      } else if (ORDER_BY.equals("ACCOUNT NAME")) {
	      	
	      	  orderBy = "ba.baName";
	      	  
	      } else if (ORDER_BY.equals("ACCOUNT USE")) {
	      	
	      	orderBy = "ba.baAccountUse";
	      	
	      }

		  if (orderBy != null) {
		  
		  	jbossQl.append("ORDER BY " + orderBy);
		  	
		  }     
			  	
		  System.out.println("QL: " + jbossQl.toString());
		  
	      Collection adBankAccounts = adBankAccountHome.getBaByCriteria(jbossQl.toString(), obj);	         
		  
		  if (adBankAccounts.size() == 0)
		     throw new GlobalNoRecordFoundException();
		     
		  Iterator i = adBankAccounts.iterator();
		
		  while (i.hasNext()) {
		  	
		  	  LocalAdBankAccount adBankAccount = (LocalAdBankAccount)i.next();   	  
		  	  
		  	  AdRepBankAccountListDetails details = new AdRepBankAccountListDetails();
		  	  details.setBalBankName(adBankAccount.getAdBank().getBnkName());
		  	  details.setBalBaName(adBankAccount.getBaName());
		  	  details.setBalBaDescription(adBankAccount.getBaDescription());
		  	  details.setBalBaAccountNumber(adBankAccount.getBaAccountNumber());
		  	  details.setBalBaAccountType(adBankAccount.getBaAccountType());
		  	  details.setBalBaAccountUse(adBankAccount.getBaAccountUse());
		  	  
		  	  // get latest bank account balance for current bank account
		  	  
		  	  double bankAccountBalance = 0;
		  	  
		  	  Collection adBankAccountBalances = adBankAccountBalanceHome.findByBaCodeAndType(adBankAccount.getBaCode(), "BOOK", AD_CMPNY);
		  	  
		  	  if (!adBankAccountBalances.isEmpty()) {
		  	  	
		  	  	// get last check
		  	  	
		  	  	ArrayList adBankAccountBalanceList = new ArrayList(adBankAccountBalances);
		  	  	
		  	  	LocalAdBankAccountBalance adBankAccountBalance = (LocalAdBankAccountBalance)adBankAccountBalanceList.get(adBankAccountBalanceList.size() - 1);
		  	  	
		  	  	bankAccountBalance = adBankAccountBalance.getBabBalance();
		  	  	
		  	  }

		  	  details.setBalBaAvailableBalance(bankAccountBalance);
		  	  details.setBalBaEnable(adBankAccount.getBaEnable());
		  	  
		  	  // account numbers and description
		  	  
		  	  if (adBankAccount.getBaCoaGlCashAccount() != null) {
		  	  
		  	      glChartOfAccount = glChartOfAccountHome.findByPrimaryKey(adBankAccount.getBaCoaGlCashAccount());
		  	  
		  	      details.setBalBaCashAccount(glChartOfAccount.getCoaAccountNumber());
		  	      details.setBalBaCashAccountDescription(glChartOfAccount.getCoaAccountDescription());
		  	      
		  	  }
		  	  
		  	  if (adBankAccount.getBaCoaGlAdjustmentAccount() != null) {
		  	  	
		  	  	  glChartOfAccount = glChartOfAccountHome.findByPrimaryKey(adBankAccount.getBaCoaGlAdjustmentAccount());
		  	  
		  	  	  details.setBalBaAdjustmentAccount(glChartOfAccount.getCoaAccountNumber());
		  	      details.setBalBaAdjustmentAccountDescription(glChartOfAccount.getCoaAccountDescription());
		  	  	  
		  	  }
		  	  
		  	  if (adBankAccount.getBaCoaGlInterestAccount() != null) {
		  	  	
		  	  	  glChartOfAccount = glChartOfAccountHome.findByPrimaryKey(adBankAccount.getBaCoaGlInterestAccount());
		  	  
		  	  	  details.setBalBaInterestAccount(glChartOfAccount.getCoaAccountNumber());
		  	      details.setBalBaInterestAccountDescription(glChartOfAccount.getCoaAccountDescription());
		  	  	  
		  	  }
		  	  
		  	  if (adBankAccount.getBaCoaGlBankChargeAccount() != null) {
		  	  	
		  	  	  glChartOfAccount = glChartOfAccountHome.findByPrimaryKey(adBankAccount.getBaCoaGlBankChargeAccount());
		  	  
		  	  	  details.setBalBaBankChargeAccount(glChartOfAccount.getCoaAccountNumber());
		  	      details.setBalBaBankChargeAccountDescription(glChartOfAccount.getCoaAccountDescription());
		  	  	  
		  	  }
		  	  
		  	  if (adBankAccount.getBaCoaGlSalesDiscount() != null) {
		  	  	
		  	  	  glChartOfAccount = glChartOfAccountHome.findByPrimaryKey(adBankAccount.getBaCoaGlSalesDiscount());
		  	  
		  	  	  details.setBalBaSalesDiscount(glChartOfAccount.getCoaAccountNumber());
		  	      details.setBalBaSalesDiscountDescription(glChartOfAccount.getCoaAccountDescription());
		  	  	  
		  	  }

			  list.add(details);
			         
		  }
			     
		  return list;
		      
	  } catch (GlobalNoRecordFoundException ex) {
	  	 
	  	  throw ex;
	  	
	  } catch (Exception ex) {
	  	

	  	  ex.printStackTrace();
	  	  throw new EJBException(ex.getMessage());
	  	
	  }
        
    }
    
	/**
	* @ejb:interface-method view-type="remote"
	* @jboss:method-attributes read-only="true"
	**/
	public com.util.AdCompanyDetails getAdCompany(Integer AD_CMPNY) {
	
	  Debug.print("AdRepBankAccountListControllerBean getAdCompany");      
	  
	  LocalAdCompanyHome adCompanyHome = null;
	        
	  // Initialize EJB Home
	    
	  try {
	        
	       adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
	           lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
	        
	  } catch (NamingException ex) {
	        
	      throw new EJBException(ex.getMessage());
	        
	  }
	
	  try {
	      
	     LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
	     
	     AdCompanyDetails details = new AdCompanyDetails();
	     details.setCmpName(adCompany.getCmpName());
	     
	     return details;  	
	  	       	
	  } catch (Exception ex) {
	  	
	  	 Debug.printStackTrace(ex);
	  	 throw new EJBException(ex.getMessage());
	  	  
	  }
	  
	}    


    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/   
    public ArrayList getAdBrResAll(Integer RS_CODE, Integer AD_CMPNY) 
    	throws GlobalNoRecordFoundException{
        
        Debug.print("AdRepBankAccountListControllerBean getAdBrResAll");
        
        LocalAdBranchResponsibilityHome adBranchResponsibilityHome = null;
        LocalAdBranchHome adBranchHome = null;
        
        LocalAdBranchResponsibility adBranchResponsibility = null;
        LocalAdBranch adBranch = null;
        
        Collection adBranchResponsibilities = null;
        
        ArrayList list = new ArrayList();
        
        // Initialize EJB Home
        
        try {
            
            adBranchResponsibilityHome = (LocalAdBranchResponsibilityHome)EJBHomeFactory.
            lookUpLocalHome(LocalAdBranchResponsibilityHome.JNDI_NAME, LocalAdBranchResponsibilityHome.class);
            adBranchHome = (LocalAdBranchHome)EJBHomeFactory.
            lookUpLocalHome(LocalAdBranchHome.JNDI_NAME, LocalAdBranchHome.class);
            
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {
            
            adBranchResponsibilities = adBranchResponsibilityHome.findByAdResponsibility(RS_CODE, AD_CMPNY);
            
        } catch (FinderException ex) {
            
        } catch (Exception ex) {
            
            throw new EJBException(ex.getMessage());
        }
        
        if (adBranchResponsibilities.isEmpty()) {
            
            throw new GlobalNoRecordFoundException();
            
        }
        
        try {
            
            Iterator i = adBranchResponsibilities.iterator();
            
            while(i.hasNext()) {
                
                adBranchResponsibility = (LocalAdBranchResponsibility)i.next();
                
                adBranch = adBranchResponsibility.getAdBranch();
                
                AdBranchDetails details = new AdBranchDetails();
                
                details.setBrCode(adBranch.getBrCode());
                details.setBrBranchCode(adBranch.getBrBranchCode());
                details.setBrName(adBranch.getBrName());
                details.setBrHeadQuarter(adBranch.getBrHeadQuarter());
                
                list.add(details);
                
            }	               
            
        } catch (Exception ex) {
            
            throw new EJBException(ex.getMessage());
        }
        
        return list;
        
    }	
	
    // SessionBean methods

    /**
     * @ejb:create-method view-type="remote"
     **/
    public void ejbCreate() throws CreateException {

       Debug.print("AdRepBankAccountListControllerBean ejbCreate");
      
    }
}
