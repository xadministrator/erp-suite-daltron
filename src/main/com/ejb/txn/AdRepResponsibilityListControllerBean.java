
/*
 * AdRepResponsibilityListControllerBean.java
 *
 * Created on Jun 22, 2005, 03:35 PM
 *
 * @author  Jolly T. Martin
 */

package com.ejb.txn;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;
import javax.naming.NamingException;

import com.ejb.ad.LocalAdBranch;
import com.ejb.ad.LocalAdBranchHome;
import com.ejb.ad.LocalAdBranchResponsibility;
import com.ejb.ad.LocalAdBranchResponsibilityHome;
import com.ejb.ad.LocalAdCompany;
import com.ejb.ad.LocalAdCompanyHome;
import com.ejb.ad.LocalAdFormFunctionResponsibility;
import com.ejb.ad.LocalAdResponsibility;
import com.ejb.ad.LocalAdResponsibilityHome;
import com.ejb.exception.GlobalNoRecordFoundException;
import com.util.AbstractSessionBean;
import com.util.AdBranchDetails;
import com.util.AdCompanyDetails;
import com.util.AdRepResponsibilityListDetails;
import com.util.Debug;
import com.util.EJBHomeFactory;

/**
 * @ejb:bean name="AdRepResponsibilityListControllerEJB"
 *           display-name="Used for viewing responsibility lists"
 *           type="Stateless"
 *           view-type="remote"
 *           jndi-name="ejb/AdRepResponsibilityListControllerEJB"
 *
 * @ejb:interface remote-class="com.ejb.txn.AdRepResponsibilityListController"
 *                extends="javax.ejb.EJBObject"
 *
 * @ejb:home remote-class="com.ejb.txn.AdRepResponsibilityListControllerHome"
 *           extends="javax.ejb.EJBHome"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="aduser"
 *                        role-link="aduserlink"
 *
 * @ejb:permission role-name="aduser"
 * 
 */

public class AdRepResponsibilityListControllerBean extends AbstractSessionBean {
	
	
	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public ArrayList executeAdRepResponsibilityList(HashMap criteria, ArrayList branchList, String ORDER_BY, boolean showFormFunction, Integer AD_CMPNY) 
	throws GlobalNoRecordFoundException {
		
		Debug.print("AdRepResponsibilityListControllerBean executeAdRepResponsibilityList");
		
		LocalAdResponsibilityHome adResponsibilityHome = null;
		
		ArrayList list = new ArrayList();
		
		//initialized EJB Home
		
		try {
			
			adResponsibilityHome = (LocalAdResponsibilityHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdResponsibilityHome.JNDI_NAME, LocalAdResponsibilityHome.class);
			
		} catch (NamingException ex) 	{
			
			throw new EJBException(ex.getMessage());
			
		}
		
		try { 
			
			StringBuffer jbossQl = new StringBuffer();
			jbossQl.append("SELECT OBJECT(rs) FROM AdResponsibility rs ");
			
			boolean firstArgument = true;
			short ctr = 0;
			int criteriaSize = criteria.size();	      
			
			  if (branchList.isEmpty()) {
			  	
			  	throw new GlobalNoRecordFoundException();
			  	
			  }
			  else {
			  	
			  	jbossQl.append(", in (rs.adBranchResponsibilities) brs WHERE brs.adBranch.brCode in (");
			  	
			  	boolean firstLoop = true;
			  	
			  	Iterator j = branchList.iterator();
			  	
			  	while(j.hasNext()) {
			  		
			  		if(firstLoop == false) { 
			  			jbossQl.append(", "); 
			  		}
			  		else { 
			  			firstLoop = false; 
			  		}
			  		
			  		AdBranchDetails mdetails = (AdBranchDetails) j.next();
			  		
			  		jbossQl.append(mdetails.getBrCode());
			  		
			  	}
			  	
			  	jbossQl.append(") ");
			  	
			  	firstArgument = false;
			  	
			  }
			  
			  Object obj[];	      
			
			// Allocate the size of the object parameter
			
			if (criteria.containsKey("responsibilityName")) {
				
				criteriaSize--;
				
			}
			
			obj = new Object[criteriaSize];
			
			if (criteria.containsKey("responsibilityName")) {
				
				if (!firstArgument) {
					
					jbossQl.append("AND ");	
					
				} else {
					
					firstArgument = false;
					jbossQl.append("WHERE ");
					
				}
				
				jbossQl.append("rs.rsName LIKE '%" + (String)criteria.get("responsibilityName") + "%' ");
				
			}
			
			if (criteria.containsKey("dateFrom")) {
				
				if (!firstArgument) {		       	  	
					jbossQl.append("AND ");		       	     
				} else {		       	  	
					firstArgument = false;
					jbossQl.append("WHERE ");		       	  	 
				}
				
				jbossQl.append("rs.rsDateFrom=?" + (ctr+1) + " ");
				obj[ctr] = (String)criteria.get("dateFrom");
				ctr++;
				
			}
			
			if (criteria.containsKey("dateTo")) {
				
				if (!firstArgument) {		       	  	
					jbossQl.append("AND ");		       	     
				} else {		       	  	
					firstArgument = false;
					jbossQl.append("WHERE ");		       	  	 
				}
				
				jbossQl.append("rs.rsDateTo=?" + (ctr+1) + " ");
				obj[ctr] = (String)criteria.get("dateTo");
				ctr++;
				
			}
			
			if (!firstArgument) {
				jbossQl.append("AND ");
			} else {
				firstArgument = false;
				jbossQl.append("WHERE ");
			}
			
			jbossQl.append("rs.rsAdCompany=" + AD_CMPNY + " ");
			
			String orderBy = null;
			
			if (ORDER_BY.equals("RESPONSIBILITY NAME")) {
				
				orderBy = "rs.rsName";
				
			} else if (ORDER_BY.equals("RESPONSIBILITY DESC")) {
				
				orderBy = "rs.rsDescription";
				
			}
			
			if (orderBy != null) {
				
				jbossQl.append("ORDER BY " + orderBy);
				
			}
			
			// responsibility
			
			Collection adResponsibilities = adResponsibilityHome.getRsByCriteria(jbossQl.toString(), obj);	         
			
			if (adResponsibilities.size() == 0)
				throw new GlobalNoRecordFoundException();
			
			Iterator i = adResponsibilities.iterator();
			
			while (i.hasNext()) {
				
				LocalAdResponsibility adResponsibility = (LocalAdResponsibility)i.next();		
				
				if(showFormFunction) {
					
					Collection adFormFunctions = adResponsibility.getAdFormFunctionResponsibilities();
					
					Iterator itrFFR = adFormFunctions.iterator();
					
					while(itrFFR.hasNext()) {
						
						LocalAdFormFunctionResponsibility adFormFunction = (LocalAdFormFunctionResponsibility)itrFFR.next();
						
						list = this.getResponsibilityList(list, adResponsibility, adFormFunction.getAdFormFunction().getFfName());
						
					}
					
				} else {
					
					list = this.getResponsibilityList(list, adResponsibility, "");
					
				}
				
			}
			
			return list;
			
		} catch (GlobalNoRecordFoundException ex) {
			
			throw ex;
			
		} catch (Exception ex) {
			
			
			ex.printStackTrace();
			throw new EJBException(ex.getMessage());
			
		}
		
	}
	
	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public com.util.AdCompanyDetails getAdCompany(Integer AD_CMPNY) {
		
		Debug.print("AdRepResponsibilityListControllerBean getAdCompany");      
		
		LocalAdCompanyHome adCompanyHome = null;
		
		// Initialize EJB Home
		
		try {
			
			adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
			
		} catch (NamingException ex) {
			
			throw new EJBException(ex.getMessage());
			
		}
		
		try {
			
			LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
			
			AdCompanyDetails details = new AdCompanyDetails();
			details.setCmpName(adCompany.getCmpName());
			
			return details;  	
			
		} catch (Exception ex) {
			
			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());
			
		}
		
	}   
	
	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public ArrayList getAdBrResAll(Integer RS_CODE, Integer AD_CMPNY) 
	throws GlobalNoRecordFoundException{
		
		Debug.print("AdRepBankAccountListControllerBean getAdBrResAll");
		
		LocalAdBranchResponsibilityHome adBranchResponsibilityHome = null;
		LocalAdBranchHome adBranchHome = null;
		
		LocalAdBranchResponsibility adBranchResponsibility = null;
		LocalAdBranch adBranch = null;
		
		Collection adBranchResponsibilities = null;
		
		ArrayList list = new ArrayList();
		
		// Initialize EJB Home
		
		try {
			
			adBranchResponsibilityHome = (LocalAdBranchResponsibilityHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdBranchResponsibilityHome.JNDI_NAME, LocalAdBranchResponsibilityHome.class);
			adBranchHome = (LocalAdBranchHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdBranchHome.JNDI_NAME, LocalAdBranchHome.class);
			
		} catch (NamingException ex) {
			
			throw new EJBException(ex.getMessage());
			
		}
		
		try {
			
			adBranchResponsibilities = adBranchResponsibilityHome.findByAdResponsibility(RS_CODE, AD_CMPNY);
			
		} catch (FinderException ex) {
			
		} catch (Exception ex) {
			
			throw new EJBException(ex.getMessage());
		}
		
		if (adBranchResponsibilities.isEmpty()) {
			
			throw new GlobalNoRecordFoundException();
			
		}
		
		try {
			
			Iterator i = adBranchResponsibilities.iterator();
			
			while(i.hasNext()) {
				
				adBranchResponsibility = (LocalAdBranchResponsibility)i.next();
				
				adBranch = adBranchResponsibility.getAdBranch();
				
				AdBranchDetails details = new AdBranchDetails();
				
				details.setBrCode(adBranch.getBrCode());
				details.setBrBranchCode(adBranch.getBrBranchCode());
				details.setBrName(adBranch.getBrName());
				details.setBrHeadQuarter(adBranch.getBrHeadQuarter());
				
				list.add(details);
				
			}	               
			
		} catch (Exception ex) {
			
			throw new EJBException(ex.getMessage());
		}
		
		return list;
		
	}	
	
	// private method
	
	private ArrayList getResponsibilityList(ArrayList list, LocalAdResponsibility adResponsibility, String formFunction) {
		
		AdRepResponsibilityListDetails details = new AdRepResponsibilityListDetails();
		
		details.setRlResponsiblityName(adResponsibility.getRsName());
		details.setRlResponsibilityDescription(adResponsibility.getRsDescription());
		details.setRlDateFrom(adResponsibility.getRsDateFrom());
		details.setRlDateTo(adResponsibility.getRsDateTo());
		details.setRlFormFunction(formFunction);
		
		list.add(details);
		
		return list;
		
		
	}
	
	
	// SessionBean methods
	
	/**
	 * @ejb:create-method view-type="remote"
	 **/
	public void ejbCreate() throws CreateException {
		
		Debug.print("AdRepResponsibilityListControllerBean ejbCreate");
		
	}
}
