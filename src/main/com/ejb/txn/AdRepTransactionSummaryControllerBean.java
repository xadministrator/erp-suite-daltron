
/*
 * ApRepTransactionSummaryControllerBean.java
 *
 * Created on April 12, 2005, 2:09 PM
 *
 * @author  Marvis Fernando C. Salvador
 */


package com.ejb.txn;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;
import javax.naming.NamingException;

import com.ejb.ad.LocalAdBranch;
import com.ejb.ad.LocalAdBranchHome;
import com.ejb.ad.LocalAdBranchResponsibility;
import com.ejb.ad.LocalAdBranchResponsibilityHome;
import com.ejb.ad.LocalAdCompany;
import com.ejb.ad.LocalAdCompanyHome;
import com.ejb.ad.LocalAdUser;
import com.ejb.ad.LocalAdUserHome;
import com.ejb.ap.LocalApCheck;
import com.ejb.ap.LocalApCheckHome;
import com.ejb.ap.LocalApVoucher;
import com.ejb.ap.LocalApVoucherHome;
import com.ejb.ar.LocalArInvoice;
import com.ejb.ar.LocalArInvoiceHome;
import com.ejb.ar.LocalArReceipt;
import com.ejb.ar.LocalArReceiptHome;
import com.ejb.ar.LocalArSalesOrder;
import com.ejb.ar.LocalArSalesOrderHome;
import com.ejb.ar.LocalArSalesOrderLine;
import com.ejb.cm.LocalCmAdjustment;
import com.ejb.cm.LocalCmAdjustmentHome;
import com.ejb.cm.LocalCmFundTransfer;
import com.ejb.cm.LocalCmFundTransferHome;
import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.gl.LocalGlJournal;
import com.ejb.gl.LocalGlJournalHome;
import com.ejb.gl.LocalGlJournalLine;
import com.ejb.inv.LocalInvAdjustment;
import com.ejb.inv.LocalInvAdjustmentHome;
import com.ejb.inv.LocalInvAssemblyTransfer;
import com.ejb.inv.LocalInvAssemblyTransferHome;
import com.ejb.inv.LocalInvBuildOrder;
import com.ejb.inv.LocalInvBuildOrderHome;
import com.ejb.inv.LocalInvBuildUnbuildAssembly;
import com.ejb.inv.LocalInvBuildUnbuildAssemblyHome;
import com.ejb.inv.LocalInvOverhead;
import com.ejb.inv.LocalInvOverheadHome;
import com.ejb.inv.LocalInvStockIssuance;
import com.ejb.inv.LocalInvStockIssuanceHome;
import com.util.AbstractSessionBean;
import com.util.AdBranchDetails;
import com.util.AdCompanyDetails;
import com.util.AdRepTransactionSummaryDetails;
import com.util.Debug;
import com.util.EJBCommon;
import com.util.EJBHomeFactory;

/**
 * @ejb:bean name="AdRepTransactionSummaryControllerEJB"
 *           display-name="Used for generation of transaction summary"
 *           type="Stateless"
 *           view-type="remote"
 *           jndi-name="ejb/AdRepTransactionSummaryControllerEJB"
 *
 * @ejb:interface remote-class="com.ejb.txn.AdRepTransactionSummaryController"
 *                extends="javax.ejb.EJBObject"
 *
 * @ejb:home remote-class="com.ejb.txn.AdRepTransactionSummaryControllerHome"
 *           extends="javax.ejb.EJBHome"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="aduser"
 *                        role-link="aduserlink"
 *
 * @ejb:permission role-name="aduser"
 * 
 */

public class AdRepTransactionSummaryControllerBean extends AbstractSessionBean {
	
	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public ArrayList getAdUsrAll(Integer AD_CMPNY) {
		
		Debug.print("AdRepTransactionSummaryControllerBean getAdUsrAll");
		
		LocalAdUserHome adUserHome = null;
		
		ArrayList list = new ArrayList();
		
		// Initialize EJB Home
		
		try {
			
			adUserHome = (LocalAdUserHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdUserHome.JNDI_NAME, LocalAdUserHome.class);
			
		} catch (NamingException ex) {
			
			throw new EJBException(ex.getMessage());
			
		}
		
		try {
			
			Collection adUsers = adUserHome.findUsrAll(AD_CMPNY);
			
			Iterator i = adUsers.iterator();
			
			while (i.hasNext()) {
				
				LocalAdUser adUser = (LocalAdUser)i.next();
				
				list.add(adUser.getUsrName());
				
			}
			
			return list;
			
		} catch (Exception ex) {
			
			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());
			
		}
		
	}
	
	
	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public ArrayList executeAdRepTransactionSummary(HashMap criteria, ArrayList branchList, String ORDER_BY, Integer AD_CMPNY) 
	throws GlobalNoRecordFoundException {
		
		Debug.print("AdRepTransactionSummaryControllerBean executeAdRepTransactionSummary");
		
		LocalGlJournalHome glJournalHome = null;        
		LocalApVoucherHome apVoucherHome = null;
		LocalApCheckHome apCheckHome = null;
		LocalArInvoiceHome arInvoiceHome = null;
		LocalArReceiptHome arReceiptHome = null;
		LocalArSalesOrderHome arSalesOrderHome = null;
		LocalCmAdjustmentHome cmAdjustmentHome = null;
		LocalCmFundTransferHome cmFundTransferHome = null;
		LocalInvAdjustmentHome invAdjustmentHome = null;
		LocalInvBuildUnbuildAssemblyHome invBuildAssemblyHome = null;
		LocalInvOverheadHome invOverheadHome = null;
		LocalInvBuildOrderHome invBuildOrderHome = null;
		LocalInvStockIssuanceHome invStockIssuanceHome = null;
		LocalInvAssemblyTransferHome invAssemblyTransferHome = null;
		
		ArrayList list = new ArrayList();
		
		
		//initialized EJB Home
		
		try {
			
			glJournalHome = (LocalGlJournalHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlJournalHome.JNDI_NAME, LocalGlJournalHome.class);
			apVoucherHome = (LocalApVoucherHome)EJBHomeFactory.
			lookUpLocalHome(LocalApVoucherHome.JNDI_NAME, LocalApVoucherHome.class);
			apCheckHome = (LocalApCheckHome)EJBHomeFactory.
			lookUpLocalHome(LocalApCheckHome.JNDI_NAME, LocalApCheckHome.class);
			arInvoiceHome = (LocalArInvoiceHome)EJBHomeFactory.
			lookUpLocalHome(LocalArInvoiceHome.JNDI_NAME, LocalArInvoiceHome.class);
			arReceiptHome = (LocalArReceiptHome)EJBHomeFactory.
			lookUpLocalHome(LocalArReceiptHome.JNDI_NAME, LocalArReceiptHome.class);
			arSalesOrderHome = (LocalArSalesOrderHome)EJBHomeFactory.
			lookUpLocalHome(LocalArSalesOrderHome.JNDI_NAME, LocalArSalesOrderHome.class);
			cmAdjustmentHome = (LocalCmAdjustmentHome)EJBHomeFactory.
			lookUpLocalHome(LocalCmAdjustmentHome.JNDI_NAME, LocalCmAdjustmentHome.class);
			cmFundTransferHome = (LocalCmFundTransferHome)EJBHomeFactory.
			lookUpLocalHome(LocalCmFundTransferHome.JNDI_NAME, LocalCmFundTransferHome.class);
			invAdjustmentHome = (LocalInvAdjustmentHome)EJBHomeFactory.
			lookUpLocalHome(LocalInvAdjustmentHome.JNDI_NAME, LocalInvAdjustmentHome.class);
			invOverheadHome = (LocalInvOverheadHome)EJBHomeFactory.
			lookUpLocalHome(LocalInvOverheadHome.JNDI_NAME, LocalInvOverheadHome.class);
			invBuildOrderHome = (LocalInvBuildOrderHome)EJBHomeFactory.
			lookUpLocalHome(LocalInvBuildOrderHome.JNDI_NAME, LocalInvBuildOrderHome.class);
			invStockIssuanceHome = (LocalInvStockIssuanceHome)EJBHomeFactory.
			lookUpLocalHome(LocalInvStockIssuanceHome.JNDI_NAME, LocalInvStockIssuanceHome.class);
			invAssemblyTransferHome = (LocalInvAssemblyTransferHome)EJBHomeFactory.
			lookUpLocalHome(LocalInvAssemblyTransferHome.JNDI_NAME, LocalInvAssemblyTransferHome.class);
			invBuildAssemblyHome = (LocalInvBuildUnbuildAssemblyHome)EJBHomeFactory.
			lookUpLocalHome(LocalInvBuildUnbuildAssemblyHome.JNDI_NAME, LocalInvBuildUnbuildAssemblyHome.class);
			
		} catch (NamingException ex) 	{
			
			throw new EJBException(ex.getMessage());
			
		}
		
		try { 
			
			if (!criteria.containsKey("type") || ((String)criteria.get("type")).equals("GL JOURNAL")) {
				
				int criteriaSize = 0;
				
				if (criteria.containsKey("docDateFrom")) criteriaSize++;
				if (criteria.containsKey("docDateTo")) criteriaSize++;
				if (criteria.containsKey("dateCreatedFrom")) criteriaSize++;
				if (criteria.containsKey("dateCreatedTo")) criteriaSize++;
				if (criteria.containsKey("dateLastModifiedFrom")) criteriaSize++;
				if (criteria.containsKey("dateLastModifiedTo")) criteriaSize++;      
				if (criteria.containsKey("dateApprovedRejectedFrom")) criteriaSize++;
				if (criteria.containsKey("dateApprovedRejectedTo")) criteriaSize++;
				if (criteria.containsKey("datePostedFrom")) criteriaSize++;
				if (criteria.containsKey("datePostedTo")) criteriaSize++;
				
				Object[] obj = new Object[criteriaSize];
				
				ArrayList tcarray = new ArrayList();
				
				AdRepTransactionSummaryDetails details = new AdRepTransactionSummaryDetails();
				
				Collection glJournals = glJournalHome.getJrByCriteria(this.getQueryByTxnTypeAndCriteria("GlJournal", "jr", criteria, branchList, obj, AD_CMPNY), obj);
				
				Iterator i = glJournals.iterator();
				
				while (i.hasNext()) {
					
					LocalGlJournal glJournal = (LocalGlJournal)i.next();
					
					details.setTxlType("GL JOURNAL");
					details.setTxlDate(glJournal.getJrEffectiveDate());
					details.setTxlDocumentNumber(glJournal.getJrDocumentNumber());
					
					Collection glJournalLines = glJournal.getGlJournalLines();
					
					Iterator jlIter = glJournalLines.iterator();
					
					while (jlIter.hasNext()) {
						
						LocalGlJournalLine glJournalLine = (LocalGlJournalLine)jlIter.next();
						
						if (glJournalLine.getJlDebit() == EJBCommon.TRUE) {
							
							details.setTxlAmount(details.getTxlAmount() + glJournalLine.getJlAmount());
							
						}
						
					}
					details.setTxlApprovalStatus(glJournal.getJrApprovalStatus());
					details.setTxlPosted(glJournal.getJrPosted());
					details.setTxlCreatedBy(glJournal.getJrCreatedBy());
					details.setTxlDateCreated(glJournal.getJrDateCreated());
					details.setTxlLastModifiedBy(glJournal.getJrLastModifiedBy());
					details.setTxlDateLastModified(glJournal.getJrDateLastModified());
					details.setTxlApprovedRejectedBy(glJournal.getJrApprovedRejectedBy());
					details.setTxlDateApprovedRejected(glJournal.getJrDateApprovedRejected());
					details.setTxlPostedBy(glJournal.getJrPostedBy());
					details.setTxlDatePosted(glJournal.getJrDatePosted());
					details.setTxlOrderBy(ORDER_BY);		
					
					list.add(details);		          
					
				}
				
			}
			
			if (!criteria.containsKey("type") || ((String)criteria.get("type")).equals("AP VOUCHER")) {
				
				int criteriaSize = 0;
				
				if (criteria.containsKey("docDateFrom")) criteriaSize++;
				if (criteria.containsKey("docDateTo")) criteriaSize++;
				if (criteria.containsKey("dateCreatedFrom")) criteriaSize++;
				if (criteria.containsKey("dateCreatedTo")) criteriaSize++;
				if (criteria.containsKey("dateLastModifiedFrom")) criteriaSize++;
				if (criteria.containsKey("dateLastModifiedTo")) criteriaSize++;      
				if (criteria.containsKey("dateApprovedRejectedFrom")) criteriaSize++;
				if (criteria.containsKey("dateApprovedRejectedTo")) criteriaSize++;
				if (criteria.containsKey("datePostedFrom")) criteriaSize++;
				if (criteria.containsKey("datePostedTo")) criteriaSize++;
				
				Object[] obj = new Object[criteriaSize];
				
				AdRepTransactionSummaryDetails details = new AdRepTransactionSummaryDetails();
				
				Collection apVouchers = apVoucherHome.getVouByCriteria(this.getQueryByTxnTypeAndCriteria("ApVoucher", "vou", criteria, branchList, obj, AD_CMPNY), obj);
				
				Iterator i = apVouchers.iterator();
				
				while (i.hasNext()) {
					
					LocalApVoucher apVoucher = (LocalApVoucher)i.next();
					
					if (apVoucher.getVouDebitMemo() == EJBCommon.FALSE) {
						
						details.setTxlType("AP VOUCHER");
						
					} else {
						
						details.setTxlType("AP DEBIT MEMO");
						
					}
					details.setTxlDate(apVoucher.getVouDate());
					details.setTxlDocumentNumber(apVoucher.getVouDocumentNumber());		          
					details.setTxlAmount(apVoucher.getVouAmountDue());		          
					details.setTxlApprovalStatus(apVoucher.getVouApprovalStatus());
					details.setTxlPosted(apVoucher.getVouPosted());
					details.setTxlCreatedBy(apVoucher.getVouCreatedBy());
					details.setTxlDateCreated(apVoucher.getVouDateCreated());
					details.setTxlLastModifiedBy(apVoucher.getVouLastModifiedBy());
					details.setTxlDateLastModified(apVoucher.getVouDateLastModified());
					details.setTxlApprovedRejectedBy(apVoucher.getVouApprovedRejectedBy());
					details.setTxlDateApprovedRejected(apVoucher.getVouDateApprovedRejected());
					details.setTxlPostedBy(apVoucher.getVouPostedBy());
					details.setTxlDatePosted(apVoucher.getVouDatePosted());
					details.setTxlOrderBy(ORDER_BY);
					
					list.add(details);		          
					
				}
				
			}
			
			if (!criteria.containsKey("type") || ((String)criteria.get("type")).equals("AP CHECK")) {
				
				int criteriaSize = 0;
				
				if (criteria.containsKey("docDateFrom")) criteriaSize++;
				if (criteria.containsKey("docDateTo")) criteriaSize++;
				if (criteria.containsKey("dateCreatedFrom")) criteriaSize++;
				if (criteria.containsKey("dateCreatedTo")) criteriaSize++;
				if (criteria.containsKey("dateLastModifiedFrom")) criteriaSize++;
				if (criteria.containsKey("dateLastModifiedTo")) criteriaSize++;      
				if (criteria.containsKey("dateApprovedRejectedFrom")) criteriaSize++;
				if (criteria.containsKey("dateApprovedRejectedTo")) criteriaSize++;
				if (criteria.containsKey("datePostedFrom")) criteriaSize++;
				if (criteria.containsKey("datePostedTo")) criteriaSize++;
				
				Object[] obj = new Object[criteriaSize];
				
				AdRepTransactionSummaryDetails details = new AdRepTransactionSummaryDetails();
				
				Collection apChecks = apCheckHome.getChkByCriteria(this.getQueryByTxnTypeAndCriteria("ApCheck", "chk", criteria, branchList, obj, AD_CMPNY), obj);
				
				Iterator i = apChecks.iterator();
				
				while (i.hasNext()) {
					
					LocalApCheck apCheck = (LocalApCheck)i.next();
					
					details.setTxlType("AP CHECK");	
					
					details.setTxlDate(apCheck.getChkDate());
					details.setTxlDocumentNumber(apCheck.getChkDocumentNumber());		          
					details.setTxlAmount(apCheck.getChkAmount());		          
					details.setTxlApprovalStatus(apCheck.getChkApprovalStatus());
					details.setTxlPosted(apCheck.getChkPosted());
					details.setTxlCreatedBy(apCheck.getChkCreatedBy());
					details.setTxlDateCreated(apCheck.getChkDateCreated());
					details.setTxlLastModifiedBy(apCheck.getChkLastModifiedBy());
					details.setTxlDateLastModified(apCheck.getChkDateLastModified());
					details.setTxlApprovedRejectedBy(apCheck.getChkApprovedRejectedBy());
					details.setTxlDateApprovedRejected(apCheck.getChkDateApprovedRejected());
					details.setTxlPostedBy(apCheck.getChkPostedBy());
					details.setTxlDatePosted(apCheck.getChkDatePosted());
					details.setTxlOrderBy(ORDER_BY);
					
					
					list.add(details);		          
					
				}
				
			}
			
			if (!criteria.containsKey("type") || ((String)criteria.get("type")).equals("AR INVOICE")) {
				
				int criteriaSize = 0;
				
				if (criteria.containsKey("docDateFrom")) criteriaSize++;
				if (criteria.containsKey("docDateTo")) criteriaSize++;
				if (criteria.containsKey("dateCreatedFrom")) criteriaSize++;
				if (criteria.containsKey("dateCreatedTo")) criteriaSize++;
				if (criteria.containsKey("dateLastModifiedFrom")) criteriaSize++;
				if (criteria.containsKey("dateLastModifiedTo")) criteriaSize++;      
				if (criteria.containsKey("dateApprovedRejectedFrom")) criteriaSize++;
				if (criteria.containsKey("dateApprovedRejectedTo")) criteriaSize++;
				if (criteria.containsKey("datePostedFrom")) criteriaSize++;
				if (criteria.containsKey("datePostedTo")) criteriaSize++;
				
				Object[] obj = new Object[criteriaSize];
				
				int ifelse = 0;
				
				Collection arInvoices = arInvoiceHome.getInvByCriteria(this.getQueryByTxnTypeAndCriteria("ArInvoice", "inv", criteria, branchList, obj, AD_CMPNY), obj);
				
				Iterator i = arInvoices.iterator();
				
				AdRepTransactionSummaryDetails details = new AdRepTransactionSummaryDetails();
				
				while (i.hasNext()) {
					
					LocalArInvoice arInvoice = (LocalArInvoice)i.next();
					
					if (arInvoice.getInvCreditMemo() == EJBCommon.FALSE) {
						
						details.setTxlType("AR INVOICE");
						
					} else {
						
						details.setTxlType("AR CREDIT MEMO");
						
					}
					details.setTxlDate(arInvoice.getInvDate());
					details.setTxlDocumentNumber(arInvoice.getInvNumber());		          
					details.setTxlAmount(arInvoice.getInvAmountDue());		          
					details.setTxlApprovalStatus(arInvoice.getInvApprovalStatus());
					details.setTxlPosted(arInvoice.getInvPosted());
					details.setTxlCreatedBy(arInvoice.getInvCreatedBy());
					details.setTxlDateCreated(arInvoice.getInvDateCreated());
					details.setTxlLastModifiedBy(arInvoice.getInvLastModifiedBy());
					details.setTxlDateLastModified(arInvoice.getInvDateLastModified());
					details.setTxlApprovedRejectedBy(arInvoice.getInvApprovedRejectedBy());
					details.setTxlDateApprovedRejected(arInvoice.getInvDateApprovedRejected());
					details.setTxlPostedBy(arInvoice.getInvPostedBy());
					details.setTxlDatePosted(arInvoice.getInvDatePosted());
					details.setTxlOrderBy(ORDER_BY);
					
					list.add(details);		          
					
				}
				
			}
			
			if (!criteria.containsKey("type") || ((String)criteria.get("type")).equals("AR RECEIPT")) {
				
				int criteriaSize = 0;
				
				if (criteria.containsKey("docDateFrom")) criteriaSize++;
				if (criteria.containsKey("docDateTo")) criteriaSize++;
				if (criteria.containsKey("dateCreatedFrom")) criteriaSize++;
				if (criteria.containsKey("dateCreatedTo")) criteriaSize++;
				if (criteria.containsKey("dateLastModifiedFrom")) criteriaSize++;
				if (criteria.containsKey("dateLastModifiedTo")) criteriaSize++;      
				if (criteria.containsKey("dateApprovedRejectedFrom")) criteriaSize++;
				if (criteria.containsKey("dateApprovedRejectedTo")) criteriaSize++;
				if (criteria.containsKey("datePostedFrom")) criteriaSize++;
				if (criteria.containsKey("datePostedTo")) criteriaSize++;
				
				Object[] obj = new Object[criteriaSize];
				
				AdRepTransactionSummaryDetails details = new AdRepTransactionSummaryDetails();
				
				Collection arReceipts = arReceiptHome.getRctByCriteria(this.getQueryByTxnTypeAndCriteria("ArReceipt", "rct", criteria, branchList, obj, AD_CMPNY), obj);
				
				Iterator i = arReceipts.iterator();
				
				while (i.hasNext()) {
					
					LocalArReceipt arReceipt = (LocalArReceipt)i.next();
					
					details.setTxlType("AR RECEIPT");	
					details.setTxlDate(arReceipt.getRctDate());
					details.setTxlDocumentNumber(arReceipt.getRctNumber());		          
					details.setTxlAmount(arReceipt.getRctAmount());		          
					details.setTxlApprovalStatus(arReceipt.getRctApprovalStatus());
					details.setTxlPosted(arReceipt.getRctPosted());
					details.setTxlCreatedBy(arReceipt.getRctCreatedBy());
					details.setTxlDateCreated(arReceipt.getRctDateCreated());
					details.setTxlLastModifiedBy(arReceipt.getRctLastModifiedBy());
					details.setTxlDateLastModified(arReceipt.getRctDateLastModified());
					details.setTxlApprovedRejectedBy(arReceipt.getRctApprovedRejectedBy());
					details.setTxlDateApprovedRejected(arReceipt.getRctDateApprovedRejected());
					details.setTxlPostedBy(arReceipt.getRctPostedBy());
					details.setTxlDatePosted(arReceipt.getRctDatePosted());
					details.setTxlOrderBy(ORDER_BY);
					
					
					list.add(details);		          
					
				}
				
			}
			
			if (!criteria.containsKey("type") || ((String)criteria.get("type")).equals("AR SALES ORDER")) {
				
				int criteriaSize = 0;
				
				if (criteria.containsKey("dateCreatedFrom")) criteriaSize++;
				if (criteria.containsKey("dateCreatedTo")) criteriaSize++;
				if (criteria.containsKey("dateLastModifiedFrom")) criteriaSize++;
				if (criteria.containsKey("dateLastModifiedTo")) criteriaSize++;      
				if (criteria.containsKey("dateApprovedRejectedFrom")) criteriaSize++;
				if (criteria.containsKey("dateApprovedRejectedTo")) criteriaSize++;
				if (criteria.containsKey("datePostedFrom")) criteriaSize++;
				if (criteria.containsKey("datePostedTo")) criteriaSize++;
				
				Object[] obj = new Object[criteriaSize];
				
				Collection arSalesOrders = arSalesOrderHome.getSOByCriteria(this.getQueryByTxnTypeAndCriteria("ArSalesOrder", "so", criteria, branchList, obj, AD_CMPNY), obj);
				
				Iterator i = arSalesOrders.iterator();
				
				while (i.hasNext()) {
					
					LocalArSalesOrder arSalesOrder = (LocalArSalesOrder)i.next();
					
					AdRepTransactionSummaryDetails details = new AdRepTransactionSummaryDetails();
					
					details.setTxlType("AR SALES ORDER");		          		          
					details.setTxlDate(arSalesOrder.getSoDate());
					details.setTxlDocumentNumber(arSalesOrder.getSoDocumentNumber());		          
					details.setTxlApprovalStatus(arSalesOrder.getSoApprovalStatus());
					details.setTxlPosted(arSalesOrder.getSoPosted());
					details.setTxlCreatedBy(arSalesOrder.getSoCreatedBy());
					details.setTxlDateCreated(arSalesOrder.getSoDateCreated());
					details.setTxlLastModifiedBy(arSalesOrder.getSoLastModifiedBy());
					details.setTxlDateLastModified(arSalesOrder.getSoDateLastModified());
					details.setTxlApprovedRejectedBy(arSalesOrder.getSoApprovedRejectedBy());
					details.setTxlDateApprovedRejected(arSalesOrder.getSoDateApprovedRejected());
					details.setTxlPostedBy(arSalesOrder.getSoPostedBy());
					details.setTxlDatePosted(arSalesOrder.getSoDatePosted());
					details.setTxlOrderBy(ORDER_BY);
					
					Collection arSalesOrderLines = arSalesOrder.getArSalesOrderLines();
					Iterator j = arSalesOrderLines.iterator();
					
					double SO_AMNT = 0;
					
					while (j.hasNext()){
						
						LocalArSalesOrderLine arSalesOrderLine = (LocalArSalesOrderLine) j.next();
						
						SO_AMNT = SO_AMNT + arSalesOrderLine.getSolAmount();
						
					}
					
					details.setTxlAmount(SO_AMNT);
					
					list.add(details);		          
					
				}
				
			}			
			
			if (!criteria.containsKey("type") || ((String)criteria.get("type")).equals("CM ADJUSTMENT")) {
				
				int criteriaSize = 0;
				
				if (criteria.containsKey("docDateFrom")) criteriaSize++;
				if (criteria.containsKey("docDateTo")) criteriaSize++;
				if (criteria.containsKey("dateCreatedFrom")) criteriaSize++;
				if (criteria.containsKey("dateCreatedTo")) criteriaSize++;
				if (criteria.containsKey("dateLastModifiedFrom")) criteriaSize++;
				if (criteria.containsKey("dateLastModifiedTo")) criteriaSize++;      
				if (criteria.containsKey("dateApprovedRejectedFrom")) criteriaSize++;
				if (criteria.containsKey("dateApprovedRejectedTo")) criteriaSize++;
				if (criteria.containsKey("datePostedFrom")) criteriaSize++;
				if (criteria.containsKey("datePostedTo")) criteriaSize++;
				
				Object[] obj = new Object[criteriaSize];
				
				AdRepTransactionSummaryDetails details = new AdRepTransactionSummaryDetails();
				
				Collection cmAdjustments = cmAdjustmentHome.getAdjByCriteria(this.getQueryByTxnTypeAndCriteria("CmAdjustment", "adj", criteria, branchList, obj, AD_CMPNY), obj);
				
				Iterator i = cmAdjustments.iterator();
				
				while (i.hasNext()){
					
					
					LocalCmAdjustment cmAdjustment = (LocalCmAdjustment)i.next();
					
					details.setTxlType("CM ADJUSTMENT");
					details.setTxlDate(cmAdjustment.getAdjDate());
					details.setTxlDocumentNumber(cmAdjustment.getAdjReferenceNumber());		          
					details.setTxlAmount(cmAdjustment.getAdjAmount());		          
					details.setTxlApprovalStatus(cmAdjustment.getAdjApprovalStatus());
					details.setTxlPosted(cmAdjustment.getAdjPosted());
					details.setTxlCreatedBy(cmAdjustment.getAdjCreatedBy());
					details.setTxlDateCreated(cmAdjustment.getAdjDateCreated());
					details.setTxlLastModifiedBy(cmAdjustment.getAdjLastModifiedBy());
					details.setTxlDateLastModified(cmAdjustment.getAdjDateLastModified());
					details.setTxlApprovedRejectedBy(cmAdjustment.getAdjApprovedRejectedBy());
					details.setTxlDateApprovedRejected(cmAdjustment.getAdjDateApprovedRejected());
					details.setTxlPostedBy(cmAdjustment.getAdjPostedBy());
					details.setTxlDatePosted(cmAdjustment.getAdjDatePosted());
					details.setTxlOrderBy(ORDER_BY);
					
					
					list.add(details);		          
					
				}  
				
			}
			
			if (!criteria.containsKey("type") || ((String)criteria.get("type")).equals("CM FUND TRANSFER")) {
				
				int criteriaSize = 0;
				
				if (criteria.containsKey("docDateFrom")) criteriaSize++;
				if (criteria.containsKey("docDateTo")) criteriaSize++;
				if (criteria.containsKey("dateCreatedFrom")) criteriaSize++;
				if (criteria.containsKey("dateCreatedTo")) criteriaSize++;
				if (criteria.containsKey("dateLastModifiedFrom")) criteriaSize++;
				if (criteria.containsKey("dateLastModifiedTo")) criteriaSize++;      
				if (criteria.containsKey("dateApprovedRejectedFrom")) criteriaSize++;
				if (criteria.containsKey("dateApprovedRejectedTo")) criteriaSize++;
				if (criteria.containsKey("datePostedFrom")) criteriaSize++;
				if (criteria.containsKey("datePostedTo")) criteriaSize++;
				
				Object[] obj = new Object[criteriaSize];
				
				AdRepTransactionSummaryDetails details = new AdRepTransactionSummaryDetails();
				
				Collection cmFundTransfers = cmFundTransferHome.getFtByCriteria(this.getQueryByTxnTypeAndCriteria("CmFundTransfer", "ft", criteria, branchList, obj, AD_CMPNY), obj);
				
				Iterator i = cmFundTransfers.iterator();
				
				while (i.hasNext()) {
					
					LocalCmFundTransfer cmFundTransfer = (LocalCmFundTransfer)i.next();
					
					details.setTxlType("CM FUND TRANSFER");
					details.setTxlDate(cmFundTransfer.getFtDate());
					details.setTxlDocumentNumber(cmFundTransfer.getFtReferenceNumber());		          
					details.setTxlAmount(cmFundTransfer.getFtAmount());		          
					details.setTxlApprovalStatus(cmFundTransfer.getFtApprovalStatus());
					details.setTxlPosted(cmFundTransfer.getFtPosted());
					details.setTxlCreatedBy(cmFundTransfer.getFtCreatedBy());
					details.setTxlDateCreated(cmFundTransfer.getFtDateCreated());
					details.setTxlLastModifiedBy(cmFundTransfer.getFtLastModifiedBy());
					details.setTxlDateLastModified(cmFundTransfer.getFtDateLastModified());
					details.setTxlApprovedRejectedBy(cmFundTransfer.getFtApprovedRejectedBy());
					details.setTxlDateApprovedRejected(cmFundTransfer.getFtDateApprovedRejected());
					details.setTxlPostedBy(cmFundTransfer.getFtPostedBy());
					details.setTxlDatePosted(cmFundTransfer.getFtDatePosted());
					details.setTxlOrderBy(ORDER_BY);
					
					
					list.add(details);		          
					
				}
				
			}
			
			if (!criteria.containsKey("type") || ((String)criteria.get("type")).equals("INV ADJUSTMENT")) {
				
				int criteriaSize = 0;
				
				if (criteria.containsKey("docDateFrom")) criteriaSize++;
				if (criteria.containsKey("docDateTo")) criteriaSize++;
				if (criteria.containsKey("dateCreatedFrom")) criteriaSize++;
				if (criteria.containsKey("dateCreatedTo")) criteriaSize++;
				if (criteria.containsKey("dateLastModifiedFrom")) criteriaSize++;
				if (criteria.containsKey("dateLastModifiedTo")) criteriaSize++;      
				if (criteria.containsKey("dateApprovedRejectedFrom")) criteriaSize++;
				if (criteria.containsKey("dateApprovedRejectedTo")) criteriaSize++;
				if (criteria.containsKey("datePostedFrom")) criteriaSize++;
				if (criteria.containsKey("datePostedTo")) criteriaSize++;
				
				Object[] obj = new Object[criteriaSize];
				
				AdRepTransactionSummaryDetails details = new AdRepTransactionSummaryDetails();
				
				Collection invAdjustments = invAdjustmentHome.getAdjByCriteria(this.getQueryByTxnTypeAndCriteria("InvAdjustment", "adj", criteria, branchList, obj, AD_CMPNY), obj);;
				
				Iterator i = invAdjustments.iterator();
				
				while (i.hasNext()) {
					
					LocalInvAdjustment invAdjustment = (LocalInvAdjustment)i.next();
					
					details.setTxlType("INV ADJUSTMENT");	
					
					details.setTxlDate(invAdjustment.getAdjDate());
					details.setTxlDocumentNumber(invAdjustment.getAdjDocumentNumber());		          	          
					details.setTxlApprovalStatus(invAdjustment.getAdjApprovalStatus());
					details.setTxlPosted(invAdjustment.getAdjPosted());
					details.setTxlCreatedBy(invAdjustment.getAdjCreatedBy());
					details.setTxlDateCreated(invAdjustment.getAdjDateCreated());
					details.setTxlLastModifiedBy(invAdjustment.getAdjLastModifiedBy());
					details.setTxlDateLastModified(invAdjustment.getAdjDateLastModified());
					details.setTxlApprovedRejectedBy(invAdjustment.getAdjApprovedRejectedBy());
					details.setTxlDateApprovedRejected(invAdjustment.getAdjDateApprovedRejected());
					details.setTxlPostedBy(invAdjustment.getAdjPostedBy());
					details.setTxlDatePosted(invAdjustment.getAdjDatePosted());
					details.setTxlOrderBy(ORDER_BY);
					
					
					list.add(details);		          
					
				}
				
			}
			
			if (!criteria.containsKey("type") || ((String)criteria.get("type")).equals("INV OVERHEAD")) {
				
				int criteriaSize = 0;
				
				if (criteria.containsKey("docDateFrom")) criteriaSize++;
				if (criteria.containsKey("docDateTo")) criteriaSize++;
				if (criteria.containsKey("dateCreatedFrom")) criteriaSize++;
				if (criteria.containsKey("dateCreatedTo")) criteriaSize++;
				if (criteria.containsKey("dateLastModifiedFrom")) criteriaSize++;
				if (criteria.containsKey("dateLastModifiedTo")) criteriaSize++;      
				if (criteria.containsKey("dateApprovedRejectedFrom")) criteriaSize++;
				if (criteria.containsKey("dateApprovedRejectedTo")) criteriaSize++;
				if (criteria.containsKey("datePostedFrom")) criteriaSize++;
				if (criteria.containsKey("datePostedTo")) criteriaSize++;
				
				Object[] obj = new Object[criteriaSize];
				
				AdRepTransactionSummaryDetails details = new AdRepTransactionSummaryDetails();
				
				Collection invOverheads = invOverheadHome.getOhByCriteria(this.getQueryByTxnTypeAndCriteria("InvOverhead", "oh", criteria, branchList, obj, AD_CMPNY), obj);;
				
				Iterator i = invOverheads.iterator();
				
				while (i.hasNext()) {
					
					LocalInvOverhead invOverhead = (LocalInvOverhead)i.next();
					
					details.setTxlType("INV OVERHEAD");	
					
					details.setTxlDate(invOverhead.getOhDate());
					details.setTxlDocumentNumber(invOverhead.getOhDocumentNumber());		          	          
					details.setTxlPosted(invOverhead.getOhPosted());
					details.setTxlCreatedBy(invOverhead.getOhCreatedBy());
					details.setTxlDateCreated(invOverhead.getOhDateCreated());
					details.setTxlLastModifiedBy(invOverhead.getOhLastModifiedBy());
					details.setTxlDateLastModified(invOverhead.getOhDateLastModified());
					details.setTxlPostedBy(invOverhead.getOhPostedBy());
					details.setTxlDatePosted(invOverhead.getOhDatePosted());
					details.setTxlOrderBy(ORDER_BY);
					
					
					list.add(details);		          
					
				}
				
			}
			
			if (!criteria.containsKey("type") || ((String)criteria.get("type")).equals("INV BUILD ORDER")) {
				
				int criteriaSize = 0;
				
				if (criteria.containsKey("docDateFrom")) criteriaSize++;
				if (criteria.containsKey("docDateTo")) criteriaSize++;
				if (criteria.containsKey("dateCreatedFrom")) criteriaSize++;
				if (criteria.containsKey("dateCreatedTo")) criteriaSize++;
				if (criteria.containsKey("dateLastModifiedFrom")) criteriaSize++;
				if (criteria.containsKey("dateLastModifiedTo")) criteriaSize++;      
				if (criteria.containsKey("dateApprovedRejectedFrom")) criteriaSize++;
				if (criteria.containsKey("dateApprovedRejectedTo")) criteriaSize++;
				if (criteria.containsKey("datePostedFrom")) criteriaSize++;
				if (criteria.containsKey("datePostedTo")) criteriaSize++;
				
				Object[] obj = new Object[criteriaSize];
				
				AdRepTransactionSummaryDetails details = new AdRepTransactionSummaryDetails();
				
				Collection invBuildOrders = invBuildOrderHome.getBorByCriteria(this.getQueryByTxnTypeAndCriteria("InvBuildOrder", "bor", criteria, branchList, obj, AD_CMPNY), obj);;
				
				Iterator i = invBuildOrders.iterator();
				
				while (i.hasNext()) {
					
					LocalInvBuildOrder invBuildOrder = (LocalInvBuildOrder)i.next();
					
					details.setTxlType("INV BUILD ORDER");	
					
					details.setTxlDate(invBuildOrder.getBorDate());
					details.setTxlDocumentNumber(invBuildOrder.getBorDocumentNumber());		          	          
					details.setTxlApprovalStatus(invBuildOrder.getBorApprovalStatus());
					details.setTxlPosted(invBuildOrder.getBorPosted());
					details.setTxlCreatedBy(invBuildOrder.getBorCreatedBy());
					details.setTxlDateCreated(invBuildOrder.getBorDateCreated());
					details.setTxlLastModifiedBy(invBuildOrder.getBorLastModifiedBy());
					details.setTxlDateLastModified(invBuildOrder.getBorDateLastModified());
					details.setTxlApprovedRejectedBy(invBuildOrder.getBorApprovedRejectedBy());
					details.setTxlDateApprovedRejected(invBuildOrder.getBorDateApprovedRejected());
					details.setTxlPostedBy(invBuildOrder.getBorPostedBy());
					details.setTxlDatePosted(invBuildOrder.getBorDatePosted());
					details.setTxlOrderBy(ORDER_BY);
					
					
					list.add(details);		          
					
				}
				
			}
			
			if (!criteria.containsKey("type") || ((String)criteria.get("type")).equals("INV STOCK ISSUANCE")) {
				
				int criteriaSize = 0;
				
				if (criteria.containsKey("docDateFrom")) criteriaSize++;
				if (criteria.containsKey("docDateTo")) criteriaSize++;
				if (criteria.containsKey("dateCreatedFrom")) criteriaSize++;
				if (criteria.containsKey("dateCreatedTo")) criteriaSize++;
				if (criteria.containsKey("dateLastModifiedFrom")) criteriaSize++;
				if (criteria.containsKey("dateLastModifiedTo")) criteriaSize++;      
				if (criteria.containsKey("dateApprovedRejectedFrom")) criteriaSize++;
				if (criteria.containsKey("dateApprovedRejectedTo")) criteriaSize++;
				if (criteria.containsKey("datePostedFrom")) criteriaSize++;
				if (criteria.containsKey("datePostedTo")) criteriaSize++;
				
				Object[] obj = new Object[criteriaSize];
				
				AdRepTransactionSummaryDetails details = new AdRepTransactionSummaryDetails();
				
				Collection invStockIssuances = invStockIssuanceHome.getSiByCriteria(this.getQueryByTxnTypeAndCriteria("InvStockIssuance", "si", criteria, branchList, obj, AD_CMPNY), obj);;
				
				Iterator i = invStockIssuances.iterator();
				
				while (i.hasNext()) {
					
					LocalInvStockIssuance invStockIssuance = (LocalInvStockIssuance)i.next();
					
					details.setTxlType("INV STOCK ISSUANCE");	
					
					details.setTxlDate(invStockIssuance.getSiDate());
					details.setTxlDocumentNumber(invStockIssuance.getSiDocumentNumber());		          	          
					details.setTxlApprovalStatus(invStockIssuance.getSiApprovalStatus());
					details.setTxlPosted(invStockIssuance.getSiPosted());
					details.setTxlCreatedBy(invStockIssuance.getSiCreatedBy());
					details.setTxlDateCreated(invStockIssuance.getSiDateCreated());
					details.setTxlLastModifiedBy(invStockIssuance.getSiLastModifiedBy());
					details.setTxlDateLastModified(invStockIssuance.getSiDateLastModified());
					details.setTxlApprovedRejectedBy(invStockIssuance.getSiApprovedRejectedBy());
					details.setTxlDateApprovedRejected(invStockIssuance.getSiDateApprovedRejected());
					details.setTxlPostedBy(invStockIssuance.getSiPostedBy());
					details.setTxlDatePosted(invStockIssuance.getSiDatePosted());
					details.setTxlOrderBy(ORDER_BY);
					
					
					list.add(details);		          
					
				}
				
			}
			
			if (!criteria.containsKey("type") || ((String)criteria.get("type")).equals("INV ASSEMBLY TRANSFER")) {
				
				int criteriaSize = 0;
				
				if (criteria.containsKey("docDateFrom")) criteriaSize++;
				if (criteria.containsKey("docDateTo")) criteriaSize++;
				if (criteria.containsKey("dateCreatedFrom")) criteriaSize++;
				if (criteria.containsKey("dateCreatedTo")) criteriaSize++;
				if (criteria.containsKey("dateLastModifiedFrom")) criteriaSize++;
				if (criteria.containsKey("dateLastModifiedTo")) criteriaSize++;      
				if (criteria.containsKey("dateApprovedRejectedFrom")) criteriaSize++;
				if (criteria.containsKey("dateApprovedRejectedTo")) criteriaSize++;
				if (criteria.containsKey("datePostedFrom")) criteriaSize++;
				if (criteria.containsKey("datePostedTo")) criteriaSize++;
				
				Object[] obj = new Object[criteriaSize];
				
				AdRepTransactionSummaryDetails details = new AdRepTransactionSummaryDetails();
				
				Collection invAssemblyTransfers = invAssemblyTransferHome.getAtrByCriteria(this.getQueryByTxnTypeAndCriteria("InvAssemblyTransfer", "atr", criteria, branchList, obj, AD_CMPNY), obj);;
				
				Iterator i = invAssemblyTransfers.iterator();
				
				while (i.hasNext()) {
					
					LocalInvAssemblyTransfer invAssemblyTransfer = (LocalInvAssemblyTransfer)i.next();
					
					details.setTxlType("INV ASSEMBLY TRANSFER");	
					
					details.setTxlDate(invAssemblyTransfer.getAtrDate());
					details.setTxlDocumentNumber(invAssemblyTransfer.getAtrDocumentNumber());		          	          
					details.setTxlApprovalStatus(invAssemblyTransfer.getAtrApprovalStatus());
					details.setTxlPosted(invAssemblyTransfer.getAtrPosted());
					details.setTxlCreatedBy(invAssemblyTransfer.getAtrCreatedBy());
					details.setTxlDateCreated(invAssemblyTransfer.getAtrDateCreated());
					details.setTxlLastModifiedBy(invAssemblyTransfer.getAtrLastModifiedBy());
					details.setTxlDateLastModified(invAssemblyTransfer.getAtrDateLastModified());
					details.setTxlApprovedRejectedBy(invAssemblyTransfer.getAtrApprovedRejectedBy());
					details.setTxlDateApprovedRejected(invAssemblyTransfer.getAtrDateApprovedRejected());
					details.setTxlPostedBy(invAssemblyTransfer.getAtrPostedBy());
					details.setTxlDatePosted(invAssemblyTransfer.getAtrDatePosted());
					details.setTxlOrderBy(ORDER_BY);
					
					
					list.add(details);		          
					
				}
				
			}
			
			if (!criteria.containsKey("type") || ((String)criteria.get("type")).equals("INV BUILD ASSEMBLY")) {
				
				int criteriaSize = 0;
				
				if (criteria.containsKey("docDateFrom")) criteriaSize++;
				if (criteria.containsKey("docDateTo")) criteriaSize++;
				if (criteria.containsKey("dateCreatedFrom")) criteriaSize++;
				if (criteria.containsKey("dateCreatedTo")) criteriaSize++;
				if (criteria.containsKey("dateLastModifiedFrom")) criteriaSize++;
				if (criteria.containsKey("dateLastModifiedTo")) criteriaSize++;      
				if (criteria.containsKey("dateApprovedRejectedFrom")) criteriaSize++;
				if (criteria.containsKey("dateApprovedRejectedTo")) criteriaSize++;
				if (criteria.containsKey("datePostedFrom")) criteriaSize++;
				if (criteria.containsKey("datePostedTo")) criteriaSize++;
				
				Object[] obj = new Object[criteriaSize];
				
				AdRepTransactionSummaryDetails details = new AdRepTransactionSummaryDetails();
				
				Collection invBuildAssemblies = invBuildAssemblyHome.getBuaByCriteria(this.getQueryByTxnTypeAndCriteria("InvBuildUnbuildAssembly", "bua", criteria, branchList, obj, AD_CMPNY), obj);;
				
				Iterator i = invBuildAssemblies.iterator();
				
				while (i.hasNext()) {
					
					LocalInvBuildUnbuildAssembly invBuildAssembly = (LocalInvBuildUnbuildAssembly)i.next();
					
					details.setTxlType("INV BUILD ASSEMBLY");	
					
					details.setTxlDate(invBuildAssembly.getBuaDate());
					details.setTxlDocumentNumber(invBuildAssembly.getBuaDocumentNumber());		          	          
					details.setTxlApprovalStatus(invBuildAssembly.getBuaApprovalStatus());
					details.setTxlPosted(invBuildAssembly.getBuaPosted());
					details.setTxlCreatedBy(invBuildAssembly.getBuaCreatedBy());
					details.setTxlDateCreated(invBuildAssembly.getBuaDateCreated());
					details.setTxlLastModifiedBy(invBuildAssembly.getBuaLastModifiedBy());
					details.setTxlDateLastModified(invBuildAssembly.getBuaDateLastModified());
					details.setTxlApprovedRejectedBy(invBuildAssembly.getBuaApprovedRejectedBy());
					details.setTxlDateApprovedRejected(invBuildAssembly.getBuaDateApprovedRejected());
					details.setTxlPostedBy(invBuildAssembly.getBuaPostedBy());
					details.setTxlDatePosted(invBuildAssembly.getBuaDatePosted());
					details.setTxlOrderBy(ORDER_BY);
					
					
					list.add(details);		          
					
				}
				
			}
			
			if (list.isEmpty()) throw new GlobalNoRecordFoundException();
			
			Collections.sort(list);
			
			return list;
			
		} catch (GlobalNoRecordFoundException ex) {
			
			throw ex;
			
		} catch (Exception ex) {
			
			
			ex.printStackTrace();
			throw new EJBException(ex.getMessage());
			
		}
		
	}
	
	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public com.util.AdCompanyDetails getAdCompany(Integer AD_CMPNY) {
		
		Debug.print("AdRepTransactionSummaryControllerBean getAdCompany");      
		
		LocalAdCompanyHome adCompanyHome = null;
		
		// Initialize EJB Home
		
		try {
			
			adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
			
		} catch (NamingException ex) {
			
			throw new EJBException(ex.getMessage());
			
		}
		
		try {
			
			LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
			
			AdCompanyDetails details = new AdCompanyDetails();
			details.setCmpName(adCompany.getCmpName());
			
			return details;  	
			
		} catch (Exception ex) {
			
			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());
			
		}
		
	}

	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public ArrayList getAdBrResAll(Integer RS_CODE, Integer AD_CMPNY) 
	throws GlobalNoRecordFoundException{
		
		Debug.print("AdRepBankAccountListControllerBean getAdBrResAll");
		
		LocalAdBranchResponsibilityHome adBranchResponsibilityHome = null;
		LocalAdBranchHome adBranchHome = null;
		
		LocalAdBranchResponsibility adBranchResponsibility = null;
		LocalAdBranch adBranch = null;
		
		Collection adBranchResponsibilities = null;
		
		ArrayList list = new ArrayList();
		
		// Initialize EJB Home
		
		try {
			
			adBranchResponsibilityHome = (LocalAdBranchResponsibilityHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdBranchResponsibilityHome.JNDI_NAME, LocalAdBranchResponsibilityHome.class);
			adBranchHome = (LocalAdBranchHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdBranchHome.JNDI_NAME, LocalAdBranchHome.class);
			
		} catch (NamingException ex) {
			
			throw new EJBException(ex.getMessage());
			
		}
		
		try {
			
			adBranchResponsibilities = adBranchResponsibilityHome.findByAdResponsibility(RS_CODE, AD_CMPNY);
			
		} catch (FinderException ex) {
			
		} catch (Exception ex) {
			
			throw new EJBException(ex.getMessage());
		}
		
		if (adBranchResponsibilities.isEmpty()) {
			
			throw new GlobalNoRecordFoundException();
			
		}
		
		try {
			
			Iterator i = adBranchResponsibilities.iterator();
			
			while(i.hasNext()) {
				
				adBranchResponsibility = (LocalAdBranchResponsibility)i.next();
				
				adBranch = adBranchResponsibility.getAdBranch();
				
				AdBranchDetails details = new AdBranchDetails();
				
				details.setBrCode(adBranch.getBrCode());
				details.setBrBranchCode(adBranch.getBrBranchCode());
				details.setBrName(adBranch.getBrName());
				details.setBrHeadQuarter(adBranch.getBrHeadQuarter());
				
				list.add(details);
				
			}	               
			
		} catch (Exception ex) {
			
			throw new EJBException(ex.getMessage());
		}
		
		return list;
		
	}	
	
	// private methods
	
	private String getQueryByTxnTypeAndCriteria(String table, String tablePrefix, HashMap criteria, ArrayList branchList, Object[] obj, Integer AD_CMPNY) 
	throws GlobalNoRecordFoundException {
		
		StringBuffer jbossQl = new StringBuffer();      
		int ctr = 0;
		boolean firstArgument = true; 
		
		jbossQl.append("SELECT OBJECT(" + tablePrefix + ") FROM " + table + " " + tablePrefix + " ");
		
		if (branchList.isEmpty()) {
		  	
		  	throw new GlobalNoRecordFoundException();
		  	
		  }
		  else {
		  	
		  	jbossQl.append("WHERE " + tablePrefix + "." + tablePrefix + "AdBranch in (");
		  	
		  	boolean firstLoop = true;
		  	
		  	Iterator j = branchList.iterator();
		  	
		  	while(j.hasNext()) {
		  		
		  		if(firstLoop == false) { 
		  			jbossQl.append(", "); 
		  		}
		  		else { 
		  			firstLoop = false; 
		  		}
		  		
		  		AdBranchDetails mdetails = (AdBranchDetails) j.next();
		  		
		  		jbossQl.append(mdetails.getBrCode());
		  		
		  	}
		  	
		  	jbossQl.append(") ");
		  	
		  	firstArgument = false;
		  	
		  }
		
		if (criteria.containsKey("createdBy")) {
			
			if (!firstArgument) {
				
				jbossQl.append("AND ");
				
			} else {
				
				firstArgument = false;
				jbossQl.append("WHERE ");
				
			}
			
			jbossQl.append(tablePrefix + "." + tablePrefix + "CreatedBy = '" + (String)criteria.get("createdBy") + "' ");
			
		}
		
		if (criteria.containsKey("dateCreatedFrom")) {
			
			
			
			if (!firstArgument) {
				
				jbossQl.append("AND ");
				
			} else {
				
				firstArgument = false;
				jbossQl.append("WHERE ");
				
			}
			
			jbossQl.append(tablePrefix + "." + tablePrefix + "DateCreated >= ?" + (ctr+1) + " ");
			obj[ctr] = (Date)criteria.get("dateCreatedFrom");
			ctr++;
			
		}
		
		if (criteria.containsKey("dateCreatedTo")) {
			
			if (!firstArgument) {
				
				jbossQl.append("AND ");
				
			} else {
				
				firstArgument = false;
				jbossQl.append("WHERE ");
				
			}
			
			jbossQl.append(tablePrefix + "." + tablePrefix + "DateCreated <= ?" + (ctr+1) + " ");
			GregorianCalendar gc = new GregorianCalendar();
			gc.setTime((Date)criteria.get("dateCreatedTo"));
			gc.set(gc.get(Calendar.YEAR), gc.get(Calendar.MONTH), gc.get(Calendar.DATE), 23, 59, 59);      	 
			obj[ctr] = gc.getTime();
			ctr++;
			
		}
		
		if (criteria.containsKey("docDateFrom")) {
			
			String effectiveDate = "";
			
			if (table.equals("GlJournal")) {
				
				effectiveDate = "Effective";
				
			}
			
			if (!firstArgument) {
				
				
				jbossQl.append("AND ");
				
			} else {
				
				firstArgument = false;
				jbossQl.append("WHERE ");
				
			}
			
			jbossQl.append(tablePrefix + "." + tablePrefix + effectiveDate + "Date >= ?" + (ctr+1) + " ");
			obj[ctr] = (Date)criteria.get("docDateFrom");
			ctr++;
			
		}
		
		if (criteria.containsKey("docDateTo")) {
			
			String effectiveDate = "";
			
			if (table.equals("GlJournal")) {
				
				effectiveDate = "Effective";
				
			}
			
			if (!firstArgument) {
				
				jbossQl.append("AND ");
				
			} else {
				
				firstArgument = false;
				jbossQl.append("WHERE ");
				
			}
			
			jbossQl.append(tablePrefix + "." + tablePrefix + effectiveDate + "Date <= ?" + (ctr+1) + " ");
			GregorianCalendar gc = new GregorianCalendar();
			gc.setTime((Date)criteria.get("docDateTo"));
			gc.set(gc.get(Calendar.YEAR), gc.get(Calendar.MONTH), gc.get(Calendar.DATE), 23, 59, 59);      	 
			obj[ctr] = gc.getTime();
			ctr++;
			
		}
		
		if (criteria.containsKey("lastModifiedBy")) {
			
			if (!firstArgument) {
				
				jbossQl.append("AND ");
				
			} else {
				
				firstArgument = false;
				jbossQl.append("WHERE ");
				
			}
			
			jbossQl.append(tablePrefix + "." + tablePrefix + "LastModifiedBy = '" + (String)criteria.get("lastModifiedBy") + "' ");
			
			
		}
		
		if (criteria.containsKey("dateLastModifiedFrom")) {
			
			if (!firstArgument) {
				
				jbossQl.append("AND ");
				
			} else {
				
				firstArgument = false;
				jbossQl.append("WHERE ");
				
			}
			
			jbossQl.append(tablePrefix + "." + tablePrefix + "DateLastModified >= ?" + (ctr+1) + " ");
			obj[ctr] = (Date)criteria.get("dateLastModifiedFrom");
			ctr++;
			
		}
		
		if (criteria.containsKey("dateLastModifiedTo")) {
			
			if (!firstArgument) {
				
				jbossQl.append("AND ");
				
			} else {
				
				firstArgument = false;
				jbossQl.append("WHERE ");
				
			}
			
			jbossQl.append(tablePrefix + "." + tablePrefix + "DateLastModified <= ?" + (ctr+1) + " ");
			GregorianCalendar gc = new GregorianCalendar();
			gc.setTime((Date)criteria.get("dateLastModifiedTo"));
			gc.set(gc.get(Calendar.YEAR), gc.get(Calendar.MONTH), gc.get(Calendar.DATE), 23, 59, 59);      	 
			obj[ctr] = gc.getTime();
			ctr++;
			
		}
		
		if (criteria.containsKey("approvedRejectedBy")) {
			
			if (!firstArgument) {
				
				jbossQl.append("AND ");
				
			} else {
				
				firstArgument = false;
				jbossQl.append("WHERE ");
				
			}
			
			jbossQl.append(tablePrefix + "." + tablePrefix + "ApprovedRejectedBy = '" + (String)criteria.get("approvedRejectedBy") + "' ");
			
		}
		
		if (criteria.containsKey("dateApprovedRejectedFrom")) {
			
			if (!firstArgument) {
				
				jbossQl.append("AND ");
				
			} else {
				
				firstArgument = false;
				jbossQl.append("WHERE ");
				
			}
			
			jbossQl.append(tablePrefix + "." + tablePrefix + "DateApprovedRejected >= ?" + (ctr+1) + " ");
			obj[ctr] = (Date)criteria.get("dateApprovedRejectedFrom");
			ctr++;
			
		}
		
		if (criteria.containsKey("dateApprovedRejectedTo")) {
			
			if (!firstArgument) {
				
				jbossQl.append("AND ");
				
			} else {
				
				firstArgument = false;
				jbossQl.append("WHERE ");
				
			}
			
			jbossQl.append(tablePrefix + "." + tablePrefix + "DateApprovedRejected <= ?" + (ctr+1) + " ");
			obj[ctr] = (Date)criteria.get("dateApprovedRejectedTo");
			ctr++;
			
		}
		
		
		if (criteria.containsKey("postedBy")) {
			
			if (!firstArgument) {
				
				jbossQl.append("AND ");
				
			} else {
				
				firstArgument = false;
				jbossQl.append("WHERE ");
				
			}
			
			jbossQl.append(tablePrefix + "." + tablePrefix + "PostedBy = '" + (String)criteria.get("postedBy") + "' ");      	 
			
		}
		
		if (criteria.containsKey("datePostedFrom")) {
			
			if (!firstArgument) {
				
				jbossQl.append("AND ");
				
			} else {
				
				firstArgument = false;
				jbossQl.append("WHERE ");
				
			}
			
			jbossQl.append(tablePrefix + "." + tablePrefix + "DatePosted >= ?" + (ctr+1) + " ");
			obj[ctr] = (Date)criteria.get("datePostedFrom");
			ctr++;
			
		}
		
		if (criteria.containsKey("datePostedTo")) {
			
			if (!firstArgument) {
				
				jbossQl.append("AND ");
				
			} else {
				
				firstArgument = false;
				jbossQl.append("WHERE ");
				
			}
			
			jbossQl.append(tablePrefix + "." + tablePrefix + "DatePosted <= ?" + (ctr+1) + " ");
			obj[ctr] = (Date)criteria.get("datePostedTo");
			ctr++;
			
		}
		
		if (!firstArgument) {
			
			jbossQl.append("AND ");
			
		} else {
			
			firstArgument = false;
			jbossQl.append("WHERE ");
			
		}
		
		jbossQl.append(tablePrefix + "." + tablePrefix + "AdCompany=" + AD_CMPNY + " ");
		
		return jbossQl.toString();
		
	}
	
	
	// SessionBean methods
	
	/**
	 * @ejb:create-method view-type="remote"
	 **/
	public void ejbCreate() throws CreateException {
		
		Debug.print("AdRepTransactionSummaryControllerBean ejbCreate");
		
	}
}
