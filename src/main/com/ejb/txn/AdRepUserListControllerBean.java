
/*
 * AdRepUserListControllerBean.java
 *
 * Created on Jun 22, 2005, 04:15 PM
 *
 * @author  Jolly T. Martin
 */

package com.ejb.txn;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.naming.NamingException;

import com.ejb.ad.LocalAdCompany;
import com.ejb.ad.LocalAdCompanyHome;
import com.ejb.ad.LocalAdUser;
import com.ejb.ad.LocalAdUserHome;
import com.ejb.ad.LocalAdUserResponsibility;
import com.ejb.exception.GlobalNoRecordFoundException;
import com.util.AbstractSessionBean;
import com.util.AdCompanyDetails;
import com.util.AdRepUserListDetails;
import com.util.Debug;
import com.util.EJBHomeFactory;

/**
 * @ejb:bean name="AdRepUserListControllerEJB"
 *           display-name="Used for viewing user lists"
 *           type="Stateless"
 *           view-type="remote"
 *           jndi-name="ejb/AdRepUserListControllerEJB"
 *
 * @ejb:interface remote-class="com.ejb.txn.AdRepUserListController"
 *                extends="javax.ejb.EJBObject"
 *
 * @ejb:home remote-class="com.ejb.txn.AdRepUserListControllerHome"
 *           extends="javax.ejb.EJBHome"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="aduser"
 *                        role-link="aduserlink"
 *
 * @ejb:permission role-name="aduser"
 * 
 */

public class AdRepUserListControllerBean extends AbstractSessionBean {
	
	
	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public ArrayList executeAdRepUserList(HashMap criteria, String ORDER_BY, Integer AD_CMPNY) 
	throws GlobalNoRecordFoundException {
		
		Debug.print("AdRepUserListControllerBean executeAdRepUserList");
		
		LocalAdUserHome adUserHome = null;
		
		ArrayList list = new ArrayList();
		
		//initialized EJB Home
		
		try {
			
			adUserHome = (LocalAdUserHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdUserHome.JNDI_NAME, LocalAdUserHome.class);
			
		} catch (NamingException ex) 	{
			
			throw new EJBException(ex.getMessage());
			
		}
		
		try { 
			
			StringBuffer jbossQl = new StringBuffer();
			jbossQl.append("SELECT OBJECT(usr) FROM AdUser usr ");
			
			boolean firstArgument = true;
			short ctr = 0;
			int criteriaSize = criteria.size();	      
			
			Object obj[];	      
			
			// Allocate the size of the object parameter
			
			if (criteria.containsKey("userName")) {
				
				criteriaSize--;
				
			}
			
			obj = new Object[criteriaSize];
			
			if (criteria.containsKey("userName")) {
				
				if (!firstArgument) {
					
					jbossQl.append("AND ");	
					
				} else {
					
					firstArgument = false;
					jbossQl.append("WHERE ");
					
				}
				
				jbossQl.append("usr.usrName LIKE '%" + (String)criteria.get("userName") + "%' ");
				
			}
			
			if (criteria.containsKey("dateFrom")) {
				
				if (!firstArgument) {		       	  	
					jbossQl.append("AND ");		       	     
				} else {		       	  	
					firstArgument = false;
					jbossQl.append("WHERE ");		       	  	 
				}
				
				jbossQl.append("usr.usrDateFrom=?" + (ctr+1) + " ");
				obj[ctr] = (String)criteria.get("dateFrom");
				ctr++;
				
			}
			
			if (criteria.containsKey("dateTo")) {
				
				if (!firstArgument) {		       	  	
					jbossQl.append("AND ");		       	     
				} else {		       	  	
					firstArgument = false;
					jbossQl.append("WHERE ");		       	  	 
				}
				
				jbossQl.append("usr.usrDateTo=?" + (ctr+1) + " ");
				obj[ctr] = (String)criteria.get("dateTo");
				ctr++;
				
			} 
			
			if (!firstArgument) {
				jbossQl.append("AND ");
			} else {
				firstArgument = false;
				jbossQl.append("WHERE ");
			}
			
			jbossQl.append("usr.usrAdCompany=" + AD_CMPNY + " ");
			
			String orderBy = null;
			
			if (ORDER_BY.equals("USER NAME")) {
				
				orderBy = "usr.usrName";
				
			} else if (ORDER_BY.equals("USER DESC")) {
				
				orderBy = "usr.usrDescription";
				
			}
			
			if (orderBy != null) {
				
				jbossQl.append("ORDER BY " + orderBy);
				
			}
			
			
			Collection adUsers = adUserHome.getUsrByCriteria(jbossQl.toString(), obj);
			
			if(adUsers.size() == 0)
				throw new GlobalNoRecordFoundException();
			
			Iterator i = adUsers.iterator();
			
			while (i.hasNext()) {
				
				LocalAdUser adUser = (LocalAdUser)i.next();		  	  		  	  
				
				Collection usrResponsibilities = adUser.getAdUserResponsibilities();
				
				Iterator itrURS = usrResponsibilities.iterator();
				
				while(itrURS.hasNext()) {
					
					AdRepUserListDetails details = new AdRepUserListDetails();
					
					LocalAdUserResponsibility usrResponsibility = (LocalAdUserResponsibility)itrURS.next();
										
					details.setUlUserName(adUser.getUsrName());
					details.setUlUserDescription(adUser.getUsrDescription());
					details.setUlPasswordExpirationCode(adUser.getUsrPasswordExpirationCode());
					details.setUlPasswordExpirationDays(adUser.getUsrPasswordExpirationDays());
					details.setUlPasswordExpirationAccess(adUser.getUsrPasswordExpirationAccess());
					details.setUlDateFrom(adUser.getUsrDateFrom());
					details.setUlDateTo(adUser.getUsrDateTo());
					details.setUlResponsibilityName(usrResponsibility.getAdResponsibility().getRsName());
					details.setUlResponsibilityDescription(usrResponsibility.getAdResponsibility().getRsDescription());
					details.setUlResponsibilityDateFrom(usrResponsibility.getUrDateFrom());
					details.setUlResponsibilityDateTo(usrResponsibility.getUrDateTo());
					
					list.add(details);
					
				}		  	  
				
			}
			
			return list;
			
		} catch (GlobalNoRecordFoundException ex) {
			
			throw ex;
			
		} catch (Exception ex) {
			
			
			ex.printStackTrace();
			throw new EJBException(ex.getMessage());
			
		}
		
	}
	
	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public com.util.AdCompanyDetails getAdCompany(Integer AD_CMPNY) {
		
		Debug.print("AdRepUserListControllerBean getAdCompany");      
		
		LocalAdCompanyHome adCompanyHome = null;
		
		// Initialize EJB Home
		
		try {
			
			adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
			
		} catch (NamingException ex) {
			
			throw new EJBException(ex.getMessage());
			
		}
		
		try {
			
			LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
			
			AdCompanyDetails details = new AdCompanyDetails();
			details.setCmpName(adCompany.getCmpName());
			
			return details;  	
			
		} catch (Exception ex) {
			
			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());
			
		}
		
	}    
	
	// SessionBean methods
	
	/**
	 * @ejb:create-method view-type="remote"
	 **/
	public void ejbCreate() throws CreateException {
		
		Debug.print("AdRepUserListControllerBean ejbCreate");
		
	}
}
