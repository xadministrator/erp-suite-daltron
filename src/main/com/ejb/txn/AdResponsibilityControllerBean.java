
/*
 * AdResponsibilityControllerBean.java
 *
 * Created on June 17, 2003, 10:31 AM
 * @author  Enrico C. Yap
 */

package com.ejb.txn;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;
import javax.ejb.RemoveException;
import javax.naming.NamingException;

import com.ejb.ad.LocalAdBranch;
import com.ejb.ad.LocalAdBranchHome;
import com.ejb.ad.LocalAdBranchResponsibility;
import com.ejb.ad.LocalAdBranchResponsibilityHome;
import com.ejb.ad.LocalAdFormFunction;
import com.ejb.ad.LocalAdFormFunctionHome;
import com.ejb.ad.LocalAdFormFunctionResponsibility;
import com.ejb.ad.LocalAdFormFunctionResponsibilityHome;
import com.ejb.ad.LocalAdResponsibility;
import com.ejb.ad.LocalAdResponsibilityHome;
import com.ejb.exception.AdRSResponsibilityNotAssignedToBranchException;
import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.exception.GlobalRecordAlreadyAssignedException;
import com.ejb.exception.GlobalRecordAlreadyDeletedException;
import com.ejb.exception.GlobalRecordAlreadyExistException;
import com.util.AbstractSessionBean;
import com.util.AdBranchDetails;
import com.util.AdModFormFunctionResponsibilityDetails;
import com.util.AdResponsibilityDetails;
import com.util.Debug;
import com.util.EJBHomeFactory;

/**
 * @ejb:bean name="AdResponsibilityControllerEJB"
 *           display-name="Used for entering reponsibilities"
 *           type="Stateless"
 *           view-type="remote"
 *           jndi-name="ejb/AdResponsibilityControllerEJB"
 *
 * @ejb:interface remote-class="com.ejb.txn.AdResponsibilityController"
 *                extends="javax.ejb.EJBObject"
 *
 * @ejb:home remote-class="com.ejb.txn.AdResponsibilityControllerHome"
 *           extends="javax.ejb.EJBHome"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="aruser"
 *                        role-link="aruserlink"
 *
 * @ejb:permission role-name="aruser"
 * 
*/

public class AdResponsibilityControllerBean extends AbstractSessionBean {
    
    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList getAdRsAll(Integer AD_CMPNY) 
        throws GlobalNoRecordFoundException {
                    
        Debug.print("AdResponsibilityControllerBean getAdRsAll");

        LocalAdResponsibilityHome adResponsibilityHome = null;

        Collection adResponsibilities = null;

        LocalAdResponsibility adResponsibility = null;

        ArrayList list = new ArrayList();
        
        // Initialize EJB Home

        try {
            
            adResponsibilityHome = (LocalAdResponsibilityHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdResponsibilityHome.JNDI_NAME, LocalAdResponsibilityHome.class);

        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }

        try {
            
            adResponsibilities = adResponsibilityHome.findRsAll(AD_CMPNY);
            
        } catch (FinderException ex) {
            
        } catch (Exception ex) {
            
            throw new EJBException(ex.getMessage());
        }

        if (adResponsibilities.isEmpty()) {
        	
            throw new GlobalNoRecordFoundException();
        	
        }
        
        Iterator i = adResponsibilities.iterator();
               
        while (i.hasNext()) {
        	
        	adResponsibility = (LocalAdResponsibility)i.next();
                                                                        
        	AdResponsibilityDetails details = new AdResponsibilityDetails();
        		details.setRsCode(adResponsibility.getRsCode());        		
                details.setRsName(adResponsibility.getRsName());
                details.setRsDescription(adResponsibility.getRsDescription());
                details.setRsDateFrom(adResponsibility.getRsDateFrom());
                details.setRsDateTo(adResponsibility.getRsDateTo());
                                                        	
        	list.add(details);
        }
        
        return list;
            
    }
    
    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    
    public ArrayList getAdBrAll(Integer AD_CMPNY) 
    	throws GlobalNoRecordFoundException{
    	
    	Debug.print("AdResponsibilityControllerBean getAdBrAll");

    	LocalAdBranchHome adBranchHome = null;
    	LocalAdBranch adBranch = null;

    	Collection adBranches = null;
    	
        ArrayList list = new ArrayList();
        
        // Initialize EJB Home

        try {
            
            adBranchHome = (LocalAdBranchHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdBranchHome.JNDI_NAME, LocalAdBranchHome.class);

        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {
        	
        	adBranches = adBranchHome.findBrAll(AD_CMPNY);
		
        } catch (FinderException ex) {
            
        } catch (Exception ex) {
            
            throw new EJBException(ex.getMessage());
        }
        
        if (adBranches.isEmpty()) {
        	
            throw new GlobalNoRecordFoundException();
        	
        }
        
        Iterator i = adBranches.iterator();
        
        while(i.hasNext()) {
        	
        	adBranch = (LocalAdBranch)i.next();
        	
        	AdBranchDetails details = new AdBranchDetails();
        	details.setBrCode(adBranch.getBrCode());
        	details.setBrBranchCode(adBranch.getBrBranchCode());
        	details.setBrName(adBranch.getBrName());
        			 
        	list.add(details);
        	
        }
        
        return list;
    	
    }
    
    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    
    public ArrayList getAdBrResAll(Integer RS_CODE, Integer AD_CMPNY) 
    	throws GlobalNoRecordFoundException{
    	
    	Debug.print("AdResponsibilityControllerBean getAdBrResAll");

    	LocalAdBranchResponsibilityHome adBranchResponsibilityHome = null;
    	LocalAdBranchHome adBranchHome = null;
    	
    	LocalAdBranchResponsibility adBranchResponsibility = null;
    	LocalAdBranch adBranch = null;

    	Collection adBranchResponsibilities = null;
    	
        ArrayList list = new ArrayList();
        
        // Initialize EJB Home

        try {
            
            adBranchResponsibilityHome = (LocalAdBranchResponsibilityHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdBranchResponsibilityHome.JNDI_NAME, LocalAdBranchResponsibilityHome.class);
            adBranchHome = (LocalAdBranchHome)EJBHomeFactory.
            	lookUpLocalHome(LocalAdBranchHome.JNDI_NAME, LocalAdBranchHome.class);

        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {
        	
        	adBranchResponsibilities = adBranchResponsibilityHome.findByAdResponsibility(RS_CODE, AD_CMPNY);
		
        } catch (FinderException ex) {
            
        } catch (Exception ex) {
            
            throw new EJBException(ex.getMessage());
        }
        
        if (adBranchResponsibilities.isEmpty()) {
        	
            throw new GlobalNoRecordFoundException();
        	
        }
        
        try {
        
	        Iterator i = adBranchResponsibilities.iterator();
	        
	        while(i.hasNext()) {
	        	
	        	adBranchResponsibility = (LocalAdBranchResponsibility)i.next();
	        		
		    	adBranch = adBranchHome.findByPrimaryKey(adBranchResponsibility.getAdBranch().getBrCode());
		    	
		    	AdBranchDetails details = new AdBranchDetails();
		    	details.setBrCode(adBranch.getBrCode());
		    	details.setBrBranchCode(adBranch.getBrBranchCode());
		    	details.setBrName(adBranch.getBrName());
		    	
		    	list.add(details);
	            	
	        }
	        
        } catch (FinderException ex) {
            
        } catch (Exception ex) {
            
            throw new EJBException(ex.getMessage());
        }
        
        return list;
    	
    }
    
    /**
     * @ejb:interface-method view-type="remote"
     **/    
    public void addAdRsEntry(com.util.AdResponsibilityDetails details, ArrayList branchList, Integer AD_CMPNY) 
        throws GlobalRecordAlreadyExistException, AdRSResponsibilityNotAssignedToBranchException {
                    
        Debug.print("AdResponsibilityControllerBean addAdRsEntry");
        
        LocalAdResponsibilityHome adResponsibilityHome = null;
        LocalAdBranchHome adBranchHome = null;
        LocalAdBranchResponsibilityHome adBranchResponsibilityHome = null;
               
        LocalAdResponsibility adResponsibility = null;
        LocalAdBranch adBranch = null;
        LocalAdBranchResponsibility adBranchResponsibility = null;
        
        ArrayList list = new ArrayList();
        
        // Initialize EJB Home
        
        try {
            
            adResponsibilityHome = (LocalAdResponsibilityHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdResponsibilityHome.JNDI_NAME, LocalAdResponsibilityHome.class);
            adBranchHome = (LocalAdBranchHome)EJBHomeFactory.
            	lookUpLocalHome(LocalAdBranchHome.JNDI_NAME, LocalAdBranchHome.class);
            adBranchResponsibilityHome = (LocalAdBranchResponsibilityHome)EJBHomeFactory.
        		lookUpLocalHome(LocalAdBranchResponsibilityHome.JNDI_NAME, LocalAdBranchResponsibilityHome.class);
                                                      
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        if(branchList.size() == 0) {
        	
        	throw new AdRSResponsibilityNotAssignedToBranchException();
        	
        }

        try { 
            
            adResponsibility = adResponsibilityHome.findByRsName(details.getRsName(), AD_CMPNY);
                       
            throw new GlobalRecordAlreadyExistException();
                                             
         } catch (GlobalRecordAlreadyExistException ex) {
        	
        	throw ex;
        	
         } catch (FinderException ex) {
        	 	
         } catch (Exception ex) {
            
            throw new EJBException(ex.getMessage());
            
         }
                            
        try {
        	
        	// create new responsibility

        	adResponsibility = adResponsibilityHome.create(details.getRsName(), 
        	        details.getRsDescription(), details.getRsDateFrom(),
        	        details.getRsDateTo(), AD_CMPNY); 
        	
        	Iterator i = branchList.iterator();
        	
        	while(i.hasNext()) {
        		
        		AdBranchDetails brDetails = (AdBranchDetails)i.next();
        		adBranchResponsibility = adBranchResponsibilityHome.create(AD_CMPNY);
        		
        		adResponsibility.addAdBranchResponsibility(adBranchResponsibility);
        		
        		adBranch = adBranchHome.findByPrimaryKey(brDetails.getBrCode());
        		adBranch.addAdBranchResponsibility(adBranchResponsibility);
        		
        	}

        } catch (Exception ex) {
        	
            throw new EJBException(ex.getMessage());
        	
        }    
        
    }
    
    
    
    /**
     * @ejb:interface-method view-type="remote"
     **/    
    public void copyAdRsEntry(com.util.AdResponsibilityDetails details, ArrayList branchList, Integer AD_CMPNY) 
        throws Exception {
                    
        Debug.print("AdResponsibilityControllerBean copyAdRsEntry");
        
        LocalAdResponsibilityHome adResponsibilityHome = null;
        LocalAdBranchHome adBranchHome = null;
        LocalAdBranchResponsibilityHome adBranchResponsibilityHome = null;
               
        LocalAdResponsibility adResponsibilityNew = null;
        LocalAdBranch adBranchNew = null;
        LocalAdBranchResponsibility adBranchResponsibilityNew = null;
        LocalAdFormFunctionResponsibilityHome adFormFunctionResponsibilityHome = null;
        ArrayList list = new ArrayList();
        
        // Initialize EJB Home
        
        try {
            
            adResponsibilityHome = (LocalAdResponsibilityHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdResponsibilityHome.JNDI_NAME, LocalAdResponsibilityHome.class);
            adBranchHome = (LocalAdBranchHome)EJBHomeFactory.
            	lookUpLocalHome(LocalAdBranchHome.JNDI_NAME, LocalAdBranchHome.class);
            adBranchResponsibilityHome = (LocalAdBranchResponsibilityHome)EJBHomeFactory.
        		lookUpLocalHome(LocalAdBranchResponsibilityHome.JNDI_NAME, LocalAdBranchResponsibilityHome.class);
            adFormFunctionResponsibilityHome = (LocalAdFormFunctionResponsibilityHome)EJBHomeFactory.
            		lookUpLocalHome(LocalAdFormFunctionResponsibilityHome.JNDI_NAME, LocalAdFormFunctionResponsibilityHome.class);
                                                      
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
       // if(branchList.size() == 0) {
        	
      //  	throw new AdRSResponsibilityNotAssignedToBranchException();
        	
      //  }

       
                            
        try {
        	LocalAdResponsibility adResponsibilitySource = adResponsibilityHome.findByRsName(details.getRsName(), AD_CMPNY);
        	// create new responsibility

        	adResponsibilityNew = adResponsibilityHome.create(("Copy of " + details.getRsName()), 
        	        details.getRsDescription(), details.getRsDateFrom(),
        	        details.getRsDateTo(), AD_CMPNY); 
        	
        	
        	Iterator i = branchList.iterator();
        	
        	while(i.hasNext()) {
        		
        		AdBranchDetails brDetails = (AdBranchDetails)i.next();
        		adBranchResponsibilityNew = adBranchResponsibilityHome.create(AD_CMPNY);
        		
        		adResponsibilityNew.addAdBranchResponsibility(adBranchResponsibilityNew);
        		
        		adBranchNew = adBranchHome.findByPrimaryKey(brDetails.getBrCode());
        		adBranchNew.addAdBranchResponsibility(adBranchResponsibilityNew);
        		
        	}
        	
        	Collection adFormFunctionResponsibilities = adResponsibilitySource.getAdFormFunctionResponsibilities();
        	System.out.println("SIZE = "+ adFormFunctionResponsibilities.size());
        	
        		
        	i = adFormFunctionResponsibilities.iterator();
        	
	        	while (i.hasNext()) {
	        		
	        		
	        		LocalAdFormFunctionResponsibility adFormFunctionResponsibility = (LocalAdFormFunctionResponsibility)i.next();
	        		System.out.println("-----"+adFormFunctionResponsibility.getFrParameter());
	        		
	        		// add form function
	        		
	        		
	        		
	        		LocalAdFormFunctionResponsibility adFormFunctionResponsibilityNew = adFormFunctionResponsibilityHome.create(adFormFunctionResponsibility.getFrParameter(), adFormFunctionResponsibility.getFrAdCompany());
	        		
	        		adResponsibilityNew.addAdFormFunctionResponsibility(adFormFunctionResponsibilityNew);
	        		
	        		adFormFunctionResponsibility.getAdFormFunction().addAdFormFunctionResponsibility(adFormFunctionResponsibilityNew);
	        		
	        		
	        	} 
	        	System.out.println("FORM FUNCTION COPY SUCCESS");
        	

        } catch (Exception ex) {
        	
            throw new EJBException(ex.getMessage());
        	
        }    
        
    }
    
    
  
    /**
     * @ejb:interface-method view-type="remote"
     **/
    public void updateAdRsEntry(com.util.AdResponsibilityDetails details, ArrayList branchList, Integer AD_CMPNY) 
        throws GlobalRecordAlreadyExistException, AdRSResponsibilityNotAssignedToBranchException { 
                    
        Debug.print("AdResponsibilityControllerBean updateAdRsEntry");
        
        LocalAdResponsibilityHome adResponsibilityHome = null; 
        LocalAdBranchResponsibilityHome adBranchResponsibilityHome = null;
        LocalAdBranchHome adBranchHome = null;
                               
        LocalAdResponsibility adResponsibility = null;        
        LocalAdBranchResponsibility adBranchResponsibility = null;
        LocalAdBranch adBranch = null;
        
        ArrayList list = new ArrayList();
        
        // Initialize EJB Home
        
        try {

            adResponsibilityHome = (LocalAdResponsibilityHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdResponsibilityHome.JNDI_NAME, LocalAdResponsibilityHome.class);
            adBranchResponsibilityHome = (LocalAdBranchResponsibilityHome)EJBHomeFactory.
            	lookUpLocalHome(LocalAdBranchResponsibilityHome.JNDI_NAME, LocalAdBranchResponsibilityHome.class);
            adBranchHome = (LocalAdBranchHome)EJBHomeFactory.
        		lookUpLocalHome(LocalAdBranchHome.JNDI_NAME, LocalAdBranchHome.class);

        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        if(branchList.size() == 0) {
        	
        	throw new AdRSResponsibilityNotAssignedToBranchException();
        	
        }
        
        try { 
            
            LocalAdResponsibility arExistingResponsibility = adResponsibilityHome.findByRsName(details.getRsName(), AD_CMPNY);
            
            if (!arExistingResponsibility.getRsCode().equals(details.getRsCode())) {
            
                 throw new GlobalRecordAlreadyExistException();
                 
            }
                                             
        } catch (GlobalRecordAlreadyExistException ex) {
        	
            throw ex;
        	
        } catch (FinderException ex) {
        	 	
        } catch (Exception ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
                        
        try {
        	        	
        	// find and update responsibility
        	
        	adResponsibility = adResponsibilityHome.findByPrimaryKey(details.getRsCode());
        	    	
                adResponsibility.setRsName(details.getRsName());
                adResponsibility.setRsDescription(details.getRsDescription());
                adResponsibility.setRsDateFrom(details.getRsDateFrom());
                adResponsibility.setRsDateTo(details.getRsDateTo());
                
                Collection adBranchResponsibilities = adBranchResponsibilityHome.findByAdResponsibility(adResponsibility.getRsCode(), AD_CMPNY);
                
                Iterator i = adBranchResponsibilities.iterator();
                
                //remove all adBranchResponsibility lines
                while(i.hasNext()) {
                	
                	adBranchResponsibility = (LocalAdBranchResponsibility) i.next();
                	
                	adResponsibility.dropAdBranchResponsibility(adBranchResponsibility);
                	
                	adBranch = adBranchHome.findByPrimaryKey(adBranchResponsibility.getAdBranch().getBrCode());
                	adBranch.dropAdBranchResponsibility(adBranchResponsibility);
                	adBranchResponsibility.remove();
                }
                
                //add adBranchResponsibility lines
                
                Iterator brListIter = branchList.iterator();
                
                while(brListIter.hasNext()) {
                	
                	AdBranchDetails brDetails = (AdBranchDetails)brListIter.next();
                	
                	adBranchResponsibility = adBranchResponsibilityHome.create(AD_CMPNY);
                	adResponsibility.addAdBranchResponsibility(adBranchResponsibility);
                	
                	adBranch = adBranchHome.findByPrimaryKey(brDetails.getBrCode());
                	adBranch.addAdBranchResponsibility(adBranchResponsibility);
                	
                }
                
        } catch (Exception ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
    }
    
   /**
    * @ejb:interface-method view-type="remote"
    **/
    public void deleteAdRsEntry(Integer RS_CODE, Integer AD_CMPNY) 
        throws GlobalRecordAlreadyDeletedException,
               GlobalRecordAlreadyAssignedException {

      Debug.print("AdResponsibilityControllerBean deleteAdRsEntry");

      LocalAdResponsibility adResponsibility = null;
      LocalAdResponsibilityHome adResponsibilityHome = null;

      // Initialize EJB Home
        
      try {

          adResponsibilityHome = (LocalAdResponsibilityHome)EJBHomeFactory.
              lookUpLocalHome(LocalAdResponsibilityHome.JNDI_NAME, LocalAdResponsibilityHome.class);           
      
      } catch (NamingException ex) {

          throw new EJBException(ex.getMessage());

      }                
      
      try {
      	
         adResponsibility = adResponsibilityHome.findByPrimaryKey(RS_CODE);
         
      } catch (FinderException ex) {
      	
         throw new GlobalRecordAlreadyDeletedException();
         
      } catch (Exception ex) {
      	
         throw new EJBException(ex.getMessage()); 
         
      }
      
      try {

          if (!adResponsibility.getAdUserResponsibilities().isEmpty()) {
      
             throw new GlobalRecordAlreadyAssignedException();
             
          }
          
      } catch (GlobalRecordAlreadyAssignedException ex) {                        
         
          throw ex;
          
      } catch (Exception ex) {
       
            throw new EJBException(ex.getMessage());
           
      }                         
         
      try {
          
      	   adResponsibility.remove();
	    
	  } catch (RemoveException ex) {
	 	
	      getSessionContext().setRollbackOnly();
	    
	      throw new EJBException(ex.getMessage());
	    
	  } catch (Exception ex) {
	 	
	      getSessionContext().setRollbackOnly();
	    
	      throw new EJBException(ex.getMessage());
	    
	  }	      
      
   }    
    
    // SessionBean methods

    /**
     * @ejb:create-method view-type="remote"
     **/
    public void ejbCreate() throws CreateException {

       Debug.print("AdResponsibilityControllerBean ejbCreate");
      
    }
}
