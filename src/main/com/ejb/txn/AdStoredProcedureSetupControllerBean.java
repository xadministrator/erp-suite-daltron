package com.ejb.txn;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.naming.NamingException;

import com.ejb.ad.LocalAdStoredProcedure;
import com.ejb.ad.LocalAdStoredProcedureHome;
import com.util.AbstractSessionBean;
import com.util.AdStoredProcedureDetails;
import com.util.Debug;
import com.util.EJBHomeFactory;

/**
 * @ejb:bean name="AdStoredProcedureSetupControllerEJB"
 *           display-name="Used for editing stored procedure setup"
 *           type="Stateless"
 *           view-type="remote"
 *           jndi-name="ejb/AdStoredProcedureSetupControllerEJB"
 *
 * @ejb:interface remote-class="com.ejb.txn.AdStoredProcedureSetupController"
 *                extends="javax.ejb.EJBObject"
 *
 * @ejb:home remote-class="com.ejb.txn.AdStoredProcedureSetupControllerHome"
 *           extends="javax.ejb.EJBHome"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="aduser"
 *                        role-link="aduserlink"
 *
 * @ejb:permission role-name="aduser"
 * 
*/

public class AdStoredProcedureSetupControllerBean extends AbstractSessionBean {

    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public com.util.AdStoredProcedureDetails getAdSp(Integer AD_CMPNY) {
                    
        Debug.print("AdStoredProcedureSetupControllerBean getAdSp");

        LocalAdStoredProcedureHome adStoredProcedureHome = null;
        LocalAdStoredProcedure adStoredProcedure = null;
        
        // Initialize EJB Home

        try {

        	adStoredProcedureHome = (LocalAdStoredProcedureHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdStoredProcedureHome.JNDI_NAME, LocalAdStoredProcedureHome.class);

        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {
        	
        	adStoredProcedure = adStoredProcedureHome.findBySpAdCompany(AD_CMPNY);
	
        	AdStoredProcedureDetails details = new AdStoredProcedureDetails();

        	details.setSpEnableGlGeneralLedgerReport(adStoredProcedure.getSpEnableGlGeneralLedgerReport());
        	details.setSpNameGlGeneralLedgerReport(adStoredProcedure.getSpNameGlGeneralLedgerReport());
        	
        	details.setSpEnableGlTrialBalanceReport(adStoredProcedure.getSpEnableGlTrialBalanceReport());
        	details.setSpNameGlTrialBalanceReport(adStoredProcedure.getSpNameGlTrialBalanceReport());
        	
        	details.setSpEnableGlIncomeStatementReport(adStoredProcedure.getSpEnableGlIncomeStatementReport());
        	details.setSpNameGlIncomeStatementReport(adStoredProcedure.getSpNameGlIncomeStatementReport());
        	
        	details.setSpEnableGlBalanceSheetReport(adStoredProcedure.getSpEnableGlBalanceSheetReport());
        	details.setSpNameGlBalanceSheetReport(adStoredProcedure.getSpNameGlBalanceSheetReport());
                
                details.setSpEnableArStatementOfAccountReport(adStoredProcedure.getSpEnableArStatementOfAccountReport());
        	details.setSpNameArStatementOfAccountReport(adStoredProcedure.getSpNameArStatementOfAccountReport());
	            
		        
	        return details;
	        
        } catch (Exception ex) {
        	
        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());
        	
        }
            
    }    
    
    /**
     * @ejb:interface-method view-type="remote"
     **/
    public Integer saveAdSpEntry(com.util.AdStoredProcedureDetails details, Integer AD_CMPNY) {
                    
        Debug.print("AdStoredProcedureSetupControllerBean saveAdSpEntry");
        
        LocalAdStoredProcedureHome adStoredProcedureHome = null;                
                
        // Initialize EJB Home
        
        try {

        	adStoredProcedureHome = (LocalAdStoredProcedureHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdStoredProcedureHome.JNDI_NAME, LocalAdStoredProcedureHome.class);           

        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {
        	
        	LocalAdStoredProcedure adStoredProcedure = null;
        	
			// update approval setup
			
        	adStoredProcedure = adStoredProcedureHome.findBySpAdCompany(AD_CMPNY);
        	
        	System.out.println("details.getSpEnableGlGeneralLedgerReport()="+details.getSpEnableGlGeneralLedgerReport());
        	System.out.println("details.getSpNameGlGeneralLedgerReport()="+details.getSpNameGlGeneralLedgerReport());
        	
        	System.out.println("details.getSpEnableGlTrialBalanceReport()="+details.getSpEnableGlTrialBalanceReport());
        	System.out.println("details.getSpNameGlTrialBalanceReport()="+details.getSpNameGlTrialBalanceReport());
        	
        	System.out.println("details.getSpEnableGlIncomeStatementReport()="+details.getSpEnableGlIncomeStatementReport());
        	System.out.println("details.getSpNameGlIncomeStatementReport()="+details.getSpNameGlIncomeStatementReport());
        	
        	System.out.println("details.getSpEnableGlBalanceSheetReport()="+details.getSpEnableGlBalanceSheetReport());
        	System.out.println("details.getSpNameGlBalanceSheetReport()="+details.getSpNameGlBalanceSheetReport());
        	
                System.out.println("details.getSpEnableArStatementOfAccountReport()="+details.getSpEnableArStatementOfAccountReport());
        	System.out.println("details.getSpNameArStatementOfAccountReport()="+details.getSpNameArStatementOfAccountReport());
        
        	
        	adStoredProcedure.setSpEnableGlGeneralLedgerReport(details.getSpEnableGlGeneralLedgerReport());
        	adStoredProcedure.setSpNameGlGeneralLedgerReport(details.getSpNameGlGeneralLedgerReport());
        	adStoredProcedure.setSpEnableGlTrialBalanceReport(details.getSpEnableGlTrialBalanceReport());
        	adStoredProcedure.setSpNameGlTrialBalanceReport(details.getSpNameGlTrialBalanceReport());
        	adStoredProcedure.setSpEnableGlIncomeStatementReport(details.getSpEnableGlIncomeStatementReport());
        	adStoredProcedure.setSpNameGlIncomeStatementReport(details.getSpNameGlIncomeStatementReport());
        	adStoredProcedure.setSpEnableGlBalanceSheetReport(details.getSpEnableGlBalanceSheetReport());
        	adStoredProcedure.setSpNameGlBalanceSheetReport(details.getSpNameGlBalanceSheetReport());
                
                adStoredProcedure.setSpEnableArStatementOfAccountReport(details.getSpEnableArStatementOfAccountReport());
        	adStoredProcedure.setSpNameArStatementOfAccountReport(details.getSpNameArStatementOfAccountReport());

        	

        	
			return adStoredProcedure.getSpCode();
			    
        } catch (Exception ex) {
        	
        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());
        	
        }
            
    }    
    
    // SessionBean methods

    /**
     * @ejb:create-method view-type="remote"
     **/
    public void ejbCreate() throws CreateException {

        Debug.print("AdStoredProcedureSetupControllerBean ejbCreate");
      
    }
    
}

