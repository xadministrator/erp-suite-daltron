
/*
 * AdUserControllerBean.java
 *
 * Created on June 17, 2003, 9:54 AM
 *
 * @author  Neil Andrew M. Ajero
 *
 */

package com.ejb.txn;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;
import javax.ejb.RemoveException;
import javax.naming.NamingException;

import sun.misc.BASE64Decoder;
import sun.misc.BASE64Encoder;


//import java.util.Base64;
import com.ejb.ad.LocalAdLookUpValue;
import com.ejb.ad.LocalAdLookUpValueHome;
import com.ejb.ad.LocalAdUser;
import com.ejb.ad.LocalAdUserHome;
import com.ejb.ap.LocalApRecurringVoucherHome;
import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.exception.GlobalRecordAlreadyAssignedException;
import com.ejb.exception.GlobalRecordAlreadyDeletedException;
import com.ejb.exception.GlobalRecordAlreadyExistException;
import com.ejb.gl.LocalGlRecurringJournalHome;
import com.ejb.pm.LocalPmUser;
import com.ejb.pm.LocalPmUserHome;
import com.util.AbstractSessionBean;
import com.util.AdModUserDetails;
import com.util.AdUserDetails;
import com.util.ArRepStatementDetails;
import com.util.Debug;
import com.util.EJBHomeFactory;

/**
 * @ejb:bean name="AdUserControllerEJB"
 *           display-name="Used for entering aging buckets"
 *           type="Stateless"
 *           view-type="remote"
 *           jndi-name="ejb/AdUserControllerEJB"
 *
 * @ejb:interface remote-class="com.ejb.txn.AdUserController"
 *                extends="javax.ejb.EJBObject"
 *
 * @ejb:home remote-class="com.ejb.txn.AdUserControllerHome"
 *           extends="javax.ejb.EJBHome"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="aduser"
 *                        role-link="aduserlink"
 *
 * @ejb:permission role-name="aduser"
 * 
 */

public class AdUserControllerBean extends AbstractSessionBean {
	
	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public ArrayList getAdUsrAll(Integer AD_CMPNY) 
	throws GlobalNoRecordFoundException {
		
		Debug.print("AdUserControllerBean getAdUsrAll");
		
		LocalAdUserHome adUserHome = null;
		
		Collection adUsers = null;
		
		LocalAdUser adUser = null;
		
		ArrayList list = new ArrayList();
		
		// Initialize EJB Home
		
		try {
			
			adUserHome = (LocalAdUserHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdUserHome.JNDI_NAME, LocalAdUserHome.class);
			
		} catch (NamingException ex) {
			
			throw new EJBException(ex.getMessage());
			
		}
		
		try {
			
			adUsers = adUserHome.findUsrAll(AD_CMPNY);
			
		} catch (FinderException ex) {
			
		} catch (Exception ex) {
			
			throw new EJBException(ex.getMessage());
		}
		
		if (adUsers.isEmpty()) {
			
			throw new GlobalNoRecordFoundException();
			
		}
		
		Iterator i = adUsers.iterator();
		
		while (i.hasNext()) {
			
			adUser = (LocalAdUser)i.next();
			
			AdModUserDetails details = new AdModUserDetails();
			
			details.setUsrCode(adUser.getUsrCode());        		
			details.setUsrName(adUser.getUsrName());
			details.setUsrDept(adUser.getUsrDept());
			details.setUsrDescription(adUser.getUsrDescription());
			details.setUsrPosition(adUser.getUsrPosition());
			details.setUsrEmailAddress(adUser.getUsrEmailAddress());
			System.out.println("adUser.getUsrHead()="+adUser.getUsrHead());
			System.out.println("adUser.getUsrInspector()="+adUser.getUsrInspector());
			details.setUsrHead(adUser.getUsrHead());
			details.setUsrInspector(adUser.getUsrInspector());
			details.setUsrPassword(this.decryptPassword(adUser.getUsrPassword()));
			details.setUsrPasswordExpirationCode(adUser.getUsrPasswordExpirationCode());
			details.setUsrPasswordExpirationDays(adUser.getUsrPasswordExpirationDays());
			details.setUsrPasswordExpirationAccess(adUser.getUsrPasswordExpirationAccess());
			details.setUsrDateFrom(adUser.getUsrDateFrom());
			details.setUsrDateTo(adUser.getUsrDateTo());
			
			Collections.sort(list, AdModUserDetails.DepartmentComparator);
			
			list.add(details);
		}
		
		return list;
		
	}
	
	/**
	 * @ejb:interface-method view-type="remote"
	 **/    
	public Integer addAdUsrEntry(com.util.AdUserDetails details, String USR_EMP_NMBR, Integer AD_CMPNY) 
	throws GlobalRecordAlreadyExistException {
		
		Debug.print("AdUserControllerBean addAdUsrEntry");
		
		LocalAdUserHome adUserHome = null;
		LocalPmUserHome pmUserHome = null;
		
		LocalAdUser adUser = null;
		
		ArrayList list = new ArrayList();
		
		// Initialize EJB Home
		
		try {
			
			adUserHome = (LocalAdUserHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdUserHome.JNDI_NAME, LocalAdUserHome.class);
			
			pmUserHome = (LocalPmUserHome)EJBHomeFactory.
					lookUpLocalHome(LocalPmUserHome.JNDI_NAME, LocalPmUserHome.class);
			
		} catch (NamingException ex) {
			
			throw new EJBException(ex.getMessage());
			
		}
		
		try { 
			
			adUser = adUserHome.findByUsrName(details.getUsrName(), AD_CMPNY);
			
			throw new GlobalRecordAlreadyExistException();
			
		} catch (GlobalRecordAlreadyExistException ex) {
			
			throw ex;
			
		} catch (FinderException ex) {
			
		} catch (Exception ex) {
			
			throw new EJBException(ex.getMessage());
			
		}
		
		try {
			
			// create new user
			
			adUser = adUserHome.create(details.getUsrName(),details.getUsrDept(), 
					details.getUsrDescription(), details.getUsrPosition(), details.getUsrEmailAddress(),
					details.getUsrHead(), details.getUsrInspector(),
					this.encryptPassword(details.getUsrPassword()),
					details.getUsrPasswordExpirationCode(), details.getUsrPasswordExpirationDays(),
					details.getUsrPasswordExpirationAccess(), (short)0, details.getUsrDateFrom(),
					details.getUsrDateTo(), AD_CMPNY); 
			
			try {
				
				LocalPmUser pmUser = pmUserHome.findUsrByEmployeeNumber(USR_EMP_NMBR, AD_CMPNY);
				adUser.setPmUser(pmUser);
				
			}catch (FinderException e) {}
			
			
			return adUser.getUsrCode();
		} catch (Exception ex) {
			
			getSessionContext().setRollbackOnly();
			throw new EJBException(ex.getMessage());
			
		}    
		
	}
	
	/**
	 * @ejb:interface-method view-type="remote"
	 **/
	public Integer updateAdUsrEntry(com.util.AdUserDetails details, String USR_EMP_NMBR, Integer AD_CMPNY) 
	throws GlobalRecordAlreadyExistException { 
		
		Debug.print("AdUserControllerBean updateAdUsrEntry");
		
		LocalAdUserHome adUserHome = null;
		LocalPmUserHome pmUserHome = null;
		
		LocalAdUser adUser = null;        
		
		ArrayList list = new ArrayList();
		
		// Initialize EJB Home
		
		try {
			
			adUserHome = (LocalAdUserHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdUserHome.JNDI_NAME, LocalAdUserHome.class);
			pmUserHome = (LocalPmUserHome)EJBHomeFactory.
					lookUpLocalHome(LocalPmUserHome.JNDI_NAME, LocalPmUserHome.class);
			
		} catch (NamingException ex) {
			
			throw new EJBException(ex.getMessage());
			
		}
		
		try { 
			
			LocalAdUser adExistingUser = adUserHome.findByUsrName(details.getUsrName(), AD_CMPNY);
			
			if (!adExistingUser.getUsrCode().equals(details.getUsrCode())) {
				
				throw new GlobalRecordAlreadyExistException();
				
			}
			
		} catch (GlobalRecordAlreadyExistException ex) {
			
			throw ex;
			
		} catch (FinderException ex) {
			
		} catch (Exception ex) {
			
			throw new EJBException(ex.getMessage());
			
		}
		
		try {
			
			// find and update user
			
			adUser = adUserHome.findByPrimaryKey(details.getUsrCode());
			
			adUser.setUsrName(details.getUsrName());
			adUser.setUsrDept(details.getUsrDept());
			adUser.setUsrDescription(details.getUsrDescription());
			adUser.setUsrHead(details.getUsrHead());
			adUser.setUsrInspector(details.getUsrInspector());
			adUser.setUsrPosition(details.getUsrPosition());
			adUser.setUsrEmailAddress(details.getUsrEmailAddress());
			adUser.setUsrPassword(this.encryptPassword(details.getUsrPassword()));
			adUser.setUsrPasswordExpirationCode(details.getUsrPasswordExpirationCode());
			adUser.setUsrPasswordExpirationDays(details.getUsrPasswordExpirationDays());
			adUser.setUsrPasswordExpirationAccess(details.getUsrPasswordExpirationAccess());
			adUser.setUsrDateFrom(details.getUsrDateFrom());
			adUser.setUsrDateTo(details.getUsrDateTo());
			
			try {
				
				LocalPmUser pmUser = pmUserHome.findUsrByEmployeeNumber(USR_EMP_NMBR, AD_CMPNY);
				adUser.setPmUser(pmUser);
				
			}catch (FinderException e) {}
			
			return adUser.getUsrCode();
		} catch (Exception ex) {
			
			throw new EJBException(ex.getMessage());
			
		}
		
	}
	
	/**
	 * @ejb:interface-method view-type="remote"
	 **/
	public void deleteAdUsrEntry(Integer USR_CODE, Integer AD_CMPNY) 
	throws GlobalRecordAlreadyDeletedException,
	GlobalRecordAlreadyAssignedException {
		
		Debug.print("AdUserControllerBean deleteAdUsrEntry");
		
		LocalAdUser adUser = null;
		LocalAdUserHome adUserHome = null;
		LocalApRecurringVoucherHome apRecurringVoucherHome = null;
		LocalGlRecurringJournalHome glRecurringJournalHome = null;
		
		// Initialize EJB Home
		
		try {
			
			adUserHome = (LocalAdUserHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdUserHome.JNDI_NAME, LocalAdUserHome.class);           
			apRecurringVoucherHome = (LocalApRecurringVoucherHome)EJBHomeFactory.
			lookUpLocalHome(LocalApRecurringVoucherHome.JNDI_NAME, LocalApRecurringVoucherHome.class);  
			glRecurringJournalHome = (LocalGlRecurringJournalHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlRecurringJournalHome.JNDI_NAME, LocalGlRecurringJournalHome.class);  
			
		} catch (NamingException ex) {
			
			throw new EJBException(ex.getMessage());
			
		}                
		
		try {
			
			adUser = adUserHome.findByPrimaryKey(USR_CODE);
			
		} catch (FinderException ex) {
			
			throw new GlobalRecordAlreadyDeletedException();
			
		} catch (Exception ex) {
			
			throw new EJBException(ex.getMessage()); 
			
		}    
		
		try {
			
			// recurring voucher
			
			Collection recurringVouchers1 = apRecurringVoucherHome.findByUserCode1(adUser.getUsrCode(), AD_CMPNY);
			Collection recurringVouchers2 = apRecurringVoucherHome.findByUserCode2(adUser.getUsrCode(), AD_CMPNY);
			Collection recurringVouchers3 = apRecurringVoucherHome.findByUserCode3(adUser.getUsrCode(), AD_CMPNY);
			Collection recurringVouchers4 = apRecurringVoucherHome.findByUserCode4(adUser.getUsrCode(), AD_CMPNY);
			Collection recurringVouchers5 = apRecurringVoucherHome.findByUserCode5(adUser.getUsrCode(), AD_CMPNY);
			
			// recurring journal
			
			Collection recurringJournals1 = glRecurringJournalHome.findByUserName1(adUser.getUsrName(), AD_CMPNY);
			Collection recurringJournals2 = glRecurringJournalHome.findByUserName2(adUser.getUsrName(), AD_CMPNY);
			Collection recurringJournals3 = glRecurringJournalHome.findByUserName3(adUser.getUsrName(), AD_CMPNY);
			Collection recurringJournals4 = glRecurringJournalHome.findByUserName4(adUser.getUsrName(), AD_CMPNY);
			Collection recurringJournals5 = glRecurringJournalHome.findByUserName5(adUser.getUsrName(), AD_CMPNY);
			
			if (!adUser.getAdApprovalUsers().isEmpty() ||
					!adUser.getAdApprovalQueues().isEmpty() ||
					!recurringVouchers1.isEmpty() ||
					!recurringVouchers2.isEmpty() ||
					!recurringVouchers3.isEmpty() ||
					!recurringVouchers4.isEmpty() ||
					!recurringVouchers5.isEmpty() ||
					!recurringJournals1.isEmpty() ||
					!recurringJournals2.isEmpty() ||
					!recurringJournals3.isEmpty() ||
					!recurringJournals4.isEmpty() ||
					!recurringJournals5.isEmpty()) {
				
				throw new GlobalRecordAlreadyAssignedException();
				
			}
			
		} catch (GlobalRecordAlreadyAssignedException ex) {                        
			
			throw ex;
			
		} catch (Exception ex) {
			
			Debug.printStackTrace(ex);
            throw new EJBException(ex.getMessage());
			
		} 
		
		// remove user
		
		try {
			
			adUser.remove();
			
		} catch (RemoveException ex) {
			
			getSessionContext().setRollbackOnly();	    
			throw new EJBException(ex.getMessage());
			
		} catch (Exception ex) {
			
			getSessionContext().setRollbackOnly();	    
			throw new EJBException(ex.getMessage());
			
		}	      
		
	}    
	
	// private method
	
	private String encryptPassword(String password) {
		
		Debug.print("AdUserControllerBean encryptPassword");
		
		String encPassword = new String();
		
		try {
			
			String keyString = "Some things are better left unread.";
			byte key[] = keyString.getBytes("UTF8");
			
			// encode string to utf-8
			byte utf8[] = password.getBytes("UTF8");
			
			int length = key.length;
			if(utf8.length < key.length)
				length = utf8.length;
			
			// encrypt
			byte data[] = new byte[length];
			for(int i =0; i< length; i++)
			{
			   data[i] = (byte)(utf8[i] ^ key[i]);
			}
			
			// encode byte to base64 
			BASE64Encoder encoder = new BASE64Encoder();
			encPassword = encoder.encode(data);
			
			//new encoder for java 8
			//encPassword = Base64.getEncoder().encodeToString(data);
			
		} catch (Exception ex) {	
			
			Debug.printStackTrace(ex);
            throw new EJBException(ex.getMessage());
            
		}
		
		return encPassword;
		
	}
	
	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public ArrayList getAdLvDEPARTMENT(Integer AD_CMPNY) {
		
		Debug.print("AdUserControllerBean getAdLvDEPARTMENT");
		
		LocalAdLookUpValueHome adLookUpValueHome = null;               
		
		ArrayList list = new ArrayList();
		
		// Initialize EJB Home
		
		try {
			
			adLookUpValueHome = (LocalAdLookUpValueHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdLookUpValueHome.JNDI_NAME, LocalAdLookUpValueHome.class);
			
		} catch (NamingException ex) {
			
			throw new EJBException(ex.getMessage());
			
		}        
		
		try {
			
			Collection adLookUpValues = adLookUpValueHome.findByLuName("AD DEPARTMENT", AD_CMPNY);
			
			Iterator i = adLookUpValues.iterator();
			
			while (i.hasNext()) {
				
				LocalAdLookUpValue adLookUpValue = (LocalAdLookUpValue)i.next();
				
				list.add(adLookUpValue.getLvName());
				
			}
			
			return list;
			
		} catch (Exception ex) {
			
			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());
			
		}
		
	}	
	
	
	
	
	
	
	
	
	
	private String decryptPassword(String password) {
		
		Debug.print("AdUserControllerBean decryptPassword");
		
		String decPassword = new String();
		
		try {
			
			String keyString = "Some things are better left unread.";
			byte key[] = keyString.getBytes("UTF8");
			
			// decode base64 to byte
			BASE64Decoder decoder = new BASE64Decoder();
			byte base64[] = decoder.decodeBuffer(password);

			//new decoder for java 8
		//	byte base64[] = Base64.getDecoder().decode(password);
			int length = key.length;
			if(base64.length < key.length)
				length = base64.length;
			
			// decrypt
			byte data[] = new byte[length];
			for(int i =0; i< length; i++)
			{
			   data[i] = (byte)(key[i] ^ base64[i]);
			}

			// decode byte to string
			String decode = new String(data, "UTF8");
			decPassword = decode;
			
		} catch (Exception ex) {	
			
			Debug.printStackTrace(ex);
            throw new EJBException(ex.getMessage());
            
		}
		
		return decPassword;
		
	}
	// SessionBean methods
	
	/**
	 * @ejb:create-method view-type="remote"
	 **/
	public void ejbCreate() throws CreateException {
		
		Debug.print("AdUserControllerBean ejbCreate");
		
	}
}
