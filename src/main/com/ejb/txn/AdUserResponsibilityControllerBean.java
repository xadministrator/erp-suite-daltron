
/*
 * AdUserResponsibilityControllerBean.java
 *
 * Created on June 18, 2003, 2:18 PM
 *
 * @author  Neil Andrew M. Ajero
 */

package com.ejb.txn;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;
import javax.ejb.RemoveException;
import javax.naming.NamingException;

import com.ejb.ad.LocalAdResponsibility;
import com.ejb.ad.LocalAdResponsibilityHome;
import com.ejb.ad.LocalAdUser;
import com.ejb.ad.LocalAdUserHome;
import com.ejb.ad.LocalAdUserResponsibility;
import com.ejb.ad.LocalAdUserResponsibilityHome;
import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.exception.GlobalRecordAlreadyDeletedException;
import com.ejb.exception.GlobalRecordAlreadyExistException;
import com.util.AbstractSessionBean;
import com.util.AdModUserResponsibilityDetails;
import com.util.AdUserDetails;
import com.util.Debug;
import com.util.EJBHomeFactory;

/**
 * @ejb:bean name="AdUserResponsibilityControllerEJB"
 *           display-name="Used for entering user's responsibilities"
 *           type="Stateless"
 *           view-type="remote"
 *           jndi-name="ejb/AdUserResponsibilityControllerEJB"
 *
 * @ejb:interface remote-class="com.ejb.txn.AdUserResponsibilityController"
 *                extends="javax.ejb.EJBObject"
 *
 * @ejb:home remote-class="com.ejb.txn.AdUserResponsibilityControllerHome"
 *           extends="javax.ejb.EJBHome"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="aduser"
 *                        role-link="aduserlink"
 *
 * @ejb:permission role-name="aduser"
 * 
*/

public class AdUserResponsibilityControllerBean extends AbstractSessionBean {


    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList getAdRsAll(Integer AD_CMPNY) {
                    
        Debug.print("ArCustomerEntryControllerBean getAdRsAll");
        
        LocalAdResponsibilityHome adResponsibilityHome = null;
        
        Collection adResponsibilities = null;
        
        LocalAdResponsibility adResponsibility = null;
        
        ArrayList list = new ArrayList();
        
        // Initialize EJB Home
        
        try {
            
            adResponsibilityHome = (LocalAdResponsibilityHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdResponsibilityHome.JNDI_NAME, LocalAdResponsibilityHome.class);
            
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {
            
            adResponsibilities = adResponsibilityHome.findRsAll(AD_CMPNY);
            
        } catch (FinderException ex) {
            
        } catch (Exception ex) {
            
            throw new EJBException(ex.getMessage());
        }
        
        if (adResponsibilities.isEmpty()) {
        	
        	return null;
        	
        }
        
        Iterator i = adResponsibilities.iterator();
               
        while (i.hasNext()) {
        	
        	adResponsibility = (LocalAdResponsibility)i.next();
        	
        	list.add(adResponsibility.getRsName());
        	
        }
        
        return list;
            
    }

    
    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList getAdUrByUsrCode(Integer USR_CODE, Integer AD_CMPNY) 
        throws GlobalNoRecordFoundException {
                    
        Debug.print("AdUserResponsibilityControllerBean getAdUrByUsrCode");

        LocalAdUserResponsibilityHome adUserResponsibilityHome = null;

        Collection adUserResponsibilities = null;

        LocalAdUserResponsibility adUserResponsibility = null;

        ArrayList list = new ArrayList();
        
        // Initialize EJB Home

        try {
            
            adUserResponsibilityHome = (LocalAdUserResponsibilityHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdUserResponsibilityHome.JNDI_NAME, LocalAdUserResponsibilityHome.class);

        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }

        try {
            
            adUserResponsibilities = adUserResponsibilityHome.findByUsrCode(USR_CODE, AD_CMPNY);
            
        } catch (FinderException ex) {
            
        } catch (Exception ex) {
            
            throw new EJBException(ex.getMessage());
        }

        if (adUserResponsibilities.isEmpty()) {
        	
            throw new GlobalNoRecordFoundException();
        	
        }
        
        Iterator i = adUserResponsibilities.iterator();
               
        while (i.hasNext()) {
        	
        	adUserResponsibility = (LocalAdUserResponsibility)i.next();
                                                                        
        	AdModUserResponsibilityDetails mdetails = new AdModUserResponsibilityDetails();
        		mdetails.setUrCode(adUserResponsibility.getUrCode());        		
                mdetails.setUrDateFrom(adUserResponsibility.getUrDateFrom());
                mdetails.setUrDateTo(adUserResponsibility.getUrDateTo());
                mdetails.setUrResponsibilityName(adUserResponsibility.getAdResponsibility().getRsName());
                
                                                        	
        	list.add(mdetails);
        }
        
        return list;
            
    }
    
    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public com.util.AdUserDetails getAdUsrByUsrCode(Integer USR_CODE, Integer AD_CMPNY) {
                    
        Debug.print("AdUserResponsibilityControllerBean getAdUsrByUsrCode");
        		    	         	   	        	    
        LocalAdUserHome adUserHome = null;
                                       
        // Initialize EJB Home
        
        try {
            
            adUserHome = (LocalAdUserHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdUserHome.JNDI_NAME, LocalAdUserHome.class);              
            
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {
        	
        	LocalAdUser adUser = adUserHome.findByPrimaryKey(USR_CODE);
   
        	AdUserDetails details = new AdUserDetails();
        		details.setUsrCode(adUser.getUsrCode());        		
                details.setUsrName(adUser.getUsrName());
                details.setUsrDescription(adUser.getUsrDescription());
                details.setUsrPassword(adUser.getUsrPassword());
                details.setUsrPasswordExpirationCode(adUser.getUsrPasswordExpirationCode());
                details.setUsrPasswordExpirationDays(adUser.getUsrPasswordExpirationDays());
                details.setUsrPasswordExpirationAccess(adUser.getUsrPasswordExpirationAccess());
                details.setUsrDateFrom(adUser.getUsrDateFrom());
                details.setUsrDateTo(adUser.getUsrDateTo());
                        
        	return details;
        	       	
        } catch (Exception ex) {
        	
        	throw new EJBException(ex.getMessage());
        	
        }
        
	}      
	
    /**
     * @ejb:interface-method view-type="remote"
     **/    
    public void addAdUrEntry(com.util.AdUserResponsibilityDetails details, 
        Integer USR_CODE, String RS_NM, Integer AD_CMPNY) 
        throws GlobalRecordAlreadyExistException {
               
                    
        Debug.print("AdUserResponsibilityControllerBean addAdUrEntry");
        
        LocalAdUserResponsibilityHome adUserResponsibilityHome = null;
        LocalAdUserHome adUserHome = null;
        LocalAdResponsibilityHome adResponsibilityHome = null;
                       
        LocalAdUserResponsibility adUserResponsibility = null;
        LocalAdUser adUser = null;
        LocalAdResponsibility adResponsibility = null;
        
        // Initialize EJB Home
        
        try {
            
            adUserHome = (LocalAdUserHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdUserHome.JNDI_NAME, LocalAdUserHome.class);
            adUserResponsibilityHome = (LocalAdUserResponsibilityHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdUserResponsibilityHome.JNDI_NAME, LocalAdUserResponsibilityHome.class);                
            adResponsibilityHome = (LocalAdResponsibilityHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdResponsibilityHome.JNDI_NAME, LocalAdResponsibilityHome.class);                
                                                                                            
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
                                   
        try {

            adUserResponsibility = adUserResponsibilityHome.findByRsNameAndUsrCode(RS_NM, USR_CODE, AD_CMPNY);
        
            throw new GlobalRecordAlreadyExistException();
                                             
        } catch (GlobalRecordAlreadyExistException ex) {
        	
           throw ex;
        	
        } catch (FinderException ex) {
        	 	
        } catch (Exception ex) {
            
           throw new EJBException(ex.getMessage());
            
        }
        
        try {
        	
        	// Create New User Responsibilities
        	
        	adUserResponsibility = adUserResponsibilityHome.create(details.getUrDateFrom(), 
        	        details.getUrDateTo(), AD_CMPNY); 
        	        
        	adUser = adUserHome.findByPrimaryKey(USR_CODE);
		      	adUser.addAdUserResponsibility(adUserResponsibility);  
		      	
        	adResponsibility = adResponsibilityHome.findByRsName(RS_NM, AD_CMPNY);
		      	adResponsibility.addAdUserResponsibility(adUserResponsibility);  		      	          	    		
		    		    
		    
		            	        
        } catch (Exception ex) {
        	
        	getSessionContext().setRollbackOnly();        	
            throw new EJBException(ex.getMessage());
        	
        }        
                                    	                                        
    }	
    
    /**
     * @ejb:interface-method view-type="remote"
     **/    
    public void updateAdUrEntry(com.util.AdUserResponsibilityDetails details, 
        Integer USR_CODE, String RS_NM, Integer AD_CMPNY) 
        throws GlobalRecordAlreadyExistException {
               
                    
        Debug.print("AdUserResponsibilityControllerBean updateAdUrEntry");
        
        LocalAdUserResponsibilityHome adUserResponsibilityHome = null;
        LocalAdUserHome adUserHome = null;
        LocalAdResponsibilityHome adResponsibilityHome = null;
               
        LocalAdUserResponsibility adUserResponsibility = null;
        LocalAdUser adUser = null;
        LocalAdResponsibility adResponsibility = null;
        
        // Initialize EJB Home
        
        try {
            
            adUserHome = (LocalAdUserHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdUserHome.JNDI_NAME, LocalAdUserHome.class);
            adUserResponsibilityHome = (LocalAdUserResponsibilityHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdUserResponsibilityHome.JNDI_NAME, LocalAdUserResponsibilityHome.class);                
            adResponsibilityHome = (LocalAdResponsibilityHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdResponsibilityHome.JNDI_NAME, LocalAdResponsibilityHome.class);                
                                                                    
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }                                 

        try {

			adUserResponsibility = adUserResponsibilityHome.findByRsNameAndUsrCode(RS_NM, USR_CODE, AD_CMPNY);
              
            if(adUserResponsibility != null &&
                !adUserResponsibility.getUrCode().equals(details.getUrCode()))  {  
        
               throw new GlobalRecordAlreadyExistException();
             
            } 
                                             
        } catch (GlobalRecordAlreadyExistException ex) {
        	
           throw ex;
        	
        } catch (FinderException ex) {
        	 	
        } catch (Exception ex) {
            
           throw new EJBException(ex.getMessage());
            
        }

        // Find and Update User Responsibilities
               
        try {
        	
        	adUserResponsibility = adUserResponsibilityHome.findByPrimaryKey(details.getUrCode());
        		
                adUserResponsibility.setUrDateFrom(details.getUrDateFrom());
                adUserResponsibility.setUrDateTo(details.getUrDateTo());

        	adUser = adUserHome.findByPrimaryKey(USR_CODE);
		      	adUser.addAdUserResponsibility(adUserResponsibility);  
		      	
        	adResponsibility = adResponsibilityHome.findByRsName(RS_NM, AD_CMPNY);
		      	adResponsibility.addAdUserResponsibility(adUserResponsibility);  
      	        	                            	
        } catch (Exception ex) {
            
            throw new EJBException(ex.getMessage());
            
        }           	
                                                	                                        
    }	 
    
   /**
    * @ejb:interface-method view-type="remote"
    **/
    public void deleteAdUrEntry(Integer UR_CODE, Integer AD_CMPNY) 
        throws GlobalRecordAlreadyDeletedException {               

      Debug.print("AdUserResponsibilityControllerBean deleteAdUrEntry");

      LocalAdUserResponsibility adUserResponsibility = null;
      LocalAdUserResponsibilityHome adUserResponsibilityHome = null;

      // Initialize EJB Home
        
      try {

          adUserResponsibilityHome = (LocalAdUserResponsibilityHome)EJBHomeFactory.
              lookUpLocalHome(LocalAdUserResponsibilityHome.JNDI_NAME, LocalAdUserResponsibilityHome.class);           
      
      } catch (NamingException ex) {

          throw new EJBException(ex.getMessage());

      }                
      
      try {
      	
         adUserResponsibility = adUserResponsibilityHome.findByPrimaryKey(UR_CODE);
         
      } catch (FinderException ex) {
      	
         throw new GlobalRecordAlreadyDeletedException();
         
      } catch (Exception ex) {
      	
         throw new EJBException(ex.getMessage()); 
         
      }
      
      try {
         	
	      adUserResponsibility.remove();
	    
	  } catch (RemoveException ex) {
	 	
	      getSessionContext().setRollbackOnly();	    
	      throw new EJBException(ex.getMessage());
	    
	  } catch (Exception ex) {
	 	
	      getSessionContext().setRollbackOnly();	    
	      throw new EJBException(ex.getMessage());
	    
	  }	      
      
   }         
    
    // SessionBean methods

    /**
     * @ejb:create-method view-type="remote"
     **/
    public void ejbCreate() throws CreateException {

       Debug.print("AdUserResponsibilityControllerBean ejbCreate");
      
    }
}
