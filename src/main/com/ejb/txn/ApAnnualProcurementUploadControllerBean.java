package com.ejb.txn;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.GregorianCalendar;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;
import javax.naming.NamingException;

import com.ejb.ad.LocalAdAmountLimitHome;
import com.ejb.ad.LocalAdApprovalHome;
import com.ejb.ad.LocalAdApprovalQueueHome;
import com.ejb.ad.LocalAdApprovalUserHome;
import com.ejb.ad.LocalAdCompany;
import com.ejb.ad.LocalAdCompanyHome;
import com.ejb.ad.LocalAdLookUpValue;
import com.ejb.ad.LocalAdLookUpValueHome;
import com.ejb.ad.LocalAdPaymentSchedule;
import com.ejb.ad.LocalAdPaymentTerm;
import com.ejb.ad.LocalAdPaymentTermHome;
import com.ejb.ad.LocalAdPreference;
import com.ejb.ad.LocalAdPreferenceHome;
import com.ejb.ad.LocalAdUser;
import com.ejb.ad.LocalAdUserHome;
import com.ejb.ap.LocalApDistributionRecord;
import com.ejb.ap.LocalApDistributionRecordHome;
import com.ejb.ap.LocalApSupplier;
import com.ejb.ap.LocalApSupplierHome;
import com.ejb.ap.LocalApTaxCode;
import com.ejb.ap.LocalApTaxCodeHome;
import com.ejb.ap.LocalApVoucher;
import com.ejb.ap.LocalApVoucherBatch;
import com.ejb.ap.LocalApVoucherBatchHome;
import com.ejb.ap.LocalApVoucherHome;
import com.ejb.ap.LocalApVoucherLineItem;
import com.ejb.ap.LocalApVoucherLineItemHome;
import com.ejb.ap.LocalApVoucherPaymentSchedule;
import com.ejb.ap.LocalApVoucherPaymentScheduleHome;
import com.ejb.ap.LocalApWithholdingTaxCode;
import com.ejb.ap.LocalApWithholdingTaxCodeHome;
import com.ejb.exception.GlobalRecordInvalidException;
import com.ejb.gl.LocalAdDocumentSequenceAssignmentHome;
import com.ejb.gl.LocalGlChartOfAccount;
import com.ejb.gl.LocalGlChartOfAccountHome;
import com.ejb.gl.LocalGlFunctionalCurrency;
import com.ejb.gl.LocalGlFunctionalCurrencyHome;
import com.ejb.gl.LocalGlFunctionalCurrencyRateHome;
import com.ejb.inv.LocalInvCostingHome;
import com.ejb.inv.LocalInvItemLocation;
import com.ejb.inv.LocalInvItemLocationHome;
import com.ejb.inv.LocalInvItem;
import com.ejb.inv.LocalInvItemHome;
import com.ejb.inv.LocalInvUnitOfMeasure;
import com.ejb.inv.LocalInvUnitOfMeasureHome;
import com.ejb.inv.LocalInvTransactionalBudgetHome;
import com.ejb.inv.LocalInvTransactionalBudget;
import com.util.AbstractSessionBean;
import com.util.ApModVoucherDetails;
import com.util.ApModAnnualProcurementDetails;
import com.util.InvModTransactionalBudgetDetails;
import com.util.InvTransactionalBudgetDetails;
import com.util.ApModVoucherLineItemDetails;
import com.util.Debug;
import com.util.EJBCommon;
import com.util.EJBHomeFactory;

/**
 * @ejb:bean name="ApAnnualProcurementUploadControllerEJB"
 *           display-name="used for importing vouchers CSV"
 *           type="Stateless"
 *           view-type="remote"
 *           jndi-name="ejb/ApAnnualProcurementUploadControllerEJB"
 *
 * @ejb:interface remote-class="com.ejb.txn.ApAnnualProcurementUploadController"
 *                extends="javax.ejb.EJBObject"
 *
 * @ejb:home remote-class="com.ejb.txn.ApAnnualProcurementUploadControllerHome"
 *           extends="javax.ejb.EJBHome"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="apuser"
 *                        role-link="apuserlink"
 *
 * @ejb:permission role-name="apuser"
 * 
*/

public class ApAnnualProcurementUploadControllerBean extends AbstractSessionBean {
	
	/**
	 * @ejb:interface-method view-type="remote"
	 **/
	public void uploadAnnualProcurement(ArrayList list, String  userDepartment, Integer AD_BRNCH, Integer AD_CMPNY) throws
		GlobalRecordInvalidException {
		
		Debug.print("ApAnnualProcurementUploadControllerBean uploadAnnualProcurement");
		
		LocalInvTransactionalBudgetHome invTransactionalBudgetHome = null;

		LocalInvTransactionalBudget invTransactionalBudget = null;
		LocalInvUnitOfMeasureHome invUnitOfMeasureHome = null;
		LocalAdLookUpValueHome adLookUpValueHome = null;
		LocalInvUnitOfMeasure invUnitOfMeasure = null;
		LocalInvItemHome invItemHome = null;

		// Initialize EJB Home
		
		try {
			
			
			invTransactionalBudgetHome = (LocalInvTransactionalBudgetHome)EJBHomeFactory.
					lookUpLocalHome(LocalInvTransactionalBudgetHome.JNDI_NAME, LocalInvTransactionalBudgetHome.class);
			invItemHome = (LocalInvItemHome)EJBHomeFactory.
					lookUpLocalHome(LocalInvItemHome.JNDI_NAME, LocalInvItemHome.class);
			invUnitOfMeasureHome = (LocalInvUnitOfMeasureHome)EJBHomeFactory.
					lookUpLocalHome(LocalInvUnitOfMeasureHome.JNDI_NAME, LocalInvUnitOfMeasureHome.class);
			adLookUpValueHome = (LocalAdLookUpValueHome)EJBHomeFactory.
					lookUpLocalHome(LocalAdLookUpValueHome.JNDI_NAME, LocalAdLookUpValueHome.class);
			
		} catch (NamingException ex) {
			
			throw new EJBException(ex.getMessage());
			
		}       
		
		try {
			
			Iterator itr = list.iterator();
			int ctr=0;
			while (itr.hasNext()) {
				ctr++;

				InvModTransactionalBudgetDetails mdetails = (InvModTransactionalBudgetDetails)itr.next();

				
				// create transactional budget
				LocalInvItem invItem = null;
				
				try {
					invItem = invItemHome.findByIiName(mdetails.getTbItemName(), AD_CMPNY);
				} catch (FinderException ex) {
					throw new GlobalRecordInvalidException("" + ctr);
				}
				LocalAdLookUpValue adLookUpValue = null;
				
				try {
					adLookUpValue = adLookUpValueHome.findByLvName(userDepartment, AD_CMPNY);
				} catch (FinderException ex) {
					throw new GlobalRecordInvalidException("" + ctr);
				}

				LocalInvUnitOfMeasure invUOM = null;
				
				try {
					invUOM = invUnitOfMeasureHome.findByUomName(mdetails.getTbUnit(), AD_CMPNY);
				} catch (FinderException ex) {
					throw new GlobalRecordInvalidException("" + ctr);
				}
				int adLvDepartmentCode = adLookUpValue.getLvCode();
				System.out.println(mdetails.getTbQtyJan() + " <== jan controller bean");
				System.out.println(mdetails.getTbQtyFeb() + " <== feb controller bean");
				System.out.println(mdetails.getTbQtyMrch() + " <== mrch controller bean");
				System.out.println(mdetails.getTbQtyAprl() + " <== aprl controller bean");
				System.out.println(mdetails.getTbQtyMay() + " <== may controller bean");
				System.out.println(mdetails.getTbQtyJun() + " <== jun controller bean");
				System.out.println(mdetails.getTbQtyJul() + " <== jul controller bean");
				System.out.println(mdetails.getTbQtyAug() + " <== aug controller bean");
				System.out.println(mdetails.getTbQtySep() + " <== sep controller bean");
				System.out.println(mdetails.getTbQtyOct() + " <== oct controller bean");
				System.out.println(mdetails.getTbQtyNov() + " <== nov controller bean");
				System.out.println(mdetails.getTbQtyDec() + " <== dec controller bean");
				
				try{
					//search for existing transactional budget
					invTransactionalBudget = invTransactionalBudgetHome.findByTbItemNameAndDepartment(mdetails.getTbItemName(), adLvDepartmentCode, AD_CMPNY);
					//invTransactionalBudget = invTransactionalBudgetHome.findByTbCodeAndDepartmentCode(mdetails.getTbCode(), adLvDepartmentCode, AD_CMPNY);
					//update for same department
					invTransactionalBudget.setTbQuantityJan(mdetails.getTbQtyJan() + invTransactionalBudget.getTbQuantityJan());
					invTransactionalBudget.setTbQuantityFeb(mdetails.getTbQtyFeb() + invTransactionalBudget.getTbQuantityFeb());
					invTransactionalBudget.setTbQuantityMrch(mdetails.getTbQtyMrch() + invTransactionalBudget.getTbQuantityMrch());
					invTransactionalBudget.setTbQuantityAprl(mdetails.getTbQtyAprl() + invTransactionalBudget.getTbQuantityAprl());
					invTransactionalBudget.setTbQuantityMay(mdetails.getTbQtyMay()+ invTransactionalBudget.getTbQuantityMay());
					invTransactionalBudget.setTbQuantityJun(mdetails.getTbQtyJun()+ invTransactionalBudget.getTbQuantityJun());
					invTransactionalBudget.setTbQuantityJul(mdetails.getTbQtyJul()+ invTransactionalBudget.getTbQuantityJul());
					invTransactionalBudget.setTbQuantityAug(mdetails.getTbQtyAug()+ invTransactionalBudget.getTbQuantityAug());
					invTransactionalBudget.setTbQuantitySep(mdetails.getTbQtySep()+ invTransactionalBudget.getTbQuantitySep());
					invTransactionalBudget.setTbQuantityOct(mdetails.getTbQtyOct()+ invTransactionalBudget.getTbQuantityOct());
					invTransactionalBudget.setTbQuantityNov(mdetails.getTbQtyNov()+ invTransactionalBudget.getTbQuantityNov());
					invTransactionalBudget.setTbQuantityDec(mdetails.getTbQtyDec()+ invTransactionalBudget.getTbQuantityDec());
					invTransactionalBudget.setTbYear(mdetails.getTbYear());
					
					
				}catch(FinderException ex){
					//create if it does not exist
					invTransactionalBudget = invTransactionalBudgetHome.create(mdetails.getTbItemName(), invItem.getIiDescription(),
							mdetails.getTbQtyJan(), mdetails.getTbQtyFeb(),
							mdetails.getTbQtyMrch(),mdetails.getTbQtyAprl(),mdetails.getTbQtyMay(),
							mdetails.getTbQtyJun(), mdetails.getTbQtyJul(), mdetails.getTbQtyAug(),
							mdetails.getTbQtySep(), mdetails.getTbQtyOct(), mdetails.getTbQtyNov(), 
							mdetails.getTbQtyDec(), mdetails.getTbUnitCost(), mdetails.getTbTotalCost(), 
							mdetails.getTbYear(), AD_CMPNY );
					
					System.out.println(mdetails.getTbUnit() + " <== mdetails.getApUnit ");
					
					invTransactionalBudget.setInvUnitOfMeasureCode(invUOM.getUomCode());
					invTransactionalBudget.setInvUnitCode(invItem.getIiCode());
					System.out.println(adLookUpValue.getLvCode() + " <== look up value code for department");
					System.out.println(invTransactionalBudget.getTbCode() + " <== getTbCode under transactionalBudget ");
					//mdetails.setTbCode(invTransactionalBudget.getTbCode());
					//System.out.println(mdetails.getTbCode() + " <== getTbCode under mdetails ");
					invTransactionalBudget.setTbAdLookupValue(adLookUpValue.getLvCode());
				}
			}
				
				

		} catch (GlobalRecordInvalidException ex) {
			Debug.printStackTrace(ex);
			getSessionContext().setRollbackOnly();
			throw ex;
	
		
	    } catch (Exception ex) {
		
		Debug.printStackTrace(ex);
		getSessionContext().setRollbackOnly();
		throw new EJBException(ex.getMessage());
		
	    }
		
	}
	
	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public short getGlFcPrecisionUnit(Integer AD_CMPNY) {
		
		Debug.print("ApVoucherImportControllerBean getGlFcPrecisionUnit");
		
		
		LocalAdCompanyHome adCompanyHome = null;
		
		
		// Initialize EJB Home
		
		try {
			
			adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);       
			
		} catch (NamingException ex) {
			
			throw new EJBException(ex.getMessage());
			
		}
		
		try {
			
			LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
			
			return  (short)(10);
			
		} catch (Exception ex) {
			
			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());
			
		}
		
	}
	
	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public ArrayList getAdLvDEPARTMENT(Integer AD_CMPNY) {
		
		Debug.print("ApRepAnnualProcurementControllerBean getAdLvDEPARTMENT");
		
		LocalAdLookUpValueHome adLookUpValueHome = null;               
		
		ArrayList list = new ArrayList();
		
		// Initialize EJB Home
		
		try {
			
			adLookUpValueHome = (LocalAdLookUpValueHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdLookUpValueHome.JNDI_NAME, LocalAdLookUpValueHome.class);
			
		} catch (NamingException ex) {
			
			throw new EJBException(ex.getMessage());
			
		}        
		
		try {
			
			Collection adLookUpValues = adLookUpValueHome.findByLuName("DEPARTMENT", AD_CMPNY);
			
			Iterator i = adLookUpValues.iterator();
			
			while (i.hasNext()) {
				
				LocalAdLookUpValue adLookUpValue = (LocalAdLookUpValue)i.next();
				
				list.add(adLookUpValue.getLvName());
				
			}
			
			return list;
			
		} catch (Exception ex) {
			
			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());
			
		}
		
	}
	
	/**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public String getAdUsrDeptartment(String USR_NM, Integer AD_CMPNY) {
                    
        Debug.print("ApPurchaseRequisitionEntryControllerBean getAdUsrAll");
        
        LocalAdUserHome adUserHome = null;               
        LocalAdUser adUser = null;
        ArrayList list = new ArrayList();
        
        // Initialize EJB Home
        
        try {
            
            adUserHome = (LocalAdUserHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdUserHome.JNDI_NAME, LocalAdUserHome.class);
            
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }        
        
        try {
        	
        	//Collection adUsers = adUserHome.findUsrByDepartment(USR_DEPT, AD_CMPNY);
        	adUser = adUserHome.findByUsrName(USR_NM, AD_CMPNY);
        	String department = adUser.getUsrDept();
        	
        	return department;
        	
        } catch (Exception ex) {
        	
        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());
        	
        }
            
    }
	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public short getInvGpQuantityPrecisionUnit(Integer AD_CMPNY) {
		
		Debug.print("ApVoucherImportControllerBean getInvGpQuantityPrecisionUnit");
		
		LocalAdPreferenceHome adPreferenceHome = null;         
		
		// Initialize EJB Home
		
		try {
			
			adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);       
			
		} catch (NamingException ex) {
			
			throw new EJBException(ex.getMessage());
			
		}
		
		
		try {
			
			LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);
			
			return (short)(10);
			
		} catch (Exception ex) {
			
			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());
			
		}
		
	}
	
	// SessionBean methods
	
	/**
	 * @ejb:create-method view-type="remote"
	 **/
	public void ejbCreate() throws CreateException {
		
		Debug.print("ApVoucherImportControllerBean ejbCreate");
		
	}
	
}