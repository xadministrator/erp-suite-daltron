/*
 * ApApprovalControllerBean.java
 *
 * Created on March 24, 2004, 8:37 PM
 *
 * @author  Dennis M. Hilario
 */

package com.ejb.txn;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.naming.NamingException;

import com.ejb.ad.LocalAdApprovalQueue;
import com.ejb.ad.LocalAdApprovalQueueHome;
import com.ejb.ad.LocalAdBankAccount;
import com.ejb.ad.LocalAdBankAccountBalance;
import com.ejb.ad.LocalAdBankAccountBalanceHome;
import com.ejb.ad.LocalAdBankAccountHome;
import com.ejb.ad.LocalAdBranchDocumentSequenceAssignment;
import com.ejb.ad.LocalAdBranchDocumentSequenceAssignmentHome;
import com.ejb.ad.LocalAdBranchItemLocation;
import com.ejb.ad.LocalAdBranchItemLocationHome;
import com.ejb.ad.LocalAdCompany;
import com.ejb.ad.LocalAdCompanyHome;
import com.ejb.ad.LocalAdPreference;
import com.ejb.ad.LocalAdPreferenceHome;
import com.ejb.ap.LocalApAppliedVoucher;
import com.ejb.ap.LocalApCheck;
import com.ejb.ap.LocalApCheckHome;
import com.ejb.ap.LocalApDistributionRecord;
import com.ejb.ap.LocalApDistributionRecordHome;
import com.ejb.ap.LocalApPurchaseOrder;
import com.ejb.ap.LocalApPurchaseOrderHome;
import com.ejb.ap.LocalApPurchaseOrderLine;
import com.ejb.ap.LocalApPurchaseRequisition;
import com.ejb.ap.LocalApPurchaseRequisitionHome;
import com.ejb.ap.LocalApPurchaseRequisitionLine;
import com.ejb.ap.LocalApSupplier;
import com.ejb.ap.LocalApSupplierBalance;
import com.ejb.ap.LocalApSupplierBalanceHome;
import com.ejb.ap.LocalApVoucher;
import com.ejb.ap.LocalApVoucherHome;
import com.ejb.ap.LocalApVoucherLineItem;
import com.ejb.ap.LocalApVoucherPaymentSchedule;
import com.ejb.ap.LocalApCanvass;
import com.ejb.ap.LocalApCanvassHome;
import com.ejb.ad.LocalAdUserHome;
import com.ejb.ad.LocalAdUser;
import com.ejb.ad.LocalAdLookUpValueHome;
import com.ejb.ad.LocalAdLookUpValue;
import com.ejb.ap.LocalApPurchaseRequisitionHome;
import com.ejb.ap.LocalApPurchaseRequisition;
import com.ejb.inv.LocalInvTransactionalBudgetHome;
import com.ejb.inv.LocalInvTransactionalBudget;
import com.ejb.inv.LocalInvItemHome;
import com.ejb.inv.LocalInvItem;
import com.ejb.exception.AdPRFCoaGlVarianceAccountNotFoundException;
import com.ejb.exception.GlJREffectiveDateNoPeriodExistException;
import com.ejb.exception.GlJREffectiveDatePeriodClosedException;
import com.ejb.exception.GlobalBranchAccountNumberInvalidException;
import com.ejb.exception.GlobalConversionDateNotExistException;
import com.ejb.exception.GlobalInventoryDateException;
import com.ejb.exception.GlobalJournalNotBalanceException;
import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.exception.GlobalRecordAlreadyDeletedException;
import com.ejb.exception.GlobalSendEmailMessageException;
import com.ejb.exception.GlobalTransactionAlreadyPostedException;
import com.ejb.exception.GlobalTransactionAlreadyVoidException;
import com.ejb.exception.GlobalTransactionAlreadyVoidPostedException;
import com.ejb.gl.LocalAdDocumentSequenceAssignment;
import com.ejb.gl.LocalAdDocumentSequenceAssignmentHome;
import com.ejb.gl.LocalGlAccountingCalendarValue;
import com.ejb.gl.LocalGlAccountingCalendarValueHome;
import com.ejb.gl.LocalGlChartOfAccount;
import com.ejb.gl.LocalGlChartOfAccountBalance;
import com.ejb.gl.LocalGlChartOfAccountBalanceHome;
import com.ejb.gl.LocalGlChartOfAccountHome;
import com.ejb.gl.LocalGlForexLedger;
import com.ejb.gl.LocalGlForexLedgerHome;
import com.ejb.gl.LocalGlFunctionalCurrency;
import com.ejb.gl.LocalGlFunctionalCurrencyHome;
import com.ejb.gl.LocalGlFunctionalCurrencyRate;
import com.ejb.gl.LocalGlFunctionalCurrencyRateHome;
import com.ejb.gl.LocalGlJournal;
import com.ejb.gl.LocalGlJournalBatch;
import com.ejb.gl.LocalGlJournalBatchHome;
import com.ejb.gl.LocalGlJournalCategory;
import com.ejb.gl.LocalGlJournalCategoryHome;
import com.ejb.gl.LocalGlJournalHome;
import com.ejb.gl.LocalGlJournalLine;
import com.ejb.gl.LocalGlJournalLineHome;
import com.ejb.gl.LocalGlJournalSource;
import com.ejb.gl.LocalGlJournalSourceHome;
import com.ejb.gl.LocalGlSetOfBook;
import com.ejb.gl.LocalGlSetOfBookHome;
import com.ejb.gl.LocalGlSuspenseAccount;
import com.ejb.gl.LocalGlSuspenseAccountHome;
import com.ejb.inv.LocalInvAdjustment;
import com.ejb.inv.LocalInvAdjustmentHome;
import com.ejb.inv.LocalInvAdjustmentLine;
import com.ejb.inv.LocalInvAdjustmentLineHome;
import com.ejb.inv.LocalInvCosting;
import com.ejb.inv.LocalInvCostingHome;
import com.ejb.inv.LocalInvDistributionRecord;
import com.ejb.inv.LocalInvDistributionRecordHome;
import com.ejb.inv.LocalInvItem;
import com.ejb.inv.LocalInvItemLocation;
import com.ejb.inv.LocalInvItemLocationHome;
import com.ejb.inv.LocalInvUnitOfMeasure;
import com.ejb.inv.LocalInvUnitOfMeasureConversion;
import com.ejb.inv.LocalInvUnitOfMeasureConversionHome;
import com.ejb.pm.LocalPmProject;
import com.ejb.pm.LocalPmProjecting;
import com.ejb.pm.LocalPmProjectingHome;
import com.util.AbstractSessionBean;
import com.util.AdModApprovalQueueDetails;
import com.util.Debug;
import com.util.EJBCommon;
import com.util.EJBHomeFactory;

/**
 * @ejb:bean name="ApApprovalControllerEJB"
 *           display-name="Used for approving ap documents"
 *           type="Stateless"
 *           view-type="remote"
 *           jndi-name="ejb/ApApprovalControllerEJB"
 *
 * @ejb:interface remote-class="com.ejb.txn.ApApprovalController"
 *                extends="javax.ejb.EJBObject"
 *
 * @ejb:home remote-class="com.ejb.txn.ApApprovalControllerHome"
 *           extends="javax.ejb.EJBHome"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="apuser"
 *                        role-link="apuserlink"
 *
 * @ejb:permission role-name="apuser"
 * 
*/

public class ApApprovalControllerBean extends AbstractSessionBean {

	
	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public ArrayList getAdApprovalNotifiedUsersByAqCode(Integer AQ_CODE, Integer AD_CMPNY) {
		
		Debug.print("ApPurchaseRequisitionEntryControllerBean getAdApprovalNotifiedUsersByAqCode");
		
		LocalAdApprovalQueueHome adApprovalQueueHome = null;
		LocalApPurchaseRequisitionHome apPurchaseRequisitionHome = null;
		LocalApPurchaseOrderHome apPurchaseOrderHome = null;
		LocalAdPreferenceHome adPreferenceHome = null;
		
		ArrayList list = new ArrayList();
		
		
		// Initialize EJB Home
		
		try {
			
			adApprovalQueueHome = (LocalAdApprovalQueueHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdApprovalQueueHome.JNDI_NAME, LocalAdApprovalQueueHome.class); 
			apPurchaseRequisitionHome = (LocalApPurchaseRequisitionHome)EJBHomeFactory.
			lookUpLocalHome(LocalApPurchaseRequisitionHome.JNDI_NAME, LocalApPurchaseRequisitionHome.class);  
			apPurchaseOrderHome = (LocalApPurchaseOrderHome)EJBHomeFactory.
			lookUpLocalHome(LocalApPurchaseOrderHome.JNDI_NAME, LocalApPurchaseOrderHome.class);     
			adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
					lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);      
					
		} catch (NamingException ex) {
			
			throw new EJBException(ex.getMessage());
			
		}
		
		try {
			
			LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);
			LocalAdApprovalQueue adApprovalQueue = null;
			try {
				
				adApprovalQueue = adApprovalQueueHome.findByPrimaryKey(AQ_CODE);
				
				System.out.println(">approval queue code is: "  + AQ_CODE);
				adApprovalQueue = adApprovalQueueHome.findByPrimaryKey(AQ_CODE);
				
				
				if(adApprovalQueue.getAqApproved()==EJBCommon.TRUE) {
					System.out.println("approved succesfull 1");
					list.add("DOCUMENT POSTED");
				}

			}catch (Exception e) {
				list.add("DOCUMENT REJECTED");
				return list;
			}
			
			if(adApprovalQueue.getAqNextLevel()==null) {
				return list;
			}
			
			
			if(adApprovalQueue.getAqDocument().equals("AP PURCHASE REQUISITION")){
			
				LocalApPurchaseRequisition apPurchaseRequisition = apPurchaseRequisitionHome.findByPrimaryKey(adApprovalQueue.getAqDocumentCode());
				if (apPurchaseRequisition.getPrPosted() == EJBCommon.TRUE) {
	
	                list.add("DOCUMENT POSTED");
	                return list;
	
	            }
			}
			
			if(adApprovalQueue.getAqDocument().equals("AP RECEIVING ITEM")){
			
				LocalApPurchaseOrder apPurchaseOrder = apPurchaseOrderHome.findByPrimaryKey(adApprovalQueue.getAqDocumentCode());
				if (apPurchaseOrder.getPoPosted() == EJBCommon.TRUE) {
	
	                list.add("DOCUMENT POSTED");
	                return list;
	
	            }
            
			}
		
			
			
			String messageUser = "";
			LocalAdApprovalQueue adNextApprovalQueue = adApprovalQueueHome.findByAqDeptAndAqLevelAndAqDocumentCode(
					adApprovalQueue.getAqDepartment(), adApprovalQueue.getAqNextLevel(), adApprovalQueue.getAqDocumentCode(), AD_CMPNY);
		//	list.add(adNextApprovalQueue.getAqLevel() +" APPROVER - "+ adNextApprovalQueue.getAdUser().getUsrDescription());
			
			messageUser = adNextApprovalQueue.getAqLevel() +" APPROVER - "+ adNextApprovalQueue.getAdUser().getUsrDescription();
			System.out.println("adNextApprovalQueue.getAdUser().getUsrDescription()="+adNextApprovalQueue.getAdUser().getUsrDescription());
			adNextApprovalQueue.setAqForApproval(EJBCommon.TRUE);
			try {
				
				
			
				
				if(adPreference.getPrfAdEnableEmailNotification()==EJBCommon.TRUE) {
					if((!adApprovalQueue.getAdUser().getUsrEmailAddress().equals("")) && (adPreference.getPrfAdEnableEmailNotification() == EJBCommon.TRUE)) {
						this.sendEmail(adNextApprovalQueue, AD_CMPNY);
						
						messageUser += " [Email Notification Sent.]";
					}
				}
				
				
			} catch (Exception e) {
				messageUser += " [Email Notification Not Sent. Cannot connect host or no internet connection.]";
			}

			list.add(messageUser);
			return list;
			
		} catch (Exception ex) {
			
			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());
			
		}
		
	}
	
    
    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList getAdAqByAqDocumentAndUserName(HashMap criteria,
        String USR_NM, Integer OFFSET, Integer LIMIT, String ORDER_BY, Integer AD_BRNCH, Integer AD_CMPNY) 
        throws GlobalNoRecordFoundException {
                    
        Debug.print("ApApprovalControllerBean getAdAqByAqDocumentAndUserName");
        
        LocalAdApprovalQueueHome adApprovalQueueHome = null;
        LocalApVoucherHome apVoucherHome = null;
        LocalApCheckHome apCheckHome = null;
        LocalApPurchaseOrderHome apPurchaseOrderHome = null;
        LocalApPurchaseRequisitionHome apPurchaseRequisitionHome = null;
        
        ArrayList list = new ArrayList();
        
        //initialized EJB Home
        
        try {
            
            adApprovalQueueHome = (LocalAdApprovalQueueHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdApprovalQueueHome.JNDI_NAME, LocalAdApprovalQueueHome.class);
            apVoucherHome = (LocalApVoucherHome)EJBHomeFactory.
                lookUpLocalHome(LocalApVoucherHome.JNDI_NAME, LocalApVoucherHome.class);
            apCheckHome = (LocalApCheckHome)EJBHomeFactory.
                lookUpLocalHome(LocalApCheckHome.JNDI_NAME, LocalApCheckHome.class);              
            apPurchaseOrderHome = (LocalApPurchaseOrderHome)EJBHomeFactory.
            	lookUpLocalHome(LocalApPurchaseOrderHome.JNDI_NAME, LocalApPurchaseOrderHome.class);     
            apPurchaseRequisitionHome = (LocalApPurchaseRequisitionHome)EJBHomeFactory.
        		lookUpLocalHome(LocalApPurchaseRequisitionHome.JNDI_NAME, LocalApPurchaseRequisitionHome.class);     
        
            
            
        } catch (NamingException ex) 	{
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {
        	
        	StringBuffer jbossQl = new StringBuffer();
        	jbossQl.append("SELECT OBJECT(aq) FROM AdApprovalQueue aq ");
        	
        	boolean firstArgument = true;
        	short ctr = 0;
        	int criteriaSize = criteria.size() + 2;	      
        	
        	Object obj[];	      
        	
        	// Allocate the size of the object parameter
        	
        	obj = new Object[criteriaSize];
        	if (criteria.containsKey("document")) {
        		if (!firstArgument) {
        			jbossQl.append("AND ");
        		} else {
        			firstArgument = false;
        			jbossQl.append("WHERE ");
        		}
        		jbossQl.append("aq.aqDocument=?" + (ctr+1) + " ");
        		obj[ctr] = (String)criteria.get("document");
        		ctr++;
        	}
        	
        	if (criteria.containsKey("dateFrom")) {
        		if (!firstArgument) {
        			jbossQl.append("AND ");
        		} else {
        			firstArgument = false;
        			jbossQl.append("WHERE ");
        		}
        		jbossQl.append("aq.aqDate>=?" + (ctr+1) + " ");
        		obj[ctr] = (Date)criteria.get("dateFrom");
        		ctr++;
        	}  
        	
        	if (criteria.containsKey("dateTo")) {
        		if (!firstArgument) {
        			jbossQl.append("AND ");
        		} else {
        			firstArgument = false;
        			jbossQl.append("WHERE ");
        		}
        		jbossQl.append("aq.aqDate<=?" + (ctr+1) + " ");
        		obj[ctr] = (Date)criteria.get("dateTo");
        		ctr++;
        		
        	}    
        	
        	if (criteria.containsKey("documentNumberFrom")) {
        		if (!firstArgument) {
        			jbossQl.append("AND ");
        		} else {
        			firstArgument = false;
        			jbossQl.append("WHERE ");
        		}
        		jbossQl.append("aq.aqDocumentNumber>=?" + (ctr+1) + " ");
        		obj[ctr] = (String)criteria.get("documentNumberFrom");
        		ctr++;
        	}  
        	
        	if (criteria.containsKey("documentNumberTo")) {
        		if (!firstArgument) {
        			jbossQl.append("AND ");
        		} else {
        			firstArgument = false;
        			jbossQl.append("WHERE ");
        		}
        		jbossQl.append("aq.aqDocumentNumber<=?" + (ctr+1) + " ");
        		obj[ctr] = (String)criteria.get("documentNumberTo");
        		ctr++;
        	}
        	
        	if (!firstArgument) {
        		jbossQl.append("AND ");	
        	} else {	
        		firstArgument = false;
        		jbossQl.append("WHERE ");
        	}
        	//--------------------------------------------------------------
        	System.out.print("Branch = " + AD_BRNCH + " AD_CMPNY = " + AD_CMPNY + " USR_NM = " + USR_NM);
        	//add department criteria
        	//-------------
        	//jbossQl.append("aq.aqAdBranch=" + AD_BRNCH + " AND aq.aqAdCompany=" + AD_CMPNY + " " + "AND aq.adUser.usrName='" + USR_NM + "'");
        	jbossQl.append("aq.aqForApproval = 1 AND aq.aqApproved = 0 AND aq.aqAdCompany=" + AD_CMPNY + " " + "AND aq.adUser.usrName='" + USR_NM + "'");
        	//-------------
        	String orderBy = null;
        	
        	if (ORDER_BY.equals("DOCUMENT NUMBER")) {	          
        		orderBy = "aq.aqDocumentNumber";
        	} else if (ORDER_BY.equals("DATE")) {
        		orderBy = "aq.aqDate";
        	}
        	
        	if (orderBy != null) {
        		jbossQl.append("ORDER BY " + orderBy + ", aq.aqDate");
        	} else {
        		jbossQl.append("ORDER BY aq.aqDate");
        	}
        	
        	jbossQl.append(" OFFSET ?" + (ctr + 1));	        
        	obj[ctr] = OFFSET;	      
        	ctr++;
        	
        	jbossQl.append(" LIMIT ?" + (ctr + 1));
        	obj[ctr] = LIMIT;
        	ctr++;
        	
        	System.out.println("jbossQl.toString()="+jbossQl.toString());
        	Collection adApprovalQueues = adApprovalQueueHome.getAqByCriteria(jbossQl.toString(), obj);	
        	
        	String AQ_DCMNT = (String)criteria.get("document"); 
        	
        	System.out.println("AQ_DCMNT------------->"+AQ_DCMNT);
      
        	if (adApprovalQueues.size() == 0)
        		throw new GlobalNoRecordFoundException();

        	Iterator i = adApprovalQueues.iterator();
        	
        	while (i.hasNext()) {
        		
        		LocalAdApprovalQueue adApprovalQueue = (LocalAdApprovalQueue)i.next();
        		
        		AdModApprovalQueueDetails details = new AdModApprovalQueueDetails();
        		
        		details.setAqCode(adApprovalQueue.getAqCode());
        		details.setAqDocument(adApprovalQueue.getAqDocument());
        		details.setAqDocumentCode(adApprovalQueue.getAqDocumentCode());
        		details.setAqDepartment(adApprovalQueue.getAqDepartment());

        		if (AQ_DCMNT.equals("AP VOUCHER") || AQ_DCMNT.equals("AP DEBIT MEMO") || AQ_DCMNT.equals("AP CHECK PAYMENT REQUEST")) {
        			
        			LocalApVoucher apVoucher = apVoucherHome.findByPrimaryKey(adApprovalQueue.getAqDocumentCode());
        			
        			details.setAqDate(apVoucher.getVouDate());
        			details.setAqDocumentNumber(apVoucher.getVouDocumentNumber());
        			details.setAqBrCode(apVoucher.getVouAdBranch());
        			
        			if (AQ_DCMNT.equals("AP VOUCHER")|| AQ_DCMNT.equals("AP CHECK PAYMENT REQUEST")) {
        				
        				details.setAqAmount(apVoucher.getVouAmountDue());
        				
        			} else {
        				
        				details.setAqAmount(apVoucher.getVouBillAmount());
        				
        			}
        			
        			details.setAqSupplierCode(apVoucher.getApSupplier().getSplSupplierCode());
        			
        			if(!apVoucher.getApVoucherLineItems().isEmpty()) {
        				
        				details.setAqType("ITEMS");
        				
        			} else if(!apVoucher.getApPurchaseOrderLines().isEmpty()) {
        				
        				details.setAqType("PO MATCHED");
        				
        			} else {
        				
        				details.setAqType("EXPENSES");
        				
        			}
        			
        			details.setAqReferenceNumber(apVoucher.getVouReferenceNumber());
        			details.setAqSupplierName(apVoucher.getApSupplier().getSplName());
        			
        		} else if (AQ_DCMNT.equals("AP PURCHASE ORDER") || AQ_DCMNT.equals("AP RECEIVING ITEM")) {
        			
        			System.out.println("AQ_DCMNT="+AQ_DCMNT);
        			LocalApPurchaseOrder apPurchaseOrder = apPurchaseOrderHome.findByPrimaryKey(adApprovalQueue.getAqDocumentCode());
        			System.out.println("apPurchaseOrder="+apPurchaseOrder.getPoDocumentNumber());
        			details.setAqDate(apPurchaseOrder.getPoDate());
        			details.setAqDocumentNumber(apPurchaseOrder.getPoDocumentNumber());
        			details.setAqBrCode(apPurchaseOrder.getPoAdBranch());
        			Collection apPurchaseOrderLines = apPurchaseOrder.getApPurchaseOrderLines(); 
        			Iterator j = apPurchaseOrderLines.iterator();
        			double TTL_AMNT = 0d; 
        			
        			while (j.hasNext()) {
        				
        				LocalApPurchaseOrderLine apPurchaseOrderLine = (LocalApPurchaseOrderLine) j.next();
        				
        				TTL_AMNT += apPurchaseOrderLine.getPlAmount();
        				
        			}
        			
        			details.setAqAmount(TTL_AMNT);		    		
        			details.setAqSupplierCode(apPurchaseOrder.getApSupplier().getSplSupplierCode());		    		
        			details.setAqDocumentType(apPurchaseOrder.getPoType());
        			details.setAqType("ITEMS");
        			details.setAqReferenceNumber(apPurchaseOrder.getPoReferenceNumber());
        			details.setAqSupplierName(apPurchaseOrder.getApSupplier().getSplName());

				} else if (AQ_DCMNT.equals("AP PURCHASE REQUISITION")) {
        			
        			LocalApPurchaseRequisition apPurchaseRequisition = apPurchaseRequisitionHome.findByPrimaryKey(adApprovalQueue.getAqDocumentCode());
        			
        			details.setAqDate(apPurchaseRequisition.getPrDate());
        			details.setAqDocumentNumber(apPurchaseRequisition.getPrNumber());
        			details.setAqBrCode(apPurchaseRequisition.getPrAdBranch());
        			
        			Collection apPurchaseRequisitionLines = apPurchaseRequisition.getApPurchaseRequisitionLines(); 
        			Iterator x = apPurchaseRequisitionLines.iterator();
        			double TTL_AMNTs = 0d; 
        			
        			while (x.hasNext()) {
        				
        				LocalApPurchaseRequisitionLine apPurchaseRequisitionLine = (LocalApPurchaseRequisitionLine) x.next();
        				
        			//	TTL_AMNTs += apPurchaseRequisitionLine.getPrlQuantity() * apPurchaseRequisitionLine.getPrlAmount();
        				
        				Collection apCanvasses = apPurchaseRequisitionLine.getApCanvasses();
        				
        				Iterator y = apCanvasses.iterator();
        				
        				while(y.hasNext()) {
        					
        					LocalApCanvass apCanvass = (LocalApCanvass)y.next();

        					if(apCanvass.getCnvPo()== EJBCommon.TRUE) {
        						
        						details.setAqSupplierCode(apCanvass.getApSupplier().getSplSupplierCode());
        						details.setAqSupplierName(apCanvass.getApSupplier().getSplName());
        						TTL_AMNTs += apCanvass.getCnvAmount();
        					}

        				}
        			}
        			
        			details.setAqAmount(TTL_AMNTs);		    				    		
        			details.setAqDocumentType(apPurchaseRequisition.getPrType());
        			details.setAqType("ITEMS");
        			details.setAqReferenceNumber(apPurchaseRequisition.getPrReferenceNumber());

        		
				} else if (AQ_DCMNT.equals("AP CANVASS")) {
	        			
						LocalApPurchaseRequisition apPurchaseRequisition = apPurchaseRequisitionHome.findByPrimaryKey(adApprovalQueue.getAqDocumentCode());
	        			
						
	        			details.setAqDate(apPurchaseRequisition.getPrDate());
	        			details.setAqDocumentNumber(apPurchaseRequisition.getPrNumber());
	        			details.setAqBrCode(apPurchaseRequisition.getPrAdBranch());
	        			Collection apPurchaseRequisitionLines = apPurchaseRequisition.getApPurchaseRequisitionLines(); 
	        			
	        			Iterator x = apPurchaseRequisitionLines.iterator();
	        			double TTL_AMNT_CNV = 0d; 
	        			
	        			while (x.hasNext()) {
	        				LocalApPurchaseRequisitionLine apPurchaseRequisitionLine = (LocalApPurchaseRequisitionLine) x.next();
	        				Collection apCanvasses = apPurchaseRequisitionLine.getApCanvasses();
		        			
		        			
		        			Iterator j = apCanvasses.iterator();
	
		        			while (j.hasNext()) {
		        				
		        				LocalApCanvass apCanvass = (LocalApCanvass) j.next();
		        				if (apCanvass.getCnvPo() == 1){
		        				TTL_AMNT_CNV += apCanvass.getCnvAmount();
		        				}
		        			}
	        			}

	        			details.setAqAmount(TTL_AMNT_CNV);		    		
	        			details.setAqSupplierCode(null);		    		
	        			details.setAqDocumentType(apPurchaseRequisition.getPrType());
	        			details.setAqType("ITEMS");
	        			details.setAqReferenceNumber(apPurchaseRequisition.getPrReferenceNumber());
	        			details.setAqSupplierName(null);
        			
        		} else {
        			
        			LocalApCheck apCheck = apCheckHome.findByPrimaryKey(adApprovalQueue.getAqDocumentCode());
        			
        			details.setAqDate(apCheck.getChkDate());
        			details.setAqDocumentNumber(apCheck.getChkDocumentNumber());
        			details.setAqAmount(apCheck.getChkAmount());		    		
        			details.setAqSupplierCode(apCheck.getApSupplier().getSplSupplierCode());		    		
        			details.setAqDocumentType(apCheck.getChkType());
        			details.setAqBrCode(apCheck.getChkAdBranch());
        			
        			if(!apCheck.getApVoucherLineItems().isEmpty()) {
        				
        				details.setAqType("ITEMS");
        				
        			} else {
        				
        				details.setAqType("EXPENSES");
        				
        			}
        			
        			details.setAqReferenceNumber(apCheck.getChkReferenceNumber());
        			details.setAqSupplierName(apCheck.getApSupplier().getSplName());
        			
        		}
        		
        		
        		list.add(details);
        		
        	}
        	
        	return list;
        	
        } catch (GlobalNoRecordFoundException ex) {
		  	 
			throw ex;
		  	
		} catch (Exception ex) {
		  	
	
		  	ex.printStackTrace();
		  	throw new EJBException(ex.getMessage());
		  	
		}
        
    }
    
    /**
     * @ejb:interface-method view-type="remote"
     **/
    public void executeApApproval(String AQ_DCMNT, Integer AQ_DCMNT_CODE, String USR_NM, boolean isApproved, String RSN_FR_RJCTN, Integer AD_BRNCH, Integer AD_CMPNY) throws
        GlobalRecordAlreadyDeletedException,
        GlobalTransactionAlreadyPostedException,
		GlobalTransactionAlreadyVoidException,
		GlobalTransactionAlreadyVoidPostedException,
		GlJREffectiveDateNoPeriodExistException,
		GlJREffectiveDatePeriodClosedException,
		GlobalJournalNotBalanceException,
		GlobalInventoryDateException,
		AdPRFCoaGlVarianceAccountNotFoundException {
                    
        Debug.print("ApApprovalControllerBean executeApApproval");
        
        LocalAdApprovalQueueHome adApprovalQueueHome = null;
        LocalApVoucherHome apVoucherHome = null;  
        LocalApCheckHome apCheckHome = null;      
        LocalAdPreferenceHome adPreferenceHome = null;
        LocalInvCostingHome invCostingHome = null;
        LocalInvItemLocationHome invItemLocationHome = null;
        LocalApPurchaseOrderHome apPurchaseOrderHome = null;
        LocalApPurchaseRequisitionHome apPurchaseRequisitionHome = null;
        LocalAdUserHome adUserHome = null;
        
        
        LocalAdApprovalQueue adApprovalQueue = null;         
                
        // Initialize EJB Home
        
        try {
            
            adApprovalQueueHome = (LocalAdApprovalQueueHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdApprovalQueueHome.JNDI_NAME, LocalAdApprovalQueueHome.class);            
            apVoucherHome = (LocalApVoucherHome)EJBHomeFactory.
                lookUpLocalHome(LocalApVoucherHome.JNDI_NAME, LocalApVoucherHome.class);
            apCheckHome = (LocalApCheckHome)EJBHomeFactory.
                lookUpLocalHome(LocalApCheckHome.JNDI_NAME, LocalApCheckHome.class);
            adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);            
            invCostingHome = (LocalInvCostingHome)EJBHomeFactory.
        		lookUpLocalHome(LocalInvCostingHome.JNDI_NAME, LocalInvCostingHome.class);
            invItemLocationHome = (LocalInvItemLocationHome)EJBHomeFactory.
        		lookUpLocalHome(LocalInvItemLocationHome.JNDI_NAME, LocalInvItemLocationHome.class);
            apPurchaseOrderHome = (LocalApPurchaseOrderHome)EJBHomeFactory.
    			lookUpLocalHome(LocalApPurchaseOrderHome.JNDI_NAME, LocalApPurchaseOrderHome.class);
            apPurchaseRequisitionHome = (LocalApPurchaseRequisitionHome)EJBHomeFactory.
                	lookUpLocalHome(LocalApPurchaseRequisitionHome.JNDI_NAME, LocalApPurchaseRequisitionHome.class);
            adUserHome = (LocalAdUserHome)EJBHomeFactory.
                	lookUpLocalHome(LocalAdUserHome.JNDI_NAME, LocalAdUserHome.class);
          
            
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }       
        
        try {
        	// validate if approval queue is already deleted
        	try {       			
        		adApprovalQueue = adApprovalQueueHome.findByAqDocumentAndAqDocumentCodeAndUsrName(AQ_DCMNT, AQ_DCMNT_CODE, USR_NM, AD_CMPNY);	        		
        	} catch (FinderException ex) {
        		throw new GlobalRecordAlreadyDeletedException();
        	}        	
        	
        	// approve/reject
        	
        	Collection adApprovalQueues = adApprovalQueueHome.findByAqDocumentAndAqDocumentCode(AQ_DCMNT, AQ_DCMNT_CODE, AD_CMPNY);
        	Collection adApprovalQueuesDesc = adApprovalQueueHome.findByAqDocumentAndAqDocumentCodeLessThanDesc(AQ_DCMNT, AQ_DCMNT_CODE, adApprovalQueue.getAqCode(), AD_CMPNY);
        	Collection adApprovalQueuesAsc = adApprovalQueueHome.findByAqDocumentAndAqDocumentCodeGreaterThanAsc(AQ_DCMNT, AQ_DCMNT_CODE, adApprovalQueue.getAqCode(), AD_CMPNY);
        	
        	Collection adAllApprovalQueues = adApprovalQueueHome.findAllByAqDocumentAndAqDocumentCode(AQ_DCMNT, AQ_DCMNT_CODE, AD_CMPNY);
        	
        	LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);
        	    
        	if (AQ_DCMNT.equals("AP VOUCHER") || AQ_DCMNT.equals("AP DEBIT MEMO")) {        		
        		LocalApVoucher apVoucher = apVoucherHome.findByPrimaryKey(AQ_DCMNT_CODE);      		
        		// start of date validation
        		Collection apVoucherLineItems = apVoucher.getApVoucherLineItems();
        		Iterator i = apVoucherLineItems.iterator();
        		
        		while (i.hasNext()) {
        			LocalApVoucherLineItem apVoucherLineItem = (LocalApVoucherLineItem) i.next(); 
        			// start date validation
        			if (adPreference.getPrfArAllowPriorDate() == EJBCommon.FALSE) {	
        				Collection invNegTxnCosting = invCostingHome.findNegTxnByGreaterThanCstDateAndIiNameAndLocName(
        						apVoucher.getVouDate(), apVoucherLineItem.getInvItemLocation().getInvItem().getIiName(),
        						apVoucherLineItem.getInvItemLocation().getInvLocation().getLocName(), AD_BRNCH, AD_CMPNY);
        				if(!invNegTxnCosting.isEmpty()) throw new GlobalInventoryDateException(apVoucherLineItem.getInvItemLocation().getInvItem().getIiName());
        			}
        		}
        		
        		if (isApproved) {   			
        			if (adApprovalQueue.getAqAndOr().equals("AND")) {
        				if (adApprovalQueues.size() == 1) {        					
        					apVoucher.setVouApprovalStatus("APPROVED");
        					apVoucher.setVouApprovedRejectedBy(USR_NM);
        					apVoucher.setVouDateApprovedRejected(EJBCommon.getGcCurrentDateWoTime().getTime());
        					
        					if (adPreference.getPrfApGlPostingType().equals("AUTO-POST UPON APPROVAL")) {
			    		   		this.executeApVouPost(apVoucher.getVouCode(), USR_NM, AD_BRNCH, AD_CMPNY);
			    	    	}
        					adApprovalQueue.remove();
        				} else {
        					// looping up
        					i = adApprovalQueuesDesc.iterator();
            				while (i.hasNext()) {
            					LocalAdApprovalQueue adRemoveApprovalQueue = (LocalAdApprovalQueue)i.next();
            					if (adRemoveApprovalQueue.getAqUserOr() == (byte)1) {
        							i.remove();
                					adRemoveApprovalQueue.remove();
        						} else {
        							break;
        						}	
            				}
            				
            				// looping down
            				if(adApprovalQueue.getAqUserOr() == (byte)1) {
            					boolean first = true;
            					i = adApprovalQueuesAsc.iterator();
                				while (i.hasNext()) {
                					LocalAdApprovalQueue adRemoveApprovalQueue = (LocalAdApprovalQueue)i.next();
                					if (first || adRemoveApprovalQueue.getAqUserOr() == (byte)1) {
            							i.remove();
                    					adRemoveApprovalQueue.remove();
                    					if(first)
                    						first = false;
            						} else {
            							break;
            						}	
                				}
            				}
            				
            				adApprovalQueue.remove();
        					Collection adRemoveApprovalQueues = adApprovalQueueHome.findByAqDocumentAndAqDocumentCode(AQ_DCMNT, AQ_DCMNT_CODE, AD_CMPNY);
            	        	
        					if(adRemoveApprovalQueues.size() == 0) {
            	        		apVoucher.setVouApprovalStatus("APPROVED");
            					apVoucher.setVouApprovedRejectedBy(USR_NM);
            					apVoucher.setVouDateApprovedRejected(EJBCommon.getGcCurrentDateWoTime().getTime());
            					if (adPreference.getPrfApGlPostingType().equals("AUTO-POST UPON APPROVAL")) {
    			    		   		this.executeApVouPost(apVoucher.getVouCode(), USR_NM, AD_BRNCH, AD_CMPNY);
    			    	    	}
            	        	}
        				}       				        				
        			} else if (adApprovalQueue.getAqAndOr().equals("OR")) {
        				apVoucher.setVouApprovalStatus("APPROVED");
        				apVoucher.setVouApprovedRejectedBy(USR_NM);
        				apVoucher.setVouDateApprovedRejected(EJBCommon.getGcCurrentDateWoTime().getTime());
        				
        				i = adApprovalQueues.iterator();
        				while (i.hasNext()) {
        					LocalAdApprovalQueue adRemoveApprovalQueue = (LocalAdApprovalQueue)i.next();
        					i.remove();
        					adRemoveApprovalQueue.remove();
        				}
        				
        				if (adPreference.getPrfApGlPostingType().equals("AUTO-POST UPON APPROVAL")) {
		    		   		this.executeApVouPost(apVoucher.getVouCode(), USR_NM, AD_BRNCH, AD_CMPNY);
		    	    	}        				
        			}       			       			    			      			        			
        		} else if (!isApproved) {
        			
        			apVoucher.setVouApprovalStatus(null);
        			apVoucher.setVouReasonForRejection(RSN_FR_RJCTN);
    				apVoucher.setVouApprovedRejectedBy(USR_NM);
    				apVoucher.setVouDateApprovedRejected(EJBCommon.getGcCurrentDateWoTime().getTime());
    				
    				i = adApprovalQueues.iterator();
    				while (i.hasNext()) {
    					LocalAdApprovalQueue adRemoveApprovalQueue = (LocalAdApprovalQueue)i.next();
    					i.remove();
    					adRemoveApprovalQueue.remove();
    				}        			
        		}
        	}else if (AQ_DCMNT.equals("AP CHECK PAYMENT REQUEST")) {	
        		
        		LocalApVoucher apVoucher = apVoucherHome.findByPrimaryKey(AQ_DCMNT_CODE);     
        		Iterator i = null;
        		if (isApproved) {   			
        			if (adApprovalQueue.getAqAndOr().equals("AND")) {
        				if (adApprovalQueues.size() == 1) {        					
        					apVoucher.setVouApprovalStatus("APPROVED");
        					apVoucher.setVouApprovedRejectedBy(USR_NM);
        					apVoucher.setVouDateApprovedRejected(EJBCommon.getGcCurrentDateWoTime().getTime());
        					apVoucher.setVouPostedBy(USR_NM);
        					apVoucher.setVouDatePosted(EJBCommon.getGcCurrentDateWoTime().getTime());
        					apVoucher.setVouPosted(EJBCommon.TRUE);
        					adApprovalQueue.setAqApproved(EJBCommon.TRUE);
							adApprovalQueue.setAqApprovedDate(new java.util.Date());
        				//	adApprovalQueue.remove();
        				} else {
        					// looping up
        					i = adApprovalQueuesDesc.iterator();
            				while (i.hasNext()) {
            					LocalAdApprovalQueue adRemoveApprovalQueue = (LocalAdApprovalQueue)i.next();
            					if (adRemoveApprovalQueue.getAqUserOr() == (byte)1) {
        							i.remove();
                					adRemoveApprovalQueue.remove();
        						} else {
        							break;
        						}	
            				}
            				
            				// looping down
            				if(adApprovalQueue.getAqUserOr() == (byte)1) {
            					boolean first = true;
            					i = adApprovalQueuesAsc.iterator();
                				while (i.hasNext()) {
                					LocalAdApprovalQueue adRemoveApprovalQueue = (LocalAdApprovalQueue)i.next();
                					if (first || adRemoveApprovalQueue.getAqUserOr() == (byte)1) {
            							i.remove();
                    					adRemoveApprovalQueue.remove();
                    					if(first)
                    						first = false;
            						} else {
            							break;
            						}	
                				}
            				}
            				
            				adApprovalQueue.remove();
        					Collection adRemoveApprovalQueues = adApprovalQueueHome.findByAqDocumentAndAqDocumentCode(AQ_DCMNT, AQ_DCMNT_CODE, AD_CMPNY);
            	        	
        					if(adRemoveApprovalQueues.size() == 0) {
            	        		apVoucher.setVouApprovalStatus("APPROVED");
            					apVoucher.setVouApprovedRejectedBy(USR_NM);
            					apVoucher.setVouDateApprovedRejected(EJBCommon.getGcCurrentDateWoTime().getTime());
            					apVoucher.setVouPostedBy(USR_NM);
            					apVoucher.setVouDatePosted(EJBCommon.getGcCurrentDateWoTime().getTime());
            					apVoucher.setVouPosted(EJBCommon.TRUE);
            	        	}
        				}       				        				
        			} else if (adApprovalQueue.getAqAndOr().equals("OR")) {
        				apVoucher.setVouApprovalStatus("APPROVED");
        				apVoucher.setVouApprovedRejectedBy(USR_NM);
        				apVoucher.setVouDateApprovedRejected(EJBCommon.getGcCurrentDateWoTime().getTime());
        				apVoucher.setVouPostedBy(USR_NM);
    					apVoucher.setVouDatePosted(EJBCommon.getGcCurrentDateWoTime().getTime());
    					apVoucher.setVouPosted(EJBCommon.TRUE);
    					
        				i = adApprovalQueues.iterator();
        				while (i.hasNext()) {
        					LocalAdApprovalQueue adRemoveApprovalQueue = (LocalAdApprovalQueue)i.next();
        					i.remove();
        					//adRemoveApprovalQueue.remove();
        					adRemoveApprovalQueue.setAqApproved(EJBCommon.TRUE);
							adRemoveApprovalQueue.setAqApprovedDate(new java.util.Date());
        				}    				
        			}       			       			    			      			        			
        		} else if (!isApproved) {
        			
        			apVoucher.setVouApprovalStatus(null);
        			apVoucher.setVouReasonForRejection(RSN_FR_RJCTN);
    				apVoucher.setVouApprovedRejectedBy(USR_NM);
    				apVoucher.setVouDateApprovedRejected(EJBCommon.getGcCurrentDateWoTime().getTime());
    				
    				i = adApprovalQueues.iterator();
    				while (i.hasNext()) {
    					LocalAdApprovalQueue adRemoveApprovalQueue = (LocalAdApprovalQueue)i.next();
    					i.remove();
    					adRemoveApprovalQueue.remove();
    				}        			
        		}
        		
        	} else if (AQ_DCMNT.equals("AP PURCHASE ORDER")) {
        		
        		LocalApPurchaseOrder apPurchaseOrder = apPurchaseOrderHome.findByPrimaryKey(AQ_DCMNT_CODE);
        		
        		if (isApproved) {
        			if (adApprovalQueue.getAqAndOr().equals("AND")) {
        				if (adApprovalQueues.size() == 1) {
        					apPurchaseOrder.setPoApprovalStatus("APPROVED");
        					
            				/*LocalAdUser approver=null;
            				try {
            					approver=adUserHome.findByUsrName(USR_NM,AD_CMPNY);
            					approver.setUsrPurchaseOrderApprovalCounter(EJBCommon.incrementStringNumber(approver.getUsrPurchaseOrderApprovalCounter()));
            				}
            				catch (Exception ex) {
    							Debug.printStackTrace(ex);
    						}*/
        					
        					apPurchaseOrder.setPoApprovedRejectedBy(USR_NM);
        					apPurchaseOrder.setPoDateApprovedRejected(EJBCommon.getGcCurrentDateWoTime().getTime());
							apPurchaseOrder.setPoPosted(EJBCommon.TRUE);
        					apPurchaseOrder.setPoPosted(EJBCommon.TRUE);
        					apPurchaseOrder.setPoPostedBy(apPurchaseOrder.getPoLastModifiedBy());
        					apPurchaseOrder.setPoDatePosted(EJBCommon.getGcCurrentDateWoTime().getTime());
        					
        					adApprovalQueue.remove();

        				} else {
        					
        					// looping up
        					Iterator i = adApprovalQueuesDesc.iterator();
            				
            				while (i.hasNext()) {
            					LocalAdApprovalQueue adRemoveApprovalQueue = (LocalAdApprovalQueue)i.next();
            					if (adRemoveApprovalQueue.getAqUserOr() == (byte)1) {
        							i.remove();
                					adRemoveApprovalQueue.remove();
        						} else {
        							break;	
        						}	
            				}
            				
            				// looping down
            				if(adApprovalQueue.getAqUserOr() == (byte)1) {
            					
            					boolean first = true;
            					
            					i = adApprovalQueuesAsc.iterator();
                				
                				while (i.hasNext()) {
                					
                					LocalAdApprovalQueue adRemoveApprovalQueue = (LocalAdApprovalQueue)i.next();
                					
                					if (first || adRemoveApprovalQueue.getAqUserOr() == (byte)1) {
            							
            							i.remove();
                    					adRemoveApprovalQueue.remove();
                    					
                    					if(first)
                    						first = false;
                    					
            						} else {
            							
            							break;
            							
            						}
                						
                				}
                				
            				}
            				
            				adApprovalQueue.remove();
            				
        					Collection adRemoveApprovalQueues = adApprovalQueueHome.findByAqDocumentAndAqDocumentCode(AQ_DCMNT, AQ_DCMNT_CODE, AD_CMPNY);
        					
        					if(adRemoveApprovalQueues.size() == 0) {	
        						apPurchaseOrder.setPoApprovalStatus("APPROVED");
        						
                				LocalAdUser approver=null;
                				try {
                					approver=adUserHome.findByUsrName(USR_NM,AD_CMPNY);
                					approver.setUsrPurchaseOrderApprovalCounter(EJBCommon.incrementStringNumber(approver.getUsrPurchaseOrderApprovalCounter()));
                				}
                				catch (Exception ex) {
        							Debug.printStackTrace(ex);
        						}
        						
        						apPurchaseOrder.setPoApprovedRejectedBy(USR_NM);
        						apPurchaseOrder.setPoDateApprovedRejected(EJBCommon.getGcCurrentDateWoTime().getTime());
        						apPurchaseOrder.setPoPosted(EJBCommon.TRUE);
        						apPurchaseOrder.setPoPostedBy(apPurchaseOrder.getPoLastModifiedBy());
        						apPurchaseOrder.setPoDatePosted(EJBCommon.getGcCurrentDateWoTime().getTime());
        						
        						
        					}
        				}
        			} else if (adApprovalQueue.getAqAndOr().equals("OR")) {
        				
        				apPurchaseOrder.setPoApprovalStatus("APPROVED");
        				
        				LocalAdUser approver=null;
        				try {
        					approver=adUserHome.findByUsrName(USR_NM,AD_CMPNY);
        					approver.setUsrPurchaseOrderApprovalCounter(EJBCommon.incrementStringNumber(approver.getUsrPurchaseOrderApprovalCounter() == null ? "0": approver.getUsrPurchaseOrderApprovalCounter() ));
        				}
        				catch (Exception ex) {
							Debug.printStackTrace(ex);
						}
        				
        				apPurchaseOrder.setPoApprovedRejectedBy(USR_NM);
        				apPurchaseOrder.setPoDateApprovedRejected(EJBCommon.getGcCurrentDateWoTime().getTime());
        				
        				Iterator i = adApprovalQueues.iterator();
        				
        				while (i.hasNext()) {
        					LocalAdApprovalQueue adRemoveApprovalQueue = (LocalAdApprovalQueue)i.next();
        					i.remove();
        					adRemoveApprovalQueue.remove();
        					
        				}
						apPurchaseOrder.setPoPosted(EJBCommon.TRUE);
        				apPurchaseOrder.setPoPosted(EJBCommon.TRUE);
        				apPurchaseOrder.setPoPostedBy(apPurchaseOrder.getPoLastModifiedBy());
        				apPurchaseOrder.setPoDatePosted(EJBCommon.getGcCurrentDateWoTime().getTime());			
						
        				
        			}       
        			
        			if (apPurchaseOrder.getPoPosted() == EJBCommon.TRUE) {
	        			// update transactional budget
        				/*LocalApPurchaseRequisition apPurchaseRequisition = null;
 				       try {
 				    	   System.out.println("apPurchaseOrder.getPoReferenceNumber()="+apPurchaseOrder.getPoReferenceNumber());
 				    	   if(!apPurchaseOrder.getPoReferenceNumber().equals("") ){
 				    		  apPurchaseRequisition = apPurchaseRequisitionHome.findByPrimaryKey(new Integer(apPurchaseOrder.getPoReferenceNumber()));
 			        			Collection apPurchaseOrderLines = apPurchaseOrder.getApPurchaseOrderLines();
 			        			Iterator plIter = apPurchaseOrderLines.iterator();
 			
 								while (plIter.hasNext()) {
 									
 									LocalApPurchaseOrderLine apPurchaseOrderLine = (LocalApPurchaseOrderLine)plIter.next();		
 									this.setInvTbForItemForCurrentMonth(apPurchaseOrderLine.getInvItemLocation().getInvItem().getIiName(), apPurchaseRequisition.getPrCreatedBy(), apPurchaseRequisition.getPrDate(), apPurchaseOrderLine.getPlQuantity(), AD_CMPNY);
 								}
 				    	   }
 				    	   
 				       } catch (FinderException ex) {
 				    	   
 				       }*/
        				
        				
        			}
        			
        			
        			
        		} else if (!isApproved) {
        			apPurchaseOrder.setPoApprovalStatus(null);
        			apPurchaseOrder.setPoReasonForRejection(RSN_FR_RJCTN);
        			apPurchaseOrder.setPoApprovedRejectedBy(USR_NM);
        			apPurchaseOrder.setPoDateApprovedRejected(EJBCommon.getGcCurrentDateWoTime().getTime());
        			Iterator i = adApprovalQueues.iterator();
        			
        			while (i.hasNext()) {
        				LocalAdApprovalQueue adRemoveApprovalQueue = (LocalAdApprovalQueue)i.next();
        				i.remove();
        				adRemoveApprovalQueue.remove();
        			}
        		}  
        	
        	
        	} else if (AQ_DCMNT.equals("AP RECEIVING ITEM")) {
        		
        		LocalApPurchaseOrder apPurchaseOrder = apPurchaseOrderHome.findByPrimaryKey(AQ_DCMNT_CODE);
        		
        		if (isApproved) {
        			if (adApprovalQueue.getAqAndOr().equals("AND")) {
        				if (adApprovalQueues.size() == 1) {
        					apPurchaseOrder.setPoApprovalStatus("APPROVED");
        					apPurchaseOrder.setPoApprovedRejectedBy(USR_NM);
        					apPurchaseOrder.setPoDateApprovedRejected(EJBCommon.getGcCurrentDateWoTime().getTime());
							//apPurchaseOrder.setPoPosted(EJBCommon.TRUE);
        					apPurchaseOrder.setPoPostedBy(apPurchaseOrder.getPoLastModifiedBy());
        					apPurchaseOrder.setPoDatePosted(EJBCommon.getGcCurrentDateWoTime().getTime());
        					adApprovalQueue.setAqApproved(EJBCommon.TRUE);
        					adApprovalQueue.setAqApprovedDate(new java.util.Date());
        					
        					if (adPreference.getPrfApGlPostingType().equals("AUTO-POST UPON APPROVAL") ) {
			    		   		this.executeApPoPost(apPurchaseOrder.getPoCode(), USR_NM, AD_BRNCH, AD_CMPNY);		
			    	    	}
        					
        					//adApprovalQueue.remove();
        				

        				} else {
        					
        					// looping up
        					Iterator i = adApprovalQueuesDesc.iterator();
            				
            				while (i.hasNext()) {
            					LocalAdApprovalQueue adRemoveApprovalQueue = (LocalAdApprovalQueue)i.next();
            					if (adRemoveApprovalQueue.getAqUserOr() == (byte)1) {
        							i.remove();
                				//	adRemoveApprovalQueue.remove();
                					adRemoveApprovalQueue.setAqApproved(EJBCommon.TRUE);
        							adRemoveApprovalQueue.setAqApprovedDate(new java.util.Date());
        						} else {
        							break;	
        						}	
            				}
            				
            				// looping down
            				if(adApprovalQueue.getAqUserOr() == (byte)1) {
            					
            					boolean first = true;
            					
            					i = adApprovalQueuesAsc.iterator();
                				
                				while (i.hasNext()) {
                					
                					LocalAdApprovalQueue adRemoveApprovalQueue = (LocalAdApprovalQueue)i.next();
                					
                					if (first || adRemoveApprovalQueue.getAqUserOr() == (byte)1) {
            							
            							i.remove();
            							
            							adRemoveApprovalQueue.setAqApproved(EJBCommon.TRUE);
        								adRemoveApprovalQueue.setAqApprovedDate(new java.util.Date());
                    				//	adRemoveApprovalQueue.remove();
                    					
                    					if(first)
                    						first = false;
                    					
            						} else {
            							
            							break;
            							
            						}
                						
                				}
                				
            				}
            				
            			//	adApprovalQueue.remove();
            				adApprovalQueue.setAqApproved(EJBCommon.TRUE);
        					adApprovalQueue.setAqApprovedDate(new java.util.Date());
        					Collection adRemoveApprovalQueues = adApprovalQueueHome.findByAqDocumentAndAqDocumentCode(AQ_DCMNT, AQ_DCMNT_CODE, AD_CMPNY);
        					
        					if(adRemoveApprovalQueues.size() == 0) {	
        						apPurchaseOrder.setPoApprovalStatus("APPROVED");
        						/*
                				LocalAdUser approver=null;
                				try {
                					approver=adUserHome.findByUsrName(USR_NM,AD_CMPNY);
                					approver.setUsrPurchaseOrderApprovalCounter(EJBCommon.incrementStringNumber(approver.getUsrPurchaseOrderApprovalCounter()));
                				}
                				catch (Exception ex) {
        							Debug.printStackTrace(ex);
        						}
        						
        						apPurchaseOrder.setPoApprovedRejectedBy(USR_NM);
        						apPurchaseOrder.setPoDateApprovedRejected(EJBCommon.getGcCurrentDateWoTime().getTime());
        						
        						if (adPreference.getPrfApGlPostingType().equals("AUTO-POST UPON APPROVAL") ) {
    			    		   		this.executeApPoPost(apPurchaseOrder.getPoCode(), USR_NM, AD_BRNCH, AD_CMPNY);		
    			    	    	}
    			    	    	*/
    			    	    	apPurchaseOrder.setPoApprovedRejectedBy(USR_NM);
        						apPurchaseOrder.setPoDateApprovedRejected(EJBCommon.getGcCurrentDateWoTime().getTime());
        					
        						apPurchaseOrder.setPoPostedBy(apPurchaseOrder.getPoLastModifiedBy());
        						apPurchaseOrder.setPoDatePosted(EJBCommon.getGcCurrentDateWoTime().getTime());
        						
        							if (adPreference.getPrfApGlPostingType().equals("AUTO-POST UPON APPROVAL") ) {
			    		   		this.executeApPoPost(apPurchaseOrder.getPoCode(), USR_NM, AD_BRNCH, AD_CMPNY);		
			    	    	}
        					
        						
        					}
        				}
        			} else if (adApprovalQueue.getAqAndOr().equals("OR")) {
        				
        				apPurchaseOrder.setPoApprovalStatus("APPROVED");
        			
        				
        				/*
        				
        				LocalAdUser approver=null;
        				try {
        					approver=adUserHome.findByUsrName(USR_NM,AD_CMPNY);
        					approver.setUsrPurchaseOrderApprovalCounter(EJBCommon.incrementStringNumber(approver.getUsrPurchaseOrderApprovalCounter() == null ? "0": approver.getUsrPurchaseOrderApprovalCounter() ));
        				}
        				catch (Exception ex) {
							Debug.printStackTrace(ex);
						}
        				*/
        				apPurchaseOrder.setPoApprovedRejectedBy(USR_NM);
        				apPurchaseOrder.setPoDateApprovedRejected(EJBCommon.getGcCurrentDateWoTime().getTime());
        				
        				Iterator i = adApprovalQueues.iterator();
        				
        				while (i.hasNext()) {
        					LocalAdApprovalQueue adRemoveApprovalQueue = (LocalAdApprovalQueue)i.next();
        					i.remove();
        					adRemoveApprovalQueue.setAqApproved(EJBCommon.TRUE);
        					adRemoveApprovalQueue.setAqApprovedDate(new java.util.Date());
        					//adRemoveApprovalQueue.remove();
        					
        				}
						
					//	apPurchaseOrder.setPoPosted(EJBCommon.TRUE);
        				apPurchaseOrder.setPoPostedBy(apPurchaseOrder.getPoLastModifiedBy());
        				apPurchaseOrder.setPoDatePosted(EJBCommon.getGcCurrentDateWoTime().getTime());
        				
        				
        				if (adPreference.getPrfApGlPostingType().equals("AUTO-POST UPON APPROVAL")) {
		    		   		this.executeApPoPost(apPurchaseOrder.getPoCode(), USR_NM, AD_BRNCH, AD_CMPNY);		
		    	    	}
		    	    	
		    	    	
        			}       
        			
        			
        			
        			
        			
        		} else if (!isApproved) {
        			apPurchaseOrder.setPoApprovalStatus(null);
        			apPurchaseOrder.setPoReasonForRejection(RSN_FR_RJCTN);
        			apPurchaseOrder.setPoApprovedRejectedBy(USR_NM);
        			apPurchaseOrder.setPoDateApprovedRejected(EJBCommon.getGcCurrentDateWoTime().getTime());
        			Iterator i = adAllApprovalQueues.iterator();
        			
        			while (i.hasNext()) {
        				LocalAdApprovalQueue adRemoveApprovalQueue = (LocalAdApprovalQueue)i.next();
        				i.remove();
        				adRemoveApprovalQueue.remove();
        			}
        		}  
        	
        	}else if (AQ_DCMNT.equals("AP CANVASS")) {
        		LocalApPurchaseRequisition apPurchaseRequisition = apPurchaseRequisitionHome.findByPrimaryKey(AQ_DCMNT_CODE);
        		if (isApproved) {
        			if (adApprovalQueue.getAqAndOr().equals("AND")) {
        				if (adApprovalQueues.size() == 1) {
        					apPurchaseRequisition.setPrCanvassApprovalStatus("APPROVED");
        					
            				LocalAdUser approver=null;
            				try {
            					approver=adUserHome.findByUsrName(USR_NM,AD_CMPNY);
            					approver.setUsrPurchaseOrderApprovalCounter(EJBCommon.incrementStringNumber(approver.getUsrPurchaseOrderApprovalCounter()));
            				}
            				catch (Exception ex) {
    							Debug.printStackTrace(ex);
    						}
        					
        					apPurchaseRequisition.setPrCanvassApprovedRejectedBy(USR_NM);
        					apPurchaseRequisition.setPrCanvassDateApprovedRejected(EJBCommon.getGcCurrentDateWoTime().getTime());
        					apPurchaseRequisition.setPrCanvassPosted(EJBCommon.TRUE);
        					apPurchaseRequisition.setPrCanvassPosted(EJBCommon.TRUE);
        					apPurchaseRequisition.setPrCanvassPostedBy(apPurchaseRequisition.getPrLastModifiedBy());
        					apPurchaseRequisition.setPrCanvassDatePosted(EJBCommon.getGcCurrentDateWoTime().getTime());
        					adApprovalQueue.remove();

        				} else {
        					// looping up
        					Iterator i = adApprovalQueuesDesc.iterator();
        					while (i.hasNext()) {
        						LocalAdApprovalQueue adRemoveApprovalQueue = (LocalAdApprovalQueue)i.next();
        						if (adRemoveApprovalQueue.getAqUserOr() == (byte)1) {
        							i.remove();
        							adRemoveApprovalQueue.remove();
        						} else {
        							break;
        						}
        					}

        					// looping down
        					if(adApprovalQueue.getAqUserOr() == (byte)1) {
        						boolean first = true;
        						i = adApprovalQueuesAsc.iterator();
        						while (i.hasNext()) {
        							LocalAdApprovalQueue adRemoveApprovalQueue = (LocalAdApprovalQueue)i.next();
        							if (first || adRemoveApprovalQueue.getAqUserOr() == (byte)1) {
        								i.remove();
        								adRemoveApprovalQueue.remove();
        								if(first)
        									first = false;
        							} else {
        								break;
        							}
        						}
        					}

        					adApprovalQueue.remove();
        					Collection adRemoveApprovalQueues = adApprovalQueueHome.findByAqDocumentAndAqDocumentCode(AQ_DCMNT, AQ_DCMNT_CODE, AD_CMPNY);
        					if(adRemoveApprovalQueues.size() == 0) {	
        						apPurchaseRequisition.setPrCanvassApprovalStatus("APPROVED");
        						
                				LocalAdUser approver=null;
                				try {
                					approver=adUserHome.findByUsrName(USR_NM,AD_CMPNY);
                					approver.setUsrPurchaseOrderApprovalCounter(EJBCommon.incrementStringNumber(approver.getUsrPurchaseOrderApprovalCounter()));
                				}
                				catch (Exception ex) {
        							Debug.printStackTrace(ex);
        						}
        						
        						apPurchaseRequisition.setPrCanvassApprovedRejectedBy(USR_NM);
        						apPurchaseRequisition.setPrCanvassDateApprovedRejected(EJBCommon.getGcCurrentDateWoTime().getTime());
        						apPurchaseRequisition.setPrCanvassPosted(EJBCommon.TRUE);
        						apPurchaseRequisition.setPrCanvassPosted(EJBCommon.TRUE);
        						apPurchaseRequisition.setPrCanvassPostedBy(apPurchaseRequisition.getPrLastModifiedBy());
        						apPurchaseRequisition.setPrCanvassDatePosted(EJBCommon.getGcCurrentDateWoTime().getTime());
        					}
        				}
        			} else if (adApprovalQueue.getAqAndOr().equals("OR")) {
        				
        				apPurchaseRequisition.setPrCanvassApprovalStatus("APPROVED");
        				
        				LocalAdUser approver=null;
        				try {
        					approver=adUserHome.findByUsrName(USR_NM,AD_CMPNY);
        					approver.setUsrPurchaseOrderApprovalCounter(EJBCommon.incrementStringNumber(approver.getUsrPurchaseOrderApprovalCounter()));
        				}
        				catch (Exception ex) {
							Debug.printStackTrace(ex);
						}
        				
        				apPurchaseRequisition.setPrCanvassApprovedRejectedBy(USR_NM);
        				apPurchaseRequisition.setPrCanvassDateApprovedRejected(EJBCommon.getGcCurrentDateWoTime().getTime());
        				
        				Iterator i = adApprovalQueues.iterator();
        				
        				while (i.hasNext()) {
        					LocalAdApprovalQueue adRemoveApprovalQueue = (LocalAdApprovalQueue)i.next();
        					i.remove();
        					adRemoveApprovalQueue.remove();
        					
        				}
        				apPurchaseRequisition.setPrCanvassPosted(EJBCommon.TRUE);
        				apPurchaseRequisition.setPrCanvassPosted(EJBCommon.TRUE);
        				apPurchaseRequisition.setPrCanvassPostedBy(apPurchaseRequisition.getPrLastModifiedBy());
        				apPurchaseRequisition.setPrCanvassDatePosted(EJBCommon.getGcCurrentDateWoTime().getTime());			
        			}       			       			    				
        		} else if (!isApproved) {

        			apPurchaseRequisition.setPrCanvassApprovalStatus(null);
        			
        			//apPurchaseRequisition.setPrReasonForRejection(RSN_FR_RJCTN);
        			apPurchaseRequisition.setPrCanvassReasonForRejection(RSN_FR_RJCTN);
        			apPurchaseRequisition.setPrCanvassApprovedRejectedBy(USR_NM);
        			apPurchaseRequisition.setPrCanvassDateApprovedRejected(EJBCommon.getGcCurrentDateWoTime().getTime());
        			Iterator i = adApprovalQueues.iterator();
        			
        			while (i.hasNext()) {
        				LocalAdApprovalQueue adRemoveApprovalQueue = (LocalAdApprovalQueue)i.next();
        				i.remove();
        				adRemoveApprovalQueue.remove();
        			}
        		}  
        			
        		
        		
        		
        	}else if (AQ_DCMNT.equals("AP PURCHASE REQUISITION")) {
        		
        		LocalApPurchaseRequisition apPurchaseRequisition = apPurchaseRequisitionHome.findByPrimaryKey(AQ_DCMNT_CODE);
        		if (isApproved) {
        			if (adApprovalQueue.getAqAndOr().equals("AND")) {
        				System.out.println("-----------------adApprovalQueues.size()="+adApprovalQueues.size());
        				if (adApprovalQueues.size() == 1) {

        					apPurchaseRequisition.setPrApprovalStatus("APPROVED");
        					apPurchaseRequisition.setPrApprovedRejectedBy(USR_NM);
        					apPurchaseRequisition.setPrDateApprovedRejected(EJBCommon.getGcCurrentDateWoTime().getTime());
        					apPurchaseRequisition.setPrPosted(EJBCommon.TRUE);
        					apPurchaseRequisition.setPrPostedBy(apPurchaseRequisition.getPrLastModifiedBy());
        					apPurchaseRequisition.setPrDatePosted(EJBCommon.getGcCurrentDateWoTime().getTime());
        					//adApprovalQueue.remove();
        					adApprovalQueue.setAqApproved(EJBCommon.TRUE);
        					adApprovalQueue.setAqApprovedDate(new java.util.Date());
        				} else {
        					// looping up
        					Iterator i = adApprovalQueuesDesc.iterator();
        					while (i.hasNext()) {
        						LocalAdApprovalQueue adRemoveApprovalQueue = (LocalAdApprovalQueue)i.next();
        						if (adRemoveApprovalQueue.getAqUserOr() == (byte)1) {
        							i.remove();
        							adApprovalQueue.setAqApproved(EJBCommon.TRUE);
        						} else {
        							break;
        						}
        					}

        					// looping down
        					if(adApprovalQueue.getAqUserOr() == (byte)1) {
        						boolean first = true;
        						i = adApprovalQueuesAsc.iterator();
        						while (i.hasNext()) {
        							LocalAdApprovalQueue adRemoveApprovalQueue = (LocalAdApprovalQueue)i.next();
        							if (first || adRemoveApprovalQueue.getAqUserOr() == (byte)1) {
        								i.remove();
        								adRemoveApprovalQueue.setAqApproved(EJBCommon.TRUE);
        								adRemoveApprovalQueue.setAqApprovedDate(new java.util.Date());
        								if(first)
        									first = false;
        							} else {
        								break;
        							}
        						}
        					}

        					//adApprovalQueue.remove();
        					adApprovalQueue.setAqApproved(EJBCommon.TRUE);
        					adApprovalQueue.setAqApprovedDate(new java.util.Date());
        					
        					Collection adRemoveApprovalQueues = adApprovalQueueHome.findByAqDocumentAndAqDocumentCode(AQ_DCMNT, AQ_DCMNT_CODE, AD_CMPNY);
        					System.out.println("-----------------adRemoveApprovalQueues.size()="+adRemoveApprovalQueues.size());
        					if(adRemoveApprovalQueues.size() == 0) {	
        						apPurchaseRequisition.setPrApprovalStatus("APPROVED");
        						
        						apPurchaseRequisition.setPrApprovedRejectedBy(USR_NM);
        						apPurchaseRequisition.setPrDateApprovedRejected(EJBCommon.getGcCurrentDateWoTime().getTime());
        						apPurchaseRequisition.setPrPosted(EJBCommon.TRUE);
        						apPurchaseRequisition.setPrPostedBy(apPurchaseRequisition.getPrLastModifiedBy());
        						apPurchaseRequisition.setPrDatePosted(EJBCommon.getGcCurrentDateWoTime().getTime());
        					}
        				}
        			} else if (adApprovalQueue.getAqAndOr().equals("OR")) {
        				
        				
        				apPurchaseRequisition.setPrApprovalStatus("APPROVED");
        				apPurchaseRequisition.setPrApprovedRejectedBy(USR_NM);
        				apPurchaseRequisition.setPrDateApprovedRejected(EJBCommon.getGcCurrentDateWoTime().getTime());
        				
        				Iterator i = adApprovalQueues.iterator();
        				
        				while (i.hasNext()) {
        					LocalAdApprovalQueue adRemoveApprovalQueue = (LocalAdApprovalQueue)i.next();
        					i.remove();
        					//adRemoveApprovalQueue.remove();
        					adRemoveApprovalQueue.setAqApproved(EJBCommon.TRUE);
        					adRemoveApprovalQueue.setAqApprovedDate(new java.util.Date());
        					
        				}
        				apPurchaseRequisition.setPrPosted(EJBCommon.TRUE);
        				apPurchaseRequisition.setPrPostedBy(apPurchaseRequisition.getPrLastModifiedBy());
        				apPurchaseRequisition.setPrDatePosted(EJBCommon.getGcCurrentDateWoTime().getTime());			
        			}       			       			    				
        		} else if (!isApproved) {
        			apPurchaseRequisition.setPrApprovalStatus(null);
        			apPurchaseRequisition.setPrReasonForRejection(RSN_FR_RJCTN);
        			apPurchaseRequisition.setPrApprovedRejectedBy(USR_NM);
        			apPurchaseRequisition.setPrDateApprovedRejected(EJBCommon.getGcCurrentDateWoTime().getTime());
        			/*Iterator i = adApprovalQueues.iterator();
        			
        			while (i.hasNext()) {
        				
        				LocalAdApprovalQueue adRemoveApprovalQueue = (LocalAdApprovalQueue)i.next();
        				i.remove();
        				adRemoveApprovalQueue.remove();
        				
        			}*/
        			
        			Iterator x = adAllApprovalQueues.iterator();
        			
        			while (x.hasNext()) {
        				LocalAdApprovalQueue adRemoveAllApprovalQueue = (LocalAdApprovalQueue)x.next();
        				x.remove();
        				adRemoveAllApprovalQueue.remove();
        				
        			}
        			
        			
        		}	

        		
        	} else {
        		LocalApCheck apCheck = apCheckHome.findByPrimaryKey(AQ_DCMNT_CODE);
        		        		    
        		// start of date validation
        		
        		Collection apVoucherLineItems = apCheck.getApVoucherLineItems();
        		Iterator i = apVoucherLineItems.iterator();
        		while (i.hasNext()) {

        			LocalApVoucherLineItem apVoucherLineItem = (LocalApVoucherLineItem) i.next(); 
        			
	                // start date validation
        			if (adPreference.getPrfArAllowPriorDate() == EJBCommon.FALSE) {	
		  	    		Collection invNegTxnCosting = invCostingHome.findNegTxnByGreaterThanCstDateAndIiNameAndLocName(
		  	    			apCheck.getChkDate(), apVoucherLineItem.getInvItemLocation().getInvItem().getIiName(),
		  	    			apVoucherLineItem.getInvItemLocation().getInvLocation().getLocName(), AD_BRNCH, AD_CMPNY);
		  	    		if(!invNegTxnCosting.isEmpty()) throw new GlobalInventoryDateException(apVoucherLineItem.getInvItemLocation().getInvItem().getIiName());
        			}
        			
        		}
        		
        		if (isApproved) {
        			        			
        			if (adApprovalQueue.getAqAndOr().equals("AND")) {
        				
        				if (adApprovalQueues.size() == 1) {
        					if (apCheck.getChkVoid() == EJBCommon.FALSE) {
        						apCheck.setChkApprovalStatus("APPROVED");
        					} else {
        						apCheck.setChkVoidApprovalStatus("APPROVED");
        					}
        					apCheck.setChkApprovedRejectedBy(USR_NM);
        					apCheck.setChkDateApprovedRejected(EJBCommon.getGcCurrentDateWoTime().getTime());
        					if (adPreference.getPrfApGlPostingType().equals("AUTO-POST UPON APPROVAL")) {
			    		   		this.executeApChkPost(apCheck.getChkCode(), USR_NM, AD_BRNCH, AD_CMPNY);		
			    	    	}			
        					adApprovalQueue.remove();			
        				} else {   					
        					// looping up
        					i = adApprovalQueuesDesc.iterator();
            				
            				while (i.hasNext()) {
            					LocalAdApprovalQueue adRemoveApprovalQueue = (LocalAdApprovalQueue)i.next();
            					if (adRemoveApprovalQueue.getAqUserOr() == (byte)1) {
        							i.remove();
                					adRemoveApprovalQueue.remove();
        						} else {
        							break;	
        						}	
            				}
            				
            				// looping down
            				if(adApprovalQueue.getAqUserOr() == (byte)1) {
            					
            					boolean first = true;
            					
            					i = adApprovalQueuesAsc.iterator();
                				
                				while (i.hasNext()) {
                					
                					LocalAdApprovalQueue adRemoveApprovalQueue = (LocalAdApprovalQueue)i.next();
                					
                					if (first || adRemoveApprovalQueue.getAqUserOr() == (byte)1) {
            							
            							i.remove();
                    					adRemoveApprovalQueue.remove();
                    					
                    					if(first)
                    						first = false;
                    					
            						} else {
            							
            							break;
            							
            						}
                						
                				}
                				
            				}
            				
            				adApprovalQueue.remove();
        					
        					Collection adRemoveApprovalQueues = adApprovalQueueHome.findByAqDocumentAndAqDocumentCode(AQ_DCMNT, AQ_DCMNT_CODE, AD_CMPNY);
            	        	
        					if(adRemoveApprovalQueues.size() == 0) {
        						
        						if (apCheck.getChkVoid() == EJBCommon.FALSE) {

        							apCheck.setChkApprovalStatus("APPROVED");

        						} else {

        							apCheck.setChkVoidApprovalStatus("APPROVED");

        						}

        						apCheck.setChkApprovedRejectedBy(USR_NM);
        						apCheck.setChkDateApprovedRejected(EJBCommon.getGcCurrentDateWoTime().getTime());

        						if (adPreference.getPrfApGlPostingType().equals("AUTO-POST UPON APPROVAL")) {

        							this.executeApChkPost(apCheck.getChkCode(), USR_NM, AD_BRNCH, AD_CMPNY);

        						}
        						
        					}
            			
        				}
        				        				        				
        			} else if (adApprovalQueue.getAqAndOr().equals("OR")) {
        				
        				if (apCheck.getChkVoid() == EJBCommon.FALSE) {
        						
    						apCheck.setChkApprovalStatus("APPROVED");
    						
    					} else {
    						
    						apCheck.setChkVoidApprovalStatus("APPROVED");
    						
    					}
    					
        				apCheck.setChkApprovedRejectedBy(USR_NM);
        				apCheck.setChkDateApprovedRejected(EJBCommon.getGcCurrentDateWoTime().getTime());
        				
        				i = adApprovalQueues.iterator();
        				
        				while (i.hasNext()) {
        					
        					LocalAdApprovalQueue adRemoveApprovalQueue = (LocalAdApprovalQueue)i.next();
        					
        					i.remove();
        					adRemoveApprovalQueue.remove();
        					
        				}
        				
        				if (adPreference.getPrfApGlPostingType().equals("AUTO-POST UPON APPROVAL")) {
    		
		    		   		this.executeApChkPost(apCheck.getChkCode(), USR_NM, AD_BRNCH, AD_CMPNY);
		    		
		    	    	}
        				
        			}        			   			
    				        			        			
        		} else if (!isApproved) {
        			
        			if (apCheck.getChkVoid() == EJBCommon.FALSE) {
        						
						apCheck.setChkApprovalStatus(null);
						
					} else {
						
						apCheck.setChkVoidApprovalStatus(null);
						
					}
					
        			apCheck.setChkApprovedRejectedBy(USR_NM);
        			apCheck.setChkReasonForRejection(RSN_FR_RJCTN);
        			apCheck.setChkDateApprovedRejected(EJBCommon.getGcCurrentDateWoTime().getTime());
    				
    				i = adApprovalQueues.iterator();
    				
    				while (i.hasNext()) {
    					
    					LocalAdApprovalQueue adRemoveApprovalQueue = (LocalAdApprovalQueue)i.next();
    					
    					i.remove();
    					adRemoveApprovalQueue.remove();
    					
    				}
        			        			
        		}
        		
        	}        	        	     
        	
        } catch (GlobalRecordAlreadyDeletedException ex) {
        	
        	getSessionContext().setRollbackOnly();
        	throw ex;
        	
        } catch (GlobalTransactionAlreadyPostedException ex) {
        	
        	getSessionContext().setRollbackOnly();
        	throw ex;
        	
        } catch (GlobalTransactionAlreadyVoidException ex) {
        	
        	getSessionContext().setRollbackOnly();
        	throw ex;
        	
        } catch (GlobalTransactionAlreadyVoidPostedException ex) {
        	
        	getSessionContext().setRollbackOnly();
        	throw ex;
        	
        } catch (GlJREffectiveDateNoPeriodExistException ex) {
        	
        	getSessionContext().setRollbackOnly();
        	throw ex;
        	
        } catch (GlJREffectiveDatePeriodClosedException ex) {
        	
        	getSessionContext().setRollbackOnly();
        	throw ex;
        	
        } catch (GlobalJournalNotBalanceException ex) {
        	
        	getSessionContext().setRollbackOnly();
        	throw ex;
        	        	
        } catch (GlobalInventoryDateException ex) {
        	
        	getSessionContext().setRollbackOnly();
        	throw ex;
        	
        } catch (AdPRFCoaGlVarianceAccountNotFoundException ex) {
        	
        	getSessionContext().setRollbackOnly();
        	throw ex;

    	} catch (Exception ex) {
    		
    		Debug.printStackTrace(ex);
    		getSessionContext().setRollbackOnly();
    		throw new EJBException(ex.getMessage());
    		
    	}
    	
    }


   /**
    * @ejb:interface-method view-type="remote"
    * @jboss:method-attributes read-only="true"
    **/
    public short getGlFcPrecisionUnit(Integer AD_CMPNY) {

       Debug.print("ApApprovalControllerBean getGlFcPrecisionUnit");

       	
       LocalAdCompanyHome adCompanyHome = null;
      
     
       // Initialize EJB Home
        
       try {
            
           adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
              lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);       
            
       } catch (NamingException ex) {
            
           throw new EJBException(ex.getMessage());
            
       }

       try {
       	
         LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
            
         return  adCompany.getGlFunctionalCurrency().getFcPrecision();
                                     
       } catch (Exception ex) {
       	 
       	 Debug.printStackTrace(ex);
         throw new EJBException(ex.getMessage());
         
       }

    }
    
    // private methods
    		    
    private double convertForeignToFunctionalCurrency(Integer FC_CODE, String FC_NM, 
	    Date CONVERSION_DATE, double CONVERSION_RATE, double AMOUNT, Integer AD_CMPNY) {
	    	
	    Debug.print("ApApprovalControllerBean convertForeignToFunctionalCurrency");
	    
	    
        LocalGlFunctionalCurrencyRateHome glFunctionalCurrencyRateHome = null;
        LocalAdCompanyHome adCompanyHome = null;
         
        LocalAdCompany adCompany = null;
                 
        // Initialize EJB Homes
         
        try {
         	
            glFunctionalCurrencyRateHome = (LocalGlFunctionalCurrencyRateHome)EJBHomeFactory.
               lookUpLocalHome(LocalGlFunctionalCurrencyRateHome.JNDI_NAME, LocalGlFunctionalCurrencyRateHome.class);                         
            adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
               lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);             
                   
         } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
         }
         
         // get company and extended precision
         
         try {
         	
             adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
         	             
         } catch (Exception ex) {
         	
             throw new EJBException(ex.getMessage());
         	
         }	     
        
         
         // Convert to functional currency if necessary
         
         if (CONVERSION_RATE != 1 && CONVERSION_RATE != 0 ) {
         	         
             AMOUNT = AMOUNT * CONVERSION_RATE;
             	
         } else if (CONVERSION_DATE != null) {
         	
         	 try {
         	 	    
         	 	 // Get functional currency rate
         	 	 
         	     LocalGlFunctionalCurrencyRate glReceiptFunctionalCurrencyRate = null;
         	     
         	     if (!FC_NM.equals("USD")) {
         	     	
        	         glReceiptFunctionalCurrencyRate = 
	     	             glFunctionalCurrencyRateHome.findByFcCodeAndDate(FC_CODE, 
	     	             CONVERSION_DATE, AD_CMPNY);
	     	             
	     	         AMOUNT = AMOUNT * glReceiptFunctionalCurrencyRate.getFrXToUsd();
	     	             
         	     }
                 
                 // Get set of book functional currency rate if necessary
         	 	         
                 if (!adCompany.getGlFunctionalCurrency().getFcName().equals("USD")) {
                 
                     LocalGlFunctionalCurrencyRate glCompanyFunctionalCurrencyRate = 
                         glFunctionalCurrencyRateHome.findByFcCodeAndDate(adCompany.getGlFunctionalCurrency().
                             getFcCode(), CONVERSION_DATE, AD_CMPNY);
                     
                     AMOUNT = AMOUNT / glCompanyFunctionalCurrencyRate.getFrXToUsd();
                 
                  }
                                 
         	             
         	 } catch (Exception ex) {
         	 	
         	 	throw new EJBException(ex.getMessage());
         	 	
         	 }       
         	
         }
         
         return EJBCommon.roundIt(AMOUNT, adCompany.getGlFunctionalCurrency().getFcPrecision());
         
	}
    
    
    private void executeApPoPost(Integer PO_CODE, String USR_NM, Integer AD_BRNCH, Integer AD_CMPNY) throws
    GlobalRecordAlreadyDeletedException,		
	GlobalTransactionAlreadyPostedException,
	GlobalTransactionAlreadyVoidException,
	GlJREffectiveDateNoPeriodExistException,
	GlJREffectiveDatePeriodClosedException,
	GlobalJournalNotBalanceException,
	AdPRFCoaGlVarianceAccountNotFoundException {
                
    Debug.print("ApReceivingItemEntryControllerBean executeApPoPost");
    
    LocalApPurchaseOrderHome apPurchaseOrderHome = null;        
    LocalAdCompanyHome adCompanyHome = null;
    LocalAdPreferenceHome adPreferenceHome = null;
    LocalGlSetOfBookHome glSetOfBookHome = null;
    LocalGlAccountingCalendarValueHome glAccountingCalendarValueHome = null;
    LocalGlJournalHome glJournalHome = null;
    LocalGlJournalBatchHome glJournalBatchHome = null;
    LocalGlSuspenseAccountHome glSuspenseAccountHome = null;
    LocalGlJournalLineHome glJournalLineHome = null;
    LocalGlJournalSourceHome glJournalSourceHome = null;
    LocalGlJournalCategoryHome glJournalCategoryHome = null;
    LocalGlFunctionalCurrencyHome glFunctionalCurrencyHome = null;
    LocalApDistributionRecordHome apDistributionRecordHome = null;
    LocalInvCostingHome invCostingHome = null;
    LocalGlChartOfAccountHome glChartOfAccountHome = null;
		LocalGlForexLedgerHome glForexLedgerHome = null;
		LocalGlFunctionalCurrencyRateHome glFunctionalCurrencyRateHome = null;

    LocalApPurchaseOrder apPurchaseOrder = null;        

    // Initialize EJB Home
    
    try {
        
        apPurchaseOrderHome = (LocalApPurchaseOrderHome)EJBHomeFactory.
            lookUpLocalHome(LocalApPurchaseOrderHome.JNDI_NAME, LocalApPurchaseOrderHome.class);
        adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
            lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
        adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
        	lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);
        glSetOfBookHome = (LocalGlSetOfBookHome)EJBHomeFactory.
        	lookUpLocalHome(LocalGlSetOfBookHome.JNDI_NAME, LocalGlSetOfBookHome.class);
        glAccountingCalendarValueHome = (LocalGlAccountingCalendarValueHome)EJBHomeFactory.
        	lookUpLocalHome(LocalGlAccountingCalendarValueHome.JNDI_NAME, LocalGlAccountingCalendarValueHome.class);
        glJournalHome = (LocalGlJournalHome)EJBHomeFactory.
        	lookUpLocalHome(LocalGlJournalHome.JNDI_NAME, LocalGlJournalHome.class);
        glJournalBatchHome = (LocalGlJournalBatchHome)EJBHomeFactory.
        	lookUpLocalHome(LocalGlJournalBatchHome.JNDI_NAME, LocalGlJournalBatchHome.class);
        glSuspenseAccountHome = (LocalGlSuspenseAccountHome)EJBHomeFactory.
        	lookUpLocalHome(LocalGlSuspenseAccountHome.JNDI_NAME, LocalGlSuspenseAccountHome.class);
        glJournalLineHome = (LocalGlJournalLineHome)EJBHomeFactory.
        	lookUpLocalHome(LocalGlJournalLineHome.JNDI_NAME, LocalGlJournalLineHome.class);
        glJournalSourceHome = (LocalGlJournalSourceHome)EJBHomeFactory.
        	lookUpLocalHome(LocalGlJournalSourceHome.JNDI_NAME, LocalGlJournalSourceHome.class);
        glJournalCategoryHome = (LocalGlJournalCategoryHome)EJBHomeFactory.
        	lookUpLocalHome(LocalGlJournalCategoryHome.JNDI_NAME, LocalGlJournalCategoryHome.class);
        glFunctionalCurrencyHome = (LocalGlFunctionalCurrencyHome)EJBHomeFactory.
        	lookUpLocalHome(LocalGlFunctionalCurrencyHome.JNDI_NAME, LocalGlFunctionalCurrencyHome.class);
        apDistributionRecordHome = (LocalApDistributionRecordHome)EJBHomeFactory.
        	lookUpLocalHome(LocalApDistributionRecordHome.JNDI_NAME, LocalApDistributionRecordHome.class);
        invCostingHome = (LocalInvCostingHome)EJBHomeFactory.
        	lookUpLocalHome(LocalInvCostingHome.JNDI_NAME, LocalInvCostingHome.class);
        glChartOfAccountHome = (LocalGlChartOfAccountHome)EJBHomeFactory.
          	lookUpLocalHome(LocalGlChartOfAccountHome.JNDI_NAME, LocalGlChartOfAccountHome.class);
			glForexLedgerHome  = (LocalGlForexLedgerHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlForexLedgerHome.JNDI_NAME, LocalGlForexLedgerHome.class);
			glFunctionalCurrencyRateHome = (LocalGlFunctionalCurrencyRateHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlFunctionalCurrencyRateHome.JNDI_NAME, LocalGlFunctionalCurrencyRateHome.class);
            
    } catch (NamingException ex) {
        
        throw new EJBException(ex.getMessage());
        
    }       
    
    try {
    	
    	LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);
    	// validate if receiving item/debit memo is already deleted
    	
    	try {
    		        			
    		apPurchaseOrder = apPurchaseOrderHome.findByPrimaryKey(PO_CODE);
    			        		
    	} catch (FinderException ex) {
    		
    		throw new GlobalRecordAlreadyDeletedException();
    		
    	}

    	// validate if receiving item is already posted or void
    	
    	if (apPurchaseOrder.getPoPosted() == EJBCommon.TRUE) {
    		
    		throw new GlobalTransactionAlreadyPostedException();
    		
    	} else if (apPurchaseOrder.getPoVoid() == EJBCommon.TRUE) {
    		
    		throw new GlobalTransactionAlreadyVoidException();
    	}
    	
    	// post receiving item
    	
		Collection apPurchaseOrderLines = apPurchaseOrder.getApPurchaseOrderLines();
		
        Iterator c = apPurchaseOrderLines.iterator();
        
		while(c.hasNext()) {
		
			LocalApPurchaseOrderLine apPurchaseOrderLine = (LocalApPurchaseOrderLine) c.next();
			LocalInvItemLocation invItemLocation = apPurchaseOrderLine.getInvItemLocation();
			
			if (apPurchaseOrderLine.getPlQuantity() == 0) continue;
			
			// start date validation
			if (adPreference.getPrfArAllowPriorDate() == EJBCommon.FALSE) {
				Collection invNegTxnCosting = invCostingHome.findNegTxnByGreaterThanCstDateAndIiNameAndLocName(
					apPurchaseOrder.getPoDate(), invItemLocation.getInvItem().getIiName(),
					invItemLocation.getInvLocation().getLocName(), AD_BRNCH, AD_CMPNY);
				if(!invNegTxnCosting.isEmpty()) throw new GlobalInventoryDateException(invItemLocation.getInvItem().getIiName());
			}
			
			
			String II_NM = apPurchaseOrderLine.getInvItemLocation().getInvItem().getIiName();
			String LOC_NM = apPurchaseOrderLine.getInvItemLocation().getInvLocation().getLocName();
			
			
			double QTY_RCVD = this.convertByUomFromAndItemAndQuantity(apPurchaseOrderLine.getInvUnitOfMeasure(), 
					apPurchaseOrderLine.getInvItemLocation().getInvItem(), apPurchaseOrderLine.getPlQuantity(), apPurchaseOrderLine.getPlConversionFactor(), AD_CMPNY);
			
			LocalInvCosting invCosting = null;
			
			try {
			
				invCosting  = invCostingHome.getByMaxCstDateToLongAndMaxCstLineNumberAndLessThanEqualCstDateAndIiNameAndLocName(apPurchaseOrder.getPoDate(), II_NM, LOC_NM, AD_BRNCH, AD_CMPNY);
				
			} catch (FinderException ex) {
																										
			}
			
			
			double ADDON_COST = apPurchaseOrderLine.getPlAddonCost();
			double COST = this.convertForeignToFunctionalCurrency(apPurchaseOrder.getGlFunctionalCurrency().getFcCode(),
        		    apPurchaseOrder.getGlFunctionalCurrency().getFcName(),
        		    apPurchaseOrder.getPoConversionDate(),
        		    apPurchaseOrder.getPoConversionRate(),
        		    apPurchaseOrderLine.getPlUnitCost(), AD_CMPNY);
        	
			double AMOUNT = (COST * QTY_RCVD) + ADDON_COST;
			
			
			// CREATE COSTING
			if (invCosting == null) {
								
				this.postToInvPo(apPurchaseOrderLine, apPurchaseOrder.getPoDate(), 
						QTY_RCVD, AMOUNT, 
						QTY_RCVD, AMOUNT, 
						0d, null, AD_BRNCH, AD_CMPNY);			
							  				
			} else {

				this.postToInvPo(apPurchaseOrderLine, apPurchaseOrder.getPoDate(), 
						QTY_RCVD, AMOUNT,
						invCosting.getCstRemainingQuantity() + QTY_RCVD,
						invCosting.getCstRemainingValue() + (AMOUNT), 0d, null, AD_BRNCH, AD_CMPNY);
				
														    	
			}
        				
		}
			    	        		        		
    	// set receiving item post status
       
       apPurchaseOrder.setPoPosted(EJBCommon.TRUE);
       apPurchaseOrder.setPoPostedBy(USR_NM);
       apPurchaseOrder.setPoDatePosted(EJBCommon.getGcCurrentDateWoTime().getTime());
       
                  
       // post to gl if necessary
       
       LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
       
   	   // validate if date has no period and period is closed
   	   
   	   LocalGlSetOfBook glJournalSetOfBook = null;
   	  
   	   try {
   	    
   	   	   glJournalSetOfBook = glSetOfBookHome.findByDate(apPurchaseOrder.getPoDate(), AD_CMPNY);
   	   	   
   	   } catch (FinderException ex) {
   	   
   	       throw new GlJREffectiveDateNoPeriodExistException();
   	   
   	   }
	
	   LocalGlAccountingCalendarValue glAccountingCalendarValue = 
	        glAccountingCalendarValueHome.findByAcCodeAndDate(
	        	glJournalSetOfBook.getGlAccountingCalendar().getAcCode(), apPurchaseOrder.getPoDate(), AD_CMPNY);
	        	
	        	
	   if (glAccountingCalendarValue.getAcvStatus() == 'N' ||
	        glAccountingCalendarValue.getAcvStatus() == 'C' ||
	        glAccountingCalendarValue.getAcvStatus() == 'P') {
	        	
	        throw new GlJREffectiveDatePeriodClosedException();
	        
	   }
	   
	   // check if receiving item is balance if not check suspense posting
            	
       LocalGlJournalLine glOffsetJournalLine = null;
    	
       Collection apDistributionRecords = apDistributionRecordHome.findImportableDrByPoCode(apPurchaseOrder.getPoCode(), AD_CMPNY);
        
       Iterator j = apDistributionRecords.iterator();
    	
       double TOTAL_DEBIT = 0d;
       double TOTAL_CREDIT = 0d;
    	
       while (j.hasNext()) {
    		
    		LocalApDistributionRecord apDistributionRecord = (LocalApDistributionRecord)j.next();
    		
    		double DR_AMNT = EJBCommon.roundIt(apDistributionRecord.getDrAmount(), adCompany.getGlFunctionalCurrency().getFcPrecision());;
    		
    		if (apDistributionRecord.getDrDebit() == EJBCommon.TRUE) {
    			
    			TOTAL_DEBIT += DR_AMNT;
    			            			
    		} else {
    			
    			TOTAL_CREDIT += DR_AMNT;
    			
    		}
    		
    	}
    	
    	TOTAL_DEBIT = EJBCommon.roundIt(TOTAL_DEBIT, adCompany.getGlFunctionalCurrency().getFcPrecision());
    	TOTAL_CREDIT = EJBCommon.roundIt(TOTAL_CREDIT, adCompany.getGlFunctionalCurrency().getFcPrecision());
    	            	            	            	
    	if (adPreference.getPrfAllowSuspensePosting() == EJBCommon.TRUE &&
    	    TOTAL_DEBIT != TOTAL_CREDIT) {
    	    	
    	    LocalGlSuspenseAccount glSuspenseAccount = null;
    	    	
    	    try { 	
    	    	
    	        glSuspenseAccount = glSuspenseAccountHome.findByJsNameAndJcName("ACCOUNTS PAYABLES", "RECEIVING ITEM", AD_CMPNY);
    	        
    	    } catch (FinderException ex) {
    	    	
    	    	throw new GlobalJournalNotBalanceException();
    	    	
    	    }
    	               	    
    	    if (TOTAL_DEBIT - TOTAL_CREDIT < 0) {
    	    	
    	    	glOffsetJournalLine = glJournalLineHome.create((short)(apDistributionRecords.size() + 1),
    	    	    EJBCommon.TRUE, TOTAL_CREDIT - TOTAL_DEBIT, "", AD_CMPNY);
    	    	              	        
    	    } else {
    	    	
    	    	glOffsetJournalLine = glJournalLineHome.create((short)(apDistributionRecords.size() + 1),
    	    	    EJBCommon.FALSE, TOTAL_DEBIT - TOTAL_CREDIT, "", AD_CMPNY);
    	    	
    	    }
    	    
    	    LocalGlChartOfAccount glChartOfAccount = glSuspenseAccount.getGlChartOfAccount();
    	    //glChartOfAccount.addGlJournalLine(glOffsetJournalLine);
    	    glOffsetJournalLine.setGlChartOfAccount(glChartOfAccount);
    	    	
		} else if (adPreference.getPrfAllowSuspensePosting() == EJBCommon.FALSE &&
		    TOTAL_DEBIT != TOTAL_CREDIT) {
		    
			throw new GlobalJournalNotBalanceException();		    	
		    	
		}
	   		       			   
	   // create journal entry			            	
            		            	
	   LocalGlJournal glJournal = glJournalHome.create(apPurchaseOrder.getPoReferenceNumber(),
		    apPurchaseOrder.getPoDescription(), apPurchaseOrder.getPoDate(),
		    0.0d, null, apPurchaseOrder.getPoDocumentNumber(), null, 1d, "N/A", null,
		    'N', EJBCommon.TRUE, EJBCommon.FALSE,
		    USR_NM, new Date(),
		    USR_NM, new Date(),
		    null, null,
		    USR_NM, EJBCommon.getGcCurrentDateWoTime().getTime(), 
		    apPurchaseOrder.getApSupplier().getSplTin(), apPurchaseOrder.getApSupplier().getSplName(), EJBCommon.FALSE,
		    null, 
		    AD_BRNCH, AD_CMPNY);
		    
	   LocalGlJournalSource glJournalSource = glJournalSourceHome.findByJsName("ACCOUNTS PAYABLES", AD_CMPNY);
	   glJournal.setGlJournalSource(glJournalSource);
    	
       LocalGlFunctionalCurrency glFunctionalCurrency = glFunctionalCurrencyHome.findByFcName(adCompany.getGlFunctionalCurrency().getFcName(), AD_CMPNY);
       glJournal.setGlFunctionalCurrency(glFunctionalCurrency);
    	
       LocalGlJournalCategory glJournalCategory = glJournalCategoryHome.findByJcName("RECEIVING ITEMS", AD_CMPNY);
       glJournal.setGlJournalCategory(glJournalCategory);
		   	            		            	
       // create journal lines
    	          	
       j = apDistributionRecords.iterator();
    	
       while (j.hasNext()) {
    		
    		LocalApDistributionRecord apDistributionRecord = (LocalApDistributionRecord)j.next();
    		
    		double DR_AMNT = EJBCommon.roundIt(apDistributionRecord.getDrAmount(), adCompany.getGlFunctionalCurrency().getFcPrecision());;
    		            		
    		LocalGlJournalLine glJournalLine = glJournalLineHome.create(apDistributionRecord.getDrLine(),	            			
    			apDistributionRecord.getDrDebit(), DR_AMNT, "", AD_CMPNY);
    			
            glJournalLine.setGlChartOfAccount(apDistributionRecord.getGlChartOfAccount());
    	    glJournalLine.setGlJournal(glJournal);
    	    apDistributionRecord.setDrImported(EJBCommon.TRUE);
    	    
	  	  	// for FOREX revaluation

	  	  	if((apPurchaseOrder.getGlFunctionalCurrency().getFcCode() !=
	  	  			adCompany.getGlFunctionalCurrency().getFcCode()) &&
				glJournalLine.getGlChartOfAccount().getGlFunctionalCurrency() != null &&
	  	  		(glJournalLine.getGlChartOfAccount().getGlFunctionalCurrency().getFcCode().equals(
	  	  			apPurchaseOrder.getGlFunctionalCurrency().getFcCode()))){
	  	  		
	  	  		double CONVERSION_RATE = 1;
	  	  		
	  	  		if (apPurchaseOrder.getPoConversionRate() != 0 && apPurchaseOrder.getPoConversionRate() != 1) {
	  	  			
	  	  			CONVERSION_RATE = apPurchaseOrder.getPoConversionRate();
	  	  			
	  	  		} else if (apPurchaseOrder.getPoConversionDate() != null){
	  	  			
	  	  			CONVERSION_RATE = this.getFrRateByFrNameAndFrDate(
	  	  					glJournalLine.getGlChartOfAccount().getGlFunctionalCurrency().getFcName(), 
							glJournal.getJrConversionDate(), AD_CMPNY);
	  	  			
	  	  		}
	  	  		
	  	  		Collection glForexLedgers = null;
	  	  		
	  	  		try {
	  	  			
	  	  			glForexLedgers = glForexLedgerHome.findLatestGlFrlByFrlDateAndByCoaCode(
	  	  				apPurchaseOrder.getPoDate(), glJournalLine.getGlChartOfAccount().getCoaCode(), AD_CMPNY);
	  	  			
	  	  		} catch(FinderException ex) {
	  	  			
	  	  		}
	  	  		
	  	  		LocalGlForexLedger glForexLedger =
	  	  			(glForexLedgers.isEmpty() || glForexLedgers == null) ? null :
	  	  				(LocalGlForexLedger) glForexLedgers.iterator().next();
	  	  		
	  	  		int FRL_LN = (glForexLedger != null &&
	  	  				glForexLedger.getFrlDate().compareTo(apPurchaseOrder.getPoDate()) == 0) ?
	  	  						glForexLedger.getFrlLine().intValue() + 1 : 1;
	  	  		
	  	  		// compute balance
	  	  		double COA_FRX_BLNC = glForexLedger == null ? 0 : glForexLedger.getFrlBalance();
	  	  		double FRL_AMNT = apDistributionRecord.getDrAmount();
	  	  		
	  	  		if(glJournalLine.getGlChartOfAccount().getCoaAccountType().equalsIgnoreCase("ASSET"))
	  	  			FRL_AMNT = (glJournalLine.getJlDebit() == EJBCommon.TRUE ? FRL_AMNT : (- 1 * FRL_AMNT));  
	  	  		else
	  	  			FRL_AMNT = (glJournalLine.getJlDebit() == EJBCommon.TRUE ? (- 1 * FRL_AMNT) : FRL_AMNT);
	  	  		
	  	  		COA_FRX_BLNC = COA_FRX_BLNC + FRL_AMNT;
	  	  		
	  	  		glForexLedger = glForexLedgerHome.create(apPurchaseOrder.getPoDate(), new Integer (FRL_LN),
	  	  			"OTH", FRL_AMNT, CONVERSION_RATE, COA_FRX_BLNC, 0d, AD_CMPNY);
	  	  		
	  	  		//glJournalLine.getGlChartOfAccount().addGlForexLedger(glForexLedger);
	  	  		glForexLedger.setGlChartOfAccount(glJournalLine.getGlChartOfAccount());
	  	  		
	  	  		// propagate balances
	  	  		try{
	  	  			
	  	  			glForexLedgers = glForexLedgerHome.findByGreaterThanFrlDateAndCoaCode(
	  	  				glForexLedger.getFrlDate(), glForexLedger.getGlChartOfAccount().getCoaCode(),
						glForexLedger.getFrlAdCompany());
	  	  			
	  	  		} catch (FinderException ex) {
	  	  			
	  	  		}
	  	  		
	  	  		Iterator itrFrl = glForexLedgers.iterator();
	  	  		
	  	  		while (itrFrl.hasNext()) {
	  	  			
	  	  			glForexLedger = (LocalGlForexLedger) itrFrl.next();
	  	  			FRL_AMNT = apDistributionRecord.getDrAmount();
	  	  			
	  	  			if(glJournalLine.getGlChartOfAccount().getCoaAccountType().equalsIgnoreCase("ASSET"))
	  	  				FRL_AMNT = (glJournalLine.getJlDebit() == EJBCommon.TRUE ? FRL_AMNT : (- 1 * FRL_AMNT));
	  	  			else
	  	  				FRL_AMNT = (glJournalLine.getJlDebit() == EJBCommon.TRUE ? (- 1 * FRL_AMNT) : FRL_AMNT);
	  	  			
	  	  			glForexLedger.setFrlBalance(glForexLedger.getFrlBalance() + FRL_AMNT);
	  	  			
	  	  		}
	  	  		
	  	  	}        	    

       }
    	
       if (glOffsetJournalLine != null) {

    	   glOffsetJournalLine.setGlJournal(glJournal);
    		
       }		
       
       // post journal to gl
       
       Collection glJournalLines = glJournal.getGlJournalLines();
   
       Iterator i = glJournalLines.iterator();
       
       while (i.hasNext()) {
       	
           LocalGlJournalLine glJournalLine = (LocalGlJournalLine)i.next();	
           		           
           // post current to current acv
             
           this.postToGl(glAccountingCalendarValue,
               glJournalLine.getGlChartOfAccount(),
               true, glJournalLine.getJlDebit(), glJournalLine.getJlAmount(), AD_CMPNY);
	 	 
         
           // post to subsequent acvs (propagate)
         
           Collection glSubsequentAccountingCalendarValues = 
               glAccountingCalendarValueHome.findSubsequentAcvByAcCodeAndAcvPeriodNumber( 
                   glJournalSetOfBook.getGlAccountingCalendar().getAcCode(),
                   glAccountingCalendarValue.getAcvPeriodNumber(), AD_CMPNY);     
                 
           Iterator acvsIter = glSubsequentAccountingCalendarValues.iterator();
         
           while (acvsIter.hasNext()) {
         	
         	   LocalGlAccountingCalendarValue glSubsequentAccountingCalendarValue = 
         	       (LocalGlAccountingCalendarValue)acvsIter.next();
         	       
         	   this.postToGl(glSubsequentAccountingCalendarValue,
         	       glJournalLine.getGlChartOfAccount(),
         	       false, glJournalLine.getJlDebit(), glJournalLine.getJlAmount(), AD_CMPNY);
         		         	
           }
           
           // post to subsequent years if necessary
       
           Collection glSubsequentSetOfBooks = glSetOfBookHome.findSubsequentSobByAcYear(glJournalSetOfBook.getGlAccountingCalendar().getAcYear(), AD_CMPNY);
	  	  	
	  	  	if (!glSubsequentSetOfBooks.isEmpty() && glJournalSetOfBook.getSobYearEndClosed() == 1) {
	  	  	
		  	  	adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);		  	  	
		  	  	LocalGlChartOfAccount glRetainedEarningsAccount = glChartOfAccountHome.findByCoaAccountNumber(adCompany.getCmpRetainedEarnings(), AD_CMPNY); 	  	
	  	  	
		  	  	Iterator sobIter = glSubsequentSetOfBooks.iterator();
		  	  	
		  	  	while (sobIter.hasNext()) {
		  	  		
		  	  		LocalGlSetOfBook glSubsequentSetOfBook = (LocalGlSetOfBook)sobIter.next();
		  	  		
		  	  		String ACCOUNT_TYPE = glJournalLine.getGlChartOfAccount().getCoaAccountType();
		  	  		
		  	  		// post to subsequent acvs of subsequent set of book(propagate)
         
		           Collection glAccountingCalendarValues = 
		               glAccountingCalendarValueHome.findByAcCode(glSubsequentSetOfBook.getGlAccountingCalendar().getAcCode(), AD_CMPNY);     
		                 
		           Iterator acvIter = glAccountingCalendarValues.iterator();
		         
		           while (acvIter.hasNext()) {
		         	
		         	   LocalGlAccountingCalendarValue glSubsequentAccountingCalendarValue = 
		         	       (LocalGlAccountingCalendarValue)acvIter.next();
		         	       
		         	    if (ACCOUNT_TYPE.equals("ASSET") || ACCOUNT_TYPE.equals("LIABILITY") ||
			 				ACCOUNT_TYPE.equals("OWNERS EQUITY")) {
		         	       
				         	this.postToGl(glSubsequentAccountingCalendarValue,
				         	     glJournalLine.getGlChartOfAccount(),
				         	     false, glJournalLine.getJlDebit(), glJournalLine.getJlAmount(), AD_CMPNY);
				         	     
				        } else { // revenue & expense
				        					             					             
				             this.postToGl(glSubsequentAccountingCalendarValue,
				         	     glRetainedEarningsAccount,
				         	     false, glJournalLine.getJlDebit(), glJournalLine.getJlAmount(), AD_CMPNY);					        
				        
				        }
		         		         	
		           }
		  	  		
		  	  	   if (glSubsequentSetOfBook.getSobYearEndClosed() == 0) break;
		  	  		
		  	  	}
		  	  	
	  	  	}
       	   	       	   	       	
       }	   			   
       	           	           	                      
    } catch (GlJREffectiveDateNoPeriodExistException ex) {
    	
    	getSessionContext().setRollbackOnly();
    	throw ex;
    	
    } catch (GlJREffectiveDatePeriodClosedException ex) {
    	
    	getSessionContext().setRollbackOnly();
    	throw ex;
    	
    } catch (GlobalJournalNotBalanceException ex) {
    	
    	getSessionContext().setRollbackOnly();
    	throw ex;
    	
    } catch (GlobalRecordAlreadyDeletedException ex) {
    	
    	getSessionContext().setRollbackOnly();
    	throw ex;
    	        	
    } catch (GlobalTransactionAlreadyPostedException ex) {
    	
    	getSessionContext().setRollbackOnly();
    	throw ex;
    	
    } catch (GlobalTransactionAlreadyVoidException ex) {
    	
    	getSessionContext().setRollbackOnly();
    	throw ex;
    	
    } catch (AdPRFCoaGlVarianceAccountNotFoundException ex) {
    	
    	getSessionContext().setRollbackOnly();
    	throw ex;
    	
	} catch (Exception ex) {
		
		Debug.printStackTrace(ex);
		getSessionContext().setRollbackOnly();
		throw new EJBException(ex.getMessage());
		
	}
	
}
	
	private void executeApVouPost(Integer VOU_CODE, String USR_NM, Integer AD_BRNCH, Integer AD_CMPNY) throws
        GlobalRecordAlreadyDeletedException,		
		GlobalTransactionAlreadyPostedException,
		GlobalTransactionAlreadyVoidException,
		GlJREffectiveDateNoPeriodExistException,
		GlJREffectiveDatePeriodClosedException,
		GlobalJournalNotBalanceException,
		AdPRFCoaGlVarianceAccountNotFoundException {
        
        Debug.print("ApApprovalControllerBean executeApVouPost");
        
        LocalApVoucherHome apVoucherHome = null;        
        LocalAdCompanyHome adCompanyHome = null;
        LocalAdPreferenceHome adPreferenceHome = null;
        LocalGlSetOfBookHome glSetOfBookHome = null;
        LocalGlAccountingCalendarValueHome glAccountingCalendarValueHome = null;
        LocalGlJournalHome glJournalHome = null;
        LocalGlJournalBatchHome glJournalBatchHome = null;
        LocalGlSuspenseAccountHome glSuspenseAccountHome = null;
        LocalGlJournalLineHome glJournalLineHome = null;
        LocalGlJournalSourceHome glJournalSourceHome = null;
        LocalGlJournalCategoryHome glJournalCategoryHome = null;
        LocalGlFunctionalCurrencyHome glFunctionalCurrencyHome = null;
        LocalApDistributionRecordHome apDistributionRecordHome = null;
        LocalInvCostingHome invCostingHome = null;
        LocalGlChartOfAccountHome glChartOfAccountHome = null;
 		LocalGlForexLedgerHome glForexLedgerHome = null;
 		LocalGlFunctionalCurrencyRateHome glFunctionalCurrencyRateHome = null;        
                        
        LocalApVoucher apVoucher = null;        
        LocalApVoucher apDebitedVoucher = null;
                
        // Initialize EJB Home
        
        try {
            
            apVoucherHome = (LocalApVoucherHome)EJBHomeFactory.
                lookUpLocalHome(LocalApVoucherHome.JNDI_NAME, LocalApVoucherHome.class);
            adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
            adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
            	lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);
            glSetOfBookHome = (LocalGlSetOfBookHome)EJBHomeFactory.
            	lookUpLocalHome(LocalGlSetOfBookHome.JNDI_NAME, LocalGlSetOfBookHome.class);
            glAccountingCalendarValueHome = (LocalGlAccountingCalendarValueHome)EJBHomeFactory.
            	lookUpLocalHome(LocalGlAccountingCalendarValueHome.JNDI_NAME, LocalGlAccountingCalendarValueHome.class);
            glJournalHome = (LocalGlJournalHome)EJBHomeFactory.
            	lookUpLocalHome(LocalGlJournalHome.JNDI_NAME, LocalGlJournalHome.class);
            glJournalBatchHome = (LocalGlJournalBatchHome)EJBHomeFactory.
            	lookUpLocalHome(LocalGlJournalBatchHome.JNDI_NAME, LocalGlJournalBatchHome.class);
            glSuspenseAccountHome = (LocalGlSuspenseAccountHome)EJBHomeFactory.
            	lookUpLocalHome(LocalGlSuspenseAccountHome.JNDI_NAME, LocalGlSuspenseAccountHome.class);
            glJournalLineHome = (LocalGlJournalLineHome)EJBHomeFactory.
            	lookUpLocalHome(LocalGlJournalLineHome.JNDI_NAME, LocalGlJournalLineHome.class);
            glJournalSourceHome = (LocalGlJournalSourceHome)EJBHomeFactory.
            	lookUpLocalHome(LocalGlJournalSourceHome.JNDI_NAME, LocalGlJournalSourceHome.class);
            glJournalCategoryHome = (LocalGlJournalCategoryHome)EJBHomeFactory.
            	lookUpLocalHome(LocalGlJournalCategoryHome.JNDI_NAME, LocalGlJournalCategoryHome.class);
            glFunctionalCurrencyHome = (LocalGlFunctionalCurrencyHome)EJBHomeFactory.
            	lookUpLocalHome(LocalGlFunctionalCurrencyHome.JNDI_NAME, LocalGlFunctionalCurrencyHome.class);
            apDistributionRecordHome = (LocalApDistributionRecordHome)EJBHomeFactory.
            	lookUpLocalHome(LocalApDistributionRecordHome.JNDI_NAME, LocalApDistributionRecordHome.class);
            invCostingHome = (LocalInvCostingHome)EJBHomeFactory.
            	lookUpLocalHome(LocalInvCostingHome.JNDI_NAME, LocalInvCostingHome.class);
            glChartOfAccountHome = (LocalGlChartOfAccountHome)EJBHomeFactory.
              	lookUpLocalHome(LocalGlChartOfAccountHome.JNDI_NAME, LocalGlChartOfAccountHome.class);
 			glForexLedgerHome  = (LocalGlForexLedgerHome)EJBHomeFactory.
				lookUpLocalHome(LocalGlForexLedgerHome.JNDI_NAME, LocalGlForexLedgerHome.class);
 			glFunctionalCurrencyRateHome = (LocalGlFunctionalCurrencyRateHome)EJBHomeFactory.
				lookUpLocalHome(LocalGlFunctionalCurrencyRateHome.JNDI_NAME, LocalGlFunctionalCurrencyRateHome.class);
                                                
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }       
        
        try {

        	// validate if voucher/debit memo is already deleted
        	
        	try {
        		
        		apVoucher = apVoucherHome.findByPrimaryKey(VOU_CODE);
        		
        	} catch (FinderException ex) {
        		
        		throw new GlobalRecordAlreadyDeletedException();
        		
        	}
        	
        	// validate if voucher/debit memo is already posted or void
        	
        	if (apVoucher.getVouPosted() == EJBCommon.TRUE) {
        		
        		throw new GlobalTransactionAlreadyPostedException();
        		
        	} else if (apVoucher.getVouVoid() == EJBCommon.TRUE) {
        		
        		throw new GlobalTransactionAlreadyVoidException();
        	}
        	
        	// post voucher/debit memo
        	LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);
        	if (apVoucher.getVouDebitMemo() == EJBCommon.FALSE) {

				// increase supplier balance

				double VOU_AMNT = this.convertForeignToFunctionalCurrency(apVoucher.getGlFunctionalCurrency().getFcCode(),
						apVoucher.getGlFunctionalCurrency().getFcName(),
						apVoucher.getVouConversionDate(), apVoucher.getVouConversionRate(),
						apVoucher.getVouAmountDue(), AD_CMPNY);

				this.post(apVoucher.getVouDate(), VOU_AMNT, apVoucher.getApSupplier(), AD_CMPNY);  

				Collection apVoucherLineItems = apVoucher.getApVoucherLineItems();

				if (apVoucherLineItems != null && !apVoucherLineItems.isEmpty()) {

					Iterator c = apVoucherLineItems.iterator();

					while(c.hasNext()) {

						LocalApVoucherLineItem apVoucherLineItem = (LocalApVoucherLineItem) c.next();

						String II_NM = apVoucherLineItem.getInvItemLocation().getInvItem().getIiName();
						String LOC_NM = apVoucherLineItem.getInvItemLocation().getInvLocation().getLocName();

						double QTY_RCVD = this.convertByUomFromAndItemAndQuantity(apVoucherLineItem.getInvUnitOfMeasure(), 
								apVoucherLineItem.getInvItemLocation().getInvItem(), apVoucherLineItem.getVliQuantity(), AD_CMPNY);

						LocalInvCosting invCosting = null;

						try {

							invCosting  = invCostingHome.getByMaxCstDateToLongAndMaxCstLineNumberAndLessThanEqualCstDateAndIiNameAndLocName(apVoucher.getVouDate(), II_NM, LOC_NM, AD_BRNCH, AD_CMPNY);

						} catch (FinderException ex) {

						}

						double COST = apVoucherLineItem.getVliUnitCost();
						// CREATE COSTING
						if (invCosting == null) {

							this.postToInvVou(apVoucherLineItem, apVoucher.getVouDate(), 
									QTY_RCVD, COST * QTY_RCVD, 
									QTY_RCVD, (QTY_RCVD * COST), 0d, null, AD_BRNCH, AD_CMPNY);			

						} else {
							
							this.postToInvVou(apVoucherLineItem, apVoucher.getVouDate(), 
									QTY_RCVD, COST * QTY_RCVD,
									invCosting.getCstRemainingQuantity() + QTY_RCVD,
									invCosting.getCstRemainingValue() + (QTY_RCVD * COST), 0d, null, AD_BRNCH, AD_CMPNY);
							

							
						}
						
						
						
						// POST TO PROJECTING
						
						if(apVoucherLineItem.getInvItemLocation().getInvItem().getIiIsVatRelief() == EJBCommon.TRUE
								&& apVoucherLineItem.getPmProject() != null
								) {
							
							this.postToPjt(apVoucherLineItem, apVoucher.getVouDate(), COST * QTY_RCVD, null, AD_BRNCH, AD_CMPNY);
							
						}
						
						

					}

				}


        		
        	} else {
        		
        		// get debited voucher
        		
        		apDebitedVoucher = apVoucherHome.findByVouDocumentNumberAndVouDebitMemoAndBrCode(
        				apVoucher.getVouDmVoucherNumber(), EJBCommon.FALSE, AD_BRNCH, AD_CMPNY);
        		
        		// decrease supplier balance
        		
        		double VOU_AMNT = this.convertForeignToFunctionalCurrency(apDebitedVoucher.getGlFunctionalCurrency().getFcCode(),
        				apDebitedVoucher.getGlFunctionalCurrency().getFcName(),
    					apDebitedVoucher.getVouConversionDate(), apDebitedVoucher.getVouConversionRate(),
    					apVoucher.getVouBillAmount(), AD_CMPNY) * -1;
        		
        		this.post(apVoucher.getVouDate(), VOU_AMNT, apVoucher.getApSupplier(), AD_CMPNY);
        		
        		// decrease voucher and vps amounts and release lock        		
        		
        		double DEBIT_PERCENT = EJBCommon.roundIt(apVoucher.getVouBillAmount() / apDebitedVoucher.getVouAmountDue(), (short)6);
        		
        		apDebitedVoucher.setVouAmountPaid(apDebitedVoucher.getVouAmountPaid() + apVoucher.getVouBillAmount());
        		
        		double TOTAL_VOUCHER_PAYMENT_SCHEDULE =  0d;
        		
        		Collection apVoucherPaymentSchedules = apDebitedVoucher.getApVoucherPaymentSchedules();
        		
        		Iterator i = apVoucherPaymentSchedules.iterator();
        		
        		while (i.hasNext()) {
        			
        			LocalApVoucherPaymentSchedule apVoucherPaymentSchedule = 
        				(LocalApVoucherPaymentSchedule)i.next();
        			
        			double VOUCHER_PAYMENT_SCHEDULE_AMOUNT = 0;
        			
        			// if last payment schedule subtract to avoid rounding difference error
        			
        			if (i.hasNext()) {
        				
        				VOUCHER_PAYMENT_SCHEDULE_AMOUNT = EJBCommon.roundIt(apVoucherPaymentSchedule.getVpsAmountDue() * DEBIT_PERCENT, this.getGlFcPrecisionUnit(AD_CMPNY));
        				
        			} else {
        				
        				VOUCHER_PAYMENT_SCHEDULE_AMOUNT = apVoucher.getVouBillAmount() - TOTAL_VOUCHER_PAYMENT_SCHEDULE;
        				
        			}
        			
        			apVoucherPaymentSchedule.setVpsAmountPaid(apVoucherPaymentSchedule.getVpsAmountPaid() + VOUCHER_PAYMENT_SCHEDULE_AMOUNT);
        			
        			apVoucherPaymentSchedule.setVpsLock(EJBCommon.FALSE);
        			
        			TOTAL_VOUCHER_PAYMENT_SCHEDULE += VOUCHER_PAYMENT_SCHEDULE_AMOUNT;
        			
        		}        			      
        		
        		Collection apVoucherLineItems = apVoucher.getApVoucherLineItems();
        		
        		if (apVoucherLineItems != null && !apVoucherLineItems.isEmpty()) {
        			
        			Iterator c = apVoucherLineItems.iterator();
        			
        			while(c.hasNext()) {
        				
        				LocalApVoucherLineItem apVoucherLineItem = (LocalApVoucherLineItem) c.next();
        				
        				String II_NM = apVoucherLineItem.getInvItemLocation().getInvItem().getIiName();
        				String LOC_NM = apVoucherLineItem.getInvItemLocation().getInvLocation().getLocName();
        				
        				double QTY_RCVD = this.convertByUomFromAndItemAndQuantity(apVoucherLineItem.getInvUnitOfMeasure(), 
        						apVoucherLineItem.getInvItemLocation().getInvItem(), apVoucherLineItem.getVliQuantity(), AD_CMPNY);
        				
        				LocalInvCosting invCosting = null;
        				// CREATE COSTING
        				try {
        					
        					invCosting  = invCostingHome.getByMaxCstDateToLongAndMaxCstLineNumberAndLessThanEqualCstDateAndIiNameAndLocName(apVoucher.getVouDate(), II_NM, LOC_NM, AD_BRNCH, AD_CMPNY);
        					
        				} catch (FinderException ex) {
        					
        				}
        				
        				double COST = apVoucherLineItem.getVliUnitCost();
        				
        				if (invCosting == null) {
        					
        					this.postToInvVou(apVoucherLineItem, apVoucher.getVouDate(), 
        							-QTY_RCVD, -COST * QTY_RCVD, 
        							-QTY_RCVD, - COST * QTY_RCVD, 
        							0d, null, AD_BRNCH, AD_CMPNY);			
        					
        				} else {

        					this.postToInvVou(apVoucherLineItem, apVoucher.getVouDate(), 
        							-QTY_RCVD, -COST * QTY_RCVD,
    								invCosting.getCstRemainingQuantity() - QTY_RCVD, invCosting.getCstRemainingValue() - (QTY_RCVD * COST), 0d, null, AD_BRNCH, AD_CMPNY);
    					
        				}
        				
        			}
        			
        		}
        	}
        	
        	// set voucher post status
        	
        	apVoucher.setVouPosted(EJBCommon.TRUE);
        	apVoucher.setVouPostedBy(USR_NM);
        	apVoucher.setVouDatePosted(EJBCommon.getGcCurrentDateWoTime().getTime());
        	
        	
        	// post to gl if necessary
        	
        	
        	LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
        	
        	if (adPreference.getPrfApGlPostingType().equals("AUTO-POST UPON APPROVAL")) {
        		
        		// validate if date has no period and period is closed
        		
        		LocalGlSetOfBook glJournalSetOfBook = null;
        		
        		try {
        			
        			glJournalSetOfBook = glSetOfBookHome.findByDate(apVoucher.getVouDate(), AD_CMPNY);
        			
        		} catch (FinderException ex) {
        			
        			throw new GlJREffectiveDateNoPeriodExistException();
        			
        		}
        		
        		LocalGlAccountingCalendarValue glAccountingCalendarValue = 
        			glAccountingCalendarValueHome.findByAcCodeAndDate(
        					glJournalSetOfBook.getGlAccountingCalendar().getAcCode(), apVoucher.getVouDate(), AD_CMPNY);
        		
        		
        		if (glAccountingCalendarValue.getAcvStatus() == 'N' ||
        				glAccountingCalendarValue.getAcvStatus() == 'C' ||
						glAccountingCalendarValue.getAcvStatus() == 'P') {
        			
        			throw new GlJREffectiveDatePeriodClosedException();
        			
        		}
        		
        		// check if voucher is balance if not check suspense posting
        		
        		LocalGlJournalLine glOffsetJournalLine = null;
        		
        		Collection apDistributionRecords = apDistributionRecordHome.findImportableDrByVouCode(apVoucher.getVouCode(), AD_CMPNY);
        		
        		Iterator j = apDistributionRecords.iterator();
        		
        		double TOTAL_DEBIT = 0d;
        		double TOTAL_CREDIT = 0d;
        		
        		while (j.hasNext()) {
        			
        			LocalApDistributionRecord apDistributionRecord = (LocalApDistributionRecord)j.next();
        			
        			double DR_AMNT = 0d;
        			
        			if (apVoucher.getVouDebitMemo() == EJBCommon.FALSE) {
        				
        				DR_AMNT = this.convertForeignToFunctionalCurrency(apVoucher.getGlFunctionalCurrency().getFcCode(),
        						apVoucher.getGlFunctionalCurrency().getFcName(), 
								apVoucher.getVouConversionDate(),
								apVoucher.getVouConversionRate(),
								apDistributionRecord.getDrAmount(), AD_CMPNY);
        				
        			} else {
        				
        				DR_AMNT = this.convertForeignToFunctionalCurrency(apDebitedVoucher.getGlFunctionalCurrency().getFcCode(),
        						apDebitedVoucher.getGlFunctionalCurrency().getFcName(), 
								apDebitedVoucher.getVouConversionDate(),
								apDebitedVoucher.getVouConversionRate(),
								apDistributionRecord.getDrAmount(), AD_CMPNY);
        				
        			}	        		
        			
        			if (apDistributionRecord.getDrDebit() == EJBCommon.TRUE) {
        				
        				TOTAL_DEBIT += DR_AMNT;
        				
        			} else {
        				
        				TOTAL_CREDIT += DR_AMNT;
        				
        			}
        			
        		}
        		
        		TOTAL_DEBIT = EJBCommon.roundIt(TOTAL_DEBIT, adCompany.getGlFunctionalCurrency().getFcPrecision());
        		TOTAL_CREDIT = EJBCommon.roundIt(TOTAL_CREDIT, adCompany.getGlFunctionalCurrency().getFcPrecision());
        		
        		if (adPreference.getPrfAllowSuspensePosting() == EJBCommon.TRUE &&
        				TOTAL_DEBIT != TOTAL_CREDIT) {
        			
        			LocalGlSuspenseAccount glSuspenseAccount = null;
        			
        			try { 	
        				
        				glSuspenseAccount = glSuspenseAccountHome.findByJsNameAndJcName("ACCOUNTS PAYABLES", apVoucher.getVouDebitMemo() == EJBCommon.FALSE ? "VOUCHERS" : "DEBIT MEMOS", AD_CMPNY);
        				
        			} catch (FinderException ex) {
        				
        				throw new GlobalJournalNotBalanceException();
        				
        			}
        			
        			if (TOTAL_DEBIT - TOTAL_CREDIT < 0) {
        				
        				glOffsetJournalLine = glJournalLineHome.create(
        						(short)(apDistributionRecords.size() + 1),
								EJBCommon.TRUE,
								TOTAL_CREDIT - TOTAL_DEBIT, "", AD_CMPNY);
        				
        			} else {
        				
        				glOffsetJournalLine = glJournalLineHome.create(
        						(short)(apDistributionRecords.size() + 1),
								EJBCommon.FALSE,
								TOTAL_DEBIT - TOTAL_CREDIT, "", AD_CMPNY);
        				
        			}
        			
        			LocalGlChartOfAccount glChartOfAccount = glSuspenseAccount.getGlChartOfAccount();
        			//glChartOfAccount.addGlJournalLine(glOffsetJournalLine);
        			glOffsetJournalLine.setGlChartOfAccount(glChartOfAccount);
        			
        			
        		} else if (adPreference.getPrfAllowSuspensePosting() == EJBCommon.FALSE &&
        				TOTAL_DEBIT != TOTAL_CREDIT) {
        			
        			throw new GlobalJournalNotBalanceException();		    	
        			
        		}
        		
        		// create journal batch if necessary
        		
        		LocalGlJournalBatch glJournalBatch = null;
        		java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat("MM/dd/yyyy");
        		
        		String VB_NNAME = "";
        		
        		try{
        			
        			VB_NNAME = apVoucher.getApVoucherBatch().getVbName();
        			
        		}catch(Exception ex){
        			
        			VB_NNAME = "";
        			
        		}
        		
        		try {
        			
        			if (adPreference.getPrfEnableApVoucherBatch() == EJBCommon.TRUE) {
        				
        				glJournalBatch = glJournalBatchHome.findByJbName(VB_NNAME, AD_BRNCH, AD_CMPNY);
        				
        			} else {
        				
        				
        				glJournalBatch = glJournalBatchHome.findByJbName(VB_NNAME, AD_BRNCH, AD_CMPNY);
        				
        			}
        			
        			
        		} catch (FinderException ex) {
        		}
        		
        		if (adPreference.getPrfEnableGlJournalBatch() ==  EJBCommon.TRUE && 
        				glJournalBatch == null && !VB_NNAME.equals("")) {
        			
        			if (adPreference.getPrfEnableApVoucherBatch() == EJBCommon.TRUE ) {
        				
        				glJournalBatch = glJournalBatchHome.create(apVoucher.getApVoucherBatch().getVbName(), apVoucher.getApVoucherBatch().getVbName(), "CLOSED", EJBCommon.getGcCurrentDateWoTime().getTime(), USR_NM, AD_BRNCH, AD_CMPNY);
        				
        			} else {
        				
        				glJournalBatch = glJournalBatchHome.create(apVoucher.getApVoucherBatch().getVbName(), apVoucher.getApVoucherBatch().getVbName(), "CLOSED", EJBCommon.getGcCurrentDateWoTime().getTime(), USR_NM, AD_BRNCH, AD_CMPNY);
        				
        			}
        			
        			
        		}

        		// create journal entry			            	
        		
        		LocalGlJournal glJournal = glJournalHome.create(apVoucher.getVouDebitMemo() == 1 ? apVoucher.getVouDmVoucherNumber() : apVoucher.getVouReferenceNumber(),
        				apVoucher.getVouDescription(), apVoucher.getVouDate(),
						0.0d, null, apVoucher.getVouDocumentNumber(), null, 1d, "N/A", null,
						'N', EJBCommon.TRUE, EJBCommon.FALSE,
						USR_NM, new Date(),
						USR_NM, new Date(),
						null, null,
						USR_NM, EJBCommon.getGcCurrentDateWoTime().getTime(), 
						apVoucher.getApSupplier().getSplTin(), apVoucher.getApSupplier().getSplName(), EJBCommon.FALSE,
						null, 
						AD_BRNCH, AD_CMPNY);
        		
        		LocalGlJournalSource glJournalSource = glJournalSourceHome.findByJsName("ACCOUNTS PAYABLES", AD_CMPNY);
        		//glJournalSource.addGlJournal(glJournal);
        		glJournal.setGlJournalSource(glJournalSource);
        		
        		
        		LocalGlFunctionalCurrency glFunctionalCurrency = glFunctionalCurrencyHome.findByFcName(adCompany.getGlFunctionalCurrency().getFcName(), AD_CMPNY);
        		//glFunctionalCurrency.addGlJournal(glJournal);
        		glJournal.setGlFunctionalCurrency(glFunctionalCurrency);
        		
        		LocalGlJournalCategory glJournalCategory = glJournalCategoryHome.findByJcName(apVoucher.getVouDebitMemo() == EJBCommon.FALSE ? "VOUCHERS" : "DEBIT MEMOS", AD_CMPNY);
        		//glJournalCategory.addGlJournal(glJournal);
        		glJournal.setGlJournalCategory(glJournalCategory);
        		
        		if (glJournalBatch != null) {
        			
        			//glJournalBatch.addGlJournal(glJournal);
        			glJournal.setGlJournalBatch(glJournalBatch);
        			
        		}           		    
        		
        		
        		// create journal lines
        		
        		j = apDistributionRecords.iterator();
        		
        		while (j.hasNext()) {
        			
        			LocalApDistributionRecord apDistributionRecord = (LocalApDistributionRecord)j.next();
        			
        			double DR_AMNT = 0d;
        			
        			if (apVoucher.getVouDebitMemo() == EJBCommon.FALSE) {
        				
        				DR_AMNT = this.convertForeignToFunctionalCurrency(apVoucher.getGlFunctionalCurrency().getFcCode(),
        						apVoucher.getGlFunctionalCurrency().getFcName(), 
								apVoucher.getVouConversionDate(),
								apVoucher.getVouConversionRate(),
								apDistributionRecord.getDrAmount(), AD_CMPNY);
        				
        			} else {
        				
        				DR_AMNT = this.convertForeignToFunctionalCurrency(apDebitedVoucher.getGlFunctionalCurrency().getFcCode(),
        						apDebitedVoucher.getGlFunctionalCurrency().getFcName(), 
								apDebitedVoucher.getVouConversionDate(),
								apDebitedVoucher.getVouConversionRate(),
								apDistributionRecord.getDrAmount(), AD_CMPNY);
        				
        			}
        			
        			LocalGlJournalLine glJournalLine = glJournalLineHome.create(
        					apDistributionRecord.getDrLine(),	            			
							apDistributionRecord.getDrDebit(),
							DR_AMNT, "", AD_CMPNY);
        			
        			//apDistributionRecord.getGlChartOfAccount().addGlJournalLine(glJournalLine);
        			glJournalLine.setGlChartOfAccount(apDistributionRecord.getGlChartOfAccount());
        			
        			//glJournal.addGlJournalLine(glJournalLine);
        			glJournalLine.setGlJournal(glJournal);
        			
        			apDistributionRecord.setDrImported(EJBCommon.TRUE);
        			
    		  	  	// for FOREX revaluation
            	    
            	    LocalApVoucher apVoucherTemp = apVoucher.getVouDebitMemo() == EJBCommon.FALSE ?
            	    	apVoucher: apDebitedVoucher;
            	    		
    		  	  	if((apVoucherTemp.getGlFunctionalCurrency().getFcCode() !=
    		  	  			adCompany.getGlFunctionalCurrency().getFcCode()) &&
    					glJournalLine.getGlChartOfAccount().getGlFunctionalCurrency() != null &&
    		  	  		(glJournalLine.getGlChartOfAccount().getGlFunctionalCurrency().getFcCode().equals(
    		  	  		apVoucherTemp.getGlFunctionalCurrency().getFcCode()))){
    		  	  		
    		  	  		double CONVERSION_RATE = 1;
    		  	  		
    		  	  		if (apVoucherTemp.getVouConversionRate() != 0 && apVoucherTemp.getVouConversionRate() != 1) {
    		  	  			
    		  	  			CONVERSION_RATE = apVoucherTemp.getVouConversionRate();
    		  	  			
    		  	  		} else if (apVoucherTemp.getVouConversionDate() != null){
    		  	  			
    		  	  			CONVERSION_RATE = this.getFrRateByFrNameAndFrDate(
    			  	  				glJournalLine.getGlChartOfAccount().getGlFunctionalCurrency().getFcName(), 
    								glJournal.getJrConversionDate(), AD_CMPNY);
    		  	  			
    		  	  		}
    		  	  		
    		  	  		Collection glForexLedgers = null;
    		  	  		
    		  	  		try {
    		  	  			
    		  	  			glForexLedgers = glForexLedgerHome.findLatestGlFrlByFrlDateAndByCoaCode(
    		  	  				apVoucherTemp.getVouDate(), glJournalLine.getGlChartOfAccount().getCoaCode(), AD_CMPNY);
    		  	  			
    		  	  		} catch(FinderException ex) {
    		  	  			
    		  	  		}
    		  	  		
    		  	  		LocalGlForexLedger glForexLedger =
    		  	  			(glForexLedgers.isEmpty() || glForexLedgers == null) ? null :
    		  	  				(LocalGlForexLedger) glForexLedgers.iterator().next();
    		  	  		
    		  	  		int FRL_LN = (glForexLedger != null &&
    		  	  				glForexLedger.getFrlDate().compareTo(apVoucherTemp.getVouDate()) == 0) ?
    		  	  						glForexLedger.getFrlLine().intValue() + 1 : 1;
    		  	  		
    		  	  		// compute balance
    		  	  		double COA_FRX_BLNC = glForexLedger == null ? 0 : glForexLedger.getFrlBalance();
    		  	  		double FRL_AMNT = apDistributionRecord.getDrAmount();
    		  	  		
    		  	  		if(glJournalLine.getGlChartOfAccount().getCoaAccountType().equalsIgnoreCase("ASSET"))
    		  	  			FRL_AMNT = (glJournalLine.getJlDebit() == EJBCommon.TRUE  ? FRL_AMNT : (- 1 * FRL_AMNT));  
    		  	  		else
    		  	  			FRL_AMNT = (glJournalLine.getJlDebit() == EJBCommon.TRUE  ? (- 1 * FRL_AMNT) : FRL_AMNT);
    		  	  		
    		  	  		COA_FRX_BLNC = COA_FRX_BLNC + FRL_AMNT;
    		  	  		
    		  	  		glForexLedger = glForexLedgerHome.create(apVoucherTemp.getVouDate(), new Integer (FRL_LN),
    		  	  			apVoucher.getVouDebitMemo() == EJBCommon.FALSE ? "APV" : "DM", FRL_AMNT, CONVERSION_RATE,
    		  	  				COA_FRX_BLNC, 0d, AD_CMPNY);
    		  	  		
    		  	  		//glJournalLine.getGlChartOfAccount().addGlForexLedger(glForexLedger);
    		  	  		glForexLedger.setGlChartOfAccount(glJournalLine.getGlChartOfAccount());
    		  	  		
    		  	  		// propagate balances
    		  	  		try{
    		  	  			
    		  	  			glForexLedgers = glForexLedgerHome.findByGreaterThanFrlDateAndCoaCode(
    		  	  				glForexLedger.getFrlDate(), glForexLedger.getGlChartOfAccount().getCoaCode(),
								glForexLedger.getFrlAdCompany());
    		  	  			
    		  	  		} catch (FinderException ex) {
    		  	  			
    		  	  		}
    		  	  		
    		  	  		Iterator itrFrl = glForexLedgers.iterator();
    		  	  		
    		  	  		while (itrFrl.hasNext()) {
    		  	  			
    		  	  			glForexLedger = (LocalGlForexLedger) itrFrl.next();
    		  	  			FRL_AMNT = apDistributionRecord.getDrAmount();
    		  	  			
    		  	  			if(glJournalLine.getGlChartOfAccount().getCoaAccountType().equalsIgnoreCase("ASSET"))
    		  	  				FRL_AMNT = (glJournalLine.getJlDebit() == EJBCommon.TRUE  ? FRL_AMNT :
    		  	  					(- 1 * FRL_AMNT));
    		  	  			else
    		  	  				FRL_AMNT = (glJournalLine.getJlDebit() == EJBCommon.TRUE  ? (- 1 * FRL_AMNT) :
    		  	  					FRL_AMNT);
    		  	  			
    		  	  			glForexLedger.setFrlBalance(glForexLedger.getFrlBalance() + FRL_AMNT);
    		  	  			
    		  	  		}
    		  	  		
    		  	  	}
    		  	  	
        		}
        		
        		if (glOffsetJournalLine != null) {
        			
        			//glJournal.addGlJournalLine(glOffsetJournalLine);
        			glOffsetJournalLine.setGlJournal(glJournal);
        			
        		}		
        		
        		// post journal to gl
        		
        		Collection glJournalLines = glJournal.getGlJournalLines();
        		
        		Iterator i = glJournalLines.iterator();
        		
        		while (i.hasNext()) {
        			
        			LocalGlJournalLine glJournalLine = (LocalGlJournalLine)i.next();	
        			
        			// post current to current acv
        			
        			this.postToGl(glAccountingCalendarValue,
        					glJournalLine.getGlChartOfAccount(),
							true, glJournalLine.getJlDebit(), glJournalLine.getJlAmount(), AD_CMPNY);
        			
        			
        			// post to subsequent acvs (propagate)
        			
        			Collection glSubsequentAccountingCalendarValues = 
        				glAccountingCalendarValueHome.findSubsequentAcvByAcCodeAndAcvPeriodNumber( 
        						glJournalSetOfBook.getGlAccountingCalendar().getAcCode(),
								glAccountingCalendarValue.getAcvPeriodNumber(), AD_CMPNY);     
        			
        			Iterator acvsIter = glSubsequentAccountingCalendarValues.iterator();
        			
        			while (acvsIter.hasNext()) {
        				
        				LocalGlAccountingCalendarValue glSubsequentAccountingCalendarValue = 
        					(LocalGlAccountingCalendarValue)acvsIter.next();
        				
        				this.postToGl(glSubsequentAccountingCalendarValue,
        						glJournalLine.getGlChartOfAccount(),
								false, glJournalLine.getJlDebit(), glJournalLine.getJlAmount(), AD_CMPNY);
        				
        			}
        			
        			// post to subsequent years if necessary
        			
        			Collection glSubsequentSetOfBooks = glSetOfBookHome.findSubsequentSobByAcYear(glJournalSetOfBook.getGlAccountingCalendar().getAcYear(), AD_CMPNY);
        			
        			if (!glSubsequentSetOfBooks.isEmpty() && glJournalSetOfBook.getSobYearEndClosed() == 1) {
        				
        				adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);		  	  	
        				LocalGlChartOfAccount glRetainedEarningsAccount = glChartOfAccountHome.findByCoaAccountNumber(adCompany.getCmpRetainedEarnings(), AD_CMPNY); 	  	
        				
        				Iterator sobIter = glSubsequentSetOfBooks.iterator();
        				
        				while (sobIter.hasNext()) {
        					
        					LocalGlSetOfBook glSubsequentSetOfBook = (LocalGlSetOfBook)sobIter.next();
        					
        					String ACCOUNT_TYPE = glJournalLine.getGlChartOfAccount().getCoaAccountType();
        					
        					// post to subsequent acvs of subsequent set of book(propagate)
        					
        					Collection glAccountingCalendarValues = 
        						glAccountingCalendarValueHome.findByAcCode(glSubsequentSetOfBook.getGlAccountingCalendar().getAcCode(), AD_CMPNY);     
        					
        					Iterator acvIter = glAccountingCalendarValues.iterator();
        					
        					while (acvIter.hasNext()) {
        						
        						LocalGlAccountingCalendarValue glSubsequentAccountingCalendarValue = 
        							(LocalGlAccountingCalendarValue)acvIter.next();
        						
        						if (ACCOUNT_TYPE.equals("ASSET") || ACCOUNT_TYPE.equals("LIABILITY") ||
        								ACCOUNT_TYPE.equals("OWNERS EQUITY")) {
        							
        							this.postToGl(glSubsequentAccountingCalendarValue,
        									glJournalLine.getGlChartOfAccount(),
											false, glJournalLine.getJlDebit(), glJournalLine.getJlAmount(), AD_CMPNY);
        							
        						} else { // revenue & expense
        							
        							this.postToGl(glSubsequentAccountingCalendarValue,
        									glRetainedEarningsAccount,
											false, glJournalLine.getJlDebit(), glJournalLine.getJlAmount(), AD_CMPNY);					        
        							
        						}
        						
        					}
        					
        					if (glSubsequentSetOfBook.getSobYearEndClosed() == 0) break;
        					
        				}
        				
        			}
        			
        		}	   			   
        		
        	}
        	
        } catch (GlJREffectiveDateNoPeriodExistException ex) {
        	
        	getSessionContext().setRollbackOnly();
        	throw ex;
        	
        } catch (GlJREffectiveDatePeriodClosedException ex) {
        	
        	getSessionContext().setRollbackOnly();
        	throw ex;
        	
        } catch (GlobalJournalNotBalanceException ex) {
        	
        	getSessionContext().setRollbackOnly();
        	throw ex;
        	
        } catch (GlobalRecordAlreadyDeletedException ex) {
        	
        	getSessionContext().setRollbackOnly();
        	throw ex;
        	        	
        } catch (GlobalTransactionAlreadyPostedException ex) {
        	
        	getSessionContext().setRollbackOnly();
        	throw ex;
        	
        } catch (GlobalTransactionAlreadyVoidException ex) {
        	
        	getSessionContext().setRollbackOnly();
        	throw ex;

        } catch (AdPRFCoaGlVarianceAccountNotFoundException ex) {
        	
        	getSessionContext().setRollbackOnly();
        	throw ex;

        } catch (Exception ex) {
    		
    		Debug.printStackTrace(ex);
    		getSessionContext().setRollbackOnly();
    		throw new EJBException(ex.getMessage());
    		
    	}
    	
    }
        
    private void executeApChkPost(Integer CHK_CODE, String USR_NM, Integer AD_BRNCH, Integer AD_CMPNY) throws
        GlobalRecordAlreadyDeletedException,
        GlobalTransactionAlreadyPostedException,
		GlobalTransactionAlreadyVoidPostedException,
		GlJREffectiveDateNoPeriodExistException,
		GlJREffectiveDatePeriodClosedException,
		GlobalJournalNotBalanceException {
        
        Debug.print("ApApprovalControllerBean executeApChkPost");
        
        LocalApCheckHome apCheckHome = null;        
        LocalAdCompanyHome adCompanyHome = null;
        LocalAdPreferenceHome adPreferenceHome = null;
        LocalGlSetOfBookHome glSetOfBookHome = null;
        LocalGlAccountingCalendarValueHome glAccountingCalendarValueHome = null;
        LocalGlJournalHome glJournalHome = null;
        LocalGlJournalBatchHome glJournalBatchHome = null;
        LocalGlSuspenseAccountHome glSuspenseAccountHome = null;
        LocalGlJournalLineHome glJournalLineHome = null;
        LocalGlJournalSourceHome glJournalSourceHome = null;
        LocalGlJournalCategoryHome glJournalCategoryHome = null;
        LocalGlFunctionalCurrencyHome glFunctionalCurrencyHome = null;
        LocalApDistributionRecordHome apDistributionRecordHome = null;
        LocalInvCostingHome invCostingHome = null;
        LocalGlChartOfAccountHome glChartOfAccountHome = null;
        LocalAdBankAccountHome adBankAccountHome = null;
        LocalAdBankAccountBalanceHome adBankAccountBalanceHome = null;
 		LocalGlForexLedgerHome glForexLedgerHome = null;
 		LocalGlFunctionalCurrencyRateHome glFunctionalCurrencyRateHome = null;        

        LocalApCheck apCheck = null;         
                
        // Initialize EJB Home
        
        try {
            
            apCheckHome = (LocalApCheckHome)EJBHomeFactory.
                lookUpLocalHome(LocalApCheckHome.JNDI_NAME, LocalApCheckHome.class);
            adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
            adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
            	lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);
            glSetOfBookHome = (LocalGlSetOfBookHome)EJBHomeFactory.
            	lookUpLocalHome(LocalGlSetOfBookHome.JNDI_NAME, LocalGlSetOfBookHome.class);
            glAccountingCalendarValueHome = (LocalGlAccountingCalendarValueHome)EJBHomeFactory.
            	lookUpLocalHome(LocalGlAccountingCalendarValueHome.JNDI_NAME, LocalGlAccountingCalendarValueHome.class);
            glJournalHome = (LocalGlJournalHome)EJBHomeFactory.
            	lookUpLocalHome(LocalGlJournalHome.JNDI_NAME, LocalGlJournalHome.class);
            glJournalBatchHome = (LocalGlJournalBatchHome)EJBHomeFactory.
            	lookUpLocalHome(LocalGlJournalBatchHome.JNDI_NAME, LocalGlJournalBatchHome.class);
            glSuspenseAccountHome = (LocalGlSuspenseAccountHome)EJBHomeFactory.
            	lookUpLocalHome(LocalGlSuspenseAccountHome.JNDI_NAME, LocalGlSuspenseAccountHome.class);
            glJournalLineHome = (LocalGlJournalLineHome)EJBHomeFactory.
            	lookUpLocalHome(LocalGlJournalLineHome.JNDI_NAME, LocalGlJournalLineHome.class);
            glJournalSourceHome = (LocalGlJournalSourceHome)EJBHomeFactory.
            	lookUpLocalHome(LocalGlJournalSourceHome.JNDI_NAME, LocalGlJournalSourceHome.class);
            glJournalCategoryHome = (LocalGlJournalCategoryHome)EJBHomeFactory.
            	lookUpLocalHome(LocalGlJournalCategoryHome.JNDI_NAME, LocalGlJournalCategoryHome.class);
            glFunctionalCurrencyHome = (LocalGlFunctionalCurrencyHome)EJBHomeFactory.
            	lookUpLocalHome(LocalGlFunctionalCurrencyHome.JNDI_NAME, LocalGlFunctionalCurrencyHome.class);
            apDistributionRecordHome = (LocalApDistributionRecordHome)EJBHomeFactory.
            	lookUpLocalHome(LocalApDistributionRecordHome.JNDI_NAME, LocalApDistributionRecordHome.class);
            invCostingHome = (LocalInvCostingHome)EJBHomeFactory.
            	lookUpLocalHome(LocalInvCostingHome.JNDI_NAME, LocalInvCostingHome.class);  
            glChartOfAccountHome = (LocalGlChartOfAccountHome)EJBHomeFactory.
            	lookUpLocalHome(LocalGlChartOfAccountHome.JNDI_NAME, LocalGlChartOfAccountHome.class);
            adBankAccountHome = (LocalAdBankAccountHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdBankAccountHome.JNDI_NAME, LocalAdBankAccountHome.class);
            adBankAccountBalanceHome = (LocalAdBankAccountBalanceHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdBankAccountBalanceHome.JNDI_NAME, LocalAdBankAccountBalanceHome.class);
 			glForexLedgerHome  = (LocalGlForexLedgerHome)EJBHomeFactory.
				lookUpLocalHome(LocalGlForexLedgerHome.JNDI_NAME, LocalGlForexLedgerHome.class);
			glFunctionalCurrencyRateHome = (LocalGlFunctionalCurrencyRateHome)EJBHomeFactory.
				lookUpLocalHome(LocalGlFunctionalCurrencyRateHome.JNDI_NAME, LocalGlFunctionalCurrencyRateHome.class);
   
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }       
        
        try {
        	
        	// validate if check is already deleted
        	
        	try {
        		
        		apCheck = apCheckHome.findByPrimaryKey(CHK_CODE);
        		
        	} catch (FinderException ex) {
        		
        		throw new GlobalRecordAlreadyDeletedException();
        		
        	}
        	
        	// validate if check is already posted
        	
        	if (apCheck.getChkVoid() == EJBCommon.FALSE && apCheck.getChkPosted() == EJBCommon.TRUE) {
        		
        		throw new GlobalTransactionAlreadyPostedException();
        		
        		// validate if check void is already posted
        		
        	} else if (apCheck.getChkVoid() == EJBCommon.TRUE && apCheck.getChkVoidPosted() == EJBCommon.TRUE) {
        		
        		throw new GlobalTransactionAlreadyVoidPostedException();
        		
        	}
        	
        	
        	// post check
        	
        	if (apCheck.getChkVoid() == EJBCommon.FALSE && apCheck.getChkPosted() == EJBCommon.FALSE) {
        		
        		
        		if (apCheck.getChkType().equals("PAYMENT")) {        		
        			
        			// increase amount paid in voucher payment schedules and voucher
        			
        			double CHK_CRDTS = 0d;
        			
        			Collection apAppliedVouchers = apCheck.getApAppliedVouchers();
        			
        			Iterator i = apAppliedVouchers.iterator();
        			
        			while (i.hasNext()) {
        				
        				LocalApAppliedVoucher apAppliedVoucher = (LocalApAppliedVoucher)i.next();
        				
        				LocalApVoucherPaymentSchedule apVoucherPaymentSchedule = apAppliedVoucher.getApVoucherPaymentSchedule();
        				
        				double AMOUNT_PAID = apAppliedVoucher.getAvApplyAmount() + apAppliedVoucher.getAvDiscountAmount() + apAppliedVoucher.getAvTaxWithheld();
        				
        				CHK_CRDTS += this.convertForeignToFunctionalCurrency(apAppliedVoucher.getApVoucherPaymentSchedule().getApVoucher().getGlFunctionalCurrency().getFcCode(),
        						apAppliedVoucher.getApVoucherPaymentSchedule().getApVoucher().getGlFunctionalCurrency().getFcName(),
								apAppliedVoucher.getApVoucherPaymentSchedule().getApVoucher().getVouConversionDate(),
								apAppliedVoucher.getApVoucherPaymentSchedule().getApVoucher().getVouConversionRate(),
								apAppliedVoucher.getAvDiscountAmount() + apAppliedVoucher.getAvTaxWithheld(), AD_CMPNY);
        				
        				apVoucherPaymentSchedule.setVpsAmountPaid(EJBCommon.roundIt(apVoucherPaymentSchedule.getVpsAmountPaid() + AMOUNT_PAID, this.getGlFcPrecisionUnit(AD_CMPNY)));
        				
        				apVoucherPaymentSchedule.getApVoucher().setVouAmountPaid(EJBCommon.roundIt(apVoucherPaymentSchedule.getApVoucher().getVouAmountPaid() + AMOUNT_PAID, this.getGlFcPrecisionUnit(AD_CMPNY)));
        				
        				// release voucher lock
        				
        				apVoucherPaymentSchedule.setVpsLock(EJBCommon.FALSE);        		   	
        				
        			}
        			
        			// decrease supplier balance
        			
        			double CHK_AMNT = this.convertForeignToFunctionalCurrency(apCheck.getGlFunctionalCurrency().getFcCode(),
        					apCheck.getGlFunctionalCurrency().getFcName(),
							apCheck.getChkConversionDate(), apCheck.getChkConversionRate(),
							apCheck.getChkAmount(), AD_CMPNY);		        		
        			
        			this.post(apCheck.getChkDate(), (CHK_AMNT + CHK_CRDTS) * -1, apCheck.getApSupplier(), AD_CMPNY); 
        			
        		} else if (apCheck.getChkType().equals("DIRECT") && !apCheck.getApVoucherLineItems().isEmpty()) {


					
					Iterator c = apCheck.getApVoucherLineItems().iterator();
					
					while(c.hasNext()) {
						
						LocalApVoucherLineItem apVoucherLineItem = (LocalApVoucherLineItem) c.next();
						
						String II_NM = apVoucherLineItem.getInvItemLocation().getInvItem().getIiName();
						String LOC_NM = apVoucherLineItem.getInvItemLocation().getInvLocation().getLocName();

						double QTY_RCVD = this.convertByUomFromAndItemAndQuantity(apVoucherLineItem.getInvUnitOfMeasure(), 
								apVoucherLineItem.getInvItemLocation().getInvItem(), apVoucherLineItem.getVliQuantity(), AD_CMPNY);
						
						LocalInvCosting invCosting = null;
						// CREATE COSTING
						try {
							
							invCosting  = invCostingHome.getByMaxCstDateToLongAndMaxCstLineNumberAndLessThanEqualCstDateAndIiNameAndLocName(apCheck.getChkDate(), II_NM, LOC_NM, AD_BRNCH, AD_CMPNY);
							
						} catch (FinderException ex) {
							
						}
						
						double COST = apVoucherLineItem.getVliUnitCost();
						// CREATE COSTING
						if (invCosting == null) {
							
							this.postToInvVou(apVoucherLineItem, apCheck.getChkDate(), 
									QTY_RCVD, COST * QTY_RCVD, 
									QTY_RCVD, COST * QTY_RCVD, 
									0d, null, AD_BRNCH, AD_CMPNY);			
							
						} else {
							
							this.postToInvVou(apVoucherLineItem, apCheck.getChkDate(), 
									QTY_RCVD, QTY_RCVD * COST,
									invCosting.getCstRemainingQuantity() + QTY_RCVD, invCosting.getCstRemainingValue() + (QTY_RCVD * COST), 
									0d, null, AD_BRNCH, AD_CMPNY);
							
						}
						
					}
        		}
        		
        		// decrease bank balance
        		
				LocalAdBankAccount adBankAccount = adBankAccountHome.findByPrimaryKey(apCheck.getAdBankAccount().getBaCode());
				
				try {
					
					// find bankaccount balance before or equal check date
					
					Collection adBankAccountBalances = adBankAccountBalanceHome.findByBeforeOrEqualDateAndBaCodeAndType(apCheck.getChkCheckDate(), apCheck.getAdBankAccount().getBaCode(), "BOOK", AD_CMPNY);

					if (!adBankAccountBalances.isEmpty()) {
						
						// get last check
						
						ArrayList adBankAccountBalanceList = new ArrayList(adBankAccountBalances);
						
						LocalAdBankAccountBalance adBankAccountBalance = (LocalAdBankAccountBalance)adBankAccountBalanceList.get(adBankAccountBalanceList.size() - 1);
						
						if (adBankAccountBalance.getBabDate().before(apCheck.getChkCheckDate())) {
							
							// create new balance

							LocalAdBankAccountBalance adNewBankAccountBalance = adBankAccountBalanceHome.create(
									apCheck.getChkCheckDate(), adBankAccountBalance.getBabBalance() - apCheck.getChkAmount(), "BOOK", AD_CMPNY);
							
							//adBankAccount.addAdBankAccountBalance(adNewBankAccountBalance);
							adNewBankAccountBalance.setAdBankAccount(adBankAccount);
							
						} else { // equals to check date

							adBankAccountBalance.setBabBalance(adBankAccountBalance.getBabBalance() - apCheck.getChkAmount());
							
						} 
						
					} else {        	
						
						// create new balance
						
						LocalAdBankAccountBalance adNewBankAccountBalance = adBankAccountBalanceHome.create(
								apCheck.getChkCheckDate(), (0 - apCheck.getChkAmount()), "BOOK", AD_CMPNY);
						
						//adBankAccount.addAdBankAccountBalance(adNewBankAccountBalance);
						adNewBankAccountBalance.setAdBankAccount(adBankAccount);
						
					}
					
					// propagate to subsequent balances if necessary
					
					adBankAccountBalances = adBankAccountBalanceHome.findByAfterDateAndBaCodeAndType(apCheck.getChkCheckDate(), apCheck.getAdBankAccount().getBaCode(), "BOOK", AD_CMPNY);

					Iterator i = adBankAccountBalances.iterator();
					
					while (i.hasNext()) {
						
						LocalAdBankAccountBalance adBankAccountBalance = (LocalAdBankAccountBalance)i.next();
						
						adBankAccountBalance.setBabBalance(adBankAccountBalance.getBabBalance() - apCheck.getChkAmount());
						
					}			
					
				} catch (Exception ex) {
					
					ex.printStackTrace();
					
				}
        		
        		// set check post status
        		
        		apCheck.setChkPosted(EJBCommon.TRUE);
        		apCheck.setChkPostedBy(USR_NM);
        		apCheck.setChkDatePosted(EJBCommon.getGcCurrentDateWoTime().getTime());
        		
        		
        	} else if (apCheck.getChkVoid() == EJBCommon.TRUE && apCheck.getChkVoidPosted() == EJBCommon.FALSE) { // void check
        		
        		
        		if (apCheck.getChkType().equals("PAYMENT")) {
        			
        			// decrease amount paid in voucher payment schedules and voucher
        			
        			double CHK_CRDTS = 0d;
        			
        			Collection apAppliedVouchers = apCheck.getApAppliedVouchers();
        			
        			Iterator i = apAppliedVouchers.iterator();
        			
        			while (i.hasNext()) {
        				
        				LocalApAppliedVoucher apAppliedVoucher = (LocalApAppliedVoucher)i.next();
        				
        				LocalApVoucherPaymentSchedule apVoucherPaymentSchedule = apAppliedVoucher.getApVoucherPaymentSchedule();
        				
        				double AMOUNT_PAID = apAppliedVoucher.getAvApplyAmount() + apAppliedVoucher.getAvDiscountAmount() + apAppliedVoucher.getAvTaxWithheld();
        				
        				CHK_CRDTS += this.convertForeignToFunctionalCurrency(apAppliedVoucher.getApVoucherPaymentSchedule().getApVoucher().getGlFunctionalCurrency().getFcCode(),
        						apAppliedVoucher.getApVoucherPaymentSchedule().getApVoucher().getGlFunctionalCurrency().getFcName(),
								apAppliedVoucher.getApVoucherPaymentSchedule().getApVoucher().getVouConversionDate(),
								apAppliedVoucher.getApVoucherPaymentSchedule().getApVoucher().getVouConversionRate(),
								apAppliedVoucher.getAvDiscountAmount() + apAppliedVoucher.getAvTaxWithheld(), AD_CMPNY);
        				
        				apVoucherPaymentSchedule.setVpsAmountPaid(EJBCommon.roundIt(apVoucherPaymentSchedule.getVpsAmountPaid() - AMOUNT_PAID, this.getGlFcPrecisionUnit(AD_CMPNY)));
        				
        				apVoucherPaymentSchedule.getApVoucher().setVouAmountPaid(EJBCommon.roundIt(apVoucherPaymentSchedule.getApVoucher().getVouAmountPaid() - AMOUNT_PAID, this.getGlFcPrecisionUnit(AD_CMPNY)));        		       		   	
        				
        			}  
        			
        			// increase supplier balance
        			
        			double CHK_AMNT = this.convertForeignToFunctionalCurrency(apCheck.getGlFunctionalCurrency().getFcCode(),
        					apCheck.getGlFunctionalCurrency().getFcName(),
							apCheck.getChkConversionDate(), apCheck.getChkConversionRate(),
							apCheck.getChkAmount(), AD_CMPNY);	        		
        			
        			this.post(apCheck.getChkDate(), CHK_AMNT + CHK_CRDTS, apCheck.getApSupplier(), AD_CMPNY);	        		
        			
        		} else if (apCheck.getChkType().equals("DIRECT") && !apCheck.getApVoucherLineItems().isEmpty()) {
        			

					// VOIDING DIRECT PAYMENT
					
					Iterator c = apCheck.getApVoucherLineItems().iterator();
					
					while(c.hasNext()) {
						
						LocalApVoucherLineItem apVoucherLineItem = (LocalApVoucherLineItem) c.next();
						
						String II_NM = apVoucherLineItem.getInvItemLocation().getInvItem().getIiName();
						String LOC_NM = apVoucherLineItem.getInvItemLocation().getInvLocation().getLocName();

						double QTY_RCVD = this.convertByUomFromAndItemAndQuantity(apVoucherLineItem.getInvUnitOfMeasure(), 
								apVoucherLineItem.getInvItemLocation().getInvItem(), apVoucherLineItem.getVliQuantity(), AD_CMPNY);
						
						LocalInvCosting invCosting = null;
						// CREATE COSTING
						try {
							
							invCosting  = invCostingHome.getByMaxCstDateToLongAndMaxCstLineNumberAndLessThanEqualCstDateAndIiNameAndLocName(apCheck.getChkDate(), II_NM, LOC_NM, AD_BRNCH, AD_CMPNY);
							
						} catch (FinderException ex) {
							
						}
						
						double COST = apVoucherLineItem.getVliUnitCost();
						
						if (invCosting == null) {
							
							this.postToInvVou(apVoucherLineItem, apCheck.getChkDate(), 
									-QTY_RCVD, -COST * QTY_RCVD, 
									-QTY_RCVD, -COST * QTY_RCVD, 
									0d, null, AD_BRNCH, AD_CMPNY);			
							
						} else {

							this.postToInvVou(apVoucherLineItem, apCheck.getChkDate(), 
									-QTY_RCVD, -(COST * QTY_RCVD),
									invCosting.getCstRemainingQuantity() - QTY_RCVD, invCosting.getCstRemainingValue() - (QTY_RCVD * COST), 
									0d, null, AD_BRNCH, AD_CMPNY);
							
							
						}
						
					}
					
        		}
        		
        		// increase bank balance
        		
				LocalAdBankAccount adBankAccount = adBankAccountHome.findByPrimaryKey(apCheck.getAdBankAccount().getBaCode());
				
				try {
					
					// find bankaccount balance before or equal check date
					
					Collection adBankAccountBalances = adBankAccountBalanceHome.findByBeforeOrEqualDateAndBaCodeAndType(apCheck.getChkCheckDate(), apCheck.getAdBankAccount().getBaCode(), "BOOK", AD_CMPNY);

					if (!adBankAccountBalances.isEmpty()) {
						
						// get last check
						
						ArrayList adBankAccountBalanceList = new ArrayList(adBankAccountBalances);
						
						LocalAdBankAccountBalance adBankAccountBalance = (LocalAdBankAccountBalance)adBankAccountBalanceList.get(adBankAccountBalanceList.size() - 1);
						
						if (adBankAccountBalance.getBabDate().before(apCheck.getChkCheckDate())) {
							
							// create new balance

							LocalAdBankAccountBalance adNewBankAccountBalance = adBankAccountBalanceHome.create(
									apCheck.getChkCheckDate(), adBankAccountBalance.getBabBalance() + apCheck.getChkAmount(), "BOOK", AD_CMPNY);
							
							//adBankAccount.addAdBankAccountBalance(adNewBankAccountBalance);
							adNewBankAccountBalance.setAdBankAccount(adBankAccount);
						} else { // equals to check date

							adBankAccountBalance.setBabBalance(adBankAccountBalance.getBabBalance() + apCheck.getChkAmount());
							
						} 
						
					} else {        	
						
						// create new balance
						
						LocalAdBankAccountBalance adNewBankAccountBalance = adBankAccountBalanceHome.create(
								apCheck.getChkCheckDate(), (apCheck.getChkAmount()), "BOOK", AD_CMPNY);
						
						//adBankAccount.addAdBankAccountBalance(adNewBankAccountBalance);
						adNewBankAccountBalance.setAdBankAccount(adBankAccount);
						
					}
					
					// propagate to subsequent balances if necessary
					
					adBankAccountBalances = adBankAccountBalanceHome.findByAfterDateAndBaCodeAndType(apCheck.getChkCheckDate(), apCheck.getAdBankAccount().getBaCode(), "BOOK", AD_CMPNY);

					Iterator i = adBankAccountBalances.iterator();
					
					while (i.hasNext()) {
						
						LocalAdBankAccountBalance adBankAccountBalance = (LocalAdBankAccountBalance)i.next();
						
						adBankAccountBalance.setBabBalance(adBankAccountBalance.getBabBalance() + apCheck.getChkAmount());
						
					}			
					
				} catch (Exception ex) {
					
					ex.printStackTrace();
					
				}
        		
        		// set check post status
        		
        		apCheck.setChkVoidPosted(EJBCommon.TRUE);
        		apCheck.setChkPostedBy(USR_NM);
        		apCheck.setChkDatePosted(EJBCommon.getGcCurrentDateWoTime().getTime());
        		
        	}
        	
        	// post to gl if necessary
        	
        	LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);
        	LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
        	
        	if (adPreference.getPrfApGlPostingType().equals("AUTO-POST UPON APPROVAL")) {
        		
        		// validate if date has no period and period is closed
        		
        		LocalGlSetOfBook glJournalSetOfBook = null;
        		
        		try {
        			
        			glJournalSetOfBook = glSetOfBookHome.findByDate(apCheck.getChkDate(), AD_CMPNY);
        			
        		} catch (FinderException ex) {
        			
        			throw new GlJREffectiveDateNoPeriodExistException();
        			
        		}
        		
        		LocalGlAccountingCalendarValue glAccountingCalendarValue = 
        			glAccountingCalendarValueHome.findByAcCodeAndDate(
        					glJournalSetOfBook.getGlAccountingCalendar().getAcCode(), apCheck.getChkDate(), AD_CMPNY);
        		
        		
        		if (glAccountingCalendarValue.getAcvStatus() == 'N' ||
        				glAccountingCalendarValue.getAcvStatus() == 'C' ||
						glAccountingCalendarValue.getAcvStatus() == 'P') {
        			
        			throw new GlJREffectiveDatePeriodClosedException();
        			
        		}
        		
        		// check if check is balance if not check suspense posting
        		
        		LocalGlJournalLine glOffsetJournalLine = null;
        		
        		Collection apDistributionRecords = null;
        		
        		if (apCheck.getChkVoid() == EJBCommon.FALSE) {
        			
        			apDistributionRecords = apDistributionRecordHome.findImportableDrByDrReversedAndChkCode(EJBCommon.FALSE, apCheck.getChkCode(), AD_CMPNY);
        			
        		} else {
        			
        			apDistributionRecords = apDistributionRecordHome.findImportableDrByDrReversedAndChkCode(EJBCommon.TRUE, apCheck.getChkCode(), AD_CMPNY);
        			
        		}
        		
        		Iterator j = apDistributionRecords.iterator();
        		
        		double TOTAL_DEBIT = 0d;
        		double TOTAL_CREDIT = 0d;
        		
        		while (j.hasNext()) {
        			
        			LocalApDistributionRecord apDistributionRecord = (LocalApDistributionRecord)j.next();
        			
        			double DR_AMNT = 0d;
        			
        			if (apDistributionRecord.getApAppliedVoucher() != null) {
        				
        				LocalApVoucher apVoucher = apDistributionRecord.getApAppliedVoucher().getApVoucherPaymentSchedule().getApVoucher();
        				
        				DR_AMNT = this.convertForeignToFunctionalCurrency(apVoucher.getGlFunctionalCurrency().getFcCode(),
        						apVoucher.getGlFunctionalCurrency().getFcName(), 
								apVoucher.getVouConversionDate(),
								apVoucher.getVouConversionRate(),
								apDistributionRecord.getDrAmount(), AD_CMPNY);
        				
        			} else {
        				
        				DR_AMNT = this.convertForeignToFunctionalCurrency(apCheck.getGlFunctionalCurrency().getFcCode(),
        						apCheck.getGlFunctionalCurrency().getFcName(), 
								apCheck.getChkConversionDate(),
								apCheck.getChkConversionRate(),
								apDistributionRecord.getDrAmount(), AD_CMPNY);            			
        				
        			}   
        			
        			if (apDistributionRecord.getDrDebit() == EJBCommon.TRUE) {
        				
        				TOTAL_DEBIT += DR_AMNT;
        				
        			} else {
        				
        				TOTAL_CREDIT += DR_AMNT;
        				
        			}
        			
        		}
        		
        		TOTAL_DEBIT = EJBCommon.roundIt(TOTAL_DEBIT, adCompany.getGlFunctionalCurrency().getFcPrecision());
        		TOTAL_CREDIT = EJBCommon.roundIt(TOTAL_CREDIT, adCompany.getGlFunctionalCurrency().getFcPrecision());
        		
        		if (adPreference.getPrfAllowSuspensePosting() == EJBCommon.TRUE &&
        				TOTAL_DEBIT != TOTAL_CREDIT) {
        			
        			LocalGlSuspenseAccount glSuspenseAccount = null;
        			
        			try { 	
        				
        				glSuspenseAccount = glSuspenseAccountHome.findByJsNameAndJcName("ACCOUNTS PAYABLES", "CHECKS", AD_CMPNY);
        				
        			} catch (FinderException ex) {
        				
        				throw new GlobalJournalNotBalanceException();
        				
        			}
        			
        			if (TOTAL_DEBIT - TOTAL_CREDIT < 0) {
        				
        				glOffsetJournalLine = glJournalLineHome.create(
        						(short)(apDistributionRecords.size() + 1),
								EJBCommon.TRUE,
								TOTAL_CREDIT - TOTAL_DEBIT, "", AD_CMPNY);
        				
        			} else {
        				
        				glOffsetJournalLine = glJournalLineHome.create(
        						(short)(apDistributionRecords.size() + 1),
								EJBCommon.FALSE,
								TOTAL_DEBIT - TOTAL_CREDIT, "", AD_CMPNY);
        				
        			}
        			
        			LocalGlChartOfAccount glChartOfAccount = glSuspenseAccount.getGlChartOfAccount();
        			//glChartOfAccount.addGlJournalLine(glOffsetJournalLine);
        			glOffsetJournalLine.setGlChartOfAccount(glChartOfAccount);
        			
        			
        		} else if (adPreference.getPrfAllowSuspensePosting() == EJBCommon.FALSE &&
        				TOTAL_DEBIT != TOTAL_CREDIT) {
        			
        			throw new GlobalJournalNotBalanceException();		    	
        			
        		}
        		
        		// create journal batch if necessary
        		
        		LocalGlJournalBatch glJournalBatch = null;
        		java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat("MM/dd/yyyy");
        		
        		String CB_NAME = "";
        			
        		try{
        			
        			CB_NAME = apCheck.getApCheckBatch().getCbName();
        			
        			
        		}catch(Exception ex){
        			CB_NAME="";
        		}
        		
        		try {
        			
        			if (adPreference.getPrfEnableApCheckBatch() == EJBCommon.TRUE) {
        				
        				glJournalBatch = glJournalBatchHome.findByJbName(CB_NAME, AD_BRNCH, AD_CMPNY);
        				
        			} else {
        				
        				glJournalBatch = glJournalBatchHome.findByJbName(CB_NAME, AD_BRNCH, AD_CMPNY);
        				
        			}		           
        			
        		} catch (FinderException ex) {
        		}
        		
        		if (adPreference.getPrfEnableGlJournalBatch() ==  EJBCommon.TRUE && 
        				glJournalBatch == null && !CB_NAME.equals("")) {
        			
        			if (adPreference.getPrfEnableApCheckBatch() == EJBCommon.TRUE) {
        				
        				glJournalBatch = glJournalBatchHome.create(apCheck.getApCheckBatch().getCbName(), apCheck.getApCheckBatch().getCbName(), "CLOSED", EJBCommon.getGcCurrentDateWoTime().getTime(), USR_NM, AD_BRNCH, AD_CMPNY);
        				
        			} else {
        				
        				glJournalBatch = glJournalBatchHome.create(apCheck.getApCheckBatch().getCbName(), apCheck.getApCheckBatch().getCbName(), "CLOSED", EJBCommon.getGcCurrentDateWoTime().getTime(), USR_NM, AD_BRNCH, AD_CMPNY);
        				
        			}
        			
        			
        		}
        		
        		// create journal entry			            	
        		
        		LocalGlJournal glJournal = glJournalHome.create(apCheck.getChkReferenceNumber(),
        				apCheck.getChkDescription(), apCheck.getChkDate(),
						0.0d, null, apCheck.getChkDocumentNumber(), null, 1d, "N/A", null,
						'N', EJBCommon.TRUE, EJBCommon.FALSE,
						USR_NM, new Date(),
						USR_NM, new Date(),
						null, null,
						USR_NM, EJBCommon.getGcCurrentDateWoTime().getTime(), 
						apCheck.getApSupplier().getSplTin(), 
						apCheck.getApSupplier().getSplName(), EJBCommon.FALSE,
						null, 
						AD_BRNCH, AD_CMPNY);
        		
        		LocalGlJournalSource glJournalSource = glJournalSourceHome.findByJsName("ACCOUNTS PAYABLES", AD_CMPNY);
        		glJournal.setGlJournalSource(glJournalSource);
        		
        		LocalGlFunctionalCurrency glFunctionalCurrency = glFunctionalCurrencyHome.findByFcName(adCompany.getGlFunctionalCurrency().getFcName(), AD_CMPNY);
        		glJournal.setGlFunctionalCurrency(glFunctionalCurrency);
        		
        		LocalGlJournalCategory glJournalCategory = glJournalCategoryHome.findByJcName("CHECKS", AD_CMPNY);
        		glJournal.setGlJournalCategory(glJournalCategory);
        		
        		if (glJournalBatch != null) {
        			
        			//glJournalBatch.addGlJournal(glJournal);
        			glJournal.setGlJournalBatch(glJournalBatch);
        			
        		}           		    
        		
        		
        		// create journal lines
        		
        		j = apDistributionRecords.iterator();
        		boolean firstFlag = true;
        		
        		while (j.hasNext()) {
        			
        			LocalApDistributionRecord apDistributionRecord = (LocalApDistributionRecord)j.next();
        			
        			double DR_AMNT = 0d;
        			LocalApVoucher apVoucher = null;

        			if (apDistributionRecord.getApAppliedVoucher() != null) {
        				
        				apVoucher = apDistributionRecord.getApAppliedVoucher().getApVoucherPaymentSchedule().getApVoucher();
        				
        				DR_AMNT = this.convertForeignToFunctionalCurrency(apVoucher.getGlFunctionalCurrency().getFcCode(),
        						apVoucher.getGlFunctionalCurrency().getFcName(), 
								apVoucher.getVouConversionDate(),
								apVoucher.getVouConversionRate(),
								apDistributionRecord.getDrAmount(), AD_CMPNY);
        				
        			} else {
        				
        				DR_AMNT = this.convertForeignToFunctionalCurrency(apCheck.getGlFunctionalCurrency().getFcCode(),
        						apCheck.getGlFunctionalCurrency().getFcName(), 
								apCheck.getChkConversionDate(),
								apCheck.getChkConversionRate(),
								apDistributionRecord.getDrAmount(), AD_CMPNY);            			
        				
        			}            		            		
        			
        			LocalGlJournalLine glJournalLine = glJournalLineHome.create(
        					apDistributionRecord.getDrLine(),	            			
							apDistributionRecord.getDrDebit(),
							DR_AMNT, "", AD_CMPNY);
        			
        			//apDistributionRecord.getGlChartOfAccount().addGlJournalLine(glJournalLine);
        			glJournalLine.setGlChartOfAccount(apDistributionRecord.getGlChartOfAccount());
        			
        			//glJournal.addGlJournalLine(glJournalLine);
        			glJournalLine.setGlJournal(glJournal);
        			
        			apDistributionRecord.setDrImported(EJBCommon.TRUE);
        			
    		       	// for FOREX revaluation            	    
            	    
            	    int FC_CODE = apDistributionRecord.getApAppliedVoucher() != null ?
            	    	apVoucher.getGlFunctionalCurrency().getFcCode().intValue() :
            	    	apCheck.getGlFunctionalCurrency().getFcCode().intValue();

            	    if((FC_CODE != adCompany.getGlFunctionalCurrency().getFcCode().intValue()) &&
    					glJournalLine.getGlChartOfAccount().getGlFunctionalCurrency() != null && (FC_CODE ==
    						glJournalLine.getGlChartOfAccount().getGlFunctionalCurrency().getFcCode().intValue())){
    		       		
    		       		double CONVERSION_RATE = apDistributionRecord.getApAppliedVoucher() != null ?
    		            	    apVoucher.getVouConversionRate() : apCheck.getChkConversionRate();

    		            Date DATE = apDistributionRecord.getApAppliedVoucher() != null ?
    		            	    apVoucher.getVouConversionDate() : apCheck.getChkConversionDate();

    		            if (DATE != null && (CONVERSION_RATE == 0 || CONVERSION_RATE == 1)){
    	    		       	
    		  	  			CONVERSION_RATE = this.getFrRateByFrNameAndFrDate(
    			  	  				glJournalLine.getGlChartOfAccount().getGlFunctionalCurrency().getFcName(), 
    								glJournal.getJrConversionDate(), AD_CMPNY);
    	    		       			
    		            } else if (CONVERSION_RATE == 0) {
    		       			
    		            	CONVERSION_RATE = 1;

    		       		} 
    		       		
    		       		Collection glForexLedgers = null;
    		       		
    		       		DATE = apDistributionRecord.getApAppliedVoucher() != null ?
    		            	    apVoucher.getVouDate() : apCheck.getChkDate();
    		       		
    		       		try {
    		       			
    		       			glForexLedgers = glForexLedgerHome.findLatestGlFrlByFrlDateAndByCoaCode(
    		       					DATE, glJournalLine.getGlChartOfAccount().getCoaCode(),
    								AD_CMPNY);
    		       			
    		       		} catch(FinderException ex) {
    		       			
    		       		}
    		       		
    		       		LocalGlForexLedger glForexLedger =
    		       			(glForexLedgers.isEmpty() || glForexLedgers == null) ? null :
    		       				(LocalGlForexLedger) glForexLedgers.iterator().next();
    		       		
    		       		int FRL_LN = (glForexLedger != null &&
    		       			glForexLedger.getFrlDate().compareTo(DATE) == 0) ?
    		       				glForexLedger.getFrlLine().intValue() + 1 : 1;
    		       		
    		       		// compute balance
    		       		double COA_FRX_BLNC = glForexLedger == null ? 0 : glForexLedger.getFrlBalance();
    		       		double FRL_AMNT = apDistributionRecord.getDrAmount();
    		       		
    		       		if(glJournalLine.getGlChartOfAccount().getCoaAccountType().equalsIgnoreCase("ASSET"))
    		       			FRL_AMNT = (glJournalLine.getJlDebit() == EJBCommon.TRUE  ? FRL_AMNT : (- 1 * FRL_AMNT));  
    		       		else
    		       			FRL_AMNT = (glJournalLine.getJlDebit() == EJBCommon.TRUE  ? (- 1 * FRL_AMNT) : FRL_AMNT);
    		       		
    		       		COA_FRX_BLNC = COA_FRX_BLNC + FRL_AMNT;
    		       		
    		       		double FRX_GN_LSS = 0d;
    		       			
    		       		if (glOffsetJournalLine != null && firstFlag) {

    		       			if(glOffsetJournalLine.getGlChartOfAccount().getCoaAccountType().equalsIgnoreCase("ASSET"))
    		       				
    		       				FRX_GN_LSS = (glOffsetJournalLine.getJlDebit() == EJBCommon.TRUE ?
    		       					glOffsetJournalLine.getJlAmount() : (- 1 * glOffsetJournalLine.getJlAmount()));
    		       			
    		       			else
    		       				
    		       				FRX_GN_LSS = (glOffsetJournalLine.getJlDebit() == EJBCommon.TRUE ?
    		       					(- 1 * glOffsetJournalLine.getJlAmount()) : glOffsetJournalLine.getJlAmount());

    		       			firstFlag = false;
    		       			
    		       		}

    		       		glForexLedger = glForexLedgerHome.create(DATE, new Integer (FRL_LN), "CHK", FRL_AMNT,
    		       				CONVERSION_RATE, COA_FRX_BLNC, FRX_GN_LSS, AD_CMPNY);
    		       		
    		       		//glJournalLine.getGlChartOfAccount().addGlForexLedger(glForexLedger);
    		       		glForexLedger.setGlChartOfAccount(glJournalLine.getGlChartOfAccount());
    		       		
    		       		// propagate balances
    		       		try{
    		       			
    		       			glForexLedgers = glForexLedgerHome.findByGreaterThanFrlDateAndCoaCode(
    		       					glForexLedger.getFrlDate(), glForexLedger.getGlChartOfAccount().getCoaCode(),
    								glForexLedger.getFrlAdCompany());
    		       			
    		       		} catch (FinderException ex) {
    		       			
    		       		}
    		       		
    		       		Iterator itrFrl = glForexLedgers.iterator();
    		       		
    		       		while (itrFrl.hasNext()) {
    		       			
    		       			glForexLedger = (LocalGlForexLedger) itrFrl.next();
    		       			FRL_AMNT = apDistributionRecord.getDrAmount();
    		       			
    		       			if(glJournalLine.getGlChartOfAccount().getCoaAccountType().equalsIgnoreCase("ASSET"))
    		       				FRL_AMNT = (glJournalLine.getJlDebit() == EJBCommon.TRUE  ? FRL_AMNT :
    		       					(- 1 * FRL_AMNT));
    		       			else
    		       				FRL_AMNT = (glJournalLine.getJlDebit() == EJBCommon.TRUE  ? (- 1 * FRL_AMNT) :
    		       					FRL_AMNT);
    		       			
    		       			glForexLedger.setFrlBalance(glForexLedger.getFrlBalance() + FRL_AMNT);
    		       			
    		       		}
    		       		
    		       	}
        			
        		}
        		
        		if (glOffsetJournalLine != null) {
        			
        			//glJournal.addGlJournalLine(glOffsetJournalLine);
        			glOffsetJournalLine.setGlJournal(glJournal);
        			
        		}		
        		
        		// post journal to gl
        		
        		Collection glJournalLines = glJournal.getGlJournalLines();
        		
        		Iterator i = glJournalLines.iterator();
        		
        		while (i.hasNext()) {
        			
        			LocalGlJournalLine glJournalLine = (LocalGlJournalLine)i.next();	
        			
        			// post current to current acv
        			
        			this.postToGl(glAccountingCalendarValue,
        					glJournalLine.getGlChartOfAccount(),
							true, glJournalLine.getJlDebit(), glJournalLine.getJlAmount(), AD_CMPNY);
        			
        			
        			// post to subsequent acvs (propagate)
        			
        			Collection glSubsequentAccountingCalendarValues = 
        				glAccountingCalendarValueHome.findSubsequentAcvByAcCodeAndAcvPeriodNumber( 
        						glJournalSetOfBook.getGlAccountingCalendar().getAcCode(),
								glAccountingCalendarValue.getAcvPeriodNumber(), AD_CMPNY);     
        			
        			Iterator acvsIter = glSubsequentAccountingCalendarValues.iterator();
        			
        			while (acvsIter.hasNext()) {
        				
        				LocalGlAccountingCalendarValue glSubsequentAccountingCalendarValue = 
        					(LocalGlAccountingCalendarValue)acvsIter.next();
        				
        				this.postToGl(glSubsequentAccountingCalendarValue,
        						glJournalLine.getGlChartOfAccount(),
								false, glJournalLine.getJlDebit(), glJournalLine.getJlAmount(), AD_CMPNY);
        				
        			}
        			
        			// post to subsequent years if necessary
        			
        			Collection glSubsequentSetOfBooks = glSetOfBookHome.findSubsequentSobByAcYear(glJournalSetOfBook.getGlAccountingCalendar().getAcYear(), AD_CMPNY);
        			
        			if (!glSubsequentSetOfBooks.isEmpty() && glJournalSetOfBook.getSobYearEndClosed() == 1) {
        				
        				adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);		  	  	
        				LocalGlChartOfAccount glRetainedEarningsAccount = glChartOfAccountHome.findByCoaAccountNumber(adCompany.getCmpRetainedEarnings(), AD_CMPNY); 	  	
        				
        				Iterator sobIter = glSubsequentSetOfBooks.iterator();
        				
        				while (sobIter.hasNext()) {
        					
        					LocalGlSetOfBook glSubsequentSetOfBook = (LocalGlSetOfBook)sobIter.next();
        					
        					String ACCOUNT_TYPE = glJournalLine.getGlChartOfAccount().getCoaAccountType();
        					
        					// post to subsequent acvs of subsequent set of book(propagate)
        					
        					Collection glAccountingCalendarValues = 
        						glAccountingCalendarValueHome.findByAcCode(glSubsequentSetOfBook.getGlAccountingCalendar().getAcCode(), AD_CMPNY);     
        					
        					Iterator acvIter = glAccountingCalendarValues.iterator();
        					
        					while (acvIter.hasNext()) {
        						
        						LocalGlAccountingCalendarValue glSubsequentAccountingCalendarValue = 
        							(LocalGlAccountingCalendarValue)acvIter.next();
        						
        						if (ACCOUNT_TYPE.equals("ASSET") || ACCOUNT_TYPE.equals("LIABILITY") ||
        								ACCOUNT_TYPE.equals("OWNERS EQUITY")) {
        							
        							this.postToGl(glSubsequentAccountingCalendarValue,
        									glJournalLine.getGlChartOfAccount(),
											false, glJournalLine.getJlDebit(), glJournalLine.getJlAmount(), AD_CMPNY);
        							
        						} else { // revenue & expense
        							
        							this.postToGl(glSubsequentAccountingCalendarValue,
        									glRetainedEarningsAccount,
											false, glJournalLine.getJlDebit(), glJournalLine.getJlAmount(), AD_CMPNY);					        
        							
        						}
        						
        					}
        					
        					if (glSubsequentSetOfBook.getSobYearEndClosed() == 0) break;
        					
        				}
        				
        			}
        			
        		}	   			   
        		
        	}
        	
        } catch (GlJREffectiveDateNoPeriodExistException ex) {
        	
        	getSessionContext().setRollbackOnly();
        	throw ex;
        	
        } catch (GlJREffectiveDatePeriodClosedException ex) {
        	
        	getSessionContext().setRollbackOnly();
        	throw ex;
        	
        } catch (GlobalJournalNotBalanceException ex) {
        	
        	getSessionContext().setRollbackOnly();
        	throw ex;        	
        	        
        } catch (GlobalRecordAlreadyDeletedException ex) {
        	
        	getSessionContext().setRollbackOnly();
        	throw ex;        	        	
        	
        } catch (GlobalTransactionAlreadyPostedException ex) {
        	
        	getSessionContext().setRollbackOnly();
        	throw ex;
        	
        } catch (GlobalTransactionAlreadyVoidPostedException ex) {
        	
        	getSessionContext().setRollbackOnly();
        	throw ex;
        
      
        
    	} catch (Exception ex) {
    		
    		Debug.printStackTrace(ex);
    		getSessionContext().setRollbackOnly();
    		throw new EJBException(ex.getMessage());
    		
    	}
    	
    }
    
    private double getInvFifoCost(Date CST_DT, Integer IL_CODE, double CST_QTY, double CST_COST, 
   		 boolean isAdjustFifo, Integer AD_BRNCH, Integer AD_CMPNY)
	 {
	  	 
		 LocalInvCostingHome invCostingHome = null;
	  	 LocalInvItemLocationHome invItemLocationHome = null;
	       
	     // Initialize EJB Home
	        
	     try {
	         
	    	 invCostingHome = (LocalInvCostingHome)EJBHomeFactory.
	    	 	lookUpLocalHome(LocalInvCostingHome.JNDI_NAME, LocalInvCostingHome.class);
	    	 invItemLocationHome = (LocalInvItemLocationHome)EJBHomeFactory.
	    	 	lookUpLocalHome(LocalInvItemLocationHome.JNDI_NAME, LocalInvItemLocationHome.class);
	     } 
	     catch (NamingException ex) {
	            
	    	 throw new EJBException(ex.getMessage());
	     }
	    	
		try {
			
			Collection invFifoCostings = invCostingHome.findFifoRemainingQuantityByLessThanOrEqualCstDateAndIlCodeAndBrCode(CST_DT, IL_CODE, AD_BRNCH, AD_CMPNY);
			
			if (invFifoCostings.size() > 0) {
				
				Iterator x = invFifoCostings.iterator();
			
	  			if (isAdjustFifo) {
	  				
	  				//executed during POST transaction
	  				
	  				double totalCost = 0d;
	  				double cost;
	  				
	  				if(CST_QTY < 0) {
	  					
	  					//for negative quantities
	 	  				double neededQty = -(CST_QTY);
	 	  				
	 	  				while(x.hasNext() && neededQty != 0) {
	 	 	  				
	 	 	  				LocalInvCosting invFifoCosting = (LocalInvCosting)x.next();
	
	 		 	  			if (invFifoCosting.getApPurchaseOrderLine() != null || invFifoCosting.getApVoucherLineItem() != null) {  				
	 		 	  				cost = invFifoCosting.getCstItemCost() / invFifoCosting.getCstQuantityReceived();  				
	 		 	  			} else if(invFifoCosting.getArInvoiceLineItem() != null) {
	 		 	  				cost = invFifoCosting.getCstCostOfSales() / invFifoCosting.getCstQuantitySold();
	 		 	  			} else if (invFifoCosting.getInvBuildUnbuildAssemblyLine() != null) {  				
	 		 	  				cost = invFifoCosting.getCstAssemblyCost() / invFifoCosting.getCstAssemblyQuantity();
	 		 	  			} else {
	 		 	  				cost = invFifoCosting.getCstAdjustCost() /invFifoCosting.getCstAdjustQuantity();
	 		 	  			}
	
	 	  					if(neededQty <= invFifoCosting.getCstRemainingLifoQuantity()) {
	 	  						
	 			  				invFifoCosting.setCstRemainingLifoQuantity(invFifoCosting.getCstRemainingLifoQuantity() - neededQty);
	 			  				totalCost += (neededQty * cost);
	 			  				neededQty = 0d;	 			  				 
	 	  					} else {
	 	  						
	 	  						neededQty -= invFifoCosting.getCstRemainingLifoQuantity();
	 	  						totalCost += (invFifoCosting.getCstRemainingLifoQuantity() * cost);
	 	  						invFifoCosting.setCstRemainingLifoQuantity(0);
	 	  					}
	 	  				}
	 	  				
	 	  				//if needed qty is not yet satisfied but no more quantities to fetch, get the default cost
	 	  				if(neededQty != 0) {
	 	  					
	 	  					LocalInvItemLocation invItemLocation = invItemLocationHome.findByPrimaryKey(IL_CODE);
	 	  					totalCost += (neededQty * invItemLocation.getInvItem().getIiUnitCost());
	 	  				}
	 	  				
	 	  				cost = totalCost / -CST_QTY;
	  				} 
	  				
	  				else {
	  					
	  					//for positive quantities
	  					cost = CST_COST;
	  				}
	  				return cost;
	  			}
	  			
	  			else {
	  				
	  				//executed during ENTRY transaction
	  				
	  				LocalInvCosting invFifoCosting = (LocalInvCosting)x.next();
	  				
	 	  			if (invFifoCosting.getApPurchaseOrderLine() != null || invFifoCosting.getApVoucherLineItem() != null) {
	 	  				return EJBCommon.roundIt(invFifoCosting.getCstItemCost() / invFifoCosting.getCstQuantityReceived(), this.getGlFcPrecisionUnit(AD_CMPNY));
	 	  			} else if(invFifoCosting.getArInvoiceLineItem() != null) {
		 	  			return  EJBCommon.roundIt(invFifoCosting.getCstCostOfSales() / invFifoCosting.getCstQuantitySold(), this.getGlFcPrecisionUnit(AD_CMPNY));
	 	  			} else if (invFifoCosting.getInvBuildUnbuildAssemblyLine() != null) {
	 	  				return EJBCommon.roundIt(invFifoCosting.getCstAssemblyCost() / invFifoCosting.getCstAssemblyQuantity(), this.getGlFcPrecisionUnit(AD_CMPNY));
	 	  			} else {
	 	  				return EJBCommon.roundIt(invFifoCosting.getCstAdjustCost() /invFifoCosting.getCstAdjustQuantity(), this.getGlFcPrecisionUnit(AD_CMPNY));
	 	  			}
	  			}
			} 
			else {
				
				//most applicable in 1st entries of data
				LocalInvItemLocation invItemLocation = invItemLocationHome.findByPrimaryKey(IL_CODE);
				return invItemLocation.getInvItem().getIiUnitCost();
			}
				
		}
		catch (Exception ex) {
			Debug.printStackTrace(ex);
		    throw new EJBException(ex.getMessage());
		}
	}     

    
    private void post(Date CHK_DT, double CHK_AMNT, LocalApSupplier apSupplier, Integer AD_CMPNY) {
    	
       Debug.print("ApApprovalControllerBean post");
    	
       LocalApSupplierBalanceHome apSupplierBalanceHome = null;
        
       // Initialize EJB Home
        
       try {
                        
           apSupplierBalanceHome = (LocalApSupplierBalanceHome)EJBHomeFactory.
               lookUpLocalHome(LocalApSupplierBalanceHome.JNDI_NAME, LocalApSupplierBalanceHome.class);              
            
       } catch (NamingException ex) {
        	
           getSessionContext().setRollbackOnly();            
           throw new EJBException(ex.getMessage());
            
       }
        
       try {
        		        	
	       // find supplier balance before or equal voucher date
	    	
	       Collection apSupplierBalances = apSupplierBalanceHome.findByBeforeOrEqualVouDateAndSplSupplierCode(CHK_DT, apSupplier.getSplSupplierCode(), AD_CMPNY);
	    	
	       if (!apSupplierBalances.isEmpty()) {
	    		
	    	   // get last voucher
	    	    
	    	   ArrayList apSupplierBalanceList = new ArrayList(apSupplierBalances);
	    	    	    	    
	    	   LocalApSupplierBalance apSupplierBalance = (LocalApSupplierBalance)apSupplierBalanceList.get(apSupplierBalanceList.size() - 1);
	    	    
	    	   if (apSupplierBalance.getSbDate().before(CHK_DT)) {
	    	    		    	    	
	    	       // create new balance
	    	    	
	    	       LocalApSupplierBalance apNewSupplierBalance = apSupplierBalanceHome.create(
		    	       CHK_DT, apSupplierBalance.getSbBalance() + CHK_AMNT, AD_CMPNY);

		           //apSupplier.addApSupplierBalance(apNewSupplierBalance);
		           apNewSupplierBalance.setApSupplier(apSupplier);
		           		        	
	    	   } else { // equals to voucher date
	    	   
	    	       apSupplierBalance.setSbBalance(apSupplierBalance.getSbBalance() + CHK_AMNT);
	    	   
	    	   } 
	    	    
	    	} else {        	
	    	
	    	    // create new balance
	    	    
		    	LocalApSupplierBalance apNewSupplierBalance = apSupplierBalanceHome.create(
		    		CHK_DT, CHK_AMNT, AD_CMPNY);

		        //apSupplier.addApSupplierBalance(apNewSupplierBalance);
		        apNewSupplierBalance.setApSupplier(apSupplier);
		        		        		        
	     	}
	     	
	     	// propagate to subsequent balances if necessary
	     	
	     	apSupplierBalances = apSupplierBalanceHome.findByAfterVouDateAndSplSupplierCode(CHK_DT, apSupplier.getSplSupplierCode(), AD_CMPNY);
	     	
	     	Iterator i = apSupplierBalances.iterator();
	     	
	     	while (i.hasNext()) {
	     		
	     		LocalApSupplierBalance apSupplierBalance = (LocalApSupplierBalance)i.next();
	     		
	     		apSupplierBalance.setSbBalance(apSupplierBalance.getSbBalance() + CHK_AMNT);
	     		
	     	}
	     		     	        	
        } catch (Exception ex) {
        	
        	Debug.printStackTrace(ex);
        	getSessionContext().setRollbackOnly();            
            throw new EJBException(ex.getMessage());
            
        }
    	
	}
    
   private void postToGl(LocalGlAccountingCalendarValue glAccountingCalendarValue, 
      LocalGlChartOfAccount glChartOfAccount, 
      boolean isCurrentAcv, byte isDebit, double JL_AMNT, Integer AD_CMPNY) {
      	
      Debug.print("ApApprovalControllerBean postToGl");
      
      LocalGlChartOfAccountBalanceHome glChartOfAccountBalanceHome = null;
      LocalAdCompanyHome adCompanyHome = null;
      
     
       // Initialize EJB Home
        
       try {
            
           glChartOfAccountBalanceHome = (LocalGlChartOfAccountBalanceHome)EJBHomeFactory.
              lookUpLocalHome(LocalGlChartOfAccountBalanceHome.JNDI_NAME, LocalGlChartOfAccountBalanceHome.class);                   
           adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
              lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);                   
              
            
       } catch (NamingException ex) {
            
           throw new EJBException(ex.getMessage());
            
       }
       
       try {          
               
               LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);                       
       	   
	       	   LocalGlChartOfAccountBalance glChartOfAccountBalance = 
		            glChartOfAccountBalanceHome.findByAcvCodeAndCoaCode(
		               	  glAccountingCalendarValue.getAcvCode(),
		               	  glChartOfAccount.getCoaCode(), AD_CMPNY);
	               	  
	           String ACCOUNT_TYPE = glChartOfAccount.getCoaAccountType();
	           short FC_EXTNDD_PRCSN = adCompany.getGlFunctionalCurrency().getFcPrecision();
	           
	           
	               	  
	           if (((ACCOUNT_TYPE.equals("ASSET") || ACCOUNT_TYPE.equals("EXPENSE")) &&
				    isDebit == EJBCommon.TRUE) ||
				    (!ACCOUNT_TYPE.equals("ASSET") && !ACCOUNT_TYPE.equals("EXPENSE") &&
				    isDebit == EJBCommon.FALSE)) {				    
				    			
 			        glChartOfAccountBalance.setCoabEndingBalance(
				       EJBCommon.roundIt(glChartOfAccountBalance.getCoabEndingBalance() + JL_AMNT, FC_EXTNDD_PRCSN)); 					   
				       
				    if (!isCurrentAcv) {
				    	
				    	glChartOfAccountBalance.setCoabBeginningBalance(
				       		EJBCommon.roundIt(glChartOfAccountBalance.getCoabBeginningBalance() + JL_AMNT, FC_EXTNDD_PRCSN)); 					   
				    	
					}
				    
		
			  } else {
	
				    glChartOfAccountBalance.setCoabEndingBalance(
				       EJBCommon.roundIt(glChartOfAccountBalance.getCoabEndingBalance() - JL_AMNT, FC_EXTNDD_PRCSN));
				       
				    if (!isCurrentAcv) {
				    	
				    	glChartOfAccountBalance.setCoabBeginningBalance(
				       		EJBCommon.roundIt(glChartOfAccountBalance.getCoabBeginningBalance() - JL_AMNT, FC_EXTNDD_PRCSN)); 					   
				    	
					}
	
		 	  }
		 	  
		 	  if (isCurrentAcv) { 
		 	 
			 	 if (isDebit == EJBCommon.TRUE) {
			 	 	
		 			glChartOfAccountBalance.setCoabTotalDebit(
		 				EJBCommon.roundIt(glChartOfAccountBalance.getCoabTotalDebit() + JL_AMNT, FC_EXTNDD_PRCSN));	 			
		 				
		 		 } else {
		 		 	
		 			glChartOfAccountBalance.setCoabTotalCredit(
		 				EJBCommon.roundIt(glChartOfAccountBalance.getCoabTotalCredit() + JL_AMNT, FC_EXTNDD_PRCSN));	 			
		 		 }       	   
		 		 
		 	}
       	
       } catch (Exception ex) {
       	
       	   Debug.printStackTrace(ex);
       	   throw new EJBException(ex.getMessage());
       	
       }
      
      
   }
   
   private void postToInvVou(LocalApVoucherLineItem apVoucherLineItem, Date CST_DT, double CST_QTY_RCVD, double CST_ITM_CST,
   		double CST_RMNNG_QTY, double CST_RMNNG_VL, double CST_VRNC_VL, String USR_NM, Integer AD_BRNCH, Integer AD_CMPNY) throws
   		AdPRFCoaGlVarianceAccountNotFoundException {
    
    	Debug.print("ApApprovalControllerBean postToInv");
    	
    	LocalInvCostingHome invCostingHome = null;
    	LocalAdPreferenceHome adPreferenceHome = null;
    	LocalAdCompanyHome adCompanyHome = null;
    	LocalInvAdjustmentLineHome invAdjustmentLineHome = null;
    	
        // Initialize EJB Home
        
        try {
            
          invCostingHome = (LocalInvCostingHome)EJBHomeFactory.
              lookUpLocalHome(LocalInvCostingHome.JNDI_NAME, LocalInvCostingHome.class);
          adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
              lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);
          adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
              lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);              
          invAdjustmentLineHome = (LocalInvAdjustmentLineHome)EJBHomeFactory.
		      lookUpLocalHome(LocalInvAdjustmentLineHome.JNDI_NAME, LocalInvAdjustmentLineHome.class);              

        } catch (NamingException ex) {
            
          throw new EJBException(ex.getMessage());
            
        }
      
        try {
        
           LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);
           LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
           LocalInvItemLocation invItemLocation = apVoucherLineItem.getInvItemLocation();
           int CST_LN_NMBR = 0;
           
           CST_QTY_RCVD = EJBCommon.roundIt(CST_QTY_RCVD, adPreference.getPrfInvQuantityPrecisionUnit());
           CST_ITM_CST = EJBCommon.roundIt(CST_ITM_CST, adCompany.getGlFunctionalCurrency().getFcPrecision());
           CST_RMNNG_QTY = EJBCommon.roundIt(CST_RMNNG_QTY, adPreference.getPrfInvQuantityPrecisionUnit());
           CST_RMNNG_VL = EJBCommon.roundIt(CST_RMNNG_VL, adCompany.getGlFunctionalCurrency().getFcPrecision());
           
           try {
           
           	   // generate line number
           
               LocalInvCosting invCurrentCosting = invCostingHome.getByMaxCstLineNumberAndCstDateToLongAndIiNameAndLocName(CST_DT.getTime(), invItemLocation.getInvItem().getIiName(), invItemLocation.getInvLocation().getLocName(), AD_BRNCH, AD_CMPNY);
               CST_LN_NMBR = invCurrentCosting.getCstLineNumber() + 1;
           
           } catch (FinderException ex) {
           
           	   CST_LN_NMBR = 1;
           
           }
           
           //void subsequent cost variance adjustments
           Collection invAdjustmentLines =invAdjustmentLineHome.findUnvoidAndIsCostVarianceGreaterThanAdjDateAndIlCodeAndBrCode(
           		CST_DT, invItemLocation.getIlCode(), AD_BRNCH, AD_CMPNY);
           Iterator i = invAdjustmentLines.iterator();
           
           while (i.hasNext()){
           	
           	LocalInvAdjustmentLine invAdjustmentLine = (LocalInvAdjustmentLine) i.next();
           	this.voidInvAdjustment(invAdjustmentLine.getInvAdjustment(), AD_BRNCH, AD_CMPNY);
           	
           }

           String prevExpiryDates = "";
           String miscListPrpgt ="";
           double qtyPrpgt = 0;
           try {           
        	   LocalInvCosting prevCst = invCostingHome.getByMaxCstDateToLongAndMaxCstLineNumberAndLessThanEqualCstDateAndRemainingQuantityNotEqualToZeroAndIlCode(
        			   CST_DT, invItemLocation.getIlCode(), AD_BRNCH, AD_CMPNY);
        	   
        	   prevExpiryDates = prevCst.getCstExpiryDate();
        	   qtyPrpgt = prevCst.getCstRemainingQuantity();

        	   if (prevExpiryDates==null){
        		   prevExpiryDates="";
        	   }

           }catch (Exception ex){
        	   
           }
           
           // create costing
           LocalInvCosting invCosting = invCostingHome.create(CST_DT, CST_DT.getTime(), CST_LN_NMBR, CST_QTY_RCVD, CST_ITM_CST, 0d, 0d, 0d, 0d, 0d, 0d, CST_RMNNG_QTY, CST_RMNNG_VL, 0d, 0d, 0d, AD_BRNCH, AD_CMPNY);
           //invItemLocation.addInvCosting(invCosting);
           invCosting.setInvItemLocation(invItemLocation);
           invCosting.setApVoucherLineItem(apVoucherLineItem);
           
//         Get Latest Expiry Dates           

           if(prevExpiryDates!=null && prevExpiryDates!="" && prevExpiryDates!=""){
			   System.out.println("apPurchaseOrderLine.getPlMisc(): "+apVoucherLineItem.getVliMisc());
			   if(apVoucherLineItem.getVliMisc()!=null && apVoucherLineItem.getVliMisc()!="" && apVoucherLineItem.getVliMisc().length()!=0){
				   int qty2Prpgt = Integer.parseInt(this.getQuantityExpiryDates(apVoucherLineItem.getVliMisc()));
				   String miscList2Prpgt = this.propagateExpiryDates(apVoucherLineItem.getVliMisc(), qty2Prpgt);
				   prevExpiryDates = prevExpiryDates.substring(1);
				   String propagateMiscPrpgt = miscList2Prpgt + prevExpiryDates;
				   
				   invCosting.setCstExpiryDate(propagateMiscPrpgt);
			   }else{
				   
				   invCosting.setCstExpiryDate(prevExpiryDates);
			   }
			   
			   
           }else{
        	   if(apVoucherLineItem.getVliMisc()!=null && apVoucherLineItem.getVliMisc()!="" && apVoucherLineItem.getVliMisc().length()!=0){
        		   int initialQty = Integer.parseInt(this.getQuantityExpiryDates(apVoucherLineItem.getVliMisc()));
    			   String initialPrpgt = this.propagateExpiryDates(apVoucherLineItem.getVliMisc(), initialQty);
    			   
            	   invCosting.setCstExpiryDate(initialPrpgt);
        	   }else{
        		   invCosting.setCstExpiryDate(prevExpiryDates);
        	   }
        	   
           }
           
			// if cost variance is not 0, generate cost variance for the transaction 
			if(CST_VRNC_VL != 0) {

				if(apVoucherLineItem.getApVoucher() != null)
					this.generateCostVariance(invCosting.getInvItemLocation(), CST_VRNC_VL,
							"APVOU" + apVoucherLineItem.getApVoucher().getVouDocumentNumber(),
							apVoucherLineItem.getApVoucher().getVouDescription(),
							apVoucherLineItem.getApVoucher().getVouDate(), USR_NM, AD_BRNCH, AD_CMPNY);
				else if (apVoucherLineItem.getApCheck()!= null)
					this.generateCostVariance(invCosting.getInvItemLocation(), CST_VRNC_VL,
							"APCHK" + apVoucherLineItem.getApCheck().getChkDocumentNumber(),
							apVoucherLineItem.getApCheck().getChkDescription(),
							apVoucherLineItem.getApCheck().getChkDate(), USR_NM, AD_BRNCH, AD_CMPNY);

				
			}

           // propagate balance if necessary           
           Collection invCostings = invCostingHome.findByGreaterThanCstDateAndIiNameAndLocName(CST_DT, invItemLocation.getInvItem().getIiName(), invItemLocation.getInvLocation().getLocName(), AD_BRNCH, AD_CMPNY);
           
           i = invCostings.iterator();
           
           String miscList = "";
           if(apVoucherLineItem.getVliMisc()!=null && apVoucherLineItem.getVliMisc()!="" && apVoucherLineItem.getVliMisc().length()!=0){
        	   double qty = Double.parseDouble(this.getQuantityExpiryDates(apVoucherLineItem.getVliMisc()));
    		   miscList = this.propagateExpiryDates(apVoucherLineItem.getVliMisc(), qty);
           }
           

		   System.out.println("miscList Propagate:" + miscList);
           
           while (i.hasNext()) {
           
               LocalInvCosting invPropagatedCosting = (LocalInvCosting)i.next();
               
               invPropagatedCosting.setCstRemainingQuantity(invPropagatedCosting.getCstRemainingQuantity() + CST_QTY_RCVD);
               invPropagatedCosting.setCstRemainingValue(invPropagatedCosting.getCstRemainingValue() + CST_ITM_CST);
               if(apVoucherLineItem.getVliMisc()!=null && apVoucherLineItem.getVliMisc()!="" && apVoucherLineItem.getVliMisc().length()!=0){
                   miscList = miscList.substring(1);
//                 String miscList2 = propagateMisc;//this.propagateExpiryDates(invCosting.getCstExpiryDate(), qty);
     			   System.out.println("invPropagatedCosting.getCstExpiryDate() : " + invPropagatedCosting.getCstExpiryDate());
     			   String propagateMisc = invPropagatedCosting.getCstExpiryDate() + miscList;
                   invPropagatedCosting.setCstExpiryDate(propagateMisc);
               }else{
            	   invPropagatedCosting.setCstExpiryDate(prevExpiryDates);
               }

           }                           

           // regenerate cost variance
           this.regenerateCostVariance(invCostings, invCosting, AD_BRNCH, AD_CMPNY);

        } catch (AdPRFCoaGlVarianceAccountNotFoundException ex){
        	
        	throw ex;

        } catch (Exception ex) {
      	
      	   Debug.printStackTrace(ex);
      	   getSessionContext().setRollbackOnly();
           throw new EJBException(ex.getMessage());
         
        }
        	    
    }
   
   public String getQuantityExpiryDates(String qntty){
	   	String separator = "$";

	   	// Remove first $ character
	   	qntty = qntty.substring(1);

	   	// Counter
	   	int start = 0;
	   	int nextIndex = qntty.indexOf(separator, start);
	   	int length = nextIndex - start;	
	   	String y;
	   	y = (qntty.substring(start, start + length));
	   	System.out.println("Y " + y);
	   	
	   	return y;
	   }
	   
	   public String propagateExpiryDates(String misc, double qty) throws Exception {
	   	//ActionErrors errors = new ActionErrors();
	   	
	   	Debug.print("ApReceivingItemControllerBean getExpiryDates");

	   	String separator = "$";

	   	// Remove first $ character
	   	misc = misc.substring(1);

	   	// Counter
	   	int start = 0;
	   	int nextIndex = misc.indexOf(separator, start);
	   	int length = nextIndex - start;	
	   	
	   	System.out.println("qty" + qty);
	   	String miscList = new String();
			
			for(int x=0; x<qty; x++) {
				
				// Date
				start = nextIndex + 1;
				nextIndex = misc.indexOf(separator, start);
				length = nextIndex - start;
				/*SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
		        sdf.setLenient(false);*/
		        try {	        	
		        	 
		        	miscList = miscList + "$" +(misc.substring(start, start + length));	        	
		        } catch (Exception ex) {
		        	
		        	throw ex;
		        }
					
					
			}	
			
			miscList = miscList+"$";
			System.out.println("miscList :" + miscList);
			return (miscList);
	   }
    
    private double convertByUomFromAndItemAndQuantity(LocalInvUnitOfMeasure invFromUnitOfMeasure, LocalInvItem invItem, double QTY_RCVD, Integer AD_CMPNY) {
			
		Debug.print("ApApprovalControllerBean convertByUomFromAndItemAndQuantity");		        
        
		LocalAdPreferenceHome adPreferenceHome = null;
		LocalInvUnitOfMeasureConversionHome invUnitOfMeasureConversionHome = null;
                
        // Initialize EJB Home
        
        try {
            
            adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
                 lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);
            invUnitOfMeasureConversionHome = (LocalInvUnitOfMeasureConversionHome)EJBHomeFactory.
                 lookUpLocalHome(LocalInvUnitOfMeasureConversionHome.JNDI_NAME, LocalInvUnitOfMeasureConversionHome.class);
                        
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
                
        try {
        
            LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);               	          	        	
        	        	
            LocalInvUnitOfMeasureConversion invUnitOfMeasureConversion = invUnitOfMeasureConversionHome.findUmcByIiNameAndUomName(invItem.getIiName(), invFromUnitOfMeasure.getUomName(), AD_CMPNY);
            LocalInvUnitOfMeasureConversion invDefaultUomConversion = invUnitOfMeasureConversionHome.findUmcByIiNameAndUomName(invItem.getIiName(), invItem.getInvUnitOfMeasure().getUomName(), AD_CMPNY);
                        
        	return EJBCommon.roundIt(QTY_RCVD * invDefaultUomConversion.getUmcConversionFactor() / invUnitOfMeasureConversion.getUmcConversionFactor(), adPreference.getPrfInvQuantityPrecisionUnit());       	        		        		       	
        						    		        		
        } catch (Exception ex) {
        	
        	Debug.printStackTrace(ex);
        	getSessionContext().setRollbackOnly();
        	throw new EJBException(ex.getMessage());
        	
        }
		
	}
    
    private double getFrRateByFrNameAndFrDate(String FC_NM, Date CONVERSION_DATE, Integer AD_CMPNY) 
    throws GlobalConversionDateNotExistException {
    	
 		Debug.print("ApApprovalControllerBean getFrRateByFrNameAndFrDate");
 		
 		LocalGlFunctionalCurrencyRateHome glFunctionalCurrencyRateHome = null; 
 		LocalGlFunctionalCurrencyHome glFunctionalCurrencyHome = null;
 		LocalAdCompanyHome adCompanyHome = null;
 		
 		// Initialize EJB Home
 		
 		try {
 			
 			glFunctionalCurrencyRateHome = (LocalGlFunctionalCurrencyRateHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlFunctionalCurrencyRateHome.JNDI_NAME, LocalGlFunctionalCurrencyRateHome.class);
 			glFunctionalCurrencyHome = (LocalGlFunctionalCurrencyHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlFunctionalCurrencyHome.JNDI_NAME, LocalGlFunctionalCurrencyHome.class);
 			adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
 			
 		} catch (NamingException ex) {
 			
 			throw new EJBException(ex.getMessage());
 			
 		}
 		
 		try {
 			
 			LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
 			LocalGlFunctionalCurrency glFunctionalCurrency = glFunctionalCurrencyHome.findByFcName(FC_NM, AD_CMPNY);
 			
 			double CONVERSION_RATE = 1;
 			
 			// Get functional currency rate
 			
 			if (!FC_NM.equals("USD")) {
 				
 				LocalGlFunctionalCurrencyRate glFunctionalCurrencyRate =
 					glFunctionalCurrencyRateHome.findByFcCodeAndDate(glFunctionalCurrency.getFcCode(),
 							CONVERSION_DATE, AD_CMPNY);
 				
 				CONVERSION_RATE = glFunctionalCurrencyRate.getFrXToUsd();
 				
 			}
 			
 			// Get set of book functional currency rate if necessary
 			
 			if (!adCompany.getGlFunctionalCurrency().getFcName().equals("USD")) {
 				
 				LocalGlFunctionalCurrencyRate glCompanyFunctionalCurrencyRate =
 					glFunctionalCurrencyRateHome.findByFcCodeAndDate(adCompany.getGlFunctionalCurrency(). getFcCode(),
 							CONVERSION_DATE, AD_CMPNY);
 				
 				CONVERSION_RATE = CONVERSION_RATE / glCompanyFunctionalCurrencyRate.getFrXToUsd();
 				
 			}
 			
 			return CONVERSION_RATE;
 			
 		} catch (FinderException ex) {	
 			
 			getSessionContext().setRollbackOnly();
 			throw new GlobalConversionDateNotExistException();  
 			
 		} catch (Exception ex) {
 			
 			Debug.printStackTrace(ex);
 			throw new EJBException(ex.getMessage());
 			
 		}
 		
 	}
    
    private void voidInvAdjustment(LocalInvAdjustment invAdjustment, Integer AD_BRNCH, Integer AD_CMPNY){
    	
    	Debug.print("ApApprovalControllerBean voidInvAdjustment");
    	
    	try{

    			Collection invDistributionRecords = invAdjustment.getInvDistributionRecords();
    			ArrayList list = new ArrayList();
    			
    			Iterator i = invDistributionRecords.iterator();
    			
    			while (i.hasNext()) {
    				
    				LocalInvDistributionRecord invDistributionRecord = (LocalInvDistributionRecord)i.next();
    				
    				list.add(invDistributionRecord);
    				
    			}
    			
    			i = list.iterator();
    			
    			while (i.hasNext()) {
    				
    				LocalInvDistributionRecord invDistributionRecord = (LocalInvDistributionRecord)i.next();    	    			    			
    				
    				this.addInvDrEntry(invAdjustment.getInvDrNextLine(), invDistributionRecord.getDrClass(), 
    						invDistributionRecord.getDrDebit() == EJBCommon.TRUE ? EJBCommon.FALSE : EJBCommon.TRUE,
    								invDistributionRecord.getDrAmount(), EJBCommon.TRUE,
									invDistributionRecord.getInvChartOfAccount().getCoaCode(), invAdjustment, AD_BRNCH, AD_CMPNY);
    				
    			}
    			
    			Collection invAjustmentLines = invAdjustment.getInvAdjustmentLines();
    			i = invAjustmentLines.iterator();
    			list.clear();

    			while (i.hasNext()) {
    				
    				LocalInvAdjustmentLine invAdjustmentLine = (LocalInvAdjustmentLine)i.next();
    				
    				list.add(invAdjustmentLine);
    				
    			}
    			
    			i = list.iterator();
    			
    			while (i.hasNext()) {
    				
    				LocalInvAdjustmentLine invAdjustmentLine = (LocalInvAdjustmentLine)i.next();    	    			    			
    				
    				this.addInvAlEntry(invAdjustmentLine.getInvItemLocation(),
    						invAdjustment, (invAdjustmentLine.getAlUnitCost()) * - 1, EJBCommon.TRUE, AD_CMPNY);

    			}
    			
    			invAdjustment.setAdjVoid(EJBCommon.TRUE);
    			
    			this.executeInvAdjPost(invAdjustment.getAdjCode(), invAdjustment.getAdjLastModifiedBy(), AD_BRNCH, AD_CMPNY);

    	} catch (Exception ex) {
    		
    		Debug.printStackTrace(ex);
    		getSessionContext().setRollbackOnly();
    		throw new EJBException(ex.getMessage());
    		
    	}
    	
    }

    private void generateCostVariance(LocalInvItemLocation invItemLocation, double CST_VRNC_VL, String ADJ_RFRNC_NMBR,
    		String ADJ_DSCRPTN, Date ADJ_DT, String USR_NM, Integer AD_BRNCH, Integer AD_CMPNY) throws
			AdPRFCoaGlVarianceAccountNotFoundException {
    	/*
    	Debug.print("ApApprovalControllerBean generateCostVariance");
    	
    	LocalAdPreferenceHome adPreferenceHome = null;
    	LocalGlChartOfAccountHome glChartOfAccountHome = null;
    	LocalAdBranchItemLocationHome adBranchItemLocationHome = null;
    	
    	// Initialize EJB Home
    	
    	try {
    		
    		adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);       
    		glChartOfAccountHome = (LocalGlChartOfAccountHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlChartOfAccountHome.JNDI_NAME, LocalGlChartOfAccountHome.class);
    		adBranchItemLocationHome = (LocalAdBranchItemLocationHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdBranchItemLocationHome.JNDI_NAME, LocalAdBranchItemLocationHome.class);       
    		
    		
    	} catch (NamingException ex) {
    		
    		throw new EJBException(ex.getMessage());
    		
    	}
    	
    	try{ 

    		LocalInvAdjustment newInvAdjustment = this.saveInvAdjustment(ADJ_RFRNC_NMBR, ADJ_DSCRPTN, ADJ_DT, USR_NM, AD_BRNCH,
    				AD_CMPNY);
    		LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);
    		LocalGlChartOfAccount glCoaVarianceAccount = null;

    		if(adPreference.getPrfInvGlCoaVarianceAccount() == null)
    			throw new AdPRFCoaGlVarianceAccountNotFoundException();
    		
    		try{
    			
    			glCoaVarianceAccount = glChartOfAccountHome.findByPrimaryKey(adPreference.getPrfInvGlCoaVarianceAccount());
    			//glCoaVarianceAccount.addInvAdjustment(newInvAdjustment);
    			newInvAdjustment.setGlChartOfAccount(glCoaVarianceAccount);
    			
    		} catch (FinderException ex) {
    			
    			throw new AdPRFCoaGlVarianceAccountNotFoundException();
    			
    		}
    		
    		LocalInvAdjustmentLine invAdjustmentLine = this.addInvAlEntry(invItemLocation, newInvAdjustment, CST_VRNC_VL, 
    				EJBCommon.FALSE, AD_CMPNY);
    		
    		// check for branch mapping
    		
    		LocalAdBranchItemLocation adBranchItemLocation = null;
    		
    		try{
    			
    			adBranchItemLocation = adBranchItemLocationHome.findBilByIlCodeAndBrCode(
    					invAdjustmentLine.getInvItemLocation().getIlCode(), AD_BRNCH, AD_CMPNY);
    			
    		} catch (FinderException ex) {
    			
    		}
    		
    		LocalGlChartOfAccount glInventoryChartOfAccount = null;
    		
    		if (adBranchItemLocation == null) {
    			
    			glInventoryChartOfAccount = glChartOfAccountHome.findByPrimaryKey(
    					invAdjustmentLine.getInvItemLocation().getIlGlCoaInventoryAccount());
    		} else {
    			
    			glInventoryChartOfAccount = glChartOfAccountHome.findByPrimaryKey(
    					adBranchItemLocation.getBilCoaGlInventoryAccount());
    			
    		}
    		
    		
    		boolean isDebit = CST_VRNC_VL < 0 ? false : true;
    		
    		//inventory dr
    		this.addInvDrEntry(newInvAdjustment.getInvDrNextLine(), "INVENTORY",
    				isDebit == true ? EJBCommon.TRUE : EJBCommon.FALSE, Math.abs(CST_VRNC_VL), EJBCommon.FALSE,
    						glInventoryChartOfAccount.getCoaCode(), newInvAdjustment, AD_BRNCH, AD_CMPNY);
    		
    		//variance dr
    		this.addInvDrEntry(newInvAdjustment.getInvDrNextLine(),"VARIANCE", 
    				!isDebit == true ? EJBCommon.TRUE : EJBCommon.FALSE, Math.abs(CST_VRNC_VL), EJBCommon.FALSE,
    						glCoaVarianceAccount.getCoaCode(), newInvAdjustment, AD_BRNCH, AD_CMPNY);
    		
    		this.executeInvAdjPost(newInvAdjustment.getAdjCode(), newInvAdjustment.getAdjLastModifiedBy(), AD_BRNCH,
    				AD_CMPNY);
    		
    	} catch (AdPRFCoaGlVarianceAccountNotFoundException ex) {
    		
    		getSessionContext().setRollbackOnly();
    		throw ex;

    	} catch (Exception ex) {
    		
    		Debug.printStackTrace(ex);
    		getSessionContext().setRollbackOnly();
    		throw new EJBException(ex.getMessage());
    		
    	}
    	*/
    }
    
    private void regenerateCostVariance(Collection invCostings, LocalInvCosting invCosting, Integer AD_BRNCH, Integer AD_CMPNY)
    throws AdPRFCoaGlVarianceAccountNotFoundException {
    	/*
    	Debug.print("ApApprovalControllerBean regenerateCostVariance");
    	
    	try {
    		
    		Iterator i = invCostings.iterator();
    		LocalInvCosting prevInvCosting = invCosting;
    		
    		while (i.hasNext()) {
    			
    			LocalInvCosting invPropagatedCosting = (LocalInvCosting)i.next();
    			
    			if(prevInvCosting.getCstRemainingQuantity() < 0) {
    				
    				double TTL_CST = 0;
    				double QNTY = 0;
    				String ADJ_RFRNC_NMBR = "";
    				String ADJ_DSCRPTN = "";
    				String ADJ_CRTD_BY = "";

    				// get unit cost adjusment, document number and unit of measure
    				if (invPropagatedCosting.getApPurchaseOrderLine() != null) {

    					TTL_CST = invPropagatedCosting.getApPurchaseOrderLine().getPlAmount();
						QNTY =  this.convertByUomFromAndItemAndQuantity(
								invPropagatedCosting.getApPurchaseOrderLine().getInvUnitOfMeasure(), 
								invPropagatedCosting.getApPurchaseOrderLine().getInvItemLocation().getInvItem(),
								invPropagatedCosting.getApPurchaseOrderLine().getPlQuantity(), AD_CMPNY);
    					
    					ADJ_DSCRPTN = invPropagatedCosting.getApPurchaseOrderLine().getApPurchaseOrder().getPoDescription();
    					ADJ_CRTD_BY = invPropagatedCosting.getApPurchaseOrderLine().getApPurchaseOrder().getPoPostedBy();
    					ADJ_RFRNC_NMBR = "APRI" +
						invPropagatedCosting.getApPurchaseOrderLine().getApPurchaseOrder().getPoDocumentNumber();
    					
    				} else if (invPropagatedCosting.getApVoucherLineItem() != null){

    					TTL_CST = invPropagatedCosting.getApVoucherLineItem().getVliAmount();
						QNTY = this.convertByUomFromAndItemAndQuantity(
								invPropagatedCosting.getApVoucherLineItem().getInvUnitOfMeasure(), 
								invPropagatedCosting.getApVoucherLineItem().getInvItemLocation().getInvItem(),
								invPropagatedCosting.getApVoucherLineItem().getVliQuantity(), AD_CMPNY);
    					
    					if (invPropagatedCosting.getApVoucherLineItem().getApVoucher() != null) {
    						
    						ADJ_DSCRPTN = invPropagatedCosting.getApVoucherLineItem().getApVoucher().getVouDescription();
    						ADJ_CRTD_BY = invPropagatedCosting.getApVoucherLineItem().getApVoucher().getVouPostedBy();
    						ADJ_RFRNC_NMBR = "APVOU" +
							invPropagatedCosting.getApVoucherLineItem().getApVoucher().getVouDocumentNumber();
    						
    					} else if (invPropagatedCosting.getApVoucherLineItem().getApCheck() != null) {
    						
    						ADJ_DSCRPTN = invPropagatedCosting.getApVoucherLineItem().getApCheck().getChkDescription();
    						ADJ_CRTD_BY = invPropagatedCosting.getApVoucherLineItem().getApCheck().getChkPostedBy();
    						ADJ_RFRNC_NMBR = "APCHK" +
							invPropagatedCosting.getApVoucherLineItem().getApCheck().getChkDocumentNumber();
    						
    					}
    					
    				} else if (invPropagatedCosting.getArInvoiceLineItem() != null){

    					QNTY = this.convertByUomFromAndItemAndQuantity(
								invPropagatedCosting.getArInvoiceLineItem().getInvUnitOfMeasure(), 
								invPropagatedCosting.getArInvoiceLineItem().getInvItemLocation().getInvItem(),
								invPropagatedCosting.getArInvoiceLineItem().getIliQuantity(), AD_CMPNY);
   						TTL_CST = prevInvCosting.getCstRemainingValue() - invPropagatedCosting.getCstRemainingValue();
   						
   						if(invPropagatedCosting.getArInvoiceLineItem().getArInvoice() != null){
   	   						
   	   							ADJ_DSCRPTN = invPropagatedCosting.getArInvoiceLineItem().getArInvoice().getInvDescription();
   	   							ADJ_CRTD_BY = invPropagatedCosting.getArInvoiceLineItem().getArInvoice().getInvPostedBy();
   	   							ADJ_RFRNC_NMBR = "ARCM" + 
   								invPropagatedCosting.getArInvoiceLineItem().getArInvoice().getInvNumber();
   	    					
   	   						} else if(invPropagatedCosting.getArInvoiceLineItem().getArReceipt() != null){

   	   							ADJ_DSCRPTN = invPropagatedCosting.getArInvoiceLineItem().getArReceipt().getRctDescription();
   	   							ADJ_CRTD_BY = invPropagatedCosting.getArInvoiceLineItem().getArReceipt().getRctPostedBy();
   	   							ADJ_RFRNC_NMBR = "ARMR" + 
   								invPropagatedCosting.getArInvoiceLineItem().getArReceipt().getRctNumber();

   	   						}
   	   						
    				} else if (invPropagatedCosting.getArSalesOrderInvoiceLine() != null){

   						TTL_CST = prevInvCosting.getCstRemainingValue() - invPropagatedCosting.getCstRemainingValue();
    					QNTY = this.convertByUomFromAndItemAndQuantity(
								invPropagatedCosting.getArSalesOrderInvoiceLine().getArSalesOrderLine().getInvUnitOfMeasure(), 
								invPropagatedCosting.getArSalesOrderInvoiceLine().getArSalesOrderLine().getInvItemLocation().getInvItem(),
								invPropagatedCosting.getArSalesOrderInvoiceLine().getSilQuantityDelivered(), AD_CMPNY);

    					ADJ_DSCRPTN = invPropagatedCosting.getArSalesOrderInvoiceLine().getArInvoice().getInvDescription();
    					ADJ_CRTD_BY = invPropagatedCosting.getArSalesOrderInvoiceLine().getArInvoice().getInvPostedBy();
    					ADJ_RFRNC_NMBR = "ARCM" + 
						invPropagatedCosting.getArSalesOrderInvoiceLine().getArInvoice().getInvNumber();
    					
    				} else if (invPropagatedCosting.getInvAdjustmentLine() != null){

    					ADJ_DSCRPTN = invPropagatedCosting.getInvAdjustmentLine().getInvAdjustment().getAdjDescription();
    					ADJ_CRTD_BY = invPropagatedCosting.getInvAdjustmentLine().getInvAdjustment().getAdjPostedBy();
    					ADJ_RFRNC_NMBR = "INVADJ" +
						invPropagatedCosting.getInvAdjustmentLine().getInvAdjustment().getAdjDocumentNumber();
    					
    					if(invPropagatedCosting.getInvAdjustmentLine().getAlAdjustQuantity() != 0) {
    						
    						TTL_CST = (invPropagatedCosting.getInvAdjustmentLine().getAlUnitCost() * 
    								invPropagatedCosting.getInvAdjustmentLine().getAlAdjustQuantity());
    						QNTY =  this.convertByUomFromAndItemAndQuantity(
    								invPropagatedCosting.getInvAdjustmentLine().getInvUnitOfMeasure(), 
									invPropagatedCosting.getInvAdjustmentLine().getInvItemLocation().getInvItem(),
									invPropagatedCosting.getInvAdjustmentLine().getAlAdjustQuantity(), AD_CMPNY);
    						
    					}

    				} else if (invPropagatedCosting.getInvAssemblyTransferLine() != null){

    					QNTY = invPropagatedCosting.getInvAssemblyTransferLine().getAtlAssembleQuantity();
    					TTL_CST = invPropagatedCosting.getInvAssemblyTransferLine().getAtlAssembleCost();

    					ADJ_DSCRPTN = invPropagatedCosting.getInvAssemblyTransferLine().getInvAssemblyTransfer().getAtrDescription();
    					ADJ_CRTD_BY = invPropagatedCosting.getInvAssemblyTransferLine().getInvAssemblyTransfer().getAtrPostedBy();
    					ADJ_RFRNC_NMBR = "INVAT" +
						invPropagatedCosting.getInvAssemblyTransferLine().getInvAssemblyTransfer().getAtrDocumentNumber();
    					
    				} else if (invPropagatedCosting.getInvBranchStockTransferLine() != null){

    					if(invPropagatedCosting.getInvBranchStockTransferLine().getInvBranchStockTransfer().getBstTransferOutNumber()
    							!= null) {

    						TTL_CST = invPropagatedCosting.getInvBranchStockTransferLine().getBslAmount();
							QNTY =  this.convertByUomFromAndItemAndQuantity(
									invPropagatedCosting.getInvBranchStockTransferLine().getInvUnitOfMeasure(), 
									invPropagatedCosting.getInvBranchStockTransferLine().getInvItemLocation().getInvItem(),
									invPropagatedCosting.getInvBranchStockTransferLine().getBslQuantityReceived(), AD_CMPNY);
							
    					} else {

    						TTL_CST = invPropagatedCosting.getInvBranchStockTransferLine().getBslAmount();
							QNTY =  this.convertByUomFromAndItemAndQuantity(
									invPropagatedCosting.getInvBranchStockTransferLine().getInvUnitOfMeasure(), 
									invPropagatedCosting.getInvBranchStockTransferLine().getInvItemLocation().getInvItem(),
									invPropagatedCosting.getInvBranchStockTransferLine().getBslQuantity(), AD_CMPNY);
    					}
    					
    					ADJ_DSCRPTN =
    						invPropagatedCosting.getInvBranchStockTransferLine().getInvBranchStockTransfer().getBstDescription();
    					ADJ_CRTD_BY =
    						invPropagatedCosting.getInvBranchStockTransferLine().getInvBranchStockTransfer().getBstPostedBy();
    					ADJ_RFRNC_NMBR = "INVBST" + 
						invPropagatedCosting.getInvBranchStockTransferLine().getInvBranchStockTransfer().getBstNumber();
    					
    				} else if (invPropagatedCosting.getInvBuildUnbuildAssemblyLine() != null){

   						TTL_CST = prevInvCosting.getCstRemainingValue() - invPropagatedCosting.getCstRemainingValue();
    					QNTY =  invPropagatedCosting.getInvBuildUnbuildAssemblyLine().getBlBuildQuantity();
    					ADJ_DSCRPTN =
    						invPropagatedCosting.getInvBuildUnbuildAssemblyLine().getInvBuildUnbuildAssembly().getBuaDescription();
    					ADJ_CRTD_BY =
    						invPropagatedCosting.getInvBuildUnbuildAssemblyLine().getInvBuildUnbuildAssembly().getBuaPostedBy();
    					ADJ_RFRNC_NMBR = "INVBUA" + 
						invPropagatedCosting.getInvBuildUnbuildAssemblyLine().getInvBuildUnbuildAssembly().getBuaDocumentNumber();
    					
    				} else if (invPropagatedCosting.getInvStockIssuanceLine()!= null){

    					TTL_CST = invPropagatedCosting.getInvStockIssuanceLine().getSilIssueCost();
						QNTY =  this.convertByUomFromAndItemAndQuantity(
								invPropagatedCosting.getInvStockIssuanceLine().getInvUnitOfMeasure(), 
								invPropagatedCosting.getInvStockIssuanceLine().getInvItemLocation().getInvItem(),
								invPropagatedCosting.getInvStockIssuanceLine().getSilIssueQuantity(), AD_CMPNY);
    					ADJ_DSCRPTN = invPropagatedCosting.getInvStockIssuanceLine().getInvStockIssuance().getSiDescription();
    					ADJ_CRTD_BY = invPropagatedCosting.getInvStockIssuanceLine().getInvStockIssuance().getSiPostedBy();
    					ADJ_RFRNC_NMBR = "INVSI" +
						invPropagatedCosting.getInvStockIssuanceLine().getInvStockIssuance().getSiDocumentNumber();
    					
    				} else if (invPropagatedCosting.getInvStockTransferLine()!= null) {

    					TTL_CST = invPropagatedCosting.getInvStockTransferLine().getStlAmount();
						QNTY =  this.convertByUomFromAndItemAndQuantity(
								invPropagatedCosting.getInvStockTransferLine().getInvUnitOfMeasure(), 
								invPropagatedCosting.getInvStockTransferLine().getInvItem(),
								invPropagatedCosting.getInvStockTransferLine().getStlQuantityDelivered(), AD_CMPNY);
    					ADJ_DSCRPTN = invPropagatedCosting.getInvStockTransferLine().getInvStockTransfer().getStDescription();
    					ADJ_CRTD_BY = invPropagatedCosting.getInvStockTransferLine().getInvStockTransfer().getStPostedBy();
    					ADJ_RFRNC_NMBR = "INVST" +
						invPropagatedCosting.getInvStockTransferLine().getInvStockTransfer().getStDocumentNumber();
    					
    				} else {
    					
    					prevInvCosting = invPropagatedCosting;
    					continue;
    					
    				}
    				
    				// if quantity is equal 0, no variance.
    				if(QNTY == 0) continue;
    				
    				// compute new cost variance
    				double UNT_CST = TTL_CST/QNTY;
    				double CST_VRNC_VL = (invPropagatedCosting.getCstRemainingQuantity() * UNT_CST -
    						invPropagatedCosting.getCstRemainingValue());
    				
    				if(CST_VRNC_VL != 0)
    					this.generateCostVariance(invPropagatedCosting.getInvItemLocation(), CST_VRNC_VL, ADJ_RFRNC_NMBR,
    							ADJ_DSCRPTN, invPropagatedCosting.getCstDate(), ADJ_CRTD_BY, AD_BRNCH, AD_CMPNY);
    				
    			}
    			
    			// set previous costing
    			prevInvCosting = invPropagatedCosting;
    			
    		}
    		
    	} catch (AdPRFCoaGlVarianceAccountNotFoundException ex){
    		
    		throw ex;
    		
    	} catch (Exception ex) {
    		
    		Debug.printStackTrace(ex);
    		getSessionContext().setRollbackOnly();
    		throw new EJBException(ex.getMessage());
    		
    	}    */    	
    }
    
    private void addInvDrEntry(short DR_LN, String DR_CLSS, byte DR_DBT, double DR_AMNT, byte DR_RVRSL, Integer COA_CODE, 
    		LocalInvAdjustment invAdjustment, Integer AD_BRNCH, Integer AD_CMPNY)
    
    throws GlobalBranchAccountNumberInvalidException {
    	
    	Debug.print("ApApprovalControllerBean addInvDrEntry");
    	
    	LocalAdCompanyHome adCompanyHome = null;
    	LocalInvDistributionRecordHome invDistributionRecordHome = null;        
    	LocalGlChartOfAccountHome glChartOfAccountHome = null;           
    	
    	// Initialize EJB Home
    	
    	try {
    		
    		adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);    
    		invDistributionRecordHome = (LocalInvDistributionRecordHome)EJBHomeFactory.
			lookUpLocalHome(LocalInvDistributionRecordHome.JNDI_NAME, LocalInvDistributionRecordHome.class);            
    		glChartOfAccountHome = (LocalGlChartOfAccountHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlChartOfAccountHome.JNDI_NAME, LocalGlChartOfAccountHome.class);                       
    		
    		
    	} catch (NamingException ex) {
    		
    		throw new EJBException(ex.getMessage());
    		
    	}            
    	
    	try {        
    		
    		// get company
    		
    		LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
    		
    		// validate coa
    		
    		LocalGlChartOfAccount glChartOfAccount = null;
    		
    		try {
    			
    			glChartOfAccount = glChartOfAccountHome.findByCoaCodeAndBranchCode(COA_CODE, AD_BRNCH, AD_CMPNY);
    			
    		} catch(FinderException ex) {
    			
    			throw new GlobalBranchAccountNumberInvalidException ();
    			
    		}
    		
    		// create distribution record        
    		
    		LocalInvDistributionRecord invDistributionRecord = invDistributionRecordHome.create(DR_LN, DR_CLSS, DR_DBT,
    				EJBCommon.roundIt(DR_AMNT, adCompany.getGlFunctionalCurrency().getFcPrecision()), DR_RVRSL, EJBCommon.FALSE,
    				AD_CMPNY);
    		
    		//invAdjustment.addInvDistributionRecord(invDistributionRecord);
    		invDistributionRecord.setInvAdjustment(invAdjustment);
    		//glChartOfAccount.addInvDistributionRecord(invDistributionRecord);
    		invDistributionRecord.setInvChartOfAccount(glChartOfAccount);
    		
    	} catch(GlobalBranchAccountNumberInvalidException ex) {
    		
    		throw new GlobalBranchAccountNumberInvalidException ();
    		
    	} catch (Exception ex) {
    		
    		Debug.printStackTrace(ex);
    		getSessionContext().setRollbackOnly();
    		throw new EJBException(ex.getMessage());
    		
    	}
    	
    }
    
    private void executeInvAdjPost(Integer ADJ_CODE, String USR_NM, Integer AD_BRNCH, Integer AD_CMPNY) throws
	GlobalRecordAlreadyDeletedException,		
	GlobalTransactionAlreadyPostedException,
	GlJREffectiveDateNoPeriodExistException,
	GlJREffectiveDatePeriodClosedException,
	GlobalJournalNotBalanceException,
	GlobalBranchAccountNumberInvalidException {
    	
    	Debug.print("ApApprovalControllerBean executeInvAdjPost");
    	
    	LocalInvAdjustmentHome invAdjustmentHome = null;        
    	LocalAdCompanyHome adCompanyHome = null;
    	LocalAdPreferenceHome adPreferenceHome = null;
    	LocalGlSetOfBookHome glSetOfBookHome = null;
    	LocalGlAccountingCalendarValueHome glAccountingCalendarValueHome = null;
    	LocalGlJournalHome glJournalHome = null;
    	LocalGlJournalBatchHome glJournalBatchHome = null;
    	LocalGlSuspenseAccountHome glSuspenseAccountHome = null;
    	LocalGlJournalLineHome glJournalLineHome = null;
    	LocalGlJournalSourceHome glJournalSourceHome = null;
    	LocalGlJournalCategoryHome glJournalCategoryHome = null;
    	LocalGlFunctionalCurrencyHome glFunctionalCurrencyHome = null;
    	LocalInvDistributionRecordHome invDistributionRecordHome = null;
    	LocalInvCostingHome invCostingHome = null;
    	LocalGlChartOfAccountHome glChartOfAccountHome = null;
    	LocalInvAdjustmentLineHome invAdjustmentLineHome = null;
    	
    	// Initialize EJB Home
    	
    	try {
    		
    		invAdjustmentHome = (LocalInvAdjustmentHome)EJBHomeFactory.
			lookUpLocalHome(LocalInvAdjustmentHome.JNDI_NAME, LocalInvAdjustmentHome.class);
    		adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
    		adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);
    		glSetOfBookHome = (LocalGlSetOfBookHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlSetOfBookHome.JNDI_NAME, LocalGlSetOfBookHome.class);
    		glAccountingCalendarValueHome = (LocalGlAccountingCalendarValueHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlAccountingCalendarValueHome.JNDI_NAME, LocalGlAccountingCalendarValueHome.class);
    		glJournalHome = (LocalGlJournalHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlJournalHome.JNDI_NAME, LocalGlJournalHome.class);
    		glJournalBatchHome = (LocalGlJournalBatchHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlJournalBatchHome.JNDI_NAME, LocalGlJournalBatchHome.class);
    		glSuspenseAccountHome = (LocalGlSuspenseAccountHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlSuspenseAccountHome.JNDI_NAME, LocalGlSuspenseAccountHome.class);
    		glJournalLineHome = (LocalGlJournalLineHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlJournalLineHome.JNDI_NAME, LocalGlJournalLineHome.class);
    		glJournalSourceHome = (LocalGlJournalSourceHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlJournalSourceHome.JNDI_NAME, LocalGlJournalSourceHome.class);
    		glJournalCategoryHome = (LocalGlJournalCategoryHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlJournalCategoryHome.JNDI_NAME, LocalGlJournalCategoryHome.class);
    		glFunctionalCurrencyHome = (LocalGlFunctionalCurrencyHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlFunctionalCurrencyHome.JNDI_NAME, LocalGlFunctionalCurrencyHome.class);
    		invDistributionRecordHome = (LocalInvDistributionRecordHome)EJBHomeFactory.
			lookUpLocalHome(LocalInvDistributionRecordHome.JNDI_NAME, LocalInvDistributionRecordHome.class);
    		invCostingHome = (LocalInvCostingHome)EJBHomeFactory.
			lookUpLocalHome(LocalInvCostingHome.JNDI_NAME, LocalInvCostingHome.class);
    		glChartOfAccountHome = (LocalGlChartOfAccountHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlChartOfAccountHome.JNDI_NAME, LocalGlChartOfAccountHome.class);
    		invAdjustmentLineHome = (LocalInvAdjustmentLineHome)EJBHomeFactory.
			lookUpLocalHome(LocalInvAdjustmentLineHome.JNDI_NAME, LocalInvAdjustmentLineHome.class);

    		
    	} catch (NamingException ex) {
    		
    		throw new EJBException(ex.getMessage());
    		
    	}       
    	
    	try {
    		
    		// validate if adjustment is already deleted
    		
    		LocalInvAdjustment invAdjustment = null;
    		
    		try {
    			
    			invAdjustment = invAdjustmentHome.findByPrimaryKey(ADJ_CODE);
    			
    		} catch (FinderException ex) {
    			
    			throw new GlobalRecordAlreadyDeletedException();
    			
    		}
    		
    		// validate if adjustment is already posted or void
    		
    		if (invAdjustment.getAdjPosted() == EJBCommon.TRUE) {

    			if (invAdjustment.getAdjVoid() != EJBCommon.TRUE)
    			throw new GlobalTransactionAlreadyPostedException();
    			
    		}
    		
    		Collection invAdjustmentLines = null;
    		
    		if(invAdjustment.getAdjVoid() == EJBCommon.FALSE)
    			invAdjustmentLines = invAdjustmentLineHome.findByAlVoidAndAdjCode(EJBCommon.FALSE, invAdjustment.getAdjCode(), AD_CMPNY);
    		else
    			invAdjustmentLines = invAdjustmentLineHome.findByAlVoidAndAdjCode(EJBCommon.TRUE, invAdjustment.getAdjCode(), AD_CMPNY);
    		
    		Iterator i = invAdjustmentLines.iterator();
    		
    		while(i.hasNext()) {
    			
    			
    			LocalInvAdjustmentLine invAdjustmentLine = (LocalInvAdjustmentLine) i.next();

    			LocalInvCosting invCosting =
    				invCostingHome.getByMaxCstDateToLongAndMaxCstLineNumberAndLessThanEqualCstDateAndIiNameAndLocName(
    						invAdjustment.getAdjDate(), invAdjustmentLine.getInvItemLocation().getInvItem().getIiName(),
    						invAdjustmentLine.getInvItemLocation().getInvLocation().getLocName(), AD_BRNCH, AD_CMPNY);
    			
    				this.postInvAdjustmentToInventory(invAdjustmentLine, invAdjustment.getAdjDate(), 0,
    						invAdjustmentLine.getAlUnitCost(), invCosting.getCstRemainingQuantity(),
							invCosting.getCstRemainingValue() + invAdjustmentLine.getAlUnitCost(), AD_BRNCH, AD_CMPNY);
    			
    		}			
    		
    		// post to gl if necessary
    		
    		LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);
    		LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
    		
    			// validate if date has no period and period is closed
    			
    			LocalGlSetOfBook glJournalSetOfBook = null;
    			
    			try {
    				
    				glJournalSetOfBook = glSetOfBookHome.findByDate(invAdjustment.getAdjDate(), AD_CMPNY);
    				
    			} catch (FinderException ex) {
    				
    				throw new GlJREffectiveDateNoPeriodExistException();
    				
    			}
    			
    			LocalGlAccountingCalendarValue glAccountingCalendarValue = 
    				glAccountingCalendarValueHome.findByAcCodeAndDate(
    						glJournalSetOfBook.getGlAccountingCalendar().getAcCode(), invAdjustment.getAdjDate(), AD_CMPNY);
    			
    			
    			if (glAccountingCalendarValue.getAcvStatus() == 'N' ||
    					glAccountingCalendarValue.getAcvStatus() == 'C' ||
						glAccountingCalendarValue.getAcvStatus() == 'P') {
    				
    				throw new GlJREffectiveDatePeriodClosedException();
    				
    			}
    			
    			// check if invoice is balance if not check suspense posting
    			
    			LocalGlJournalLine glOffsetJournalLine = null;
    			
    			Collection invDistributionRecords = null;
    			
    			if (invAdjustment.getAdjVoid() == EJBCommon.FALSE) {
    				
    				invDistributionRecords = invDistributionRecordHome.findImportableDrByDrReversedAndAdjCode(EJBCommon.FALSE,
    						invAdjustment.getAdjCode(), AD_CMPNY);
    				
    			} else {
    				
    				invDistributionRecords = invDistributionRecordHome.findImportableDrByDrReversedAndAdjCode(EJBCommon.TRUE,
    						invAdjustment.getAdjCode(), AD_CMPNY);
    				
    			}
    			
    			Iterator j = invDistributionRecords.iterator();
    			
    			double TOTAL_DEBIT = 0d;
    			double TOTAL_CREDIT = 0d;
    			
    			while (j.hasNext()) {
    				
    				LocalInvDistributionRecord invDistributionRecord = (LocalInvDistributionRecord)j.next();
    				
    				double DR_AMNT = 0d;
    				
    				DR_AMNT = invDistributionRecord.getDrAmount();
    				
    				if (invDistributionRecord.getDrDebit() == EJBCommon.TRUE) {
    					
    					TOTAL_DEBIT += DR_AMNT;
    					
    				} else {
    					
    					TOTAL_CREDIT += DR_AMNT;
    					
    				}
    				
    			}
    			
    			TOTAL_DEBIT = EJBCommon.roundIt(TOTAL_DEBIT, adCompany.getGlFunctionalCurrency().getFcPrecision());
    			TOTAL_CREDIT = EJBCommon.roundIt(TOTAL_CREDIT, adCompany.getGlFunctionalCurrency().getFcPrecision());
    			
    			if (adPreference.getPrfAllowSuspensePosting() == EJBCommon.TRUE &&
    					TOTAL_DEBIT != TOTAL_CREDIT) {
    				
    				LocalGlSuspenseAccount glSuspenseAccount = null;
    				
    				try { 	
    					
    					glSuspenseAccount = glSuspenseAccountHome.findByJsNameAndJcName("INVENTORY", "INVENTORY ADJUSTMENTS",
    							AD_CMPNY);
    					
    				} catch (FinderException ex) {
    					
    					throw new GlobalJournalNotBalanceException();
    					
    				}
    				
    				if (TOTAL_DEBIT - TOTAL_CREDIT < 0) {
    					
    					glOffsetJournalLine = glJournalLineHome.create((short)(invDistributionRecords.size() + 1), EJBCommon.TRUE, 
    							TOTAL_CREDIT - TOTAL_DEBIT, "", AD_CMPNY);
    					
    				} else {
    					
    					glOffsetJournalLine = glJournalLineHome.create((short)(invDistributionRecords.size() + 1), EJBCommon.FALSE,
    							TOTAL_DEBIT - TOTAL_CREDIT, "", AD_CMPNY);
    					
    				}
    				
    				LocalGlChartOfAccount glChartOfAccount = glSuspenseAccount.getGlChartOfAccount();
    				//glChartOfAccount.addGlJournalLine(glOffsetJournalLine);
    				glOffsetJournalLine.setGlChartOfAccount(glChartOfAccount);    				
    				
    			} else if (adPreference.getPrfAllowSuspensePosting() == EJBCommon.FALSE &&
    					TOTAL_DEBIT != TOTAL_CREDIT) {
    				
    				throw new GlobalJournalNotBalanceException();		    	
    				
    			}
    			
    			// create journal batch if necessary
    			
    			LocalGlJournalBatch glJournalBatch = null;
    			java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat("MM/dd/yyyy");
    			
    			try {
    				
    				glJournalBatch = glJournalBatchHome.findByJbName("JOURNAL IMPORT " + formatter.format(new Date()) +
    						" INVENTORY ADJUSTMENTS", AD_BRNCH, AD_CMPNY);
    				
    			} catch (FinderException ex) {
    				
    			}
    			
    			if (glJournalBatch == null) {
    				
    				glJournalBatch = glJournalBatchHome.create("JOURNAL IMPORT " + formatter.format(new Date()) +
    						" INVENTORY ADJUSTMENTS", "JOURNAL IMPORT", "CLOSED", EJBCommon.getGcCurrentDateWoTime().getTime(),
							USR_NM, AD_BRNCH, AD_CMPNY);
    				
    			}
    			
    			// create journal entry			            	
    			
    			LocalGlJournal glJournal = glJournalHome.create(invAdjustment.getAdjReferenceNumber(),
    					invAdjustment.getAdjDescription(), invAdjustment.getAdjDate(),
						0.0d, null, invAdjustment.getAdjDocumentNumber(), null, 1d, "N/A", null,
						'N', EJBCommon.TRUE, EJBCommon.FALSE,
						USR_NM, new Date(),
						USR_NM, new Date(),
						null, null,
						USR_NM, EJBCommon.getGcCurrentDateWoTime().getTime(), 
						null, null, EJBCommon.FALSE, null,
						AD_BRNCH, AD_CMPNY);
    			
    			LocalGlJournalSource glJournalSource = glJournalSourceHome.findByJsName("INVENTORY", AD_CMPNY);
    			//glJournalSource.addGlJournal(glJournal);
    			glJournal.setGlJournalSource(glJournalSource);
    			
    			LocalGlFunctionalCurrency glFunctionalCurrency = glFunctionalCurrencyHome.findByFcName(
    					adCompany.getGlFunctionalCurrency().getFcName(), AD_CMPNY);
    			//glFunctionalCurrency.addGlJournal(glJournal);
    			glJournal.setGlFunctionalCurrency(glFunctionalCurrency);
    			
    			LocalGlJournalCategory glJournalCategory = glJournalCategoryHome.findByJcName("INVENTORY ADJUSTMENTS", AD_CMPNY);
    			//glJournalCategory.addGlJournal(glJournal); 
    			glJournal.setGlJournalCategory(glJournalCategory);

    			if (glJournalBatch != null) {
    				
    				//glJournalBatch.addGlJournal(glJournal);
    				glJournal.setGlJournalBatch(glJournalBatch);
    				
    			}           		    
    			
    			// create journal lines
    			
    			j = invDistributionRecords.iterator();
    			
    			while (j.hasNext()) {
    				
    				LocalInvDistributionRecord invDistributionRecord = (LocalInvDistributionRecord)j.next();
    				
    				double DR_AMNT = 0d;
    				
    				DR_AMNT = invDistributionRecord.getDrAmount();
    				
    				LocalGlJournalLine glJournalLine = glJournalLineHome.create(invDistributionRecord.getDrLine(),	            			
    						invDistributionRecord.getDrDebit(), DR_AMNT, "", AD_CMPNY);
    				
    				invDistributionRecord.getInvChartOfAccount().addGlJournalLine(glJournalLine);
    				
    				//glJournal.addGlJournalLine(glJournalLine);
    				glJournalLine.setGlJournal(glJournal);
    				
    				invDistributionRecord.setDrImported(EJBCommon.TRUE);
    				
    				
    			}
    			
    			if (glOffsetJournalLine != null) {
    				
    				//glJournal.addGlJournalLine(glOffsetJournalLine);
    				glOffsetJournalLine.setGlJournal(glJournal);
    			}		
    			
    			// post journal to gl
    			
    			Collection glJournalLines = glJournal.getGlJournalLines();
    			
    			i = glJournalLines.iterator();
    			
    			while (i.hasNext()) {
    				
    				LocalGlJournalLine glJournalLine = (LocalGlJournalLine)i.next();	
    				
    				// post current to current acv
    				
    				this.postToGl(glAccountingCalendarValue,
    						glJournalLine.getGlChartOfAccount(),
							true, glJournalLine.getJlDebit(), glJournalLine.getJlAmount(), AD_CMPNY);
    				
    				
    				// post to subsequent acvs (propagate)
    				
    				Collection glSubsequentAccountingCalendarValues = 
    					glAccountingCalendarValueHome.findSubsequentAcvByAcCodeAndAcvPeriodNumber( 
    							glJournalSetOfBook.getGlAccountingCalendar().getAcCode(),
								glAccountingCalendarValue.getAcvPeriodNumber(), AD_CMPNY);     
    				
    				Iterator acvsIter = glSubsequentAccountingCalendarValues.iterator();
    				
    				while (acvsIter.hasNext()) {
    					
    					LocalGlAccountingCalendarValue glSubsequentAccountingCalendarValue = 
    						(LocalGlAccountingCalendarValue)acvsIter.next();
    					
    					this.postToGl(glSubsequentAccountingCalendarValue,
    							glJournalLine.getGlChartOfAccount(),
								false, glJournalLine.getJlDebit(), glJournalLine.getJlAmount(), AD_CMPNY);
    					
    				}
    				
    				// post to subsequent years if necessary
    				
    				Collection glSubsequentSetOfBooks = glSetOfBookHome.findSubsequentSobByAcYear(
    						glJournalSetOfBook.getGlAccountingCalendar().getAcYear(), AD_CMPNY);
    				
    				if (!glSubsequentSetOfBooks.isEmpty() && glJournalSetOfBook.getSobYearEndClosed() == 1) {
    					
    					adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);		  	  	
    					LocalGlChartOfAccount glRetainedEarningsAccount =
    						glChartOfAccountHome.findByCoaAccountNumberAndBranchCode(adCompany.getCmpRetainedEarnings(),
    								AD_BRNCH, AD_CMPNY); 	  	
    					
    					Iterator sobIter = glSubsequentSetOfBooks.iterator();
    					
    					while (sobIter.hasNext()) {
    						
    						LocalGlSetOfBook glSubsequentSetOfBook = (LocalGlSetOfBook)sobIter.next();
    						
    						String ACCOUNT_TYPE = glJournalLine.getGlChartOfAccount().getCoaAccountType();
    						
    						// post to subsequent acvs of subsequent set of book(propagate)
    						
    						Collection glAccountingCalendarValues = 
    							glAccountingCalendarValueHome.findByAcCode(
    									glSubsequentSetOfBook.getGlAccountingCalendar().getAcCode(), AD_CMPNY);     
    						
    						Iterator acvIter = glAccountingCalendarValues.iterator();
    						
    						while (acvIter.hasNext()) {
    							
    							LocalGlAccountingCalendarValue glSubsequentAccountingCalendarValue = 
    								(LocalGlAccountingCalendarValue)acvIter.next();
    							
    							if (ACCOUNT_TYPE.equals("ASSET") || ACCOUNT_TYPE.equals("LIABILITY") ||
    									ACCOUNT_TYPE.equals("OWNERS EQUITY")) {
    								
    								this.postToGl(glSubsequentAccountingCalendarValue,
    										glJournalLine.getGlChartOfAccount(),
											false, glJournalLine.getJlDebit(), glJournalLine.getJlAmount(), AD_CMPNY);
    								
    							} else { // revenue & expense
    								
    								this.postToGl(glSubsequentAccountingCalendarValue,
    										glRetainedEarningsAccount,
											false, glJournalLine.getJlDebit(), glJournalLine.getJlAmount(), AD_CMPNY);					        
    								
    							}
    							
    						}
    						
    						if (glSubsequentSetOfBook.getSobYearEndClosed() == 0) break;
    						
    					}
    					
    				}
    				
    			}

			invAdjustment.setAdjPosted(EJBCommon.TRUE);

    	} catch (GlJREffectiveDateNoPeriodExistException ex) {
    		
    		getSessionContext().setRollbackOnly();
    		throw ex;
    		
    	} catch (GlJREffectiveDatePeriodClosedException ex) {
    		
    		getSessionContext().setRollbackOnly();
    		throw ex;
    		
    	} catch (GlobalJournalNotBalanceException ex) {
    		
    		getSessionContext().setRollbackOnly();
    		throw ex;
    		
    	} catch (GlobalRecordAlreadyDeletedException ex) {
    		
    		getSessionContext().setRollbackOnly();
    		throw ex;
    		
    	} catch (GlobalTransactionAlreadyPostedException ex) {
    		
    		getSessionContext().setRollbackOnly();
    		throw ex;
    		
    	} catch (Exception ex) {
    		
    		Debug.printStackTrace(ex);
    		getSessionContext().setRollbackOnly();
    		throw new EJBException(ex.getMessage());
    		
    	}
    	
    }

    private LocalInvAdjustmentLine addInvAlEntry(LocalInvItemLocation invItemLocation, LocalInvAdjustment invAdjustment,
    		double CST_VRNC_VL, byte AL_VD, Integer AD_CMPNY) {
    	
    	Debug.print("ArMiscReceiptEntryControllerBean addInvAlEntry");
    	
    	LocalInvAdjustmentLineHome invAdjustmentLineHome = null;
    	
    	// Initialize EJB Home
    	
    	try {
    		
    		invAdjustmentLineHome = (LocalInvAdjustmentLineHome)EJBHomeFactory.
			lookUpLocalHome(LocalInvAdjustmentLineHome.JNDI_NAME, LocalInvAdjustmentLineHome.class); 
    		
    	} catch (NamingException ex) {
    		
    		throw new EJBException(ex.getMessage());
    		
    	}            
    	
    	try {
    		
    		// create dr entry
    		LocalInvAdjustmentLine invAdjustmentLine = null;
    		invAdjustmentLine = invAdjustmentLineHome.create(CST_VRNC_VL,null,null, 0,0, AL_VD, AD_CMPNY);
    		
    		// map adjustment, unit of measure, item location
    		//invAdjustment.addInvAdjustmentLine(invAdjustmentLine);	
    		invAdjustmentLine.setInvAdjustment(invAdjustment);
    		//invItemLocation.getInvItem().getInvUnitOfMeasure().addInvAdjustmentLine(invAdjustmentLine);
    		invAdjustmentLine.setInvUnitOfMeasure(invItemLocation.getInvItem().getInvUnitOfMeasure());
    		//invItemLocation.addInvAdjustmentLine(invAdjustmentLine);
    		invAdjustmentLine.setInvItemLocation(invItemLocation);
    		
    		return invAdjustmentLine;
    		
    	} catch (Exception ex) {
    		
    		Debug.printStackTrace(ex);
    		getSessionContext().setRollbackOnly();
    		throw new EJBException(ex.getMessage());
    		
    	}
    	
    } 

    private LocalInvAdjustment saveInvAdjustment(String ADJ_RFRNC_NMBR, String ADJ_DSCRPTN,
    		Date ADJ_DATE, String USR_NM, Integer AD_BRNCH, Integer AD_CMPNY){
    	
    	Debug.print("ApApprovalControllerBean saveInvAdjustment");
    	
    	LocalInvAdjustmentHome invAdjustmentHome = null;
    	LocalAdDocumentSequenceAssignmentHome adDocumentSequenceAssignmentHome = null;
    	LocalAdBranchDocumentSequenceAssignmentHome adBranchDocumentSequenceAssignmentHome = null;
    	// Initialize EJB Home    	
    	
    	try{
    	
    		invAdjustmentHome = (LocalInvAdjustmentHome)EJBHomeFactory.
			lookUpLocalHome(LocalInvAdjustmentHome.JNDI_NAME, LocalInvAdjustmentHome.class);
			adDocumentSequenceAssignmentHome = (LocalAdDocumentSequenceAssignmentHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdDocumentSequenceAssignmentHome.JNDI_NAME, LocalAdDocumentSequenceAssignmentHome.class);
			adBranchDocumentSequenceAssignmentHome = (LocalAdBranchDocumentSequenceAssignmentHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdBranchDocumentSequenceAssignmentHome.JNDI_NAME, LocalAdBranchDocumentSequenceAssignmentHome.class); 

    	} catch (NamingException ex) {
    		
    		throw new EJBException(ex.getMessage());
    		
    	}       
    	
    	try{
    		
    		// generate adj document number
    		String ADJ_DCMNT_NMBR = null;
    		
    		LocalAdBranchDocumentSequenceAssignment adBranchDocumentSequenceAssignment = null;
    		LocalAdDocumentSequenceAssignment adDocumentSequenceAssignment = null;
    		
    		try {
    			
    			adDocumentSequenceAssignment = adDocumentSequenceAssignmentHome.findByDcName("INV ADJUSTMENT", AD_CMPNY);
    			
    		} catch (FinderException ex) {
    			
    		}
    		
    		try {
    			
    			adBranchDocumentSequenceAssignment = adBranchDocumentSequenceAssignmentHome.findBdsByDsaCodeAndBrCode(
    					adDocumentSequenceAssignment.getDsaCode(), AD_BRNCH, AD_CMPNY);
    			
    		} catch (FinderException ex) {
    			
    		}
    		
    		while (true) {
    			
    			if (adBranchDocumentSequenceAssignment == null || adBranchDocumentSequenceAssignment.getBdsNextSequence() == null) {
    				
    				try {
    					
    					invAdjustmentHome.findByAdjDocumentNumberAndBrCode(adDocumentSequenceAssignment.getDsaNextSequence(),
    							AD_BRNCH, AD_CMPNY);		            		
    					adDocumentSequenceAssignment.setDsaNextSequence(EJBCommon.incrementStringNumber(
    							adDocumentSequenceAssignment.getDsaNextSequence()));	
    					
    				} catch (FinderException ex) {
    					
    					ADJ_DCMNT_NMBR = adDocumentSequenceAssignment.getDsaNextSequence();
    					adDocumentSequenceAssignment.setDsaNextSequence(EJBCommon.incrementStringNumber(
    							adDocumentSequenceAssignment.getDsaNextSequence()));	
    					break;
    					
    				}
    				
    			} else {
    				
    				try {
    					
    					invAdjustmentHome.findByAdjDocumentNumberAndBrCode(
    							adBranchDocumentSequenceAssignment.getBdsNextSequence(), AD_BRNCH, AD_CMPNY);		            		
    					adBranchDocumentSequenceAssignment.setBdsNextSequence(EJBCommon.incrementStringNumber(
    							adBranchDocumentSequenceAssignment.getBdsNextSequence()));
    					
    				} catch (FinderException ex) {
    					
    					ADJ_DCMNT_NMBR = adBranchDocumentSequenceAssignment.getBdsNextSequence();
    					adBranchDocumentSequenceAssignment.setBdsNextSequence(EJBCommon.incrementStringNumber(
    							adBranchDocumentSequenceAssignment.getBdsNextSequence()));	
    					break;
    					
    				}
    				
    			}
    			
    		}		            
    		
    		LocalInvAdjustment invAdjustment = invAdjustmentHome.create(ADJ_DCMNT_NMBR, ADJ_RFRNC_NMBR,
    				ADJ_DSCRPTN, ADJ_DATE, "COST-VARIANCE", "N/A", EJBCommon.FALSE, USR_NM, ADJ_DATE, USR_NM, ADJ_DATE, null, null,
					USR_NM, ADJ_DATE, null, null, EJBCommon.TRUE, EJBCommon.FALSE, AD_BRNCH, AD_CMPNY);
    		
    		return invAdjustment;
    		
    	} catch (Exception ex) {
    		
    		Debug.printStackTrace(ex);
    		getSessionContext().setRollbackOnly();
    		throw new EJBException(ex.getMessage());
    		
    	}       
    	
    	
    }
    
    private void postInvAdjustmentToInventory(LocalInvAdjustmentLine invAdjustmentLine, Date CST_DT, double CST_ADJST_QTY,
    		double CST_ADJST_CST, double CST_RMNNG_QTY, double CST_RMNNG_VL, Integer AD_BRNCH,Integer AD_CMPNY) {
    	
    	Debug.print("ApApprovalControllerBean postInvAdjustmentToInventory");
    	
    	LocalInvCostingHome invCostingHome = null;
    	LocalAdPreferenceHome adPreferenceHome = null;
    	LocalAdCompanyHome adCompanyHome = null;
    	
    	// Initialize EJB Home
    	
    	try {
    		
    		invCostingHome = (LocalInvCostingHome)EJBHomeFactory.
			lookUpLocalHome(LocalInvCostingHome.JNDI_NAME, LocalInvCostingHome.class);
    		adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);
    		adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);              
    		
    	} catch (NamingException ex) {
    		
    		getSessionContext().setRollbackOnly();
    		throw new EJBException(ex.getMessage());
    		
    	}
    	
    	try {
    		
    		LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);
    		LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
    		LocalInvItemLocation invItemLocation = invAdjustmentLine.getInvItemLocation();
    		int CST_LN_NMBR = 0;
    		
    		CST_ADJST_QTY = EJBCommon.roundIt(CST_ADJST_QTY, adPreference.getPrfInvQuantityPrecisionUnit());
    		CST_ADJST_CST = EJBCommon.roundIt(CST_ADJST_CST, adCompany.getGlFunctionalCurrency().getFcPrecision());
    		CST_RMNNG_QTY = EJBCommon.roundIt(CST_RMNNG_QTY, adPreference.getPrfInvQuantityPrecisionUnit());
    		CST_RMNNG_VL = EJBCommon.roundIt(CST_RMNNG_VL, adCompany.getGlFunctionalCurrency().getFcPrecision());
    		
    		if (CST_ADJST_QTY < 0) {
    			
    			invItemLocation.setIlCommittedQuantity(invItemLocation.getIlCommittedQuantity() - Math.abs(CST_ADJST_QTY));
    			
    		}
    		
    		// create costing
    		
    		try {
    			
    			// generate line number
    			
    			LocalInvCosting invCurrentCosting = invCostingHome.getByMaxCstLineNumberAndCstDateToLongAndIiNameAndLocName(
    					CST_DT.getTime(), invItemLocation.getInvItem().getIiName(), invItemLocation.getInvLocation().getLocName(),
						AD_BRNCH, AD_CMPNY);
    			CST_LN_NMBR = invCurrentCosting.getCstLineNumber() + 1;
    			
    		} catch (FinderException ex) {
    			
    			CST_LN_NMBR = 1;
    			
    		}
    		
    		LocalInvCosting invCosting = invCostingHome.create(CST_DT, CST_DT.getTime(), CST_LN_NMBR, 0d, 0d, 0d, 0d,
    				CST_ADJST_QTY, CST_ADJST_CST, 0d, 0d, CST_RMNNG_QTY, CST_RMNNG_VL, 0d, 0d, CST_ADJST_QTY > 0 ? CST_ADJST_QTY : 0, AD_BRNCH, AD_CMPNY);
    		//invItemLocation.addInvCosting(invCosting);
    		invCosting.setInvItemLocation(invItemLocation);
    		invCosting.setInvAdjustmentLine(invAdjustmentLine);
    		
    		// propagate balance if necessary           
    		
    		Collection invCostings = invCostingHome.findByGreaterThanCstDateAndIiNameAndLocName(CST_DT, invItemLocation.getInvItem().getIiName(), invItemLocation.getInvLocation().getLocName(), AD_BRNCH, AD_CMPNY);
    		
    		Iterator i = invCostings.iterator();
    		
    		while (i.hasNext()) {
    			
    			LocalInvCosting invPropagatedCosting = (LocalInvCosting)i.next();
    			
    			invPropagatedCosting.setCstRemainingQuantity(invPropagatedCosting.getCstRemainingQuantity() + CST_ADJST_QTY);
    			invPropagatedCosting.setCstRemainingValue(invPropagatedCosting.getCstRemainingValue() + CST_ADJST_CST);
    			
    		}                           
    		
    		
    	} catch (Exception ex) {
    		
    		Debug.printStackTrace(ex);
    		getSessionContext().setRollbackOnly();
    		throw new EJBException(ex.getMessage());
    		
    	}
    	
    	
    	
    }
    
    private void setInvTbForItemForCurrentMonth(String itemName, String userName, Date date, double qtyConsumed, Integer AD_CMPNY) 
	throws GlobalNoRecordFoundException
	{
		
		Debug.print("ApApprovalControllerBean getInvTrnsctnlBdgtForCurrentMonth");
		LocalInvTransactionalBudgetHome invTransactionalBudgetHome = null;               
		LocalInvItemHome invItemHome = null;
		LocalAdUserHome adUserHome = null;
		LocalAdLookUpValueHome adLookUpValueHome = null;
		ArrayList list = new ArrayList();
		
		// Initialize EJB Home
		
		try {
			
			invTransactionalBudgetHome = (LocalInvTransactionalBudgetHome)EJBHomeFactory.
				lookUpLocalHome(LocalInvTransactionalBudgetHome.JNDI_NAME, LocalInvTransactionalBudgetHome.class);
			invItemHome = (LocalInvItemHome)EJBHomeFactory.
					lookUpLocalHome(LocalInvItemHome.JNDI_NAME, LocalInvItemHome.class);
			adUserHome = (LocalAdUserHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdUserHome.JNDI_NAME, LocalAdUserHome.class);
			adLookUpValueHome = (LocalAdLookUpValueHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdLookUpValueHome.JNDI_NAME, LocalAdLookUpValueHome.class);
			
		} catch (NamingException ex) {
			
			throw new EJBException(ex.getMessage());
			
		}        
		try {
			java.text.SimpleDateFormat sdf = new java.text.SimpleDateFormat("MM");
			java.text.SimpleDateFormat sdfYear = new java.text.SimpleDateFormat("yyyy");
			String month = sdf.format(date);
			
			LocalAdUser adUser = adUserHome.findByUsrName(userName, AD_CMPNY);
			LocalAdLookUpValue adLookUpValue = adLookUpValueHome.findByLuNameAndLvName("DEPARTMENT", adUser.getUsrDept(), AD_CMPNY);
			System.out.println("itemName-" + itemName);
			System.out.println("adLookUpValue.getLvCode()-" + adLookUpValue.getLvCode());
			System.out.println("Integer.parseInt(sdfYear.format(date))-" + Integer.parseInt(sdfYear.format(date)));
			LocalInvTransactionalBudget invTransactionalBudget = invTransactionalBudgetHome.findByTbItemNameAndTbDeptAndTbYear(itemName, adLookUpValue.getLvCode(), Integer.parseInt(sdfYear.format(date)), AD_CMPNY);
			//LocalInvItem invItem = LocalInvItemHome
			
			
			if (month.equals("01")){
				
				invTransactionalBudget.setTbQuantityJan( invTransactionalBudget.getTbQuantityJan() - qtyConsumed);
			}else if (month.equals("02")){
				
				invTransactionalBudget.setTbQuantityFeb( invTransactionalBudget.getTbQuantityFeb() - qtyConsumed);
			}else if (month.equals("03")){
				invTransactionalBudget.setTbQuantityMrch( invTransactionalBudget.getTbQuantityMrch() - qtyConsumed);
			}else if (month.equals("04")){
				invTransactionalBudget.setTbQuantityAprl( invTransactionalBudget.getTbQuantityAprl() - qtyConsumed);
			}else if (month.equals("05")){
				invTransactionalBudget.setTbQuantityMay( invTransactionalBudget.getTbQuantityMay() - qtyConsumed);
			}else if (month.equals("06")){
				invTransactionalBudget.setTbQuantityJun( invTransactionalBudget.getTbQuantityJun() - qtyConsumed);
			}else if (month.equals("07")){
				invTransactionalBudget.setTbQuantityJul( invTransactionalBudget.getTbQuantityJul() - qtyConsumed);
			}else if (month.equals("08")){
				invTransactionalBudget.setTbQuantityAug( invTransactionalBudget.getTbQuantityAug() - qtyConsumed);
			}else if (month.equals("09")){
				invTransactionalBudget.setTbQuantitySep( invTransactionalBudget.getTbQuantitySep() - qtyConsumed);
			}else if (month.equals("10")){
				invTransactionalBudget.setTbQuantityOct( invTransactionalBudget.getTbQuantityOct() - qtyConsumed);
			}else if (month.equals("11")){
				invTransactionalBudget.setTbQuantityNov( invTransactionalBudget.getTbQuantityNov() - qtyConsumed);
			}else {
				invTransactionalBudget.setTbQuantityDec( invTransactionalBudget.getTbQuantityDec() - qtyConsumed);
			}
			
		} catch (FinderException ex) {
			System.out.println("no item found in transactional budget table");
			  //throw new GlobalNoRecordFoundException();
        }
	}

    
    
    private double convertByUomFromAndItemAndQuantity(LocalInvUnitOfMeasure invFromUnitOfMeasure, LocalInvItem invItem, double QTY_RCVD, double conversionFactor, Integer AD_CMPNY) {
		
		Debug.print("ApReceivingItemEntryControllerBean convertByUomFromAndUomToAndQuantity");		        
        
		LocalAdPreferenceHome adPreferenceHome = null;
		LocalInvUnitOfMeasureConversionHome invUnitOfMeasureConversionHome = null;
		        
        // Initialize EJB Home
        
        try {
            
            adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
                 lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);   
            invUnitOfMeasureConversionHome = (LocalInvUnitOfMeasureConversionHome)EJBHomeFactory.
				lookUpLocalHome(LocalInvUnitOfMeasureConversionHome.JNDI_NAME, LocalInvUnitOfMeasureConversionHome.class);   
               
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
                
        try {
        
        	Debug.print("ApReceivingItemEntryControllerBean convertByUomFromAndUomToAndQuantity A");
            LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);               	          	        	
            
            LocalInvUnitOfMeasureConversion invUnitOfMeasureConversion = invUnitOfMeasureConversionHome.findUmcByIiNameAndUomName(invItem.getIiName(), invFromUnitOfMeasure.getUomName(), AD_CMPNY);
            LocalInvUnitOfMeasureConversion invDefaultUomConversion = invUnitOfMeasureConversionHome.findUmcByIiNameAndUomName(invItem.getIiName(), invItem.getInvUnitOfMeasure().getUomName(), AD_CMPNY);
            Debug.print("ApReceivingItemEntryControllerBean convertByUomFromAndUomToAndQuantity B");
        	//return EJBCommon.roundIt(QTY_RCVD * invDefaultUomConversion.getUmcConversionFactor() / invUnitOfMeasureConversion.getUmcConversionFactor(), adPreference.getPrfInvQuantityPrecisionUnit());
        	
            return EJBCommon.roundIt(QTY_RCVD * conversionFactor, adPreference.getPrfInvQuantityPrecisionUnit());
        	     	        		        		       	
        						    		        		
        } catch (Exception ex) {
        	
        	Debug.printStackTrace(ex);
        	getSessionContext().setRollbackOnly();
        	throw new EJBException(ex.getMessage());
        	
        }
		
	}
    
    
    
    private void postToPjt(LocalApVoucherLineItem apVoucherLineItem, Date PJT_DT, double PJT_PRJCT_CST,
			String USR_NM, Integer AD_BRNCH, Integer AD_CMPNY)
	{
	
	Debug.print("ApVoucherEntryControllerBean postToPjt");
	
	LocalPmProjectingHome pmProjectingHome = null;
	
	LocalAdPreferenceHome adPreferenceHome = null;
	LocalAdCompanyHome adCompanyHome = null;
	
	// Initialize EJB Home
	
	try {
	
		pmProjectingHome = (LocalPmProjectingHome)EJBHomeFactory.
			lookUpLocalHome(LocalPmProjectingHome.JNDI_NAME, LocalPmProjectingHome.class);
		
		
		adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
		lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);
		adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
		lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
	
	} catch (NamingException ex) {
	
	throw new EJBException(ex.getMessage());
	
	}
	
	try {
	
		LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);
		LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
		LocalInvItemLocation invItemLocation = apVoucherLineItem.getInvItemLocation();
		LocalPmProject pmProject = apVoucherLineItem.getPmProject();
		
		int CST_LN_NMBR = 0;
		
		PJT_PRJCT_CST = EJBCommon.roundIt(PJT_PRJCT_CST, adPreference.getPrfInvCostPrecisionUnit());
		
		String II_NM = invItemLocation.getInvItem().getIiName();
		String LOC_NM = invItemLocation.getInvLocation().getLocName();
		String PRJ_NM = pmProject.getPrjProjectCode();
		
		try {
		
		// generate line number
		
		LocalPmProjecting pmCurrentProjecting = pmProjectingHome.getByMaxPrjLineNumberAndPrjDateToLongAndIiNameAndLocNameAndPrjName(PJT_DT.getTime(), II_NM, LOC_NM, PRJ_NM, AD_BRNCH, AD_CMPNY);
		CST_LN_NMBR = pmCurrentProjecting.getPjtLineNumber() + 1;
		
		} catch (FinderException ex) {
		
		CST_LN_NMBR = 1;
		
		}
		
		// create projecting
		LocalPmProjecting pmProjecting = pmProjectingHome.create(PJT_DT, PJT_DT.getTime(), CST_LN_NMBR, PJT_PRJCT_CST, AD_BRNCH, AD_CMPNY);
		pmProjecting.setApVoucherLineItem(apVoucherLineItem);
		pmProjecting.setInvItemLocation(invItemLocation);
		pmProjecting.setPmProject(apVoucherLineItem.getPmProject());
		
		if(apVoucherLineItem.getPmProjectTypeType() != null) {
		
		pmProjecting.setPmProjectTypeType(apVoucherLineItem.getPmProjectTypeType());
		}
		
		if(apVoucherLineItem.getPmProjectPhase() != null) {
		
		pmProjecting.setPmProjectPhase(apVoucherLineItem.getPmProjectPhase());
		}
	
	} catch (Exception ex) {
	
	Debug.printStackTrace(ex);
	getSessionContext().setRollbackOnly();
	throw new EJBException(ex.getMessage());
	
	}
	
	
	
	}
    
    private void postToInvPo(LocalApPurchaseOrderLine apPurchaseOrderLine, Date CST_DT, double CST_QTY_RCVD, double CST_ITM_CST,
       		double CST_RMNNG_QTY, double CST_RMNNG_VL, double CST_VRNC_VL, String USR_NM, Integer AD_BRNCH, Integer AD_CMPNY) throws
       		AdPRFCoaGlVarianceAccountNotFoundException {
        
        	Debug.print("ApReceivingItemEntryControllerBean postToInv");
        	
        	LocalInvCostingHome invCostingHome = null;
        	LocalAdPreferenceHome adPreferenceHome = null;
        	LocalAdCompanyHome adCompanyHome = null;
        	LocalInvAdjustmentLineHome invAdjustmentLineHome = null;
        	
            // Initialize EJB Home
            
            try {
                
              invCostingHome = (LocalInvCostingHome)EJBHomeFactory.
                  lookUpLocalHome(LocalInvCostingHome.JNDI_NAME, LocalInvCostingHome.class);
              adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
                  lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);
              adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
                  lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);              
              invAdjustmentLineHome = (LocalInvAdjustmentLineHome)EJBHomeFactory.
          	      lookUpLocalHome(LocalInvAdjustmentLineHome.JNDI_NAME, LocalInvAdjustmentLineHome.class);              

            } catch (NamingException ex) {
                
              throw new EJBException(ex.getMessage());
                
            }
          
            try {
            
               LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);
               LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
               LocalInvItemLocation invItemLocation = apPurchaseOrderLine.getInvItemLocation();
               int CST_LN_NMBR = 0;
               
               CST_QTY_RCVD = EJBCommon.roundIt(CST_QTY_RCVD, adPreference.getPrfInvQuantityPrecisionUnit());
               CST_ITM_CST = EJBCommon.roundIt(CST_ITM_CST, adPreference.getPrfInvCostPrecisionUnit());
               CST_RMNNG_QTY = EJBCommon.roundIt(CST_RMNNG_QTY, adPreference.getPrfInvQuantityPrecisionUnit());
               CST_RMNNG_VL = EJBCommon.roundIt(CST_RMNNG_VL, adPreference.getPrfInvCostPrecisionUnit());
               
               try {
               
               	   // generate line number
                   LocalInvCosting invCurrentCosting = invCostingHome.getByMaxCstLineNumberAndCstDateToLongAndIiNameAndLocName(CST_DT.getTime(), invItemLocation.getInvItem().getIiName(), invItemLocation.getInvLocation().getLocName(), AD_BRNCH, AD_CMPNY);
                   CST_LN_NMBR = invCurrentCosting.getCstLineNumber() + 1;
               
               } catch (FinderException ex) {
               
               	   CST_LN_NMBR = 1;
               
               }

               //void subsequent cost variance adjustments
               Collection invAdjustmentLines =invAdjustmentLineHome.findUnvoidAndIsCostVarianceGreaterThanAdjDateAndIlCodeAndBrCode(
               		CST_DT, invItemLocation.getIlCode(), AD_BRNCH, AD_CMPNY);
               Iterator i = invAdjustmentLines.iterator();
               
               while (i.hasNext()){
               	
               	LocalInvAdjustmentLine invAdjustmentLine = (LocalInvAdjustmentLine) i.next();
               	this.voidInvAdjustment(invAdjustmentLine.getInvAdjustment(), AD_BRNCH, AD_CMPNY);
               	
               }
               
               
               // computation
               
               //CST_ITM_CST = amount in pesos      
               /*
               if (apPurchaseOrderLine.getApPurchaseOrder().getPoFreight() > 0) {
    	           double poFreight = apPurchaseOrderLine.getApPurchaseOrder().getPoFreight();
    	           double poTax1 = apPurchaseOrderLine.getApPurchaseOrder().getPoTax1();
    	           double poTax2 = apPurchaseOrderLine.getApPurchaseOrder().getPoTax2();
    	           double poDuties = apPurchaseOrderLine.getApPurchaseOrder().getPoDuties();
    	           double poBrokerageFees = apPurchaseOrderLine.getApPurchaseOrder().getPoBrokerageFees();
    	           double poOthers1 = apPurchaseOrderLine.getApPurchaseOrder().getPoOthers1();		
    	           double poOthers4 = apPurchaseOrderLine.getApPurchaseOrder().getPoOthers4();		
    	
    	
    	           double poRate = apPurchaseOrderLine.getApPurchaseOrder().getPoConversionRate();
    	           double forexDiff = 52.0;
    	           
    	           double baseCost = CST_ITM_CST / poRate;										
    	           double poOthers2 = (baseCost * forexDiff) - CST_ITM_CST;						
    	           double poOthers3 = (((poDuties + poTax1)) / (CST_ITM_CST)) * poOthers2;		
    	           
    	           double miscTaxes = (poFreight + poTax1 + poTax2 + poDuties + poBrokerageFees
    	           			+ poOthers1 + poOthers2 + poOthers3 + poOthers4);
    	           double costPlusTaxes = CST_ITM_CST + miscTaxes;
    	           
    	           double beforeForexDiff = (costPlusTaxes - poOthers2 - poOthers3) / baseCost;
    	           
    	           CST_ITM_CST = beforeForexDiff * baseCost;
    	           
    	           apPurchaseOrderLine.getApPurchaseOrder().setPoOthers2(poOthers2);
    	           apPurchaseOrderLine.getApPurchaseOrder().setPoOthers3(poOthers3);
               }*/
               
               String prevExpiryDates = "";
               String miscListPrpgt ="";
               double qtyPrpgt = 0;
               try {           
            	   LocalInvCosting prevCst = invCostingHome.getByMaxCstDateToLongAndMaxCstLineNumberAndLessThanEqualCstDateAndRemainingQuantityNotEqualToZeroAndIlCode(
            			   CST_DT, invItemLocation.getIlCode(), AD_BRNCH, AD_CMPNY);

            	   prevExpiryDates = prevCst.getCstExpiryDate();
            	   qtyPrpgt = prevCst.getCstRemainingQuantity();
            	   if (prevExpiryDates==null){
            		   prevExpiryDates="";
            	   }

               }catch (Exception ex){
            	   prevExpiryDates="";
               }
               // create costing
               LocalInvCosting invCosting = invCostingHome.create(CST_DT, CST_DT.getTime(), CST_LN_NMBR, CST_QTY_RCVD, CST_ITM_CST, 0d, 0d, 0d, 0d, 0d, 0d, CST_RMNNG_QTY, CST_RMNNG_VL, 0d, 0d, CST_QTY_RCVD > 0 ? CST_QTY_RCVD : 0, AD_BRNCH, AD_CMPNY);
               invCosting.setCstQCNumber(apPurchaseOrderLine.getPlQcNumber());
               invCosting.setCstExpiryDate(apPurchaseOrderLine.getPlQcExpiryDate().toString());
               
               invCosting.setInvItemLocation(invItemLocation);
               invCosting.setApPurchaseOrderLine(apPurchaseOrderLine);
               
               //Get Latest Expiry Dates     
               
               if(prevExpiryDates!=null && prevExpiryDates!="" && prevExpiryDates.length()!=0){
            	   
            	   if(apPurchaseOrderLine.getPlMisc()!=null && apPurchaseOrderLine.getPlMisc()!="" && apPurchaseOrderLine.getPlMisc().length()!=0){
            		   
            		   int qty2Prpgt = Integer.parseInt(this.getQuantityExpiryDates(apPurchaseOrderLine.getPlMisc()));
            		   String miscList2Prpgt = this.propagateExpiryDates(apPurchaseOrderLine.getPlMisc(), qty2Prpgt);
            		   prevExpiryDates = prevExpiryDates.substring(1);
            		   String propagateMiscPrpgt = miscList2Prpgt + prevExpiryDates;
            		   
            		   invCosting.setCstExpiryDate(propagateMiscPrpgt);
            	   }else{
            		   invCosting.setCstExpiryDate(prevExpiryDates);
            	   }   
               }else{
            	   if(apPurchaseOrderLine.getPlMisc()!=null && apPurchaseOrderLine.getPlMisc()!="" && apPurchaseOrderLine.getPlMisc().length()!=0){
            		   int initialQty = Integer.parseInt(this.getQuantityExpiryDates(apPurchaseOrderLine.getPlMisc()));
            		   String initialPrpgt = this.propagateExpiryDates(apPurchaseOrderLine.getPlMisc(), initialQty);
            		   
            		   invCosting.setCstExpiryDate(initialPrpgt);
            	   }else{
            		   invCosting.setCstExpiryDate(prevExpiryDates);
            	   }	   
               }
              
               
               
    			   
    			// if cost variance is not 0, generate cost variance for the transaction 
    			if(CST_VRNC_VL != 0) {
    				
    				this.generateCostVariance(invCosting.getInvItemLocation(), CST_VRNC_VL,
    						"APRI" + apPurchaseOrderLine.getApPurchaseOrder().getPoDocumentNumber(),
    						apPurchaseOrderLine.getApPurchaseOrder().getPoDescription(),
    						apPurchaseOrderLine.getApPurchaseOrder().getPoDate(), USR_NM, AD_BRNCH, AD_CMPNY);
    				
    			}

               // propagate balance if necessary    
    			
               Collection invCostings = invCostingHome.findByGreaterThanCstDateAndIiNameAndLocName(CST_DT, invItemLocation.getInvItem().getIiName(), invItemLocation.getInvLocation().getLocName(), AD_BRNCH, AD_CMPNY);
               String miscList = "";
               i = invCostings.iterator();
               if(apPurchaseOrderLine.getPlMisc()!=null && apPurchaseOrderLine.getPlMisc()!="" && apPurchaseOrderLine.getPlMisc().length()!=0){
            	   double qty = Double.parseDouble(this.getQuantityExpiryDates(apPurchaseOrderLine.getPlMisc()));
            	   miscList = this.propagateExpiryDates(apPurchaseOrderLine.getPlMisc(), qty);
               }
              // miscList = miscList.substring(1);
               while (i.hasNext()) {
                   
                   LocalInvCosting invPropagatedCosting = (LocalInvCosting)i.next();
                   
            	   invPropagatedCosting.setCstRemainingQuantity(invPropagatedCosting.getCstRemainingQuantity() + CST_QTY_RCVD);
                   invPropagatedCosting.setCstRemainingValue(invPropagatedCosting.getCstRemainingValue() + CST_ITM_CST);
     			   //String miscList2 = propagateMisc;//this.propagateExpiryDates(invCosting.getCstExpiryDate(), qty);
                   /*
                   if(apPurchaseOrderLine.getPlMisc()!=null && apPurchaseOrderLine.getPlMisc()!="" && apPurchaseOrderLine.getPlMisc().length()!=0){
                	   

                	   String propagateMisc = miscList+invPropagatedCosting.getCstExpiryDate().substring(1);

                	   invPropagatedCosting.setCstExpiryDate(propagateMisc);
                   }else{
                	   invPropagatedCosting.setCstExpiryDate(invPropagatedCosting.getCstExpiryDate());
                   }*/
                   
               }                           
                         
               // regenerate cost variance
               this.regenerateCostVariance(invCostings, invCosting, AD_BRNCH, AD_CMPNY);

            } catch (AdPRFCoaGlVarianceAccountNotFoundException ex){
            	
            	throw ex;
            	
            } catch (Exception ex) {
          	
          	   Debug.printStackTrace(ex);
          	   getSessionContext().setRollbackOnly();
               throw new EJBException(ex.getMessage());
             
            }
        
        	
        
        }
    
    private byte sendEmail(LocalAdApprovalQueue adApprovalQueue, Integer AD_CMPNY) {
		
		Debug.print("ApPurchaseRequisitionEntryControllerBean sendEmail");
		
		
		LocalApPurchaseRequisitionHome apPurchaseRequisitionHome = null;
		LocalApPurchaseOrderHome apPurchaseOrderHome = null;
		LocalApCanvassHome apCanvassHome = null;
		LocalAdPreferenceHome adPreferenceHome = null;
		
		
		// Initialize EJB Home
		
		try {

			apPurchaseRequisitionHome = (LocalApPurchaseRequisitionHome)EJBHomeFactory.
					lookUpLocalHome(LocalApPurchaseRequisitionHome.JNDI_NAME, LocalApPurchaseRequisitionHome.class);
					
			apPurchaseOrderHome = (LocalApPurchaseOrderHome)EJBHomeFactory.
					lookUpLocalHome(LocalApPurchaseOrderHome.JNDI_NAME, LocalApPurchaseOrderHome.class);
			
			apCanvassHome = (LocalApCanvassHome)EJBHomeFactory.
					lookUpLocalHome(LocalApCanvassHome.JNDI_NAME, LocalApCanvassHome.class);
			
			adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
					lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);
			
		} catch (NamingException ex) {
			
			throw new EJBException(ex.getMessage());
			
		}
		
		StringBuilder composedEmail = new StringBuilder();
		LocalApPurchaseRequisition apPurchaseRequisition = null;
		LocalApPurchaseOrder apPurchaseOrder = null;
		LocalAdPreference adPreference = null;
		
		String subjectMessage = "";
		String messageContent = "";
		String emailTo = "";
		
		
		
		try {

			adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);

		
			messageContent = "";
			
			final String emailFrom = adPreference.getPrfMailFrom();
		    emailTo = adApprovalQueue.getAdUser().getUsrEmailAddress();
			final String emailPassword = adPreference.getPrfMailPassword();
			boolean isAuthenticator = adPreference.getPrfMailAuthenticator()==EJBCommon.TRUE?true:false;
			
			
			HashMap hm = new HashMap();
			
			if(adApprovalQueue.getAqDocument().equals("AP PURCHASE REQUISITION")){
			
				 apPurchaseRequisition = apPurchaseRequisitionHome.findByPrimaryKey(adApprovalQueue.getAqDocumentCode());
				 subjectMessage = "DALTRON - OFS - PURCHASE REQUISTION APPROVAL PR #:"+apPurchaseRequisition.getPrNumber();
				 
				 Iterator i = apPurchaseRequisition.getApPurchaseRequisitionLines().iterator();
			
				while(i.hasNext()) {
					
					LocalApPurchaseRequisitionLine apPurchaseRequisitionLine = (LocalApPurchaseRequisitionLine)i.next();
					
					Collection apCanvasses = apCanvassHome.findByPrlCodeAndCnvPo(
							apPurchaseRequisitionLine.getPrlCode(), EJBCommon.TRUE, AD_CMPNY);
					
					Iterator j = apCanvasses.iterator();
	
					
					while(j.hasNext()) {
						
						LocalApCanvass apCanvass = (LocalApCanvass)j.next();
						
						
						if (hm.containsKey(apCanvass.getApSupplier().getSplSupplierCode())){
							AdModApprovalQueueDetails adModApprovalQueueExistingDetails = (AdModApprovalQueueDetails)
			        				 hm.get(apCanvass.getApSupplier().getSplSupplierCode());
							
							adModApprovalQueueExistingDetails.setAqSupplierCode(apCanvass.getApSupplier().getSplSupplierCode());
							adModApprovalQueueExistingDetails.setAqSupplierName(apCanvass.getApSupplier().getSplName());
							adModApprovalQueueExistingDetails.setAqAmount(adModApprovalQueueExistingDetails.getAqAmount() +  apCanvass.getCnvAmount());
	
						} else {
							
							AdModApprovalQueueDetails adModApprovalQueueNewDetails = new AdModApprovalQueueDetails();
	
							adModApprovalQueueNewDetails.setAqSupplierCode(apCanvass.getApSupplier().getSplSupplierCode());
							adModApprovalQueueNewDetails.setAqSupplierName(apCanvass.getApSupplier().getSplName());
							adModApprovalQueueNewDetails.setAqAmount(apCanvass.getCnvAmount());
	
							hm.put(apCanvass.getApSupplier().getSplSupplierCode(), adModApprovalQueueNewDetails);	
						}
					}
				}
				Set set = hm.entrySet();
			
				composedEmail.append("<table border='1' style='width:100%'>");
	
				composedEmail.append("<tr>");
				composedEmail.append("<th> VENDOR </th>");
				composedEmail.append("<th> AMOUNT </th>");
				composedEmail.append("</tr>");
	
				Iterator x = set.iterator();
	
				while(x.hasNext()) {
					
				
					Map.Entry me = (Map.Entry)x.next();
	
					AdModApprovalQueueDetails adModApprovalQueueDetails = (AdModApprovalQueueDetails)
								me.getValue();
					
					composedEmail.append("<tr>");
					composedEmail.append("<td>");
					composedEmail.append(adModApprovalQueueDetails.getAqSupplierName());
					composedEmail.append("</td>");
					composedEmail.append("<td>");
					composedEmail.append(adModApprovalQueueDetails.getAqAmount());
					composedEmail.append("</td>");
					composedEmail.append("</tr>");
	
				}
				
				composedEmail.append("</table></body></html>");
				
				messageContent =  "Dear "+adApprovalQueue.getAdUser().getUsrDescription()+",<br><br>"+
		              "A purchase request was raised by "+adApprovalQueue.getAqRequesterName()+" for your approval.<br>"+
		              "PR Number: "+adApprovalQueue.getAqDocumentNumber()+".<br>"+
		              "Delivery Period: "+EJBCommon.convertSQLDateToString(apPurchaseRequisition.getPrDeliveryPeriod())+".<br>"+
		              "Description: "+apPurchaseRequisition.getPrDescription()+".<br>"+
		              composedEmail.toString() +
		              
		              "Please click the link <a href=\"http://live.daltronpng.com\">http://live.daltronpng.com:8080/daltron</a>.<br><br><br>"+
		              
		              "This is an automated message and was sent from a notification-only email address.<br><br>"+
		              "Please do not reply to this message.<br><br>";
			}
			if(adApprovalQueue.getAqDocument().equals("AP RECEIVING ITEM")){
			
				 apPurchaseOrder = apPurchaseOrderHome.findByPrimaryKey(adApprovalQueue.getAqDocumentCode());
				 subjectMessage = "DALTRON - OFS - RECEIVING ITEM APPROVAL RI #:"+apPurchaseOrder.getPoDocumentNumber();
				 
				 composedEmail.append("<table border='1' style='width:100%'>");
	
				 composedEmail.append("<tr>");
				 composedEmail.append("<th> VENDOR </th>");
				 composedEmail.append("<th> AMOUNT </th>");
				 composedEmail.append("</tr>");
				
				 composedEmail.append("<tr>");
				 composedEmail.append("<td>");
				 composedEmail.append(apPurchaseOrder.getApSupplier().getSplName());
				 composedEmail.append("</td>");
				 composedEmail.append("<td>");
				 composedEmail.append(apPurchaseOrder.getPoTotalAmount());
				 composedEmail.append("</td>");
				 composedEmail.append("</tr>");
				 composedEmail.append("</table></body></html>");
				 
				 messageContent =  "Dear "+adApprovalQueue.getAdUser().getUsrDescription()+",<br><br>"+
		              "A purchase request was raised by "+adApprovalQueue.getAqRequesterName()+" for your approval.<br>"+
		              "RI Number: "+adApprovalQueue.getAqDocumentNumber()+".<br>"+
		              "Date: "+EJBCommon.convertSQLDateToString(apPurchaseOrder.getPoDate())+".<br>"+
		              "Description: "+apPurchaseOrder.getPoDescription()+".<br>"+
		              composedEmail.toString() +
		              
		              "Please click the link <a href=\"http://live.daltronpng.com\">http://live.daltronpng.com:8080/daltron</a>.<br><br><br>"+
		              
		              "This is an automated message and was sent from a notification-only email address.<br><br>"+
		              "Please do not reply to this message.<br><br>";
			}
			

		
			
			
			
			
		} catch (Exception ex) {
			
			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());
			
		}
		
			
	
		

		System.out.println("email to:"  + emailTo);
		Properties props = new Properties();
		

		
	
		props.put("mail.smtp.host", adPreference.getPrfMailHost());
	
		props.put("mail.smtp.socketFactory.port",adPreference.getPrfMailSocketFactoryPort());
	//	props.put("mail.smtp.auth", adPreference.getPrfMailAuthenticator()); //this will allow to not use username nad password
		props.put("mail.smtp.port", adPreference.getPrfMailPort());
	
	
		
		System.out.println("Mail host:" + props.get("mail.smtp.host"));
		System.out.println("socket factory:" + props.get("mail.smtp.socketFactory.port"));
		System.out.println("port:" + props.get("mail.smtp.port"));
		System.out.println("email from:" + adPreference.getPrfMailFrom());
		System.out.println("email to:" + emailTo);
		

		final String emailFrom = adPreference.getPrfMailFrom();
		final String emailPassword = adPreference.getPrfMailPassword();
		boolean isAuthenticator = adPreference.getPrfMailAuthenticator()==EJBCommon.TRUE?true:false;
		
		
		props.put("mail.smtp.socketFactory.class",
				"javax.net.ssl.SSLSocketFactory");
		
		Session session;

		
		if(isAuthenticator) {
			
			props.put("mail.smtp.starttls.enable", "true");
		
			
			props.put("mail.smtp.auth", "true");
			session = Session.getInstance(props,
			new javax.mail.Authenticator() {
				protected PasswordAuthentication getPasswordAuthentication() {
					return new PasswordAuthentication(emailFrom,emailPassword);
				}
			});
			
			
			
			
		}else {
			session = Session.getInstance(props, null);
		}
		

	
		try {

			Message message = new MimeMessage(session);
			message.setFrom(new InternetAddress(emailFrom));

			message.setRecipients(Message.RecipientType.TO,
					InternetAddress.parse(emailTo));
			
			message.setRecipients(Message.RecipientType.BCC,
					InternetAddress.parse(adPreference.getPrfMailBcc()));

			message.setSubject(subjectMessage);
		
			System.out.println("adApprovalQueue.getAqRequesterName()="+adApprovalQueue.getAqRequesterName());
			System.out.println("adApprovalQueue.getAqDocumentNumber()="+adApprovalQueue.getAqDocumentNumber());
			System.out.println("adApprovalQueue.getAdUser().getUsrDescription()="+adApprovalQueue.getAdUser().getUsrDescription());

			message.setContent(messageContent,"text/html");
			

			Transport.send(message);
			adApprovalQueue.setAqEmailSent(EJBCommon.TRUE);
			
			System.out.println("Done");

		} catch (MessagingException e) {
			throw new RuntimeException(e);
		} catch( Exception ex) {
			System.out.println("caught error");
			System.out.println(ex.toString());
		}
		
		return 0;
	}
    
    
    

    // SessionBean methods

    /**
     * @ejb:create-method view-type="remote"
     **/
    public void ejbCreate() throws CreateException {

       Debug.print("ApApprovalControllerBean ejbCreate");
      
    }
}
