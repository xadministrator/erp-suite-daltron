/*
 * ApCanvassControllerBean.java
 *
 * Created on April 20, 2006 02:00 PM
 * @author  Aliza D.J. Anos
 */

package com.ejb.txn;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;
import javax.naming.NamingException;

import com.ejb.ad.LocalAdCompany;
import com.ejb.ad.LocalAdCompanyHome;
import com.ejb.ad.LocalAdPreference;
import com.ejb.ad.LocalAdPreferenceHome;
import com.ejb.ap.LocalApCanvass;
import com.ejb.ap.LocalApCanvassHome;
import com.ejb.ap.LocalApPurchaseRequisitionLine;
import com.ejb.ap.LocalApPurchaseRequisitionLineHome;
import com.ejb.ap.LocalApSupplier;
import com.ejb.ap.LocalApSupplierHome;
import com.ejb.ar.LocalArStandardMemoLine;
import com.ejb.ar.LocalArStandardMemoLineHome;
import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.exception.GlobalRecordAlreadyExistException;
import com.util.AbstractSessionBean;
import com.util.ApModCanvassDetails;
import com.util.ApModPurchaseRequisitionLineDetails;
import com.util.ArStandardMemoLineDetails;
import com.util.Debug;
import com.util.EJBCommon;
import com.util.EJBHomeFactory;

/**
 * @ejb:bean name="ApCanvassControllerEJB"
 *           display-name="used for entering canvass lines"
 *           type="Stateless"
 *           view-type="remote"
 *           jndi-name="ejb/ApCanvassControllerEJB"
 *
 * @ejb:interface remote-class="com.ejb.txn.ApCanvassController"
 *                extends="javax.ejb.EJBObject"
 *
 * @ejb:home remote-class="com.ejb.txn.ApCanvassControllerHome"
 *           extends="javax.ejb.EJBHome"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="apuser"
 *                        role-link="apuserlink"
 *
 * @ejb:permission role-name="apuser"
 * 
 */

public class ApCanvassControllerBean extends AbstractSessionBean {
	
	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public ArrayList getApSplAll(Integer AD_BRNCH, Integer AD_CMPNY) {
		
		Debug.print("ApCanvassControllerBean getApSplAll");
		
		LocalApSupplierHome apSupplierHome = null;
		
		ArrayList list = new ArrayList();
		
		// Initialize EJB Home
		
		try {
			
			apSupplierHome = (LocalApSupplierHome)EJBHomeFactory.
				lookUpLocalHome(LocalApSupplierHome.JNDI_NAME, LocalApSupplierHome.class);
			
		} catch (NamingException ex) {
			
			throw new EJBException(ex.getMessage());
			
		}
		
		try {
			
			Collection apSuppliers = apSupplierHome.findEnabledSplAll(AD_BRNCH, AD_CMPNY);
			
			Iterator i = apSuppliers.iterator();
			
			while (i.hasNext()) {
				
				LocalApSupplier apSupplier = (LocalApSupplier)i.next();
				
				list.add(apSupplier.getSplSupplierCode());
				
			}
			
			return list;
			
		} catch (Exception ex) {
			
			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());
			
		}
		
	}
	
	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public ApModPurchaseRequisitionLineDetails getApPrlByPrlCode(Integer PRL_CODE, Integer AD_CMPNY) {
		
		Debug.print("ApCanvassControllerBean getApPrlByPrlCode");
		
		LocalApPurchaseRequisitionLineHome apPurchaseRequisitionLineHome = null;
		
		//Initialize EJB Home
		
		try {
			
			apPurchaseRequisitionLineHome = (LocalApPurchaseRequisitionLineHome)EJBHomeFactory.
				lookUpLocalHome(LocalApPurchaseRequisitionLineHome.JNDI_NAME, LocalApPurchaseRequisitionLineHome.class);
			
		} catch (NamingException ex) {
			
			throw new EJBException(ex.getMessage());
			
		}
		
		try {
			
			LocalApPurchaseRequisitionLine apPurchaseRequisitionLine = apPurchaseRequisitionLineHome.findByPrimaryKey(PRL_CODE);
			
			ApModPurchaseRequisitionLineDetails details = new ApModPurchaseRequisitionLineDetails();
			
			details.setPrlPrNumber(apPurchaseRequisitionLine.getApPurchaseRequisition().getPrNumber());
			details.setPrlPrDate(apPurchaseRequisitionLine.getApPurchaseRequisition().getPrDate());
			details.setPrlIlIiName(apPurchaseRequisitionLine.getInvItemLocation().getInvItem().getIiName());
			details.setPrlIlLocName(apPurchaseRequisitionLine.getInvItemLocation().getInvLocation().getLocName());
			details.setPrlUomName(apPurchaseRequisitionLine.getInvUnitOfMeasure().getUomName());
			details.setPrlQuantity(apPurchaseRequisitionLine.getPrlQuantity());
			details.setPrlAmount(apPurchaseRequisitionLine.getPrlAmount());
			details.setPrlIlIiDescription(apPurchaseRequisitionLine.getInvItemLocation().getInvItem().getIiDescription());
			Collection apCanvasses = apPurchaseRequisitionLine.getApCanvasses();

			//get canvass lines
			
			Iterator i = apCanvasses.iterator();
			
			while(i.hasNext()) {
				
				LocalApCanvass apCanvass = (LocalApCanvass)i.next();
				
				ApModCanvassDetails cnvDetails = new ApModCanvassDetails();
				
				cnvDetails.setCnvLine(apCanvass.getCnvLine());
				System.out.println("REMARKS="+apCanvass.getCnvRemarks());
				cnvDetails.setCnvRemarks(apCanvass.getCnvRemarks());
				cnvDetails.setCnvSplSupplierCode(apCanvass.getApSupplier().getSplSupplierCode());
				cnvDetails.setCnvQuantity(apCanvass.getCnvQuantity());
				cnvDetails.setCnvUnitCost(apCanvass.getCnvUnitCost());
				cnvDetails.setCnvAmount(apCanvass.getCnvAmount());
				cnvDetails.setCnvPo(apCanvass.getCnvPo());
				cnvDetails.setCnvSplSupplierName(apCanvass.getApSupplier().getSplName());
				
				details.saveCnvList(cnvDetails);
				
			}
			
			return details;
			
		} catch (Exception ex) {
			
			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());
			
		}
		
	}
	
	/*
	*//**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **//*
    public com.util.ApCanvassDetails getApSplBySplName(String SML_NM, Integer AD_CMPNY) 
    throws GlobalNoRecordFoundException {
        
        Debug.print("ApCanvassControllerBean getApSplBySplName");
        
        LocalApSupplierHome apSupplierHome = null;   
          
        
        // Initialize EJB Home
        
        try {
            
        	apSupplierHome = (LocalApSupplierHome)EJBHomeFactory.
            lookUpLocalHome(LocalApSupplierHome.JNDI_NAME, LocalApSupplierHome.class);
            
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }        
        
        try {
            
            LocalApSupplier apSupplier = null;
            
            
            try {
                
                arStandardMemoLine = arStandardMemoLineHome.findBySmlName(SML_NM, AD_CMPNY);
                
                apSupplier = apSupplierHome.findBySplName(SPL_NM, SPL_AD_CMPNY)
                
            } catch (FinderException ex) {
                
                throw new GlobalNoRecordFoundException();
                
            }
            
            ArStandardMemoLineDetails details = new ArStandardMemoLineDetails();
            details.setSmlDescription(arStandardMemoLine.getSmlDescription());
            details.setSmlUnitPrice(arStandardMemoLine.getSmlUnitPrice());
            details.setSmlTax(arStandardMemoLine.getSmlTax());
            
            return details;
            
        } catch (GlobalNoRecordFoundException ex) {
            
            throw ex;
            
        } catch (Exception ex) {
            
            Debug.printStackTrace(ex);
            throw new EJBException(ex.getMessage());
            
        }
        
    }*/
    
	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public void saveApCnvss(ArrayList cnvList, Integer PRL_CODE, Integer AD_CMPNY) 
		throws GlobalRecordAlreadyExistException {
		
		Debug.print("ApCanvassControllerBean saveApCnvss");

		LocalApCanvassHome apCanvassHome = null;
		LocalApPurchaseRequisitionLineHome apPurchaseRequisitionLineHome = null;
		LocalApSupplierHome apSupplierHome = null;
		
		//Initialize EJB Home
		
		try {
			
			apCanvassHome = (LocalApCanvassHome)EJBHomeFactory.
				lookUpLocalHome(LocalApCanvassHome.JNDI_NAME, LocalApCanvassHome.class);
			apPurchaseRequisitionLineHome = (LocalApPurchaseRequisitionLineHome)EJBHomeFactory.
				lookUpLocalHome(LocalApPurchaseRequisitionLineHome.JNDI_NAME, LocalApPurchaseRequisitionLineHome.class);
			apSupplierHome = (LocalApSupplierHome)EJBHomeFactory.
				lookUpLocalHome(LocalApSupplierHome.JNDI_NAME, LocalApSupplierHome.class);
			
		} catch (NamingException ex) {
			
			throw new EJBException(ex.getMessage());
			
		}
		
		try {
			
			LocalApPurchaseRequisitionLine apPurchaseRequisitionLine = apPurchaseRequisitionLine = apPurchaseRequisitionLineHome.findByPrimaryKey(PRL_CODE);
			
			Collection apCanvasses = apPurchaseRequisitionLine.getApCanvasses();
			
			//remove all canvass lines
			
			Iterator i = apCanvasses.iterator();
			
			while(i.hasNext()) {
				
				LocalApCanvass apCanvass = (LocalApCanvass)i.next();
				
				i.remove();
				
				apCanvass.remove();
				
			}
			
			//add new canvass lines to purchase requisition line
			
			double PRL_AMNT = 0d;
			double PRL_QTTY = 0d;
			
			i = cnvList.iterator();
			
			while(i.hasNext()) {
				
				ApModCanvassDetails mdetails = (ApModCanvassDetails)i.next();
				System.out.println("REMARKS="+mdetails.getCnvRemarks());
				LocalApCanvass apCanvass = apCanvassHome.create(mdetails.getCnvLine(), mdetails.getCnvRemarks(),
						mdetails.getCnvQuantity(), mdetails.getCnvUnitCost(),
						EJBCommon.roundIt(mdetails.getCnvQuantity() * mdetails.getCnvUnitCost(), this.getGlFcPrecisionUnit(AD_CMPNY)),
						mdetails.getCnvPo(), AD_CMPNY);
				
				apCanvasses = apPurchaseRequisitionLine.getApCanvasses();
				
				LocalApSupplier apSupplier = apSupplierHome.findBySplSupplierCode(mdetails.getCnvSplSupplierCode(), AD_CMPNY);
				
				if(this.hasSupplier(apPurchaseRequisitionLine, apSupplier.getSplSupplierCode())) {
					
					throw new GlobalRecordAlreadyExistException(apSupplier.getSplSupplierCode());
					
				}
					
				apSupplier.addApCanvass(apCanvass);
				apPurchaseRequisitionLine.addApCanvass(apCanvass);
				if(apCanvass.getCnvPo() == 1){
				PRL_AMNT += apCanvass.getCnvAmount();
				
				PRL_QTTY += apCanvass.getCnvQuantity();
				}
				
			}
			System.out.println("PRL_QTTY-----"+PRL_QTTY);
			//apPurchaseRequisitionLine.setPrlAmount(PRL_AMNT);
			//apPurchaseRequisitionLine.setPrlQuantity(PRL_QTTY);
			
		} catch (GlobalRecordAlreadyExistException ex) {
			
			getSessionContext().setRollbackOnly();
			throw ex;
			
		} catch (Exception ex) {
			
			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());
			
		}
		
	}
	
	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public short getGlFcPrecisionUnit(Integer AD_CMPNY) {
		
		Debug.print("ApCanvassControllerBean getGlFcPrecisionUnit");
		
		LocalAdCompanyHome adCompanyHome = null;
		
		
		// Initialize EJB Home
		
		try {
			
			adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);       
			
		} catch (NamingException ex) {
			
			throw new EJBException(ex.getMessage());
			
		}
		
		try {
			
			LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
			
			return  adCompany.getGlFunctionalCurrency().getFcPrecision();
			
		} catch (Exception ex) {
			
			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());
			
		}
		
	}
	
	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public short getInvGpQuantityPrecisionUnit(Integer AD_CMPNY) {
		
		Debug.print("ApCanvassControllerBean getInvGpQuantityPrecisionUnit");
		
		LocalAdPreferenceHome adPreferenceHome = null;         
		
		// Initialize EJB Home
		
		try {
			
			adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);       
			
		} catch (NamingException ex) {
			
			throw new EJBException(ex.getMessage());
			
		}
		
		
		try {
			
			LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);
			
			return adPreference.getPrfInvQuantityPrecisionUnit();
			
		} catch (Exception ex) {
			
			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());
			
		}
		
	}
	
	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public short getAdPrfApJournalLineNumber(Integer AD_CMPNY) {
		
		Debug.print("ApCanvassControllerBean getAdPrfApJournalLineNumber");
		
		LocalAdPreferenceHome adPreferenceHome = null;
		
		// Initialize EJB Home
		
		try {
			
			adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);       
			
		} catch (NamingException ex) {
			
			throw new EJBException(ex.getMessage());
			
		}
		
		
		try {
			
			LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);
			
			return adPreference.getPrfApJournalLineNumber();
			
		} catch (Exception ex) {
			
			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());
			
		}
		
	}
	
	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public byte getAdPrfApUseSupplierPulldown(Integer AD_CMPNY) {
		
		Debug.print("ApCanvassControllerBean getAdPrfApUseSupplierPulldown");
		
		LocalAdPreferenceHome adPreferenceHome = null;
		
		
		// Initialize EJB Home
		
		try {
			
			adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);       
			
		} catch (NamingException ex) {
			
			throw new EJBException(ex.getMessage());
			
		}
		
		
		try {
			
			LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);
			
			return adPreference.getPrfApUseSupplierPulldown();
			
		} catch (Exception ex) {
			
			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());
			
		}
		
	}
	
	//private methods
	
	private boolean hasSupplier(LocalApPurchaseRequisitionLine apPurchaseRequisitionLine, String SPL_SPPLR_CD) {
		
		Debug.print("ApCanvassControllerBean hasSupplier");
		
		Collection apCanvasses = apPurchaseRequisitionLine.getApCanvasses();
		
		Iterator i = apCanvasses.iterator();
		
		while(i.hasNext()) {
			
			LocalApCanvass apCanvass = (LocalApCanvass)i.next();
			
			if(apCanvass.getApSupplier().getSplSupplierCode().equals(SPL_SPPLR_CD)) {
				
				return true;
				
			}
			
		}
		
		return false;
		
	}
	
	//	 SessionBean methods
	
	/**
	 * @ejb:create-method view-type="remote"
	 **/
	public void ejbCreate() throws CreateException {
		
		Debug.print("ApCanvassControllerBean ejbCreate");
		
	}
	
}