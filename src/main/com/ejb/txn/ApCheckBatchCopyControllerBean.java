
/*
 * ApCheckBatchCopyControllerBean.java
 *
 * Created on May 27, 2004, 8:10 AM
 *
 * @author  Neil Andrew M. Ajero
 */

package com.ejb.txn;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.naming.NamingException;

import com.ejb.ad.LocalAdCompany;
import com.ejb.ad.LocalAdCompanyHome;
import com.ejb.ap.LocalApCheck;
import com.ejb.ap.LocalApCheckBatch;
import com.ejb.ap.LocalApCheckBatchHome;
import com.ejb.ap.LocalApCheckHome;
import com.ejb.exception.GlobalBatchCopyInvalidException;
import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.exception.GlobalTransactionBatchCloseException;
import com.util.AbstractSessionBean;
import com.util.ApModCheckDetails;
import com.util.Debug;
import com.util.EJBHomeFactory;

/**
 * @ejb:bean name="ApCheckBatchCopyControllerEJB"
 *           display-name="Used for copying check batch"
 *           type="Stateless"
 *           view-type="remote"
 *           jndi-name="ejb/ApCheckBatchCopyControllerEJB"
 *
 * @ejb:interface remote-class="com.ejb.txn.ApCheckBatchCopyController"
 *                extends="javax.ejb.EJBObject"
 *
 * @ejb:home remote-class="com.ejb.txn.ApCheckBatchCopyControllerHome"
 *           extends="javax.ejb.EJBHome"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="apuser"
 *                        role-link="apuserlink"
 *
 * @ejb:permission role-name="apuser"
 * 
*/

public class ApCheckBatchCopyControllerBean extends AbstractSessionBean {

    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList getApOpenCbAll(Integer AD_BRNCH, Integer AD_CMPNY) {
                    
        Debug.print("ApCheckBatchCopyControllerBean getApOpenCbAll");
        
        LocalApCheckBatchHome apCheckBatchHome = null;               
        
        ArrayList list = new ArrayList();
        
        // Initialize EJB Home
        
        try {
            
        	apCheckBatchHome = (LocalApCheckBatchHome)EJBHomeFactory.
                lookUpLocalHome(LocalApCheckBatchHome.JNDI_NAME, LocalApCheckBatchHome.class);
            
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }        
        
        try {
        	
        	Collection apCheckBatches = apCheckBatchHome.findOpenCbAll(AD_BRNCH, AD_CMPNY);
        	
        	Iterator i = apCheckBatches.iterator();
        	
        	while (i.hasNext()) {
        		
        		LocalApCheckBatch apCheckBatch = (LocalApCheckBatch)i.next();
        		
        		list.add(apCheckBatch.getCbName());
        		
        	}
        	
        	return list;
        	
        } catch (Exception ex) {
        	
        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());
        	
        }
            
    }  
    
    /**
     * @ejb:interface-method view-type="remote"
     **/
    public void executeApChkBatchCopy(ArrayList list, String CB_NM_TO, Integer AD_BRNCH, Integer AD_CMPNY) throws
        GlobalTransactionBatchCloseException,
		GlobalBatchCopyInvalidException {
                    
        Debug.print("ApCheckBatchCopyControllerBean executeApChkBatchCopy");
        
        LocalApCheckHome apCheckHome = null;
        LocalApCheckBatchHome apCheckBatchHome = null; 

        // Initialize EJB Home
        
        try {
            
        	apCheckHome = (LocalApCheckHome)EJBHomeFactory.
			    lookUpLocalHome(LocalApCheckHome.JNDI_NAME, LocalApCheckHome.class);
        	apCheckBatchHome = (LocalApCheckBatchHome)EJBHomeFactory.
                lookUpLocalHome(LocalApCheckBatchHome.JNDI_NAME, LocalApCheckBatchHome.class);
                         
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }

        try {

        	// find check batch to
        	
        	LocalApCheckBatch apCheckBatchTo = apCheckBatchHome.findByCbName(CB_NM_TO, AD_BRNCH, AD_CMPNY);

        	// validate if batch to is closed
        	
        	if(apCheckBatchTo.getCbStatus().equals("CLOSED")) {
        		
        		throw new GlobalTransactionBatchCloseException();
        		
        	}
        	    			
        	Iterator i = list.iterator();
        	
        	while (i.hasNext()) {
        		
        		LocalApCheck apCheck = apCheckHome.findByPrimaryKey((Integer)i.next());
        		
        		if (!apCheck.getApCheckBatch().getCbType().equals(apCheckBatchTo.getCbType())) {
        			
        			throw new GlobalBatchCopyInvalidException();
        			
        		}
        		
        		apCheck.getApCheckBatch().dropApCheck(apCheck);        		
        		apCheckBatchTo.addApCheck(apCheck);

        	}

        } catch (GlobalTransactionBatchCloseException ex) {
       	   
       	   getSessionContext().setRollbackOnly();    	
       	   throw ex;	
       	   
        } catch (GlobalBatchCopyInvalidException ex) {
        	   
    	   getSessionContext().setRollbackOnly();    	
    	   throw ex;
                                    
        } catch (Exception ex) {
       	 
       	   Debug.printStackTrace(ex);
       	   getSessionContext().setRollbackOnly();
           throw new EJBException(ex.getMessage());
         
        }

    }
    
    
    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
     public ArrayList getApChkByCbName(String CB_NM, Integer AD_CMPNY)
  	  throws GlobalNoRecordFoundException {
  	
  	  Debug.print("ApCheckBatchCopyControllerBean getApChkByCbName");
  	  
  	  LocalApCheckHome apCheckHome = null;
  	  
  	  // Initialize EJB Home
  	    
  	  try {
  	  	
  	      apCheckHome = (LocalApCheckHome)EJBHomeFactory.
  	          lookUpLocalHome(LocalApCheckHome.JNDI_NAME, LocalApCheckHome.class);          
  	        
  	  } catch (NamingException ex) {
  	        
  	      throw new EJBException(ex.getMessage());
  	        
  	  }
  	  
  	  try { 
  	      
  	      ArrayList chkList = new ArrayList();
  	        	        	
  	      Collection apChecks = null;
  	      
  	      try {
  	      	
  	         apChecks = apCheckHome.findByCbName(CB_NM, AD_CMPNY);
  	         
  	      } catch (Exception ex) {
  	      	
  	      	 throw new EJBException(ex.getMessage());
  	      	 
  	      }
  	      
  	      if (apChecks.isEmpty())
  	         throw new GlobalNoRecordFoundException();
  	         
  	      Iterator i = apChecks.iterator();
  	      while (i.hasNext()) {
  	
  	         LocalApCheck apCheck = (LocalApCheck) i.next();
  	
  		     ApModCheckDetails mdetails = new ApModCheckDetails();
  		     mdetails.setChkCode(apCheck.getChkCode());
               mdetails.setChkType(apCheck.getChkType());
  		     mdetails.setChkDate(apCheck.getChkDate());
  		     mdetails.setChkNumber(apCheck.getChkNumber());
  		     mdetails.setChkDocumentNumber(apCheck.getChkDocumentNumber());
  		     mdetails.setChkAmount(apCheck.getChkAmount());
  		     mdetails.setChkReleased(apCheck.getChkReleased());
  		     mdetails.setChkSplSupplierCode(apCheck.getApSupplier().getSplSupplierCode());
  		     mdetails.setChkBaName(apCheck.getAdBankAccount().getBaName());
  		           	  
  	      	 chkList.add(mdetails);
  	      	
  	      }
  	         
  	      return chkList;
    
  	  } catch (GlobalNoRecordFoundException ex) {
  	  	 
  	  	  throw ex;
  	  	
  	  } catch (Exception ex) {
  	  	

  	  	  ex.printStackTrace();
  	  	  throw new EJBException(ex.getMessage());
  	  	
  	  }
          
     }
     
     /**
      * @ejb:interface-method view-type="remote"
      * @jboss:method-attributes read-only="true"
      **/
      public short getGlFcPrecisionUnit(Integer AD_CMPNY) {

         Debug.print("ApCheckBatchCopyControllerBean getGlFcPrecisionUnit");

        
         LocalAdCompanyHome adCompanyHome = null;
         
       
         // Initialize EJB Home
          
         try {
              
             adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);       
              
         } catch (NamingException ex) {
              
             throw new EJBException(ex.getMessage());
              
         }

         try {
         	
           LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
              
           return  adCompany.getGlFunctionalCurrency().getFcPrecision();
                                       
         } catch (Exception ex) {
         	 
         	 Debug.printStackTrace(ex);
           throw new EJBException(ex.getMessage());
           
         }

      }


    // SessionBean methods

    /**
     * @ejb:create-method view-type="remote"
     **/
    public void ejbCreate() throws CreateException {

       Debug.print("ApCheckBatchCopyControllerBean ejbCreate");
      
    }
}
