package com.ejb.txn;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;
import javax.naming.NamingException;

import com.ejb.ad.LocalAdLookUpValue;
import com.ejb.ad.LocalAdLookUpValueHome;
import com.ejb.ap.LocalApCheckBatch;
import com.ejb.ap.LocalApCheckBatchHome;
import com.ejb.ap.LocalApCheckHome;
import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.exception.GlobalRecordAlreadyAssignedException;
import com.ejb.exception.GlobalRecordAlreadyDeletedException;
import com.ejb.exception.GlobalRecordAlreadyExistException;
import com.ejb.exception.GlobalTransactionBatchCloseException;
import com.util.AbstractSessionBean;
import com.util.ApCheckBatchDetails;
import com.util.Debug;
import com.util.EJBCommon;
import com.util.EJBHomeFactory;

/**
 * @ejb:bean name="ApCheckBatchEntryControllerEJB"
 *           display-name="used for entering check batches"
 *           type="Stateless"
 *           view-type="remote"
 *           jndi-name="ejb/ApCheckBatchEntryControllerEJB"
 *
 * @ejb:interface remote-class="com.ejb.txn.ApCheckBatchEntryController"
 *                extends="javax.ejb.EJBObject"
 *
 * @ejb:home remote-class="com.ejb.txn.ApCheckBatchEntryControllerHome"
 *           extends="javax.ejb.EJBHome"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="apuser"
 *                        role-link="apuserlink"
 *
 * @ejb:permission role-name="apuser"
 * 
*/

public class ApCheckBatchEntryControllerBean extends AbstractSessionBean {

	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public ArrayList getAdLvDEPARTMENT(Integer AD_CMPNY) {
		
		Debug.print("ApDirectCheckEntryControllerBean getAdLvDEPARTMENT");
		
		LocalAdLookUpValueHome adLookUpValueHome = null;               
		
		ArrayList list = new ArrayList();
		
		// Initialize EJB Home
		
		try {
			
			adLookUpValueHome = (LocalAdLookUpValueHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdLookUpValueHome.JNDI_NAME, LocalAdLookUpValueHome.class);
			
		} catch (NamingException ex) {
			
			throw new EJBException(ex.getMessage());
			
		}        
		
		try {
			
			Collection adLookUpValues = adLookUpValueHome.findByLuName("AD DEPARTMENT", AD_CMPNY);
			
			Iterator i = adLookUpValues.iterator();
			
			while (i.hasNext()) {
				
				LocalAdLookUpValue adLookUpValue = (LocalAdLookUpValue)i.next();
				
				list.add(adLookUpValue.getLvName());
				
			}
			
			return list;
			
		} catch (Exception ex) {
			
			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());
			
		}
		
	}
	
	
    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public com.util.ApCheckBatchDetails getApCbByCbCode(Integer VB_CODE, Integer AD_CMPNY) 
        throws GlobalNoRecordFoundException {
                    
        Debug.print("ApCheckBatchEntryControllerBean getApCbByCbCode");
        
        LocalApCheckBatchHome apCheckBatchHome = null;        
                
        // Initialize EJB Home
        
        try {
            
            apCheckBatchHome = (LocalApCheckBatchHome)EJBHomeFactory.
                lookUpLocalHome(LocalApCheckBatchHome.JNDI_NAME, LocalApCheckBatchHome.class);
            
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }        
        
        try {
        	
        	LocalApCheckBatch apCheckBatch = null;
        	
        	
        	try {
        		
        		apCheckBatch = apCheckBatchHome.findByPrimaryKey(VB_CODE);
        		
        	} catch (FinderException ex) {
        		
        		throw new GlobalNoRecordFoundException();
        		
        	}
        	
        	ApCheckBatchDetails details = new ApCheckBatchDetails();
        	details.setCbCode(apCheckBatch.getCbCode());
        	details.setCbName(apCheckBatch.getCbName());
        	details.setCbDescription(apCheckBatch.getCbDescription());
        	details.setCbStatus(apCheckBatch.getCbStatus());
        	details.setCbType(apCheckBatch.getCbType());
        	details.setCbDepartment(apCheckBatch.getCbDepartment());
        	details.setCbDateCreated(apCheckBatch.getCbDateCreated());
        	details.setCbCreatedBy(apCheckBatch.getCbCreatedBy());
        	
        	return details;
        	
        	
        } catch (GlobalNoRecordFoundException ex) {
        	
        	throw ex;
        	        	
        } catch (Exception ex) {
        	
        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());
        	
        }
            
    }
    
    
    /**
     * @ejb:interface-method view-type="remote"
     **/
    public Integer saveApCbEntry(com.util.ApCheckBatchDetails details, Integer AD_BRNCH, Integer AD_CMPNY) throws
        GlobalRecordAlreadyExistException,
        GlobalRecordAlreadyDeletedException,
		GlobalTransactionBatchCloseException,
		GlobalRecordAlreadyAssignedException {
                    
        Debug.print("ApCheckBatchEntryControllerBean saveApCbEntry");
        
        LocalApCheckBatchHome apCheckBatchHome = null;  
        LocalApCheckHome apCheckHome = null;       
        
        LocalApCheckBatch apCheckBatch = null;
         
                
        // Initialize EJB Home
        
        try {
            
            apCheckBatchHome = (LocalApCheckBatchHome)EJBHomeFactory.
                lookUpLocalHome(LocalApCheckBatchHome.JNDI_NAME, LocalApCheckBatchHome.class);     
            apCheckHome = (LocalApCheckHome)EJBHomeFactory.
                lookUpLocalHome(LocalApCheckHome.JNDI_NAME, LocalApCheckHome.class);       
                        
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        } 
        
        try {
 
                		
			// validate if check batch is already deleted
			
			try {
				
				if (details.getCbCode() != null) {
					
					apCheckBatch = apCheckBatchHome.findByPrimaryKey(details.getCbCode());
					
				}
				
		 	} catch (FinderException ex) {
		 		
		 		throw new GlobalRecordAlreadyDeletedException();	 		
				
			} 
	        
	        // validate if check batch exists
	        		
			try {
				
			    LocalApCheckBatch apExistingCheckBatch = apCheckBatchHome.findByCbName(details.getCbName(), AD_BRNCH, AD_CMPNY);
			
			    if (details.getCbCode() == null ||
			        details.getCbCode() != null && !apExistingCheckBatch.getCbCode().equals(details.getCbCode())) {
			    	
			        throw new GlobalRecordAlreadyExistException();
			        
			    }
			 
			} catch (GlobalRecordAlreadyExistException ex) {
				
				throw ex;
			
			} catch (FinderException ex) {
			    
			} 	
			
			// validate if check batch closing
			
			if (details.getCbStatus().equals("CLOSED")) {
				
				Collection apChecks = apCheckHome.findByChkPostedAndChkVoidAndCbName(EJBCommon.FALSE, EJBCommon.FALSE, details.getCbName(), AD_CMPNY);
				
				if (!apChecks.isEmpty()) {
					
					throw new GlobalTransactionBatchCloseException();
					
				}
				
			}
			
			// validate if check already assigned
			
			if (details.getCbCode() != null) {
				
				Collection apChecks = apCheckBatch.getApChecks();
				
				if (!apCheckBatch.getCbType().equals(details.getCbType()) &&
				    !apChecks.isEmpty()) {
				    	
				    throw new GlobalRecordAlreadyAssignedException();
				    	
				}
				
			}											
			
			
			if (details.getCbCode() == null) {
				
				apCheckBatch = apCheckBatchHome.create(details.getCbName(),
				   details.getCbDescription(), details.getCbStatus(), details.getCbType(),
				   details.getCbDateCreated(), details.getCbCreatedBy(), details.getCbDepartment(), AD_BRNCH, AD_CMPNY);
				
			} else {
				
				apCheckBatch.setCbName(details.getCbName());
				apCheckBatch.setCbDescription(details.getCbDescription());
				apCheckBatch.setCbStatus(details.getCbStatus());
				apCheckBatch.setCbType(details.getCbType());
				apCheckBatch.setCbDepartment(details.getCbDepartment());
				
			}
			
			return apCheckBatch.getCbCode();				
		      	            	
        } catch (GlobalRecordAlreadyDeletedException ex) {
        	 
        	getSessionContext().setRollbackOnly();      	  
      	    throw ex;
        
        } catch (GlobalRecordAlreadyExistException ex) {	 	
        
            getSessionContext().setRollbackOnly();      	  
      	    throw ex;
      	    
      	} catch (GlobalTransactionBatchCloseException ex) {
        	
        	getSessionContext().setRollbackOnly();
        	throw ex;
        	
        } catch (GlobalRecordAlreadyAssignedException ex) {
        	
        	getSessionContext().setRollbackOnly();
        	throw ex;
                	
        } catch (Exception ex) {
        	
        	Debug.printStackTrace(ex);
        	getSessionContext().setRollbackOnly();
        	throw new EJBException(ex.getMessage());
        	
        }
            
    }    
            
    
    /**
     * @ejb:interface-method view-type="remote"
     **/
    public void deleteApCbEntry(Integer VB_CODE, Integer AD_CMPNY) throws 
        GlobalRecordAlreadyDeletedException,
        GlobalRecordAlreadyAssignedException {
                    
        Debug.print("ApCheckBatchEntryControllerBean deleteApCbEntry");
        
        LocalApCheckBatchHome apCheckBatchHome = null;  
        LocalApCheckHome apCheckHome = null;               
                
        // Initialize EJB Home
        
        try {
            
            apCheckBatchHome = (LocalApCheckBatchHome)EJBHomeFactory.
                lookUpLocalHome(LocalApCheckBatchHome.JNDI_NAME, LocalApCheckBatchHome.class);     
            apCheckHome = (LocalApCheckHome)EJBHomeFactory.
                lookUpLocalHome(LocalApCheckHome.JNDI_NAME, LocalApCheckHome.class);       
                        
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }        
        
        try {
        	
        	LocalApCheckBatch apCheckBatch = apCheckBatchHome.findByPrimaryKey(VB_CODE);
        	
        	Collection apChecks = apCheckBatch.getApChecks();
        	
        	if (!apChecks.isEmpty()) {
        		
        		throw new GlobalRecordAlreadyAssignedException();
        		
        	}
        	
        	apCheckBatch.remove();
        	
        } catch (FinderException ex) {	
            
            getSessionContext().setRollbackOnly();
            throw new GlobalRecordAlreadyDeletedException();      	
        	
        } catch (GlobalRecordAlreadyAssignedException ex) {	
            
            getSessionContext().setRollbackOnly();
            throw ex; 
        	
        } catch (Exception ex) {
        	
        	Debug.printStackTrace(ex);
        	getSessionContext().setRollbackOnly();
        	throw new EJBException(ex.getMessage());
        	
        }
            
    }
    
              
	// SessionBean methods

    /**
     * @ejb:create-method view-type="remote"
     **/
    public void ejbCreate() throws CreateException {

       Debug.print("ApCheckBatchEntryControllerBean ejbCreate");
      
    }
    
   // private methods

   

}