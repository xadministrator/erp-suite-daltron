
/*
 * ApCheckBatchSubmitControllerBean.java
 *
 * Created on February 24, 2004, 10:19 AM
 *
 * @author  Dennis M. Hilario
 */

package com.ejb.txn;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;
import javax.naming.NamingException;

import com.ejb.ad.LocalAdAmountLimit;
import com.ejb.ad.LocalAdAmountLimitHome;
import com.ejb.ad.LocalAdApproval;
import com.ejb.ad.LocalAdApprovalCoaLineHome;
import com.ejb.ad.LocalAdApprovalHome;
import com.ejb.ad.LocalAdApprovalQueue;
import com.ejb.ad.LocalAdApprovalQueueHome;
import com.ejb.ad.LocalAdApprovalUser;
import com.ejb.ad.LocalAdApprovalUserHome;
import com.ejb.ad.LocalAdBankAccount;
import com.ejb.ad.LocalAdBankAccountBalance;
import com.ejb.ad.LocalAdBankAccountBalanceHome;
import com.ejb.ad.LocalAdBankAccountHome;
import com.ejb.ad.LocalAdBranchDocumentSequenceAssignment;
import com.ejb.ad.LocalAdBranchDocumentSequenceAssignmentHome;
import com.ejb.ad.LocalAdBranchItemLocation;
import com.ejb.ad.LocalAdBranchItemLocationHome;
import com.ejb.ad.LocalAdCompany;
import com.ejb.ad.LocalAdCompanyHome;
import com.ejb.ad.LocalAdPreference;
import com.ejb.ad.LocalAdPreferenceHome;
import com.ejb.ap.LocalApAppliedVoucher;
import com.ejb.ap.LocalApAppliedVoucherHome;
import com.ejb.ap.LocalApCheck;
import com.ejb.ap.LocalApCheckBatch;
import com.ejb.ap.LocalApCheckBatchHome;
import com.ejb.ap.LocalApCheckHome;
import com.ejb.ap.LocalApDistributionRecord;
import com.ejb.ap.LocalApDistributionRecordHome;
import com.ejb.ap.LocalApSupplier;
import com.ejb.ap.LocalApSupplierBalance;
import com.ejb.ap.LocalApSupplierBalanceHome;
import com.ejb.ap.LocalApSupplierHome;
import com.ejb.ap.LocalApVoucher;
import com.ejb.ap.LocalApVoucherLineItem;
import com.ejb.ap.LocalApVoucherPaymentSchedule;
import com.ejb.ap.LocalApVoucherPaymentScheduleHome;
import com.ejb.exception.AdPRFCoaGlVarianceAccountNotFoundException;
import com.ejb.exception.ApCHKVoucherHasNoWTaxCodeException;
import com.ejb.exception.ApVOUOverapplicationNotAllowedException;
import com.ejb.exception.GlJREffectiveDateNoPeriodExistException;
import com.ejb.exception.GlJREffectiveDatePeriodClosedException;
import com.ejb.exception.GlobalBranchAccountNumberInvalidException;
import com.ejb.exception.GlobalConversionDateNotExistException;
import com.ejb.exception.GlobalInventoryDateException;
import com.ejb.exception.GlobalJournalNotBalanceException;
import com.ejb.exception.GlobalNoApprovalApproverFoundException;
import com.ejb.exception.GlobalNoApprovalRequesterFoundException;
import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.exception.GlobalRecordAlreadyDeletedException;
import com.ejb.exception.GlobalTransactionAlreadyApprovedException;
import com.ejb.exception.GlobalTransactionAlreadyLockedException;
import com.ejb.exception.GlobalTransactionAlreadyPendingException;
import com.ejb.exception.GlobalTransactionAlreadyPostedException;
import com.ejb.exception.GlobalTransactionAlreadyVoidPostedException;
import com.ejb.gl.LocalAdDocumentSequenceAssignment;
import com.ejb.gl.LocalAdDocumentSequenceAssignmentHome;
import com.ejb.gl.LocalGlAccountingCalendarValue;
import com.ejb.gl.LocalGlAccountingCalendarValueHome;
import com.ejb.gl.LocalGlChartOfAccount;
import com.ejb.gl.LocalGlChartOfAccountBalance;
import com.ejb.gl.LocalGlChartOfAccountBalanceHome;
import com.ejb.gl.LocalGlChartOfAccountHome;
import com.ejb.gl.LocalGlForexLedger;
import com.ejb.gl.LocalGlForexLedgerHome;
import com.ejb.gl.LocalGlFunctionalCurrency;
import com.ejb.gl.LocalGlFunctionalCurrencyHome;
import com.ejb.gl.LocalGlFunctionalCurrencyRate;
import com.ejb.gl.LocalGlFunctionalCurrencyRateHome;
import com.ejb.gl.LocalGlInvestorAccountBalance;
import com.ejb.gl.LocalGlInvestorAccountBalanceHome;
import com.ejb.gl.LocalGlJournal;
import com.ejb.gl.LocalGlJournalBatch;
import com.ejb.gl.LocalGlJournalBatchHome;
import com.ejb.gl.LocalGlJournalCategory;
import com.ejb.gl.LocalGlJournalCategoryHome;
import com.ejb.gl.LocalGlJournalHome;
import com.ejb.gl.LocalGlJournalLine;
import com.ejb.gl.LocalGlJournalLineHome;
import com.ejb.gl.LocalGlJournalSource;
import com.ejb.gl.LocalGlJournalSourceHome;
import com.ejb.gl.LocalGlSetOfBook;
import com.ejb.gl.LocalGlSetOfBookHome;
import com.ejb.gl.LocalGlSuspenseAccount;
import com.ejb.gl.LocalGlSuspenseAccountHome;
import com.ejb.inv.LocalInvAdjustment;
import com.ejb.inv.LocalInvAdjustmentHome;
import com.ejb.inv.LocalInvAdjustmentLine;
import com.ejb.inv.LocalInvAdjustmentLineHome;
import com.ejb.inv.LocalInvCosting;
import com.ejb.inv.LocalInvCostingHome;
import com.ejb.inv.LocalInvDistributionRecord;
import com.ejb.inv.LocalInvDistributionRecordHome;
import com.ejb.inv.LocalInvItem;
import com.ejb.inv.LocalInvItemLocation;
import com.ejb.inv.LocalInvUnitOfMeasure;
import com.ejb.inv.LocalInvUnitOfMeasureConversion;
import com.ejb.inv.LocalInvUnitOfMeasureConversionHome;
import com.util.AbstractSessionBean;
import com.util.ApModAppliedVoucherDetails;
import com.util.ApModCheckDetails;
import com.util.Debug;
import com.util.EJBCommon;
import com.util.EJBHomeFactory;

/**
 * @ejb:bean name="ApCheckBatchSubmitControllerEJB"
 *           display-name="Used for check submission"
 *           type="Stateless"
 *           view-type="remote"
 *           jndi-name="ejb/ApCheckBatchSubmitControllerEJB"
 *
 * @ejb:interface remote-class="com.ejb.txn.ApCheckBatchSubmitController"
 *                extends="javax.ejb.EJBObject"
 *
 * @ejb:home remote-class="com.ejb.txn.ApCheckBatchSubmitControllerHome"
 *           extends="javax.ejb.EJBHome"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="apuser"
 *                        role-link="apuserlink"
 *
 * @ejb:permission role-name="apuser"
 * 
*/

public class ApCheckBatchSubmitControllerBean extends AbstractSessionBean {


    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList getApSplAll(Integer AD_BRNCH, Integer AD_CMPNY) {
                    
        Debug.print("ApCheckBatchSubmitControllerBean getApSplAll");
        
        LocalApSupplierHome apSupplierHome = null;               
        
        ArrayList list = new ArrayList();
        
        // Initialize EJB Home
        
        try {
            
            apSupplierHome = (LocalApSupplierHome)EJBHomeFactory.
                lookUpLocalHome(LocalApSupplierHome.JNDI_NAME, LocalApSupplierHome.class);
            
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }        
        
        try {
        	
        	Collection apSuppliers = apSupplierHome.findEnabledSplAll(AD_BRNCH, AD_CMPNY);
        	
        	Iterator i = apSuppliers.iterator();
        	
        	while (i.hasNext()) {
        		
        		LocalApSupplier apSupplier = (LocalApSupplier)i.next();
        		
        		list.add(apSupplier.getSplSupplierCode());
        		
        	}
        	
        	return list;
        	
        } catch (Exception ex) {
        	
        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());
        	
        }
            
    } 


    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList getAdBaAll(Integer AD_BRNCH, Integer AD_CMPNY) {
                    
        Debug.print("ApCheckBatchSubmitControllerBean getAdBaAll");
        
        LocalAdBankAccountHome adBankAccountHome = null;               
        
        ArrayList list = new ArrayList();
        
        // Initialize EJB Home
        
        try {
            
            adBankAccountHome = (LocalAdBankAccountHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdBankAccountHome.JNDI_NAME, LocalAdBankAccountHome.class);
            
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }        
        
        try {
        	
        	Collection adBankAccounts = adBankAccountHome.findEnabledBaAll(AD_BRNCH, AD_CMPNY);
        	
        	Iterator i = adBankAccounts.iterator();
        	
        	while (i.hasNext()) {
        		
        		LocalAdBankAccount adBankAccount = (LocalAdBankAccount)i.next();
        		
        		list.add(adBankAccount.getBaName());
        		
        	}
        	
        	return list;
        	
        } catch (Exception ex) {
        	
        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());
        	
        }
            
    }
    
    
    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList getGlFcAll(Integer AD_CMPNY) {
                    
        Debug.print("ApCheckBatchSubmitControllerBean getGlFcAll");
        
        LocalGlFunctionalCurrencyHome glFunctionalCurrencyHome = null;

        ArrayList list = new ArrayList();
        
        // Initialize EJB Home
        
        try {
            
            glFunctionalCurrencyHome = (LocalGlFunctionalCurrencyHome)EJBHomeFactory.
                lookUpLocalHome(LocalGlFunctionalCurrencyHome.JNDI_NAME, LocalGlFunctionalCurrencyHome.class);
            
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {
            
            Collection glFunctionalCurrencies = glFunctionalCurrencyHome.findFcAll(AD_CMPNY);
	        
	        Iterator i = glFunctionalCurrencies.iterator();
	               
	        while (i.hasNext()) {
	        	
	        	LocalGlFunctionalCurrency glFunctionalCurrency = (LocalGlFunctionalCurrency)i.next();
	        	
	        	list.add(glFunctionalCurrency.getFcName());
	        	
	        }
	        
	        return list;
          
        } catch (Exception ex) {
        	
        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());
        	
        }
            
    }
  
    
    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList getApOpenCbAll(Integer AD_BRNCH, Integer AD_CMPNY) {
                    
        Debug.print("ApCheckBatchSubmitControllerBean getApOpenCbAll");
        
        LocalApCheckBatchHome apCheckBatchHome = null;               
        
        ArrayList list = new ArrayList();
        
        // Initialize EJB Home
        
        try {
            
        	apCheckBatchHome = (LocalApCheckBatchHome)EJBHomeFactory.
                lookUpLocalHome(LocalApCheckBatchHome.JNDI_NAME, LocalApCheckBatchHome.class);
            
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }        
        
        try {
        	
        	Collection apCheckBatches = apCheckBatchHome.findOpenCbAll(AD_BRNCH, AD_CMPNY);
        	
        	Iterator i = apCheckBatches.iterator();
        	
        	while (i.hasNext()) {
        		
        		LocalApCheckBatch apCheckBatch = (LocalApCheckBatch)i.next();
        		
        		list.add(apCheckBatch.getCbName());
        		
        	}
        	
        	return list;
        	
        } catch (Exception ex) {
        	
        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());
        	
        }
            
    }  
    
    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
     public byte getAdPrfEnableApCheckBatch(Integer AD_CMPNY) {

        Debug.print("ApCheckBatchSubmitControllerBean getAdPrfEnableApCheckBatch");
                   
        LocalAdPreferenceHome adPreferenceHome = null;
       
       
        // Initialize EJB Home
         
        try {
             
           adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
               lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);       
             
        } catch (NamingException ex) {
             
           throw new EJBException(ex.getMessage());
             
        }
       

        try {
       	
           LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);
          
           return adPreference.getPrfEnableApCheckBatch();
          
        } catch (Exception ex) {
         	 
           Debug.printStackTrace(ex);
           throw new EJBException(ex.getMessage());
          
        }
       
     }
   
  /**
   * @ejb:interface-method view-type="remote"
   * @jboss:method-attributes read-only="true"
   **/
   public ArrayList getApChkByCriteria(HashMap criteria, String ORDER_BY, Integer OFFSET, Integer LIMIT, Integer AD_BRNCH, Integer AD_CMPNY)
	  throws GlobalNoRecordFoundException {
	
	  Debug.print("ApCheckBatchSubmitControllerBean getApChkByCriteria");
	  
	  LocalApCheckHome apCheckHome = null;
	  
	  // Initialize EJB Home
	    
	  try {
	  	
	      apCheckHome = (LocalApCheckHome)EJBHomeFactory.
	          lookUpLocalHome(LocalApCheckHome.JNDI_NAME, LocalApCheckHome.class);          
	        
	  } catch (NamingException ex) {
	        
	      throw new EJBException(ex.getMessage());
	        
	  }
	  
	  try { 
	      
	      ArrayList chkList = new ArrayList();
	      
	      StringBuffer jbossQl = new StringBuffer();
	      jbossQl.append("SELECT OBJECT(chk) FROM ApCheck chk ");
	      
	      boolean firstArgument = true;
	      short ctr = 0;
	      int criteriaSize = criteria.size() + 2;
	      
	      
		  Object obj[] = new Object[criteriaSize];
		  	      
		  if (criteria.containsKey("bankAccount")) {
		  	
		  	 firstArgument = false;
		  	 
		  	 jbossQl.append("WHERE chk.adBankAccount.baName=?" + (ctr+1) + " ");
		  	 obj[ctr] = (String)criteria.get("bankAccount");
		  	 ctr++;
		  	 
		  } 
		  
		  if (criteria.containsKey("currency")) {
		  	
		  	 firstArgument = false;
		  	 
		  	 jbossQl.append("WHERE chk.glFunctionalCurrency.fcName=?" + (ctr+1) + " ");
		  	 obj[ctr] = (String)criteria.get("currency");
		  	 ctr++;
		  	 
		  }
		  
		  if (criteria.containsKey("batchName")) {
		  	
		  	 if (!firstArgument) {
		  	 
		  	 	jbossQl.append("AND ");
		  	 
		  	 } else {
		  	 
		  	 	firstArgument = false;
		  	 	jbossQl.append("WHERE ");
		  	 
		  	 }
		  	 
		  	 jbossQl.append("chk.apCheckBatch.cbName=?" + (ctr+1) + " ");
		  	 obj[ctr] = (String)criteria.get("batchName");
		  	 ctr++;
		  } 
		      
		  if (criteria.containsKey("supplierCode")) {
		  	
		  	 if (!firstArgument) {
		  	 
		  	 	jbossQl.append("AND ");
		  	 
		  	 } else {
		  	 
		  	 	firstArgument = false;
		  	 	jbossQl.append("WHERE ");
		  	 
		  	 }
		  	 
		  	 jbossQl.append("chk.apSupplier.splSupplierCode=?" + (ctr+1) + " ");
		  	 obj[ctr] = (String)criteria.get("supplierCode");
		  	 ctr++;
		  }      
		      
		  if (criteria.containsKey("dateFrom")) {
		  	
		  	 if (!firstArgument) {
		  	 	jbossQl.append("AND ");
		  	 
		  	 } else {
		  	 
		  	 	firstArgument = false;
		  	 	jbossQl.append("WHERE ");
		  	 
		  	 }
		  	 
		  	 jbossQl.append("chk.chkDate>=?" + (ctr+1) + " ");
		  	 obj[ctr] = (Date)criteria.get("dateFrom");
		  	 ctr++;
		  }  
		      
		  if (criteria.containsKey("dateTo")) {
		  	
		  	 if (!firstArgument) {
		  	 
		  	 	jbossQl.append("AND ");
		  	 
		  	 } else {
		  	 
		  	 	firstArgument = false;
		  	 	jbossQl.append("WHERE ");
		  	 
		  	 }
		  	 
		  	 jbossQl.append("chk.chkDate<=?" + (ctr+1) + " ");
		  	 obj[ctr] = (Date)criteria.get("dateTo");
		  	 ctr++;
		  	 
		  }    
		      
		  if (criteria.containsKey("checkNumberFrom")) {
		  	
		  	 if (!firstArgument) {
		  	 
		  	 	jbossQl.append("AND ");
		  	 
		  	 } else {
		  	 
		  	 	firstArgument = false;
		  	 	jbossQl.append("WHERE ");
		  	 
		  	 }
		  	 
		  	 jbossQl.append("chk.chkNumber>=?" + (ctr+1) + " ");
		  	 obj[ctr] = (String)criteria.get("checkNumberFrom");
		  	 ctr++;
		  	 
		  }  
		      
		  if (criteria.containsKey("checkNumberTo")) {
		  	
		  	 if (!firstArgument) {
		  	 
		  	 	jbossQl.append("AND ");
		  	 
		  	 } else {
		  	 
		  	 	firstArgument = false;
		  	 	jbossQl.append("WHERE ");
		  	 
		  	 }
		  	 
		  	 jbossQl.append("chk.chkNumber<=?" + (ctr+1) + " ");
		  	 obj[ctr] = (String)criteria.get("checkNumberTo");
		  	 ctr++;
		  	 
		  } 	      
		      
		  if (criteria.containsKey("documentNumberFrom")) {
		  	
		  	 if (!firstArgument) {
		  	 
		  	 	jbossQl.append("AND ");
		  	 
		  	 } else {
		  	 
		  	 	firstArgument = false;
		  	 	jbossQl.append("WHERE ");
		  	 
		  	 }
		  	 
		  	 jbossQl.append("chk.chkDocumentNumber>=?" + (ctr+1) + " ");
		  	 obj[ctr] = (String)criteria.get("documentNumberFrom");
		  	 ctr++;
		  	 
		  }  
		      
		  if (criteria.containsKey("documentNumberTo")) {
		  	
		  	 if (!firstArgument) {
		  	 
		  	 	jbossQl.append("AND ");
		  	 
		  	 } else {
		  	 
		  	 	firstArgument = false;
		  	 	jbossQl.append("WHERE ");
		  	 
		  	 }
		  	 
		  	 jbossQl.append("chk.chkDocumentNumber<=?" + (ctr+1) + " ");
		  	 obj[ctr] = (String)criteria.get("documentNumberTo");
		  	 ctr++;
		  	 
		  } 	
		  
		  if (!firstArgument) {
		      
		      jbossQl.append("AND ");
		      
		  } else {
		      
		      firstArgument = false;
		      jbossQl.append("WHERE ");
		      
		  }
		  
		  jbossQl.append("chk.chkAdBranch=" + AD_BRNCH + " ");
		  		  
		  if (!firstArgument) {
		  	 
	  	 	jbossQl.append("AND ");
	  	 
	  	  } else {
	  	 
	  	 	firstArgument = false;
	  	 	jbossQl.append("WHERE ");
	  	 
	  	  }  
	  	  	  	  	
	  	  jbossQl.append("chk.chkApprovalStatus IS NULL AND chk.chkPosted = 0 AND chk.chkVoid = 0 AND chk.chkAdCompany=" + AD_CMPNY + " ");	  	  		  	  
	  	         	     
	      String orderBy = null;
		      
	      if (ORDER_BY.equals("BANK ACCOUNT")) {
	          
	          orderBy = "chk.adBankAccount.baName";
	          	
	      } else if (ORDER_BY.equals("SUPPLIER CODE")) {
	      	 
	      	  orderBy = "chk.apSupplier.splSupplierCode";
	      	  
	      } else if (ORDER_BY.equals("DOCUMENT NUMBER")) {
	      	
	      	  orderBy = "chk.chkDocumentNumber";
	      	
	      } else if (ORDER_BY.equals("CHECK NUMBER")) {
	      	
	      	  orderBy = "chk.chkNumber";
	      	
	      }
	      
		  
		  if (orderBy != null) {
		  
		  	jbossQl.append("ORDER BY " + orderBy + ", chk.chkDate");
		  	
		  }  else {
		  	
		  	jbossQl.append("ORDER BY chk.chkDate");
		  	
		  } 
		  
	               
	      jbossQl.append(" OFFSET ?" + (ctr + 1));
	      obj[ctr] = OFFSET;
	      ctr++;
	      
	      jbossQl.append(" LIMIT ?" + (ctr + 1));
	      obj[ctr] = LIMIT;
	      
	      System.out.println("QL + " + jbossQl);
	
	      Collection apChecks = null;
	      
	      try {
	      	
	         apChecks = apCheckHome.getChkByCriteria(jbossQl.toString(), obj);
	         
	      } catch (Exception ex) {
	      	
	      	 throw new EJBException(ex.getMessage());
	      	 
	      }
	      
	      if (apChecks.isEmpty())
	         throw new GlobalNoRecordFoundException();
	         
	      Iterator i = apChecks.iterator();
	      while (i.hasNext()) {
	
	         LocalApCheck apCheck = (LocalApCheck) i.next();
	
		     ApModCheckDetails mdetails = new ApModCheckDetails();
		     mdetails.setChkCode(apCheck.getChkCode());
		     mdetails.setChkDate(apCheck.getChkDate());
		     mdetails.setChkNumber(apCheck.getChkNumber());
		     mdetails.setChkDocumentNumber(apCheck.getChkDocumentNumber());
		     mdetails.setChkAmount(apCheck.getChkAmount());
		     mdetails.setChkVoid(apCheck.getChkVoid());
		     mdetails.setChkSplSupplierCode(apCheck.getApSupplier().getSplSupplierCode());
		     mdetails.setChkBaName(apCheck.getAdBankAccount().getBaName());
		           	  
		     if (!apCheck.getApVoucherLineItems().isEmpty()) {
		  	  	
		  	    mdetails.setChkType("ITEMS");
		  	 
		     } else {
				
				mdetails.setChkType("EXPENSES");
		  	 }
		     
	      	 chkList.add(mdetails);
	      	
	      }
	         
	      return chkList;
  
	  } catch (GlobalNoRecordFoundException ex) {
	  	 
	  	  throw ex;
	  	
	  } catch (Exception ex) {
	  	

	  	  ex.printStackTrace();
	  	  throw new EJBException(ex.getMessage());
	  	
	  }
        
   }
   
   
   /**
     * @ejb:interface-method view-type="remote"
     **/
    public void executeApChkBatchSubmit(Integer CHK_CODE, Integer AD_BRNCH, Integer AD_CMPNY) throws
        GlobalRecordAlreadyDeletedException,
    	GlobalTransactionAlreadyApprovedException,
		GlobalTransactionAlreadyPendingException,
		GlobalTransactionAlreadyPostedException,
		GlobalNoApprovalRequesterFoundException,
		GlobalNoApprovalApproverFoundException,
		GlobalTransactionAlreadyVoidPostedException,
		GlJREffectiveDateNoPeriodExistException,
		GlJREffectiveDatePeriodClosedException,
		GlobalJournalNotBalanceException,
		GlobalInventoryDateException,
		AdPRFCoaGlVarianceAccountNotFoundException {
                    
        Debug.print("ApCheckBatchSubmitControllerBean executeApChkBatchSubmit");
        
        LocalApCheckHome apCheckHome = null;        
        LocalAdApprovalHome adApprovalHome = null;
        LocalAdAmountLimitHome adAmountLimitHome = null;
        LocalAdApprovalUserHome adApprovalUserHome = null;
        LocalAdApprovalQueueHome adApprovalQueueHome = null;
        LocalAdApprovalCoaLineHome adApprovalCoaLineHome = null;   
        LocalAdPreferenceHome adPreferenceHome = null;
        LocalInvCostingHome invCostingHome = null;
        
        // Initialize EJB Home
        
        try {
            
            apCheckHome = (LocalApCheckHome)EJBHomeFactory.
                lookUpLocalHome(LocalApCheckHome.JNDI_NAME, LocalApCheckHome.class);
            adApprovalHome = (LocalAdApprovalHome)EJBHomeFactory.
              	lookUpLocalHome(LocalAdApprovalHome.JNDI_NAME, LocalAdApprovalHome.class);
            adAmountLimitHome = (LocalAdAmountLimitHome)EJBHomeFactory.
              	lookUpLocalHome(LocalAdAmountLimitHome.JNDI_NAME, LocalAdAmountLimitHome.class);
            adApprovalUserHome = (LocalAdApprovalUserHome)EJBHomeFactory.
              	lookUpLocalHome(LocalAdApprovalUserHome.JNDI_NAME, LocalAdApprovalUserHome.class);
            adApprovalQueueHome = (LocalAdApprovalQueueHome)EJBHomeFactory.
              	lookUpLocalHome(LocalAdApprovalQueueHome.JNDI_NAME, LocalAdApprovalQueueHome.class);
            adApprovalCoaLineHome = (LocalAdApprovalCoaLineHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdApprovalCoaLineHome.JNDI_NAME, LocalAdApprovalCoaLineHome.class);
            adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
            	lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);
            invCostingHome = (LocalInvCostingHome)EJBHomeFactory.
        		lookUpLocalHome(LocalInvCostingHome.JNDI_NAME, LocalInvCostingHome.class);
                                    
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }       
        
        try {
        	
        	LocalApCheck apCheck = null;
       	   
       	   try {
       	   	
       	   	  apCheck = apCheckHome.findByPrimaryKey(CHK_CODE);
       	   	
       	   } catch (FinderException ex) {
       	   	
       	   	  throw new GlobalRecordAlreadyDeletedException();
       	   	
       	   }
       	          	   
       	   // validate check
       	   
       	   if (apCheck.getChkApprovalStatus() != null) {
        			
	    	   if (apCheck.getChkApprovalStatus().equals("APPROVED") ||
	    		   apCheck.getChkApprovalStatus().equals("N/A")) {         		    	
	    		 
	    		   throw new GlobalTransactionAlreadyApprovedException(); 
	    		    	        		    	
	    	   } else if (apCheck.getChkApprovalStatus().equals("PENDING")) {
	    			
	    		   throw new GlobalTransactionAlreadyPendingException();
	    			
	    	   }
	    		
	       }
				
		   if (apCheck.getChkPosted() == EJBCommon.TRUE) {
				
			   throw new GlobalTransactionAlreadyPostedException();
				
		   }
        	        	
           // generate approval status
        		       		
            String CHK_APPRVL_STATUS = null;
        		
    		LocalAdApproval adApproval = adApprovalHome.findByAprAdCompany(AD_CMPNY);
    		
    		// check if ap check approval is enabled
    		
    		if (adApproval.getAprEnableApCheck() == EJBCommon.FALSE) {
    			        			        			
    			CHK_APPRVL_STATUS = "N/A";
    			
    		} else {
    			
    			// check if check is self approved
    			
    			LocalAdAmountLimit adAmountLimit = null;
    			
    			try {
    				
    				adAmountLimit = adAmountLimitHome.findByAdcTypeAndAuTypeAndUsrName("AP CHECK", "REQUESTER", apCheck.getChkLastModifiedBy(), AD_CMPNY);       			
    				
    			} catch (FinderException ex) {
    				
    				throw new GlobalNoApprovalRequesterFoundException();
    				
    			}
    			
    			if (apCheck.getChkAmount() <= adAmountLimit.getCalAmountLimit()) {
    				
    				CHK_APPRVL_STATUS = "N/A";
    				
    			} else {
    				
    				// for approval, create approval queue
    				
    				 Collection adAmountLimits = adAmountLimitHome.findByAdcTypeAndGreaterThanCalAmountLimit("AP CHECK", adAmountLimit.getCalAmountLimit(), AD_CMPNY);
    				 
    				 if (adAmountLimits.isEmpty()) {
    				 	
    				 	Collection adApprovalUsers = adApprovalUserHome.findByAuTypeAndCalCode("APPROVER", adAmountLimit.getCalCode(), AD_CMPNY);
    				 	
    				 	if (adApprovalUsers.isEmpty()) {
    				 		
    				 		throw new GlobalNoApprovalApproverFoundException();
    				 		
    				 	}
    				 	        				 	
    				 	Iterator j = adApprovalUsers.iterator();
    				 	
    				 	while (j.hasNext()) {
    				 		
    				 		LocalAdApprovalUser adApprovalUser = (LocalAdApprovalUser)j.next();
    				 		
    				 		LocalAdApprovalQueue adApprovalQueue = adApprovalQueueHome.create(EJBCommon.TRUE, "AP CHECK", apCheck.getChkCode(), 
    				 				apCheck.getChkDocumentNumber(), apCheck.getChkDate(), adAmountLimit.getCalAndOr(), adApprovalUser.getAuOr(), AD_BRNCH, AD_CMPNY);
    				 		
    				 		adApprovalUser.getAdUser().addAdApprovalQueue(adApprovalQueue);
    				 		
    				 	}        				 	
    				 	        				 	
    				 } else {
    				 	
    				 	boolean isApprovalUsersFound = false;
    				 	
    				 	Iterator i = adAmountLimits.iterator();
    				 	        				 	
    				 	while (i.hasNext()) {
    				 		
    				 		LocalAdAmountLimit adNextAmountLimit = (LocalAdAmountLimit)i.next();        				 		
    				 		        				 		        				 		        				 		
    				 		if (apCheck.getChkAmount() <= adNextAmountLimit.getCalAmountLimit()) {
    				 			
    				 			Collection adApprovalUsers = adApprovalUserHome.findByAuTypeAndCalCode("APPROVER", adAmountLimit.getCalCode(), AD_CMPNY);        				 			           				 	
    				 			
    				 			Iterator j = adApprovalUsers.iterator();
	        				 	
	        				 	while (j.hasNext()) {
	        				 		
	        				 		isApprovalUsersFound = true;
	        				 		
	        				 		LocalAdApprovalUser adApprovalUser = (LocalAdApprovalUser)j.next();
	        				 		
	        				 		LocalAdApprovalQueue adApprovalQueue = adApprovalQueueHome.create(EJBCommon.TRUE, "AP CHECK", apCheck.getChkCode(), 
	        				 				apCheck.getChkDocumentNumber(), apCheck.getChkDate(), adAmountLimit.getCalAndOr(), adApprovalUser.getAuOr(), AD_BRNCH, AD_CMPNY);
	        				 		
	        				 		adApprovalUser.getAdUser().addAdApprovalQueue(adApprovalQueue);
	        				 		
	        				 	}        				 			
    				 			        				 			        				 			       				 			
    				 			break;
	        				 	        				 			
    				 		} else if (!i.hasNext()) {
    				 			
    				 			Collection adApprovalUsers = adApprovalUserHome.findByAuTypeAndCalCode("APPROVER", adNextAmountLimit.getCalCode(), AD_CMPNY);        				 			           				 	
    				 			
    				 			Iterator j = adApprovalUsers.iterator();
	        				 	
	        				 	while (j.hasNext()) {
	        				 		
	        				 		isApprovalUsersFound = true;
	        				 		
	        				 		LocalAdApprovalUser adApprovalUser = (LocalAdApprovalUser)j.next();
	        				 		
	        				 		LocalAdApprovalQueue adApprovalQueue = adApprovalQueueHome.create(EJBCommon.TRUE, "AP CHECK", apCheck.getChkCode(), 
	        				 				apCheck.getChkDocumentNumber(), apCheck.getChkDate(), adNextAmountLimit.getCalAndOr(), adApprovalUser.getAuOr(), AD_BRNCH, AD_CMPNY);
	        				 		
	        				 		adApprovalUser.getAdUser().addAdApprovalQueue(adApprovalQueue);
	        				 		
	        				 	}        				 			
    				 			        				 			        				 			       				 			
    				 			break;
    				 			
    				 		}        				 		
    				 		        				 		
    				 		adAmountLimit = adNextAmountLimit;
    				 		
    				 	}
    				 	
    				 	if (!isApprovalUsersFound) {
    				 		
    				 		throw new GlobalNoApprovalApproverFoundException();
    				 		
    				 	}        				 
    				 	
    			    }
    			    
    			    CHK_APPRVL_STATUS = "PENDING";
    			}        			        			        			
    		}        		   
    		
    		LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);
        	
    		//    start date validation
    		
    		if (apCheck != null || apCheck.getChkType().equals("DIRECT")) {

				Collection apVoucherLineItems = apCheck.getApVoucherLineItems();

				Iterator j = apVoucherLineItems.iterator();

				while (j.hasNext()) {

					LocalApVoucherLineItem apVoucherLineItem = (LocalApVoucherLineItem) j.next();

					// start date validation
					if (adPreference.getPrfArAllowPriorDate() == EJBCommon.FALSE) {
						Collection invNegTxnCosting = invCostingHome.findNegTxnByGreaterThanCstDateAndIiNameAndLocName(
			 	  	    		apCheck.getChkDate(), apVoucherLineItem.getInvItemLocation().getInvItem().getIiName(),
			 	  	    		apVoucherLineItem.getInvItemLocation().getInvLocation().getLocName(), AD_BRNCH, AD_CMPNY);
			 	  	    if(!invNegTxnCosting.isEmpty()) throw new GlobalInventoryDateException(apVoucherLineItem.getInvItemLocation().getInvItem().getIiName());
					}
		 	  	     
				}
				
			}
    		
    		
        	if (CHK_APPRVL_STATUS != null && CHK_APPRVL_STATUS.equals("N/A") && adPreference.getPrfApGlPostingType().equals("AUTO-POST UPON APPROVAL")) {
        		
        		this.executeApChkPost(apCheck.getChkCode(), apCheck.getChkLastModifiedBy(), AD_BRNCH, AD_CMPNY);
        		
        	}

        	
        	// set check approval status
        	
        	apCheck.setChkApprovalStatus(CHK_APPRVL_STATUS);
        	        
        } catch (GlobalRecordAlreadyDeletedException ex) {
       	   
       	   getSessionContext().setRollbackOnly();    	
       	   throw ex;	         	
       	  
        } catch (GlobalTransactionAlreadyApprovedException ex) {
       	   
       	   getSessionContext().setRollbackOnly();    	
       	   throw ex;	
       
        } catch (GlobalTransactionAlreadyPendingException ex) {
       	   
       	   getSessionContext().setRollbackOnly();    	
       	   throw ex;	
       	   
        } catch (GlobalTransactionAlreadyPostedException ex) {
       	   
       	   getSessionContext().setRollbackOnly();    	
       	   throw ex;
       	   
        } catch (GlobalNoApprovalRequesterFoundException ex) {
         	   
       	   getSessionContext().setRollbackOnly();    	
       	   throw ex;
       	   
        } catch (GlobalNoApprovalApproverFoundException ex) {
       	   
       	   getSessionContext().setRollbackOnly();    	
       	   throw ex;
       	   
       	} catch (GlobalTransactionAlreadyVoidPostedException ex) {
       	   
       	   getSessionContext().setRollbackOnly();    	
       	   throw ex;
       	   
       	} catch (GlJREffectiveDateNoPeriodExistException ex) {
       	   
       	   getSessionContext().setRollbackOnly();    	
       	   throw ex;
       	   
       	} catch (GlJREffectiveDatePeriodClosedException ex) {
       	   
       	   getSessionContext().setRollbackOnly();    	
       	   throw ex;
       	   
       	} catch (GlobalJournalNotBalanceException ex) {
       	   
       	   getSessionContext().setRollbackOnly();    	
       	   throw ex;
       	   
       	} catch (GlobalInventoryDateException ex) {
        	   
    	   getSessionContext().setRollbackOnly();    	
    	   throw ex;

       	} catch (AdPRFCoaGlVarianceAccountNotFoundException ex) {
       		
       		getSessionContext().setRollbackOnly();
       		throw ex;
       		
        } catch (Exception ex) {
       	 
       	   Debug.printStackTrace(ex);
       	   getSessionContext().setRollbackOnly();
           throw new EJBException(ex.getMessage());
         
        }
    	
    }
   
   
   /**
    * @ejb:interface-method view-type="remote"
    * @jboss:method-attributes read-only="true"
    **/
    public short getGlFcPrecisionUnit(Integer AD_CMPNY) {

       Debug.print("ApCheckBatchSubmitControllerBean getGlFcPrecisionUnit");

      
       LocalAdCompanyHome adCompanyHome = null;
      
     
       // Initialize EJB Home
        
       try {
            
           adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
              lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);       
            
       } catch (NamingException ex) {
            
           throw new EJBException(ex.getMessage());
            
       }

       try {
       	
         LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
            
         return  adCompany.getGlFunctionalCurrency().getFcPrecision();
                                     
       } catch (Exception ex) {
       	 
       	 Debug.printStackTrace(ex);
         throw new EJBException(ex.getMessage());
         
       }

    }
    
    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
     public byte getAdPrfApUseSupplierPulldown(Integer AD_CMPNY) {

        Debug.print("ApCheckBatchSubmitControllerBean getAdPrfApUseSupplierPulldown");
                   
        LocalAdPreferenceHome adPreferenceHome = null;
       
       
        // Initialize EJB Home
         
        try {
             
           adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
               lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);       
             
        } catch (NamingException ex) {
             
           throw new EJBException(ex.getMessage());
             
        }
       

        try {
        	
           LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);
           
           return adPreference.getPrfApUseSupplierPulldown();
          
        } catch (Exception ex) {
         	 
           Debug.printStackTrace(ex);
           throw new EJBException(ex.getMessage());
          
        }
       
     }
    
    // private methods    
		    
    private LocalApAppliedVoucher addApAvEntry(ApModAppliedVoucherDetails mdetails, LocalApCheck apCheck, Integer AD_CMPNY) 
        throws ApVOUOverapplicationNotAllowedException,
        GlobalTransactionAlreadyLockedException,
		ApCHKVoucherHasNoWTaxCodeException {
			
		Debug.print("ApPaymentEntryControllerBean addApDrEntry");
		
		LocalApAppliedVoucherHome apAppliedVoucherHome = null;        
		LocalApVoucherPaymentScheduleHome apVoucherPaymentScheduleHome = null;
		LocalAdCompanyHome adCompanyHome = null;
           
                
        // Initialize EJB Home
        
        try {
            
            apAppliedVoucherHome = (LocalApAppliedVoucherHome)EJBHomeFactory.
                lookUpLocalHome(LocalApAppliedVoucherHome.JNDI_NAME, LocalApAppliedVoucherHome.class);            
            apVoucherPaymentScheduleHome = (LocalApVoucherPaymentScheduleHome)EJBHomeFactory.
                lookUpLocalHome(LocalApVoucherPaymentScheduleHome.JNDI_NAME, LocalApVoucherPaymentScheduleHome.class);            
            adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);            
                        
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }            
                
        try {
        	
        	// get company
        	
        	LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
        	
        	// get functional currency name
        	
        	String FC_NM = adCompany.getGlFunctionalCurrency().getFcName();
        	        	
        	
        	// validate overapplication
        	
        	LocalApVoucherPaymentSchedule apVoucherPaymentSchedule = 
        	    apVoucherPaymentScheduleHome.findByPrimaryKey(mdetails.getAvVpsCode());
        	    
        	if (EJBCommon.roundIt(apVoucherPaymentSchedule.getVpsAmountDue() - apVoucherPaymentSchedule.getVpsAmountPaid(), this.getGlFcPrecisionUnit(AD_CMPNY)) <
        	    EJBCommon.roundIt(mdetails.getAvApplyAmount() + mdetails.getAvTaxWithheld() + mdetails.getAvDiscountAmount(), this.getGlFcPrecisionUnit(AD_CMPNY))) {
        	    	
        	    throw new ApVOUOverapplicationNotAllowedException(apVoucherPaymentSchedule.getApVoucher().getVouDocumentNumber() + "-" + apVoucherPaymentSchedule.getVpsNumber());
        	    	
        	}
        	
        	// validate if vps already locked
        	
        	if (apVoucherPaymentSchedule.getVpsLock() == EJBCommon.TRUE) {
        		
        		throw new GlobalTransactionAlreadyLockedException(apVoucherPaymentSchedule.getApVoucher().getVouDocumentNumber() + "-" + apVoucherPaymentSchedule.getVpsNumber());
        		
        	}
        	
        	// validate voucher wtax code if necessary
        	
        	if (mdetails.getAvTaxWithheld() > 0 && 
        	    apVoucherPaymentSchedule.getApVoucher().getApWithholdingTaxCode().getGlChartOfAccount() == null) {
        		
        		throw new ApCHKVoucherHasNoWTaxCodeException(apVoucherPaymentSchedule.getApVoucher().getVouDocumentNumber() + "-" + apVoucherPaymentSchedule.getVpsNumber());
        		
        	}
        	
        	double AV_FRX_GN_LSS = 0d;
        	
        	if (!FC_NM.equals(apVoucherPaymentSchedule.getApVoucher().getGlFunctionalCurrency().getFcName()) ||
        	    !FC_NM.equals(apCheck.getGlFunctionalCurrency().getFcName())) {
        	   
        	    double AV_ALLCTD_PYMNT_AMNT = this.convertForeignToFunctionalCurrency(apCheck.getGlFunctionalCurrency().getFcCode(),
        	                                       apCheck.getGlFunctionalCurrency().getFcName(),
        	                                       apCheck.getChkConversionDate(),
        	                                       apCheck.getChkConversionRate(),
        	                                       mdetails.getAvAllocatedPaymentAmount(), AD_CMPNY);
        	                                       
        	    double AV_APPLY_AMNT = this.convertForeignToFunctionalCurrency(apVoucherPaymentSchedule.getApVoucher().getGlFunctionalCurrency().getFcCode(),
        	                                       apVoucherPaymentSchedule.getApVoucher().getGlFunctionalCurrency().getFcName(),
        	                                       apVoucherPaymentSchedule.getApVoucher().getVouConversionDate(),
        	                                       apVoucherPaymentSchedule.getApVoucher().getVouConversionRate(),
        	                                       mdetails.getAvApplyAmount(), AD_CMPNY);
        	                                       
        	    double AV_TX_WTHHLD = this.convertForeignToFunctionalCurrency(apVoucherPaymentSchedule.getApVoucher().getGlFunctionalCurrency().getFcCode(),
        	                                       apVoucherPaymentSchedule.getApVoucher().getGlFunctionalCurrency().getFcName(),
        	                                       apVoucherPaymentSchedule.getApVoucher().getVouConversionDate(),
        	                                       apVoucherPaymentSchedule.getApVoucher().getVouConversionRate(),
        	                                       mdetails.getAvTaxWithheld(), AD_CMPNY);
        	                                       
        	    double AV_DSCNT_AMNT = this.convertForeignToFunctionalCurrency(apVoucherPaymentSchedule.getApVoucher().getGlFunctionalCurrency().getFcCode(),
        	                                       apVoucherPaymentSchedule.getApVoucher().getGlFunctionalCurrency().getFcName(),
        	                                       apVoucherPaymentSchedule.getApVoucher().getVouConversionDate(),
        	                                       apVoucherPaymentSchedule.getApVoucher().getVouConversionRate(),
        	                                       mdetails.getAvDiscountAmount(), AD_CMPNY);
        	    	
        	    AV_FRX_GN_LSS = EJBCommon.roundIt((AV_ALLCTD_PYMNT_AMNT + AV_TX_WTHHLD + AV_DSCNT_AMNT) -
        	                    (AV_APPLY_AMNT + AV_TX_WTHHLD + AV_DSCNT_AMNT), this.getGlFcPrecisionUnit(AD_CMPNY));
        	    
        	}
		    
		    // create applied voucher
		    
		    LocalApAppliedVoucher apAppliedVoucher = apAppliedVoucherHome.create(mdetails.getAvApplyAmount(),
		        mdetails.getAvTaxWithheld(), mdetails.getAvDiscountAmount(), mdetails.getAvAllocatedPaymentAmount(), AV_FRX_GN_LSS, AD_CMPNY);
		        
		    apCheck.addApAppliedVoucher(apAppliedVoucher);		    
		    apVoucherPaymentSchedule.addApAppliedVoucher(apAppliedVoucher);		    		    
		    
		    // lock voucher
		    
		    apVoucherPaymentSchedule.setVpsLock(EJBCommon.TRUE);
		    
		    return apAppliedVoucher;
		    		    		    		    
		} catch (ApVOUOverapplicationNotAllowedException ex) {        	
        	
        	getSessionContext().setRollbackOnly();
        	throw ex;	
        	
        } catch (GlobalTransactionAlreadyLockedException ex) {        	
        	
        	getSessionContext().setRollbackOnly();
        	throw ex;	
        	
        } catch (ApCHKVoucherHasNoWTaxCodeException ex) {        	
        	
        	getSessionContext().setRollbackOnly();
        	throw ex;	   		    
        	
        } catch (Exception ex) {
        	
        	Debug.printStackTrace(ex);
        	getSessionContext().setRollbackOnly();
        	throw new EJBException(ex.getMessage());
        	
        }
		
	}
	
	private void addApDrEntry(short DR_LN, String DR_CLSS,
	    byte DR_DBT, double DR_AMNT, byte DR_RVRSD, Integer COA_CODE, LocalApCheck apCheck,
	    LocalApAppliedVoucher apAppliedVoucher, Integer AD_CMPNY) {
			
		Debug.print("ApPaymentEntryControllerBean addApDrEntry");
		
		LocalApDistributionRecordHome apDistributionRecordHome = null;        
		LocalGlChartOfAccountHome glChartOfAccountHome = null;
		LocalAdCompanyHome adCompanyHome = null;
           
                
        // Initialize EJB Home
        
        try {
            
            apDistributionRecordHome = (LocalApDistributionRecordHome)EJBHomeFactory.
                lookUpLocalHome(LocalApDistributionRecordHome.JNDI_NAME, LocalApDistributionRecordHome.class);            
            glChartOfAccountHome = (LocalGlChartOfAccountHome)EJBHomeFactory.
                lookUpLocalHome(LocalGlChartOfAccountHome.JNDI_NAME, LocalGlChartOfAccountHome.class);            
            adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);            
             
                        
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }            
                
        try {
        	
        	// get company
        	
        	LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
        	        	
        	LocalGlChartOfAccount glChartOfAccount = glChartOfAccountHome.findByPrimaryKey(COA_CODE);
        	    
		    // create distribution record 
		    
		    LocalApDistributionRecord apDistributionRecord = apDistributionRecordHome.create(
			    DR_LN, DR_CLSS, EJBCommon.roundIt(DR_AMNT, adCompany.getGlFunctionalCurrency().getFcPrecision()),
			    DR_DBT, EJBCommon.FALSE, DR_RVRSD, AD_CMPNY);
			    
			//apCheck.addApDistributionRecord(apDistributionRecord);
			apDistributionRecord.setApCheck(apCheck);
			//glChartOfAccount.addApDistributionRecord(apDistributionRecord);
			apDistributionRecord.setGlChartOfAccount(glChartOfAccount);
			
			// to be used by gl journal interface for cross currency receipts
			if (apAppliedVoucher != null) {
			
				//apAppliedVoucher.addApDistributionRecord(apDistributionRecord);
				apDistributionRecord.setApAppliedVoucher(apAppliedVoucher);
				
			}
		    		        		
        } catch (Exception ex) {
        	
        	Debug.printStackTrace(ex);
        	getSessionContext().setRollbackOnly();
        	throw new EJBException(ex.getMessage());
        	
        }
		
	}		
	

    private double convertForeignToFunctionalCurrency(Integer FC_CODE, String FC_NM, 
	    Date CONVERSION_DATE, double CONVERSION_RATE, double AMOUNT, Integer AD_CMPNY) {
	    	
	    Debug.print("ApPaymentEntryControllerBean convertForeignToFunctionalCurrency");
	    
	    
        LocalGlFunctionalCurrencyRateHome glFunctionalCurrencyRateHome = null;
        LocalAdCompanyHome adCompanyHome = null;
         
        LocalAdCompany adCompany = null;
                 
        // Initialize EJB Homes
         
        try {
         	
            glFunctionalCurrencyRateHome = (LocalGlFunctionalCurrencyRateHome)EJBHomeFactory.
               lookUpLocalHome(LocalGlFunctionalCurrencyRateHome.JNDI_NAME, LocalGlFunctionalCurrencyRateHome.class);                         
            adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
               lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);             
                   
         } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
         }
         
         // get company and extended precision
         
         try {
         	
             adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
         	             
         } catch (Exception ex) {
         	
             throw new EJBException(ex.getMessage());
         	
         }	     
        
         
         // Convert to functional currency if necessary
         
         if (CONVERSION_RATE != 1 && CONVERSION_RATE != 0 ) {
         	         
             AMOUNT = AMOUNT * CONVERSION_RATE;
             	
         } else if (CONVERSION_DATE != null) {
         	
         	 try {
         	 	    
         	 	 // Get functional currency rate
         	 	 
         	     LocalGlFunctionalCurrencyRate glReceiptFunctionalCurrencyRate = null;
         	     
         	     if (!FC_NM.equals("USD")) {
         	     	
        	         glReceiptFunctionalCurrencyRate = 
	     	             glFunctionalCurrencyRateHome.findByFcCodeAndDate(FC_CODE, 
	     	             CONVERSION_DATE, AD_CMPNY);
	     	             
	     	         AMOUNT = AMOUNT * glReceiptFunctionalCurrencyRate.getFrXToUsd();
	     	             
         	     }
                 
                 // Get set of book functional currency rate if necessary
         	 	         
                 if (!adCompany.getGlFunctionalCurrency().getFcName().equals("USD")) {
                 
                     LocalGlFunctionalCurrencyRate glCompanyFunctionalCurrencyRate = 
                         glFunctionalCurrencyRateHome.findByFcCodeAndDate(adCompany.getGlFunctionalCurrency().
                             getFcCode(), CONVERSION_DATE, AD_CMPNY);
                     
                     AMOUNT = AMOUNT / glCompanyFunctionalCurrencyRate.getFrXToUsd();
                 
                  }
                                 
         	             
         	 } catch (Exception ex) {
         	 	
         	 	throw new EJBException(ex.getMessage());
         	 	
         	 }       
         	
         }
         
         return EJBCommon.roundIt(AMOUNT, adCompany.getGlFunctionalCurrency().getFcPrecision());
         
	}
        
    private void executeApChkPost(Integer CHK_CODE, String USR_NM, Integer AD_BRNCH, Integer AD_CMPNY) throws
        GlobalRecordAlreadyDeletedException,
        GlobalTransactionAlreadyPostedException,
		GlobalTransactionAlreadyVoidPostedException,
		GlJREffectiveDateNoPeriodExistException,
		GlJREffectiveDatePeriodClosedException,
		GlobalJournalNotBalanceException, 
		AdPRFCoaGlVarianceAccountNotFoundException {
        
        Debug.print("ApCheckBatchSubmitControllerBean executeApChkPost");
        
        LocalApCheckHome apCheckHome = null;        
        LocalAdCompanyHome adCompanyHome = null;
        LocalAdPreferenceHome adPreferenceHome = null;
        LocalGlSetOfBookHome glSetOfBookHome = null;
        LocalGlAccountingCalendarValueHome glAccountingCalendarValueHome = null;
        LocalGlJournalHome glJournalHome = null;
        LocalGlJournalBatchHome glJournalBatchHome = null;
        LocalGlSuspenseAccountHome glSuspenseAccountHome = null;
        LocalGlJournalLineHome glJournalLineHome = null;
        LocalGlJournalSourceHome glJournalSourceHome = null;
        LocalGlJournalCategoryHome glJournalCategoryHome = null;
        LocalGlFunctionalCurrencyHome glFunctionalCurrencyHome = null;
        LocalApDistributionRecordHome apDistributionRecordHome = null;
        LocalInvCostingHome invCostingHome = null;
        LocalGlChartOfAccountHome glChartOfAccountHome = null;
        LocalAdBankAccountHome adBankAccountHome = null;
        LocalAdBankAccountBalanceHome adBankAccountBalanceHome = null;
 		LocalGlForexLedgerHome glForexLedgerHome = null;
 		LocalGlFunctionalCurrencyRateHome glFunctionalCurrencyRateHome = null; 
 		
        LocalApCheck apCheck = null;         
                
        // Initialize EJB Home
        
        try {
            
            apCheckHome = (LocalApCheckHome)EJBHomeFactory.
                lookUpLocalHome(LocalApCheckHome.JNDI_NAME, LocalApCheckHome.class);
            adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
            adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
            	lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);
            glSetOfBookHome = (LocalGlSetOfBookHome)EJBHomeFactory.
            	lookUpLocalHome(LocalGlSetOfBookHome.JNDI_NAME, LocalGlSetOfBookHome.class);
            glAccountingCalendarValueHome = (LocalGlAccountingCalendarValueHome)EJBHomeFactory.
            	lookUpLocalHome(LocalGlAccountingCalendarValueHome.JNDI_NAME, LocalGlAccountingCalendarValueHome.class);
            glJournalHome = (LocalGlJournalHome)EJBHomeFactory.
            	lookUpLocalHome(LocalGlJournalHome.JNDI_NAME, LocalGlJournalHome.class);
            glJournalBatchHome = (LocalGlJournalBatchHome)EJBHomeFactory.
            	lookUpLocalHome(LocalGlJournalBatchHome.JNDI_NAME, LocalGlJournalBatchHome.class);
            glSuspenseAccountHome = (LocalGlSuspenseAccountHome)EJBHomeFactory.
            	lookUpLocalHome(LocalGlSuspenseAccountHome.JNDI_NAME, LocalGlSuspenseAccountHome.class);
            glJournalLineHome = (LocalGlJournalLineHome)EJBHomeFactory.
            	lookUpLocalHome(LocalGlJournalLineHome.JNDI_NAME, LocalGlJournalLineHome.class);
            glJournalSourceHome = (LocalGlJournalSourceHome)EJBHomeFactory.
            	lookUpLocalHome(LocalGlJournalSourceHome.JNDI_NAME, LocalGlJournalSourceHome.class);
            glJournalCategoryHome = (LocalGlJournalCategoryHome)EJBHomeFactory.
            	lookUpLocalHome(LocalGlJournalCategoryHome.JNDI_NAME, LocalGlJournalCategoryHome.class);
            glFunctionalCurrencyHome = (LocalGlFunctionalCurrencyHome)EJBHomeFactory.
            	lookUpLocalHome(LocalGlFunctionalCurrencyHome.JNDI_NAME, LocalGlFunctionalCurrencyHome.class);
            apDistributionRecordHome = (LocalApDistributionRecordHome)EJBHomeFactory.
            	lookUpLocalHome(LocalApDistributionRecordHome.JNDI_NAME, LocalApDistributionRecordHome.class);
            invCostingHome = (LocalInvCostingHome)EJBHomeFactory.
            	lookUpLocalHome(LocalInvCostingHome.JNDI_NAME, LocalInvCostingHome.class);  
            glChartOfAccountHome = (LocalGlChartOfAccountHome)EJBHomeFactory.
            	lookUpLocalHome(LocalGlChartOfAccountHome.JNDI_NAME, LocalGlChartOfAccountHome.class);
            adBankAccountHome = (LocalAdBankAccountHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdBankAccountHome.JNDI_NAME, LocalAdBankAccountHome.class);
            adBankAccountBalanceHome = (LocalAdBankAccountBalanceHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdBankAccountBalanceHome.JNDI_NAME, LocalAdBankAccountBalanceHome.class);
 			glForexLedgerHome  = (LocalGlForexLedgerHome)EJBHomeFactory.
				lookUpLocalHome(LocalGlForexLedgerHome.JNDI_NAME, LocalGlForexLedgerHome.class);
			glFunctionalCurrencyRateHome = (LocalGlFunctionalCurrencyRateHome)EJBHomeFactory.
				lookUpLocalHome(LocalGlFunctionalCurrencyRateHome.JNDI_NAME, LocalGlFunctionalCurrencyRateHome.class);        
                                    
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }       
        
        try {
        	
        	// validate if check is already deleted
        	
        	try {
        		
        		apCheck = apCheckHome.findByPrimaryKey(CHK_CODE);
        		
        	} catch (FinderException ex) {
        		
        		throw new GlobalRecordAlreadyDeletedException();
        		
        	}
        	
        	// validate if check is already posted
        	
        	if (apCheck.getChkVoid() == EJBCommon.FALSE && apCheck.getChkPosted() == EJBCommon.TRUE) {
        		
        		throw new GlobalTransactionAlreadyPostedException();
        		
        		// validate if check void is already posted
        		
        	} else if (apCheck.getChkVoid() == EJBCommon.TRUE && apCheck.getChkVoidPosted() == EJBCommon.TRUE) {
        		
        		throw new GlobalTransactionAlreadyVoidPostedException();
        		
        	}
        	
        	
        	// post check
        	
        	if (apCheck.getChkVoid() == EJBCommon.FALSE && apCheck.getChkPosted() == EJBCommon.FALSE) {
        		
        		
        		if (apCheck.getChkType().equals("PAYMENT")) {        		
        			
        			// increase amount paid in voucher payment schedules and voucher
        			
        			double CHK_CRDTS = 0d;
        			
        			Collection apAppliedVouchers = apCheck.getApAppliedVouchers();
        			
        			Iterator i = apAppliedVouchers.iterator();
        			
        			while (i.hasNext()) {
        				
        				LocalApAppliedVoucher apAppliedVoucher = (LocalApAppliedVoucher)i.next();
        				
        				LocalApVoucherPaymentSchedule apVoucherPaymentSchedule = apAppliedVoucher.getApVoucherPaymentSchedule();
        				
        				double AMOUNT_PAID = apAppliedVoucher.getAvApplyAmount() + apAppliedVoucher.getAvDiscountAmount() + apAppliedVoucher.getAvTaxWithheld();
        				
        				CHK_CRDTS += this.convertForeignToFunctionalCurrency(apAppliedVoucher.getApVoucherPaymentSchedule().getApVoucher().getGlFunctionalCurrency().getFcCode(),
        						apAppliedVoucher.getApVoucherPaymentSchedule().getApVoucher().getGlFunctionalCurrency().getFcName(),
								apAppliedVoucher.getApVoucherPaymentSchedule().getApVoucher().getVouConversionDate(),
								apAppliedVoucher.getApVoucherPaymentSchedule().getApVoucher().getVouConversionRate(),
								apAppliedVoucher.getAvDiscountAmount() + apAppliedVoucher.getAvTaxWithheld(), AD_CMPNY);
        				
        				apVoucherPaymentSchedule.setVpsAmountPaid(EJBCommon.roundIt(apVoucherPaymentSchedule.getVpsAmountPaid() + AMOUNT_PAID, this.getGlFcPrecisionUnit(AD_CMPNY)));
        				
        				apVoucherPaymentSchedule.getApVoucher().setVouAmountPaid(EJBCommon.roundIt(apVoucherPaymentSchedule.getApVoucher().getVouAmountPaid() + AMOUNT_PAID, this.getGlFcPrecisionUnit(AD_CMPNY)));
        				
        				// release voucher lock
        				
        				apVoucherPaymentSchedule.setVpsLock(EJBCommon.FALSE);        		   	
        				
        			}
        			
        			// decrease supplier balance
        			
        			double CHK_AMNT = this.convertForeignToFunctionalCurrency(apCheck.getGlFunctionalCurrency().getFcCode(),
        					apCheck.getGlFunctionalCurrency().getFcName(),
							apCheck.getChkConversionDate(), apCheck.getChkConversionRate(),
							apCheck.getChkAmount(), AD_CMPNY);		        		
        			
        			this.post(apCheck.getChkDate(), (CHK_AMNT + CHK_CRDTS) * -1, apCheck.getApSupplier(), AD_CMPNY); 
        			
        		} else if (apCheck.getChkType().equals("DIRECT") && !apCheck.getApVoucherLineItems().isEmpty()) {
        			
        			Iterator c = apCheck.getApVoucherLineItems().iterator();
        			
        			while(c.hasNext()) {
        				
        				LocalApVoucherLineItem apVoucherLineItem = (LocalApVoucherLineItem) c.next();
        				
        				String II_NM = apVoucherLineItem.getInvItemLocation().getInvItem().getIiName();
        				String LOC_NM = apVoucherLineItem.getInvItemLocation().getInvLocation().getLocName();
        				double ITM_COST = this.convertForeignToFunctionalCurrency(apCheck.getGlFunctionalCurrency().getFcCode(),
        						apCheck.getGlFunctionalCurrency().getFcName(),
								apCheck.getChkConversionDate(), apCheck.getChkConversionRate(),
								apVoucherLineItem.getVliAmount(), AD_CMPNY);
        				double QTY_RCVD = this.convertByUomFromAndItemAndQuantity(apVoucherLineItem.getInvUnitOfMeasure(), 
        						apVoucherLineItem.getInvItemLocation().getInvItem(), apVoucherLineItem.getVliQuantity(), AD_CMPNY);
        				
        				LocalInvCosting invCosting = null;
        				
        				try {
        					
        					invCosting  = invCostingHome.getByMaxCstDateToLongAndMaxCstLineNumberAndLessThanEqualCstDateAndIiNameAndLocName(apCheck.getChkDate(), II_NM, LOC_NM, AD_BRNCH, AD_CMPNY);
        					
        				} catch (FinderException ex) {
        					
        				}
        				
        				if (invCosting == null) {
        					
        					this.postToInv(apVoucherLineItem, apCheck.getChkDate(), QTY_RCVD, ITM_COST, QTY_RCVD, ITM_COST, 0d, null,
        							AD_BRNCH, AD_CMPNY);			
        					
        				} else {
        					
        			        //compute cost variance   
        					double CST_VRNC_VL = 0d;

        					if(invCosting.getCstRemainingQuantity() < 0)
        						CST_VRNC_VL = (invCosting.getCstRemainingQuantity() * (ITM_COST/QTY_RCVD) -
        								invCosting.getCstRemainingValue());

        					this.postToInv(apVoucherLineItem, apCheck.getChkDate(), QTY_RCVD, ITM_COST,
        							invCosting.getCstRemainingQuantity() + QTY_RCVD, invCosting.getCstRemainingValue() + ITM_COST,
									CST_VRNC_VL, USR_NM, AD_BRNCH, AD_CMPNY);
        					
        				}
        				
        			}
        			
        		}
        		
        		// decrease bank balance
        		
				LocalAdBankAccount adBankAccount = adBankAccountHome.findByPrimaryKey(apCheck.getAdBankAccount().getBaCode());
				
				try {
					
					// find bankaccount balance before or equal check date
					
					Collection adBankAccountBalances = adBankAccountBalanceHome.findByBeforeOrEqualDateAndBaCodeAndType(apCheck.getChkCheckDate(), apCheck.getAdBankAccount().getBaCode(), "BOOK", AD_CMPNY);

					if (!adBankAccountBalances.isEmpty()) {
						
						// get last check
						
						ArrayList adBankAccountBalanceList = new ArrayList(adBankAccountBalances);
						
						LocalAdBankAccountBalance adBankAccountBalance = (LocalAdBankAccountBalance)adBankAccountBalanceList.get(adBankAccountBalanceList.size() - 1);
						
						if (adBankAccountBalance.getBabDate().before(apCheck.getChkCheckDate())) {
							
							// create new balance

							LocalAdBankAccountBalance adNewBankAccountBalance = adBankAccountBalanceHome.create(
									apCheck.getChkCheckDate(), adBankAccountBalance.getBabBalance() - apCheck.getChkAmount(), "BOOK", AD_CMPNY);
							
							//adBankAccount.addAdBankAccountBalance(adNewBankAccountBalance);
							adNewBankAccountBalance.setAdBankAccount(adBankAccount);
							
						} else { // equals to check date

							adBankAccountBalance.setBabBalance(adBankAccountBalance.getBabBalance() - apCheck.getChkAmount());
							
						} 
						
					} else {        	
						
						// create new balance
						
						LocalAdBankAccountBalance adNewBankAccountBalance = adBankAccountBalanceHome.create(
								apCheck.getChkCheckDate(), (0 - apCheck.getChkAmount()), "BOOK", AD_CMPNY);
						
						//adBankAccount.addAdBankAccountBalance(adNewBankAccountBalance);
						adNewBankAccountBalance.setAdBankAccount(adBankAccount);
						
					}
					
					// propagate to subsequent balances if necessary
					
					adBankAccountBalances = adBankAccountBalanceHome.findByAfterDateAndBaCodeAndType(apCheck.getChkCheckDate(), apCheck.getAdBankAccount().getBaCode(), "BOOK", AD_CMPNY);

					Iterator i = adBankAccountBalances.iterator();
					
					while (i.hasNext()) {
						
						LocalAdBankAccountBalance adBankAccountBalance = (LocalAdBankAccountBalance)i.next();
						
						adBankAccountBalance.setBabBalance(adBankAccountBalance.getBabBalance() - apCheck.getChkAmount());
						
					}			
					
				} catch (Exception ex) {
					
					ex.printStackTrace();
					
				}
        		
        		// set check post status
        		
        		apCheck.setChkPosted(EJBCommon.TRUE);
        		apCheck.setChkPostedBy(USR_NM);
        		apCheck.setChkDatePosted(EJBCommon.getGcCurrentDateWoTime().getTime());
        		
        		
        	} else if (apCheck.getChkVoid() == EJBCommon.TRUE && apCheck.getChkVoidPosted() == EJBCommon.FALSE) { // void check
        		
        		
        		if (apCheck.getChkType().equals("PAYMENT")) {
        			
        			// decrease amount paid in voucher payment schedules and voucher
        			
        			double CHK_CRDTS = 0d;
        			
        			Collection apAppliedVouchers = apCheck.getApAppliedVouchers();
        			
        			Iterator i = apAppliedVouchers.iterator();
        			
        			while (i.hasNext()) {
        				
        				LocalApAppliedVoucher apAppliedVoucher = (LocalApAppliedVoucher)i.next();
        				
        				LocalApVoucherPaymentSchedule apVoucherPaymentSchedule = apAppliedVoucher.getApVoucherPaymentSchedule();
        				
        				double AMOUNT_PAID = apAppliedVoucher.getAvApplyAmount() + apAppliedVoucher.getAvDiscountAmount() + apAppliedVoucher.getAvTaxWithheld();
        				
        				CHK_CRDTS += this.convertForeignToFunctionalCurrency(apAppliedVoucher.getApVoucherPaymentSchedule().getApVoucher().getGlFunctionalCurrency().getFcCode(),
        						apAppliedVoucher.getApVoucherPaymentSchedule().getApVoucher().getGlFunctionalCurrency().getFcName(),
								apAppliedVoucher.getApVoucherPaymentSchedule().getApVoucher().getVouConversionDate(),
								apAppliedVoucher.getApVoucherPaymentSchedule().getApVoucher().getVouConversionRate(),
								apAppliedVoucher.getAvDiscountAmount() + apAppliedVoucher.getAvTaxWithheld(), AD_CMPNY);
        				
        				apVoucherPaymentSchedule.setVpsAmountPaid(EJBCommon.roundIt(apVoucherPaymentSchedule.getVpsAmountPaid() - AMOUNT_PAID, this.getGlFcPrecisionUnit(AD_CMPNY)));
        				
        				apVoucherPaymentSchedule.getApVoucher().setVouAmountPaid(EJBCommon.roundIt(apVoucherPaymentSchedule.getApVoucher().getVouAmountPaid() - AMOUNT_PAID, this.getGlFcPrecisionUnit(AD_CMPNY)));        		       		   	
        				
        			}  
        			
        			// increase supplier balance
        			
        			double CHK_AMNT = this.convertForeignToFunctionalCurrency(apCheck.getGlFunctionalCurrency().getFcCode(),
        					apCheck.getGlFunctionalCurrency().getFcName(),
							apCheck.getChkConversionDate(), apCheck.getChkConversionRate(),
							apCheck.getChkAmount(), AD_CMPNY);	        		
        			
        			this.post(apCheck.getChkDate(), CHK_AMNT + CHK_CRDTS, apCheck.getApSupplier(), AD_CMPNY);	        		
        			
        		} else if (apCheck.getChkType().equals("DIRECT") && !apCheck.getApVoucherLineItems().isEmpty()) {
        			
        			Iterator c = apCheck.getApVoucherLineItems().iterator();
        			
        			while(c.hasNext()) {
        				
        				LocalApVoucherLineItem apVoucherLineItem = (LocalApVoucherLineItem) c.next();
        				
        				String II_NM = apVoucherLineItem.getInvItemLocation().getInvItem().getIiName();
        				String LOC_NM = apVoucherLineItem.getInvItemLocation().getInvLocation().getLocName();
        				double ITM_COST = this.convertForeignToFunctionalCurrency(apCheck.getGlFunctionalCurrency().getFcCode(),
        						apCheck.getGlFunctionalCurrency().getFcName(),
								apCheck.getChkConversionDate(), apCheck.getChkConversionRate(),
								apVoucherLineItem.getVliAmount(), AD_CMPNY);
        				double QTY_RCVD = this.convertByUomFromAndItemAndQuantity(apVoucherLineItem.getInvUnitOfMeasure(), 
        						apVoucherLineItem.getInvItemLocation().getInvItem(), apVoucherLineItem.getVliQuantity(), AD_CMPNY);
        				
        				LocalInvCosting invCosting = null;
        				
        				try {
        					
        					invCosting  = invCostingHome.getByMaxCstDateToLongAndMaxCstLineNumberAndLessThanEqualCstDateAndIiNameAndLocName(apCheck.getChkDate(), II_NM, LOC_NM, AD_BRNCH, AD_CMPNY);
        					
        				} catch (FinderException ex) {
        					
        				}
        				
        				if (invCosting == null) {
        					
        					this.postToInv(apVoucherLineItem, apCheck.getChkDate(), -QTY_RCVD, -ITM_COST, -QTY_RCVD, -ITM_COST, 0d,
        							null, AD_BRNCH, AD_CMPNY);			
        					
        				} else {
        					
        					this.postToInv(apVoucherLineItem, apCheck.getChkDate(), -QTY_RCVD, -ITM_COST,
        							invCosting.getCstRemainingQuantity() - QTY_RCVD,invCosting.getCstRemainingValue() - ITM_COST, 0d,
									null, AD_BRNCH, AD_CMPNY);
        					
        				}
        				
        			}
        			
        		}
        		
        		// increase bank balance
        		
				LocalAdBankAccount adBankAccount = adBankAccountHome.findByPrimaryKey(apCheck.getAdBankAccount().getBaCode());
				
				try {
					
					// find bankaccount balance before or equal check date
					
					Collection adBankAccountBalances = adBankAccountBalanceHome.findByBeforeOrEqualDateAndBaCodeAndType(apCheck.getChkCheckDate(), apCheck.getAdBankAccount().getBaCode(), "BOOK", AD_CMPNY);

					if (!adBankAccountBalances.isEmpty()) {
						
						// get last check
						
						ArrayList adBankAccountBalanceList = new ArrayList(adBankAccountBalances);
						
						LocalAdBankAccountBalance adBankAccountBalance = (LocalAdBankAccountBalance)adBankAccountBalanceList.get(adBankAccountBalanceList.size() - 1);
						
						if (adBankAccountBalance.getBabDate().before(apCheck.getChkCheckDate())) {
							
							// create new balance

							LocalAdBankAccountBalance adNewBankAccountBalance = adBankAccountBalanceHome.create(
									apCheck.getChkCheckDate(), adBankAccountBalance.getBabBalance() + apCheck.getChkAmount(), "BOOK", AD_CMPNY);
							
							//adBankAccount.addAdBankAccountBalance(adNewBankAccountBalance);
							adNewBankAccountBalance.setAdBankAccount(adBankAccount);
							
						} else { // equals to check date

							adBankAccountBalance.setBabBalance(adBankAccountBalance.getBabBalance() + apCheck.getChkAmount());
							
						} 
						
					} else {        	
						
						// create new balance
						
						LocalAdBankAccountBalance adNewBankAccountBalance = adBankAccountBalanceHome.create(
								apCheck.getChkCheckDate(), (apCheck.getChkAmount()), "BOOK", AD_CMPNY);
						
						//adBankAccount.addAdBankAccountBalance(adNewBankAccountBalance);
						adNewBankAccountBalance.setAdBankAccount(adBankAccount);
						
					}
					
					// propagate to subsequent balances if necessary
					
					adBankAccountBalances = adBankAccountBalanceHome.findByAfterDateAndBaCodeAndType(apCheck.getChkCheckDate(), apCheck.getAdBankAccount().getBaCode(), "BOOK", AD_CMPNY);

					Iterator i = adBankAccountBalances.iterator();
					
					while (i.hasNext()) {
						
						LocalAdBankAccountBalance adBankAccountBalance = (LocalAdBankAccountBalance)i.next();
						
						adBankAccountBalance.setBabBalance(adBankAccountBalance.getBabBalance() + apCheck.getChkAmount());
						
					}			
					
				} catch (Exception ex) {
					
					ex.printStackTrace();
					
				}
        		
        		// set check post status
        		
        		apCheck.setChkVoidPosted(EJBCommon.TRUE);
        		apCheck.setChkPostedBy(USR_NM);
        		apCheck.setChkDatePosted(EJBCommon.getGcCurrentDateWoTime().getTime());
        		
        	}
        	
        	// post to gl if necessary
        	
        	LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);
        	LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
        	
        	if (adPreference.getPrfApGlPostingType().equals("AUTO-POST UPON APPROVAL")) {
        		
        		// validate if date has no period and period is closed
        		
        		LocalGlSetOfBook glJournalSetOfBook = null;
        		
        		try {
        			
        			glJournalSetOfBook = glSetOfBookHome.findByDate(apCheck.getChkDate(), AD_CMPNY);
        			
        		} catch (FinderException ex) {
        			
        			throw new GlJREffectiveDateNoPeriodExistException();
        			
        		}
        		
        		LocalGlAccountingCalendarValue glAccountingCalendarValue = 
        			glAccountingCalendarValueHome.findByAcCodeAndDate(
        					glJournalSetOfBook.getGlAccountingCalendar().getAcCode(), apCheck.getChkDate(), AD_CMPNY);
        		
        		
        		if (glAccountingCalendarValue.getAcvStatus() == 'N' ||
        				glAccountingCalendarValue.getAcvStatus() == 'C' ||
						glAccountingCalendarValue.getAcvStatus() == 'P') {
        			
        			throw new GlJREffectiveDatePeriodClosedException();
        			
        		}
        		
        		// check if check is balance if not check suspense posting
        		
        		LocalGlJournalLine glOffsetJournalLine = null;
        		
        		Collection apDistributionRecords = null;
        		
        		if (apCheck.getChkVoid() == EJBCommon.FALSE) {
        			
        			apDistributionRecords = apDistributionRecordHome.findImportableDrByDrReversedAndChkCode(EJBCommon.FALSE, apCheck.getChkCode(), AD_CMPNY);
        			
        		} else {
        			
        			apDistributionRecords = apDistributionRecordHome.findImportableDrByDrReversedAndChkCode(EJBCommon.TRUE, apCheck.getChkCode(), AD_CMPNY);
        			
        		}
        		
        		Iterator j = apDistributionRecords.iterator();
        		
        		double TOTAL_DEBIT = 0d;
        		double TOTAL_CREDIT = 0d;
        		
        		while (j.hasNext()) {
        			
        			LocalApDistributionRecord apDistributionRecord = (LocalApDistributionRecord)j.next();
        			
        			double DR_AMNT = 0d;
        			
        			if (apDistributionRecord.getApAppliedVoucher() != null) {
        				
        				LocalApVoucher apVoucher = apDistributionRecord.getApAppliedVoucher().getApVoucherPaymentSchedule().getApVoucher();
        				
        				DR_AMNT = this.convertForeignToFunctionalCurrency(apVoucher.getGlFunctionalCurrency().getFcCode(),
        						apVoucher.getGlFunctionalCurrency().getFcName(), 
								apVoucher.getVouConversionDate(),
								apVoucher.getVouConversionRate(),
								apDistributionRecord.getDrAmount(), AD_CMPNY);
        				
        			} else {
        				
        				DR_AMNT = this.convertForeignToFunctionalCurrency(apCheck.getGlFunctionalCurrency().getFcCode(),
        						apCheck.getGlFunctionalCurrency().getFcName(), 
								apCheck.getChkConversionDate(),
								apCheck.getChkConversionRate(),
								apDistributionRecord.getDrAmount(), AD_CMPNY);            			
        				
        			}   
        			
        			if (apDistributionRecord.getDrDebit() == EJBCommon.TRUE) {
        				
        				TOTAL_DEBIT += DR_AMNT;
        				
        			} else {
        				
        				TOTAL_CREDIT += DR_AMNT;
        				
        			}
        			
        		}
        		
        		TOTAL_DEBIT = EJBCommon.roundIt(TOTAL_DEBIT, adCompany.getGlFunctionalCurrency().getFcPrecision());
        		TOTAL_CREDIT = EJBCommon.roundIt(TOTAL_CREDIT, adCompany.getGlFunctionalCurrency().getFcPrecision());
        		
        		if (adPreference.getPrfAllowSuspensePosting() == EJBCommon.TRUE &&
        				TOTAL_DEBIT != TOTAL_CREDIT) {
        			
        			LocalGlSuspenseAccount glSuspenseAccount = null;
        			
        			try { 	
        				
        				glSuspenseAccount = glSuspenseAccountHome.findByJsNameAndJcName("ACCOUNTS PAYABLES", "CHECKS", AD_CMPNY);
        				
        			} catch (FinderException ex) {
        				
        				throw new GlobalJournalNotBalanceException();
        				
        			}
        			
        			if (TOTAL_DEBIT - TOTAL_CREDIT < 0) {
        				
        				glOffsetJournalLine = glJournalLineHome.create(
        						(short)(apDistributionRecords.size() + 1),
								EJBCommon.TRUE,
								TOTAL_CREDIT - TOTAL_DEBIT, "", AD_CMPNY);
        				
        			} else {
        				
        				glOffsetJournalLine = glJournalLineHome.create(
        						(short)(apDistributionRecords.size() + 1),
								EJBCommon.FALSE,
								TOTAL_DEBIT - TOTAL_CREDIT, "", AD_CMPNY);
        				
        			}
        			
        			LocalGlChartOfAccount glChartOfAccount = glSuspenseAccount.getGlChartOfAccount();
        			//glChartOfAccount.addGlJournalLine(glOffsetJournalLine);
        			glOffsetJournalLine.setGlChartOfAccount(glChartOfAccount);
        			
        			
        		} else if (adPreference.getPrfAllowSuspensePosting() == EJBCommon.FALSE &&
        				TOTAL_DEBIT != TOTAL_CREDIT) {
        			
        			throw new GlobalJournalNotBalanceException();		    	
        			
        		}
        		
        		// create journal batch if necessary
        		
        		LocalGlJournalBatch glJournalBatch = null;
        		java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat("MM/dd/yyyy");
        		
        		try {
        			
        			if (adPreference.getPrfEnableApCheckBatch() == EJBCommon.TRUE) {
        				
        				glJournalBatch = glJournalBatchHome.findByJbName("JOURNAL IMPORT " + formatter.format(new Date()) + " " + apCheck.getApCheckBatch().getCbName(), AD_BRNCH, AD_CMPNY);
        				
        			} else {
        				
        				glJournalBatch = glJournalBatchHome.findByJbName("JOURNAL IMPORT " + formatter.format(new Date()) + " CHECKS", AD_BRNCH, AD_CMPNY);
        				
        			}		           
        			
        		} catch (FinderException ex) {
        		}
        		
        		if (adPreference.getPrfEnableGlJournalBatch() ==  EJBCommon.TRUE && 
        				glJournalBatch == null) {
        			
        			if (adPreference.getPrfEnableApCheckBatch() == EJBCommon.TRUE) {
        				
        				glJournalBatch = glJournalBatchHome.create("JOURNAL IMPORT " + formatter.format(new Date()) + " " + apCheck.getApCheckBatch().getCbName(), "JOURNAL IMPORT", "CLOSED", EJBCommon.getGcCurrentDateWoTime().getTime(), USR_NM, AD_BRNCH, AD_CMPNY);
        				
        			} else {
        				
        				glJournalBatch = glJournalBatchHome.create("JOURNAL IMPORT " + formatter.format(new Date()) + " CHECKS", "JOURNAL IMPORT", "CLOSED", EJBCommon.getGcCurrentDateWoTime().getTime(), USR_NM, AD_BRNCH, AD_CMPNY);
        				
        			}
        			
        			
        		}
        		
        		// create journal entry			            	
        		
        		LocalGlJournal glJournal = glJournalHome.create(apCheck.getChkReferenceNumber(),
        				apCheck.getChkDescription(), apCheck.getChkDate(),
						0.0d, null, apCheck.getChkDocumentNumber(), null, 1d, "N/A", null,
						'N', EJBCommon.TRUE, EJBCommon.FALSE,
						USR_NM, new Date(),
						USR_NM, new Date(),
						null, null,
						USR_NM, EJBCommon.getGcCurrentDateWoTime().getTime(), 
						apCheck.getApSupplier().getSplTin(), 
						apCheck.getApSupplier().getSplName(), EJBCommon.FALSE,
						null, 
						AD_BRNCH, AD_CMPNY);
        		
        		LocalGlJournalSource glJournalSource = glJournalSourceHome.findByJsName("ACCOUNTS PAYABLES", AD_CMPNY);
        		//glJournalSource.addGlJournal(glJournal);
        		glJournal.setGlJournalSource(glJournalSource);
        		
        		LocalGlFunctionalCurrency glFunctionalCurrency = glFunctionalCurrencyHome.findByFcName(adCompany.getGlFunctionalCurrency().getFcName(), AD_CMPNY);
        		//glFunctionalCurrency.addGlJournal(glJournal);
        		glJournal.setGlFunctionalCurrency(glFunctionalCurrency);
        		
        		LocalGlJournalCategory glJournalCategory = glJournalCategoryHome.findByJcName("CHECKS", AD_CMPNY);
        		//glJournalCategory.addGlJournal(glJournal);
        		glJournal.setGlJournalCategory(glJournalCategory);
        		
        		if (glJournalBatch != null) {
        			
        			//glJournalBatch.addGlJournal(glJournal);
        			glJournal.setGlJournalBatch(glJournalBatch);
        			
        		}           		    
        		
        		
        		// create journal lines
        		
        		j = apDistributionRecords.iterator();
        		boolean firstFlag = true;
        		
        		while (j.hasNext()) {
        			
        			LocalApDistributionRecord apDistributionRecord = (LocalApDistributionRecord)j.next();
        			
        			double DR_AMNT = 0d;
        			LocalApVoucher apVoucher = null;
        			
        			if (apDistributionRecord.getApAppliedVoucher() != null) {
        				
        				apVoucher = apDistributionRecord.getApAppliedVoucher().getApVoucherPaymentSchedule().getApVoucher();
        				
        				DR_AMNT = this.convertForeignToFunctionalCurrency(apVoucher.getGlFunctionalCurrency().getFcCode(),
        						apVoucher.getGlFunctionalCurrency().getFcName(), 
								apVoucher.getVouConversionDate(),
								apVoucher.getVouConversionRate(),
								apDistributionRecord.getDrAmount(), AD_CMPNY);
        				
        			} else {
        				
        				DR_AMNT = this.convertForeignToFunctionalCurrency(apCheck.getGlFunctionalCurrency().getFcCode(),
        						apCheck.getGlFunctionalCurrency().getFcName(), 
								apCheck.getChkConversionDate(),
								apCheck.getChkConversionRate(),
								apDistributionRecord.getDrAmount(), AD_CMPNY);            			
        				
        			}            		            		
        			
        			LocalGlJournalLine glJournalLine = glJournalLineHome.create(
        					apDistributionRecord.getDrLine(),	            			
							apDistributionRecord.getDrDebit(),
							DR_AMNT, "", AD_CMPNY);
        			
        			//apDistributionRecord.getGlChartOfAccount().addGlJournalLine(glJournalLine);
        			glJournalLine.setGlChartOfAccount(apDistributionRecord.getGlChartOfAccount());
        			
        			//glJournal.addGlJournalLine(glJournalLine);
        			glJournalLine.setGlJournal(glJournal);        			

        			apDistributionRecord.setDrImported(EJBCommon.TRUE);
        			
    		       	// for FOREX revaluation            	    
            	    
            	    int FC_CODE = apDistributionRecord.getApAppliedVoucher() != null ?
            	    	apVoucher.getGlFunctionalCurrency().getFcCode().intValue() :
            	    	apCheck.getGlFunctionalCurrency().getFcCode().intValue();

    		       	if((FC_CODE != adCompany.getGlFunctionalCurrency().getFcCode().intValue()) &&
    					glJournalLine.getGlChartOfAccount().getGlFunctionalCurrency() != null && (FC_CODE ==
    						glJournalLine.getGlChartOfAccount().getGlFunctionalCurrency().getFcCode().intValue())){
    		       		
    		       		double CONVERSION_RATE = apDistributionRecord.getApAppliedVoucher() != null ?
    		            	    apVoucher.getVouConversionRate() : apCheck.getChkConversionRate();

    		            Date DATE = apDistributionRecord.getApAppliedVoucher() != null ?
    		            	    apVoucher.getVouConversionDate() : apCheck.getChkConversionDate();

    		            if (DATE != null && (CONVERSION_RATE == 0 || CONVERSION_RATE == 1)){
    	    		       	
    		  	  			CONVERSION_RATE = this.getFrRateByFrNameAndFrDate(
    			  	  				glJournalLine.getGlChartOfAccount().getGlFunctionalCurrency().getFcName(), 
    								glJournal.getJrConversionDate(), AD_CMPNY);
    	    		       			
    		            } else if (CONVERSION_RATE == 0) {
    		       			
    		            	CONVERSION_RATE = 1;

    		       		} 
    		       		
    		       		Collection glForexLedgers = null;
    		       		
    		       		DATE = apDistributionRecord.getApAppliedVoucher() != null ?
    		            	    apVoucher.getVouDate() : apCheck.getChkDate();
    		       		
    		       		try {
    		       			
    		       			glForexLedgers = glForexLedgerHome.findLatestGlFrlByFrlDateAndByCoaCode(
    		       					DATE, glJournalLine.getGlChartOfAccount().getCoaCode(),
    								AD_CMPNY);
    		       			
    		       		} catch(FinderException ex) {
    		       			
    		       		}
    		       		
    		       		LocalGlForexLedger glForexLedger =
    		       			(glForexLedgers.isEmpty() || glForexLedgers == null) ? null :
    		       				(LocalGlForexLedger) glForexLedgers.iterator().next();
    		       		
    		       		int FRL_LN = (glForexLedger != null &&
    		       			glForexLedger.getFrlDate().compareTo(DATE) == 0) ?
    		       				glForexLedger.getFrlLine().intValue() + 1 : 1;
    		       		
    		       		// compute balance
    		       		double COA_FRX_BLNC = glForexLedger == null ? 0 : glForexLedger.getFrlBalance();
    		       		double FRL_AMNT = apDistributionRecord.getDrAmount();
    		       		
    		       		if(glJournalLine.getGlChartOfAccount().getCoaAccountType().equalsIgnoreCase("ASSET"))
    		       			FRL_AMNT = (glJournalLine.getJlDebit() == EJBCommon.TRUE ? FRL_AMNT : (- 1 * FRL_AMNT));  
    		       		else
    		       			FRL_AMNT = (glJournalLine.getJlDebit() == EJBCommon.TRUE ? (- 1 * FRL_AMNT) : FRL_AMNT);
    		       		
    		       		COA_FRX_BLNC = COA_FRX_BLNC + FRL_AMNT;
    		       		
    		       		double FRX_GN_LSS = 0d;
		       			
    		       		if (glOffsetJournalLine != null && firstFlag) {
    		       			
    		       			if(glOffsetJournalLine.getGlChartOfAccount().getCoaAccountType().equalsIgnoreCase("ASSET"))
    		       				
    		       				FRX_GN_LSS = (glOffsetJournalLine.getJlDebit() == EJBCommon.TRUE ?
    		       						glOffsetJournalLine.getJlAmount() : (- 1 * glOffsetJournalLine.getJlAmount()));
    		       			
    		       			else
    		       				
    		       				FRX_GN_LSS = (glOffsetJournalLine.getJlDebit() == EJBCommon.TRUE ?
    		       						(- 1 * glOffsetJournalLine.getJlAmount()) : glOffsetJournalLine.getJlAmount());
    		       			
    		       			firstFlag = false;
    		       			
    		       		}
    		       		
    		       		glForexLedger = glForexLedgerHome.create(DATE, new Integer (FRL_LN), "CHK", FRL_AMNT,
    		       				CONVERSION_RATE, COA_FRX_BLNC, FRX_GN_LSS, AD_CMPNY);
    		       		
    		       		//glJournalLine.getGlChartOfAccount().addGlForexLedger(glForexLedger);
    		       		glForexLedger.setGlChartOfAccount(glJournalLine.getGlChartOfAccount());
    		       		
    		       		// propagate balances
    		       		try{
    		       			
    		       			glForexLedgers = glForexLedgerHome.findByGreaterThanFrlDateAndCoaCode(
    		       					glForexLedger.getFrlDate(), glForexLedger.getGlChartOfAccount().getCoaCode(),
    								glForexLedger.getFrlAdCompany());
    		       			
    		       		} catch (FinderException ex) {
    		       			
    		       		}
    		       		
    		       		Iterator itrFrl = glForexLedgers.iterator();
    		       		
    		       		while (itrFrl.hasNext()) {
    		       			
    		       			glForexLedger = (LocalGlForexLedger) itrFrl.next();
    		       			FRL_AMNT = apDistributionRecord.getDrAmount();
    		       			
    		       			if(glJournalLine.getGlChartOfAccount().getCoaAccountType().equalsIgnoreCase("ASSET"))
    		       				FRL_AMNT = (glJournalLine.getJlDebit() == EJBCommon.TRUE ? FRL_AMNT :
    		       					(- 1 * FRL_AMNT));
    		       			else
    		       				FRL_AMNT = (glJournalLine.getJlDebit() == EJBCommon.TRUE ? (- 1 * FRL_AMNT) :
    		       					FRL_AMNT);
    		       			
    		       			glForexLedger.setFrlBalance(glForexLedger.getFrlBalance() + FRL_AMNT);
    		       			
    		       		}
    		       		
    		       	}
    		       	
        		}
        		
        		if (glOffsetJournalLine != null) {
        			
        			//glJournal.addGlJournalLine(glOffsetJournalLine);
        			glOffsetJournalLine.setGlJournal(glJournal);
        			
        		}		
        		
        		// post journal to gl
        		
        		Collection glJournalLines = glJournal.getGlJournalLines();
        		
        		Iterator i = glJournalLines.iterator();
        		
        		while (i.hasNext()) {
        			
        			LocalGlJournalLine glJournalLine = (LocalGlJournalLine)i.next();	
        			
        			// post current to current acv
        			
        			this.postToGl(glAccountingCalendarValue,
        					glJournalLine.getGlChartOfAccount(),
							true, glJournalLine.getJlDebit(), glJournalLine.getJlAmount(), AD_CMPNY);
        			
        			
        			// post to subsequent acvs (propagate)
        			
        			Collection glSubsequentAccountingCalendarValues = 
        				glAccountingCalendarValueHome.findSubsequentAcvByAcCodeAndAcvPeriodNumber( 
        						glJournalSetOfBook.getGlAccountingCalendar().getAcCode(),
								glAccountingCalendarValue.getAcvPeriodNumber(), AD_CMPNY);     
        			
        			Iterator acvsIter = glSubsequentAccountingCalendarValues.iterator();
        			
        			while (acvsIter.hasNext()) {
        				
        				LocalGlAccountingCalendarValue glSubsequentAccountingCalendarValue = 
        					(LocalGlAccountingCalendarValue)acvsIter.next();
        				
        				this.postToGl(glSubsequentAccountingCalendarValue,
        						glJournalLine.getGlChartOfAccount(),
								false, glJournalLine.getJlDebit(), glJournalLine.getJlAmount(), AD_CMPNY);
        				
        			}
        			
        			// post to subsequent years if necessary
        			
        			Collection glSubsequentSetOfBooks = glSetOfBookHome.findSubsequentSobByAcYear(glJournalSetOfBook.getGlAccountingCalendar().getAcYear(), AD_CMPNY);
        			
        			if (!glSubsequentSetOfBooks.isEmpty() && glJournalSetOfBook.getSobYearEndClosed() == 1) {
        				
        				adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);		  	  	
        				LocalGlChartOfAccount glRetainedEarningsAccount = glChartOfAccountHome.findByCoaAccountNumber(adCompany.getCmpRetainedEarnings(), AD_CMPNY); 	  	
        				
        				Iterator sobIter = glSubsequentSetOfBooks.iterator();
        				
        				while (sobIter.hasNext()) {
        					
        					LocalGlSetOfBook glSubsequentSetOfBook = (LocalGlSetOfBook)sobIter.next();
        					
        					String ACCOUNT_TYPE = glJournalLine.getGlChartOfAccount().getCoaAccountType();
        					
        					// post to subsequent acvs of subsequent set of book(propagate)
        					
        					Collection glAccountingCalendarValues = 
        						glAccountingCalendarValueHome.findByAcCode(glSubsequentSetOfBook.getGlAccountingCalendar().getAcCode(), AD_CMPNY);     
        					
        					Iterator acvIter = glAccountingCalendarValues.iterator();
        					
        					while (acvIter.hasNext()) {
        						
        						LocalGlAccountingCalendarValue glSubsequentAccountingCalendarValue = 
        							(LocalGlAccountingCalendarValue)acvIter.next();
        						
        						if (ACCOUNT_TYPE.equals("ASSET") || ACCOUNT_TYPE.equals("LIABILITY") ||
        								ACCOUNT_TYPE.equals("OWNERS EQUITY")) {
        							
        							this.postToGl(glSubsequentAccountingCalendarValue,
        									glJournalLine.getGlChartOfAccount(),
											false, glJournalLine.getJlDebit(), glJournalLine.getJlAmount(), AD_CMPNY);
        							
        						} else { // revenue & expense
        							
        							this.postToGl(glSubsequentAccountingCalendarValue,
        									glRetainedEarningsAccount,
											false, glJournalLine.getJlDebit(), glJournalLine.getJlAmount(), AD_CMPNY);					        
        							
        						}
        						
        					}
        					
        					if (glSubsequentSetOfBook.getSobYearEndClosed() == 0) break;
        					
        				}
        				
        			}
        			
        		}
        		
        		
        		
        		
        		
        		
        		
        		if(apCheck.getApSupplier().getApSupplierClass().getScLedger() == EJBCommon.TRUE){
					  
			    	// Post Investors Account balance  
				       
					    // post current to current acv
					       	
					       	this.postToGlInvestor(glAccountingCalendarValue,
					       			apCheck.getApSupplier(),
									true, (byte)1, apCheck.getChkAmount(), AD_CMPNY);
					       	
					       	
					       	// post to subsequent acvs (propagate)
					       	
					       	Collection glSubsequentAccountingCalendarValues = 
					       		glAccountingCalendarValueHome.findSubsequentAcvByAcCodeAndAcvPeriodNumber( 
					       				glJournalSetOfBook.getGlAccountingCalendar().getAcCode(),
										glAccountingCalendarValue.getAcvPeriodNumber(), AD_CMPNY);     
					       	
					       	Iterator acvsIter = glSubsequentAccountingCalendarValues.iterator();
					       	
					       	while (acvsIter.hasNext()) {
					       		
					       		LocalGlAccountingCalendarValue glSubsequentAccountingCalendarValue = 
					       			(LocalGlAccountingCalendarValue)acvsIter.next();
					       		
					       		
					       		this.postToGlInvestor(glSubsequentAccountingCalendarValue,
					       				apCheck.getApSupplier(),
						       			false, (byte)1, apCheck.getChkAmount(), AD_CMPNY);
					       		
					       	}

					       	
					       	// post to subsequent years if necessary
					       	
					       	Collection glSubsequentSetOfBooks = glSetOfBookHome.findSubsequentSobByAcYear(glJournalSetOfBook.getGlAccountingCalendar().getAcYear(), AD_CMPNY);
					       	
					       	if (!glSubsequentSetOfBooks.isEmpty() && glJournalSetOfBook.getSobYearEndClosed() == 1) {
					       		
					       		adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);		  	  	
					       		
					       		Iterator sobIter = glSubsequentSetOfBooks.iterator();
					       		
					       		while (sobIter.hasNext()) {
					       			
					       			LocalGlSetOfBook glSubsequentSetOfBook = (LocalGlSetOfBook)sobIter.next();

					       			// post to subsequent acvs of subsequent set of book(propagate)
					       			
					       			Collection glAccountingCalendarValues = 
					       				glAccountingCalendarValueHome.findByAcCode(glSubsequentSetOfBook.getGlAccountingCalendar().getAcCode(), AD_CMPNY);     
					       			
					       			Iterator acvIter = glAccountingCalendarValues.iterator();
					       			
					       			while (acvIter.hasNext()) {
					       				
					       				LocalGlAccountingCalendarValue glSubsequentAccountingCalendarValue = 
					       					(LocalGlAccountingCalendarValue)acvIter.next();
					       				
					       				this.postToGlInvestor(glSubsequentAccountingCalendarValue,
					       						apCheck.getApSupplier(),
								       			false, (byte)1, apCheck.getChkAmount(), AD_CMPNY);

					       			}
					       			
					       			if (glSubsequentSetOfBook.getSobYearEndClosed() == 0) break;
					       			
					       		}
					       		
					       	}
			    	  
			    	  
			    	  
			      }
        		
        	}
        	
        } catch (GlJREffectiveDateNoPeriodExistException ex) {
        	
        	getSessionContext().setRollbackOnly();
        	throw ex;
        	
        } catch (GlJREffectiveDatePeriodClosedException ex) {
        	
        	getSessionContext().setRollbackOnly();
        	throw ex;
        	
        } catch (GlobalJournalNotBalanceException ex) {
        	
        	getSessionContext().setRollbackOnly();
        	throw ex;        	
        	        
        } catch (GlobalRecordAlreadyDeletedException ex) {
        	
        	getSessionContext().setRollbackOnly();
        	throw ex;        	        	
        	
        } catch (GlobalTransactionAlreadyPostedException ex) {
        	
        	getSessionContext().setRollbackOnly();
        	throw ex;
        	
        } catch (GlobalTransactionAlreadyVoidPostedException ex) {
        	
        	getSessionContext().setRollbackOnly();
        	throw ex;

        } catch (AdPRFCoaGlVarianceAccountNotFoundException ex) {
        	
        	getSessionContext().setRollbackOnly();
        	throw ex;

    	} catch (Exception ex) {
    		
    		Debug.printStackTrace(ex);
    		getSessionContext().setRollbackOnly();
    		throw new EJBException(ex.getMessage());
    		
    	}
    	
    }
    
    
    
    private void postToGlInvestor(LocalGlAccountingCalendarValue glAccountingCalendarValue, 
		      LocalApSupplier apSupplier, 
		      boolean isCurrentAcv, byte isDebit, double JL_AMNT, Integer AD_CMPNY) {
		      	
		      Debug.print("ApDirectCheckEntryControllerBean postToGlInvestor");
		      
		      LocalGlChartOfAccountBalanceHome glChartOfAccountBalanceHome = null;
		      LocalAdCompanyHome adCompanyHome = null;
		      
		      LocalGlInvestorAccountBalanceHome glInvestorAccountBalanceHome = null;
		      
		     
		       // Initialize EJB Home
		        
		       try {
		            
		           glChartOfAccountBalanceHome = (LocalGlChartOfAccountBalanceHome)EJBHomeFactory.
		              lookUpLocalHome(LocalGlChartOfAccountBalanceHome.JNDI_NAME, LocalGlChartOfAccountBalanceHome.class);                   
		           adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
		              lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);  
		           glInvestorAccountBalanceHome = (LocalGlInvestorAccountBalanceHome)EJBHomeFactory.
				              lookUpLocalHome(LocalGlInvestorAccountBalanceHome.JNDI_NAME, LocalGlInvestorAccountBalanceHome.class);  
				           
		           
		              
		            
		       } catch (NamingException ex) {
		            
		           throw new EJBException(ex.getMessage());
		            
		       }
		       
		       try {          
		               
		               LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);                       
		       	   
		               
			       	   LocalGlInvestorAccountBalance glInvestorAccountBalance =
			       			glInvestorAccountBalanceHome.findByAcvCodeAndSplCode(
			       					glAccountingCalendarValue.getAcvCode(),
					               	  apSupplier.getSplCode(), AD_CMPNY);
			               	  
			           
			           short FC_EXTNDD_PRCSN = adCompany.getGlFunctionalCurrency().getFcPrecision();
			           
			           
			           glInvestorAccountBalance.setIrabEndingBalance(
						       EJBCommon.roundIt(glInvestorAccountBalance.getIrabEndingBalance() - JL_AMNT, FC_EXTNDD_PRCSN)); 					   
						     
		 			        
					    if (!isCurrentAcv) {
					    	
					    	glInvestorAccountBalance.setIrabBeginningBalance(
					       		EJBCommon.roundIt(glInvestorAccountBalance.getIrabBeginningBalance() - JL_AMNT, FC_EXTNDD_PRCSN)); 					   
					    	
						}
				 	  
				 	  if (isCurrentAcv) { 
				 	 
				 		 glInvestorAccountBalance.setIrabTotalDebit(
					 				EJBCommon.roundIt(glInvestorAccountBalance.getIrabTotalDebit() + JL_AMNT, FC_EXTNDD_PRCSN));      	   
				 		 
				 	}
		       	
		       } catch (Exception ex) {
		       	
		       	   Debug.printStackTrace(ex);
		       	   throw new EJBException(ex.getMessage());
		       	
		       }

		   }
    
    private void post(Date CHK_DT, double CHK_AMNT, LocalApSupplier apSupplier, Integer AD_CMPNY) {
    	
       Debug.print("ApCheckBatchSubmitControllerBean post");
    	
       LocalApSupplierBalanceHome apSupplierBalanceHome = null;
        
       // Initialize EJB Home
        
       try {
                        
           apSupplierBalanceHome = (LocalApSupplierBalanceHome)EJBHomeFactory.
               lookUpLocalHome(LocalApSupplierBalanceHome.JNDI_NAME, LocalApSupplierBalanceHome.class);              
            
       } catch (NamingException ex) {
        	
           getSessionContext().setRollbackOnly();            
           throw new EJBException(ex.getMessage());
            
       }
        
       try {
        		        	
	       // find supplier balance before or equal voucher date
	    	
	       Collection apSupplierBalances = apSupplierBalanceHome.findByBeforeOrEqualVouDateAndSplSupplierCode(CHK_DT, apSupplier.getSplSupplierCode(), AD_CMPNY);
	    	
	       if (!apSupplierBalances.isEmpty()) {
	    		
	    	   // get last voucher
	    	    
	    	   ArrayList apSupplierBalanceList = new ArrayList(apSupplierBalances);
	    	    	    	    
	    	   LocalApSupplierBalance apSupplierBalance = (LocalApSupplierBalance)apSupplierBalanceList.get(apSupplierBalanceList.size() - 1);
	    	    
	    	   if (apSupplierBalance.getSbDate().before(CHK_DT)) {
	    	    		    	    	
	    	       // create new balance
	    	    	
	    	       LocalApSupplierBalance apNewSupplierBalance = apSupplierBalanceHome.create(
		    	       CHK_DT, apSupplierBalance.getSbBalance() + CHK_AMNT, AD_CMPNY);

		           //apSupplier.addApSupplierBalance(apNewSupplierBalance);
		           apNewSupplierBalance.setApSupplier(apSupplier);
		           		        	
	    	   } else { // equals to voucher date
	    	   
	    	       apSupplierBalance.setSbBalance(apSupplierBalance.getSbBalance() + CHK_AMNT);
	    	   
	    	   } 
	    	    
	    	} else {        	
	    	
	    	    // create new balance
	    	    
		    	LocalApSupplierBalance apNewSupplierBalance = apSupplierBalanceHome.create(
		    		CHK_DT, CHK_AMNT, AD_CMPNY);

		        //apSupplier.addApSupplierBalance(apNewSupplierBalance);
		    	apNewSupplierBalance.setApSupplier(apSupplier);
		        		        		        
	     	}
	     	
	     	// propagate to subsequent balances if necessary
	     	
	     	apSupplierBalances = apSupplierBalanceHome.findByAfterVouDateAndSplSupplierCode(CHK_DT, apSupplier.getSplSupplierCode(), AD_CMPNY);
	     	
	     	Iterator i = apSupplierBalances.iterator();
	     	
	     	while (i.hasNext()) {
	     		
	     		LocalApSupplierBalance apSupplierBalance = (LocalApSupplierBalance)i.next();
	     		
	     		apSupplierBalance.setSbBalance(apSupplierBalance.getSbBalance() + CHK_AMNT);
	     		
	     	}
	     		     	        	
        } catch (Exception ex) {
        	
        	Debug.printStackTrace(ex);
        	getSessionContext().setRollbackOnly();            
            throw new EJBException(ex.getMessage());
            
        }
    	
	}
   
   private void postToGl(LocalGlAccountingCalendarValue glAccountingCalendarValue, 
      LocalGlChartOfAccount glChartOfAccount, 
      boolean isCurrentAcv, byte isDebit, double JL_AMNT, Integer AD_CMPNY) {
      	
      Debug.print("ApCheckBatchSubmitControllerBean postToGl");
      
      LocalGlChartOfAccountBalanceHome glChartOfAccountBalanceHome = null;
      LocalAdCompanyHome adCompanyHome = null;
      
     
       // Initialize EJB Home
        
       try {
            
           glChartOfAccountBalanceHome = (LocalGlChartOfAccountBalanceHome)EJBHomeFactory.
              lookUpLocalHome(LocalGlChartOfAccountBalanceHome.JNDI_NAME, LocalGlChartOfAccountBalanceHome.class);                   
           adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
              lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);                   
              
            
       } catch (NamingException ex) {
            
           throw new EJBException(ex.getMessage());
            
       }
       
       try {          
               
               LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);                       
       	   
	       	   LocalGlChartOfAccountBalance glChartOfAccountBalance = 
		            glChartOfAccountBalanceHome.findByAcvCodeAndCoaCode(
		               	  glAccountingCalendarValue.getAcvCode(),
		               	  glChartOfAccount.getCoaCode(), AD_CMPNY);
	               	  
	           String ACCOUNT_TYPE = glChartOfAccount.getCoaAccountType();
	           short FC_EXTNDD_PRCSN = adCompany.getGlFunctionalCurrency().getFcPrecision();
	           
	           
	               	  
	           if (((ACCOUNT_TYPE.equals("ASSET") || ACCOUNT_TYPE.equals("EXPENSE")) &&
				    isDebit == EJBCommon.TRUE) ||
				    (!ACCOUNT_TYPE.equals("ASSET") && !ACCOUNT_TYPE.equals("EXPENSE") &&
				    isDebit == EJBCommon.FALSE)) {				    
				    			
 			        glChartOfAccountBalance.setCoabEndingBalance(
				       EJBCommon.roundIt(glChartOfAccountBalance.getCoabEndingBalance() + JL_AMNT, FC_EXTNDD_PRCSN)); 					   
				       
				    if (!isCurrentAcv) {
				    	
				    	glChartOfAccountBalance.setCoabBeginningBalance(
				       		EJBCommon.roundIt(glChartOfAccountBalance.getCoabBeginningBalance() + JL_AMNT, FC_EXTNDD_PRCSN)); 					   
				    	
					}
				    
		
			  } else {
	
				    glChartOfAccountBalance.setCoabEndingBalance(
				       EJBCommon.roundIt(glChartOfAccountBalance.getCoabEndingBalance() - JL_AMNT, FC_EXTNDD_PRCSN));
				       
				    if (!isCurrentAcv) {
				    	
				    	glChartOfAccountBalance.setCoabBeginningBalance(
				       		EJBCommon.roundIt(glChartOfAccountBalance.getCoabBeginningBalance() - JL_AMNT, FC_EXTNDD_PRCSN)); 					   
				    	
					}
	
		 	  }
		 	  
		 	  if (isCurrentAcv) { 
		 	 
			 	 if (isDebit == EJBCommon.TRUE) {
			 	 	
		 			glChartOfAccountBalance.setCoabTotalDebit(
		 				EJBCommon.roundIt(glChartOfAccountBalance.getCoabTotalDebit() + JL_AMNT, FC_EXTNDD_PRCSN));	 			
		 				
		 		 } else {
		 		 	
		 			glChartOfAccountBalance.setCoabTotalCredit(
		 				EJBCommon.roundIt(glChartOfAccountBalance.getCoabTotalCredit() + JL_AMNT, FC_EXTNDD_PRCSN));	 			
		 		 }       	   
		 		 
		 	}
       	
       } catch (Exception ex) {
       	
       	   Debug.printStackTrace(ex);
       	   throw new EJBException(ex.getMessage());
       	
       }
      
      
   }
   
   private void postToInv(LocalApVoucherLineItem apVoucherLineItem, Date CST_DT, double CST_QTY_RCVD, double CST_ITM_CST,
   		double CST_RMNNG_QTY, double CST_RMNNG_VL, double CST_VRNC_VL, String USR_NM, Integer AD_BRNCH, Integer AD_CMPNY) throws 
   		AdPRFCoaGlVarianceAccountNotFoundException {
    
    	Debug.print("ApCheckBatchSubmitControllerBean postToInv");
    	
    	LocalInvCostingHome invCostingHome = null;
    	LocalAdPreferenceHome adPreferenceHome = null;
    	LocalAdCompanyHome adCompanyHome = null;
    	LocalInvAdjustmentLineHome invAdjustmentLineHome = null;

        // Initialize EJB Home
        
        try {
            
          invCostingHome = (LocalInvCostingHome)EJBHomeFactory.
              lookUpLocalHome(LocalInvCostingHome.JNDI_NAME, LocalInvCostingHome.class);
          adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
              lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);
          adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
              lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);              
          invAdjustmentLineHome = (LocalInvAdjustmentLineHome)EJBHomeFactory.
      	      lookUpLocalHome(LocalInvAdjustmentLineHome.JNDI_NAME, LocalInvAdjustmentLineHome.class);              

        } catch (NamingException ex) {
            
          throw new EJBException(ex.getMessage());
            
        }
      
        try {
        
           LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);
           LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
           LocalInvItemLocation invItemLocation = apVoucherLineItem.getInvItemLocation();
           int CST_LN_NMBR = 0;
           
           CST_QTY_RCVD = EJBCommon.roundIt(CST_QTY_RCVD, adPreference.getPrfInvQuantityPrecisionUnit());
           CST_ITM_CST = EJBCommon.roundIt(CST_ITM_CST, adCompany.getGlFunctionalCurrency().getFcPrecision());
           CST_RMNNG_QTY = EJBCommon.roundIt(CST_RMNNG_QTY, adPreference.getPrfInvQuantityPrecisionUnit());
           CST_RMNNG_VL = EJBCommon.roundIt(CST_RMNNG_VL, adCompany.getGlFunctionalCurrency().getFcPrecision());
           
           try {
           
           	   // generate line number
               LocalInvCosting invCurrentCosting = invCostingHome.getByMaxCstLineNumberAndCstDateToLongAndIiNameAndLocName(CST_DT.getTime(), invItemLocation.getInvItem().getIiName(), invItemLocation.getInvLocation().getLocName(), AD_BRNCH, AD_CMPNY);
               CST_LN_NMBR = invCurrentCosting.getCstLineNumber() + 1;
           
           } catch (FinderException ex) {
           
           	   CST_LN_NMBR = 1;
           
           }

           //void subsequent cost variance adjustments
           Collection invAdjustmentLines = invAdjustmentLineHome.findUnvoidAndIsCostVarianceGreaterThanAdjDateAndIlCodeAndBrCode(
           		CST_DT, invItemLocation.getIlCode(), AD_BRNCH, AD_CMPNY);
           Iterator i = invAdjustmentLines.iterator();
           
           while (i.hasNext()){
           	
           	LocalInvAdjustmentLine invAdjustmentLine = (LocalInvAdjustmentLine) i.next();
           	this.voidInvAdjustment(invAdjustmentLine.getInvAdjustment(), AD_BRNCH, AD_CMPNY);
           	
           }
           
           // create costing
           LocalInvCosting invCosting = invCostingHome.create(CST_DT, CST_DT.getTime(), CST_LN_NMBR, CST_QTY_RCVD, CST_ITM_CST, 0d, 0d, 0d, 0d, 0d, 0d, CST_RMNNG_QTY, CST_RMNNG_VL, 0d, 0d, CST_QTY_RCVD > 0 ? CST_QTY_RCVD : 0, AD_BRNCH, AD_CMPNY);
           //invItemLocation.addInvCosting(invCosting);
           invCosting.setInvItemLocation(invItemLocation);
           invCosting.setApVoucherLineItem(apVoucherLineItem);
           
			// if cost variance is not 0, generate cost variance for the transaction 
			if(CST_VRNC_VL != 0) {
				
				this.generateCostVariance(invCosting.getInvItemLocation(), CST_VRNC_VL,
						"APCHK" + apVoucherLineItem.getApCheck().getChkDocumentNumber(),
						apVoucherLineItem.getApCheck().getChkDescription(),
						apVoucherLineItem.getApCheck().getChkDate(), USR_NM, AD_BRNCH, AD_CMPNY);
				
			}

           // propagate balance if necessary           
           Collection invCostings = invCostingHome.findByGreaterThanCstDateAndIiNameAndLocName(CST_DT, invItemLocation.getInvItem().getIiName(), invItemLocation.getInvLocation().getLocName(), AD_BRNCH, AD_CMPNY);
           
           i = invCostings.iterator();
           
           while (i.hasNext()) {
           
               LocalInvCosting invPropagatedCosting = (LocalInvCosting)i.next();
               
               invPropagatedCosting.setCstRemainingQuantity(invPropagatedCosting.getCstRemainingQuantity() + CST_QTY_RCVD);
               invPropagatedCosting.setCstRemainingValue(invPropagatedCosting.getCstRemainingValue() + CST_ITM_CST);
           
           }                           

			// regenerate cost variance
           this.regenerateCostVariance(invCostings, invCosting, AD_BRNCH, AD_CMPNY);

        } catch (AdPRFCoaGlVarianceAccountNotFoundException ex){
        	
        	throw ex;

        } catch (Exception ex) {
      	
      	   Debug.printStackTrace(ex);
      	   getSessionContext().setRollbackOnly();
           throw new EJBException(ex.getMessage());
         
        }
        	    
    }
    
    private double convertByUomFromAndItemAndQuantity(LocalInvUnitOfMeasure invFromUnitOfMeasure, LocalInvItem invItem, double QTY_RCVD, Integer AD_CMPNY) {
			
		Debug.print("ApCheckBatchSubmitControllerBean convertByUomFromAndItemAndQuantity");		        
        
		LocalAdPreferenceHome adPreferenceHome = null;
		LocalInvUnitOfMeasureConversionHome invUnitOfMeasureConversionHome = null;
                
        // Initialize EJB Home
        
        try {
            
            adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
                 lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);   
            invUnitOfMeasureConversionHome = (LocalInvUnitOfMeasureConversionHome)EJBHomeFactory.
        		lookUpLocalHome(LocalInvUnitOfMeasureConversionHome.JNDI_NAME, LocalInvUnitOfMeasureConversionHome.class);
            
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
                
        try {
        
            LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);               	          	        	
        	        	
            LocalInvUnitOfMeasureConversion invUnitOfMeasureConversion = invUnitOfMeasureConversionHome.findUmcByIiNameAndUomName(invItem.getIiName(), invFromUnitOfMeasure.getUomName(), AD_CMPNY);
            LocalInvUnitOfMeasureConversion invDefaultUomConversion = invUnitOfMeasureConversionHome.findUmcByIiNameAndUomName(invItem.getIiName(), invItem.getInvUnitOfMeasure().getUomName(), AD_CMPNY);
                        
        	return EJBCommon.roundIt(QTY_RCVD * invDefaultUomConversion.getUmcConversionFactor() / invUnitOfMeasureConversion.getUmcConversionFactor(), adPreference.getPrfInvQuantityPrecisionUnit());       	        		        		       	
        						    		        		
        } catch (Exception ex) {
        	
        	Debug.printStackTrace(ex);
        	getSessionContext().setRollbackOnly();
        	throw new EJBException(ex.getMessage());
        	
        }
		
	}

    private double getFrRateByFrNameAndFrDate(String FC_NM, Date CONVERSION_DATE, Integer AD_CMPNY) 
    throws GlobalConversionDateNotExistException {
    	
 		Debug.print("ApCheckBatchSubmitControllerBean getFrRateByFrNameAndFrDate");
 		
 		LocalGlFunctionalCurrencyRateHome glFunctionalCurrencyRateHome = null; 
 		LocalGlFunctionalCurrencyHome glFunctionalCurrencyHome = null;
 		LocalAdCompanyHome adCompanyHome = null;
 		
 		// Initialize EJB Home
 		
 		try {
 			
 			glFunctionalCurrencyRateHome = (LocalGlFunctionalCurrencyRateHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlFunctionalCurrencyRateHome.JNDI_NAME, LocalGlFunctionalCurrencyRateHome.class);
 			glFunctionalCurrencyHome = (LocalGlFunctionalCurrencyHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlFunctionalCurrencyHome.JNDI_NAME, LocalGlFunctionalCurrencyHome.class);
 			adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
 			
 		} catch (NamingException ex) {
 			
 			throw new EJBException(ex.getMessage());
 			
 		}
 		
 		try {
 			
 			LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
 			LocalGlFunctionalCurrency glFunctionalCurrency = glFunctionalCurrencyHome.findByFcName(FC_NM, AD_CMPNY);
 			
 			double CONVERSION_RATE = 1;
 			
 			// Get functional currency rate
 			
 			if (!FC_NM.equals("USD")) {
 				
 				LocalGlFunctionalCurrencyRate glFunctionalCurrencyRate =
 					glFunctionalCurrencyRateHome.findByFcCodeAndDate(glFunctionalCurrency.getFcCode(),
 							CONVERSION_DATE, AD_CMPNY);
 				
 				CONVERSION_RATE = glFunctionalCurrencyRate.getFrXToUsd();
 				
 			}
 			
 			// Get set of book functional currency rate if necessary
 			
 			if (!adCompany.getGlFunctionalCurrency().getFcName().equals("USD")) {
 				
 				LocalGlFunctionalCurrencyRate glCompanyFunctionalCurrencyRate =
 					glFunctionalCurrencyRateHome.findByFcCodeAndDate(adCompany.getGlFunctionalCurrency(). getFcCode(),
 							CONVERSION_DATE, AD_CMPNY);
 				
 				CONVERSION_RATE = CONVERSION_RATE / glCompanyFunctionalCurrencyRate.getFrXToUsd();
 				
 			}
 			
 			return CONVERSION_RATE;
 			
 		} catch (FinderException ex) {	
 			
 			getSessionContext().setRollbackOnly();
 			throw new GlobalConversionDateNotExistException();  
 			
 		} catch (Exception ex) {
 			
 			Debug.printStackTrace(ex);
 			throw new EJBException(ex.getMessage());
 			
 		}
 		
 	}

    private void voidInvAdjustment(LocalInvAdjustment invAdjustment, Integer AD_BRNCH, Integer AD_CMPNY){
    	
    	Debug.print("ApCheckBatchSubmitController voidInvAdjustment");
    	
    	try{

    			Collection invDistributionRecords = invAdjustment.getInvDistributionRecords();
    			ArrayList list = new ArrayList();
    			
    			Iterator i = invDistributionRecords.iterator();
    			
    			while (i.hasNext()) {
    				
    				LocalInvDistributionRecord invDistributionRecord = (LocalInvDistributionRecord)i.next();
    				
    				list.add(invDistributionRecord);
    				
    			}
    			
    			i = list.iterator();
    			
    			while (i.hasNext()) {
    				
    				LocalInvDistributionRecord invDistributionRecord = (LocalInvDistributionRecord)i.next();    	    			    			
    				
    				this.addInvDrEntry(invAdjustment.getInvDrNextLine(), invDistributionRecord.getDrClass(), 
    						invDistributionRecord.getDrDebit() == EJBCommon.TRUE ? EJBCommon.FALSE : EJBCommon.TRUE,
    								invDistributionRecord.getDrAmount(), EJBCommon.TRUE,
									invDistributionRecord.getInvChartOfAccount().getCoaCode(), invAdjustment, AD_BRNCH, AD_CMPNY);
    				
    			}	    
    			
    			Collection invAjustmentLines = invAdjustment.getInvAdjustmentLines();
    			i = invAjustmentLines.iterator();
    			list.clear();

    			while (i.hasNext()) {
    				
    				LocalInvAdjustmentLine invAdjustmentLine = (LocalInvAdjustmentLine)i.next();
    				
    				list.add(invAdjustmentLine);
    				
    			}
    			
    			i = list.iterator();
    			
    			while (i.hasNext()) {
    				
    				LocalInvAdjustmentLine invAdjustmentLine = (LocalInvAdjustmentLine)i.next();    	    			    			
    				
    				this.addInvAlEntry(invAdjustmentLine.getInvItemLocation(),
    						invAdjustment, (invAdjustmentLine.getAlUnitCost()) * - 1, EJBCommon.TRUE, AD_CMPNY);

    			}
    			
    			invAdjustment.setAdjVoid(EJBCommon.TRUE);
    			
    			this.executeInvAdjPost(invAdjustment.getAdjCode(), invAdjustment.getAdjLastModifiedBy(), AD_BRNCH, AD_CMPNY);

    	} catch (Exception ex) {
    		
    		Debug.printStackTrace(ex);
    		getSessionContext().setRollbackOnly();
    		throw new EJBException(ex.getMessage());
    		
    	}
    	
    }

    private void generateCostVariance(LocalInvItemLocation invItemLocation, double CST_VRNC_VL, String ADJ_RFRNC_NMBR,
    		String ADJ_DSCRPTN, Date ADJ_DT, String USR_NM, Integer AD_BRNCH, Integer AD_CMPNY) throws
			AdPRFCoaGlVarianceAccountNotFoundException {
    	
    	Debug.print("ApCheckBatchSubmitController generateCostVariance");
    	/*
    	LocalAdPreferenceHome adPreferenceHome = null;
    	LocalGlChartOfAccountHome glChartOfAccountHome = null;
    	LocalAdBranchItemLocationHome adBranchItemLocationHome = null;
    	
    	// Initialize EJB Home
    	
    	try {
    		
    		adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);       
    		glChartOfAccountHome = (LocalGlChartOfAccountHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlChartOfAccountHome.JNDI_NAME, LocalGlChartOfAccountHome.class);
    		adBranchItemLocationHome = (LocalAdBranchItemLocationHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdBranchItemLocationHome.JNDI_NAME, LocalAdBranchItemLocationHome.class);       
    		
    		
    	} catch (NamingException ex) {
    		
    		throw new EJBException(ex.getMessage());
    		
    	}
    	
    	try{ 

    		LocalInvAdjustment newInvAdjustment = this.saveInvAdjustment(ADJ_RFRNC_NMBR, ADJ_DSCRPTN, ADJ_DT, USR_NM, AD_BRNCH,
    				AD_CMPNY);
    		LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);
    		LocalGlChartOfAccount glCoaVarianceAccount = null;

    		if(adPreference.getPrfInvGlCoaVarianceAccount() == null)
    			throw new AdPRFCoaGlVarianceAccountNotFoundException();
    		
    		try{
    			
    			glCoaVarianceAccount = glChartOfAccountHome.findByPrimaryKey(adPreference.getPrfInvGlCoaVarianceAccount());
    			//glCoaVarianceAccount.addInvAdjustment(newInvAdjustment);
    			newInvAdjustment.setGlChartOfAccount(glCoaVarianceAccount);
    			
    		} catch (FinderException ex) {
    			
    			throw new AdPRFCoaGlVarianceAccountNotFoundException();
    			
    		}
    		
    		LocalInvAdjustmentLine invAdjustmentLine = this.addInvAlEntry(invItemLocation, newInvAdjustment, CST_VRNC_VL, 
    				EJBCommon.FALSE, AD_CMPNY);
    		
    		// check for branch mapping
    		
    		LocalAdBranchItemLocation adBranchItemLocation = null;
    		
    		try{
    			
    			adBranchItemLocation = adBranchItemLocationHome.findBilByIlCodeAndBrCode(
    					invAdjustmentLine.getInvItemLocation().getIlCode(), AD_BRNCH, AD_CMPNY);
    			
    		} catch (FinderException ex) {
    			
    		}
    		
    		LocalGlChartOfAccount glInventoryChartOfAccount = null;
    		
    		if (adBranchItemLocation == null) {
    			
    			glInventoryChartOfAccount = glChartOfAccountHome.findByPrimaryKey(
    					invAdjustmentLine.getInvItemLocation().getIlGlCoaInventoryAccount());
    		} else {
    			
    			glInventoryChartOfAccount = glChartOfAccountHome.findByPrimaryKey(
    					adBranchItemLocation.getBilCoaGlInventoryAccount());
    			
    		}
    		
    		
    		boolean isDebit = CST_VRNC_VL < 0 ? false : true;
    		
    		//inventory dr
    		this.addInvDrEntry(newInvAdjustment.getInvDrNextLine(), "INVENTORY",
    				isDebit == true ? EJBCommon.TRUE : EJBCommon.FALSE, Math.abs(CST_VRNC_VL), EJBCommon.FALSE,
    						glInventoryChartOfAccount.getCoaCode(), newInvAdjustment, AD_BRNCH, AD_CMPNY);
    		
    		//variance dr
    		this.addInvDrEntry(newInvAdjustment.getInvDrNextLine(),"VARIANCE", 
    				!isDebit == true ? EJBCommon.TRUE : EJBCommon.FALSE, Math.abs(CST_VRNC_VL), EJBCommon.FALSE,
    						glCoaVarianceAccount.getCoaCode(), newInvAdjustment, AD_BRNCH, AD_CMPNY);
    		
    		this.executeInvAdjPost(newInvAdjustment.getAdjCode(), newInvAdjustment.getAdjLastModifiedBy(), AD_BRNCH,
    				AD_CMPNY);
    		
    	} catch (AdPRFCoaGlVarianceAccountNotFoundException ex) {
    		
    		getSessionContext().setRollbackOnly();
    		throw ex;

    	} catch (Exception ex) {
    		
    		Debug.printStackTrace(ex);
    		getSessionContext().setRollbackOnly();
    		throw new EJBException(ex.getMessage());
    		
    	}
    	*/
    }
    
    private void regenerateCostVariance(Collection invCostings, LocalInvCosting invCosting, Integer AD_BRNCH, Integer AD_CMPNY)
    throws AdPRFCoaGlVarianceAccountNotFoundException {
    	
    	Debug.print("ApCheckBatchSubmitController regenerateCostVariance");
    	/*
    	try {
    		
    		Iterator i = invCostings.iterator();
    		LocalInvCosting prevInvCosting = invCosting;
    		
    		while (i.hasNext()) {
    			
    			LocalInvCosting invPropagatedCosting = (LocalInvCosting)i.next();
    			
    			if(prevInvCosting.getCstRemainingQuantity() < 0) {
    				
    				double TTL_CST = 0;
    				double QNTY = 0;
    				String ADJ_RFRNC_NMBR = "";
    				String ADJ_DSCRPTN = "";
    				String ADJ_CRTD_BY = "";

    				// get unit cost adjusment, document number and unit of measure
    				if (invPropagatedCosting.getApPurchaseOrderLine() != null) {

    					TTL_CST = invPropagatedCosting.getApPurchaseOrderLine().getPlAmount();
						QNTY =  this.convertByUomFromAndItemAndQuantity(
								invPropagatedCosting.getApPurchaseOrderLine().getInvUnitOfMeasure(), 
								invPropagatedCosting.getApPurchaseOrderLine().getInvItemLocation().getInvItem(),
								invPropagatedCosting.getApPurchaseOrderLine().getPlQuantity(), AD_CMPNY);
    					
    					ADJ_DSCRPTN = invPropagatedCosting.getApPurchaseOrderLine().getApPurchaseOrder().getPoDescription();
    					ADJ_CRTD_BY = invPropagatedCosting.getApPurchaseOrderLine().getApPurchaseOrder().getPoPostedBy();
    					ADJ_RFRNC_NMBR = "APRI" +
						invPropagatedCosting.getApPurchaseOrderLine().getApPurchaseOrder().getPoDocumentNumber();
    					
    				} else if (invPropagatedCosting.getApVoucherLineItem() != null){

    					TTL_CST = invPropagatedCosting.getApVoucherLineItem().getVliAmount();
						QNTY = this.convertByUomFromAndItemAndQuantity(
								invPropagatedCosting.getApVoucherLineItem().getInvUnitOfMeasure(), 
								invPropagatedCosting.getApVoucherLineItem().getInvItemLocation().getInvItem(),
								invPropagatedCosting.getApVoucherLineItem().getVliQuantity(), AD_CMPNY);
    					
    					if (invPropagatedCosting.getApVoucherLineItem().getApVoucher() != null) {
    						
    						ADJ_DSCRPTN = invPropagatedCosting.getApVoucherLineItem().getApVoucher().getVouDescription();
    						ADJ_CRTD_BY = invPropagatedCosting.getApVoucherLineItem().getApVoucher().getVouPostedBy();
    						ADJ_RFRNC_NMBR = "APVOU" +
							invPropagatedCosting.getApVoucherLineItem().getApVoucher().getVouDocumentNumber();
    						
    					} else if (invPropagatedCosting.getApVoucherLineItem().getApCheck() != null) {
    						
    						ADJ_DSCRPTN = invPropagatedCosting.getApVoucherLineItem().getApCheck().getChkDescription();
    						ADJ_CRTD_BY = invPropagatedCosting.getApVoucherLineItem().getApCheck().getChkPostedBy();
    						ADJ_RFRNC_NMBR = "APCHK" +
							invPropagatedCosting.getApVoucherLineItem().getApCheck().getChkDocumentNumber();
    						
    					}
    					
    				} else if (invPropagatedCosting.getArInvoiceLineItem() != null){

    					QNTY = this.convertByUomFromAndItemAndQuantity(
								invPropagatedCosting.getArInvoiceLineItem().getInvUnitOfMeasure(), 
								invPropagatedCosting.getArInvoiceLineItem().getInvItemLocation().getInvItem(),
								invPropagatedCosting.getArInvoiceLineItem().getIliQuantity(), AD_CMPNY);
   						TTL_CST = prevInvCosting.getCstRemainingValue() - invPropagatedCosting.getCstRemainingValue();
   						
   						if(invPropagatedCosting.getArInvoiceLineItem().getArInvoice() != null){
   	   						
   	   							ADJ_DSCRPTN = invPropagatedCosting.getArInvoiceLineItem().getArInvoice().getInvDescription();
   	   							ADJ_CRTD_BY = invPropagatedCosting.getArInvoiceLineItem().getArInvoice().getInvPostedBy();
   	   							ADJ_RFRNC_NMBR = "ARCM" + 
   								invPropagatedCosting.getArInvoiceLineItem().getArInvoice().getInvNumber();
   	    					
   	   						} else if(invPropagatedCosting.getArInvoiceLineItem().getArReceipt() != null){

   	   							ADJ_DSCRPTN = invPropagatedCosting.getArInvoiceLineItem().getArReceipt().getRctDescription();
   	   							ADJ_CRTD_BY = invPropagatedCosting.getArInvoiceLineItem().getArReceipt().getRctPostedBy();
   	   							ADJ_RFRNC_NMBR = "ARMR" + 
   								invPropagatedCosting.getArInvoiceLineItem().getArReceipt().getRctNumber();

   	   						}
   	   						
    				} else if (invPropagatedCosting.getArSalesOrderInvoiceLine() != null){

   						TTL_CST = prevInvCosting.getCstRemainingValue() - invPropagatedCosting.getCstRemainingValue();
    					QNTY = this.convertByUomFromAndItemAndQuantity(
								invPropagatedCosting.getArSalesOrderInvoiceLine().getArSalesOrderLine().getInvUnitOfMeasure(), 
								invPropagatedCosting.getArSalesOrderInvoiceLine().getArSalesOrderLine().getInvItemLocation().getInvItem(),
								invPropagatedCosting.getArSalesOrderInvoiceLine().getSilQuantityDelivered(), AD_CMPNY);

    					ADJ_DSCRPTN = invPropagatedCosting.getArSalesOrderInvoiceLine().getArInvoice().getInvDescription();
    					ADJ_CRTD_BY = invPropagatedCosting.getArSalesOrderInvoiceLine().getArInvoice().getInvPostedBy();
    					ADJ_RFRNC_NMBR = "ARCM" + 
						invPropagatedCosting.getArSalesOrderInvoiceLine().getArInvoice().getInvNumber();
    					
    				} else if (invPropagatedCosting.getInvAdjustmentLine() != null){

    					ADJ_DSCRPTN = invPropagatedCosting.getInvAdjustmentLine().getInvAdjustment().getAdjDescription();
    					ADJ_CRTD_BY = invPropagatedCosting.getInvAdjustmentLine().getInvAdjustment().getAdjPostedBy();
    					ADJ_RFRNC_NMBR = "INVADJ" +
						invPropagatedCosting.getInvAdjustmentLine().getInvAdjustment().getAdjDocumentNumber();
    					
    					if(invPropagatedCosting.getInvAdjustmentLine().getAlAdjustQuantity() != 0) {
    						
    						TTL_CST = (invPropagatedCosting.getInvAdjustmentLine().getAlUnitCost() * 
    								invPropagatedCosting.getInvAdjustmentLine().getAlAdjustQuantity());
    						QNTY =  this.convertByUomFromAndItemAndQuantity(
    								invPropagatedCosting.getInvAdjustmentLine().getInvUnitOfMeasure(), 
									invPropagatedCosting.getInvAdjustmentLine().getInvItemLocation().getInvItem(),
									invPropagatedCosting.getInvAdjustmentLine().getAlAdjustQuantity(), AD_CMPNY);
    						
    					}

    				} else if (invPropagatedCosting.getInvAssemblyTransferLine() != null){

    					QNTY = invPropagatedCosting.getInvAssemblyTransferLine().getAtlAssembleQuantity();
    					TTL_CST = invPropagatedCosting.getInvAssemblyTransferLine().getAtlAssembleCost();

    					ADJ_DSCRPTN = invPropagatedCosting.getInvAssemblyTransferLine().getInvAssemblyTransfer().getAtrDescription();
    					ADJ_CRTD_BY = invPropagatedCosting.getInvAssemblyTransferLine().getInvAssemblyTransfer().getAtrPostedBy();
    					ADJ_RFRNC_NMBR = "INVAT" +
						invPropagatedCosting.getInvAssemblyTransferLine().getInvAssemblyTransfer().getAtrDocumentNumber();
    					
    				} else if (invPropagatedCosting.getInvBranchStockTransferLine() != null){

    					if(invPropagatedCosting.getInvBranchStockTransferLine().getInvBranchStockTransfer().getBstTransferOutNumber()
    							!= null) {

    						TTL_CST = invPropagatedCosting.getInvBranchStockTransferLine().getBslAmount();
							QNTY =  this.convertByUomFromAndItemAndQuantity(
									invPropagatedCosting.getInvBranchStockTransferLine().getInvUnitOfMeasure(), 
									invPropagatedCosting.getInvBranchStockTransferLine().getInvItemLocation().getInvItem(),
									invPropagatedCosting.getInvBranchStockTransferLine().getBslQuantityReceived(), AD_CMPNY);
							
    					} else {

    						TTL_CST = invPropagatedCosting.getInvBranchStockTransferLine().getBslAmount();
							QNTY =  this.convertByUomFromAndItemAndQuantity(
									invPropagatedCosting.getInvBranchStockTransferLine().getInvUnitOfMeasure(), 
									invPropagatedCosting.getInvBranchStockTransferLine().getInvItemLocation().getInvItem(),
									invPropagatedCosting.getInvBranchStockTransferLine().getBslQuantity(), AD_CMPNY);
    					}
    					
    					ADJ_DSCRPTN =
    						invPropagatedCosting.getInvBranchStockTransferLine().getInvBranchStockTransfer().getBstDescription();
    					ADJ_CRTD_BY =
    						invPropagatedCosting.getInvBranchStockTransferLine().getInvBranchStockTransfer().getBstPostedBy();
    					ADJ_RFRNC_NMBR = "INVBST" + 
						invPropagatedCosting.getInvBranchStockTransferLine().getInvBranchStockTransfer().getBstNumber();
    					
    				} else if (invPropagatedCosting.getInvBuildUnbuildAssemblyLine() != null){

   						TTL_CST = prevInvCosting.getCstRemainingValue() - invPropagatedCosting.getCstRemainingValue();
    					QNTY =  invPropagatedCosting.getInvBuildUnbuildAssemblyLine().getBlBuildQuantity();
    					ADJ_DSCRPTN =
    						invPropagatedCosting.getInvBuildUnbuildAssemblyLine().getInvBuildUnbuildAssembly().getBuaDescription();
    					ADJ_CRTD_BY =
    						invPropagatedCosting.getInvBuildUnbuildAssemblyLine().getInvBuildUnbuildAssembly().getBuaPostedBy();
    					ADJ_RFRNC_NMBR = "INVBUA" + 
						invPropagatedCosting.getInvBuildUnbuildAssemblyLine().getInvBuildUnbuildAssembly().getBuaDocumentNumber();
    					
    				} else if (invPropagatedCosting.getInvStockIssuanceLine()!= null){

    					TTL_CST = invPropagatedCosting.getInvStockIssuanceLine().getSilIssueCost();
						QNTY =  this.convertByUomFromAndItemAndQuantity(
								invPropagatedCosting.getInvStockIssuanceLine().getInvUnitOfMeasure(), 
								invPropagatedCosting.getInvStockIssuanceLine().getInvItemLocation().getInvItem(),
								invPropagatedCosting.getInvStockIssuanceLine().getSilIssueQuantity(), AD_CMPNY);
    					ADJ_DSCRPTN = invPropagatedCosting.getInvStockIssuanceLine().getInvStockIssuance().getSiDescription();
    					ADJ_CRTD_BY = invPropagatedCosting.getInvStockIssuanceLine().getInvStockIssuance().getSiPostedBy();
    					ADJ_RFRNC_NMBR = "INVSI" +
						invPropagatedCosting.getInvStockIssuanceLine().getInvStockIssuance().getSiDocumentNumber();
    					
    				} else if (invPropagatedCosting.getInvStockTransferLine()!= null) {

    					TTL_CST = invPropagatedCosting.getInvStockTransferLine().getStlAmount();
						QNTY =  this.convertByUomFromAndItemAndQuantity(
								invPropagatedCosting.getInvStockTransferLine().getInvUnitOfMeasure(), 
								invPropagatedCosting.getInvStockTransferLine().getInvItem(),
								invPropagatedCosting.getInvStockTransferLine().getStlQuantityDelivered(), AD_CMPNY);
    					ADJ_DSCRPTN = invPropagatedCosting.getInvStockTransferLine().getInvStockTransfer().getStDescription();
    					ADJ_CRTD_BY = invPropagatedCosting.getInvStockTransferLine().getInvStockTransfer().getStPostedBy();
    					ADJ_RFRNC_NMBR = "INVST" +
						invPropagatedCosting.getInvStockTransferLine().getInvStockTransfer().getStDocumentNumber();
    					
    				} else {
    					
    					prevInvCosting = invPropagatedCosting;
    					continue;
    					
    				}
    				
    				// if quantity is equal 0, no variance.
    				if(QNTY == 0) continue;
    				
    				// compute new cost variance
    				double UNT_CST = TTL_CST/QNTY;
    				double CST_VRNC_VL = (invPropagatedCosting.getCstRemainingQuantity() * UNT_CST -
    						invPropagatedCosting.getCstRemainingValue());
    				
    				if(CST_VRNC_VL != 0)
    					this.generateCostVariance(invPropagatedCosting.getInvItemLocation(), CST_VRNC_VL, ADJ_RFRNC_NMBR,
    							ADJ_DSCRPTN, invPropagatedCosting.getCstDate(), ADJ_CRTD_BY, AD_BRNCH, AD_CMPNY);
    				
    			}
    			
    			// set previous costing
    			prevInvCosting = invPropagatedCosting;
    			
    		}
    		
    	} catch (AdPRFCoaGlVarianceAccountNotFoundException ex){
    		
    		throw ex;
    		
    	} catch (Exception ex) {
    		
    		Debug.printStackTrace(ex);
    		getSessionContext().setRollbackOnly();
    		throw new EJBException(ex.getMessage());
    		
    	}     */   	
    }
    
    private void addInvDrEntry(short DR_LN, String DR_CLSS, byte DR_DBT, double DR_AMNT, byte DR_RVRSL, Integer COA_CODE, 
    		LocalInvAdjustment invAdjustment, Integer AD_BRNCH, Integer AD_CMPNY)
    
    throws GlobalBranchAccountNumberInvalidException {
    	
    	Debug.print("ApCheckBatchSubmitController addInvDrEntry");
    	
    	LocalAdCompanyHome adCompanyHome = null;
    	LocalInvDistributionRecordHome invDistributionRecordHome = null;        
    	LocalGlChartOfAccountHome glChartOfAccountHome = null;           
    	
    	// Initialize EJB Home
    	
    	try {
    		
    		adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);    
    		invDistributionRecordHome = (LocalInvDistributionRecordHome)EJBHomeFactory.
			lookUpLocalHome(LocalInvDistributionRecordHome.JNDI_NAME, LocalInvDistributionRecordHome.class);            
    		glChartOfAccountHome = (LocalGlChartOfAccountHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlChartOfAccountHome.JNDI_NAME, LocalGlChartOfAccountHome.class);                       
    		
    		
    	} catch (NamingException ex) {
    		
    		throw new EJBException(ex.getMessage());
    		
    	}            
    	
    	try {        
    		
    		// get company
    		
    		LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
    		
    		// validate coa
    		
    		LocalGlChartOfAccount glChartOfAccount = null;
    		
    		try {
    			
    			glChartOfAccount = glChartOfAccountHome.findByCoaCodeAndBranchCode(COA_CODE, AD_BRNCH, AD_CMPNY);
    			
    		} catch(FinderException ex) {
    			
    			throw new GlobalBranchAccountNumberInvalidException ();
    			
    		}
    		
    		// create distribution record        
    		
    		LocalInvDistributionRecord invDistributionRecord = invDistributionRecordHome.create(DR_LN, DR_CLSS, DR_DBT,
    				EJBCommon.roundIt(DR_AMNT, adCompany.getGlFunctionalCurrency().getFcPrecision()), DR_RVRSL, EJBCommon.FALSE,
					 AD_CMPNY);
    		
    		//invAdjustment.addInvDistributionRecord(invDistributionRecord);    		
    		invDistributionRecord.setInvAdjustment(invAdjustment);
    		//glChartOfAccount.addInvDistributionRecord(invDistributionRecord);
    		invDistributionRecord.setInvChartOfAccount(glChartOfAccount);
    		
    	} catch(GlobalBranchAccountNumberInvalidException ex) {
    		
    		throw new GlobalBranchAccountNumberInvalidException ();
    		
    	} catch (Exception ex) {
    		
    		Debug.printStackTrace(ex);
    		getSessionContext().setRollbackOnly();
    		throw new EJBException(ex.getMessage());
    		
    	}
    	
    }
    
    private void executeInvAdjPost(Integer ADJ_CODE, String USR_NM, Integer AD_BRNCH, Integer AD_CMPNY) throws
	GlobalRecordAlreadyDeletedException,		
	GlobalTransactionAlreadyPostedException,
	GlJREffectiveDateNoPeriodExistException,
	GlJREffectiveDatePeriodClosedException,
	GlobalJournalNotBalanceException,
	GlobalBranchAccountNumberInvalidException {
    	
    	Debug.print("ApCheckBatchSubmitController executeInvAdjPost");
    	
    	LocalInvAdjustmentHome invAdjustmentHome = null;        
    	LocalAdCompanyHome adCompanyHome = null;
    	LocalAdPreferenceHome adPreferenceHome = null;
    	LocalGlSetOfBookHome glSetOfBookHome = null;
    	LocalGlAccountingCalendarValueHome glAccountingCalendarValueHome = null;
    	LocalGlJournalHome glJournalHome = null;
    	LocalGlJournalBatchHome glJournalBatchHome = null;
    	LocalGlSuspenseAccountHome glSuspenseAccountHome = null;
    	LocalGlJournalLineHome glJournalLineHome = null;
    	LocalGlJournalSourceHome glJournalSourceHome = null;
    	LocalGlJournalCategoryHome glJournalCategoryHome = null;
    	LocalGlFunctionalCurrencyHome glFunctionalCurrencyHome = null;
    	LocalInvDistributionRecordHome invDistributionRecordHome = null;
    	LocalInvCostingHome invCostingHome = null;
    	LocalGlChartOfAccountHome glChartOfAccountHome = null;
    	LocalInvAdjustmentLineHome invAdjustmentLineHome = null;
    	
    	
    	// Initialize EJB Home
    	
    	try {
    		
    		invAdjustmentHome = (LocalInvAdjustmentHome)EJBHomeFactory.
			lookUpLocalHome(LocalInvAdjustmentHome.JNDI_NAME, LocalInvAdjustmentHome.class);
    		adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
    		adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);
    		glSetOfBookHome = (LocalGlSetOfBookHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlSetOfBookHome.JNDI_NAME, LocalGlSetOfBookHome.class);
    		glAccountingCalendarValueHome = (LocalGlAccountingCalendarValueHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlAccountingCalendarValueHome.JNDI_NAME, LocalGlAccountingCalendarValueHome.class);
    		glJournalHome = (LocalGlJournalHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlJournalHome.JNDI_NAME, LocalGlJournalHome.class);
    		glJournalBatchHome = (LocalGlJournalBatchHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlJournalBatchHome.JNDI_NAME, LocalGlJournalBatchHome.class);
    		glSuspenseAccountHome = (LocalGlSuspenseAccountHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlSuspenseAccountHome.JNDI_NAME, LocalGlSuspenseAccountHome.class);
    		glJournalLineHome = (LocalGlJournalLineHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlJournalLineHome.JNDI_NAME, LocalGlJournalLineHome.class);
    		glJournalSourceHome = (LocalGlJournalSourceHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlJournalSourceHome.JNDI_NAME, LocalGlJournalSourceHome.class);
    		glJournalCategoryHome = (LocalGlJournalCategoryHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlJournalCategoryHome.JNDI_NAME, LocalGlJournalCategoryHome.class);
    		glFunctionalCurrencyHome = (LocalGlFunctionalCurrencyHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlFunctionalCurrencyHome.JNDI_NAME, LocalGlFunctionalCurrencyHome.class);
    		invDistributionRecordHome = (LocalInvDistributionRecordHome)EJBHomeFactory.
			lookUpLocalHome(LocalInvDistributionRecordHome.JNDI_NAME, LocalInvDistributionRecordHome.class);
    		invCostingHome = (LocalInvCostingHome)EJBHomeFactory.
			lookUpLocalHome(LocalInvCostingHome.JNDI_NAME, LocalInvCostingHome.class);
    		glChartOfAccountHome = (LocalGlChartOfAccountHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlChartOfAccountHome.JNDI_NAME, LocalGlChartOfAccountHome.class);
    		invAdjustmentLineHome = (LocalInvAdjustmentLineHome)EJBHomeFactory.
			lookUpLocalHome(LocalInvAdjustmentLineHome.JNDI_NAME, LocalInvAdjustmentLineHome.class);

    		
    	} catch (NamingException ex) {
    		
    		throw new EJBException(ex.getMessage());
    		
    	}       
    	
    	try {
    		
    		// validate if adjustment is already deleted
    		
    		LocalInvAdjustment invAdjustment = null;
    		
    		try {
    			
    			invAdjustment = invAdjustmentHome.findByPrimaryKey(ADJ_CODE);
    			
    		} catch (FinderException ex) {
    			
    			throw new GlobalRecordAlreadyDeletedException();
    			
    		}
    		
    		// validate if adjustment is already posted or void
    		
    		if (invAdjustment.getAdjPosted() == EJBCommon.TRUE) {

    			if (invAdjustment.getAdjVoid() != EJBCommon.TRUE)
    			throw new GlobalTransactionAlreadyPostedException();
    			
    		}
    		
    		Collection invAdjustmentLines = null;
    		
    		if(invAdjustment.getAdjVoid() == EJBCommon.FALSE)
    			invAdjustmentLines = invAdjustmentLineHome.findByAlVoidAndAdjCode(EJBCommon.FALSE, invAdjustment.getAdjCode(), AD_CMPNY);
    		else
    			invAdjustmentLines = invAdjustmentLineHome.findByAlVoidAndAdjCode(EJBCommon.TRUE, invAdjustment.getAdjCode(), AD_CMPNY);

    		
    		Iterator i = invAdjustmentLines.iterator();
    		
    		while(i.hasNext()) {
    			
    			
    			LocalInvAdjustmentLine invAdjustmentLine = (LocalInvAdjustmentLine) i.next();

    			LocalInvCosting invCosting =
    				invCostingHome.getByMaxCstDateToLongAndMaxCstLineNumberAndLessThanEqualCstDateAndIiNameAndLocName(
    						invAdjustment.getAdjDate(), invAdjustmentLine.getInvItemLocation().getInvItem().getIiName(),
    						invAdjustmentLine.getInvItemLocation().getInvLocation().getLocName(), AD_BRNCH, AD_CMPNY);
    			
				this.postInvAdjustmentToInventory(invAdjustmentLine, invAdjustment.getAdjDate(), 0,
						invAdjustmentLine.getAlUnitCost(), invCosting.getCstRemainingQuantity(),
						invCosting.getCstRemainingValue() + invAdjustmentLine.getAlUnitCost(), AD_BRNCH, AD_CMPNY);
    			
    		}			
    		
    		// post to gl if necessary
    		
    		LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);
    		LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
    		
    			// validate if date has no period and period is closed
    			
    			LocalGlSetOfBook glJournalSetOfBook = null;
    			
    			try {
    				
    				glJournalSetOfBook = glSetOfBookHome.findByDate(invAdjustment.getAdjDate(), AD_CMPNY);
    				
    			} catch (FinderException ex) {
    				
    				throw new GlJREffectiveDateNoPeriodExistException();
    				
    			}
    			
    			LocalGlAccountingCalendarValue glAccountingCalendarValue = 
    				glAccountingCalendarValueHome.findByAcCodeAndDate(
    						glJournalSetOfBook.getGlAccountingCalendar().getAcCode(), invAdjustment.getAdjDate(), AD_CMPNY);
    			
    			
    			if (glAccountingCalendarValue.getAcvStatus() == 'N' ||
    					glAccountingCalendarValue.getAcvStatus() == 'C' ||
						glAccountingCalendarValue.getAcvStatus() == 'P') {
    				
    				throw new GlJREffectiveDatePeriodClosedException();
    				
    			}
    			
    			// check if invoice is balance if not check suspense posting
    			
    			LocalGlJournalLine glOffsetJournalLine = null;
    			
    			Collection invDistributionRecords = null;
    			
    			if (invAdjustment.getAdjVoid() == EJBCommon.FALSE) {
    				
    				invDistributionRecords = invDistributionRecordHome.findImportableDrByDrReversedAndAdjCode(EJBCommon.FALSE,
    						invAdjustment.getAdjCode(), AD_CMPNY);
    				
    			} else {
    				
    				invDistributionRecords = invDistributionRecordHome.findImportableDrByDrReversedAndAdjCode(EJBCommon.TRUE,
    						invAdjustment.getAdjCode(), AD_CMPNY);
    				
    			}
    			
    			Iterator j = invDistributionRecords.iterator();
    			
    			double TOTAL_DEBIT = 0d;
    			double TOTAL_CREDIT = 0d;
    			
    			while (j.hasNext()) {
    				
    				LocalInvDistributionRecord invDistributionRecord = (LocalInvDistributionRecord)j.next();
    				
    				double DR_AMNT = 0d;
    				
    				DR_AMNT = invDistributionRecord.getDrAmount();
    				
    				if (invDistributionRecord.getDrDebit() == EJBCommon.TRUE) {
    					
    					TOTAL_DEBIT += DR_AMNT;
    					
    				} else {
    					
    					TOTAL_CREDIT += DR_AMNT;
    					
    				}
    				
    			}
    			
    			TOTAL_DEBIT = EJBCommon.roundIt(TOTAL_DEBIT, adCompany.getGlFunctionalCurrency().getFcPrecision());
    			TOTAL_CREDIT = EJBCommon.roundIt(TOTAL_CREDIT, adCompany.getGlFunctionalCurrency().getFcPrecision());
    			
    			if (adPreference.getPrfAllowSuspensePosting() == EJBCommon.TRUE &&
    					TOTAL_DEBIT != TOTAL_CREDIT) {
    				
    				LocalGlSuspenseAccount glSuspenseAccount = null;
    				
    				try { 	
    					
    					glSuspenseAccount = glSuspenseAccountHome.findByJsNameAndJcName("INVENTORY", "INVENTORY ADJUSTMENTS",
    							AD_CMPNY);
    					
    				} catch (FinderException ex) {
    					
    					throw new GlobalJournalNotBalanceException();
    					
    				}
    				
    				if (TOTAL_DEBIT - TOTAL_CREDIT < 0) {
    					
    					glOffsetJournalLine = glJournalLineHome.create((short)(invDistributionRecords.size() + 1), EJBCommon.TRUE, 
    							TOTAL_CREDIT - TOTAL_DEBIT, "", AD_CMPNY);
    					
    				} else {
    					
    					glOffsetJournalLine = glJournalLineHome.create((short)(invDistributionRecords.size() + 1), EJBCommon.FALSE,
    							TOTAL_DEBIT - TOTAL_CREDIT, "", AD_CMPNY);
    					
    				}
    				
    				LocalGlChartOfAccount glChartOfAccount = glSuspenseAccount.getGlChartOfAccount();
    				//glChartOfAccount.addGlJournalLine(glOffsetJournalLine);
    				glOffsetJournalLine.setGlChartOfAccount(glChartOfAccount);
    				
    				
    			} else if (adPreference.getPrfAllowSuspensePosting() == EJBCommon.FALSE &&
    					TOTAL_DEBIT != TOTAL_CREDIT) {
    				
    				throw new GlobalJournalNotBalanceException();		    	
    				
    			}
    			
    			// create journal batch if necessary
    			
    			LocalGlJournalBatch glJournalBatch = null;
    			java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat("MM/dd/yyyy");
    			
    			try {
    				
    				glJournalBatch = glJournalBatchHome.findByJbName("JOURNAL IMPORT " + formatter.format(new Date()) +
    						" INVENTORY ADJUSTMENTS", AD_BRNCH, AD_CMPNY);
    				
    			} catch (FinderException ex) {
    				
    			}
    			
    			if (glJournalBatch == null) {
    				
    				glJournalBatch = glJournalBatchHome.create("JOURNAL IMPORT " + formatter.format(new Date()) +
    						" INVENTORY ADJUSTMENTS", "JOURNAL IMPORT", "CLOSED", EJBCommon.getGcCurrentDateWoTime().getTime(),
							USR_NM, AD_BRNCH, AD_CMPNY);
    				
    			}
    			
    			// create journal entry			            	
    			
    			LocalGlJournal glJournal = glJournalHome.create(invAdjustment.getAdjReferenceNumber(),
    					invAdjustment.getAdjDescription(), invAdjustment.getAdjDate(),
						0.0d, null, invAdjustment.getAdjDocumentNumber(), null, 1d, "N/A", null,
						'N', EJBCommon.TRUE, EJBCommon.FALSE,
						USR_NM, new Date(),
						USR_NM, new Date(),
						null, null,
						USR_NM, EJBCommon.getGcCurrentDateWoTime().getTime(), 
						null, null, EJBCommon.FALSE, null, 
						AD_BRNCH, AD_CMPNY);
    			
    			LocalGlJournalSource glJournalSource = glJournalSourceHome.findByJsName("INVENTORY", AD_CMPNY);
    			glJournal.setGlJournalSource(glJournalSource);
    			    			
    			LocalGlFunctionalCurrency glFunctionalCurrency = glFunctionalCurrencyHome.findByFcName(adCompany.getGlFunctionalCurrency().getFcName(), AD_CMPNY);
    			glJournal.setGlFunctionalCurrency(glFunctionalCurrency);
    			
    			LocalGlJournalCategory glJournalCategory = glJournalCategoryHome.findByJcName("INVENTORY ADJUSTMENTS", AD_CMPNY);
    			glJournal.setGlJournalCategory(glJournalCategory);
    			
    			if (glJournalBatch != null) {
    				
    				glJournal.setGlJournalBatch(glJournalBatch);
    				
    			}           		    
    			
    			// create journal lines
    			
    			j = invDistributionRecords.iterator();
    			
    			while (j.hasNext()) {
    				
    				LocalInvDistributionRecord invDistributionRecord = (LocalInvDistributionRecord)j.next();
    				
    				double DR_AMNT = 0d;
    				
    				DR_AMNT = invDistributionRecord.getDrAmount();
    				
    				LocalGlJournalLine glJournalLine = glJournalLineHome.create(invDistributionRecord.getDrLine(),	            			
    						invDistributionRecord.getDrDebit(), DR_AMNT, "", AD_CMPNY);
    				
    				//invDistributionRecord.getInvChartOfAccount().addGlJournalLine(glJournalLine);
    				glJournalLine.setGlChartOfAccount(invDistributionRecord.getInvChartOfAccount());
    				
    				//glJournal.addGlJournalLine(glJournalLine);
    				glJournalLine.setGlJournal(glJournal);
    				
    				invDistributionRecord.setDrImported(EJBCommon.TRUE);
    				
    				
    			}
    			
    			if (glOffsetJournalLine != null) {
    				
    				//glJournal.addGlJournalLine(glOffsetJournalLine);
    				glOffsetJournalLine.setGlJournal(glJournal);
    				
    			}		
    			
    			// post journal to gl
    			
    			Collection glJournalLines = glJournal.getGlJournalLines();
    			
    			i = glJournalLines.iterator();
    			
    			while (i.hasNext()) {
    				
    				LocalGlJournalLine glJournalLine = (LocalGlJournalLine)i.next();	
    				
    				// post current to current acv
    				
    				this.postToGl(glAccountingCalendarValue,
    						glJournalLine.getGlChartOfAccount(),
							true, glJournalLine.getJlDebit(), glJournalLine.getJlAmount(), AD_CMPNY);
    				
    				
    				// post to subsequent acvs (propagate)
    				
    				Collection glSubsequentAccountingCalendarValues = 
    					glAccountingCalendarValueHome.findSubsequentAcvByAcCodeAndAcvPeriodNumber( 
    							glJournalSetOfBook.getGlAccountingCalendar().getAcCode(),
								glAccountingCalendarValue.getAcvPeriodNumber(), AD_CMPNY);     
    				
    				Iterator acvsIter = glSubsequentAccountingCalendarValues.iterator();
    				
    				while (acvsIter.hasNext()) {
    					
    					LocalGlAccountingCalendarValue glSubsequentAccountingCalendarValue = 
    						(LocalGlAccountingCalendarValue)acvsIter.next();
    					
    					this.postToGl(glSubsequentAccountingCalendarValue,
    							glJournalLine.getGlChartOfAccount(),
								false, glJournalLine.getJlDebit(), glJournalLine.getJlAmount(), AD_CMPNY);
    					
    				}
    				
    				// post to subsequent years if necessary
    				
    				Collection glSubsequentSetOfBooks = glSetOfBookHome.findSubsequentSobByAcYear(
    						glJournalSetOfBook.getGlAccountingCalendar().getAcYear(), AD_CMPNY);
    				
    				if (!glSubsequentSetOfBooks.isEmpty() && glJournalSetOfBook.getSobYearEndClosed() == 1) {
    					
    					adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);		  	  	
    					LocalGlChartOfAccount glRetainedEarningsAccount =
    						glChartOfAccountHome.findByCoaAccountNumberAndBranchCode(adCompany.getCmpRetainedEarnings(),
    								AD_BRNCH, AD_CMPNY); 	  	
    					
    					Iterator sobIter = glSubsequentSetOfBooks.iterator();
    					
    					while (sobIter.hasNext()) {
    						
    						LocalGlSetOfBook glSubsequentSetOfBook = (LocalGlSetOfBook)sobIter.next();
    						
    						String ACCOUNT_TYPE = glJournalLine.getGlChartOfAccount().getCoaAccountType();
    						
    						// post to subsequent acvs of subsequent set of book(propagate)
    						
    						Collection glAccountingCalendarValues = 
    							glAccountingCalendarValueHome.findByAcCode(
    									glSubsequentSetOfBook.getGlAccountingCalendar().getAcCode(), AD_CMPNY);     
    						
    						Iterator acvIter = glAccountingCalendarValues.iterator();
    						
    						while (acvIter.hasNext()) {
    							
    							LocalGlAccountingCalendarValue glSubsequentAccountingCalendarValue = 
    								(LocalGlAccountingCalendarValue)acvIter.next();
    							
    							if (ACCOUNT_TYPE.equals("ASSET") || ACCOUNT_TYPE.equals("LIABILITY") ||
    									ACCOUNT_TYPE.equals("OWNERS EQUITY")) {
    								
    								this.postToGl(glSubsequentAccountingCalendarValue,
    										glJournalLine.getGlChartOfAccount(),
											false, glJournalLine.getJlDebit(), glJournalLine.getJlAmount(), AD_CMPNY);
    								
    							} else { // revenue & expense
    								
    								this.postToGl(glSubsequentAccountingCalendarValue,
    										glRetainedEarningsAccount,
											false, glJournalLine.getJlDebit(), glJournalLine.getJlAmount(), AD_CMPNY);					        
    								
    							}
    							
    						}
    						
    						if (glSubsequentSetOfBook.getSobYearEndClosed() == 0) break;
    						
    					}
    					
    				}
    				
    			}
			
			invAdjustment.setAdjPosted(EJBCommon.TRUE);

    	} catch (GlJREffectiveDateNoPeriodExistException ex) {
    		
    		getSessionContext().setRollbackOnly();
    		throw ex;
    		
    	} catch (GlJREffectiveDatePeriodClosedException ex) {
    		
    		getSessionContext().setRollbackOnly();
    		throw ex;
    		
    	} catch (GlobalJournalNotBalanceException ex) {
    		
    		getSessionContext().setRollbackOnly();
    		throw ex;
    		
    	} catch (GlobalRecordAlreadyDeletedException ex) {
    		
    		getSessionContext().setRollbackOnly();
    		throw ex;
    		
    	} catch (GlobalTransactionAlreadyPostedException ex) {
    		
    		getSessionContext().setRollbackOnly();
    		throw ex;
    		
    	} catch (Exception ex) {
    		
    		Debug.printStackTrace(ex);
    		getSessionContext().setRollbackOnly();
    		throw new EJBException(ex.getMessage());
    		
    	}
    	
    }

    private LocalInvAdjustmentLine addInvAlEntry(LocalInvItemLocation invItemLocation, LocalInvAdjustment invAdjustment,
    		double CST_VRNC_VL, byte AL_VD, Integer AD_CMPNY) {
    	
    	Debug.print("ArMiscReceiptEntryControllerBean addInvAlEntry");
    	
    	LocalInvAdjustmentLineHome invAdjustmentLineHome = null;
    	
    	// Initialize EJB Home
    	
    	try {
    		
    		invAdjustmentLineHome = (LocalInvAdjustmentLineHome)EJBHomeFactory.
			lookUpLocalHome(LocalInvAdjustmentLineHome.JNDI_NAME, LocalInvAdjustmentLineHome.class); 
    		
    	} catch (NamingException ex) {
    		
    		throw new EJBException(ex.getMessage());
    		
    	}            
    	
    	try {
    		
    		// create dr entry
    		LocalInvAdjustmentLine invAdjustmentLine = null;
    		invAdjustmentLine = invAdjustmentLineHome.create(CST_VRNC_VL, null, null, 0,0, AL_VD, AD_CMPNY);
    		
    		// map adjustment, unit of measure, item location
    		//invAdjustment.addInvAdjustmentLine(invAdjustmentLine);
    		invAdjustmentLine.setInvAdjustment(invAdjustment);
    		//invItemLocation.getInvItem().getInvUnitOfMeasure().addInvAdjustmentLine(invAdjustmentLine);
    		invAdjustmentLine.setInvUnitOfMeasure(invItemLocation.getInvItem().getInvUnitOfMeasure());
    		//invItemLocation.addInvAdjustmentLine(invAdjustmentLine);
    		invAdjustmentLine.setInvItemLocation(invItemLocation);
    		
    		return invAdjustmentLine;
    		
    	} catch (Exception ex) {
    		
    		Debug.printStackTrace(ex);
    		getSessionContext().setRollbackOnly();
    		throw new EJBException(ex.getMessage());
    		
    	}
    	
    } 

    private LocalInvAdjustment saveInvAdjustment(String ADJ_RFRNC_NMBR, String ADJ_DSCRPTN,
    		Date ADJ_DATE, String USR_NM, Integer AD_BRNCH, Integer AD_CMPNY){
    	
    	Debug.print("ApCheckBatchSubmitController saveInvAdjustment");
    	
    	LocalInvAdjustmentHome invAdjustmentHome = null;
    	LocalAdDocumentSequenceAssignmentHome adDocumentSequenceAssignmentHome = null;
    	LocalAdBranchDocumentSequenceAssignmentHome adBranchDocumentSequenceAssignmentHome = null;
    	// Initialize EJB Home    	
    	
    	try{
    	
    		invAdjustmentHome = (LocalInvAdjustmentHome)EJBHomeFactory.
			lookUpLocalHome(LocalInvAdjustmentHome.JNDI_NAME, LocalInvAdjustmentHome.class);
			adDocumentSequenceAssignmentHome = (LocalAdDocumentSequenceAssignmentHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdDocumentSequenceAssignmentHome.JNDI_NAME, LocalAdDocumentSequenceAssignmentHome.class);
			adBranchDocumentSequenceAssignmentHome = (LocalAdBranchDocumentSequenceAssignmentHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdBranchDocumentSequenceAssignmentHome.JNDI_NAME, LocalAdBranchDocumentSequenceAssignmentHome.class); 

    	} catch (NamingException ex) {
    		
    		throw new EJBException(ex.getMessage());
    		
    	}       
    	
    	try{
    		
    		// generate adj document number
    		String ADJ_DCMNT_NMBR = null;
    		
    		LocalAdBranchDocumentSequenceAssignment adBranchDocumentSequenceAssignment = null;
    		LocalAdDocumentSequenceAssignment adDocumentSequenceAssignment = null;
    		
    		try {
    			
    			adDocumentSequenceAssignment = adDocumentSequenceAssignmentHome.findByDcName("INV ADJUSTMENT", AD_CMPNY);
    			
    		} catch (FinderException ex) {
    			
    		}
    		
    		try {
    			
    			adBranchDocumentSequenceAssignment = adBranchDocumentSequenceAssignmentHome.findBdsByDsaCodeAndBrCode(
    					adDocumentSequenceAssignment.getDsaCode(), AD_BRNCH, AD_CMPNY);
    			
    		} catch (FinderException ex) {
    			
    		}
    		
    		while (true) {
    			
    			if (adBranchDocumentSequenceAssignment == null || adBranchDocumentSequenceAssignment.getBdsNextSequence() == null) {
    				
    				try {
    					
    					invAdjustmentHome.findByAdjDocumentNumberAndBrCode(adDocumentSequenceAssignment.getDsaNextSequence(),
    							AD_BRNCH, AD_CMPNY);		            		
    					adDocumentSequenceAssignment.setDsaNextSequence(EJBCommon.incrementStringNumber(
    							adDocumentSequenceAssignment.getDsaNextSequence()));	
    					
    				} catch (FinderException ex) {
    					
    					ADJ_DCMNT_NMBR = adDocumentSequenceAssignment.getDsaNextSequence();
    					adDocumentSequenceAssignment.setDsaNextSequence(EJBCommon.incrementStringNumber(
    							adDocumentSequenceAssignment.getDsaNextSequence()));	
    					break;
    					
    				}
    				
    			} else {
    				
    				try {
    					
    					invAdjustmentHome.findByAdjDocumentNumberAndBrCode(
    							adBranchDocumentSequenceAssignment.getBdsNextSequence(), AD_BRNCH, AD_CMPNY);		            		
    					adBranchDocumentSequenceAssignment.setBdsNextSequence(EJBCommon.incrementStringNumber(
    							adBranchDocumentSequenceAssignment.getBdsNextSequence()));
    					
    				} catch (FinderException ex) {
    					
    					ADJ_DCMNT_NMBR = adBranchDocumentSequenceAssignment.getBdsNextSequence();
    					adBranchDocumentSequenceAssignment.setBdsNextSequence(EJBCommon.incrementStringNumber(
    							adBranchDocumentSequenceAssignment.getBdsNextSequence()));	
    					break;
    					
    				}
    				
    			}
    			
    		}		            
    		
    		LocalInvAdjustment invAdjustment = invAdjustmentHome.create(ADJ_DCMNT_NMBR, ADJ_RFRNC_NMBR,
    				ADJ_DSCRPTN, ADJ_DATE, "COST-VARIANCE", "N/A", EJBCommon.FALSE, USR_NM, ADJ_DATE, USR_NM, ADJ_DATE, null, null,
					USR_NM, ADJ_DATE, null, null, EJBCommon.TRUE, EJBCommon.FALSE, AD_BRNCH, AD_CMPNY);
    		
    		return invAdjustment;
    		
    	} catch (Exception ex) {
    		
    		Debug.printStackTrace(ex);
    		getSessionContext().setRollbackOnly();
    		throw new EJBException(ex.getMessage());
    		
    	}       
    	
    	
    }
    
    private void postInvAdjustmentToInventory(LocalInvAdjustmentLine invAdjustmentLine, Date CST_DT, double CST_ADJST_QTY,
    		double CST_ADJST_CST, double CST_RMNNG_QTY, double CST_RMNNG_VL, Integer AD_BRNCH,Integer AD_CMPNY) {
    	
    	Debug.print("ApCheckBatchSubmitController postInvAdjustmentToInventory");
    	
    	LocalInvCostingHome invCostingHome = null;
    	LocalAdPreferenceHome adPreferenceHome = null;
    	LocalAdCompanyHome adCompanyHome = null;
    	
    	// Initialize EJB Home
    	
    	try {
    		
    		invCostingHome = (LocalInvCostingHome)EJBHomeFactory.
			lookUpLocalHome(LocalInvCostingHome.JNDI_NAME, LocalInvCostingHome.class);
    		adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);
    		adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);              
    		
    	} catch (NamingException ex) {
    		
    		getSessionContext().setRollbackOnly();
    		throw new EJBException(ex.getMessage());
    		
    	}
    	
    	try {
    		
    		LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);
    		LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
    		LocalInvItemLocation invItemLocation = invAdjustmentLine.getInvItemLocation();
    		int CST_LN_NMBR = 0;
    		
    		CST_ADJST_QTY = EJBCommon.roundIt(CST_ADJST_QTY, adPreference.getPrfInvQuantityPrecisionUnit());
    		CST_ADJST_CST = EJBCommon.roundIt(CST_ADJST_CST, adCompany.getGlFunctionalCurrency().getFcPrecision());
    		CST_RMNNG_QTY = EJBCommon.roundIt(CST_RMNNG_QTY, adPreference.getPrfInvQuantityPrecisionUnit());
    		CST_RMNNG_VL = EJBCommon.roundIt(CST_RMNNG_VL, adCompany.getGlFunctionalCurrency().getFcPrecision());
    		
    		if (CST_ADJST_QTY < 0) {
    			
    			invItemLocation.setIlCommittedQuantity(invItemLocation.getIlCommittedQuantity() - Math.abs(CST_ADJST_QTY));
    			
    		}
    		
    		// create costing
    		
    		try {
    			
    			// generate line number
    			
    			LocalInvCosting invCurrentCosting = invCostingHome.getByMaxCstLineNumberAndCstDateToLongAndIiNameAndLocName(
    					CST_DT.getTime(), invItemLocation.getInvItem().getIiName(), invItemLocation.getInvLocation().getLocName(),
						AD_BRNCH, AD_CMPNY);
    			CST_LN_NMBR = invCurrentCosting.getCstLineNumber() + 1;
    			
    		} catch (FinderException ex) {
    			
    			CST_LN_NMBR = 1;
    			
    		}
    		
    		LocalInvCosting invCosting = invCostingHome.create(CST_DT, CST_DT.getTime(), CST_LN_NMBR, 0d, 0d, 0d, 0d,
    				CST_ADJST_QTY, CST_ADJST_CST, 0d, 0d, CST_RMNNG_QTY, CST_RMNNG_VL, 0d, 0d, CST_ADJST_QTY > 0 ? CST_ADJST_QTY : 0, AD_BRNCH, AD_CMPNY);
    		//invItemLocation.addInvCosting(invCosting);
    		invCosting.setInvItemLocation(invItemLocation);
    		//invCosting.setInvAdjustmentLine(invAdjustmentLine);
    		invAdjustmentLine.setInvCosting(invCosting);
    		
    		// propagate balance if necessary           
    		
    		Collection invCostings = invCostingHome.findByGreaterThanCstDateAndIiNameAndLocName(CST_DT, invItemLocation.getInvItem().getIiName(), invItemLocation.getInvLocation().getLocName(), AD_BRNCH, AD_CMPNY);
    		
    		Iterator i = invCostings.iterator();
    		
    		while (i.hasNext()) {
    			
    			LocalInvCosting invPropagatedCosting = (LocalInvCosting)i.next();
    			
    			invPropagatedCosting.setCstRemainingQuantity(invPropagatedCosting.getCstRemainingQuantity() + CST_ADJST_QTY);
    			invPropagatedCosting.setCstRemainingValue(invPropagatedCosting.getCstRemainingValue() + CST_ADJST_CST);
    			
    		}                           
    		
    		
    	} catch (Exception ex) {
    		
    		Debug.printStackTrace(ex);
    		getSessionContext().setRollbackOnly();
    		throw new EJBException(ex.getMessage());
    		
    	}
    	
    	
    	
    }

   // SessionBean methods

   /**
    * @ejb:create-method view-type="remote"
    **/
   public void ejbCreate() throws CreateException {

      Debug.print("ApCheckBatchSubmitControllerBean ejbCreate");
      
   }

   // private methods
   	
		   
}
