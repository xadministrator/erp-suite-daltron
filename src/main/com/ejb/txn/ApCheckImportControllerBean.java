
/*
 * ApCheckImportControllerBean.java
 *
 * Created on December 27, 2005 02:23 PM
 * @author  Farrah S. Garing
 */

package com.ejb.txn;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;
import javax.naming.NamingException;

import com.ejb.ad.LocalAdAmountLimitHome;
import com.ejb.ad.LocalAdApprovalHome;
import com.ejb.ad.LocalAdApprovalQueueHome;
import com.ejb.ad.LocalAdApprovalUserHome;
import com.ejb.ad.LocalAdBankAccount;
import com.ejb.ad.LocalAdBankAccountHome;
import com.ejb.ad.LocalAdCompany;
import com.ejb.ad.LocalAdCompanyHome;
import com.ejb.ad.LocalAdPreferenceHome;
import com.ejb.ap.LocalApAppliedVoucher;
import com.ejb.ap.LocalApAppliedVoucherHome;
import com.ejb.ap.LocalApCheck;
import com.ejb.ap.LocalApCheckBatch;
import com.ejb.ap.LocalApCheckBatchHome;
import com.ejb.ap.LocalApCheckHome;
import com.ejb.ap.LocalApDistributionRecord;
import com.ejb.ap.LocalApDistributionRecordHome;
import com.ejb.ap.LocalApSupplier;
import com.ejb.ap.LocalApSupplierHome;
import com.ejb.ap.LocalApVoucher;
import com.ejb.ap.LocalApVoucherHome;
import com.ejb.ap.LocalApVoucherPaymentSchedule;
import com.ejb.ap.LocalApVoucherPaymentScheduleHome;
import com.ejb.exception.ApCHKCheckNumberNotUniqueException;
import com.ejb.exception.ApCHKVoucherHasNoWTaxCodeException;
import com.ejb.exception.ApVOUOverapplicationNotAllowedException;
import com.ejb.exception.GlobalDocumentNumberNotUniqueException;
import com.ejb.exception.GlobalJournalNotBalanceException;
import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.exception.GlobalRecordInvalidException;
import com.ejb.exception.GlobalTransactionAlreadyLockedException;
import com.ejb.exception.GlobalTransactionAlreadyPostedException;
import com.ejb.gl.LocalAdDocumentSequenceAssignmentHome;
import com.ejb.gl.LocalGlChartOfAccount;
import com.ejb.gl.LocalGlChartOfAccountHome;
import com.ejb.gl.LocalGlFunctionalCurrency;
import com.ejb.gl.LocalGlFunctionalCurrencyHome;
import com.ejb.gl.LocalGlFunctionalCurrencyRate;
import com.ejb.gl.LocalGlFunctionalCurrencyRateHome;
import com.util.AbstractSessionBean;
import com.util.ApModAppliedVoucherDetails;
import com.util.ApModCheckDetails;
import com.util.Debug;
import com.util.EJBCommon;
import com.util.EJBHomeFactory;

/**
 * @ejb:bean name="ApCheckImportControllerEJB"
 *           display-name="Used for uploading pos from external file to pos interface"
 *           type="Stateless"
 *           view-type="remote"
 *           jndi-name="ejb/ApCheckImportControllerEJB"
 *
 * @ejb:interface remote-class="com.ejb.txn.ApCheckImportController"
 *                extends="javax.ejb.EJBObject"
 *
 * @ejb:home remote-class="com.ejb.txn.ApCheckImportControllerHome"
 *           extends="javax.ejb.EJBHome"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="aruser"
 *                        role-link="aruserlink"
 *
 * @ejb:permission role-name="aruser"
 * 
 */

public class ApCheckImportControllerBean extends AbstractSessionBean {
	
	/**
	 * @ejb:interface-method view-type="remote"
	 **/
	public void importCheck(ArrayList list, String FC_NM, String CB_NM, Integer AD_BRNCH, Integer AD_CMPNY) throws
			GlobalDocumentNumberNotUniqueException,
			ApCHKCheckNumberNotUniqueException,
			ApCHKVoucherHasNoWTaxCodeException,
			GlobalNoRecordFoundException,
			GlobalJournalNotBalanceException,
			ApVOUOverapplicationNotAllowedException,
			GlobalTransactionAlreadyLockedException,
			GlobalRecordInvalidException,
			GlobalTransactionAlreadyPostedException {
		
		Debug.print("ApCheckImportControllerBean saveApChkEntry");
		
		LocalApCheckHome apCheckHome = null;        
		LocalApCheckBatchHome apCheckBatchHome = null;
		LocalAdCompanyHome adCompanyHome = null;
		LocalGlFunctionalCurrencyHome glFunctionalCurrencyHome = null;
		LocalAdDocumentSequenceAssignmentHome adDocumentSequenceAssignmentHome = null;
		LocalAdBankAccountHome adBankAccountHome = null;
		LocalApSupplierHome apSupplierHome = null;
		LocalGlFunctionalCurrencyRateHome glFunctionalCurrencyRateHome = null;
		LocalApDistributionRecordHome apDistributionRecordHome = null;
		LocalAdApprovalHome adApprovalHome = null;
		LocalAdAmountLimitHome adAmountLimitHome = null;
		LocalAdApprovalUserHome adApprovalUserHome = null;
		LocalAdApprovalQueueHome adApprovalQueueHome = null;
		LocalAdPreferenceHome adPreferenceHome = null;
		LocalApVoucherPaymentScheduleHome apVoucherPaymentScheduleHome = null;
		LocalApVoucherHome apVoucherHome = null; 
		
		LocalApCheck apCheck = null;
		
		// Initialize EJB Home
		
		try {
			
			apCheckHome = (LocalApCheckHome)EJBHomeFactory.
			lookUpLocalHome(LocalApCheckHome.JNDI_NAME, LocalApCheckHome.class);
			apCheckBatchHome = (LocalApCheckBatchHome)EJBHomeFactory.
			lookUpLocalHome(LocalApCheckBatchHome.JNDI_NAME, LocalApCheckBatchHome.class);
			adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
			glFunctionalCurrencyHome = (LocalGlFunctionalCurrencyHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlFunctionalCurrencyHome.JNDI_NAME, LocalGlFunctionalCurrencyHome.class);
			adDocumentSequenceAssignmentHome = (LocalAdDocumentSequenceAssignmentHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdDocumentSequenceAssignmentHome.JNDI_NAME, LocalAdDocumentSequenceAssignmentHome.class);            
			adBankAccountHome = (LocalAdBankAccountHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdBankAccountHome.JNDI_NAME, LocalAdBankAccountHome.class);
			apSupplierHome = (LocalApSupplierHome)EJBHomeFactory.
			lookUpLocalHome(LocalApSupplierHome.JNDI_NAME, LocalApSupplierHome.class);
			glFunctionalCurrencyRateHome = (LocalGlFunctionalCurrencyRateHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlFunctionalCurrencyRateHome.JNDI_NAME, LocalGlFunctionalCurrencyRateHome.class);
			apDistributionRecordHome = (LocalApDistributionRecordHome)EJBHomeFactory.
			lookUpLocalHome(LocalApDistributionRecordHome.JNDI_NAME, LocalApDistributionRecordHome.class);
			adApprovalHome = (LocalAdApprovalHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdApprovalHome.JNDI_NAME, LocalAdApprovalHome.class);
			adAmountLimitHome = (LocalAdAmountLimitHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdAmountLimitHome.JNDI_NAME, LocalAdAmountLimitHome.class);
			adApprovalUserHome = (LocalAdApprovalUserHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdApprovalUserHome.JNDI_NAME, LocalAdApprovalUserHome.class);
			adApprovalQueueHome = (LocalAdApprovalQueueHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdApprovalQueueHome.JNDI_NAME, LocalAdApprovalQueueHome.class);
			adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);
			apVoucherPaymentScheduleHome = (LocalApVoucherPaymentScheduleHome)EJBHomeFactory.
			lookUpLocalHome(LocalApVoucherPaymentScheduleHome.JNDI_NAME, LocalApVoucherPaymentScheduleHome.class);
			apVoucherHome = (LocalApVoucherHome)EJBHomeFactory.
			lookUpLocalHome(LocalApVoucherHome.JNDI_NAME, LocalApVoucherHome.class);
			
		} catch (NamingException ex) {
			
			throw new EJBException(ex.getMessage());
			
		}       
		
		try {
			
			Iterator i = list.iterator();
			
			while (i.hasNext()) {
				
				ApModCheckDetails details = (ApModCheckDetails) i.next();

				details = validate(details);
				
				LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);

				// validate if document number is unique document number is automatic then set next sequence					
					
				LocalApCheck apExistingCheck = null;
				
				try {
					
					apExistingCheck = apCheckHome.findByChkDocumentNumberAndBrCode(
							details.getChkDocumentNumber(), AD_BRNCH, AD_CMPNY);
					
				} catch (FinderException ex) { 
				}
				
				if (apExistingCheck != null) {
					
					throw new GlobalDocumentNumberNotUniqueException(details.getChkDocumentNumber());
					
				}
				
				// validate if check number is unique check is automatic then set next sequence
				
				try {
					
					apExistingCheck = apCheckHome.findByChkNumberAndBaName(
							details.getChkNumber(), details.getChkBaName(), AD_CMPNY);
					
				} catch (FinderException ex) {
					
					
				}

				if (apExistingCheck != null) {
					
					throw new ApCHKCheckNumberNotUniqueException(details.getChkNumber() + " for bank account: " + details.getChkBaName());
					
				}
				
				LocalAdBankAccount adBankAccount = null;
				
				try {
					
					adBankAccount = adBankAccountHome.findByBaName(details.getChkBaName(), AD_CMPNY);

				} catch (FinderException ex){
					
					throw new GlobalNoRecordFoundException("Bank Account " + details.getChkBaName());
					
				}
				
				if (details.getChkNumber() == null || details.getChkNumber().trim().length() == 0) {
					
					while (true) {
						
						try {
							
							apCheckHome.findByChkNumberAndBaName(adBankAccount.getBaNextCheckNumber(), details.getChkBaName(), AD_CMPNY);		            		
							adBankAccount.setBaNextCheckNumber(EJBCommon.incrementStringNumber(adBankAccount.getBaNextCheckNumber()));	
							
						} catch (FinderException ex) {
							
							details.setChkNumber(adBankAccount.getBaNextCheckNumber());
							adBankAccount.setBaNextCheckNumber(EJBCommon.incrementStringNumber(adBankAccount.getBaNextCheckNumber()));	
							break;
							
						}	            			            			            			            	
						
					}
					
				}
				
				// create check
				
				apCheck = apCheckHome.create("PAYMENT", null, details.getChkDate(), details.getChkCheckDate(), null, null, null, null, null,
						details.getChkNumber(), details.getChkDocumentNumber(),
						details.getChkReferenceNumber(),
						EJBCommon.FALSE, EJBCommon.FALSE, null, null, null, 0d, 0d, 0d, 0d, 0d,
						EJBCommon.FALSE, EJBCommon.FALSE,
						details.getChkConversionDate(), 1.0,
						0d, details.getChkAmount(), null, null, EJBCommon.FALSE, 
						EJBCommon.FALSE, details.getChkCrossCheck(), null, EJBCommon.FALSE, details.getChkCreatedBy(),
						details.getChkDateCreated(), details.getChkLastModifiedBy(),
						details.getChkDateLastModified(), null, null, null, null, EJBCommon.FALSE, null,
						EJBCommon.FALSE, null, EJBCommon.FALSE, EJBCommon.FALSE, null, null, AD_BRNCH, AD_CMPNY);          	    	        	    	        	    

					apCheck.setAdBankAccount(adBankAccount);
					
				try {				
					
					LocalGlFunctionalCurrency glFunctionalCurrency = glFunctionalCurrencyHome.findByFcName(FC_NM, AD_CMPNY);
					apCheck.setGlFunctionalCurrency(glFunctionalCurrency);
					
				} catch (FinderException ex){
					
					throw new GlobalNoRecordFoundException("Functional Currency " + FC_NM);
					
				}
				
				LocalApSupplier apSupplier = null;
				
				try {
					
					apSupplier = apSupplierHome.findBySplSupplierCode(details.getChkSplName(), AD_CMPNY);
					apCheck.setApSupplier(apSupplier);

				} catch (FinderException ex){
					
					throw new GlobalNoRecordFoundException("Supplier " + details.getChkSplName());
					
				}

				LocalApCheckBatch apCheckBatch = null;
				
				try {
					
					apCheckBatch = apCheckBatchHome.findByCbName(CB_NM, AD_BRNCH, AD_CMPNY);
					apCheck.setApCheckBatch(apCheckBatch);
					
				} catch (FinderException ex) {
					
					// create new batch and validate
					
					apCheckBatch = apCheckBatchHome.create(CB_NM, "CHECKBATCH" + CB_NM, "OPEN", "MISC", 
							apCheck.getChkDateCreated(), apCheck.getChkCreatedBy(), "", AD_BRNCH, AD_CMPNY);
					apCheckBatch.addApCheck(apCheck);
					
				}
				
				// remove all distribution records
				
				Collection apDistributionRecords = apCheck.getApDistributionRecords();
				
				Iterator iDr = apDistributionRecords.iterator();     	  
				
				while (iDr.hasNext()) {
					
					LocalApDistributionRecord apDistributionRecord = (LocalApDistributionRecord)i.next();
					
					iDr.remove();
					
					apDistributionRecord.remove();
					
				}      

				// release vps locks and remove all applied vouchers 
				
				Collection apAppliedVouchers = apCheck.getApAppliedVouchers();
				
				iDr = apAppliedVouchers.iterator();     	  
				
				while (iDr.hasNext()) {
					
					LocalApAppliedVoucher apAppliedVoucher = (LocalApAppliedVoucher)iDr.next();
					
					apAppliedVoucher.getApVoucherPaymentSchedule().setVpsLock(EJBCommon.FALSE);
					
					iDr.remove();
					
					apAppliedVoucher.remove();
					
				}		  	    
				
				// add new applied vouchers and distribution record
				
				Integer payableAccount = null;
				double payableAmount = 0d;
 
				ArrayList avList = details.getChkAvList();
				iDr = avList.iterator();
				
				while (iDr.hasNext()) {
					
					ApModAppliedVoucherDetails mAvDetails = (ApModAppliedVoucherDetails) iDr.next();
					
					// validate if voucher exists
					
					Collection apVoucherPaymentSchedules = null;
					
					LocalApVoucher apVoucher = null;
					
					try{
						
						apVoucher = apVoucherHome.findByVouDocumentNumber(mAvDetails.getAvVpsVouDocumentNumber(),AD_CMPNY);
						
						
					} catch (FinderException ex) {
						
						throw new GlobalNoRecordFoundException("Voucher " + mAvDetails.getAvVpsVouDocumentNumber());
						
					}
					
					// check if check's supplier matches voucher's supplier 
					if (!apSupplier.getSplName().equals(apVoucher.getApSupplier().getSplName())){
						
	    				throw new GlobalRecordInvalidException(details.getChkDocumentNumber() + ":Supplier " + details.getChkSplName() + " - " + apVoucher.getApSupplier().getSplName());
						
					}
					
					if (apVoucher.getVouPosted() == EJBCommon.FALSE) {
						
						throw new GlobalTransactionAlreadyPostedException("Voucher " + mAvDetails.getAvVpsVouDocumentNumber() + " for cv # " + details.getChkDocumentNumber());
						
					}

					if (apVoucher.getVouAmountDue() == apVoucher.getVouAmountPaid()) {
						
						throw new ApVOUOverapplicationNotAllowedException(mAvDetails.getAvVpsChkDocumentNumber() + " for Voucher:" + apVoucher.getVouDocumentNumber());
						
					}
					
					
					try {
						
						apVoucherPaymentSchedules = apVoucherPaymentScheduleHome.findOpenVpsByVouDocumentNumber(mAvDetails.getAvVpsVouDocumentNumber(), AD_BRNCH, AD_CMPNY);
						
					} catch (FinderException ex) {
						
						throw new GlobalNoRecordFoundException("Voucher " + mAvDetails.getAvVpsVouDocumentNumber());
						
					}
					
					Iterator itrVPS = apVoucherPaymentSchedules.iterator();
					double totalPayment = mAvDetails.getAvApplyAmount();
					LocalApAppliedVoucher apAppliedVoucher = null;
					
					while (itrVPS.hasNext() && totalPayment > 0d ) {

						LocalApVoucherPaymentSchedule apVoucherPaymentSchedule= (LocalApVoucherPaymentSchedule) itrVPS.next();
						ApModAppliedVoucherDetails  mDetails = new ApModAppliedVoucherDetails();
						
						Collection listAV = apVoucherPaymentSchedule.getApAppliedVouchers();
						Iterator iAV = listAV.iterator();
			        	double APPLY_AMNT = 0d;
			        	double DSCNT_AMNT = 0d;
			        	
			        	while (iAV.hasNext()) {
			        		
			        		LocalApAppliedVoucher existingApAppliedVoucher = (LocalApAppliedVoucher) iAV.next();
			        		
			        		if(existingApAppliedVoucher.getApCheck().getChkVoidPosted() == EJBCommon.FALSE) {
			        			
			        			APPLY_AMNT = APPLY_AMNT + existingApAppliedVoucher.getAvApplyAmount();        		
			        			DSCNT_AMNT = DSCNT_AMNT + existingApAppliedVoucher.getAvDiscountAmount();
			        			
			        		}
			        		
			        	}

		          		if (itrVPS.hasNext() && totalPayment > apVoucherPaymentSchedule.getVpsAmountDue() - apVoucherPaymentSchedule.getVpsAmountPaid() - APPLY_AMNT - DSCNT_AMNT) {
		          			
		          			mDetails.setAvApplyAmount(apVoucherPaymentSchedule.getVpsAmountDue() - apVoucherPaymentSchedule.getVpsAmountPaid() - APPLY_AMNT - DSCNT_AMNT);

		          		} else {
		          			
		          			mDetails.setAvApplyAmount(totalPayment);
		          			mDetails.setAvDiscountAmount(mAvDetails.getAvDiscountAmount());
		          			
		          		}

						if(mDetails.getAvApplyAmount() == 0) {
							
							continue;
							
						}
		          		
		          		totalPayment = totalPayment - mDetails.getAvApplyAmount();
		          		mDetails.setAvVpsCode(apVoucherPaymentSchedule.getVpsCode());
						mDetails.setAvVpsVouDocumentNumber(mAvDetails.getAvVpsVouDocumentNumber());
						mDetails.setAvVpsChkDocumentNumber(mAvDetails.getAvVpsChkDocumentNumber());
						
						apAppliedVoucher = this.addApAvEntry(mDetails, apCheck, apCheckBatch, AD_CMPNY);
						
						// get payable account
						
						LocalApDistributionRecord apDistributionRecord = 
							apDistributionRecordHome.findByDrClassAndVouCode("PAYABLE", apAppliedVoucher.getApVoucherPaymentSchedule().getApVoucher().getVouCode(), AD_CMPNY);
						
						// check if applied voucher is a functional currency
						
						if (apAppliedVoucher.getApVoucherPaymentSchedule().getApVoucher().getGlFunctionalCurrency().getFcCode().equals(
								adCompany.getGlFunctionalCurrency().getFcCode()) &&
								apAppliedVoucher.getApVoucherPaymentSchedule().getApVoucher().getGlFunctionalCurrency().getFcCode().equals(
										apCheck.getGlFunctionalCurrency().getFcCode()) &&
										(payableAccount == null || apDistributionRecord.getGlChartOfAccount().getCoaCode().equals(payableAccount))) {
							
							payableAccount = apDistributionRecord.getGlChartOfAccount().getCoaCode();
							payableAmount += (apAppliedVoucher.getAvApplyAmount() + apAppliedVoucher.getAvTaxWithheld() + apAppliedVoucher.getAvDiscountAmount());
							
						} else {	      	  	          	    
							
							this.addApDrEntry(apCheck.getApDrNextLine(), "PAYABLE", EJBCommon.TRUE,
									apAppliedVoucher.getAvApplyAmount() + apAppliedVoucher.getAvTaxWithheld() + apAppliedVoucher.getAvDiscountAmount(),
									EJBCommon.FALSE,
									apDistributionRecord.getGlChartOfAccount().getCoaCode(),
									apCheck, apAppliedVoucher, AD_CMPNY);
							
						}
						
						// create discount distribution records if necessary
						
						if (apAppliedVoucher.getAvDiscountAmount() != 0) {
							
							short precisionUnit = this.getGlFcPrecisionUnit(AD_CMPNY);
							
							// get discount percent
							
							double DISCOUNT_PERCENT = EJBCommon.roundIt(apAppliedVoucher.getAvDiscountAmount() / (apAppliedVoucher.getAvApplyAmount() + apAppliedVoucher.getAvTaxWithheld() + apAppliedVoucher.getAvDiscountAmount()), (short)6);
							DISCOUNT_PERCENT = EJBCommon.roundIt(DISCOUNT_PERCENT * ((apAppliedVoucher.getAvApplyAmount() + apAppliedVoucher.getAvTaxWithheld() + apAppliedVoucher.getAvDiscountAmount()) / apAppliedVoucher.getApVoucherPaymentSchedule().getApVoucher().getVouAmountDue()), (short)6);
							
							Collection apDiscountDistributionRecords = apAppliedVoucher.getApVoucherPaymentSchedule().getApVoucher().getApDistributionRecords();
							
							// get total debit and credit for rounding difference calculation
							
							double TOTAL_DEBIT = 0d;
							double TOTAL_CREDIT = 0d;
							boolean isRoundingDifferenceCalculated = false;
							
							Iterator j = apDiscountDistributionRecords.iterator();
							
							while (j.hasNext()) {
								
								LocalApDistributionRecord apDiscountDistributionRecord = (LocalApDistributionRecord)j.next();
								
								if (apDiscountDistributionRecord.getDrDebit() == EJBCommon.TRUE) {
									
									TOTAL_DEBIT += EJBCommon.roundIt(apDiscountDistributionRecord.getDrAmount() * DISCOUNT_PERCENT, precisionUnit);
									
								} else {
									
									TOTAL_CREDIT += EJBCommon.roundIt(apDiscountDistributionRecord.getDrAmount() * DISCOUNT_PERCENT, precisionUnit);
									
								}
								
							}
							
							j = apDiscountDistributionRecords.iterator();
							
							while (j.hasNext()) {
								
								LocalApDistributionRecord apDiscountDistributionRecord = (LocalApDistributionRecord)j.next();	 
								
								if (apDiscountDistributionRecord.getDrClass().equals("PAYABLE")) continue;       		
								
								double DR_AMNT = EJBCommon.roundIt(apDiscountDistributionRecord.getDrAmount() * DISCOUNT_PERCENT, precisionUnit);
								
								// calculate rounding difference if necessary    		        		
								
								if (apDiscountDistributionRecord.getDrDebit() == EJBCommon.TRUE &&
										TOTAL_DEBIT != TOTAL_CREDIT && !isRoundingDifferenceCalculated) {
									
									DR_AMNT = DR_AMNT + TOTAL_CREDIT - TOTAL_DEBIT;        		    
									
									isRoundingDifferenceCalculated = true; 	
									
								}
								
								this.addApDrEntry(apCheck.getApDrNextLine(), apDiscountDistributionRecord.getDrClass(),
										apDiscountDistributionRecord.getDrDebit() == EJBCommon.TRUE ? EJBCommon.FALSE : EJBCommon.TRUE,
												DR_AMNT,
												EJBCommon.FALSE,
												apDiscountDistributionRecord.getGlChartOfAccount().getCoaCode(), apCheck, apAppliedVoucher, AD_CMPNY);
								
								
							}
							
						}

					}   
					
					// create tax withheld distribution record if necessary
					
					if (mAvDetails.getAvTaxWithheld() > 0) {
						
						this.addApDrEntry(apCheck.getApDrNextLine(), "W-TAX", EJBCommon.FALSE, 
								apAppliedVoucher.getAvTaxWithheld(), EJBCommon.FALSE, 
								apAppliedVoucher.getApVoucherPaymentSchedule().getApVoucher().getApWithholdingTaxCode().getGlChartOfAccount().getCoaCode(),
								apCheck, apAppliedVoucher, AD_CMPNY);
						
					}

				}

				if (payableAccount != null) {
					
					this.addApDrEntry(apCheck.getApDrNextLine(), "PAYABLE", EJBCommon.TRUE,
							payableAmount, EJBCommon.FALSE, payableAccount,
							apCheck, null, AD_CMPNY);
					
				}

				// create cash distribution record
				
				this.addApDrEntry(apCheck.getApDrNextLine(), "CASH", EJBCommon.FALSE, 
						apCheck.getChkAmount(), EJBCommon.FALSE, apCheck.getAdBankAccount().getBaCoaGlCashAccount(),
						apCheck, null, AD_CMPNY);
				
			}				

		} catch (GlobalDocumentNumberNotUniqueException ex) {
			
			getSessionContext().setRollbackOnly();
			throw ex;
			
		} catch (ApCHKCheckNumberNotUniqueException ex) {
			
			getSessionContext().setRollbackOnly();
			throw ex;
			
		} catch (ApCHKVoucherHasNoWTaxCodeException ex) {
			
			getSessionContext().setRollbackOnly();
			throw ex;
			
		} catch (GlobalNoRecordFoundException ex) {
			
			getSessionContext().setRollbackOnly();
			throw ex;	   		    

		} catch (GlobalJournalNotBalanceException ex) {
			
			getSessionContext().setRollbackOnly();
			throw ex;			

        } catch (ApVOUOverapplicationNotAllowedException ex) {        	
        	
        	getSessionContext().setRollbackOnly();
        	throw ex;	   		    

        } catch (GlobalRecordInvalidException ex) {        	
        	
        	getSessionContext().setRollbackOnly();
        	throw ex;	   		    

        } catch (GlobalTransactionAlreadyLockedException ex) {        	
        	
        	getSessionContext().setRollbackOnly();
        	throw ex;

        } catch (GlobalTransactionAlreadyPostedException ex) {        	
        	
        	getSessionContext().setRollbackOnly();
        	throw ex;			

		} catch (Exception ex) {
			
			Debug.printStackTrace(ex);
			getSessionContext().setRollbackOnly();
			throw new EJBException(ex.getMessage());
			
		}
		
		
	}
	
	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public short getGlFcPrecisionUnit(Integer AD_CMPNY) {
		
		Debug.print("ApCheckImportControllerBean getGlFcPrecisionUnit");
		
		
		LocalAdCompanyHome adCompanyHome = null;
		
		
		// Initialize EJB Home
		
		try {
			
			adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);       
			
		} catch (NamingException ex) {
			
			throw new EJBException(ex.getMessage());
			
		}
		
		try {
			
			LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
			
			return  adCompany.getGlFunctionalCurrency().getFcPrecision();
			
		} catch (Exception ex) {
			
			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());
			
		}
		
	}        
	
	// private methods
	
	private void addApDrEntry(short DR_LN, String DR_CLSS,
			byte DR_DBT, double DR_AMNT, byte DR_RVRSD, Integer COA_CODE, LocalApCheck apCheck,
			LocalApAppliedVoucher apAppliedVoucher, Integer AD_CMPNY) {
		
		Debug.print("ApCheckImportControllerBean addApDrEntry");
		
		LocalApDistributionRecordHome apDistributionRecordHome = null;        
		LocalGlChartOfAccountHome glChartOfAccountHome = null;
		LocalAdCompanyHome adCompanyHome = null;
		
		
		// Initialize EJB Home
		
		try {
			
			apDistributionRecordHome = (LocalApDistributionRecordHome)EJBHomeFactory.
			lookUpLocalHome(LocalApDistributionRecordHome.JNDI_NAME, LocalApDistributionRecordHome.class);            
			glChartOfAccountHome = (LocalGlChartOfAccountHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlChartOfAccountHome.JNDI_NAME, LocalGlChartOfAccountHome.class);            
			adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);            
			
			
		} catch (NamingException ex) {
			
			throw new EJBException(ex.getMessage());
			
		}            
		
		try {
			
			// get company
			
			LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
			
			LocalGlChartOfAccount glChartOfAccount = glChartOfAccountHome.findByPrimaryKey(COA_CODE);
			
			// create distribution record 
			
			LocalApDistributionRecord apDistributionRecord = apDistributionRecordHome.create(
					DR_LN, DR_CLSS, EJBCommon.roundIt(DR_AMNT, adCompany.getGlFunctionalCurrency().getFcPrecision()),
					DR_DBT, EJBCommon.FALSE, DR_RVRSD, AD_CMPNY);
			
			apCheck.addApDistributionRecord(apDistributionRecord);
			glChartOfAccount.addApDistributionRecord(apDistributionRecord);		   
			
			// to be used by gl journal interface for cross currency receipts
			if (apAppliedVoucher != null) {
				
				apAppliedVoucher.addApDistributionRecord(apDistributionRecord);
				
			}
			
		} catch (Exception ex) {
			
			Debug.printStackTrace(ex);
			getSessionContext().setRollbackOnly();
			throw new EJBException(ex.getMessage());
			
		}
		
	}		
	

    
    private LocalApAppliedVoucher addApAvEntry(ApModAppliedVoucherDetails mdetails, LocalApCheck apCheck, LocalApCheckBatch apCheckBatch, Integer AD_CMPNY) 
        throws ApVOUOverapplicationNotAllowedException,
        GlobalTransactionAlreadyLockedException,
		ApCHKVoucherHasNoWTaxCodeException {
			
		Debug.print("ApCheckImportControllerBean addApAvEntry");
		
		LocalApAppliedVoucherHome apAppliedVoucherHome = null;        
		LocalApVoucherPaymentScheduleHome apVoucherPaymentScheduleHome = null;
		LocalAdCompanyHome adCompanyHome = null;
           
                
        // Initialize EJB Home
        
        try {
            
            apAppliedVoucherHome = (LocalApAppliedVoucherHome)EJBHomeFactory.
                lookUpLocalHome(LocalApAppliedVoucherHome.JNDI_NAME, LocalApAppliedVoucherHome.class);            
            apVoucherPaymentScheduleHome = (LocalApVoucherPaymentScheduleHome)EJBHomeFactory.
                lookUpLocalHome(LocalApVoucherPaymentScheduleHome.JNDI_NAME, LocalApVoucherPaymentScheduleHome.class);            
            adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);            
                        
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }            
                
        try {
        	
        	// get company
        	
        	LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
        	
        	// get functional currency name
        	
        	String FC_NM = adCompany.getGlFunctionalCurrency().getFcName();
        	        	
        	
        	// validate overapplication
        	
        	LocalApVoucherPaymentSchedule apVoucherPaymentSchedule = 
        	    apVoucherPaymentScheduleHome.findByPrimaryKey(mdetails.getAvVpsCode());
        	

        	// validate if vps already locked
        	
        	if (apVoucherPaymentSchedule.getVpsLock() == EJBCommon.TRUE) {
        		
				Collection apChecks = apCheckBatch.getApChecks();
				
				Iterator i = apChecks.iterator();
				
				boolean found = false;
				while(i.hasNext()) {
					
					LocalApCheck existingApCheck = (LocalApCheck)i.next();
					
					Collection apAppliedVouchers = existingApCheck.getApAppliedVouchers();
					
					Iterator j = apAppliedVouchers.iterator();
					
					while(j.hasNext()) {
						
						LocalApAppliedVoucher apAppliedVoucher = (LocalApAppliedVoucher)j.next();
						
						if(apAppliedVoucher.getApVoucherPaymentSchedule().getApVoucher().getVouDocumentNumber().equals(mdetails.getAvVpsVouDocumentNumber()))
							found = true;
						
					}
					
				}
				
				if(found == false) {
					
					throw new GlobalTransactionAlreadyLockedException(apVoucherPaymentSchedule.getApVoucher().getVouDocumentNumber() + "-" + apVoucherPaymentSchedule.getVpsNumber());
					
				}
				
			}

        	Collection list = apVoucherPaymentSchedule.getApAppliedVouchers();
        	
        	Iterator i = list.iterator();
        	double APPLY_AMNT = 0d;
    		double TX_WTHHLD = 0d;
    		double DSCNT_AMNT = 0d;
        	
        	while (i.hasNext()) {
        		
        		LocalApAppliedVoucher apAppliedVoucher = (LocalApAppliedVoucher) i.next();
        		
        		if(apAppliedVoucher.getApCheck().getChkVoidPosted() == EJBCommon.FALSE) {
        			
        			APPLY_AMNT = APPLY_AMNT + apAppliedVoucher.getAvApplyAmount();
        			TX_WTHHLD = TX_WTHHLD + apAppliedVoucher.getAvTaxWithheld();
        			DSCNT_AMNT = DSCNT_AMNT + apAppliedVoucher.getAvDiscountAmount();
        			
        		}
        		
        	}
        	    
    		APPLY_AMNT = APPLY_AMNT + mdetails.getAvApplyAmount();
    		TX_WTHHLD = TX_WTHHLD + mdetails.getAvTaxWithheld();
    		DSCNT_AMNT = DSCNT_AMNT + mdetails.getAvDiscountAmount();
        	
    		System.out.println(apVoucherPaymentSchedule.getVpsAmountDue() - apVoucherPaymentSchedule.getVpsAmountPaid());
    		System.out.println(APPLY_AMNT + TX_WTHHLD + DSCNT_AMNT);
    		
        	if (EJBCommon.roundIt(apVoucherPaymentSchedule.getVpsAmountDue() - apVoucherPaymentSchedule.getVpsAmountPaid(), this.getGlFcPrecisionUnit(AD_CMPNY)) <
        	    EJBCommon.roundIt(APPLY_AMNT + TX_WTHHLD + DSCNT_AMNT, this.getGlFcPrecisionUnit(AD_CMPNY))) {
        	    	
        	    throw new ApVOUOverapplicationNotAllowedException(mdetails.getAvVpsChkDocumentNumber() + " for Voucher:" + apVoucherPaymentSchedule.getApVoucher().getVouDocumentNumber());
        	    	
        	}
        	
        	
        	// validate voucher wtax code if necessary
        	
        	if (mdetails.getAvTaxWithheld() > 0 && 
        	    apVoucherPaymentSchedule.getApVoucher().getApWithholdingTaxCode().getGlChartOfAccount() == null) {
        		
        		throw new ApCHKVoucherHasNoWTaxCodeException(apVoucherPaymentSchedule.getApVoucher().getVouDocumentNumber() + "-" + apVoucherPaymentSchedule.getVpsNumber());
        		
        	}
        	
        	double AV_FRX_GN_LSS = 0d;
        	
        	if (!FC_NM.equals(apVoucherPaymentSchedule.getApVoucher().getGlFunctionalCurrency().getFcName()) ||
        	    !FC_NM.equals(apCheck.getGlFunctionalCurrency().getFcName())) {
        	   
        	    double AV_ALLCTD_PYMNT_AMNT = this.convertForeignToFunctionalCurrency(apCheck.getGlFunctionalCurrency().getFcCode(),
        	                                       apCheck.getGlFunctionalCurrency().getFcName(),
        	                                       apCheck.getChkConversionDate(),
        	                                       apCheck.getChkConversionRate(),
        	                                       mdetails.getAvAllocatedPaymentAmount(), AD_CMPNY);
        	                                       
        	    double AV_APPLY_AMNT = this.convertForeignToFunctionalCurrency(apVoucherPaymentSchedule.getApVoucher().getGlFunctionalCurrency().getFcCode(),
        	                                       apVoucherPaymentSchedule.getApVoucher().getGlFunctionalCurrency().getFcName(),
        	                                       apVoucherPaymentSchedule.getApVoucher().getVouConversionDate(),
        	                                       apVoucherPaymentSchedule.getApVoucher().getVouConversionRate(),
        	                                       mdetails.getAvApplyAmount(), AD_CMPNY);
        	                                       
        	    double AV_TX_WTHHLD = this.convertForeignToFunctionalCurrency(apVoucherPaymentSchedule.getApVoucher().getGlFunctionalCurrency().getFcCode(),
        	                                       apVoucherPaymentSchedule.getApVoucher().getGlFunctionalCurrency().getFcName(),
        	                                       apVoucherPaymentSchedule.getApVoucher().getVouConversionDate(),
        	                                       apVoucherPaymentSchedule.getApVoucher().getVouConversionRate(),
        	                                       mdetails.getAvTaxWithheld(), AD_CMPNY);
        	                                       
        	    double AV_DSCNT_AMNT = this.convertForeignToFunctionalCurrency(apVoucherPaymentSchedule.getApVoucher().getGlFunctionalCurrency().getFcCode(),
        	                                       apVoucherPaymentSchedule.getApVoucher().getGlFunctionalCurrency().getFcName(),
        	                                       apVoucherPaymentSchedule.getApVoucher().getVouConversionDate(),
        	                                       apVoucherPaymentSchedule.getApVoucher().getVouConversionRate(),
        	                                       mdetails.getAvDiscountAmount(), AD_CMPNY);
        	    	
        	    AV_FRX_GN_LSS = EJBCommon.roundIt((AV_ALLCTD_PYMNT_AMNT + AV_TX_WTHHLD + AV_DSCNT_AMNT) -
        	                    (AV_APPLY_AMNT + AV_TX_WTHHLD + AV_DSCNT_AMNT), this.getGlFcPrecisionUnit(AD_CMPNY));
        	    
        	}
		    
		    // create applied voucher
		    
		    LocalApAppliedVoucher apAppliedVoucher = apAppliedVoucherHome.create(mdetails.getAvApplyAmount(),
		        mdetails.getAvTaxWithheld(), mdetails.getAvDiscountAmount(), mdetails.getAvAllocatedPaymentAmount(), AV_FRX_GN_LSS, AD_CMPNY);
		        
		    apCheck.addApAppliedVoucher(apAppliedVoucher);
		    apVoucherPaymentSchedule.addApAppliedVoucher(apAppliedVoucher);		    		    
		    
		    // lock voucher
		    
		    apVoucherPaymentSchedule.setVpsLock(EJBCommon.TRUE);

		    return apAppliedVoucher;
		    		    		    		    
		} catch (ApVOUOverapplicationNotAllowedException ex) {        	
        	
        	getSessionContext().setRollbackOnly();
        	throw ex;	
        	
        } catch (GlobalTransactionAlreadyLockedException ex) {        	
        	
        	getSessionContext().setRollbackOnly();
        	throw ex;	
        	
        } catch (ApCHKVoucherHasNoWTaxCodeException ex) {        	
        	
        	getSessionContext().setRollbackOnly();
        	throw ex;	   		    
        	
        } catch (Exception ex) {
        	
        	Debug.printStackTrace(ex);
        	getSessionContext().setRollbackOnly();
        	throw new EJBException(ex.getMessage());
        	
        }
		
	}
	
	private double convertForeignToFunctionalCurrency(Integer FC_CODE, String FC_NM, 
			Date CONVERSION_DATE, double CONVERSION_RATE, double AMOUNT, Integer AD_CMPNY) {
		
		Debug.print("ApCheckImportControllerBean convertForeignToFunctionalCurrency");
		
		
		LocalGlFunctionalCurrencyRateHome glFunctionalCurrencyRateHome = null;
		LocalAdCompanyHome adCompanyHome = null;
		
		LocalAdCompany adCompany = null;
		
		// Initialize EJB Homes
		
		try {
			
			glFunctionalCurrencyRateHome = (LocalGlFunctionalCurrencyRateHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlFunctionalCurrencyRateHome.JNDI_NAME, LocalGlFunctionalCurrencyRateHome.class);                         
			adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);             
			
		} catch (NamingException ex) {
			
			throw new EJBException(ex.getMessage());
			
		}
		
		// get company and extended precision
		
		try {
			
			adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
			
		} catch (Exception ex) {
			
			throw new EJBException(ex.getMessage());
			
		}	     
		
		
		// Convert to functional currency if necessary
		
		if (CONVERSION_RATE != 1 && CONVERSION_RATE != 0 ) {
			
			AMOUNT = AMOUNT / CONVERSION_RATE;
			
		}      
			
		
		return EJBCommon.roundIt(AMOUNT, adCompany.getGlFunctionalCurrency().getFcPrecision());
		
	}
	
    private ApModCheckDetails validate(ApModCheckDetails mDetails) throws
    GlobalRecordInvalidException,
	GlobalJournalNotBalanceException {
    	
    	Debug.print("ApCheckImportControllerBean validate");

    	try {
    		
        	String bank = null;
        	String checkNumber = null;
        	double TOTAL_AMOUNT = 0d;
        	
        	ArrayList list = mDetails.getChkAvList();
        	
        	Iterator i = list.iterator();
    		
    		while(i.hasNext()){
    			
    			ApModAppliedVoucherDetails details =  (ApModAppliedVoucherDetails) i.next();
    			
    			if (bank == null) {
    				
    				bank = details.getAvVpsChkBankAccount();

    			} else if (!details.getAvVpsChkBankAccount().equals(bank)) {
    				
    				throw new GlobalRecordInvalidException(details.getAvVpsChkDocumentNumber() + ":Bank Account " + bank + " - " + details.getAvVpsChkBankAccount());
    				
    			}

    			if (checkNumber == null) {
    				
    				checkNumber = details.getAvVpsChkCheckNumber();
    				
    			} else if (!details.getAvVpsChkCheckNumber().equals(checkNumber)) {
    				
    				throw new GlobalRecordInvalidException(details.getAvVpsChkDocumentNumber() + ":Check Number " + checkNumber + " - " + details.getAvVpsChkCheckNumber());
    				
    			}

				TOTAL_AMOUNT = TOTAL_AMOUNT + details.getAvApplyAmount();
    			
    		}
    		
			if (TOTAL_AMOUNT != mDetails.getChkAmount()) {
				
				throw new GlobalJournalNotBalanceException(mDetails.getChkDocumentNumber());
				
			}
    		
    		mDetails.setChkNumber(checkNumber);
    		mDetails.setChkBaName(bank);
    		
        	return mDetails;
    		
    	} catch (GlobalRecordInvalidException ex){
    		
    		throw ex;
    		
    	} catch (GlobalJournalNotBalanceException ex) {
    		
    		throw ex;
    		
    	}

    }    
    
	// SessionBean methods
	
	/**
	 * @ejb:create-method view-type="remote"
	 **/
	public void ejbCreate() throws CreateException {
		
		Debug.print("ApCheckImportControllerBean ejbCreate");
		
	}
}
