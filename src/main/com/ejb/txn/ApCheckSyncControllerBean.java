
/*
 * ApCheckSyncControllerBean
 *
 * Created on January 2008
 *
 * @author  Cybon
 */

package com.ejb.txn;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.StringTokenizer;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;
import javax.ejb.SessionBean;
import javax.ejb.SessionContext;
import javax.naming.NamingException;

import com.ejb.ad.LocalAdBankAccount;
import com.ejb.ad.LocalAdBankAccountHome;
import com.ejb.ad.LocalAdBranch;
import com.ejb.ad.LocalAdBranchDocumentSequenceAssignment;
import com.ejb.ad.LocalAdBranchDocumentSequenceAssignmentHome;
import com.ejb.ad.LocalAdBranchHome;
import com.ejb.ad.LocalAdBranchItemLocation;
import com.ejb.ad.LocalAdBranchItemLocationHome;
import com.ejb.ad.LocalAdCompany;
import com.ejb.ad.LocalAdCompanyHome;
import com.ejb.ad.LocalAdPreference;
import com.ejb.ad.LocalAdPreferenceHome;
import com.ejb.ap.LocalApSupplier;
import com.ejb.ap.LocalApSupplierHome;
import com.ejb.ap.LocalApTaxCode;
import com.ejb.ap.LocalApTaxCodeHome;
import com.ejb.ap.LocalApVoucherLineItem;
import com.ejb.ap.LocalApWithholdingTaxCode;
import com.ejb.ap.LocalApWithholdingTaxCodeHome;
import com.ejb.ar.LocalArCustomer;
import com.ejb.ar.LocalArCustomerHome;
import com.ejb.ar.LocalArDistributionRecord;
import com.ejb.ar.LocalArDistributionRecordHome;
import com.ejb.ar.LocalArTaxCode;
import com.ejb.ar.LocalArTaxCodeHome;
import com.ejb.ar.LocalArWithholdingTaxCode;
import com.ejb.ar.LocalArWithholdingTaxCodeHome;
import com.ejb.cm.LocalCmFundTransferHome;
import com.ejb.exception.GlobalBranchAccountNumberInvalidException;
import com.ejb.gl.LocalAdDocumentSequenceAssignment;
import com.ejb.gl.LocalAdDocumentSequenceAssignmentHome;
import com.ejb.gl.LocalGlChartOfAccount;
import com.ejb.gl.LocalGlChartOfAccountHome;
import com.ejb.gl.LocalGlFunctionalCurrency;
import com.ejb.gl.LocalGlFunctionalCurrencyHome;
import com.ejb.inv.LocalInvUnitOfMeasure;

import com.ejb.ap.LocalApCheckBatch;
import com.ejb.ap.LocalApCheckBatchHome;
import com.ejb.ap.LocalApCheckHome;
import com.ejb.ap.LocalApDistributionRecordHome;
import com.ejb.ap.LocalApCheck;
import com.ejb.ap.LocalApDistributionRecord;
import com.util.ApModCheckDetails;
import com.util.ApModDistributionRecordDetails;

import com.ejb.ap.LocalApSupplier;
import com.ejb.ap.LocalApSupplierHome;

import com.util.Debug;
import com.util.EJBCommon;
import com.util.EJBHomeFactory;
import com.util.AbstractSessionBean;

/**
 * @ejb:bean name="ApCheckSyncControllerBean"
 *           type="Stateless"
 *           view-type="service-endpoint"
 *           
 * @wsee:port-component name="ApCheckSync"
 *
 * @jboss:port-component uri="omega-ejb/ApCheckSyncWS"
 *
 * @ejb:interface service-endpoint-class="com.ejb.txn.ApCheckSyncWS"
 * 
 */

public class ApCheckSyncControllerBean implements SessionBean {
	
	private SessionContext ctx;
	
	/**
	 * @ejb:interface-method
	 **/
	public int setApCheckAllNewAndVoid(String[] newChk, String CB_NM, String BR_BRNCH_CODE, Integer AD_CMPNY) {               
		
		Debug.print("ApCheckSyncControllerBean setCheckAllNewAndVoid");  
	
		LocalAdBranchHome adBranchHome = null;
		//LocalInvCostingHome invCostingHome = null; 
		//LocalAdBranchItemLocationHome adBranchItemLocationHome = null;
		//LocalInvItemLocationHome invItemLocationHome = null;
		//LocalInvItemHome invItemHome = null;
		//LocalAdPreferenceHome adPreferenceHome = null;
		LocalApCheckHome apCheckHome = null;
		LocalApDistributionRecordHome apDistributionRecordHome = null;
		LocalAdDocumentSequenceAssignmentHome adDocumentSequenceAssignmentHome = null;
		LocalAdBranchDocumentSequenceAssignmentHome adBranchDocumentSequenceAssignmentHome = null;
		LocalApCheckBatchHome apCheckBatchHome = null;
		LocalApSupplierHome apSupplierHome= null;
		LocalAdBankAccountHome adBankAccountHome = null;
        LocalApTaxCodeHome apTaxCodeHome = null;
        LocalApWithholdingTaxCodeHome apWithholdingTaxCodeHome = null;
        //LocalArCustomerHome arCustomerHome = null;
        LocalGlFunctionalCurrencyHome glFunctionalCurrencyHome = null;
        LocalGlChartOfAccountHome glChartOfAccountHome = null;
        //LocalArCheckBatchHome arCheckBatchHome = null;
        //LocalArInvoiceLineItemHome arInvoiceLineItemHome = null;
        //LocalInvUnitOfMeasureHome invUnitOfMeasureHome = null;
        //LocalCmFundTransferHome cmFundTransferHome = null;
        //LocalInvLocationHome invLocationHome = null;
        
        
		
		
		try {
			adBranchHome = (LocalAdBranchHome)EJBHomeFactory.lookUpLocalHome(LocalAdBranchHome.JNDI_NAME, LocalAdBranchHome.class);
			apCheckHome=(LocalApCheckHome)EJBHomeFactory.lookUpLocalHome(LocalApCheckHome.JNDI_NAME, LocalApCheckHome.class);			
			apDistributionRecordHome=(LocalApDistributionRecordHome)EJBHomeFactory.lookUpLocalHome(LocalApDistributionRecordHome.JNDI_NAME, LocalApDistributionRecordHome.class);
			adDocumentSequenceAssignmentHome = (LocalAdDocumentSequenceAssignmentHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdDocumentSequenceAssignmentHome.JNDI_NAME, LocalAdDocumentSequenceAssignmentHome.class);			
			apCheckBatchHome = (LocalApCheckBatchHome)EJBHomeFactory.
			lookUpLocalHome(LocalApCheckBatchHome.JNDI_NAME, LocalApCheckBatchHome.class);
			apSupplierHome = (LocalApSupplierHome)EJBHomeFactory.
			lookUpLocalHome(LocalApSupplierHome.JNDI_NAME, LocalApSupplierHome.class);
			adBankAccountHome = (LocalAdBankAccountHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdBankAccountHome.JNDI_NAME, LocalAdBankAccountHome.class);
			apTaxCodeHome = (LocalApTaxCodeHome)EJBHomeFactory.
			lookUpLocalHome(LocalApTaxCodeHome.JNDI_NAME, LocalApTaxCodeHome.class);
			apWithholdingTaxCodeHome = (LocalApWithholdingTaxCodeHome)EJBHomeFactory.
			lookUpLocalHome(LocalApWithholdingTaxCodeHome.JNDI_NAME, LocalApWithholdingTaxCodeHome.class);
			glChartOfAccountHome = (LocalGlChartOfAccountHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlChartOfAccountHome.JNDI_NAME, LocalGlChartOfAccountHome.class);
			//glJournalCategoryHome=(LocalGlJournalCategoryHome)EJBHomeFactory.lookUpLocalHome(LocalGlJournalCategoryHome.JNDI_NAME, LocalGlJournalCategoryHome.class);			
			//glJournalSourceHome=(LocalGlJournalSourceHome)EJBHomeFactory.lookUpLocalHome(LocalGlJournalSourceHome.JNDI_NAME, LocalGlJournalSourceHome.class);			
			glFunctionalCurrencyHome=(LocalGlFunctionalCurrencyHome)EJBHomeFactory.lookUpLocalHome(LocalGlFunctionalCurrencyHome.JNDI_NAME, LocalGlFunctionalCurrencyHome.class);			
			//glJournalBatchHome=(LocalGlJournalBatchHome)EJBHomeFactory.lookUpLocalHome(LocalGlJournalBatchHome.JNDI_NAME, LocalGlJournalBatchHome.class);			
			adDocumentSequenceAssignmentHome = (LocalAdDocumentSequenceAssignmentHome)EJBHomeFactory.lookUpLocalHome(LocalAdDocumentSequenceAssignmentHome.JNDI_NAME, LocalAdDocumentSequenceAssignmentHome.class);				
			
			
		} catch (NamingException ex) {
			
			throw new EJBException(ex.getMessage());
			
		}
		
		
		try {
		    		    		
			int success = 0;
				
			LocalAdBranch adBranch = adBranchHome.findByBrBranchCode(BR_BRNCH_CODE, AD_CMPNY);
								
				for (int i = 0; i < newChk.length; i++) {
					
					ApModCheckDetails apModCheckDetails = checkDecode(newChk[i]);
					
            		LocalApCheck apCheck = apCheckHome.create(
            				apModCheckDetails.getChkType(),
            				apModCheckDetails.getChkDescription(),
            				apModCheckDetails.getChkDate(),
            				apModCheckDetails.getChkCheckDate(),
            				apModCheckDetails.getChkNumber(),
            				apModCheckDetails.getChkDocumentNumber(),
            				apModCheckDetails.getChkReferenceNumber(),
                                        null,
                                        null,
                                        null,
                                        null,
                                        null,
            				EJBCommon.FALSE, EJBCommon.FALSE, null, null, null, 0d, 0d, 0d, 0d, 0d,
            				EJBCommon.FALSE, EJBCommon.FALSE,
            				apModCheckDetails.getChkConversionDate(),
            				apModCheckDetails.getChkConversionRate(),
            				apModCheckDetails.getChkBillAmount(),
            				apModCheckDetails.getChkAmount(),
            				apModCheckDetails.getChkApprovalStatus(),
            				apModCheckDetails.getChkReasonForRejection(),
            				apModCheckDetails.getChkPosted(),
            				apModCheckDetails.getChkVoid(),
            				apModCheckDetails.getChkCrossCheck(),
            				apModCheckDetails.getChkVoidApprovalStatus(),
            				apModCheckDetails.getChkVoidPosted(),
            				apModCheckDetails.getChkCreatedBy(),
            				apModCheckDetails.getChkDateCreated(),
            				apModCheckDetails.getChkLastModifiedBy(),
            				apModCheckDetails.getChkDateLastModified(),
            				apModCheckDetails.getChkApprovedRejectedBy(),
            				apModCheckDetails.getChkDateApprovedRejected(),
            				apModCheckDetails.getChkPostedBy(),
            				apModCheckDetails.getChkDatePosted(),
            				apModCheckDetails.getChkReconciled(),
            				apModCheckDetails.getChkDateReconciled(),
            				apModCheckDetails.getChkReleased(),
            				apModCheckDetails.getChkDateReleased(),
            				apModCheckDetails.getChkPrinted(),
            				apModCheckDetails.getChkCvPrinted(),
            				apModCheckDetails.getChkMemo(),            				
            				apModCheckDetails.getChkSupplierName(),
            				
            				adBranch.getBrCode(),
            				AD_CMPNY
            				);
            		
            		//Foreign Keys
            		try {
            			
            			LocalApCheckBatch apCheckBatch = apCheckBatchHome.findByCbName(CB_NM, apCheck.getChkAdBranch(), apCheck.getChkAdCompany());
            			apCheck.setApCheckBatch(apCheckBatch);
            			
            		} catch (FinderException ex) {
            			Debug.print("Error Batch:"+ex.getMessage());	
            		}
            		
            		try {
            			
            			LocalApSupplier apSupplier = apSupplierHome.findBySplSupplierCode(apModCheckDetails.getChkSplSupplierCode(), apCheck.getChkAdCompany());          			            			
            			apCheck.setApSupplier(apSupplier);
            			
            		} catch (Exception ex) {
            			Debug.print("Error Supplier:"+ ex.getMessage());
            		}
            		
            		try {
            			
            			LocalAdBankAccount adBankAccount = adBankAccountHome.findByBaNameAndBrCode(apModCheckDetails.getChkBaName(), apCheck.getChkAdBranch(),AD_CMPNY);            			
            			apCheck.setAdBankAccount(adBankAccount);
            			
            		} catch (Exception ex) {
            			Debug.print("Error Bank Account:"+ ex.getMessage());
            		}
            		
            		try {
            			
            			LocalApWithholdingTaxCode apWithholdingTaxCode = apWithholdingTaxCodeHome.findByWtcName(apModCheckDetails.getChkWtcName(),AD_CMPNY);
            			apCheck.setApWithholdingTaxCode(apWithholdingTaxCode);
            			
            		} catch (Exception ex) {
            			Debug.print("Error Withholding Tax Code:"+ ex.getMessage());
            		}
            		
            		try {
            			
            			LocalApTaxCode apTaxCode = apTaxCodeHome.findByTcName(apModCheckDetails.getChkTcName(),AD_CMPNY);
            			apCheck.setApTaxCode(apTaxCode);
            			
            		} catch (Exception ex) {
            			Debug.print("Error Tax Code:"+ ex.getMessage());
            		}
            		
            		try {
            			
            			LocalGlFunctionalCurrency glFunctionalCurrency = glFunctionalCurrencyHome.findByFcName(apModCheckDetails.getChkFcName(),AD_CMPNY);
            			apCheck.setGlFunctionalCurrency(glFunctionalCurrency);
            			
            		} catch (Exception ex) {
            			Debug.print("Error Functional Currency:"+ ex.getMessage());
            		}
            		
				// Generate Expense, Tax, Withholding Tax, Cash on Check Lines
				
            		Iterator chkIter = apModCheckDetails.getChkDrList().iterator();
            		short lineNumber = 0;            		
					ApModDistributionRecordDetails apModDistributionRecordDetails = (ApModDistributionRecordDetails)chkIter.next();
					//short precisionUnit = this.getGlFcPrecisionUnit(AD_CMPNY);
					short precisionUnit = 2;					
					double DR_W_TAX_AMNT = 0d;
					double DR_TAX_AMNT = 0d;
					double DR_EXPNSE_AMT = 0d;
					double DR_CASH_AMT = 0d;
					
					apModDistributionRecordDetails.setDrAdCompany(AD_CMPNY);					
					
					if (apCheck.getApTaxCode().getTcType().equals("NONE") &&
							apCheck.getApTaxCode().getTcType().equals("EXEMPT")&&apCheck.getApTaxCode().getTcType().equals("ZERO"))
					{
						DR_EXPNSE_AMT = EJBCommon.roundIt(apModCheckDetails.getChkBillAmount(),precisionUnit);
						DR_TAX_AMNT = 0;
					}
					else if (apCheck.getApTaxCode().getTcType().equals("INCLUSIVE"))
					{
						DR_EXPNSE_AMT = EJBCommon.roundIt(apModCheckDetails.getChkBillAmount() / (1 + (apCheck.getApTaxCode().getTcRate() / 100)), precisionUnit);
						DR_TAX_AMNT = apModCheckDetails.getChkBillAmount() - DR_EXPNSE_AMT;
						
					}
					else if (apCheck.getApTaxCode().getTcType().equals("EXCLUSIVE"))
					{
						DR_TAX_AMNT = EJBCommon.roundIt(apModCheckDetails.getChkBillAmount() *apCheck.getApTaxCode().getTcRate() / 100, precisionUnit);
						DR_EXPNSE_AMT = apModCheckDetails.getChkBillAmount();
					}
					
					DR_CASH_AMT = DR_TAX_AMNT + DR_EXPNSE_AMT;
					DR_W_TAX_AMNT = EJBCommon.roundIt(DR_EXPNSE_AMT * apCheck.getApWithholdingTaxCode().getWtcRate() / 100, precisionUnit);
					DR_CASH_AMT -= DR_W_TAX_AMNT;
					
					LocalApDistributionRecord apDRTax = apDistributionRecordHome.create(++lineNumber,"TAX",DR_TAX_AMNT,(byte)1,(byte)0,(byte)0,AD_CMPNY);
					try {						
						LocalGlChartOfAccount glChartOfAccount = glChartOfAccountHome.findByCoaAccountNumber(apCheck.getApTaxCode().getGlChartOfAccount().getCoaAccountNumber(),apModDistributionRecordDetails.getDrAdCompany());
						glChartOfAccount.addApDistributionRecord(apDRTax);						
					} catch (Exception ex) {
						System.out.println("Error Chart Of Account:"+ ex.getMessage());
					}					
					apCheck.addApDistributionRecord(apDRTax);
					
					LocalApDistributionRecord apDRWithTax = apDistributionRecordHome.create(++lineNumber,"W-TAX",DR_W_TAX_AMNT,(byte)0,(byte)0,(byte)0,AD_CMPNY);
					try {
						LocalGlChartOfAccount glChartOfAccount = glChartOfAccountHome.findByCoaAccountNumber(apCheck.getApWithholdingTaxCode().getGlChartOfAccount().getCoaAccountNumber(),apModDistributionRecordDetails.getDrAdCompany());					
						glChartOfAccount.addApDistributionRecord(apDRWithTax);
					} catch (Exception ex) {
						System.out.println("Error Chart Of Account:"+ ex.getMessage());
					}
					apCheck.addApDistributionRecord(apDRWithTax);
					
					LocalApDistributionRecord apDRExpense = apDistributionRecordHome.create(++lineNumber,"EXPENSE",DR_EXPNSE_AMT,(byte)1,(byte)0,(byte)0,AD_CMPNY);
					try {
						LocalGlChartOfAccount glChartOfAccount = glChartOfAccountHome.findByCoaCode(apCheck.getApSupplier().getSplCoaGlExpenseAccount(),apModDistributionRecordDetails.getDrAdCompany());			
						glChartOfAccount.addApDistributionRecord(apDRExpense);						
					} catch (Exception ex) {
						System.out.println("Error Chart Of Account:"+ ex.getMessage());
					}
					apCheck.addApDistributionRecord(apDRExpense);
					
					LocalApDistributionRecord apDRCash = apDistributionRecordHome.create(++lineNumber,"CASH",DR_CASH_AMT,(byte)0,(byte)0,(byte)0,AD_CMPNY);
					try {
						LocalGlChartOfAccount glChartOfAccount = glChartOfAccountHome.findByCoaCode(apCheck.getAdBankAccount().getBaCoaGlCashAccount(),apModDistributionRecordDetails.getDrAdCompany());			
						glChartOfAccount.addApDistributionRecord(apDRCash);
					
					} catch (Exception ex) {
						System.out.println("Error Chart Of Account:"+ ex.getMessage());
					}
					apCheck.addApDistributionRecord(apDRCash);
				}
					
				success = 1;
				return success;
			
			
		} catch (Exception ex) {
						
			ex.printStackTrace();
			//ctx.setRollbackOnly();
			throw new EJBException (ex.getMessage());			
		}
		
		
	}    
	
	private void addApDrEntry(ApModDistributionRecordDetails mdetails, LocalApCheck apCheck, Integer AD_BRNCH, Integer AD_CMPNY) 
	throws GlobalBranchAccountNumberInvalidException {
		
		Debug.print("ApCheckSyncControllerBean addApDrEntry");
		
		LocalApDistributionRecordHome apDistributionRecordHome = null;        
		LocalGlChartOfAccountHome glChartOfAccountHome = null;
		LocalAdCompanyHome adCompanyHome = null;
		
		
		// Initialize EJB Home
		
		try {
			
			apDistributionRecordHome = (LocalApDistributionRecordHome)EJBHomeFactory.
			lookUpLocalHome(LocalApDistributionRecordHome.JNDI_NAME, LocalApDistributionRecordHome.class);            
			glChartOfAccountHome = (LocalGlChartOfAccountHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlChartOfAccountHome.JNDI_NAME, LocalGlChartOfAccountHome.class);            
			adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);            
			
			
		} catch (NamingException ex) {
			
			throw new EJBException(ex.getMessage());
			
		}            
		
		try {
			
			// get company
			
			LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
			
			// validate if coa exists
			
			LocalGlChartOfAccount glChartOfAccount = null;
			
			try {
				
				glChartOfAccount = glChartOfAccountHome.findByCoaAccountNumberAndBranchCode(mdetails.getDrCoaAccountNumber(), AD_BRNCH, AD_CMPNY);
				
				if (glChartOfAccount.getCoaEnable() == EJBCommon.FALSE) 
					throw new GlobalBranchAccountNumberInvalidException();
				
			} catch (FinderException ex) {
				
				throw new GlobalBranchAccountNumberInvalidException();
				
			}
			
			// create distribution record 
			
			LocalApDistributionRecord apDistributionRecord = apDistributionRecordHome.create(
					mdetails.getDrLine(), 
					mdetails.getDrClass(),
					EJBCommon.roundIt(mdetails.getDrAmount(), adCompany.getGlFunctionalCurrency().getFcPrecision()),
					mdetails.getDrDebit(), EJBCommon.FALSE, mdetails.getDrReversed(), AD_CMPNY);
			
			apCheck.addApDistributionRecord(apDistributionRecord);
			glChartOfAccount.addApDistributionRecord(apDistributionRecord);
			
		} catch(GlobalBranchAccountNumberInvalidException ex) {
			
			throw ex;
			
		} catch (Exception ex) {
			
			Debug.printStackTrace(ex);
			//getSessionContext().setRollbackOnly();
			throw new EJBException(ex.getMessage());
			
		}
		
	}
	
	private ApModCheckDetails checkDecode(String check) throws Exception {
		
		Debug.print("ApCheckSyncControllerBean checkDecode");
		
		String separator = "$";
		ApModCheckDetails apModCheckDetails = new ApModCheckDetails();
		
		
		// Remove first $ character
		check = check.substring(1);
		
		// check code
		int start = 0;
		int nextIndex = check.indexOf(separator, start);
		int length = nextIndex - start;
		apModCheckDetails.setChkCode(new Integer(Integer.parseInt(check.substring(start, start + length))));		
				
		// check type
		start = nextIndex + 1;
		nextIndex = check.indexOf(separator, start);
		length = nextIndex - start;
		apModCheckDetails.setChkType(check.substring(start, start + length));		
		// Description
		start = nextIndex + 1;
		nextIndex = check.indexOf(separator, start);
		length = nextIndex - start;		
		apModCheckDetails.setChkDescription(check.substring(start, start + length));
		// Date
		start = nextIndex + 1;
		nextIndex = check.indexOf(separator, start);
		length = nextIndex - start;
		SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
        sdf.setLenient(false);
        try {
        	apModCheckDetails.setChkDate(sdf.parse(check.substring(start, start + length)));
        	System.out.println("Date-" + apModCheckDetails.getChkDate());
        } catch (Exception ex) {
        	
        	throw ex;
        }
        // Check Date
		start = nextIndex + 1;
		nextIndex = check.indexOf(separator, start);
		length = nextIndex - start;		
        sdf.setLenient(false);
        try {
        	apModCheckDetails.setChkCheckDate(sdf.parse(check.substring(start, start + length)));
        	System.out.println("Date-" + apModCheckDetails.getChkCheckDate());
        } catch (Exception ex) {
        	
        	throw ex;
        }
        //  Check number
		start = nextIndex + 1;
		nextIndex = check.indexOf(separator, start);
		length = nextIndex - start;
		apModCheckDetails.setChkNumber(check.substring(start, start + length));
        //  Document number
		start = nextIndex + 1;
		nextIndex = check.indexOf(separator, start);
		length = nextIndex - start;
		apModCheckDetails.setChkDocumentNumber(check.substring(start, start + length));
        //  Reference number
		start = nextIndex + 1;
		nextIndex = check.indexOf(separator, start);
		length = nextIndex - start;
		apModCheckDetails.setChkReferenceNumber(check.substring(start, start + length));
		//  Conversion Date
		start = nextIndex + 1;
		nextIndex = check.indexOf(separator, start);
		length = nextIndex - start;		
        sdf.setLenient(false);
        try {
        	//apModCheckDetails.setChkConversionDate(sdf.parse(check.substring(start, start + length)));
        	System.out.println("Date-" + apModCheckDetails.getChkConversionDate());
        } catch (Exception ex) {
        	
        	throw ex;
        }
        //  Conversion Rate
		start = nextIndex + 1;
		nextIndex = check.indexOf(separator, start);
		length = nextIndex - start;
		apModCheckDetails.setChkConversionRate(Double.parseDouble(check.substring(start, start + length)));
		// Bill amount
		start = nextIndex + 1;
		nextIndex = check.indexOf(separator, start);
		length = nextIndex - start;
		apModCheckDetails.setChkBillAmount(Double.parseDouble(check.substring(start, start + length)));
		// check amount
		start = nextIndex + 1;
		nextIndex = check.indexOf(separator, start);
		length = nextIndex - start;
		apModCheckDetails.setChkAmount(Double.parseDouble(check.substring(start, start + length)));		
		// check approval status
		start = nextIndex + 1;
		nextIndex = check.indexOf(separator, start);
		length = nextIndex - start;
		//apModCheckDetails.setChkApprovalStatus(check.substring(start, start + length));
		// Posted
		start = nextIndex + 1;
		nextIndex = check.indexOf(separator, start);
		length = nextIndex - start;
		apModCheckDetails.setChkPosted((byte)Integer.parseInt(check.substring(start, start + length)));
		// void
		start = nextIndex + 1;
		nextIndex = check.indexOf(separator, start);
		length = nextIndex - start;
		apModCheckDetails.setChkVoid((byte)Integer.parseInt(check.substring(start, start + length)));
		// cross check
		start = nextIndex + 1;
		nextIndex = check.indexOf(separator, start);
		length = nextIndex - start;
		apModCheckDetails.setChkCrossCheck((byte)Integer.parseInt(check.substring(start, start + length)));
		// void approval status
		start = nextIndex + 1;
		nextIndex = check.indexOf(separator, start);
		length = nextIndex - start;
		//apModCheckDetails.setChkVoidApprovalStatus(check.substring(start, start + length));
		
		// void posted
		start = nextIndex + 1;
		nextIndex = check.indexOf(separator, start);
		length = nextIndex - start;
		apModCheckDetails.setChkVoidPosted((byte)Integer.parseInt(check.substring(start, start + length)));
		// reason for rejection
		start = nextIndex + 1;
		nextIndex = check.indexOf(separator, start);
		length = nextIndex - start;
		apModCheckDetails.setChkReasonForRejection(check.substring(start, start + length));
		// created by
		start = nextIndex + 1;
		nextIndex = check.indexOf(separator, start);
		length = nextIndex - start;
		apModCheckDetails.setChkCreatedBy(check.substring(start, start + length));
		// date created
		start = nextIndex + 1;
		nextIndex = check.indexOf(separator, start);
		length = nextIndex - start;		
        sdf.setLenient(false);
        try {
        	apModCheckDetails.setChkDateCreated(sdf.parse(check.substring(start, start + length)));
        	System.out.println("Date-" + apModCheckDetails.getChkConversionDate());
        } catch (Exception ex) {
        	
        	throw ex;
        }
        //  Last Modified By
		start = nextIndex + 1;
		nextIndex = check.indexOf(separator, start);
		length = nextIndex - start;
		apModCheckDetails.setChkLastModifiedBy(check.substring(start, start + length));		
		// Date Last Modified
		start = nextIndex + 1;
		nextIndex = check.indexOf(separator, start);
		length = nextIndex - start;		
        sdf.setLenient(false);
        try {
        	apModCheckDetails.setChkDateLastModified(sdf.parse(check.substring(start, start + length)));
        	System.out.println("Date-" + apModCheckDetails.getChkDateLastModified());
        } catch (Exception ex) {
        	
        	throw ex;
        }
		// Approved rejected By
		start = nextIndex + 1;
		nextIndex = check.indexOf(separator, start);
		length = nextIndex - start;
		apModCheckDetails.setChkApprovedRejectedBy(check.substring(start, start + length));
		// Date Approved Rejected
		start = nextIndex + 1;
		nextIndex = check.indexOf(separator, start);
		length = nextIndex - start;		
        sdf.setLenient(false);
        try {
        	apModCheckDetails.setChkDateApprovedRejected(sdf.parse(check.substring(start, start + length)));
        	System.out.println("Date-" + apModCheckDetails.getChkDateApprovedRejected());
        } catch (Exception ex) {
        	
        	throw ex;
        }
		// Posted By
		start = nextIndex + 1;
		nextIndex = check.indexOf(separator, start);
		length = nextIndex - start;
		apModCheckDetails.setChkPostedBy(check.substring(start, start + length));
		// Date Posted
		start = nextIndex + 1;
		nextIndex = check.indexOf(separator, start);
		length = nextIndex - start;		
        sdf.setLenient(false);
        try {
        	apModCheckDetails.setChkDatePosted(sdf.parse(check.substring(start, start + length)));
        	System.out.println("Date-" + apModCheckDetails.getChkPostedBy());
        } catch (Exception ex) {
        	
        	throw ex;
        }
		// Reconciled
		start = nextIndex + 1;
		nextIndex = check.indexOf(separator, start);
		length = nextIndex - start;
		apModCheckDetails.setChkReconciled((byte)Integer.parseInt(check.substring(start, start + length)));
		// Date Reconciled
		start = nextIndex + 1;
		nextIndex = check.indexOf(separator, start);
		length = nextIndex - start;		
        sdf.setLenient(false);
        try {
        	apModCheckDetails.setChkDateReconciled(sdf.parse(check.substring(start, start + length)));
        	System.out.println("Date-" + apModCheckDetails.getChkDateReconciled());
        } catch (Exception ex) {
        	
        	throw ex;
        }
		// Released
		start = nextIndex + 1;
		nextIndex = check.indexOf(separator, start);
		length = nextIndex - start;
		apModCheckDetails.setChkReleased((byte)Integer.parseInt(check.substring(start, start + length)));
		// Printed
		start = nextIndex + 1;
		nextIndex = check.indexOf(separator, start);
		length = nextIndex - start;
		apModCheckDetails.setChkPrinted((byte)Integer.parseInt(check.substring(start, start + length)));
		// Check Voucher Printed
		start = nextIndex + 1;
		nextIndex = check.indexOf(separator, start);
		length = nextIndex - start;
		apModCheckDetails.setChkCvPrinted((byte)Integer.parseInt(check.substring(start, start + length)));
		// Memo
		start = nextIndex + 1;
		nextIndex = check.indexOf(separator, start);
		length = nextIndex - start;
		apModCheckDetails.setChkMemo(check.substring(start, start + length));
		// Supplier Name 
		start = nextIndex + 1;
		nextIndex = check.indexOf(separator, start);
		length = nextIndex - start;
		apModCheckDetails.setChkSupplierName(check.substring(start, start + length));
				
		// AdBankAccount
		start = nextIndex + 1;
		nextIndex = check.indexOf(separator, start);
		length = nextIndex - start;
		apModCheckDetails.setChkBaName(check.substring(start, start + length));		
		
		// Supplier Code
		start = nextIndex + 1;
		nextIndex = check.indexOf(separator, start);
		length = nextIndex - start;
		apModCheckDetails.setChkSplSupplierCode(check.substring(start, start + length));
				
		// WithHolding Tax Code
		start = nextIndex + 1;
		nextIndex = check.indexOf(separator, start);
		length = nextIndex - start;	
		apModCheckDetails.setChkWtcName(check.substring(start, start + length));
		
		// Tax Code
		start = nextIndex + 1;
		nextIndex = check.indexOf(separator, start);
		length = nextIndex - start;
		apModCheckDetails.setChkTcName(check.substring(start, start + length));
		
		// Functional Currency
		start = nextIndex + 1;
		nextIndex = check.indexOf(separator, start);
		length = nextIndex - start;		
		apModCheckDetails.setChkFcName(check.substring(start, start + length));
		
		//apModCheckDetails.setChkAdCompany(AD_CMPNY);
		
		
		// end separator
		start = nextIndex + 1;
		nextIndex = check.indexOf(separator, start);
		length = nextIndex - start;
		
		String lineSeparator = "~";
		
		// begin lineSeparator
		start = nextIndex + 1;
		nextIndex = check.indexOf(lineSeparator, start);
		length = nextIndex - start;
		
		ArrayList invIliList = new ArrayList();
		
		while (true) {
			
			ApModDistributionRecordDetails apModDistributionRecordDetails = new ApModDistributionRecordDetails();
			
			// begin separator
			start = nextIndex + 1;
			nextIndex = check.indexOf(separator, start);
			length = nextIndex - start;	

			// DR Code
			start = nextIndex + 1;
			nextIndex = check.indexOf(separator, start);
			length = nextIndex - start;	
			apModDistributionRecordDetails.setDrCode(new Integer(Integer.parseInt(check.substring(start, start + length))));

			// DR line
			start = nextIndex + 1;
			nextIndex = check.indexOf(separator, start);
			length = nextIndex - start;	
			apModDistributionRecordDetails.setDrLine((short)Integer.parseInt(check.substring(start, start + length)));
			
			// DR Class
			start = nextIndex + 1;
			nextIndex = check.indexOf(separator, start);
			length = nextIndex - start;	
			apModDistributionRecordDetails.setDrClass(check.substring(start, start + length));
			
			// DR Debit
			start = nextIndex + 1;
			nextIndex = check.indexOf(separator, start);
			length = nextIndex - start;	
			apModDistributionRecordDetails.setDrDebit((byte)Integer.parseInt(check.substring(start, start + length)));
			
			// DR Amount
			start = nextIndex + 1;
			nextIndex = check.indexOf(separator, start);
			length = nextIndex - start;	
			apModDistributionRecordDetails.setDrAmount(Double.parseDouble(check.substring(start, start + length)));
			
			// DR Imported
			start = nextIndex + 1;
			nextIndex = check.indexOf(separator, start);
			length = nextIndex - start;	
			apModDistributionRecordDetails.setDrImported((byte)Integer.parseInt(check.substring(start, start + length)));
			
			// DR Reversed
			start = nextIndex + 1;
			nextIndex = check.indexOf(separator, start);
			length = nextIndex - start;	
			apModDistributionRecordDetails.setDrReversed((byte)Integer.parseInt(check.substring(start, start + length)));
			
			//	GL COA
			start = nextIndex + 1;
			nextIndex = check.indexOf(separator, start);
			length = nextIndex - start;
			apModDistributionRecordDetails.setDrCoaAccountNumber(check.substring(start, start + length));

			// AP Voucher
			start = nextIndex + 1;
			nextIndex = check.indexOf(separator, start);
			length = nextIndex - start;

			// AP Recurring Voucher
			start = nextIndex + 1;
			nextIndex = check.indexOf(separator, start);
			length = nextIndex - start;

			// AP Check
			start = nextIndex + 1;
			nextIndex = check.indexOf(separator, start);
			length = nextIndex - start;			

			// AP Applied Voucher
			start = nextIndex + 1;
			nextIndex = check.indexOf(separator, start);
			length = nextIndex - start;			

			// AP Purchase Order
			start = nextIndex + 1;
			nextIndex = check.indexOf(separator, start);
			length = nextIndex - start;

			invIliList.add(apModDistributionRecordDetails);
			
			// begin lineSeparator
			start = nextIndex + 1;
			nextIndex = check.indexOf(lineSeparator, start);
			length = nextIndex - start;
			
			int tempStart = nextIndex + 1;
			if (check.indexOf(separator, tempStart) == -1) break;
			
		}
		
		apModCheckDetails.setChkDrList(invIliList);
		
		return apModCheckDetails;
		
	}
	
	
	public void ejbCreate() throws CreateException {       
		Debug.print("ApCheckSyncControllerBean ejbCreate");        
	}                
	
	public void ejbRemove() {};
	
	public void ejbActivate() {}
	public void ejbPassivate() {}
	
	public void setSessionContext(SessionContext ctx) {}
	
	
}

