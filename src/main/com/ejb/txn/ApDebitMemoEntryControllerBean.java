package com.ejb.txn;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;
import javax.naming.NamingException;

import com.ejb.ad.LocalAdAmountLimit;
import com.ejb.ad.LocalAdAmountLimitHome;
import com.ejb.ad.LocalAdApproval;
import com.ejb.ad.LocalAdApprovalHome;
import com.ejb.ad.LocalAdApprovalQueue;
import com.ejb.ad.LocalAdApprovalQueueHome;
import com.ejb.ad.LocalAdApprovalUser;
import com.ejb.ad.LocalAdApprovalUserHome;
import com.ejb.ad.LocalAdBranchDocumentSequenceAssignment;
import com.ejb.ad.LocalAdBranchDocumentSequenceAssignmentHome;
import com.ejb.ad.LocalAdBranchItemLocation;
import com.ejb.ad.LocalAdBranchItemLocationHome;
import com.ejb.ad.LocalAdBranchSupplier;
import com.ejb.ad.LocalAdBranchSupplierHome;
import com.ejb.ad.LocalAdCompany;
import com.ejb.ad.LocalAdCompanyHome;
import com.ejb.ad.LocalAdDeleteAuditTrailHome;
import com.ejb.ad.LocalAdPreference;
import com.ejb.ad.LocalAdPreferenceHome;
import com.ejb.ap.LocalApDistributionRecord;
import com.ejb.ap.LocalApDistributionRecordHome;
import com.ejb.ap.LocalApSupplier;
import com.ejb.ap.LocalApSupplierBalance;
import com.ejb.ap.LocalApSupplierBalanceHome;
import com.ejb.ap.LocalApSupplierHome;
import com.ejb.ap.LocalApTaxCode;
import com.ejb.ap.LocalApVoucher;
import com.ejb.ap.LocalApVoucherBatch;
import com.ejb.ap.LocalApVoucherBatchHome;
import com.ejb.ap.LocalApVoucherHome;
import com.ejb.ap.LocalApVoucherLineItem;
import com.ejb.ap.LocalApVoucherLineItemHome;
import com.ejb.ap.LocalApVoucherPaymentSchedule;
import com.ejb.ap.LocalApVoucherPaymentScheduleHome;
import com.ejb.ap.LocalApWithholdingTaxCode;
import com.ejb.exception.AdPRFCoaGlVarianceAccountNotFoundException;
import com.ejb.exception.ApVOUOverapplicationNotAllowedException;
import com.ejb.exception.GlJREffectiveDateNoPeriodExistException;
import com.ejb.exception.GlJREffectiveDatePeriodClosedException;
import com.ejb.exception.GlobalBranchAccountNumberInvalidException;
import com.ejb.exception.GlobalDocumentNumberNotUniqueException;
import com.ejb.exception.GlobalInvItemLocationNotFoundException;
import com.ejb.exception.GlobalInventoryDateException;
import com.ejb.exception.GlobalJournalNotBalanceException;
import com.ejb.exception.GlobalNoApprovalApproverFoundException;
import com.ejb.exception.GlobalNoApprovalRequesterFoundException;
import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.exception.GlobalRecordAlreadyDeletedException;
import com.ejb.exception.GlobalTransactionAlreadyApprovedException;
import com.ejb.exception.GlobalTransactionAlreadyLockedException;
import com.ejb.exception.GlobalTransactionAlreadyPendingException;
import com.ejb.exception.GlobalTransactionAlreadyPostedException;
import com.ejb.exception.GlobalTransactionAlreadyVoidException;
import com.ejb.gl.LocalAdDocumentSequenceAssignment;
import com.ejb.gl.LocalAdDocumentSequenceAssignmentHome;
import com.ejb.gl.LocalGlAccountingCalendarValue;
import com.ejb.gl.LocalGlAccountingCalendarValueHome;
import com.ejb.gl.LocalGlChartOfAccount;
import com.ejb.gl.LocalGlChartOfAccountBalance;
import com.ejb.gl.LocalGlChartOfAccountBalanceHome;
import com.ejb.gl.LocalGlChartOfAccountHome;
import com.ejb.gl.LocalGlFunctionalCurrency;
import com.ejb.gl.LocalGlFunctionalCurrencyHome;
import com.ejb.gl.LocalGlFunctionalCurrencyRate;
import com.ejb.gl.LocalGlFunctionalCurrencyRateHome;
import com.ejb.gl.LocalGlJournal;
import com.ejb.gl.LocalGlJournalBatch;
import com.ejb.gl.LocalGlJournalBatchHome;
import com.ejb.gl.LocalGlJournalCategory;
import com.ejb.gl.LocalGlJournalCategoryHome;
import com.ejb.gl.LocalGlJournalHome;
import com.ejb.gl.LocalGlJournalLine;
import com.ejb.gl.LocalGlJournalLineHome;
import com.ejb.gl.LocalGlJournalSource;
import com.ejb.gl.LocalGlJournalSourceHome;
import com.ejb.gl.LocalGlSetOfBook;
import com.ejb.gl.LocalGlSetOfBookHome;
import com.ejb.gl.LocalGlSuspenseAccount;
import com.ejb.gl.LocalGlSuspenseAccountHome;
import com.ejb.inv.LocalInvAdjustment;
import com.ejb.inv.LocalInvAdjustmentHome;
import com.ejb.inv.LocalInvAdjustmentLine;
import com.ejb.inv.LocalInvAdjustmentLineHome;
import com.ejb.inv.LocalInvCosting;
import com.ejb.inv.LocalInvCostingHome;
import com.ejb.inv.LocalInvDistributionRecord;
import com.ejb.inv.LocalInvDistributionRecordHome;
import com.ejb.inv.LocalInvItem;
import com.ejb.inv.LocalInvItemHome;
import com.ejb.inv.LocalInvItemLocation;
import com.ejb.inv.LocalInvItemLocationHome;
import com.ejb.inv.LocalInvLocation;
import com.ejb.inv.LocalInvLocationHome;
import com.ejb.inv.LocalInvUnitOfMeasure;
import com.ejb.inv.LocalInvUnitOfMeasureConversion;
import com.ejb.inv.LocalInvUnitOfMeasureConversionHome;
import com.ejb.inv.LocalInvUnitOfMeasureHome;
import com.util.AbstractSessionBean;
import com.util.ApModDistributionRecordDetails;
import com.util.ApModVoucherDetails;
import com.util.ApModVoucherLineItemDetails;
import com.util.Debug;
import com.util.EJBCommon;
import com.util.EJBHomeFactory;
import com.util.InvModUnitOfMeasureDetails;

/**
 * @ejb:bean name="ApDebitMemoEntryControllerEJB"
 *           display-name="used for entering vouchers"
 *           type="Stateless"
 *           view-type="remote"
 *           jndi-name="ejb/ApDebitMemoEntryControllerEJB"
 *
 * @ejb:interface remote-class="com.ejb.txn.ApDebitMemoEntryController"
 *                extends="javax.ejb.EJBObject"
 *
 * @ejb:home remote-class="com.ejb.txn.ApDebitMemoEntryControllerHome"
 *           extends="javax.ejb.EJBHome"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="apuser"
 *                        role-link="apuserlink"
 *
 * @ejb:permission role-name="apuser"
 *
*/

public class ApDebitMemoEntryControllerBean extends AbstractSessionBean {

    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList getApSplAll(Integer AD_BRNCH, Integer AD_CMPNY) {

        Debug.print("ApDebitMemoEntryControllerBean getApSplAll");

        LocalApSupplierHome apSupplierHome = null;

        ArrayList list = new ArrayList();

        // Initialize EJB Home

        try {

            apSupplierHome = (LocalApSupplierHome)EJBHomeFactory.
                lookUpLocalHome(LocalApSupplierHome.JNDI_NAME, LocalApSupplierHome.class);

        } catch (NamingException ex) {

            throw new EJBException(ex.getMessage());

        }

        try {

        	Collection apSuppliers = apSupplierHome.findEnabledSplAll(AD_BRNCH, AD_CMPNY);

        	Iterator i = apSuppliers.iterator();

        	while (i.hasNext()) {

        		LocalApSupplier apSupplier = (LocalApSupplier)i.next();

        		list.add(apSupplier.getSplSupplierCode());

        	}

        	return list;

        } catch (Exception ex) {

        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());

        }

    }

    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList getInvLocAll(Integer AD_CMPNY) {

        Debug.print("ApDebitMemoEntryControllerBean getInvLocAll");

        LocalInvLocationHome invLocationHome = null;
        Collection invLocations = null;
        ArrayList list = new ArrayList();

        // Initialize EJB Home

        try {

        	invLocationHome = (LocalInvLocationHome)EJBHomeFactory.
                lookUpLocalHome(LocalInvLocationHome.JNDI_NAME, LocalInvLocationHome.class);

        } catch (NamingException ex) {

            throw new EJBException(ex.getMessage());

        }

        try {

            invLocations = invLocationHome.findLocAll(AD_CMPNY);

	        if (invLocations.isEmpty()) {

	        	return null;

	        }

	        Iterator i = invLocations.iterator();

	        while (i.hasNext()) {

	        	LocalInvLocation invLocation = (LocalInvLocation)i.next();
	        	String details = invLocation.getLocName();

	        	list.add(details);

	        }

	        return list;

    	} catch (Exception ex) {

    		Debug.printStackTrace(ex);
    		throw new EJBException(ex.getMessage());

    	}

    }

    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList getInvUomByIiName(String II_NM, Integer AD_CMPNY) {

    	Debug.print("ApDebitMemoEntryControllerBean getInvUomByIiName");

        LocalInvUnitOfMeasureHome invUnitOfMeasureHome = null;
        LocalInvItemHome invItemHome = null;
        ArrayList list = new ArrayList();

        // Initialize EJB Home

        try {

        	invUnitOfMeasureHome = (LocalInvUnitOfMeasureHome)EJBHomeFactory.
                lookUpLocalHome(LocalInvUnitOfMeasureHome.JNDI_NAME, LocalInvUnitOfMeasureHome.class);
        	invItemHome = (LocalInvItemHome)EJBHomeFactory.
            	lookUpLocalHome(LocalInvItemHome.JNDI_NAME, LocalInvItemHome.class);

        } catch (NamingException ex) {

            throw new EJBException(ex.getMessage());
        }

        try {

        	LocalInvItem invItem = null;
        	LocalInvUnitOfMeasure invItemUnitOfMeasure = null;

        	invItem = invItemHome.findByIiName(II_NM, AD_CMPNY);
        	invItemUnitOfMeasure = invItem.getInvUnitOfMeasure();

        	Collection invUnitOfMeasures = null;
        	Iterator i = invUnitOfMeasureHome.findByUomAdLvClass(
        			invItemUnitOfMeasure.getUomAdLvClass(), AD_CMPNY).iterator();
        	while (i.hasNext()) {

        		LocalInvUnitOfMeasure invUnitOfMeasure = (LocalInvUnitOfMeasure) i.next();
        		InvModUnitOfMeasureDetails details = new InvModUnitOfMeasureDetails();
        		details.setUomName(invUnitOfMeasure.getUomName());

        		if (invUnitOfMeasure.getUomName().equals(invItemUnitOfMeasure.getUomName())) {

        			details.setDefault(true);

        		}

        		list.add(details);

        	}

        	return list;

        } catch (Exception ex) {

        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());

        }

    }

    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
     public double getInvIiUnitCostByIiNameAndUomName(String VOU_DM_VCHR_NMBR, String II_NM, String LOC_NM, String UOM_NM, Integer AD_BRNCH, Integer AD_CMPNY)
        throws GlobalNoRecordFoundException {

        Debug.print("ApVoucherEntryControllerBean getInvIiUnitCostByIiNameAndUomName");

        LocalInvItemHome invItemHome = null;
        LocalInvCostingHome invCostingHome = null;
        LocalApVoucherHome apVoucherHome = null;
        LocalInvUnitOfMeasureConversionHome invUnitOfMeasureConversionHome = null;

        // Initialize EJB Home

        try {

            invItemHome = (LocalInvItemHome)EJBHomeFactory.
                lookUpLocalHome(LocalInvItemHome.JNDI_NAME, LocalInvItemHome.class);
            invCostingHome = (LocalInvCostingHome)EJBHomeFactory.
               lookUpLocalHome(LocalInvCostingHome.JNDI_NAME, LocalInvCostingHome.class);
            apVoucherHome = (LocalApVoucherHome)EJBHomeFactory.
               lookUpLocalHome(LocalApVoucherHome.JNDI_NAME, LocalApVoucherHome.class);
            invUnitOfMeasureConversionHome = (LocalInvUnitOfMeasureConversionHome)EJBHomeFactory.
    			lookUpLocalHome(LocalInvUnitOfMeasureConversionHome.JNDI_NAME, LocalInvUnitOfMeasureConversionHome.class);

        } catch (NamingException ex) {

           throw new EJBException(ex.getMessage());

        }

        try {

        	LocalApVoucher apVoucher = null;
            LocalInvItem invItem = null;
            double COST = 0d;

        	try {

	    		apVoucher = apVoucherHome.findByVouDocumentNumberAndVouDebitMemoAndBrCode(VOU_DM_VCHR_NMBR, EJBCommon.FALSE, AD_BRNCH, AD_CMPNY);

	    		if (apVoucher.getVouPosted() != EJBCommon.TRUE) {

	    			throw new GlobalNoRecordFoundException();

	    		}

	    	} catch (FinderException ex) {

	    		throw new GlobalNoRecordFoundException();

	    	}

	    	try {

				invItem = invItemHome.findByIiName(II_NM, AD_CMPNY);
				LocalInvCosting invCosting  = invCostingHome.getByMaxCstDateToLongAndMaxCstLineNumberAndRemainingQuantityNotEqualToZeroAndIiNameAndLocName(II_NM, LOC_NM, AD_BRNCH, AD_CMPNY);

				COST =  this.convertFunctionalToForeignCurrency(apVoucher.getGlFunctionalCurrency().getFcCode(),
		                   apVoucher.getGlFunctionalCurrency().getFcName(),
		                   apVoucher.getVouConversionDate(), apVoucher.getVouConversionRate(),
		                   Math.abs(invCosting.getCstRemainingValue() / invCosting.getCstRemainingQuantity()), AD_CMPNY);

			} catch (FinderException ex) {

				COST = invItem.getIiUnitCost();

			}

			LocalInvUnitOfMeasureConversion invUnitOfMeasureConversion = invUnitOfMeasureConversionHome.findUmcByIiNameAndUomName(II_NM, UOM_NM, AD_CMPNY);
        	LocalInvUnitOfMeasureConversion invDefaultUomConversion = invUnitOfMeasureConversionHome.findUmcByIiNameAndUomName(II_NM, invItem.getInvUnitOfMeasure().getUomName(), AD_CMPNY);

        	return EJBCommon.roundIt(invItem.getIiUnitCost() * invDefaultUomConversion.getUmcConversionFactor() / invUnitOfMeasureConversion.getUmcConversionFactor(), this.getGlFcPrecisionUnit(AD_CMPNY));

        } catch (GlobalNoRecordFoundException ex) {

        	getSessionContext().setRollbackOnly();
        	throw ex;

        } catch (Exception ex) {

           Debug.printStackTrace(ex);
           throw new EJBException(ex.getMessage());

        }

     }


    /**
    * @ejb:interface-method view-type="remote"
    * @jboss:method-attributes read-only="true"
    **/
    public short getAdPrfApJournalLineNumber(Integer AD_CMPNY) {

       Debug.print("ApDebitMemoEntryControllerBean getAdPrfApJournalLineNumber");

       LocalAdPreferenceHome adPreferenceHome = null;


       // Initialize EJB Home

       try {

          adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
              lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);

       } catch (NamingException ex) {

          throw new EJBException(ex.getMessage());

       }


       try {

          LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);

          return adPreference.getPrfApJournalLineNumber();

       } catch (Exception ex) {

          Debug.printStackTrace(ex);
          throw new EJBException(ex.getMessage());

       }

    }

    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ApModVoucherDetails getApVouByVouCode(Integer VOU_CODE, Integer AD_CMPNY)
        throws GlobalNoRecordFoundException {

        Debug.print("ApDebitMemoEntryControllerBean getApVouByVouCode");

        LocalApVoucherHome apVoucherHome = null;
        LocalApDistributionRecordHome apDistributionRecordHome = null;

        // Initialize EJB Home

        try {

            apVoucherHome = (LocalApVoucherHome)EJBHomeFactory.
                lookUpLocalHome(LocalApVoucherHome.JNDI_NAME, LocalApVoucherHome.class);
            apDistributionRecordHome = (LocalApDistributionRecordHome)EJBHomeFactory.
        		lookUpLocalHome(LocalApDistributionRecordHome.JNDI_NAME, LocalApDistributionRecordHome.class);

        } catch (NamingException ex) {

            throw new EJBException(ex.getMessage());

        }

        try {

        	LocalApVoucher apDebitMemo = null;


        	try {

        		apDebitMemo = apVoucherHome.findByPrimaryKey(VOU_CODE);

        	} catch (FinderException ex) {

        		throw new GlobalNoRecordFoundException();

        	}

        	ArrayList list = new ArrayList();

        	// get voucher line items if any

        	Collection apVoucherLineItems = apDebitMemo.getApVoucherLineItems();

        	double TOTAL_DEBIT = 0d;
        	double TOTAL_CREDIT = 0d;

        	if (!apVoucherLineItems.isEmpty()) {

        		Iterator i = apVoucherLineItems.iterator();

        		while (i.hasNext()) {

        			LocalApVoucherLineItem apVoucherLineItem = (LocalApVoucherLineItem) i.next();

        			ApModVoucherLineItemDetails vliDetails = new ApModVoucherLineItemDetails();

        			vliDetails.setVliCode(apVoucherLineItem.getVliCode());
        			vliDetails.setVliLine(apVoucherLineItem.getVliLine());
        			vliDetails.setVliQuantity(apVoucherLineItem.getVliQuantity());
        			vliDetails.setVliUnitCost(apVoucherLineItem.getVliUnitCost());
        			vliDetails.setVliIiName(apVoucherLineItem.getInvItemLocation().getInvItem().getIiName());
        			vliDetails.setVliLocName(apVoucherLineItem.getInvItemLocation().getInvLocation().getLocName());
        			vliDetails.setVliUomName(apVoucherLineItem.getInvUnitOfMeasure().getUomName());
        			vliDetails.setVliIiDescription(apVoucherLineItem.getInvItemLocation().getInvItem().getIiDescription());

        			list.add(vliDetails);

        		}

        	} else {

	        	// get distribution records

	        	Collection apDistributionRecords = apDistributionRecordHome.findByVouCode(apDebitMemo.getVouCode(), AD_CMPNY);

	        	short lineNumber = 1;

	        	Iterator i = apDistributionRecords.iterator();

	        	TOTAL_DEBIT = 0d;
	        	TOTAL_CREDIT = 0d;

	        	while (i.hasNext()) {

	        		LocalApDistributionRecord apDistributionRecord = (LocalApDistributionRecord)i.next();

	        		ApModDistributionRecordDetails mdetails = new ApModDistributionRecordDetails();

	        		mdetails.setDrCode(apDistributionRecord.getDrCode());
	        		mdetails.setDrLine(lineNumber);
	        		mdetails.setDrClass(apDistributionRecord.getDrClass());
	        		mdetails.setDrDebit(apDistributionRecord.getDrDebit());
	        		mdetails.setDrAmount(apDistributionRecord.getDrAmount());
	        		mdetails.setDrCoaAccountNumber(apDistributionRecord.getGlChartOfAccount().getCoaAccountNumber());
				    mdetails.setDrCoaAccountDescription(apDistributionRecord.getGlChartOfAccount().getCoaAccountDescription());

				    if(apDistributionRecord.getDrDebit() == EJBCommon.TRUE) {

				    	TOTAL_DEBIT += apDistributionRecord.getDrAmount();

				    } else {

				    	TOTAL_CREDIT += apDistributionRecord.getDrAmount();

				    }

	        		list.add(mdetails);

	        		lineNumber++;

	        	}

        	}

        	ApModVoucherDetails mVouDetails = new ApModVoucherDetails();

        	mVouDetails.setVouCode(apDebitMemo.getVouCode());
        	mVouDetails.setVouDescription(apDebitMemo.getVouDescription());
        	mVouDetails.setVouDate(apDebitMemo.getVouDate());
        	mVouDetails.setVouDocumentNumber(apDebitMemo.getVouDocumentNumber());
        	mVouDetails.setVouDmVoucherNumber(apDebitMemo.getVouDmVoucherNumber());
        	mVouDetails.setVouBillAmount(apDebitMemo.getVouBillAmount());
        	mVouDetails.setVouTotalDebit(TOTAL_DEBIT);
        	mVouDetails.setVouTotalCredit(TOTAL_CREDIT);
        	mVouDetails.setVouApprovalStatus(apDebitMemo.getVouApprovalStatus());
        	mVouDetails.setVouReasonForRejection(apDebitMemo.getVouReasonForRejection());
        	mVouDetails.setVouPosted(apDebitMemo.getVouPosted());
        	mVouDetails.setVouVoid(apDebitMemo.getVouVoid());
        	mVouDetails.setVouCreatedBy(apDebitMemo.getVouCreatedBy());
        	mVouDetails.setVouDateCreated(apDebitMemo.getVouDateCreated());
        	mVouDetails.setVouLastModifiedBy(apDebitMemo.getVouLastModifiedBy());
        	mVouDetails.setVouDateLastModified(apDebitMemo.getVouDateLastModified());
        	mVouDetails.setVouApprovedRejectedBy(apDebitMemo.getVouApprovedRejectedBy());
        	mVouDetails.setVouDateApprovedRejected(apDebitMemo.getVouDateApprovedRejected());
        	mVouDetails.setVouPostedBy(apDebitMemo.getVouPostedBy());
        	mVouDetails.setVouDatePosted(apDebitMemo.getVouDatePosted());
        	mVouDetails.setVouSplSupplierCode(apDebitMemo.getApSupplier().getSplSupplierCode());
        	mVouDetails.setVouVbName(apDebitMemo.getApVoucherBatch() != null ? apDebitMemo.getApVoucherBatch().getVbName() : null);
        	mVouDetails.setVouSplName(apDebitMemo.getApSupplier().getSplName());

        	if (!apVoucherLineItems.isEmpty()) {

        		mVouDetails.setVouVliList(list);
        		mVouDetails.setVouAmountDue(apDebitMemo.getVouAmountDue());

        	} else {

        		mVouDetails.setVouDrList(list);

        	}

        	return mVouDetails;

        } catch (GlobalNoRecordFoundException ex) {

        	throw ex;

        } catch (Exception ex) {

        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());

        }

    }

    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList getApDrByVouDmVoucherNumberAndVouBillAmountAndSplSupplierCodeBrCode(String VOU_DM_VCHR_NMBR, double VOU_BLL_AMNT, String SPL_SPPLR_CODE, Integer AD_BRNCH, Integer AD_CMPNY)
        throws GlobalNoRecordFoundException,
        ApVOUOverapplicationNotAllowedException {

        Debug.print("ApDebitMemoEntryControllerBean getApDrByVouDmVoucherNumberAndVouBillAmount");

        LocalApSupplierHome apSupplierHome = null;
        LocalGlChartOfAccountHome glChartOfAccountHome = null;
        LocalApVoucherHome apVoucherHome = null;
        LocalApVoucherPaymentScheduleHome apVoucherPaymentScheduleHome = null;
        LocalAdPreferenceHome  adPreferenceHome=null;
        ArrayList list = new ArrayList();


        // Initialize EJB Home

        try {

            apSupplierHome = (LocalApSupplierHome)EJBHomeFactory.
                lookUpLocalHome(LocalApSupplierHome.JNDI_NAME, LocalApSupplierHome.class);
            apVoucherHome = (LocalApVoucherHome)EJBHomeFactory.
                lookUpLocalHome(LocalApVoucherHome.JNDI_NAME, LocalApVoucherHome.class);
            glChartOfAccountHome = (LocalGlChartOfAccountHome)EJBHomeFactory.
                lookUpLocalHome(LocalGlChartOfAccountHome.JNDI_NAME, LocalGlChartOfAccountHome.class);
            apVoucherPaymentScheduleHome = (LocalApVoucherPaymentScheduleHome)EJBHomeFactory.
                lookUpLocalHome(LocalApVoucherPaymentScheduleHome.JNDI_NAME, LocalApVoucherPaymentScheduleHome.class);
            adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
            	lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);

        } catch (NamingException ex) {

            throw new EJBException(ex.getMessage());

        }

        try {


        	LocalApVoucher apVoucher = null;

        	short precisionUnit = this.getGlFcPrecisionUnit(AD_CMPNY);

        	// validate if voucher exist or overapplied

        	try {
        		LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);
        		apVoucher = apVoucherHome.findByVouDocumentNumberAndVouDebitMemoAndSplSupplierCodeBrCode(VOU_DM_VCHR_NMBR,
        		    EJBCommon.FALSE, SPL_SPPLR_CODE, AD_BRNCH, AD_CMPNY);

        		if (apVoucher.getVouPosted() != EJBCommon.TRUE) {

        			throw new GlobalNoRecordFoundException();

        		} else if (VOU_BLL_AMNT > EJBCommon.roundIt(apVoucher.getVouAmountDue() - apVoucher.getVouAmountPaid(), precisionUnit)) {
        			if(adPreference.getPrfApDebitMemoOverrideCost()!=1){
        				throw new ApVOUOverapplicationNotAllowedException();
        			}
        		}


        	} catch (FinderException ex) {

        		throw new GlobalNoRecordFoundException();

        	}


        	// get amount percent

        	double AMOUNT_PERCENT = VOU_BLL_AMNT / apVoucher.getVouAmountDue();

        	Collection apDistributionRecords = apVoucher.getApDistributionRecords();

        	// get total debit and credit for rounding difference calculation

        	double TOTAL_DEBIT = 0d;
        	double TOTAL_CREDIT = 0d;
        	boolean isRoundingDifferenceCalculated = false;

        	Iterator i = apDistributionRecords.iterator();

        	while (i.hasNext()) {

        		LocalApDistributionRecord apDistributionRecord = (LocalApDistributionRecord)i.next();

        		if (apDistributionRecord.getDrDebit() == EJBCommon.TRUE) {

        			TOTAL_DEBIT += EJBCommon.roundIt(apDistributionRecord.getDrAmount() * AMOUNT_PERCENT, precisionUnit);

        		} else {

        			TOTAL_CREDIT += EJBCommon.roundIt(apDistributionRecord.getDrAmount() * AMOUNT_PERCENT, precisionUnit);

        		}

        	}

        	// get default debit memo lines

        	short lineNumber = 1;

        	i = apDistributionRecords.iterator();

        	while (i.hasNext()) {

        		LocalApDistributionRecord apDistributionRecord = (LocalApDistributionRecord)i.next();

        		ApModDistributionRecordDetails mdetails = new ApModDistributionRecordDetails();

        		mdetails.setDrLine(lineNumber++);
        		mdetails.setDrClass(apDistributionRecord.getDrClass());
        		mdetails.setDrDebit(apDistributionRecord.getDrDebit() == EJBCommon.TRUE ?
        		    EJBCommon.FALSE : EJBCommon.TRUE);

        		double DR_AMNT = EJBCommon.roundIt(apDistributionRecord.getDrAmount() * AMOUNT_PERCENT, precisionUnit);

        		// calculate rounding difference if necessary

        		if (apDistributionRecord.getDrDebit() == EJBCommon.TRUE &&
        		    TOTAL_DEBIT != TOTAL_CREDIT && !isRoundingDifferenceCalculated) {

        		    DR_AMNT = DR_AMNT + TOTAL_CREDIT - TOTAL_DEBIT;

        		    isRoundingDifferenceCalculated = true;

        		}

        		mdetails.setDrAmount(DR_AMNT);
        		mdetails.setDrCoaAccountNumber(apDistributionRecord.getGlChartOfAccount().getCoaAccountNumber());
	        	mdetails.setDrCoaAccountDescription(apDistributionRecord.getGlChartOfAccount().getCoaAccountDescription());

	        	list.add(mdetails);

        	}


        	return list;

        } catch (GlobalNoRecordFoundException ex) {

        	throw ex;

        } catch (ApVOUOverapplicationNotAllowedException ex) {

        	throw ex;

        } catch (Exception ex) {

        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());

        }

    }


    /**
     * @ejb:interface-method view-type="remote"
     **/
    public Integer saveApVouEntry(com.util.ApVoucherDetails details, String SPL_SPPLR_CODE, String VB_NM, ArrayList drList, boolean isDraft, Integer AD_BRNCH, Integer AD_CMPNY) throws
        GlobalRecordAlreadyDeletedException,
        GlobalNoRecordFoundException,
		GlobalDocumentNumberNotUniqueException,
		GlobalBranchAccountNumberInvalidException,
		GlobalTransactionAlreadyApprovedException,
		GlobalTransactionAlreadyPendingException,
		GlobalTransactionAlreadyPostedException,
		GlobalTransactionAlreadyVoidException,
		ApVOUOverapplicationNotAllowedException,
		GlobalTransactionAlreadyLockedException,
		GlobalNoApprovalRequesterFoundException,
		GlobalNoApprovalApproverFoundException,
		GlJREffectiveDateNoPeriodExistException,
		GlJREffectiveDatePeriodClosedException,
		GlobalJournalNotBalanceException {

        Debug.print("ApDebitMemoEntryControllerBean saveApVouEntry");

        LocalApVoucherHome apVoucherHome = null;
        LocalApVoucherBatchHome apVoucherBatchHome = null;
        LocalAdCompanyHome adCompanyHome = null;
        LocalAdDocumentSequenceAssignmentHome adDocumentSequenceAssignmentHome = null;
        LocalApSupplierHome apSupplierHome = null;
        LocalApVoucherPaymentScheduleHome apVoucherPaymentScheduleHome = null;
        LocalAdApprovalHome adApprovalHome = null;
        LocalAdAmountLimitHome adAmountLimitHome = null;
        LocalAdApprovalUserHome adApprovalUserHome = null;
        LocalAdApprovalQueueHome adApprovalQueueHome = null;
        LocalAdPreferenceHome adPreferenceHome = null;
        LocalAdBranchDocumentSequenceAssignmentHome adBranchDocumentSequenceAssignmentHome = null;

        LocalApVoucher apDebitMemo = null;
        LocalApVoucher apVoucher = null;


        // Initialize EJB Home

        try {

            apVoucherHome = (LocalApVoucherHome)EJBHomeFactory.
                lookUpLocalHome(LocalApVoucherHome.JNDI_NAME, LocalApVoucherHome.class);
            apVoucherBatchHome = (LocalApVoucherBatchHome)EJBHomeFactory.
            	lookUpLocalHome(LocalApVoucherBatchHome.JNDI_NAME, LocalApVoucherBatchHome.class);
            adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
            adDocumentSequenceAssignmentHome = (LocalAdDocumentSequenceAssignmentHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdDocumentSequenceAssignmentHome.JNDI_NAME, LocalAdDocumentSequenceAssignmentHome.class);
            apSupplierHome = (LocalApSupplierHome)EJBHomeFactory.
                lookUpLocalHome(LocalApSupplierHome.JNDI_NAME, LocalApSupplierHome.class);
            apVoucherPaymentScheduleHome = (LocalApVoucherPaymentScheduleHome)EJBHomeFactory.
                lookUpLocalHome(LocalApVoucherPaymentScheduleHome.JNDI_NAME, LocalApVoucherPaymentScheduleHome.class);
            adApprovalHome = (LocalAdApprovalHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdApprovalHome.JNDI_NAME, LocalAdApprovalHome.class);
            adAmountLimitHome = (LocalAdAmountLimitHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdAmountLimitHome.JNDI_NAME, LocalAdAmountLimitHome.class);
            adApprovalUserHome = (LocalAdApprovalUserHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdApprovalUserHome.JNDI_NAME, LocalAdApprovalUserHome.class);
            adApprovalQueueHome = (LocalAdApprovalQueueHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdApprovalQueueHome.JNDI_NAME, LocalAdApprovalQueueHome.class);
            adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);
            adBranchDocumentSequenceAssignmentHome = (LocalAdBranchDocumentSequenceAssignmentHome)EJBHomeFactory.
            	lookUpLocalHome(LocalAdBranchDocumentSequenceAssignmentHome.JNDI_NAME, LocalAdBranchDocumentSequenceAssignmentHome.class);

        } catch (NamingException ex) {

            throw new EJBException(ex.getMessage());

        }

        try {

        	// validate if debit memo is already deleted

        	try {

        		if (details.getVouCode() != null) {

        			apDebitMemo = apVoucherHome.findByPrimaryKey(details.getVouCode());

        		}

        	} catch (FinderException ex) {

        		throw new GlobalRecordAlreadyDeletedException();

        	}

        	// validate if debit memo is already posted, void, approved or pending or locked

	        if (details.getVouCode() != null) {

	        	if (apDebitMemo.getVouApprovalStatus() != null) {

	        		if (apDebitMemo.getVouApprovalStatus().equals("APPROVED") ||
		        		apDebitMemo.getVouApprovalStatus().equals("N/A")) {

	        		    throw new GlobalTransactionAlreadyApprovedException();

	        		} else if (apDebitMemo.getVouApprovalStatus().equals("PENDING")) {

	        			throw new GlobalTransactionAlreadyPendingException();

	        		}

	        	}

        		if (apDebitMemo.getVouPosted() == EJBCommon.TRUE) {

        			throw new GlobalTransactionAlreadyPostedException();

        		} else if (apDebitMemo.getVouVoid() == EJBCommon.TRUE) {

        			throw new GlobalTransactionAlreadyVoidException();

        		}

        	}


        	// debit memo void

	    	if (details.getVouCode() != null && details.getVouVoid() == EJBCommon.TRUE &&
	    	    apDebitMemo.getVouPosted() == EJBCommon.FALSE) {

        		// release lock

        		LocalApVoucher apLockedVoucher = apVoucherHome.findByVouDocumentNumberAndVouDebitMemoAndBrCode(apDebitMemo.getVouDmVoucherNumber(),
        		    EJBCommon.FALSE, AD_BRNCH, AD_CMPNY);

        		Collection apLockedVoucherPaymentSchedules = apLockedVoucher.getApVoucherPaymentSchedules();

        		Iterator vpsIter = apLockedVoucherPaymentSchedules.iterator();

        		while (vpsIter.hasNext()) {

        			LocalApVoucherPaymentSchedule apVoucherPaymentSchedule =
        			    (LocalApVoucherPaymentSchedule)vpsIter.next();

        			apVoucherPaymentSchedule.setVpsLock(EJBCommon.FALSE);

        		}

        		apDebitMemo.setVouVoid(EJBCommon.TRUE);
        		apDebitMemo.setVouLastModifiedBy(details.getVouLastModifiedBy());
        		apDebitMemo.setVouDateLastModified(details.getVouDateLastModified());

        		return apDebitMemo.getVouCode();

	    	}

	    	// validate if voucher number exists

        	try {

        		apVoucher = apVoucherHome.findByVouDocumentNumberAndVouDebitMemoAndBrCode(details.getVouDmVoucherNumber(), EJBCommon.FALSE, AD_BRNCH, AD_CMPNY);

        	} catch (FinderException ex) {

        		throw new GlobalNoRecordFoundException();

        	}


        	// validate if document number is unique document number is automatic then set next sequence

        	try {

        	    LocalAdBranchDocumentSequenceAssignment adBranchDocumentSequenceAssignment = null;
	 			LocalAdDocumentSequenceAssignment adDocumentSequenceAssignment = null;

	        	if (details.getVouCode() == null) {

	        	    try {

	        	        adDocumentSequenceAssignment = adDocumentSequenceAssignmentHome.findByDcName("AP DEBIT MEMO", AD_CMPNY);

	        	    } catch(FinderException ex) { }

	        	    try {

	 					adBranchDocumentSequenceAssignment = adBranchDocumentSequenceAssignmentHome.findBdsByDsaCodeAndBrCode(adDocumentSequenceAssignment.getDsaCode(), AD_BRNCH, AD_CMPNY);

	 				} catch (FinderException ex) { }

		    		LocalApVoucher apExistingDebitMemo = null;

		    		try {

		    		    apExistingDebitMemo = apVoucherHome.findByVouDocumentNumberAndVouDebitMemoAndBrCode(
		        		    details.getVouDocumentNumber(), EJBCommon.TRUE, AD_BRNCH, AD_CMPNY);

		        	} catch (FinderException ex) {
		        	}

		            if (apExistingDebitMemo != null) {

		            	throw new GlobalDocumentNumberNotUniqueException();

		            }

			        if (adDocumentSequenceAssignment.getAdDocumentSequence().getDsNumberingType() == 'A' &&
			            (details.getVouDocumentNumber() == null || details.getVouDocumentNumber().trim().length() == 0)) {

			            while (true) {

			                if (adBranchDocumentSequenceAssignment == null || adBranchDocumentSequenceAssignment.getBdsNextSequence() == null) {

				            	try {

				            		apVoucherHome.findByVouDocumentNumberAndVouDebitMemoAndBrCode(adDocumentSequenceAssignment.getDsaNextSequence(), EJBCommon.TRUE, AD_BRNCH, AD_CMPNY);
				            		adDocumentSequenceAssignment.setDsaNextSequence(EJBCommon.incrementStringNumber(adDocumentSequenceAssignment.getDsaNextSequence()));

				            	} catch (FinderException ex) {

				            		details.setVouDocumentNumber(adDocumentSequenceAssignment.getDsaNextSequence());
				            		adDocumentSequenceAssignment.setDsaNextSequence(EJBCommon.incrementStringNumber(adDocumentSequenceAssignment.getDsaNextSequence()));
						            break;

				            	}

			                } else {

			                    try {

				            		apVoucherHome.findByVouDocumentNumberAndVouDebitMemoAndBrCode(adBranchDocumentSequenceAssignment.getBdsNextSequence(), EJBCommon.TRUE, AD_BRNCH, AD_CMPNY);
				            		adBranchDocumentSequenceAssignment.setBdsNextSequence(EJBCommon.incrementStringNumber(adBranchDocumentSequenceAssignment.getBdsNextSequence()));

				            	} catch (FinderException ex) {

				            		details.setVouDocumentNumber(adBranchDocumentSequenceAssignment.getBdsNextSequence());
				            		adBranchDocumentSequenceAssignment.setBdsNextSequence(EJBCommon.incrementStringNumber(adBranchDocumentSequenceAssignment.getBdsNextSequence()));
						            break;

				            	}

			                }

			            }

			        }

			    } else {

			    	LocalApVoucher apExistingDebitMemo = null;

			    	try {

		    		    apExistingDebitMemo = apVoucherHome.findByVouDocumentNumberAndVouDebitMemoAndBrCode(
		        		    details.getVouDocumentNumber(), EJBCommon.TRUE, AD_BRNCH, AD_CMPNY);

		        	} catch (FinderException ex) {
		        	}

		        	if (apExistingDebitMemo != null &&
		                !apExistingDebitMemo.getVouCode().equals(details.getVouCode())) {

		            	throw new GlobalDocumentNumberNotUniqueException();

		            }

		            if (apDebitMemo.getVouDocumentNumber() != details.getVouDocumentNumber() &&
		                (details.getVouDocumentNumber() == null || details.getVouDocumentNumber().trim().length() == 0)) {

		                details.setVouDocumentNumber(apDebitMemo.getVouDocumentNumber());

		         	}

			    }

        	} catch (GlobalDocumentNumberNotUniqueException ex) {

	 			getSessionContext().setRollbackOnly();
	 			throw ex;

	 		} catch (Exception ex) {

	 			Debug.printStackTrace(ex);
	 			getSessionContext().setRollbackOnly();
	 			throw new EJBException(ex.getMessage());

	 		}

        	// validate if voucher entered is already locked by dm or check

        	Collection apValidateVoucherPaymentSchedules =
        	        apVoucherPaymentScheduleHome.findByVpsLockAndVouCode(EJBCommon.TRUE, apVoucher.getVouCode(), AD_CMPNY);

            if (details.getVouCode() == null && !apValidateVoucherPaymentSchedules.isEmpty() ||
                details.getVouCode() != null && !details.getVouDmVoucherNumber().equals(apDebitMemo.getVouDmVoucherNumber()) &&
                !apValidateVoucherPaymentSchedules.isEmpty()) {

                throw new GlobalTransactionAlreadyLockedException();

            }

        	// create voucher

        	if (details.getVouCode() == null) {


				apDebitMemo = apVoucherHome.create(EJBCommon.TRUE,
				    details.getVouDescription(), details.getVouDate(),
				    details.getVouDocumentNumber(), null, details.getVouDmVoucherNumber(),
				    null, 0d,
				    details.getVouBillAmount(), 0d,
				    0d, null, null, EJBCommon.FALSE, EJBCommon.FALSE, EJBCommon.FALSE, details.getVouCreatedBy(), details.getVouDateCreated(),
        	    	details.getVouLastModifiedBy(), details.getVouDateLastModified(),
        	    	null, null, null, null, EJBCommon.FALSE, null,
        	    	EJBCommon.FALSE, EJBCommon.FALSE,
        	    	AD_BRNCH, AD_CMPNY);


        	} else {

        		// release lock

	    	    LocalApVoucher apLockedVoucher = apVoucherHome.findByVouDocumentNumberAndVouDebitMemoAndBrCode(apDebitMemo.getVouDmVoucherNumber(),
        		    EJBCommon.FALSE, AD_BRNCH, AD_CMPNY);

        		Collection apLockedVoucherPaymentSchedules = apLockedVoucher.getApVoucherPaymentSchedules();

        		Iterator vpsIter = apLockedVoucherPaymentSchedules.iterator();

        		while (vpsIter.hasNext()) {

        			LocalApVoucherPaymentSchedule apVoucherPaymentSchedule =
        			    (LocalApVoucherPaymentSchedule)vpsIter.next();

        			apVoucherPaymentSchedule.setVpsLock(EJBCommon.FALSE);

        		}

        		apDebitMemo.setVouDescription(details.getVouDescription());
        		apDebitMemo.setVouDate(details.getVouDate());
        		apDebitMemo.setVouDocumentNumber(details.getVouDocumentNumber());
        		apDebitMemo.setVouDmVoucherNumber(details.getVouDmVoucherNumber());
        		apDebitMemo.setVouBillAmount(details.getVouBillAmount());
        		apDebitMemo.setVouLastModifiedBy(details.getVouLastModifiedBy());
        		apDebitMemo.setVouDateLastModified(details.getVouDateLastModified());
        		apDebitMemo.setVouReasonForRejection(null);


        	}

        	LocalApSupplier apSupplier = apSupplierHome.findBySplSupplierCode(SPL_SPPLR_CODE, AD_CMPNY);
        	apSupplier.addApVoucher(apDebitMemo);

        	try {

        		LocalApVoucherBatch apVoucherBatch = apVoucherBatchHome.findByVbName(VB_NM, AD_BRNCH, AD_CMPNY);
        		apVoucherBatch.addApVoucher(apDebitMemo);

        	} catch (FinderException ex) {

        	}

        	// remove all demo memo lines

        	Collection apVoucherLineItems = apDebitMemo.getApVoucherLineItems();

	  	    Iterator i = apVoucherLineItems.iterator();

	  	    while (i.hasNext()) {

	  	   	    LocalApVoucherLineItem apVoucherLineItem = (LocalApVoucherLineItem)i.next();

	  	  	    i.remove();

	  	  	    apVoucherLineItem.remove();

	  	    }

        	// remove all distribution records

	  	    Collection apDistributionRecords = apDebitMemo.getApDistributionRecords();

	  	    i = apDistributionRecords.iterator();

	  	    while (i.hasNext()) {

	  	   	    LocalApDistributionRecord apDistributionRecord = (LocalApDistributionRecord)i.next();

	  	  	    i.remove();

	  	  	    apDistributionRecord.remove();

	  	    }


	  	    // add new distribution records

      	    i = drList.iterator();

      	    while (i.hasNext()) {

      	    	ApModDistributionRecordDetails mDrDetails = (ApModDistributionRecordDetails) i.next();
      	    	LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);
      	    	if (mDrDetails.getDrClass().equals("PAYABLE")) {
      	    		if(adPreference.getPrfApDebitMemoOverrideCost()!=1){
      	    			if (apDebitMemo.getVouBillAmount() > EJBCommon.roundIt(apVoucher.getVouAmountDue() - apVoucher.getVouAmountPaid(), this.getGlFcPrecisionUnit(AD_CMPNY))) {

      	    				throw new ApVOUOverapplicationNotAllowedException();

      	    			}
      	    		}
      	    	}

      	    	this.addApDrEntry(mDrDetails, apDebitMemo, AD_BRNCH, AD_CMPNY);

      	    }

      	    // create new voucher payment schedule lock

      	    Collection apVoucherPaymentSchedules =
      	       apVoucher.getApVoucherPaymentSchedules();

      	   i = apVoucherPaymentSchedules.iterator();

      	   while (i.hasNext()) {

      	   	   LocalApVoucherPaymentSchedule apVoucherPaymentSchedule =
        		   (LocalApVoucherPaymentSchedule)i.next();

        	   apVoucherPaymentSchedule.setVpsLock(EJBCommon.TRUE);

      	   }

      	   // generate approval status

            String VOU_APPRVL_STATUS = null;

        	if (!isDraft) {

        		LocalAdApproval adApproval = adApprovalHome.findByAprAdCompany(AD_CMPNY);

        		// check if ap voucher approval is enabled

        		if (adApproval.getAprEnableApDebitMemo() == EJBCommon.FALSE) {

        			VOU_APPRVL_STATUS = "N/A";

        		} else {

        			// check if voucher is self approved

        			LocalAdAmountLimit adAmountLimit = null;

        			try {

        				adAmountLimit = adAmountLimitHome.findByAdcTypeAndAuTypeAndUsrName("AP DEBIT MEMO", "REQUESTER", details.getVouLastModifiedBy(), AD_CMPNY);

        			} catch (FinderException ex) {

        				throw new GlobalNoApprovalRequesterFoundException();

        			}

        			if (apDebitMemo.getVouBillAmount() <= adAmountLimit.getCalAmountLimit()) {

        				VOU_APPRVL_STATUS = "N/A";

        			} else {

        				// for approval, create approval queue

        				 Collection adAmountLimits = adAmountLimitHome.findByAdcTypeAndGreaterThanCalAmountLimit("AP DEBIT MEMO", adAmountLimit.getCalAmountLimit(), AD_CMPNY);

        				 if (adAmountLimits.isEmpty()) {

        				 	Collection adApprovalUsers = adApprovalUserHome.findByAuTypeAndCalCode("APPROVER", adAmountLimit.getCalCode(), AD_CMPNY);

        				 	if (adApprovalUsers.isEmpty()) {

        				 		throw new GlobalNoApprovalApproverFoundException();

        				 	}

        				 	Iterator j = adApprovalUsers.iterator();

        				 	while (j.hasNext()) {

        				 		LocalAdApprovalUser adApprovalUser = (LocalAdApprovalUser)j.next();

        				 		LocalAdApprovalQueue adApprovalQueue = adApprovalQueueHome.create(EJBCommon.TRUE, "AP DEBIT MEMO", apDebitMemo.getVouCode(),
        				 				apDebitMemo.getVouDocumentNumber(), apDebitMemo.getVouDate(), adAmountLimit.getCalAndOr(), adApprovalUser.getAuOr(), AD_BRNCH, AD_CMPNY);

        				 		adApprovalUser.getAdUser().addAdApprovalQueue(adApprovalQueue);

        				 	}

        				 } else {

        				 	boolean isApprovalUsersFound = false;

        				 	i = adAmountLimits.iterator();

        				 	while (i.hasNext()) {

        				 		LocalAdAmountLimit adNextAmountLimit = (LocalAdAmountLimit)i.next();

        				 		if (apDebitMemo.getVouBillAmount() <= adNextAmountLimit.getCalAmountLimit()) {

        				 			Collection adApprovalUsers = adApprovalUserHome.findByAuTypeAndCalCode("APPROVER", adAmountLimit.getCalCode(), AD_CMPNY);

        				 			Iterator j = adApprovalUsers.iterator();

		        				 	while (j.hasNext()) {

		        				 		isApprovalUsersFound = true;

		        				 		LocalAdApprovalUser adApprovalUser = (LocalAdApprovalUser)j.next();

		        				 		LocalAdApprovalQueue adApprovalQueue = adApprovalQueueHome.create(EJBCommon.TRUE, "AP DEBIT MEMO", apDebitMemo.getVouCode(),
		        				 				apDebitMemo.getVouDocumentNumber(), apDebitMemo.getVouDate(), adAmountLimit.getCalAndOr(), adApprovalUser.getAuOr(), AD_BRNCH, AD_CMPNY);

		        				 		adApprovalUser.getAdUser().addAdApprovalQueue(adApprovalQueue);

		        				 	}

	    				 			break;

        				 		} else if (!i.hasNext()) {

        				 			Collection adApprovalUsers = adApprovalUserHome.findByAuTypeAndCalCode("APPROVER", adNextAmountLimit.getCalCode(), AD_CMPNY);

        				 			Iterator j = adApprovalUsers.iterator();

		        				 	while (j.hasNext()) {

		        				 		isApprovalUsersFound = true;

		        				 		LocalAdApprovalUser adApprovalUser = (LocalAdApprovalUser)j.next();

		        				 		LocalAdApprovalQueue adApprovalQueue = adApprovalQueueHome.create(EJBCommon.TRUE, "AP DEBIT MEMO", apDebitMemo.getVouCode(),
		        				 				apDebitMemo.getVouDocumentNumber(), apDebitMemo.getVouDate(), adNextAmountLimit.getCalAndOr(), adApprovalUser.getAuOr(), AD_BRNCH, AD_CMPNY);

		        				 		adApprovalUser.getAdUser().addAdApprovalQueue(adApprovalQueue);

		        				 	}

	    				 			break;

        				 		}

        				 		adAmountLimit = adNextAmountLimit;

        				 	}

        				 	if (!isApprovalUsersFound) {

        				 		throw new GlobalNoApprovalApproverFoundException();

        				 	}

        			    }

        			    VOU_APPRVL_STATUS = "PENDING";
        			}
        		}
        	}

        	LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);

        	if (VOU_APPRVL_STATUS != null && VOU_APPRVL_STATUS.equals("N/A") && adPreference.getPrfApGlPostingType().equals("AUTO-POST UPON APPROVAL")) {

        		this.executeApDebitMemoPost(apDebitMemo.getVouCode(), apDebitMemo.getVouLastModifiedBy(), AD_BRNCH, AD_CMPNY);

        	}

        	// set debit memo approval status

        	apDebitMemo.setVouApprovalStatus(VOU_APPRVL_STATUS);


      	    return apDebitMemo.getVouCode();


        } catch (GlobalRecordAlreadyDeletedException ex) {

        	getSessionContext().setRollbackOnly();
        	throw ex;

        } catch (GlobalNoRecordFoundException ex) {

        	getSessionContext().setRollbackOnly();
        	throw ex;

        } catch (GlobalDocumentNumberNotUniqueException ex) {

        	getSessionContext().setRollbackOnly();
        	throw ex;

        } catch (GlobalTransactionAlreadyApprovedException ex) {

        	getSessionContext().setRollbackOnly();
        	throw ex;

        } catch (GlobalTransactionAlreadyPendingException ex) {

        	getSessionContext().setRollbackOnly();
        	throw ex;

        } catch (GlobalTransactionAlreadyPostedException ex) {

        	getSessionContext().setRollbackOnly();
        	throw ex;

        } catch (GlobalTransactionAlreadyVoidException ex) {

        	getSessionContext().setRollbackOnly();
        	throw ex;

        } catch (ApVOUOverapplicationNotAllowedException ex) {

        	getSessionContext().setRollbackOnly();
        	throw ex;

        } catch (GlobalTransactionAlreadyLockedException ex) {

        	getSessionContext().setRollbackOnly();
        	throw ex;

        } catch (GlobalBranchAccountNumberInvalidException ex) {

        	getSessionContext().setRollbackOnly();
        	throw ex;

        } catch (GlobalNoApprovalRequesterFoundException ex) {

        	getSessionContext().setRollbackOnly();
        	throw ex;

        } catch (GlobalNoApprovalApproverFoundException ex) {

        	getSessionContext().setRollbackOnly();
        	throw ex;

        } catch (GlJREffectiveDateNoPeriodExistException ex) {

        	getSessionContext().setRollbackOnly();
        	throw ex;

        } catch (GlJREffectiveDatePeriodClosedException ex) {

        	getSessionContext().setRollbackOnly();
        	throw ex;

        } catch (GlobalJournalNotBalanceException ex) {

        	getSessionContext().setRollbackOnly();
        	throw ex;

        } catch (Exception ex) {

        	Debug.printStackTrace(ex);
        	getSessionContext().setRollbackOnly();
        	throw new EJBException(ex.getMessage());

        }


    }

    /**
     * @ejb:interface-method view-type="remote"
     **/
    public Integer saveApVouVliEntry(com.util.ApVoucherDetails details, String SPL_SPPLR_CODE, String VB_NM, ArrayList vliList, boolean isDraft, Integer AD_BRNCH, Integer AD_CMPNY) throws
        GlobalRecordAlreadyDeletedException,
        GlobalNoRecordFoundException,
		GlobalDocumentNumberNotUniqueException,
		GlobalTransactionAlreadyApprovedException,
		GlobalTransactionAlreadyPendingException,
		GlobalTransactionAlreadyPostedException,
		GlobalTransactionAlreadyVoidException,
		ApVOUOverapplicationNotAllowedException,
		GlobalTransactionAlreadyLockedException,
		GlobalNoApprovalRequesterFoundException,
		GlobalNoApprovalApproverFoundException,
		GlobalInvItemLocationNotFoundException,
		GlJREffectiveDateNoPeriodExistException,
		GlJREffectiveDatePeriodClosedException,
		GlobalJournalNotBalanceException,
		GlobalInventoryDateException,
		GlobalBranchAccountNumberInvalidException,
		AdPRFCoaGlVarianceAccountNotFoundException {

        Debug.print("ApDebitMemoEntryControllerBean saveApVouEntry");

        LocalApVoucherHome apVoucherHome = null;
        LocalApVoucherBatchHome apVoucherBatchHome = null;
        LocalAdCompanyHome adCompanyHome = null;
        LocalAdDocumentSequenceAssignmentHome adDocumentSequenceAssignmentHome = null;
        LocalApSupplierHome apSupplierHome = null;
        LocalApVoucherPaymentScheduleHome apVoucherPaymentScheduleHome = null;
        LocalAdApprovalHome adApprovalHome = null;
        LocalAdAmountLimitHome adAmountLimitHome = null;
        LocalAdApprovalUserHome adApprovalUserHome = null;
        LocalAdApprovalQueueHome adApprovalQueueHome = null;
        LocalAdPreferenceHome adPreferenceHome = null;
        LocalInvItemLocationHome invItemLocationHome = null;
        LocalInvCostingHome invCostingHome = null;
        LocalAdBranchDocumentSequenceAssignmentHome adBranchDocumentSequenceAssignmentHome = null;
        LocalAdBranchItemLocationHome adBranchItemLocationHome = null;
        LocalAdBranchSupplierHome adBranchSupplierHome = null;

        LocalApVoucher apDebitMemo = null;
        LocalApVoucher apVoucher = null;


        // Initialize EJB Home

        try {

            apVoucherHome = (LocalApVoucherHome)EJBHomeFactory.
                lookUpLocalHome(LocalApVoucherHome.JNDI_NAME, LocalApVoucherHome.class);
            apVoucherBatchHome = (LocalApVoucherBatchHome)EJBHomeFactory.
            	lookUpLocalHome(LocalApVoucherBatchHome.JNDI_NAME, LocalApVoucherBatchHome.class);
            adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
            adDocumentSequenceAssignmentHome = (LocalAdDocumentSequenceAssignmentHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdDocumentSequenceAssignmentHome.JNDI_NAME, LocalAdDocumentSequenceAssignmentHome.class);
            apSupplierHome = (LocalApSupplierHome)EJBHomeFactory.
                lookUpLocalHome(LocalApSupplierHome.JNDI_NAME, LocalApSupplierHome.class);
            apVoucherPaymentScheduleHome = (LocalApVoucherPaymentScheduleHome)EJBHomeFactory.
                lookUpLocalHome(LocalApVoucherPaymentScheduleHome.JNDI_NAME, LocalApVoucherPaymentScheduleHome.class);
            adApprovalHome = (LocalAdApprovalHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdApprovalHome.JNDI_NAME, LocalAdApprovalHome.class);
            adAmountLimitHome = (LocalAdAmountLimitHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdAmountLimitHome.JNDI_NAME, LocalAdAmountLimitHome.class);
            adApprovalUserHome = (LocalAdApprovalUserHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdApprovalUserHome.JNDI_NAME, LocalAdApprovalUserHome.class);
            adApprovalQueueHome = (LocalAdApprovalQueueHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdApprovalQueueHome.JNDI_NAME, LocalAdApprovalQueueHome.class);
            adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);
            invItemLocationHome = (LocalInvItemLocationHome)EJBHomeFactory.
                lookUpLocalHome(LocalInvItemLocationHome.JNDI_NAME, LocalInvItemLocationHome.class);
            invCostingHome = (LocalInvCostingHome)EJBHomeFactory.
                lookUpLocalHome(LocalInvCostingHome.JNDI_NAME, LocalInvCostingHome.class);
            adBranchDocumentSequenceAssignmentHome = (LocalAdBranchDocumentSequenceAssignmentHome)EJBHomeFactory.
            	lookUpLocalHome(LocalAdBranchDocumentSequenceAssignmentHome.JNDI_NAME, LocalAdBranchDocumentSequenceAssignmentHome.class);
            adBranchItemLocationHome = (LocalAdBranchItemLocationHome)EJBHomeFactory.
        		lookUpLocalHome(LocalAdBranchItemLocationHome.JNDI_NAME, LocalAdBranchItemLocationHome.class);
            adBranchSupplierHome = (LocalAdBranchSupplierHome)EJBHomeFactory.
    			lookUpLocalHome(LocalAdBranchSupplierHome.JNDI_NAME, LocalAdBranchSupplierHome.class);

        } catch (NamingException ex) {

            throw new EJBException(ex.getMessage());

        }

        try {

        	// validate if debit memo is already deleted

        	try {

        		if (details.getVouCode() != null) {

        			apDebitMemo = apVoucherHome.findByPrimaryKey(details.getVouCode());

        		}

        	} catch (FinderException ex) {

        		throw new GlobalRecordAlreadyDeletedException();

        	}

        	// validate if debit memo is already posted, void, approved or pending or locked

	        if (details.getVouCode() != null) {

	        	if (apDebitMemo.getVouApprovalStatus() != null) {

	        		if (apDebitMemo.getVouApprovalStatus().equals("APPROVED") ||
		        		apDebitMemo.getVouApprovalStatus().equals("N/A")) {

	        		    throw new GlobalTransactionAlreadyApprovedException();

	        		} else if (apDebitMemo.getVouApprovalStatus().equals("PENDING")) {

	        			throw new GlobalTransactionAlreadyPendingException();

	        		}

	        	}

        		if (apDebitMemo.getVouPosted() == EJBCommon.TRUE) {

        			throw new GlobalTransactionAlreadyPostedException();

        		} else if (apDebitMemo.getVouVoid() == EJBCommon.TRUE) {

        			throw new GlobalTransactionAlreadyVoidException();

        		}

        	}


        	// debit memo void

	    	if (details.getVouCode() != null && details.getVouVoid() == EJBCommon.TRUE &&
	    	    apDebitMemo.getVouPosted() == EJBCommon.FALSE) {

        		// release lock

        		LocalApVoucher apLockedVoucher = apVoucherHome.findByVouDocumentNumberAndVouDebitMemoAndBrCode(apDebitMemo.getVouDmVoucherNumber(),
        		    EJBCommon.FALSE, AD_BRNCH, AD_CMPNY);

        		Collection apLockedVoucherPaymentSchedules = apLockedVoucher.getApVoucherPaymentSchedules();

        		Iterator vpsIter = apLockedVoucherPaymentSchedules.iterator();

        		while (vpsIter.hasNext()) {

        			LocalApVoucherPaymentSchedule apVoucherPaymentSchedule =
        			    (LocalApVoucherPaymentSchedule)vpsIter.next();

        			apVoucherPaymentSchedule.setVpsLock(EJBCommon.FALSE);

        		}

        		apDebitMemo.setVouVoid(EJBCommon.TRUE);
        		apDebitMemo.setVouLastModifiedBy(details.getVouLastModifiedBy());
        		apDebitMemo.setVouDateLastModified(details.getVouDateLastModified());

        		return apDebitMemo.getVouCode();

	    	}

	    	// validate if voucher number exists

        	try {

        		apVoucher = apVoucherHome.findByVouDocumentNumberAndVouDebitMemoAndBrCode(details.getVouDmVoucherNumber(), EJBCommon.FALSE, AD_BRNCH, AD_CMPNY);

        		if (apVoucher.getVouPosted() != EJBCommon.TRUE) {

        			throw new GlobalNoRecordFoundException();

        		}

        	} catch (FinderException ex) {

        		throw new GlobalNoRecordFoundException();

        	}


        	// validate if document number is unique document number is automatic then set next sequence

        	try {

        	    LocalAdBranchDocumentSequenceAssignment adBranchDocumentSequenceAssignment = null;
	 			LocalAdDocumentSequenceAssignment adDocumentSequenceAssignment = null;

	        	if (details.getVouCode() == null) {

	        	    try {

	        	        adDocumentSequenceAssignment = adDocumentSequenceAssignmentHome.findByDcName("AP DEBIT MEMO", AD_CMPNY);

	        	    } catch(FinderException ex) { }

	        	    try {

	 					adBranchDocumentSequenceAssignment = adBranchDocumentSequenceAssignmentHome.findBdsByDsaCodeAndBrCode(adDocumentSequenceAssignment.getDsaCode(), AD_BRNCH, AD_CMPNY);

	 				} catch (FinderException ex) { }

		    		LocalApVoucher apExistingDebitMemo = null;

		    		try {

		    		    apExistingDebitMemo = apVoucherHome.findByVouDocumentNumberAndVouDebitMemoAndBrCode(
		        		    details.getVouDocumentNumber(), EJBCommon.TRUE, AD_BRNCH, AD_CMPNY);

		        	} catch (FinderException ex) {
		        	}

		            if (apExistingDebitMemo != null) {

		            	throw new GlobalDocumentNumberNotUniqueException();

		            }

			        if (adDocumentSequenceAssignment.getAdDocumentSequence().getDsNumberingType() == 'A' &&
			            (details.getVouDocumentNumber() == null || details.getVouDocumentNumber().trim().length() == 0)) {

			            while (true) {

			                if (adBranchDocumentSequenceAssignment == null || adBranchDocumentSequenceAssignment.getBdsNextSequence() == null) {

				            	try {

				            		apVoucherHome.findByVouDocumentNumberAndVouDebitMemoAndBrCode(adDocumentSequenceAssignment.getDsaNextSequence(), EJBCommon.TRUE, AD_BRNCH, AD_CMPNY);
				            		adDocumentSequenceAssignment.setDsaNextSequence(EJBCommon.incrementStringNumber(adDocumentSequenceAssignment.getDsaNextSequence()));

				            	} catch (FinderException ex) {

				            		details.setVouDocumentNumber(adDocumentSequenceAssignment.getDsaNextSequence());
				            		adDocumentSequenceAssignment.setDsaNextSequence(EJBCommon.incrementStringNumber(adDocumentSequenceAssignment.getDsaNextSequence()));
						            break;

				            	}

			                } else {

			                    try {

				            		apVoucherHome.findByVouDocumentNumberAndVouDebitMemoAndBrCode(adBranchDocumentSequenceAssignment.getBdsNextSequence(), EJBCommon.TRUE, AD_BRNCH, AD_CMPNY);
				            		adBranchDocumentSequenceAssignment.setBdsNextSequence(EJBCommon.incrementStringNumber(adBranchDocumentSequenceAssignment.getBdsNextSequence()));

				            	} catch (FinderException ex) {

				            		details.setVouDocumentNumber(adBranchDocumentSequenceAssignment.getBdsNextSequence());
				            		adBranchDocumentSequenceAssignment.setBdsNextSequence(EJBCommon.incrementStringNumber(adBranchDocumentSequenceAssignment.getBdsNextSequence()));
						            break;

				            	}

			                }

			            }

			        }

			    } else {

			    	LocalApVoucher apExistingDebitMemo = null;

			    	try {

		    		    apExistingDebitMemo = apVoucherHome.findByVouDocumentNumberAndVouDebitMemoAndBrCode(
		        		    details.getVouDocumentNumber(), EJBCommon.TRUE, AD_BRNCH, AD_CMPNY);

		        	} catch (FinderException ex) {
		        	}

		        	if (apExistingDebitMemo != null &&
		                !apExistingDebitMemo.getVouCode().equals(details.getVouCode())) {

		            	throw new GlobalDocumentNumberNotUniqueException();

		            }

		            if (apDebitMemo.getVouDocumentNumber() != details.getVouDocumentNumber() &&
		                (details.getVouDocumentNumber() == null || details.getVouDocumentNumber().trim().length() == 0)) {

		                details.setVouDocumentNumber(apDebitMemo.getVouDocumentNumber());

		         	}

			    }

        	} catch (GlobalDocumentNumberNotUniqueException ex) {

	 			getSessionContext().setRollbackOnly();
	 			throw ex;

	 		} catch (Exception ex) {

	 			Debug.printStackTrace(ex);
	 			getSessionContext().setRollbackOnly();
	 			throw new EJBException(ex.getMessage());

	 		}


        	// validate if voucher entered is already locked by dm or check

        	Collection apValidateVoucherPaymentSchedules =
        	        apVoucherPaymentScheduleHome.findByVpsLockAndVouCode(EJBCommon.TRUE, apVoucher.getVouCode(), AD_CMPNY);

            if (details.getVouCode() == null && !apValidateVoucherPaymentSchedules.isEmpty() ||
                details.getVouCode() != null && !details.getVouDmVoucherNumber().equals(apDebitMemo.getVouDmVoucherNumber()) &&
                !apValidateVoucherPaymentSchedules.isEmpty()) {

                throw new GlobalTransactionAlreadyLockedException();

            }

            // used in checking if debit memo should re-generate distribution records and re-calculate taxes

	        boolean isRecalculate = true;

        	// create voucher

        	if (details.getVouCode() == null) {


				apDebitMemo = apVoucherHome.create(EJBCommon.TRUE,
				    details.getVouDescription(), details.getVouDate(),
				    details.getVouDocumentNumber(), null, details.getVouDmVoucherNumber(),
				    null, 0d,
				    0d, 0d,
				    0d, null, null, EJBCommon.FALSE, EJBCommon.FALSE, EJBCommon.FALSE, details.getVouCreatedBy(), details.getVouDateCreated(),
        	    	details.getVouLastModifiedBy(), details.getVouDateLastModified(),
        	    	null, null, null, null, EJBCommon.FALSE, null,
        	    	EJBCommon.FALSE, EJBCommon.FALSE,
        	    	AD_BRNCH, AD_CMPNY);


        	} else {

        		// release lock

	    	    LocalApVoucher apLockedVoucher = apVoucherHome.findByVouDocumentNumberAndVouDebitMemoAndBrCode(apDebitMemo.getVouDmVoucherNumber(),
        		    EJBCommon.FALSE, AD_BRNCH, AD_CMPNY);

        		Collection apLockedVoucherPaymentSchedules = apLockedVoucher.getApVoucherPaymentSchedules();

        		Iterator vpsIter = apLockedVoucherPaymentSchedules.iterator();

        		while (vpsIter.hasNext()) {

        			LocalApVoucherPaymentSchedule apVoucherPaymentSchedule =
        			    (LocalApVoucherPaymentSchedule)vpsIter.next();

        			apVoucherPaymentSchedule.setVpsLock(EJBCommon.FALSE);

        		}

        		// check if critical fields are changed

        		if (!apDebitMemo.getApSupplier().getSplSupplierCode().equals(SPL_SPPLR_CODE) ||
        		    !apDebitMemo.getVouDmVoucherNumber().equals(details.getVouDmVoucherNumber()) ||
					vliList.size() != apDebitMemo.getApVoucherLineItems().size() ||
					!(apDebitMemo.getVouDate().equals(details.getVouDate()))) {

        			isRecalculate = true;

        		} else if (vliList.size() == apDebitMemo.getApVoucherLineItems().size()) {

        			Iterator vliIter = apDebitMemo.getApVoucherLineItems().iterator();
        			Iterator vliListIter = vliList.iterator();

        			while (vliIter.hasNext()) {

        				LocalApVoucherLineItem apVoucherLineItem = (LocalApVoucherLineItem)vliIter.next();
        				ApModVoucherLineItemDetails mdetails = (ApModVoucherLineItemDetails)vliListIter.next();

        				if (!apVoucherLineItem.getInvItemLocation().getInvItem().getIiName().equals(mdetails.getVliIiName()) ||
        				    !apVoucherLineItem.getInvItemLocation().getInvLocation().getLocName().equals(mdetails.getVliLocName()) ||
        				    !apVoucherLineItem.getInvUnitOfMeasure().getUomName().equals(mdetails.getVliUomName()) ||
							apVoucherLineItem.getVliQuantity() != mdetails.getVliQuantity() ||
							apVoucherLineItem.getVliUnitCost() != mdetails.getVliUnitCost()) {

        					isRecalculate = true;
        					break;

        				}

        				isRecalculate = false;

        			}

        		} else {

        			isRecalculate = false;

        		}

        		apDebitMemo.setVouDescription(details.getVouDescription());
        		apDebitMemo.setVouDate(details.getVouDate());
        		apDebitMemo.setVouDocumentNumber(details.getVouDocumentNumber());
        		apDebitMemo.setVouDmVoucherNumber(details.getVouDmVoucherNumber());
        		apDebitMemo.setVouLastModifiedBy(details.getVouLastModifiedBy());
        		apDebitMemo.setVouDateLastModified(details.getVouDateLastModified());
        		apDebitMemo.setVouReasonForRejection(null);

        	}

        	LocalApSupplier apSupplier = apSupplierHome.findBySplSupplierCode(SPL_SPPLR_CODE, AD_CMPNY);
        	apSupplier.addApVoucher(apDebitMemo);

        	LocalApTaxCode apTaxCode = apVoucher.getApTaxCode();

        	LocalApWithholdingTaxCode apWithholdingTaxCode = apVoucher.getApWithholdingTaxCode();

        	try {

        		LocalApVoucherBatch apVoucherBatch = apVoucherBatchHome.findByVbName(VB_NM, AD_BRNCH, AD_CMPNY);
        		apVoucherBatch.addApVoucher(apDebitMemo);

        	} catch (FinderException ex) {

        	}

        	LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);

        	if (isRecalculate) {

	        	// remove all demo memo lines

	        	Collection apVoucherLineItems = apDebitMemo.getApVoucherLineItems();

		  	    Iterator i = apVoucherLineItems.iterator();

		  	    while (i.hasNext()) {

		  	   	    LocalApVoucherLineItem apVoucherLineItem = (LocalApVoucherLineItem)i.next();

		  	  	    i.remove();

		  	  	    apVoucherLineItem.remove();

		  	    }

	        	// remove all distribution records

		  	    Collection apDistributionRecords = apDebitMemo.getApDistributionRecords();

		  	    i = apDistributionRecords.iterator();

		  	    while (i.hasNext()) {

		  	   	    LocalApDistributionRecord apDistributionRecord = (LocalApDistributionRecord)i.next();

		  	  	    i.remove();

		  	  	    apDistributionRecord.remove();

		  	    }

		  	    // add new invoice lines and distribution record

		  	    double TOTAL_TAX = 0d;
		  	    double TOTAL_LINE = 0d;

	      	    i = vliList.iterator();

	      	    LocalInvItemLocation invItemLocation = null;

	      	    while (i.hasNext()) {

	      	  	    ApModVoucherLineItemDetails mVliDetails = (ApModVoucherLineItemDetails) i.next();

	      	  	    try {

	 	  	    		invItemLocation = invItemLocationHome.findByLocNameAndIiName(mVliDetails.getVliLocName(), mVliDetails.getVliIiName(), AD_CMPNY);

	 	  	    	} catch (FinderException ex) {

	 	  	    		throw new GlobalInvItemLocationNotFoundException(String.valueOf(mVliDetails.getVliLine()));

	 	  	    	}

	 	  	    	// start date validation
        			System.out.println("Check A");

        			if (adPreference.getPrfArAllowPriorDate() == EJBCommon.FALSE) {
        				Collection invNegTxnCosting = invCostingHome.findNegTxnByGreaterThanCstDateAndIiNameAndLocName(
        	  	    			apDebitMemo.getVouDate(), invItemLocation.getInvItem().getIiName(),
        						invItemLocation.getInvLocation().getLocName(), AD_BRNCH, AD_CMPNY);
        	  	    		if(!invNegTxnCosting.isEmpty()) throw new GlobalInventoryDateException(invItemLocation.getInvItem().getIiName());
        			}


	  	    		System.out.println("Check B");
	      	  	    LocalApVoucherLineItem apVoucherLineItem = this.addApVliEntry(mVliDetails, apDebitMemo, apTaxCode, invItemLocation, AD_CMPNY);

	      	  	    // add cost of sales distribution and inventory

	 	  	    	//double COST =  apVoucherLineItem.getVliUnitCost();

					double QTY_RCVD = this.convertByUomFromAndItemAndQuantity(apVoucherLineItem.getInvUnitOfMeasure(),
    						apVoucherLineItem.getInvItemLocation().getInvItem(), apVoucherLineItem.getVliQuantity(), AD_CMPNY);

					LocalAdBranchItemLocation adBranchItemLocation = null;

					try {

					    adBranchItemLocation = adBranchItemLocationHome.findBilByIlCodeAndBrCode(invItemLocation.getIlCode(), AD_BRNCH, AD_CMPNY);

					} catch (FinderException ex) { }

					if(adBranchItemLocation != null) {

					    this.addApDrEntry(apDebitMemo.getApDrNextLine(),
				            "EXPENSE", EJBCommon.FALSE, apVoucherLineItem.getVliAmount(),
				            adBranchItemLocation.getBilCoaGlInventoryAccount(), apDebitMemo, AD_BRNCH, AD_CMPNY);

					} else {

					    this.addApDrEntry(apDebitMemo.getApDrNextLine(),
		      	  	        "EXPENSE", EJBCommon.FALSE, apVoucherLineItem.getVliAmount(),
		      	  	        invItemLocation.getIlGlCoaInventoryAccount(), apDebitMemo, AD_BRNCH, AD_CMPNY);

					}

	      	  	    //TOTAL_LINE += COST * QTY_RCVD;
	      	  	    TOTAL_LINE += apVoucherLineItem.getVliAmount();
	      	  	    TOTAL_TAX += apVoucherLineItem.getVliTaxAmount();

	      	    }


	      	    // add tax distribution if necessary

	      	    if (!apTaxCode.getTcType().equals("NONE") &&
	        	    !apTaxCode.getTcType().equals("EXEMPT")) {

	  	    	    this.addApDrEntry(apDebitMemo.getApDrNextLine(),
		      	        "TAX", EJBCommon.FALSE, TOTAL_TAX, apTaxCode.getGlChartOfAccount().getCoaCode(),
		      	        apDebitMemo, AD_BRNCH, AD_CMPNY);

		        }

	      	    // add wtax distribution if necessary

	      	   // LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);

	      	    double W_TAX_AMOUNT = 0d;

	      	    if (apWithholdingTaxCode.getWtcRate() != 0 && adPreference.getPrfApWTaxRealization().equals("VOUCHER")) {

	      	    	W_TAX_AMOUNT = EJBCommon.roundIt(TOTAL_LINE * (apWithholdingTaxCode.getWtcRate() / 100), this.getGlFcPrecisionUnit(AD_CMPNY));

	      	    	this.addApDrEntry(apDebitMemo.getApDrNextLine(), "W-TAX",
	      	    	    EJBCommon.TRUE, W_TAX_AMOUNT, apWithholdingTaxCode.getGlChartOfAccount().getCoaCode(),
	      	    	    apDebitMemo, AD_BRNCH, AD_CMPNY);

	      	    }


	      	    // add payable distribution

	      	    LocalAdBranchSupplier adBranchSupplier = null;

	      	    try {

	      	        adBranchSupplier = adBranchSupplierHome.findBSplBySplCodeAndBrCode(apDebitMemo.getApSupplier().getSplCode(), AD_BRNCH, AD_CMPNY);

	      	    } catch (FinderException ex) { }

	      	    if(adBranchSupplier != null) {

	      	        this.addApDrEntry(apDebitMemo.getApDrNextLine(), "PAYABLE",
      	                EJBCommon.TRUE, TOTAL_LINE + TOTAL_TAX - W_TAX_AMOUNT,
      	                adBranchSupplier.getBsplGlCoaPayableAccount(),
      	                apDebitMemo, AD_BRNCH, AD_CMPNY);

	      	    } else {

		      	    this.addApDrEntry(apDebitMemo.getApDrNextLine(), "PAYABLE",
		      	        EJBCommon.TRUE, TOTAL_LINE + TOTAL_TAX - W_TAX_AMOUNT,
		      	        apDebitMemo.getApSupplier().getSplCoaGlPayableAccount(),
		      	        apDebitMemo, AD_BRNCH, AD_CMPNY);

	      	    }

	      	    // set invoice amount due


	      	    apDebitMemo.setVouBillAmount(TOTAL_LINE + TOTAL_TAX - W_TAX_AMOUNT);

	      	    if(adPreference.getPrfApDebitMemoOverrideCost()!=1){
	      	    	if (apDebitMemo.getVouBillAmount() > apVoucher.getVouAmountDue() - apVoucher.getVouAmountPaid()) {

	      	    		throw new ApVOUOverapplicationNotAllowedException();

	      	    	}

	      	    }

	      	    // create new voucher payment schedule lock

	      	    Collection apVoucherPaymentSchedules =
	      	       apVoucher.getApVoucherPaymentSchedules();

      	   		i = apVoucherPaymentSchedules.iterator();

      	   		while (i.hasNext()) {

	      	   	   LocalApVoucherPaymentSchedule apVoucherPaymentSchedule =
	        		   (LocalApVoucherPaymentSchedule)i.next();

        	   	   apVoucherPaymentSchedule.setVpsLock(EJBCommon.TRUE);

      	        }

        	} else {

    			Iterator i = vliList.iterator();

        		LocalInvItemLocation invItemLocation = null;

        		while (i.hasNext()) {

        			ApModVoucherLineItemDetails mVliDetails = (ApModVoucherLineItemDetails) i.next();

        			try {

        				invItemLocation = invItemLocationHome.findByLocNameAndIiName(mVliDetails.getVliLocName(), mVliDetails.getVliIiName(), AD_CMPNY);

        			} catch (FinderException ex) {

        				throw new GlobalInvItemLocationNotFoundException(String.valueOf(mVliDetails.getVliLine()));

        			}

        			//	start date validation
        			System.out.println("Check C");
        			if (adPreference.getPrfArAllowPriorDate() == EJBCommon.FALSE) {
        				Collection invNegTxnCosting = invCostingHome.findNegTxnByGreaterThanCstDateAndIiNameAndLocName(
        	  	    			apDebitMemo.getVouDate(), invItemLocation.getInvItem().getIiName(),
        						invItemLocation.getInvLocation().getLocName(), AD_BRNCH, AD_CMPNY);
        	  	    		if(!invNegTxnCosting.isEmpty()) throw new GlobalInventoryDateException(invItemLocation.getInvItem().getIiName());
        			}
	  	    		System.out.println("Check D");
        		}

    		}

      	   // generate approval status

            String VOU_APPRVL_STATUS = null;

        	if (!isDraft) {

        		LocalAdApproval adApproval = adApprovalHome.findByAprAdCompany(AD_CMPNY);

        		// check if ap voucher approval is enabled

        		if (adApproval.getAprEnableApDebitMemo() == EJBCommon.FALSE) {

        			VOU_APPRVL_STATUS = "N/A";

        		} else {

        			// check if voucher is self approved

        			LocalAdAmountLimit adAmountLimit = null;

        			try {

        				adAmountLimit = adAmountLimitHome.findByAdcTypeAndAuTypeAndUsrName("AP DEBIT MEMO", "REQUESTER", details.getVouLastModifiedBy(), AD_CMPNY);

        			} catch (FinderException ex) {

        				throw new GlobalNoApprovalRequesterFoundException();

        			}

        			if (apDebitMemo.getVouBillAmount() <= adAmountLimit.getCalAmountLimit()) {

        				VOU_APPRVL_STATUS = "N/A";

        			} else {

        				// for approval, create approval queue

        				 Collection adAmountLimits = adAmountLimitHome.findByAdcTypeAndGreaterThanCalAmountLimit("AP DEBIT MEMO", adAmountLimit.getCalAmountLimit(), AD_CMPNY);

        				 if (adAmountLimits.isEmpty()) {

        				 	Collection adApprovalUsers = adApprovalUserHome.findByAuTypeAndCalCode("APPROVER", adAmountLimit.getCalCode(), AD_CMPNY);

        				 	if (adApprovalUsers.isEmpty()) {

        				 		throw new GlobalNoApprovalApproverFoundException();

        				 	}

        				 	Iterator j = adApprovalUsers.iterator();

        				 	while (j.hasNext()) {

        				 		LocalAdApprovalUser adApprovalUser = (LocalAdApprovalUser)j.next();

        				 		LocalAdApprovalQueue adApprovalQueue = adApprovalQueueHome.create(EJBCommon.TRUE, "AP DEBIT MEMO", apDebitMemo.getVouCode(),
        				 				apDebitMemo.getVouDocumentNumber(), apDebitMemo.getVouDate(), adAmountLimit.getCalAndOr(), adApprovalUser.getAuOr(), AD_BRNCH, AD_CMPNY);

        				 		adApprovalUser.getAdUser().addAdApprovalQueue(adApprovalQueue);

        				 	}

        				 } else {

        				 	boolean isApprovalUsersFound = false;

        				 	Iterator i = adAmountLimits.iterator();

        				 	while (i.hasNext()) {

        				 		LocalAdAmountLimit adNextAmountLimit = (LocalAdAmountLimit)i.next();

        				 		if (apDebitMemo.getVouBillAmount() <= adNextAmountLimit.getCalAmountLimit()) {

        				 			Collection adApprovalUsers = adApprovalUserHome.findByAuTypeAndCalCode("APPROVER", adAmountLimit.getCalCode(), AD_CMPNY);

        				 			Iterator j = adApprovalUsers.iterator();

		        				 	while (j.hasNext()) {

		        				 		isApprovalUsersFound = true;

		        				 		LocalAdApprovalUser adApprovalUser = (LocalAdApprovalUser)j.next();

		        				 		LocalAdApprovalQueue adApprovalQueue = adApprovalQueueHome.create(EJBCommon.TRUE, "AP DEBIT MEMO", apDebitMemo.getVouCode(),
		        				 				apDebitMemo.getVouDocumentNumber(), apDebitMemo.getVouDate(), adAmountLimit.getCalAndOr(), adApprovalUser.getAuOr(), AD_BRNCH, AD_CMPNY);

		        				 		adApprovalUser.getAdUser().addAdApprovalQueue(adApprovalQueue);

		        				 	}

	    				 			break;

        				 		} else if (!i.hasNext()) {

        				 			Collection adApprovalUsers = adApprovalUserHome.findByAuTypeAndCalCode("APPROVER", adNextAmountLimit.getCalCode(), AD_CMPNY);

        				 			Iterator j = adApprovalUsers.iterator();

		        				 	while (j.hasNext()) {

		        				 		isApprovalUsersFound = true;

		        				 		LocalAdApprovalUser adApprovalUser = (LocalAdApprovalUser)j.next();

		        				 		LocalAdApprovalQueue adApprovalQueue = adApprovalQueueHome.create(EJBCommon.TRUE, "AP DEBIT MEMO", apDebitMemo.getVouCode(),
		        				 				apDebitMemo.getVouDocumentNumber(), apDebitMemo.getVouDate(), adNextAmountLimit.getCalAndOr(), adApprovalUser.getAuOr(), AD_BRNCH, AD_CMPNY);

		        				 		adApprovalUser.getAdUser().addAdApprovalQueue(adApprovalQueue);

		        				 	}

	    				 			break;

        				 		}

        				 		adAmountLimit = adNextAmountLimit;

        				 	}

        				 	if (!isApprovalUsersFound) {

        				 		throw new GlobalNoApprovalApproverFoundException();

        				 	}

        			    }

        			    VOU_APPRVL_STATUS = "PENDING";
        			}
        		}
        	}

        	//LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);

        	if (VOU_APPRVL_STATUS != null && VOU_APPRVL_STATUS.equals("N/A") && adPreference.getPrfApGlPostingType().equals("AUTO-POST UPON APPROVAL")) {

        		this.executeApDebitMemoPost(apDebitMemo.getVouCode(), apDebitMemo.getVouLastModifiedBy(), AD_BRNCH, AD_CMPNY);

        	}

        	// set debit memo approval status

        	apDebitMemo.setVouApprovalStatus(VOU_APPRVL_STATUS);


      	    return apDebitMemo.getVouCode();


        } catch (GlobalRecordAlreadyDeletedException ex) {

        	getSessionContext().setRollbackOnly();
        	throw ex;

        } catch (GlobalNoRecordFoundException ex) {

        	getSessionContext().setRollbackOnly();
        	throw ex;

        } catch (GlobalDocumentNumberNotUniqueException ex) {

        	getSessionContext().setRollbackOnly();
        	throw ex;

        } catch (GlobalTransactionAlreadyApprovedException ex) {

        	getSessionContext().setRollbackOnly();
        	throw ex;

        } catch (GlobalTransactionAlreadyPendingException ex) {

        	getSessionContext().setRollbackOnly();
        	throw ex;

        } catch (GlobalTransactionAlreadyPostedException ex) {

        	getSessionContext().setRollbackOnly();
        	throw ex;

        } catch (GlobalTransactionAlreadyVoidException ex) {

        	getSessionContext().setRollbackOnly();
        	throw ex;

        } catch (ApVOUOverapplicationNotAllowedException ex) {

        	getSessionContext().setRollbackOnly();
        	throw ex;

        } catch (GlobalTransactionAlreadyLockedException ex) {

        	getSessionContext().setRollbackOnly();
        	throw ex;

        } catch (GlobalNoApprovalRequesterFoundException ex) {

        	getSessionContext().setRollbackOnly();
        	throw ex;

        } catch (GlobalNoApprovalApproverFoundException ex) {

        	getSessionContext().setRollbackOnly();
        	throw ex;

        } catch (GlobalInventoryDateException ex) {

        	getSessionContext().setRollbackOnly();
        	throw ex;

        } catch (GlobalBranchAccountNumberInvalidException ex) {

            getSessionContext().setRollbackOnly();
            throw ex;

        } catch (AdPRFCoaGlVarianceAccountNotFoundException ex){

            getSessionContext().setRollbackOnly();
            throw ex;

        } catch (Exception ex) {

        	Debug.printStackTrace(ex);
        	getSessionContext().setRollbackOnly();
        	throw new EJBException(ex.getMessage());

        }


    }


    /**
     * @ejb:interface-method view-type="remote"
     **/
    public Integer saveApVouVliEntryMobile(com.util.ApVoucherDetails details, String SPL_SPPLR_CODE, String VB_NM, ArrayList vliList, boolean isDraft, Integer AD_BRNCH, Integer AD_CMPNY) throws
        GlobalRecordAlreadyDeletedException,
        GlobalNoRecordFoundException,
		GlobalDocumentNumberNotUniqueException,
		GlobalTransactionAlreadyApprovedException,
		GlobalTransactionAlreadyPendingException,
		GlobalTransactionAlreadyPostedException,
		GlobalTransactionAlreadyVoidException,
		ApVOUOverapplicationNotAllowedException,
		GlobalTransactionAlreadyLockedException,
		GlobalNoApprovalRequesterFoundException,
		GlobalNoApprovalApproverFoundException,
		GlobalInvItemLocationNotFoundException,
		GlJREffectiveDateNoPeriodExistException,
		GlJREffectiveDatePeriodClosedException,
		GlobalJournalNotBalanceException,
		GlobalInventoryDateException,
		GlobalBranchAccountNumberInvalidException,
		AdPRFCoaGlVarianceAccountNotFoundException {

        Debug.print("ApDebitMemoEntryControllerBean saveApVouEntryMobile");

        LocalApVoucherHome apVoucherHome = null;
        LocalApVoucherBatchHome apVoucherBatchHome = null;
        LocalAdCompanyHome adCompanyHome = null;
        LocalAdDocumentSequenceAssignmentHome adDocumentSequenceAssignmentHome = null;
        LocalApSupplierHome apSupplierHome = null;
        LocalApVoucherPaymentScheduleHome apVoucherPaymentScheduleHome = null;
        LocalAdApprovalHome adApprovalHome = null;
        LocalAdAmountLimitHome adAmountLimitHome = null;
        LocalAdApprovalUserHome adApprovalUserHome = null;
        LocalAdApprovalQueueHome adApprovalQueueHome = null;
        LocalAdPreferenceHome adPreferenceHome = null;
        LocalInvItemLocationHome invItemLocationHome = null;
        LocalInvCostingHome invCostingHome = null;
        LocalAdBranchDocumentSequenceAssignmentHome adBranchDocumentSequenceAssignmentHome = null;
        LocalAdBranchItemLocationHome adBranchItemLocationHome = null;
        LocalAdBranchSupplierHome adBranchSupplierHome = null;

        LocalApVoucher apDebitMemo = null;
        LocalApVoucher apVoucher = null;
        LocalInvItemHome invItemHome = null;
        LocalInvLocationHome invLocationHome = null;


        // Initialize EJB Home

        try {

            apVoucherHome = (LocalApVoucherHome)EJBHomeFactory.
                lookUpLocalHome(LocalApVoucherHome.JNDI_NAME, LocalApVoucherHome.class);
            apVoucherBatchHome = (LocalApVoucherBatchHome)EJBHomeFactory.
            	lookUpLocalHome(LocalApVoucherBatchHome.JNDI_NAME, LocalApVoucherBatchHome.class);
            adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
            adDocumentSequenceAssignmentHome = (LocalAdDocumentSequenceAssignmentHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdDocumentSequenceAssignmentHome.JNDI_NAME, LocalAdDocumentSequenceAssignmentHome.class);
            apSupplierHome = (LocalApSupplierHome)EJBHomeFactory.
                lookUpLocalHome(LocalApSupplierHome.JNDI_NAME, LocalApSupplierHome.class);
            apVoucherPaymentScheduleHome = (LocalApVoucherPaymentScheduleHome)EJBHomeFactory.
                lookUpLocalHome(LocalApVoucherPaymentScheduleHome.JNDI_NAME, LocalApVoucherPaymentScheduleHome.class);
            adApprovalHome = (LocalAdApprovalHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdApprovalHome.JNDI_NAME, LocalAdApprovalHome.class);
            adAmountLimitHome = (LocalAdAmountLimitHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdAmountLimitHome.JNDI_NAME, LocalAdAmountLimitHome.class);
            adApprovalUserHome = (LocalAdApprovalUserHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdApprovalUserHome.JNDI_NAME, LocalAdApprovalUserHome.class);
            adApprovalQueueHome = (LocalAdApprovalQueueHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdApprovalQueueHome.JNDI_NAME, LocalAdApprovalQueueHome.class);
            adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);
            invItemLocationHome = (LocalInvItemLocationHome)EJBHomeFactory.
                lookUpLocalHome(LocalInvItemLocationHome.JNDI_NAME, LocalInvItemLocationHome.class);
            invCostingHome = (LocalInvCostingHome)EJBHomeFactory.
                lookUpLocalHome(LocalInvCostingHome.JNDI_NAME, LocalInvCostingHome.class);
            adBranchDocumentSequenceAssignmentHome = (LocalAdBranchDocumentSequenceAssignmentHome)EJBHomeFactory.
            	lookUpLocalHome(LocalAdBranchDocumentSequenceAssignmentHome.JNDI_NAME, LocalAdBranchDocumentSequenceAssignmentHome.class);
            adBranchItemLocationHome = (LocalAdBranchItemLocationHome)EJBHomeFactory.
        		lookUpLocalHome(LocalAdBranchItemLocationHome.JNDI_NAME, LocalAdBranchItemLocationHome.class);
            adBranchSupplierHome = (LocalAdBranchSupplierHome)EJBHomeFactory.
    			lookUpLocalHome(LocalAdBranchSupplierHome.JNDI_NAME, LocalAdBranchSupplierHome.class);
            invItemHome = (LocalInvItemHome)EJBHomeFactory.
    			lookUpLocalHome(LocalInvItemHome.JNDI_NAME, LocalInvItemHome.class);
            invLocationHome = (LocalInvLocationHome)EJBHomeFactory.
    			lookUpLocalHome(LocalInvLocationHome.JNDI_NAME, LocalInvLocationHome.class);

        } catch (NamingException ex) {

            throw new EJBException(ex.getMessage());

        }

        try {

        	// validate if debit memo is already deleted

        	try {

        		if (details.getVouCode() != null) {

        			apDebitMemo = apVoucherHome.findByPrimaryKey(details.getVouCode());

        		}

        	} catch (FinderException ex) {

        		throw new GlobalRecordAlreadyDeletedException();

        	}

        	// validate if debit memo is already posted, void, approved or pending or locked

	        if (details.getVouCode() != null) {

	        	if (apDebitMemo.getVouApprovalStatus() != null) {

	        		if (apDebitMemo.getVouApprovalStatus().equals("APPROVED") ||
		        		apDebitMemo.getVouApprovalStatus().equals("N/A")) {

	        		    throw new GlobalTransactionAlreadyApprovedException();

	        		} else if (apDebitMemo.getVouApprovalStatus().equals("PENDING")) {

	        			throw new GlobalTransactionAlreadyPendingException();

	        		}

	        	}

        		if (apDebitMemo.getVouPosted() == EJBCommon.TRUE) {

        			throw new GlobalTransactionAlreadyPostedException();

        		} else if (apDebitMemo.getVouVoid() == EJBCommon.TRUE) {

        			throw new GlobalTransactionAlreadyVoidException();

        		}

        	}


        	// debit memo void

	    	if (details.getVouCode() != null && details.getVouVoid() == EJBCommon.TRUE &&
	    	    apDebitMemo.getVouPosted() == EJBCommon.FALSE) {

        		// release lock

        		LocalApVoucher apLockedVoucher = apVoucherHome.findByVouDocumentNumberAndVouDebitMemoAndBrCode(apDebitMemo.getVouDmVoucherNumber(),
        		    EJBCommon.FALSE, AD_BRNCH, AD_CMPNY);

        		Collection apLockedVoucherPaymentSchedules = apLockedVoucher.getApVoucherPaymentSchedules();

        		Iterator vpsIter = apLockedVoucherPaymentSchedules.iterator();

        		while (vpsIter.hasNext()) {

        			LocalApVoucherPaymentSchedule apVoucherPaymentSchedule =
        			    (LocalApVoucherPaymentSchedule)vpsIter.next();

        			apVoucherPaymentSchedule.setVpsLock(EJBCommon.FALSE);

        		}

        		apDebitMemo.setVouVoid(EJBCommon.TRUE);
        		apDebitMemo.setVouLastModifiedBy(details.getVouLastModifiedBy());
        		apDebitMemo.setVouDateLastModified(details.getVouDateLastModified());

        		return apDebitMemo.getVouCode();

	    	}

	    	// validate if voucher number exists

        	try {

        		apVoucher = apVoucherHome.findByVouDocumentNumberAndVouDebitMemoAndBrCode(details.getVouDmVoucherNumber(), EJBCommon.FALSE, AD_BRNCH, AD_CMPNY);

        		if (apVoucher.getVouPosted() != EJBCommon.TRUE) {

        			throw new GlobalNoRecordFoundException();

        		}

        	} catch (FinderException ex) {

        		throw new GlobalNoRecordFoundException();

        	}


        	// validate if document number is unique document number is automatic then set next sequence

        	try {

        	    LocalAdBranchDocumentSequenceAssignment adBranchDocumentSequenceAssignment = null;
	 			LocalAdDocumentSequenceAssignment adDocumentSequenceAssignment = null;

	        	if (details.getVouCode() == null) {

	        	    try {

	        	        adDocumentSequenceAssignment = adDocumentSequenceAssignmentHome.findByDcName("AP DEBIT MEMO", AD_CMPNY);

	        	    } catch(FinderException ex) { }

	        	    try {

	 					adBranchDocumentSequenceAssignment = adBranchDocumentSequenceAssignmentHome.findBdsByDsaCodeAndBrCode(adDocumentSequenceAssignment.getDsaCode(), AD_BRNCH, AD_CMPNY);

	 				} catch (FinderException ex) { }

		    		LocalApVoucher apExistingDebitMemo = null;

		    		try {

		    		    apExistingDebitMemo = apVoucherHome.findByVouDocumentNumberAndVouDebitMemoAndBrCode(
		        		    details.getVouDocumentNumber(), EJBCommon.TRUE, AD_BRNCH, AD_CMPNY);

		        	} catch (FinderException ex) {
		        	}

		            if (apExistingDebitMemo != null) {

		            	throw new GlobalDocumentNumberNotUniqueException();

		            }

			        if (adDocumentSequenceAssignment.getAdDocumentSequence().getDsNumberingType() == 'A' &&
			            (details.getVouDocumentNumber() == null || details.getVouDocumentNumber().trim().length() == 0)) {

			            while (true) {

			                if (adBranchDocumentSequenceAssignment == null || adBranchDocumentSequenceAssignment.getBdsNextSequence() == null) {

				            	try {

				            		apVoucherHome.findByVouDocumentNumberAndVouDebitMemoAndBrCode(adDocumentSequenceAssignment.getDsaNextSequence(), EJBCommon.TRUE, AD_BRNCH, AD_CMPNY);
				            		adDocumentSequenceAssignment.setDsaNextSequence(EJBCommon.incrementStringNumber(adDocumentSequenceAssignment.getDsaNextSequence()));

				            	} catch (FinderException ex) {

				            		details.setVouDocumentNumber(adDocumentSequenceAssignment.getDsaNextSequence());
				            		adDocumentSequenceAssignment.setDsaNextSequence(EJBCommon.incrementStringNumber(adDocumentSequenceAssignment.getDsaNextSequence()));
						            break;

				            	}

			                } else {

			                    try {

				            		apVoucherHome.findByVouDocumentNumberAndVouDebitMemoAndBrCode(adBranchDocumentSequenceAssignment.getBdsNextSequence(), EJBCommon.TRUE, AD_BRNCH, AD_CMPNY);
				            		adBranchDocumentSequenceAssignment.setBdsNextSequence(EJBCommon.incrementStringNumber(adBranchDocumentSequenceAssignment.getBdsNextSequence()));

				            	} catch (FinderException ex) {

				            		details.setVouDocumentNumber(adBranchDocumentSequenceAssignment.getBdsNextSequence());
				            		adBranchDocumentSequenceAssignment.setBdsNextSequence(EJBCommon.incrementStringNumber(adBranchDocumentSequenceAssignment.getBdsNextSequence()));
						            break;

				            	}

			                }

			            }

			        }

			    } else {

			    	LocalApVoucher apExistingDebitMemo = null;

			    	try {

		    		    apExistingDebitMemo = apVoucherHome.findByVouDocumentNumberAndVouDebitMemoAndBrCode(
		        		    details.getVouDocumentNumber(), EJBCommon.TRUE, AD_BRNCH, AD_CMPNY);

		        	} catch (FinderException ex) {
		        	}

		        	if (apExistingDebitMemo != null &&
		                !apExistingDebitMemo.getVouCode().equals(details.getVouCode())) {

		            	throw new GlobalDocumentNumberNotUniqueException();

		            }

		            if (apDebitMemo.getVouDocumentNumber() != details.getVouDocumentNumber() &&
		                (details.getVouDocumentNumber() == null || details.getVouDocumentNumber().trim().length() == 0)) {

		                details.setVouDocumentNumber(apDebitMemo.getVouDocumentNumber());

		         	}

			    }

        	} catch (GlobalDocumentNumberNotUniqueException ex) {

	 			getSessionContext().setRollbackOnly();
	 			throw ex;

	 		} catch (Exception ex) {

	 			Debug.printStackTrace(ex);
	 			getSessionContext().setRollbackOnly();
	 			throw new EJBException(ex.getMessage());

	 		}


        	// validate if voucher entered is already locked by dm or check

        	Collection apValidateVoucherPaymentSchedules =
        	        apVoucherPaymentScheduleHome.findByVpsLockAndVouCode(EJBCommon.TRUE, apVoucher.getVouCode(), AD_CMPNY);

            if (details.getVouCode() == null && !apValidateVoucherPaymentSchedules.isEmpty() ||
                details.getVouCode() != null && !details.getVouDmVoucherNumber().equals(apDebitMemo.getVouDmVoucherNumber()) &&
                !apValidateVoucherPaymentSchedules.isEmpty()) {

                throw new GlobalTransactionAlreadyLockedException();

            }

            // used in checking if debit memo should re-generate distribution records and re-calculate taxes

	        boolean isRecalculate = true;

        	// create voucher

        	if (details.getVouCode() == null) {


				apDebitMemo = apVoucherHome.create(EJBCommon.TRUE,
				    details.getVouDescription(), details.getVouDate(),
				    details.getVouDocumentNumber(), null, details.getVouDmVoucherNumber(),
				    null, 0d,
				    0d, 0d,
				    0d, null, null, EJBCommon.FALSE, EJBCommon.FALSE, EJBCommon.FALSE, details.getVouCreatedBy(), details.getVouDateCreated(),
        	    	details.getVouLastModifiedBy(), details.getVouDateLastModified(),
        	    	null, null, null, null, EJBCommon.FALSE, null,
        	    	EJBCommon.FALSE, EJBCommon.FALSE,
        	    	AD_BRNCH, AD_CMPNY);


        	} else {

        		// release lock

	    	    LocalApVoucher apLockedVoucher = apVoucherHome.findByVouDocumentNumberAndVouDebitMemoAndBrCode(apDebitMemo.getVouDmVoucherNumber(),
        		    EJBCommon.FALSE, AD_BRNCH, AD_CMPNY);

        		Collection apLockedVoucherPaymentSchedules = apLockedVoucher.getApVoucherPaymentSchedules();

        		Iterator vpsIter = apLockedVoucherPaymentSchedules.iterator();

        		while (vpsIter.hasNext()) {

        			LocalApVoucherPaymentSchedule apVoucherPaymentSchedule =
        			    (LocalApVoucherPaymentSchedule)vpsIter.next();

        			apVoucherPaymentSchedule.setVpsLock(EJBCommon.FALSE);

        		}

        		// check if critical fields are changed

        		if (!apDebitMemo.getApSupplier().getSplSupplierCode().equals(SPL_SPPLR_CODE) ||
        		    !apDebitMemo.getVouDmVoucherNumber().equals(details.getVouDmVoucherNumber()) ||
					vliList.size() != apDebitMemo.getApVoucherLineItems().size() ||
					!(apDebitMemo.getVouDate().equals(details.getVouDate()))) {

        			isRecalculate = true;

        		} else if (vliList.size() == apDebitMemo.getApVoucherLineItems().size()) {

        			Iterator vliIter = apDebitMemo.getApVoucherLineItems().iterator();
        			Iterator vliListIter = vliList.iterator();

        			while (vliIter.hasNext()) {

        				LocalApVoucherLineItem apVoucherLineItem = (LocalApVoucherLineItem)vliIter.next();
        				ApModVoucherLineItemDetails mdetails = (ApModVoucherLineItemDetails)vliListIter.next();

        				if (!apVoucherLineItem.getInvItemLocation().getInvItem().getIiName().equals(mdetails.getVliIiName()) ||
        				    !apVoucherLineItem.getInvItemLocation().getInvLocation().getLocName().equals(mdetails.getVliLocName()) ||
        				    !apVoucherLineItem.getInvUnitOfMeasure().getUomName().equals(mdetails.getVliUomName()) ||
							apVoucherLineItem.getVliQuantity() != mdetails.getVliQuantity() ||
							apVoucherLineItem.getVliUnitCost() != mdetails.getVliUnitCost()) {

        					isRecalculate = true;
        					break;

        				}

        				isRecalculate = false;

        			}

        		} else {

        			isRecalculate = false;

        		}

        		apDebitMemo.setVouDescription(details.getVouDescription());
        		apDebitMemo.setVouDate(details.getVouDate());
        		apDebitMemo.setVouDocumentNumber(details.getVouDocumentNumber());
        		apDebitMemo.setVouDmVoucherNumber(details.getVouDmVoucherNumber());
        		apDebitMemo.setVouLastModifiedBy(details.getVouLastModifiedBy());
        		apDebitMemo.setVouDateLastModified(details.getVouDateLastModified());
        		apDebitMemo.setVouReasonForRejection(null);

        	}

        	LocalApSupplier apSupplier = apSupplierHome.findBySplSupplierCode(SPL_SPPLR_CODE, AD_CMPNY);
        	apSupplier.addApVoucher(apDebitMemo);

        	LocalApTaxCode apTaxCode = apVoucher.getApTaxCode();

        	LocalApWithholdingTaxCode apWithholdingTaxCode = apVoucher.getApWithholdingTaxCode();

        	try {

        		LocalApVoucherBatch apVoucherBatch = apVoucherBatchHome.findByVbName(VB_NM, AD_BRNCH, AD_CMPNY);
        		apVoucherBatch.addApVoucher(apDebitMemo);

        	} catch (FinderException ex) {

        	}

        	LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);

        	if (isRecalculate) {

	        	// remove all demo memo lines

	        	Collection apVoucherLineItems = apDebitMemo.getApVoucherLineItems();

		  	    Iterator i = apVoucherLineItems.iterator();

		  	    while (i.hasNext()) {

		  	   	    LocalApVoucherLineItem apVoucherLineItem = (LocalApVoucherLineItem)i.next();

		  	  	    i.remove();

		  	  	    apVoucherLineItem.remove();

		  	    }

	        	// remove all distribution records

		  	    Collection apDistributionRecords = apDebitMemo.getApDistributionRecords();

		  	    i = apDistributionRecords.iterator();

		  	    while (i.hasNext()) {

		  	   	    LocalApDistributionRecord apDistributionRecord = (LocalApDistributionRecord)i.next();

		  	  	    i.remove();

		  	  	    apDistributionRecord.remove();

		  	    }

		  	    // add new invoice lines and distribution record

		  	    double TOTAL_TAX = 0d;
		  	    double TOTAL_LINE = 0d;

	      	    i = vliList.iterator();

	      	    LocalInvItemLocation invItemLocation = null;
	      	    LocalInvItem item = null;
	      	    LocalInvLocation location = null;
	      	    while (i.hasNext()) {

	      	  	    ApModVoucherLineItemDetails mVliDetails = (ApModVoucherLineItemDetails) i.next();

	      	  	    try {

	      	  	    	item = invItemHome.findByIiName(mVliDetails.getVliIiName(), AD_CMPNY);
	      	  	    	location = invLocationHome.findByPrimaryKey(item.getIiDefaultLocation());
	 	  	    		invItemLocation = invItemLocationHome.findByLocNameAndIiName(location.getLocName(), mVliDetails.getVliIiName(), AD_CMPNY);

	 	  	    	} catch (FinderException ex) {

	 	  	    		throw new GlobalInvItemLocationNotFoundException(String.valueOf(mVliDetails.getVliLine()));

	 	  	    	}


	      	  	    //supply missing entries for the data collector
	      	  	    mVliDetails.setVliUnitCost(item.getIiUnitCost());
	      	  	    mVliDetails.setVliAmount(mVliDetails.getVliQuantity()*item.getIiUnitCost());
	      	  	    mVliDetails.setVliLocName(location.getLocName());
	      	  	    mVliDetails.setVliUomName(item.getInvUnitOfMeasure().getUomName());

	 	  	    	// start date validation
        			System.out.println("Check A");

        			if (adPreference.getPrfArAllowPriorDate() == EJBCommon.FALSE) {
        				Collection invNegTxnCosting = invCostingHome.findNegTxnByGreaterThanCstDateAndIiNameAndLocName(
        	  	    			apDebitMemo.getVouDate(), invItemLocation.getInvItem().getIiName(),
        						invItemLocation.getInvLocation().getLocName(), AD_BRNCH, AD_CMPNY);
        	  	    		if(!invNegTxnCosting.isEmpty()) throw new GlobalInventoryDateException(invItemLocation.getInvItem().getIiName());
        			}


	  	    		System.out.println("Check B");
	      	  	    LocalApVoucherLineItem apVoucherLineItem = this.addApVliEntry(mVliDetails, apDebitMemo, apTaxCode, invItemLocation, AD_CMPNY);

	      	  	    // add cost of sales distribution and inventory

	 	  	    	//double COST =  apVoucherLineItem.getVliUnitCost();

					double QTY_RCVD = this.convertByUomFromAndItemAndQuantity(apVoucherLineItem.getInvUnitOfMeasure(),
    						apVoucherLineItem.getInvItemLocation().getInvItem(), apVoucherLineItem.getVliQuantity(), AD_CMPNY);

					LocalAdBranchItemLocation adBranchItemLocation = null;

					try {

					    adBranchItemLocation = adBranchItemLocationHome.findBilByIlCodeAndBrCode(invItemLocation.getIlCode(), AD_BRNCH, AD_CMPNY);

					} catch (FinderException ex) { }

					if(adBranchItemLocation != null) {

					    this.addApDrEntry(apDebitMemo.getApDrNextLine(),
				            "EXPENSE", EJBCommon.FALSE, apVoucherLineItem.getVliAmount(),
				            adBranchItemLocation.getBilCoaGlInventoryAccount(), apDebitMemo, AD_BRNCH, AD_CMPNY);

					} else {

					    this.addApDrEntry(apDebitMemo.getApDrNextLine(),
		      	  	        "EXPENSE", EJBCommon.FALSE, apVoucherLineItem.getVliAmount(),
		      	  	        invItemLocation.getIlGlCoaInventoryAccount(), apDebitMemo, AD_BRNCH, AD_CMPNY);

					}

	      	  	    //TOTAL_LINE += COST * QTY_RCVD;
	      	  	    TOTAL_LINE += apVoucherLineItem.getVliAmount();
	      	  	    TOTAL_TAX += apVoucherLineItem.getVliTaxAmount();

	      	    }


	      	    // add tax distribution if necessary

	      	    if (!apTaxCode.getTcType().equals("NONE") &&
	        	    !apTaxCode.getTcType().equals("EXEMPT")) {

	  	    	    this.addApDrEntry(apDebitMemo.getApDrNextLine(),
		      	        "TAX", EJBCommon.FALSE, TOTAL_TAX, apTaxCode.getGlChartOfAccount().getCoaCode(),
		      	        apDebitMemo, AD_BRNCH, AD_CMPNY);

		        }

	      	    // add wtax distribution if necessary

	      	   // LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);

	      	    double W_TAX_AMOUNT = 0d;

	      	    if (apWithholdingTaxCode.getWtcRate() != 0 && adPreference.getPrfApWTaxRealization().equals("VOUCHER")) {

	      	    	W_TAX_AMOUNT = EJBCommon.roundIt(TOTAL_LINE * (apWithholdingTaxCode.getWtcRate() / 100), this.getGlFcPrecisionUnit(AD_CMPNY));

	      	    	this.addApDrEntry(apDebitMemo.getApDrNextLine(), "W-TAX",
	      	    	    EJBCommon.TRUE, W_TAX_AMOUNT, apWithholdingTaxCode.getGlChartOfAccount().getCoaCode(),
	      	    	    apDebitMemo, AD_BRNCH, AD_CMPNY);

	      	    }


	      	    // add payable distribution

	      	    LocalAdBranchSupplier adBranchSupplier = null;

	      	    try {

	      	        adBranchSupplier = adBranchSupplierHome.findBSplBySplCodeAndBrCode(apDebitMemo.getApSupplier().getSplCode(), AD_BRNCH, AD_CMPNY);

	      	    } catch (FinderException ex) { }

	      	    if(adBranchSupplier != null) {

	      	        this.addApDrEntry(apDebitMemo.getApDrNextLine(), "PAYABLE",
      	                EJBCommon.TRUE, TOTAL_LINE + TOTAL_TAX - W_TAX_AMOUNT,
      	                adBranchSupplier.getBsplGlCoaPayableAccount(),
      	                apDebitMemo, AD_BRNCH, AD_CMPNY);

	      	    } else {

		      	    this.addApDrEntry(apDebitMemo.getApDrNextLine(), "PAYABLE",
		      	        EJBCommon.TRUE, TOTAL_LINE + TOTAL_TAX - W_TAX_AMOUNT,
		      	        apDebitMemo.getApSupplier().getSplCoaGlPayableAccount(),
		      	        apDebitMemo, AD_BRNCH, AD_CMPNY);

	      	    }

	      	    // set invoice amount due


	      	    apDebitMemo.setVouBillAmount(TOTAL_LINE + TOTAL_TAX - W_TAX_AMOUNT);

	      	    if(adPreference.getPrfApDebitMemoOverrideCost()!=1){
	      	    	if (apDebitMemo.getVouBillAmount() > apVoucher.getVouAmountDue() - apVoucher.getVouAmountPaid()) {

	      	    		throw new ApVOUOverapplicationNotAllowedException();

	      	    	}

	      	    }

	      	    // create new voucher payment schedule lock

	      	    Collection apVoucherPaymentSchedules =
	      	       apVoucher.getApVoucherPaymentSchedules();

      	   		i = apVoucherPaymentSchedules.iterator();

      	   		while (i.hasNext()) {

	      	   	   LocalApVoucherPaymentSchedule apVoucherPaymentSchedule =
	        		   (LocalApVoucherPaymentSchedule)i.next();

        	   	   apVoucherPaymentSchedule.setVpsLock(EJBCommon.TRUE);

      	        }

        	} else {

    			Iterator i = vliList.iterator();

        		LocalInvItemLocation invItemLocation = null;

        		while (i.hasNext()) {

        			ApModVoucherLineItemDetails mVliDetails = (ApModVoucherLineItemDetails) i.next();

        			try {

        				invItemLocation = invItemLocationHome.findByLocNameAndIiName(mVliDetails.getVliLocName(), mVliDetails.getVliIiName(), AD_CMPNY);

        			} catch (FinderException ex) {

        				throw new GlobalInvItemLocationNotFoundException(String.valueOf(mVliDetails.getVliLine()));

        			}

        			//	start date validation
        			System.out.println("Check C");
        			if (adPreference.getPrfArAllowPriorDate() == EJBCommon.FALSE) {
        				Collection invNegTxnCosting = invCostingHome.findNegTxnByGreaterThanCstDateAndIiNameAndLocName(
        	  	    			apDebitMemo.getVouDate(), invItemLocation.getInvItem().getIiName(),
        						invItemLocation.getInvLocation().getLocName(), AD_BRNCH, AD_CMPNY);
        	  	    		if(!invNegTxnCosting.isEmpty()) throw new GlobalInventoryDateException(invItemLocation.getInvItem().getIiName());
        			}
	  	    		System.out.println("Check D");
        		}

    		}

      	   // generate approval status

            String VOU_APPRVL_STATUS = null;

        	if (!isDraft) {

        		LocalAdApproval adApproval = adApprovalHome.findByAprAdCompany(AD_CMPNY);

        		// check if ap voucher approval is enabled

        		if (adApproval.getAprEnableApDebitMemo() == EJBCommon.FALSE) {

        			VOU_APPRVL_STATUS = "N/A";

        		} else {

        			// check if voucher is self approved

        			LocalAdAmountLimit adAmountLimit = null;

        			try {

        				adAmountLimit = adAmountLimitHome.findByAdcTypeAndAuTypeAndUsrName("AP DEBIT MEMO", "REQUESTER", details.getVouLastModifiedBy(), AD_CMPNY);

        			} catch (FinderException ex) {

        				throw new GlobalNoApprovalRequesterFoundException();

        			}

        			if (apDebitMemo.getVouBillAmount() <= adAmountLimit.getCalAmountLimit()) {

        				VOU_APPRVL_STATUS = "N/A";

        			} else {

        				// for approval, create approval queue

        				 Collection adAmountLimits = adAmountLimitHome.findByAdcTypeAndGreaterThanCalAmountLimit("AP DEBIT MEMO", adAmountLimit.getCalAmountLimit(), AD_CMPNY);

        				 if (adAmountLimits.isEmpty()) {

        				 	Collection adApprovalUsers = adApprovalUserHome.findByAuTypeAndCalCode("APPROVER", adAmountLimit.getCalCode(), AD_CMPNY);

        				 	if (adApprovalUsers.isEmpty()) {

        				 		throw new GlobalNoApprovalApproverFoundException();

        				 	}

        				 	Iterator j = adApprovalUsers.iterator();

        				 	while (j.hasNext()) {

        				 		LocalAdApprovalUser adApprovalUser = (LocalAdApprovalUser)j.next();

        				 		LocalAdApprovalQueue adApprovalQueue = adApprovalQueueHome.create(EJBCommon.TRUE, "AP DEBIT MEMO", apDebitMemo.getVouCode(),
        				 				apDebitMemo.getVouDocumentNumber(), apDebitMemo.getVouDate(), adAmountLimit.getCalAndOr(), adApprovalUser.getAuOr(), AD_BRNCH, AD_CMPNY);

        				 		adApprovalUser.getAdUser().addAdApprovalQueue(adApprovalQueue);

        				 	}

        				 } else {

        				 	boolean isApprovalUsersFound = false;

        				 	Iterator i = adAmountLimits.iterator();

        				 	while (i.hasNext()) {

        				 		LocalAdAmountLimit adNextAmountLimit = (LocalAdAmountLimit)i.next();

        				 		if (apDebitMemo.getVouBillAmount() <= adNextAmountLimit.getCalAmountLimit()) {

        				 			Collection adApprovalUsers = adApprovalUserHome.findByAuTypeAndCalCode("APPROVER", adAmountLimit.getCalCode(), AD_CMPNY);

        				 			Iterator j = adApprovalUsers.iterator();

		        				 	while (j.hasNext()) {

		        				 		isApprovalUsersFound = true;

		        				 		LocalAdApprovalUser adApprovalUser = (LocalAdApprovalUser)j.next();

		        				 		LocalAdApprovalQueue adApprovalQueue = adApprovalQueueHome.create(EJBCommon.TRUE, "AP DEBIT MEMO", apDebitMemo.getVouCode(),
		        				 				apDebitMemo.getVouDocumentNumber(), apDebitMemo.getVouDate(), adAmountLimit.getCalAndOr(), adApprovalUser.getAuOr(), AD_BRNCH, AD_CMPNY);

		        				 		adApprovalUser.getAdUser().addAdApprovalQueue(adApprovalQueue);

		        				 	}

	    				 			break;

        				 		} else if (!i.hasNext()) {

        				 			Collection adApprovalUsers = adApprovalUserHome.findByAuTypeAndCalCode("APPROVER", adNextAmountLimit.getCalCode(), AD_CMPNY);

        				 			Iterator j = adApprovalUsers.iterator();

		        				 	while (j.hasNext()) {

		        				 		isApprovalUsersFound = true;

		        				 		LocalAdApprovalUser adApprovalUser = (LocalAdApprovalUser)j.next();

		        				 		LocalAdApprovalQueue adApprovalQueue = adApprovalQueueHome.create(EJBCommon.TRUE, "AP DEBIT MEMO", apDebitMemo.getVouCode(),
		        				 				apDebitMemo.getVouDocumentNumber(), apDebitMemo.getVouDate(), adNextAmountLimit.getCalAndOr(), adApprovalUser.getAuOr(), AD_BRNCH, AD_CMPNY);

		        				 		adApprovalUser.getAdUser().addAdApprovalQueue(adApprovalQueue);

		        				 	}

	    				 			break;

        				 		}

        				 		adAmountLimit = adNextAmountLimit;

        				 	}

        				 	if (!isApprovalUsersFound) {

        				 		throw new GlobalNoApprovalApproverFoundException();

        				 	}

        			    }

        			    VOU_APPRVL_STATUS = "PENDING";
        			}
        		}
        	}

        	//LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);

        	if (VOU_APPRVL_STATUS != null && VOU_APPRVL_STATUS.equals("N/A") && adPreference.getPrfApGlPostingType().equals("AUTO-POST UPON APPROVAL")) {

        		this.executeApDebitMemoPost(apDebitMemo.getVouCode(), apDebitMemo.getVouLastModifiedBy(), AD_BRNCH, AD_CMPNY);

        	}

        	// set debit memo approval status

        	apDebitMemo.setVouApprovalStatus(VOU_APPRVL_STATUS);


      	    return apDebitMemo.getVouCode();


        } catch (GlobalRecordAlreadyDeletedException ex) {

        	getSessionContext().setRollbackOnly();
        	throw ex;

        } catch (GlobalNoRecordFoundException ex) {

        	getSessionContext().setRollbackOnly();
        	throw ex;

        } catch (GlobalDocumentNumberNotUniqueException ex) {

        	getSessionContext().setRollbackOnly();
        	throw ex;

        } catch (GlobalTransactionAlreadyApprovedException ex) {

        	getSessionContext().setRollbackOnly();
        	throw ex;

        } catch (GlobalTransactionAlreadyPendingException ex) {

        	getSessionContext().setRollbackOnly();
        	throw ex;

        } catch (GlobalTransactionAlreadyPostedException ex) {

        	getSessionContext().setRollbackOnly();
        	throw ex;

        } catch (GlobalTransactionAlreadyVoidException ex) {

        	getSessionContext().setRollbackOnly();
        	throw ex;

        } catch (ApVOUOverapplicationNotAllowedException ex) {

        	getSessionContext().setRollbackOnly();
        	throw ex;

        } catch (GlobalTransactionAlreadyLockedException ex) {

        	getSessionContext().setRollbackOnly();
        	throw ex;

        } catch (GlobalNoApprovalRequesterFoundException ex) {

        	getSessionContext().setRollbackOnly();
        	throw ex;

        } catch (GlobalNoApprovalApproverFoundException ex) {

        	getSessionContext().setRollbackOnly();
        	throw ex;

        } catch (GlobalInventoryDateException ex) {

        	getSessionContext().setRollbackOnly();
        	throw ex;

        } catch (GlobalBranchAccountNumberInvalidException ex) {

            getSessionContext().setRollbackOnly();
            throw ex;

        } catch (AdPRFCoaGlVarianceAccountNotFoundException ex){

            getSessionContext().setRollbackOnly();
            throw ex;

        } catch (Exception ex) {

        	Debug.printStackTrace(ex);
        	getSessionContext().setRollbackOnly();
        	throw new EJBException(ex.getMessage());

        }


    }

    /**
     * @ejb:interface-method view-type="remote"
     **/
    public void deleteApVouEntry(Integer VOU_CODE, String AD_USR, Integer AD_BRNCH, Integer AD_CMPNY) throws
        GlobalRecordAlreadyDeletedException {

        Debug.print("ApDebitMemoEntryControllerBean deleteApVouEntry");

        LocalApVoucherHome apVoucherHome = null;
        LocalAdApprovalQueueHome adApprovalQueueHome = null;
        LocalAdDeleteAuditTrailHome adDeleteAuditTrailHome = null;

        // Initialize EJB Home

        try {

            apVoucherHome = (LocalApVoucherHome)EJBHomeFactory.
                lookUpLocalHome(LocalApVoucherHome.JNDI_NAME, LocalApVoucherHome.class);
            adApprovalQueueHome = (LocalAdApprovalQueueHome)EJBHomeFactory.
			    lookUpLocalHome(LocalAdApprovalQueueHome.JNDI_NAME, LocalAdApprovalQueueHome.class);
            adDeleteAuditTrailHome = (LocalAdDeleteAuditTrailHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdDeleteAuditTrailHome.JNDI_NAME, LocalAdDeleteAuditTrailHome.class);

        } catch (NamingException ex) {

            throw new EJBException(ex.getMessage());

        }

        try {

        	LocalApVoucher apDebitMemo = apVoucherHome.findByPrimaryKey(VOU_CODE);

        	if (apDebitMemo.getVouApprovalStatus() != null && apDebitMemo.getVouApprovalStatus().equals("PENDING")) {

        		Collection adApprovalQueues = adApprovalQueueHome.findByAqDocumentAndAqDocumentCode("AP DEBIT MEMO", apDebitMemo.getVouCode(), AD_CMPNY);

        		Iterator i = adApprovalQueues.iterator();

        		while(i.hasNext()) {

        			LocalAdApprovalQueue adApprovalQueue = (LocalAdApprovalQueue)i.next();

        			adApprovalQueue.remove();

        		}

        	}

        	// release lock

        	LocalApVoucher apVoucher = apVoucherHome.findByVouDocumentNumberAndVouDebitMemoAndBrCode(apDebitMemo.getVouDmVoucherNumber(), EJBCommon.FALSE, AD_BRNCH, AD_CMPNY);

      	    Collection apVoucherPaymentSchedules = apVoucher.getApVoucherPaymentSchedules();

  	   		Iterator i = apVoucherPaymentSchedules.iterator();

  	   		while (i.hasNext()) {

      	   	   LocalApVoucherPaymentSchedule apVoucherPaymentSchedule = (LocalApVoucherPaymentSchedule)i.next();

    	   	   apVoucherPaymentSchedule.setVpsLock(EJBCommon.FALSE);

  	        }

  	   		adDeleteAuditTrailHome.create("AP DEBIT MEMO", apDebitMemo.getVouDate(), apDebitMemo.getVouDocumentNumber(), apDebitMemo.getVouReferenceNumber(),
					apDebitMemo.getVouAmountDue(), AD_USR, new Date(), AD_CMPNY);

  	   	    apDebitMemo.remove();

        } catch (FinderException ex) {

            getSessionContext().setRollbackOnly();
            throw new GlobalRecordAlreadyDeletedException();

        } catch (Exception ex) {

        	Debug.printStackTrace(ex);
        	getSessionContext().setRollbackOnly();
        	throw new EJBException(ex.getMessage());

        }

    }

   /**
    * @ejb:interface-method view-type="remote"
    * @jboss:method-attributes read-only="true"
    **/
    public short getGlFcPrecisionUnit(Integer AD_CMPNY) {

       Debug.print("ApDebitMemoEntryControllerBean getGlFcPrecisionUnit");


       LocalAdCompanyHome adCompanyHome = null;


       // Initialize EJB Home

       try {

           adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
              lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);

       } catch (NamingException ex) {

           throw new EJBException(ex.getMessage());

       }

       try {

         LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);

         return  adCompany.getGlFunctionalCurrency().getFcPrecision();

       } catch (Exception ex) {

       	 Debug.printStackTrace(ex);
         throw new EJBException(ex.getMessage());

       }

    }


    /**
    * @ejb:interface-method view-type="remote"
    * @jboss:method-attributes read-only="true"
    **/
    public ArrayList getAdApprovalNotifiedUsersByVouCode(Integer VOU_CODE, Integer AD_CMPNY) {

       Debug.print("ApDebitMemoEntryControllerBean getAdApprovalNotifiedUsersByVouCode");


       LocalAdApprovalQueueHome adApprovalQueueHome = null;
       LocalApVoucherHome apVoucherHome = null;

       ArrayList list = new ArrayList();


       // Initialize EJB Home

       try {

           adApprovalQueueHome = (LocalAdApprovalQueueHome)EJBHomeFactory.
              lookUpLocalHome(LocalAdApprovalQueueHome.JNDI_NAME, LocalAdApprovalQueueHome.class);
           apVoucherHome = (LocalApVoucherHome)EJBHomeFactory.
              lookUpLocalHome(LocalApVoucherHome.JNDI_NAME, LocalApVoucherHome.class);

       } catch (NamingException ex) {

           throw new EJBException(ex.getMessage());

       }

       try {

         LocalApVoucher apVoucher = apVoucherHome.findByPrimaryKey(VOU_CODE);

         if (apVoucher.getVouPosted() == EJBCommon.TRUE) {

         	list.add("DOCUMENT POSTED");
         	return list;

         }

         Collection adApprovalQueues = adApprovalQueueHome.findByAqDocumentAndAqDocumentCode("AP DEBIT MEMO", VOU_CODE, AD_CMPNY);

         Iterator i = adApprovalQueues.iterator();

         while(i.hasNext()) {

         	LocalAdApprovalQueue adApprovalQueue = (LocalAdApprovalQueue)i.next();

         	list.add(adApprovalQueue.getAdUser().getUsrDescription());

         }

         return list;

       } catch (Exception ex) {

       	 Debug.printStackTrace(ex);
         throw new EJBException(ex.getMessage());

       }

    }


    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
     public byte getAdPrfEnableApDebitMemoBatch(Integer AD_CMPNY) {

        Debug.print("ApDebitMemoEntryControllerBean getAdPrfEnableApDebitMemoBatch");

        LocalAdPreferenceHome adPreferenceHome = null;


        // Initialize EJB Home

        try {

           adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
               lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);

        } catch (NamingException ex) {

           throw new EJBException(ex.getMessage());

        }


        try {

           LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);

           return adPreference.getPrfEnableApVoucherBatch();

        } catch (Exception ex) {

           Debug.printStackTrace(ex);
           throw new EJBException(ex.getMessage());

        }

     }


     /**
      * @ejb:interface-method view-type="remote"
      * @jboss:method-attributes read-only="true"
      **/
     public ArrayList getApOpenVbAll(Integer AD_BRNCH, Integer AD_CMPNY) {

         Debug.print("GlJournalEntryControllerBean getApOpenVbAll");

         LocalApVoucherBatchHome apVoucherBatchHome = null;

         ArrayList list = new ArrayList();

         // Initialize EJB Home

         try {

         	apVoucherBatchHome = (LocalApVoucherBatchHome)EJBHomeFactory.
                 lookUpLocalHome(LocalApVoucherBatchHome.JNDI_NAME, LocalApVoucherBatchHome.class);

         } catch (NamingException ex) {

             throw new EJBException(ex.getMessage());

         }

         try {

         	Collection apVoucherBatches = apVoucherBatchHome.findOpenVbByVbType("DEBIT MEMO", AD_BRNCH, AD_CMPNY);

         	Iterator i = apVoucherBatches.iterator();

         	while (i.hasNext()) {

         		LocalApVoucherBatch apVoucherBatch = (LocalApVoucherBatch)i.next();

         		list.add(apVoucherBatch.getVbName());

         	}

         	return list;

         } catch (Exception ex) {

         	Debug.printStackTrace(ex);
         	throw new EJBException(ex.getMessage());

         }

     }

     /**
      * @ejb:interface-method view-type="remote"
      * @jboss:method-attributes read-only="true"
      **/
      public short getInvGpQuantityPrecisionUnit(Integer AD_CMPNY) {

         Debug.print("ApVoucherEntryControllerBean getInvGpQuantityPrecisionUnit");

         LocalAdPreferenceHome adPreferenceHome = null;

          // Initialize EJB Home

          try {

          	adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
                 lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);

          } catch (NamingException ex) {

             throw new EJBException(ex.getMessage());

          }


          try {

             LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);

             return adPreference.getPrfInvQuantityPrecisionUnit();

          } catch (Exception ex) {

             Debug.printStackTrace(ex);
             throw new EJBException(ex.getMessage());

          }

      }

      /**
       * @ejb:interface-method view-type="remote"
       * @jboss:method-attributes read-only="true"
       **/
       public byte getAdPrfApUseSupplierPulldown(Integer AD_CMPNY) {

          Debug.print("ApDebitMemoEntryControllerBean getAdPrfApUseSupplierPulldown");

          LocalAdPreferenceHome adPreferenceHome = null;


          // Initialize EJB Home

          try {

             adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
                 lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);

          } catch (NamingException ex) {

             throw new EJBException(ex.getMessage());

          }


          try {

             LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);

             return adPreference.getPrfApUseSupplierPulldown();

          } catch (Exception ex) {

             Debug.printStackTrace(ex);
             throw new EJBException(ex.getMessage());

          }

       }


       /**
        * @ejb:interface-method view-type="remote"
        * @jboss:method-attributes read-only="true"
        **/
       public String getApSplNameBySplSupplierCode(String SPL_SPPLR_CODE, Integer AD_CMPNY)
       throws GlobalNoRecordFoundException {

       	Debug.print("ApVoucherEntryControllerBean getApSplNameBySplSupplierCode");

       	LocalApSupplierHome apSupplierHome = null;

       	// Initialize EJB Home

       	try {

       		apSupplierHome = (LocalApSupplierHome)EJBHomeFactory.
			lookUpLocalHome(LocalApSupplierHome.JNDI_NAME, LocalApSupplierHome.class);

       	} catch (NamingException ex) {

       		throw new EJBException(ex.getMessage());

       	}

       	try {

       		LocalApSupplier apSupplier = null;


       		try {

       			apSupplier = apSupplierHome.findBySplSupplierCode(SPL_SPPLR_CODE, AD_CMPNY);

       		} catch (FinderException ex) {

       			throw new GlobalNoRecordFoundException();

       		}

       		return apSupplier.getSplName();

       	} catch (GlobalNoRecordFoundException ex) {

       		throw ex;

       	} catch (Exception ex) {

       		Debug.printStackTrace(ex);
       		throw new EJBException(ex.getMessage());

       	}

       }

    // private methods

       private double getInvFifoCost(Date CST_DT, Integer IL_CODE, double CST_QTY, double CST_COST,
      		 boolean isAdjustFifo, Integer AD_BRNCH, Integer AD_CMPNY)
   	 {

   		 LocalInvCostingHome invCostingHome = null;
   	  	 LocalInvItemLocationHome invItemLocationHome = null;

   	     // Initialize EJB Home

   	     try {

   	    	 invCostingHome = (LocalInvCostingHome)EJBHomeFactory.
   	    	 	lookUpLocalHome(LocalInvCostingHome.JNDI_NAME, LocalInvCostingHome.class);
   	    	 invItemLocationHome = (LocalInvItemLocationHome)EJBHomeFactory.
   	    	 	lookUpLocalHome(LocalInvItemLocationHome.JNDI_NAME, LocalInvItemLocationHome.class);
   	     }
   	     catch (NamingException ex) {

   	    	 throw new EJBException(ex.getMessage());
   	     }

   		try {

   			Collection invFifoCostings = invCostingHome.findFifoRemainingQuantityByLessThanOrEqualCstDateAndIlCodeAndBrCode(CST_DT, IL_CODE, AD_BRNCH, AD_CMPNY);

   			if (invFifoCostings.size() > 0) {

   				Iterator x = invFifoCostings.iterator();

   	  			if (isAdjustFifo) {

   	  				//executed during POST transaction

   	  				double totalCost = 0d;
   	  				double cost;

   	  				if(CST_QTY < 0) {

   	  					//for negative quantities
   	 	  				double neededQty = -(CST_QTY);

   	 	  				while(x.hasNext() && neededQty != 0) {

   	 	 	  				LocalInvCosting invFifoCosting = (LocalInvCosting)x.next();

   	 		 	  			if (invFifoCosting.getApPurchaseOrderLine() != null || invFifoCosting.getApVoucherLineItem() != null) {
   	 		 	  				cost = invFifoCosting.getCstItemCost() / invFifoCosting.getCstQuantityReceived();
   	 		 	  			} else if(invFifoCosting.getArInvoiceLineItem() != null) {
   	 		 	  				cost = invFifoCosting.getCstCostOfSales() / invFifoCosting.getCstQuantitySold();
   	 		 	  			} else if (invFifoCosting.getInvBuildUnbuildAssemblyLine() != null) {
   	 		 	  				cost = invFifoCosting.getCstAssemblyCost() / invFifoCosting.getCstAssemblyQuantity();
   	 		 	  			} else {
   	 		 	  				cost = invFifoCosting.getCstAdjustCost() /invFifoCosting.getCstAdjustQuantity();
   	 		 	  			}

   	 	  					if(neededQty <= invFifoCosting.getCstRemainingLifoQuantity()) {

   	 			  				invFifoCosting.setCstRemainingLifoQuantity(invFifoCosting.getCstRemainingLifoQuantity() - neededQty);
   	 			  				totalCost += (neededQty * cost);
   	 			  				neededQty = 0d;
   	 	  					} else {

   	 	  						neededQty -= invFifoCosting.getCstRemainingLifoQuantity();
   	 	  						totalCost += (invFifoCosting.getCstRemainingLifoQuantity() * cost);
   	 	  						invFifoCosting.setCstRemainingLifoQuantity(0);
   	 	  					}
   	 	  				}

   	 	  				//if needed qty is not yet satisfied but no more quantities to fetch, get the default cost
   	 	  				if(neededQty != 0) {

   	 	  					LocalInvItemLocation invItemLocation = invItemLocationHome.findByPrimaryKey(IL_CODE);
   	 	  					totalCost += (neededQty * invItemLocation.getInvItem().getIiUnitCost());
   	 	  				}

   	 	  				cost = totalCost / -CST_QTY;
   	  				}

   	  				else {

   	  					//for positive quantities
   	  					cost = CST_COST;
   	  				}
   	  				return cost;
   	  			}

   	  			else {

   	  				//executed during ENTRY transaction

   	  				LocalInvCosting invFifoCosting = (LocalInvCosting)x.next();

   	 	  			if (invFifoCosting.getApPurchaseOrderLine() != null || invFifoCosting.getApVoucherLineItem() != null) {
   	 	  				return EJBCommon.roundIt(invFifoCosting.getCstItemCost() / invFifoCosting.getCstQuantityReceived(), this.getGlFcPrecisionUnit(AD_CMPNY));
  	 	  			} else if(invFifoCosting.getArInvoiceLineItem() != null) {
  		 	  			return  EJBCommon.roundIt(invFifoCosting.getCstCostOfSales() / invFifoCosting.getCstQuantitySold(), this.getGlFcPrecisionUnit(AD_CMPNY));
   	 	  			} else if (invFifoCosting.getInvBuildUnbuildAssemblyLine() != null) {
   	 	  				return EJBCommon.roundIt(invFifoCosting.getCstAssemblyCost() / invFifoCosting.getCstAssemblyQuantity(), this.getGlFcPrecisionUnit(AD_CMPNY));
   	 	  			} else {
   	 	  				return EJBCommon.roundIt(invFifoCosting.getCstAdjustCost() /invFifoCosting.getCstAdjustQuantity(), this.getGlFcPrecisionUnit(AD_CMPNY));
   	 	  			}
   	  			}
   			}
   			else {

   				//most applicable in 1st entries of data
   				LocalInvItemLocation invItemLocation = invItemLocationHome.findByPrimaryKey(IL_CODE);
   				return invItemLocation.getInvItem().getIiUnitCost();
   			}

   		}
   		catch (Exception ex) {
   			Debug.printStackTrace(ex);
   		    throw new EJBException(ex.getMessage());
   		}
   	}


    private void addApDrEntry(ApModDistributionRecordDetails mdetails, LocalApVoucher apDebitMemo, Integer AD_BRNCH, Integer AD_CMPNY)
        throws GlobalBranchAccountNumberInvalidException {

		Debug.print("ApDebitMemoEntryControllerBean addApDrEntry");

		LocalApDistributionRecordHome apDistributionRecordHome = null;
		LocalGlChartOfAccountHome glChartOfAccountHome = null;
		LocalAdCompanyHome adCompanyHome = null;


        // Initialize EJB Home

        try {

            apDistributionRecordHome = (LocalApDistributionRecordHome)EJBHomeFactory.
                lookUpLocalHome(LocalApDistributionRecordHome.JNDI_NAME, LocalApDistributionRecordHome.class);
            glChartOfAccountHome = (LocalGlChartOfAccountHome)EJBHomeFactory.
                lookUpLocalHome(LocalGlChartOfAccountHome.JNDI_NAME, LocalGlChartOfAccountHome.class);
            adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);


        } catch (NamingException ex) {

            throw new EJBException(ex.getMessage());

        }

        try {

        	// get company

        	LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);

        	// validate if coa exists

        	LocalGlChartOfAccount glChartOfAccount = null;

        	try {

        		glChartOfAccount = glChartOfAccountHome.findByCoaAccountNumberAndBranchCode(mdetails.getDrCoaAccountNumber(), AD_BRNCH, AD_CMPNY);

        		if (glChartOfAccount.getCoaEnable() == EJBCommon.FALSE)
        		    throw new GlobalBranchAccountNumberInvalidException();

        	} catch (FinderException ex) {

        		throw new GlobalBranchAccountNumberInvalidException();

        	}

		    // create distribution record

		    LocalApDistributionRecord apDistributionRecord = apDistributionRecordHome.create(
			    mdetails.getDrLine(),
			    mdetails.getDrClass(),
			    EJBCommon.roundIt(mdetails.getDrAmount(), adCompany.getGlFunctionalCurrency().getFcPrecision()),
			    mdetails.getDrDebit(), EJBCommon.FALSE, EJBCommon.FALSE, AD_CMPNY);

			apDebitMemo.addApDistributionRecord(apDistributionRecord);
			glChartOfAccount.addApDistributionRecord(apDistributionRecord);

        } catch(GlobalBranchAccountNumberInvalidException ex) {

            throw ex;

        } catch (Exception ex) {

        	Debug.printStackTrace(ex);
        	getSessionContext().setRollbackOnly();
        	throw new EJBException(ex.getMessage());

        }

	}

	private void executeApDebitMemoPost(Integer VOU_CODE, String USR_NM, Integer AD_BRNCH, Integer AD_CMPNY) throws
        GlobalRecordAlreadyDeletedException,
		GlobalTransactionAlreadyPostedException,
		GlobalTransactionAlreadyVoidException,
		GlJREffectiveDateNoPeriodExistException,
		GlJREffectiveDatePeriodClosedException,
		GlobalJournalNotBalanceException,
		AdPRFCoaGlVarianceAccountNotFoundException {

        Debug.print("ApDebitMemoEntryControllerBean executeApDebitMemoPost");

        LocalApVoucherHome apVoucherHome = null;
        LocalAdCompanyHome adCompanyHome = null;
        LocalAdPreferenceHome adPreferenceHome = null;
        LocalGlSetOfBookHome glSetOfBookHome = null;
        LocalGlAccountingCalendarValueHome glAccountingCalendarValueHome = null;
        LocalGlJournalHome glJournalHome = null;
        LocalGlJournalBatchHome glJournalBatchHome = null;
        LocalGlSuspenseAccountHome glSuspenseAccountHome = null;
        LocalGlJournalLineHome glJournalLineHome = null;
        LocalGlJournalSourceHome glJournalSourceHome = null;
        LocalGlJournalCategoryHome glJournalCategoryHome = null;
        LocalGlFunctionalCurrencyHome glFunctionalCurrencyHome = null;
        LocalApDistributionRecordHome apDistributionRecordHome = null;
        LocalInvCostingHome invCostingHome = null;
        LocalGlChartOfAccountHome glChartOfAccountHome = null;

        LocalApVoucher apVoucher = null;
        LocalApVoucher apDebitedVoucher = null;

        // Initialize EJB Home

        try {

            apVoucherHome = (LocalApVoucherHome)EJBHomeFactory.
                lookUpLocalHome(LocalApVoucherHome.JNDI_NAME, LocalApVoucherHome.class);
            adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
            adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
            	lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);
            glSetOfBookHome = (LocalGlSetOfBookHome)EJBHomeFactory.
            	lookUpLocalHome(LocalGlSetOfBookHome.JNDI_NAME, LocalGlSetOfBookHome.class);
            glAccountingCalendarValueHome = (LocalGlAccountingCalendarValueHome)EJBHomeFactory.
            	lookUpLocalHome(LocalGlAccountingCalendarValueHome.JNDI_NAME, LocalGlAccountingCalendarValueHome.class);
            glJournalHome = (LocalGlJournalHome)EJBHomeFactory.
            	lookUpLocalHome(LocalGlJournalHome.JNDI_NAME, LocalGlJournalHome.class);
            glJournalBatchHome = (LocalGlJournalBatchHome)EJBHomeFactory.
            	lookUpLocalHome(LocalGlJournalBatchHome.JNDI_NAME, LocalGlJournalBatchHome.class);
            glSuspenseAccountHome = (LocalGlSuspenseAccountHome)EJBHomeFactory.
            	lookUpLocalHome(LocalGlSuspenseAccountHome.JNDI_NAME, LocalGlSuspenseAccountHome.class);
            glJournalLineHome = (LocalGlJournalLineHome)EJBHomeFactory.
            	lookUpLocalHome(LocalGlJournalLineHome.JNDI_NAME, LocalGlJournalLineHome.class);
            glJournalSourceHome = (LocalGlJournalSourceHome)EJBHomeFactory.
            	lookUpLocalHome(LocalGlJournalSourceHome.JNDI_NAME, LocalGlJournalSourceHome.class);
            glJournalCategoryHome = (LocalGlJournalCategoryHome)EJBHomeFactory.
            	lookUpLocalHome(LocalGlJournalCategoryHome.JNDI_NAME, LocalGlJournalCategoryHome.class);
            glFunctionalCurrencyHome = (LocalGlFunctionalCurrencyHome)EJBHomeFactory.
            	lookUpLocalHome(LocalGlFunctionalCurrencyHome.JNDI_NAME, LocalGlFunctionalCurrencyHome.class);
            apDistributionRecordHome = (LocalApDistributionRecordHome)EJBHomeFactory.
            	lookUpLocalHome(LocalApDistributionRecordHome.JNDI_NAME, LocalApDistributionRecordHome.class);
            invCostingHome = (LocalInvCostingHome)EJBHomeFactory.
            	lookUpLocalHome(LocalInvCostingHome.JNDI_NAME, LocalInvCostingHome.class);
            glChartOfAccountHome = (LocalGlChartOfAccountHome)EJBHomeFactory.
            	lookUpLocalHome(LocalGlChartOfAccountHome.JNDI_NAME, LocalGlChartOfAccountHome.class);

        } catch (NamingException ex) {

            throw new EJBException(ex.getMessage());

        }

        try {

        	// validate if voucher/debit memo is already deleted

        	try {

        		apVoucher = apVoucherHome.findByPrimaryKey(VOU_CODE);

        	} catch (FinderException ex) {

        		throw new GlobalRecordAlreadyDeletedException();

        	}

        	// validate if voucher/debit memo is already posted or void

        	if (apVoucher.getVouPosted() == EJBCommon.TRUE) {

        		throw new GlobalTransactionAlreadyPostedException();

        	} else if (apVoucher.getVouVoid() == EJBCommon.TRUE) {

        		throw new GlobalTransactionAlreadyVoidException();
        	}


        	if (apVoucher.getVouVoid() == EJBCommon.FALSE && apVoucher.getVouPosted() == EJBCommon.FALSE) {

        		// get debited voucher

        		apDebitedVoucher = apVoucherHome.findByVouDocumentNumberAndVouDebitMemoAndBrCode(
        				apVoucher.getVouDmVoucherNumber(), EJBCommon.FALSE, AD_BRNCH, AD_CMPNY);

        		// decrease supplier balance

        		double VOU_AMNT = this.convertForeignToFunctionalCurrency(apDebitedVoucher.getGlFunctionalCurrency().getFcCode(),
        				apDebitedVoucher.getGlFunctionalCurrency().getFcName(),
    					apDebitedVoucher.getVouConversionDate(), apDebitedVoucher.getVouConversionRate(),
    					apVoucher.getVouBillAmount(), AD_CMPNY) * -1;

        		this.post(apVoucher.getVouDate(), VOU_AMNT, apVoucher.getApSupplier(), AD_CMPNY);

        		// decrease voucher and vps amounts and release lock

        		double DEBIT_PERCENT = EJBCommon.roundIt(apVoucher.getVouBillAmount() / apDebitedVoucher.getVouAmountDue(), (short)6);

        		apDebitedVoucher.setVouAmountPaid(apDebitedVoucher.getVouAmountPaid() + apVoucher.getVouBillAmount());

        		double TOTAL_VOUCHER_PAYMENT_SCHEDULE =  0d;

        		Collection apVoucherPaymentSchedules = apDebitedVoucher.getApVoucherPaymentSchedules();

        		Iterator i = apVoucherPaymentSchedules.iterator();

        		while (i.hasNext()) {

        			LocalApVoucherPaymentSchedule apVoucherPaymentSchedule =
        				(LocalApVoucherPaymentSchedule)i.next();

        			double VOUCHER_PAYMENT_SCHEDULE_AMOUNT = 0;

        			// if last payment schedule subtract to avoid rounding difference error

        			if (i.hasNext()) {

        				VOUCHER_PAYMENT_SCHEDULE_AMOUNT = EJBCommon.roundIt(apVoucherPaymentSchedule.getVpsAmountDue() * DEBIT_PERCENT, this.getGlFcPrecisionUnit(AD_CMPNY));

        			} else {

        				VOUCHER_PAYMENT_SCHEDULE_AMOUNT = apVoucher.getVouBillAmount() - TOTAL_VOUCHER_PAYMENT_SCHEDULE;

        			}

        			apVoucherPaymentSchedule.setVpsAmountPaid(apVoucherPaymentSchedule.getVpsAmountPaid() + VOUCHER_PAYMENT_SCHEDULE_AMOUNT);

        			apVoucherPaymentSchedule.setVpsLock(EJBCommon.FALSE);

        			TOTAL_VOUCHER_PAYMENT_SCHEDULE += VOUCHER_PAYMENT_SCHEDULE_AMOUNT;

        		}

        		Collection apVoucherLineItems = apVoucher.getApVoucherLineItems();

        		if (apVoucherLineItems != null && !apVoucherLineItems.isEmpty()) {

        			Iterator c = apVoucherLineItems.iterator();

        			while(c.hasNext()) {

        				LocalApVoucherLineItem apVoucherLineItem = (LocalApVoucherLineItem) c.next();

        				String II_NM = apVoucherLineItem.getInvItemLocation().getInvItem().getIiName();
        				String LOC_NM = apVoucherLineItem.getInvItemLocation().getInvLocation().getLocName();

        				double QTY_RCVD = this.convertByUomFromAndItemAndQuantity(apVoucherLineItem.getInvUnitOfMeasure(),
        						apVoucherLineItem.getInvItemLocation().getInvItem(), apVoucherLineItem.getVliQuantity(), AD_CMPNY);

        				LocalInvCosting invCosting = null;
        				// CREATE COSTING
        				try {

        					invCosting  = invCostingHome.getByMaxCstDateToLongAndMaxCstLineNumberAndLessThanEqualCstDateAndIiNameAndLocName(apVoucher.getVouDate(), II_NM, LOC_NM, AD_BRNCH, AD_CMPNY);

        				} catch (FinderException ex) {

        				}

        				double COST = apVoucherLineItem.getVliUnitCost();

        				if (invCosting == null) {

        					this.postToInv(apVoucherLineItem, apVoucher.getVouDate(),
        							-QTY_RCVD, -COST * QTY_RCVD,
        							-QTY_RCVD, -COST * QTY_RCVD,
        							0d, null, AD_BRNCH, AD_CMPNY);

        				} else {

        					this.postToInv(apVoucherLineItem, apVoucher.getVouDate(),
        							-QTY_RCVD, -COST * QTY_RCVD,
    								invCosting.getCstRemainingQuantity() - QTY_RCVD, invCosting.getCstRemainingValue() - (COST * QTY_RCVD),
    								0d, null, AD_BRNCH, AD_CMPNY);

        				}

        			}

        		}

        	} else if (apVoucher.getVouVoid() == EJBCommon.TRUE && apVoucher.getVouPosted()== EJBCommon.FALSE) {

        	}




        	// set voucher post status

        	apVoucher.setVouPosted(EJBCommon.TRUE);
        	apVoucher.setVouPostedBy(USR_NM);
        	apVoucher.setVouDatePosted(EJBCommon.getGcCurrentDateWoTime().getTime());


        	// post to gl if necessary

        	LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);
        	LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);

        	if (adPreference.getPrfApGlPostingType().equals("AUTO-POST UPON APPROVAL")) {

        		// validate if date has no period and period is closed

        		LocalGlSetOfBook glJournalSetOfBook = null;

        		try {

        			glJournalSetOfBook = glSetOfBookHome.findByDate(apVoucher.getVouDate(), AD_CMPNY);

        		} catch (FinderException ex) {

        			throw new GlJREffectiveDateNoPeriodExistException();

        		}

        		LocalGlAccountingCalendarValue glAccountingCalendarValue =
        			glAccountingCalendarValueHome.findByAcCodeAndDate(
        					glJournalSetOfBook.getGlAccountingCalendar().getAcCode(), apVoucher.getVouDate(), AD_CMPNY);


        		if (glAccountingCalendarValue.getAcvStatus() == 'N' ||
        				glAccountingCalendarValue.getAcvStatus() == 'C' ||
						glAccountingCalendarValue.getAcvStatus() == 'P') {

        			throw new GlJREffectiveDatePeriodClosedException();

        		}

        		// check if voucher is balance if not check suspense posting

        		LocalGlJournalLine glOffsetJournalLine = null;

        		Collection apDistributionRecords = apDistributionRecordHome.findImportableDrByVouCode(apVoucher.getVouCode(), AD_CMPNY);

        		Iterator j = apDistributionRecords.iterator();

        		double TOTAL_DEBIT = 0d;
        		double TOTAL_CREDIT = 0d;

        		while (j.hasNext()) {

        			LocalApDistributionRecord apDistributionRecord = (LocalApDistributionRecord)j.next();

        			double DR_AMNT = 0d;

        			if (apVoucher.getVouDebitMemo() == EJBCommon.FALSE) {

        				DR_AMNT = this.convertForeignToFunctionalCurrency(apVoucher.getGlFunctionalCurrency().getFcCode(),
        						apVoucher.getGlFunctionalCurrency().getFcName(),
								apVoucher.getVouConversionDate(),
								apVoucher.getVouConversionRate(),
								apDistributionRecord.getDrAmount(), AD_CMPNY);

        			} else {

        				DR_AMNT = this.convertForeignToFunctionalCurrency(apDebitedVoucher.getGlFunctionalCurrency().getFcCode(),
        						apDebitedVoucher.getGlFunctionalCurrency().getFcName(),
								apDebitedVoucher.getVouConversionDate(),
								apDebitedVoucher.getVouConversionRate(),
								apDistributionRecord.getDrAmount(), AD_CMPNY);

        			}

        			if (apDistributionRecord.getDrDebit() == EJBCommon.TRUE) {

        				TOTAL_DEBIT += DR_AMNT;

        			} else {

        				TOTAL_CREDIT += DR_AMNT;

        			}

        		}

        		TOTAL_DEBIT = EJBCommon.roundIt(TOTAL_DEBIT, adCompany.getGlFunctionalCurrency().getFcPrecision());
        		TOTAL_CREDIT = EJBCommon.roundIt(TOTAL_CREDIT, adCompany.getGlFunctionalCurrency().getFcPrecision());

        		if (adPreference.getPrfAllowSuspensePosting() == EJBCommon.TRUE &&
        				TOTAL_DEBIT != TOTAL_CREDIT) {

        			LocalGlSuspenseAccount glSuspenseAccount = null;

        			try {

        				glSuspenseAccount = glSuspenseAccountHome.findByJsNameAndJcName("ACCOUNTS PAYABLES", apVoucher.getVouDebitMemo() == EJBCommon.FALSE ? "VOUCHERS" : "DEBIT MEMOS", AD_CMPNY);

        			} catch (FinderException ex) {

        				throw new GlobalJournalNotBalanceException();

        			}

        			if (TOTAL_DEBIT - TOTAL_CREDIT < 0) {

        				glOffsetJournalLine = glJournalLineHome.create((short)(apDistributionRecords.size() + 1),
								EJBCommon.TRUE, TOTAL_CREDIT - TOTAL_DEBIT, "", AD_CMPNY);

        			} else {

        				glOffsetJournalLine = glJournalLineHome.create((short)(apDistributionRecords.size() + 1),
								EJBCommon.FALSE, TOTAL_DEBIT - TOTAL_CREDIT, "", AD_CMPNY);

        			}

        			LocalGlChartOfAccount glChartOfAccount = glSuspenseAccount.getGlChartOfAccount();
        			//glChartOfAccount.addGlJournalLine(glOffsetJournalLine);
        			glOffsetJournalLine.setGlChartOfAccount(glChartOfAccount);


        		} else if (adPreference.getPrfAllowSuspensePosting() == EJBCommon.FALSE &&
        				TOTAL_DEBIT != TOTAL_CREDIT) {

        			throw new GlobalJournalNotBalanceException();

        		}

        		// create journal batch if necessary

        		LocalGlJournalBatch glJournalBatch = null;
        		java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat("MM/dd/yyyy");

        		try {

        			if (adPreference.getPrfEnableApVoucherBatch() == EJBCommon.TRUE) {

        				glJournalBatch = glJournalBatchHome.findByJbName("JOURNAL IMPORT " + formatter.format(new Date()) + " " + apVoucher.getApVoucherBatch().getVbName(), AD_BRNCH, AD_CMPNY);

        			} else {

        				glJournalBatch = glJournalBatchHome.findByJbName("JOURNAL IMPORT " + formatter.format(new Date()) + " VOUCHERS", AD_BRNCH, AD_CMPNY);

        			}


        		} catch (FinderException ex) {
        		}

        		if (adPreference.getPrfEnableGlJournalBatch() ==  EJBCommon.TRUE &&
        				glJournalBatch == null) {

        			if (adPreference.getPrfEnableApVoucherBatch() == EJBCommon.TRUE) {

        				glJournalBatch = glJournalBatchHome.create("JOURNAL IMPORT " + formatter.format(new Date()) + " " + apVoucher.getApVoucherBatch().getVbName(), "JOURNAL IMPORT", "CLOSED", EJBCommon.getGcCurrentDateWoTime().getTime(), USR_NM, AD_BRNCH, AD_CMPNY);

        			} else {

        				glJournalBatch = glJournalBatchHome.create("JOURNAL IMPORT " + formatter.format(new Date()) + " VOUCHERS", "JOURNAL IMPORT", "CLOSED", EJBCommon.getGcCurrentDateWoTime().getTime(), USR_NM, AD_BRNCH, AD_CMPNY);

        			}


        		}

        		// create journal entry

        		LocalGlJournal glJournal = glJournalHome.create(apVoucher.getVouDmVoucherNumber(),
        				apVoucher.getVouDescription(), apVoucher.getVouDate(),
						0.0d, null, apVoucher.getVouDocumentNumber(), null, 1d, "N/A", null,
						'N', EJBCommon.TRUE, EJBCommon.FALSE,
						USR_NM, new Date(),
						USR_NM, new Date(),
						null, null,
						USR_NM, EJBCommon.getGcCurrentDateWoTime().getTime(),
						apVoucher.getApSupplier().getSplTin(),
						apVoucher.getApSupplier().getSplName(), EJBCommon.FALSE,
						null,
						AD_BRNCH, AD_CMPNY);

        		LocalGlJournalSource glJournalSource = glJournalSourceHome.findByJsName("ACCOUNTS PAYABLES", AD_CMPNY);
        		glJournal.setGlJournalSource(glJournalSource);

        		LocalGlFunctionalCurrency glFunctionalCurrency = glFunctionalCurrencyHome.findByFcName(adCompany.getGlFunctionalCurrency().getFcName(), AD_CMPNY);
        		glJournal.setGlFunctionalCurrency(glFunctionalCurrency);

        		LocalGlJournalCategory glJournalCategory = glJournalCategoryHome.findByJcName(apVoucher.getVouDebitMemo() == EJBCommon.FALSE ? "VOUCHERS" : "DEBIT MEMOS", AD_CMPNY);
        		glJournal.setGlJournalCategory(glJournalCategory);

        		if (glJournalBatch != null) {

        			glJournal.setGlJournalBatch(glJournalBatch);

        		}

        		// create journal lines

        		j = apDistributionRecords.iterator();

        		while (j.hasNext()) {

        			LocalApDistributionRecord apDistributionRecord = (LocalApDistributionRecord)j.next();

        			double DR_AMNT = 0d;

        			if (apVoucher.getVouDebitMemo() == EJBCommon.FALSE) {

        				DR_AMNT = this.convertForeignToFunctionalCurrency(apVoucher.getGlFunctionalCurrency().getFcCode(),
        						apVoucher.getGlFunctionalCurrency().getFcName(),
								apVoucher.getVouConversionDate(),
								apVoucher.getVouConversionRate(),
								apDistributionRecord.getDrAmount(), AD_CMPNY);

        			} else {

        				DR_AMNT = this.convertForeignToFunctionalCurrency(apDebitedVoucher.getGlFunctionalCurrency().getFcCode(),
        						apDebitedVoucher.getGlFunctionalCurrency().getFcName(),
								apDebitedVoucher.getVouConversionDate(),
								apDebitedVoucher.getVouConversionRate(),
								apDistributionRecord.getDrAmount(), AD_CMPNY);

        			}

        			LocalGlJournalLine glJournalLine = glJournalLineHome.create(apDistributionRecord.getDrLine(),
							apDistributionRecord.getDrDebit(), DR_AMNT, "", AD_CMPNY);

        			//apDistributionRecord.getGlChartOfAccount().addGlJournalLine(glJournalLine);
        			glJournalLine.setGlChartOfAccount(apDistributionRecord.getGlChartOfAccount());

        			//glJournal.addGlJournalLine(glJournalLine);
        			glJournalLine.setGlJournal(glJournal);

        			apDistributionRecord.setDrImported(EJBCommon.TRUE);


        		}

        		if (glOffsetJournalLine != null) {

        			//glJournal.addGlJournalLine(glOffsetJournalLine);
        			glOffsetJournalLine.setGlJournal(glJournal);

        		}

        		// post journal to gl

        		Collection glJournalLines = glJournal.getGlJournalLines();

        		 Iterator i = glJournalLines.iterator();

        		while (i.hasNext()) {

        			LocalGlJournalLine glJournalLine = (LocalGlJournalLine)i.next();

        			// post current to current acv

        			this.postToGl(glAccountingCalendarValue,
        					glJournalLine.getGlChartOfAccount(),
							true, glJournalLine.getJlDebit(), glJournalLine.getJlAmount(), AD_CMPNY);


        			// post to subsequent acvs (propagate)

        			Collection glSubsequentAccountingCalendarValues =
        				glAccountingCalendarValueHome.findSubsequentAcvByAcCodeAndAcvPeriodNumber(
        						glJournalSetOfBook.getGlAccountingCalendar().getAcCode(),
								glAccountingCalendarValue.getAcvPeriodNumber(), AD_CMPNY);

        			Iterator acvsIter = glSubsequentAccountingCalendarValues.iterator();

        			while (acvsIter.hasNext()) {

        				LocalGlAccountingCalendarValue glSubsequentAccountingCalendarValue =
        					(LocalGlAccountingCalendarValue)acvsIter.next();

        				this.postToGl(glSubsequentAccountingCalendarValue,
        						glJournalLine.getGlChartOfAccount(),
								false, glJournalLine.getJlDebit(), glJournalLine.getJlAmount(), AD_CMPNY);

        			}

        			// post to subsequent years if necessary

        			Collection glSubsequentSetOfBooks = glSetOfBookHome.findSubsequentSobByAcYear(glJournalSetOfBook.getGlAccountingCalendar().getAcYear(), AD_CMPNY);

        			if (!glSubsequentSetOfBooks.isEmpty() && glJournalSetOfBook.getSobYearEndClosed() == 1) {

        				adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
        				LocalGlChartOfAccount glRetainedEarningsAccount = glChartOfAccountHome.findByCoaAccountNumber(adCompany.getCmpRetainedEarnings(), AD_CMPNY);

        				Iterator sobIter = glSubsequentSetOfBooks.iterator();

        				while (sobIter.hasNext()) {

        					LocalGlSetOfBook glSubsequentSetOfBook = (LocalGlSetOfBook)sobIter.next();

        					String ACCOUNT_TYPE = glJournalLine.getGlChartOfAccount().getCoaAccountType();

        					// post to subsequent acvs of subsequent set of book(propagate)

        					Collection glAccountingCalendarValues =
        						glAccountingCalendarValueHome.findByAcCode(glSubsequentSetOfBook.getGlAccountingCalendar().getAcCode(), AD_CMPNY);

        					Iterator acvIter = glAccountingCalendarValues.iterator();

        					while (acvIter.hasNext()) {

        						LocalGlAccountingCalendarValue glSubsequentAccountingCalendarValue =
        							(LocalGlAccountingCalendarValue)acvIter.next();

        						if (ACCOUNT_TYPE.equals("ASSET") || ACCOUNT_TYPE.equals("LIABILITY") ||
        								ACCOUNT_TYPE.equals("OWNERS EQUITY")) {

        							this.postToGl(glSubsequentAccountingCalendarValue,
        									glJournalLine.getGlChartOfAccount(),
											false, glJournalLine.getJlDebit(), glJournalLine.getJlAmount(), AD_CMPNY);

        						} else { // revenue & expense

        							this.postToGl(glSubsequentAccountingCalendarValue,
        									glRetainedEarningsAccount,
											false, glJournalLine.getJlDebit(), glJournalLine.getJlAmount(), AD_CMPNY);

        						}

        					}

        					if (glSubsequentSetOfBook.getSobYearEndClosed() == 0) break;

        				}

        			}

        		}

        	}

        } catch (GlJREffectiveDateNoPeriodExistException ex) {

        	getSessionContext().setRollbackOnly();
        	throw ex;

        } catch (GlJREffectiveDatePeriodClosedException ex) {

        	getSessionContext().setRollbackOnly();
        	throw ex;

        } catch (GlobalJournalNotBalanceException ex) {

        	getSessionContext().setRollbackOnly();
        	throw ex;

        } catch (GlobalRecordAlreadyDeletedException ex) {

        	getSessionContext().setRollbackOnly();
        	throw ex;

        } catch (GlobalTransactionAlreadyPostedException ex) {

        	getSessionContext().setRollbackOnly();
        	throw ex;

        } catch (GlobalTransactionAlreadyVoidException ex) {

        	getSessionContext().setRollbackOnly();
        	throw ex;

        } catch (AdPRFCoaGlVarianceAccountNotFoundException ex) {

        	getSessionContext().setRollbackOnly();
        	throw ex;

    	} catch (Exception ex) {

    		Debug.printStackTrace(ex);
    		getSessionContext().setRollbackOnly();
    		throw new EJBException(ex.getMessage());

    	}

    }

    private void post(Date VOU_DT, double VOU_AMNT, LocalApSupplier apSupplier, Integer AD_CMPNY) {

       Debug.print("ApDebitMemoEntryControllerBean post");

       LocalApSupplierBalanceHome apSupplierBalanceHome = null;

       // Initialize EJB Home

       try {

           apSupplierBalanceHome = (LocalApSupplierBalanceHome)EJBHomeFactory.
               lookUpLocalHome(LocalApSupplierBalanceHome.JNDI_NAME, LocalApSupplierBalanceHome.class);

       } catch (NamingException ex) {

           getSessionContext().setRollbackOnly();
           throw new EJBException(ex.getMessage());

       }

       try {

	       // find supplier balance before or equal voucher date

	       Collection apSupplierBalances = apSupplierBalanceHome.findByBeforeOrEqualVouDateAndSplSupplierCode(VOU_DT, apSupplier.getSplSupplierCode(), AD_CMPNY);

	       if (!apSupplierBalances.isEmpty()) {

	    	   // get last voucher

	    	   ArrayList apSupplierBalanceList = new ArrayList(apSupplierBalances);

	    	   LocalApSupplierBalance apSupplierBalance = (LocalApSupplierBalance)apSupplierBalanceList.get(apSupplierBalanceList.size() - 1);

	    	   if (apSupplierBalance.getSbDate().before(VOU_DT)) {

	    	       // create new balance

	    	       LocalApSupplierBalance apNewSupplierBalance = apSupplierBalanceHome.create(
		    	       VOU_DT, apSupplierBalance.getSbBalance() + VOU_AMNT, AD_CMPNY);

		           //apSupplier.addApSupplierBalance(apNewSupplierBalance);
		           apNewSupplierBalance.setApSupplier(apSupplier);

	    	   } else { // equals to voucher date

	    	       apSupplierBalance.setSbBalance(apSupplierBalance.getSbBalance() + VOU_AMNT);

	    	   }

	    	} else {

	    	    // create new balance

		    	LocalApSupplierBalance apNewSupplierBalance = apSupplierBalanceHome.create(
		    		VOU_DT, VOU_AMNT, AD_CMPNY);

		        //apSupplier.addApSupplierBalance(apNewSupplierBalance);
		        apNewSupplierBalance.setApSupplier(apSupplier);

	     	}

	     	// propagate to subsequent balances if necessary

	     	apSupplierBalances = apSupplierBalanceHome.findByAfterVouDateAndSplSupplierCode(VOU_DT, apSupplier.getSplSupplierCode(), AD_CMPNY);

	     	Iterator i = apSupplierBalances.iterator();

	     	while (i.hasNext()) {

	     		LocalApSupplierBalance apSupplierBalance = (LocalApSupplierBalance)i.next();

	     		apSupplierBalance.setSbBalance(apSupplierBalance.getSbBalance() + VOU_AMNT);

	     	}

        } catch (Exception ex) {

        	Debug.printStackTrace(ex);
        	getSessionContext().setRollbackOnly();
            throw new EJBException(ex.getMessage());

        }

	}

    private double convertForeignToFunctionalCurrency(Integer FC_CODE, String FC_NM,
	    Date CONVERSION_DATE, double CONVERSION_RATE, double AMOUNT, Integer AD_CMPNY) {

	    Debug.print("ApDebitMemoEntryControllerBean convertForeignToFunctionalCurrency");


        LocalGlFunctionalCurrencyRateHome glFunctionalCurrencyRateHome = null;
        LocalAdCompanyHome adCompanyHome = null;

        LocalAdCompany adCompany = null;

        // Initialize EJB Homes

        try {

            glFunctionalCurrencyRateHome = (LocalGlFunctionalCurrencyRateHome)EJBHomeFactory.
               lookUpLocalHome(LocalGlFunctionalCurrencyRateHome.JNDI_NAME, LocalGlFunctionalCurrencyRateHome.class);
            adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
               lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);

         } catch (NamingException ex) {

            throw new EJBException(ex.getMessage());

         }

         // get company and extended precision

         try {

             adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);

         } catch (Exception ex) {

             throw new EJBException(ex.getMessage());

         }


         // Convert to functional currency if necessary

         if (CONVERSION_RATE != 1 && CONVERSION_RATE != 0 ) {

             AMOUNT = AMOUNT / CONVERSION_RATE;

         } 

         return EJBCommon.roundIt(AMOUNT, adCompany.getGlFunctionalCurrency().getFcPrecision());

	}

    private double convertFunctionalToForeignCurrency(Integer FC_CODE, String FC_NM,
	    Date CONVERSION_DATE, double CONVERSION_RATE, double AMOUNT, Integer AD_CMPNY) {

	    Debug.print("ApDebitMemoEntryControllerBean convertFunctionalToForeignCurrency");


        LocalGlFunctionalCurrencyRateHome glFunctionalCurrencyRateHome = null;
        LocalAdCompanyHome adCompanyHome = null;

        LocalAdCompany adCompany = null;

        // Initialize EJB Homes

        try {

            glFunctionalCurrencyRateHome = (LocalGlFunctionalCurrencyRateHome)EJBHomeFactory.
               lookUpLocalHome(LocalGlFunctionalCurrencyRateHome.JNDI_NAME, LocalGlFunctionalCurrencyRateHome.class);
            adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
               lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);

         } catch (NamingException ex) {

            throw new EJBException(ex.getMessage());

         }

         // get company and extended precision

         try {

             adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);

         } catch (Exception ex) {

             throw new EJBException(ex.getMessage());

         }


         // Convert to functional currency if necessary

         if (CONVERSION_RATE != 1 && CONVERSION_RATE != 0 ) {

             AMOUNT = AMOUNT / CONVERSION_RATE;

         } else if (CONVERSION_DATE != null) {

         	 try {

         	 	 // Get functional currency rate

         	     LocalGlFunctionalCurrencyRate glReceiptFunctionalCurrencyRate = null;

         	     if (!FC_NM.equals("USD")) {

        	         glReceiptFunctionalCurrencyRate =
	     	             glFunctionalCurrencyRateHome.findByFcCodeAndDate(FC_CODE,
	     	             CONVERSION_DATE, AD_CMPNY);

	     	         AMOUNT = AMOUNT / glReceiptFunctionalCurrencyRate.getFrXToUsd();

         	     }

                 // Get set of book functional currency rate if necessary

                 if (!adCompany.getGlFunctionalCurrency().getFcName().equals("USD")) {

                     LocalGlFunctionalCurrencyRate glCompanyFunctionalCurrencyRate =
                         glFunctionalCurrencyRateHome.findByFcCodeAndDate(adCompany.getGlFunctionalCurrency().
                             getFcCode(), CONVERSION_DATE, AD_CMPNY);

                     AMOUNT = AMOUNT * glCompanyFunctionalCurrencyRate.getFrXToUsd();

                  }


         	 } catch (Exception ex) {

         	 	throw new EJBException(ex.getMessage());

         	 }

         }

         return EJBCommon.roundIt(AMOUNT, adCompany.getGlFunctionalCurrency().getFcPrecision());

	}

   private void postToGl(LocalGlAccountingCalendarValue glAccountingCalendarValue,
      LocalGlChartOfAccount glChartOfAccount,
      boolean isCurrentAcv, byte isDebit, double JL_AMNT, Integer AD_CMPNY) {

      Debug.print("ApDebitMemoEntryControllerBean postToGl");

      LocalGlChartOfAccountBalanceHome glChartOfAccountBalanceHome = null;
      LocalAdCompanyHome adCompanyHome = null;


       // Initialize EJB Home

       try {

           glChartOfAccountBalanceHome = (LocalGlChartOfAccountBalanceHome)EJBHomeFactory.
              lookUpLocalHome(LocalGlChartOfAccountBalanceHome.JNDI_NAME, LocalGlChartOfAccountBalanceHome.class);
           adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
              lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);


       } catch (NamingException ex) {

           throw new EJBException(ex.getMessage());

       }

       try {

               LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);

	       	   LocalGlChartOfAccountBalance glChartOfAccountBalance =
		            glChartOfAccountBalanceHome.findByAcvCodeAndCoaCode(
		               	  glAccountingCalendarValue.getAcvCode(),
		               	  glChartOfAccount.getCoaCode(), AD_CMPNY);

	           String ACCOUNT_TYPE = glChartOfAccount.getCoaAccountType();
	           short FC_EXTNDD_PRCSN = adCompany.getGlFunctionalCurrency().getFcPrecision();



	           if (((ACCOUNT_TYPE.equals("ASSET") || ACCOUNT_TYPE.equals("EXPENSE")) &&
				    isDebit == EJBCommon.TRUE) ||
				    (!ACCOUNT_TYPE.equals("ASSET") && !ACCOUNT_TYPE.equals("EXPENSE") &&
				    isDebit == EJBCommon.FALSE)) {

 			        glChartOfAccountBalance.setCoabEndingBalance(
				       EJBCommon.roundIt(glChartOfAccountBalance.getCoabEndingBalance() + JL_AMNT, FC_EXTNDD_PRCSN));

				    if (!isCurrentAcv) {

				    	glChartOfAccountBalance.setCoabBeginningBalance(
				       		EJBCommon.roundIt(glChartOfAccountBalance.getCoabBeginningBalance() + JL_AMNT, FC_EXTNDD_PRCSN));

					}


			  } else {

				    glChartOfAccountBalance.setCoabEndingBalance(
				       EJBCommon.roundIt(glChartOfAccountBalance.getCoabEndingBalance() - JL_AMNT, FC_EXTNDD_PRCSN));

				    if (!isCurrentAcv) {

				    	glChartOfAccountBalance.setCoabBeginningBalance(
				       		EJBCommon.roundIt(glChartOfAccountBalance.getCoabBeginningBalance() - JL_AMNT, FC_EXTNDD_PRCSN));

					}

		 	  }

		 	  if (isCurrentAcv) {

			 	 if (isDebit == EJBCommon.TRUE) {

		 			glChartOfAccountBalance.setCoabTotalDebit(
		 				EJBCommon.roundIt(glChartOfAccountBalance.getCoabTotalDebit() + JL_AMNT, FC_EXTNDD_PRCSN));

		 		 } else {

		 			glChartOfAccountBalance.setCoabTotalCredit(
		 				EJBCommon.roundIt(glChartOfAccountBalance.getCoabTotalCredit() + JL_AMNT, FC_EXTNDD_PRCSN));
		 		 }

		 	}

       } catch (Exception ex) {

       	   Debug.printStackTrace(ex);
       	   throw new EJBException(ex.getMessage());

       }


   }

   private LocalApVoucherLineItem addApVliEntry(ApModVoucherLineItemDetails mdetails, LocalApVoucher apVoucher,
        LocalApTaxCode apTaxCode, LocalInvItemLocation invItemLocation, Integer AD_CMPNY) {

		Debug.print("ArCreditMemoEntryControllerBean addArIlEntry");

		LocalApVoucherLineItemHome apVoucherLineItemHome = null;
		LocalInvUnitOfMeasureHome invUnitOfMeasureHome = null;

        // Initialize EJB Home

        try {

            apVoucherLineItemHome = (LocalApVoucherLineItemHome)EJBHomeFactory.
                lookUpLocalHome(LocalApVoucherLineItemHome.JNDI_NAME, LocalApVoucherLineItemHome.class);
            invUnitOfMeasureHome = (LocalInvUnitOfMeasureHome)EJBHomeFactory.
                lookUpLocalHome(LocalInvUnitOfMeasureHome.JNDI_NAME, LocalInvUnitOfMeasureHome.class);

        } catch (NamingException ex) {

            throw new EJBException(ex.getMessage());

        }

        try {

        	short precisionUnit = this.getGlFcPrecisionUnit(AD_CMPNY);

        	double VLI_AMNT = 0d;
        	double VLI_TAX_AMNT = 0d;

			// calculate net amount

			if (apTaxCode.getTcType().equals("INCLUSIVE")) {

				VLI_AMNT = EJBCommon.roundIt(mdetails.getVliAmount() / (1 + (apTaxCode.getTcRate() / 100)), precisionUnit);

	        } else {

	            // tax exclusive, none, zero rated or exempt

	            VLI_AMNT = mdetails.getVliAmount();

	    	}

	    	// calculate tax

	    	if (!apTaxCode.getTcType().equals("NONE") &&
	    	    !apTaxCode.getTcType().equals("EXEMPT")) {


	        	if (apTaxCode.getTcType().equals("INCLUSIVE")) {

	        		VLI_TAX_AMNT = EJBCommon.roundIt(mdetails.getVliAmount() - VLI_AMNT, precisionUnit);

	        	} else if (apTaxCode.getTcType().equals("EXCLUSIVE")) {

	        		VLI_TAX_AMNT = EJBCommon.roundIt(mdetails.getVliAmount() * apTaxCode.getTcRate() / 100, precisionUnit);

	            } else {

	            	// tax none zero-rated or exempt

	        	}

		    }

        	LocalApVoucherLineItem apVoucherLineItem = apVoucherLineItemHome.create(
        		mdetails.getVliLine(), mdetails.getVliQuantity(), mdetails.getVliUnitCost(),
        		VLI_AMNT, VLI_TAX_AMNT, mdetails.getVliDiscount1(), mdetails.getVliDiscount2(),
        		 mdetails.getVliDiscount3(), mdetails.getVliDiscount4(), mdetails.getVliTotalDiscount(),
        		 null, null, null, mdetails.getVliTax(),
        		 AD_CMPNY);

            //apVoucher.addApVoucherLineItem(apVoucherLineItem);
            apVoucherLineItem.setApVoucher(apVoucher);

            //invItemLocation.addApVoucherLineItem(apVoucherLineItem);
            apVoucherLineItem.setInvItemLocation(invItemLocation);

            LocalInvUnitOfMeasure invUnitOfMeasure = invUnitOfMeasureHome.findByUomName(mdetails.getVliUomName(), AD_CMPNY);
            //invUnitOfMeasure.addApVoucherLineItem(apVoucherLineItem);
            apVoucherLineItem.setInvUnitOfMeasure(invUnitOfMeasure);

         	return apVoucherLineItem;

        } catch (Exception ex) {

        	Debug.printStackTrace(ex);
        	getSessionContext().setRollbackOnly();
        	throw new EJBException(ex.getMessage());

        }

	}

	private void addApDrEntry(short DR_LN, String DR_CLSS,
	    byte DR_DBT, double DR_AMNT, Integer COA_CODE, LocalApVoucher apVoucher, Integer AD_BRNCH, Integer AD_CMPNY) throws
	    GlobalBranchAccountNumberInvalidException {

		Debug.print("ArCreditMemoEntryControllerBean addArDrEntry");

		LocalApDistributionRecordHome apDistributionRecordHome = null;
		LocalGlChartOfAccountHome glChartOfAccountHome = null;
		LocalAdCompanyHome adCompanyHome = null;


        // Initialize EJB Home

        try {

            apDistributionRecordHome = (LocalApDistributionRecordHome)EJBHomeFactory.
                lookUpLocalHome(LocalApDistributionRecordHome.JNDI_NAME, LocalApDistributionRecordHome.class);
            glChartOfAccountHome = (LocalGlChartOfAccountHome)EJBHomeFactory.
                lookUpLocalHome(LocalGlChartOfAccountHome.JNDI_NAME, LocalGlChartOfAccountHome.class);
            adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);

        } catch (NamingException ex) {

            throw new EJBException(ex.getMessage());

        }

        try {

        	// get company

        	LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey( AD_CMPNY);

        	LocalGlChartOfAccount glChartOfAccount = null;

    		try {

    		    glChartOfAccount = glChartOfAccountHome.findByCoaCodeAndBranchCode(COA_CODE, AD_BRNCH, AD_CMPNY);

    		    if (glChartOfAccount.getCoaEnable() == EJBCommon.FALSE)
        		    throw new GlobalBranchAccountNumberInvalidException();

    		} catch (FinderException ex) {

        		throw new GlobalBranchAccountNumberInvalidException();

        	}

		    // create distribution record

		    LocalApDistributionRecord apDistributionRecord = apDistributionRecordHome.create(
			    DR_LN, DR_CLSS, EJBCommon.roundIt(DR_AMNT, adCompany.getGlFunctionalCurrency().getFcPrecision()), DR_DBT,
			    EJBCommon.FALSE, EJBCommon.FALSE, AD_CMPNY);

			//apVoucher.addApDistributionRecord(apDistributionRecord);
			apDistributionRecord.setApVoucher(apVoucher);
			//glChartOfAccount.addApDistributionRecord(apDistributionRecord);
			apDistributionRecord.setGlChartOfAccount(glChartOfAccount);

        } catch(GlobalBranchAccountNumberInvalidException ex) {

            throw ex;

        } catch (Exception ex) {

        	Debug.printStackTrace(ex);
        	getSessionContext().setRollbackOnly();
        	throw new EJBException(ex.getMessage());

        }

	}

	private void postToInv(LocalApVoucherLineItem apVoucherLineItem, Date CST_DT, double CST_QTY_RCVD, double CST_ITM_CST,
			double CST_RMNNG_QTY, double CST_RMNNG_VL, double CST_VRNC_VL, String USR_NM, Integer AD_BRNCH, Integer AD_CMPNY) throws
	   		AdPRFCoaGlVarianceAccountNotFoundException {

    	Debug.print("ApDebitMemoEntryControllerBean postToInv");

    	LocalInvCostingHome invCostingHome = null;
    	LocalAdPreferenceHome adPreferenceHome = null;
    	LocalAdCompanyHome adCompanyHome = null;
    	LocalInvAdjustmentLineHome invAdjustmentLineHome = null;

        // Initialize EJB Home

        try {

          invCostingHome = (LocalInvCostingHome)EJBHomeFactory.
              lookUpLocalHome(LocalInvCostingHome.JNDI_NAME, LocalInvCostingHome.class);
          adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
              lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);
          adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
              lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
          invAdjustmentLineHome = (LocalInvAdjustmentLineHome)EJBHomeFactory.
  	          lookUpLocalHome(LocalInvAdjustmentLineHome.JNDI_NAME, LocalInvAdjustmentLineHome.class);

        } catch (NamingException ex) {

          throw new EJBException(ex.getMessage());

        }

        try {

           LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);
           LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
           LocalInvItemLocation invItemLocation = apVoucherLineItem.getInvItemLocation();
           int CST_LN_NMBR = 0;

           CST_QTY_RCVD = EJBCommon.roundIt(CST_QTY_RCVD, adPreference.getPrfInvQuantityPrecisionUnit());
           CST_ITM_CST = EJBCommon.roundIt(CST_ITM_CST, adCompany.getGlFunctionalCurrency().getFcPrecision());
           CST_RMNNG_QTY = EJBCommon.roundIt(CST_RMNNG_QTY, adPreference.getPrfInvQuantityPrecisionUnit());
           CST_RMNNG_VL = EJBCommon.roundIt(CST_RMNNG_VL, adCompany.getGlFunctionalCurrency().getFcPrecision());

           try {

           	   // generate line number
               LocalInvCosting invCurrentCosting = invCostingHome.getByMaxCstLineNumberAndCstDateToLongAndIiNameAndLocName(CST_DT.getTime(), invItemLocation.getInvItem().getIiName(), invItemLocation.getInvLocation().getLocName(), AD_BRNCH, AD_CMPNY);
               CST_LN_NMBR = invCurrentCosting.getCstLineNumber() + 1;

           } catch (FinderException ex) {

           	   CST_LN_NMBR = 1;

           }

           //void subsequent cost variance adjustments
           Collection invAdjustmentLines = invAdjustmentLineHome.findUnvoidAndIsCostVarianceGreaterThanAdjDateAndIlCodeAndBrCode(
           		CST_DT, invItemLocation.getIlCode(), AD_BRNCH, AD_CMPNY);
           Iterator i = invAdjustmentLines.iterator();

           while (i.hasNext()){

           	LocalInvAdjustmentLine invAdjustmentLine = (LocalInvAdjustmentLine) i.next();
           	this.voidInvAdjustment(invAdjustmentLine.getInvAdjustment(), AD_BRNCH, AD_CMPNY);

           }

           // create costing
           LocalInvCosting invCosting = invCostingHome.create(CST_DT, CST_DT.getTime(), CST_LN_NMBR, CST_QTY_RCVD, CST_ITM_CST, 0d, 0d, 0d, 0d, 0d, 0d, CST_RMNNG_QTY, CST_RMNNG_VL, 0d, 0d, 0d, AD_BRNCH, AD_CMPNY);
           //invItemLocation.addInvCosting(invCosting);
           invCosting.setInvItemLocation(invItemLocation);
           invCosting.setApVoucherLineItem(apVoucherLineItem);

           // if cost variance is not 0, generate cost variance for the transaction
           if(CST_VRNC_VL != 0) {

           	this.generateCostVariance(invCosting.getInvItemLocation(), CST_VRNC_VL,
           			"APVOU" + apVoucherLineItem.getApVoucher().getVouDocumentNumber(),
					apVoucherLineItem.getApVoucher().getVouDescription(),
					apVoucherLineItem.getApVoucher().getVouDate(), USR_NM, AD_BRNCH, AD_CMPNY);

           }

           // propagate balance if necessary

           Collection invCostings = invCostingHome.findByGreaterThanCstDateAndIiNameAndLocName(CST_DT, invItemLocation.getInvItem().getIiName(), invItemLocation.getInvLocation().getLocName(), AD_BRNCH, AD_CMPNY);

           i = invCostings.iterator();

           while (i.hasNext()) {

               LocalInvCosting invPropagatedCosting = (LocalInvCosting)i.next();

               invPropagatedCosting.setCstRemainingQuantity(invPropagatedCosting.getCstRemainingQuantity() + CST_QTY_RCVD);
               invPropagatedCosting.setCstRemainingValue(invPropagatedCosting.getCstRemainingValue() + CST_ITM_CST);

           }

           // regenerate cost variance
           this.regenerateCostVariance(invCostings, invCosting, AD_BRNCH, AD_CMPNY);

        } catch (AdPRFCoaGlVarianceAccountNotFoundException ex) {

        	getSessionContext().setRollbackOnly();
        	throw ex;

        } catch (Exception ex) {

      	   Debug.printStackTrace(ex);
      	   getSessionContext().setRollbackOnly();
           throw new EJBException(ex.getMessage());

        }



    }

    private double convertByUomFromAndItemAndQuantity(LocalInvUnitOfMeasure invFromUnitOfMeasure, LocalInvItem invItem, double QTY_RCVD, Integer AD_CMPNY) {

		Debug.print("ApDebitMemoEntryControllerBean convertByUomFromAndItemAndQuantity");

		LocalAdPreferenceHome adPreferenceHome = null;
		LocalInvUnitOfMeasureConversionHome invUnitOfMeasureConversionHome = null;

        // Initialize EJB Home

        try {

            adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
                 lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);
            invUnitOfMeasureConversionHome = (LocalInvUnitOfMeasureConversionHome)EJBHomeFactory.
            	lookUpLocalHome(LocalInvUnitOfMeasureConversionHome.JNDI_NAME, LocalInvUnitOfMeasureConversionHome.class);

        } catch (NamingException ex) {

            throw new EJBException(ex.getMessage());

        }

        try {

            LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);

            LocalInvUnitOfMeasureConversion invUnitOfMeasureConversion = invUnitOfMeasureConversionHome.findUmcByIiNameAndUomName(invItem.getIiName(), invFromUnitOfMeasure.getUomName(), AD_CMPNY);
            LocalInvUnitOfMeasureConversion invDefaultUomConversion = invUnitOfMeasureConversionHome.findUmcByIiNameAndUomName(invItem.getIiName(), invItem.getInvUnitOfMeasure().getUomName(), AD_CMPNY);

        	return EJBCommon.roundIt(QTY_RCVD * invDefaultUomConversion.getUmcConversionFactor() / invUnitOfMeasureConversion.getUmcConversionFactor(), adPreference.getPrfInvQuantityPrecisionUnit());


        } catch (Exception ex) {

        	Debug.printStackTrace(ex);
        	getSessionContext().setRollbackOnly();
        	throw new EJBException(ex.getMessage());

        }

	}
    private void voidInvAdjustment(LocalInvAdjustment invAdjustment, Integer AD_BRNCH, Integer AD_CMPNY){

    	Debug.print("ApVoucherEntryController voidInvAdjustment");

    	try{

    			Collection invDistributionRecords = invAdjustment.getInvDistributionRecords();
    			ArrayList list = new ArrayList();

    			Iterator i = invDistributionRecords.iterator();

    			while (i.hasNext()) {

    				LocalInvDistributionRecord invDistributionRecord = (LocalInvDistributionRecord)i.next();

    				list.add(invDistributionRecord);

    			}

    			i = list.iterator();

    			while (i.hasNext()) {

    				LocalInvDistributionRecord invDistributionRecord = (LocalInvDistributionRecord)i.next();

    				this.addInvDrEntry(invAdjustment.getInvDrNextLine(), invDistributionRecord.getDrClass(),
    						invDistributionRecord.getDrDebit() == EJBCommon.TRUE ? EJBCommon.FALSE : EJBCommon.TRUE,
    								invDistributionRecord.getDrAmount(), EJBCommon.TRUE,
									invDistributionRecord.getInvChartOfAccount().getCoaCode(), invAdjustment, AD_BRNCH, AD_CMPNY);

    			}

    			Collection invAjustmentLines = invAdjustment.getInvAdjustmentLines();
    			i = invAjustmentLines.iterator();
    			list.clear();

    			while (i.hasNext()) {

    				LocalInvAdjustmentLine invAdjustmentLine = (LocalInvAdjustmentLine)i.next();

    				list.add(invAdjustmentLine);

    			}

    			i = list.iterator();

    			while (i.hasNext()) {

    				LocalInvAdjustmentLine invAdjustmentLine = (LocalInvAdjustmentLine)i.next();

    				this.addInvAlEntry(invAdjustmentLine.getInvItemLocation(),
    						invAdjustment, (invAdjustmentLine.getAlUnitCost()) * - 1, EJBCommon.TRUE, AD_CMPNY);

    			}

    			invAdjustment.setAdjVoid(EJBCommon.TRUE);

    			this.executeInvAdjPost(invAdjustment.getAdjCode(), invAdjustment.getAdjLastModifiedBy(), AD_BRNCH, AD_CMPNY);

    	} catch (Exception ex) {

    		Debug.printStackTrace(ex);
    		getSessionContext().setRollbackOnly();
    		throw new EJBException(ex.getMessage());

    	}

    }

    private void generateCostVariance(LocalInvItemLocation invItemLocation, double CST_VRNC_VL, String ADJ_RFRNC_NMBR,
    		String ADJ_DSCRPTN, Date ADJ_DT, String USR_NM, Integer AD_BRNCH,
			Integer AD_CMPNY) throws
			AdPRFCoaGlVarianceAccountNotFoundException {

    	Debug.print("ApVoucherEntryController generateCostVariance");
    	/*
    	LocalAdPreferenceHome adPreferenceHome = null;
    	LocalGlChartOfAccountHome glChartOfAccountHome = null;
    	LocalAdBranchItemLocationHome adBranchItemLocationHome = null;

    	// Initialize EJB Home

    	try {

    		adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);
    		glChartOfAccountHome = (LocalGlChartOfAccountHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlChartOfAccountHome.JNDI_NAME, LocalGlChartOfAccountHome.class);
    		adBranchItemLocationHome = (LocalAdBranchItemLocationHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdBranchItemLocationHome.JNDI_NAME, LocalAdBranchItemLocationHome.class);


    	} catch (NamingException ex) {

    		throw new EJBException(ex.getMessage());

    	}

    	try{

    		LocalInvAdjustment newInvAdjustment = this.saveInvAdjustment(ADJ_RFRNC_NMBR, ADJ_DSCRPTN, ADJ_DT, USR_NM, AD_BRNCH,
    				AD_CMPNY);
    		LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);
    		LocalGlChartOfAccount glCoaVarianceAccount = null;

    		if(adPreference.getPrfInvGlCoaVarianceAccount() == null)
    			throw new AdPRFCoaGlVarianceAccountNotFoundException();

    		try{

    			glCoaVarianceAccount = glChartOfAccountHome.findByPrimaryKey(adPreference.getPrfInvGlCoaVarianceAccount());
    			//glCoaVarianceAccount.addInvAdjustment(newInvAdjustment);
    			newInvAdjustment.setGlChartOfAccount(glCoaVarianceAccount);

    		} catch (FinderException ex) {

    			throw new AdPRFCoaGlVarianceAccountNotFoundException();

    		}

    		LocalInvAdjustmentLine invAdjustmentLine = this.addInvAlEntry(invItemLocation, newInvAdjustment, CST_VRNC_VL,
    				EJBCommon.FALSE, AD_CMPNY);

    		// check for branch mapping

    		LocalAdBranchItemLocation adBranchItemLocation = null;

    		try{

    			adBranchItemLocation = adBranchItemLocationHome.findBilByIlCodeAndBrCode(
    					invAdjustmentLine.getInvItemLocation().getIlCode(), AD_BRNCH, AD_CMPNY);

    		} catch (FinderException ex) {

    		}

    		LocalGlChartOfAccount glInventoryChartOfAccount = null;

    		if (adBranchItemLocation == null) {

    			glInventoryChartOfAccount = glChartOfAccountHome.findByPrimaryKey(
    					invAdjustmentLine.getInvItemLocation().getIlGlCoaInventoryAccount());
    		} else {

    			glInventoryChartOfAccount = glChartOfAccountHome.findByPrimaryKey(
    					adBranchItemLocation.getBilCoaGlInventoryAccount());

    		}


    		boolean isDebit = CST_VRNC_VL < 0 ? false : true;

    		//inventory dr
    		this.addInvDrEntry(newInvAdjustment.getInvDrNextLine(), "INVENTORY",
    				isDebit == true ? EJBCommon.TRUE : EJBCommon.FALSE, Math.abs(CST_VRNC_VL), EJBCommon.FALSE,
    						glInventoryChartOfAccount.getCoaCode(), newInvAdjustment, AD_BRNCH, AD_CMPNY);

    		//variance dr
    		this.addInvDrEntry(newInvAdjustment.getInvDrNextLine(),"VARIANCE",
    				!isDebit == true ? EJBCommon.TRUE : EJBCommon.FALSE, Math.abs(CST_VRNC_VL), EJBCommon.FALSE,
    						glCoaVarianceAccount.getCoaCode(), newInvAdjustment, AD_BRNCH, AD_CMPNY);

    		this.executeInvAdjPost(newInvAdjustment.getAdjCode(), newInvAdjustment.getAdjLastModifiedBy(), AD_BRNCH,
    				AD_CMPNY);

    	} catch (AdPRFCoaGlVarianceAccountNotFoundException ex) {

    		getSessionContext().setRollbackOnly();
    		throw ex;

    	} catch (Exception ex) {

    		Debug.printStackTrace(ex);
    		getSessionContext().setRollbackOnly();
    		throw new EJBException(ex.getMessage());

    	}*/

    }

    private void regenerateCostVariance(Collection invCostings, LocalInvCosting invCosting, Integer AD_BRNCH, Integer AD_CMPNY)
    throws AdPRFCoaGlVarianceAccountNotFoundException {

    	Debug.print("ApVoucherEntryController regenerateCostVariance");
    	/*
    	try {

    		Iterator i = invCostings.iterator();
    		LocalInvCosting prevInvCosting = invCosting;

    		while (i.hasNext()) {

    			LocalInvCosting invPropagatedCosting = (LocalInvCosting)i.next();

    			if(prevInvCosting.getCstRemainingQuantity() < 0) {

    				double TTL_CST = 0;
    				double QNTY = 0;
    				String ADJ_RFRNC_NMBR = "";
    				String ADJ_DSCRPTN = "";
    				String ADJ_CRTD_BY = "";

    				// get unit cost adjusment, document number and unit of measure
    				if (invPropagatedCosting.getApPurchaseOrderLine() != null) {

    					TTL_CST = invPropagatedCosting.getApPurchaseOrderLine().getPlAmount();
						QNTY =  this.convertByUomFromAndItemAndQuantity(
								invPropagatedCosting.getApPurchaseOrderLine().getInvUnitOfMeasure(),
								invPropagatedCosting.getApPurchaseOrderLine().getInvItemLocation().getInvItem(),
								invPropagatedCosting.getApPurchaseOrderLine().getPlQuantity(), AD_CMPNY);

    					ADJ_DSCRPTN = invPropagatedCosting.getApPurchaseOrderLine().getApPurchaseOrder().getPoDescription();
    					ADJ_CRTD_BY = invPropagatedCosting.getApPurchaseOrderLine().getApPurchaseOrder().getPoPostedBy();
    					ADJ_RFRNC_NMBR = "APRI" +
						invPropagatedCosting.getApPurchaseOrderLine().getApPurchaseOrder().getPoDocumentNumber();

    				} else if (invPropagatedCosting.getApVoucherLineItem() != null){

    					TTL_CST = invPropagatedCosting.getApVoucherLineItem().getVliAmount();
						QNTY = this.convertByUomFromAndItemAndQuantity(
								invPropagatedCosting.getApVoucherLineItem().getInvUnitOfMeasure(),
								invPropagatedCosting.getApVoucherLineItem().getInvItemLocation().getInvItem(),
								invPropagatedCosting.getApVoucherLineItem().getVliQuantity(), AD_CMPNY);

    					if (invPropagatedCosting.getApVoucherLineItem().getApVoucher() != null) {

    						ADJ_DSCRPTN = invPropagatedCosting.getApVoucherLineItem().getApVoucher().getVouDescription();
    						ADJ_CRTD_BY = invPropagatedCosting.getApVoucherLineItem().getApVoucher().getVouPostedBy();
    						ADJ_RFRNC_NMBR = "APVOU" +
							invPropagatedCosting.getApVoucherLineItem().getApVoucher().getVouDocumentNumber();

    					} else if (invPropagatedCosting.getApVoucherLineItem().getApCheck() != null) {

    						ADJ_DSCRPTN = invPropagatedCosting.getApVoucherLineItem().getApCheck().getChkDescription();
    						ADJ_CRTD_BY = invPropagatedCosting.getApVoucherLineItem().getApCheck().getChkPostedBy();
    						ADJ_RFRNC_NMBR = "APCHK" +
							invPropagatedCosting.getApVoucherLineItem().getApCheck().getChkDocumentNumber();

    					}

    				} else if (invPropagatedCosting.getArInvoiceLineItem() != null){

    					QNTY = this.convertByUomFromAndItemAndQuantity(
								invPropagatedCosting.getArInvoiceLineItem().getInvUnitOfMeasure(),
								invPropagatedCosting.getArInvoiceLineItem().getInvItemLocation().getInvItem(),
								invPropagatedCosting.getArInvoiceLineItem().getIliQuantity(), AD_CMPNY);
   						TTL_CST = prevInvCosting.getCstRemainingValue() - invPropagatedCosting.getCstRemainingValue();

   						if(invPropagatedCosting.getArInvoiceLineItem().getArInvoice() != null){

   	   							ADJ_DSCRPTN = invPropagatedCosting.getArInvoiceLineItem().getArInvoice().getInvDescription();
   	   							ADJ_CRTD_BY = invPropagatedCosting.getArInvoiceLineItem().getArInvoice().getInvPostedBy();
   	   							ADJ_RFRNC_NMBR = "ARCM" +
   								invPropagatedCosting.getArInvoiceLineItem().getArInvoice().getInvNumber();

   	   						} else if(invPropagatedCosting.getArInvoiceLineItem().getArReceipt() != null){

   	   							ADJ_DSCRPTN = invPropagatedCosting.getArInvoiceLineItem().getArReceipt().getRctDescription();
   	   							ADJ_CRTD_BY = invPropagatedCosting.getArInvoiceLineItem().getArReceipt().getRctPostedBy();
   	   							ADJ_RFRNC_NMBR = "ARMR" +
   								invPropagatedCosting.getArInvoiceLineItem().getArReceipt().getRctNumber();

   	   						}

    				} else if (invPropagatedCosting.getArSalesOrderInvoiceLine() != null){

   						TTL_CST = prevInvCosting.getCstRemainingValue() - invPropagatedCosting.getCstRemainingValue();
    					QNTY = this.convertByUomFromAndItemAndQuantity(
								invPropagatedCosting.getArSalesOrderInvoiceLine().getArSalesOrderLine().getInvUnitOfMeasure(),
								invPropagatedCosting.getArSalesOrderInvoiceLine().getArSalesOrderLine().getInvItemLocation().getInvItem(),
								invPropagatedCosting.getArSalesOrderInvoiceLine().getSilQuantityDelivered(), AD_CMPNY);

    					ADJ_DSCRPTN = invPropagatedCosting.getArSalesOrderInvoiceLine().getArInvoice().getInvDescription();
    					ADJ_CRTD_BY = invPropagatedCosting.getArSalesOrderInvoiceLine().getArInvoice().getInvPostedBy();
    					ADJ_RFRNC_NMBR = "ARCM" +
						invPropagatedCosting.getArSalesOrderInvoiceLine().getArInvoice().getInvNumber();

    				} else if (invPropagatedCosting.getInvAdjustmentLine() != null){

    					ADJ_DSCRPTN = invPropagatedCosting.getInvAdjustmentLine().getInvAdjustment().getAdjDescription();
    					ADJ_CRTD_BY = invPropagatedCosting.getInvAdjustmentLine().getInvAdjustment().getAdjPostedBy();
    					ADJ_RFRNC_NMBR = "INVADJ" +
						invPropagatedCosting.getInvAdjustmentLine().getInvAdjustment().getAdjDocumentNumber();

    					if(invPropagatedCosting.getInvAdjustmentLine().getAlAdjustQuantity() != 0) {

    						TTL_CST = (invPropagatedCosting.getInvAdjustmentLine().getAlUnitCost() *
    								invPropagatedCosting.getInvAdjustmentLine().getAlAdjustQuantity());
    						QNTY =  this.convertByUomFromAndItemAndQuantity(
    								invPropagatedCosting.getInvAdjustmentLine().getInvUnitOfMeasure(),
									invPropagatedCosting.getInvAdjustmentLine().getInvItemLocation().getInvItem(),
									invPropagatedCosting.getInvAdjustmentLine().getAlAdjustQuantity(), AD_CMPNY);

    					}

    				} else if (invPropagatedCosting.getInvAssemblyTransferLine() != null){

    					QNTY = invPropagatedCosting.getInvAssemblyTransferLine().getAtlAssembleQuantity();
    					TTL_CST = invPropagatedCosting.getInvAssemblyTransferLine().getAtlAssembleCost();

    					ADJ_DSCRPTN = invPropagatedCosting.getInvAssemblyTransferLine().getInvAssemblyTransfer().getAtrDescription();
    					ADJ_CRTD_BY = invPropagatedCosting.getInvAssemblyTransferLine().getInvAssemblyTransfer().getAtrPostedBy();
    					ADJ_RFRNC_NMBR = "INVAT" +
						invPropagatedCosting.getInvAssemblyTransferLine().getInvAssemblyTransfer().getAtrDocumentNumber();

    				} else if (invPropagatedCosting.getInvBranchStockTransferLine() != null){

    					if(invPropagatedCosting.getInvBranchStockTransferLine().getInvBranchStockTransfer().getBstTransferOutNumber()
    							!= null) {

    						TTL_CST = invPropagatedCosting.getInvBranchStockTransferLine().getBslAmount();
							QNTY =  this.convertByUomFromAndItemAndQuantity(
									invPropagatedCosting.getInvBranchStockTransferLine().getInvUnitOfMeasure(),
									invPropagatedCosting.getInvBranchStockTransferLine().getInvItemLocation().getInvItem(),
									invPropagatedCosting.getInvBranchStockTransferLine().getBslQuantityReceived(), AD_CMPNY);

    					} else {

    						TTL_CST = invPropagatedCosting.getInvBranchStockTransferLine().getBslAmount();
							QNTY =  this.convertByUomFromAndItemAndQuantity(
									invPropagatedCosting.getInvBranchStockTransferLine().getInvUnitOfMeasure(),
									invPropagatedCosting.getInvBranchStockTransferLine().getInvItemLocation().getInvItem(),
									invPropagatedCosting.getInvBranchStockTransferLine().getBslQuantity(), AD_CMPNY);
    					}

    					ADJ_DSCRPTN =
    						invPropagatedCosting.getInvBranchStockTransferLine().getInvBranchStockTransfer().getBstDescription();
    					ADJ_CRTD_BY =
    						invPropagatedCosting.getInvBranchStockTransferLine().getInvBranchStockTransfer().getBstPostedBy();
    					ADJ_RFRNC_NMBR = "INVBST" +
						invPropagatedCosting.getInvBranchStockTransferLine().getInvBranchStockTransfer().getBstNumber();

    				} else if (invPropagatedCosting.getInvBuildUnbuildAssemblyLine() != null){

   						TTL_CST = prevInvCosting.getCstRemainingValue() - invPropagatedCosting.getCstRemainingValue();
    					QNTY =  invPropagatedCosting.getInvBuildUnbuildAssemblyLine().getBlBuildQuantity();
    					ADJ_DSCRPTN =
    						invPropagatedCosting.getInvBuildUnbuildAssemblyLine().getInvBuildUnbuildAssembly().getBuaDescription();
    					ADJ_CRTD_BY =
    						invPropagatedCosting.getInvBuildUnbuildAssemblyLine().getInvBuildUnbuildAssembly().getBuaPostedBy();
    					ADJ_RFRNC_NMBR = "INVBUA" +
						invPropagatedCosting.getInvBuildUnbuildAssemblyLine().getInvBuildUnbuildAssembly().getBuaDocumentNumber();

    				} else if (invPropagatedCosting.getInvStockIssuanceLine()!= null){

    					TTL_CST = invPropagatedCosting.getInvStockIssuanceLine().getSilIssueCost();
						QNTY =  this.convertByUomFromAndItemAndQuantity(
								invPropagatedCosting.getInvStockIssuanceLine().getInvUnitOfMeasure(),
								invPropagatedCosting.getInvStockIssuanceLine().getInvItemLocation().getInvItem(),
								invPropagatedCosting.getInvStockIssuanceLine().getSilIssueQuantity(), AD_CMPNY);
    					ADJ_DSCRPTN = invPropagatedCosting.getInvStockIssuanceLine().getInvStockIssuance().getSiDescription();
    					ADJ_CRTD_BY = invPropagatedCosting.getInvStockIssuanceLine().getInvStockIssuance().getSiPostedBy();
    					ADJ_RFRNC_NMBR = "INVSI" +
						invPropagatedCosting.getInvStockIssuanceLine().getInvStockIssuance().getSiDocumentNumber();

    				} else if (invPropagatedCosting.getInvStockTransferLine()!= null) {

    					TTL_CST = invPropagatedCosting.getInvStockTransferLine().getStlAmount();
						QNTY =  this.convertByUomFromAndItemAndQuantity(
								invPropagatedCosting.getInvStockTransferLine().getInvUnitOfMeasure(),
								invPropagatedCosting.getInvStockTransferLine().getInvItem(),
								invPropagatedCosting.getInvStockTransferLine().getStlQuantityDelivered(), AD_CMPNY);
    					ADJ_DSCRPTN = invPropagatedCosting.getInvStockTransferLine().getInvStockTransfer().getStDescription();
    					ADJ_CRTD_BY = invPropagatedCosting.getInvStockTransferLine().getInvStockTransfer().getStPostedBy();
    					ADJ_RFRNC_NMBR = "INVST" +
						invPropagatedCosting.getInvStockTransferLine().getInvStockTransfer().getStDocumentNumber();

    				} else {

    					prevInvCosting = invPropagatedCosting;
    					continue;

    				}

    				// if quantity is equal 0, no variance.
    				if(QNTY == 0) continue;

    				// compute new cost variance
    				double UNT_CST = TTL_CST/QNTY;
    				double CST_VRNC_VL = (invPropagatedCosting.getCstRemainingQuantity() * UNT_CST -
    						invPropagatedCosting.getCstRemainingValue());

    				if(CST_VRNC_VL != 0)
    					this.generateCostVariance(invPropagatedCosting.getInvItemLocation(), CST_VRNC_VL, ADJ_RFRNC_NMBR,
    							ADJ_DSCRPTN, invPropagatedCosting.getCstDate(), ADJ_CRTD_BY, AD_BRNCH, AD_CMPNY);

    			}

    			// set previous costing
    			prevInvCosting = invPropagatedCosting;

    		}

    	} catch (AdPRFCoaGlVarianceAccountNotFoundException ex){

    		throw ex;

    	} catch (Exception ex) {

    		Debug.printStackTrace(ex);
    		getSessionContext().setRollbackOnly();
    		throw new EJBException(ex.getMessage());

    	}     */
    }

    private void addInvDrEntry(short DR_LN, String DR_CLSS, byte DR_DBT, double DR_AMNT, byte DR_RVRSL, Integer COA_CODE,
    		LocalInvAdjustment invAdjustment, Integer AD_BRNCH, Integer AD_CMPNY)

    throws GlobalBranchAccountNumberInvalidException {

    	Debug.print("ApVoucherEntryController addInvDrEntry");

    	LocalAdCompanyHome adCompanyHome = null;
    	LocalInvDistributionRecordHome invDistributionRecordHome = null;
    	LocalGlChartOfAccountHome glChartOfAccountHome = null;

    	// Initialize EJB Home

    	try {

    		adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
    		invDistributionRecordHome = (LocalInvDistributionRecordHome)EJBHomeFactory.
			lookUpLocalHome(LocalInvDistributionRecordHome.JNDI_NAME, LocalInvDistributionRecordHome.class);
    		glChartOfAccountHome = (LocalGlChartOfAccountHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlChartOfAccountHome.JNDI_NAME, LocalGlChartOfAccountHome.class);


    	} catch (NamingException ex) {

    		throw new EJBException(ex.getMessage());

    	}

    	try {

    		// get company

    		LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);

    		// validate coa

    		LocalGlChartOfAccount glChartOfAccount = null;

    		try {

    			glChartOfAccount = glChartOfAccountHome.findByCoaCodeAndBranchCode(COA_CODE, AD_BRNCH, AD_CMPNY);

    		} catch(FinderException ex) {

    			throw new GlobalBranchAccountNumberInvalidException ();

    		}

    		// create distribution record

    		LocalInvDistributionRecord invDistributionRecord = invDistributionRecordHome.create(DR_LN, DR_CLSS, DR_DBT,
    				EJBCommon.roundIt(DR_AMNT, adCompany.getGlFunctionalCurrency().getFcPrecision()), DR_RVRSL, EJBCommon.FALSE,
					 AD_CMPNY);

    		//invAdjustment.addInvDistributionRecord(invDistributionRecord);
    		invDistributionRecord.setInvAdjustment(invAdjustment);
    		//glChartOfAccount.addInvDistributionRecord(invDistributionRecord);
    		invDistributionRecord.setInvChartOfAccount(glChartOfAccount);

    	} catch(GlobalBranchAccountNumberInvalidException ex) {

    		throw new GlobalBranchAccountNumberInvalidException ();

    	} catch (Exception ex) {

    		Debug.printStackTrace(ex);
    		getSessionContext().setRollbackOnly();
    		throw new EJBException(ex.getMessage());

    	}

    }

    private void executeInvAdjPost(Integer ADJ_CODE, String USR_NM, Integer AD_BRNCH, Integer AD_CMPNY) throws
	GlobalRecordAlreadyDeletedException,
	GlobalTransactionAlreadyPostedException,
	GlJREffectiveDateNoPeriodExistException,
	GlJREffectiveDatePeriodClosedException,
	GlobalJournalNotBalanceException,
	GlobalBranchAccountNumberInvalidException {

    	Debug.print("ApDebitMemoEntryController executeInvAdjPost");

    	LocalInvAdjustmentHome invAdjustmentHome = null;
    	LocalAdCompanyHome adCompanyHome = null;
    	LocalAdPreferenceHome adPreferenceHome = null;
    	LocalGlSetOfBookHome glSetOfBookHome = null;
    	LocalGlAccountingCalendarValueHome glAccountingCalendarValueHome = null;
    	LocalGlJournalHome glJournalHome = null;
    	LocalGlJournalBatchHome glJournalBatchHome = null;
    	LocalGlSuspenseAccountHome glSuspenseAccountHome = null;
    	LocalGlJournalLineHome glJournalLineHome = null;
    	LocalGlJournalSourceHome glJournalSourceHome = null;
    	LocalGlJournalCategoryHome glJournalCategoryHome = null;
    	LocalGlFunctionalCurrencyHome glFunctionalCurrencyHome = null;
    	LocalInvDistributionRecordHome invDistributionRecordHome = null;
    	LocalInvCostingHome invCostingHome = null;
    	LocalGlChartOfAccountHome glChartOfAccountHome = null;
    	LocalInvAdjustmentLineHome invAdjustmentLineHome = null;


    	// Initialize EJB Home

    	try {

    		invAdjustmentHome = (LocalInvAdjustmentHome)EJBHomeFactory.
			lookUpLocalHome(LocalInvAdjustmentHome.JNDI_NAME, LocalInvAdjustmentHome.class);
    		adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
    		adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);
    		glSetOfBookHome = (LocalGlSetOfBookHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlSetOfBookHome.JNDI_NAME, LocalGlSetOfBookHome.class);
    		glAccountingCalendarValueHome = (LocalGlAccountingCalendarValueHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlAccountingCalendarValueHome.JNDI_NAME, LocalGlAccountingCalendarValueHome.class);
    		glJournalHome = (LocalGlJournalHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlJournalHome.JNDI_NAME, LocalGlJournalHome.class);
    		glJournalBatchHome = (LocalGlJournalBatchHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlJournalBatchHome.JNDI_NAME, LocalGlJournalBatchHome.class);
    		glSuspenseAccountHome = (LocalGlSuspenseAccountHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlSuspenseAccountHome.JNDI_NAME, LocalGlSuspenseAccountHome.class);
    		glJournalLineHome = (LocalGlJournalLineHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlJournalLineHome.JNDI_NAME, LocalGlJournalLineHome.class);
    		glJournalSourceHome = (LocalGlJournalSourceHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlJournalSourceHome.JNDI_NAME, LocalGlJournalSourceHome.class);
    		glJournalCategoryHome = (LocalGlJournalCategoryHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlJournalCategoryHome.JNDI_NAME, LocalGlJournalCategoryHome.class);
    		glFunctionalCurrencyHome = (LocalGlFunctionalCurrencyHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlFunctionalCurrencyHome.JNDI_NAME, LocalGlFunctionalCurrencyHome.class);
    		invDistributionRecordHome = (LocalInvDistributionRecordHome)EJBHomeFactory.
			lookUpLocalHome(LocalInvDistributionRecordHome.JNDI_NAME, LocalInvDistributionRecordHome.class);
    		invCostingHome = (LocalInvCostingHome)EJBHomeFactory.
			lookUpLocalHome(LocalInvCostingHome.JNDI_NAME, LocalInvCostingHome.class);
    		glChartOfAccountHome = (LocalGlChartOfAccountHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlChartOfAccountHome.JNDI_NAME, LocalGlChartOfAccountHome.class);
    		invAdjustmentLineHome = (LocalInvAdjustmentLineHome)EJBHomeFactory.
			lookUpLocalHome(LocalInvAdjustmentLineHome.JNDI_NAME, LocalInvAdjustmentLineHome.class);

    	} catch (NamingException ex) {

    		throw new EJBException(ex.getMessage());

    	}

    	try {

    		// validate if adjustment is already deleted

    		LocalInvAdjustment invAdjustment = null;

    		try {

    			invAdjustment = invAdjustmentHome.findByPrimaryKey(ADJ_CODE);

    		} catch (FinderException ex) {

    			throw new GlobalRecordAlreadyDeletedException();

    		}

    		// validate if adjustment is already posted or void

    		if (invAdjustment.getAdjPosted() == EJBCommon.TRUE) {

    			if (invAdjustment.getAdjVoid() != EJBCommon.TRUE)
    			throw new GlobalTransactionAlreadyPostedException();

    		}

    		Collection invAdjustmentLines = null;

    		if(invAdjustment.getAdjVoid() == EJBCommon.FALSE)
    			invAdjustmentLines = invAdjustmentLineHome.findByAlVoidAndAdjCode(EJBCommon.FALSE, invAdjustment.getAdjCode(), AD_CMPNY);
    		else
    			invAdjustmentLines = invAdjustmentLineHome.findByAlVoidAndAdjCode(EJBCommon.TRUE, invAdjustment.getAdjCode(), AD_CMPNY);


    		Iterator i = invAdjustmentLines.iterator();

    		while(i.hasNext()) {


    			LocalInvAdjustmentLine invAdjustmentLine = (LocalInvAdjustmentLine) i.next();

    			LocalInvCosting invCosting =
    				invCostingHome.getByMaxCstDateToLongAndMaxCstLineNumberAndLessThanEqualCstDateAndIiNameAndLocName(
    						invAdjustment.getAdjDate(), invAdjustmentLine.getInvItemLocation().getInvItem().getIiName(),
    						invAdjustmentLine.getInvItemLocation().getInvLocation().getLocName(), AD_BRNCH, AD_CMPNY);

				this.postInvAdjustmentToInventory(invAdjustmentLine, invAdjustment.getAdjDate(), 0,
						invAdjustmentLine.getAlUnitCost(), invCosting.getCstRemainingQuantity(),
						invCosting.getCstRemainingValue() + invAdjustmentLine.getAlUnitCost(), AD_BRNCH, AD_CMPNY);

    		}

    		// post to gl if necessary

    		LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);
    		LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);

    		// validate if date has no period and period is closed

    			LocalGlSetOfBook glJournalSetOfBook = null;

    			try {

    				glJournalSetOfBook = glSetOfBookHome.findByDate(invAdjustment.getAdjDate(), AD_CMPNY);

    			} catch (FinderException ex) {

    				throw new GlJREffectiveDateNoPeriodExistException();

    			}

    			LocalGlAccountingCalendarValue glAccountingCalendarValue =
    				glAccountingCalendarValueHome.findByAcCodeAndDate(
    						glJournalSetOfBook.getGlAccountingCalendar().getAcCode(), invAdjustment.getAdjDate(), AD_CMPNY);


    			if (glAccountingCalendarValue.getAcvStatus() == 'N' ||
    					glAccountingCalendarValue.getAcvStatus() == 'C' ||
						glAccountingCalendarValue.getAcvStatus() == 'P') {

    				throw new GlJREffectiveDatePeriodClosedException();

    			}

    			// check if invoice is balance if not check suspense posting

    			LocalGlJournalLine glOffsetJournalLine = null;

    			Collection invDistributionRecords = null;

    			if (invAdjustment.getAdjVoid() == EJBCommon.FALSE) {

    				invDistributionRecords = invDistributionRecordHome.findImportableDrByDrReversedAndAdjCode(EJBCommon.FALSE,
    						invAdjustment.getAdjCode(), AD_CMPNY);

    			} else {

    				invDistributionRecords = invDistributionRecordHome.findImportableDrByDrReversedAndAdjCode(EJBCommon.TRUE,
    						invAdjustment.getAdjCode(), AD_CMPNY);

    			}


    			Iterator j = invDistributionRecords.iterator();

    			double TOTAL_DEBIT = 0d;
    			double TOTAL_CREDIT = 0d;

    			while (j.hasNext()) {

    				LocalInvDistributionRecord invDistributionRecord = (LocalInvDistributionRecord)j.next();

    				double DR_AMNT = 0d;

    				DR_AMNT = invDistributionRecord.getDrAmount();

    				if (invDistributionRecord.getDrDebit() == EJBCommon.TRUE) {

    					TOTAL_DEBIT += DR_AMNT;

    				} else {

    					TOTAL_CREDIT += DR_AMNT;

    				}

    			}

    			TOTAL_DEBIT = EJBCommon.roundIt(TOTAL_DEBIT, adCompany.getGlFunctionalCurrency().getFcPrecision());
    			TOTAL_CREDIT = EJBCommon.roundIt(TOTAL_CREDIT, adCompany.getGlFunctionalCurrency().getFcPrecision());

    			if (adPreference.getPrfAllowSuspensePosting() == EJBCommon.TRUE &&
    					TOTAL_DEBIT != TOTAL_CREDIT) {

    				LocalGlSuspenseAccount glSuspenseAccount = null;

    				try {

    					glSuspenseAccount = glSuspenseAccountHome.findByJsNameAndJcName("INVENTORY", "INVENTORY ADJUSTMENTS",
    							AD_CMPNY);

    				} catch (FinderException ex) {

    					throw new GlobalJournalNotBalanceException();

    				}

    				if (TOTAL_DEBIT - TOTAL_CREDIT < 0) {

    					glOffsetJournalLine = glJournalLineHome.create((short)(invDistributionRecords.size() + 1), EJBCommon.TRUE,
    							TOTAL_CREDIT - TOTAL_DEBIT, "", AD_CMPNY);

    				} else {

    					glOffsetJournalLine = glJournalLineHome.create((short)(invDistributionRecords.size() + 1), EJBCommon.FALSE,
    							TOTAL_DEBIT - TOTAL_CREDIT, "", AD_CMPNY);

    				}

    				LocalGlChartOfAccount glChartOfAccount = glSuspenseAccount.getGlChartOfAccount();
    				//glChartOfAccount.addGlJournalLine(glOffsetJournalLine);
    				glOffsetJournalLine.setGlChartOfAccount(glChartOfAccount);


    			} else if (adPreference.getPrfAllowSuspensePosting() == EJBCommon.FALSE &&
    					TOTAL_DEBIT != TOTAL_CREDIT) {

    				throw new GlobalJournalNotBalanceException();

    			}

    			// create journal batch if necessary

    			LocalGlJournalBatch glJournalBatch = null;
    			java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat("MM/dd/yyyy");

    			try {

    				glJournalBatch = glJournalBatchHome.findByJbName("JOURNAL IMPORT " + formatter.format(new Date()) +
    						" INVENTORY ADJUSTMENTS", AD_BRNCH, AD_CMPNY);

    			} catch (FinderException ex) {

    			}

    			if (glJournalBatch == null) {

    				glJournalBatch = glJournalBatchHome.create("JOURNAL IMPORT " + formatter.format(new Date()) +
    						" INVENTORY ADJUSTMENTS", "JOURNAL IMPORT", "CLOSED", EJBCommon.getGcCurrentDateWoTime().getTime(),
							USR_NM, AD_BRNCH, AD_CMPNY);

    			}

    			// create journal entry

    			LocalGlJournal glJournal = glJournalHome.create(invAdjustment.getAdjReferenceNumber(),
    					invAdjustment.getAdjDescription(), invAdjustment.getAdjDate(),
						0.0d, null, invAdjustment.getAdjDocumentNumber(), null, 1d, "N/A", null,
						'N', EJBCommon.TRUE, EJBCommon.FALSE,
						USR_NM, new Date(),
						USR_NM, new Date(),
						null, null,
						USR_NM, EJBCommon.getGcCurrentDateWoTime().getTime(),
						null, null, EJBCommon.FALSE, null,
						AD_BRNCH, AD_CMPNY);

    			LocalGlJournalSource glJournalSource = glJournalSourceHome.findByJsName("INVENTORY", AD_CMPNY);
    			glJournal.setGlJournalSource(glJournalSource);

    			LocalGlFunctionalCurrency glFunctionalCurrency = glFunctionalCurrencyHome.findByFcName(
    					adCompany.getGlFunctionalCurrency().getFcName(), AD_CMPNY);
    			glJournal.setGlFunctionalCurrency(glFunctionalCurrency);

    			LocalGlJournalCategory glJournalCategory = glJournalCategoryHome.findByJcName("INVENTORY ADJUSTMENTS", AD_CMPNY);
    			glJournal.setGlJournalCategory(glJournalCategory);

    			if (glJournalBatch != null) {

    				glJournal.setGlJournalBatch(glJournalBatch);

    			}

    			// create journal lines

    			j = invDistributionRecords.iterator();

    			while (j.hasNext()) {

    				LocalInvDistributionRecord invDistributionRecord = (LocalInvDistributionRecord)j.next();

    				double DR_AMNT = 0d;

    				DR_AMNT = invDistributionRecord.getDrAmount();

    				LocalGlJournalLine glJournalLine = glJournalLineHome.create(invDistributionRecord.getDrLine(),
    						invDistributionRecord.getDrDebit(), DR_AMNT, "", AD_CMPNY);

    				glJournalLine.setGlChartOfAccount(invDistributionRecord.getInvChartOfAccount());

    				glJournalLine.setGlJournal(glJournal);

    				invDistributionRecord.setDrImported(EJBCommon.TRUE);


    			}

    			if (glOffsetJournalLine != null) {

    				//glJournal.addGlJournalLine(glOffsetJournalLine);
    				glOffsetJournalLine.setGlJournal(glJournal);

    			}

    			// post journal to gl

    			Collection glJournalLines = glJournal.getGlJournalLines();

    			i = glJournalLines.iterator();

    			while (i.hasNext()) {

    				LocalGlJournalLine glJournalLine = (LocalGlJournalLine)i.next();

    				// post current to current acv

    				this.postToGl(glAccountingCalendarValue,
    						glJournalLine.getGlChartOfAccount(),
							true, glJournalLine.getJlDebit(), glJournalLine.getJlAmount(), AD_CMPNY);


    				// post to subsequent acvs (propagate)

    				Collection glSubsequentAccountingCalendarValues =
    					glAccountingCalendarValueHome.findSubsequentAcvByAcCodeAndAcvPeriodNumber(
    							glJournalSetOfBook.getGlAccountingCalendar().getAcCode(),
								glAccountingCalendarValue.getAcvPeriodNumber(), AD_CMPNY);

    				Iterator acvsIter = glSubsequentAccountingCalendarValues.iterator();

    				while (acvsIter.hasNext()) {

    					LocalGlAccountingCalendarValue glSubsequentAccountingCalendarValue =
    						(LocalGlAccountingCalendarValue)acvsIter.next();

    					this.postToGl(glSubsequentAccountingCalendarValue,
    							glJournalLine.getGlChartOfAccount(),
								false, glJournalLine.getJlDebit(), glJournalLine.getJlAmount(), AD_CMPNY);

    				}

    				// post to subsequent years if necessary

    				Collection glSubsequentSetOfBooks = glSetOfBookHome.findSubsequentSobByAcYear(
    						glJournalSetOfBook.getGlAccountingCalendar().getAcYear(), AD_CMPNY);

    				if (!glSubsequentSetOfBooks.isEmpty() && glJournalSetOfBook.getSobYearEndClosed() == 1) {

    					adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
    					LocalGlChartOfAccount glRetainedEarningsAccount =
    						glChartOfAccountHome.findByCoaAccountNumberAndBranchCode(adCompany.getCmpRetainedEarnings(),
    								AD_BRNCH, AD_CMPNY);

    					Iterator sobIter = glSubsequentSetOfBooks.iterator();

    					while (sobIter.hasNext()) {

    						LocalGlSetOfBook glSubsequentSetOfBook = (LocalGlSetOfBook)sobIter.next();

    						String ACCOUNT_TYPE = glJournalLine.getGlChartOfAccount().getCoaAccountType();

    						// post to subsequent acvs of subsequent set of book(propagate)

    						Collection glAccountingCalendarValues =
    							glAccountingCalendarValueHome.findByAcCode(
    									glSubsequentSetOfBook.getGlAccountingCalendar().getAcCode(), AD_CMPNY);

    						Iterator acvIter = glAccountingCalendarValues.iterator();

    						while (acvIter.hasNext()) {

    							LocalGlAccountingCalendarValue glSubsequentAccountingCalendarValue =
    								(LocalGlAccountingCalendarValue)acvIter.next();

    							if (ACCOUNT_TYPE.equals("ASSET") || ACCOUNT_TYPE.equals("LIABILITY") ||
    									ACCOUNT_TYPE.equals("OWNERS EQUITY")) {

    								this.postToGl(glSubsequentAccountingCalendarValue,
    										glJournalLine.getGlChartOfAccount(),
											false, glJournalLine.getJlDebit(), glJournalLine.getJlAmount(), AD_CMPNY);

    							} else { // revenue & expense

    								this.postToGl(glSubsequentAccountingCalendarValue,
    										glRetainedEarningsAccount,
											false, glJournalLine.getJlDebit(), glJournalLine.getJlAmount(), AD_CMPNY);

    							}

    						}

    						if (glSubsequentSetOfBook.getSobYearEndClosed() == 0) break;

    					}

    				}

    			}

			invAdjustment.setAdjPosted(EJBCommon.TRUE);

    	} catch (GlJREffectiveDateNoPeriodExistException ex) {

    		getSessionContext().setRollbackOnly();
    		throw ex;

    	} catch (GlJREffectiveDatePeriodClosedException ex) {

    		getSessionContext().setRollbackOnly();
    		throw ex;

    	} catch (GlobalJournalNotBalanceException ex) {

    		getSessionContext().setRollbackOnly();
    		throw ex;

    	} catch (GlobalRecordAlreadyDeletedException ex) {

    		getSessionContext().setRollbackOnly();
    		throw ex;

    	} catch (GlobalTransactionAlreadyPostedException ex) {

    		getSessionContext().setRollbackOnly();
    		throw ex;

    	} catch (Exception ex) {

    		Debug.printStackTrace(ex);
    		getSessionContext().setRollbackOnly();
    		throw new EJBException(ex.getMessage());

    	}

    }

    private LocalInvAdjustmentLine addInvAlEntry(LocalInvItemLocation invItemLocation, LocalInvAdjustment invAdjustment,
    		double CST_VRNC_VL, byte AL_VD, Integer AD_CMPNY) {

    	Debug.print("ArMiscReceiptEntryControllerBean addInvAlEntry");

    	LocalInvAdjustmentLineHome invAdjustmentLineHome = null;

    	// Initialize EJB Home

    	try {

    		invAdjustmentLineHome = (LocalInvAdjustmentLineHome)EJBHomeFactory.
			lookUpLocalHome(LocalInvAdjustmentLineHome.JNDI_NAME, LocalInvAdjustmentLineHome.class);

    	} catch (NamingException ex) {

    		throw new EJBException(ex.getMessage());

    	}

    	try {

    		// create dr entry
    		LocalInvAdjustmentLine invAdjustmentLine = null;
    		invAdjustmentLine = invAdjustmentLineHome.create(CST_VRNC_VL, null, null, 0,0, AL_VD, AD_CMPNY);

    		// map adjustment, unit of measure, item location
    		//invAdjustment.addInvAdjustmentLine(invAdjustmentLine);
    		invAdjustmentLine.setInvAdjustment(invAdjustment);
    		//invItemLocation.getInvItem().getInvUnitOfMeasure().addInvAdjustmentLine(invAdjustmentLine);
    		invAdjustmentLine.setInvUnitOfMeasure(invItemLocation.getInvItem().getInvUnitOfMeasure());
    		//invItemLocation.addInvAdjustmentLine(invAdjustmentLine);
    		invAdjustmentLine.setInvItemLocation(invItemLocation);

    		return invAdjustmentLine;

    	} catch (Exception ex) {

    		Debug.printStackTrace(ex);
    		getSessionContext().setRollbackOnly();
    		throw new EJBException(ex.getMessage());

    	}

    }

    private LocalInvAdjustment saveInvAdjustment(String ADJ_RFRNC_NMBR, String ADJ_DSCRPTN,
    		Date ADJ_DATE, String USR_NM, Integer AD_BRNCH, Integer AD_CMPNY){

    	Debug.print("ApVoucherEntryController saveInvAdjustment");

    	LocalInvAdjustmentHome invAdjustmentHome = null;
    	LocalAdDocumentSequenceAssignmentHome adDocumentSequenceAssignmentHome = null;
    	LocalAdBranchDocumentSequenceAssignmentHome adBranchDocumentSequenceAssignmentHome = null;
    	// Initialize EJB Home

    	try{

    		invAdjustmentHome = (LocalInvAdjustmentHome)EJBHomeFactory.
			lookUpLocalHome(LocalInvAdjustmentHome.JNDI_NAME, LocalInvAdjustmentHome.class);
			adDocumentSequenceAssignmentHome = (LocalAdDocumentSequenceAssignmentHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdDocumentSequenceAssignmentHome.JNDI_NAME, LocalAdDocumentSequenceAssignmentHome.class);
			adBranchDocumentSequenceAssignmentHome = (LocalAdBranchDocumentSequenceAssignmentHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdBranchDocumentSequenceAssignmentHome.JNDI_NAME, LocalAdBranchDocumentSequenceAssignmentHome.class);

    	} catch (NamingException ex) {

    		throw new EJBException(ex.getMessage());

    	}

    	try{

    		// generate adj document number
    		String ADJ_DCMNT_NMBR = null;

    		LocalAdBranchDocumentSequenceAssignment adBranchDocumentSequenceAssignment = null;
    		LocalAdDocumentSequenceAssignment adDocumentSequenceAssignment = null;

    		try {

    			adDocumentSequenceAssignment = adDocumentSequenceAssignmentHome.findByDcName("INV ADJUSTMENT", AD_CMPNY);

    		} catch (FinderException ex) {

    		}

    		try {

    			adBranchDocumentSequenceAssignment = adBranchDocumentSequenceAssignmentHome.findBdsByDsaCodeAndBrCode(
    					adDocumentSequenceAssignment.getDsaCode(), AD_BRNCH, AD_CMPNY);

    		} catch (FinderException ex) {

    		}

    		while (true) {

    			if (adBranchDocumentSequenceAssignment == null || adBranchDocumentSequenceAssignment.getBdsNextSequence() == null) {

    				try {

    					invAdjustmentHome.findByAdjDocumentNumberAndBrCode(adDocumentSequenceAssignment.getDsaNextSequence(),
    							AD_BRNCH, AD_CMPNY);
    					adDocumentSequenceAssignment.setDsaNextSequence(EJBCommon.incrementStringNumber(
    							adDocumentSequenceAssignment.getDsaNextSequence()));

    				} catch (FinderException ex) {

    					ADJ_DCMNT_NMBR = adDocumentSequenceAssignment.getDsaNextSequence();
    					adDocumentSequenceAssignment.setDsaNextSequence(EJBCommon.incrementStringNumber(
    							adDocumentSequenceAssignment.getDsaNextSequence()));
    					break;

    				}

    			} else {

    				try {

    					invAdjustmentHome.findByAdjDocumentNumberAndBrCode(
    							adBranchDocumentSequenceAssignment.getBdsNextSequence(), AD_BRNCH, AD_CMPNY);
    					adBranchDocumentSequenceAssignment.setBdsNextSequence(EJBCommon.incrementStringNumber(
    							adBranchDocumentSequenceAssignment.getBdsNextSequence()));

    				} catch (FinderException ex) {

    					ADJ_DCMNT_NMBR = adBranchDocumentSequenceAssignment.getBdsNextSequence();
    					adBranchDocumentSequenceAssignment.setBdsNextSequence(EJBCommon.incrementStringNumber(
    							adBranchDocumentSequenceAssignment.getBdsNextSequence()));
    					break;

    				}

    			}

    		}

    		LocalInvAdjustment invAdjustment = invAdjustmentHome.create(ADJ_DCMNT_NMBR, ADJ_RFRNC_NMBR,
    				ADJ_DSCRPTN, ADJ_DATE, "COST-VARIANCE", "N/A", EJBCommon.FALSE, USR_NM, ADJ_DATE, USR_NM, ADJ_DATE, null, null,
					USR_NM, ADJ_DATE, null, null, EJBCommon.TRUE, EJBCommon.FALSE, AD_BRNCH, AD_CMPNY);

    		return invAdjustment;

    	} catch (Exception ex) {

    		Debug.printStackTrace(ex);
    		getSessionContext().setRollbackOnly();
    		throw new EJBException(ex.getMessage());

    	}


    }

    private void postInvAdjustmentToInventory(LocalInvAdjustmentLine invAdjustmentLine, Date CST_DT, double CST_ADJST_QTY,
    		double CST_ADJST_CST, double CST_RMNNG_QTY, double CST_RMNNG_VL, Integer AD_BRNCH,Integer AD_CMPNY) {

    	Debug.print("ApVoucherEntryController postInvAdjustmentToInventory");

    	LocalInvCostingHome invCostingHome = null;
    	LocalAdPreferenceHome adPreferenceHome = null;
    	LocalAdCompanyHome adCompanyHome = null;

    	// Initialize EJB Home

    	try {

    		invCostingHome = (LocalInvCostingHome)EJBHomeFactory.
			lookUpLocalHome(LocalInvCostingHome.JNDI_NAME, LocalInvCostingHome.class);
    		adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);
    		adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);

    	} catch (NamingException ex) {

    		getSessionContext().setRollbackOnly();
    		throw new EJBException(ex.getMessage());

    	}

    	try {

    		LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);
    		LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
    		LocalInvItemLocation invItemLocation = invAdjustmentLine.getInvItemLocation();
    		int CST_LN_NMBR = 0;

    		CST_ADJST_QTY = EJBCommon.roundIt(CST_ADJST_QTY, adPreference.getPrfInvQuantityPrecisionUnit());
    		CST_ADJST_CST = EJBCommon.roundIt(CST_ADJST_CST, adCompany.getGlFunctionalCurrency().getFcPrecision());
    		CST_RMNNG_QTY = EJBCommon.roundIt(CST_RMNNG_QTY, adPreference.getPrfInvQuantityPrecisionUnit());
    		CST_RMNNG_VL = EJBCommon.roundIt(CST_RMNNG_VL, adCompany.getGlFunctionalCurrency().getFcPrecision());

    		if (CST_ADJST_QTY < 0) {

    			invItemLocation.setIlCommittedQuantity(invItemLocation.getIlCommittedQuantity() - Math.abs(CST_ADJST_QTY));

    		}

    		// create costing

    		try {

    			// generate line number

    			LocalInvCosting invCurrentCosting = invCostingHome.getByMaxCstLineNumberAndCstDateToLongAndIiNameAndLocName(
    					CST_DT.getTime(), invItemLocation.getInvItem().getIiName(), invItemLocation.getInvLocation().getLocName(),
						AD_BRNCH, AD_CMPNY);
    			CST_LN_NMBR = invCurrentCosting.getCstLineNumber() + 1;

    		} catch (FinderException ex) {

    			CST_LN_NMBR = 1;

    		}

    		LocalInvCosting invCosting = invCostingHome.create(CST_DT, CST_DT.getTime(), CST_LN_NMBR, 0d, 0d, 0d, 0d,
    				CST_ADJST_QTY, CST_ADJST_CST, 0d, 0d, CST_RMNNG_QTY, CST_RMNNG_VL, 0d, 0d, CST_ADJST_QTY > 0 ? CST_ADJST_QTY : 0, AD_BRNCH, AD_CMPNY);
    		//invItemLocation.addInvCosting(invCosting);
    		invCosting.setInvItemLocation(invItemLocation);
    		invCosting.setInvAdjustmentLine(invAdjustmentLine);

    		// propagate balance if necessary

    		Collection invCostings = invCostingHome.findByGreaterThanCstDateAndIiNameAndLocName(CST_DT, invItemLocation.getInvItem().getIiName(), invItemLocation.getInvLocation().getLocName(), AD_BRNCH, AD_CMPNY);

    		Iterator i = invCostings.iterator();

    		while (i.hasNext()) {

    			LocalInvCosting invPropagatedCosting = (LocalInvCosting)i.next();

    			invPropagatedCosting.setCstRemainingQuantity(invPropagatedCosting.getCstRemainingQuantity() + CST_ADJST_QTY);
    			invPropagatedCosting.setCstRemainingValue(invPropagatedCosting.getCstRemainingValue() + CST_ADJST_CST);

    		}


    	} catch (Exception ex) {

    		Debug.printStackTrace(ex);
    		getSessionContext().setRollbackOnly();
    		throw new EJBException(ex.getMessage());

    	}



    }

	// SessionBean methods

    /**
     * @ejb:create-method view-type="remote"
     **/
    public void ejbCreate() throws CreateException {

       Debug.print("ApDebitMemoEntryControllerBean ejbCreate");

    }

}