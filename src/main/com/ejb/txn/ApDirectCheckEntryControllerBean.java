
/*
 * ApDirectCheckControllerBean.java
 *
 * Created on February 23, 2004, 5:30 PM
 *
 * @author  Neil Andrew M. Ajero
 */

package com.ejb.txn;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;
import javax.naming.NamingException;




import com.ejb.ad.LocalAdAmountLimit;
import com.ejb.ad.LocalAdAmountLimitHome;
import com.ejb.ad.LocalAdApproval;
import com.ejb.ad.LocalAdApprovalHome;
import com.ejb.ad.LocalAdApprovalQueue;
import com.ejb.ad.LocalAdApprovalQueueHome;
import com.ejb.ad.LocalAdApprovalUser;
import com.ejb.ad.LocalAdApprovalUserHome;
import com.ejb.ad.LocalAdBankAccount;
import com.ejb.ad.LocalAdBankAccountBalance;
import com.ejb.ad.LocalAdBankAccountBalanceHome;
import com.ejb.ad.LocalAdBankAccountHome;
import com.ejb.ad.LocalAdBranchApTaxCode;
import com.ejb.ad.LocalAdBranchApTaxCodeHome;
import com.ejb.ad.LocalAdBranchBankAccount;
import com.ejb.ad.LocalAdBranchBankAccountHome;
import com.ejb.ad.LocalAdBranchDocumentSequenceAssignment;
import com.ejb.ad.LocalAdBranchDocumentSequenceAssignmentHome;
import com.ejb.ad.LocalAdBranchItemLocation;
import com.ejb.ad.LocalAdBranchItemLocationHome;
import com.ejb.ad.LocalAdBranchSupplier;
import com.ejb.ad.LocalAdBranchSupplierHome;
import com.ejb.ad.LocalAdCompany;
import com.ejb.ad.LocalAdCompanyHome;
import com.ejb.ad.LocalAdDeleteAuditTrailHome;
import com.ejb.ad.LocalAdLookUpValue;
import com.ejb.ad.LocalAdLookUpValueHome;
import com.ejb.ad.LocalAdPaymentTerm;
import com.ejb.ad.LocalAdPaymentTermHome;
import com.ejb.ad.LocalAdPreference;
import com.ejb.ad.LocalAdPreferenceHome;
import com.ejb.ad.LocalAdUser;
import com.ejb.ad.LocalAdUserHome;
import com.ejb.ap.LocalApAppliedVoucher;
import com.ejb.ap.LocalApCheck;
import com.ejb.ap.LocalApCheckBatch;
import com.ejb.ap.LocalApCheckBatchHome;
import com.ejb.ap.LocalApCheckHome;
import com.ejb.ap.LocalApDistributionRecord;
import com.ejb.ap.LocalApDistributionRecordHome;
import com.ejb.ap.LocalApSupplier;
import com.ejb.ap.LocalApSupplierBalance;
import com.ejb.ap.LocalApSupplierBalanceHome;
import com.ejb.ap.LocalApSupplierHome;
import com.ejb.ap.LocalApTaxCode;
import com.ejb.ap.LocalApTaxCodeHome;
import com.ejb.ap.LocalApVoucher;
import com.ejb.ap.LocalApVoucherLineItem;
import com.ejb.ap.LocalApVoucherLineItemHome;
import com.ejb.ap.LocalApVoucherPaymentSchedule;
import com.ejb.ap.LocalApWithholdingTaxCode;
import com.ejb.ap.LocalApWithholdingTaxCodeHome;
import com.ejb.exception.AdPRFCoaGlVarianceAccountNotFoundException;
import com.ejb.exception.ApCHKCheckNumberNotUniqueException;
import com.ejb.exception.GlJREffectiveDateNoPeriodExistException;
import com.ejb.exception.GlJREffectiveDatePeriodClosedException;
import com.ejb.exception.GlobalBranchAccountNumberInvalidException;
import com.ejb.exception.GlobalConversionDateNotExistException;
import com.ejb.exception.GlobalDocumentNumberNotUniqueException;
import com.ejb.exception.GlobalInvItemLocationNotFoundException;
import com.ejb.exception.GlobalInventoryDateException;
import com.ejb.exception.GlobalJournalNotBalanceException;
import com.ejb.exception.GlobalNoApprovalApproverFoundException;
import com.ejb.exception.GlobalNoApprovalRequesterFoundException;
import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.exception.GlobalRecordAlreadyDeletedException;
import com.ejb.exception.GlobalTransactionAlreadyApprovedException;
import com.ejb.exception.GlobalTransactionAlreadyPendingException;
import com.ejb.exception.GlobalTransactionAlreadyPostedException;
import com.ejb.exception.GlobalTransactionAlreadyVoidException;
import com.ejb.exception.GlobalTransactionAlreadyVoidPostedException;
import com.ejb.genfld.LocalGenSegmentHome;
import com.ejb.genfld.LocalGenValueSetValueHome;
import com.ejb.gl.LocalAdDocumentSequenceAssignment;
import com.ejb.gl.LocalAdDocumentSequenceAssignmentHome;
import com.ejb.gl.LocalGlAccountingCalendarValue;
import com.ejb.gl.LocalGlAccountingCalendarValueHome;
import com.ejb.gl.LocalGlChartOfAccount;
import com.ejb.gl.LocalGlChartOfAccountBalance;
import com.ejb.gl.LocalGlChartOfAccountBalanceHome;
import com.ejb.gl.LocalGlChartOfAccountHome;
import com.ejb.gl.LocalGlForexLedger;
import com.ejb.gl.LocalGlForexLedgerHome;
import com.ejb.gl.LocalGlFunctionalCurrency;
import com.ejb.gl.LocalGlFunctionalCurrencyHome;
import com.ejb.gl.LocalGlFunctionalCurrencyRate;
import com.ejb.gl.LocalGlFunctionalCurrencyRateHome;
import com.ejb.gl.LocalGlInvestorAccountBalance;
import com.ejb.gl.LocalGlInvestorAccountBalanceHome;
import com.ejb.gl.LocalGlJournal;
import com.ejb.gl.LocalGlJournalBatch;
import com.ejb.gl.LocalGlJournalBatchHome;
import com.ejb.gl.LocalGlJournalCategory;
import com.ejb.gl.LocalGlJournalCategoryHome;
import com.ejb.gl.LocalGlJournalHome;
import com.ejb.gl.LocalGlJournalLine;
import com.ejb.gl.LocalGlJournalLineHome;
import com.ejb.gl.LocalGlJournalSource;
import com.ejb.gl.LocalGlJournalSourceHome;
import com.ejb.gl.LocalGlSetOfBook;
import com.ejb.gl.LocalGlSetOfBookHome;
import com.ejb.gl.LocalGlSuspenseAccount;
import com.ejb.gl.LocalGlSuspenseAccountHome;
import com.ejb.inv.LocalInvAdjustment;
import com.ejb.inv.LocalInvAdjustmentHome;
import com.ejb.inv.LocalInvAdjustmentLine;
import com.ejb.inv.LocalInvAdjustmentLineHome;
import com.ejb.inv.LocalInvCosting;
import com.ejb.inv.LocalInvCostingHome;
import com.ejb.inv.LocalInvDistributionRecord;
import com.ejb.inv.LocalInvDistributionRecordHome;
import com.ejb.inv.LocalInvItem;
import com.ejb.inv.LocalInvItemHome;
import com.ejb.inv.LocalInvItemLocation;
import com.ejb.inv.LocalInvItemLocationHome;
import com.ejb.inv.LocalInvLineItem;
import com.ejb.inv.LocalInvLineItemTemplate;
import com.ejb.inv.LocalInvLineItemTemplateHome;
import com.ejb.inv.LocalInvLocation;
import com.ejb.inv.LocalInvLocationHome;
import com.ejb.inv.LocalInvTag;
import com.ejb.inv.LocalInvTagHome;
import com.ejb.inv.LocalInvUnitOfMeasure;
import com.ejb.inv.LocalInvUnitOfMeasureConversion;
import com.ejb.inv.LocalInvUnitOfMeasureConversionHome;
import com.ejb.inv.LocalInvUnitOfMeasureHome;

import com.util.AbstractSessionBean;
import com.util.ApModCheckDetails;
import com.util.ApModDistributionRecordDetails;
import com.util.ApModSupplierDetails;
import com.util.ApModVoucherLineItemDetails;
import com.util.ApTaxCodeDetails;
import com.util.Debug;
import com.util.EJBCommon;
import com.util.EJBHomeFactory;
import com.util.GlModFunctionalCurrencyDetails;
import com.util.InvModLineItemDetails;
import com.util.InvModTagListDetails;
import com.util.InvModUnitOfMeasureDetails;

/**
 * @ejb:bean name="ApDirectCheckEntryControllerEJB"
 *           display-name="used for entering direct checks"
 *           type="Stateless"
 *           view-type="remote"
 *           jndi-name="ejb/ApDirectCheckEntryControllerEJB"
 *
 * @ejb:interface remote-class="com.ejb.txn.ApDirectCheckEntryController"
 *                extends="javax.ejb.EJBObject"
 *
 * @ejb:home remote-class="com.ejb.txn.ApDirectCheckEntryControllerHome"
 *           extends="javax.ejb.EJBHome"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="apuser"
 *                        role-link="apuserlink"
 *
 * @ejb:permission role-name="apuser"
 *
 */

public class ApDirectCheckEntryControllerBean extends AbstractSessionBean {

	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public ArrayList getGlFcAllWithDefault(Integer AD_CMPNY) {

		Debug.print("ApDirectCheckEntryControllerBean getGlFcAllWithDefault");

		LocalGlFunctionalCurrencyHome glFunctionalCurrencyHome = null;
		LocalAdCompanyHome adCompanyHome = null;

		Collection glFunctionalCurrencies = null;

		LocalGlFunctionalCurrency glFunctionalCurrency = null;
		LocalAdCompany adCompany = null;


		ArrayList list = new ArrayList();

		// Initialize EJB Home

		try {

			glFunctionalCurrencyHome = (LocalGlFunctionalCurrencyHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlFunctionalCurrencyHome.JNDI_NAME, LocalGlFunctionalCurrencyHome.class);
			adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}

		try {

			adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);

			glFunctionalCurrencies = glFunctionalCurrencyHome.findFcAllEnabled(
					EJBCommon.getGcCurrentDateWoTime().getTime(), AD_CMPNY);

		} catch (Exception ex) {

			throw new EJBException(ex.getMessage());
		}

		if (glFunctionalCurrencies.isEmpty()) {

			return null;

		}

		Iterator i = glFunctionalCurrencies.iterator();

		while (i.hasNext()) {

			glFunctionalCurrency = (LocalGlFunctionalCurrency)i.next();

			GlModFunctionalCurrencyDetails mdetails = new GlModFunctionalCurrencyDetails(
					glFunctionalCurrency.getFcCode(), glFunctionalCurrency.getFcName(),
					adCompany.getGlFunctionalCurrency().getFcName().equals(glFunctionalCurrency.getFcName()) ?
							EJBCommon.TRUE : EJBCommon.FALSE);

			list.add(mdetails);

		}

		return list;

	}


	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public ArrayList getAdPytAll(Integer AD_CMPNY) {

		Debug.print("ApDirectCheckEntryControllerBean getAdPytAll");

		LocalAdPaymentTermHome adPaymentTermHome = null;

		ArrayList list = new ArrayList();

		// Initialize EJB Home

		try {

			adPaymentTermHome = (LocalAdPaymentTermHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdPaymentTermHome.JNDI_NAME, LocalAdPaymentTermHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}

		try {

			Collection adPaymentTerms = adPaymentTermHome.findEnabledPytAll(AD_CMPNY);

			Iterator i = adPaymentTerms.iterator();

			while (i.hasNext()) {

				LocalAdPaymentTerm adPaymentTerm = (LocalAdPaymentTerm)i.next();

				list.add(adPaymentTerm.getPytName());

			}

			return list;

		} catch (Exception ex) {

			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());

		}

	}

	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public ArrayList getAdBaAll(Integer AD_BRNCH, Integer AD_CMPNY) {

		Debug.print("ApDirectCheckEntryControllerBean getAdBaAll");

		LocalAdBankAccountHome adBankAccountHome = null;

		ArrayList list = new ArrayList();

		// Initialize EJB Home

		try {

			adBankAccountHome = (LocalAdBankAccountHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdBankAccountHome.JNDI_NAME, LocalAdBankAccountHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}

		try {

			Collection adBankAccounts = adBankAccountHome.findEnabledBaAll(AD_BRNCH, AD_CMPNY);

			Iterator i = adBankAccounts.iterator();

			while (i.hasNext()) {

				LocalAdBankAccount adBankAccount = (LocalAdBankAccount)i.next();

				list.add(adBankAccount.getBaName());

			}

			return list;

		} catch (Exception ex) {

			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());

		}

	}


	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public ArrayList getApTcAll(Integer AD_CMPNY) {

		Debug.print("ApDirectCheckEntryControllerBean getApTcAll");

		LocalApTaxCodeHome apTaxCodeHome = null;

		ArrayList list = new ArrayList();

		// Initialize EJB Home

		try {

			apTaxCodeHome = (LocalApTaxCodeHome)EJBHomeFactory.
			lookUpLocalHome(LocalApTaxCodeHome.JNDI_NAME, LocalApTaxCodeHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}

		try {

			Collection apTaxCodes = apTaxCodeHome.findEnabledTcAll(AD_CMPNY);

			Iterator i = apTaxCodes.iterator();

			while (i.hasNext()) {

				LocalApTaxCode apTaxCode = (LocalApTaxCode)i.next();

				list.add(apTaxCode.getTcName());

			}

			return list;

		} catch (Exception ex) {

			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());

		}

	}

	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public ApTaxCodeDetails getApTcByTcName(String TC_NM, Integer AD_CMPNY) {

		Debug.print("ApPurchaseOrderEntryControllerBean getArTcByTcName");

		LocalApTaxCodeHome apTaxCodeHome = null;

		ArrayList list = new ArrayList();

		// Initialize EJB Home

		try {

			apTaxCodeHome = (LocalApTaxCodeHome)EJBHomeFactory.
			lookUpLocalHome(LocalApTaxCodeHome.JNDI_NAME, LocalApTaxCodeHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}

		try {

			LocalApTaxCode apTaxCode = apTaxCodeHome.findByTcName(TC_NM, AD_CMPNY);

			ApTaxCodeDetails details = new ApTaxCodeDetails();
			details.setTcType(apTaxCode.getTcType());
			details.setTcRate(apTaxCode.getTcRate());

			return details;

		} catch (Exception ex) {

			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());

		}

	}

	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public ArrayList getApWtcAll(Integer AD_CMPNY) {

		Debug.print("ApDirectCheckEntryControllerBean getApWtcAll");

		LocalApWithholdingTaxCodeHome apWithholdingTaxCodeHome = null;

		ArrayList list = new ArrayList();

		// Initialize EJB Home

		try {

			apWithholdingTaxCodeHome = (LocalApWithholdingTaxCodeHome)EJBHomeFactory.
			lookUpLocalHome(LocalApWithholdingTaxCodeHome.JNDI_NAME, LocalApWithholdingTaxCodeHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}

		try {

			Collection apWithholdingTaxCodes = apWithholdingTaxCodeHome.findEnabledWtcAll(AD_CMPNY);

			Iterator i = apWithholdingTaxCodes.iterator();

			while (i.hasNext()) {

				LocalApWithholdingTaxCode apWithholdingTaxCode = (LocalApWithholdingTaxCode)i.next();

				list.add(apWithholdingTaxCode.getWtcName());

			}

			return list;

		} catch (Exception ex) {

			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());

		}

	}

	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public ArrayList getApSplAll(Integer AD_BRNCH, Integer AD_CMPNY) {

		Debug.print("ApDirectCheckEntryControllerBean getApSplAll");

		LocalApSupplierHome apSupplierHome = null;

		ArrayList list = new ArrayList();

		// Initialize EJB Home

		try {

			apSupplierHome = (LocalApSupplierHome)EJBHomeFactory.
			lookUpLocalHome(LocalApSupplierHome.JNDI_NAME, LocalApSupplierHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}

		try {

			Collection apSuppliers = apSupplierHome.findEnabledSplAll(AD_BRNCH, AD_CMPNY);

			Iterator i = apSuppliers.iterator();

			while (i.hasNext()) {

				LocalApSupplier apSupplier = (LocalApSupplier)i.next();

				list.add(apSupplier.getSplSupplierCode());

			}

			return list;

		} catch (Exception ex) {

			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());

		}

	}


	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public short getAdPrfApJournalLineNumber(Integer AD_CMPNY) {

		Debug.print("ApDebitMemoEntryControllerBean getAdPrfApJournalLineNumber");

		LocalAdPreferenceHome adPreferenceHome = null;


		// Initialize EJB Home

		try {

			adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}


		try {

			LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);

			return adPreference.getPrfApJournalLineNumber();

		} catch (Exception ex) {

			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());

		}

	}

	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public ApModCheckDetails getApChkByChkCode(Integer CHK_CODE, Integer AD_CMPNY)
	throws GlobalNoRecordFoundException {

		Debug.print("ApDirectCheckEntryControllerBean getApChkByChkCode");

		LocalAdUserHome adUserHome = null;
		LocalApCheckHome apCheckHome = null;
		LocalAdCompanyHome adCompanyHome = null;
		LocalApDistributionRecordHome apDistributionRecordHome = null;
		LocalInvTagHome invTagHome = null;

		// Initialize EJB Home

		try {

			apCheckHome = (LocalApCheckHome)EJBHomeFactory.
			lookUpLocalHome(LocalApCheckHome.JNDI_NAME, LocalApCheckHome.class);
			adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
			apDistributionRecordHome = (LocalApDistributionRecordHome)EJBHomeFactory.
			lookUpLocalHome(LocalApDistributionRecordHome.JNDI_NAME, LocalApDistributionRecordHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}

		try {

			LocalApCheck apCheck = null;


			try {

				apCheck = apCheckHome.findByPrimaryKey(CHK_CODE);

			} catch (FinderException ex) {

				throw new GlobalNoRecordFoundException();

			}

			ArrayList list = new ArrayList();

			// get voucher line items if any

			Collection apVoucherLineItems = apCheck.getApVoucherLineItems();

			double TOTAL_DEBIT = 0d;
			double TOTAL_CREDIT = 0d;

			if (!apVoucherLineItems.isEmpty()) {

				Iterator i = apVoucherLineItems.iterator();

				while (i.hasNext()) {

					LocalApVoucherLineItem apVoucherLineItem = (LocalApVoucherLineItem) i.next();

					ApModVoucherLineItemDetails vliDetails = new ApModVoucherLineItemDetails();

					vliDetails.setVliCode(apVoucherLineItem.getVliCode());
					vliDetails.setVliLine(apVoucherLineItem.getVliLine());
					vliDetails.setVliQuantity(apVoucherLineItem.getVliQuantity());
					vliDetails.setVliUnitCost(apVoucherLineItem.getVliUnitCost());
					vliDetails.setVliIiName(apVoucherLineItem.getInvItemLocation().getInvItem().getIiName());
					vliDetails.setVliLocName(apVoucherLineItem.getInvItemLocation().getInvLocation().getLocName());
					vliDetails.setVliUomName(apVoucherLineItem.getInvUnitOfMeasure().getUomName());
					vliDetails.setVliIiDescription(apVoucherLineItem.getInvItemLocation().getInvItem().getIiDescription());
					vliDetails.setVliDiscount1(apVoucherLineItem.getVliDiscount1());
					vliDetails.setVliDiscount2(apVoucherLineItem.getVliDiscount2());
					vliDetails.setVliDiscount3(apVoucherLineItem.getVliDiscount3());
					vliDetails.setVliDiscount4(apVoucherLineItem.getVliDiscount4());
					vliDetails.setVliTotalDiscount(apVoucherLineItem.getVliTotalDiscount());
					vliDetails.setVliMisc(apVoucherLineItem.getVliMisc());
					if (apCheck.getApTaxCode().getTcType().equalsIgnoreCase("INCLUSIVE"))
						vliDetails.setVliAmount(apVoucherLineItem.getVliAmount() + apVoucherLineItem.getVliTaxAmount());
					else
						vliDetails.setVliAmount(apVoucherLineItem.getVliAmount());


					ArrayList tagList = new ArrayList();

					if(apVoucherLineItem.getInvItemLocation().getInvItem().getIiTraceMisc()==EJBCommon.TRUE) {


						tagList = this.getInvTagList(apVoucherLineItem);

		    			vliDetails.setTagList(tagList);
		    			vliDetails.setIsTraceMisc(true);

					}

					list.add(vliDetails);

				}

			} else {

				// get distribution records

				Collection apDistributionRecords = apDistributionRecordHome.findByChkCode(apCheck.getChkCode(), AD_CMPNY);

				short lineNumber = 1;

				Iterator i = apDistributionRecords.iterator();

				TOTAL_DEBIT = 0d;
				TOTAL_CREDIT = 0d;

				while (i.hasNext()) {

					LocalApDistributionRecord apDistributionRecord = (LocalApDistributionRecord)i.next();

					ApModDistributionRecordDetails mdetails = new ApModDistributionRecordDetails();

					mdetails.setDrCode(apDistributionRecord.getDrCode());
					mdetails.setDrLine(lineNumber);
					mdetails.setDrClass(apDistributionRecord.getDrClass());
					mdetails.setDrDebit(apDistributionRecord.getDrDebit());
					mdetails.setDrAmount(apDistributionRecord.getDrAmount());
					mdetails.setDrCoaAccountNumber(apDistributionRecord.getGlChartOfAccount().getCoaAccountNumber());
					mdetails.setDrCoaAccountDescription(apDistributionRecord.getGlChartOfAccount().getCoaAccountDescription());

					if(apDistributionRecord.getDrDebit() == EJBCommon.TRUE) {

						TOTAL_DEBIT += apDistributionRecord.getDrAmount();

					} else {

						TOTAL_CREDIT += apDistributionRecord.getDrAmount();

					}

					list.add(mdetails);

					lineNumber++;

				}

			}

			ApModCheckDetails mChkDetails = new ApModCheckDetails();

			mChkDetails.setChkCode(apCheck.getChkCode());
			mChkDetails.setChkDate(apCheck.getChkDate());
			mChkDetails.setChkCheckDate(apCheck.getChkCheckDate());
			mChkDetails.setChkNumber(apCheck.getChkNumber());
			mChkDetails.setChkDocumentNumber(apCheck.getChkDocumentNumber());
			mChkDetails.setChkReferenceNumber(apCheck.getChkReferenceNumber());
            mChkDetails.setChkInfoType(apCheck.getChkInfoType());
            mChkDetails.setChkInfoBioNumber(apCheck.getChkInfoBioNumber());
            mChkDetails.setChkInfoBioDescription(apCheck.getChkInfoBioDescription());
            mChkDetails.setChkInfoTypeStatus(apCheck.getChkInfoTypeStatus());
            mChkDetails.setChkInfoRequestStatus(apCheck.getChkInfoRequestStatus());
			mChkDetails.setChkBillAmount(apCheck.getChkBillAmount());
			mChkDetails.setChkAmount(apCheck.getChkAmount());
			mChkDetails.setChkVoid(apCheck.getChkVoid());
			mChkDetails.setChkCrossCheck(apCheck.getChkCrossCheck());
			mChkDetails.setChkTotalDebit(TOTAL_DEBIT);
			mChkDetails.setChkTotalCredit(TOTAL_CREDIT);
			mChkDetails.setChkDescription(apCheck.getChkDescription());
                mChkDetails.setChkInfoType(apCheck.getChkInfoType());
                mChkDetails.setChkInfoBioNumber(apCheck.getChkInfoBioNumber());
                mChkDetails.setChkInfoBioDescription(apCheck.getChkInfoBioDescription());
                mChkDetails.setChkInfoTypeStatus(apCheck.getChkInfoTypeStatus());
                mChkDetails.setChkInfoRequestStatus(apCheck.getChkInfoRequestStatus());
			mChkDetails.setChkConversionDate(apCheck.getChkConversionDate());
			mChkDetails.setChkConversionRate(apCheck.getChkConversionRate());
			mChkDetails.setChkApprovalStatus(apCheck.getChkApprovalStatus());
			mChkDetails.setChkReasonForRejection(apCheck.getChkReasonForRejection());
			mChkDetails.setChkPosted(apCheck.getChkPosted());
			mChkDetails.setChkVoid(apCheck.getChkVoid());
			mChkDetails.setChkVoidApprovalStatus(apCheck.getChkVoidApprovalStatus());
			mChkDetails.setChkVoidPosted(apCheck.getChkVoidPosted());
			mChkDetails.setChkCreatedBy(apCheck.getChkCreatedBy());
			mChkDetails.setChkDateCreated(apCheck.getChkDateCreated());
			mChkDetails.setChkLastModifiedBy(apCheck.getChkLastModifiedBy());
			mChkDetails.setChkDateLastModified(apCheck.getChkDateLastModified());
			mChkDetails.setChkApprovedRejectedBy(apCheck.getChkApprovedRejectedBy());
			mChkDetails.setChkDateApprovedRejected(apCheck.getChkDateApprovedRejected());
			mChkDetails.setChkPostedBy(apCheck.getChkPostedBy());
			mChkDetails.setChkDatePosted(apCheck.getChkDatePosted());
			mChkDetails.setChkFcName(apCheck.getGlFunctionalCurrency().getFcName());
			mChkDetails.setChkSplSupplierCode(apCheck.getApSupplier().getSplSupplierCode());
			mChkDetails.setChkBaName(apCheck.getAdBankAccount().getBaName());
			mChkDetails.setChkTcName(apCheck.getApTaxCode().getTcName());
			mChkDetails.setChkTcType(apCheck.getApTaxCode().getTcType());
			mChkDetails.setChkTcRate(apCheck.getApTaxCode().getTcRate());
			mChkDetails.setChkWtcName(apCheck.getApWithholdingTaxCode().getWtcName());
			mChkDetails.setChkCbName(apCheck.getApCheckBatch() != null ? apCheck.getApCheckBatch().getCbName() : null);
			mChkDetails.setChkSplName(apCheck.getChkSupplierName() == null ? apCheck.getApSupplier().getSplName() : apCheck.getChkSupplierName());
			mChkDetails.setChkMemo(apCheck.getChkMemo());

			mChkDetails.setChkInvtInscribedStock(apCheck.getChkInvtInscribedStock());
			mChkDetails.setChkInvtTreasuryBill(apCheck.getChkInvtTreasuryBill());
			mChkDetails.setChkInvtNextRunDate(apCheck.getChkInvtNextRunDate());
			mChkDetails.setChkInvtSettlementDate(apCheck.getChkInvtSettlementDate());
			mChkDetails.setChkInvtMaturityDate(apCheck.getChkInvtMaturityDate());
			mChkDetails.setChkInvtBidYield(apCheck.getChkInvtBidYield());
			mChkDetails.setChkInvtCouponRate(apCheck.getChkInvtCouponRate());
			mChkDetails.setChkInvtSettleAmount(apCheck.getChkInvtSettleAmount());
			mChkDetails.setChkInvtFaceValue(apCheck.getChkInvtFaceValue());
			mChkDetails.setChkInvtPremiumAmount(apCheck.getChkInvtPremiumAmount());

			mChkDetails.setChkSupplierClassName(apCheck.getApSupplier().getApSupplierClass().getScName());
			mChkDetails.setChkScInvestment(apCheck.getApSupplier().getApSupplierClass().getScIsInvestment());


			mChkDetails.setChkPytName(apCheck.getAdPaymentTerm() != null ? apCheck.getAdPaymentTerm().getPytName() : null);

			if (!apVoucherLineItems.isEmpty()) {

				mChkDetails.setChkVliList(list);

			} else {

				mChkDetails.setChkDrList(list);

			}

			return mChkDetails;

		} catch (GlobalNoRecordFoundException ex) {

			throw ex;

		} catch (Exception ex) {

			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());

		}

	}

	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public ApModSupplierDetails getApSplBySplSupplierCode(String SPL_SPPLR_CODE, Integer AD_CMPNY)
	throws GlobalNoRecordFoundException {

		Debug.print("ApDirectCheckEntryControllerBean getApSplBySplSupplierCode");

		LocalApSupplierHome apSupplierHome = null;

		// Initialize EJB Home

		try {

			apSupplierHome = (LocalApSupplierHome)EJBHomeFactory.
			lookUpLocalHome(LocalApSupplierHome.JNDI_NAME, LocalApSupplierHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}

		try {

			LocalApSupplier apSupplier = null;


			try {

				apSupplier = apSupplierHome.findBySplSupplierCode(SPL_SPPLR_CODE, AD_CMPNY);

			} catch (FinderException ex) {

				throw new GlobalNoRecordFoundException();

			}

			ApModSupplierDetails mdetails = new ApModSupplierDetails();

			mdetails.setSplStBaName(apSupplier.getAdBankAccount().getBaCode() != null ?
					apSupplier.getAdBankAccount().getBaName() : null);
			mdetails.setSplScWtcName(apSupplier.getApSupplierClass().getApWithholdingTaxCode() != null ?
					apSupplier.getApSupplierClass().getApWithholdingTaxCode().getWtcName() : null);
			mdetails.setSplName(apSupplier.getSplName());

            mdetails.setSplSupplierClass(apSupplier.getApSupplierClass().getScName());

            mdetails.setSplScInvestment(apSupplier.getApSupplierClass().getScIsInvestment());
            mdetails.setSplScLoan(apSupplier.getApSupplierClass().getScIsLoan());

        	if (apSupplier.getApSupplierClass().getApTaxCode() != null ){

        		mdetails.setSplScTcName( apSupplier.getApSupplierClass().getApTaxCode().getTcName());
        		mdetails.setSplScTcType( apSupplier.getApSupplierClass().getApTaxCode().getTcType());
        		mdetails.setSplScTcRate( apSupplier.getApSupplierClass().getApTaxCode().getTcRate());

        	}




        	if(apSupplier.getInvLineItemTemplate() != null) {
        		mdetails.setSplLitName(apSupplier.getInvLineItemTemplate().getLitName());
        	}

        	return mdetails;

		} catch (GlobalNoRecordFoundException ex) {

			throw ex;

		} catch (Exception ex) {

			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());

		}

	}


	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public ArrayList getApDrBySplSupplierCodeAndTcNameAndWtcNameAndChkBillAmountAndBaName(String SPL_SPPLR_CODE, String TC_NM,
			String WTC_NM, double CHK_BLL_AMNT, String BA_NM, Integer AD_BRNCH, Integer AD_CMPNY)
	throws GlobalNoRecordFoundException {

		Debug.print("ApDirectCheckEntryControllerBean getApDrBySplSupplierCodeAndTcNameAndWtcNameAndChkBillAmountAndBaName");

		LocalApSupplierHome apSupplierHome = null;
		LocalApTaxCodeHome apTaxCodeHome = null;
		LocalApWithholdingTaxCodeHome apWithholdingTaxCodeHome = null;
		LocalGlChartOfAccountHome glChartOfAccountHome = null;
		LocalAdCompanyHome adCompanyHome = null;
		LocalGenSegmentHome genSegmentHome = null;
		LocalGenValueSetValueHome genValueSetValueHome = null;
		LocalAdBankAccountHome adBankAccountHome = null;
		LocalAdBranchSupplierHome adBranchSupplierHome = null;
		LocalAdBranchApTaxCodeHome adBranchApTaxCodeHome = null;
		LocalAdBranchBankAccountHome adBranchBankAccountHome = null;


		ArrayList list = new ArrayList();

		System.out.println("------------");
		// Initialize EJB Home

		try {

			adBranchSupplierHome = (LocalAdBranchSupplierHome)EJBHomeFactory.
					lookUpLocalHome(LocalAdBranchSupplierHome.JNDI_NAME, LocalAdBranchSupplierHome.class);
			adBranchApTaxCodeHome = (LocalAdBranchApTaxCodeHome)EJBHomeFactory.
					lookUpLocalHome(LocalAdBranchApTaxCodeHome.JNDI_NAME, LocalAdBranchApTaxCodeHome.class);
			adBranchBankAccountHome = (LocalAdBranchBankAccountHome)EJBHomeFactory.
					lookUpLocalHome(LocalAdBranchBankAccountHome.JNDI_NAME, LocalAdBranchBankAccountHome.class);


			apSupplierHome = (LocalApSupplierHome)EJBHomeFactory.
			lookUpLocalHome(LocalApSupplierHome.JNDI_NAME, LocalApSupplierHome.class);
			apTaxCodeHome = (LocalApTaxCodeHome)EJBHomeFactory.
			lookUpLocalHome(LocalApTaxCodeHome.JNDI_NAME, LocalApTaxCodeHome.class);
			apWithholdingTaxCodeHome = (LocalApWithholdingTaxCodeHome)EJBHomeFactory.
			lookUpLocalHome(LocalApWithholdingTaxCodeHome.JNDI_NAME, LocalApWithholdingTaxCodeHome.class);
			glChartOfAccountHome = (LocalGlChartOfAccountHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlChartOfAccountHome.JNDI_NAME, LocalGlChartOfAccountHome.class);
			adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
			genSegmentHome = (LocalGenSegmentHome)EJBHomeFactory.
			lookUpLocalHome(LocalGenSegmentHome.JNDI_NAME, LocalGenSegmentHome.class);
			adBankAccountHome = (LocalAdBankAccountHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdBankAccountHome.JNDI_NAME, LocalAdBankAccountHome.class);
			System.out.println("1------------");
		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}

		try {

			LocalApSupplier apSupplier = null;
			LocalApTaxCode apTaxCode = null;
			LocalApWithholdingTaxCode apWithholdingTaxCode = null;
			System.out.println("2------------");

			try {
				System.out.println("SPL_SPPLR_CODE="+SPL_SPPLR_CODE);
				System.out.println("AD_CMPNY="+AD_CMPNY);
				apSupplier = apSupplierHome.findBySplSupplierCode(SPL_SPPLR_CODE, AD_CMPNY);
				System.out.println("1");
				apTaxCode = apTaxCodeHome.findByTcName(TC_NM, AD_CMPNY);
				System.out.println("2");
				apWithholdingTaxCode = apWithholdingTaxCodeHome.findByWtcName(WTC_NM, AD_CMPNY);
				System.out.println("3");

			} catch (FinderException ex) {

				throw new GlobalNoRecordFoundException();

			}

			double NET_AMOUNT = 0d;
			double TAX_AMOUNT = 0d;
			double W_TAX_AMOUNT = 0d;
			short LINE_NUMBER = 0;

			// create dr net expense
			System.out.println("4");
			if (apTaxCode.getTcType().equals("INCLUSIVE")) {

				NET_AMOUNT = EJBCommon.roundIt(CHK_BLL_AMNT / (1 + (apTaxCode.getTcRate() / 100)), this.getGlFcPrecisionUnit(AD_CMPNY));

			} else {

				// tax exclusive, none, zero rated or exempt

				NET_AMOUNT = CHK_BLL_AMNT;

			}

			ApModDistributionRecordDetails mdetails = new ApModDistributionRecordDetails();
			mdetails.setDrLine(++LINE_NUMBER);
			mdetails.setDrClass("EXPENSE");
			mdetails.setDrDebit(EJBCommon.TRUE);
			mdetails.setDrAmount(NET_AMOUNT);
			System.out.println("5");




			 LocalAdBranchSupplier adBranchSupplier = null;

            try {

            	adBranchSupplier = adBranchSupplierHome.findBSplBySplCodeAndBrCode(apSupplier.getSplCode(), AD_BRNCH, AD_CMPNY);

            } catch(FinderException ex) {

            }

			LocalGlChartOfAccount glChartOfAccount = glChartOfAccountHome.findByPrimaryKey(adBranchSupplier.getBsplGlCoaExpenseAccount());
			System.out.println("6");
			mdetails.setDrCoaAccountNumber(glChartOfAccount.getCoaAccountNumber());
			mdetails.setDrCoaAccountDescription(glChartOfAccount.getCoaAccountDescription());

			list.add(mdetails);

			// create tax line if necessary

			if (!apTaxCode.getTcType().equals("NONE") &&
					!apTaxCode.getTcType().equals("EXEMPT")) {

				System.out.println("7");
				if (apTaxCode.getTcType().equals("INCLUSIVE")) {

					TAX_AMOUNT = EJBCommon.roundIt(CHK_BLL_AMNT - NET_AMOUNT, this.getGlFcPrecisionUnit(AD_CMPNY));

				} else if (apTaxCode.getTcType().equals("EXCLUSIVE")) {

					TAX_AMOUNT = EJBCommon.roundIt(CHK_BLL_AMNT * apTaxCode.getTcRate() / 100, this.getGlFcPrecisionUnit(AD_CMPNY));

				} else {

					// tax none zero-rated or exempt

				}

				mdetails = new ApModDistributionRecordDetails();
				mdetails.setDrLine(++LINE_NUMBER);
				mdetails.setDrClass("TAX");
				mdetails.setDrDebit(EJBCommon.TRUE);
				mdetails.setDrAmount(TAX_AMOUNT);


				LocalAdBranchApTaxCode adBranchTaxCode = null;

                 try {

                 	adBranchTaxCode = adBranchApTaxCodeHome.findBtcByTcCodeAndBrCode(apTaxCode.getTcCode(), AD_BRNCH, AD_CMPNY);

                 } catch(FinderException ex) {

		         }
				if(adBranchTaxCode != null){
					glChartOfAccount = glChartOfAccountHome.findByPrimaryKey(adBranchTaxCode.getBtcGlCoaTaxCode());

					mdetails.setDrCoaAccountNumber(glChartOfAccount.getCoaAccountNumber());
					mdetails.setDrCoaAccountDescription(glChartOfAccount.getCoaAccountDescription());


				} else {
					mdetails.setDrCoaAccountNumber(apTaxCode.getGlChartOfAccount().getCoaAccountNumber());
					mdetails.setDrCoaAccountDescription(apTaxCode.getGlChartOfAccount().getCoaAccountDescription());


				}
                 System.out.println("8");
				list.add(mdetails);

			}

			// create withholding tax if necessary

			if (apWithholdingTaxCode.getWtcRate() != 0) {
				System.out.println("9");
				W_TAX_AMOUNT = EJBCommon.roundIt(NET_AMOUNT * (apWithholdingTaxCode.getWtcRate() / 100), this.getGlFcPrecisionUnit(AD_CMPNY));

				mdetails = new ApModDistributionRecordDetails();
				mdetails.setDrLine(++LINE_NUMBER);
				mdetails.setDrClass("W-TAX");
				mdetails.setDrDebit(EJBCommon.FALSE);
				mdetails.setDrAmount(W_TAX_AMOUNT);

				mdetails.setDrCoaAccountNumber(apWithholdingTaxCode.getGlChartOfAccount().getCoaAccountNumber());
				mdetails.setDrCoaAccountDescription(apWithholdingTaxCode.getGlChartOfAccount().getCoaAccountDescription());

				list.add(mdetails);
				System.out.println("10");
			}

			// create cash account

			mdetails = new ApModDistributionRecordDetails();
			mdetails.setDrLine(++LINE_NUMBER);
			mdetails.setDrClass("CASH");
			mdetails.setDrDebit(EJBCommon.FALSE);
			mdetails.setDrAmount(NET_AMOUNT + TAX_AMOUNT - W_TAX_AMOUNT);



			LocalAdBankAccount adBankAccount = adBankAccountHome.findByBaName(BA_NM, AD_CMPNY);

			// add branch bank account
	      	  LocalAdBranchBankAccount adBranchBankAccount = null;

              try {
                  adBranchBankAccount = adBranchBankAccountHome.findBbaByBaCodeAndBrCode(adBankAccount.getBaCode(), AD_BRNCH, AD_CMPNY);

              } catch(FinderException ex) {

              }

			LocalGlChartOfAccount glChartOfAccountBA = glChartOfAccountHome.findByPrimaryKey(adBranchBankAccount.getBbaGlCoaCashAccount());

			mdetails.setDrCoaAccountNumber(glChartOfAccountBA.getCoaAccountNumber());
			mdetails.setDrCoaAccountDescription(glChartOfAccountBA.getCoaAccountDescription());
			System.out.println("11");
			list.add(mdetails);

			return list;


		} catch (GlobalNoRecordFoundException ex) {

			throw ex;

		} catch (Exception ex) {

			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());

		}

	}


	/**
	 * @ejb:interface-method view-type="remote"
	 **/
	public Integer saveApChkEntry(com.util.ApCheckDetails details, String PYT_NM,
			String BA_NM, String TC_NM, String WTC_NM, String FC_NM,
			String SPL_SPPLR_CODE, String CB_NM, ArrayList drList, boolean isDraft, Integer AD_BRNCH, Integer AD_CMPNY)
	throws GlobalRecordAlreadyDeletedException,
	GlobalDocumentNumberNotUniqueException,
	ApCHKCheckNumberNotUniqueException,
	GlobalConversionDateNotExistException,
	GlobalBranchAccountNumberInvalidException,
	GlobalTransactionAlreadyApprovedException,
	GlobalTransactionAlreadyPendingException,
	GlobalTransactionAlreadyPostedException,
	GlobalTransactionAlreadyVoidException,
	GlobalNoApprovalRequesterFoundException,
	GlobalNoApprovalApproverFoundException,
	GlJREffectiveDateNoPeriodExistException,
	GlJREffectiveDatePeriodClosedException,
	GlobalJournalNotBalanceException {

		Debug.print("ApDirectCheckEntryControllerBean saveApChkEntry");


		LocalApCheckHome apCheckHome = null;
		LocalApCheckBatchHome apCheckBatchHome = null;
		LocalGlFunctionalCurrencyHome glFunctionalCurrencyHome = null;
		LocalApTaxCodeHome apTaxCodeHome = null;
		LocalApWithholdingTaxCodeHome apWithholdingTaxCodeHome = null;
		LocalApSupplierHome apSupplierHome = null;
		LocalGlFunctionalCurrencyRateHome glFunctionalCurrencyRateHome = null;
		LocalAdDocumentSequenceAssignmentHome adDocumentSequenceAssignmentHome = null;
		LocalAdBankAccountHome adBankAccountHome = null;
		LocalAdApprovalHome adApprovalHome = null;
		LocalAdAmountLimitHome adAmountLimitHome = null;
		LocalAdApprovalUserHome adApprovalUserHome = null;
		LocalAdApprovalQueueHome adApprovalQueueHome = null;
		LocalAdPreferenceHome adPreferenceHome = null;
		LocalAdBranchDocumentSequenceAssignmentHome adBranchDocumentSequenceAssignmentHome = null;
		LocalAdCompanyHome adCompanyHome = null;
		LocalAdPaymentTermHome adPaymentTermHome = null;
		LocalInvTagHome invTagHome = null;

		LocalApCheck apCheck = null;


		// Initialize EJB Home

		try {


			apCheckHome = (LocalApCheckHome)EJBHomeFactory.
			lookUpLocalHome(LocalApCheckHome.JNDI_NAME, LocalApCheckHome.class);
			apCheckBatchHome = (LocalApCheckBatchHome)EJBHomeFactory.
			lookUpLocalHome(LocalApCheckBatchHome.JNDI_NAME, LocalApCheckBatchHome.class);
			glFunctionalCurrencyHome = (LocalGlFunctionalCurrencyHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlFunctionalCurrencyHome.JNDI_NAME, LocalGlFunctionalCurrencyHome.class);
			apTaxCodeHome = (LocalApTaxCodeHome)EJBHomeFactory.
			lookUpLocalHome(LocalApTaxCodeHome.JNDI_NAME, LocalApTaxCodeHome.class);
			apWithholdingTaxCodeHome = (LocalApWithholdingTaxCodeHome)EJBHomeFactory.
			lookUpLocalHome(LocalApWithholdingTaxCodeHome.JNDI_NAME, LocalApWithholdingTaxCodeHome.class);
			apSupplierHome = (LocalApSupplierHome)EJBHomeFactory.
			lookUpLocalHome(LocalApSupplierHome.JNDI_NAME, LocalApSupplierHome.class);
			glFunctionalCurrencyRateHome = (LocalGlFunctionalCurrencyRateHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlFunctionalCurrencyRateHome.JNDI_NAME, LocalGlFunctionalCurrencyRateHome.class);
			adDocumentSequenceAssignmentHome = (LocalAdDocumentSequenceAssignmentHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdDocumentSequenceAssignmentHome.JNDI_NAME, LocalAdDocumentSequenceAssignmentHome.class);
			adBankAccountHome = (LocalAdBankAccountHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdBankAccountHome.JNDI_NAME, LocalAdBankAccountHome.class);
			adApprovalHome = (LocalAdApprovalHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdApprovalHome.JNDI_NAME, LocalAdApprovalHome.class);
			adAmountLimitHome = (LocalAdAmountLimitHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdAmountLimitHome.JNDI_NAME, LocalAdAmountLimitHome.class);
			adApprovalUserHome = (LocalAdApprovalUserHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdApprovalUserHome.JNDI_NAME, LocalAdApprovalUserHome.class);
			adApprovalQueueHome = (LocalAdApprovalQueueHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdApprovalQueueHome.JNDI_NAME, LocalAdApprovalQueueHome.class);
			adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);
			adBranchDocumentSequenceAssignmentHome = (LocalAdBranchDocumentSequenceAssignmentHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdBranchDocumentSequenceAssignmentHome.JNDI_NAME, LocalAdBranchDocumentSequenceAssignmentHome.class);
 			adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME,LocalAdCompanyHome.class);
 			invTagHome = (LocalInvTagHome)EJBHomeFactory.
			lookUpLocalHome(LocalInvTagHome.JNDI_NAME,LocalInvTagHome.class);

 			adPaymentTermHome = (LocalAdPaymentTermHome)EJBHomeFactory.
 			lookUpLocalHome(LocalAdPaymentTermHome.JNDI_NAME,LocalAdPaymentTermHome.class);
		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}

		try {

			// validate if check is already deleted

			try {

				if (details.getChkCode() != null) {

					apCheck = apCheckHome.findByPrimaryKey(details.getChkCode());

				}

			} catch (FinderException ex) {

				throw new GlobalRecordAlreadyDeletedException();

			}

			// validate if check is already posted, void, approved or pending

			if (details.getChkCode() != null && details.getChkVoid() == EJBCommon.FALSE) {


				if (apCheck.getChkApprovalStatus() != null) {

					if (apCheck.getChkApprovalStatus().equals("APPROVED") ||
							apCheck.getChkApprovalStatus().equals("N/A")) {

						throw new GlobalTransactionAlreadyApprovedException();

					} else if (apCheck.getChkApprovalStatus().equals("PENDING")) {

						throw new GlobalTransactionAlreadyPendingException();

					}

				}

				if (apCheck.getChkPosted() == EJBCommon.TRUE) {

					throw new GlobalTransactionAlreadyPostedException();

				} else if (apCheck.getChkVoid() == EJBCommon.TRUE) {

					throw new GlobalTransactionAlreadyVoidException();

				}

			}

			// check void

			if (details.getChkCode() != null && details.getChkVoid() == EJBCommon.TRUE) {

				if (apCheck.getChkVoid() == EJBCommon.TRUE) {

					throw new GlobalTransactionAlreadyVoidException();

				}

				if (apCheck.getChkPosted() == EJBCommon.TRUE) {

					// generate approval status

					String CHK_APPRVL_STATUS = null;

					if (!isDraft) {

						LocalAdApproval adApproval = adApprovalHome.findByAprAdCompany(AD_CMPNY);

						// check if ap check approval is enabled

						if (adApproval.getAprEnableApCheck() == EJBCommon.FALSE) {

							CHK_APPRVL_STATUS = "N/A";

						} else {

							// check if check is self approved

							LocalAdAmountLimit adAmountLimit = null;

							try {

								adAmountLimit = adAmountLimitHome.findByAdcTypeAndAuTypeAndUsrName("AP CHECK", "REQUESTER", details.getChkLastModifiedBy(), AD_CMPNY);

							} catch (FinderException ex) {

								throw new GlobalNoApprovalRequesterFoundException();

							}

							if (apCheck.getChkAmount() <= adAmountLimit.getCalAmountLimit()) {

								CHK_APPRVL_STATUS = "N/A";

							} else {

								// for approval, create approval queue

								Collection adAmountLimits = adAmountLimitHome.findByAdcTypeAndGreaterThanCalAmountLimit("AP CHECK", adAmountLimit.getCalAmountLimit(), AD_CMPNY);

								if (adAmountLimits.isEmpty()) {

									Collection adApprovalUsers = adApprovalUserHome.findByAuTypeAndCalCode("APPROVER", adAmountLimit.getCalCode(), AD_CMPNY);

									if (adApprovalUsers.isEmpty()) {

										throw new GlobalNoApprovalApproverFoundException();

									}

									Iterator j = adApprovalUsers.iterator();

									while (j.hasNext()) {

										LocalAdApprovalUser adApprovalUser = (LocalAdApprovalUser)j.next();

										LocalAdApprovalQueue adApprovalQueue = adApprovalQueueHome.create(EJBCommon.TRUE, "AP CHECK", apCheck.getChkCode(),
												apCheck.getChkDocumentNumber(), apCheck.getChkDate(), adAmountLimit.getCalAndOr(), adApprovalUser.getAuOr(), AD_BRNCH, AD_CMPNY);

										adApprovalUser.getAdUser().addAdApprovalQueue(adApprovalQueue);

									}

								} else {

									boolean isApprovalUsersFound = false;

									Iterator i = adAmountLimits.iterator();

									while (i.hasNext()) {

										LocalAdAmountLimit adNextAmountLimit = (LocalAdAmountLimit)i.next();

										if (apCheck.getChkAmount() <= adNextAmountLimit.getCalAmountLimit()) {

											Collection adApprovalUsers = adApprovalUserHome.findByAuTypeAndCalCode("APPROVER", adAmountLimit.getCalCode(), AD_CMPNY);

											Iterator j = adApprovalUsers.iterator();

											while (j.hasNext()) {

												isApprovalUsersFound = true;

												LocalAdApprovalUser adApprovalUser = (LocalAdApprovalUser)j.next();

												LocalAdApprovalQueue adApprovalQueue = adApprovalQueueHome.create(EJBCommon.TRUE, "AP CHECK", apCheck.getChkCode(),
														apCheck.getChkDocumentNumber(), apCheck.getChkDate(), adAmountLimit.getCalAndOr(), adApprovalUser.getAuOr(), AD_BRNCH, AD_CMPNY);

												adApprovalUser.getAdUser().addAdApprovalQueue(adApprovalQueue);

											}

											break;

										} else if (!i.hasNext()) {

											Collection adApprovalUsers = adApprovalUserHome.findByAuTypeAndCalCode("APPROVER", adNextAmountLimit.getCalCode(), AD_CMPNY);

											Iterator j = adApprovalUsers.iterator();

											while (j.hasNext()) {

												isApprovalUsersFound = true;

												LocalAdApprovalUser adApprovalUser = (LocalAdApprovalUser)j.next();

												LocalAdApprovalQueue adApprovalQueue = adApprovalQueueHome.create(EJBCommon.TRUE, "AP CHECK", apCheck.getChkCode(),
														apCheck.getChkDocumentNumber(), apCheck.getChkDate(), adNextAmountLimit.getCalAndOr(), adApprovalUser.getAuOr(), AD_BRNCH, AD_CMPNY);

												adApprovalUser.getAdUser().addAdApprovalQueue(adApprovalQueue);

											}

											break;

										}

										adAmountLimit = adNextAmountLimit;

									}

									if (!isApprovalUsersFound) {

										throw new GlobalNoApprovalApproverFoundException();

									}

								}

								CHK_APPRVL_STATUS = "PENDING";
							}
						}
					}


					// reverse distribution records

					Collection apDistributionRecords = apCheck.getApDistributionRecords();
					ArrayList list = new ArrayList();

					Iterator i = apDistributionRecords.iterator();

					while (i.hasNext()) {

						LocalApDistributionRecord apDistributionRecord = (LocalApDistributionRecord)i.next();

						list.add(apDistributionRecord);

					}

					i = list.iterator();

					while (i.hasNext()) {

						LocalApDistributionRecord apDistributionRecord = (LocalApDistributionRecord)i.next();

						ApModDistributionRecordDetails mDrDetails = new ApModDistributionRecordDetails();
						mDrDetails.setDrLine(apCheck.getApDrNextLine());
						mDrDetails.setDrClass(apDistributionRecord.getDrClass());
						mDrDetails.setDrDebit(apDistributionRecord.getDrDebit() == EJBCommon.TRUE ? EJBCommon.FALSE : EJBCommon.TRUE);
						mDrDetails.setDrAmount(apDistributionRecord.getDrAmount());
						mDrDetails.setDrReversed(EJBCommon.TRUE);
						mDrDetails.setDrCoaAccountNumber(apDistributionRecord.getGlChartOfAccount().getCoaAccountNumber());

						this.addApDrEntry(mDrDetails, apCheck, AD_BRNCH, AD_CMPNY);

					}

					LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);

					if (CHK_APPRVL_STATUS != null && CHK_APPRVL_STATUS.equals("N/A") && adPreference.getPrfApGlPostingType().equals("AUTO-POST UPON APPROVAL")) {

						apCheck.setChkVoid(EJBCommon.TRUE);
						this.executeApChkPost(apCheck.getChkCode(), details.getChkLastModifiedBy(), AD_BRNCH, AD_CMPNY);

					}

					// set void approval status

					apCheck.setChkVoidApprovalStatus(CHK_APPRVL_STATUS);


				}

				apCheck.setChkVoid(EJBCommon.TRUE);
				apCheck.setChkLastModifiedBy(details.getChkLastModifiedBy());
				apCheck.setChkDateLastModified(details.getChkDateLastModified());

				return apCheck.getChkCode();

			}

			// validate if document number is unique document number is automatic then set next sequence

			try {

				LocalAdBranchDocumentSequenceAssignment adBranchDocumentSequenceAssignment = null;
				LocalAdDocumentSequenceAssignment adDocumentSequenceAssignment = null;

				if (details.getChkCode() == null) {

					try {

						adDocumentSequenceAssignment = adDocumentSequenceAssignmentHome.findByDcName("AP CHECK", AD_CMPNY);

					} catch(FinderException ex) { }

					try {

						adBranchDocumentSequenceAssignment = adBranchDocumentSequenceAssignmentHome.findBdsByDsaCodeAndBrCode(adDocumentSequenceAssignment.getDsaCode(), AD_BRNCH, AD_CMPNY);

					} catch (FinderException ex) { }


					LocalApCheck apExistingCheck = null;

					try {

						apExistingCheck = apCheckHome.findByChkDocumentNumberAndBrCode(
								details.getChkDocumentNumber(), AD_BRNCH, AD_CMPNY);

					} catch (FinderException ex) {
					}

					if (apExistingCheck != null) {

						throw new GlobalDocumentNumberNotUniqueException();

					}

					if (adDocumentSequenceAssignment.getAdDocumentSequence().getDsNumberingType() == 'A' &&
							(details.getChkDocumentNumber() == null || details.getChkDocumentNumber().trim().length() == 0)) {

						while (true) {

							if (adBranchDocumentSequenceAssignment == null || adBranchDocumentSequenceAssignment.getBdsNextSequence() == null) {

								try {

									apCheckHome.findByChkDocumentNumberAndBrCode(adDocumentSequenceAssignment.getDsaNextSequence(), AD_BRNCH, AD_CMPNY);
									adDocumentSequenceAssignment.setDsaNextSequence(EJBCommon.incrementStringNumber(adDocumentSequenceAssignment.getDsaNextSequence()));

								} catch (FinderException ex) {

									details.setChkDocumentNumber(adDocumentSequenceAssignment.getDsaNextSequence());
									adDocumentSequenceAssignment.setDsaNextSequence(EJBCommon.incrementStringNumber(adDocumentSequenceAssignment.getDsaNextSequence()));
									break;

								}

							} else {

								try {

									apCheckHome.findByChkDocumentNumberAndBrCode(adBranchDocumentSequenceAssignment.getBdsNextSequence(), AD_BRNCH, AD_CMPNY);
									adBranchDocumentSequenceAssignment.setBdsNextSequence(EJBCommon.incrementStringNumber(adBranchDocumentSequenceAssignment.getBdsNextSequence()));

								} catch (FinderException ex) {

									details.setChkDocumentNumber(adBranchDocumentSequenceAssignment.getBdsNextSequence());
									adBranchDocumentSequenceAssignment.setBdsNextSequence(EJBCommon.incrementStringNumber(adBranchDocumentSequenceAssignment.getBdsNextSequence()));
									break;

								}

							}

						}

					}

				} else {

					LocalApCheck apExistingCheck = null;

					try {

						apExistingCheck = apCheckHome.findByChkDocumentNumberAndBrCode (
								details.getChkDocumentNumber(), AD_BRNCH, AD_CMPNY);

					} catch (FinderException ex) {
					}

					if (apExistingCheck != null &&
							!apExistingCheck.getChkCode().equals(details.getChkCode())) {

						throw new GlobalDocumentNumberNotUniqueException();

					}

					if (apCheck.getChkDocumentNumber() != details.getChkDocumentNumber() &&
							(details.getChkDocumentNumber() == null || details.getChkDocumentNumber().trim().length() == 0)) {

						details.setChkDocumentNumber(apCheck.getChkDocumentNumber());

					}

				}

			} catch (GlobalDocumentNumberNotUniqueException ex) {

				getSessionContext().setRollbackOnly();
				throw ex;

			} catch (Exception ex) {

				Debug.printStackTrace(ex);
				getSessionContext().setRollbackOnly();
				throw new EJBException(ex.getMessage());

			}

			// validate if check number is unique check is automatic then set next sequence

			if (details.getChkCode() == null) {

				LocalApCheck apExistingCheck = null;

				try {

					apExistingCheck = apCheckHome.findByChkNumberAndBaName(
							details.getChkNumber(), BA_NM, AD_CMPNY);

				} catch (FinderException ex) {
				}

				if (apExistingCheck != null) {

					throw new ApCHKCheckNumberNotUniqueException();

				}

				LocalAdBankAccount adBankAccount = adBankAccountHome.findByBaName(BA_NM, AD_CMPNY);

				if (details.getChkNumber() == null || details.getChkNumber().trim().length() == 0) {

					while (true) {

						try {

							apCheckHome.findByChkNumberAndBaName(adBankAccount.getBaNextCheckNumber(), BA_NM, AD_CMPNY);
							adBankAccount.setBaNextCheckNumber(EJBCommon.incrementStringNumber(adBankAccount.getBaNextCheckNumber()));

						} catch (FinderException ex) {

							details.setChkNumber(adBankAccount.getBaNextCheckNumber());
							adBankAccount.setBaNextCheckNumber(EJBCommon.incrementStringNumber(adBankAccount.getBaNextCheckNumber()));
							break;

						}

					}

				}

			} else {

				LocalApCheck apExistingCheck = null;

				try {

					apExistingCheck = apCheckHome.findByChkNumberAndBaName(
							details.getChkNumber(), BA_NM, AD_CMPNY);

				} catch (FinderException ex) {
				}

				if (apExistingCheck != null &&
						!apExistingCheck.getChkCode().equals(details.getChkCode())) {

					throw new ApCHKCheckNumberNotUniqueException();

				}

				if (apCheck.getChkNumber() != details.getChkNumber() &&
						(details.getChkNumber() == null || details.getChkNumber().trim().length() == 0)) {

					details.setChkNumber(apCheck.getChkNumber());

				}

			}


			// validate if conversion date exists

			try {

				if (details.getChkConversionDate() != null) {

					LocalGlFunctionalCurrency glValidateFunctionalCurrency = glFunctionalCurrencyHome.findByFcName(FC_NM, AD_CMPNY);
					LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);

					if (!glValidateFunctionalCurrency.getFcName().equals("USD")) {

						LocalGlFunctionalCurrencyRate glFunctionalCurrencyRate =
							glFunctionalCurrencyRateHome.findByFcCodeAndDate(glValidateFunctionalCurrency.getFcCode(),
									details.getChkConversionDate(), AD_CMPNY);

					} else if (!adCompany.getGlFunctionalCurrency().getFcName().equals("USD")){

						LocalGlFunctionalCurrencyRate glFunctionalCurrencyRate =
							glFunctionalCurrencyRateHome.findByFcCodeAndDate(
									adCompany.getGlFunctionalCurrency().getFcCode(), details.getChkConversionDate(), AD_CMPNY);

					}

				}

			} catch (FinderException ex) {

				throw new GlobalConversionDateNotExistException();

			}


			// create direct check
			if (details.getChkCode() == null) {

				apCheck = apCheckHome.create("DIRECT", details.getChkDescription(),
						details.getChkDate(), details.getChkCheckDate(), details.getChkNumber(), details.getChkDocumentNumber(),
						details.getChkReferenceNumber(), details.getChkInfoType(), details.getChkInfoBioNumber(), details.getChkInfoBioDescription(), details.getChkInfoTypeStatus(), details.getChkInfoRequestStatus(),
						details.getChkInvtInscribedStock(), details.getChkInvtTreasuryBill(), details.getChkInvtNextRunDate(), details.getChkInvtSettlementDate(), details.getChkInvtMaturityDate(), details.getChkInvtBidYield(), details.getChkInvtCouponRate(), details.getChkInvtSettleAmount(), details.getChkInvtFaceValue(), details.getChkInvtPremiumAmount(),
						details.getChkLoan(), details.getChkLoanGenerated(),
						details.getChkConversionDate(), details.getChkConversionRate(),
						details.getChkBillAmount(), details.getChkAmount(), null, null, EJBCommon.FALSE,
						EJBCommon.FALSE, details.getChkCrossCheck(), null, EJBCommon.FALSE, details.getChkCreatedBy(),
						details.getChkDateCreated(), details.getChkLastModifiedBy(),
						details.getChkDateLastModified(), null, null, null, null, EJBCommon.FALSE, null,
						EJBCommon.FALSE, null, EJBCommon.FALSE, EJBCommon.FALSE, details.getChkMemo(), null, AD_BRNCH, AD_CMPNY);

			} else {

				apCheck.setChkDescription(details.getChkDescription());

                                apCheck.setChkInfoType(details.getChkInfoType());
                                apCheck.setChkInfoBioNumber(details.getChkInfoBioNumber());
                                apCheck.setChkInfoBioDescription(details.getChkInfoBioDescription());
                                apCheck.setChkInfoTypeStatus(details.getChkInfoTypeStatus());
                                apCheck.setChkInfoRequestStatus(details.getChkInfoRequestStatus());


				apCheck.setChkDate(details.getChkDate());
				apCheck.setChkCheckDate(details.getChkCheckDate());
				apCheck.setChkNumber(details.getChkNumber());
				apCheck.setChkDocumentNumber(details.getChkDocumentNumber());
				apCheck.setChkReferenceNumber(details.getChkReferenceNumber());
				apCheck.setChkConversionDate(details.getChkConversionDate());
				apCheck.setChkConversionRate(details.getChkConversionRate());
				apCheck.setChkBillAmount(details.getChkBillAmount());
				apCheck.setChkAmount(details.getChkAmount());
				apCheck.setChkCrossCheck(details.getChkCrossCheck());
				apCheck.setChkLastModifiedBy(details.getChkLastModifiedBy());
				apCheck.setChkDateLastModified(details.getChkDateLastModified());
				apCheck.setChkReasonForRejection(null);
				apCheck.setChkMemo(details.getChkMemo());
				apCheck.setChkSupplierName(null);

				apCheck.setChkInvtInscribedStock(details.getChkInvtInscribedStock());
				apCheck.setChkInvtTreasuryBill(details.getChkInvtTreasuryBill());
				apCheck.setChkInvtNextRunDate(details.getChkInvtNextRunDate());
				apCheck.setChkInvtSettlementDate(details.getChkInvtSettlementDate());
				apCheck.setChkInvtMaturityDate(details.getChkInvtMaturityDate());
				apCheck.setChkInvtBidYield(details.getChkInvtBidYield());
				apCheck.setChkInvtCouponRate(details.getChkInvtCouponRate());
				apCheck.setChkInvtSettleAmount(details.getChkInvtSettleAmount());
				apCheck.setChkInvtFaceValue(details.getChkInvtFaceValue());
				apCheck.setChkInvtPremiumAmount(details.getChkInvtPremiumAmount());

				apCheck.setChkLoan(details.getChkLoan());
				apCheck.setChkLoanGenerated(details.getChkLoanGenerated());

			}

			LocalAdBankAccount adBankAccount = adBankAccountHome.findByBaName(BA_NM, AD_CMPNY);
			//adBankAccount.addApCheck(apCheck);
			apCheck.setAdBankAccount(adBankAccount);

			LocalGlFunctionalCurrency glFunctionalCurrency = glFunctionalCurrencyHome.findByFcName(FC_NM, AD_CMPNY);
			//glFunctionalCurrency.addApCheck(apCheck);
			apCheck.setGlFunctionalCurrency(glFunctionalCurrency);

			LocalApSupplier apSupplier = apSupplierHome.findBySplSupplierCode(SPL_SPPLR_CODE, AD_CMPNY);
			//apSupplier.addApCheck(apCheck);
			apCheck.setApSupplier(apSupplier);

			if(details.getChkSupplierName().length() > 0 && !apSupplier.getSplName().equals(details.getChkSupplierName())) {
				apCheck.setChkSupplierName(details.getChkSupplierName());
			}

			LocalApTaxCode apTaxCode = apTaxCodeHome.findByTcName(TC_NM, AD_CMPNY);
			//apTaxCode.addApCheck(apCheck);
			apCheck.setApTaxCode(apTaxCode);

			LocalApWithholdingTaxCode apWithholdingTaxCode = apWithholdingTaxCodeHome.findByWtcName(WTC_NM, AD_CMPNY);
			//apWithholdingTaxCode.addApCheck(apCheck);
			apCheck.setApWithholdingTaxCode(apWithholdingTaxCode);



			try{
				LocalApCheckBatch apCheckBatch = apCheckBatchHome.findByDirectCbName(CB_NM, AD_BRNCH, AD_CMPNY);
				//apCheckBatch.addApCheck(apCheck);
				apCheck.setApCheckBatch(apCheckBatch);
			} catch (FinderException ex) {
			}

			try{

				LocalAdPaymentTerm adPaymentTerm = adPaymentTermHome.findByPytName(PYT_NM, AD_CMPNY);
				//adPaymentTerm.addApVoucher(apVoucher);
				apCheck.setAdPaymentTerm(adPaymentTerm);
			} catch (Exception ex){}

			// remove all voucher line items

			Collection apVoucherLineItems = apCheck.getApVoucherLineItems();

			Iterator i = apVoucherLineItems.iterator();

			while (i.hasNext()) {

				LocalApVoucherLineItem apVoucherLineItem = (LocalApVoucherLineItem)i.next();

				i.remove();

				apVoucherLineItem.remove();

			}

			// remove all distribution records

			Collection apDistributionRecords = apCheck.getApDistributionRecords();

			i = apDistributionRecords.iterator();

			while (i.hasNext()) {

				LocalApDistributionRecord apDistributionRecord = (LocalApDistributionRecord)i.next();

				i.remove();

				apDistributionRecord.remove();

			}


			// add new distribution records

			i = drList.iterator();

			while (i.hasNext()) {

				ApModDistributionRecordDetails mDrDetails = (ApModDistributionRecordDetails) i.next();
				mDrDetails.setDrReversed(EJBCommon.FALSE);

				this.addApDrEntry(mDrDetails, apCheck, AD_BRNCH, AD_CMPNY);

			}


			// generate approval status

			String CHK_APPRVL_STATUS = null;

			if (!isDraft) {

				LocalAdApproval adApproval = adApprovalHome.findByAprAdCompany(AD_CMPNY);

				// check if ap check approval is enabled

				if (adApproval.getAprEnableApCheck() == EJBCommon.FALSE) {

					CHK_APPRVL_STATUS = "N/A";

				} else {

					// check if check is self approved

					LocalAdAmountLimit adAmountLimit = null;

					try {

						adAmountLimit = adAmountLimitHome.findByAdcTypeAndAuTypeAndUsrName("AP CHECK", "REQUESTER", details.getChkLastModifiedBy(), AD_CMPNY);

					} catch (FinderException ex) {

						throw new GlobalNoApprovalRequesterFoundException();

					}

					if (apCheck.getChkAmount() <= adAmountLimit.getCalAmountLimit()) {

						CHK_APPRVL_STATUS = "N/A";

					} else {

						// for approval, create approval queue

						Collection adAmountLimits = adAmountLimitHome.findByAdcTypeAndGreaterThanCalAmountLimit("AP CHECK", adAmountLimit.getCalAmountLimit(), AD_CMPNY);

						if (adAmountLimits.isEmpty()) {

							Collection adApprovalUsers = adApprovalUserHome.findByAuTypeAndCalCode("APPROVER", adAmountLimit.getCalCode(), AD_CMPNY);

							if (adApprovalUsers.isEmpty()) {

								throw new GlobalNoApprovalApproverFoundException();

							}

							Iterator j = adApprovalUsers.iterator();

							while (j.hasNext()) {

								LocalAdApprovalUser adApprovalUser = (LocalAdApprovalUser)j.next();

								LocalAdApprovalQueue adApprovalQueue = adApprovalQueueHome.create(EJBCommon.TRUE, "AP CHECK", apCheck.getChkCode(),
										apCheck.getChkDocumentNumber(), apCheck.getChkDate(), adAmountLimit.getCalAndOr(), adApprovalUser.getAuOr(), AD_BRNCH, AD_CMPNY);

								adApprovalUser.getAdUser().addAdApprovalQueue(adApprovalQueue);

							}

						} else {

							boolean isApprovalUsersFound = false;

							i = adAmountLimits.iterator();

							while (i.hasNext()) {

								LocalAdAmountLimit adNextAmountLimit = (LocalAdAmountLimit)i.next();

								if (apCheck.getChkAmount() <= adNextAmountLimit.getCalAmountLimit()) {

									Collection adApprovalUsers = adApprovalUserHome.findByAuTypeAndCalCode("APPROVER", adAmountLimit.getCalCode(), AD_CMPNY);

									Iterator j = adApprovalUsers.iterator();

									while (j.hasNext()) {

										isApprovalUsersFound = true;

										LocalAdApprovalUser adApprovalUser = (LocalAdApprovalUser)j.next();

										LocalAdApprovalQueue adApprovalQueue = adApprovalQueueHome.create(EJBCommon.TRUE, "AP CHECK", apCheck.getChkCode(),
												apCheck.getChkDocumentNumber(), apCheck.getChkDate(), adAmountLimit.getCalAndOr(), adApprovalUser.getAuOr(), AD_BRNCH, AD_CMPNY);

										adApprovalUser.getAdUser().addAdApprovalQueue(adApprovalQueue);

									}

									break;

								} else if (!i.hasNext()) {

									Collection adApprovalUsers = adApprovalUserHome.findByAuTypeAndCalCode("APPROVER", adNextAmountLimit.getCalCode(), AD_CMPNY);

									Iterator j = adApprovalUsers.iterator();

									while (j.hasNext()) {

										isApprovalUsersFound = true;

										LocalAdApprovalUser adApprovalUser = (LocalAdApprovalUser)j.next();

										LocalAdApprovalQueue adApprovalQueue = adApprovalQueueHome.create(EJBCommon.TRUE, "AP CHECK", apCheck.getChkCode(),
												apCheck.getChkDocumentNumber(), apCheck.getChkDate(), adNextAmountLimit.getCalAndOr(), adApprovalUser.getAuOr(), AD_BRNCH, AD_CMPNY);

										adApprovalUser.getAdUser().addAdApprovalQueue(adApprovalQueue);

									}

									break;

								}

								adAmountLimit = adNextAmountLimit;

							}

							if (!isApprovalUsersFound) {

								throw new GlobalNoApprovalApproverFoundException();

							}

						}

						CHK_APPRVL_STATUS = "PENDING";
					}
				}
			}

			LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);

			if (CHK_APPRVL_STATUS != null && CHK_APPRVL_STATUS.equals("N/A") && adPreference.getPrfApGlPostingType().equals("AUTO-POST UPON APPROVAL")) {

				this.executeApChkPost(apCheck.getChkCode(), details.getChkLastModifiedBy(), AD_BRNCH, AD_CMPNY);

			}

			// set check approval status

			apCheck.setChkApprovalStatus(CHK_APPRVL_STATUS);

			return apCheck.getChkCode();

		} catch (GlobalRecordAlreadyDeletedException ex) {

			getSessionContext().setRollbackOnly();
			throw ex;

		} catch (GlobalDocumentNumberNotUniqueException ex) {

			getSessionContext().setRollbackOnly();
			throw ex;

		} catch (ApCHKCheckNumberNotUniqueException ex) {

			getSessionContext().setRollbackOnly();
			throw ex;

		} catch (GlobalConversionDateNotExistException ex) {

			getSessionContext().setRollbackOnly();
			throw ex;

		} catch (GlobalBranchAccountNumberInvalidException ex) {

			getSessionContext().setRollbackOnly();
			throw ex;

		} catch (GlobalTransactionAlreadyApprovedException ex) {

			getSessionContext().setRollbackOnly();
			throw ex;

		} catch (GlobalTransactionAlreadyPendingException ex) {

			getSessionContext().setRollbackOnly();
			throw ex;

		} catch (GlobalTransactionAlreadyPostedException ex) {

			getSessionContext().setRollbackOnly();
			throw ex;

		} catch (GlobalTransactionAlreadyVoidException ex) {

			getSessionContext().setRollbackOnly();
			throw ex;

		} catch (GlobalNoApprovalRequesterFoundException ex) {

			getSessionContext().setRollbackOnly();
			throw ex;

		} catch (GlobalNoApprovalApproverFoundException ex) {

			getSessionContext().setRollbackOnly();
			throw ex;

		} catch (GlJREffectiveDateNoPeriodExistException ex) {

			getSessionContext().setRollbackOnly();
			throw ex;

		} catch (GlJREffectiveDatePeriodClosedException ex) {

			getSessionContext().setRollbackOnly();
			throw ex;

		} catch (GlobalJournalNotBalanceException ex) {

			getSessionContext().setRollbackOnly();
			throw ex;

		} catch (Exception ex) {

			Debug.printStackTrace(ex);
			getSessionContext().setRollbackOnly();
			throw new EJBException(ex.getMessage());

		}


	}

	/**
	 * @ejb:interface-method view-type="remote"
	 **/
	public Integer saveApChkVliEntry(com.util.ApCheckDetails details,
			String BA_NM, String TC_NM, String WTC_NM, String FC_NM,
			String SPL_SPPLR_CODE, String CB_NM, ArrayList vliList, boolean isDraft, Integer AD_BRNCH, Integer AD_CMPNY)
	throws GlobalRecordAlreadyDeletedException,
	GlobalDocumentNumberNotUniqueException,
	ApCHKCheckNumberNotUniqueException,
	GlobalConversionDateNotExistException,
	GlobalTransactionAlreadyApprovedException,
	GlobalTransactionAlreadyPendingException,
	GlobalTransactionAlreadyPostedException,
	GlobalTransactionAlreadyVoidException,
	GlobalNoApprovalRequesterFoundException,
	GlobalNoApprovalApproverFoundException,
	GlobalInvItemLocationNotFoundException,
	GlJREffectiveDateNoPeriodExistException,
	GlJREffectiveDatePeriodClosedException,
	GlobalJournalNotBalanceException,
	GlobalInventoryDateException,
	GlobalBranchAccountNumberInvalidException,
	AdPRFCoaGlVarianceAccountNotFoundException {

		Debug.print("ApDirectCheckEntryControllerBean saveApChkVliEntry");

		LocalAdUserHome adUserHome = null;
		LocalApCheckHome apCheckHome = null;
		LocalApCheckBatchHome apCheckBatchHome = null;
		LocalGlFunctionalCurrencyHome glFunctionalCurrencyHome = null;
		LocalApTaxCodeHome apTaxCodeHome = null;
		LocalApWithholdingTaxCodeHome apWithholdingTaxCodeHome = null;
		LocalApSupplierHome apSupplierHome = null;
		LocalGlFunctionalCurrencyRateHome glFunctionalCurrencyRateHome = null;
		LocalAdDocumentSequenceAssignmentHome adDocumentSequenceAssignmentHome = null;
		LocalAdBankAccountHome adBankAccountHome = null;
		LocalAdApprovalHome adApprovalHome = null;
		LocalAdAmountLimitHome adAmountLimitHome = null;
		LocalAdApprovalUserHome adApprovalUserHome = null;
		LocalAdApprovalQueueHome adApprovalQueueHome = null;
		LocalAdPreferenceHome adPreferenceHome = null;
		LocalInvItemLocationHome invItemLocationHome = null;
		LocalInvCostingHome invCostingHome = null;
		LocalAdBranchDocumentSequenceAssignmentHome adBranchDocumentSequenceAssignmentHome = null;
		LocalAdBranchItemLocationHome adBranchItemLocationHome = null;
		LocalAdBranchBankAccountHome adBranchBankAccountHome = null;
		LocalAdBranchApTaxCodeHome adBranchApTaxCodeHome = null;
		LocalApVoucherLineItemHome apVoucherLineItemHome = null;
		LocalInvTagHome invTagHome = null;

		LocalApCheck apCheck = null;

		// Initialize EJB Home

		try {

			adUserHome = (LocalAdUserHome)EJBHomeFactory.
					lookUpLocalHome(LocalAdUserHome.JNDI_NAME, LocalAdUserHome.class);


			adBranchApTaxCodeHome = (LocalAdBranchApTaxCodeHome)EJBHomeFactory.
					lookUpLocalHome(LocalAdBranchApTaxCodeHome.JNDI_NAME, LocalAdBranchApTaxCodeHome.class);

			adBranchBankAccountHome = (LocalAdBranchBankAccountHome)EJBHomeFactory.
					lookUpLocalHome(LocalAdBranchBankAccountHome.JNDI_NAME, LocalAdBranchBankAccountHome.class);
			apCheckHome = (LocalApCheckHome)EJBHomeFactory.
			lookUpLocalHome(LocalApCheckHome.JNDI_NAME, LocalApCheckHome.class);
			apCheckBatchHome = (LocalApCheckBatchHome)EJBHomeFactory.
			lookUpLocalHome(LocalApCheckBatchHome.JNDI_NAME, LocalApCheckBatchHome.class);
			glFunctionalCurrencyHome = (LocalGlFunctionalCurrencyHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlFunctionalCurrencyHome.JNDI_NAME, LocalGlFunctionalCurrencyHome.class);
			apTaxCodeHome = (LocalApTaxCodeHome)EJBHomeFactory.
			lookUpLocalHome(LocalApTaxCodeHome.JNDI_NAME, LocalApTaxCodeHome.class);
			apWithholdingTaxCodeHome = (LocalApWithholdingTaxCodeHome)EJBHomeFactory.
			lookUpLocalHome(LocalApWithholdingTaxCodeHome.JNDI_NAME, LocalApWithholdingTaxCodeHome.class);
			apSupplierHome = (LocalApSupplierHome)EJBHomeFactory.
			lookUpLocalHome(LocalApSupplierHome.JNDI_NAME, LocalApSupplierHome.class);
			glFunctionalCurrencyRateHome = (LocalGlFunctionalCurrencyRateHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlFunctionalCurrencyRateHome.JNDI_NAME, LocalGlFunctionalCurrencyRateHome.class);
			adDocumentSequenceAssignmentHome = (LocalAdDocumentSequenceAssignmentHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdDocumentSequenceAssignmentHome.JNDI_NAME, LocalAdDocumentSequenceAssignmentHome.class);
			adBankAccountHome = (LocalAdBankAccountHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdBankAccountHome.JNDI_NAME, LocalAdBankAccountHome.class);
			adApprovalHome = (LocalAdApprovalHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdApprovalHome.JNDI_NAME, LocalAdApprovalHome.class);
			adAmountLimitHome = (LocalAdAmountLimitHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdAmountLimitHome.JNDI_NAME, LocalAdAmountLimitHome.class);
			adApprovalUserHome = (LocalAdApprovalUserHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdApprovalUserHome.JNDI_NAME, LocalAdApprovalUserHome.class);
			adApprovalQueueHome = (LocalAdApprovalQueueHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdApprovalQueueHome.JNDI_NAME, LocalAdApprovalQueueHome.class);
			adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);
			invItemLocationHome = (LocalInvItemLocationHome)EJBHomeFactory.
			lookUpLocalHome(LocalInvItemLocationHome.JNDI_NAME, LocalInvItemLocationHome.class);
			invCostingHome = (LocalInvCostingHome)EJBHomeFactory.
			lookUpLocalHome(LocalInvCostingHome.JNDI_NAME, LocalInvCostingHome.class);
			adBranchDocumentSequenceAssignmentHome = (LocalAdBranchDocumentSequenceAssignmentHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdBranchDocumentSequenceAssignmentHome.JNDI_NAME, LocalAdBranchDocumentSequenceAssignmentHome.class);
			adBranchItemLocationHome = (LocalAdBranchItemLocationHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdBranchItemLocationHome.JNDI_NAME, LocalAdBranchItemLocationHome.class);
			apVoucherLineItemHome = (LocalApVoucherLineItemHome)EJBHomeFactory.
			lookUpLocalHome(LocalApVoucherLineItemHome.JNDI_NAME, LocalApVoucherLineItemHome.class);
			invTagHome = (LocalInvTagHome)EJBHomeFactory.
			lookUpLocalHome(LocalInvTagHome.JNDI_NAME, LocalInvTagHome.class);


		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}

		try {

			LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);

			// validate if check is already deleted

			try {

				if (details.getChkCode() != null) {

					apCheck = apCheckHome.findByPrimaryKey(details.getChkCode());

				}

			} catch (FinderException ex) {

				throw new GlobalRecordAlreadyDeletedException();

			}

			// validate if check is already posted, void, approved or pending

			if (details.getChkCode() != null && details.getChkVoid() == EJBCommon.FALSE) {


				if (apCheck.getChkApprovalStatus() != null) {

					if (apCheck.getChkApprovalStatus().equals("APPROVED") ||
							apCheck.getChkApprovalStatus().equals("N/A")) {

						throw new GlobalTransactionAlreadyApprovedException();

					} else if (apCheck.getChkApprovalStatus().equals("PENDING")) {

						throw new GlobalTransactionAlreadyPendingException();

					}

				}

				if (apCheck.getChkPosted() == EJBCommon.TRUE) {

					throw new GlobalTransactionAlreadyPostedException();

				} else if (apCheck.getChkVoid() == EJBCommon.TRUE) {

					throw new GlobalTransactionAlreadyVoidException();

				}

			}

			// check void

			if (details.getChkCode() != null && details.getChkVoid() == EJBCommon.TRUE) {

				if (apCheck.getChkVoid() == EJBCommon.TRUE) {

					throw new GlobalTransactionAlreadyVoidException();

				}

				if (apCheck.getChkPosted() == EJBCommon.TRUE) {

					// generate approval status

					String CHK_APPRVL_STATUS = null;

					if (!isDraft) {

						LocalAdApproval adApproval = adApprovalHome.findByAprAdCompany(AD_CMPNY);

						// check if ap check approval is enabled

						if (adApproval.getAprEnableApCheck() == EJBCommon.FALSE) {

							CHK_APPRVL_STATUS = "N/A";

						} else {

							// check if check is self approved

							LocalAdAmountLimit adAmountLimit = null;

							try {

								adAmountLimit = adAmountLimitHome.findByAdcTypeAndAuTypeAndUsrName("AP CHECK", "REQUESTER", details.getChkLastModifiedBy(), AD_CMPNY);

							} catch (FinderException ex) {

								throw new GlobalNoApprovalRequesterFoundException();

							}

							if (apCheck.getChkAmount() <= adAmountLimit.getCalAmountLimit()) {

								CHK_APPRVL_STATUS = "N/A";

							} else {

								// for approval, create approval queue

								Collection adAmountLimits = adAmountLimitHome.findByAdcTypeAndGreaterThanCalAmountLimit("AP CHECK", adAmountLimit.getCalAmountLimit(), AD_CMPNY);

								if (adAmountLimits.isEmpty()) {

									Collection adApprovalUsers = adApprovalUserHome.findByAuTypeAndCalCode("APPROVER", adAmountLimit.getCalCode(), AD_CMPNY);

									if (adApprovalUsers.isEmpty()) {

										throw new GlobalNoApprovalApproverFoundException();

									}

									Iterator j = adApprovalUsers.iterator();

									while (j.hasNext()) {

										LocalAdApprovalUser adApprovalUser = (LocalAdApprovalUser)j.next();

										LocalAdApprovalQueue adApprovalQueue = adApprovalQueueHome.create(EJBCommon.TRUE, "AP CHECK", apCheck.getChkCode(),
												apCheck.getChkDocumentNumber(), apCheck.getChkDate(), adAmountLimit.getCalAndOr(), adApprovalUser.getAuOr(), AD_BRNCH, AD_CMPNY);

										adApprovalUser.getAdUser().addAdApprovalQueue(adApprovalQueue);

									}

								} else {

									boolean isApprovalUsersFound = false;

									Iterator i = adAmountLimits.iterator();

									while (i.hasNext()) {

										LocalAdAmountLimit adNextAmountLimit = (LocalAdAmountLimit)i.next();

										if (apCheck.getChkAmount() <= adNextAmountLimit.getCalAmountLimit()) {

											Collection adApprovalUsers = adApprovalUserHome.findByAuTypeAndCalCode("APPROVER", adAmountLimit.getCalCode(), AD_CMPNY);

											Iterator j = adApprovalUsers.iterator();

											while (j.hasNext()) {

												isApprovalUsersFound = true;

												LocalAdApprovalUser adApprovalUser = (LocalAdApprovalUser)j.next();

												LocalAdApprovalQueue adApprovalQueue = adApprovalQueueHome.create(EJBCommon.TRUE, "AP CHECK", apCheck.getChkCode(),
														apCheck.getChkDocumentNumber(), apCheck.getChkDate(), adAmountLimit.getCalAndOr(), adApprovalUser.getAuOr(), AD_BRNCH, AD_CMPNY);

												adApprovalUser.getAdUser().addAdApprovalQueue(adApprovalQueue);

											}

											break;

										} else if (!i.hasNext()) {

											Collection adApprovalUsers = adApprovalUserHome.findByAuTypeAndCalCode("APPROVER", adNextAmountLimit.getCalCode(), AD_CMPNY);

											Iterator j = adApprovalUsers.iterator();

											while (j.hasNext()) {

												isApprovalUsersFound = true;

												LocalAdApprovalUser adApprovalUser = (LocalAdApprovalUser)j.next();

												LocalAdApprovalQueue adApprovalQueue = adApprovalQueueHome.create(EJBCommon.TRUE, "AP CHECK", apCheck.getChkCode(),
														apCheck.getChkDocumentNumber(), apCheck.getChkDate(), adNextAmountLimit.getCalAndOr(), adApprovalUser.getAuOr(), AD_BRNCH, AD_CMPNY);

												adApprovalUser.getAdUser().addAdApprovalQueue(adApprovalQueue);

											}

											break;

										}

										adAmountLimit = adNextAmountLimit;

									}

									if (!isApprovalUsersFound) {

										throw new GlobalNoApprovalApproverFoundException();

									}

								}

								CHK_APPRVL_STATUS = "PENDING";
							}
						}
					}


					// reverse distribution records

					Collection apDistributionRecords = apCheck.getApDistributionRecords();
					ArrayList list = new ArrayList();

					Iterator i = apDistributionRecords.iterator();

					while (i.hasNext()) {

						LocalApDistributionRecord apDistributionRecord = (LocalApDistributionRecord)i.next();

						list.add(apDistributionRecord);

					}

					i = list.iterator();

					while (i.hasNext()) {

						LocalApDistributionRecord apDistributionRecord = (LocalApDistributionRecord)i.next();

						ApModDistributionRecordDetails mDrDetails = new ApModDistributionRecordDetails();
						mDrDetails.setDrLine(apCheck.getApDrNextLine());
						mDrDetails.setDrClass(apDistributionRecord.getDrClass());
						mDrDetails.setDrDebit(apDistributionRecord.getDrDebit() == EJBCommon.TRUE ? EJBCommon.FALSE : EJBCommon.TRUE);
						mDrDetails.setDrAmount(apDistributionRecord.getDrAmount());
						mDrDetails.setDrReversed(EJBCommon.TRUE);
						mDrDetails.setDrCoaAccountNumber(apDistributionRecord.getGlChartOfAccount().getCoaAccountNumber());

						this.addApDrEntry(mDrDetails, apCheck, AD_BRNCH, AD_CMPNY);

					}

					if (CHK_APPRVL_STATUS != null && CHK_APPRVL_STATUS.equals("N/A") && adPreference.getPrfApGlPostingType().equals("AUTO-POST UPON APPROVAL")) {

						apCheck.setChkVoid(EJBCommon.TRUE);
						this.executeApChkPost(apCheck.getChkCode(), details.getChkLastModifiedBy(), AD_BRNCH, AD_CMPNY);

					}

					// set void approval status

					apCheck.setChkVoidApprovalStatus(CHK_APPRVL_STATUS);


				}

				apCheck.setChkVoid(EJBCommon.TRUE);
				apCheck.setChkLastModifiedBy(details.getChkLastModifiedBy());
				apCheck.setChkDateLastModified(details.getChkDateLastModified());

				return apCheck.getChkCode();

			}

			// validate if document number is unique document number is automatic then set next sequence

			try {

				LocalAdBranchDocumentSequenceAssignment adBranchDocumentSequenceAssignment = null;
				LocalAdDocumentSequenceAssignment adDocumentSequenceAssignment = null;

				if (details.getChkCode() == null) {

					try {

						adDocumentSequenceAssignment = adDocumentSequenceAssignmentHome.findByDcName("AP CHECK", AD_CMPNY);

					} catch(FinderException ex) { }

					try {

						adBranchDocumentSequenceAssignment = adBranchDocumentSequenceAssignmentHome.findBdsByDsaCodeAndBrCode(adDocumentSequenceAssignment.getDsaCode(), AD_BRNCH, AD_CMPNY);

					} catch (FinderException ex) { }


					LocalApCheck apExistingCheck = null;

					try {

						apExistingCheck = apCheckHome.findByChkDocumentNumberAndBrCode(
								details.getChkDocumentNumber(), AD_BRNCH, AD_CMPNY);

					} catch (FinderException ex) {
					}

					if (apExistingCheck != null) {

						throw new GlobalDocumentNumberNotUniqueException();

					}

					if (adDocumentSequenceAssignment.getAdDocumentSequence().getDsNumberingType() == 'A' &&
							(details.getChkDocumentNumber() == null || details.getChkDocumentNumber().trim().length() == 0)) {

						while (true) {

							if (adBranchDocumentSequenceAssignment == null || adBranchDocumentSequenceAssignment.getBdsNextSequence() == null) {

								try {

									apCheckHome.findByChkDocumentNumberAndBrCode(adDocumentSequenceAssignment.getDsaNextSequence(), AD_BRNCH, AD_CMPNY);
									adDocumentSequenceAssignment.setDsaNextSequence(EJBCommon.incrementStringNumber(adDocumentSequenceAssignment.getDsaNextSequence()));

								} catch (FinderException ex) {

									details.setChkDocumentNumber(adDocumentSequenceAssignment.getDsaNextSequence());
									adDocumentSequenceAssignment.setDsaNextSequence(EJBCommon.incrementStringNumber(adDocumentSequenceAssignment.getDsaNextSequence()));
									break;

								}

							} else {

								try {

									apCheckHome.findByChkDocumentNumberAndBrCode(adBranchDocumentSequenceAssignment.getBdsNextSequence(), AD_BRNCH, AD_CMPNY);
									adBranchDocumentSequenceAssignment.setBdsNextSequence(EJBCommon.incrementStringNumber(adBranchDocumentSequenceAssignment.getBdsNextSequence()));

								} catch (FinderException ex) {

									details.setChkDocumentNumber(adBranchDocumentSequenceAssignment.getBdsNextSequence());
									adBranchDocumentSequenceAssignment.setBdsNextSequence(EJBCommon.incrementStringNumber(adBranchDocumentSequenceAssignment.getBdsNextSequence()));
									break;

								}

							}

						}

					}

				} else {

					LocalApCheck apExistingCheck = null;

					try {

						apExistingCheck = apCheckHome.findByChkDocumentNumberAndBrCode(
								details.getChkDocumentNumber(), AD_BRNCH, AD_CMPNY);

					} catch (FinderException ex) {
					}

					if (apExistingCheck != null &&
							!apExistingCheck.getChkCode().equals(details.getChkCode())) {

						throw new GlobalDocumentNumberNotUniqueException();

					}

					if (apCheck.getChkDocumentNumber() != details.getChkDocumentNumber() &&
							(details.getChkDocumentNumber() == null || details.getChkDocumentNumber().trim().length() == 0)) {

						details.setChkDocumentNumber(apCheck.getChkDocumentNumber());

					}

				}

			} catch (GlobalDocumentNumberNotUniqueException ex) {

				getSessionContext().setRollbackOnly();
				throw ex;

			} catch (Exception ex) {

				Debug.printStackTrace(ex);
				getSessionContext().setRollbackOnly();
				throw new EJBException(ex.getMessage());

			}

			// validate if check number is unique check is automatic then set next sequence

			if (details.getChkCode() == null) {

				LocalApCheck apExistingCheck = null;

				try {

					apExistingCheck = apCheckHome.findByChkNumberAndBaName(
							details.getChkNumber(), BA_NM, AD_CMPNY);

				} catch (FinderException ex) {
				}

				if (apExistingCheck != null) {

					throw new ApCHKCheckNumberNotUniqueException();

				}

				LocalAdBankAccount adBankAccount = adBankAccountHome.findByBaName(BA_NM, AD_CMPNY);

				if (details.getChkNumber() == null || details.getChkNumber().trim().length() == 0) {

					while (true) {

						try {

							apCheckHome.findByChkNumberAndBaName(adBankAccount.getBaNextCheckNumber(), BA_NM, AD_CMPNY);
							adBankAccount.setBaNextCheckNumber(EJBCommon.incrementStringNumber(adBankAccount.getBaNextCheckNumber()));

						} catch (FinderException ex) {

							details.setChkNumber(adBankAccount.getBaNextCheckNumber());
							adBankAccount.setBaNextCheckNumber(EJBCommon.incrementStringNumber(adBankAccount.getBaNextCheckNumber()));
							break;

						}

					}

				}

			} else {

				LocalApCheck apExistingCheck = null;

				try {

					apExistingCheck = apCheckHome.findByChkNumberAndBaName(
							details.getChkNumber(), BA_NM, AD_CMPNY);

				} catch (FinderException ex) {
				}

				if (apExistingCheck != null &&
						!apExistingCheck.getChkCode().equals(details.getChkCode())) {

					throw new ApCHKCheckNumberNotUniqueException();

				}

				if (apCheck.getChkNumber() != details.getChkNumber() &&
						(details.getChkNumber() == null || details.getChkNumber().trim().length() == 0)) {

					details.setChkNumber(apCheck.getChkNumber());

				}

			}


			// validate if conversion date exists

            try {

                if (details.getChkConversionDate() != null) {


                    LocalGlFunctionalCurrency glValidateFunctionalCurrency = glFunctionalCurrencyHome.findByFcName(FC_NM, AD_CMPNY);
                    LocalGlFunctionalCurrencyRate glFunctionalCurrencyRate =
                        glFunctionalCurrencyRateHome.findByFcCodeAndDate(glValidateFunctionalCurrency.getFcCode(),
                                details.getChkConversionDate(), AD_CMPNY);

                    details.setChkConversionRate(glFunctionalCurrencyRate.getFrXToUsd());

                }

            } catch (FinderException ex) {

                throw new GlobalConversionDateNotExistException();

            }

			// used in checking if check should re-generate distribution records and re-calculate taxes
			boolean isRecalculate = true;

			// create direct check

			if (details.getChkCode() == null) {

				apCheck = apCheckHome.create("DIRECT", details.getChkDescription(),
						details.getChkDate(), details.getChkCheckDate(), details.getChkNumber(), details.getChkDocumentNumber(),
						details.getChkReferenceNumber(), details.getChkInfoType(), details.getChkInfoBioNumber(), details.getChkInfoBioDescription(), details.getChkInfoTypeStatus(), details.getChkInfoRequestStatus(),
						EJBCommon.FALSE, EJBCommon.FALSE, details.getChkInvtNextRunDate(), details.getChkInvtSettlementDate(), details.getChkInvtMaturityDate(), details.getChkInvtBidYield(), details.getChkInvtCouponRate(), details.getChkInvtSettleAmount(), details.getChkInvtFaceValue(), details.getChkInvtPremiumAmount(),
						details.getChkLoan(), details.getChkLoanGenerated(),
						details.getChkConversionDate(), details.getChkConversionRate(),
						0d, 0d, null, null, EJBCommon.FALSE,
						EJBCommon.FALSE, details.getChkCrossCheck(), null, EJBCommon.FALSE, details.getChkCreatedBy(),
						details.getChkDateCreated(), details.getChkLastModifiedBy(),
						details.getChkDateLastModified(), null, null, null, null, EJBCommon.FALSE, null,
						EJBCommon.FALSE, null, EJBCommon.FALSE, EJBCommon.FALSE, details.getChkMemo(), null, AD_BRNCH, AD_CMPNY);

			} else {

				// check if critical fields are changed

				if (!apCheck.getApTaxCode().getTcName().equals(TC_NM) ||
						!apCheck.getApWithholdingTaxCode().getWtcName().equals(WTC_NM) ||
						!apCheck.getApSupplier().getSplSupplierCode().equals(SPL_SPPLR_CODE) ||
						vliList.size() != apCheck.getApVoucherLineItems().size() ||
						!(apCheck.getChkDate().equals(details.getChkDate()))) {

					isRecalculate = true;

				} else if (vliList.size() == apCheck.getApVoucherLineItems().size()) {

					Iterator ilIter = apCheck.getApVoucherLineItems().iterator();
					Iterator ilListIter = vliList.iterator();

					while (ilIter.hasNext()) {

						LocalApVoucherLineItem apVoucherLineItem = (LocalApVoucherLineItem)ilIter.next();
						ApModVoucherLineItemDetails mdetails = (ApModVoucherLineItemDetails)ilListIter.next();

						if (!apVoucherLineItem.getInvItemLocation().getInvItem().getIiName().equals(mdetails.getVliIiName()) ||
								!apVoucherLineItem.getInvItemLocation().getInvLocation().getLocName().equals(mdetails.getVliLocName()) ||
								!apVoucherLineItem.getInvUnitOfMeasure().getUomName().equals(mdetails.getVliUomName()) ||
								apVoucherLineItem.getVliQuantity() != mdetails.getVliQuantity() ||
								apVoucherLineItem.getVliUnitCost() != mdetails.getVliUnitCost() ||
								apVoucherLineItem.getVliTotalDiscount() != mdetails.getVliTotalDiscount()) {

							isRecalculate = true;
							break;

						}

					//	isRecalculate = false;

					}

				} else {

					//isRecalculate = false;

				}

				apCheck.setChkDescription(details.getChkDescription());
                apCheck.setChkInfoType(details.getChkInfoType());
                apCheck.setChkInfoBioNumber(details.getChkInfoBioNumber());
                apCheck.setChkInfoBioDescription(details.getChkInfoBioDescription());
                apCheck.setChkInfoTypeStatus(details.getChkInfoTypeStatus());
                apCheck.setChkInfoRequestStatus(details.getChkInfoRequestStatus());
				apCheck.setChkDate(details.getChkDate());
				apCheck.setChkCheckDate(details.getChkCheckDate());
				apCheck.setChkNumber(details.getChkNumber());
				apCheck.setChkDocumentNumber(details.getChkDocumentNumber());
				apCheck.setChkReferenceNumber(details.getChkReferenceNumber());
				apCheck.setChkConversionDate(details.getChkConversionDate());
				apCheck.setChkConversionRate(details.getChkConversionRate());
				apCheck.setChkBillAmount(details.getChkBillAmount());
				apCheck.setChkAmount(details.getChkAmount());
				apCheck.setChkCrossCheck(details.getChkCrossCheck());
				apCheck.setChkLastModifiedBy(details.getChkLastModifiedBy());
				apCheck.setChkDateLastModified(details.getChkDateLastModified());
				apCheck.setChkReasonForRejection(null);
				apCheck.setChkMemo(details.getChkMemo());
				apCheck.setChkSupplierName(null);

				apCheck.setChkLoan(details.getChkLoan());
				apCheck.setChkLoanGenerated(details.getChkLoanGenerated());

			}

			LocalAdBankAccount adBankAccount = adBankAccountHome.findByBaName(BA_NM, AD_CMPNY);
			apCheck.setAdBankAccount(adBankAccount);

			LocalGlFunctionalCurrency glFunctionalCurrency = glFunctionalCurrencyHome.findByFcName(FC_NM, AD_CMPNY);
			apCheck.setGlFunctionalCurrency(glFunctionalCurrency);

			LocalApSupplier apSupplier = apSupplierHome.findBySplSupplierCode(SPL_SPPLR_CODE, AD_CMPNY);
			apCheck.setApSupplier(apSupplier);

			if(details.getChkSupplierName().length() > 0 && !apSupplier.getSplName().equals(details.getChkSupplierName())) {
				apCheck.setChkSupplierName(details.getChkSupplierName());
			}

			LocalApTaxCode apTaxCode = apTaxCodeHome.findByTcName(TC_NM, AD_CMPNY);
			apCheck.setApTaxCode(apTaxCode);

			LocalApWithholdingTaxCode apWithholdingTaxCode = apWithholdingTaxCodeHome.findByWtcName(WTC_NM, AD_CMPNY);
			apCheck.setApWithholdingTaxCode(apWithholdingTaxCode);

			try{

			LocalApCheckBatch apCheckBatch = apCheckBatchHome.findByDirectCbName(CB_NM, AD_BRNCH, AD_CMPNY);
			apCheck.setApCheckBatch(apCheckBatch);

			}catch (Exception e) {
				// TODO: handle exception
			}


			if (isRecalculate) {

				// remove all voucher line items

				Collection apVoucherLineItems = apCheck.getApVoucherLineItems();

				Iterator i = apVoucherLineItems.iterator();

				while (i.hasNext()) {

					LocalApVoucherLineItem apVoucherLineItem = (LocalApVoucherLineItem)i.next();

					//remove all invTags
					Collection invTags = apVoucherLineItem.getInvTags();

		  	   	    Iterator x = invTags.iterator();

		  	   	    while (x.hasNext()){

		  	   	    	LocalInvTag invTag = (LocalInvTag)x.next();

		  	   	    	x.remove();

		  	   	    	invTag.remove();
		  	   	    }



					i.remove();

					apVoucherLineItem.remove();

				}




				// remove all distribution records

				Collection apDistributionRecords = apCheck.getApDistributionRecords();

				i = apDistributionRecords.iterator();

				while (i.hasNext()) {

					LocalApDistributionRecord apDistributionRecord = (LocalApDistributionRecord)i.next();

					i.remove();

					apDistributionRecord.remove();

				}

				// add new voucher line item and distribution record

				double TOTAL_TAX = 0d;
				double TOTAL_LINE = 0d;

				i = vliList.iterator();

				LocalInvItemLocation invItemLocation = null;

				while (i.hasNext()) {

					ApModVoucherLineItemDetails mVliDetails = (ApModVoucherLineItemDetails) i.next();

					try {

						invItemLocation = invItemLocationHome.findByLocNameAndIiName(mVliDetails.getVliLocName(), mVliDetails.getVliIiName(), AD_CMPNY);

					} catch (FinderException ex) {

						throw new GlobalInvItemLocationNotFoundException(String.valueOf(mVliDetails.getVliLine()));

					}

					//	start date validation
					if (adPreference.getPrfArAllowPriorDate() == EJBCommon.FALSE) {
						Collection invNegTxnCosting = invCostingHome.findNegTxnByGreaterThanCstDateAndIiNameAndLocName(
								apCheck.getChkDate(), invItemLocation.getInvItem().getIiName(),
								invItemLocation.getInvLocation().getLocName(), AD_BRNCH, AD_CMPNY);
						if(!invNegTxnCosting.isEmpty()) throw new GlobalInventoryDateException(invItemLocation.getInvItem().getIiName());
					}

					LocalApVoucherLineItem apVoucherLineItem = this.addApVliEntry(mVliDetails, apCheck, invItemLocation, AD_CMPNY);

					// add inventory distributions

					LocalAdBranchItemLocation adBranchItemLocation = null;

					try {

						adBranchItemLocation = adBranchItemLocationHome.findBilByIlCodeAndBrCode(apVoucherLineItem.getInvItemLocation().getIlCode(), AD_BRNCH, AD_CMPNY);

					} catch(FinderException ex) {

					}

					if (adBranchItemLocation != null) {

						// Use AdBranchItemLocation account
						this.addApDrVliEntry(apCheck.getApDrNextLine(),
								"EXPENSE", EJBCommon.TRUE, apVoucherLineItem.getVliAmount(),
								adBranchItemLocation.getBilCoaGlInventoryAccount(), apCheck, AD_BRNCH, AD_CMPNY);

					} else {

						// Use default account
						this.addApDrVliEntry(apCheck.getApDrNextLine(),
								"EXPENSE", EJBCommon.TRUE, apVoucherLineItem.getVliAmount(),
								apVoucherLineItem.getInvItemLocation().getIlGlCoaInventoryAccount(), apCheck, AD_BRNCH, AD_CMPNY);

					}

					TOTAL_LINE += apVoucherLineItem.getVliAmount();
					TOTAL_TAX += apVoucherLineItem.getVliTaxAmount();

				}


				// add tax distribution if necessary

				if (!apTaxCode.getTcType().equals("NONE") &&
						!apTaxCode.getTcType().equals("EXEMPT")) {

					LocalAdBranchApTaxCode adBranchTaxCode = null;
                    Debug.print("ArInvoiceEntryControllerBean saveArInvIliEntry J");
                    try {
                  	  adBranchTaxCode = adBranchApTaxCodeHome.findBtcByTcCodeAndBrCode(apCheck.getApTaxCode().getTcCode(), AD_BRNCH, AD_CMPNY);

                    } catch(FinderException ex) {

                    }

					this.addApDrVliEntry(apCheck.getApDrNextLine(),
							"TAX", EJBCommon.TRUE, TOTAL_TAX, adBranchTaxCode.getBtcGlCoaTaxCode(),
							apCheck, AD_BRNCH, AD_CMPNY);

				}

				// add wtax distribution if necessary

				double W_TAX_AMOUNT = 0d;

				if (apWithholdingTaxCode.getWtcRate() != 0 && adPreference.getPrfApWTaxRealization().equals("VOUCHER")) {

					W_TAX_AMOUNT = EJBCommon.roundIt(TOTAL_LINE * (apWithholdingTaxCode.getWtcRate() / 100), this.getGlFcPrecisionUnit(AD_CMPNY));

					this.addApDrVliEntry(apCheck.getApDrNextLine(), "W-TAX",
							EJBCommon.FALSE, W_TAX_AMOUNT, apWithholdingTaxCode.getGlChartOfAccount().getCoaCode(),
							apCheck, AD_BRNCH, AD_CMPNY);

				}

				// add cash distribution
		      	  LocalAdBranchBankAccount adBranchBankAccount = null;
	              Debug.print("ArInvoiceEntryControllerBean saveArInvIliEntry J");
	              try {
	                  adBranchBankAccount = adBranchBankAccountHome.findBbaByBaCodeAndBrCode(apCheck.getAdBankAccount().getBaCode(), AD_BRNCH, AD_CMPNY);

	              } catch(FinderException ex) {

	              }

				// add payable distribution

				this.addApDrVliEntry(apCheck.getApDrNextLine(), "CASH",
						EJBCommon.FALSE, TOTAL_LINE + TOTAL_TAX - W_TAX_AMOUNT,
						adBranchBankAccount.getBbaGlCoaCashAccount(),
						apCheck, AD_BRNCH, AD_CMPNY);

				// set voucher amount due

				apCheck.setChkAmount(TOTAL_LINE + TOTAL_TAX - W_TAX_AMOUNT);

			} else {

				Iterator i = vliList.iterator();

				LocalInvItemLocation invItemLocation = null;

				while (i.hasNext()) {

					ApModVoucherLineItemDetails mVliDetails = (ApModVoucherLineItemDetails) i.next();

					try {

						invItemLocation = invItemLocationHome.findByLocNameAndIiName(mVliDetails.getVliLocName(), mVliDetails.getVliIiName(), AD_CMPNY);

					} catch (FinderException ex) {

						throw new GlobalInvItemLocationNotFoundException(String.valueOf(mVliDetails.getVliLine()));

					}


				//	LocalApVoucherLineItem apVoucherLineItem = this.addApVliEntry(mVliDetails, apCheck, invItemLocation, AD_CMPNY);


		  	   	    }


					//	start date validation
					if (adPreference.getPrfArAllowPriorDate() == EJBCommon.FALSE) {
						Collection invNegTxnCosting = invCostingHome.findNegTxnByGreaterThanCstDateAndIiNameAndLocName(
								apCheck.getChkDate(), invItemLocation.getInvItem().getIiName(),
								invItemLocation.getInvLocation().getLocName(), AD_BRNCH, AD_CMPNY);
						if(!invNegTxnCosting.isEmpty()) throw new GlobalInventoryDateException(invItemLocation.getInvItem().getIiName());
					}



			}

			// generate approval status

			String CHK_APPRVL_STATUS = null;

			if (!isDraft) {

				LocalAdApproval adApproval = adApprovalHome.findByAprAdCompany(AD_CMPNY);

				// check if ap check approval is enabled

				if (adApproval.getAprEnableApCheck() == EJBCommon.FALSE) {

					CHK_APPRVL_STATUS = "N/A";

				} else {

					// check if check is self approved

					LocalAdAmountLimit adAmountLimit = null;

					try {

						adAmountLimit = adAmountLimitHome.findByAdcTypeAndAuTypeAndUsrName("AP CHECK", "REQUESTER", details.getChkLastModifiedBy(), AD_CMPNY);

					} catch (FinderException ex) {

						throw new GlobalNoApprovalRequesterFoundException();

					}

					if (apCheck.getChkAmount() <= adAmountLimit.getCalAmountLimit()) {

						CHK_APPRVL_STATUS = "N/A";

					} else {

						// for approval, create approval queue

						Collection adAmountLimits = adAmountLimitHome.findByAdcTypeAndGreaterThanCalAmountLimit("AP CHECK", adAmountLimit.getCalAmountLimit(), AD_CMPNY);

						if (adAmountLimits.isEmpty()) {

							Collection adApprovalUsers = adApprovalUserHome.findByAuTypeAndCalCode("APPROVER", adAmountLimit.getCalCode(), AD_CMPNY);

							if (adApprovalUsers.isEmpty()) {

								throw new GlobalNoApprovalApproverFoundException();

							}

							Iterator j = adApprovalUsers.iterator();

							while (j.hasNext()) {

								LocalAdApprovalUser adApprovalUser = (LocalAdApprovalUser)j.next();

								LocalAdApprovalQueue adApprovalQueue = adApprovalQueueHome.create(EJBCommon.TRUE, "AP CHECK", apCheck.getChkCode(),
										apCheck.getChkDocumentNumber(), apCheck.getChkDate(), adAmountLimit.getCalAndOr(), adApprovalUser.getAuOr(), AD_BRNCH, AD_CMPNY);

								adApprovalUser.getAdUser().addAdApprovalQueue(adApprovalQueue);

							}

						} else {

							boolean isApprovalUsersFound = false;

							Iterator i = adAmountLimits.iterator();

							while (i.hasNext()) {

								LocalAdAmountLimit adNextAmountLimit = (LocalAdAmountLimit)i.next();

								if (apCheck.getChkAmount() <= adNextAmountLimit.getCalAmountLimit()) {

									Collection adApprovalUsers = adApprovalUserHome.findByAuTypeAndCalCode("APPROVER", adAmountLimit.getCalCode(), AD_CMPNY);

									Iterator j = adApprovalUsers.iterator();

									while (j.hasNext()) {

										isApprovalUsersFound = true;

										LocalAdApprovalUser adApprovalUser = (LocalAdApprovalUser)j.next();

										LocalAdApprovalQueue adApprovalQueue = adApprovalQueueHome.create(EJBCommon.TRUE, "AP CHECK", apCheck.getChkCode(),
												apCheck.getChkDocumentNumber(), apCheck.getChkDate(), adAmountLimit.getCalAndOr(), adApprovalUser.getAuOr(), AD_BRNCH, AD_CMPNY);

										adApprovalUser.getAdUser().addAdApprovalQueue(adApprovalQueue);

									}

									break;

								} else if (!i.hasNext()) {

									Collection adApprovalUsers = adApprovalUserHome.findByAuTypeAndCalCode("APPROVER", adNextAmountLimit.getCalCode(), AD_CMPNY);

									Iterator j = adApprovalUsers.iterator();

									while (j.hasNext()) {

										isApprovalUsersFound = true;

										LocalAdApprovalUser adApprovalUser = (LocalAdApprovalUser)j.next();

										LocalAdApprovalQueue adApprovalQueue = adApprovalQueueHome.create(EJBCommon.TRUE, "AP CHECK", apCheck.getChkCode(),
												apCheck.getChkDocumentNumber(), apCheck.getChkDate(), adNextAmountLimit.getCalAndOr(), adApprovalUser.getAuOr(), AD_BRNCH, AD_CMPNY);

										adApprovalUser.getAdUser().addAdApprovalQueue(adApprovalQueue);

									}

									break;

								}

								adAmountLimit = adNextAmountLimit;

							}

							if (!isApprovalUsersFound) {

								throw new GlobalNoApprovalApproverFoundException();

							}

						}

						CHK_APPRVL_STATUS = "PENDING";
					}
				}
			}


			if (CHK_APPRVL_STATUS != null && CHK_APPRVL_STATUS.equals("N/A") && adPreference.getPrfApGlPostingType().equals("AUTO-POST UPON APPROVAL")) {

				this.executeApChkPost(apCheck.getChkCode(), details.getChkLastModifiedBy(), AD_BRNCH, AD_CMPNY);

			}

			// set check approval status

			apCheck.setChkApprovalStatus(CHK_APPRVL_STATUS);

			return apCheck.getChkCode();

		} catch (GlobalRecordAlreadyDeletedException ex) {

			getSessionContext().setRollbackOnly();
			throw ex;

		} catch (GlobalDocumentNumberNotUniqueException ex) {

			getSessionContext().setRollbackOnly();
			throw ex;

		} catch (ApCHKCheckNumberNotUniqueException ex) {

			getSessionContext().setRollbackOnly();
			throw ex;

		} catch (GlobalConversionDateNotExistException ex) {

			getSessionContext().setRollbackOnly();
			throw ex;

		} catch (GlobalTransactionAlreadyApprovedException ex) {

			getSessionContext().setRollbackOnly();
			throw ex;

		} catch (GlobalTransactionAlreadyPendingException ex) {

			getSessionContext().setRollbackOnly();
			throw ex;

		} catch (GlobalTransactionAlreadyPostedException ex) {

			getSessionContext().setRollbackOnly();
			throw ex;

		} catch (GlobalTransactionAlreadyVoidException ex) {

			getSessionContext().setRollbackOnly();
			throw ex;

		} catch (GlobalNoApprovalRequesterFoundException ex) {

			getSessionContext().setRollbackOnly();
			throw ex;

		} catch (GlobalNoApprovalApproverFoundException ex) {

			getSessionContext().setRollbackOnly();
			throw ex;

		} catch (GlobalInvItemLocationNotFoundException ex) {

			getSessionContext().setRollbackOnly();
			throw ex;

		} catch (GlobalInventoryDateException ex) {

			getSessionContext().setRollbackOnly();
			throw ex;

		} catch (GlobalBranchAccountNumberInvalidException ex) {

			getSessionContext().setRollbackOnly();
			throw ex;

        } catch (AdPRFCoaGlVarianceAccountNotFoundException ex){

            getSessionContext().setRollbackOnly();
            throw ex;

		} catch (Exception ex) {

			Debug.printStackTrace(ex);
			getSessionContext().setRollbackOnly();
			throw new EJBException(ex.getMessage());

		}


	}

	/**
	 * @ejb:interface-method view-type="remote"
	 **/
	public void deleteApChkEntry(Integer CHK_CODE, String AD_USR, Integer AD_CMPNY) throws
	GlobalRecordAlreadyDeletedException {

		Debug.print("ApDirectCheckEntryControllerBean deleteApChkEntry");

		LocalApCheckHome apCheckHome = null;
		LocalAdApprovalQueueHome adApprovalQueueHome = null;
		LocalAdDeleteAuditTrailHome adDeleteAuditTrailHome = null;

		// Initialize EJB Home

		try {

			apCheckHome = (LocalApCheckHome)EJBHomeFactory.
			lookUpLocalHome(LocalApCheckHome.JNDI_NAME, LocalApCheckHome.class);
			adApprovalQueueHome = (LocalAdApprovalQueueHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdApprovalQueueHome.JNDI_NAME, LocalAdApprovalQueueHome.class);
			adDeleteAuditTrailHome = (LocalAdDeleteAuditTrailHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdDeleteAuditTrailHome.JNDI_NAME, LocalAdDeleteAuditTrailHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}

		try {

			LocalApCheck apCheck = apCheckHome.findByPrimaryKey(CHK_CODE);

			if (apCheck.getChkApprovalStatus() != null && apCheck.getChkApprovalStatus().equals("PENDING")) {

				Collection adApprovalQueues = adApprovalQueueHome.findByAqDocumentAndAqDocumentCode("AP CHECK", apCheck.getChkCode(), AD_CMPNY);

				Iterator i = adApprovalQueues.iterator();

				while(i.hasNext()) {

					LocalAdApprovalQueue adApprovalQueue = (LocalAdApprovalQueue)i.next();

					adApprovalQueue.remove();

				}

			}

			adDeleteAuditTrailHome.create("AP CHECK", apCheck.getChkDate(), apCheck.getChkDocumentNumber(), apCheck.getChkReferenceNumber(),
 					apCheck.getChkAmount(), AD_USR, new Date(), AD_CMPNY);

			apCheck.remove();

		} catch (FinderException ex) {

			getSessionContext().setRollbackOnly();
			throw new GlobalRecordAlreadyDeletedException();

		} catch (Exception ex) {

			Debug.printStackTrace(ex);
			getSessionContext().setRollbackOnly();
			throw new EJBException(ex.getMessage());

		}

	}

	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public short getGlFcPrecisionUnit(Integer AD_CMPNY) {

		Debug.print("ApDirectCheckEntryControllerBean getGlFcPrecisionUnit");


		LocalAdCompanyHome adCompanyHome = null;


		// Initialize EJB Home

		try {

			adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}

		try {

			LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);

			return  adCompany.getGlFunctionalCurrency().getFcPrecision();

		} catch (Exception ex) {

			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());

		}

	}


	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public ArrayList getAdApprovalNotifiedUsersByChkCode(Integer CHK_CODE, Integer AD_CMPNY) {

		Debug.print("ApDirectCheckEntryControllerBean getAdApprovalNotifiedUsersByChkCode");


		LocalAdApprovalQueueHome adApprovalQueueHome = null;
		LocalApCheckHome apCheckHome = null;

		ArrayList list = new ArrayList();


		// Initialize EJB Home

		try {

			adApprovalQueueHome = (LocalAdApprovalQueueHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdApprovalQueueHome.JNDI_NAME, LocalAdApprovalQueueHome.class);
			apCheckHome = (LocalApCheckHome)EJBHomeFactory.
			lookUpLocalHome(LocalApCheckHome.JNDI_NAME, LocalApCheckHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}

		try {

			LocalApCheck apCheck = apCheckHome.findByPrimaryKey(CHK_CODE);

			if (apCheck.getChkVoid() == EJBCommon.FALSE && apCheck.getChkPosted() == EJBCommon.TRUE) {

				list.add("DOCUMENT POSTED");
				return list;

			} else if (apCheck.getChkVoid() == EJBCommon.TRUE && apCheck.getChkVoidPosted() == EJBCommon.TRUE) {

				list.add("DOCUMENT POSTED");
				return list;

			}

			Collection adApprovalQueues = adApprovalQueueHome.findByAqDocumentAndAqDocumentCode("AP CHECK", CHK_CODE, AD_CMPNY);

			Iterator i = adApprovalQueues.iterator();

			while(i.hasNext()) {

				LocalAdApprovalQueue adApprovalQueue = (LocalAdApprovalQueue)i.next();

				list.add(adApprovalQueue.getAdUser().getUsrDescription());

			}

			return list;

		} catch (Exception ex) {

			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());

		}

	}



	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public byte getAdPrfEnableApCheckBatch(Integer AD_CMPNY) {

		Debug.print("ApDirectCheckEntryControllerBean getAdPrfEnableApCheckBatch");

		LocalAdPreferenceHome adPreferenceHome = null;


		// Initialize EJB Home

		try {

			adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}


		try {

			LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);

			return adPreference.getPrfEnableApCheckBatch();

		} catch (Exception ex) {

			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());

		}

	}


	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public ArrayList getApOpenCbAll(String DPRTMNT, Integer AD_BRNCH, Integer AD_CMPNY) {

		Debug.print("ApDirectCheckEntryControllerBean getApOpenCbAll");

		LocalApCheckBatchHome apCheckBatchHome = null;

		ArrayList list = new ArrayList();

		// Initialize EJB Home

		try {

			apCheckBatchHome = (LocalApCheckBatchHome)EJBHomeFactory.
			lookUpLocalHome(LocalApCheckBatchHome.JNDI_NAME, LocalApCheckBatchHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}

		try {

			Collection apCheckBatches = apCheckBatchHome.findOpenCbByCbType("DIRECT", AD_BRNCH, AD_CMPNY);
			if(DPRTMNT.equals("") || DPRTMNT.equals("default") || DPRTMNT.equals("NO RECORD FOUND")){
				System.out.println("------------>");
				apCheckBatches = apCheckBatchHome.findOpenCbByCbType("DIRECT", AD_BRNCH, AD_CMPNY);

			} else {
				System.out.println("------------>else");
				apCheckBatches = apCheckBatchHome.findOpenCbByCbTypeDepartment("DIRECT",DPRTMNT, AD_BRNCH, AD_CMPNY);
			}
			Iterator i = apCheckBatches.iterator();

			while (i.hasNext()) {

				LocalApCheckBatch apCheckBatch = (LocalApCheckBatch)i.next();

				list.add(apCheckBatch.getCbName());

			}

			return list;

		} catch (Exception ex) {

			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());

		}

	}

	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public ArrayList getInvLocAll(Integer AD_CMPNY) {

		Debug.print("ApDirectCheckEntryControllerBean getInvLocAll");

		LocalInvLocationHome invLocationHome = null;
		Collection invLocations = null;
		ArrayList list = new ArrayList();

		// Initialize EJB Home

		try {

			invLocationHome = (LocalInvLocationHome)EJBHomeFactory.
			lookUpLocalHome(LocalInvLocationHome.JNDI_NAME, LocalInvLocationHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}

		try {

			invLocations = invLocationHome.findLocAll(AD_CMPNY);

			if (invLocations.isEmpty()) {

				return null;

			}

			Iterator i = invLocations.iterator();

			while (i.hasNext()) {

				LocalInvLocation invLocation = (LocalInvLocation)i.next();
				String details = invLocation.getLocName();

				list.add(details);

			}

			return list;

		} catch (Exception ex) {

			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());

		}

	}

	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public ArrayList getInvUomByIiName(String II_NM, Integer AD_CMPNY) {

		Debug.print("ApDirectCheckEntryControllerBean getInvUomByIiName");

		LocalInvUnitOfMeasureHome invUnitOfMeasureHome = null;
		LocalInvItemHome invItemHome = null;
		ArrayList list = new ArrayList();

		// Initialize EJB Home

		try {

			invUnitOfMeasureHome = (LocalInvUnitOfMeasureHome)EJBHomeFactory.
			lookUpLocalHome(LocalInvUnitOfMeasureHome.JNDI_NAME, LocalInvUnitOfMeasureHome.class);
			invItemHome = (LocalInvItemHome)EJBHomeFactory.
			lookUpLocalHome(LocalInvItemHome.JNDI_NAME, LocalInvItemHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());
		}

		try {

			LocalInvItem invItem = null;
			LocalInvUnitOfMeasure invItemUnitOfMeasure = null;

			invItem = invItemHome.findByIiName(II_NM, AD_CMPNY);
			invItemUnitOfMeasure = invItem.getInvUnitOfMeasure();

			Collection invUnitOfMeasures = null;
			Iterator i = invUnitOfMeasureHome.findByUomAdLvClass(
					invItemUnitOfMeasure.getUomAdLvClass(), AD_CMPNY).iterator();
			while (i.hasNext()) {

				LocalInvUnitOfMeasure invUnitOfMeasure = (LocalInvUnitOfMeasure) i.next();
				InvModUnitOfMeasureDetails details = new InvModUnitOfMeasureDetails();
				details.setUomName(invUnitOfMeasure.getUomName());

				if (invUnitOfMeasure.getUomName().equals(invItemUnitOfMeasure.getUomName())) {

					details.setDefault(true);

				}

				list.add(details);

			}

			return list;

		} catch (Exception ex) {

			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());

		}

	}

	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public double getInvIiUnitCostByIiNameAndUomName(String II_NM, String UOM_NM, Integer AD_CMPNY) {

		Debug.print("ApDirectCheckEntryControllerBean getInvIiUnitCostByIiNameAndUomName");

		LocalInvItemHome invItemHome = null;
		LocalInvUnitOfMeasureConversionHome invUnitOfMeasureConversionHome = null;

		// Initialize EJB Home

		try {

			invItemHome = (LocalInvItemHome)EJBHomeFactory.
				lookUpLocalHome(LocalInvItemHome.JNDI_NAME, LocalInvItemHome.class);
			invUnitOfMeasureConversionHome = (LocalInvUnitOfMeasureConversionHome)EJBHomeFactory.
    			lookUpLocalHome(LocalInvUnitOfMeasureConversionHome.JNDI_NAME, LocalInvUnitOfMeasureConversionHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}

		try {

			LocalInvItem invItem = invItemHome.findByIiName(II_NM, AD_CMPNY);

			LocalInvUnitOfMeasureConversion invUnitOfMeasureConversion = invUnitOfMeasureConversionHome.findUmcByIiNameAndUomName(II_NM, UOM_NM, AD_CMPNY);
        	LocalInvUnitOfMeasureConversion invDefaultUomConversion = invUnitOfMeasureConversionHome.findUmcByIiNameAndUomName(II_NM, invItem.getInvUnitOfMeasure().getUomName(), AD_CMPNY);

        	return EJBCommon.roundIt(invItem.getIiUnitCost() * invDefaultUomConversion.getUmcConversionFactor() / invUnitOfMeasureConversion.getUmcConversionFactor(), this.getGlFcPrecisionUnit(AD_CMPNY));

		} catch (Exception ex) {

			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());

		}

	}

	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public short getInvGpQuantityPrecisionUnit(Integer AD_CMPNY) {

		Debug.print("ApDirectCheckEntryControllerBean getInvGpQuantityPrecisionUnit");

		LocalAdPreferenceHome adPreferenceHome = null;

		// Initialize EJB Home

		try {

			adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}


		try {

			LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);

			return adPreference.getPrfInvQuantityPrecisionUnit();

		} catch (Exception ex) {

			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());

		}

	}

	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public byte getAdPrfApUseSupplierPulldown(Integer AD_CMPNY) {

		Debug.print("ApDirectCheckEntryControllerBean getAdPrfApUseSupplierPulldown");

		LocalAdPreferenceHome adPreferenceHome = null;


		// Initialize EJB Home

		try {

			adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}


		try {

			LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);

			return adPreference.getPrfApUseSupplierPulldown();

		} catch (Exception ex) {

			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());

		}

	}

	/**
 	 * @ejb:interface-method view-type="remote"
 	 * @jboss:method-attributes read-only="true"
 	 **/
 	public ArrayList getInvLitByCstLitName(String CST_LIT_NAME, Integer AD_CMPNY)
 	    throws GlobalNoRecordFoundException {

 	    Debug.print("ApDirectCheckEntryControllerBean getInvLitByCstLitName");

 	    LocalInvLineItemTemplateHome invLineItemTemplateHome = null;

 	    // Initialize EJB Home

 	    try {

 	    	invLineItemTemplateHome = (LocalInvLineItemTemplateHome)EJBHomeFactory.
 	            lookUpLocalHome(LocalInvLineItemTemplateHome.JNDI_NAME, LocalInvLineItemTemplateHome.class);

 	    } catch (NamingException ex) {

 	        throw new EJBException(ex.getMessage());

 	    }

 	    try {

 	    	LocalInvLineItemTemplate invLineItemTemplate = null;


 	    	try {

 	    		invLineItemTemplate = invLineItemTemplateHome.findByLitName(CST_LIT_NAME, AD_CMPNY);

 	    	} catch (FinderException ex) {

 	    		throw new GlobalNoRecordFoundException();

 	    	}

 	    	ArrayList list = new ArrayList();

 	    	// get line items if any

 	    	Collection invLineItems = invLineItemTemplate.getInvLineItems();

 	    	if (!invLineItems.isEmpty()) {

 	    		Iterator i = invLineItems.iterator();

 	    		while (i.hasNext()) {

 	    			LocalInvLineItem invLineItem = (LocalInvLineItem) i.next();

 	    			InvModLineItemDetails liDetails = new InvModLineItemDetails();

 	    			liDetails.setLiCode(invLineItem.getLiCode());
 	    			liDetails.setLiLine(invLineItem.getLiLine());
 	    			liDetails.setLiIiName(invLineItem.getInvItemLocation().getInvItem().getIiName());
 	    			liDetails.setLiLocName(invLineItem.getInvItemLocation().getInvLocation().getLocName());
 	    			liDetails.setLiUomName(invLineItem.getInvUnitOfMeasure().getUomName());
 	    			liDetails.setLiIiDescription(invLineItem.getInvItemLocation().getInvItem().getIiDescription());

 	    			list.add(liDetails);

 	    		}

 	    	}

 			return list;

 	    } catch (GlobalNoRecordFoundException ex) {

 	    	throw ex;

 	    } catch (Exception ex) {

 	    	Debug.printStackTrace(ex);
 	    	throw new EJBException(ex.getMessage());

 	    }

 	}

 	/**
 	 * @ejb:interface-method view-type="remote"
 	 * @jboss:method-attributes read-only="true"
 	 **/
 	public double getFrRateByFrNameAndFrDate(String FC_NM, Date CONVERSION_DATE, Integer AD_CMPNY)
 	throws GlobalConversionDateNotExistException {

 		Debug.print("ApDirectCheckEntryControllerBean getFrRateByFrNameAndFrDate");

 		LocalGlFunctionalCurrencyRateHome glFunctionalCurrencyRateHome = null;
 		LocalGlFunctionalCurrencyHome glFunctionalCurrencyHome = null;
 		LocalAdCompanyHome adCompanyHome = null;

 		// Initialize EJB Home

 		try {

 			glFunctionalCurrencyRateHome = (LocalGlFunctionalCurrencyRateHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlFunctionalCurrencyRateHome.JNDI_NAME, LocalGlFunctionalCurrencyRateHome.class);
 			glFunctionalCurrencyHome = (LocalGlFunctionalCurrencyHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlFunctionalCurrencyHome.JNDI_NAME, LocalGlFunctionalCurrencyHome.class);
 			adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);

 		} catch (NamingException ex) {

 			throw new EJBException(ex.getMessage());

 		}

 		try {

 			LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
 			LocalGlFunctionalCurrency glFunctionalCurrency = glFunctionalCurrencyHome.findByFcName(FC_NM, AD_CMPNY);

 			double CONVERSION_RATE = 1;

 			// Get functional currency rate

 			if (!FC_NM.equals("USD")) {

 				LocalGlFunctionalCurrencyRate glFunctionalCurrencyRate =
 					glFunctionalCurrencyRateHome.findByFcCodeAndDate(glFunctionalCurrency.getFcCode(),
 							CONVERSION_DATE, AD_CMPNY);

 				CONVERSION_RATE = glFunctionalCurrencyRate.getFrXToUsd();

 			}

 			// Get set of book functional currency rate if necessary

 			if (!adCompany.getGlFunctionalCurrency().getFcName().equals("USD")) {

 				LocalGlFunctionalCurrencyRate glCompanyFunctionalCurrencyRate =
 					glFunctionalCurrencyRateHome.findByFcCodeAndDate(adCompany.getGlFunctionalCurrency(). getFcCode(),
 							CONVERSION_DATE, AD_CMPNY);

 				CONVERSION_RATE = CONVERSION_RATE / glCompanyFunctionalCurrencyRate.getFrXToUsd();

 			}

 			return CONVERSION_RATE;

 		} catch (FinderException ex) {

 			getSessionContext().setRollbackOnly();
 			throw new GlobalConversionDateNotExistException();

 		} catch (Exception ex) {

 			Debug.printStackTrace(ex);
 			throw new EJBException(ex.getMessage());

 		}

 	}

	// private methods

	private void addApDrEntry(ApModDistributionRecordDetails mdetails, LocalApCheck apCheck, Integer AD_BRNCH, Integer AD_CMPNY)
	throws GlobalBranchAccountNumberInvalidException {

		Debug.print("ApDirectCheckEntryControllerBean addApDrEntry");

		LocalApDistributionRecordHome apDistributionRecordHome = null;
		LocalGlChartOfAccountHome glChartOfAccountHome = null;
		LocalAdCompanyHome adCompanyHome = null;


		// Initialize EJB Home

		try {

			apDistributionRecordHome = (LocalApDistributionRecordHome)EJBHomeFactory.
			lookUpLocalHome(LocalApDistributionRecordHome.JNDI_NAME, LocalApDistributionRecordHome.class);
			glChartOfAccountHome = (LocalGlChartOfAccountHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlChartOfAccountHome.JNDI_NAME, LocalGlChartOfAccountHome.class);
			adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);


		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}

		try {

			// get company

			LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);

			// validate if coa exists

			LocalGlChartOfAccount glChartOfAccount = null;

			try {

				glChartOfAccount = glChartOfAccountHome.findByCoaAccountNumberAndBranchCode(mdetails.getDrCoaAccountNumber(), AD_BRNCH, AD_CMPNY);

				if (glChartOfAccount.getCoaEnable() == EJBCommon.FALSE)
					throw new GlobalBranchAccountNumberInvalidException();

			} catch (FinderException ex) {

				throw new GlobalBranchAccountNumberInvalidException();

			}

			// create distribution record

			LocalApDistributionRecord apDistributionRecord = apDistributionRecordHome.create(
					mdetails.getDrLine(),
					mdetails.getDrClass(),
					EJBCommon.roundIt(mdetails.getDrAmount(), adCompany.getGlFunctionalCurrency().getFcPrecision()),
					mdetails.getDrDebit(), EJBCommon.FALSE, mdetails.getDrReversed(), AD_CMPNY);

			//apCheck.addApDistributionRecord(apDistributionRecord);
			apDistributionRecord.setApCheck(apCheck);
			//glChartOfAccount.addApDistributionRecord(apDistributionRecord);
			apDistributionRecord.setGlChartOfAccount(glChartOfAccount);

		} catch(GlobalBranchAccountNumberInvalidException ex) {

			throw ex;

		} catch (Exception ex) {

			Debug.printStackTrace(ex);
			getSessionContext().setRollbackOnly();
			throw new EJBException(ex.getMessage());

		}

	}

	private void addApDrVliEntry(short DR_LN, String DR_CLSS, byte DR_DBT, double DR_AMNT, Integer COA_CODE, LocalApCheck apCheck, Integer AD_BRNCH, Integer AD_CMPNY) throws
	GlobalBranchAccountNumberInvalidException {

		Debug.print("ApDirectCheckEntryControllerBean addApDrVliEntry");

		LocalApDistributionRecordHome apDistributionRecordHome = null;
		LocalGlChartOfAccountHome glChartOfAccountHome = null;
		LocalAdCompanyHome adCompanyHome = null;


		// Initialize EJB Home

		try {

			apDistributionRecordHome = (LocalApDistributionRecordHome)EJBHomeFactory.
			lookUpLocalHome(LocalApDistributionRecordHome.JNDI_NAME, LocalApDistributionRecordHome.class);
			glChartOfAccountHome = (LocalGlChartOfAccountHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlChartOfAccountHome.JNDI_NAME, LocalGlChartOfAccountHome.class);
			adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}

		try {

			// get company

			LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);

			LocalGlChartOfAccount glChartOfAccount = null;

			try {

				glChartOfAccount = glChartOfAccountHome.findByCoaCodeAndBranchCode(COA_CODE, AD_BRNCH, AD_CMPNY);

				if (glChartOfAccount.getCoaEnable() == EJBCommon.FALSE)
					throw new GlobalBranchAccountNumberInvalidException();

			} catch (FinderException ex) {

				throw new GlobalBranchAccountNumberInvalidException();

			}

			// create distribution record

			LocalApDistributionRecord apDistributionRecord = apDistributionRecordHome.create(
					DR_LN, DR_CLSS, EJBCommon.roundIt(DR_AMNT, adCompany.getGlFunctionalCurrency().getFcPrecision()),
					DR_DBT, EJBCommon.FALSE, EJBCommon.FALSE, AD_CMPNY);

			//apCheck.addApDistributionRecord(apDistributionRecord);
			apDistributionRecord.setApCheck(apCheck);
			//glChartOfAccount.addApDistributionRecord(apDistributionRecord);
			apDistributionRecord.setGlChartOfAccount(glChartOfAccount);

		} catch(GlobalBranchAccountNumberInvalidException ex) {

			throw ex;

		} catch (Exception ex) {

			Debug.printStackTrace(ex);
			getSessionContext().setRollbackOnly();
			throw new EJBException(ex.getMessage());

		}

	}

	private LocalApVoucherLineItem addApVliEntry(ApModVoucherLineItemDetails mdetails, LocalApCheck apCheck, LocalInvItemLocation invItemLocation, Integer AD_CMPNY) {

		Debug.print("ApDirectCheckEntryControllerBean addApVliEntry");

		LocalAdUserHome adUserHome = null;
		LocalApVoucherLineItemHome apVoucherLineItemHome = null;
		LocalInvItemLocationHome invItemLocationHome = null;
		LocalInvUnitOfMeasureHome invUnitOfMeasureHome = null;
		LocalInvTagHome invTagHome = null;

		// Initialize EJB Home

		try {

			adUserHome = (LocalAdUserHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdUserHome.JNDI_NAME, LocalAdUserHome.class);
			apVoucherLineItemHome = (LocalApVoucherLineItemHome)EJBHomeFactory.
			lookUpLocalHome(LocalApVoucherLineItemHome.JNDI_NAME, LocalApVoucherLineItemHome.class);
			invItemLocationHome = (LocalInvItemLocationHome)EJBHomeFactory.
			lookUpLocalHome(LocalInvItemLocationHome.JNDI_NAME, LocalInvItemLocationHome.class);
			invUnitOfMeasureHome = (LocalInvUnitOfMeasureHome)EJBHomeFactory.
			lookUpLocalHome(LocalInvUnitOfMeasureHome.JNDI_NAME, LocalInvUnitOfMeasureHome.class);
			invTagHome = (LocalInvTagHome)EJBHomeFactory.
			lookUpLocalHome(LocalInvTagHome.JNDI_NAME, LocalInvTagHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}

		try {

			short precisionUnit = this.getGlFcPrecisionUnit(AD_CMPNY);

			double VLI_AMNT = 0d;
			double VLI_TAX_AMNT = 0d;

			// calculate net amount

			LocalApTaxCode apTaxCode = apCheck.getApTaxCode();

			if (apTaxCode.getTcType().equals("INCLUSIVE")) {

				VLI_AMNT = EJBCommon.roundIt(mdetails.getVliAmount() / (1 + (apTaxCode.getTcRate() / 100)), precisionUnit);

			} else {

				// tax exclusive, none, zero rated or exempt

				VLI_AMNT = mdetails.getVliAmount();

			}

			// calculate tax

			if (!apTaxCode.getTcType().equals("NONE") &&
					!apTaxCode.getTcType().equals("EXEMPT")) {


				if (apTaxCode.getTcType().equals("INCLUSIVE")) {

					VLI_TAX_AMNT = EJBCommon.roundIt(mdetails.getVliAmount() - VLI_AMNT, precisionUnit);

				} else if (apTaxCode.getTcType().equals("EXCLUSIVE")) {

					VLI_TAX_AMNT = EJBCommon.roundIt(mdetails.getVliAmount() * apTaxCode.getTcRate() / 100, precisionUnit);

				} else {

					// tax none zero-rated or exempt

				}

			}

			LocalApVoucherLineItem apVoucherLineItem = apVoucherLineItemHome.create(
					mdetails.getVliLine(), mdetails.getVliQuantity(), mdetails.getVliUnitCost(),
					VLI_AMNT, VLI_TAX_AMNT, mdetails.getVliDiscount1(),  mdetails.getVliDiscount2(),
					 mdetails.getVliDiscount3(),  mdetails.getVliDiscount4(),  mdetails.getVliTotalDiscount(),
					 null, null, null, mdetails.getVliTax(),
					 AD_CMPNY);

			//apCheck.addApVoucherLineItem(apVoucherLineItem);
			apVoucherLineItem.setApCheck(apCheck);

			//invItemLocation.addApVoucherLineItem(apVoucherLineItem);
			apVoucherLineItem.setInvItemLocation(invItemLocation);

			LocalInvUnitOfMeasure invUnitOfMeasure = invUnitOfMeasureHome.findByUomName(mdetails.getVliUomName(), AD_CMPNY);
			//invUnitOfMeasure.addApVoucherLineItem(apVoucherLineItem);
			apVoucherLineItem.setInvUnitOfMeasure(invUnitOfMeasure);

			apVoucherLineItem.setVliMisc(mdetails.getVliMisc());

			if(mdetails.getIsTraceMic()) {

				this.createInvTagList(apVoucherLineItem, mdetails.getTagList(), AD_CMPNY);
				/*
				try {
					System.out.println("aabot?");
      	  	    	//Iterator t = apPurchaseOrderLine.getInvTag().iterator();
	      	  	    Iterator t = mdetails.getTagList().iterator();

	      	  	    LocalInvTag invTag  = null;
	      	  	    System.out.println("umabot?");
	      	  	    while (t.hasNext()){
	      	  	    	InvModTagListDetails tgLstDetails = (InvModTagListDetails)t.next();
	      	  	    	System.out.println(tgLstDetails.getTgCustodian() + "<== custodian");
	      	  	    	System.out.println(tgLstDetails.getTgSpecs() + "<== specs");
	      	  	    	System.out.println(tgLstDetails.getTgPropertyCode() + "<== propertyCode");
	      	  	    	System.out.println(tgLstDetails.getTgExpiryDate() + "<== expiryDate");
	      	  	    	System.out.println(tgLstDetails.getTgSerialNumber() + "<== serial number");

	      	  	    	if (tgLstDetails.getTgCode()==null){
	      	  	    		System.out.println("ngcreate ba?");
		      	  	    	invTag = invTagHome.create(tgLstDetails.getTgPropertyCode(),
		      	  	    			tgLstDetails.getTgSerialNumber(),null,tgLstDetails.getTgExpiryDate(),
		      	  	    			tgLstDetails.getTgSpecs(), AD_CMPNY, tgLstDetails.getTgTransactionDate(),
		      	  	    			tgLstDetails.getTgType());

		      	  	    	invTag.setApVoucherLineItem(apVoucherLineItem);
		      	  	    	invTag.setInvItemLocation(apVoucherLineItem.getInvItemLocation());
		      	  	    	LocalAdUser adUser = null;
		      	  	    	try {
		      	  	    		adUser = adUserHome.findByUsrName(tgLstDetails.getTgCustodian(), AD_CMPNY);
		      	  	    	}catch(FinderException ex){

		      	  	    	}
		      	  	    	invTag.setAdUser(adUser);
		      	  	    	System.out.println("ngcreate ba?");
	      	  	    	}

	      	  	    }




				}catch(Exception ex) {

				}
				*/

			}

			return apVoucherLineItem;

		} catch (Exception ex) {

			Debug.printStackTrace(ex);
			getSessionContext().setRollbackOnly();
			throw new EJBException(ex.getMessage());

		}

	}


	private double getInvFifoCost(Date CST_DT, Integer IL_CODE, double CST_QTY, double CST_COST,
			boolean isAdjustFifo, Integer AD_BRNCH, Integer AD_CMPNY)
	{

		LocalInvCostingHome invCostingHome = null;
		LocalInvItemLocationHome invItemLocationHome = null;

		// Initialize EJB Home

		try {

			invCostingHome = (LocalInvCostingHome)EJBHomeFactory.
			lookUpLocalHome(LocalInvCostingHome.JNDI_NAME, LocalInvCostingHome.class);
			invItemLocationHome = (LocalInvItemLocationHome)EJBHomeFactory.
			lookUpLocalHome(LocalInvItemLocationHome.JNDI_NAME, LocalInvItemLocationHome.class);
		}
		catch (NamingException ex) {

			throw new EJBException(ex.getMessage());
		}

		try {

			Collection invFifoCostings = invCostingHome.findFifoRemainingQuantityByLessThanOrEqualCstDateAndIlCodeAndBrCode(CST_DT, IL_CODE, AD_BRNCH, AD_CMPNY);

			if (invFifoCostings.size() > 0) {

				Iterator x = invFifoCostings.iterator();

				if (isAdjustFifo) {

					//executed during POST transaction

					double totalCost = 0d;
					double cost;

					if(CST_QTY < 0) {

						//for negative quantities
						double neededQty = -(CST_QTY);

						while(x.hasNext() && neededQty != 0) {

							LocalInvCosting invFifoCosting = (LocalInvCosting)x.next();

							if (invFifoCosting.getApPurchaseOrderLine() != null || invFifoCosting.getApVoucherLineItem() != null) {
								cost = invFifoCosting.getCstItemCost() / invFifoCosting.getCstQuantityReceived();
							} else if(invFifoCosting.getArInvoiceLineItem() != null) {
								cost = invFifoCosting.getCstCostOfSales() / invFifoCosting.getCstQuantitySold();
							} else if (invFifoCosting.getInvBuildUnbuildAssemblyLine() != null) {
								cost = invFifoCosting.getCstAssemblyCost() / invFifoCosting.getCstAssemblyQuantity();
							} else {
								cost = invFifoCosting.getCstAdjustCost() /invFifoCosting.getCstAdjustQuantity();
							}

							if(neededQty <= invFifoCosting.getCstRemainingLifoQuantity()) {

								invFifoCosting.setCstRemainingLifoQuantity(invFifoCosting.getCstRemainingLifoQuantity() - neededQty);
								totalCost += (neededQty * cost);
								neededQty = 0d;
							} else {

								neededQty -= invFifoCosting.getCstRemainingLifoQuantity();
								totalCost += (invFifoCosting.getCstRemainingLifoQuantity() * cost);
								invFifoCosting.setCstRemainingLifoQuantity(0);
							}
						}

						//if needed qty is not yet satisfied but no more quantities to fetch, get the default cost
						if(neededQty != 0) {

							LocalInvItemLocation invItemLocation = invItemLocationHome.findByPrimaryKey(IL_CODE);
							totalCost += (neededQty * invItemLocation.getInvItem().getIiUnitCost());
						}

						cost = totalCost / -CST_QTY;
					}

					else {

						//for positive quantities
						cost = CST_COST;
					}
					return cost;
				}

				else {

					//executed during ENTRY transaction

					LocalInvCosting invFifoCosting = (LocalInvCosting)x.next();

					if (invFifoCosting.getApPurchaseOrderLine() != null || invFifoCosting.getApVoucherLineItem() != null) {
						return EJBCommon.roundIt(invFifoCosting.getCstItemCost() / invFifoCosting.getCstQuantityReceived(), this.getInvGpCostPrecisionUnit(AD_CMPNY));
					} else if(invFifoCosting.getArInvoiceLineItem() != null) {
						return  EJBCommon.roundIt(invFifoCosting.getCstCostOfSales() / invFifoCosting.getCstQuantitySold(), this.getInvGpCostPrecisionUnit(AD_CMPNY));
					} else if (invFifoCosting.getInvBuildUnbuildAssemblyLine() != null) {
						return EJBCommon.roundIt(invFifoCosting.getCstAssemblyCost() / invFifoCosting.getCstAssemblyQuantity(), this.getInvGpCostPrecisionUnit(AD_CMPNY));
					} else {
						return EJBCommon.roundIt(invFifoCosting.getCstAdjustCost() /invFifoCosting.getCstAdjustQuantity(), this.getInvGpCostPrecisionUnit(AD_CMPNY));
					}
				}
			}
			else {

				//most applicable in 1st entries of data
				LocalInvItemLocation invItemLocation = invItemLocationHome.findByPrimaryKey(IL_CODE);
				return invItemLocation.getInvItem().getIiUnitCost();
			}

		}
		catch (Exception ex) {
			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());
		}
	}

	/**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
     public short getInvGpCostPrecisionUnit(Integer AD_CMPNY) {

        Debug.print("ApReceivingItemEntryControllerBean getInvGpCostPrecisionUnit");

        LocalAdPreferenceHome adPreferenceHome = null;

         // Initialize EJB Home

         try {

         	adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);

         } catch (NamingException ex) {

            throw new EJBException(ex.getMessage());

         }


         try {

            LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);

            return adPreference.getPrfInvCostPrecisionUnit();

         } catch (Exception ex) {

            Debug.printStackTrace(ex);
            throw new EJBException(ex.getMessage());

         }

     }

	private double convertForeignToFunctionalCurrency(Integer FC_CODE, String FC_NM,
			Date CONVERSION_DATE, double CONVERSION_RATE, double AMOUNT, Integer AD_CMPNY) {

		Debug.print("ApPaymentEntryControllerBean convertForeignToFunctionalCurrency");


		LocalGlFunctionalCurrencyRateHome glFunctionalCurrencyRateHome = null;
		LocalAdCompanyHome adCompanyHome = null;

		LocalAdCompany adCompany = null;

		// Initialize EJB Homes

		try {

			glFunctionalCurrencyRateHome = (LocalGlFunctionalCurrencyRateHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlFunctionalCurrencyRateHome.JNDI_NAME, LocalGlFunctionalCurrencyRateHome.class);
			adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}

		// get company and extended precision

		try {

			adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);

		} catch (Exception ex) {

			throw new EJBException(ex.getMessage());

		}


		// Convert to functional currency if necessary

		if (CONVERSION_RATE != 1 && CONVERSION_RATE != 0 ) {

			AMOUNT = AMOUNT / CONVERSION_RATE;

		}

		return EJBCommon.roundIt(AMOUNT, adCompany.getGlFunctionalCurrency().getFcPrecision());

	}

	private void executeApChkPost(Integer CHK_CODE, String USR_NM, Integer AD_BRNCH, Integer AD_CMPNY) throws
	GlobalRecordAlreadyDeletedException,
	GlobalTransactionAlreadyPostedException,
	GlobalTransactionAlreadyVoidPostedException,
	GlJREffectiveDateNoPeriodExistException,
	GlJREffectiveDatePeriodClosedException,
	GlobalJournalNotBalanceException,
	AdPRFCoaGlVarianceAccountNotFoundException {

		Debug.print("ApDirectCheckEntryControllerBean executeApChkPost");

		LocalApCheckHome apCheckHome = null;
		LocalAdCompanyHome adCompanyHome = null;
		LocalAdPreferenceHome adPreferenceHome = null;
		LocalGlSetOfBookHome glSetOfBookHome = null;
		LocalGlAccountingCalendarValueHome glAccountingCalendarValueHome = null;
		LocalGlJournalHome glJournalHome = null;
		LocalGlJournalBatchHome glJournalBatchHome = null;
		LocalGlSuspenseAccountHome glSuspenseAccountHome = null;
		LocalGlJournalLineHome glJournalLineHome = null;
		LocalGlJournalSourceHome glJournalSourceHome = null;
		LocalGlJournalCategoryHome glJournalCategoryHome = null;
		LocalGlFunctionalCurrencyHome glFunctionalCurrencyHome = null;
		LocalApDistributionRecordHome apDistributionRecordHome = null;
		LocalInvCostingHome invCostingHome = null;
		LocalGlChartOfAccountHome glChartOfAccountHome = null;
		LocalAdBankAccountBalanceHome adBankAccountBalanceHome = null;
		LocalAdBankAccountHome adBankAccountHome = null;
 		LocalGlForexLedgerHome glForexLedgerHome = null;
 		LocalGlFunctionalCurrencyRateHome glFunctionalCurrencyRateHome = null;

		LocalApCheck apCheck = null;

		// Initialize EJB Home

		try {

			apCheckHome = (LocalApCheckHome)EJBHomeFactory.
				lookUpLocalHome(LocalApCheckHome.JNDI_NAME, LocalApCheckHome.class);
			adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
			adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);
			glSetOfBookHome = (LocalGlSetOfBookHome)EJBHomeFactory.
				lookUpLocalHome(LocalGlSetOfBookHome.JNDI_NAME, LocalGlSetOfBookHome.class);
			glAccountingCalendarValueHome = (LocalGlAccountingCalendarValueHome)EJBHomeFactory.
				lookUpLocalHome(LocalGlAccountingCalendarValueHome.JNDI_NAME, LocalGlAccountingCalendarValueHome.class);
			glJournalHome = (LocalGlJournalHome)EJBHomeFactory.
				lookUpLocalHome(LocalGlJournalHome.JNDI_NAME, LocalGlJournalHome.class);
			glJournalBatchHome = (LocalGlJournalBatchHome)EJBHomeFactory.
				lookUpLocalHome(LocalGlJournalBatchHome.JNDI_NAME, LocalGlJournalBatchHome.class);
			glSuspenseAccountHome = (LocalGlSuspenseAccountHome)EJBHomeFactory.
				lookUpLocalHome(LocalGlSuspenseAccountHome.JNDI_NAME, LocalGlSuspenseAccountHome.class);
			glJournalLineHome = (LocalGlJournalLineHome)EJBHomeFactory.
				lookUpLocalHome(LocalGlJournalLineHome.JNDI_NAME, LocalGlJournalLineHome.class);
			glJournalSourceHome = (LocalGlJournalSourceHome)EJBHomeFactory.
				lookUpLocalHome(LocalGlJournalSourceHome.JNDI_NAME, LocalGlJournalSourceHome.class);
			glJournalCategoryHome = (LocalGlJournalCategoryHome)EJBHomeFactory.
				lookUpLocalHome(LocalGlJournalCategoryHome.JNDI_NAME, LocalGlJournalCategoryHome.class);
			glFunctionalCurrencyHome = (LocalGlFunctionalCurrencyHome)EJBHomeFactory.
				lookUpLocalHome(LocalGlFunctionalCurrencyHome.JNDI_NAME, LocalGlFunctionalCurrencyHome.class);
			apDistributionRecordHome = (LocalApDistributionRecordHome)EJBHomeFactory.
				lookUpLocalHome(LocalApDistributionRecordHome.JNDI_NAME, LocalApDistributionRecordHome.class);
			invCostingHome = (LocalInvCostingHome)EJBHomeFactory.
				lookUpLocalHome(LocalInvCostingHome.JNDI_NAME, LocalInvCostingHome.class);
			glChartOfAccountHome = (LocalGlChartOfAccountHome)EJBHomeFactory.
				lookUpLocalHome(LocalGlChartOfAccountHome.JNDI_NAME, LocalGlChartOfAccountHome.class);
			adBankAccountBalanceHome = (LocalAdBankAccountBalanceHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdBankAccountBalanceHome.JNDI_NAME, LocalAdBankAccountBalanceHome.class);
			adBankAccountBalanceHome = (LocalAdBankAccountBalanceHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdBankAccountBalanceHome.JNDI_NAME, LocalAdBankAccountBalanceHome.class);
			adBankAccountHome = (LocalAdBankAccountHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdBankAccountHome.JNDI_NAME, LocalAdBankAccountHome.class);
 			glForexLedgerHome  = (LocalGlForexLedgerHome)EJBHomeFactory.
				lookUpLocalHome(LocalGlForexLedgerHome.JNDI_NAME, LocalGlForexLedgerHome.class);
 			glFunctionalCurrencyRateHome = (LocalGlFunctionalCurrencyRateHome)EJBHomeFactory.
				lookUpLocalHome(LocalGlFunctionalCurrencyRateHome.JNDI_NAME, LocalGlFunctionalCurrencyRateHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}

		try {

			// validate if check is already deleted

			try {

				apCheck = apCheckHome.findByPrimaryKey(CHK_CODE);

			} catch (FinderException ex) {

				throw new GlobalRecordAlreadyDeletedException();

			}

			// validate if check is already posted

			if (apCheck.getChkVoid() == EJBCommon.FALSE && apCheck.getChkPosted() == EJBCommon.TRUE) {

				throw new GlobalTransactionAlreadyPostedException();

				// validate if check void is already posted

			} else if (apCheck.getChkVoid() == EJBCommon.TRUE && apCheck.getChkVoidPosted() == EJBCommon.TRUE) {

				throw new GlobalTransactionAlreadyVoidPostedException();

			}


			// post check

			if (apCheck.getChkVoid() == EJBCommon.FALSE && apCheck.getChkPosted() == EJBCommon.FALSE) {
				// POSTING DIRECT PAYMENT

				if (apCheck.getChkType().equals("DIRECT") && !apCheck.getApVoucherLineItems().isEmpty()) {

					Iterator c = apCheck.getApVoucherLineItems().iterator();

					while(c.hasNext()) {

						LocalApVoucherLineItem apVoucherLineItem = (LocalApVoucherLineItem) c.next();

						String II_NM = apVoucherLineItem.getInvItemLocation().getInvItem().getIiName();
						String LOC_NM = apVoucherLineItem.getInvItemLocation().getInvLocation().getLocName();

						double QTY_RCVD = this.convertByUomFromAndItemAndQuantity(apVoucherLineItem.getInvUnitOfMeasure(),
								apVoucherLineItem.getInvItemLocation().getInvItem(), apVoucherLineItem.getVliQuantity(), AD_CMPNY);

						LocalInvCosting invCosting = null;

						try {

							invCosting  = invCostingHome.getByMaxCstDateToLongAndMaxCstLineNumberAndLessThanEqualCstDateAndIiNameAndLocName(apCheck.getChkDate(), II_NM, LOC_NM, AD_BRNCH, AD_CMPNY);

						} catch (FinderException ex) {

						}

						double COST = apVoucherLineItem.getVliUnitCost();

						if (invCosting == null) {

							this.postToInv(apVoucherLineItem, apCheck.getChkDate(),
									QTY_RCVD, COST * QTY_RCVD,
									QTY_RCVD, COST * QTY_RCVD,
									0d, null, AD_BRNCH, AD_CMPNY);

						} else {

							this.postToInv(apVoucherLineItem, apCheck.getChkDate(),
									QTY_RCVD, COST * QTY_RCVD,
									invCosting.getCstRemainingQuantity() + QTY_RCVD,
									invCosting.getCstRemainingValue() + (COST * QTY_RCVD),
									0d, null, AD_BRNCH, AD_CMPNY);


						}

					}

				}

				// decrease bank balance

				LocalAdBankAccount adBankAccount = adBankAccountHome.findByPrimaryKey(apCheck.getAdBankAccount().getBaCode());

				try {

					// find bankaccount balance before or equal check date

					Collection adBankAccountBalances = adBankAccountBalanceHome.findByBeforeOrEqualDateAndBaCodeAndType(apCheck.getChkCheckDate(), apCheck.getAdBankAccount().getBaCode(), "BOOK", AD_CMPNY);

					if (!adBankAccountBalances.isEmpty()) {

						// get last check

						ArrayList adBankAccountBalanceList = new ArrayList(adBankAccountBalances);

						LocalAdBankAccountBalance adBankAccountBalance = (LocalAdBankAccountBalance)adBankAccountBalanceList.get(adBankAccountBalanceList.size() - 1);

						if (adBankAccountBalance.getBabDate().before(apCheck.getChkCheckDate())) {

							// create new balance

							LocalAdBankAccountBalance adNewBankAccountBalance = adBankAccountBalanceHome.create(
									apCheck.getChkCheckDate(), adBankAccountBalance.getBabBalance() - apCheck.getChkAmount(), "BOOK", AD_CMPNY);

							//adBankAccount.addAdBankAccountBalance(adNewBankAccountBalance);
							adNewBankAccountBalance.setAdBankAccount(adBankAccount);

						} else { // equals to check date

							adBankAccountBalance.setBabBalance(adBankAccountBalance.getBabBalance() - apCheck.getChkAmount());

						}

					} else {

						// create new balance

						LocalAdBankAccountBalance adNewBankAccountBalance = adBankAccountBalanceHome.create(
								apCheck.getChkCheckDate(), (0 - apCheck.getChkAmount()), "BOOK", AD_CMPNY);

						//adBankAccount.addAdBankAccountBalance(adNewBankAccountBalance);
						adNewBankAccountBalance.setAdBankAccount(adBankAccount);

					}

					// propagate to subsequent balances if necessary

					adBankAccountBalances = adBankAccountBalanceHome.findByAfterDateAndBaCodeAndType(apCheck.getChkCheckDate(), apCheck.getAdBankAccount().getBaCode(), "BOOK", AD_CMPNY);

					Iterator i = adBankAccountBalances.iterator();

					while (i.hasNext()) {

						LocalAdBankAccountBalance adBankAccountBalance = (LocalAdBankAccountBalance)i.next();

						adBankAccountBalance.setBabBalance(adBankAccountBalance.getBabBalance() - apCheck.getChkAmount());

					}

				} catch (Exception ex) {

					ex.printStackTrace();

				}

				// set check post status

				apCheck.setChkPosted(EJBCommon.TRUE);
				apCheck.setChkPostedBy(USR_NM);
				apCheck.setChkDatePosted(EJBCommon.getGcCurrentDateWoTime().getTime());

				// set Relese Check Upon Posting if Bank Account is CASH

        		if(apCheck.getAdBankAccount().getBaIsCashAccount() == EJBCommon.TRUE){

        			apCheck.setChkReleased(EJBCommon.TRUE);
        			apCheck.setChkDateReleased(apCheck.getChkCheckDate());

        		}


			} else if (apCheck.getChkVoid() == EJBCommon.TRUE && apCheck.getChkVoidPosted() == EJBCommon.FALSE) {
				// VOIDING PAYMENT

				 if (apCheck.getChkType().equals("DIRECT") && !apCheck.getApVoucherLineItems().isEmpty()) {
					// VOIDING DIRECT PAYMENT

					Iterator c = apCheck.getApVoucherLineItems().iterator();

					while(c.hasNext()) {

						LocalApVoucherLineItem apVoucherLineItem = (LocalApVoucherLineItem) c.next();

						String II_NM = apVoucherLineItem.getInvItemLocation().getInvItem().getIiName();
						String LOC_NM = apVoucherLineItem.getInvItemLocation().getInvLocation().getLocName();

						double QTY_RCVD = this.convertByUomFromAndItemAndQuantity(apVoucherLineItem.getInvUnitOfMeasure(),
								apVoucherLineItem.getInvItemLocation().getInvItem(), apVoucherLineItem.getVliQuantity(), AD_CMPNY);

						LocalInvCosting invCosting = null;

						try {

							invCosting  = invCostingHome.getByMaxCstDateToLongAndMaxCstLineNumberAndLessThanEqualCstDateAndIiNameAndLocName(apCheck.getChkDate(), II_NM, LOC_NM, AD_BRNCH, AD_CMPNY);

						} catch (FinderException ex) {

						}

						double COST = apVoucherLineItem.getVliUnitCost();

						if (invCosting == null) {

							this.postToInv(apVoucherLineItem, apCheck.getChkDate(),
									-QTY_RCVD, -COST * QTY_RCVD,
									-QTY_RCVD, -COST * QTY_RCVD,
									0d, null,AD_BRNCH, AD_CMPNY);

						} else {

							this.postToInv(apVoucherLineItem, apCheck.getChkDate(),
									-QTY_RCVD, -COST * QTY_RCVD,
									invCosting.getCstRemainingQuantity() - QTY_RCVD, invCosting.getCstRemainingValue() - (COST * QTY_RCVD),
									0d, null, AD_BRNCH, AD_CMPNY);


						}

					}

				}

				// increase bank balance

				LocalAdBankAccount adBankAccount = adBankAccountHome.findByPrimaryKey(apCheck.getAdBankAccount().getBaCode());

				try {

					// find bankaccount balance before or equal check date

					Collection adBankAccountBalances = adBankAccountBalanceHome.findByBeforeOrEqualDateAndBaCodeAndType(apCheck.getChkCheckDate(), apCheck.getAdBankAccount().getBaCode(), "BOOK", AD_CMPNY);

					if (!adBankAccountBalances.isEmpty()) {

						// get last check

						ArrayList adBankAccountBalanceList = new ArrayList(adBankAccountBalances);

						LocalAdBankAccountBalance adBankAccountBalance = (LocalAdBankAccountBalance)adBankAccountBalanceList.get(adBankAccountBalanceList.size() - 1);

						if (adBankAccountBalance.getBabDate().before(apCheck.getChkCheckDate())) {

							// create new balance

							LocalAdBankAccountBalance adNewBankAccountBalance = adBankAccountBalanceHome.create(
									apCheck.getChkCheckDate(), adBankAccountBalance.getBabBalance() + apCheck.getChkAmount(), "BOOK", AD_CMPNY);

							//adBankAccount.addAdBankAccountBalance(adNewBankAccountBalance);
							adNewBankAccountBalance.setAdBankAccount(adBankAccount);

						} else { // equals to check date

							adBankAccountBalance.setBabBalance(adBankAccountBalance.getBabBalance() + apCheck.getChkAmount());

						}

					} else {

						// create new balance

						LocalAdBankAccountBalance adNewBankAccountBalance = adBankAccountBalanceHome.create(
								apCheck.getChkCheckDate(), (apCheck.getChkAmount()), "BOOK", AD_CMPNY);

						//adBankAccount.addAdBankAccountBalance(adNewBankAccountBalance);
						adNewBankAccountBalance.setAdBankAccount(adBankAccount);

					}

					// propagate to subsequent balances if necessary

					adBankAccountBalances = adBankAccountBalanceHome.findByAfterDateAndBaCodeAndType(apCheck.getChkCheckDate(), apCheck.getAdBankAccount().getBaCode(), "BOOK", AD_CMPNY);

					Iterator i = adBankAccountBalances.iterator();

					while (i.hasNext()) {

						LocalAdBankAccountBalance adBankAccountBalance = (LocalAdBankAccountBalance)i.next();

						adBankAccountBalance.setBabBalance(adBankAccountBalance.getBabBalance() + apCheck.getChkAmount());

					}

				} catch (Exception ex) {

					ex.printStackTrace();

				}

				// set check post status

				apCheck.setChkVoidPosted(EJBCommon.TRUE);
				apCheck.setChkPostedBy(USR_NM);
				apCheck.setChkDatePosted(EJBCommon.getGcCurrentDateWoTime().getTime());

			}

			// post to gl if necessary

			LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);
			LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);

			if (adPreference.getPrfApGlPostingType().equals("AUTO-POST UPON APPROVAL")) {

				// validate if date has no period and period is closed

				LocalGlSetOfBook glJournalSetOfBook = null;

				try {

					glJournalSetOfBook = glSetOfBookHome.findByDate(apCheck.getChkDate(), AD_CMPNY);

				} catch (FinderException ex) {

					throw new GlJREffectiveDateNoPeriodExistException();

				}

				LocalGlAccountingCalendarValue glAccountingCalendarValue =
					glAccountingCalendarValueHome.findByAcCodeAndDate(
							glJournalSetOfBook.getGlAccountingCalendar().getAcCode(), apCheck.getChkDate(), AD_CMPNY);


				if (glAccountingCalendarValue.getAcvStatus() == 'N' ||
						glAccountingCalendarValue.getAcvStatus() == 'C' ||
						glAccountingCalendarValue.getAcvStatus() == 'P') {

					throw new GlJREffectiveDatePeriodClosedException();

				}

				// check if check is balance if not check suspense posting

				LocalGlJournalLine glOffsetJournalLine = null;

				Collection apDistributionRecords = null;

				if (apCheck.getChkVoid() == EJBCommon.FALSE) {

					apDistributionRecords = apDistributionRecordHome.findImportableDrByDrReversedAndChkCode(EJBCommon.FALSE, apCheck.getChkCode(), AD_CMPNY);

				} else {

					apDistributionRecords = apDistributionRecordHome.findImportableDrByDrReversedAndChkCode(EJBCommon.TRUE, apCheck.getChkCode(), AD_CMPNY);

				}

				Iterator j = apDistributionRecords.iterator();

				double TOTAL_DEBIT = 0d;
				double TOTAL_CREDIT = 0d;

				while (j.hasNext()) {

					LocalApDistributionRecord apDistributionRecord = (LocalApDistributionRecord)j.next();

					double DR_AMNT = 0d;

					if (apDistributionRecord.getApAppliedVoucher() != null) {

						LocalApVoucher apVoucher = apDistributionRecord.getApAppliedVoucher().getApVoucherPaymentSchedule().getApVoucher();

						DR_AMNT = this.convertForeignToFunctionalCurrency(apVoucher.getGlFunctionalCurrency().getFcCode(),
								apVoucher.getGlFunctionalCurrency().getFcName(),
								apVoucher.getVouConversionDate(),
								apVoucher.getVouConversionRate(),
								apDistributionRecord.getDrAmount(), AD_CMPNY);

					} else {

						DR_AMNT = this.convertForeignToFunctionalCurrency(apCheck.getGlFunctionalCurrency().getFcCode(),
								apCheck.getGlFunctionalCurrency().getFcName(),
								apCheck.getChkConversionDate(),
								apCheck.getChkConversionRate(),
								apDistributionRecord.getDrAmount(), AD_CMPNY);

					}

					if (apDistributionRecord.getDrDebit() == EJBCommon.TRUE) {

						TOTAL_DEBIT += DR_AMNT;

					} else {

						TOTAL_CREDIT += DR_AMNT;

					}

				}

				TOTAL_DEBIT = EJBCommon.roundIt(TOTAL_DEBIT, adCompany.getGlFunctionalCurrency().getFcPrecision());
				TOTAL_CREDIT = EJBCommon.roundIt(TOTAL_CREDIT, adCompany.getGlFunctionalCurrency().getFcPrecision());

				if (adPreference.getPrfAllowSuspensePosting() == EJBCommon.TRUE &&
						TOTAL_DEBIT != TOTAL_CREDIT) {

					LocalGlSuspenseAccount glSuspenseAccount = null;

					try {

						glSuspenseAccount = glSuspenseAccountHome.findByJsNameAndJcName("ACCOUNTS PAYABLES", "CHECKS", AD_CMPNY);

					} catch (FinderException ex) {

						throw new GlobalJournalNotBalanceException();

					}

					if (TOTAL_DEBIT - TOTAL_CREDIT < 0) {

						glOffsetJournalLine = glJournalLineHome.create((short)(apDistributionRecords.size() + 1),
								EJBCommon.TRUE, TOTAL_CREDIT - TOTAL_DEBIT, "", AD_CMPNY);

					} else {

						glOffsetJournalLine = glJournalLineHome.create((short)(apDistributionRecords.size() + 1),
								EJBCommon.FALSE, TOTAL_DEBIT - TOTAL_CREDIT, "", AD_CMPNY);

					}

					LocalGlChartOfAccount glChartOfAccount = glSuspenseAccount.getGlChartOfAccount();
					//glChartOfAccount.addGlJournalLine(glOffsetJournalLine);
					glOffsetJournalLine.setGlChartOfAccount(glChartOfAccount);


				} else if (adPreference.getPrfAllowSuspensePosting() == EJBCommon.FALSE &&
						TOTAL_DEBIT != TOTAL_CREDIT) {

					throw new GlobalJournalNotBalanceException();

				}

				// create journal batch if necessary

				LocalGlJournalBatch glJournalBatch = null;
				java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat("MM/dd/yyyy");

				try {

					if (adPreference.getPrfEnableApCheckBatch() == EJBCommon.TRUE) {

						glJournalBatch = glJournalBatchHome.findByJbName("JOURNAL IMPORT " + formatter.format(new Date()) + " " + apCheck.getApCheckBatch().getCbName(), AD_BRNCH, AD_CMPNY);

					} else {

						glJournalBatch = glJournalBatchHome.findByJbName("JOURNAL IMPORT " + formatter.format(new Date()) + " CHECKS", AD_BRNCH, AD_CMPNY);

					}

				} catch (FinderException ex) {
				}

				if (adPreference.getPrfEnableGlJournalBatch() ==  EJBCommon.TRUE &&
						glJournalBatch == null) {

					if (adPreference.getPrfEnableApCheckBatch() == EJBCommon.TRUE) {

						glJournalBatch = glJournalBatchHome.create("JOURNAL IMPORT " + formatter.format(new Date()) + " " + apCheck.getApCheckBatch().getCbName(), "JOURNAL IMPORT", "CLOSED", EJBCommon.getGcCurrentDateWoTime().getTime(), USR_NM, AD_BRNCH, AD_CMPNY);

					} else {

						glJournalBatch = glJournalBatchHome.create("JOURNAL IMPORT " + formatter.format(new Date()) + " CHECKS", "JOURNAL IMPORT", "CLOSED", EJBCommon.getGcCurrentDateWoTime().getTime(), USR_NM, AD_BRNCH, AD_CMPNY);

					}


				}

				// create journal entry
				String supplierName = apCheck.getChkSupplierName() == null ? apCheck.getApSupplier().getSplName() :
        			apCheck.getChkSupplierName();

				LocalGlJournal glJournal = glJournalHome.create(apCheck.getChkReferenceNumber(),
						apCheck.getChkDescription(), apCheck.getChkDate(),
						0.0d, null, apCheck.getChkDocumentNumber(), null, 1d, "N/A", null,
						'N', EJBCommon.TRUE, EJBCommon.FALSE,
						USR_NM, new Date(),
						USR_NM, new Date(),
						null, null,
						USR_NM, EJBCommon.getGcCurrentDateWoTime().getTime(),
						apCheck.getApSupplier().getSplTin(),
						supplierName, EJBCommon.FALSE,
						null,
						AD_BRNCH, AD_CMPNY);

				LocalGlJournalSource glJournalSource = glJournalSourceHome.findByJsName("ACCOUNTS PAYABLES", AD_CMPNY);
				glJournal.setGlJournalSource(glJournalSource);

				LocalGlFunctionalCurrency glFunctionalCurrency = glFunctionalCurrencyHome.findByFcName(adCompany.getGlFunctionalCurrency().getFcName(), AD_CMPNY);
				glJournal.setGlFunctionalCurrency(glFunctionalCurrency);

				LocalGlJournalCategory glJournalCategory = glJournalCategoryHome.findByJcName("CHECKS", AD_CMPNY);
				glJournal.setGlJournalCategory(glJournalCategory);

				if (glJournalBatch != null) {

					glJournal.setGlJournalBatch(glJournalBatch);

				}


				// create journal lines

				j = apDistributionRecords.iterator();

				while (j.hasNext()) {

					LocalApDistributionRecord apDistributionRecord = (LocalApDistributionRecord)j.next();

					double DR_AMNT = 0d;

					LocalApVoucher apVoucher = null;

					if (apDistributionRecord.getApAppliedVoucher() != null) {

						apVoucher = apDistributionRecord.getApAppliedVoucher().getApVoucherPaymentSchedule().getApVoucher();

						DR_AMNT = this.convertForeignToFunctionalCurrency(apVoucher.getGlFunctionalCurrency().getFcCode(),
								apVoucher.getGlFunctionalCurrency().getFcName(),
								apVoucher.getVouConversionDate(),
								apVoucher.getVouConversionRate(),
								apDistributionRecord.getDrAmount(), AD_CMPNY);

					} else {

						DR_AMNT = this.convertForeignToFunctionalCurrency(apCheck.getGlFunctionalCurrency().getFcCode(),
								apCheck.getGlFunctionalCurrency().getFcName(),
								apCheck.getChkConversionDate(),
								apCheck.getChkConversionRate(),
								apDistributionRecord.getDrAmount(), AD_CMPNY);

					}

					LocalGlJournalLine glJournalLine = glJournalLineHome.create(apDistributionRecord.getDrLine(),
							apDistributionRecord.getDrDebit(), DR_AMNT, "", AD_CMPNY);

					//apDistributionRecord.getGlChartOfAccount().addGlJournalLine(glJournalLine);
					glJournalLine.setGlChartOfAccount(apDistributionRecord.getGlChartOfAccount());
					//glJournal.addGlJournalLine(glJournalLine);
					glJournalLine.setGlJournal(glJournal);

					apDistributionRecord.setDrImported(EJBCommon.TRUE);

    		       	// for FOREX revaluation

            	    int FC_CODE = apDistributionRecord.getApAppliedVoucher() != null ?
            	    	apVoucher.getGlFunctionalCurrency().getFcCode().intValue() :
            	    	apCheck.getGlFunctionalCurrency().getFcCode().intValue();

    		       	if((FC_CODE != adCompany.getGlFunctionalCurrency().getFcCode().intValue()) &&
    					glJournalLine.getGlChartOfAccount().getGlFunctionalCurrency() != null && (FC_CODE ==
    						glJournalLine.getGlChartOfAccount().getGlFunctionalCurrency().getFcCode().intValue())){

    		       		double CONVERSION_RATE = apDistributionRecord.getApAppliedVoucher() != null ?
    		            	    apVoucher.getVouConversionRate() : apCheck.getChkConversionRate();

    		            Date DATE = apDistributionRecord.getApAppliedVoucher() != null ?
    		            	    apVoucher.getVouConversionDate() : apCheck.getChkConversionDate();

    		            if (DATE != null && (CONVERSION_RATE == 0 || CONVERSION_RATE == 1)){

    		  	  			CONVERSION_RATE = this.getFrRateByFrNameAndFrDate(
    			  	  				glJournalLine.getGlChartOfAccount().getGlFunctionalCurrency().getFcName(),
    								glJournal.getJrConversionDate(), AD_CMPNY);

    		            } else if (CONVERSION_RATE == 0) {

    		            	CONVERSION_RATE = 1;

    		       		}

    		       		Collection glForexLedgers = null;

    		       		DATE = apDistributionRecord.getApAppliedVoucher() != null ?
    		            	    apVoucher.getVouDate() : apCheck.getChkDate();

    		       		try {

    		       			glForexLedgers = glForexLedgerHome.findLatestGlFrlByFrlDateAndByCoaCode(
    		       					DATE, glJournalLine.getGlChartOfAccount().getCoaCode(),
    								AD_CMPNY);

    		       		} catch(FinderException ex) {

    		       		}

    		       		LocalGlForexLedger glForexLedger =
    		       			(glForexLedgers.isEmpty() || glForexLedgers == null) ? null :
    		       				(LocalGlForexLedger) glForexLedgers.iterator().next();

    		       		int FRL_LN = (glForexLedger != null &&
    		       			glForexLedger.getFrlDate().compareTo(DATE) == 0) ?
    		       				glForexLedger.getFrlLine().intValue() + 1 : 1;

    		       		// compute balance
    		       		double COA_FRX_BLNC = glForexLedger == null ? 0 : glForexLedger.getFrlBalance();
    		       		double FRL_AMNT = apDistributionRecord.getDrAmount();

    		       		if(glJournalLine.getGlChartOfAccount().getCoaAccountType().equalsIgnoreCase("ASSET"))
    		       			FRL_AMNT = (glJournalLine.getJlDebit() == EJBCommon.TRUE  ? FRL_AMNT : (- 1 * FRL_AMNT));
    		       		else
    		       			FRL_AMNT = (glJournalLine.getJlDebit() == EJBCommon.TRUE  ? (- 1 * FRL_AMNT) : FRL_AMNT);

    		       		COA_FRX_BLNC = COA_FRX_BLNC + FRL_AMNT;

    		       		glForexLedger = glForexLedgerHome.create(DATE, new Integer (FRL_LN), "CHK", FRL_AMNT,
    		       				CONVERSION_RATE, COA_FRX_BLNC, 0d, AD_CMPNY);

    		       		//glJournalLine.getGlChartOfAccount().addGlForexLedger(glForexLedger);
    		       		glForexLedger.setGlChartOfAccount(glJournalLine.getGlChartOfAccount());

    		       		// propagate balances
    		       		try{

    		       			glForexLedgers = glForexLedgerHome.findByGreaterThanFrlDateAndCoaCode(
    		       					glForexLedger.getFrlDate(), glForexLedger.getGlChartOfAccount().getCoaCode(),
    								glForexLedger.getFrlAdCompany());

    		       		} catch (FinderException ex) {

    		       		}

    		       		Iterator itrFrl = glForexLedgers.iterator();

    		       		while (itrFrl.hasNext()) {

    		       			glForexLedger = (LocalGlForexLedger) itrFrl.next();
    		       			FRL_AMNT = apDistributionRecord.getDrAmount();

    		       			if(glJournalLine.getGlChartOfAccount().getCoaAccountType().equalsIgnoreCase("ASSET"))
    		       				FRL_AMNT = (glJournalLine.getJlDebit() == EJBCommon.TRUE  ? FRL_AMNT :
    		       					(- 1 * FRL_AMNT));
    		       			else
    		       				FRL_AMNT = (glJournalLine.getJlDebit() == EJBCommon.TRUE  ? (- 1 * FRL_AMNT) :
    		       					FRL_AMNT);

    		       			glForexLedger.setFrlBalance(glForexLedger.getFrlBalance() + FRL_AMNT);

    		       		}

    		       	}

				}

				if (glOffsetJournalLine != null) {

					//glJournal.addGlJournalLine(glOffsetJournalLine);
					glOffsetJournalLine.setGlJournal(glJournal);

				}

				// post journal to gl

				Collection glJournalLines = glJournal.getGlJournalLines();

				Iterator i = glJournalLines.iterator();

				while (i.hasNext()) {

					LocalGlJournalLine glJournalLine = (LocalGlJournalLine)i.next();

					// post current to current acv

					this.postToGl(glAccountingCalendarValue,
							glJournalLine.getGlChartOfAccount(),
							true, glJournalLine.getJlDebit(), glJournalLine.getJlAmount(), AD_CMPNY);


					// post to subsequent acvs (propagate)

					Collection glSubsequentAccountingCalendarValues =
						glAccountingCalendarValueHome.findSubsequentAcvByAcCodeAndAcvPeriodNumber(
								glJournalSetOfBook.getGlAccountingCalendar().getAcCode(),
								glAccountingCalendarValue.getAcvPeriodNumber(), AD_CMPNY);

					Iterator acvsIter = glSubsequentAccountingCalendarValues.iterator();

					while (acvsIter.hasNext()) {

						LocalGlAccountingCalendarValue glSubsequentAccountingCalendarValue =
							(LocalGlAccountingCalendarValue)acvsIter.next();

						this.postToGl(glSubsequentAccountingCalendarValue,
								glJournalLine.getGlChartOfAccount(),
								false, glJournalLine.getJlDebit(), glJournalLine.getJlAmount(), AD_CMPNY);

					}

					// post to subsequent years if necessary

					Collection glSubsequentSetOfBooks = glSetOfBookHome.findSubsequentSobByAcYear(glJournalSetOfBook.getGlAccountingCalendar().getAcYear(), AD_CMPNY);

					if (!glSubsequentSetOfBooks.isEmpty() && glJournalSetOfBook.getSobYearEndClosed() == 1) {

						adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
						LocalGlChartOfAccount glRetainedEarningsAccount = glChartOfAccountHome.findByCoaAccountNumber(adCompany.getCmpRetainedEarnings(), AD_CMPNY);

						Iterator sobIter = glSubsequentSetOfBooks.iterator();

						while (sobIter.hasNext()) {

							LocalGlSetOfBook glSubsequentSetOfBook = (LocalGlSetOfBook)sobIter.next();

							String ACCOUNT_TYPE = glJournalLine.getGlChartOfAccount().getCoaAccountType();

							// post to subsequent acvs of subsequent set of book(propagate)

							Collection glAccountingCalendarValues =
								glAccountingCalendarValueHome.findByAcCode(glSubsequentSetOfBook.getGlAccountingCalendar().getAcCode(), AD_CMPNY);

							Iterator acvIter = glAccountingCalendarValues.iterator();

							while (acvIter.hasNext()) {

								LocalGlAccountingCalendarValue glSubsequentAccountingCalendarValue =
									(LocalGlAccountingCalendarValue)acvIter.next();

								if (ACCOUNT_TYPE.equals("ASSET") || ACCOUNT_TYPE.equals("LIABILITY") ||
										ACCOUNT_TYPE.equals("OWNERS EQUITY")) {

									this.postToGl(glSubsequentAccountingCalendarValue,
											glJournalLine.getGlChartOfAccount(),
											false, glJournalLine.getJlDebit(), glJournalLine.getJlAmount(), AD_CMPNY);

								} else { // revenue & expense

									this.postToGl(glSubsequentAccountingCalendarValue,
											glRetainedEarningsAccount,
											false, glJournalLine.getJlDebit(), glJournalLine.getJlAmount(), AD_CMPNY);

								}

							}

							if (glSubsequentSetOfBook.getSobYearEndClosed() == 0) break;

						}

					}




				}






				if(apCheck.getApSupplier().getApSupplierClass().getScLedger() == EJBCommon.TRUE){

			    	// Post Investors Account balance

                                // post current to current acv
                                    System.out.println("post gl part 0");
                                    this.postToGlInvestor(glAccountingCalendarValue,
                                                    apCheck.getApSupplier(),
                                                    true,
                                                    apCheck.getChkVoid() == EJBCommon.FALSE ? EJBCommon.TRUE : EJBCommon.FALSE ,
                                                    apCheck.getChkVoid() == EJBCommon.FALSE ? apCheck.getChkAmount() : -apCheck.getChkAmount(),
                                                     AD_CMPNY);

                                    System.out.println("post gl done");
                                    // post to subsequent acvs (propagate)

                                    Collection glSubsequentAccountingCalendarValues =
                                            glAccountingCalendarValueHome.findSubsequentAcvByAcCodeAndAcvPeriodNumber(
                                                            glJournalSetOfBook.getGlAccountingCalendar().getAcCode(),
                                                                    glAccountingCalendarValue.getAcvPeriodNumber(), AD_CMPNY);

                                    Iterator acvsIter = glSubsequentAccountingCalendarValues.iterator();

                                    while (acvsIter.hasNext()) {

                                            LocalGlAccountingCalendarValue glSubsequentAccountingCalendarValue =
                                                    (LocalGlAccountingCalendarValue)acvsIter.next();


                                            this.postToGlInvestor(glSubsequentAccountingCalendarValue,
                                                            apCheck.getApSupplier(),
                                                            false,
                                                            apCheck.getChkVoid() == EJBCommon.FALSE ? EJBCommon.TRUE : EJBCommon.FALSE,
                                                            apCheck.getChkVoid() == EJBCommon.FALSE ? apCheck.getChkAmount() : -apCheck.getChkAmount(),
                                                            AD_CMPNY);

                                    }


                                    // post to subsequent years if necessary

                                    Collection glSubsequentSetOfBooks = glSetOfBookHome.findSubsequentSobByAcYear(glJournalSetOfBook.getGlAccountingCalendar().getAcYear(), AD_CMPNY);

                                    if (!glSubsequentSetOfBooks.isEmpty() && glJournalSetOfBook.getSobYearEndClosed() == 1) {

                                            adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);

                                            Iterator sobIter = glSubsequentSetOfBooks.iterator();

                                            while (sobIter.hasNext()) {

                                                    LocalGlSetOfBook glSubsequentSetOfBook = (LocalGlSetOfBook)sobIter.next();

                                                    // post to subsequent acvs of subsequent set of book(propagate)

                                                    Collection glAccountingCalendarValues =
                                                            glAccountingCalendarValueHome.findByAcCode(glSubsequentSetOfBook.getGlAccountingCalendar().getAcCode(), AD_CMPNY);

                                                    Iterator acvIter = glAccountingCalendarValues.iterator();

                                                    while (acvIter.hasNext()) {

                                                            LocalGlAccountingCalendarValue glSubsequentAccountingCalendarValue =
                                                                    (LocalGlAccountingCalendarValue)acvIter.next();

                                                            this.postToGlInvestor(glSubsequentAccountingCalendarValue,
                                                                            apCheck.getApSupplier(),
                                                                            false,
                                                                            apCheck.getChkVoid() == EJBCommon.FALSE ? EJBCommon.TRUE : EJBCommon.FALSE,
                                                                            apCheck.getChkVoid() == EJBCommon.FALSE ? apCheck.getChkAmount() : -apCheck.getChkAmount(),
                                                                            AD_CMPNY);

                                                    }

                                                    if (glSubsequentSetOfBook.getSobYearEndClosed() == 0) break;

                                            }

                                    }



			      }

			}

		} catch (GlJREffectiveDateNoPeriodExistException ex) {

			getSessionContext().setRollbackOnly();
			throw ex;

		} catch (GlJREffectiveDatePeriodClosedException ex) {

			getSessionContext().setRollbackOnly();
			throw ex;

		} catch (GlobalJournalNotBalanceException ex) {

			getSessionContext().setRollbackOnly();
			throw ex;

		} catch (GlobalRecordAlreadyDeletedException ex) {

			getSessionContext().setRollbackOnly();
			throw ex;

		} catch (GlobalTransactionAlreadyPostedException ex) {

			getSessionContext().setRollbackOnly();
			throw ex;

		} catch (GlobalTransactionAlreadyVoidPostedException ex) {

			getSessionContext().setRollbackOnly();
			throw ex;

        } catch (AdPRFCoaGlVarianceAccountNotFoundException ex) {

        	getSessionContext().setRollbackOnly();
        	throw ex;

		} catch (Exception ex) {

			Debug.printStackTrace(ex);
			getSessionContext().setRollbackOnly();
			throw new EJBException(ex.getMessage());

		}

	}

	private void post(Date CHK_DT, double CHK_AMNT, LocalApSupplier apSupplier, Integer AD_CMPNY) {

		Debug.print("ApDirectCheckEntryControllerBean post");

		LocalApSupplierBalanceHome apSupplierBalanceHome = null;

		// Initialize EJB Home

		try {

			apSupplierBalanceHome = (LocalApSupplierBalanceHome)EJBHomeFactory.
			lookUpLocalHome(LocalApSupplierBalanceHome.JNDI_NAME, LocalApSupplierBalanceHome.class);

		} catch (NamingException ex) {

			getSessionContext().setRollbackOnly();
			throw new EJBException(ex.getMessage());

		}

		try {

			// find supplier balance before or equal voucher date

			Collection apSupplierBalances = apSupplierBalanceHome.findByBeforeOrEqualVouDateAndSplSupplierCode(CHK_DT, apSupplier.getSplSupplierCode(), AD_CMPNY);

			if (!apSupplierBalances.isEmpty()) {

				// get last voucher

				ArrayList apSupplierBalanceList = new ArrayList(apSupplierBalances);

				LocalApSupplierBalance apSupplierBalance = (LocalApSupplierBalance)apSupplierBalanceList.get(apSupplierBalanceList.size() - 1);

				if (apSupplierBalance.getSbDate().before(CHK_DT)) {

					// create new balance

					LocalApSupplierBalance apNewSupplierBalance = apSupplierBalanceHome.create(
							CHK_DT, apSupplierBalance.getSbBalance() + CHK_AMNT, AD_CMPNY);

					//apSupplier.addApSupplierBalance(apNewSupplierBalance);
					apNewSupplierBalance.setApSupplier(apSupplier);

				} else { // equals to voucher date

					apSupplierBalance.setSbBalance(apSupplierBalance.getSbBalance() + CHK_AMNT);

				}

			} else {

				// create new balance

				LocalApSupplierBalance apNewSupplierBalance = apSupplierBalanceHome.create(
						CHK_DT, CHK_AMNT, AD_CMPNY);

				//apSupplier.addApSupplierBalance(apNewSupplierBalance);
				apNewSupplierBalance.setApSupplier(apSupplier);

			}

			// propagate to subsequent balances if necessary

			apSupplierBalances = apSupplierBalanceHome.findByAfterVouDateAndSplSupplierCode(CHK_DT, apSupplier.getSplSupplierCode(), AD_CMPNY);

			Iterator i = apSupplierBalances.iterator();

			while (i.hasNext()) {

				LocalApSupplierBalance apSupplierBalance = (LocalApSupplierBalance)i.next();

				apSupplierBalance.setSbBalance(apSupplierBalance.getSbBalance() + CHK_AMNT);

			}

		} catch (Exception ex) {

			Debug.printStackTrace(ex);
			getSessionContext().setRollbackOnly();
			throw new EJBException(ex.getMessage());

		}

	}

	private void postToGl(LocalGlAccountingCalendarValue glAccountingCalendarValue,
			LocalGlChartOfAccount glChartOfAccount,
			boolean isCurrentAcv, byte isDebit, double JL_AMNT, Integer AD_CMPNY) {

		Debug.print("ApDirectCheckEntryControllerBean postToGl");

		LocalGlChartOfAccountBalanceHome glChartOfAccountBalanceHome = null;
		LocalAdCompanyHome adCompanyHome = null;


		// Initialize EJB Home

		try {

			glChartOfAccountBalanceHome = (LocalGlChartOfAccountBalanceHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlChartOfAccountBalanceHome.JNDI_NAME, LocalGlChartOfAccountBalanceHome.class);
			adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);


		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}

		try {

			LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);

			LocalGlChartOfAccountBalance glChartOfAccountBalance =
				glChartOfAccountBalanceHome.findByAcvCodeAndCoaCode(
						glAccountingCalendarValue.getAcvCode(),
						glChartOfAccount.getCoaCode(), AD_CMPNY);

			short FC_EXTNDD_PRCSN = adCompany.getGlFunctionalCurrency().getFcPrecision();

			if (((glChartOfAccount.getCoaAccountType().equals("ASSET") || glChartOfAccount.getCoaAccountType().equals("EXPENSE")) &&
					isDebit == EJBCommon.TRUE) ||
					(!glChartOfAccount.getCoaAccountType().equals("ASSET") && !glChartOfAccount.getCoaAccountType().equals("EXPENSE") &&
							isDebit == EJBCommon.FALSE)) {

				glChartOfAccountBalance.setCoabEndingBalance(
						EJBCommon.roundIt(glChartOfAccountBalance.getCoabEndingBalance() + JL_AMNT, FC_EXTNDD_PRCSN));

				if (!isCurrentAcv) {

					glChartOfAccountBalance.setCoabBeginningBalance(
							EJBCommon.roundIt(glChartOfAccountBalance.getCoabBeginningBalance() + JL_AMNT, FC_EXTNDD_PRCSN));

				}


			} else {

				glChartOfAccountBalance.setCoabEndingBalance(
						EJBCommon.roundIt(glChartOfAccountBalance.getCoabEndingBalance() - JL_AMNT, FC_EXTNDD_PRCSN));

				if (!isCurrentAcv) {

					glChartOfAccountBalance.setCoabBeginningBalance(
							EJBCommon.roundIt(glChartOfAccountBalance.getCoabBeginningBalance() - JL_AMNT, FC_EXTNDD_PRCSN));

				}

			}

			if (isCurrentAcv) {

				if (isDebit == EJBCommon.TRUE) {

					glChartOfAccountBalance.setCoabTotalDebit(
							EJBCommon.roundIt(glChartOfAccountBalance.getCoabTotalDebit() + JL_AMNT, FC_EXTNDD_PRCSN));

				} else {

					glChartOfAccountBalance.setCoabTotalCredit(
							EJBCommon.roundIt(glChartOfAccountBalance.getCoabTotalCredit() + JL_AMNT, FC_EXTNDD_PRCSN));
				}

			}

		} catch (Exception ex) {

			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());

		}


	}







	private void postToGlInvestor(LocalGlAccountingCalendarValue glAccountingCalendarValue,
		      LocalApSupplier apSupplier,
		      boolean isCurrentAcv, byte isDebit, double JL_AMNT, Integer AD_CMPNY) throws FinderException,GlJREffectiveDateNoPeriodExistException{

		      Debug.print("ApDirectCheckEntryControllerBean postToGlInvestor");

		      LocalGlChartOfAccountBalanceHome glChartOfAccountBalanceHome = null;
		      LocalAdCompanyHome adCompanyHome = null;

		      LocalGlInvestorAccountBalanceHome glInvestorAccountBalanceHome = null;

		       // Initialize EJB Home

		       try {

		           glChartOfAccountBalanceHome = (LocalGlChartOfAccountBalanceHome)EJBHomeFactory.
		              lookUpLocalHome(LocalGlChartOfAccountBalanceHome.JNDI_NAME, LocalGlChartOfAccountBalanceHome.class);
		           adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
		              lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
		           glInvestorAccountBalanceHome = (LocalGlInvestorAccountBalanceHome)EJBHomeFactory.
				              lookUpLocalHome(LocalGlInvestorAccountBalanceHome.JNDI_NAME, LocalGlInvestorAccountBalanceHome.class);


		       } catch (NamingException ex) {

		           throw new EJBException(ex.getMessage());

		       }

		       try {

                            LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);

                             System.out.println("account cal val code: " + glAccountingCalendarValue.getAcvCode());
                            System.out.println(" supplier code: " + apSupplier.getSplCode());

                             LocalGlInvestorAccountBalance glInvestorAccountBalance =null;
                            try{
                                glInvestorAccountBalance =
                                             glInvestorAccountBalanceHome.findByAcvCodeAndSplCode(
                                                             glAccountingCalendarValue.getAcvCode(),
                                                       apSupplier.getSplCode(), AD_CMPNY);
                            }catch(FinderException ex){
                                throw new GlJREffectiveDateNoPeriodExistException();
                            }
                              //  LocalGlInvestorAccountBalance


                                short FC_EXTNDD_PRCSN = adCompany.getGlFunctionalCurrency().getFcPrecision();


                                glInvestorAccountBalance.setIrabEndingBalance(
                                                    EJBCommon.roundIt(glInvestorAccountBalance.getIrabEndingBalance() - JL_AMNT, FC_EXTNDD_PRCSN));


                            if (!isCurrentAcv) {

                                glInvestorAccountBalance.setIrabBeginningBalance(
                                        EJBCommon.roundIt(glInvestorAccountBalance.getIrabBeginningBalance() - JL_AMNT, FC_EXTNDD_PRCSN));

                            }

                            if (isCurrentAcv) {

                                   glInvestorAccountBalance.setIrabTotalDebit(
                                                          EJBCommon.roundIt(glInvestorAccountBalance.getIrabTotalDebit() + JL_AMNT, FC_EXTNDD_PRCSN));

                            }
                       } catch (GlJREffectiveDateNoPeriodExistException ex){
                           throw ex;
		       } catch (Exception ex) {

		       	   Debug.printStackTrace(ex);
		       	   throw new EJBException(ex.getMessage());

		       }

		   }

	private void postToInv(LocalApVoucherLineItem apVoucherLineItem, Date CST_DT, double CST_QTY_RCVD, double CST_ITM_CST,
			double CST_RMNNG_QTY, double CST_RMNNG_VL, double CST_VRNC_VL, String USR_NM, Integer AD_BRNCH, Integer AD_CMPNY)throws
			AdPRFCoaGlVarianceAccountNotFoundException {

		Debug.print("ApDirectCheckEntryControllerBean postToInv");

		LocalInvCostingHome invCostingHome = null;
		LocalAdPreferenceHome adPreferenceHome = null;
		LocalAdCompanyHome adCompanyHome = null;
    	LocalInvAdjustmentLineHome invAdjustmentLineHome = null;

		// Initialize EJB Home

		try {

			invCostingHome = (LocalInvCostingHome)EJBHomeFactory.
			lookUpLocalHome(LocalInvCostingHome.JNDI_NAME, LocalInvCostingHome.class);
			adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);
			adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
			invAdjustmentLineHome = (LocalInvAdjustmentLineHome)EJBHomeFactory.
			lookUpLocalHome(LocalInvAdjustmentLineHome.JNDI_NAME, LocalInvAdjustmentLineHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}

		try {

			LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);
			LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
			LocalInvItemLocation invItemLocation = apVoucherLineItem.getInvItemLocation();
			int CST_LN_NMBR = 0;

			CST_QTY_RCVD = EJBCommon.roundIt(CST_QTY_RCVD, adPreference.getPrfInvQuantityPrecisionUnit());
			CST_ITM_CST = EJBCommon.roundIt(CST_ITM_CST, adCompany.getGlFunctionalCurrency().getFcPrecision());
			CST_RMNNG_QTY = EJBCommon.roundIt(CST_RMNNG_QTY, adPreference.getPrfInvQuantityPrecisionUnit());
			CST_RMNNG_VL = EJBCommon.roundIt(CST_RMNNG_VL, adCompany.getGlFunctionalCurrency().getFcPrecision());

			try {

				// generate line number

				LocalInvCosting invCurrentCosting = invCostingHome.getByMaxCstLineNumberAndCstDateToLongAndIiNameAndLocName(CST_DT.getTime(), invItemLocation.getInvItem().getIiName(), invItemLocation.getInvLocation().getLocName(), AD_BRNCH, AD_CMPNY);
				CST_LN_NMBR = invCurrentCosting.getCstLineNumber() + 1;

			} catch (FinderException ex) {

				CST_LN_NMBR = 1;

			}

	           //void subsequent cost variance adjustments
	           Collection invAdjustmentLines = invAdjustmentLineHome.findUnvoidAndIsCostVarianceGreaterThanAdjDateAndIlCodeAndBrCode(
	           		CST_DT, invItemLocation.getIlCode(), AD_BRNCH, AD_CMPNY);
	           Iterator i = invAdjustmentLines.iterator();

	           while (i.hasNext()){

	           	LocalInvAdjustmentLine invAdjustmentLine = (LocalInvAdjustmentLine) i.next();
	           	this.voidInvAdjustment(invAdjustmentLine.getInvAdjustment(), AD_BRNCH, AD_CMPNY);

	           }

	           String prevExpiryDates = "";
	           String miscListPrpgt ="";
	           double qtyPrpgt = 0;
	           try {
	        	   LocalInvCosting prevCst = invCostingHome.getByMaxCstDateToLongAndMaxCstLineNumberAndLessThanEqualCstDateAndRemainingQuantityNotEqualToZeroAndIlCode(
	        			   CST_DT, invItemLocation.getIlCode(), AD_BRNCH, AD_CMPNY);

	        	   prevExpiryDates = prevCst.getCstExpiryDate();
	        	   qtyPrpgt = prevCst.getCstRemainingQuantity();

	        	   if (prevExpiryDates==null){
	        		   prevExpiryDates="";
	        	   }

	           }catch (Exception ex){

	           }

			// create costing
			LocalInvCosting invCosting = invCostingHome.create(CST_DT, CST_DT.getTime(), CST_LN_NMBR, CST_QTY_RCVD, CST_ITM_CST, 0d, 0d, 0d, 0d, 0d, 0d, CST_RMNNG_QTY, CST_RMNNG_VL, 0d, 0d, 0d, AD_BRNCH, AD_CMPNY);
			//invItemLocation.addInvCosting(invCosting);
			invCosting.setInvItemLocation(invItemLocation);
			invCosting.setApVoucherLineItem(apVoucherLineItem);

//			Get Latest Expiry Dates

			if(prevExpiryDates!=null && prevExpiryDates!="" && prevExpiryDates.length()!=0){
				System.out.println("apPurchaseOrderLine.getPlMisc(): "+apVoucherLineItem.getVliMisc().length());
				if(apVoucherLineItem.getVliMisc()!=null && apVoucherLineItem.getVliMisc()!="" && apVoucherLineItem.getVliMisc().length()!=0){
					double qty2Prpgt = Double.parseDouble(this.getQuantityExpiryDates(apVoucherLineItem.getVliMisc()));
					String miscList2Prpgt = this.propagateExpiryDates(apVoucherLineItem.getVliMisc(), qty2Prpgt);

					String propagateMiscPrpgt = miscList2Prpgt + prevExpiryDates;

					invCosting.setCstExpiryDate(propagateMiscPrpgt);
				}else{
					invCosting.setCstExpiryDate(prevExpiryDates);
				}


			}else{
				if(apVoucherLineItem.getVliMisc()!=null && apVoucherLineItem.getVliMisc()!="" && apVoucherLineItem.getVliMisc().length()!=0){
					double initialQty = Double.parseDouble(this.getQuantityExpiryDates(apVoucherLineItem.getVliMisc()));
					String initialPrpgt = this.propagateExpiryDates(apVoucherLineItem.getVliMisc(), initialQty);

					invCosting.setCstExpiryDate(initialPrpgt);
				}else{
					invCosting.setCstExpiryDate(prevExpiryDates);
				}
			}

			// if cost variance is not 0, generate cost variance for the transaction
			if(CST_VRNC_VL != 0) {

				this.generateCostVariance(invCosting.getInvItemLocation(), CST_VRNC_VL,
						"APCHK" + apVoucherLineItem.getApCheck().getChkDocumentNumber(),
						apVoucherLineItem.getApCheck().getChkDescription(),
						apVoucherLineItem.getApCheck().getChkDate(), USR_NM, AD_BRNCH, AD_CMPNY);

			}

			// propagate balance if necessary
			Collection invCostings = invCostingHome.findByGreaterThanCstDateAndIiNameAndLocName(CST_DT, invItemLocation.getInvItem().getIiName(), invItemLocation.getInvLocation().getLocName(), AD_BRNCH, AD_CMPNY);

			i = invCostings.iterator();
			String miscList = "";
			if(apVoucherLineItem.getVliMisc()!=null && apVoucherLineItem.getVliMisc()!="" && apVoucherLineItem.getVliMisc().length()!=0){
				double qty = Double.parseDouble(this.getQuantityExpiryDates(apVoucherLineItem.getVliMisc()));
				miscList = this.propagateExpiryDates(apVoucherLineItem.getVliMisc(), qty);

				System.out.println("miscList Propagate:" + miscList);
			}

			while (i.hasNext()) {

				LocalInvCosting invPropagatedCosting = (LocalInvCosting)i.next();

				invPropagatedCosting.setCstRemainingQuantity(invPropagatedCosting.getCstRemainingQuantity() + CST_QTY_RCVD);
				invPropagatedCosting.setCstRemainingValue(invPropagatedCosting.getCstRemainingValue() + CST_ITM_CST);
				if(apVoucherLineItem.getVliMisc()!=null && apVoucherLineItem.getVliMisc()!="" && apVoucherLineItem.getVliMisc().length()!=0){
					System.out.println("invPropagatedCosting.getCstExpiryDate() : " + invPropagatedCosting.getCstExpiryDate());
					String propagateMisc = invPropagatedCosting.getCstExpiryDate() + miscList;

					invPropagatedCosting.setCstExpiryDate(propagateMisc);
				}else{
					invPropagatedCosting.setCstExpiryDate(prevExpiryDates);
				}
			}
			// regenerate cost variance
			this.regenerateCostVariance(invCostings, invCosting, AD_BRNCH, AD_CMPNY);

		} catch (AdPRFCoaGlVarianceAccountNotFoundException ex) {

			getSessionContext().setRollbackOnly();
			throw ex;

		} catch (Exception ex) {

			Debug.printStackTrace(ex);
			getSessionContext().setRollbackOnly();
			throw new EJBException(ex.getMessage());

		}

	}

	public String getQuantityExpiryDates(String qntty){


		 String[] arrayMisc = qntty.split("_");
		 String y = arrayMisc[0];

		/*
    	String separator = "$";
    	System.out.println("qnnty expry date :" + qntty);
    	// Remove first $ character
    	qntty = qntty.substring(1);
dsad
    	// Counter
    	int start = 0;
    	int nextIndex = qntty.indexOf(separator, start);
    	int length = nextIndex - start;
    	String y;
    	y = (qntty.substring(start, start + length));
    	System.out.println("Y " + y);
		*/
    	return y;
    }

    public String propagateExpiryDates(String misc, double qty) throws Exception {
    	//ActionErrors errors = new ActionErrors();
    	/*
    	Debug.print("ApReceivingItemControllerBean getExpiryDates");

    	String separator = "$";

    	// Remove first $ character
    	misc = misc.substring(1);

    	// Counter
    	int start = 0;
    	int nextIndex = misc.indexOf(separator, start);
    	int length = nextIndex - start;

    	System.out.println("qty" + qty);
    	String miscList = new String();

		for(int x=0; x<qty; x++) {

			// Date
			start = nextIndex + 1;
			nextIndex = misc.indexOf(separator, start);
			length = nextIndex - start;

	        try {

	        	miscList = miscList + "$" +(misc.substring(start, start + length));
	        } catch (Exception ex) {

	        	throw ex;
	        }


		}

		miscList = miscList+"$";
		System.out.println("miscList :" + miscList);
		return (miscList);
		*/

		return misc;
    }
	private double convertByUomFromAndItemAndQuantity(LocalInvUnitOfMeasure invFromUnitOfMeasure, LocalInvItem invItem, double QTY_RCVD, Integer AD_CMPNY) {

		Debug.print("ApDirectCheckEntryControllerBean convertByUomFromAndItemAndQuantity");

		LocalAdPreferenceHome adPreferenceHome = null;
		LocalInvUnitOfMeasureConversionHome invUnitOfMeasureConversionHome = null;

		// Initialize EJB Home

		try {

			adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);
			invUnitOfMeasureConversionHome = (LocalInvUnitOfMeasureConversionHome)EJBHomeFactory.
        		lookUpLocalHome(LocalInvUnitOfMeasureConversionHome.JNDI_NAME, LocalInvUnitOfMeasureConversionHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}

		try {

			LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);

			LocalInvUnitOfMeasureConversion invUnitOfMeasureConversion = invUnitOfMeasureConversionHome.findUmcByIiNameAndUomName(invItem.getIiName(), invFromUnitOfMeasure.getUomName(), AD_CMPNY);
            LocalInvUnitOfMeasureConversion invDefaultUomConversion = invUnitOfMeasureConversionHome.findUmcByIiNameAndUomName(invItem.getIiName(), invItem.getInvUnitOfMeasure().getUomName(), AD_CMPNY);

        	return EJBCommon.roundIt(QTY_RCVD * invDefaultUomConversion.getUmcConversionFactor() / invUnitOfMeasureConversion.getUmcConversionFactor(), adPreference.getPrfInvQuantityPrecisionUnit());

		} catch (Exception ex) {

			Debug.printStackTrace(ex);
			getSessionContext().setRollbackOnly();
			throw new EJBException(ex.getMessage());

		}

	}

    private void voidInvAdjustment(LocalInvAdjustment invAdjustment, Integer AD_BRNCH, Integer AD_CMPNY){

    	Debug.print("ApDirectCheckEntryControllerBean voidInvAdjustment");

    	try{

    			Collection invDistributionRecords = invAdjustment.getInvDistributionRecords();
    			ArrayList list = new ArrayList();

    			Iterator i = invDistributionRecords.iterator();

    			while (i.hasNext()) {

    				LocalInvDistributionRecord invDistributionRecord = (LocalInvDistributionRecord)i.next();

    				list.add(invDistributionRecord);

    			}

    			i = list.iterator();

    			while (i.hasNext()) {

    				LocalInvDistributionRecord invDistributionRecord = (LocalInvDistributionRecord)i.next();

    				this.addInvDrEntry(invAdjustment.getInvDrNextLine(), invDistributionRecord.getDrClass(),
    						invDistributionRecord.getDrDebit() == EJBCommon.TRUE ? EJBCommon.FALSE : EJBCommon.TRUE,
    								invDistributionRecord.getDrAmount(), EJBCommon.TRUE,
									invDistributionRecord.getInvChartOfAccount().getCoaCode(), invAdjustment, AD_BRNCH, AD_CMPNY);

    			}

    			Collection invAjustmentLines = invAdjustment.getInvAdjustmentLines();
    			i = invAjustmentLines.iterator();
    			list.clear();

    			while (i.hasNext()) {

    				LocalInvAdjustmentLine invAdjustmentLine = (LocalInvAdjustmentLine)i.next();

    				list.add(invAdjustmentLine);

    			}

    			i = list.iterator();

    			while (i.hasNext()) {

    				LocalInvAdjustmentLine invAdjustmentLine = (LocalInvAdjustmentLine)i.next();

    				this.addInvAlEntry(invAdjustmentLine.getInvItemLocation(),
    						invAdjustment, (invAdjustmentLine.getAlUnitCost()) * - 1, EJBCommon.TRUE, AD_CMPNY);

    			}

    			invAdjustment.setAdjVoid(EJBCommon.TRUE);

    			this.executeInvAdjPost(invAdjustment.getAdjCode(), invAdjustment.getAdjLastModifiedBy(), AD_BRNCH, AD_CMPNY);

    	} catch (Exception ex) {

    		Debug.printStackTrace(ex);
    		getSessionContext().setRollbackOnly();
    		throw new EJBException(ex.getMessage());

    	}

    }

    private void generateCostVariance(LocalInvItemLocation invItemLocation, double CST_VRNC_VL, String ADJ_RFRNC_NMBR,
    		String ADJ_DSCRPTN, Date ADJ_DT, String USR_NM, Integer AD_BRNCH,
			Integer AD_CMPNY) throws
			AdPRFCoaGlVarianceAccountNotFoundException {
    	/*
    	Debug.print("ApDirectCheckEntryControllerBean generateCostVariance");

    	LocalAdPreferenceHome adPreferenceHome = null;
    	LocalGlChartOfAccountHome glChartOfAccountHome = null;
    	LocalAdBranchItemLocationHome adBranchItemLocationHome = null;

    	// Initialize EJB Home

    	try {

    		adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);
    		glChartOfAccountHome = (LocalGlChartOfAccountHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlChartOfAccountHome.JNDI_NAME, LocalGlChartOfAccountHome.class);
    		adBranchItemLocationHome = (LocalAdBranchItemLocationHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdBranchItemLocationHome.JNDI_NAME, LocalAdBranchItemLocationHome.class);


    	} catch (NamingException ex) {

    		throw new EJBException(ex.getMessage());

    	}

    	try{

    		LocalInvAdjustment newInvAdjustment = this.saveInvAdjustment(ADJ_RFRNC_NMBR, ADJ_DSCRPTN, ADJ_DT, USR_NM, AD_BRNCH,
    				AD_CMPNY);
    		LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);
    		LocalGlChartOfAccount glCoaVarianceAccount = null;

    		if(adPreference.getPrfInvGlCoaVarianceAccount() == null)
    			throw new AdPRFCoaGlVarianceAccountNotFoundException();

    		try{

    			glCoaVarianceAccount = glChartOfAccountHome.findByPrimaryKey(adPreference.getPrfInvGlCoaVarianceAccount());
    			//glCoaVarianceAccount.addInvAdjustment(newInvAdjustment);
    			newInvAdjustment.setGlChartOfAccount(glCoaVarianceAccount);

    		} catch (FinderException ex) {

    			throw new AdPRFCoaGlVarianceAccountNotFoundException();

    		}

    		LocalInvAdjustmentLine invAdjustmentLine = this.addInvAlEntry(invItemLocation, newInvAdjustment,
    				CST_VRNC_VL, EJBCommon.FALSE, AD_CMPNY);

    		// check for branch mapping

    		LocalAdBranchItemLocation adBranchItemLocation = null;

    		try{

    			adBranchItemLocation = adBranchItemLocationHome.findBilByIlCodeAndBrCode(
    					invAdjustmentLine.getInvItemLocation().getIlCode(), AD_BRNCH, AD_CMPNY);

    		} catch (FinderException ex) {

    		}

    		LocalGlChartOfAccount glInventoryChartOfAccount = null;

    		if (adBranchItemLocation == null) {

    			glInventoryChartOfAccount = glChartOfAccountHome.findByPrimaryKey(
    					invAdjustmentLine.getInvItemLocation().getIlGlCoaInventoryAccount());
    		} else {

    			glInventoryChartOfAccount = glChartOfAccountHome.findByPrimaryKey(
    					adBranchItemLocation.getBilCoaGlInventoryAccount());

    		}


    		boolean isDebit = CST_VRNC_VL < 0 ? false : true;

    		//inventory dr
    		this.addInvDrEntry(newInvAdjustment.getInvDrNextLine(), "INVENTORY",
    				isDebit == true ? EJBCommon.TRUE : EJBCommon.FALSE, Math.abs(CST_VRNC_VL), EJBCommon.FALSE,
    						glInventoryChartOfAccount.getCoaCode(), newInvAdjustment, AD_BRNCH, AD_CMPNY);

    		//variance dr
    		this.addInvDrEntry(newInvAdjustment.getInvDrNextLine(),"VARIANCE",
    				!isDebit == true ? EJBCommon.TRUE : EJBCommon.FALSE, Math.abs(CST_VRNC_VL), EJBCommon.FALSE,
    						glCoaVarianceAccount.getCoaCode(), newInvAdjustment, AD_BRNCH, AD_CMPNY);

    		this.executeInvAdjPost(newInvAdjustment.getAdjCode(), newInvAdjustment.getAdjLastModifiedBy(), AD_BRNCH,
    				AD_CMPNY);

    	} catch (AdPRFCoaGlVarianceAccountNotFoundException ex) {

    		getSessionContext().setRollbackOnly();
    		throw ex;

    	} catch (Exception ex) {

    		Debug.printStackTrace(ex);
    		getSessionContext().setRollbackOnly();
    		throw new EJBException(ex.getMessage());

    	}*/

    }

    private void regenerateCostVariance(Collection invCostings, LocalInvCosting invCosting, Integer AD_BRNCH, Integer AD_CMPNY)
    throws AdPRFCoaGlVarianceAccountNotFoundException {
    	/*
    	Debug.print("ApDirectCheckEntryControllerBean regenerateCostVariance");

    	try {

    		Iterator i = invCostings.iterator();
    		LocalInvCosting prevInvCosting = invCosting;

    		while (i.hasNext()) {

    			LocalInvCosting invPropagatedCosting = (LocalInvCosting)i.next();

    			if(prevInvCosting.getCstRemainingQuantity() < 0) {

    				double TTL_CST = 0;
    				double QNTY = 0;
    				String ADJ_RFRNC_NMBR = "";
    				String ADJ_DSCRPTN = "";
    				String ADJ_CRTD_BY = "";

    				// get unit cost adjusment, document number and unit of measure
    				if (invPropagatedCosting.getApPurchaseOrderLine() != null) {

    					TTL_CST = invPropagatedCosting.getApPurchaseOrderLine().getPlAmount();
						QNTY =  this.convertByUomFromAndItemAndQuantity(
								invPropagatedCosting.getApPurchaseOrderLine().getInvUnitOfMeasure(),
								invPropagatedCosting.getApPurchaseOrderLine().getInvItemLocation().getInvItem(),
								invPropagatedCosting.getApPurchaseOrderLine().getPlQuantity(), AD_CMPNY);

    					ADJ_DSCRPTN = invPropagatedCosting.getApPurchaseOrderLine().getApPurchaseOrder().getPoDescription();
    					ADJ_CRTD_BY = invPropagatedCosting.getApPurchaseOrderLine().getApPurchaseOrder().getPoPostedBy();
    					ADJ_RFRNC_NMBR = "APRI" +
						invPropagatedCosting.getApPurchaseOrderLine().getApPurchaseOrder().getPoDocumentNumber();

    				} else if (invPropagatedCosting.getApVoucherLineItem() != null){

    					TTL_CST = invPropagatedCosting.getApVoucherLineItem().getVliAmount();
						QNTY = this.convertByUomFromAndItemAndQuantity(
								invPropagatedCosting.getApVoucherLineItem().getInvUnitOfMeasure(),
								invPropagatedCosting.getApVoucherLineItem().getInvItemLocation().getInvItem(),
								invPropagatedCosting.getApVoucherLineItem().getVliQuantity(), AD_CMPNY);

    					if (invPropagatedCosting.getApVoucherLineItem().getApVoucher() != null) {

    						ADJ_DSCRPTN = invPropagatedCosting.getApVoucherLineItem().getApVoucher().getVouDescription();
    						ADJ_CRTD_BY = invPropagatedCosting.getApVoucherLineItem().getApVoucher().getVouPostedBy();
    						ADJ_RFRNC_NMBR = "APVOU" +
							invPropagatedCosting.getApVoucherLineItem().getApVoucher().getVouDocumentNumber();

    					} else if (invPropagatedCosting.getApVoucherLineItem().getApCheck() != null) {

    						ADJ_DSCRPTN = invPropagatedCosting.getApVoucherLineItem().getApCheck().getChkDescription();
    						ADJ_CRTD_BY = invPropagatedCosting.getApVoucherLineItem().getApCheck().getChkPostedBy();
    						ADJ_RFRNC_NMBR = "APCHK" +
							invPropagatedCosting.getApVoucherLineItem().getApCheck().getChkDocumentNumber();

    					}

    				} else if (invPropagatedCosting.getArInvoiceLineItem() != null){

    					QNTY = this.convertByUomFromAndItemAndQuantity(
								invPropagatedCosting.getArInvoiceLineItem().getInvUnitOfMeasure(),
								invPropagatedCosting.getArInvoiceLineItem().getInvItemLocation().getInvItem(),
								invPropagatedCosting.getArInvoiceLineItem().getIliQuantity(), AD_CMPNY);
   						TTL_CST = prevInvCosting.getCstRemainingValue() - invPropagatedCosting.getCstRemainingValue();

   						if(invPropagatedCosting.getArInvoiceLineItem().getArInvoice() != null){

   	   							ADJ_DSCRPTN = invPropagatedCosting.getArInvoiceLineItem().getArInvoice().getInvDescription();
   	   							ADJ_CRTD_BY = invPropagatedCosting.getArInvoiceLineItem().getArInvoice().getInvPostedBy();
   	   							ADJ_RFRNC_NMBR = "ARCM" +
   								invPropagatedCosting.getArInvoiceLineItem().getArInvoice().getInvNumber();

   	   						} else if(invPropagatedCosting.getArInvoiceLineItem().getArReceipt() != null){

   	   							ADJ_DSCRPTN = invPropagatedCosting.getArInvoiceLineItem().getArReceipt().getRctDescription();
   	   							ADJ_CRTD_BY = invPropagatedCosting.getArInvoiceLineItem().getArReceipt().getRctPostedBy();
   	   							ADJ_RFRNC_NMBR = "ARMR" +
   								invPropagatedCosting.getArInvoiceLineItem().getArReceipt().getRctNumber();

   	   						}

    				} else if (invPropagatedCosting.getArSalesOrderInvoiceLine() != null){

   						TTL_CST = prevInvCosting.getCstRemainingValue() - invPropagatedCosting.getCstRemainingValue();
    					QNTY = this.convertByUomFromAndItemAndQuantity(
								invPropagatedCosting.getArSalesOrderInvoiceLine().getArSalesOrderLine().getInvUnitOfMeasure(),
								invPropagatedCosting.getArSalesOrderInvoiceLine().getArSalesOrderLine().getInvItemLocation().getInvItem(),
								invPropagatedCosting.getArSalesOrderInvoiceLine().getSilQuantityDelivered(), AD_CMPNY);

    					ADJ_DSCRPTN = invPropagatedCosting.getArSalesOrderInvoiceLine().getArInvoice().getInvDescription();
    					ADJ_CRTD_BY = invPropagatedCosting.getArSalesOrderInvoiceLine().getArInvoice().getInvPostedBy();
    					ADJ_RFRNC_NMBR = "ARCM" +
						invPropagatedCosting.getArSalesOrderInvoiceLine().getArInvoice().getInvNumber();

    				} else if (invPropagatedCosting.getInvAdjustmentLine() != null){

    					ADJ_DSCRPTN = invPropagatedCosting.getInvAdjustmentLine().getInvAdjustment().getAdjDescription();
    					ADJ_CRTD_BY = invPropagatedCosting.getInvAdjustmentLine().getInvAdjustment().getAdjPostedBy();
    					ADJ_RFRNC_NMBR = "INVADJ" +
						invPropagatedCosting.getInvAdjustmentLine().getInvAdjustment().getAdjDocumentNumber();

    					if(invPropagatedCosting.getInvAdjustmentLine().getAlAdjustQuantity() != 0) {

    						TTL_CST = (invPropagatedCosting.getInvAdjustmentLine().getAlUnitCost() *
    								invPropagatedCosting.getInvAdjustmentLine().getAlAdjustQuantity());
    						QNTY =  this.convertByUomFromAndItemAndQuantity(
    								invPropagatedCosting.getInvAdjustmentLine().getInvUnitOfMeasure(),
									invPropagatedCosting.getInvAdjustmentLine().getInvItemLocation().getInvItem(),
									invPropagatedCosting.getInvAdjustmentLine().getAlAdjustQuantity(), AD_CMPNY);

    					}

    				} else if (invPropagatedCosting.getInvAssemblyTransferLine() != null){

    					QNTY = invPropagatedCosting.getInvAssemblyTransferLine().getAtlAssembleQuantity();
    					TTL_CST = invPropagatedCosting.getInvAssemblyTransferLine().getAtlAssembleCost();

    					ADJ_DSCRPTN = invPropagatedCosting.getInvAssemblyTransferLine().getInvAssemblyTransfer().getAtrDescription();
    					ADJ_CRTD_BY = invPropagatedCosting.getInvAssemblyTransferLine().getInvAssemblyTransfer().getAtrPostedBy();
    					ADJ_RFRNC_NMBR = "INVAT" +
						invPropagatedCosting.getInvAssemblyTransferLine().getInvAssemblyTransfer().getAtrDocumentNumber();

    				} else if (invPropagatedCosting.getInvBranchStockTransferLine() != null){

    					if(invPropagatedCosting.getInvBranchStockTransferLine().getInvBranchStockTransfer().getBstTransferOutNumber()
    							!= null) {

    						TTL_CST = invPropagatedCosting.getInvBranchStockTransferLine().getBslAmount();
							QNTY =  this.convertByUomFromAndItemAndQuantity(
									invPropagatedCosting.getInvBranchStockTransferLine().getInvUnitOfMeasure(),
									invPropagatedCosting.getInvBranchStockTransferLine().getInvItemLocation().getInvItem(),
									invPropagatedCosting.getInvBranchStockTransferLine().getBslQuantityReceived(), AD_CMPNY);

    					} else {

    						TTL_CST = invPropagatedCosting.getInvBranchStockTransferLine().getBslAmount();
							QNTY =  this.convertByUomFromAndItemAndQuantity(
									invPropagatedCosting.getInvBranchStockTransferLine().getInvUnitOfMeasure(),
									invPropagatedCosting.getInvBranchStockTransferLine().getInvItemLocation().getInvItem(),
									invPropagatedCosting.getInvBranchStockTransferLine().getBslQuantity(), AD_CMPNY);
    					}

    					ADJ_DSCRPTN =
    						invPropagatedCosting.getInvBranchStockTransferLine().getInvBranchStockTransfer().getBstDescription();
    					ADJ_CRTD_BY =
    						invPropagatedCosting.getInvBranchStockTransferLine().getInvBranchStockTransfer().getBstPostedBy();
    					ADJ_RFRNC_NMBR = "INVBST" +
						invPropagatedCosting.getInvBranchStockTransferLine().getInvBranchStockTransfer().getBstNumber();

    				} else if (invPropagatedCosting.getInvBuildUnbuildAssemblyLine() != null){

   						TTL_CST = prevInvCosting.getCstRemainingValue() - invPropagatedCosting.getCstRemainingValue();
    					QNTY =  invPropagatedCosting.getInvBuildUnbuildAssemblyLine().getBlBuildQuantity();
    					ADJ_DSCRPTN =
    						invPropagatedCosting.getInvBuildUnbuildAssemblyLine().getInvBuildUnbuildAssembly().getBuaDescription();
    					ADJ_CRTD_BY =
    						invPropagatedCosting.getInvBuildUnbuildAssemblyLine().getInvBuildUnbuildAssembly().getBuaPostedBy();
    					ADJ_RFRNC_NMBR = "INVBUA" +
						invPropagatedCosting.getInvBuildUnbuildAssemblyLine().getInvBuildUnbuildAssembly().getBuaDocumentNumber();

    				} else if (invPropagatedCosting.getInvStockIssuanceLine()!= null){

    					TTL_CST = invPropagatedCosting.getInvStockIssuanceLine().getSilIssueCost();
						QNTY =  this.convertByUomFromAndItemAndQuantity(
								invPropagatedCosting.getInvStockIssuanceLine().getInvUnitOfMeasure(),
								invPropagatedCosting.getInvStockIssuanceLine().getInvItemLocation().getInvItem(),
								invPropagatedCosting.getInvStockIssuanceLine().getSilIssueQuantity(), AD_CMPNY);
    					ADJ_DSCRPTN = invPropagatedCosting.getInvStockIssuanceLine().getInvStockIssuance().getSiDescription();
    					ADJ_CRTD_BY = invPropagatedCosting.getInvStockIssuanceLine().getInvStockIssuance().getSiPostedBy();
    					ADJ_RFRNC_NMBR = "INVSI" +
						invPropagatedCosting.getInvStockIssuanceLine().getInvStockIssuance().getSiDocumentNumber();

    				} else if (invPropagatedCosting.getInvStockTransferLine()!= null) {

    					TTL_CST = invPropagatedCosting.getInvStockTransferLine().getStlAmount();
						QNTY =  this.convertByUomFromAndItemAndQuantity(
								invPropagatedCosting.getInvStockTransferLine().getInvUnitOfMeasure(),
								invPropagatedCosting.getInvStockTransferLine().getInvItem(),
								invPropagatedCosting.getInvStockTransferLine().getStlQuantityDelivered(), AD_CMPNY);
    					ADJ_DSCRPTN = invPropagatedCosting.getInvStockTransferLine().getInvStockTransfer().getStDescription();
    					ADJ_CRTD_BY = invPropagatedCosting.getInvStockTransferLine().getInvStockTransfer().getStPostedBy();
    					ADJ_RFRNC_NMBR = "INVST" +
						invPropagatedCosting.getInvStockTransferLine().getInvStockTransfer().getStDocumentNumber();

    				} else {

    					prevInvCosting = invPropagatedCosting;
    					continue;

    				}

    				// if quantity is equal 0, no variance.
    				if(QNTY == 0) continue;

    				// compute new cost variance
    				double UNT_CST = TTL_CST/QNTY;
    				double CST_VRNC_VL = (invPropagatedCosting.getCstRemainingQuantity() * UNT_CST -
    						invPropagatedCosting.getCstRemainingValue());

    				if(CST_VRNC_VL != 0)
    					this.generateCostVariance(invPropagatedCosting.getInvItemLocation(), CST_VRNC_VL,
    							ADJ_RFRNC_NMBR, ADJ_DSCRPTN, invPropagatedCosting.getCstDate(), ADJ_CRTD_BY, AD_BRNCH, AD_CMPNY);

    			}

    			// set previous costing
    			prevInvCosting = invPropagatedCosting;

    		}

    	} catch (AdPRFCoaGlVarianceAccountNotFoundException ex){

    		throw ex;

    	} catch (Exception ex) {

    		Debug.printStackTrace(ex);
    		getSessionContext().setRollbackOnly();
    		throw new EJBException(ex.getMessage());

    	} */
    }

    private void addInvDrEntry(short DR_LN, String DR_CLSS, byte DR_DBT, double DR_AMNT, byte DR_RVRSL, Integer COA_CODE,
    		LocalInvAdjustment invAdjustment, Integer AD_BRNCH, Integer AD_CMPNY)

    throws GlobalBranchAccountNumberInvalidException {

    	Debug.print("ApDirectCheckEntryControllerBean addInvDrEntry");

    	LocalAdCompanyHome adCompanyHome = null;
    	LocalInvDistributionRecordHome invDistributionRecordHome = null;
    	LocalGlChartOfAccountHome glChartOfAccountHome = null;

    	// Initialize EJB Home

    	try {

    		adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
    		invDistributionRecordHome = (LocalInvDistributionRecordHome)EJBHomeFactory.
			lookUpLocalHome(LocalInvDistributionRecordHome.JNDI_NAME, LocalInvDistributionRecordHome.class);
    		glChartOfAccountHome = (LocalGlChartOfAccountHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlChartOfAccountHome.JNDI_NAME, LocalGlChartOfAccountHome.class);


    	} catch (NamingException ex) {

    		throw new EJBException(ex.getMessage());

    	}

    	try {

    		// get company

    		LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);

    		// validate coa

    		LocalGlChartOfAccount glChartOfAccount = null;

    		try {

    			glChartOfAccount = glChartOfAccountHome.findByCoaCodeAndBranchCode(COA_CODE, AD_BRNCH, AD_CMPNY);

    		} catch(FinderException ex) {

    			throw new GlobalBranchAccountNumberInvalidException ();

    		}

    		// create distribution record

    		LocalInvDistributionRecord invDistributionRecord = invDistributionRecordHome.create(DR_LN, DR_CLSS, DR_DBT,
    				EJBCommon.roundIt(DR_AMNT, adCompany.getGlFunctionalCurrency().getFcPrecision()), DR_RVRSL, EJBCommon.FALSE,
					 AD_CMPNY);

    		//invAdjustment.addInvDistributionRecord(invDistributionRecord);
    		invDistributionRecord.setInvAdjustment(invAdjustment);
    		//glChartOfAccount.addInvDistributionRecord(invDistributionRecord);
    		invDistributionRecord.setInvChartOfAccount(glChartOfAccount);

    	} catch(GlobalBranchAccountNumberInvalidException ex) {

    		throw new GlobalBranchAccountNumberInvalidException ();

    	} catch (Exception ex) {

    		Debug.printStackTrace(ex);
    		getSessionContext().setRollbackOnly();
    		throw new EJBException(ex.getMessage());

    	}

    }

    private void executeInvAdjPost(Integer ADJ_CODE, String USR_NM, Integer AD_BRNCH, Integer AD_CMPNY) throws
	GlobalRecordAlreadyDeletedException,
	GlobalTransactionAlreadyPostedException,
	GlJREffectiveDateNoPeriodExistException,
	GlJREffectiveDatePeriodClosedException,
	GlobalJournalNotBalanceException,
	GlobalBranchAccountNumberInvalidException {

    	Debug.print("ApDirectCheckEntryControllerBean executeInvAdjPost");

    	LocalInvAdjustmentHome invAdjustmentHome = null;
    	LocalAdCompanyHome adCompanyHome = null;
    	LocalAdPreferenceHome adPreferenceHome = null;
    	LocalGlSetOfBookHome glSetOfBookHome = null;
    	LocalGlAccountingCalendarValueHome glAccountingCalendarValueHome = null;
    	LocalGlJournalHome glJournalHome = null;
    	LocalGlJournalBatchHome glJournalBatchHome = null;
    	LocalGlSuspenseAccountHome glSuspenseAccountHome = null;
    	LocalGlJournalLineHome glJournalLineHome = null;
    	LocalGlJournalSourceHome glJournalSourceHome = null;
    	LocalGlJournalCategoryHome glJournalCategoryHome = null;
    	LocalGlFunctionalCurrencyHome glFunctionalCurrencyHome = null;
    	LocalInvDistributionRecordHome invDistributionRecordHome = null;
    	LocalInvCostingHome invCostingHome = null;
    	LocalGlChartOfAccountHome glChartOfAccountHome = null;
    	LocalInvAdjustmentLineHome invAdjustmentLineHome = null;


    	// Initialize EJB Home

    	try {

    		invAdjustmentHome = (LocalInvAdjustmentHome)EJBHomeFactory.
			lookUpLocalHome(LocalInvAdjustmentHome.JNDI_NAME, LocalInvAdjustmentHome.class);
    		adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
    		adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);
    		glSetOfBookHome = (LocalGlSetOfBookHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlSetOfBookHome.JNDI_NAME, LocalGlSetOfBookHome.class);
    		glAccountingCalendarValueHome = (LocalGlAccountingCalendarValueHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlAccountingCalendarValueHome.JNDI_NAME, LocalGlAccountingCalendarValueHome.class);
    		glJournalHome = (LocalGlJournalHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlJournalHome.JNDI_NAME, LocalGlJournalHome.class);
    		glJournalBatchHome = (LocalGlJournalBatchHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlJournalBatchHome.JNDI_NAME, LocalGlJournalBatchHome.class);
    		glSuspenseAccountHome = (LocalGlSuspenseAccountHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlSuspenseAccountHome.JNDI_NAME, LocalGlSuspenseAccountHome.class);
    		glJournalLineHome = (LocalGlJournalLineHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlJournalLineHome.JNDI_NAME, LocalGlJournalLineHome.class);
    		glJournalSourceHome = (LocalGlJournalSourceHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlJournalSourceHome.JNDI_NAME, LocalGlJournalSourceHome.class);
    		glJournalCategoryHome = (LocalGlJournalCategoryHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlJournalCategoryHome.JNDI_NAME, LocalGlJournalCategoryHome.class);
    		glFunctionalCurrencyHome = (LocalGlFunctionalCurrencyHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlFunctionalCurrencyHome.JNDI_NAME, LocalGlFunctionalCurrencyHome.class);
    		invDistributionRecordHome = (LocalInvDistributionRecordHome)EJBHomeFactory.
			lookUpLocalHome(LocalInvDistributionRecordHome.JNDI_NAME, LocalInvDistributionRecordHome.class);
    		invCostingHome = (LocalInvCostingHome)EJBHomeFactory.
			lookUpLocalHome(LocalInvCostingHome.JNDI_NAME, LocalInvCostingHome.class);
    		glChartOfAccountHome = (LocalGlChartOfAccountHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlChartOfAccountHome.JNDI_NAME, LocalGlChartOfAccountHome.class);
    		invAdjustmentLineHome = (LocalInvAdjustmentLineHome)EJBHomeFactory.
			lookUpLocalHome(LocalInvAdjustmentLineHome.JNDI_NAME, LocalInvAdjustmentLineHome.class);


    	} catch (NamingException ex) {

    		throw new EJBException(ex.getMessage());

    	}

    	try {

    		// validate if adjustment is already deleted

    		LocalInvAdjustment invAdjustment = null;

    		try {

    			invAdjustment = invAdjustmentHome.findByPrimaryKey(ADJ_CODE);

    		} catch (FinderException ex) {

    			throw new GlobalRecordAlreadyDeletedException();

    		}

    		// validate if adjustment is already posted or void

    		if (invAdjustment.getAdjPosted() == EJBCommon.TRUE) {

    			if (invAdjustment.getAdjVoid() != EJBCommon.TRUE)
    			throw new GlobalTransactionAlreadyPostedException();

    		}

    		Collection invAdjustmentLines = null;

    		if(invAdjustment.getAdjVoid() == EJBCommon.FALSE)
    			invAdjustmentLines = invAdjustmentLineHome.findByAlVoidAndAdjCode(EJBCommon.FALSE, invAdjustment.getAdjCode(), AD_CMPNY);
    		else
    			invAdjustmentLines = invAdjustmentLineHome.findByAlVoidAndAdjCode(EJBCommon.TRUE, invAdjustment.getAdjCode(), AD_CMPNY);


    		Iterator i = invAdjustmentLines.iterator();

    		while(i.hasNext()) {


    			LocalInvAdjustmentLine invAdjustmentLine = (LocalInvAdjustmentLine) i.next();

    			LocalInvCosting invCosting =
    				invCostingHome.getByMaxCstDateToLongAndMaxCstLineNumberAndLessThanEqualCstDateAndIiNameAndLocName(
    						invAdjustment.getAdjDate(), invAdjustmentLine.getInvItemLocation().getInvItem().getIiName(),
    						invAdjustmentLine.getInvItemLocation().getInvLocation().getLocName(), AD_BRNCH, AD_CMPNY);

				this.postInvAdjustmentToInventory(invAdjustmentLine, invAdjustment.getAdjDate(), 0,
						invAdjustmentLine.getAlUnitCost(), invCosting.getCstRemainingQuantity(),
						invCosting.getCstRemainingValue() + invAdjustmentLine.getAlUnitCost(), AD_BRNCH, AD_CMPNY);

    		}

    		// post to gl if necessary

    		LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);
    		LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);

    		// validate if date has no period and period is closed

    			LocalGlSetOfBook glJournalSetOfBook = null;

    			try {

    				glJournalSetOfBook = glSetOfBookHome.findByDate(invAdjustment.getAdjDate(), AD_CMPNY);

    			} catch (FinderException ex) {

    				throw new GlJREffectiveDateNoPeriodExistException();

    			}

    			LocalGlAccountingCalendarValue glAccountingCalendarValue =
    				glAccountingCalendarValueHome.findByAcCodeAndDate(
    						glJournalSetOfBook.getGlAccountingCalendar().getAcCode(), invAdjustment.getAdjDate(), AD_CMPNY);


    			if (glAccountingCalendarValue.getAcvStatus() == 'N' ||
    					glAccountingCalendarValue.getAcvStatus() == 'C' ||
						glAccountingCalendarValue.getAcvStatus() == 'P') {

    				throw new GlJREffectiveDatePeriodClosedException();

    			}

    			// check if invoice is balance if not check suspense posting

    			LocalGlJournalLine glOffsetJournalLine = null;

    			Collection invDistributionRecords = null;

    			if (invAdjustment.getAdjVoid() == EJBCommon.FALSE) {

    				invDistributionRecords = invDistributionRecordHome.findImportableDrByDrReversedAndAdjCode(EJBCommon.FALSE,
    						invAdjustment.getAdjCode(), AD_CMPNY);

    			} else {

    				invDistributionRecords = invDistributionRecordHome.findImportableDrByDrReversedAndAdjCode(EJBCommon.TRUE,
    						invAdjustment.getAdjCode(), AD_CMPNY);

    			}


    			Iterator j = invDistributionRecords.iterator();

    			double TOTAL_DEBIT = 0d;
    			double TOTAL_CREDIT = 0d;

    			while (j.hasNext()) {

    				LocalInvDistributionRecord invDistributionRecord = (LocalInvDistributionRecord)j.next();

    				double DR_AMNT = 0d;

    				DR_AMNT = invDistributionRecord.getDrAmount();

    				if (invDistributionRecord.getDrDebit() == EJBCommon.TRUE) {

    					TOTAL_DEBIT += DR_AMNT;

    				} else {

    					TOTAL_CREDIT += DR_AMNT;

    				}

    			}

    			TOTAL_DEBIT = EJBCommon.roundIt(TOTAL_DEBIT, adCompany.getGlFunctionalCurrency().getFcPrecision());
    			TOTAL_CREDIT = EJBCommon.roundIt(TOTAL_CREDIT, adCompany.getGlFunctionalCurrency().getFcPrecision());

    			if (adPreference.getPrfAllowSuspensePosting() == EJBCommon.TRUE &&
    					TOTAL_DEBIT != TOTAL_CREDIT) {

    				LocalGlSuspenseAccount glSuspenseAccount = null;

    				try {

    					glSuspenseAccount = glSuspenseAccountHome.findByJsNameAndJcName("INVENTORY", "INVENTORY ADJUSTMENTS",
    							AD_CMPNY);

    				} catch (FinderException ex) {

    					throw new GlobalJournalNotBalanceException();

    				}

    				if (TOTAL_DEBIT - TOTAL_CREDIT < 0) {

    					glOffsetJournalLine = glJournalLineHome.create((short)(invDistributionRecords.size() + 1), EJBCommon.TRUE,
    							TOTAL_CREDIT - TOTAL_DEBIT, "", AD_CMPNY);

    				} else {

    					glOffsetJournalLine = glJournalLineHome.create((short)(invDistributionRecords.size() + 1), EJBCommon.FALSE,
    							TOTAL_DEBIT - TOTAL_CREDIT, "", AD_CMPNY);

    				}

    				LocalGlChartOfAccount glChartOfAccount = glSuspenseAccount.getGlChartOfAccount();
    				//glChartOfAccount.addGlJournalLine(glOffsetJournalLine);
    				glOffsetJournalLine.setGlChartOfAccount(glChartOfAccount);


    			} else if (adPreference.getPrfAllowSuspensePosting() == EJBCommon.FALSE &&
    					TOTAL_DEBIT != TOTAL_CREDIT) {

    				throw new GlobalJournalNotBalanceException();

    			}

    			// create journal batch if necessary

    			LocalGlJournalBatch glJournalBatch = null;
    			java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat("MM/dd/yyyy");

    			try {

    				glJournalBatch = glJournalBatchHome.findByJbName("JOURNAL IMPORT " + formatter.format(new Date()) +
    						" INVENTORY ADJUSTMENTS", AD_BRNCH, AD_CMPNY);

    			} catch (FinderException ex) {

    			}

    			if (glJournalBatch == null) {

    				glJournalBatch = glJournalBatchHome.create("JOURNAL IMPORT " + formatter.format(new Date()) +
    						" INVENTORY ADJUSTMENTS", "JOURNAL IMPORT", "CLOSED", EJBCommon.getGcCurrentDateWoTime().getTime(),
							USR_NM, AD_BRNCH, AD_CMPNY);

    			}

    			// create journal entry

    			LocalGlJournal glJournal = glJournalHome.create(invAdjustment.getAdjReferenceNumber(),
    					invAdjustment.getAdjDescription(), invAdjustment.getAdjDate(),
						0.0d, null, invAdjustment.getAdjDocumentNumber(), null, 1d, "N/A", null,
						'N', EJBCommon.TRUE, EJBCommon.FALSE,
						USR_NM, new Date(),
						USR_NM, new Date(),
						null, null,
						USR_NM, EJBCommon.getGcCurrentDateWoTime().getTime(),
						null, null, EJBCommon.FALSE, null,

						AD_BRNCH, AD_CMPNY);

    			LocalGlJournalSource glJournalSource = glJournalSourceHome.findByJsName("INVENTORY", AD_CMPNY);
    			glJournal.setGlJournalSource(glJournalSource);

    			LocalGlFunctionalCurrency glFunctionalCurrency = glFunctionalCurrencyHome.findByFcName(adCompany.getGlFunctionalCurrency().getFcName(), AD_CMPNY);
    			glJournal.setGlFunctionalCurrency(glFunctionalCurrency);

    			LocalGlJournalCategory glJournalCategory = glJournalCategoryHome.findByJcName("INVENTORY ADJUSTMENTS", AD_CMPNY);
    			glJournal.setGlJournalCategory(glJournalCategory);

    			if (glJournalBatch != null) {

    				glJournal.setGlJournalBatch(glJournalBatch);

    			}

    			// create journal lines

    			j = invDistributionRecords.iterator();

    			while (j.hasNext()) {

    				LocalInvDistributionRecord invDistributionRecord = (LocalInvDistributionRecord)j.next();

    				double DR_AMNT = 0d;

    				DR_AMNT = invDistributionRecord.getDrAmount();

    				LocalGlJournalLine glJournalLine = glJournalLineHome.create(invDistributionRecord.getDrLine(),
    						invDistributionRecord.getDrDebit(), DR_AMNT, "", AD_CMPNY);

    				//invDistributionRecord.getInvChartOfAccount().addGlJournalLine(glJournalLine);
    				glJournalLine.setGlChartOfAccount(invDistributionRecord.getInvChartOfAccount());
    				//glJournal.addGlJournalLine(glJournalLine);
    				glJournalLine.setGlJournal(glJournal);

    				invDistributionRecord.setDrImported(EJBCommon.TRUE);


    			}

    			if (glOffsetJournalLine != null) {

    				//glJournal.addGlJournalLine(glOffsetJournalLine);
    				glOffsetJournalLine.setGlJournal(glJournal);

    			}

    			// post journal to gl

    			Collection glJournalLines = glJournal.getGlJournalLines();

    			i = glJournalLines.iterator();

    			while (i.hasNext()) {

    				LocalGlJournalLine glJournalLine = (LocalGlJournalLine)i.next();

    				// post current to current acv

    				this.postToGl(glAccountingCalendarValue,
    						glJournalLine.getGlChartOfAccount(),
							true, glJournalLine.getJlDebit(), glJournalLine.getJlAmount(), AD_CMPNY);


    				// post to subsequent acvs (propagate)

    				Collection glSubsequentAccountingCalendarValues =
    					glAccountingCalendarValueHome.findSubsequentAcvByAcCodeAndAcvPeriodNumber(
    							glJournalSetOfBook.getGlAccountingCalendar().getAcCode(),
								glAccountingCalendarValue.getAcvPeriodNumber(), AD_CMPNY);

    				Iterator acvsIter = glSubsequentAccountingCalendarValues.iterator();

    				while (acvsIter.hasNext()) {

    					LocalGlAccountingCalendarValue glSubsequentAccountingCalendarValue =
    						(LocalGlAccountingCalendarValue)acvsIter.next();

    					this.postToGl(glSubsequentAccountingCalendarValue,
    							glJournalLine.getGlChartOfAccount(),
								false, glJournalLine.getJlDebit(), glJournalLine.getJlAmount(), AD_CMPNY);

    				}

    				// post to subsequent years if necessary

    				Collection glSubsequentSetOfBooks = glSetOfBookHome.findSubsequentSobByAcYear(
    						glJournalSetOfBook.getGlAccountingCalendar().getAcYear(), AD_CMPNY);

    				if (!glSubsequentSetOfBooks.isEmpty() && glJournalSetOfBook.getSobYearEndClosed() == 1) {

    					adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
    					LocalGlChartOfAccount glRetainedEarningsAccount =
    						glChartOfAccountHome.findByCoaAccountNumberAndBranchCode(adCompany.getCmpRetainedEarnings(),
    								AD_BRNCH, AD_CMPNY);

    					Iterator sobIter = glSubsequentSetOfBooks.iterator();

    					while (sobIter.hasNext()) {

    						LocalGlSetOfBook glSubsequentSetOfBook = (LocalGlSetOfBook)sobIter.next();

    						String ACCOUNT_TYPE = glJournalLine.getGlChartOfAccount().getCoaAccountType();

    						// post to subsequent acvs of subsequent set of book(propagate)

    						Collection glAccountingCalendarValues =
    							glAccountingCalendarValueHome.findByAcCode(
    									glSubsequentSetOfBook.getGlAccountingCalendar().getAcCode(), AD_CMPNY);

    						Iterator acvIter = glAccountingCalendarValues.iterator();

    						while (acvIter.hasNext()) {

    							LocalGlAccountingCalendarValue glSubsequentAccountingCalendarValue =
    								(LocalGlAccountingCalendarValue)acvIter.next();

    							if (ACCOUNT_TYPE.equals("ASSET") || ACCOUNT_TYPE.equals("LIABILITY") ||
    									ACCOUNT_TYPE.equals("OWNERS EQUITY")) {

    								this.postToGl(glSubsequentAccountingCalendarValue,
    										glJournalLine.getGlChartOfAccount(),
											false, glJournalLine.getJlDebit(), glJournalLine.getJlAmount(), AD_CMPNY);

    							} else { // revenue & expense

    								this.postToGl(glSubsequentAccountingCalendarValue,
    										glRetainedEarningsAccount,
											false, glJournalLine.getJlDebit(), glJournalLine.getJlAmount(), AD_CMPNY);

    							}

    						}

    						if (glSubsequentSetOfBook.getSobYearEndClosed() == 0) break;

    					}

    				}

    			}

			invAdjustment.setAdjPosted(EJBCommon.TRUE);

    	} catch (GlJREffectiveDateNoPeriodExistException ex) {

    		getSessionContext().setRollbackOnly();
    		throw ex;

    	} catch (GlJREffectiveDatePeriodClosedException ex) {

    		getSessionContext().setRollbackOnly();
    		throw ex;

    	} catch (GlobalJournalNotBalanceException ex) {

    		getSessionContext().setRollbackOnly();
    		throw ex;

    	} catch (GlobalRecordAlreadyDeletedException ex) {

    		getSessionContext().setRollbackOnly();
    		throw ex;

    	} catch (GlobalTransactionAlreadyPostedException ex) {

    		getSessionContext().setRollbackOnly();
    		throw ex;

    	} catch (Exception ex) {

    		Debug.printStackTrace(ex);
    		getSessionContext().setRollbackOnly();
    		throw new EJBException(ex.getMessage());

    	}

    }

private LocalInvAdjustmentLine addInvAlEntry(LocalInvItemLocation invItemLocation, LocalInvAdjustment invAdjustment,
		double CST_VRNC_VL, byte AL_VD, Integer AD_CMPNY) {

	Debug.print("ArMiscReceiptEntryControllerBean addInvAlEntry");

	LocalInvAdjustmentLineHome invAdjustmentLineHome = null;

	// Initialize EJB Home

	try {

		invAdjustmentLineHome = (LocalInvAdjustmentLineHome)EJBHomeFactory.
		lookUpLocalHome(LocalInvAdjustmentLineHome.JNDI_NAME, LocalInvAdjustmentLineHome.class);

	} catch (NamingException ex) {

		throw new EJBException(ex.getMessage());

	}

	try {

		// create dr entry
		LocalInvAdjustmentLine invAdjustmentLine = null;
		invAdjustmentLine = invAdjustmentLineHome.create(CST_VRNC_VL, null, null, 0,0, AL_VD, AD_CMPNY);

		// map adjustment, unit of measure, item location
		//invAdjustment.addInvAdjustmentLine(invAdjustmentLine);
		invAdjustmentLine.setInvAdjustment(invAdjustment);
		//invItemLocation.getInvItem().getInvUnitOfMeasure().addInvAdjustmentLine(invAdjustmentLine);
		invAdjustmentLine.setInvUnitOfMeasure(invItemLocation.getInvItem().getInvUnitOfMeasure());
		//invItemLocation.addInvAdjustmentLine(invAdjustmentLine);
		invAdjustmentLine.setInvItemLocation(invItemLocation);

		return invAdjustmentLine;

	} catch (Exception ex) {

		Debug.printStackTrace(ex);
		getSessionContext().setRollbackOnly();
		throw new EJBException(ex.getMessage());

	}

}

    private LocalInvAdjustment saveInvAdjustment(String ADJ_RFRNC_NMBR, String ADJ_DSCRPTN,
    		Date ADJ_DATE, String USR_NM, Integer AD_BRNCH, Integer AD_CMPNY){

    	Debug.print("ApDirectCheckEntryControllerBean saveInvAdjustment");

    	LocalInvAdjustmentHome invAdjustmentHome = null;
    	LocalAdDocumentSequenceAssignmentHome adDocumentSequenceAssignmentHome = null;
    	LocalAdBranchDocumentSequenceAssignmentHome adBranchDocumentSequenceAssignmentHome = null;
    	// Initialize EJB Home

    	try{

    		invAdjustmentHome = (LocalInvAdjustmentHome)EJBHomeFactory.
			lookUpLocalHome(LocalInvAdjustmentHome.JNDI_NAME, LocalInvAdjustmentHome.class);
			adDocumentSequenceAssignmentHome = (LocalAdDocumentSequenceAssignmentHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdDocumentSequenceAssignmentHome.JNDI_NAME, LocalAdDocumentSequenceAssignmentHome.class);
			adBranchDocumentSequenceAssignmentHome = (LocalAdBranchDocumentSequenceAssignmentHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdBranchDocumentSequenceAssignmentHome.JNDI_NAME, LocalAdBranchDocumentSequenceAssignmentHome.class);

    	} catch (NamingException ex) {

    		throw new EJBException(ex.getMessage());

    	}

    	try{

    		// generate adj document number
    		String ADJ_DCMNT_NMBR = null;

    		LocalAdBranchDocumentSequenceAssignment adBranchDocumentSequenceAssignment = null;
    		LocalAdDocumentSequenceAssignment adDocumentSequenceAssignment = null;

    		try {

    			adDocumentSequenceAssignment = adDocumentSequenceAssignmentHome.findByDcName("INV ADJUSTMENT", AD_CMPNY);

    		} catch (FinderException ex) {

    		}

    		try {

    			adBranchDocumentSequenceAssignment = adBranchDocumentSequenceAssignmentHome.findBdsByDsaCodeAndBrCode(
    					adDocumentSequenceAssignment.getDsaCode(), AD_BRNCH, AD_CMPNY);

    		} catch (FinderException ex) {

    		}

    		while (true) {

    			if (adBranchDocumentSequenceAssignment == null || adBranchDocumentSequenceAssignment.getBdsNextSequence() == null) {

    				try {

    					invAdjustmentHome.findByAdjDocumentNumberAndBrCode(adDocumentSequenceAssignment.getDsaNextSequence(),
    							AD_BRNCH, AD_CMPNY);
    					adDocumentSequenceAssignment.setDsaNextSequence(EJBCommon.incrementStringNumber(
    							adDocumentSequenceAssignment.getDsaNextSequence()));

    				} catch (FinderException ex) {

    					ADJ_DCMNT_NMBR = adDocumentSequenceAssignment.getDsaNextSequence();
    					adDocumentSequenceAssignment.setDsaNextSequence(EJBCommon.incrementStringNumber(
    							adDocumentSequenceAssignment.getDsaNextSequence()));
    					break;

    				}

    			} else {

    				try {

    					invAdjustmentHome.findByAdjDocumentNumberAndBrCode(
    							adBranchDocumentSequenceAssignment.getBdsNextSequence(), AD_BRNCH, AD_CMPNY);
    					adBranchDocumentSequenceAssignment.setBdsNextSequence(EJBCommon.incrementStringNumber(
    							adBranchDocumentSequenceAssignment.getBdsNextSequence()));

    				} catch (FinderException ex) {

    					ADJ_DCMNT_NMBR = adBranchDocumentSequenceAssignment.getBdsNextSequence();
    					adBranchDocumentSequenceAssignment.setBdsNextSequence(EJBCommon.incrementStringNumber(
    							adBranchDocumentSequenceAssignment.getBdsNextSequence()));
    					break;

    				}

    			}

    		}

    		LocalInvAdjustment invAdjustment = invAdjustmentHome.create(ADJ_DCMNT_NMBR, ADJ_RFRNC_NMBR,
    				ADJ_DSCRPTN, ADJ_DATE, "COST-VARIANCE", "N/A", EJBCommon.FALSE, USR_NM, ADJ_DATE, USR_NM, ADJ_DATE, null, null,
					USR_NM, ADJ_DATE, null, null, EJBCommon.TRUE, EJBCommon.FALSE, AD_BRNCH, AD_CMPNY);

    		return invAdjustment;

    	} catch (Exception ex) {

    		Debug.printStackTrace(ex);
    		getSessionContext().setRollbackOnly();
    		throw new EJBException(ex.getMessage());

    	}


    }


    private void createInvTagList(LocalApVoucherLineItem apVoucherLineItem, ArrayList list, Integer AD_CMPNY) throws Exception {

    	Debug.print("ApDirectCheckEntryControllerBean createInvTagList");

    	LocalAdUserHome adUserHome = null;
    	LocalInvTagHome invTagHome = null;

    	// Initialize EJB Home

    	try{
    		adUserHome = (LocalAdUserHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdUserHome.JNDI_NAME, LocalAdUserHome.class);
    		invTagHome = (LocalInvTagHome)EJBHomeFactory.
			lookUpLocalHome(LocalInvTagHome.JNDI_NAME, LocalInvTagHome.class);

    	} catch (NamingException ex) {

    		throw new EJBException(ex.getMessage());

    	}



    	try {
			System.out.println("aabot?");
	  	    	//Iterator t = apPurchaseOrderLine.getInvTag().iterator();
  	  	    Iterator t = list.iterator();

  	  	    LocalInvTag invTag  = null;
  	  	    System.out.println("umabot?");
  	  	    while (t.hasNext()){
  	  	    	InvModTagListDetails tgLstDetails = (InvModTagListDetails)t.next();
  	  	    	System.out.println(tgLstDetails.getTgCustodian() + "<== custodian");
  	  	    	System.out.println(tgLstDetails.getTgSpecs() + "<== specs");
  	  	    	System.out.println(tgLstDetails.getTgPropertyCode() + "<== propertyCode");
  	  	    	System.out.println(tgLstDetails.getTgExpiryDate() + "<== expiryDate");
  	  	    	System.out.println(tgLstDetails.getTgSerialNumber() + "<== serial number");

  	  	    	if (tgLstDetails.getTgCode()==null){
  	  	    		System.out.println("ngcreate ba?");
      	  	    	invTag = invTagHome.create(tgLstDetails.getTgPropertyCode(),
      	  	    			tgLstDetails.getTgSerialNumber(),null,tgLstDetails.getTgExpiryDate(),
      	  	    			tgLstDetails.getTgSpecs(), AD_CMPNY, tgLstDetails.getTgTransactionDate(),
      	  	    			tgLstDetails.getTgType());

      	  	    	invTag.setApVoucherLineItem(apVoucherLineItem);
      	  	    	invTag.setInvItemLocation(apVoucherLineItem.getInvItemLocation());
      	  	    	LocalAdUser adUser = null;
      	  	    	try {
      	  	    		adUser = adUserHome.findByUsrName(tgLstDetails.getTgCustodian(), AD_CMPNY);
      	  	    	}catch(FinderException ex){

      	  	    	}
      	  	    	invTag.setAdUser(adUser);
      	  	    	System.out.println("ngcreate ba?");
  	  	    	}

  	  	    }




		}catch(Exception ex) {
			throw ex;
		}
    }


    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList getAdUsrAll(Integer AD_CMPNY) {

        Debug.print("ApDirectCheckEntryControllerBean getAdUsrAll");

        LocalAdUserHome adUserHome = null;

        LocalAdUser adUser = null;

        Collection adUsers = null;

        ArrayList list = new ArrayList();

        // Initialize EJB Home

        try {

            adUserHome = (LocalAdUserHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdUserHome.JNDI_NAME, LocalAdUserHome.class);

        } catch (NamingException ex) {

            throw new EJBException(ex.getMessage());

        }

        try {

            adUsers = adUserHome.findUsrAll(AD_CMPNY);

        } catch (FinderException ex) {

        } catch (Exception ex) {

            throw new EJBException(ex.getMessage());
        }

        if (adUsers.isEmpty()) {

            return null;

        }

        Iterator i = adUsers.iterator();

        while (i.hasNext()) {

            adUser = (LocalAdUser)i.next();

            list.add(adUser.getUsrName());

        }

        return list;

    }

    private ArrayList getInvTagList(LocalApVoucherLineItem apVoucherLineItem) {

    	Debug.print("ApDirectCheckEntryControllerBean getInvTagList");
    	ArrayList list = new ArrayList();

    	Collection invTags = apVoucherLineItem.getInvTags();
		Iterator x = invTags.iterator();
		while (x.hasNext()) {
			LocalInvTag invTag = (LocalInvTag) x.next();
			InvModTagListDetails tgLstDetails = new InvModTagListDetails();
			tgLstDetails.setTgPropertyCode(invTag.getTgPropertyCode());
			tgLstDetails.setTgSpecs(invTag.getTgSpecs());
			tgLstDetails.setTgExpiryDate(invTag.getTgExpiryDate());
			tgLstDetails.setTgSerialNumber(invTag.getTgSerialNumber());
			try{

				tgLstDetails.setTgCustodian(invTag.getAdUser().getUsrName());
			}
			catch(Exception ex){
				tgLstDetails.setTgCustodian("");
			}

			list.add(tgLstDetails);

			System.out.println(tgLstDetails.getTgPropertyCode() + "<== property code inside controllerbean ");
			System.out.println(tgLstDetails.getTgSpecs() + "<== specs inside controllerbean ");
			System.out.println(list+ "<== taglist inside controllerbean ");

		}

    	return list;

    }



    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public boolean getInvTraceMisc(String II_NAME, Integer AD_CMPNY) {

    	Debug.print("ApDirectCheckEntryControllerBean getInvTraceMisc");

        LocalInvLocationHome invLocationHome = null;
        LocalInvItemHome invItemHome = null;
        Collection invLocations = null;
        ArrayList list = new ArrayList();
        boolean isTraceMisc = false;
        // Initialize EJB Home

        try {

        	invLocationHome = (LocalInvLocationHome)EJBHomeFactory.
                lookUpLocalHome(LocalInvLocationHome.JNDI_NAME, LocalInvLocationHome.class);
        	invItemHome = (LocalInvItemHome)EJBHomeFactory.
                    lookUpLocalHome(LocalInvItemHome.JNDI_NAME, LocalInvItemHome.class);

        } catch (NamingException ex) {

            throw new EJBException(ex.getMessage());

        }

        try {

            LocalInvItem invItem = invItemHome.findByIiName(II_NAME, AD_CMPNY);
            if (invItem.getIiTraceMisc() == 1){
            	isTraceMisc = true;
            }
            return isTraceMisc;
    	} catch (Exception ex) {

    		Debug.printStackTrace(ex);
    		throw new EJBException(ex.getMessage());

    	}

    }




    private void postInvAdjustmentToInventory(LocalInvAdjustmentLine invAdjustmentLine, Date CST_DT, double CST_ADJST_QTY,
    		double CST_ADJST_CST, double CST_RMNNG_QTY, double CST_RMNNG_VL, Integer AD_BRNCH,Integer AD_CMPNY) {

    	Debug.print("ApDirectCheckEntryControllerBean postInvAdjustmentToInventory");

    	LocalInvCostingHome invCostingHome = null;
    	LocalAdPreferenceHome adPreferenceHome = null;
    	LocalAdCompanyHome adCompanyHome = null;

    	// Initialize EJB Home

    	try {

    		invCostingHome = (LocalInvCostingHome)EJBHomeFactory.
			lookUpLocalHome(LocalInvCostingHome.JNDI_NAME, LocalInvCostingHome.class);
    		adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);
    		adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);

    	} catch (NamingException ex) {

    		getSessionContext().setRollbackOnly();
    		throw new EJBException(ex.getMessage());

    	}

    	try {

    		LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);
    		LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
    		LocalInvItemLocation invItemLocation = invAdjustmentLine.getInvItemLocation();
    		int CST_LN_NMBR = 0;

    		CST_ADJST_QTY = EJBCommon.roundIt(CST_ADJST_QTY, adPreference.getPrfInvQuantityPrecisionUnit());
    		CST_ADJST_CST = EJBCommon.roundIt(CST_ADJST_CST, adCompany.getGlFunctionalCurrency().getFcPrecision());
    		CST_RMNNG_QTY = EJBCommon.roundIt(CST_RMNNG_QTY, adPreference.getPrfInvQuantityPrecisionUnit());
    		CST_RMNNG_VL = EJBCommon.roundIt(CST_RMNNG_VL, adCompany.getGlFunctionalCurrency().getFcPrecision());

    		if (CST_ADJST_QTY < 0) {

    			invItemLocation.setIlCommittedQuantity(invItemLocation.getIlCommittedQuantity() - Math.abs(CST_ADJST_QTY));

    		}

    		// create costing

    		try {

    			// generate line number

    			LocalInvCosting invCurrentCosting = invCostingHome.getByMaxCstLineNumberAndCstDateToLongAndIiNameAndLocName(
    					CST_DT.getTime(), invItemLocation.getInvItem().getIiName(), invItemLocation.getInvLocation().getLocName(),
						AD_BRNCH, AD_CMPNY);
    			CST_LN_NMBR = invCurrentCosting.getCstLineNumber() + 1;

    		} catch (FinderException ex) {

    			CST_LN_NMBR = 1;

    		}

    		LocalInvCosting invCosting = invCostingHome.create(CST_DT, CST_DT.getTime(), CST_LN_NMBR, 0d, 0d, 0d, 0d,
    				CST_ADJST_QTY, CST_ADJST_CST, 0d, 0d, CST_RMNNG_QTY, CST_RMNNG_VL, 0d, 0d, CST_ADJST_QTY > 0 ? CST_ADJST_QTY : 0, AD_BRNCH, AD_CMPNY);
    		//invItemLocation.addInvCosting(invCosting);
    		invCosting.setInvItemLocation(invItemLocation);
    		invCosting.setInvAdjustmentLine(invAdjustmentLine);

    		// propagate balance if necessary

    		Collection invCostings = invCostingHome.findByGreaterThanCstDateAndIiNameAndLocName(CST_DT, invItemLocation.getInvItem().getIiName(), invItemLocation.getInvLocation().getLocName(), AD_BRNCH, AD_CMPNY);

    		Iterator i = invCostings.iterator();

    		while (i.hasNext()) {

    			LocalInvCosting invPropagatedCosting = (LocalInvCosting)i.next();

    			invPropagatedCosting.setCstRemainingQuantity(invPropagatedCosting.getCstRemainingQuantity() + CST_ADJST_QTY);
    			invPropagatedCosting.setCstRemainingValue(invPropagatedCosting.getCstRemainingValue() + CST_ADJST_CST);

    		}


    	} catch (Exception ex) {

    		Debug.printStackTrace(ex);
    		getSessionContext().setRollbackOnly();
    		throw new EJBException(ex.getMessage());

    	}



    }






	// SessionBean methods

	/**
	 * @ejb:create-method view-type="remote"
	 **/
	public void ejbCreate() throws CreateException {

		Debug.print("ApDirectCheckEntryControllerBean ejbCreate");

	}

}