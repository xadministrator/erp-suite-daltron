package com.ejb.txn;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.naming.NamingException;

import com.ejb.ap.LocalApCheckBatch;
import com.ejb.ap.LocalApCheckBatchHome;
import com.ejb.exception.GlobalNoRecordFoundException;
import com.util.AbstractSessionBean;
import com.util.ApCheckBatchDetails;
import com.util.Debug;
import com.util.EJBHomeFactory;

/**
 * @ejb:bean name="ApFindCheckBatchControllerEJB"
 *           display-name="Used for searching check batches"
 *           type="Stateless"
 *           view-type="remote"
 *           jndi-name="ejb/ApFindCheckBatchControllerEJB"
 *
 * @ejb:interface remote-class="com.ejb.txn.ApFindCheckBatchController"
 *                extends="javax.ejb.EJBObject"
 *
 * @ejb:home remote-class="com.ejb.txn.ApFindCheckBatchControllerHome"
 *           extends="javax.ejb.EJBHome"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="apuser"
 *                        role-link="apuserlink"
 *
 * @ejb:permission role-name="apuser"
 * 
*/

public class ApFindCheckBatchControllerBean extends AbstractSessionBean {


   
   /**
   * @ejb:interface-method view-type="remote"
   * @jboss:method-attributes read-only="true"
   **/
   public ArrayList getApCbByCriteria(HashMap criteria, String ORDER_BY, Integer OFFSET, Integer LIMIT, Integer AD_BRNCH, Integer AD_CMPNY)
      throws GlobalNoRecordFoundException {

      Debug.print("ApFindCheckBatchControllerBean getApCbByCriteria");
      
      LocalApCheckBatchHome apCheckBatchHome = null;
      
      // Initialize EJB Home
        
      try {
      	
          apCheckBatchHome = (LocalApCheckBatchHome)EJBHomeFactory.
              lookUpLocalHome(LocalApCheckBatchHome.JNDI_NAME, LocalApCheckBatchHome.class);
           
      } catch (NamingException ex) {
            
          throw new EJBException(ex.getMessage());
            
      }
            
      ArrayList list = new ArrayList();
      
      StringBuffer jbossQl = new StringBuffer();
      jbossQl.append("SELECT OBJECT(cb) FROM ApCheckBatch cb ");
      
      boolean firstArgument = true;
      short ctr = 0;
      Object obj[];
      
       // Allocate the size of the object parameter

       
      if (criteria.containsKey("batchName")) {
      	
      	 obj = new Object[(criteria.size()-1) + 2];
      	 
      } else {
      	
      	 obj = new Object[criteria.size() + 2];
	
      }
      
  
         
      
      if (criteria.containsKey("batchName")) {
      	
      	 if (!firstArgument) {
      	 
      	    jbossQl.append("AND ");	
      	 	
         } else {
         	
         	firstArgument = false;
         	jbossQl.append("WHERE ");
         	
         }
         
      	 jbossQl.append("cb.cbName LIKE '%" + (String)criteria.get("batchName") + "%' ");
      	 
      }
      	 
        
      if (criteria.containsKey("status")) {
	      	
	  	 if (!firstArgument) {
	  	 	jbossQl.append("AND ");
	  	 } else {
	  	 	firstArgument = false;
	  	 	jbossQl.append("WHERE ");
	  	 }
	  	 jbossQl.append("cb.cbStatus=?" + (ctr+1) + " ");
	  	 obj[ctr] = (String)criteria.get("status");
	  	 ctr++;
	  }  
	  
	  if (criteria.containsKey("dateCreated")) {
	      	
	  	 if (!firstArgument) {
	  	 	jbossQl.append("AND ");
	  	 } else {
	  	 	firstArgument = false;
	  	 	jbossQl.append("WHERE ");
	  	 }
	  	 jbossQl.append("cb.cbDateCreated=?" + (ctr+1) + " ");
	  	 obj[ctr] = (Date)criteria.get("dateCreated");
	  	 ctr++;
	  } 
	  
	  if (!firstArgument) {
	      jbossQl.append("AND ");
	  } else {
	      firstArgument = false;
	      jbossQl.append("WHERE ");
	  }
	  jbossQl.append("cb.cbAdBranch=" + AD_BRNCH + " ");
	  
	  if (!firstArgument) {
  	 	 jbossQl.append("AND ");
  	  } else {
  	 	 firstArgument = false;
  	 	 jbossQl.append("WHERE ");
  	  }
  	  jbossQl.append("cb.cbAdCompany=" + AD_CMPNY + " ");
	      
         
      String orderBy = null;
	      
	  if (ORDER_BY.equals("BATCH NAME")) {
	
	  	  orderBy = "cb.cbName";
	  	  
	  } else if (ORDER_BY.equals("DATE CREATED")) {
	
	  	  orderBy = "cb.cbDateCreated";
	  	
	  }
	  
	  if (orderBy != null) {
	  
	  	jbossQl.append("ORDER BY " + orderBy);
	  	
	  }
               
      jbossQl.append(" OFFSET ?" + (ctr + 1));
      obj[ctr] = OFFSET;
      ctr++;
      
      jbossQl.append(" LIMIT ?" + (ctr + 1));
      obj[ctr] = LIMIT;
      
      System.out.println("QL + " + jbossQl);
      
      

      Collection apCheckBatches = null;
      
      try {
      	
         apCheckBatches = apCheckBatchHome.getCbByCriteria(jbossQl.toString(), obj);
         
      } catch (Exception ex) {
      	
      	 throw new EJBException(ex.getMessage());
      	 
      }
      
      if (apCheckBatches.isEmpty())
         throw new GlobalNoRecordFoundException();
         
      Iterator i = apCheckBatches.iterator();
      while (i.hasNext()) {
      	      	
         LocalApCheckBatch apCheckBatch = (LocalApCheckBatch) i.next();
         
         ApCheckBatchDetails details = new ApCheckBatchDetails();
         
         details.setCbCode(apCheckBatch.getCbCode());
         details.setCbName(apCheckBatch.getCbName());
         details.setCbDescription(apCheckBatch.getCbDescription());
         details.setCbStatus(apCheckBatch.getCbStatus());
         details.setCbDateCreated(apCheckBatch.getCbDateCreated());
         details.setCbCreatedBy(apCheckBatch.getCbCreatedBy());
         
         list.add(details);
      	
      }
         
      return list;
  
   }
   
   /**
    * @ejb:interface-method view-type="remote"
    * @jboss:method-attributes read-only="true"
    **/
    public Integer getApCbSizeByCriteria(HashMap criteria, Integer AD_BRNCH, Integer AD_CMPNY)
       throws GlobalNoRecordFoundException {

       Debug.print("ApFindCheckBatchControllerBean getApCbSizeByCriteria");
       
       LocalApCheckBatchHome apCheckBatchHome = null;
       
       // Initialize EJB Home
         
       try {
       	
           apCheckBatchHome = (LocalApCheckBatchHome)EJBHomeFactory.
               lookUpLocalHome(LocalApCheckBatchHome.JNDI_NAME, LocalApCheckBatchHome.class);
            
       } catch (NamingException ex) {
             
           throw new EJBException(ex.getMessage());
             
       }
             
       StringBuffer jbossQl = new StringBuffer();
       jbossQl.append("SELECT OBJECT(cb) FROM ApCheckBatch cb ");
       
       boolean firstArgument = true;
       short ctr = 0;
       Object obj[];
       
        // Allocate the size of the object parameter

        
       if (criteria.containsKey("batchName")) {
       	
       	 obj = new Object[(criteria.size()-1)];
       	 
       } else {
       	
       	 obj = new Object[criteria.size()];
 	
       }
       
   
          
       
       if (criteria.containsKey("batchName")) {
       	
       	 if (!firstArgument) {
       	 
       	    jbossQl.append("AND ");	
       	 	
          } else {
          	
          	firstArgument = false;
          	jbossQl.append("WHERE ");
          	
          }
          
       	 jbossQl.append("cb.cbName LIKE '%" + (String)criteria.get("batchName") + "%' ");
       	 
       }
       	 
         
       if (criteria.containsKey("status")) {
 	      	
 	  	 if (!firstArgument) {
 	  	 	jbossQl.append("AND ");
 	  	 } else {
 	  	 	firstArgument = false;
 	  	 	jbossQl.append("WHERE ");
 	  	 }
 	  	 jbossQl.append("cb.cbStatus=?" + (ctr+1) + " ");
 	  	 obj[ctr] = (String)criteria.get("status");
 	  	 ctr++;
 	  }  
 	  
 	  if (criteria.containsKey("dateCreated")) {
 	      	
 	  	 if (!firstArgument) {
 	  	 	jbossQl.append("AND ");
 	  	 } else {
 	  	 	firstArgument = false;
 	  	 	jbossQl.append("WHERE ");
 	  	 }
 	  	 jbossQl.append("cb.cbDateCreated=?" + (ctr+1) + " ");
 	  	 obj[ctr] = (Date)criteria.get("dateCreated");
 	  	 ctr++;
 	  } 
 	  
 	  if (!firstArgument) {
 	      jbossQl.append("AND ");
 	  } else {
 	      firstArgument = false;
 	      jbossQl.append("WHERE ");
 	  }
 	  jbossQl.append("cb.cbAdBranch=" + AD_BRNCH + " ");
 	  
 	  if (!firstArgument) {
   	 	 jbossQl.append("AND ");
   	  } else {
   	 	 firstArgument = false;
   	 	 jbossQl.append("WHERE ");
   	  }
   	  jbossQl.append("cb.cbAdCompany=" + AD_CMPNY + " ");
 	      
       Collection apCheckBatches = null;
       
       try {
       	
          apCheckBatches = apCheckBatchHome.getCbByCriteria(jbossQl.toString(), obj);
          
       } catch (Exception ex) {
       	
       	 throw new EJBException(ex.getMessage());
       	 
       }
       
       if (apCheckBatches.isEmpty())
          throw new GlobalNoRecordFoundException();
          
       return new Integer(apCheckBatches.size());
   
    }
   
   // SessionBean methods

   /**
    * @ejb:create-method view-type="remote"
    **/
   public void ejbCreate() throws CreateException {

      Debug.print("ApCheckBatchControllerBean ejbCreate");
      
   }

   // private methods

     
}
