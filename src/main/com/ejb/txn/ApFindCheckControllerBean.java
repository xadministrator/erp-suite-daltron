

/*
 * ApFindCheckControllerBean.java
 *
 * Created on February 20, 2004, 8:38 PM
 *
 * @author  Neil Andrew M. Ajero
 */

package com.ejb.txn;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.naming.NamingException;

import com.ejb.ad.LocalAdBankAccount;
import com.ejb.ad.LocalAdBankAccountHome;
import com.ejb.ad.LocalAdCompany;
import com.ejb.ad.LocalAdCompanyHome;
import com.ejb.ad.LocalAdPreference;
import com.ejb.ad.LocalAdPreferenceHome;
import com.ejb.ap.LocalApCheck;
import com.ejb.ap.LocalApCheckBatch;
import com.ejb.ap.LocalApCheckBatchHome;
import com.ejb.ap.LocalApCheckHome;
import com.ejb.ap.LocalApSupplier;
import com.ejb.ap.LocalApSupplierHome;
import com.ejb.exception.GlobalNoRecordFoundException;
import com.util.AbstractSessionBean;
import com.util.ApModCheckDetails;
import com.util.Debug;
import com.util.EJBCommon;
import com.util.EJBHomeFactory;

/**
 * @ejb:bean name="ApFindCheckControllerEJB"
 *           display-name="Used for finding checks"
 *           type="Stateless"
 *           view-type="remote"
 *           jndi-name="ejb/ApFindCheckControllerEJB"
 *
 * @ejb:interface remote-class="com.ejb.txn.ApFindCheckController"
 *                extends="javax.ejb.EJBObject"
 *
 * @ejb:home remote-class="com.ejb.txn.ApFindCheckControllerHome"
 *           extends="javax.ejb.EJBHome"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="apuser"
 *                        role-link="apuserlink"
 *
 * @ejb:permission role-name="apuser"
 * 
*/

public class ApFindCheckControllerBean extends AbstractSessionBean {


    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList getApSplAll(Integer AD_BRNCH, Integer AD_CMPNY) {
                    
        Debug.print("ApFindCheckControllerBean getApSplAll");
        
        LocalApSupplierHome apSupplierHome = null;               
        
        ArrayList list = new ArrayList();
        
        // Initialize EJB Home
        
        try {
            
            apSupplierHome = (LocalApSupplierHome)EJBHomeFactory.
                lookUpLocalHome(LocalApSupplierHome.JNDI_NAME, LocalApSupplierHome.class);
            
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }        
        
        try {
        	
        	Collection apSuppliers = apSupplierHome.findEnabledSplAll(AD_BRNCH, AD_CMPNY);        	
        	        	
        	Iterator i = apSuppliers.iterator();
        	
        	while (i.hasNext()) {
        		
        		LocalApSupplier apSupplier = (LocalApSupplier)i.next();
        		
        		list.add(apSupplier.getSplSupplierCode());
        		
        	}
        	
        	return list;
        	
        } catch (Exception ex) {
        	
        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());
        	
        }
            
    } 


    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList getAdBaAll(Integer AD_BRNCH, Integer AD_CMPNY) {
                    
        Debug.print("ApFindCheckControllerBean getAdBaAll");
        
        LocalAdBankAccountHome adBankAccountHome = null;               
        
        ArrayList list = new ArrayList();
        
        // Initialize EJB Home
        
        try {
            
            adBankAccountHome = (LocalAdBankAccountHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdBankAccountHome.JNDI_NAME, LocalAdBankAccountHome.class);
            
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }        
        
        try {
        	
        	Collection adBankAccounts = adBankAccountHome.findEnabledBaAll(AD_BRNCH, AD_CMPNY);
        	
        	Iterator i = adBankAccounts.iterator();
        	
        	while (i.hasNext()) {
        		
        		LocalAdBankAccount adBankAccount = (LocalAdBankAccount)i.next();
        		
        		list.add(adBankAccount.getBaName());
        		
        	}
        	
        	return list;
        	
        } catch (Exception ex) {
        	
        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());
        	
        }
            
    }
    
    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList getApOpenCbAll(Integer AD_BRNCH, Integer AD_CMPNY) {
                    
        Debug.print("ApFindCheckControllerBean getApOpenCbAll");
        
        LocalApCheckBatchHome apCheckBatchHome = null;               
        
        ArrayList list = new ArrayList();
        
        // Initialize EJB Home
        
        try {
            
        	apCheckBatchHome = (LocalApCheckBatchHome)EJBHomeFactory.
                lookUpLocalHome(LocalApCheckBatchHome.JNDI_NAME, LocalApCheckBatchHome.class);
            
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }        
        
        try {
        	
        	Collection apCheckBatches = apCheckBatchHome.findOpenCbAll(AD_BRNCH, AD_CMPNY);
        	
        	Iterator i = apCheckBatches.iterator();
        	
        	while (i.hasNext()) {
        		
        		LocalApCheckBatch apCheckBatch = (LocalApCheckBatch)i.next();
        		
        		list.add(apCheckBatch.getCbName());
        		
        	}
        	
        	return list;
        	
        } catch (Exception ex) {
        	
        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());
        	
        }
            
    }  
    
    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
     public byte getAdPrfEnableApCheckBatch(Integer AD_CMPNY) {

        Debug.print("ApFindCheckControllerBean getAdPrfEnableApCheckBatch");
                   
        LocalAdPreferenceHome adPreferenceHome = null;
       
       
        // Initialize EJB Home
         
        try {
             
           adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
               lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);       
             
        } catch (NamingException ex) {
             
           throw new EJBException(ex.getMessage());
             
        }
       

        try {
       	
           LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);
          
           return adPreference.getPrfEnableApCheckBatch();
          
        } catch (Exception ex) {
         	 
           Debug.printStackTrace(ex);
           throw new EJBException(ex.getMessage());
          
        }
       
     }
    
     /**
      * @ejb:interface-method view-type="remote"
      * @jboss:method-attributes read-only="true"
      **/
      public String getAdPrfDefaultCheckType(Integer AD_CMPNY) {

         Debug.print("ApFindCheckControllerBean getAdPrfDefaultCheckType");
                    
         LocalAdPreferenceHome adPreferenceHome = null;
        
        
         // Initialize EJB Home
          
         try {
              
            adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);       
              
         } catch (NamingException ex) {
              
            throw new EJBException(ex.getMessage());
              
         }
        

         try {
        	
            LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);
           
            return adPreference.getPrfApFindCheckDefaultType();
           
         } catch (Exception ex) {
          	 
            Debug.printStackTrace(ex);
            throw new EJBException(ex.getMessage());
           
         }
        
      }
     
  /**
   * @ejb:interface-method view-type="remote"
   * @jboss:method-attributes read-only="true"
   **/
   public ArrayList getApChkByCriteria(HashMap criteria, String ORDER_BY, Integer OFFSET, Integer LIMIT, Integer AD_BRNCH, Integer AD_CMPNY)
	  throws GlobalNoRecordFoundException {
	
	  Debug.print("ApFindCheckControllerBean getApChkByCriteria");
	  
	  LocalApCheckHome apCheckHome = null;
	  
	  // Initialize EJB Home
	    
	  try {
	  	
	      apCheckHome = (LocalApCheckHome)EJBHomeFactory.
	          lookUpLocalHome(LocalApCheckHome.JNDI_NAME, LocalApCheckHome.class);          
	        
	  } catch (NamingException ex) {
	        
	      throw new EJBException(ex.getMessage());
	        
	  }
	  
	  try { 
	      
	      ArrayList chkList = new ArrayList();
	      
	      StringBuffer jbossQl = new StringBuffer();
	      jbossQl.append("SELECT OBJECT(chk) FROM ApCheck chk ");
	      
	      boolean firstArgument = true;
	      short ctr = 0;
	      int criteriaSize = criteria.size() + 2;
		  Object obj[];
		  
		  if (criteria.containsKey("approvalStatus")) {
	      	
	      	 String approvalStatus = (String)criteria.get("approvalStatus");
	      	
	      	 if (approvalStatus.equals("DRAFT") || approvalStatus.equals("REJECTED")) {
	      	 	
	      	 	 criteriaSize--;
	      	 	
	      	 }
	      	
	      }
		  
		  obj = new Object[criteriaSize];
		  	
		  if (criteria.containsKey("batchName")) {
		  	  
		  	 firstArgument = false;

		  	 jbossQl.append("WHERE chk.apCheckBatch.cbName=?" + (ctr+1) + " ");
		  	 obj[ctr] = (String)criteria.get("batchName");
		  	 ctr++;
		  }	
		  
		  if (criteria.containsKey("bankAccount")) {
		  	
		  	if (!firstArgument) {
			  	 
		  	 	jbossQl.append("AND ");
		  	 
		  	 } else {
		  	 
		  	 	firstArgument = false;
		  	 	jbossQl.append("WHERE ");
		  	 
		  	 }
		  	 
		  	 jbossQl.append("chk.adBankAccount.baName=?" + (ctr+1) + " ");
		  	 obj[ctr] = (String)criteria.get("bankAccount");
		  	 ctr++;
		  	 
		  } 
		   
		  if (criteria.containsKey("checkType")) {
		  	
		  	 if (!firstArgument) {
		  	 
		  	 	jbossQl.append("AND ");
		  	 
		  	 } else {
		  	 
		  	 	firstArgument = false;
		  	 	jbossQl.append("WHERE ");
		  	 
		  	 }
		  	 
		  	 jbossQl.append("chk.chkType=?" + (ctr+1) + " ");
		  	 obj[ctr] = (String)criteria.get("checkType");
		  	 ctr++;
		  }		   
		  
		  if (criteria.containsKey("checkVoid")) {
		  	
		  	 if (!firstArgument) {
		  	 
		  	 	jbossQl.append("AND ");
		  	 
		  	 } else {
		  	 
		  	 	firstArgument = false;
		  	 	jbossQl.append("WHERE ");
		  	 
		  	 }
		  	 
		  	 jbossQl.append("chk.chkVoid=?" + (ctr+1) + " ");
		  	 obj[ctr] = (Byte)criteria.get("checkVoid");
		  	 ctr++;
		  }		   
		  
		  if (criteria.containsKey("checkPrinted")) {
		  	
		  	 if (!firstArgument) {
		  	 
		  	 	jbossQl.append("AND ");
		  	 
		  	 } else {
		  	 
		  	 	firstArgument = false;
		  	 	jbossQl.append("WHERE ");
		  	 
		  	 }
		  	 
		  	 jbossQl.append("chk.chkPrinted=?" + (ctr+1) + " ");
		  	 obj[ctr] = (Byte)criteria.get("checkPrinted");
		  	 ctr++;
		  }		   
		      
		  if (criteria.containsKey("supplierCode")) {
		  	
		  	 if (!firstArgument) {
		  	 
		  	 	jbossQl.append("AND ");
		  	 
		  	 } else {
		  	 
		  	 	firstArgument = false;
		  	 	jbossQl.append("WHERE ");
		  	 
		  	 }
		  	 
		  	 jbossQl.append("chk.apSupplier.splSupplierCode=?" + (ctr+1) + " ");
		  	 obj[ctr] = (String)criteria.get("supplierCode");
		  	 ctr++;
		  }      
		      
		  if (criteria.containsKey("dateFrom")) {
		  	
		  	 if (!firstArgument) {
		  	 	jbossQl.append("AND ");
		  	 
		  	 } else {
		  	 
		  	 	firstArgument = false;
		  	 	jbossQl.append("WHERE ");
		  	 
		  	 }
		  	 
		  	 jbossQl.append("chk.chkDate>=?" + (ctr+1) + " ");
		  	 obj[ctr] = (Date)criteria.get("dateFrom");
		  	 ctr++;
		  }  
		      
		  if (criteria.containsKey("dateTo")) {
		  	
		  	 if (!firstArgument) {
		  	 
		  	 	jbossQl.append("AND ");
		  	 
		  	 } else {
		  	 
		  	 	firstArgument = false;
		  	 	jbossQl.append("WHERE ");
		  	 
		  	 }
		  	 
		  	 jbossQl.append("chk.chkDate<=?" + (ctr+1) + " ");
		  	 obj[ctr] = (Date)criteria.get("dateTo");
		  	 ctr++;
		  	 
		  }    
		      
		  if (criteria.containsKey("checkNumberFrom")) {
		  	
		  	 if (!firstArgument) {
		  	 
		  	 	jbossQl.append("AND ");
		  	 
		  	 } else {
		  	 
		  	 	firstArgument = false;
		  	 	jbossQl.append("WHERE ");
		  	 
		  	 }
		  	 
		  	 jbossQl.append("chk.chkNumber>=?" + (ctr+1) + " ");
		  	 obj[ctr] = (String)criteria.get("checkNumberFrom");
		  	 ctr++;
		  	 
		  }  
		      
		  if (criteria.containsKey("checkNumberTo")) {
		  	
		  	 if (!firstArgument) {
		  	 
		  	 	jbossQl.append("AND ");
		  	 
		  	 } else {
		  	 
		  	 	firstArgument = false;
		  	 	jbossQl.append("WHERE ");
		  	 
		  	 }
		  	 
		  	 jbossQl.append("chk.chkNumber<=?" + (ctr+1) + " ");
		  	 obj[ctr] = (String)criteria.get("checkNumberTo");
		  	 ctr++;
		  	 
		  } 	      
		      
		  if (criteria.containsKey("documentNumberFrom")) {
		  	
		  	 if (!firstArgument) {
		  	 
		  	 	jbossQl.append("AND ");
		  	 
		  	 } else {
		  	 
		  	 	firstArgument = false;
		  	 	jbossQl.append("WHERE ");
		  	 
		  	 }
		  	 
		  	 jbossQl.append("chk.chkDocumentNumber>=?" + (ctr+1) + " ");
		  	 obj[ctr] = (String)criteria.get("documentNumberFrom");
		  	 ctr++;
		  	 
		  }  
		      
		  if (criteria.containsKey("documentNumberTo")) {
		  	
		  	 if (!firstArgument) {
		  	 
		  	 	jbossQl.append("AND ");
		  	 
		  	 } else {
		  	 
		  	 	firstArgument = false;
		  	 	jbossQl.append("WHERE ");
		  	 
		  	 }
		  	 
		  	 jbossQl.append("chk.chkDocumentNumber<=?" + (ctr+1) + " ");
		  	 obj[ctr] = (String)criteria.get("documentNumberTo");
		  	 ctr++;
		  	 
		  }
		  
		  if (criteria.containsKey("referenceNumberFrom")) {
			  	
			  	 if (!firstArgument) {
			  	 
			  	 	jbossQl.append("AND ");
			  	 
			  	 } else {
			  	 
			  	 	firstArgument = false;
			  	 	jbossQl.append("WHERE ");
			  	 
			  	 }
			  	 
			  	 jbossQl.append("chk.chkReferenceNumber>=?" + (ctr+1) + " ");
			  	 obj[ctr] = (String)criteria.get("referenceNumberFrom");
			  	 ctr++;
			  	 
			  }  
			      
			  if (criteria.containsKey("referenceNumberTo")) {
			  	
			  	 if (!firstArgument) {
			  	 
			  	 	jbossQl.append("AND ");
			  	 
			  	 } else {
			  	 
			  	 	firstArgument = false;
			  	 	jbossQl.append("WHERE ");
			  	 
			  	 }
			  	 
			  	 jbossQl.append("chk.chkReferenceNumber<=?" + (ctr+1) + " ");
			  	 obj[ctr] = (String)criteria.get("referenceNumberTo");
			  	 ctr++;
			  	 
			  }
		      	      
		  if (criteria.containsKey("released")) {
		   	
		   	  if (!firstArgument) {
		   	  	
		   	     jbossQl.append("AND ");
		   	     
		   	  } else {
		   	  	
		   	  	 firstArgument = false;
		   	  	 jbossQl.append("WHERE ");
		   	  	 
		   	  }
		   	  
		   	  jbossQl.append("chk.chkReleased=?" + (ctr+1) + " ");
		   	  
		   	  String released = (String)criteria.get("released");
		   	  
		   	  if (released.equals("YES")) {
		   	  	
		   	  	obj[ctr] = new Byte(EJBCommon.TRUE);
		   	  	
		   	  } else {
		   	  	
		   	  	obj[ctr] = new Byte(EJBCommon.FALSE);
		   	  	
		   	  }       	  
		   	 
		   	  ctr++;
		   	  
		  }	
		  
		  if (criteria.containsKey("approvalStatus")) {
		   	
		   	  if (!firstArgument) {
		   	  	
		   	     jbossQl.append("AND ");
		   	     
		   	  } else {
		   	  	
		   	  	 firstArgument = false;
		   	  	 jbossQl.append("WHERE ");
		   	  	 
		   	  }
		   	  
		   	  String approvalStatus = (String)criteria.get("approvalStatus");
	       	  
	       	  if (approvalStatus.equals("DRAFT")) {
	       	      
		       	  jbossQl.append("chk.chkApprovalStatus IS NULL AND chk.chkReasonForRejection IS NULL ");
	       	  	
	       	  } else if (approvalStatus.equals("REJECTED")) {
	       	  	
		       	  jbossQl.append("chk.chkReasonForRejection IS NOT NULL ");
	       	  	
	      	  } else {
	      	  	
		      	  jbossQl.append("chk.chkApprovalStatus=?" + (ctr+1) + " ");
		       	  obj[ctr] = approvalStatus;
		       	  ctr++;
	      	  	
	      	  }
		   	  
		  }
		  
		  if (criteria.containsKey("posted")) {
		   	
		   	  if (!firstArgument) {
		   	  	
		   	     jbossQl.append("AND ");
		   	     
		   	  } else {
		   	  	
		   	  	 firstArgument = false;
		   	  	 jbossQl.append("WHERE ");
		   	  	 
		   	  }
		   	  
		   	  jbossQl.append("chk.chkPosted=?" + (ctr+1) + " ");
		   	  
		   	  String posted = (String)criteria.get("posted");
		   	  
		   	  if (posted.equals("YES")) {
		   	  	
		   	  	obj[ctr] = new Byte(EJBCommon.TRUE);
		   	  	
		   	  } else {
		   	  	
		   	  	obj[ctr] = new Byte(EJBCommon.FALSE);
		   	  	
		   	  }       	  
		   	 
		   	  ctr++;
		   	  
		  }	
		  
		  if (!firstArgument) {
		      
		      jbossQl.append("AND ");
		      
		  } else {
		      
		      firstArgument = false;
		      jbossQl.append("WHERE ");
		      
		  }
		  
		  jbossQl.append("chk.chkAdBranch=" + AD_BRNCH + " ");
		  
		  if (!firstArgument) {
	   	  	
	   	     jbossQl.append("AND ");
	   	     
	   	  } else {
	   	  	
	   	  	 firstArgument = false;
	   	  	 jbossQl.append("WHERE ");
	   	  	 
	   	  }
	   	  
	   	  jbossQl.append("chk.chkAdCompany=" + AD_CMPNY + " ");
	             	     
	      String orderBy = null;
		      
	      if (ORDER_BY.equals("BANK ACCOUNT")) {
	          
	          orderBy = "chk.adBankAccount.baName";
	          	
	      } else if (ORDER_BY.equals("SUPPLIER CODE")) {
	      	 
	      	  orderBy = "chk.apSupplier.splSupplierCode";
	      	  
	      } else if (ORDER_BY.equals("DOCUMENT NUMBER")) {
	      	
	      	  orderBy = "chk.chkDocumentNumber";
	      	
	      } else if (ORDER_BY.equals("CHECK NUMBER")) {
	      	
	      	  orderBy = "chk.chkNumber";
	      	
	      } else if (ORDER_BY.equals("REFERENCE NUMBER")) {
	      	
	      	  orderBy = "chk.chkReferenceNumber";
	      	
	      }
	      
		  
		  if (orderBy != null) {
		  
		  	jbossQl.append("ORDER BY " + orderBy + ", chk.chkDate");
		  	
		  }  else {
		  	
		  	jbossQl.append("ORDER BY chk.chkDate");
		  	
		  }
		  
	               
	      jbossQl.append(" OFFSET ?" + (ctr + 1));
	      obj[ctr] = OFFSET;
	      ctr++;
	      
	      jbossQl.append(" LIMIT ?" + (ctr + 1));
	      obj[ctr] = LIMIT;
	      
	      System.out.println("QL + " + jbossQl);
	
	      Collection apChecks = null;
	      
	      try {
	      	
	         apChecks = apCheckHome.getChkByCriteria(jbossQl.toString(), obj);
	         
	      } catch (Exception ex) {
	      	
	      	 throw new EJBException(ex.getMessage());
	      	 
	      }
	      
	      if (apChecks.isEmpty())
	         throw new GlobalNoRecordFoundException();
	         
	      Iterator i = apChecks.iterator();
	      while (i.hasNext()) {
	
	         LocalApCheck apCheck = (LocalApCheck) i.next();
	
		     ApModCheckDetails mdetails = new ApModCheckDetails();
		     mdetails.setChkCode(apCheck.getChkCode());
             mdetails.setChkType(apCheck.getChkType());
		     mdetails.setChkDate(apCheck.getChkDate());
		     mdetails.setChkNumber(apCheck.getChkNumber());
		     mdetails.setChkDocumentNumber(apCheck.getChkDocumentNumber());
		     mdetails.setChkReferenceNumber(apCheck.getChkReferenceNumber());
		     mdetails.setChkAmount(apCheck.getChkAmount());
		     mdetails.setChkCrossCheck(apCheck.getChkCrossCheck());
		     mdetails.setChkReleased(apCheck.getChkReleased());
		     mdetails.setChkSplName(apCheck.getChkSupplierName() == null ? apCheck.getApSupplier().getSplName() : apCheck.getChkSupplierName());
		     mdetails.setChkBaName(apCheck.getAdBankAccount().getBaName());
		           	  
	      	 chkList.add(mdetails);
	      	
	      }
	         
	      return chkList;
  
	  } catch (GlobalNoRecordFoundException ex) {
	  	 
	  	  throw ex;
	  	
	  } catch (Exception ex) {
	  	

	  	  ex.printStackTrace();
	  	  throw new EJBException(ex.getMessage());
	  	
	  }
        
   }
   
   /**
    * @ejb:interface-method view-type="remote"
    * @jboss:method-attributes read-only="true"
    **/
    public Integer getApChkSizeByCriteria(HashMap criteria, Integer AD_BRNCH, Integer AD_CMPNY)
 	  throws GlobalNoRecordFoundException {
 	
 	  Debug.print("ApFindCheckControllerBean getApChkSizeByCriteria");
 	  
 	  LocalApCheckHome apCheckHome = null;
 	  
 	  // Initialize EJB Home
 	    
 	  try {
 	  	
 	      apCheckHome = (LocalApCheckHome)EJBHomeFactory.
 	          lookUpLocalHome(LocalApCheckHome.JNDI_NAME, LocalApCheckHome.class);          
 	        
 	  } catch (NamingException ex) {
 	        
 	      throw new EJBException(ex.getMessage());
 	        
 	  }
 	  
 	  try { 
 	      
 	      StringBuffer jbossQl = new StringBuffer();
 	      jbossQl.append("SELECT OBJECT(chk) FROM ApCheck chk ");
 	      
 	      boolean firstArgument = true;
 	      short ctr = 0;
 	      int criteriaSize = criteria.size();
 		  Object obj[];
 		  
 		  if (criteria.containsKey("approvalStatus")) {
 	      	
 	      	 String approvalStatus = (String)criteria.get("approvalStatus");
 	      	
 	      	 if (approvalStatus.equals("DRAFT") || approvalStatus.equals("REJECTED")) {
 	      	 	
 	      	 	 criteriaSize--;
 	      	 	
 	      	 }
 	      	
 	      }
 		  
 		  obj = new Object[criteriaSize];
 		  	
 		  if (criteria.containsKey("batchName")) {
 		  	  
 		  	 firstArgument = false;

 		  	 jbossQl.append("WHERE chk.apCheckBatch.cbName=?" + (ctr+1) + " ");
 		  	 obj[ctr] = (String)criteria.get("batchName");
 		  	 ctr++;
 		  }	
 		  
 		  if (criteria.containsKey("bankAccount")) {
 		  	
 		  	if (!firstArgument) {
 			  	 
 		  	 	jbossQl.append("AND ");
 		  	 
 		  	 } else {
 		  	 
 		  	 	firstArgument = false;
 		  	 	jbossQl.append("WHERE ");
 		  	 
 		  	 }
 		  	 
 		  	 jbossQl.append("chk.adBankAccount.baName=?" + (ctr+1) + " ");
 		  	 obj[ctr] = (String)criteria.get("bankAccount");
 		  	 ctr++;
 		  	 
 		  } 
 		   
 		  if (criteria.containsKey("checkType")) {
 		  	
 		  	 if (!firstArgument) {
 		  	 
 		  	 	jbossQl.append("AND ");
 		  	 
 		  	 } else {
 		  	 
 		  	 	firstArgument = false;
 		  	 	jbossQl.append("WHERE ");
 		  	 
 		  	 }
 		  	 
 		  	 jbossQl.append("chk.chkType=?" + (ctr+1) + " ");
 		  	 obj[ctr] = (String)criteria.get("checkType");
 		  	 ctr++;
 		  }		   
 		  
 		  if (criteria.containsKey("checkVoid")) {
 		  	
 		  	 if (!firstArgument) {
 		  	 
 		  	 	jbossQl.append("AND ");
 		  	 
 		  	 } else {
 		  	 
 		  	 	firstArgument = false;
 		  	 	jbossQl.append("WHERE ");
 		  	 
 		  	 }
 		  	 
 		  	 jbossQl.append("chk.chkVoid=?" + (ctr+1) + " ");
 		  	 obj[ctr] = (Byte)criteria.get("checkVoid");
 		  	 ctr++;
 		  }		   
 		      
 		  if (criteria.containsKey("supplierCode")) {
 		  	
 		  	 if (!firstArgument) {
 		  	 
 		  	 	jbossQl.append("AND ");
 		  	 
 		  	 } else {
 		  	 
 		  	 	firstArgument = false;
 		  	 	jbossQl.append("WHERE ");
 		  	 
 		  	 }
 		  	 
 		  	 jbossQl.append("chk.apSupplier.splSupplierCode=?" + (ctr+1) + " ");
 		  	 obj[ctr] = (String)criteria.get("supplierCode");
 		  	 ctr++;
 		  }      
 		      
 		  if (criteria.containsKey("dateFrom")) {
 		  	
 		  	 if (!firstArgument) {
 		  	 	jbossQl.append("AND ");
 		  	 
 		  	 } else {
 		  	 
 		  	 	firstArgument = false;
 		  	 	jbossQl.append("WHERE ");
 		  	 
 		  	 }
 		  	 
 		  	 jbossQl.append("chk.chkDate>=?" + (ctr+1) + " ");
 		  	 obj[ctr] = (Date)criteria.get("dateFrom");
 		  	 ctr++;
 		  }  
 		      
 		  if (criteria.containsKey("dateTo")) {
 		  	
 		  	 if (!firstArgument) {
 		  	 
 		  	 	jbossQl.append("AND ");
 		  	 
 		  	 } else {
 		  	 
 		  	 	firstArgument = false;
 		  	 	jbossQl.append("WHERE ");
 		  	 
 		  	 }
 		  	 
 		  	 jbossQl.append("chk.chkDate<=?" + (ctr+1) + " ");
 		  	 obj[ctr] = (Date)criteria.get("dateTo");
 		  	 ctr++;
 		  	 
 		  }    
 		      
 		  if (criteria.containsKey("checkNumberFrom")) {
 		  	
 		  	 if (!firstArgument) {
 		  	 
 		  	 	jbossQl.append("AND ");
 		  	 
 		  	 } else {
 		  	 
 		  	 	firstArgument = false;
 		  	 	jbossQl.append("WHERE ");
 		  	 
 		  	 }
 		  	 
 		  	 jbossQl.append("chk.chkNumber>=?" + (ctr+1) + " ");
 		  	 obj[ctr] = (String)criteria.get("checkNumberFrom");
 		  	 ctr++;
 		  	 
 		  }  
 		      
 		  if (criteria.containsKey("checkNumberTo")) {
 		  	
 		  	 if (!firstArgument) {
 		  	 
 		  	 	jbossQl.append("AND ");
 		  	 
 		  	 } else {
 		  	 
 		  	 	firstArgument = false;
 		  	 	jbossQl.append("WHERE ");
 		  	 
 		  	 }
 		  	 
 		  	 jbossQl.append("chk.chkNumber<=?" + (ctr+1) + " ");
 		  	 obj[ctr] = (String)criteria.get("checkNumberTo");
 		  	 ctr++;
 		  	 
 		  } 	      
 		      
 		  if (criteria.containsKey("documentNumberFrom")) {
 		  	
 		  	 if (!firstArgument) {
 		  	 
 		  	 	jbossQl.append("AND ");
 		  	 
 		  	 } else {
 		  	 
 		  	 	firstArgument = false;
 		  	 	jbossQl.append("WHERE ");
 		  	 
 		  	 }
 		  	 
 		  	 jbossQl.append("chk.chkDocumentNumber>=?" + (ctr+1) + " ");
 		  	 obj[ctr] = (String)criteria.get("documentNumberFrom");
 		  	 ctr++;
 		  	 
 		  }  
 		      
 		  if (criteria.containsKey("documentNumberTo")) {
 		  	
 		  	 if (!firstArgument) {
 		  	 
 		  	 	jbossQl.append("AND ");
 		  	 
 		  	 } else {
 		  	 
 		  	 	firstArgument = false;
 		  	 	jbossQl.append("WHERE ");
 		  	 
 		  	 }
 		  	 
 		  	 jbossQl.append("chk.chkDocumentNumber<=?" + (ctr+1) + " ");
 		  	 obj[ctr] = (String)criteria.get("documentNumberTo");
 		  	 ctr++;
 		  	 
 		  } 
 		  
 		 if (criteria.containsKey("referenceNumberFrom")) {
  		  	
 		  	 if (!firstArgument) {
 		  	 
 		  	 	jbossQl.append("AND ");
 		  	 
 		  	 } else {
 		  	 
 		  	 	firstArgument = false;
 		  	 	jbossQl.append("WHERE ");
 		  	 
 		  	 }
 		  	 
 		  	 jbossQl.append("chk.chkReferenceNumber>=?" + (ctr+1) + " ");
 		  	 obj[ctr] = (String)criteria.get("referenceNumberFrom");
 		  	 ctr++;
 		  	 
 		  }  
 		      
 		  if (criteria.containsKey("referenceNumberTo")) {
 		  	
 		  	 if (!firstArgument) {
 		  	 
 		  	 	jbossQl.append("AND ");
 		  	 
 		  	 } else {
 		  	 
 		  	 	firstArgument = false;
 		  	 	jbossQl.append("WHERE ");
 		  	 
 		  	 }
 		  	 
 		  	 jbossQl.append("chk.chkReferenceNumber<=?" + (ctr+1) + " ");
 		  	 obj[ctr] = (String)criteria.get("referenceNumberTo");
 		  	 ctr++;
 		  	 
 		  }
 		  
 		      	      
 		  if (criteria.containsKey("released")) {
 		   	
 		   	  if (!firstArgument) {
 		   	  	
 		   	     jbossQl.append("AND ");
 		   	     
 		   	  } else {
 		   	  	
 		   	  	 firstArgument = false;
 		   	  	 jbossQl.append("WHERE ");
 		   	  	 
 		   	  }
 		   	  
 		   	  jbossQl.append("chk.chkReleased=?" + (ctr+1) + " ");
 		   	  
 		   	  String released = (String)criteria.get("released");
 		   	  
 		   	  if (released.equals("YES")) {
 		   	  	
 		   	  	obj[ctr] = new Byte(EJBCommon.TRUE);
 		   	  	
 		   	  } else {
 		   	  	
 		   	  	obj[ctr] = new Byte(EJBCommon.FALSE);
 		   	  	
 		   	  }       	  
 		   	 
 		   	  ctr++;
 		   	  
 		  }	
 		  
 		  if (criteria.containsKey("approvalStatus")) {
 		   	
 		   	  if (!firstArgument) {
 		   	  	
 		   	     jbossQl.append("AND ");
 		   	     
 		   	  } else {
 		   	  	
 		   	  	 firstArgument = false;
 		   	  	 jbossQl.append("WHERE ");
 		   	  	 
 		   	  }
 		   	  
 		   	  String approvalStatus = (String)criteria.get("approvalStatus");
 	       	  
 	       	  if (approvalStatus.equals("DRAFT")) {
 	       	      
 		       	  jbossQl.append("chk.chkApprovalStatus IS NULL AND chk.chkReasonForRejection IS NULL ");
 	       	  	
 	       	  } else if (approvalStatus.equals("REJECTED")) {
 	       	  	
 		       	  jbossQl.append("chk.chkReasonForRejection IS NOT NULL ");
 	       	  	
 	      	  } else {
 	      	  	
 		      	  jbossQl.append("chk.chkApprovalStatus=?" + (ctr+1) + " ");
 		       	  obj[ctr] = approvalStatus;
 		       	  ctr++;
 	      	  	
 	      	  }
 		   	  
 		  }
 		  
 		  if (criteria.containsKey("posted")) {
 		   	
 		   	  if (!firstArgument) {
 		   	  	
 		   	     jbossQl.append("AND ");
 		   	     
 		   	  } else {
 		   	  	
 		   	  	 firstArgument = false;
 		   	  	 jbossQl.append("WHERE ");
 		   	  	 
 		   	  }
 		   	  
 		   	  jbossQl.append("chk.chkPosted=?" + (ctr+1) + " ");
 		   	  
 		   	  String posted = (String)criteria.get("posted");
 		   	  
 		   	  if (posted.equals("YES")) {
 		   	  	
 		   	  	obj[ctr] = new Byte(EJBCommon.TRUE);
 		   	  	
 		   	  } else {
 		   	  	
 		   	  	obj[ctr] = new Byte(EJBCommon.FALSE);
 		   	  	
 		   	  }       	  
 		   	 
 		   	  ctr++;
 		   	  
 		  }	  	 
 		  
 		  if (!firstArgument) {
 		      
 		      jbossQl.append("AND ");
 		      
 		  } else {
 		      
 		      firstArgument = false;
 		      jbossQl.append("WHERE ");
 		      
 		  }
 		  
 		  jbossQl.append("chk.chkAdBranch=" + AD_BRNCH + " ");
 		  
 		  if (!firstArgument) {
 	   	  	
 	   	     jbossQl.append("AND ");
 	   	     
 	   	  } else {
 	   	  	
 	   	  	 firstArgument = false;
 	   	  	 jbossQl.append("WHERE ");
 	   	  	 
 	   	  }
 	   	  
 	   	  jbossQl.append("chk.chkAdCompany=" + AD_CMPNY + " ");
 	             	     
 	      Collection apChecks = null;
 	      
 	      try {
 	      	
 	         apChecks = apCheckHome.getChkByCriteria(jbossQl.toString(), obj);
 	         
 	      } catch (Exception ex) {
 	      	
 	      	 throw new EJBException(ex.getMessage());
 	      	 
 	      }
 	      
 	      if (apChecks.isEmpty())
 	         throw new GlobalNoRecordFoundException();
 	         
 	      return new Integer(apChecks.size());
   
 	  } catch (GlobalNoRecordFoundException ex) {
 	  	 
 	  	  throw ex;
 	  	
 	  } catch (Exception ex) {
 	  	

 	  	  ex.printStackTrace();
 	  	  throw new EJBException(ex.getMessage());
 	  	
 	  }
         
    }
   
   
   /**
    * @ejb:interface-method view-type="remote"
    * @jboss:method-attributes read-only="true"
    **/
    public short getGlFcPrecisionUnit(Integer AD_CMPNY) {

       Debug.print("ApFindCheckControllerBean getGlFcPrecisionUnit");

      
       LocalAdCompanyHome adCompanyHome = null;
      
     
       // Initialize EJB Home
        
       try {
            
           adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
              lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);       
            
       } catch (NamingException ex) {
            
           throw new EJBException(ex.getMessage());
            
       }

       try {
       	
         LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
            
         return  adCompany.getGlFunctionalCurrency().getFcPrecision();
                                     
       } catch (Exception ex) {
       	 
       	 Debug.printStackTrace(ex);
         throw new EJBException(ex.getMessage());
         
       }

    }
    
    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
     public byte getAdPrfApUseSupplierPulldown(Integer AD_CMPNY) {

        Debug.print("ApFindCheckControllerBean getAdPrfApUseSupplierPulldown");
                   
        LocalAdPreferenceHome adPreferenceHome = null;
       
       
        // Initialize EJB Home
         
        try {
             
           adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
               lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);       
             
        } catch (NamingException ex) {
             
           throw new EJBException(ex.getMessage());
             
        }
       

        try {
        	
           LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);
           
           return adPreference.getPrfApUseSupplierPulldown();
          
        } catch (Exception ex) {
         	 
           Debug.printStackTrace(ex);
           throw new EJBException(ex.getMessage());
          
        }
       
     }

   // SessionBean methods

   /**
    * @ejb:create-method view-type="remote"
    **/
   public void ejbCreate() throws CreateException {

      Debug.print("ApFindCheckControllerBean ejbCreate");
      
   }

   // private methods
   	
		   
}
