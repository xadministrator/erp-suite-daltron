
/*
 * InvFindPurchaseOrderControllerBean.java
 *
 * Created on April 20, 2005, 12:37 PM
 *
 * @author  Jolly T. Martin
 */


package com.ejb.txn;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;
import javax.naming.NamingException;

import com.ejb.ad.LocalAdPreference;
import com.ejb.ad.LocalAdPreferenceHome;
import com.ejb.ap.LocalApPurchaseOrder;
import com.ejb.ap.LocalApPurchaseOrderHome;
import com.ejb.ap.LocalApPurchaseOrderLine;
import com.ejb.ap.LocalApPurchaseOrderLineHome;
import com.ejb.ap.LocalApSupplier;
import com.ejb.ap.LocalApSupplierHome;
import com.ejb.ap.LocalApVoucherBatch;
import com.ejb.ap.LocalApVoucherBatchHome;
import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.gl.LocalGlFunctionalCurrency;
import com.ejb.gl.LocalGlFunctionalCurrencyHome;
import com.ejb.inv.LocalInvTagHome;
import com.util.AbstractSessionBean;
import com.util.ApModPurchaseOrderDetails;
import com.util.Debug;
import com.util.EJBCommon;
import com.util.EJBHomeFactory;

/**
 * @ejb:bean name="ApFindPurchaseOrderControllerEJB"
 *           display-name="Used for finding purchase orders"
 *           type="Stateless"
 *           view-type="remote"
 *           jndi-name="ejb/ApFindPurchaseOrderControllerEJB"
 *
 * @ejb:interface remote-class="com.ejb.txn.ApFindPurchaseOrderController"
 *                extends="javax.ejb.EJBObject"
 *
 * @ejb:home remote-class="com.ejb.txn.ApFindPurchaseOrderControllerHome"
 *           extends="javax.ejb.EJBHome"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="invuser"
 *                        role-link="invuserlink"
 *
 * @ejb:permission role-name="invuser"
 * 
*/

public class ApFindPurchaseOrderControllerBean extends AbstractSessionBean {
    
    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList getApSplAll(Integer AD_BRNCH, Integer AD_CMPNY) {
                    
        Debug.print("ApFindPurchaseOrderControllerBean getApSplAll");
        
        LocalApSupplierHome apSupplierHome = null;

        ArrayList list = new ArrayList();
        
        // Initialize EJB Home
        
        try {
            
            apSupplierHome = (LocalApSupplierHome)EJBHomeFactory.
                lookUpLocalHome(LocalApSupplierHome.JNDI_NAME, LocalApSupplierHome.class);
            
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {
            
            Collection apSuppliers = apSupplierHome.findEnabledSplAll(AD_BRNCH, AD_CMPNY);

	        Iterator i = apSuppliers.iterator();
	               
	        while (i.hasNext()) {
	        	
	        	LocalApSupplier apSupplier = (LocalApSupplier)i.next();
	        	
	        	list.add(apSupplier.getSplSupplierCode());
	        	
	        }
	        
	        return list;
            
        } catch (Exception ex) {
        	
        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());
        	
        }
            
    }
    
    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList getGlFcAll(Integer AD_CMPNY) {
                    
        Debug.print("ApFindPurchaseOrderControllerBean getGlFcAll");
        
        LocalGlFunctionalCurrencyHome glFunctionalCurrencyHome = null;

        ArrayList list = new ArrayList();
        
        // Initialize EJB Home
        
        try {
            
            glFunctionalCurrencyHome = (LocalGlFunctionalCurrencyHome)EJBHomeFactory.
                lookUpLocalHome(LocalGlFunctionalCurrencyHome.JNDI_NAME, LocalGlFunctionalCurrencyHome.class);
            
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {
            
            Collection glFunctionalCurrencies = glFunctionalCurrencyHome.findFcAll(AD_CMPNY);
	        
	        Iterator i = glFunctionalCurrencies.iterator();
	               
	        while (i.hasNext()) {
	        	
	        	LocalGlFunctionalCurrency glFunctionalCurrency = (LocalGlFunctionalCurrency)i.next();
	        	
	        	list.add(glFunctionalCurrency.getFcName());
	        	
	        }
	        
	        return list;
          
        } catch (Exception ex) {
        	
        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());
        	
        }
            
    }
    
    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList getApPoByCriteria(HashMap criteria,
        Integer OFFSET, Integer LIMIT, String ORDER_BY, Integer AD_BRNCH, boolean isPoLookup, Integer AD_CMPNY) 
        throws GlobalNoRecordFoundException {
                    
        Debug.print("ApFindPurchaseOrderControllerBean getApPoByCriteria");
        
        LocalApPurchaseOrderHome apPurchaseOrderHome = null;
        LocalApPurchaseOrderLineHome apPurchaseOrderLineHome = null;

        ArrayList list = new ArrayList();
        
        //initialized EJB Home
        
        try {
            
        	apPurchaseOrderHome = (LocalApPurchaseOrderHome)EJBHomeFactory.
                lookUpLocalHome(LocalApPurchaseOrderHome.JNDI_NAME, LocalApPurchaseOrderHome.class);
        	apPurchaseOrderLineHome = (LocalApPurchaseOrderLineHome)EJBHomeFactory.
            	lookUpLocalHome(LocalApPurchaseOrderLineHome.JNDI_NAME, LocalApPurchaseOrderLineHome.class);

        } catch (NamingException ex) 	{
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try { 
        	
        	StringBuffer jbossQl = new StringBuffer();
        	jbossQl.append("SELECT OBJECT(po) FROM ApPurchaseOrder po ");
        	
        	boolean firstArgument = true;
        	short ctr = 0;
        	int criteriaSize = criteria.size() + 2;	      
        	String serialNumber = "";
        	Object obj[];	      
        	
        	// Allocate the size of the object parameter
        	
        	if (criteria.containsKey("referenceNumber")) {
        		
        		criteriaSize--;
        		
        	}
        	
        	if (criteria.containsKey("serialNumber")) {
        		
        		criteriaSize--;
        		
        	}
        	
        	if (criteria.containsKey("approvalStatus")) {
	      	
		      	 String approvalStatus = (String)criteria.get("approvalStatus");
		      	
		      	 if (approvalStatus.equals("DRAFT") || approvalStatus.equals("REJECTED")) {
		      	 	
		      	 	 criteriaSize--;
		      	 	
		      	 }
	      	
	       }
        	
        	obj = new Object[criteriaSize];
        	
        	if (criteria.containsKey("referenceNumber")) {
        		
        		if (!firstArgument) {
        			
        			jbossQl.append("AND ");	
        			
        		} else {
        			
        			firstArgument = false;
        			jbossQl.append("WHERE ");
        			
        		}
        		
        		jbossQl.append("po.poReferenceNumber LIKE '%" + (String)criteria.get("referenceNumber") + "%' ");
        		
        	}
              
          if (criteria.containsKey("serialNumber")) {
        		
        		serialNumber = (String)criteria.get("serialNumber");
        		
        	
        		
        	}
                
             
            if (criteria.containsKey("batchName")) {
	   	
			   	  if (!firstArgument) {		       	  	
			   	     jbossQl.append("AND ");		       	     
			   	  } else {		       	  	
			   	  	 firstArgument = false;
			   	  	 jbossQl.append("WHERE ");		       	  	 
			   	  }
			   	  
			   	  jbossQl.append("po.apVoucherBatch.vbName=?" + (ctr+1) + " ");
			   	  obj[ctr] = (String)criteria.get("batchName");
			   	  ctr++;
	   	  
	        }
                
                
        	
        	if (criteria.containsKey("supplierCode")) {
	   	
			   	  if (!firstArgument) {		       	  	
			   	     jbossQl.append("AND ");		       	     
			   	  } else {		       	  	
			   	  	 firstArgument = false;
			   	  	 jbossQl.append("WHERE ");		       	  	 
			   	  }
			   	  
			   	  jbossQl.append("po.apSupplier.splSupplierCode=?" + (ctr+1) + " ");
			   	  obj[ctr] = (String)criteria.get("supplierCode");
			   	  ctr++;
	   	  
	        }
	        
	        if (criteria.containsKey("type")) {
	   	
			   	  if (!firstArgument) {		       	  	
			   	     jbossQl.append("AND ");		       	     
			   	  } else {		       	  	
			   	  	 firstArgument = false;
			   	  	 jbossQl.append("WHERE ");		       	  	 
			   	  }
			   	  
			   	  jbossQl.append("po.poType=?" + (ctr+1) + " ");
			   	  obj[ctr] = (String)criteria.get("type");
			   	  ctr++;
	   	  
	        }
	        
			if (!firstArgument) {
			 	jbossQl.append("AND ");
			} else {
			 	firstArgument = false;
			 	jbossQl.append("WHERE ");
			}
			 
		    jbossQl.append("po.poReceiving=?" + (ctr+1) + " ");
		    obj[ctr] = (Byte)criteria.get("receiving");
		    ctr++;
		    
	        if (!firstArgument) {
	  	 	  jbossQl.append("AND ");
	  	    } else {
	  	 	  firstArgument = false;
	  	 	  jbossQl.append("WHERE ");
	  	    }
	  	 
	  	    jbossQl.append("po.poVoid=?" + (ctr+1) + " ");
	  	    obj[ctr] = (Byte)criteria.get("purchaseOrderVoid");
	        ctr++;
	        
	        if (criteria.containsKey("currency")) {
	       	
	       	  if (!firstArgument) {
	       	  	
	       	     jbossQl.append("AND ");
	       	     
	       	  } else {
	       	  	
	       	  	 firstArgument = false;
	       	  	 jbossQl.append("WHERE ");
	       	  	 
	       	  }
	       	  
	       	  jbossQl.append("po.glFunctionalCurrency.fcName=?" + (ctr+1) + " ");
	       	  obj[ctr] = (String)criteria.get("currency");
	       	  ctr++;
	       	  
	      }
	      
	      if (criteria.containsKey("approvalStatus")) {
	       	
	       	  if (!firstArgument) {
	       	  	
	       	     jbossQl.append("AND ");
	       	     
	       	  } else {
	       	  	
	       	  	 firstArgument = false;
	       	  	 jbossQl.append("WHERE ");
	       	  	 
	       	  }
	       	  
	       	  String approvalStatus = (String)criteria.get("approvalStatus");
       	  
	       	  if (approvalStatus.equals("DRAFT")) {
	       	      
	       		jbossQl.append("po.poApprovalStatus IS NULL AND po.poReasonForRejection IS NULL ");
	       	  	
	       	  } else if (approvalStatus.equals("REJECTED")) {
	       	  	
		       	  jbossQl.append("po.poReasonForRejection IS NOT NULL ");
	       	  	
	      	  } else {
	      	  	
		      	  jbossQl.append("po.poApprovalStatus=?" + (ctr+1) + " ");
		       	  obj[ctr] = approvalStatus;
		       	  ctr++;
	      	  	
	      	  }     
	       	  
	      }
	      
	      
	      if (criteria.containsKey("posted")) {
	       	
	       	  if (!firstArgument) {
	       	  	
	       	     jbossQl.append("AND ");
	       	     
	       	  } else {
	       	  	
	       	  	 firstArgument = false;
	       	  	 jbossQl.append("WHERE ");
	       	  	 
	       	  }
	       	  
	       	  jbossQl.append("po.poPosted=?" + (ctr+1) + " ");
	       	  
	       	  String posted = (String)criteria.get("posted");
	       	  
	       	  if (posted.equals("YES")) {
	       	  	
	       	  	obj[ctr] = new Byte(EJBCommon.TRUE);
	       	  	
	       	  } else {
	       	  	
	       	  	obj[ctr] = new Byte(EJBCommon.FALSE);
	       	  	
	       	  }       	  
	       	 
	       	  ctr++;
	       	  
	      }
	      
	      if (criteria.containsKey("printed")) {
		       	
	       	  if (!firstArgument) {
	       	  	
	       	     jbossQl.append("AND ");
	       	     
	       	  } else {
	       	  	
	       	  	 firstArgument = false;
	       	  	 jbossQl.append("WHERE ");
	       	  	 
	       	  }
	       	  
	       	  jbossQl.append("po.poPrinted=?" + (ctr+1) + " ");
	       	  obj[ctr] = (Byte)criteria.get("printed");
	       	  ctr++;
	       	  
	      }
        	
        	if (criteria.containsKey("documentNumberFrom")) {
        		
        		if (!firstArgument) {
        			
        			jbossQl.append("AND ");
        			
        		} else {
        			
        			firstArgument = false;
        			jbossQl.append("WHERE ");
        			
        		}
        		
        		jbossQl.append("po.poDocumentNumber>=?" + (ctr+1) + " ");
        		obj[ctr] = (String)criteria.get("documentNumberFrom");
        		ctr++;
        		
        	}
        	
        	if (criteria.containsKey("documentNumberTo")) {
        		
        		if (!firstArgument) {
        			
        			jbossQl.append("AND ");
        			
        		} else {
        			
        			firstArgument = false;
        			jbossQl.append("WHERE ");
        			
        		}
        		
        		jbossQl.append("po.poDocumentNumber<=?" + (ctr+1) + " ");
        		obj[ctr] = (String)criteria.get("documentNumberTo");
        		ctr++;
        		
        	}
        	
        	if (criteria.containsKey("dateFrom")) {
        		
        		if (!firstArgument) {
        			jbossQl.append("AND ");
        		} else {
        			firstArgument = false;
        			jbossQl.append("WHERE ");
        		}
        		jbossQl.append("po.poDate>=?" + (ctr+1) + " ");
        		obj[ctr] = (Date)criteria.get("dateFrom");
        		ctr++;
        	}  
        	
        	if (criteria.containsKey("dateTo")) {
        		
        		if (!firstArgument) {
        			jbossQl.append("AND ");
        		} else {
        			firstArgument = false;
        			jbossQl.append("WHERE ");
        		}
        		jbossQl.append("po.poDate<=?" + (ctr+1) + " ");
        		obj[ctr] = (Date)criteria.get("dateTo");
        		ctr++;
        		
        	}    
        	
        	if (!firstArgument) {
        		
        		jbossQl.append("AND ");
        		
        	} else {
        		
        		firstArgument = false;
        		jbossQl.append("WHERE ");
        		
        	}
        	
        	jbossQl.append("po.poAdBranch=" + AD_BRNCH + " ");
        	
        	if (!firstArgument) {
        		
        		jbossQl.append("AND ");
        		
        	} else {
        		
        		firstArgument = false;
        		jbossQl.append("WHERE ");
        		
        	}
        	
        	jbossQl.append("po.poAdCompany=" + AD_CMPNY + " ");
        	
        	String orderBy = null;
        	
        	if (ORDER_BY.equals("SUPPLIER CODE")) {	          
		      	      		
		  	  orderBy = "po.apSupplier.splSupplierCode";

		    } else if (ORDER_BY.equals("DOCUMENT NUMBER")) {
		
		  	  orderBy = "po.poDocumentNumber";
		  	
		    }
        	        	
        	if (orderBy != null) {
        		
        		jbossQl.append("ORDER BY " + orderBy + ", po.poDate");
        		
        	} else {
        		
        		jbossQl.append("ORDER BY po.poDate");
        		
        	}
        	
        	if (!isPoLookup) {
        		
	        	jbossQl.append(" OFFSET ?" + (ctr + 1));	        
	        	obj[ctr] = OFFSET;	      
	        	ctr++;
	        	System.out.println("LIMIT------------->"+LIMIT);
	        	jbossQl.append(" LIMIT ?" + (ctr + 1));
	        	obj[ctr] = LIMIT;
	        	ctr++;		
	        	
        	} else {
        		
        		jbossQl.append(" OFFSET ?" + (ctr + 1));	        
	        	obj[ctr] = 0;	      
	        	ctr++;
	        	System.out.println("LIMIT------------->"+LIMIT);
	        	jbossQl.append(" LIMIT ?" + (ctr + 1));
	        	obj[ctr] = 0;
	        	ctr++;	
        	}
        	
        	System.out.println(jbossQl.toString());
        	Collection apPurchaseOrders = apPurchaseOrderHome.getPoByCriteria(jbossQl.toString(), obj);	         
        	
        	if (apPurchaseOrders.size() == 0)
        		throw new GlobalNoRecordFoundException();
        	
        	Iterator i = apPurchaseOrders.iterator();
        	
        	while (i.hasNext()) {
        		
        		LocalApPurchaseOrder apPurchaseOrder = (LocalApPurchaseOrder)i.next(); 
        		
        		/*
        		if (isPoLookup) {
	        		boolean isPoBreak = false;
	        		Collection apPurchaseOrderLines = apPurchaseOrder.getApPurchaseOrderLines();
	        		Iterator j = apPurchaseOrderLines.iterator();
	        		while (j.hasNext()) {
	        			
	        			LocalApPurchaseOrderLine apPurchaseOrderLine = (LocalApPurchaseOrderLine)j.next();
	        			Collection apReceivedPls = apPurchaseOrderLineHome.findByPlPlCode(apPurchaseOrderLine.getPlCode(), AD_CMPNY);
	        			
	        			double totalReceived = 0;
	        			Iterator k = apReceivedPls.iterator();
	        			while (k.hasNext()) {
	        				
	        				LocalApPurchaseOrderLine apReceivedPl = (LocalApPurchaseOrderLine)k.next();
	        				totalReceived += apReceivedPl.getPlQuantity();
	        				
	        			}
	        			
	        			if (apReceivedPls.size() == 0 || totalReceived != apPurchaseOrderLine.getPlQuantity()) {
	        				ApModPurchaseOrderDetails mdetails = new ApModPurchaseOrderDetails();
	    	        		mdetails.setPoCode(apPurchaseOrder.getPoCode());    
	    	        		mdetails.setPoSplSupplierCode(apPurchaseOrder.getApSupplier().getSplSupplierCode());
	    	        		mdetails.setPoDate(apPurchaseOrder.getPoDate());        		
	    	        		mdetails.setPoDocumentNumber(apPurchaseOrder.getPoDocumentNumber());
	    	        		mdetails.setPoReferenceNumber(apPurchaseOrder.getPoReferenceNumber());
	    	        		mdetails.setPoReceiving(apPurchaseOrder.getPoReceiving());
	    	        		
	    	        		
    	        		    if(!serialNumber.trim().equals("")){
		      
						     	if(this.searchSerialNumber(apPurchaseOrder,serialNumber)){
						     	   list.add(mdetails);
						     	
						     	}
						     	
					        }else{
						      	  list.add(mdetails);
						    }

	    	        		
	    	        		
	    	        		
	    	        		
	    	        //		list.add(mdetails);
	    	        		isPoBreak = true;
	        			}
	        			if (isPoBreak) break;
	        			
	        		}
	        		
	        		if (isPoBreak) continue;
	        		
        		} else {
        			
        			ApModPurchaseOrderDetails mdetails = new ApModPurchaseOrderDetails();
	        		mdetails.setPoCode(apPurchaseOrder.getPoCode());    
	        		mdetails.setPoSplSupplierCode(apPurchaseOrder.getApSupplier().getSplSupplierCode());
	        		mdetails.setPoDate(apPurchaseOrder.getPoDate());        		
	        		mdetails.setPoDocumentNumber(apPurchaseOrder.getPoDocumentNumber());
	        		mdetails.setPoReferenceNumber(apPurchaseOrder.getPoReferenceNumber());
	        		mdetails.setPoReceiving(apPurchaseOrder.getPoReceiving());
	        		
        		    if(!serialNumber.trim().equals("")){
		      
				     	if(this.searchSerialNumber(apPurchaseOrder,serialNumber)){
				     	   list.add(mdetails);
				     	
				     	}
				     	
			        }else{
				      	  list.add(mdetails);
				    }

	        	//	list.add(mdetails);
        		}
                */

                ApModPurchaseOrderDetails mdetails = new ApModPurchaseOrderDetails();
                mdetails.setPoCode(apPurchaseOrder.getPoCode());    
                mdetails.setPoSplSupplierCode(apPurchaseOrder.getApSupplier().getSplSupplierCode());
                mdetails.setPoDate(apPurchaseOrder.getPoDate());                
                mdetails.setPoDocumentNumber(apPurchaseOrder.getPoDocumentNumber());
                mdetails.setPoReferenceNumber(apPurchaseOrder.getPoReferenceNumber());
                mdetails.setPoReceiving(apPurchaseOrder.getPoReceiving());
                
                if(!serialNumber.trim().equals("")){
          
                    if(this.searchSerialNumber(apPurchaseOrder,serialNumber)){
                       list.add(mdetails);
                    
                    }
                    
                }else{
                      list.add(mdetails);
                }
        		
        		
        	}
        	System.out.println("the size is : " + list.size());
        	return list;
        	
        } catch (GlobalNoRecordFoundException ex) {
	  	 
	  	  throw ex;
	  	
	  } catch (Exception ex) {
	  	

	  	  ex.printStackTrace();
	  	  throw new EJBException(ex.getMessage());
	  	
	  }
        
    }
    
    
     /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
     public byte getAdPrfEnableApPOBatch(Integer AD_CMPNY) {

        Debug.print("ApFindPurchaseOrderControllerBean getAdPrfEnableApVoucherBatch");
                   
        LocalAdPreferenceHome adPreferenceHome = null;
       
       
        // Initialize EJB Home
         
        try {
             
           adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
               lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);       
             
        } catch (NamingException ex) {
             
           throw new EJBException(ex.getMessage());
             
        }
       

        try {
       	
           LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);
          
           return adPreference.getPrfEnableApPOBatch();
          
        } catch (Exception ex) {
         	 
           Debug.printStackTrace(ex);
           throw new EJBException(ex.getMessage());
          
        }
       
     }
    
     /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList getApOpenVbAll(String TYPE, Integer AD_BRNCH, Integer AD_CMPNY) {
                    
        Debug.print("ApFindPurchaseOrderControllerBean getApOpenVbAll");
        
        LocalApVoucherBatchHome apVoucherBatchHome = null;               
        
        ArrayList list = new ArrayList();
        
        // Initialize EJB Home
        
        try {
            
        	apVoucherBatchHome = (LocalApVoucherBatchHome)EJBHomeFactory.
                lookUpLocalHome(LocalApVoucherBatchHome.JNDI_NAME, LocalApVoucherBatchHome.class);
            
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }        
        
        try {
        	
        	Collection apVoucherBatches = apVoucherBatchHome.findOpenVbByVbType(TYPE, AD_BRNCH, AD_CMPNY);
        	
        	Iterator i = apVoucherBatches.iterator();
        	
        	while (i.hasNext()) {
        		
        		LocalApVoucherBatch apVoucherBatch = (LocalApVoucherBatch)i.next();
        		
        		list.add(apVoucherBatch.getVbName());
        		
        	}
        	
        	return list;
        	
        } catch (Exception ex) {
        	
        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());
        	
        }
            
    }  
    
    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public Integer getApPoSizeByCriteria(HashMap criteria, Integer AD_BRNCH, Integer AD_CMPNY) 
        throws GlobalNoRecordFoundException {
                    
        Debug.print("ApFindPurchaseOrderControllerBean getApPoSizeByCriteria");
        
        LocalApPurchaseOrderHome apPurchaseOrderHome = null;

        //initialized EJB Home
        
        try {
            
        	apPurchaseOrderHome = (LocalApPurchaseOrderHome)EJBHomeFactory.
                lookUpLocalHome(LocalApPurchaseOrderHome.JNDI_NAME, LocalApPurchaseOrderHome.class);

        } catch (NamingException ex) 	{
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try { 
        	
        	StringBuffer jbossQl = new StringBuffer();
        	jbossQl.append("SELECT OBJECT(po) FROM ApPurchaseOrder po ");
        	
        	boolean firstArgument = true;
        	short ctr = 0;
        	int criteriaSize = criteria.size();	      
        	
        	Object obj[];	      
        	
        	// Allocate the size of the object parameter
        	
        	if (criteria.containsKey("referenceNumber")) {
        		
        		criteriaSize--;
        		
        	}
        	
        	if (criteria.containsKey("approvalStatus")) {
	      	
		      	 String approvalStatus = (String)criteria.get("approvalStatus");
		      	
		      	 if (approvalStatus.equals("DRAFT") || approvalStatus.equals("REJECTED")) {
		      	 	
		      	 	 criteriaSize--;
		      	 	
		      	 }
	      	
	       }
        	
        	obj = new Object[criteriaSize];
        	
        	if (criteria.containsKey("referenceNumber")) {
        		
        		if (!firstArgument) {
        			
        			jbossQl.append("AND ");	
        			
        		} else {
        			
        			firstArgument = false;
        			jbossQl.append("WHERE ");
        			
        		}
        		
        		jbossQl.append("po.poReferenceNumber LIKE '%" + (String)criteria.get("referenceNumber") + "%' ");
        		
        	}
        	
        	if (criteria.containsKey("supplierCode")) {
	   	
			   	  if (!firstArgument) {		       	  	
			   	     jbossQl.append("AND ");		       	     
			   	  } else {		       	  	
			   	  	 firstArgument = false;
			   	  	 jbossQl.append("WHERE ");		       	  	 
			   	  }
			   	  
			   	  jbossQl.append("po.apSupplier.splSupplierCode=?" + (ctr+1) + " ");
			   	  obj[ctr] = (String)criteria.get("supplierCode");
			   	  ctr++;
	   	  
	        }
	        
	        if (criteria.containsKey("type")) {
	   	
			   	  if (!firstArgument) {		       	  	
			   	     jbossQl.append("AND ");		       	     
			   	  } else {		       	  	
			   	  	 firstArgument = false;
			   	  	 jbossQl.append("WHERE ");		       	  	 
			   	  }
			   	  
			   	  jbossQl.append("po.poType=?" + (ctr+1) + " ");
			   	  obj[ctr] = (String)criteria.get("type");
			   	  ctr++;
	   	  
	        }
	        
			if (!firstArgument) {
			 	jbossQl.append("AND ");
			} else {
			 	firstArgument = false;
			 	jbossQl.append("WHERE ");
			}
			 
		    jbossQl.append("po.poReceiving=?" + (ctr+1) + " ");
		    obj[ctr] = (Byte)criteria.get("receiving");
		    ctr++;
		    
	        if (!firstArgument) {
	  	 	  jbossQl.append("AND ");
	  	    } else {
	  	 	  firstArgument = false;
	  	 	  jbossQl.append("WHERE ");
	  	    }
	  	 
	  	    jbossQl.append("po.poVoid=?" + (ctr+1) + " ");
	  	    obj[ctr] = (Byte)criteria.get("purchaseOrderVoid");
	        ctr++;
	        
	        if (criteria.containsKey("currency")) {
	       	
	       	  if (!firstArgument) {
	       	  	
	       	     jbossQl.append("AND ");
	       	     
	       	  } else {
	       	  	
	       	  	 firstArgument = false;
	       	  	 jbossQl.append("WHERE ");
	       	  	 
	       	  }
	       	  
	       	  jbossQl.append("po.glFunctionalCurrency.fcName=?" + (ctr+1) + " ");
	       	  obj[ctr] = (String)criteria.get("currency");
	       	  ctr++;
	       	  
	      }
	      
	      if (criteria.containsKey("approvalStatus")) {
	       	
	       	  if (!firstArgument) {
	       	  	
	       	     jbossQl.append("AND ");
	       	     
	       	  } else {
	       	  	
	       	  	 firstArgument = false;
	       	  	 jbossQl.append("WHERE ");
	       	  	 
	       	  }
	       	  
	       	  String approvalStatus = (String)criteria.get("approvalStatus");
       	  
	       	  if (approvalStatus.equals("DRAFT")) {
	       	      
		       	  jbossQl.append("po.poApprovalStatus IS NULL AND po.poReasonForRejection IS NULL ");
	       	  	
	       	  } else if (approvalStatus.equals("REJECTED")) {
	       	  	
		       	  jbossQl.append("po.poReasonForRejection IS NOT NULL ");
	       	  	
	      	  } else {
	      	  	
		      	  jbossQl.append("po.poApprovalStatus=?" + (ctr+1) + " ");
		       	  obj[ctr] = approvalStatus;
		       	  ctr++;
	      	  	
	      	  }     
	       	  
	      }
	      
	      
	      if (criteria.containsKey("posted")) {
	       	
	       	  if (!firstArgument) {
	       	  	
	       	     jbossQl.append("AND ");
	       	     
	       	  } else {
	       	  	
	       	  	 firstArgument = false;
	       	  	 jbossQl.append("WHERE ");
	       	  	 
	       	  }
	       	  
	       	  jbossQl.append("po.poPosted=?" + (ctr+1) + " ");
	       	  
	       	  String posted = (String)criteria.get("posted");
	       	  
	       	  if (posted.equals("YES")) {
	       	  	
	       	  	obj[ctr] = new Byte(EJBCommon.TRUE);
	       	  	
	       	  } else {
	       	  	
	       	  	obj[ctr] = new Byte(EJBCommon.FALSE);
	       	  	
	       	  }       	  
	       	 
	       	  ctr++;
	       	  
	      }
        	
        	if (criteria.containsKey("documentNumberFrom")) {
        		
        		if (!firstArgument) {
        			
        			jbossQl.append("AND ");
        			
        		} else {
        			
        			firstArgument = false;
        			jbossQl.append("WHERE ");
        			
        		}
        		
        		jbossQl.append("po.poDocumentNumber>=?" + (ctr+1) + " ");
        		obj[ctr] = (String)criteria.get("documentNumberFrom");
        		ctr++;
        		
        	}
        	
        	if (criteria.containsKey("documentNumberTo")) {
        		
        		if (!firstArgument) {
        			
        			jbossQl.append("AND ");
        			
        		} else {
        			
        			firstArgument = false;
        			jbossQl.append("WHERE ");
        			
        		}
        		
        		jbossQl.append("po.poDocumentNumber<=?" + (ctr+1) + " ");
        		obj[ctr] = (String)criteria.get("documentNumberTo");
        		ctr++;
        		
        	}
        	
        	if (criteria.containsKey("dateFrom")) {
        		
        		if (!firstArgument) {
        			jbossQl.append("AND ");
        		} else {
        			firstArgument = false;
        			jbossQl.append("WHERE ");
        		}
        		jbossQl.append("po.poDate>=?" + (ctr+1) + " ");
        		obj[ctr] = (Date)criteria.get("dateFrom");
        		ctr++;
        	}  
        	
        	if (criteria.containsKey("dateTo")) {
        		
        		if (!firstArgument) {
        			jbossQl.append("AND ");
        		} else {
        			firstArgument = false;
        			jbossQl.append("WHERE ");
        		}
        		jbossQl.append("po.poDate<=?" + (ctr+1) + " ");
        		obj[ctr] = (Date)criteria.get("dateTo");
        		ctr++;
        		
        	}    
        	
        	if (!firstArgument) {
        		
        		jbossQl.append("AND ");
        		
        	} else {
        		
        		firstArgument = false;
        		jbossQl.append("WHERE ");
        		
        	}
        	
        	jbossQl.append("po.poAdBranch=" + AD_BRNCH + " ");
        	
        	if (!firstArgument) {
        		
        		jbossQl.append("AND ");
        		
        	} else {
        		
        		firstArgument = false;
        		jbossQl.append("WHERE ");
        		
        	}
        	
        	jbossQl.append("po.poAdCompany=" + AD_CMPNY + " ");        	        	
        	
        	Collection apPurchaseOrders = apPurchaseOrderHome.getPoByCriteria(jbossQl.toString(), obj);	         
        	
        	if (apPurchaseOrders.size() == 0)
        		throw new GlobalNoRecordFoundException();
        	
        	return new Integer(apPurchaseOrders.size());
        	
        } catch (GlobalNoRecordFoundException ex) {
	  	 
	  	  throw ex;
	  	
	  } catch (Exception ex) {
	  	

	  	  ex.printStackTrace();
	  	  throw new EJBException(ex.getMessage());
	  	
	  }
        
    }
    
    /**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public byte getAdPrfApUseSupplierPulldown(Integer AD_CMPNY) {
		
		Debug.print("ApFindPurchaseOrderControllerBean getAdPrfApUseSupplierPulldown");
		
		LocalAdPreferenceHome adPreferenceHome = null;

		// Initialize EJB Home
		
		try {
			
			adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);       
			
		} catch (NamingException ex) {
			
			throw new EJBException(ex.getMessage());
			
		}
		
		try {
			
			LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);
			
			return  adPreference.getPrfApUseSupplierPulldown();
			
		} catch (Exception ex) {
			
			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());
			
		}
		
	}


	private boolean searchSerialNumber(LocalApPurchaseOrder apPurchaseOrder, String serialNumber) throws FinderException{
    	Debug.print("ArFindInvoiceControllerBean searchSerialNumber");
    
    
    	LocalInvTagHome invTagHome = null;
  
    	try {

           invTagHome = (LocalInvTagHome)EJBHomeFactory.
              lookUpLocalHome(LocalInvTagHome.JNDI_NAME, LocalInvTagHome.class);

       } catch (NamingException ex) {

           throw new EJBException(ex.getMessage());

       }
    
    
   		 Collection apPurchaseOrderLines = apPurchaseOrder.getApPurchaseOrderLines();
    		
    		Iterator i = apPurchaseOrderLines.iterator();
    		
    		while(i.hasNext()){
    		
    			LocalApPurchaseOrderLine apPurchaseOrderLine = (LocalApPurchaseOrderLine)i.next();
    			
    			if(apPurchaseOrderLine.getInvTags().size()>0){
    			
    				Collection invTags = invTagHome.findTgSerialNumberByPlCode(serialNumber, apPurchaseOrderLine.getPlCode());
    				
    				if(invTags.size()> 0){
    					return true;
    				}
    			}
    		
    		
    		}
    	
    
    
    	return false;
    }
    // SessionBean methods

    /**
     * @ejb:create-method view-type="remote"
     **/
    public void ejbCreate() throws CreateException {

       Debug.print("ApFindPurchaseOrderControllerBean ejbCreate");
      
    }
}
