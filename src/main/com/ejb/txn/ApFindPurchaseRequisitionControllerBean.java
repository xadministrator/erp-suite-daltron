
/*
 * ApFindPurchaseRequisitionControllerBean.java
 *
 * Created on April 20, 2005, 9:30 PM
 *
 * @author  Aliza D.J. Anos
 */


package com.ejb.txn;

import java.sql.Time;
import java.util.*;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;
import javax.naming.NamingException;


import com.ejb.ad.LocalAdAmountLimit;
import com.ejb.ad.LocalAdAmountLimitHome;
import com.ejb.ad.LocalAdApproval;
import com.ejb.ad.LocalAdApprovalHome;
import com.ejb.ad.LocalAdApprovalQueue;
import com.ejb.ad.LocalAdApprovalQueueHome;
import com.ejb.ad.LocalAdApprovalUser;
import com.ejb.ad.LocalAdApprovalUserHome;
import com.ejb.ad.LocalAdBranch;
import com.ejb.ad.LocalAdBranchHome;
import com.ejb.ad.LocalAdBranchDocumentSequenceAssignment;
import com.ejb.ad.LocalAdBranchDocumentSequenceAssignmentHome;
import com.ejb.ad.LocalAdBranchItemLocationHome;
import com.ejb.ad.LocalAdCompany;
import com.ejb.ad.LocalAdCompanyHome;
import com.ejb.ad.LocalAdLookUpValue;
import com.ejb.ad.LocalAdLookUpValueHome;
import com.ejb.ad.LocalAdPaymentTerm;
import com.ejb.ad.LocalAdPaymentTermHome;
import com.ejb.ad.LocalAdPreference;
import com.ejb.ad.LocalAdPreferenceHome;
import com.ejb.ad.LocalAdUser;
import com.ejb.ad.LocalAdUserHome;
import com.ejb.ap.LocalApCanvass;
import com.ejb.ap.LocalApCanvassHome;
import com.ejb.ap.LocalApPurchaseOrder;
import com.ejb.ap.LocalApPurchaseOrderHome;
import com.ejb.ap.LocalApPurchaseOrderLine;
import com.ejb.ap.LocalApPurchaseOrderLineHome;
import com.ejb.ap.LocalApPurchaseRequisition;
import com.ejb.ap.LocalApPurchaseRequisitionHome;
import com.ejb.ap.LocalApPurchaseRequisitionLine;
import com.ejb.ap.LocalApPurchaseRequisitionLineHome;
import com.ejb.ap.LocalApSupplier;
import com.ejb.ap.LocalApSupplierHome;
import com.ejb.ap.LocalApTaxCode;
import com.ejb.ap.LocalApTaxCodeHome;
import com.ejb.ad.LocalAdUserHome;
import com.ejb.ad.LocalAdUser;
import com.ejb.ad.LocalAdLookUpValueHome;
import com.ejb.ad.LocalAdLookUpValue;
import com.ejb.inv.LocalInvTransactionalBudgetHome;
import com.ejb.inv.LocalInvTransactionalBudget;
import com.ejb.inv.LocalInvItemHome;
import com.ejb.inv.LocalInvItem;
import com.ejb.inv.LocalInvTag;
import com.ejb.inv.LocalInvTagHome;
import com.ejb.ad.LocalAdUserHome;

import com.ejb.exception.GlobalConversionDateNotExistException;
import com.ejb.exception.GlobalDocumentNumberNotUniqueException;
import com.ejb.exception.GlobalInvItemLocationNotFoundException;
import com.ejb.exception.GlobalNoApprovalApproverFoundException;
import com.ejb.exception.GlobalNoApprovalRequesterFoundException;
import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.exception.GlobalPaymentTermInvalidException;
import com.ejb.exception.GlobalRecordAlreadyDeletedException;
import com.ejb.exception.GlobalRecordAlreadyExistException;
import com.ejb.exception.GlobalSupplierItemInvalidException;
import com.ejb.exception.GlobalTransactionAlreadyApprovedException;
import com.ejb.exception.GlobalTransactionAlreadyPendingException;
import com.ejb.exception.GlobalTransactionAlreadyPostedException;
import com.ejb.exception.GlobalTransactionAlreadyVoidException;
import com.ejb.genfld.LocalGenSegmentHome;
import com.ejb.genfld.LocalGenValueSetValueHome;
import com.ejb.gl.LocalAdDocumentSequenceAssignment;
import com.ejb.gl.LocalAdDocumentSequenceAssignmentHome;
import com.ejb.gl.LocalGlFunctionalCurrency;
import com.ejb.gl.LocalGlFunctionalCurrencyHome;
import com.ejb.gl.LocalGlFunctionalCurrencyRate;
import com.ejb.gl.LocalGlFunctionalCurrencyRateHome;
import com.ejb.inv.LocalInvItem;
import com.ejb.inv.LocalInvItemHome;
import com.ejb.inv.LocalInvItemLocation;
import com.ejb.inv.LocalInvItemLocationHome;
import com.ejb.inv.LocalInvUnitOfMeasure;
import com.ejb.inv.LocalInvUnitOfMeasureConversion;
import com.ejb.inv.LocalInvUnitOfMeasureConversionHome;
import com.ejb.inv.LocalInvUnitOfMeasureHome;
import com.util.AbstractSessionBean;
import com.util.ApModPurchaseOrderDetails;
import com.util.ApModPurchaseOrderLineDetails;
import com.util.ApModPurchaseRequisitionDetails;
import com.util.ApModPurchaseRequisitionLineDetails;
import com.util.InvModTagListDetails;
import com.util.Debug;
import com.util.EJBCommon;
import com.util.EJBHomeFactory;

/**
 * @ejb:bean name="ApFindPurchaseRequisitionControllerEJB"
 *           display-name="Used for finding purchase requisitions"
 *           type="Stateless"
 *           view-type="remote"
 *           jndi-name="ejb/ApFindPurchaseRequisitionControllerEJB"
 *
 * @ejb:interface remote-class="com.ejb.txn.ApFindPurchaseRequisitionController"
 *                extends="javax.ejb.EJBObject"
 *
 * @ejb:home remote-class="com.ejb.txn.ApFindPurchaseRequisitionControllerHome"
 *           extends="javax.ejb.EJBHome"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="invuser"
 *                        role-link="invuserlink"
 *
 * @ejb:permission role-name="invuser"
 *
*/

public class ApFindPurchaseRequisitionControllerBean extends AbstractSessionBean {


	 /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList getAdUsrAll(Integer AD_CMPNY) {

        Debug.print("ApFindPurchaseRequisitionControllerBean getAdUsrAll");

        LocalAdUserHome adUserHome = null;

        LocalAdUser adUser = null;

        Collection adUsers = null;

        ArrayList list = new ArrayList();

        // Initialize EJB Home

        try {

            adUserHome = (LocalAdUserHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdUserHome.JNDI_NAME, LocalAdUserHome.class);

        } catch (NamingException ex) {

            throw new EJBException(ex.getMessage());

        }

        try {

            adUsers = adUserHome.findUsrAll(AD_CMPNY);

        } catch (FinderException ex) {

        } catch (Exception ex) {

            throw new EJBException(ex.getMessage());
        }

        if (adUsers.isEmpty()) {

            return null;

        }

        Iterator i = adUsers.iterator();

        while (i.hasNext()) {

            adUser = (LocalAdUser)i.next();

            list.add(adUser.getUsrName());

        }

        return list;

    }

    /**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public ArrayList getAdLvDEPARTMENT(Integer AD_CMPNY) {

		Debug.print("ApRepAnnualProcurementControllerBean getAdLvDEPARTMENT");

		LocalAdLookUpValueHome adLookUpValueHome = null;

		ArrayList list = new ArrayList();

		// Initialize EJB Home

		try {

			adLookUpValueHome = (LocalAdLookUpValueHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdLookUpValueHome.JNDI_NAME, LocalAdLookUpValueHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}

		try {

			Collection adLookUpValues = adLookUpValueHome.findByLuName("AD DEPARTMENT", AD_CMPNY);

			Iterator i = adLookUpValues.iterator();

			while (i.hasNext()) {

				LocalAdLookUpValue adLookUpValue = (LocalAdLookUpValue)i.next();

				list.add(adLookUpValue.getLvName());

			}

			return list;

		} catch (Exception ex) {

			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());

		}

	}

    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList getGlFcAll(Integer AD_CMPNY) {

        Debug.print("ApFindPurchaseRequisitionControllerBean getGlFcAll");

        LocalGlFunctionalCurrencyHome glFunctionalCurrencyHome = null;

        ArrayList list = new ArrayList();

        // Initialize EJB Home

        try {

            glFunctionalCurrencyHome = (LocalGlFunctionalCurrencyHome)EJBHomeFactory.
                lookUpLocalHome(LocalGlFunctionalCurrencyHome.JNDI_NAME, LocalGlFunctionalCurrencyHome.class);

        } catch (NamingException ex) {

            throw new EJBException(ex.getMessage());

        }

        try {

            Collection glFunctionalCurrencies = glFunctionalCurrencyHome.findFcAll(AD_CMPNY);

	        Iterator i = glFunctionalCurrencies.iterator();

	        while (i.hasNext()) {

	        	LocalGlFunctionalCurrency glFunctionalCurrency = (LocalGlFunctionalCurrency)i.next();

	        	list.add(glFunctionalCurrency.getFcName());

	        }

	        return list;

        } catch (Exception ex) {

        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());

        }

    }

    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public short getGlFcPrecisionUnit(Integer AD_CMPNY) {

    	Debug.print("ApFindPurchaseRequisitionControllerBean getGlFcPrecisionUnit");

    	LocalAdCompanyHome adCompanyHome = null;


    	// Initialize EJB Home

    	try {

    		adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);

    	} catch (NamingException ex) {

    		throw new EJBException(ex.getMessage());

    	}

    	try {

    		LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);

    		return  adCompany.getGlFunctionalCurrency().getFcPrecision();

    	} catch (Exception ex) {

    		Debug.printStackTrace(ex);
    		throw new EJBException(ex.getMessage());

    	}

    }

    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList getApPrByCriteria(HashMap criteria,
        Integer OFFSET, Integer LIMIT, String ORDER_BY, Integer AD_BRNCH, boolean omitBranch, Integer AD_CMPNY)
        throws GlobalNoRecordFoundException {

        Debug.print("ApFindPurchaseRequisitionControllerBean getApPrByCriteria");

        LocalApPurchaseRequisitionHome apPurchaseRequisitionHome = null;
        LocalAdBranchHome adBranchHome = null;

        ArrayList list = new ArrayList();

        //initialized EJB Home

        try {

        	apPurchaseRequisitionHome = (LocalApPurchaseRequisitionHome)EJBHomeFactory.
                lookUpLocalHome(LocalApPurchaseRequisitionHome.JNDI_NAME, LocalApPurchaseRequisitionHome.class);

        	adBranchHome = (LocalAdBranchHome)EJBHomeFactory.
                    lookUpLocalHome(LocalAdBranchHome.JNDI_NAME, LocalAdBranchHome.class);

        } catch (NamingException ex) 	{

            throw new EJBException(ex.getMessage());

        }

        try {

        	StringBuffer jbossQl = new StringBuffer();

        	jbossQl.append("SELECT OBJECT(pr) FROM ApPurchaseRequisition pr ");

        	boolean firstArgument = true;
        	short ctr = 0;
        	int criteriaSize = criteria.size() + 2;

        	Object obj[];

        	// Allocate the size of the object parameter

        	if (criteria.containsKey("referenceNumber")) {

        		criteriaSize--;

        	}

        	if (criteria.containsKey("approvalStatus")) {

        		String approvalStatus = (String)criteria.get("approvalStatus");

        		if (approvalStatus.equals("DRAFT") || approvalStatus.equals("REJECTED")) {

        			criteriaSize--;

        		}

        	}

        	if (criteria.containsKey("canvassApprovalStatus")) {

        		String canvassApprovalStatus = (String)criteria.get("canvassApprovalStatus");

        		if (canvassApprovalStatus.equals("DRAFT") || canvassApprovalStatus.equals("REJECTED")) {

        			criteriaSize--;

        		}

        	}

        	obj = new Object[criteriaSize];

        	if (criteria.containsKey("referenceNumber")) {

        		if (!firstArgument) {

        			jbossQl.append("AND ");

        		} else {

        			firstArgument = false;
        			jbossQl.append("WHERE ");

        		}

        		jbossQl.append("pr.prReferenceNumber LIKE '%" + (String)criteria.get("referenceNumber") + "%' ");

        	}

        	if (!firstArgument) {
        		jbossQl.append("AND ");
        	} else {
        		firstArgument = false;
        		jbossQl.append("WHERE ");
        	}

        	jbossQl.append("pr.prVoid=?" + (ctr+1) + " ");
        	obj[ctr] = (Byte)criteria.get("purchaseRequisitionVoid");
        	ctr++;

        	if(criteria.containsKey("purchaseRequisitionGenerated")){

        		if (!firstArgument) {

        			jbossQl.append("AND ");

        		} else {

        			firstArgument = false;
        			jbossQl.append("WHERE ");

        		}

        		jbossQl.append("pr.prGenerated=?" + (ctr+1) + " ");
            	obj[ctr] = (Byte)criteria.get("purchaseRequisitionGenerated");
            	ctr++;
        	}


        	/*if(criteria.containsKey("canvassRejected")){

        		if (!firstArgument) {

        			jbossQl.append("AND ");

        		} else {

        			firstArgument = false;
        			jbossQl.append("WHERE ");

        		}

        		if((Byte)criteria.get("canvassRejected") == 1){

        			jbossQl.append("pr.prCanvassReasonForRejection IS NOT NULL ");
        		} else {
        			jbossQl.append("pr.prCanvassReasonForRejection IS NULL ");
        		}


            	obj[ctr] = (Byte)criteria.get("canvassRejected");
            	ctr++;
        	} else {

        		jbossQl.append("AND pr.prCanvassReasonForRejection IS NULL ");
        	}
        	*/

			if (criteria.containsKey("user")) {

        		if (!firstArgument) {

        			jbossQl.append("AND ");

        		} else {

        			firstArgument = false;
        			jbossQl.append("WHERE ");

        		}

        		jbossQl.append("pr.prLastModifiedBy = '" + (String)criteria.get("user") + "' ");
        		obj[ctr] = (String)criteria.get("user");
        		ctr++;

        	}
			/*
			if (criteria.containsKey("department")) {

        		if (!firstArgument) {

        			jbossQl.append("AND ");

        		} else {

        			firstArgument = false;
        			jbossQl.append("WHERE ");

        		}

        		jbossQl.append("pr.prLastModifiedBy = '" + (String)criteria.get("user") + "' ");
        		obj[ctr] = (String)criteria.get("user");
        		ctr++;

        	}
			*/

        	if (criteria.containsKey("currency")) {

        		if (!firstArgument) {

        			jbossQl.append("AND ");

        		} else {

        			firstArgument = false;
        			jbossQl.append("WHERE ");

        		}

        		jbossQl.append("pr.glFunctionalCurrency.fcName=?" + (ctr+1) + " ");
        		obj[ctr] = (String)criteria.get("currency");
        		ctr++;

        	}


        	if (criteria.containsKey("approvalStatus")) {

        		if (!firstArgument) {

        			jbossQl.append("AND ");

        		} else {

        			firstArgument = false;
        			jbossQl.append("WHERE ");

        		}

        		String approvalStatus = (String)criteria.get("approvalStatus");

        		if (approvalStatus.equals("DRAFT")) {

        			jbossQl.append("pr.prApprovalStatus IS NULL AND pr.prReasonForRejection IS NULL ");

        		} else if (approvalStatus.equals("REJECTED")) {

        			jbossQl.append("pr.prReasonForRejection IS NOT NULL ");

        		} else {

        			jbossQl.append("pr.prApprovalStatus=?" + (ctr+1) + " ");
        			obj[ctr] = approvalStatus;
        			ctr++;

        		}

        	}


        	if (criteria.containsKey("posted")) {

        		if (!firstArgument) {

        			jbossQl.append("AND ");

        		} else {

        			firstArgument = false;
        			jbossQl.append("WHERE ");

        		}

        		jbossQl.append("pr.prPosted=?" + (ctr+1) + " ");

        		String posted = (String)criteria.get("posted");

        		if (posted.equals("YES")) {

        			obj[ctr] = new Byte(EJBCommon.TRUE);

        		} else {

        			obj[ctr] = new Byte(EJBCommon.FALSE);

        		}

        		ctr++;

        	}

        	if (criteria.containsKey("canvassApprovalStatus")) {

        		if (!firstArgument) {

        			jbossQl.append("AND ");

        		} else {

        			firstArgument = false;
        			jbossQl.append("WHERE ");

        		}

        		String canvassApprovalStatus = (String)criteria.get("canvassApprovalStatus");

        		if (canvassApprovalStatus.equals("DRAFT")) {

        			jbossQl.append("pr.prCanvassApprovalStatus IS NULL AND pr.prCanvassReasonForRejection IS NULL ");

        		} else if (canvassApprovalStatus.equals("REJECTED")) {

        			jbossQl.append("pr.prCanvassReasonForRejection IS NOT NULL ");
        			omitBranch=true;

        		} else {

        			jbossQl.append("pr.prCanvassApprovalStatus=?" + (ctr+1) + " ");
        			obj[ctr] = canvassApprovalStatus;
        			ctr++;

        		}

        	}



			if (criteria.containsKey("canvassPosted")) {

        		if (!firstArgument) {

        			jbossQl.append("AND ");

        		} else {

        			firstArgument = false;
        			jbossQl.append("WHERE ");

        		}

        		jbossQl.append("pr.prCanvassPosted=?" + (ctr+1) + " ");

        		String canvassPosted = (String)criteria.get("canvassPosted");

        		if (canvassPosted.equals("YES")) {

        			obj[ctr] = new Byte(EJBCommon.TRUE);

        		} else {

        			obj[ctr] = new Byte(EJBCommon.FALSE);

        		}

        		ctr++;

        	}



        	if (criteria.containsKey("documentNumberFrom")) {

        		if (!firstArgument) {

        			jbossQl.append("AND ");

        		} else {

        			firstArgument = false;
        			jbossQl.append("WHERE ");

        		}

        		jbossQl.append("pr.prNumber>=?" + (ctr+1) + " ");
        		obj[ctr] = (String)criteria.get("documentNumberFrom");
        		ctr++;

        	}

        	if (criteria.containsKey("documentNumberTo")) {

        		if (!firstArgument) {

        			jbossQl.append("AND ");

        		} else {

        			firstArgument = false;
        			jbossQl.append("WHERE ");

        		}

        		jbossQl.append("pr.prNumber<=?" + (ctr+1) + " ");
        		obj[ctr] = (String)criteria.get("documentNumberTo");
        		ctr++;

        	}

        	if (criteria.containsKey("dateFrom")) {

        		if (!firstArgument) {
        			jbossQl.append("AND ");
        		} else {
        			firstArgument = false;
        			jbossQl.append("WHERE ");
        		}
        		jbossQl.append("pr.prDate>=?" + (ctr+1) + " ");
        		obj[ctr] = (Date)criteria.get("dateFrom");
        		ctr++;
        	}

        	if (criteria.containsKey("dateTo")) {

        		if (!firstArgument) {
        			jbossQl.append("AND ");
        		} else {
        			firstArgument = false;
        			jbossQl.append("WHERE ");
        		}
        		jbossQl.append("pr.prDate<=?" + (ctr+1) + " ");
        		obj[ctr] = (Date)criteria.get("dateTo");
        		ctr++;

        	}

        	if (criteria.containsKey("nextRunDateFrom")) {

       	  	 if (!firstArgument) {
       	  	 	jbossQl.append("AND ");
       	  	 } else {
       	  	 	firstArgument = false;
       	  	 	jbossQl.append("WHERE ");
       	  	 }
       	  	 jbossQl.append("pr.prNextRunDate>=?" + (ctr+1) + " ");
       	  	 System.out.println("DATE--->"+criteria.get("nextRunDateFrom"));
       	  	 obj[ctr] = (Date)criteria.get("nextRunDateFrom");
       	  	 ctr++;
       	  }

       	  if (criteria.containsKey("nextRunDateTo")) {

       	  	 if (!firstArgument) {
       	  	 	jbossQl.append("AND ");
       	  	 } else {
       	  	 	firstArgument = false;
       	  	 	jbossQl.append("WHERE ");
       	  	 }
       	  	 jbossQl.append("pr.prNextRunDate<=?" + (ctr+1) + " ");
       	  	 obj[ctr] = (Date)criteria.get("nextRunDateTo");
       	  	 ctr++;

       	  }

        	if (!firstArgument) {

        		jbossQl.append("AND ");

        	} else {

        		firstArgument = false;
        		jbossQl.append("WHERE ");

        	}

        	if (!omitBranch) {
	        	jbossQl.append("pr.prAdBranch=" + AD_BRNCH + " ");

	        	if (!firstArgument) {

	        		jbossQl.append("AND ");

	        	} else {

	        		firstArgument = false;
	        		jbossQl.append("WHERE ");

	        	}
        	}

        	jbossQl.append("pr.prAdCompany=" + AD_CMPNY + " ");

        	String orderBy = null;

        	if (ORDER_BY.equals("DATE")) {

        		orderBy = "pr.prDate";

        	} else if (ORDER_BY.equals("DOCUMENT NUMBER")) {

        		orderBy = "pr.prNumber";

        	}

        	jbossQl.append("ORDER BY " + orderBy);

        	jbossQl.append(" OFFSET ?" + (ctr + 1));
        	obj[ctr] = OFFSET;
        	ctr++;

        	jbossQl.append(" LIMIT ?" + (ctr + 1));
        	obj[ctr] = LIMIT;
        	ctr++;
        	System.out.println(jbossQl.toString());
        	Collection apPurchaseRequisitions = apPurchaseRequisitionHome.getPrByCriteria(jbossQl.toString(), obj);

        	if (apPurchaseRequisitions.size() == 0)
        		throw new GlobalNoRecordFoundException();

        	Iterator i = apPurchaseRequisitions.iterator();

        	while (i.hasNext()) {

        		LocalApPurchaseRequisition apPurchaseRequisition = (LocalApPurchaseRequisition)i.next();

        		ApModPurchaseRequisitionDetails mdetails = new ApModPurchaseRequisitionDetails();
        		mdetails.setPrCode(apPurchaseRequisition.getPrCode());
        		mdetails.setPrNumber(apPurchaseRequisition.getPrNumber());
        		mdetails.setPrDate(apPurchaseRequisition.getPrDate());
        		mdetails.setPrReferenceNumber(apPurchaseRequisition.getPrReferenceNumber());
        		LocalAdBranch adBranchCode = adBranchHome.findByPrimaryKey(apPurchaseRequisition.getPrAdBranch());
        		mdetails.setPrAdBranchCode(adBranchCode.getBrBranchCode());
        		mdetails.setPrLastModifiedBy(apPurchaseRequisition.getPrLastModifiedBy());

        		list.add(mdetails);

        	}

        	return list;

        } catch (GlobalNoRecordFoundException ex) {

	  	  throw ex;

	  } catch (Exception ex) {


	  	  ex.printStackTrace();
	  	  throw new EJBException(ex.getMessage());

	  }

    }

    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public Integer getApPrSizeByCriteria(HashMap criteria, Integer AD_BRNCH, Integer AD_CMPNY)
        throws GlobalNoRecordFoundException {

        Debug.print("ApFindPurchaseRequisitionControllerBean getApPrSizeByCriteria");

        LocalApPurchaseRequisitionHome apPurchaseRequisitionHome = null;

        //initialized EJB Home

        try {

        	apPurchaseRequisitionHome = (LocalApPurchaseRequisitionHome)EJBHomeFactory.
                lookUpLocalHome(LocalApPurchaseRequisitionHome.JNDI_NAME, LocalApPurchaseRequisitionHome.class);

        } catch (NamingException ex) 	{

            throw new EJBException(ex.getMessage());

        }

        try {

        	StringBuffer jbossQl = new StringBuffer();
        	jbossQl.append("SELECT OBJECT(pr) FROM ApPurchaseRequisition pr ");

        	boolean firstArgument = true;
        	short ctr = 0;
        	int criteriaSize = criteria.size();

        	Object obj[];

        	// Allocate the size of the object parameter

        	if (criteria.containsKey("referenceNumber")) {

        		criteriaSize--;

        	}

        	if (criteria.containsKey("approvalStatus")) {

        		String approvalStatus = (String)criteria.get("approvalStatus");

        		if (approvalStatus.equals("DRAFT") || approvalStatus.equals("REJECTED")) {

        			criteriaSize--;

        		}

        	}

        	if (criteria.containsKey("canvassApprovalStatus")) {

        		String canvassApprovalStatus = (String)criteria.get("canvassApprovalStatus");

        		if (canvassApprovalStatus.equals("DRAFT") || canvassApprovalStatus.equals("REJECTED")) {

        			criteriaSize--;

        		}

        	}

        	obj = new Object[criteriaSize];

        	if (criteria.containsKey("referenceNumber")) {

        		if (!firstArgument) {

        			jbossQl.append("AND ");

        		} else {

        			firstArgument = false;
        			jbossQl.append("WHERE ");

        		}

        		jbossQl.append("pr.prReferenceNumber LIKE '%" + (String)criteria.get("referenceNumber") + "%' ");

        	}

        	if (!firstArgument) {
        		jbossQl.append("AND ");
        	} else {
        		firstArgument = false;
        		jbossQl.append("WHERE ");
        	}

        	jbossQl.append("pr.prVoid=?" + (ctr+1) + " ");
        	obj[ctr] = (Byte)criteria.get("purchaseRequisitionVoid");
        	ctr++;

        	if(criteria.containsKey("purchaseRequisitionGenerated")){

        		if (!firstArgument) {

        			jbossQl.append("AND ");

        		} else {

        			firstArgument = false;
        			jbossQl.append("WHERE ");

        		}

        		jbossQl.append("pr.prGenerated=?" + (ctr+1) + " ");
            	obj[ctr] = (Byte)criteria.get("purchaseRequisitionGenerated");
            	ctr++;
        	}




        	/*if(criteria.containsKey("canvassRejected")){

        		if (!firstArgument) {

        			jbossQl.append("AND ");

        		} else {

        			firstArgument = false;
        			jbossQl.append("WHERE ");

        		}

        		if((Byte)criteria.get("canvassRejected") == 1){
        			jbossQl.append("pr.prCanvassReasonForRejection IS NOT NULL ");
        		} else {
        			jbossQl.append("pr.prCanvassReasonForRejection IS NULL ");
        		}


            	obj[ctr] = (Byte)criteria.get("canvassRejected");
            	ctr++;
        	} else {
        		jbossQl.append("AND pr.prCanvassReasonForRejection IS NULL ");
        	}*/

        	if (criteria.containsKey("user")) {

        		if (!firstArgument) {

        			jbossQl.append("AND ");

        		} else {

        			firstArgument = false;
        			jbossQl.append("WHERE ");

        		}

        		jbossQl.append("pr.prLastModifiedBy LIKE '%" + (String)criteria.get("user") + "%' ");
        		obj[ctr] = (String)criteria.get("user");
        		ctr++;

        	}

        	if (criteria.containsKey("currency")) {

        		if (!firstArgument) {

        			jbossQl.append("AND ");

        		} else {

        			firstArgument = false;
        			jbossQl.append("WHERE ");

        		}

        		jbossQl.append("pr.glFunctionalCurrency.fcName=?" + (ctr+1) + " ");
        		obj[ctr] = (String)criteria.get("currency");
        		ctr++;

        	}



        	if (criteria.containsKey("approvalStatus")) {

        		if (!firstArgument) {

        			jbossQl.append("AND ");

        		} else {

        			firstArgument = false;
        			jbossQl.append("WHERE ");

        		}

        		String approvalStatus = (String)criteria.get("approvalStatus");

        		if (approvalStatus.equals("DRAFT")) {

        			jbossQl.append("pr.prApprovalStatus IS NULL AND pr.prReasonForRejection IS NULL ");

        		} else if (approvalStatus.equals("REJECTED")) {

        			jbossQl.append("pr.prReasonForRejection IS NOT NULL ");

        		} else {

        			jbossQl.append("pr.prApprovalStatus=?" + (ctr+1) + " ");
        			obj[ctr] = approvalStatus;
        			ctr++;

        		}

        	}

        	if (criteria.containsKey("posted")) {

        		if (!firstArgument) {

        			jbossQl.append("AND ");

        		} else {

        			firstArgument = false;
        			jbossQl.append("WHERE ");

        		}

        		jbossQl.append("pr.prPosted=?" + (ctr+1) + " ");

        		String posted = (String)criteria.get("posted");

        		if (posted.equals("YES")) {

        			obj[ctr] = new Byte(EJBCommon.TRUE);

        		} else {

        			obj[ctr] = new Byte(EJBCommon.FALSE);

        		}

        		ctr++;

        	}

        	if (criteria.containsKey("canvassApprovalStatus")) {

        		if (!firstArgument) {

        			jbossQl.append("AND ");

        		} else {

        			firstArgument = false;
        			jbossQl.append("WHERE ");

        		}

        		String canvassApprovalStatus = (String)criteria.get("canvassApprovalStatus");

        		if (canvassApprovalStatus.equals("DRAFT")) {

        			jbossQl.append("pr.prCanvassApprovalStatus IS NULL AND pr.prCanvassReasonForRejection IS NULL ");

        		} else if (canvassApprovalStatus.equals("REJECTED")) {

        			jbossQl.append("pr.prCanvassReasonForRejection IS NOT NULL ");

        		} else {

        			jbossQl.append("pr.prCanvassApprovalStatus=?" + (ctr+1) + " ");
        			obj[ctr] = canvassApprovalStatus;
        			ctr++;

        		}

        	}

        	if (criteria.containsKey("canvassPosted")) {

        		if (!firstArgument) {

        			jbossQl.append("AND ");

        		} else {

        			firstArgument = false;
        			jbossQl.append("WHERE ");

        		}

        		jbossQl.append("pr.prCanvassPosted=?" + (ctr+1) + " ");

        		String canvassPosted = (String)criteria.get("canvassPosted");

        		if (canvassPosted.equals("YES")) {

        			obj[ctr] = new Byte(EJBCommon.TRUE);

        		} else {

        			obj[ctr] = new Byte(EJBCommon.FALSE);

        		}

        		ctr++;

        	}

        	if (criteria.containsKey("documentNumberFrom")) {

        		if (!firstArgument) {

        			jbossQl.append("AND ");

        		} else {

        			firstArgument = false;
        			jbossQl.append("WHERE ");

        		}

        		jbossQl.append("pr.prNumber>=?" + (ctr+1) + " ");
        		obj[ctr] = (String)criteria.get("documentNumberFrom");
        		ctr++;

        	}

        	if (criteria.containsKey("documentNumberTo")) {

        		if (!firstArgument) {

        			jbossQl.append("AND ");

        		} else {

        			firstArgument = false;
        			jbossQl.append("WHERE ");

        		}

        		jbossQl.append("pr.prNumber<=?" + (ctr+1) + " ");
        		obj[ctr] = (String)criteria.get("documentNumberTo");
        		ctr++;

        	}

        	if (criteria.containsKey("dateFrom")) {

        		if (!firstArgument) {
        			jbossQl.append("AND ");
        		} else {
        			firstArgument = false;
        			jbossQl.append("WHERE ");
        		}
        		jbossQl.append("pr.prDate>=?" + (ctr+1) + " ");
        		obj[ctr] = (Date)criteria.get("dateFrom");
        		ctr++;
        	}

        	if (criteria.containsKey("dateTo")) {

        		if (!firstArgument) {
        			jbossQl.append("AND ");
        		} else {
        			firstArgument = false;
        			jbossQl.append("WHERE ");
        		}
        		jbossQl.append("pr.prDate<=?" + (ctr+1) + " ");
        		obj[ctr] = (Date)criteria.get("dateTo");
        		ctr++;

        	}

        	if (criteria.containsKey("nextRunDateFrom")) {

       	  	 if (!firstArgument) {
       	  	 	jbossQl.append("AND ");
       	  	 } else {
       	  	 	firstArgument = false;
       	  	 	jbossQl.append("WHERE ");
       	  	 }
       	  	 jbossQl.append("pr.prNextRunDate>=?" + (ctr+1) + " ");
       	  	 obj[ctr] = (Date)criteria.get("nextRunDateFrom");
       	  	 ctr++;
       	  }

       	  if (criteria.containsKey("nextRunDateTo")) {

       	  	 if (!firstArgument) {
       	  	 	jbossQl.append("AND ");
       	  	 } else {
       	  	 	firstArgument = false;
       	  	 	jbossQl.append("WHERE ");
       	  	 }
       	  	 jbossQl.append("pr.prNextRunDate<=?" + (ctr+1) + " ");
       	  	 obj[ctr] = (Date)criteria.get("nextRunDateTo");
       	  	 ctr++;

       	  }

        	if (!firstArgument) {

        		jbossQl.append("AND ");

        	} else {

        		firstArgument = false;
        		jbossQl.append("WHERE ");

        	}

        	jbossQl.append("pr.prAdBranch=" + AD_BRNCH + " ");

        	if (!firstArgument) {

        		jbossQl.append("AND ");

        	} else {

        		firstArgument = false;
        		jbossQl.append("WHERE ");

        	}

        	jbossQl.append("pr.prAdCompany=" + AD_CMPNY + " ");
        	System.out.println("jbossQl.toString" + jbossQl.toString());
        	Collection apPurchaseRequisitions = apPurchaseRequisitionHome.getPrByCriteria(jbossQl.toString(), obj);

        	if (apPurchaseRequisitions.size() == 0)
        		throw new GlobalNoRecordFoundException();

        	return new Integer(apPurchaseRequisitions.size());

        } catch (GlobalNoRecordFoundException ex) {

        	throw ex;

        } catch (Exception ex) {

        	ex.printStackTrace();
        	throw new EJBException(ex.getMessage());

        }

    }



    /**
     * @ejb:interface-method view-type="remote"
     **/
	public ArrayList generateApPo(Integer PR_CODE, String CRTD_BY, String BR_BRNCH_CODE, Integer AD_CMPNY) {

		Debug.print("ApFindPurchaseRequisitionControllerBean generateApPo");

		LocalApCanvassHome apCanvassHome = null;
		LocalApPurchaseRequisitionHome apPurchaseRequisitionHome = null;
		LocalApPurchaseOrderHome apPurchaseOrderHome = null;
		LocalApPurchaseOrderLineHome apPurchaseOrderLineHome = null;
		LocalAdDocumentSequenceAssignmentHome adDocumentSequenceAssignmentHome = null;
		LocalAdBranchDocumentSequenceAssignmentHome adBranchDocumentSequenceAssignmentHome = null;
		LocalGlFunctionalCurrencyHome glFunctionalCurrencyHome = null;
		LocalApTaxCodeHome apTaxCodeHome = null;
		LocalInvTagHome invTagHome = null;
		LocalAdBranchHome adBranchHome = null;

		LocalApPurchaseOrder apPurchaseOrder = null;

		//Initialize EJB Home

		try {

			apCanvassHome = (LocalApCanvassHome)EJBHomeFactory.
				lookUpLocalHome(LocalApCanvassHome.JNDI_NAME, LocalApCanvassHome.class);
			apPurchaseRequisitionHome = (LocalApPurchaseRequisitionHome)EJBHomeFactory.
				lookUpLocalHome(LocalApPurchaseRequisitionHome.JNDI_NAME, LocalApPurchaseRequisitionHome.class);
			apPurchaseOrderHome = (LocalApPurchaseOrderHome)EJBHomeFactory.
				lookUpLocalHome(LocalApPurchaseOrderHome.JNDI_NAME, LocalApPurchaseOrderHome.class);
			apPurchaseOrderLineHome = (LocalApPurchaseOrderLineHome)EJBHomeFactory.
				lookUpLocalHome(LocalApPurchaseOrderLineHome.JNDI_NAME, LocalApPurchaseOrderLineHome.class);
			adDocumentSequenceAssignmentHome = (LocalAdDocumentSequenceAssignmentHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdDocumentSequenceAssignmentHome.JNDI_NAME, LocalAdDocumentSequenceAssignmentHome.class);
			adBranchDocumentSequenceAssignmentHome = (LocalAdBranchDocumentSequenceAssignmentHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdBranchDocumentSequenceAssignmentHome.JNDI_NAME, LocalAdBranchDocumentSequenceAssignmentHome.class);
			glFunctionalCurrencyHome = (LocalGlFunctionalCurrencyHome)EJBHomeFactory.
				lookUpLocalHome(LocalGlFunctionalCurrencyHome.JNDI_NAME, LocalGlFunctionalCurrencyHome.class);
			apTaxCodeHome = (LocalApTaxCodeHome)EJBHomeFactory.
				lookUpLocalHome(LocalApTaxCodeHome.JNDI_NAME, LocalApTaxCodeHome.class);
			invTagHome = (LocalInvTagHome)EJBHomeFactory.
				lookUpLocalHome(LocalInvTagHome.JNDI_NAME, LocalInvTagHome.class);
			adBranchHome = (LocalAdBranchHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdBranchHome.JNDI_NAME, LocalAdBranchHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());
		}

		try {

			LocalAdBranch adBranch = adBranchHome.findByBrBranchCode(BR_BRNCH_CODE, AD_CMPNY);
			Integer AD_BRNCH = adBranch.getBrCode();

			LocalApPurchaseRequisition apPurchaseRequisition = null;

			try {

				apPurchaseRequisition = apPurchaseRequisitionHome.findByPrimaryKey(PR_CODE);

			} catch(FinderException ex) {


			}

			ArrayList poList = new ArrayList();

			Date CURR_DT = EJBCommon.getGcCurrentDateWoTime().getTime();

			Collection apGenPoLines = apCanvassHome.findByPrCodeAndCnvPo(PR_CODE, EJBCommon.TRUE, AD_CMPNY);

			Iterator i = apGenPoLines.iterator();

			String SPL_SPPLR_CODE = null;
			short PL_LN = 1;


			while(i.hasNext()) {

				LocalApCanvass apCanvass = (LocalApCanvass)i.next();

				LocalApSupplier apSupplier = apCanvass.getApSupplier();

				if(SPL_SPPLR_CODE == null || !apSupplier.getSplSupplierCode().equals(SPL_SPPLR_CODE)) {

					SPL_SPPLR_CODE = apSupplier.getSplSupplierCode();
					PL_LN = 1;

					String PO_NMBR = null;

					//generate document number

					LocalAdBranchDocumentSequenceAssignment adBranchDocumentSequenceAssignment = null;
		 			LocalAdDocumentSequenceAssignment adDocumentSequenceAssignment = null;

					try {

				        adDocumentSequenceAssignment = adDocumentSequenceAssignmentHome.findByDcName("AP PURCHASE ORDER", AD_CMPNY);

				    } catch(FinderException ex) { }

				    try {

	 					adBranchDocumentSequenceAssignment = adBranchDocumentSequenceAssignmentHome.findBdsByDsaCodeAndBrCode(adDocumentSequenceAssignment.getDsaCode(), AD_BRNCH, AD_CMPNY);

	 				} catch (FinderException ex) { }

					if (adDocumentSequenceAssignment.getAdDocumentSequence().getDsNumberingType() == 'A') {

						while (true) {

						    if (adBranchDocumentSequenceAssignment == null || adBranchDocumentSequenceAssignment.getBdsNextSequence() == null) {

								try {

									apPurchaseOrderHome.findByPoDocumentNumberAndBrCode(adDocumentSequenceAssignment.getDsaNextSequence(), AD_BRNCH, AD_CMPNY);
									adDocumentSequenceAssignment.setDsaNextSequence(EJBCommon.incrementStringNumber(adDocumentSequenceAssignment.getDsaNextSequence()));

								} catch (FinderException ex) {

									PO_NMBR = adDocumentSequenceAssignment.getDsaNextSequence();
									adDocumentSequenceAssignment.setDsaNextSequence(EJBCommon.incrementStringNumber(adDocumentSequenceAssignment.getDsaNextSequence()));
									break;

								}

						    } else {

						        try {

									apPurchaseOrderHome.findByPoDocumentNumberAndBrCode(adBranchDocumentSequenceAssignment.getBdsNextSequence(), AD_BRNCH, AD_CMPNY);
									adBranchDocumentSequenceAssignment.setBdsNextSequence(EJBCommon.incrementStringNumber(adBranchDocumentSequenceAssignment.getBdsNextSequence()));

								} catch (FinderException ex) {

									PO_NMBR = adBranchDocumentSequenceAssignment.getBdsNextSequence();
									adBranchDocumentSequenceAssignment.setBdsNextSequence(EJBCommon.incrementStringNumber(adBranchDocumentSequenceAssignment.getBdsNextSequence()));
									break;

								}

						    }

						}

					}

					//create new purchase order

					java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat("MM/dd/yyyy HH:mm:ss.SSSS");
		          	String PO_DESC = "GENERATED PO " + formatter.format(new java.util.Date()) + " > " + apPurchaseRequisition.getPrDescription();
					System.out.println(apPurchaseRequisition.getPrCode()+ "PR GENERATED = TRUE");
					apPurchaseRequisition.setPrGenerated(EJBCommon.TRUE);
					apPurchaseOrder = apPurchaseOrderHome.create(EJBCommon.FALSE, null, CURR_DT, apPurchaseRequisition.getPrDeliveryPeriod(), PO_NMBR, Integer.toString(apPurchaseRequisition.getPrCode()), null,
							PO_DESC, null, null, apPurchaseRequisition.getPrConversionDate(), apPurchaseRequisition.getPrConversionRate(), 0d , EJBCommon.FALSE, EJBCommon.FALSE, null, null,
							EJBCommon.FALSE, null, CRTD_BY, CURR_DT, CRTD_BY, CURR_DT, null, null, null, null,
							EJBCommon.FALSE,
							0d,0d,0d,0d,0d,
							AD_BRNCH, AD_CMPNY);

					/*.create(EJBCommon.FALSE, null, CURR_DT, apPurchaseRequisition.getPrDeliveryPeriod(), PO_NMBR, Integer.toString(apPurchaseRequisition.getPrCode()), null,
							PO_DESC, null, null, apPurchaseRequisition.getPrConversionDate(), apPurchaseRequisition.getPrConversionRate(), 0d , EJBCommon.FALSE, EJBCommon.FALSE, null, null,
							EJBCommon.FALSE, null, CRTD_BY, CURR_DT, CRTD_BY, CURR_DT, null, null, null, null,
							EJBCommon.FALSE, EJBCommon.FALSE, EJBCommon.FALSE,
							apPurchaseRequisition.getPrMisc1(),apPurchaseRequisition.getPrMisc2(),apPurchaseRequisition.getPrMisc3(),
							apPurchaseRequisition.getPrMisc4(),apPurchaseRequisition.getPrMisc5(),apPurchaseRequisition.getPrMisc6(),
							EJBCommon.FALSE, EJBCommon.FALSE, EJBCommon.FALSE,
							EJBCommon.FALSE, EJBCommon.FALSE, EJBCommon.FALSE, EJBCommon.FALSE, EJBCommon.FALSE,
							null, null, null, null, null, null, null, null, null,
							AD_BRNCH, AD_CMPNY);*/


					apPurchaseOrder.setPoApprovedRejectedBy(apPurchaseRequisition.getPrCanvassApprovedRejectedBy());
					apPurchaseOrder.setApSupplier(apSupplier);

					LocalAdPaymentTerm adPaymentTerm = apSupplier.getAdPaymentTerm();
					apPurchaseOrder.setAdPaymentTerm(adPaymentTerm);

					LocalGlFunctionalCurrency glFunctionalCurrency = apPurchaseRequisition.getGlFunctionalCurrency();
					apPurchaseOrder.setGlFunctionalCurrency(glFunctionalCurrency);

					LocalApTaxCode apTaxCode = apPurchaseRequisition.getApTaxCode();
					apPurchaseOrder.setApTaxCode(apTaxCode);

					poList.add(apPurchaseOrder.getPoCode());

				}

				//add purchase order line

				double PL_AMNT = EJBCommon.roundIt(apCanvass.getCnvQuantity() * apCanvass.getCnvUnitCost(), this.getGlFcPrecisionUnit(AD_CMPNY));

				LocalApPurchaseOrderLine apPurchaseOrderLine =
					apPurchaseOrderLineHome.create(PL_LN, apCanvass.getCnvQuantity(),
					apCanvass.getCnvUnitCost(), PL_AMNT , null, null, 0d, 0d, null, 0, 0, 0,0, 0, AD_CMPNY);

				apPurchaseOrderLine.setApPurchaseOrder(apPurchaseOrder);

				LocalInvUnitOfMeasure invUnitOfMeasure = apCanvass.getApPurchaseRequisitionLine().getInvUnitOfMeasure();
				apPurchaseOrderLine.setInvUnitOfMeasure(invUnitOfMeasure);

				LocalInvItemLocation invItemLocation = apCanvass.getApPurchaseRequisitionLine().getInvItemLocation();
				apPurchaseOrderLine.setInvItemLocation(invItemLocation);

				try{
      	  	    	System.out.println("aabot?");
      	  	    	//Iterator t = apPurchaseOrderLine.getInvTag().iterator();
	      	  	    Iterator t = apCanvass.getApPurchaseRequisitionLine().getInvTags().iterator();


	      	  	    System.out.println("umabot?");
	      	  	    while (t.hasNext()){
	      	  	    	LocalInvTag invPlTag = (LocalInvTag)t.next();

	      	  	    	LocalInvTag invTag = invTagHome.create(invPlTag.getTgPropertyCode(),
	      	  	    		invPlTag.getTgSerialNumber(),null,invPlTag.getTgExpiryDate(),
	      	  	    		invPlTag.getTgSpecs(), AD_CMPNY, invPlTag.getTgTransactionDate(),
	      	  	    		invPlTag.getTgType());

		      	  	    invTag.setApPurchaseOrderLine(apPurchaseOrderLine);

		      	  	    invTag.setAdUser(invPlTag.getAdUser());
		      	  	    System.out.println("ngcreate ba?");
	      	  	    }

      	  	    }catch(Exception ex){

      	  	    }

				PL_LN++;

			}

			return poList;

		} catch (Exception ex) {

			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());

		}

	}


	/**
	 * @ejb:interface-method view-type="remote"
	 **/
	public Integer saveApPoEntry(com.util.ApPurchaseOrderDetails details, String PYT_NM, String TC_NM, String FC_NM, String SPL_SPPLR_CODE, ArrayList plList, boolean isDraft, String BR_BRNCH_CODE, Integer AD_CMPNY) throws
	GlobalRecordAlreadyDeletedException,
	GlobalDocumentNumberNotUniqueException,
	GlobalConversionDateNotExistException,
	GlobalPaymentTermInvalidException,
	GlobalTransactionAlreadyApprovedException,
	GlobalTransactionAlreadyPendingException,
	GlobalTransactionAlreadyPostedException,
	GlobalTransactionAlreadyVoidException,
	GlobalInvItemLocationNotFoundException,
	GlobalNoApprovalRequesterFoundException,
	GlobalNoApprovalApproverFoundException,
	GlobalSupplierItemInvalidException {

		Debug.print("ApFindPurchaseRequisitionControllerBean saveApPoEntry");

		LocalApPurchaseOrderHome apPurchaseOrderHome = null;
		LocalApPurchaseOrderLineHome apPurchaseOrderLineHome = null;
		LocalAdCompanyHome adCompanyHome = null;
		LocalAdDocumentSequenceAssignmentHome adDocumentSequenceAssignmentHome = null;
		LocalApSupplierHome apSupplierHome = null;
		LocalAdPaymentTermHome adPaymentTermHome = null;
		LocalApTaxCodeHome apTaxCodeHome = null;
		LocalGlFunctionalCurrencyHome glFunctionalCurrencyHome = null;
		LocalGlFunctionalCurrencyRateHome glFunctionalCurrencyRateHome = null;
		LocalAdPreferenceHome adPreferenceHome = null;
		LocalInvItemLocationHome invItemLocationHome = null;
		LocalInvUnitOfMeasureHome invUnitOfMeasureHome = null;
		LocalAdApprovalHome adApprovalHome = null;
		LocalAdAmountLimitHome adAmountLimitHome = null;
		LocalAdApprovalUserHome adApprovalUserHome = null;
        LocalAdApprovalQueueHome adApprovalQueueHome = null;
        LocalAdBranchDocumentSequenceAssignmentHome adBranchDocumentSequenceAssignmentHome = null;
        LocalInvItemHome invItemHome = null;
        LocalApPurchaseRequisitionHome apPurchaseRequisitionHome = null;
        LocalInvTagHome invTagHome = null;
        LocalAdUserHome adUserHome = null;
        LocalAdBranchHome adBranchHome = null;

		LocalApPurchaseOrder apPurchaseOrder = null;

		// Initialize EJB Home

		try {

			apPurchaseOrderHome = (LocalApPurchaseOrderHome)EJBHomeFactory.
				lookUpLocalHome(LocalApPurchaseOrderHome.JNDI_NAME, LocalApPurchaseOrderHome.class);
			apPurchaseOrderLineHome = (LocalApPurchaseOrderLineHome)EJBHomeFactory.
				lookUpLocalHome(LocalApPurchaseOrderLineHome.JNDI_NAME, LocalApPurchaseOrderLineHome.class);
			adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
			adDocumentSequenceAssignmentHome = (LocalAdDocumentSequenceAssignmentHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdDocumentSequenceAssignmentHome.JNDI_NAME, LocalAdDocumentSequenceAssignmentHome.class);
			apSupplierHome = (LocalApSupplierHome)EJBHomeFactory.
				lookUpLocalHome(LocalApSupplierHome.JNDI_NAME, LocalApSupplierHome.class);
			adPaymentTermHome = (LocalAdPaymentTermHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdPaymentTermHome.JNDI_NAME, LocalAdPaymentTermHome.class);
			apTaxCodeHome = (LocalApTaxCodeHome)EJBHomeFactory.
				lookUpLocalHome(LocalApTaxCodeHome.JNDI_NAME, LocalApTaxCodeHome.class);
			glFunctionalCurrencyHome = (LocalGlFunctionalCurrencyHome)EJBHomeFactory.
				lookUpLocalHome(LocalGlFunctionalCurrencyHome.JNDI_NAME, LocalGlFunctionalCurrencyHome.class);
			glFunctionalCurrencyRateHome = (LocalGlFunctionalCurrencyRateHome)EJBHomeFactory.
				lookUpLocalHome(LocalGlFunctionalCurrencyRateHome.JNDI_NAME, LocalGlFunctionalCurrencyRateHome.class);
			adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);
			invItemLocationHome = (LocalInvItemLocationHome)EJBHomeFactory.
				lookUpLocalHome(LocalInvItemLocationHome.JNDI_NAME, LocalInvItemLocationHome.class);
			invUnitOfMeasureHome = (LocalInvUnitOfMeasureHome)EJBHomeFactory.
				lookUpLocalHome(LocalInvUnitOfMeasureHome.JNDI_NAME, LocalInvUnitOfMeasureHome.class);
			adApprovalHome = (LocalAdApprovalHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdApprovalHome.JNDI_NAME, LocalAdApprovalHome.class);
			adAmountLimitHome = (LocalAdAmountLimitHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdAmountLimitHome.JNDI_NAME, LocalAdAmountLimitHome.class);
			adApprovalUserHome = (LocalAdApprovalUserHome)EJBHomeFactory.
        		lookUpLocalHome(LocalAdApprovalUserHome.JNDI_NAME, LocalAdApprovalUserHome.class);
			adApprovalQueueHome = (LocalAdApprovalQueueHome)EJBHomeFactory.
        		lookUpLocalHome(LocalAdApprovalQueueHome.JNDI_NAME, LocalAdApprovalQueueHome.class);
			adBranchDocumentSequenceAssignmentHome = (LocalAdBranchDocumentSequenceAssignmentHome)EJBHomeFactory.
    			lookUpLocalHome(LocalAdBranchDocumentSequenceAssignmentHome.JNDI_NAME, LocalAdBranchDocumentSequenceAssignmentHome.class);
			invItemHome = (LocalInvItemHome)EJBHomeFactory.
			lookUpLocalHome(LocalInvItemHome.JNDI_NAME, LocalInvItemHome.class);
			apPurchaseRequisitionHome = (LocalApPurchaseRequisitionHome)EJBHomeFactory.
			lookUpLocalHome(LocalApPurchaseRequisitionHome.JNDI_NAME, LocalApPurchaseRequisitionHome.class);
			invTagHome = (LocalInvTagHome)EJBHomeFactory.
			lookUpLocalHome(LocalInvTagHome.JNDI_NAME, LocalInvTagHome.class);
			adUserHome = (LocalAdUserHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdUserHome.JNDI_NAME, LocalAdUserHome.class);
			adBranchHome = (LocalAdBranchHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdBranchHome.JNDI_NAME, LocalAdBranchHome.class);
		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}

		try {

			LocalAdBranch adBranch = adBranchHome.findByBrBranchCode(BR_BRNCH_CODE, AD_CMPNY);
			Integer AD_BRNCH = adBranch.getBrCode();

			// validate if purchase order is already deleted

			try {

				if (details.getPoCode() != null) {

					apPurchaseOrder = apPurchaseOrderHome.findByPrimaryKey(details.getPoCode());

				}

			} catch (FinderException ex) {

				throw new GlobalRecordAlreadyDeletedException();

			}

			// validate if purchase order is already posted, void, approved or pending

			if (details.getPoCode() != null && details.getPoVoid() == EJBCommon.FALSE) {

	        	if (apPurchaseOrder.getPoApprovalStatus() != null) {

	        		if (apPurchaseOrder.getPoApprovalStatus().equals("APPROVED") ||
	        			apPurchaseOrder.getPoApprovalStatus().equals("N/A")) {

	        		    throw new GlobalTransactionAlreadyApprovedException();

	        		} else if (apPurchaseOrder.getPoApprovalStatus().equals("PENDING")) {

	        			throw new GlobalTransactionAlreadyPendingException();

	        		}
	        	}

        		if (apPurchaseOrder.getPoPosted() == EJBCommon.TRUE) {

        			throw new GlobalTransactionAlreadyPostedException();

        		} else if (apPurchaseOrder.getPoVoid() == EJBCommon.TRUE) {

        			throw new GlobalTransactionAlreadyVoidException();

        		}
        	}

			// purchase order void

			if (details.getPoCode() != null && details.getPoVoid() == EJBCommon.TRUE) {

				apPurchaseOrder.setPoVoid(EJBCommon.TRUE);
				apPurchaseOrder.setPoLastModifiedBy(details.getPoLastModifiedBy());
				apPurchaseOrder.setPoDateLastModified(details.getPoDateLastModified());

				return apPurchaseOrder.getPoCode();

			}

			// validate if document number is unique document number is automatic then set next sequence

			try {

			    LocalAdBranchDocumentSequenceAssignment adBranchDocumentSequenceAssignment = null;
	 			LocalAdDocumentSequenceAssignment adDocumentSequenceAssignment = null;

				if (details.getPoCode() == null) {

				    try {

				        adDocumentSequenceAssignment = adDocumentSequenceAssignmentHome.findByDcName("AP PURCHASE ORDER", AD_CMPNY);

				    } catch(FinderException ex) { }

				    try {

	 					adBranchDocumentSequenceAssignment = adBranchDocumentSequenceAssignmentHome.findBdsByDsaCodeAndBrCode(adDocumentSequenceAssignment.getDsaCode(), AD_BRNCH, AD_CMPNY);

	 				} catch (FinderException ex) { }

					LocalApPurchaseOrder apExistingPurchaseOrder = null;

					try {

						apExistingPurchaseOrder = apPurchaseOrderHome.findByPoDocumentNumberAndBrCode(
								details.getPoDocumentNumber(), AD_BRNCH, AD_CMPNY);

					} catch (FinderException ex) {
					}

					if (apExistingPurchaseOrder != null) {

						throw new GlobalDocumentNumberNotUniqueException();

					}

					if (adDocumentSequenceAssignment.getAdDocumentSequence().getDsNumberingType() == 'A' &&
							(details.getPoDocumentNumber() == null || details.getPoDocumentNumber().trim().length() == 0)) {

						while (true) {

						    if (adBranchDocumentSequenceAssignment == null || adBranchDocumentSequenceAssignment.getBdsNextSequence() == null) {

								try {

									apPurchaseOrderHome.findByPoDocumentNumberAndBrCode(adDocumentSequenceAssignment.getDsaNextSequence(), AD_BRNCH, AD_CMPNY);
									adDocumentSequenceAssignment.setDsaNextSequence(EJBCommon.incrementStringNumber(adDocumentSequenceAssignment.getDsaNextSequence()));

								} catch (FinderException ex) {

									details.setPoDocumentNumber(adDocumentSequenceAssignment.getDsaNextSequence());
									adDocumentSequenceAssignment.setDsaNextSequence(EJBCommon.incrementStringNumber(adDocumentSequenceAssignment.getDsaNextSequence()));
									break;

								}

						    } else {

						        try {

									apPurchaseOrderHome.findByPoDocumentNumberAndBrCode(adBranchDocumentSequenceAssignment.getBdsNextSequence(), AD_BRNCH, AD_CMPNY);
									adBranchDocumentSequenceAssignment.setBdsNextSequence(EJBCommon.incrementStringNumber(adBranchDocumentSequenceAssignment.getBdsNextSequence()));

								} catch (FinderException ex) {

									details.setPoDocumentNumber(adBranchDocumentSequenceAssignment.getBdsNextSequence());
									adBranchDocumentSequenceAssignment.setBdsNextSequence(EJBCommon.incrementStringNumber(adBranchDocumentSequenceAssignment.getBdsNextSequence()));
									break;

								}

						    }

						}

					}

				} else {

				    LocalApPurchaseOrder apExistingPurchaseOrder = null;

					try {

						apExistingPurchaseOrder = apPurchaseOrderHome.findByPoDocumentNumberAndBrCode(
								details.getPoDocumentNumber(), AD_BRNCH, AD_CMPNY);

					} catch (FinderException ex) {
					}

					if (apExistingPurchaseOrder != null &&
							!apExistingPurchaseOrder.getPoCode().equals(details.getPoCode())) {

						throw new GlobalDocumentNumberNotUniqueException();

					}

					if (apPurchaseOrder.getPoDocumentNumber() != details.getPoDocumentNumber() &&
							(details.getPoDocumentNumber() == null || details.getPoDocumentNumber().trim().length() == 0)) {

						details.setPoDocumentNumber(apPurchaseOrder.getPoDocumentNumber());

					}

				}

			} catch (GlobalDocumentNumberNotUniqueException ex) {

	 			getSessionContext().setRollbackOnly();
	 			throw ex;

	 		} catch (Exception ex) {

	 			Debug.printStackTrace(ex);
	 			getSessionContext().setRollbackOnly();
	 			throw new EJBException(ex.getMessage());

	 		}

			// validate if conversion date exists

			try {

				if (details.getPoConversionDate() != null) {

					LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
					LocalGlFunctionalCurrency glValidateFunctionalCurrency = glFunctionalCurrencyHome.findByFcName(FC_NM, AD_CMPNY);

					if (!glValidateFunctionalCurrency.getFcName().equals("USD")) {

					LocalGlFunctionalCurrencyRate glFunctionalCurrencyRate = glFunctionalCurrencyRateHome.findByFcCodeAndDate(glValidateFunctionalCurrency.getFcCode(),
							details.getPoConversionDate(), AD_CMPNY);

					} else if (!adCompany.getGlFunctionalCurrency().getFcName().equals("USD")){

						LocalGlFunctionalCurrencyRate glFunctionalCurrencyRate =
							glFunctionalCurrencyRateHome.findByFcCodeAndDate(
								adCompany.getGlFunctionalCurrency().getFcCode(), details.getPoConversionDate(), AD_CMPNY);

					}
				}

			} catch (FinderException ex) {

				throw new GlobalConversionDateNotExistException();

			}

			// validate if payment term has at least one payment schedule

			if (adPaymentTermHome.findByPytName(PYT_NM, AD_CMPNY).getAdPaymentSchedules().isEmpty()) {

				throw new GlobalPaymentTermInvalidException();

			}

			boolean isRecalculate = true;

			// create purchase order

			if (details.getPoCode() == null) {

				apPurchaseOrder = apPurchaseOrderHome.create(EJBCommon.FALSE, null, details.getPoDate(), details.getPoDeliveryPeriod(), details.getPoDocumentNumber(),
						details.getPoReferenceNumber(), null, details.getPoDescription(), details.getPoBillTo(), details.getPoShipTo(),
						details.getPoConversionDate(), details.getPoConversionRate(), 0d, EJBCommon.FALSE, EJBCommon.FALSE, null, null,
						EJBCommon.FALSE, null, details.getPoCreatedBy(), details.getPoDateCreated(), details.getPoLastModifiedBy(),
						details.getPoDateLastModified(), null, null, null, null, EJBCommon.FALSE,
						0d,0d,0d,0d,0d,
						AD_BRNCH, AD_CMPNY);

				/*.create(EJBCommon.FALSE, null, details.getPoDate(), details.getPoDeliveryPeriod(), details.getPoDocumentNumber(),
						details.getPoReferenceNumber(), null, details.getPoDescription(), details.getPoBillTo(), details.getPoShipTo(),
						details.getPoConversionDate(), details.getPoConversionRate(), 0d, EJBCommon.FALSE, EJBCommon.FALSE, null, null,
						EJBCommon.FALSE, null, details.getPoCreatedBy(), details.getPoDateCreated(), details.getPoLastModifiedBy(),
						details.getPoDateLastModified(), null, null, null, null, EJBCommon.FALSE, EJBCommon.FALSE,
						EJBCommon.FALSE,
						EJBCommon.FALSE, EJBCommon.FALSE, EJBCommon.FALSE, EJBCommon.FALSE,
						EJBCommon.FALSE, EJBCommon.FALSE, EJBCommon.FALSE, EJBCommon.FALSE,
						null, null, null, null, null, null, null, null, null,
						AD_BRNCH, AD_CMPNY);*/

			} else {

				// check if critical fields are changed

				if (!apPurchaseOrder.getApTaxCode().getTcName().equals(TC_NM) ||
						!apPurchaseOrder.getApSupplier().getSplSupplierCode().equals(SPL_SPPLR_CODE) ||
						!apPurchaseOrder.getAdPaymentTerm().getPytName().equals(PYT_NM) ||
						plList.size() != apPurchaseOrder.getApPurchaseOrderLines().size()) {

					isRecalculate = true;

				} else if (plList.size() == apPurchaseOrder.getApPurchaseOrderLines().size()) {

					Iterator ilIter = apPurchaseOrder.getApPurchaseOrderLines().iterator();
					Iterator ilListIter = plList.iterator();

					while (ilIter.hasNext()) {

						LocalApPurchaseOrderLine apPurchaseOrderLine = (LocalApPurchaseOrderLine)ilIter.next();
						ApModPurchaseOrderLineDetails mdetails = (ApModPurchaseOrderLineDetails)ilListIter.next();

						if (!apPurchaseOrderLine.getInvItemLocation().getInvItem().getIiName().equals(mdetails.getPlIiName()) ||
								!apPurchaseOrderLine.getInvItemLocation().getInvItem().getIiDescription().equals(mdetails.getPlIiDescription()) ||
								!apPurchaseOrderLine.getInvItemLocation().getInvLocation().getLocName().equals(mdetails.getPlLocName()) ||
								!apPurchaseOrderLine.getInvUnitOfMeasure().getUomName().equals(mdetails.getPlUomName()) ||
								apPurchaseOrderLine.getPlQuantity() != mdetails.getPlQuantity() ||
								apPurchaseOrderLine.getPlUnitCost() != mdetails.getPlUnitCost() ||
								apPurchaseOrderLine.getPlTotalDiscount() != mdetails.getPlTotalDiscount()) {

							isRecalculate = true;
							break;

						}

				//		isRecalculate = false;

					}

				} else {

				//	isRecalculate = false;

				}

				apPurchaseOrder.setPoDate(details.getPoDate());
				apPurchaseOrder.setPoDeliveryPeriod(details.getPoDeliveryPeriod());
				apPurchaseOrder.setPoDocumentNumber(details.getPoDocumentNumber());
				apPurchaseOrder.setPoReferenceNumber(details.getPoReferenceNumber());
				apPurchaseOrder.setPoDescription(details.getPoDescription());
				apPurchaseOrder.setPoBillTo(details.getPoBillTo());
				apPurchaseOrder.setPoShipTo(details.getPoShipTo());
				apPurchaseOrder.setPoPrinted(details.getPoPrinted());
				apPurchaseOrder.setPoVoid(details.getPoVoid());
				apPurchaseOrder.setPoConversionDate(details.getPoConversionDate());
				apPurchaseOrder.setPoConversionRate(details.getPoConversionRate());
				apPurchaseOrder.setPoLastModifiedBy(details.getPoLastModifiedBy());
				apPurchaseOrder.setPoDateLastModified(details.getPoDateLastModified());



			}

			LocalApSupplier apSupplier = apSupplierHome.findBySplSupplierCode(SPL_SPPLR_CODE, AD_CMPNY);
			apPurchaseOrder.setApSupplier(apSupplier);

			LocalAdPaymentTerm adPaymentTerm = adPaymentTermHome.findByPytName(PYT_NM, AD_CMPNY);
			apPurchaseOrder.setAdPaymentTerm(adPaymentTerm);

			LocalApTaxCode apTaxCode = apTaxCodeHome.findByTcName(TC_NM, AD_CMPNY);
			apPurchaseOrder.setApTaxCode(apTaxCode);

			LocalGlFunctionalCurrency glFunctionalCurrency = glFunctionalCurrencyHome.findByFcName(FC_NM, AD_CMPNY);
			apPurchaseOrder.setGlFunctionalCurrency(glFunctionalCurrency);

			double ABS_TOTAL_AMOUNT = 0d;

			//	 Map Supplier and Item

			Iterator iter = plList.iterator();

			while (iter.hasNext()) {

				ApModPurchaseOrderLineDetails mPlDetails = (ApModPurchaseOrderLineDetails) iter.next();
				LocalInvItem invItem = invItemHome.findByIiName(mPlDetails.getPlIiName(), AD_CMPNY);

				if(invItem.getApSupplier() != null &&
						!invItem.getApSupplier().getSplSupplierCode().equals(
								apPurchaseOrder.getApSupplier().getSplSupplierCode())) {

					throw new GlobalSupplierItemInvalidException("" + mPlDetails.getPlLine());

				}

			}


			if (isRecalculate) {

				// remove all purchase order line items

				Collection apPurchaseOrderLines = apPurchaseOrder.getApPurchaseOrderLines();

				Iterator i = apPurchaseOrderLines.iterator();

				while (i.hasNext()) {

					LocalApPurchaseOrderLine apPurchaseOrderLine = (LocalApPurchaseOrderLine)i.next();

					i.remove();

					apPurchaseOrderLine.remove();

				}

				// add new purchase order line item

				i = plList.iterator();

				LocalInvItemLocation invItemLocation = null;

				while (i.hasNext()) {

					ApModPurchaseOrderLineDetails mPlDetails = (ApModPurchaseOrderLineDetails) i.next();

					LocalApPurchaseOrderLine apPurchaseOrderLine = apPurchaseOrderLineHome.create(
							mPlDetails.getPlLine(),	mPlDetails.getPlQuantity(), mPlDetails.getPlUnitCost(),
							mPlDetails.getPlAmount(), null, null, 0d, 0d, null, mPlDetails.getPlDiscount1(),
							mPlDetails.getPlDiscount2(), mPlDetails.getPlDiscount3(), mPlDetails.getPlDiscount4(),
							mPlDetails.getPlTotalDiscount(), AD_CMPNY);

					//apPurchaseOrder.addApPurchaseOrderLine(apPurchaseOrderLine);
					apPurchaseOrderLine.setApPurchaseOrder(apPurchaseOrder);

					try {

						invItemLocation = invItemLocationHome.findByLocNameAndIiName(
								mPlDetails.getPlLocName(), mPlDetails.getPlIiName(), AD_CMPNY);

					} catch (FinderException ex) {

						throw new GlobalInvItemLocationNotFoundException(String.valueOf(mPlDetails.getPlLine()));

					}

					ABS_TOTAL_AMOUNT += apPurchaseOrderLine.getPlAmount();

					//invItemLocation.addApPurchaseOrderLine(apPurchaseOrderLine);
					apPurchaseOrderLine.setInvItemLocation(invItemLocation);

					LocalInvUnitOfMeasure invUnitOfMeasure = invUnitOfMeasureHome.findByUomName(
							mPlDetails.getPlUomName(), AD_CMPNY);

					//invUnitOfMeasure.addApPurchaseOrderLine(apPurchaseOrderLine);
					apPurchaseOrderLine.setInvUnitOfMeasure(invUnitOfMeasure);

					//TODO: add new inv Tag
	      	  	    try{
	      	  	    	System.out.println("aabot?");
	      	  	    	//Iterator t = apPurchaseOrderLine.getInvTag().iterator();
		      	  	    Iterator t = mPlDetails.getPlTagList().iterator();

		      	  	    LocalInvTag invTag  = null;
		      	  	    System.out.println("umabot?");
		      	  	    while (t.hasNext()){
		      	  	    	InvModTagListDetails tgLstDetails = (InvModTagListDetails)t.next();
		      	  	    	System.out.println(tgLstDetails.getTgCustodian() + "<== custodian");
		      	  	    	System.out.println(tgLstDetails.getTgSpecs() + "<== specs");
		      	  	    	System.out.println(tgLstDetails.getTgPropertyCode() + "<== propertyCode");
		      	  	    	System.out.println(tgLstDetails.getTgExpiryDate() + "<== expiryDate");
		      	  	    	System.out.println(tgLstDetails.getTgSerialNumber() + "<== serial number");

		      	  	    	if (tgLstDetails.getTgCode()==null){
		      	  	    		System.out.println("ngcreate ba?");
			      	  	    	invTag = invTagHome.create(tgLstDetails.getTgPropertyCode(),
			      	  	    			tgLstDetails.getTgSerialNumber(),null,tgLstDetails.getTgExpiryDate(),
			      	  	    			tgLstDetails.getTgSpecs(), AD_CMPNY, tgLstDetails.getTgTransactionDate(),
			      	  	    			tgLstDetails.getTgType());

			      	  	    	invTag.setApPurchaseOrderLine(apPurchaseOrderLine);
			      	  	    	LocalAdUser adUser = null;
			      	  	    	try {
			      	  	    		adUser = adUserHome.findByUsrName(tgLstDetails.getTgCustodian(), AD_CMPNY);
			      	  	    	}catch(FinderException ex){

			      	  	    	}
			      	  	    	invTag.setAdUser(adUser);
			      	  	    	System.out.println("ngcreate ba?");
		      	  	    	}

		      	  	    }

	      	  	    }catch(Exception ex){

	      	  	    }

				}

			} else {


				Collection apPurchaseOrderLines = apPurchaseOrder.getApPurchaseOrderLines();

				Iterator i = apPurchaseOrderLines.iterator();

				while (i.hasNext()) {

					LocalApPurchaseOrderLine apPurchaseOrderLine = (LocalApPurchaseOrderLine)i.next();

					ABS_TOTAL_AMOUNT += apPurchaseOrderLine.getPlAmount();

				}
			}

			LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);

			// set purchase order approval status

			String PO_APPRVL_STATUS = null;

			if(!isDraft) {

				LocalAdApproval adApproval = adApprovalHome.findByAprAdCompany(AD_CMPNY);

				// check if ap voucher approval is enabled

				if (adApproval.getAprEnableApPurchaseOrder() == EJBCommon.FALSE) {

					PO_APPRVL_STATUS = "N/A";

				} else {

					// check if voucher is self approved

					LocalAdAmountLimit adAmountLimit = null;

					try {

						adAmountLimit = adAmountLimitHome.findByAdcTypeAndAuTypeAndUsrName("AP PURCHASE ORDER", "REQUESTER", details.getPoLastModifiedBy(), AD_CMPNY);

					} catch (FinderException ex) {

						throw new GlobalNoApprovalRequesterFoundException();

					}

					if (ABS_TOTAL_AMOUNT <= adAmountLimit.getCalAmountLimit()) {

						PO_APPRVL_STATUS = "N/A";

					} else {

						// for approval, create approval queue

						Collection adAmountLimits = adAmountLimitHome.findByAdcTypeAndGreaterThanCalAmountLimit("AP PURCHASE ORDER", adAmountLimit.getCalAmountLimit(), AD_CMPNY);

						if (adAmountLimits.isEmpty()) {

							Collection adApprovalUsers = adApprovalUserHome.findByAuTypeAndCalCode("APPROVER", adAmountLimit.getCalCode(), AD_CMPNY);

							if (adApprovalUsers.isEmpty()) {

								throw new GlobalNoApprovalApproverFoundException();

							}

							Iterator j = adApprovalUsers.iterator();

							while (j.hasNext()) {

								LocalAdApprovalUser adApprovalUser = (LocalAdApprovalUser)j.next();

								LocalAdApprovalQueue adApprovalQueue = adApprovalQueueHome.create(EJBCommon.TRUE, "AP PURCHASE ORDER", apPurchaseOrder.getPoCode(),
										apPurchaseOrder.getPoDocumentNumber(), apPurchaseOrder.getPoDate(), adAmountLimit.getCalAndOr(), adApprovalUser.getAuOr(), AD_BRNCH, AD_CMPNY);

								adApprovalUser.getAdUser().addAdApprovalQueue(adApprovalQueue);

							}

						} else {

							boolean isApprovalUsersFound = false;

							Iterator i = adAmountLimits.iterator();

							while (i.hasNext()) {

								LocalAdAmountLimit adNextAmountLimit = (LocalAdAmountLimit)i.next();

								if (ABS_TOTAL_AMOUNT <= adNextAmountLimit.getCalAmountLimit()) {

									Collection adApprovalUsers = adApprovalUserHome.findByAuTypeAndCalCode("APPROVER", adAmountLimit.getCalCode(), AD_CMPNY);

									Iterator j = adApprovalUsers.iterator();

									while (j.hasNext()) {

										isApprovalUsersFound = true;

										LocalAdApprovalUser adApprovalUser = (LocalAdApprovalUser)j.next();

										LocalAdApprovalQueue adApprovalQueue = adApprovalQueueHome.create(EJBCommon.TRUE, "AP PURCHASE ORDER", apPurchaseOrder.getPoCode(),
												apPurchaseOrder.getPoDocumentNumber(), apPurchaseOrder.getPoDate(), adAmountLimit.getCalAndOr(), adApprovalUser.getAuOr(), AD_BRNCH, AD_CMPNY);

										adApprovalUser.getAdUser().addAdApprovalQueue(adApprovalQueue);

									}

									break;

								} else if (!i.hasNext()) {

									Collection adApprovalUsers = adApprovalUserHome.findByAuTypeAndCalCode("APPROVER", adNextAmountLimit.getCalCode(), AD_CMPNY);

									Iterator j = adApprovalUsers.iterator();

									while (j.hasNext()) {

										isApprovalUsersFound = true;

										LocalAdApprovalUser adApprovalUser = (LocalAdApprovalUser)j.next();

										LocalAdApprovalQueue adApprovalQueue = adApprovalQueueHome.create(EJBCommon.TRUE, "AP PURCHASE ORDER", apPurchaseOrder.getPoCode(),
												apPurchaseOrder.getPoDocumentNumber(), apPurchaseOrder.getPoDate(), adNextAmountLimit.getCalAndOr(), adApprovalUser.getAuOr(), AD_BRNCH, AD_CMPNY);

										adApprovalUser.getAdUser().addAdApprovalQueue(adApprovalQueue);

									}

									break;

								}

								adAmountLimit = adNextAmountLimit;

							}

							if (!isApprovalUsersFound) {

								throw new GlobalNoApprovalApproverFoundException();

							}

						}

						PO_APPRVL_STATUS = "PENDING";

					}

				}

				apPurchaseOrder.setPoApprovalStatus(PO_APPRVL_STATUS);

				// set post purchase order

				if(PO_APPRVL_STATUS.equals("N/A")) {

					 apPurchaseOrder.setPoPosted(EJBCommon.TRUE);
					 apPurchaseOrder.setPoPosted(EJBCommon.TRUE);
					 apPurchaseOrder.setPoPostedBy(apPurchaseOrder.getPoLastModifiedBy());
					 apPurchaseOrder.setPoDatePosted(EJBCommon.getGcCurrentDateWoTime().getTime());

					 LocalApPurchaseRequisition apPurchaseRequisition = null;
			         try {
			        	 apPurchaseRequisition = apPurchaseRequisitionHome.findByPrimaryKey(new Integer(apPurchaseOrder.getPoReferenceNumber()));
			    	     Iterator ilListIter = plList.iterator();

			    	     while (ilListIter.hasNext()) {

			    	    	 ApModPurchaseOrderLineDetails mdetails = (ApModPurchaseOrderLineDetails)ilListIter.next();
			    	    	 this.setInvTbForItemForCurrentMonth(mdetails.getPlIiName(), apPurchaseRequisition.getPrCreatedBy(), apPurchaseRequisition.getPrDate(), mdetails.getPlQuantity(), AD_CMPNY);
						}
					 } catch (FinderException ex) {

					 }

				 }

			}

 	  	    return apPurchaseOrder.getPoCode();


		} catch (GlobalRecordAlreadyDeletedException ex) {

			getSessionContext().setRollbackOnly();
			throw ex;

		} catch (GlobalDocumentNumberNotUniqueException ex) {

			getSessionContext().setRollbackOnly();
			throw ex;

		} catch (GlobalConversionDateNotExistException ex) {

			getSessionContext().setRollbackOnly();
			throw ex;

		} catch (GlobalPaymentTermInvalidException ex) {

			getSessionContext().setRollbackOnly();
			throw ex;

		} catch (GlobalTransactionAlreadyApprovedException ex) {

			getSessionContext().setRollbackOnly();
			throw ex;

		} catch (GlobalTransactionAlreadyPendingException ex) {

			getSessionContext().setRollbackOnly();
			throw ex;

		} catch (GlobalTransactionAlreadyPostedException ex) {

			getSessionContext().setRollbackOnly();
			throw ex;

		} catch (GlobalTransactionAlreadyVoidException ex) {

			getSessionContext().setRollbackOnly();
			throw ex;

		} catch (GlobalInvItemLocationNotFoundException ex) {

			getSessionContext().setRollbackOnly();
			throw ex;

		} catch (GlobalNoApprovalRequesterFoundException ex) {

			getSessionContext().setRollbackOnly();
			throw ex;

		} catch (GlobalNoApprovalApproverFoundException ex) {

			getSessionContext().setRollbackOnly();
			throw ex;

		} catch (GlobalSupplierItemInvalidException ex) {

			getSessionContext().setRollbackOnly();
			throw ex;

		} catch (Exception ex) {

			Debug.printStackTrace(ex);
			getSessionContext().setRollbackOnly();
			throw new EJBException(ex.getMessage());

		}

	}

	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public ApModPurchaseOrderDetails getApPoByPoCode(Integer PO_CODE, Integer AD_CMPNY)
	throws GlobalNoRecordFoundException {

		Debug.print("ApFindPurchaseRequisitionControllerBean getApPoByPoCode");

		LocalApPurchaseOrderHome apPurchaseOrderHome = null;
		LocalAdCompanyHome adCompanyHome = null;
		LocalGenValueSetValueHome genValueSetValueHome = null;
		LocalGenSegmentHome genSegmentHome = null;


		// Initialize EJB Home

		try {

			apPurchaseOrderHome = (LocalApPurchaseOrderHome)EJBHomeFactory.
			lookUpLocalHome(LocalApPurchaseOrderHome.JNDI_NAME, LocalApPurchaseOrderHome.class);
			adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
			genValueSetValueHome = (LocalGenValueSetValueHome)EJBHomeFactory.
			lookUpLocalHome(LocalGenValueSetValueHome.JNDI_NAME, LocalGenValueSetValueHome.class);
			genSegmentHome = (LocalGenSegmentHome)EJBHomeFactory.
			lookUpLocalHome(LocalGenSegmentHome.JNDI_NAME, LocalGenSegmentHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}

		try {

			LocalApPurchaseOrder apPurchaseOrder = null;


			try {

				apPurchaseOrder = apPurchaseOrderHome.findByPrimaryKey(PO_CODE);

			} catch (FinderException ex) {

				throw new GlobalNoRecordFoundException();

			}

			ArrayList list = new ArrayList();

			// get purchase order lines if any

			Collection apPurchaseOrderLines = apPurchaseOrder.getApPurchaseOrderLines();

			Iterator i = apPurchaseOrderLines.iterator();

			while (i.hasNext()) {

				LocalApPurchaseOrderLine apPurchaseOrderLine = (LocalApPurchaseOrderLine) i.next();

				ApModPurchaseOrderLineDetails plDetails = new ApModPurchaseOrderLineDetails();

				plDetails.setPlCode(apPurchaseOrderLine.getPlCode());
				plDetails.setPlLine(apPurchaseOrderLine.getPlLine());
				plDetails.setPlIiName(apPurchaseOrderLine.getInvItemLocation().getInvItem().getIiName());
				plDetails.setPlLocName(apPurchaseOrderLine.getInvItemLocation().getInvLocation().getLocName());
				plDetails.setPlQuantity(apPurchaseOrderLine.getPlQuantity());
				plDetails.setPlUomName(apPurchaseOrderLine.getInvUnitOfMeasure().getUomName());
				plDetails.setPlUnitCost(apPurchaseOrderLine.getPlUnitCost());
				plDetails.setPlIiDescription(apPurchaseOrderLine.getInvItemLocation().getInvItem().getIiDescription());
    			plDetails.setPlDiscount1(apPurchaseOrderLine.getPlDiscount1());
    			plDetails.setPlDiscount2(apPurchaseOrderLine.getPlDiscount2());
    			plDetails.setPlDiscount3(apPurchaseOrderLine.getPlDiscount3());
    			plDetails.setPlDiscount4(apPurchaseOrderLine.getPlDiscount4());
    			plDetails.setPlTotalDiscount(apPurchaseOrderLine.getPlTotalDiscount());
    			plDetails.setPlAmount(apPurchaseOrderLine.getPlAmount());

    			ArrayList tagList = new ArrayList();
				Collection invTags = apPurchaseOrderLine.getInvTags();
    			Iterator x = invTags.iterator();
    			while (x.hasNext()) {
    				LocalInvTag invTag = (LocalInvTag) x.next();
    				InvModTagListDetails tgLstDetails = new InvModTagListDetails();
    				tgLstDetails.setTgPropertyCode(invTag.getTgPropertyCode());
    				tgLstDetails.setTgSpecs(invTag.getTgSpecs());
    				tgLstDetails.setTgExpiryDate(invTag.getTgExpiryDate());
    				tgLstDetails.setTgSerialNumber(invTag.getTgSerialNumber());
    				try{

    					tgLstDetails.setTgCustodian(invTag.getAdUser().getUsrName());
    				}
    				catch(Exception ex){
    					tgLstDetails.setTgCustodian("");
    				}

    				tagList.add(tgLstDetails);

    				System.out.println(tgLstDetails.getTgPropertyCode() + "<== property code inside controllerbean getApPoByPoCode");
    				System.out.println(tgLstDetails.getTgSpecs() + "<== specs inside controllerbean getApPoByPoCode");
    				System.out.println(tagList+ "<== taglist inside controllerbean getApPoByPoCode");

    			}

    			plDetails.setPlTagList(tagList);

				list.add(plDetails);

			}

			ApModPurchaseOrderDetails mPoDetails = new ApModPurchaseOrderDetails();

			mPoDetails.setPoCode(apPurchaseOrder.getPoCode());
			mPoDetails.setPoDate(apPurchaseOrder.getPoDate());
			mPoDetails.setPoDocumentNumber(apPurchaseOrder.getPoDocumentNumber());
			mPoDetails.setPoReferenceNumber(apPurchaseOrder.getPoReferenceNumber());
			mPoDetails.setPoDescription(apPurchaseOrder.getPoDescription());
			mPoDetails.setPoVoid(apPurchaseOrder.getPoVoid());
			mPoDetails.setPoBillTo(apPurchaseOrder.getPoBillTo());
			mPoDetails.setPoShipTo(apPurchaseOrder.getPoShipTo());
			mPoDetails.setPoConversionDate(apPurchaseOrder.getPoConversionDate());
			mPoDetails.setPoConversionRate(apPurchaseOrder.getPoConversionRate());
			mPoDetails.setPoApprovalStatus(apPurchaseOrder.getPoApprovalStatus());
			mPoDetails.setPoPosted(apPurchaseOrder.getPoPosted());
			mPoDetails.setPoCreatedBy(apPurchaseOrder.getPoCreatedBy());
			mPoDetails.setPoDateCreated(apPurchaseOrder.getPoDateCreated());
			mPoDetails.setPoLastModifiedBy(apPurchaseOrder.getPoLastModifiedBy());
			mPoDetails.setPoDateLastModified(apPurchaseOrder.getPoDateLastModified());
			mPoDetails.setPoApprovedRejectedBy(apPurchaseOrder.getPoApprovedRejectedBy());
			mPoDetails.setPoDateApprovedRejected(apPurchaseOrder.getPoDateApprovedRejected());
			mPoDetails.setPoPostedBy(apPurchaseOrder.getPoPostedBy());
			mPoDetails.setPoDatePosted(apPurchaseOrder.getPoDatePosted());
			mPoDetails.setPoReasonForRejection(apPurchaseOrder.getPoReasonForRejection());
			mPoDetails.setPoSplSupplierCode(apPurchaseOrder.getApSupplier().getSplSupplierCode());
			mPoDetails.setPoPytName(apPurchaseOrder.getAdPaymentTerm().getPytName());
			mPoDetails.setPoTcName(apPurchaseOrder.getApTaxCode().getTcName());
        	mPoDetails.setPoTcType(apPurchaseOrder.getApTaxCode().getTcType());
        	mPoDetails.setPoTcRate(apPurchaseOrder.getApTaxCode().getTcRate());
			mPoDetails.setPoFcName(apPurchaseOrder.getGlFunctionalCurrency().getFcName());
			mPoDetails.setPoPrinted(EJBCommon.FALSE);
			mPoDetails.setPoSplName(apPurchaseOrder.getApSupplier().getSplName());

			mPoDetails.setPoPlList(list);

			return mPoDetails;

		} catch (GlobalNoRecordFoundException ex) {

			throw ex;

		} catch (Exception ex) {

			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());

		}

	}




	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public ApModPurchaseRequisitionDetails getApPrByPrCode(Integer PR_CODE, Integer AD_CMPNY)
	throws GlobalNoRecordFoundException {

		Debug.print("ApFindPurchaseRequisitionControllerBean getApPrByPrCode");

		LocalApPurchaseRequisitionHome apPurchaseRequisitionHome = null;
		LocalAdCompanyHome adCompanyHome = null;
		LocalGenValueSetValueHome genValueSetValueHome = null;
		LocalGenSegmentHome genSegmentHome = null;

		// Initialize EJB Home

		try {

			apPurchaseRequisitionHome = (LocalApPurchaseRequisitionHome)EJBHomeFactory.
			lookUpLocalHome(LocalApPurchaseRequisitionHome.JNDI_NAME, LocalApPurchaseRequisitionHome.class);
			adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
			genValueSetValueHome = (LocalGenValueSetValueHome)EJBHomeFactory.
			lookUpLocalHome(LocalGenValueSetValueHome.JNDI_NAME, LocalGenValueSetValueHome.class);
			genSegmentHome = (LocalGenSegmentHome)EJBHomeFactory.
			lookUpLocalHome(LocalGenSegmentHome.JNDI_NAME, LocalGenSegmentHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}

		try {

			LocalApPurchaseRequisition apPurchaseRequisition = null;


			try {

				apPurchaseRequisition = apPurchaseRequisitionHome.findByPrimaryKey(PR_CODE);

			} catch (FinderException ex) {

				throw new GlobalNoRecordFoundException();

			}

			ArrayList list = new ArrayList();

			// get purchase order lines if any

			Collection apPurchaseRequisitionLines = apPurchaseRequisition.getApPurchaseRequisitionLines();
			System.out.println("SAMPLE"+apPurchaseRequisitionLines.size());
			Iterator i = apPurchaseRequisitionLines.iterator();

			while (i.hasNext()) {

				LocalApPurchaseRequisitionLine apPurchaseRequisitionLine = (LocalApPurchaseRequisitionLine) i.next();

				ApModPurchaseRequisitionLineDetails plDetails = new ApModPurchaseRequisitionLineDetails();

				plDetails.setPrlCode(apPurchaseRequisitionLine.getPrlCode());
				plDetails.setPrlLine(apPurchaseRequisitionLine.getPrlLine());
				plDetails.setPrlIlIiName(apPurchaseRequisitionLine.getInvItemLocation().getInvItem().getIiName());
				plDetails.setPrlIlLocName(apPurchaseRequisitionLine.getInvItemLocation().getInvLocation().getLocName());
				plDetails.setPrlQuantity(apPurchaseRequisitionLine.getPrlQuantity());
				plDetails.setPrlUomName(apPurchaseRequisitionLine.getInvUnitOfMeasure().getUomName());
				plDetails.setPrlAmount(apPurchaseRequisitionLine.getPrlAmount());



				list.add(plDetails);

			}

			ApModPurchaseRequisitionDetails mPrDetails = new ApModPurchaseRequisitionDetails();


			mPrDetails.setPrPrlList(list);

			return mPrDetails;

		} catch (GlobalNoRecordFoundException ex) {

			throw ex;

		} catch (Exception ex) {

			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());

		}

	}









	/**
	    * @ejb:interface-method view-type="remote"
	    **/
	public Integer generateApPr(ArrayList prSelectedList, Integer AD_BRNCH, String AD_USR, Integer AD_CMPNY) {

		Debug.print("ApFindPurchaseRequisitionControllerBean generateApPr");

		LocalApPurchaseRequisitionHome apPurchaseRequisitionHome = null;
		LocalApPurchaseRequisitionLineHome apPurchaseRequisitionLineHome = null;
		LocalInvItemLocationHome invItemLocationHome = null;
		LocalApTaxCodeHome apTaxCodeHome = null;
		LocalGlFunctionalCurrencyHome glFunctionalCurrencyHome = null;
		LocalAdBranchDocumentSequenceAssignmentHome adBranchDocumentSequenceAssignmentHome = null;
		LocalAdDocumentSequenceAssignmentHome adDocumentSequenceAssignmentHome = null;
		LocalAdPreferenceHome adPreferenceHome = null;
		LocalApPurchaseOrderLineHome apPurchaseOrderLineHome = null;
		LocalApCanvassHome apCanvassHome = null;
		LocalAdBranchItemLocationHome adBranchItemLocationHome = null;
		LocalApSupplierHome apSupplierHome = null;

		//Initialize EJB Home

		try {

			apPurchaseRequisitionHome = (LocalApPurchaseRequisitionHome)EJBHomeFactory.
				lookUpLocalHome(LocalApPurchaseRequisitionHome.JNDI_NAME, LocalApPurchaseRequisitionHome.class);
			apPurchaseRequisitionLineHome = (LocalApPurchaseRequisitionLineHome)EJBHomeFactory.
				lookUpLocalHome(LocalApPurchaseRequisitionLineHome.JNDI_NAME, LocalApPurchaseRequisitionLineHome.class);
			invItemLocationHome = (LocalInvItemLocationHome)EJBHomeFactory.
				lookUpLocalHome(LocalInvItemLocationHome.JNDI_NAME, LocalInvItemLocationHome.class);
			apTaxCodeHome = (LocalApTaxCodeHome)EJBHomeFactory.
				lookUpLocalHome(LocalApTaxCodeHome.JNDI_NAME, LocalApTaxCodeHome.class);
			glFunctionalCurrencyHome = (LocalGlFunctionalCurrencyHome)EJBHomeFactory.
				lookUpLocalHome(LocalGlFunctionalCurrencyHome.JNDI_NAME, LocalGlFunctionalCurrencyHome.class);
			adBranchDocumentSequenceAssignmentHome = (LocalAdBranchDocumentSequenceAssignmentHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdBranchDocumentSequenceAssignmentHome.JNDI_NAME, LocalAdBranchDocumentSequenceAssignmentHome.class);
			adDocumentSequenceAssignmentHome = (LocalAdDocumentSequenceAssignmentHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdDocumentSequenceAssignmentHome.JNDI_NAME, LocalAdDocumentSequenceAssignmentHome.class);
			adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);
			apPurchaseOrderLineHome = (LocalApPurchaseOrderLineHome)EJBHomeFactory.
				lookUpLocalHome(LocalApPurchaseOrderLineHome.JNDI_NAME, LocalApPurchaseOrderLineHome.class);
			apCanvassHome = (LocalApCanvassHome)EJBHomeFactory.
				lookUpLocalHome(LocalApCanvassHome.JNDI_NAME, LocalApCanvassHome.class);
			adBranchItemLocationHome = (LocalAdBranchItemLocationHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdBranchItemLocationHome.JNDI_NAME, LocalAdBranchItemLocationHome.class);
			apSupplierHome = (LocalApSupplierHome)EJBHomeFactory.
					lookUpLocalHome(LocalApSupplierHome.JNDI_NAME, LocalApSupplierHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}

		try {

	        java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat("MM/dd/yyyy HH:mm:ss.SSSS");

	        LocalApPurchaseRequisition apPurchaseRequisition = null;
			Iterator i = prSelectedList.iterator();



			Double qty = 0d;
			while (i.hasNext()) {


				// validate if document number is unique document number is automatic then set next sequence

		    	LocalAdBranchDocumentSequenceAssignment adBranchDocumentSequenceAssignment = null;
	 			LocalAdDocumentSequenceAssignment adDocumentSequenceAssignment = null;
	 			String prNumber = null;

	 			try {

	 				adDocumentSequenceAssignment = adDocumentSequenceAssignmentHome.findByDcName("AP PURCHASE REQUISITION", AD_CMPNY);

	 			} catch (FinderException ex) {

	 			}

	 			try {

	 				adBranchDocumentSequenceAssignment = adBranchDocumentSequenceAssignmentHome.findBdsByDsaCodeAndBrCode(adDocumentSequenceAssignment.getDsaCode(), AD_BRNCH, AD_CMPNY);

	 			} catch (FinderException ex) {

	 			}

		        if (adDocumentSequenceAssignment.getAdDocumentSequence().getDsNumberingType() == 'A') {

		            while (true) {

		            	if (adBranchDocumentSequenceAssignment == null || adBranchDocumentSequenceAssignment.getBdsNextSequence() == null) {

		            		try {

			            		apPurchaseRequisitionHome.findByPrNumberAndBrCode(adDocumentSequenceAssignment.getDsaNextSequence(), AD_BRNCH, AD_CMPNY);
			            		adDocumentSequenceAssignment.setDsaNextSequence(EJBCommon.incrementStringNumber(adDocumentSequenceAssignment.getDsaNextSequence()));

			            	} catch (FinderException ex) {

			            		prNumber = adDocumentSequenceAssignment.getDsaNextSequence();
			            		adDocumentSequenceAssignment.setDsaNextSequence(EJBCommon.incrementStringNumber(adDocumentSequenceAssignment.getDsaNextSequence()));
					            break;

			            	}

		            	} else {

		            		try {

		            			apPurchaseRequisitionHome.findByPrNumberAndBrCode(adBranchDocumentSequenceAssignment.getBdsNextSequence(), AD_BRNCH, AD_CMPNY);
			            		adBranchDocumentSequenceAssignment.setBdsNextSequence(EJBCommon.incrementStringNumber(adBranchDocumentSequenceAssignment.getBdsNextSequence()));

			            	} catch (FinderException ex) {

			            		prNumber = adBranchDocumentSequenceAssignment.getBdsNextSequence();
			            		adBranchDocumentSequenceAssignment.setBdsNextSequence(EJBCommon.incrementStringNumber(adBranchDocumentSequenceAssignment.getBdsNextSequence()));
					            break;

			            	}

		            	}

		            }

		        }


		        LocalApPurchaseRequisition apForConPurchaseRequisition = apPurchaseRequisitionHome.findByPrimaryKey((Integer)i.next());

				//create purchase requisition
		        apPurchaseRequisition = apPurchaseRequisitionHome.create("GENERATED PR " + formatter.format(new java.util.Date()),
		        		prNumber, EJBCommon.getGcCurrentDateWoTime().getTime(), apForConPurchaseRequisition.getPrDeliveryPeriod(),  null ,
		        		apForConPurchaseRequisition.getPrConversionDate(), apForConPurchaseRequisition.getPrConversionRate(),
		        		null, EJBCommon.FALSE, EJBCommon.FALSE, null, EJBCommon.FALSE, EJBCommon.FALSE, null,null,
						AD_USR, EJBCommon.getGcCurrentDateWoTime().getTime(), AD_USR, new java.util.Date(),
						AD_USR, EJBCommon.getGcCurrentDateWoTime().getTime(), null, null,
						null, null, null, null,
						"REGULAR","Inventoriable",
						null,null,null,null,
						AD_BRNCH, AD_CMPNY);

		        LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);

				LocalApTaxCode apTaxCode = apTaxCodeHome.findByTcName(adPreference.getPrfApDefaultPrTax(), AD_CMPNY);
				apPurchaseRequisition.setApTaxCode(apTaxCode);

				LocalGlFunctionalCurrency glFunctionalCurrency = glFunctionalCurrencyHome.findByFcName(adPreference.getPrfApDefaultPrCurrency(), AD_CMPNY);
				apPurchaseRequisition.setGlFunctionalCurrency(glFunctionalCurrency);


				Collection apPurchaseRequisitionLines = apForConPurchaseRequisition.getApPurchaseRequisitionLines();

				Iterator j = apPurchaseRequisitionLines.iterator();

				short ctr=0;
				while (j.hasNext()) {

					LocalApPurchaseRequisitionLine apPurchaseRequisitionLine = (LocalApPurchaseRequisitionLine)j.next();

						LocalApPurchaseRequisitionLine apPurchaseRequisitionLineNew = apPurchaseRequisitionLineHome.create(++ctr, apPurchaseRequisitionLine.getPrlQuantity(), apPurchaseRequisitionLine.getPrlAmount(), AD_CMPNY);
						apPurchaseRequisition.addApPurchaseRequisitionLine(apPurchaseRequisitionLineNew);
						LocalInvItemLocation invItemLocation = invItemLocationHome.findByIiNameAndLocName(apPurchaseRequisitionLine.getInvItemLocation().getInvItem().getIiName(), apPurchaseRequisitionLine.getInvItemLocation().getInvLocation().getLocName(), AD_CMPNY);
						invItemLocation.addApPurchaseRequisitionLine(apPurchaseRequisitionLineNew);
						apPurchaseRequisitionLineNew.setPrlQuantity(apPurchaseRequisitionLine.getPrlQuantity());
						apPurchaseRequisitionLineNew.setPrlAmount(invItemLocation.getInvItem().getIiUnitCost());

						LocalInvUnitOfMeasure invUnitOfMeasure = invItemLocation.getInvItem().getInvUnitOfMeasure();
						invUnitOfMeasure.addApPurchaseRequisitionLine(apPurchaseRequisitionLineNew);

						Collection apCanvasses = apPurchaseRequisitionLine.getApCanvasses();
						Iterator k = apCanvasses.iterator();
						System.out.println("apCanvasses----->"+apCanvasses.size());
						double PRL_AMNT = 0d;
						double PRL_QTTY = 0d;

						while(k.hasNext()){
							LocalApCanvass apCanvass = (LocalApCanvass)k.next();

									LocalApCanvass apCanvassNew = apCanvassHome.create(apCanvass.getCnvLine(), apCanvass.getCnvRemarks(),
											apCanvass.getCnvQuantity(), apCanvass.getCnvUnitCost(),
									EJBCommon.roundIt(apCanvass.getCnvQuantity() * apCanvass.getCnvUnitCost(), this.getGlFcPrecisionUnit(AD_CMPNY)),
									apCanvass.getCnvPo(), AD_CMPNY);


									apCanvasses = apPurchaseRequisitionLine.getApCanvasses();

									LocalApSupplier apSupplier = apSupplierHome.findBySplSupplierCode(apCanvass.getApSupplier().getSplSupplierCode(), AD_CMPNY);

									//if(this.hasSupplier(apPurchaseRequisitionLine, apSupplier.getSplSupplierCode())) {

									//	throw new GlobalRecordAlreadyExistException(apSupplier.getSplSupplierCode());

									//}
									System.out.println(apSupplier.getSplSupplierCode());
									apSupplier.addApCanvass(apCanvassNew);
									apPurchaseRequisitionLineNew.addApCanvass(apCanvassNew);
									if(apCanvass.getCnvPo() == 1){
										PRL_AMNT += apCanvass.getCnvAmount();

										PRL_QTTY += apCanvass.getCnvQuantity();
								    }

						}
						System.out.println("PRL_QTTY-----"+PRL_QTTY);
						//apPurchaseRequisitionLine.setPrlAmount(PRL_AMNT);
						//apPurchaseRequisitionLine.setPrlQuantity(PRL_QTTY);


				}

				// adjust recurring pr date.
				GregorianCalendar gc = new GregorianCalendar();
			     gc.setTime(apForConPurchaseRequisition.getPrNextRunDate());

			     if (apForConPurchaseRequisition.getPrSchedule().equals("DAILY")) {

			     	gc.add(Calendar.DATE, 1);

			     } else if (apForConPurchaseRequisition.getPrSchedule().equals("WEEKLY")) {

			     	gc.add(Calendar.DATE, 7);

			     } else if (apForConPurchaseRequisition.getPrSchedule().equals("SEMI MONTHLY")) {

			     	gc.add(Calendar.DATE, 15);

			     } else if (apForConPurchaseRequisition.getPrSchedule().equals("MONTHLY")) {

			     	gc.add(Calendar.MONTH, 1);

			     } else if (apForConPurchaseRequisition.getPrSchedule().equals("QUARTERLY")) {

			     	gc.add(Calendar.MONTH, 3);

			     } else if (apForConPurchaseRequisition.getPrSchedule().equals("SEMI ANNUALLY")) {

			     	gc.add(Calendar.MONTH, 6);

			     } else if (apForConPurchaseRequisition.getPrSchedule().equals("ANNUALLY")) {

			     	gc.add(Calendar.YEAR, 1);

			     }


			     apForConPurchaseRequisition.setPrLastRunDate(EJBCommon.getGcCurrentDateWoTime().getTime());
			     apForConPurchaseRequisition.setPrNextRunDate(gc.getTime());

			}



			return apPurchaseRequisition.getPrCode();

		} catch (Exception ex) {

			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());

		}

	}






	/**
	    * @ejb:interface-method view-type="remote"
	    **/
	public Integer consolidateApPr(ArrayList prSelectedList, Integer AD_BRNCH, String AD_USR, Integer AD_CMPNY) {

		Debug.print("ApFindPurchaseRequisitionControllerBean consolidateApPr");

		LocalApPurchaseRequisitionHome apPurchaseRequisitionHome = null;
		LocalApPurchaseRequisitionLineHome apPurchaseRequisitionLineHome = null;
		LocalInvItemLocationHome invItemLocationHome = null;
		LocalApTaxCodeHome apTaxCodeHome = null;
		LocalGlFunctionalCurrencyHome glFunctionalCurrencyHome = null;
		LocalAdBranchDocumentSequenceAssignmentHome adBranchDocumentSequenceAssignmentHome = null;
		LocalAdDocumentSequenceAssignmentHome adDocumentSequenceAssignmentHome = null;
		LocalAdPreferenceHome adPreferenceHome = null;
		LocalApPurchaseOrderLineHome apPurchaseOrderLineHome = null;
		LocalApCanvassHome apCanvassHome = null;
		LocalAdBranchItemLocationHome adBranchItemLocationHome = null;

		//Initialize EJB Home

		try {

			apPurchaseRequisitionHome = (LocalApPurchaseRequisitionHome)EJBHomeFactory.
				lookUpLocalHome(LocalApPurchaseRequisitionHome.JNDI_NAME, LocalApPurchaseRequisitionHome.class);
			apPurchaseRequisitionLineHome = (LocalApPurchaseRequisitionLineHome)EJBHomeFactory.
				lookUpLocalHome(LocalApPurchaseRequisitionLineHome.JNDI_NAME, LocalApPurchaseRequisitionLineHome.class);
			invItemLocationHome = (LocalInvItemLocationHome)EJBHomeFactory.
				lookUpLocalHome(LocalInvItemLocationHome.JNDI_NAME, LocalInvItemLocationHome.class);
			apTaxCodeHome = (LocalApTaxCodeHome)EJBHomeFactory.
				lookUpLocalHome(LocalApTaxCodeHome.JNDI_NAME, LocalApTaxCodeHome.class);
			glFunctionalCurrencyHome = (LocalGlFunctionalCurrencyHome)EJBHomeFactory.
				lookUpLocalHome(LocalGlFunctionalCurrencyHome.JNDI_NAME, LocalGlFunctionalCurrencyHome.class);
			adBranchDocumentSequenceAssignmentHome = (LocalAdBranchDocumentSequenceAssignmentHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdBranchDocumentSequenceAssignmentHome.JNDI_NAME, LocalAdBranchDocumentSequenceAssignmentHome.class);
			adDocumentSequenceAssignmentHome = (LocalAdDocumentSequenceAssignmentHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdDocumentSequenceAssignmentHome.JNDI_NAME, LocalAdDocumentSequenceAssignmentHome.class);
			adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);
			apPurchaseOrderLineHome = (LocalApPurchaseOrderLineHome)EJBHomeFactory.
				lookUpLocalHome(LocalApPurchaseOrderLineHome.JNDI_NAME, LocalApPurchaseOrderLineHome.class);
			apCanvassHome = (LocalApCanvassHome)EJBHomeFactory.
				lookUpLocalHome(LocalApCanvassHome.JNDI_NAME, LocalApCanvassHome.class);
			adBranchItemLocationHome = (LocalAdBranchItemLocationHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdBranchItemLocationHome.JNDI_NAME, LocalAdBranchItemLocationHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}

		try {

			// validate if document number is unique document number is automatic then set next sequence

	    	LocalAdBranchDocumentSequenceAssignment adBranchDocumentSequenceAssignment = null;
 			LocalAdDocumentSequenceAssignment adDocumentSequenceAssignment = null;
 			String prNumber = null;

 			try {

 				adDocumentSequenceAssignment = adDocumentSequenceAssignmentHome.findByDcName("AP PURCHASE REQUISITION", AD_CMPNY);

 			} catch (FinderException ex) {

 			}

 			try {

 				adBranchDocumentSequenceAssignment = adBranchDocumentSequenceAssignmentHome.findBdsByDsaCodeAndBrCode(adDocumentSequenceAssignment.getDsaCode(), AD_BRNCH, AD_CMPNY);

 			} catch (FinderException ex) {

 			}

	        if (adDocumentSequenceAssignment.getAdDocumentSequence().getDsNumberingType() == 'A') {

	            while (true) {

	            	if (adBranchDocumentSequenceAssignment == null || adBranchDocumentSequenceAssignment.getBdsNextSequence() == null) {

	            		try {

		            		apPurchaseRequisitionHome.findByPrNumberAndBrCode(adDocumentSequenceAssignment.getDsaNextSequence(), AD_BRNCH, AD_CMPNY);
		            		adDocumentSequenceAssignment.setDsaNextSequence(EJBCommon.incrementStringNumber(adDocumentSequenceAssignment.getDsaNextSequence()));

		            	} catch (FinderException ex) {

		            		prNumber = adDocumentSequenceAssignment.getDsaNextSequence();
		            		adDocumentSequenceAssignment.setDsaNextSequence(EJBCommon.incrementStringNumber(adDocumentSequenceAssignment.getDsaNextSequence()));
				            break;

		            	}

	            	} else {

	            		try {

	            			apPurchaseRequisitionHome.findByPrNumberAndBrCode(adBranchDocumentSequenceAssignment.getBdsNextSequence(), AD_BRNCH, AD_CMPNY);
		            		adBranchDocumentSequenceAssignment.setBdsNextSequence(EJBCommon.incrementStringNumber(adBranchDocumentSequenceAssignment.getBdsNextSequence()));

		            	} catch (FinderException ex) {

		            		prNumber = adBranchDocumentSequenceAssignment.getBdsNextSequence();
		            		adBranchDocumentSequenceAssignment.setBdsNextSequence(EJBCommon.incrementStringNumber(adBranchDocumentSequenceAssignment.getBdsNextSequence()));
				            break;

		            	}

	            	}

	            }

	        }
	        java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat("MM/dd/yyyy HH:mm:ss.SSSS");
	        //create purchase requisition
	        LocalApPurchaseRequisition apPurchaseRequisition = apPurchaseRequisitionHome.create("CONSOLIDATED PR " + formatter.format(new java.util.Date()),
	        		prNumber, new java.util.Date(), null, null , null,
					1.000000, "APPROVED", EJBCommon.TRUE, EJBCommon.FALSE, null, EJBCommon.FALSE, EJBCommon.FALSE, null,null,
					AD_USR, new java.util.Date(), AD_USR, new java.util.Date(),
					AD_USR, new java.util.Date(), null, null,
					null, null, null, null,
					"REGULAR","Inventoriable",
					null,null,null,null,
					AD_BRNCH, AD_CMPNY);

	        LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);

			LocalApTaxCode apTaxCode = apTaxCodeHome.findByTcName(adPreference.getPrfApDefaultPrTax(), AD_CMPNY);
			apPurchaseRequisition.setApTaxCode(apTaxCode);

			LocalGlFunctionalCurrency glFunctionalCurrency = glFunctionalCurrencyHome.findByFcName(adPreference.getPrfApDefaultPrCurrency(), AD_CMPNY);
			apPurchaseRequisition.setGlFunctionalCurrency(glFunctionalCurrency);

			// psuedo code
			HashMap hm = new HashMap();
			Iterator i = prSelectedList.iterator();
			ApModPurchaseRequisitionLineDetails prlDetails = null;


			Double qty = 0d;
			while (i.hasNext()) {


				LocalApPurchaseRequisition apForConPurchaseRequisition = apPurchaseRequisitionHome.findByPrimaryKey((Integer)i.next());

				Collection apPurchaseRequisitionLines = apForConPurchaseRequisition.getApPurchaseRequisitionLines();
				apForConPurchaseRequisition.setPrCanvassPosted(EJBCommon.TRUE);
				apForConPurchaseRequisition.setPrCanvassApprovalStatus("N/A");
				apForConPurchaseRequisition.setPrGenerated(EJBCommon.TRUE);
				apForConPurchaseRequisition.setPrCanvassApprovedRejectedBy(AD_USR);
				apForConPurchaseRequisition.setPrCanvassPostedBy(AD_USR);
				apForConPurchaseRequisition.setPrCanvassDateApprovedRejected(new java.util.Date());
				apForConPurchaseRequisition.setPrCanvassDatePosted(new java.util.Date());

				Iterator j = apPurchaseRequisitionLines.iterator();

				while (j.hasNext()) {

				    LocalApPurchaseRequisitionLine apPurchaseRequisitionLine = (LocalApPurchaseRequisitionLine)j.next();
				   if (hm.containsKey(apPurchaseRequisitionLine.getInvItemLocation().getInvItem().getIiName()+apPurchaseRequisitionLine.getInvItemLocation().getInvLocation().getLocName() )){

                       ApModPurchaseRequisitionLineDetails apExistingPrlDetails = (ApModPurchaseRequisitionLineDetails)
                      		hm.get(apPurchaseRequisitionLine.getInvItemLocation().getInvItem().getIiName()+apPurchaseRequisitionLine.getInvItemLocation().getInvLocation().getLocName());
                       		apExistingPrlDetails.setPrlQuantity(apExistingPrlDetails.getPrlQuantity() + apPurchaseRequisitionLine.getPrlQuantity());
                       	    hm.put(apPurchaseRequisitionLine.getInvItemLocation().getInvItem().getIiName()+apPurchaseRequisitionLine.getInvItemLocation().getInvLocation().getLocName(),  apExistingPrlDetails);


				   } else {


					    ApModPurchaseRequisitionLineDetails apNewPrlDetails = new ApModPurchaseRequisitionLineDetails();
					    //apNewPrlDetails.setPrlQuantity(apPurchaseRequisitionLine.getPrlQuantity());
					    apNewPrlDetails.setPrlIlIiName(apPurchaseRequisitionLine.getInvItemLocation().getInvItem().getIiName());
					    apNewPrlDetails.setPrlIlLocName(apPurchaseRequisitionLine.getInvItemLocation().getInvLocation().getLocName());
					    apNewPrlDetails.setPrlQuantity(apPurchaseRequisitionLine.getPrlQuantity());

					    System.out.println("QTY="+apPurchaseRequisitionLine.getPrlQuantity());

		                hm.put(apPurchaseRequisitionLine.getInvItemLocation().getInvItem().getIiName()+apPurchaseRequisitionLine.getInvItemLocation().getInvLocation().getLocName(), apNewPrlDetails);
		                System.out.println("--->"+hm.get(apPurchaseRequisitionLine.getInvItemLocation().getInvItem().getIiName()+apPurchaseRequisitionLine.getInvItemLocation().getInvLocation().getLocName()));
		                System.out.println("3");

				   }



				}

			}


			// looping for hm
			//create prLines
			Set set = hm.entrySet();

			Iterator z = set.iterator();

			//while(z.hasNext()){
			//      Map.Entry me = (Map.Entry)z.next();
			//      System.out.println(me.getKey() + " : " + me.getValue() );
			//}

			short ctr=0;
			while(z.hasNext()) {
				 Map.Entry me = (Map.Entry)z.next();
				ApModPurchaseRequisitionLineDetails prlDetailsC = (ApModPurchaseRequisitionLineDetails)
						me.getValue();
				System.out.println("--->"+me.getValue());
				System.out.println("getPrlIlIiName------>"+prlDetailsC.getPrlIlIiName());

				LocalApPurchaseRequisitionLine apPurchaseRequisitionLine = apPurchaseRequisitionLineHome.create(++ctr, 0d, 0d, AD_CMPNY);
				apPurchaseRequisition.addApPurchaseRequisitionLine(apPurchaseRequisitionLine);
				System.out.println("1");
				LocalInvItemLocation invItemLocation = invItemLocationHome.findByIiNameAndLocName(prlDetailsC.getPrlIlIiName(), prlDetailsC.getPrlIlLocName(), AD_CMPNY);
				invItemLocation.addApPurchaseRequisitionLine(apPurchaseRequisitionLine);
				System.out.println("2");
				apPurchaseRequisitionLine.setPrlQuantity(prlDetailsC.getPrlQuantity());
				apPurchaseRequisitionLine.setPrlAmount(invItemLocation.getInvItem().getIiUnitCost());

				LocalInvUnitOfMeasure invUnitOfMeasure = invItemLocation.getInvItem().getInvUnitOfMeasure();
				invUnitOfMeasure.addApPurchaseRequisitionLine(apPurchaseRequisitionLine);


			}

			return apPurchaseRequisition.getPrCode();

		} catch (Exception ex) {

			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());

		}

	}



	//private methods

		private boolean hasSupplier(LocalApPurchaseRequisitionLine apPurchaseRequisitionLine, String SPL_SPPLR_CD) {

			Debug.print("ApCanvassControllerBean hasSupplier");

			Collection apCanvasses = apPurchaseRequisitionLine.getApCanvasses();

			Iterator i = apCanvasses.iterator();

			while(i.hasNext()) {

				LocalApCanvass apCanvass = (LocalApCanvass)i.next();

				if(apCanvass.getApSupplier().getSplSupplierCode().equals(SPL_SPPLR_CD)) {

					return true;

				}

			}

			return false;

		}


	private double getInvLastPurchasePriceBySplSupplierCode(LocalInvItemLocation invItemLocation, String SPL_SPPLR_CODE, Integer AD_BRNCH, Integer AD_CMPNY) {

		Debug.print("ApFindPurchaseRequisitionControllerBean getInvLastPurchasePriceBySplSupplierCode");

		LocalApPurchaseOrderLineHome apPurchaseOrderLineHome = null;

		// Initialize EJB Home

		try {

			apPurchaseOrderLineHome = (LocalApPurchaseOrderLineHome)EJBHomeFactory.
				lookUpLocalHome(LocalApPurchaseOrderLineHome.JNDI_NAME, LocalApPurchaseOrderLineHome.class);

		} catch (NamingException ex) {

		    throw new EJBException(ex.getMessage());

		}

		try {

			LocalApPurchaseOrderLine apPurchaseOrderLine = apPurchaseOrderLineHome.
			getByMaxPoDateAndMaxPlCodeAndIiNameAndLocNameAndPoReceivingAndPoPostedAndSplSpplrCode(
					invItemLocation.getInvItem().getIiName(),
					invItemLocation.getInvLocation().getLocName(),
					EJBCommon.TRUE, EJBCommon.TRUE,
					invItemLocation.getInvItem().getApSupplier().getSplSupplierCode(),
					AD_BRNCH, AD_CMPNY);

			return this.convertCostByUom(invItemLocation.getInvItem().getIiName(), apPurchaseOrderLine.getInvUnitOfMeasure().getUomName(),
					apPurchaseOrderLine.getPlUnitCost(), false, AD_CMPNY);

		} catch(FinderException ex) {

			return 0d;

		}

	}

	private double getInvLastPurchasePrice(LocalInvItemLocation invItemLocation, Integer AD_BRNCH, Integer AD_CMPNY) {

		Debug.print("ApFindPurchaseRequisitionControllerBean getInvLastPurchasePrice");

		LocalApPurchaseOrderLineHome apPurchaseOrderLineHome = null;

		// Initialize EJB Home

		try {

			apPurchaseOrderLineHome = (LocalApPurchaseOrderLineHome)EJBHomeFactory.
				lookUpLocalHome(LocalApPurchaseOrderLineHome.JNDI_NAME, LocalApPurchaseOrderLineHome.class);

		} catch (NamingException ex) {

		    throw new EJBException(ex.getMessage());

		}

		try {

			LocalApPurchaseOrderLine apPurchaseOrderLine = apPurchaseOrderLineHome.
			getByMaxPoDateAndMaxPlCodeAndIiNameAndLocNameAndPoReceivingAndPoPosted(
					invItemLocation.getInvItem().getIiName(),
					invItemLocation.getInvLocation().getLocName(),
					EJBCommon.TRUE, EJBCommon.TRUE,
					AD_BRNCH, AD_CMPNY);

			return this.convertCostByUom(invItemLocation.getInvItem().getIiName(),
					apPurchaseOrderLine.getInvUnitOfMeasure().getUomName(),
					apPurchaseOrderLine.getPlUnitCost(), false, AD_CMPNY);

		} catch(FinderException ex) {

			return 0d;

		}

	}

	private double convertCostByUom(String II_NM, String UOM_NM, double unitCost, boolean isFromDefault, Integer AD_CMPNY) {

		Debug.print("ApFindPurchaseRequisitionControllerBean convertCostByUom");

		LocalInvItemHome invItemHome = null;
		LocalInvUnitOfMeasureConversionHome invUnitOfMeasureConversionHome = null;

	    // Initialize EJB Home

	    try {

	        invUnitOfMeasureConversionHome = (LocalInvUnitOfMeasureConversionHome)EJBHomeFactory.
				lookUpLocalHome(LocalInvUnitOfMeasureConversionHome.JNDI_NAME, LocalInvUnitOfMeasureConversionHome.class);
	        invItemHome = (LocalInvItemHome)EJBHomeFactory.
				lookUpLocalHome(LocalInvItemHome.JNDI_NAME, LocalInvItemHome.class);

	    } catch (NamingException ex) {

	        throw new EJBException(ex.getMessage());

	    }

	    try {

	    	LocalInvItem invItem = invItemHome.findByIiName(II_NM, AD_CMPNY);
	        LocalInvUnitOfMeasureConversion invUnitOfMeasureConversion = invUnitOfMeasureConversionHome.findUmcByIiNameAndUomName(II_NM, UOM_NM, AD_CMPNY);
	        LocalInvUnitOfMeasureConversion invDefaultUomConversion = invUnitOfMeasureConversionHome.findUmcByIiNameAndUomName(II_NM, invItem.getInvUnitOfMeasure().getUomName(), AD_CMPNY);

	        if (isFromDefault) {

	        	return EJBCommon.roundIt(unitCost * invDefaultUomConversion.getUmcConversionFactor() / invUnitOfMeasureConversion.getUmcConversionFactor(), this.getGlFcPrecisionUnit(AD_CMPNY));

	        } else {

	        	return EJBCommon.roundIt(unitCost * invUnitOfMeasureConversion.getUmcConversionFactor() / invDefaultUomConversion.getUmcConversionFactor(), this.getGlFcPrecisionUnit(AD_CMPNY));

	        }



	    } catch (Exception ex) {

	    	Debug.printStackTrace(ex);
	    	getSessionContext().setRollbackOnly();
	    	throw new EJBException(ex.getMessage());

	    }

	}


	private void setInvTbForItemForCurrentMonth(String itemName, String userName, Date date, double qtyConsumed, Integer AD_CMPNY)
	throws GlobalNoRecordFoundException
	{

		Debug.print("ApFindPurchaseRequisitionControllerBean getInvTrnsctnlBdgtForCurrentMonth");
		LocalInvTransactionalBudgetHome invTransactionalBudgetHome = null;
		LocalInvItemHome invItemHome = null;
		LocalAdUserHome adUserHome = null;
		LocalAdLookUpValueHome adLookUpValueHome = null;
		ArrayList list = new ArrayList();

		// Initialize EJB Home

		try {

			invTransactionalBudgetHome = (LocalInvTransactionalBudgetHome)EJBHomeFactory.
				lookUpLocalHome(LocalInvTransactionalBudgetHome.JNDI_NAME, LocalInvTransactionalBudgetHome.class);
			invItemHome = (LocalInvItemHome)EJBHomeFactory.
					lookUpLocalHome(LocalInvItemHome.JNDI_NAME, LocalInvItemHome.class);
			adUserHome = (LocalAdUserHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdUserHome.JNDI_NAME, LocalAdUserHome.class);
			adLookUpValueHome = (LocalAdLookUpValueHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdLookUpValueHome.JNDI_NAME, LocalAdLookUpValueHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}
		try {
			java.text.SimpleDateFormat sdf = new java.text.SimpleDateFormat("MM");
			java.text.SimpleDateFormat sdfYear = new java.text.SimpleDateFormat("yyyy");
			String month = sdf.format(date);

			LocalAdUser adUser = adUserHome.findByUsrName(userName, AD_CMPNY);
			LocalAdLookUpValue adLookUpValue = adLookUpValueHome.findByLuNameAndLvName("AD DEPARTMENT", adUser.getUsrDept(), AD_CMPNY);
			System.out.println("itemName-" + itemName);
			System.out.println("adLookUpValue.getLvCode()-" + adLookUpValue.getLvCode());
			System.out.println("Integer.parseInt(sdfYear.format(date))-" + Integer.parseInt(sdfYear.format(date)));
			LocalInvTransactionalBudget invTransactionalBudget = invTransactionalBudgetHome.findByTbItemNameAndTbDeptAndTbYear(itemName, adLookUpValue.getLvCode(), Integer.parseInt(sdfYear.format(date)), AD_CMPNY);
			//LocalInvItem invItem = LocalInvItemHome


			if (month.equals("01")){

				invTransactionalBudget.setTbQuantityJan( invTransactionalBudget.getTbQuantityJan() - qtyConsumed);
			}else if (month.equals("02")){

				invTransactionalBudget.setTbQuantityFeb( invTransactionalBudget.getTbQuantityFeb() - qtyConsumed);
			}else if (month.equals("03")){
				invTransactionalBudget.setTbQuantityMrch( invTransactionalBudget.getTbQuantityMrch() - qtyConsumed);
			}else if (month.equals("04")){
				invTransactionalBudget.setTbQuantityAprl( invTransactionalBudget.getTbQuantityAprl() - qtyConsumed);
			}else if (month.equals("05")){
				invTransactionalBudget.setTbQuantityMay( invTransactionalBudget.getTbQuantityMay() - qtyConsumed);
			}else if (month.equals("06")){
				invTransactionalBudget.setTbQuantityJun( invTransactionalBudget.getTbQuantityJun() - qtyConsumed);
			}else if (month.equals("07")){
				invTransactionalBudget.setTbQuantityJul( invTransactionalBudget.getTbQuantityJul() - qtyConsumed);
			}else if (month.equals("08")){
				invTransactionalBudget.setTbQuantityAug( invTransactionalBudget.getTbQuantityAug() - qtyConsumed);
			}else if (month.equals("09")){
				invTransactionalBudget.setTbQuantitySep( invTransactionalBudget.getTbQuantitySep() - qtyConsumed);
			}else if (month.equals("10")){
				invTransactionalBudget.setTbQuantityOct( invTransactionalBudget.getTbQuantityOct() - qtyConsumed);
			}else if (month.equals("11")){
				invTransactionalBudget.setTbQuantityNov( invTransactionalBudget.getTbQuantityNov() - qtyConsumed);
			}else {
				invTransactionalBudget.setTbQuantityDec( invTransactionalBudget.getTbQuantityDec() - qtyConsumed);
			}

		} catch (FinderException ex) {
			System.out.println("no item found in transactional budget table");
			  //throw new GlobalNoRecordFoundException();
        }
	}

    // SessionBean methods

    /**
     * @ejb:create-method view-type="remote"
     **/
    public void ejbCreate() throws CreateException {

       Debug.print("ApFindPurchaseRequisitionControllerBean ejbCreate");

    }
}
