
/*
 * ApFindRecurringVoucherControllerBean.java
 *
 * Created on February 20, 2004, 5:43 PM
 *
 * @author  Neil Andrew M. Ajero
 */

package com.ejb.txn;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.naming.NamingException;

import com.ejb.ad.LocalAdPreference;
import com.ejb.ad.LocalAdPreferenceHome;
import com.ejb.ap.LocalApRecurringVoucher;
import com.ejb.ap.LocalApRecurringVoucherHome;
import com.ejb.ap.LocalApSupplier;
import com.ejb.ap.LocalApSupplierHome;
import com.ejb.exception.GlobalNoRecordFoundException;
import com.util.AbstractSessionBean;
import com.util.ApModRecurringVoucherDetails;
import com.util.Debug;
import com.util.EJBHomeFactory;

/**
 * @ejb:bean name="ApFindRecurringVoucherControllerEJB"
 *           display-name="Used for finding recurring vouchers"
 *           type="Stateless"
 *           view-type="remote"
 *           jndi-name="ejb/ApFindRecurringVoucherControllerEJB"
 *
 * @ejb:interface remote-class="com.ejb.txn.ApFindRecurringVoucherController"
 *                extends="javax.ejb.EJBObject"
 *
 * @ejb:home remote-class="com.ejb.txn.ApFindRecurringVoucherControllerHome"
 *           extends="javax.ejb.EJBHome"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="apuser"
 *                        role-link="apuserlink"
 *
 * @ejb:permission role-name="apuser"
 * 
*/

public class ApFindRecurringVoucherControllerBean extends AbstractSessionBean {


    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList getApSplAll(Integer AD_BRNCH, Integer AD_CMPNY) {
                    
        Debug.print("ApFindRecurringVoucherControllerBean getApSplAll");
        
        LocalApSupplierHome apSupplierHome = null;               
        
        ArrayList list = new ArrayList();
        
        // Initialize EJB Home
        
        try {
            
            apSupplierHome = (LocalApSupplierHome)EJBHomeFactory.
                lookUpLocalHome(LocalApSupplierHome.JNDI_NAME, LocalApSupplierHome.class);
            
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }        
        
        try {
        	
        	Collection apSuppliers = apSupplierHome.findEnabledSplAll(AD_BRNCH, AD_CMPNY);
        	
        	Iterator i = apSuppliers.iterator();
        	
        	while (i.hasNext()) {
        		
        		LocalApSupplier apSupplier = (LocalApSupplier)i.next();
        		
        		list.add(apSupplier.getSplSupplierCode());
        		
        	}
        	
        	return list;
        	
        } catch (Exception ex) {
        	
        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());
        	
        }
            
    } 
    
   
  /**
   * @ejb:interface-method view-type="remote"
   * @jboss:method-attributes read-only="true"
   **/
   public ArrayList getApRvByCriteria(HashMap criteria, String ORDER_BY, Integer OFFSET, Integer LIMIT, Integer AD_BRNCH, Integer AD_CMPNY)
      throws GlobalNoRecordFoundException {

      Debug.print("ApFindRecurringVoucherControllerBean getApRvByCriteria");
      
      LocalApRecurringVoucherHome apRecurringJournalHome = null;
      
      // Initialize EJB Home
        
      try {
      	
          apRecurringJournalHome = (LocalApRecurringVoucherHome)EJBHomeFactory.
              lookUpLocalHome(LocalApRecurringVoucherHome.JNDI_NAME, LocalApRecurringVoucherHome.class);          
            
      } catch (NamingException ex) {
            
          throw new EJBException(ex.getMessage());
            
      }

      
      ArrayList rvList = new ArrayList();
      
      StringBuffer jbossQl = new StringBuffer();
      jbossQl.append("SELECT OBJECT(rv) FROM ApRecurringVoucher rv ");
      
      boolean firstArgument = true;
      short ctr = 0;
      int criteriaSize = criteria.size() + 2;
      Object obj[];
      
       // Allocate the size of the object parameter

       
      if (criteria.containsKey("name")) {
      	
      	  criteriaSize--; 
      	 
      } 
            
      
      obj = new Object[criteriaSize];
         
      
      if (criteria.containsKey("name")) {
      	
      	 if (!firstArgument) {
      	 
      	    jbossQl.append("AND ");	
      	 	
         } else {
         	
         	firstArgument = false;
         	jbossQl.append("WHERE ");
         	
         }
         
      	 jbossQl.append("rv.rvName LIKE '%" + (String)criteria.get("name") + "%' ");
      	 
      }            	 

	  	  	  
	  if (criteria.containsKey("supplierCode")) {
       	
       	  if (!firstArgument) {
       	  	
       	     jbossQl.append("AND ");
       	     
       	  } else {
       	  	
       	  	 firstArgument = false;
       	  	 jbossQl.append("WHERE ");
       	  	 
       	  }
       	  
       	  jbossQl.append("rv.apSupplier.splSupplierCode=?" + (ctr+1) + " ");
       	  obj[ctr] = (String)criteria.get("supplierCode");
       	  ctr++;
       	  
      } 
        
      if (criteria.containsKey("nextRunDateFrom")) {
	      	
	  	 if (!firstArgument) {
	  	 	jbossQl.append("AND ");
	  	 } else {
	  	 	firstArgument = false;
	  	 	jbossQl.append("WHERE ");
	  	 }
	  	 jbossQl.append("rv.rvNextRunDate>=?" + (ctr+1) + " ");
	  	 obj[ctr] = (Date)criteria.get("nextRunDateFrom");
	  	 ctr++;
	  }  
	      
	  if (criteria.containsKey("nextRunDateTo")) {
	  	
	  	 if (!firstArgument) {
	  	 	jbossQl.append("AND ");
	  	 } else {
	  	 	firstArgument = false;
	  	 	jbossQl.append("WHERE ");
	  	 }
	  	 jbossQl.append("rv.rvNextRunDate<=?" + (ctr+1) + " ");
	  	 obj[ctr] = (Date)criteria.get("nextRunDateTo");
	  	 ctr++;
	  	 
	  } 
	  
	  if (!firstArgument) {
	      jbossQl.append("AND ");
	  } else {
	      firstArgument = false;
	      jbossQl.append("WHERE ");
	  }
	  jbossQl.append("rv.rvAdBranch=" + AD_BRNCH + " ");
	  
	  if (!firstArgument) {
  	 	jbossQl.append("AND ");
  	  } else {
  	 	firstArgument = false;
  	 	jbossQl.append("WHERE ");
  	  }
  	  jbossQl.append("rv.rvAdCompany=" + AD_CMPNY + " ");
             	     
      String orderBy = null;
	      
	  if (ORDER_BY.equals("NAME")) {
	
	  	  orderBy = "rv.rvName";
	  	  
	  } else if (ORDER_BY.equals("NEXT RUN DATE")) {
	
	  	  orderBy = "rv.rvNextRunDate";
	  	
	  } else if (ORDER_BY.equals("SUPPLIER CODE")) {
	
	  	  orderBy = "rv.apSupplier.splSupplierCode";
	  	  
	  } 
	  
	  if (orderBy != null) {
	  
	  	jbossQl.append("ORDER BY " + orderBy);
	  	
	  } 
	  
               
      jbossQl.append(" OFFSET ?" + (ctr + 1));
      obj[ctr] = OFFSET;
      ctr++;
      
      jbossQl.append(" LIMIT ?" + (ctr + 1));
      obj[ctr] = LIMIT;
      
      System.out.println("QL + " + jbossQl);

      Collection apRecurringJournals = null;
      
      try {
      	
         apRecurringJournals = apRecurringJournalHome.getRvByCriteria(jbossQl.toString(), obj);
         
      } catch (Exception ex) {
      	
      	 throw new EJBException(ex.getMessage());
      	 
      }
      
      if (apRecurringJournals.isEmpty())
         throw new GlobalNoRecordFoundException();
         
      Iterator i = apRecurringJournals.iterator();
      while (i.hasNext()) {

         LocalApRecurringVoucher apRecurringVoucher = (LocalApRecurringVoucher) i.next();

	     ApModRecurringVoucherDetails mdetails = new ApModRecurringVoucherDetails();
	     mdetails.setRvCode(apRecurringVoucher.getRvCode());
	     mdetails.setRvName(apRecurringVoucher.getRvName());
	     mdetails.setRvDescription(apRecurringVoucher.getRvDescription());
	     mdetails.setRvSchedule(apRecurringVoucher.getRvSchedule());	     
	     mdetails.setRvSplSupplierCode(apRecurringVoucher.getApSupplier().getSplSupplierCode());
	     mdetails.setRvNextRunDate(apRecurringVoucher.getRvNextRunDate());
	     mdetails.setRvLastRunDate(apRecurringVoucher.getRvLastRunDate());     
	           	  
      	 rvList.add(mdetails);
      	
      }
         
      return rvList;
  
   }
   
   /**
    * @ejb:interface-method view-type="remote"
    * @jboss:method-attributes read-only="true"
    **/
    public Integer getApRvSizeByCriteria(HashMap criteria, Integer AD_BRNCH, Integer AD_CMPNY)
       throws GlobalNoRecordFoundException {

       Debug.print("ApFindRecurringVoucherControllerBean getApRvSizeByCriteria");
       
       LocalApRecurringVoucherHome apRecurringJournalHome = null;
       
       // Initialize EJB Home
         
       try {
       	
           apRecurringJournalHome = (LocalApRecurringVoucherHome)EJBHomeFactory.
               lookUpLocalHome(LocalApRecurringVoucherHome.JNDI_NAME, LocalApRecurringVoucherHome.class);          
             
       } catch (NamingException ex) {
             
           throw new EJBException(ex.getMessage());
             
       }

       StringBuffer jbossQl = new StringBuffer();
       jbossQl.append("SELECT OBJECT(rv) FROM ApRecurringVoucher rv ");
       
       boolean firstArgument = true;
       short ctr = 0;
       int criteriaSize = criteria.size();
       Object obj[];
       
        // Allocate the size of the object parameter

        
       if (criteria.containsKey("name")) {
       	
       	  criteriaSize--; 
       	 
       } 
             
       
       obj = new Object[criteriaSize];
          
       
       if (criteria.containsKey("name")) {
       	
       	 if (!firstArgument) {
       	 
       	    jbossQl.append("AND ");	
       	 	
          } else {
          	
          	firstArgument = false;
          	jbossQl.append("WHERE ");
          	
          }
          
       	 jbossQl.append("rv.rvName LIKE '%" + (String)criteria.get("name") + "%' ");
       	 
       }            	 

 	  	  	  
 	  if (criteria.containsKey("supplierCode")) {
        	
        	  if (!firstArgument) {
        	  	
        	     jbossQl.append("AND ");
        	     
        	  } else {
        	  	
        	  	 firstArgument = false;
        	  	 jbossQl.append("WHERE ");
        	  	 
        	  }
        	  
        	  jbossQl.append("rv.apSupplier.splSupplierCode=?" + (ctr+1) + " ");
        	  obj[ctr] = (String)criteria.get("supplierCode");
        	  ctr++;
        	  
       } 
         
       if (criteria.containsKey("nextRunDateFrom")) {
 	      	
 	  	 if (!firstArgument) {
 	  	 	jbossQl.append("AND ");
 	  	 } else {
 	  	 	firstArgument = false;
 	  	 	jbossQl.append("WHERE ");
 	  	 }
 	  	 jbossQl.append("rv.rvNextRunDate>=?" + (ctr+1) + " ");
 	  	 obj[ctr] = (Date)criteria.get("nextRunDateFrom");
 	  	 ctr++;
 	  }  
 	      
 	  if (criteria.containsKey("nextRunDateTo")) {
 	  	
 	  	 if (!firstArgument) {
 	  	 	jbossQl.append("AND ");
 	  	 } else {
 	  	 	firstArgument = false;
 	  	 	jbossQl.append("WHERE ");
 	  	 }
 	  	 jbossQl.append("rv.rvNextRunDate<=?" + (ctr+1) + " ");
 	  	 obj[ctr] = (Date)criteria.get("nextRunDateTo");
 	  	 ctr++;
 	  	 
 	  }         
 	  
 	  if (!firstArgument) {
 	      jbossQl.append("AND ");
 	  } else {
 	      firstArgument = false;
 	      jbossQl.append("WHERE ");
 	  }
 	  jbossQl.append("rv.rvAdBranch=" + AD_BRNCH + " ");
 	  
 	  if (!firstArgument) {
   	 	jbossQl.append("AND ");
   	  } else {
   	 	firstArgument = false;
   	 	jbossQl.append("WHERE ");
   	  }
   	  jbossQl.append("rv.rvAdCompany=" + AD_CMPNY + " ");
              	     
       Collection apRecurringJournals = null;
       
       try {
       	
          apRecurringJournals = apRecurringJournalHome.getRvByCriteria(jbossQl.toString(), obj);
          
       } catch (Exception ex) {
       	
       	 throw new EJBException(ex.getMessage());
       	 
       }
       
       if (apRecurringJournals.isEmpty())
          throw new GlobalNoRecordFoundException();
          
       return new Integer(apRecurringJournals.size());
   
    }
      
   /**
    * @ejb:interface-method view-type="remote"
    * @jboss:method-attributes read-only="true"
    **/
    public byte getAdPrfApUseSupplierPulldown(Integer AD_CMPNY) {

       Debug.print("ApFindRecurringVoucherControllerBean getAdPrfApUseSupplierPulldown");
                  
       LocalAdPreferenceHome adPreferenceHome = null;
      
      
       // Initialize EJB Home
        
       try {
            
          adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
              lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);       
            
       } catch (NamingException ex) {
            
          throw new EJBException(ex.getMessage());
            
       }
      

       try {
       	
          LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);
          
          return adPreference.getPrfApUseSupplierPulldown();
         
       } catch (Exception ex) {
        	 
          Debug.printStackTrace(ex);
          throw new EJBException(ex.getMessage());
         
       }
      
    }

   // SessionBean methods

   /**
    * @ejb:create-method view-type="remote"
    **/
   public void ejbCreate() throws CreateException {

      Debug.print("ApFindRecurringVoucherControllerBean ejbCreate");
      
   }

   // private methods
   	
		   
}
