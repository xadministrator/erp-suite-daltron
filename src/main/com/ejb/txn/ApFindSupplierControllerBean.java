
/*
 * ApFindSupplierControllerBean.java
 *
 * Created on February 17, 2004, 9:47 AM
 *
 * @author  Neil Andrew M. Ajero
 */

package com.ejb.txn;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.naming.NamingException;

import com.ejb.ap.LocalApSupplier;
import com.ejb.ap.LocalApSupplierClass;
import com.ejb.ap.LocalApSupplierClassHome;
import com.ejb.ap.LocalApSupplierHome;
import com.ejb.ap.LocalApSupplierType;
import com.ejb.ap.LocalApSupplierTypeHome;
import com.ejb.exception.GlobalNoRecordFoundException;
import com.util.AbstractSessionBean;
import com.util.AdBranchDetails;
import com.util.ApModSupplierDetails;
import com.util.Debug;
import com.util.EJBCommon;
import com.util.EJBHomeFactory;

/**
 * @ejb:bean name="ApFindSupplierControllerEJB"
 *           display-name="Used for finding suppliers"
 *           type="Stateless"
 *           view-type="remote"
 *           jndi-name="ejb/ApFindSupplierControllerEJB"
 *
 * @ejb:interface remote-class="com.ejb.txn.ApFindSupplierController"
 *                extends="javax.ejb.EJBObject"
 *
 * @ejb:home remote-class="com.ejb.txn.ApFindSupplierControllerHome"
 *           extends="javax.ejb.EJBHome"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="apuser"
 *                        role-link="apuserlink"
 *
 * @ejb:permission role-name="apuser"
 * 
 */

public class ApFindSupplierControllerBean extends AbstractSessionBean {
	
	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public ArrayList getApStAll(Integer AD_CMPNY) {
		
		Debug.print("ApSupplierEntryControllerBean getApStAll");
		
		LocalApSupplierTypeHome apSupplierTypeHome = null;
		LocalApSupplierType apSupplierType = null;
		
		ArrayList list = new ArrayList();
		
		// Initialize EJB Home
		
		try {
			
			apSupplierTypeHome = (LocalApSupplierTypeHome)EJBHomeFactory.
			lookUpLocalHome(LocalApSupplierTypeHome.JNDI_NAME, LocalApSupplierTypeHome.class);
			
		} catch (NamingException ex) {
			
			throw new EJBException(ex.getMessage());
			
		}
		
		try {
			
			Collection apSupplierTypes = apSupplierTypeHome.findEnabledStAll(AD_CMPNY);
			
			Iterator i = apSupplierTypes.iterator();
			
			while (i.hasNext()) {
				
				apSupplierType = (LocalApSupplierType)i.next();
				
				list.add(apSupplierType.getStName());
				
			}
			
			return list;
			
		} catch (Exception ex) {
			
			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());
			
		}
		
	}
	
	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public ArrayList getApScAll(Integer AD_CMPNY) {
		
		Debug.print("ApSupplierEntryControllerBean getApScAll");
		
		LocalApSupplierClassHome apSupplierClassHome = null;
		LocalApSupplierClass apSupplierClass = null;
		
		ArrayList list = new ArrayList();
		
		// Initialize EJB Home
		
		try {
			
			apSupplierClassHome = (LocalApSupplierClassHome)EJBHomeFactory.
			lookUpLocalHome(LocalApSupplierClassHome.JNDI_NAME, LocalApSupplierClassHome.class);
			
		} catch (NamingException ex) {
			
			throw new EJBException(ex.getMessage());
			
		}
		
		try {
			
			Collection apSupplierClasses = apSupplierClassHome.findEnabledScAll(AD_CMPNY);
			
			Iterator i = apSupplierClasses.iterator();
			
			while (i.hasNext()) {
				
				apSupplierClass = (LocalApSupplierClass)i.next();
				
				list.add(apSupplierClass.getScName());
				
			}
			
			return list;
			
		} catch (Exception ex) {
			
			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());
			
		}
		
	}
	
	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public ArrayList getApSplByCriteria(HashMap criteria,
			Integer OFFSET, Integer LIMIT, String ORDER_BY, ArrayList adBrnchList, Integer AD_CMPNY) 
	throws GlobalNoRecordFoundException {
		
		Debug.print("ApFindSupplierControllerBean getApSplByCriteria");
		
		LocalApSupplierHome apSupplierHome = null;
		
		ArrayList list = new ArrayList();
		
		//initialized EJB Home
		
		try {
			
			apSupplierHome = (LocalApSupplierHome)EJBHomeFactory.
			lookUpLocalHome(LocalApSupplierHome.JNDI_NAME, LocalApSupplierHome.class);
			
		} catch (NamingException ex) 	{
			
			throw new EJBException(ex.getMessage());
			
		}
		
		try { 
			
			StringBuffer jbossQl = new StringBuffer();
			jbossQl.append("SELECT DISTINCT OBJECT(spl) FROM ApSupplier spl ");
			
			boolean firstArgument = true;
			short ctr = 0;
			int criteriaSize = criteria.size() + 2;
			
			if (adBrnchList.isEmpty()) {
				
			}
			else {
				
				jbossQl.append(", in (spl.adBranchSuppliers) bspl WHERE bspl.adBranch.brCode in (");
				
				boolean firstLoop = true;
				
				Iterator j = adBrnchList.iterator();
				
				while(j.hasNext()) {
					
					if(firstLoop == false) { 
						jbossQl.append(", "); 
					}
					else { 
						firstLoop = false; 
					}
					
					AdBranchDetails mdetails = (AdBranchDetails) j.next();
					
					jbossQl.append(mdetails.getBrCode());
					
				}
				
				jbossQl.append(") ");
				
				firstArgument = false;
				
			}                                      
			
			Object obj[] = null;		      
			
			// Allocate the size of the object parameter
			
			
			if (criteria.containsKey("supplierCode")) {
				
				criteriaSize--;
				
			} 
			
			if (criteria.containsKey("name")) {
				
				criteriaSize--;
				
			} 
			
			if (criteria.containsKey("email")) {
				
				criteriaSize--;
				
			}   	  
			
			obj = new Object[criteriaSize];    
			
			if (criteria.containsKey("supplierCode")) {
				
				if (!firstArgument) {
					
					jbossQl.append("AND ");	
					
				} else {
					
					firstArgument = false;
					jbossQl.append("WHERE ");
					
				}
				
				jbossQl.append("spl.splSupplierCode LIKE '%" + (String)criteria.get("supplierCode") + "%' ");
				
			}
			
			if (criteria.containsKey("name")) {
				
				if (!firstArgument) {
					jbossQl.append("AND ");
				} else {
					firstArgument = false;
					jbossQl.append("WHERE ");
				}
				
				jbossQl.append("spl.splName LIKE '%" + (String)criteria.get("name") + "%' ");		
				
			} 
			
			if (criteria.containsKey("email")) {
				
				if (!firstArgument) {
					jbossQl.append("AND ");
				} else {
					firstArgument = false;
					jbossQl.append("WHERE ");
				}
				
				jbossQl.append("spl.splEmail LIKE '%" + (String)criteria.get("email") + "%' ");
				
				
			}	
			
			if (criteria.containsKey("supplierType")) {
				
				if (!firstArgument) {		       	  	
					jbossQl.append("AND ");		       	     
				} else {		       	  	
					firstArgument = false;
					jbossQl.append("WHERE ");		       	  	 
				}
				
				jbossQl.append("spl.apSupplierType.stName=?" + (ctr+1) + " ");
				obj[ctr] = (String)criteria.get("supplierType");
				ctr++;
				
			}	
			
			if (criteria.containsKey("supplierClass")) {
				
				if (!firstArgument) {		       	  	
					jbossQl.append("AND ");		       	     
				} else {		       	  	
					firstArgument = false;
					jbossQl.append("WHERE ");		       	  	 
				}
				
				jbossQl.append("spl.apSupplierClass.scName=?" + (ctr+1) + " ");
				obj[ctr] = (String)criteria.get("supplierClass");
				ctr++;
				
			}
			
			if (criteria.containsKey("enable") &&
					criteria.containsKey("disable")) {
				
				if (!firstArgument) {
					jbossQl.append("AND ");
				} else {
					firstArgument = false;
					jbossQl.append("WHERE ");
				}
				
				jbossQl.append("(spl.splEnable=?" + (ctr+1) + " OR ");
				obj[ctr] = new Byte(EJBCommon.TRUE);
				ctr++;
				
				jbossQl.append("spl.splEnable=?" + (ctr+1) + ") ");
				obj[ctr] = new Byte(EJBCommon.FALSE);
				ctr++;
				
			} else {
				
				if (criteria.containsKey("enable")) {
					
					if (!firstArgument) {
						jbossQl.append("AND ");
					} else {
						firstArgument = false;
						jbossQl.append("WHERE ");
					}
					jbossQl.append("spl.splEnable=?" + (ctr+1) + " ");
					obj[ctr] = new Byte(EJBCommon.TRUE);
					ctr++;
					
				} 
				
				if (criteria.containsKey("disable")) {
					
					if (!firstArgument) {
						jbossQl.append("AND ");
					} else {
						firstArgument = false;
						jbossQl.append("WHERE ");
					}
					jbossQl.append("spl.splEnable=?" + (ctr+1) + " ");		      	 		      	 		      	 	      	 
					obj[ctr] = new Byte(EJBCommon.FALSE);
					ctr++;
					
				}	    
				
			}  	      
			
			if (!firstArgument) {
				jbossQl.append("AND ");
			} else {
				firstArgument = false;
				jbossQl.append("WHERE ");
			}
			jbossQl.append("spl.splAdCompany=" + AD_CMPNY + " ");
			
			String orderBy = null;
			
			if (ORDER_BY.equals("SUPPLIER CODE")) {	          
				
				orderBy = "spl.splSupplierCode";
				
			} else {
				
				orderBy = "spl.splName";
				
			} 
			
			jbossQl.append("ORDER BY " + orderBy);
			
			jbossQl.append(" OFFSET ?" + (ctr + 1));	        
			obj[ctr] = OFFSET;	      
			ctr++;
			
			jbossQl.append(" LIMIT ?" + (ctr + 1));
			obj[ctr] = LIMIT;
			ctr++;		      
			System.out.println("jbossQL-" + jbossQl.toString());
			Collection apSuppliers = apSupplierHome.getSplByCriteria(jbossQl.toString(), obj);
			
			
			if (apSuppliers.size() == 0)
				throw new GlobalNoRecordFoundException();
			
			Iterator i = apSuppliers.iterator();
			
			while (i.hasNext()) {
				
				LocalApSupplier apSupplier = (LocalApSupplier)i.next();   	  
				
				ApModSupplierDetails mdetails = new ApModSupplierDetails();
				mdetails.setSplCode(apSupplier.getSplCode());
				mdetails.setSplSupplierCode(apSupplier.getSplSupplierCode());
				mdetails.setSplName(apSupplier.getSplName());
				mdetails.setSplTin(apSupplier.getSplTin());
				mdetails.setSplEmail(apSupplier.getSplEmail());
				mdetails.setSplStName(apSupplier.getApSupplierType() != null ? apSupplier.getApSupplierType().getStName() : null);
				mdetails.setSplScName(apSupplier.getApSupplierClass().getScName());
				mdetails.setSplPytName(apSupplier.getAdPaymentTerm().getPytName());
				mdetails.setSplEnable(apSupplier.getSplEnable());      	  
				
				list.add(mdetails);
				
			}
			
			return list;
			
		} catch (GlobalNoRecordFoundException ex) {
			
			throw ex;
			
		} catch (Exception ex) {
			
			
			ex.printStackTrace();
			throw new EJBException(ex.getMessage());
			
		}
		
	}
	
	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public Integer getApSplSizeByCriteria(HashMap criteria, ArrayList adBrnchList, Integer AD_CMPNY) 
	throws GlobalNoRecordFoundException {
		
		Debug.print("ApFindSupplierControllerBean getApSplSizeByCriteria");
		
		LocalApSupplierHome apSupplierHome = null;
		
		//initialized EJB Home
		
		try {
			
			apSupplierHome = (LocalApSupplierHome)EJBHomeFactory.
			lookUpLocalHome(LocalApSupplierHome.JNDI_NAME, LocalApSupplierHome.class);
			
		} catch (NamingException ex) 	{
			
			throw new EJBException(ex.getMessage());
			
		}
		
		try { 
			
			StringBuffer jbossQl = new StringBuffer();
			jbossQl.append("SELECT DISTINCT OBJECT(spl) FROM ApSupplier spl ");
			
			boolean firstArgument = true;
			short ctr = 0;
			int criteriaSize = criteria.size();
			
			if (adBrnchList.isEmpty()) {
				
			}
			else {
				
				jbossQl.append(", in (spl.adBranchSuppliers) bspl WHERE bspl.adBranch.brCode in (0");
				
				boolean firstLoop = true;
				
				Iterator i = adBrnchList.iterator();
				
				while(i.hasNext()) {
					
					if(firstLoop == false) { 
						jbossQl.append(", "); 
					}
					else { 
						firstLoop = false; 
					}
					
					AdBranchDetails mdetails = (AdBranchDetails) i.next();
					
					jbossQl.append(mdetails.getBrCode());
					
				}
				
				jbossQl.append(") ");
				
				firstArgument = false;
				
			}                                      		  
			
			Object obj[] = null;		      
			
			// Allocate the size of the object parameter
			
			
			if (criteria.containsKey("supplierCode")) {
				
				criteriaSize--;
				
			} 
			
			if (criteria.containsKey("name")) {
				
				criteriaSize--;
				
			} 
			
			if (criteria.containsKey("email")) {
				
				criteriaSize--;
				
			}   	  
			
			obj = new Object[criteriaSize];    
			
			if (criteria.containsKey("supplierCode")) {
				
				if (!firstArgument) {
					
					jbossQl.append("AND ");	
					
				} else {
					
					firstArgument = false;
					jbossQl.append("WHERE ");
					
				}
				
				jbossQl.append("spl.splSupplierCode LIKE '%" + (String)criteria.get("supplierCode") + "%' ");
				
			}
			
			if (criteria.containsKey("name")) {
				
				if (!firstArgument) {
					jbossQl.append("AND ");
				} else {
					firstArgument = false;
					jbossQl.append("WHERE ");
				}
				
				jbossQl.append("spl.splName LIKE '%" + (String)criteria.get("name") + "%' ");		
				
			} 
			
			if (criteria.containsKey("email")) {
				
				if (!firstArgument) {
					jbossQl.append("AND ");
				} else {
					firstArgument = false;
					jbossQl.append("WHERE ");
				}
				
				jbossQl.append("spl.splEmail LIKE '%" + (String)criteria.get("email") + "%' ");
				
				
			}	
			
			if (criteria.containsKey("supplierType")) {
				
				if (!firstArgument) {		       	  	
					jbossQl.append("AND ");		       	     
				} else {		       	  	
					firstArgument = false;
					jbossQl.append("WHERE ");		       	  	 
				}
				
				jbossQl.append("spl.apSupplierType.stName=?" + (ctr+1) + " ");
				obj[ctr] = (String)criteria.get("supplierType");
				ctr++;
				
			}	
			
			if (criteria.containsKey("supplierClass")) {
				
				if (!firstArgument) {		       	  	
					jbossQl.append("AND ");		       	     
				} else {		       	  	
					firstArgument = false;
					jbossQl.append("WHERE ");		       	  	 
				}
				
				jbossQl.append("spl.apSupplierClass.scName=?" + (ctr+1) + " ");
				obj[ctr] = (String)criteria.get("supplierClass");
				ctr++;
				
			}
			
			if (criteria.containsKey("enable") &&
					criteria.containsKey("disable")) {
				
				if (!firstArgument) {
					jbossQl.append("AND ");
				} else {
					firstArgument = false;
					jbossQl.append("WHERE ");
				}
				
				jbossQl.append("(spl.splEnable=?" + (ctr+1) + " OR ");
				obj[ctr] = new Byte(EJBCommon.TRUE);
				ctr++;
				
				jbossQl.append("spl.splEnable=?" + (ctr+1) + ") ");
				obj[ctr] = new Byte(EJBCommon.FALSE);
				ctr++;
				
			} else {
				
				if (criteria.containsKey("enable")) {
					
					if (!firstArgument) {
						jbossQl.append("AND ");
					} else {
						firstArgument = false;
						jbossQl.append("WHERE ");
					}
					jbossQl.append("spl.splEnable=?" + (ctr+1) + " ");
					obj[ctr] = new Byte(EJBCommon.TRUE);
					ctr++;
					
				} 
				
				if (criteria.containsKey("disable")) {
					
					if (!firstArgument) {
						jbossQl.append("AND ");
					} else {
						firstArgument = false;
						jbossQl.append("WHERE ");
					}
					jbossQl.append("spl.splEnable=?" + (ctr+1) + " ");		      	 		      	 		      	 	      	 
					obj[ctr] = new Byte(EJBCommon.FALSE);
					ctr++;
					
				}	    
				
			}  	      
			
			if (!firstArgument) {
				jbossQl.append("AND ");
			} else {
				firstArgument = false;
				jbossQl.append("WHERE ");
			}
			jbossQl.append("spl.splAdCompany=" + AD_CMPNY + " ");
			
			Collection apSuppliers = apSupplierHome.getSplByCriteria(jbossQl.toString(), obj);	         
			
			if (apSuppliers.size() == 0)
				throw new GlobalNoRecordFoundException();
			
			return new Integer(apSuppliers.size());
			
		} catch (GlobalNoRecordFoundException ex) {
			
			throw ex;
			
		} catch (Exception ex) {
			
			
			ex.printStackTrace();
			throw new EJBException(ex.getMessage());
			
		}
		
	}
	
	// SessionBean methods
	
	/**
	 * @ejb:create-method view-type="remote"
	 **/
	public void ejbCreate() throws CreateException {
		
		Debug.print("ApFindSupplierControllerBean ejbCreate");
		
	}
}
