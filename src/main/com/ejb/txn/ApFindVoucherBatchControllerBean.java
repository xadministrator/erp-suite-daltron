package com.ejb.txn;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.naming.NamingException;

import com.ejb.ap.LocalApVoucherBatch;
import com.ejb.ap.LocalApVoucherBatchHome;
import com.ejb.exception.GlobalNoRecordFoundException;
import com.util.AbstractSessionBean;
import com.util.ApVoucherBatchDetails;
import com.util.Debug;
import com.util.EJBHomeFactory;

/**
 * @ejb:bean name="ApFindVoucherBatchControllerEJB"
 *           display-name="Used for searching voucher batches"
 *           type="Stateless"
 *           view-type="remote"
 *           jndi-name="ejb/ApFindVoucherBatchControllerEJB"
 *
 * @ejb:interface remote-class="com.ejb.txn.ApFindVoucherBatchController"
 *                extends="javax.ejb.EJBObject"
 *
 * @ejb:home remote-class="com.ejb.txn.ApFindVoucherBatchControllerHome"
 *           extends="javax.ejb.EJBHome"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="apuser"
 *                        role-link="apuserlink"
 *
 * @ejb:permission role-name="apuser"
 * 
*/

public class ApFindVoucherBatchControllerBean extends AbstractSessionBean {


   
   /**
   * @ejb:interface-method view-type="remote"
   * @jboss:method-attributes read-only="true"
   **/
   public ArrayList getApVbByCriteria(HashMap criteria, String ORDER_BY, Integer OFFSET, Integer LIMIT, Integer AD_BRNCH, Integer AD_CMPNY)
      throws GlobalNoRecordFoundException {

      Debug.print("ApFindVoucherBatchControllerBean getApVbByCriteria");
      
      LocalApVoucherBatchHome apVoucherBatchHome = null;
      
      // Initialize EJB Home
        
      try {
      	
          apVoucherBatchHome = (LocalApVoucherBatchHome)EJBHomeFactory.
              lookUpLocalHome(LocalApVoucherBatchHome.JNDI_NAME, LocalApVoucherBatchHome.class);
           
      } catch (NamingException ex) {
            
          throw new EJBException(ex.getMessage());
            
      }
            
      ArrayList list = new ArrayList();
      
      StringBuffer jbossQl = new StringBuffer();
      jbossQl.append("SELECT OBJECT(vb) FROM ApVoucherBatch vb ");
      
      boolean firstArgument = true;
      short ctr = 0;
      Object obj[];
      
       // Allocate the size of the object parameter

       
      if (criteria.containsKey("batchName")) {
      	
      	 obj = new Object[(criteria.size()-1) + 2];
      	 
      } else {
      	
      	 obj = new Object[criteria.size() + 2];
	
      }
      
  
         
      
      if (criteria.containsKey("batchName")) {
      	
      	 if (!firstArgument) {
      	 
      	    jbossQl.append("AND ");	
      	 	
         } else {
         	
         	firstArgument = false;
         	jbossQl.append("WHERE ");
         	
         }
         
      	 jbossQl.append("vb.vbName LIKE '%" + (String)criteria.get("batchName") + "%' ");
      	 
      }
      	 
        
      if (criteria.containsKey("status")) {
	      	
	  	 if (!firstArgument) {
	  	 	jbossQl.append("AND ");
	  	 } else {
	  	 	firstArgument = false;
	  	 	jbossQl.append("WHERE ");
	  	 }
	  	 jbossQl.append("vb.vbStatus=?" + (ctr+1) + " ");
	  	 obj[ctr] = (String)criteria.get("status");
	  	 ctr++;
	  }  
	  
	  if (criteria.containsKey("dateCreated")) {
	      	
	  	 if (!firstArgument) {
	  	 	jbossQl.append("AND ");
	  	 } else {
	  	 	firstArgument = false;
	  	 	jbossQl.append("WHERE ");
	  	 }
	  	 jbossQl.append("vb.vbDateCreated=?" + (ctr+1) + " ");
	  	 obj[ctr] = (Date)criteria.get("dateCreated");
	  	 ctr++;
	  } 
	  
	  if (!firstArgument) {
	      jbossQl.append("AND ");
	  } else {
	      firstArgument = false;
	      jbossQl.append("WHERE ");
	  }
	  jbossQl.append("vb.vbAdBranch=" + AD_BRNCH + " ");
	  
	  if (!firstArgument) {
  	 	jbossQl.append("AND ");
  	  } else {
  	 	firstArgument = false;
  	 	jbossQl.append("WHERE ");
  	  }
  	  jbossQl.append("vb.vbAdCompany=" + AD_CMPNY + " ");
	      
         
      String orderBy = null;
	      
	  if (ORDER_BY.equals("BATCH NAME")) {
	
	  	  orderBy = "vb.vbName";
	  	  
	  } else if (ORDER_BY.equals("DATE CREATED")) {
	
	  	  orderBy = "vb.vbDateCreated";
	  	
	  }
	  
	  if (orderBy != null) {
	  
	  	jbossQl.append("ORDER BY " + orderBy);
	  	
	  }
               
      jbossQl.append(" OFFSET ?" + (ctr + 1));
      obj[ctr] = OFFSET;
      ctr++;
      
      jbossQl.append(" LIMIT ?" + (ctr + 1));
      obj[ctr] = LIMIT;
      
      System.out.println("QL + " + jbossQl);
      
      

      Collection apVoucherBatches = null;
      
      try {
      	
         apVoucherBatches = apVoucherBatchHome.getVbByCriteria(jbossQl.toString(), obj);
         
      } catch (Exception ex) {
      	
      	 throw new EJBException(ex.getMessage());
      	 
      }
      
      if (apVoucherBatches.isEmpty())
         throw new GlobalNoRecordFoundException();
         
      Iterator i = apVoucherBatches.iterator();
      while (i.hasNext()) {
      	      	
         LocalApVoucherBatch apVoucherBatch = (LocalApVoucherBatch) i.next();
         
         ApVoucherBatchDetails details = new ApVoucherBatchDetails();
         
         details.setVbCode(apVoucherBatch.getVbCode());
         details.setVbName(apVoucherBatch.getVbName());
         details.setVbDescription(apVoucherBatch.getVbDescription());
         details.setVbStatus(apVoucherBatch.getVbStatus());
         details.setVbDateCreated(apVoucherBatch.getVbDateCreated());
         details.setVbCreatedBy(apVoucherBatch.getVbCreatedBy());
         
         list.add(details);
      	
      }
         
      return list;
  
   }
   
   /**
    * @ejb:interface-method view-type="remote"
    * @jboss:method-attributes read-only="true"
    **/
    public Integer getApVbSizeByCriteria(HashMap criteria, Integer AD_BRNCH, Integer AD_CMPNY)
       throws GlobalNoRecordFoundException {

       Debug.print("ApFindVoucherBatchControllerBean getApVbSizeByCriteria");
       
       LocalApVoucherBatchHome apVoucherBatchHome = null;
       
       // Initialize EJB Home
         
       try {
       	
           apVoucherBatchHome = (LocalApVoucherBatchHome)EJBHomeFactory.
               lookUpLocalHome(LocalApVoucherBatchHome.JNDI_NAME, LocalApVoucherBatchHome.class);
            
       } catch (NamingException ex) {
             
           throw new EJBException(ex.getMessage());
             
       }
             
       StringBuffer jbossQl = new StringBuffer();
       jbossQl.append("SELECT OBJECT(vb) FROM ApVoucherBatch vb ");
       
       boolean firstArgument = true;
       short ctr = 0;
       Object obj[];
       
        // Allocate the size of the object parameter

        
       if (criteria.containsKey("batchName")) {
       	
       	 obj = new Object[criteria.size()-1];
       	 
       } else {
       	
       	 obj = new Object[criteria.size()];
 	
       }
       
   
          
       
       if (criteria.containsKey("batchName")) {
       	
       	 if (!firstArgument) {
       	 
       	    jbossQl.append("AND ");	
       	 	
          } else {
          	
          	firstArgument = false;
          	jbossQl.append("WHERE ");
          	
          }
          
       	 jbossQl.append("vb.vbName LIKE '%" + (String)criteria.get("batchName") + "%' ");
       	 
       }
       	 
         
       if (criteria.containsKey("status")) {
 	      	
 	  	 if (!firstArgument) {
 	  	 	jbossQl.append("AND ");
 	  	 } else {
 	  	 	firstArgument = false;
 	  	 	jbossQl.append("WHERE ");
 	  	 }
 	  	 jbossQl.append("vb.vbStatus=?" + (ctr+1) + " ");
 	  	 obj[ctr] = (String)criteria.get("status");
 	  	 ctr++;
 	  }  
 	  
 	  if (criteria.containsKey("dateCreated")) {
 	      	
 	  	 if (!firstArgument) {
 	  	 	jbossQl.append("AND ");
 	  	 } else {
 	  	 	firstArgument = false;
 	  	 	jbossQl.append("WHERE ");
 	  	 }
 	  	 jbossQl.append("vb.vbDateCreated=?" + (ctr+1) + " ");
 	  	 obj[ctr] = (Date)criteria.get("dateCreated");
 	  	 ctr++;
 	  } 
 	  
 	  if (!firstArgument) {
 	      jbossQl.append("AND ");
 	  } else {
 	      firstArgument = false;
 	      jbossQl.append("WHERE ");
 	  }
 	  jbossQl.append("vb.vbAdBranch=" + AD_BRNCH + " ");
 	  
 	  if (!firstArgument) {
   	 	jbossQl.append("AND ");
   	  } else {
   	 	firstArgument = false;
   	 	jbossQl.append("WHERE ");
   	  }
   	  jbossQl.append("vb.vbAdCompany=" + AD_CMPNY + " ");
 	   
       Collection apVoucherBatches = null;
       
       try {
       	
          apVoucherBatches = apVoucherBatchHome.getVbByCriteria(jbossQl.toString(), obj);
          
       } catch (Exception ex) {
       	
       	 throw new EJBException(ex.getMessage());
       	 
       }
       
       if (apVoucherBatches.isEmpty())
          throw new GlobalNoRecordFoundException();
          
       return new Integer(apVoucherBatches.size());
   
    }
   
   // SessionBean methods

   /**
    * @ejb:create-method view-type="remote"
    **/
   public void ejbCreate() throws CreateException {

      Debug.print("ApVoucherBatchControllerBean ejbCreate");
      
   }

   // private methods

     
}
