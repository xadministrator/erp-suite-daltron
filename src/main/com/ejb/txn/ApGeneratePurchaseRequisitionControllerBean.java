/*
 * ApGeneratePurchaseRequisitionControllerBean.java
 *
 * Created on April 12, 2006 1:00 PM
 *
 * @author  Aliza D.J. Anos
 */

package com.ejb.txn;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;
import javax.naming.NamingException;

import com.ejb.ad.LocalAdBranch;
import com.ejb.ad.LocalAdBranchDocumentSequenceAssignment;
import com.ejb.ad.LocalAdBranchDocumentSequenceAssignmentHome;
import com.ejb.ad.LocalAdBranchHome;
import com.ejb.ad.LocalAdBranchItemLocation;
import com.ejb.ad.LocalAdBranchItemLocationHome;
import com.ejb.ad.LocalAdCompany;
import com.ejb.ad.LocalAdCompanyHome;
import com.ejb.ad.LocalAdLookUpValue;
import com.ejb.ad.LocalAdLookUpValueHome;
import com.ejb.ad.LocalAdPreference;
import com.ejb.ad.LocalAdPreferenceHome;
import com.ejb.ap.LocalApCanvass;
import com.ejb.ap.LocalApCanvassHome;
import com.ejb.ap.LocalApPurchaseOrderLine;
import com.ejb.ap.LocalApPurchaseOrderLineHome;
import com.ejb.ap.LocalApPurchaseRequisition;
import com.ejb.ap.LocalApPurchaseRequisitionHome;
import com.ejb.ap.LocalApPurchaseRequisitionLine;
import com.ejb.ap.LocalApPurchaseRequisitionLineHome;
import com.ejb.ap.LocalApSupplier;
import com.ejb.ap.LocalApTaxCode;
import com.ejb.ap.LocalApTaxCodeHome;
import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.gl.LocalAdDocumentSequenceAssignment;
import com.ejb.gl.LocalAdDocumentSequenceAssignmentHome;
import com.ejb.gl.LocalGlFunctionalCurrency;
import com.ejb.gl.LocalGlFunctionalCurrencyHome;
import com.ejb.inv.LocalInvCosting;
import com.ejb.inv.LocalInvCostingHome;
import com.ejb.inv.LocalInvItem;
import com.ejb.inv.LocalInvItemHome;
import com.ejb.inv.LocalInvItemLocation;
import com.ejb.inv.LocalInvItemLocationHome;
import com.ejb.inv.LocalInvUnitOfMeasure;
import com.ejb.inv.LocalInvUnitOfMeasureConversion;
import com.ejb.inv.LocalInvUnitOfMeasureConversionHome;
import com.util.AbstractSessionBean;
import com.util.AdBranchDetails;
import com.util.AdModBranchItemLocationDetails;
import com.util.ApModPurchaseRequisitionDetails;
import com.util.ApModPurchaseRequisitionLineDetails;
import com.util.Debug;
import com.util.EJBCommon;
import com.util.EJBHomeFactory;

/**
 * @ejb:bean name="ApGeneratePurchaseRequisitionControllerEJB"
 *           display-name="Used for generating purchase requisitions"
 *           type="Stateless"
 *           view-type="remote"
 *           jndi-name="ejb/ApGeneratePurchaseRequisitionControllerEJB"
 *
 * @ejb:interface remote-class="com.ejb.txn.ApGeneratePurchaseRequisitionController"
 *                extends="javax.ejb.EJBObject"
 *
 * @ejb:home remote-class="com.ejb.txn.ApGeneratePurchaseRequisitionControllerHome"
 *           extends="javax.ejb.EJBHome"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="apuser"
 *                        role-link="apuserlink"
 *
 * @ejb:permission role-name="apuser"
 * 
*/

public class ApGeneratePurchaseRequisitionControllerBean extends AbstractSessionBean {
	
	/**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList getAdBrAll(Integer AD_CMPNY) {
        
        Debug.print("ApGeneratePurchaseRequisitionControllerBean getAdBrAll");
        
        LocalAdBranchHome AdBranchHome = null;
        Collection AdBranches = null;
        ArrayList list = new ArrayList();
        
        // Initialize EJB Home
        
        try {
            
            AdBranchHome = (LocalAdBranchHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdBranchHome.JNDI_NAME, LocalAdBranchHome.class);
            
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {
            
            AdBranches = AdBranchHome.findBrAll(AD_CMPNY);            
            
            if (AdBranches.isEmpty()) {
                
                return null;
                
            }
            
            Iterator i = AdBranches.iterator();
            
            while (i.hasNext()) {
                
                LocalAdBranch AdBranch = (LocalAdBranch)i.next();	
                String details = AdBranch.getBrName();
                
                list.add(details);
                
            }
            
            return list;
            
        } catch (Exception ex) {
            
            Debug.printStackTrace(ex);
            throw new EJBException(ex.getMessage());
            
        }
        
    }
	
	/**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
	public ArrayList getApGenPRLinesByCriteria(HashMap criteria,String ORDER_BY, ArrayList branchList, Integer AD_CMPNY)
	throws GlobalNoRecordFoundException {
		
		Debug.print("ApGeneratePurchaseRequisitionControllerBean getApGenPRLinesByCriteria");
		
		LocalAdBranchItemLocationHome adBranchItemLocationHome = null;
		LocalInvCostingHome invReorderItemCostHome = null;
		LocalAdPreferenceHome adPreferenceHome = null;
		LocalAdCompanyHome adCompanyHome = null;
		LocalAdBranchHome adBranchHome = null;
		
		ArrayList list = new ArrayList();
		
		// Initialize EJB Home
		
		try {
			
			invReorderItemCostHome = (LocalInvCostingHome)EJBHomeFactory.
				lookUpLocalHome(LocalInvCostingHome.JNDI_NAME, LocalInvCostingHome.class);			
			adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);
			adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
			adBranchItemLocationHome = (LocalAdBranchItemLocationHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdBranchItemLocationHome.JNDI_NAME, LocalAdBranchItemLocationHome.class); 
			adBranchHome = (LocalAdBranchHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdBranchHome.JNDI_NAME, LocalAdBranchHome.class);
			
		} catch (NamingException ex) {
			
			throw new EJBException(ex.getMessage());
			
		}
		
		try { 
			
			LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);
			
			LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
			
			StringBuffer jbossQl = new StringBuffer();
			jbossQl.append("SELECT OBJECT(bil) FROM AdBranchItemLocation bil ");
			
			boolean firstArgument = true;
			short ctr = 0;
			int criteriaSize = criteria.size();	      
			
			Object obj[];
			
			if (branchList.isEmpty()) {
				throw new GlobalNoRecordFoundException();
				
			}
			else {
				
				jbossQl.append(" WHERE bil.adBranch.brCode in (");
				
				boolean firstLoop = true;
				
				Iterator j = branchList.iterator();
				
				while(j.hasNext()) {
					
					if(firstLoop == false) { 
						jbossQl.append(", "); 
					}
					else { 
						firstLoop = false; 
					}
										
					LocalAdBranch adBranch = adBranchHome.findByBrName((String) j.next(), AD_CMPNY);
					jbossQl.append(adBranch.getBrCode());
					
				}
				
				jbossQl.append(") ");
				
				firstArgument = false;
			}
				
			// Allocate the size of the object parameter
			
			if (criteria.containsKey("itemName")) {
				
				criteriaSize--;
				
			}
			
			obj = new Object[criteriaSize];
			
			if (criteria.containsKey("itemName")) {
				
				if (!firstArgument) {
					
					jbossQl.append("AND ");	
					
				} else {
					
					firstArgument = false;
					jbossQl.append("WHERE ");
					
				}
				
				jbossQl.append("bil.invItemLocation.invItem.iiName  LIKE '%" + (String)criteria.get("itemName") + "%' ");
				
			}
			
			if (criteria.containsKey("itemClass")) {
				
				if (!firstArgument) {
					
					jbossQl.append("AND ");	
					
				} else {
					
					firstArgument = false;
					jbossQl.append("WHERE ");
					
				}
				
				jbossQl.append("bil.invItemLocation.invItem.iiClass=?" + (ctr+1) + " ");
				obj[ctr] = (String)criteria.get("itemClass");
				ctr++;
				
			}
			
			if (criteria.containsKey("itemCategory")) {
				
				if (!firstArgument) {
					
					jbossQl.append("AND ");	
					
				} else {
					
					firstArgument = false;
					jbossQl.append("WHERE ");
					
				}
				
				jbossQl.append("bil.invItemLocation.invItem.iiAdLvCategory=?" + (ctr+1) + " ");
				obj[ctr] = (String)criteria.get("itemCategory");
				ctr++;
				
			}	
			
			if (!firstArgument) {
				
				jbossQl.append("AND ");
				
			} else {
				
				firstArgument = false;
				jbossQl.append("WHERE ");
				
			}
			
			jbossQl.append("bil.bilReorderPoint > 0 AND bil.bilAdCompany=" + AD_CMPNY + " ");
			
			String orderBy = null;
			
			if (ORDER_BY.equals("ITEM NAME")) {
				
				orderBy = "bil.invItemLocation.invItem.iiName";
				
			} else if (ORDER_BY.equals("ITEM DESCRIPTION")) {
				
				orderBy = "bil.invItemLocation.invItem.iiDescription";
				
			} else if (ORDER_BY.equals("ITEM CATEGORY")) {
				
				orderBy = "bil.invItemLocation.invItem.iiAdLvCategory";
				
			} else if (ORDER_BY.equals("ITEM CLASS")) {
				
				orderBy = "bil.invItemLocation.invItem.iiClass";
				
			}
			
			if (orderBy != null) {
				
				jbossQl.append("ORDER BY " + orderBy);
				
			}
			
			Collection adBranchItemLocations = null;
			
			try {
				
				adBranchItemLocations = adBranchItemLocationHome.getBilByCriteria(jbossQl.toString(), obj);
				
			} catch (FinderException ex) {
				
				throw new GlobalNoRecordFoundException();
				
			}
			
			Iterator i = adBranchItemLocations.iterator();
			
			while (i.hasNext()) {
				
				LocalAdBranchItemLocation adBranchItemLocation = (LocalAdBranchItemLocation) i.next();
				
				AdModBranchItemLocationDetails details = new AdModBranchItemLocationDetails();
				
				details.setBilIlIiName(adBranchItemLocation.getInvItemLocation().getInvItem().getIiName());
				details.setBilIlIiDescription(adBranchItemLocation.getInvItemLocation().getInvItem().getIiDescription());
				details.setBilIlIiClass(adBranchItemLocation.getInvItemLocation().getInvItem().getIiClass());
				details.setBilIlLocName(adBranchItemLocation.getInvItemLocation().getInvLocation().getLocName());
				details.setBilReorderPoint(adBranchItemLocation.getBilReorderPoint());
				details.setBilReorderQuantity(adBranchItemLocation.getBilReorderQuantity());
				details.setBilBrName(adBranchItemLocation.getAdBranch().getBrBranchCode());
				details.setBilBrCode(adBranchItemLocation.getAdBranch().getBrCode());
				
				double quantity = 0d;
				
				try {
					
					LocalInvCosting invReorderItemCost = 
						invReorderItemCostHome.getByMaxCstDateToLongAndMaxCstLineNumberAndIiNameAndLocName(
								details.getBilIlIiName(),details.getBilIlLocName(), adBranchItemLocation.getAdBranch().getBrCode(), AD_CMPNY);
					
					quantity = invReorderItemCost.getCstRemainingQuantity();
					
					
				} catch (FinderException ex){
					
					quantity = 0;
					
				}
				details.setBilQuantityOnHand(quantity);
				details.setBilQuantity(adBranchItemLocation.getBilReorderQuantity() - quantity);
				if (quantity <= details.getBilReorderPoint()) {
					
					list.add(details);
					
				}
				
			}
			
			if(list.isEmpty() || list.size() == 0) {
				
				throw new GlobalNoRecordFoundException();
				
			}
			
			return list;	   
			
		} catch (GlobalNoRecordFoundException ex) {
			
			throw ex;
			
		} catch (Exception ex) {
			
			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());
			
		}
		
	}
	
	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public ArrayList getAdLvInvItemCategoryAll(Integer AD_CMPNY) {
		
		Debug.print("ApGeneratePurchaseRequisitionControllerBean getAdLvInvItemCategoryAll");
		
		LocalAdLookUpValueHome adLookUpValueHome = null;               
		
		ArrayList list = new ArrayList();
		
		// Initialize EJB Home
		
		try {
			
			adLookUpValueHome = (LocalAdLookUpValueHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdLookUpValueHome.JNDI_NAME, LocalAdLookUpValueHome.class);
			
		} catch (NamingException ex) {
			
			throw new EJBException(ex.getMessage());
			
		}        
		
		try {
			
			Collection adLookUpValues = adLookUpValueHome.findByLuName("INV ITEM CATEGORY", AD_CMPNY);
			
			Iterator i = adLookUpValues.iterator();
			
			while (i.hasNext()) {
				
				LocalAdLookUpValue adLookUpValue = (LocalAdLookUpValue)i.next();
				
				list.add(adLookUpValue.getLvName());
				
			}
			
			return list;
			
		} catch (Exception ex) {
			
			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());
			
		}
		
	}	
	
	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public Integer generateApPurchaseRequisition(ApModPurchaseRequisitionDetails mdetails, ArrayList prlList, Integer AD_BRNCH, Integer AD_CMPNY) {
		
		Debug.print("ApGeneratePurchaseRequisitionControllerBean generateApPurchaseRequisition");
		
		LocalApPurchaseRequisitionHome apPurchaseRequisitionHome = null;
		LocalApPurchaseRequisitionLineHome apPurchaseRequisitionLineHome = null;
		LocalInvItemLocationHome invItemLocationHome = null;
		LocalApTaxCodeHome apTaxCodeHome = null;
		LocalGlFunctionalCurrencyHome glFunctionalCurrencyHome = null;
		LocalAdBranchDocumentSequenceAssignmentHome adBranchDocumentSequenceAssignmentHome = null;
		LocalAdDocumentSequenceAssignmentHome adDocumentSequenceAssignmentHome = null;
		LocalAdPreferenceHome adPreferenceHome = null;
		LocalApPurchaseOrderLineHome apPurchaseOrderLineHome = null;
		LocalApCanvassHome apCanvassHome = null;
		LocalAdBranchItemLocationHome adBranchItemLocationHome = null;
		
		//Initialize EJB Home
		
		try {
			
			apPurchaseRequisitionHome = (LocalApPurchaseRequisitionHome)EJBHomeFactory.
				lookUpLocalHome(LocalApPurchaseRequisitionHome.JNDI_NAME, LocalApPurchaseRequisitionHome.class);
			apPurchaseRequisitionLineHome = (LocalApPurchaseRequisitionLineHome)EJBHomeFactory.
				lookUpLocalHome(LocalApPurchaseRequisitionLineHome.JNDI_NAME, LocalApPurchaseRequisitionLineHome.class);
			invItemLocationHome = (LocalInvItemLocationHome)EJBHomeFactory.
				lookUpLocalHome(LocalInvItemLocationHome.JNDI_NAME, LocalInvItemLocationHome.class);
			apTaxCodeHome = (LocalApTaxCodeHome)EJBHomeFactory.
				lookUpLocalHome(LocalApTaxCodeHome.JNDI_NAME, LocalApTaxCodeHome.class);
			glFunctionalCurrencyHome = (LocalGlFunctionalCurrencyHome)EJBHomeFactory.
				lookUpLocalHome(LocalGlFunctionalCurrencyHome.JNDI_NAME, LocalGlFunctionalCurrencyHome.class);
			adBranchDocumentSequenceAssignmentHome = (LocalAdBranchDocumentSequenceAssignmentHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdBranchDocumentSequenceAssignmentHome.JNDI_NAME, LocalAdBranchDocumentSequenceAssignmentHome.class);
			adDocumentSequenceAssignmentHome = (LocalAdDocumentSequenceAssignmentHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdDocumentSequenceAssignmentHome.JNDI_NAME, LocalAdDocumentSequenceAssignmentHome.class);
			adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);
			apPurchaseOrderLineHome = (LocalApPurchaseOrderLineHome)EJBHomeFactory.
				lookUpLocalHome(LocalApPurchaseOrderLineHome.JNDI_NAME, LocalApPurchaseOrderLineHome.class);
			apCanvassHome = (LocalApCanvassHome)EJBHomeFactory.
				lookUpLocalHome(LocalApCanvassHome.JNDI_NAME, LocalApCanvassHome.class);
			adBranchItemLocationHome = (LocalAdBranchItemLocationHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdBranchItemLocationHome.JNDI_NAME, LocalAdBranchItemLocationHome.class);
			
		} catch (NamingException ex) {
			
			throw new EJBException(ex.getMessage());
			
		}     
		
		try {
			
			// validate if document number is unique document number is automatic then set next sequence
        	
	    	LocalAdBranchDocumentSequenceAssignment adBranchDocumentSequenceAssignment = null;
 			LocalAdDocumentSequenceAssignment adDocumentSequenceAssignment = null;
				
 			try {
 				
 				adDocumentSequenceAssignment = adDocumentSequenceAssignmentHome.findByDcName("AP PURCHASE REQUISITION", AD_CMPNY);
 				
 			} catch (FinderException ex) {
 				
 			}
 			
 			try {
 				
 				adBranchDocumentSequenceAssignment = adBranchDocumentSequenceAssignmentHome.findBdsByDsaCodeAndBrCode(adDocumentSequenceAssignment.getDsaCode(), AD_BRNCH, AD_CMPNY);
 				
 			} catch (FinderException ex) {
 				
 			}
    		
	        if (adDocumentSequenceAssignment.getAdDocumentSequence().getDsNumberingType() == 'A') {
	            	
	            while (true) {
	            	
	            	if (adBranchDocumentSequenceAssignment == null || adBranchDocumentSequenceAssignment.getBdsNextSequence() == null) {
	            		
	            		try {
		            		
		            		apPurchaseRequisitionHome.findByPrNumberAndBrCode(adDocumentSequenceAssignment.getDsaNextSequence(), AD_BRNCH, AD_CMPNY);		            		
		            		adDocumentSequenceAssignment.setDsaNextSequence(EJBCommon.incrementStringNumber(adDocumentSequenceAssignment.getDsaNextSequence()));	
		            		
		            	} catch (FinderException ex) {
		            		
		            		mdetails.setPrNumber(adDocumentSequenceAssignment.getDsaNextSequence());
		            		adDocumentSequenceAssignment.setDsaNextSequence(EJBCommon.incrementStringNumber(adDocumentSequenceAssignment.getDsaNextSequence()));	
				            break;
		            		
		            	}
	            		
	            	} else {
	            		
	            		try {
		            		
	            			apPurchaseRequisitionHome.findByPrNumberAndBrCode(adBranchDocumentSequenceAssignment.getBdsNextSequence(), AD_BRNCH, AD_CMPNY);		            		
		            		adBranchDocumentSequenceAssignment.setBdsNextSequence(EJBCommon.incrementStringNumber(adBranchDocumentSequenceAssignment.getBdsNextSequence()));	
		            		
		            	} catch (FinderException ex) {
		            		
		            		mdetails.setPrNumber(adBranchDocumentSequenceAssignment.getBdsNextSequence());
		            		adBranchDocumentSequenceAssignment.setBdsNextSequence(EJBCommon.incrementStringNumber(adBranchDocumentSequenceAssignment.getBdsNextSequence()));	
				            break;
		            		
		            	}
	            		
	            	}
	            		
	            }
	            
	        }
	        
	        //create purchase requisition
	        LocalApPurchaseRequisition apPurchaseRequisition = apPurchaseRequisitionHome.create(mdetails.getPrDescription(), 
					mdetails.getPrNumber(), mdetails.getPrDate(), mdetails.getPrDeliveryPeriod(), mdetails.getPrReferenceNumber(), mdetails.getPrConversionDate(), 
					mdetails.getPrConversionRate(), null, EJBCommon.FALSE, EJBCommon.FALSE, null, EJBCommon.FALSE, EJBCommon.FALSE, null, null,
					mdetails.getPrCreatedBy(), mdetails.getPrDateCreated(), mdetails.getPrCreatedBy(), mdetails.getPrDateCreated(), 
					null, null, null, null, 
					null, null, null, null,
					mdetails.getPrTagStatus(),mdetails.getPrType(),
					null,null,null,null,
					AD_BRNCH, AD_CMPNY);
	        
	        LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);
	        	
			LocalApTaxCode apTaxCode = apTaxCodeHome.findByTcName(adPreference.getPrfApDefaultPrTax(), AD_CMPNY);
			apPurchaseRequisition.setApTaxCode(apTaxCode);
			
			LocalGlFunctionalCurrency glFunctionalCurrency = glFunctionalCurrencyHome.findByFcName(adPreference.getPrfApDefaultPrCurrency(), AD_CMPNY);
			apPurchaseRequisition.setGlFunctionalCurrency(glFunctionalCurrency);
			
			//add purchase requisition lines
			
			Iterator i = prlList.iterator();
			
			while(i.hasNext()) {
				
				ApModPurchaseRequisitionLineDetails prlDetails = (ApModPurchaseRequisitionLineDetails)i.next();
				
				LocalApPurchaseRequisitionLine apPurchaseRequisitionLine = apPurchaseRequisitionLineHome.create(prlDetails.getPrlLine(), 0d, 0d, AD_CMPNY);
				apPurchaseRequisition.addApPurchaseRequisitionLine(apPurchaseRequisitionLine);
				
				LocalInvItemLocation invItemLocation = invItemLocationHome.findByIiNameAndLocName(prlDetails.getPrlIlIiName(), prlDetails.getPrlIlLocName(), AD_CMPNY);
				invItemLocation.addApPurchaseRequisitionLine(apPurchaseRequisitionLine);
				
				apPurchaseRequisitionLine.setPrlQuantity(prlDetails.getPrlQuantity());
				
				LocalInvUnitOfMeasure invUnitOfMeasure = invItemLocation.getInvItem().getInvUnitOfMeasure();
				invUnitOfMeasure.addApPurchaseRequisitionLine(apPurchaseRequisitionLine);
				
				//add canvass lines
				double REORDR_QTY = apPurchaseRequisitionLine.getPrlQuantity();
												
				LocalInvItem invItem = invItemLocation.getInvItem();
				
				double II_UNT_CST = 0d;
				
				if (invItem.getApSupplier() != null) {
					
					// if item has supplier retrieve last purchase price
					
					II_UNT_CST = this.getInvLastPurchasePriceBySplSupplierCode(invItemLocation, invItem.getApSupplier().getSplSupplierCode(), AD_BRNCH, AD_CMPNY);
					
				} else {
					
					// if item has no supplier retrieve last purchase price
					
					II_UNT_CST = this.getInvLastPurchasePrice(invItemLocation, AD_BRNCH, AD_CMPNY);
					
					
				}
				
				if (II_UNT_CST == 0) {
					
					II_UNT_CST = invItemLocation.getInvItem().getIiUnitCost();
					
				}
				
				apPurchaseRequisitionLine.setPrlAmount(REORDR_QTY * II_UNT_CST);
				
				//create canvass line for item's default supplier
				
				short CNV_LN = 0;
																
				if (invItemLocation.getInvItem().getApSupplier() != null) {
					
					CNV_LN++;
										
					//create canvass line
					LocalApCanvass apCanvass = apCanvassHome.create(CNV_LN,null, REORDR_QTY, II_UNT_CST, 
							EJBCommon.roundIt(REORDR_QTY * II_UNT_CST, this.getGlFcPrecisionUnit(AD_CMPNY)),
							EJBCommon.TRUE, AD_CMPNY);
					
					LocalApSupplier apSupplier = invItemLocation.getInvItem().getApSupplier();
					apSupplier.addApCanvass(apCanvass);
					apPurchaseRequisitionLine.addApCanvass(apCanvass);
											
				}
				
				CNV_LN++;
				
				//create canvass lines for other suppliers of item
				Collection apPurchaseOrderLines = null;
				
				try {
					
					apPurchaseOrderLines = apPurchaseOrderLineHome.findByPlIlCodeAndPoReceivingAndPoPostedAndBrCode(apPurchaseRequisitionLine.getInvItemLocation().getIlCode(), EJBCommon.TRUE, EJBCommon.TRUE, AD_BRNCH, AD_CMPNY);
					
				} catch(FinderException ex) {
					
				}
				
				if(!apPurchaseOrderLines.isEmpty() && apPurchaseOrderLines.size() > 0) {
					
					Iterator plIter = apPurchaseOrderLines.iterator();
					
					while(plIter.hasNext()) {
						
						LocalApPurchaseOrderLine apPurchaseOrderLine = (LocalApPurchaseOrderLine)plIter.next();
						
						// continue if next purchase order line has existing supplier in canvass
						
						Collection apCanvasses = apCanvassHome.findByPrlCodeAndSplSupplierCode(apPurchaseRequisitionLine.getPrlCode(), 
								apPurchaseOrderLine.getApPurchaseOrder().getApSupplier().getSplSupplierCode(), AD_CMPNY);
						
						if(!apCanvasses.isEmpty()) continue;
						
						// convert purchase order line unit cost
						double POL_UNT_CST = this.convertCostByUom(apPurchaseOrderLine.getInvItemLocation().getInvItem().getIiName(),
								apPurchaseOrderLine.getInvUnitOfMeasure().getUomName(),
								apPurchaseOrderLine.getPlUnitCost(), false, AD_CMPNY);
											
						//create canvass line
						LocalApCanvass apCanvass = apCanvassHome.create(CNV_LN,null, 0d, POL_UNT_CST, 0d, EJBCommon.FALSE, AD_CMPNY);
						
						if (CNV_LN == 1) {
							
							apCanvass.setCnvQuantity(REORDR_QTY);
							apCanvass.setCnvAmount(EJBCommon.roundIt(REORDR_QTY * II_UNT_CST, this.getGlFcPrecisionUnit(AD_CMPNY)));
							apCanvass.setCnvPo(EJBCommon.TRUE);
							
						}
						
						LocalApSupplier apSupplier = apPurchaseOrderLine.getApPurchaseOrder().getApSupplier();
						apSupplier.addApCanvass(apCanvass);
						
						apPurchaseRequisitionLine.addApCanvass(apCanvass);
						
						CNV_LN++;
						
					}
					
				}
				
			}
			
			return apPurchaseRequisition.getPrCode();
			
		} catch (Exception ex) {
			
			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());
			
		}
		
	}
	
	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public short getGlFcPrecisionUnit(Integer AD_CMPNY) {
		
		Debug.print("ApGeneratePurchaseRequisitionControllerBean getGlFcPrecisionUnit");
		
		LocalAdCompanyHome adCompanyHome = null;
		
		
		// Initialize EJB Home
		
		try {
			
			adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);       
			
		} catch (NamingException ex) {
			
			throw new EJBException(ex.getMessage());
			
		}
		
		try {
			
			LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
			
			return  adCompany.getGlFunctionalCurrency().getFcPrecision();
			
		} catch (Exception ex) {
			
			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());
			
		}
		
	}
	
	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public short getInvGpQuantityPrecisionUnit(Integer AD_CMPNY) {
		
		Debug.print("ApGeneratePurchaseRequisitionControllerBean getInvGpQuantityPrecisionUnit");
		
		LocalAdPreferenceHome adPreferenceHome = null;         
		
		// Initialize EJB Home
		
		try {
			
			adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);       
			
		} catch (NamingException ex) {
			
			throw new EJBException(ex.getMessage());
			
		}
		
		
		try {
			
			LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);
			
			return adPreference.getPrfInvQuantityPrecisionUnit();
			
		} catch (Exception ex) {
			
			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());
			
		}
		
	}
	
	//private methods
	
	private double getInvLastPurchasePriceBySplSupplierCode(LocalInvItemLocation invItemLocation, String SPL_SPPLR_CODE, Integer AD_BRNCH, Integer AD_CMPNY) {
		
		Debug.print("ApGeneratePurchaseRequisitionControllerBean getInvLastPurchasePriceBySplSupplierCode");
	    
		LocalApPurchaseOrderLineHome apPurchaseOrderLineHome = null;
        
		// Initialize EJB Home
		
		try {
		    
			apPurchaseOrderLineHome = (LocalApPurchaseOrderLineHome)EJBHomeFactory.
				lookUpLocalHome(LocalApPurchaseOrderLineHome.JNDI_NAME, LocalApPurchaseOrderLineHome.class);   
		       
		} catch (NamingException ex) {
		    
		    throw new EJBException(ex.getMessage());
		    
		}
			
		try {
			
			LocalApPurchaseOrderLine apPurchaseOrderLine = apPurchaseOrderLineHome.
			getByMaxPoDateAndMaxPlCodeAndIiNameAndLocNameAndPoReceivingAndPoPostedAndSplSpplrCode(
					invItemLocation.getInvItem().getIiName(),
					invItemLocation.getInvLocation().getLocName(),
					EJBCommon.TRUE, EJBCommon.TRUE,
					invItemLocation.getInvItem().getApSupplier().getSplSupplierCode(),
					AD_BRNCH, AD_CMPNY);
			
			return this.convertCostByUom(invItemLocation.getInvItem().getIiName(), apPurchaseOrderLine.getInvUnitOfMeasure().getUomName(),
					apPurchaseOrderLine.getPlUnitCost(), false, AD_CMPNY);
		
		} catch(FinderException ex) {
			
			return 0d;
			
		}
		
	}
	
	private double getInvLastPurchasePrice(LocalInvItemLocation invItemLocation, Integer AD_BRNCH, Integer AD_CMPNY) {
		
		Debug.print("ApGeneratePurchaseRequisitionControllerBean getInvLastPurchasePrice");
	    
		LocalApPurchaseOrderLineHome apPurchaseOrderLineHome = null;
        
		// Initialize EJB Home
		
		try {
		    
			apPurchaseOrderLineHome = (LocalApPurchaseOrderLineHome)EJBHomeFactory.
				lookUpLocalHome(LocalApPurchaseOrderLineHome.JNDI_NAME, LocalApPurchaseOrderLineHome.class);   
		       
		} catch (NamingException ex) {
		    
		    throw new EJBException(ex.getMessage());
		    
		}
			
		try {
			
			LocalApPurchaseOrderLine apPurchaseOrderLine = apPurchaseOrderLineHome.
			getByMaxPoDateAndMaxPlCodeAndIiNameAndLocNameAndPoReceivingAndPoPosted(
					invItemLocation.getInvItem().getIiName(),
					invItemLocation.getInvLocation().getLocName(),
					EJBCommon.TRUE, EJBCommon.TRUE,					
					AD_BRNCH, AD_CMPNY);
			
			return this.convertCostByUom(invItemLocation.getInvItem().getIiName(),
					apPurchaseOrderLine.getInvUnitOfMeasure().getUomName(),
					apPurchaseOrderLine.getPlUnitCost(), false, AD_CMPNY);
			
		} catch(FinderException ex) {
			
			return 0d;
			
		}
		
	}
	
	private double convertCostByUom(String II_NM, String UOM_NM, double unitCost, boolean isFromDefault, Integer AD_CMPNY) {
		
		Debug.print("ApGeneratePurchaseRequisitionControllerBean convertCostByUom");		        
	    
		LocalInvItemHome invItemHome = null;
		LocalInvUnitOfMeasureConversionHome invUnitOfMeasureConversionHome = null;
		        
	    // Initialize EJB Home
	    
	    try {
	        
	        invUnitOfMeasureConversionHome = (LocalInvUnitOfMeasureConversionHome)EJBHomeFactory.
				lookUpLocalHome(LocalInvUnitOfMeasureConversionHome.JNDI_NAME, LocalInvUnitOfMeasureConversionHome.class);   
	        invItemHome = (LocalInvItemHome)EJBHomeFactory.
				lookUpLocalHome(LocalInvItemHome.JNDI_NAME, LocalInvItemHome.class);
	           
	    } catch (NamingException ex) {
	        
	        throw new EJBException(ex.getMessage());
	        
	    }
	            
	    try {
	    
	    	LocalInvItem invItem = invItemHome.findByIiName(II_NM, AD_CMPNY);
	        LocalInvUnitOfMeasureConversion invUnitOfMeasureConversion = invUnitOfMeasureConversionHome.findUmcByIiNameAndUomName(II_NM, UOM_NM, AD_CMPNY);
	        LocalInvUnitOfMeasureConversion invDefaultUomConversion = invUnitOfMeasureConversionHome.findUmcByIiNameAndUomName(II_NM, invItem.getInvUnitOfMeasure().getUomName(), AD_CMPNY);
	                    
	        if (isFromDefault) {	        	
	        
	        	return EJBCommon.roundIt(unitCost * invDefaultUomConversion.getUmcConversionFactor() / invUnitOfMeasureConversion.getUmcConversionFactor(), this.getGlFcPrecisionUnit(AD_CMPNY));
	    	
	        } else {
	        	
	        	return EJBCommon.roundIt(unitCost * invUnitOfMeasureConversion.getUmcConversionFactor() / invDefaultUomConversion.getUmcConversionFactor(), this.getGlFcPrecisionUnit(AD_CMPNY));
	        	
	        }
	    	
	    	
	    						    		        		
	    } catch (Exception ex) {
	    	
	    	Debug.printStackTrace(ex);
	    	getSessionContext().setRollbackOnly();
	    	throw new EJBException(ex.getMessage());
	    	
	    }
		
	}
	
	//SessionBean method

    /**
     * @ejb:create-method view-type="remote"
     **/
    public void ejbCreate() throws CreateException {

       Debug.print("ApGeneratePurchaseRequisitionControllerBean ejbCreate");
      
    }
}