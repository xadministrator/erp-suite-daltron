
/*
 * ApGlJournalInterfaceControllerBean.java
 *
 * Created on February 26, 2004, 09:43 AM
 *
 * @author  Dennis M. Hilario
 */

package com.ejb.txn;

import java.util.Collection;
import java.util.Date;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.naming.NamingException;

import com.ejb.ad.LocalAdCompany;
import com.ejb.ad.LocalAdCompanyHome;
import com.ejb.ap.LocalApCheck;
import com.ejb.ap.LocalApCheckHome;
import com.ejb.ap.LocalApDistributionRecord;
import com.ejb.ap.LocalApDistributionRecordHome;
import com.ejb.ap.LocalApVoucher;
import com.ejb.ap.LocalApVoucherHome;
import com.ejb.gl.LocalGlFunctionalCurrency;
import com.ejb.gl.LocalGlFunctionalCurrencyRate;
import com.ejb.gl.LocalGlFunctionalCurrencyRateHome;
import com.ejb.gl.LocalGlJournalInterface;
import com.ejb.gl.LocalGlJournalInterfaceHome;
import com.ejb.gl.LocalGlJournalLineInterface;
import com.ejb.gl.LocalGlJournalLineInterfaceHome;
import com.util.AbstractSessionBean;
import com.util.Debug;
import com.util.EJBCommon;
import com.util.EJBHomeFactory;

/**
 * @ejb:bean name="ApGlJournalInterfaceControllerEJB"
 *           display-name="Used for journal interface run"
 *           type="Stateless"
 *           view-type="remote"
 *           jndi-name="ejb/ApGlJournalInterfaceControllerEJB"
 *
 * @ejb:interface remote-class="com.ejb.txn.ApGlJournalInterfaceController"
 *                extends="javax.ejb.EJBObject"
 *
 * @ejb:home remote-class="com.ejb.txn.ApGlJournalInterfaceControllerHome"
 *           extends="javax.ejb.EJBHome"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="apuser"
 *                        role-link="apuserlink"
 *
 * @ejb:permission role-name="apuser"
 * 
*/

public class ApGlJournalInterfaceControllerBean extends AbstractSessionBean {

    /**
     * @ejb:interface-method view-type="remote"
     **/
    public long executeApGlJriRun(Date JRI_DT_FRM, Date JRI_DT_TO, Integer AD_BRNCH, Integer AD_CMPNY) {
                    
        Debug.print("ApGlJournalInterfaceControllerBean executeApChkPost");
        
        LocalGlJournalInterfaceHome glJournalInterfaceHome = null;
        LocalGlJournalLineInterfaceHome glJournalLineInterfaceHome = null;
        LocalApDistributionRecordHome apDistributionRecordHome = null;        
        LocalApVoucherHome apVoucherHome = null;
        LocalApCheckHome apCheckHome = null;
        LocalAdCompanyHome adCompanyHome = null;
        
        java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat("MM/dd/yyyy HH:mm:ss.SSSS"); 
        long IMPORTED_JOURNALS = 0L;
        short lineNumber = 0;
                       
                
        // Initialize EJB Home
        
        try {
            
            glJournalInterfaceHome = (LocalGlJournalInterfaceHome)EJBHomeFactory.
                lookUpLocalHome(LocalGlJournalInterfaceHome.JNDI_NAME, LocalGlJournalInterfaceHome.class);
            glJournalLineInterfaceHome = (LocalGlJournalLineInterfaceHome)EJBHomeFactory.
                lookUpLocalHome(LocalGlJournalLineInterfaceHome.JNDI_NAME, LocalGlJournalLineInterfaceHome.class);
            apDistributionRecordHome = (LocalApDistributionRecordHome)EJBHomeFactory.
                lookUpLocalHome(LocalApDistributionRecordHome.JNDI_NAME, LocalApDistributionRecordHome.class);
            apVoucherHome = (LocalApVoucherHome)EJBHomeFactory.
                lookUpLocalHome(LocalApVoucherHome.JNDI_NAME, LocalApVoucherHome.class);
            apCheckHome = (LocalApCheckHome)EJBHomeFactory.
                lookUpLocalHome(LocalApCheckHome.JNDI_NAME, LocalApCheckHome.class);    
            adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);    
                
                                    
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }       
        
        try {   
      
            LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
            
            LocalGlFunctionalCurrency glFunctionalCurrency = adCompany.getGlFunctionalCurrency();
                            
            // voucher
            
            Collection apVouchers = apVoucherHome.findPostedVouByVouDebitMemoAndVouDateRange(EJBCommon.FALSE, JRI_DT_FRM, JRI_DT_TO, AD_CMPNY);
            
            Iterator i = apVouchers.iterator();
            
            while (i.hasNext()) {
            	
            	LocalApVoucher apVoucher = (LocalApVoucher)i.next();
            	
            	Collection apDistributionRecords = apDistributionRecordHome.findImportableDrByVouCode(apVoucher.getVouCode(), AD_CMPNY);
            	            	            	
            	if (!apDistributionRecords.isEmpty()) {
            		
            		LocalGlJournalInterface glJournalInterface = 
            		     glJournalInterfaceHome.create(apVoucher.getVouReferenceNumber(),
            		         apVoucher.getVouDescription(), apVoucher.getVouDate(), "VOUCHERS", "ACCOUNTS PAYABLES",
            		         glFunctionalCurrency.getFcName(), null, apVoucher.getVouDocumentNumber(), 
            		         null, 1d, 'N', EJBCommon.FALSE, apVoucher.getApSupplier().getSplTin(), 
							 apVoucher.getApSupplier().getSplName(), null, AD_BRNCH, AD_CMPNY);
            		         
            		IMPORTED_JOURNALS++;
            		lineNumber = 0;
            		
            		Iterator j = apDistributionRecords.iterator();
            	
	            	while (j.hasNext()) {
	            		
	            		LocalApDistributionRecord apDistributionRecord = (LocalApDistributionRecord)j.next();
	            	    
	            	    LocalGlJournalLineInterface glJournalLineInterface = glJournalLineInterfaceHome.create(
	        		    	++lineNumber,
	        		    	apDistributionRecord.getDrDebit(),
	        		    	this.convertForeignToFunctionalCurrency(apVoucher.getGlFunctionalCurrency().getFcCode(),
	        		    	    apVoucher.getGlFunctionalCurrency().getFcName(), 
	        		    	    apVoucher.getVouConversionDate(), apVoucher.getVouConversionRate(), apDistributionRecord.getDrAmount(), AD_CMPNY),
	        		    	apDistributionRecord.getGlChartOfAccount().getCoaAccountNumber(), AD_CMPNY);
	        		    	
	        		    glJournalInterface.addGlJournalLineInterface(glJournalLineInterface);
	        		    	
	        		    apDistributionRecord.setDrImported(EJBCommon.TRUE);	        		    	
	            	    		            			            		
	            			            		
	            	}
	            	
            	}
            	
            }            
            
            // debit memo
            
            apVouchers = apVoucherHome.findPostedVouByVouDebitMemoAndVouDateRange(EJBCommon.TRUE, JRI_DT_FRM, JRI_DT_TO, AD_CMPNY);
            
            i = apVouchers.iterator();
            
            while (i.hasNext()) {
            	
            	LocalApVoucher apVoucher = (LocalApVoucher)i.next();
            	
            	LocalApVoucher apDebitedVoucher = apVoucherHome.findByVouDocumentNumberAndVouDebitMemoAndBrCode(apVoucher.getVouDmVoucherNumber(), EJBCommon.FALSE, AD_BRNCH, AD_CMPNY);
            	
            	Collection apDistributionRecords = apDistributionRecordHome.findImportableDrByVouCode(apVoucher.getVouCode(), AD_CMPNY);
            	            	            	
            	if (!apDistributionRecords.isEmpty()) {
            		
            		LocalGlJournalInterface glJournalInterface = 
            		     glJournalInterfaceHome.create(apVoucher.getVouReferenceNumber(),
            		         apVoucher.getVouDescription(), apVoucher.getVouDate(), "DEBIT MEMOS", "ACCOUNTS PAYABLES",
            		         glFunctionalCurrency.getFcName(), null, apVoucher.getVouDocumentNumber(), 
            		         null, 1d, 'N', EJBCommon.FALSE, apVoucher.getApSupplier().getSplTin(), 
							 apVoucher.getApSupplier().getSplName(), null, AD_BRNCH, AD_CMPNY);
            		         
            		IMPORTED_JOURNALS++;
            		lineNumber = 0;
            		
            		Iterator j = apDistributionRecords.iterator();
            	
	            	while (j.hasNext()) {
	            		
	            		LocalApDistributionRecord apDistributionRecord = (LocalApDistributionRecord)j.next();
	            	    
	            	    LocalGlJournalLineInterface glJournalLineInterface = glJournalLineInterfaceHome.create(
	        		    	++lineNumber,
	        		    	apDistributionRecord.getDrDebit(),
	        		    	this.convertForeignToFunctionalCurrency(apDebitedVoucher.getGlFunctionalCurrency().getFcCode(),
	        		    	    apDebitedVoucher.getGlFunctionalCurrency().getFcName(), 
	        		    	    apDebitedVoucher.getVouConversionDate(), apDebitedVoucher.getVouConversionRate(), apDistributionRecord.getDrAmount(), AD_CMPNY),
	        		    	apDistributionRecord.getGlChartOfAccount().getCoaAccountNumber(), AD_CMPNY);
	        		    	
	        		    glJournalInterface.addGlJournalLineInterface(glJournalLineInterface);
	        		    	
	        		    apDistributionRecord.setDrImported(EJBCommon.TRUE);	        		    	
	            	    		            			            		
	            			            		
	            	}
	            	
            	}
            	
            }
            
            
            // checks
            
            Collection apChecks = apCheckHome.findPostedChkByChkDateRange(JRI_DT_FRM, JRI_DT_TO, AD_CMPNY);
            
            i = apChecks.iterator();
            
            while (i.hasNext()) {
            	
            	LocalApCheck apCheck = (LocalApCheck)i.next();
            	            	
            	Collection apDistributionRecords = apDistributionRecordHome.findImportableDrByDrReversedAndChkCode(EJBCommon.FALSE, apCheck.getChkCode(), AD_CMPNY);
            	            	            	
            	if (!apDistributionRecords.isEmpty()) {
            		
            		LocalGlJournalInterface glJournalInterface = 
            		     glJournalInterfaceHome.create(apCheck.getChkReferenceNumber(),
            		         apCheck.getChkDescription(), apCheck.getChkDate(), "CHECKS", "ACCOUNTS PAYABLES",
            		         glFunctionalCurrency.getFcName(), null, apCheck.getChkDocumentNumber(), 
            		         null, 1d, 'N', EJBCommon.FALSE, apCheck.getApSupplier().getSplTin(), 
							 apCheck.getApSupplier().getSplName(), null, AD_BRNCH, AD_CMPNY);
            		         
            		IMPORTED_JOURNALS++;
            		lineNumber = 0;
            		
            		Iterator j = apDistributionRecords.iterator();
            	
	            	while (j.hasNext()) {
	            		
	            		LocalApDistributionRecord apDistributionRecord = (LocalApDistributionRecord)j.next();
	            		
	            		double JLI_AMNT = 0;
	            		
	            		if (apDistributionRecord.getApAppliedVoucher() != null) {
	            			
	            			LocalApVoucher apVoucher = apDistributionRecord.getApAppliedVoucher().getApVoucherPaymentSchedule().getApVoucher();
	            			
	            			JLI_AMNT = this.convertForeignToFunctionalCurrency(apVoucher.getGlFunctionalCurrency().getFcCode(),
	        		    	    apVoucher.getGlFunctionalCurrency().getFcName(), 
	        		    	    apVoucher.getVouConversionDate(), apVoucher.getVouConversionRate(), apDistributionRecord.getDrAmount(), AD_CMPNY);
	            			
	            		} else {
	            			
	            			JLI_AMNT = this.convertForeignToFunctionalCurrency(apCheck.getGlFunctionalCurrency().getFcCode(),
	        		    	    apCheck.getGlFunctionalCurrency().getFcName(), 
	        		    	    apCheck.getChkConversionDate(), apCheck.getChkConversionRate(), apDistributionRecord.getDrAmount(), AD_CMPNY);
	            			
	            		}
	            	    
	            	    LocalGlJournalLineInterface glJournalLineInterface = glJournalLineInterfaceHome.create(
	        		    	++lineNumber, 
	        		    	apDistributionRecord.getDrDebit(),        		    	
	        		    	JLI_AMNT,
	        		    	apDistributionRecord.getGlChartOfAccount().getCoaAccountNumber(), AD_CMPNY);
	        		    	
	        		    glJournalInterface.addGlJournalLineInterface(glJournalLineInterface);
	        		    	
	        		    apDistributionRecord.setDrImported(EJBCommon.TRUE);	        		    	
	            	    		            			            		
	            			            		
	            	}
	            	
            	}
            	
            }
            
            
            // void/reversed checks
            
            apChecks = apCheckHome.findVoidPostedChkByChkDateRange(JRI_DT_FRM, JRI_DT_TO, AD_CMPNY);
            
            i = apChecks.iterator();
            
            while (i.hasNext()) {
            	
            	LocalApCheck apCheck = (LocalApCheck)i.next();
            	            	
            	Collection apDistributionRecords = apDistributionRecordHome.findImportableDrByDrReversedAndChkCode(EJBCommon.TRUE, apCheck.getChkCode(), AD_CMPNY);
            	            	            	
            	if (!apDistributionRecords.isEmpty()) {
            		
            		LocalGlJournalInterface glJournalInterface = 
            		     glJournalInterfaceHome.create("REVERSED " + apCheck.getChkReferenceNumber(),
            		         apCheck.getChkDescription(), apCheck.getChkDate(), "CHECKS", "ACCOUNTS PAYABLES",
            		         glFunctionalCurrency.getFcName(), null, apCheck.getChkDocumentNumber(), 
            		         null, 1d, 'N', EJBCommon.FALSE, apCheck.getApSupplier().getSplTin(), 
							 apCheck.getApSupplier().getSplName(), null, AD_BRNCH, AD_CMPNY);
            		         
            		IMPORTED_JOURNALS++;
            		lineNumber = 0;
            		
            		Iterator j = apDistributionRecords.iterator();
            	
	            	while (j.hasNext()) {
	            		
	            		LocalApDistributionRecord apDistributionRecord = (LocalApDistributionRecord)j.next();
	            		
	            		double JLI_AMNT = 0;
	            		
	            		if (apDistributionRecord.getApAppliedVoucher() != null) {
	            			
	            			LocalApVoucher apVoucher = apDistributionRecord.getApAppliedVoucher().getApVoucherPaymentSchedule().getApVoucher();
	            			
	            			JLI_AMNT = this.convertForeignToFunctionalCurrency(apVoucher.getGlFunctionalCurrency().getFcCode(),
	        		    	    apVoucher.getGlFunctionalCurrency().getFcName(), 
	        		    	    apVoucher.getVouConversionDate(), apVoucher.getVouConversionRate(), apDistributionRecord.getDrAmount(), AD_CMPNY);
	            			
	            		} else {
	            			
	            			JLI_AMNT = this.convertForeignToFunctionalCurrency(apCheck.getGlFunctionalCurrency().getFcCode(),
	        		    	    apCheck.getGlFunctionalCurrency().getFcName(), 
	        		    	    apCheck.getChkConversionDate(), apCheck.getChkConversionRate(), apDistributionRecord.getDrAmount(), AD_CMPNY);
	            			
	            		}
	            	    
	            	    LocalGlJournalLineInterface glJournalLineInterface = glJournalLineInterfaceHome.create(
	        		    	++lineNumber,
	        		    	apDistributionRecord.getDrDebit(),
	        		    	JLI_AMNT,
	        		    	apDistributionRecord.getGlChartOfAccount().getCoaAccountNumber(), AD_CMPNY);
	        		    	
	        		    glJournalInterface.addGlJournalLineInterface(glJournalLineInterface);
	        		    	
	        		    apDistributionRecord.setDrImported(EJBCommon.TRUE);	        		    	
	            	    		            			            		
	            			            		
	            	}
	            	
            	}
            	
            }
            
            return IMPORTED_JOURNALS;
                
    	} catch (Exception ex) {
    		
    		Debug.printStackTrace(ex);
    		getSessionContext().setRollbackOnly();
    		throw new EJBException(ex.getMessage());
    		
    	}
    	
    }
   		    
    private double convertForeignToFunctionalCurrency(Integer FC_CODE, String FC_NM, 
	    Date CONVERSION_DATE, double CONVERSION_RATE, double AMOUNT, Integer AD_CMPNY) {
	    	
	    Debug.print("ApGlJournalInterfaceControllerBean convertForeignToFunctionalCurrency");
	    
	    
        LocalGlFunctionalCurrencyRateHome glFunctionalCurrencyRateHome = null;
        LocalAdCompanyHome adCompanyHome = null;
         
        LocalAdCompany adCompany = null;
                 
        // Initialize EJB Homes
         
        try {
         	
            glFunctionalCurrencyRateHome = (LocalGlFunctionalCurrencyRateHome)EJBHomeFactory.
               lookUpLocalHome(LocalGlFunctionalCurrencyRateHome.JNDI_NAME, LocalGlFunctionalCurrencyRateHome.class);                         
            adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
               lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);             
                   
         } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
         }
         
         // get company and extended precision
         
         try {
         	
             adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
         	             
         } catch (Exception ex) {
         	
             throw new EJBException(ex.getMessage());
         	
         }	     
        
         
         // Convert to functional currency if necessary
         
         if (CONVERSION_RATE != 1 && CONVERSION_RATE != 0 ) {
         	         
             AMOUNT = AMOUNT * CONVERSION_RATE;
             	
         } else if (CONVERSION_DATE != null) {
         	
         	 try {
         	 	    
         	 	 // Get functional currency rate
         	 	 
         	     LocalGlFunctionalCurrencyRate glReceiptFunctionalCurrencyRate = null;
         	     
         	     if (!FC_NM.equals("USD")) {
         	     	
        	         glReceiptFunctionalCurrencyRate = 
	     	             glFunctionalCurrencyRateHome.findByFcCodeAndDate(FC_CODE, 
	     	             CONVERSION_DATE, AD_CMPNY);
	     	             
	     	         AMOUNT = AMOUNT * glReceiptFunctionalCurrencyRate.getFrXToUsd();
	     	             
         	     }
                 
                 // Get set of book functional currency rate if necessary
         	 	         
                 if (!adCompany.getGlFunctionalCurrency().getFcName().equals("USD")) {
                 
                     LocalGlFunctionalCurrencyRate glCompanyFunctionalCurrencyRate = 
                         glFunctionalCurrencyRateHome.findByFcCodeAndDate(adCompany.getGlFunctionalCurrency().
                             getFcCode(), CONVERSION_DATE, AD_CMPNY);
                     
                     AMOUNT = AMOUNT / glCompanyFunctionalCurrencyRate.getFrXToUsd();
                 
                  }
                                 
         	             
         	 } catch (Exception ex) {
         	 	
         	 	throw new EJBException(ex.getMessage());
         	 	
         	 }       
         	
         }
         
         return EJBCommon.roundIt(AMOUNT, adCompany.getGlFunctionalCurrency().getFcPrecision());
         
	}
	
   // SessionBean methods

   /**
    * @ejb:create-method view-type="remote"
    **/
   public void ejbCreate() throws CreateException {

      Debug.print("ApGlJournalInterfaceControllerBean ejbCreate");
      
   }

   // private methods
   	
		   
}
