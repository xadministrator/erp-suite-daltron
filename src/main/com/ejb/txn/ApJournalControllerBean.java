
/*
 * ApJournalControllerBean.java
 *
 * Created on Mar 01, 2004, 10:23 AM
 *
 * @author  Dennis Hilario
 */

package com.ejb.txn;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;
import javax.naming.NamingException;

import com.ejb.ad.LocalAdCompany;
import com.ejb.ad.LocalAdCompanyHome;
import com.ejb.ad.LocalAdPreference;
import com.ejb.ad.LocalAdPreferenceHome;
import com.ejb.ap.LocalApAppliedVoucher;
import com.ejb.ap.LocalApCheck;
import com.ejb.ap.LocalApCheckHome;
import com.ejb.ap.LocalApDistributionRecord;
import com.ejb.ap.LocalApDistributionRecordHome;
import com.ejb.ap.LocalApPurchaseOrder;
import com.ejb.ap.LocalApPurchaseOrderHome;
import com.ejb.ap.LocalApVoucher;
import com.ejb.ap.LocalApVoucherHome;
import com.ejb.ar.LocalArAppliedInvoice;
import com.ejb.exception.GlobalBranchAccountNumberInvalidException;
import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.genfld.LocalGenSegmentHome;
import com.ejb.genfld.LocalGenValueSetValueHome;
import com.ejb.gl.LocalGlChartOfAccount;
import com.ejb.gl.LocalGlChartOfAccountHome;
import com.util.AbstractSessionBean;
import com.util.ApModDistributionRecordDetails;
import com.util.Debug;
import com.util.EJBCommon;
import com.util.EJBHomeFactory;

/**
 * @ejb:bean name="ApJournalControllerEJB"
 *           display-name="used for viewing journals"
 *           type="Stateless"
 *           view-type="remote"
 *           jndi-name="ejb/ApJournalControllerEJB"
 *
 * @ejb:interface remote-class="com.ejb.txn.ApJournalController"
 *                extends="javax.ejb.EJBObject"
 *
 * @ejb:home remote-class="com.ejb.txn.ApJournalControllerHome"
 *           extends="javax.ejb.EJBHome"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="apuser"
 *                        role-link="apuserlink"
 *
 * @ejb:permission role-name="apuser"
 * 
*/

public class ApJournalControllerBean extends AbstractSessionBean {

    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList getApDrByChkCode(Integer CHK_CODE, Integer AD_CMPNY) 
        throws GlobalNoRecordFoundException {
                    
        Debug.print("ApJournalControllerBean getApDrByChkCode");
        
        LocalApCheckHome apCheckHome = null;
        LocalApDistributionRecordHome apDistributionRecordHome = null;
  
        // Initialize EJB Home
        
        try {
            
            apCheckHome = (LocalApCheckHome)EJBHomeFactory.
                lookUpLocalHome(LocalApCheckHome.JNDI_NAME, LocalApCheckHome.class);
            apDistributionRecordHome = (LocalApDistributionRecordHome)EJBHomeFactory.
            	lookUpLocalHome(LocalApDistributionRecordHome.JNDI_NAME, LocalApDistributionRecordHome.class);
       
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }        
        
        try {
        	
        	LocalApCheck apCheck = null;

        	try {
        		
        		apCheck = apCheckHome.findByPrimaryKey(CHK_CODE);
        		
        	} catch (FinderException ex) {
        		
        		throw new GlobalNoRecordFoundException();
        		
        	}
        	
        	ArrayList list = new ArrayList();

        	// get distribution records
        	
        	Collection apDistributionRecords = apDistributionRecordHome.findByChkCode(apCheck.getChkCode(), AD_CMPNY);       	            
        	
        	short lineNumber = 1;
        	
        	Iterator i = apDistributionRecords.iterator();
        	
        	while (i.hasNext()) {
        		
        		LocalApDistributionRecord apDistributionRecord = (LocalApDistributionRecord)i.next();
        		
        		ApModDistributionRecordDetails mdetails = new ApModDistributionRecordDetails();
        		
        		mdetails.setDrCode(apDistributionRecord.getDrCode());
        		mdetails.setDrLine(lineNumber);
        		mdetails.setDrClass(apDistributionRecord.getDrClass());
        		mdetails.setDrDebit(apDistributionRecord.getDrDebit());
        		mdetails.setDrAmount(apDistributionRecord.getDrAmount());
        		mdetails.setDrCoaAccountNumber(apDistributionRecord.getGlChartOfAccount().getCoaAccountNumber());       		
			    mdetails.setDrCoaAccountDescription(apDistributionRecord.getGlChartOfAccount().getCoaAccountDescription());			    
			    
        		list.add(mdetails);
        		
        		lineNumber++;
        		
        	}
        	
        	if (list.isEmpty()) {
        		
        		throw new GlobalNoRecordFoundException();
        		
        	}
        	        	        	
        	return list;
        	
        } catch (GlobalNoRecordFoundException ex) {
        	
        	throw ex;
        	        	
        } catch (Exception ex) {
        	
        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());
        	
        }
            
    }
    
    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList getApDrByVouCode(Integer VOU_CODE, Integer AD_CMPNY) 
        throws GlobalNoRecordFoundException {
                    
        Debug.print("ApJournalControllerBean getApDrByVouCode");
        
        LocalApVoucherHome apVoucherHome = null;
        LocalAdCompanyHome adCompanyHome = null;
        LocalGenSegmentHome genSegmentHome = null;
        LocalGenValueSetValueHome genValueSetValueHome = null;
        LocalApDistributionRecordHome apDistributionRecordHome = null;
                
        // Initialize EJB Home
        
        try {
            
            apVoucherHome = (LocalApVoucherHome)EJBHomeFactory.
                lookUpLocalHome(LocalApVoucherHome.JNDI_NAME, LocalApVoucherHome.class);
            adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
            genSegmentHome = (LocalGenSegmentHome)EJBHomeFactory.
                lookUpLocalHome(LocalGenSegmentHome.JNDI_NAME, LocalGenSegmentHome.class);                
            genValueSetValueHome = (LocalGenValueSetValueHome)EJBHomeFactory.
                lookUpLocalHome(LocalGenValueSetValueHome.JNDI_NAME, LocalGenValueSetValueHome.class);
            apDistributionRecordHome = (LocalApDistributionRecordHome)EJBHomeFactory.
            	lookUpLocalHome(LocalApDistributionRecordHome.JNDI_NAME, LocalApDistributionRecordHome.class);
                        
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }        
        
        try {
        	
        	LocalApVoucher apVoucher = null;
        	        	
        	try {
        		
        		apVoucher = apVoucherHome.findByPrimaryKey(VOU_CODE);
        		
        	} catch (FinderException ex) {
        		
        		throw new GlobalNoRecordFoundException();
        		
        	}
        	
        	ArrayList list = new ArrayList();

        	// get distribution records
        	
        	Collection apDistributionRecords = apDistributionRecordHome.findByVouCode(apVoucher.getVouCode(), AD_CMPNY);       	            
        	
        	short lineNumber = 1;
        	
        	Iterator i = apDistributionRecords.iterator();
        	
        	while (i.hasNext()) {
        		
        		LocalApDistributionRecord apDistributionRecord = (LocalApDistributionRecord)i.next();
        		
        		ApModDistributionRecordDetails mdetails = new ApModDistributionRecordDetails();
        		
        		mdetails.setDrCode(apDistributionRecord.getDrCode());
        		mdetails.setDrLine(lineNumber);
        		mdetails.setDrClass(apDistributionRecord.getDrClass());
        		mdetails.setDrDebit(apDistributionRecord.getDrDebit());
        		mdetails.setDrAmount(apDistributionRecord.getDrAmount());
        		mdetails.setDrCoaAccountNumber(apDistributionRecord.getGlChartOfAccount().getCoaAccountNumber());       		
			    mdetails.setDrCoaAccountDescription(apDistributionRecord.getGlChartOfAccount().getCoaAccountDescription());			    
			    
        		list.add(mdetails);
        		
        		lineNumber++;
        		
        	}
        	
        	if (list.isEmpty()) {
        		
        		throw new GlobalNoRecordFoundException();
        		
        	}
        	        	        	
        	return list;
        	
        } catch (GlobalNoRecordFoundException ex) {
        	
        	throw ex;
        	        	
        } catch (Exception ex) {
        	
        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());
        	
        }
            
    }
    
    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList getApDrByPoCode(Integer PO_CODE, Integer AD_CMPNY) 
        throws GlobalNoRecordFoundException {
                    
        Debug.print("ApJournalControllerBean getApDrByPoCode");
        
        LocalApPurchaseOrderHome apPurchaseOrderHome = null;
        LocalAdCompanyHome adCompanyHome = null;
        LocalGenSegmentHome genSegmentHome = null;
        LocalGenValueSetValueHome genValueSetValueHome = null;
        LocalApDistributionRecordHome apDistributionRecordHome = null;
                
        // Initialize EJB Home
        
        try {
            
            apPurchaseOrderHome = (LocalApPurchaseOrderHome)EJBHomeFactory.
                lookUpLocalHome(LocalApPurchaseOrderHome.JNDI_NAME, LocalApPurchaseOrderHome.class);
            adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
            genSegmentHome = (LocalGenSegmentHome)EJBHomeFactory.
                lookUpLocalHome(LocalGenSegmentHome.JNDI_NAME, LocalGenSegmentHome.class);                
            genValueSetValueHome = (LocalGenValueSetValueHome)EJBHomeFactory.
                lookUpLocalHome(LocalGenValueSetValueHome.JNDI_NAME, LocalGenValueSetValueHome.class);
            apDistributionRecordHome = (LocalApDistributionRecordHome)EJBHomeFactory.
            	lookUpLocalHome(LocalApDistributionRecordHome.JNDI_NAME, LocalApDistributionRecordHome.class);
                        
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }        
        
        try {
        	
        	LocalApPurchaseOrder apPurchaseOrder = null;
        	        	
        	try {
        		
        		apPurchaseOrder = apPurchaseOrderHome.findByPrimaryKey(PO_CODE);
        		
        	} catch (FinderException ex) {
        		
        		throw new GlobalNoRecordFoundException();
        		
        	}

        	ArrayList list = new ArrayList();

        	// get distribution records
        	
        	Collection apDistributionRecords = apDistributionRecordHome.findByPoCode(apPurchaseOrder.getPoCode(), AD_CMPNY);       	            
        	
        	short lineNumber = 1;
        	
        	Iterator i = apDistributionRecords.iterator();
        	
        	while (i.hasNext()) {
        		
        		LocalApDistributionRecord apDistributionRecord = (LocalApDistributionRecord)i.next();
        		
        		ApModDistributionRecordDetails mdetails = new ApModDistributionRecordDetails();
        		
        		mdetails.setDrCode(apDistributionRecord.getDrCode());
        		mdetails.setDrLine(lineNumber);
        		mdetails.setDrClass(apDistributionRecord.getDrClass());
        		mdetails.setDrDebit(apDistributionRecord.getDrDebit());
        		mdetails.setDrAmount(apDistributionRecord.getDrAmount());
        		mdetails.setDrCoaAccountNumber(apDistributionRecord.getGlChartOfAccount().getCoaAccountNumber());       		
			    mdetails.setDrCoaAccountDescription(apDistributionRecord.getGlChartOfAccount().getCoaAccountDescription());			    
			    
        		list.add(mdetails);
        		
        		lineNumber++;
        		
        	}
        	
        	if (list.isEmpty()) {
        		
        		throw new GlobalNoRecordFoundException();
        		
        	}
        	        	        	
        	return list;
        	
        } catch (GlobalNoRecordFoundException ex) {
        	
        	throw ex;
        	        	
        } catch (Exception ex) {
        	
        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());
        	
        }
            
    }
    
    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public short getGlFcPrecisionUnit(Integer AD_CMPNY) {

       Debug.print("ApJournalControllerBean getGlFcPrecisionUnit");

      
       LocalAdCompanyHome adCompanyHome = null;
      
     
       // Initialize EJB Home
        
       try {
            
           adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
              lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);       
            
       } catch (NamingException ex) {
            
           throw new EJBException(ex.getMessage());
            
       }

       try {
       	
         LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
            
         return  adCompany.getGlFunctionalCurrency().getFcPrecision();
                                     
       } catch (Exception ex) {
       	 
       	 Debug.printStackTrace(ex);
         throw new EJBException(ex.getMessage());
         
       }

    }
    
    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
     public short getAdPrfApJournalLineNumber(Integer AD_CMPNY) {

        Debug.print("ApJournalControllerBean getAdPrfApJournalLineNumber");
                   
        LocalAdPreferenceHome adPreferenceHome = null;
       
       
        // Initialize EJB Home
         
        try {
             
           adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
               lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);       
             
        } catch (NamingException ex) {
             
           throw new EJBException(ex.getMessage());
             
        }
       
        try {
       	
           LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);
          
           return adPreference.getPrfApJournalLineNumber();
          
        } catch (Exception ex) {
         	 
           Debug.printStackTrace(ex);
           throw new EJBException(ex.getMessage());
          
        }
       
     }
    
    /**
     * @ejb:interface-method view-type="remote"
     **/
    public void saveApDrEntry(Integer PRMRY_KEY, ArrayList drList, String transactionType, Integer AD_BRNCH, Integer AD_CMPNY) 
       throws GlobalBranchAccountNumberInvalidException {
       
       Debug.print("ApJournalControllerBean saveApDrEntry");

       LocalApDistributionRecordHome apDistributionRecordHome = null;
       LocalGlChartOfAccountHome glChartOfAccountHome = null;
       LocalApVoucherHome apVoucherHome = null;
       LocalApCheckHome apCheckHome = null;
       LocalApPurchaseOrderHome apPurchaseOrderHome = null;
       

       // Initialize EJB Home
      
       try {
            
       		apDistributionRecordHome = (LocalApDistributionRecordHome)EJBHomeFactory.
               	lookUpLocalHome(LocalApDistributionRecordHome.JNDI_NAME, LocalApDistributionRecordHome.class);
       		glChartOfAccountHome = (LocalGlChartOfAccountHome)EJBHomeFactory.
               	lookUpLocalHome(LocalGlChartOfAccountHome.JNDI_NAME, LocalGlChartOfAccountHome.class);
       		apVoucherHome = (LocalApVoucherHome)EJBHomeFactory.
       			lookUpLocalHome(LocalApVoucherHome.JNDI_NAME, LocalApVoucherHome.class);
       		apCheckHome = (LocalApCheckHome)EJBHomeFactory.
   	   	   		lookUpLocalHome(LocalApCheckHome.JNDI_NAME, LocalApCheckHome.class);
       		apPurchaseOrderHome = (LocalApPurchaseOrderHome)EJBHomeFactory.
   	   	   		lookUpLocalHome(LocalApPurchaseOrderHome.JNDI_NAME, LocalApPurchaseOrderHome.class);
            
       } catch (NamingException ex) {
            
           throw new EJBException(ex.getMessage());
            
       }
      
       try {
          
       		LocalApVoucher apVoucher = null;
       		LocalApCheck apCheck = null;
       		LocalApPurchaseOrder apPurchaseOrder = null;
	       
       		Collection apDistributionRecords = null;
       		Iterator i;
           
       		if (transactionType.equals("VOUCHER") || transactionType.equals("DEBIT MEMO")) {
    	       	
    	       		apVoucher = apVoucherHome.findByPrimaryKey(PRMRY_KEY);
    	       		
    	            // remove all distribution records
    	       		
    	       		apDistributionRecords = apVoucher.getApDistributionRecords();
    	       		
    	       	    i = apDistributionRecords.iterator();     	  
    	       	  
    	       	    while (i.hasNext()) {
    	       	  	
    	       	   	    LocalApDistributionRecord apDistributionRecord = (LocalApDistributionRecord)i.next();
    	       	  	   
    	       	  	    i.remove();
    	       	  	    
    	       	  	    apDistributionRecord.remove();
    	       	  	
    	       	    }
    	       	           	    
    	       	    // add new distribution records
    	       	  
    	       	    i = drList.iterator();
    	       	  
    	       	    while (i.hasNext()) {
    	       	  	
    	       	  	    ApModDistributionRecordDetails mDrDetails = (ApModDistributionRecordDetails) i.next();      	  	  
    	       	  	          	  	  
    	       	  	    this.addApDrEntry(mDrDetails, apVoucher, null, null, null, AD_BRNCH, AD_CMPNY);
    	       	  	
    	       	    }
    	       			       			       	
    	       } else if (transactionType.equals("PAYMENT") || transactionType.equals("DIRECT CHECK")) {
    	       	
		       	    apCheck = apCheckHome.findByPrimaryKey(PRMRY_KEY);
		       	    ArrayList appliedVoucherList = new ArrayList(); 
		       	 
		            // remove all distribution records
		       	    
		       	    apDistributionRecords = apCheck.getApDistributionRecords();
		       	    
		       	    i = apDistributionRecords.iterator();     	  
		       	  
		       	    while (i.hasNext()) {
		       	  	
		       	   	    LocalApDistributionRecord apDistributionRecord = (LocalApDistributionRecord)i.next();
		       	   	    appliedVoucherList.add(apDistributionRecord.getApAppliedVoucher());
		       	  	    i.remove();
		       	  	    
		       	  	    apDistributionRecord.remove();
		       	  	
		       	    }
		       	           	    
		       	    // add new distribution records
		       	  
		       	    i = drList.iterator();
		       	    int ctr = 0;
		       	    while (i.hasNext()) {
		       	  	
		       	  	    ApModDistributionRecordDetails mDrDetails = (ApModDistributionRecordDetails) i.next();     
		       	  	    try{
		       	  	    	LocalApAppliedVoucher apAppliedVoucher  = (LocalApAppliedVoucher)appliedVoucherList.toArray()[ctr];
		       	  	    	this.addApDrEntry(mDrDetails, null, apCheck, apAppliedVoucher, null, AD_BRNCH, AD_CMPNY);

		       	  	    }catch(Exception e){
		       	  	    	//LocalApAppliedVoucher apAppliedVoucher  = (LocalApAppliedVoucher)appliedVoucherList.toArray()[ctr];
		       	  	    	this.addApDrEntry(mDrDetails, null, apCheck, null, null, AD_BRNCH, AD_CMPNY);

		       	  	    }
		       	  	   ctr++;
		       	    }
	       	
	       } else if (transactionType.equals("RECEIVING")) {
	       	
	       		    apPurchaseOrder = apPurchaseOrderHome.findByPrimaryKey(PRMRY_KEY);
    	       		
    	            // remove all distribution records
    	       		
    	       		apDistributionRecords = apPurchaseOrder.getApDistributionRecords();
    	       		
    	       	    i = apDistributionRecords.iterator();     	  
    	       	  
    	       	    while (i.hasNext()) {
    	       	  	
    	       	   	    LocalApDistributionRecord apDistributionRecord = (LocalApDistributionRecord)i.next();
    	       	  	   
    	       	  	    i.remove();
    	       	  	    
    	       	  	    apDistributionRecord.remove();
    	       	  	
    	       	    }
    	       	           	    
    	       	    // add new distribution records
    	       	  
    	       	    i = drList.iterator();
    	       	  
    	       	    while (i.hasNext()) {
    	       	  	
    	       	  	    ApModDistributionRecordDetails mDrDetails = (ApModDistributionRecordDetails) i.next();      	  	  
    	       	  	          	  	  
    	       	  	    this.addApDrEntry(mDrDetails, null, null, null, apPurchaseOrder, AD_BRNCH, AD_CMPNY);
    	       	  	
    	       	    }
	       	
	       }

       } catch (GlobalBranchAccountNumberInvalidException ex) {
       	    
           getSessionContext().setRollbackOnly();
       	   throw ex;

       } catch (Exception ex) {
          
          getSessionContext().setRollbackOnly();
          throw new EJBException(ex.getMessage());
       }

       
    }
    
//  private methods
    
    private void addApDrEntry(ApModDistributionRecordDetails mdetails, LocalApVoucher apVoucher,
    	LocalApCheck apCheck, LocalApAppliedVoucher apAppliedVoucher, LocalApPurchaseOrder apPurchaseOrder, Integer AD_BRNCH, Integer AD_CMPNY) 
    	throws GlobalBranchAccountNumberInvalidException {
		
    	Debug.print("ApJournalControllerBean addApDrEntry");
	
    	LocalApDistributionRecordHome apDistributionRecordHome = null;        
    	LocalGlChartOfAccountHome glChartOfAccountHome = null;
    	LocalAdCompanyHome adCompanyHome = null;
       
            
	    // Initialize EJB Home
	    
	    try {
	        
	        apDistributionRecordHome = (LocalApDistributionRecordHome)EJBHomeFactory.
	            lookUpLocalHome(LocalApDistributionRecordHome.JNDI_NAME, LocalApDistributionRecordHome.class);            
	        glChartOfAccountHome = (LocalGlChartOfAccountHome)EJBHomeFactory.
	            lookUpLocalHome(LocalGlChartOfAccountHome.JNDI_NAME, LocalGlChartOfAccountHome.class);            
	        adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
	            lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);            
	         
	                    
	    } catch (NamingException ex) {
	        
	        throw new EJBException(ex.getMessage());
	        
	    }            
	            
	    try {
	    	
	    	// get company
	    	
	    	LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
	    	
	    	// validate if coa exists
	    	
	    	LocalGlChartOfAccount glChartOfAccount = null;
	    	
	    	try {
	    	
	    		glChartOfAccount = glChartOfAccountHome.findByCoaAccountNumberAndBranchCode(mdetails.getDrCoaAccountNumber(), AD_BRNCH, AD_CMPNY);
	    		
	    		if (glChartOfAccount.getCoaEnable() == EJBCommon.FALSE) 
	    		    throw new GlobalBranchAccountNumberInvalidException();
	    		
	    	} catch (FinderException ex) {
	    		
	    		throw new GlobalBranchAccountNumberInvalidException();
	    		
	    	}
	    	        			    
		    // create distribution record 
		    
		    LocalApDistributionRecord apDistributionRecord = apDistributionRecordHome.create(
			    mdetails.getDrLine(), 
			    mdetails.getDrClass(),
			    EJBCommon.roundIt(mdetails.getDrAmount(), adCompany.getGlFunctionalCurrency().getFcPrecision()),
			    mdetails.getDrDebit(), 
			    EJBCommon.FALSE, EJBCommon.FALSE, AD_CMPNY);
		    
		    glChartOfAccount.addApDistributionRecord(apDistributionRecord);
			    
		    if (apVoucher != null) {
		    	
		    	apVoucher.addApDistributionRecord(apDistributionRecord);
		    					
		    } else if (apCheck != null) {
		    	
		    	apCheck.addApDistributionRecord(apDistributionRecord);			
		    }else if (apAppliedVoucher != null) {
					apAppliedVoucher.addApDistributionRecord(apDistributionRecord);				
					
		    } else if (apPurchaseOrder != null) {
		    
		    	apPurchaseOrder.addApDistributionRecord(apDistributionRecord);
		    
		    }
		    
	    } catch(GlobalBranchAccountNumberInvalidException ex) {
            
            throw ex;
		    		            		
	    } catch (Exception ex) {
	    	
	    	Debug.printStackTrace(ex);
	    	getSessionContext().setRollbackOnly();
	    	throw new EJBException(ex.getMessage());
	    	
	    }
	
    }
	// SessionBean methods

    /**
     * @ejb:create-method view-type="remote"
     **/
    public void ejbCreate() throws CreateException {

       Debug.print("ApJournalControllerBean ejbCreate");
      
    }

}