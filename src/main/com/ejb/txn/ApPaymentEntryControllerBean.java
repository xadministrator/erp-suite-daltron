package com.ejb.txn;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;
import javax.naming.NamingException;

import com.ejb.ad.LocalAdAmountLimit;
import com.ejb.ad.LocalAdAmountLimitHome;
import com.ejb.ad.LocalAdApproval;
import com.ejb.ad.LocalAdApprovalHome;
import com.ejb.ad.LocalAdApprovalQueue;
import com.ejb.ad.LocalAdApprovalQueueHome;
import com.ejb.ad.LocalAdApprovalUser;
import com.ejb.ad.LocalAdApprovalUserHome;
import com.ejb.ad.LocalAdBankAccount;
import com.ejb.ad.LocalAdBankAccountBalance;
import com.ejb.ad.LocalAdBankAccountBalanceHome;
import com.ejb.ad.LocalAdBankAccountHome;
import com.ejb.ad.LocalAdBranchBankAccount;
import com.ejb.ad.LocalAdBranchBankAccountHome;
import com.ejb.ad.LocalAdBranchDocumentSequenceAssignment;
import com.ejb.ad.LocalAdBranchDocumentSequenceAssignmentHome;
import com.ejb.ad.LocalAdCompany;
import com.ejb.ad.LocalAdCompanyHome;
import com.ejb.ad.LocalAdDeleteAuditTrailHome;
import com.ejb.ad.LocalAdDiscount;
import com.ejb.ad.LocalAdDiscountHome;
import com.ejb.ad.LocalAdLookUpValue;
import com.ejb.ad.LocalAdLookUpValueHome;
import com.ejb.ad.LocalAdPreference;
import com.ejb.ad.LocalAdPreferenceHome;
import com.ejb.ap.LocalApAppliedVoucher;
import com.ejb.ap.LocalApAppliedVoucherHome;
import com.ejb.ap.LocalApCheck;
import com.ejb.ap.LocalApCheckBatch;
import com.ejb.ap.LocalApCheckBatchHome;
import com.ejb.ap.LocalApCheckHome;
import com.ejb.ap.LocalApDistributionRecord;
import com.ejb.ap.LocalApDistributionRecordHome;
import com.ejb.ap.LocalApPurchaseOrder;
import com.ejb.ap.LocalApPurchaseOrderHome;
import com.ejb.ap.LocalApPurchaseOrderLine;
import com.ejb.ap.LocalApSupplier;
import com.ejb.ap.LocalApSupplierBalance;
import com.ejb.ap.LocalApSupplierBalanceHome;
import com.ejb.ap.LocalApSupplierHome;
import com.ejb.ap.LocalApTaxCode;
import com.ejb.ap.LocalApVoucher;
import com.ejb.ap.LocalApVoucherBatchHome;
import com.ejb.ap.LocalApVoucherHome;
import com.ejb.ap.LocalApVoucherLineItem;
import com.ejb.ap.LocalApVoucherPaymentSchedule;
import com.ejb.ap.LocalApVoucherPaymentScheduleHome;
import com.ejb.exception.ApCHKCheckNumberNotUniqueException;
import com.ejb.exception.ApCHKVoucherHasNoWTaxCodeException;
import com.ejb.exception.ApVOUOverapplicationNotAllowedException;
import com.ejb.exception.GlJREffectiveDateNoPeriodExistException;
import com.ejb.exception.GlJREffectiveDatePeriodClosedException;
import com.ejb.exception.GlobalBranchAccountNumberInvalidException;
import com.ejb.exception.GlobalConversionDateNotExistException;
import com.ejb.exception.GlobalDocumentNumberNotUniqueException;
import com.ejb.exception.GlobalJournalNotBalanceException;
import com.ejb.exception.GlobalNoApprovalApproverFoundException;
import com.ejb.exception.GlobalNoApprovalRequesterFoundException;
import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.exception.GlobalRecordAlreadyDeletedException;
import com.ejb.exception.GlobalTransactionAlreadyApprovedException;
import com.ejb.exception.GlobalTransactionAlreadyLockedException;
import com.ejb.exception.GlobalTransactionAlreadyPendingException;
import com.ejb.exception.GlobalTransactionAlreadyPostedException;
import com.ejb.exception.GlobalTransactionAlreadyVoidException;
import com.ejb.exception.GlobalTransactionAlreadyVoidPostedException;
import com.ejb.gl.LocalAdDocumentSequenceAssignment;
import com.ejb.gl.LocalAdDocumentSequenceAssignmentHome;
import com.ejb.gl.LocalGlAccountingCalendarValue;
import com.ejb.gl.LocalGlAccountingCalendarValueHome;
import com.ejb.gl.LocalGlChartOfAccount;
import com.ejb.gl.LocalGlChartOfAccountBalance;
import com.ejb.gl.LocalGlChartOfAccountBalanceHome;
import com.ejb.gl.LocalGlChartOfAccountHome;
import com.ejb.gl.LocalGlForexLedger;
import com.ejb.gl.LocalGlForexLedgerHome;
import com.ejb.gl.LocalGlFunctionalCurrency;
import com.ejb.gl.LocalGlFunctionalCurrencyHome;
import com.ejb.gl.LocalGlFunctionalCurrencyRate;
import com.ejb.gl.LocalGlFunctionalCurrencyRateHome;
import com.ejb.gl.LocalGlJournal;
import com.ejb.gl.LocalGlJournalBatch;
import com.ejb.gl.LocalGlJournalBatchHome;
import com.ejb.gl.LocalGlJournalCategory;
import com.ejb.gl.LocalGlJournalCategoryHome;
import com.ejb.gl.LocalGlJournalHome;
import com.ejb.gl.LocalGlJournalLine;
import com.ejb.gl.LocalGlJournalLineHome;
import com.ejb.gl.LocalGlJournalSource;
import com.ejb.gl.LocalGlJournalSourceHome;
import com.ejb.gl.LocalGlSetOfBook;
import com.ejb.gl.LocalGlSetOfBookHome;
import com.ejb.gl.LocalGlSuspenseAccount;
import com.ejb.gl.LocalGlSuspenseAccountHome;
import com.ejb.inv.LocalInvCosting;
import com.ejb.inv.LocalInvCostingHome;
import com.ejb.inv.LocalInvItem;
import com.ejb.inv.LocalInvItemLocation;
import com.ejb.inv.LocalInvItemLocationHome;
import com.ejb.inv.LocalInvUnitOfMeasure;
import com.ejb.inv.LocalInvUnitOfMeasureConversion;
import com.ejb.inv.LocalInvUnitOfMeasureConversionHome;

import com.util.AbstractSessionBean;
import com.util.ApCheckDetails;
import com.util.ApModAppliedVoucherDetails;
import com.util.ApModCheckDetails;
import com.util.ApModSupplierDetails;
import com.util.ApModVoucherPaymentScheduleDetails;
import com.util.Debug;
import com.util.EJBCommon;
import com.util.EJBHomeFactory;
import com.util.GlModFunctionalCurrencyDetails;
import javax.ejb.RemoveException;

/**
 * @ejb:bean name="ApPaymentEntryControllerEJB"
 *           display-name="used for entering check payment"
 *           type="Stateless"
 *           view-type="remote"
 *           jndi-name="ejb/ApPaymentEntryControllerEJB"
 *
 * @ejb:interface remote-class="com.ejb.txn.ApPaymentEntryController"
 *                extends="javax.ejb.EJBObject"
 *
 * @ejb:home remote-class="com.ejb.txn.ApPaymentEntryControllerHome"
 *           extends="javax.ejb.EJBHome"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="apuser"
 *                        role-link="apuserlink"
 *
 * @ejb:permission role-name="apuser"
 *
*/

public class ApPaymentEntryControllerBean extends AbstractSessionBean {

    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList getGlFcAllWithDefault(Integer AD_CMPNY) {

        Debug.print("ApPaymentEntryControllerBean getGlFcAllWithDefault");

        LocalGlFunctionalCurrencyHome glFunctionalCurrencyHome = null;
        LocalAdCompanyHome adCompanyHome = null;

        Collection glFunctionalCurrencies = null;

        LocalGlFunctionalCurrency glFunctionalCurrency = null;
        LocalAdCompany adCompany = null;


        ArrayList list = new ArrayList();

        // Initialize EJB Home

        try {

            glFunctionalCurrencyHome = (LocalGlFunctionalCurrencyHome)EJBHomeFactory.
                lookUpLocalHome(LocalGlFunctionalCurrencyHome.JNDI_NAME, LocalGlFunctionalCurrencyHome.class);
            adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);

        } catch (NamingException ex) {

            throw new EJBException(ex.getMessage());

        }

        try {

            adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);

            glFunctionalCurrencies = glFunctionalCurrencyHome.findFcAllEnabled(
            	EJBCommon.getGcCurrentDateWoTime().getTime(), AD_CMPNY);

        } catch (Exception ex) {

            throw new EJBException(ex.getMessage());
        }

        if (glFunctionalCurrencies.isEmpty()) {

        	return null;

        }

        Iterator i = glFunctionalCurrencies.iterator();

        while (i.hasNext()) {

        	glFunctionalCurrency = (LocalGlFunctionalCurrency)i.next();

        	GlModFunctionalCurrencyDetails mdetails = new GlModFunctionalCurrencyDetails(
        	    glFunctionalCurrency.getFcCode(), glFunctionalCurrency.getFcName(),
        	    adCompany.getGlFunctionalCurrency().getFcName().equals(glFunctionalCurrency.getFcName()) ?
        	       EJBCommon.TRUE : EJBCommon.FALSE);

        	list.add(mdetails);

        }

        return list;

    }


    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList getAdBaAll(Integer AD_BRNCH, Integer AD_CMPNY) {

        Debug.print("ApPaymentEntryControllerBean getAdBaAll");

        LocalAdBankAccountHome adBankAccountHome = null;

        ArrayList list = new ArrayList();

        // Initialize EJB Home

        try {

            adBankAccountHome = (LocalAdBankAccountHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdBankAccountHome.JNDI_NAME, LocalAdBankAccountHome.class);

        } catch (NamingException ex) {

            throw new EJBException(ex.getMessage());

        }

        try {

        	Collection adBankAccounts = adBankAccountHome.findEnabledBaAll(AD_BRNCH, AD_CMPNY);

        	Iterator i = adBankAccounts.iterator();

        	while (i.hasNext()) {

        		LocalAdBankAccount adBankAccount = (LocalAdBankAccount)i.next();

        		list.add(adBankAccount.getBaName());

        	}

        	return list;

        } catch (Exception ex) {

        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());

        }

    }


    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList getApSplAll(Integer AD_BRNCH, Integer AD_CMPNY) {

        Debug.print("ApPaymentEntryControllerBean getApSplAll");

        LocalApSupplierHome apSupplierHome = null;

        ArrayList list = new ArrayList();

        // Initialize EJB Home

        try {

            apSupplierHome = (LocalApSupplierHome)EJBHomeFactory.
                lookUpLocalHome(LocalApSupplierHome.JNDI_NAME, LocalApSupplierHome.class);

        } catch (NamingException ex) {

            throw new EJBException(ex.getMessage());

        }

        try {

        	Collection apSuppliers = apSupplierHome.findEnabledSplAll(AD_BRNCH, AD_CMPNY);

        	Iterator i = apSuppliers.iterator();

        	while (i.hasNext()) {

        		LocalApSupplier apSupplier = (LocalApSupplier)i.next();

        		list.add(apSupplier.getSplSupplierCode());

        	}

        	return list;

        } catch (Exception ex) {

        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());

        }

    }

    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ApModCheckDetails getApChkByChkCode(Integer CHK_CODE, Integer AD_CMPNY)
        throws GlobalNoRecordFoundException {

        Debug.print("ApPaymentEntryControllerBean getApChkByChkCode");

        LocalApCheckHome apCheckHome = null;
        LocalAdCompanyHome adCompanyHome = null;

        // Initialize EJB Home

        try {

            apCheckHome = (LocalApCheckHome)EJBHomeFactory.
                lookUpLocalHome(LocalApCheckHome.JNDI_NAME, LocalApCheckHome.class);
            adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);

        } catch (NamingException ex) {

            throw new EJBException(ex.getMessage());

        }

        try {

        	LocalApCheck apCheck = null;


        	try {
        		System.out.println("CHK_CODE="+CHK_CODE);

        		apCheck = apCheckHome.findByPrimaryKey(CHK_CODE);

        	} catch (FinderException ex) {

        		throw new GlobalNoRecordFoundException();

        	}

        	ArrayList chkAvList = new ArrayList();

        	// get applied vouchers

        	Collection apAppliedVouchers = apCheck.getApAppliedVouchers();

        	Iterator i = apAppliedVouchers.iterator();

        	while (i.hasNext()) {

        		LocalApAppliedVoucher apAppliedVoucher = (LocalApAppliedVoucher)i.next();

        		ApModAppliedVoucherDetails mdetails = new ApModAppliedVoucherDetails();

        		mdetails.setAvApplyAmount(apAppliedVoucher.getAvApplyAmount());
        		mdetails.setAvTaxWithheld(apAppliedVoucher.getAvTaxWithheld());
        		mdetails.setAvDiscountAmount(apAppliedVoucher.getAvDiscountAmount());
        		mdetails.setAvAllocatedPaymentAmount(apAppliedVoucher.getAvAllocatedPaymentAmount());
        		mdetails.setAvVpsCode(apAppliedVoucher.getApVoucherPaymentSchedule().getVpsCode());
        		mdetails.setAvVpsVouCode(apAppliedVoucher.getApVoucherPaymentSchedule().getApVoucher().getVouCode());
        		mdetails.setAvVpsNumber(apAppliedVoucher.getApVoucherPaymentSchedule().getVpsNumber());
        		mdetails.setAvVpsDueDate(apAppliedVoucher.getApVoucherPaymentSchedule().getVpsDueDate());
        		mdetails.setAvVpsAmountDue(EJBCommon.roundIt(apAppliedVoucher.getApVoucherPaymentSchedule().getVpsAmountDue() -
        		                           apAppliedVoucher.getApVoucherPaymentSchedule().getVpsAmountPaid(), this.getGlFcPrecisionUnit(AD_CMPNY)));
        		mdetails.setAvVpsVouDocumentNumber(apAppliedVoucher.getApVoucherPaymentSchedule().getApVoucher().getVouDocumentNumber());
        		mdetails.setAvVpsVouReferenceNumber(apAppliedVoucher.getApVoucherPaymentSchedule().getApVoucher().getVouReferenceNumber());
        		mdetails.setAvVpsVouDescription(apAppliedVoucher.getApVoucherPaymentSchedule().getApVoucher().getVouDescription());
        		mdetails.setAvVpsVouFcName(apAppliedVoucher.getApVoucherPaymentSchedule().getApVoucher().getGlFunctionalCurrency().getFcName());

        		mdetails.setAvVpsConversionDate(EJBCommon.convertSQLDateToString(apAppliedVoucher.getApVoucherPaymentSchedule().getApVoucher().getVouConversionDate()));
        		mdetails.setAvVpsConversionRate(apAppliedVoucher.getApVoucherPaymentSchedule().getApVoucher().getVouConversionRate());
        		chkAvList.add(mdetails);

        	}

        	ApModCheckDetails mChkDetails = new ApModCheckDetails();

        	mChkDetails.setChkCode(apCheck.getChkCode());
        	mChkDetails.setChkDate(apCheck.getChkDate());
        	mChkDetails.setChkCheckDate(apCheck.getChkCheckDate());
        	mChkDetails.setChkNumber(apCheck.getChkNumber());
        	mChkDetails.setChkDocumentNumber(apCheck.getChkDocumentNumber());
        	mChkDetails.setChkReferenceNumber(apCheck.getChkReferenceNumber());
        	mChkDetails.setChkAmount(apCheck.getChkAmount());
        	mChkDetails.setChkVoid(apCheck.getChkVoid());
        	mChkDetails.setChkDescription(apCheck.getChkDescription());
        	mChkDetails.setChkConversionDate(apCheck.getChkConversionDate());
        	mChkDetails.setChkConversionRate(apCheck.getChkConversionRate());
        	mChkDetails.setChkApprovalStatus(apCheck.getChkApprovalStatus());
        	mChkDetails.setChkReasonForRejection(apCheck.getChkReasonForRejection());
        	mChkDetails.setChkPosted(apCheck.getChkPosted());
        	mChkDetails.setChkVoid(apCheck.getChkVoid());
        	mChkDetails.setChkCrossCheck(apCheck.getChkCrossCheck());
        	mChkDetails.setChkVoidApprovalStatus(apCheck.getChkVoidApprovalStatus());
        	mChkDetails.setChkVoidPosted(apCheck.getChkVoidPosted());
        	mChkDetails.setChkCreatedBy(apCheck.getChkCreatedBy());
        	mChkDetails.setChkDateCreated(apCheck.getChkDateCreated());
        	mChkDetails.setChkLastModifiedBy(apCheck.getChkLastModifiedBy());
        	mChkDetails.setChkDateLastModified(apCheck.getChkDateLastModified());
        	mChkDetails.setChkApprovedRejectedBy(apCheck.getChkApprovedRejectedBy());
        	mChkDetails.setChkDateApprovedRejected(apCheck.getChkDateApprovedRejected());
        	mChkDetails.setChkPostedBy(apCheck.getChkPostedBy());
        	mChkDetails.setChkDatePosted(apCheck.getChkDatePosted());
        	mChkDetails.setChkFcName(apCheck.getGlFunctionalCurrency().getFcName());
        	mChkDetails.setChkSplSupplierCode(apCheck.getApSupplier().getSplSupplierCode());
        	mChkDetails.setChkBaName(apCheck.getAdBankAccount().getBaName());
        	mChkDetails.setChkCbName(apCheck.getApCheckBatch() != null ? apCheck.getApCheckBatch().getCbName() : null);
        	mChkDetails.setChkAvList(chkAvList);
        	mChkDetails.setChkSplName(apCheck.getChkSupplierName() == null ? apCheck.getApSupplier().getSplName() : apCheck.getChkSupplierName());
			mChkDetails.setChkMemo(apCheck.getChkMemo());

        	return mChkDetails;

        } catch (GlobalNoRecordFoundException ex) {

        	throw ex;

        } catch (Exception ex) {

        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());

        }

    }


    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ApModSupplierDetails getApSplBySplSupplierCode(String SPL_SPPLR_CODE, Integer AD_CMPNY)
        throws GlobalNoRecordFoundException {

        Debug.print("ApPaymentEntryControllerBean getApSplBySplSupplierCode");

        LocalApSupplierHome apSupplierHome = null;

        // Initialize EJB Home

        try {

            apSupplierHome = (LocalApSupplierHome)EJBHomeFactory.
                lookUpLocalHome(LocalApSupplierHome.JNDI_NAME, LocalApSupplierHome.class);

        } catch (NamingException ex) {

            throw new EJBException(ex.getMessage());

        }

        try {

        	LocalApSupplier apSupplier = null;


        	try {

        		apSupplier = apSupplierHome.findBySplSupplierCode(SPL_SPPLR_CODE, AD_CMPNY);

        	} catch (FinderException ex) {

        		throw new GlobalNoRecordFoundException();

        	}

        	ApModSupplierDetails mdetails = new ApModSupplierDetails();

        	mdetails.setSplStBaName(apSupplier.getAdBankAccount().getBaName());
        	mdetails.setSplName(apSupplier.getSplName());

        	return mdetails;

        } catch (GlobalNoRecordFoundException ex) {

        	throw ex;

        } catch (Exception ex) {

        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());

        }

    }


    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList getApVpsBySplSupplierCode(String SPL_SPPLR_CODE, Integer AD_BRNCH, Integer AD_CMPNY)
        throws GlobalNoRecordFoundException {

        Debug.print("ApPaymentEntryControllerBean getApVpsBySplSupplierCode");

        LocalApVoucherPaymentScheduleHome apVoucherPaymentScheduleHome = null;
        LocalAdDiscountHome adDiscountHome = null;
        LocalAdPreferenceHome adPreferenceHome = null;
        LocalApPurchaseOrderHome apPurchaseOrderHome = null;

        ArrayList list = new ArrayList();


        // Initialize EJB Home

        try {

            apVoucherPaymentScheduleHome = (LocalApVoucherPaymentScheduleHome)EJBHomeFactory.
                lookUpLocalHome(LocalApVoucherPaymentScheduleHome.JNDI_NAME, LocalApVoucherPaymentScheduleHome.class);
            adDiscountHome = (LocalAdDiscountHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdDiscountHome.JNDI_NAME, LocalAdDiscountHome.class);
            adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);
            apPurchaseOrderHome = (LocalApPurchaseOrderHome)EJBHomeFactory.
            	lookUpLocalHome(LocalApPurchaseOrderHome.JNDI_NAME, LocalApPurchaseOrderHome.class);

        } catch (NamingException ex) {

            throw new EJBException(ex.getMessage());

        }

        try {

        	Collection apVoucherPaymentSchedules =
        	    apVoucherPaymentScheduleHome.findOpenVpsByVpsLockAndSplSupplierCode(EJBCommon.FALSE, SPL_SPPLR_CODE, AD_BRNCH, AD_CMPNY);


        	if (apVoucherPaymentSchedules.isEmpty()) {

        		throw new GlobalNoRecordFoundException();

        	}

        	short precisionUnit = this.getGlFcPrecisionUnit(AD_CMPNY);

        	Iterator i = apVoucherPaymentSchedules.iterator();

        	while (i.hasNext()) {

        		LocalApVoucherPaymentSchedule apVoucherPaymentSchedule =
        		   (LocalApVoucherPaymentSchedule)i.next();

        		
        		System.out.println("DOC NUMBER: " + apVoucherPaymentSchedule.getApVoucher().getVouDocumentNumber());
                // verification if vps is already closed
        		if (EJBCommon.roundIt(apVoucherPaymentSchedule.getVpsAmountDue(), precisionUnit) == EJBCommon.roundIt(apVoucherPaymentSchedule.getVpsAmountPaid(), precisionUnit)) continue;

        		System.out.println("DOC NUMBER PASSED: " + apVoucherPaymentSchedule.getApVoucher().getVouDocumentNumber());
        		
        		ApModVoucherPaymentScheduleDetails mdetails = new ApModVoucherPaymentScheduleDetails();


        		
        		
        		mdetails.setVpsCode(apVoucherPaymentSchedule.getVpsCode());
        		mdetails.setVpsNumber(apVoucherPaymentSchedule.getVpsNumber());
        		mdetails.setVpsDueDate(apVoucherPaymentSchedule.getVpsDueDate());
        		mdetails.setVpsAmountDue(EJBCommon.roundIt(apVoucherPaymentSchedule.getVpsAmountDue() -
        		                           apVoucherPaymentSchedule.getVpsAmountPaid(), this.getGlFcPrecisionUnit(AD_CMPNY)));
        		mdetails.setVpsVouCode(apVoucherPaymentSchedule.getApVoucher().getVouCode());
        		mdetails.setVpsVouDocumentNumber(apVoucherPaymentSchedule.getApVoucher().getVouDocumentNumber());
        		mdetails.setVpsVouReferenceNumber(apVoucherPaymentSchedule.getApVoucher().getVouReferenceNumber());
        		mdetails.setVpsVouDescription(apVoucherPaymentSchedule.getApVoucher().getVouDescription());
        		mdetails.setVpsVouFcName(apVoucherPaymentSchedule.getApVoucher().getGlFunctionalCurrency().getFcName());
        		mdetails.setVpsVouConversionDate(apVoucherPaymentSchedule.getApVoucher().getVouConversionDate());
        		mdetails.setVpsVouConversionRate(apVoucherPaymentSchedule.getApVoucher().getVouConversionRate());

        		// calculate default discount
        		short VOUCHER_AGE = (short)((new Date().getTime() -
	                    apVoucherPaymentSchedule.getApVoucher().getVouDate().getTime()) / (1000 * 60 * 60 * 24));

	            double VPS_DSCNT_AMNT = 0d;

		        Collection adDiscounts = adDiscountHome.findByPsLineNumberAndPytName(
		                	apVoucherPaymentSchedule.getVpsNumber(),
		                	apVoucherPaymentSchedule.getApVoucher().getAdPaymentTerm().getPytName(), AD_CMPNY);

	            Iterator j = adDiscounts.iterator();

	            while (j.hasNext()) {

	    	        LocalAdDiscount adDiscount = (LocalAdDiscount)j.next();

	    	        if (adDiscount.getDscPaidWithinDay() >= VOUCHER_AGE) {

	    		        VPS_DSCNT_AMNT = EJBCommon.roundIt(mdetails.getVpsAmountDue() * adDiscount.getDscDiscountPercent() / 100, this.getGlFcPrecisionUnit(AD_CMPNY));

	    		        break;

	    	        }

	            }

	            // calculate default tax withheld

	            LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);

	            double APPLY_AMOUNT = mdetails.getVpsAmountDue() - VPS_DSCNT_AMNT;
	            double W_TAX_AMOUNT = 0d;

	            if (apVoucherPaymentSchedule.getApVoucher().getApWithholdingTaxCode().getWtcRate() != 0 &&
	                adPreference.getPrfApWTaxRealization().equals("PAYMENT")) {

	                LocalApTaxCode apTaxCode = null;

	                if (apVoucherPaymentSchedule.getApVoucher().getApPurchaseOrderLines().isEmpty()) {
	                	apTaxCode = apVoucherPaymentSchedule.getApVoucher().getApTaxCode();
	                } else {

	                	Collection apPurchaseOrderLines = apVoucherPaymentSchedule.getApVoucher().getApPurchaseOrderLines();
	                	ArrayList apPoList = new ArrayList(apPurchaseOrderLines);
	                	apTaxCode = ((LocalApPurchaseOrderLine)apPoList.get(0)).getApPurchaseOrder().getApTaxCode();
	                }

	                double NET_AMOUNT = 0d;

	                if (apTaxCode.getTcType().equals("INCLUSIVE") || apTaxCode.getTcType().equals("EXCLUSIVE")) {

		    			NET_AMOUNT = EJBCommon.roundIt(APPLY_AMOUNT / (1 + (apTaxCode.getTcRate() / 100)), this.getGlFcPrecisionUnit(AD_CMPNY));

		            } else {

		            	NET_AMOUNT = APPLY_AMOUNT;

		            }

	                W_TAX_AMOUNT = EJBCommon.roundIt(NET_AMOUNT * (apVoucherPaymentSchedule.getApVoucher().getApWithholdingTaxCode().getWtcRate() / 100), this.getGlFcPrecisionUnit(AD_CMPNY));

	                APPLY_AMOUNT -= W_TAX_AMOUNT;



	            }

         		mdetails.setVpsAvApplyAmount(APPLY_AMOUNT);
         		mdetails.setVpsAvTaxWithheld(W_TAX_AMOUNT);
        		mdetails.setVpsAvDiscountAmount(VPS_DSCNT_AMNT);
        		System.out.println("DOC NUMBER ADDED: " + apVoucherPaymentSchedule.getApVoucher().getVouDocumentNumber());
        		list.add(mdetails);

        	}

        	if (list.isEmpty()) {

        		throw new GlobalNoRecordFoundException();

        	}

        	return list;

        } catch (GlobalNoRecordFoundException ex) {

        	throw ex;

        } catch (Exception ex) {

        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());

        }

    }



    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList getApPaymentsForProcessingByAdBranchAndAdCompany(Integer AD_BRNCH, Integer AD_CMPNY)
        throws GlobalNoRecordFoundException {

        Debug.print("ApPaymentEntryControllerBean getApPaymentsForProcessingByAdBranchAndAdCompany");

        LocalApVoucherPaymentScheduleHome apVoucherPaymentScheduleHome = null;
        LocalAdDiscountHome adDiscountHome = null;
        LocalAdPreferenceHome adPreferenceHome = null;
        LocalApPurchaseOrderHome apPurchaseOrderHome = null;

        ArrayList list = new ArrayList();
        ArrayList supplierList = new ArrayList();

        // Initialize EJB Home

        try {

            apVoucherPaymentScheduleHome = (LocalApVoucherPaymentScheduleHome)EJBHomeFactory.
                lookUpLocalHome(LocalApVoucherPaymentScheduleHome.JNDI_NAME, LocalApVoucherPaymentScheduleHome.class);
            adDiscountHome = (LocalAdDiscountHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdDiscountHome.JNDI_NAME, LocalAdDiscountHome.class);
            adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);
            apPurchaseOrderHome = (LocalApPurchaseOrderHome)EJBHomeFactory.
            	lookUpLocalHome(LocalApPurchaseOrderHome.JNDI_NAME, LocalApPurchaseOrderHome.class);

        } catch (NamingException ex) {

            throw new EJBException(ex.getMessage());

        }

        try {

        	Collection apVoucherPaymentSchedules =
        	    apVoucherPaymentScheduleHome.findOpenVpsByVpsLock(EJBCommon.FALSE, AD_BRNCH, AD_CMPNY);


        	if (apVoucherPaymentSchedules.isEmpty()) {

        		throw new GlobalNoRecordFoundException();

        	}

        	short precisionUnit = this.getGlFcPrecisionUnit(AD_CMPNY);

        	Iterator i = apVoucherPaymentSchedules.iterator();

        	while (i.hasNext()) {

        		LocalApVoucherPaymentSchedule apVoucherPaymentSchedule =
        		   (LocalApVoucherPaymentSchedule)i.next();

        		if (EJBCommon.roundIt(apVoucherPaymentSchedule.getVpsAmountDue(), precisionUnit) == EJBCommon.roundIt(apVoucherPaymentSchedule.getVpsAmountPaid(), precisionUnit)
        				|| supplierList.contains(apVoucherPaymentSchedule.getApVoucher().getApSupplier().getSplSupplierCode())) continue;

        		ApModVoucherPaymentScheduleDetails mdetails = new ApModVoucherPaymentScheduleDetails();

        		mdetails.setVpsCode(apVoucherPaymentSchedule.getVpsCode());
        		mdetails.setVpsNumber(apVoucherPaymentSchedule.getVpsNumber());
        		mdetails.setVpsDueDate(apVoucherPaymentSchedule.getVpsDueDate());
        		mdetails.setVpsAmountDue(EJBCommon.roundIt(apVoucherPaymentSchedule.getVpsAmountDue() -
        		                           apVoucherPaymentSchedule.getVpsAmountPaid(), this.getGlFcPrecisionUnit(AD_CMPNY)));
        		mdetails.setVpsVouCode(apVoucherPaymentSchedule.getApVoucher().getVouCode());
        		mdetails.setVpsVouDocumentNumber(apVoucherPaymentSchedule.getApVoucher().getApSupplier().getSplSupplierCode());
        		mdetails.setVpsVouReferenceNumber(apVoucherPaymentSchedule.getApVoucher().getApSupplier().getSplName());
        		mdetails.setVpsVouDescription(apVoucherPaymentSchedule.getApVoucher().getVouDescription());
        		mdetails.setVpsVouFcName(apVoucherPaymentSchedule.getApVoucher().getGlFunctionalCurrency().getFcName());

        		supplierList.add(apVoucherPaymentSchedule.getApVoucher().getApSupplier().getSplSupplierCode());
        		// calculate default discount

        		short VOUCHER_AGE = (short)((new Date().getTime() -
	                    apVoucherPaymentSchedule.getApVoucher().getVouDate().getTime()) / (1000 * 60 * 60 * 24));

	            double VPS_DSCNT_AMNT = 0d;

		        Collection adDiscounts = adDiscountHome.findByPsLineNumberAndPytName(
		                	apVoucherPaymentSchedule.getVpsNumber(),
		                	apVoucherPaymentSchedule.getApVoucher().getAdPaymentTerm().getPytName(), AD_CMPNY);

	            Iterator j = adDiscounts.iterator();

	            while (j.hasNext()) {

	    	        LocalAdDiscount adDiscount = (LocalAdDiscount)j.next();

	    	        if (adDiscount.getDscPaidWithinDay() >= VOUCHER_AGE) {

	    		        VPS_DSCNT_AMNT = EJBCommon.roundIt(mdetails.getVpsAmountDue() * adDiscount.getDscDiscountPercent() / 100, this.getGlFcPrecisionUnit(AD_CMPNY));

	    		        break;

	    	        }

	            }

	            // calculate default tax withheld

	            LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);

	            double APPLY_AMOUNT = mdetails.getVpsAmountDue() - VPS_DSCNT_AMNT;
	            double W_TAX_AMOUNT = 0d;

	            if (apVoucherPaymentSchedule.getApVoucher().getApWithholdingTaxCode().getWtcRate() != 0 &&
	                adPreference.getPrfApWTaxRealization().equals("PAYMENT")) {

	                LocalApTaxCode apTaxCode = null;

	                if (apVoucherPaymentSchedule.getApVoucher().getApPurchaseOrderLines().isEmpty()) {
	                	apTaxCode = apVoucherPaymentSchedule.getApVoucher().getApTaxCode();
	                } else {

	                	Collection apPurchaseOrderLines = apVoucherPaymentSchedule.getApVoucher().getApPurchaseOrderLines();
	                	ArrayList apPoList = new ArrayList(apPurchaseOrderLines);
	                	apTaxCode = ((LocalApPurchaseOrderLine)apPoList.get(0)).getApPurchaseOrder().getApTaxCode();
	                }

	                double NET_AMOUNT = 0d;

	                if (apTaxCode.getTcType().equals("INCLUSIVE") || apTaxCode.getTcType().equals("EXCLUSIVE")) {

		    			NET_AMOUNT = EJBCommon.roundIt(APPLY_AMOUNT / (1 + (apTaxCode.getTcRate() / 100)), this.getGlFcPrecisionUnit(AD_CMPNY));

		            } else {

		            	NET_AMOUNT = APPLY_AMOUNT;

		            }

	                W_TAX_AMOUNT = EJBCommon.roundIt(NET_AMOUNT * (apVoucherPaymentSchedule.getApVoucher().getApWithholdingTaxCode().getWtcRate() / 100), this.getGlFcPrecisionUnit(AD_CMPNY));

	                APPLY_AMOUNT -= W_TAX_AMOUNT;



	            }

         		mdetails.setVpsAvApplyAmount(APPLY_AMOUNT);
         		mdetails.setVpsAvTaxWithheld(W_TAX_AMOUNT);
        		mdetails.setVpsAvDiscountAmount(VPS_DSCNT_AMNT);

        		list.add(mdetails);

        	}

        	if (list.isEmpty()) {

        		throw new GlobalNoRecordFoundException();

        	}

        	return list;

        } catch (GlobalNoRecordFoundException ex) {

        	throw ex;

        } catch (Exception ex) {

        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());

        }
    }

    /**
     * @ejb:interface-method view-type="remote"
     **/
    public Integer saveApChkEntry(com.util.ApCheckDetails details, String BA_NM, String FC_NM, String SPL_SPPLR_CODE,
        String CB_NM, ArrayList avList, boolean isDraft, Integer AD_BRNCH, Integer AD_CMPNY) throws
        GlobalRecordAlreadyDeletedException,
		GlobalDocumentNumberNotUniqueException,
		ApCHKCheckNumberNotUniqueException,
		GlobalConversionDateNotExistException,
		GlobalTransactionAlreadyApprovedException,
		GlobalTransactionAlreadyPendingException,
		GlobalTransactionAlreadyPostedException,
		GlobalTransactionAlreadyVoidException,
		ApVOUOverapplicationNotAllowedException,
		GlobalTransactionAlreadyLockedException,
		GlobalNoApprovalRequesterFoundException,
		GlobalNoApprovalApproverFoundException,
		ApCHKVoucherHasNoWTaxCodeException,
		GlJREffectiveDateNoPeriodExistException,
		GlJREffectiveDatePeriodClosedException,
		GlobalJournalNotBalanceException,
		GlobalBranchAccountNumberInvalidException {

        Debug.print("ApPaymentEntryControllerBean saveApChkEntry");

        LocalApCheckHome apCheckHome = null;
        LocalApCheckBatchHome apCheckBatchHome = null;
        LocalAdCompanyHome adCompanyHome = null;
        LocalGlFunctionalCurrencyHome glFunctionalCurrencyHome = null;
        LocalAdDocumentSequenceAssignmentHome adDocumentSequenceAssignmentHome = null;
        LocalAdBankAccountHome adBankAccountHome = null;
        LocalApSupplierHome apSupplierHome = null;
        LocalGlFunctionalCurrencyRateHome glFunctionalCurrencyRateHome = null;
        LocalApDistributionRecordHome apDistributionRecordHome = null;
        LocalAdApprovalHome adApprovalHome = null;
        LocalAdAmountLimitHome adAmountLimitHome = null;
        LocalAdApprovalUserHome adApprovalUserHome = null;
        LocalAdApprovalQueueHome adApprovalQueueHome = null;
        LocalAdPreferenceHome adPreferenceHome = null;
        LocalAdBranchDocumentSequenceAssignmentHome adBranchDocumentSequenceAssignmentHome = null;
        LocalAdBranchBankAccountHome adBranchBankAccountHome = null;

        LocalApCheck apCheck = null;

        // Initialize EJB Home

        try {
        	adBranchBankAccountHome = (LocalAdBranchBankAccountHome)EJBHomeFactory.
                    lookUpLocalHome(LocalAdBranchBankAccountHome.JNDI_NAME, LocalAdBranchBankAccountHome.class);
            apCheckHome = (LocalApCheckHome)EJBHomeFactory.
                lookUpLocalHome(LocalApCheckHome.JNDI_NAME, LocalApCheckHome.class);
            apCheckBatchHome = (LocalApCheckBatchHome)EJBHomeFactory.
            	lookUpLocalHome(LocalApCheckBatchHome.JNDI_NAME, LocalApCheckBatchHome.class);
            adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
            glFunctionalCurrencyHome = (LocalGlFunctionalCurrencyHome)EJBHomeFactory.
                lookUpLocalHome(LocalGlFunctionalCurrencyHome.JNDI_NAME, LocalGlFunctionalCurrencyHome.class);
            adDocumentSequenceAssignmentHome = (LocalAdDocumentSequenceAssignmentHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdDocumentSequenceAssignmentHome.JNDI_NAME, LocalAdDocumentSequenceAssignmentHome.class);
            adBankAccountHome = (LocalAdBankAccountHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdBankAccountHome.JNDI_NAME, LocalAdBankAccountHome.class);
            apSupplierHome = (LocalApSupplierHome)EJBHomeFactory.
                lookUpLocalHome(LocalApSupplierHome.JNDI_NAME, LocalApSupplierHome.class);
            glFunctionalCurrencyRateHome = (LocalGlFunctionalCurrencyRateHome)EJBHomeFactory.
                lookUpLocalHome(LocalGlFunctionalCurrencyRateHome.JNDI_NAME, LocalGlFunctionalCurrencyRateHome.class);
            apDistributionRecordHome = (LocalApDistributionRecordHome)EJBHomeFactory.
                lookUpLocalHome(LocalApDistributionRecordHome.JNDI_NAME, LocalApDistributionRecordHome.class);
            adApprovalHome = (LocalAdApprovalHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdApprovalHome.JNDI_NAME, LocalAdApprovalHome.class);
            adAmountLimitHome = (LocalAdAmountLimitHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdAmountLimitHome.JNDI_NAME, LocalAdAmountLimitHome.class);
            adApprovalUserHome = (LocalAdApprovalUserHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdApprovalUserHome.JNDI_NAME, LocalAdApprovalUserHome.class);
            adApprovalQueueHome = (LocalAdApprovalQueueHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdApprovalQueueHome.JNDI_NAME, LocalAdApprovalQueueHome.class);
            adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);
            adBranchDocumentSequenceAssignmentHome = (LocalAdBranchDocumentSequenceAssignmentHome)EJBHomeFactory.
            	lookUpLocalHome(LocalAdBranchDocumentSequenceAssignmentHome.JNDI_NAME, LocalAdBranchDocumentSequenceAssignmentHome.class);

        } catch (NamingException ex) {

            throw new EJBException(ex.getMessage());

        }




        try {

        	LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);

        	// validate if check is already deleted

        	try {

        		if (details.getChkCode() != null) {

        			apCheck = apCheckHome.findByPrimaryKey(details.getChkCode());

        		}

        	} catch (FinderException ex) {

        		throw new GlobalRecordAlreadyDeletedException();

        	}

        	// validate if check is already posted, void, approved or pending

	        if (details.getChkCode() != null && details.getChkVoid() == EJBCommon.FALSE) {


	        	if (apCheck.getChkApprovalStatus() != null) {

	        		if (apCheck.getChkApprovalStatus().equals("APPROVED") ||
		        		apCheck.getChkApprovalStatus().equals("N/A")) {

	        		    throw new GlobalTransactionAlreadyApprovedException();

	        		} else if (apCheck.getChkApprovalStatus().equals("PENDING")) {

	        			throw new GlobalTransactionAlreadyPendingException();

	        		}

	        	}

        		if (apCheck.getChkPosted() == EJBCommon.TRUE) {

        			throw new GlobalTransactionAlreadyPostedException();

        		} else if (apCheck.getChkVoid() == EJBCommon.TRUE) {

        			throw new GlobalTransactionAlreadyVoidException();

        		}

        	}

        	// check void

	    	if (details.getChkCode() != null && details.getChkVoid() == EJBCommon.TRUE) {

	    		if (apCheck.getChkVoid() == EJBCommon.TRUE) {

        			throw new GlobalTransactionAlreadyVoidException();

        		}

        		if (apCheck.getChkPosted() == EJBCommon.TRUE) {

		    	    // generate approval status

		            String CHK_APPRVL_STATUS = null;

		        	if (!isDraft) {

		        		LocalAdApproval adApproval = adApprovalHome.findByAprAdCompany(AD_CMPNY);

		        		// check if ap check approval is enabled

		        		if (adApproval.getAprEnableApCheck() == EJBCommon.FALSE) {

		        			CHK_APPRVL_STATUS = "N/A";

		        		} else {

		        			// check if check is self approved

		        			LocalAdAmountLimit adAmountLimit = null;

		        			try {

		        				adAmountLimit = adAmountLimitHome.findByAdcTypeAndAuTypeAndUsrName("AP CHECK", "REQUESTER", details.getChkLastModifiedBy(), AD_CMPNY);

		        			} catch (FinderException ex) {

		        				throw new GlobalNoApprovalRequesterFoundException();

		        			}

		        			if (apCheck.getChkAmount() <= adAmountLimit.getCalAmountLimit()) {

		        				CHK_APPRVL_STATUS = "N/A";

		        			} else {

		        				// for approval, create approval queue

		        				 Collection adAmountLimits = adAmountLimitHome.findByAdcTypeAndGreaterThanCalAmountLimit("AP CHECK", adAmountLimit.getCalAmountLimit(), AD_CMPNY);

		        				 if (adAmountLimits.isEmpty()) {

		        				 	Collection adApprovalUsers = adApprovalUserHome.findByAuTypeAndCalCode("APPROVER", adAmountLimit.getCalCode(), AD_CMPNY);

		        				 	if (adApprovalUsers.isEmpty()) {

		        				 		throw new GlobalNoApprovalApproverFoundException();

		        				 	}

		        				 	Iterator j = adApprovalUsers.iterator();

		        				 	while (j.hasNext()) {

		        				 		LocalAdApprovalUser adApprovalUser = (LocalAdApprovalUser)j.next();

		        				 		LocalAdApprovalQueue adApprovalQueue = adApprovalQueueHome.create(EJBCommon.TRUE, "AP CHECK", apCheck.getChkCode(),
		        				 				apCheck.getChkDocumentNumber(), apCheck.getChkDate(), adAmountLimit.getCalAndOr(), adApprovalUser.getAuOr(), AD_BRNCH, AD_CMPNY);

		        				 		adApprovalUser.getAdUser().addAdApprovalQueue(adApprovalQueue);

		        				 	}

		        				 } else {

		        				 	boolean isApprovalUsersFound = false;

		        				 	Iterator i = adAmountLimits.iterator();

		        				 	while (i.hasNext()) {

		        				 		LocalAdAmountLimit adNextAmountLimit = (LocalAdAmountLimit)i.next();

		        				 		if (apCheck.getChkAmount() <= adNextAmountLimit.getCalAmountLimit()) {

		        				 			Collection adApprovalUsers = adApprovalUserHome.findByAuTypeAndCalCode("APPROVER", adAmountLimit.getCalCode(), AD_CMPNY);

		        				 			Iterator j = adApprovalUsers.iterator();

				        				 	while (j.hasNext()) {

				        				 		isApprovalUsersFound = true;

				        				 		LocalAdApprovalUser adApprovalUser = (LocalAdApprovalUser)j.next();

				        				 		LocalAdApprovalQueue adApprovalQueue = adApprovalQueueHome.create(EJBCommon.TRUE, "AP CHECK", apCheck.getChkCode(),
				        				 				apCheck.getChkDocumentNumber(), apCheck.getChkDate(), adAmountLimit.getCalAndOr(), adApprovalUser.getAuOr(), AD_BRNCH, AD_CMPNY);

				        				 		adApprovalUser.getAdUser().addAdApprovalQueue(adApprovalQueue);

				        				 	}

			    				 			break;

		        				 		} else if (!i.hasNext()) {

		        				 			Collection adApprovalUsers = adApprovalUserHome.findByAuTypeAndCalCode("APPROVER", adNextAmountLimit.getCalCode(), AD_CMPNY);

		        				 			Iterator j = adApprovalUsers.iterator();

				        				 	while (j.hasNext()) {

				        				 		isApprovalUsersFound = true;

				        				 		LocalAdApprovalUser adApprovalUser = (LocalAdApprovalUser)j.next();

				        				 		LocalAdApprovalQueue adApprovalQueue = adApprovalQueueHome.create(EJBCommon.TRUE, "AP CHECK", apCheck.getChkCode(),
				        				 				apCheck.getChkDocumentNumber(), apCheck.getChkDate(), adNextAmountLimit.getCalAndOr(), adApprovalUser.getAuOr(), AD_BRNCH, AD_CMPNY);

				        				 		adApprovalUser.getAdUser().addAdApprovalQueue(adApprovalQueue);

				        				 	}

			    				 			break;

		        				 		}

		        				 		adAmountLimit = adNextAmountLimit;

		        				 	}

		        				 	if (!isApprovalUsersFound) {

		        				 		throw new GlobalNoApprovalApproverFoundException();

		        				 	}

		        			    }

		        			    CHK_APPRVL_STATUS = "PENDING";
		        			}
		        		}
		        	}


		    		// reverse distribution records

		    		Collection apDistributionRecords = apCheck.getApDistributionRecords();
		    		ArrayList list = new ArrayList();

		    		Iterator i = apDistributionRecords.iterator();

		    		while (i.hasNext()) {

		    			LocalApDistributionRecord apDistributionRecord = (LocalApDistributionRecord)i.next();

		    			list.add(apDistributionRecord);

		    		}

		    		i = list.iterator();

		    		while (i.hasNext()) {

		    			LocalApDistributionRecord apDistributionRecord = (LocalApDistributionRecord)i.next();

		    			this.addApDrEntry(apCheck.getApDrNextLine(), apDistributionRecord.getDrClass(),
		    			    apDistributionRecord.getDrDebit() == EJBCommon.TRUE ? EJBCommon.FALSE : EJBCommon.TRUE,
		    			    apDistributionRecord.getDrAmount(), EJBCommon.TRUE, apDistributionRecord.getGlChartOfAccount().getCoaCode(),
		    			    apCheck, apDistributionRecord.getApAppliedVoucher(), AD_BRNCH, AD_CMPNY);

		    		}



		        	if (CHK_APPRVL_STATUS != null && CHK_APPRVL_STATUS.equals("N/A") && adPreference.getPrfApGlPostingType().equals("AUTO-POST UPON APPROVAL")) {

		        		apCheck.setChkVoid(EJBCommon.TRUE);
		        		this.executeApChkPost(apCheck.getChkCode(), details.getChkLastModifiedBy(), AD_BRNCH, AD_CMPNY);

		        	}

		        	// set void approval status

		    	    apCheck.setChkVoidApprovalStatus(CHK_APPRVL_STATUS);

		        } else {

		        	// release voucher lock

		        	Collection apLockedAppliedVouchers = apCheck.getApAppliedVouchers();

		        	Iterator avIter = apLockedAppliedVouchers.iterator();

		        	while (avIter.hasNext()) {

		        		LocalApAppliedVoucher apAppliedVoucher =
		        		    (LocalApAppliedVoucher) avIter.next();

		        	    apAppliedVoucher.getApVoucherPaymentSchedule().setVpsLock(EJBCommon.FALSE);

		        	}

		        }

		        apCheck.setChkVoid(EJBCommon.TRUE);
		        apCheck.setChkLastModifiedBy(details.getChkLastModifiedBy());
        		apCheck.setChkDateLastModified(details.getChkDateLastModified());

	    	    return apCheck.getChkCode();

	    	}

        	// validate if document number is unique document number is automatic then set next sequence

        	try {

        	    LocalAdBranchDocumentSequenceAssignment adBranchDocumentSequenceAssignment = null;
	 			LocalAdDocumentSequenceAssignment adDocumentSequenceAssignment = null;

	        	if (details.getChkCode() == null) {

	        	    try {

	        	        adDocumentSequenceAssignment = adDocumentSequenceAssignmentHome.findByDcName("AP CHECK", AD_CMPNY);

	        	    } catch(FinderException ex) { }

	        	    try {

	 					adBranchDocumentSequenceAssignment = adBranchDocumentSequenceAssignmentHome.findBdsByDsaCodeAndBrCode(adDocumentSequenceAssignment.getDsaCode(), AD_BRNCH, AD_CMPNY);

	 				} catch (FinderException ex) { }


		    		LocalApCheck apExistingCheck = null;

		    		try {

		    		    apExistingCheck = apCheckHome.findByChkDocumentNumberAndBrCode(
		        		    details.getChkDocumentNumber(), AD_BRNCH, AD_CMPNY);

		        	} catch (FinderException ex) {
		        	}

		            if (apExistingCheck != null) {

		            	throw new GlobalDocumentNumberNotUniqueException();

		            }

			        if (adDocumentSequenceAssignment.getAdDocumentSequence().getDsNumberingType() == 'A' &&
			            (details.getChkDocumentNumber() == null || details.getChkDocumentNumber().trim().length() == 0)) {

			            while (true) {

			                if (adBranchDocumentSequenceAssignment == null || adBranchDocumentSequenceAssignment.getBdsNextSequence() == null) {

				            	try {

				            		apCheckHome.findByChkDocumentNumberAndBrCode(adDocumentSequenceAssignment.getDsaNextSequence(), AD_BRNCH, AD_CMPNY);
				            		adDocumentSequenceAssignment.setDsaNextSequence(EJBCommon.incrementStringNumber(adDocumentSequenceAssignment.getDsaNextSequence()));

				            	} catch (FinderException ex) {

				            		details.setChkDocumentNumber(adDocumentSequenceAssignment.getDsaNextSequence());
				            		adDocumentSequenceAssignment.setDsaNextSequence(EJBCommon.incrementStringNumber(adDocumentSequenceAssignment.getDsaNextSequence()));
						            break;

				            	}

			                } else {

			                    try {

				            		apCheckHome.findByChkDocumentNumberAndBrCode(adBranchDocumentSequenceAssignment.getBdsNextSequence(), AD_BRNCH, AD_CMPNY);
				            		adBranchDocumentSequenceAssignment.setBdsNextSequence(EJBCommon.incrementStringNumber(adBranchDocumentSequenceAssignment.getBdsNextSequence()));

				            	} catch (FinderException ex) {

				            		details.setChkDocumentNumber(adBranchDocumentSequenceAssignment.getBdsNextSequence());
				            		adBranchDocumentSequenceAssignment.setBdsNextSequence(EJBCommon.incrementStringNumber(adBranchDocumentSequenceAssignment.getBdsNextSequence()));
						            break;

				            	}

			                }

			            }

			        }

			    } else {

			    	LocalApCheck apExistingCheck = null;

			    	try {

		    		    apExistingCheck = apCheckHome.findByChkDocumentNumberAndBrCode(
		        		    details.getChkDocumentNumber(), AD_BRNCH, AD_CMPNY);

		        	} catch (FinderException ex) {
		        	}

		        	if (apExistingCheck != null &&
		                !apExistingCheck.getChkCode().equals(details.getChkCode())) {

		            	throw new GlobalDocumentNumberNotUniqueException();

		            }

		            if (apCheck.getChkDocumentNumber() != details.getChkDocumentNumber() &&
		                (details.getChkDocumentNumber() == null || details.getChkDocumentNumber().trim().length() == 0)) {

		                details.setChkDocumentNumber(apCheck.getChkDocumentNumber());

		         	}

			    }

        	} catch (GlobalDocumentNumberNotUniqueException ex) {

	    	    getSessionContext().setRollbackOnly();
	    	    throw ex;

	    	} catch (Exception ex) {

	    	    Debug.printStackTrace(ex);
	    	    getSessionContext().setRollbackOnly();
	    	    throw new EJBException(ex.getMessage());

	    	}

		    // validate if check number is unique check is automatic then set next sequence

        	if (details.getChkCode() == null) {

	    		LocalApCheck apExistingCheck = null;

	    		try {

	    		    apExistingCheck = apCheckHome.findByChkNumberAndBaName(
	        		    details.getChkNumber(), BA_NM, AD_CMPNY);

	        	} catch (FinderException ex) {
	        	}

	            if (apExistingCheck != null) {

	            	throw new ApCHKCheckNumberNotUniqueException();

	            }

		        LocalAdBankAccount adBankAccount = adBankAccountHome.findByBaName(BA_NM, AD_CMPNY);

		        if (details.getChkNumber() == null || details.getChkNumber().trim().length() == 0) {

		     		while (true) {

		            	try {

		            		apCheckHome.findByChkNumberAndBaName(adBankAccount.getBaNextCheckNumber(), BA_NM, AD_CMPNY);
		            		adBankAccount.setBaNextCheckNumber(EJBCommon.incrementStringNumber(adBankAccount.getBaNextCheckNumber()));

		            	} catch (FinderException ex) {

		            		details.setChkNumber(adBankAccount.getBaNextCheckNumber());
		            		adBankAccount.setBaNextCheckNumber(EJBCommon.incrementStringNumber(adBankAccount.getBaNextCheckNumber()));
				            break;

		            	}

		            }

		        }

		    } else {

		    	LocalApCheck apExistingCheck = null;

		    	try {

	    		    apExistingCheck = apCheckHome.findByChkNumberAndBaName(
	        		    details.getChkNumber(), BA_NM, AD_CMPNY);

	        	} catch (FinderException ex) {
	        	}

	        	if (apExistingCheck != null &&
	                !apExistingCheck.getChkCode().equals(details.getChkCode())) {

	            	throw new ApCHKCheckNumberNotUniqueException();

	            }

	            if (apCheck.getChkNumber() != details.getChkNumber() &&
	                (details.getChkNumber() == null || details.getChkNumber().trim().length() == 0)) {

	                details.setChkNumber(apCheck.getChkNumber());

	         	}

		    }

        	// validate if conversion date exists

            try {

                if (details.getChkConversionDate() != null) {


                    LocalGlFunctionalCurrency glValidateFunctionalCurrency = glFunctionalCurrencyHome.findByFcName(FC_NM, AD_CMPNY);
                    LocalGlFunctionalCurrencyRate glFunctionalCurrencyRate =
                        glFunctionalCurrencyRateHome.findByFcCodeAndDate(glValidateFunctionalCurrency.getFcCode(),
                                details.getChkConversionDate(), AD_CMPNY);

                    details.setChkConversionRate(glFunctionalCurrencyRate.getFrXToUsd());

                }else{
                	if(details.getChkConversionRate() == 0 ){
                		details.setChkConversionRate(1d);
                	}
                }

            } catch (FinderException ex) {

                throw new GlobalConversionDateNotExistException();

            }

	        // used in checking if check should re-generate distribution records and re-calculate taxes
	        boolean isRecalculate = true;

        	// create check

        	if (details.getChkCode() == null) {

				apCheck = apCheckHome.create("PAYMENT", details.getChkDescription(),
				    details.getChkDate(), details.getChkCheckDate(), details.getChkNumber(), details.getChkDocumentNumber(),
				    details.getChkReferenceNumber(), null,null,null,null,null,
				    EJBCommon.FALSE, EJBCommon.FALSE, null, null, null, 0d, 0d, 0d, 0d, 0d,
				    details.getChkLoan(), details.getChkLoanGenerated(),
				    details.getChkConversionDate(), details.getChkConversionRate(),
				    0d, details.getChkAmount(), null, null, EJBCommon.FALSE,
				    EJBCommon.FALSE, details.getChkCrossCheck(), null, EJBCommon.FALSE, details.getChkCreatedBy(),
				    details.getChkDateCreated(), details.getChkLastModifiedBy(),
				    details.getChkDateLastModified(), null, null, null, null, EJBCommon.FALSE, null,
				    EJBCommon.FALSE, null, EJBCommon.FALSE, EJBCommon.FALSE, details.getChkMemo(), null, AD_BRNCH, AD_CMPNY);

        	} else {

        		// check if critical fields are changed

        		if (!apCheck.getAdBankAccount().getBaName().equals(BA_NM) ||
					!apCheck.getApSupplier().getSplSupplierCode().equals(SPL_SPPLR_CODE) ||
					apCheck.getChkConversionRate() != details.getChkConversionRate() ||
					avList.size() != apCheck.getApAppliedVouchers().size()) {

        			isRecalculate = true;

        		} else if (avList.size() == apCheck.getApAppliedVouchers().size()) {

        			Iterator avIter = apCheck.getApAppliedVouchers().iterator();
        			Iterator avListIter = avList.iterator();

        			while (avIter.hasNext()) {

        				LocalApAppliedVoucher apAppliedVoucher = (LocalApAppliedVoucher)avIter.next();
        				ApModAppliedVoucherDetails mdetails = (ApModAppliedVoucherDetails)avListIter.next();

        				if (!apAppliedVoucher.getApVoucherPaymentSchedule().getVpsCode().equals(mdetails.getAvVpsCode()) ||
        					apAppliedVoucher.getAvApplyAmount() != mdetails.getAvApplyAmount() ||
        				    apAppliedVoucher.getAvTaxWithheld() != mdetails.getAvTaxWithheld() ||
        				    apAppliedVoucher.getAvDiscountAmount() != mdetails.getAvDiscountAmount() ||
        				    apAppliedVoucher.getAvAllocatedPaymentAmount() != mdetails.getAvAllocatedPaymentAmount()) {

        					isRecalculate = true;
        					break;

        				}

        				isRecalculate = false;

        			}

        		} else {

        			isRecalculate = false;

        		}

        		apCheck.setChkDescription(details.getChkDescription());
        		apCheck.setChkDate(details.getChkDate());
        		apCheck.setChkCheckDate(details.getChkCheckDate());
        		apCheck.setChkNumber(details.getChkNumber());
        		apCheck.setChkDocumentNumber(details.getChkDocumentNumber());
        		apCheck.setChkReferenceNumber(details.getChkReferenceNumber());
        		apCheck.setChkConversionDate(details.getChkConversionDate());
        		apCheck.setChkConversionRate(details.getChkConversionRate());
        		apCheck.setChkAmount(details.getChkAmount());
        		apCheck.setChkCrossCheck(details.getChkCrossCheck());
        		apCheck.setChkLastModifiedBy(details.getChkLastModifiedBy());
        		apCheck.setChkDateLastModified(details.getChkDateLastModified());
        		apCheck.setChkReasonForRejection(null);
        		apCheck.setChkMemo(details.getChkMemo());
        		apCheck.setChkSupplierName(null);

        		apCheck.setChkLoan(details.getChkLoan());
        		apCheck.setChkLoanGenerated(details.getChkLoanGenerated());
        	}

        	LocalAdBankAccount adBankAccount = adBankAccountHome.findByBaName(BA_NM, AD_CMPNY);
        	apCheck.setAdBankAccount(adBankAccount);

        	LocalGlFunctionalCurrency glFunctionalCurrency = glFunctionalCurrencyHome.findByFcName(FC_NM, AD_CMPNY);
        	apCheck.setGlFunctionalCurrency(glFunctionalCurrency);

        	LocalApSupplier apSupplier = apSupplierHome.findBySplSupplierCode(SPL_SPPLR_CODE, AD_CMPNY);
        	apCheck.setApSupplier(apSupplier);

        	if(details.getChkSupplierName().length() > 0 && !apSupplier.getSplName().equals(details.getChkSupplierName())) {
				apCheck.setChkSupplierName(details.getChkSupplierName());
			}

        	try {

        		LocalApCheckBatch apCheckBatch = apCheckBatchHome.findByPaymentCbName(CB_NM, AD_BRNCH, AD_CMPNY);
        		apCheck.setApCheckBatch(apCheckBatch);

        	} catch (FinderException ex) {

        	}

        	if (isRecalculate) {

	        	// remove all distribution records

		  	    Collection apDistributionRecords = apCheck.getApDistributionRecords();

		  	    Iterator i = apDistributionRecords.iterator();

		  	    while (i.hasNext()) {

		  	   	    LocalApDistributionRecord apDistributionRecord = (LocalApDistributionRecord)i.next();

		  	  	    i.remove();

		  	  	    apDistributionRecord.remove();

		  	    }


	        	// release vps locks and remove all applied vouchers

		  	    Collection apAppliedVouchers = apCheck.getApAppliedVouchers();

		  	    i = apAppliedVouchers.iterator();

		  	    while (i.hasNext()) {

		  	   	    LocalApAppliedVoucher apAppliedVoucher = (LocalApAppliedVoucher)i.next();

		  	   	    apAppliedVoucher.getApVoucherPaymentSchedule().setVpsLock(EJBCommon.FALSE);

		  	  	    i.remove();

		  	  	    apAppliedVoucher.remove();

		  	    }

		  	    // add new applied vouchers and distribution record

		  	    double totalApplyAmount = 0d;
		  	    double AV_FRX_GN_LSS = 0d;

		  	  double VOU_CONVERSION_RT = 0d;
		  	  double CHK_CONVERSION_RT = 0d;
		  	  LocalApDistributionRecord apDistributionRecord = null;

	      	    i = avList.iterator();

	      	    while (i.hasNext()) {

	      	  	    ApModAppliedVoucherDetails mAvDetails = (ApModAppliedVoucherDetails) i.next();
	      	  	    totalApplyAmount += mAvDetails.getAvApplyAmount();

	      	  	    LocalApAppliedVoucher apAppliedVoucher = this.addApAvEntry(mAvDetails, apCheck, AD_CMPNY);

	      	  	    VOU_CONVERSION_RT = apAppliedVoucher.getApVoucherPaymentSchedule().getApVoucher().getVouConversionRate();
	      	  	    CHK_CONVERSION_RT = apAppliedVoucher.getApCheck().getChkConversionRate();

	      	  	    AV_FRX_GN_LSS += apAppliedVoucher.getAvForexGainLoss();

	      	  	    // get payable account
	      	  	    apDistributionRecord = apDistributionRecordHome.findByDrClassAndVouCode("PAYABLE", apAppliedVoucher.getApVoucherPaymentSchedule().getApVoucher().getVouCode(), AD_CMPNY);


	      	    }


	      	    // add cash distribution

		      	  LocalAdBranchBankAccount adBranchBankAccount = null;

	              try {
	                  adBranchBankAccount = adBranchBankAccountHome.findBbaByBaCodeAndBrCode(apCheck.getAdBankAccount().getBaCode(), AD_BRNCH, AD_CMPNY);

	              } catch(FinderException ex) {

	              }

	      	    // create cash distribution record

	            double APPLY_AMOUNT = this.convertForeignToFunctionalCurrency(apCheck.getGlFunctionalCurrency().getFcCode(),
                      apCheck.getGlFunctionalCurrency().getFcName(),
                      apCheck.getChkConversionDate(),
                      apCheck.getChkConversionRate(),
                      totalApplyAmount, AD_CMPNY);

	            double PAYABLE_AMOUNT = this.convertForeignToFunctionalCurrency(apDistributionRecord.getApVoucher().getGlFunctionalCurrency().getFcCode(),
	            			apDistributionRecord.getApVoucher().getGlFunctionalCurrency().getFcName(),
	            			apDistributionRecord.getApVoucher().getVouConversionDate(),
	            			apDistributionRecord.getApVoucher().getVouConversionRate(),
	                      totalApplyAmount, AD_CMPNY);

	            if(CHK_CONVERSION_RT > VOU_CONVERSION_RT) {

	            	this.addApDrEntry(apCheck.getApDrNextLine(), "FOREX GAIN", EJBCommon.FALSE,
	            			Math.abs(AV_FRX_GN_LSS), EJBCommon.FALSE, adPreference.getPrfMiscPosGiftCertificateAccount(),
		      	    		apCheck, null, AD_BRNCH, AD_CMPNY);
	            }

	            if(VOU_CONVERSION_RT > CHK_CONVERSION_RT) {

	            	this.addApDrEntry(apCheck.getApDrNextLine(), "FOREX LOSS", EJBCommon.TRUE,
	            			Math.abs(AV_FRX_GN_LSS), EJBCommon.FALSE, adPreference.getPrfMiscPosGiftCertificateAccount(),
		      	    		apCheck, null, AD_BRNCH, AD_CMPNY);
	            }


	            this.addApDrEntry(apCheck.getApDrNextLine(), "PAYABLE", EJBCommon.TRUE,
	            		PAYABLE_AMOUNT, EJBCommon.FALSE, apDistributionRecord.getGlChartOfAccount().getCoaCode(),
		      	  	    apCheck, null, AD_BRNCH, AD_CMPNY);

	      	    this.addApDrEntry(apCheck.getApDrNextLine(), "CASH", EJBCommon.FALSE,
	      	    		APPLY_AMOUNT, EJBCommon.FALSE, adBranchBankAccount.getBbaGlCoaCashAccount(),
	      	    		apCheck, null, AD_BRNCH, AD_CMPNY);

        	}

      	    // generate approval status

            String CHK_APPRVL_STATUS = null;

        	if (!isDraft) {

        		LocalAdApproval adApproval = adApprovalHome.findByAprAdCompany(AD_CMPNY);

        		// check if ap check approval is enabled

        		if (adApproval.getAprEnableApCheck() == EJBCommon.FALSE) {

        			CHK_APPRVL_STATUS = "N/A";

        		} else {

        			// check if check is self approved

        			LocalAdAmountLimit adAmountLimit = null;

        			try {

        				adAmountLimit = adAmountLimitHome.findByAdcTypeAndAuTypeAndUsrName("AP CHECK", "REQUESTER", details.getChkLastModifiedBy(), AD_CMPNY);

        			} catch (FinderException ex) {

        				throw new GlobalNoApprovalRequesterFoundException();

        			}

        			if (apCheck.getChkAmount() <= adAmountLimit.getCalAmountLimit()) {

        				CHK_APPRVL_STATUS = "N/A";

        			} else {

        				// for approval, create approval queue

        				 Collection adAmountLimits = adAmountLimitHome.findByAdcTypeAndGreaterThanCalAmountLimit("AP CHECK", adAmountLimit.getCalAmountLimit(), AD_CMPNY);

        				 if (adAmountLimits.isEmpty()) {

        				 	Collection adApprovalUsers = adApprovalUserHome.findByAuTypeAndCalCode("APPROVER", adAmountLimit.getCalCode(), AD_CMPNY);

        				 	if (adApprovalUsers.isEmpty()) {

        				 		throw new GlobalNoApprovalApproverFoundException();

        				 	}

        				 	Iterator j = adApprovalUsers.iterator();

        				 	while (j.hasNext()) {

        				 		LocalAdApprovalUser adApprovalUser = (LocalAdApprovalUser)j.next();

        				 		LocalAdApprovalQueue adApprovalQueue = adApprovalQueueHome.create(EJBCommon.TRUE, "AP CHECK", apCheck.getChkCode(),
        				 				apCheck.getChkDocumentNumber(), apCheck.getChkDate(), adAmountLimit.getCalAndOr(), adApprovalUser.getAuOr(), AD_BRNCH, AD_CMPNY);

        				 		adApprovalUser.getAdUser().addAdApprovalQueue(adApprovalQueue);

        				 	}

        				 } else {

        				 	boolean isApprovalUsersFound = false;

        				 	Iterator i = adAmountLimits.iterator();

        				 	while (i.hasNext()) {

        				 		LocalAdAmountLimit adNextAmountLimit = (LocalAdAmountLimit)i.next();

        				 		if (apCheck.getChkAmount() <= adNextAmountLimit.getCalAmountLimit()) {

        				 			Collection adApprovalUsers = adApprovalUserHome.findByAuTypeAndCalCode("APPROVER", adAmountLimit.getCalCode(), AD_CMPNY);

        				 			Iterator j = adApprovalUsers.iterator();

		        				 	while (j.hasNext()) {

		        				 		isApprovalUsersFound = true;

		        				 		LocalAdApprovalUser adApprovalUser = (LocalAdApprovalUser)j.next();

		        				 		LocalAdApprovalQueue adApprovalQueue = adApprovalQueueHome.create(EJBCommon.TRUE, "AP CHECK", apCheck.getChkCode(),
		        				 				apCheck.getChkDocumentNumber(), apCheck.getChkDate(), adAmountLimit.getCalAndOr(), adApprovalUser.getAuOr(), AD_BRNCH, AD_CMPNY);

		        				 		adApprovalUser.getAdUser().addAdApprovalQueue(adApprovalQueue);

		        				 	}

	    				 			break;

        				 		} else if (!i.hasNext()) {

        				 			Collection adApprovalUsers = adApprovalUserHome.findByAuTypeAndCalCode("APPROVER", adNextAmountLimit.getCalCode(), AD_CMPNY);

        				 			Iterator j = adApprovalUsers.iterator();

		        				 	while (j.hasNext()) {

		        				 		isApprovalUsersFound = true;

		        				 		LocalAdApprovalUser adApprovalUser = (LocalAdApprovalUser)j.next();

		        				 		LocalAdApprovalQueue adApprovalQueue = adApprovalQueueHome.create(EJBCommon.TRUE, "AP CHECK", apCheck.getChkCode(),
		        				 				apCheck.getChkDocumentNumber(), apCheck.getChkDate(), adNextAmountLimit.getCalAndOr(), adApprovalUser.getAuOr(), AD_BRNCH, AD_CMPNY);

		        				 		adApprovalUser.getAdUser().addAdApprovalQueue(adApprovalQueue);

		        				 	}

	    				 			break;

        				 		}

        				 		adAmountLimit = adNextAmountLimit;

        				 	}

        				 	if (!isApprovalUsersFound) {

        				 		throw new GlobalNoApprovalApproverFoundException();

        				 	}

        			    }

        			    CHK_APPRVL_STATUS = "PENDING";
        			}
        		}
        	}


        	if (CHK_APPRVL_STATUS != null && CHK_APPRVL_STATUS.equals("N/A") && adPreference.getPrfApGlPostingType().equals("AUTO-POST UPON APPROVAL")) {

        		this.executeApChkPost(apCheck.getChkCode(), apCheck.getChkLastModifiedBy(), AD_BRNCH, AD_CMPNY);

        	}

        	// set check approval status

        	apCheck.setChkApprovalStatus(CHK_APPRVL_STATUS);

      	    return apCheck.getChkCode();


        } catch (GlobalRecordAlreadyDeletedException ex) {

        	getSessionContext().setRollbackOnly();
        	throw ex;

        } catch (GlobalDocumentNumberNotUniqueException ex) {

        	getSessionContext().setRollbackOnly();
        	throw ex;

        } catch (ApCHKCheckNumberNotUniqueException ex) {

        	getSessionContext().setRollbackOnly();
        	throw ex;

        } catch (GlobalConversionDateNotExistException ex) {

        	getSessionContext().setRollbackOnly();
        	throw ex;

        } catch (GlobalTransactionAlreadyApprovedException ex) {

        	getSessionContext().setRollbackOnly();
        	throw ex;

        } catch (GlobalTransactionAlreadyPendingException ex) {

        	getSessionContext().setRollbackOnly();
        	throw ex;

        } catch (GlobalTransactionAlreadyPostedException ex) {

        	getSessionContext().setRollbackOnly();
        	throw ex;

        } catch (GlobalTransactionAlreadyVoidException ex) {

        	getSessionContext().setRollbackOnly();
        	throw ex;

        } catch (ApVOUOverapplicationNotAllowedException ex) {

        	getSessionContext().setRollbackOnly();
        	throw ex;

        } catch (ApCHKVoucherHasNoWTaxCodeException ex) {

        	getSessionContext().setRollbackOnly();
        	throw ex;

        } catch (GlobalTransactionAlreadyLockedException ex) {

        	getSessionContext().setRollbackOnly();
        	throw ex;

        } catch (GlobalNoApprovalRequesterFoundException ex) {

        	getSessionContext().setRollbackOnly();
        	throw ex;

        } catch (GlobalNoApprovalApproverFoundException ex) {

        	getSessionContext().setRollbackOnly();
        	throw ex;

        } catch (GlJREffectiveDateNoPeriodExistException ex) {

        	getSessionContext().setRollbackOnly();
        	throw ex;

        } catch (GlJREffectiveDatePeriodClosedException ex) {

        	getSessionContext().setRollbackOnly();
        	throw ex;

        } catch (GlobalJournalNotBalanceException ex) {

        	getSessionContext().setRollbackOnly();
        	throw ex;

        } catch (GlobalBranchAccountNumberInvalidException ex) {

            getSessionContext().setRollbackOnly();
            throw ex;

        } catch (Exception ex) {

        	Debug.printStackTrace(ex);
        	getSessionContext().setRollbackOnly();
        	throw new EJBException(ex.getMessage());

        }


    }


    /**
     * @ejb:interface-method view-type="remote"
     **/
    public void deleteApChkEntry(Integer CHK_CODE, String AD_USR, Integer AD_CMPNY) throws
        GlobalRecordAlreadyDeletedException {

        Debug.print("ApPaymentEntryControllerBean deleteApChkEntry");

        LocalApCheckHome apCheckHome = null;
        LocalApAppliedVoucherHome apAppliedVoucherHome = null;
        LocalAdApprovalQueueHome adApprovalQueueHome = null;
        LocalAdDeleteAuditTrailHome adDeleteAuditTrailHome = null;

        // Initialize EJB Home

        try {

            apCheckHome = (LocalApCheckHome)EJBHomeFactory.
                lookUpLocalHome(LocalApCheckHome.JNDI_NAME, LocalApCheckHome.class);
            adApprovalQueueHome = (LocalAdApprovalQueueHome)EJBHomeFactory.
			    lookUpLocalHome(LocalAdApprovalQueueHome.JNDI_NAME, LocalAdApprovalQueueHome.class);
            apAppliedVoucherHome = (LocalApAppliedVoucherHome)EJBHomeFactory.
				lookUpLocalHome(LocalApAppliedVoucherHome.JNDI_NAME, LocalApAppliedVoucherHome.class);
            adDeleteAuditTrailHome = (LocalAdDeleteAuditTrailHome)EJBHomeFactory.
            	lookUpLocalHome(LocalAdDeleteAuditTrailHome.JNDI_NAME, LocalAdDeleteAuditTrailHome.class);

        } catch (NamingException ex) {

            throw new EJBException(ex.getMessage());

        }

        try {

        	LocalApCheck apCheck = apCheckHome.findByPrimaryKey(CHK_CODE);

        	if (apCheck.getChkApprovalStatus() != null && apCheck.getChkApprovalStatus().equals("PENDING")) {

        		Collection adApprovalQueues = adApprovalQueueHome.findByAqDocumentAndAqDocumentCode("AP CHECK", apCheck.getChkCode(), AD_CMPNY);

        		Iterator i = adApprovalQueues.iterator();

        		while(i.hasNext()) {

        			LocalAdApprovalQueue adApprovalQueue = (LocalAdApprovalQueue)i.next();

        			adApprovalQueue.remove();

        		}

        	}

        	// release lock and remove applied voucher

      	    Collection apAppliedVouchers = apCheck.getApAppliedVouchers();

  	   		Iterator i = apAppliedVouchers.iterator();

  	   		while (i.hasNext()) {

      	   	   LocalApAppliedVoucher apAppliedVoucher = (LocalApAppliedVoucher)i.next();

    	   	   apAppliedVoucher.getApVoucherPaymentSchedule().setVpsLock(EJBCommon.FALSE);

    	   	   i.remove();

    	   	   apAppliedVoucher.remove();

  	        }

  	   		adDeleteAuditTrailHome.create("AP CHECK", apCheck.getChkDate(), apCheck.getChkDocumentNumber(), apCheck.getChkReferenceNumber(),
					apCheck.getChkAmount(), AD_USR, new Date(), AD_CMPNY);

        	apCheck.remove();

        } catch (FinderException ex) {

            getSessionContext().setRollbackOnly();
            throw new GlobalRecordAlreadyDeletedException();

        } catch (Exception ex) {

        	Debug.printStackTrace(ex);
        	getSessionContext().setRollbackOnly();
        	throw new EJBException(ex.getMessage());

        }

    }

   /**
    * @ejb:interface-method view-type="remote"
    * @jboss:method-attributes read-only="true"
    **/
    public short getGlFcPrecisionUnit(Integer AD_CMPNY) {

       Debug.print("ApPaymentEntryControllerBean getGlFcPrecisionUnit");


       LocalAdCompanyHome adCompanyHome = null;


       // Initialize EJB Home

       try {

           adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
              lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);

       } catch (NamingException ex) {

           throw new EJBException(ex.getMessage());

       }

       try {

         LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);

         return  adCompany.getGlFunctionalCurrency().getFcPrecision();

       } catch (Exception ex) {

       	 Debug.printStackTrace(ex);
         throw new EJBException(ex.getMessage());

       }

    }


    /**
    * @ejb:interface-method view-type="remote"
    * @jboss:method-attributes read-only="true"
    **/
    public ArrayList getAdApprovalNotifiedUsersByChkCode(Integer CHK_CODE, Integer AD_CMPNY) {

       Debug.print("ApPaymentEntryControllerBean getAdApprovalNotifiedUsersByChkCode");


       LocalAdApprovalQueueHome adApprovalQueueHome = null;
       LocalApCheckHome apCheckHome = null;

       ArrayList list = new ArrayList();


       // Initialize EJB Home

       try {

           adApprovalQueueHome = (LocalAdApprovalQueueHome)EJBHomeFactory.
              lookUpLocalHome(LocalAdApprovalQueueHome.JNDI_NAME, LocalAdApprovalQueueHome.class);
           apCheckHome = (LocalApCheckHome)EJBHomeFactory.
              lookUpLocalHome(LocalApCheckHome.JNDI_NAME, LocalApCheckHome.class);

       } catch (NamingException ex) {

           throw new EJBException(ex.getMessage());

       }

       try {

         LocalApCheck apCheck = apCheckHome.findByPrimaryKey(CHK_CODE);

         if (apCheck.getChkVoid() == EJBCommon.FALSE && apCheck.getChkPosted() == EJBCommon.TRUE) {

         	list.add("DOCUMENT POSTED");
         	return list;

         } else if (apCheck.getChkVoid() == EJBCommon.TRUE && apCheck.getChkVoidPosted() == EJBCommon.TRUE) {

         	list.add("DOCUMENT POSTED");
         	return list;

         }

         Collection adApprovalQueues = adApprovalQueueHome.findByAqDocumentAndAqDocumentCode("AP CHECK", CHK_CODE, AD_CMPNY);

         Iterator i = adApprovalQueues.iterator();

         while(i.hasNext()) {

         	LocalAdApprovalQueue adApprovalQueue = (LocalAdApprovalQueue)i.next();

         	list.add(adApprovalQueue.getAdUser().getUsrDescription());

         }

         return list;

       } catch (Exception ex) {

       	 Debug.printStackTrace(ex);
         throw new EJBException(ex.getMessage());

       }

    }



    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
     public byte getAdPrfEnableApCheckBatch(Integer AD_CMPNY) {

        Debug.print("ApPaymentEntryControllerBean getAdPrfEnableApCheckBatch");

        LocalAdPreferenceHome adPreferenceHome = null;


        // Initialize EJB Home

        try {

           adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
               lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);

        } catch (NamingException ex) {

           throw new EJBException(ex.getMessage());

        }


        try {

           LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);

           return adPreference.getPrfEnableApCheckBatch();

        } catch (Exception ex) {

           Debug.printStackTrace(ex);
           throw new EJBException(ex.getMessage());

        }

     }


     /**
      * @ejb:interface-method view-type="remote"
      * @jboss:method-attributes read-only="true"
      **/
     public ArrayList getApOpenCbAll(Integer AD_BRNCH, Integer AD_CMPNY) {

         Debug.print("ApPaymentEntryControllerBean getApOpenCbAll");

         LocalApCheckBatchHome apCheckBatchHome = null;

         ArrayList list = new ArrayList();

         // Initialize EJB Home

         try {

         	apCheckBatchHome = (LocalApCheckBatchHome)EJBHomeFactory.
                 lookUpLocalHome(LocalApCheckBatchHome.JNDI_NAME, LocalApCheckBatchHome.class);

         } catch (NamingException ex) {

             throw new EJBException(ex.getMessage());

         }

         try {

         	Collection apCheckBatches = apCheckBatchHome.findOpenCbByCbType("PAYMENT", AD_BRNCH, AD_CMPNY);

         	Iterator i = apCheckBatches.iterator();

         	while (i.hasNext()) {

         		LocalApCheckBatch apCheckBatch = (LocalApCheckBatch)i.next();

         		list.add(apCheckBatch.getCbName());

         	}

         	return list;

         } catch (Exception ex) {

         	Debug.printStackTrace(ex);
         	throw new EJBException(ex.getMessage());

         }

     }

     /**
      * @ejb:interface-method view-type="remote"
      * @jboss:method-attributes read-only="true"
      **/
      public byte getAdPrfApUseSupplierPulldown(Integer AD_CMPNY) {

         Debug.print("ApPaymentEntryControllerBean getAdPrfApUseSupplierPulldown");

         LocalAdPreferenceHome adPreferenceHome = null;


         // Initialize EJB Home

         try {

            adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);

         } catch (NamingException ex) {

            throw new EJBException(ex.getMessage());

         }


         try {

            LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);

            return adPreference.getPrfApUseSupplierPulldown();

         } catch (Exception ex) {

            Debug.printStackTrace(ex);
            throw new EJBException(ex.getMessage());

         }

      }


      /**
  	 * @ejb:interface-method view-type="remote"
  	 * @jboss:method-attributes read-only="true"
  	 **/
  	public ArrayList getAdLvPurchaseRequisitionMisc1(Integer AD_CMPNY) {

  		Debug.print("ApPaymentEntryControllerBean getAdLvPurchaseRequisitionMisc1");

  		LocalAdLookUpValueHome adLookUpValueHome = null;

  		ArrayList list = new ArrayList();

  		// Initialize EJB Home

  		try {

  			adLookUpValueHome = (LocalAdLookUpValueHome)EJBHomeFactory.
  				lookUpLocalHome(LocalAdLookUpValueHome.JNDI_NAME, LocalAdLookUpValueHome.class);

  		} catch (NamingException ex) {

  			throw new EJBException(ex.getMessage());

  		}

  		try {

  			Collection adLookUpValues = adLookUpValueHome.findByLuName("AP PURCHASE REQUISITION MISC1", AD_CMPNY);

  			Iterator i = adLookUpValues.iterator();

  			while (i.hasNext()) {

  				LocalAdLookUpValue adLookUpValue = (LocalAdLookUpValue)i.next();

  				list.add(adLookUpValue.getLvName());

  			}
  			System.out.println("MISC1="+list.size());

  			return list;

  		} catch (Exception ex) {

  			Debug.printStackTrace(ex);
  			throw new EJBException(ex.getMessage());

  		}

  	}

  	/**
  	 * @ejb:interface-method view-type="remote"
  	 * @jboss:method-attributes read-only="true"
  	 **/
  	public ArrayList getAdLvPurchaseRequisitionMisc2(Integer AD_CMPNY) {

  		Debug.print("ApPaymentEntryControllerBean getAdLvPurchaseRequisitionMisc2");

  		LocalAdLookUpValueHome adLookUpValueHome = null;

  		ArrayList list = new ArrayList();

  		// Initialize EJB Home

  		try {

  			adLookUpValueHome = (LocalAdLookUpValueHome)EJBHomeFactory.
  				lookUpLocalHome(LocalAdLookUpValueHome.JNDI_NAME, LocalAdLookUpValueHome.class);

  		} catch (NamingException ex) {

  			throw new EJBException(ex.getMessage());

  		}

  		try {

  			Collection adLookUpValues = adLookUpValueHome.findByLuName("AP PURCHASE REQUISITION MISC2", AD_CMPNY);

  			Iterator i = adLookUpValues.iterator();

  			while (i.hasNext()) {

  				LocalAdLookUpValue adLookUpValue = (LocalAdLookUpValue)i.next();

  				list.add(adLookUpValue.getLvName());

  			}
  			System.out.println("PR T7="+list.size());

  			return list;

  		} catch (Exception ex) {

  			Debug.printStackTrace(ex);
  			throw new EJBException(ex.getMessage());

  		}

  	}


  	/**
  	 * @ejb:interface-method view-type="remote"
  	 * @jboss:method-attributes read-only="true"
  	 **/
  	public ArrayList getAdLvPurchaseRequisitionMisc3(Integer AD_CMPNY) {

  		Debug.print("ApPaymentEntryControllerBean getAdLvPurchaseRequisitionMisc3");

  		LocalAdLookUpValueHome adLookUpValueHome = null;

  		ArrayList list = new ArrayList();

  		// Initialize EJB Home

  		try {

  			adLookUpValueHome = (LocalAdLookUpValueHome)EJBHomeFactory.
  				lookUpLocalHome(LocalAdLookUpValueHome.JNDI_NAME, LocalAdLookUpValueHome.class);

  		} catch (NamingException ex) {

  			throw new EJBException(ex.getMessage());

  		}

  		try {

  			Collection adLookUpValues = adLookUpValueHome.findByLuName("AP PURCHASE REQUISITION MISC3", AD_CMPNY);

  			Iterator i = adLookUpValues.iterator();

  			while (i.hasNext()) {

  				LocalAdLookUpValue adLookUpValue = (LocalAdLookUpValue)i.next();

  				list.add(adLookUpValue.getLvName());

  			}
  			System.out.println("PR T9="+list.size());

  			return list;

  		} catch (Exception ex) {

  			Debug.printStackTrace(ex);
  			throw new EJBException(ex.getMessage());

  		}

  	}

  	/**
  	 * @ejb:interface-method view-type="remote"
  	 * @jboss:method-attributes read-only="true"
  	 **/
  	public ArrayList getAdLvPurchaseRequisitionMisc4(Integer AD_CMPNY) {

  		Debug.print("ApPaymentEntryControllerBean getAdLvPurchaseRequisitionMisc4");

  		LocalAdLookUpValueHome adLookUpValueHome = null;

  		ArrayList list = new ArrayList();

  		// Initialize EJB Home

  		try {

  			adLookUpValueHome = (LocalAdLookUpValueHome)EJBHomeFactory.
  				lookUpLocalHome(LocalAdLookUpValueHome.JNDI_NAME, LocalAdLookUpValueHome.class);

  		} catch (NamingException ex) {

  			throw new EJBException(ex.getMessage());

  		}

  		try {

  			Collection adLookUpValues = adLookUpValueHome.findByLuName("AP PURCHASE REQUISITION MISC4", AD_CMPNY);

  			Iterator i = adLookUpValues.iterator();

  			while (i.hasNext()) {

  				LocalAdLookUpValue adLookUpValue = (LocalAdLookUpValue)i.next();

  				list.add(adLookUpValue.getLvName());

  			}
  			System.out.println("PR T9="+list.size());

  			return list;

  		} catch (Exception ex) {

  			Debug.printStackTrace(ex);
  			throw new EJBException(ex.getMessage());

  		}

  	}

  	/**
  	 * @ejb:interface-method view-type="remote"
  	 * @jboss:method-attributes read-only="true"
  	 **/
  	public ArrayList getAdLvPurchaseRequisitionMisc5(Integer AD_CMPNY) {

  		Debug.print("ApPaymentEntryControllerBean getAdLvPurchaseRequisitionMisc5");

  		LocalAdLookUpValueHome adLookUpValueHome = null;

  		ArrayList list = new ArrayList();

  		// Initialize EJB Home

  		try {

  			adLookUpValueHome = (LocalAdLookUpValueHome)EJBHomeFactory.
  				lookUpLocalHome(LocalAdLookUpValueHome.JNDI_NAME, LocalAdLookUpValueHome.class);

  		} catch (NamingException ex) {

  			throw new EJBException(ex.getMessage());

  		}

  		try {

  			Collection adLookUpValues = adLookUpValueHome.findByLuName("AP PURCHASE REQUISITION MISC5", AD_CMPNY);

  			Iterator i = adLookUpValues.iterator();

  			while (i.hasNext()) {

  				LocalAdLookUpValue adLookUpValue = (LocalAdLookUpValue)i.next();

  				list.add(adLookUpValue.getLvName());

  			}
  			System.out.println("PR T9="+list.size());

  			return list;

  		} catch (Exception ex) {

  			Debug.printStackTrace(ex);
  			throw new EJBException(ex.getMessage());

  		}

  	}

  	/**
  	 * @ejb:interface-method view-type="remote"
  	 * @jboss:method-attributes read-only="true"
  	 **/
  	public ArrayList getAdLvPurchaseRequisitionMisc6(Integer AD_CMPNY) {

  		Debug.print("ApPaymentEntryControllerBean getAdLvPurchaseRequisitionMisc6");

  		LocalAdLookUpValueHome adLookUpValueHome = null;

  		ArrayList list = new ArrayList();

  		// Initialize EJB Home

  		try {

  			adLookUpValueHome = (LocalAdLookUpValueHome)EJBHomeFactory.
  				lookUpLocalHome(LocalAdLookUpValueHome.JNDI_NAME, LocalAdLookUpValueHome.class);

  		} catch (NamingException ex) {

  			throw new EJBException(ex.getMessage());

  		}

  		try {

  			Collection adLookUpValues = adLookUpValueHome.findByLuName("AP PURCHASE REQUISITION MISC6", AD_CMPNY);

  			Iterator i = adLookUpValues.iterator();

  			while (i.hasNext()) {

  				LocalAdLookUpValue adLookUpValue = (LocalAdLookUpValue)i.next();

  				list.add(adLookUpValue.getLvName());

  			}
  			System.out.println("PR T9="+list.size());

  			return list;

  		} catch (Exception ex) {

  			Debug.printStackTrace(ex);
  			throw new EJBException(ex.getMessage());

  		}

  	}

      /**
       * @ejb:interface-method view-type="remote"
       * @jboss:method-attributes read-only="true"
       **/
       public String getAdPrfApDefaultCheckDate(Integer AD_CMPNY) {

          Debug.print("ApPaymentEntryControllerBean getAdPrfApDefaultCheckDate");

          LocalAdPreferenceHome adPreferenceHome = null;


          // Initialize EJB Home

          try {

             adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
                 lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);

          } catch (NamingException ex) {

             throw new EJBException(ex.getMessage());

          }


          try {

             LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);

             return adPreference.getPrfApDefaultCheckDate();

          } catch (Exception ex) {

             Debug.printStackTrace(ex);
             throw new EJBException(ex.getMessage());

          }

       }

    	/**
    	 * @ejb:interface-method view-type="remote"
    	 * @jboss:method-attributes read-only="true"
    	 **/
    	public double getFrRateByFrNameAndFrDate(String FC_NM, Date CONVERSION_DATE, Integer AD_CMPNY)
    	throws GlobalConversionDateNotExistException {

    		Debug.print("GlJournalEntryControllerBean getFrRateByFrNameAndFrDate");

    		LocalGlFunctionalCurrencyRateHome glFunctionalCurrencyRateHome = null;
    		LocalGlFunctionalCurrencyHome glFunctionalCurrencyHome = null;
    		LocalAdCompanyHome adCompanyHome = null;

    		// Initialize EJB Home

    		try {

    			glFunctionalCurrencyRateHome = (LocalGlFunctionalCurrencyRateHome)EJBHomeFactory.
				lookUpLocalHome(LocalGlFunctionalCurrencyRateHome.JNDI_NAME, LocalGlFunctionalCurrencyRateHome.class);
    			glFunctionalCurrencyHome = (LocalGlFunctionalCurrencyHome)EJBHomeFactory.
				lookUpLocalHome(LocalGlFunctionalCurrencyHome.JNDI_NAME, LocalGlFunctionalCurrencyHome.class);
    			adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);

    		} catch (NamingException ex) {

    			throw new EJBException(ex.getMessage());

    		}

    		try {

    			LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
    			LocalGlFunctionalCurrency glFunctionalCurrency = glFunctionalCurrencyHome.findByFcName(FC_NM, AD_CMPNY);

    			double CONVERSION_RATE = 1;

    			// Get functional currency rate

    			if (!FC_NM.equals("USD")) {

    				LocalGlFunctionalCurrencyRate glFunctionalCurrencyRate =
    					glFunctionalCurrencyRateHome.findByFcCodeAndDate(glFunctionalCurrency.getFcCode(),
    							CONVERSION_DATE, AD_CMPNY);

    				CONVERSION_RATE = glFunctionalCurrencyRate.getFrXToUsd();

    			}

    			// Get set of book functional currency rate if necessary

    			if (!adCompany.getGlFunctionalCurrency().getFcName().equals("USD")) {

    				LocalGlFunctionalCurrencyRate glCompanyFunctionalCurrencyRate =
    					glFunctionalCurrencyRateHome.findByFcCodeAndDate(adCompany.getGlFunctionalCurrency(). getFcCode(),
    							CONVERSION_DATE, AD_CMPNY);

    				CONVERSION_RATE = CONVERSION_RATE / glCompanyFunctionalCurrencyRate.getFrXToUsd();

    			}

    			return CONVERSION_RATE;

    		} catch (FinderException ex) {

    			getSessionContext().setRollbackOnly();
    			throw new GlobalConversionDateNotExistException();

    		} catch (Exception ex) {

    			Debug.printStackTrace(ex);
    			throw new EJBException(ex.getMessage());

    		}

    	}

    // private methods

    private LocalApAppliedVoucher addApAvEntry(ApModAppliedVoucherDetails mdetails, LocalApCheck apCheck, Integer AD_CMPNY)
        throws ApVOUOverapplicationNotAllowedException,
        GlobalTransactionAlreadyLockedException,
		ApCHKVoucherHasNoWTaxCodeException {

		Debug.print("ApPaymentEntryControllerBean addApDrEntry");

		LocalApAppliedVoucherHome apAppliedVoucherHome = null;
		LocalApVoucherPaymentScheduleHome apVoucherPaymentScheduleHome = null;
		LocalAdCompanyHome adCompanyHome = null;


        // Initialize EJB Home

        try {

            apAppliedVoucherHome = (LocalApAppliedVoucherHome)EJBHomeFactory.
                lookUpLocalHome(LocalApAppliedVoucherHome.JNDI_NAME, LocalApAppliedVoucherHome.class);
            apVoucherPaymentScheduleHome = (LocalApVoucherPaymentScheduleHome)EJBHomeFactory.
                lookUpLocalHome(LocalApVoucherPaymentScheduleHome.JNDI_NAME, LocalApVoucherPaymentScheduleHome.class);
            adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);

        } catch (NamingException ex) {

            throw new EJBException(ex.getMessage());

        }

        try {

        	// get company

        	LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);

        	// get functional currency name

        	String FC_NM = adCompany.getGlFunctionalCurrency().getFcName();


        	// validate overapplication
        	System.out.println("vpscode : " + mdetails.getAvVpsCode());
        	LocalApVoucherPaymentSchedule apVoucherPaymentSchedule =  apVoucherPaymentScheduleHome.findByPrimaryKey(mdetails.getAvVpsCode());

        	if (EJBCommon.roundIt(apVoucherPaymentSchedule.getVpsAmountDue() - apVoucherPaymentSchedule.getVpsAmountPaid(), this.getGlFcPrecisionUnit(AD_CMPNY)) <
        	    EJBCommon.roundIt(mdetails.getAvApplyAmount() + mdetails.getAvTaxWithheld() + mdetails.getAvDiscountAmount(), this.getGlFcPrecisionUnit(AD_CMPNY))) {

        	    throw new ApVOUOverapplicationNotAllowedException(apVoucherPaymentSchedule.getApVoucher().getVouDocumentNumber() + "-" + apVoucherPaymentSchedule.getVpsNumber());

        	}

        	// validate if vps already locked

        	if (apVoucherPaymentSchedule.getVpsLock() == EJBCommon.TRUE) {

        		throw new GlobalTransactionAlreadyLockedException(apVoucherPaymentSchedule.getApVoucher().getVouDocumentNumber() + "-" + apVoucherPaymentSchedule.getVpsNumber());

        	}

        	// validate voucher wtax code if necessary

        	if (mdetails.getAvTaxWithheld() > 0 &&
        	    apVoucherPaymentSchedule.getApVoucher().getApWithholdingTaxCode().getGlChartOfAccount() == null) {

        		throw new ApCHKVoucherHasNoWTaxCodeException(apVoucherPaymentSchedule.getApVoucher().getVouDocumentNumber() + "-" + apVoucherPaymentSchedule.getVpsNumber());

        	}

        	double AV_FRX_GN_LSS = 0d;

        	System.out.println("VOUCHER CONVERSION RATE="+apVoucherPaymentSchedule.getApVoucher().getVouConversionRate());
        	System.out.println("PAYMENT CONVERSION RATE="+apCheck.getChkConversionRate());

        	if ( apVoucherPaymentSchedule.getApVoucher().getVouConversionRate() != apCheck.getChkConversionRate()) {

        		double VOUCHER_AMOUNT = EJBCommon.roundIt(apVoucherPaymentSchedule.getVpsAmountDue() / apVoucherPaymentSchedule.getApVoucher().getVouConversionRate(), this.getGlFcPrecisionUnit(AD_CMPNY));
        		double EXPECTED_CHK_AMOUNT = EJBCommon.roundIt(mdetails.getAvApplyAmount() / apVoucherPaymentSchedule.getApVoucher().getVouConversionRate(), this.getGlFcPrecisionUnit(AD_CMPNY));
        		double ACTUAL_CHK_AMOUNT = EJBCommon.roundIt(mdetails.getAvApplyAmount() / apCheck.getChkConversionRate(), this.getGlFcPrecisionUnit(AD_CMPNY));

        		System.out.println("VOUCHER_AMOUNT="+VOUCHER_AMOUNT);
        		System.out.println("EXPECTED_CHK_AMOUNT="+EXPECTED_CHK_AMOUNT);
        		System.out.println("ACTUAL_CHK_AMOUNT="+ACTUAL_CHK_AMOUNT);

        		AV_FRX_GN_LSS = EXPECTED_CHK_AMOUNT - ACTUAL_CHK_AMOUNT;
        	}

		    // create applied voucher

		    LocalApAppliedVoucher apAppliedVoucher = apAppliedVoucherHome.create(mdetails.getAvApplyAmount(),
		        0d, 0d, 0d, AV_FRX_GN_LSS, AD_CMPNY);

		    apAppliedVoucher.setApCheck(apCheck);
		    apAppliedVoucher.setApVoucherPaymentSchedule(apVoucherPaymentSchedule);

		    // lock voucher

		    apVoucherPaymentSchedule.setVpsLock(EJBCommon.TRUE);

		    return apAppliedVoucher;

		} catch (ApVOUOverapplicationNotAllowedException ex) {

        	getSessionContext().setRollbackOnly();
        	throw ex;

        } catch (GlobalTransactionAlreadyLockedException ex) {

        	getSessionContext().setRollbackOnly();
        	throw ex;

        } catch (ApCHKVoucherHasNoWTaxCodeException ex) {

        	getSessionContext().setRollbackOnly();
        	throw ex;

        } catch (Exception ex) {

        	Debug.printStackTrace(ex);
        	getSessionContext().setRollbackOnly();
        	throw new EJBException(ex.getMessage());

        }

	}

	private void addApDrEntry(short DR_LN, String DR_CLSS,
	    byte DR_DBT, double DR_AMNT, byte DR_RVRSD, Integer COA_CODE, LocalApCheck apCheck,
	    LocalApAppliedVoucher apAppliedVoucher, Integer AD_BRNCH, Integer AD_CMPNY) throws
	    GlobalBranchAccountNumberInvalidException {

		Debug.print("ApPaymentEntryControllerBean addApDrEntry");

		LocalApDistributionRecordHome apDistributionRecordHome = null;
		LocalGlChartOfAccountHome glChartOfAccountHome = null;
		LocalAdCompanyHome adCompanyHome = null;


        // Initialize EJB Home

        try {

            apDistributionRecordHome = (LocalApDistributionRecordHome)EJBHomeFactory.
                lookUpLocalHome(LocalApDistributionRecordHome.JNDI_NAME, LocalApDistributionRecordHome.class);
            glChartOfAccountHome = (LocalGlChartOfAccountHome)EJBHomeFactory.
                lookUpLocalHome(LocalGlChartOfAccountHome.JNDI_NAME, LocalGlChartOfAccountHome.class);
            adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);


        } catch (NamingException ex) {

            throw new EJBException(ex.getMessage());

        }

        try {

        	// get company

        	LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);

        	LocalGlChartOfAccount glChartOfAccount = null;

    		try {

    		    glChartOfAccount = glChartOfAccountHome.findByCoaCodeAndBranchCode(COA_CODE, AD_BRNCH, AD_CMPNY);

    		    if (glChartOfAccount.getCoaEnable() == EJBCommon.FALSE)
        		    throw new GlobalBranchAccountNumberInvalidException();

    		} catch (FinderException ex) {

        		throw new GlobalBranchAccountNumberInvalidException();

        	}

		    // create distribution record

		    LocalApDistributionRecord apDistributionRecord = apDistributionRecordHome.create(
			    DR_LN, DR_CLSS, EJBCommon.roundIt(DR_AMNT, adCompany.getGlFunctionalCurrency().getFcPrecision()),
			    DR_DBT, EJBCommon.FALSE, DR_RVRSD, AD_CMPNY);

			//apCheck.addApDistributionRecord(apDistributionRecord);
			apDistributionRecord.setApCheck(apCheck);
			//glChartOfAccount.addApDistributionRecord(apDistributionRecord);
			apDistributionRecord.setGlChartOfAccount(glChartOfAccount);

			// to be used by gl journal interface for cross currency receipts
			if (apAppliedVoucher != null) {

				//apAppliedVoucher.addApDistributionRecord(apDistributionRecord);
				apDistributionRecord.setApAppliedVoucher(apAppliedVoucher);

			}

        } catch(GlobalBranchAccountNumberInvalidException ex) {

            throw ex;

        } catch (Exception ex) {

        	Debug.printStackTrace(ex);
        	getSessionContext().setRollbackOnly();
        	throw new EJBException(ex.getMessage());

        }

	}


	/**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
     public short getInvGpCostPrecisionUnit(Integer AD_CMPNY) {

        Debug.print("ApReceivingItemEntryControllerBean getInvGpCostPrecisionUnit");

        LocalAdPreferenceHome adPreferenceHome = null;

         // Initialize EJB Home

         try {

         	adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);

         } catch (NamingException ex) {

            throw new EJBException(ex.getMessage());

         }


         try {

            LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);

            return adPreference.getPrfInvCostPrecisionUnit();

         } catch (Exception ex) {

            Debug.printStackTrace(ex);
            throw new EJBException(ex.getMessage());

         }

     }

	private double getInvFifoCost(Date CST_DT, Integer IL_CODE, double CST_QTY, double CST_COST,
			boolean isAdjustFifo, Integer AD_BRNCH, Integer AD_CMPNY)
	{

		LocalInvCostingHome invCostingHome = null;
		LocalInvItemLocationHome invItemLocationHome = null;

		// Initialize EJB Home

		try {

			invCostingHome = (LocalInvCostingHome)EJBHomeFactory.
			lookUpLocalHome(LocalInvCostingHome.JNDI_NAME, LocalInvCostingHome.class);
			invItemLocationHome = (LocalInvItemLocationHome)EJBHomeFactory.
			lookUpLocalHome(LocalInvItemLocationHome.JNDI_NAME, LocalInvItemLocationHome.class);
		}
		catch (NamingException ex) {

			throw new EJBException(ex.getMessage());
		}

		try {

			Collection invFifoCostings = invCostingHome.findFifoRemainingQuantityByLessThanOrEqualCstDateAndIlCodeAndBrCode(CST_DT, IL_CODE, AD_BRNCH, AD_CMPNY);

			if (invFifoCostings.size() > 0) {

				Iterator x = invFifoCostings.iterator();

				if (isAdjustFifo) {

					//executed during POST transaction

					double totalCost = 0d;
					double cost;

					if(CST_QTY < 0) {

						//for negative quantities
						double neededQty = -(CST_QTY);

						while(x.hasNext() && neededQty != 0) {

							LocalInvCosting invFifoCosting = (LocalInvCosting)x.next();

							if (invFifoCosting.getApPurchaseOrderLine() != null || invFifoCosting.getApVoucherLineItem() != null) {
								cost = invFifoCosting.getCstItemCost() / invFifoCosting.getCstQuantityReceived();
							} else if(invFifoCosting.getArInvoiceLineItem() != null) {
								cost = invFifoCosting.getCstCostOfSales() / invFifoCosting.getCstQuantitySold();
							} else if (invFifoCosting.getInvBuildUnbuildAssemblyLine() != null) {
								cost = invFifoCosting.getCstAssemblyCost() / invFifoCosting.getCstAssemblyQuantity();
							} else {
								cost = invFifoCosting.getCstAdjustCost() /invFifoCosting.getCstAdjustQuantity();
							}

							if(neededQty <= invFifoCosting.getCstRemainingLifoQuantity()) {

								invFifoCosting.setCstRemainingLifoQuantity(invFifoCosting.getCstRemainingLifoQuantity() - neededQty);
								totalCost += (neededQty * cost);
								neededQty = 0d;
							} else {

								neededQty -= invFifoCosting.getCstRemainingLifoQuantity();
								totalCost += (invFifoCosting.getCstRemainingLifoQuantity() * cost);
								invFifoCosting.setCstRemainingLifoQuantity(0);
							}
						}

						//if needed qty is not yet satisfied but no more quantities to fetch, get the default cost
						if(neededQty != 0) {

							LocalInvItemLocation invItemLocation = invItemLocationHome.findByPrimaryKey(IL_CODE);
							totalCost += (neededQty * invItemLocation.getInvItem().getIiUnitCost());
						}

						cost = totalCost / -CST_QTY;
					}

					else {

						//for positive quantities
						cost = CST_COST;
					}
					return cost;
				}

				else {

					//executed during ENTRY transaction

					LocalInvCosting invFifoCosting = (LocalInvCosting)x.next();

					if (invFifoCosting.getApPurchaseOrderLine() != null || invFifoCosting.getApVoucherLineItem() != null) {
						return EJBCommon.roundIt(invFifoCosting.getCstItemCost() / invFifoCosting.getCstQuantityReceived(), this.getInvGpCostPrecisionUnit(AD_CMPNY));
					} else if(invFifoCosting.getArInvoiceLineItem() != null) {
						return  EJBCommon.roundIt(invFifoCosting.getCstCostOfSales() / invFifoCosting.getCstQuantitySold(), this.getInvGpCostPrecisionUnit(AD_CMPNY));
					} else if (invFifoCosting.getInvBuildUnbuildAssemblyLine() != null) {
						return EJBCommon.roundIt(invFifoCosting.getCstAssemblyCost() / invFifoCosting.getCstAssemblyQuantity(), this.getInvGpCostPrecisionUnit(AD_CMPNY));
					} else {
						return EJBCommon.roundIt(invFifoCosting.getCstAdjustCost() /invFifoCosting.getCstAdjustQuantity(), this.getInvGpCostPrecisionUnit(AD_CMPNY));
					}
				}
			}
			else {

				//most applicable in 1st entries of data
				LocalInvItemLocation invItemLocation = invItemLocationHome.findByPrimaryKey(IL_CODE);
				return invItemLocation.getInvItem().getIiUnitCost();
			}

		}
		catch (Exception ex) {
			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());
		}
	}

    private double convertForeignToFunctionalCurrency(Integer FC_CODE, String FC_NM,
	    Date CONVERSION_DATE, double CONVERSION_RATE, double AMOUNT, Integer AD_CMPNY) {

	    Debug.print("ApPaymentEntryControllerBean convertForeignToFunctionalCurrency");


        LocalGlFunctionalCurrencyRateHome glFunctionalCurrencyRateHome = null;
        LocalAdCompanyHome adCompanyHome = null;

        LocalAdCompany adCompany = null;

        // Initialize EJB Homes

        try {

            glFunctionalCurrencyRateHome = (LocalGlFunctionalCurrencyRateHome)EJBHomeFactory.
               lookUpLocalHome(LocalGlFunctionalCurrencyRateHome.JNDI_NAME, LocalGlFunctionalCurrencyRateHome.class);
            adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
               lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);

         } catch (NamingException ex) {

            throw new EJBException(ex.getMessage());

         }

         // get company and extended precision

         try {

             adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);

         } catch (Exception ex) {

             throw new EJBException(ex.getMessage());

         }


         // Convert to functional currency if necessary

         if (CONVERSION_RATE != 1 && CONVERSION_RATE != 0 ) {

             AMOUNT = AMOUNT / CONVERSION_RATE;

         }

         return EJBCommon.roundIt(AMOUNT, adCompany.getGlFunctionalCurrency().getFcPrecision());

	}

    private void executeApChkPost(Integer CHK_CODE, String USR_NM, Integer AD_BRNCH, Integer AD_CMPNY) throws
        GlobalRecordAlreadyDeletedException,
        GlobalTransactionAlreadyPostedException,
		GlobalTransactionAlreadyVoidPostedException,
		GlJREffectiveDateNoPeriodExistException,
		GlJREffectiveDatePeriodClosedException,
		GlobalJournalNotBalanceException {

        Debug.print("ApPaymentEntryControllerBean executeApChkPost");

        LocalApCheckHome apCheckHome = null;
        LocalAdCompanyHome adCompanyHome = null;
        LocalAdPreferenceHome adPreferenceHome = null;
        LocalGlSetOfBookHome glSetOfBookHome = null;
        LocalGlAccountingCalendarValueHome glAccountingCalendarValueHome = null;
        LocalGlJournalHome glJournalHome = null;
        LocalGlJournalBatchHome glJournalBatchHome = null;
        LocalGlSuspenseAccountHome glSuspenseAccountHome = null;
        LocalGlJournalLineHome glJournalLineHome = null;
        LocalGlJournalSourceHome glJournalSourceHome = null;
        LocalGlJournalCategoryHome glJournalCategoryHome = null;
        LocalGlFunctionalCurrencyHome glFunctionalCurrencyHome = null;
        LocalApDistributionRecordHome apDistributionRecordHome = null;
        LocalInvCostingHome invCostingHome = null;
        LocalGlChartOfAccountHome glChartOfAccountHome = null;
        LocalAdBankAccountHome adBankAccountHome = null;
        LocalAdBankAccountBalanceHome adBankAccountBalanceHome = null;
 		LocalGlForexLedgerHome glForexLedgerHome = null;
 		LocalGlFunctionalCurrencyRateHome glFunctionalCurrencyRateHome = null;

        LocalApCheck apCheck = null;

        // Initialize EJB Home

        try {

            apCheckHome = (LocalApCheckHome)EJBHomeFactory.
                lookUpLocalHome(LocalApCheckHome.JNDI_NAME, LocalApCheckHome.class);
            adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
            adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
            	lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);
            glSetOfBookHome = (LocalGlSetOfBookHome)EJBHomeFactory.
            	lookUpLocalHome(LocalGlSetOfBookHome.JNDI_NAME, LocalGlSetOfBookHome.class);
            glAccountingCalendarValueHome = (LocalGlAccountingCalendarValueHome)EJBHomeFactory.
            	lookUpLocalHome(LocalGlAccountingCalendarValueHome.JNDI_NAME, LocalGlAccountingCalendarValueHome.class);
            glJournalHome = (LocalGlJournalHome)EJBHomeFactory.
            	lookUpLocalHome(LocalGlJournalHome.JNDI_NAME, LocalGlJournalHome.class);
            glJournalBatchHome = (LocalGlJournalBatchHome)EJBHomeFactory.
            	lookUpLocalHome(LocalGlJournalBatchHome.JNDI_NAME, LocalGlJournalBatchHome.class);
            glSuspenseAccountHome = (LocalGlSuspenseAccountHome)EJBHomeFactory.
            	lookUpLocalHome(LocalGlSuspenseAccountHome.JNDI_NAME, LocalGlSuspenseAccountHome.class);
            glJournalLineHome = (LocalGlJournalLineHome)EJBHomeFactory.
            	lookUpLocalHome(LocalGlJournalLineHome.JNDI_NAME, LocalGlJournalLineHome.class);
            glJournalSourceHome = (LocalGlJournalSourceHome)EJBHomeFactory.
            	lookUpLocalHome(LocalGlJournalSourceHome.JNDI_NAME, LocalGlJournalSourceHome.class);
            glJournalCategoryHome = (LocalGlJournalCategoryHome)EJBHomeFactory.
            	lookUpLocalHome(LocalGlJournalCategoryHome.JNDI_NAME, LocalGlJournalCategoryHome.class);
            glFunctionalCurrencyHome = (LocalGlFunctionalCurrencyHome)EJBHomeFactory.
            	lookUpLocalHome(LocalGlFunctionalCurrencyHome.JNDI_NAME, LocalGlFunctionalCurrencyHome.class);
            apDistributionRecordHome = (LocalApDistributionRecordHome)EJBHomeFactory.
            	lookUpLocalHome(LocalApDistributionRecordHome.JNDI_NAME, LocalApDistributionRecordHome.class);
            invCostingHome = (LocalInvCostingHome)EJBHomeFactory.
            	lookUpLocalHome(LocalInvCostingHome.JNDI_NAME, LocalInvCostingHome.class);
            glChartOfAccountHome = (LocalGlChartOfAccountHome)EJBHomeFactory.
            	lookUpLocalHome(LocalGlChartOfAccountHome.JNDI_NAME, LocalGlChartOfAccountHome.class);
            adBankAccountHome = (LocalAdBankAccountHome)EJBHomeFactory.
        		lookUpLocalHome(LocalAdBankAccountHome.JNDI_NAME, LocalAdBankAccountHome.class);
            adBankAccountBalanceHome = (LocalAdBankAccountBalanceHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdBankAccountBalanceHome.JNDI_NAME, LocalAdBankAccountBalanceHome.class);
 			glForexLedgerHome  = (LocalGlForexLedgerHome)EJBHomeFactory.
				lookUpLocalHome(LocalGlForexLedgerHome.JNDI_NAME, LocalGlForexLedgerHome.class);
 			glFunctionalCurrencyRateHome = (LocalGlFunctionalCurrencyRateHome)EJBHomeFactory.
				lookUpLocalHome(LocalGlFunctionalCurrencyRateHome.JNDI_NAME, LocalGlFunctionalCurrencyRateHome.class);

        } catch (NamingException ex) {

            throw new EJBException(ex.getMessage());

        }

        try {

        	// validate if check is already deleted

        	try {

        		apCheck = apCheckHome.findByPrimaryKey(CHK_CODE);

        	} catch (FinderException ex) {

        		throw new GlobalRecordAlreadyDeletedException();

        	}

        	// validate if check is already posted

        	if (apCheck.getChkVoid() == EJBCommon.FALSE && apCheck.getChkPosted() == EJBCommon.TRUE) {

        		throw new GlobalTransactionAlreadyPostedException();

        		// validate if check void is already posted

        	} else if (apCheck.getChkVoid() == EJBCommon.TRUE && apCheck.getChkVoidPosted() == EJBCommon.TRUE) {

        		throw new GlobalTransactionAlreadyVoidPostedException();

        	}


        	// post check

        	if (apCheck.getChkVoid() == EJBCommon.FALSE && apCheck.getChkPosted() == EJBCommon.FALSE) {


        		double CHK_AMNT = this.convertForeignToFunctionalCurrency(apCheck.getGlFunctionalCurrency().getFcCode(),
    					apCheck.getGlFunctionalCurrency().getFcName(),
						apCheck.getChkConversionDate(), apCheck.getChkConversionRate(),
						apCheck.getChkAmount(), AD_CMPNY);

        		double AV_FRX_GN_LSS = 0d;

        		if (apCheck.getChkType().equals("PAYMENT")) {

        			// increase amount paid in voucher payment schedules and voucher

        			Collection apAppliedVouchers = apCheck.getApAppliedVouchers();

        			Iterator i = apAppliedVouchers.iterator();

        			while (i.hasNext()) {

        				LocalApAppliedVoucher apAppliedVoucher = (LocalApAppliedVoucher)i.next();

        				AV_FRX_GN_LSS += apAppliedVoucher.getAvForexGainLoss();
        				LocalApVoucherPaymentSchedule apVoucherPaymentSchedule = apAppliedVoucher.getApVoucherPaymentSchedule();

        				double AMOUNT_PAID = apAppliedVoucher.getAvApplyAmount() + apAppliedVoucher.getAvDiscountAmount() + apAppliedVoucher.getAvTaxWithheld();


        				apVoucherPaymentSchedule.setVpsAmountPaid(EJBCommon.roundIt(apVoucherPaymentSchedule.getVpsAmountPaid() + AMOUNT_PAID, this.getGlFcPrecisionUnit(AD_CMPNY)));

        				apVoucherPaymentSchedule.getApVoucher().setVouAmountPaid(EJBCommon.roundIt(apVoucherPaymentSchedule.getApVoucher().getVouAmountPaid() + AMOUNT_PAID, this.getGlFcPrecisionUnit(AD_CMPNY)));

        				// release voucher lock

        				apVoucherPaymentSchedule.setVpsLock(EJBCommon.FALSE);

        			}

        			// decrease supplier balance

        			//this.post(apCheck.getChkDate(), (CHK_AMNT + CHK_CRDTS) * -1, apCheck.getApSupplier(), AD_CMPNY);

        		}

        		// decrease bank balance

				LocalAdBankAccount adBankAccount = adBankAccountHome.findByPrimaryKey(apCheck.getAdBankAccount().getBaCode());

				try {

					// find bankaccount balance before or equal check date

					Collection adBankAccountBalances = adBankAccountBalanceHome.findByBeforeOrEqualDateAndBaCodeAndType(apCheck.getChkCheckDate(), apCheck.getAdBankAccount().getBaCode(), "BOOK", AD_CMPNY);

					if (!adBankAccountBalances.isEmpty()) {

						// get last check

						ArrayList adBankAccountBalanceList = new ArrayList(adBankAccountBalances);

						LocalAdBankAccountBalance adBankAccountBalance = (LocalAdBankAccountBalance)adBankAccountBalanceList.get(adBankAccountBalanceList.size() - 1);

						if (adBankAccountBalance.getBabDate().before(apCheck.getChkCheckDate())) {

							// create new balance

							LocalAdBankAccountBalance adNewBankAccountBalance = adBankAccountBalanceHome.create(
									apCheck.getChkCheckDate(), adBankAccountBalance.getBabBalance() - CHK_AMNT - AV_FRX_GN_LSS, "BOOK", AD_CMPNY);

							//adBankAccount.addAdBankAccountBalance(adNewBankAccountBalance);
							adNewBankAccountBalance.setAdBankAccount(adBankAccount);

						} else { // equals to check date

							adBankAccountBalance.setBabBalance(adBankAccountBalance.getBabBalance() - CHK_AMNT - AV_FRX_GN_LSS);

						}

					} else {

						// create new balance

						LocalAdBankAccountBalance adNewBankAccountBalance = adBankAccountBalanceHome.create(
								apCheck.getChkCheckDate(), (0 - CHK_AMNT - AV_FRX_GN_LSS), "BOOK", AD_CMPNY);

						adNewBankAccountBalance.setAdBankAccount(adBankAccount);

					}

					// propagate to subsequent balances if necessary

					adBankAccountBalances = adBankAccountBalanceHome.findByAfterDateAndBaCodeAndType(apCheck.getChkCheckDate(), apCheck.getAdBankAccount().getBaCode(), "BOOK", AD_CMPNY);

					Iterator i = adBankAccountBalances.iterator();

					while (i.hasNext()) {

						LocalAdBankAccountBalance adBankAccountBalance = (LocalAdBankAccountBalance)i.next();

						adBankAccountBalance.setBabBalance(adBankAccountBalance.getBabBalance() - CHK_AMNT - AV_FRX_GN_LSS);

					}

				} catch (Exception ex) {

					ex.printStackTrace();

				}

        		// set check post status

        		apCheck.setChkPosted(EJBCommon.TRUE);
        		apCheck.setChkPostedBy(USR_NM);
        		apCheck.setChkDatePosted(EJBCommon.getGcCurrentDateWoTime().getTime());

        		// set Relese Check Upon Posting if Bank Account is CASH

        		if(apCheck.getAdBankAccount().getBaIsCashAccount() == EJBCommon.TRUE){

        			apCheck.setChkReleased(EJBCommon.TRUE);
        			apCheck.setChkDateReleased(apCheck.getChkCheckDate());

        		}


        	} else if (apCheck.getChkVoid() == EJBCommon.TRUE && apCheck.getChkVoidPosted() == EJBCommon.FALSE) {

        		// VOIDING CHECK

        		double CHK_AMNT = this.convertForeignToFunctionalCurrency(apCheck.getGlFunctionalCurrency().getFcCode(),
    					apCheck.getGlFunctionalCurrency().getFcName(),
						apCheck.getChkConversionDate(), apCheck.getChkConversionRate(),
						apCheck.getChkAmount(), AD_CMPNY);
        		double AV_FRX_GN_LSS = 0d;
        		if (apCheck.getChkType().equals("PAYMENT")) {

        			// decrease amount paid in voucher payment schedules and voucher

        			Collection apAppliedVouchers = apCheck.getApAppliedVouchers();

        			Iterator i = apAppliedVouchers.iterator();

        			while (i.hasNext()) {

        				LocalApAppliedVoucher apAppliedVoucher = (LocalApAppliedVoucher)i.next();

        				LocalApVoucherPaymentSchedule apVoucherPaymentSchedule = apAppliedVoucher.getApVoucherPaymentSchedule();
        				AV_FRX_GN_LSS += apAppliedVoucher.getAvForexGainLoss();
        				double AMOUNT_PAID = apAppliedVoucher.getAvApplyAmount() + apAppliedVoucher.getAvDiscountAmount() + apAppliedVoucher.getAvTaxWithheld();

        				apVoucherPaymentSchedule.setVpsAmountPaid(EJBCommon.roundIt(apVoucherPaymentSchedule.getVpsAmountPaid() - AMOUNT_PAID, this.getGlFcPrecisionUnit(AD_CMPNY)));
        				apVoucherPaymentSchedule.getApVoucher().setVouAmountPaid(EJBCommon.roundIt(apVoucherPaymentSchedule.getApVoucher().getVouAmountPaid() - AMOUNT_PAID, this.getGlFcPrecisionUnit(AD_CMPNY)));

        			}

        			// increase supplier balance

        			//this.post(apCheck.getChkDate(), CHK_AMNT + CHK_CRDTS, apCheck.getApSupplier(), AD_CMPNY);

        		}

        		// increase bank balance

				LocalAdBankAccount adBankAccount = adBankAccountHome.findByPrimaryKey(apCheck.getAdBankAccount().getBaCode());

				try {

					// find bankaccount balance before or equal check date

					Collection adBankAccountBalances = adBankAccountBalanceHome.findByBeforeOrEqualDateAndBaCodeAndType(apCheck.getChkCheckDate(), apCheck.getAdBankAccount().getBaCode(), "BOOK", AD_CMPNY);

					if (!adBankAccountBalances.isEmpty()) {

						// get last check

						ArrayList adBankAccountBalanceList = new ArrayList(adBankAccountBalances);

						LocalAdBankAccountBalance adBankAccountBalance = (LocalAdBankAccountBalance)adBankAccountBalanceList.get(adBankAccountBalanceList.size() - 1);

						if (adBankAccountBalance.getBabDate().before(apCheck.getChkCheckDate())) {

							// create new balance

							LocalAdBankAccountBalance adNewBankAccountBalance = adBankAccountBalanceHome.create(
									apCheck.getChkCheckDate(), adBankAccountBalance.getBabBalance() + CHK_AMNT - AV_FRX_GN_LSS, "BOOK", AD_CMPNY);

							adNewBankAccountBalance.setAdBankAccount(adBankAccount);

						} else { // equals to check date

							adBankAccountBalance.setBabBalance(adBankAccountBalance.getBabBalance() + CHK_AMNT - AV_FRX_GN_LSS);

						}

					} else {

						// create new balance

						LocalAdBankAccountBalance adNewBankAccountBalance = adBankAccountBalanceHome.create(
								apCheck.getChkCheckDate(), CHK_AMNT - AV_FRX_GN_LSS, "BOOK", AD_CMPNY);

						adNewBankAccountBalance.setAdBankAccount(adBankAccount);

					}

					// propagate to subsequent balances if necessary

					adBankAccountBalances = adBankAccountBalanceHome.findByAfterDateAndBaCodeAndType(apCheck.getChkCheckDate(), apCheck.getAdBankAccount().getBaCode(), "BOOK", AD_CMPNY);

					Iterator i = adBankAccountBalances.iterator();

					while (i.hasNext()) {

						LocalAdBankAccountBalance adBankAccountBalance = (LocalAdBankAccountBalance)i.next();

						adBankAccountBalance.setBabBalance(adBankAccountBalance.getBabBalance() + CHK_AMNT - AV_FRX_GN_LSS);

					}

				} catch (Exception ex) {

					ex.printStackTrace();

				}

        		// set check post status

        		apCheck.setChkVoidPosted(EJBCommon.TRUE);
        		apCheck.setChkPostedBy(USR_NM);
        		apCheck.setChkDatePosted(EJBCommon.getGcCurrentDateWoTime().getTime());

        	}

        	// post to gl if necessary

        	LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);
        	LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);

        	if (adPreference.getPrfApGlPostingType().equals("AUTO-POST UPON APPROVAL")) {

        		// validate if date has no period and period is closed

        		LocalGlSetOfBook glJournalSetOfBook = null;

        		try {

        			glJournalSetOfBook = glSetOfBookHome.findByDate(apCheck.getChkDate(), AD_CMPNY);

        		} catch (FinderException ex) {

        			throw new GlJREffectiveDateNoPeriodExistException();

        		}

        		LocalGlAccountingCalendarValue glAccountingCalendarValue =
        			glAccountingCalendarValueHome.findByAcCodeAndDate(
        					glJournalSetOfBook.getGlAccountingCalendar().getAcCode(), apCheck.getChkDate(), AD_CMPNY);


        		if (glAccountingCalendarValue.getAcvStatus() == 'N' ||
        				glAccountingCalendarValue.getAcvStatus() == 'C' ||
						glAccountingCalendarValue.getAcvStatus() == 'P') {

        			throw new GlJREffectiveDatePeriodClosedException();

        		}

        		// check if check is balance if not check suspense posting

        		LocalGlJournalLine glOffsetJournalLine = null;

        		Collection apDistributionRecords = null;

        		if (apCheck.getChkVoid() == EJBCommon.FALSE) {

        			apDistributionRecords = apDistributionRecordHome.findImportableDrByDrReversedAndChkCode(EJBCommon.FALSE, apCheck.getChkCode(), AD_CMPNY);

        		} else {

        			apDistributionRecords = apDistributionRecordHome.findImportableDrByDrReversedAndChkCode(EJBCommon.TRUE, apCheck.getChkCode(), AD_CMPNY);

        		}

        		Iterator j = apDistributionRecords.iterator();
        		boolean firstFlag = true;

        		double TOTAL_DEBIT = 0d;
        		double TOTAL_CREDIT = 0d;

        		while (j.hasNext()) {

        			LocalApDistributionRecord apDistributionRecord = (LocalApDistributionRecord)j.next();

        			double DR_AMNT = apDistributionRecord.getDrAmount();

        			if (apDistributionRecord.getDrDebit() == EJBCommon.TRUE) {

        				TOTAL_DEBIT += DR_AMNT;

        			} else {

        				TOTAL_CREDIT += DR_AMNT;

        			}

        		}

        		TOTAL_DEBIT = EJBCommon.roundIt(TOTAL_DEBIT, adCompany.getGlFunctionalCurrency().getFcPrecision());
        		TOTAL_CREDIT = EJBCommon.roundIt(TOTAL_CREDIT, adCompany.getGlFunctionalCurrency().getFcPrecision());

        		if (adPreference.getPrfAllowSuspensePosting() == EJBCommon.TRUE &&
        				TOTAL_DEBIT != TOTAL_CREDIT) {

        			LocalGlSuspenseAccount glSuspenseAccount = null;

        			try {

        				glSuspenseAccount = glSuspenseAccountHome.findByJsNameAndJcName("ACCOUNTS PAYABLES", "CHECKS", AD_CMPNY);

        			} catch (FinderException ex) {

        				throw new GlobalJournalNotBalanceException();

        			}

        			if (TOTAL_DEBIT - TOTAL_CREDIT < 0) {

        				glOffsetJournalLine = glJournalLineHome.create(
        						(short)(apDistributionRecords.size() + 1),
								EJBCommon.TRUE,
								TOTAL_CREDIT - TOTAL_DEBIT, "", AD_CMPNY);

        			} else {

        				glOffsetJournalLine = glJournalLineHome.create(
        						(short)(apDistributionRecords.size() + 1),
								EJBCommon.FALSE,
								TOTAL_DEBIT - TOTAL_CREDIT, "", AD_CMPNY);

        			}

        			LocalGlChartOfAccount glChartOfAccount = glSuspenseAccount.getGlChartOfAccount();
        			glOffsetJournalLine.setGlChartOfAccount(glChartOfAccount);


        		} else if (adPreference.getPrfAllowSuspensePosting() == EJBCommon.FALSE &&
        				TOTAL_DEBIT != TOTAL_CREDIT) {

        			throw new GlobalJournalNotBalanceException();

        		}

        		// create journal batch if necessary

        		LocalGlJournalBatch glJournalBatch = null;
        		java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat("MM/dd/yyyy");

        		try {

        			if (adPreference.getPrfEnableApCheckBatch() == EJBCommon.TRUE) {

        				glJournalBatch = glJournalBatchHome.findByJbName("JOURNAL IMPORT " + formatter.format(new Date()) + " " + apCheck.getApCheckBatch().getCbName(), AD_BRNCH, AD_CMPNY);

        			} else {

        				glJournalBatch = glJournalBatchHome.findByJbName("JOURNAL IMPORT " + formatter.format(new Date()) + " CHECKS", AD_BRNCH, AD_CMPNY);

        			}

        		} catch (FinderException ex) {
        		}

        		if (adPreference.getPrfEnableGlJournalBatch() ==  EJBCommon.TRUE &&
        				glJournalBatch == null) {

        			if (adPreference.getPrfEnableApCheckBatch() == EJBCommon.TRUE) {

        				glJournalBatch = glJournalBatchHome.create("JOURNAL IMPORT " + formatter.format(new Date()) + " " + apCheck.getApCheckBatch().getCbName(), "JOURNAL IMPORT", "CLOSED", EJBCommon.getGcCurrentDateWoTime().getTime(), USR_NM, AD_BRNCH, AD_CMPNY);

        			} else {

        				glJournalBatch = glJournalBatchHome.create("JOURNAL IMPORT " + formatter.format(new Date()) + " CHECKS", "JOURNAL IMPORT", "CLOSED", EJBCommon.getGcCurrentDateWoTime().getTime(), USR_NM, AD_BRNCH, AD_CMPNY);

        			}


        		}

        		// create journal entry
        		String supplierName = apCheck.getChkSupplierName() == null ? apCheck.getApSupplier().getSplName() :
        			apCheck.getChkSupplierName();

        		LocalGlJournal glJournal = glJournalHome.create(apCheck.getChkReferenceNumber(),
        				apCheck.getChkDescription(), apCheck.getChkDate(),
						0.0d, null, apCheck.getChkDocumentNumber(), null, 1d, "N/A", null,
						'N', EJBCommon.TRUE, EJBCommon.FALSE,
						USR_NM, new Date(),
						USR_NM, new Date(),
						null, null,
						USR_NM, EJBCommon.getGcCurrentDateWoTime().getTime(),
						apCheck.getApSupplier().getSplTin(),
						supplierName, EJBCommon.FALSE,
						null,
						AD_BRNCH, AD_CMPNY);

        		LocalGlJournalSource glJournalSource = glJournalSourceHome.findByJsName("ACCOUNTS PAYABLES", AD_CMPNY);
        		glJournal.setGlJournalSource(glJournalSource);

        		LocalGlFunctionalCurrency glFunctionalCurrency = glFunctionalCurrencyHome.findByFcName(adCompany.getGlFunctionalCurrency().getFcName(), AD_CMPNY);
        		glJournal.setGlFunctionalCurrency(glFunctionalCurrency);

        		LocalGlJournalCategory glJournalCategory = glJournalCategoryHome.findByJcName("CHECKS", AD_CMPNY);
        		glJournal.setGlJournalCategory(glJournalCategory);

        		if (glJournalBatch != null) {

        			glJournal.setGlJournalBatch(glJournalBatch);

        		}


        		// create journal lines

        		j = apDistributionRecords.iterator();

        		while (j.hasNext()) {

        			LocalApDistributionRecord apDistributionRecord = (LocalApDistributionRecord)j.next();

        			double DR_AMNT = apDistributionRecord.getDrAmount();


        			LocalGlJournalLine glJournalLine = glJournalLineHome.create(
        					apDistributionRecord.getDrLine(),
							apDistributionRecord.getDrDebit(),
							DR_AMNT, "", AD_CMPNY);

        			glJournalLine.setGlChartOfAccount(apDistributionRecord.getGlChartOfAccount());
        			glJournalLine.setGlJournal(glJournal);

        			apDistributionRecord.setDrImported(EJBCommon.TRUE);

    		       	/*// for FOREX revaluation

            	    int FC_CODE = apDistributionRecord.getApAppliedVoucher() != null ?
            	    	apVoucher.getGlFunctionalCurrency().getFcCode().intValue() :
            	    	apCheck.getGlFunctionalCurrency().getFcCode().intValue();

    		       	if((FC_CODE != adCompany.getGlFunctionalCurrency().getFcCode().intValue()) &&
    					glJournalLine.getGlChartOfAccount().getGlFunctionalCurrency() != null && (FC_CODE ==
    						glJournalLine.getGlChartOfAccount().getGlFunctionalCurrency().getFcCode().intValue())){

    		       		double CONVERSION_RATE = apDistributionRecord.getApAppliedVoucher() != null ?
    		            	    apVoucher.getVouConversionRate() : apCheck.getChkConversionRate();

    		            Date DATE = apDistributionRecord.getApAppliedVoucher() != null ?
    		            	    apVoucher.getVouConversionDate() : apCheck.getChkConversionDate();

    		            if (DATE != null && (CONVERSION_RATE == 0 || CONVERSION_RATE == 1)){

    		  	  			CONVERSION_RATE = this.getFrRateByFrNameAndFrDate(
    			  	  				glJournalLine.getGlChartOfAccount().getGlFunctionalCurrency().getFcName(),
    								glJournal.getJrConversionDate(), AD_CMPNY);

    		            } else if (CONVERSION_RATE == 0) {

    		            	CONVERSION_RATE = 1;

    		       		}

    		       		Collection glForexLedgers = null;

    		       		DATE = apDistributionRecord.getApAppliedVoucher() != null ?
    		            	    apVoucher.getVouDate() : apCheck.getChkDate();

    		       		try {

    		       			glForexLedgers = glForexLedgerHome.findLatestGlFrlByFrlDateAndByCoaCode(
    		       					DATE, glJournalLine.getGlChartOfAccount().getCoaCode(),
    								AD_CMPNY);

    		       		} catch(FinderException ex) {

    		       		}

    		       		LocalGlForexLedger glForexLedger =
    		       			(glForexLedgers.isEmpty() || glForexLedgers == null) ? null :
    		       				(LocalGlForexLedger) glForexLedgers.iterator().next();

    		       		int FRL_LN = (glForexLedger != null &&
    		       			glForexLedger.getFrlDate().compareTo(DATE) == 0) ?
    		       				glForexLedger.getFrlLine().intValue() + 1 : 1;

    		       		// compute balance
    		       		double COA_FRX_BLNC = glForexLedger == null ? 0 : glForexLedger.getFrlBalance();
    		       		double FRL_AMNT = apDistributionRecord.getDrAmount();

    		       		if(glJournalLine.getGlChartOfAccount().getCoaAccountType().equalsIgnoreCase("ASSET"))
    		       			FRL_AMNT = (glJournalLine.getJlDebit() == EJBCommon.TRUE ? FRL_AMNT : (- 1 * FRL_AMNT));
    		       		else
    		       			FRL_AMNT = (glJournalLine.getJlDebit() == EJBCommon.TRUE ? (- 1 * FRL_AMNT) : FRL_AMNT);

    		       		COA_FRX_BLNC = COA_FRX_BLNC + FRL_AMNT;

    		       		double FRX_GN_LSS = 0d;

    		       		if (glOffsetJournalLine != null && firstFlag) {

    		       			if(glOffsetJournalLine.getGlChartOfAccount().getCoaAccountType().equalsIgnoreCase("ASSET"))

    		       				FRX_GN_LSS = (glOffsetJournalLine.getJlDebit() == EJBCommon.TRUE ?
    		       						glOffsetJournalLine.getJlAmount() : (- 1 * glOffsetJournalLine.getJlAmount()));

    		       			else

    		       				FRX_GN_LSS = (glOffsetJournalLine.getJlDebit() == EJBCommon.TRUE ?
    		       						(- 1 * glOffsetJournalLine.getJlAmount()) : glOffsetJournalLine.getJlAmount());

    		       			firstFlag = false;

    		       		}

    		       		glForexLedger = glForexLedgerHome.create(DATE, new Integer (FRL_LN), "CHK", FRL_AMNT,
    		       				CONVERSION_RATE, COA_FRX_BLNC, FRX_GN_LSS, AD_CMPNY);

    		       		//glJournalLine.getGlChartOfAccount().addGlForexLedger(glForexLedger);
    		       		glForexLedger.setGlChartOfAccount(glJournalLine.getGlChartOfAccount());

    		       		// propagate balances
    		       		try{

    		       			glForexLedgers = glForexLedgerHome.findByGreaterThanFrlDateAndCoaCode(
    		       					glForexLedger.getFrlDate(), glForexLedger.getGlChartOfAccount().getCoaCode(),
    								glForexLedger.getFrlAdCompany());

    		       		} catch (FinderException ex) {

    		       		}

    		       		Iterator itrFrl = glForexLedgers.iterator();

    		       		while (itrFrl.hasNext()) {

    		       			glForexLedger = (LocalGlForexLedger) itrFrl.next();
    		       			FRL_AMNT = apDistributionRecord.getDrAmount();

    		       			if(glJournalLine.getGlChartOfAccount().getCoaAccountType().equalsIgnoreCase("ASSET"))
    		       				FRL_AMNT = (glJournalLine.getJlDebit() == EJBCommon.TRUE ? FRL_AMNT :
    		       					(- 1 * FRL_AMNT));
    		       			else
    		       				FRL_AMNT = (glJournalLine.getJlDebit() == EJBCommon.TRUE ? (- 1 * FRL_AMNT) :
    		       					FRL_AMNT);

    		       			glForexLedger.setFrlBalance(glForexLedger.getFrlBalance() + FRL_AMNT);

    		       		}

    		       	}*/

        		}

        		/*if (glOffsetJournalLine != null) {

        			glOffsetJournalLine.setGlJournal(glJournal);

        		}
        		*/
        		// post journal to gl

        		Collection glJournalLines = glJournal.getGlJournalLines();

        		Iterator i = glJournalLines.iterator();

        		while (i.hasNext()) {

        			LocalGlJournalLine glJournalLine = (LocalGlJournalLine)i.next();

        			// post current to current acv

        			this.postToGl(glAccountingCalendarValue,
        					glJournalLine.getGlChartOfAccount(),
							true, glJournalLine.getJlDebit(), glJournalLine.getJlAmount(), AD_CMPNY);


        			// post to subsequent acvs (propagate)

        			Collection glSubsequentAccountingCalendarValues =
        				glAccountingCalendarValueHome.findSubsequentAcvByAcCodeAndAcvPeriodNumber(
        						glJournalSetOfBook.getGlAccountingCalendar().getAcCode(),
								glAccountingCalendarValue.getAcvPeriodNumber(), AD_CMPNY);

        			Iterator acvsIter = glSubsequentAccountingCalendarValues.iterator();

        			while (acvsIter.hasNext()) {

        				LocalGlAccountingCalendarValue glSubsequentAccountingCalendarValue =
        					(LocalGlAccountingCalendarValue)acvsIter.next();

        				this.postToGl(glSubsequentAccountingCalendarValue,
        						glJournalLine.getGlChartOfAccount(),
								false, glJournalLine.getJlDebit(), glJournalLine.getJlAmount(), AD_CMPNY);

        			}

        			// post to subsequent years if necessary

        			Collection glSubsequentSetOfBooks = glSetOfBookHome.findSubsequentSobByAcYear(glJournalSetOfBook.getGlAccountingCalendar().getAcYear(), AD_CMPNY);

        			if (!glSubsequentSetOfBooks.isEmpty() && glJournalSetOfBook.getSobYearEndClosed() == 1) {

        				adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
        				LocalGlChartOfAccount glRetainedEarningsAccount = glChartOfAccountHome.findByCoaAccountNumber(adCompany.getCmpRetainedEarnings(), AD_CMPNY);

        				Iterator sobIter = glSubsequentSetOfBooks.iterator();

        				while (sobIter.hasNext()) {

        					LocalGlSetOfBook glSubsequentSetOfBook = (LocalGlSetOfBook)sobIter.next();

        					String ACCOUNT_TYPE = glJournalLine.getGlChartOfAccount().getCoaAccountType();

        					// post to subsequent acvs of subsequent set of book(propagate)

        					Collection glAccountingCalendarValues =
        						glAccountingCalendarValueHome.findByAcCode(glSubsequentSetOfBook.getGlAccountingCalendar().getAcCode(), AD_CMPNY);

        					Iterator acvIter = glAccountingCalendarValues.iterator();

        					while (acvIter.hasNext()) {

        						LocalGlAccountingCalendarValue glSubsequentAccountingCalendarValue =
        							(LocalGlAccountingCalendarValue)acvIter.next();

        						if (ACCOUNT_TYPE.equals("ASSET") || ACCOUNT_TYPE.equals("LIABILITY") ||
        								ACCOUNT_TYPE.equals("OWNERS EQUITY")) {

        							this.postToGl(glSubsequentAccountingCalendarValue,
        									glJournalLine.getGlChartOfAccount(),
											false, glJournalLine.getJlDebit(), glJournalLine.getJlAmount(), AD_CMPNY);

        						} else { // revenue & expense

        							this.postToGl(glSubsequentAccountingCalendarValue,
        									glRetainedEarningsAccount,
											false, glJournalLine.getJlDebit(), glJournalLine.getJlAmount(), AD_CMPNY);

        						}

        					}

        					if (glSubsequentSetOfBook.getSobYearEndClosed() == 0) break;

        				}

        			}

        		}

        	}

        } catch (GlJREffectiveDateNoPeriodExistException ex) {

        	getSessionContext().setRollbackOnly();
        	throw ex;

        } catch (GlJREffectiveDatePeriodClosedException ex) {

        	getSessionContext().setRollbackOnly();
        	throw ex;

        } catch (GlobalJournalNotBalanceException ex) {

        	getSessionContext().setRollbackOnly();
        	throw ex;

        } catch (GlobalRecordAlreadyDeletedException ex) {

        	getSessionContext().setRollbackOnly();
        	throw ex;

        } catch (GlobalTransactionAlreadyPostedException ex) {

        	getSessionContext().setRollbackOnly();
        	throw ex;

        } catch (GlobalTransactionAlreadyVoidPostedException ex) {

        	getSessionContext().setRollbackOnly();
        	throw ex;

    	} catch (Exception ex) {

    		Debug.printStackTrace(ex);
    		getSessionContext().setRollbackOnly();
    		throw new EJBException(ex.getMessage());

    	}

    }

    private void post(Date CHK_DT, double CHK_AMNT, LocalApSupplier apSupplier, Integer AD_CMPNY) {

       Debug.print("ApPaymentEntryControllerBean post");

       LocalApSupplierBalanceHome apSupplierBalanceHome = null;

       // Initialize EJB Home

       try {

           apSupplierBalanceHome = (LocalApSupplierBalanceHome)EJBHomeFactory.
               lookUpLocalHome(LocalApSupplierBalanceHome.JNDI_NAME, LocalApSupplierBalanceHome.class);

       } catch (NamingException ex) {

           getSessionContext().setRollbackOnly();
           throw new EJBException(ex.getMessage());

       }

       try {

	       // find supplier balance before or equal voucher date

	       Collection apSupplierBalances = apSupplierBalanceHome.findByBeforeOrEqualVouDateAndSplSupplierCode(CHK_DT, apSupplier.getSplSupplierCode(), AD_CMPNY);

	       if (!apSupplierBalances.isEmpty()) {

	    	   // get last voucher

	    	   ArrayList apSupplierBalanceList = new ArrayList(apSupplierBalances);

	    	   LocalApSupplierBalance apSupplierBalance = (LocalApSupplierBalance)apSupplierBalanceList.get(apSupplierBalanceList.size() - 1);

	    	   if (apSupplierBalance.getSbDate().before(CHK_DT)) {

	    	       // create new balance

	    	       LocalApSupplierBalance apNewSupplierBalance = apSupplierBalanceHome.create(
		    	       CHK_DT, apSupplierBalance.getSbBalance() + CHK_AMNT, AD_CMPNY);

		           //apSupplier.addApSupplierBalance(apNewSupplierBalance);
		           apNewSupplierBalance.setApSupplier(apSupplier);

	    	   } else { // equals to voucher date

	    	       apSupplierBalance.setSbBalance(apSupplierBalance.getSbBalance() + CHK_AMNT);

	    	   }

	    	} else {

	    	    // create new balance

		    	LocalApSupplierBalance apNewSupplierBalance = apSupplierBalanceHome.create(
		    		CHK_DT, CHK_AMNT, AD_CMPNY);

		        //apSupplier.addApSupplierBalance(apNewSupplierBalance);
		        apNewSupplierBalance.setApSupplier(apSupplier);

	     	}

	     	// propagate to subsequent balances if necessary

	     	apSupplierBalances = apSupplierBalanceHome.findByAfterVouDateAndSplSupplierCode(CHK_DT, apSupplier.getSplSupplierCode(), AD_CMPNY);

	     	Iterator i = apSupplierBalances.iterator();

	     	while (i.hasNext()) {

	     		LocalApSupplierBalance apSupplierBalance = (LocalApSupplierBalance)i.next();

	     		apSupplierBalance.setSbBalance(apSupplierBalance.getSbBalance() + CHK_AMNT);

	     	}

        } catch (Exception ex) {

        	Debug.printStackTrace(ex);
        	getSessionContext().setRollbackOnly();
            throw new EJBException(ex.getMessage());

        }

	}

   private void postToGl(LocalGlAccountingCalendarValue glAccountingCalendarValue,
      LocalGlChartOfAccount glChartOfAccount,
      boolean isCurrentAcv, byte isDebit, double JL_AMNT, Integer AD_CMPNY) {

      Debug.print("ApPaymentEntryControllerBean postToGl");

      LocalGlChartOfAccountBalanceHome glChartOfAccountBalanceHome = null;
      LocalAdCompanyHome adCompanyHome = null;


       // Initialize EJB Home

       try {

           glChartOfAccountBalanceHome = (LocalGlChartOfAccountBalanceHome)EJBHomeFactory.
              lookUpLocalHome(LocalGlChartOfAccountBalanceHome.JNDI_NAME, LocalGlChartOfAccountBalanceHome.class);
           adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
              lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);


       } catch (NamingException ex) {

           throw new EJBException(ex.getMessage());

       }

       try {

               LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);

	       	   LocalGlChartOfAccountBalance glChartOfAccountBalance =
		            glChartOfAccountBalanceHome.findByAcvCodeAndCoaCode(
		               	  glAccountingCalendarValue.getAcvCode(),
		               	  glChartOfAccount.getCoaCode(), AD_CMPNY);

	           String ACCOUNT_TYPE = glChartOfAccount.getCoaAccountType();
	           short FC_EXTNDD_PRCSN = adCompany.getGlFunctionalCurrency().getFcPrecision();



	           if (((ACCOUNT_TYPE.equals("ASSET") || ACCOUNT_TYPE.equals("EXPENSE")) &&
				    isDebit == EJBCommon.TRUE) ||
				    (!ACCOUNT_TYPE.equals("ASSET") && !ACCOUNT_TYPE.equals("EXPENSE") &&
				    isDebit == EJBCommon.FALSE)) {

 			        glChartOfAccountBalance.setCoabEndingBalance(
				       EJBCommon.roundIt(glChartOfAccountBalance.getCoabEndingBalance() + JL_AMNT, FC_EXTNDD_PRCSN));

				    if (!isCurrentAcv) {

				    	glChartOfAccountBalance.setCoabBeginningBalance(
				       		EJBCommon.roundIt(glChartOfAccountBalance.getCoabBeginningBalance() + JL_AMNT, FC_EXTNDD_PRCSN));

					}


			  } else {

				    glChartOfAccountBalance.setCoabEndingBalance(
				       EJBCommon.roundIt(glChartOfAccountBalance.getCoabEndingBalance() - JL_AMNT, FC_EXTNDD_PRCSN));

				    if (!isCurrentAcv) {

				    	glChartOfAccountBalance.setCoabBeginningBalance(
				       		EJBCommon.roundIt(glChartOfAccountBalance.getCoabBeginningBalance() - JL_AMNT, FC_EXTNDD_PRCSN));

					}

		 	  }

		 	  if (isCurrentAcv) {

			 	 if (isDebit == EJBCommon.TRUE) {

		 			glChartOfAccountBalance.setCoabTotalDebit(
		 				EJBCommon.roundIt(glChartOfAccountBalance.getCoabTotalDebit() + JL_AMNT, FC_EXTNDD_PRCSN));

		 		 } else {

		 			glChartOfAccountBalance.setCoabTotalCredit(
		 				EJBCommon.roundIt(glChartOfAccountBalance.getCoabTotalCredit() + JL_AMNT, FC_EXTNDD_PRCSN));
		 		 }

		 	}

       } catch (Exception ex) {

       	   Debug.printStackTrace(ex);
       	   throw new EJBException(ex.getMessage());

       }


   }

   private void postToInv(LocalApVoucherLineItem apVoucherLineItem, Date CST_DT, double CST_QTY_RCVD, double CST_ITM_CST, double CST_RMNNG_QTY, double CST_RMNNG_VL, Integer AD_BRNCH, Integer AD_CMPNY) {

    	Debug.print("ApCheckPostControllerBean postToInv");

    	LocalInvCostingHome invCostingHome = null;
    	LocalAdPreferenceHome adPreferenceHome = null;
    	LocalAdCompanyHome adCompanyHome = null;

        // Initialize EJB Home

        try {

          invCostingHome = (LocalInvCostingHome)EJBHomeFactory.
              lookUpLocalHome(LocalInvCostingHome.JNDI_NAME, LocalInvCostingHome.class);
          adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
              lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);
          adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
              lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);

        } catch (NamingException ex) {

          throw new EJBException(ex.getMessage());

        }

        try {

           LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);
           LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
           LocalInvItemLocation invItemLocation = apVoucherLineItem.getInvItemLocation();
           int CST_LN_NMBR = 0;

           CST_QTY_RCVD = EJBCommon.roundIt(CST_QTY_RCVD, adPreference.getPrfInvQuantityPrecisionUnit());
           CST_ITM_CST = EJBCommon.roundIt(CST_ITM_CST, adCompany.getGlFunctionalCurrency().getFcPrecision());
           CST_RMNNG_QTY = EJBCommon.roundIt(CST_RMNNG_QTY, adPreference.getPrfInvQuantityPrecisionUnit());
           CST_RMNNG_VL = EJBCommon.roundIt(CST_RMNNG_VL, adCompany.getGlFunctionalCurrency().getFcPrecision());

           // create costing

           try {

           	   // generate line number

               LocalInvCosting invCurrentCosting = invCostingHome.getByMaxCstLineNumberAndCstDateToLongAndIiNameAndLocName(CST_DT.getTime(), invItemLocation.getInvItem().getIiName(), invItemLocation.getInvLocation().getLocName(), AD_BRNCH, AD_CMPNY);
               CST_LN_NMBR = invCurrentCosting.getCstLineNumber() + 1;

           } catch (FinderException ex) {

           	   CST_LN_NMBR = 1;

           }

           LocalInvCosting invCosting = invCostingHome.create(CST_DT, CST_DT.getTime(), CST_LN_NMBR, CST_QTY_RCVD, CST_ITM_CST, 0d, 0d, 0d, 0d, 0d, 0d, CST_RMNNG_QTY, CST_RMNNG_VL, 0d, 0d, 0d, AD_BRNCH, AD_CMPNY);
           //invItemLocation.addInvCosting(invCosting);
           invCosting.setInvItemLocation(invItemLocation);
           invCosting.setApVoucherLineItem(apVoucherLineItem);

           // propagate balance if necessary

           Collection invCostings = invCostingHome.findByGreaterThanCstDateAndIiNameAndLocName(CST_DT, invItemLocation.getInvItem().getIiName(), invItemLocation.getInvLocation().getLocName(), AD_BRNCH, AD_CMPNY);

           Iterator i = invCostings.iterator();

           while (i.hasNext()) {

               LocalInvCosting invPropagatedCosting = (LocalInvCosting)i.next();

               invPropagatedCosting.setCstRemainingQuantity(invPropagatedCosting.getCstRemainingQuantity() + CST_QTY_RCVD);
               invPropagatedCosting.setCstRemainingValue(invPropagatedCosting.getCstRemainingValue() + CST_ITM_CST);

           }


        } catch (Exception ex) {

      	   Debug.printStackTrace(ex);
      	   getSessionContext().setRollbackOnly();
           throw new EJBException(ex.getMessage());

        }

    }

    private double convertByUomFromAndItemAndQuantity(LocalInvUnitOfMeasure invFromUnitOfMeasure, LocalInvItem invItem, double QTY_RCVD, Integer AD_CMPNY) {

		Debug.print("ApCheckPostControllerBean convertByUomFromAndItemAndQuantity");

		LocalAdPreferenceHome adPreferenceHome = null;
		LocalInvUnitOfMeasureConversionHome invUnitOfMeasureConversionHome = null;

        // Initialize EJB Home

        try {

            adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
                 lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);
            invUnitOfMeasureConversionHome = (LocalInvUnitOfMeasureConversionHome)EJBHomeFactory.
                  lookUpLocalHome(LocalInvUnitOfMeasureConversionHome.JNDI_NAME, LocalInvUnitOfMeasureConversionHome.class);

        } catch (NamingException ex) {

            throw new EJBException(ex.getMessage());

        }

        try {

            LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);


            LocalInvUnitOfMeasureConversion invUnitOfMeasureConversion = invUnitOfMeasureConversionHome.findUmcByIiNameAndUomName(invItem.getIiName(), invFromUnitOfMeasure.getUomName(), AD_CMPNY);
            LocalInvUnitOfMeasureConversion invDefaultUomConversion = invUnitOfMeasureConversionHome.findUmcByIiNameAndUomName(invItem.getIiName(), invItem.getInvUnitOfMeasure().getUomName(), AD_CMPNY);

            return EJBCommon.roundIt(QTY_RCVD * invDefaultUomConversion.getUmcConversionFactor() / invUnitOfMeasureConversion.getUmcConversionFactor(), adPreference.getPrfInvQuantityPrecisionUnit());

        } catch (Exception ex) {

        	Debug.printStackTrace(ex);
        	getSessionContext().setRollbackOnly();
        	throw new EJBException(ex.getMessage());

        }

	}

	// SessionBean methods

    /**
     * @ejb:create-method view-type="remote"
     **/
    public void ejbCreate() throws CreateException {

       Debug.print("ApPaymentEntryControllerBean ejbCreate");

    }

}