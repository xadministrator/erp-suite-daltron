package com.ejb.txn;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;
import javax.naming.NamingException;

import com.ejb.ad.LocalAdAmountLimit;
import com.ejb.ad.LocalAdAmountLimitHome;
import com.ejb.ad.LocalAdApproval;
import com.ejb.ad.LocalAdApprovalHome;
import com.ejb.ad.LocalAdApprovalQueue;
import com.ejb.ad.LocalAdApprovalQueueHome;
import com.ejb.ad.LocalAdApprovalUser;
import com.ejb.ad.LocalAdApprovalUserHome;
import com.ejb.ad.LocalAdBranchDocumentSequenceAssignment;
import com.ejb.ad.LocalAdBranchDocumentSequenceAssignmentHome;
import com.ejb.ad.LocalAdCompany;
import com.ejb.ad.LocalAdCompanyHome;
import com.ejb.ad.LocalAdDeleteAuditTrailHome;
import com.ejb.ad.LocalAdPaymentTerm;
import com.ejb.ad.LocalAdPaymentTermHome;
import com.ejb.ad.LocalAdPreference;
import com.ejb.ad.LocalAdPreferenceHome;
import com.ejb.ap.LocalApPurchaseOrder;
import com.ejb.ap.LocalApPurchaseOrderHome;
import com.ejb.ap.LocalApPurchaseOrderLine;
import com.ejb.ap.LocalApPurchaseOrderLineHome;
import com.ejb.ap.LocalApSupplier;
import com.ejb.ap.LocalApSupplierHome;
import com.ejb.ad.LocalAdUserHome;
import com.ejb.ad.LocalAdUser;
import com.ejb.ad.LocalAdLookUpValueHome;
import com.ejb.ad.LocalAdLookUpValue;
import com.ejb.ap.LocalApTaxCode;
import com.ejb.ap.LocalApTaxCodeHome;
import com.ejb.ap.LocalApPurchaseRequisitionHome;
import com.ejb.ap.LocalApPurchaseRequisition;
import com.ejb.ap.LocalApVoucherBatch;
import com.ejb.ap.LocalApVoucherBatchHome;
import com.ejb.ap.LocalApVoucherLineItem;
import com.ejb.exception.GlobalConversionDateNotExistException;
import com.ejb.exception.GlobalDocumentNumberNotUniqueException;
import com.ejb.exception.GlobalInvItemLocationNotFoundException;
import com.ejb.exception.GlobalNoApprovalApproverFoundException;
import com.ejb.exception.GlobalNoApprovalRequesterFoundException;
import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.exception.GlobalPaymentTermInvalidException;
import com.ejb.exception.GlobalRecordAlreadyDeletedException;
import com.ejb.exception.GlobalSupplierItemInvalidException;
import com.ejb.exception.GlobalTransactionAlreadyApprovedException;
import com.ejb.exception.GlobalTransactionAlreadyPendingException;
import com.ejb.exception.GlobalTransactionAlreadyPostedException;
import com.ejb.exception.GlobalTransactionAlreadyVoidException;
import com.ejb.exception.GlobalRecordInvalidException;
import com.ejb.genfld.LocalGenSegmentHome;
import com.ejb.genfld.LocalGenValueSetValueHome;
import com.ejb.gl.LocalAdDocumentSequenceAssignment;
import com.ejb.gl.LocalAdDocumentSequenceAssignmentHome;
import com.ejb.gl.LocalGlAccountingCalendarValue;
import com.ejb.gl.LocalGlChartOfAccount;
import com.ejb.gl.LocalGlChartOfAccountBalance;
import com.ejb.gl.LocalGlChartOfAccountBalanceHome;
import com.ejb.gl.LocalGlFunctionalCurrency;
import com.ejb.gl.LocalGlFunctionalCurrencyHome;
import com.ejb.gl.LocalGlFunctionalCurrencyRate;
import com.ejb.gl.LocalGlFunctionalCurrencyRateHome;
import com.ejb.inv.LocalInvItem;
import com.ejb.inv.LocalInvItemHome;
import com.ejb.inv.LocalInvItemLocation;
import com.ejb.inv.LocalInvItemLocationHome;
import com.ejb.inv.LocalInvLocation;
import com.ejb.inv.LocalInvLocationHome;
import com.ejb.inv.LocalInvTag;
import com.ejb.inv.LocalInvTagHome;
import com.ejb.inv.LocalInvTransactionalBudget;
import com.ejb.inv.LocalInvTransactionalBudgetHome;
import com.ejb.inv.LocalInvUnitOfMeasure;
import com.ejb.inv.LocalInvUnitOfMeasureConversion;
import com.ejb.inv.LocalInvUnitOfMeasureConversionHome;
import com.ejb.inv.LocalInvUnitOfMeasureHome;
import com.util.AbstractSessionBean;
import com.util.ApModPurchaseOrderDetails;
import com.util.ApModPurchaseOrderLineDetails;
import com.util.ApModSupplierDetails;
import com.util.ApTaxCodeDetails;
import com.util.Debug;
import com.util.EJBCommon;
import com.util.EJBHomeFactory;
import com.util.GlModFunctionalCurrencyDetails;
import com.util.InvModTagListDetails;
import com.util.InvModUnitOfMeasureDetails;

/**
 * @ejb:bean name="ApPurchaseOrderEntryControllerEJB"
 *           display-name="used for entering purchase orders"
 *           type="Stateless"
 *           view-type="remote"
 *           jndi-name="ejb/ApPurchaseOrderEntryControllerEJB"
 *
 * @ejb:interface remote-class="com.ejb.txn.ApPurchaseOrderEntryController"
 *                extends="javax.ejb.EJBObject"
 *
 * @ejb:home remote-class="com.ejb.txn.ApPurchaseOrderEntryControllerHome"
 *           extends="javax.ejb.EJBHome"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="apuser"
 *                        role-link="apuserlink"
 *
 * @ejb:permission role-name="apuser"
 *
 */

public class ApPurchaseOrderEntryControllerBean extends AbstractSessionBean {

	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public ArrayList getApSplAll(Integer AD_BRNCH, Integer AD_CMPNY) {

		Debug.print("ApPurchaseOrderEntryControllerBean getApSplAll");

		LocalApSupplierHome apSupplierHome = null;

		ArrayList list = new ArrayList();

		// Initialize EJB Home

		try {

			apSupplierHome = (LocalApSupplierHome)EJBHomeFactory.
			lookUpLocalHome(LocalApSupplierHome.JNDI_NAME, LocalApSupplierHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}

		try {

			Collection apSuppliers = apSupplierHome.findEnabledSplAll(AD_BRNCH, AD_CMPNY);

			Iterator i = apSuppliers.iterator();

			while (i.hasNext()) {

				LocalApSupplier apSupplier = (LocalApSupplier)i.next();

				list.add(apSupplier.getSplSupplierCode());

			}

			return list;

		} catch (Exception ex) {

			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());

		}

	}

	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public ArrayList getAdPytAll(Integer AD_CMPNY) {

		Debug.print("ApPurchaseOrderEntryControllerBean getAdPytAll");

		LocalAdPaymentTermHome adPaymentTermHome = null;

		ArrayList list = new ArrayList();

		// Initialize EJB Home

		try {

			adPaymentTermHome = (LocalAdPaymentTermHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdPaymentTermHome.JNDI_NAME, LocalAdPaymentTermHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}

		try {

			Collection adPaymentTerms = adPaymentTermHome.findEnabledPytAll(AD_CMPNY);

			Iterator i = adPaymentTerms.iterator();

			while (i.hasNext()) {

				LocalAdPaymentTerm adPaymentTerm = (LocalAdPaymentTerm)i.next();

				list.add(adPaymentTerm.getPytName());

			}

			return list;

		} catch (Exception ex) {

			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());

		}

	}

	/**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList getAdUsrAll(Integer AD_CMPNY) {

        Debug.print("ApReceivingItemEntryControllerBean getAdUsrAll");

        LocalAdUserHome adUserHome = null;

        LocalAdUser adUser = null;

        Collection adUsers = null;

        ArrayList list = new ArrayList();

        // Initialize EJB Home

        try {

            adUserHome = (LocalAdUserHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdUserHome.JNDI_NAME, LocalAdUserHome.class);

        } catch (NamingException ex) {

            throw new EJBException(ex.getMessage());

        }

        try {

            adUsers = adUserHome.findUsrAll(AD_CMPNY);

        } catch (FinderException ex) {

        } catch (Exception ex) {

            throw new EJBException(ex.getMessage());
        }

        if (adUsers.isEmpty()) {

            return null;

        }

        Iterator i = adUsers.iterator();

        while (i.hasNext()) {

            adUser = (LocalAdUser)i.next();

            list.add(adUser.getUsrName());

        }

        return list;

    }





    /**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public ArrayList getAdLvPurchaseRequisitionMisc1(Integer AD_CMPNY) {

		Debug.print("ApPurchaseOrderEntryControllerBean getAdLvPurchaseRequisitionMisc1");

		LocalAdLookUpValueHome adLookUpValueHome = null;

		ArrayList list = new ArrayList();

		// Initialize EJB Home

		try {

			adLookUpValueHome = (LocalAdLookUpValueHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdLookUpValueHome.JNDI_NAME, LocalAdLookUpValueHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}

		try {

			Collection adLookUpValues = adLookUpValueHome.findByLuName("AP PURCHASE REQUISITION MISC1", AD_CMPNY);

			Iterator i = adLookUpValues.iterator();

			while (i.hasNext()) {

				LocalAdLookUpValue adLookUpValue = (LocalAdLookUpValue)i.next();

				list.add(adLookUpValue.getLvName());

			}
			System.out.println("MISC1="+list.size());

			return list;

		} catch (Exception ex) {

			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());

		}

	}

	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public ArrayList getAdLvPurchaseRequisitionMisc2(Integer AD_CMPNY) {

		Debug.print("ApPurchaseOrderEntryControllerBean getAdLvPurchaseRequisitionMisc2");

		LocalAdLookUpValueHome adLookUpValueHome = null;

		ArrayList list = new ArrayList();

		// Initialize EJB Home

		try {

			adLookUpValueHome = (LocalAdLookUpValueHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdLookUpValueHome.JNDI_NAME, LocalAdLookUpValueHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}

		try {

			Collection adLookUpValues = adLookUpValueHome.findByLuName("AP PURCHASE REQUISITION MISC2", AD_CMPNY);

			Iterator i = adLookUpValues.iterator();

			while (i.hasNext()) {

				LocalAdLookUpValue adLookUpValue = (LocalAdLookUpValue)i.next();

				list.add(adLookUpValue.getLvName());

			}
			System.out.println("PR T7="+list.size());

			return list;

		} catch (Exception ex) {

			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());

		}

	}


	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public ArrayList getAdLvPurchaseRequisitionMisc3(Integer AD_CMPNY) {

		Debug.print("ApPurchaseOrderEntryControllerBean getAdLvPurchaseRequisitionMisc3");

		LocalAdLookUpValueHome adLookUpValueHome = null;

		ArrayList list = new ArrayList();

		// Initialize EJB Home

		try {

			adLookUpValueHome = (LocalAdLookUpValueHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdLookUpValueHome.JNDI_NAME, LocalAdLookUpValueHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}

		try {

			Collection adLookUpValues = adLookUpValueHome.findByLuName("AP PURCHASE REQUISITION MISC3", AD_CMPNY);

			Iterator i = adLookUpValues.iterator();

			while (i.hasNext()) {

				LocalAdLookUpValue adLookUpValue = (LocalAdLookUpValue)i.next();

				list.add(adLookUpValue.getLvName());

			}
			System.out.println("PR T9="+list.size());

			return list;

		} catch (Exception ex) {

			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());

		}

	}


	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public ArrayList getAdLvPurchaseRequisitionMisc4(Integer AD_CMPNY) {

		Debug.print("ApPurchaseOrderEntryControllerBean getAdLvPurchaseRequisitionMisc4");

		LocalAdLookUpValueHome adLookUpValueHome = null;

		ArrayList list = new ArrayList();

		// Initialize EJB Home

		try {

			adLookUpValueHome = (LocalAdLookUpValueHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdLookUpValueHome.JNDI_NAME, LocalAdLookUpValueHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}

		try {

			Collection adLookUpValues = adLookUpValueHome.findByLuName("AP PURCHASE REQUISITION MISC4", AD_CMPNY);

			Iterator i = adLookUpValues.iterator();

			while (i.hasNext()) {

				LocalAdLookUpValue adLookUpValue = (LocalAdLookUpValue)i.next();

				list.add(adLookUpValue.getLvName());

			}
			System.out.println("PR T9="+list.size());

			return list;

		} catch (Exception ex) {

			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());

		}

	}



	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public ArrayList getAdLvPurchaseRequisitionMisc5(Integer AD_CMPNY) {

		Debug.print("ApPurchaseOrderEntryControllerBean getAdLvPurchaseRequisitionMisc5");

		LocalAdLookUpValueHome adLookUpValueHome = null;

		ArrayList list = new ArrayList();

		// Initialize EJB Home

		try {

			adLookUpValueHome = (LocalAdLookUpValueHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdLookUpValueHome.JNDI_NAME, LocalAdLookUpValueHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}

		try {

			Collection adLookUpValues = adLookUpValueHome.findByLuName("AP PURCHASE REQUISITION MISC5", AD_CMPNY);

			Iterator i = adLookUpValues.iterator();

			while (i.hasNext()) {

				LocalAdLookUpValue adLookUpValue = (LocalAdLookUpValue)i.next();

				list.add(adLookUpValue.getLvName());

			}
			System.out.println("PR T9="+list.size());

			return list;

		} catch (Exception ex) {

			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());

		}

	}


	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public ArrayList getAdLvPurchaseRequisitionMisc6(Integer AD_CMPNY) {

		Debug.print("ApPurchaseOrderEntryControllerBean getAdLvPurchaseRequisitionMisc6");

		LocalAdLookUpValueHome adLookUpValueHome = null;

		ArrayList list = new ArrayList();

		// Initialize EJB Home

		try {

			adLookUpValueHome = (LocalAdLookUpValueHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdLookUpValueHome.JNDI_NAME, LocalAdLookUpValueHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}

		try {

			Collection adLookUpValues = adLookUpValueHome.findByLuName("AP PURCHASE REQUISITION MISC6", AD_CMPNY);

			Iterator i = adLookUpValues.iterator();

			while (i.hasNext()) {

				LocalAdLookUpValue adLookUpValue = (LocalAdLookUpValue)i.next();

				list.add(adLookUpValue.getLvName());

			}
			System.out.println("PR T9="+list.size());

			return list;

		} catch (Exception ex) {

			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());

		}

	}


	private void setInvTbForItemForCurrentMonth(String itemName, String userName, Date date, double qtyConsumed, Integer AD_CMPNY)
	throws GlobalNoRecordFoundException
	{

		Debug.print("ApPurchaseOrderEntryControllerBean getInvTrnsctnlBdgtForCurrentMonth");
		LocalInvTransactionalBudgetHome invTransactionalBudgetHome = null;
		LocalInvItemHome invItemHome = null;
		LocalAdUserHome adUserHome = null;
		LocalAdLookUpValueHome adLookUpValueHome = null;
		ArrayList list = new ArrayList();

		// Initialize EJB Home

		try {

			invTransactionalBudgetHome = (LocalInvTransactionalBudgetHome)EJBHomeFactory.
				lookUpLocalHome(LocalInvTransactionalBudgetHome.JNDI_NAME, LocalInvTransactionalBudgetHome.class);
			invItemHome = (LocalInvItemHome)EJBHomeFactory.
					lookUpLocalHome(LocalInvItemHome.JNDI_NAME, LocalInvItemHome.class);
			adUserHome = (LocalAdUserHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdUserHome.JNDI_NAME, LocalAdUserHome.class);
			adLookUpValueHome = (LocalAdLookUpValueHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdLookUpValueHome.JNDI_NAME, LocalAdLookUpValueHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}
		try {
			java.text.SimpleDateFormat sdf = new java.text.SimpleDateFormat("MM");
			java.text.SimpleDateFormat sdfYear = new java.text.SimpleDateFormat("yyyy");
			String month = sdf.format(date);

			LocalAdUser adUser = adUserHome.findByUsrName(userName, AD_CMPNY);
			LocalAdLookUpValue adLookUpValue = adLookUpValueHome.findByLuNameAndLvName("DEPARTMENT", adUser.getUsrDept(), AD_CMPNY);
			System.out.println("itemName-" + itemName);
			System.out.println("adLookUpValue.getLvCode()-" + adLookUpValue.getLvCode());
			System.out.println("Integer.parseInt(sdfYear.format(date))-" + Integer.parseInt(sdfYear.format(date)));
			LocalInvTransactionalBudget invTransactionalBudget = invTransactionalBudgetHome.findByTbItemNameAndTbDeptAndTbYear(itemName, adLookUpValue.getLvCode(), Integer.parseInt(sdfYear.format(date)), AD_CMPNY);
			//LocalInvItem invItem = LocalInvItemHome


			if (month.equals("01")){

				invTransactionalBudget.setTbQuantityJan( invTransactionalBudget.getTbQuantityJan() - qtyConsumed);
			}else if (month.equals("02")){

				invTransactionalBudget.setTbQuantityFeb( invTransactionalBudget.getTbQuantityFeb() - qtyConsumed);
			}else if (month.equals("03")){
				invTransactionalBudget.setTbQuantityMrch( invTransactionalBudget.getTbQuantityMrch() - qtyConsumed);
			}else if (month.equals("04")){
				invTransactionalBudget.setTbQuantityAprl( invTransactionalBudget.getTbQuantityAprl() - qtyConsumed);
			}else if (month.equals("05")){
				invTransactionalBudget.setTbQuantityMay( invTransactionalBudget.getTbQuantityMay() - qtyConsumed);
			}else if (month.equals("06")){
				invTransactionalBudget.setTbQuantityJun( invTransactionalBudget.getTbQuantityJun() - qtyConsumed);
			}else if (month.equals("07")){
				invTransactionalBudget.setTbQuantityJul( invTransactionalBudget.getTbQuantityJul() - qtyConsumed);
			}else if (month.equals("08")){
				invTransactionalBudget.setTbQuantityAug( invTransactionalBudget.getTbQuantityAug() - qtyConsumed);
			}else if (month.equals("09")){
				invTransactionalBudget.setTbQuantitySep( invTransactionalBudget.getTbQuantitySep() - qtyConsumed);
			}else if (month.equals("10")){
				invTransactionalBudget.setTbQuantityOct( invTransactionalBudget.getTbQuantityOct() - qtyConsumed);
			}else if (month.equals("11")){
				invTransactionalBudget.setTbQuantityNov( invTransactionalBudget.getTbQuantityNov() - qtyConsumed);
			}else {
				invTransactionalBudget.setTbQuantityDec( invTransactionalBudget.getTbQuantityDec() - qtyConsumed);
			}

		} catch (FinderException ex) {
			System.out.println("no item found in transactional budget table");
			  //throw new GlobalNoRecordFoundException();
        }
	}

	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public ArrayList getApTcAll(Integer AD_CMPNY) {

		Debug.print("ApPurchaseOrderEntryControllerBean getApTcAll");

		LocalApTaxCodeHome apTaxCodeHome = null;

		ArrayList list = new ArrayList();

		// Initialize EJB Home

		try {

			apTaxCodeHome = (LocalApTaxCodeHome)EJBHomeFactory.
			lookUpLocalHome(LocalApTaxCodeHome.JNDI_NAME, LocalApTaxCodeHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}

		try {

			Collection apTaxCodes = apTaxCodeHome.findEnabledTcAll(AD_CMPNY);

			Iterator i = apTaxCodes.iterator();

			while (i.hasNext()) {

				LocalApTaxCode apTaxCode = (LocalApTaxCode)i.next();

				list.add(apTaxCode.getTcName());

			}

			return list;

		} catch (Exception ex) {

			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());

		}

	}

	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public ApTaxCodeDetails getApTcByTcName(String TC_NM, Integer AD_CMPNY) {

		Debug.print("ApPurchaseOrderEntryControllerBean getArTcByTcName");

		LocalApTaxCodeHome apTaxCodeHome = null;

		ArrayList list = new ArrayList();

		// Initialize EJB Home

		try {

			apTaxCodeHome = (LocalApTaxCodeHome)EJBHomeFactory.
			lookUpLocalHome(LocalApTaxCodeHome.JNDI_NAME, LocalApTaxCodeHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}

		try {

			LocalApTaxCode apTaxCode = apTaxCodeHome.findByTcName(TC_NM, AD_CMPNY);

			ApTaxCodeDetails details = new ApTaxCodeDetails();
			details.setTcType(apTaxCode.getTcType());
			details.setTcRate(apTaxCode.getTcRate());

			return details;

		} catch (Exception ex) {

			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());

		}

	}

	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public ArrayList getGlFcAllWithDefault(Integer AD_CMPNY) {

		Debug.print("ApPurchaseOrderEntryControllerBean getGlFcAllWithDefault");

		LocalGlFunctionalCurrencyHome glFunctionalCurrencyHome = null;
		LocalAdCompanyHome adCompanyHome = null;

		Collection glFunctionalCurrencies = null;

		LocalGlFunctionalCurrency glFunctionalCurrency = null;
		LocalAdCompany adCompany = null;


		ArrayList list = new ArrayList();

		// Initialize EJB Home

		try {

			glFunctionalCurrencyHome = (LocalGlFunctionalCurrencyHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlFunctionalCurrencyHome.JNDI_NAME, LocalGlFunctionalCurrencyHome.class);
			adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}

		try {

			adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);

			glFunctionalCurrencies = glFunctionalCurrencyHome.findFcAllEnabled(
					EJBCommon.getGcCurrentDateWoTime().getTime(), AD_CMPNY);

		} catch (Exception ex) {

			throw new EJBException(ex.getMessage());
		}

		if (glFunctionalCurrencies.isEmpty()) {

			return null;

		}

		Iterator i = glFunctionalCurrencies.iterator();

		while (i.hasNext()) {

			glFunctionalCurrency = (LocalGlFunctionalCurrency)i.next();

			GlModFunctionalCurrencyDetails mdetails = new GlModFunctionalCurrencyDetails(
					glFunctionalCurrency.getFcCode(), glFunctionalCurrency.getFcName(),
					adCompany.getGlFunctionalCurrency().getFcName().equals(glFunctionalCurrency.getFcName()) ?
							EJBCommon.TRUE : EJBCommon.FALSE);

			list.add(mdetails);

		}

		return list;

	}

	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public ArrayList getInvLocAll(Integer AD_CMPNY) {

		Debug.print("ApPurchaseOrderEntryControllerBean getInvLocAll");

		LocalInvLocationHome invLocationHome = null;
		Collection invLocations = null;
		ArrayList list = new ArrayList();

		// Initialize EJB Home

		try {

			invLocationHome = (LocalInvLocationHome)EJBHomeFactory.
			lookUpLocalHome(LocalInvLocationHome.JNDI_NAME, LocalInvLocationHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}

		try {

			invLocations = invLocationHome.findLocAll(AD_CMPNY);

			if (invLocations.isEmpty()) {

				return null;

			}

			Iterator i = invLocations.iterator();

			while (i.hasNext()) {

				LocalInvLocation invLocation = (LocalInvLocation)i.next();
				String details = invLocation.getLocName();

				list.add(details);

			}

			return list;

		} catch (Exception ex) {

			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());

		}

	}

	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public short getGlFcPrecisionUnit(Integer AD_CMPNY) {

		Debug.print("ApPurchaseOrderEntryControllerBean getGlFcPrecisionUnit");


		LocalAdCompanyHome adCompanyHome = null;


		// Initialize EJB Home

		try {

			adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}

		try {

			LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);

			return  adCompany.getGlFunctionalCurrency().getFcPrecision();

		} catch (Exception ex) {

			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());

		}

	}

	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public short getInvGpQuantityPrecisionUnit(Integer AD_CMPNY) {

		Debug.print("ApPurchaseOrderEntryControllerBean getInvGpQuantityPrecisionUnit");

		LocalAdPreferenceHome adPreferenceHome = null;

		// Initialize EJB Home

		try {

			adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}


		try {

			LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);

			return adPreference.getPrfInvQuantityPrecisionUnit();

		} catch (Exception ex) {

			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());

		}

	}


	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public short getInvGpCostPrecisionUnit(Integer AD_CMPNY) {

		Debug.print("ApPurchaseOrderEntryControllerBean getInvGpCostPrecisionUnit");

		LocalAdPreferenceHome adPreferenceHome = null;

		// Initialize EJB Home

		try {

			adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}


		try {

			LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);

			return adPreference.getPrfInvCostPrecisionUnit();

		} catch (Exception ex) {

			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());

		}

	}

	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public short getAdPrfApJournalLineNumber(Integer AD_CMPNY) {

		Debug.print("ApDebitMemoEntryControllerBean getAdPrfApJournalLineNumber");

		LocalAdPreferenceHome adPreferenceHome = null;


		// Initialize EJB Home

		try {

			adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}


		try {

			LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);

			return adPreference.getPrfApJournalLineNumber();

		} catch (Exception ex) {

			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());

		}

	}

	/**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public boolean getInvTraceMisc(String II_NAME, Integer AD_CMPNY) {

        Debug.print("InvAdjustmentEntryControllerBean getInvLocAll");

        LocalInvLocationHome invLocationHome = null;
        LocalInvItemHome invItemHome = null;
        Collection invLocations = null;
        ArrayList list = new ArrayList();
        boolean isTraceMisc = false;
        // Initialize EJB Home

        try {

        	invLocationHome = (LocalInvLocationHome)EJBHomeFactory.
                lookUpLocalHome(LocalInvLocationHome.JNDI_NAME, LocalInvLocationHome.class);
        	invItemHome = (LocalInvItemHome)EJBHomeFactory.
                    lookUpLocalHome(LocalInvItemHome.JNDI_NAME, LocalInvItemHome.class);

        } catch (NamingException ex) {

            throw new EJBException(ex.getMessage());

        }

        try {

            LocalInvItem invItem = invItemHome.findByIiName(II_NAME, AD_CMPNY);
            if (invItem.getIiTraceMisc() == 1){
            	isTraceMisc = true;
            }
            return isTraceMisc;
    	} catch (Exception ex) {

    		Debug.printStackTrace(ex);
    		throw new EJBException(ex.getMessage());

    	}

    }

	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public ApModPurchaseOrderDetails getApPoByPoCode(Integer PO_CODE, Integer AD_CMPNY)
	throws GlobalNoRecordFoundException {

		Debug.print("ApPurchaseOrderEntryControllerBean getApPoByPoCode");

		LocalApPurchaseOrderHome apPurchaseOrderHome = null;
		LocalAdCompanyHome adCompanyHome = null;
		LocalGenValueSetValueHome genValueSetValueHome = null;
		LocalGenSegmentHome genSegmentHome = null;
		LocalInvTagHome invTagHome = null;

		// Initialize EJB Home

		try {

			apPurchaseOrderHome = (LocalApPurchaseOrderHome)EJBHomeFactory.
			lookUpLocalHome(LocalApPurchaseOrderHome.JNDI_NAME, LocalApPurchaseOrderHome.class);
			adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
			genValueSetValueHome = (LocalGenValueSetValueHome)EJBHomeFactory.
			lookUpLocalHome(LocalGenValueSetValueHome.JNDI_NAME, LocalGenValueSetValueHome.class);
			genSegmentHome = (LocalGenSegmentHome)EJBHomeFactory.
			lookUpLocalHome(LocalGenSegmentHome.JNDI_NAME, LocalGenSegmentHome.class);
			invTagHome = (LocalInvTagHome)EJBHomeFactory.
                	lookUpLocalHome(LocalInvTagHome.JNDI_NAME, LocalInvTagHome.class);
		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}

		try {

			LocalApPurchaseOrder apPurchaseOrder = null;


			try {

				apPurchaseOrder = apPurchaseOrderHome.findByPrimaryKey(PO_CODE);

			} catch (FinderException ex) {

				throw new GlobalNoRecordFoundException();

			}

			String specs = null;
        	String propertyCode = null;
        	String expiryDate = null;
        	String serialNumber = null;
			ArrayList list = new ArrayList();

			// get purchase order lines if any

			Collection apPurchaseOrderLines = apPurchaseOrder.getApPurchaseOrderLines();

			Iterator i = apPurchaseOrderLines.iterator();

			while (i.hasNext()) {

				LocalApPurchaseOrderLine apPurchaseOrderLine = (LocalApPurchaseOrderLine) i.next();

				ApModPurchaseOrderLineDetails plDetails = new ApModPurchaseOrderLineDetails();

				ArrayList tagList = new ArrayList();
				plDetails.setTraceMisc(apPurchaseOrderLine.getInvItemLocation().getInvItem().getIiTraceMisc());
	        	System.out.println(plDetails.getTraceMisc() + "<== getTraceMisc under getInvAdjByAdjCode controllerbean");
	        	if (plDetails.getTraceMisc() == 1){

	        		tagList = this.getInvTagList(apPurchaseOrderLine);

	    			plDetails.setPlTagList(tagList);

	        	}
				plDetails.setPlCode(apPurchaseOrderLine.getPlCode());
				plDetails.setPlLine(apPurchaseOrderLine.getPlLine());
				plDetails.setPlIiName(apPurchaseOrderLine.getInvItemLocation().getInvItem().getIiName());
				plDetails.setPlLocName(apPurchaseOrderLine.getInvItemLocation().getInvLocation().getLocName());
				plDetails.setPlQuantity(apPurchaseOrderLine.getPlQuantity());
				plDetails.setPlUomName(apPurchaseOrderLine.getInvUnitOfMeasure().getUomName());
				plDetails.setPlUnitCost(apPurchaseOrderLine.getPlUnitCost());
				plDetails.setPlQcNumber(apPurchaseOrderLine.getPlQcNumber());
				plDetails.setPlQcExpiryDate(apPurchaseOrderLine.getPlQcExpiryDate());
				plDetails.setPlIiDescription(apPurchaseOrderLine.getInvItemLocation().getInvItem().getIiDescription());
    			plDetails.setPlDiscount1(apPurchaseOrderLine.getPlDiscount1());
    			plDetails.setPlDiscount2(apPurchaseOrderLine.getPlDiscount2());
    			plDetails.setPlDiscount3(apPurchaseOrderLine.getPlDiscount3());
    			plDetails.setPlDiscount4(apPurchaseOrderLine.getPlDiscount4());
    			plDetails.setPlTotalDiscount(apPurchaseOrderLine.getPlTotalDiscount());
    			plDetails.setPlAmount(apPurchaseOrderLine.getPlAmount());

				list.add(plDetails);

			}

			ApModPurchaseOrderDetails mPoDetails = new ApModPurchaseOrderDetails();

			mPoDetails.setPoCode(apPurchaseOrder.getPoCode());

                        if(apPurchaseOrder.getApVoucherBatch()!=null){
                            mPoDetails.setPoBatchName(apPurchaseOrder.getApVoucherBatch().getVbName());
                        }


			mPoDetails.setPoDate(apPurchaseOrder.getPoDate());
			mPoDetails.setPoDeliveryPeriod(apPurchaseOrder.getPoDeliveryPeriod());
			mPoDetails.setPoDocumentNumber(apPurchaseOrder.getPoDocumentNumber());
			mPoDetails.setPoReferenceNumber(apPurchaseOrder.getPoReferenceNumber());
			mPoDetails.setPoDescription(apPurchaseOrder.getPoDescription());
			mPoDetails.setPoVoid(apPurchaseOrder.getPoVoid());
			mPoDetails.setPoShipmentNumber(apPurchaseOrder.getPoShipmentNumber());
			mPoDetails.setPoBillTo(apPurchaseOrder.getPoBillTo());
			mPoDetails.setPoShipTo(apPurchaseOrder.getPoShipTo());
			mPoDetails.setPoConversionDate(apPurchaseOrder.getPoConversionDate());
			mPoDetails.setPoConversionRate(apPurchaseOrder.getPoConversionRate());
			mPoDetails.setPoApprovalStatus(apPurchaseOrder.getPoApprovalStatus());
			mPoDetails.setPoPosted(apPurchaseOrder.getPoPosted());
			mPoDetails.setPoCreatedBy(apPurchaseOrder.getPoCreatedBy());
			mPoDetails.setPoDateCreated(apPurchaseOrder.getPoDateCreated());
			mPoDetails.setPoLastModifiedBy(apPurchaseOrder.getPoLastModifiedBy());
			mPoDetails.setPoDateLastModified(apPurchaseOrder.getPoDateLastModified());
			mPoDetails.setPoApprovedRejectedBy(apPurchaseOrder.getPoApprovedRejectedBy());
			mPoDetails.setPoDateApprovedRejected(apPurchaseOrder.getPoDateApprovedRejected());
			mPoDetails.setPoPostedBy(apPurchaseOrder.getPoPostedBy());
			mPoDetails.setPoDatePosted(apPurchaseOrder.getPoDatePosted());
			mPoDetails.setPoReasonForRejection(apPurchaseOrder.getPoReasonForRejection());
			mPoDetails.setPoSplSupplierCode(apPurchaseOrder.getApSupplier().getSplSupplierCode());
			mPoDetails.setPoPytName(apPurchaseOrder.getAdPaymentTerm().getPytName());
			mPoDetails.setPoTcName(apPurchaseOrder.getApTaxCode().getTcName());
        	mPoDetails.setPoTcType(apPurchaseOrder.getApTaxCode().getTcType());
        	mPoDetails.setPoTcRate(apPurchaseOrder.getApTaxCode().getTcRate());
			mPoDetails.setPoFcName(apPurchaseOrder.getGlFunctionalCurrency().getFcName());
			mPoDetails.setPoPrinted(EJBCommon.FALSE);
			mPoDetails.setPoSplName(apPurchaseOrder.getApSupplier().getSplName());

			mPoDetails.setPoPlList(list);

			return mPoDetails;

		} catch (GlobalNoRecordFoundException ex) {

			throw ex;

		} catch (Exception ex) {

			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());

		}

	}

	/**
	 * @ejb:interface-method view-type="remote"
	 **/
	public Integer saveApPoEntry(com.util.ApPurchaseOrderDetails details,String BTCH_NM, String PYT_NM, String TC_NM, String FC_NM, String SPL_SPPLR_CODE, ArrayList plList, boolean isDraft, boolean validateShipmentNumber, Integer AD_BRNCH, Integer AD_CMPNY) throws
	GlobalRecordAlreadyDeletedException,
	GlobalDocumentNumberNotUniqueException,
	GlobalConversionDateNotExistException,
	GlobalPaymentTermInvalidException,
	GlobalTransactionAlreadyApprovedException,
	GlobalTransactionAlreadyPendingException,
	GlobalTransactionAlreadyPostedException,
	GlobalTransactionAlreadyVoidException,
	GlobalInvItemLocationNotFoundException,
	GlobalNoApprovalRequesterFoundException,
	GlobalNoApprovalApproverFoundException,
	GlobalSupplierItemInvalidException,
	GlobalRecordInvalidException {

		Debug.print("ApPurchaseOrderEntryControllerBean saveApPoEntry");

		LocalApPurchaseOrderHome apPurchaseOrderHome = null;
		LocalApPurchaseOrderLineHome apPurchaseOrderLineHome = null;
		LocalAdCompanyHome adCompanyHome = null;
		LocalAdDocumentSequenceAssignmentHome adDocumentSequenceAssignmentHome = null;
		LocalApSupplierHome apSupplierHome = null;
		LocalAdPaymentTermHome adPaymentTermHome = null;
                LocalApVoucherBatchHome apVoucherBatchHome = null;
		LocalApTaxCodeHome apTaxCodeHome = null;
		LocalGlFunctionalCurrencyHome glFunctionalCurrencyHome = null;
		LocalGlFunctionalCurrencyRateHome glFunctionalCurrencyRateHome = null;
		LocalAdPreferenceHome adPreferenceHome = null;
		LocalInvItemLocationHome invItemLocationHome = null;
		LocalInvUnitOfMeasureHome invUnitOfMeasureHome = null;
		LocalAdApprovalHome adApprovalHome = null;
		LocalAdAmountLimitHome adAmountLimitHome = null;
		LocalAdApprovalUserHome adApprovalUserHome = null;
        LocalAdApprovalQueueHome adApprovalQueueHome = null;
        LocalAdBranchDocumentSequenceAssignmentHome adBranchDocumentSequenceAssignmentHome = null;
        LocalInvItemHome invItemHome = null;
		LocalInvTransactionalBudgetHome invTransactionalBudgetHome = null;
		LocalApPurchaseRequisitionHome apPurchaseRequisitionHome = null;
		LocalInvTagHome invTagHome = null;
		LocalApPurchaseOrder apPurchaseOrder = null;
		LocalAdUserHome adUserHome = null;
		// Initialize EJB Home

		try {

			apPurchaseOrderHome = (LocalApPurchaseOrderHome)EJBHomeFactory.
				lookUpLocalHome(LocalApPurchaseOrderHome.JNDI_NAME, LocalApPurchaseOrderHome.class);
			apPurchaseOrderLineHome = (LocalApPurchaseOrderLineHome)EJBHomeFactory.
				lookUpLocalHome(LocalApPurchaseOrderLineHome.JNDI_NAME, LocalApPurchaseOrderLineHome.class);
			adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
			adDocumentSequenceAssignmentHome = (LocalAdDocumentSequenceAssignmentHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdDocumentSequenceAssignmentHome.JNDI_NAME, LocalAdDocumentSequenceAssignmentHome.class);
			apSupplierHome = (LocalApSupplierHome)EJBHomeFactory.
				lookUpLocalHome(LocalApSupplierHome.JNDI_NAME, LocalApSupplierHome.class);
			adPaymentTermHome = (LocalAdPaymentTermHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdPaymentTermHome.JNDI_NAME, LocalAdPaymentTermHome.class);
                        apVoucherBatchHome = (LocalApVoucherBatchHome)EJBHomeFactory.
				lookUpLocalHome(LocalApVoucherBatchHome.JNDI_NAME, LocalApVoucherBatchHome.class);
			apTaxCodeHome = (LocalApTaxCodeHome)EJBHomeFactory.
				lookUpLocalHome(LocalApTaxCodeHome.JNDI_NAME, LocalApTaxCodeHome.class);
			glFunctionalCurrencyHome = (LocalGlFunctionalCurrencyHome)EJBHomeFactory.
				lookUpLocalHome(LocalGlFunctionalCurrencyHome.JNDI_NAME, LocalGlFunctionalCurrencyHome.class);
			glFunctionalCurrencyRateHome = (LocalGlFunctionalCurrencyRateHome)EJBHomeFactory.
				lookUpLocalHome(LocalGlFunctionalCurrencyRateHome.JNDI_NAME, LocalGlFunctionalCurrencyRateHome.class);
			adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);
			invItemLocationHome = (LocalInvItemLocationHome)EJBHomeFactory.
				lookUpLocalHome(LocalInvItemLocationHome.JNDI_NAME, LocalInvItemLocationHome.class);
			invUnitOfMeasureHome = (LocalInvUnitOfMeasureHome)EJBHomeFactory.
				lookUpLocalHome(LocalInvUnitOfMeasureHome.JNDI_NAME, LocalInvUnitOfMeasureHome.class);
			adApprovalHome = (LocalAdApprovalHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdApprovalHome.JNDI_NAME, LocalAdApprovalHome.class);
			adAmountLimitHome = (LocalAdAmountLimitHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdAmountLimitHome.JNDI_NAME, LocalAdAmountLimitHome.class);
			adApprovalUserHome = (LocalAdApprovalUserHome)EJBHomeFactory.
        		lookUpLocalHome(LocalAdApprovalUserHome.JNDI_NAME, LocalAdApprovalUserHome.class);
			adApprovalQueueHome = (LocalAdApprovalQueueHome)EJBHomeFactory.
        		lookUpLocalHome(LocalAdApprovalQueueHome.JNDI_NAME, LocalAdApprovalQueueHome.class);
			adBranchDocumentSequenceAssignmentHome = (LocalAdBranchDocumentSequenceAssignmentHome)EJBHomeFactory.
    			lookUpLocalHome(LocalAdBranchDocumentSequenceAssignmentHome.JNDI_NAME, LocalAdBranchDocumentSequenceAssignmentHome.class);
			invItemHome = (LocalInvItemHome)EJBHomeFactory.
			lookUpLocalHome(LocalInvItemHome.JNDI_NAME, LocalInvItemHome.class);
			apPurchaseRequisitionHome = (LocalApPurchaseRequisitionHome)EJBHomeFactory.
			lookUpLocalHome(LocalApPurchaseRequisitionHome.JNDI_NAME, LocalApPurchaseRequisitionHome.class);
			invTagHome = (LocalInvTagHome)EJBHomeFactory.
    				lookUpLocalHome(LocalInvTagHome.JNDI_NAME, LocalInvTagHome.class);
			adUserHome = (LocalAdUserHome)EJBHomeFactory.
    				lookUpLocalHome(LocalAdUserHome.JNDI_NAME, LocalAdUserHome.class);


		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}

		try {

			// validate if purchase order is already deleted

			try {

				if (details.getPoCode() != null) {

					apPurchaseOrder = apPurchaseOrderHome.findByPrimaryKey(details.getPoCode());

				}

			} catch (FinderException ex) {

				throw new GlobalRecordAlreadyDeletedException();

			}

			// validate if purchase order is already posted, void, approved or pending

			if (details.getPoCode() != null && details.getPoVoid() == EJBCommon.FALSE) {

	        	if (apPurchaseOrder.getPoApprovalStatus() != null) {

	        		if (apPurchaseOrder.getPoApprovalStatus().equals("APPROVED") ||
	        			apPurchaseOrder.getPoApprovalStatus().equals("N/A")) {

	        		    throw new GlobalTransactionAlreadyApprovedException();

	        		} else if (apPurchaseOrder.getPoApprovalStatus().equals("PENDING")) {

	        			throw new GlobalTransactionAlreadyPendingException();

	        		}
	        	}

        		if (apPurchaseOrder.getPoPosted() == EJBCommon.TRUE) {

        			throw new GlobalTransactionAlreadyPostedException();

        		} else if (apPurchaseOrder.getPoVoid() == EJBCommon.TRUE) {

        			throw new GlobalTransactionAlreadyVoidException();

        		}
        	}

			// purchase order void

			if (details.getPoCode() != null && details.getPoVoid() == EJBCommon.TRUE) {

				apPurchaseOrder.setPoVoid(EJBCommon.TRUE);
				apPurchaseOrder.setPoLastModifiedBy(details.getPoLastModifiedBy());
				apPurchaseOrder.setPoDateLastModified(details.getPoDateLastModified());

				return apPurchaseOrder.getPoCode();

			}

			// validate if document number is unique document number is automatic then set next sequence

			try {

			    LocalAdBranchDocumentSequenceAssignment adBranchDocumentSequenceAssignment = null;
	 			LocalAdDocumentSequenceAssignment adDocumentSequenceAssignment = null;

				if (details.getPoCode() == null) {

				    try {

				        adDocumentSequenceAssignment = adDocumentSequenceAssignmentHome.findByDcName("AP PURCHASE ORDER", AD_CMPNY);

				    } catch(FinderException ex) { }

				    try {

	 					adBranchDocumentSequenceAssignment = adBranchDocumentSequenceAssignmentHome.findBdsByDsaCodeAndBrCode(adDocumentSequenceAssignment.getDsaCode(), AD_BRNCH, AD_CMPNY);

	 				} catch (FinderException ex) { }

					LocalApPurchaseOrder apExistingPurchaseOrder = null;

					try {

						apExistingPurchaseOrder = apPurchaseOrderHome.findByPoDocumentNumberAndBrCode(
								details.getPoDocumentNumber(), AD_BRNCH, AD_CMPNY);

					} catch (FinderException ex) {
					}

					if (apExistingPurchaseOrder != null) {

						throw new GlobalDocumentNumberNotUniqueException();

					}

					if (adDocumentSequenceAssignment.getAdDocumentSequence().getDsNumberingType() == 'A' &&
							(details.getPoDocumentNumber() == null || details.getPoDocumentNumber().trim().length() == 0)) {

						while (true) {

						    if (adBranchDocumentSequenceAssignment == null || adBranchDocumentSequenceAssignment.getBdsNextSequence() == null) {

								try {

									apPurchaseOrderHome.findByPoDocumentNumberAndBrCode(adDocumentSequenceAssignment.getDsaNextSequence(), AD_BRNCH, AD_CMPNY);
									adDocumentSequenceAssignment.setDsaNextSequence(EJBCommon.incrementStringNumber(adDocumentSequenceAssignment.getDsaNextSequence()));

								} catch (FinderException ex) {

									details.setPoDocumentNumber(adDocumentSequenceAssignment.getDsaNextSequence());
									adDocumentSequenceAssignment.setDsaNextSequence(EJBCommon.incrementStringNumber(adDocumentSequenceAssignment.getDsaNextSequence()));
									break;

								}

						    } else {

						        try {

									apPurchaseOrderHome.findByPoDocumentNumberAndBrCode(adBranchDocumentSequenceAssignment.getBdsNextSequence(), AD_BRNCH, AD_CMPNY);
									adBranchDocumentSequenceAssignment.setBdsNextSequence(EJBCommon.incrementStringNumber(adBranchDocumentSequenceAssignment.getBdsNextSequence()));

								} catch (FinderException ex) {

									details.setPoDocumentNumber(adBranchDocumentSequenceAssignment.getBdsNextSequence());
									adBranchDocumentSequenceAssignment.setBdsNextSequence(EJBCommon.incrementStringNumber(adBranchDocumentSequenceAssignment.getBdsNextSequence()));
									break;

								}

						    }

						}

					}

				} else {

				    LocalApPurchaseOrder apExistingPurchaseOrder = null;

					try {

						apExistingPurchaseOrder = apPurchaseOrderHome.findByPoDocumentNumberAndBrCode(
								details.getPoDocumentNumber(), AD_BRNCH, AD_CMPNY);

					} catch (FinderException ex) {
					}

					if (apExistingPurchaseOrder != null &&
							!apExistingPurchaseOrder.getPoCode().equals(details.getPoCode())) {

						throw new GlobalDocumentNumberNotUniqueException();

					}

					if (apPurchaseOrder.getPoDocumentNumber() != details.getPoDocumentNumber() &&
							(details.getPoDocumentNumber() == null || details.getPoDocumentNumber().trim().length() == 0)) {

						details.setPoDocumentNumber(apPurchaseOrder.getPoDocumentNumber());

					}

				}

			} catch (GlobalDocumentNumberNotUniqueException ex) {

	 			getSessionContext().setRollbackOnly();
	 			throw ex;

	 		} catch (Exception ex) {

	 			Debug.printStackTrace(ex);
	 			getSessionContext().setRollbackOnly();
	 			throw new EJBException(ex.getMessage());

	 		}

			try {
				System.out.println("PO SHIPMENT" + details.getPoShipmentNumber() + details.getPoShipmentNumber().length());
			} catch (Exception ex) {

			}

	 		// validate if shipment number exists
	 		if (validateShipmentNumber && details.getPoShipmentNumber() != null && details.getPoShipmentNumber().length() > 0) {

	 			Collection apExistingPurchaseOrderLines = apPurchaseOrderLineHome.findByPoReceivingAndPoShipmentAndBrCode(EJBCommon.FALSE, details.getPoShipmentNumber(), AD_CMPNY);
	 			if (apExistingPurchaseOrderLines.size() > 0) {

	 				Iterator i = apExistingPurchaseOrderLines.iterator();
	 				while (i.hasNext()) {
	 					LocalApPurchaseOrderLine apExistingPurchaseOrderLine = (LocalApPurchaseOrderLine)i.next();
	 					if (!apExistingPurchaseOrderLine.getApPurchaseOrder().getPoCode().equals(details.getPoCode()))
	 						throw new GlobalRecordInvalidException();
	 				}

	 			}

	 		} else if (!validateShipmentNumber && details.getPoShipmentNumber() != null && details.getPoShipmentNumber().length() > 0){
	 			System.out.println("details.getPoShipmentNumber()" + details.getPoShipmentNumber());
	 			Collection apExistingPurchaseOrderLines = apPurchaseOrderLineHome.findByPoReceivingAndPoShipmentAndBrCode(EJBCommon.FALSE, details.getPoShipmentNumber(), AD_CMPNY);
	 			System.out.println("PL SIZE-" + apExistingPurchaseOrderLines.size());
	 			if (apExistingPurchaseOrderLines.size() == 0) throw new GlobalRecordInvalidException();


	 		}
		 	// validate if conversion date exists
	
	            try {
	
	                if (details.getPoConversionDate() != null) {
	
	
	                    LocalGlFunctionalCurrency glValidateFunctionalCurrency = glFunctionalCurrencyHome.findByFcName(FC_NM, AD_CMPNY);
	                    LocalGlFunctionalCurrencyRate glFunctionalCurrencyRate =
	                        glFunctionalCurrencyRateHome.findByFcCodeAndDate(glValidateFunctionalCurrency.getFcCode(),
	                                details.getPoConversionDate(), AD_CMPNY);
	                    
	                    details.setPoConversionRate(glFunctionalCurrencyRate.getFrXToUsd());
	
	                }
	
	            } catch (FinderException ex) {
	
	                throw new GlobalConversionDateNotExistException();
	
	            }

			// validate if payment term has at least one payment schedule

			if (adPaymentTermHome.findByPytName(PYT_NM, AD_CMPNY).getAdPaymentSchedules().isEmpty()) {

				throw new GlobalPaymentTermInvalidException();

			}

			boolean isRecalculate = true;

			// create purchase order

			if (details.getPoCode() == null) {
				System.out.println("PO CODE====="+details.getPoCode() );
				System.out.println("details.getPoShipmentNumber()---"+details.getPoShipmentNumber());
				apPurchaseOrder = apPurchaseOrderHome.create(EJBCommon.FALSE, null, details.getPoDate(), details.getPoDeliveryPeriod(), details.getPoDocumentNumber(),
						details.getPoReferenceNumber(), null, details.getPoDescription(), details.getPoBillTo(), details.getPoShipTo(),
						details.getPoConversionDate(), details.getPoConversionRate(), 0d, EJBCommon.FALSE, EJBCommon.FALSE, details.getPoShipmentNumber(), null,
						EJBCommon.FALSE, null, details.getPoCreatedBy(), details.getPoDateCreated(), details.getPoLastModifiedBy(),
						details.getPoDateLastModified(), null, null, null, null, EJBCommon.FALSE,
						0d,0d,0d,0d,0d,
						AD_BRNCH,AD_CMPNY);



				/*.create(EJBCommon.FALSE, null, details.getPoDate(), details.getPoDeliveryPeriod(), details.getPoDocumentNumber(),
						details.getPoReferenceNumber(), null, details.getPoDescription(), details.getPoBillTo(), details.getPoShipTo(),
						details.getPoConversionDate(), details.getPoConversionRate(), 0d, EJBCommon.FALSE, EJBCommon.FALSE, details.getPoShipmentNumber(), null,
						EJBCommon.FALSE, null, details.getPoCreatedBy(), details.getPoDateCreated(), details.getPoLastModifiedBy(),
						details.getPoDateLastModified(), null, null, null, null, EJBCommon.FALSE, EJBCommon.FALSE,
						EJBCommon.FALSE,
						EJBCommon.FALSE, EJBCommon.FALSE, EJBCommon.FALSE, EJBCommon.FALSE,
						EJBCommon.FALSE, EJBCommon.FALSE, EJBCommon.FALSE, EJBCommon.FALSE,
						null,null, null, null, null,null,null,
						AD_BRNCH, AD_CMPNY);*/

			} else {
				System.out.println("PO CODE NOT NULL");
				// check if critical fields are changed

				if (!apPurchaseOrder.getApTaxCode().getTcName().equals(TC_NM) ||
						!apPurchaseOrder.getApSupplier().getSplSupplierCode().equals(SPL_SPPLR_CODE) ||
						!apPurchaseOrder.getAdPaymentTerm().getPytName().equals(PYT_NM) ||
						plList.size() != apPurchaseOrder.getApPurchaseOrderLines().size()) {

					isRecalculate = true;

				} else if (plList.size() == apPurchaseOrder.getApPurchaseOrderLines().size()) {

					Iterator ilIter = apPurchaseOrder.getApPurchaseOrderLines().iterator();
					Iterator ilListIter = plList.iterator();

					while (ilIter.hasNext()) {

						LocalApPurchaseOrderLine apPurchaseOrderLine = (LocalApPurchaseOrderLine)ilIter.next();
						ApModPurchaseOrderLineDetails mdetails = (ApModPurchaseOrderLineDetails)ilListIter.next();

						if (!apPurchaseOrderLine.getInvItemLocation().getInvItem().getIiName().equals(mdetails.getPlIiName()) ||
								!apPurchaseOrderLine.getInvItemLocation().getInvItem().getIiDescription().equals(mdetails.getPlIiDescription()) ||
								!apPurchaseOrderLine.getInvItemLocation().getInvLocation().getLocName().equals(mdetails.getPlLocName()) ||
								!apPurchaseOrderLine.getInvUnitOfMeasure().getUomName().equals(mdetails.getPlUomName()) ||
								apPurchaseOrderLine.getPlQuantity() != mdetails.getPlQuantity() ||
								apPurchaseOrderLine.getPlUnitCost() != mdetails.getPlUnitCost() ||
								apPurchaseOrderLine.getPlQcNumber() != mdetails.getPlQcNumber() ||
								apPurchaseOrderLine.getPlQcExpiryDate() != mdetails.getPlQcExpiryDate() ||
								apPurchaseOrderLine.getPlTotalDiscount() != mdetails.getPlTotalDiscount()) {

							isRecalculate = true;
							break;

						}
						isRecalculate = false;

					}

				} else {

					isRecalculate = false;

				}

				apPurchaseOrder.setPoDate(details.getPoDate());
				apPurchaseOrder.setPoDeliveryPeriod(details.getPoDeliveryPeriod());
				apPurchaseOrder.setPoDocumentNumber(details.getPoDocumentNumber());
				apPurchaseOrder.setPoReferenceNumber(details.getPoReferenceNumber());
				apPurchaseOrder.setPoDescription(details.getPoDescription());
				apPurchaseOrder.setPoBillTo(details.getPoBillTo());
				apPurchaseOrder.setPoShipTo(details.getPoShipTo());
				apPurchaseOrder.setPoPrinted(details.getPoPrinted());
				apPurchaseOrder.setPoVoid(details.getPoVoid());
				apPurchaseOrder.setPoShipmentNumber(details.getPoShipmentNumber());
				apPurchaseOrder.setPoConversionDate(details.getPoConversionDate());
				apPurchaseOrder.setPoConversionRate(details.getPoConversionRate());
				apPurchaseOrder.setPoLastModifiedBy(details.getPoLastModifiedBy());
				apPurchaseOrder.setPoDateLastModified(details.getPoDateLastModified());

			}

			LocalApSupplier apSupplier = apSupplierHome.findBySplSupplierCode(SPL_SPPLR_CODE, AD_CMPNY);
			apPurchaseOrder.setApSupplier(apSupplier);

            try {

            	 LocalApVoucherBatch apVoucherBatch = apVoucherBatchHome.findByVbName(BTCH_NM, AD_BRNCH, AD_CMPNY);
                 apPurchaseOrder.setApVoucherBatch(apVoucherBatch);

            }catch (FinderException ex){

            }

			LocalAdPaymentTerm adPaymentTerm = adPaymentTermHome.findByPytName(PYT_NM, AD_CMPNY);
			apPurchaseOrder.setAdPaymentTerm(adPaymentTerm);

			LocalApTaxCode apTaxCode = apTaxCodeHome.findByTcName(TC_NM, AD_CMPNY);
			apPurchaseOrder.setApTaxCode(apTaxCode);

			LocalGlFunctionalCurrency glFunctionalCurrency = glFunctionalCurrencyHome.findByFcName(FC_NM, AD_CMPNY);
			apPurchaseOrder.setGlFunctionalCurrency(glFunctionalCurrency);


			double ABS_TOTAL_AMOUNT = 0d;

			//	 Map Supplier and Item

			Iterator iter = plList.iterator();

			while (iter.hasNext()) {

				ApModPurchaseOrderLineDetails mPlDetails = (ApModPurchaseOrderLineDetails) iter.next();
				LocalInvItem invItem = invItemHome.findByIiName(mPlDetails.getPlIiName(), AD_CMPNY);

				if(invItem.getApSupplier() != null &&
						!invItem.getApSupplier().getSplSupplierCode().equals(
								apPurchaseOrder.getApSupplier().getSplSupplierCode())) {

					throw new GlobalSupplierItemInvalidException("" + mPlDetails.getPlLine());

				}

			}


			if (isRecalculate) {

				// remove all purchase order line items

				Collection apPurchaseOrderLines = apPurchaseOrder.getApPurchaseOrderLines();

				Iterator i = apPurchaseOrderLines.iterator();

				while (i.hasNext()) {

					LocalApPurchaseOrderLine apPurchaseOrderLine = (LocalApPurchaseOrderLine)i.next();

					//remove all inv tag inside PO line
		  	   	    Collection invTags = apPurchaseOrderLine.getInvTags();

		  	   	    Iterator x = invTags.iterator();

		  	   	    while (x.hasNext()){

		  	   	    	LocalInvTag invTag = (LocalInvTag)x.next();

		  	   	    	x.remove();

		  	   	    	invTag.remove();
		  	   	    }

					i.remove();

					apPurchaseOrderLine.remove();

				}

				// add new purchase order line item

				i = plList.iterator();

				LocalInvItemLocation invItemLocation = null;

				while (i.hasNext()) {

					ApModPurchaseOrderLineDetails mPlDetails = (ApModPurchaseOrderLineDetails) i.next();

					LocalApPurchaseOrderLine apPurchaseOrderLine = apPurchaseOrderLineHome.create(
							mPlDetails.getPlLine(),	mPlDetails.getPlQuantity(), mPlDetails.getPlUnitCost(),
							mPlDetails.getPlAmount(), mPlDetails.getPlQcNumber(), mPlDetails.getPlQcExpiryDate(), mPlDetails.getPlConversionFactor(), 0d, null, mPlDetails.getPlDiscount1(),
							mPlDetails.getPlDiscount2(), mPlDetails.getPlDiscount3(), mPlDetails.getPlDiscount4(),
							mPlDetails.getPlTotalDiscount(), AD_CMPNY);

					//apPurchaseOrder.addApPurchaseOrderLine(apPurchaseOrderLine);
					apPurchaseOrderLine.setApPurchaseOrder(apPurchaseOrder);

					try {

						invItemLocation = invItemLocationHome.findByLocNameAndIiName(
								mPlDetails.getPlLocName(), mPlDetails.getPlIiName(), AD_CMPNY);

					} catch (FinderException ex) {

						throw new GlobalInvItemLocationNotFoundException(String.valueOf(mPlDetails.getPlLine()));

					}

					ABS_TOTAL_AMOUNT += apPurchaseOrderLine.getPlAmount();

					//invItemLocation.addApPurchaseOrderLine(apPurchaseOrderLine);
					apPurchaseOrderLine.setInvItemLocation(invItemLocation);

					LocalInvUnitOfMeasure invUnitOfMeasure = invUnitOfMeasureHome.findByUomName(
							mPlDetails.getPlUomName(), AD_CMPNY);

					//invUnitOfMeasure.addApPurchaseOrderLine(apPurchaseOrderLine);
					apPurchaseOrderLine.setInvUnitOfMeasure(invUnitOfMeasure);


					if (apPurchaseOrderLine.getInvItemLocation().getInvItem().getIiTraceMisc() == 1){
						//TODO: add new inv Tag

						this.createInvTagList(apPurchaseOrderLine,  mPlDetails.getPlTagList(), AD_CMPNY);

						/*
		      	  	    try{
		      	  	    	System.out.println("aabot?");
		      	  	    	//Iterator t = apPurchaseOrderLine.getInvTag().iterator();




			      	  	    Iterator t = mPlDetails.getPlTagList().iterator();

			      	  	    LocalInvTag invTag  = null;
			      	  	    System.out.println("umabot?");
			      	  	    while (t.hasNext()){
			      	  	    	InvModTagListDetails tgLstDetails = (InvModTagListDetails)t.next();
			      	  	    	System.out.println(tgLstDetails.getTgCustodian() + "<== custodian");
			      	  	    	System.out.println(tgLstDetails.getTgSpecs() + "<== specs");
			      	  	    	System.out.println(tgLstDetails.getTgPropertyCode() + "<== propertyCode");
			      	  	    	System.out.println(tgLstDetails.getTgExpiryDate() + "<== expiryDate");
			      	  	    	System.out.println(tgLstDetails.getTgSerialNumber() + "<== serial number");

			      	  	    	if (tgLstDetails.getTgCode()==null){
			      	  	    		System.out.println("ngcreate ba?");
				      	  	    	invTag = invTagHome.create(tgLstDetails.getTgPropertyCode(),
				      	  	    			tgLstDetails.getTgSerialNumber(),null,tgLstDetails.getTgExpiryDate(),
				      	  	    			tgLstDetails.getTgSpecs(), AD_CMPNY, tgLstDetails.getTgTransactionDate(),
				      	  	    			tgLstDetails.getTgType());

				      	  	    	invTag.setApPurchaseOrderLine(apPurchaseOrderLine);
				      	  	    	invTag.setInvItemLocation(apPurchaseOrderLine.getInvItemLocation());
				      	  	    	LocalAdUser adUser = null;
				      	  	    	try {
				      	  	    		adUser = adUserHome.findByUsrName(tgLstDetails.getTgCustodian(), AD_CMPNY);
				      	  	    	}catch(FinderException ex){

				      	  	    	}
				      	  	    	invTag.setAdUser(adUser);
				      	  	    	System.out.println("ngcreate ba?");
			      	  	    	}

			      	  	    }

		      	  	    }catch(Exception ex){

		      	  	    }
		      	  	    */
					}
				}

			} else {
				System.out.println("is not recalculate");
				Iterator y = plList.iterator();
				System.out.println(plList + "<== piList");
				while (y.hasNext()) {
					ApModPurchaseOrderLineDetails mPlDetails = (ApModPurchaseOrderLineDetails) y.next();
					System.out.println(mPlDetails + "<== mPlDetails");

					System.out.println(mPlDetails.getPlTagList().size() + "<== mPlDetails taglist size");
					LocalApPurchaseOrderLine apPurchaseOrderLine = apPurchaseOrderLineHome.findByPrimaryKey(mPlDetails.getPlCode());

					//remove all inv tag inside PO line
		  	   	    Collection invTags = apPurchaseOrderLine.getInvTags();

		  	   	    Iterator x = invTags.iterator();

		  	   	    while (x.hasNext()){

		  	   	    	LocalInvTag invTag = (LocalInvTag)x.next();

		  	   	    	x.remove();

		  	   	    	invTag.remove();
		  	   	    }
		  	   	    if (mPlDetails.getTraceMisc() == 1){

		  	   	    	this.createInvTagList(apPurchaseOrderLine,  mPlDetails.getPlTagList(), AD_CMPNY);
		  	   	    	/*
			  	   	    try{
			  	   	    	System.out.println("mgcecreate ng bago?");
			      	  	    System.out.println(mPlDetails + "<== mPlDetails");
			      	  	    System.out.println(mPlDetails.getPlTagList() + "<== mPlDetails.getPlTagList()");
			      	  	    Iterator t = mPlDetails.getPlTagList().iterator();
			      	  	    while (t.hasNext()){
			      	  	    	InvModTagListDetails tgLstDetails = (InvModTagListDetails)t.next();
			      	  	    	System.out.println(tgLstDetails.getTgCustodian() + "<== custodian");
			      	  	    	System.out.println(tgLstDetails.getTgSpecs() + "<== specs");
			      	  	    	System.out.println(tgLstDetails.getTgPropertyCode() + "<== propertyCode");
			      	  	    	System.out.println(tgLstDetails.getTgExpiryDate() + "<== expiryDate");
		     	  	    		System.out.println("ngcreate ng bago?");
		     	  	    	    LocalInvTag invTag = invTagHome.create(tgLstDetails.getTgPropertyCode(),
			      	  	    			tgLstDetails.getTgSerialNumber(),null,tgLstDetails.getTgExpiryDate(),
			      	  	    			tgLstDetails.getTgSpecs(), AD_CMPNY, tgLstDetails.getTgTransactionDate(),
			      	  	    			tgLstDetails.getTgType());

			      	  	    	invTag.setApPurchaseOrderLine(apPurchaseOrderLine);
			      	  	    	LocalAdUser adUser = null;
			      	  	    	try {
			      	  	    		adUser = adUserHome.findByUsrName(tgLstDetails.getTgCustodian(), AD_CMPNY);
			      	  	    	}catch(FinderException ex){

			      	  	    	}
			      	  	    	invTag.setAdUser(adUser);
			      	  	    	System.out.println("ngcreate ba?");

			      	  	    }
			  	   	    }catch(Exception ex){

			  	   	    }
						*/
		  	   	    }
				}

				Collection apPurchaseOrderLines = apPurchaseOrder.getApPurchaseOrderLines();

				Iterator i = apPurchaseOrderLines.iterator();

				while (i.hasNext()) {

					LocalApPurchaseOrderLine apPurchaseOrderLine = (LocalApPurchaseOrderLine)i.next();

					ABS_TOTAL_AMOUNT += apPurchaseOrderLine.getPlAmount();



				}
			}

			LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);

			// set purchase order approval status

			String PO_APPRVL_STATUS = null;

			if(!isDraft) {

				LocalAdApproval adApproval = adApprovalHome.findByAprAdCompany(AD_CMPNY);

				// check if ap voucher approval is enabled

				if (adApproval.getAprEnableApPurchaseOrder() == EJBCommon.FALSE) {

					PO_APPRVL_STATUS = "N/A";

				} else {

					// check if voucher is self approved

					LocalAdAmountLimit adAmountLimit = null;

					try {

						adAmountLimit = adAmountLimitHome.findByAdcTypeAndAuTypeAndUsrName("AP PURCHASE ORDER", "REQUESTER", details.getPoLastModifiedBy(), AD_CMPNY);

					} catch (FinderException ex) {

						throw new GlobalNoApprovalRequesterFoundException();

					}

					if (ABS_TOTAL_AMOUNT <= adAmountLimit.getCalAmountLimit()) {

						PO_APPRVL_STATUS = "N/A";

					} else {

						// for approval, create approval queue

						Collection adAmountLimits = adAmountLimitHome.findByAdcTypeAndGreaterThanCalAmountLimit("AP PURCHASE ORDER", adAmountLimit.getCalAmountLimit(), AD_CMPNY);

						if (adAmountLimits.isEmpty()) {

							Collection adApprovalUsers = adApprovalUserHome.findByAuTypeAndCalCode("APPROVER", adAmountLimit.getCalCode(), AD_CMPNY);

							if (adApprovalUsers.isEmpty()) {

								throw new GlobalNoApprovalApproverFoundException();

							}

							Iterator j = adApprovalUsers.iterator();

							while (j.hasNext()) {

								LocalAdApprovalUser adApprovalUser = (LocalAdApprovalUser)j.next();

								LocalAdApprovalQueue adApprovalQueue = adApprovalQueueHome.create(EJBCommon.TRUE, "AP PURCHASE ORDER", apPurchaseOrder.getPoCode(),
										apPurchaseOrder.getPoDocumentNumber(), apPurchaseOrder.getPoDate(), adAmountLimit.getCalAndOr(), adApprovalUser.getAuOr(), AD_BRNCH, AD_CMPNY);

								adApprovalUser.getAdUser().addAdApprovalQueue(adApprovalQueue);

							}

						} else {

							boolean isApprovalUsersFound = false;

							Iterator i = adAmountLimits.iterator();

							while (i.hasNext()) {

								LocalAdAmountLimit adNextAmountLimit = (LocalAdAmountLimit)i.next();

								if (ABS_TOTAL_AMOUNT <= adNextAmountLimit.getCalAmountLimit()) {

									Collection adApprovalUsers = adApprovalUserHome.findByAuTypeAndCalCode("APPROVER", adAmountLimit.getCalCode(), AD_CMPNY);

									Iterator j = adApprovalUsers.iterator();

									while (j.hasNext()) {

										isApprovalUsersFound = true;

										LocalAdApprovalUser adApprovalUser = (LocalAdApprovalUser)j.next();

										LocalAdApprovalQueue adApprovalQueue = adApprovalQueueHome.create(EJBCommon.TRUE, "AP PURCHASE ORDER", apPurchaseOrder.getPoCode(),
												apPurchaseOrder.getPoDocumentNumber(), apPurchaseOrder.getPoDate(), adAmountLimit.getCalAndOr(), adApprovalUser.getAuOr(), AD_BRNCH, AD_CMPNY);

										adApprovalUser.getAdUser().addAdApprovalQueue(adApprovalQueue);

									}

									break;

								} else if (!i.hasNext()) {

									Collection adApprovalUsers = adApprovalUserHome.findByAuTypeAndCalCode("APPROVER", adNextAmountLimit.getCalCode(), AD_CMPNY);

									Iterator j = adApprovalUsers.iterator();

									while (j.hasNext()) {

										isApprovalUsersFound = true;

										LocalAdApprovalUser adApprovalUser = (LocalAdApprovalUser)j.next();

										LocalAdApprovalQueue adApprovalQueue = adApprovalQueueHome.create(EJBCommon.TRUE, "AP PURCHASE ORDER", apPurchaseOrder.getPoCode(),
												apPurchaseOrder.getPoDocumentNumber(), apPurchaseOrder.getPoDate(), adNextAmountLimit.getCalAndOr(), adApprovalUser.getAuOr(), AD_BRNCH, AD_CMPNY);

										adApprovalUser.getAdUser().addAdApprovalQueue(adApprovalQueue);

									}

									break;

								}

								adAmountLimit = adNextAmountLimit;

							}

							if (!isApprovalUsersFound) {

								throw new GlobalNoApprovalApproverFoundException();

							}

						}

						PO_APPRVL_STATUS = "PENDING";

					}

				}

				apPurchaseOrder.setPoApprovalStatus(PO_APPRVL_STATUS);

				// set post purchase order

				if(PO_APPRVL_STATUS.equals("N/A")) {

					 apPurchaseOrder.setPoPosted(EJBCommon.TRUE);
					 apPurchaseOrder.setPoPosted(EJBCommon.TRUE);
					 apPurchaseOrder.setPoPostedBy(apPurchaseOrder.getPoLastModifiedBy());
					 apPurchaseOrder.setPoDatePosted(EJBCommon.getGcCurrentDateWoTime().getTime());

					 if (!isDraft && !apPurchaseOrder.getPoReferenceNumber().equals("")) {

				       LocalApPurchaseRequisition apPurchaseRequisition = null;
				       try {
				    	   apPurchaseRequisition = apPurchaseRequisitionHome.findByPrNumberAndBrCode(apPurchaseOrder.getPoReferenceNumber(),AD_BRNCH,AD_CMPNY);
				    	   Iterator ilListIter = plList.iterator();

							while (ilListIter.hasNext()) {

								ApModPurchaseOrderLineDetails mdetails = (ApModPurchaseOrderLineDetails)ilListIter.next();

								this.setInvTbForItemForCurrentMonth(mdetails.getPlIiName(), apPurchaseRequisition.getPrCreatedBy(), apPurchaseRequisition.getPrDate(), mdetails.getPlQuantity(), AD_CMPNY);
							}
				       } catch (FinderException ex) {

				       }


				   }
				}

			}

 	  	    return apPurchaseOrder.getPoCode();


		} catch (GlobalRecordAlreadyDeletedException ex) {

			getSessionContext().setRollbackOnly();
			throw ex;

		} catch (GlobalDocumentNumberNotUniqueException ex) {

			getSessionContext().setRollbackOnly();
			throw ex;

		} catch (GlobalConversionDateNotExistException ex) {

			getSessionContext().setRollbackOnly();
			throw ex;

		} catch (GlobalPaymentTermInvalidException ex) {

			getSessionContext().setRollbackOnly();
			throw ex;

		} catch (GlobalTransactionAlreadyApprovedException ex) {

			getSessionContext().setRollbackOnly();
			throw ex;

		} catch (GlobalTransactionAlreadyPendingException ex) {

			getSessionContext().setRollbackOnly();
			throw ex;

		} catch (GlobalTransactionAlreadyPostedException ex) {

			getSessionContext().setRollbackOnly();
			throw ex;

		} catch (GlobalTransactionAlreadyVoidException ex) {

			getSessionContext().setRollbackOnly();
			throw ex;

		} catch (GlobalInvItemLocationNotFoundException ex) {

			getSessionContext().setRollbackOnly();
			throw ex;

		} catch (GlobalNoApprovalRequesterFoundException ex) {

			getSessionContext().setRollbackOnly();
			throw ex;

		} catch (GlobalNoApprovalApproverFoundException ex) {

			getSessionContext().setRollbackOnly();
			throw ex;

		} catch (GlobalSupplierItemInvalidException ex) {

			getSessionContext().setRollbackOnly();
			throw ex;
		 }catch (GlobalRecordInvalidException ex) {

			getSessionContext().setRollbackOnly();
			throw ex;

		}  catch (Exception ex) {

			Debug.printStackTrace(ex);
			getSessionContext().setRollbackOnly();
			throw new EJBException(ex.getMessage());

		}

	}

	/**
	 * @ejb:interface-method view-type="remote"
	 **/
	public void deleteApPoEntry(Integer PO_CODE, String AD_USR, Integer AD_CMPNY) throws
	GlobalRecordAlreadyDeletedException {

		Debug.print("ApPurchaseOrderEntryControllerBean deleteApPoEntry");

		LocalApPurchaseOrderHome apPurchaseOrderHome = null;
		LocalAdApprovalQueueHome adApprovalQueueHome = null;
		LocalAdDeleteAuditTrailHome adDeleteAuditTrailHome = null;

		// Initialize EJB Home

		try {

			apPurchaseOrderHome = (LocalApPurchaseOrderHome)EJBHomeFactory.
			lookUpLocalHome(LocalApPurchaseOrderHome.JNDI_NAME, LocalApPurchaseOrderHome.class);
			adApprovalQueueHome = (LocalAdApprovalQueueHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdApprovalQueueHome.JNDI_NAME, LocalAdApprovalQueueHome.class);
			adDeleteAuditTrailHome = (LocalAdDeleteAuditTrailHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdDeleteAuditTrailHome.JNDI_NAME, LocalAdDeleteAuditTrailHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}

		try {

			LocalApPurchaseOrder apPurchaseOrder = apPurchaseOrderHome.findByPrimaryKey(PO_CODE);

            if (apPurchaseOrder.getPoApprovalStatus() != null && apPurchaseOrder.getPoApprovalStatus().equals("PENDING")) {

                Collection adApprovalQueues = adApprovalQueueHome.findByAqDocumentAndAqDocumentCode("AP PURCHASE ORDER", apPurchaseOrder.getPoCode(), AD_CMPNY);

                Iterator i = adApprovalQueues.iterator();

                while(i.hasNext()) {

                    LocalAdApprovalQueue adApprovalQueue = (LocalAdApprovalQueue)i.next();

                    adApprovalQueue.remove();
                }
            }

			adDeleteAuditTrailHome.create("AP PURCHASE ORDER", apPurchaseOrder.getPoDate(), apPurchaseOrder.getPoDocumentNumber(), apPurchaseOrder.getPoReferenceNumber(),
 					0d, AD_USR, new Date(), AD_CMPNY);

			apPurchaseOrder.remove();

		} catch (FinderException ex) {

			getSessionContext().setRollbackOnly();
			throw new GlobalRecordAlreadyDeletedException();

		} catch (Exception ex) {

			Debug.printStackTrace(ex);
			getSessionContext().setRollbackOnly();
			throw new EJBException(ex.getMessage());

		}

	}

	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public ApModSupplierDetails getApSplBySplSupplierCode(String SPL_SPPLR_CODE, Integer AD_CMPNY)
	throws GlobalNoRecordFoundException {

		Debug.print("ApPurchaseOrderEntryControllerBean getApSplBySplSupplierCode");

		LocalApSupplierHome apSupplierHome = null;

		// Initialize EJB Home

		try {

			apSupplierHome = (LocalApSupplierHome)EJBHomeFactory.
			lookUpLocalHome(LocalApSupplierHome.JNDI_NAME, LocalApSupplierHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}

		try {

			LocalApSupplier apSupplier = null;


			try {

				apSupplier = apSupplierHome.findBySplSupplierCode(SPL_SPPLR_CODE, AD_CMPNY);

			} catch (FinderException ex) {

				throw new GlobalNoRecordFoundException();

			}

			ApModSupplierDetails mdetails = new ApModSupplierDetails();

			mdetails.setSplPytName(apSupplier.getAdPaymentTerm() != null ?
					apSupplier.getAdPaymentTerm().getPytName() : null);
			mdetails.setSplScWtcName(apSupplier.getApSupplierClass().getApWithholdingTaxCode() != null ?
					apSupplier.getApSupplierClass().getApWithholdingTaxCode().getWtcName() : null);
			mdetails.setSplName(apSupplier.getSplName());

        	if (apSupplier.getApSupplierClass().getApTaxCode() != null ){

        		mdetails.setSplScTcName( apSupplier.getApSupplierClass().getApTaxCode().getTcName());
        		mdetails.setSplScTcType( apSupplier.getApSupplierClass().getApTaxCode().getTcType());
        		mdetails.setSplScTcRate( apSupplier.getApSupplierClass().getApTaxCode().getTcRate());

        	}

			return mdetails;

		} catch (GlobalNoRecordFoundException ex) {

			throw ex;

		} catch (Exception ex) {

			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());

		}

	}

	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public ArrayList getInvUomByIiName(String II_NM, Integer AD_CMPNY) {

		Debug.print("ApPurchaseOrderEntryControllerBean getInvUomByIiName");

		LocalInvUnitOfMeasureHome invUnitOfMeasureHome = null;
		LocalInvUnitOfMeasureConversionHome invUnitOfMeasureConversionHome = null;
		LocalInvItemHome invItemHome = null;
		ArrayList list = new ArrayList();

		// Initialize EJB Home

		try {

			invUnitOfMeasureHome = (LocalInvUnitOfMeasureHome)EJBHomeFactory.
				lookUpLocalHome(LocalInvUnitOfMeasureHome.JNDI_NAME, LocalInvUnitOfMeasureHome.class);
			invItemHome = (LocalInvItemHome)EJBHomeFactory.
				lookUpLocalHome(LocalInvItemHome.JNDI_NAME, LocalInvItemHome.class);
			invUnitOfMeasureConversionHome = (LocalInvUnitOfMeasureConversionHome)EJBHomeFactory.
    			lookUpLocalHome(LocalInvUnitOfMeasureConversionHome.JNDI_NAME, LocalInvUnitOfMeasureConversionHome.class);
		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());
		}

		try {

			LocalInvItem invItem = null;
			LocalInvUnitOfMeasure invItemUnitOfMeasure = null;

			invItem = invItemHome.findByIiName(II_NM, AD_CMPNY);
			invItemUnitOfMeasure = invItem.getInvUnitOfMeasure();

			Collection invUnitOfMeasures = null;
			Iterator i = invUnitOfMeasureHome.findByUomAdLvClass(
					invItemUnitOfMeasure.getUomAdLvClass(), AD_CMPNY).iterator();
			//TODO
			while (i.hasNext()) {

				LocalInvUnitOfMeasure invUnitOfMeasure = (LocalInvUnitOfMeasure) i.next();
				InvModUnitOfMeasureDetails details = new InvModUnitOfMeasureDetails();
				details.setUomName(invUnitOfMeasure.getUomName());

				if (invUnitOfMeasure.getUomName().equals(invItemUnitOfMeasure.getUomName())) {

					details.setDefault(true);

				}

				list.add(details);

			}

			return list;

		} catch (Exception ex) {

			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());

		}

	}

	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public double getInvIiUnitCostByIiNameAndUomName(String II_NM, String UOM_NM, Integer AD_CMPNY) {

		Debug.print("ApPurchaseOrderEntryControllerBean getInvIiUnitCostByIiNameAndUomName");

		LocalInvItemHome invItemHome = null;
		LocalInvUnitOfMeasureConversionHome invUnitOfMeasureConversionHome = null;

		// Initialize EJB Home

		try {

			invItemHome = (LocalInvItemHome)EJBHomeFactory.
				lookUpLocalHome(LocalInvItemHome.JNDI_NAME, LocalInvItemHome.class);
			invUnitOfMeasureConversionHome = (LocalInvUnitOfMeasureConversionHome)EJBHomeFactory.
    			lookUpLocalHome(LocalInvUnitOfMeasureConversionHome.JNDI_NAME, LocalInvUnitOfMeasureConversionHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}

		try {


			LocalInvItem invItem = invItemHome.findByIiName(II_NM, AD_CMPNY);

			LocalInvUnitOfMeasureConversion invUnitOfMeasureConversion = invUnitOfMeasureConversionHome.findUmcByIiNameAndUomName(II_NM, UOM_NM, AD_CMPNY);
        	LocalInvUnitOfMeasureConversion invDefaultUomConversion = invUnitOfMeasureConversionHome.findUmcByIiNameAndUomName(II_NM, invItem.getInvUnitOfMeasure().getUomName(), AD_CMPNY);

        	return EJBCommon.roundIt(invItem.getIiUnitCost() * invDefaultUomConversion.getUmcConversionFactor() / invUnitOfMeasureConversion.getUmcConversionFactor(), this.getInvGpCostPrecisionUnit(AD_CMPNY));

		} catch (Exception ex) {

			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());

		}

	}

	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public byte getAdPrfApUseSupplierPulldown(Integer AD_CMPNY) {

		Debug.print("ApPurchaseOrderEntryControllerBean getAdPrfApUseSupplierPulldown");

		LocalAdPreferenceHome adPreferenceHome = null;

		// Initialize EJB Home

		try {

			adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}

		try {

			LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);

			return  adPreference.getPrfApUseSupplierPulldown();

		} catch (Exception ex) {

			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());

		}

	}

	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public ArrayList getAdApprovalNotifiedUsersByPoCode(Integer PO_CODE, Integer AD_CMPNY) {

		Debug.print("ApPurchaseOrderEntryControllerBean getAdApprovalNotifiedUsersByVouCode");

		LocalAdApprovalQueueHome adApprovalQueueHome = null;
		LocalApPurchaseOrderHome apPurchaseOrderHome = null;

		ArrayList list = new ArrayList();


		// Initialize EJB Home

		try {

			adApprovalQueueHome = (LocalAdApprovalQueueHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdApprovalQueueHome.JNDI_NAME, LocalAdApprovalQueueHome.class);
			apPurchaseOrderHome = (LocalApPurchaseOrderHome)EJBHomeFactory.
			lookUpLocalHome(LocalApPurchaseOrderHome.JNDI_NAME, LocalApPurchaseOrderHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}

		try {

			LocalApPurchaseOrder apPurchaseOrder = apPurchaseOrderHome.findByPrimaryKey(PO_CODE);

			if (apPurchaseOrder.getPoPosted() == EJBCommon.TRUE) {

				list.add("DOCUMENT POSTED");
				return list;

			}

			Collection adApprovalQueues = adApprovalQueueHome.findByAqDocumentAndAqDocumentCode("AP PURCHASE ORDER", PO_CODE, AD_CMPNY);

			Iterator i = adApprovalQueues.iterator();

			while(i.hasNext()) {

				LocalAdApprovalQueue adApprovalQueue = (LocalAdApprovalQueue)i.next();

				list.add(adApprovalQueue.getAdUser().getUsrDescription());

			}

			return list;

		} catch (Exception ex) {

			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());

		}

	}

 	/**
 	 * @ejb:interface-method view-type="remote"
 	 * @jboss:method-attributes read-only="true"
 	 **/
 	public double getFrRateByFrNameAndFrDate(String FC_NM, Date CONVERSION_DATE, Integer AD_CMPNY)
 	throws GlobalConversionDateNotExistException {

 		Debug.print("GlJournalEntryControllerBean getFrRateByFrNameAndFrDate");

 		LocalGlFunctionalCurrencyRateHome glFunctionalCurrencyRateHome = null;
 		LocalGlFunctionalCurrencyHome glFunctionalCurrencyHome = null;
 		LocalAdCompanyHome adCompanyHome = null;

 		// Initialize EJB Home

 		try {

 			glFunctionalCurrencyRateHome = (LocalGlFunctionalCurrencyRateHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlFunctionalCurrencyRateHome.JNDI_NAME, LocalGlFunctionalCurrencyRateHome.class);
 			glFunctionalCurrencyHome = (LocalGlFunctionalCurrencyHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlFunctionalCurrencyHome.JNDI_NAME, LocalGlFunctionalCurrencyHome.class);
 			adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);

 		} catch (NamingException ex) {

 			throw new EJBException(ex.getMessage());

 		}

 		try {

 			LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
 			LocalGlFunctionalCurrency glFunctionalCurrency = glFunctionalCurrencyHome.findByFcName(FC_NM, AD_CMPNY);

 			double CONVERSION_RATE = 1;

 			// Get functional currency rate

 			System.out.println("FC_NM="+FC_NM);
 			System.out.println("CONVERSION_DATE="+CONVERSION_DATE);

 			if (!FC_NM.equals("USD")) {
 				System.out.println("1-------------------------------->");
 				LocalGlFunctionalCurrencyRate glFunctionalCurrencyRate =
 					glFunctionalCurrencyRateHome.findByFcCodeAndDate(glFunctionalCurrency.getFcCode(),
 							CONVERSION_DATE, AD_CMPNY);

 				CONVERSION_RATE = glFunctionalCurrencyRate.getFrXToUsd();

 			}

 			// Get set of book functional currency rate if necessary
 			System.out.println("2nd curr: " + adCompany.getGlFunctionalCurrency().getFcName());
 			if (!adCompany.getGlFunctionalCurrency().getFcName().equals("USD")) {
 				System.out.println("2-------------------------------->");
 				LocalGlFunctionalCurrencyRate glCompanyFunctionalCurrencyRate =
 					glFunctionalCurrencyRateHome.findByFcCodeAndDate(adCompany.getGlFunctionalCurrency(). getFcCode(),
 							CONVERSION_DATE, AD_CMPNY);

 				CONVERSION_RATE = CONVERSION_RATE / glCompanyFunctionalCurrencyRate.getFrXToUsd();

 			}

 			return CONVERSION_RATE;

 		} catch (FinderException ex) {

 			getSessionContext().setRollbackOnly();
 			throw new GlobalConversionDateNotExistException();

 		} catch (Exception ex) {

 			Debug.printStackTrace(ex);
 			throw new EJBException(ex.getMessage());

 		}

 	}


        /**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public byte getAdPrfEnableApPOBatch(Integer AD_CMPNY) {

		Debug.print("ApDebitMemoEntryControllerBean getAdPrfEnableApPOBatch");

		LocalAdPreferenceHome adPreferenceHome = null;


		// Initialize EJB Home

		try {

			adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}


		try {

			LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);

			return adPreference.getPrfEnableApPOBatch();

		} catch (Exception ex) {

			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());

		}

	}


        /**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public ArrayList getApOpenVbAll(String DPRTMNT, Integer AD_BRNCH, Integer AD_CMPNY) {

		Debug.print("ApPurchaseOrderEntryControllerBean getApOpenVbAll");

		LocalApVoucherBatchHome apVoucherBatchHome = null;

		ArrayList list = new ArrayList();

		// Initialize EJB Home

		try {

			apVoucherBatchHome = (LocalApVoucherBatchHome)EJBHomeFactory.
			lookUpLocalHome(LocalApVoucherBatchHome.JNDI_NAME, LocalApVoucherBatchHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}

		try {
			Collection apVoucherBatches = null;

			if(DPRTMNT.equals("") || DPRTMNT.equals("default") || DPRTMNT.equals("NO RECORD FOUND")){
				System.out.println("------------>");
				apVoucherBatches = apVoucherBatchHome.findOpenVbByVbType("PURCHASE ORDER", AD_BRNCH, AD_CMPNY);

			} else {
				System.out.println("------------>else");
				apVoucherBatches = apVoucherBatchHome.findOpenVbByVbTypeDepartment("PURCHASE ORDER", DPRTMNT, AD_BRNCH, AD_CMPNY);

			}

			Iterator i = apVoucherBatches.iterator();

			while (i.hasNext()) {

				LocalApVoucherBatch apVoucherBatch = (LocalApVoucherBatch)i.next();

				list.add(apVoucherBatch.getVbName());

			}

			return list;

		} catch (Exception ex) {

			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());

		}

	}

	// private methods

	private double convertForeignToFunctionalCurrency(Integer FC_CODE, String FC_NM,
			Date CONVERSION_DATE, double CONVERSION_RATE, double AMOUNT, Integer AD_CMPNY) {

		Debug.print("ApPurchaseOrderEntryControllerBean convertForeignToFunctionalCurrency");


		LocalGlFunctionalCurrencyRateHome glFunctionalCurrencyRateHome = null;
		LocalAdCompanyHome adCompanyHome = null;

		LocalAdCompany adCompany = null;

		// Initialize EJB Homes

		try {

			glFunctionalCurrencyRateHome = (LocalGlFunctionalCurrencyRateHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlFunctionalCurrencyRateHome.JNDI_NAME, LocalGlFunctionalCurrencyRateHome.class);
			adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}

		// get company and extended precision

		try {

			adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);

		} catch (Exception ex) {

			throw new EJBException(ex.getMessage());

		}


		// Convert to functional currency if necessary

		if (CONVERSION_RATE != 1 && CONVERSION_RATE != 0 ) {

			AMOUNT = AMOUNT / CONVERSION_RATE;

		}

		return EJBCommon.roundIt(AMOUNT, this.getInvGpCostPrecisionUnit(AD_CMPNY));

	}

	private void postToGl(LocalGlAccountingCalendarValue glAccountingCalendarValue,
			LocalGlChartOfAccount glChartOfAccount,
			boolean isCurrentAcv, byte isDebit, double JL_AMNT, Integer AD_CMPNY) {

		Debug.print("ApPurchaseOrderEntryControllerBean postToGl");

		LocalGlChartOfAccountBalanceHome glChartOfAccountBalanceHome = null;
		LocalAdCompanyHome adCompanyHome = null;


		// Initialize EJB Home

		try {

			glChartOfAccountBalanceHome = (LocalGlChartOfAccountBalanceHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlChartOfAccountBalanceHome.JNDI_NAME, LocalGlChartOfAccountBalanceHome.class);
			adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);


		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}

		try {

			LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);

			LocalGlChartOfAccountBalance glChartOfAccountBalance =
				glChartOfAccountBalanceHome.findByAcvCodeAndCoaCode(
						glAccountingCalendarValue.getAcvCode(),
						glChartOfAccount.getCoaCode(), AD_CMPNY);

			String ACCOUNT_TYPE = glChartOfAccount.getCoaAccountType();
			short FC_EXTNDD_PRCSN = adCompany.getGlFunctionalCurrency().getFcExtendedPrecision();



			if (((ACCOUNT_TYPE.equals("ASSET") || ACCOUNT_TYPE.equals("EXPENSE")) &&
					isDebit == EJBCommon.TRUE) ||
					(!ACCOUNT_TYPE.equals("ASSET") && !ACCOUNT_TYPE.equals("EXPENSE") &&
							isDebit == EJBCommon.FALSE)) {

				glChartOfAccountBalance.setCoabEndingBalance(
						EJBCommon.roundIt(glChartOfAccountBalance.getCoabEndingBalance() + JL_AMNT, FC_EXTNDD_PRCSN));

				if (!isCurrentAcv) {

					glChartOfAccountBalance.setCoabBeginningBalance(
							EJBCommon.roundIt(glChartOfAccountBalance.getCoabBeginningBalance() + JL_AMNT, FC_EXTNDD_PRCSN));

				}


			} else {

				glChartOfAccountBalance.setCoabEndingBalance(
						EJBCommon.roundIt(glChartOfAccountBalance.getCoabEndingBalance() - JL_AMNT, FC_EXTNDD_PRCSN));

				if (!isCurrentAcv) {

					glChartOfAccountBalance.setCoabBeginningBalance(
							EJBCommon.roundIt(glChartOfAccountBalance.getCoabBeginningBalance() - JL_AMNT, FC_EXTNDD_PRCSN));

				}

			}

			if (isCurrentAcv) {

				if (isDebit == EJBCommon.TRUE) {

					glChartOfAccountBalance.setCoabTotalDebit(
							EJBCommon.roundIt(glChartOfAccountBalance.getCoabTotalDebit() + JL_AMNT, FC_EXTNDD_PRCSN));

				} else {

					glChartOfAccountBalance.setCoabTotalCredit(
							EJBCommon.roundIt(glChartOfAccountBalance.getCoabTotalCredit() + JL_AMNT, FC_EXTNDD_PRCSN));
				}

			}

		} catch (Exception ex) {

			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());

		}


	}

	private double convertByUomFromAndItemAndQuantity(LocalInvUnitOfMeasure invFromUnitOfMeasure, LocalInvItem invItem, double QTY_RCVD, Integer AD_CMPNY) {

		Debug.print("ApPurchaseOrderEntryControllerBean convertByUomFromAndUomToAndQuantity");

		LocalAdPreferenceHome adPreferenceHome = null;
		LocalInvUnitOfMeasureConversionHome invUnitOfMeasureConversionHome = null;

		// Initialize EJB Home

		try {

			adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);
			invUnitOfMeasureConversionHome = (LocalInvUnitOfMeasureConversionHome)EJBHomeFactory.
    			lookUpLocalHome(LocalInvUnitOfMeasureConversionHome.JNDI_NAME, LocalInvUnitOfMeasureConversionHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}

		try {

			LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);

			LocalInvUnitOfMeasureConversion invUnitOfMeasureConversion = invUnitOfMeasureConversionHome.findUmcByIiNameAndUomName(invItem.getIiName(), invFromUnitOfMeasure.getUomName(), AD_CMPNY);
            LocalInvUnitOfMeasureConversion invDefaultUomConversion = invUnitOfMeasureConversionHome.findUmcByIiNameAndUomName(invItem.getIiName(), invItem.getInvUnitOfMeasure().getUomName(), AD_CMPNY);

        	return EJBCommon.roundIt(QTY_RCVD * invDefaultUomConversion.getUmcConversionFactor() / invUnitOfMeasureConversion.getUmcConversionFactor(), adPreference.getPrfInvQuantityPrecisionUnit());


		} catch (Exception ex) {

			Debug.printStackTrace(ex);
			getSessionContext().setRollbackOnly();
			throw new EJBException(ex.getMessage());

		}

	}



	private void createInvTagList(LocalApPurchaseOrderLine apPurchaseOrderLine, ArrayList list, Integer AD_CMPNY) throws Exception {

    	Debug.print("ApPurchaseOrderEntryControllerBean createInvTagList");

    	LocalAdUserHome adUserHome = null;
    	LocalInvTagHome invTagHome = null;

    	// Initialize EJB Home

    	try{
    		adUserHome = (LocalAdUserHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdUserHome.JNDI_NAME, LocalAdUserHome.class);
    		invTagHome = (LocalInvTagHome)EJBHomeFactory.
			lookUpLocalHome(LocalInvTagHome.JNDI_NAME, LocalInvTagHome.class);

    	} catch (NamingException ex) {

    		throw new EJBException(ex.getMessage());

    	}



    	try {
			System.out.println("aabot?");
	  	    	//Iterator t = apPurchaseOrderLine.getInvTag().iterator();
  	  	    Iterator t = list.iterator();

  	  	    LocalInvTag invTag  = null;
  	  	    System.out.println("umabot?");
  	  	    while (t.hasNext()){
  	  	    	InvModTagListDetails tgLstDetails = (InvModTagListDetails)t.next();
  	  	    	System.out.println(tgLstDetails.getTgCustodian() + "<== custodian");
  	  	    	System.out.println(tgLstDetails.getTgSpecs() + "<== specs");
  	  	    	System.out.println(tgLstDetails.getTgPropertyCode() + "<== propertyCode");
  	  	    	System.out.println(tgLstDetails.getTgExpiryDate() + "<== expiryDate");
  	  	    	System.out.println(tgLstDetails.getTgSerialNumber() + "<== serial number");

  	  	    	if (tgLstDetails.getTgCode()==null){
  	  	    		System.out.println("ngcreate ba?");
      	  	    	invTag = invTagHome.create(tgLstDetails.getTgPropertyCode(),
      	  	    			tgLstDetails.getTgSerialNumber(),null,tgLstDetails.getTgExpiryDate(),
      	  	    			tgLstDetails.getTgSpecs(), AD_CMPNY, tgLstDetails.getTgTransactionDate(),
      	  	    			tgLstDetails.getTgType());

      	  	    	invTag.setApPurchaseOrderLine(apPurchaseOrderLine);
      	  	    	invTag.setInvItemLocation(apPurchaseOrderLine.getInvItemLocation());
      	  	    	LocalAdUser adUser = null;
      	  	    	try {
      	  	    		adUser = adUserHome.findByUsrName(tgLstDetails.getTgCustodian(), AD_CMPNY);
      	  	    	}catch(FinderException ex){

      	  	    	}
      	  	    	invTag.setAdUser(adUser);
      	  	    	System.out.println("ngcreate ba?");
  	  	    	}

  	  	    }




		}catch(Exception ex) {
			throw ex;
		}
    }


    private ArrayList getInvTagList(LocalApPurchaseOrderLine apPurchaseOrderLine) {

    	ArrayList list = new ArrayList();

    	Collection invTags = apPurchaseOrderLine.getInvTags();
		Iterator x = invTags.iterator();
		while (x.hasNext()) {
			LocalInvTag invTag = (LocalInvTag) x.next();
			InvModTagListDetails tgLstDetails = new InvModTagListDetails();
			tgLstDetails.setTgPropertyCode(invTag.getTgPropertyCode());
			tgLstDetails.setTgSpecs(invTag.getTgSpecs());
			tgLstDetails.setTgExpiryDate(invTag.getTgExpiryDate());
			tgLstDetails.setTgSerialNumber(invTag.getTgSerialNumber());
			try{

				tgLstDetails.setTgCustodian(invTag.getAdUser().getUsrName());
			}
			catch(Exception ex){
				tgLstDetails.setTgCustodian("");
			}

			list.add(tgLstDetails);

			System.out.println(tgLstDetails.getTgPropertyCode() + "<== property code inside controllerbean ");
			System.out.println(tgLstDetails.getTgSpecs() + "<== specs inside controllerbean ");
			System.out.println(list+ "<== taglist inside controllerbean ");

		}

    	return list;

    }






	// SessionBean methods

	/**
	 * @ejb:create-method view-type="remote"
	 **/
	public void ejbCreate() throws CreateException {

		Debug.print("ApPurchaseOrderEntryControllerBean ejbCreate");

	}

}