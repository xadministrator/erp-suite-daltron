/*
 * ApGeneratePurchaseRequisitionControllerBean.java
 *
 * Created on April 18, 2006 1:00 PM
 *
 * @author  Aliza D.J. Anos
 */

package com.ejb.txn;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;
import javax.naming.NamingException;

import org.joda.time.DateTime;
import org.joda.time.LocalDateTime;

import java.util.Properties;
import java.util.Set;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import com.ejb.ad.LocalAdAmountLimit;
import com.ejb.ad.LocalAdAmountLimitHome;
import com.ejb.ad.LocalAdApprovalUserHome;
import com.ejb.ad.LocalAdLookUpValue;
import com.ejb.ad.LocalAdLookUpValueHome;
import com.ejb.ad.LocalAdPaymentTermHome;
import com.ejb.ad.LocalAdUser;
import com.ejb.ad.LocalAdUserHome;
import com.ejb.ad.LocalAdApproval;
import com.ejb.ad.LocalAdApprovalHome;
import com.ejb.ad.LocalAdApprovalUser;
import com.ejb.ad.LocalAdApprovalQueue;
import com.ejb.ad.LocalAdApprovalQueueHome;
import com.ejb.ad.LocalAdBranchDocumentSequenceAssignment;
import com.ejb.ad.LocalAdBranchDocumentSequenceAssignmentHome;
import com.ejb.ad.LocalAdCompany;
import com.ejb.ad.LocalAdCompanyHome;
import com.ejb.ad.LocalAdBranch;
import com.ejb.ad.LocalAdBranchHome;
import com.ejb.ad.LocalAdDeleteAuditTrailHome;
import com.ejb.ad.LocalAdPaymentTerm;
import com.ejb.ad.LocalAdPreference;
import com.ejb.ad.LocalAdPreferenceHome;
import com.ejb.ap.LocalApCanvass;
import com.ejb.ap.LocalApCanvassHome;
import com.ejb.ap.LocalApPurchaseOrder;
import com.ejb.ap.LocalApPurchaseOrderHome;
import com.ejb.ap.LocalApPurchaseOrderLine;
import com.ejb.ap.LocalApPurchaseOrderLineHome;
import com.ejb.ap.LocalApPurchaseRequisition;
import com.ejb.ap.LocalApPurchaseRequisitionHome;
import com.ejb.ap.LocalApPurchaseRequisitionLine;
import com.ejb.ap.LocalApPurchaseRequisitionLineHome;
import com.ejb.ap.LocalApSupplier;
import com.ejb.ap.LocalApSupplierHome;
import com.ejb.ap.LocalApTaxCode;
import com.ejb.ap.LocalApTaxCodeHome;
import com.ejb.exception.GlobalConversionDateNotExistException;
import com.ejb.exception.GlobalDocumentNumberNotUniqueException;
import com.ejb.exception.GlobalInsufficientBudgetQuantityException;
import com.ejb.exception.GlobalInvItemLocationNotFoundException;
import com.ejb.exception.GlobalNoApprovalApproverFoundException;
import com.ejb.exception.GlobalNoApprovalRequesterFoundException;
import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.exception.GlobalPaymentTermInvalidException;
import com.ejb.exception.GlobalRecordAlreadyDeletedException;
import com.ejb.exception.GlobalSupplierItemInvalidException;
import com.ejb.exception.GlobalTransactionAlreadyApprovedException;
import com.ejb.exception.GlobalTransactionAlreadyPendingException;
import com.ejb.exception.GlobalTransactionAlreadyPostedException;
import com.ejb.exception.GlobalTransactionAlreadyVoidException;
import com.ejb.exception.GlobalRecordInvalidException;
import com.ejb.exception.GlobalSendEmailMessageException;
import com.ejb.gl.LocalAdDocumentSequenceAssignment;
import com.ejb.gl.LocalAdDocumentSequenceAssignmentHome;
import com.ejb.gl.LocalGlFunctionalCurrency;
import com.ejb.gl.LocalGlFunctionalCurrencyHome;
import com.ejb.gl.LocalGlFunctionalCurrencyRate;
import com.ejb.gl.LocalGlFunctionalCurrencyRateHome;
import com.ejb.inv.LocalInvCosting;
import com.ejb.inv.LocalInvCostingHome;
import com.ejb.inv.LocalInvItem;
import com.ejb.inv.LocalInvItemHome;
import com.ejb.inv.LocalInvItemLocation;
import com.ejb.inv.LocalInvItemLocationHome;
import com.ejb.inv.LocalInvLocation;
import com.ejb.inv.LocalInvLocationHome;
import com.ejb.inv.LocalInvTag;
import com.ejb.inv.LocalInvTagHome;
import com.ejb.inv.LocalInvTransactionalBudget;
import com.ejb.inv.LocalInvTransactionalBudgetHome;
import com.ejb.inv.LocalInvUnitOfMeasure;
import com.ejb.inv.LocalInvUnitOfMeasureConversion;
import com.ejb.inv.LocalInvUnitOfMeasureConversionHome;
import com.ejb.inv.LocalInvUnitOfMeasureHome;
import com.util.AbstractSessionBean;
import com.util.AdApprovalQueueDetails;
import com.util.AdModApprovalQueueDetails;
import com.util.ApModCanvassDetails;
import com.util.ApModPurchaseOrderLineDetails;
import com.util.ApModPurchaseRequisitionDetails;
import com.util.ApModPurchaseRequisitionLineDetails;
import com.util.ApModSupplierDetails;
import com.util.ApPurchaseRequisitionDetails;
import com.util.Debug;
import com.util.EJBCommon;
import com.util.EJBHomeFactory;
import com.util.GlModFunctionalCurrencyDetails;
import com.util.GlTaxInterfaceDetails;
import com.util.InvModTagListDetails;
import com.util.InvModUnitOfMeasureDetails;



/**
 * @ejb:bean name="ApPurchaseRequisitionEntryControllerEJB"
 *           display-name="used for entering purchase requisitions"
 *           type="Stateless"
 *           view-type="remote"
 *           jndi-name="ejb/ApPurchaseRequisitionEntryControllerEJB"
 *
 * @ejb:interface remote-class="com.ejb.txn.ApPurchaseRequisitionEntryController"
 *                extends="javax.ejb.EJBObject"
 *
 * @ejb:home remote-class="com.ejb.txn.ApPurchaseRequisitionEntryControllerHome"
 *           extends="javax.ejb.EJBHome"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="apuser"
 *                        role-link="apuserlink"
 *
 * @ejb:permission role-name="apuser"
 *
 */

public class ApPurchaseRequisitionEntryControllerBean extends AbstractSessionBean {

	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public ArrayList getApTcAll(Integer AD_CMPNY) {

		Debug.print("ApPurchaseRequisitionEntryControllerBean getApTcAll");

		LocalApTaxCodeHome apTaxCodeHome = null;

		ArrayList list = new ArrayList();

		// Initialize EJB Home

		try {

			apTaxCodeHome = (LocalApTaxCodeHome)EJBHomeFactory.
				lookUpLocalHome(LocalApTaxCodeHome.JNDI_NAME, LocalApTaxCodeHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}

		try {

			Collection apTaxCodes = apTaxCodeHome.findEnabledTcAll(AD_CMPNY);

			Iterator i = apTaxCodes.iterator();

			while (i.hasNext()) {

				LocalApTaxCode apTaxCode = (LocalApTaxCode)i.next();

				list.add(apTaxCode.getTcName());

			}

			return list;

		} catch (Exception ex) {

			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());

		}

	}

	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public ArrayList getAdPrType(Integer AD_CMPNY) {

		Debug.print("ApPurchaseRequisitionEntryControllerBean getAdPrType");

		LocalAdLookUpValueHome adLookUpValueHome = null;

		ArrayList list = new ArrayList();

		// Initialize EJB Home

		try {

			adLookUpValueHome = (LocalAdLookUpValueHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdLookUpValueHome.JNDI_NAME, LocalAdLookUpValueHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}

		try {

			Collection adLookUpValues = adLookUpValueHome.findByLuName("AP PR TYPE", AD_CMPNY);

			Iterator i = adLookUpValues.iterator();

			while (i.hasNext()) {

				LocalAdLookUpValue adLookUpValue = (LocalAdLookUpValue)i.next();

				list.add(adLookUpValue.getLvName());

			}

			return list;

		} catch (Exception ex) {

			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());

		}

	}
	/**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public boolean getInvTraceMisc(String II_NAME, Integer AD_CMPNY) {

        Debug.print("InvAdjustmentEntryControllerBean getInvLocAll");

        LocalInvLocationHome invLocationHome = null;
        LocalInvItemHome invItemHome = null;
        Collection invLocations = null;
        ArrayList list = new ArrayList();
        boolean isTraceMisc = false;
        // Initialize EJB Home

        try {

        	invLocationHome = (LocalInvLocationHome)EJBHomeFactory.
                lookUpLocalHome(LocalInvLocationHome.JNDI_NAME, LocalInvLocationHome.class);
        	invItemHome = (LocalInvItemHome)EJBHomeFactory.
                    lookUpLocalHome(LocalInvItemHome.JNDI_NAME, LocalInvItemHome.class);

        } catch (NamingException ex) {

            throw new EJBException(ex.getMessage());

        }

        try {

            LocalInvItem invItem = invItemHome.findByIiName(II_NAME, AD_CMPNY);
            if (invItem.getIiTraceMisc() == 1){
            	isTraceMisc = true;
            }
            return isTraceMisc;
    	} catch (Exception ex) {

    		Debug.printStackTrace(ex);
    		throw new EJBException(ex.getMessage());

    	}

    }
	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public ArrayList getInvTbAllByItemName(String itemName) {

		Debug.print("ApPurchaseRequisitionEntryControllerBean getInvTbAllByItemName");

		LocalInvTransactionalBudgetHome invTransactionalBudgetHome = null;
		Collection invTransactionalBudgets = null;

		ArrayList list = new ArrayList();

		// Initialize EJB Home

		try {

			invTransactionalBudgetHome = (LocalInvTransactionalBudgetHome)EJBHomeFactory.
				lookUpLocalHome(LocalInvTransactionalBudgetHome.JNDI_NAME, LocalInvTransactionalBudgetHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}

		try {
			invTransactionalBudgets = invTransactionalBudgetHome.findByTbItemNameAll(itemName);
			if (invTransactionalBudgets.isEmpty()){
				return null;
			}


			Iterator i = invTransactionalBudgets.iterator();

			while (i.hasNext()) {

				LocalInvTransactionalBudget invTransactionalBudget = (LocalInvTransactionalBudget)i.next();

				list.add(invTransactionalBudget.getTbName());

			}

			return list;

		} catch (Exception ex) {

		}
		return list;
	}

	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/

	public double getInvTbForItemForCurrentMonth(String itemName, String userName, Integer AD_CMPNY)
	throws GlobalNoRecordFoundException
	{

		Debug.print("ApPurchaseRequisitionEntryControllerBean getInvTrnsctnlBdgtForCurrentMonth");
		//TODO: get Transactional Budget for item for current month
		LocalInvTransactionalBudgetHome invTransactionalBudgetHome = null;
		LocalInvItemHome invItemHome = null;
		LocalAdUserHome adUserHome = null;
		LocalAdLookUpValueHome adLookUpValueHome = null;
		ArrayList list = new ArrayList();

		// Initialize EJB Home

		try {

			invTransactionalBudgetHome = (LocalInvTransactionalBudgetHome)EJBHomeFactory.
				lookUpLocalHome(LocalInvTransactionalBudgetHome.JNDI_NAME, LocalInvTransactionalBudgetHome.class);
			invItemHome = (LocalInvItemHome)EJBHomeFactory.
					lookUpLocalHome(LocalInvItemHome.JNDI_NAME, LocalInvItemHome.class);
			adUserHome = (LocalAdUserHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdUserHome.JNDI_NAME, LocalAdUserHome.class);
			adLookUpValueHome = (LocalAdLookUpValueHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdLookUpValueHome.JNDI_NAME, LocalAdLookUpValueHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}
		double quantityForCurrentMonth = 0.0;
		try {
			Date date = new Date();
			java.text.SimpleDateFormat sdf = new java.text.SimpleDateFormat("MM");
			java.text.SimpleDateFormat sdfYear = new java.text.SimpleDateFormat("yyyy");
			String month = sdf.format(date);

			LocalAdUser adUser = adUserHome.findByUsrName(userName, AD_CMPNY);
			LocalAdLookUpValue adLookUpValue = adLookUpValueHome.findByLuNameAndLvName("AD DEPARTMENT", adUser.getUsrDept(), AD_CMPNY);
			System.out.println("itemName-" + itemName);
			System.out.println("adLookUpValue.getLvCode()-" + adLookUpValue.getLvCode());
			System.out.println("Integer.parseInt(sdfYear.format(date))-" + Integer.parseInt(sdfYear.format(date)));
			LocalInvTransactionalBudget invTransactionalBudget = invTransactionalBudgetHome.findByTbItemNameAndTbDeptAndTbYear(itemName, adLookUpValue.getLvCode(), Integer.parseInt(sdfYear.format(date)), AD_CMPNY);
			//LocalInvItem invItem = LocalInvItemHome


			if (month.equals("01")){

				quantityForCurrentMonth = invTransactionalBudget.getTbQuantityJan();
			}else if (month.equals("02")){

				quantityForCurrentMonth = invTransactionalBudget.getTbQuantityFeb();
			}else if (month.equals("03")){
				quantityForCurrentMonth = invTransactionalBudget.getTbQuantityMrch();
			}else if (month.equals("04")){
				quantityForCurrentMonth = invTransactionalBudget.getTbQuantityAprl();
			}else if (month.equals("05")){
				quantityForCurrentMonth = invTransactionalBudget.getTbQuantityMay();
			}else if (month.equals("06")){
				quantityForCurrentMonth = invTransactionalBudget.getTbQuantityJun();
			}else if (month.equals("07")){
				quantityForCurrentMonth = invTransactionalBudget.getTbQuantityJul();
			}else if (month.equals("08")){
				quantityForCurrentMonth = invTransactionalBudget.getTbQuantityAug();
			}else if (month.equals("09")){
				quantityForCurrentMonth = invTransactionalBudget.getTbQuantitySep();
			}else if (month.equals("10")){
				quantityForCurrentMonth = invTransactionalBudget.getTbQuantityOct();
			}else if (month.equals("11")){
				quantityForCurrentMonth = invTransactionalBudget.getTbQuantityNov();
			}else {
				quantityForCurrentMonth = invTransactionalBudget.getTbQuantityDec();
			}
			System.out.println(month + "<== current month");
			return quantityForCurrentMonth;

		} catch (FinderException ex) {
			System.out.println("no item found in transactional budget table");
			  //throw new GlobalNoRecordFoundException();
        }
		return quantityForCurrentMonth;
	}

	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public ArrayList getGlFcAllWithDefault(Integer AD_CMPNY) {

		Debug.print("ApPurchaseRequisitionEntryControllerBean getGlFcAllWithDefault");

		LocalGlFunctionalCurrencyHome glFunctionalCurrencyHome = null;
		LocalAdCompanyHome adCompanyHome = null;

		Collection glFunctionalCurrencies = null;

		LocalGlFunctionalCurrency glFunctionalCurrency = null;
		LocalAdCompany adCompany = null;


		ArrayList list = new ArrayList();

		// Initialize EJB Home

		try {

			glFunctionalCurrencyHome = (LocalGlFunctionalCurrencyHome)EJBHomeFactory.
				lookUpLocalHome(LocalGlFunctionalCurrencyHome.JNDI_NAME, LocalGlFunctionalCurrencyHome.class);
			adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}

		try {

			adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);

			glFunctionalCurrencies = glFunctionalCurrencyHome.findFcAllEnabled(
					EJBCommon.getGcCurrentDateWoTime().getTime(), AD_CMPNY);

		} catch (Exception ex) {

			throw new EJBException(ex.getMessage());
		}

		if (glFunctionalCurrencies.isEmpty()) {

			return null;

		}

		Iterator i = glFunctionalCurrencies.iterator();

		while (i.hasNext()) {

			glFunctionalCurrency = (LocalGlFunctionalCurrency)i.next();

			GlModFunctionalCurrencyDetails mdetails = new GlModFunctionalCurrencyDetails(
					glFunctionalCurrency.getFcCode(), glFunctionalCurrency.getFcName(),
					adCompany.getGlFunctionalCurrency().getFcName().equals(glFunctionalCurrency.getFcName()) ?
							EJBCommon.TRUE : EJBCommon.FALSE);

			list.add(mdetails);

		}

		return list;

	}

	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public ArrayList getInvLocAll(Integer AD_CMPNY) {

		Debug.print("ApPurchaseRequisitionEntryControllerBean getInvLocAll");

		LocalInvLocationHome invLocationHome = null;
		Collection invLocations = null;
		ArrayList list = new ArrayList();

		// Initialize EJB Home

		try {

			invLocationHome = (LocalInvLocationHome)EJBHomeFactory.
				lookUpLocalHome(LocalInvLocationHome.JNDI_NAME, LocalInvLocationHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}

		try {

			invLocations = invLocationHome.findLocAll(AD_CMPNY);

			if (invLocations.isEmpty()) {

				return null;

			}

			Iterator i = invLocations.iterator();

			while (i.hasNext()) {

				LocalInvLocation invLocation = (LocalInvLocation)i.next();
				String details = invLocation.getLocName();

				list.add(details);

			}

			return list;

		} catch (Exception ex) {

			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());

		}

	}

	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public short getGlFcPrecisionUnit(Integer AD_CMPNY) {

		Debug.print("ApPurchaseRequisitionEntryControllerBean getGlFcPrecisionUnit");

		LocalAdCompanyHome adCompanyHome = null;


		// Initialize EJB Home

		try {

			adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}

		try {

			LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);

			return  adCompany.getGlFunctionalCurrency().getFcPrecision();

		} catch (Exception ex) {

			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());

		}

	}

	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public short getInvGpQuantityPrecisionUnit(Integer AD_CMPNY) {

		Debug.print("ApPurchaseRequisitionEntryControllerBean getInvGpQuantityPrecisionUnit");

		LocalAdPreferenceHome adPreferenceHome = null;

		// Initialize EJB Home

		try {

			adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}


		try {

			LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);

			return adPreference.getPrfInvQuantityPrecisionUnit();

		} catch (Exception ex) {

			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());

		}

	}

	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public short getAdPrfApJournalLineNumber(Integer AD_CMPNY) {

		Debug.print("ApPurchaseRequisitionEntryControllerBean getAdPrfApJournalLineNumber");

		LocalAdPreferenceHome adPreferenceHome = null;

		// Initialize EJB Home

		try {

			adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}


		try {

			LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);

			return adPreference.getPrfApJournalLineNumber();

		} catch (Exception ex) {

			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());

		}

	}


	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public String getAdPrfApDefaultPrTax(Integer AD_CMPNY) {

		Debug.print("ApPurchaseRequisitionEntryControllerBean getAdPrfApDefaultPrTax");

		LocalAdPreferenceHome adPreferenceHome = null;

		// Initialize EJB Home

		try {

			adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}


		try {

			LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);

			return adPreference.getPrfApDefaultPrTax();

		} catch (Exception ex) {

			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());

		}

	}

	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public String getAdPrfApDefaultPrCurrency(Integer AD_CMPNY) {

		Debug.print("ApPurchaseRequisitionEntryControllerBean getAdPrfApDefaultPrCurrency");

		LocalAdPreferenceHome adPreferenceHome = null;

		// Initialize EJB Home

		try {

			adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}


		try {

			LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);

			return adPreference.getPrfApDefaultPrCurrency();

		} catch (Exception ex) {

			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());

		}

	}

	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public ApModPurchaseRequisitionDetails getApPrByPrCode(Integer PR_CODE, Integer AD_CMPNY)
	throws GlobalNoRecordFoundException {

		Debug.print("ApPurchaseRequisitionEntryControllerBean getApPrByPrCode");

		LocalApPurchaseRequisitionHome apPurchaseRequisitionHome = null;
		LocalAdCompanyHome adCompanyHome = null;
		LocalAdUserHome adUserHome = null;
		LocalInvTagHome invTagHome = null;
		LocalAdBranchHome adBranchHome = null;

		LocalAdApprovalQueueHome adApprovalQueueHome = null;

		// Initialize EJB Home

		try {

			apPurchaseRequisitionHome = (LocalApPurchaseRequisitionHome)EJBHomeFactory.
				lookUpLocalHome(LocalApPurchaseRequisitionHome.JNDI_NAME, LocalApPurchaseRequisitionHome.class);
			adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
			adUserHome = (LocalAdUserHome)EJBHomeFactory.
	                lookUpLocalHome(LocalAdUserHome.JNDI_NAME, LocalAdUserHome.class);
			invTagHome = (LocalInvTagHome)EJBHomeFactory.
	                lookUpLocalHome(LocalInvTagHome.JNDI_NAME, LocalInvTagHome.class);
			adBranchHome = (LocalAdBranchHome)EJBHomeFactory.
            lookUpLocalHome(LocalAdBranchHome.JNDI_NAME, LocalAdBranchHome.class);

			adApprovalQueueHome = (LocalAdApprovalQueueHome)EJBHomeFactory.
		            lookUpLocalHome(LocalAdApprovalQueueHome.JNDI_NAME, LocalAdApprovalQueueHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}

		try {

			LocalApPurchaseRequisition apPurchaseRequisition = null;


			try {

				apPurchaseRequisition = apPurchaseRequisitionHome.findByPrimaryKey(PR_CODE);

			} catch (FinderException ex) {

				throw new GlobalNoRecordFoundException();

			}

			String specs = null;
        	String propertyCode = null;
        	String expiryDate = null;
        	String serialNumber = null;
			ArrayList list = new ArrayList();




			// get purchase requisition lines if any

			Collection apPurchaseRequisitionLines = apPurchaseRequisition.getApPurchaseRequisitionLines();

			Iterator i = apPurchaseRequisitionLines.iterator();

			double PR_TOTAL_AMNT = 0d;

			while (i.hasNext()) {

				LocalApPurchaseRequisitionLine apPurchaseRequisitionLine = (LocalApPurchaseRequisitionLine) i.next();

				ApModPurchaseRequisitionLineDetails prlDetails = new ApModPurchaseRequisitionLineDetails();
				prlDetails.setTraceMisc(apPurchaseRequisitionLine.getInvItemLocation().getInvItem().getIiTraceMisc());
				ArrayList tagList = new ArrayList();
				if (prlDetails.getTraceMisc() == 1){
					Collection invTags = invTagHome.findByPrlCode(apPurchaseRequisitionLine.getPrlCode(),AD_CMPNY);
	    			Iterator x = invTags.iterator();
	    			while (x.hasNext()) {
	    				LocalInvTag invTag = (LocalInvTag) x.next();
	    				InvModTagListDetails tgLstDetails = new InvModTagListDetails();
	    				tgLstDetails.setTgPropertyCode(invTag.getTgPropertyCode());
	    				tgLstDetails.setTgSpecs(invTag.getTgSpecs());
	    				tgLstDetails.setTgExpiryDate(invTag.getTgExpiryDate());
	    				tgLstDetails.setTgSerialNumber(invTag.getTgSerialNumber());
	    				try{

	    					tgLstDetails.setTgCustodian(invTag.getAdUser().getUsrName());
	    				}
	    				catch(Exception ex){
	    					tgLstDetails.setTgCustodian("");
	    				}

	    				tagList.add(tgLstDetails);



	    			}
	    			prlDetails.setPrlTagList(tagList);
				}
				prlDetails.setPrlCode(apPurchaseRequisitionLine.getPrlCode());
				prlDetails.setPrlLine(apPurchaseRequisitionLine.getPrlLine());
				prlDetails.setPrlIlIiName(apPurchaseRequisitionLine.getInvItemLocation().getInvItem().getIiName());
				prlDetails.setPrlIlLocName(apPurchaseRequisitionLine.getInvItemLocation().getInvLocation().getLocName());
				prlDetails.setPrlQuantity(apPurchaseRequisitionLine.getPrlQuantity());
				prlDetails.setPrlAmount(apPurchaseRequisitionLine.getPrlAmount());
				prlDetails.setPrlIlIiDescription(apPurchaseRequisitionLine.getInvItemLocation().getInvItem().getIiDescription());
				prlDetails.setPrlUomName(apPurchaseRequisitionLine.getInvUnitOfMeasure().getUomName());


				PR_TOTAL_AMNT += apPurchaseRequisitionLine.getPrlAmount();

				list.add(prlDetails);

			}



			Collection adApprovalQueues = adApprovalQueueHome.findByAqDocumentAndAqDocumentCodeAll("AP PURCHASE REQUISITION", apPurchaseRequisition.getPrCode(), AD_CMPNY);

			System.out.println("adApprovalQueues="+adApprovalQueues.size());
			ArrayList approverlist = new ArrayList();

			i = adApprovalQueues.iterator();

			while (i.hasNext()) {
				LocalAdApprovalQueue adApprovalQueue = (LocalAdApprovalQueue)i.next();

				AdModApprovalQueueDetails adModApprovalQueueDetails = new AdModApprovalQueueDetails();
				adModApprovalQueueDetails.setAqApprovedDate(adApprovalQueue.getAqApprovedDate());
				adModApprovalQueueDetails.setAqApproverName(adApprovalQueue.getAdUser().getUsrDescription());
				adModApprovalQueueDetails.setAqStatus(adApprovalQueue.getAqApproved() == EJBCommon.TRUE ? adApprovalQueue.getAqLevel()+ " APPROVED" : adApprovalQueue.getAqLevel()+" PENDING");

				approverlist.add(adModApprovalQueueDetails);
			}


			ApModPurchaseRequisitionDetails mPrDetails = new ApModPurchaseRequisitionDetails();

			mPrDetails.setPrCode(apPurchaseRequisition.getPrCode());
			mPrDetails.setPrDate(apPurchaseRequisition.getPrDate());
			mPrDetails.setPrNumber(apPurchaseRequisition.getPrNumber());
			mPrDetails.setPrReferenceNumber(apPurchaseRequisition.getPrReferenceNumber());
			mPrDetails.setPrDescription(apPurchaseRequisition.getPrDescription());
			mPrDetails.setPrVoid(apPurchaseRequisition.getPrVoid());
			mPrDetails.setPrConversionDate(apPurchaseRequisition.getPrConversionDate());
			mPrDetails.setPrConversionRate(apPurchaseRequisition.getPrConversionRate());
			mPrDetails.setPrApprovalStatus(apPurchaseRequisition.getPrApprovalStatus());
			mPrDetails.setPrPosted(apPurchaseRequisition.getPrPosted());
			mPrDetails.setPrGenerated(apPurchaseRequisition.getPrGenerated());
			mPrDetails.setPrCanvassReasonForRejection(apPurchaseRequisition.getPrCanvassReasonForRejection());
			mPrDetails.setPrCreatedBy(apPurchaseRequisition.getPrCreatedBy());
			mPrDetails.setPrDateCreated(apPurchaseRequisition.getPrDateCreated());
			mPrDetails.setPrLastModifiedBy(apPurchaseRequisition.getPrLastModifiedBy());
			mPrDetails.setPrDateLastModified(apPurchaseRequisition.getPrDateLastModified());
			mPrDetails.setPrApprovedRejectedBy(apPurchaseRequisition.getPrApprovedRejectedBy());
			mPrDetails.setPrDateApprovedRejected(apPurchaseRequisition.getPrDateApprovedRejected());
			mPrDetails.setPrPostedBy(apPurchaseRequisition.getPrPostedBy());
			mPrDetails.setPrDatePosted(apPurchaseRequisition.getPrDatePosted());
			mPrDetails.setPrTagStatus(apPurchaseRequisition.getPrTagStatus());
			mPrDetails.setPrType(apPurchaseRequisition.getPrType());
			mPrDetails.setPrDeliveryPeriod(apPurchaseRequisition.getPrDeliveryPeriod());
			mPrDetails.setPrSchedule(apPurchaseRequisition.getPrSchedule());
			mPrDetails.setPrNextRunDate(apPurchaseRequisition.getPrNextRunDate());
			mPrDetails.setPrLastRunDate(apPurchaseRequisition.getPrLastRunDate());


        	if (apPurchaseRequisition.getPrAdUserName1() != null) {

        		LocalAdUser userName1 = adUserHome.findByPrimaryKey(apPurchaseRequisition.getPrAdUserName1());
                mPrDetails.setPrAdNotifiedUser1(userName1.getUsrName());

        	}


			mPrDetails.setPrCanvassApprovalStatus(apPurchaseRequisition.getPrCanvassApprovalStatus());
			mPrDetails.setPrCanvassApprovedRejectedBy(apPurchaseRequisition.getPrCanvassApprovedRejectedBy());
			mPrDetails.setPrCanvassDateApprovedRejected(apPurchaseRequisition.getPrCanvassDateApprovedRejected());
			mPrDetails.setPrCanvassDatePosted(apPurchaseRequisition.getPrCanvassDatePosted());
			mPrDetails.setPrCanvassPosted(apPurchaseRequisition.getPrCanvassPosted());
			mPrDetails.setPrCanvassPostedBy(apPurchaseRequisition.getPrCanvassPostedBy());

			mPrDetails.setPrReasonForRejection(apPurchaseRequisition.getPrReasonForRejection());
			mPrDetails.setPrCanvassReasonForRejection(apPurchaseRequisition.getPrCanvassReasonForRejection());
			System.out.println("TC_NAME="+apPurchaseRequisition.getApTaxCode().getTcName());
			System.out.println("FC_NAME="+apPurchaseRequisition.getGlFunctionalCurrency().getFcName());
			mPrDetails.setPrTcName(apPurchaseRequisition.getApTaxCode().getTcName());
			mPrDetails.setPrFcName(apPurchaseRequisition.getGlFunctionalCurrency().getFcName());
			mPrDetails.setPrBrCode(apPurchaseRequisition.getPrAdBranch());
			mPrDetails.setPrAmount(PR_TOTAL_AMNT);

			mPrDetails.setPrPrlList(list);
			mPrDetails.setPrAPRList(approverlist);

			return mPrDetails;

		} catch (GlobalNoRecordFoundException ex) {

			throw ex;

		} catch (Exception ex) {

			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());

		}

	}

	/**
	 * @ejb:interface-method view-type="remote"
	 **/
	public ApModPurchaseRequisitionLineDetails getApCnvByPrlCode(ApModPurchaseRequisitionLineDetails mdetails, Integer PRL_CODE, Integer AD_CMPNY) {

		Debug.print("ApPurchaseRequisitionEntryControllerBean getApCnvByPrlCode");

		LocalApCanvassHome apCanvassHome = null;
		LocalApPurchaseRequisitionLineHome apPurchaseRequisitionLineHome = null;

		LocalApPurchaseRequisitionLine apPurchaseRequisitionLine = null;

		ArrayList list = new ArrayList();

		//Initialize EJB Home

		try {

			apCanvassHome = (LocalApCanvassHome)EJBHomeFactory.
				lookUpLocalHome(LocalApCanvassHome.JNDI_NAME, LocalApCanvassHome.class);
			apPurchaseRequisitionLineHome = (LocalApPurchaseRequisitionLineHome)EJBHomeFactory.
				lookUpLocalHome(LocalApPurchaseRequisitionLineHome.JNDI_NAME, LocalApPurchaseRequisitionLineHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}

		try {

			ApModPurchaseRequisitionLineDetails details = new ApModPurchaseRequisitionLineDetails();

			if(PRL_CODE == null) {

				//regenerate canvass lines
                System.out.println("regenerate canvass");
				details.setRegenerateCanvass(true);

			} else {

				try {

					apPurchaseRequisitionLine = apPurchaseRequisitionLineHome.findByPrimaryKey(PRL_CODE);

				} catch (FinderException ex) {

				}

				System.out.println("iiname : " + apPurchaseRequisitionLine.getInvItemLocation().getInvItem().getIiName());
				System.out.println("locname : " + apPurchaseRequisitionLine.getInvItemLocation().getInvLocation().getLocName());
				System.out.println("uom : " + apPurchaseRequisitionLine.getInvUnitOfMeasure().getUomName());

				if(!apPurchaseRequisitionLine.getInvItemLocation().getInvItem().getIiName().equals(mdetails.getPrlIlIiName()) ||
					!apPurchaseRequisitionLine.getInvItemLocation().getInvLocation().getLocName().equals(mdetails.getPrlIlLocName())||
					!apPurchaseRequisitionLine.getInvUnitOfMeasure().getUomName().equals(mdetails.getPrlUomName())) {
                    
					details.setRegenerateCanvass(true);

				} else {

					details.setRegenerateCanvass(false);

					Collection apCanvasses = apPurchaseRequisitionLine.getApCanvasses();

					Iterator i = apCanvasses.iterator();

					while(i.hasNext()) {

						LocalApCanvass apCanvass = (LocalApCanvass)i.next();

						ApModCanvassDetails cnvDetails = new ApModCanvassDetails();
						
						System.out.println("cnv code : " + apCanvass.getCnvCode());
						

						cnvDetails.setCnvCode(apCanvass.getCnvCode());
						cnvDetails.setCnvRemarks(apCanvass.getCnvRemarks());
						cnvDetails.setCnvLine(apCanvass.getCnvLine());
						cnvDetails.setCnvQuantity(apCanvass.getCnvQuantity());
						cnvDetails.setCnvUnitCost(apCanvass.getCnvUnitCost());
						cnvDetails.setCnvPo(apCanvass.getCnvPo());
						cnvDetails.setCnvSplSupplierCode(apCanvass.getApSupplier().getSplSupplierCode());

						details.saveCnvList(cnvDetails);

					}

				}

			}

			return details;

		} catch (Exception ex) {

			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());

		}

	}

	/**
	 * @ejb:interface-method view-type="remote"
	 **/
	public Integer getApPrlCodeByPrlLineAndPrCode(short PRL_LN, Integer PR_CODE, Integer AD_BRNCH, Integer AD_CMPNY) {

		Debug.print("ApPurchaseRequisitionEntryControllerBean getApPrlCode");

		LocalApPurchaseRequisitionLineHome apPurchaseRequisitionLineHome = null;

		//Initialize EJB Home

		try {

			apPurchaseRequisitionLineHome = (LocalApPurchaseRequisitionLineHome)EJBHomeFactory.
				lookUpLocalHome(LocalApPurchaseRequisitionLineHome.JNDI_NAME, LocalApPurchaseRequisitionLineHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}

		try {
			
			System.out.println("pr code: " + PR_CODE);
			
			System.out.println("prl line: " + PRL_LN);
			LocalApPurchaseRequisitionLine apPurchaseRequisitionLine = apPurchaseRequisitionLineHome.findByPrCodeAndPrlLineAndBrCode(PR_CODE, PRL_LN, AD_BRNCH, AD_CMPNY);

			return apPurchaseRequisitionLine.getPrlCode();

		} catch (Exception ex) {

			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());

		}

	}

	/**
	 * @ejb:interface-method view-type="remote"
	 **/
	public Integer saveApPrCanvass(com.util.ApPurchaseRequisitionDetails details, String RV_AD_NTFD_USR1, String TC_NM, String FC_NM, ArrayList prlList, boolean isDraft, Integer AD_BRNCH, Integer AD_CMPNY) throws
	GlobalRecordAlreadyDeletedException,
	GlobalDocumentNumberNotUniqueException,
	GlobalConversionDateNotExistException,
	GlobalPaymentTermInvalidException,
	GlobalTransactionAlreadyApprovedException,
	GlobalTransactionAlreadyPendingException,
	GlobalTransactionAlreadyPostedException,
	GlobalTransactionAlreadyVoidException,
	GlobalInvItemLocationNotFoundException,
	GlobalNoApprovalRequesterFoundException,
	GlobalNoApprovalApproverFoundException,
	GlobalSupplierItemInvalidException,
	GlobalRecordInvalidException {

		Debug.print("ApPurchaseRequisitionEntryControllerBean saveApPrCanvass");

		LocalApPurchaseRequisitionHome apPurchaseRequisitionHome = null;
		LocalApPurchaseRequisitionLineHome apPurchaseRequisitionLineHome = null;
		LocalAdCompanyHome adCompanyHome = null;
		LocalAdDocumentSequenceAssignmentHome adDocumentSequenceAssignmentHome = null;
		LocalApSupplierHome apSupplierHome = null;
		LocalApTaxCodeHome apTaxCodeHome = null;
		LocalGlFunctionalCurrencyHome glFunctionalCurrencyHome = null;
		LocalGlFunctionalCurrencyRateHome glFunctionalCurrencyRateHome = null;
		LocalAdPreferenceHome adPreferenceHome = null;
		LocalInvItemLocationHome invItemLocationHome = null;
		LocalInvUnitOfMeasureHome invUnitOfMeasureHome = null;
		LocalAdApprovalHome adApprovalHome = null;
		LocalAdUserHome adUserHome = null;
		LocalAdBranchDocumentSequenceAssignmentHome adBranchDocumentSequenceAssignmentHome = null;
        LocalApPurchaseOrderLineHome apPurchaseOrderLineHome = null;
        LocalApCanvassHome apCanvassHome = null;
        LocalAdApprovalUserHome adApprovalUserHome = null;
        LocalAdApprovalQueueHome adApprovalQueueHome = null;
		LocalApPurchaseRequisition apPurchaseRequisition = null;
		LocalAdAmountLimitHome adAmountLimitHome = null;
		LocalInvTagHome invTagHome = null;


		ArrayList list = new ArrayList();

		// Initialize EJB Home

		try {

			apPurchaseRequisitionHome = (LocalApPurchaseRequisitionHome)EJBHomeFactory.
				lookUpLocalHome(LocalApPurchaseRequisitionHome.JNDI_NAME, LocalApPurchaseRequisitionHome.class);
			apPurchaseRequisitionLineHome = (LocalApPurchaseRequisitionLineHome)EJBHomeFactory.
				lookUpLocalHome(LocalApPurchaseRequisitionLineHome.JNDI_NAME, LocalApPurchaseRequisitionLineHome.class);
			adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
			adDocumentSequenceAssignmentHome = (LocalAdDocumentSequenceAssignmentHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdDocumentSequenceAssignmentHome.JNDI_NAME, LocalAdDocumentSequenceAssignmentHome.class);
			apSupplierHome = (LocalApSupplierHome)EJBHomeFactory.
				lookUpLocalHome(LocalApSupplierHome.JNDI_NAME, LocalApSupplierHome.class);
			apTaxCodeHome = (LocalApTaxCodeHome)EJBHomeFactory.
				lookUpLocalHome(LocalApTaxCodeHome.JNDI_NAME, LocalApTaxCodeHome.class);
			glFunctionalCurrencyHome = (LocalGlFunctionalCurrencyHome)EJBHomeFactory.
				lookUpLocalHome(LocalGlFunctionalCurrencyHome.JNDI_NAME, LocalGlFunctionalCurrencyHome.class);
			glFunctionalCurrencyRateHome = (LocalGlFunctionalCurrencyRateHome)EJBHomeFactory.
				lookUpLocalHome(LocalGlFunctionalCurrencyRateHome.JNDI_NAME, LocalGlFunctionalCurrencyRateHome.class);
			adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);
			invItemLocationHome = (LocalInvItemLocationHome)EJBHomeFactory.
				lookUpLocalHome(LocalInvItemLocationHome.JNDI_NAME, LocalInvItemLocationHome.class);
			invUnitOfMeasureHome = (LocalInvUnitOfMeasureHome)EJBHomeFactory.
				lookUpLocalHome(LocalInvUnitOfMeasureHome.JNDI_NAME, LocalInvUnitOfMeasureHome.class);
			adApprovalHome = (LocalAdApprovalHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdApprovalHome.JNDI_NAME, LocalAdApprovalHome.class);
			adApprovalUserHome = (LocalAdApprovalUserHome)EJBHomeFactory.
    		lookUpLocalHome(LocalAdApprovalUserHome.JNDI_NAME, LocalAdApprovalUserHome.class);

			adApprovalQueueHome = (LocalAdApprovalQueueHome)EJBHomeFactory.
    		lookUpLocalHome(LocalAdApprovalQueueHome.JNDI_NAME, LocalAdApprovalQueueHome.class);
			adUserHome = (LocalAdUserHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdUserHome.JNDI_NAME, LocalAdUserHome.class);
			adAmountLimitHome = (LocalAdAmountLimitHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdAmountLimitHome.JNDI_NAME, LocalAdAmountLimitHome.class);


			adBranchDocumentSequenceAssignmentHome = (LocalAdBranchDocumentSequenceAssignmentHome)EJBHomeFactory.
    			lookUpLocalHome(LocalAdBranchDocumentSequenceAssignmentHome.JNDI_NAME, LocalAdBranchDocumentSequenceAssignmentHome.class);
			apPurchaseOrderLineHome = (LocalApPurchaseOrderLineHome)EJBHomeFactory.
				lookUpLocalHome(LocalApPurchaseOrderLineHome.JNDI_NAME, LocalApPurchaseOrderLineHome.class);
			apCanvassHome = (LocalApCanvassHome)EJBHomeFactory.
				lookUpLocalHome(LocalApCanvassHome.JNDI_NAME, LocalApCanvassHome.class);
			invTagHome = (LocalInvTagHome)EJBHomeFactory.
				lookUpLocalHome(LocalInvTagHome.JNDI_NAME, LocalInvTagHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}

		try {

			// validate if purchase requisition is already deleted

			try {

				if (details.getPrCode() != null) {

					apPurchaseRequisition = apPurchaseRequisitionHome.findByPrimaryKey(details.getPrCode());

				}

			} catch (FinderException ex) {

				throw new GlobalRecordAlreadyDeletedException();

			}

			// validate if purchase requisition is already posted, void, approved or pending

			if (details.getPrCode() != null) {

	        	if (apPurchaseRequisition.getPrCanvassApprovalStatus() != null) {

	        		if (apPurchaseRequisition.getPrCanvassApprovalStatus().equals("APPROVED") ||
	        				apPurchaseRequisition.getPrCanvassApprovalStatus().equals("N/A")) {

	        		    throw new GlobalTransactionAlreadyApprovedException();

	        		} else if (apPurchaseRequisition.getPrCanvassApprovalStatus().equals("PENDING")) {

	        			throw new GlobalTransactionAlreadyPendingException();

	        		}
	        	}

        		if (apPurchaseRequisition.getPrCanvassPosted() == EJBCommon.TRUE) {

        			throw new GlobalTransactionAlreadyPostedException();

        		} else if (apPurchaseRequisition.getPrVoid() == EJBCommon.TRUE) {

        			throw new GlobalTransactionAlreadyVoidException();

        		}
        	}

			// purchase requisition void

			if (details.getPrCode() != null && details.getPrVoid() == EJBCommon.TRUE &&
					apPurchaseRequisition.getPrCanvassPosted() == EJBCommon.FALSE) {

				apPurchaseRequisition.setPrVoid(EJBCommon.TRUE);
				apPurchaseRequisition.setPrLastModifiedBy(details.getPrLastModifiedBy());
				apPurchaseRequisition.setPrDateLastModified(details.getPrDateLastModified());

				return apPurchaseRequisition.getPrCode();

			}

			// validate if document number is unique document number is automatic then set next sequence

			LocalAdBranchDocumentSequenceAssignment adBranchDocumentSequenceAssignment = null;
			LocalAdDocumentSequenceAssignment adDocumentSequenceAssignment = null;

			if (details.getPrCode() == null) {

				try {

					adDocumentSequenceAssignment = adDocumentSequenceAssignmentHome.findByDcName("AP PURCHASE REQUISITION", AD_CMPNY);

				} catch(FinderException ex) { }

				try {

					adBranchDocumentSequenceAssignment = adBranchDocumentSequenceAssignmentHome.findBdsByDsaCodeAndBrCode(adDocumentSequenceAssignment.getDsaCode(), AD_BRNCH, AD_CMPNY);

				} catch (FinderException ex) { }

				LocalApPurchaseRequisition apExistingPurchaseRequisition = null;

				try {

					apExistingPurchaseRequisition = apPurchaseRequisitionHome.findByPrNumberAndBrCode(
							details.getPrNumber(), AD_BRNCH, AD_CMPNY);

				} catch (FinderException ex) {
				}

				if (apExistingPurchaseRequisition != null) {

					throw new GlobalDocumentNumberNotUniqueException();

				}

				if (adDocumentSequenceAssignment.getAdDocumentSequence().getDsNumberingType() == 'A' &&
						(details.getPrNumber() == null || details.getPrNumber().trim().length() == 0)) {

					while (true) {

						if (adBranchDocumentSequenceAssignment == null || adBranchDocumentSequenceAssignment.getBdsNextSequence() == null) {

							try {

								apPurchaseRequisitionHome.findByPrNumberAndBrCode(adDocumentSequenceAssignment.getDsaNextSequence(), AD_BRNCH, AD_CMPNY);
								adDocumentSequenceAssignment.setDsaNextSequence(EJBCommon.incrementStringNumber(adDocumentSequenceAssignment.getDsaNextSequence()));

							} catch (FinderException ex) {

								details.setPrNumber(adDocumentSequenceAssignment.getDsaNextSequence());
								adDocumentSequenceAssignment.setDsaNextSequence(EJBCommon.incrementStringNumber(adDocumentSequenceAssignment.getDsaNextSequence()));
								break;

							}

						} else {

							try {

								apPurchaseRequisitionHome.findByPrNumberAndBrCode(adBranchDocumentSequenceAssignment.getBdsNextSequence(), AD_BRNCH, AD_CMPNY);
								adBranchDocumentSequenceAssignment.setBdsNextSequence(EJBCommon.incrementStringNumber(adBranchDocumentSequenceAssignment.getBdsNextSequence()));

							} catch (FinderException ex) {

								details.setPrNumber(adBranchDocumentSequenceAssignment.getBdsNextSequence());
								adBranchDocumentSequenceAssignment.setBdsNextSequence(EJBCommon.incrementStringNumber(adBranchDocumentSequenceAssignment.getBdsNextSequence()));
								break;

							}

						}

					}

				}

			} else {

				LocalApPurchaseRequisition apExistingPurchaseRequisition = null;

				try {

					apExistingPurchaseRequisition = apPurchaseRequisitionHome.findByPrNumberAndBrCode(
							details.getPrNumber(), AD_BRNCH, AD_CMPNY);

				} catch (FinderException ex) {
				}

				if (apExistingPurchaseRequisition != null &&
						!apExistingPurchaseRequisition.getPrCode().equals(details.getPrCode())) {

					throw new GlobalDocumentNumberNotUniqueException();

				}

				if (apPurchaseRequisition.getPrNumber() != details.getPrNumber() &&
						(details.getPrNumber() == null || details.getPrNumber().trim().length() == 0)) {

					details.setPrNumber(apPurchaseRequisition.getPrNumber());

				}

			}

			// validate if conversion date exists

			// validate if conversion date exists

            try {

                if (details.getPrConversionDate() != null) {


                    LocalGlFunctionalCurrency glValidateFunctionalCurrency = glFunctionalCurrencyHome.findByFcName(FC_NM, AD_CMPNY);
                    LocalGlFunctionalCurrencyRate glFunctionalCurrencyRate =
                        glFunctionalCurrencyRateHome.findByFcCodeAndDate(glValidateFunctionalCurrency.getFcCode(),
                                details.getPrConversionDate(), AD_CMPNY);

                }

            } catch (FinderException ex) {

                throw new GlobalConversionDateNotExistException();

            }

			// get all notified users
			LocalAdUser userName1 = null;

		    try {

		    	userName1 = adUserHome.findByUsrName(RV_AD_NTFD_USR1, AD_CMPNY);

		    } catch (FinderException ex) {

		    }

			boolean isRecalculate = true;

			// create purchase requisition

			if (details.getPrCode() == null) {

				apPurchaseRequisition = apPurchaseRequisitionHome.create(details.getPrDescription(),details.getPrNumber(), details.getPrDate(), details.getPrDeliveryPeriod(), details.getPrReferenceNumber(),
						details.getPrConversionDate(), details.getPrConversionRate(),
						null, EJBCommon.FALSE, EJBCommon.FALSE, null, EJBCommon.FALSE,EJBCommon.FALSE, null, null,
						details.getPrCreatedBy(), details.getPrDateCreated(), details.getPrLastModifiedBy(),details.getPrDateLastModified(),
						details.getPrApprovedRejectedBy(), details.getPrDateApprovedRejected(), details.getPrPostedBy(), details.getPrDatePosted(),
						details.getPrCanvassApprovedRejectedBy(), details.getPrCanvassDateApprovedRejected(), details.getPrCanvassPostedBy(), details.getPrCanvassDatePosted(),
						details.getPrTagStatus(),details.getPrType(),
						userName1 != null ? userName1.getUsrCode() : null,details.getPrSchedule(), details.getPrNextRunDate(), null,
						AD_BRNCH, AD_CMPNY);

			} else {

				// check if critical fields are changed

				if (!apPurchaseRequisition.getApTaxCode().getTcName().equals(TC_NM) ||
						prlList.size() != apPurchaseRequisition.getApPurchaseRequisitionLines().size()) {

					isRecalculate = true;

				} else if (prlList.size() == apPurchaseRequisition.getApPurchaseRequisitionLines().size()) {

					Iterator ilIter = apPurchaseRequisition.getApPurchaseRequisitionLines().iterator();
					Iterator ilListIter = prlList.iterator();

					while (ilIter.hasNext()) {

						LocalApPurchaseRequisitionLine apPurchaseRequisitionLine = (LocalApPurchaseRequisitionLine)ilIter.next();
						ApModPurchaseRequisitionLineDetails mdetails = (ApModPurchaseRequisitionLineDetails)ilListIter.next();

						if (!apPurchaseRequisitionLine.getInvItemLocation().getInvItem().getIiName().equals(mdetails.getPrlIlIiName()) ||
								!apPurchaseRequisitionLine.getInvItemLocation().getInvItem().getIiDescription().equals(mdetails.getPrlIlIiDescription()) ||
								!apPurchaseRequisitionLine.getInvItemLocation().getInvLocation().getLocName().equals(mdetails.getPrlIlLocName()) ||
								!apPurchaseRequisitionLine.getInvUnitOfMeasure().getUomName().equals(mdetails.getPrlUomName()) ||
								apPurchaseRequisitionLine.getPrlQuantity() != mdetails.getPrlQuantity() ||
								apPurchaseRequisitionLine.getPrlAmount() != mdetails.getPrlAmount()) {

					//		isRecalculate = true;

						}

					}

				} else {

				//	isRecalculate = false;

				}

				apPurchaseRequisition.setPrDescription(details.getPrDescription());
				apPurchaseRequisition.setPrDate(details.getPrDate());
				apPurchaseRequisition.setPrNumber(details.getPrNumber());
				apPurchaseRequisition.setPrReferenceNumber(details.getPrReferenceNumber());
				apPurchaseRequisition.setPrVoid(details.getPrVoid());
				apPurchaseRequisition.setPrConversionDate(details.getPrConversionDate());
				apPurchaseRequisition.setPrConversionRate(details.getPrConversionRate());
				apPurchaseRequisition.setPrLastModifiedBy(details.getPrLastModifiedBy());
				apPurchaseRequisition.setPrDateLastModified(details.getPrDateLastModified());
				apPurchaseRequisition.setPrDeliveryPeriod(details.getPrDeliveryPeriod());
				apPurchaseRequisition.setPrAdUserName1(userName1 != null ? userName1.getUsrCode() : null);
				apPurchaseRequisition.setPrSchedule(details.getPrSchedule());
				apPurchaseRequisition.setPrNextRunDate(details.getPrNextRunDate());

			}

			LocalApTaxCode apTaxCode = apTaxCodeHome.findByTcName(TC_NM, AD_CMPNY);
			apPurchaseRequisition.setApTaxCode(apTaxCode);

			LocalGlFunctionalCurrency glFunctionalCurrency = glFunctionalCurrencyHome.findByFcName(FC_NM, AD_CMPNY);
			apPurchaseRequisition.setGlFunctionalCurrency(glFunctionalCurrency);

			double TOTAL_AMOUNT = 0d;

			if (isRecalculate) {

				// remove purchase requisition lines

				Collection apPurchaseRequisitionLines = apPurchaseRequisition.getApPurchaseRequisitionLines();

				Iterator i = apPurchaseRequisitionLines.iterator();

				while (i.hasNext()) {

					LocalApPurchaseRequisitionLine apPurchaseRequisitionLine = (LocalApPurchaseRequisitionLine)i.next();

					i.remove();

					apPurchaseRequisitionLine.remove();

				}

				// add new purchase requisition lines

				i = prlList.iterator();

				LocalInvItemLocation invItemLocation = null;


				while (i.hasNext()) {

					ApModPurchaseRequisitionLineDetails mPrlDetails = (ApModPurchaseRequisitionLineDetails) i.next();

					// save purchase requisition line, qty and amount based on the retrieved unit cost

					LocalApPurchaseRequisitionLine apPurchaseRequisitionLine = apPurchaseRequisitionLineHome.create(mPrlDetails.getPrlLine(), mPrlDetails.getPrlQuantity(), mPrlDetails.getPrlAmount(), AD_CMPNY);

					apPurchaseRequisition.addApPurchaseRequisitionLine(apPurchaseRequisitionLine);

					try {

						invItemLocation = invItemLocationHome.findByLocNameAndIiName(
								mPrlDetails.getPrlIlLocName(), mPrlDetails.getPrlIlIiName(), AD_CMPNY);


					} catch (FinderException ex) {

						throw new GlobalInvItemLocationNotFoundException(String.valueOf(mPrlDetails.getPrlLine()));

					}


					invItemLocation.addApPurchaseRequisitionLine(apPurchaseRequisitionLine);

					LocalInvUnitOfMeasure invUnitOfMeasure = invUnitOfMeasureHome.findByUomName(
							mPrlDetails.getPrlUomName(), AD_CMPNY);

					invUnitOfMeasure.addApPurchaseRequisitionLine(apPurchaseRequisitionLine);

					short CNV_LN = 0;
					double II_UNT_CST = mPrlDetails.getPrlAmount();
					double CNV_QTY = mPrlDetails.getPrlQuantity();



					if(mPrlDetails.getRegenerateCanvass() == true) {

						if (invItemLocation.getInvItem().getApSupplier() != null) {

							//generate canvass lines for purchase requisition line
							CNV_LN++;

							//create canvass line
							LocalApCanvass apCanvass = apCanvassHome.create(CNV_LN, null, CNV_QTY, II_UNT_CST, II_UNT_CST, EJBCommon.TRUE, AD_CMPNY);

							LocalApSupplier apSupplier = invItemLocation.getInvItem().getApSupplier();
							apSupplier.addApCanvass(apCanvass);

							apPurchaseRequisitionLine.addApCanvass(apCanvass);

						}

						CNV_LN++;

						//create canvass lines for other suppliers of item
						Collection apPurchaseOrderLines = null;

						try {

							apPurchaseOrderLines = apPurchaseOrderLineHome.findByPlIlCodeAndPoReceivingAndPoPostedAndBrCode(apPurchaseRequisitionLine.getInvItemLocation().getIlCode(), EJBCommon.TRUE, EJBCommon.TRUE, AD_BRNCH, AD_CMPNY);

						} catch(FinderException ex) {

						}

						if(!apPurchaseOrderLines.isEmpty() && apPurchaseOrderLines.size() > 0) {

							Iterator plIter = apPurchaseOrderLines.iterator();

							while(plIter.hasNext()) {

								LocalApPurchaseOrderLine apPurchaseOrderLine = (LocalApPurchaseOrderLine)plIter.next();

								// continue if next purchase order line has existing supplier in canvass

								 Collection apCanvasses = apCanvassHome.findByPrlCodeAndSplSupplierCode(apPurchaseRequisitionLine.getPrlCode(),
										apPurchaseOrderLine.getApPurchaseOrder().getApSupplier().getSplSupplierCode(), AD_CMPNY);

								if(!apCanvasses.isEmpty()) continue;

								// convert purchase order line unit cost
								double POL_UNT_CST = this.convertCostByUom(apPurchaseOrderLine.getInvItemLocation().getInvItem().getIiName(),
										apPurchaseOrderLine.getInvUnitOfMeasure().getUomName(),
										apPurchaseOrderLine.getPlUnitCost(), false, AD_CMPNY);
								POL_UNT_CST = this.convertCostByUom(apPurchaseOrderLine.getInvItemLocation().getInvItem().getIiName(),
										apPurchaseRequisitionLine.getInvUnitOfMeasure().getUomName(),
										POL_UNT_CST, true, AD_CMPNY);

								//create canvass line
								LocalApCanvass apCanvass = apCanvassHome.create(CNV_LN,null, 0d, POL_UNT_CST, 0d, EJBCommon.FALSE, AD_CMPNY);

								if (CNV_LN == 1) {

									apCanvass.setCnvQuantity(CNV_QTY);
									apCanvass.setCnvAmount(II_UNT_CST);
									apCanvass.setCnvPo(EJBCommon.TRUE);

								}

								LocalApSupplier apSupplier = apPurchaseOrderLine.getApPurchaseOrder().getApSupplier();
								apSupplier.addApCanvass(apCanvass);

								apPurchaseRequisitionLine.addApCanvass(apCanvass);

								CNV_LN++;

							}

						}

					} else {

						CNV_LN++;
						ArrayList cnvList = mPrlDetails.getCnvList();

						Iterator cnvIter = cnvList.iterator();

						while(cnvIter.hasNext()) {

							ApModCanvassDetails cnvDetails = (ApModCanvassDetails)cnvIter.next();

							//create canvass line
							LocalApCanvass apCanvass = apCanvassHome.create(CNV_LN, cnvDetails.getCnvRemarks(),
									cnvDetails.getCnvQuantity(), cnvDetails.getCnvUnitCost(),
									EJBCommon.roundIt(cnvDetails.getCnvUnitCost() * cnvDetails.getCnvQuantity(), this.getGlFcPrecisionUnit(AD_CMPNY)),
									cnvDetails.getCnvPo(), AD_CMPNY);

							LocalApSupplier apSupplier = apSupplierHome.findBySplSupplierCode(cnvDetails.getCnvSplSupplierCode(), AD_CMPNY);
							apSupplier.addApCanvass(apCanvass);

							apPurchaseRequisitionLine.addApCanvass(apCanvass);

							CNV_LN++;

						}

					}

					 try{
	      	  	    	System.out.println("aabot?");
	      	  	    	System.out.println(mPrlDetails.getPrlTagList() + "<== mPrlDetails.getPrlTagList");
		      	  	    Iterator t = mPrlDetails.getPrlTagList().iterator();

		      	  	    LocalInvTag invTag  = null;
		      	  	    System.out.println(t + "<== mPrlDetails.getPrlTagList().iterator()");
		      	  	    while (t.hasNext()){
		      	  	    	InvModTagListDetails tgLstDetails = (InvModTagListDetails)t.next();
		      	  	    	System.out.println(tgLstDetails.getTgCustodian() + "<== custodian");
		      	  	    	System.out.println(tgLstDetails.getTgSpecs() + "<== specs");
		      	  	    	System.out.println(tgLstDetails.getTgPropertyCode() + "<== propertyCode");
		      	  	    	System.out.println(tgLstDetails.getTgExpiryDate() + "<== expiryDate");
		      	  	    	System.out.println(tgLstDetails.getTgSerialNumber() + "<== serial number");

		      	  	    	if (tgLstDetails.getTgCode()==null){
		      	  	    		System.out.println("ngcreate ba?");
			      	  	    	invTag = invTagHome.create(tgLstDetails.getTgPropertyCode(),
			      	  	    			tgLstDetails.getTgSerialNumber(),null,tgLstDetails.getTgExpiryDate(),
			      	  	    			tgLstDetails.getTgSpecs(), AD_CMPNY,  tgLstDetails.getTgTransactionDate(),
			      	  	    			tgLstDetails.getTgType());

			      	  	    	invTag.setApPurchaseRequisitionLine(apPurchaseRequisitionLine);
			      	  	    	LocalAdUser adUser = null;
			      	  	    	try {
			      	  	    		adUser = adUserHome.findByUsrName(tgLstDetails.getTgCustodian(), AD_CMPNY);
			      	  	    	}catch(FinderException ex){

			      	  	    	}
			      	  	    	invTag.setAdUser(adUser);
			      	  	    	System.out.println("ngcreate ba?");
		      	  	    	}

		      	  	    }

	      	  	    }catch(Exception ex){

	      	  	    }


				}

			} else {

				Iterator y = prlList.iterator();
				System.out.println(prlList + "<== prlList");
				while (y.hasNext()) {
					ApModPurchaseRequisitionLineDetails mPrlDetails = (ApModPurchaseRequisitionLineDetails) y.next();
					System.out.println(mPrlDetails + "<== mPrlDetails");
					LocalApPurchaseRequisitionLine apPurchaseRequisitionLine = apPurchaseRequisitionLineHome.findByPrimaryKey(mPrlDetails.getPrlCode());

					//remove all inv tag inside PR line
		  	   	    Collection invTags = apPurchaseRequisitionLine.getInvTags();

		  	   	    Iterator x = invTags.iterator();

		  	   	    while (x.hasNext()){

		  	   	    	LocalInvTag invTag = (LocalInvTag)x.next();

		  	   	    	x.remove();

		  	   	    	invTag.remove();
		  	   	    }

	      	  	    System.out.println("mgcecreate ng bago?");
	      	  	    System.out.println(mPrlDetails + "<== mPrlDetails");
	      	  	    System.out.println(mPrlDetails.getPrlTagList() + "<== mPrlDetails.getPrlTagList()");
	      	  	    Iterator t = mPrlDetails.getPrlTagList().iterator();
	      	  	    while (t.hasNext()){
	      	  	    	InvModTagListDetails tgLstDetails = (InvModTagListDetails)t.next();
	      	  	    	System.out.println(tgLstDetails.getTgCustodian() + "<== custodian");
	      	  	    	System.out.println(tgLstDetails.getTgSpecs() + "<== specs");
	      	  	    	System.out.println(tgLstDetails.getTgPropertyCode() + "<== propertyCode");
	      	  	    	System.out.println(tgLstDetails.getTgExpiryDate() + "<== expiryDate");
	  	  	    		System.out.println("ngcreate ng bago?");
	  	  	    	    LocalInvTag invTag = invTagHome.create(tgLstDetails.getTgPropertyCode(),
	      	  	    			tgLstDetails.getTgSerialNumber(),null,tgLstDetails.getTgExpiryDate(),
	      	  	    			tgLstDetails.getTgSpecs(), AD_CMPNY, tgLstDetails.getTgTransactionDate(),
	      	  	    			tgLstDetails.getTgType());

	      	  	    	invTag.setApPurchaseRequisitionLine(apPurchaseRequisitionLine);
	      	  	    	LocalAdUser adUser = null;
	      	  	    	try {
	      	  	    		adUser = adUserHome.findByUsrName(tgLstDetails.getTgCustodian(), AD_CMPNY);
	      	  	    	}catch(FinderException ex){

	      	  	    	}
	      	  	    	invTag.setAdUser(adUser);
	      	  	    	System.out.println("ngcreate ba?");

	      	  	    }

				}
			}

			// check if all lines' canvass are complete

			Collection apCheckPurchaseRequisitionLines = apPurchaseRequisition.getApPurchaseRequisitionLines();

			Iterator checkIter = apCheckPurchaseRequisitionLines.iterator();

			while (checkIter.hasNext()) {
				LocalApPurchaseRequisitionLine apCheckApPurchaseRequisitionLine = (LocalApPurchaseRequisitionLine)checkIter.next();

				Collection apCanvasses = apCanvassHome.findByPrlCodeAndCnvPo(apCheckApPurchaseRequisitionLine.getPrlCode(), EJBCommon.TRUE, AD_CMPNY);
				Iterator apCnvIter = apCanvasses.iterator();
				while (apCnvIter.hasNext()) {
					LocalApCanvass apCanvass = (LocalApCanvass)apCnvIter.next();
					TOTAL_AMOUNT += apCanvass.getCnvAmount();
				}
				if (apCheckApPurchaseRequisitionLine.getApCanvasses().size() == 0 ||
						(apCanvassHome.findByPrlCodeAndCnvPo(apCheckApPurchaseRequisitionLine.getPrlCode(), EJBCommon.TRUE, AD_CMPNY)).size() == 0) {
					throw new GlobalRecordInvalidException(String.valueOf(apCheckApPurchaseRequisitionLine.getPrlLine()));
				}
			}



			LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);

			// set purchase requisition approval status


			String PR_APPRVL_STATUS = null;

			if(!isDraft) {

				LocalAdApproval adApproval = adApprovalHome.findByAprAdCompany(AD_CMPNY);

				// check if ap voucher approval is enabled

				if (adApproval.getAprEnableApCanvass() == EJBCommon.FALSE) {

					PR_APPRVL_STATUS = "N/A";

					System.out.print("APPROVAL SETUP FALSE");

				} else {
					System.out.print("APPROVAL SETUP YES");




					// check if voucher is self approved
					Collection adAmountLimits1 = null;
					LocalAdAmountLimit adAmountLimit = null;

					try {

						adAmountLimits1 = adAmountLimitHome.findByAdcTypeAndAuTypeAndUsrNameAndAmountLimit("AP CANVASS", "REQUESTER", details.getPrLastModifiedBy(), TOTAL_AMOUNT, AD_CMPNY);

					} catch (FinderException ex) {
						System.out.println("ERRORRRRRRRRRRRRRRRRRRR");

						throw new GlobalNoApprovalRequesterFoundException();

					}

					Iterator x = adAmountLimits1.iterator();

					while(x.hasNext()){

						adAmountLimit = (LocalAdAmountLimit)x.next();

						break;
					}



					System.out.print("TOTAL_AMOUNT = "+ TOTAL_AMOUNT);

					System.out.print("AMOUNT LIMIT = "+ adAmountLimit.getCalAmountLimit());
					if (TOTAL_AMOUNT > adAmountLimit.getCalAmountLimit()) {
						System.out.println("AMOUNT YES");
						PR_APPRVL_STATUS = "N/A";

					} else {
						System.out.println("AMOUNT NO");
						// for approval, create approval queue

						Collection adAmountLimits = adAmountLimitHome.findByAdcTypeAndGreaterThanCalAmountLimit("AP CANVASS", adAmountLimit.getCalAmountLimit(), AD_CMPNY);

						if (adAmountLimits.isEmpty()) {

							Collection adApprovalUsers = adApprovalUserHome.findByAuTypeAndCalCode("APPROVER", adAmountLimit.getCalCode(), AD_CMPNY);

							if (adApprovalUsers.isEmpty()) {

								throw new GlobalNoApprovalApproverFoundException();

							}

							Iterator j = adApprovalUsers.iterator();

							while (j.hasNext()) {

								LocalAdApprovalUser adApprovalUser = (LocalAdApprovalUser)j.next();

								LocalAdApprovalQueue adApprovalQueue = adApprovalQueueHome.create(EJBCommon.TRUE, "AP CANVASS", apPurchaseRequisition.getPrCode(),
										apPurchaseRequisition.getPrNumber(), apPurchaseRequisition.getPrDate(), adAmountLimit.getCalAndOr(), adApprovalUser.getAuOr(), AD_BRNCH, AD_CMPNY);

								adApprovalUser.getAdUser().addAdApprovalQueue(adApprovalQueue);

							}

						} else {

							boolean isApprovalUsersFound = false;

							Iterator i = adAmountLimits.iterator();

							while (i.hasNext()) {

								LocalAdAmountLimit adNextAmountLimit = (LocalAdAmountLimit)i.next();

								if (TOTAL_AMOUNT <= adNextAmountLimit.getCalAmountLimit()) {

									Collection adApprovalUsers = adApprovalUserHome.findByAuTypeAndCalCode("APPROVER", adAmountLimit.getCalCode(), AD_CMPNY);

									Iterator j = adApprovalUsers.iterator();

									while (j.hasNext()) {

										isApprovalUsersFound = true;

										LocalAdApprovalUser adApprovalUser = (LocalAdApprovalUser)j.next();

										LocalAdApprovalQueue adApprovalQueue = adApprovalQueueHome.create(EJBCommon.TRUE, "AP CANVASS", apPurchaseRequisition.getPrCode(),
												apPurchaseRequisition.getPrNumber(), apPurchaseRequisition.getPrDate(), adAmountLimit.getCalAndOr(), adApprovalUser.getAuOr(), AD_BRNCH, AD_CMPNY);

										adApprovalUser.getAdUser().addAdApprovalQueue(adApprovalQueue);

									}

									break;

								} else if (!i.hasNext()) {

									Collection adApprovalUsers = adApprovalUserHome.findByAuTypeAndCalCode("APPROVER", adNextAmountLimit.getCalCode(), AD_CMPNY);

									Iterator j = adApprovalUsers.iterator();

									while (j.hasNext()) {

										isApprovalUsersFound = true;

										LocalAdApprovalUser adApprovalUser = (LocalAdApprovalUser)j.next();

										LocalAdApprovalQueue adApprovalQueue = adApprovalQueueHome.create(EJBCommon.TRUE, "AP CANVASS", apPurchaseRequisition.getPrCode(),
												apPurchaseRequisition.getPrNumber(), apPurchaseRequisition.getPrDate(), adNextAmountLimit.getCalAndOr(), adApprovalUser.getAuOr(), AD_BRNCH, AD_CMPNY);

										adApprovalUser.getAdUser().addAdApprovalQueue(adApprovalQueue);

									}

									break;

								}

								adAmountLimit = adNextAmountLimit;

							}

							if (!isApprovalUsersFound) {

								throw new GlobalNoApprovalApproverFoundException();

							}

						}

						PR_APPRVL_STATUS = "PENDING";

					}


				}

				apPurchaseRequisition.setPrCanvassApprovalStatus(PR_APPRVL_STATUS);

				// set post purchase order

				if(PR_APPRVL_STATUS.equals("N/A")) {

				 apPurchaseRequisition.setPrCanvassPosted(EJBCommon.TRUE);
				 apPurchaseRequisition.setPrCanvassPosted(EJBCommon.TRUE);
				 apPurchaseRequisition.setPrCanvassPostedBy(apPurchaseRequisition.getPrLastModifiedBy());
				 apPurchaseRequisition.setPrCanvassDatePosted(EJBCommon.getGcCurrentDateWoTime().getTime());

				 }

			}

 	  	    return apPurchaseRequisition.getPrCode();


		} catch (GlobalRecordAlreadyDeletedException ex) {

			getSessionContext().setRollbackOnly();
			throw ex;

		} catch (GlobalDocumentNumberNotUniqueException ex) {

			getSessionContext().setRollbackOnly();
			throw ex;

		} catch (GlobalConversionDateNotExistException ex) {

			getSessionContext().setRollbackOnly();
			throw ex;

		} catch (GlobalTransactionAlreadyApprovedException ex) {

			getSessionContext().setRollbackOnly();
			throw ex;

		} catch (GlobalTransactionAlreadyPendingException ex) {

			getSessionContext().setRollbackOnly();
			throw ex;

		} catch (GlobalTransactionAlreadyPostedException ex) {

			getSessionContext().setRollbackOnly();
			throw ex;

		} catch (GlobalTransactionAlreadyVoidException ex) {

			getSessionContext().setRollbackOnly();
			throw ex;

		} catch (GlobalInvItemLocationNotFoundException ex) {

			getSessionContext().setRollbackOnly();
			throw ex;

		} catch (GlobalNoApprovalRequesterFoundException ex) {

			getSessionContext().setRollbackOnly();
			throw ex;

		} catch (GlobalNoApprovalApproverFoundException ex) {

			getSessionContext().setRollbackOnly();
			throw ex;

		} catch (GlobalRecordInvalidException ex) {

			getSessionContext().setRollbackOnly();
			throw ex;
		} catch (Exception ex) {

			Debug.printStackTrace(ex);
			getSessionContext().setRollbackOnly();
			throw new EJBException(ex.getMessage());

		}


	}


	//TODO: saveApPrEntry
	/**
	 * @ejb:interface-method view-type="remote"
	 **/
	public Integer saveApPrEntry(com.util.ApPurchaseRequisitionDetails details, String RV_AD_NTFD_USR1, String TC_NM, String FC_NM, ArrayList prlList, boolean isDraft, Integer AD_BRNCH, Integer AD_CMPNY) throws
	GlobalRecordAlreadyDeletedException,
	GlobalDocumentNumberNotUniqueException,
	GlobalConversionDateNotExistException,
	GlobalTransactionAlreadyApprovedException,
	GlobalTransactionAlreadyPendingException,
	GlobalTransactionAlreadyPostedException,
	GlobalTransactionAlreadyVoidException,
	GlobalInvItemLocationNotFoundException,
	GlobalSupplierItemInvalidException,
	GlobalRecordInvalidException,
	GlobalNoApprovalApproverFoundException,
	GlobalNoApprovalRequesterFoundException,
	GlobalInsufficientBudgetQuantityException,
	GlobalSendEmailMessageException{

		Debug.print("ApPurchaseRequisitionEntryControllerBean saveApPrEntry");

		LocalApPurchaseRequisitionHome apPurchaseRequisitionHome = null;
		LocalApPurchaseRequisitionLineHome apPurchaseRequisitionLineHome = null;
		LocalAdCompanyHome adCompanyHome = null;
		LocalAdBranchHome adBranchHome = null;
		LocalInvTransactionalBudgetHome invTransactionalBudgetHome = null;
		LocalAdApprovalUserHome adApprovalUserHome = null;
		LocalAdDocumentSequenceAssignmentHome adDocumentSequenceAssignmentHome = null;
		LocalApSupplierHome apSupplierHome = null;
		LocalApTaxCodeHome apTaxCodeHome = null;
		LocalGlFunctionalCurrencyHome glFunctionalCurrencyHome = null;
		LocalGlFunctionalCurrencyRateHome glFunctionalCurrencyRateHome = null;
		LocalAdPreferenceHome adPreferenceHome = null;
		LocalInvItemLocationHome invItemLocationHome = null;
		LocalInvUnitOfMeasureHome invUnitOfMeasureHome = null;
		LocalAdApprovalHome adApprovalHome = null;
		LocalAdUserHome adUserHome = null;
		LocalAdBranchDocumentSequenceAssignmentHome adBranchDocumentSequenceAssignmentHome = null;
        LocalApPurchaseOrderLineHome apPurchaseOrderLineHome = null;
        LocalAdAmountLimitHome adAmountLimitHome = null;
        LocalApCanvassHome apCanvassHome = null;
        LocalAdApprovalQueueHome adApprovalQueueHome = null;
		LocalApPurchaseRequisition apPurchaseRequisition = null;
		LocalAdLookUpValueHome adLookUpValueHome = null;
		LocalInvTagHome invTagHome = null;
		LocalInvItemHome invItemHome = null;
		LocalInvLocationHome invLocationHome = null;

		ArrayList list = new ArrayList();

		// Initialize EJB Home

		try {

			apPurchaseRequisitionHome = (LocalApPurchaseRequisitionHome)EJBHomeFactory.
				lookUpLocalHome(LocalApPurchaseRequisitionHome.JNDI_NAME, LocalApPurchaseRequisitionHome.class);
			apPurchaseRequisitionLineHome = (LocalApPurchaseRequisitionLineHome)EJBHomeFactory.
				lookUpLocalHome(LocalApPurchaseRequisitionLineHome.JNDI_NAME, LocalApPurchaseRequisitionLineHome.class);
			adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
					lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);

			adBranchHome = (LocalAdBranchHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdBranchHome.JNDI_NAME, LocalAdBranchHome.class);
			adDocumentSequenceAssignmentHome = (LocalAdDocumentSequenceAssignmentHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdDocumentSequenceAssignmentHome.JNDI_NAME, LocalAdDocumentSequenceAssignmentHome.class);
			apSupplierHome = (LocalApSupplierHome)EJBHomeFactory.
				lookUpLocalHome(LocalApSupplierHome.JNDI_NAME, LocalApSupplierHome.class);
			apTaxCodeHome = (LocalApTaxCodeHome)EJBHomeFactory.
				lookUpLocalHome(LocalApTaxCodeHome.JNDI_NAME, LocalApTaxCodeHome.class);
			glFunctionalCurrencyHome = (LocalGlFunctionalCurrencyHome)EJBHomeFactory.
				lookUpLocalHome(LocalGlFunctionalCurrencyHome.JNDI_NAME, LocalGlFunctionalCurrencyHome.class);
			glFunctionalCurrencyRateHome = (LocalGlFunctionalCurrencyRateHome)EJBHomeFactory.
				lookUpLocalHome(LocalGlFunctionalCurrencyRateHome.JNDI_NAME, LocalGlFunctionalCurrencyRateHome.class);
			adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);
			invItemLocationHome = (LocalInvItemLocationHome)EJBHomeFactory.
				lookUpLocalHome(LocalInvItemLocationHome.JNDI_NAME, LocalInvItemLocationHome.class);
			invUnitOfMeasureHome = (LocalInvUnitOfMeasureHome)EJBHomeFactory.
				lookUpLocalHome(LocalInvUnitOfMeasureHome.JNDI_NAME, LocalInvUnitOfMeasureHome.class);
			adApprovalHome = (LocalAdApprovalHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdApprovalHome.JNDI_NAME, LocalAdApprovalHome.class);
			adAmountLimitHome = (LocalAdAmountLimitHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdAmountLimitHome.JNDI_NAME, LocalAdAmountLimitHome.class);

			adApprovalQueueHome = (LocalAdApprovalQueueHome)EJBHomeFactory.
    		lookUpLocalHome(LocalAdApprovalQueueHome.JNDI_NAME, LocalAdApprovalQueueHome.class);
			adUserHome = (LocalAdUserHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdUserHome.JNDI_NAME, LocalAdUserHome.class);


			adBranchDocumentSequenceAssignmentHome = (LocalAdBranchDocumentSequenceAssignmentHome)EJBHomeFactory.
    			lookUpLocalHome(LocalAdBranchDocumentSequenceAssignmentHome.JNDI_NAME, LocalAdBranchDocumentSequenceAssignmentHome.class);
			apPurchaseOrderLineHome = (LocalApPurchaseOrderLineHome)EJBHomeFactory.
				lookUpLocalHome(LocalApPurchaseOrderLineHome.JNDI_NAME, LocalApPurchaseOrderLineHome.class);
			apCanvassHome = (LocalApCanvassHome)EJBHomeFactory.
				lookUpLocalHome(LocalApCanvassHome.JNDI_NAME, LocalApCanvassHome.class);
			invTransactionalBudgetHome = (LocalInvTransactionalBudgetHome)EJBHomeFactory.
				lookUpLocalHome(LocalInvTransactionalBudgetHome.JNDI_NAME, LocalInvTransactionalBudgetHome.class);
			adApprovalUserHome = (LocalAdApprovalUserHome)EJBHomeFactory.
	        		lookUpLocalHome(LocalAdApprovalUserHome.JNDI_NAME, LocalAdApprovalUserHome.class);
			adLookUpValueHome = (LocalAdLookUpValueHome)EJBHomeFactory.
					lookUpLocalHome(LocalAdLookUpValueHome.JNDI_NAME, LocalAdLookUpValueHome.class);
			invTagHome = (LocalInvTagHome)EJBHomeFactory.
					lookUpLocalHome(LocalInvTagHome.JNDI_NAME, LocalInvTagHome.class);
			invItemHome = (LocalInvItemHome)EJBHomeFactory.
					lookUpLocalHome(LocalInvItemHome.JNDI_NAME, LocalInvItemHome.class);
			invLocationHome = (LocalInvLocationHome)EJBHomeFactory.
					lookUpLocalHome(LocalInvLocationHome.JNDI_NAME, LocalInvLocationHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}

		try {

			// validate if purchase requisition is already deleted

			try {

				if (details.getPrCode() != null) {

					apPurchaseRequisition = apPurchaseRequisitionHome.findByPrimaryKey(details.getPrCode());

				}

			} catch (FinderException ex) {

				throw new GlobalRecordAlreadyDeletedException();

			}



			// validate if purchase requisition is already posted, void, approved or pending

			if (details.getPrCode() != null && details.getPrVoid() == EJBCommon.FALSE) {

	        	if (apPurchaseRequisition.getPrApprovalStatus() != null) {

	        		if (apPurchaseRequisition.getPrApprovalStatus().equals("APPROVED") ||
	        				apPurchaseRequisition.getPrApprovalStatus().equals("N/A")) {

	        		    throw new GlobalTransactionAlreadyApprovedException();

	        		} else if (apPurchaseRequisition.getPrApprovalStatus().equals("PENDING")) {

	        			throw new GlobalTransactionAlreadyPendingException();

	        		}
	        	}

        		if (apPurchaseRequisition.getPrPosted() == EJBCommon.TRUE) {

        			throw new GlobalTransactionAlreadyPostedException();

        		} else if (apPurchaseRequisition.getPrVoid() == EJBCommon.TRUE) {

        			throw new GlobalTransactionAlreadyVoidException();

        		}
        	}

			// purchase requisition void

			if (details.getPrCode() != null && details.getPrVoid() == EJBCommon.TRUE) {

				apPurchaseRequisition.setPrVoid(EJBCommon.TRUE);
				apPurchaseRequisition.setPrLastModifiedBy(details.getPrLastModifiedBy());
				apPurchaseRequisition.setPrDateLastModified(details.getPrDateLastModified());

				return apPurchaseRequisition.getPrCode();

			}

			// validate if document number is unique document number is automatic then set next sequence

			LocalAdBranchDocumentSequenceAssignment adBranchDocumentSequenceAssignment = null;
			LocalAdDocumentSequenceAssignment adDocumentSequenceAssignment = null;

			if (details.getPrCode() == null) {

				try {

					adDocumentSequenceAssignment = adDocumentSequenceAssignmentHome.findByDcName("AP PURCHASE REQUISITION", AD_CMPNY);

				} catch(FinderException ex) { }

				try {

					adBranchDocumentSequenceAssignment = adBranchDocumentSequenceAssignmentHome.findBdsByDsaCodeAndBrCode(adDocumentSequenceAssignment.getDsaCode(), AD_BRNCH, AD_CMPNY);

				} catch (FinderException ex) { }

				LocalApPurchaseRequisition apExistingPurchaseRequisition = null;

				try {

					apExistingPurchaseRequisition = apPurchaseRequisitionHome.findByPrNumberAndBrCode(
							details.getPrNumber(), AD_BRNCH, AD_CMPNY);

				} catch (FinderException ex) {
				}

				if (apExistingPurchaseRequisition != null) {

					throw new GlobalDocumentNumberNotUniqueException();

				}

				if (adDocumentSequenceAssignment.getAdDocumentSequence().getDsNumberingType() == 'A' &&
						(details.getPrNumber() == null || details.getPrNumber().trim().length() == 0)) {

					while (true) {

						if (adBranchDocumentSequenceAssignment == null || adBranchDocumentSequenceAssignment.getBdsNextSequence() == null) {

							try {

								apPurchaseRequisitionHome.findByPrNumberAndBrCode(adDocumentSequenceAssignment.getDsaNextSequence(), AD_BRNCH, AD_CMPNY);
								adDocumentSequenceAssignment.setDsaNextSequence(EJBCommon.incrementStringNumber(adDocumentSequenceAssignment.getDsaNextSequence()));

							} catch (FinderException ex) {

								details.setPrNumber(adDocumentSequenceAssignment.getDsaNextSequence());
								adDocumentSequenceAssignment.setDsaNextSequence(EJBCommon.incrementStringNumber(adDocumentSequenceAssignment.getDsaNextSequence()));
								break;

							}

						} else {

							try {

								apPurchaseRequisitionHome.findByPrNumberAndBrCode(adBranchDocumentSequenceAssignment.getBdsNextSequence(), AD_BRNCH, AD_CMPNY);
								adBranchDocumentSequenceAssignment.setBdsNextSequence(EJBCommon.incrementStringNumber(adBranchDocumentSequenceAssignment.getBdsNextSequence()));

							} catch (FinderException ex) {

								details.setPrNumber(adBranchDocumentSequenceAssignment.getBdsNextSequence());
								adBranchDocumentSequenceAssignment.setBdsNextSequence(EJBCommon.incrementStringNumber(adBranchDocumentSequenceAssignment.getBdsNextSequence()));
								break;

							}

						}

					}

				}

			} else {

				LocalApPurchaseRequisition apExistingPurchaseRequisition = null;

				try {

					apExistingPurchaseRequisition = apPurchaseRequisitionHome.findByPrNumberAndBrCode(
							details.getPrNumber(), AD_BRNCH, AD_CMPNY);

				} catch (FinderException ex) {
				}

				if (apExistingPurchaseRequisition != null &&
						!apExistingPurchaseRequisition.getPrCode().equals(details.getPrCode())) {

					throw new GlobalDocumentNumberNotUniqueException();

				}

				if (apPurchaseRequisition.getPrNumber() != details.getPrNumber() &&
						(details.getPrNumber() == null || details.getPrNumber().trim().length() == 0)) {

					details.setPrNumber(apPurchaseRequisition.getPrNumber());

				}

			}

			// validate if conversion date exists

            try {

                if (details.getPrConversionDate() != null) {


                    LocalGlFunctionalCurrency glValidateFunctionalCurrency = glFunctionalCurrencyHome.findByFcName(FC_NM, AD_CMPNY);
                    LocalGlFunctionalCurrencyRate glFunctionalCurrencyRate =
                        glFunctionalCurrencyRateHome.findByFcCodeAndDate(glValidateFunctionalCurrency.getFcCode(),
                                details.getPrConversionDate(), AD_CMPNY);

                    details.setPrConversionRate(glFunctionalCurrencyRate.getFrXToUsd());
                }

            } catch (FinderException ex) {

                throw new GlobalConversionDateNotExistException();

            }

			// get all notified users

		    LocalAdUser userName1 = null;

		    try {

		    	userName1 = adUserHome.findByUsrName(RV_AD_NTFD_USR1, AD_CMPNY);

		    } catch (FinderException ex) {

		    }


			boolean isRecalculate = true;

			// create purchase requisition

			if (details.getPrCode() == null) {

				apPurchaseRequisition = apPurchaseRequisitionHome.create(details.getPrDescription(),
						details.getPrNumber(), details.getPrDate(), details.getPrDeliveryPeriod(), details.getPrReferenceNumber(),
						details.getPrConversionDate(), details.getPrConversionRate(), null, EJBCommon.FALSE, EJBCommon.FALSE,  null, EJBCommon.FALSE,EJBCommon.FALSE, null, null,
						details.getPrCreatedBy(), details.getPrDateCreated(), details.getPrLastModifiedBy(),
						details.getPrDateLastModified(), details.getPrApprovedRejectedBy(), details.getPrDateApprovedRejected(),details.getPrPostedBy(), details.getPrDatePosted(),
						null,null,null,null,
						details.getPrTagStatus(),details.getPrType(),
						userName1 != null ? userName1.getUsrCode() : null,details.getPrSchedule(), details.getPrNextRunDate(), details.getPrLastRunDate(),
						AD_BRNCH, AD_CMPNY);


  			} else {
  
  				// check if critical fields are changed
  
  				if (!apPurchaseRequisition.getApTaxCode().getTcName().equals(TC_NM) ||
  						prlList.size() != apPurchaseRequisition.getApPurchaseRequisitionLines().size()) {
  
  					isRecalculate = true;
  
  				} else if (prlList.size() == apPurchaseRequisition.getApPurchaseRequisitionLines().size()) {
  
  					Iterator ilIter = apPurchaseRequisition.getApPurchaseRequisitionLines().iterator();
  					Iterator ilListIter = prlList.iterator();
  
  					while (ilIter.hasNext()) {
  
  						LocalApPurchaseRequisitionLine apPurchaseRequisitionLine = (LocalApPurchaseRequisitionLine)ilIter.next();
  						ApModPurchaseRequisitionLineDetails mdetails = (ApModPurchaseRequisitionLineDetails)ilListIter.next();
  
  						if (!apPurchaseRequisitionLine.getInvItemLocation().getInvItem().getIiName().equals(mdetails.getPrlIlIiName()) ||
  								!apPurchaseRequisitionLine.getInvItemLocation().getInvItem().getIiDescription().equals(mdetails.getPrlIlIiDescription()) ||
  								!apPurchaseRequisitionLine.getInvItemLocation().getInvLocation().getLocName().equals(mdetails.getPrlIlLocName()) ||
  								!apPurchaseRequisitionLine.getInvUnitOfMeasure().getUomName().equals(mdetails.getPrlUomName()) ||
  								apPurchaseRequisitionLine.getPrlQuantity() != mdetails.getPrlQuantity() ||
  								apPurchaseRequisitionLine.getPrlAmount() != mdetails.getPrlAmount()) {
  
  					//		isRecalculate = true;
  
  						}
  
  					}
  
  				} else {
  
  				//	isRecalculate = false;
  
  				}
  
  				apPurchaseRequisition.setPrDescription(details.getPrDescription());
  				apPurchaseRequisition.setPrDate(details.getPrDate());
  				apPurchaseRequisition.setPrDeliveryPeriod(details.getPrDeliveryPeriod());
  				apPurchaseRequisition.setPrNumber(details.getPrNumber());
  				apPurchaseRequisition.setPrReferenceNumber(details.getPrReferenceNumber());
  				apPurchaseRequisition.setPrVoid(details.getPrVoid());
  				apPurchaseRequisition.setPrConversionDate(details.getPrConversionDate());
  				apPurchaseRequisition.setPrConversionRate(details.getPrConversionRate());
  				apPurchaseRequisition.setPrTagStatus(details.getPrTagStatus());
  				apPurchaseRequisition.setPrType(details.getPrType());
  				apPurchaseRequisition.setPrAdUserName1(userName1 != null ? userName1.getUsrCode() : null);
  				apPurchaseRequisition.setPrSchedule(details.getPrSchedule());
  				apPurchaseRequisition.setPrNextRunDate(details.getPrNextRunDate());
  				apPurchaseRequisition.setPrLastModifiedBy(details.getPrLastModifiedBy());
  				apPurchaseRequisition.setPrDateLastModified(details.getPrDateLastModified());
  				apPurchaseRequisition.setPrCanvassReasonForRejection(null);
  				apPurchaseRequisition.setPrReasonForRejection(null);
  
  			}

			System.out.println("TC_NM="+TC_NM);
			System.out.println("FC_NM="+FC_NM);
			LocalApTaxCode apTaxCode = apTaxCodeHome.findByTcName(TC_NM, AD_CMPNY);
			apPurchaseRequisition.setApTaxCode(apTaxCode);

			LocalGlFunctionalCurrency glFunctionalCurrency = glFunctionalCurrencyHome.findByFcName(FC_NM, AD_CMPNY);
			apPurchaseRequisition.setGlFunctionalCurrency(glFunctionalCurrency);

			double TOTAL_AMOUNT = 0d;

			if (isRecalculate) {
				System.out.println("isRecalculated");
				// remove purchase requisition lines

				Collection apPurchaseRequisitionLines = apPurchaseRequisition.getApPurchaseRequisitionLines();

				Iterator i = apPurchaseRequisitionLines.iterator();

				while (i.hasNext()) {

					LocalApPurchaseRequisitionLine apPurchaseRequisitionLine = (LocalApPurchaseRequisitionLine)i.next();

					//remove all inv tag inside PR line
		  	   	    Collection invTags = apPurchaseRequisitionLine.getInvTags();

		  	   	    Iterator x = invTags.iterator();

		  	   	    while (x.hasNext()){

		  	   	    	LocalInvTag invTag = (LocalInvTag)x.next();

		  	   	    	x.remove();

		  	   	    	invTag.remove();
		  	   	    }

					i.remove();

					apPurchaseRequisitionLine.remove();

				}

				// add new purchase requisition lines

				i = prlList.iterator();

				LocalInvItemLocation invItemLocation = null;

				while (i.hasNext()) {

					ApModPurchaseRequisitionLineDetails mPrlDetails = (ApModPurchaseRequisitionLineDetails) i.next();

					System.out.println(mPrlDetails.getPrlTagList() + "<== mPrlDetails.getPrlTagList() inside controller bean");
					// save purchase requisition line, qty and amount based on the retrieved unit cost

					if (mPrlDetails.getPrlAmount()==0) {
						mPrlDetails.setPrlAmount(invItemHome.findByIiName(mPrlDetails.getPrlIlIiName(), AD_CMPNY).getIiUnitCost()*mPrlDetails.getPrlQuantity());
					}

					LocalApPurchaseRequisitionLine apPurchaseRequisitionLine = apPurchaseRequisitionLineHome.create(mPrlDetails.getPrlLine(), mPrlDetails.getPrlQuantity(), mPrlDetails.getPrlAmount(), AD_CMPNY);
					apPurchaseRequisition.addApPurchaseRequisitionLine(apPurchaseRequisitionLine);


					try {

						invItemLocation = invItemLocationHome.findByLocNameAndIiName(
								mPrlDetails.getPrlIlLocName(), mPrlDetails.getPrlIlIiName(), AD_CMPNY);

					} catch (FinderException ex) {

						throw new GlobalInvItemLocationNotFoundException(String.valueOf(mPrlDetails.getPrlLine()));

					}


					invItemLocation.addApPurchaseRequisitionLine(apPurchaseRequisitionLine);

					LocalInvUnitOfMeasure invUnitOfMeasure = invUnitOfMeasureHome.findByUomName(mPrlDetails.getPrlUomName(), AD_CMPNY);
					invUnitOfMeasure.addApPurchaseRequisitionLine(apPurchaseRequisitionLine);

					short CNV_LN = 0;
					double II_UNT_CST = mPrlDetails.getPrlAmount();
					double CNV_QTY = mPrlDetails.getPrlQuantity();


					if(mPrlDetails.getRegenerateCanvass() == true) {
					
					   /*

						if (invItemLocation.getInvItem().getApSupplier() != null) {

							//generate canvass lines for purchase requisition line
							CNV_LN++;
							
							//create canvass line
							LocalApCanvass apCanvass = apCanvassHome.create(CNV_LN,null, CNV_QTY, II_UNT_CST, CNV_QTY * II_UNT_CST, EJBCommon.TRUE, AD_CMPNY);

							LocalApSupplier apSupplier = invItemLocation.getInvItem().getApSupplier();
							
						
							apSupplier.addApCanvass(apCanvass);

							apPurchaseRequisitionLine.addApCanvass(apCanvass);

						}

						CNV_LN++;

						//create canvass lines for other suppliers of item
						Collection apPurchaseOrderLines = null;

						try {

							apPurchaseOrderLines = apPurchaseOrderLineHome.findByPlIlCodeAndPoReceivingAndPoPostedAndBrCode(apPurchaseRequisitionLine.getInvItemLocation().getIlCode(), EJBCommon.TRUE, EJBCommon.TRUE, AD_BRNCH, AD_CMPNY);
                            
						} catch(FinderException ex) {

						}

						if(!apPurchaseOrderLines.isEmpty() && apPurchaseOrderLines.size() > 0) {

							Iterator plIter = apPurchaseOrderLines.iterator();

							while(plIter.hasNext()) {

								LocalApPurchaseOrderLine apPurchaseOrderLine = (LocalApPurchaseOrderLine)plIter.next();


								// continue if next purchase order line has existing supplier in canvass

								Collection apCanvasses = apCanvassHome.findByPrlCodeAndSplSupplierCode(apPurchaseRequisitionLine.getPrlCode(),
										apPurchaseOrderLine.getApPurchaseOrder().getApSupplier().getSplSupplierCode(), AD_CMPNY);

                                
                               System.out.println("canv: " + apCanvasses.size());


								if(!apCanvasses.isEmpty()) continue;

								// convert purchase order line unit cost
								double POL_UNT_CST = this.convertCostByUom(apPurchaseOrderLine.getInvItemLocation().getInvItem().getIiName(),
										apPurchaseOrderLine.getInvUnitOfMeasure().getUomName(),
										apPurchaseOrderLine.getPlUnitCost(), false, AD_CMPNY);
								POL_UNT_CST = this.convertCostByUom(apPurchaseOrderLine.getInvItemLocation().getInvItem().getIiName(),
										apPurchaseRequisitionLine.getInvUnitOfMeasure().getUomName(),
										POL_UNT_CST, true, AD_CMPNY);

								//create canvass line
								LocalApCanvass apCanvass = apCanvassHome.create(CNV_LN,null, apPurchaseRequisitionLine.getPrlQuantity(), POL_UNT_CST, apPurchaseRequisitionLine.getPrlQuantity() * POL_UNT_CST, EJBCommon.FALSE, AD_CMPNY);

								if (CNV_LN == 1) {

									apCanvass.setCnvQuantity(CNV_QTY);
									apCanvass.setCnvAmount(II_UNT_CST);
									apCanvass.setCnvPo(EJBCommon.TRUE);

								}

								LocalApSupplier apSupplier = apPurchaseOrderLine.getApPurchaseOrder().getApSupplier();
								apSupplier.addApCanvass(apCanvass);

								apPurchaseRequisitionLine.addApCanvass(apCanvass);

								CNV_LN++;

							}

						}
                         
					} else {

						CNV_LN++;
						ArrayList cnvList = mPrlDetails.getCnvList();
						
						
                     
                   
						Iterator cnvIter = cnvList.iterator();

						while(cnvIter.hasNext()) {

							ApModCanvassDetails cnvDetails = (ApModCanvassDetails)cnvIter.next();
							
							
						
                  
							//create canvass line
							LocalApCanvass apCanvass = apCanvassHome.create(CNV_LN, cnvDetails.getCnvRemarks(),
									cnvDetails.getCnvQuantity(), cnvDetails.getCnvUnitCost(),
									EJBCommon.roundIt(cnvDetails.getCnvUnitCost() * cnvDetails.getCnvQuantity(), this.getGlFcPrecisionUnit(AD_CMPNY)),
									cnvDetails.getCnvPo(), AD_CMPNY);

							LocalApSupplier apSupplier = apSupplierHome.findBySplSupplierCode(cnvDetails.getCnvSplSupplierCode(), AD_CMPNY);
							apSupplier.addApCanvass(apCanvass);

							apPurchaseRequisitionLine.addApCanvass(apCanvass);

							CNV_LN++;

						}

					}
					
					
					*/
					

					}
					
					
					
                        CNV_LN++;
                        ArrayList cnvList = mPrlDetails.getCnvList();
                        
                        
                     
                   
                        Iterator cnvIter = cnvList.iterator();

                        while(cnvIter.hasNext()) {

                            ApModCanvassDetails cnvDetails = (ApModCanvassDetails)cnvIter.next();
                            
                            
                        
                  
                            //create canvass line
                            LocalApCanvass apCanvass = apCanvassHome.create(CNV_LN, cnvDetails.getCnvRemarks(),
                                    cnvDetails.getCnvQuantity(), cnvDetails.getCnvUnitCost(),
                                    EJBCommon.roundIt(cnvDetails.getCnvUnitCost() * cnvDetails.getCnvQuantity(), this.getGlFcPrecisionUnit(AD_CMPNY)),
                                    cnvDetails.getCnvPo(), AD_CMPNY);

                            LocalApSupplier apSupplier = apSupplierHome.findBySplSupplierCode(cnvDetails.getCnvSplSupplierCode(), AD_CMPNY);
                            apSupplier.addApCanvass(apCanvass);

                            apPurchaseRequisitionLine.addApCanvass(apCanvass);

                            CNV_LN++;

                        }
                    
					//TODO: add new inv Tag
					if (mPrlDetails.getTraceMisc() == 1){
		      	  	    try{
		      	  	    	System.out.println("aabot?");
		      	  	    	System.out.println(mPrlDetails.getPrlTagList() + "<== mPrlDetails.getPrlTagList");
			      	  	    Iterator t = mPrlDetails.getPrlTagList().iterator();

			      	  	    LocalInvTag invTag  = null;
			      	  	    System.out.println(t + "<== mPrlDetails.getPrlTagList().iterator()");
			      	  	    while (t.hasNext()){
			      	  	    	InvModTagListDetails tgLstDetails = (InvModTagListDetails)t.next();
			      	  	    	System.out.println(tgLstDetails.getTgCustodian() + "<== custodian");
			      	  	    	System.out.println(tgLstDetails.getTgSpecs() + "<== specs");
			      	  	    	System.out.println(tgLstDetails.getTgPropertyCode() + "<== propertyCode");
			      	  	    	System.out.println(tgLstDetails.getTgExpiryDate() + "<== expiryDate");
			      	  	    	System.out.println(tgLstDetails.getTgSerialNumber() + "<== serial number");

			      	  	    	if (tgLstDetails.getTgCode()==null){
			      	  	    		System.out.println("ngcreate ba?");
				      	  	    	invTag = invTagHome.create(tgLstDetails.getTgPropertyCode(),
				      	  	    			tgLstDetails.getTgSerialNumber(),null,tgLstDetails.getTgExpiryDate(),
				      	  	    			tgLstDetails.getTgSpecs(), AD_CMPNY, tgLstDetails.getTgTransactionDate(),
				      	  	    			tgLstDetails.getTgType());

				      	  	    	invTag.setApPurchaseRequisitionLine(apPurchaseRequisitionLine);
				      	  	    	LocalAdUser adUser = null;
				      	  	    	try {
				      	  	    		adUser = adUserHome.findByUsrName(tgLstDetails.getTgCustodian(), AD_CMPNY);
				      	  	    	}catch(FinderException ex){

				      	  	    	}
				      	  	    	invTag.setAdUser(adUser);
				      	  	    	System.out.println("ngcreate ba?");
			      	  	    	}

			      	  	    }

		      	  	    }catch(Exception ex){

		      	  	    }
					}
				}

			}else {
				System.out.println("is not recalculated");
				Iterator y = prlList.iterator();
				System.out.println(prlList + "<== prlList");
				while (y.hasNext()) {
					ApModPurchaseRequisitionLineDetails mPrlDetails = (ApModPurchaseRequisitionLineDetails) y.next();
					System.out.println(mPrlDetails + "<== mPrlDetails");
					LocalApPurchaseRequisitionLine apPurchaseRequisitionLine = apPurchaseRequisitionLineHome.findByPrimaryKey(mPrlDetails.getPrlCode());

					//remove all inv tag inside PR line
		  	   	    Collection invTags = apPurchaseRequisitionLine.getInvTags();

		  	   	    Iterator x = invTags.iterator();

		  	   	    while (x.hasNext()){

		  	   	    	LocalInvTag invTag = (LocalInvTag)x.next();

		  	   	    	x.remove();

		  	   	    	invTag.remove();
		  	   	    }

	      	  	    System.out.println("mgcecreate ng bago?");
	      	  	    System.out.println(mPrlDetails + "<== mPrlDetails");
	      	  	    System.out.println(mPrlDetails.getPrlTagList() + "<== mPrlDetails.getPrlTagList()");
	      	  	    if (mPrlDetails.getTraceMisc() == 1){
		      	  	    Iterator t = mPrlDetails.getPrlTagList().iterator();
		      	  	    while (t.hasNext()){
		      	  	    	InvModTagListDetails tgLstDetails = (InvModTagListDetails)t.next();
		      	  	    	System.out.println(tgLstDetails.getTgCustodian() + "<== custodian");
		      	  	    	System.out.println(tgLstDetails.getTgSpecs() + "<== specs");
		      	  	    	System.out.println(tgLstDetails.getTgPropertyCode() + "<== propertyCode");
		      	  	    	System.out.println(tgLstDetails.getTgExpiryDate() + "<== expiryDate");
	      	  	    		System.out.println("ngcreate ng bago?");
	      	  	    	    LocalInvTag invTag = invTagHome.create(tgLstDetails.getTgPropertyCode(),
		      	  	    			tgLstDetails.getTgSerialNumber(),null,tgLstDetails.getTgExpiryDate(),
		      	  	    			tgLstDetails.getTgSpecs(), AD_CMPNY, tgLstDetails.getTgTransactionDate(),
		      	  	    			tgLstDetails.getTgType());

		      	  	    	invTag.setApPurchaseRequisitionLine(apPurchaseRequisitionLine);
		      	  	    	LocalAdUser adUser = null;
		      	  	    	try {
		      	  	    		adUser = adUserHome.findByUsrName(tgLstDetails.getTgCustodian(), AD_CMPNY);
		      	  	    	}catch(FinderException ex){

		      	  	    	}
		      	  	    	invTag.setAdUser(adUser);
		      	  	    	System.out.println("ngcreate ba?");

		      	  	    }
	      	  	    }
				}
			}


			Collection apCheckPurchaseRequisitionLines = apPurchaseRequisition.getApPurchaseRequisitionLines();

			Iterator checkIter = apCheckPurchaseRequisitionLines.iterator();

			while (checkIter.hasNext()) {
				LocalApPurchaseRequisitionLine apCheckApPurchaseRequisitionLine = (LocalApPurchaseRequisitionLine)checkIter.next();

				// validate if enough budget quantity
				if (!isDraft) {
					try {
						java.text.SimpleDateFormat sdf = new java.text.SimpleDateFormat("yyyy");
						LocalAdUser adUser = adUserHome.findByUsrName(apCheckApPurchaseRequisitionLine.getApPurchaseRequisition().getPrCreatedBy(), AD_CMPNY);
						LocalAdLookUpValue adLookUpValue = adLookUpValueHome.findByLuNameAndLvName("AD DEPARTMENT", adUser.getUsrDept(), AD_CMPNY);
						System.out.println("itemName-" + apCheckApPurchaseRequisitionLine.getInvItemLocation().getInvItem().getIiName());
						System.out.println("adLookUpValue.getLvCode()-" + adLookUpValue.getLvCode());
						System.out.println("Integer.parseInt(sdfYear.format(date))-" + Integer.parseInt(sdf.format(apCheckApPurchaseRequisitionLine.getApPurchaseRequisition().getPrDate())));
						LocalInvTransactionalBudget invTransactionalBudget = invTransactionalBudgetHome.findByTbItemNameAndTbDeptAndTbYear(apCheckApPurchaseRequisitionLine.getInvItemLocation().getInvItem().getIiName(), adLookUpValue.getLvCode(), Integer.parseInt(sdf.format(apCheckApPurchaseRequisitionLine.getApPurchaseRequisition().getPrDate())), AD_CMPNY);
						sdf = new java.text.SimpleDateFormat("MM");
						String month = sdf.format(apCheckApPurchaseRequisitionLine.getApPurchaseRequisition().getPrDate());
						double quantityForCurrentMonth = 0;
						System.out.println("month" + month);
						if (month.equals("01")){
							quantityForCurrentMonth = invTransactionalBudget.getTbQuantityJan();
						}else if (month.equals("02")){
							quantityForCurrentMonth = invTransactionalBudget.getTbQuantityFeb();
						}else if (month.equals("03")){
							quantityForCurrentMonth = invTransactionalBudget.getTbQuantityMrch();
						}else if (month.equals("04")){
							quantityForCurrentMonth = invTransactionalBudget.getTbQuantityAprl();
						}else if (month.equals("05")){
							quantityForCurrentMonth = invTransactionalBudget.getTbQuantityMay();
						}else if (month.equals("06")){
							quantityForCurrentMonth = invTransactionalBudget.getTbQuantityJun();
						}else if (month.equals("07")){
							quantityForCurrentMonth = invTransactionalBudget.getTbQuantityJul();
						}else if (month.equals("08")){
							quantityForCurrentMonth = invTransactionalBudget.getTbQuantityAug();
						}else if (month.equals("09")){
							quantityForCurrentMonth = invTransactionalBudget.getTbQuantitySep();
						}else if (month.equals("10")){
							quantityForCurrentMonth = invTransactionalBudget.getTbQuantityOct();
						}else if (month.equals("11")){
							quantityForCurrentMonth = invTransactionalBudget.getTbQuantityNov();
						}else {
							quantityForCurrentMonth = invTransactionalBudget.getTbQuantityDec();
						}
						System.out.println("quantityForCurrentMonth" + quantityForCurrentMonth);
						System.out.println("apCheckApPurchaseRequisitionLine" + apCheckApPurchaseRequisitionLine.getPrlQuantity());
						if (apCheckApPurchaseRequisitionLine.getPrlQuantity() > quantityForCurrentMonth) throw new GlobalInsufficientBudgetQuantityException(String.valueOf(apCheckApPurchaseRequisitionLine.getPrlLine()));
					} catch (FinderException ex) {}
				}

			}

			LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);

			LocalAdUser adUser = adUserHome.findByUsrName(apPurchaseRequisition.getPrCreatedBy(), AD_CMPNY);

			// set purchase requisition approval status
			Double ABS_TOTAL_AMOUNT = 0d;
			String PR_APPRVL_STATUS = null;

			if(!isDraft) {

				LocalAdApproval adApproval = adApprovalHome.findByAprAdCompany(AD_CMPNY);

				// check if pr approval is enabled

				if (adApproval.getAprEnableApPurReq() == EJBCommon.FALSE) {

					PR_APPRVL_STATUS = "N/A";

				} else {


					// check if pr is self approved

					LocalAdAmountLimit adAmountLimit = null;

					try {

						adAmountLimit = adAmountLimitHome.findByCalDeptAdcTypeAndAuTypeAndUsrName(adUser.getUsrDept(),"AP PURCHASE REQUISITION", "REQUESTER", details.getPrLastModifiedBy(), AD_CMPNY);

					} catch (FinderException ex) {

						throw new GlobalNoApprovalRequesterFoundException();

					}

					if (!(ABS_TOTAL_AMOUNT <= adAmountLimit.getCalAmountLimit())) {

						PR_APPRVL_STATUS = "N/A";

					} else {

						// for approval, create approval queue



						Collection adAmountLimits = adAmountLimitHome.findByAdcTypeAndGreaterThanCalDeptAndCalAmountLimit(adUser.getUsrDept(),"AP PURCHASE REQUISITION", adAmountLimit.getCalAmountLimit(), AD_CMPNY);



						if (adAmountLimits.isEmpty()) {

							Collection adApprovalUsers = adApprovalUserHome.findByCalDeptAndAuTypeAndCalCode(adUser.getUsrDept(),"APPROVER", adAmountLimit.getCalCode(), AD_CMPNY);

							if (adApprovalUsers.isEmpty()) {

								throw new GlobalNoApprovalApproverFoundException();

							}

							Iterator j = adApprovalUsers.iterator();

							while (j.hasNext()) {

								LocalAdApprovalUser adApprovalUser = (LocalAdApprovalUser)j.next();

								LocalAdApprovalQueue adApprovalQueue = adApprovalQueueHome.create(adUser.getUsrDept(),adApprovalUser.getAuLevel(), EJBCommon.FALSE, EJBCommon.incrementStringNumber(adApprovalUser.getAuLevel()),"AP PURCHASE REQUISITION", apPurchaseRequisition.getPrCode(),
										apPurchaseRequisition.getPrNumber(), apPurchaseRequisition.getPrDate(), null, adAmountLimit.getCalAndOr(), adApprovalUser.getAuOr(), EJBCommon.FALSE, adUser.getUsrDescription(),
												AD_BRNCH, AD_CMPNY);

								adApprovalUser.getAdUser().addAdApprovalQueue(adApprovalQueue);

								if(adApprovalUser.getAuLevel().equals("LEVEL 1")) {
									adApprovalQueue.setAqForApproval(EJBCommon.TRUE);
									if((!adApprovalQueue.getAdUser().getUsrEmailAddress().equals("")) && (adPreference.getPrfAdEnableEmailNotification() == EJBCommon.TRUE)) {
										
										
										this.sendEmail(adApprovalQueue, AD_CMPNY);
									}

								}

							}

						} else {

							boolean isApprovalUsersFound = false;

							Iterator i = adAmountLimits.iterator();

							while (i.hasNext()) {

								LocalAdAmountLimit adNextAmountLimit = (LocalAdAmountLimit)i.next();

								if (ABS_TOTAL_AMOUNT <= adNextAmountLimit.getCalAmountLimit()) {

									Collection adApprovalUsers = adApprovalUserHome.findByCalDeptAndAuTypeAndCalCode(adUser.getUsrDept(),"APPROVER", adAmountLimit.getCalCode(), AD_CMPNY);

									Iterator j = adApprovalUsers.iterator();

									while (j.hasNext()) {

										isApprovalUsersFound = true;

										LocalAdApprovalUser adApprovalUser = (LocalAdApprovalUser)j.next();

										LocalAdApprovalQueue adApprovalQueue = adApprovalQueueHome.create(adUser.getUsrDept(), adApprovalUser.getAuLevel(), EJBCommon.FALSE, EJBCommon.incrementStringNumber(adApprovalUser.getAuLevel()), "AP PURCHASE REQUISITION", apPurchaseRequisition.getPrCode(),
												apPurchaseRequisition.getPrNumber(), apPurchaseRequisition.getPrDate(), null, adAmountLimit.getCalAndOr(), adApprovalUser.getAuOr(), EJBCommon.FALSE, adUser.getUsrDescription(),
														AD_BRNCH, AD_CMPNY);

										adApprovalUser.getAdUser().addAdApprovalQueue(adApprovalQueue);

										if(adApprovalUser.getAuLevel().equals("LEVEL 1")) {
											adApprovalQueue.setAqForApproval(EJBCommon.TRUE);
											if((!adApprovalQueue.getAdUser().getUsrEmailAddress().equals("")) && (adPreference.getPrfAdEnableEmailNotification() == EJBCommon.TRUE))  {
												
												this.sendEmail(adApprovalQueue, AD_CMPNY);
											}

										}
									}

									break;

								} else if (!i.hasNext()) {

									Collection adApprovalUsers = adApprovalUserHome.findByCalDeptAndAuTypeAndCalCode(adUser.getUsrDept(),"APPROVER", adNextAmountLimit.getCalCode(), AD_CMPNY);

									Iterator j = adApprovalUsers.iterator();

									while (j.hasNext()) {

										isApprovalUsersFound = true;

										LocalAdApprovalUser adApprovalUser = (LocalAdApprovalUser)j.next();

										LocalAdApprovalQueue adApprovalQueue = adApprovalQueueHome.create(adUser.getUsrDept(), adApprovalUser.getAuLevel(), EJBCommon.FALSE, EJBCommon.incrementStringNumber(adApprovalUser.getAuLevel()) ,"AP PURCHASE REQUISITION", apPurchaseRequisition.getPrCode(),
												apPurchaseRequisition.getPrNumber(), apPurchaseRequisition.getPrDate(), null, adNextAmountLimit.getCalAndOr(), adApprovalUser.getAuOr(), EJBCommon.FALSE, adUser.getUsrDescription(),
														AD_BRNCH, AD_CMPNY);

										adApprovalUser.getAdUser().addAdApprovalQueue(adApprovalQueue);

										if(adApprovalUser.getAuLevel().equals("LEVEL 1")) {
											adApprovalQueue.setAqForApproval(EJBCommon.TRUE);
											if((!adApprovalQueue.getAdUser().getUsrEmailAddress().equals("")) && (adPreference.getPrfAdEnableEmailNotification() == EJBCommon.TRUE)) {
												
												
										
													this.sendEmail(adApprovalQueue, AD_CMPNY);
											
												
											}

										}
									}

									break;

								}

								adAmountLimit = adNextAmountLimit;

							}

							if (!isApprovalUsersFound) {

								throw new GlobalNoApprovalApproverFoundException();

							}

						}

						PR_APPRVL_STATUS = "PENDING";

					}

				}



				apPurchaseRequisition.setPrApprovalStatus(PR_APPRVL_STATUS);

				// set post purchase order

				if(PR_APPRVL_STATUS.equals("N/A")) {

					 apPurchaseRequisition.setPrPosted(EJBCommon.TRUE);
					 apPurchaseRequisition.setPrGenerated(EJBCommon.FALSE);
					 apPurchaseRequisition.setPrPostedBy(apPurchaseRequisition.getPrLastModifiedBy());
					 apPurchaseRequisition.setPrDatePosted(EJBCommon.getGcCurrentDateWoTime().getTime());

				 }

			}

 	  	    return apPurchaseRequisition.getPrCode();


		} catch (GlobalRecordAlreadyDeletedException ex) {

			getSessionContext().setRollbackOnly();
			throw ex;

		} catch (GlobalDocumentNumberNotUniqueException ex) {

			getSessionContext().setRollbackOnly();
			throw ex;

		} catch (GlobalConversionDateNotExistException ex) {

			getSessionContext().setRollbackOnly();
			throw ex;

		} catch (GlobalTransactionAlreadyApprovedException ex) {

			getSessionContext().setRollbackOnly();
			throw ex;

		} catch (GlobalTransactionAlreadyPendingException ex) {

			getSessionContext().setRollbackOnly();
			throw ex;

		} catch (GlobalTransactionAlreadyPostedException ex) {

			getSessionContext().setRollbackOnly();
			throw ex;

		} catch (GlobalTransactionAlreadyVoidException ex) {

			getSessionContext().setRollbackOnly();
			throw ex;

		} catch (GlobalInvItemLocationNotFoundException ex) {

			getSessionContext().setRollbackOnly();
			throw ex;

		} catch (GlobalInsufficientBudgetQuantityException ex) {

			getSessionContext().setRollbackOnly();
			throw ex;

		} catch (GlobalNoApprovalApproverFoundException ex) {

			getSessionContext().setRollbackOnly();
			throw ex;

		} catch (GlobalNoApprovalRequesterFoundException ex) {

			getSessionContext().setRollbackOnly();
			throw ex;

		} catch (GlobalSendEmailMessageException ex) {

			getSessionContext().setRollbackOnly();
			throw ex;

		} catch (Exception ex) {

			Debug.printStackTrace(ex);
			getSessionContext().setRollbackOnly();
			throw new EJBException(ex.getMessage());

		}


	}




	/**
	 * @ejb:interface-method view-type="remote"
	 **/
	public void deleteApPrEntry(Integer PR_CODE, String AD_USR, Integer AD_CMPNY) throws
	GlobalRecordAlreadyDeletedException {

		Debug.print("ApPurchaseRequisitionEntryControllerBean deleteApPrEntry");

		LocalApPurchaseRequisitionHome apPurchaseRequisitionHome = null;
		LocalAdApprovalQueueHome adApprovalQueueHome = null;
		LocalAdDeleteAuditTrailHome adDeleteAuditTrailHome = null;

		// Initialize EJB Home

		try {

			apPurchaseRequisitionHome = (LocalApPurchaseRequisitionHome)EJBHomeFactory.
				lookUpLocalHome(LocalApPurchaseRequisitionHome.JNDI_NAME, LocalApPurchaseRequisitionHome.class);
			adApprovalQueueHome = (LocalAdApprovalQueueHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdApprovalQueueHome.JNDI_NAME, LocalAdApprovalQueueHome.class);
			adDeleteAuditTrailHome = (LocalAdDeleteAuditTrailHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdDeleteAuditTrailHome.JNDI_NAME, LocalAdDeleteAuditTrailHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}

		try {

			LocalApPurchaseRequisition apPurchaseRequisition = apPurchaseRequisitionHome.findByPrimaryKey(PR_CODE);

			adDeleteAuditTrailHome.create("AP PURCHASE REQUISITION", apPurchaseRequisition.getPrDate(), apPurchaseRequisition.getPrNumber(), apPurchaseRequisition.getPrReferenceNumber(),
 					0d, AD_USR, new Date(), AD_CMPNY);

			apPurchaseRequisition.remove();

		} catch (FinderException ex) {

			getSessionContext().setRollbackOnly();
			throw new GlobalRecordAlreadyDeletedException();

		} catch (Exception ex) {

			Debug.printStackTrace(ex);
			getSessionContext().setRollbackOnly();
			throw new EJBException(ex.getMessage());

		}

	}

	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public Integer generateApPo(Integer PR_CODE, String CRTD_BY, Integer AD_BRNCH, Integer AD_CMPNY) {

		Debug.print("ApPurchaseRequisitionEntryControllerBean generateApPo");

		LocalApCanvassHome apCanvassHome = null;
		LocalApPurchaseRequisitionHome apPurchaseRequisitionHome = null;
		LocalApPurchaseOrderHome apPurchaseOrderHome = null;
		LocalApPurchaseOrderLineHome apPurchaseOrderLineHome = null;
		LocalAdDocumentSequenceAssignmentHome adDocumentSequenceAssignmentHome = null;
		LocalAdBranchDocumentSequenceAssignmentHome adBranchDocumentSequenceAssignmentHome = null;
		LocalGlFunctionalCurrencyHome glFunctionalCurrencyHome = null;
		LocalApTaxCodeHome apTaxCodeHome = null;
		LocalInvTagHome invTagHome = null;

		LocalApPurchaseOrder apPurchaseOrder = null;

		//Initialize EJB Home

		try {

			apCanvassHome = (LocalApCanvassHome)EJBHomeFactory.
				lookUpLocalHome(LocalApCanvassHome.JNDI_NAME, LocalApCanvassHome.class);
			apPurchaseRequisitionHome = (LocalApPurchaseRequisitionHome)EJBHomeFactory.
				lookUpLocalHome(LocalApPurchaseRequisitionHome.JNDI_NAME, LocalApPurchaseRequisitionHome.class);
			apPurchaseOrderHome = (LocalApPurchaseOrderHome)EJBHomeFactory.
				lookUpLocalHome(LocalApPurchaseOrderHome.JNDI_NAME, LocalApPurchaseOrderHome.class);
			apPurchaseOrderLineHome = (LocalApPurchaseOrderLineHome)EJBHomeFactory.
				lookUpLocalHome(LocalApPurchaseOrderLineHome.JNDI_NAME, LocalApPurchaseOrderLineHome.class);
			adDocumentSequenceAssignmentHome = (LocalAdDocumentSequenceAssignmentHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdDocumentSequenceAssignmentHome.JNDI_NAME, LocalAdDocumentSequenceAssignmentHome.class);
			adBranchDocumentSequenceAssignmentHome = (LocalAdBranchDocumentSequenceAssignmentHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdBranchDocumentSequenceAssignmentHome.JNDI_NAME, LocalAdBranchDocumentSequenceAssignmentHome.class);
			glFunctionalCurrencyHome = (LocalGlFunctionalCurrencyHome)EJBHomeFactory.
				lookUpLocalHome(LocalGlFunctionalCurrencyHome.JNDI_NAME, LocalGlFunctionalCurrencyHome.class);
			apTaxCodeHome = (LocalApTaxCodeHome)EJBHomeFactory.
				lookUpLocalHome(LocalApTaxCodeHome.JNDI_NAME, LocalApTaxCodeHome.class);
			invTagHome = (LocalInvTagHome)EJBHomeFactory.
				lookUpLocalHome(LocalInvTagHome.JNDI_NAME, LocalInvTagHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());
		}

		try {

			LocalApPurchaseRequisition apPurchaseRequisition = null;

			try {

				apPurchaseRequisition = apPurchaseRequisitionHome.findByPrimaryKey(PR_CODE);

			} catch(FinderException ex) {


			}

			Date CURR_DT = EJBCommon.getGcCurrentDateWoTime().getTime();

			Collection apGenPoLines = apCanvassHome.findByPrCodeAndCnvPo(PR_CODE, EJBCommon.TRUE, AD_CMPNY);

			Iterator i = apGenPoLines.iterator();

			String SPL_SPPLR_CODE = null;
			short PL_LN = 1;
			int PO_COUNT = 0;

			while(i.hasNext()) {

				LocalApCanvass apCanvass = (LocalApCanvass)i.next();

				LocalApSupplier apSupplier = apCanvass.getApSupplier();

				if(SPL_SPPLR_CODE == null || !apSupplier.getSplSupplierCode().equals(SPL_SPPLR_CODE)) {

					SPL_SPPLR_CODE = apSupplier.getSplSupplierCode();
					PL_LN = 1;

					String PO_NMBR = null;

					//generate document number

					LocalAdBranchDocumentSequenceAssignment adBranchDocumentSequenceAssignment = null;
		 			LocalAdDocumentSequenceAssignment adDocumentSequenceAssignment = null;

					try {

				        adDocumentSequenceAssignment = adDocumentSequenceAssignmentHome.findByDcName("AP PURCHASE ORDER", AD_CMPNY);

				    } catch(FinderException ex) { }

				    try {

	 					adBranchDocumentSequenceAssignment = adBranchDocumentSequenceAssignmentHome.findBdsByDsaCodeAndBrCode(adDocumentSequenceAssignment.getDsaCode(), AD_BRNCH, AD_CMPNY);

	 				} catch (FinderException ex) { }

					if (adDocumentSequenceAssignment.getAdDocumentSequence().getDsNumberingType() == 'A') {

						while (true) {

						    if (adBranchDocumentSequenceAssignment == null || adBranchDocumentSequenceAssignment.getBdsNextSequence() == null) {

								try {

									apPurchaseOrderHome.findByPoDocumentNumberAndBrCode(adDocumentSequenceAssignment.getDsaNextSequence(), AD_BRNCH, AD_CMPNY);
									adDocumentSequenceAssignment.setDsaNextSequence(EJBCommon.incrementStringNumber(adDocumentSequenceAssignment.getDsaNextSequence()));

								} catch (FinderException ex) {

									PO_NMBR = adDocumentSequenceAssignment.getDsaNextSequence();
									adDocumentSequenceAssignment.setDsaNextSequence(EJBCommon.incrementStringNumber(adDocumentSequenceAssignment.getDsaNextSequence()));
									break;

								}

						    } else {

						        try {

									apPurchaseOrderHome.findByPoDocumentNumberAndBrCode(adBranchDocumentSequenceAssignment.getBdsNextSequence(), AD_BRNCH, AD_CMPNY);
									adBranchDocumentSequenceAssignment.setBdsNextSequence(EJBCommon.incrementStringNumber(adBranchDocumentSequenceAssignment.getBdsNextSequence()));

								} catch (FinderException ex) {

									PO_NMBR = adBranchDocumentSequenceAssignment.getBdsNextSequence();
									adBranchDocumentSequenceAssignment.setBdsNextSequence(EJBCommon.incrementStringNumber(adBranchDocumentSequenceAssignment.getBdsNextSequence()));
									break;

								}

						    }

						}

					}

					//create new purchase order

					java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat("dd/MM/yyyy HH:mm:ss.SSSS");
		          	String PO_DESC = "GENERATED PO " + formatter.format(new java.util.Date());
					System.out.println(apPurchaseRequisition.getPrCode()+ "PR GENERATED = TRUE");
					apPurchaseRequisition.setPrGenerated(EJBCommon.TRUE);
					apPurchaseOrder = apPurchaseOrderHome.create(EJBCommon.FALSE, null, CURR_DT, apPurchaseRequisition.getPrDeliveryPeriod(), PO_NMBR, apPurchaseRequisition.getPrNumber(), null,
							PO_DESC, null, null, apPurchaseRequisition.getPrConversionDate(), apPurchaseRequisition.getPrConversionRate(), 0d , EJBCommon.FALSE, EJBCommon.FALSE, null, null,
							EJBCommon.FALSE, null, CRTD_BY, CURR_DT, CRTD_BY, CURR_DT, null, null, null, null,
							EJBCommon.FALSE,
							0d,0d,0d,0d,0d,
							AD_BRNCH, AD_CMPNY);

					/*.create(EJBCommon.FALSE, null, CURR_DT, apPurchaseRequisition.getPrDeliveryPeriod(), PO_NMBR, apPurchaseRequisition.getPrNumber(), null,
							PO_DESC, null, null, apPurchaseRequisition.getPrConversionDate(), apPurchaseRequisition.getPrConversionRate(), 0d , EJBCommon.FALSE, EJBCommon.FALSE, null, null,
							EJBCommon.FALSE, null, CRTD_BY, CURR_DT, CRTD_BY, CURR_DT, null, null, null, null,
							EJBCommon.FALSE, EJBCommon.FALSE, EJBCommon.FALSE,

							EJBCommon.FALSE, EJBCommon.FALSE, EJBCommon.FALSE,
							EJBCommon.FALSE, EJBCommon.FALSE, EJBCommon.FALSE, EJBCommon.FALSE, EJBCommon.FALSE,
							null, null, null, null, null, null, null, null, null,
							AD_BRNCH, AD_CMPNY);*/

					apPurchaseOrder.setPoApprovedRejectedBy(apPurchaseRequisition.getPrCanvassApprovedRejectedBy());
					apPurchaseOrder.setApSupplier(apSupplier);


					LocalAdPaymentTerm adPaymentTerm = apSupplier.getAdPaymentTerm();
					apPurchaseOrder.setAdPaymentTerm(adPaymentTerm);

					LocalGlFunctionalCurrency glFunctionalCurrency = apPurchaseRequisition.getGlFunctionalCurrency();
					apPurchaseOrder.setGlFunctionalCurrency(glFunctionalCurrency);

					LocalApTaxCode apTaxCode = apPurchaseRequisition.getApTaxCode();
					apPurchaseOrder.setApTaxCode(apTaxCode);

					PO_COUNT++;

				}

				//add purchase order line

				double PL_AMNT = EJBCommon.roundIt(apCanvass.getCnvQuantity() * apCanvass.getCnvUnitCost(), this.getGlFcPrecisionUnit(AD_CMPNY));

				LocalApPurchaseOrderLine apPurchaseOrderLine =
					apPurchaseOrderLineHome.create(PL_LN, apCanvass.getCnvQuantity(),
					apCanvass.getCnvUnitCost(), PL_AMNT,null, null, 0d, 0d, null, 0, 0, 0, 0, 0, AD_CMPNY);

				apPurchaseOrder.addApPurchaseOrderLine(apPurchaseOrderLine);

				LocalInvUnitOfMeasure invUnitOfMeasure = apCanvass.getApPurchaseRequisitionLine().getInvUnitOfMeasure();
				invUnitOfMeasure.addApPurchaseOrderLine(apPurchaseOrderLine);

				LocalInvItemLocation invItemLocation = apCanvass.getApPurchaseRequisitionLine().getInvItemLocation();
				invItemLocation.addApPurchaseOrderLine(apPurchaseOrderLine);

				try{
      	  	    	System.out.println("aabot?");
      	  	    	//Iterator t = apPurchaseOrderLine.getInvTag().iterator();
	      	  	    Iterator t = apCanvass.getApPurchaseRequisitionLine().getInvTags().iterator();


	      	  	    System.out.println("umabot?");
	      	  	    while (t.hasNext()){
	      	  	    	LocalInvTag invPlTag = (LocalInvTag)t.next();

	      	  	    	LocalInvTag invTag = invTagHome.create(invPlTag.getTgPropertyCode(),
	      	  	    		invPlTag.getTgSerialNumber(),null,invPlTag.getTgExpiryDate(),
	      	  	    		invPlTag.getTgSpecs(), AD_CMPNY, invPlTag.getTgTransactionDate(),
	      	  	    		invPlTag.getTgType());

		      	  	    invTag.setApPurchaseOrderLine(apPurchaseOrderLine);

		      	  	    invTag.setAdUser(invPlTag.getAdUser());
		      	  	    System.out.println("ngcreate ba?");
	      	  	    }

      	  	    }catch(Exception ex){

      	  	    }

				PL_LN++;

			}

			return new Integer(PO_COUNT);

		} catch (Exception ex) {

			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());

		}

	}

	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public ArrayList getInvUomByIiName(String II_NM, Integer AD_CMPNY) {

		Debug.print("ApPurchaseRequisitionEntryControllerBean getInvUomByIiName");

		LocalInvUnitOfMeasureHome invUnitOfMeasureHome = null;
		LocalInvUnitOfMeasureConversionHome invUnitOfMeasureConversionHome = null;
		LocalInvItemHome invItemHome = null;
		ArrayList list = new ArrayList();

		// Initialize EJB Home

		try {

			invUnitOfMeasureHome = (LocalInvUnitOfMeasureHome)EJBHomeFactory.
				lookUpLocalHome(LocalInvUnitOfMeasureHome.JNDI_NAME, LocalInvUnitOfMeasureHome.class);
			invItemHome = (LocalInvItemHome)EJBHomeFactory.
				lookUpLocalHome(LocalInvItemHome.JNDI_NAME, LocalInvItemHome.class);
			invUnitOfMeasureConversionHome = (LocalInvUnitOfMeasureConversionHome)EJBHomeFactory.
				lookUpLocalHome(LocalInvUnitOfMeasureConversionHome.JNDI_NAME, LocalInvUnitOfMeasureConversionHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());
		}

		try {

			LocalInvItem invItem = null;
			LocalInvUnitOfMeasure invItemUnitOfMeasure = null;

			invItem = invItemHome.findByIiName(II_NM, AD_CMPNY);
			invItemUnitOfMeasure = invItem.getInvUnitOfMeasure();

			Collection invUnitOfMeasures = null;
			Iterator i = invUnitOfMeasureHome.findByUomAdLvClass(
					invItemUnitOfMeasure.getUomAdLvClass(), AD_CMPNY).iterator();
			while (i.hasNext()) {

				LocalInvUnitOfMeasure invUnitOfMeasure = (LocalInvUnitOfMeasure) i.next();

				try {
	                LocalInvUnitOfMeasureConversion invUnitOfMeasureConversion = invUnitOfMeasureConversionHome.findUmcByIiNameAndUomName(II_NM, invUnitOfMeasure.getUomName(), AD_CMPNY);
	                if (invUnitOfMeasureConversion.getUmcBaseUnit() == EJBCommon.FALSE && invUnitOfMeasureConversion.getUmcConversionFactor() == 1) continue;
                } catch (FinderException ex) {
                	continue;
                }

				InvModUnitOfMeasureDetails details = new InvModUnitOfMeasureDetails();
				details.setUomName(invUnitOfMeasure.getUomName());

				if (invUnitOfMeasure.getUomName().equals(invItemUnitOfMeasure.getUomName())) {

					details.setDefault(true);

				}

				list.add(details);

			}

			return list;

		} catch (Exception ex) {

			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());

		}

	}

	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public double getInvIiUnitCostByIiNameAndUomName(String II_NM, String LOC_NM, String UOM_NM, Integer AD_BRNCH, Integer AD_CMPNY) {

		Debug.print("ApPurchaseRequisitionEntryControllerBean getInvIiUnitCostByIiNameAndUomName");

		LocalInvItemLocationHome invItemLocationHome = null;
		LocalApPurchaseOrderLineHome apPurchaseOrderLineHome = null;

		// Initialize EJB Home

		try {

			invItemLocationHome = (LocalInvItemLocationHome)EJBHomeFactory.
				lookUpLocalHome(LocalInvItemLocationHome.JNDI_NAME, LocalInvItemLocationHome.class);
			apPurchaseOrderLineHome = (LocalApPurchaseOrderLineHome)EJBHomeFactory.
				lookUpLocalHome(LocalApPurchaseOrderLineHome.JNDI_NAME, LocalApPurchaseOrderLineHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}

		try {


			LocalInvItemLocation invItemLocation = null;

			try {

				invItemLocation = invItemLocationHome.findByIiNameAndLocName(II_NM, LOC_NM, AD_CMPNY);

			} catch (FinderException ex) {

				return 0;
			}

			LocalInvItem invItem = invItemLocation.getInvItem();

			double II_UNT_CST = 0d;

			II_UNT_CST = invItemLocation.getInvItem().getIiUnitCost();
			/*
			if (invItem.getApSupplier() != null) {

				// if item has supplier retrieve last purchase price

				II_UNT_CST = this.getInvLastPurchasePriceBySplSupplierCode(invItemLocation, invItem.getApSupplier().getSplSupplierCode(), AD_BRNCH, AD_CMPNY);

			} else {

				// if item has no supplier retrieve last purchase price

				II_UNT_CST = this.getInvLastPurchasePrice(invItemLocation, AD_BRNCH, AD_CMPNY);


			}*/

			if (II_UNT_CST == 0) {

				II_UNT_CST = invItemLocation.getInvItem().getIiUnitCost();

			}

			return this.convertCostByUom(II_NM, UOM_NM, II_UNT_CST, true, AD_CMPNY);

		} catch (Exception ex) {

			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());

		}

	}

	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public double getFrRateByFrNameAndFrDate(String FC_NM, Date CONVERSION_DATE, Integer AD_CMPNY)
	throws GlobalConversionDateNotExistException {

		Debug.print("ApPurchaseRequisitionEntryControllerBean getFrRateByFrNameAndFrDate");

		LocalGlFunctionalCurrencyRateHome glFunctionalCurrencyRateHome = null;
		LocalGlFunctionalCurrencyHome glFunctionalCurrencyHome = null;
		LocalAdCompanyHome adCompanyHome = null;

		// Initialize EJB Home

		try {

			glFunctionalCurrencyRateHome = (LocalGlFunctionalCurrencyRateHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlFunctionalCurrencyRateHome.JNDI_NAME, LocalGlFunctionalCurrencyRateHome.class);
			glFunctionalCurrencyHome = (LocalGlFunctionalCurrencyHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlFunctionalCurrencyHome.JNDI_NAME, LocalGlFunctionalCurrencyHome.class);
			adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}

		try {

			LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
			LocalGlFunctionalCurrency glFunctionalCurrency = glFunctionalCurrencyHome.findByFcName(FC_NM, AD_CMPNY);

			double CONVERSION_RATE = 1;

			// Get functional currency rate

			if (!FC_NM.equals("USD")) {

				LocalGlFunctionalCurrencyRate glFunctionalCurrencyRate =
					glFunctionalCurrencyRateHome.findByFcCodeAndDate(glFunctionalCurrency.getFcCode(),
							CONVERSION_DATE, AD_CMPNY);

				CONVERSION_RATE = glFunctionalCurrencyRate.getFrXToUsd();

			}

			// Get set of book functional currency rate if necessary

			if (!adCompany.getGlFunctionalCurrency().getFcName().equals("USD")) {

				LocalGlFunctionalCurrencyRate glCompanyFunctionalCurrencyRate =
					glFunctionalCurrencyRateHome.findByFcCodeAndDate(adCompany.getGlFunctionalCurrency(). getFcCode(),
							CONVERSION_DATE, AD_CMPNY);

				CONVERSION_RATE = CONVERSION_RATE / glCompanyFunctionalCurrencyRate.getFrXToUsd();

			}

			return CONVERSION_RATE;

		} catch (FinderException ex) {

			getSessionContext().setRollbackOnly();
			throw new GlobalConversionDateNotExistException();

		} catch (Exception ex) {

			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());

		}

	}

	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public byte getAdPrfApShowPrCost(Integer AD_CMPNY) {

		Debug.print("ApPurchaseRequisitionEntryControllerBean getAdPrfApShowPrCost");

		LocalAdPreferenceHome adPreferenceHome = null;

		// Initialize EJB Home

		try {

			adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}


		try {

			LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);

			return adPreference.getPrfApShowPrCost();

		} catch (Exception ex) {

			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());

		}

	}



	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public ArrayList getAdApprovalNotifiedUsersByPrCode(Integer PR_CODE, Integer AD_CMPNY) {

		Debug.print("ApPurchaseRequisitionEntryControllerBean getAdApprovalNotifiedUsersByPrCode");

		LocalAdApprovalQueueHome adApprovalQueueHome = null;
		LocalApPurchaseRequisitionHome apPurchaseRequisitionHome = null;

		ArrayList list = new ArrayList();


		// Initialize EJB Home

		try {

			adApprovalQueueHome = (LocalAdApprovalQueueHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdApprovalQueueHome.JNDI_NAME, LocalAdApprovalQueueHome.class);
			apPurchaseRequisitionHome = (LocalApPurchaseRequisitionHome)EJBHomeFactory.
			lookUpLocalHome(LocalApPurchaseRequisitionHome.JNDI_NAME, LocalApPurchaseRequisitionHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}

		try {

			LocalApPurchaseRequisition apPurchaseRequisition = apPurchaseRequisitionHome.findByPrimaryKey(PR_CODE);

			if (apPurchaseRequisition.getPrPosted() == EJBCommon.TRUE) {

				list.add("DOCUMENT POSTED");
				return list;

			}

			Collection adApprovalQueues = adApprovalQueueHome.findByAqDocumentAndAqDocumentCode("AP PURCHASE REQUISITION", PR_CODE, AD_CMPNY);

			Iterator i = adApprovalQueues.iterator();

			while(i.hasNext()) {

				LocalAdApprovalQueue adApprovalQueue = (LocalAdApprovalQueue)i.next();

				list.add(adApprovalQueue.getAqLevel() + " APPROVER - "+ adApprovalQueue.getAdUser().getUsrDescription());

			}

			return list;

		} catch (Exception ex) {

			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());

		}

	}


	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public ArrayList getAdApprovalNotifiedUsersByPrCodeCanvass(Integer PR_CODE, Integer AD_CMPNY) {

		Debug.print("ApPurchaseRequisitionEntryControllerBean getAdApprovalNotifiedUsersByPrCodeCanvass");

		LocalAdApprovalQueueHome adApprovalQueueHome = null;
		LocalApPurchaseRequisitionHome apPurchaseRequisitionHome = null;

		ArrayList list = new ArrayList();


		// Initialize EJB Home

		try {

			adApprovalQueueHome = (LocalAdApprovalQueueHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdApprovalQueueHome.JNDI_NAME, LocalAdApprovalQueueHome.class);
			apPurchaseRequisitionHome = (LocalApPurchaseRequisitionHome)EJBHomeFactory.
			lookUpLocalHome(LocalApPurchaseRequisitionHome.JNDI_NAME, LocalApPurchaseRequisitionHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}

		try {

			LocalApPurchaseRequisition apPurchaseRequisition = apPurchaseRequisitionHome.findByPrimaryKey(PR_CODE);

			if (apPurchaseRequisition.getPrCanvassPosted() == EJBCommon.TRUE) {

				list.add("DOCUMENT POSTED");
				return list;

			}

			Collection adApprovalQueues = adApprovalQueueHome.findByAqDocumentAndAqDocumentCode("AP CANVASS", PR_CODE, AD_CMPNY);

			Iterator i = adApprovalQueues.iterator();

			while(i.hasNext()) {

				LocalAdApprovalQueue adApprovalQueue = (LocalAdApprovalQueue)i.next();

				list.add(adApprovalQueue.getAdUser().getUsrDescription());

			}

			return list;

		} catch (Exception ex) {

			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());

		}

	}




	//private methods

	private double getInvLastPurchasePriceBySplSupplierCode(LocalInvItemLocation invItemLocation, String SPL_SPPLR_CODE, Integer AD_BRNCH, Integer AD_CMPNY) {

		Debug.print("ApPurchaseRequisitionEntryControllerBean getInvLastPurchasePriceBySplSupplierCode");

		LocalApPurchaseOrderLineHome apPurchaseOrderLineHome = null;

		// Initialize EJB Home

		try {

			apPurchaseOrderLineHome = (LocalApPurchaseOrderLineHome)EJBHomeFactory.
				lookUpLocalHome(LocalApPurchaseOrderLineHome.JNDI_NAME, LocalApPurchaseOrderLineHome.class);

		} catch (NamingException ex) {

		    throw new EJBException(ex.getMessage());

		}

		try {

			LocalApPurchaseOrderLine apPurchaseOrderLine = apPurchaseOrderLineHome.
			getByMaxPoDateAndMaxPlCodeAndIiNameAndLocNameAndPoReceivingAndPoPostedAndSplSpplrCode(
					invItemLocation.getInvItem().getIiName(),
					invItemLocation.getInvLocation().getLocName(),
					EJBCommon.TRUE, EJBCommon.TRUE,
					invItemLocation.getInvItem().getApSupplier().getSplSupplierCode(),
					AD_BRNCH, AD_CMPNY);

			return this.convertCostByUom(invItemLocation.getInvItem().getIiName(), apPurchaseOrderLine.getInvUnitOfMeasure().getUomName(),
					apPurchaseOrderLine.getPlUnitCost(), false, AD_CMPNY);

		} catch(FinderException ex) {

			return 0d;

		}

	}




	private byte sendEmail(LocalAdApprovalQueue adApprovalQueue, Integer AD_CMPNY) throws GlobalSendEmailMessageException {

		Debug.print("ApPurchaseRequisitionEntryControllerBean sendEmail");


		LocalApPurchaseRequisitionHome apPurchaseRequisitionHome = null;
		LocalApCanvassHome apCanvassHome = null;
		LocalAdPreferenceHome adPreferenceHome = null;


		// Initialize EJB Home

		try {


			apPurchaseRequisitionHome = (LocalApPurchaseRequisitionHome)EJBHomeFactory.
			lookUpLocalHome(LocalApPurchaseRequisitionHome.JNDI_NAME, LocalApPurchaseRequisitionHome.class);

			apCanvassHome = (LocalApCanvassHome)EJBHomeFactory.
					lookUpLocalHome(LocalApCanvassHome.JNDI_NAME, LocalApCanvassHome.class);
			
			adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
					lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}

		StringBuilder composedEmail = new StringBuilder();
		LocalApPurchaseRequisition apPurchaseRequisition = null;
		LocalAdPreference adPreference = null;
		try {

			
			adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);
			
			
			apPurchaseRequisition = apPurchaseRequisitionHome.findByPrimaryKey(adApprovalQueue.getAqDocumentCode());

			HashMap hm = new HashMap();

			Iterator i = apPurchaseRequisition.getApPurchaseRequisitionLines().iterator();

			while(i.hasNext()) {

				LocalApPurchaseRequisitionLine apPurchaseRequisitionLine = (LocalApPurchaseRequisitionLine)i.next();

				Collection apCanvasses = apCanvassHome.findByPrlCodeAndCnvPo(
						apPurchaseRequisitionLine.getPrlCode(), EJBCommon.TRUE, AD_CMPNY);

				Iterator j = apCanvasses.iterator();


				while(j.hasNext()) {

					LocalApCanvass apCanvass = (LocalApCanvass)j.next();


					if (hm.containsKey(apCanvass.getApSupplier().getSplSupplierCode())){
						AdModApprovalQueueDetails adModApprovalQueueExistingDetails = (AdModApprovalQueueDetails)
		        				 hm.get(apCanvass.getApSupplier().getSplSupplierCode());

						adModApprovalQueueExistingDetails.setAqSupplierCode(apCanvass.getApSupplier().getSplSupplierCode());
						adModApprovalQueueExistingDetails.setAqSupplierName(apCanvass.getApSupplier().getSplName());
						adModApprovalQueueExistingDetails.setAqAmount(adModApprovalQueueExistingDetails.getAqAmount() +  apCanvass.getCnvAmount());

					} else {

						AdModApprovalQueueDetails adModApprovalQueueNewDetails = new AdModApprovalQueueDetails();

						adModApprovalQueueNewDetails.setAqSupplierCode(apCanvass.getApSupplier().getSplSupplierCode());
						adModApprovalQueueNewDetails.setAqSupplierName(apCanvass.getApSupplier().getSplName());
						adModApprovalQueueNewDetails.setAqAmount(apCanvass.getCnvAmount());

						hm.put(apCanvass.getApSupplier().getSplSupplierCode(), adModApprovalQueueNewDetails);
					}
				}
			}

			
			
		
			
			
			
			
			
			
			
			
			
			

			Set set = hm.entrySet();

			composedEmail.append("<table border='1' style='width:100%'>");

			composedEmail.append("<tr>");
			composedEmail.append("<th> VENDOR </th>");
			composedEmail.append("<th> AMOUNT </th>");
			composedEmail.append("</tr>");


			Iterator x = set.iterator();

			while(x.hasNext()) {


				Map.Entry me = (Map.Entry)x.next();

				AdModApprovalQueueDetails adModApprovalQueueDetails = (AdModApprovalQueueDetails)
							me.getValue();

				composedEmail.append("<tr>");
				composedEmail.append("<td>");
				composedEmail.append(adModApprovalQueueDetails.getAqSupplierName());
				composedEmail.append("</td>");
				composedEmail.append("<td>");
				composedEmail.append(adModApprovalQueueDetails.getAqAmount());
				composedEmail.append("</td>");
				composedEmail.append("</tr>");

			}

			composedEmail.append("</table>");




		} catch (Exception ex) {

			Debug.printStackTrace(ex);
			throw new GlobalSendEmailMessageException(ex.getMessage());

		}


		String emailTo = adApprovalQueue.getAdUser().getUsrEmailAddress();
		Properties props = new Properties();
		/*
		 * GMAIL SETTINGS
		 * props.put("mail.smtp.host", "smtp.gmail.com");
		props.put("mail.smtp.socketFactory.port", "465");
		props.put("mail.smtp.socketFactory.class",
				"javax.net.ssl.SSLSocketFactory");
		props.put("mail.smtp.auth", "true");
		props.put("mail.smtp.port", "465");

		Session session = Session.getDefaultInstance(props,
		new javax.mail.Authenticator() {
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication("chrisr@daltron.net.pg","Sh0wc453");
			}
		});*/

	//	props.put("mail.smtp.host", "180.150.253.101");
	//	props.put("mail.smtp.socketFactory.port", "25");
	//	props.put("mail.smtp.auth", "false");
	//	props.put("mail.smtp.port", "25");
		
		//180.150.253.105
		//180.150.253.101
		
		
		props.put("mail.smtp.host", adPreference.getPrfMailHost());
	
		props.put("mail.smtp.socketFactory.port",adPreference.getPrfMailSocketFactoryPort());
		props.put("mail.smtp.auth", "false"); //this will allow to not use username nad password
		props.put("mail.smtp.port", adPreference.getPrfMailPort());
	
	
		
		System.out.println("Mail host:" + props.get("mail.smtp.host"));
		System.out.println("socket factory:" + props.get("mail.smtp.socketFactory.port"));
		System.out.println("port:" + props.get("mail.smtp.port"));
		System.out.println("email from:" + adPreference.getPrfMailFrom());
		System.out.println("email to:" + emailTo);
		
		
		

		
		
		final String emailFrom = adPreference.getPrfMailFrom();
		final String emailPassword = adPreference.getPrfMailPassword();
		boolean isAuthenticator = adPreference.getPrfMailAuthenticator()==EJBCommon.TRUE?true:false;
		
		
		
		
		
		props.put("mail.smtp.socketFactory.class",
				"javax.net.ssl.SSLSocketFactory");
		
		
		
		Session session;

		
		if(isAuthenticator) {
			System.out.println("authentication entered");
			
			props.put("mail.smtp.starttls.enable", "true");
			
			props.put("mail.smtp.auth", "true");
			session = Session.getInstance(props,
			new javax.mail.Authenticator() {
				protected PasswordAuthentication getPasswordAuthentication() {
					return new PasswordAuthentication(emailFrom,emailPassword);
				}
			});
			
			
		}else {
			session = Session.getInstance(props, null);
		}
		

		try {

			Message message = new MimeMessage(session);
			message.setFrom(new InternetAddress(emailFrom));
			message.setRecipients(Message.RecipientType.TO,
					InternetAddress.parse(emailTo));


			message.setRecipients(Message.RecipientType.BCC,
					InternetAddress.parse(adPreference.getPrfMailBcc()));

			message.setSubject("DALTRON - OFS - PURCHASE REQUISTION APPROVAL PR #:"+apPurchaseRequisition.getPrNumber());
			/*message.setText("Dear Mr/Mrs," +
					"\n\n No spam to my email, please!");*/

			/*message.setContent(
		              "<h1>This is actual message embedded in HTML tags</h1>",
		             "text/html");*/
			System.out.println("adApprovalQueue.getAqRequesterName()="+adApprovalQueue.getAqRequesterName());
			System.out.println("adApprovalQueue.getAqDocumentNumber()="+adApprovalQueue.getAqDocumentNumber());
			System.out.println("adApprovalQueue.getAdUser().getUsrDescription()="+adApprovalQueue.getAdUser().getUsrDescription());

			message.setContent(
		              "Dear "+adApprovalQueue.getAdUser().getUsrDescription()+",<br><br>"+
		              "A purchase request was raised by "+adApprovalQueue.getAqRequesterName()+" for your approval.<br>"+
		              "PR Number: "+adApprovalQueue.getAqDocumentNumber()+".<br>"+
		              "Delivery Period: "+EJBCommon.convertSQLDateToString(apPurchaseRequisition.getPrDeliveryPeriod())+".<br>"+
		              "Description: "+apPurchaseRequisition.getPrDescription()+".<br>"+
		              composedEmail.toString() +

		              "Please click the link <a href=\"http://live.daltronpng.com\">http://live.daltronpng.com:8080/daltron</a>.<br><br><br>"+

		              "This is an automated message and was sent from a notification-only email address.<br><br>"+
		              "Please do not reply to this message.<br><br>",
		             "text/html");


			Transport.send(message);
			adApprovalQueue.setAqEmailSent(EJBCommon.TRUE);


			System.out.println("Done");

		} catch (MessagingException e) {
			Debug.printStackTrace(e);
			throw new GlobalSendEmailMessageException(e.getMessage());
		}

		return 0;
	}

	private double getInvLastPurchasePrice(LocalInvItemLocation invItemLocation, Integer AD_BRNCH, Integer AD_CMPNY) {

		Debug.print("ApGeneratePurchaseRequisitionControllerBean getInvLastPurchasePrice");

		LocalApPurchaseOrderLineHome apPurchaseOrderLineHome = null;

		// Initialize EJB Home

		try {

			apPurchaseOrderLineHome = (LocalApPurchaseOrderLineHome)EJBHomeFactory.
				lookUpLocalHome(LocalApPurchaseOrderLineHome.JNDI_NAME, LocalApPurchaseOrderLineHome.class);

		} catch (NamingException ex) {

		    throw new EJBException(ex.getMessage());

		}

		try {

			LocalApPurchaseOrderLine apPurchaseOrderLine = apPurchaseOrderLineHome.
			getByMaxPoDateAndMaxPlCodeAndIiNameAndLocNameAndPoReceivingAndPoPosted(
					invItemLocation.getInvItem().getIiName(),
					invItemLocation.getInvLocation().getLocName(),
					EJBCommon.TRUE, EJBCommon.TRUE,
					AD_BRNCH, AD_CMPNY);

			return this.convertCostByUom(invItemLocation.getInvItem().getIiName(),
					apPurchaseOrderLine.getInvUnitOfMeasure().getUomName(),
					apPurchaseOrderLine.getPlUnitCost(), false, AD_CMPNY);

		} catch(FinderException ex) {

			return 0d;

		}

	}

	private double convertCostByUom(String II_NM, String UOM_NM, double unitCost, boolean isFromDefault, Integer AD_CMPNY) {

		Debug.print("ApPurchaseRequisitionEntryControllerBean convertCostByUom");

		LocalInvItemHome invItemHome = null;
		LocalInvUnitOfMeasureConversionHome invUnitOfMeasureConversionHome = null;

	    // Initialize EJB Home

	    try {

	        invUnitOfMeasureConversionHome = (LocalInvUnitOfMeasureConversionHome)EJBHomeFactory.
				lookUpLocalHome(LocalInvUnitOfMeasureConversionHome.JNDI_NAME, LocalInvUnitOfMeasureConversionHome.class);
	        invItemHome = (LocalInvItemHome)EJBHomeFactory.
				lookUpLocalHome(LocalInvItemHome.JNDI_NAME, LocalInvItemHome.class);

	    } catch (NamingException ex) {

	        throw new EJBException(ex.getMessage());

	    }

	    try {

	    	LocalInvItem invItem = invItemHome.findByIiName(II_NM, AD_CMPNY);

	    	LocalInvUnitOfMeasureConversion invUnitOfMeasureConversion = invUnitOfMeasureConversionHome.findUmcByIiNameAndUomName(II_NM, UOM_NM, AD_CMPNY);
	    	LocalInvUnitOfMeasureConversion invDefaultUomConversion = invUnitOfMeasureConversionHome.findUmcByIiNameAndUomName(II_NM, invItem.getInvUnitOfMeasure().getUomName(), AD_CMPNY);

	        if (isFromDefault) {

	        	return EJBCommon.roundIt(unitCost * invDefaultUomConversion.getUmcConversionFactor() / invUnitOfMeasureConversion.getUmcConversionFactor(), this.getGlFcPrecisionUnit(AD_CMPNY));

	        } else {

	        	return EJBCommon.roundIt(unitCost * invUnitOfMeasureConversion.getUmcConversionFactor() / invDefaultUomConversion.getUmcConversionFactor(), this.getGlFcPrecisionUnit(AD_CMPNY));

	        }



	    } catch (Exception ex) {

	    	Debug.printStackTrace(ex);
	    	getSessionContext().setRollbackOnly();
	    	throw new EJBException(ex.getMessage());

	    }

	}

	/**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList getAdUsrAll(Integer AD_CMPNY) {

        Debug.print("ApPurchaseRequisitionEntryControllerBean getAdUsrAll");

        LocalAdUserHome adUserHome = null;

        ArrayList list = new ArrayList();

        // Initialize EJB Home

        try {

            adUserHome = (LocalAdUserHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdUserHome.JNDI_NAME, LocalAdUserHome.class);

        } catch (NamingException ex) {

            throw new EJBException(ex.getMessage());

        }

        try {

        	Collection adUsers = adUserHome.findUsrAll(AD_CMPNY);

        	Iterator i = adUsers.iterator();

        	while (i.hasNext()) {

        		LocalAdUser adUser = (LocalAdUser)i.next();

        		list.add(adUser.getUsrName());

        	}

        	return list;

        } catch (Exception ex) {

        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());

        }

    }

    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public String getAdUsrDepartment(String USR_NM, Integer AD_CMPNY) {

        Debug.print("ApPurchaseRequisitionEntryControllerBean getAdUsrAll");

        LocalAdUserHome adUserHome = null;
        LocalAdUser adUser = null;
        ArrayList list = new ArrayList();

        // Initialize EJB Home

        try {

            adUserHome = (LocalAdUserHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdUserHome.JNDI_NAME, LocalAdUserHome.class);

        } catch (NamingException ex) {

            throw new EJBException(ex.getMessage());

        }

        try {

        	//Collection adUsers = adUserHome.findUsrByDepartment(USR_DEPT, AD_CMPNY);
        	adUser = adUserHome.findByUsrName(USR_NM, AD_CMPNY);
        	String department = adUser.getUsrDept();

        	return department;

        } catch (Exception ex) {

        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());

        }

    }

    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList getAdUsrByDeptartmentAll(String USR_DEPT, Integer AD_CMPNY) {

        Debug.print("ApPurchaseRequisitionEntryControllerBean getAdUsrAll");

        LocalAdUserHome adUserHome = null;

        ArrayList list = new ArrayList();

        // Initialize EJB Home

        try {

            adUserHome = (LocalAdUserHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdUserHome.JNDI_NAME, LocalAdUserHome.class);

        } catch (NamingException ex) {

            throw new EJBException(ex.getMessage());

        }

        try {

        	Collection adUsers = adUserHome.findUsrByDepartment(USR_DEPT, AD_CMPNY);

        	Iterator i = adUsers.iterator();

        	while (i.hasNext()) {

        		LocalAdUser adUser = (LocalAdUser)i.next();

        		list.add(adUser.getUsrName());

        	}

        	return list;

        } catch (Exception ex) {

        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());

        }

    }


    /**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public ArrayList getAdLvPurchaseRequisitionType(Integer AD_CMPNY) {

		Debug.print("ApPurchaseRequisitionEntryControllerBean getAdLvPurchaseRequisitionType");

		LocalAdLookUpValueHome adLookUpValueHome = null;

		ArrayList list = new ArrayList();

		// Initialize EJB Home

		try {

			adLookUpValueHome = (LocalAdLookUpValueHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdLookUpValueHome.JNDI_NAME, LocalAdLookUpValueHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}

		try {

			Collection adLookUpValues = adLookUpValueHome.findByLuName("AP PURCHASE REQUISITION TYPE", AD_CMPNY);

			Iterator i = adLookUpValues.iterator();

			while (i.hasNext()) {

				LocalAdLookUpValue adLookUpValue = (LocalAdLookUpValue)i.next();

				list.add(adLookUpValue.getLvName());

			}
			System.out.println("PR size="+list.size());

			return list;

		} catch (Exception ex) {

			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());

		}

	}


	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public ArrayList getAdLvPurchaseRequisitionMisc1(Integer AD_CMPNY) {

		Debug.print("ApPurchaseRequisitionEntryControllerBean getAdLvPurchaseRequisitionMisc1");

		LocalAdLookUpValueHome adLookUpValueHome = null;

		ArrayList list = new ArrayList();

		// Initialize EJB Home

		try {

			adLookUpValueHome = (LocalAdLookUpValueHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdLookUpValueHome.JNDI_NAME, LocalAdLookUpValueHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}

		try {

			Collection adLookUpValues = adLookUpValueHome.findByLuName("AP PURCHASE REQUISITION MISC1", AD_CMPNY);

			Iterator i = adLookUpValues.iterator();

			while (i.hasNext()) {

				LocalAdLookUpValue adLookUpValue = (LocalAdLookUpValue)i.next();

				list.add(adLookUpValue.getLvName());

			}
			System.out.println("MISC1="+list.size());

			return list;

		} catch (Exception ex) {

			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());

		}

	}

	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public ArrayList getAdLvPurchaseRequisitionMisc2(Integer AD_CMPNY) {

		Debug.print("ApPurchaseRequisitionEntryControllerBean getAdLvPurchaseRequisitionMisc2");

		LocalAdLookUpValueHome adLookUpValueHome = null;

		ArrayList list = new ArrayList();

		// Initialize EJB Home

		try {

			adLookUpValueHome = (LocalAdLookUpValueHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdLookUpValueHome.JNDI_NAME, LocalAdLookUpValueHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}

		try {

			Collection adLookUpValues = adLookUpValueHome.findByLuName("AP PURCHASE REQUISITION MISC2", AD_CMPNY);

			Iterator i = adLookUpValues.iterator();

			while (i.hasNext()) {

				LocalAdLookUpValue adLookUpValue = (LocalAdLookUpValue)i.next();

				list.add(adLookUpValue.getLvName());

			}
			System.out.println("PR T7="+list.size());

			return list;

		} catch (Exception ex) {

			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());

		}

	}


	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public ArrayList getAdLvPurchaseRequisitionMisc3(Integer AD_CMPNY) {

		Debug.print("ApPurchaseRequisitionEntryControllerBean getAdLvPurchaseRequisitionMisc3");

		LocalAdLookUpValueHome adLookUpValueHome = null;

		ArrayList list = new ArrayList();

		// Initialize EJB Home

		try {

			adLookUpValueHome = (LocalAdLookUpValueHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdLookUpValueHome.JNDI_NAME, LocalAdLookUpValueHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}

		try {

			Collection adLookUpValues = adLookUpValueHome.findByLuName("AP PURCHASE REQUISITION MISC3", AD_CMPNY);

			Iterator i = adLookUpValues.iterator();

			while (i.hasNext()) {

				LocalAdLookUpValue adLookUpValue = (LocalAdLookUpValue)i.next();

				list.add(adLookUpValue.getLvName());

			}
			System.out.println("PR T9="+list.size());

			return list;

		} catch (Exception ex) {

			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());

		}

	}


	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public ArrayList getAdLvPurchaseRequisitionMisc4(Integer AD_CMPNY) {

		Debug.print("ApPurchaseRequisitionEntryControllerBean getAdLvPurchaseRequisitionMisc4");

		LocalAdLookUpValueHome adLookUpValueHome = null;

		ArrayList list = new ArrayList();

		// Initialize EJB Home

		try {

			adLookUpValueHome = (LocalAdLookUpValueHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdLookUpValueHome.JNDI_NAME, LocalAdLookUpValueHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}

		try {

			Collection adLookUpValues = adLookUpValueHome.findByLuName("AP PURCHASE REQUISITION MISC4", AD_CMPNY);

			Iterator i = adLookUpValues.iterator();

			while (i.hasNext()) {

				LocalAdLookUpValue adLookUpValue = (LocalAdLookUpValue)i.next();

				list.add(adLookUpValue.getLvName());

			}
			System.out.println("PR T9="+list.size());

			return list;

		} catch (Exception ex) {

			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());

		}

	}

	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public ArrayList getAdLvPurchaseRequisitionMisc5(Integer AD_CMPNY) {

		Debug.print("ApPurchaseRequisitionEntryControllerBean getAdLvPurchaseRequisitionMisc5");

		LocalAdLookUpValueHome adLookUpValueHome = null;

		ArrayList list = new ArrayList();

		// Initialize EJB Home

		try {

			adLookUpValueHome = (LocalAdLookUpValueHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdLookUpValueHome.JNDI_NAME, LocalAdLookUpValueHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}

		try {

			Collection adLookUpValues = adLookUpValueHome.findByLuName("AP PURCHASE REQUISITION MISC5", AD_CMPNY);

			Iterator i = adLookUpValues.iterator();

			while (i.hasNext()) {

				LocalAdLookUpValue adLookUpValue = (LocalAdLookUpValue)i.next();

				list.add(adLookUpValue.getLvName());

			}
			System.out.println("PR T9="+list.size());

			return list;

		} catch (Exception ex) {

			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());

		}

	}

	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public ArrayList getAdLvPurchaseRequisitionMisc6(Integer AD_CMPNY) {

		Debug.print("ApPurchaseRequisitionEntryControllerBean getAdLvPurchaseRequisitionMisc6");

		LocalAdLookUpValueHome adLookUpValueHome = null;

		ArrayList list = new ArrayList();

		// Initialize EJB Home

		try {

			adLookUpValueHome = (LocalAdLookUpValueHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdLookUpValueHome.JNDI_NAME, LocalAdLookUpValueHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}

		try {

			Collection adLookUpValues = adLookUpValueHome.findByLuName("AP PURCHASE REQUISITION MISC6", AD_CMPNY);

			Iterator i = adLookUpValues.iterator();

			while (i.hasNext()) {

				LocalAdLookUpValue adLookUpValue = (LocalAdLookUpValue)i.next();

				list.add(adLookUpValue.getLvName());

			}
			System.out.println("PR T9="+list.size());

			return list;

		} catch (Exception ex) {

			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());

		}

	}
	
	
	/**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public double getAverageCost(String II_NM, String LOC_NM, String UOM_NM, Date currentDate, Integer AD_BRNCH, Integer AD_CMPNY) {

        Debug.print("ApPurchaseRequisitionEntryControllerBean getInvIiUnitCostByIiNameAndUomName");

        LocalInvItemLocationHome invItemLocationHome = null;
        LocalApPurchaseOrderLineHome apPurchaseOrderLineHome = null;
        LocalInvCostingHome invCostingHome = null;

        // Initialize EJB Home

        try {
        
            invCostingHome = (LocalInvCostingHome)EJBHomeFactory.
                lookUpLocalHome(LocalInvCostingHome.JNDI_NAME, LocalInvCostingHome.class);
            invItemLocationHome = (LocalInvItemLocationHome)EJBHomeFactory.
                lookUpLocalHome(LocalInvItemLocationHome.JNDI_NAME, LocalInvItemLocationHome.class);
            apPurchaseOrderLineHome = (LocalApPurchaseOrderLineHome)EJBHomeFactory.
                lookUpLocalHome(LocalApPurchaseOrderLineHome.JNDI_NAME, LocalApPurchaseOrderLineHome.class);

        } catch (NamingException ex) {

            throw new EJBException(ex.getMessage());

        }

        try {


            LocalInvItemLocation invItemLocation = null;

            try {

                invItemLocation = invItemLocationHome.findByIiNameAndLocName(II_NM, LOC_NM, AD_CMPNY);

            } catch (FinderException ex) {

                return 0;
            }
            
            
            
             // get all item costings

            double COST = invItemLocation.getInvItem().getIiUnitCost();


            try {
            
                //Get average cost. 
                LocalInvCosting invCosting  = invCostingHome.getByMaxCstDateToLongAndMaxCstLineNumberAndLessThanEqualCstDateAndIiNameAndLocName(
                        currentDate, invItemLocation.getInvItem().getIiName(), invItemLocation.getInvLocation().getLocName(), AD_BRNCH, AD_CMPNY);

                
                COST = invCosting.getCstRemainingQuantity() == 0 ? COST:
                    Math.abs(invCosting.getCstRemainingValue() / invCosting.getCstRemainingQuantity());

            } catch (FinderException ex) {

                COST = invItemLocation.getInvItem().getIiUnitCost();

            }
            

       

            double II_UNT_CST = COST;

 
            /*
            if (invItem.getApSupplier() != null) {

                // if item has supplier retrieve last purchase price

                II_UNT_CST = this.getInvLastPurchasePriceBySplSupplierCode(invItemLocation, invItem.getApSupplier().getSplSupplierCode(), AD_BRNCH, AD_CMPNY);

            } else {

                // if item has no supplier retrieve last purchase price

                II_UNT_CST = this.getInvLastPurchasePrice(invItemLocation, AD_BRNCH, AD_CMPNY);


            }*/

       

            return this.convertCostByUom(II_NM, UOM_NM, II_UNT_CST, true, AD_CMPNY);

        } catch (Exception ex) {

            Debug.printStackTrace(ex);
            throw new EJBException(ex.getMessage());

        }

    }
	
	
	
	
	
	
	
	/**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
	public double getIiAveCost(LocalInvItem invItem, Date currentDate, Integer AD_BRNCH, Integer AD_CMPNY) {

        Debug.print("InvItemEntryControllerBean getIiAveCost");

        LocalInvCostingHome invCostingHome = null;

        try {

            invCostingHome = (LocalInvCostingHome)EJBHomeFactory.
                lookUpLocalHome(LocalInvCostingHome.JNDI_NAME, LocalInvCostingHome.class);

        } catch (NamingException ex) {

            throw new EJBException(ex.getMessage());

        }

        try {

            double TOTAL_COST = 0;

            // get all item locations

            Collection invItemLocations = invItem.getInvItemLocations();

            if (invItemLocations.size() == 0) return invItem.getIiUnitCost();

            Iterator ilItr = invItemLocations.iterator();

            while (ilItr.hasNext()) {

                LocalInvItemLocation invItemLocation = (LocalInvItemLocation)ilItr.next();

                // get all item costings

                double COST = invItemLocation.getInvItem().getIiUnitCost();


                    try {
                    
                        //Get average cost. 
                        LocalInvCosting invCosting  = invCostingHome.getByMaxCstDateToLongAndMaxCstLineNumberAndLessThanEqualCstDateAndIiNameAndLocName(
                                currentDate, invItemLocation.getInvItem().getIiName(), invItemLocation.getInvLocation().getLocName(), AD_BRNCH, AD_CMPNY);

                        COST = invCosting.getCstRemainingQuantity() == 0 ? COST:
                            Math.abs(invCosting.getCstRemainingValue() / invCosting.getCstRemainingQuantity());

                    } catch (FinderException ex) {

                        COST = invItemLocation.getInvItem().getIiUnitCost();

                    }



                    TOTAL_COST += COST;

            }

            System.out.println("TOTAL_COST="+TOTAL_COST);

            return TOTAL_COST;

        } catch (Exception ex) {

            Debug.printStackTrace(ex);
            throw new EJBException(ex.getMessage());

        }

    }



	// SessionBean methods

	/**
	 * @ejb:create-method view-type="remote"
	 **/
	public void ejbCreate() throws CreateException {

		Debug.print("ApPurchaseRequisitionEntryControllerBean ejbCreate");

	}

}