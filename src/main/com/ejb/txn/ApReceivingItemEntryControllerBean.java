package com.ejb.txn;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;
import javax.ejb.RemoveException;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.naming.NamingException;

import com.ejb.ad.LocalAdAmountLimit;
import com.ejb.ad.LocalAdAmountLimitHome;
import com.ejb.ad.LocalAdApproval;
import com.ejb.ad.LocalAdApprovalHome;
import com.ejb.ad.LocalAdApprovalQueue;
import com.ejb.ad.LocalAdApprovalQueueHome;
import com.ejb.ad.LocalAdApprovalUser;
import com.ejb.ad.LocalAdApprovalUserHome;
import com.ejb.ad.LocalAdBranchApTaxCode;
import com.ejb.ad.LocalAdBranchApTaxCodeHome;
import com.ejb.ad.LocalAdBranchArTaxCode;
import com.ejb.ad.LocalAdBranchDocumentSequenceAssignment;
import com.ejb.ad.LocalAdBranchDocumentSequenceAssignmentHome;
import com.ejb.ad.LocalAdBranchItemLocation;
import com.ejb.ad.LocalAdBranchItemLocationHome;
import com.ejb.ad.LocalAdCompany;
import com.ejb.ad.LocalAdCompanyHome;
import com.ejb.ad.LocalAdDeleteAuditTrailHome;
import com.ejb.ad.LocalAdLookUpValue;
import com.ejb.ad.LocalAdLookUpValueHome;
import com.ejb.ad.LocalAdPaymentTerm;
import com.ejb.ad.LocalAdPaymentTermHome;
import com.ejb.ad.LocalAdPreference;
import com.ejb.ad.LocalAdPreferenceHome;
import com.ejb.ad.LocalAdUser;
import com.ejb.ad.LocalAdUserHome;
import com.ejb.ap.LocalApDistributionRecord;
import com.ejb.ap.LocalApDistributionRecordHome;
import com.ejb.ap.LocalApPurchaseOrder;
import com.ejb.ap.LocalApPurchaseOrderHome;
import com.ejb.ap.LocalApPurchaseOrderLine;
import com.ejb.ap.LocalApPurchaseOrderLineHome;
import com.ejb.ap.LocalApPurchaseRequisition;
import com.ejb.ap.LocalApSupplier;
import com.ejb.ap.LocalApSupplierHome;
import com.ejb.ap.LocalApTaxCode;
import com.ejb.ap.LocalApTaxCodeHome;
import com.ejb.ap.LocalApVoucherLineItem;
import com.ejb.ap.LocalApWithholdingTaxCodeHome;
import com.ejb.exception.AdPRFCoaGlPettyCashAccountNotFoundException;
import com.ejb.exception.AdPRFCoaGlVarianceAccountNotFoundException;
import com.ejb.exception.ApRINoPurchaseOrderLinesFoundException;
import com.ejb.exception.GlJREffectiveDateNoPeriodExistException;
import com.ejb.exception.GlJREffectiveDatePeriodClosedException;
import com.ejb.exception.GlobalBranchAccountNumberInvalidException;
import com.ejb.exception.GlobalConversionDateNotExistException;
import com.ejb.exception.GlobalDocumentNumberNotUniqueException;
import com.ejb.exception.GlobalInvItemLocationNotFoundException;
import com.ejb.exception.GlobalInvTagMissingException;
import com.ejb.exception.GlobalInvTagExistingException;
import com.ejb.exception.GlobalInventoryDateException;
import com.ejb.exception.GlobalJournalNotBalanceException;
import com.ejb.exception.GlobalMiscInfoIsRequiredException;
import com.ejb.exception.GlobalNoApprovalApproverFoundException;
import com.ejb.exception.GlobalNoApprovalRequesterFoundException;
import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.exception.GlobalPaymentTermInvalidException;
import com.ejb.exception.GlobalRecordAlreadyDeletedException;
import com.ejb.exception.GlobalTransactionAlreadyApprovedException;
import com.ejb.exception.GlobalTransactionAlreadyLockedException;
import com.ejb.exception.GlobalTransactionAlreadyPendingException;
import com.ejb.exception.GlobalTransactionAlreadyPostedException;
import com.ejb.exception.GlobalTransactionAlreadyVoidException;
import com.ejb.exception.GlobalTransactionAlreadyVoidPostedException;
import com.ejb.exception.GlobalReferenceNumberNotUniqueException;
import com.ejb.exception.GlobalSendEmailMessageException;
import com.ejb.exception.GlobalRecordInvalidException;
import com.ejb.exception.InvTagSerialNumberAlreadyExistException;
import com.ejb.genfld.LocalGenSegmentHome;
import com.ejb.genfld.LocalGenValueSetValueHome;
import com.ejb.gl.LocalAdDocumentSequenceAssignment;
import com.ejb.gl.LocalAdDocumentSequenceAssignmentHome;
import com.ejb.gl.LocalGlAccountingCalendarValue;
import com.ejb.gl.LocalGlAccountingCalendarValueHome;
import com.ejb.gl.LocalGlChartOfAccount;
import com.ejb.gl.LocalGlChartOfAccountBalance;
import com.ejb.gl.LocalGlChartOfAccountBalanceHome;
import com.ejb.gl.LocalGlChartOfAccountHome;
import com.ejb.gl.LocalGlForexLedger;
import com.ejb.gl.LocalGlForexLedgerHome;
import com.ejb.gl.LocalGlFunctionalCurrency;
import com.ejb.gl.LocalGlFunctionalCurrencyHome;
import com.ejb.gl.LocalGlFunctionalCurrencyRate;
import com.ejb.gl.LocalGlFunctionalCurrencyRateHome;
import com.ejb.gl.LocalGlJournal;
import com.ejb.gl.LocalGlJournalBatch;
import com.ejb.gl.LocalGlJournalBatchHome;
import com.ejb.gl.LocalGlJournalCategory;
import com.ejb.gl.LocalGlJournalCategoryHome;
import com.ejb.gl.LocalGlJournalHome;
import com.ejb.gl.LocalGlJournalLine;
import com.ejb.gl.LocalGlJournalLineHome;
import com.ejb.gl.LocalGlJournalSource;
import com.ejb.gl.LocalGlJournalSourceHome;
import com.ejb.gl.LocalGlSetOfBook;
import com.ejb.gl.LocalGlSetOfBookHome;
import com.ejb.gl.LocalGlSuspenseAccount;
import com.ejb.gl.LocalGlSuspenseAccountHome;
import com.ejb.inv.LocalInvAdjustment;
import com.ejb.inv.LocalInvAdjustmentHome;
import com.ejb.inv.LocalInvAdjustmentLine;
import com.ejb.inv.LocalInvAdjustmentLineHome;
import com.ejb.inv.LocalInvCosting;
import com.ejb.inv.LocalInvCostingHome;
import com.ejb.inv.LocalInvDistributionRecord;
import com.ejb.inv.LocalInvDistributionRecordHome;
import com.ejb.inv.LocalInvItem;
import com.ejb.inv.LocalInvItemHome;
import com.ejb.inv.LocalInvTag;
import com.ejb.inv.LocalInvTagHome;
import com.ejb.inv.LocalInvItemLocation;
import com.ejb.inv.LocalInvItemLocationHome;
import com.ejb.inv.LocalInvLocation;
import com.ejb.inv.LocalInvLocationHome;
import com.ejb.inv.LocalInvUnitOfMeasure;
import com.ejb.inv.LocalInvUnitOfMeasureConversion;
import com.ejb.inv.LocalInvUnitOfMeasureConversionHome;
import com.ejb.inv.LocalInvUnitOfMeasureHome;

import com.util.AbstractSessionBean;
import com.util.AdModApprovalQueueDetails;
import com.util.ApModCheckDetails;
import com.util.ApModPurchaseOrderDetails;
import com.util.ApModPurchaseOrderLineDetails;
import com.util.InvModTagListDetails;
import com.util.ApModSupplierDetails;
import com.util.ApPurchaseOrderDetails;
import com.util.ApTaxCodeDetails;
import com.util.Debug;
import com.util.EJBCommon;
import com.util.EJBHomeFactory;
import com.util.GlModFunctionalCurrencyDetails;
import com.util.InvModItemDetails;
import com.util.InvModUnitOfMeasureDetails;

/**
 * @ejb:bean name="ApReceivingItemEntryControllerEJB"
 *           display-name="used for entering receiving items"
 *           type="Stateless"
 *           view-type="remote"
 *           jndi-name="ejb/ApReceivingItemEntryControllerEJB"
 *
 * @ejb:interface remote-class="com.ejb.txn.ApReceivingItemEntryController"
 *                extends="javax.ejb.EJBObject"
 *
 * @ejb:home remote-class="com.ejb.txn.ApReceivingItemEntryControllerHome"
 *           extends="javax.ejb.EJBHome"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="apuser"
 *                        role-link="apuserlink"
 *
 * @ejb:permission role-name="apuser"
 *
*/

public class ApReceivingItemEntryControllerBean extends AbstractSessionBean {

    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList getGlFcAllWithDefault(Integer AD_CMPNY) {

        Debug.print("ApReceivingItemEntryControllerBean getGlFcAllWithDefault");

        LocalGlFunctionalCurrencyHome glFunctionalCurrencyHome = null;
        LocalAdCompanyHome adCompanyHome = null;

        Collection glFunctionalCurrencies = null;

        LocalGlFunctionalCurrency glFunctionalCurrency = null;
        LocalAdCompany adCompany = null;


        ArrayList list = new ArrayList();

        // Initialize EJB Home

        try {

            glFunctionalCurrencyHome = (LocalGlFunctionalCurrencyHome)EJBHomeFactory.
                lookUpLocalHome(LocalGlFunctionalCurrencyHome.JNDI_NAME, LocalGlFunctionalCurrencyHome.class);
            adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);

        } catch (NamingException ex) {

            throw new EJBException(ex.getMessage());

        }

        try {

            adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);

            glFunctionalCurrencies = glFunctionalCurrencyHome.findFcAllEnabled(
            	EJBCommon.getGcCurrentDateWoTime().getTime(), AD_CMPNY);

        } catch (Exception ex) {

            throw new EJBException(ex.getMessage());
        }

        if (glFunctionalCurrencies.isEmpty()) {

        	return null;

        }

        Iterator i = glFunctionalCurrencies.iterator();

        while (i.hasNext()) {

        	glFunctionalCurrency = (LocalGlFunctionalCurrency)i.next();

        	GlModFunctionalCurrencyDetails mdetails = new GlModFunctionalCurrencyDetails(
        	    glFunctionalCurrency.getFcCode(), glFunctionalCurrency.getFcName(),
        	    adCompany.getGlFunctionalCurrency().getFcName().equals(glFunctionalCurrency.getFcName()) ?
        	       EJBCommon.TRUE : EJBCommon.FALSE);

        	list.add(mdetails);

        }

        return list;

    }

    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList getAdPytAll(Integer AD_CMPNY) {

        Debug.print("ApReceivingItemEntryControllerBean getAdPytAll");

        LocalAdPaymentTermHome adPaymentTermHome = null;

        ArrayList list = new ArrayList();

        // Initialize EJB Home

        try {

            adPaymentTermHome = (LocalAdPaymentTermHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdPaymentTermHome.JNDI_NAME, LocalAdPaymentTermHome.class);

        } catch (NamingException ex) {

            throw new EJBException(ex.getMessage());

        }

        try {

        	Collection adPaymentTerms = adPaymentTermHome.findEnabledPytAll(AD_CMPNY);

        	Iterator i = adPaymentTerms.iterator();

        	while (i.hasNext()) {

        		LocalAdPaymentTerm adPaymentTerm = (LocalAdPaymentTerm)i.next();

        		list.add(adPaymentTerm.getPytName());

        	}

        	return list;

        } catch (Exception ex) {

        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());

        }

    }

    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList getApTcAll(Integer AD_CMPNY) {

        Debug.print("ApReceivingItemEntryControllerBean getApTcAll");

        LocalApTaxCodeHome apTaxCodeHome = null;

        ArrayList list = new ArrayList();

        // Initialize EJB Home

        try {

            apTaxCodeHome = (LocalApTaxCodeHome)EJBHomeFactory.
                lookUpLocalHome(LocalApTaxCodeHome.JNDI_NAME, LocalApTaxCodeHome.class);

        } catch (NamingException ex) {

            throw new EJBException(ex.getMessage());

        }

        try {

        	Collection apTaxCodes = apTaxCodeHome.findEnabledTcAll(AD_CMPNY);

        	Iterator i = apTaxCodes.iterator();

        	while (i.hasNext()) {

        		LocalApTaxCode apTaxCode = (LocalApTaxCode)i.next();

        		list.add(apTaxCode.getTcName());

        	}

        	return list;

        } catch (Exception ex) {

        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());

        }

    }

	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public ApTaxCodeDetails getApTcByTcName(String TC_NM, Integer AD_CMPNY) {

		Debug.print("ApPurchaseOrderEntryControllerBean getArTcByTcName");

		LocalApTaxCodeHome apTaxCodeHome = null;

		ArrayList list = new ArrayList();

		// Initialize EJB Home

		try {

			apTaxCodeHome = (LocalApTaxCodeHome)EJBHomeFactory.
			lookUpLocalHome(LocalApTaxCodeHome.JNDI_NAME, LocalApTaxCodeHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}

		try {

			LocalApTaxCode apTaxCode = apTaxCodeHome.findByTcName(TC_NM, AD_CMPNY);

			ApTaxCodeDetails details = new ApTaxCodeDetails();
			details.setTcType(apTaxCode.getTcType());
			details.setTcRate(apTaxCode.getTcRate());

			return details;

		} catch (Exception ex) {

			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());

		}

	}

    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList getApSplAll(Integer AD_BRNCH, Integer AD_CMPNY) {

        Debug.print("ApReceivingItemEntryControllerBean getApSplAll");

        LocalApSupplierHome apSupplierHome = null;

        ArrayList list = new ArrayList();

        // Initialize EJB Home

        try {

            apSupplierHome = (LocalApSupplierHome)EJBHomeFactory.
                lookUpLocalHome(LocalApSupplierHome.JNDI_NAME, LocalApSupplierHome.class);

        } catch (NamingException ex) {

            throw new EJBException(ex.getMessage());

        }

        try {

        	Collection apSuppliers = apSupplierHome.findEnabledSplAll(AD_BRNCH, AD_CMPNY);

        	Iterator i = apSuppliers.iterator();

        	while (i.hasNext()) {

        		LocalApSupplier apSupplier = (LocalApSupplier)i.next();

        		list.add(apSupplier.getSplSupplierCode());

        	}

        	return list;

        } catch (Exception ex) {

        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());

        }

    }

    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList getApRcvOpenPo(Integer AD_BRNCH, Integer AD_CMPNY) {

        Debug.print("ApReceivingItemEntryControllerBean getApRcvOpenPo");

        LocalApPurchaseOrderHome apPurchaseOrderHome = null;

        ArrayList list = new ArrayList();

        // Initialize EJB Home

        try {

            apPurchaseOrderHome = (LocalApPurchaseOrderHome)EJBHomeFactory.
                lookUpLocalHome(LocalApPurchaseOrderHome.JNDI_NAME, LocalApPurchaseOrderHome.class);

        } catch (NamingException ex) {

            throw new EJBException(ex.getMessage());

        }

        try {

        	Collection poNumbers = apPurchaseOrderHome.findPostedPoAll(AD_BRNCH, AD_CMPNY);

        	Iterator i = poNumbers.iterator();

        	while (i.hasNext()) {

        		LocalApPurchaseOrder apPurchaseOrder = (LocalApPurchaseOrder)i.next();

        		Collection apPurchaseOrderLineColl = apPurchaseOrder.getApPurchaseOrderLines();

        		Iterator j = apPurchaseOrderLineColl.iterator();
        		double totalPoUnServed=0;
        		while (j.hasNext()) {
        			LocalApPurchaseOrderLine apPurchaseOrderLine = (LocalApPurchaseOrderLine)j.next();
        			totalPoUnServed += apPurchaseOrderLine.getPlQuantity();
        		}

        		Collection apPurchaseOrderUnServedColl = apPurchaseOrderHome.findByPoRcvPoNumberAndPoReceivingAndPoAdBranchAndPoAdCompany(apPurchaseOrder.getPoDocumentNumber(), AD_BRNCH, AD_CMPNY);

        		Iterator k = apPurchaseOrderUnServedColl.iterator();
        		double totalPoServed=0;
        		while (k.hasNext()) {
        			LocalApPurchaseOrder apPurchaseOrderUnServed = (LocalApPurchaseOrder)k.next();
        			Collection apPurchaseOrderLineUnserved = apPurchaseOrderUnServed.getApPurchaseOrderLines();
        			Iterator l = apPurchaseOrderLineUnserved.iterator();
        			while (l.hasNext()) {
        				LocalApPurchaseOrderLine apPurchaseOrderLine = (LocalApPurchaseOrderLine)l.next();
        				totalPoServed += apPurchaseOrderLine.getPlQuantity();
        			}
        		}

        		if (totalPoServed<totalPoUnServed) list.add(apPurchaseOrder.getPoDocumentNumber());

        	}

        	return list;

        } catch (Exception ex) {

        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());

        }

    }

    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList getInvLocAll(Integer AD_CMPNY) {

        Debug.print("ApReceivingItemEntryControllerBean getInvLocAll");

        LocalInvLocationHome invLocationHome = null;
        Collection invLocations = null;
        ArrayList list = new ArrayList();

        // Initialize EJB Home

        try {

        	invLocationHome = (LocalInvLocationHome)EJBHomeFactory.
                lookUpLocalHome(LocalInvLocationHome.JNDI_NAME, LocalInvLocationHome.class);

        } catch (NamingException ex) {

            throw new EJBException(ex.getMessage());

        }

        try {

            invLocations = invLocationHome.findLocAll(AD_CMPNY);

	        if (invLocations.isEmpty()) {

	        	return null;

	        }

	        Iterator i = invLocations.iterator();

	        while (i.hasNext()) {

	        	LocalInvLocation invLocation = (LocalInvLocation)i.next();
	        	String details = invLocation.getLocName();

	        	list.add(details);

	        }

	        return list;

    	} catch (Exception ex) {

    		Debug.printStackTrace(ex);
    		throw new EJBException(ex.getMessage());

    	}

    }

    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList getInvUomByIiName(String II_NM, Integer AD_CMPNY) {

    	Debug.print("ApReceivingItemEntryControllerBean getInvUomByIiName");

        LocalInvUnitOfMeasureHome invUnitOfMeasureHome = null;
        LocalInvItemHome invItemHome = null;
        ArrayList list = new ArrayList();

        // Initialize EJB Home

        try {

        	invUnitOfMeasureHome = (LocalInvUnitOfMeasureHome)EJBHomeFactory.
                lookUpLocalHome(LocalInvUnitOfMeasureHome.JNDI_NAME, LocalInvUnitOfMeasureHome.class);
        	invItemHome = (LocalInvItemHome)EJBHomeFactory.
            	lookUpLocalHome(LocalInvItemHome.JNDI_NAME, LocalInvItemHome.class);

        } catch (NamingException ex) {

            throw new EJBException(ex.getMessage());
        }

        try {

        	LocalInvItem invItem = null;
        	LocalInvUnitOfMeasure invItemUnitOfMeasure = null;

        	invItem = invItemHome.findByIiName(II_NM, AD_CMPNY);
        	invItemUnitOfMeasure = invItem.getInvUnitOfMeasure();

        	Collection invUnitOfMeasures = null;
        	Iterator i = invUnitOfMeasureHome.findByUomAdLvClass(
        			invItemUnitOfMeasure.getUomAdLvClass(), AD_CMPNY).iterator();
        	while (i.hasNext()) {

        		LocalInvUnitOfMeasure invUnitOfMeasure = (LocalInvUnitOfMeasure) i.next();
        		InvModUnitOfMeasureDetails details = new InvModUnitOfMeasureDetails();
        		details.setUomName(invUnitOfMeasure.getUomName());
        		details.setUomConversionFactor(invUnitOfMeasure.getUomConversionFactor());
        		if (invUnitOfMeasure.getUomName().equals(invItemUnitOfMeasure.getUomName())) {

        			details.setDefault(true);

        		}

        		list.add(details);

        	}

        	return list;

        } catch (Exception ex) {

        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());

        }

    }
    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public boolean getInvTraceMisc(String II_NAME, Integer AD_CMPNY) {

        Debug.print("InvAdjustmentEntryControllerBean getInvLocAll");

        LocalInvLocationHome invLocationHome = null;
        LocalInvItemHome invItemHome = null;
        Collection invLocations = null;
        ArrayList list = new ArrayList();
        boolean isTraceMisc = false;
        // Initialize EJB Home

        try {

        	invLocationHome = (LocalInvLocationHome)EJBHomeFactory.
                lookUpLocalHome(LocalInvLocationHome.JNDI_NAME, LocalInvLocationHome.class);
        	invItemHome = (LocalInvItemHome)EJBHomeFactory.
                    lookUpLocalHome(LocalInvItemHome.JNDI_NAME, LocalInvItemHome.class);

        } catch (NamingException ex) {

            throw new EJBException(ex.getMessage());

        }

        try {

            LocalInvItem invItem = invItemHome.findByIiName(II_NAME, AD_CMPNY);
            if (invItem.getIiTraceMisc() == 1){
            	isTraceMisc = true;
            }
            return isTraceMisc;
    	} catch (Exception ex) {

    		Debug.printStackTrace(ex);
    		throw new EJBException(ex.getMessage());

    	}

    }

    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ApModPurchaseOrderDetails getApPoByPoRcvPoNumberAndSplSupplierCodeAndAdBranch(String PO_RCV_PO_NMBR, String SPL_SPPLR_CODE, Integer AD_BRNCH, Integer AD_CMPNY)
    	throws GlobalNoRecordFoundException,
    	ApRINoPurchaseOrderLinesFoundException {

    	Debug.print("ApReceivingItemEntryControllerBean getApPoByPoRcvPoNumberAndSplSupplierCodeAndAdBranch");

        LocalApPurchaseOrderHome apPurchaseOrderHome = null;
        LocalApPurchaseOrderLineHome apPurchaseOrderLineHome = null;
        LocalInvTagHome invTagHome = null;
        ArrayList list = new ArrayList();

        // Initialize EJB Home

        try {

        	apPurchaseOrderHome = (LocalApPurchaseOrderHome)EJBHomeFactory.
                lookUpLocalHome(LocalApPurchaseOrderHome.JNDI_NAME, LocalApPurchaseOrderHome.class);
        	apPurchaseOrderLineHome = (LocalApPurchaseOrderLineHome)EJBHomeFactory.
                lookUpLocalHome(LocalApPurchaseOrderLineHome.JNDI_NAME, LocalApPurchaseOrderLineHome.class);
        	invTagHome = (LocalInvTagHome)EJBHomeFactory.
                	lookUpLocalHome(LocalInvTagHome.JNDI_NAME, LocalInvTagHome.class);

        } catch (NamingException ex) {

            throw new EJBException(ex.getMessage());
        }

        LocalApPurchaseOrder apPurchaseOrder = null;

        try {

        	apPurchaseOrder = apPurchaseOrderHome.findByPoRcvPoNumberAndPoReceivingAndSplSupplierCodeAndBrCode(PO_RCV_PO_NMBR, EJBCommon.FALSE, SPL_SPPLR_CODE, AD_BRNCH, AD_CMPNY);

        	if (apPurchaseOrder.getPoPosted() != EJBCommon.TRUE) {

        		throw new GlobalNoRecordFoundException();

        	}

        	//TODO: get receiving item line

        	Collection apPurchaseOrderLines = apPurchaseOrder.getApPurchaseOrderLines();

        	String serialNumber = null;

    		Iterator i = apPurchaseOrderLines.iterator();

    		while (i.hasNext()) {

    			LocalApPurchaseOrderLine apPurchaseOrderLine = (LocalApPurchaseOrderLine) i.next();

    			ApModPurchaseOrderLineDetails plDetails = new ApModPurchaseOrderLineDetails();


    			plDetails.setTraceMisc(apPurchaseOrderLine.getInvItemLocation().getInvItem().getIiTraceMisc());
	        	System.out.println(plDetails.getTraceMisc() + "<== getTraceMisc under getInvAdjByAdjCode controllerbean");
	        	if (plDetails.getTraceMisc() == 1){

	        		ArrayList tagList = new ArrayList();

	        		tagList = this.getInvTagList(apPurchaseOrderLine);
	        		plDetails.setPlTagList(tagList);
	        		plDetails.setTraceMisc(plDetails.getTraceMisc());


	        	}
    			plDetails.setPlCode(apPurchaseOrderLine.getPlCode());
    			plDetails.setPlLine(apPurchaseOrderLine.getPlLine());
    			plDetails.setPlUnitCost(apPurchaseOrderLine.getPlUnitCost());
    			plDetails.setPlQcNumber(apPurchaseOrderLine.getPlQcNumber());
    			plDetails.setPlQcExpiryDate(apPurchaseOrderLine.getPlQcExpiryDate());
    			plDetails.setPlIiName(apPurchaseOrderLine.getInvItemLocation().getInvItem().getIiName());
    			plDetails.setPlLocName(apPurchaseOrderLine.getInvItemLocation().getInvLocation().getLocName());
    			plDetails.setPlUomName(apPurchaseOrderLine.getInvUnitOfMeasure().getUomName());
    			plDetails.setPlIiDescription(apPurchaseOrderLine.getInvItemLocation().getInvItem().getIiDescription());
    			plDetails.setPlDiscount1(apPurchaseOrderLine.getPlDiscount1());
    			plDetails.setPlDiscount2(apPurchaseOrderLine.getPlDiscount2());
    			plDetails.setPlDiscount3(apPurchaseOrderLine.getPlDiscount3());
    			plDetails.setPlDiscount4(apPurchaseOrderLine.getPlDiscount4());
    			plDetails.setPlMisc(apPurchaseOrderLine.getPlMisc());
    			plDetails.setPlConversionFactor(this.getConversionFactorByUomFromAndItem(apPurchaseOrderLine.getInvUnitOfMeasure(), apPurchaseOrderLine.getInvItemLocation().getInvItem(), AD_CMPNY));


    			double RCVD_QTY = 0d;
    			double RCVD_DSCNT = 0d;
    			double RCVD_AMNT = 0d;

    			Collection apReceivingItemLines = apPurchaseOrderLineHome.findByPlPlCode(apPurchaseOrderLine.getPlCode(), AD_CMPNY);

    			Iterator j = apReceivingItemLines.iterator();

    			while (j.hasNext()) {

    				LocalApPurchaseOrderLine apReceivingItemLine = (LocalApPurchaseOrderLine) j.next();

    				if (apReceivingItemLine.getApPurchaseOrder().getPoPosted() == EJBCommon.TRUE) {

    					RCVD_QTY += apReceivingItemLine.getPlQuantity();
    					RCVD_DSCNT += apReceivingItemLine.getPlTotalDiscount();
    					RCVD_AMNT += apReceivingItemLine.getPlAmount() + apReceivingItemLine.getPlTaxAmount();

    				}

    			}

    			double REMAINING_QTY = apPurchaseOrderLine.getPlQuantity() - RCVD_QTY;
    			double REMAINING_DSCNT = apPurchaseOrderLine.getPlTotalDiscount() - RCVD_DSCNT;
    			double REMAINING_AMNT = apPurchaseOrderLine.getPlAmount() - RCVD_AMNT;

    			if (REMAINING_QTY <= 0) continue;

    			plDetails.setPlRemaining(REMAINING_QTY < 0 ? 0 : REMAINING_QTY);
    			plDetails.setPlQuantity(REMAINING_QTY < 0 ? 0 : REMAINING_QTY);
    			plDetails.setPlTotalDiscount(REMAINING_DSCNT);
    			plDetails.setPlAmount(REMAINING_AMNT);

    			list.add(plDetails);

    		}

    		if (list == null || list.size() == 0) {

    			throw new ApRINoPurchaseOrderLinesFoundException();

    		}

    		ApModPurchaseOrderDetails mdetails = new ApModPurchaseOrderDetails();

    		mdetails.setPoCode(apPurchaseOrder.getPoCode());
    		mdetails.setPoTcName(apPurchaseOrder.getApTaxCode().getTcName());
    		mdetails.setPoTcType(apPurchaseOrder.getApTaxCode().getTcType());
    		mdetails.setPoTcRate(apPurchaseOrder.getApTaxCode().getTcRate());
    		mdetails.setPoFcName(apPurchaseOrder.getGlFunctionalCurrency().getFcName());
    		mdetails.setPoPytName(apPurchaseOrder.getAdPaymentTerm().getPytName());
    		mdetails.setPoSplName(apPurchaseOrder.getApSupplier().getSplName());
    		mdetails.setPoConversionDate(apPurchaseOrder.getPoConversionDate());
    		mdetails.setPoConversionRate(apPurchaseOrder.getPoConversionRate());
    		mdetails.setPoDescription(apPurchaseOrder.getPoDescription());
    		mdetails.setPoPlList(list);

    		return mdetails;

        } catch (FinderException ex) {

    		throw new GlobalNoRecordFoundException();

        } catch(GlobalNoRecordFoundException ex) {

        	getSessionContext().setRollbackOnly();
        	throw ex;

        } catch(ApRINoPurchaseOrderLinesFoundException ex) {

        	getSessionContext().setRollbackOnly();
        	throw ex;

        } catch (Exception ex) {

        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());

        }

    }

    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
     public double getInvIiUnitCostByIiNameAndUomName(String II_NM, String UOM_NM, Integer AD_CMPNY) {

        Debug.print("ApReceivingItemEntryControllerBean getInvIiUnitCostByIiNameAndUomName");

        LocalInvItemHome invItemHome = null;
        LocalInvUnitOfMeasureConversionHome invUnitOfMeasureConversionHome = null;
        LocalAdPreferenceHome adPreferenceHome = null;


        // Initialize EJB Home

        try {

        	invItemHome = (LocalInvItemHome)EJBHomeFactory.
               lookUpLocalHome(LocalInvItemHome.JNDI_NAME, LocalInvItemHome.class);
        	invUnitOfMeasureConversionHome = (LocalInvUnitOfMeasureConversionHome)EJBHomeFactory.
    			lookUpLocalHome(LocalInvUnitOfMeasureConversionHome.JNDI_NAME, LocalInvUnitOfMeasureConversionHome.class);
        	adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
            lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);

        } catch (NamingException ex) {

           throw new EJBException(ex.getMessage());

        }

        try {

        	LocalInvItem invItem = invItemHome.findByIiName(II_NM, AD_CMPNY);
        	LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);
        	LocalInvUnitOfMeasureConversion invUnitOfMeasureConversion = invUnitOfMeasureConversionHome.findUmcByIiNameAndUomName(II_NM, UOM_NM, AD_CMPNY);
        	LocalInvUnitOfMeasureConversion invDefaultUomConversion = invUnitOfMeasureConversionHome.findUmcByIiNameAndUomName(II_NM, invItem.getInvUnitOfMeasure().getUomName(), AD_CMPNY);

        	return EJBCommon.roundIt(invItem.getIiUnitCost() * invDefaultUomConversion.getUmcConversionFactor() / invUnitOfMeasureConversion.getUmcConversionFactor(), adPreference.getPrfInvCostPrecisionUnit());

        } catch (Exception ex) {

           Debug.printStackTrace(ex);
           throw new EJBException(ex.getMessage());

        }

     }

     /**
      * @ejb:interface-method view-type="remote"
      * @jboss:method-attributes read-only="true"
      **/
      public double getInvUmcByIiNameAndUomName(String II_NM, String UOM_NM, Integer AD_CMPNY) {

         Debug.print("ApReceivingItemEntryControllerBean getInvUmcByIiNameAndUomName");

         LocalInvItemHome invItemHome = null;
         LocalInvUnitOfMeasureConversionHome invUnitOfMeasureConversionHome = null;
         LocalAdPreferenceHome adPreferenceHome = null;


         // Initialize EJB Home

         try {

         	invItemHome = (LocalInvItemHome)EJBHomeFactory.
                lookUpLocalHome(LocalInvItemHome.JNDI_NAME, LocalInvItemHome.class);
         	invUnitOfMeasureConversionHome = (LocalInvUnitOfMeasureConversionHome)EJBHomeFactory.
     			lookUpLocalHome(LocalInvUnitOfMeasureConversionHome.JNDI_NAME, LocalInvUnitOfMeasureConversionHome.class);
         	adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
             lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);

         } catch (NamingException ex) {

            throw new EJBException(ex.getMessage());

         }

         try {

         	LocalInvItem invItem = invItemHome.findByIiName(II_NM, AD_CMPNY);
         	LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);
         	LocalInvUnitOfMeasureConversion invUnitOfMeasureConversion = invUnitOfMeasureConversionHome.findUmcByIiNameAndUomName(II_NM, UOM_NM, AD_CMPNY);
         	LocalInvUnitOfMeasureConversion invDefaultUomConversion = invUnitOfMeasureConversionHome.findUmcByIiNameAndUomName(II_NM, invItem.getInvUnitOfMeasure().getUomName(), AD_CMPNY);

         	return EJBCommon.roundIt(invDefaultUomConversion.getUmcConversionFactor() / invUnitOfMeasureConversion.getUmcConversionFactor(), adPreference.getPrfInvCostPrecisionUnit());

         } catch (Exception ex) {

            Debug.printStackTrace(ex);
            throw new EJBException(ex.getMessage());

         }

      }

      /**
       * @ejb:interface-method view-type="remote"
       * @jboss:method-attributes read-only="true"
       **/
      public ArrayList getAdUsrAll(Integer AD_CMPNY) {

          Debug.print("ApReceivingItemEntryControllerBean getAdUsrAll");

          LocalAdUserHome adUserHome = null;

          LocalAdUser adUser = null;

          Collection adUsers = null;

          ArrayList list = new ArrayList();

          // Initialize EJB Home

          try {

              adUserHome = (LocalAdUserHome)EJBHomeFactory.
                  lookUpLocalHome(LocalAdUserHome.JNDI_NAME, LocalAdUserHome.class);

          } catch (NamingException ex) {

              throw new EJBException(ex.getMessage());

          }

          try {

              adUsers = adUserHome.findUsrAll(AD_CMPNY);

          } catch (FinderException ex) {

          } catch (Exception ex) {

              throw new EJBException(ex.getMessage());
          }

          if (adUsers.isEmpty()) {

              return null;

          }

          Iterator i = adUsers.iterator();

          while (i.hasNext()) {

              adUser = (LocalAdUser)i.next();

              list.add(adUser.getUsrName());

          }

          return list;

      }

      /**
       * @ejb:interface-method view-type="remote"
       * @jboss:method-attributes read-only="true"
       **/
      public ArrayList getAdUsrInspectorAll(byte INSPCTR, Integer AD_CMPNY) {

          Debug.print("ApReceivingItemEntryControllerBean getAdUsrAll");

          LocalAdUserHome adUserHome = null;

          LocalAdUser adUser = null;

          Collection adUsers = null;

          ArrayList list = new ArrayList();

          // Initialize EJB Home

          try {

              adUserHome = (LocalAdUserHome)EJBHomeFactory.
                  lookUpLocalHome(LocalAdUserHome.JNDI_NAME, LocalAdUserHome.class);

          } catch (NamingException ex) {

              throw new EJBException(ex.getMessage());

          }

          try {

              adUsers = adUserHome.findUsrInspectorAll(INSPCTR, AD_CMPNY);

          } catch (FinderException ex) {

          } catch (Exception ex) {

              throw new EJBException(ex.getMessage());
          }

          if (adUsers.isEmpty()) {

              return null;

          }

          Iterator i = adUsers.iterator();

          while (i.hasNext()) {

              adUser = (LocalAdUser)i.next();

              list.add(adUser.getUsrName());

          }

          return list;

      }

    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ApModPurchaseOrderDetails getApPoByPoCode(Integer PO_CODE, Integer AD_CMPNY)
        throws GlobalNoRecordFoundException {

        Debug.print("ApReceivingItemEntryControllerBean getApPoByPoCode");

        LocalApPurchaseOrderHome apPurchaseOrderHome = null;
        LocalAdCompanyHome adCompanyHome = null;
        LocalGenValueSetValueHome genValueSetValueHome = null;
        LocalGenSegmentHome genSegmentHome = null;
        LocalApDistributionRecordHome apDistributionRecordHome = null;
        LocalApPurchaseOrderLineHome apPurchaseOrderLineHome = null;
        LocalInvTagHome invTagHome = null;
        LocalAdApprovalQueueHome adApprovalQueueHome = null;
        // Initialize EJB Home

        try {

            apPurchaseOrderHome = (LocalApPurchaseOrderHome)EJBHomeFactory.
                lookUpLocalHome(LocalApPurchaseOrderHome.JNDI_NAME, LocalApPurchaseOrderHome.class);
            adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
            genValueSetValueHome = (LocalGenValueSetValueHome)EJBHomeFactory.
                lookUpLocalHome(LocalGenValueSetValueHome.JNDI_NAME, LocalGenValueSetValueHome.class);
            genSegmentHome = (LocalGenSegmentHome)EJBHomeFactory.
                lookUpLocalHome(LocalGenSegmentHome.JNDI_NAME, LocalGenSegmentHome.class);
            apDistributionRecordHome = (LocalApDistributionRecordHome)EJBHomeFactory.
            	lookUpLocalHome(LocalApDistributionRecordHome.JNDI_NAME, LocalApDistributionRecordHome.class);
            apPurchaseOrderLineHome = (LocalApPurchaseOrderLineHome)EJBHomeFactory.
            	lookUpLocalHome(LocalApPurchaseOrderLineHome.JNDI_NAME, LocalApPurchaseOrderLineHome.class);
            invTagHome = (LocalInvTagHome)EJBHomeFactory.
                	lookUpLocalHome(LocalInvTagHome.JNDI_NAME, LocalInvTagHome.class);
        	adApprovalQueueHome = (LocalAdApprovalQueueHome)EJBHomeFactory.
		            lookUpLocalHome(LocalAdApprovalQueueHome.JNDI_NAME, LocalAdApprovalQueueHome.class);

        } catch (NamingException ex) {

            throw new EJBException(ex.getMessage());

        }
        //TODO: getApPoByPoCOde
        try {

        	LocalApPurchaseOrder apPurchaseOrder = null;


        	try {

        		apPurchaseOrder = apPurchaseOrderHome.findByPrimaryKey(PO_CODE);

        	} catch (FinderException ex) {

        		throw new GlobalNoRecordFoundException();

        	}

        	ArrayList list = new ArrayList();

        	// get receiving item line

        	Collection apPurchaseOrderLines = apPurchaseOrder.getApPurchaseOrderLines();

    		Iterator i = apPurchaseOrderLines.iterator();
    		while (i.hasNext()) {
    			LocalApPurchaseOrderLine apPurchaseOrderLine = (LocalApPurchaseOrderLine) i.next();

    			ApModPurchaseOrderLineDetails plDetails = new ApModPurchaseOrderLineDetails();

    			ArrayList tagList = new ArrayList();

    			plDetails.setPlCode(apPurchaseOrderLine.getPlCode());
    			plDetails.setPlLine(apPurchaseOrderLine.getPlLine());
    			System.out.println("dadaan ba d2?");
    			if (apPurchaseOrderLine.getPlPlCode() != null) {
    				double PL_RCVD = 0d;
	    			double PL_RMNNG = 0d;
	    			System.out.println("e d2?");
	    			Collection apReceivingItemLines = apPurchaseOrderLineHome.findByPlPlCode(apPurchaseOrderLine.getPlPlCode(), AD_CMPNY);

	    			Iterator j = apReceivingItemLines.iterator();

	    			while (j.hasNext()) {
	    				LocalApPurchaseOrderLine apReceivingItemLine = (LocalApPurchaseOrderLine) j.next();

	    				if (apReceivingItemLine.getApPurchaseOrder().getPoPosted() == EJBCommon.TRUE) {

	    					PL_RCVD += apReceivingItemLine.getPlQuantity();

	    				}

	    			}

	    			try {
	    				LocalApPurchaseOrderLine apPlLine = apPurchaseOrderLineHome.findByPrimaryKey(apPurchaseOrderLine.getPlPlCode());
	    				PL_RMNNG = apPlLine.getPlQuantity() - PL_RCVD;

	    				plDetails.setPlRemaining(PL_RMNNG < 0 ? 0 : PL_RMNNG);

	    			} catch (FinderException ex) {

	    			}


	    		}
    			plDetails.setTraceMisc(apPurchaseOrderLine.getInvItemLocation().getInvItem().getIiTraceMisc());
	        	System.out.println(plDetails.getTraceMisc() + "<== getTraceMisc under getInvAdjByAdjCode controllerbean");
	        	if (plDetails.getTraceMisc() == 1){


	        		tagList = this.getInvTagList(apPurchaseOrderLine);



	    			plDetails.setPlTagList(tagList);
	        	}
                plDetails.setPlPlCode(apPurchaseOrderLine.getPlPlCode());
    			plDetails.setPlQuantity(apPurchaseOrderLine.getPlQuantity());
    			plDetails.setPlUnitCost(apPurchaseOrderLine.getPlUnitCost());
    			plDetails.setPlQcNumber(apPurchaseOrderLine.getPlQcNumber());
    			plDetails.setPlQcExpiryDate(apPurchaseOrderLine.getPlQcExpiryDate());
    			plDetails.setPlIiName(apPurchaseOrderLine.getInvItemLocation().getInvItem().getIiName());
    			plDetails.setPlLocName(apPurchaseOrderLine.getInvItemLocation().getInvLocation().getLocName());
    			plDetails.setPlUomName(apPurchaseOrderLine.getInvUnitOfMeasure().getUomName());
    			plDetails.setPlIiDescription(apPurchaseOrderLine.getInvItemLocation().getInvItem().getIiDescription());
    			plDetails.setPlDiscount1(apPurchaseOrderLine.getPlDiscount1());
    			plDetails.setPlDiscount2(apPurchaseOrderLine.getPlDiscount2());
    			plDetails.setPlDiscount3(apPurchaseOrderLine.getPlDiscount3());
    			plDetails.setPlDiscount4(apPurchaseOrderLine.getPlDiscount4());
    			plDetails.setPlTotalDiscount(apPurchaseOrderLine.getPlTotalDiscount());
    			plDetails.setPlMisc(apPurchaseOrderLine.getPlMisc());
    			plDetails.setPlConversionFactor(apPurchaseOrderLine.getPlConversionFactor());
    			plDetails.setPlAddonCost(apPurchaseOrderLine.getPlAddonCost());

    			if (apPurchaseOrder.getApTaxCode().getTcType().equals("INCLUSIVE"))
    				plDetails.setPlAmount(apPurchaseOrderLine.getPlAmount() +
    					apPurchaseOrderLine.getPlTaxAmount());
    			else
    				plDetails.setPlAmount(apPurchaseOrderLine.getPlAmount());
    			//plDetails.setPlMisc(apPurchaseOrderLine.getPlMisc());
    			list.add(plDetails);

    		}


			Collection adApprovalQueues = adApprovalQueueHome.findByAqDocumentAndAqDocumentCodeAll("AP RECEIVING ITEM", apPurchaseOrder.getPoCode(), AD_CMPNY);

			System.out.println("adApprovalQueues="+adApprovalQueues.size());
			ArrayList approverlist = new ArrayList();

			i = adApprovalQueues.iterator();

			while (i.hasNext()) {
				LocalAdApprovalQueue adApprovalQueue = (LocalAdApprovalQueue)i.next();

				AdModApprovalQueueDetails adModApprovalQueueDetails = new AdModApprovalQueueDetails();
				adModApprovalQueueDetails.setAqApprovedDate(adApprovalQueue.getAqApprovedDate());
				adModApprovalQueueDetails.setAqApproverName(adApprovalQueue.getAdUser().getUsrDescription());
				adModApprovalQueueDetails.setAqStatus(adApprovalQueue.getAqApproved() == EJBCommon.TRUE ? adApprovalQueue.getAqLevel()+ " APPROVED" : adApprovalQueue.getAqLevel()+" PENDING");

				approverlist.add(adModApprovalQueueDetails);
			}

        	ApModPurchaseOrderDetails mPoDetails = new ApModPurchaseOrderDetails();

        	mPoDetails.setPoCode(apPurchaseOrder.getPoCode());
        	mPoDetails.setPoRcvPoNumber(apPurchaseOrder.getPoRcvPoNumber());
        	mPoDetails.setPoType(apPurchaseOrder.getPoType());
        	mPoDetails.setPoDescription(apPurchaseOrder.getPoDescription());
        	mPoDetails.setPoDate(apPurchaseOrder.getPoDate());
        	mPoDetails.setPoDocumentNumber(apPurchaseOrder.getPoDocumentNumber());
        	mPoDetails.setPoReferenceNumber(apPurchaseOrder.getPoReferenceNumber());
        	mPoDetails.setPoConversionDate(apPurchaseOrder.getPoConversionDate());
        	mPoDetails.setPoConversionRate(apPurchaseOrder.getPoConversionRate());
        	mPoDetails.setPoTotalAmount(apPurchaseOrder.getPoTotalAmount());
        	mPoDetails.setPoApprovalStatus(apPurchaseOrder.getPoApprovalStatus());
        	mPoDetails.setPoReasonForRejection(apPurchaseOrder.getPoReasonForRejection());
        	mPoDetails.setPoPosted(apPurchaseOrder.getPoPosted());
        	mPoDetails.setPoVoid(apPurchaseOrder.getPoVoid());
        	mPoDetails.setPoShipmentNumber(apPurchaseOrder.getPoShipmentNumber());
        	mPoDetails.setPoCreatedBy(apPurchaseOrder.getPoCreatedBy());
        	mPoDetails.setPoDateCreated(apPurchaseOrder.getPoDateCreated());
        	mPoDetails.setPoLastModifiedBy(apPurchaseOrder.getPoLastModifiedBy());
        	mPoDetails.setPoDateLastModified(apPurchaseOrder.getPoDateLastModified());
        	mPoDetails.setPoApprovedRejectedBy(apPurchaseOrder.getPoApprovedRejectedBy());
        	mPoDetails.setPoDateApprovedRejected(apPurchaseOrder.getPoDateApprovedRejected());
        	mPoDetails.setPoPostedBy(apPurchaseOrder.getPoPostedBy());
        	mPoDetails.setPoDatePosted(apPurchaseOrder.getPoDatePosted());
        	mPoDetails.setPoFcName(apPurchaseOrder.getGlFunctionalCurrency().getFcName());
        	mPoDetails.setPoTcName(apPurchaseOrder.getApTaxCode().getTcName());
        	mPoDetails.setPoTcType(apPurchaseOrder.getApTaxCode().getTcType());
        	mPoDetails.setPoTcRate(apPurchaseOrder.getApTaxCode().getTcRate());
        	mPoDetails.setPoSplSupplierCode(apPurchaseOrder.getApSupplier().getSplSupplierCode());
        	mPoDetails.setPoPytName(apPurchaseOrder.getAdPaymentTerm().getPytName());
        	mPoDetails.setPoLock(apPurchaseOrder.getPoLock());
        	mPoDetails.setPoSplName(apPurchaseOrder.getApSupplier().getSplName());
        	mPoDetails.setPoFreight(apPurchaseOrder.getPoFreight());
        	mPoDetails.setPoDuties(apPurchaseOrder.getPoDuties());
        	mPoDetails.setPoEntryFee(apPurchaseOrder.getPoEntryFee());
        	mPoDetails.setPoStorage(apPurchaseOrder.getPoStorage());
        	mPoDetails.setPoWharfageHandling(apPurchaseOrder.getPoWharfageHandling());


	
			
        	mPoDetails.setPoPlList(list);
        	
        	mPoDetails.setPoAPRList(approverlist);

        	return mPoDetails;

        } catch (GlobalNoRecordFoundException ex) {

        	throw ex;

        } catch (Exception ex) {

        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());

        }

    }


    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ApModSupplierDetails getApSplBySplSupplierCode(String SPL_SPPLR_CODE, Integer AD_CMPNY)
        throws GlobalNoRecordFoundException {

        Debug.print("ApReceivingItemEntryControllerBean getApSplBySplSupplierCode");

        LocalApSupplierHome apSupplierHome = null;

        // Initialize EJB Home

        try {

            apSupplierHome = (LocalApSupplierHome)EJBHomeFactory.
                lookUpLocalHome(LocalApSupplierHome.JNDI_NAME, LocalApSupplierHome.class);

        } catch (NamingException ex) {

            throw new EJBException(ex.getMessage());

        }

        try {

        	LocalApSupplier apSupplier = null;


        	try {

        		apSupplier = apSupplierHome.findBySplSupplierCode(SPL_SPPLR_CODE, AD_CMPNY);

        	} catch (FinderException ex) {

        		throw new GlobalNoRecordFoundException();

        	}

        	ApModSupplierDetails mdetails = new ApModSupplierDetails();

        	mdetails.setSplPytName(apSupplier.getAdPaymentTerm() != null ?
        	    apSupplier.getAdPaymentTerm().getPytName() : null);
        	mdetails.setSplName(apSupplier.getSplName());

        	if (apSupplier.getApSupplierClass().getApTaxCode() != null ){

        		mdetails.setSplScTcName( apSupplier.getApSupplierClass().getApTaxCode().getTcName());
        		mdetails.setSplScTcType( apSupplier.getApSupplierClass().getApTaxCode().getTcType());
        		mdetails.setSplScTcRate( apSupplier.getApSupplierClass().getApTaxCode().getTcRate());

        	}

        	return mdetails;

        } catch (GlobalNoRecordFoundException ex) {

        	throw ex;

        } catch (Exception ex) {

        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());

        }

    }

    /**
     * @ejb:interface-method view-type="remote"
     **/
    public Integer saveApPoEntryMobile(com.util.ApPurchaseOrderDetails details, String PYT_NM,
            String FC_NM, ArrayList plList, boolean isDraft, Integer AD_BRNCH, Integer AD_CMPNY) throws
        GlobalRecordAlreadyDeletedException,
		GlobalDocumentNumberNotUniqueException,
		GlobalConversionDateNotExistException,
		GlobalPaymentTermInvalidException,
		GlobalTransactionAlreadyPendingException,
		GlobalTransactionAlreadyPostedException,
		GlobalTransactionAlreadyVoidException,
		GlobalInvItemLocationNotFoundException,
		GlJREffectiveDateNoPeriodExistException,
		GlJREffectiveDatePeriodClosedException,
		GlobalJournalNotBalanceException,
		GlobalTransactionAlreadyLockedException,
		GlobalInventoryDateException,
		GlobalTransactionAlreadyVoidPostedException,
		GlobalBranchAccountNumberInvalidException,
		AdPRFCoaGlVarianceAccountNotFoundException,
		GlobalMiscInfoIsRequiredException, InvTagSerialNumberAlreadyExistException {

        Debug.print("ApReceivingItemEntryControllerBean saveApPoEntryMobile");

        LocalApPurchaseOrderHome apPurchaseOrderHome = null;
        LocalAdCompanyHome adCompanyHome = null;
        LocalGlFunctionalCurrencyHome glFunctionalCurrencyHome = null;
        LocalAdDocumentSequenceAssignmentHome adDocumentSequenceAssignmentHome = null;
        LocalAdPaymentTermHome adPaymentTermHome = null;
        LocalApTaxCodeHome apTaxCodeHome = null;
        LocalApWithholdingTaxCodeHome apWithholdingTaxCodeHome = null;
        LocalApSupplierHome apSupplierHome = null;
        LocalGlFunctionalCurrencyRateHome glFunctionalCurrencyRateHome = null;
        LocalAdApprovalHome adApprovalHome = null;
        LocalAdAmountLimitHome adAmountLimitHome = null;
        LocalAdApprovalUserHome adApprovalUserHome = null;
        LocalAdApprovalQueueHome adApprovalQueueHome = null;
        LocalAdPreferenceHome adPreferenceHome = null;
        LocalInvItemLocationHome invItemLocationHome = null;
        LocalInvLocationHome invLocationHome = null;
        LocalInvCostingHome invCostingHome = null;
        LocalAdBranchDocumentSequenceAssignmentHome adBranchDocumentSequenceAssignmentHome = null;
        LocalAdBranchItemLocationHome adBranchItemLocationHome = null;
        LocalInvTagHome invTagHome = null;
        LocalAdUserHome adUserHome = null;
        LocalAdBranchApTaxCodeHome adBranchApTaxCodeHome = null;

        LocalApPurchaseOrder apPurchaseOrder = null;

        // Initialize EJB Home
        Date txnStartDate = new Date();

        try {

        	adBranchApTaxCodeHome = (LocalAdBranchApTaxCodeHome)EJBHomeFactory.
                    lookUpLocalHome(LocalAdBranchApTaxCodeHome.JNDI_NAME, LocalAdBranchApTaxCodeHome.class);

            apPurchaseOrderHome = (LocalApPurchaseOrderHome)EJBHomeFactory.
                lookUpLocalHome(LocalApPurchaseOrderHome.JNDI_NAME, LocalApPurchaseOrderHome.class);
            adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
            glFunctionalCurrencyHome = (LocalGlFunctionalCurrencyHome)EJBHomeFactory.
                lookUpLocalHome(LocalGlFunctionalCurrencyHome.JNDI_NAME, LocalGlFunctionalCurrencyHome.class);
            adDocumentSequenceAssignmentHome = (LocalAdDocumentSequenceAssignmentHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdDocumentSequenceAssignmentHome.JNDI_NAME, LocalAdDocumentSequenceAssignmentHome.class);
            adPaymentTermHome = (LocalAdPaymentTermHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdPaymentTermHome.JNDI_NAME, LocalAdPaymentTermHome.class);
            apTaxCodeHome = (LocalApTaxCodeHome)EJBHomeFactory.
                lookUpLocalHome(LocalApTaxCodeHome.JNDI_NAME, LocalApTaxCodeHome.class);
            apSupplierHome = (LocalApSupplierHome)EJBHomeFactory.
                lookUpLocalHome(LocalApSupplierHome.JNDI_NAME, LocalApSupplierHome.class);
            glFunctionalCurrencyRateHome = (LocalGlFunctionalCurrencyRateHome)EJBHomeFactory.
                lookUpLocalHome(LocalGlFunctionalCurrencyRateHome.JNDI_NAME, LocalGlFunctionalCurrencyRateHome.class);
            adApprovalHome = (LocalAdApprovalHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdApprovalHome.JNDI_NAME, LocalAdApprovalHome.class);
            adAmountLimitHome = (LocalAdAmountLimitHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdAmountLimitHome.JNDI_NAME, LocalAdAmountLimitHome.class);
            adApprovalUserHome = (LocalAdApprovalUserHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdApprovalUserHome.JNDI_NAME, LocalAdApprovalUserHome.class);
            adApprovalQueueHome = (LocalAdApprovalQueueHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdApprovalQueueHome.JNDI_NAME, LocalAdApprovalQueueHome.class);
            adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);
            invItemLocationHome = (LocalInvItemLocationHome)EJBHomeFactory.
            	lookUpLocalHome(LocalInvItemLocationHome.JNDI_NAME, LocalInvItemLocationHome.class);
            invLocationHome = (LocalInvLocationHome)EJBHomeFactory.
            	lookUpLocalHome(LocalInvLocationHome.JNDI_NAME, LocalInvLocationHome.class);
            invCostingHome = (LocalInvCostingHome)EJBHomeFactory.
            	lookUpLocalHome(LocalInvCostingHome.JNDI_NAME, LocalInvCostingHome.class);
            adBranchDocumentSequenceAssignmentHome = (LocalAdBranchDocumentSequenceAssignmentHome)EJBHomeFactory.
        		lookUpLocalHome(LocalAdBranchDocumentSequenceAssignmentHome.JNDI_NAME, LocalAdBranchDocumentSequenceAssignmentHome.class);
            adBranchItemLocationHome = (LocalAdBranchItemLocationHome)EJBHomeFactory.
    			lookUpLocalHome(LocalAdBranchItemLocationHome.JNDI_NAME, LocalAdBranchItemLocationHome.class);
            invTagHome = (LocalInvTagHome)EJBHomeFactory.
    				lookUpLocalHome(LocalInvTagHome.JNDI_NAME, LocalInvTagHome.class);
            adUserHome = (LocalAdUserHome)EJBHomeFactory.
    				lookUpLocalHome(LocalAdUserHome.JNDI_NAME, LocalAdUserHome.class);

        } catch (NamingException ex) {

            throw new EJBException(ex.getMessage());

        }

        try {

        	// validate if receiving item is already deleted

        	try {

        		if (details.getPoCode() != null) {

        			apPurchaseOrder = apPurchaseOrderHome.findByPrimaryKey(details.getPoCode());

        		}

        	} catch (FinderException ex) {

        		throw new GlobalRecordAlreadyDeletedException();

        	}


        	// validate if receiving item is already deleted

        	try {

        		if (details.getPoCode() != null) {

        			apPurchaseOrder = apPurchaseOrderHome.findByPrimaryKey(details.getPoCode());

        		}

        	} catch (FinderException ex) {

        		throw new GlobalRecordAlreadyDeletedException();

        	}

        	// validate if receiving item is already posted, void, approved or pending

	        if (details.getPoCode() != null) {


	        	if (apPurchaseOrder.getPoApprovalStatus() != null) {

	        		if (apPurchaseOrder.getPoApprovalStatus().equals("APPROVED") ||
	        		    apPurchaseOrder.getPoApprovalStatus().equals("N/A")) {

	        		    throw new GlobalTransactionAlreadyApprovedException();


	        		} else if (apPurchaseOrder.getPoApprovalStatus().equals("PENDING")) {

	        			throw new GlobalTransactionAlreadyPendingException();

	        		}

	        	}

        		if (apPurchaseOrder.getPoPosted() == EJBCommon.TRUE) {

        			throw new GlobalTransactionAlreadyPostedException();

        		} else if (apPurchaseOrder.getPoVoid() == EJBCommon.TRUE) {

        			throw new GlobalTransactionAlreadyVoidException();

        		}

        	}

        	// set receiving item as void

	    	if (details.getPoCode() != null && details.getPoVoid() == EJBCommon.TRUE &&
	    	    apPurchaseOrder.getPoPosted() == EJBCommon.FALSE) {

        		apPurchaseOrder.setPoVoid(EJBCommon.TRUE);
        		apPurchaseOrder.setPoLastModifiedBy(details.getPoLastModifiedBy());
        		apPurchaseOrder.setPoDateLastModified(details.getPoDateLastModified());

        		return apPurchaseOrder.getPoCode();

	    	}

        	// validate if document number is unique document number is automatic then set next sequence
	    	Debug.print("ApReceivingItemEntryControllerBean saveApPoEntryMobile A");
	    	try {

	    	    LocalAdBranchDocumentSequenceAssignment adBranchDocumentSequenceAssignment = null;
	 			LocalAdDocumentSequenceAssignment adDocumentSequenceAssignment = null;

	        	if (details.getPoCode() == null) {

	        	    try {

	        	        adDocumentSequenceAssignment = adDocumentSequenceAssignmentHome.findByDcName("AP RECEIVING ITEM", AD_CMPNY);

	        	    } catch (FinderException ex) { }

	        	    try {

	 					adBranchDocumentSequenceAssignment = adBranchDocumentSequenceAssignmentHome.findBdsByDsaCodeAndBrCode(adDocumentSequenceAssignment.getDsaCode(), AD_BRNCH, AD_CMPNY);

	 				} catch (FinderException ex) { }


		    		LocalApPurchaseOrder apExistingReceivingItem = null;

		    		try {
		    			apExistingReceivingItem = apPurchaseOrderHome.findByPoDocumentNumberAndPoReceivingAndBrCode(
		        		    details.getPoDocumentNumber(), EJBCommon.TRUE, AD_BRNCH, AD_CMPNY);

		        	} catch (FinderException ex) {

		        	}

		            if (apExistingReceivingItem != null) {

		            	throw new GlobalDocumentNumberNotUniqueException();

		            }

			        if (adDocumentSequenceAssignment.getAdDocumentSequence().getDsNumberingType() == 'A' &&
			            (details.getPoDocumentNumber() == null || details.getPoDocumentNumber().trim().length() == 0)) {

			            while (true) {

			                if (adBranchDocumentSequenceAssignment == null || adBranchDocumentSequenceAssignment.getBdsNextSequence() == null) {

				            	try {

				            		apPurchaseOrderHome.findByPoDocumentNumberAndPoReceivingAndBrCode(adDocumentSequenceAssignment.getDsaNextSequence(), EJBCommon.TRUE, AD_BRNCH, AD_CMPNY);
				            		adDocumentSequenceAssignment.setDsaNextSequence(EJBCommon.incrementStringNumber(adDocumentSequenceAssignment.getDsaNextSequence()));

				            	} catch (FinderException ex) {

				            		details.setPoDocumentNumber(adDocumentSequenceAssignment.getDsaNextSequence());
				            		adDocumentSequenceAssignment.setDsaNextSequence(EJBCommon.incrementStringNumber(adDocumentSequenceAssignment.getDsaNextSequence()));

				            		break;

				            	}

			                } else {

			                    try {

				            		apPurchaseOrderHome.findByPoDocumentNumberAndPoReceivingAndBrCode(adBranchDocumentSequenceAssignment.getBdsNextSequence(), EJBCommon.TRUE, AD_BRNCH, AD_CMPNY);
				            		adBranchDocumentSequenceAssignment.setBdsNextSequence(EJBCommon.incrementStringNumber(adBranchDocumentSequenceAssignment.getBdsNextSequence()));

				            	} catch (FinderException ex) {

				            		details.setPoDocumentNumber(adBranchDocumentSequenceAssignment.getBdsNextSequence());
				            		adBranchDocumentSequenceAssignment.setBdsNextSequence(EJBCommon.incrementStringNumber(adBranchDocumentSequenceAssignment.getBdsNextSequence()));
						            break;

				            	}

			                }

			            }

			        }

			        Debug.print("ApReceivingItemEntryControllerBean saveApPoEntryMobile A1");

			    } else {

			    	LocalApPurchaseOrder apExistingReceivingItem = null;

			    	try {

			    		apExistingReceivingItem = apPurchaseOrderHome.findByPoDocumentNumberAndPoReceivingAndBrCode(
		        		    details.getPoDocumentNumber(), EJBCommon.TRUE, AD_BRNCH, AD_CMPNY);

		        	} catch (FinderException ex) {

		        	}

		        	if (apExistingReceivingItem != null &&
		                !apExistingReceivingItem.getPoCode().equals(details.getPoCode())) {

		            	throw new GlobalDocumentNumberNotUniqueException();

		            }

		            if (apPurchaseOrder.getPoDocumentNumber() != details.getPoDocumentNumber() &&
		                (details.getPoDocumentNumber() == null || details.getPoDocumentNumber().trim().length() == 0)) {

		                details.setPoDocumentNumber(apPurchaseOrder.getPoDocumentNumber());

		         	}

		            Debug.print("ApReceivingItemEntryControllerBean saveApPoEntryMobile A2");

			    }

	        	Debug.print("ApReceivingItemEntryControllerBean saveApPoEntryMobile A Done");

	    	} catch (GlobalDocumentNumberNotUniqueException ex) {

	 			getSessionContext().setRollbackOnly();
	 			throw ex;

	 		} catch (Exception ex) {

	 			Debug.printStackTrace(ex);
	 			getSessionContext().setRollbackOnly();
	 			throw new EJBException(ex.getMessage());

	 		}

		    // validate if conversion date exists
	 		Debug.print("ApReceivingItemEntryControllerBean saveApPoEntryMobile B");
	        try {

	      	    if (details.getPoConversionDate() != null) {

	      	    	LocalGlFunctionalCurrency glValidateFunctionalCurrency = glFunctionalCurrencyHome.findByFcName(FC_NM, AD_CMPNY);
	      	    	LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);

	      	    	if (!glValidateFunctionalCurrency.getFcName().equals("USD")) {

	      	    		LocalGlFunctionalCurrencyRate glFunctionalCurrencyRate =
	      	    			glFunctionalCurrencyRateHome.findByFcCodeAndDate(glValidateFunctionalCurrency.getFcCode(),
	      	    					details.getPoConversionDate(), AD_CMPNY);

	      	    	} else if (!adCompany.getGlFunctionalCurrency().getFcName().equals("USD")){

	      	    		LocalGlFunctionalCurrencyRate glFunctionalCurrencyRate =
	      	    			glFunctionalCurrencyRateHome.findByFcCodeAndDate(
	      	    					adCompany.getGlFunctionalCurrency().getFcCode(), details.getPoConversionDate(), AD_CMPNY);

	      	    	}

		        }
	      	  Debug.print("ApReceivingItemEntryControllerBean saveApPoEntryMobile B Done");

	        } catch (FinderException ex) {

	        	 throw new GlobalConversionDateNotExistException();

	        }

	        // validate if payment term has at least one payment schedule

	        if (adPaymentTermHome.findByPytName("IMMEDIATE", AD_CMPNY).getAdPaymentSchedules().isEmpty()) {

	        	throw new GlobalPaymentTermInvalidException();

	        }

	        // lock purchase order

	        LocalApPurchaseOrder apExistingPurchaseOrder = null;
	        Debug.print("ApReceivingItemEntryControllerBean saveApPoEntryMobile C");
	        if (details.getPoType().equals("PO MATCHED")) {

	        	try {

	    		    apExistingPurchaseOrder = apPurchaseOrderHome.findByPoDocumentNumberAndPoReceivingAndBrCode(
	        		    details.getPoRcvPoNumber(), EJBCommon.FALSE, AD_BRNCH, AD_CMPNY);

		        	if (apPurchaseOrder == null || (apPurchaseOrder != null
		        			&& !apPurchaseOrder.getPoRcvPoNumber().equals(details.getPoRcvPoNumber()))) {

			    		    if (apExistingPurchaseOrder.getPoLock() == EJBCommon.TRUE) {

			    		    	throw new GlobalTransactionAlreadyLockedException();

			    		    }

			    		    apExistingPurchaseOrder.setPoLock(EJBCommon.TRUE);

			    		    if (apPurchaseOrder != null && apPurchaseOrder.getPoRcvPoNumber() != null){

			    		    	LocalApPurchaseOrder apPreviousPO = null;

			    		    	apPreviousPO = apPurchaseOrderHome.findByPoDocumentNumberAndPoReceivingAndBrCode(
			    		    			apPurchaseOrder.getPoRcvPoNumber(), EJBCommon.FALSE, AD_BRNCH, AD_CMPNY);

			    		    	apPreviousPO.setPoLock(EJBCommon.FALSE);

			    		    }

			    		    if (apExistingPurchaseOrder.getPoVoid() == EJBCommon.TRUE) {

			        			throw new GlobalTransactionAlreadyVoidPostedException();

			        		}

		        	}

	        	} catch (FinderException ex) {

	        	}

	        }
	        else {

	        	//replace the empty string with null
	        	details.setPoRcvPoNumber(null);

	        }
	        Debug.print("ApReceivingItemEntryControllerBean saveApPoEntryMobile C Done");
	        // used in checking if receiving item should re-generate distribution records and re-calculate taxes
	        boolean isRecalculate = true;

        	// create receiving item

        	boolean newPurchaseOrder = false;
        	Debug.print("ApReceivingItemEntryControllerBean saveApPoEntryMobile D");

        	if (details.getPoCode() == null) {

        		apPurchaseOrder = apPurchaseOrderHome.create(EJBCommon.TRUE, details.getPoType(),
				    details.getPoDate(), details.getPoDeliveryPeriod(), details.getPoDocumentNumber(), details.getPoReferenceNumber(),
				    details.getPoRcvPoNumber(), details.getPoDescription(), null, null,
				    details.getPoConversionDate(), details.getPoConversionRate(), details.getPoTotalAmount(),
				    EJBCommon.FALSE, EJBCommon.FALSE, details.getPoShipmentNumber(),
				    null, EJBCommon.FALSE, null, details.getPoCreatedBy(), details.getPoDateCreated(),
        	    	details.getPoLastModifiedBy(), details.getPoDateLastModified(),
        	    	null, null, null, null, EJBCommon.FALSE,
        	    	details.getPoFreight(), details.getPoDuties(), details.getPoEntryFee(), details.getPoStorage(), details.getPoWharfageHandling(),
        	    	AD_BRNCH, AD_CMPNY);

        	    newPurchaseOrder = true;
        	    Debug.print("ApReceivingItemEntryControllerBean saveApPoEntryMobile D1");

        	} else {

        	    // check if critical fields are changed

        		if (!apPurchaseOrder.getApTaxCode().getTcName().equals("") ||
					!apPurchaseOrder.getApSupplier().getSplSupplierCode().equals("") ||
					!apPurchaseOrder.getAdPaymentTerm().getPytName().equals(PYT_NM) ||
					plList.size() != apPurchaseOrder.getApPurchaseOrderLines().size() ||
					apPurchaseOrder.getPoFreight() != details.getPoFreight() ||
					apPurchaseOrder.getPoDuties() != details.getPoDuties() ||
					apPurchaseOrder.getPoEntryFee() != details.getPoEntryFee() ||
					apPurchaseOrder.getPoStorage() != details.getPoStorage() ||
					apPurchaseOrder.getPoWharfageHandling() != details.getPoWharfageHandling() ||
					!(apPurchaseOrder.getPoDate().equals(details.getPoDate()))) {

        			isRecalculate = true;

        		} else if (plList.size() == apPurchaseOrder.getApPurchaseOrderLines().size()) {

        			Iterator ilIter = apPurchaseOrder.getApPurchaseOrderLines().iterator();
        			Iterator ilListIter = plList.iterator();

        			while (ilIter.hasNext()) {

        				LocalApPurchaseOrderLine apPurchaseOrderLine = (LocalApPurchaseOrderLine)ilIter.next();
        				ApModPurchaseOrderLineDetails mdetails = (ApModPurchaseOrderLineDetails)ilListIter.next();

        				if (!apPurchaseOrderLine.getInvItemLocation().getInvItem().getIiName().equals(mdetails.getPlIiName()) ||
        				    !apPurchaseOrderLine.getInvItemLocation().getInvLocation().getLocName().equals(mdetails.getPlLocName()) ||
        				    !apPurchaseOrderLine.getInvUnitOfMeasure().getUomName().equals(mdetails.getPlUomName()) ||
							apPurchaseOrderLine.getPlQuantity() != mdetails.getPlQuantity() ||
							apPurchaseOrderLine.getPlUnitCost() != mdetails.getPlUnitCost()||
							apPurchaseOrderLine.getPlTotalDiscount() != mdetails.getPlTotalDiscount()) {

        					isRecalculate = true;
        					break;

        				}

        			//	isRecalculate = false;

        			}
        			Debug.print("ApReceivingItemEntryControllerBean saveApPoEntryMobile D3");

        		} else {

        	//		isRecalculate = false;

        		}

        		apPurchaseOrder.setPoReceiving(EJBCommon.TRUE);
        		apPurchaseOrder.setPoDescription(details.getPoDescription());
        		apPurchaseOrder.setPoRcvPoNumber(details.getPoRcvPoNumber());
        		apPurchaseOrder.setPoType(details.getPoType());
        		apPurchaseOrder.setPoDate(details.getPoDate());
        		apPurchaseOrder.setPoDeliveryPeriod(details.getPoDeliveryPeriod());
        		apPurchaseOrder.setPoDocumentNumber(details.getPoDocumentNumber());
        		apPurchaseOrder.setPoReferenceNumber(details.getPoReferenceNumber());
        		apPurchaseOrder.setPoShipmentNumber(details.getPoShipmentNumber());
        		apPurchaseOrder.setPoFreight(details.getPoFreight());
        		apPurchaseOrder.setPoDuties(details.getPoDuties());
        		apPurchaseOrder.setPoEntryFee(details.getPoEntryFee());
        		apPurchaseOrder.setPoStorage(details.getPoStorage());
        		apPurchaseOrder.setPoWharfageHandling(details.getPoWharfageHandling());
        		apPurchaseOrder.setPoConversionDate(details.getPoConversionDate());
        		apPurchaseOrder.setPoConversionRate(details.getPoConversionRate());
        		apPurchaseOrder.setPoLastModifiedBy(details.getPoLastModifiedBy());
        		apPurchaseOrder.setPoDateLastModified(details.getPoDateLastModified());
        		apPurchaseOrder.setPoReasonForRejection(null);

        	}
        	Debug.print("ApReceivingItemEntryControllerBean saveApPoEntryMobile D Done");

        	// get original PO
        	LocalApPurchaseOrder apOrigPurchaseOrder = null;
        	apOrigPurchaseOrder = apPurchaseOrderHome.findByPoDocumentNumberAndBrCode(details.getPoRcvPoNumber(), AD_BRNCH, AD_CMPNY);
            LocalApTaxCode apTaxCode = apOrigPurchaseOrder.getApTaxCode();
            apPurchaseOrder.setPoReferenceNumber(apOrigPurchaseOrder.getPoReferenceNumber());
        	apPurchaseOrder.setAdPaymentTerm(apOrigPurchaseOrder.getAdPaymentTerm());
        	apPurchaseOrder.setApTaxCode(apOrigPurchaseOrder.getApTaxCode());
        	apPurchaseOrder.setGlFunctionalCurrency(apOrigPurchaseOrder.getGlFunctionalCurrency());
        	apPurchaseOrder.setApSupplier(apOrigPurchaseOrder.getApSupplier());

        	double TOTAL_AMOUNT = 0;

        	Debug.print("ApReceivingItemEntryControllerBean saveApPoEntryMobile E");
        	LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);

        	if (isRecalculate) {

	        	// remove all receiving item line

	        	Collection apPurchaseOrderLines = apPurchaseOrder.getApPurchaseOrderLines();

		  	    Iterator i = apPurchaseOrderLines.iterator();

		  	    while (i.hasNext()) {

		  	   	    LocalApPurchaseOrderLine apPurchaseOrderLine = (LocalApPurchaseOrderLine)i.next();

		  	   	    i.remove();

		  	  	    apPurchaseOrderLine.remove();

		  	    }

	        	// remove all distribution records

		  	    Collection apDistributionRecords = apPurchaseOrder.getApDistributionRecords();

		  	    i = apDistributionRecords.iterator();

		  	    while (i.hasNext()) {

		  	   	    LocalApDistributionRecord apDistributionRecord = (LocalApDistributionRecord)i.next();

		  	  	    i.remove();

		  	  	    apDistributionRecord.remove();

		  	    }

		  	  Debug.print("ApReceivingItemEntryControllerBean saveApPoEntryMobile E01");

		  	    // add new receiving item line and distribution record

		  	    double TOTAL_TAX = 0d;
		  	    double TOTAL_LINE = 0d;


		  	    LocalInvItemHome invItemHome = null;
		  	    LocalApPurchaseOrderLineHome apPurchaseOrderLineHome = null;

		        try {


		        	invItemHome = (LocalInvItemHome)EJBHomeFactory.
		            	lookUpLocalHome(LocalInvItemHome.JNDI_NAME, LocalInvItemHome.class);
		        	apPurchaseOrderLineHome = (LocalApPurchaseOrderLineHome)EJBHomeFactory.
			            	lookUpLocalHome(LocalApPurchaseOrderLineHome.JNDI_NAME, LocalApPurchaseOrderLineHome.class);

		        } catch (NamingException ex) {

		            throw new EJBException(ex.getMessage());
		        }

		        //get all inv tags that was already received
		        Collection invTagColl = invTagHome.findAllByPlCodeNotNull(AD_CMPNY);

	      	    i = plList.iterator();
	      	    LocalInvItemLocation invItemLocation = null;

	      	    while (i.hasNext()) {

	      	  	    ApModPurchaseOrderLineDetails mPlDetails = (ApModPurchaseOrderLineDetails) i.next();

	      	  	    try {
	      	  	    	System.out.println("AD_CMPNY: " + AD_CMPNY);
	      	  	    	System.out.println("mPlDetails.getPlPartNumber(): " + mPlDetails.getPlPartNumber());
	      	  	    	System.out.println("InvItem: " + invItemHome.findByPartNumber(mPlDetails.getPlPartNumber(), AD_CMPNY));

	      	  	    	LocalInvItem invItem = invItemHome.findByPartNumber(mPlDetails.getPlPartNumber(), AD_CMPNY);

	      	  	    	System.out.println("InvItemLocation: " + invLocationHome.findByPrimaryKey(invItem.getIiDefaultLocation()));

	      	  	    	LocalInvLocation invLocation = invLocationHome.findByPrimaryKey(invItem.getIiDefaultLocation());
	      	  	    	invItemLocation = invItemLocationHome.findByLocNameAndIiName(invLocation.getLocName(), invItem.getIiName(), AD_CMPNY);

	      	  	    	LocalApPurchaseOrderLine apOrigPurchaseOrderLine = apPurchaseOrderLineHome.findPlByPoNumberAndIiNameAndLocName(apOrigPurchaseOrder.getPoDocumentNumber(),invItem.getIiName(), invLocation.getLocName(), apOrigPurchaseOrder.getPoAdBranch(),AD_CMPNY);

	      	  	    	mPlDetails.setPlCode(apOrigPurchaseOrderLine.getPlCode());
	      	  	    	mPlDetails.setPlLine(apOrigPurchaseOrderLine.getPlLine());
	      	  	    	mPlDetails.setPlIiName(invItem.getIiName());
	      	  	    	System.out.println("LOCATION: " + invLocation.getLocName());
	      	  	    	mPlDetails.setPlLocName(invLocation.getLocName());
	      	  	    	mPlDetails.setPlUomName(apOrigPurchaseOrderLine.getInvUnitOfMeasure().getUomName());
	      	  	    	mPlDetails.setPlUnitCost(apOrigPurchaseOrderLine.getPlUnitCost());
	      	  	    	mPlDetails.setPlAmount(mPlDetails.getPlUnitCost()*mPlDetails.getPlQuantity());
	      	  	    	mPlDetails.setPlConversionFactor(apOrigPurchaseOrderLine.getPlConversionFactor());
	      	  	    	mPlDetails.setPlPlCode(apOrigPurchaseOrderLine.getPlCode());
	      	  	    	mPlDetails.setPlDiscount1(apOrigPurchaseOrderLine.getPlDiscount1());
	      	  	    	mPlDetails.setPlDiscount2(apOrigPurchaseOrderLine.getPlDiscount2());
	      	  	    	mPlDetails.setPlDiscount3(apOrigPurchaseOrderLine.getPlDiscount3());
	      	  	    	mPlDetails.setPlDiscount4(apOrigPurchaseOrderLine.getPlDiscount4());
	      	  	    	mPlDetails.setPlTotalDiscount(apOrigPurchaseOrderLine.getPlTotalDiscount());
	      	  	        mPlDetails.setPlConversionFactor(this.getConversionFactorByUomFromAndItem(apOrigPurchaseOrderLine.getInvUnitOfMeasure(), apOrigPurchaseOrderLine.getInvItemLocation().getInvItem(), AD_CMPNY));
	      	  	    	//mPlDetails

	      	  	    } catch (FinderException ex) {

	      	  	    	throw new GlobalInvItemLocationNotFoundException(String.valueOf(mPlDetails.getPlLine()));

	      	  	    }

	      	  	    //	start date validation


	      	  	    if(mPlDetails.getPlQuantity() > 0 && adPreference.getPrfArAllowPriorDate() == EJBCommon.FALSE) {

	      	  	    	Collection invNegTxnCosting = invCostingHome.findNegTxnByGreaterThanCstDateAndIiNameAndLocName(
	      	  	    			apPurchaseOrder.getPoDate(), invItemLocation.getInvItem().getIiName(),
								invItemLocation.getInvLocation().getLocName(), AD_BRNCH, AD_CMPNY);
	      	  	    	if(!invNegTxnCosting.isEmpty()) throw new GlobalInventoryDateException(invItemLocation.getInvItem().getIiName());

	      	  	    }


	      	  	    LocalApPurchaseOrderLine apPurchaseOrderLine = this.addApPlEntry(mPlDetails, apPurchaseOrder, invItemLocation, newPurchaseOrder, AD_CMPNY);

	      	  	    if(mPlDetails.getPlQuantity() == 0) continue;
	      	  	    // add inventory distributions

	      	  	    LocalAdBranchItemLocation adBranchItemLocation = null;

	      	  	    try {
	      	  	    	adBranchItemLocation = adBranchItemLocationHome.findBilByIlCodeAndBrCode(apPurchaseOrderLine.getInvItemLocation().getIlCode(), AD_BRNCH, AD_CMPNY);

	      	  	    } catch(FinderException ex) {

	      	  	    }

	      	  	    if(adBranchItemLocation != null) {

		      	  	    // Use AdBranchItemLocation Coa Account
	      	  	    	this.addApDrPlEntry(apPurchaseOrder.getApDrNextLine(),
	      	  	    			"INVENTORY", EJBCommon.TRUE, apPurchaseOrderLine.getPlAmount(),
								adBranchItemLocation.getBilCoaGlInventoryAccount(), apPurchaseOrder, AD_BRNCH, AD_CMPNY);

	      	  	    } else {

	      	  	    	// Use default account
	      	  	    	this.addApDrPlEntry(apPurchaseOrder.getApDrNextLine(),
	      	  	    			"INVENTORY", EJBCommon.TRUE, apPurchaseOrderLine.getPlAmount(),
								apPurchaseOrderLine.getInvItemLocation().getIlGlCoaInventoryAccount(), apPurchaseOrder, AD_BRNCH, AD_CMPNY);


	      	  	    }

	      	  	    TOTAL_LINE += apPurchaseOrderLine.getPlAmount();
	      	  	    TOTAL_TAX += apPurchaseOrderLine.getPlTaxAmount();
	      	  	    Iterator t = null;

		      	  	LocalInvItem invItem = invItemHome.findByIiName(mPlDetails.getPlIiName(), AD_CMPNY);
		      	  	if(invItem.getIiTraceMisc() == 1){//change this
		      	  		t = mPlDetails.getPlTagList().iterator();
	   	  	    	}
		      	  	LocalInvTag invTag  = null;

		      	  	if (invItem.getIiPartNumber().charAt(2)=='1') {
		      	  		if(invItem.getIiTraceMisc() == 1){
		      	  		while (t.hasNext()){
		      	  			InvModTagListDetails tgLstDetails = (InvModTagListDetails)t.next();

			      	  	    //check if the serial number was already received else throw an exception
			      	  	    Iterator v = invTagColl.iterator();
			      	  	    while (v.hasNext()) {
			      	  	    	LocalInvTag invExistingTag = (LocalInvTag)(v.next());
			      	  	    	if (tgLstDetails.getTgSerialNumber().equals(invExistingTag.getTgSerialNumber())) {
			      	  	    		
			      	  	    		
			      	  	    		throw new InvTagSerialNumberAlreadyExistException(tgLstDetails.getTgSerialNumber());
			      	  	    	}
			      	  	    }

			      	  	    if (tgLstDetails.getTgCode()==null){
			      	  	    	invTag = invTagHome.create(tgLstDetails.getTgPropertyCode(),
				      	  	    tgLstDetails.getTgSerialNumber(),null,tgLstDetails.getTgExpiryDate(),
				      	  	    tgLstDetails.getTgSpecs(), AD_CMPNY, tgLstDetails.getTgTransactionDate(),
	      	  	    			tgLstDetails.getTgType());

			      	  	    	invTag.setApPurchaseOrderLine(apPurchaseOrderLine);
				      	  	    LocalAdUser adUser = null;
				      	  	    try {
				      	  	    	adUser = adUserHome.findByUsrName(tgLstDetails.getTgCustodian(), AD_CMPNY);
				      	  	    }catch(FinderException ex){

				      	  	    }
				      	  	    if(invItem.getIiNonInventoriable()==1){
				      	  	    	invTag.setTgDocumentNumber(adPreference.getPrfInvNextCustodianNumber2());
				      	  	    	adPreference.setPrfInvNextCustodianNumber2(EJBCommon.incrementStringNumber(adPreference.getPrfInvNextCustodianNumber2()));
				      	  	    }else{
				      	  	    	invTag.setTgDocumentNumber(adPreference.getPrfInvNextCustodianNumber1());
			      	  	    		adPreference.setPrfInvNextCustodianNumber1(EJBCommon.incrementStringNumber(adPreference.getPrfInvNextCustodianNumber1()));
				      	  	    }
				      	  	    invTag.setAdUser(adUser);
			      	  	    }
		      	  		}
		      	  	}
		      	  	}

	      	    }
	      	    Debug.print("ApReceivingItemEntryControllerBean saveApPoEntryMobile E02");
	      	    // add tax distribution if necessary

	      	    if (!apTaxCode.getTcType().equals("NONE") &&
	        	    !apTaxCode.getTcType().equals("EXEMPT")) {

	      	    	if (adPreference.getPrfApUseAccruedVat() == EJBCommon.FALSE) {

	      	    		 LocalAdBranchApTaxCode adBranchTaxCode = null;
                         Debug.print("ApReceivingItemEntryControllerBean saveArInvIliEntry J");

                         try {
                       	  adBranchTaxCode = adBranchApTaxCodeHome.findBtcByTcCodeAndBrCode(apPurchaseOrder.getApTaxCode().getTcCode(), AD_BRNCH, AD_CMPNY);

                         } catch(FinderException ex) {

                         }

                         if(adBranchTaxCode != null){


                       	  this.addApDrPlEntry(apPurchaseOrder.getApDrNextLine(),
	          			      	        "TAX", EJBCommon.TRUE, TOTAL_TAX,  adBranchTaxCode.getBtcGlCoaTaxCode(),
	          			      	        apPurchaseOrder, AD_BRNCH, AD_CMPNY);

                         } else {

                       	  this.addApDrPlEntry(apPurchaseOrder.getApDrNextLine(),
	          			      	        "TAX", EJBCommon.TRUE, TOTAL_TAX,  apTaxCode.getGlChartOfAccount().getCoaCode(),
	          			      	        apPurchaseOrder, AD_BRNCH, AD_CMPNY);

                         }

	      	    	} else {

	      	    		this.addApDrPlEntry(apPurchaseOrder.getApDrNextLine(),
			      	        "ACC TAX", EJBCommon.TRUE, TOTAL_TAX, adPreference.getPrfApGlCoaAccruedVatAccount(),
			      	        apPurchaseOrder, AD_BRNCH, AD_CMPNY);

	      	    	}

		        }

	      	    TOTAL_AMOUNT = TOTAL_LINE + TOTAL_TAX;

	      	} else {

		  	    LocalInvItemHome invItemHome = null;
		  	    LocalApPurchaseOrderLineHome apPurchaseOrderLineHome = null;

		        try {

		        	invItemHome = (LocalInvItemHome)EJBHomeFactory.
		            	lookUpLocalHome(LocalInvItemHome.JNDI_NAME, LocalInvItemHome.class);
		        	apPurchaseOrderLineHome = (LocalApPurchaseOrderLineHome)EJBHomeFactory.
			            	lookUpLocalHome(LocalApPurchaseOrderLineHome.JNDI_NAME, LocalApPurchaseOrderLineHome.class);

		        } catch (NamingException ex) {

		            throw new EJBException(ex.getMessage());
		        }

	      		Iterator i = plList.iterator();

	      	    LocalInvItemLocation invItemLocation = null;

	      	    while (i.hasNext()) {

	      	  	    ApModPurchaseOrderLineDetails mPlDetails = (ApModPurchaseOrderLineDetails) i.next();

	      	  	    try {

	      	  	    	LocalInvItem invItem = invItemHome.findByPartNumber(mPlDetails.getPlPartNumber(), AD_CMPNY);
	      	  	    	LocalInvLocation invLocation = invLocationHome.findByPrimaryKey(invItem.getIiDefaultLocation());
	      	  	    	invItemLocation = invItemLocationHome.findByLocNameAndIiName(invLocation.getLocName(), invItem.getIiName(), AD_CMPNY);

	      	  	    	LocalApPurchaseOrderLine apOrigPurchaseOrderLine = apPurchaseOrderLineHome.findPlByPoNumberAndIiNameAndLocName(apOrigPurchaseOrder.getPoDocumentNumber(),invItem.getIiName(), invLocation.getLocName(), apOrigPurchaseOrder.getPoAdBranch(),AD_CMPNY);

	      	  	    	mPlDetails.setPlCode(apOrigPurchaseOrderLine.getPlCode());
	      	  	    	mPlDetails.setPlLine(apOrigPurchaseOrderLine.getPlLine());
	      	  	    	mPlDetails.setPlIiName(invItem.getIiName());
	      	  	    	System.out.println("LOCATION: " + invLocation.getLocName());
	      	  	    	mPlDetails.setPlLocName(invLocation.getLocName());
	      	  	    	mPlDetails.setPlUomName(apOrigPurchaseOrderLine.getInvUnitOfMeasure().getUomName());
	      	  	    	mPlDetails.setPlUnitCost(apOrigPurchaseOrderLine.getPlUnitCost());
	      	  	    	mPlDetails.setPlAmount(mPlDetails.getPlUnitCost()*mPlDetails.getPlQuantity());
	      	  	    	mPlDetails.setPlConversionFactor(apOrigPurchaseOrderLine.getPlConversionFactor());
	      	  	    	mPlDetails.setPlPlCode(apOrigPurchaseOrderLine.getPlCode());
	      	  	    	mPlDetails.setPlDiscount1(apOrigPurchaseOrderLine.getPlDiscount1());
	      	  	    	mPlDetails.setPlDiscount2(apOrigPurchaseOrderLine.getPlDiscount2());
	      	  	    	mPlDetails.setPlDiscount3(apOrigPurchaseOrderLine.getPlDiscount3());
	      	  	    	mPlDetails.setPlDiscount4(apOrigPurchaseOrderLine.getPlDiscount4());
	      	  	    	mPlDetails.setPlTotalDiscount(apOrigPurchaseOrderLine.getPlTotalDiscount());
	      	  	        mPlDetails.setPlConversionFactor(this.getConversionFactorByUomFromAndItem(apOrigPurchaseOrderLine.getInvUnitOfMeasure(), apOrigPurchaseOrderLine.getInvItemLocation().getInvItem(), AD_CMPNY));


	 	  	    	} catch (FinderException ex) {

	 	  	    		throw new GlobalInvItemLocationNotFoundException(String.valueOf(mPlDetails.getPlLine()));

	 	  	    	}

 	 	  	        //	start date validation
	 	  	    	if (adPreference.getPrfArAllowPriorDate() == EJBCommon.FALSE) {
		 	  	    	Collection invNegTxnCosting = invCostingHome.findNegTxnByGreaterThanCstDateAndIiNameAndLocName(
		  	    			apPurchaseOrder.getPoDate(), invItemLocation.getInvItem().getIiName(),
							invItemLocation.getInvLocation().getLocName(), AD_BRNCH, AD_CMPNY);
		  	    		if(!invNegTxnCosting.isEmpty()) throw new GlobalInventoryDateException(invItemLocation.getInvItem().getIiName());
	 	  	    	}

	      	    }

	      	    Debug.print("ApReceivingItemEntryControllerBean saveApPoEntryMobile E03");

				Collection apDistributionRecords = apPurchaseOrder.getApDistributionRecords();

        		i = apDistributionRecords.iterator();

        		while(i.hasNext()) {

        			LocalApDistributionRecord apDistributionRecord = (LocalApDistributionRecord)i.next();

        			if(apDistributionRecord.getDrDebit() == 0) {

        				TOTAL_AMOUNT += apDistributionRecord.getDrAmount();

        			}
        		}

	      	}

        	Debug.print("ApReceivingItemEntryControllerBean saveApPoEntryMobile E Done");
		    // generate approval status

            String PO_APPRVL_STATUS = null;

        	if (!isDraft) {

        		PO_APPRVL_STATUS = "N/A";

        		// release purchase order lock

        		if (details.getPoType().equals("PO MATCHED")) {

        			apExistingPurchaseOrder.setPoLock(EJBCommon.FALSE);

    	        }

        	}


        	if (PO_APPRVL_STATUS != null && PO_APPRVL_STATUS.equals("N/A")) {

        		this.executeApReceivingPost(apPurchaseOrder.getPoCode(), apPurchaseOrder.getPoLastModifiedBy(), AD_BRNCH, AD_CMPNY);

        	}

        	// set receiving item approval status

        	apPurchaseOrder.setPoApprovalStatus(PO_APPRVL_STATUS);
      	    return apPurchaseOrder.getPoCode();


        } catch (GlobalRecordAlreadyDeletedException ex) {

        	getSessionContext().setRollbackOnly();
        	throw ex;

        } catch (GlobalDocumentNumberNotUniqueException ex) {

        	getSessionContext().setRollbackOnly();
        	throw ex;

        } catch (GlobalConversionDateNotExistException ex) {

        	getSessionContext().setRollbackOnly();
        	throw ex;

        } catch (GlobalPaymentTermInvalidException ex) {

        	getSessionContext().setRollbackOnly();
        	throw ex;

        } catch (GlobalTransactionAlreadyPendingException ex) {

        	getSessionContext().setRollbackOnly();
        	throw ex;

        } catch (GlobalTransactionAlreadyPostedException ex) {

        	getSessionContext().setRollbackOnly();
        	throw ex;

        } catch (GlobalTransactionAlreadyVoidException ex) {

        	getSessionContext().setRollbackOnly();
        	throw ex;

        } catch (GlobalInvItemLocationNotFoundException ex) {

        	getSessionContext().setRollbackOnly();
        	throw ex;

        } catch (GlJREffectiveDateNoPeriodExistException ex) {

        	getSessionContext().setRollbackOnly();
        	throw ex;

        } catch (GlJREffectiveDatePeriodClosedException ex) {

        	getSessionContext().setRollbackOnly();
        	throw ex;

        } catch (GlobalJournalNotBalanceException ex) {

        	getSessionContext().setRollbackOnly();
        	throw ex;

        } catch (GlobalTransactionAlreadyLockedException ex) {

        	getSessionContext().setRollbackOnly();
        	throw ex;

        } catch (GlobalInventoryDateException ex) {

        	getSessionContext().setRollbackOnly();
        	throw ex;

        } catch (GlobalTransactionAlreadyVoidPostedException ex) {

        	getSessionContext().setRollbackOnly();
        	throw ex;

        } catch (GlobalBranchAccountNumberInvalidException ex) {

            getSessionContext().setRollbackOnly();
            throw ex;

        } catch (AdPRFCoaGlVarianceAccountNotFoundException ex){

            getSessionContext().setRollbackOnly();
            throw ex;

        } catch (GlobalMiscInfoIsRequiredException ex){

            getSessionContext().setRollbackOnly();
            throw ex;

        } catch (InvTagSerialNumberAlreadyExistException ex){

            getSessionContext().setRollbackOnly();
            throw ex;

        } catch (Exception ex) {

        	Debug.printStackTrace(ex);
        	getSessionContext().setRollbackOnly();
        	throw new EJBException(ex.getMessage());

        }

    }

    /**
     * @ejb:interface-method view-type="remote"
     **/
    public Integer saveApPoEntry(com.util.ApPurchaseOrderDetails details, boolean recalculateJournal, String PYT_NM, String TC_NM,
        String FC_NM, String SPL_SPPLR_CODE, ArrayList plList, boolean isDraft, Integer AD_BRNCH, Integer AD_CMPNY) throws
        GlobalRecordAlreadyDeletedException,
		GlobalDocumentNumberNotUniqueException,
		GlobalConversionDateNotExistException,
		GlobalPaymentTermInvalidException,
		GlobalTransactionAlreadyPendingException,
		GlobalTransactionAlreadyPostedException,
		GlobalTransactionAlreadyVoidException,
		GlobalInvItemLocationNotFoundException,
		GlobalNoApprovalRequesterFoundException,
		GlobalNoApprovalApproverFoundException,
		GlobalInvTagMissingException,
		GlobalInvTagExistingException,
		GlJREffectiveDateNoPeriodExistException,
		GlJREffectiveDatePeriodClosedException,
		GlobalJournalNotBalanceException,
		GlobalTransactionAlreadyLockedException,
		GlobalInventoryDateException,
		GlobalTransactionAlreadyVoidPostedException,
		GlobalBranchAccountNumberInvalidException,
		AdPRFCoaGlVarianceAccountNotFoundException,
		GlobalMiscInfoIsRequiredException,
		GlobalRecordInvalidException,
		GlobalReferenceNumberNotUniqueException,
		InvTagSerialNumberAlreadyExistException {

        Debug.print("ApReceivingItemEntryControllerBean saveApPoEntry");

        LocalApPurchaseOrderHome apPurchaseOrderHome = null;
        LocalAdCompanyHome adCompanyHome = null;
        LocalGlFunctionalCurrencyHome glFunctionalCurrencyHome = null;
        LocalAdDocumentSequenceAssignmentHome adDocumentSequenceAssignmentHome = null;
        LocalAdPaymentTermHome adPaymentTermHome = null;
        LocalApTaxCodeHome apTaxCodeHome = null;
        LocalApWithholdingTaxCodeHome apWithholdingTaxCodeHome = null;
        LocalApSupplierHome apSupplierHome = null;
        LocalGlFunctionalCurrencyRateHome glFunctionalCurrencyRateHome = null;
        LocalAdApprovalHome adApprovalHome = null;
        LocalAdAmountLimitHome adAmountLimitHome = null;
        LocalAdApprovalUserHome adApprovalUserHome = null;
        LocalAdApprovalQueueHome adApprovalQueueHome = null;
        LocalAdPreferenceHome adPreferenceHome = null;
        LocalInvItemLocationHome invItemLocationHome = null;
        LocalInvCostingHome invCostingHome = null;
        LocalAdBranchDocumentSequenceAssignmentHome adBranchDocumentSequenceAssignmentHome = null;
        LocalAdBranchItemLocationHome adBranchItemLocationHome = null;
        LocalApPurchaseOrderLineHome apPurchaseOrderLineHome = null;
        LocalInvTagHome invTagHome = null;
        LocalAdUserHome adUserHome = null;
        LocalApPurchaseOrder apPurchaseOrder = null;
        LocalInvItemHome invItemHome = null;
        LocalAdBranchApTaxCodeHome adBranchApTaxCodeHome = null;

        // Initialize EJB Home
        Date txnStartDate = new Date();

        try {

        	adBranchApTaxCodeHome = (LocalAdBranchApTaxCodeHome)EJBHomeFactory.
                    lookUpLocalHome(LocalAdBranchApTaxCodeHome.JNDI_NAME, LocalAdBranchApTaxCodeHome.class);
            apPurchaseOrderHome = (LocalApPurchaseOrderHome)EJBHomeFactory.
                lookUpLocalHome(LocalApPurchaseOrderHome.JNDI_NAME, LocalApPurchaseOrderHome.class);
            adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
            glFunctionalCurrencyHome = (LocalGlFunctionalCurrencyHome)EJBHomeFactory.
                lookUpLocalHome(LocalGlFunctionalCurrencyHome.JNDI_NAME, LocalGlFunctionalCurrencyHome.class);
            adDocumentSequenceAssignmentHome = (LocalAdDocumentSequenceAssignmentHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdDocumentSequenceAssignmentHome.JNDI_NAME, LocalAdDocumentSequenceAssignmentHome.class);
            adPaymentTermHome = (LocalAdPaymentTermHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdPaymentTermHome.JNDI_NAME, LocalAdPaymentTermHome.class);
            apTaxCodeHome = (LocalApTaxCodeHome)EJBHomeFactory.
                lookUpLocalHome(LocalApTaxCodeHome.JNDI_NAME, LocalApTaxCodeHome.class);
            apSupplierHome = (LocalApSupplierHome)EJBHomeFactory.
                lookUpLocalHome(LocalApSupplierHome.JNDI_NAME, LocalApSupplierHome.class);
            glFunctionalCurrencyRateHome = (LocalGlFunctionalCurrencyRateHome)EJBHomeFactory.
                lookUpLocalHome(LocalGlFunctionalCurrencyRateHome.JNDI_NAME, LocalGlFunctionalCurrencyRateHome.class);
            adApprovalHome = (LocalAdApprovalHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdApprovalHome.JNDI_NAME, LocalAdApprovalHome.class);
            adAmountLimitHome = (LocalAdAmountLimitHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdAmountLimitHome.JNDI_NAME, LocalAdAmountLimitHome.class);
            adApprovalUserHome = (LocalAdApprovalUserHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdApprovalUserHome.JNDI_NAME, LocalAdApprovalUserHome.class);
            adApprovalQueueHome = (LocalAdApprovalQueueHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdApprovalQueueHome.JNDI_NAME, LocalAdApprovalQueueHome.class);
            adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);
            invItemLocationHome = (LocalInvItemLocationHome)EJBHomeFactory.
                lookUpLocalHome(LocalInvItemLocationHome.JNDI_NAME, LocalInvItemLocationHome.class);
            invCostingHome = (LocalInvCostingHome)EJBHomeFactory.
            	lookUpLocalHome(LocalInvCostingHome.JNDI_NAME, LocalInvCostingHome.class);
            adBranchDocumentSequenceAssignmentHome = (LocalAdBranchDocumentSequenceAssignmentHome)EJBHomeFactory.
        		lookUpLocalHome(LocalAdBranchDocumentSequenceAssignmentHome.JNDI_NAME, LocalAdBranchDocumentSequenceAssignmentHome.class);
            adBranchItemLocationHome = (LocalAdBranchItemLocationHome)EJBHomeFactory.
    			lookUpLocalHome(LocalAdBranchItemLocationHome.JNDI_NAME, LocalAdBranchItemLocationHome.class);
            apPurchaseOrderLineHome = (LocalApPurchaseOrderLineHome)EJBHomeFactory.
				lookUpLocalHome(LocalApPurchaseOrderLineHome.JNDI_NAME, LocalApPurchaseOrderLineHome.class);
            invTagHome = (LocalInvTagHome)EJBHomeFactory.
    				lookUpLocalHome(LocalInvTagHome.JNDI_NAME, LocalInvTagHome.class);

            adUserHome = (LocalAdUserHome)EJBHomeFactory.
    				lookUpLocalHome(LocalAdUserHome.JNDI_NAME, LocalAdUserHome.class);
            invItemHome = (LocalInvItemHome)EJBHomeFactory.
    				lookUpLocalHome(LocalInvItemHome.JNDI_NAME, LocalInvItemHome.class);

        } catch (NamingException ex) {

            throw new EJBException(ex.getMessage());

        }

        try {

        	// validate if receiving item is already deleted

        	try {

        		if (details.getPoCode() != null) {

        			apPurchaseOrder = apPurchaseOrderHome.findByPrimaryKey(details.getPoCode());

        		}

        	} catch (FinderException ex) {

        		throw new GlobalRecordAlreadyDeletedException();

        	}

        	// validate if receiving item is already posted, void, approved or pending

	        if (details.getPoCode() != null) {


	        	if (apPurchaseOrder.getPoApprovalStatus() != null) {

	        		if (apPurchaseOrder.getPoApprovalStatus().equals("APPROVED") ||
	        		    apPurchaseOrder.getPoApprovalStatus().equals("N/A")) {

	        		    throw new GlobalTransactionAlreadyApprovedException();


	        		} else if (apPurchaseOrder.getPoApprovalStatus().equals("PENDING")) {

	        			throw new GlobalTransactionAlreadyPendingException();

	        		}

	        	}

        		if (apPurchaseOrder.getPoPosted() == EJBCommon.TRUE) {

        			throw new GlobalTransactionAlreadyPostedException();

        		} else if (apPurchaseOrder.getPoVoid() == EJBCommon.TRUE) {

        			throw new GlobalTransactionAlreadyVoidException();

        		}

        	}

        	// set receiving item as void

	    	if (details.getPoCode() != null && details.getPoVoid() == EJBCommon.TRUE &&
	    	    apPurchaseOrder.getPoPosted() == EJBCommon.FALSE) {

        		apPurchaseOrder.setPoVoid(EJBCommon.TRUE);
        		apPurchaseOrder.setPoLastModifiedBy(details.getPoLastModifiedBy());
        		apPurchaseOrder.setPoDateLastModified(details.getPoDateLastModified());

        		return apPurchaseOrder.getPoCode();

	    	}

        	// validate if document number is unique document number is automatic then set next sequence

	    	this.validateDocumentNumber(details, AD_BRNCH, AD_CMPNY, apPurchaseOrderHome,
            adDocumentSequenceAssignmentHome, adBranchDocumentSequenceAssignmentHome,
            apPurchaseOrder);



	        // validate if payment term has at least one payment schedule

	        if (adPaymentTermHome.findByPytName(PYT_NM, AD_CMPNY).getAdPaymentSchedules().isEmpty()) {

	        	throw new GlobalPaymentTermInvalidException();

	        }

	        // validate if shipment number exists
	        if (details.getPoShipmentNumber() != null && details.getPoShipmentNumber().length() > 0) {

	        	Collection apExistingApPurchaseOrderLine = apPurchaseOrderLineHome.findByPoReceivingAndPoShipmentAndBrCode(EJBCommon.FALSE, details.getPoShipmentNumber(), AD_CMPNY);
	        	if (apExistingApPurchaseOrderLine.size() == 0) throw new GlobalReferenceNumberNotUniqueException();
	        }



		     // validate if conversion date exists

	            try {

	                if (details.getPoConversionDate() != null) {


	                    LocalGlFunctionalCurrency glValidateFunctionalCurrency = glFunctionalCurrencyHome.findByFcName(FC_NM, AD_CMPNY);
	                    LocalGlFunctionalCurrencyRate glFunctionalCurrencyRate =
	                        glFunctionalCurrencyRateHome.findByFcCodeAndDate(glValidateFunctionalCurrency.getFcCode(),
	                                details.getPoConversionDate(), AD_CMPNY);

	                    details.setPoConversionRate(glFunctionalCurrencyRate.getFrXToUsd());
	                }

	            } catch (FinderException ex) {

	                throw new GlobalConversionDateNotExistException();

	            }


	        // lock purchase order

	        LocalApPurchaseOrder apExistingPurchaseOrder = null;

	        if (details.getPoType().equals("PO MATCHED")) {

	        	try {

	    		    apExistingPurchaseOrder = apPurchaseOrderHome.findByPoDocumentNumberAndPoReceivingAndBrCode(
	        		    details.getPoRcvPoNumber(), EJBCommon.FALSE, AD_BRNCH, AD_CMPNY);

		        	if (apPurchaseOrder == null || (apPurchaseOrder != null
		        			&& !apPurchaseOrder.getPoRcvPoNumber().equals(details.getPoRcvPoNumber()))) {

			    		    if (apExistingPurchaseOrder.getPoLock() == EJBCommon.TRUE) {

			    		    	throw new GlobalTransactionAlreadyLockedException();

			    		    }

			    		    apExistingPurchaseOrder.setPoLock(EJBCommon.TRUE);

			    		    if (apPurchaseOrder != null && apPurchaseOrder.getPoRcvPoNumber() != null){

			    		    	LocalApPurchaseOrder apPreviousPO = null;

			    		    	apPreviousPO = apPurchaseOrderHome.findByPoDocumentNumberAndPoReceivingAndBrCode(
			    		    			apPurchaseOrder.getPoRcvPoNumber(), EJBCommon.FALSE, AD_BRNCH, AD_CMPNY);

			    		    	apPreviousPO.setPoLock(EJBCommon.FALSE);

			    		    }

			    		    if (apExistingPurchaseOrder.getPoVoid() == EJBCommon.TRUE) {

			        			throw new GlobalTransactionAlreadyVoidPostedException();

			        		}

		        	}

	        	} catch (FinderException ex) {

	        	}

	        }

	        // used in checking if receiving item should re-generate distribution records and re-calculate taxes
	        boolean isRecalculate = true;

        	// create receiving item

        	boolean newPurchaseOrder = false;

        	if (details.getPoCode() == null) {

        		apPurchaseOrder = apPurchaseOrderHome.create(EJBCommon.TRUE, details.getPoType(),
				    details.getPoDate(), details.getPoDeliveryPeriod(), details.getPoDocumentNumber(), details.getPoReferenceNumber(),
				    details.getPoRcvPoNumber(), details.getPoDescription(), null, null,
				    details.getPoConversionDate(), details.getPoConversionRate(), details.getPoTotalAmount(),
				    EJBCommon.FALSE, EJBCommon.FALSE, details.getPoShipmentNumber(),
				    null, EJBCommon.FALSE, null, details.getPoCreatedBy(), details.getPoDateCreated(),
        	    	details.getPoLastModifiedBy(), details.getPoDateLastModified(),
        	    	null, null, null, null, EJBCommon.FALSE,
        	    	details.getPoFreight(), details.getPoDuties(), details.getPoEntryFee(), details.getPoStorage(), details.getPoWharfageHandling(),
        	    	AD_BRNCH, AD_CMPNY);

        		
        		
        		recalculateJournal = true;
        	    newPurchaseOrder = true;

        	} else {

        	    // check if critical fields are changed

        		if (!apPurchaseOrder.getApTaxCode().getTcName().equals(TC_NM) ||
					!apPurchaseOrder.getApSupplier().getSplSupplierCode().equals(SPL_SPPLR_CODE) ||
					!apPurchaseOrder.getAdPaymentTerm().getPytName().equals(PYT_NM) ||
					plList.size() != apPurchaseOrder.getApPurchaseOrderLines().size() ||
					apPurchaseOrder.getPoFreight() != details.getPoFreight() ||
					apPurchaseOrder.getPoDuties() != details.getPoDuties() ||
					apPurchaseOrder.getPoEntryFee() != details.getPoEntryFee() ||
					apPurchaseOrder.getPoStorage() != details.getPoStorage() ||
					apPurchaseOrder.getPoWharfageHandling() != details.getPoWharfageHandling() ||
					!(apPurchaseOrder.getPoDate().equals(details.getPoDate()))) {
        			System.out.println("enter recal  T 1");
        			isRecalculate = true;
        			

        		} else if (plList.size() == apPurchaseOrder.getApPurchaseOrderLines().size()) {

        			Iterator ilIter = apPurchaseOrder.getApPurchaseOrderLines().iterator();
        			Iterator ilListIter = plList.iterator();

        			while (ilIter.hasNext()) {

        				LocalApPurchaseOrderLine apPurchaseOrderLine = (LocalApPurchaseOrderLine)ilIter.next();
        				ApModPurchaseOrderLineDetails mdetails = (ApModPurchaseOrderLineDetails)ilListIter.next();

        			
        				
        				if (!apPurchaseOrderLine.getInvItemLocation().getInvItem().getIiName().equals(mdetails.getPlIiName()) ||
        				    !apPurchaseOrderLine.getInvItemLocation().getInvLocation().getLocName().equals(mdetails.getPlLocName()) ||
        				    !apPurchaseOrderLine.getInvUnitOfMeasure().getUomName().equals(mdetails.getPlUomName()) ||
							apPurchaseOrderLine.getPlQuantity() != mdetails.getPlQuantity() ||
							apPurchaseOrderLine.getPlUnitCost() != mdetails.getPlUnitCost()||
							apPurchaseOrderLine.getPlAmount() != mdetails.getPlAmount()

							//apPurchaseOrderLine.getPlQcNumber() != mdetails.getPlQcNumber()||
						
						//	apPurchaseOrderLine.getPlTotalDiscount() != mdetails.getPlTotalDiscount()) {
        						) {
        					isRecalculate = true;
        					
        					System.out.println("enter recal  T 2");
        					break;

        				}
        	

        			}

        		}

        		apPurchaseOrder.setPoReceiving(EJBCommon.TRUE);
        		apPurchaseOrder.setPoDescription(details.getPoDescription());
        		apPurchaseOrder.setPoRcvPoNumber(details.getPoRcvPoNumber());
        		apPurchaseOrder.setPoType(details.getPoType());
        		apPurchaseOrder.setPoDate(details.getPoDate());
        		apPurchaseOrder.setPoDeliveryPeriod(details.getPoDeliveryPeriod());
        		apPurchaseOrder.setPoDocumentNumber(details.getPoDocumentNumber());
        		apPurchaseOrder.setPoReferenceNumber(details.getPoReferenceNumber());
        		apPurchaseOrder.setPoShipmentNumber(details.getPoShipmentNumber());
        		apPurchaseOrder.setPoFreight(details.getPoFreight());
        		apPurchaseOrder.setPoDuties(details.getPoDuties());
        		apPurchaseOrder.setPoEntryFee(details.getPoEntryFee());
        		apPurchaseOrder.setPoStorage(details.getPoStorage());
        		apPurchaseOrder.setPoWharfageHandling(details.getPoWharfageHandling());
        		apPurchaseOrder.setPoConversionDate(details.getPoConversionDate());
        		apPurchaseOrder.setPoConversionRate(details.getPoConversionRate());
        		apPurchaseOrder.setPoTotalAmount(details.getPoTotalAmount());
        		apPurchaseOrder.setPoLastModifiedBy(details.getPoLastModifiedBy());
        		apPurchaseOrder.setPoDateLastModified(details.getPoDateLastModified());
        		apPurchaseOrder.setPoReasonForRejection(null);

        	}


        	LocalAdPaymentTerm adPaymentTerm = adPaymentTermHome.findByPytName(PYT_NM, AD_CMPNY);
        	apPurchaseOrder.setAdPaymentTerm(adPaymentTerm);

        	LocalApTaxCode apTaxCode = apTaxCodeHome.findByTcName(TC_NM, AD_CMPNY);
        	apPurchaseOrder.setApTaxCode(apTaxCode);

        	LocalGlFunctionalCurrency glFunctionalCurrency = glFunctionalCurrencyHome.findByFcName(FC_NM, AD_CMPNY);
        	apPurchaseOrder.setGlFunctionalCurrency(glFunctionalCurrency);

        	LocalApSupplier apSupplier = apSupplierHome.findBySplSupplierCode(SPL_SPPLR_CODE, AD_CMPNY);
        	apPurchaseOrder.setApSupplier(apSupplier);

        	

        	LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);
			LocalAdUser adUser = adUserHome.findByUsrName(apPurchaseOrder.getPoCreatedBy(), AD_CMPNY);

            double TOTAL_AMOUNT = this.calculateJournal(plList, AD_BRNCH, AD_CMPNY, invItemLocationHome,
                invCostingHome, adBranchItemLocationHome, invTagHome, adUserHome, apPurchaseOrder,
                invItemHome, adBranchApTaxCodeHome, isRecalculate, newPurchaseOrder, apTaxCode,
                adPreference, adUser); 

			// set receiving item approval status

			this.setupApprovalStatus(details, isDraft, AD_BRNCH, AD_CMPNY, adApprovalHome, adAmountLimitHome,
          adApprovalUserHome, adApprovalQueueHome, apPurchaseOrder, apExistingPurchaseOrder,
          adPreference, adUser, TOTAL_AMOUNT);

	  	    return apPurchaseOrder.getPoCode();


        } catch (GlobalRecordAlreadyDeletedException ex) {

        	getSessionContext().setRollbackOnly();
        	throw ex;

        } catch (GlobalDocumentNumberNotUniqueException ex) {

        	getSessionContext().setRollbackOnly();
        	throw ex;

        } catch (GlobalConversionDateNotExistException ex) {

        	getSessionContext().setRollbackOnly();
        	throw ex;


        } catch (GlobalPaymentTermInvalidException ex) {

        	getSessionContext().setRollbackOnly();
        	throw ex;

        } catch (GlobalTransactionAlreadyPendingException ex) {

        	getSessionContext().setRollbackOnly();
        	throw ex;

        } catch (GlobalTransactionAlreadyPostedException ex) {

        	getSessionContext().setRollbackOnly();
        	throw ex;

        } catch (GlobalTransactionAlreadyVoidException ex) {

        	getSessionContext().setRollbackOnly();
        	throw ex;

        } catch (GlobalInvItemLocationNotFoundException ex) {

        	getSessionContext().setRollbackOnly();
        	throw ex;

        } catch (GlobalNoApprovalRequesterFoundException ex) {

			getSessionContext().setRollbackOnly();
			throw ex;

		} catch (GlobalNoApprovalApproverFoundException ex) {

			getSessionContext().setRollbackOnly();
			throw ex;

        } catch (GlJREffectiveDateNoPeriodExistException ex) {

        	getSessionContext().setRollbackOnly();
        	throw ex;

        } catch (GlJREffectiveDatePeriodClosedException ex) {

        	getSessionContext().setRollbackOnly();
        	throw ex;

        } catch (GlobalJournalNotBalanceException ex) {

        	getSessionContext().setRollbackOnly();
        	throw ex;

        } catch (GlobalTransactionAlreadyLockedException ex) {

        	getSessionContext().setRollbackOnly();
        	throw ex;

        } catch (GlobalInventoryDateException ex) {

        	getSessionContext().setRollbackOnly();
        	throw ex;

        } catch (GlobalTransactionAlreadyVoidPostedException ex) {

        	getSessionContext().setRollbackOnly();
        	throw ex;

        } catch (GlobalBranchAccountNumberInvalidException ex) {

            getSessionContext().setRollbackOnly();
            throw ex;

        } catch (AdPRFCoaGlVarianceAccountNotFoundException ex){

            getSessionContext().setRollbackOnly();
            throw ex;

        } catch (GlobalMiscInfoIsRequiredException ex){

            getSessionContext().setRollbackOnly();
            throw ex;


        } catch (GlobalReferenceNumberNotUniqueException ex){

            getSessionContext().setRollbackOnly();
            throw ex;
   /*     }catch (GlobalInvTagMissingException ex) {

        	getSessionContext().setRollbackOnly();
        	throw ex;*/

        }catch (InvTagSerialNumberAlreadyExistException ex) {

        	getSessionContext().setRollbackOnly();
        	throw ex;

        }catch (Exception ex) {

        	Debug.printStackTrace(ex);
        	getSessionContext().setRollbackOnly();
        	throw new EJBException(ex.getMessage());

        }

    }

    private void setupApprovalStatus(
        com.util.ApPurchaseOrderDetails details, boolean isDraft, Integer AD_BRNCH,
        Integer AD_CMPNY, LocalAdApprovalHome adApprovalHome,
        LocalAdAmountLimitHome adAmountLimitHome, LocalAdApprovalUserHome adApprovalUserHome,
        LocalAdApprovalQueueHome adApprovalQueueHome, LocalApPurchaseOrder apPurchaseOrder,
        LocalApPurchaseOrder apExistingPurchaseOrder, LocalAdPreference adPreference,
        LocalAdUser adUser, double TOTAL_AMOUNT
    ) throws FinderException, GlobalNoApprovalRequesterFoundException,
        GlobalNoApprovalApproverFoundException, CreateException, GlobalSendEmailMessageException,
        GlobalRecordAlreadyDeletedException, GlobalTransactionAlreadyPostedException,
        GlobalTransactionAlreadyVoidException, GlJREffectiveDateNoPeriodExistException,
        GlJREffectiveDatePeriodClosedException, GlobalJournalNotBalanceException,
        AdPRFCoaGlVarianceAccountNotFoundException {
      String PO_APPRVL_STATUS = null;

			if(!isDraft) {

				if (details.getPoType().equals("PO MATCHED")) {

        			apExistingPurchaseOrder.setPoLock(EJBCommon.FALSE);

    	        }

				LocalAdApproval adApproval = adApprovalHome.findByAprAdCompany(AD_CMPNY);

				// check if receiving item approval is enabled

				if (adApproval.getAprEnableApReceivingItem()== EJBCommon.FALSE) {

					PO_APPRVL_STATUS = "N/A";

				} else {

					// check if voucher is self approved

					LocalAdAmountLimit adAmountLimit = null;

					try {

						adAmountLimit = adAmountLimitHome.findByCalDeptAdcTypeAndAuTypeAndUsrName(adUser.getUsrDept(),"AP RECEIVING ITEM", "REQUESTER", details.getPoLastModifiedBy(), AD_CMPNY);

					} catch (FinderException ex) {

						throw new GlobalNoApprovalRequesterFoundException();

					}

					if (TOTAL_AMOUNT < adAmountLimit.getCalAmountLimit()) {

						PO_APPRVL_STATUS = "N/A";

					} else {

						// for approval, create approval queue

						Collection adAmountLimits = adAmountLimitHome.findByAdcTypeAndGreaterThanCalDeptAndCalAmountLimit(adUser.getUsrDept(),"AP RECEIVING ITEM", adAmountLimit.getCalAmountLimit(), AD_CMPNY);

						if (adAmountLimits.isEmpty()) {

							Collection adApprovalUsers = adApprovalUserHome.findByCalDeptAndAuTypeAndCalCode(adUser.getUsrDept(),"APPROVER", adAmountLimit.getCalCode(), AD_CMPNY);

							if (adApprovalUsers.isEmpty()) {

								throw new GlobalNoApprovalApproverFoundException();

							}

							Iterator j = adApprovalUsers.iterator();

							while (j.hasNext()) {

								LocalAdApprovalUser adApprovalUser = (LocalAdApprovalUser)j.next();

						
								LocalAdApprovalQueue adApprovalQueue = adApprovalQueueHome.create(adUser.getUsrDept(),adApprovalUser.getAuLevel(), EJBCommon.FALSE, EJBCommon.incrementStringNumber(adApprovalUser.getAuLevel()),"AP RECEIVING ITEM", apPurchaseOrder.getPoCode(),
										apPurchaseOrder.getPoDocumentNumber(), apPurchaseOrder.getPoDate(), null, adAmountLimit.getCalAndOr(), adApprovalUser.getAuOr(), EJBCommon.FALSE, adUser.getUsrDescription(),
												AD_BRNCH, AD_CMPNY);

								
								adApprovalUser.getAdUser().addAdApprovalQueue(adApprovalQueue);

								if(adApprovalUser.getAuLevel().equals("LEVEL 1")) {
									adApprovalQueue.setAqForApproval(EJBCommon.TRUE);
									if((!adApprovalQueue.getAdUser().getUsrEmailAddress().equals("")) && (adPreference.getPrfAdEnableEmailNotification() == EJBCommon.TRUE)) {
										
										
										this.sendEmail(adApprovalQueue, AD_CMPNY);
									}

								}
							}

						} else {

							boolean isApprovalUsersFound = false;

							Iterator i = adAmountLimits.iterator();

							while (i.hasNext()) {

								LocalAdAmountLimit adNextAmountLimit = (LocalAdAmountLimit)i.next();

								if (TOTAL_AMOUNT < adNextAmountLimit.getCalAmountLimit()) {

									Collection adApprovalUsers = adApprovalUserHome.findByCalDeptAndAuTypeAndCalCode(adUser.getUsrDept(),"APPROVER", adAmountLimit.getCalCode(), AD_CMPNY);

									Iterator j = adApprovalUsers.iterator();

									while (j.hasNext()) {

										isApprovalUsersFound = true;

										LocalAdApprovalUser adApprovalUser = (LocalAdApprovalUser)j.next();

							
										LocalAdApprovalQueue adApprovalQueue = adApprovalQueueHome.create(adUser.getUsrDept(), adApprovalUser.getAuLevel(), EJBCommon.FALSE, EJBCommon.incrementStringNumber(adApprovalUser.getAuLevel()), "AP RECEIVING ITEM",  apPurchaseOrder.getPoCode(),
												apPurchaseOrder.getPoDocumentNumber(), apPurchaseOrder.getPoDate(), null, adAmountLimit.getCalAndOr(), adApprovalUser.getAuOr(), EJBCommon.FALSE, adUser.getUsrDescription(),
														AD_BRNCH, AD_CMPNY);

										adApprovalUser.getAdUser().addAdApprovalQueue(adApprovalQueue);
										
										System.out.println("user to send email add: " + adApprovalQueue.getAdUser().getUsrEmailAddress());
										
										if(adApprovalUser.getAuLevel().equals("LEVEL 1")) {
											adApprovalQueue.setAqForApproval(EJBCommon.TRUE);
											if((!adApprovalQueue.getAdUser().getUsrEmailAddress().equals("")) && (adPreference.getPrfAdEnableEmailNotification() == EJBCommon.TRUE)) {
												
												
												this.sendEmail(adApprovalQueue, AD_CMPNY);
											}
		
										}

									}

									break;

								} else if (!i.hasNext()) {

									Collection adApprovalUsers = adApprovalUserHome.findByCalDeptAndAuTypeAndCalCode(adUser.getUsrDept(),"APPROVER", adNextAmountLimit.getCalCode(), AD_CMPNY);

									Iterator j = adApprovalUsers.iterator();

									while (j.hasNext()) {

										isApprovalUsersFound = true;

										LocalAdApprovalUser adApprovalUser = (LocalAdApprovalUser)j.next();

									
										LocalAdApprovalQueue adApprovalQueue = adApprovalQueueHome.create(adUser.getUsrDept(), adApprovalUser.getAuLevel(), EJBCommon.FALSE, EJBCommon.incrementStringNumber(adApprovalUser.getAuLevel()) ,"AP RECEIVING ITEM", apPurchaseOrder.getPoCode(),
												apPurchaseOrder.getPoDocumentNumber(), apPurchaseOrder.getPoDate(), null, adNextAmountLimit.getCalAndOr(), adApprovalUser.getAuOr(), EJBCommon.FALSE, adUser.getUsrDescription(),
														AD_BRNCH, AD_CMPNY);


										adApprovalUser.getAdUser().addAdApprovalQueue(adApprovalQueue);
										System.out.println("user to send email add: " + adApprovalQueue.getAdUser().getUsrEmailAddress());
										
										if(adApprovalUser.getAuLevel().equals("LEVEL 1")) {
											adApprovalQueue.setAqForApproval(EJBCommon.TRUE);
											if((!adApprovalQueue.getAdUser().getUsrEmailAddress().equals("")) && (adPreference.getPrfAdEnableEmailNotification() == EJBCommon.TRUE)) {
												
												
												this.sendEmail(adApprovalQueue, AD_CMPNY);
											}
		
										}
									}

									break;

								}

								adAmountLimit = adNextAmountLimit;

							}

							if (!isApprovalUsersFound) {

								throw new GlobalNoApprovalApproverFoundException();

							}

						}

						PO_APPRVL_STATUS = "PENDING";

					}

				}

				apPurchaseOrder.setPoApprovalStatus(PO_APPRVL_STATUS);

				if (PO_APPRVL_STATUS != null && PO_APPRVL_STATUS.equals("N/A")) {

	        		this.executeApReceivingPost(apPurchaseOrder.getPoCode(), apPurchaseOrder.getPoLastModifiedBy(), AD_BRNCH, AD_CMPNY);

	        	}

			}
    }

    private double calculateJournal(
        ArrayList plList, Integer AD_BRNCH, Integer AD_CMPNY,
        LocalInvItemLocationHome invItemLocationHome, LocalInvCostingHome invCostingHome,
        LocalAdBranchItemLocationHome adBranchItemLocationHome, LocalInvTagHome invTagHome,
        LocalAdUserHome adUserHome, LocalApPurchaseOrder apPurchaseOrder,
        LocalInvItemHome invItemHome, LocalAdBranchApTaxCodeHome adBranchApTaxCodeHome,
        boolean isRecalculate, boolean newPurchaseOrder, LocalApTaxCode apTaxCode,
        LocalAdPreference adPreference, LocalAdUser adUser
    ) throws RemoveException, GlobalInvItemLocationNotFoundException, FinderException,
        GlobalInventoryDateException, GlobalMiscInfoIsRequiredException,
        GlobalBranchAccountNumberInvalidException, CreateException,
        InvTagSerialNumberAlreadyExistException {
      double TOTAL_AMOUNT = 0;
      double TOTAL_TAX = 0d;
      double TOTAL_LINE = 0d;

   	if (isRecalculate) {
      System.out.println("isrecalculated");
      // remove all receiving item line

      Collection apPurchaseOrderLines = apPurchaseOrder.getApPurchaseOrderLines();

      Iterator i = apPurchaseOrderLines.iterator();

      while (i.hasNext()) {

          LocalApPurchaseOrderLine apPurchaseOrderLine = (LocalApPurchaseOrderLine)i.next();

          //remove all inv tag inside PO line
          Collection invTags = apPurchaseOrderLine.getInvTags();

          Iterator x = invTags.iterator();

          while (x.hasNext()){

          	LocalInvTag invTag = (LocalInvTag)x.next();

          	x.remove();

          	invTag.remove();
          }

          i.remove();

          apPurchaseOrderLine.remove();

      }

      // remove all distribution records

      Collection apDistributionRecords = apPurchaseOrder.getApDistributionRecords();

      i = apDistributionRecords.iterator();

      while (i.hasNext()) {

          LocalApDistributionRecord apDistributionRecord = (LocalApDistributionRecord)i.next();

         
       	   i.remove();

      	    apDistributionRecord.remove();
          
        

      }


      double PO_TTL_AMNT = this.convertForeignToFunctionalCurrency(apPurchaseOrder.getGlFunctionalCurrency().getFcCode(),
      		    apPurchaseOrder.getGlFunctionalCurrency().getFcName(),
      		    apPurchaseOrder.getPoConversionDate(),
      		    apPurchaseOrder.getPoConversionRate(),
      		  apPurchaseOrder.getPoTotalAmount(), AD_CMPNY);

      double TTL_ADDON_COST = apPurchaseOrder.getPoDuties() + apPurchaseOrder.getPoFreight() + apPurchaseOrder.getPoEntryFee() +
      					apPurchaseOrder.getPoStorage() + apPurchaseOrder.getPoWharfageHandling();

      // add new receiving item line and distribution record


        i = plList.iterator();

        LocalInvItemLocation invItemLocation = null;

        while (i.hasNext()) {

      	    ApModPurchaseOrderLineDetails mPlDetails = (ApModPurchaseOrderLineDetails) i.next();

      	    try {

      	    	invItemLocation = invItemLocationHome.findByLocNameAndIiName(mPlDetails.getPlLocName(), mPlDetails.getPlIiName(), AD_CMPNY);


      	    } catch (FinderException ex) {

      	    	throw new GlobalInvItemLocationNotFoundException(String.valueOf(mPlDetails.getPlLine()));

      	    }

      	    //	start date validation

      	    if(mPlDetails.getPlQuantity() > 0 && adPreference.getPrfArAllowPriorDate() == EJBCommon.FALSE) {

      	    	Collection invNegTxnCosting = invCostingHome.findNegTxnByGreaterThanCstDateAndIiNameAndLocName(
      	    			apPurchaseOrder.getPoDate(), invItemLocation.getInvItem().getIiName(),
      		invItemLocation.getInvLocation().getLocName(), AD_BRNCH, AD_CMPNY);
      	    	if(!invNegTxnCosting.isEmpty()) throw new GlobalInventoryDateException(invItemLocation.getInvItem().getIiName());

      	    }

      	    LocalApPurchaseOrderLine apPurchaseOrderLine = this.addApPlEntry(mPlDetails, apPurchaseOrder, invItemLocation, newPurchaseOrder, AD_CMPNY);

      	    if(mPlDetails.getPlQuantity() == 0) continue;

      	    // add inventory distributions

      	    LocalAdBranchItemLocation adBranchItemLocation = null;

      	    try {

      	    	adBranchItemLocation = adBranchItemLocationHome.findBilByIlCodeAndBrCode(apPurchaseOrderLine.getInvItemLocation().getIlCode(), AD_BRNCH, AD_CMPNY);

      	    } catch(FinderException ex) {

      	    }


      	    double PL_AMNT = this.convertForeignToFunctionalCurrency(apPurchaseOrder.getGlFunctionalCurrency().getFcCode(),
      		    apPurchaseOrder.getGlFunctionalCurrency().getFcName(),
      		    apPurchaseOrder.getPoConversionDate(),
      		    apPurchaseOrder.getPoConversionRate(),
      		    apPurchaseOrderLine.getPlAmount(), AD_CMPNY);

      	    double PL_TX_AMNT = this.convertForeignToFunctionalCurrency(apPurchaseOrder.getGlFunctionalCurrency().getFcCode(),
        		    apPurchaseOrder.getGlFunctionalCurrency().getFcName(),
        		    apPurchaseOrder.getPoConversionDate(),
        		    apPurchaseOrder.getPoConversionRate(),
        		    apPurchaseOrderLine.getPlTaxAmount(), AD_CMPNY);

  	
        	    double ADDON_COST = EJBCommon.roundIt((PL_AMNT / PO_TTL_AMNT) * TTL_ADDON_COST , this.getInvGpCostPrecisionUnit(AD_CMPNY) );

        	    
       
        	    
        	    if(ADDON_COST > 0) {

        	  	    this.addApDrPlEntry(apPurchaseOrder.getApDrNextLine(),
      	  	    			"ADDON COST", EJBCommon.FALSE, ADDON_COST,
      	  	    		adBranchItemLocation.getBilCoaGlAccruedInventoryAccount(), apPurchaseOrder, AD_BRNCH, AD_CMPNY);

        	    }

        	    if(adBranchItemLocation != null) {

      	  	    // Use AdBranchItemLocation Coa Account
        	    	this.addApDrPlEntry(apPurchaseOrder.getApDrNextLine(), "INVENTORY",
        	    			EJBCommon.TRUE, PL_AMNT + ADDON_COST,
      			adBranchItemLocation.getBilCoaGlInventoryAccount(), apPurchaseOrder, AD_BRNCH, AD_CMPNY);

        	    	this.addApDrPlEntry(apPurchaseOrder.getApDrNextLine(), "ACC INV",
          				EJBCommon.FALSE, PL_AMNT + PL_TX_AMNT,
          				adBranchItemLocation.getBilCoaGlAccruedInventoryAccount(),
      			apPurchaseOrder, AD_BRNCH, AD_CMPNY);

        	    } else {

        	    	// Use default account
        	    	this.addApDrPlEntry(apPurchaseOrder.getApDrNextLine(),"INVENTORY",
        	    			EJBCommon.TRUE, PL_AMNT + ADDON_COST,
      			apPurchaseOrderLine.getInvItemLocation().getIlGlCoaInventoryAccount(), apPurchaseOrder, AD_BRNCH, AD_CMPNY);

        	    	this.addApDrPlEntry(apPurchaseOrder.getApDrNextLine(), "ACC INV",
          				EJBCommon.FALSE, PL_AMNT + PL_TX_AMNT,
      			apPurchaseOrderLine.getInvItemLocation().getIlGlCoaAccruedInventoryAccount(),
      			apPurchaseOrder, AD_BRNCH, AD_CMPNY);

        	    }
      	    
        

      	    TOTAL_LINE += PL_AMNT;
      	    TOTAL_TAX += PL_TX_AMNT;
      	    //TODO: add new inv Tag
      	    // validate inventoriable and non-inventoriable items

      	    System.out.println(mPlDetails.getPlTagList() + "<== mPlDetails.getPlTagList() controller validation");
      	    /*if (invItemLocation.getInvItem().getIiDineInCharge()==1 && mPlDetails.getPlTagList() != null){
      	    	throw new GlobalInvTagExistingException (invItemLocation.getInvItem().getIiName());
      	    }*/
      	    boolean invTagExisting = false;
      	    String serialNumberExisting = null;
      	    if (mPlDetails.getTraceMisc() == 1){
      	  	/*if (invItemLocation.getInvItem().getIiDineInCharge()==0
      	  			&& mPlDetails.getPlTagList().size() < mPlDetails.getPlQuantity()){
      	  		System.out.println(mPlDetails.getPlTagList().size() + "<= taglist size against PlRemaining =>" + mPlDetails.getPlRemaining());
        	    	throw new GlobalInvTagMissingException (invItemLocation.getInvItem().getIiName());
        	    }*/

        	    try{


        	    	//Iterator t = apPurchaseOrderLine.getInvTag().iterator();
      	  	    Iterator t = mPlDetails.getPlTagList().iterator();
      	  	    LocalInvItem invItem = invItemHome.findByIiName(mPlDetails.getPlIiName(), AD_CMPNY);
      	  	    LocalInvTag invTag  = null;
      	  	    System.out.println("umabot?");
      	  	    while (t.hasNext()){
      	  	    	InvModTagListDetails tgLstDetails = (InvModTagListDetails)t.next();
      	 
      	  	    	//get all inv tags that was already received
      	        Collection invTagColl = invTagHome.findAllPlReceiving(AD_CMPNY);

        	  	    //check if the serial number was already received else throw an exception
        	  	    Iterator v = invTagColl.iterator();
        	 

        	  	    //Serial Validation
        	  	    while (v.hasNext()) {
        	  	    	LocalInvTag invExistingTag = (LocalInvTag)(v.next());
        	  	    	if(invExistingTag.getApPurchaseOrderLine().getApPurchaseOrder().getPoReceiving() == 1){
        	  	    		
        	  	    	System.out.println("existing  : " + tgLstDetails.getTgSerialNumber() + " and " + invExistingTag.getTgSerialNumber());
          	  	    	if (tgLstDetails.getTgSerialNumber().equals(invExistingTag.getTgSerialNumber())) {
          	  	    		
          	  	    		System.out.println("existing  : " + tgLstDetails.getTgSerialNumber() + " and " + invExistingTag.getTgSerialNumber());
          	  	    		invTagExisting=true;
          	  	    		serialNumberExisting = tgLstDetails.getTgSerialNumber();
          	  	    		throw new InvTagSerialNumberAlreadyExistException(tgLstDetails.getTgSerialNumber());
          	  	    	}
        	  	    	}

        	  	    }

        	  	    //
      	  	    	/*if (invItemLocation.getInvItem().getIiDineInCharge()==0 && tgLstDetails.getTgCustodian().equals("")
      	  	    			&&tgLstDetails.getTgSpecs().equals("")&&tgLstDetails.getTgPropertyCode().equals("")
      	  	    			&&tgLstDetails.getTgExpiryDate() == null && tgLstDetails.getTgSerialNumber().equals("")){
      	  	    		throw new GlobalInvTagMissingException (invItemLocation.getInvItem().getIiDescription());
      	  	    	}*/

      	  	    	if (tgLstDetails.getTgCode()==null){
      	  	    		System.out.println("1. ngcreate ba?");
        	  	    	invTag = invTagHome.create(tgLstDetails.getTgPropertyCode(),
        	  	    			tgLstDetails.getTgSerialNumber(),null,tgLstDetails.getTgExpiryDate(),
        	  	    			tgLstDetails.getTgSpecs(), AD_CMPNY, tgLstDetails.getTgTransactionDate(),
        	  	    			tgLstDetails.getTgType());

        	  	    	invTag.setApPurchaseOrderLine(apPurchaseOrderLine);
        	  	    
        	  	    	try {
        	  	    		LocalAdUser adUserTag = adUserHome.findByUsrName(tgLstDetails.getTgCustodian(), AD_CMPNY);
        	  	    	}catch(FinderException ex){

        	  	    	}
        	  	    	if(invItem.getIiNonInventoriable()==1){
        	  	    		invTag.setTgDocumentNumber(adPreference.getPrfInvNextCustodianNumber2());
        	  	    		//adPreference.setPrfInvNextCustodianNumber2(EJBCommon.incrementStringNumber(adPreference.getPrfInvNextCustodianNumber2()));
        	  	    	}else{
        	  	    		invTag.setTgDocumentNumber(adPreference.getPrfInvNextCustodianNumber1());
      	  	    			//adPreference.setPrfInvNextCustodianNumber1(EJBCommon.incrementStringNumber(adPreference.getPrfInvNextCustodianNumber1()));

        	  	    	}

        	  	    	invTag.setAdUser(adUser);
        	  	    	System.out.println("2. ngcreate ba?");
      	  	    	}

      	  	    }

        	    } catch (InvTagSerialNumberAlreadyExistException ex) {

      	  	    if (invTagExisting) {
      	  	    	throw new InvTagSerialNumberAlreadyExistException(serialNumberExisting);
      	  	    }
        	    }
        	    /*catch(Exception ex){

        	    	if (invItemLocation.getInvItem().getIiDineInCharge()==0 ){
        	    		throw new GlobalInvTagMissingException (mPlDetails.getPlIiName());
        	    	}

        	    }*/
      	    }



        }


    

        // add tax distribution if necessary

        if (!apTaxCode.getTcType().equals("NONE") && !apTaxCode.getTcType().equals("EXEMPT")) {

        	if (adPreference.getPrfApUseAccruedVat() == EJBCommon.FALSE) {

        		// add branch tax code
      	      	  LocalAdBranchApTaxCode adBranchTaxCode = null;


                    try {
                  	  adBranchTaxCode = adBranchApTaxCodeHome.findBtcByTcCodeAndBrCode(apPurchaseOrder.getApTaxCode().getTcCode(), AD_BRNCH, AD_CMPNY);

                    } catch(FinderException ex) {

                    }

                    if(adBranchTaxCode != null){

                  	  this.addApDrPlEntry(apPurchaseOrder.getApDrNextLine(),
      			      	        "TAX", EJBCommon.TRUE, TOTAL_TAX,  adBranchTaxCode.getBtcGlCoaTaxCode(),
      			      	        apPurchaseOrder, AD_BRNCH, AD_CMPNY);
                    } else {

                  	  this.addApDrPlEntry(apPurchaseOrder.getApDrNextLine(),
      			      	        "TAX", EJBCommon.TRUE, TOTAL_TAX,  apTaxCode.getGlChartOfAccount().getCoaCode(),
      			      	        apPurchaseOrder, AD_BRNCH, AD_CMPNY);

                    }

        	} else {

        		this.addApDrPlEntry(apPurchaseOrder.getApDrNextLine(),
      	        "ACC TAX", EJBCommon.TRUE, TOTAL_TAX, adPreference.getPrfApGlCoaAccruedVatAccount(),
      	        apPurchaseOrder, AD_BRNCH, AD_CMPNY);

        	}

      }

        TOTAL_AMOUNT = TOTAL_LINE + TOTAL_TAX;
        
      

  	}
      return TOTAL_AMOUNT;
    }

    private void validateDocumentNumber(
        com.util.ApPurchaseOrderDetails details, Integer AD_BRNCH, Integer AD_CMPNY,
        LocalApPurchaseOrderHome apPurchaseOrderHome,
        LocalAdDocumentSequenceAssignmentHome adDocumentSequenceAssignmentHome,
        LocalAdBranchDocumentSequenceAssignmentHome adBranchDocumentSequenceAssignmentHome,
        LocalApPurchaseOrder apPurchaseOrder
    ) throws GlobalDocumentNumberNotUniqueException {
      try {

          LocalAdBranchDocumentSequenceAssignment adBranchDocumentSequenceAssignment = null;
      LocalAdDocumentSequenceAssignment adDocumentSequenceAssignment = null;

        	if (details.getPoCode() == null) {

        	    try {

        	        adDocumentSequenceAssignment = adDocumentSequenceAssignmentHome.findByDcName("AP RECEIVING ITEM", AD_CMPNY);

        	    } catch (FinderException ex) { }

        	    try {

      		adBranchDocumentSequenceAssignment = adBranchDocumentSequenceAssignmentHome.findBdsByDsaCodeAndBrCode(adDocumentSequenceAssignment.getDsaCode(), AD_BRNCH, AD_CMPNY);

      	} catch (FinderException ex) { }


      		LocalApPurchaseOrder apExistingReceivingItem = null;

      		try {

      			apExistingReceivingItem = apPurchaseOrderHome.findByPoDocumentNumberAndPoReceivingAndBrCode(
          		    details.getPoDocumentNumber(), EJBCommon.TRUE, AD_BRNCH, AD_CMPNY);

          	} catch (FinderException ex) {

          	}

              if (apExistingReceivingItem != null) {

              	throw new GlobalDocumentNumberNotUniqueException();

              }

            if (adDocumentSequenceAssignment.getAdDocumentSequence().getDsNumberingType() == 'A' &&
                (details.getPoDocumentNumber() == null || details.getPoDocumentNumber().trim().length() == 0)) {

                while (true) {

                    if (adBranchDocumentSequenceAssignment == null || adBranchDocumentSequenceAssignment.getBdsNextSequence() == null) {

                  	try {

                  		apPurchaseOrderHome.findByPoDocumentNumberAndPoReceivingAndBrCode(adDocumentSequenceAssignment.getDsaNextSequence(), EJBCommon.TRUE, AD_BRNCH, AD_CMPNY);
                  		adDocumentSequenceAssignment.setDsaNextSequence(EJBCommon.incrementStringNumber(adDocumentSequenceAssignment.getDsaNextSequence()));

                  	} catch (FinderException ex) {

                  		details.setPoDocumentNumber(adDocumentSequenceAssignment.getDsaNextSequence());
                  		adDocumentSequenceAssignment.setDsaNextSequence(EJBCommon.incrementStringNumber(adDocumentSequenceAssignment.getDsaNextSequence()));
      		            break;

                  	}

                    } else {

                        try {

                  		apPurchaseOrderHome.findByPoDocumentNumberAndPoReceivingAndBrCode(adBranchDocumentSequenceAssignment.getBdsNextSequence(), EJBCommon.TRUE, AD_BRNCH, AD_CMPNY);
                  		adBranchDocumentSequenceAssignment.setBdsNextSequence(EJBCommon.incrementStringNumber(adBranchDocumentSequenceAssignment.getBdsNextSequence()));

                  	} catch (FinderException ex) {

                  		details.setPoDocumentNumber(adBranchDocumentSequenceAssignment.getBdsNextSequence());
                  		adBranchDocumentSequenceAssignment.setBdsNextSequence(EJBCommon.incrementStringNumber(adBranchDocumentSequenceAssignment.getBdsNextSequence()));
      		            break;

                  	}

                    }

                }

            }



        } else {

        	LocalApPurchaseOrder apExistingReceivingItem = null;

        	try {

        		apExistingReceivingItem = apPurchaseOrderHome.findByPoDocumentNumberAndPoReceivingAndBrCode(
          		    details.getPoDocumentNumber(), EJBCommon.TRUE, AD_BRNCH, AD_CMPNY);

          	} catch (FinderException ex) {

          	}

          	if (apExistingReceivingItem != null &&
                  !apExistingReceivingItem.getPoCode().equals(details.getPoCode())) {

              	throw new GlobalDocumentNumberNotUniqueException();

              }

              if (apPurchaseOrder.getPoDocumentNumber() != details.getPoDocumentNumber() &&
                  (details.getPoDocumentNumber() == null || details.getPoDocumentNumber().trim().length() == 0)) {

                  details.setPoDocumentNumber(apPurchaseOrder.getPoDocumentNumber());

           	}



        }

      } catch (GlobalDocumentNumberNotUniqueException ex) {

      getSessionContext().setRollbackOnly();
      throw ex;

	} catch (Exception ex) {

      Debug.printStackTrace(ex);
      getSessionContext().setRollbackOnly();
      throw new EJBException(ex.getMessage());

	}
    }

    /**
     * @ejb:interface-method view-type="remote"
     **/
    public void deleteApPoEntry(Integer PO_CODE, String AD_USR, Integer AD_BRNCH, Integer AD_CMPNY) throws
        GlobalRecordAlreadyDeletedException {

        Debug.print("ApReceivingItemEntryControllerBean deleteApPoEntry");

        LocalApPurchaseOrderHome apPurchaseOrderHome = null;
        LocalAdApprovalQueueHome adApprovalQueueHome = null;
        LocalAdDeleteAuditTrailHome adDeleteAuditTrailHome = null;

        // Initialize EJB Home

        try {

            apPurchaseOrderHome = (LocalApPurchaseOrderHome)EJBHomeFactory.
                lookUpLocalHome(LocalApPurchaseOrderHome.JNDI_NAME, LocalApPurchaseOrderHome.class);
            adApprovalQueueHome = (LocalAdApprovalQueueHome)EJBHomeFactory.
			    lookUpLocalHome(LocalAdApprovalQueueHome.JNDI_NAME, LocalAdApprovalQueueHome.class);
            adDeleteAuditTrailHome = (LocalAdDeleteAuditTrailHome)EJBHomeFactory.
            	lookUpLocalHome(LocalAdDeleteAuditTrailHome.JNDI_NAME, LocalAdDeleteAuditTrailHome.class);

        } catch (NamingException ex) {

            throw new EJBException(ex.getMessage());

        }

        try {

        	LocalApPurchaseOrder apPurchaseOrder = apPurchaseOrderHome.findByPrimaryKey(PO_CODE);

        	LocalApPurchaseOrder apExistingPurchaseOrder  = null;

        	if(apPurchaseOrder.getPoRcvPoNumber() == null) {

        	    apExistingPurchaseOrder = apPurchaseOrderHome.findByPoDocumentNumberAndPoReceivingAndBrCode(
        			apPurchaseOrder.getPoDocumentNumber(), EJBCommon.TRUE, AD_BRNCH, AD_CMPNY);

        	} else {

        	    apExistingPurchaseOrder = apPurchaseOrderHome.findByPoDocumentNumberAndPoReceivingAndBrCode(
        			apPurchaseOrder.getPoRcvPoNumber(), EJBCommon.FALSE, AD_BRNCH, AD_CMPNY);

        	}

        	apExistingPurchaseOrder.setPoLock(EJBCommon.FALSE);
        	apExistingPurchaseOrder.setPoApprovalStatus(null);
        	apExistingPurchaseOrder.setPoPosted(EJBCommon.FALSE);
        	apExistingPurchaseOrder.setPoPostedBy(null);
        	apExistingPurchaseOrder.setPoDatePosted(null);

        	adDeleteAuditTrailHome.create("AP RECEIVING ITEM", apPurchaseOrder.getPoDate(), apPurchaseOrder.getPoDocumentNumber(), apPurchaseOrder.getPoReferenceNumber(),
        			0d, AD_USR, new Date(), AD_CMPNY);

        	apPurchaseOrder.remove();

        } catch (FinderException ex) {

            getSessionContext().setRollbackOnly();
            throw new GlobalRecordAlreadyDeletedException();

        } catch (Exception ex) {

        	Debug.printStackTrace(ex);
        	getSessionContext().setRollbackOnly();
        	throw new EJBException(ex.getMessage());

        }

    }

   /**
    * @ejb:interface-method view-type="remote"
    * @jboss:method-attributes read-only="true"
    **/
    public short getGlFcPrecisionUnit(Integer AD_CMPNY) {

       Debug.print("ApReceivingItemEntryControllerBean getGlFcPrecisionUnit");


       LocalAdCompanyHome adCompanyHome = null;


       // Initialize EJB Home

       try {

           adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
              lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);

       } catch (NamingException ex) {

           throw new EJBException(ex.getMessage());

       }

       try {

         LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);

         return  adCompany.getGlFunctionalCurrency().getFcPrecision();

       } catch (Exception ex) {

       	 Debug.printStackTrace(ex);
         throw new EJBException(ex.getMessage());

       }

    }

   /**
    * @ejb:interface-method view-type="remote"
    * @jboss:method-attributes read-only="true"
    **/
    public ArrayList getAdApprovalNotifiedUsersByPoCode(Integer PO_CODE, Integer AD_CMPNY) {

       Debug.print("ApReceivingItemEntryControllerBean getAdApprovalNotifiedUsersByPoCode");


       LocalAdApprovalQueueHome adApprovalQueueHome = null;
       LocalApPurchaseOrderHome apPurchaseOrderHome = null;

       ArrayList list = new ArrayList();


       // Initialize EJB Home

       try {

           adApprovalQueueHome = (LocalAdApprovalQueueHome)EJBHomeFactory.
              lookUpLocalHome(LocalAdApprovalQueueHome.JNDI_NAME, LocalAdApprovalQueueHome.class);
           apPurchaseOrderHome = (LocalApPurchaseOrderHome)EJBHomeFactory.
              lookUpLocalHome(LocalApPurchaseOrderHome.JNDI_NAME, LocalApPurchaseOrderHome.class);

       } catch (NamingException ex) {

           throw new EJBException(ex.getMessage());

       }

       try {

         LocalApPurchaseOrder apPurchaseOrder = apPurchaseOrderHome.findByPrimaryKey(PO_CODE);

         if (apPurchaseOrder.getPoPosted() == EJBCommon.TRUE) {

         	list.add("DOCUMENT POSTED");
         	return list;

         }

         Collection adApprovalQueues = adApprovalQueueHome.findByAqDocumentAndAqDocumentCode("AP RECEIVING ITEM", PO_CODE, AD_CMPNY);

         Iterator i = adApprovalQueues.iterator();

         while(i.hasNext()) {

         	LocalAdApprovalQueue adApprovalQueue = (LocalAdApprovalQueue)i.next();

         	list.add(adApprovalQueue.getAdUser().getUsrDescription());

         }

         return list;

       } catch (Exception ex) {

       	 Debug.printStackTrace(ex);
         throw new EJBException(ex.getMessage());

       }

    }

    /**
    * @ejb:interface-method view-type="remote"
    * @jboss:method-attributes read-only="true"
    **/
    public short getAdPrfApJournalLineNumber(Integer AD_CMPNY) {

       Debug.print("ApReceivingItemEntryControllerBean getAdPrfApJournalLineNumber");

       LocalAdPreferenceHome adPreferenceHome = null;


       // Initialize EJB Home

       try {

          adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
              lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);

       } catch (NamingException ex) {

          throw new EJBException(ex.getMessage());

       }


       try {

          LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);

          return adPreference.getPrfApJournalLineNumber();

       } catch (Exception ex) {

          Debug.printStackTrace(ex);
          throw new EJBException(ex.getMessage());

       }

    }


    /**
      * @ejb:interface-method view-type="remote"
      * @jboss:method-attributes read-only="true"
      **/
      public short getInvGpQuantityPrecisionUnit(Integer AD_CMPNY) {

         Debug.print("ApReceivingItemEntryControllerBean getInvGpQuantityPrecisionUnit");

         LocalAdPreferenceHome adPreferenceHome = null;

          // Initialize EJB Home

          try {

          	adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
                 lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);

          } catch (NamingException ex) {

             throw new EJBException(ex.getMessage());

          }


          try {

             LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);

             return adPreference.getPrfInvQuantityPrecisionUnit();

          } catch (Exception ex) {

             Debug.printStackTrace(ex);
             throw new EJBException(ex.getMessage());

          }

      }


      /**
       * @ejb:interface-method view-type="remote"
       * @jboss:method-attributes read-only="true"
       **/
       public short getInvGpCostPrecisionUnit(Integer AD_CMPNY) {

          Debug.print("ApReceivingItemEntryControllerBean getInvGpCostPrecisionUnit");

          LocalAdPreferenceHome adPreferenceHome = null;

           // Initialize EJB Home

           try {

           	adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
                  lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);

           } catch (NamingException ex) {

              throw new EJBException(ex.getMessage());

           }


           try {

              LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);

              return adPreference.getPrfInvCostPrecisionUnit();

           } catch (Exception ex) {

              Debug.printStackTrace(ex);
              throw new EJBException(ex.getMessage());

           }

       }

      /**
  	 * @ejb:interface-method view-type="remote"
  	 * @jboss:method-attributes read-only="true"
  	 **/
  	public byte getAdPrfApUseSupplierPulldown(Integer AD_CMPNY) {

  		Debug.print("ApReceivingItemEntryControllerBean getAdPrfApUseSupplierPulldown");

  		LocalAdPreferenceHome adPreferenceHome = null;

  		// Initialize EJB Home

  		try {

  			adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
  			lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);

  		} catch (NamingException ex) {

  			throw new EJBException(ex.getMessage());

  		}

  		try {

  			LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);

  			return  adPreference.getPrfApUseSupplierPulldown();

  		} catch (Exception ex) {

  			Debug.printStackTrace(ex);
  			throw new EJBException(ex.getMessage());

  		}

  	}

    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public byte getAdPrfEnableGlJournalBatch(Integer AD_CMPNY) {

    	Debug.print("ApReceivingItemEntryControllerBean getAdPrfApEnableApVoucherBatch");

    	LocalAdPreferenceHome adPreferenceHome = null;


    	// Initialize EJB Home

    	try {

    		adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);

    	} catch (NamingException ex) {

    		throw new EJBException(ex.getMessage());

    	}


    	try {

    		LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);

    		return adPreference.getPrfEnableGlJournalBatch();

    	} catch (Exception ex) {

    		Debug.printStackTrace(ex);
    		throw new EJBException(ex.getMessage());

    	}

    }


    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList getGlOpenJbAll(Integer AD_BRNCH, Integer AD_CMPNY) {

    	Debug.print("ApReceivingItemEntryControllerBean getGlOpenJbAll");

    	LocalGlJournalBatchHome glJournalBatchHome = null;

    	ArrayList list = new ArrayList();

    	// Initialize EJB Home

    	try {

    		glJournalBatchHome = (LocalGlJournalBatchHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlJournalBatchHome.JNDI_NAME, LocalGlJournalBatchHome.class);

    	} catch (NamingException ex) {

    		throw new EJBException(ex.getMessage());

    	}

    	try {

    		Collection glJournalBatches = glJournalBatchHome.findOpenJbAll(AD_BRNCH, AD_CMPNY);

    		Iterator i = glJournalBatches.iterator();

    		while (i.hasNext()) {

    			LocalGlJournalBatch glJournalBatch = (LocalGlJournalBatch)i.next();

    			list.add(glJournalBatch.getJbName());

    		}

    		return list;

    	} catch (Exception ex) {

    		Debug.printStackTrace(ex);
    		throw new EJBException(ex.getMessage());

    	}

    }

    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public double getFrRateByFrNameAndFrDate(String FC_NM, Date CONVERSION_DATE, Integer AD_CMPNY)
    throws GlobalConversionDateNotExistException {

    	Debug.print("ApReceivingItemEntryControllerBean getFrRateByFrNameAndFrDate");

    	LocalGlFunctionalCurrencyRateHome glFunctionalCurrencyRateHome = null;
    	LocalGlFunctionalCurrencyHome glFunctionalCurrencyHome = null;
    	LocalAdCompanyHome adCompanyHome = null;

    	// Initialize EJB Home

    	try {

    		glFunctionalCurrencyRateHome = (LocalGlFunctionalCurrencyRateHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlFunctionalCurrencyRateHome.JNDI_NAME, LocalGlFunctionalCurrencyRateHome.class);
    		glFunctionalCurrencyHome = (LocalGlFunctionalCurrencyHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlFunctionalCurrencyHome.JNDI_NAME, LocalGlFunctionalCurrencyHome.class);
    		adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);

    	} catch (NamingException ex) {

    		throw new EJBException(ex.getMessage());

    	}

    	try {

    		LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
    		LocalGlFunctionalCurrency glFunctionalCurrency = glFunctionalCurrencyHome.findByFcName(FC_NM, AD_CMPNY);

    		double CONVERSION_RATE = 1;

    		// Get functional currency rate

    		if (!FC_NM.equals("USD")) {

    			LocalGlFunctionalCurrencyRate glFunctionalCurrencyRate =
    				glFunctionalCurrencyRateHome.findByFcCodeAndDate(glFunctionalCurrency.getFcCode(),
    						CONVERSION_DATE, AD_CMPNY);

    			CONVERSION_RATE = glFunctionalCurrencyRate.getFrXToUsd();

    		}

    		// Get set of book functional currency rate if necessary

    		if (!adCompany.getGlFunctionalCurrency().getFcName().equals("USD")) {

    			LocalGlFunctionalCurrencyRate glCompanyFunctionalCurrencyRate =
    				glFunctionalCurrencyRateHome.findByFcCodeAndDate(adCompany.getGlFunctionalCurrency(). getFcCode(),
    						CONVERSION_DATE, AD_CMPNY);

    			CONVERSION_RATE = CONVERSION_RATE / glCompanyFunctionalCurrencyRate.getFrXToUsd();

    		}

    		return CONVERSION_RATE;

    	} catch (FinderException ex) {

    		getSessionContext().setRollbackOnly();
    		throw new GlobalConversionDateNotExistException();

    	} catch (Exception ex) {

    		Debug.printStackTrace(ex);
    		throw new EJBException(ex.getMessage());

    	}

    }

    // private methods



    private double getInvFifoCost(Date CST_DT, Integer IL_CODE, double CST_QTY, double CST_COST,
			boolean isAdjustFifo, Integer AD_BRNCH, Integer AD_CMPNY)
	{

		LocalInvCostingHome invCostingHome = null;
		LocalInvItemLocationHome invItemLocationHome = null;

		// Initialize EJB Home

		try {

			invCostingHome = (LocalInvCostingHome)EJBHomeFactory.
			lookUpLocalHome(LocalInvCostingHome.JNDI_NAME, LocalInvCostingHome.class);
			invItemLocationHome = (LocalInvItemLocationHome)EJBHomeFactory.
			lookUpLocalHome(LocalInvItemLocationHome.JNDI_NAME, LocalInvItemLocationHome.class);
		}
		catch (NamingException ex) {

			throw new EJBException(ex.getMessage());
		}

		try {

			Collection invFifoCostings = invCostingHome.findFifoRemainingQuantityByLessThanOrEqualCstDateAndIlCodeAndBrCode(CST_DT, IL_CODE, AD_BRNCH, AD_CMPNY);

			if (invFifoCostings.size() > 0) {

				Iterator x = invFifoCostings.iterator();

				if (isAdjustFifo) {

					//executed during POST transaction

					double totalCost = 0d;
					double cost;

					if(CST_QTY < 0) {

						//for negative quantities
						double neededQty = -(CST_QTY);

						while(x.hasNext() && neededQty != 0) {

							LocalInvCosting invFifoCosting = (LocalInvCosting)x.next();

							if (invFifoCosting.getApPurchaseOrderLine() != null || invFifoCosting.getApVoucherLineItem() != null) {
								cost = invFifoCosting.getCstItemCost() / invFifoCosting.getCstQuantityReceived();
							} else if(invFifoCosting.getArInvoiceLineItem() != null) {
								cost = invFifoCosting.getCstCostOfSales() / invFifoCosting.getCstQuantitySold();
							} else if (invFifoCosting.getInvBuildUnbuildAssemblyLine() != null) {
								cost = invFifoCosting.getCstAssemblyCost() / invFifoCosting.getCstAssemblyQuantity();
							} else {
								cost = invFifoCosting.getCstAdjustCost() /invFifoCosting.getCstAdjustQuantity();
							}

							if(neededQty <= invFifoCosting.getCstRemainingLifoQuantity()) {

								invFifoCosting.setCstRemainingLifoQuantity(invFifoCosting.getCstRemainingLifoQuantity() - neededQty);
								totalCost += (neededQty * cost);
								neededQty = 0d;
							} else {

								neededQty -= invFifoCosting.getCstRemainingLifoQuantity();
								totalCost += (invFifoCosting.getCstRemainingLifoQuantity() * cost);
								invFifoCosting.setCstRemainingLifoQuantity(0);
							}
						}

						//if needed qty is not yet satisfied but no more quantities to fetch, get the default cost
						if(neededQty != 0) {

							LocalInvItemLocation invItemLocation = invItemLocationHome.findByPrimaryKey(IL_CODE);
							totalCost += (neededQty * invItemLocation.getInvItem().getIiUnitCost());
						}

						cost = totalCost / -CST_QTY;
					}

					else {

						//for positive quantities
						cost = CST_COST;
					}
					return cost;
				}

				else {

					//executed during ENTRY transaction

					LocalInvCosting invFifoCosting = (LocalInvCosting)x.next();

					if (invFifoCosting.getApPurchaseOrderLine() != null || invFifoCosting.getApVoucherLineItem() != null) {
						return EJBCommon.roundIt(invFifoCosting.getCstItemCost() / invFifoCosting.getCstQuantityReceived(), this.getInvGpCostPrecisionUnit(AD_CMPNY));
					} else if(invFifoCosting.getArInvoiceLineItem() != null) {
						return  EJBCommon.roundIt(invFifoCosting.getCstCostOfSales() / invFifoCosting.getCstQuantitySold(), this.getInvGpCostPrecisionUnit(AD_CMPNY));
					} else if (invFifoCosting.getInvBuildUnbuildAssemblyLine() != null) {
						return EJBCommon.roundIt(invFifoCosting.getCstAssemblyCost() / invFifoCosting.getCstAssemblyQuantity(), this.getInvGpCostPrecisionUnit(AD_CMPNY));
					} else {
						return EJBCommon.roundIt(invFifoCosting.getCstAdjustCost() /invFifoCosting.getCstAdjustQuantity(), this.getInvGpCostPrecisionUnit(AD_CMPNY));
					}
				}
			}
			else {

				//most applicable in 1st entries of data
				LocalInvItemLocation invItemLocation = invItemLocationHome.findByPrimaryKey(IL_CODE);
				return invItemLocation.getInvItem().getIiUnitCost();
			}

		}
		catch (Exception ex) {
			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());
		}
	}


    private void addApDrPlEntry(short DR_LN, String DR_CLSS, byte DR_DBT, double DR_AMNT, Integer COA_CODE, LocalApPurchaseOrder apPurchaseOrder, Integer AD_BRNCH, Integer AD_CMPNY) throws
        GlobalBranchAccountNumberInvalidException {

    	Debug.print("ApReceivingItemEntryControllerBean addApDrPlEntry");

    	LocalApDistributionRecordHome apDistributionRecordHome = null;
    	LocalGlChartOfAccountHome glChartOfAccountHome = null;
    	LocalAdCompanyHome adCompanyHome = null;


    	// Initialize EJB Home

    	try {

    		apDistributionRecordHome = (LocalApDistributionRecordHome)EJBHomeFactory.
				lookUpLocalHome(LocalApDistributionRecordHome.JNDI_NAME, LocalApDistributionRecordHome.class);
    		glChartOfAccountHome = (LocalGlChartOfAccountHome)EJBHomeFactory.
				lookUpLocalHome(LocalGlChartOfAccountHome.JNDI_NAME, LocalGlChartOfAccountHome.class);
    		adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);

    	} catch (NamingException ex) {

    		throw new EJBException(ex.getMessage());

    	}

    	try {

    		// get company

    		LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);

    		LocalGlChartOfAccount glChartOfAccount = null;

    		try {
    		    System.out.println("COA_CODE="+COA_CODE);
    		    System.out.println("AD_BRNCH="+AD_BRNCH);
    		    glChartOfAccount = glChartOfAccountHome.findByCoaCodeAndBranchCode(COA_CODE, AD_BRNCH, AD_CMPNY);

    		    if (glChartOfAccount.getCoaEnable() == EJBCommon.FALSE)
        		    throw new GlobalBranchAccountNumberInvalidException();

    		} catch (FinderException ex) {

        		throw new GlobalBranchAccountNumberInvalidException();

        	}

    		// create distribution record

    		LocalApDistributionRecord apDistributionRecord = apDistributionRecordHome.create(
    				DR_LN, DR_CLSS, EJBCommon.roundIt(DR_AMNT, adCompany.getGlFunctionalCurrency().getFcPrecision()),
					DR_DBT, EJBCommon.FALSE, EJBCommon.FALSE, AD_CMPNY);

    		//apPurchaseOrder.addApDistributionRecord(apDistributionRecord);
    		apDistributionRecord.setApPurchaseOrder(apPurchaseOrder);
    		//glChartOfAccount.addApDistributionRecord(apDistributionRecord);
    		apDistributionRecord.setGlChartOfAccount(glChartOfAccount);

    	} catch(GlobalBranchAccountNumberInvalidException ex) {

    	    throw ex;

    	} catch (Exception ex) {

    		Debug.printStackTrace(ex);
    		getSessionContext().setRollbackOnly();
    		throw new EJBException(ex.getMessage());

    	}

    }

    private void executeApReceivingPost(Integer PO_CODE, String USR_NM, Integer AD_BRNCH, Integer AD_CMPNY) throws
        GlobalRecordAlreadyDeletedException,
		GlobalTransactionAlreadyPostedException,
		GlobalTransactionAlreadyVoidException,
		GlJREffectiveDateNoPeriodExistException,
		GlJREffectiveDatePeriodClosedException,
		GlobalJournalNotBalanceException,
		AdPRFCoaGlVarianceAccountNotFoundException {

        Debug.print("ApReceivingItemEntryControllerBean executeApReceivingPost");

        LocalApPurchaseOrderHome apPurchaseOrderHome = null;
        LocalAdCompanyHome adCompanyHome = null;
        LocalAdPreferenceHome adPreferenceHome = null;
        LocalGlSetOfBookHome glSetOfBookHome = null;
        LocalGlAccountingCalendarValueHome glAccountingCalendarValueHome = null;
        LocalGlJournalHome glJournalHome = null;
        LocalGlJournalBatchHome glJournalBatchHome = null;
        LocalGlSuspenseAccountHome glSuspenseAccountHome = null;
        LocalGlJournalLineHome glJournalLineHome = null;
        LocalGlJournalSourceHome glJournalSourceHome = null;
        LocalGlJournalCategoryHome glJournalCategoryHome = null;
        LocalGlFunctionalCurrencyHome glFunctionalCurrencyHome = null;
        LocalApDistributionRecordHome apDistributionRecordHome = null;
        LocalInvCostingHome invCostingHome = null;
        LocalGlChartOfAccountHome glChartOfAccountHome = null;
 		LocalGlForexLedgerHome glForexLedgerHome = null;
 		LocalGlFunctionalCurrencyRateHome glFunctionalCurrencyRateHome = null;

        LocalApPurchaseOrder apPurchaseOrder = null;

        // Initialize EJB Home

        try {

            apPurchaseOrderHome = (LocalApPurchaseOrderHome)EJBHomeFactory.
                lookUpLocalHome(LocalApPurchaseOrderHome.JNDI_NAME, LocalApPurchaseOrderHome.class);
            adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
            adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
            	lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);
            glSetOfBookHome = (LocalGlSetOfBookHome)EJBHomeFactory.
            	lookUpLocalHome(LocalGlSetOfBookHome.JNDI_NAME, LocalGlSetOfBookHome.class);
            glAccountingCalendarValueHome = (LocalGlAccountingCalendarValueHome)EJBHomeFactory.
            	lookUpLocalHome(LocalGlAccountingCalendarValueHome.JNDI_NAME, LocalGlAccountingCalendarValueHome.class);
            glJournalHome = (LocalGlJournalHome)EJBHomeFactory.
            	lookUpLocalHome(LocalGlJournalHome.JNDI_NAME, LocalGlJournalHome.class);
            glJournalBatchHome = (LocalGlJournalBatchHome)EJBHomeFactory.
            	lookUpLocalHome(LocalGlJournalBatchHome.JNDI_NAME, LocalGlJournalBatchHome.class);
            glSuspenseAccountHome = (LocalGlSuspenseAccountHome)EJBHomeFactory.
            	lookUpLocalHome(LocalGlSuspenseAccountHome.JNDI_NAME, LocalGlSuspenseAccountHome.class);
            glJournalLineHome = (LocalGlJournalLineHome)EJBHomeFactory.
            	lookUpLocalHome(LocalGlJournalLineHome.JNDI_NAME, LocalGlJournalLineHome.class);
            glJournalSourceHome = (LocalGlJournalSourceHome)EJBHomeFactory.
            	lookUpLocalHome(LocalGlJournalSourceHome.JNDI_NAME, LocalGlJournalSourceHome.class);
            glJournalCategoryHome = (LocalGlJournalCategoryHome)EJBHomeFactory.
            	lookUpLocalHome(LocalGlJournalCategoryHome.JNDI_NAME, LocalGlJournalCategoryHome.class);
            glFunctionalCurrencyHome = (LocalGlFunctionalCurrencyHome)EJBHomeFactory.
            	lookUpLocalHome(LocalGlFunctionalCurrencyHome.JNDI_NAME, LocalGlFunctionalCurrencyHome.class);
            apDistributionRecordHome = (LocalApDistributionRecordHome)EJBHomeFactory.
            	lookUpLocalHome(LocalApDistributionRecordHome.JNDI_NAME, LocalApDistributionRecordHome.class);
            invCostingHome = (LocalInvCostingHome)EJBHomeFactory.
            	lookUpLocalHome(LocalInvCostingHome.JNDI_NAME, LocalInvCostingHome.class);
            glChartOfAccountHome = (LocalGlChartOfAccountHome)EJBHomeFactory.
              	lookUpLocalHome(LocalGlChartOfAccountHome.JNDI_NAME, LocalGlChartOfAccountHome.class);
 			glForexLedgerHome  = (LocalGlForexLedgerHome)EJBHomeFactory.
				lookUpLocalHome(LocalGlForexLedgerHome.JNDI_NAME, LocalGlForexLedgerHome.class);
 			glFunctionalCurrencyRateHome = (LocalGlFunctionalCurrencyRateHome)EJBHomeFactory.
				lookUpLocalHome(LocalGlFunctionalCurrencyRateHome.JNDI_NAME, LocalGlFunctionalCurrencyRateHome.class);

        } catch (NamingException ex) {

            throw new EJBException(ex.getMessage());

        }

        try {

        	LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);
        	// validate if receiving item/debit memo is already deleted

        	try {

        		apPurchaseOrder = apPurchaseOrderHome.findByPrimaryKey(PO_CODE);

        	} catch (FinderException ex) {

        		throw new GlobalRecordAlreadyDeletedException();

        	}

        	// validate if receiving item is already posted or void

        	if (apPurchaseOrder.getPoPosted() == EJBCommon.TRUE) {

        		throw new GlobalTransactionAlreadyPostedException();

        	} else if (apPurchaseOrder.getPoVoid() == EJBCommon.TRUE) {

        		throw new GlobalTransactionAlreadyVoidException();
        	}

        	// post receiving item

    		Collection apPurchaseOrderLines = apPurchaseOrder.getApPurchaseOrderLines();

            Iterator c = apPurchaseOrderLines.iterator();

			while(c.hasNext()) {

				LocalApPurchaseOrderLine apPurchaseOrderLine = (LocalApPurchaseOrderLine) c.next();
				LocalInvItemLocation invItemLocation = apPurchaseOrderLine.getInvItemLocation();

				if (apPurchaseOrderLine.getPlQuantity() == 0) continue;

				// start date validation
				if (adPreference.getPrfArAllowPriorDate() == EJBCommon.FALSE) {
					Collection invNegTxnCosting = invCostingHome.findNegTxnByGreaterThanCstDateAndIiNameAndLocName(
						apPurchaseOrder.getPoDate(), invItemLocation.getInvItem().getIiName(),
						invItemLocation.getInvLocation().getLocName(), AD_BRNCH, AD_CMPNY);
					if(!invNegTxnCosting.isEmpty()) throw new GlobalInventoryDateException(invItemLocation.getInvItem().getIiName());
				}


				String II_NM = apPurchaseOrderLine.getInvItemLocation().getInvItem().getIiName();
				String LOC_NM = apPurchaseOrderLine.getInvItemLocation().getInvLocation().getLocName();


				double QTY_RCVD = this.convertByUomFromAndItemAndQuantity(apPurchaseOrderLine.getInvUnitOfMeasure(),
						apPurchaseOrderLine.getInvItemLocation().getInvItem(), apPurchaseOrderLine.getPlQuantity(), apPurchaseOrderLine.getPlConversionFactor(), AD_CMPNY);

				LocalInvCosting invCosting = null;

				try {

					invCosting  = invCostingHome.getByMaxCstDateToLongAndMaxCstLineNumberAndLessThanEqualCstDateAndIiNameAndLocName(apPurchaseOrder.getPoDate(), II_NM, LOC_NM, AD_BRNCH, AD_CMPNY);

				} catch (FinderException ex) {

				}

				double ADDON_COST = apPurchaseOrderLine.getPlAddonCost();
				double COST = this.convertForeignToFunctionalCurrency(apPurchaseOrder.getGlFunctionalCurrency().getFcCode(),
	        		    apPurchaseOrder.getGlFunctionalCurrency().getFcName(),
	        		    apPurchaseOrder.getPoConversionDate(),
	        		    apPurchaseOrder.getPoConversionRate(),
	        		    apPurchaseOrderLine.getPlUnitCost(), AD_CMPNY);

				double AMOUNT = (COST * QTY_RCVD) + ADDON_COST;

				System.out.println("ADDON_COST-------------------------->"+ADDON_COST);
				System.out.println("AMOUNT-------------------------->"+AMOUNT);
				if (invCosting == null) {

					this.postToInv(apPurchaseOrderLine, apPurchaseOrder.getPoDate(),
						//	QTY_RCVD, AMOUNT,
							QTY_RCVD, AMOUNT,
							QTY_RCVD, AMOUNT,
							0d, null, AD_BRNCH, AD_CMPNY);

				} else {

					this.postToInv(apPurchaseOrderLine, apPurchaseOrder.getPoDate(),
							QTY_RCVD, COST * QTY_RCVD,
							invCosting.getCstRemainingQuantity() + QTY_RCVD, invCosting.getCstRemainingValue() + AMOUNT,
							0d, null, AD_BRNCH, AD_CMPNY);

				}

			}

        	// set receiving item post status

           apPurchaseOrder.setPoPosted(EJBCommon.TRUE);
           apPurchaseOrder.setPoPostedBy(USR_NM);
           apPurchaseOrder.setPoDatePosted(EJBCommon.getGcCurrentDateWoTime().getTime());


           // post to gl if necessary

           LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);

       	   // validate if date has no period and period is closed

       	   LocalGlSetOfBook glJournalSetOfBook = null;

       	   try {

       	   	   glJournalSetOfBook = glSetOfBookHome.findByDate(apPurchaseOrder.getPoDate(), AD_CMPNY);

       	   } catch (FinderException ex) {

       	       throw new GlJREffectiveDateNoPeriodExistException();

       	   }

		   LocalGlAccountingCalendarValue glAccountingCalendarValue =
		        glAccountingCalendarValueHome.findByAcCodeAndDate(
		        	glJournalSetOfBook.getGlAccountingCalendar().getAcCode(), apPurchaseOrder.getPoDate(), AD_CMPNY);


		   if (glAccountingCalendarValue.getAcvStatus() == 'N' ||
		        glAccountingCalendarValue.getAcvStatus() == 'C' ||
		        glAccountingCalendarValue.getAcvStatus() == 'P') {

		        throw new GlJREffectiveDatePeriodClosedException();

		   }

		   // check if receiving item is balance if not check suspense posting

           LocalGlJournalLine glOffsetJournalLine = null;

           Collection apDistributionRecords = apDistributionRecordHome.findImportableDrByPoCode(apPurchaseOrder.getPoCode(), AD_CMPNY);

           Iterator j = apDistributionRecords.iterator();

           double TOTAL_DEBIT = 0d;
           double TOTAL_CREDIT = 0d;

           while (j.hasNext()) {

        		LocalApDistributionRecord apDistributionRecord = (LocalApDistributionRecord)j.next();

        		double DR_AMNT = apDistributionRecord.getDrAmount();

        		if (apDistributionRecord.getDrDebit() == EJBCommon.TRUE) {

        			TOTAL_DEBIT += DR_AMNT;

        		} else {

        			TOTAL_CREDIT += DR_AMNT;

        		}

        	}

        	TOTAL_DEBIT = EJBCommon.roundIt(TOTAL_DEBIT, adCompany.getGlFunctionalCurrency().getFcPrecision());
        	TOTAL_CREDIT = EJBCommon.roundIt(TOTAL_CREDIT, adCompany.getGlFunctionalCurrency().getFcPrecision());

        	if (adPreference.getPrfAllowSuspensePosting() == EJBCommon.TRUE &&
        	    TOTAL_DEBIT != TOTAL_CREDIT) {

        	    LocalGlSuspenseAccount glSuspenseAccount = null;

        	    try {

        	        glSuspenseAccount = glSuspenseAccountHome.findByJsNameAndJcName("ACCOUNTS PAYABLES", "RECEIVING ITEM", AD_CMPNY);

        	    } catch (FinderException ex) {

        	    	throw new GlobalJournalNotBalanceException();

        	    }

        	    if (TOTAL_DEBIT - TOTAL_CREDIT < 0) {

        	    	glOffsetJournalLine = glJournalLineHome.create((short)(apDistributionRecords.size() + 1),
        	    	    EJBCommon.TRUE, TOTAL_CREDIT - TOTAL_DEBIT, "", AD_CMPNY);

        	    } else {

        	    	glOffsetJournalLine = glJournalLineHome.create((short)(apDistributionRecords.size() + 1),
        	    	    EJBCommon.FALSE, TOTAL_DEBIT - TOTAL_CREDIT, "", AD_CMPNY);

        	    }

        	    LocalGlChartOfAccount glChartOfAccount = glSuspenseAccount.getGlChartOfAccount();
        	    //glChartOfAccount.addGlJournalLine(glOffsetJournalLine);
        	    glOffsetJournalLine.setGlChartOfAccount(glChartOfAccount);

			} else if (adPreference.getPrfAllowSuspensePosting() == EJBCommon.FALSE &&
			    TOTAL_DEBIT != TOTAL_CREDIT) {

				throw new GlobalJournalNotBalanceException();

			}

		   // create journal entry

    	   LocalGlJournal glJournal = glJournalHome.create(apPurchaseOrder.getPoReferenceNumber(),
    		    apPurchaseOrder.getPoDescription(), apPurchaseOrder.getPoDate(),
    		    0.0d, null, apPurchaseOrder.getPoDocumentNumber(), null, 1d, "N/A", null,
    		    'N', EJBCommon.TRUE, EJBCommon.FALSE,
    		    USR_NM, new Date(),
    		    USR_NM, new Date(),
    		    null, null,
    		    USR_NM, EJBCommon.getGcCurrentDateWoTime().getTime(),
    		    apPurchaseOrder.getApSupplier().getSplTin(), apPurchaseOrder.getApSupplier().getSplName(), EJBCommon.FALSE,
    		    null,
    		    AD_BRNCH, AD_CMPNY);

    	   LocalGlJournalSource glJournalSource = glJournalSourceHome.findByJsName("ACCOUNTS PAYABLES", AD_CMPNY);
    	   glJournal.setGlJournalSource(glJournalSource);

           LocalGlFunctionalCurrency glFunctionalCurrency = glFunctionalCurrencyHome.findByFcName(adCompany.getGlFunctionalCurrency().getFcName(), AD_CMPNY);
           glJournal.setGlFunctionalCurrency(glFunctionalCurrency);

           LocalGlJournalCategory glJournalCategory = glJournalCategoryHome.findByJcName("RECEIVING ITEMS", AD_CMPNY);
           glJournal.setGlJournalCategory(glJournalCategory);

           // create journal lines

           j = apDistributionRecords.iterator();

           while (j.hasNext()) {

        		LocalApDistributionRecord apDistributionRecord = (LocalApDistributionRecord)j.next();

        		double DR_AMNT = 0d;

    			DR_AMNT = apDistributionRecord.getDrAmount();


        		LocalGlJournalLine glJournalLine = glJournalLineHome.create(apDistributionRecord.getDrLine(),
        			apDistributionRecord.getDrDebit(), DR_AMNT, "", AD_CMPNY);

                glJournalLine.setGlChartOfAccount(apDistributionRecord.getGlChartOfAccount());
        	    glJournalLine.setGlJournal(glJournal);
        	    apDistributionRecord.setDrImported(EJBCommon.TRUE);

		  	  	// for FOREX revaluation

		  	  	if((apPurchaseOrder.getGlFunctionalCurrency().getFcCode() !=
		  	  			adCompany.getGlFunctionalCurrency().getFcCode()) &&
					glJournalLine.getGlChartOfAccount().getGlFunctionalCurrency() != null &&
		  	  		(glJournalLine.getGlChartOfAccount().getGlFunctionalCurrency().getFcCode().equals(
		  	  			apPurchaseOrder.getGlFunctionalCurrency().getFcCode()))){

		  	  		double CONVERSION_RATE = 1;

		  	  		if (apPurchaseOrder.getPoConversionRate() != 0 && apPurchaseOrder.getPoConversionRate() != 1) {

		  	  			CONVERSION_RATE = apPurchaseOrder.getPoConversionRate();

		  	  		} else if (apPurchaseOrder.getPoConversionDate() != null){

		  	  			CONVERSION_RATE = this.getFrRateByFrNameAndFrDate(
		  	  					glJournalLine.getGlChartOfAccount().getGlFunctionalCurrency().getFcName(),
								glJournal.getJrConversionDate(), AD_CMPNY);

		  	  		}

		  	  		Collection glForexLedgers = null;

		  	  		try {

		  	  			glForexLedgers = glForexLedgerHome.findLatestGlFrlByFrlDateAndByCoaCode(
		  	  				apPurchaseOrder.getPoDate(), glJournalLine.getGlChartOfAccount().getCoaCode(), AD_CMPNY);

		  	  		} catch(FinderException ex) {

		  	  		}

		  	  		LocalGlForexLedger glForexLedger =
		  	  			(glForexLedgers.isEmpty() || glForexLedgers == null) ? null :
		  	  				(LocalGlForexLedger) glForexLedgers.iterator().next();

		  	  		int FRL_LN = (glForexLedger != null &&
		  	  				glForexLedger.getFrlDate().compareTo(apPurchaseOrder.getPoDate()) == 0) ?
		  	  						glForexLedger.getFrlLine().intValue() + 1 : 1;

		  	  		// compute balance
		  	  		double COA_FRX_BLNC = glForexLedger == null ? 0 : glForexLedger.getFrlBalance();
		  	  		double FRL_AMNT = apDistributionRecord.getDrAmount();

		  	  		if(glJournalLine.getGlChartOfAccount().getCoaAccountType().equalsIgnoreCase("ASSET"))
		  	  			FRL_AMNT = (glJournalLine.getJlDebit() == EJBCommon.TRUE ? FRL_AMNT : (- 1 * FRL_AMNT));
		  	  		else
		  	  			FRL_AMNT = (glJournalLine.getJlDebit() == EJBCommon.TRUE ? (- 1 * FRL_AMNT) : FRL_AMNT);

		  	  		COA_FRX_BLNC = COA_FRX_BLNC + FRL_AMNT;

		  	  		glForexLedger = glForexLedgerHome.create(apPurchaseOrder.getPoDate(), new Integer (FRL_LN),
		  	  			"OTH", FRL_AMNT, CONVERSION_RATE, COA_FRX_BLNC, 0d, AD_CMPNY);

		  	  		//glJournalLine.getGlChartOfAccount().addGlForexLedger(glForexLedger);
		  	  		glForexLedger.setGlChartOfAccount(glJournalLine.getGlChartOfAccount());

		  	  		// propagate balances
		  	  		try{

		  	  			glForexLedgers = glForexLedgerHome.findByGreaterThanFrlDateAndCoaCode(
		  	  				glForexLedger.getFrlDate(), glForexLedger.getGlChartOfAccount().getCoaCode(),
							glForexLedger.getFrlAdCompany());

		  	  		} catch (FinderException ex) {

		  	  		}

		  	  		Iterator itrFrl = glForexLedgers.iterator();

		  	  		while (itrFrl.hasNext()) {

		  	  			glForexLedger = (LocalGlForexLedger) itrFrl.next();
		  	  			FRL_AMNT = apDistributionRecord.getDrAmount();

		  	  			if(glJournalLine.getGlChartOfAccount().getCoaAccountType().equalsIgnoreCase("ASSET"))
		  	  				FRL_AMNT = (glJournalLine.getJlDebit() == EJBCommon.TRUE ? FRL_AMNT : (- 1 * FRL_AMNT));
		  	  			else
		  	  				FRL_AMNT = (glJournalLine.getJlDebit() == EJBCommon.TRUE ? (- 1 * FRL_AMNT) : FRL_AMNT);

		  	  			glForexLedger.setFrlBalance(glForexLedger.getFrlBalance() + FRL_AMNT);

		  	  		}

		  	  	}

           }

           if (glOffsetJournalLine != null) {

        	   glOffsetJournalLine.setGlJournal(glJournal);

           }

           // post journal to gl

           Collection glJournalLines = glJournal.getGlJournalLines();

	       Iterator i = glJournalLines.iterator();

	       while (i.hasNext()) {

	           LocalGlJournalLine glJournalLine = (LocalGlJournalLine)i.next();

	           // post current to current acv

	           this.postToGl(glAccountingCalendarValue,
	               glJournalLine.getGlChartOfAccount(),
	               true, glJournalLine.getJlDebit(), glJournalLine.getJlAmount(), AD_CMPNY);


	           // post to subsequent acvs (propagate)

	           Collection glSubsequentAccountingCalendarValues =
	               glAccountingCalendarValueHome.findSubsequentAcvByAcCodeAndAcvPeriodNumber(
	                   glJournalSetOfBook.getGlAccountingCalendar().getAcCode(),
	                   glAccountingCalendarValue.getAcvPeriodNumber(), AD_CMPNY);

	           Iterator acvsIter = glSubsequentAccountingCalendarValues.iterator();

	           while (acvsIter.hasNext()) {

	         	   LocalGlAccountingCalendarValue glSubsequentAccountingCalendarValue =
	         	       (LocalGlAccountingCalendarValue)acvsIter.next();

	         	   this.postToGl(glSubsequentAccountingCalendarValue,
	         	       glJournalLine.getGlChartOfAccount(),
	         	       false, glJournalLine.getJlDebit(), glJournalLine.getJlAmount(), AD_CMPNY);

	           }

	           // post to subsequent years if necessary

	           Collection glSubsequentSetOfBooks = glSetOfBookHome.findSubsequentSobByAcYear(glJournalSetOfBook.getGlAccountingCalendar().getAcYear(), AD_CMPNY);

		  	  	if (!glSubsequentSetOfBooks.isEmpty() && glJournalSetOfBook.getSobYearEndClosed() == 1) {

			  	  	adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
			  	  	LocalGlChartOfAccount glRetainedEarningsAccount = glChartOfAccountHome.findByCoaAccountNumber(adCompany.getCmpRetainedEarnings(), AD_CMPNY);

			  	  	Iterator sobIter = glSubsequentSetOfBooks.iterator();

			  	  	while (sobIter.hasNext()) {

			  	  		LocalGlSetOfBook glSubsequentSetOfBook = (LocalGlSetOfBook)sobIter.next();

			  	  		String ACCOUNT_TYPE = glJournalLine.getGlChartOfAccount().getCoaAccountType();

			  	  		// post to subsequent acvs of subsequent set of book(propagate)

			           Collection glAccountingCalendarValues =
			               glAccountingCalendarValueHome.findByAcCode(glSubsequentSetOfBook.getGlAccountingCalendar().getAcCode(), AD_CMPNY);

			           Iterator acvIter = glAccountingCalendarValues.iterator();

			           while (acvIter.hasNext()) {

			         	   LocalGlAccountingCalendarValue glSubsequentAccountingCalendarValue =
			         	       (LocalGlAccountingCalendarValue)acvIter.next();

			         	    if (ACCOUNT_TYPE.equals("ASSET") || ACCOUNT_TYPE.equals("LIABILITY") ||
				 				ACCOUNT_TYPE.equals("OWNERS EQUITY")) {

					         	this.postToGl(glSubsequentAccountingCalendarValue,
					         	     glJournalLine.getGlChartOfAccount(),
					         	     false, glJournalLine.getJlDebit(), glJournalLine.getJlAmount(), AD_CMPNY);

					        } else { // revenue & expense

					             this.postToGl(glSubsequentAccountingCalendarValue,
					         	     glRetainedEarningsAccount,
					         	     false, glJournalLine.getJlDebit(), glJournalLine.getJlAmount(), AD_CMPNY);

					        }

			           }

			  	  	   if (glSubsequentSetOfBook.getSobYearEndClosed() == 0) break;

			  	  	}

		  	  	}

           }

        } catch (GlJREffectiveDateNoPeriodExistException ex) {

        	getSessionContext().setRollbackOnly();
        	throw ex;

        } catch (GlJREffectiveDatePeriodClosedException ex) {

        	getSessionContext().setRollbackOnly();
        	throw ex;

        } catch (GlobalJournalNotBalanceException ex) {

        	getSessionContext().setRollbackOnly();
        	throw ex;

        } catch (GlobalRecordAlreadyDeletedException ex) {

        	getSessionContext().setRollbackOnly();
        	throw ex;

        } catch (GlobalTransactionAlreadyPostedException ex) {

        	getSessionContext().setRollbackOnly();
        	throw ex;

        } catch (GlobalTransactionAlreadyVoidException ex) {

        	getSessionContext().setRollbackOnly();
        	throw ex;

        } catch (AdPRFCoaGlVarianceAccountNotFoundException ex) {

        	getSessionContext().setRollbackOnly();
        	throw ex;

    	} catch (Exception ex) {

    		Debug.printStackTrace(ex);
    		getSessionContext().setRollbackOnly();
    		throw new EJBException(ex.getMessage());

    	}

    }

    private double convertForeignToFunctionalCurrency(Integer FC_CODE, String FC_NM,
	    Date CONVERSION_DATE, double CONVERSION_RATE, double AMOUNT, Integer AD_CMPNY) {

	    Debug.print("ApReceivingItemEntryControllerBean convertForeignToFunctionalCurrency");


        LocalGlFunctionalCurrencyRateHome glFunctionalCurrencyRateHome = null;
        LocalAdCompanyHome adCompanyHome = null;

        LocalAdCompany adCompany = null;

        // Initialize EJB Homes

        try {

            glFunctionalCurrencyRateHome = (LocalGlFunctionalCurrencyRateHome)EJBHomeFactory.
               lookUpLocalHome(LocalGlFunctionalCurrencyRateHome.JNDI_NAME, LocalGlFunctionalCurrencyRateHome.class);
            adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
               lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);

         } catch (NamingException ex) {

            throw new EJBException(ex.getMessage());

         }

         // get company and extended precision

         try {

             adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);

         } catch (Exception ex) {

             throw new EJBException(ex.getMessage());

         }


         // Convert to functional currency if necessary

         if (CONVERSION_RATE != 1 && CONVERSION_RATE != 0 ) {

             AMOUNT = AMOUNT / CONVERSION_RATE;

         }

         return EJBCommon.roundIt(AMOUNT, this.getInvGpCostPrecisionUnit(AD_CMPNY));

	}

	private LocalApPurchaseOrderLine addApPlEntry(ApModPurchaseOrderLineDetails mdetails, LocalApPurchaseOrder apPurchaseOrder,
		LocalInvItemLocation invItemLocation, boolean newPurchaseOrder, Integer AD_CMPNY) throws GlobalMiscInfoIsRequiredException {

		Debug.print("ApReceivingItemEntryControllerBean addApPlEntry");

		LocalApPurchaseOrderLineHome apPurchaseOrderLineHome = null;
		LocalInvItemLocationHome invItemLocationHome = null;
		LocalInvUnitOfMeasureHome invUnitOfMeasureHome = null;

        // Initialize EJB Home

        try {

            apPurchaseOrderLineHome = (LocalApPurchaseOrderLineHome)EJBHomeFactory.
                lookUpLocalHome(LocalApPurchaseOrderLineHome.JNDI_NAME, LocalApPurchaseOrderLineHome.class);
            invItemLocationHome = (LocalInvItemLocationHome)EJBHomeFactory.
                lookUpLocalHome(LocalInvItemLocationHome.JNDI_NAME, LocalInvItemLocationHome.class);
            invUnitOfMeasureHome = (LocalInvUnitOfMeasureHome)EJBHomeFactory.
            	lookUpLocalHome(LocalInvUnitOfMeasureHome.JNDI_NAME, LocalInvUnitOfMeasureHome.class);

        } catch (NamingException ex) {

            throw new EJBException(ex.getMessage());

        }

        try {

        	short precisionUnit = this.getGlFcPrecisionUnit(AD_CMPNY);

        	double VLI_AMNT = 0d;
        	double VLI_TAX_AMNT = 0d;

			// calculate net amount



			LocalApTaxCode apTaxCode = apPurchaseOrder.getApTaxCode();
			if (apTaxCode.getTcType().equals("INCLUSIVE")) {

				VLI_AMNT = EJBCommon.roundIt(mdetails.getPlAmount() / (1 + (apTaxCode.getTcRate() / 100)), precisionUnit);

	        } else {

	            // tax exclusive, none, zero rated or exempt

	            VLI_AMNT = mdetails.getPlAmount();

	    	}

	    	// calculate tax

	    	if (!apTaxCode.getTcType().equals("NONE") &&
	    	    !apTaxCode.getTcType().equals("EXEMPT")) {


	        	if (apTaxCode.getTcType().equals("INCLUSIVE")) {

	        		VLI_TAX_AMNT = EJBCommon.roundIt(mdetails.getPlAmount() - VLI_AMNT, precisionUnit);

	        	} else if (apTaxCode.getTcType().equals("EXCLUSIVE")) {

	        		VLI_TAX_AMNT = EJBCommon.roundIt(mdetails.getPlAmount() * apTaxCode.getTcRate() / 100, precisionUnit);

	            } else {

	            	// tax none zero-rated or exempt

	        	}

		    }

		    LocalApPurchaseOrderLine apPurchaseOrderLine = apPurchaseOrderLineHome.create(
	        		mdetails.getPlLine(), mdetails.getPlQuantity(), mdetails.getPlUnitCost(),
	        		VLI_AMNT, mdetails.getPlQcNumber(), mdetails.getPlQcExpiryDate(),
	        		mdetails.getPlConversionFactor(), VLI_TAX_AMNT,
	        		apPurchaseOrder.getPoType().equals("PO MATCHED") ? mdetails.getPlPlCode() : null,
	        		mdetails.getPlDiscount1(), mdetails.getPlDiscount2(), mdetails.getPlDiscount3(), mdetails.getPlDiscount4(), mdetails.getPlTotalDiscount(),
	        		AD_CMPNY);

		    double PO_TTL_AMNT = this.convertForeignToFunctionalCurrency(apPurchaseOrder.getGlFunctionalCurrency().getFcCode(),
          		    apPurchaseOrder.getGlFunctionalCurrency().getFcName(),
          		    apPurchaseOrder.getPoConversionDate(),
          		    apPurchaseOrder.getPoConversionRate(),
          		  apPurchaseOrder.getPoTotalAmount(), AD_CMPNY);

		    double AMOUNT_LOCAL = this.convertForeignToFunctionalCurrency(apPurchaseOrder.getGlFunctionalCurrency().getFcCode(),
          		    apPurchaseOrder.getGlFunctionalCurrency().getFcName(),
          		    apPurchaseOrder.getPoConversionDate(),
          		    apPurchaseOrder.getPoConversionRate(),
          		    VLI_AMNT, AD_CMPNY);

	  	    double TTL_ADDON_COST = apPurchaseOrder.getPoDuties() + apPurchaseOrder.getPoFreight() + apPurchaseOrder.getPoEntryFee() +
  									apPurchaseOrder.getPoStorage() + apPurchaseOrder.getPoWharfageHandling();
	  	    double UNIT_COST_LOCAL = EJBCommon.roundIt(mdetails.getPlUnitCost() * apPurchaseOrder.getPoConversionRate(), precisionUnit);
	  	    double ADDON_COST = EJBCommon.roundIt((AMOUNT_LOCAL / PO_TTL_AMNT) * TTL_ADDON_COST, precisionUnit);

            apPurchaseOrderLine.setPlUnitCostLocal(UNIT_COST_LOCAL);
		    apPurchaseOrderLine.setPlAmountLocal(AMOUNT_LOCAL);
		    apPurchaseOrderLine.setPlAddonCost(ADDON_COST );

		    apPurchaseOrderLine.setApPurchaseOrder(apPurchaseOrder);
		    apPurchaseOrderLine.setInvItemLocation(invItemLocation);
            LocalInvUnitOfMeasure invUnitOfMeasure = invUnitOfMeasureHome.findByUomName(mdetails.getPlUomName(), AD_CMPNY);
            apPurchaseOrderLine.setInvUnitOfMeasure(invUnitOfMeasure);


            //validate misc

//	    	validate misc
            /*if(apPurchaseOrderLine.getInvItemLocation().getInvItem().getIiTraceMisc()==1){
            	if(mdetails.getPlMisc()==null || mdetails.getPlMisc()==""){


	    			throw new GlobalMiscInfoIsRequiredException();

	    		}else{
	    			int qty2Prpgt = Integer.parseInt(this.getQuantityExpiryDates(mdetails.getPlMisc()));

	    			String miscList2Prpgt = this.checkExpiryDates(mdetails.getPlMisc(), qty2Prpgt, "False");
	    			if(miscList2Prpgt!="Error"){
	    				apPurchaseOrderLine.setPlMisc(mdetails.getPlMisc().trim());
	    			}else{
	    				throw new GlobalMiscInfoIsRequiredException();
	    			}

	    		}
            }else{
            	apPurchaseOrderLine.setPlMisc(mdetails.getPlMisc().trim());
            }*/

         	return apPurchaseOrderLine;

        }/*catch (GlobalMiscInfoIsRequiredException ex){

            getSessionContext().setRollbackOnly();
            throw ex;

        } */catch (Exception ex) {

        	Debug.printStackTrace(ex);
        	getSessionContext().setRollbackOnly();
        	throw new EJBException(ex.getMessage());

        }

	}

	public String checkExpiryDates(String misc, double qty, String reverse) throws Exception {
    	//ActionErrors errors = new ActionErrors();
    	Debug.print("ApReceivingItemControllerBean getExpiryDates");

    	String separator = "";
    	if(reverse=="False"){
    		separator ="$";
    	}else{
    		separator =" ";
    	}

    	// Remove first $ character
    	misc = misc.substring(1);

    	// Counter
    	int start = 0;
    	int nextIndex = misc.indexOf(separator, start);
    	int length = nextIndex - start;

    	String miscList = new String();
    	String miscList2 = "";

    	for(int x=0; x<qty; x++) {

    		// Date
    		start = nextIndex + 1;
    		nextIndex = misc.indexOf(separator, start);
    		length = nextIndex - start;
    		String g= misc.substring(start, start + length);

    		if(g.length()!=0){
    			if(g!=null || g!="" || g!="null"){
    				if(g.contains("null")){
    					miscList2 = "Error";
    				}else{
    					miscList = miscList + "$" + g;
    				}
    			}else{
    				miscList2 = "Error";
    			}

    		}else{
    			miscList2 = "Error";
    		}
    	}

    	if(miscList2==""){
    		miscList = miscList+"$";
    	}else{
    		miscList = miscList2;
    	}

    	return (miscList);
    }

   private void postToGl(LocalGlAccountingCalendarValue glAccountingCalendarValue,
      LocalGlChartOfAccount glChartOfAccount,
      boolean isCurrentAcv, byte isDebit, double JL_AMNT, Integer AD_CMPNY) {

      Debug.print("ApReceivingItemEntryControllerBean postToGl");

      LocalGlChartOfAccountBalanceHome glChartOfAccountBalanceHome = null;
      LocalAdCompanyHome adCompanyHome = null;


       // Initialize EJB Home

       try {

           glChartOfAccountBalanceHome = (LocalGlChartOfAccountBalanceHome)EJBHomeFactory.
              lookUpLocalHome(LocalGlChartOfAccountBalanceHome.JNDI_NAME, LocalGlChartOfAccountBalanceHome.class);
           adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
              lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);


       } catch (NamingException ex) {

           throw new EJBException(ex.getMessage());

       }

       try {

               LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);

	       	   LocalGlChartOfAccountBalance glChartOfAccountBalance =
		            glChartOfAccountBalanceHome.findByAcvCodeAndCoaCode(
		               	  glAccountingCalendarValue.getAcvCode(),
		               	  glChartOfAccount.getCoaCode(), AD_CMPNY);

	           String ACCOUNT_TYPE = glChartOfAccount.getCoaAccountType();
	           short FC_EXTNDD_PRCSN = adCompany.getGlFunctionalCurrency().getFcExtendedPrecision();



	           if (((ACCOUNT_TYPE.equals("ASSET") || ACCOUNT_TYPE.equals("EXPENSE")) &&
				    isDebit == EJBCommon.TRUE) ||
				    (!ACCOUNT_TYPE.equals("ASSET") && !ACCOUNT_TYPE.equals("EXPENSE") &&
				    isDebit == EJBCommon.FALSE)) {

 			        glChartOfAccountBalance.setCoabEndingBalance(
				       EJBCommon.roundIt(glChartOfAccountBalance.getCoabEndingBalance() + JL_AMNT, FC_EXTNDD_PRCSN));

				    if (!isCurrentAcv) {

				    	glChartOfAccountBalance.setCoabBeginningBalance(
				       		EJBCommon.roundIt(glChartOfAccountBalance.getCoabBeginningBalance() + JL_AMNT, FC_EXTNDD_PRCSN));

					}


			  } else {

				    glChartOfAccountBalance.setCoabEndingBalance(
				       EJBCommon.roundIt(glChartOfAccountBalance.getCoabEndingBalance() - JL_AMNT, FC_EXTNDD_PRCSN));

				    if (!isCurrentAcv) {

				    	glChartOfAccountBalance.setCoabBeginningBalance(
				       		EJBCommon.roundIt(glChartOfAccountBalance.getCoabBeginningBalance() - JL_AMNT, FC_EXTNDD_PRCSN));

					}

		 	  }

		 	  if (isCurrentAcv) {

			 	 if (isDebit == EJBCommon.TRUE) {

		 			glChartOfAccountBalance.setCoabTotalDebit(
		 				EJBCommon.roundIt(glChartOfAccountBalance.getCoabTotalDebit() + JL_AMNT, FC_EXTNDD_PRCSN));

		 		 } else {

		 			glChartOfAccountBalance.setCoabTotalCredit(
		 				EJBCommon.roundIt(glChartOfAccountBalance.getCoabTotalCredit() + JL_AMNT, FC_EXTNDD_PRCSN));
		 		 }

		 	}

       } catch (Exception ex) {

       	   Debug.printStackTrace(ex);
       	   throw new EJBException(ex.getMessage());

       }


   }

   private void postToInv(LocalApPurchaseOrderLine apPurchaseOrderLine, Date CST_DT, double CST_QTY_RCVD, double CST_ITM_CST,
   		double CST_RMNNG_QTY, double CST_RMNNG_VL, double CST_VRNC_VL, String USR_NM, Integer AD_BRNCH, Integer AD_CMPNY) throws
   		AdPRFCoaGlVarianceAccountNotFoundException {

    	Debug.print("ApReceivingItemEntryControllerBean postToInv");

    	LocalInvCostingHome invCostingHome = null;
    	LocalAdPreferenceHome adPreferenceHome = null;
    	LocalAdCompanyHome adCompanyHome = null;
    	LocalInvAdjustmentLineHome invAdjustmentLineHome = null;

        // Initialize EJB Home

        try {

          invCostingHome = (LocalInvCostingHome)EJBHomeFactory.
              lookUpLocalHome(LocalInvCostingHome.JNDI_NAME, LocalInvCostingHome.class);
          adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
              lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);
          adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
              lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
          invAdjustmentLineHome = (LocalInvAdjustmentLineHome)EJBHomeFactory.
      	      lookUpLocalHome(LocalInvAdjustmentLineHome.JNDI_NAME, LocalInvAdjustmentLineHome.class);

        } catch (NamingException ex) {

          throw new EJBException(ex.getMessage());

        }

        try {

           LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);
           LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
           LocalInvItemLocation invItemLocation = apPurchaseOrderLine.getInvItemLocation();
           int CST_LN_NMBR = 0;

           CST_QTY_RCVD = EJBCommon.roundIt(CST_QTY_RCVD, adPreference.getPrfInvQuantityPrecisionUnit());
           CST_ITM_CST = EJBCommon.roundIt(CST_ITM_CST, adPreference.getPrfInvCostPrecisionUnit());
           CST_RMNNG_QTY = EJBCommon.roundIt(CST_RMNNG_QTY, adPreference.getPrfInvQuantityPrecisionUnit());
           CST_RMNNG_VL = EJBCommon.roundIt(CST_RMNNG_VL, adPreference.getPrfInvCostPrecisionUnit());

           try {

           	   // generate line number
               LocalInvCosting invCurrentCosting = invCostingHome.getByMaxCstLineNumberAndCstDateToLongAndIiNameAndLocName(CST_DT.getTime(), invItemLocation.getInvItem().getIiName(), invItemLocation.getInvLocation().getLocName(), AD_BRNCH, AD_CMPNY);
               CST_LN_NMBR = invCurrentCosting.getCstLineNumber() + 1;

           } catch (FinderException ex) {

           	   CST_LN_NMBR = 1;

           }

           //void subsequent cost variance adjustments
           Collection invAdjustmentLines =invAdjustmentLineHome.findUnvoidAndIsCostVarianceGreaterThanAdjDateAndIlCodeAndBrCode(
           		CST_DT, invItemLocation.getIlCode(), AD_BRNCH, AD_CMPNY);
           Iterator i = invAdjustmentLines.iterator();

           while (i.hasNext()){

           	LocalInvAdjustmentLine invAdjustmentLine = (LocalInvAdjustmentLine) i.next();
           	this.voidInvAdjustment(invAdjustmentLine.getInvAdjustment(), AD_BRNCH, AD_CMPNY);

           }



           String prevExpiryDates = "";
           String prevQcNumbers = "";

           String miscListPrpgt ="";
           double qtyPrpgt = 0;
           try {
        	   LocalInvCosting prevCst = invCostingHome.getByMaxCstDateToLongAndMaxCstLineNumberAndLessThanEqualCstDateAndRemainingQuantityNotEqualToZeroAndIlCode(
        			   CST_DT, invItemLocation.getIlCode(), AD_BRNCH, AD_CMPNY);

        	   prevExpiryDates = prevCst.getCstExpiryDate();
        	   prevQcNumbers = prevCst.getCstQCNumber();

        	   qtyPrpgt = prevCst.getCstRemainingQuantity();
        	   if (prevExpiryDates==null){
        		   prevExpiryDates="";
        	   }

        	   if (prevQcNumbers==null){
        		   prevQcNumbers="";
        	   }

           }catch (Exception ex){
        	   prevQcNumbers = "";
        	   prevExpiryDates="";
           }
           // create costing
           LocalInvCosting invCosting = invCostingHome.create(CST_DT, CST_DT.getTime(), CST_LN_NMBR, CST_QTY_RCVD, CST_ITM_CST, 0d, 0d, 0d, 0d, 0d, 0d, CST_RMNNG_QTY, CST_RMNNG_VL, 0d, 0d, 0d, AD_BRNCH, AD_CMPNY);
           invCosting.setInvItemLocation(invItemLocation);
           invCosting.setApPurchaseOrderLine(apPurchaseOrderLine);
           invCosting.setCstQCNumber(apPurchaseOrderLine.getPlQcNumber());
           invCosting.setCstQCExpiryDate(apPurchaseOrderLine.getPlQcExpiryDate());


           /*// Get Latest QC Numbers
           if(prevQcNumbers!=null && prevQcNumbers!="" && prevQcNumbers.length()!=0){

        	   if(apPurchaseOrderLine.getPlQCNumber()!=null && apPurchaseOrderLine.getPlQCNumber()!="" && apPurchaseOrderLine.getPlQCNumber().length()!=0){

        		   int qty2Prpgt = Integer.parseInt(this.getQuantityExpiryDates(apPurchaseOrderLine.getPlQCNumber()));
        		   String miscList2Prpgt = this.propagateExpiryDates(apPurchaseOrderLine.getPlQCNumber(), qty2Prpgt);
        		   prevQcNumbers = prevQcNumbers.substring(1);
        		   String propagateQcNumberPrpgt = miscList2Prpgt + prevQcNumbers;

        		   invCosting.setCstQCNumber(propagateQcNumberPrpgt);
        	   } else {
        		   invCosting.setCstQCNumber(prevQcNumbers);
        	   }

           }else{

        	   if(apPurchaseOrderLine.getPlQCNumber()!=null && apPurchaseOrderLine.getPlQCNumber()!="" && apPurchaseOrderLine.getPlQCNumber().length()!=0){
        		   int initialQty = Integer.parseInt(this.getQuantityExpiryDates(apPurchaseOrderLine.getPlQCNumber()));
        		   String initialPrpgt = this.propagateExpiryDates(apPurchaseOrderLine.getPlQCNumber(), initialQty);

        		   invCosting.setCstQCNumber(initialPrpgt);
        	   }else{
        		   invCosting.setCstQCNumber(prevExpiryDates);
        	   }

           }*/



           //Get Latest Expiry Dates

           if(prevExpiryDates!=null && prevExpiryDates!="" && prevExpiryDates.length()!=0){

        	   if(apPurchaseOrderLine.getPlMisc()!=null && apPurchaseOrderLine.getPlMisc()!="" && apPurchaseOrderLine.getPlMisc().length()!=0){

        		   int qty2Prpgt = Integer.parseInt(this.getQuantityExpiryDates(apPurchaseOrderLine.getPlMisc()));
        		   String miscList2Prpgt = this.propagateExpiryDates(apPurchaseOrderLine.getPlMisc(), qty2Prpgt);
        		   prevExpiryDates = prevExpiryDates.substring(1);
        		   String propagateMiscPrpgt = miscList2Prpgt + prevExpiryDates;

        		   invCosting.setCstExpiryDate(propagateMiscPrpgt);
        	   }else{
        		   invCosting.setCstExpiryDate(prevExpiryDates);
        	   }
           }else{
        	   if(apPurchaseOrderLine.getPlMisc()!=null && apPurchaseOrderLine.getPlMisc()!="" && apPurchaseOrderLine.getPlMisc().length()!=0){
        		   int initialQty = Integer.parseInt(this.getQuantityExpiryDates(apPurchaseOrderLine.getPlMisc()));
        		   String initialPrpgt = this.propagateExpiryDates(apPurchaseOrderLine.getPlMisc(), initialQty);

        		   invCosting.setCstExpiryDate(initialPrpgt);
        	   }else{
        		   invCosting.setCstExpiryDate(prevExpiryDates);
        	   }
           }




			// if cost variance is not 0, generate cost variance for the transaction
			if(CST_VRNC_VL != 0) {

				this.generateCostVariance(invCosting.getInvItemLocation(), CST_VRNC_VL,
						"APRI" + apPurchaseOrderLine.getApPurchaseOrder().getPoDocumentNumber(),
						apPurchaseOrderLine.getApPurchaseOrder().getPoDescription(),
						apPurchaseOrderLine.getApPurchaseOrder().getPoDate(), USR_NM, AD_BRNCH, AD_CMPNY);

			}

           // propagate balance if necessary

           Collection invCostings = invCostingHome.findByGreaterThanCstDateAndIiNameAndLocName(CST_DT, invItemLocation.getInvItem().getIiName(), invItemLocation.getInvLocation().getLocName(), AD_BRNCH, AD_CMPNY);
           String miscList = "";
           i = invCostings.iterator();
           if(apPurchaseOrderLine.getPlMisc()!=null && apPurchaseOrderLine.getPlMisc()!="" && apPurchaseOrderLine.getPlMisc().length()!=0){
        	   double qty = Double.parseDouble(this.getQuantityExpiryDates(apPurchaseOrderLine.getPlMisc()));
        	   miscList = this.propagateExpiryDates(apPurchaseOrderLine.getPlMisc(), qty);
           }
          // miscList = miscList.substring(1);
           while (i.hasNext()) {

               LocalInvCosting invPropagatedCosting = (LocalInvCosting)i.next();

        	   invPropagatedCosting.setCstRemainingQuantity(invPropagatedCosting.getCstRemainingQuantity() + CST_QTY_RCVD);
               invPropagatedCosting.setCstRemainingValue(invPropagatedCosting.getCstRemainingValue() + CST_ITM_CST);
 			   //String miscList2 = propagateMisc;//this.propagateExpiryDates(invCosting.getCstExpiryDate(), qty);
               /*
               if(apPurchaseOrderLine.getPlMisc()!=null && apPurchaseOrderLine.getPlMisc()!="" && apPurchaseOrderLine.getPlMisc().length()!=0){


            	   String propagateMisc = miscList+invPropagatedCosting.getCstExpiryDate().substring(1);

            	   invPropagatedCosting.setCstExpiryDate(propagateMisc);
               }else{
            	   invPropagatedCosting.setCstExpiryDate(invPropagatedCosting.getCstExpiryDate());
               }*/

           }

           // regenerate cost variance
           this.regenerateCostVariance(invCostings, invCosting, AD_BRNCH, AD_CMPNY);

        } catch (AdPRFCoaGlVarianceAccountNotFoundException ex){

        	throw ex;

        } catch (Exception ex) {

      	   Debug.printStackTrace(ex);
      	   getSessionContext().setRollbackOnly();
           throw new EJBException(ex.getMessage());

        }



    }

    private double convertByUomFromAndItemAndQuantity(LocalInvUnitOfMeasure invFromUnitOfMeasure, LocalInvItem invItem, double QTY_RCVD, double conversionFactor, Integer AD_CMPNY) {

		Debug.print("ApReceivingItemEntryControllerBean convertByUomFromAndUomToAndQuantity");

		LocalAdPreferenceHome adPreferenceHome = null;
		LocalInvUnitOfMeasureConversionHome invUnitOfMeasureConversionHome = null;

        // Initialize EJB Home

        try {

            adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
                 lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);
            invUnitOfMeasureConversionHome = (LocalInvUnitOfMeasureConversionHome)EJBHomeFactory.
				lookUpLocalHome(LocalInvUnitOfMeasureConversionHome.JNDI_NAME, LocalInvUnitOfMeasureConversionHome.class);

        } catch (NamingException ex) {

            throw new EJBException(ex.getMessage());

        }

        try {

        	Debug.print("ApReceivingItemEntryControllerBean convertByUomFromAndUomToAndQuantity A");
            LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);

            LocalInvUnitOfMeasureConversion invUnitOfMeasureConversion = invUnitOfMeasureConversionHome.findUmcByIiNameAndUomName(invItem.getIiName(), invFromUnitOfMeasure.getUomName(), AD_CMPNY);
            LocalInvUnitOfMeasureConversion invDefaultUomConversion = invUnitOfMeasureConversionHome.findUmcByIiNameAndUomName(invItem.getIiName(), invItem.getInvUnitOfMeasure().getUomName(), AD_CMPNY);
            Debug.print("ApReceivingItemEntryControllerBean convertByUomFromAndUomToAndQuantity B");
        	//return EJBCommon.roundIt(QTY_RCVD * invDefaultUomConversion.getUmcConversionFactor() / invUnitOfMeasureConversion.getUmcConversionFactor(), adPreference.getPrfInvQuantityPrecisionUnit());

            return EJBCommon.roundIt(QTY_RCVD * conversionFactor, adPreference.getPrfInvQuantityPrecisionUnit());


        } catch (Exception ex) {

        	Debug.printStackTrace(ex);
        	getSessionContext().setRollbackOnly();
        	throw new EJBException(ex.getMessage());

        }

	}

    private double getConversionFactorByUomFromAndItem(LocalInvUnitOfMeasure invFromUnitOfMeasure, LocalInvItem invItem, Integer AD_CMPNY) {

		Debug.print("ApReceivingItemEntryControllerBean getConversionFactorByUomFromAndItem");

		LocalAdPreferenceHome adPreferenceHome = null;
		LocalInvUnitOfMeasureConversionHome invUnitOfMeasureConversionHome = null;

        // Initialize EJB Home

        try {

            adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
                 lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);
            invUnitOfMeasureConversionHome = (LocalInvUnitOfMeasureConversionHome)EJBHomeFactory.
				lookUpLocalHome(LocalInvUnitOfMeasureConversionHome.JNDI_NAME, LocalInvUnitOfMeasureConversionHome.class);

        } catch (NamingException ex) {

            throw new EJBException(ex.getMessage());

        }

        try {

            LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);

            LocalInvUnitOfMeasureConversion invUnitOfMeasureConversion = invUnitOfMeasureConversionHome.findUmcByIiNameAndUomName(invItem.getIiName(), invFromUnitOfMeasure.getUomName(), AD_CMPNY);
            LocalInvUnitOfMeasureConversion invDefaultUomConversion = invUnitOfMeasureConversionHome.findUmcByIiNameAndUomName(invItem.getIiName(), invItem.getInvUnitOfMeasure().getUomName(), AD_CMPNY);

        	return EJBCommon.roundIt(invDefaultUomConversion.getUmcConversionFactor() / invUnitOfMeasureConversion.getUmcConversionFactor(), adPreference.getPrfInvQuantityPrecisionUnit());



        } catch (Exception ex) {

        	Debug.printStackTrace(ex);
        	getSessionContext().setRollbackOnly();
        	throw new EJBException(ex.getMessage());

        }

	}

    public String getQuantityExpiryDates(String qntty){
    	String separator = "$";

    	// Remove first $ character
    	qntty = qntty.substring(1);

    	// Counter
    	int start = 0;
    	int nextIndex = qntty.indexOf(separator, start);
    	int length = nextIndex - start;
    	String y;
    	y = (qntty.substring(start, start + length));

    	return y;
    }

    public String propagateExpiryDates(String misc, double qty) throws Exception {
    	//ActionErrors errors = new ActionErrors();

    	Debug.print("ApReceivingItemControllerBean getExpiryDates");

    	String separator = "$";

    	// Remove first $ character
    	misc = misc.substring(1);

    	// Counter
    	int start = 0;
    	int nextIndex = misc.indexOf(separator, start);
    	int length = nextIndex - start;

    	String miscList = new String();

		for(int x=0; x<qty; x++) {

			// Date
			start = nextIndex + 1;
			nextIndex = misc.indexOf(separator, start);
			length = nextIndex - start;
			/*SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
	        sdf.setLenient(false);*/
	        try {

	        	miscList = miscList + "$" +(misc.substring(start, start + length));
	        } catch (Exception ex) {

	        	throw ex;
	        }


		}

		miscList = miscList+"$";
		return (miscList);
    }

    private void voidInvAdjustment(LocalInvAdjustment invAdjustment, Integer AD_BRNCH, Integer AD_CMPNY){

    	Debug.print("ApReceivingItemEntryControllerBean voidInvAdjustment");

    	try{

    			Collection invDistributionRecords = invAdjustment.getInvDistributionRecords();
    			ArrayList list = new ArrayList();

    			Iterator i = invDistributionRecords.iterator();

    			while (i.hasNext()) {

    				LocalInvDistributionRecord invDistributionRecord = (LocalInvDistributionRecord)i.next();

    				list.add(invDistributionRecord);

    			}

    			i = list.iterator();

    			while (i.hasNext()) {

    				LocalInvDistributionRecord invDistributionRecord = (LocalInvDistributionRecord)i.next();

    				this.addInvDrEntry(invAdjustment.getInvDrNextLine(), invDistributionRecord.getDrClass(),
    						invDistributionRecord.getDrDebit() == EJBCommon.TRUE ? EJBCommon.FALSE : EJBCommon.TRUE,
    								invDistributionRecord.getDrAmount(), EJBCommon.TRUE,
									invDistributionRecord.getInvChartOfAccount().getCoaCode(), invAdjustment, AD_BRNCH, AD_CMPNY);

    			}

    			Collection invAjustmentLines = invAdjustment.getInvAdjustmentLines();
    			i = invAjustmentLines.iterator();
    			list.clear();

    			while (i.hasNext()) {

    				LocalInvAdjustmentLine invAdjustmentLine = (LocalInvAdjustmentLine)i.next();

    				list.add(invAdjustmentLine);

    			}

    			i = list.iterator();

    			while (i.hasNext()) {

    				LocalInvAdjustmentLine invAdjustmentLine = (LocalInvAdjustmentLine)i.next();

    				this.addInvAlEntry(invAdjustmentLine.getInvItemLocation(),
    						invAdjustment, (invAdjustmentLine.getAlUnitCost()) * - 1, EJBCommon.TRUE, AD_CMPNY);

    			}

    			invAdjustment.setAdjVoid(EJBCommon.TRUE);

    			this.executeInvAdjPost(invAdjustment.getAdjCode(), invAdjustment.getAdjLastModifiedBy(), AD_BRNCH, AD_CMPNY);

    	} catch (Exception ex) {

    		Debug.printStackTrace(ex);
    		getSessionContext().setRollbackOnly();
    		throw new EJBException(ex.getMessage());

    	}

    }

    private void generateCostVariance(LocalInvItemLocation invItemLocation, double CST_VRNC_VL, String ADJ_RFRNC_NMBR,
    		String ADJ_DSCRPTN, Date ADJ_DT, String USR_NM, Integer AD_BRNCH,
			Integer AD_CMPNY) throws
			AdPRFCoaGlVarianceAccountNotFoundException {
    	/*
    	Debug.print("ApReceivingItemEntryControllerBean generateCostVariance");

    	LocalAdPreferenceHome adPreferenceHome = null;
    	LocalGlChartOfAccountHome glChartOfAccountHome = null;
    	LocalAdBranchItemLocationHome adBranchItemLocationHome = null;

    	// Initialize EJB Home

    	try {

    		adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);
    		glChartOfAccountHome = (LocalGlChartOfAccountHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlChartOfAccountHome.JNDI_NAME, LocalGlChartOfAccountHome.class);
    		adBranchItemLocationHome = (LocalAdBranchItemLocationHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdBranchItemLocationHome.JNDI_NAME, LocalAdBranchItemLocationHome.class);


    	} catch (NamingException ex) {

    		throw new EJBException(ex.getMessage());

    	}

    	try{

    		LocalInvAdjustment newInvAdjustment = this.saveInvAdjustment(ADJ_RFRNC_NMBR, ADJ_DSCRPTN, ADJ_DT, USR_NM, AD_BRNCH,
    				AD_CMPNY);
    		LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);
    		LocalGlChartOfAccount glCoaVarianceAccount = null;

    		if(adPreference.getPrfInvGlCoaVarianceAccount() == null)
    			throw new AdPRFCoaGlVarianceAccountNotFoundException();

    		try{

    			glCoaVarianceAccount = glChartOfAccountHome.findByPrimaryKey(adPreference.getPrfInvGlCoaVarianceAccount());
    			//glCoaVarianceAccount.addInvAdjustment(newInvAdjustment);
    			newInvAdjustment.setGlChartOfAccount(glCoaVarianceAccount);

    		} catch (FinderException ex) {

    			throw new AdPRFCoaGlVarianceAccountNotFoundException();

    		}

    		LocalInvAdjustmentLine invAdjustmentLine = this.addInvAlEntry(invItemLocation, newInvAdjustment, CST_VRNC_VL,
    				EJBCommon.FALSE, AD_CMPNY);

    		// check for branch mapping

    		LocalAdBranchItemLocation adBranchItemLocation = null;

    		try{

    			adBranchItemLocation = adBranchItemLocationHome.findBilByIlCodeAndBrCode(
    					invAdjustmentLine.getInvItemLocation().getIlCode(), AD_BRNCH, AD_CMPNY);

    		} catch (FinderException ex) {

    		}

    		LocalGlChartOfAccount glInventoryChartOfAccount = null;

    		if (adBranchItemLocation == null) {

    			glInventoryChartOfAccount = glChartOfAccountHome.findByPrimaryKey(
    					invAdjustmentLine.getInvItemLocation().getIlGlCoaInventoryAccount());
    		} else {

    			glInventoryChartOfAccount = glChartOfAccountHome.findByPrimaryKey(
    					adBranchItemLocation.getBilCoaGlInventoryAccount());

    		}


    		boolean isDebit = CST_VRNC_VL < 0 ? false : true;

    		//inventory dr
    		this.addInvDrEntry(newInvAdjustment.getInvDrNextLine(), "INVENTORY",
    				isDebit == true ? EJBCommon.TRUE : EJBCommon.FALSE, Math.abs(CST_VRNC_VL), EJBCommon.FALSE,
    						glInventoryChartOfAccount.getCoaCode(), newInvAdjustment, AD_BRNCH, AD_CMPNY);

    		//variance dr
    		this.addInvDrEntry(newInvAdjustment.getInvDrNextLine(),"VARIANCE",
    				!isDebit == true ? EJBCommon.TRUE : EJBCommon.FALSE, Math.abs(CST_VRNC_VL), EJBCommon.FALSE,
    						glCoaVarianceAccount.getCoaCode(), newInvAdjustment, AD_BRNCH, AD_CMPNY);

    		this.executeInvAdjPost(newInvAdjustment.getAdjCode(), newInvAdjustment.getAdjLastModifiedBy(), AD_BRNCH,
    				AD_CMPNY);

    	} catch (AdPRFCoaGlVarianceAccountNotFoundException ex) {

    		getSessionContext().setRollbackOnly();
    		throw ex;

    	} catch (Exception ex) {

    		Debug.printStackTrace(ex);
    		getSessionContext().setRollbackOnly();
    		throw new EJBException(ex.getMessage());

    	}
    	*/
    }

    private void regenerateCostVariance(Collection invCostings, LocalInvCosting invCosting, Integer AD_BRNCH, Integer AD_CMPNY)
    throws AdPRFCoaGlVarianceAccountNotFoundException {
    	/*
    	Debug.print("ApReceivingItemEntryControllerBean regenerateCostVariance");

    	try {

    		Iterator i = invCostings.iterator();
    		LocalInvCosting prevInvCosting = invCosting;

    		while (i.hasNext()) {

    			LocalInvCosting invPropagatedCosting = (LocalInvCosting)i.next();

    			if(prevInvCosting.getCstRemainingQuantity() < 0) {

    				double TTL_CST = 0;
    				double QNTY = 0;
    				String ADJ_RFRNC_NMBR = "";
    				String ADJ_DSCRPTN = "";
    				String ADJ_CRTD_BY = "";

    				// get unit cost adjusment, document number and unit of measure
    				Debug.print("ApReceivingItemEntryControllerBean regenerateCostVariance A");
    				if (invPropagatedCosting.getApPurchaseOrderLine() != null) {

    					TTL_CST = invPropagatedCosting.getApPurchaseOrderLine().getPlAmount();
						QNTY =  this.convertByUomFromAndItemAndQuantity(
								invPropagatedCosting.getApPurchaseOrderLine().getInvUnitOfMeasure(),
								invPropagatedCosting.getApPurchaseOrderLine().getInvItemLocation().getInvItem(),
								invPropagatedCosting.getApPurchaseOrderLine().getPlQuantity(), AD_CMPNY);

    					ADJ_DSCRPTN = invPropagatedCosting.getApPurchaseOrderLine().getApPurchaseOrder().getPoDescription();
    					ADJ_CRTD_BY = invPropagatedCosting.getApPurchaseOrderLine().getApPurchaseOrder().getPoPostedBy();
    					ADJ_RFRNC_NMBR = "APRI" +
						invPropagatedCosting.getApPurchaseOrderLine().getApPurchaseOrder().getPoDocumentNumber();

    					Debug.print("ApReceivingItemEntryControllerBean regenerateCostVariance B");

    				} else if (invPropagatedCosting.getApVoucherLineItem() != null){

    					TTL_CST = invPropagatedCosting.getApVoucherLineItem().getVliAmount();
						QNTY = this.convertByUomFromAndItemAndQuantity(
								invPropagatedCosting.getApVoucherLineItem().getInvUnitOfMeasure(),
								invPropagatedCosting.getApVoucherLineItem().getInvItemLocation().getInvItem(),
								invPropagatedCosting.getApVoucherLineItem().getVliQuantity(), AD_CMPNY);

    					if (invPropagatedCosting.getApVoucherLineItem().getApVoucher() != null) {

    						ADJ_DSCRPTN = invPropagatedCosting.getApVoucherLineItem().getApVoucher().getVouDescription();
    						ADJ_CRTD_BY = invPropagatedCosting.getApVoucherLineItem().getApVoucher().getVouPostedBy();
    						ADJ_RFRNC_NMBR = "APVOU" +
							invPropagatedCosting.getApVoucherLineItem().getApVoucher().getVouDocumentNumber();

    					} else if (invPropagatedCosting.getApVoucherLineItem().getApCheck() != null) {

    						ADJ_DSCRPTN = invPropagatedCosting.getApVoucherLineItem().getApCheck().getChkDescription();
    						ADJ_CRTD_BY = invPropagatedCosting.getApVoucherLineItem().getApCheck().getChkPostedBy();
    						ADJ_RFRNC_NMBR = "APCHK" +
							invPropagatedCosting.getApVoucherLineItem().getApCheck().getChkDocumentNumber();

    					}
    					Debug.print("ApReceivingItemEntryControllerBean regenerateCostVariance C");
    				} else if (invPropagatedCosting.getArInvoiceLineItem() != null){

    					QNTY = this.convertByUomFromAndItemAndQuantity(
    							invPropagatedCosting.getArInvoiceLineItem().getInvUnitOfMeasure(),
    							invPropagatedCosting.getArInvoiceLineItem().getInvItemLocation().getInvItem(),
    							invPropagatedCosting.getArInvoiceLineItem().getIliQuantity(), AD_CMPNY);
    					TTL_CST = prevInvCosting.getCstRemainingValue() - invPropagatedCosting.getCstRemainingValue();

    					if(invPropagatedCosting.getArInvoiceLineItem().getArInvoice() != null){

    						ADJ_DSCRPTN = invPropagatedCosting.getArInvoiceLineItem().getArInvoice().getInvDescription();
    						ADJ_CRTD_BY = invPropagatedCosting.getArInvoiceLineItem().getArInvoice().getInvPostedBy();
    						ADJ_RFRNC_NMBR = "ARCM" +
    						invPropagatedCosting.getArInvoiceLineItem().getArInvoice().getInvNumber();

    					} else if(invPropagatedCosting.getArInvoiceLineItem().getArReceipt() != null){

    						ADJ_DSCRPTN = invPropagatedCosting.getArInvoiceLineItem().getArReceipt().getRctDescription();
    						ADJ_CRTD_BY = invPropagatedCosting.getArInvoiceLineItem().getArReceipt().getRctPostedBy();
    						ADJ_RFRNC_NMBR = "ARMR" +
    						invPropagatedCosting.getArInvoiceLineItem().getArReceipt().getRctNumber();

    					}
    					Debug.print("ApReceivingItemEntryControllerBean regenerateCostVariance D");

    				} else if (invPropagatedCosting.getArSalesOrderInvoiceLine() != null){

   						TTL_CST = prevInvCosting.getCstRemainingValue() - invPropagatedCosting.getCstRemainingValue();
    					QNTY = this.convertByUomFromAndItemAndQuantity(
								invPropagatedCosting.getArSalesOrderInvoiceLine().getArSalesOrderLine().getInvUnitOfMeasure(),
								invPropagatedCosting.getArSalesOrderInvoiceLine().getArSalesOrderLine().getInvItemLocation().getInvItem(),
								invPropagatedCosting.getArSalesOrderInvoiceLine().getSilQuantityDelivered(), AD_CMPNY);

    					ADJ_DSCRPTN = invPropagatedCosting.getArSalesOrderInvoiceLine().getArInvoice().getInvDescription();
    					ADJ_CRTD_BY = invPropagatedCosting.getArSalesOrderInvoiceLine().getArInvoice().getInvPostedBy();
    					ADJ_RFRNC_NMBR = "ARCM" +
						invPropagatedCosting.getArSalesOrderInvoiceLine().getArInvoice().getInvNumber();

    					Debug.print("ApReceivingItemEntryControllerBean regenerateCostVariance E");

    				} else if (invPropagatedCosting.getInvAdjustmentLine() != null){

    					ADJ_DSCRPTN = invPropagatedCosting.getInvAdjustmentLine().getInvAdjustment().getAdjDescription();
    					ADJ_CRTD_BY = invPropagatedCosting.getInvAdjustmentLine().getInvAdjustment().getAdjPostedBy();
    					ADJ_RFRNC_NMBR = "INVADJ" +
    					invPropagatedCosting.getInvAdjustmentLine().getInvAdjustment().getAdjDocumentNumber();

    					if(invPropagatedCosting.getInvAdjustmentLine().getAlAdjustQuantity() != 0) {

    						TTL_CST = (invPropagatedCosting.getInvAdjustmentLine().getAlUnitCost() *
    								invPropagatedCosting.getInvAdjustmentLine().getAlAdjustQuantity());
    						QNTY =  this.convertByUomFromAndItemAndQuantity(
    								invPropagatedCosting.getInvAdjustmentLine().getInvUnitOfMeasure(),
    								invPropagatedCosting.getInvAdjustmentLine().getInvItemLocation().getInvItem(),
    								invPropagatedCosting.getInvAdjustmentLine().getAlAdjustQuantity(), AD_CMPNY);

    					}
    					Debug.print("ApReceivingItemEntryControllerBean regenerateCostVariance F");

    				} else if (invPropagatedCosting.getInvAssemblyTransferLine() != null){

    					QNTY = invPropagatedCosting.getInvAssemblyTransferLine().getAtlAssembleQuantity();
    					TTL_CST = invPropagatedCosting.getInvAssemblyTransferLine().getAtlAssembleCost();

    					ADJ_DSCRPTN = invPropagatedCosting.getInvAssemblyTransferLine().getInvAssemblyTransfer().getAtrDescription();
    					ADJ_CRTD_BY = invPropagatedCosting.getInvAssemblyTransferLine().getInvAssemblyTransfer().getAtrPostedBy();
    					ADJ_RFRNC_NMBR = "INVAT" +
						invPropagatedCosting.getInvAssemblyTransferLine().getInvAssemblyTransfer().getAtrDocumentNumber();
    					Debug.print("ApReceivingItemEntryControllerBean regenerateCostVariance G");
    				} else if (invPropagatedCosting.getInvBranchStockTransferLine() != null){

    					if(invPropagatedCosting.getInvBranchStockTransferLine().getInvBranchStockTransfer().getBstTransferOutNumber()
    							!= null) {

    						TTL_CST = invPropagatedCosting.getInvBranchStockTransferLine().getBslAmount();
							QNTY =  this.convertByUomFromAndItemAndQuantity(
									invPropagatedCosting.getInvBranchStockTransferLine().getInvUnitOfMeasure(),
									invPropagatedCosting.getInvBranchStockTransferLine().getInvItemLocation().getInvItem(),
									invPropagatedCosting.getInvBranchStockTransferLine().getBslQuantityReceived(), AD_CMPNY);

    					} else {

    						TTL_CST = invPropagatedCosting.getInvBranchStockTransferLine().getBslAmount();
							QNTY =  this.convertByUomFromAndItemAndQuantity(
									invPropagatedCosting.getInvBranchStockTransferLine().getInvUnitOfMeasure(),
									invPropagatedCosting.getInvBranchStockTransferLine().getInvItemLocation().getInvItem(),
									invPropagatedCosting.getInvBranchStockTransferLine().getBslQuantity(), AD_CMPNY);
    					}

    					ADJ_DSCRPTN =
    						invPropagatedCosting.getInvBranchStockTransferLine().getInvBranchStockTransfer().getBstDescription();
    					ADJ_CRTD_BY =
    						invPropagatedCosting.getInvBranchStockTransferLine().getInvBranchStockTransfer().getBstPostedBy();
    					ADJ_RFRNC_NMBR = "INVBST" +
						invPropagatedCosting.getInvBranchStockTransferLine().getInvBranchStockTransfer().getBstNumber();

    					Debug.print("ApReceivingItemEntryControllerBean regenerateCostVariance H");

    				} else if (invPropagatedCosting.getInvBuildUnbuildAssemblyLine() != null){

   						TTL_CST = prevInvCosting.getCstRemainingValue() - invPropagatedCosting.getCstRemainingValue();
    					QNTY =  invPropagatedCosting.getInvBuildUnbuildAssemblyLine().getBlBuildQuantity();
    					ADJ_DSCRPTN =
    						invPropagatedCosting.getInvBuildUnbuildAssemblyLine().getInvBuildUnbuildAssembly().getBuaDescription();
    					ADJ_CRTD_BY =
    						invPropagatedCosting.getInvBuildUnbuildAssemblyLine().getInvBuildUnbuildAssembly().getBuaPostedBy();
    					ADJ_RFRNC_NMBR = "INVBUA" +
						invPropagatedCosting.getInvBuildUnbuildAssemblyLine().getInvBuildUnbuildAssembly().getBuaDocumentNumber();
    					Debug.print("ApReceivingItemEntryControllerBean regenerateCostVariance I");
    				} else if (invPropagatedCosting.getInvStockIssuanceLine()!= null){

    					TTL_CST = invPropagatedCosting.getInvStockIssuanceLine().getSilIssueCost();
						QNTY =  this.convertByUomFromAndItemAndQuantity(
								invPropagatedCosting.getInvStockIssuanceLine().getInvUnitOfMeasure(),
								invPropagatedCosting.getInvStockIssuanceLine().getInvItemLocation().getInvItem(),
								invPropagatedCosting.getInvStockIssuanceLine().getSilIssueQuantity(), AD_CMPNY);
    					ADJ_DSCRPTN = invPropagatedCosting.getInvStockIssuanceLine().getInvStockIssuance().getSiDescription();
    					ADJ_CRTD_BY = invPropagatedCosting.getInvStockIssuanceLine().getInvStockIssuance().getSiPostedBy();
    					ADJ_RFRNC_NMBR = "INVSI" +
						invPropagatedCosting.getInvStockIssuanceLine().getInvStockIssuance().getSiDocumentNumber();
    					Debug.print("ApReceivingItemEntryControllerBean regenerateCostVariance J");
    				} else if (invPropagatedCosting.getInvStockTransferLine()!= null) {

    					TTL_CST = invPropagatedCosting.getInvStockTransferLine().getStlAmount();
						QNTY =  this.convertByUomFromAndItemAndQuantity(
								invPropagatedCosting.getInvStockTransferLine().getInvUnitOfMeasure(),
								invPropagatedCosting.getInvStockTransferLine().getInvItem(),
								invPropagatedCosting.getInvStockTransferLine().getStlQuantityDelivered(), AD_CMPNY);
    					ADJ_DSCRPTN = invPropagatedCosting.getInvStockTransferLine().getInvStockTransfer().getStDescription();
    					ADJ_CRTD_BY = invPropagatedCosting.getInvStockTransferLine().getInvStockTransfer().getStPostedBy();
    					ADJ_RFRNC_NMBR = "INVST" +
						invPropagatedCosting.getInvStockTransferLine().getInvStockTransfer().getStDocumentNumber();
    					Debug.print("ApReceivingItemEntryControllerBean regenerateCostVariance K");
    				} else {

    					prevInvCosting = invPropagatedCosting;
    					continue;

    				}

    				// if quantity is equal 0, no variance.
    				if(QNTY == 0) continue;

    				// compute new cost variance
    				double UNT_CST = TTL_CST/QNTY;
    				double CST_VRNC_VL = (invPropagatedCosting.getCstRemainingQuantity() * UNT_CST -
    						invPropagatedCosting.getCstRemainingValue());
    				Debug.print("ApReceivingItemEntryControllerBean regenerateCostVariance L");
    				if(CST_VRNC_VL != 0)
    					this.generateCostVariance(invPropagatedCosting.getInvItemLocation(), CST_VRNC_VL, ADJ_RFRNC_NMBR,ADJ_DSCRPTN, invPropagatedCosting.getCstDate(), ADJ_CRTD_BY, AD_BRNCH, AD_CMPNY);

    				Debug.print("ApReceivingItemEntryControllerBean regenerateCostVariance M");
    			}

    			// set previous costing
    			prevInvCosting = invPropagatedCosting;

    		}

    	} catch (AdPRFCoaGlVarianceAccountNotFoundException ex){

    		throw ex;

    	} catch (Exception ex) {

    		Debug.printStackTrace(ex);
    		getSessionContext().setRollbackOnly();
    		throw new EJBException(ex.getMessage());

    	}      */
    }

    private void addInvDrEntry(short DR_LN, String DR_CLSS, byte DR_DBT, double DR_AMNT, byte DR_RVRSL, Integer COA_CODE,
    		LocalInvAdjustment invAdjustment, Integer AD_BRNCH, Integer AD_CMPNY)

    throws GlobalBranchAccountNumberInvalidException {

    	Debug.print("ApReceivingItemEntryControllerBean addInvDrEntry");

    	LocalAdCompanyHome adCompanyHome = null;
    	LocalInvDistributionRecordHome invDistributionRecordHome = null;
    	LocalGlChartOfAccountHome glChartOfAccountHome = null;

    	// Initialize EJB Home

    	try {

    		adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
    		invDistributionRecordHome = (LocalInvDistributionRecordHome)EJBHomeFactory.
			lookUpLocalHome(LocalInvDistributionRecordHome.JNDI_NAME, LocalInvDistributionRecordHome.class);
    		glChartOfAccountHome = (LocalGlChartOfAccountHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlChartOfAccountHome.JNDI_NAME, LocalGlChartOfAccountHome.class);


    	} catch (NamingException ex) {

    		throw new EJBException(ex.getMessage());

    	}

    	try {

    		// get company

    		LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);

    		// validate coa

    		LocalGlChartOfAccount glChartOfAccount = null;

    		try {

    			glChartOfAccount = glChartOfAccountHome.findByCoaCodeAndBranchCode(COA_CODE, AD_BRNCH, AD_CMPNY);

    		} catch(FinderException ex) {

    			throw new GlobalBranchAccountNumberInvalidException ();

    		}

    		// create distribution record

    		LocalInvDistributionRecord invDistributionRecord = invDistributionRecordHome.create(DR_LN, DR_CLSS, DR_DBT,
    				EJBCommon.roundIt(DR_AMNT, adCompany.getGlFunctionalCurrency().getFcPrecision()), DR_RVRSL, EJBCommon.FALSE,
					 AD_CMPNY);

    		//invAdjustment.addInvDistributionRecord(invDistributionRecord);
    		invDistributionRecord.setInvAdjustment(invAdjustment);

    		//glChartOfAccount.addInvDistributionRecord(invDistributionRecord);
    		invDistributionRecord.setInvChartOfAccount(glChartOfAccount);



    	} catch(GlobalBranchAccountNumberInvalidException ex) {

    		throw new GlobalBranchAccountNumberInvalidException ();

    	} catch (Exception ex) {

    		Debug.printStackTrace(ex);
    		getSessionContext().setRollbackOnly();
    		throw new EJBException(ex.getMessage());

    	}

    }

    private void executeInvAdjPost(Integer ADJ_CODE, String USR_NM, Integer AD_BRNCH, Integer AD_CMPNY) throws
	GlobalRecordAlreadyDeletedException,
	GlobalTransactionAlreadyPostedException,
	GlJREffectiveDateNoPeriodExistException,
	GlJREffectiveDatePeriodClosedException,
	GlobalJournalNotBalanceException,
	GlobalBranchAccountNumberInvalidException {

    	Debug.print("ApReceivingItemEntryControllerBean executeInvAdjPost");

    	LocalInvAdjustmentHome invAdjustmentHome = null;
    	LocalAdCompanyHome adCompanyHome = null;
    	LocalAdPreferenceHome adPreferenceHome = null;
    	LocalGlSetOfBookHome glSetOfBookHome = null;
    	LocalGlAccountingCalendarValueHome glAccountingCalendarValueHome = null;
    	LocalGlJournalHome glJournalHome = null;
    	LocalGlJournalBatchHome glJournalBatchHome = null;
    	LocalGlSuspenseAccountHome glSuspenseAccountHome = null;
    	LocalGlJournalLineHome glJournalLineHome = null;
    	LocalGlJournalSourceHome glJournalSourceHome = null;
    	LocalGlJournalCategoryHome glJournalCategoryHome = null;
    	LocalGlFunctionalCurrencyHome glFunctionalCurrencyHome = null;
    	LocalInvDistributionRecordHome invDistributionRecordHome = null;
    	LocalInvCostingHome invCostingHome = null;
    	LocalGlChartOfAccountHome glChartOfAccountHome = null;
    	LocalInvAdjustmentLineHome invAdjustmentLineHome = null;


    	// Initialize EJB Home

    	try {

    		invAdjustmentHome = (LocalInvAdjustmentHome)EJBHomeFactory.
			lookUpLocalHome(LocalInvAdjustmentHome.JNDI_NAME, LocalInvAdjustmentHome.class);
    		adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
    		adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);
    		glSetOfBookHome = (LocalGlSetOfBookHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlSetOfBookHome.JNDI_NAME, LocalGlSetOfBookHome.class);
    		glAccountingCalendarValueHome = (LocalGlAccountingCalendarValueHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlAccountingCalendarValueHome.JNDI_NAME, LocalGlAccountingCalendarValueHome.class);
    		glJournalHome = (LocalGlJournalHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlJournalHome.JNDI_NAME, LocalGlJournalHome.class);
    		glJournalBatchHome = (LocalGlJournalBatchHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlJournalBatchHome.JNDI_NAME, LocalGlJournalBatchHome.class);
    		glSuspenseAccountHome = (LocalGlSuspenseAccountHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlSuspenseAccountHome.JNDI_NAME, LocalGlSuspenseAccountHome.class);
    		glJournalLineHome = (LocalGlJournalLineHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlJournalLineHome.JNDI_NAME, LocalGlJournalLineHome.class);
    		glJournalSourceHome = (LocalGlJournalSourceHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlJournalSourceHome.JNDI_NAME, LocalGlJournalSourceHome.class);
    		glJournalCategoryHome = (LocalGlJournalCategoryHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlJournalCategoryHome.JNDI_NAME, LocalGlJournalCategoryHome.class);
    		glFunctionalCurrencyHome = (LocalGlFunctionalCurrencyHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlFunctionalCurrencyHome.JNDI_NAME, LocalGlFunctionalCurrencyHome.class);
    		invDistributionRecordHome = (LocalInvDistributionRecordHome)EJBHomeFactory.
			lookUpLocalHome(LocalInvDistributionRecordHome.JNDI_NAME, LocalInvDistributionRecordHome.class);
    		invCostingHome = (LocalInvCostingHome)EJBHomeFactory.
			lookUpLocalHome(LocalInvCostingHome.JNDI_NAME, LocalInvCostingHome.class);
    		glChartOfAccountHome = (LocalGlChartOfAccountHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlChartOfAccountHome.JNDI_NAME, LocalGlChartOfAccountHome.class);
    		invAdjustmentLineHome = (LocalInvAdjustmentLineHome)EJBHomeFactory.
			lookUpLocalHome(LocalInvAdjustmentLineHome.JNDI_NAME, LocalInvAdjustmentLineHome.class);


    	} catch (NamingException ex) {

    		throw new EJBException(ex.getMessage());

    	}

    	try {

    		// validate if adjustment is already deleted

    		LocalInvAdjustment invAdjustment = null;

    		try {

    			invAdjustment = invAdjustmentHome.findByPrimaryKey(ADJ_CODE);

    		} catch (FinderException ex) {

    			throw new GlobalRecordAlreadyDeletedException();

    		}

    		// validate if adjustment is already posted or void

    		if (invAdjustment.getAdjPosted() == EJBCommon.TRUE) {

    			if (invAdjustment.getAdjVoid() != EJBCommon.TRUE)
    			throw new GlobalTransactionAlreadyPostedException();

    		}


    		Collection invAdjustmentLines = null;

    		if(invAdjustment.getAdjVoid() == EJBCommon.FALSE)
    			invAdjustmentLines = invAdjustmentLineHome.findByAlVoidAndAdjCode(EJBCommon.FALSE, invAdjustment.getAdjCode(), AD_CMPNY);
    		else
    			invAdjustmentLines = invAdjustmentLineHome.findByAlVoidAndAdjCode(EJBCommon.TRUE, invAdjustment.getAdjCode(), AD_CMPNY);

    		Iterator i = invAdjustmentLines.iterator();

    		while(i.hasNext()) {


    			LocalInvAdjustmentLine invAdjustmentLine = (LocalInvAdjustmentLine) i.next();

    			LocalInvCosting invCosting =
    				invCostingHome.getByMaxCstDateToLongAndMaxCstLineNumberAndLessThanEqualCstDateAndIiNameAndLocName(
    						invAdjustment.getAdjDate(), invAdjustmentLine.getInvItemLocation().getInvItem().getIiName(),
    						invAdjustmentLine.getInvItemLocation().getInvLocation().getLocName(), AD_BRNCH, AD_CMPNY);

				this.postInvAdjustmentToInventory(invAdjustmentLine, invAdjustment.getAdjDate(), 0,
						invAdjustmentLine.getAlUnitCost(), invCosting.getCstRemainingQuantity(),
						invCosting.getCstRemainingValue() + invAdjustmentLine.getAlUnitCost(), AD_BRNCH, AD_CMPNY);

    		}

    		// post to gl if necessary

    		LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);
    		LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);

    			// validate if date has no period and period is closed

    			LocalGlSetOfBook glJournalSetOfBook = null;

    			try {

    				glJournalSetOfBook = glSetOfBookHome.findByDate(invAdjustment.getAdjDate(), AD_CMPNY);

    			} catch (FinderException ex) {

    				throw new GlJREffectiveDateNoPeriodExistException();

    			}

    			LocalGlAccountingCalendarValue glAccountingCalendarValue =
    				glAccountingCalendarValueHome.findByAcCodeAndDate(
    						glJournalSetOfBook.getGlAccountingCalendar().getAcCode(), invAdjustment.getAdjDate(), AD_CMPNY);


    			if (glAccountingCalendarValue.getAcvStatus() == 'N' ||
    					glAccountingCalendarValue.getAcvStatus() == 'C' ||
						glAccountingCalendarValue.getAcvStatus() == 'P') {

    				throw new GlJREffectiveDatePeriodClosedException();

    			}

    			// check if invoice is balance if not check suspense posting

    			LocalGlJournalLine glOffsetJournalLine = null;

    			Collection invDistributionRecords = null;

    			if (invAdjustment.getAdjVoid() == EJBCommon.FALSE) {

    				invDistributionRecords = invDistributionRecordHome.findImportableDrByDrReversedAndAdjCode(EJBCommon.FALSE,
    						invAdjustment.getAdjCode(), AD_CMPNY);

    			} else {

    				invDistributionRecords = invDistributionRecordHome.findImportableDrByDrReversedAndAdjCode(EJBCommon.TRUE,
    						invAdjustment.getAdjCode(), AD_CMPNY);

    			}


    			Iterator j = invDistributionRecords.iterator();

    			double TOTAL_DEBIT = 0d;
    			double TOTAL_CREDIT = 0d;

    			while (j.hasNext()) {

    				LocalInvDistributionRecord invDistributionRecord = (LocalInvDistributionRecord)j.next();

    				double DR_AMNT = 0d;

    				DR_AMNT = invDistributionRecord.getDrAmount();

    				if (invDistributionRecord.getDrDebit() == EJBCommon.TRUE) {

    					TOTAL_DEBIT += DR_AMNT;

    				} else {

    					TOTAL_CREDIT += DR_AMNT;

    				}

    			}

    			TOTAL_DEBIT = EJBCommon.roundIt(TOTAL_DEBIT, this.getGlFcPrecisionUnit(AD_CMPNY));
    			TOTAL_CREDIT = EJBCommon.roundIt(TOTAL_CREDIT, this.getGlFcPrecisionUnit(AD_CMPNY));

    			if (adPreference.getPrfAllowSuspensePosting() == EJBCommon.TRUE &&
    					TOTAL_DEBIT != TOTAL_CREDIT) {

    				LocalGlSuspenseAccount glSuspenseAccount = null;

    				try {

    					glSuspenseAccount = glSuspenseAccountHome.findByJsNameAndJcName("INVENTORY", "INVENTORY ADJUSTMENTS",
    							AD_CMPNY);

    				} catch (FinderException ex) {

    					throw new GlobalJournalNotBalanceException();

    				}

    				if (TOTAL_DEBIT - TOTAL_CREDIT < 0) {

    					glOffsetJournalLine = glJournalLineHome.create((short)(invDistributionRecords.size() + 1), EJBCommon.TRUE,
    							TOTAL_CREDIT - TOTAL_DEBIT, "", AD_CMPNY);

    				} else {

    					glOffsetJournalLine = glJournalLineHome.create((short)(invDistributionRecords.size() + 1), EJBCommon.FALSE,
    							TOTAL_DEBIT - TOTAL_CREDIT, "", AD_CMPNY);

    				}

    				LocalGlChartOfAccount glChartOfAccount = glSuspenseAccount.getGlChartOfAccount();
    				//glChartOfAccount.addGlJournalLine(glOffsetJournalLine);
    				glOffsetJournalLine.setGlChartOfAccount(glChartOfAccount);

    			} else if (adPreference.getPrfAllowSuspensePosting() == EJBCommon.FALSE &&
    					TOTAL_DEBIT != TOTAL_CREDIT) {

    				throw new GlobalJournalNotBalanceException();

    			}

    			// create journal batch if necessary

    			LocalGlJournalBatch glJournalBatch = null;
    			java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat("MM/dd/yyyy");

    			try {

    				glJournalBatch = glJournalBatchHome.findByJbName("JOURNAL IMPORT " + formatter.format(new Date()) +
    						" INVENTORY ADJUSTMENTS", AD_BRNCH, AD_CMPNY);

    			} catch (FinderException ex) {

    			}

    			if (glJournalBatch == null) {

    				glJournalBatch = glJournalBatchHome.create("JOURNAL IMPORT " + formatter.format(new Date()) +
    						" INVENTORY ADJUSTMENTS", "JOURNAL IMPORT", "CLOSED", EJBCommon.getGcCurrentDateWoTime().getTime(),
							USR_NM, AD_BRNCH, AD_CMPNY);

    			}

    			// create journal entry

    			LocalGlJournal glJournal = glJournalHome.create(invAdjustment.getAdjReferenceNumber(),
    					invAdjustment.getAdjDescription(), invAdjustment.getAdjDate(),
						0.0d, null, invAdjustment.getAdjDocumentNumber(), null, 1d, "N/A", null,
						'N', EJBCommon.TRUE, EJBCommon.FALSE,
						USR_NM, new Date(),
						USR_NM, new Date(),
						null, null,
						USR_NM, EJBCommon.getGcCurrentDateWoTime().getTime(),
						null, null, EJBCommon.FALSE, null,
						AD_BRNCH, AD_CMPNY);

    			LocalGlJournalSource glJournalSource = glJournalSourceHome.findByJsName("INVENTORY", AD_CMPNY);
    			//glJournalSource.addGlJournal(glJournal);
    			glJournal.setGlJournalSource(glJournalSource);

    			LocalGlFunctionalCurrency glFunctionalCurrency = glFunctionalCurrencyHome.findByFcName(
    					adCompany.getGlFunctionalCurrency().getFcName(), AD_CMPNY);
    			//glFunctionalCurrency.addGlJournal(glJournal);
    			glJournal.setGlFunctionalCurrency(glFunctionalCurrency);

    			LocalGlJournalCategory glJournalCategory = glJournalCategoryHome.findByJcName("INVENTORY ADJUSTMENTS", AD_CMPNY);
    			//glJournalCategory.addGlJournal(glJournal);
    			glJournal.setGlJournalCategory(glJournalCategory);

    			if (glJournalBatch != null) {

    				//glJournalBatch.addGlJournal(glJournal);
    				glJournal.setGlJournalBatch(glJournalBatch);
    			}

    			// create journal lines

    			j = invDistributionRecords.iterator();

    			while (j.hasNext()) {

    				LocalInvDistributionRecord invDistributionRecord = (LocalInvDistributionRecord)j.next();

    				double DR_AMNT = 0d;

    				DR_AMNT = invDistributionRecord.getDrAmount();

    				LocalGlJournalLine glJournalLine = glJournalLineHome.create(invDistributionRecord.getDrLine(),
    						invDistributionRecord.getDrDebit(), DR_AMNT, "", AD_CMPNY);

    				//invDistributionRecord.getInvChartOfAccount().addGlJournalLine(glJournalLine);
    				glJournalLine.setGlChartOfAccount(invDistributionRecord.getInvChartOfAccount());

    				//glJournal.addGlJournalLine(glJournalLine);
    				glJournalLine.setGlJournal(glJournal);

    				invDistributionRecord.setDrImported(EJBCommon.TRUE);


    			}

    			if (glOffsetJournalLine != null) {

    				//glJournal.addGlJournalLine(glOffsetJournalLine);
    				glOffsetJournalLine.setGlJournal(glJournal);
    			}

    			// post journal to gl

    			Collection glJournalLines = glJournal.getGlJournalLines();

    			i = glJournalLines.iterator();

    			while (i.hasNext()) {

    				LocalGlJournalLine glJournalLine = (LocalGlJournalLine)i.next();

    				// post current to current acv

    				this.postToGl(glAccountingCalendarValue,
    						glJournalLine.getGlChartOfAccount(),
							true, glJournalLine.getJlDebit(), glJournalLine.getJlAmount(), AD_CMPNY);


    				// post to subsequent acvs (propagate)

    				Collection glSubsequentAccountingCalendarValues =
    					glAccountingCalendarValueHome.findSubsequentAcvByAcCodeAndAcvPeriodNumber(
    							glJournalSetOfBook.getGlAccountingCalendar().getAcCode(),
								glAccountingCalendarValue.getAcvPeriodNumber(), AD_CMPNY);

    				Iterator acvsIter = glSubsequentAccountingCalendarValues.iterator();

    				while (acvsIter.hasNext()) {

    					LocalGlAccountingCalendarValue glSubsequentAccountingCalendarValue =
    						(LocalGlAccountingCalendarValue)acvsIter.next();

    					this.postToGl(glSubsequentAccountingCalendarValue,
    							glJournalLine.getGlChartOfAccount(),
								false, glJournalLine.getJlDebit(), glJournalLine.getJlAmount(), AD_CMPNY);

    				}

    				// post to subsequent years if necessary

    				Collection glSubsequentSetOfBooks = glSetOfBookHome.findSubsequentSobByAcYear(
    						glJournalSetOfBook.getGlAccountingCalendar().getAcYear(), AD_CMPNY);

    				if (!glSubsequentSetOfBooks.isEmpty() && glJournalSetOfBook.getSobYearEndClosed() == 1) {

    					adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
    					LocalGlChartOfAccount glRetainedEarningsAccount =
    						glChartOfAccountHome.findByCoaAccountNumberAndBranchCode(adCompany.getCmpRetainedEarnings(),
    								AD_BRNCH, AD_CMPNY);

    					Iterator sobIter = glSubsequentSetOfBooks.iterator();

    					while (sobIter.hasNext()) {

    						LocalGlSetOfBook glSubsequentSetOfBook = (LocalGlSetOfBook)sobIter.next();

    						String ACCOUNT_TYPE = glJournalLine.getGlChartOfAccount().getCoaAccountType();

    						// post to subsequent acvs of subsequent set of book(propagate)

    						Collection glAccountingCalendarValues =
    							glAccountingCalendarValueHome.findByAcCode(
    									glSubsequentSetOfBook.getGlAccountingCalendar().getAcCode(), AD_CMPNY);

    						Iterator acvIter = glAccountingCalendarValues.iterator();

    						while (acvIter.hasNext()) {

    							LocalGlAccountingCalendarValue glSubsequentAccountingCalendarValue =
    								(LocalGlAccountingCalendarValue)acvIter.next();

    							if (ACCOUNT_TYPE.equals("ASSET") || ACCOUNT_TYPE.equals("LIABILITY") ||
    									ACCOUNT_TYPE.equals("OWNERS EQUITY")) {

    								this.postToGl(glSubsequentAccountingCalendarValue,
    										glJournalLine.getGlChartOfAccount(),
											false, glJournalLine.getJlDebit(), glJournalLine.getJlAmount(), AD_CMPNY);

    							} else { // revenue & expense

    								this.postToGl(glSubsequentAccountingCalendarValue,
    										glRetainedEarningsAccount,
											false, glJournalLine.getJlDebit(), glJournalLine.getJlAmount(), AD_CMPNY);

    							}

    						}

    						if (glSubsequentSetOfBook.getSobYearEndClosed() == 0) break;

    					}

    				}

    			}

    		invAdjustment.setAdjPosted(EJBCommon.TRUE);

    	} catch (GlJREffectiveDateNoPeriodExistException ex) {

    		getSessionContext().setRollbackOnly();
    		throw ex;

    	} catch (GlJREffectiveDatePeriodClosedException ex) {

    		getSessionContext().setRollbackOnly();
    		throw ex;

    	} catch (GlobalJournalNotBalanceException ex) {

    		getSessionContext().setRollbackOnly();
    		throw ex;

    	} catch (GlobalRecordAlreadyDeletedException ex) {

    		getSessionContext().setRollbackOnly();
    		throw ex;

    	} catch (GlobalTransactionAlreadyPostedException ex) {

    		getSessionContext().setRollbackOnly();
    		throw ex;

    	} catch (Exception ex) {

    		Debug.printStackTrace(ex);
    		getSessionContext().setRollbackOnly();
    		throw new EJBException(ex.getMessage());

    	}

    }

    private LocalInvAdjustmentLine addInvAlEntry(LocalInvItemLocation invItemLocation, LocalInvAdjustment invAdjustment,
                    double CST_VRNC_VL, byte AL_VD, Integer AD_CMPNY) {

    	Debug.print("ArMiscReceiptEntryControllerBean addInvAlEntry");

    	LocalInvAdjustmentLineHome invAdjustmentLineHome = null;

    	// Initialize EJB Home

    	try {

    		invAdjustmentLineHome = (LocalInvAdjustmentLineHome)EJBHomeFactory.
			lookUpLocalHome(LocalInvAdjustmentLineHome.JNDI_NAME, LocalInvAdjustmentLineHome.class);

    	} catch (NamingException ex) {

    		throw new EJBException(ex.getMessage());

    	}

    	try {

    		// create dr entry
    		LocalInvAdjustmentLine invAdjustmentLine = null;
    		invAdjustmentLine = invAdjustmentLineHome.create(CST_VRNC_VL, null,null, 0,0, AL_VD, AD_CMPNY);

    		// map adjustment, unit of measure, item location
    		//invAdjustment.addInvAdjustmentLine(invAdjustmentLine);
    		  invAdjustmentLine.setInvAdjustment(invAdjustment);
    		//invItemLocation.getInvItem().getInvUnitOfMeasure().addInvAdjustmentLine(invAdjustmentLine);
    		invAdjustmentLine.setInvUnitOfMeasure(invItemLocation.getInvItem().getInvUnitOfMeasure());
    		//invItemLocation.addInvAdjustmentLine(invAdjustmentLine);
    		invAdjustmentLine.setInvItemLocation(invItemLocation);

    		return invAdjustmentLine;

    	} catch (Exception ex) {

    		Debug.printStackTrace(ex);
    		getSessionContext().setRollbackOnly();
    		throw new EJBException(ex.getMessage());

    	}

    }

    private LocalInvAdjustment saveInvAdjustment(String ADJ_RFRNC_NMBR, String ADJ_DSCRPTN,
    		Date ADJ_DATE, String USR_NM, Integer AD_BRNCH, Integer AD_CMPNY){

    	Debug.print("ApReceivingItemEntryControllerBean saveInvAdjustment");

    	LocalInvAdjustmentHome invAdjustmentHome = null;
    	LocalAdDocumentSequenceAssignmentHome adDocumentSequenceAssignmentHome = null;
    	LocalAdBranchDocumentSequenceAssignmentHome adBranchDocumentSequenceAssignmentHome = null;
    	// Initialize EJB Home

    	try{

    		invAdjustmentHome = (LocalInvAdjustmentHome)EJBHomeFactory.
			lookUpLocalHome(LocalInvAdjustmentHome.JNDI_NAME, LocalInvAdjustmentHome.class);
			adDocumentSequenceAssignmentHome = (LocalAdDocumentSequenceAssignmentHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdDocumentSequenceAssignmentHome.JNDI_NAME, LocalAdDocumentSequenceAssignmentHome.class);
			adBranchDocumentSequenceAssignmentHome = (LocalAdBranchDocumentSequenceAssignmentHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdBranchDocumentSequenceAssignmentHome.JNDI_NAME, LocalAdBranchDocumentSequenceAssignmentHome.class);

    	} catch (NamingException ex) {

    		throw new EJBException(ex.getMessage());

    	}

    	try{

    		// generate adj document number
    		String ADJ_DCMNT_NMBR = null;

    		LocalAdBranchDocumentSequenceAssignment adBranchDocumentSequenceAssignment = null;
    		LocalAdDocumentSequenceAssignment adDocumentSequenceAssignment = null;

    		try {

    			adDocumentSequenceAssignment = adDocumentSequenceAssignmentHome.findByDcName("INV ADJUSTMENT", AD_CMPNY);

    		} catch (FinderException ex) {

    		}

    		try {

    			adBranchDocumentSequenceAssignment = adBranchDocumentSequenceAssignmentHome.findBdsByDsaCodeAndBrCode(
    					adDocumentSequenceAssignment.getDsaCode(), AD_BRNCH, AD_CMPNY);

    		} catch (FinderException ex) {

    		}

    		while (true) {

    			if (adBranchDocumentSequenceAssignment == null || adBranchDocumentSequenceAssignment.getBdsNextSequence() == null) {

    				try {

    					invAdjustmentHome.findByAdjDocumentNumberAndBrCode(adDocumentSequenceAssignment.getDsaNextSequence(),
    							AD_BRNCH, AD_CMPNY);
    					adDocumentSequenceAssignment.setDsaNextSequence(EJBCommon.incrementStringNumber(
    							adDocumentSequenceAssignment.getDsaNextSequence()));

    				} catch (FinderException ex) {

    					ADJ_DCMNT_NMBR = adDocumentSequenceAssignment.getDsaNextSequence();
    					adDocumentSequenceAssignment.setDsaNextSequence(EJBCommon.incrementStringNumber(
    							adDocumentSequenceAssignment.getDsaNextSequence()));
    					break;

    				}

    			} else {

    				try {

    					invAdjustmentHome.findByAdjDocumentNumberAndBrCode(
    							adBranchDocumentSequenceAssignment.getBdsNextSequence(), AD_BRNCH, AD_CMPNY);
    					adBranchDocumentSequenceAssignment.setBdsNextSequence(EJBCommon.incrementStringNumber(
    							adBranchDocumentSequenceAssignment.getBdsNextSequence()));

    				} catch (FinderException ex) {

    					ADJ_DCMNT_NMBR = adBranchDocumentSequenceAssignment.getBdsNextSequence();
    					adBranchDocumentSequenceAssignment.setBdsNextSequence(EJBCommon.incrementStringNumber(
    							adBranchDocumentSequenceAssignment.getBdsNextSequence()));
    					break;

    				}

    			}

    		}

    		LocalInvAdjustment invAdjustment = invAdjustmentHome.create(ADJ_DCMNT_NMBR, ADJ_RFRNC_NMBR,
    				ADJ_DSCRPTN, ADJ_DATE, "COST-VARIANCE", "N/A", EJBCommon.FALSE, USR_NM, ADJ_DATE, USR_NM, ADJ_DATE, null, null,
					USR_NM, ADJ_DATE, null, null, EJBCommon.TRUE, EJBCommon.FALSE, AD_BRNCH, AD_CMPNY);

    		return invAdjustment;

    	} catch (Exception ex) {

    		Debug.printStackTrace(ex);
    		getSessionContext().setRollbackOnly();
    		throw new EJBException(ex.getMessage());

    	}


    }

    private void postInvAdjustmentToInventory(LocalInvAdjustmentLine invAdjustmentLine, Date CST_DT, double CST_ADJST_QTY,
    		double CST_ADJST_CST, double CST_RMNNG_QTY, double CST_RMNNG_VL, Integer AD_BRNCH,Integer AD_CMPNY) {

    	Debug.print("ApReceivingItemEntryControllerBean postInvAdjustmentToInventory");

    	LocalInvCostingHome invCostingHome = null;
    	LocalAdPreferenceHome adPreferenceHome = null;
    	LocalAdCompanyHome adCompanyHome = null;

    	// Initialize EJB Home

    	try {

    		invCostingHome = (LocalInvCostingHome)EJBHomeFactory.
			lookUpLocalHome(LocalInvCostingHome.JNDI_NAME, LocalInvCostingHome.class);
    		adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);
    		adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);

    	} catch (NamingException ex) {

    		getSessionContext().setRollbackOnly();
    		throw new EJBException(ex.getMessage());

    	}

    	try {

    		LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);
    		LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
    		LocalInvItemLocation invItemLocation = invAdjustmentLine.getInvItemLocation();
    		int CST_LN_NMBR = 0;

    		CST_ADJST_QTY = EJBCommon.roundIt(CST_ADJST_QTY, adPreference.getPrfInvQuantityPrecisionUnit());
    		CST_ADJST_CST = EJBCommon.roundIt(CST_ADJST_CST, adPreference.getPrfInvCostPrecisionUnit());
    		CST_RMNNG_QTY = EJBCommon.roundIt(CST_RMNNG_QTY, adPreference.getPrfInvQuantityPrecisionUnit());
    		CST_RMNNG_VL = EJBCommon.roundIt(CST_RMNNG_VL, adPreference.getPrfInvCostPrecisionUnit());

    		if (CST_ADJST_QTY < 0) {

    			invItemLocation.setIlCommittedQuantity(invItemLocation.getIlCommittedQuantity() - Math.abs(CST_ADJST_QTY));

    		}

    		// create costing

    		try {

    			// generate line number

    			LocalInvCosting invCurrentCosting = invCostingHome.getByMaxCstLineNumberAndCstDateToLongAndIiNameAndLocName(
    					CST_DT.getTime(), invItemLocation.getInvItem().getIiName(), invItemLocation.getInvLocation().getLocName(),
						AD_BRNCH, AD_CMPNY);
    			CST_LN_NMBR = invCurrentCosting.getCstLineNumber() + 1;

    		} catch (FinderException ex) {

    			CST_LN_NMBR = 1;

    		}

    		LocalInvCosting invCosting = invCostingHome.create(CST_DT, CST_DT.getTime(), CST_LN_NMBR, 0d, 0d, 0d, 0d,
    				CST_ADJST_QTY, CST_ADJST_CST, 0d, 0d, CST_RMNNG_QTY, CST_RMNNG_VL, 0d, 0d, CST_ADJST_QTY > 0 ? CST_ADJST_QTY : 0, AD_BRNCH, AD_CMPNY);
    		//invItemLocation.addInvCosting(invCosting);
    		invCosting.setInvItemLocation(invItemLocation);
    		invCosting.setInvAdjustmentLine(invAdjustmentLine);

    		// propagate balance if necessary

    		Collection invCostings = invCostingHome.findByGreaterThanCstDateAndIiNameAndLocName(CST_DT, invItemLocation.getInvItem().getIiName(), invItemLocation.getInvLocation().getLocName(), AD_BRNCH, AD_CMPNY);

    		Iterator i = invCostings.iterator();

    		while (i.hasNext()) {

    			LocalInvCosting invPropagatedCosting = (LocalInvCosting)i.next();

    			invPropagatedCosting.setCstRemainingQuantity(invPropagatedCosting.getCstRemainingQuantity() + CST_ADJST_QTY);
    			invPropagatedCosting.setCstRemainingValue(invPropagatedCosting.getCstRemainingValue() + CST_ADJST_CST);

    		}


    	} catch (Exception ex) {

    		Debug.printStackTrace(ex);
    		getSessionContext().setRollbackOnly();
    		throw new EJBException(ex.getMessage());

    	}



    }


    /**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public ArrayList getAdLvPurchaseRequisitionMisc1(Integer AD_CMPNY) {

		Debug.print("ApReceivingItemEntryControllerBean getAdLvPurchaseRequisitionMisc1");

		LocalAdLookUpValueHome adLookUpValueHome = null;

		ArrayList list = new ArrayList();

		// Initialize EJB Home

		try {

			adLookUpValueHome = (LocalAdLookUpValueHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdLookUpValueHome.JNDI_NAME, LocalAdLookUpValueHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}

		try {

			Collection adLookUpValues = adLookUpValueHome.findByLuName("AP PURCHASE REQUISITION MISC1", AD_CMPNY);

			Iterator i = adLookUpValues.iterator();

			while (i.hasNext()) {

				LocalAdLookUpValue adLookUpValue = (LocalAdLookUpValue)i.next();

				list.add(adLookUpValue.getLvName());

			}
			System.out.println("MISC1="+list.size());

			return list;

		} catch (Exception ex) {

			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());

		}

	}

	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public ArrayList getAdLvPurchaseRequisitionMisc2(Integer AD_CMPNY) {

		Debug.print("ApReceivingItemEntryControllerBean getAdLvPurchaseRequisitionMisc2");

		LocalAdLookUpValueHome adLookUpValueHome = null;

		ArrayList list = new ArrayList();

		// Initialize EJB Home

		try {

			adLookUpValueHome = (LocalAdLookUpValueHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdLookUpValueHome.JNDI_NAME, LocalAdLookUpValueHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}

		try {

			Collection adLookUpValues = adLookUpValueHome.findByLuName("AP PURCHASE REQUISITION MISC2", AD_CMPNY);

			Iterator i = adLookUpValues.iterator();

			while (i.hasNext()) {

				LocalAdLookUpValue adLookUpValue = (LocalAdLookUpValue)i.next();

				list.add(adLookUpValue.getLvName());

			}
			System.out.println("PR T7="+list.size());

			return list;

		} catch (Exception ex) {

			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());

		}

	}


	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public ArrayList getAdLvPurchaseRequisitionMisc3(Integer AD_CMPNY) {

		Debug.print("ApReceivingItemEntryControllerBean getAdLvPurchaseRequisitionMisc3");

		LocalAdLookUpValueHome adLookUpValueHome = null;

		ArrayList list = new ArrayList();

		// Initialize EJB Home

		try {

			adLookUpValueHome = (LocalAdLookUpValueHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdLookUpValueHome.JNDI_NAME, LocalAdLookUpValueHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}

		try {

			Collection adLookUpValues = adLookUpValueHome.findByLuName("AP PURCHASE REQUISITION MISC3", AD_CMPNY);

			Iterator i = adLookUpValues.iterator();

			while (i.hasNext()) {

				LocalAdLookUpValue adLookUpValue = (LocalAdLookUpValue)i.next();

				list.add(adLookUpValue.getLvName());

			}
			System.out.println("PR T9="+list.size());

			return list;

		} catch (Exception ex) {

			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());

		}

	}

	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public ArrayList getAdLvPurchaseRequisitionMisc4(Integer AD_CMPNY) {

		Debug.print("ApReceivingItemEntryControllerBean getAdLvPurchaseRequisitionMisc4");

		LocalAdLookUpValueHome adLookUpValueHome = null;

		ArrayList list = new ArrayList();

		// Initialize EJB Home

		try {

			adLookUpValueHome = (LocalAdLookUpValueHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdLookUpValueHome.JNDI_NAME, LocalAdLookUpValueHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}

		try {

			Collection adLookUpValues = adLookUpValueHome.findByLuName("AP PURCHASE REQUISITION MISC4", AD_CMPNY);

			Iterator i = adLookUpValues.iterator();

			while (i.hasNext()) {

				LocalAdLookUpValue adLookUpValue = (LocalAdLookUpValue)i.next();

				list.add(adLookUpValue.getLvName());

			}
			System.out.println("PR T9="+list.size());

			return list;

		} catch (Exception ex) {

			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());

		}

	}

	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public ArrayList getAdLvPurchaseRequisitionMisc5(Integer AD_CMPNY) {

		Debug.print("ApReceivingItemEntryControllerBean getAdLvPurchaseRequisitionMisc5");

		LocalAdLookUpValueHome adLookUpValueHome = null;

		ArrayList list = new ArrayList();

		// Initialize EJB Home

		try {

			adLookUpValueHome = (LocalAdLookUpValueHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdLookUpValueHome.JNDI_NAME, LocalAdLookUpValueHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}

		try {

			Collection adLookUpValues = adLookUpValueHome.findByLuName("AP PURCHASE REQUISITION MISC5", AD_CMPNY);

			Iterator i = adLookUpValues.iterator();

			while (i.hasNext()) {

				LocalAdLookUpValue adLookUpValue = (LocalAdLookUpValue)i.next();

				list.add(adLookUpValue.getLvName());

			}
			System.out.println("PR T9="+list.size());

			return list;

		} catch (Exception ex) {

			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());

		}

	}

	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public ArrayList getAdLvPurchaseRequisitionMisc6(Integer AD_CMPNY) {

		Debug.print("ApReceivingItemEntryControllerBean getAdLvPurchaseRequisitionMisc6");

		LocalAdLookUpValueHome adLookUpValueHome = null;

		ArrayList list = new ArrayList();

		// Initialize EJB Home

		try {

			adLookUpValueHome = (LocalAdLookUpValueHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdLookUpValueHome.JNDI_NAME, LocalAdLookUpValueHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}

		try {

			Collection adLookUpValues = adLookUpValueHome.findByLuName("AP PURCHASE REQUISITION MISC6", AD_CMPNY);

			Iterator i = adLookUpValues.iterator();

			while (i.hasNext()) {

				LocalAdLookUpValue adLookUpValue = (LocalAdLookUpValue)i.next();

				list.add(adLookUpValue.getLvName());

			}
			System.out.println("PR T9="+list.size());

			return list;

		} catch (Exception ex) {

			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());

		}

	}



    private void createInvTagList(LocalApPurchaseOrderLine apPurchaseOrderLine, ArrayList list, Integer AD_CMPNY) throws Exception {

    	Debug.print("ApReceivingItemEntryControllerBean createInvTagList");

    	LocalAdUserHome adUserHome = null;
    	LocalInvTagHome invTagHome = null;

    	// Initialize EJB Home

    	try{
    		adUserHome = (LocalAdUserHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdUserHome.JNDI_NAME, LocalAdUserHome.class);
    		invTagHome = (LocalInvTagHome)EJBHomeFactory.
			lookUpLocalHome(LocalInvTagHome.JNDI_NAME, LocalInvTagHome.class);

    	} catch (NamingException ex) {

    		throw new EJBException(ex.getMessage());

    	}



    	try {
			System.out.println("aabot?");
	  	    	//Iterator t = apPurchaseOrderLine.getInvTag().iterator();
  	  	    Iterator t = list.iterator();

  	  	    LocalInvTag invTag  = null;
  	  	    System.out.println("umabot?");
  	  	    while (t.hasNext()){
  	  	    	InvModTagListDetails tgLstDetails = (InvModTagListDetails)t.next();
  	  	    	System.out.println(tgLstDetails.getTgCustodian() + "<== custodian");
  	  	    	System.out.println(tgLstDetails.getTgSpecs() + "<== specs");
  	  	    	System.out.println(tgLstDetails.getTgPropertyCode() + "<== propertyCode");
  	  	    	System.out.println(tgLstDetails.getTgExpiryDate() + "<== expiryDate");
  	  	    	System.out.println(tgLstDetails.getTgSerialNumber() + "<== serial number");

  	  	    	if (tgLstDetails.getTgCode()==null){
  	  	    		System.out.println("ngcreate ba?");
      	  	    	invTag = invTagHome.create(tgLstDetails.getTgPropertyCode(),
      	  	    			tgLstDetails.getTgSerialNumber(),null,tgLstDetails.getTgExpiryDate(),
      	  	    			tgLstDetails.getTgSpecs(), AD_CMPNY, tgLstDetails.getTgTransactionDate(),
      	  	    			tgLstDetails.getTgType());

      	  	    	invTag.setApPurchaseOrderLine(apPurchaseOrderLine);
      	  	    	invTag.setInvItemLocation(apPurchaseOrderLine.getInvItemLocation());
      	  	    	LocalAdUser adUser = null;
      	  	    	try {
      	  	    		adUser = adUserHome.findByUsrName(tgLstDetails.getTgCustodian(), AD_CMPNY);
      	  	    	}catch(FinderException ex){

      	  	    	}
      	  	    	invTag.setAdUser(adUser);
      	  	    	System.out.println("ngcreate ba?");
  	  	    	}

  	  	    }




		}catch(Exception ex) {
			throw ex;
		}
    }


    private ArrayList getInvTagList(LocalApPurchaseOrderLine apPurchaseOrderLine) {

    	Debug.print("ApReceivingItemEntryControllerBean getInvTagList");
    	ArrayList list = new ArrayList();

    	Collection invTags = apPurchaseOrderLine.getInvTags();
		Iterator x = invTags.iterator();
		while (x.hasNext()) {
			LocalInvTag invTag = (LocalInvTag) x.next();
			InvModTagListDetails tgLstDetails = new InvModTagListDetails();
			tgLstDetails.setTgPropertyCode(invTag.getTgPropertyCode());
			tgLstDetails.setTgSpecs(invTag.getTgSpecs());
			tgLstDetails.setTgExpiryDate(invTag.getTgExpiryDate());
			tgLstDetails.setTgSerialNumber(invTag.getTgSerialNumber());
			try{

				tgLstDetails.setTgCustodian(invTag.getAdUser().getUsrName());
			}
			catch(Exception ex){
				tgLstDetails.setTgCustodian("");
			}

			list.add(tgLstDetails);

			System.out.println(tgLstDetails.getTgPropertyCode() + "<== property code inside controllerbean ");
			System.out.println(tgLstDetails.getTgSpecs() + "<== specs inside controllerbean ");
			System.out.println(list+ "<== taglist inside controllerbean ");

		}

    	return list;

    }

    
    private byte sendEmail(LocalAdApprovalQueue adApprovalQueue, Integer AD_CMPNY) throws GlobalSendEmailMessageException {

		Debug.print("ApReceivingItemEntryControllerBean sendEmail");


		LocalApPurchaseOrderHome apPurchaseOrderHome = null;

		LocalAdPreferenceHome adPreferenceHome = null;


		// Initialize EJB Home

		try {


			apPurchaseOrderHome = (LocalApPurchaseOrderHome)EJBHomeFactory.
			lookUpLocalHome(LocalApPurchaseOrderHome.JNDI_NAME, LocalApPurchaseOrderHome.class);

	
			
			adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
					lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}

		StringBuilder composedEmail = new StringBuilder();
		LocalApPurchaseOrder apPurchaseOrder = null;
		LocalAdPreference adPreference = null;
		try {

			
			adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);
			
			
			apPurchaseOrder = apPurchaseOrderHome.findByPrimaryKey(adApprovalQueue.getAqDocumentCode());

			HashMap hm = new HashMap();

			
			AdModApprovalQueueDetails adModApprovalQueueNewDetails = new AdModApprovalQueueDetails();
			adModApprovalQueueNewDetails.setAqSupplierCode(apPurchaseOrder.getApSupplier().getSplSupplierCode());
			adModApprovalQueueNewDetails.setAqSupplierName(apPurchaseOrder.getApSupplier().getSplName());
			adModApprovalQueueNewDetails.setAqAmount(apPurchaseOrder.getPoTotalAmount());

			hm.put(apPurchaseOrder.getApSupplier().getSplSupplierCode(), adModApprovalQueueNewDetails);

			Set set = hm.entrySet();

			composedEmail.append("<table border='1' style='width:100%'>");

			composedEmail.append("<tr>");
			composedEmail.append("<th> VENDOR </th>");
			composedEmail.append("<th> AMOUNT </th>");
			composedEmail.append("</tr>");


			Iterator x = set.iterator();

			while(x.hasNext()) {


				Map.Entry me = (Map.Entry)x.next();

				AdModApprovalQueueDetails adModApprovalQueueDetails = (AdModApprovalQueueDetails)
							me.getValue();

				composedEmail.append("<tr>");
				composedEmail.append("<td>");
				composedEmail.append(adModApprovalQueueDetails.getAqSupplierName());
				composedEmail.append("</td>");
				composedEmail.append("<td>");
				composedEmail.append(adModApprovalQueueDetails.getAqAmount());
				composedEmail.append("</td>");
				composedEmail.append("</tr>");

			}

			composedEmail.append("</table>");




		} catch (Exception ex) {

			Debug.printStackTrace(ex);
			throw new GlobalSendEmailMessageException(ex.getMessage());

		}


		String emailTo = adApprovalQueue.getAdUser().getUsrEmailAddress();
		Properties props = new Properties();
		/*
		 * GMAIL SETTINGS
		 * props.put("mail.smtp.host", "smtp.gmail.com");
		props.put("mail.smtp.socketFactory.port", "465");
		props.put("mail.smtp.socketFactory.class",
				"javax.net.ssl.SSLSocketFactory");
		props.put("mail.smtp.auth", "true");
		props.put("mail.smtp.port", "465");

		Session session = Session.getDefaultInstance(props,
		new javax.mail.Authenticator() {
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication("chrisr@daltron.net.pg","Sh0wc453");
			}
		});*/

	//	props.put("mail.smtp.host", "180.150.253.101");
	//	props.put("mail.smtp.socketFactory.port", "25");
	//	props.put("mail.smtp.auth", "false");
	//	props.put("mail.smtp.port", "25");
		
		//180.150.253.105
		//180.150.253.101
		
		
		props.put("mail.smtp.host", adPreference.getPrfMailHost());
	
		props.put("mail.smtp.socketFactory.port",adPreference.getPrfMailSocketFactoryPort());
		props.put("mail.smtp.auth", adPreference.getPrfMailAuthenticator()); //this will allow to not use username nad password
		props.put("mail.smtp.port", adPreference.getPrfMailPort());
	
	
		
		System.out.println("Mail host:" + props.get("mail.smtp.host"));
		System.out.println("socket factory:" + props.get("mail.smtp.socketFactory.port"));
		System.out.println("port:" + props.get("mail.smtp.port"));
		System.out.println("email from:" + adPreference.getPrfMailFrom());
		System.out.println("email to:" + emailTo);
		
		
		

		
		
		final String emailFrom = adPreference.getPrfMailFrom();
		final String emailPassword = adPreference.getPrfMailPassword();
		boolean isAuthenticator = adPreference.getPrfMailAuthenticator()==EJBCommon.TRUE?true:false;
		
		
		
		
		
		props.put("mail.smtp.socketFactory.class",
				"javax.net.ssl.SSLSocketFactory");
		
		
		
		Session session;

		
		if(isAuthenticator) {
			System.out.println("authentication entered");
			
			props.put("mail.smtp.starttls.enable", "true");
			
			props.put("mail.smtp.auth", "true");
			session = Session.getInstance(props,
			new javax.mail.Authenticator() {
				protected PasswordAuthentication getPasswordAuthentication() {
					return new PasswordAuthentication(emailFrom,emailPassword);
				}
			});
			
			
		}else {
			session = Session.getInstance(props, null);
		}
		

		try {

			Message message = new MimeMessage(session);
			message.setFrom(new InternetAddress(emailFrom));
			message.setRecipients(Message.RecipientType.TO,
					InternetAddress.parse(emailTo));


			message.setRecipients(Message.RecipientType.BCC,
					InternetAddress.parse(adPreference.getPrfMailBcc()));

			message.setSubject("DALTRON - OFS - RECEIVING ITEM RI #:"+apPurchaseOrder.getPoDocumentNumber());
			/*message.setText("Dear Mr/Mrs," +
					"\n\n No spam to my email, please!");*/

			/*message.setContent(
		              "<h1>This is actual message embedded in HTML tags</h1>",
		             "text/html");*/
			System.out.println("adApprovalQueue.getAqRequesterName()="+adApprovalQueue.getAqRequesterName());
			System.out.println("adApprovalQueue.getAqDocumentNumber()="+adApprovalQueue.getAqDocumentNumber());
			System.out.println("adApprovalQueue.getAdUser().getUsrDescription()="+adApprovalQueue.getAdUser().getUsrDescription());

			message.setContent(
		              "Dear "+adApprovalQueue.getAdUser().getUsrDescription()+",<br><br>"+
		              "A purchase request was raised by "+adApprovalQueue.getAqRequesterName()+" for your approval.<br>"+
		              "RI Number: "+adApprovalQueue.getAqDocumentNumber()+".<br>"+
		              "Date: "+EJBCommon.convertSQLDateToString(apPurchaseOrder.getPoDate())+".<br>"+
		              "Description: "+apPurchaseOrder.getPoDescription()+".<br>"+
		              composedEmail.toString() +

		              "Please click the link <a href=\"http://live.daltronpng.com\">http://live.daltronpng.com:8080/daltron</a>.<br><br><br>"+

		              "This is an automated message and was sent from a notification-only email address.<br><br>"+
		              "Please do not reply to this message.<br><br>",
		             "text/html");


			Transport.send(message);
			adApprovalQueue.setAqEmailSent(EJBCommon.TRUE);


			System.out.println("Done");

		} catch (MessagingException e) {
			Debug.printStackTrace(e);
			throw new GlobalSendEmailMessageException(e.getMessage());
		}

		return 0;
	}
    

	// SessionBean methods

    /**
     * @ejb:create-method view-type="remote"
     **/
    public void ejbCreate() throws CreateException {

       Debug.print("ApReceivingItemEntryControllerBean ejbCreate");

    }

}