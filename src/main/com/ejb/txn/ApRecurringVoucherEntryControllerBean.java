
/*
 * ApRecurringVoucherControllerBean.java
 *
 * Created on February 18, 2004, 9:31 AM
 *
 * @author  Neil Andrew M. Ajero
 */

package com.ejb.txn;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;
import javax.naming.NamingException;

import com.ejb.ad.LocalAdCompany;
import com.ejb.ad.LocalAdCompanyHome;
import com.ejb.ad.LocalAdPaymentTerm;
import com.ejb.ad.LocalAdPaymentTermHome;
import com.ejb.ad.LocalAdPreference;
import com.ejb.ad.LocalAdPreferenceHome;
import com.ejb.ad.LocalAdUser;
import com.ejb.ad.LocalAdUserHome;
import com.ejb.ap.LocalApDistributionRecord;
import com.ejb.ap.LocalApDistributionRecordHome;
import com.ejb.ap.LocalApRecurringVoucher;
import com.ejb.ap.LocalApRecurringVoucherHome;
import com.ejb.ap.LocalApSupplier;
import com.ejb.ap.LocalApSupplierHome;
import com.ejb.ap.LocalApTaxCode;
import com.ejb.ap.LocalApTaxCodeHome;
import com.ejb.ap.LocalApVoucherBatch;
import com.ejb.ap.LocalApVoucherBatchHome;
import com.ejb.ap.LocalApWithholdingTaxCode;
import com.ejb.ap.LocalApWithholdingTaxCodeHome;
import com.ejb.exception.GlobalBranchAccountNumberInvalidException;
import com.ejb.exception.GlobalConversionDateNotExistException;
import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.exception.GlobalPaymentTermInvalidException;
import com.ejb.exception.GlobalRecordAlreadyDeletedException;
import com.ejb.exception.GlobalRecordAlreadyExistException;
import com.ejb.exception.GlobalRecordInvalidException;
import com.ejb.genfld.LocalGenSegmentHome;
import com.ejb.genfld.LocalGenValueSetValueHome;
import com.ejb.gl.LocalGlChartOfAccount;
import com.ejb.gl.LocalGlChartOfAccountHome;
import com.ejb.gl.LocalGlFunctionalCurrency;
import com.ejb.gl.LocalGlFunctionalCurrencyHome;
import com.ejb.gl.LocalGlFunctionalCurrencyRate;
import com.ejb.gl.LocalGlFunctionalCurrencyRateHome;
import com.util.AbstractSessionBean;
import com.util.ApModDistributionRecordDetails;
import com.util.ApModRecurringVoucherDetails;
import com.util.ApModSupplierDetails;
import com.util.Debug;
import com.util.EJBCommon;
import com.util.EJBHomeFactory;
import com.util.GlModFunctionalCurrencyDetails;

/**
 * @ejb:bean name="ApRecurringVoucherEntryControllerEJB"
 *           display-name="used for entering recurring vouchers"
 *           type="Stateless"
 *           view-type="remote"
 *           jndi-name="ejb/ApRecurringVoucherEntryControllerEJB"
 *
 * @ejb:interface remote-class="com.ejb.txn.ApRecurringVoucherEntryController"
 *                extends="javax.ejb.EJBObject"
 *
 * @ejb:home remote-class="com.ejb.txn.ApRecurringVoucherEntryControllerHome"
 *           extends="javax.ejb.EJBHome"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="apuser"
 *                        role-link="apuserlink"
 *
 * @ejb:permission role-name="apuser"
 * 
*/

public class ApRecurringVoucherEntryControllerBean extends AbstractSessionBean {

    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList getGlFcAllWithDefault(Integer AD_CMPNY) {
                    
        Debug.print("ApRecurringVoucherEntryControllerBean getGlFcAllWithDefault");
        
        LocalGlFunctionalCurrencyHome glFunctionalCurrencyHome = null;
        LocalAdCompanyHome adCompanyHome = null;
        
        Collection glFunctionalCurrencies = null;
        
        LocalGlFunctionalCurrency glFunctionalCurrency = null;
        LocalAdCompany adCompany = null;
        
        
        ArrayList list = new ArrayList();
        
        // Initialize EJB Home
        
        try {
            
            glFunctionalCurrencyHome = (LocalGlFunctionalCurrencyHome)EJBHomeFactory.
                lookUpLocalHome(LocalGlFunctionalCurrencyHome.JNDI_NAME, LocalGlFunctionalCurrencyHome.class);
            adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
            
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {
            
            adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
	               
            glFunctionalCurrencies = glFunctionalCurrencyHome.findFcAllEnabled(
            	EJBCommon.getGcCurrentDateWoTime().getTime(), AD_CMPNY);
            
        } catch (Exception ex) {
            
            throw new EJBException(ex.getMessage());
        }
        
        if (glFunctionalCurrencies.isEmpty()) {
        	
        	return null;
        	
        }
        
        Iterator i = glFunctionalCurrencies.iterator();
               
        while (i.hasNext()) {
        	
        	glFunctionalCurrency = (LocalGlFunctionalCurrency)i.next();
        	
        	GlModFunctionalCurrencyDetails mdetails = new GlModFunctionalCurrencyDetails(
        	    glFunctionalCurrency.getFcCode(), glFunctionalCurrency.getFcName(),
        	    adCompany.getGlFunctionalCurrency().getFcName().equals(glFunctionalCurrency.getFcName()) ?
        	       EJBCommon.TRUE : EJBCommon.FALSE);
        	
        	list.add(mdetails);
        	
        }
        
        return list;
            
    }
    
    
    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList getAdPytAll(Integer AD_CMPNY) {
                    
        Debug.print("ApRecurringVoucherEntryControllerBean getAdPytAll");
        
        LocalAdPaymentTermHome adPaymentTermHome = null;               
        
        ArrayList list = new ArrayList();
        
        // Initialize EJB Home
        
        try {
            
            adPaymentTermHome = (LocalAdPaymentTermHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdPaymentTermHome.JNDI_NAME, LocalAdPaymentTermHome.class);
            
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }        
        
        try {
        	
        	Collection adPaymentTerms = adPaymentTermHome.findEnabledPytAll(AD_CMPNY);
        	
        	Iterator i = adPaymentTerms.iterator();
        	
        	while (i.hasNext()) {
        		
        		LocalAdPaymentTerm adPaymentTerm = (LocalAdPaymentTerm)i.next();
        		
        		list.add(adPaymentTerm.getPytName());
        		
        	}
        	
        	return list;
        	
        } catch (Exception ex) {
        	
        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());
        	
        }
            
    }
    
    
    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList getApTcAll(Integer AD_CMPNY) {
                    
        Debug.print("ApRecurringVoucherEntryControllerBean getApTcAll");
        
        LocalApTaxCodeHome apTaxCodeHome = null;               
        
        ArrayList list = new ArrayList();
        
        // Initialize EJB Home
        
        try {
            
            apTaxCodeHome = (LocalApTaxCodeHome)EJBHomeFactory.
                lookUpLocalHome(LocalApTaxCodeHome.JNDI_NAME, LocalApTaxCodeHome.class);
            
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }        
        
        try {
        	
        	Collection apTaxCodes = apTaxCodeHome.findEnabledTcAll(AD_CMPNY);
        	
        	Iterator i = apTaxCodes.iterator();
        	
        	while (i.hasNext()) {
        		
        		LocalApTaxCode apTaxCode = (LocalApTaxCode)i.next();
        		
        		list.add(apTaxCode.getTcName());
        		
        	}
        	
        	return list;
        	
        } catch (Exception ex) {
        	
        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());
        	
        }
            
    }

    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList getAdUsrAll(Integer AD_CMPNY) {
                    
        Debug.print("ApRecurringVoucherEntryControllerBean getAdUsrAll");
        
        LocalAdUserHome adUserHome = null;               
        
        ArrayList list = new ArrayList();
        
        // Initialize EJB Home
        
        try {
            
            adUserHome = (LocalAdUserHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdUserHome.JNDI_NAME, LocalAdUserHome.class);
            
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }        
        
        try {
        	
        	Collection adUsers = adUserHome.findUsrAll(AD_CMPNY);
        	
        	Iterator i = adUsers.iterator();
        	
        	while (i.hasNext()) {
        		
        		LocalAdUser adUser = (LocalAdUser)i.next();
        		
        		list.add(adUser.getUsrName());
        		
        	}
        	
        	return list;
        	
        } catch (Exception ex) {
        	
        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());
        	
        }
            
    }
    
    
    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList getApWtcAll(Integer AD_CMPNY) {
                    
        Debug.print("ApRecurringVoucherEntryControllerBean getApWtcAll");
        
        LocalApWithholdingTaxCodeHome apWithholdingTaxCodeHome = null;               
        
        ArrayList list = new ArrayList();
        
        // Initialize EJB Home
        
        try {
            
            apWithholdingTaxCodeHome = (LocalApWithholdingTaxCodeHome)EJBHomeFactory.
                lookUpLocalHome(LocalApWithholdingTaxCodeHome.JNDI_NAME, LocalApWithholdingTaxCodeHome.class);
            
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }        
        
        try {
        	
        	Collection apWithholdingTaxCodes = apWithholdingTaxCodeHome.findEnabledWtcAll(AD_CMPNY);
        	
        	Iterator i = apWithholdingTaxCodes.iterator();
        	
        	while (i.hasNext()) {
        		
        		LocalApWithholdingTaxCode apWithholdingTaxCode = (LocalApWithholdingTaxCode)i.next();
        		
        		list.add(apWithholdingTaxCode.getWtcName());
        		
        	}
        	
        	return list;
        	
        } catch (Exception ex) {
        	
        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());
        	
        }
            
    }        
    
    
    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList getApSplAll(Integer AD_BRNCH, Integer AD_CMPNY) {
                    
        Debug.print("ApRecurringVoucherEntryControllerBean getApSplAll");
        
        LocalApSupplierHome apSupplierHome = null;               
        
        ArrayList list = new ArrayList();
        
        // Initialize EJB Home
        
        try {
            
            apSupplierHome = (LocalApSupplierHome)EJBHomeFactory.
                lookUpLocalHome(LocalApSupplierHome.JNDI_NAME, LocalApSupplierHome.class);
            
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }        
        
        try {
        	
        	Collection apSuppliers = apSupplierHome.findEnabledSplAll(AD_BRNCH, AD_CMPNY);
        	
        	Iterator i = apSuppliers.iterator();
        	
        	while (i.hasNext()) {
        		
        		LocalApSupplier apSupplier = (LocalApSupplier)i.next();
        		
        		list.add(apSupplier.getSplSupplierCode());
        		
        	}
        	
        	return list;
        	
        } catch (Exception ex) {
        	
        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());
        	
        }
            
    }      
    
    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList getApOpenVbAll(Integer AD_BRNCH, Integer AD_CMPNY) {
                    
        Debug.print("ApRecurringVoucherEntryControllerBean getApOpenVbAll");
        
        LocalApVoucherBatchHome apVoucherBatchHome = null;               
        
        ArrayList list = new ArrayList();
        
        // Initialize EJB Home
        
        try {
            
        	apVoucherBatchHome = (LocalApVoucherBatchHome)EJBHomeFactory.
                lookUpLocalHome(LocalApVoucherBatchHome.JNDI_NAME, LocalApVoucherBatchHome.class);
            
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }        
        
        try {
        	
        	Collection apVoucherBatches = apVoucherBatchHome.findOpenVbByVbType("VOUCHER", AD_BRNCH, AD_CMPNY);
        	
        	Iterator i = apVoucherBatches.iterator();
        	
        	while (i.hasNext()) {
        		
        		LocalApVoucherBatch apVoucherBatch = (LocalApVoucherBatch)i.next();
        		
        		list.add(apVoucherBatch.getVbName());
        		
        	}
        	
        	return list;
        	
        } catch (Exception ex) {
        	
        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());
        	
        }
            
    }
        
    
   /**
    * @ejb:interface-method view-type="remote"
    * @jboss:method-attributes read-only="true"
    **/
    public short getAdPrfApJournalLineNumber(Integer AD_CMPNY) {

       Debug.print("ApDebitMemoEntryControllerBean getAdPrfApJournalLineNumber");
                  
       LocalAdPreferenceHome adPreferenceHome = null;
      
      
       // Initialize EJB Home
        
       try {
            
          adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
              lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);       
            
       } catch (NamingException ex) {
            
          throw new EJBException(ex.getMessage());
            
       }
      

       try {
      	
          LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);
         
          return adPreference.getPrfApJournalLineNumber();
         
       } catch (Exception ex) {
        	 
          Debug.printStackTrace(ex);
          throw new EJBException(ex.getMessage());
         
       }
      
    }
    
    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
     public byte getAdPrfEnableApVoucherBatch(Integer AD_CMPNY) {

        Debug.print("ApDebitMemoEntryControllerBean getAdPrfApEnableApVoucherBatch");
                   
        LocalAdPreferenceHome adPreferenceHome = null;
       
       
        // Initialize EJB Home
         
        try {
             
           adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
               lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);       
             
        } catch (NamingException ex) {
             
           throw new EJBException(ex.getMessage());
             
        }
       

        try {
       	
           LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);
          
           return adPreference.getPrfEnableApVoucherBatch();
          
        } catch (Exception ex) {
         	 
           Debug.printStackTrace(ex);
           throw new EJBException(ex.getMessage());
          
        }
       
     }    
    
    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ApModRecurringVoucherDetails getApRvByRvCode(Integer RV_CODE, Integer AD_CMPNY) 
        throws GlobalNoRecordFoundException {
                    
        Debug.print("ApRecurringVoucherEntryControllerBean getApRvByRvCode");
        
        LocalApRecurringVoucherHome apRecurringVoucherHome = null;        
        LocalAdCompanyHome adCompanyHome = null;
        LocalGenValueSetValueHome genValueSetValueHome = null;
        LocalGenSegmentHome genSegmentHome = null;
        LocalAdUserHome adUserHome = null;
        LocalApDistributionRecordHome apDistributionRecordHome = null;
                
        // Initialize EJB Home
        
        try {
            
            apRecurringVoucherHome = (LocalApRecurringVoucherHome)EJBHomeFactory.
                lookUpLocalHome(LocalApRecurringVoucherHome.JNDI_NAME, LocalApRecurringVoucherHome.class);
            adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
            genValueSetValueHome = (LocalGenValueSetValueHome)EJBHomeFactory.
                lookUpLocalHome(LocalGenValueSetValueHome.JNDI_NAME, LocalGenValueSetValueHome.class);
            genSegmentHome = (LocalGenSegmentHome)EJBHomeFactory.
                lookUpLocalHome(LocalGenSegmentHome.JNDI_NAME, LocalGenSegmentHome.class);
            adUserHome = (LocalAdUserHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdUserHome.JNDI_NAME, LocalAdUserHome.class);
            apDistributionRecordHome = (LocalApDistributionRecordHome)EJBHomeFactory.
            	lookUpLocalHome(LocalApDistributionRecordHome.JNDI_NAME, LocalApDistributionRecordHome.class);
            
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }        
        
        try {
        	
        	LocalApRecurringVoucher apRecurringVoucher = null;
        	
        	
        	try {
        		
        		apRecurringVoucher = apRecurringVoucherHome.findByPrimaryKey(RV_CODE);
        		
        	} catch (FinderException ex) {
        		
        		throw new GlobalNoRecordFoundException();
        		
        	}
        	
        	ArrayList rvDrList = new ArrayList();
        	
        	// get distribution records
        	
        	Collection apDistributionRecords = apDistributionRecordHome.findByRvCode(apRecurringVoucher.getRvCode(), AD_CMPNY);     	            
        	
        	short lineNumber = 1;
        	
        	double TOTAL_DEBIT = 0d;
        	double TOTAL_CREDIT = 0d;
        	
        	Iterator i = apDistributionRecords.iterator();
        	
        	while (i.hasNext()) {
        		
        		LocalApDistributionRecord apDistributionRecord = (LocalApDistributionRecord)i.next();
        		
        		ApModDistributionRecordDetails mdetails = new ApModDistributionRecordDetails();
        		
        		mdetails.setDrCode(apDistributionRecord.getDrCode());
        		mdetails.setDrLine(lineNumber);
        		mdetails.setDrClass(apDistributionRecord.getDrClass());
        		mdetails.setDrDebit(apDistributionRecord.getDrDebit());
        		mdetails.setDrAmount(apDistributionRecord.getDrAmount());
        		mdetails.setDrCoaAccountNumber(apDistributionRecord.getGlChartOfAccount().getCoaAccountNumber());       		
			    mdetails.setDrCoaAccountDescription(apDistributionRecord.getGlChartOfAccount().getCoaAccountDescription());			    
			    
			    if(apDistributionRecord.getDrDebit() == EJBCommon.TRUE) {
			    	
			    	TOTAL_DEBIT += apDistributionRecord.getDrAmount();
			    	
			    } else {
			    	
			    	TOTAL_CREDIT += apDistributionRecord.getDrAmount();
			    	
			    }
			    
        		rvDrList.add(mdetails);
        		
        		lineNumber++;
        		
        	}
        	
        	ApModRecurringVoucherDetails mRvDetails = new ApModRecurringVoucherDetails();
        	
        	mRvDetails.setRvCode(apRecurringVoucher.getRvCode());
        	mRvDetails.setRvName(apRecurringVoucher.getRvName());
        	mRvDetails.setRvDescription(apRecurringVoucher.getRvDescription());
        	mRvDetails.setRvConversionDate(apRecurringVoucher.getRvConversionDate());
        	mRvDetails.setRvConversionRate(apRecurringVoucher.getRvConversionRate());
        	mRvDetails.setRvAmount(apRecurringVoucher.getRvAmount());
        	mRvDetails.setRvAmountDue(apRecurringVoucher.getRvAmountDue());
        	mRvDetails.setRvTotalDebit(TOTAL_DEBIT);
        	mRvDetails.setRvTotalCredit(TOTAL_CREDIT);
        	mRvDetails.setRvSchedule(apRecurringVoucher.getRvSchedule());
        	mRvDetails.setRvNextRunDate(apRecurringVoucher.getRvNextRunDate());
        	mRvDetails.setRvLastRunDate(apRecurringVoucher.getRvLastRunDate());
        	
        	
        	if (apRecurringVoucher.getRvAdUserName1() != null) {
        		
        		LocalAdUser userName1 = adUserHome.findByPrimaryKey(apRecurringVoucher.getRvAdUserName1());
                mRvDetails.setRvAdNotifiedUser1(userName1.getUsrName());
                
        	} 

        	if (apRecurringVoucher.getRvAdUserName2() != null) {
        		
        		LocalAdUser userName2 = adUserHome.findByPrimaryKey(apRecurringVoucher.getRvAdUserName2());
                mRvDetails.setRvAdNotifiedUser2(userName2.getUsrName());
                
        	} 
        	
        	if (apRecurringVoucher.getRvAdUserName3() != null) {
        		
        		LocalAdUser userName3 = adUserHome.findByPrimaryKey(apRecurringVoucher.getRvAdUserName3());
                mRvDetails.setRvAdNotifiedUser3(userName3.getUsrName());
                
        	} 
        	
        	if (apRecurringVoucher.getRvAdUserName4() != null) {
        		
        		LocalAdUser userName4 = adUserHome.findByPrimaryKey(apRecurringVoucher.getRvAdUserName4());
                mRvDetails.setRvAdNotifiedUser4(userName4.getUsrName());
                
        	} 
        	
        	if (apRecurringVoucher.getRvAdUserName5() != null) {
        		
        		LocalAdUser userName5 = adUserHome.findByPrimaryKey(apRecurringVoucher.getRvAdUserName5());
                mRvDetails.setRvAdNotifiedUser5(userName5.getUsrName());
                
        	}         	        	        	
        	  	
        	mRvDetails.setRvTcName(apRecurringVoucher.getApTaxCode().getTcName());
        	mRvDetails.setRvPytName(apRecurringVoucher.getAdPaymentTerm().getPytName());
        	mRvDetails.setRvSplSupplierCode(apRecurringVoucher.getApSupplier().getSplSupplierCode());
        	mRvDetails.setRvWtcName(apRecurringVoucher.getApWithholdingTaxCode().getWtcName());
        	mRvDetails.setRvFcName(apRecurringVoucher.getGlFunctionalCurrency().getFcName());  	
        	mRvDetails.setRvDrList(rvDrList);
        	mRvDetails.setRvVbName(apRecurringVoucher.getApVoucherBatch() != null ? apRecurringVoucher.getApVoucherBatch().getVbName() : null);
        	
        	return mRvDetails;
        	
        } catch (GlobalNoRecordFoundException ex) {
        	
        	throw ex;
        	        	
        } catch (Exception ex) {
        	
        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());
        	
        }
            
    }
    
    
    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ApModSupplierDetails getApSplBySplSupplierCode(String SPL_SPPLR_CODE, Integer AD_CMPNY) 
        throws GlobalNoRecordFoundException {
                    
        Debug.print("ApRecurringVoucherEntryControllerBean getApSplBySplSupplierCode");
        
        LocalApSupplierHome apSupplierHome = null;        
                        
        // Initialize EJB Home
        
        try {
            
            apSupplierHome = (LocalApSupplierHome)EJBHomeFactory.
                lookUpLocalHome(LocalApSupplierHome.JNDI_NAME, LocalApSupplierHome.class);
            
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }        
        
        try {
        	
        	LocalApSupplier apSupplier = null;
        	
        	
        	try {
        		
        		apSupplier = apSupplierHome.findBySplSupplierCode(SPL_SPPLR_CODE, AD_CMPNY);
        		
        	} catch (FinderException ex) {
        		
        		throw new GlobalNoRecordFoundException();
        		
        	}
        	
        	ApModSupplierDetails mdetails = new ApModSupplierDetails();
        	
        	mdetails.setSplPytName(apSupplier.getAdPaymentTerm() != null ?
        	    apSupplier.getAdPaymentTerm().getPytName() : null);
        	mdetails.setSplScTcName(apSupplier.getApSupplierClass().getApTaxCode() != null ?
        	    apSupplier.getApSupplierClass().getApTaxCode().getTcName() : null);
        	mdetails.setSplScWtcName(apSupplier.getApSupplierClass().getApWithholdingTaxCode() != null ?
        	    apSupplier.getApSupplierClass().getApWithholdingTaxCode().getWtcName() : null);
        	
        	return mdetails;
        	
        } catch (GlobalNoRecordFoundException ex) {
        	
        	throw ex;
        	        	
        } catch (Exception ex) {
        	
        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());
        	
        }
            
    }
    
    
    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList getApDrBySplSupplierCodeAndTcNameAndWtcNameAndRvAmount(String SPL_SPPLR_CODE, String TC_NM, 
        String WTC_NM, double RV_AMNT, Integer AD_CMPNY) 
        throws GlobalNoRecordFoundException {
                    
        Debug.print("ApRecurringVoucherEntryControllerBean getApDrBySplSupplierCodeAndTcNameAndWtcNameAndRvAmount");
        
        LocalApSupplierHome apSupplierHome = null;     
        LocalApTaxCodeHome apTaxCodeHome = null;
        LocalApWithholdingTaxCodeHome apWithholdingTaxCodeHome = null;
        LocalGlChartOfAccountHome glChartOfAccountHome = null;
        LocalAdPreferenceHome adPreferenceHome = null;
        
        ArrayList list = new ArrayList();
           
                        
        // Initialize EJB Home
        
        try {
            
            apSupplierHome = (LocalApSupplierHome)EJBHomeFactory.
                lookUpLocalHome(LocalApSupplierHome.JNDI_NAME, LocalApSupplierHome.class);
            apTaxCodeHome = (LocalApTaxCodeHome)EJBHomeFactory.
                lookUpLocalHome(LocalApTaxCodeHome.JNDI_NAME, LocalApTaxCodeHome.class);
            apWithholdingTaxCodeHome = (LocalApWithholdingTaxCodeHome)EJBHomeFactory.
                lookUpLocalHome(LocalApWithholdingTaxCodeHome.JNDI_NAME, LocalApWithholdingTaxCodeHome.class);
            glChartOfAccountHome = (LocalGlChartOfAccountHome)EJBHomeFactory.
                lookUpLocalHome(LocalGlChartOfAccountHome.JNDI_NAME, LocalGlChartOfAccountHome.class);
            adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);
            
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }        
        
        try {
        	
        	LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);
        	
        	LocalApSupplier apSupplier = null;
        	LocalApTaxCode apTaxCode = null;
        	LocalApWithholdingTaxCode apWithholdingTaxCode = null;       	
        	        	
        	try {
        		
        		apSupplier = apSupplierHome.findBySplSupplierCode(SPL_SPPLR_CODE, AD_CMPNY);
        		apTaxCode = apTaxCodeHome.findByTcName(TC_NM, AD_CMPNY);
        		apWithholdingTaxCode = apWithholdingTaxCodeHome.findByWtcName(WTC_NM, AD_CMPNY);
        		        		
        	} catch (FinderException ex) {
        		
        		throw new GlobalNoRecordFoundException();
        		
        	}
        	
        	double NET_AMOUNT = 0d;
        	double TAX_AMOUNT = 0d;
        	double W_TAX_AMOUNT = 0d;
        	short LINE_NUMBER = 0;
        		
    		// create dr net expense
    		
    		if (apTaxCode.getTcType().equals("INCLUSIVE")) {
    			
    			NET_AMOUNT = EJBCommon.roundIt(RV_AMNT / (1 + (apTaxCode.getTcRate() / 100)), this.getGlFcPrecisionUnit(AD_CMPNY));
    			
            } else {
                
                // tax exclusive, none, zero rated or exempt
                
                NET_AMOUNT = RV_AMNT;
            
        	} 
        	
        	ApModDistributionRecordDetails mdetails = new ApModDistributionRecordDetails();
        	mdetails.setDrLine(++LINE_NUMBER);
        	mdetails.setDrClass("EXPENSE");
        	mdetails.setDrDebit(EJBCommon.TRUE);
        	mdetails.setDrAmount(NET_AMOUNT);
        	        	
        	LocalGlChartOfAccount glChartOfAccount = glChartOfAccountHome.findByPrimaryKey(apSupplier.getSplCoaGlExpenseAccount());
        	
        	mdetails.setDrCoaAccountNumber(glChartOfAccount.getCoaAccountNumber());
        	mdetails.setDrCoaAccountDescription(glChartOfAccount.getCoaAccountDescription());
        	
        	list.add(mdetails);
        	        	        	
        	// create tax line if necessary
        	   	
        	if (!apTaxCode.getTcType().equals("NONE") &&
        	    !apTaxCode.getTcType().equals("EXEMPT")) {
        	    	
        	            	
	        	if (apTaxCode.getTcType().equals("INCLUSIVE")) {
	        		
	        		TAX_AMOUNT = EJBCommon.roundIt(RV_AMNT - NET_AMOUNT, this.getGlFcPrecisionUnit(AD_CMPNY));
	        		
	        	} else if (apTaxCode.getTcType().equals("EXCLUSIVE")) {
	        		
	        		TAX_AMOUNT = EJBCommon.roundIt(RV_AMNT * apTaxCode.getTcRate() / 100, this.getGlFcPrecisionUnit(AD_CMPNY));
	        		
	            } else {
	            	
	            	// tax none zero-rated or exempt
	            	
	        	}
        	    	
        	    mdetails = new ApModDistributionRecordDetails();
	        	mdetails.setDrLine(++LINE_NUMBER);
	        	mdetails.setDrClass("TAX");
	        	mdetails.setDrDebit(EJBCommon.TRUE);
	        	mdetails.setDrAmount(TAX_AMOUNT);
	        		        	
	        	mdetails.setDrCoaAccountNumber(apTaxCode.getGlChartOfAccount().getCoaAccountNumber());
			    mdetails.setDrCoaAccountDescription(apTaxCode.getGlChartOfAccount().getCoaAccountDescription());
        	
        		list.add(mdetails);        		        		    
			            	    	
        	}
        	
        	// create withholding tax if necessary
        	
        	if (apWithholdingTaxCode.getWtcRate() != 0 && adPreference.getPrfApWTaxRealization().equals("VOUCHER")) {
        		
        		W_TAX_AMOUNT = EJBCommon.roundIt(NET_AMOUNT * (apWithholdingTaxCode.getWtcRate() / 100), this.getGlFcPrecisionUnit(AD_CMPNY));
        		
        		mdetails = new ApModDistributionRecordDetails();
	        	mdetails.setDrLine(++LINE_NUMBER);
	        	mdetails.setDrClass("W-TAX");
	        	mdetails.setDrDebit(EJBCommon.FALSE);
	        	mdetails.setDrAmount(W_TAX_AMOUNT);
	        		        	
	        	mdetails.setDrCoaAccountNumber(apWithholdingTaxCode.getGlChartOfAccount().getCoaAccountNumber());
			    mdetails.setDrCoaAccountDescription(apWithholdingTaxCode.getGlChartOfAccount().getCoaAccountDescription());
        	
        		list.add(mdetails);
        		
        	}
        	
        	// create accounts payable
        	
        	
        	mdetails = new ApModDistributionRecordDetails();
	    	mdetails.setDrLine(++LINE_NUMBER);
	    	mdetails.setDrClass("PAYABLE");
	    	mdetails.setDrDebit(EJBCommon.FALSE);
	    	mdetails.setDrAmount(NET_AMOUNT + TAX_AMOUNT - W_TAX_AMOUNT);
	    		    	
	    	glChartOfAccount = glChartOfAccountHome.findByPrimaryKey(apSupplier.getSplCoaGlPayableAccount());	    	
	    	
	    	mdetails.setDrCoaAccountNumber(glChartOfAccount.getCoaAccountNumber());
		    mdetails.setDrCoaAccountDescription(glChartOfAccount.getCoaAccountDescription());
	
	 		list.add(mdetails);
        	
        	return list;
        	
        	
        } catch (GlobalNoRecordFoundException ex) {
        	
        	throw ex;
        	        	
        } catch (Exception ex) {
        	
        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());
        	
        }
            
    }
    
    
    /**
     * @ejb:interface-method view-type="remote"
     **/
    public void saveApRvEntry(com.util.ApRecurringVoucherDetails details, 
        String RV_AD_NTFD_USR1, String RV_AD_NTFD_USR2, String RV_AD_NTFD_USR3, 
        String RV_AD_NTFD_USR4, String RV_AD_NTFD_USR5, String PYT_NM, String TC_NM, 
        String WTC_NM, String FC_NM, String SPL_SPPLR_CODE, ArrayList drList, String VB_NM, Integer AD_BRNCH, Integer AD_CMPNY) 
        throws GlobalRecordAlreadyExistException,
			   GlobalRecordAlreadyDeletedException,
			   GlobalConversionDateNotExistException,
			   GlobalPaymentTermInvalidException,
			   GlobalBranchAccountNumberInvalidException,
			   GlobalRecordInvalidException {
                    
        Debug.print("ApRecurringVoucherEntryControllerBean saveApRvEntry");
        
        LocalApRecurringVoucherHome apRecurringVoucherHome = null;        
        LocalGlFunctionalCurrencyHome glFunctionalCurrencyHome = null;
        LocalAdPaymentTermHome adPaymentTermHome = null;
        LocalApTaxCodeHome apTaxCodeHome = null;
        LocalApWithholdingTaxCodeHome apWithholdingTaxCodeHome = null;
        LocalApSupplierHome apSupplierHome = null;
        LocalGlFunctionalCurrencyRateHome glFunctionalCurrencyRateHome = null;
        LocalAdUserHome adUserHome = null;
        LocalAdCompanyHome adCompanyHome = null;
        LocalApVoucherBatchHome apVoucherBatchHome = null;
        
        LocalApRecurringVoucher apRecurringVoucher = null;            
                
        // Initialize EJB Home
        
        try {
            
            apRecurringVoucherHome = (LocalApRecurringVoucherHome)EJBHomeFactory.
                lookUpLocalHome(LocalApRecurringVoucherHome.JNDI_NAME, LocalApRecurringVoucherHome.class);
            glFunctionalCurrencyHome = (LocalGlFunctionalCurrencyHome)EJBHomeFactory.
                lookUpLocalHome(LocalGlFunctionalCurrencyHome.JNDI_NAME, LocalGlFunctionalCurrencyHome.class);            
            adPaymentTermHome = (LocalAdPaymentTermHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdPaymentTermHome.JNDI_NAME, LocalAdPaymentTermHome.class);
            apTaxCodeHome = (LocalApTaxCodeHome)EJBHomeFactory.
                lookUpLocalHome(LocalApTaxCodeHome.JNDI_NAME, LocalApTaxCodeHome.class);
            apWithholdingTaxCodeHome = (LocalApWithholdingTaxCodeHome)EJBHomeFactory.
                lookUpLocalHome(LocalApWithholdingTaxCodeHome.JNDI_NAME, LocalApWithholdingTaxCodeHome.class);
            apSupplierHome = (LocalApSupplierHome)EJBHomeFactory.
                lookUpLocalHome(LocalApSupplierHome.JNDI_NAME, LocalApSupplierHome.class);
            glFunctionalCurrencyRateHome = (LocalGlFunctionalCurrencyRateHome)EJBHomeFactory.
                lookUpLocalHome(LocalGlFunctionalCurrencyRateHome.JNDI_NAME, LocalGlFunctionalCurrencyRateHome.class);
            adUserHome = (LocalAdUserHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdUserHome.JNDI_NAME, LocalAdUserHome.class);             
 			adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME,LocalAdCompanyHome.class);
 			apVoucherBatchHome = (LocalApVoucherBatchHome)EJBHomeFactory.
        		lookUpLocalHome(LocalApVoucherBatchHome.JNDI_NAME, LocalApVoucherBatchHome.class);

        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }       
        
        try {
        	
        	// validate if recurring voucher is already deleted
        	
        	try {
        		
        		if (details.getRvCode() != null) {
        			
        			apRecurringVoucher = apRecurringVoucherHome.findByPrimaryKey(details.getRvCode());
        			
        		}
        		
        	} catch (FinderException ex) {
        		
        		throw new GlobalRecordAlreadyDeletedException();
        		
        	}

			// validate if recurring voucher exists
        		
			try {
				
				if(details.getRvName().equals("Interest Income")) {

					LocalApRecurringVoucher apExistingRecurringVoucher = apRecurringVoucherHome.findByRvNameAndSupplierCode(details.getRvName(), SPL_SPPLR_CODE, AD_BRNCH, AD_CMPNY);

					if (details.getRvCode() == null ||
							details.getRvCode() != null && !apExistingRecurringVoucher.getRvCode().equals(details.getRvCode())) {

						throw new GlobalRecordAlreadyExistException();

					}

					
				} else {
					
					LocalApRecurringVoucher apExistingRecurringVoucher = apRecurringVoucherHome.findByRvName(details.getRvName(), AD_BRNCH, AD_CMPNY);
		    		
				    if (details.getRvCode() == null ||
				        details.getRvCode() != null && !apExistingRecurringVoucher.getRvCode().equals(details.getRvCode())) {
				    	
				        throw new GlobalRecordAlreadyExistException();
				        
				    }
				 		
				}
			    		
			} catch (FinderException ex) {
			    
			}
			
		    
		    // validate if conversion date exists
      
	        try {
	      	  
	      	    if (details.getRvConversionDate() != null) {
	      	 
					LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);	      	
					LocalGlFunctionalCurrency glValidateFunctionalCurrency = glFunctionalCurrencyHome.findByFcName(
						FC_NM, AD_CMPNY);
		        	  
	 				if (!glValidateFunctionalCurrency.getFcName().equals("USD")) {
					
	 					LocalGlFunctionalCurrencyRate glFunctionalCurrencyRate = 
	 						glFunctionalCurrencyRateHome.findByFcCodeAndDate(glValidateFunctionalCurrency.getFcCode(),
	 								details.getRvConversionDate(), AD_CMPNY);
					
	 				} else if (!adCompany.getGlFunctionalCurrency().getFcName().equals("USD")){
	 					
	 					LocalGlFunctionalCurrencyRate glFunctionalCurrencyRate = 
	 						glFunctionalCurrencyRateHome.findByFcCodeAndDate(
 								adCompany.getGlFunctionalCurrency().getFcCode(), details.getRvConversionDate(), AD_CMPNY);

	 				}
		      	          
		        }	          
	      	      	      	
	        } catch (FinderException ex) {
	      	  
	        	 throw new GlobalConversionDateNotExistException();
	      	
	        }
	        
	        // validate if payment term has at least one payment schedule
	        
	        if (adPaymentTermHome.findByPytName(PYT_NM, AD_CMPNY).getAdPaymentSchedules().isEmpty()) {
	        	
	        	throw new GlobalPaymentTermInvalidException();
	        	
	        }
	        
	        LocalAdUser userName2 = null;
	        LocalAdUser userName3 = null;
	        LocalAdUser userName4 = null;
	        LocalAdUser userName5 = null;

		    // get all notified users
		    
		    LocalAdUser userName1 = adUserHome.findByUsrName(RV_AD_NTFD_USR1, AD_CMPNY);
            
             	    
		    try {
		    
		        userName2 = adUserHome.findByUsrName(RV_AD_NTFD_USR2, AD_CMPNY);

	        } catch (FinderException ex) {

	        }
	        
	        
	        if(RV_AD_NTFD_USR3 != null) {

		        try {
	
			    	userName3 = adUserHome.findByUsrName(RV_AD_NTFD_USR3, AD_CMPNY);
	
		        } catch (FinderException ex) {
	
		        }
	        
	        }
	        
	        if(RV_AD_NTFD_USR4 != null) { 
	        
		        try {
	
			    	userName4 = adUserHome.findByUsrName(RV_AD_NTFD_USR4, AD_CMPNY);
	
		        } catch (FinderException ex) {
	
		        }
	        
	        }
	        
	        if(RV_AD_NTFD_USR5 != null) {
	        
		        try {
	
			    	userName5 = adUserHome.findByUsrName(RV_AD_NTFD_USR5, AD_CMPNY);	          
	
		        } catch (FinderException ex) {
	
		        }
	        
	        }
	      	      	      	
        	// create recurring voucher
        	
        	if (details.getRvCode() == null) {
			
				apRecurringVoucher = apRecurringVoucherHome.create(details.getRvName(),
				    details.getRvDescription(), details.getRvConversionDate(), 
				    details.getRvConversionRate(), details.getRvAmount(), details.getRvAmountDue(),
				    userName1.getUsrCode(), 
				    userName2 != null ? userName2.getUsrCode() : null,
				    userName3 != null ? userName3.getUsrCode() : null,
				    userName4 != null ? userName4.getUsrCode() : null,
				    userName5 != null ? userName5.getUsrCode() : null, 
				    details.getRvSchedule(), details.getRvNextRunDate(), null, AD_BRNCH, AD_CMPNY);             	    	
        	    	        	    
			            		
        	} else {
        		
	    		apRecurringVoucher.setRvName(details.getRvName());
	    		apRecurringVoucher.setRvDescription(details.getRvDescription());
	    		apRecurringVoucher.setRvConversionDate(details.getRvConversionDate());
	    		apRecurringVoucher.setRvConversionRate(details.getRvConversionRate());
	    		apRecurringVoucher.setRvAdUserName1(userName1.getUsrCode());
	    		apRecurringVoucher.setRvAdUserName2(userName2 != null ? userName2.getUsrCode() : null);
	    		apRecurringVoucher.setRvAdUserName3(userName3 != null ? userName3.getUsrCode() : null);
	    		apRecurringVoucher.setRvAdUserName4(userName4 != null ? userName4.getUsrCode() : null);
	    		apRecurringVoucher.setRvAdUserName5(userName5 != null ? userName5.getUsrCode() : null);
	    		apRecurringVoucher.setRvAmount(details.getRvAmount());
	    		apRecurringVoucher.setRvAmountDue(details.getRvAmountDue());
	    		apRecurringVoucher.setRvSchedule(details.getRvSchedule());
	    		apRecurringVoucher.setRvNextRunDate(details.getRvNextRunDate());
        		
        	}
        	
        	LocalAdPaymentTerm adPaymentTerm = adPaymentTermHome.findByPytName(PYT_NM, AD_CMPNY);
        	adPaymentTerm.addApRecurringVoucher(apRecurringVoucher);
        	
        	LocalApTaxCode apTaxCode = apTaxCodeHome.findByTcName(TC_NM, AD_CMPNY);
        	apTaxCode.addApRecurringVoucher(apRecurringVoucher);
        	
        	LocalApWithholdingTaxCode apWithholdingTaxCode = apWithholdingTaxCodeHome.findByWtcName(WTC_NM, AD_CMPNY);
        	apWithholdingTaxCode.addApRecurringVoucher(apRecurringVoucher);
        	
        	LocalGlFunctionalCurrency glFunctionalCurrency = glFunctionalCurrencyHome.findByFcName(FC_NM, AD_CMPNY);
        	glFunctionalCurrency.addApRecurringVoucher(apRecurringVoucher);
        	
        	LocalApSupplier apSupplier = apSupplierHome.findBySplSupplierCode(SPL_SPPLR_CODE, AD_CMPNY);
        	apSupplier.addApRecurringVoucher(apRecurringVoucher);
        	
        	// interest income
        	if(details.getRvName().equals("Interest Income") && 
        		!apSupplier.getApSupplierClass().getScName().contains("Investors")) {

        		throw new GlobalRecordInvalidException();
        		
        	}
        	
        	try {
        		
            	LocalApVoucherBatch apVoucherBatch = apVoucherBatchHome.findByVbName(VB_NM, AD_BRNCH, AD_CMPNY);
            	apVoucherBatch.addApRecurringVoucher(apRecurringVoucher);	
            	
            } catch (FinderException ex) {
            		
            }
        	
        	// remove all distribution records
      	   
	  	    Collection apDistributionRecords = apRecurringVoucher.getApDistributionRecords();
	  	  
	  	    Iterator i = apDistributionRecords.iterator();     	  
	  	  
	  	    while (i.hasNext()) {
	  	  	
	  	   	    LocalApDistributionRecord apDistributionRecord = (LocalApDistributionRecord)i.next();
	  	  	   
	  	  	    i.remove();
	  	  	    
	  	  	    apDistributionRecord.remove();
	  	  	
	  	    }
	  	    
	  	    
	  	    // add new distribution records
      	  
      	    i = drList.iterator();
      	  
      	    while (i.hasNext()) {
      	  	
      	  	    ApModDistributionRecordDetails mDrDetails = (ApModDistributionRecordDetails) i.next();      	  	  
      	  	          	  	  
      	  	    this.addApDrEntry(mDrDetails, apRecurringVoucher, AD_BRNCH, AD_CMPNY);
      	  	
      	    }

      	} catch (GlobalRecordAlreadyDeletedException ex) {
        	
        	getSessionContext().setRollbackOnly();      	  
      	    throw ex;
      	    
      	} catch (GlobalRecordAlreadyExistException ex) {
        	
        	getSessionContext().setRollbackOnly();      	  
      	    throw ex;
        	
        } catch (GlobalConversionDateNotExistException ex) {
        	 
        	getSessionContext().setRollbackOnly();      	  
      	    throw ex;
      	
        } catch (GlobalPaymentTermInvalidException ex) {
       	 
        	getSessionContext().setRollbackOnly();      	  
     	    throw ex;

        } catch (GlobalBranchAccountNumberInvalidException ex) {
        	 
        	getSessionContext().setRollbackOnly();      	  
      	    throw ex;
      	                    	
        } catch (GlobalRecordInvalidException ex) {
        	 
        	getSessionContext().setRollbackOnly();      	  
      	    throw ex;
      	                    	
        } catch (Exception ex) {
        	
        	Debug.printStackTrace(ex);
        	getSessionContext().setRollbackOnly();
        	throw new EJBException(ex.getMessage());
        	
        }

            
    }
    
   /**
    * @ejb:interface-method view-type="remote"
    * @jboss:method-attributes read-only="true"
    **/
    public short getGlFcPrecisionUnit(Integer AD_CMPNY) {

       Debug.print("ApRecurringVoucherEntryControllerBean getGlFcPrecisionUnit");

      
       LocalAdCompanyHome adCompanyHome = null;
      
     
       // Initialize EJB Home
        
       try {
            
           adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
              lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);       
            
       } catch (NamingException ex) {
            
           throw new EJBException(ex.getMessage());
            
       }

       try {
       	
         LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
            
         return  adCompany.getGlFunctionalCurrency().getFcPrecision();
                                     
       } catch (Exception ex) {
       	 
       	 Debug.printStackTrace(ex);
         throw new EJBException(ex.getMessage());
         
       }

    }

    /**
     * @ejb:interface-method view-type="remote"
     **/
    public void deleteApRvEntry(Integer RV_CODE, Integer AD_CMPNY) 
        throws GlobalRecordAlreadyDeletedException {
                    
        Debug.print("ApRecurringVoucherControllerBean deleteApRvEntry");
        
        LocalApRecurringVoucherHome apRecurringVoucherHome = null;         
               
        // Initialize EJB Home
        
        try {
            
            apRecurringVoucherHome = (LocalApRecurringVoucherHome)EJBHomeFactory.
                lookUpLocalHome(LocalApRecurringVoucherHome.JNDI_NAME, LocalApRecurringVoucherHome.class);                      
            
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }

	    try {
         
            LocalApRecurringVoucher apRecurringVoucher = apRecurringVoucherHome.findByPrimaryKey(RV_CODE);
            
            apRecurringVoucher.remove();

	    } catch (FinderException ex) {
	    	
	         throw new GlobalRecordAlreadyDeletedException();       	
        	
        } catch (Exception ex) {
        	
        	 throw new EJBException(ex.getMessage());
        	
        }
        
	}
    
    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
     public byte getAdPrfApUseSupplierPulldown(Integer AD_CMPNY) {

        Debug.print("ApRecurringVoucherEntryControllerBean getAdPrfApUseSupplierPulldown");
                   
        LocalAdPreferenceHome adPreferenceHome = null;
       
       
        // Initialize EJB Home
         
        try {
             
           adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
               lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);       
             
        } catch (NamingException ex) {
             
           throw new EJBException(ex.getMessage());
             
        }
       

        try {
        	
           LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);
           
           return adPreference.getPrfApUseSupplierPulldown();
          
        } catch (Exception ex) {
         	 
           Debug.printStackTrace(ex);
           throw new EJBException(ex.getMessage());
          
        }
       
     }

  	
  	/**
  	 * @ejb:interface-method view-type="remote"
  	 * @jboss:method-attributes read-only="true"
  	 **/
  	public double getFrRateByFrNameAndFrDate(String FC_NM, Date CONVERSION_DATE, Integer AD_CMPNY) 
  	throws GlobalConversionDateNotExistException {
  		
  		Debug.print("ApRecurringVoucherEntryControllerBean getFrRateByFrNameAndFrDate");
  		
  		LocalGlFunctionalCurrencyRateHome glFunctionalCurrencyRateHome = null; 
  		LocalGlFunctionalCurrencyHome glFunctionalCurrencyHome = null;
  		LocalAdCompanyHome adCompanyHome = null;
  		
  		// Initialize EJB Home
  		
  		try {
  			
  			glFunctionalCurrencyRateHome = (LocalGlFunctionalCurrencyRateHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlFunctionalCurrencyRateHome.JNDI_NAME, LocalGlFunctionalCurrencyRateHome.class);
  			glFunctionalCurrencyHome = (LocalGlFunctionalCurrencyHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlFunctionalCurrencyHome.JNDI_NAME, LocalGlFunctionalCurrencyHome.class);
  			adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
  			
  		} catch (NamingException ex) {
  			
  			throw new EJBException(ex.getMessage());
  			
  		}
  		
  		try {
  			
  			LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
  			LocalGlFunctionalCurrency glFunctionalCurrency = glFunctionalCurrencyHome.findByFcName(FC_NM, AD_CMPNY);
  			
  			double CONVERSION_RATE = 1;
  			
  			// Get functional currency rate
  			
  			if (!FC_NM.equals("USD")) {
  				
  				LocalGlFunctionalCurrencyRate glFunctionalCurrencyRate =
  					glFunctionalCurrencyRateHome.findByFcCodeAndDate(glFunctionalCurrency.getFcCode(),
  							CONVERSION_DATE, AD_CMPNY);
  				
  				CONVERSION_RATE = glFunctionalCurrencyRate.getFrXToUsd();
  				
  			}
  			
  			// Get set of book functional currency rate if necessary
  			
  			if (!adCompany.getGlFunctionalCurrency().getFcName().equals("USD")) {
  				
  				LocalGlFunctionalCurrencyRate glCompanyFunctionalCurrencyRate =
  					glFunctionalCurrencyRateHome.findByFcCodeAndDate(adCompany.getGlFunctionalCurrency(). getFcCode(),
  							CONVERSION_DATE, AD_CMPNY);
  				
  				CONVERSION_RATE = CONVERSION_RATE / glCompanyFunctionalCurrencyRate.getFrXToUsd();
  				
  			}
  			
  			return CONVERSION_RATE;
  			
  		} catch (FinderException ex) {	
  			
  			getSessionContext().setRollbackOnly();
  			throw new GlobalConversionDateNotExistException();  
  			
  		} catch (Exception ex) {
  			
  			Debug.printStackTrace(ex);
  			throw new EJBException(ex.getMessage());
  			
  		}
  		
  	}
  	
    
    // private methods
    
    private void addApDrEntry(ApModDistributionRecordDetails mdetails, LocalApRecurringVoucher apRecurringVoucher, Integer AD_BRNCH, Integer AD_CMPNY) 
        throws GlobalBranchAccountNumberInvalidException {
			
		Debug.print("ApRecurringVoucherEntryControllerBean addApDrEntry");
		
		LocalApDistributionRecordHome apDistributionRecordHome = null;        
		LocalGlChartOfAccountHome glChartOfAccountHome = null;
		LocalAdCompanyHome adCompanyHome = null;
           
                
        // Initialize EJB Home
        
        try {
            
            apDistributionRecordHome = (LocalApDistributionRecordHome)EJBHomeFactory.
                lookUpLocalHome(LocalApDistributionRecordHome.JNDI_NAME, LocalApDistributionRecordHome.class);            
            glChartOfAccountHome = (LocalGlChartOfAccountHome)EJBHomeFactory.
                lookUpLocalHome(LocalGlChartOfAccountHome.JNDI_NAME, LocalGlChartOfAccountHome.class);            
            adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);            
             
                        
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }            
                
        try {
        	
        	// get company
        	
        	LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
        	
        	// validate if coa exists
        	
        	LocalGlChartOfAccount glChartOfAccount = null;
        	
        	try {
        	
        		glChartOfAccount = glChartOfAccountHome.findByCoaAccountNumberAndBranchCode(mdetails.getDrCoaAccountNumber(), AD_BRNCH, AD_CMPNY);
        		
        		if (glChartOfAccount.getCoaEnable() == EJBCommon.FALSE) 
        		    throw new GlobalBranchAccountNumberInvalidException();
        		
        	} catch (FinderException ex) {
        		
        		throw new GlobalBranchAccountNumberInvalidException();
        		
        	}
        	        			    
		    // create distribution record 
		    
		    LocalApDistributionRecord apDistributionRecord = apDistributionRecordHome.create(
			    mdetails.getDrLine(), 
			    mdetails.getDrClass(),
			    EJBCommon.roundIt(mdetails.getDrAmount(), adCompany.getGlFunctionalCurrency().getFcPrecision()),
			    mdetails.getDrDebit(), EJBCommon.FALSE, EJBCommon.FALSE, AD_CMPNY);
			    
			apRecurringVoucher.addApDistributionRecord(apDistributionRecord);
			glChartOfAccount.addApDistributionRecord(apDistributionRecord);	
			
        } catch(GlobalBranchAccountNumberInvalidException ex) {
            
            throw ex;
		    		            		
        } catch (Exception ex) {
        	
        	Debug.printStackTrace(ex);
        	getSessionContext().setRollbackOnly();
        	throw new EJBException(ex.getMessage());
        	
        }
		
	}	
	
	// SessionBean methods

    /**
     * @ejb:create-method view-type="remote"
     **/
    public void ejbCreate() throws CreateException {

       Debug.print("ApRecurringVoucherEntryControllerBean ejbCreate");
      
    }

}