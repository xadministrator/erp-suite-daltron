
/*
 * ApRecurringVoucherGenerationControllerBean.java
 *
 * Created on February 20, 2004, 9:54 AM
 *
 * @author  Neil Andrew M. Ajero
 */

package com.ejb.txn;

import java.sql.Array;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;
import javax.naming.NamingException;

import com.ejb.ad.LocalAdCompany;
import com.ejb.ad.LocalAdCompanyHome;
import com.ejb.ad.LocalAdPaymentSchedule;
import com.ejb.ad.LocalAdPaymentTerm;
import com.ejb.ad.LocalAdPreference;
import com.ejb.ad.LocalAdPreferenceHome;
import com.ejb.ap.LocalApDistributionRecord;
import com.ejb.ap.LocalApDistributionRecordHome;
import com.ejb.ap.LocalApRecurringVoucher;
import com.ejb.ap.LocalApRecurringVoucherHome;
import com.ejb.ap.LocalApSupplier;
import com.ejb.ap.LocalApSupplierBalance;
import com.ejb.ap.LocalApSupplierBalanceHome;
import com.ejb.ap.LocalApSupplierHome;
import com.ejb.ap.LocalApTaxCode;
import com.ejb.ap.LocalApVoucher;
import com.ejb.ap.LocalApVoucherBatch;
import com.ejb.ap.LocalApVoucherBatchHome;
import com.ejb.ap.LocalApVoucherHome;
import com.ejb.ap.LocalApVoucherPaymentSchedule;
import com.ejb.ap.LocalApVoucherPaymentScheduleHome;
import com.ejb.ap.LocalApWithholdingTaxCode;
import com.ejb.exception.GlobalAmountInvalidException;
import com.ejb.exception.GlobalDocumentNumberNotUniqueException;
import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.exception.GlobalRecordAlreadyDeletedException;
import com.ejb.gl.LocalAdDocumentSequenceAssignment;
import com.ejb.gl.LocalAdDocumentSequenceAssignmentHome;
import com.ejb.gl.LocalGlFunctionalCurrency;
import com.util.AbstractSessionBean;
import com.util.ApModRecurringVoucherDetails;
import com.util.Debug;
import com.util.EJBCommon;
import com.util.EJBHomeFactory;

/**
 * @ejb:bean name="ApRecurringVoucherGenerationControllerEJB"
 *           display-name="Used for generating vouchers"
 *           type="Stateless"
 *           view-type="remote"
 *           jndi-name="ejb/ApRecurringVoucherGenerationControllerEJB"
 *
 * @ejb:interface remote-class="com.ejb.txn.ApRecurringVoucherGenerationController"
 *                extends="javax.ejb.EJBObject"
 *
 * @ejb:home remote-class="com.ejb.txn.ApRecurringVoucherGenerationControllerHome"
 *           extends="javax.ejb.EJBHome"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="apuser"
 *                        role-link="apuserlink"
 *
 * @ejb:permission role-name="apuser"
 * 
*/

public class ApRecurringVoucherGenerationControllerBean extends AbstractSessionBean {


    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList getApSplAll(Integer AD_BRNCH, Integer AD_CMPNY) {
                    
        Debug.print("ApRecurringVoucherGenerationControllerBean getApSplAll");
        
        LocalApSupplierHome apSupplierHome = null;               
        
        ArrayList list = new ArrayList();
        
        // Initialize EJB Home
        
        try {
            
            apSupplierHome = (LocalApSupplierHome)EJBHomeFactory.
                lookUpLocalHome(LocalApSupplierHome.JNDI_NAME, LocalApSupplierHome.class);
            
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }        
        
        try {
        	
        	Collection apSuppliers = apSupplierHome.findEnabledSplAll(AD_BRNCH, AD_CMPNY);
        	
        	Iterator i = apSuppliers.iterator();
        	
        	while (i.hasNext()) {
        		
        		LocalApSupplier apSupplier = (LocalApSupplier)i.next();
        		
        		list.add(apSupplier.getSplSupplierCode());
        		
        	}
        	
        	return list;
        	
        } catch (Exception ex) {
        	
        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());
        	
        }
            
    } 
    
   
  /**
   * @ejb:interface-method view-type="remote"
   * @jboss:method-attributes read-only="true"
   **/
   public ArrayList getApRvByCriteria(HashMap criteria, String ORDER_BY, Integer OFFSET, Integer LIMIT, double RV_INTRST_RT, Integer AD_BRNCH, Integer AD_CMPNY)
      throws GlobalNoRecordFoundException {

      Debug.print("ApRecurringVoucherGenerationControllerBean getApRvByCriteria");
      
      LocalApRecurringVoucherHome apRecurringJournalHome = null;
      
      // Initialize EJB Home
        
      try {
      	
          apRecurringJournalHome = (LocalApRecurringVoucherHome)EJBHomeFactory.
              lookUpLocalHome(LocalApRecurringVoucherHome.JNDI_NAME, LocalApRecurringVoucherHome.class);          
            
      } catch (NamingException ex) {
            
          throw new EJBException(ex.getMessage());
            
      }

      
      ArrayList rvList = new ArrayList();
      
      StringBuffer jbossQl = new StringBuffer();
      jbossQl.append("SELECT OBJECT(rv) FROM ApRecurringVoucher rv ");
      
      boolean firstArgument = true;
      short ctr = 0;
      int criteriaSize = criteria.size() + 2;
      Object obj[];
      
       // Allocate the size of the object parameter

       
      if (criteria.containsKey("name")) {
      	
      	  criteriaSize--; 
      	 
      } 
            
      
      obj = new Object[criteriaSize];
         
      
      if (criteria.containsKey("name")) {
      	
      	 if (!firstArgument) {
      	 
      	    jbossQl.append("AND ");	
      	 	
         } else {
         	
         	firstArgument = false;
         	jbossQl.append("WHERE ");
         	
         }
         
      	 jbossQl.append("rv.rvName LIKE '%" + (String)criteria.get("name") + "%' ");
      	 
      }            	 

	  	  	  
	  if (criteria.containsKey("supplierCode")) {
       	
       	  if (!firstArgument) {
       	  	
       	     jbossQl.append("AND ");
       	     
       	  } else {
       	  	
       	  	 firstArgument = false;
       	  	 jbossQl.append("WHERE ");
       	  	 
       	  }
       	  
       	  jbossQl.append("rv.apSupplier.splSupplierCode=?" + (ctr+1) + " ");
       	  obj[ctr] = (String)criteria.get("supplierCode");
       	  ctr++;
       	  
      } 
        
      if (criteria.containsKey("nextRunDateFrom")) {
	      	
	  	 if (!firstArgument) {
	  	 	jbossQl.append("AND ");
	  	 } else {
	  	 	firstArgument = false;
	  	 	jbossQl.append("WHERE ");
	  	 }
	  	 jbossQl.append("rv.rvNextRunDate>=?" + (ctr+1) + " ");
	  	 obj[ctr] = (Date)criteria.get("nextRunDateFrom");
	  	 ctr++;
	  }  
	      
	  if (criteria.containsKey("nextRunDateTo")) {
	  	
	  	 if (!firstArgument) {
	  	 	jbossQl.append("AND ");
	  	 } else {
	  	 	firstArgument = false;
	  	 	jbossQl.append("WHERE ");
	  	 }
	  	 jbossQl.append("rv.rvNextRunDate<=?" + (ctr+1) + " ");
	  	 obj[ctr] = (Date)criteria.get("nextRunDateTo");
	  	 ctr++;
	  	 
	  }  
	  
	  if (!firstArgument) {
	      jbossQl.append("AND ");
	  } else {
	      firstArgument = false;
	      jbossQl.append("WHERE ");
	  }
	  
	  jbossQl.append("rv.rvAdBranch=" + AD_BRNCH + " ");
	  
	  if (!firstArgument) {
  	 	jbossQl.append("AND ");
  	  } else {
  	 	firstArgument = false;
  	 	jbossQl.append("WHERE ");
  	  }
	  
	  jbossQl.append("rv.rvAdCompany=" + AD_CMPNY + " ");
             	     
      String orderBy = null;
	      
	  if (ORDER_BY.equals("NAME")) {
	
	  	  orderBy = "rv.rvName";
	  	  
	  } else if (ORDER_BY.equals("NEXT RUN DATE")) {
	
	  	  orderBy = "rv.rvNextRunDate";
	  	
	  } else if (ORDER_BY.equals("SUPPLIER CODE")) {
	
	  	  orderBy = "rv.apSupplier.splSupplierCode";
	  	  
	  } 
	  
	  if (orderBy != null) {
	  
	  	jbossQl.append("ORDER BY " + orderBy);
	  	
	  } 
	  
               
      jbossQl.append(" OFFSET ?" + (ctr + 1));
      obj[ctr] = OFFSET;
      ctr++;
      
      jbossQl.append(" LIMIT ?" + (ctr + 1));
      obj[ctr] = LIMIT;
      
      System.out.println("QL + " + jbossQl);

      Collection apRecurringJournals = null;
      
      try {
      	
         apRecurringJournals = apRecurringJournalHome.getRvByCriteria(jbossQl.toString(), obj);
         
      } catch (Exception ex) {
      	
      	 throw new EJBException(ex.getMessage());
      	 
      }
      
      if (apRecurringJournals.isEmpty())
         throw new GlobalNoRecordFoundException();
         
      Iterator i = apRecurringJournals.iterator();
      while (i.hasNext()) {

         LocalApRecurringVoucher apRecurringVoucher = (LocalApRecurringVoucher) i.next();
	     
	     // generate new next run date
	     
	     GregorianCalendar gc = new GregorianCalendar();
	     gc.setTime(apRecurringVoucher.getRvNextRunDate());	     
	     	     
	     if (apRecurringVoucher.getRvSchedule().equals("DAILY")) {
	     	
	     	gc.add(Calendar.DATE, 1);
	     	
	     } else if (apRecurringVoucher.getRvSchedule().equals("WEEKLY")) {
	     	
	     	gc.add(Calendar.DATE, 7);
	     	
	     } else if (apRecurringVoucher.getRvSchedule().equals("SEMI MONTHLY")) {
	     	
	     	gc.add(Calendar.DATE, 15);
	     	
	     } else if (apRecurringVoucher.getRvSchedule().equals("MONTHLY")) {
	     	
	     	gc.add(Calendar.MONTH, 1);
	     	
	     } else if (apRecurringVoucher.getRvSchedule().equals("QUARTERLY")) {
	     	
	     	gc.add(Calendar.MONTH, 3);
	     	
	     } else if (apRecurringVoucher.getRvSchedule().equals("SEMI ANNUALLY")) {
	     	
	     	gc.add(Calendar.MONTH, 6);
	     	
	     } else if (apRecurringVoucher.getRvSchedule().equals("ANNUALLY")) {
	     	
	     	gc.add(Calendar.YEAR, 1);
	     	
	     }
	     
	     ApModRecurringVoucherDetails mdetails = new ApModRecurringVoucherDetails();
	     mdetails.setRvCode(apRecurringVoucher.getRvCode());
	     mdetails.setRvName(apRecurringVoucher.getRvName());
	     mdetails.setRvNextRunDate(apRecurringVoucher.getRvNextRunDate());
	     mdetails.setRvSplSupplierCode(apRecurringVoucher.getApSupplier().getSplSupplierCode());
	     mdetails.setRvSchedule(apRecurringVoucher.getRvSchedule());
	     mdetails.setRvLastRunDate(apRecurringVoucher.getRvLastRunDate());     
	     mdetails.setRvNewNextRunDate(gc.getTime());
	     mdetails.setRvInterestRate(RV_INTRST_RT);
	           	  
      	 rvList.add(mdetails);
      	
      }
         
      return rvList;
  
   }

    /**
    * @ejb:interface-method view-type="remote"
    **/
    public void executeApRvGeneration(Integer RV_CODE, Date RV_NXT_RN_DT,
        String VOU_DCMNT_NMBR, Date VOU_DT, double RV_INTRST_RT, String USR_NM, 
        Integer AD_BRNCH, Integer AD_CMPNY) throws     	
		GlobalRecordAlreadyDeletedException,
		GlobalDocumentNumberNotUniqueException,
		GlobalAmountInvalidException {

       Debug.print("ApRecurringVoucherGenerationControllerBean executeApRvGeneration");

       LocalApRecurringVoucherHome apRecurringVoucherHome = null;
       LocalApVoucherHome apVoucherHome = null;
       LocalAdDocumentSequenceAssignmentHome adDocumentSequenceAssignmentHome = null;
       LocalAdCompanyHome adCompanyHome = null;
       LocalApDistributionRecordHome apDistributionRecordHome = null; 
       LocalApVoucherPaymentScheduleHome apVoucherPaymentScheduleHome = null;
       LocalApVoucherBatchHome apVoucherBatchHome = null;
       LocalAdPreferenceHome adPreferenceHome = null;
       
       java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat("MM/dd/yyyy HH:mm:ss.SSSS");
       
       // Initialize EJB Home
        
       try {
                 
           apRecurringVoucherHome = (LocalApRecurringVoucherHome)EJBHomeFactory.
              lookUpLocalHome(LocalApRecurringVoucherHome.JNDI_NAME, LocalApRecurringVoucherHome.class);                  
           apVoucherHome = (LocalApVoucherHome)EJBHomeFactory.
              lookUpLocalHome(LocalApVoucherHome.JNDI_NAME, LocalApVoucherHome.class);                                
           adDocumentSequenceAssignmentHome = (LocalAdDocumentSequenceAssignmentHome)EJBHomeFactory.
              lookUpLocalHome(LocalAdDocumentSequenceAssignmentHome.JNDI_NAME, LocalAdDocumentSequenceAssignmentHome.class);          
           adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
              lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
           apDistributionRecordHome = (LocalApDistributionRecordHome)EJBHomeFactory.
              lookUpLocalHome(LocalApDistributionRecordHome.JNDI_NAME, LocalApDistributionRecordHome.class);                                
           apVoucherPaymentScheduleHome = (LocalApVoucherPaymentScheduleHome)EJBHomeFactory.
              lookUpLocalHome(LocalApVoucherPaymentScheduleHome.JNDI_NAME, LocalApVoucherPaymentScheduleHome.class);
           apVoucherBatchHome = (LocalApVoucherBatchHome)EJBHomeFactory.
        	  lookUpLocalHome(LocalApVoucherBatchHome.JNDI_NAME, LocalApVoucherBatchHome.class);
           adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
           	  lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);


       } catch (NamingException ex) {
            
           throw new EJBException(ex.getMessage());
            
       }

       try {
       	
       	   // get recurring voucher
       	
       	   LocalApRecurringVoucher apRecurringVoucher = null;
       	
       	   try {
       	   	
       	   	   apRecurringVoucher = apRecurringVoucherHome.findByPrimaryKey(RV_CODE);
       	   	
       	   } catch (FinderException ex) {
       	   	
       	   	   throw new GlobalRecordAlreadyDeletedException();
       	   	
       	   }          	         	          	  	 
		   
		   // validate if document number is unique
		   
		   try {
		   
 		   	  LocalApVoucher apExistingVoucher = apVoucherHome.findByVouDocumentNumberAndVouDebitMemoAndBrCode(VOU_DCMNT_NMBR, EJBCommon.FALSE, AD_BRNCH, AD_CMPNY);
 		   	  
 		   	  throw new GlobalDocumentNumberNotUniqueException(apRecurringVoucher.getRvName());
 		   	  
 		   } catch (FinderException ex) {
	   	   }
	   	   
	   	   // generate document number if necessary
	   	   
	   	   LocalAdDocumentSequenceAssignment adDocumentSequenceAssignment =
		            adDocumentSequenceAssignmentHome.findByDcName("AP VOUCHER", AD_CMPNY);
		        	    
	       if (adDocumentSequenceAssignment.getAdDocumentSequence().getDsNumberingType() == 'A' && 
	            (VOU_DCMNT_NMBR == null || VOU_DCMNT_NMBR.trim().length() == 0)) {
	            	
	            while (true) {
	            	
	            	try {
	            		
	            		apVoucherHome.findByVouDocumentNumberAndVouDebitMemoAndBrCode(adDocumentSequenceAssignment.getDsaNextSequence(), EJBCommon.FALSE, AD_BRNCH, AD_CMPNY);		            		
	            		adDocumentSequenceAssignment.setDsaNextSequence(EJBCommon.incrementStringNumber(adDocumentSequenceAssignment.getDsaNextSequence()));	
	            		
	            	} catch (FinderException ex) {
	            		
	            		VOU_DCMNT_NMBR = adDocumentSequenceAssignment.getDsaNextSequence();
	            		adDocumentSequenceAssignment.setDsaNextSequence(EJBCommon.incrementStringNumber(adDocumentSequenceAssignment.getDsaNextSequence()));	
			            break;
	            		
	            	}	            			            			            		            	
	            	
	            }		            
	            		            	
	        }
     	    
    		// generate approval status
    		
    		LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
    		
    		String VOU_APPRVL_STATUS = null;    	
    		
    		// generate voucher batch if necessary
    		
    		LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);
		    LocalApVoucherBatch apVoucherBatch = null;
		   
		    if (adPreference.getPrfEnableApVoucherBatch() == EJBCommon.TRUE) {
		   
		       apVoucherBatch = apRecurringVoucher.getApVoucherBatch();
		    }
    							     

	        // generate recurring voucher
	        
	        LocalApVoucher apVoucher = 
	        	
	        		apVoucherHome.create("EXPENSES", EJBCommon.FALSE, 
	        				apRecurringVoucher.getRvDescription(), VOU_DT, VOU_DCMNT_NMBR, VOU_DCMNT_NMBR, 
	        				null, 
	        				apRecurringVoucher.getRvConversionDate(), 
	        				apRecurringVoucher.getRvConversionRate(), 
	        				apRecurringVoucher.getRvAmount(), 
	        				apRecurringVoucher.getRvAmountDue(), 
	        				0d, 
	        				VOU_APPRVL_STATUS, null,
	        				EJBCommon.FALSE, EJBCommon.FALSE, EJBCommon.FALSE, USR_NM, new Date(), USR_NM,
	        				new Date(), null, null, null, 
	        				null, EJBCommon.FALSE, null, 
	        				EJBCommon.FALSE, EJBCommon.FALSE,
	        				AD_BRNCH, AD_CMPNY);
	        		
	        		
	        		
	        apVoucher.setAdPaymentTerm(apRecurringVoucher.getAdPaymentTerm());
	        apVoucher.setApTaxCode(apRecurringVoucher.getApTaxCode());
	        apVoucher.setApWithholdingTaxCode(apRecurringVoucher.getApWithholdingTaxCode());
	        apVoucher.setGlFunctionalCurrency(adCompany.getGlFunctionalCurrency());
	        apVoucher.setApSupplier(apRecurringVoucher.getApSupplier());
			
			
	       /* apRecurringVoucher.getApSupplier().addApVoucher(apVoucher);
	        apRecurringVoucher.getApTaxCode().addApVoucher(apVoucher);
	        apRecurringVoucher.getApWithholdingTaxCode().addApVoucher(apVoucher);
	        apRecurringVoucher.getAdPaymentTerm().addApVoucher(apVoucher);
	        adCompany.getGlFunctionalCurrency().addApVoucher(apVoucher);
	        */
	        if (apVoucherBatch != null) {
           
               apVoucherBatch.addApVoucher(apVoucher);
           
            }
	        
	        double interestIncome = 0;
	        if (apRecurringVoucher.getRvName().equals("Interest Income")) {
	        	
	        	// get interest income
	        	interestIncome = getInterestIncome(apRecurringVoucher.getApSupplier(), VOU_DT, RV_INTRST_RT, AD_CMPNY);

	        	// set voucher reference number
	        	GregorianCalendar firstDateGc = new GregorianCalendar();
	     		firstDateGc.setTime(VOU_DT);	
	     		firstDateGc.add(Calendar.MONTH, -1);
	 			
	     		GregorianCalendar lastDateGc = new GregorianCalendar(
	     				firstDateGc.get(Calendar.YEAR), 
	     				firstDateGc.get(Calendar.MONTH), 
	     				firstDateGc.getActualMaximum(Calendar.DATE), 
	     				0, 0, 0);

	        	apVoucher.setVouReferenceNumber("INT-" + EJBCommon.convertSQLDateToString(lastDateGc.getTime()));
	        	
	        	// set voucher amount
	        	apVoucher.setVouBillAmount(interestIncome);
	        	apVoucher.setVouAmountDue(interestIncome);
	        	
	        }
	  	    
	        Collection apDistributionRecords = apRecurringVoucher.getApDistributionRecords();
	        
	        Iterator i = apDistributionRecords.iterator();
	        
	        while (i.hasNext()) {
	        	
	        	LocalApDistributionRecord apDistributionRecord = 
	        	    (LocalApDistributionRecord)i.next();
	        	
	        	double drAmount = EJBCommon.roundIt(apDistributionRecord.getDrAmount(), adCompany.getGlFunctionalCurrency().getFcPrecision());
	        	
	        	if (apRecurringVoucher.getRvName().equals("Interest Income")) {
	        		drAmount = EJBCommon.roundIt(interestIncome, adCompany.getGlFunctionalCurrency().getFcPrecision());
	        	}
	        	    
	        	LocalApDistributionRecord apNewDistributionRecord = apDistributionRecordHome.create(
				    apDistributionRecord.getDrLine(), 
				    apDistributionRecord.getDrClass(),
				    drAmount, apDistributionRecord.getDrDebit(), EJBCommon.FALSE, EJBCommon.FALSE, AD_CMPNY);
	        		
	            apDistributionRecord.getGlChartOfAccount().addApDistributionRecord(apNewDistributionRecord);
	            
	            apVoucher.addApDistributionRecord(apNewDistributionRecord);
	        	
	        }
	        
	        // create voucher payment schedule          	    
      	        
      	    short precisionUnit = this.getGlFcPrecisionUnit(AD_CMPNY);
      	    double TOTAL_PAYMENT_SCHEDULE =  0d;
      	    
      	    LocalAdPaymentTerm adPaymentTerm = apRecurringVoucher.getAdPaymentTerm();
      	          	          	    
			Collection adPaymentSchedules = adPaymentTerm.getAdPaymentSchedules();
			
		    i = adPaymentSchedules.iterator();
		
		    while (i.hasNext()) {
			
		        LocalAdPaymentSchedule adPaymentSchedule = (LocalAdPaymentSchedule)i.next();
		        			    
			    // get date due 
			
			    GregorianCalendar gcDateDue = new GregorianCalendar();
			    gcDateDue.setTime(apVoucher.getVouDate());
			    gcDateDue.add(Calendar.DATE, adPaymentSchedule.getPsDueDay());
			    
			    // create a payment schedule 
			    
			    double PAYMENT_SCHEDULE_AMOUNT = 0;
			    
			    // if last payment schedule subtract to avoid rounding difference error
			    
			    if (i.hasNext()) {
			    			    
			        PAYMENT_SCHEDULE_AMOUNT = EJBCommon.roundIt((adPaymentSchedule.getPsRelativeAmount() / adPaymentTerm.getPytBaseAmount()) * apVoucher.getVouAmountDue(), precisionUnit);
			        
			    } else {
			    	
			    	PAYMENT_SCHEDULE_AMOUNT = apVoucher.getVouAmountDue() - TOTAL_PAYMENT_SCHEDULE;
			    	
			    }
			    
		  	    LocalApVoucherPaymentSchedule apVoucherPaymentSchedule =
		            apVoucherPaymentScheduleHome.create(gcDateDue.getTime(), 
		                adPaymentSchedule.getPsLineNumber(),			  	        
				        PAYMENT_SCHEDULE_AMOUNT,
				        0d, EJBCommon.FALSE, AD_CMPNY);					    					
				    
			    apVoucher.addApVoucherPaymentSchedule(apVoucherPaymentSchedule);
			    			    
			    TOTAL_PAYMENT_SCHEDULE += PAYMENT_SCHEDULE_AMOUNT;
			
		    }
	        
	        	        
	        // set new run date
	        
	        apRecurringVoucher.setRvNextRunDate(RV_NXT_RN_DT);
	        apRecurringVoucher.setRvLastRunDate(EJBCommon.getGcCurrentDateWoTime().getTime());

       } catch (GlobalRecordAlreadyDeletedException ex) {
       	   
       	   getSessionContext().setRollbackOnly();    	
       	   throw ex;
	        		
       } catch (GlobalDocumentNumberNotUniqueException ex) {
       	 
       	   getSessionContext().setRollbackOnly();
      	   throw ex;    	       	       	              	         	 	   
                                                    
       } catch (GlobalAmountInvalidException ex) {
      	 
       		getSessionContext().setRollbackOnly();      	  
     	    throw ex;
     	                    	
       } catch (Exception ex) {
       	 
       	   Debug.printStackTrace(ex);
       	   getSessionContext().setRollbackOnly();
           throw new EJBException(ex.getMessage());
         
       }

    }

   
   /**
    * @ejb:interface-method view-type="remote"
    * @jboss:method-attributes read-only="true"
    **/
    public short getGlFcPrecisionUnit(Integer AD_CMPNY) {

       Debug.print("ApRecurringVoucherGenerationControllerBean getGlFcPrecisionUnit");

      
       LocalAdCompanyHome adCompanyHome = null;
      
     
       // Initialize EJB Home
        
       try {
            
           adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
              lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);       
            
       } catch (NamingException ex) {
            
           throw new EJBException(ex.getMessage());
            
       }

       try {
       	
         LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
            
         return  adCompany.getGlFunctionalCurrency().getFcPrecision();
                                     
       } catch (Exception ex) {
       	 
       	 Debug.printStackTrace(ex);
         throw new EJBException(ex.getMessage());
         
       }

    }    
       
   /**
    * @ejb:interface-method view-type="remote"
    * @jboss:method-attributes read-only="true"
    **/
    public byte getAdPrfApUseSupplierPulldown(Integer AD_CMPNY) {

       Debug.print("ApRecurringVoucherGenerationControllerBean getAdPrfApUseSupplierPulldown");
                  
       LocalAdPreferenceHome adPreferenceHome = null;
      
      
       // Initialize EJB Home
        
       try {
            
          adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
              lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);       
            
       } catch (NamingException ex) {
            
          throw new EJBException(ex.getMessage());
            
       }
      

       try {
       	
          LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);
          
          return adPreference.getPrfApUseSupplierPulldown();
         
       } catch (Exception ex) {
        	 
          Debug.printStackTrace(ex);
          throw new EJBException(ex.getMessage());
         
       }
      
    }

    private double getInterestIncome(LocalApSupplier apSupplier, Date VOU_DT, double RV_INTRST_RT, Integer AD_CMPNY) 
    throws GlobalAmountInvalidException {

     	Debug.print("ApRecurringVoucherGenerationControllerBean getInterestIncome");

     	LocalApSupplierBalanceHome apSupplierBalanceHome = null;
     	
	    double totalInterestIncome = 0d;     	
     	double interestIncome = 0;
     	double interest = 0d;
 		
 		// Initialize EJB Home

     	try {

     		apSupplierBalanceHome = (LocalApSupplierBalanceHome)EJBHomeFactory.
     		lookUpLocalHome(LocalApSupplierBalanceHome.JNDI_NAME, LocalApSupplierBalanceHome.class);       
     	
     	} catch (NamingException ex) {

     		throw new EJBException(ex.getMessage());

     	}


     	try {
     		
     		GregorianCalendar firstDateGc = new GregorianCalendar();
     		firstDateGc.setTime(VOU_DT);	
     		firstDateGc.add(Calendar.MONTH, -1);
 			
     		GregorianCalendar lastDateGc = new GregorianCalendar(
     				firstDateGc.get(Calendar.YEAR), 
     				firstDateGc.get(Calendar.MONTH), 
     				firstDateGc.getActualMaximum(Calendar.DATE), 
     				0, 0, 0);
     		
     		GregorianCalendar lastLastMonthGc = new GregorianCalendar();
     		lastLastMonthGc.setTime(lastDateGc.getTime());
     		lastLastMonthGc.add(Calendar.MONTH, -1);
     		
     		
     		Collection apSupplierBalances = apSupplierBalanceHome.findBySbDateFromAndSbDateToAndSplCode(
     				lastLastMonthGc.getTime(), lastDateGc.getTime(), apSupplier.getSplCode(), AD_CMPNY);
     		
     		if (!apSupplierBalances.isEmpty()) {
     			
     			long inclusiveDays = 0;
     			
     			// A.) calculate interest if first txn is not on the first day of the period
     			
 				ArrayList apSupplierBalancesList = new ArrayList(apSupplierBalances);

     			LocalApSupplierBalance apSupplierBalance = (LocalApSupplierBalance)apSupplierBalancesList.get(0);

 				if(apSupplierBalance.getSbDate().after(firstDateGc.getTime())) {
 					
 					double balance = 0;
 	    			
 					Collection apSupplierPriorBalances = apSupplierBalanceHome.findByBeforeSbDateAndSplCode(
 	    					firstDateGc.getTime(), apSupplier.getSplCode(), AD_CMPNY);
 					
 					if (!apSupplierPriorBalances.isEmpty()) {
 	    				
 	    				ArrayList apSupplierPriorBalanceList = new ArrayList(apSupplierPriorBalances);
 						
 						LocalApSupplierBalance apSupplierPriorBalance = (LocalApSupplierBalance)apSupplierPriorBalanceList.get(apSupplierPriorBalanceList.size() - 1);

 						balance = apSupplierPriorBalance.getSbBalance();	  

 	    			}
 					
 					inclusiveDays = (VOU_DT.getTime() - apSupplierBalance.getSbDate().getTime()) / (1000 * 60 * 60 * 24);
 					
 					if (balance > 500000) {
 	    				
 	    				interestIncome = balance * (((RV_INTRST_RT + 0.5)/100) / 365) * inclusiveDays;
 	    				
 	    			} else {
 	    				
 	    				interestIncome = balance * ((RV_INTRST_RT/100) / 365) * inclusiveDays;
 	    				
 	    			}
 					
 				}
 				
 				if (!apSupplierBalances.isEmpty()) {
 					
 					double balance = 0d;
 			     	
 					double baseInterest = 0d;
 			     	double bonusInterest = 0d;
 			     	double totalBaseInterest = 0d;
 			     	double totalBonusInterest = 0d;
 			     	double totalInclusiveDays = 0d;
 			     	
 		 			
 					apSupplierBalancesList = new ArrayList(apSupplierBalances);

 	    			apSupplierBalance = (LocalApSupplierBalance)apSupplierBalancesList.get(0);

 			     	Date olderDateBeforeVouDate = apSupplierBalance.getSbDate();
 			     	Date latestDateBeforeVouDate = apSupplierBalance.getSbDate();
 	    			
 		     		Collection apLatestDateAndBalances = apSupplierBalanceHome.findBySbDateFromAndSbDateToAndSplCode(
 		     				apSupplierBalance.getSbDate(), lastDateGc.getTime(), apSupplier.getSplCode(), AD_CMPNY);
 			     	
 	    			Iterator x = apLatestDateAndBalances.iterator();
 	    			
 	    			while (x.hasNext())
 	    			{
 	    				LocalApSupplierBalance apLatestDateAndBalance = (LocalApSupplierBalance)x.next();
 	    				
 	    				if((apLatestDateAndBalance.getSbDate().getTime() <= lastDateGc.getTime().getTime()) 
 	    						&& (apLatestDateAndBalance.getSbDate().getTime() >= olderDateBeforeVouDate.getTime()))
 	    				{
 	    					latestDateBeforeVouDate = apLatestDateAndBalance.getSbDate();
 	    				}
 	    				
 	    				balance = apLatestDateAndBalance.getSbBalance();
 	    				
 	 	    			inclusiveDays = ((latestDateBeforeVouDate.getTime() - olderDateBeforeVouDate.getTime()) / (24 * 60 * 60 * 1000));
 	 	    			totalInclusiveDays += inclusiveDays;
 	 	    			
 	 	    			
 	 	 				// B.) calculate period interest income		BASE 	 	    			
 	 	    			
 	 	    			baseInterest = (balance) * ((RV_INTRST_RT/100) / 365) * inclusiveDays;
 	 	    			totalBaseInterest += baseInterest;
 	 					 	 					

 	 	    			// C.) compute interest for inexcess of 500k		BONUS
 	     				if ((interestIncome + balance) > 500000)
 	     				{
 	     					bonusInterest = (balance - 500000) * ((0.5/100) / 365) * inclusiveDays;
 	     				}
 	     				totalBonusInterest += bonusInterest;
 	     			
 	     				
 	     				interestIncome = baseInterest + bonusInterest;
 	     				totalInterestIncome += interestIncome;

 	     				
 	     				System.out.println("Since Date : " + olderDateBeforeVouDate);
	 					System.out.println("Balance : " + balance);
 	 					System.out.println("Inclusive Days : " + inclusiveDays);
 	 					System.out.println("Base interest : " + baseInterest);
 	 					System.out.println("Bonus interest : " + bonusInterest);
 	 					System.out.println("Total Interest Income : " + interestIncome);
 	 					
 	 					olderDateBeforeVouDate = latestDateBeforeVouDate;
 	     			}
 	    			
 	    				System.out.println("****************************************");
	 					System.out.println("Ending Balance : " + balance);
 	 					System.out.println("Total Inclusive Days : " + totalInclusiveDays);
 	 					System.out.println("Total Base interest : " + totalBaseInterest);
 	 					System.out.println("Total Bonus interest : " + totalBonusInterest);
 	 					System.out.println("Total Interest Income : " + totalInterestIncome);
 	    				System.out.println("****************************************"); 	 					
 	    			
 				}
 				
 				
 				
     		} else {
     			
     			double balance = 0;
     			
     			long inclusiveDays = firstDateGc.getActualMaximum(Calendar.DATE);
     			
     			Collection apSupplierPriorBalances = apSupplierBalanceHome.findByBeforeSbDateAndSplCode(
     					firstDateGc.getTime(), apSupplier.getSplCode(), AD_CMPNY);
         		
     			if (!apSupplierPriorBalances.isEmpty()) {
     				
     				ArrayList apSupplierPriorBalanceList = new ArrayList(apSupplierPriorBalances);
 					
 					LocalApSupplierBalance apSupplierPriorBalance = (LocalApSupplierBalance)apSupplierPriorBalanceList.get(apSupplierPriorBalanceList.size() - 1);

 					balance = apSupplierPriorBalance.getSbBalance();	  

     			}
     			
     			interestIncome = balance * ((RV_INTRST_RT/100) / 365) * inclusiveDays;

 				// D.) compute interest for inexcess of 500k
     			if (balance > 500000) {
     				
         			interest = (balance - 500000) * (((0.5)/100) / 365) * inclusiveDays;
         			interestIncome += interest;
         			totalInterestIncome += interestIncome;
     			} 
     		}
     		
     		if (interestIncome == 0)
     			throw new GlobalAmountInvalidException();
     		
     		return totalInterestIncome;

     	} catch(GlobalAmountInvalidException ex) {
             
             throw ex;
 		    		            		
         } catch (Exception ex) {

     		Debug.printStackTrace(ex);
     		throw new EJBException(ex.getMessage());

     	}

    }

   // SessionBean methods

   /**
    * @ejb:create-method view-type="remote"
    **/
   public void ejbCreate() throws CreateException {

      Debug.print("GlJournalBatchControllerBean ejbCreate");
      
   }

   // private methods
   	
		   
}
