
/*
 * ApRepAgingSummaryControllerBean.java
 *
 * Created on July 12, 2005, 02:37 PM
 *
 * @author  Arnel Masikip
 */

package com.ejb.txn;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;
import javax.naming.NamingException;

import com.ejb.ad.LocalAdBranch;
import com.ejb.ad.LocalAdBranchHome;
import com.ejb.ad.LocalAdBranchResponsibility;
import com.ejb.ad.LocalAdBranchResponsibilityHome;
import com.ejb.ad.LocalAdCompany;
import com.ejb.ad.LocalAdCompanyHome;
import com.ejb.ad.LocalAdPreference;
import com.ejb.ad.LocalAdPreferenceHome;
import com.ejb.ap.LocalApAppliedVoucher;
import com.ejb.ap.LocalApAppliedVoucherHome;
import com.ejb.ap.LocalApSupplierClass;
import com.ejb.ap.LocalApSupplierClassHome;
import com.ejb.ap.LocalApSupplierType;
import com.ejb.ap.LocalApSupplierTypeHome;
import com.ejb.ap.LocalApVoucher;
import com.ejb.ap.LocalApVoucherHome;
import com.ejb.ap.LocalApVoucherPaymentSchedule;
import com.ejb.ap.LocalApVoucherPaymentScheduleHome;
import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.gl.LocalGlFunctionalCurrencyRate;
import com.ejb.gl.LocalGlFunctionalCurrencyRateHome;
import com.util.AbstractSessionBean;
import com.util.AdBranchDetails;
import com.util.AdCompanyDetails;
import com.util.AdPreferenceDetails;
import com.util.ApRepAgingSummaryDetails;
import com.util.Debug;
import com.util.EJBCommon;
import com.util.EJBHomeFactory;

/**
 * @ejb:bean name="ApRepAgingSummaryControllerEJB"
 *           display-name="Used for reporting summarized aged payables"
 *           type="Stateless"
 *           view-type="remote"
 *           jndi-name="ejb/ApRepAgingSummaryControllerEJB"
 *
 * @ejb:interface remote-class="com.ejb.txn.ApRepAgingSummaryController"
 *                extends="javax.ejb.EJBObject"
 *
 * @ejb:home remote-class="com.ejb.txn.ApRepAgingSummaryControllerHome"
 *           extends="javax.ejb.EJBHome"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="apuser"
 *                        role-link="apuserlink"
 *
 * @ejb:permission role-name="apuser"
 * 
*/

public class ApRepAgingSummaryControllerBean extends AbstractSessionBean {
	
	/**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList getApScAll(Integer AD_CMPNY) {
                    
        Debug.print("ApRepAgingSummaryControllerBean getApScAll");
        
        LocalApSupplierClassHome apSupplierClassHome = null;

        ArrayList list = new ArrayList();
        
        // Initialize EJB Home
        
        try {
            
            apSupplierClassHome = (LocalApSupplierClassHome)EJBHomeFactory.
                lookUpLocalHome(LocalApSupplierClassHome.JNDI_NAME, LocalApSupplierClassHome.class);
            
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {
            
            Collection apSupplierClasses = apSupplierClassHome.findEnabledScAll(AD_CMPNY);

	        Iterator i = apSupplierClasses.iterator();
	               
	        while (i.hasNext()) {
	        	
	        	LocalApSupplierClass apSupplierClass = (LocalApSupplierClass)i.next();
	        	
	        	list.add(apSupplierClass.getScName());
	        	
	        }
	        
	        return list;
            
        } catch (Exception ex) {
        	
        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());
        	
        }
            
    }

    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList getApStAll(Integer AD_CMPNY) {
                    
        Debug.print("ApRepAgingSummaryControllerBean getApStAll");
        
        LocalApSupplierTypeHome apSupplierTypeHome = null;

        ArrayList list = new ArrayList();
        
        // Initialize EJB Home
        
        try {
            
            apSupplierTypeHome = (LocalApSupplierTypeHome)EJBHomeFactory.
                lookUpLocalHome(LocalApSupplierTypeHome.JNDI_NAME, LocalApSupplierTypeHome.class);
            
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {
            
            Collection apSupplierTypes = apSupplierTypeHome.findEnabledStAll(AD_CMPNY);

	        Iterator i = apSupplierTypes.iterator();
	               
	        while (i.hasNext()) {
	        	
	        	LocalApSupplierType apSupplierType = (LocalApSupplierType)i.next();
	        	
	        	list.add(apSupplierType.getStName());
	        	
	        }
	        
	        return list;
            
        } catch (Exception ex) {
        	
        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());
        	
        }
            
    }
    
    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList executeApRepAgingSummary(HashMap criteria, String AGNG_BY, String GROUP_BY,
        ArrayList adBrnchList, String currency, Integer AD_CMPNY) throws
        GlobalNoRecordFoundException {
                    
        Debug.print("ApRepAgingSummaryControllerBean executeApRepAgingSummary");
        
        LocalApVoucherPaymentScheduleHome apVoucherPaymentScheduleHome = null;
        LocalApVoucherHome apVoucherHome = null;
        LocalApAppliedVoucherHome apAppliedVoucherHome = null;
        LocalAdCompanyHome adCompanyHome = null;
        LocalAdPreferenceHome adPreferenceHome = null;
        
        ArrayList list = new ArrayList();
        
        //initialized EJB Home
        
        try {
            
            apVoucherPaymentScheduleHome = (LocalApVoucherPaymentScheduleHome)EJBHomeFactory.
                lookUpLocalHome(LocalApVoucherPaymentScheduleHome.JNDI_NAME, LocalApVoucherPaymentScheduleHome.class);
            apVoucherHome = (LocalApVoucherHome)EJBHomeFactory.
            	lookUpLocalHome(LocalApVoucherHome.JNDI_NAME, LocalApVoucherHome.class);
            apAppliedVoucherHome = (LocalApAppliedVoucherHome)EJBHomeFactory.
        		lookUpLocalHome(LocalApAppliedVoucherHome.JNDI_NAME, LocalApAppliedVoucherHome.class);	
            adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
            adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
        		lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);
    
        } catch (NamingException ex) 	{
            
            throw new EJBException(ex.getMessage());
            
        }
        
		try { 
		
		  LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
		  short precisionUnit = adCompany.getGlFunctionalCurrency().getFcPrecision();
		  Date agingDate = null;
		  
		  LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(adCompany.getCmpCode());
		  int agingBucket1 = adPreference.getPrfApAgingBucket();
		  int agingBucket2 = adPreference.getPrfApAgingBucket() * 2;
		  int agingBucket3 = adPreference.getPrfApAgingBucket() * 3;
		  int agingBucket4 = adPreference.getPrfApAgingBucket() * 4;
		   
		  StringBuffer jbossQl = new StringBuffer();
		  jbossQl.append("SELECT OBJECT(vps) FROM ApVoucherPaymentSchedule vps ");
		  
	      boolean firstArgument = true;
	      short ctr = 0;
		  int criteriaSize = criteria.size();	      
	      
	      Object obj[];	      
		
		  // Allocate the size of the object parameter
		
	      if (criteria.containsKey("supplierCode")) {
	      	
	      	 criteriaSize--;
	      	 
	      }

          if (criteria.containsKey("includeUnpostedTransaction")) {
	      	
	      	 criteriaSize--;
	      	 
	      }
	      
	      if (criteria.containsKey("includePaid")) {
	      	
	      	 criteriaSize--;
	      	 
	      }
	      	      
	      obj = new Object[criteriaSize];
		       	      
		  if (criteria.containsKey("supplierCode")) {
		  	
		  	 if (!firstArgument) {
		  	 
		  	    jbossQl.append("AND ");	
		  	 	
		     } else {
		     	
		     	firstArgument = false;
		     	jbossQl.append("WHERE ");
		     	
		     }
		     
		  	 jbossQl.append("vps.apVoucher.apSupplier.splSupplierCode LIKE '%" + (String)criteria.get("supplierCode") + "%' ");
		  	 
		  }

		  if (criteria.containsKey("supplierType")) {
	   	
		   	  if (!firstArgument) {		       	  	
		   	     jbossQl.append("AND ");		       	     
		   	  } else {		       	  	
		   	  	 firstArgument = false;
		   	  	 jbossQl.append("WHERE ");		       	  	 
		   	  }
		   	  
		   	  jbossQl.append("vps.apVoucher.apSupplier.apSupplierType.stName=?" + (ctr+1) + " ");
		   	  obj[ctr] = (String)criteria.get("supplierType");
		   	  ctr++;
	   	  
	      }	
			
		  if (criteria.containsKey("supplierClass")) {
	   	
		   	  if (!firstArgument) {		       	  	
		   	     jbossQl.append("AND ");		       	     
		   	  } else {		       	  	
		   	  	 firstArgument = false;
		   	  	 jbossQl.append("WHERE ");		       	  	 
		   	  }
		   	  
		   	  jbossQl.append("vps.apVoucher.apSupplier.apSupplierClass.scName=?" + (ctr+1) + " ");
		   	  obj[ctr] = (String)criteria.get("supplierClass");
		   	  ctr++;
	   	  
	      }		          
          			
	      if (criteria.containsKey("date")) {
		      	
		  	 if (!firstArgument) {
		  	 	jbossQl.append("AND ");
		  	 } else {
		  	 	firstArgument = false;
		  	 	jbossQl.append("WHERE ");
		  	 }
		  	 jbossQl.append("vps.apVoucher.vouDate<=?" + (ctr+1) + " ");
		  	 obj[ctr] = (Date)criteria.get("date");
		  	 agingDate = (Date)criteria.get("date");
		  	 ctr++;
		  }  		      		  
		  		  
	      if (criteria.containsKey("includeUnpostedTransaction")) {
	          
	          String unposted = (String)criteria.get("includeUnpostedTransaction");
	          
	          if (!firstArgument) {
		       	  	
	       	     jbossQl.append("AND ");
	       	     
	       	  } else {
	       	  	
	       	  	 firstArgument = false;
	       	  	 jbossQl.append("WHERE ");
	       	  	 
	       	  }	       	  
	                  	
	       	  if (unposted.equals("NO")) {
	       			       	  		       	  
		       	  jbossQl.append("vps.apVoucher.vouPosted = 1 ");
		       	  
	       	  } else {
	       	  	
	       	      jbossQl.append("vps.apVoucher.vouVoid = 0 ");  	  
	       	  	
	       	  }   	 
	       	  	       	  
	      }	
	      
	      if(adBrnchList.isEmpty()) {
	      	
	      	throw new GlobalNoRecordFoundException();
	      	
	      } else {    
	      	
	          if (!firstArgument) {
	              
	              jbossQl.append("AND ");
	              
	          } else {
	              
	              firstArgument = false;
	              jbossQl.append("WHERE ");
	              
	          }	      
	          
	          jbossQl.append("vps.apVoucher.vouAdBranch in (");
	          
	          boolean firstLoop = true;
	          
	          Iterator i = adBrnchList.iterator();
	          
	          while(i.hasNext()) {
	              
	              if(firstLoop == false) { 
	                  jbossQl.append(", "); 
	              }
	              else { 
	                  firstLoop = false; 
	              }
	              
	              AdBranchDetails mdetails = (AdBranchDetails) i.next();
	              
	              jbossQl.append(mdetails.getBrCode());
	              
	          }
	          
	          jbossQl.append(") ");
	          
	          firstArgument = false;
	          
	      }
	      
	      if (!firstArgument) {
		       	  	
	   	     jbossQl.append("AND ");
	   	     
	   	  } else {
	   	  	
	   	  	 firstArgument = false;
	   	  	 jbossQl.append("WHERE ");
	   	  	 
	   	  }	     
	   	  
		  jbossQl.append("vps.apVoucher.vouDebitMemo = 0 AND vps.vpsAdCompany=" + AD_CMPNY);
		  		  		   
		  String includePaid = (String)criteria.get("includePaid");
		  
		  Collection apVoucherPaymentSchedules = apVoucherPaymentScheduleHome.getVpsByCriteria(jbossQl.toString(), obj);	         
		  
		  if (apVoucherPaymentSchedules.size() == 0)
		     throw new GlobalNoRecordFoundException();		  
		  		  
		  Iterator i = apVoucherPaymentSchedules.iterator();
		  		     
		  while (i.hasNext()) {
		  	
		  	  LocalApVoucherPaymentSchedule apVoucherPaymentSchedule = (LocalApVoucherPaymentSchedule)i.next();   	  
		  	  		  	
		  	  LocalApVoucher apVoucher = apVoucherPaymentSchedule.getApVoucher();
		  	  
		  	  ApRepAgingSummaryDetails mdetails = new ApRepAgingSummaryDetails();
		  	  		  	  		  	  
		  	  mdetails.setAgSupplierCode( apVoucher.getApSupplier().getSplName());		  	  		  	  		  	  
		  	  mdetails.setAgSupplierClass(apVoucher.getApSupplier().getApSupplierClass().getScName());
		  	  mdetails.setAgDescription(apVoucher.getVouDescription());
		  	  if(apVoucher.getApSupplier().getApSupplierType() == null ) {

		  		  mdetails.setAgSupplierType("UNDEFINE");
		  	  } else {

		  	  		mdetails.setAgSupplierType(apVoucher.getApSupplier().getApSupplierType().getStName());
		  	  		
		  	  }
		  	  mdetails.setGroupBy(GROUP_BY);
		  	  		  	  
		  	  double AMNT_DUE = 0d;		  	  
		  	  		  	  
		  	  if (includePaid.equals("NO")) {
		  	  	
		  	  	    // get future dm
		  	  	
		  	  	   double DM_AMNT = 0d;
		  	  	
		  	  	   Collection apDebitMemos = apVoucherHome.findByVouDebitMemoAndVouDmVoucherNumberAndVouVoidAndVouPosted(EJBCommon.TRUE, apVoucher.getVouDocumentNumber(), EJBCommon.FALSE, EJBCommon.TRUE, AD_CMPNY);
		  	  	    
		  	  	   Iterator apDmIter = apDebitMemos.iterator();
		  	  	    
		  	  	   while (apDmIter.hasNext()) {
		  	  	    	
		  	  	    	LocalApVoucher apDebitMemo = (LocalApVoucher) apDmIter.next();
		  	  	    	
		  	  	    	if (apDebitMemo.getVouDate().after(agingDate)) {
		  	  	    		
		  	  	        	DM_AMNT += EJBCommon.roundIt(apDebitMemo.getVouBillAmount() * (apVoucherPaymentSchedule.getVpsAmountDue() / apVoucher.getVouAmountDue()), precisionUnit);
		  	  	        	
		  	  	        } 
		  	  	    	
		  	  	    }
		  	  	    
		  	  	    // get future checks
		  	  	   
		  	  	    double CHK_AMNT = 0d;
		  	  	    
		  	  	    Collection apAppliedVouchers = apAppliedVoucherHome.findPostedAvByVpsCode(apVoucherPaymentSchedule.getVpsCode(), AD_CMPNY);
		  	  	    
		  	  	    Iterator apAvIter = apAppliedVouchers.iterator();
					
		  	  	    while (apAvIter.hasNext()) {
		  	  	    	
		  	  	    	LocalApAppliedVoucher apAppliedVoucher = (LocalApAppliedVoucher) apAvIter.next();
		
		  	  	    	if (apAppliedVoucher.getApCheck().getChkDate().after(agingDate)) {
		  	  	    	
		  	  	    		CHK_AMNT += apAppliedVoucher.getAvApplyAmount() + apAppliedVoucher.getAvDiscountAmount() + apAppliedVoucher.getAvTaxWithheld();
		  	  	    		
		  	  	    	}
		  	  	    	
		  	  	    }
		  	  	    		
		  	  	    if(currency.equals("USD") && apVoucher.getGlFunctionalCurrency().getFcName().equals("USD")){

		  	  	    	AMNT_DUE=apVoucherPaymentSchedule.getVpsAmountDue();

		  	  	    }else{
			  	  	    AMNT_DUE = this.convertForeignToFunctionalCurrency(apVoucher.getGlFunctionalCurrency().getFcCode(),
		  	  	    			apVoucher.getGlFunctionalCurrency().getFcName(),
		  	  	    			apVoucher.getVouConversionDate(),
		  	  	    			apVoucher.getVouConversionRate(),
		  	  	    			apVoucherPaymentSchedule.getVpsAmountDue() -  apVoucherPaymentSchedule.getVpsAmountPaid() + DM_AMNT + CHK_AMNT, AD_CMPNY);

		  	  	    }
		  	  	    	
		  	  	   
		  	  		
			  	    if (AMNT_DUE == 0) continue;
			  	      
		  	  } else {
		  		if(currency.equals("USD") && apVoucher.getGlFunctionalCurrency().getFcName().equals("USD")){
		  			
		  			AMNT_DUE=apVoucherPaymentSchedule.getVpsAmountDue();
		  			
		  		}else{
		  			AMNT_DUE = this.convertForeignToFunctionalCurrency(apVoucher.getGlFunctionalCurrency().getFcCode(),
					  	      apVoucher.getGlFunctionalCurrency().getFcName(),
					  	      apVoucher.getVouConversionDate(),
					  	      apVoucher.getVouConversionRate(),
					  	      apVoucherPaymentSchedule.getVpsAmountDue(), AD_CMPNY);
		  			
		  		}
		  	  	   
		  	  	
		  	  }
		  	  
		  	  
		  	  		  	  		  	  	
		  	  mdetails.setAgAmount(AMNT_DUE);		  	  		  	  
		  	  		  	  
		  	  int VOUCHER_AGE = 0;
		  	  
		  	  if (AGNG_BY.equals("DUE DATE")) {
		  	  	
		  	  	  VOUCHER_AGE = (short)((agingDate.getTime() -
			            apVoucherPaymentSchedule.getVpsDueDate().getTime()) / (1000 * 60 * 60 * 24));
		  	  	
		  	  }	else {
		  	  	  
		  	  	  VOUCHER_AGE = (short)((agingDate.getTime() -
			            apVoucher.getVouDate().getTime()) / (1000 * 60 * 60 * 24));       				        				
		  	  	
		  	  }
		  	  
		  	  if (VOUCHER_AGE <= 0) {
		  	  	
		  	  	   mdetails.setAgBucket0(AMNT_DUE);
		  	  	
		  	  } else if (VOUCHER_AGE >= 1 && VOUCHER_AGE <= agingBucket1) {
		  	  	
		  	  	   mdetails.setAgBucket1(AMNT_DUE);
		  	  	
		  	  } else if (VOUCHER_AGE >= (agingBucket1 + 1) && VOUCHER_AGE <= agingBucket2) {
		  	  	
		  	  	   mdetails.setAgBucket2(AMNT_DUE);
		  	  	
		  	  } else if (VOUCHER_AGE >= (agingBucket2 + 1) && VOUCHER_AGE <= agingBucket3) {
		  	  	
		  	  	   mdetails.setAgBucket3(AMNT_DUE);
		  	  	
		  	  } else if (VOUCHER_AGE >= (agingBucket3 + 1) && VOUCHER_AGE <= agingBucket4) {
		  	  	
		  	  	   mdetails.setAgBucket4(AMNT_DUE);
		  	  	
		  	  } else if (VOUCHER_AGE > agingBucket4) {
		  	  	
		  	  	   mdetails.setAgBucket5(AMNT_DUE);
		  	  	   
		  	  }		  	  
		  	  			      	  	
		  	  mdetails.setAgVouFcSymbol(apVoucher.getGlFunctionalCurrency().getFcSymbol());
		  	
			  list.add(mdetails);	
			  
			  if (includePaid.equals("YES") && apVoucherPaymentSchedule.getVpsAmountPaid() != 0) {
		 
			     // get dm on or after aging date
		  	  	
		  	  	 double DM_AMNT = 0d;
		
		  	  	 Collection apDebitMemos = apVoucherHome.findByVouDebitMemoAndVouDmVoucherNumberAndVouVoidAndVouPosted(EJBCommon.TRUE, apVoucher.getVouDocumentNumber(), EJBCommon.FALSE, EJBCommon.TRUE, AD_CMPNY);
		
		  	  	 Iterator apDmIter = apDebitMemos.iterator();
		  	  	 
		  	  	 while (apDmIter.hasNext()) {
		  	  	 	
		  	  	 	LocalApVoucher apDebitMemo = (LocalApVoucher) apDmIter.next();
		  	  	 	
		  	  	 	mdetails = new ApRepAgingSummaryDetails();
		  	  	 	mdetails.setAgSupplierCode(apVoucher.getApSupplier().getSplName());
		  	  	 	mdetails.setAgSupplierClass(apVoucher.getApSupplier().getApSupplierClass().getScName());
		  	  	 	mdetails.setAgDescription(apVoucher.getVouDescription());
		  	  	 	if(apVoucher.getApSupplier().getApSupplierType() == null ) {
				  	  	
				  		mdetails.setAgSupplierType("UNDEFINE");
				  		
				  	} else {
				  	  	
				  		mdetails.setAgSupplierType(apVoucher.getApSupplier().getApSupplierType().getStName());
				  	  		
				  	}	
				  	mdetails.setGroupBy(GROUP_BY);
		  	  	 			  	  	 	
		  	  	 	if (apDebitMemo.getVouDate().before(agingDate) || apDebitMemo.getVouDate().equals(agingDate)) {		  	  	 	
		  	  	 	      
			  	  	 	if(currency.equals("USD") && apVoucher.getGlFunctionalCurrency().getFcName().equals("USD")){
	
			  	  	 		DM_AMNT=(EJBCommon.roundIt(apDebitMemo.getVouBillAmount() * (apVoucherPaymentSchedule.getVpsAmountDue() / apVoucher.getVouAmountDue()), precisionUnit)) * -1;
	
			  	  	 	}else{
			  	  	 		DM_AMNT = this.convertForeignToFunctionalCurrency(apVoucher.getGlFunctionalCurrency().getFcCode(),
			  	  	 				apVoucher.getGlFunctionalCurrency().getFcName(),
			  	  	 				apVoucher.getVouConversionDate(),
			  	  	 				apVoucher.getVouConversionRate(),
			  	  	 				EJBCommon.roundIt(apDebitMemo.getVouBillAmount() * (apVoucherPaymentSchedule.getVpsAmountDue() / apVoucher.getVouAmountDue()), precisionUnit), AD_CMPNY) * -1;

			  	  	 	}
		  	  	 		
		  	  	 		mdetails.setAgAmount(DM_AMNT);
		  	  	 		
		  	  	 		if (VOUCHER_AGE <= 0) {
		  	  	 			
		  	  	 			mdetails.setAgBucket0(DM_AMNT);
		  	  	 			
		  	  	 		} else if (VOUCHER_AGE >= 1 && VOUCHER_AGE <= agingBucket1) {
		  	  	 			
		  	  	 			mdetails.setAgBucket1(DM_AMNT);
		  	  	 			
		  	  	 		} else if (VOUCHER_AGE >= (agingBucket1 + 1) && VOUCHER_AGE <= agingBucket2) {
		  	  	 			
		  	  	 			mdetails.setAgBucket2(DM_AMNT);
		  	  	 			
		  	  	 		} else if (VOUCHER_AGE >= (agingBucket2 + 1) && VOUCHER_AGE <= agingBucket3) {
		  	  	 			
		  	  	 			mdetails.setAgBucket3(DM_AMNT);
		  	  	 			
		  	  	 		} else if (VOUCHER_AGE >= (agingBucket3 + 1) && VOUCHER_AGE <= agingBucket4) {
		  	  	 			
		  	  	 			mdetails.setAgBucket4(DM_AMNT);
		  	  	 			
		  	  	 		} else if (VOUCHER_AGE > agingBucket4) {
		  	  	 			
		  	  	 			mdetails.setAgBucket5(DM_AMNT);
		  	  	 			
		  	  	 		}  	 
		  	  	 	mdetails.setAgVouFcSymbol(apVoucher.getGlFunctionalCurrency().getFcSymbol());
		  	  	 		list.add(mdetails);
		
		  	  	 	}
		
		  	  	 }
		
		  	  	  // get checks on or before aging date
		  	  	   
		  	  	  double CHK_AMNT = 0d;
		
		  	  	  Collection apAppliedVouchers = apAppliedVoucherHome.findPostedAvByVpsCode(apVoucherPaymentSchedule.getVpsCode(), AD_CMPNY);
		  	  	    
		  	  	  Iterator apAvIter = apAppliedVouchers.iterator();
					
		  	  	  while (apAvIter.hasNext()) {
		  	  	    	
		  	  	      LocalApAppliedVoucher apAppliedVoucher = (LocalApAppliedVoucher) apAvIter.next();
		  	  	      
		  	  	      mdetails = new ApRepAgingSummaryDetails();
		  	  	      mdetails.setAgSupplierCode(apVoucher.getApSupplier().getSplName());
		  	  	      mdetails.setAgSupplierClass(apVoucher.getApSupplier().getApSupplierClass().getScName());
		  	  	      mdetails.setAgDescription(apVoucher.getVouDescription());
		  	  	      if(apVoucher.getApSupplier().getApSupplierType() == null ) {
				  	  	
		  	  	      		mdetails.setAgSupplierType("UNDEFINE");
		  	  	      		
		  	  	      } else {
				  	  	
		  	  	      		mdetails.setAgSupplierType(apVoucher.getApSupplier().getApSupplierType().getStName());
				  	  		
		  	  	      }	
		  	  	      mdetails.setGroupBy(GROUP_BY);
		  	  	    		  	  	      
		  	  	      if (apAppliedVoucher.getApCheck().getChkDate().before(agingDate) || apAppliedVoucher.getApCheck().getChkDate().equals(agingDate)) {
		
		  	  	    	if(currency.equals("USD") && apVoucher.getGlFunctionalCurrency().getFcName().equals("USD")){
		  	  	    	
			  	  	 		DM_AMNT=(apAppliedVoucher.getAvApplyAmount() + apAppliedVoucher.getAvDiscountAmount() + apAppliedVoucher.getAvTaxWithheld()) * -1;
	
		  	  	    	}else{
		  	  	    		CHK_AMNT = this.convertForeignToFunctionalCurrency(apVoucher.getGlFunctionalCurrency().getFcCode(),
		  	  	    				apVoucher.getGlFunctionalCurrency().getFcName(),
		  	  	    				apVoucher.getVouConversionDate(),
		  	  	    				apVoucher.getVouConversionRate(),
		  	  	    				apAppliedVoucher.getAvApplyAmount() + apAppliedVoucher.getAvDiscountAmount() + apAppliedVoucher.getAvTaxWithheld(), AD_CMPNY) * -1;

		  	  	    	}
		  	  	      	
		  	  	      	mdetails.setAgAmount(CHK_AMNT);
		  	  	      	
		  	  	      	if (VOUCHER_AGE <= 0) {
		  	  	      		
		  	  	      		mdetails.setAgBucket0(CHK_AMNT);
		  	  	      		
		  	  	      	} else if (VOUCHER_AGE >= 1 && VOUCHER_AGE <= agingBucket1) {
		  	  	      		
		  	  	      		mdetails.setAgBucket1(CHK_AMNT);
		  	  	      		
		  	  	      	} else if (VOUCHER_AGE >= (agingBucket1 + 1) && VOUCHER_AGE <= agingBucket2) {
		  	  	      		
		  	  	      		mdetails.setAgBucket2(CHK_AMNT);
		  	  	      		
		  	  	      	} else if (VOUCHER_AGE >= (agingBucket2 + 1) && VOUCHER_AGE <= agingBucket3) {
		  	  	      		
		  	  	      		mdetails.setAgBucket3(CHK_AMNT);
		  	  	      		
		  	  	      	} else if (VOUCHER_AGE >= (agingBucket3 + 1) && VOUCHER_AGE <= agingBucket4) {
		  	  	      		
		  	  	      		mdetails.setAgBucket4(CHK_AMNT);
		  	  	      		
		  	  	      	} else if (VOUCHER_AGE > agingBucket4) {
		  	  	      		
		  	  	      		mdetails.setAgBucket5(CHK_AMNT);
		  	  	      		
		  	  	      	}  	 
		  	  	    mdetails.setAgVouFcSymbol(apVoucher.getGlFunctionalCurrency().getFcSymbol());
		  	  	    
		  	  	      	list.add(mdetails);
		  	  	      	
		  	  	      }
		
		  	  	  }
		
			  }
			         
		  }
		  			  
		  if (list.isEmpty()) {
		  	
		  	  throw new GlobalNoRecordFoundException();
		  	
		  }
		  
		  if(GROUP_BY.equals("SUPPLIER CODE")) {
		  	
		  		Collections.sort(list, ApRepAgingSummaryDetails.SupplierCodeComparator);
		  		
		  }else if(GROUP_BY.equals("SUPPLIER TYPE")) {
		  		
		  		Collections.sort(list, ApRepAgingSummaryDetails.SupplierTypeComparator);
		  		
		  }else if(GROUP_BY.equals("SUPPLIER CLASS")) {
		  	
		  		Collections.sort(list, ApRepAgingSummaryDetails.SupplierClassComparator);
		  		
		  }
		  
		  return list;
		      
	  } catch (GlobalNoRecordFoundException ex) {
	  	 
	  	  throw ex;
	  	
	  } catch (Exception ex) {
	  	
	  	  Debug.printStackTrace(ex);
	  	  throw new EJBException(ex.getMessage());
	  	
	  }
        
    }
    
	/**
	* @ejb:interface-method view-type="remote"
	* @jboss:method-attributes read-only="true"
	**/
	public com.util.AdCompanyDetails getAdCompany(Integer AD_CMPNY) {
	
	  Debug.print("ApRepAgingSummaryControllerBean getAdCompany");      
	  
	  LocalAdCompanyHome adCompanyHome = null;
	        
	  // Initialize EJB Home
	    
	  try {
	        
	       adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
	           lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
	        
	  } catch (NamingException ex) {
	        
	      throw new EJBException(ex.getMessage());
	        
	  }
	
	  try {
	      
	     LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
	     
	     AdCompanyDetails details = new AdCompanyDetails();
	     details.setCmpName(adCompany.getCmpName());
	     
	     return details;  	
	     
	  } catch (Exception ex) {
	      
	      Debug.printStackTrace(ex);
	      throw new EJBException(ex.getMessage());
	      
	  }
	  
	}  
	
	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/   
	public ArrayList getAdBrResAll(Integer RS_CODE, Integer AD_CMPNY) 
		throws GlobalNoRecordFoundException{
	    
	    Debug.print("ApRepAgingSummaryControllerBean getAdBrResAll");
	    
	    LocalAdBranchResponsibilityHome adBranchResponsibilityHome = null;
	    LocalAdBranchHome adBranchHome = null;
	    
	    LocalAdBranchResponsibility adBranchResponsibility = null;
	    LocalAdBranch adBranch = null;
	    
	    Collection adBranchResponsibilities = null;
	    
	    ArrayList list = new ArrayList();
	    
	    // Initialize EJB Home
	    
	    try {
	        
	        adBranchResponsibilityHome = (LocalAdBranchResponsibilityHome)EJBHomeFactory.
	        lookUpLocalHome(LocalAdBranchResponsibilityHome.JNDI_NAME, LocalAdBranchResponsibilityHome.class);
	        adBranchHome = (LocalAdBranchHome)EJBHomeFactory.
	        lookUpLocalHome(LocalAdBranchHome.JNDI_NAME, LocalAdBranchHome.class);
	        
	    } catch (NamingException ex) {
	        
	        throw new EJBException(ex.getMessage());
	        
	    }
	    
	    try {
	        
	        adBranchResponsibilities = adBranchResponsibilityHome.findByAdResponsibility(RS_CODE, AD_CMPNY);
	        
	    } catch (FinderException ex) {
	        
	    } catch (Exception ex) {
	        
	        throw new EJBException(ex.getMessage());
	    }
	    
	    if (adBranchResponsibilities.isEmpty()) {
	        
	        throw new GlobalNoRecordFoundException();
	        
	    }
	    
	    try {
	        
	        Iterator i = adBranchResponsibilities.iterator();
	        
	        while(i.hasNext()) {
	            
	            adBranchResponsibility = (LocalAdBranchResponsibility)i.next();
	            
	            adBranch = adBranchResponsibility.getAdBranch();
	            
	            AdBranchDetails details = new AdBranchDetails();
	            
	            details.setBrCode(adBranch.getBrCode());
	            details.setBrBranchCode(adBranch.getBrBranchCode());
	            details.setBrName(adBranch.getBrName());
	            details.setBrHeadQuarter(adBranch.getBrHeadQuarter());
	            
	            list.add(details);
	            
	        }	               
	        
	    } catch (Exception ex) {
	        
	        throw new EJBException(ex.getMessage());
	    }
	    
	    return list;
	    
	}
	
	/**
	* @ejb:interface-method view-type="remote"
	* @jboss:method-attributes read-only="true"
	**/
	public com.util.AdPreferenceDetails getAdPreference(Integer AD_CMPNY) {
	
	  Debug.print("ApRepAgingSummaryControllerBean getAdPreference");      
	  
	  LocalAdPreferenceHome adPreferenceHome = null;
	        
	  // Initialize EJB Home
	    
	  try {
	        
		  adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
	           lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);
	        
	  } catch (NamingException ex) {
	        
	      throw new EJBException(ex.getMessage());
	        
	  }
	
	  try {
	      
	     LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);
	     
	     AdPreferenceDetails details = new AdPreferenceDetails();
	     details.setPrfApAgingBucket(adPreference.getPrfApAgingBucket());
	     details.setPrfArAgingBucket(adPreference.getPrfArAgingBucket());
	     
	     return details;  	
	  	       	
	  } catch (Exception ex) {
	  	
	  	 Debug.printStackTrace(ex);
	  	 throw new EJBException(ex.getMessage());
	  	  
	  }
	  
	} 
	
	// private methods
	private double convertForeignToFunctionalCurrency(Integer FC_CODE, String FC_NM, 
	    Date CONVERSION_DATE, double CONVERSION_RATE, double AMOUNT, Integer AD_CMPNY) {
	    	
	    Debug.print("ApRepAgingSummaryControllerBean convertForeignToFunctionalCurrency");
	    
	    
        LocalGlFunctionalCurrencyRateHome glFunctionalCurrencyRateHome = null;
        LocalAdCompanyHome adCompanyHome = null;
         
        LocalAdCompany adCompany = null;
                 
        // Initialize EJB Homes
         
        try {
         	
            glFunctionalCurrencyRateHome = (LocalGlFunctionalCurrencyRateHome)EJBHomeFactory.
               lookUpLocalHome(LocalGlFunctionalCurrencyRateHome.JNDI_NAME, LocalGlFunctionalCurrencyRateHome.class);                         
            adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
               lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);             
                   
         } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
         }
         
         // get company and extended precision
         
         try {
         	
             adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
         	             
         } catch (Exception ex) {
         	
             throw new EJBException(ex.getMessage());
         	
         }	     
        
         
         // Convert to functional currency if necessary
         
         if (CONVERSION_RATE != 1 && CONVERSION_RATE != 0 ) {
         	         
             AMOUNT = AMOUNT / CONVERSION_RATE;
             	
         } 
         return EJBCommon.roundIt(AMOUNT, adCompany.getGlFunctionalCurrency().getFcPrecision());
         
	}
    
    // SessionBean methods

    /**
     * @ejb:create-method view-type="remote"
     **/
    public void ejbCreate() throws CreateException {

       Debug.print("ApRepAgingSummaryControllerBean ejbCreate");
      
    }
}
