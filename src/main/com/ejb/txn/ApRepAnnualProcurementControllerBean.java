
/* 
 * ApRepInputTaxControllerBean.java
 *
 * Created on March 16, 2004, 1:40 PM
 *
 * @author  Neil Andrew M. Ajero
 */

package com.ejb.txn;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;
import javax.naming.NamingException;

import com.ejb.ad.LocalAdBranch;
import com.ejb.ad.LocalAdBranchHome;
import com.ejb.ad.LocalAdBranchResponsibility;
import com.ejb.ad.LocalAdBranchResponsibilityHome;
import com.ejb.ad.LocalAdCompany;
import com.ejb.ad.LocalAdCompanyHome;
import com.ejb.ad.LocalAdLookUpValue;
import com.ejb.ad.LocalAdLookUpValueHome;
import com.ejb.ad.LocalAdUser;
import com.ejb.ad.LocalAdUserHome;
import com.ejb.ap.LocalApDistributionRecord;
import com.ejb.ap.LocalApDistributionRecordHome;
import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.gl.LocalGlFunctionalCurrencyRate;
import com.ejb.gl.LocalGlFunctionalCurrencyRateHome;
import com.ejb.gl.LocalGlJournalLine;
import com.ejb.gl.LocalGlJournalLineHome;
import com.ejb.gl.LocalGlTaxInterface;
import com.ejb.gl.LocalGlTaxInterfaceHome;
import com.ejb.inv.LocalInvTransactionalBudget;
import com.ejb.inv.LocalInvTransactionalBudgetHome;
import com.util.AbstractSessionBean;
import com.util.AdBranchDetails;
import com.util.AdCompanyDetails;
import com.util.ApRepAnnualProcurementDetails;
import com.util.ApRepInputTaxDetails;
import com.util.Debug;
import com.util.EJBCommon;
import com.util.EJBHomeFactory;

/**
 * @ejb:bean name="ApRepAnnualProcurementControllerEJB"
 *           display-name="Used for viewing annual procurement plan summary"
 *           type="Stateless"
 *           view-type="remote"
 *           jndi-name="ejb/ApRepAnnualProcurementControllerEJB"
 *
 * @ejb:interface remote-class="com.ejb.txn.ApRepAnnualProcurementController"
 *                extends="javax.ejb.EJBObject"
 *
 * @ejb:home remote-class="com.ejb.txn.ApRepAnnualProcurementControllerHome"
 *           extends="javax.ejb.EJBHome"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="apuser"
 *                        role-link="apuserlink"
 *
 * @ejb:permission role-name="apuser"
 * 
*/

public class ApRepAnnualProcurementControllerBean extends AbstractSessionBean {             
	
	/**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/    
    public ArrayList executeApRepAnnualProcurement(HashMap criteria, String month, ArrayList adBrnchList, Integer AD_CMPNY) 
        throws GlobalNoRecordFoundException {
                    
        Debug.print("ApRepAnnualProcurementControllerBean executeApRepAnnualProcurement");
        
        LocalInvTransactionalBudgetHome invTransactionalBudgetHome = null;
        LocalApDistributionRecordHome apDistributionRecordHome = null;                
        LocalGlTaxInterfaceHome glTaxInterfaceHome = null;        
        LocalGlJournalLineHome glJournalHome = null;                        
        LocalAdLookUpValueHome adLookUpValueHome = null;
        
        ArrayList list = new ArrayList();
        
        try {
        	        
            apDistributionRecordHome = (LocalApDistributionRecordHome)EJBHomeFactory.
				lookUpLocalHome(LocalApDistributionRecordHome.JNDI_NAME, LocalApDistributionRecordHome.class);                        
            glTaxInterfaceHome = (LocalGlTaxInterfaceHome)EJBHomeFactory.
        		lookUpLocalHome(LocalGlTaxInterfaceHome.JNDI_NAME, LocalGlTaxInterfaceHome.class);
            glJournalHome = (LocalGlJournalLineHome)EJBHomeFactory.
				lookUpLocalHome(LocalGlJournalLineHome.JNDI_NAME,LocalGlJournalLineHome.class);                        
            invTransactionalBudgetHome = (LocalInvTransactionalBudgetHome)EJBHomeFactory.
    				lookUpLocalHome(LocalInvTransactionalBudgetHome.JNDI_NAME,LocalInvTransactionalBudgetHome.class);
            adLookUpValueHome = (LocalAdLookUpValueHome)EJBHomeFactory.
    				lookUpLocalHome(LocalAdLookUpValueHome.JNDI_NAME, LocalAdLookUpValueHome.class);
            
            
        } catch (NamingException ex) 	{
            
            throw new EJBException(ex.getMessage());
            
        }                        
        
        try { 
        	
        			
			//get all available records
        	StringBuffer jbossQl = new StringBuffer();
  		    
        	jbossQl.append("SELECT OBJECT(tb) FROM InvTransactionalBudget tb ");
  		  
  	        boolean firstArgument = true;
  	        short ctr = 0;
  		    int criteriaSize = criteria.size();
  		    String department = " ";
  		    String year = null;
  	        Object obj[];	      
  		
  		    // Allocate the size of the object parameter
  		  
  		      		    
  		    if (criteria.containsKey("itemName")) {
    	      	
 	      	   criteriaSize--;
 	      	 
 	        } 
  		    /*
  		    if (criteria.containsKey("year")) {
  	      	
	      	   criteriaSize++;
	      	 
	        } 
  		    */
  		    obj = new Object[criteriaSize];
        	
  		           	        		        	
        	        	
        	if (criteria.containsKey("itemName")) {
                
                if (!firstArgument) {
                    
                    jbossQl.append("AND ");	
                    
                } else {
                    
                    firstArgument = false;
                    jbossQl.append("WHERE ");
                    
                }
                
                jbossQl.append("tb.tbName LIKE '%" + (String)criteria.get("itemName") + "%' ");
                
            }
        	/*
        	String viewMonth = null;
        	if (month.equals("January")){
        		viewMonth = "tb.tbQuantityJan";
        	}else if(month.equals("February")){
        		viewMonth = "tb.tbQuantityFeb";
        	}else if(month.equals("March")){
        		viewMonth = "tb.tbQuantityMrch";
        	}else if(month.equals("April")){
        		viewMonth = "tb.tbQuantityAprl";
        	}else if(month.equals("May")){
        		viewMonth = "tb.tbQuantityMay";
        	}else if(month.equals("June")){
        		viewMonth = "tb.tbQuantityJun";
        	}else if(month.equals("July")){
        		viewMonth = "tb.tbQuantityJul";
        	}else if(month.equals("August")){
        		viewMonth = "tb.tbQuantityAug";
        	}else if(month.equals("September")){
        		viewMonth = "tb.tbQuantitySep";
        	}else if(month.equals("October")){
        		viewMonth = "tb.tbQuantityOct";
        	}else if(month.equals("November")){
        		viewMonth = "tb.tbQuantityNov";
        	}else{
        		viewMonth = "tb.tbQuantityDec";
        	}
        	
        	if (month != null){
        		//jbossQl.append();
        	}
        	*/
        	
        	if (criteria.containsKey("department")) {
        		LocalAdLookUpValue adLkUpVl = adLookUpValueHome.findByLvName((String)criteria.get("department"), AD_CMPNY);
        		
                if (!firstArgument) {
                    
                    jbossQl.append("AND ");	
                    
                } else {
                    
                    firstArgument = false;
                    jbossQl.append("WHERE ");
                    
                }
                
                jbossQl.append("tb.tbAdLookupValue=" + adLkUpVl.getLvCode() + " ");
                obj[ctr] = (String)criteria.get("department");
  		  	   	ctr++;
  		  	   	department = (String)criteria.get("department");
            }
        	
        	if (criteria.containsKey("year")) {
		      	
 		  	   if (!firstArgument) {
 		  	 	  jbossQl.append("AND ");
 		  	   } else {
 		  	 	  firstArgument = false;
 		  	 	  jbossQl.append("WHERE ");
 		  	   }
 		  	   jbossQl.append("tb.tbYear=" + (String)criteria.get("year") + " ");
 		  	   obj[ctr] = (String)criteria.get("year");
 		  	   System.out.println(obj[ctr] + "<== obj[ctr] year");
 		  	   ctr++;
 		    }  
        	
        	year = (String)criteria.get("year");
        	System.out.println(year + "<== year");
        	System.out.println(criteriaSize + "<== criteria size");
		    /*
		    if(adBrnchList.isEmpty()) {
		    	
		    	throw new GlobalNoRecordFoundException();
		    	
		    } else {
		        
		        if (!firstArgument) {
		            
		            jbossQl.append("AND ");
		            
		        } else {
		            
		            firstArgument = false;
		            jbossQl.append("WHERE ");
		            
		        }	      
		        
		        jbossQl.append("ti.tiAdBranch in (");
		        
		        boolean firstLoop = true;
		        
		        Iterator i = adBrnchList.iterator();
		        
		        while(i.hasNext()) {
		            
		            if(firstLoop == false) { 
		                jbossQl.append(", "); 
		            }
		            else { 
		                firstLoop = false; 
		            }
		            
		            AdBranchDetails mdetails = (AdBranchDetails) i.next();
		            
		            jbossQl.append(mdetails.getBrCode());
		            
		        }
		        
		        jbossQl.append(") ");
		        
		        firstArgument = false;
		        
		    }
		     */	
		    if (!firstArgument) {
		       	  	
	   	       jbossQl.append("AND ");
	   	     
	   	    } else {
	   	  	
	   	  	   firstArgument = false;
	   	  	   jbossQl.append("WHERE ");
	   	  	 
	   	    }

		    jbossQl.append("tb.tbAdCompany=" + AD_CMPNY + " ");
		   	   	    
		    short PRECISION_UNIT = this.getGlFcPrecisionUnit(AD_CMPNY);
		    System.out.println(criteria + "<== criteria");
		    System.out.println(jbossQl.toString() + "<== query");
		    System.out.println(obj + "<== object variable");
		    System.out.println(month + "<== month");
		    Collection invTransactionalBudget = invTransactionalBudgetHome.getTbByCriteria(jbossQl.toString(), obj);
		    System.out.println(invTransactionalBudget + "<== collection transactional budget");
		    ArrayList apSupplierList = new ArrayList();		    		  
		    		            			        		        		        	
        	Iterator i = invTransactionalBudget.iterator();	        		        	
        	
        	while(i.hasNext()) {
        		
        		//
        		//LocalAdLookUpValue adLkUpVl = adLookUpValueHome.findByLvName(department, AD_CMPNY);
        		
        		LocalInvTransactionalBudget invTransactionalBudgetInterface = (LocalInvTransactionalBudget) i.next();
        		      		        		        		
        		ApRepAnnualProcurementDetails details = new ApRepAnnualProcurementDetails();	        		
    			
        		details.setApItemName(invTransactionalBudgetInterface.getTbName());
        		details.setApItemDesc(invTransactionalBudgetInterface.getTbDesc());
        		details.setApInvItemCategory(invTransactionalBudgetInterface.getInvItem().getIiAdLvCategory());
        		details.setApDepartment(department);
        		details.setApUnit(invTransactionalBudgetInterface.getInvUnitOfMeasure().getUomShortName());
        		details.setApQtyJan(invTransactionalBudgetInterface.getTbQuantityJan());
        		details.setApQtyFeb(invTransactionalBudgetInterface.getTbQuantityFeb());
        		details.setApQtyMrch(invTransactionalBudgetInterface.getTbQuantityMrch());
        		details.setApQtyAprl(invTransactionalBudgetInterface.getTbQuantityAprl());
        		details.setApQtyMay(invTransactionalBudgetInterface.getTbQuantityMay());
        		details.setApQtyJun(invTransactionalBudgetInterface.getTbQuantityJun());
        		details.setApQtyJul(invTransactionalBudgetInterface.getTbQuantityJul());
        		details.setApQtyAug(invTransactionalBudgetInterface.getTbQuantityAug());
        		details.setApQtySep(invTransactionalBudgetInterface.getTbQuantitySep());
        		details.setApQtyOct(invTransactionalBudgetInterface.getTbQuantityOct());
        		details.setApQtyNov(invTransactionalBudgetInterface.getTbQuantityNov());
        		details.setApQtyDec(invTransactionalBudgetInterface.getTbQuantityDec());
        		details.setApTotalCost(invTransactionalBudgetInterface.getTbTotalAmount());
        		details.setApYear(invTransactionalBudgetInterface.getTbYear().toString());
        		details.setApUnitCost(invTransactionalBudgetInterface.getTbAmount());
        		
        		list.add(details);
        		
        	}
	                	        	
        	if (list.isEmpty()) {
    		  	
    		  	  throw new GlobalNoRecordFoundException();
    		  	
    		}	      
        
        } catch (GlobalNoRecordFoundException ex) {
   	  	 
        	Debug.printStackTrace(ex);
        	throw ex;
   	  	
        } catch (Exception ex) {
   	  		
			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());
   	  	
   	  	}
        
        return list;        
    }
	
    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
     public short getGlFcPrecisionUnit(Integer AD_CMPNY) {

        Debug.print("ApRepAnnualProcurementControllerBean getGlFcPrecisionUnit");

       
        LocalAdCompanyHome adCompanyHome = null;
       
      
        // Initialize EJB Home
         
        try {
             
            adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
               lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);       
             
        } catch (NamingException ex) {
             
            throw new EJBException(ex.getMessage());
             
        }

        try {
        	
          LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
             
          return  adCompany.getGlFunctionalCurrency().getFcPrecision();
                                      
        } catch (Exception ex) {
        	 
        	 Debug.printStackTrace(ex);
          throw new EJBException(ex.getMessage());
          
        }

     }
    
	/**
	* @ejb:interface-method view-type="remote"
	* @jboss:method-attributes read-only="true"
	**/
	public com.util.AdCompanyDetails getAdCompany(Integer AD_CMPNY) {
	
	  Debug.print("ApRepAnnualProcurementControllerBean getAdCompany");      
	  
	  LocalAdCompanyHome adCompanyHome = null;
	        
	  // Initialize EJB Home
	    
	  try {
	        
	       adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
	           lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
	        
	  } catch (NamingException ex) {
	        
	      throw new EJBException(ex.getMessage());
	        
	  }
	
	  try {
	      
	     LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
	     
	     AdCompanyDetails details = new AdCompanyDetails();
	     details.setCmpName(adCompany.getCmpName());
	     
	     return details;  	
	  	       	
	  } catch (Exception ex) {
	  	
	  	 Debug.printStackTrace(ex);
	  	 throw new EJBException(ex.getMessage());
	  	  
	  }
	  
	}  
	
	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public ArrayList getAdLvDEPARTMENT(Integer AD_CMPNY) {
		
		Debug.print("ApRepAnnualProcurementControllerBean getAdLvDEPARTMENT");
		
		LocalAdLookUpValueHome adLookUpValueHome = null;               
		
		ArrayList list = new ArrayList();
		
		// Initialize EJB Home
		
		try {
			
			adLookUpValueHome = (LocalAdLookUpValueHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdLookUpValueHome.JNDI_NAME, LocalAdLookUpValueHome.class);
			
		} catch (NamingException ex) {
			
			throw new EJBException(ex.getMessage());
			
		}        
		
		try {
			
			Collection adLookUpValues = adLookUpValueHome.findByLuName("DEPARTMENT", AD_CMPNY);
			
			Iterator i = adLookUpValues.iterator();
			
			while (i.hasNext()) {
				
				LocalAdLookUpValue adLookUpValue = (LocalAdLookUpValue)i.next();
				
				list.add(adLookUpValue.getLvName());
				
			}
			
			return list;
			
		} catch (Exception ex) {
			
			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());
			
		}
		
	}
	/**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public String getAdUsrDeptartment(String USR_NM, Integer AD_CMPNY) {
                    
        Debug.print("ApPurchaseRequisitionEntryControllerBean getAdUsrAll");
        
        LocalAdUserHome adUserHome = null;               
        LocalAdUser adUser = null;
        ArrayList list = new ArrayList();
        
        // Initialize EJB Home
        
        try {
            
            adUserHome = (LocalAdUserHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdUserHome.JNDI_NAME, LocalAdUserHome.class);
            
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }        
        
        try {
        	
        	//Collection adUsers = adUserHome.findUsrByDepartment(USR_DEPT, AD_CMPNY);
        	adUser = adUserHome.findByUsrName(USR_NM, AD_CMPNY);
        	String department = adUser.getUsrDept();
        	
        	return department;
        	
        } catch (Exception ex) {
        	
        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());
        	
        }
            
    }
	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/   
	public ArrayList getAdBrResAll(Integer RS_CODE, Integer AD_CMPNY) 
		throws GlobalNoRecordFoundException{
	    
	    Debug.print("ApRepAnnualProcurementControllerBean getAdBrResAll");
	    
	    LocalAdBranchResponsibilityHome adBranchResponsibilityHome = null;
	    LocalAdBranchHome adBranchHome = null;
	    
	    LocalAdBranchResponsibility adBranchResponsibility = null;
	    LocalAdBranch adBranch = null;
	    
	    Collection adBranchResponsibilities = null;
	    
	    ArrayList list = new ArrayList();
	    
	    // Initialize EJB Home
	    
	    try {
	        
	        adBranchResponsibilityHome = (LocalAdBranchResponsibilityHome)EJBHomeFactory.
	        lookUpLocalHome(LocalAdBranchResponsibilityHome.JNDI_NAME, LocalAdBranchResponsibilityHome.class);
	        adBranchHome = (LocalAdBranchHome)EJBHomeFactory.
	        lookUpLocalHome(LocalAdBranchHome.JNDI_NAME, LocalAdBranchHome.class);
	        
	    } catch (NamingException ex) {
	        
	        throw new EJBException(ex.getMessage());
	        
	    }
	    
	    try {
	        
	        adBranchResponsibilities = adBranchResponsibilityHome.findByAdResponsibility(RS_CODE, AD_CMPNY);
	        
	    } catch (FinderException ex) {
	        
	    } catch (Exception ex) {
	        
	        throw new EJBException(ex.getMessage());
	    }
	    
	    if (adBranchResponsibilities.isEmpty()) {
	        
	        throw new GlobalNoRecordFoundException();
	        
	    }
	    
	    try {
	        
	        Iterator i = adBranchResponsibilities.iterator();
	        
	        while(i.hasNext()) {
	            
	            adBranchResponsibility = (LocalAdBranchResponsibility)i.next();
	            
	            adBranch = adBranchResponsibility.getAdBranch();
	            
	            AdBranchDetails details = new AdBranchDetails();
	            
	            details.setBrCode(adBranch.getBrCode());
	            details.setBrBranchCode(adBranch.getBrBranchCode());
	            details.setBrName(adBranch.getBrName());
	            details.setBrHeadQuarter(adBranch.getBrHeadQuarter());
	            
	            list.add(details);
	            
	        }	               
	        
	    } catch (Exception ex) {
	        
	        throw new EJBException(ex.getMessage());
	    }
	    
	    return list;
	    
	}    
		
	private double convertForeignToFunctionalCurrency(Integer FC_CODE, String FC_NM, 
	    Date CONVERSION_DATE, double CONVERSION_RATE, double AMOUNT, Integer AD_CMPNY) {
	    	  
	    
        LocalGlFunctionalCurrencyRateHome glFunctionalCurrencyRateHome = null;
        LocalAdCompanyHome adCompanyHome = null;
         
        LocalAdCompany adCompany = null;
                 
        // Initialize EJB Homes
         
        try {
         	
            glFunctionalCurrencyRateHome = (LocalGlFunctionalCurrencyRateHome)EJBHomeFactory.
               lookUpLocalHome(LocalGlFunctionalCurrencyRateHome.JNDI_NAME, LocalGlFunctionalCurrencyRateHome.class);                         
            adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
               lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);             
                   
         } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
         }
         
         // get company and extended precision
         
         try {
         	
             adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
         	             
         } catch (Exception ex) {
         	
             throw new EJBException(ex.getMessage());
         	
         }	     
        
         
         // Convert to functional currency if necessary
         
         if (CONVERSION_RATE != 1 && CONVERSION_RATE != 0 ) {
         	         
             AMOUNT = AMOUNT / CONVERSION_RATE;
             	
         }
         return EJBCommon.roundIt(AMOUNT, adCompany.getGlFunctionalCurrency().getFcPrecision());
         
	}

    // SessionBean methods

    /**
     * @ejb:create-method view-type="remote"
     **/
    public void ejbCreate() throws CreateException {

       Debug.print("ApRepAnnualProcurementControllerBean ejbCreate");
      
    }
}
