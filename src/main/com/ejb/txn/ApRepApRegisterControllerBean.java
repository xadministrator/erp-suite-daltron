

/*
 * ApRepApRegisterControllerBean.java
 *
 * Created on February 27, 2004, 12:01 NN
 *
 * @author  Neil Andrew M. Ajero
 */

package com.ejb.txn;

import com.ejb.ad.LocalAdBankAccount;
import com.ejb.ad.LocalAdBankAccountHome;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;
import javax.naming.NamingException;

import com.ejb.ad.LocalAdBranch;
import com.ejb.ad.LocalAdBranchHome;
import com.ejb.ad.LocalAdBranchResponsibility;
import com.ejb.ad.LocalAdBranchResponsibilityHome;
import com.ejb.ad.LocalAdCompany;
import com.ejb.ad.LocalAdCompanyHome;
import com.ejb.ap.LocalApCheck;
import com.ejb.ap.LocalApCheckHome;
import com.ejb.ap.LocalApDistributionRecord;
import com.ejb.ap.LocalApDistributionRecordHome;
import com.ejb.ap.LocalApSupplier;
import com.ejb.ap.LocalApSupplierBalance;
import com.ejb.ap.LocalApSupplierBalanceHome;
import com.ejb.ap.LocalApSupplierClass;
import com.ejb.ap.LocalApSupplierClassHome;
import com.ejb.ap.LocalApSupplierHome;
import com.ejb.ap.LocalApSupplierType;
import com.ejb.ap.LocalApSupplierTypeHome;
import com.ejb.ap.LocalApVoucher;
import com.ejb.ap.LocalApVoucherBatch;
import com.ejb.ap.LocalApVoucherBatchHome;
import com.ejb.ap.LocalApVoucherHome;
import com.ejb.ap.LocalApVoucherLineItem;
import com.ejb.ap.LocalApVoucherPaymentSchedule;
import com.ejb.ap.LocalApVoucherPaymentScheduleHome;
import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.gl.LocalGlFunctionalCurrencyRate;
import com.ejb.gl.LocalGlFunctionalCurrencyRateHome;
import com.util.AbstractSessionBean;
import com.util.AdBranchDetails;
import com.util.AdCompanyDetails;
import com.util.ApRepApRegisterDetails;
import com.util.ApRepCheckRegisterDetails;
import com.util.Debug;
import com.util.EJBCommon;
import com.util.EJBHomeFactory;
import java.text.SimpleDateFormat;
import java.util.regex.Pattern;

/**
 * @ejb:bean name="ApRepApRegisterControllerEJB"
 *           display-name="Used for finding suppliers"
 *           type="Stateless"
 *           view-type="remote"
 *           jndi-name="ejb/ApRepApRegisterControllerEJB"
 *
 * @ejb:interface remote-class="com.ejb.txn.ApRepApRegisterController"
 *                extends="javax.ejb.EJBObject"
 *
 * @ejb:home remote-class="com.ejb.txn.ApRepApRegisterControllerHome"
 *           extends="javax.ejb.EJBHome"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="apuser"
 *                        role-link="apuserlink"
 *
 * @ejb:permission role-name="apuser"
 * 
*/

public class ApRepApRegisterControllerBean extends AbstractSessionBean {

    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList getApScAll(Integer AD_CMPNY) {
                    
        Debug.print("ApRepApRegisterControllerBean getApScAll");
        
        LocalApSupplierClassHome apSupplierClassHome = null;

        ArrayList list = new ArrayList();
        
        // Initialize EJB Home
        
        try {
            
            apSupplierClassHome = (LocalApSupplierClassHome)EJBHomeFactory.
                lookUpLocalHome(LocalApSupplierClassHome.JNDI_NAME, LocalApSupplierClassHome.class);
            
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {
            
            Collection apSupplierClasses = apSupplierClassHome.findEnabledScAll(AD_CMPNY);

	        Iterator i = apSupplierClasses.iterator();
	               
	        while (i.hasNext()) {
	        	
	        	LocalApSupplierClass apSupplierClass = (LocalApSupplierClass)i.next();
	        	
	        	list.add(apSupplierClass.getScName());
	        	
	        }
	        
	        return list;
            
        } catch (Exception ex) {
        	
        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());
        	
        }
            
    }

    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList getApStAll(Integer AD_CMPNY) {
                    
        Debug.print("ApRepApRegisterControllerBean getApStAll");
        
        LocalApSupplierTypeHome apSupplierTypeHome = null;

        ArrayList list = new ArrayList();
        
        // Initialize EJB Home
        
        try {
            
            apSupplierTypeHome = (LocalApSupplierTypeHome)EJBHomeFactory.
                lookUpLocalHome(LocalApSupplierTypeHome.JNDI_NAME, LocalApSupplierTypeHome.class);
            
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {
            
            Collection apSupplierTypes = apSupplierTypeHome.findEnabledStAll(AD_CMPNY);

	        Iterator i = apSupplierTypes.iterator();
	               
	        while (i.hasNext()) {
	        	
	        	LocalApSupplierType apSupplierType = (LocalApSupplierType)i.next();
	        	
	        	list.add(apSupplierType.getStName());
	        	
	        }
	        
	        return list;
            
        } catch (Exception ex) {
        	
        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());
        	
        }
            
    }
    
  
    
    /**
    * @ejb:interface-method view-type="remote"
    * @jboss:method-attributes read-only="true"
    **/
    public ArrayList getAllVouchers(HashMap criteria, ArrayList adBrnchList, Integer AD_CMPNY) 
        throws GlobalNoRecordFoundException {
     
        Debug.print("ApRepApRegisterControllerBean getAllVoucherNumber");
        
        LocalApVoucherHome apVoucherHome = null;
        LocalApVoucherPaymentScheduleHome apVoucherPaymentScheduleHome = null;
        LocalApCheckHome apCheckHome = null;
        LocalAdCompanyHome adCompanyHome = null;
        LocalApDistributionRecordHome apDistributionRecordHome = null;
        LocalApSupplierBalanceHome apSupplierBalanceHome = null;
        LocalApSupplierHome apSupplierHome = null;
        
        ArrayList list = new ArrayList();
     
        //initialized EJB Home
        
        try {

            apVoucherHome = (LocalApVoucherHome)EJBHomeFactory.
                    lookUpLocalHome(LocalApVoucherHome.JNDI_NAME, LocalApVoucherHome.class);
            apCheckHome = (LocalApCheckHome)EJBHomeFactory.
                    lookUpLocalHome(LocalApCheckHome.JNDI_NAME, LocalApCheckHome.class);
            adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
                    lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
            apDistributionRecordHome = (LocalApDistributionRecordHome)EJBHomeFactory.
                    lookUpLocalHome(LocalApDistributionRecordHome.JNDI_NAME, LocalApDistributionRecordHome.class);
            apSupplierHome = (LocalApSupplierHome)EJBHomeFactory.
                    lookUpLocalHome(LocalApSupplierHome.JNDI_NAME, LocalApSupplierHome.class);
            apSupplierBalanceHome = (LocalApSupplierBalanceHome)EJBHomeFactory.
                    lookUpLocalHome(LocalApSupplierBalanceHome.JNDI_NAME, LocalApSupplierBalanceHome.class);
            apVoucherPaymentScheduleHome = (LocalApVoucherPaymentScheduleHome)EJBHomeFactory.
                    lookUpLocalHome(LocalApVoucherPaymentScheduleHome.JNDI_NAME, LocalApVoucherPaymentScheduleHome.class);

        } catch (NamingException ex) 	{
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try { 
		
            LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);

            StringBuffer jbossQl = new StringBuffer();
		  
            boolean firstArgument = true;
            short ctr = 0;
            int criteriaSize = criteria.size();	      
   
            Object obj[];	      
		
            // Allocate the size of the object parameter

            if (criteria.containsKey("supplierCode")) {

               criteriaSize--;

            }
            
        
            if (criteria.containsKey("includedUnposted")) {
	      	
	      	 criteriaSize--;
	      	 
            }
          
            if (criteria.containsKey("paymentStatus")) {
	      	
	      	 criteriaSize--;
	      	 
	      }

            if (criteria.containsKey("includedPayments")) {

                   criteriaSize--;

            }
	      
            obj = new Object[criteriaSize];

            jbossQl.append("SELECT OBJECT(vou) FROM ApVoucher vou ");

            if (criteria.containsKey("supplierCode")) {

                   if (!firstArgument) {

                      jbossQl.append("AND ");	

               } else {

                  firstArgument = false;
                  jbossQl.append("WHERE ");

               }

                   jbossQl.append("vou.apSupplier.splSupplierCode LIKE '%" + (String)criteria.get("supplierCode") + "%' ");

            }
                
                
                
                
            if (criteria.containsKey("voucherBatch")) {

                if (!firstArgument) {		       	  	
                   jbossQl.append("AND ");		       	     
                } else {		       	  	
                       firstArgument = false;
                       jbossQl.append("WHERE ");		       	  	 
                }

                jbossQl.append("vou.apVoucherBatch.vbName=?" + (ctr+1) + " ");
                obj[ctr] = (String)criteria.get("voucherBatch");
                ctr++;
  
            }
                
                

            if (criteria.containsKey("supplierType")) {

                if (!firstArgument) {		       	  	
                   jbossQl.append("AND ");		       	     
                } else {		       	  	
                       firstArgument = false;
                       jbossQl.append("WHERE ");		       	  	 
                }

                jbossQl.append("vou.apSupplier.apSupplierType.stName=?" + (ctr+1) + " ");
                obj[ctr] = (String)criteria.get("supplierType");
                ctr++;
   	  
            }	
			
		  if (criteria.containsKey("supplierClass")) {
	   	
		   	  if (!firstArgument) {		       	  	
		   	     jbossQl.append("AND ");		       	     
		   	  } else {		       	  	
		   	  	 firstArgument = false;
		   	  	 jbossQl.append("WHERE ");		       	  	 
		   	  }
		   	  
		   	  jbossQl.append("vou.apSupplier.apSupplierClass.scName=?" + (ctr+1) + " ");
		   	  obj[ctr] = (String)criteria.get("supplierClass");
		   	  ctr++;
	   	  
            }			     
          			
        if (criteria.containsKey("dateFrom")) {

            if (!firstArgument) {
                   jbossQl.append("AND ");
            } else {
                   firstArgument = false;
                   jbossQl.append("WHERE ");
            }
            jbossQl.append("vou.vouDate>=?" + (ctr+1) + " ");
            obj[ctr] = (Date)criteria.get("dateFrom");
            ctr++;
        }  
	      
        if (criteria.containsKey("dateTo")) {

            if (!firstArgument) {
                   jbossQl.append("AND ");
            } else {
                   firstArgument = false;
                   jbossQl.append("WHERE ");
            }
            jbossQl.append("vou.vouDate<=?" + (ctr+1) + " ");
            obj[ctr] = (Date)criteria.get("dateTo");
            ctr++;

         }    
		  
            if (criteria.containsKey("documentNumberFrom")) {

                   if (!firstArgument) {
                          jbossQl.append("AND ");
                   } else {
                          firstArgument = false;
                          jbossQl.append("WHERE ");
                   }
                   jbossQl.append("vou.vouDocumentNumber>=?" + (ctr+1) + " ");
                   obj[ctr] = (String)criteria.get("documentNumberFrom");
                   ctr++;
            }  

            if (criteria.containsKey("documentNumberTo")) {

                   if (!firstArgument) {
                          jbossQl.append("AND ");
                   } else {
                          firstArgument = false;
                          jbossQl.append("WHERE ");
                   }
                   jbossQl.append("vou.vouDocumentNumber<=?" + (ctr+1) + " ");
                   obj[ctr] = (String)criteria.get("documentNumberTo");
                   ctr++;

            }
	      
            if (criteria.containsKey("includedUnposted")) {

                String unposted = (String)criteria.get("includedUnposted");

                if (!firstArgument) {

                   jbossQl.append("AND ");

                } else {

                       firstArgument = false;
                       jbossQl.append("WHERE ");

                }	       	  

                if (unposted.equals("NO")) {

                                jbossQl.append("vou.vouPosted = 1 ");

                } else {

                    jbossQl.append("vou.vouVoid = 0 ");  	  

                }   	 

            }	
	      
            if (criteria.containsKey("paymentStatus")) {

              String paymentStatus = (String)criteria.get("paymentStatus");

              if (!firstArgument) {

                      jbossQl.append("AND ");

              } else {

                      firstArgument = false;
                      jbossQl.append("WHERE ");

              }
              System.out.println("paymentStatus: " + paymentStatus);
              if (paymentStatus.equals("PAID")) {	      	 

                      jbossQl.append("vou.vouAmountDue = vou.vouAmountPaid ");

              } else if (paymentStatus.equals("UNPAID")) {	         

                      jbossQl.append("vou.vouAmountDue <> vou.vouAmountPaid ");

              }

            }
	      
            if(adBrnchList.isEmpty()) {
              System.out.print("dito");
              throw new GlobalNoRecordFoundException(); 

            } else {

                if (!firstArgument) {

                   jbossQl.append("AND ");

                } else {

                       firstArgument = false;
                       jbossQl.append("WHERE ");

                }	      

                jbossQl.append("vou.vouAdBranch in (");

                boolean firstLoop = true;

                Iterator i = adBrnchList.iterator();

                while(i.hasNext()) {

                    if(firstLoop == false) { 
                        jbossQl.append(", "); 
                    }
                    else { 
                        firstLoop = false; 
                    }

                    AdBranchDetails mdetails = (AdBranchDetails) i.next();

                    jbossQl.append(mdetails.getBrCode());

                }

                jbossQl.append(") ");

                firstArgument = false;

            }
	      
	      if (!firstArgument) {
		       	  	
       	     jbossQl.append("AND ");
       	     
            } else {
       	  	
       	  	 firstArgument = false;
       	  	 jbossQl.append("WHERE ");
       	  	 
            }	  
       	  
            jbossQl.append("vou.vouAdCompany = " + AD_CMPNY + " ");
                          System.out.println("jbossQl.toString(): " + jbossQl.toString());
            Collection apVouchers = apVoucherHome.getVouByCriteria(jbossQl.toString(), obj);	         

            
            double toalAmount = 0d;
            int numberOfTransaction = 0;
            
         
            Iterator i = apVouchers.iterator();
		  	
            while (i.hasNext()) {
                  
             
                LocalApVoucher apVoucher = (LocalApVoucher)i.next();   
                
                
                ApRepApRegisterDetails mdetails = new ApRepApRegisterDetails();
                
                Collection paymentSchedList = apVoucherPaymentScheduleHome.findOpenVpsByVouCode(apVoucher.getVouCode(), AD_CMPNY);
                
               
                Iterator g = paymentSchedList.iterator();
	          
                while(g.hasNext()) {
         
                            
                    LocalApVoucherPaymentSchedule apVoucherPaymentSchedule = (LocalApVoucherPaymentSchedule)g.next();
                    
                    
                    
                    mdetails.setArgVpsCode(apVoucherPaymentSchedule.getVpsCode());
                    System.out.println(mdetails.getArgVpsCode() + "  vps code");
                }
                mdetails.setArgDate(apVoucher.getVouDate());
		  		
		  		
                mdetails.setArgDocumentNumber(apVoucher.getVouDocumentNumber());
                mdetails.setArgReferenceNumber(apVoucher.getVouReferenceNumber());
                mdetails.setArgDescription(apVoucher.getVouDescription());
                mdetails.setArgSplSupplierCode(apVoucher.getApSupplier().getSplSupplierCode());
                mdetails.setArgSplSupplierClass(apVoucher.getApSupplier().getApSupplierClass().getScName());
                mdetails.setArgSplName(apVoucher.getApSupplier().getSplName());
                mdetails.setArgSplTin(apVoucher.getApSupplier().getSplTin());
                mdetails.setArgSplAddress(apVoucher.getApSupplier().getSplAddress());
                mdetails.setArgPoNumber(apVoucher.getVouPoNumber()==null ?  "" : apVoucher.getVouPoNumber());
                mdetails.setArgFunctionalCurrencyName(apVoucher.getGlFunctionalCurrency().getFcName());
                
                mdetails.setArgPosted(apVoucher.getVouPosted()!=0);

                mdetails.setArgVoid(apVoucher.getVouVoid()!=0);
                mdetails.setArgAmountPaid(apVoucher.getVouAmountPaid());
                mdetails.setArgCurrency(apVoucher.getGlFunctionalCurrency().getFcName());
                
                if(apVoucher.getApSupplier().getSplAccountNumber()==null){
                    mdetails.setArgSplSupplierAccountNumber(apVoucher.getApSupplier().getSplAccountNumber());
                }else{
                     mdetails.setArgSplSupplierAccountNumber("");
                }
                
                
                // type
                if(apVoucher.getApSupplier().getApSupplierType() == null){

                        mdetails.setArgSplSupplierType("UNDEFINE");

                } else {

                        mdetails.setArgSplSupplierType(apVoucher.getApSupplier().getApSupplierType().getStName());

                }

                

                double AMNT_DUE = 0d;

                if (apVoucher.getVouDebitMemo() == EJBCommon.FALSE) {

                    AMNT_DUE = EJBCommon.roundIt(this.convertForeignToFunctionalCurrency(
                                    apVoucher.getGlFunctionalCurrency().getFcCode(),
                                    apVoucher.getGlFunctionalCurrency().getFcName(),
                                    apVoucher.getVouConversionDate(),
                                    apVoucher.getVouConversionRate(),
                                    apVoucher.getVouAmountDue(), AD_CMPNY), adCompany.getGlFunctionalCurrency().getFcPrecision());

                    mdetails.setArgType("VOUCHER");
                    mdetails.setArgAmount(AMNT_DUE);
                    list.add(mdetails);
                }else{
                    continue;
                } 

                
            }
            
            
            return list;
		      
        } catch (GlobalNoRecordFoundException ex) {
	  	 
	  	  throw ex;
	  	
        } catch (Exception ex) {
	  	

	  	  ex.printStackTrace();
	  	  throw new EJBException(ex.getMessage());
	  	
        }
   
    }
    
    
    
    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList executeApRepApRegister(HashMap criteria, String ORDER_BY, String GROUP_BY, boolean SHOW_ENTRIES, boolean SUMMARIZE, boolean INCLUDE_DC, ArrayList adBrnchList, Integer AD_CMPNY) 
        throws GlobalNoRecordFoundException {
                    
        Debug.print("ApRepApRegisterControllerBean executeApRepApRegister");
        
        LocalApVoucherHome apVoucherHome = null;
        LocalApCheckHome apCheckHome = null;
        LocalAdCompanyHome adCompanyHome = null;
        LocalApDistributionRecordHome apDistributionRecordHome = null;
        LocalApSupplierBalanceHome apSupplierBalanceHome = null;
        LocalApSupplierHome apSupplierHome = null;
        
        ArrayList list = new ArrayList();
        
        //initialized EJB Home
        
        try {

        	apVoucherHome = (LocalApVoucherHome)EJBHomeFactory.
        		lookUpLocalHome(LocalApVoucherHome.JNDI_NAME, LocalApVoucherHome.class);
        	apCheckHome = (LocalApCheckHome)EJBHomeFactory.
        		lookUpLocalHome(LocalApCheckHome.JNDI_NAME, LocalApCheckHome.class);
        	adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
        		lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
        	apDistributionRecordHome = (LocalApDistributionRecordHome)EJBHomeFactory.
        		lookUpLocalHome(LocalApDistributionRecordHome.JNDI_NAME, LocalApDistributionRecordHome.class);
        	apSupplierHome = (LocalApSupplierHome)EJBHomeFactory.
        		lookUpLocalHome(LocalApSupplierHome.JNDI_NAME, LocalApSupplierHome.class);
        	apSupplierBalanceHome = (LocalApSupplierBalanceHome)EJBHomeFactory.
        		lookUpLocalHome(LocalApSupplierBalanceHome.JNDI_NAME, LocalApSupplierBalanceHome.class);

        } catch (NamingException ex) 	{
            
            throw new EJBException(ex.getMessage());
            
        }
        
		try { 
		
		  LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
           
		  StringBuffer jbossQl = new StringBuffer();
		  
	      boolean firstArgument = true;
	      short ctr = 0;
		  int criteriaSize = criteria.size();	      
		  Date dateFrom = null;
		  Date dateLast = null;
		  int dtChck=0;
	      Object obj[];	      
		
		  // Allocate the size of the object parameter
		
	      if (criteria.containsKey("supplierCode")) {
	      	
	      	 criteriaSize--;
	      	 
	      }
              
            

          if (criteria.containsKey("includedUnposted")) {
	      	
	      	 criteriaSize--;
	      	 
	      }
          
          if (criteria.containsKey("paymentStatus")) {
	      	
	      	 criteriaSize--;
	      	 
	      }
          
          if (criteria.containsKey("includedPayments")) {

        	 criteriaSize--;

          }
	      
	      obj = new Object[criteriaSize];
	      
	      jbossQl.append("SELECT OBJECT(vou) FROM ApVoucher vou ");
		       	      
		  if (criteria.containsKey("supplierCode")) {
		  	
		  	 if (!firstArgument) {
		  	 
		  	    jbossQl.append("AND ");	
		  	 	
		     } else {
		     	
		     	firstArgument = false;
		     	jbossQl.append("WHERE ");
		     	
		     }
		     
		  	 jbossQl.append("vou.apSupplier.splSupplierCode LIKE '%" + (String)criteria.get("supplierCode") + "%' ");
		  	 
		  }
                  
                   if (criteria.containsKey("voucherBatch")) {
		  	
		  	 if (!firstArgument) {
		  	 
		  	    jbossQl.append("AND ");	
		  	 	
		     } else {
		     	
		     	firstArgument = false;
		     	jbossQl.append("WHERE ");
		     	
		     }
		     
		  	 jbossQl.append("vou.apVoucherBatch.vbName = '" + (String)criteria.get("voucherBatch") + "' ");
		  	 obj[ctr] = (String)criteria.get("voucherBatch");
		   	  ctr++;
		  }

		  if (criteria.containsKey("supplierType")) {
	   	
		   	  if (!firstArgument) {		       	  	
		   	     jbossQl.append("AND ");		       	     
		   	  } else {		       	  	
		   	  	 firstArgument = false;
		   	  	 jbossQl.append("WHERE ");		       	  	 
		   	  }
		   	  
		   	  jbossQl.append("vou.apSupplier.apSupplierType.stName=?" + (ctr+1) + " ");
		   	  obj[ctr] = (String)criteria.get("supplierType");
		   	  ctr++;
	   	  
	      }	
			
		  if (criteria.containsKey("supplierClass")) {
	   	
		   	  if (!firstArgument) {		       	  	
		   	     jbossQl.append("AND ");		       	     
		   	  } else {		       	  	
		   	  	 firstArgument = false;
		   	  	 jbossQl.append("WHERE ");		       	  	 
		   	  }
		   	  
		   	  jbossQl.append("vou.apSupplier.apSupplierClass.scName=?" + (ctr+1) + " ");
		   	  obj[ctr] = (String)criteria.get("supplierClass");
		   	  ctr++;
	   	  
	      }			     
          			
	      if (criteria.containsKey("dateFrom")) {
		      	
		  	 if (!firstArgument) {
		  	 	jbossQl.append("AND ");
		  	 } else {
		  	 	firstArgument = false;
		  	 	jbossQl.append("WHERE ");
		  	 }
		  	 jbossQl.append("vou.vouDate>=?" + (ctr+1) + " ");
		  	 obj[ctr] = (Date)criteria.get("dateFrom");
		  	 ctr++;
		  }  
		      
		  if (criteria.containsKey("dateTo")) {
		  	
		  	 if (!firstArgument) {
		  	 	jbossQl.append("AND ");
		  	 } else {
		  	 	firstArgument = false;
		  	 	jbossQl.append("WHERE ");
		  	 }
		  	 jbossQl.append("vou.vouDate<=?" + (ctr+1) + " ");
		  	 obj[ctr] = (Date)criteria.get("dateTo");
		  	 ctr++;
		  	 
		  }    
		  
		  if (criteria.containsKey("documentNumberFrom")) {
		      	
		  	 if (!firstArgument) {
		  	 	jbossQl.append("AND ");
		  	 } else {
		  	 	firstArgument = false;
		  	 	jbossQl.append("WHERE ");
		  	 }
		  	 jbossQl.append("vou.vouDocumentNumber>=?" + (ctr+1) + " ");
		  	 obj[ctr] = (String)criteria.get("documentNumberFrom");
		  	 ctr++;
		  }  
		      
		  if (criteria.containsKey("documentNumberTo")) {
		  	
		  	 if (!firstArgument) {
		  	 	jbossQl.append("AND ");
		  	 } else {
		  	 	firstArgument = false;
		  	 	jbossQl.append("WHERE ");
		  	 }
		  	 jbossQl.append("vou.vouDocumentNumber<=?" + (ctr+1) + " ");
		  	 obj[ctr] = (String)criteria.get("documentNumberTo");
		  	 ctr++;
		  	 
		  }
	      
	      if (criteria.containsKey("includedUnposted")) {
	          
	          String unposted = (String)criteria.get("includedUnposted");
	          
	          if (!firstArgument) {
		       	  	
	       	     jbossQl.append("AND ");
	       	     
	       	  } else {
	       	  	
	       	  	 firstArgument = false;
	       	  	 jbossQl.append("WHERE ");
	       	  	 
	       	  }	       	  
	                  	
	       	  if (unposted.equals("NO")) {
	       			       	  		       	  
				  jbossQl.append("vou.vouPosted = 1 ");
		       	  
	       	  } else {
	       	  	
	       	      jbossQl.append("vou.vouVoid = 0 ");  	  
	       	  	
	       	  }   	 
	       	  	       	  
	      }	
	      
	      if (criteria.containsKey("paymentStatus")) {
	      	
	      	String paymentStatus = (String)criteria.get("paymentStatus");
	      	
	      	if (!firstArgument) {
	      		
	      		jbossQl.append("AND ");
	      		
	      	} else {
	      		
	      		firstArgument = false;
	      		jbossQl.append("WHERE ");
	      		
	      	}
	      	System.out.println("paymentStatus: " + paymentStatus);
	      	if (paymentStatus.equals("PAID")) {	      	 
	      		
	      		jbossQl.append("vou.vouAmountDue = vou.vouAmountPaid ");
	      		
	      	} else if (paymentStatus.equals("UNPAID")) {	         
	      		
	      		jbossQl.append("vou.vouAmountDue <> vou.vouAmountPaid ");
	      		
	      	}
	      	
	      }
	      
	      if(adBrnchList.isEmpty()) {
	      	System.out.print("dito");
	      	throw new GlobalNoRecordFoundException(); 
	      	
	      } else {
	          
	          if (!firstArgument) {
		       	  	
	       	     jbossQl.append("AND ");
	       	     
	       	  } else {
	       	  	
	       	  	 firstArgument = false;
	       	  	 jbossQl.append("WHERE ");
	       	  	 
	       	  }	      
	          
	          jbossQl.append("vou.vouAdBranch in (");
	          
	          boolean firstLoop = true;
	          
	          Iterator i = adBrnchList.iterator();
	          
	          while(i.hasNext()) {
	              
	              if(firstLoop == false) { 
	                  jbossQl.append(", "); 
	              }
	              else { 
	                  firstLoop = false; 
	              }
	              
	              AdBranchDetails mdetails = (AdBranchDetails) i.next();
	              
	              jbossQl.append(mdetails.getBrCode());
	              
	          }
	          
	          jbossQl.append(") ");
	          
	          firstArgument = false;
	          
	      }
	      
	      if (!firstArgument) {
		       	  	
       	     jbossQl.append("AND ");
       	     
       	  } else {
       	  	
       	  	 firstArgument = false;
       	  	 jbossQl.append("WHERE ");
       	  	 
       	  }	  
       	  
       	  jbossQl.append("vou.vouAdCompany = " + AD_CMPNY + " ");
			System.out.println("jbossQl.toString(): " + jbossQl.toString());
       	  Collection apVouchers = apVoucherHome.getVouByCriteria(jbossQl.toString(), obj);	         
		  
		  //if (apVouchers.size() == 0)
		  //   throw new GlobalNoRecordFoundException();
		     
		  if(SHOW_ENTRIES) {
		  	
		  	Iterator i = apVouchers.iterator();
		  	
		  	while (i.hasNext()) {
		  		
		  		  boolean first = true;
			  	
			  	  LocalApVoucher apVoucher = (LocalApVoucher)i.next();   	  
			  	
			  	  Collection apDistributionRecords = apVoucher.getApDistributionRecords();
			  	  
			  	  Iterator j = apDistributionRecords.iterator();
			  	  
			  	  while(j.hasNext()) {
			  	  	dtChck = dtChck + 1;
			  	  	LocalApDistributionRecord apDistributionRecord = (LocalApDistributionRecord)j.next();
			  	  	
			  	  	ApRepApRegisterDetails mdetails = new ApRepApRegisterDetails();
			  	  	
			  	  if(dtChck==1 && (Date)criteria.get("dateFrom")==null){
					  dateFrom = apVoucher.getVouDate();
					  System.out.println("apVoucher.getVouDate();1: " + apVoucher.getVouDate());
					  System.out.println("date here1: " +dateFrom);
				  }
			  	dateLast = apVoucher.getVouDate();
			  	  	mdetails.setArgDate(apVoucher.getVouDate());
			  	  	mdetails.setArgDocumentNumber(apVoucher.getVouDocumentNumber());
			  	  	mdetails.setArgReferenceNumber(apVoucher.getVouReferenceNumber());
			  	  	mdetails.setArgDescription(apVoucher.getVouDescription());
			  	  	mdetails.setArgSplSupplierCode(apVoucher.getApSupplier().getSplSupplierCode());
			  	  	mdetails.setArgSplSupplierClass(apVoucher.getApSupplier().getApSupplierClass().getScName());
			  	  	mdetails.setArgSplName(apVoucher.getApSupplier().getSplName());
			  	  	mdetails.setArgSplTin(apVoucher.getApSupplier().getSplTin());
			  	  	mdetails.setArgSplAddress(apVoucher.getApSupplier().getSplAddress());			  	   
			  	  	mdetails.setArgPoNumber(apVoucher.getVouPoNumber()==null ?  "" : apVoucher.getVouPoNumber());
			  	  	// type
			  	  	if(apVoucher.getApSupplier().getApSupplierType() == null){
			  	  		
			  	  		mdetails.setArgSplSupplierType("UNDEFINE");
			  	  		
			  	  	} else {
			  	  		
			  	  		mdetails.setArgSplSupplierType(apVoucher.getApSupplier().getApSupplierType().getStName());
			  	  		
			  	  	}
			  	  	
			  	  	if(first) {
			  	  		
			  	  		double AMNT_DUE = 0d;
			  	  		
			  	  		if (apVoucher.getVouDebitMemo() == EJBCommon.FALSE) {
			  	  			
			  	  			AMNT_DUE = EJBCommon.roundIt(this.convertForeignToFunctionalCurrency(
			  	  					apVoucher.getGlFunctionalCurrency().getFcCode(),
									apVoucher.getGlFunctionalCurrency().getFcName(),
									apVoucher.getVouConversionDate(),
									apVoucher.getVouConversionRate(),
									apVoucher.getVouAmountDue(), AD_CMPNY), adCompany.getGlFunctionalCurrency().getFcPrecision());
			  	  		
			  	  		} else {
			  	  			
			  	  			
			  	  			LocalApVoucher apDebitedVoucher = apVoucherHome.findByVouDocumentNumberAndVouDebitMemoAndBrCode(apVoucher.getVouDmVoucherNumber(), EJBCommon.FALSE, apVoucher.getVouAdBranch(), AD_CMPNY);
			  	  			
			  	  			AMNT_DUE = EJBCommon.roundIt(this.convertForeignToFunctionalCurrency(
			  	  					apDebitedVoucher.getGlFunctionalCurrency().getFcCode(),
									apDebitedVoucher.getGlFunctionalCurrency().getFcName(),
									apDebitedVoucher.getVouConversionDate(),
									apDebitedVoucher.getVouConversionRate(),
									apVoucher.getVouBillAmount(), AD_CMPNY) * -1, adCompany.getGlFunctionalCurrency().getFcPrecision());
			  	  			
			  	  		}
			  	  		
			  	  		mdetails.setArgAmount(AMNT_DUE);
			  	  		
			  	  		first = false;
			  	  		
			  	  	}
			  	  	
			  	  	if (apVoucher.getVouDebitMemo() == EJBCommon.FALSE) {
			  	  		
			  	  		mdetails.setArgType("VOUCHER");
			  	  		
			  	  	} else {
			  	  		
			  	  		mdetails.setArgType("DEBIT MEMO");
			  	  		
			  	  	}
			  	  	
			  	  	mdetails.setOrderBy(ORDER_BY);
			  	  	
			  	  	//distribution record details
			  	  	mdetails.setArgDrCoaAccountNumber(apDistributionRecord.getGlChartOfAccount().getCoaAccountNumber());
			  	  	mdetails.setArgDrCoaAccountDescription(apDistributionRecord.getGlChartOfAccount().getCoaAccountDescription());
			  	  	
			  	  	if(apDistributionRecord.getDrDebit() == EJBCommon.TRUE) {
			  	  		
			  	  		mdetails.setArgDrDebitAmount(apDistributionRecord.getDrAmount());
			  	  		
			  	  	} else {
			  	  		
			  	  		mdetails.setArgDrCreditAmount(apDistributionRecord.getDrAmount());
			  	  		
			  	  	}
			  	  	
			  	  	
			  	  	list.add(mdetails);
			  	  	
			  	  }
				         
			  }
		  	
		  } else {
		  	
		  	Iterator i = apVouchers.iterator();
		  	
		  	while (i.hasNext()) {
		  		
		  		LocalApVoucher apVoucher = (LocalApVoucher)i.next();   	  
		  		dtChck = dtChck + 1;
		  		ApRepApRegisterDetails mdetails = new ApRepApRegisterDetails();
		  		
		  		if(dtChck==1 && (Date)criteria.get("dateFrom")==null){
					  dateFrom = apVoucher.getVouDate();
					  System.out.println("apVoucher.getVouDate()2: " + apVoucher.getVouDate());
					  System.out.println("date here 2: "+dateFrom);
				  }
			  	dateLast = apVoucher.getVouDate();
			  	
		  		mdetails.setArgDate(apVoucher.getVouDate());
		  		
		  		
		  		mdetails.setArgDocumentNumber(apVoucher.getVouDocumentNumber());
		  		mdetails.setArgReferenceNumber(apVoucher.getVouReferenceNumber());
		  		mdetails.setArgDescription(apVoucher.getVouDescription());
		  		mdetails.setArgSplSupplierCode(apVoucher.getApSupplier().getSplSupplierCode());
		  		mdetails.setArgSplSupplierClass(apVoucher.getApSupplier().getApSupplierClass().getScName());
		  		mdetails.setArgSplName(apVoucher.getApSupplier().getSplName());
		  		mdetails.setArgSplTin(apVoucher.getApSupplier().getSplTin());
		  		mdetails.setArgSplAddress(apVoucher.getApSupplier().getSplAddress());
		  		mdetails.setArgPoNumber(apVoucher.getVouPoNumber()==null ?  "" : apVoucher.getVouPoNumber());
		  		
		  		// type
		  		if(apVoucher.getApSupplier().getApSupplierType() == null){
		  			
		  			mdetails.setArgSplSupplierType("UNDEFINE");
		  			
		  		} else {
		  			
		  			mdetails.setArgSplSupplierType(apVoucher.getApSupplier().getApSupplierType().getStName());
		  			
		  		}
		  		
		  		double AMNT_DUE = 0d;
		  		
		  		if (apVoucher.getVouDebitMemo() == EJBCommon.FALSE) {
		  			
		  			AMNT_DUE = EJBCommon.roundIt(this.convertForeignToFunctionalCurrency(
		  					apVoucher.getGlFunctionalCurrency().getFcCode(),
							apVoucher.getGlFunctionalCurrency().getFcName(),
							apVoucher.getVouConversionDate(),
							apVoucher.getVouConversionRate(),
							apVoucher.getVouAmountDue(), AD_CMPNY), adCompany.getGlFunctionalCurrency().getFcPrecision());
		  			
		  			mdetails.setArgType("VOUCHER");
		  			
		  		} else {
		  			
		  			
		  			LocalApVoucher apDebitedVoucher = apVoucherHome.findByVouDocumentNumberAndVouDebitMemoAndBrCode(apVoucher.getVouDmVoucherNumber(), EJBCommon.FALSE, apVoucher.getVouAdBranch(), AD_CMPNY);
		  			
		  			AMNT_DUE = EJBCommon.roundIt(this.convertForeignToFunctionalCurrency(
		  					apDebitedVoucher.getGlFunctionalCurrency().getFcCode(),
							apDebitedVoucher.getGlFunctionalCurrency().getFcName(),
							apDebitedVoucher.getVouConversionDate(),
							apDebitedVoucher.getVouConversionRate(),
							apVoucher.getVouBillAmount(), AD_CMPNY) * -1, adCompany.getGlFunctionalCurrency().getFcPrecision());
		  			
		  			mdetails.setArgType("DEBIT MEMO");
		  			
		  		}
		  		
		  		mdetails.setArgAmount(AMNT_DUE);
		  		mdetails.setOrderBy(ORDER_BY);
		  		
		  		list.add(mdetails);
		  		
		  	}
		  	
		  }
		  
		  // Payments
		  
		  if(((String)criteria.get("includedPayments")).equals("YES")) {
			  System.out.println("includedPayments");
			  if (criteria.containsKey("documentNumberFrom")) {

				  criteriaSize--;

			  }

			  if (criteria.containsKey("documentNumberTo")) {

				  criteriaSize--;

			  }

			  obj = new Object[criteriaSize];
			  firstArgument = true;
			  ctr = 0;
  			
			  jbossQl = new StringBuffer();
			  jbossQl.append("SELECT OBJECT(chk) FROM ApCheck chk ");

			  if (criteria.containsKey("supplierCode")) {

				  if (!firstArgument) {

					  jbossQl.append("AND ");	

				  } else {

					  firstArgument = false;
					  jbossQl.append("WHERE ");

				  }

				  jbossQl.append("chk.apSupplier.splSupplierCode LIKE '%" + (String)criteria.get("supplierCode") + "%' ");

			  }

			  if (criteria.containsKey("supplierType")) {

				  if (!firstArgument) {		       	  	
					  jbossQl.append("AND ");		       	     
				  } else {		       	  	
					  firstArgument = false;
					  jbossQl.append("WHERE ");		       	  	 
				  }

				  jbossQl.append("chk.apSupplier.apSupplierType.stName=?" + (ctr+1) + " ");
				  obj[ctr] = (String)criteria.get("supplierType");
				  ctr++;

			  }	

			  if (criteria.containsKey("supplierClass")) {

				  if (!firstArgument) {		       	  	
					  jbossQl.append("AND ");		       	     
				  } else {		       	  	
					  firstArgument = false;
					  jbossQl.append("WHERE ");		       	  	 
				  }

				  jbossQl.append("chk.apSupplier.apSupplierClass.scName=?" + (ctr+1) + " ");
				  obj[ctr] = (String)criteria.get("supplierClass");
				  ctr++;

			  }			     

			  if (criteria.containsKey("dateFrom")) {

				  /*if (!firstArgument) {
					  jbossQl.append("AND ");
				  } else {
					  firstArgument = false;
					  jbossQl.append("WHERE ");
				  }
				  jbossQl.append("chk.chkDate>=?" + (ctr+1) + " ");*/
				  if (!firstArgument) {
				  jbossQl.append("AND ");
				  } else {
					  firstArgument = false;
					  jbossQl.append("WHERE ");
				  }
				  jbossQl.append("chk.chkDate>=?" + (ctr+1) + " ");
				  obj[ctr] = (Date)criteria.get("dateFrom");
				  ctr++;
			  }  

			  if (criteria.containsKey("dateTo")) {

				  if (!firstArgument) {
					  jbossQl.append("AND ");
				  } else {
					  firstArgument = false;
					  jbossQl.append("WHERE ");
				  }
				  jbossQl.append("chk.chkDate<=?" + (ctr+1) + " ");
				  obj[ctr] = (Date)criteria.get("dateTo");
				  ctr++;

			  }    

			  if (criteria.containsKey("includedUnposted")) {

				  String unposted = (String)criteria.get("includedUnposted");

				  if (!firstArgument) {

					  jbossQl.append("AND ");

				  } else {

					  firstArgument = false;
					  jbossQl.append("WHERE ");

				  }	       	  

				  if (unposted.equals("NO")) {

					  jbossQl.append("chk.chkPosted = 1 ");

				  } else {

					  jbossQl.append("chk.chkVoid = 0 ");  	  

				  }   	 

			  }	

			  if(adBrnchList.isEmpty()) {

				  throw new GlobalNoRecordFoundException(); 

			  } else {

				  if (!firstArgument) {

					  jbossQl.append("AND ");

				  } else {

					  firstArgument = false;
					  jbossQl.append("WHERE ");

				  }	      

				  jbossQl.append("chk.chkAdBranch in (");

				  boolean firstLoop = true;

				  Iterator i = adBrnchList.iterator();

				  while(i.hasNext()) {

					  if(firstLoop == false) { 
						  jbossQl.append(", "); 
					  }
					  else { 
						  firstLoop = false; 
					  }

					  AdBranchDetails mdetails = (AdBranchDetails) i.next();

					  jbossQl.append(mdetails.getBrCode());

				  }

				  jbossQl.append(") ");

				  firstArgument = false;

			  }

			  if (!firstArgument) {

				  jbossQl.append("AND ");

			  } else {

				  firstArgument = false;
				  jbossQl.append("WHERE ");

			  }	  

			  jbossQl.append("chk.chkAdCompany = " + AD_CMPNY + " ");

			  Collection apChecks = apCheckHome.getChkByCriteria(jbossQl.toString(), obj);	         

			  if(SHOW_ENTRIES) {

				  Iterator i = apChecks.iterator();

				  while (i.hasNext()) {

					  boolean first = true;

					  LocalApCheck apCheck = (LocalApCheck)i.next();   	  
					  
					  if(apCheck.getChkType()=="PAYMENT"){
						  Collection apDistributionRecords = apCheck.getApDistributionRecords();

						  Iterator j = apDistributionRecords.iterator();

						  while(j.hasNext()) {

							  LocalApDistributionRecord apDistributionRecord = (LocalApDistributionRecord)j.next();
							  
							  ApRepApRegisterDetails mdetails = new ApRepApRegisterDetails();
							  mdetails.setArgDate(apCheck.getChkDate());
							  System.out.println("mdetails.setArgDate(apCheck.getChkDate());: " + apCheck.getChkDate());
							  mdetails.setArgDocumentNumber(apCheck.getChkDocumentNumber());
							  mdetails.setArgReferenceNumber(apCheck.getChkReferenceNumber());
							  mdetails.setArgDescription(apCheck.getChkDescription());
							  mdetails.setArgSplSupplierCode(apCheck.getApSupplier().getSplSupplierCode());
							  mdetails.setArgSplSupplierClass(apCheck.getApSupplier().getApSupplierClass().getScName());
							  mdetails.setArgSplName(apCheck.getChkSupplierName() == null ? apCheck.getApSupplier().getSplName() : apCheck.getChkSupplierName());
							  mdetails.setArgSplTin(apCheck.getApSupplier().getSplTin());
							  mdetails.setArgSplAddress(apCheck.getApSupplier().getSplAddress());
							  mdetails.setArgCheckType(apCheck.getChkType());
							  
							  // type
							  if(apCheck.getApSupplier().getApSupplierType() == null){

								  mdetails.setArgSplSupplierType("UNDEFINE");

							  } else {

								  mdetails.setArgSplSupplierType(apCheck.getApSupplier().getApSupplierType().getStName());

							  }

							  if(first) {

								  double AMNT_DUE = EJBCommon.roundIt(this.convertForeignToFunctionalCurrency(
										  apCheck.getGlFunctionalCurrency().getFcCode(),
										  apCheck.getGlFunctionalCurrency().getFcName(),
										  apCheck.getChkConversionDate(),
										  apCheck.getChkConversionRate(),
										  apCheck.getChkAmount(), AD_CMPNY), adCompany.getGlFunctionalCurrency().getFcPrecision());

								  mdetails.setArgAmount(AMNT_DUE);

								  first = false;

							  }

							  mdetails.setOrderBy(ORDER_BY);

							  //distribution record details
							  mdetails.setArgDrCoaAccountNumber(apDistributionRecord.getGlChartOfAccount().getCoaAccountNumber());
							  mdetails.setArgDrCoaAccountDescription(apDistributionRecord.getGlChartOfAccount().getCoaAccountDescription());

							  if(apDistributionRecord.getDrDebit() == EJBCommon.TRUE) {

								  mdetails.setArgDrDebitAmount(apDistributionRecord.getDrAmount());

							  } else {

								  mdetails.setArgDrCreditAmount(apDistributionRecord.getDrAmount());

							  }

							  mdetails.setArgType("CHECK");
							  
							  list.add(mdetails);

						  }

					  }
					  
				  }

			  } else {

				  Iterator i = apChecks.iterator();
				  System.out.println("includedPayments2");
				  
				  while (i.hasNext()) {
					  System.out.println("includedPayments2");
					  LocalApCheck apCheck = (LocalApCheck)i.next();   	  
					  if(apCheck.getChkType().equals("PAYMENT")){
						  System.out.println("includedPayments3");
						  ApRepApRegisterDetails mdetails = new ApRepApRegisterDetails();
						  mdetails.setArgDate(apCheck.getChkDate());
						  System.out.println("apCheck.getChkDate(): " + apCheck.getChkDate());
						  mdetails.setArgDocumentNumber(apCheck.getChkDocumentNumber());
						  mdetails.setArgReferenceNumber(apCheck.getChkReferenceNumber());
						  mdetails.setArgDescription(apCheck.getChkDescription());
						  mdetails.setArgSplSupplierCode(apCheck.getApSupplier().getSplSupplierCode());
						  mdetails.setArgSplSupplierClass(apCheck.getApSupplier().getApSupplierClass().getScName());
						  mdetails.setArgSplName(apCheck.getChkSupplierName() == null ? apCheck.getApSupplier().getSplName() : apCheck.getChkSupplierName());
						  mdetails.setArgSplTin(apCheck.getApSupplier().getSplTin());
						  mdetails.setArgSplAddress(apCheck.getApSupplier().getSplAddress());
						  mdetails.setArgCheckType(apCheck.getChkType());
						  // type
						  if(apCheck.getApSupplier().getApSupplierType() == null){

							  mdetails.setArgSplSupplierType("UNDEFINE");

						  } else {

							  mdetails.setArgSplSupplierType(apCheck.getApSupplier().getApSupplierType().getStName());

						  }

						  double AMNT_DUE = EJBCommon.roundIt(this.convertForeignToFunctionalCurrency(
								  apCheck.getGlFunctionalCurrency().getFcCode(),
								  apCheck.getGlFunctionalCurrency().getFcName(),
								  apCheck.getChkConversionDate(),
								  apCheck.getChkConversionRate(),
								  apCheck.getChkAmount(), AD_CMPNY), adCompany.getGlFunctionalCurrency().getFcPrecision());

						  System.out.println("apCheck.getChkDate(): " + apCheck.getChkDate());
						  mdetails.setArgAmount(AMNT_DUE);
						  mdetails.setOrderBy(ORDER_BY);
						  System.out.println("includedPayments4");
						  mdetails.setArgType("CHECK");

						  list.add(mdetails);

					  }
				  }
					  

			  }

		  }
		  System.out.println("date here 3: " + dateFrom);
 // DC
		  double drAmnt = 0d;
		  double balance = 0d;
		  double begbalance = 0d;
		  double ptdbalance = 0d;
		  double SplBalance = 0d;
		  String checksup = "";
		  System.out.println("boom boom pow!");
		  
		  System.out.println(INCLUDE_DC + " and " + GROUP_BY);
		  if(INCLUDE_DC && GROUP_BY.equals("SUPPLIER CODE")) {
			  boolean issetBegBal = true;
			  
			  System.out.println("Kaboom!");
			  if (criteria.containsKey("documentNumberFrom")) {

				  criteriaSize--;

			  }

			  if (criteria.containsKey("documentNumberTo")) {

				  criteriaSize--;

			  }

			  obj = new Object[criteriaSize];
			  firstArgument = true;
			  ctr = 0;

			  jbossQl = new StringBuffer();
			  jbossQl.append("SELECT OBJECT(chk) FROM ApCheck chk ");

			  if (criteria.containsKey("supplierCode")) {

				  if (!firstArgument) {

					  jbossQl.append("AND ");	

				  } else {

					  firstArgument = false;
					  jbossQl.append("WHERE ");

				  }

				  jbossQl.append("chk.apSupplier.splSupplierCode LIKE '%" + (String)criteria.get("supplierCode") + "%' ");

			  }

			  if (criteria.containsKey("supplierType")) {

				  if (!firstArgument) {		       	  	
					  jbossQl.append("AND ");		       	     
				  } else {		       	  	
					  firstArgument = false;
					  jbossQl.append("WHERE ");		       	  	 
				  }

				  jbossQl.append("chk.apSupplier.apSupplierType.stName=?" + (ctr+1) + " ");
				  obj[ctr] = (String)criteria.get("supplierType");
				  ctr++;

			  }	

			  if (criteria.containsKey("supplierClass")) {

				  if (!firstArgument) {		       	  	
					  jbossQl.append("AND ");		       	     
				  } else {		       	  	
					  firstArgument = false;
					  jbossQl.append("WHERE ");		       	  	 
				  }

				  jbossQl.append("chk.apSupplier.apSupplierClass.scName=?" + (ctr+1) + " ");
				  obj[ctr] = (String)criteria.get("supplierClass");
				  ctr++;

			  }			     

			  if (criteria.containsKey("dateFrom")) {

				  /*if (!firstArgument) {
					  jbossQl.append("AND ");
				  } else {
					  firstArgument = false;
					  jbossQl.append("WHERE ");
				  }
				  jbossQl.append("chk.chkDate>=?" + (ctr+1) + " ");*/
				  
				  if (!firstArgument) {
				  jbossQl.append("AND ");
				  } else {
					  firstArgument = false;
					  jbossQl.append("WHERE ");
				  }
				  jbossQl.append("chk.chkDate>=?" + (ctr+1) + " ");
				  
				  obj[ctr] = (Date)criteria.get("dateFrom");
				  ctr++;
			  }  

			  if (criteria.containsKey("dateTo")) {

				  if (!firstArgument) {
					  jbossQl.append("AND ");
				  } else {
					  firstArgument = false;
					  jbossQl.append("WHERE ");
				  }
				  jbossQl.append("chk.chkDate<=?" + (ctr+1) + " ");
				  obj[ctr] = (Date)criteria.get("dateTo");
				  ctr++;

			  }    

			  if (criteria.containsKey("includedUnposted")) {

				  String unposted = (String)criteria.get("includedUnposted");

				  if (!firstArgument) {

					  jbossQl.append("AND ");

				  } else {

					  firstArgument = false;
					  jbossQl.append("WHERE ");

				  }	       	  

				  if (unposted.equals("NO")) {

					  jbossQl.append("chk.chkPosted = 1 ");

				  } else {

					  jbossQl.append("chk.chkVoid = 0 ");  	  

				  }   	 

			  }	

			  if(adBrnchList.isEmpty()) {

				  throw new GlobalNoRecordFoundException(); 

			  } else {

				  if (!firstArgument) {

					  jbossQl.append("AND ");

				  } else {

					  firstArgument = false;
					  jbossQl.append("WHERE ");

				  }	      

				  jbossQl.append("chk.chkAdBranch in (");

				  boolean firstLoop = true;

				  Iterator i = adBrnchList.iterator();

				  while(i.hasNext()) {

					  if(firstLoop == false) { 
						  jbossQl.append(", "); 
					  }
					  else { 
						  firstLoop = false; 
					  }

					  AdBranchDetails mdetails = (AdBranchDetails) i.next();

					  jbossQl.append(mdetails.getBrCode());

				  }

				  jbossQl.append(") ");

				  firstArgument = false;

			  }

			  if (!firstArgument) {

				  jbossQl.append("AND ");

			  } else {

				  firstArgument = false;
				  jbossQl.append("WHERE ");

			  }	  

			  GregorianCalendar lastDateGc = new GregorianCalendar();
			  if((Date)criteria.get("dateTo") == null){
				  lastDateGc.setTime(dateLast);
			  }else{
				  dateLast = (Date)criteria.get("dateTo");
				  lastDateGc.setTime(dateLast);
			  }
			 
			  if((Date)criteria.get("dateFrom") != null){
				  System.out.println("DATE FROM NOT NULL");
				  dateFrom = (Date)criteria.get("dateFrom");
			  }
			  
			  GregorianCalendar firstDateGc = new GregorianCalendar(
					  lastDateGc.get(Calendar.YEAR), 
					  lastDateGc.get(Calendar.MONTH), 
					  lastDateGc.getActualMinimum(Calendar.DATE), 
					  0, 0, 0);
			  
			  GregorianCalendar lastMonthDateGc2 = null;
			  
			  jbossQl.append("chk.chkAdCompany = " + AD_CMPNY + " ");
			  String supplierCode= (String)criteria.get("supplierCode");
			  
			  Collection apChecks = apCheckHome.getChkByCriteria(jbossQl.toString(), obj);	
			  System.out.println("b4 ENTER: " + supplierCode);
			  if(supplierCode!=null && supplierCode!=""){
				  System.out.println("ENTER");
				  LocalApSupplier apSupplier= apSupplierHome.findBySplSupplierCode((String)criteria.get("supplierCode"), AD_CMPNY);	

					  System.out.println("checksup: "+checksup);
					  ApRepApRegisterDetails mdetails2 = new ApRepApRegisterDetails();


					  mdetails2.setArgDocumentNumber("");
					  mdetails2.setArgReferenceNumber("");
					  mdetails2.setArgDescription("Balance Forward");
					  mdetails2.setArgSplSupplierCode(apSupplier.getSplSupplierCode());
					  System.out.println("checksup: "+apSupplier.getSplSupplierCode());
					  System.out.println("apSupplierBalance.getApSupplier().getSplSupplierCode(): "+apSupplier.getSplSupplierCode());
					  mdetails2.setArgSplSupplierClass(apSupplier.getApSupplierClass().getScName());
					  mdetails2.setArgSplName(apSupplier.getSplName());
					  mdetails2.setArgSplTin(apSupplier.getSplTin());
					  mdetails2.setArgSplAddress(apSupplier.getSplAddress());

//					  get balance OK
					  System.out.println("(Date)criteria.get('dateTo'): "+dateFrom);
					  Collection apSupplierBalances = 
						  apSupplierBalanceHome.findByBeforeSbDateAndSplCode(dateFrom, apSupplier.getSplCode(), AD_CMPNY);

					  if (apSupplierBalances.isEmpty()) {
						  balance=0;

					  } else {

						  ArrayList apSupplierBalanceList = new ArrayList(apSupplierBalances);
						  System.out.println("The content of arraylist is: " + apSupplierBalanceList);
						  Iterator itr = apSupplierBalances.iterator();
						  while (itr.hasNext()){
							  LocalApSupplierBalance supBal = (LocalApSupplierBalance)itr.next(); 
							  System.out.println("/************************apSupplierBalanceList*****************************/");
							  System.out.println("supBal: "+supBal.getSbCode());
							  System.out.println("supBal: "+supBal.getSbBalance());
							  System.out.println("supBal: "+supBal.getSbDate());
							  System.out.println("supBal: "+supBal.getSbAdCompany());
							  System.out.println("supBal: "+supBal.getApSupplier().getSplName());

							  System.out.println("/*****************************************************/");
						  }
						  System.out.println("apSupplierBalanceList.get(): "+apSupplierBalanceList.get(apSupplierBalanceList.size() - 1));

						  LocalApSupplierBalance apSupplierBalance = (LocalApSupplierBalance)apSupplierBalanceList.get(apSupplierBalanceList.size() - 1);

						  balance=(apSupplierBalance.getSbBalance()); 
						  System.out.println("balance una: " + balance);
						  mdetails2.setArgDate(apSupplierBalance.getSbDate());
//						  type
						  if(apSupplierBalance.getApSupplier().getApSupplierType() == null){

							  mdetails2.setArgSplSupplierType("UNDEFINE");

						  } else {

							  mdetails2.setArgSplSupplierType(apSupplierBalance.getApSupplier().getApSupplierType().getStName());

						  }

						  System.out.println("/************************BALANCE*****************************/");

						  //get beginning balance OK
						  if(issetBegBal)
						  {
							  GregorianCalendar lastMonthDateGc = new GregorianCalendar();
							  lastMonthDateGc.setTime(dateFrom);	
							  lastMonthDateGc.add(Calendar.MONTH, -1);

							  lastMonthDateGc2 = new GregorianCalendar(
									  lastMonthDateGc.get(Calendar.YEAR), 
									  lastMonthDateGc.get(Calendar.MONTH), 
									  lastMonthDateGc.getActualMaximum(Calendar.DATE), 
									  0, 0, 0);

							  Collection apBegBalances = apSupplierBalanceHome.findByBeforeSbDateAndSplCode(lastMonthDateGc2.getTime(), apSupplier.getSplCode(), AD_CMPNY);

							  Iterator x = apBegBalances.iterator();

							  while (x.hasNext())
							  {
								  LocalApSupplierBalance apBegBalance = (LocalApSupplierBalance)x.next();

								  double begBalance = apBegBalance.getSbBalance();

								  begbalance=(begBalance);

							  }

							  issetBegBal = false;

						  }


						  // get ptd balance OK (same as Net Transactions)

						  double priorBalance = 0d;

						  Collection apSupplierPriorBalances = apSupplierBalanceHome.findByBeforeSbDateAndSplCode(firstDateGc.getTime(), apSupplier.getSplCode(), AD_CMPNY);

						  if (!apSupplierPriorBalances.isEmpty()) {

							  ArrayList apSupplierPriorBalanceList = new ArrayList(apSupplierPriorBalances);

							  LocalApSupplierBalance apSupplierPriorBalance = (LocalApSupplierBalance)apSupplierPriorBalanceList.get(apSupplierPriorBalanceList.size() - 1);

							  priorBalance = apSupplierPriorBalance.getSbBalance();   	  

						  }

						  ptdbalance = (apSupplierBalance.getSbBalance() - priorBalance)-drAmnt;
						  System.out.println("details.setSlSplPtdBalance: " + (apSupplierBalance.getSbBalance() - priorBalance));
					  }




					  //get Interest Income

					  double interestIncome = 0d;

					  GregorianCalendar vouDateGc = new GregorianCalendar();
					  vouDateGc.setTime(dateFrom);	

					  GregorianCalendar firstDayOfMonth = new GregorianCalendar(
							  vouDateGc.get(Calendar.YEAR), 
							  vouDateGc.get(Calendar.MONTH), 
							  vouDateGc.getActualMinimum(Calendar.DATE), 
							  0, 0, 0);

					  String referenceNumber = "INT-" + EJBCommon.convertSQLDateToString(vouDateGc.getTime());

					  Collection apInterestIncomes = apVoucherHome.findByVouReferenceNumberAndSplName(
							  referenceNumber, apSupplier.getSplName(), AD_CMPNY);

					  Iterator itr = apInterestIncomes.iterator();

					  while (itr.hasNext()) {

						  LocalApVoucher apInterestIncome = (LocalApVoucher) itr.next();

						  System.out.println(apInterestIncome.getVouDate());
						  interestIncome = apInterestIncome.getVouAmountDue();

					  }

					  SplBalance = (balance + interestIncome);
					  System.out.println("interestIncome: " + interestIncome);
					  System.out.println("balance: " + balance);
					  System.out.println("SplBalance: " + SplBalance);
					  System.out.println("/**************************FINISH***************************/");


					 /* double AMNT_DUE2 = EJBCommon.roundIt(this.convertForeignToFunctionalCurrency(
							  apCheck.getGlFunctionalCurrency().getFcCode(),
							  apCheck.getGlFunctionalCurrency().getFcName(),
							  apCheck.getChkConversionDate(),
							  apCheck.getChkConversionRate(),
							  SplBalance, AD_CMPNY), adCompany.getGlFunctionalCurrency().getFcPrecision());
*/
					  mdetails2.setArgAmount(SplBalance);
					  mdetails2.setOrderBy(ORDER_BY);
					  mdetails2.setArgType("CHECK");
					  checksup = apSupplier.getSplSupplierCode();
					  if(SplBalance!=0){
						  list.add(mdetails2);	
					  }
					  
					  // }
				  //}
			  
			  }else{
				  System.out.println("ENTER2");
				  Collection apSuppliers= apSupplierHome.findEnabledSplAllOrderBySplSupplierCode(AD_CMPNY);
				  Iterator sup = apSuppliers.iterator();
				 
				  while(sup.hasNext()){
					  LocalApSupplier apSupplier = (LocalApSupplier)sup.next();
					  Iterator bal = apChecks.iterator();
					  //while(bal.hasNext()){
						 // LocalApCheck  apCheck = (LocalApCheck)bal.next();

						  // if(!al.contains(apCheck.getApSupplier().getSplSupplierCode())){
						  //  al.add(apCheck.getApSupplier().getSplSupplierCode());
						  System.out.println("checksup: "+checksup);
						  ApRepApRegisterDetails mdetails2 = new ApRepApRegisterDetails();


						  mdetails2.setArgDocumentNumber("");
						  mdetails2.setArgReferenceNumber("");
						  mdetails2.setArgDescription("Balance Forward");
						  mdetails2.setArgSplSupplierCode(apSupplier.getSplSupplierCode());
						  System.out.println("checksup: "+apSupplier.getSplSupplierCode());
						  System.out.println("apSupplierBalance.getApSupplier().getSplSupplierCode(): "+apSupplier.getSplSupplierCode());
						  mdetails2.setArgSplSupplierClass(apSupplier.getApSupplierClass().getScName());
						  mdetails2.setArgSplName(apSupplier.getSplName());
						  mdetails2.setArgSplTin(apSupplier.getSplTin());
						  mdetails2.setArgSplAddress(apSupplier.getSplAddress());


//						  get balance OK
						  System.out.println("(Date)criteria.get('dateTo'): "+dateFrom);
						  Collection apSupplierBalances = 
							  apSupplierBalanceHome.findByBeforeSbDateAndSplCode(dateFrom, apSupplier.getSplCode(), AD_CMPNY);

						  if (apSupplierBalances.isEmpty()) {
							  balance=0;

						  } else {

							  ArrayList apSupplierBalanceList = new ArrayList(apSupplierBalances);
							  System.out.println("The content of arraylist is: " + apSupplierBalanceList);
							  Iterator itr = apSupplierBalances.iterator();
							  while (itr.hasNext()){
								  LocalApSupplierBalance supBal = (LocalApSupplierBalance)itr.next(); 
								  System.out.println("/************************apSupplierBalanceList*****************************/");
								  System.out.println("supBal: "+supBal.getSbCode());
								  System.out.println("supBal: "+supBal.getSbBalance());
								  System.out.println("supBal: "+supBal.getSbDate());
								  System.out.println("supBal: "+supBal.getSbAdCompany());
								  System.out.println("supBal: "+supBal.getApSupplier().getSplName());

								  System.out.println("/*****************************************************/");
							  }
							  System.out.println("apSupplierBalanceList.get(): "+apSupplierBalanceList.get(apSupplierBalanceList.size() - 1));

							  LocalApSupplierBalance apSupplierBalance = (LocalApSupplierBalance)apSupplierBalanceList.get(apSupplierBalanceList.size() - 1);

							  balance=(apSupplierBalance.getSbBalance()); 
							  System.out.println("balance una: " + balance);
							  mdetails2.setArgDate(apSupplierBalance.getSbDate());
//							  type
							  if(apSupplierBalance.getApSupplier().getApSupplierType() == null){

								  mdetails2.setArgSplSupplierType("UNDEFINE");

							  } else {

								  mdetails2.setArgSplSupplierType(apSupplierBalance.getApSupplier().getApSupplierType().getStName());

							  }

							  System.out.println("/************************BALANCE*****************************/");

							  //get beginning balance OK
							  if(issetBegBal)
							  {
								  GregorianCalendar lastMonthDateGc = new GregorianCalendar();
								  lastMonthDateGc.setTime(dateFrom);	
								  lastMonthDateGc.add(Calendar.MONTH, -1);

								  lastMonthDateGc2 = new GregorianCalendar(
										  lastMonthDateGc.get(Calendar.YEAR), 
										  lastMonthDateGc.get(Calendar.MONTH), 
										  lastMonthDateGc.getActualMaximum(Calendar.DATE), 
										  0, 0, 0);

								  Collection apBegBalances = apSupplierBalanceHome.findByBeforeSbDateAndSplCode(lastMonthDateGc2.getTime(), apSupplier.getSplCode(), AD_CMPNY);

								  Iterator x = apBegBalances.iterator();

								  while (x.hasNext())
								  {
									  LocalApSupplierBalance apBegBalance = (LocalApSupplierBalance)x.next();

									  double begBalance = apBegBalance.getSbBalance();

									  begbalance=(begBalance);

								  }

								  issetBegBal = false;

							  }


							  // get ptd balance OK (same as Net Transactions)

							  double priorBalance = 0d;

							  Collection apSupplierPriorBalances = apSupplierBalanceHome.findByBeforeSbDateAndSplCode(firstDateGc.getTime(), apSupplier.getSplCode(), AD_CMPNY);

							  if (!apSupplierPriorBalances.isEmpty()) {

								  ArrayList apSupplierPriorBalanceList = new ArrayList(apSupplierPriorBalances);

								  LocalApSupplierBalance apSupplierPriorBalance = (LocalApSupplierBalance)apSupplierPriorBalanceList.get(apSupplierPriorBalanceList.size() - 1);

								  priorBalance = apSupplierPriorBalance.getSbBalance();   	  

							  }

							  ptdbalance = (apSupplierBalance.getSbBalance() - priorBalance)-drAmnt;
							  System.out.println("details.setSlSplPtdBalance: " + (apSupplierBalance.getSbBalance() - priorBalance));
						  }




						  //get Interest Income

						  double interestIncome = 0d;

						  GregorianCalendar vouDateGc = new GregorianCalendar();
						  vouDateGc.setTime(dateFrom);	

						  GregorianCalendar firstDayOfMonth = new GregorianCalendar(
								  vouDateGc.get(Calendar.YEAR), 
								  vouDateGc.get(Calendar.MONTH), 
								  vouDateGc.getActualMinimum(Calendar.DATE), 
								  0, 0, 0);

						  String referenceNumber = "INT-" + EJBCommon.convertSQLDateToString(vouDateGc.getTime());

						  Collection apInterestIncomes = apVoucherHome.findByVouReferenceNumberAndSplName(
								  referenceNumber, apSupplier.getSplName(), AD_CMPNY);

						  Iterator itr = apInterestIncomes.iterator();

						  while (itr.hasNext()) {

							  LocalApVoucher apInterestIncome = (LocalApVoucher) itr.next();

							  System.out.println(apInterestIncome.getVouDate());
							  interestIncome = apInterestIncome.getVouAmountDue();

						  }

						  SplBalance = (balance + interestIncome);
						  System.out.println("interestIncome: " + interestIncome);
						  System.out.println("balance: " + balance);
						  System.out.println("SplBalance: " + SplBalance);
						  System.out.println("/**************************FINISH***************************/");


						 /* double AMNT_DUE2 = EJBCommon.roundIt(this.convertForeignToFunctionalCurrency(
								  apCheck.getGlFunctionalCurrency().getFcCode(),
								  apCheck.getGlFunctionalCurrency().getFcName(),
								  apCheck.getChkConversionDate(),
								  apCheck.getChkConversionRate(),
								  SplBalance, AD_CMPNY), adCompany.getGlFunctionalCurrency().getFcPrecision());
	*/
						  mdetails2.setArgAmount(SplBalance);
						  mdetails2.setOrderBy(ORDER_BY);
						  mdetails2.setArgType("CHECK");
						  checksup = apSupplier.getSplSupplierCode();
						  if(SplBalance!=0){
							  list.add(mdetails2);	
						  }
						  
						  // }
					  //}
				  }
			  }

			  
			 
			  
			// for(int i=0; i < al.size(); i++){ 
			//	 al.remove(i);
			//  }
			  
			  if(SHOW_ENTRIES) {

				  Iterator i = apChecks.iterator();

				  while (i.hasNext()) {

					  boolean first = true;

					  LocalApCheck apCheck = (LocalApCheck)i.next();   	
					  System.out.println("apCheck.getChkType(): " + apCheck.getChkType());
					  Iterator d = adBrnchList.iterator();

					  while(d.hasNext()) {
						  AdBranchDetails mdetail = (AdBranchDetails) d.next();
						  if(apCheck.getChkType().equals("DIRECT")){
							  System.out.println("Direct");
							  Collection apDistributionRecords = apDistributionRecordHome.findChkByDateAndCoaAccountDescriptionAndSupplier((Date)criteria.get("dateTo"), apCheck.getApSupplier().getSplCode(), mdetail.getBrCode(), AD_CMPNY);

							  Iterator j = apDistributionRecords.iterator();

							  while(j.hasNext()) {

								  LocalApDistributionRecord apDistributionRecord = (LocalApDistributionRecord)j.next();


								  ApRepApRegisterDetails mdetails = new ApRepApRegisterDetails();
								  mdetails.setArgDate(apCheck.getChkDate());
								  mdetails.setArgDocumentNumber(apCheck.getChkDocumentNumber());
								  mdetails.setArgReferenceNumber(apCheck.getChkReferenceNumber());
								  mdetails.setArgDescription(apCheck.getChkDescription());
								  mdetails.setArgSplSupplierCode(apCheck.getApSupplier().getSplSupplierCode());
								  mdetails.setArgSplSupplierClass(apCheck.getApSupplier().getApSupplierClass().getScName());
								  mdetails.setArgSplName(apCheck.getChkSupplierName() == null ? apCheck.getApSupplier().getSplName() : apCheck.getChkSupplierName());
								  mdetails.setArgSplTin(apCheck.getApSupplier().getSplTin());
								  mdetails.setArgSplAddress(apCheck.getApSupplier().getSplAddress());
								  mdetails.setArgCheckType(apCheck.getChkType());

								  // type
								  if(apCheck.getApSupplier().getApSupplierType() == null){

									  mdetails.setArgSplSupplierType("UNDEFINE");

								  } else {

									  mdetails.setArgSplSupplierType(apCheck.getApSupplier().getApSupplierType().getStName());

								  }

								  Iterator d2 = adBrnchList.iterator();
								  boolean addCheck = false;
								  while (d2.hasNext()) {
									  AdBranchDetails mdetail2 = (AdBranchDetails) d2.next();
									  if(apCheck.getChkType().equals("DIRECT")){
										  System.out.println("Direct");
										  System.out.println("apCheck.getApSupplier().getSplCode(): " + apCheck.getApSupplier().getSplCode());

										  Collection apDistributionRecords2 = apDistributionRecordHome.findChkByDateAndCoaAccountDescriptionAndSupplier((Date)criteria.get("dateTo"), apCheck.getApSupplier().getSplCode(), mdetail2.getBrCode(), AD_CMPNY);

										  Iterator j2 = apDistributionRecords.iterator();
										  String chkDoc = apCheck.getChkDocumentNumber();
										  while(j2.hasNext()) {
											  addCheck = true;
											  System.out.println("DirectOR " + apCheck.getApSupplier().getSplCode());
											  System.out.println("/************************DISTRIBUTION*****************************/");
											  LocalApDistributionRecord apDistributionRecord2 = (LocalApDistributionRecord)j2.next();

											  if(apDistributionRecord.getDrDebit()!=0){
												  if(chkDoc==apDistributionRecord2.getApCheck().getChkDocumentNumber()){
													  drAmnt = drAmnt + apDistributionRecord2.getDrAmount();
												  }
												  System.out.println("drAmnt1" + drAmnt);
											  } else{
												  if(chkDoc==apDistributionRecord.getApCheck().getChkDocumentNumber()){
													  drAmnt = drAmnt - apDistributionRecord2.getDrAmount();
												  }
												  System.out.println("drAmnt2" + drAmnt);
											  }

											  if(first) {

												  double AMNT_DUE = EJBCommon.roundIt(this.convertForeignToFunctionalCurrency(
														  apCheck.getGlFunctionalCurrency().getFcCode(),
														  apCheck.getGlFunctionalCurrency().getFcName(),
														  apCheck.getChkConversionDate(),
														  apCheck.getChkConversionRate(),
														  drAmnt, AD_CMPNY), adCompany.getGlFunctionalCurrency().getFcPrecision());

												  if(apDistributionRecord.getApCheck().getChkDocumentNumber() == apCheck.getChkDocumentNumber()){
													  if(AMNT_DUE<0){
														  System.out.println("AMNT_DUE: " + AMNT_DUE);
														  mdetails.setArgAmount(AMNT_DUE);
													  }else{
														  System.out.println("AMNT_DUE: " + AMNT_DUE);
														  mdetails.setArgAmount(AMNT_DUE);
													  }
												  }

												  first = false;

											  }


										  }
									  }


								  }

								  mdetails.setOrderBy(ORDER_BY);

								  //distribution record details
								  mdetails.setArgDrCoaAccountNumber(apDistributionRecord.getGlChartOfAccount().getCoaAccountNumber());
								  mdetails.setArgDrCoaAccountDescription(apDistributionRecord.getGlChartOfAccount().getCoaAccountDescription());

								  if(apDistributionRecord.getDrDebit() == EJBCommon.TRUE) {

									  mdetails.setArgDrDebitAmount(apDistributionRecord.getDrAmount());

								  } else {

									  mdetails.setArgDrCreditAmount(apDistributionRecord.getDrAmount());

								  }

								  balance=0;
								  drAmnt = 0;
								  mdetails.setArgType("CHECK");
								  if(addCheck==true){
									  list.add(mdetails);
								  }
								  addCheck = false;

							  }

						  }
					  }

				  }

			  } else {
				  System.out.println("Direct 2-asd");

				  Iterator i = apChecks.iterator();

				  while(i.hasNext()) {
					  LocalApCheck apCheck = (LocalApCheck)i.next(); 

					  ApRepApRegisterDetails mdetails = new ApRepApRegisterDetails();

					  mdetails.setArgDate(apCheck.getChkDate());
					  System.out.println("wewe: " + apCheck.getChkDate());
					  mdetails.setArgDocumentNumber(apCheck.getChkDocumentNumber());
					  mdetails.setArgReferenceNumber(apCheck.getChkReferenceNumber());
					  mdetails.setArgDescription(apCheck.getChkDescription());
					  mdetails.setArgSplSupplierCode(apCheck.getApSupplier().getSplSupplierCode());
					  mdetails.setArgSplSupplierClass(apCheck.getApSupplier().getApSupplierClass().getScName());
					  mdetails.setArgSplName(apCheck.getChkSupplierName() == null ? apCheck.getApSupplier().getSplName() : apCheck.getChkSupplierName());
					  mdetails.setArgSplTin(apCheck.getApSupplier().getSplTin());
					  mdetails.setArgSplAddress(apCheck.getApSupplier().getSplAddress());
					  mdetails.setArgCheckType(apCheck.getChkType());
					  System.out.println("apCheck.getChkDocumentNumber(): " + apCheck.getChkDocumentNumber());
					  System.out.println("apCheck.getChkType(): " + apCheck.getChkType());
//					 type
					  if(apCheck.getApSupplier().getApSupplierType() == null){

						  mdetails.setArgSplSupplierType("UNDEFINE");

					  } else {

						  mdetails.setArgSplSupplierType(apCheck.getApSupplier().getApSupplierType().getStName());

					  }
					  Iterator d = adBrnchList.iterator();
					  boolean addCheck = false;
					  while (d.hasNext()) {
						  AdBranchDetails mdetail2 = (AdBranchDetails) d.next();
						  if(apCheck.getChkType().equals("DIRECT")){
							  System.out.println("Direct");
							  System.out.println("apCheck.getApSupplier().getSplCode(): " + apCheck.getApSupplier().getSplCode());

							  Collection apDistributionRecords = apDistributionRecordHome.findChkByDateAndCoaAccountDescriptionAndSupplier((Date)criteria.get("dateTo"), apCheck.getApSupplier().getSplCode(), mdetail2.getBrCode(), AD_CMPNY);
							  
							  Iterator j = apDistributionRecords.iterator();
							  String chkDoc = apCheck.getChkDocumentNumber();
							  while(j.hasNext()) {
								  addCheck = true;
								  System.out.println("DirectOR " + apCheck.getApSupplier().getSplCode());
								  System.out.println("/************************DISTRIBUTION*****************************/");
								  LocalApDistributionRecord apDistributionRecord = (LocalApDistributionRecord)j.next();
								  
								  System.out.println("chkDoc: "+chkDoc);
								  System.out.println("apDistributionRecord.getApCheck().getChkDocumentNumber(): "+apDistributionRecord.getApCheck().getChkDocumentNumber());
								  if(apDistributionRecord.getDrDebit()!=0){
									  if(chkDoc==apDistributionRecord.getApCheck().getChkDocumentNumber()){
										  drAmnt = drAmnt + apDistributionRecord.getDrAmount();
									  }
									  System.out.println("drAmnt1" + drAmnt);
								  } else{
									  if(chkDoc==apDistributionRecord.getApCheck().getChkDocumentNumber()){
										  drAmnt = drAmnt - apDistributionRecord.getDrAmount();
									  }
									  System.out.println("drAmnt2" + drAmnt);
								  }
								  double samp =0;
								  double AMNT_DUE = EJBCommon.roundIt(this.convertForeignToFunctionalCurrency(
										  apCheck.getGlFunctionalCurrency().getFcCode(),
										  apCheck.getGlFunctionalCurrency().getFcName(),
										  apCheck.getChkConversionDate(),
										  apCheck.getChkConversionRate(),
										  drAmnt, AD_CMPNY), adCompany.getGlFunctionalCurrency().getFcPrecision());
								  
								  if(apDistributionRecord.getApCheck().getChkDocumentNumber() == apCheck.getChkDocumentNumber()){
									  if(AMNT_DUE<0){
										  System.out.println("AMNT_DUE: " + AMNT_DUE);
										  mdetails.setArgAmount(AMNT_DUE);
									  }else{
										  samp = samp + AMNT_DUE;
										  System.out.println("samp: " + samp);
										  mdetails.setArgAmount(AMNT_DUE);
									  }
								  }

							  }
						  }


					  }
					 
					  
					  balance=0;
					  drAmnt = 0;
					  mdetails.setOrderBy(ORDER_BY);
					  mdetails.setArgType("CHECK");
					  if(addCheck==true){
						  list.add(mdetails);
					  }
					  addCheck = false;
					  //details.setSlSplAmntDC(drAmnt);
					 // details.setSlSplInterestIncome(interestIncome);
					  //System.out.println("drAmnt: "+drAmnt);
					  

				  }


			  } 

		  }
		  
		  if(list.isEmpty() || list.size() == 0) {

			  System.out.print("dito ba wala");
			  throw new GlobalNoRecordFoundException();

		  }

		  // sort
		  
		  		  
		  if (GROUP_BY.equals("SUPPLIER CODE")) {
       	  	
		  	Collections.sort(list, ApRepApRegisterDetails.SupplierCodeComparator);
       	  	
       	  } else if (GROUP_BY.equals("SUPPLIER TYPE")) {
       	  	
       	  	Collections.sort(list, ApRepApRegisterDetails.SupplierTypeComparator);
       	  	
       	  } else if (GROUP_BY.equals("SUPPLIER CLASS")) {
       	  	
       	  	Collections.sort(list, ApRepApRegisterDetails.SupplierClassComparator);
       	  	
       	  } else {
       	  	
       	  	Collections.sort(list, ApRepApRegisterDetails.NoGroupComparator);
       	  	
       	  }
		  
		  list = setInterestIncome(list, GROUP_BY, criteria.containsKey("dateFrom") ? (Date)criteria.get("dateFrom") : null, AD_CMPNY);
		  
		  // Collections.sort(list, ApRepApRegisterDetails.NoGroupComparator);
		  if(SHOW_ENTRIES){
			  if(SUMMARIZE) {
				  System.out.println("TAMA PA RIN");
				  Collections.sort(list, ApRepApRegisterDetails.CoaAccountNumberComparator);
				  
			  }
		  }
		  
		  System.out.print("passeD");
		  return list;
		      
	  } catch (GlobalNoRecordFoundException ex) {
	  	 
	  	  throw ex;
	  	
	  } catch (Exception ex) {
	  	

	  	  ex.printStackTrace();
	  	  throw new EJBException(ex.getMessage());
	  	
	  }
        
    }
    
    
    /**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public ArrayList getApOpenVbAll(String DPRTMNT, Integer AD_BRNCH, Integer AD_CMPNY) {

		Debug.print("ApRepApRegisterControllerBean getApOpenVbAll");

		LocalApVoucherBatchHome apVoucherBatchHome = null;               

		ArrayList list = new ArrayList();

		// Initialize EJB Home

		try {

			apVoucherBatchHome = (LocalApVoucherBatchHome)EJBHomeFactory.
			lookUpLocalHome(LocalApVoucherBatchHome.JNDI_NAME, LocalApVoucherBatchHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}        

		try {
			Collection apVoucherBatches = null;
			
			if(DPRTMNT.equals("") || DPRTMNT.equals("default") || DPRTMNT.equals("NO RECORD FOUND")){
				System.out.println("------------>");
				apVoucherBatches = apVoucherBatchHome.findOpenVbByVbType("VOUCHER", AD_BRNCH, AD_CMPNY);
				
			} else {
				System.out.println("------------>else");
				apVoucherBatches = apVoucherBatchHome.findOpenVbByVbTypeDepartment("VOUCHER", DPRTMNT, AD_BRNCH, AD_CMPNY);

			}
			
			Iterator i = apVoucherBatches.iterator();

			while (i.hasNext()) {

				LocalApVoucherBatch apVoucherBatch = (LocalApVoucherBatch)i.next();

				list.add(apVoucherBatch.getVbName());

			}

			return list;

		} catch (Exception ex) {

			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());

		}

	}

    
    
    
    
    
    
    
	/**
	* @ejb:interface-method view-type="remote"
	* @jboss:method-attributes read-only="true"
	**/
	public com.util.AdCompanyDetails getAdCompany(Integer AD_CMPNY) {
	
	  Debug.print("ApRepApRegisterControllerBean getAdCompany");      
	  
	  LocalAdCompanyHome adCompanyHome = null;
	        
	  // Initialize EJB Home
	    
	  try {
	        
	       adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
	           lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
	        
	  } catch (NamingException ex) {
	        
	      throw new EJBException(ex.getMessage());
	        
	  }
	
	  try {
	      
	     LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
	     
	     AdCompanyDetails details = new AdCompanyDetails();
	     details.setCmpName(adCompany.getCmpName());
	     
	     return details;  	
	  	       	
	  } catch (Exception ex) {
	  	
	  	 Debug.printStackTrace(ex);
	  	 throw new EJBException(ex.getMessage());
	  	  
	  }
	  
	} 
	
	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/   
	public ArrayList getAdBrResAll(Integer RS_CODE, Integer AD_CMPNY) 
		throws GlobalNoRecordFoundException{
	    
	    Debug.print("ApRepApRegisterControllerBean getAdBrResAll");
	    
	    LocalAdBranchResponsibilityHome adBranchResponsibilityHome = null;
	    LocalAdBranchHome adBranchHome = null;
	    
	    LocalAdBranchResponsibility adBranchResponsibility = null;
	    LocalAdBranch adBranch = null;
	    
	    Collection adBranchResponsibilities = null;
	    
	    ArrayList list = new ArrayList();
	    
	    // Initialize EJB Home
	    
	    try {
	        
	        adBranchResponsibilityHome = (LocalAdBranchResponsibilityHome)EJBHomeFactory.
	        lookUpLocalHome(LocalAdBranchResponsibilityHome.JNDI_NAME, LocalAdBranchResponsibilityHome.class);
	        adBranchHome = (LocalAdBranchHome)EJBHomeFactory.
	        lookUpLocalHome(LocalAdBranchHome.JNDI_NAME, LocalAdBranchHome.class);
	        
	    } catch (NamingException ex) {
	        
	        throw new EJBException(ex.getMessage());
	        
	    }
	    
	    try {
	        
	        adBranchResponsibilities = adBranchResponsibilityHome.findByAdResponsibility(RS_CODE, AD_CMPNY);
	        
	    } catch (FinderException ex) {
	        
	    } catch (Exception ex) {
	        
	        throw new EJBException(ex.getMessage());
	    }
	    
	    if (adBranchResponsibilities.isEmpty()) {
	        
	        throw new GlobalNoRecordFoundException();
	        
	    }
	    
	    try {
	        
	        Iterator i = adBranchResponsibilities.iterator();
	        
	        while(i.hasNext()) {
	            
	            adBranchResponsibility = (LocalAdBranchResponsibility)i.next();
	            
	            adBranch = adBranchResponsibility.getAdBranch();
	            
	            AdBranchDetails details = new AdBranchDetails();
	            
	            details.setBrCode(adBranch.getBrCode());
	            details.setBrBranchCode(adBranch.getBrBranchCode());
	            details.setBrName(adBranch.getBrName());
	            details.setBrHeadQuarter(adBranch.getBrHeadQuarter());
	            
	            list.add(details);
	            
	        }	               
	        
	    } catch (Exception ex) {
	        
	        throw new EJBException(ex.getMessage());
	    }
	    
	    return list;
	    
	}
	
	// private methods
	private double convertForeignToFunctionalCurrency(Integer FC_CODE, String FC_NM, 
	    Date CONVERSION_DATE, double CONVERSION_RATE, double AMOUNT, Integer AD_CMPNY) {
	    	
	    Debug.print("ApRepApRegisterControllerBean convertForeignToFunctionalCurrency");
	    
	    
        LocalGlFunctionalCurrencyRateHome glFunctionalCurrencyRateHome = null;
        LocalAdCompanyHome adCompanyHome = null;
         
        LocalAdCompany adCompany = null;
                 
        // Initialize EJB Homes
         
        try {
         	
            glFunctionalCurrencyRateHome = (LocalGlFunctionalCurrencyRateHome)EJBHomeFactory.
               lookUpLocalHome(LocalGlFunctionalCurrencyRateHome.JNDI_NAME, LocalGlFunctionalCurrencyRateHome.class);                         
            adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
               lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);             
                   
         } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
         }
         
         // get company and extended precision
         
         try {
         	
             adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
         	             
         } catch (Exception ex) {
         	
             throw new EJBException(ex.getMessage());
         	
         }	     
        
         
         // Convert to functional currency if necessary
         if (CONVERSION_RATE != 1 && CONVERSION_RATE != 0 ) {
         	         
             AMOUNT = AMOUNT / CONVERSION_RATE;
             	
         }

         return EJBCommon.roundIt(AMOUNT, adCompany.getGlFunctionalCurrency().getFcPrecision());
         
	}

	private ArrayList setInterestIncome(ArrayList list, String GROUP_BY, Date dateFrom, Integer AD_CMPNY) {

		Debug.print("ApRepApRegisterControllerBean setInterestIncome");

		LocalApSupplierHome apSupplierHome = null;
		LocalApSupplierBalanceHome apSupplierBalanceHome = null;
		LocalApVoucherHome apVoucherHome = null;
		
		// Initialize EJB Home

		try {

			apSupplierHome = (LocalApSupplierHome)EJBHomeFactory.
			lookUpLocalHome(LocalApSupplierHome.JNDI_NAME, LocalApSupplierHome.class);       
			apSupplierBalanceHome = (LocalApSupplierBalanceHome)EJBHomeFactory.
			lookUpLocalHome(LocalApSupplierBalanceHome.JNDI_NAME, LocalApSupplierBalanceHome.class);       
			apVoucherHome = (LocalApVoucherHome)EJBHomeFactory.
			lookUpLocalHome(LocalApVoucherHome.JNDI_NAME, LocalApVoucherHome.class);       
			
		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}

		try {
			
			// set running balance, beginning balance
			// set interest income
			
			double ARG_BLNC = 0d;
			double ARG_BGNNG_BLNC = 0d;
			double ARG_PREV_INTRST_INCM = 0d;
			
			String group = null;
			
			LocalApSupplier apSupplier = null;
			Collection apSupplierBalances = null;
			Collection apInterestVouchers = null;

			Iterator listIter = list.iterator();
			System.out.print("1");
			while (listIter.hasNext()) {

				ApRepApRegisterDetails details = (ApRepApRegisterDetails)listIter.next();
				System.out.print("2");
				if (GROUP_BY.equals("SUPPLIER CODE")) {
					
					if (group == null || !group.equals(details.getArgSplSupplierCode())) {
						System.out.print("2.1");
						ARG_BLNC = 0d;
						ARG_BGNNG_BLNC = 0d;
						ARG_PREV_INTRST_INCM = 0d;

						apSupplier = apSupplierHome.findBySplSupplierCode(details.getArgSplSupplierCode(), AD_CMPNY);
						System.out.print("2.2");	
						if(dateFrom != null) {
							System.out.print("2.2.1");
							// get beginning balance

							GregorianCalendar gc = new GregorianCalendar();
							gc.setTime(dateFrom);	
							
							apSupplierBalances = apSupplierBalanceHome.findByBeforeSbDateAndSplCode(
									gc.getTime(), apSupplier.getSplCode(), AD_CMPNY);

							if(!apSupplierBalances.isEmpty()) {
								System.out.print("2.2.1.1");
								ArrayList apSupplierBalanceList = new ArrayList(apSupplierBalances);
								
								LocalApSupplierBalance apSupplierBalance = (LocalApSupplierBalance)apSupplierBalanceList.get(apSupplierBalanceList.size() - 1);

								ARG_BGNNG_BLNC = apSupplierBalance.getSbBalance();	  

							}
							
							// get prev month interest income

							String referenceNumber = "INT-" + EJBCommon.convertSQLDateToString(dateFrom); 
							System.out.print("2.2.2");
							apInterestVouchers = apVoucherHome.findByVouReferenceNumberAndVouPostedAndSplName(
									referenceNumber, EJBCommon.TRUE, apSupplier.getSplName(), AD_CMPNY);
							System.out.print("2.2.3");
							Iterator i = apInterestVouchers.iterator();
							while (i.hasNext()) {
								System.out.print("2.2.3.1");
								LocalApVoucher apInterestVoucher = (LocalApVoucher) i.next();
								ARG_PREV_INTRST_INCM += apInterestVoucher.getVouAmountDue();

							}

						}
						System.out.print("2.3");
						ARG_BLNC += ARG_BGNNG_BLNC;
						group = details.getArgSplSupplierCode();

					}
					
				} else if (GROUP_BY.equals("SUPPLIER TYPE")) {
					System.out.print("2.4");
					if (group == null || !group.equals(details.getArgSplSupplierType())) {
						System.out.print("2.4.1");
						ARG_BLNC = 0d;
						group = details.getArgSplSupplierType();
					}

				} else if (GROUP_BY.equals("SUPPLIER CLASS")) {
					System.out.print("2.5");
					if (group == null || !group.equals(details.getArgSplSupplierClass())) {
						System.out.print("2.5.1");
						ARG_BLNC = 0d;
						group = details.getArgSplSupplierClass();
					}

				} 
				System.out.print("2.6");
				if(details.getArgType().equals("CHECK")) {
					System.out.print("2.6.1");
					ARG_BLNC -= details.getArgAmount();

				} else {
					System.out.print("2.6.2");
					ARG_BLNC += details.getArgAmount();

				}
				System.out.print("2.7");
				details.setArgBalance(ARG_BLNC);
				
				System.out.print("2.7  s");
				details.setArgBeginningBalance(ARG_BGNNG_BLNC);
				
				System.out.print("2.7  a");
				details.setArgPrevInterestIncome(ARG_PREV_INTRST_INCM);
				
				System.out.print("2.7  v");
				
			}

			return list;

		} catch (Exception ex) {

			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());

		}

	}
    
    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public void getUpdateVoucherReferenceByDocumentNumber(String VOU_DCMNT_NMBR, String VOU_RFRNC_NMBR, Integer AD_CMPNY)
            throws GlobalNoRecordFoundException {
     
        Debug.print("ApRepApRegisterControllerBean getUpdateVoucherReferenceByDocumentNumber");
        
        LocalApVoucherHome apVoucherHome = null;
        LocalApVoucherPaymentScheduleHome apVoucherPaymentScheduleHome = null;
        LocalApCheckHome apCheckHome = null;
        LocalAdCompanyHome adCompanyHome = null;
        LocalApDistributionRecordHome apDistributionRecordHome = null;
        LocalApSupplierBalanceHome apSupplierBalanceHome = null;
        LocalApSupplierHome apSupplierHome = null;
        
        ArrayList list = new ArrayList();
     
        //initialized EJB Home
        
        try {

            apVoucherHome = (LocalApVoucherHome)EJBHomeFactory.
                    lookUpLocalHome(LocalApVoucherHome.JNDI_NAME, LocalApVoucherHome.class);
           

        } catch (NamingException ex) 	{
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try { 
		

            LocalApVoucher apVoucher = apVoucherHome.findByVouDocumentNumber(VOU_DCMNT_NMBR,AD_CMPNY);
           
         
            apVoucher.setVouReferenceNumber(VOU_RFRNC_NMBR);
     
	
	 
	  	
        } catch (Exception ex) {
	  	

	  	  ex.printStackTrace();
	  	  throw new EJBException(ex.getMessage());
	  	
        }
        
        
        
  
    }  
        
        
    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList getAdBaAll(Integer AD_BRNCH, Integer AD_CMPNY) {
                    
        Debug.print("ApRepApRegisterControllerBean getAdBaAll");
        
        LocalAdBankAccountHome adBankAccountHome = null;               
        
        ArrayList list = new ArrayList();
        
        // Initialize EJB Home
        
        try {
            
            adBankAccountHome = (LocalAdBankAccountHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdBankAccountHome.JNDI_NAME, LocalAdBankAccountHome.class);
            
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }        
        
        try {
        	
        	Collection adBankAccounts = adBankAccountHome.findEnabledBaAll(AD_BRNCH, AD_CMPNY);
        	
        	Iterator i = adBankAccounts.iterator();
        	
        	while (i.hasNext()) {
        		
        		LocalAdBankAccount adBankAccount = (LocalAdBankAccount)i.next();
        		
        		list.add(adBankAccount.getBaName());
        		
        	}
        	
        	return list;
        	
        } catch (Exception ex) {
        	
        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());
        	
        }
            
    }
    // SessionBean methods

    /**
     * @ejb:create-method view-type="remote"
     **/
    public void ejbCreate() throws CreateException {

       Debug.print("ApRepApRegisterControllerBean ejbCreate");
      
    }
}
