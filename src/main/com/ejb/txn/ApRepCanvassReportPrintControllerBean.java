
/*
 * ApRepCanvassReportPrintControllerBean.java
 *
 * Created on April 24, 2006 3:31 PM
 * @author  Aliza D.J. Anos
 */

package com.ejb.txn;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;
import javax.naming.NamingException;

import com.ejb.ad.LocalAdCompany;
import com.ejb.ad.LocalAdCompanyHome;
import com.ejb.ap.LocalApCanvass;
import com.ejb.ap.LocalApPurchaseOrderLine;
import com.ejb.ap.LocalApPurchaseOrderLineHome;
import com.ejb.ap.LocalApPurchaseRequisition;
import com.ejb.ap.LocalApPurchaseRequisitionHome;
import com.ejb.ap.LocalApPurchaseRequisitionLine;
import com.ejb.exception.GlobalNoRecordFoundException;
import com.util.AbstractSessionBean;
import com.util.AdCompanyDetails;
import com.util.ApRepCanvassReportPrintDetails;
import com.util.Debug;
import com.util.EJBCommon;
import com.util.EJBHomeFactory;

/**
 * @ejb:bean name="ApRepCanvassReportPrintControllerEJB"
 *           display-name="Used for printing canvass reports"
 *           type="Stateless"
 *           view-type="remote"
 *           jndi-name="ejb/ApRepCanvassReportPrintControllerEJB"
 *
 * @ejb:interface remote-class="com.ejb.txn.ApRepCanvassReportPrintController"
 *                extends="javax.ejb.EJBObject"
 *
 * @ejb:home remote-class="com.ejb.txn.ApRepCanvassReportPrintControllerHome"
 *           extends="javax.ejb.EJBHome"
 *
 * @ejb:transaction type="Required"
 * 
*/

public class ApRepCanvassReportPrintControllerBean extends AbstractSessionBean {
    
	/**
     * @ejb:interface-method view-type="remote"
     **/
    public void selectApRepCanvassPerTotal(ArrayList prCodeList, Integer AD_BRNCH, Integer AD_CMPNY) 
         {
        
        Debug.print("ApRepCanvassReportPrintControllerBean selectApRepCanvassPerTotal");
        
        LocalApPurchaseRequisitionHome apPurchaseRequisitionHome = null;
        LocalApPurchaseOrderLineHome apPurchaseOrderLineHome = null;

        ArrayList list = new ArrayList();
                
        // Initialize EJB Home
        
        try {
            
        	apPurchaseRequisitionHome = (LocalApPurchaseRequisitionHome)EJBHomeFactory.
                lookUpLocalHome(LocalApPurchaseRequisitionHome .JNDI_NAME, LocalApPurchaseRequisitionHome .class);
        	apPurchaseOrderLineHome = (LocalApPurchaseOrderLineHome)EJBHomeFactory.
            	lookUpLocalHome(LocalApPurchaseOrderLineHome .JNDI_NAME, LocalApPurchaseOrderLineHome .class);


        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }        
        
        try {
        	
        	Iterator i = prCodeList.iterator();
        	
        	while (i.hasNext()) {
        		
        		Integer PR_CODE = (Integer) i.next();
        		
        		LocalApPurchaseRequisition apPurchaseRequisition = null;
        		
        		try {
        			
        			apPurchaseRequisition = apPurchaseRequisitionHome.findByPrimaryKey(PR_CODE);
        			
        		} catch (FinderException ex) {
        			
        			continue;
        			
        		}	        	
        		
        		java.util.HashMap hm = new java.util.HashMap();
        		
        		// get purchase requisition lines
        		
        		Collection apPurchaseRequisitionLines = apPurchaseRequisition.getApPurchaseRequisitionLines();        	            
        		
        		Iterator prlIter = apPurchaseRequisitionLines.iterator();
        		
        		while (prlIter.hasNext()) {
        			
        			LocalApPurchaseRequisitionLine apPurchaseRequisitionLine = (LocalApPurchaseRequisitionLine)prlIter.next();
        				Collection apCanvasses = apPurchaseRequisitionLine.getApCanvasses();
        				Iterator apCnvIter = apCanvasses.iterator();
        				while (apCnvIter.hasNext()) {
        					
        					LocalApCanvass apCanvass = (LocalApCanvass)apCnvIter.next();
        					Double cnvAmount = (Double)hm.get(apCanvass.getApSupplier().getSplCode());
        					double dAmount = 0;
        					if (cnvAmount != null) dAmount = cnvAmount.doubleValue();
        					hm.put(apCanvass.getApSupplier().getSplCode(), new Double(dAmount + apCanvass.getCnvAmount()));
        					
        				}
        			
        		}
        		
        		Integer currentSupplier = null;
            	java.util.Set hmList = hm.keySet();
        		Iterator hmIter = hmList.iterator();
        		while (hmIter.hasNext()) {
        			Integer supplierCode = (Integer)hmIter.next();
        			if (currentSupplier == null) currentSupplier = supplierCode;
        			else {
        				Double currentValue = (Double)hm.get(currentSupplier);
        				Double value = (Double)hm.get(supplierCode);
        				if (value.doubleValue() < currentValue.doubleValue()) currentSupplier = supplierCode;
        			}
        		}
        		
        		if (currentSupplier != null) {
	        		
	        		prlIter = apPurchaseRequisitionLines.iterator();
	        		
	        		while (prlIter.hasNext()) {
	        			
	        			LocalApPurchaseRequisitionLine apPurchaseRequisitionLine = (LocalApPurchaseRequisitionLine)prlIter.next();
	        				Collection apCanvasses = apPurchaseRequisitionLine.getApCanvasses();
	        				Iterator apCnvIter = apCanvasses.iterator();
	        				while (apCnvIter.hasNext()) {
	        					
	        					LocalApCanvass apCanvass = (LocalApCanvass)apCnvIter.next();
	        					if (apCanvass.getApSupplier().getSplCode().equals(currentSupplier)) {
	        						apCanvass.setCnvPo(EJBCommon.TRUE);
	        					} else {
	        						apCanvass.setCnvPo(EJBCommon.FALSE);
	        					}
	        					
	        				}
	        			
	        		}
	        		
        		}
        		
        	}
	        
        	
    		
    		
    			
        } catch (Exception ex) {
        	
        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());
        	
        }
            
    }
    
    /**
     * @ejb:interface-method view-type="remote"
     **/
    public ArrayList executeApRepCanvassReportPrint(ArrayList prCodeList, Integer AD_BRNCH, Integer AD_CMPNY) 
        throws GlobalNoRecordFoundException {
        
        Debug.print("ApRepCanvassReportPrintControllerBean executeApRepCanvassReportPrint");
        
        LocalApPurchaseRequisitionHome apPurchaseRequisitionHome = null;
        LocalApPurchaseOrderLineHome apPurchaseOrderLineHome = null;

        ArrayList list = new ArrayList();
                
        // Initialize EJB Home
        
        try {
            
        	apPurchaseRequisitionHome = (LocalApPurchaseRequisitionHome)EJBHomeFactory.
                lookUpLocalHome(LocalApPurchaseRequisitionHome .JNDI_NAME, LocalApPurchaseRequisitionHome .class);
        	apPurchaseOrderLineHome = (LocalApPurchaseOrderLineHome)EJBHomeFactory.
            	lookUpLocalHome(LocalApPurchaseOrderLineHome .JNDI_NAME, LocalApPurchaseOrderLineHome .class);

        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }        
        
        try {
        	
        	Iterator i = prCodeList.iterator();
        	
        	while (i.hasNext()) {
        		
        		Integer PR_CODE = (Integer) i.next();
        		
        		LocalApPurchaseRequisition apPurchaseRequisition = null;
        		
        		try {
        			
        			apPurchaseRequisition = apPurchaseRequisitionHome.findByPrimaryKey(PR_CODE);
        			
        		} catch (FinderException ex) {
        			
        			continue;
        			
        		}	        	
        		
        		// get purchase requisition lines
        		
        		Collection apPurchaseRequisitionLines = apPurchaseRequisition.getApPurchaseRequisitionLines();        	            
        		
        		Iterator prlIter = apPurchaseRequisitionLines.iterator();
        		
        		while (prlIter.hasNext()) {
        			
        			LocalApPurchaseRequisitionLine apPurchaseRequisitionLine = (LocalApPurchaseRequisitionLine)prlIter.next();
        			
        			//get last delivery date of item
        			
        			boolean first = true;
        			boolean asteriskSet = false;
        			
        			Date LST_DLVRY_DT = null;
        			
        			Collection apPurchaseOrderLines = null;
    				
    				try {
						
    					apPurchaseOrderLines = apPurchaseOrderLineHome.findByPlIlCodeAndPoReceivingAndPoPostedAndBrCode(
								apPurchaseRequisitionLine.getInvItemLocation().getIlCode(), EJBCommon.TRUE, EJBCommon.TRUE, 
								AD_BRNCH, AD_CMPNY);
    					
    					if(!apPurchaseOrderLines.isEmpty() && apPurchaseOrderLines.size() > 0) {
    						
    						LocalApPurchaseOrderLine apPurchaseOrderLine = (LocalApPurchaseOrderLine)apPurchaseOrderLines.iterator().next();
    						
    						LST_DLVRY_DT = apPurchaseOrderLine.getApPurchaseOrder().getPoDate();
    						
    					}
    					
					} catch(FinderException ex) {
						
					}
					
        			Collection apCanvasses = apPurchaseRequisitionLine.getApCanvasses();
        			
        			Iterator cnvIter = apCanvasses.iterator();
        			
        			while(cnvIter.hasNext()) {
        				
        				LocalApCanvass apCanvass = (LocalApCanvass)cnvIter.next();
        				
        				ApRepCanvassReportPrintDetails details = new ApRepCanvassReportPrintDetails();
        				
        				//pr details
        				details.setCrpPrNumber(apCanvass.getApPurchaseRequisitionLine().getApPurchaseRequisition().getPrNumber());
        				details.setCrpPrDate(apCanvass.getApPurchaseRequisitionLine().getApPurchaseRequisition().getPrDate());
        				details.setCrpPrDescription(apCanvass.getApPurchaseRequisitionLine().getApPurchaseRequisition().getPrDescription());
        				details.setCrpPrFcSymbol(apCanvass.getApPurchaseRequisitionLine().getApPurchaseRequisition().getGlFunctionalCurrency().getFcSymbol());
        				
        				//pr line details
        				details.setCrpPrlIiName(apCanvass.getApPurchaseRequisitionLine().getInvItemLocation().getInvItem().getIiName());
        				details.setCrpPrlIiDescription(apCanvass.getApPurchaseRequisitionLine().getInvItemLocation().getInvItem().getIiDescription());
        				details.setCrpPrlLocName(apCanvass.getApPurchaseRequisitionLine().getInvItemLocation().getInvLocation().getLocName());
        				details.setCrpPrlUomName(apCanvass.getApPurchaseRequisitionLine().getInvUnitOfMeasure().getUomName());
        				
        				if(first) {
        					
        					if(LST_DLVRY_DT != null) {
            					
            					details.setCrpPrlLastDeliveryDate(EJBCommon.convertSQLDateToString(LST_DLVRY_DT));
            					
            				} else {
            					
            					details.setCrpPrlLastDeliveryDate("** ** ****");
            					asteriskSet = true;
            					
            				}
        					
        					first = false;
        					
        				} else {
        					
        					if(!asteriskSet) {
            					
            					details.setCrpPrlLastDeliveryDate("** ** ****");
            					asteriskSet = true;
            					
            				}
        					
        				}
        				
        				//canvass details
        				details.setCrpPrlCnvRemarks(apCanvass.getCnvRemarks());
        				details.setCrpPrlCnvSplSupplierCode(apCanvass.getApSupplier().getSplSupplierCode());
        				details.setCrpPrlCnvSplName(apCanvass.getApSupplier().getSplName());
        				details.setCrpPrlCnvQuantity(apCanvass.getCnvQuantity());
        				details.setCrpPrlCnvUnitPrice(apCanvass.getCnvUnitCost());
        				details.setCrpPrlCnvAmount(apCanvass.getCnvAmount());
        				details.setCrpPrlPO(apCanvass.getCnvPo());
        				
        				list.add(details);
        				
        			}
        			
        		}
        		
        	}
	        
	        if (list.isEmpty()) {
	        	
	        	throw new GlobalNoRecordFoundException();
	        	
	        }        	       	
        	
        	return list;    
        	
        } catch (GlobalNoRecordFoundException ex) {
        	
        	throw ex;
        	        	
        } catch (Exception ex) {
        	
        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());
        	
        }
            
    }
    
    /**
	* @ejb:interface-method view-type="remote"
	* @jboss:method-attributes read-only="true"
	**/
	public AdCompanyDetails getAdCompany(Integer AD_CMPNY) {
	
	  Debug.print("ApRepCanvassReportPrintControllerBean getAdCompany");      
	  
	  LocalAdCompanyHome adCompanyHome = null;
	        
	  // Initialize EJB Home
	    
	  try {
	        
	       adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
	           lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
	        
	  } catch (NamingException ex) {
	        
	      throw new EJBException(ex.getMessage());
	        
	  }
	
	  try {
	      
	     LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
	     
	     AdCompanyDetails details = new AdCompanyDetails();
	     
	     details.setCmpName(adCompany.getCmpName());
	     
	     return details;  	
	  	       	
	  } catch (Exception ex) {
	  	
	  	 Debug.printStackTrace(ex);
	  	 throw new EJBException(ex.getMessage());
	  	  
	  }
	  
	}
    
    // SessionBean methods

    /**
     * @ejb:create-method view-type="remote"
     **/
    public void ejbCreate() throws CreateException {

       Debug.print("ApRepCanvassReportPrintControllerBean ejbCreate");
      
    }
}
