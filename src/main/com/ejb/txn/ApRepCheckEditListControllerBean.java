
/*
 * ApRepCheckEditListControllerBean.java
 *
 * Created on June 23, 2004, 1:47 PM
 *
 * @author  Neil Andrew M. Ajero
 */

package com.ejb.txn;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;
import javax.naming.NamingException;

import com.ejb.ad.LocalAdCompany;
import com.ejb.ad.LocalAdCompanyHome;
import com.ejb.ap.LocalApAppliedVoucher;
import com.ejb.ap.LocalApCheck;
import com.ejb.ap.LocalApCheckHome;
import com.ejb.ap.LocalApDistributionRecord;
import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.genfld.LocalGenSegmentHome;
import com.util.AbstractSessionBean;
import com.util.AdCompanyDetails;
import com.util.ApRepCheckEditListDetails;
import com.util.Debug;
import com.util.EJBHomeFactory;

/**
 * @ejb:bean name="ApRepCheckEditListControllerEJB"
 *           display-name="Used for printing check list transactions"
 *           type="Stateless"
 *           view-type="remote"
 *           jndi-name="ejb/ApRepCheckEditListControllerEJB"
 *
 * @ejb:interface remote-class="com.ejb.txn.ApRepCheckEditListController"
 *                extends="javax.ejb.EJBObject"
 *
 * @ejb:home remote-class="com.ejb.txn.ApRepCheckEditListControllerHome"
 *           extends="javax.ejb.EJBHome"
 *
 * @ejb:transaction type="Required"
 * 
*/

public class ApRepCheckEditListControllerBean extends AbstractSessionBean {
    
    
    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList executeApRepCheckEditList(ArrayList chkCodeList, Integer AD_CMPNY) 
        throws GlobalNoRecordFoundException {
                    
        Debug.print("ApRepCheckPrintControllerBean executeApRepCheckEditList");
        
        LocalApCheckHome apCheckHome = null;        
        LocalGenSegmentHome genSegmentHome = null;
        
        ArrayList list = new ArrayList();
                
        // Initialize EJB Home
        
        try {
            
        	apCheckHome = (LocalApCheckHome)EJBHomeFactory.
                lookUpLocalHome(LocalApCheckHome.JNDI_NAME, LocalApCheckHome.class);
            genSegmentHome = (LocalGenSegmentHome)EJBHomeFactory.
                lookUpLocalHome(LocalGenSegmentHome.JNDI_NAME, LocalGenSegmentHome.class);
            
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }        
        
        try {
        	
        	Iterator i = chkCodeList.iterator();
        	
        	while (i.hasNext()) {
        	
        	    Integer CHK_CODE = (Integer) i.next();
        	
	        	LocalApCheck apCheck = null;

	        	try {
	        		
	        		apCheck = apCheckHome.findByPrimaryKey(CHK_CODE);

	        	} catch (FinderException ex) {
	        		
	        		continue;
	        		
	        	}
	        		        	
	        	// get check distribution records
	        	
	        	Collection apDistributionRecords = apCheck.getApDistributionRecords();        	            
	        	
	        	Iterator drIter = apDistributionRecords.iterator();
	        	
	        	while (drIter.hasNext()) {
	        		
	        		LocalApDistributionRecord apDistributionRecord = (LocalApDistributionRecord)drIter.next();
	        		
	        		ApRepCheckEditListDetails details = new ApRepCheckEditListDetails();
	        		
	        		details.setCelChkBatchName(apCheck.getApCheckBatch() != null ? apCheck.getApCheckBatch().getCbName() : "N/A");
	        		details.setCelChkBatchDescription(apCheck.getApCheckBatch() != null ? apCheck.getApCheckBatch().getCbDescription() : "N/A");
	        		details.setCelChkTransactionTotal(apCheck.getApCheckBatch() != null ? apCheck.getApCheckBatch().getApChecks().size() : 0);
	        		details.setCelChkDateCreated(apCheck.getChkDateCreated());
	        		details.setCelChkCreatedBy(apCheck.getChkCreatedBy());
	        		details.setCelChkNumber(apCheck.getChkNumber());
	        		details.setCelChkDescription(apCheck.getChkDescription());
	        		details.setCelChkDocumentNumber(apCheck.getChkDocumentNumber());
	        		details.setCelChkDate(apCheck.getChkDate());
	        		details.setCelSplSupplierCode(apCheck.getApSupplier().getSplSupplierCode());
	        		details.setCelSplSupplierName(apCheck.getChkSupplierName() == null ? apCheck.getApSupplier().getSplName() : apCheck.getChkSupplierName());
	        		details.setCelChkAdBaName(apCheck.getAdBankAccount().getBaName());
	        		details.setCelChkAmount(apCheck.getChkAmount());
	        		details.setCelDrAccountNumber(apDistributionRecord.getGlChartOfAccount().getCoaAccountNumber());
	        		details.setCelDrAccountDescription(apDistributionRecord.getGlChartOfAccount().getCoaAccountDescription());
	        		details.setCelDrClass(apDistributionRecord.getDrClass());
	        		details.setCelDrDebit(apDistributionRecord.getDrDebit());
	        		details.setCelDrAmount(apDistributionRecord.getDrAmount());	

	        		list.add(details);

	        	}
	        	
	        }
	        
	        if (list.isEmpty()) {
	        	
	        	throw new GlobalNoRecordFoundException();
	        	
	        }        	       	
        	
        	return list;        	
        	        	
        } catch (GlobalNoRecordFoundException ex) {
        	
        	throw ex;
        	        	
        } catch (Exception ex) {
        	
        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());
        	
        }
            
    }
    
    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList executeApRepCheckEditListSub(ArrayList chkCodeList, Integer AD_CMPNY) 
        throws GlobalNoRecordFoundException {
                    
        Debug.print("ApRepCheckPrintControllerBean executeApRepCheckEditListSub");
        
        LocalApCheckHome apCheckHome = null;        
        
        ArrayList list = new ArrayList();
                
        // Initialize EJB Home
        
        try {
            
        	apCheckHome = (LocalApCheckHome)EJBHomeFactory.
                lookUpLocalHome(LocalApCheckHome.JNDI_NAME, LocalApCheckHome.class);
        	
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }        
        
        try {
        	
        	Iterator i = chkCodeList.iterator();
        	
        	while (i.hasNext()) {
        	
        	    Integer CHK_CODE = (Integer) i.next();
        	
	        	LocalApCheck apCheck = null;

	        	try {
	        		
	        		apCheck = apCheckHome.findByPrimaryKey(CHK_CODE);

	        	} catch (FinderException ex) {
	        		
	        		continue;
	        		
	        	}
	        	
	        	Collection apAppliedVouchers  = apCheck.getApAppliedVouchers();
	        	
	            Iterator appIter = apAppliedVouchers.iterator();
	            
	            while(appIter.hasNext()) {
	            	
	                LocalApAppliedVoucher apAppliedVoucher = (LocalApAppliedVoucher)appIter.next();	
	        		        	
		    		ApRepCheckEditListDetails details = new ApRepCheckEditListDetails();
		    		
		    		details.setCelChkDocumentNumber(apCheck.getChkDocumentNumber());
		    		details.setCelVouNumber(apAppliedVoucher.getApVoucherPaymentSchedule().getApVoucher().getVouDocumentNumber());
		    		details.setCelAppliedAmount(apAppliedVoucher.getAvApplyAmount() + apAppliedVoucher.getAvTaxWithheld() + apAppliedVoucher.getAvDiscountAmount());	
		
		    		list.add(details);
		    		
	            }
        		
	        }
	        
	        if (list.isEmpty()) {
	        	
	        	throw new GlobalNoRecordFoundException();
	        	
	        }        	       	
        	
        	return list;        	
        	        	
        } catch (GlobalNoRecordFoundException ex) {
        	
        	throw ex;
        	        	
        } catch (Exception ex) {
        	
        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());
        	
        }
            
    }
    
	/**
	* @ejb:interface-method view-type="remote"
	* @jboss:method-attributes read-only="true"
	**/
	public com.util.AdCompanyDetails getAdCompany(Integer AD_CMPNY) {
	
	  Debug.print("ApRepVoucherPrintControllerBean getAdCompany");      
	  
	  LocalAdCompanyHome adCompanyHome = null;
	        
	  // Initialize EJB Home
	    
	  try {
	        
	       adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
	           lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
	        
	  } catch (NamingException ex) {
	        
	      throw new EJBException(ex.getMessage());
	        
	  }
	
	  try {
	      
	     LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
	     
	     AdCompanyDetails details = new AdCompanyDetails();
	     details.setCmpName(adCompany.getCmpName());
	     details.setCmpAddress(adCompany.getCmpAddress());
	     details.setCmpCity(adCompany.getCmpCity());
	     details.setCmpCountry(adCompany.getCmpCountry());
	     details.setCmpPhone(adCompany.getCmpPhone());
	     
	     return details;  	
	  	       	
	  } catch (Exception ex) {
	  	
	  	 Debug.printStackTrace(ex);
	  	 throw new EJBException(ex.getMessage());
	  	  
	  }
	  
	}    

    // SessionBean methods

    /**
     * @ejb:create-method view-type="remote"
     **/
    public void ejbCreate() throws CreateException {

       Debug.print("ApRepVoucherPrintControllerBean ejbCreate");
      
    }
}
