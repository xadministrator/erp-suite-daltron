
/*
 * ApRepCheckPrintControllerBean.java
 *
 * Created on Feb 25, 2004, 10:24 AM
 * @author  Dennis Hilario
 */

package com.ejb.txn;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;
import javax.naming.NamingException;

import com.ejb.ad.LocalAdApproval;
import com.ejb.ad.LocalAdApprovalDocument;
import com.ejb.ad.LocalAdApprovalDocumentHome;
import com.ejb.ad.LocalAdApprovalHome;
import com.ejb.ad.LocalAdBranch;
import com.ejb.ad.LocalAdBranchHome;
import com.ejb.ad.LocalAdCompany;
import com.ejb.ad.LocalAdCompanyHome;
import com.ejb.ad.LocalAdPreference;
import com.ejb.ad.LocalAdPreferenceHome;
import com.ejb.ap.LocalApCheck;
import com.ejb.ap.LocalApCheckHome;
import com.ejb.ap.LocalApDistributionRecord;
import com.ejb.ap.LocalApDistributionRecordHome;
import com.ejb.exception.GlobalNoRecordFoundException;
import com.util.AbstractSessionBean;
import com.util.AdCompanyDetails;
import com.util.ApModCheckDetails;
import com.util.ApRepApRegisterDetails;
import com.util.Debug;
import com.util.EJBCommon;
import com.util.EJBHomeFactory;

/**
 * @ejb:bean name="ApRepCheckPrintControllerEJB"
 *           display-name="Used for printing check transactions"
 *           type="Stateless"
 *           view-type="remote"
 *           jndi-name="ejb/ApRepCheckPrintControllerEJB"
 *
 * @ejb:interface remote-class="com.ejb.txn.ApRepCheckPrintController"
 *                extends="javax.ejb.EJBObject"
 *
 * @ejb:home remote-class="com.ejb.txn.ApRepCheckPrintControllerHome"
 *           extends="javax.ejb.EJBHome"
 *
 * @ejb:transaction type="Required"
 * 
*/

public class ApRepCheckPrintControllerBean extends AbstractSessionBean {
    
    /**
     * @ejb:interface-method view-type="remote"
     **/
    public ArrayList executeApRepCheckPrint(ArrayList chkCodeList, Integer AD_CMPNY) 
        throws GlobalNoRecordFoundException {
                    
        Debug.print("ApRepCheckPrintControllerBean executeApRepCheckPrint");
        
        LocalApCheckHome apCheckHome = null;
        LocalApCheck apCheck = null;
        LocalAdApprovalHome adApprovalHome = null;
        LocalAdApprovalDocumentHome adApprovalDocumentHome = null;
        LocalApDistributionRecordHome apDistributionRecordHome = null;
        LocalAdBranchHome adBranchHome = null;
        
        ArrayList list = new ArrayList();
        
        // Initialize EJB Home
        
        try {
            
            apCheckHome = (LocalApCheckHome)EJBHomeFactory.
                lookUpLocalHome(LocalApCheckHome.JNDI_NAME, LocalApCheckHome.class);
            adApprovalHome = (LocalAdApprovalHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdApprovalHome.JNDI_NAME, LocalAdApprovalHome.class);
            adApprovalDocumentHome = (LocalAdApprovalDocumentHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdApprovalDocumentHome.JNDI_NAME, LocalAdApprovalDocumentHome.class);
            apDistributionRecordHome = (LocalApDistributionRecordHome)EJBHomeFactory.
				lookUpLocalHome(LocalApDistributionRecordHome.JNDI_NAME, LocalApDistributionRecordHome.class);
            adBranchHome = (LocalAdBranchHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdBranchHome .JNDI_NAME, LocalAdBranchHome .class);
            
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {	
        
              Iterator i = chkCodeList.iterator();
        	
        	  while (i.hasNext()) {
        	
        	    Integer CHK_CODE = (Integer) i.next();
       	      
	       	      try {
	       	      	
		       	  	apCheck = apCheckHome.findByPrimaryKey(CHK_CODE);	       	                
		       	  	
		       	  } catch (FinderException ex) {
		       	  	
		       	  	continue;
		       	  	
		       	  }
		       	  		       	  	
		       	  LocalAdApproval adApproval = adApprovalHome.findByAprAdCompany(AD_CMPNY);
		       	  LocalAdApprovalDocument adApprovalDocument = adApprovalDocumentHome.findByAdcType("AP CHECK", AD_CMPNY);
		       	  
		       	  if (adApprovalDocument.getAdcPrintOption().equals("PRINT APPROVED ONLY")) {
	        	
	        		  if (apCheck.getChkApprovalStatus() == null || 
	        			  apCheck.getChkApprovalStatus().equals("PENDING")) {
	        		    
	        		      continue;
	        		    
	        		  } 
	        	
	        	
	        	  } else if (adApprovalDocument.getAdcPrintOption().equals("PRINT UNAPPROVED ONLY")) {
	        	
	        		  if (apCheck.getChkApprovalStatus() != null && 
	        			  (apCheck.getChkApprovalStatus().equals("N/A") || 
	        			   apCheck.getChkApprovalStatus().equals("APPROVED"))) {
	        		
	        			  continue;
	        		
	        		  }
	        	
	        	  }
	        	
	        	  if (adApprovalDocument.getAdcAllowDuplicate() == EJBCommon.FALSE && 
	        		  apCheck.getChkPrinted() == EJBCommon.TRUE){
	        		
	        		  continue;	
	        		
	        	  }
	        		        		        		        	      
	        	  // set printed
	        	
	        	  apCheck.setChkPrinted(EJBCommon.TRUE);
	        	  
	        	  if (!this.getAdPrfUseBankForm(AD_CMPNY)) {
	        	  	
	        	  	ApModCheckDetails mdetails = new ApModCheckDetails();
	        	  	
	        	  	mdetails.setChkDescription(apCheck.getChkDescription());
	        	  	mdetails.setChkCheckDate(apCheck.getChkCheckDate()); // change date to check date
	        	  	mdetails.setChkNumber(apCheck.getChkNumber());
	        	  	mdetails.setChkAmount(apCheck.getChkAmount());	   	
	        	  	// supplier name
	        	  	mdetails.setChkSplName(apCheck.getChkSupplierName() == null ? apCheck.getApSupplier().getSplName() :  apCheck.getChkSupplierName());
	        	  	mdetails.setChkSplAddress(apCheck.getApSupplier().getSplAddress());
	        	  	mdetails.setChkFcName(apCheck.getGlFunctionalCurrency().getFcName());
	        	  	mdetails.setChkBaName(apCheck.getAdBankAccount().getBaName());   		       	      
	        	  	mdetails.setChkBaAccountNumber(apCheck.getAdBankAccount().getBaAccountNumber());
	        	  	
	        	  	mdetails.setChkBaAccountNumberShow(apCheck.getAdBankAccount().getBaAccountNumberShow());
	        	  	mdetails.setChkBaAccountNumberTop(apCheck.getAdBankAccount().getBaAccountNumberTop());
	        	  	mdetails.setChkBaAccountNumberLeft(apCheck.getAdBankAccount().getBaAccountNumberLeft());
	        	  	
	        	  	mdetails.setChkBaAccountNameShow(apCheck.getAdBankAccount().getBaAccountNameShow());
	        	  	mdetails.setChkBaAccountNameTop(apCheck.getAdBankAccount().getBaAccountNameTop());
	        	  	mdetails.setChkBaAccountNameLeft(apCheck.getAdBankAccount().getBaAccountNameLeft());
	        	  	
	        	  	mdetails.setChkBaNumberShow(apCheck.getAdBankAccount().getBaNumberShow());
	        	  	mdetails.setChkBaNumberTop(apCheck.getAdBankAccount().getBaNumberTop());
	        	  	mdetails.setChkBaNumberLeft(apCheck.getAdBankAccount().getBaNumberLeft());
	        	  	
	        	  	mdetails.setChkBaDateShow(apCheck.getAdBankAccount().getBaDateShow());
	        	  	mdetails.setChkBaDateTop(apCheck.getAdBankAccount().getBaDateTop());
	        	  	mdetails.setChkBaDateLeft(apCheck.getAdBankAccount().getBaDateLeft());
	        	  	
	        	  	mdetails.setChkBaPayeeShow(apCheck.getAdBankAccount().getBaPayeeShow());
	        	  	mdetails.setChkBaPayeeTop(apCheck.getAdBankAccount().getBaPayeeTop());
	        	  	mdetails.setChkBaPayeeLeft(apCheck.getAdBankAccount().getBaPayeeLeft());
	        	  	
	        	  	mdetails.setChkBaAmountShow(apCheck.getAdBankAccount().getBaAmountShow());
	        	  	mdetails.setChkBaAmountTop(apCheck.getAdBankAccount().getBaAmountTop());
	        	  	mdetails.setChkBaAmountLeft(apCheck.getAdBankAccount().getBaAmountLeft());
	        	  	
	        	  	mdetails.setChkBaWordAmountShow(apCheck.getAdBankAccount().getBaWordAmountShow());
	        	  	mdetails.setChkBaWordAmountTop(apCheck.getAdBankAccount().getBaWordAmountTop());
	        	  	mdetails.setChkBaWordAmountLeft(apCheck.getAdBankAccount().getBaWordAmountLeft());
	        	  	
	        	  	mdetails.setChkBaCurrencyShow(apCheck.getAdBankAccount().getBaCurrencyShow());
	        	  	mdetails.setChkBaCurrencyTop(apCheck.getAdBankAccount().getBaCurrencyTop());
	        	  	mdetails.setChkBaCurrencyLeft(apCheck.getAdBankAccount().getBaCurrencyLeft());
	        	  	
	        	  	mdetails.setChkBaAddressShow(apCheck.getAdBankAccount().getBaAddressShow());
	        	  	mdetails.setChkBaAddressTop(apCheck.getAdBankAccount().getBaAddressTop());
	        	  	mdetails.setChkBaAddressLeft(apCheck.getAdBankAccount().getBaAddressLeft());
	        	  	
	        	  	mdetails.setChkBaMemoShow(apCheck.getAdBankAccount().getBaMemoShow());
	        	  	mdetails.setChkBaMemoTop(apCheck.getAdBankAccount().getBaMemoTop());
	        	  	mdetails.setChkBaMemoLeft(apCheck.getAdBankAccount().getBaMemoLeft());
	        	  	
	        	  	mdetails.setChkBaDocNumberShow(apCheck.getAdBankAccount().getBadocNumberShow());
	        	  	mdetails.setChkBaDocNumberTop(apCheck.getAdBankAccount().getBadocNumberTop());
	        	  	mdetails.setChkBaDocNumberLeft(apCheck.getAdBankAccount().getBadocNumberLeft());
	        	  	
	        	  	mdetails.setChkBaFontSize(apCheck.getAdBankAccount().getBaFontSize());
	        	  	mdetails.setChkBaFontStyle(apCheck.getAdBankAccount().getBaFontStyle());
	        	  	
	        	  	mdetails.setChkCrossCheck(apCheck.getChkCrossCheck());
	        	  	mdetails.setChkDocumentNumber(apCheck.getChkDocumentNumber());
	        	  	System.out.println("docNumber" + apCheck.getChkDocumentNumber());
	        	  	mdetails.setChkSplRemarks(apCheck.getApSupplier().getSplRemarks());

//        			Include Branch
    				//LocalAdBranch adBranch = adBranchHome.findByPrimaryKey(apCheck.getChkAdBranch());
    				//mdetails.setBrnhCde(adBranch.getBrBranchCode());
    				//mdetails.setBrnhNm(adBranch.getBrName());
	        	  	
    				//System.out.println("adBranch.getBrName() : "+adBranch.getBrName());
    	   			System.out.println("1 Controller Check Date : " + mdetails.getChkCheckDate());
    	   			System.out.println("1 Controller Date : " + mdetails.getChkDate());

    	   			
	        	  	list.add(mdetails);
	        	  	
	        	 } else { 
			       	  
	        	 	// get distribution records
	        	 	
	        	 	Collection apDistributionRecords = apDistributionRecordHome.findByChkCode(apCheck.getChkCode(), AD_CMPNY);       	            
	        	 	
	        	 	Iterator drIter = apDistributionRecords.iterator();
	        	 	
	        	 	while(drIter.hasNext()) {
	        	 		
	        	 		LocalApDistributionRecord apDistributionRecord = (LocalApDistributionRecord)drIter.next();
	        	 		
	        	 		ApModCheckDetails mdetails = new ApModCheckDetails();
	        	 		
	        	 		mdetails.setChkDescription(apCheck.getChkDescription());
	        	 		mdetails.setChkCheckDate(apCheck.getChkCheckDate()); // change date to check date
	        	 		mdetails.setChkNumber(apCheck.getChkNumber());
	        	 		mdetails.setChkAmount(apCheck.getChkAmount());	   	      
	        	 		// supplier name
		        	  	mdetails.setChkSplName(apCheck.getChkSupplierName() == null ? apCheck.getApSupplier().getSplName() :  apCheck.getChkSupplierName());
		        	  	mdetails.setChkSplAddress(apCheck.getApSupplier().getSplAddress());
	        	 		mdetails.setChkFcName(apCheck.getGlFunctionalCurrency().getFcName());
	        	 		mdetails.setChkBaName(apCheck.getAdBankAccount().getBaName());   		       	      
	        	 		mdetails.setChkBaAccountNumber(apCheck.getAdBankAccount().getBaAccountNumber());
	        	 		
	        	 		mdetails.setChkDocumentNumber(apCheck.getChkDocumentNumber());
	        	 		System.out.println("docNumber2" + apCheck.getChkDocumentNumber());
	        	 		mdetails.setChkDate(apCheck.getChkDate());
	        	 		//mdetails.setChkDescription(apCheck.getChkDescription());
	        	 		mdetails.setChkCreatedBy(apCheck.getChkCreatedBy());
	        	 		mdetails.setChkApprovedRejectedBy(apCheck.getChkApprovedRejectedBy());
	        	 		mdetails.setChkApprovalStatus(apCheck.getChkApprovalStatus());
	        	 		mdetails.setChkPosted(apCheck.getChkPosted());
	        	 		mdetails.setChkDrDebit(apDistributionRecord.getDrDebit());
	        	 		mdetails.setChkDrAmount(apDistributionRecord.getDrAmount());		        	
	        	 		mdetails.setChkDrCoaAccountNumber(apDistributionRecord.getGlChartOfAccount().getCoaAccountNumber());
	        	 		mdetails.setChkDrCoaAccountDescription(apDistributionRecord.getGlChartOfAccount().getCoaAccountDescription());
	        	 		
	        	 		mdetails.setChkBaAccountNumberShow(apCheck.getAdBankAccount().getBaAccountNumberShow());
	        	 		mdetails.setChkBaAccountNumberTop(apCheck.getAdBankAccount().getBaAccountNumberTop());
	        	 		mdetails.setChkBaAccountNumberLeft(apCheck.getAdBankAccount().getBaAccountNumberLeft());
	        	 		
	        	 		mdetails.setChkBaAccountNameShow(apCheck.getAdBankAccount().getBaAccountNameShow());
	        	 		mdetails.setChkBaAccountNameTop(apCheck.getAdBankAccount().getBaAccountNameTop());
	        	 		mdetails.setChkBaAccountNameLeft(apCheck.getAdBankAccount().getBaAccountNameLeft());
	        	 		
	        	 		mdetails.setChkBaNumberShow(apCheck.getAdBankAccount().getBaNumberShow());
	        	 		mdetails.setChkBaNumberTop(apCheck.getAdBankAccount().getBaNumberTop());
	        	 		mdetails.setChkBaNumberLeft(apCheck.getAdBankAccount().getBaNumberLeft());
	        	 		
	        	 		mdetails.setChkBaDateShow(apCheck.getAdBankAccount().getBaDateShow());
	        	 		mdetails.setChkBaDateTop(apCheck.getAdBankAccount().getBaDateTop());
	        	 		mdetails.setChkBaDateLeft(apCheck.getAdBankAccount().getBaDateLeft());
	        	 		
	        	 		mdetails.setChkBaPayeeShow(apCheck.getAdBankAccount().getBaPayeeShow());
	        	 		mdetails.setChkBaPayeeTop(apCheck.getAdBankAccount().getBaPayeeTop());
	        	 		mdetails.setChkBaPayeeLeft(apCheck.getAdBankAccount().getBaPayeeLeft());
	        	 		
	        	 		mdetails.setChkBaAmountShow(apCheck.getAdBankAccount().getBaAmountShow());
	        	 		mdetails.setChkBaAmountTop(apCheck.getAdBankAccount().getBaAmountTop());
	        	 		mdetails.setChkBaAmountLeft(apCheck.getAdBankAccount().getBaAmountLeft());
	        	 		
	        	 		mdetails.setChkBaWordAmountShow(apCheck.getAdBankAccount().getBaWordAmountShow());
	        	 		mdetails.setChkBaWordAmountTop(apCheck.getAdBankAccount().getBaWordAmountTop());
	        	 		mdetails.setChkBaWordAmountLeft(apCheck.getAdBankAccount().getBaWordAmountLeft());
	        	 		
	        	 		mdetails.setChkBaCurrencyShow(apCheck.getAdBankAccount().getBaCurrencyShow());
	        	 		mdetails.setChkBaCurrencyTop(apCheck.getAdBankAccount().getBaCurrencyTop());
	        	 		mdetails.setChkBaCurrencyLeft(apCheck.getAdBankAccount().getBaCurrencyLeft());
	        	 		
	        	 		mdetails.setChkBaAddressShow(apCheck.getAdBankAccount().getBaAddressShow());
	        	 		mdetails.setChkBaAddressTop(apCheck.getAdBankAccount().getBaAddressTop());
	        	 		mdetails.setChkBaAddressLeft(apCheck.getAdBankAccount().getBaAddressLeft());
	        	 		
	        	 		mdetails.setChkBaMemoShow(apCheck.getAdBankAccount().getBaMemoShow());
	        	 		mdetails.setChkBaMemoTop(apCheck.getAdBankAccount().getBaMemoTop());
	        	 		mdetails.setChkBaMemoLeft(apCheck.getAdBankAccount().getBaMemoLeft());
	        	 		
	        	 		mdetails.setChkBaDocNumberShow(apCheck.getAdBankAccount().getBadocNumberShow());
	        	 		mdetails.setChkBaDocNumberTop(apCheck.getAdBankAccount().getBadocNumberTop());
	        	 		mdetails.setChkBaDocNumberLeft(apCheck.getAdBankAccount().getBadocNumberLeft());
	        	 		
	        	 		mdetails.setChkBaFontSize(apCheck.getAdBankAccount().getBaFontSize());
	        	 		mdetails.setChkBaFontStyle(apCheck.getAdBankAccount().getBaFontStyle());
	        	 		
	        	 		mdetails.setChkCrossCheck(apCheck.getChkCrossCheck());
	        	 		mdetails.setChkSplRemarks(apCheck.getApSupplier().getSplRemarks());

//	        			Include Branch
	    				LocalAdBranch adBranch = adBranchHome.findByPrimaryKey(apCheck.getChkAdBranch());
	    				mdetails.setBrnhCde(adBranch.getBrBranchCode());
	    				mdetails.setBrnhNm(adBranch.getBrName());
	    				
	    				System.out.println("adBranch.getBrName() 2: "+adBranch.getBrName());
	    	   			System.out.println("2 Controller Check Date : " + mdetails.getChkCheckDate());
	    	   			System.out.println("2 Controller Date : " + mdetails.getChkDate());
	    	   			
	        	 		list.add(mdetails);
	        	 		
	        	 	}
	        	 	
	        	 }
	        	  
        	  }
		   	  
		   	  if (list.isEmpty()) {
	        	
	        	throw new GlobalNoRecordFoundException();
	        	
	          }


	       	  return list;
	       	      
        } catch (GlobalNoRecordFoundException ex) {
       	
       	  throw ex;
       	
        } catch (Exception ex) {
       	
       	  Debug.printStackTrace(ex);
       	  throw new EJBException(ex.getMessage());
       	
        }
       
    }
    
    public static Comparator SupplierCodeComparator = new Comparator() {
       	
       	public int compare(Object AR, Object anotherAR) {
       		
       		String ARG_SPL_SPPLR_CODE1 = ((ApRepApRegisterDetails) AR).getArgSplSupplierCode();
       		String ARG_SPL_SPPLR_TYP1 = ((ApRepApRegisterDetails) AR).getArgSplSupplierType();
       		Date ARG_DT1 = ((ApRepApRegisterDetails) AR).getArgDate();
       		String ARG_DCMNT_NMBR1 = ((ApRepApRegisterDetails) AR).getArgDocumentNumber();
       		
       		String ARG_SPL_SPPLR_CODE2 = ((ApRepApRegisterDetails) anotherAR).getArgSplSupplierCode();
       		String ARG_SPL_SPPLR_TYP2 = ((ApRepApRegisterDetails) anotherAR).getArgSplSupplierType();
       		Date ARG_DT2 = ((ApRepApRegisterDetails) anotherAR).getArgDate();
       	    String ARG_DCMNT_NMBR2 = ((ApRepApRegisterDetails) anotherAR).getArgDocumentNumber();
       	    
       		String ORDER_BY = ((ApRepApRegisterDetails) AR).getOrderBy();
       		
       		if (!(ARG_SPL_SPPLR_CODE1.equals(ARG_SPL_SPPLR_CODE2))) {
       			
       			return ARG_SPL_SPPLR_CODE1.compareTo(ARG_SPL_SPPLR_CODE2);
       			
       		} else {
       			
       			if(ORDER_BY.equals("DATE") && !(ARG_DT1.equals(ARG_DT2))){
       				
       				return ARG_DT1.compareTo(ARG_DT2);
       				
       			} else if(ORDER_BY.equals("SUPPLIER TYPE") && !(ARG_SPL_SPPLR_TYP1.equals(ARG_SPL_SPPLR_TYP2))){
       				
       				return ARG_SPL_SPPLR_TYP1.compareTo(ARG_SPL_SPPLR_TYP2);
       				
       			} else {
       				
       				return ARG_DCMNT_NMBR1.compareTo(ARG_DCMNT_NMBR2);
       				
       			}
       			
       		}
       		
       	}
       	
       };
       
       
    /**
	* @ejb:interface-method view-type="remote"
	* @jboss:method-attributes read-only="true"
	**/
	public com.util.AdCompanyDetails getAdCompany(Integer AD_CMPNY) {
	
	  Debug.print("ApRepCheckPrintControllerBean getAdCompany");      
	  
	  LocalAdCompanyHome adCompanyHome = null;
	        
	  // Initialize EJB Home
	    
	  try {
	        
	       adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
	           lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
	        
	  } catch (NamingException ex) {
	        
	      throw new EJBException(ex.getMessage());
	        
	  }
	
	  try {
	      
	     LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
	     
	     AdCompanyDetails details = new AdCompanyDetails();
	     
	     details.setCmpName(adCompany.getCmpName());
	     
	     return details;  	
	  	       	
	  } catch (Exception ex) {
	  	
	  	 Debug.printStackTrace(ex);
	  	 throw new EJBException(ex.getMessage());
	  	  
	  }
	  
	}
        
	/**
	* @ejb:interface-method view-type="remote"
	* @jboss:method-attributes read-only="true"
	**/
	public boolean getAdPrfUseBankForm(Integer AD_CMPNY) {
	
	  Debug.print("ApRepCheckPrintControllerBean getAdPrfUseBankForm");      
	  
	  LocalAdPreferenceHome adPreferenceHome = null;
	  
	  // Initialize EJB Home
	    
	  try {
	        
	  	adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
        	lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);
	        
	  } catch (NamingException ex) {
	        
	      throw new EJBException(ex.getMessage());
	        
	  }
	
	  try {
	      
	  	LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);
	  	
	  	if(adPreference.getPrfCmUseBankForm() == EJBCommon.TRUE)
	  		return true;
	  	
	  	return 	false;
	  	       	
	  } catch (Exception ex) {
	  	
	  	 Debug.printStackTrace(ex);
	  	 throw new EJBException(ex.getMessage());
	  	  
	  }
	  
	}
	
	// private methods
         
    // SessionBean methods

    /**
     * @ejb:create-method view-type="remote"
     **/
    public void ejbCreate() throws CreateException {

       Debug.print("ApRepCheckPrintControllerBean ejbCreate");
      
    }
}
