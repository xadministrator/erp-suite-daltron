
/*
 * ApRepCheckRegisterControllerBean.java
 *
 * Created on March 01, 2004, 1:50 PM
 *
 * @author  Neil Andrew M. Ajero
 */

package com.ejb.txn;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;
import javax.naming.NamingException;

import com.ejb.ad.LocalAdBankAccount;
import com.ejb.ad.LocalAdBankAccountHome;
import com.ejb.ad.LocalAdBranch;
import com.ejb.ad.LocalAdBranchHome;
import com.ejb.ad.LocalAdBranchResponsibility;
import com.ejb.ad.LocalAdBranchResponsibilityHome;
import com.ejb.ad.LocalAdCompany;
import com.ejb.ad.LocalAdCompanyHome;
import com.ejb.ap.LocalApCheck;
import com.ejb.ap.LocalApCheckHome;
import com.ejb.ap.LocalApDistributionRecord;
import com.ejb.ap.LocalApDistributionRecordHome;
import com.ejb.ap.LocalApSupplierClass;
import com.ejb.ap.LocalApSupplierClassHome;
import com.ejb.ap.LocalApSupplierType;
import com.ejb.ap.LocalApSupplierTypeHome;
import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.gl.LocalGlChartOfAccount;
import com.ejb.gl.LocalGlChartOfAccountHome;
import com.ejb.gl.LocalGlFunctionalCurrencyRate;
import com.ejb.gl.LocalGlFunctionalCurrencyRateHome;
import com.util.AbstractSessionBean;
import com.util.AdBranchDetails;
import com.util.AdCompanyDetails;
import com.util.ApRepCheckRegisterDetails;
import com.util.Debug;
import com.util.EJBCommon;
import com.util.EJBHomeFactory;
import com.util.GlChartOfAccountDetails;

/**
 * @ejb:bean name="ApRepCheckRegisterControllerEJB"
 *           display-name="Used for finding check registers"
 *           type="Stateless"
 *           view-type="remote"
 *           jndi-name="ejb/ApRepCheckRegisterControllerEJB"
 *
 * @ejb:interface remote-class="com.ejb.txn.ApRepCheckRegisterController"
 *                extends="javax.ejb.EJBObject"
 *
 * @ejb:home remote-class="com.ejb.txn.ApRepCheckRegisterControllerHome"
 *           extends="javax.ejb.EJBHome"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="apuser"
 *                        role-link="apuserlink"
 *
 * @ejb:permission role-name="apuser"
 * 
*/

public class ApRepCheckRegisterControllerBean extends AbstractSessionBean {

    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList getApScAll(Integer AD_CMPNY) {
                    
        Debug.print("ApRepCheckRegisterControllerBean getApScAll");
        
        LocalApSupplierClassHome apSupplierClassHome = null;

        ArrayList list = new ArrayList();
        
        // Initialize EJB Home
        
        try {
            
            apSupplierClassHome = (LocalApSupplierClassHome)EJBHomeFactory.
                lookUpLocalHome(LocalApSupplierClassHome.JNDI_NAME, LocalApSupplierClassHome.class);
            
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {
            
            Collection apSupplierClasses = apSupplierClassHome.findEnabledScAll(AD_CMPNY);

	        Iterator i = apSupplierClasses.iterator();
	               
	        while (i.hasNext()) {
	        	
	        	LocalApSupplierClass apSupplierClass = (LocalApSupplierClass)i.next();
	        	
	        	list.add(apSupplierClass.getScName());
	        	
	        }
	        
	        return list;
            
        } catch (Exception ex) {
        	
        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());
        	
        }
            
    }

    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList getApStAll(Integer AD_CMPNY) {
                    
        Debug.print("ApRepCheckRegisterControllerBean getApStAll");
        
        LocalApSupplierTypeHome apSupplierTypeHome = null;

        ArrayList list = new ArrayList();
        
        // Initialize EJB Home
        
        try {
            
            apSupplierTypeHome = (LocalApSupplierTypeHome)EJBHomeFactory.
                lookUpLocalHome(LocalApSupplierTypeHome.JNDI_NAME, LocalApSupplierTypeHome.class);
            
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {
            
            Collection apSupplierTypes = apSupplierTypeHome.findEnabledStAll(AD_CMPNY);

	        Iterator i = apSupplierTypes.iterator();
	               
	        while (i.hasNext()) {
	        	
	        	LocalApSupplierType apSupplierType = (LocalApSupplierType)i.next();
	        	
	        	list.add(apSupplierType.getStName());
	        	
	        }
	        
	        return list;
            
        } catch (Exception ex) {
        	
        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());
        	
        }
            
    }

    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList getAdBaAll(Integer AD_BRNCH, Integer AD_CMPNY) {
                    
        Debug.print("ApRepCheckRegisterControllerBean getAdBaAll");
        
        LocalAdBankAccountHome adBankAccountHome = null;

        ArrayList list = new ArrayList();
        
        // Initialize EJB Home
        
        try {
            
            adBankAccountHome = (LocalAdBankAccountHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdBankAccountHome.JNDI_NAME, LocalAdBankAccountHome.class);
            
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {
            
            Collection adBankAccounts = adBankAccountHome.findEnabledBaAll(AD_BRNCH, AD_CMPNY);

	        Iterator i = adBankAccounts.iterator();
	               
	        while (i.hasNext()) {
	        	
	        	LocalAdBankAccount adBankAccount = (LocalAdBankAccount)i.next();
	        	
	        	list.add(adBankAccount.getBaName());
	        	
	        }
	        
	        return list;
            
        } catch (Exception ex) {
        	
        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());
        	
        }
            
    }
    
    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList executeApRepCheckRegister(HashMap criteria, String ORDER_BY, String GROUP_BY, boolean SHOW_ENTRIES, boolean SUMMARIZE, 
    		boolean SHOW_PDC, boolean DEBIT_FIRST, String INCLD_VD_CHCK, String currency, String accountName, ArrayList bankAccountList, ArrayList adBrnchList, Integer AD_CMPNY) 
        throws GlobalNoRecordFoundException {
                    
        Debug.print("ApRepCheckRegisterControllerBean executeApRepCheckRegister");
        
        LocalApCheckHome apCheckHome = null;
        LocalAdCompanyHome adCompanyHome = null;
        LocalApDistributionRecordHome apDistributionRecordHome = null;
                
        ArrayList list = new ArrayList();
        int a = 0;
	
        //initialized EJB Home
        
        try {
            
            apCheckHome = (LocalApCheckHome)EJBHomeFactory.
                lookUpLocalHome(LocalApCheckHome.JNDI_NAME, LocalApCheckHome.class);
            adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
            apDistributionRecordHome = (LocalApDistributionRecordHome)EJBHomeFactory.
            lookUpLocalHome(LocalApDistributionRecordHome.JNDI_NAME, LocalApDistributionRecordHome.class);
                        
        } catch (NamingException ex) 	{
            
            throw new EJBException(ex.getMessage());
            
        }
        
		try { 
		
		  LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
		  short precisionUnit = adCompany.getGlFunctionalCurrency().getFcPrecision();
		  
		  StringBuffer jbossQl = new StringBuffer();
		  jbossQl.append("SELECT OBJECT(chk) FROM ApCheck chk ");
		  
	      boolean firstArgument = true;
	      short ctr = 0;
		  int criteriaSize = criteria.size();	      
	      
	      Object obj[];	      
		
		  // Allocate the size of the object parameter
		
	      if (criteria.containsKey("supplierCode")) {
	      	
	      	 criteriaSize--;
	      	 
	      }

             if (criteria.containsKey("includedUnposted")) {
	      	
	      	 criteriaSize--;
	      	 
	      }
             
             
             if (criteria.containsKey("directCheckOnly")) {
	      	
	      	 criteriaSize--;
	      	 
	      }
             
              if (criteria.containsKey("inscribeStock")) {
	      	
	      	 criteriaSize--;
	      	 
	      }
              
               if (criteria.containsKey("treasuryBill")) {
	      	
	      	 criteriaSize--;
	      	 
	      }
	      
	      obj = new Object[criteriaSize];
		       	      
		  if (criteria.containsKey("supplierCode")) {
		  	
		  	 if (!firstArgument) {
		  	 
		  	    jbossQl.append("AND ");	
		  	 	
		     } else {
		     	
		     	firstArgument = false;
		     	jbossQl.append("WHERE ");
		     	
		     }
		     
		  	 jbossQl.append("chk.apSupplier.splSupplierCode LIKE '%" + (String)criteria.get("supplierCode") + "%' ");
		  	 
		  }
                  
                   if (!bankAccountList.isEmpty()) {
			  
			  
                        Iterator j = bankAccountList.iterator();

                        if (!firstArgument) {

                            jbossQl.append("AND ");	
		  	 	
                        } else {

                            firstArgument = false;
                            jbossQl.append("WHERE ");

                        }
                          
                        jbossQl.append("chk.adBankAccount.baName in (");
		 	 
		 	  
		 	  boolean firstLoop = true;
		 	  
		 	  
                        while(j.hasNext()) {
			  		
                            if(firstLoop == false) { 
                                jbossQl.append(", "); 
                            }
                            else { 
                                firstLoop = false; 
                            }


                            String BNK_ACCNT = (String) j.next();

                            jbossQl.append("'"+BNK_ACCNT+"'");
                        
			  		
                        }
		  	
                        jbossQl.append(") ");

		  }
    
                  
                  /*
		  if (criteria.containsKey("bankAccount")) {
		  	
		   	  if (!firstArgument) {		       	  	
		   	     jbossQl.append("AND ");		       	     
		   	  } else {		       	  	
		   	  	 firstArgument = false;
		   	  	 jbossQl.append("WHERE ");		       	  	 
		   	  }
		  	 
		  	 jbossQl.append("chk.adBankAccount.baName=?" + (ctr+1) + " ");
		  	 obj[ctr] = (String)criteria.get("bankAccount");
		  	 ctr++;
		  	 
		  }
                  */
		  if (criteria.containsKey("referenceNumber")) {
			  	
		   	  if (!firstArgument) {		       	  	
		   	     jbossQl.append("AND ");		       	     
		   	  } else {		       	  	
		   	  	 firstArgument = false;
		   	  	 jbossQl.append("WHERE ");		       	  	 
		   	  }
		  	 
		  	 jbossQl.append("chk.adRefernceNumber.baName=?" + (ctr+1) + " ");
		  	 obj[ctr] = (String)criteria.get("bankAccount");
		  	 ctr++;
		  	 
		  }

		  
		  if (criteria.containsKey("supplierType")) {
	   	
		   	  if (!firstArgument) {		       	  	
		   	     jbossQl.append("AND ");		       	     
		   	  } else {		       	  	
		   	  	 firstArgument = false;
		   	  	 jbossQl.append("WHERE ");		       	  	 
		   	  }
		   	  
		   	  jbossQl.append("chk.apSupplier.apSupplierType.stName=?" + (ctr+1) + " ");
		   	  obj[ctr] = (String)criteria.get("supplierType");
		   	  ctr++;
	   	  
	      }	
			
		  if (criteria.containsKey("supplierClass")) {
	   	
		   	  if (!firstArgument) {		       	  	
		   	     jbossQl.append("AND ");		       	     
		   	  } else {		       	  	
		   	  	 firstArgument = false;
		   	  	 jbossQl.append("WHERE ");		       	  	 
		   	  }
		   	  
		   	  jbossQl.append("chk.apSupplier.apSupplierClass.scName=?" + (ctr+1) + " ");
		   	  obj[ctr] = (String)criteria.get("supplierClass");
		   	  ctr++;
	   	  
	      }			     
          			
		  if (criteria.containsKey("dateFrom")) {
		  	
		  	 if (!firstArgument) {
		  	 	jbossQl.append("AND ");
		  	 
		  	 } else {
		  	 
		  	 	firstArgument = false;
		  	 	jbossQl.append("WHERE ");
		  	 
		  	 }
		  	 
		  	 jbossQl.append("chk.chkDate>=?" + (ctr+1) + " ");
		  	 obj[ctr] = (Date)criteria.get("dateFrom");
		  	 ctr++;
		  }  
		      
		  if (criteria.containsKey("dateTo")) {
		  	
		  	 if (!firstArgument) {
		  	 
		  	 	jbossQl.append("AND ");
		  	 
		  	 } else {
		  	 
		  	 	firstArgument = false;
		  	 	jbossQl.append("WHERE ");
		  	 
		  	 }
		  	 
		  	 jbossQl.append("chk.chkDate<=?" + (ctr+1) + " ");
		  	 obj[ctr] = (Date)criteria.get("dateTo");
		  	 ctr++;
		  	 
		  }    
		      
		  if (criteria.containsKey("checkNumberFrom")) {
		  	
		  	 if (!firstArgument) {
		  	 
		  	 	jbossQl.append("AND ");
		  	 
		  	 } else {
		  	 
		  	 	firstArgument = false;
		  	 	jbossQl.append("WHERE ");
		  	 
		  	 }
		  	 
		  	 jbossQl.append("chk.chkNumber>=?" + (ctr+1) + " ");
		  	 obj[ctr] = (String)criteria.get("checkNumberFrom");
		  	 ctr++;
		  	 
		  }  
		      
		  if (criteria.containsKey("checkNumberTo")) {
		  	
		  	 if (!firstArgument) {
		  	 
		  	 	jbossQl.append("AND ");
		  	 
		  	 } else {
		  	 
		  	 	firstArgument = false;
		  	 	jbossQl.append("WHERE ");
		  	 
		  	 }
		  	 
		  	 jbossQl.append("chk.chkNumber<=?" + (ctr+1) + " ");
		  	 obj[ctr] = (String)criteria.get("checkNumberTo");
		  	 ctr++;
		  	 
		  } 	      
		      
		  if (criteria.containsKey("documentNumberFrom")) {
		  	
		  	 if (!firstArgument) {
		  	 
		  	 	jbossQl.append("AND ");
		  	 
		  	 } else {
		  	 
		  	 	firstArgument = false;
		  	 	jbossQl.append("WHERE ");
		  	 
		  	 }
		  	 
		  	 jbossQl.append("chk.chkDocumentNumber>=?" + (ctr+1) + " ");
		  	 obj[ctr] = (String)criteria.get("documentNumberFrom");
		  	 ctr++;
		  	 
		  }  
		      
		  if (criteria.containsKey("documentNumberTo")) {
		  	
		  	 if (!firstArgument) {
		  	 
		  	 	jbossQl.append("AND ");
		  	 
		  	 } else {
		  	 
		  	 	firstArgument = false;
		  	 	jbossQl.append("WHERE ");
		  	 
		  	 }
		  	 
		  	 jbossQl.append("chk.chkDocumentNumber<=?" + (ctr+1) + " ");
		  	 obj[ctr] = (String)criteria.get("documentNumberTo");
		  	 ctr++;
		  	 
		  }
                  
                  
                
		
		  if (INCLD_VD_CHCK!="") {

	    	  if (!firstArgument) {

		    	jbossQl.append("AND ");
	       	
	       	  } else {

	    		  firstArgument = false;
	    		  jbossQl.append("WHERE ");

	    	  }
	    	  System.out.println("INCLD_VD_CHCK: " + INCLD_VD_CHCK);
       		  if (INCLD_VD_CHCK.equals("All with Void Checks")){

       		  	jbossQl.append("((chk.chkPosted = 1 AND (chk.chkVoid = 1 OR chk.chkVoidPosted = 1)) OR (chk.chkPosted = 1 AND (chk.chkVoid = 0 AND chk.chkVoidPosted = 0)))");
       			a=1;
       		  }
	       	  if (INCLD_VD_CHCK.equals("Void Checks Only")){

	       		  jbossQl.append("((chk.chkVoid = 1 OR chk.chkVoidPosted = 1) AND chk.chkPosted = 1)");
	       		a=1;
	       	  }
	       	  
	       	  if (INCLD_VD_CHCK.equals("")){

	       		jbossQl.append("((chk.chkPosted = 1 AND chk.chkVoid = 0) OR (chk.chkPosted = 1 AND chk.chkVoid = 0 AND chk.chkVoidPosted = 0)) ");
	       	  }
	    	  
			  	 
	      }
		  
	      if (criteria.containsKey("includedUnposted")) {
	    	  String unposted;
	          unposted = (String)criteria.get("includedUnposted");
	          System.out.println("unposted: "+unposted);
	          if (!firstArgument) {
	        	  if (unposted.equals("NO")) { 
	        		  System.out.println("unposted4: "+unposted);
	        		  jbossQl.append("AND ");
	        	  }else{
	        		  System.out.println("unposted2: "+unposted);
	        		  if (INCLD_VD_CHCK.equals("All with Void Checks") || INCLD_VD_CHCK.equals("Void Checks Only")){
	        			  System.out.println("unposted3: "+unposted);
		    			  jbossQl.append("OR ");
		    		  }else{
		    			  jbossQl.append("AND ");
		    		  }
	        	  }
	       	
	       	  } else {
	       	  	
	       	  	 firstArgument = false;
	       	  	 jbossQl.append("WHERE ");
	       	  	 
	       	  }
	                  	
	       	  if (unposted.equals("NO")) {	   
	       		  
	       		  if (INCLD_VD_CHCK.equals("All with Void Checks")){

	       		  	jbossQl.append("((chk.chkPosted = 1 AND (chk.chkVoid = 1 OR chk.chkVoidPosted = 1)) OR (chk.chkPosted = 1 AND (chk.chkVoid = 0 AND chk.chkVoidPosted = 0)))");
	       		  }
		       	  if (INCLD_VD_CHCK.equals("Void Checks Only")){

		       		  jbossQl.append("((chk.chkVoid = 1 OR chk.chkVoidPosted = 1) AND chk.chkPosted = 1)");
		       		a=1;
		       	  }
		       	  
		       	  if (INCLD_VD_CHCK.equals("")){

		       		jbossQl.append("((chk.chkPosted = 1 AND chk.chkVoid = 0) OR (chk.chkPosted = 1 AND chk.chkVoid = 0 AND chk.chkVoidPosted = 0)) ");
		       	  }
		      } else {
		    	  	  if (INCLD_VD_CHCK.equals("All with Void Checks")){

		       			jbossQl.append("((chk.chkPosted = 0 AND (chk.chkVoid = 1 OR chk.chkVoidPosted = 1)) OR (chk.chkPosted = 0 AND (chk.chkVoid = 0 AND chk.chkVoidPosted = 0)))");
		       			a=1;
		       		  }
			       	  if (INCLD_VD_CHCK.equals("Void Checks Only")){

			       		  jbossQl.append("chk.chkVoid = 1 OR chk.chkVoidPosted = 1");
			       		a=1;
			       	  }
			       	  
			       	  if (INCLD_VD_CHCK.equals("")){

			       		jbossQl.append("chk.chkVoid = 0 ");
			       	  }
	       	  	  
	       	  	
	       	  }   	 
	       	  	       	  
	      }	  
	      
	      if (criteria.containsKey("directCheckOnly")) {
		  	
                if (!firstArgument) {

                       jbossQl.append("AND ");

                } else {

                       firstArgument = false;
                       jbossQl.append("WHERE ");

                }

                jbossQl.append("chk.chkType='DIRECT'");
           

            }
            
            if (criteria.containsKey("inscribeStock")) {
		  	
                if (!firstArgument) {

                       jbossQl.append("AND ");

                } else {

                       firstArgument = false;
                       jbossQl.append("WHERE ");

                }

                jbossQl.append("chk.chkInvtInscribedStock=1");
           

            }
             
            if (criteria.containsKey("treasuryBill")) {
		  	
                if (!firstArgument) {

                       jbossQl.append("AND ");

                } else {

                       firstArgument = false;
                       jbossQl.append("WHERE ");

                }

                jbossQl.append("chk.chkInvtTreasuryBill=1");
           

            }

	      if (SHOW_PDC) {

	    	  if (!firstArgument) {

	    		  jbossQl.append("AND ");

	    	  } else {

	    		  firstArgument = false;
	    		  jbossQl.append("WHERE ");

	    	  }

	    	  jbossQl.append("chk.chkDate<chk.chkCheckDate ");
			  	 
	      }
	      
	      if(adBrnchList.isEmpty()) {
	      	
	      	throw new GlobalNoRecordFoundException();
	          
	      } else {
	      	
	          if (!firstArgument) {
	              
	              jbossQl.append("AND ");
	              
	          } else {
	              
	              firstArgument = false;
	              jbossQl.append("WHERE ");
	              
	          }	      
	          
	          jbossQl.append("chk.chkAdBranch in (");
	          
	          boolean firstLoop = true;
	          
	          Iterator i = adBrnchList.iterator();
	          
	          while(i.hasNext()) {
	              
	              if(firstLoop == false) { 
	                  jbossQl.append(", "); 
	              }
	              else { 
	                  firstLoop = false; 
	              }
	              
	              AdBranchDetails mdetails = (AdBranchDetails) i.next();
	              
	              jbossQl.append(mdetails.getBrCode());
	              
	          }
	          
	          jbossQl.append(") ");
	          
	          firstArgument = false;
	          
	      }
	      
	      if (!firstArgument) {
		       	  	
       	     jbossQl.append("AND ");
       	     
       	  } else {
       	  	
       	  	 firstArgument = false;
       	  	 jbossQl.append("WHERE ");
       	  	 
       	  }
              
            
       	  
       	  jbossQl.append("chk.chkAdCompany = " + AD_CMPNY + " ");
			System.out.println("jbossQl.toString(): " + jbossQl.toString());
       	  Collection apChecks = apCheckHome.getChkByCriteria(jbossQl.toString(), obj);	         
		  
		  if (apChecks.size() == 0)
		     throw new GlobalNoRecordFoundException();
		     
		  Iterator i = apChecks.iterator();
		 
		  while (i.hasNext()) {
		  	
		  	LocalApCheck apCheck = (LocalApCheck)i.next();   	  
		  	
		  	if (SHOW_ENTRIES) {
		  		
		  		boolean first = true;		  		
		  		

		  		Collection distributionRecords = apDistributionRecordHome.findByChkCode(apCheck.getChkCode() , AD_CMPNY);
		  		
		  		Iterator j = distributionRecords.iterator();
		  		
		  		while (j.hasNext()) {
		  		
		  			LocalApDistributionRecord distributionRecord = (LocalApDistributionRecord)j.next();
		  			LocalGlChartOfAccount glChartOfAccount = distributionRecord.getGlChartOfAccount();
		  			
		  			ApRepCheckRegisterDetails details = new ApRepCheckRegisterDetails();
		  			details.setCrDate(apCheck.getChkDate());
		  			details.setCrCheckNumber(apCheck.getChkNumber());
		  			details.setCrReferenceNumber(apCheck.getChkReferenceNumber());
		  			details.setCrDocumentNumber(apCheck.getChkDocumentNumber());
		  			details.setCrDescription(apCheck.getChkDescription());
		  			details.setCrSplSupplierCode(apCheck.getApSupplier().getSplSupplierCode() + "-" + 
		  						(apCheck.getChkSupplierName() == null ? apCheck.getApSupplier().getSplName() : apCheck.getChkSupplierName()));		  	  
		  			details.setCrSplSupplierClass(apCheck.getApSupplier().getApSupplierClass().getScName());
		  			details.setCrSplSupplierCode2(apCheck.getApSupplier().getSplSupplierCode());
		  			details.setCrSplName(apCheck.getChkSupplierName() == null ? apCheck.getApSupplier().getSplName() : apCheck.getChkSupplierName());
		  			details.setCrCheckDate(apCheck.getChkCheckDate());
		  			details.setCrCheckVoid(new Byte(apCheck.getChkVoid()));
		  			details.setCrCheckVoidPosted(new Byte(apCheck.getChkVoidPosted()));

		  			//details.setCrBankAccount(apCheck.getAdBankAccount().getBaName());
		  			// type
		  			if(apCheck.getApSupplier().getApSupplierType() == null){
		  				
		  				details.setCrSplSupplierType("UNDEFINE");
		  				
		  			} else {
		  				
		  				details.setCrSplSupplierType(apCheck.getApSupplier().getApSupplierType().getStName());
		  				
		  			}
		  			if (first) {
		  				//if(apCheck.getChkVoid()!=1 && apCheck.getChkVoidPosted()!=1){
		  				// set amount only on the first iteration.
		  				if(currency.equals("USD") && apCheck.getGlFunctionalCurrency().getFcName().equals("USD")){

		  					details.setCrAmount(apCheck.getChkAmount());

		  				}else{
		  					details.setCrAmount(EJBCommon.roundIt(this.convertForeignToFunctionalCurrency(
		  							apCheck.getGlFunctionalCurrency().getFcCode(),
		  							apCheck.getGlFunctionalCurrency().getFcName(),
		  							apCheck.getChkConversionDate(),
		  							apCheck.getChkConversionRate(),
		  							apCheck.getChkAmount(), AD_CMPNY), adCompany.getGlFunctionalCurrency().getFcPrecision()));

		  				}
			  				
			  				first = false;
		  				//}	
		  			} else {
		  				
		  				details.setCrAmount(0);
		  				
		  			}
		  			details.setCrBankAccount(apCheck.getAdBankAccount().getBaName());
		  			//System.out.println("ORDER_BY: " + ORDER_BY);
		  			details.setOrderBy(ORDER_BY);
		  			
		  			// entries
		  			details.setCrDrGlCoaAccountNumber(glChartOfAccount.getCoaAccountNumber());
		  			details.setCrDrGlCoaAccountDescription(glChartOfAccount.getCoaAccountDescription());
		  			
		  			if (distributionRecord.getDrDebit() == 1) {
		  				
		  				details.setCrDrDebit(distributionRecord.getDrAmount());
		  				
		  			} else {
		  				
		  				details.setCrDrCredit(distributionRecord.getDrAmount());

		  			}
		  			details.setCrFcSymbol(apCheck.getGlFunctionalCurrency().getFcSymbol());
		  			list.add(details);
		  			
		  		}		
		  		
		  	} else {
		  		
		  		ApRepCheckRegisterDetails details = new ApRepCheckRegisterDetails();
		  		details.setCrDate(apCheck.getChkDate());
		  		details.setCrCheckNumber(apCheck.getChkNumber());
		  		details.setCrReferenceNumber(apCheck.getChkReferenceNumber());
		  		details.setCrDocumentNumber(apCheck.getChkDocumentNumber());
		  		details.setCrDescription(apCheck.getChkDescription());
		  		details.setCrSplSupplierCode(apCheck.getApSupplier().getSplSupplierCode() + "-" + 
		  					(apCheck.getChkSupplierName() == null ? apCheck.getApSupplier().getSplName() : apCheck.getChkSupplierName()));		  	  
		  		details.setCrSplSupplierClass(apCheck.getApSupplier().getApSupplierClass().getScName());
	  			details.setCrSplSupplierCode2(apCheck.getApSupplier().getSplSupplierCode());
	  			details.setCrSplName(apCheck.getChkSupplierName() == null ? apCheck.getApSupplier().getSplName() : apCheck.getChkSupplierName());
	  			details.setCrCheckDate(apCheck.getChkCheckDate());
	  			details.setCrCheckVoid(new Byte(apCheck.getChkVoid()));
	  			details.setCrCheckVoidPosted(new Byte(apCheck.getChkVoidPosted()));

		  		// type
		  		if(apCheck.getApSupplier().getApSupplierType() == null){
		  			
                                    details.setCrSplSupplierType("UNDEFINE");
		  			
		  		} else {
		  			
                                    details.setCrSplSupplierType(apCheck.getApSupplier().getApSupplierType().getStName());
		  			
		  		}
                                
                                
                              
		  		
		  		//if(apCheck.getChkVoid()!=1 && apCheck.getChkVoidPosted()!=1){
		  		if(currency.equals("USD") && apCheck.getGlFunctionalCurrency().getFcName().equals("USD")){

  					details.setCrAmount(apCheck.getChkAmount());

  				}else{
  					details.setCrAmount(EJBCommon.roundIt(this.convertForeignToFunctionalCurrency(
			  				apCheck.getGlFunctionalCurrency().getFcCode(),
							apCheck.getGlFunctionalCurrency().getFcName(),
							apCheck.getChkConversionDate(),
							apCheck.getChkConversionRate(),
							apCheck.getChkAmount(), AD_CMPNY), adCompany.getGlFunctionalCurrency().getFcPrecision()));
  				}
		  			
		  		//}
		  		
                                
                                
		  		details.setCrBankAccount(apCheck.getAdBankAccount().getBaName());
		  		details.setOrderBy(ORDER_BY);
		  		details.setCrFcSymbol(apCheck.getGlFunctionalCurrency().getFcSymbol());
		  		
                                details.setCrInvestInscribeStock(apCheck.getChkInvtInscribedStock());
                                details.setCrInvestTreasuryBills(apCheck.getChkInvtTreasuryBill());
                                details.setCrInvestBidYield(apCheck.getChkInvtBidYield());
                                details.setCrInvestCouponRate(apCheck.getChkInvtCouponRate());
                                details.setCrInvestFaceValue(apCheck.getChkInvtFaceValue());
                                details.setCrInvestMaturityDate(apCheck.getChkInvtMaturityDate());
                                details.setCrInvestNextRunDate(apCheck.getChkInvtNextRunDate());
                                details.setCrInvestNumberOfDays(0.0);
                                details.setCrInvestPremAmount(apCheck.getChkInvtPremiumAmount());
                                details.setCrInvestSettlementAmount(apCheck.getChkInvtSettleAmount());
                                details.setCrInvestSettlementDate(apCheck.getChkInvtSettlementDate());
                                details.setCrDrGlCoaAccountNumber("");
                                details.setCrDrGlCoaAccountDescription("");
                                
                                
                                
                                
                                if(accountName.equals("")){
                                      list.add(details);
                                }else{
                                    
                                    double coaAmount = 0.0d;
                                
                                    Collection distributionRecords = apDistributionRecordHome.findByChkCode(apCheck.getChkCode() , AD_CMPNY);

                                    Iterator j = distributionRecords.iterator();

                                    while (j.hasNext()) {

                                        LocalApDistributionRecord distributionRecord = (LocalApDistributionRecord)j.next();

                                        LocalGlChartOfAccount glChartOfAccount = distributionRecord.getGlChartOfAccount();

                                        if(glChartOfAccount.getCoaAccountDescription().equals(accountName)){
                                            details.setCrDrGlCoaAccountNumber(glChartOfAccount.getCoaAccountNumber());
                                            details.setCrDrGlCoaAccountDescription(glChartOfAccount.getCoaAccountDescription());

                                            coaAmount += distributionRecord.getDrAmount(); 
                                        }
                                        
                                        
                                            

                                    }
                                    
                                    
                                    details.setCrAmount(EJBCommon.roundIt(this.convertForeignToFunctionalCurrency(
			  				apCheck.getGlFunctionalCurrency().getFcCode(),
							apCheck.getGlFunctionalCurrency().getFcName(),
							apCheck.getChkConversionDate(),
							apCheck.getChkConversionRate(),
							coaAmount, AD_CMPNY), adCompany.getGlFunctionalCurrency().getFcPrecision()));
                                    
                                 
                                    if(distributionRecords.size()>0){
                                         list.add(details);
                                    }
                                }
                           
		  	}
		  	
		  }
			
		  // sort

		  if(SHOW_ENTRIES) {

			  System.out.println("DEBIT_FIRST: "+DEBIT_FIRST );
			  
			  if(DEBIT_FIRST)
				  Collections.sort(list, ApRepCheckRegisterDetails.NoGroupComparator);
			  else
				  Collections.sort(list, ApRepCheckRegisterDetails.CoaAccountNumberComparator);
		  }
		  
		  if (GROUP_BY.equals("SUPPLIER CODE")) {
       	  	
       	  	Collections.sort(list, ApRepCheckRegisterDetails.SupplierCodeComparator);

       	  } else if (GROUP_BY.equals("SUPPLIER TYPE")) {
       	  	
       	  	Collections.sort(list, ApRepCheckRegisterDetails.SupplierTypeComparator);

       	  } else if (GROUP_BY.equals("SUPPLIER CLASS")) {
       	  	
       	  	Collections.sort(list, ApRepCheckRegisterDetails.SupplierClassComparator);

       	  } else {
       	  	
       	  	Collections.sort(list, ApRepCheckRegisterDetails.NoGroupComparator);

	      }
		  
		  if (SUMMARIZE) {
		  	
		  	Collections.sort(list, ApRepCheckRegisterDetails.CoaAccountNumberComparator);
		  	
		  }
		  
			  //System.out.println("sort checkVoid");
		     Collections.sort(list, ApRepCheckRegisterDetails.checkVoid);
			  //Collections.sort(list, ApRepCheckRegisterDetails.checkVoidPosted);
		  
		  return list;
		      
	  } catch (GlobalNoRecordFoundException ex) {
	  	 
	  	  throw ex;
	  	
	  } catch (Exception ex) {
	  	

	  	  ex.printStackTrace();
	  	  throw new EJBException(ex.getMessage());
	  	
	  }
        
    }
    
	/**
	* @ejb:interface-method view-type="remote"
	* @jboss:method-attributes read-only="true"
	**/
	public com.util.AdCompanyDetails getAdCompany(Integer AD_CMPNY) {
	
	  Debug.print("ApRepCheckRegisterControllerBean getAdCompany");      
	  
	  LocalAdCompanyHome adCompanyHome = null;
	        
	  // Initialize EJB Home
	    
	  try {
	        
	       adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
	           lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
	        
	  } catch (NamingException ex) {
	        
	      throw new EJBException(ex.getMessage());
	        
	  }
	
	  try {
	      
	     LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
	     
	     AdCompanyDetails details = new AdCompanyDetails();
	     details.setCmpName(adCompany.getCmpName());
	     
	     return details;  	
	  	       	
	  } catch (Exception ex) {
	  	
	  	 Debug.printStackTrace(ex);
	  	 throw new EJBException(ex.getMessage());
	  	  
	  }
	  
	}  
	
        /**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/   
	public ArrayList getEnableGlCoa(Integer AD_CMPNY) 
		throws GlobalNoRecordFoundException{
            
            Debug.print("ApRepCheckRegisterControllerBean getEnableGlCoa");
	    
            LocalGlChartOfAccountHome glChartOfAccountHome= null;
            
            
            Collection glChartOfAccounts = null;
            
            ArrayList list = new ArrayList();
	    
	    // Initialize EJB Home
	    
	    try {
	        
	        glChartOfAccountHome = (LocalGlChartOfAccountHome)EJBHomeFactory.
	        lookUpLocalHome(LocalGlChartOfAccountHome.JNDI_NAME, LocalGlChartOfAccountHome.class);
	      
	    } catch (NamingException ex) {
	        
	        throw new EJBException(ex.getMessage());
	        
	    }
            
            try {
	        
	        glChartOfAccounts = glChartOfAccountHome.findCoaAllEnabled(new java.util.Date(), AD_CMPNY);
	        
	    } catch (FinderException ex) {
	        
	    } catch (Exception ex) {
	        
	        throw new EJBException(ex.getMessage());
	    }
	    
	    if (glChartOfAccounts.isEmpty()) {
	        
	        throw new GlobalNoRecordFoundException();
	        
	    }
	
	    try {
	        
	        Iterator i = glChartOfAccounts.iterator();
	        
	        while(i.hasNext()) {
	            
	            LocalGlChartOfAccount glChartOfAccount = (LocalGlChartOfAccount)i.next();
	            
	            GlChartOfAccountDetails details = new GlChartOfAccountDetails();
                    
                    
                    String coaName = glChartOfAccount.getCoaAccountDescription();
                    
                 
	            
	            list.add(coaName);
	            
	        }	               
	        
	    } catch (Exception ex) {
	        
	        throw new EJBException(ex.getMessage());
	    }
            
            
            
            
            return list;
            
        }
        
        
        
        
        
        
	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/   
	public ArrayList getAdBrResAll(Integer RS_CODE, Integer AD_CMPNY) 
		throws GlobalNoRecordFoundException{
	    
	    Debug.print("ApRepCheckRegisterControllerBean getAdBrResAll");
	    
	    LocalAdBranchResponsibilityHome adBranchResponsibilityHome = null;
	    LocalAdBranchHome adBranchHome = null;
	    
	    LocalAdBranchResponsibility adBranchResponsibility = null;
	    LocalAdBranch adBranch = null;
	    
	    Collection adBranchResponsibilities = null;
	    
	    ArrayList list = new ArrayList();
	    
	    // Initialize EJB Home
	    
	    try {
	        
	        adBranchResponsibilityHome = (LocalAdBranchResponsibilityHome)EJBHomeFactory.
	        lookUpLocalHome(LocalAdBranchResponsibilityHome.JNDI_NAME, LocalAdBranchResponsibilityHome.class);
	        adBranchHome = (LocalAdBranchHome)EJBHomeFactory.
	        lookUpLocalHome(LocalAdBranchHome.JNDI_NAME, LocalAdBranchHome.class);
	        
	    } catch (NamingException ex) {
	        
	        throw new EJBException(ex.getMessage());
	        
	    }
	    
	    try {
	        
	        adBranchResponsibilities = adBranchResponsibilityHome.findByAdResponsibility(RS_CODE, AD_CMPNY);
	        
	    } catch (FinderException ex) {
	        
	    } catch (Exception ex) {
	        
	        throw new EJBException(ex.getMessage());
	    }
	    
	    if (adBranchResponsibilities.isEmpty()) {
	        
	        throw new GlobalNoRecordFoundException();
	        
	    }
	    
	    try {
	        
	        Iterator i = adBranchResponsibilities.iterator();
	        
	        while(i.hasNext()) {
	            
	            adBranchResponsibility = (LocalAdBranchResponsibility)i.next();
	            
	            adBranch = adBranchResponsibility.getAdBranch();
	            
	            AdBranchDetails details = new AdBranchDetails();
	            
	            details.setBrCode(adBranch.getBrCode());
	            details.setBrBranchCode(adBranch.getBrBranchCode());
	            details.setBrName(adBranch.getBrName());
	            details.setBrHeadQuarter(adBranch.getBrHeadQuarter());
	            
	            list.add(details);
	            
	        }	               
	        
	    } catch (Exception ex) {
	        
	        throw new EJBException(ex.getMessage());
	    }
	    
	    return list;
	    
	}    

	// private methods
	private double convertForeignToFunctionalCurrency(Integer FC_CODE, String FC_NM, 
	    Date CONVERSION_DATE, double CONVERSION_RATE, double AMOUNT, Integer AD_CMPNY) {
	    	
	    Debug.print("ApRepCheckRegisterControllerBean convertForeignToFunctionalCurrency");
	    
	    
        LocalGlFunctionalCurrencyRateHome glFunctionalCurrencyRateHome = null;
        LocalAdCompanyHome adCompanyHome = null;
         
        LocalAdCompany adCompany = null;
                 
        // Initialize EJB Homes
         
        try {
         	
            glFunctionalCurrencyRateHome = (LocalGlFunctionalCurrencyRateHome)EJBHomeFactory.
               lookUpLocalHome(LocalGlFunctionalCurrencyRateHome.JNDI_NAME, LocalGlFunctionalCurrencyRateHome.class);                         
            adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
               lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);             
                   
         } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
         }
         
         // get company and extended precision
         
         try {
         	
             adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
         	             
         } catch (Exception ex) {
         	
             throw new EJBException(ex.getMessage());
         	
         }	     
        
         
         // Convert to functional currency if necessary
         
         if (CONVERSION_RATE != 1 && CONVERSION_RATE != 0 ) {
         	         
             AMOUNT = AMOUNT / CONVERSION_RATE;
             	
         }
         return EJBCommon.roundIt(AMOUNT, adCompany.getGlFunctionalCurrency().getFcPrecision());
         
	}

    // SessionBean methods

    /**
     * @ejb:create-method view-type="remote"
     **/
    public void ejbCreate() throws CreateException {

       Debug.print("ApRepCheckRegisterControllerBean ejbCreate");
      
    }
}
