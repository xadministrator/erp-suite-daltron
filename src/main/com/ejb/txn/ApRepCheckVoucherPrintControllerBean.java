
/*
 * ApRepCheckVoucherPrintControllerBean.java
 *
 * Created on February 24, 2004, 9:02 AM
 *
 * @author  Neil Andrew M. Ajero
 */

package com.ejb.txn;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.Iterator;
import java.util.StringTokenizer;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;
import javax.naming.NamingException;

import com.ejb.ad.LocalAdApproval;
import com.ejb.ad.LocalAdApprovalDocument;
import com.ejb.ad.LocalAdApprovalDocumentHome;
import com.ejb.ad.LocalAdApprovalHome;
import com.ejb.ad.LocalAdBranch;
import com.ejb.ad.LocalAdBranchHome;
import com.ejb.ad.LocalAdCompany;
import com.ejb.ad.LocalAdCompanyHome;
import com.ejb.ad.LocalAdPreference;
import com.ejb.ad.LocalAdPreferenceHome;
import com.ejb.ad.LocalAdUser;
import com.ejb.ad.LocalAdUserHome;
import com.ejb.ap.LocalApAppliedVoucher;
import com.ejb.ap.LocalApAppliedVoucherHome;
import com.ejb.ap.LocalApCheck;
import com.ejb.ap.LocalApCheckHome;
import com.ejb.ap.LocalApDistributionRecord;
import com.ejb.ap.LocalApDistributionRecordHome;
import com.ejb.ap.LocalApTaxCode;
import com.ejb.ap.LocalApWithholdingTaxCode;
import com.ejb.exception.GenVSVNoValueSetValueFoundException;
import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.genfld.LocalGenField;
import com.ejb.genfld.LocalGenSegment;
import com.ejb.genfld.LocalGenSegmentHome;
import com.ejb.genfld.LocalGenValueSet;
import com.ejb.genfld.LocalGenValueSetValue;
import com.ejb.genfld.LocalGenValueSetValueHome;
import com.ejb.gl.LocalGlFunctionalCurrencyRateHome;
import com.util.AbstractSessionBean;
import com.util.AdCompanyDetails;
import com.util.ApRepCheckVoucherPrintDetails;
import com.util.Debug;
import com.util.EJBCommon;
import com.util.EJBHomeFactory;

/**
 * @ejb:bean name="ApRepCheckVoucherPrintControllerEJB"
 *           display-name="Used for printing voucher transactions"
 *           type="Stateless"
 *           view-type="remote"
 *           jndi-name="ejb/ApRepCheckVoucherPrintControllerEJB"
 *
 * @ejb:interface remote-class="com.ejb.txn.ApRepCheckVoucherPrintController"
 *                extends="javax.ejb.EJBObject"
 *
 * @ejb:home remote-class="com.ejb.txn.ApRepCheckVoucherPrintControllerHome"
 *           extends="javax.ejb.EJBHome"
 *
 * @ejb:transaction type="Required"
 * 
*/

public class ApRepCheckVoucherPrintControllerBean extends AbstractSessionBean {
    
    
    /**
     * @ejb:interface-method view-type="remote"
     **/
    public ArrayList executeApRepCheckVoucherPrint(ArrayList chkCodeList, Integer AD_CMPNY) 
        throws GlobalNoRecordFoundException,
        GenVSVNoValueSetValueFoundException {
                    
        Debug.print("ApRepCheckVoucherPrintControllerBean executeApRepCheckVoucherPrint");
        
        LocalApCheckHome apCheckHome = null;
        LocalAdApprovalHome adApprovalHome = null;
        LocalAdApprovalDocumentHome adApprovalDocumentHome = null;
        LocalApDistributionRecordHome apDistributionRecordHome = null;
        LocalAdPreferenceHome adPreferenceHome = null;
        LocalGenValueSetValueHome genValueSetValueHome = null;
        LocalAdUserHome adUserHome = null;
        LocalAdBranchHome adBranchHome = null;
        LocalAdCompanyHome adCompanyHome = null;
        
        ArrayList list = new ArrayList();
        String asd = "a";
        // Initialize EJB Home
        int a=1;
        try {
            
            apCheckHome = (LocalApCheckHome)EJBHomeFactory.
                lookUpLocalHome(LocalApCheckHome.JNDI_NAME, LocalApCheckHome.class);
	        adApprovalHome = (LocalAdApprovalHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdApprovalHome.JNDI_NAME, LocalAdApprovalHome.class);
            adApprovalDocumentHome = (LocalAdApprovalDocumentHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdApprovalDocumentHome.JNDI_NAME, LocalAdApprovalDocumentHome.class);
            apDistributionRecordHome = (LocalApDistributionRecordHome)EJBHomeFactory.
			    lookUpLocalHome(LocalApDistributionRecordHome.JNDI_NAME, LocalApDistributionRecordHome.class);
            adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
		    	lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);
            genValueSetValueHome = (LocalGenValueSetValueHome)EJBHomeFactory.
	    		lookUpLocalHome(LocalGenValueSetValueHome.JNDI_NAME, LocalGenValueSetValueHome.class);
            adUserHome = (LocalAdUserHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdUserHome.JNDI_NAME, LocalAdUserHome.class);
            adBranchHome = (LocalAdBranchHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdBranchHome.JNDI_NAME, LocalAdBranchHome .class);
            adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome .class);
            
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }        
        
        try {
        	
        	Iterator i = chkCodeList.iterator();
        	
        	while (i.hasNext()) {
        	
        	    Integer CHK_CODE = (Integer) i.next();
        	
        	    double TOTAL_AMOUNT = 0;
	        	LocalApCheck apCheck = null;
	        	
	        	
	        	try {
	        		
	        		apCheck = apCheckHome.findByPrimaryKey(CHK_CODE);
	        		
	        	} catch (FinderException ex) {
	        		
	        		continue;
	        		
	        	}
	        	
	        	LocalAdApproval adApproval = adApprovalHome.findByAprAdCompany(AD_CMPNY);
	        	LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);
	        	LocalAdApprovalDocument adApprovalDocument = adApprovalDocumentHome.findByAdcType("AP CHECK", AD_CMPNY);
	        	
	        	if (adApprovalDocument.getAdcPrintOption().equals("PRINT APPROVED ONLY")) {
	        	
	        		if (apCheck.getChkApprovalStatus() == null || 
	        			apCheck.getChkApprovalStatus().equals("PENDING")) {
	        		    
	        		    continue;
	        		    
	        		} 
	        	
	        	
	        	} else if (adApprovalDocument.getAdcPrintOption().equals("PRINT UNAPPROVED ONLY")) {
	        	
	        		if (apCheck.getChkApprovalStatus() != null && 
	        			(apCheck.getChkApprovalStatus().equals("N/A") || 
	        			 apCheck.getChkApprovalStatus().equals("APPROVED"))) {
	        		
	        			continue;
	        		
	        		}
	        	
	        	}
	        	
	        	if (adApprovalDocument.getAdcAllowDuplicate() == EJBCommon.FALSE && 
	        		apCheck.getChkCvPrinted() == EJBCommon.TRUE){
	        		
	        		continue;	
	        		
	        	}
	        		        		        	
	        	// show duplicate
	        	
	        	boolean showDuplicate = false;
	        	
	        	if (adApprovalDocument.getAdcTrackDuplicate() == EJBCommon.TRUE &&
	        		apCheck.getChkCvPrinted() == EJBCommon.TRUE) {
	        		
	        		showDuplicate = true;
	        		
	        	}
	        	    
	        	// set printed
	        	
	        	apCheck.setChkCvPrinted(EJBCommon.TRUE);
	        	
	        	if(apCheck.getChkType().equals("PAYMENT") && adPreference.getPrfApCheckVoucherDataSource().equals("AP VOUCHER")){
	        		
	        		Collection apAppliedVouchers = apCheck.getApAppliedVouchers();
	        		
	        		Collection apDistributionRecords = apDistributionRecordHome.findDrsByDrClassAndChkCode("CASH", 
	        			apCheck.getChkCode(), AD_CMPNY);
	        		
	        		Iterator iter = apDistributionRecords.iterator();
	        		
	        		LocalApDistributionRecord apChkCaskDr = null;
	        		
	        		while(iter.hasNext()) {
	        			
	        			apChkCaskDr = (LocalApDistributionRecord) iter.next();
	        			
	        			if (apChkCaskDr.getClass().equals("CASH"))
	        				break;
	        			
	        		}

	        		iter = apAppliedVouchers.iterator();
	        		
	        		while(iter.hasNext()) {
	        			
	        			LocalApAppliedVoucher apAppliedVoucher = (LocalApAppliedVoucher) iter.next();
	        			boolean first = true;
	        			
	        			Iterator drIter = apAppliedVoucher.getApVoucherPaymentSchedule().getApVoucher().getApDistributionRecords().iterator();
	        			
	        			while(drIter.hasNext()) {
	        				
	        				ApRepCheckVoucherPrintDetails details = new ApRepCheckVoucherPrintDetails();
	        				
	        				if(!asd.equals(apCheck.getChkDocumentNumber()) && asd!="a"){
		        				a++;
		        			}
	        				
	        				details.setCvCheckType(apCheck.getChkType());   
	        				details.setCvChkNumber(apCheck.getChkNumber());
	        				details.setCvChkSplName(apCheck.getChkSupplierName() == null ? apCheck.getApSupplier().getSplName() : apCheck.getChkSupplierName());
        					details.setCvChkDocumentNumber(apCheck.getChkDocumentNumber());
        					details.setCvChkDate(apCheck.getChkCheckDate());
        					details.setCvChkCurrencyDescription(apCheck.getGlFunctionalCurrency().getFcDescription());
        					details.setCvChkCurrencySymbol(apCheck.getGlFunctionalCurrency().getFcSymbol());
        					details.setCvChkCreatedBy(apCheck.getChkCreatedBy());
        					details.setCvChkCheckedBy(adPreference.getPrfApDefaultChecker());
        					details.setCvChkApprovedRejectedBy(apCheck.getChkApprovedRejectedBy());
        					details.setCvChkDescription(apCheck.getChkDescription());
        					details.setCvChkReferenceNumber(apCheck.getChkReferenceNumber());
    	        			details.setCvChkAdBaName(apCheck.getAdBankAccount().getBaName());
    	        			details.setCvChkAdBaAccountNumber(apCheck.getAdBankAccount().getBaAccountNumber());
    	        			details.setCvChkSplAddress(apCheck.getApSupplier().getSplAddress());
    	        			details.setCvChkMemo(apCheck.getChkMemo());
    	        			details.setCvVouNumber(apAppliedVoucher.getApVoucherPaymentSchedule().getApVoucher().getVouDocumentNumber());
    	        			details.setCvChkSplCode(apCheck.getApSupplier().getSplSupplierCode());
    	        			//details.setCvChkAmount(apCheck.getChkAmount());
    	        			details.setCvChkAmount(this.convertForeignToFunctionalCurrency(apCheck.getGlFunctionalCurrency().getFcCode(), apCheck.getGlFunctionalCurrency().getFcName(), apCheck.getChkConversionDate(), apCheck.getChkConversionRate(), apCheck.getChkAmount(), AD_CMPNY));
    	        			System.out.println("vouNumber1: " + apAppliedVoucher.getApVoucherPaymentSchedule().getApVoucher().getVouDocumentNumber());
	        				
    	        			// get user name description
	        				
	        			/*	LocalAdUser adUser = adUserHome.findByUsrName(apCheck.getChkCreatedBy(), AD_CMPNY);
	        				details.setCvChkCreatedByDescription(adUser.getUsrDescription());
	        				
	        				LocalAdUser adUser2 = adUserHome.findByUsrName(adPreference.getPrfApDefaultChecker(), AD_CMPNY);
	        				details.setCvChkCheckByDescription(adUser2.getUsrDescription());
	        				
	        				LocalAdUser adUser3 = adUserHome.findByUsrName(apCheck.getChkApprovedRejectedBy(), AD_CMPNY);
	        				details.setCvChkApprovedRejectedByDescription(adUser3.getUsrDescription());
	        				*/
    	        			System.out.println("apCheck.getChkCreatedBy(): "+apCheck.getChkCreatedBy());
    	        			System.out.println("apCheck.getChkApprovedRejectedBy(): "+apCheck.getChkApprovedRejectedBy());
            				try{
            					LocalAdUser adUser = adUserHome.findByUsrName(apCheck.getChkCreatedBy(), AD_CMPNY);
                				details.setCvChkCreatedByDescription(adUser.getUsrName());
                				details.setCvChkApprovedRejectedByDescription(adUser.getUsrDescription());
            				}catch(Exception e){
            					details.setCvChkCreatedByDescription("");
            				}

            				try{
            					System.out.println("adPreference.getPrfApDefaultChecker()(): "+adPreference.getPrfApDefaultChecker());
                				LocalAdUser adUser2 = adUserHome.findByUsrName(adPreference.getPrfApDefaultChecker(), AD_CMPNY);
                				details.setCvChkCheckByDescription(adUser2.getUsrDescription());
                				details.setCvChkApprovedRejectedByDescription(adUser2.getUsrDescription());
            				}catch(Exception e){
            					details.setCvChkCheckByDescription("");
            				}

            				try{
            					System.out.println("apCheck.getChkApprovedRejectedBy(): "+apCheck.getChkApprovedRejectedBy());
                				LocalAdUser adUser3 = adUserHome.findByUsrName(apCheck.getChkApprovedRejectedBy(), AD_CMPNY);
                				details.setCvChkApprovedRejectedByDescription(adUser3.getUsrDescription());
            				}catch(Exception e){
            					details.setCvChkApprovedRejectedByDescription("");
            				}
            				
            				try{
            					System.out.println("2: " + apCheck.getChkPostedBy());
            					LocalAdUser adUser4 = adUserHome.findByUsrName(apCheck.getChkPostedBy(), AD_CMPNY);
                				details.setCvVouPostedBy(adUser4.getUsrDescription());
                				System.out.println("postedBy: " + adUser4.getUsrDescription());
            				}catch(Exception e){
            					details.setCvVouPostedBy("");
            				}
            				
	        				if(first) {
	        					System.out.println("vouNumber2: " + apAppliedVoucher.getApVoucherPaymentSchedule().getApVoucher().getVouDocumentNumber());
	        					details.setCvVouNumber(apAppliedVoucher.getApVoucherPaymentSchedule().getApVoucher().getVouDocumentNumber());
	        					details.setCvVouDate(apAppliedVoucher.getApVoucherPaymentSchedule().getVpsDueDate());
	        					details.setCvVouDescription (apAppliedVoucher.getApVoucherPaymentSchedule().getApVoucher().getVouDescription());
	        					//details.setCvVouPostedBy(apCheck.getChkPostedBy());
	        					details.setCvApplyAmount(apAppliedVoucher.getAvApplyAmount());
	        					details.setCvVouReferenceNumber(apAppliedVoucher.getApVoucherPaymentSchedule().getApVoucher().getVouReferenceNumber());
	        					
	        				}
	        				
	        				LocalApTaxCode apTaxCode = apAppliedVoucher.getApVoucherPaymentSchedule().getApVoucher().getApTaxCode();
	        				LocalApWithholdingTaxCode apWithholdingTaxCode = apAppliedVoucher.getApVoucherPaymentSchedule().getApVoucher().getApWithholdingTaxCode();
	        				short precisionUnit = this.getGlFcPrecisionUnit(AD_CMPNY);
	        				
	        				double NET_AMNT = 0d;
	        				double TOTAL_NET_AMNT = 0d;
	        				double TAX_AMNT = 0d;
	        				double WTAX_AMNT = 0d;
	        				double ratio = 0d;
	        				
	        				// calculate net amount
	        				NET_AMNT = EJBCommon.roundIt((apAppliedVoucher.getAvApplyAmount() / ((1 + (apTaxCode.getTcRate()/100)) - (apWithholdingTaxCode.getWtcRate()/100))), precisionUnit);
	        				
	        				// calculate total net amount
        					LocalApDistributionRecord apDR = null;
        					
	        				// calculate tax
	        				if(first && apTaxCode.getTcRate() != 0){
	        					TAX_AMNT = EJBCommon.roundIt(NET_AMNT * apTaxCode.getTcRate()/100, precisionUnit);
	        					details.setCvApplyTaxAmount(TAX_AMNT);
	        				}
	        				
	        				
	        				
							try {
	        				
								apDR = apDistributionRecordHome.findByDrClassAndVouCode("W-TAX", apAppliedVoucher.getApVoucherPaymentSchedule().getApVoucher().getVouCode(), AD_CMPNY);
								
							} catch (FinderException ex) {
								
							}

        					if (apDR != null ) {

        						// calculate wtax
        						
        						if(first && apWithholdingTaxCode.getWtcRate() != 0) {
    	        					WTAX_AMNT = EJBCommon.roundIt(NET_AMNT * apWithholdingTaxCode.getWtcRate()/100, precisionUnit) + apAppliedVoucher.getAvTaxWithheld();
    		        				details.setCvApplyWithholdingTaxAmount(WTAX_AMNT);
    	        				}
        						details.setCvApplyAmmountDue(apAppliedVoucher.getApVoucherPaymentSchedule().getVpsAmountDue());
        						TOTAL_NET_AMNT = EJBCommon.roundIt((apAppliedVoucher.getApVoucherPaymentSchedule().getVpsAmountDue() / ((1 + (apTaxCode.getTcRate()/100)) - (apWithholdingTaxCode.getWtcRate()/100))), precisionUnit);
        						System.out.println("TOTAL_NET_AMNT---------------->"+TOTAL_NET_AMNT);
        						
        					} else {
        						
        						WTAX_AMNT = apAppliedVoucher.getAvTaxWithheld();
		        				details.setCvApplyWithholdingTaxAmount(WTAX_AMNT);
		        				
        						TOTAL_NET_AMNT = EJBCommon.roundIt(((apAppliedVoucher.getApVoucherPaymentSchedule().getVpsAmountDue() - WTAX_AMNT)/ ((1 + (apTaxCode.getTcRate()/100)) - (apWithholdingTaxCode.getWtcRate()/100))), precisionUnit);
        						System.out.println("TOTAL_NET_AMNT---------------->"+TOTAL_NET_AMNT);
        					}

    						// distribution record
	        				LocalApDistributionRecord apDistributionRecord = (LocalApDistributionRecord) drIter.next();
	        				
	        				if(apDistributionRecord.getDrClass().equals("PAYABLE")) {
	        					
	        					// payable account
	        					details.setCvVouDrCoaAccountNumber(apDistributionRecord.getGlChartOfAccount().getCoaAccountNumber());
	        					
	        					// get natural account
	        					LocalGenValueSetValue genValueSetValue = 
	        						this.getGenValueSetValue(apDistributionRecord.getGlChartOfAccount().getCoaAccountNumber(), AD_CMPNY);
	        					details.setCvVouDrCoaNaturalDesc(genValueSetValue.getVsvDescription());

	        					// cash account
	        					details.setCvChkDrCoaAccountNumber(
	        							apChkCaskDr.getGlChartOfAccount().getCoaAccountNumber());
	        					details.setCvChkDrCoaAccountDescription(
	        							apChkCaskDr.getGlChartOfAccount().getCoaAccountDescription());
	        					genValueSetValue = this.getGenValueSetValue(
	        							apChkCaskDr.getGlChartOfAccount().getCoaAccountNumber(), AD_CMPNY);
	        					details.setCvChkDrCoaNaturalDesc(genValueSetValue.getVsvDescription());

	        					// ratio
	        					ratio = EJBCommon.roundIt(apDistributionRecord.getDrAmount()/TOTAL_NET_AMNT, precisionUnit);
	        					System.out.println("ration="+ratio);	        						        					
	        				} else {
	        					continue;
	        				}
	        				
	        				details.setCvApplyNetAmount(NET_AMNT * ratio);
	        				
	        				if(apCheck.getApTaxCode()!=null){
	        					details.setCvTaxCode(apCheck.getApTaxCode().getTcName());
	        				}
	        				
	        				if(apCheck.getApWithholdingTaxCode()!=null){
	        					details.setCvWithholdingTaxCode(apCheck.getApWithholdingTaxCode().getWtcName());
	        				}
	        				
	        				asd = apCheck.getChkDocumentNumber();
	        				
	        				first = false;
	        				list.add(details);
	        				
	        			}
	        			
	        		}
	        		
	        	} else if (apCheck.getChkType().equals("DIRECT") && adPreference.getPrfApCheckVoucherDataSource().equals("AP VOUCHER")) {
	        		
	        		// get CASH dr
	        		
	        		Collection apDistributionRecords = apDistributionRecordHome.findDrsByDrClassAndChkCode("CASH", 
	        				apCheck.getChkCode(), AD_CMPNY);

	        		Iterator drIter = apDistributionRecords.iterator();	  

	        		LocalApDistributionRecord apChkCashDr = null;

	        		if (drIter.hasNext()) {

	        			apChkCashDr = (LocalApDistributionRecord) drIter.next();

	        		}	
	        		
	        		// get distribution records
	        		
	        		apDistributionRecords = apDistributionRecordHome.findByChkCode(apCheck.getChkCode(), AD_CMPNY);       	            
	        		
	        		drIter = apDistributionRecords.iterator();	  
	        		
	        		while (drIter.hasNext()) {
	        			
	        			LocalApDistributionRecord apDistributionRecord = (LocalApDistributionRecord)drIter.next();
	        			
	        			if(apDistributionRecord.getDrClass().equals("CASH") || apDistributionRecord.getDrClass().equals("TAX") || 
	        					apDistributionRecord.getDrClass().equals("W-TAX")) continue;
	        				
	        			ApRepCheckVoucherPrintDetails details = new ApRepCheckVoucherPrintDetails();
	        			
	        			if(!asd.equals(apCheck.getChkDocumentNumber()) && asd!="a"){
	        				a++;
	        			}
	        			
	        			details.setCvCheckType(apCheck.getChkType());
		        		details.setCvChkDocumentNumber(apCheck.getChkDocumentNumber());
	        			details.setCvChkReferenceNumber(apCheck.getChkReferenceNumber());
	        			details.setCvChkNumber(apCheck.getChkNumber());
	        			details.setCvChkDate(apCheck.getChkDate());
	        			details.setCvChkCheckDate(apCheck.getChkCheckDate());
	        			details.setCvChkDescription(apCheck.getChkDescription());
	        			details.setCvVouDescription(apCheck.getChkDescription());
	        			details.setCvChkMemo(apCheck.getChkMemo());
	        			details.setCvChkAmount(apCheck.getChkAmount());
	        			details.setCvChkAmount(this.convertForeignToFunctionalCurrency(apCheck.getGlFunctionalCurrency().getFcCode(), apCheck.getGlFunctionalCurrency().getFcName(), apCheck.getChkConversionDate(), apCheck.getChkConversionRate(), apCheck.getChkAmount(), AD_CMPNY));
	        			details.setCvChkCurrencySymbol(apCheck.getGlFunctionalCurrency().getFcSymbol());
	        			details.setCvChkCurrencyDescription(apCheck.getGlFunctionalCurrency().getFcDescription());
	        			details.setCvChkAdBaName(apCheck.getAdBankAccount().getBaName());
	        			details.setCvChkAdBaAccountNumber(apCheck.getAdBankAccount().getBaAccountNumber());
	        			details.setCvChkSplName(apCheck.getChkSupplierName() == null ? apCheck.getApSupplier().getSplName() : apCheck.getChkSupplierName());
        				details.setCvChkSplAddress(apCheck.getApSupplier().getSplAddress());
        				details.setCvChkCreatedBy(apCheck.getChkCreatedBy());
        				details.setCvChkApprovedRejectedBy(apCheck.getChkApprovedRejectedBy());
        				details.setCvChkCheckedBy(adPreference.getPrfApDefaultChecker());
        				details.setCvChkSplCode(apCheck.getApSupplier().getSplSupplierCode());
        				Collection apAppliedVouchers = apCheck.getApAppliedVouchers();
        				Iterator iter = apDistributionRecords.iterator();
        				iter = apAppliedVouchers.iterator();
        				double amountDue = 0d;
        				while(iter.hasNext()) {
        				LocalApAppliedVoucher apAppliedVoucher = (LocalApAppliedVoucher) iter.next();
        				details.setCvVouNumber(apAppliedVoucher.getApVoucherPaymentSchedule().getApVoucher().getVouDocumentNumber());
        				amountDue += apAppliedVoucher.getApVoucherPaymentSchedule().getVpsAmountDue();
        				}
        				details.setCvApplyAmmountDue(amountDue);
        				// log details
        				
	        			details.setCvChkCreatedBy(apCheck.getChkCreatedBy());
	        			/*LocalAdUser adUser = adUserHome.findByUsrName(apCheck.getChkCreatedBy(), AD_CMPNY);
        				details.setCvChkCreatedByDescription(adUser.getUsrDescription());
        				
        				LocalAdUser adUser2 = adUserHome.findByUsrName(adPreference.getPrfApDefaultChecker(), AD_CMPNY);
        				details.setCvChkCheckByDescription(adUser2.getUsrDescription());
        				
        				LocalAdUser adUser3 = adUserHome.findByUsrName(apCheck.getChkApprovedRejectedBy(), AD_CMPNY);
        				details.setCvChkApprovedRejectedByDescription(adUser3.getUsrDescription());
        				*/
        				
        				System.out.println("apCheck.getChkCreatedBy(): "+apCheck.getChkCreatedBy());
        				try{
        					LocalAdUser adUser = adUserHome.findByUsrName(apCheck.getChkCreatedBy(), AD_CMPNY);
            				details.setCvChkCreatedByDescription(adUser.getUsrName());
            				details.setCvChkApprovedRejectedByDescription(adUser.getUsrDescription());
        				}catch(Exception e){
        					details.setCvChkCreatedByDescription("");
        				}

        				try{
        					System.out.println("adPreference.getPrfApDefaultChecker()(): "+adPreference.getPrfApDefaultChecker());
            				LocalAdUser adUser2 = adUserHome.findByUsrName(adPreference.getPrfApDefaultChecker(), AD_CMPNY);
            				details.setCvChkCheckByDescription(adUser2.getUsrDescription());
            				details.setCvChkApprovedRejectedByDescription(adUser2.getUsrDescription());
        				}catch(Exception e){
        					details.setCvChkCheckByDescription("");
        				}

        				try{
        					System.out.println("apCheck.getChkApprovedRejectedBy()1: "+apCheck.getChkApprovedRejectedBy());
            				LocalAdUser adUser3 = adUserHome.findByUsrName(apCheck.getChkApprovedRejectedBy(), AD_CMPNY);
            				details.setCvChkApprovedRejectedByDescription(adUser3.getUsrDescription());
        				}catch(Exception e){
        					details.setCvChkApprovedRejectedByDescription("");
        				}
        				
	        			if(apCheck.getChkApprovedRejectedBy() == null || apCheck.getChkApprovedRejectedBy().equals("")) {
	        				details.setCvChkApprovedRejectedBy(adPreference.getPrfApDefaultApprover());
	        			} else {
	        				details.setCvChkApprovedRejectedBy(apCheck.getChkApprovedRejectedBy());
	        			}
	        			
        				try{
        					System.out.println("3: " + apCheck.getChkPostedBy());
        					LocalAdUser adUser4 = adUserHome.findByUsrName(apCheck.getChkPostedBy(), AD_CMPNY);
            				details.setCvVouPostedBy(adUser4.getUsrDescription());
            				System.out.println("postedBy: " + adUser4.getUsrDescription());
        				}catch(Exception e){
        					details.setCvVouPostedBy("");
        				}
        				
	        			details.setCvChkCheckedBy(adPreference.getPrfApDefaultChecker());
	        			details.setCvChkDateCreated(apCheck.getChkDateCreated());
	        			details.setCvChkDateApprovedRejected(apCheck.getChkDateApprovedRejected());
	        			details.setCvApprovalStatus(apCheck.getChkApprovalStatus());
	        			details.setCvPosted(apCheck.getChkPosted());
	        			
	        			details.setCvChkDrDebit(apDistributionRecord.getDrDebit());
	        			details.setCvChkDrAmount(apDistributionRecord.getDrAmount());		        	
	        			details.setCvApplyNetAmount(apDistributionRecord.getDrDebit() == EJBCommon.TRUE ? apDistributionRecord.getDrAmount() : (apDistributionRecord.getDrAmount() * -1));
	        			
    					// expense account
    					details.setCvVouDrCoaAccountNumber(apDistributionRecord.getGlChartOfAccount().getCoaAccountNumber());
    					LocalGenValueSetValue genValueSetValue = this.getGenValueSetValue(
    							apDistributionRecord.getGlChartOfAccount().getCoaAccountNumber(), AD_CMPNY);
    					details.setCvVouDrCoaNaturalDesc(genValueSetValue.getVsvDescription());

    					// cash account
    					details.setCvChkDrCoaAccountNumber(apChkCashDr.getGlChartOfAccount().getCoaAccountNumber());
    					genValueSetValue = this.getGenValueSetValue(
    							apChkCashDr.getGlChartOfAccount().getCoaAccountNumber(), AD_CMPNY);
    					details.setCvChkDrCoaNaturalDesc(genValueSetValue.getVsvDescription());
	        			
	        			details.setCvShowDuplicate(showDuplicate ? EJBCommon.TRUE : EJBCommon.FALSE);
	        			details.setCvChkCrossCheck(apCheck.getChkCrossCheck());
	        			details.setCvChkSplCode(apCheck.getApSupplier().getSplSupplierCode());
	        			asd = apCheck.getChkDocumentNumber();
	        			
	        			if(apCheck.getApTaxCode()!=null){
        					details.setCvTaxCode(apCheck.getApTaxCode().getTcName());
        				}
	        			if(apCheck.getApWithholdingTaxCode()!=null){
        					details.setCvWithholdingTaxCode(apCheck.getApWithholdingTaxCode().getWtcName());
        				}
	        			list.add(details);
	        			
	        		}
	        		
	        	} else {
	        		System.out.println("ELSE----------------->");
	        		
	        		// get distribution records
	        		
	        		Collection apDistributionRecords = apDistributionRecordHome.findByChkCode(apCheck.getChkCode(), AD_CMPNY);       	            
	        		
	        		Iterator drIter = apDistributionRecords.iterator();	        		        	
	        		
	        		
	        		while (drIter.hasNext()) {
	        			
	        			LocalApDistributionRecord apDistributionRecord = (LocalApDistributionRecord)drIter.next();
	        			
	        			ApRepCheckVoucherPrintDetails details = new ApRepCheckVoucherPrintDetails();
	        			
	        			if(!asd.equals(apCheck.getChkDocumentNumber()) && asd!="a"){
	        				a++;
	        			}
	        			
	        			details.setCvChkDocumentNumber(apCheck.getChkDocumentNumber());
	        			details.setCvChkNumber(apCheck.getChkNumber());
	        			details.setCvChkDate(apCheck.getChkDate());
	        			details.setCvChkCheckDate(apCheck.getChkCheckDate());
	        			details.setCvChkDescription(apCheck.getChkDescription());
	        			details.setCvChkAmount(apCheck.getChkAmount());
	        			details.setCvChkAmount(this.convertForeignToFunctionalCurrency(apCheck.getGlFunctionalCurrency().getFcCode(), apCheck.getGlFunctionalCurrency().getFcName(), apCheck.getChkConversionDate(), apCheck.getChkConversionRate(), apCheck.getChkAmount(), AD_CMPNY));
	        			details.setCvChkCreatedBy(apCheck.getChkCreatedBy());
	        			if(apCheck.getChkApprovedRejectedBy() == null || apCheck.getChkApprovedRejectedBy().equals("")) {
	        				
	        				details.setCvChkApprovedRejectedBy(adPreference.getPrfApDefaultApprover());
	        				
	        			} else {
	        				
	        				details.setCvChkApprovedRejectedBy(apCheck.getChkApprovedRejectedBy());
	        				
	        			}
	        			
	        			details.setCvChkCheckedBy(adPreference.getPrfApDefaultChecker());
	        			// supplier name
        				details.setCvChkSplName(apCheck.getChkSupplierName() == null ? apCheck.getApSupplier().getSplName() : apCheck.getChkSupplierName());
    					details.setCvChkCurrencySymbol(apCheck.getGlFunctionalCurrency().getFcSymbol());
	        			details.setCvChkCurrencyDescription(apCheck.getGlFunctionalCurrency().getFcDescription());
	        			details.setCvChkDrDebit(apDistributionRecord.getDrDebit());
	        			details.setCvChkDrAmount(apDistributionRecord.getDrAmount());		        	
	        			details.setCvChkAdBaName(apCheck.getAdBankAccount().getBaName());
	        			details.setCvChkAdBaAccountNumber(apCheck.getAdBankAccount().getBaAccountNumber());
	        			details.setCvShowDuplicate(showDuplicate ? EJBCommon.TRUE : EJBCommon.FALSE);
	        			details.setCvApprovalStatus(apCheck.getChkApprovalStatus());
	        			details.setCvPosted(apCheck.getChkPosted());
	        			details.setCvChkDrCoaAccountNumber(apDistributionRecord.getGlChartOfAccount().getCoaAccountNumber());
	        			details.setCvChkDrCoaAccountDescription(apDistributionRecord.getGlChartOfAccount().getCoaAccountDescription());			    
	        			details.setCvChkCrossCheck(apCheck.getChkCrossCheck());
	        			details.setCvChkDateCreated(apCheck.getChkDateCreated());
	        			details.setCvChkDateApprovedRejected(apCheck.getChkDateApprovedRejected());
	        			details.setCvChkReferenceNumber(apCheck.getChkReferenceNumber());
	        			details.setCvChkSplAddress(apCheck.getApSupplier().getSplAddress());
	        			details.setCvChkMemo(apCheck.getChkMemo());
	        			details.setCvChkCreatedBy(apCheck.getChkCreatedBy());
	        			details.setCvChkApprovedRejectedBy(apCheck.getChkApprovedRejectedBy());
	        			details.setCvChkSplCode(apCheck.getApSupplier().getSplSupplierCode());
        				Collection apAppliedVouchers = apCheck.getApAppliedVouchers();
        				Iterator iter = apDistributionRecords.iterator();
        				iter = apAppliedVouchers.iterator();
        				double amountDue = 0d;
        				while(iter.hasNext()) {
        				LocalApAppliedVoucher apAppliedVoucher = (LocalApAppliedVoucher) iter.next();
        				details.setCvVouNumber(apAppliedVoucher.getApVoucherPaymentSchedule().getApVoucher().getVouDocumentNumber());
        				amountDue += apAppliedVoucher.getApVoucherPaymentSchedule().getVpsAmountDue();
        				}
        				System.out.println("amountdue="+amountDue);
        				details.setCvApplyAmmountDue(amountDue);
	        			// set check type
	        			details.setCvCheckType(apCheck.getChkType());
	        			
//            			Include Branch
        				LocalAdBranch adBranch = adBranchHome.findByPrimaryKey(apCheck.getChkAdBranch());
        				details.setCvChkBranchCode(adBranch.getBrBranchCode());
        				details.setCvChkBranchName(adBranch.getBrName());
	        			
        				// get user name description
        				
        				System.out.println("apCheck.getChkCreatedBy(): "+apCheck.getChkCreatedBy());
        				try{
        					LocalAdUser adUser = adUserHome.findByUsrName(apCheck.getChkCreatedBy(), AD_CMPNY);
            				details.setCvChkCreatedByDescription(adUser.getUsrName());
            				details.setCvChkApprovedRejectedByDescription(adUser.getUsrDescription());
        				}catch(Exception e){
        					details.setCvChkCreatedByDescription("");
        				}

        				try{
        					System.out.println("adPreference.getPrfApDefaultChecker()(): "+adPreference.getPrfApDefaultChecker());
            				LocalAdUser adUser2 = adUserHome.findByUsrName(adPreference.getPrfApDefaultChecker(), AD_CMPNY);
            				details.setCvChkCheckByDescription(adUser2.getUsrDescription());
            				details.setCvChkApprovedRejectedByDescription(adUser2.getUsrDescription());
        				}catch(Exception e){
        					details.setCvChkCheckByDescription("");
        				}

        				try{
        					System.out.println("apCheck.getChkApprovedRejectedBy(): "+apCheck.getChkApprovedRejectedBy());
            				LocalAdUser adUser3 = adUserHome.findByUsrName(apCheck.getChkApprovedRejectedBy(), AD_CMPNY);
            				details.setCvChkApprovedRejectedByDescription(adUser3.getUsrDescription());
        				}catch(Exception e){
        					details.setCvChkApprovedRejectedByDescription("");
        				}
        				
        				try{
        					System.out.println("4: " + apCheck.getChkPostedBy());
        					LocalAdUser adUser4 = adUserHome.findByUsrName(apCheck.getChkPostedBy(), AD_CMPNY);
            				details.setCvVouPostedBy(adUser4.getUsrDescription());
            				System.out.println("postedBy: " + adUser4.getUsrDescription());
        				}catch(Exception e){
        					details.setCvVouPostedBy("");
        				}
        				
        				asd = apCheck.getChkDocumentNumber();
        				
        				if(apCheck.getApTaxCode()!=null){
        					details.setCvTaxCode(apCheck.getApTaxCode().getTcName());
        				}
        				if(apCheck.getApWithholdingTaxCode()!=null){
        					details.setCvWithholdingTaxCode(apCheck.getApWithholdingTaxCode().getWtcName());
        				}
	        			list.add(details);
	        			
	        		}
	        		
	        	} 
	        	
        	}
        	
        	if (list.isEmpty()) {
        		
        		throw new GlobalNoRecordFoundException();
        		
	        }
 	          
        	if(adCompanyHome.findByPrimaryKey(AD_CMPNY).getCmpShortName().equals("DNATA")){
        		//if(a==1){
        			try{
        		   		Collections.sort(list, ApRepCheckVoucherPrintDetails.sortByAccount);
        		   	  }catch(Exception e){
        		   		  
        		   	  }
        		//}
        		
        		
        	}
		   	  
		   	  
        	return list;
        	
        } catch (GlobalNoRecordFoundException ex) {
        	
        	throw ex;
        	        	
        } catch (GenVSVNoValueSetValueFoundException ex){
        	
        	throw ex;
        	        	
        } catch (Exception ex) {
        	
        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());
        	
        }
            
    }
    
    /**
     * @ejb:interface-method view-type="remote"
     **/
    public ArrayList executeApRepCheckVoucherPrintSub(ArrayList chkCodeList, Integer AD_CMPNY) 
        throws GlobalNoRecordFoundException {
                    
        Debug.print("ApRepCheckVoucherPrintControllerBean executeApRepCheckVoucherPrintSub");
        
        LocalApCheckHome apCheckHome = null;
        LocalAdApprovalHome adApprovalHome = null;
        LocalAdApprovalDocumentHome adApprovalDocumentHome = null;
        LocalApAppliedVoucherHome apAppliedVoucherHome = null;
        
        ArrayList list = new ArrayList();
                
        // Initialize EJB Home
        
        try {
            
            apCheckHome = (LocalApCheckHome)EJBHomeFactory.
                lookUpLocalHome(LocalApCheckHome.JNDI_NAME, LocalApCheckHome.class);
	        adApprovalHome = (LocalAdApprovalHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdApprovalHome.JNDI_NAME, LocalAdApprovalHome.class);
            adApprovalDocumentHome = (LocalAdApprovalDocumentHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdApprovalDocumentHome.JNDI_NAME, LocalAdApprovalDocumentHome.class);
            apAppliedVoucherHome = (LocalApAppliedVoucherHome)EJBHomeFactory.
			    lookUpLocalHome(LocalApAppliedVoucherHome.JNDI_NAME, LocalApAppliedVoucherHome.class);
               
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }        
        
        try {
        	
        	Iterator i = chkCodeList.iterator();
        	
        	while (i.hasNext()) {
        	
        	    Integer CHK_CODE = (Integer) i.next();
        	
	        	LocalApCheck apCheck = null;
	        	
	        	
	        	try {
	        		
	        		apCheck = apCheckHome.findByPrimaryKey(CHK_CODE);
	        		
	        	} catch (FinderException ex) {
	        		
	        		continue;
	        		
	        	}
	        	
	        	LocalAdApproval adApproval = adApprovalHome.findByAprAdCompany(AD_CMPNY);
	        	LocalAdApprovalDocument adApprovalDocument = adApprovalDocumentHome.findByAdcType("AP CHECK", AD_CMPNY);
	        	
	        	if (adApprovalDocument.getAdcPrintOption().equals("PRINT APPROVED ONLY")) {
	        	
	        		if (apCheck.getChkApprovalStatus() == null || 
	        			apCheck.getChkApprovalStatus().equals("PENDING")) {
	        		    
	        		    continue;
	        		    
	        		} 
	        	
	        	
	        	} else if (adApprovalDocument.getAdcPrintOption().equals("PRINT UNAPPROVED ONLY")) {
	        	
	        		if (apCheck.getChkApprovalStatus() != null && 
	        			(apCheck.getChkApprovalStatus().equals("N/A") || 
	        			 apCheck.getChkApprovalStatus().equals("APPROVED"))) {
	        		
	        			continue;
	        		
	        		}
	        	
	        	}
	        	
	        	if (adApprovalDocument.getAdcAllowDuplicate() == EJBCommon.FALSE && 
	        		apCheck.getChkCvPrinted() == EJBCommon.TRUE){
	        		
	        		continue;	
	        		
	        	}
	        	
	        	Collection apAppliedVouchers = null;
	        	
	        	try {
	        		
	        		apAppliedVouchers = apAppliedVoucherHome.findByChkCode(CHK_CODE, AD_CMPNY);
	        		
	        	} catch(FinderException ex) {
	        		
	        	}
	        		        		
	        	String tempVoucher = null;
	        	
	        	Iterator apAppVouIter = apAppliedVouchers.iterator();
	        	
	        	while(apAppVouIter.hasNext()) {
	        		
	        		LocalApAppliedVoucher apAppliedVoucher = (LocalApAppliedVoucher) apAppVouIter.next();
	        		
	        		ApRepCheckVoucherPrintDetails details = new ApRepCheckVoucherPrintDetails();
	        		
	        		details.setCvChkDocumentNumber(apCheck.getChkDocumentNumber());
	        		
	        		Collection apPurchaseOrderLines = apAppliedVoucher.getApVoucherPaymentSchedule().getApVoucher().getApPurchaseOrderLines();
	        		if(apPurchaseOrderLines.size() != 0 && apPurchaseOrderLines != null) {
	        			
	        			details.setCvPoDocumentNumber(apAppliedVoucher.getApVoucherPaymentSchedule().getApVoucher().getVouPoNumber());
	        			details.setCvPoDate(apAppliedVoucher.getApVoucherPaymentSchedule().getApVoucher().getVouDate());
	        			
	        		}
	        		System.out.println("vouNumber3: " + apAppliedVoucher.getApVoucherPaymentSchedule().getApVoucher().getVouDocumentNumber());
	        		details.setCvVouNumber(apAppliedVoucher.getApVoucherPaymentSchedule().getApVoucher().getVouDocumentNumber());
	        		details.setCvVouDate(apAppliedVoucher.getApVoucherPaymentSchedule().getApVoucher().getVouDate());
        			
	        		details.setCvChkAmount(apAppliedVoucher.getAvApplyAmount());
	        		details.setCvVouReferenceNumber(apAppliedVoucher.getApVoucherPaymentSchedule().getApVoucher().getVouReferenceNumber());
	        		
	        		if(tempVoucher == null || !tempVoucher.equals(apAppliedVoucher.getApVoucherPaymentSchedule().getApVoucher().getVouDocumentNumber())) {
	        			
	        			tempVoucher = apAppliedVoucher.getApVoucherPaymentSchedule().getApVoucher().getVouDocumentNumber();
	        			
	        			double WTAX_AMNT = 0;
	        			double NET_AMNT =0;
	        			
	        			// set withholding tax amount, net amount & gross amount
	        			Collection apDistributionRecords = apAppliedVoucher.getApVoucherPaymentSchedule().getApVoucher().getApDistributionRecords();       	            

	        			Iterator drIter = apDistributionRecords.iterator();	

	        			while(drIter.hasNext()) {

	        				LocalApDistributionRecord apDistributionRecord = (LocalApDistributionRecord)drIter.next();

	        				if(apDistributionRecord.getDrClass().equals("W-TAX")) {
	        					WTAX_AMNT += apDistributionRecord.getDrAmount();
	        				} else if(apDistributionRecord.getDrClass().equals("PAYABLE")) {
	        					NET_AMNT += apDistributionRecord.getDrAmount();
	        				}
	        				
	        			}

	        			details.setCvVouGrossAmount(WTAX_AMNT + NET_AMNT);
	        			details.setCvVouWithholdingTaxAmount(WTAX_AMNT);
	        			details.setCvVouNetAmount(NET_AMNT);
	        			
	        		}
	        		
	        		if(apCheck.getApTaxCode()!=null){
    					details.setCvTaxCode(apCheck.getApTaxCode().getTcName());
    				}
	        		if(apCheck.getApWithholdingTaxCode()!=null){
    					details.setCvWithholdingTaxCode(apCheck.getApWithholdingTaxCode().getWtcName());
    				}
	        		list.add(details);
	        		
	        	}
	        	
	        }
        	
	        if (list.isEmpty()) {
	        	
	        	throw new GlobalNoRecordFoundException();
	        	
	        }  
        	
        	return list;
        	
        } catch (GlobalNoRecordFoundException ex) {
        	
        	throw ex;
        	        	
        } catch (Exception ex) {
        	
        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());
        	
        }
            
    }
    
	/**
	* @ejb:interface-method view-type="remote"
	* @jboss:method-attributes read-only="true"
	**/
	public com.util.AdCompanyDetails getAdCompany(Integer AD_CMPNY) {
	
	  Debug.print("ApRepCheckVoucherPrintControllerBean getAdCompany");      
	  
	  LocalAdCompanyHome adCompanyHome = null;
	        
	  // Initialize EJB Home
	    
	  try {
	        
	       adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
	           lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
	        
	  } catch (NamingException ex) {
	        
	      throw new EJBException(ex.getMessage());
	        
	  }
	
	  try {
	      
	     LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
	     
	     AdCompanyDetails details = new AdCompanyDetails();
	     details.setCmpName(adCompany.getCmpName());
	     details.setCmpAddress(adCompany.getCmpAddress());
	     
	     return details;  	
	  	       	
	  } catch (Exception ex) {
	  	
	  	 Debug.printStackTrace(ex);
	  	 throw new EJBException(ex.getMessage());
	  	  
	  }
	  
	}    

	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public short getGlFcPrecisionUnit(Integer AD_CMPNY) {
		
		Debug.print("ApRepCheckVoucherPrintControllerBean getGlFcPrecisionUnit");
		
		
		LocalAdCompanyHome adCompanyHome = null;
		
		
		// Initialize EJB Home
		
		try {
			
			adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);       
			
		} catch (NamingException ex) {
			
			throw new EJBException(ex.getMessage());
			
		}
		
		try {
			
			LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
			
			return  adCompany.getGlFunctionalCurrency().getFcPrecision();
			
		} catch (Exception ex) {
			
			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());
			
		}
		
	}
	
	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public String getAdPrfApCheckVoucherDataSource(Integer AD_CMPNY) {
		
		Debug.print("ApRepCheckVoucherPrintControllerBean getAdPrfApCheckVoucherDataSource");
		
		LocalAdPreferenceHome adPreferenceHome = null;
		
		try {
			
			adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);       
			
		} catch (NamingException ex) {
			
			throw new EJBException(ex.getMessage());
			
		}
		
		try {
			
			LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);
			
			return  adPreference.getPrfApCheckVoucherDataSource();
			
		} catch (Exception ex) {
			
			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());
			
		}
		
	
	}
	
	private LocalGenValueSetValue getGenValueSetValue(String COA, Integer AD_CMPNY) 
	throws GlobalNoRecordFoundException,
	GenVSVNoValueSetValueFoundException {
		
		Debug.print("ApRepVoucherPrintControllerBean getGenValueSetValue");
		
		LocalAdCompanyHome adCompanyHome = null;
		LocalGenSegmentHome genSegmentHome = null;
		LocalGenValueSetValueHome genValueSetValueHome = null;
		
		LocalAdCompany adCompany = null;
		LocalGenValueSetValue genValueSetValue = null;
		
		try {

			adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
			genSegmentHome = (LocalGenSegmentHome)EJBHomeFactory.
				lookUpLocalHome(LocalGenSegmentHome.JNDI_NAME, LocalGenSegmentHome.class); 
			genValueSetValueHome = (LocalGenValueSetValueHome)EJBHomeFactory.
				lookUpLocalHome(LocalGenValueSetValueHome.JNDI_NAME, LocalGenValueSetValueHome.class);
		
		} catch (NamingException ex) {
			
			throw new EJBException(ex.getMessage());
			
		}
		
		try {
			
			adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
			
		} catch (Exception ex) {
			
			throw new EJBException(ex.getMessage());
			
		}
		
		try {
			
			// get coa separator
			LocalGenField genField = adCompany.getGenField();
			char chrSeparator = genField.getFlSegmentSeparator();
			String strSeparator =  String.valueOf(chrSeparator);      

			// get natural account segment
			Collection genSegments = genSegmentHome.findByFlCode(genField.getFlCode(), AD_CMPNY);
			Iterator i = genSegments.iterator();
			LocalGenValueSet genValueSet = null;
			while(i.hasNext()){
				
				LocalGenSegment genSegment = (LocalGenSegment)i.next();
				if(genSegment.getSgSegmentType() == 'N') {
					genValueSet = genSegment.getGenValueSet();
					break;
				}
					
			}
			
			// get value set value
			StringTokenizer st = new StringTokenizer(COA, strSeparator);
			while (st.hasMoreTokens()) {
				
				try {
					genValueSetValue = genValueSetValueHome.findByVsCodeAndVsvValue(genValueSet.getVsCode(), st.nextToken(), AD_CMPNY);
				} catch (Exception ex) {

				}
				
				if(genValueSetValue != null)
					break;
				
			}  
			
			if(genValueSetValue == null)
				throw new GenVSVNoValueSetValueFoundException();

			return  genValueSetValue;
			
		} catch (Exception ex) {
			
			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());
			
		}
		
	}
	
	 private double convertForeignToFunctionalCurrency(Integer FC_CODE, String FC_NM,
			    Date CONVERSION_DATE, double CONVERSION_RATE, double AMOUNT, Integer AD_CMPNY) {

			    Debug.print("ApRepCheckVoucherPrintControllerBean convertForeignToFunctionalCurrency");


		        LocalGlFunctionalCurrencyRateHome glFunctionalCurrencyRateHome = null;
		        LocalAdCompanyHome adCompanyHome = null;

		        LocalAdCompany adCompany = null;

		        // Initialize EJB Homes

		        try {

		            glFunctionalCurrencyRateHome = (LocalGlFunctionalCurrencyRateHome)EJBHomeFactory.
		               lookUpLocalHome(LocalGlFunctionalCurrencyRateHome.JNDI_NAME, LocalGlFunctionalCurrencyRateHome.class);
		            adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
		               lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);

		         } catch (NamingException ex) {

		            throw new EJBException(ex.getMessage());

		         }

		         // get company and extended precision

		         try {

		             adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);

		         } catch (Exception ex) {

		             throw new EJBException(ex.getMessage());

		         }


		         // Convert to functional currency if necessary

		         if (CONVERSION_RATE != 1 && CONVERSION_RATE != 0 ) {

		             AMOUNT = AMOUNT / CONVERSION_RATE;

		         }

		         return EJBCommon.roundIt(AMOUNT, adCompany.getGlFunctionalCurrency().getFcPrecision());

			}


    // SessionBean methods

    /**
     * @ejb:create-method view-type="remote"
     **/
    public void ejbCreate() throws CreateException {

       Debug.print("ApRepCheckVoucherPrintControllerBean ejbCreate");
      
    }
}
