
/*
 * ApRepDebitMemoPrintControllerBean.java
 *
 * Created on February 24, 2004, 1:57 PM
 *
 * @author  Neil Andrew M. Ajero
 */

package com.ejb.txn;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;
import javax.naming.NamingException;

import com.ejb.ad.LocalAdApproval;
import com.ejb.ad.LocalAdApprovalDocument;
import com.ejb.ad.LocalAdApprovalDocumentHome;
import com.ejb.ad.LocalAdApprovalHome;
import com.ejb.ad.LocalAdCompany;
import com.ejb.ad.LocalAdCompanyHome;
import com.ejb.ad.LocalAdPreference;
import com.ejb.ad.LocalAdPreferenceHome;
import com.ejb.ad.LocalAdUser;
import com.ejb.ad.LocalAdUserHome;
import com.ejb.ap.LocalApDistributionRecord;
import com.ejb.ap.LocalApDistributionRecordHome;
import com.ejb.ap.LocalApVoucher;
import com.ejb.ap.LocalApVoucherHome;
import com.ejb.exception.GlobalNoRecordFoundException;
import com.util.AbstractSessionBean;
import com.util.AdCompanyDetails;
import com.util.ApRepDebitMemoPrintDetails;
import com.util.Debug;
import com.util.EJBCommon;
import com.util.EJBHomeFactory;

/**
 * @ejb:bean name="ApRepDebitMemoPrintControllerEJB"
 *           display-name="Used for printing debit memo transactions"
 *           type="Stateless"
 *           view-type="remote"
 *           jndi-name="ejb/ApRepDebitMemoPrintControllerEJB"
 *
 * @ejb:interface remote-class="com.ejb.txn.ApRepDebitMemoPrintController"
 *                extends="javax.ejb.EJBObject"
 *
 * @ejb:home remote-class="com.ejb.txn.ApRepDebitMemoPrintControllerHome"
 *           extends="javax.ejb.EJBHome"
 *
 * @ejb:transaction type="Required"
 * 
*/

public class ApRepDebitMemoPrintControllerBean extends AbstractSessionBean {
    
    
    /**
     * @ejb:interface-method view-type="remote"
     **/
    public ArrayList executeApRepDebitMemoPrint(ArrayList vouCodeList, Integer AD_CMPNY) 
        throws GlobalNoRecordFoundException {
                    
        Debug.print("ApRepDebitMemoPrintControllerBean executeApRepDebitMemoPrint");
        
        LocalApVoucherHome apVoucherHome = null;        
        LocalAdApprovalHome adApprovalHome = null;
        LocalAdApprovalDocumentHome adApprovalDocumentHome = null;
        LocalApDistributionRecordHome apDistributionRecordHome = null;
        LocalAdPreferenceHome adPreferenceHome = null;
        LocalAdUserHome adUserHome = null;
        
        ArrayList list = new ArrayList();
                
        // Initialize EJB Home
        
        try {
            
            apVoucherHome = (LocalApVoucherHome)EJBHomeFactory.
                lookUpLocalHome(LocalApVoucherHome.JNDI_NAME, LocalApVoucherHome.class);
            adApprovalHome = (LocalAdApprovalHome)EJBHomeFactory.
              lookUpLocalHome(LocalAdApprovalHome.JNDI_NAME, LocalAdApprovalHome.class);
            adApprovalDocumentHome = (LocalAdApprovalDocumentHome)EJBHomeFactory.
              lookUpLocalHome(LocalAdApprovalDocumentHome.JNDI_NAME, LocalAdApprovalDocumentHome.class);
            apDistributionRecordHome = (LocalApDistributionRecordHome)EJBHomeFactory.
		      lookUpLocalHome(LocalApDistributionRecordHome.JNDI_NAME, LocalApDistributionRecordHome.class);
            adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
			  lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);
            adUserHome = (LocalAdUserHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdUserHome.JNDI_NAME, LocalAdUserHome.class);
            
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }        
        
        try {
        	
        	Iterator i = vouCodeList.iterator();
        	
        	while (i.hasNext()) {
        	
        	    Integer VOU_CODE = (Integer) i.next();
        	
	        	LocalApVoucher apDebitMemo = null;
	        	
	        	
	        	try {
	        		
	        		apDebitMemo = apVoucherHome.findByPrimaryKey(VOU_CODE);
	        		
	        	} catch (FinderException ex) {
	        		
	        		continue;
	        		
	        	}
	        	
	        	LocalAdApproval adApproval = adApprovalHome.findByAprAdCompany(AD_CMPNY);
  	      	    LocalAdApprovalDocument adApprovalDocument = adApprovalDocumentHome.findByAdcType("AP DEBIT MEMO", AD_CMPNY);
  	      	  	  	      	    
  	      	    if (adApprovalDocument.getAdcPrintOption().equals("PRINT APPROVED ONLY")) {
	        	
	        		if (apDebitMemo.getVouApprovalStatus() == null || 
	        			apDebitMemo.getVouApprovalStatus().equals("PENDING")) {
	        		    
	        		    continue;
	        		    
	        		} 
	        	
	        	
	        	} else if (adApprovalDocument.getAdcPrintOption().equals("PRINT UNAPPROVED ONLY")) {
	        	
	        		if (apDebitMemo.getVouApprovalStatus() != null && 
	        			(apDebitMemo.getVouApprovalStatus().equals("N/A") || 
	        			 apDebitMemo.getVouApprovalStatus().equals("APPROVED"))) {
	        		
	        			continue;
	        		
	        		}
	        	
	        	}
	        	
	        	if (adApprovalDocument.getAdcAllowDuplicate() == EJBCommon.FALSE && 
	        		apDebitMemo.getVouPrinted() == EJBCommon.TRUE){
	        		
	        		continue;	
	        		
	        	}
	        		        		        	
	        	// show duplicate
	        	
	        	boolean showDuplicate = false;
	        	
	        	if (adApprovalDocument.getAdcTrackDuplicate() == EJBCommon.TRUE &&
	        		apDebitMemo.getVouPrinted() == EJBCommon.TRUE) {
	        		
	        		showDuplicate = true;
	        		
	        	}
	        	    
	        	// set printed
	        	
	        	apDebitMemo.setVouPrinted(EJBCommon.TRUE);
	        		        	
	        	// get distribution records
	        	
	        	Collection apDistributionRecords = apDistributionRecordHome.findByVouCode(apDebitMemo.getVouCode(), AD_CMPNY);        	            
	        	
	        	Iterator drIter = apDistributionRecords.iterator();
	        	LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);
	        	
	        	while (drIter.hasNext()) {
	        		
	        		LocalApDistributionRecord apDistributionRecord = (LocalApDistributionRecord)drIter.next();
	        		
	        		ApRepDebitMemoPrintDetails details = new ApRepDebitMemoPrintDetails();
	        		
	        		details.setDpVouSplSupplierCode(apDebitMemo.getApSupplier().getSplSupplierCode());
	        		details.setDpVouDate(apDebitMemo.getVouDate());
	        		details.setDpVouSplAddress(apDebitMemo.getApSupplier().getSplAddress());
	        		details.setDpVouDocumentNumber(apDebitMemo.getVouDocumentNumber());
	        		details.setDpVouSplTin(apDebitMemo.getApSupplier().getSplTin());
	        		details.setDpVouDmVoucherNumber(apDebitMemo.getVouDmVoucherNumber());
	        		
	        		try{
	        			LocalApVoucher apLocalApVoucher = apVoucherHome.findByVouDocumentNumber(apDebitMemo.getVouDmVoucherNumber(), AD_CMPNY);
	        			System.out.println("getVouReferenceNumber"+apLocalApVoucher.getVouReferenceNumber());	
	        			details.setDpVouDrNumber(apLocalApVoucher.getVouReferenceNumber());
	        		}catch (Exception e) {
						// TODO: handle exception
	        			details.setDpVouDrNumber("");
					}
	        		
	        		
	        		details.setDpVouDescription(apDebitMemo.getVouDescription());
	        		details.setDpVouBillAmount(apDebitMemo.getVouBillAmount());
	        		details.setDpVouCreatedBy(apDebitMemo.getVouCreatedBy());
	        		details.setDpVouApprovedBy(apDebitMemo.getVouApprovedRejectedBy());
	        		details.setDpDrCoaAccountNumber(apDistributionRecord.getGlChartOfAccount().getCoaAccountNumber());
	        		details.setDpDrCoaAccountDescription(apDistributionRecord.getGlChartOfAccount().getCoaAccountDescription());	
	        		details.setDpDrDebit(apDistributionRecord.getDrDebit());
	        		details.setDpDrAmount(apDistributionRecord.getDrAmount());
	        		details.setDpShowDuplicate(showDuplicate ? EJBCommon.TRUE : EJBCommon.FALSE);
	        		details.setDpVouApprovalStatus(apDebitMemo.getVouApprovalStatus());
	        		
	        		// get user name description
    				
    				System.out.println("apVoucher.getChkCreatedBy(): "+apDebitMemo.getVouCreatedBy());
    				try{
    					LocalAdUser adUser = adUserHome.findByUsrName(apDebitMemo.getVouCreatedBy(), AD_CMPNY);
        				details.setDpVouCreatedByDescription(adUser.getUsrDescription());
    				}catch(Exception e){
    					details.setDpVouCreatedByDescription("");
    				}

    				System.out.println("apVoucher.getChkCreatedBy(): "+details.getDpVouCreatedByDescription());
    				
    				/*try{
    					System.out.println("adPreference.getPrfApDefaultChecker(): "+adPreference.getPrfApDefaultChecker());
        				LocalAdUser adUser2 = adUserHome.findByUsrName(adPreference.getPrfApDefaultChecker(), AD_CMPNY);
        				details.setDpVouCheckByDescription(adUser2.getUsrDescription());
    				}catch(Exception e){
    					details.setDpVouCheckByDescription("");
    				}*/

    				try{
    					System.out.println("apVoucher.getVouApprovedRejectedBy(): "+apDebitMemo.getVouApprovedRejectedBy());
        				LocalAdUser adUser3 = adUserHome.findByUsrName(apDebitMemo.getVouApprovedRejectedBy(), AD_CMPNY);
        				details.setDpVouApprovedRejectedByDescription(adUser3.getUsrDescription());
        				
    				}catch(Exception e){
    					details.setDpVouApprovedRejectedByDescription("");
    				}
    				System.out.println("DpVouApprovedRejectedByDescription(): "+details.getDpVouApprovedRejectedByDescription());
	        		list.add(details);
	        		
	        	}
	        	
	        }
	        
	        if (list.isEmpty()) {
	        	
	        	throw new GlobalNoRecordFoundException();
	        	
	        }        	       	
        	
        	return list;
	        	
        	
        } catch (GlobalNoRecordFoundException ex) {
        	
        	throw ex;
        	        	
        } catch (Exception ex) {
        	
        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());
        	
        }
            
    }
    
	/**
	* @ejb:interface-method view-type="remote"
	* @jboss:method-attributes read-only="true"
	**/
	public com.util.AdCompanyDetails getAdCompany(Integer AD_CMPNY) {
	
	  Debug.print("ApRepVoucherPrintControllerBean getAdCompany");      
	  
	  LocalAdCompanyHome adCompanyHome = null;
	        
	  // Initialize EJB Home
	    
	  try {
	        
	       adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
	           lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
	        
	  } catch (NamingException ex) {
	        
	      throw new EJBException(ex.getMessage());
	        
	  }
	
	  try {
	      
	     LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
	     
	     AdCompanyDetails details = new AdCompanyDetails();
	     details.setCmpName(adCompany.getCmpName());
	     details.setCmpAddress(adCompany.getCmpAddress());
	     details.setCmpCity(adCompany.getCmpCity());
	     details.setCmpCountry(adCompany.getCmpCountry());
	     details.setCmpPhone(adCompany.getCmpPhone());
	     
	     return details;  	
	  	       	
	  } catch (Exception ex) {
	  	
	  	 Debug.printStackTrace(ex);
	  	 throw new EJBException(ex.getMessage());
	  	  
	  }
	  
	}    

    // SessionBean methods

    /**
     * @ejb:create-method view-type="remote"
     **/
    public void ejbCreate() throws CreateException {

       Debug.print("ApRepVoucherPrintControllerBean ejbCreate");
      
    }
}
