package com.ejb.txn;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;
import javax.naming.NamingException;

import com.ejb.ad.LocalAdBranch;
import com.ejb.ad.LocalAdBranchHome;
import com.ejb.ad.LocalAdBranchResponsibility;
import com.ejb.ad.LocalAdBranchResponsibilityHome;
import com.ejb.ad.LocalAdCompany;
import com.ejb.ad.LocalAdCompanyHome;
import com.ejb.ad.LocalAdBranch;
import com.ejb.ad.LocalAdBranchHome;
import com.ejb.ad.LocalAdLookUpValue;
import com.ejb.ad.LocalAdLookUpValueHome;
import com.ejb.ad.LocalAdPreference;
import com.ejb.ad.LocalAdPreferenceHome;
import com.ejb.ap.LocalApPurchaseOrderLine;
import com.ejb.ap.LocalApPurchaseOrderLineHome;
import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.inv.LocalInvLocation;
import com.ejb.inv.LocalInvLocationHome;
import com.util.AbstractSessionBean;
import com.util.AdBranchDetails;
import com.util.AdCompanyDetails;
import com.util.ApRepCheckRegisterDetails;
import com.util.ApRepPurchaseOrderDetails;
import com.util.Debug;
import com.util.EJBHomeFactory;

/**
* @ejb:bean name="ApRepPurchaseOrderControllerEJB"
*           display-name="Used for generation of purchase order reports"
*           type="Stateless"
*           view-type="remote"
*           jndi-name="ejb/ApRepPurchaseOrderControllerEJB"
*
* @ejb:interface remote-class="com.ejb.txn.ApRepPurchaseOrderController"
*                extends="javax.ejb.EJBObject"
*
* @ejb:home remote-class="com.ejb.txn.ApRepPurchaseOrderControllerHome"
*           extends="javax.ejb.EJBHome"
*
* @ejb:transaction type="Required"
*
* @ejb:security-role-ref role-name="invuser"
*                        role-link="invuserlink"
*
* @ejb:permission role-name="invuser"
* 
*/

public class ApRepPurchaseOrderControllerBean extends AbstractSessionBean {
	
	
	/**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
	public ArrayList getAdLvInvItemCategoryAll(Integer AD_CMPNY) {
		
		Debug.print("ApRepPurchaseOrderControllerBean getAdLvInvItemCategoryAll");
		
		LocalAdLookUpValueHome adLookUpValueHome = null;               
		
		ArrayList list = new ArrayList();
		
		// Initialize EJB Home
		
		try {
			
			adLookUpValueHome = (LocalAdLookUpValueHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdLookUpValueHome.JNDI_NAME, LocalAdLookUpValueHome.class);
			
		} catch (NamingException ex) {
			
			throw new EJBException(ex.getMessage());
			
		}        
		
		try {
			
			Collection adLookUpValues = adLookUpValueHome.findByLuName("INV ITEM CATEGORY", AD_CMPNY);
			
			Iterator i = adLookUpValues.iterator();
			
			while (i.hasNext()) {
				
				LocalAdLookUpValue adLookUpValue = (LocalAdLookUpValue)i.next();
				
				list.add(adLookUpValue.getLvName());
				
			}
			
			return list;
			
		} catch (Exception ex) {
			
			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());
			
		}
		
	}
	
	/**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
	public ArrayList getInvLocAll(Integer AD_CMPNY) {
		
		Debug.print("ApRepPurchaseOrderControllerBean getInvLocAll");
		
		LocalInvLocationHome invLocationHome = null;
		Collection invLocations = null;
		ArrayList list = new ArrayList();
		
		// Initialize EJB Home
		
		try {
			
			invLocationHome = (LocalInvLocationHome)EJBHomeFactory.
				lookUpLocalHome(LocalInvLocationHome.JNDI_NAME, LocalInvLocationHome.class);
			
		} catch (NamingException ex) {
			
			throw new EJBException(ex.getMessage());
			
		}
		
		try {
			
			invLocations = invLocationHome.findLocAll(AD_CMPNY);            
			
			if (invLocations.isEmpty()) {
				
				return null;
				
			}
			
			Iterator i = invLocations.iterator();
			
			while (i.hasNext()) {
				
				LocalInvLocation invLocation = (LocalInvLocation)i.next();	
				String details = invLocation.getLocName();
				
				list.add(details);
				
			}
			
			return list;
			
		} catch (Exception ex) {
			
			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());
			
		}
		
	}

	/**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
	public ArrayList executeApRepPurchaseOrder(HashMap criteria,String ORDER_BY, boolean SUMMARIZE, ArrayList adBrnchList, Integer AD_CMPNY)
	throws GlobalNoRecordFoundException {
		
		Debug.print("ApRepPurchaseOrderControllerBean executeApRepPurchaseOrder");
		
		LocalApPurchaseOrderLineHome apPurchaseOrderLineHome = null;
		LocalAdPreferenceHome adPreferenceHome = null;
		LocalAdCompanyHome adCompanyHome = null;
		LocalAdBranchHome adBranchHome = null;
		
		ArrayList apPurchaseOrderList = new ArrayList();
		
		// Initialize EJB Home
		
		try {
			
			apPurchaseOrderLineHome = (LocalApPurchaseOrderLineHome)EJBHomeFactory.
				lookUpLocalHome(LocalApPurchaseOrderLineHome.JNDI_NAME, LocalApPurchaseOrderLineHome.class);
			adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);
			adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
			
			adBranchHome = (LocalAdBranchHome)EJBHomeFactory.
                    lookUpLocalHome(LocalAdBranchHome.JNDI_NAME, LocalAdBranchHome.class);
			
		} catch (NamingException ex) {
			
			throw new EJBException(ex.getMessage());
			
		}
		
		try { 
			
			LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);
			
			LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
			
			StringBuffer jbossQl = new StringBuffer();
			jbossQl.append("SELECT OBJECT (pl) FROM ApPurchaseOrderLine pl ");
			
			boolean firstArgument = true;
			short ctr = 0;
			int criteriaSize = criteria.size();	      
			
			Object obj[];    
			
			// Allocate the size of the object parameter
			
			if (criteria.containsKey("itemName")) {
				
				criteriaSize--;
				
			}
			
			if (criteria.containsKey("supplierCode")) {
				
				criteriaSize--;
				
			}
			
			if (criteria.containsKey("includeUnposted")) {
				
				criteriaSize--;
				
			}

			if (criteria.containsKey("includeCancelled")) {
				
				criteriaSize--;
				
			}
			
			obj = new Object[criteriaSize];
			
			if (criteria.containsKey("itemName")) {
				
				if (!firstArgument) {
					
					jbossQl.append("AND ");	
					
				} else {
					
					firstArgument = false;
					jbossQl.append("WHERE ");
					
				}
				
				jbossQl.append("pl.invItemLocation.invItem.iiName LIKE '%"
						+ (String)criteria.get("itemName") + "%' ");
				
			}

			if (((String)criteria.get("includeUnposted")).equals("no")){
				
				if (!firstArgument) {
					
					jbossQl.append("AND ");	
					
				} else {
					
					firstArgument = false;
					jbossQl.append("WHERE ");
					
				}
				
				jbossQl.append("pl.apPurchaseOrder.poPosted=1 ");

			}

			if (((String)criteria.get("includeCancelled")).equals("no")){
				
				if (!firstArgument) {
					
					jbossQl.append("AND ");	
					
				} else {
					
					firstArgument = false;
					jbossQl.append("WHERE ");
					
				}
				
				jbossQl.append("pl.apPurchaseOrder.poVoid=0 ");

			}
			
			if (criteria.containsKey("supplierCode")) {
				
				if (!firstArgument) {
					
					jbossQl.append("AND ");	
					
				} else {
					
					firstArgument = false;
					jbossQl.append("WHERE ");
					
				}
				
				jbossQl.append("pl.apPurchaseOrder.apSupplier.splSupplierCode LIKE '%"
						+ (String)criteria.get("supplierCode") + "%' ");
				
			}
			
			if (criteria.containsKey("category")) {
				
				if (!firstArgument) {	
					
					jbossQl.append("AND ");		
					
				} else {		
					
					firstArgument = false;
					jbossQl.append("WHERE ");
					
				}
				
				jbossQl.append("pl.invItemLocation.invItem.iiAdLvCategory=?" + (ctr+1) + " ");
				obj[ctr] = (String)criteria.get("category");
				ctr++;
				
			}	

			if (criteria.containsKey("location")) {
				
				if (!firstArgument) {
					
					jbossQl.append("AND ");	
					
				} else {		       	  	
					
					firstArgument = false;
					jbossQl.append("WHERE ");
					
				}
				
				jbossQl.append("pl.invItemLocation.invLocation.locName=?" + (ctr+1) + " ");
				obj[ctr] = (String)criteria.get("location");
				ctr++;
				
			}	
			
			if (criteria.containsKey("dateFrom")) {
				
				if (!firstArgument) {
					
					jbossQl.append("AND ");
					
				} else {
					
					firstArgument = false;
					jbossQl.append("WHERE ");
					
				}
				
				jbossQl.append("pl.apPurchaseOrder.poDate>=?" + (ctr+1) + " ");
				obj[ctr] = (Date)criteria.get("dateFrom");
				ctr++;
				
			}
			
			if (criteria.containsKey("dateTo")) {
				
				if (!firstArgument) {
					
					jbossQl.append("AND ");
					
				} else {
					
					firstArgument = false;
					jbossQl.append("WHERE ");
					
				}
				
				jbossQl.append("pl.apPurchaseOrder.poDate<=?" + (ctr+1) + " ");
				obj[ctr] = (Date)criteria.get("dateTo");
				ctr++;
				
			}
			
			if(adBrnchList.isEmpty()) {
				
				throw new GlobalNoRecordFoundException();
				
			} else {
			    
			    if (!firstArgument) {
			        
			        jbossQl.append("AND ");
			        
			    } else {
			        
			        firstArgument = false;
			        jbossQl.append("WHERE ");
			        
			    }	      
			    
			    jbossQl.append("pl.apPurchaseOrder.poAdBranch in (");
			    
			    boolean firstLoop = true;
			    
			    Iterator i = adBrnchList.iterator();
			    
			    while(i.hasNext()) {
			        
			        if(firstLoop == false) { 
			            jbossQl.append(", "); 
			        }
			        else { 
			            firstLoop = false; 
			        }
			        
			        AdBranchDetails mdetails = (AdBranchDetails) i.next();
			        
			        jbossQl.append(mdetails.getBrCode());
			        
			    }
			    
			    jbossQl.append(") ");
			    
			    firstArgument = false;
			    
			}

			jbossQl.append("AND pl.apPurchaseOrder.poReceiving = 0 AND pl.plAdCompany=" + AD_CMPNY + " ");  	  	  

			String orderBy = null;
	          
	          if (ORDER_BY.equals("DATE")) {
		      	 
		      	  orderBy = "pl.apPurchaseOrder.poDate";
		      	  
		      } else if (ORDER_BY.equals("ITEM NAME")) {
		      	
		      	  orderBy = "pl.invItemLocation.invItem.iiName";
		      	
		      } else if (ORDER_BY.equals("ITEM DESCRIPTION")) {
		      	
		      	  orderBy = "pl.invItemLocation.invItem.iiDescription";
		      	
		      } else if (ORDER_BY.equals("DOCUMENT NUMBER")) {
		      	
		      	  orderBy = "pl.apPurchaseOrder.poDocumentNumber";
		      	
		      } else if (ORDER_BY.equals("LOCATION")) {
		      	
		      	  orderBy = "pl.invItemLocation.invLocation.locName";
		      	
		      } else if (ORDER_BY.equals("PO NUMBER")) {
		      	
		      	  orderBy = "pl.apPurchaseOrder.poCode";
		      	
		      }

			  if (orderBy != null) {
			  
			  	jbossQl.append("ORDER BY " + orderBy);
			  	
			  }     
				 			
			Collection apPurchaseOrderLines = null;

			try {
				
				apPurchaseOrderLines = apPurchaseOrderLineHome.getPolByCriteria(jbossQl.toString(), obj);
				
			} catch (FinderException ex)	{
				
				throw new GlobalNoRecordFoundException ();
				
			} 	
			
			if (apPurchaseOrderLines.isEmpty())
				throw new GlobalNoRecordFoundException ();
			
			Iterator i = apPurchaseOrderLines.iterator();
			
			while (i.hasNext()) {
				
				LocalApPurchaseOrderLine apPurchaseOrderLine = (LocalApPurchaseOrderLine) i.next();
				
				ApRepPurchaseOrderDetails details = new ApRepPurchaseOrderDetails();
				
				details.setPoDate(apPurchaseOrderLine.getApPurchaseOrder().getPoDate());
				LocalAdBranch adBranchCode = adBranchHome.findByPrimaryKey(apPurchaseOrderLine.getApPurchaseOrder().getPoAdBranch());
				details.setPoBranchCode(adBranchCode.getBrBranchCode());
				details.setPoItemName(apPurchaseOrderLine.getInvItemLocation().getInvItem().getIiName());
				details.setPoLocation(apPurchaseOrderLine.getInvItemLocation().getInvLocation().getLocName());
				details.setPoUnit(apPurchaseOrderLine.getInvUnitOfMeasure().getUomName());
				details.setPoSupplierName(apPurchaseOrderLine.getApPurchaseOrder().getApSupplier().getSplName());
				details.setPoDocNo(apPurchaseOrderLine.getApPurchaseOrder().getPoDocumentNumber());
				details.setPoNumber(apPurchaseOrderLine.getApPurchaseOrder().getPoCode().toString());
				details.setPoQuantity(apPurchaseOrderLine.getPlQuantity());
				details.setPoUnitCost(apPurchaseOrderLine.getPlUnitCost());
				details.setPoDescription(apPurchaseOrderLine.getApPurchaseOrder().getPoDescription());
				details.setPoAmount(apPurchaseOrderLine.getPlAmount());
				details.setPoReferenceNumber(apPurchaseOrderLine.getApPurchaseOrder().getPoReferenceNumber());
				details.setPoItemDescription(
						apPurchaseOrderLine.getInvItemLocation().getInvItem().getIiDescription());
				
				details.setPoCreatedBy(apPurchaseOrderLine.getApPurchaseOrder().getPoCreatedBy());
				details.setOrderBy(ORDER_BY);
				if(apPurchaseOrderLine.getApPurchaseOrder().getPoVoid() == 1)
					details.setPoVoid("YES");
				else
					details.setPoVoid("NO");
				
				obj =  new Object [2] ;

				obj[0] = apPurchaseOrderLine.getApPurchaseOrder().getPoDocumentNumber();
				obj[1] = apPurchaseOrderLine.getInvItemLocation().getInvItem().getIiName();

				Collection apReceivingItemsLines = null;
				
				try {
				
					apReceivingItemsLines = apPurchaseOrderLineHome.getPolByCriteria(
						"SELECT OBJECT (pl) FROM ApPurchaseOrderLine pl WHERE pl.apPurchaseOrder.poRcvPoNumber =?1"
						+ " and pl.invItemLocation.invItem.iiName=?2"
						, obj);
				
				} catch (FinderException ex) {
					
					
					
				}
				
				if (!apPurchaseOrderLines.isEmpty()) {
					
					Iterator j = apReceivingItemsLines.iterator();
					
					double TTL_RCV_QTY = 0d;
					
					while (j.hasNext()){
						
						LocalApPurchaseOrderLine receivingItem = (LocalApPurchaseOrderLine) j.next();
						System.out.println("receivingItem="+receivingItem.getApPurchaseOrder().getPoDocumentNumber());
						TTL_RCV_QTY += receivingItem.getPlQuantity();
						
					}
					
					System.out.println("TTL_RCV_QTY="+TTL_RCV_QTY);
					details.setPoQuantityReceived(TTL_RCV_QTY);
					
				} else {
					
					details.setPoQuantityReceived(0);
					
				}					
				
				if(apPurchaseOrderLine.getApPurchaseOrder().getPoApprovedRejectedBy()!=null)
					details.setPoApprovedRejectedBy(apPurchaseOrderLine.getApPurchaseOrder().getPoApprovedRejectedBy());
				
				apPurchaseOrderList.add(details);
				
			}
			
			if (SUMMARIZE) {
			  	
			  	Collections.sort(apPurchaseOrderList, ApRepPurchaseOrderDetails.ItemNameComparator);
			  	
			  }
			
			return apPurchaseOrderList;	  
			
		} catch (GlobalNoRecordFoundException ex) {
			
			throw ex;
			
		} catch (Exception ex) {
			
			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());
			
		}
	}

	/**
	* @ejb:interface-method view-type="remote"
	* @jboss:method-attributes read-only="true"
	**/
	public com.util.AdCompanyDetails getAdCompany(Integer AD_CMPNY) {
	
	  Debug.print("ApRepPurchaseOrderControllerBean getAdCompany");      
	  
	  LocalAdCompanyHome adCompanyHome = null;
	        
	  // Initialize EJB Home
	    
	  try {
	        
	       adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
	           lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
	        
	  } catch (NamingException ex) {
	        
	      throw new EJBException(ex.getMessage());
	        
	  }
	
	  try {
	      
	     LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
	     
	     AdCompanyDetails details = new AdCompanyDetails();
	     details.setCmpName(adCompany.getCmpName());
	     
	     return details;  	
	  	       	
	  } catch (Exception ex) {
	  	
	  	 Debug.printStackTrace(ex);
	  	 throw new EJBException(ex.getMessage());
	  	  
	  }
	  
	}
	
	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/   
	public ArrayList getAdBrResAll(Integer RS_CODE, Integer AD_CMPNY) 
		throws GlobalNoRecordFoundException{
	    
	    Debug.print("ApRepPurchaseOrderControllerBean getAdBrResAll");
	    
	    LocalAdBranchResponsibilityHome adBranchResponsibilityHome = null;
	    LocalAdBranchHome adBranchHome = null;
	    
	    LocalAdBranchResponsibility adBranchResponsibility = null;
	    LocalAdBranch adBranch = null;
	    
	    Collection adBranchResponsibilities = null;
	    
	    ArrayList list = new ArrayList();
	    
	    // Initialize EJB Home
	    
	    try {
	        
	        adBranchResponsibilityHome = (LocalAdBranchResponsibilityHome)EJBHomeFactory.
	        lookUpLocalHome(LocalAdBranchResponsibilityHome.JNDI_NAME, LocalAdBranchResponsibilityHome.class);
	        adBranchHome = (LocalAdBranchHome)EJBHomeFactory.
	        lookUpLocalHome(LocalAdBranchHome.JNDI_NAME, LocalAdBranchHome.class);
	        
	    } catch (NamingException ex) {
	        
	        throw new EJBException(ex.getMessage());
	        
	    }
	    
	    try {
	        
	        adBranchResponsibilities = adBranchResponsibilityHome.findByAdResponsibility(RS_CODE, AD_CMPNY);
	        
	    } catch (FinderException ex) {
	        
	    } catch (Exception ex) {
	        
	        throw new EJBException(ex.getMessage());
	    }
	    
	    if (adBranchResponsibilities.isEmpty()) {
	        
	        throw new GlobalNoRecordFoundException();
	        
	    }
	    
	    try {
	        
	        Iterator i = adBranchResponsibilities.iterator();
	        
	        while(i.hasNext()) {
	            
	            adBranchResponsibility = (LocalAdBranchResponsibility)i.next();
	            
	            adBranch = adBranchResponsibility.getAdBranch();
	            
	            AdBranchDetails details = new AdBranchDetails();
	            
	            details.setBrCode(adBranch.getBrCode());
	            details.setBrBranchCode(adBranch.getBrBranchCode());
	            details.setBrName(adBranch.getBrName());
	            details.setBrHeadQuarter(adBranch.getBrHeadQuarter());
	            
	            list.add(details);
	            
	        }	               
	        
	    } catch (Exception ex) {
	        
	        throw new EJBException(ex.getMessage());
	    }
	    
	    return list;
	    
	}    
	
	// SessionBean methods
	
	/**
	 * @ejb:create-method view-type="remote"
	 **/
	public void ejbCreate() throws CreateException {
	    
	    Debug.print("ApRepPurchaseOrderControllerBean ejbCreate");
	    
	}
	
}

