/*
 * ApRepPurchaseOrderPrintControllerBean.java
 *
 * Created on April 19, 2005, 05:00 PM
 *
 * @author  Ma. Jennifer G. Manuel
 */

package com.ejb.txn;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;
import javax.naming.NamingException;

import com.ejb.ad.LocalAdApproval;
import com.ejb.ad.LocalAdApprovalDocument;
import com.ejb.ad.LocalAdApprovalDocumentHome;
import com.ejb.ad.LocalAdApprovalHome;
import com.ejb.ad.LocalAdBranch;
import com.ejb.ad.LocalAdBranchHome;
import com.ejb.ad.LocalAdCompany;
import com.ejb.ad.LocalAdCompanyHome;
import com.ejb.ad.LocalAdPreference;
import com.ejb.ad.LocalAdPreferenceHome;
import com.ejb.ad.LocalAdUserResponsibility;
import com.ejb.ad.LocalAdUserResponsibilityHome;
import com.ejb.ad.LocalAdUser;
import com.ejb.ad.LocalAdUserHome;
import com.ejb.ap.LocalApPurchaseOrder;
import com.ejb.ap.LocalApPurchaseOrderHome;
import com.ejb.ap.LocalApPurchaseOrderLine;
import com.ejb.inv.LocalInvItem;
import com.ejb.inv.LocalInvItemHome;
import com.ejb.inv.LocalInvTag;
import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.inv.LocalInvCosting;
import com.ejb.inv.LocalInvCostingHome;
import com.util.AbstractSessionBean;
import com.util.AdCompanyDetails;
import com.util.ApRepPurchaseOrderPrintDetails;
import com.util.Debug;
import com.util.EJBCommon;
import com.util.EJBHomeFactory;



/**
 * @ejb:bean name="ApRepPurchaseOrderPrintControllerEJB"
 *           display-name="Used for printing purchase order transactions"
 *           type="Stateless"
 *           view-type="remote"
 *           jndi-name="ejb/ApRepPurchaseOrderPrintControllerEJB"
 *
 * @ejb:interface remote-class="com.ejb.txn.ApRepPurchaseOrderPrintController"
 *                extends="javax.ejb.EJBObject"
 *
 * @ejb:home remote-class="com.ejb.txn.ApRepPurchaseOrderPrintControllerHome"
 *           extends="javax.ejb.EJBHome"
 *
 * @ejb:transaction type="Required"
 *
 */

public class ApRepPurchaseOrderPrintControllerBean extends AbstractSessionBean {


	/**
	 * @ejb:interface-method view-type="remote"
	 **/
	public ArrayList executeApRepPurchaseOrderPrint(ArrayList poCodeList, Integer AD_BRNCH, Integer AD_CMPNY)
	throws GlobalNoRecordFoundException {

		Debug.print("ApRepPurchaseOrderPrintControllerBean executeApRepPurchaseOrderPrint");

		LocalApPurchaseOrderHome apPurchaseOrderHome = null;
		LocalAdPreferenceHome adPreferenceHome = null;
		LocalInvCostingHome invCostingHome = null;
		LocalAdApprovalHome adApprovalHome = null;
		LocalAdApprovalDocumentHome adApprovalDocumentHome = null;
		LocalAdBranchHome adBranchHome = null;
		LocalAdUserHome adUserHome = null;
		LocalAdUserResponsibilityHome adUserResponsibilityHome = null;
		LocalInvItemHome invItemHome = null;



		ArrayList list = new ArrayList();

		// Initialize EJB Home

		try {

			apPurchaseOrderHome = (LocalApPurchaseOrderHome)EJBHomeFactory.
			lookUpLocalHome(LocalApPurchaseOrderHome .JNDI_NAME, LocalApPurchaseOrderHome .class);
			adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdPreferenceHome .JNDI_NAME, LocalAdPreferenceHome .class);
			invCostingHome = (LocalInvCostingHome)EJBHomeFactory.
			lookUpLocalHome(LocalInvCostingHome .JNDI_NAME, LocalInvCostingHome .class);
			adApprovalHome = (LocalAdApprovalHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdApprovalHome .JNDI_NAME, LocalAdApprovalHome .class);
			adApprovalDocumentHome = (LocalAdApprovalDocumentHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdApprovalDocumentHome .JNDI_NAME, LocalAdApprovalDocumentHome .class);
			adBranchHome = (LocalAdBranchHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdBranchHome .JNDI_NAME, LocalAdBranchHome .class);
			adUserHome = (LocalAdUserHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdUserHome .JNDI_NAME, LocalAdUserHome .class);
			adUserResponsibilityHome = (LocalAdUserResponsibilityHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdUserResponsibilityHome .JNDI_NAME, LocalAdUserResponsibilityHome .class);
			invItemHome = (LocalInvItemHome)EJBHomeFactory.
					lookUpLocalHome(LocalInvItemHome .JNDI_NAME, LocalInvItemHome .class);



		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}

		try {

			LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);


			Iterator i = poCodeList.iterator();

			while (i.hasNext()) {

				Integer PO_CODE = (Integer) i.next();

				LocalApPurchaseOrder apPurchaseOrder = null;


				try {

					apPurchaseOrder = apPurchaseOrderHome.findByPrimaryKey(PO_CODE);

				} catch (FinderException ex) {

					continue;

				}

				LocalAdApproval adApproval = adApprovalHome.findByAprAdCompany(AD_CMPNY);

				LocalAdApprovalDocument adApprovalDocument = adApprovalDocumentHome.findByAdcType("AP PURCHASE ORDER", AD_CMPNY);

				if (adApprovalDocument.getAdcPrintOption().equals("PRINT APPROVED ONLY")) {

					if (apPurchaseOrder.getPoApprovalStatus() == null ||
							apPurchaseOrder.getPoApprovalStatus().equals("PENDING")) {

						continue;

					}


				} else if (adApprovalDocument.getAdcPrintOption().equals("PRINT UNAPPROVED ONLY")) {

					if (apPurchaseOrder.getPoApprovalStatus() != null &&
							(apPurchaseOrder.getPoApprovalStatus().equals("N/A") ||
									apPurchaseOrder.getPoApprovalStatus().equals("APPROVED"))) {

						continue;

					}

				}

				if (adApprovalDocument.getAdcAllowDuplicate() == EJBCommon.FALSE &&
						apPurchaseOrder.getPoPrinted() == EJBCommon.TRUE){

					continue;

				}

				// set printed

				apPurchaseOrder.setPoPrinted(EJBCommon.TRUE);

				// get purchase order lines

				Collection apPurchaseOrderLines = apPurchaseOrder.getApPurchaseOrderLines();

				Iterator ilIter = apPurchaseOrderLines.iterator();

				double totalAmount = 0;
				while (ilIter.hasNext()) {
					LocalApPurchaseOrderLine apPurchaseOrderLine = (LocalApPurchaseOrderLine)ilIter.next();
					totalAmount+=apPurchaseOrderLine.getPlAmount();
				}
				ilIter = apPurchaseOrderLines.iterator();
				while (ilIter.hasNext()) {

					LocalApPurchaseOrderLine apPurchaseOrderLine = (LocalApPurchaseOrderLine)ilIter.next();

					String II_NM = apPurchaseOrderLine.getInvItemLocation().getInvItem().getIiName();
					String LOC_NM = apPurchaseOrderLine.getInvItemLocation().getInvLocation().getLocName();
					boolean isService = apPurchaseOrderLine.getInvItemLocation().getInvItem().getIiAdLvCategory().startsWith("Service");
					ApRepPurchaseOrderPrintDetails details = new ApRepPurchaseOrderPrintDetails();
					details.setPopPoDate(apPurchaseOrder.getPoDate());
					details.setPopPoDeliveryPeriod(apPurchaseOrder.getPoDeliveryPeriod());
					details.setPopPoSupplierName(apPurchaseOrder.getApSupplier().getSplName());

					if (isService){
						details.setPopPlIsService(true);
					}else{
						details.setPopPlIsService(false);
					}
					details.setPopPoContactPerson(apPurchaseOrder.getApSupplier().getSplContact());
					details.setPopPoContactNumber(apPurchaseOrder.getApSupplier().getSplPhone());
					details.setPopPoDocumentNumber(apPurchaseOrder.getPoDocumentNumber());
					details.setPopPoReferenceNumber(apPurchaseOrder.getPoReferenceNumber());

					details.setPopPoCreatedBy(apPurchaseOrder.getPoCreatedBy());
					LocalAdUser adUser = null;

					try{
						adUser = adUserHome.findByUsrName(apPurchaseOrder.getPoCreatedBy(), AD_CMPNY);
						String desc = adUser.getUsrDescription().toString();
						details.setPopPoCreatedBy(adUser.getUsrDept());
						details.setPopPoCreatedBy(adUser.getUsrDept());
					}catch(Exception e){
					}

					details.setPopPoShipTo(apPurchaseOrder.getPoShipTo());
					details.setPopPoPaymentTermName(apPurchaseOrder.getAdPaymentTerm().getPytName());
					details.setPopPoApprovalStatus(apPurchaseOrder.getPoApprovalStatus());

					if(adUser!=null) {
						try {
							LocalAdUser user1=null;
							try {
								user1=adUserHome.findByUsrName(apPurchaseOrder.getPoApprovedRejectedBy(), AD_CMPNY);
							}
							catch (FinderException ex) {}
							details.setPopPlApprovedRejectedBy(apPurchaseOrder.getPoApprovedRejectedBy().substring(0, 2)+apPurchaseOrder.getPoDocumentNumber().substring(2)+"-"+user1.getUsrPurchaseOrderApprovalCounter());
							Collection userResponsibility = adUserResponsibilityHome.findByUsrCode(adUser.getUsrCode(), AD_CMPNY);
							Iterator userRes = userResponsibility.iterator();
							while (userRes.hasNext()) {
								LocalAdUserResponsibility rs = (LocalAdUserResponsibility)userRes.next();
								System.out.println("rs d2: " + rs.getAdResponsibility().getRsName());
								details.setPopPoUserResponsibility(rs.getAdResponsibility().getRsName());
							}
						} catch (Exception ex) {

						}

					} else {
						details.setPopPlApprovedRejectedBy("");
						details.setPopPoUserResponsibility("");
					}

					if(apPurchaseOrder.getPoApprovedRejectedBy() == null || apPurchaseOrder.getPoApprovedRejectedBy().equals("")) {
						//details.setPopPoApprovedRejectedBy(adPreference.getPrfApDefaultApprover());

					} else {
						//details.setPopPoApprovedRejectedBy(apPurchaseOrder.getPoApprovedRejectedBy());

					}

					details.setPopPoCheckedBy(adPreference.getPrfApDefaultChecker());
					details.setPopPoDescription(apPurchaseOrder.getPoDescription());
					details.setPopPllIiName(II_NM);
					details.setPopPlIiDescription(apPurchaseOrderLine.getInvItemLocation().getInvItem().getIiDescription());

					details.setPopPlIiPartNumber(apPurchaseOrderLine.getInvItemLocation().getInvItem().getIiPartNumber());
					details.setPopPlIiBarCode1(apPurchaseOrderLine.getInvItemLocation().getInvItem().getIiBarCode1());
					details.setPopPlIiBarCode2(apPurchaseOrderLine.getInvItemLocation().getInvItem().getIiBarCode2());
					details.setPopPlIiBarCode3(apPurchaseOrderLine.getInvItemLocation().getInvItem().getIiBarCode3());
					details.setPopPlIiBrand(apPurchaseOrderLine.getInvItemLocation().getInvItem().getIiBrand());


					details.setPopPlLocName(LOC_NM);
					details.setPopPlUomName(apPurchaseOrderLine.getInvUnitOfMeasure().getUomName());
					details.setPopPlQuantity(apPurchaseOrderLine.getPlQuantity());
					details.setPopPlUnitCost(apPurchaseOrderLine.getPlUnitCost());
					details.setPopPlAmount(apPurchaseOrderLine.getPlAmount());
					details.setPopPlTotalAmount(totalAmount);

					//details.setPopPlBranchName(apPurchaseOrder.getPoAdBranch().toString());

					//        			Include Branch
					LocalAdBranch adBranch = adBranchHome.findByPrimaryKey(apPurchaseOrder.getPoAdBranch());
					details.setPopPlBranchCode(adBranch.getBrBranchCode());
					details.setPopPlBranchName(adBranch.getBrName());

					try {

						LocalInvCosting invCosting = invCostingHome.getByMaxCstDateToLongAndMaxCstLineNumberAndIiNameAndLocName(II_NM, LOC_NM, AD_BRNCH, AD_CMPNY);
						details.setPopPlEndingBalance(invCosting.getCstRemainingValue() + apPurchaseOrderLine.getPlAmount());

					} catch(FinderException ex) {
						details.setPopPlEndingBalance(apPurchaseOrderLine.getPlAmount());
					}

					//get discount
					String discount = "";

					if (apPurchaseOrderLine.getPlDiscount1() != 0)
						discount = "" + apPurchaseOrderLine.getPlDiscount1();

					if (apPurchaseOrderLine.getPlDiscount2() != 0)
						discount = discount + (!discount.equals("") ? "," : "") + apPurchaseOrderLine.getPlDiscount2();

					if (apPurchaseOrderLine.getPlDiscount3() != 0)
						discount = discount + (!discount.endsWith(",") && !discount.equals("") ? "," : "") +
						apPurchaseOrderLine.getPlDiscount3();

					if (apPurchaseOrderLine.getPlDiscount4() != 0)
						discount = discount + (!discount.endsWith(",") && !discount.equals("") ? "," : "") +
						apPurchaseOrderLine.getPlDiscount4();

					discount = discount.replaceAll(".0,",",");

					if (discount.endsWith(","))
						discount = discount.substring(0, discount.length() - 1);

					if (discount.endsWith(".0"))
						discount = discount.substring(0, discount.length() - 2);

					details.setPopPlDiscount(discount);
					details.setPopPlDiscount1(apPurchaseOrderLine.getPlDiscount1());
					details.setPopPlDiscount2(apPurchaseOrderLine.getPlDiscount2());
					details.setPopPlDiscount3(apPurchaseOrderLine.getPlDiscount3());
					details.setPopPlDiscount4(apPurchaseOrderLine.getPlDiscount4());
					details.setPopPlTotalDiscount(apPurchaseOrderLine.getPlTotalDiscount());
					details.setPopPoBillTo(apPurchaseOrder.getPoBillTo());
					details.setPopPlUnitOfMeasureShortName(apPurchaseOrderLine.getInvUnitOfMeasure().getUomShortName());
					details.setPopPoTaxRate(apPurchaseOrder.getApTaxCode().getTcRate());

					// get Tax Amount
					double NT_AMNT = calculatePlNetAmount(details, apPurchaseOrder.getApTaxCode().getTcRate(),
							apPurchaseOrder.getApTaxCode().getTcType(),this.getGlFcPrecisionUnit(AD_CMPNY)) ;

					details.setPopPlTaxAmount(calculatePlTaxAmount(details, apPurchaseOrder.getApTaxCode().getTcRate(),
							apPurchaseOrder.getApTaxCode().getTcType(), NT_AMNT, this.getGlFcPrecisionUnit(AD_CMPNY)));

					// get unit cost wo vat
					details.setPopPlUnitCostWoTax(calculatePlNetUnitCost(details,
							apPurchaseOrder.getApTaxCode().getTcRate(), apPurchaseOrder.getApTaxCode().getTcType(),
							this.getGlFcPrecisionUnit(AD_CMPNY)));

					// get unit cost vat inclusive
					details.setPopPlUnitCostTaxInclusive(calculatePlUnitCostTaxInclusive(details,
							apPurchaseOrder.getApTaxCode().getTcRate(), apPurchaseOrder.getApTaxCode().getTcType(),
							this.getGlFcPrecisionUnit(AD_CMPNY)));


					details.setPopPoTaxType(apPurchaseOrder.getApTaxCode().getTcType());

					details.setPopPoCurrency(apPurchaseOrder.getGlFunctionalCurrency().getFcName());

					details.setPopPoSupplierAddress(apPurchaseOrder.getApSupplier().getSplAddress());
					details.setPopPoSupplierCode(apPurchaseOrder.getApSupplier().getSplSupplierCode());
					details.setPopPlSupplierPhoneNumber(apPurchaseOrder.getApSupplier().getSplPhone());
					details.setPopPlSupplierFaxNumber(apPurchaseOrder.getApSupplier().getSplFax());





					//trace misc
        			if(apPurchaseOrderLine.getInvItemLocation().getInvItem().getIiTraceMisc()==EJBCommon.TRUE) {


						if(apPurchaseOrderLine.getInvTags().size()>0) {
							System.out.println("new code");
							StringBuilder strBProperty = new StringBuilder();
							StringBuilder strBSerial = new StringBuilder();
							StringBuilder strBSpecs = new StringBuilder();
							StringBuilder strBCustodian= new StringBuilder();
							StringBuilder strBExpirationDate = new StringBuilder();
							Iterator it = apPurchaseOrderLine.getInvTags().iterator();


							while(it.hasNext()) {

								LocalInvTag invTag = (LocalInvTag)it.next();

								//property code
								if(!invTag.getTgPropertyCode().equals("")) {
									strBProperty.append(invTag.getTgPropertyCode());
									strBProperty.append(System.getProperty("line.separator"));
								}

								//serial

								if(!invTag.getTgSerialNumber().equals("")) {
									strBSerial.append(invTag.getTgSerialNumber());
									strBSerial.append(System.getProperty("line.separator"));
								}

								//spec

								if(!invTag.getTgSpecs().equals("")) {
									strBSpecs.append(invTag.getTgSpecs());
									strBSpecs.append(System.getProperty("line.separator"));
								}

								//custodian

								if(invTag.getAdUser()!= null) {
									strBCustodian.append(invTag.getAdUser().getUsrName());
									strBCustodian.append(System.getProperty("line.separator"));
								}

								//exp date

								if(invTag.getTgExpiryDate()!=null) {
									strBExpirationDate.append(invTag.getTgExpiryDate());
									strBExpirationDate.append(System.getProperty("line.separator"));

								}



							}
							//property code
        					details.setPopPlPropertyCode(strBProperty.toString());
        					//serial number
        					details.setPopPlSerialNumber(strBSerial.toString());
        					//specs
        					details.setPopPlSpecs(strBSpecs.toString());
        					//custodian
        					details.setPopPlCustodian(strBCustodian.toString());
        					//expiration date
        					details.setPopPlExpiryDate(strBExpirationDate.toString());



						}


        			}







					list.add(details);

				}

			}

			if (list.isEmpty()) {

				throw new GlobalNoRecordFoundException();

			}

			return list;

		} catch (GlobalNoRecordFoundException ex) {

			throw ex;

		} catch (Exception ex) {

			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());

		}

	}

	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public com.util.AdCompanyDetails getAdCompany(Integer AD_CMPNY) {

		Debug.print("ApRepPurchaseOrderPrintControllerBean getAdCompany");

		LocalAdCompanyHome adCompanyHome = null;

		// Initialize EJB Home

		try {

			adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}

		try {

			LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);

			AdCompanyDetails details = new AdCompanyDetails();
			details.setCmpName(adCompany.getCmpName());

			return details;

		} catch (Exception ex) {

			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());

		}

	}

	//private methods

	private double calculatePlNetUnitCost(ApRepPurchaseOrderPrintDetails mdetails, double tcRate, String tcType,
			short precisionUnit) {

		Debug.print("ApRepPurchaseOrderPrintControllerBean calculatePlNetUnitCost");

		double amount = mdetails.getPopPlUnitCost();

		if (tcType.equals("INCLUSIVE")) {

			amount = EJBCommon.roundIt(mdetails.getPopPlUnitCost() / (1 + (tcRate / 100)), precisionUnit);

		}

		return amount;

	}

	private short getGlFcPrecisionUnit(Integer AD_CMPNY) {

		Debug.print("ApRepPurchaseOrderPrintControllerBean getGlFcPrecisionUnit");

		LocalAdCompanyHome adCompanyHome = null;


		// Initialize EJB Home

		try {

			adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}

		try {

			LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);

			return  adCompany.getGlFunctionalCurrency().getFcPrecision();

		} catch (Exception ex) {

			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());

		}

	}

	private double calculatePlTaxAmount(ApRepPurchaseOrderPrintDetails mdetails, double tcRate, String tcType,
			double amount, short precisionUnit) {

		Debug.print("ApRepPurchaseOrderPrintControllerBean calculatePlTaxAmount");

		double taxAmount = 0d;

		if (!tcType.equals("NONE") &&
				!tcType.equals("EXEMPT")) {


			if (tcType.equals("INCLUSIVE")) {

				taxAmount = EJBCommon.roundIt(mdetails.getPopPlAmount() - amount, precisionUnit);

			} else if (tcType.equals("EXCLUSIVE")) {

				taxAmount = EJBCommon.roundIt(mdetails.getPopPlAmount() * tcRate / 100, precisionUnit);

			} else {

				// tax none zero-rated or exempt

			}

		}

		return taxAmount;

	}

	private double calculatePlNetAmount(ApRepPurchaseOrderPrintDetails mdetails, double tcRate, String tcType,
			short precisionUnit) {

		Debug.print("ApRepPurchaseOrderPrintControllerBean calculatePlNetAmount");

		double amount = 0d;

		if (tcType.equals("INCLUSIVE")) {

			amount = EJBCommon.roundIt(mdetails.getPopPlAmount() / (1 + (tcRate / 100)), precisionUnit);

		} else {

			// tax exclusive, none, zero rated or exempt

			amount = mdetails.getPopPlAmount();

		}

		return amount;

	}

	private double calculatePlUnitCostTaxInclusive(ApRepPurchaseOrderPrintDetails mdetails, double tcRate, String tcType,
			short precisionUnit) {

		Debug.print("ApRepPurchaseOrderPrintControllerBean calculatePlUnitCostTaxInclusive");

		double amount = mdetails.getPopPlUnitCost();

		if (tcType.equals("EXCLUSIVE")) {

			amount = EJBCommon.roundIt(mdetails.getPopPlUnitCost() * (1 + (tcRate / 100)), precisionUnit);

		}

		return amount;

	}

	// SessionBean methods

	/**
	 * @ejb:create-method view-type="remote"
	 **/
	public void ejbCreate() throws CreateException {

		Debug.print("ApRepPurchaseOrderPrintControllerBean ejbCreate");

	}
}
