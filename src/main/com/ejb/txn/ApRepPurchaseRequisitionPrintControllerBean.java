
/*
 * ApRepPurchaseRequisitionPrintControllerBean.java
 *
 * Created on April 24, 2006 11:27 AM
 * @author  Aliza D.J. Anos
 */

package com.ejb.txn;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;
import javax.naming.NamingException;

import com.ejb.ad.LocalAdCompany;
import com.ejb.ad.LocalAdCompanyHome;
import com.ejb.ad.LocalAdUserHome;
import com.ejb.ap.LocalApPurchaseRequisition;
import com.ejb.ap.LocalApPurchaseRequisitionHome;
import com.ejb.ap.LocalApPurchaseRequisitionLine;
import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.inv.LocalInvTag;
import com.util.AbstractSessionBean;
import com.util.AdCompanyDetails;
import com.util.ApRepPurchaseRequisitionPrintDetails;
import com.util.Debug;
import com.util.EJBCommon;
import com.util.EJBHomeFactory;

/**
 * @ejb:bean name="ApRepPurchaseRequisitionPrintControllerEJB"
 *           display-name="Used for printing purchase requisition transactions"
 *           type="Stateless"
 *           view-type="remote"
 *           jndi-name="ejb/ApRepPurchaseRequisitionPrintControllerEJB"
 *
 * @ejb:interface remote-class="com.ejb.txn.ApRepPurchaseRequisitionPrintController"
 *                extends="javax.ejb.EJBObject"
 *
 * @ejb:home remote-class="com.ejb.txn.ApRepPurchaseRequisitionPrintControllerHome"
 *           extends="javax.ejb.EJBHome"
 *
 * @ejb:transaction type="Required"
 *
*/

public class ApRepPurchaseRequisitionPrintControllerBean extends AbstractSessionBean {

    /**
     * @ejb:interface-method view-type="remote"
     **/
    public ArrayList executeApRepPurchaseRequisitionPrint(ArrayList prCodeList, Integer AD_CMPNY)
        throws GlobalNoRecordFoundException {

        Debug.print("ApRepPurchaseRequisitionPrintControllerBean executeApRepPurchaseRequisitionPrint");

        LocalApPurchaseRequisitionHome apPurchaseRequisitionHome = null;
        LocalAdUserHome adUserHome = null;

        ArrayList list = new ArrayList();

        // Initialize EJB Home

        try {

        	apPurchaseRequisitionHome = (LocalApPurchaseRequisitionHome)EJBHomeFactory.
                lookUpLocalHome(LocalApPurchaseRequisitionHome .JNDI_NAME, LocalApPurchaseRequisitionHome .class);
        	adUserHome = (LocalAdUserHome)EJBHomeFactory.
                    lookUpLocalHome(LocalAdUserHome .JNDI_NAME, LocalAdUserHome .class);

        } catch (NamingException ex) {

            throw new EJBException(ex.getMessage());

        }

        try {

        	Iterator i = prCodeList.iterator();

        	while (i.hasNext()) {

        		Integer PR_CODE = (Integer) i.next();

        		LocalApPurchaseRequisition apPurchaseRequisition = null;

        		try {

        			apPurchaseRequisition = apPurchaseRequisitionHome.findByPrimaryKey(PR_CODE);

        		} catch (FinderException ex) {

        			continue;

        		}

        		// get purchase requisition lines

        		Collection apPurchaseRequisitionLines = apPurchaseRequisition.getApPurchaseRequisitionLines();

        		Iterator prlIter = apPurchaseRequisitionLines.iterator();
        		double grandTotal = 0;
        		double totalPerLine;
        		while (prlIter.hasNext()) {

        			LocalApPurchaseRequisitionLine apPurchaseRequisitionLine = (LocalApPurchaseRequisitionLine)prlIter.next();

        			boolean isService = apPurchaseRequisitionLine.getInvItemLocation().getInvItem().getIiAdLvCategory().startsWith("Service");

        			ApRepPurchaseRequisitionPrintDetails details = new ApRepPurchaseRequisitionPrintDetails();

        			if (isService){
        				System.out.println(isService + " <== service ba?");
        				details.setPrpPrIsService(true);
        				System.out.println(details.getPrpPrIsService() + " <== details.getRrpPlIsService");
        			}
        			else {
        				System.out.println(isService + " <== o hindi?");
        				details.setPrpPrIsService(false);
        				System.out.println(details.getPrpPrIsService() + " <== details.getRrpPlIsService");
        			}

        			details.setPrpPrNumber(apPurchaseRequisitionLine.getApPurchaseRequisition().getPrNumber());
        			details.setPrpPrDate(apPurchaseRequisitionLine.getApPurchaseRequisition().getPrDate());
        			details.setPrpPrDeliveryPeriod(apPurchaseRequisitionLine.getApPurchaseRequisition().getPrDeliveryPeriod());
        			details.setPrpPrDescription(apPurchaseRequisitionLine.getApPurchaseRequisition().getPrDescription());
        			details.setPrpFcSymbol(apPurchaseRequisitionLine.getApPurchaseRequisition().getGlFunctionalCurrency().getFcSymbol());
        			details.setPrpPrlIiName(apPurchaseRequisitionLine.getInvItemLocation().getInvItem().getIiName());
        			details.setPrpPrlIiDescription(apPurchaseRequisitionLine.getInvItemLocation().getInvItem().getIiDescription());
        			details.setPrpPrlIiPartNumber(apPurchaseRequisitionLine.getInvItemLocation().getInvItem().getIiPartNumber());
        			details.setPrpPrlIiBarCode1(apPurchaseRequisitionLine.getInvItemLocation().getInvItem().getIiBarCode1());
        			details.setPrpPrlIiBarCode2(apPurchaseRequisitionLine.getInvItemLocation().getInvItem().getIiBarCode2());
        			details.setPrpPrlIiBarCode3(apPurchaseRequisitionLine.getInvItemLocation().getInvItem().getIiBarCode3());
        			details.setPrpPrlIiBrand(apPurchaseRequisitionLine.getInvItemLocation().getInvItem().getIiBrand());
        			details.setPrpPrlLocName(apPurchaseRequisitionLine.getInvItemLocation().getInvLocation().getLocName());
        			details.setPrpPrlQuantity(apPurchaseRequisitionLine.getPrlQuantity());
        			details.setPrpPrlUomShortName(apPurchaseRequisitionLine.getInvUnitOfMeasure().getUomShortName());
        			details.setPrpPrlAmount(apPurchaseRequisitionLine.getPrlAmount());
        			details.setPrpPrlApprovedBy(apPurchaseRequisition.getPrApprovedRejectedBy());
        			details.setPrpPrlUnitCost(apPurchaseRequisitionLine.getInvItemLocation().getInvItem().getIiUnitCost());
        			totalPerLine = apPurchaseRequisitionLine.getPrlQuantity() * apPurchaseRequisitionLine.getPrlAmount();
        			grandTotal += totalPerLine;
        			details.setPrpPrlGrandTotalAmount(grandTotal);

					try {
						details.setPrpPrDepartment(adUserHome.findByUsrName(apPurchaseRequisition.getPrCreatedBy(), AD_CMPNY).getUsrDept());
					} catch (FinderException ex) {
						throw new EJBException(ex.getMessage());
					}




					//trace misc
        			if(apPurchaseRequisitionLine.getInvItemLocation().getInvItem().getIiTraceMisc()==EJBCommon.TRUE) {


						if(apPurchaseRequisitionLine.getInvTags().size()>0) {
							System.out.println("new code");
							StringBuilder strBProperty = new StringBuilder();
							StringBuilder strBSerial = new StringBuilder();
							StringBuilder strBSpecs = new StringBuilder();
							StringBuilder strBCustodian= new StringBuilder();
							StringBuilder strBExpirationDate = new StringBuilder();
							Iterator it = apPurchaseRequisitionLine.getInvTags().iterator();


							while(it.hasNext()) {

								LocalInvTag invTag = (LocalInvTag)it.next();

								//property code
								if(!invTag.getTgPropertyCode().equals("")) {
									strBProperty.append(invTag.getTgPropertyCode());
									strBProperty.append(System.getProperty("line.separator"));
								}

								//serial

								if(!invTag.getTgSerialNumber().equals("")) {
									strBSerial.append(invTag.getTgSerialNumber());
									strBSerial.append(System.getProperty("line.separator"));
								}

								//spec

								if(!invTag.getTgSpecs().equals("")) {
									strBSpecs.append(invTag.getTgSpecs());
									strBSpecs.append(System.getProperty("line.separator"));
								}

								//custodian

								if(invTag.getAdUser()!= null) {
									strBCustodian.append(invTag.getAdUser().getUsrName());
									strBCustodian.append(System.getProperty("line.separator"));
								}

								//exp date

								if(invTag.getTgExpiryDate()!=null) {
									strBExpirationDate.append(invTag.getTgExpiryDate());
									strBExpirationDate.append(System.getProperty("line.separator"));

								}



							}
							//property code
        					details.setPrpPrlPropertyCode(strBProperty.toString());
        					//serial number
        					details.setPrpPrlSerialNumber(strBSerial.toString());
        					//specs
        					details.setPrpPrlSpecs(strBSpecs.toString());
        					//custodian
        					details.setPrpPrlCustodian(strBCustodian.toString());
        					//expiration date
        					details.setPrpPrlExpiryDate(strBExpirationDate.toString());



						}


        			}



        			list.add(details);

        		}

        	}

	        if (list.isEmpty()) {

	        	throw new GlobalNoRecordFoundException();

	        }

        	return list;

        } catch (GlobalNoRecordFoundException ex) {

        	throw ex;

        } catch (Exception ex) {

        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());

        }

    }

    /**
	* @ejb:interface-method view-type="remote"
	* @jboss:method-attributes read-only="true"
	**/
	public AdCompanyDetails getAdCompany(Integer AD_CMPNY) {

	  Debug.print("ApRepPurchaseRequisitionPrintControllerBean getAdCompany");

	  LocalAdCompanyHome adCompanyHome = null;

	  // Initialize EJB Home

	  try {

	       adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
	           lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);

	  } catch (NamingException ex) {

	      throw new EJBException(ex.getMessage());

	  }

	  try {

	     LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);

	     AdCompanyDetails details = new AdCompanyDetails();

	     details.setCmpName(adCompany.getCmpName());

	     return details;

	  } catch (Exception ex) {

	  	 Debug.printStackTrace(ex);
	  	 throw new EJBException(ex.getMessage());

	  }

	}

    // SessionBean methods

    /**
     * @ejb:create-method view-type="remote"
     **/
    public void ejbCreate() throws CreateException {

       Debug.print("ApRepPurchaseRequisitionPrintControllerBean ejbCreate");

    }
}
