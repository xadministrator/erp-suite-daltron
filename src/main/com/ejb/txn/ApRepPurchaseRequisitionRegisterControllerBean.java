package com.ejb.txn;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;
import javax.naming.NamingException;

import com.ejb.ad.LocalAdBranch;
import com.ejb.ad.LocalAdBranchHome;
import com.ejb.ad.LocalAdBranchResponsibility;
import com.ejb.ad.LocalAdBranchResponsibilityHome;
import com.ejb.ad.LocalAdCompany;
import com.ejb.ad.LocalAdCompanyHome;
import com.ejb.ad.LocalAdLookUpValue;
import com.ejb.ad.LocalAdLookUpValueHome;
import com.ejb.ad.LocalAdUserHome;
import com.ejb.ap.LocalApSupplier;
import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.gl.LocalGlFunctionalCurrencyRate;
import com.ejb.gl.LocalGlFunctionalCurrencyRateHome;
import com.ejb.gl.LocalGlRecurringJournalHome;
import com.ejb.ap.LocalApPurchaseOrder;
import com.ejb.ap.LocalApPurchaseOrderHome;
import com.ejb.ap.LocalApPurchaseRequisition;
import com.ejb.ap.LocalApPurchaseRequisitionHome;
import com.ejb.ap.LocalApPurchaseRequisitionLine;
import com.ejb.ap.LocalApPurchaseRequisitionLineHome;
import com.ejb.inv.LocalInvBillOfMaterial;
import com.ejb.inv.LocalInvBillOfMaterialHome;
import com.ejb.inv.LocalInvItem;
import com.ejb.inv.LocalInvItemHome;
import com.ejb.inv.LocalInvItemLocationHome;
import com.ejb.inv.LocalInvLocation;
import com.ejb.inv.LocalInvLocationHome;
import com.util.AbstractSessionBean;
import com.util.AdBranchDetails;
import com.util.AdCompanyDetails;
import com.util.Debug;
import com.util.EJBCommon;
import com.util.EJBHomeFactory;
import com.util.ApRepPurchaseRequisitionRegisterDetails;
import com.util.InvRepItemListDetails;


/**
 * @ejb:bean name="ApRepPurchaseRequisitionRegisterControllerEJB"
 *           display-name="Used for finding ap requisition"
 *           type="Stateless"
 *           view-type="remote"
 *           jndi-name="ejb/ApRepPurchaseRequisitionRegisterControllerEJB"
 *
 * @ejb:interface remote-class="com.ejb.txn.ApRepPurchaseRequisitionRegisterController"
 *                extends="javax.ejb.EJBObject"
 *
 * @ejb:home remote-class="com.ejb.txn.ApRepPurchaseRequisitionRegisterControllerHome"
 *           extends="javax.ejb.EJBHome"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="apuser"
 *                        role-link="apuserlink"
 *
 * @ejb:permission role-name="apuser"
 * 
*/

public class ApRepPurchaseRequisitionRegisterControllerBean extends AbstractSessionBean {
	
	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public com.util.AdCompanyDetails getAdCompany(Integer AD_CMPNY) {
		
		Debug.print("ApRepPurchaseRequisitionRegisterControllerBean getAdCompany");      
		
		LocalAdCompanyHome adCompanyHome = null;
		
		// Initialize EJB Home
		
		try {
			
			adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
			
		} catch (NamingException ex) {
			
			throw new EJBException(ex.getMessage());
			
		}
		
		try {
			
			LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
			
			AdCompanyDetails details = new AdCompanyDetails();
			details.setCmpName(adCompany.getCmpName());
			
			return details;  	
			
		} catch (Exception ex) {
			
			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());
			
		}
		
	}  
	
	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/   
	public ArrayList getAdBrResAll(Integer RS_CODE, Integer AD_CMPNY) 
	throws GlobalNoRecordFoundException{
		
		Debug.print("ApRepPurchaseRequisitionRegisterControllerBean getAdBrResAll");
		
		LocalAdBranchResponsibilityHome adBranchResponsibilityHome = null;
		LocalAdBranchHome adBranchHome = null;
		
		LocalAdBranchResponsibility adBranchResponsibility = null;
		LocalAdBranch adBranch = null;
		
		Collection adBranchResponsibilities = null;
		
		ArrayList list = new ArrayList();
		
		// Initialize EJB Home
		
		try {
			
			adBranchResponsibilityHome = (LocalAdBranchResponsibilityHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdBranchResponsibilityHome.JNDI_NAME, LocalAdBranchResponsibilityHome.class);
			adBranchHome = (LocalAdBranchHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdBranchHome.JNDI_NAME, LocalAdBranchHome.class);
			
		} catch (NamingException ex) {
			
			throw new EJBException(ex.getMessage());
			
		}
		
		try {
			
			adBranchResponsibilities = adBranchResponsibilityHome.findByAdResponsibility(RS_CODE, AD_CMPNY);
			
		} catch (FinderException ex) {
			
		} catch (Exception ex) {
			
			throw new EJBException(ex.getMessage());
		}
		
		if (adBranchResponsibilities.isEmpty()) {
			
			throw new GlobalNoRecordFoundException();
			
		}
		
		try {
			
			Iterator i = adBranchResponsibilities.iterator();
			
			while(i.hasNext()) {
				
				adBranchResponsibility = (LocalAdBranchResponsibility)i.next();
				
				adBranch = adBranchResponsibility.getAdBranch();
				
				AdBranchDetails details = new AdBranchDetails();
				
				details.setBrCode(adBranch.getBrCode());
				details.setBrBranchCode(adBranch.getBrBranchCode());
				details.setBrName(adBranch.getBrName());
				details.setBrHeadQuarter(adBranch.getBrHeadQuarter());
				
				list.add(details);
				
			}	               
			
		} catch (Exception ex) {
			
			throw new EJBException(ex.getMessage());
		}
		
		return list;
		
	}    
	
	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public short getGlFcPrecisionUnit(Integer AD_CMPNY) {
		
		Debug.print("ApRepPurchaseRequisitionRegisterControllerBean getGlFcPrecisionUnit");
		
		
		LocalAdCompanyHome adCompanyHome = null;
		
		
		// Initialize EJB Home
		
		try {
			
			adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);       
			
		} catch (NamingException ex) {
			
			throw new EJBException(ex.getMessage());
			
		}
		
		try {
			
			LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
			
			return  adCompany.getGlFunctionalCurrency().getFcPrecision();
			
		} catch (Exception ex) {
			
			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());
			
		}
		
	}
	
	
	/**
	 * @throws FinderException 
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public ArrayList executeApRepPurchaseRequisitionRegister(HashMap criteria, String ORDER_BY, String GROUP_BY, boolean SHOW_LN_ITEMS, ArrayList adBrnchList, String REPORT_TYPE, Integer AD_CMPNY) 
	throws GlobalNoRecordFoundException, FinderException {
		
		Debug.print("ApRepPurchaseRequisitionRegisterControllerBean executeApRepPurchaseRequisitionRegister");
		
		LocalApPurchaseRequisitionHome apPurchaseRequisitionHome = null;
		LocalApPurchaseRequisitionLineHome apPurchaseRequisitionLineHome = null;
		LocalAdCompanyHome adCompanyHome = null;
		
		LocalInvItemHome invItemHome = null;
        LocalInvItemLocationHome invItemLocationHome = null;
        LocalInvLocationHome invLocationHome = null;
        
        LocalInvBillOfMaterialHome invBillOfMaterialHome = null;
        LocalApPurchaseOrderHome apPurchaseOrderHome = null;
        LocalAdUserHome adUserHome = null;
        
		ArrayList list = new ArrayList();
		ArrayList listAdj = new ArrayList();
		//initialized EJB Home
		
		try {
			
			apPurchaseRequisitionHome = (LocalApPurchaseRequisitionHome)EJBHomeFactory.
			lookUpLocalHome(LocalApPurchaseRequisitionHome.JNDI_NAME, LocalApPurchaseRequisitionHome.class);
			apPurchaseRequisitionLineHome = (LocalApPurchaseRequisitionLineHome)EJBHomeFactory.
			lookUpLocalHome(LocalApPurchaseRequisitionLineHome.JNDI_NAME, LocalApPurchaseRequisitionLineHome.class);
			adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
			
		    invItemHome = (LocalInvItemHome)EJBHomeFactory.
            lookUpLocalHome(LocalInvItemHome.JNDI_NAME, LocalInvItemHome.class);
            invItemLocationHome = (LocalInvItemLocationHome)EJBHomeFactory.
            lookUpLocalHome(LocalInvItemLocationHome.JNDI_NAME, LocalInvItemLocationHome.class);
            invLocationHome = (LocalInvLocationHome)EJBHomeFactory.
            lookUpLocalHome(LocalInvLocationHome.JNDI_NAME, LocalInvLocationHome.class);
			
            invBillOfMaterialHome = (LocalInvBillOfMaterialHome)EJBHomeFactory.
                    lookUpLocalHome(LocalInvBillOfMaterialHome.JNDI_NAME, LocalInvBillOfMaterialHome.class);
            apPurchaseOrderHome = (LocalApPurchaseOrderHome)EJBHomeFactory.
                    lookUpLocalHome(LocalApPurchaseOrderHome.JNDI_NAME, LocalApPurchaseOrderHome.class);
            adUserHome = (LocalAdUserHome)EJBHomeFactory.
                    lookUpLocalHome(LocalAdUserHome.JNDI_NAME, LocalAdUserHome.class);
		} catch (NamingException ex) 	{
			
			throw new EJBException(ex.getMessage());
			
		}
		
		if(REPORT_TYPE.equals("RM_USED")){
	            
	            return list;
		}else if(REPORT_TYPE.equals("PROD_LIST")){
	            return list;
			
		}else if(REPORT_TYPE.equals("RM_VAR")){
	            return list;
		} else {
			try { 
				LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
				StringBuffer jbossQl = new StringBuffer();
				jbossQl.append("SELECT OBJECT(pr) FROM ApPurchaseRequisition pr ");
				boolean firstArgument = true;
				short ctr = 0;
				int criteriaSize = criteria.size();	      
				Object obj[];	      
				
				// Allocate the size of the object parameter
				if (criteria.containsKey("includedUnposted")) {
					criteriaSize--;
				}
				obj = new Object[criteriaSize];
				if (criteria.containsKey("type")) {
					if (!firstArgument) {
						jbossQl.append("AND ");
					} else {
						firstArgument = false;
						jbossQl.append("WHERE ");
					}
					jbossQl.append("pr.prType=?" + (ctr+1) + " ");
					System.out.println("TYPE="+(String)criteria.get("type"));
					obj[ctr] = (String)criteria.get("type");
					ctr++;
				}  
					if (criteria.containsKey("accountDescription")) {
					if (!firstArgument) {
						jbossQl.append("AND ");
					} else {
						firstArgument = false;
						jbossQl.append("WHERE ");
					}
					System.out.print("hhhhhererhere"+(String)criteria.get("accountDescription"));
					jbossQl.append("pr.glChartOfAccount.coaAccountNumber=?" + (ctr+1) + " ");
					obj[ctr] = (String)criteria.get("accountDescription");
					ctr++;
				} 
				if (criteria.containsKey("dateFrom")) {
					if (!firstArgument) {
						jbossQl.append("AND ");
					} else {
						firstArgument = false;
						jbossQl.append("WHERE ");
					}
					jbossQl.append("pr.prDate>=?" + (ctr+1) + " ");
					obj[ctr] = (Date)criteria.get("dateFrom");
					ctr++;
				}  
				if (criteria.containsKey("dateTo")) {
					if (!firstArgument) {
						jbossQl.append("AND ");
					} else {
						firstArgument = false;
						jbossQl.append("WHERE ");
					}
					jbossQl.append("pr.prDate<=?" + (ctr+1) + " ");
					obj[ctr] = (Date)criteria.get("dateTo");
					ctr++;
				}    
				if (criteria.containsKey("documentNumberFrom")) {
					if (!firstArgument) {
						jbossQl.append("AND ");
					} else {
						firstArgument = false;
						jbossQl.append("WHERE ");
					}
					jbossQl.append("pr.prDocumentNumber>=?" + (ctr+1) + " ");
					obj[ctr] = (String)criteria.get("documentNumberFrom");
					ctr++;
				}  
				if (criteria.containsKey("documentNumberTo")) {
					if (!firstArgument) {
						jbossQl.append("AND ");
					} else {
						firstArgument = false;
						jbossQl.append("WHERE ");
					}
					jbossQl.append("pr.prDocumentNumber<=?" + (ctr+1) + " ");
					obj[ctr] = (String)criteria.get("documentNumberTo");
					ctr++;
				}
				if (criteria.containsKey("referenceNumber")) {
					if (!firstArgument) {
						jbossQl.append("AND ");
					} else {
						firstArgument = false;
						jbossQl.append("WHERE ");
					}
					jbossQl.append("pr.prReferenceNumber>=?" + (ctr+1) + " ");
					obj[ctr] = (String)criteria.get("referenceNumber");
					ctr++;
				}
				if (((String)criteria.get("includedUnposted")).equals("NO")) {
					if (!firstArgument) {
						jbossQl.append("AND ");
					} else {
						firstArgument = false;
						jbossQl.append("WHERE ");
					}	       	  
					jbossQl.append("pr.prPosted = 1 ");
				} 
				if(adBrnchList.isEmpty()) {
					throw new GlobalNoRecordFoundException();
				} else {
					if (!firstArgument) {
						jbossQl.append("AND ");
					} else {
						firstArgument = false;
						jbossQl.append("WHERE ");
					}	      
					jbossQl.append("pr.prAdBranch in (");
					boolean firstLoop = true;
					Iterator i = adBrnchList.iterator();
					while(i.hasNext()) {
						if(firstLoop == false) { 
							jbossQl.append(", "); 
						}
						else { 
							firstLoop = false; 
						}
						AdBranchDetails mdetails = (AdBranchDetails) i.next();
						jbossQl.append(mdetails.getBrCode());
					}
					jbossQl.append(") ");
					firstArgument = false;
				}
				if (!firstArgument) {
					jbossQl.append("AND ");
				} else {
					firstArgument = false;
					jbossQl.append("WHERE ");
				}
				jbossQl.append("pr.prAdCompany = " + AD_CMPNY + " ");
				String orderBy = null;
				if (ORDER_BY.equals("DATE")) {
					orderBy = "pr.prDate";
				} else if (ORDER_BY.equals("DOCUMENT NUMBER")) {
					orderBy = "pr.prDocumentNumber";
				} 
				if (orderBy != null) {
					jbossQl.append("ORDER BY " + orderBy);
				} 
				Collection apPurchaseRequisitions = null;
				try {
					System.out.println("jbossQl.toString()"+jbossQl.toString());
					apPurchaseRequisitions = apPurchaseRequisitionHome.getPrByCriteria(jbossQl.toString(), obj);
					System.out.println("Purchase Requisition="+apPurchaseRequisitions.size());
				} catch (FinderException ex)	{
				} 	
				if (apPurchaseRequisitions.size() == 0)
					throw new GlobalNoRecordFoundException();
				Iterator i = apPurchaseRequisitions.iterator();
				while (i.hasNext()) {
					LocalApPurchaseRequisition apPurchaseRequisition = (LocalApPurchaseRequisition)i.next();   	  
					if (SHOW_LN_ITEMS) {
						double AMOUNT = 0d;
						Collection apPurchaseRequisitionLines = apPurchaseRequisition.getApPurchaseRequisitionLines();
						Iterator alItr = apPurchaseRequisitionLines.iterator();
						while (alItr.hasNext()) {
							LocalApPurchaseRequisitionLine apPurchaseRequisitionLine = (LocalApPurchaseRequisitionLine)alItr.next();
							ApRepPurchaseRequisitionRegisterDetails details = new ApRepPurchaseRequisitionRegisterDetails();
							details.setPrDate(apPurchaseRequisition.getPrDate());
							details.setPrDocumentNumber(apPurchaseRequisition.getPrNumber());
							details.setPrReferenceNumber(apPurchaseRequisition.getPrReferenceNumber());
							details.setPrType(apPurchaseRequisition.getPrType());
							details.setPrDescription(apPurchaseRequisition.getPrDescription());
							details.setPrItemName(apPurchaseRequisitionLine.getInvItemLocation().getInvItem().getIiName());
							details.setPrItemDesc(apPurchaseRequisitionLine.getInvItemLocation().getInvItem().getIiDescription());
							
							try {   
								details.setPrDepartment(adUserHome.findByUsrName(apPurchaseRequisition.getPrCreatedBy(), AD_CMPNY).getUsrDept());
							} catch (FinderException ex) {
								throw new EJBException(ex.getMessage());
							}
							
							LocalApPurchaseOrder apPurchaseOrder;
							try {   
								apPurchaseOrder=apPurchaseOrderHome.findByPoReferenceNumber(apPurchaseRequisition.getPrCode().toString(), AD_CMPNY);
								details.setPrApSupplier(apPurchaseOrder.getApSupplier().getSplName());
								details.setPrApPoNumber(apPurchaseOrder.getPoDocumentNumber());
								details.setPrPoDate(apPurchaseOrder.getPoDate());
							} catch (FinderException ex) {
								details.setPrApSupplier("");
								details.setPrApPoNumber("");
							}
							
							details.setPrLocation(apPurchaseRequisitionLine.getInvItemLocation().getInvLocation().getLocName());
							details.setPrQuantity(apPurchaseRequisitionLine.getPrlQuantity());
							details.setPrUnit(apPurchaseRequisitionLine.getInvUnitOfMeasure().getUomName());
							details.setPrUnitCost(apPurchaseRequisitionLine.getInvItemLocation().getInvItem().getIiUnitCost());
							details.setPrAccountDescription(null);
						    AMOUNT = EJBCommon.roundIt(apPurchaseRequisitionLine.getPrlAmount() * apPurchaseRequisitionLine.getInvItemLocation().getInvItem().getIiUnitCost(),  this.getGlFcPrecisionUnit(AD_CMPNY));
							details.setPrAmount(AMOUNT);
							details.setOrderBy(ORDER_BY);
							System.out.println("1");
							list.add(details);
						} 
					} else {
						System.out.println("ELSE-------------------------");
						ApRepPurchaseRequisitionRegisterDetails details = new ApRepPurchaseRequisitionRegisterDetails();
						details.setPrDate(apPurchaseRequisition.getPrDate());
						details.setPrDocumentNumber(apPurchaseRequisition.getPrNumber());
						details.setPrReferenceNumber(apPurchaseRequisition.getPrReferenceNumber());
						details.setPrType(apPurchaseRequisition.getPrType());
						details.setPrDescription(apPurchaseRequisition.getPrDescription());
						details.setPrAccountDescription(null);
					    
					    double AMOUNT = 0d;
						Collection apPurchaseRequisitionLines = apPurchaseRequisition.getApPurchaseRequisitionLines();
						Iterator alItr = apPurchaseRequisitionLines.iterator();
						while (alItr.hasNext()) {
							LocalApPurchaseRequisitionLine apPurchaseRequisitionLine = (LocalApPurchaseRequisitionLine)alItr.next();
							AMOUNT += EJBCommon.roundIt(apPurchaseRequisitionLine.getPrlQuantity() * apPurchaseRequisitionLine.getInvItemLocation().getInvItem().getIiUnitCost(), this.getGlFcPrecisionUnit(AD_CMPNY));
						} 
						details.setPrAmount(AMOUNT);
						details.setOrderBy(ORDER_BY);
						list.add(details);
					}
				}
				
				// sort
				System.out.println("2");
				if (GROUP_BY.equals("ITEM NAME")) {
					Collections.sort(list, ApRepPurchaseRequisitionRegisterDetails.ItemNameComparator);
				} else if (GROUP_BY.equals("DATE")) {
					Collections.sort(list, ApRepPurchaseRequisitionRegisterDetails.DateComparator);
				} else {
					Collections.sort(list, ApRepPurchaseRequisitionRegisterDetails.NoGroupComparator);
				}
				System.out.println("3");
				return list;
			} catch (GlobalNoRecordFoundException ex) {
				throw ex;
			} catch (Exception ex) {
				ex.printStackTrace();
				throw new EJBException(ex.getMessage());
			}
		}
		
		
	}
	
	
	// private methods
	private double convertForeignToFunctionalCurrency(Integer FC_CODE, String FC_NM, 
			Date CONVERSION_DATE, double CONVERSION_RATE, double AMOUNT, Integer AD_CMPNY) {
		
		Debug.print("ApRepPurchaseRequisitionRegisterControllerBean convertForeignToFunctionalCurrency");
		
		
		LocalGlFunctionalCurrencyRateHome glFunctionalCurrencyRateHome = null;
		LocalAdCompanyHome adCompanyHome = null;
		
		LocalAdCompany adCompany = null;
		
		// Initialize EJB Homes
		
		try {
			
			glFunctionalCurrencyRateHome = (LocalGlFunctionalCurrencyRateHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlFunctionalCurrencyRateHome.JNDI_NAME, LocalGlFunctionalCurrencyRateHome.class);                         
			adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);             
			
		} catch (NamingException ex) {
			
			throw new EJBException(ex.getMessage());
			
		}
		
		// get company and extended precision
		
		try {
			
			adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
			
		} catch (Exception ex) {
			
			throw new EJBException(ex.getMessage());
			
		}	     
		
		
		// Convert to functional currency if necessary
		
		if (CONVERSION_RATE != 1 && CONVERSION_RATE != 0 ) {
			
			AMOUNT = AMOUNT * CONVERSION_RATE;
			
		}
		
		return EJBCommon.roundIt(AMOUNT, adCompany.getGlFunctionalCurrency().getFcPrecision());
		
	}
	
	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public ArrayList getAdLvPurchaseRequisitionType(Integer AD_CMPNY) {
		
		Debug.print("ApRepPurchaseRequisitionRegisterControllerBean getAdLvPurchaseRequisitionType");
		
		LocalAdLookUpValueHome adLookUpValueHome = null;               
		
		ArrayList list = new ArrayList();
		
		// Initialize EJB Home
		
		try {
			
			adLookUpValueHome = (LocalAdLookUpValueHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdLookUpValueHome.JNDI_NAME, LocalAdLookUpValueHome.class);
			
		} catch (NamingException ex) {
			
			throw new EJBException(ex.getMessage());
			
		}        
		
		try {
			
			Collection adLookUpValues = adLookUpValueHome.findByLuName("PR TYPE", AD_CMPNY);
			
			Iterator i = adLookUpValues.iterator();
			
			while (i.hasNext()) {
				
				LocalAdLookUpValue adLookUpValue = (LocalAdLookUpValue)i.next();
				
				list.add(adLookUpValue.getLvName());
				
			}
			
			return list;
			
		} catch (Exception ex) {
			
			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());
			
		}
		
	}
	
	/**
	 * @ejb:create-method view-type="remote"
	 **/
	public void ejbCreate() throws CreateException {
		
		Debug.print("ApRepPurchaseRequisitionControllerBean ejbCreate");
		
	}
}
