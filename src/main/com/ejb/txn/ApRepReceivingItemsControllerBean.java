package com.ejb.txn;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;
import javax.naming.NamingException;

import com.ejb.ad.LocalAdBranch;
import com.ejb.ad.LocalAdBranchHome;
import com.ejb.ad.LocalAdBranchResponsibility;
import com.ejb.ad.LocalAdBranchResponsibilityHome;
import com.ejb.ad.LocalAdCompany;
import com.ejb.ad.LocalAdCompanyHome;
import com.ejb.ad.LocalAdLookUpValue;
import com.ejb.ad.LocalAdLookUpValueHome;
import com.ejb.ad.LocalAdPreference;
import com.ejb.ad.LocalAdPreferenceHome;
import com.ejb.ap.LocalApPurchaseOrderLine;
import com.ejb.ap.LocalApPurchaseOrderLineHome;
import com.ejb.ap.LocalApVoucherLineItem;
import com.ejb.ap.LocalApVoucherLineItemHome;
import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.inv.LocalInvItem;
import com.ejb.inv.LocalInvLocation;
import com.ejb.inv.LocalInvLocationHome;
import com.ejb.inv.LocalInvUnitOfMeasure;
import com.ejb.inv.LocalInvUnitOfMeasureConversion;
import com.ejb.inv.LocalInvUnitOfMeasureConversionHome;
import com.util.AbstractSessionBean;
import com.util.AdBranchDetails;
import com.util.AdCompanyDetails;
import com.util.ApRepReceivingItemsDetails;
import com.util.Debug;
import com.util.EJBCommon;
import com.util.EJBHomeFactory;

/**
* @ejb:bean name="ApRepReceivingItemsControllerEJB"
*           display-name="Used for generation of receivings items reports"
*           type="Stateless"
*           view-type="remote"
*           jndi-name="ejb/ApRepReceivingItemsControllerEJB"
*
* @ejb:interface remote-class="com.ejb.txn.ApRepReceivingItemsController"
*                extends="javax.ejb.EJBObject"
*
* @ejb:home remote-class="com.ejb.txn.ApRepReceivingItemsControllerHome"
*           extends="javax.ejb.EJBHome"
*
* @ejb:transaction type="Required"
*
* @ejb:security-role-ref role-name="invuser"
*                        role-link="invuserlink"
*
* @ejb:permission role-name="invuser"
* 
*/

public class ApRepReceivingItemsControllerBean extends AbstractSessionBean {
	
	
	/**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
	public ArrayList getAdLvInvItemCategoryAll(Integer AD_CMPNY) {
		
		Debug.print("ApRepReceivingItemsControllerBean getAdLvInvItemCategoryAll");
		
		LocalAdLookUpValueHome adLookUpValueHome = null;               
		
		ArrayList list = new ArrayList();
		
		// Initialize EJB Home
		
		try {
			
			adLookUpValueHome = (LocalAdLookUpValueHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdLookUpValueHome.JNDI_NAME, LocalAdLookUpValueHome.class);
			
		} catch (NamingException ex) {
			
			throw new EJBException(ex.getMessage());
			
		}        
		
		try {
			
			Collection adLookUpValues = adLookUpValueHome.findByLuName("INV ITEM CATEGORY", AD_CMPNY);
			
			Iterator i = adLookUpValues.iterator();
			
			while (i.hasNext()) {
				
				LocalAdLookUpValue adLookUpValue = (LocalAdLookUpValue)i.next();
				
				list.add(adLookUpValue.getLvName());
				
			}
			
			return list;
			
		} catch (Exception ex) {
			
			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());
			
		}
		
	}
	
	/**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
	public ArrayList getInvLocAll(Integer AD_CMPNY) {
		
		Debug.print("ApRepReceivingItemsControllerBean getInvLocAll");
		
		LocalInvLocationHome invLocationHome = null;
		Collection invLocations = null;
		ArrayList list = new ArrayList();
		
		// Initialize EJB Home
		
		try {
			
			invLocationHome = (LocalInvLocationHome)EJBHomeFactory.
				lookUpLocalHome(LocalInvLocationHome.JNDI_NAME, LocalInvLocationHome.class);
			
		} catch (NamingException ex) {
			
			throw new EJBException(ex.getMessage());
			
		}
		
		try {
			
			invLocations = invLocationHome.findLocAll(AD_CMPNY);            
			
			if (invLocations.isEmpty()) {
				
				return null;
				
			}
			
			Iterator i = invLocations.iterator();
			
			while (i.hasNext()) {
				
				LocalInvLocation invLocation = (LocalInvLocation)i.next();	
				String details = invLocation.getLocName();
				
				list.add(details);
				
			}
			
			return list;
			
		} catch (Exception ex) {
			
			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());
			
		}
		
	}

	/**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
	public ArrayList executeApRepReceivingItems(HashMap criteria,String ORDER_BY, String GROUP_BY, ArrayList adBrnchList, Integer AD_CMPNY)
	throws GlobalNoRecordFoundException {
		
		Debug.print("ApRepReceivingItemsControllerBean executeApRepReceivingItems");
		
		LocalApPurchaseOrderLineHome apReceivingItemLineHome = null;
		LocalAdPreferenceHome adPreferenceHome = null;
		LocalAdCompanyHome adCompanyHome = null;
		LocalApVoucherLineItemHome apVoucherLineItemHome = null;
		
		ArrayList apReceivingItemList = new ArrayList();
				
		// Initialize EJB Home
		
		try {
			
			apReceivingItemLineHome = (LocalApPurchaseOrderLineHome)EJBHomeFactory.
			lookUpLocalHome(LocalApPurchaseOrderLineHome.JNDI_NAME, LocalApPurchaseOrderLineHome.class);
			adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);
			adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
			apVoucherLineItemHome = (LocalApVoucherLineItemHome)EJBHomeFactory.
			lookUpLocalHome(LocalApVoucherLineItemHome.JNDI_NAME, LocalApVoucherLineItemHome.class);
						
		} catch (NamingException ex) {
			
			throw new EJBException(ex.getMessage());
			
		}
		
		try { 
			
			LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);
			
			LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
			
			StringBuffer jbossQl = new StringBuffer();
			
			boolean firstArgument = true;
			short ctr = 0;
			int criteriaSize = criteria.size();	      
			
			Object obj[];    
			
			// Allocate the size of the object parameter
			
			if (criteria.containsKey("itemName")) {
				
				criteriaSize--;
				
			}
			
			if (criteria.containsKey("supplierCode")) {
				
				criteriaSize--;
				
			}
			
			if (criteria.containsKey("includeUnposted")) {
				
				criteriaSize--;
				
			}
			
			if (criteria.containsKey("includedPoReceiving")) {
				
				criteriaSize--;
				
			}
			
			if (criteria.containsKey("includedVoucher")) {
				
				criteriaSize--;
				
			}
			
			if (criteria.containsKey("includedDirectCheck")) {
				
				criteriaSize--;
				
			}
			
			if (criteria.containsKey("poReceivingDocumentNumberFrom")){
				
				criteriaSize--;
				
			}
			
			if (criteria.containsKey("poReceivingDocumentNumberTo")){
				
				criteriaSize--;
				
			}
			
			if (criteria.containsKey("voucherDocumentNumberFrom")){
				
				criteriaSize--;
				
			}
			
			if (criteria.containsKey("voucherDocumentNumberTo")){
				
				criteriaSize--;
				
			}
			
			if (criteria.containsKey("checkDocumentNumberFrom")){
				
				criteriaSize--;
				
			}
			
			if (criteria.containsKey("checkDocumentNumberTo")){
				
				criteriaSize--;
				
			}
			
			
			// receiving items
			if(((String)criteria.get("includedPoReceiving")).equals("YES")) {
				
				int objSize = criteriaSize;
				
				if (criteria.containsKey("poReceivingDocumentNumberFrom")){
					
					objSize++;
					
				}
				
				if (criteria.containsKey("poReceivingDocumentNumberTo")){
					
					objSize++;
					
				}
				
				obj = new Object[objSize];	
				
				jbossQl.append("SELECT OBJECT (pl) FROM ApPurchaseOrderLine pl ");
				
				if (criteria.containsKey("itemName")) {
					
					if (!firstArgument) {
						
						jbossQl.append("AND ");	
						
					} else {
						
						firstArgument = false;
						jbossQl.append("WHERE ");
						
					}
					
					jbossQl.append("pl.invItemLocation.invItem.iiName LIKE '%" + (String)criteria.get("itemName") + "%' ");
					
				}
				
				if (((String)criteria.get("includeUnposted")).equals("NO")){
					
					if (!firstArgument) {
						
						jbossQl.append("AND ");	
						
					} else {
						
						firstArgument = false;
						jbossQl.append("WHERE ");
						
					}
					
					jbossQl.append("pl.apPurchaseOrder.poPosted=1");
					
				}
				
				if (criteria.containsKey("supplierCode")) {
					
					if (!firstArgument) {
						
						jbossQl.append("AND ");	
						
					} else {
						
						firstArgument = false;
						jbossQl.append("WHERE ");
						
					}
					
					jbossQl.append("pl.apPurchaseOrder.apSupplier.splSupplierCode LIKE '%" + (String)criteria.get("supplierCode") + "%' ");
					
				}
				
				if (criteria.containsKey("category")) {
					
					if (!firstArgument) {		
						
						jbossQl.append("AND ");	
						
					} else {		       	 
						
						firstArgument = false;
						jbossQl.append("WHERE ");
						
					}
					
					jbossQl.append("pl.invItemLocation.invItem.iiAdLvCategory=?" + (ctr+1) + " ");
					obj[ctr] = (String)criteria.get("category");
					ctr++;
					
				}	
				
				if (criteria.containsKey("location")) {
					
					if (!firstArgument) {		
						
						jbossQl.append("AND ");		
						
					} else {		       	  	
						
						firstArgument = false;
						jbossQl.append("WHERE ");
						
					}
					
					jbossQl.append("pl.invItemLocation.invLocation.locName=?" + (ctr+1) + " ");
					obj[ctr] = (String)criteria.get("location");
					ctr++;
					
				}	
				
				if (criteria.containsKey("dateFrom")) {
					
					if (!firstArgument) {
						
						jbossQl.append("AND ");
						
					} else {
						
						firstArgument = false;
						jbossQl.append("WHERE ");
						
					}
					
					jbossQl.append("pl.apPurchaseOrder.poDate>=?" + (ctr+1) + " ");
					obj[ctr] = (Date)criteria.get("dateFrom");
					ctr++;
					
				}
				
				if (criteria.containsKey("dateTo")) {
					
					if (!firstArgument) {
						
						jbossQl.append("AND ");
						
					} else {
						
						firstArgument = false;
						jbossQl.append("WHERE ");
						
					}
					
					jbossQl.append("pl.apPurchaseOrder.poDate<=?" + (ctr+1) + " ");
					obj[ctr] = (Date)criteria.get("dateTo");
					ctr++;
					
				}
				
				if (criteria.containsKey("poReceivingDocumentNumberFrom")) {
					
					if (!firstArgument) {
						
						jbossQl.append("AND ");
						
					} else {
						
						firstArgument = false;
						jbossQl.append("WHERE ");
						
					}
					
					jbossQl.append("pl.apPurchaseOrder.poDocumentNumber>=?" + (ctr+1) + " ");
					obj[ctr] = (String)criteria.get("poReceivingDocumentNumberFrom");
					ctr++;
					
				}  
				
				if (criteria.containsKey("poReceivingDocumentNumberTo")) {
					
					if (!firstArgument) {
						
						jbossQl.append("AND ");
						
					} else {
						
						firstArgument = false;
						jbossQl.append("WHERE ");
						
					}
					
					jbossQl.append("pl.apPurchaseOrder.poDocumentNumber<=?" + (ctr+1) + " ");
					obj[ctr] = (String)criteria.get("poReceivingDocumentNumberTo");
					ctr++;
					
				}
				
				if(adBrnchList.isEmpty()) {
					
					throw new GlobalNoRecordFoundException();
					
				} else {
					
					if (!firstArgument) {
						
						jbossQl.append("AND ");
						
					} else {
						
						firstArgument = false;
						jbossQl.append("WHERE ");
						
					}	      
					
					jbossQl.append("pl.apPurchaseOrder.poAdBranch in (");
					
					boolean firstLoop = true;
					
					Iterator i = adBrnchList.iterator();
					
					while(i.hasNext()) {
						
						if(firstLoop == false) { 
							jbossQl.append(", "); 
						}
						else { 
							firstLoop = false; 
						}
						
						AdBranchDetails mdetails = (AdBranchDetails) i.next();
						
						jbossQl.append(mdetails.getBrCode());
						
					}
					
					jbossQl.append(") ");
					
					firstArgument = false;
					
				}
				
				jbossQl.append("AND pl.apPurchaseOrder.poReceiving = 1 AND pl.plAdCompany=" + AD_CMPNY + " ");  	  	  
				
				String orderBy = null;
				
				if (ORDER_BY.equals("DATE")) {
					
					orderBy = "pl.apPurchaseOrder.poDate";
					
				} else if (ORDER_BY.equals("ITEM NAME")) {
					
					orderBy = "pl.invItemLocation.invItem.iiName";
					
				} else if (ORDER_BY.equals("ITEM DESCRIPTION")) {
					
					orderBy = "pl.invItemLocation.invItem.iiDescription";
					
				} else if (ORDER_BY.equals("DOCUMENT NUMBER")) {
					
					orderBy = "pl.apPurchaseOrder.poDocumentNumber";
					
				} else if (ORDER_BY.equals("LOCATION")) {
					
					orderBy = "pl.invItemLocation.invLocation.locName";
					
				} 
				
				if (orderBy != null) {
					
					jbossQl.append("ORDER BY " + orderBy);
					
				}     
				
				Collection apReceivingItemLines = null;
				
				try {
					
					apReceivingItemLines = apReceivingItemLineHome.getPolByCriteria(jbossQl.toString(), obj);
					
				} catch (FinderException ex)	{

				} 	

				Iterator i = apReceivingItemLines.iterator();
				
				while (i.hasNext()) {
					
					LocalApPurchaseOrderLine apReceivingItemLine = (LocalApPurchaseOrderLine) i.next();
					
					ApRepReceivingItemsDetails details = new ApRepReceivingItemsDetails();
					
					details.setRrDate(apReceivingItemLine.getApPurchaseOrder().getPoDate());
					details.setRrItemName(apReceivingItemLine.getInvItemLocation().getInvItem().getIiName());
					details.setRrItemDescription(apReceivingItemLine.getInvItemLocation().getInvItem().getIiDescription());
					details.setRrLocation(apReceivingItemLine.getInvItemLocation().getInvLocation().getLocName());
					details.setRrSupplierName(apReceivingItemLine.getApPurchaseOrder().getApSupplier().getSplName());
					details.setRrDocNo(apReceivingItemLine.getApPurchaseOrder().getPoDocumentNumber());
					details.setRrUnit(apReceivingItemLine.getInvUnitOfMeasure().getUomName());
					details.setRrQuantity(apReceivingItemLine.getPlQuantity());
					details.setRrUnitCost(apReceivingItemLine.getPlUnitCost());
					details.setRrAmount(apReceivingItemLine.getPlAmount());
					details.setRrQcNumber(apReceivingItemLine.getPlQcNumber());
					details.setRrQcExpiryDate(apReceivingItemLine.getPlQcExpiryDate());
					details.setOrderBy(ORDER_BY);
					details.setRrRefNo(apReceivingItemLine.getApPurchaseOrder().getPoReferenceNumber());
					details.setRrDesc(apReceivingItemLine.getApPurchaseOrder().getPoDescription());
										
					apReceivingItemList.add(details);
					
				}
				
			}

			// voucher
			if (((String)criteria.get("includedVoucher")).equals("YES")) {
				
				int objSize = criteriaSize;
				
				if (criteria.containsKey("voucherDocumentNumberFrom")){
					
					objSize++;
					
				}
				
				if (criteria.containsKey("voucherDocumentNumberTo")){
					
					objSize++;
					
				}
				
				obj = new Object[objSize];						
					
				jbossQl = new StringBuffer();
				firstArgument = true;
				
				ctr = 0;
				
				jbossQl.append("SELECT OBJECT (vli) FROM ApVoucherLineItem vli ");
				
				if (criteria.containsKey("itemName")) {
					
					if (!firstArgument) {
						
						jbossQl.append("AND ");	
						
					} else {
						
						firstArgument = false;
						jbossQl.append("WHERE ");
						
					}
					
					jbossQl.append("vli.invItemLocation.invItem.iiName LIKE '%" + (String)criteria.get("itemName") + "%' ");
					
				}
				
				if (((String)criteria.get("includeUnposted")).equals("NO")){
					
					if (!firstArgument) {
						
						jbossQl.append("AND ");	
						
					} else {
						
						firstArgument = false;
						jbossQl.append("WHERE ");
						
					}
					
					jbossQl.append("vli.apVoucher.vouPosted=1");
					
				}
				
				if (criteria.containsKey("supplierCode")) {
					
					if (!firstArgument) {
						
						jbossQl.append("AND ");	
						
					} else {
						
						firstArgument = false;
						jbossQl.append("WHERE ");
						
					}
					
					jbossQl.append("vli.apVoucher.apSupplier.splSupplierCode LIKE '%" + (String)criteria.get("supplierCode") + "%' ");
					
				}
				
				if (criteria.containsKey("category")) {
					
					if (!firstArgument) {	
						
						jbossQl.append("AND ");	
						
					} else {		       	  
						
						firstArgument = false;
						jbossQl.append("WHERE ");	
						
					}
					
					jbossQl.append("vli.invItemLocation.invItem.iiAdLvCategory=?" + (ctr+1) + " ");
					obj[ctr] = (String)criteria.get("category");
					ctr++;
					
				}	
				
				if (criteria.containsKey("location")) {
					
					if (!firstArgument) {	
						
						jbossQl.append("AND ");		     
						
					} else {		       	  
						
						firstArgument = false;
						jbossQl.append("WHERE ");	
						
					}
					
					jbossQl.append("vli.invItemLocation.invLocation.locName=?" + (ctr+1) + " ");
					obj[ctr] = (String)criteria.get("location");
					ctr++;
					
				}	
				
				if (criteria.containsKey("dateFrom")) {
					
					if (!firstArgument) {
						
						jbossQl.append("AND ");
						
					} else {
						
						firstArgument = false;
						jbossQl.append("WHERE ");
						
					}
					
					jbossQl.append("vli.apVoucher.vouDate>=?" + (ctr+1) + " ");
					obj[ctr] = (Date)criteria.get("dateFrom");
					ctr++;
					
				}
				
				if (criteria.containsKey("dateTo")) {
					
					if (!firstArgument) {
						
						jbossQl.append("AND ");
						
					} else {
						
						firstArgument = false;
						jbossQl.append("WHERE ");
						
					}
					
					jbossQl.append("vli.apVoucher.vouDate<=?" + (ctr+1) + " ");
					obj[ctr] = (Date)criteria.get("dateTo");
					ctr++;
					
				}
				
				if (criteria.containsKey("voucherDocumentNumberFrom")) {
					
					if (!firstArgument) {
						
						jbossQl.append("AND ");
						
					} else {
						
						firstArgument = false;
						jbossQl.append("WHERE ");
						
					}
					
					jbossQl.append("vli.apVoucher.vouDocumentNumber>=?" + (ctr+1) + " ");
					obj[ctr] = (String)criteria.get("voucherDocumentNumberFrom");
					ctr++;
					
				}  
				
				if (criteria.containsKey("voucherDocumentNumberTo")) {
					
					if (!firstArgument) {
						
						jbossQl.append("AND ");
						
					} else {
						
						firstArgument = false;
						jbossQl.append("WHERE ");
						
					}
					
					jbossQl.append("vli.apVoucher.vouDocumentNumber<=?" + (ctr+1) + " ");
					obj[ctr] = (String)criteria.get("voucherDocumentNumberTo");
					ctr++;
					
				}
								
				if(adBrnchList.isEmpty()) {
					
					throw new GlobalNoRecordFoundException();
					
				} else {
					
					if (!firstArgument) {
						
						jbossQl.append("AND ");
						
					} else {
						
						firstArgument = false;
						jbossQl.append("WHERE ");
						
					}	      
					
					jbossQl.append("vli.apVoucher.vouAdBranch in (");
					
					boolean firstLoop = true;
					
					Iterator i = adBrnchList.iterator();
					
					while(i.hasNext()) {
						
						if(firstLoop == false) { 
							jbossQl.append(", "); 
						}
						else { 
							firstLoop = false; 
						}
						
						AdBranchDetails mdetails = (AdBranchDetails) i.next();
						
						jbossQl.append(mdetails.getBrCode());
						
					}
					
					jbossQl.append(") ");
					
					firstArgument = false;
					
				}
				
				jbossQl.append("AND vli.apVoucher.vouDebitMemo = 0 AND vli.vliAdCompany=" + AD_CMPNY + " ");  	  	  
				
				String orderBy = null;
				
				if (ORDER_BY.equals("DATE")) {
					
					orderBy = "vli.apVoucher.vouDate";
					
				} else if (ORDER_BY.equals("ITEM NAME")) {
					
					orderBy = "vli.invItemLocation.invItem.iiName";
					
				} else if (ORDER_BY.equals("ITEM DESCRIPTION")) {
					
					orderBy = "vli.invItemLocation.invItem.iiDescription";
					
				} else if (ORDER_BY.equals("DOCUMENT NUMBER")) {
					
					orderBy = "vli.apVoucher.vouDocumentNumber";
					
				} else if (ORDER_BY.equals("LOCATION")) {
					
					orderBy = "vli.invItemLocation.invLocation.locName";
					
				} 
				
				if (orderBy != null) {
					
					jbossQl.append("ORDER BY " + orderBy);
					
				}     
				
				Collection apVoucherLineItems = null;
				
				try {
					
					apVoucherLineItems = apVoucherLineItemHome.getVliByCriteria(jbossQl.toString(), obj);
					
				} catch (FinderException ex)	{

				} 	
				
				Iterator i = apVoucherLineItems.iterator();
				
				while (i.hasNext()) {
					
					LocalApVoucherLineItem apVoucherLineItem = (LocalApVoucherLineItem) i.next();
					
					ApRepReceivingItemsDetails details = new ApRepReceivingItemsDetails();
					
					details.setRrDate(apVoucherLineItem.getApVoucher().getVouDate());
					details.setRrItemName(apVoucherLineItem.getInvItemLocation().getInvItem().getIiName());
					details.setRrItemDescription(apVoucherLineItem.getInvItemLocation().getInvItem().getIiDescription());
					details.setRrLocation(apVoucherLineItem.getInvItemLocation().getInvLocation().getLocName());
					details.setRrSupplierName(apVoucherLineItem.getApVoucher().getApSupplier().getSplName());
					details.setRrDocNo(apVoucherLineItem.getApVoucher().getVouDocumentNumber());
					details.setRrUnit(apVoucherLineItem.getInvUnitOfMeasure().getUomName());
					details.setRrUnitCost(apVoucherLineItem.getVliUnitCost());
					details.setRrAmount(apVoucherLineItem.getVliAmount());
					
					details.setOrderBy(ORDER_BY);
										
					double quantity = apVoucherLineItem.getVliQuantity();
					
					// get debit memo
					Collection apDebitMemoLineItems = apVoucherLineItemHome.findByVouDebitMemoAndVouPostedVouDmVoucherNumberAndIlCodeAndBrCode(
							EJBCommon.TRUE, EJBCommon.TRUE, apVoucherLineItem.getApVoucher().getVouDocumentNumber(), 
							apVoucherLineItem.getInvItemLocation().getIlCode(), 
							apVoucherLineItem.getApVoucher().getVouAdBranch(), AD_CMPNY);
					
					Iterator dmItr = apDebitMemoLineItems.iterator();
					
					while(dmItr.hasNext()){
					    
						LocalApVoucherLineItem apDebitMemoLineItem = (LocalApVoucherLineItem)dmItr.next();
						
						double convertedQty =  apDebitMemoLineItem.getVliQuantity() *
							(apVoucherLineItem.getInvUnitOfMeasure().getUomConversionFactor()/
							        apDebitMemoLineItem.getInvUnitOfMeasure().getUomConversionFactor());

						quantity =  quantity - convertedQty;

					}
					
					if(!((String)criteria.get("includeUnposted")).equals("NO")) {
					    
					    apDebitMemoLineItems = apVoucherLineItemHome.findByVouDebitMemoAndVouPostedVouDmVoucherNumberAndIlCodeAndBrCode(
								EJBCommon.TRUE, EJBCommon.FALSE, apVoucherLineItem.getApVoucher().getVouDocumentNumber(), 
								apVoucherLineItem.getInvItemLocation().getIlCode(), 
								apVoucherLineItem.getApVoucher().getVouAdBranch(), AD_CMPNY);
						
						dmItr = apDebitMemoLineItems.iterator();
						
						while(dmItr.hasNext()){
						    
							LocalApVoucherLineItem apDebitMemoLineItem = (LocalApVoucherLineItem)dmItr.next();
							
							double convertedQty =  this.convertByUomFromAndUomToAndInvItemAndQuantity(
								apDebitMemoLineItem.getInvUnitOfMeasure(), apVoucherLineItem.getInvUnitOfMeasure(),
								apVoucherLineItem.getInvItemLocation().getInvItem(),
								apDebitMemoLineItem.getVliQuantity(), AD_CMPNY);

							quantity =  quantity - convertedQty;
														
						}
						
					}
					
					details.setRrQuantity(quantity);
					details.setRrRefNo(apVoucherLineItem.getApVoucher().getVouReferenceNumber());
					details.setRrDesc(apVoucherLineItem.getApVoucher().getVouDescription());
															
					apReceivingItemList.add(details);
					
				}
				
			}
			
			// Check
			if(((String)criteria.get("includedDirectCheck")).equals("YES")) {
				
				int objSize = criteriaSize;
				
				if (criteria.containsKey("checkDocumentNumberFrom")){
					
					objSize++;
					
				}
				
				if (criteria.containsKey("checkDocumentNumberTo")){
					
					objSize++;
					
				}
				
				obj = new Object[objSize];						
				
				jbossQl = new StringBuffer();
				firstArgument = true;
				
				ctr = 0;
				
				jbossQl.append("SELECT OBJECT (vli) FROM ApVoucherLineItem vli ");
				
				if (criteria.containsKey("itemName")) {
					
					if (!firstArgument) {
						
						jbossQl.append("AND ");	
						
					} else {
						
						firstArgument = false;
						jbossQl.append("WHERE ");
						
					}
					
					jbossQl.append("vli.invItemLocation.invItem.iiName LIKE '%" + (String)criteria.get("itemName") + "%' ");
					
				}
				
				if (((String)criteria.get("includeUnposted")).equals("NO")){
					
					if (!firstArgument) {
						
						jbossQl.append("AND ");	
						
					} else {
						
						firstArgument = false;
						jbossQl.append("WHERE ");
						
					}
					
					jbossQl.append("vli.apCheck.chkPosted=1");
					
				}
				
				if (criteria.containsKey("supplierCode")) {
					
					if (!firstArgument) {
						
						jbossQl.append("AND ");	
						
					} else {
						
						firstArgument = false;
						jbossQl.append("WHERE ");
						
					}
					
					jbossQl.append("vli.apCheck.apSupplier.splSupplierCode LIKE '%" + (String)criteria.get("supplierCode") + "%' ");
					
				}
				
				if (criteria.containsKey("category")) {
					
					if (!firstArgument) {	
						
						jbossQl.append("AND ");		
						
					} else {		       	  	
						
						firstArgument = false;
						jbossQl.append("WHERE ");	
						
					}
					
					jbossQl.append("vli.invItemLocation.invItem.iiAdLvCategory=?" + (ctr+1) + " ");
					obj[ctr] = (String)criteria.get("category");
					ctr++;
					
				}	
				
				if (criteria.containsKey("location")) {
					
					if (!firstArgument) {
						
						jbossQl.append("AND ");		
						
					} else {		       	  	
						
						firstArgument = false;
						jbossQl.append("WHERE ");
						
					}
					
					jbossQl.append("vli.invItemLocation.invLocation.locName=?" + (ctr+1) + " ");
					obj[ctr] = (String)criteria.get("location");
					ctr++;
					
				}	
				
				if (criteria.containsKey("dateFrom")) {
					
					if (!firstArgument) {
						
						jbossQl.append("AND ");
						
					} else {
						
						firstArgument = false;
						jbossQl.append("WHERE ");
						
					}
					
					jbossQl.append("vli.apCheck.chkDate>=?" + (ctr+1) + " ");
					obj[ctr] = (Date)criteria.get("dateFrom");
					ctr++;
					
				}
				
				if (criteria.containsKey("dateTo")) {
					
					if (!firstArgument) {
						
						jbossQl.append("AND ");
						
					} else {
						
						firstArgument = false;
						jbossQl.append("WHERE ");
						
					}
					
					jbossQl.append("vli.apCheck.chkDate<=?" + (ctr+1) + " ");
					obj[ctr] = (Date)criteria.get("dateTo");
					ctr++;
					
				}
				
				if (criteria.containsKey("checkDocumentNumberFrom")) {
					
					if (!firstArgument) {
						
						jbossQl.append("AND ");
						
					} else {
						
						firstArgument = false;
						jbossQl.append("WHERE ");
						
					}
					
					jbossQl.append("vli.apCheck.chkDocumentNumber>=?" + (ctr+1) + " ");
					obj[ctr] = (String)criteria.get("checkDocumentNumberFrom");
					ctr++;
					
				}  
				
				if (criteria.containsKey("checkDocumentNumberTo")) {
					
					if (!firstArgument) {
						
						jbossQl.append("AND ");
						
					} else {
						
						firstArgument = false;
						jbossQl.append("WHERE ");
						
					}
					
					jbossQl.append("vli.apCheck.chkDocumentNumber<=?" + (ctr+1) + " ");
					obj[ctr] = (String)criteria.get("checkDocumentNumberTo");
					ctr++;
					
				}
				
				if(adBrnchList.isEmpty()) {
					
					throw new GlobalNoRecordFoundException();
					
				} else {
					
					if (!firstArgument) {
						
						jbossQl.append("AND ");
						
					} else {
						
						firstArgument = false;
						jbossQl.append("WHERE ");
						
					}	      
					
					jbossQl.append("vli.apCheck.chkAdBranch in (");
					
					boolean firstLoop = true;
					
					Iterator i = adBrnchList.iterator();
					
					while(i.hasNext()) {
						
						if(firstLoop == false) { 
							jbossQl.append(", "); 
						}
						else { 
							firstLoop = false; 
						}
						
						AdBranchDetails mdetails = (AdBranchDetails) i.next();
						
						jbossQl.append(mdetails.getBrCode());
						
					}
					
					jbossQl.append(") ");
					
					firstArgument = false;
					
				}
				
				jbossQl.append("AND vli.apCheck.chkType = 'DIRECT' AND vli.vliAdCompany=" + AD_CMPNY + " ");  	  	  
				
				String orderBy = null;
				
				if (ORDER_BY.equals("DATE")) {
					
					orderBy = "vli.apCheck.chkDate";
					
				} else if (ORDER_BY.equals("ITEM NAME")) {
					
					orderBy = "vli.invItemLocation.invItem.iiName";
					
				} else if (ORDER_BY.equals("ITEM DESCRIPTION")) {
					
					orderBy = "vli.invItemLocation.invItem.iiDescription";
					
				} else if (ORDER_BY.equals("DOCUMENT NUMBER")) {
					
					orderBy = "vli.apCheck.chkDocumentNumber";
					
				} else if (ORDER_BY.equals("LOCATION")) {
					
					orderBy = "vli.invItemLocation.invLocation.locName";
					
				} 
				
				if (orderBy != null) {
					
					jbossQl.append("ORDER BY " + orderBy);
					
				}     
				
				Collection apReceivingItemLines = null;
				
				try {
					
					apReceivingItemLines = apVoucherLineItemHome.getVliByCriteria(jbossQl.toString(), obj);
					
				} catch (FinderException ex)	{

				} 	

				Iterator i = apReceivingItemLines.iterator();
				
				while (i.hasNext()) {
					
					LocalApVoucherLineItem apVoucherLineItem = (LocalApVoucherLineItem) i.next();
					
					ApRepReceivingItemsDetails details = new ApRepReceivingItemsDetails();
					
					details.setRrDate(apVoucherLineItem.getApCheck().getChkDate());
					details.setRrItemName(apVoucherLineItem.getInvItemLocation().getInvItem().getIiName());
					details.setRrItemDescription(apVoucherLineItem.getInvItemLocation().getInvItem().getIiDescription());
					details.setRrLocation(apVoucherLineItem.getInvItemLocation().getInvLocation().getLocName());
					details.setRrSupplierName(apVoucherLineItem.getApCheck().getApSupplier().getSplName());
					details.setRrDocNo(apVoucherLineItem.getApCheck().getChkDocumentNumber());
					details.setRrUnit(apVoucherLineItem.getInvUnitOfMeasure().getUomName());
					details.setRrQuantity(apVoucherLineItem.getVliQuantity());
					details.setRrUnitCost(apVoucherLineItem.getVliUnitCost());
					details.setRrAmount(apVoucherLineItem.getVliAmount());
					
					details.setOrderBy(ORDER_BY);
					details.setRrRefNo(apVoucherLineItem.getApCheck().getChkReferenceNumber());
					details.setRrDesc(apVoucherLineItem.getApCheck().getChkDescription());
							
					apReceivingItemList.add(details);
					
				}
			}
			
			// sort
			
			if(GROUP_BY.equals("SUPPLIER CODE")) {
				
				Collections.sort(apReceivingItemList, ApRepReceivingItemsDetails.SupplierNameComparator);
				
			} else if(GROUP_BY.equals("ITEM CODE")) {
				
				Collections.sort(apReceivingItemList, ApRepReceivingItemsDetails.ItemNameComparator);
				
			} else if(GROUP_BY.equals("DATE")) {
				
				Collections.sort(apReceivingItemList, ApRepReceivingItemsDetails.DateComparator);
				
			} else {
				
				Collections.sort(apReceivingItemList, ApRepReceivingItemsDetails.NoGroupComparator);
				
			}
			
			if (apReceivingItemList.isEmpty())
				throw new GlobalNoRecordFoundException ();
			
			return apReceivingItemList;	  
			
		} catch (GlobalNoRecordFoundException ex) {
			
			throw ex;
			
		} catch (Exception ex) {
			
			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());
			
		}
	}

	/**
	* @ejb:interface-method view-type="remote"
	* @jboss:method-attributes read-only="true"
	**/
	public com.util.AdCompanyDetails getAdCompany(Integer AD_CMPNY) {
	
	  Debug.print("ApRepReceivingItemsControllerBean getAdCompany");      
	  
	  LocalAdCompanyHome adCompanyHome = null;
	        
	  // Initialize EJB Home
	    
	  try {
	        
	       adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
	           lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
	        
	  } catch (NamingException ex) {
	        
	      throw new EJBException(ex.getMessage());
	        
	  }
	
	  try {
	      
	     LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
	     
	     AdCompanyDetails details = new AdCompanyDetails();
	     details.setCmpName(adCompany.getCmpName());
	     
	     return details;  	
	  	       	
	  } catch (Exception ex) {
	  	
	  	 Debug.printStackTrace(ex);
	  	 throw new EJBException(ex.getMessage());
	  	  
	  }
	  
	}
	
	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/   
	public ArrayList getAdBrResAll(Integer RS_CODE, Integer AD_CMPNY) 
		throws GlobalNoRecordFoundException{
	    
	    Debug.print("ApRepReceivingItemsControllerBean getAdBrResAll");
	    
	    LocalAdBranchResponsibilityHome adBranchResponsibilityHome = null;
	    LocalAdBranchHome adBranchHome = null;
	    
	    LocalAdBranchResponsibility adBranchResponsibility = null;
	    LocalAdBranch adBranch = null;
	    
	    Collection adBranchResponsibilities = null;
	    
	    ArrayList list = new ArrayList();
	    
	    // Initialize EJB Home
	    
	    try {
	        
	        adBranchResponsibilityHome = (LocalAdBranchResponsibilityHome)EJBHomeFactory.
	        lookUpLocalHome(LocalAdBranchResponsibilityHome.JNDI_NAME, LocalAdBranchResponsibilityHome.class);
	        adBranchHome = (LocalAdBranchHome)EJBHomeFactory.
	        lookUpLocalHome(LocalAdBranchHome.JNDI_NAME, LocalAdBranchHome.class);
	        
	    } catch (NamingException ex) {
	        
	        throw new EJBException(ex.getMessage());
	        
	    }
	    
	    try {
	        
	        adBranchResponsibilities = adBranchResponsibilityHome.findByAdResponsibility(RS_CODE, AD_CMPNY);
	        
	    } catch (FinderException ex) {
	        
	    } catch (Exception ex) {
	        
	        throw new EJBException(ex.getMessage());
	    }
	    
	    if (adBranchResponsibilities.isEmpty()) {
	        
	        throw new GlobalNoRecordFoundException();
	        
	    }
	    
	    try {
	        
	        Iterator i = adBranchResponsibilities.iterator();
	        
	        while(i.hasNext()) {
	            
	            adBranchResponsibility = (LocalAdBranchResponsibility)i.next();
	            
	            adBranch = adBranchResponsibility.getAdBranch();
	            
	            AdBranchDetails details = new AdBranchDetails();
	            
	            details.setBrCode(adBranch.getBrCode());
	            details.setBrBranchCode(adBranch.getBrBranchCode());
	            details.setBrName(adBranch.getBrName());
	            details.setBrHeadQuarter(adBranch.getBrHeadQuarter());
	            
	            list.add(details);
	            
	        }	               
	        
	    } catch (Exception ex) {
	        
	        throw new EJBException(ex.getMessage());
	    }
	    
	    return list;
	    
	}    
	
	private double convertByUomFromAndUomToAndInvItemAndQuantity(LocalInvUnitOfMeasure invFromUnitOfMeasure, LocalInvUnitOfMeasure invToUnitOfMeasure, LocalInvItem invItem, double QTY, Integer AD_CMPNY) {
		
		Debug.print("ApRepReceivingItemsControllerBean convertByUomFromAndUomToAndInvItemAndQuantity");		        
		
		LocalAdPreferenceHome adPreferenceHome = null;
		LocalInvUnitOfMeasureConversionHome invUnitOfMeasureConversionHome = null;
		
		// Initialize EJB Home
		
		try {
			
			adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);
            invUnitOfMeasureConversionHome = (LocalInvUnitOfMeasureConversionHome)EJBHomeFactory.
    		lookUpLocalHome(LocalInvUnitOfMeasureConversionHome.JNDI_NAME, LocalInvUnitOfMeasureConversionHome.class);   

			
		} catch (NamingException ex) {
			
			throw new EJBException(ex.getMessage());
			
		}
		
		try {
			
			LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);               	          	        	
			
            LocalInvUnitOfMeasureConversion invFromUnitOfMeasureConversion =
            	invUnitOfMeasureConversionHome.findUmcByIiNameAndUomName(invItem.getIiName(),
            	invFromUnitOfMeasure.getUomName(), AD_CMPNY);
            LocalInvUnitOfMeasureConversion invToUnitOfMeasureConversion =
            	invUnitOfMeasureConversionHome.findUmcByIiNameAndUomName(invItem.getIiName(),
            	invToUnitOfMeasure.getUomName(), AD_CMPNY);

            return EJBCommon.roundIt(QTY * invToUnitOfMeasureConversion.getUmcConversionFactor() /
            	invFromUnitOfMeasureConversion.getUmcConversionFactor(), adPreference.getPrfInvQuantityPrecisionUnit());       	        		        		       	
			
		} catch (Exception ex) {
			
			Debug.printStackTrace(ex);
			getSessionContext().setRollbackOnly();
			throw new EJBException(ex.getMessage());
			
		}
		
	}

	// SessionBean methods
	
	/**
* @ejb:create-method view-type="remote"
**/
	public void ejbCreate() throws CreateException {
		
		Debug.print("ApRepReceivingItemsControllerBean ejbCreate");
		
	}
	
}

