/*
 * ApRepReceivingReportPrintControllerBean.java
 *
 * Created on April 19, 2005, 05:28 PM
 *
 * @author  Ma. Jennifer G. Manuel
 */

package com.ejb.txn;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;
import javax.naming.NamingException;

import com.ejb.ad.LocalAdApproval;
import com.ejb.ad.LocalAdApprovalDocument;
import com.ejb.ad.LocalAdApprovalDocumentHome;
import com.ejb.ad.LocalAdApprovalHome;
import com.ejb.ad.LocalAdBranch;
import com.ejb.ad.LocalAdBranchHome;
import com.ejb.ad.LocalAdCompany;
import com.ejb.ad.LocalAdCompanyHome;
import com.ejb.ad.LocalAdPreference;
import com.ejb.ad.LocalAdPreferenceHome;
import com.ejb.ad.LocalAdUser;
import com.ejb.ad.LocalAdUserHome;
import com.ejb.ap.LocalApDistributionRecord;
import com.ejb.ap.LocalApDistributionRecordHome;
import com.ejb.ap.LocalApPurchaseOrder;
import com.ejb.ap.LocalApPurchaseOrderHome;
import com.ejb.ap.LocalApPurchaseOrderLine;
import com.ejb.ap.LocalApPurchaseOrderLineHome;
import com.ejb.ar.LocalArDistributionRecordHome;
import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.inv.LocalInvCosting;
import com.ejb.inv.LocalInvCostingHome;
import com.ejb.inv.LocalInvItem;
import com.ejb.inv.LocalInvItemHome;
import com.ejb.inv.LocalInvTag;
import com.ejb.inv.LocalInvTagHome;

import com.util.AbstractSessionBean;
import com.util.AdCompanyDetails;
import com.util.ApModDistributionRecordDetails;
import com.util.ApRepPurchaseOrderPrintDetails;
import com.util.ApRepReceivingReportPrintDetails;
import com.util.Debug;
import com.util.EJBCommon;
import com.util.EJBHomeFactory;
import com.util.InvItemDetails;

/**
 * @ejb:bean name="ApRepReceivingReportPrintControllerEJB"
 *           display-name="Used for printing receiving report transactions"
 *           type="Stateless"
 *           view-type="remote"
 *           jndi-name="ejb/ApRepReceivingReportPrintControllerEJB"
 *
 * @ejb:interface remote-class="com.ejb.txn.ApRepReceivingReportPrintController"
 *                extends="javax.ejb.EJBObject"
 *
 * @ejb:home remote-class="com.ejb.txn.ApRepReceivingReportPrintControllerHome"
 *           extends="javax.ejb.EJBHome"
 *
 * @ejb:transaction type="Required"
 *
*/

public class ApRepReceivingReportPrintControllerBean extends AbstractSessionBean {

	/**
     * @ejb:interface-method view-type="remote"
     **/
    public ArrayList executeApRepReceivingReportPrintSub(ArrayList poCodeList, Integer AD_CMPNY)
        throws GlobalNoRecordFoundException {

		Debug.print("ApRepReceivingReportPrintControllerBean executeApRepReceivingReportPrintSub");

		LocalApPurchaseOrderHome apPurchaseOrderHome = null;
        LocalAdApprovalHome adApprovalHome = null;
        LocalAdApprovalDocumentHome adApprovalDocumentHome = null;
        LocalApDistributionRecordHome apDistributionRecordHome = null;


        ArrayList list = new ArrayList();

        // Initialize EJB Home

        try {

        	apPurchaseOrderHome = (LocalApPurchaseOrderHome)EJBHomeFactory.
            	lookUpLocalHome(LocalApPurchaseOrderHome .JNDI_NAME, LocalApPurchaseOrderHome .class);
            adApprovalHome = (LocalAdApprovalHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdApprovalHome.JNDI_NAME, LocalAdApprovalHome.class);
            adApprovalDocumentHome = (LocalAdApprovalDocumentHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdApprovalDocumentHome.JNDI_NAME, LocalAdApprovalDocumentHome.class);
            apDistributionRecordHome = (LocalApDistributionRecordHome)EJBHomeFactory.
        		lookUpLocalHome(LocalApDistributionRecordHome.JNDI_NAME, LocalApDistributionRecordHome.class);


        } catch (NamingException ex) {

            throw new EJBException(ex.getMessage());

        }

        try {

        	Iterator i = poCodeList.iterator();

        	while (i.hasNext()) {

        		Integer PO_CODE = (Integer) i.next();

        		LocalApPurchaseOrder apPurchaseOrder = null;

        		try {

        			apPurchaseOrder = apPurchaseOrderHome.findByPrimaryKey(PO_CODE);

        		} catch (FinderException ex) {

        			continue;

        		}

        		// get total vat of all receipt

        		Collection apDistributionRecords = apDistributionRecordHome.findByChkCode(apPurchaseOrder.getPoCode(), AD_CMPNY);

        		Iterator j = apDistributionRecords.iterator();

        		while(j.hasNext()) {

        			LocalApDistributionRecord apDistributionRecord = (LocalApDistributionRecord)j.next();

        			ApModDistributionRecordDetails mdetails = new ApModDistributionRecordDetails();

        			mdetails.setDrCoaAccountNumber(apDistributionRecord.getGlChartOfAccount().getCoaAccountNumber());
        			mdetails.setDrCoaAccountDescription(apDistributionRecord.getGlChartOfAccount().getCoaAccountDescription());
        			mdetails.setDrDebit(apDistributionRecord.getDrDebit());
        			mdetails.setDrAmount(apDistributionRecord.getDrAmount());
        			mdetails.setDrClass(apDistributionRecord.getDrClass());
        			System.out.println("mdetails: " + mdetails.getDrCoaAccountDescription());
        			list.add(mdetails);

        		}

        	}

        	if (list.isEmpty()) {

        		throw new GlobalNoRecordFoundException();

        	}

        	//if (isMiscReceiptItems) {

        		//Collections.sort(list, ArModReceiptDetails.sortByItemCategory);
        		//Collections.sort(list, ArModReceiptDetails.sortByReceiptNumber);

        	//}
        	return list;

        } catch (GlobalNoRecordFoundException ex) {

        	throw ex;

        } catch (Exception ex) {

        	ex.printStackTrace();
        	throw new EJBException(ex.getMessage());

        }
    }

    /**
     * @ejb:interface-method view-type="remote"
     **/
    public ArrayList executeApRepReceivingReportPrint(ArrayList poCodeList, String USR_NM, Integer AD_BRNCH, Integer AD_CMPNY, String reportType)
        throws GlobalNoRecordFoundException {

        Debug.print("ApRepReceivingReportPrintControllerBean executeApRepReceivingReportPrint");

        LocalApPurchaseOrderHome apPurchaseOrderHome = null;
        LocalInvCostingHome invCostingHome = null;
        LocalAdPreferenceHome adPreferenceHome = null;
        LocalApPurchaseOrderLineHome apPurchaseOrderLineHome = null;

        LocalAdBranchHome adBranchHome = null;
        LocalInvItemHome invItemHome = null;
        LocalInvTagHome invTagHome = null;

        LocalAdUserHome adUserHome = null;
        ArrayList list = new ArrayList();

        // Initialize EJB Home

        try {

        	apPurchaseOrderHome = (LocalApPurchaseOrderHome)EJBHomeFactory.
                lookUpLocalHome(LocalApPurchaseOrderHome .JNDI_NAME, LocalApPurchaseOrderHome .class);
        	invCostingHome = (LocalInvCostingHome)EJBHomeFactory.
            	lookUpLocalHome(LocalInvCostingHome .JNDI_NAME, LocalInvCostingHome .class);
        	adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
        		lookUpLocalHome(LocalAdPreferenceHome .JNDI_NAME, LocalAdPreferenceHome .class);

        	apPurchaseOrderLineHome = (LocalApPurchaseOrderLineHome)EJBHomeFactory.
    			lookUpLocalHome(LocalApPurchaseOrderLineHome .JNDI_NAME, LocalApPurchaseOrderLineHome .class);
        	adBranchHome = (LocalAdBranchHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdBranchHome.JNDI_NAME, LocalAdBranchHome.class);
        	invItemHome = (LocalInvItemHome)EJBHomeFactory.
    				lookUpLocalHome(LocalInvItemHome.JNDI_NAME, LocalInvItemHome.class);
        	invTagHome = (LocalInvTagHome)EJBHomeFactory.
    				lookUpLocalHome(LocalInvTagHome.JNDI_NAME, LocalInvTagHome.class);

        	adUserHome = (LocalAdUserHome)EJBHomeFactory.
    				lookUpLocalHome(LocalAdUserHome.JNDI_NAME, LocalAdUserHome.class);

        } catch (NamingException ex) {

            throw new EJBException(ex.getMessage());

        }

        try {


        	LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);

        	Iterator i = poCodeList.iterator();

        	while (i.hasNext()) {

        		Integer PO_CODE = (Integer) i.next();

        		LocalApPurchaseOrder apPurchaseOrder = null;


        		try {

        			apPurchaseOrder = apPurchaseOrderHome.findByPrimaryKey(PO_CODE);

        		} catch (FinderException ex) {

        			continue;

        		}

        		String DFLT_APPRVR = null;

        		// get purchase order lines

        		Collection apPurchaseOrderLines = apPurchaseOrder.getApPurchaseOrderLines();

        		Iterator ilIter = apPurchaseOrderLines.iterator();

        		while (ilIter.hasNext()) {

        			LocalApPurchaseOrderLine apPurchaseOrderLine = (LocalApPurchaseOrderLine)ilIter.next();

        			String II_NM = apPurchaseOrderLine.getInvItemLocation().getInvItem().getIiName();
        			String LOC_NM = apPurchaseOrderLine.getInvItemLocation().getInvLocation().getLocName();
        			boolean isService = apPurchaseOrderLine.getInvItemLocation().getInvItem().getIiAdLvCategory().startsWith("Service");
        			String userDepartment = this.getAdUsrDepartment(USR_NM, AD_CMPNY);

        			if(!apPurchaseOrderLine.getInvTags().isEmpty() && reportType.equals("NormalFormat" )
        					&& apPurchaseOrderLine.getInvItemLocation().getInvItem().getIiTraceMisc()==1){
    					System.out.println("invTags is not empty");


    					ApRepReceivingReportPrintDetails details = new ApRepReceivingReportPrintDetails();
						if (isService){
            				System.out.println(isService + " <== service ba?");
            				details.setRrpPlIsService(true);
            				System.out.println(details.getRrpPlIsService() + " <== details.getRrpPlIsService");
            			}
            			else {
            				System.out.println(isService + " <== o hindi?");
            				details.setRrpPlIsService(false);
            				System.out.println(details.getRrpPlIsService() + " <== details.getRrpPlIsService");
            			}
            			if (apPurchaseOrderLine.getInvItemLocation().getInvItem().getIiNonInventoriable()==1){
            				details.setRrpPlIsInventoriable(false);
            			}else{
            				details.setRrpPlIsInventoriable(true);
            			}
            			details.setRrpPoType(apPurchaseOrder.getPoType());
            			details.setRrpPoDate(apPurchaseOrder.getPoDate());
            			details.setRrpPoDocumentNumber(apPurchaseOrder.getPoDocumentNumber());
            			details.setRrpPoReferenceNumber(apPurchaseOrder.getPoReferenceNumber());
            			details.setRrpPoCreatedBy(apPurchaseOrder.getPoCreatedBy());
            			details.setRrpPaymentTerm(apPurchaseOrder.getAdPaymentTerm().getPytName());
            			//details.setRrpFixedAssetReceiving(apPurchaseOrder.getPoFixedAssetReceiving());
            			details.setRrpPoDepartment(userDepartment);


            			if(apPurchaseOrder.getPoApprovedRejectedBy() != null && !apPurchaseOrder.getPoApprovedRejectedBy().equals("")) {

            				details.setRrpPoApprovedRejectedBy(apPurchaseOrder.getPoApprovedRejectedBy());

            			} else {

            				details.setRrpPoApprovedRejectedBy(adPreference.getPrfApDefaultApprover());

            			}

            			details.setRrpPoRcvPoNumber(apPurchaseOrder.getPoRcvPoNumber());
            			details.setRrpPoDescription(apPurchaseOrder.getPoDescription());
            			details.setRrpPllIiName(II_NM);
            			details.setRrpPlLocName(LOC_NM);
            			details.setRrpPlLocPosition(apPurchaseOrderLine.getInvItemLocation().getInvLocation().getLocPosition());
            			details.setRrpPlLocBranch(apPurchaseOrderLine.getInvItemLocation().getInvLocation().getLocBranch());
            			details.setRrpPlLocDepartment(apPurchaseOrderLine.getInvItemLocation().getInvLocation().getLocDepartment());
            			details.setRrpPlLocDateHired(apPurchaseOrderLine.getInvItemLocation().getInvLocation().getLocDateHired());
            			details.setRrpPlLocEmploymentStatus(apPurchaseOrderLine.getInvItemLocation().getInvLocation().getLocEmploymentStatus());
            			details.setRrpPlIiRemarks(apPurchaseOrderLine.getInvItemLocation().getInvItem().getIiRemarks());
            			details.setRrpPlUomName(apPurchaseOrderLine.getInvUnitOfMeasure().getUomName());
            			//details.setRrpPlItemBarcode(apPurchaseOrderLine.getInvItemLocation().getInvItem().getIiPartNumber());

            			details.setRrpPlQuantity(apPurchaseOrderLine.getPlQuantity());
            			details.setRrpPlUnitCost(apPurchaseOrderLine.getPlUnitCost());
            			details.setRrpPlAmount(apPurchaseOrderLine.getPlAmount());


            			//double NT_AMNT = this.calculatePlNetAmount(apPurchaseOrderLine.getPlAmount(), apPurchaseOrder.getApTaxCode().getTcRate(),
            			//	apPurchaseOrder.getApTaxCode().getTcType(),this.getGlFcPrecisionUnit(AD_CMPNY)) ;

            			//details.setRrpPlTaxAmount(this.calculatePlTaxAmount(apPurchaseOrderLine.getPlAmount(), apPurchaseOrder.getApTaxCode().getTcRate(),
                			//	apPurchaseOrder.getApTaxCode().getTcType(), NT_AMNT, this.getGlFcPrecisionUnit(AD_CMPNY)));
            			details.setRrpPlTaxAmount(apPurchaseOrderLine.getPlTaxAmount());
            			details.setRrpPlItemDescription(apPurchaseOrderLine.getInvItemLocation().getInvItem().getIiDescription());
            			details.setRrpPoSupplierName(apPurchaseOrderLine.getApPurchaseOrder().getApSupplier().getSplName());
            			details.setRrpPoSupplierAddress(apPurchaseOrder.getApSupplier().getSplAddress());
            			details.setRrpPoContactPerson(apPurchaseOrderLine.getApPurchaseOrder().getApSupplier().getSplContact());
            			details.setRrpPoContactNumber(apPurchaseOrderLine.getApPurchaseOrder().getApSupplier().getSplPhone());
            			details.setRrpPoCheckedBy(adPreference.getPrfApDefaultChecker());

            			details.setRrpPoTaxRate(apPurchaseOrder.getApTaxCode().getTcRate());


            			// get unit cost wo vat
            			details.setRrpPlUnitCostWoTax(calculatePlNetUnitCost(details,
            					apPurchaseOrder.getApTaxCode().getTcRate(), apPurchaseOrder.getApTaxCode().getTcType(),
            					this.getGlFcPrecisionUnit(AD_CMPNY)));

            			// get unit cost vat inclusive
            			details.setRrpPlUnitCostTaxInclusive(calculatePlUnitCostTaxInclusive(details,
                				apPurchaseOrder.getApTaxCode().getTcRate(), apPurchaseOrder.getApTaxCode().getTcType(),
    							this.getGlFcPrecisionUnit(AD_CMPNY)));

//            			Include Branch
        				LocalAdBranch adBranch = adBranchHome.findByPrimaryKey(apPurchaseOrder.getPoAdBranch());
        				details.setBranchCode(adBranch.getBrBranchCode());
        				details.setBranchName(adBranch.getBrName());

        				if(apPurchaseOrderLine.getApPurchaseOrder().getPoType().equals("PO MATCHED")) {

    	        			try {

    	        				LocalApPurchaseOrder existingApPurchaseOrder = apPurchaseOrderHome.findByPoDocumentNumberAndBrCode(apPurchaseOrderLine.getApPurchaseOrder().getPoRcvPoNumber(), AD_BRNCH, AD_CMPNY);
    	        				details.setRrpPoRcvPoDate(existingApPurchaseOrder.getPoDate());

    	        			} catch(FinderException ex) {

    	        			}

    	        			try {

    	        				LocalApPurchaseOrderLine existingApPurchaseOrderLine = apPurchaseOrderLineHome.findByPrimaryKey(apPurchaseOrderLine.getPlPlCode());
    	        				details.setRrpPlQuantityOrdered(existingApPurchaseOrderLine.getPlQuantity());

    	        			} catch(FinderException ex) {

    	        			}

            			}

            			LocalInvCosting invCosting = null;

            			try {

            				invCosting = invCostingHome.getByMaxCstDateToLongAndMaxCstLineNumberAndIiNameAndLocName(II_NM, LOC_NM, AD_BRNCH, AD_CMPNY);
            				details.setRrpPlEndingBalance(apPurchaseOrderLine.getPlAmount() + invCosting.getCstRemainingValue());

            			} catch(FinderException ex) {
            				details.setRrpPlEndingBalance(apPurchaseOrderLine.getPlAmount());
            			}


            			System.out.println("new code");
						StringBuilder strBProperty = new StringBuilder();
						StringBuilder strBSerial = new StringBuilder();
						StringBuilder strBSpecs = new StringBuilder();
						StringBuilder strBCustodian= new StringBuilder();
						StringBuilder strBExpirationDate = new StringBuilder();
						Iterator it = apPurchaseOrderLine.getInvTags().iterator();

    					Collection invTags = apPurchaseOrderLine.getInvTags();
    					Iterator tags =  invTags.iterator();
    					while (tags.hasNext()){
    						/*

    						LocalInvTag invTag = (LocalInvTag)tags.next();
    						System.out.println(invTag.getTgDocumentNumber() + "<== tg document number");
    						try {
    							details.setRrpTgCustodian(invTag.getAdUser().getUsrDescription());
    							details.setRrpTgCustodianPosition(invTag.getAdUser().getUsrPosition());
    						}catch(Exception x){
    							details.setRrpTgCustodian("");
    							details.setRrpTgCustodianPosition("");
    						}

    						details.setRrpTgDocumentNumber(invTag.getTgDocumentNumber());
    						details.setRrpTgExpiryDateString(EJBCommon.convertSQLDateToString(invTag.getTgExpiryDate()));
    						details.setRrpTgPropertyCode(invTag.getTgPropertyCode());
    						details.setRrpTgSerialNumber(invTag.getTgSerialNumber());
    						details.setRrpTgSpecs(invTag.getTgSpecs());

							*/









							LocalInvTag invTag = (LocalInvTag)tags.next();

							//property code
							if(!invTag.getTgPropertyCode().equals("")) {
								strBProperty.append(invTag.getTgPropertyCode());
								strBProperty.append(System.getProperty("line.separator"));
							}

							//serial

							if(!invTag.getTgSerialNumber().equals("")) {
								strBSerial.append(invTag.getTgSerialNumber());
								strBSerial.append(System.getProperty("line.separator"));
							}

							//spec

							if(!invTag.getTgSpecs().equals("")) {
								strBSpecs.append(invTag.getTgSpecs());
								strBSpecs.append(System.getProperty("line.separator"));
							}

							//custodian

							if(invTag.getAdUser()!= null) {
								strBCustodian.append(invTag.getAdUser().getUsrName());
								strBCustodian.append(System.getProperty("line.separator"));
							}

							//exp date

							if(invTag.getTgExpiryDate()!=null) {
								strBExpirationDate.append(invTag.getTgExpiryDate());
								strBExpirationDate.append(System.getProperty("line.separator"));

							}




							//property code
        					details.setRrpTgPropertyCode(strBProperty.toString());
        					//serial number
        					details.setRrpTgSerialNumber(strBSerial.toString());
        					//specs
        					details.setRrpTgSpecs(strBSpecs.toString());
        					//custodian
        					details.setRrpTgCustodian(strBCustodian.toString());
        					//expiration date
        					details.setRrpTgExpiryDateString(strBExpirationDate.toString());





















    					}


    					//property code
    					details.setRrpTgPropertyCode(strBProperty.toString());
    					//serial number
    					details.setRrpTgSerialNumber(strBSerial.toString());
    					//specs
    					details.setRrpTgSpecs(strBSpecs.toString());
    					//custodian
    					details.setRrpTgCustodian(strBCustodian.toString());
    					//expiration date
    					details.setRrpTgExpiryDateString(strBExpirationDate.toString());


    					list.add(details);
    				} else {
    					System.out.println("invTags is empty");
    					ApRepReceivingReportPrintDetails details = new ApRepReceivingReportPrintDetails();
    					if (isService){
            				System.out.println(isService + " <== service ba?");
            				details.setRrpPlIsService(true);
            				System.out.println(details.getRrpPlIsService() + " <== details.getRrpPlIsService");
            			}
            			else {
            				System.out.println(isService + " <== o hindi?");
            				details.setRrpPlIsService(false);
            				System.out.println(details.getRrpPlIsService() + " <== details.getRrpPlIsService");
            			}
            			if (apPurchaseOrderLine.getInvItemLocation().getInvItem().getIiNonInventoriable()==1){
            				details.setRrpPlIsInventoriable(false);
            			}else{
            				details.setRrpPlIsInventoriable(true);
            			}
            			details.setRrpPoType(apPurchaseOrder.getPoType());
            			details.setRrpPoDate(apPurchaseOrder.getPoDate());
            			details.setRrpPoDocumentNumber(apPurchaseOrder.getPoDocumentNumber());
            			details.setRrpPoReferenceNumber(apPurchaseOrder.getPoReferenceNumber());
            			details.setRrpPoCreatedBy(apPurchaseOrder.getPoCreatedBy());
            			details.setRrpPaymentTerm(apPurchaseOrder.getAdPaymentTerm().getPytName());
            			//details.setRrpFixedAssetReceiving(apPurchaseOrder.getPoFixedAssetReceiving());

            			details.setRrpPoDepartment(userDepartment);

            			if(apPurchaseOrder.getPoApprovedRejectedBy() != null && !apPurchaseOrder.getPoApprovedRejectedBy().equals("")) {

            				details.setRrpPoApprovedRejectedBy(apPurchaseOrder.getPoApprovedRejectedBy());

            			} else {

            				details.setRrpPoApprovedRejectedBy(adPreference.getPrfApDefaultApprover());

            			}

            			details.setRrpPoRcvPoNumber(apPurchaseOrder.getPoRcvPoNumber());
            			details.setRrpPoDescription(apPurchaseOrder.getPoDescription());
            			details.setRrpPllIiName(II_NM);
            			details.setRrpPlLocName(LOC_NM);
            			details.setRrpPlLocPosition(apPurchaseOrderLine.getInvItemLocation().getInvLocation().getLocPosition());
            			details.setRrpPlLocBranch(apPurchaseOrderLine.getInvItemLocation().getInvLocation().getLocBranch());
            			details.setRrpPlLocDepartment(apPurchaseOrderLine.getInvItemLocation().getInvLocation().getLocDepartment());
            			details.setRrpPlLocDateHired(apPurchaseOrderLine.getInvItemLocation().getInvLocation().getLocDateHired());
            			details.setRrpPlLocEmploymentStatus(apPurchaseOrderLine.getInvItemLocation().getInvLocation().getLocEmploymentStatus());
            			details.setRrpPlIiRemarks(apPurchaseOrderLine.getInvItemLocation().getInvItem().getIiRemarks());
            			details.setRrpPlUomName(apPurchaseOrderLine.getInvUnitOfMeasure().getUomName());
            			//details.setRrpPlItemBarcode(apPurchaseOrderLine.getInvItemLocation().getInvItem().getIiPartNumber());

            			details.setRrpPlQuantity(apPurchaseOrderLine.getPlQuantity());
            			details.setRrpPlUnitCost(apPurchaseOrderLine.getPlUnitCost());
            			details.setRrpPlAmount(apPurchaseOrderLine.getPlAmount());


            			//double NT_AMNT = this.calculatePlNetAmount(apPurchaseOrderLine.getPlAmount(), apPurchaseOrder.getApTaxCode().getTcRate(),
            			//	apPurchaseOrder.getApTaxCode().getTcType(),this.getGlFcPrecisionUnit(AD_CMPNY)) ;

            			//details.setRrpPlTaxAmount(this.calculatePlTaxAmount(apPurchaseOrderLine.getPlAmount(), apPurchaseOrder.getApTaxCode().getTcRate(),
                			//	apPurchaseOrder.getApTaxCode().getTcType(), NT_AMNT, this.getGlFcPrecisionUnit(AD_CMPNY)));
            			details.setRrpPlTaxAmount(apPurchaseOrderLine.getPlTaxAmount());
            			details.setRrpPlItemDescription(apPurchaseOrderLine.getInvItemLocation().getInvItem().getIiDescription());
            			details.setRrpPoSupplierName(apPurchaseOrderLine.getApPurchaseOrder().getApSupplier().getSplName());
            			details.setRrpPoSupplierAddress(apPurchaseOrder.getApSupplier().getSplAddress());
            			details.setRrpPoContactPerson(apPurchaseOrderLine.getApPurchaseOrder().getApSupplier().getSplContact());
            			details.setRrpPoContactNumber(apPurchaseOrderLine.getApPurchaseOrder().getApSupplier().getSplPhone());
            			details.setRrpPoCheckedBy(adPreference.getPrfApDefaultChecker());

            			details.setRrpPoTaxRate(apPurchaseOrder.getApTaxCode().getTcRate());


            			// get unit cost wo vat
            			details.setRrpPlUnitCostWoTax(calculatePlNetUnitCost(details,
            					apPurchaseOrder.getApTaxCode().getTcRate(), apPurchaseOrder.getApTaxCode().getTcType(),
            					this.getGlFcPrecisionUnit(AD_CMPNY)));

            			// get unit cost vat inclusive
            			details.setRrpPlUnitCostTaxInclusive(calculatePlUnitCostTaxInclusive(details,
                				apPurchaseOrder.getApTaxCode().getTcRate(), apPurchaseOrder.getApTaxCode().getTcType(),
    							this.getGlFcPrecisionUnit(AD_CMPNY)));

//            			Include Branch
        				LocalAdBranch adBranch = adBranchHome.findByPrimaryKey(apPurchaseOrder.getPoAdBranch());
        				details.setBranchCode(adBranch.getBrBranchCode());
        				details.setBranchName(adBranch.getBrName());

        				if(apPurchaseOrderLine.getApPurchaseOrder().getPoType().equals("PO MATCHED")) {

    	        			try {

    	        				LocalApPurchaseOrder existingApPurchaseOrder = apPurchaseOrderHome.findByPoDocumentNumberAndBrCode(apPurchaseOrderLine.getApPurchaseOrder().getPoRcvPoNumber(), AD_BRNCH, AD_CMPNY);
    	        				details.setRrpPoRcvPoDate(existingApPurchaseOrder.getPoDate());

    	        			} catch(FinderException ex) {

    	        			}

    	        			try {

    	        				LocalApPurchaseOrderLine existingApPurchaseOrderLine = apPurchaseOrderLineHome.findByPrimaryKey(apPurchaseOrderLine.getPlPlCode());
    	        				details.setRrpPlQuantityOrdered(existingApPurchaseOrderLine.getPlQuantity());

    	        			} catch(FinderException ex) {

    	        			}

            			}

            			LocalInvCosting invCosting = null;

            			try {

            				invCosting = invCostingHome.getByMaxCstDateToLongAndMaxCstLineNumberAndIiNameAndLocName(II_NM, LOC_NM, AD_BRNCH, AD_CMPNY);
            				details.setRrpPlEndingBalance(apPurchaseOrderLine.getPlAmount() + invCosting.getCstRemainingValue());

            			} catch(FinderException ex) {
            				details.setRrpPlEndingBalance(apPurchaseOrderLine.getPlAmount());
            			}

    					list.add(details);
    				}


        		}

        	}

	        if (list.isEmpty()) {

	        	throw new GlobalNoRecordFoundException();

	        }

        	return list;

        } catch (GlobalNoRecordFoundException ex) {

        	throw ex;

        } catch (Exception ex) {

        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());

        }

    }

	/**
	* @ejb:interface-method view-type="remote"
	* @jboss:method-attributes read-only="true"
	**/
	public com.util.AdCompanyDetails getAdCompany(Integer AD_CMPNY) {

	  Debug.print("ApRepReceivingReportPrintControllerBean getAdCompany");

	  LocalAdCompanyHome adCompanyHome = null;

	  // Initialize EJB Home

	  try {

	       adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
	           lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);

	  } catch (NamingException ex) {

	      throw new EJBException(ex.getMessage());

	  }

	  try {

	     LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);

	     AdCompanyDetails details = new AdCompanyDetails();
	     details.setCmpName(adCompany.getCmpName());

	     return details;

	  } catch (Exception ex) {

	  	 Debug.printStackTrace(ex);
	  	 throw new EJBException(ex.getMessage());

	  }

	}

	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public InvItemDetails getInvItemByIiName(String II_NM, Integer AD_CMPNY) {

		Debug.print("InvItemLocationEntryControllerBean getInvItemByIiName");

		LocalInvItemHome invItemHome = null;
		LocalInvItem invItem = null;

		// Initialize EJB Home

		try {

			invItemHome = (LocalInvItemHome)EJBHomeFactory.
			lookUpLocalHome(LocalInvItemHome.JNDI_NAME, LocalInvItemHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}

		try {

			System.out.println("II_NM : " + II_NM);
			System.out.println("AD_CMPNY : " + AD_CMPNY);

			invItem = invItemHome.findByIiName(II_NM, AD_CMPNY);

			InvItemDetails details = new InvItemDetails();

			details.setIiName(invItem.getIiName());
			details.setIiDescription(invItem.getIiDescription());


			return details;

		} catch (Exception ex) {

			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());

		}

	}
	/**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public String getAdUsrDepartment(String USR_NM, Integer AD_CMPNY) {

        Debug.print("ApPurchaseRequisitionEntryControllerBean getAdUsrAll");

        LocalAdUserHome adUserHome = null;
        LocalAdUser adUser = null;
        ArrayList list = new ArrayList();

        // Initialize EJB Home

        try {

            adUserHome = (LocalAdUserHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdUserHome.JNDI_NAME, LocalAdUserHome.class);

        } catch (NamingException ex) {

            throw new EJBException(ex.getMessage());

        }

        try {

        	//Collection adUsers = adUserHome.findUsrByDepartment(USR_DEPT, AD_CMPNY);
        	adUser = adUserHome.findByUsrName(USR_NM, AD_CMPNY);
        	String department = adUser.getUsrDept();

        	return department;

        } catch (Exception ex) {

        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());

        }

    }
	private short getGlFcPrecisionUnit(Integer AD_CMPNY) {

		Debug.print("ApRepReceivingReportPrintControllerBean getGlFcPrecisionUnit");

		LocalAdCompanyHome adCompanyHome = null;


		// Initialize EJB Home

		try {

			adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}

		try {

			LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);

			return  adCompany.getGlFunctionalCurrency().getFcPrecision();

		} catch (Exception ex) {

			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());

		}

	}

	private double calculatePlNetAmount(double plAmount, double tcRate, String tcType,
			short precisionUnit) {

		Debug.print("ApRepReceivingReportPrintControllerBean calculatePlNetAmount");

		double amount = 0d;

		if (tcType.equals("INCLUSIVE")) {

			amount = EJBCommon.roundIt(plAmount / (1 + (tcRate / 100)), precisionUnit);

	    } else {

	        // tax exclusive, none, zero rated or exempt

	        amount = plAmount;

		}

		return amount;

	}

	private double calculatePlTaxAmount(double plAmount, double tcRate, String tcType,
			double amount, short precisionUnit) {

		Debug.print("ApRepReceivingReportPrintControllerBean calculatePlTaxAmount");

		double taxAmount = 0d;

		if (!tcType.equals("NONE") &&
	    	    !tcType.equals("EXEMPT")) {


        	if (tcType.equals("INCLUSIVE")) {

        		taxAmount = EJBCommon.roundIt(plAmount - amount, precisionUnit);

        	} else if (tcType.equals("EXCLUSIVE")) {

        		taxAmount = EJBCommon.roundIt(plAmount * tcRate / 100, precisionUnit);

            } else {

            	// tax none zero-rated or exempt

        	}

		}

		return taxAmount;

	}

	private double calculatePlNetUnitCost(ApRepReceivingReportPrintDetails mdetails, double tcRate, String tcType,
    		short precisionUnit) {

    	Debug.print("ApRepReceivingReportPrintControllerBean calculatePlNetUnitCost");

		double amount = mdetails.getRrpPlUnitCost();

		if (tcType.equals("INCLUSIVE")) {

			amount = EJBCommon.roundIt(mdetails.getRrpPlUnitCost() / (1 + (tcRate / 100)), precisionUnit);

	    }

		return amount;

	}

	private double calculatePlUnitCostTaxInclusive(ApRepReceivingReportPrintDetails mdetails, double tcRate, String tcType,
    		short precisionUnit) {

    	Debug.print("ApRepReceivingReportPrintControllerBean calculatePlUnitCostTaxInclusive");

		double amount = mdetails.getRrpPlUnitCost();

		if (tcType.equals("EXCLUSIVE")) {

			amount = EJBCommon.roundIt(mdetails.getRrpPlUnitCost() * (1 + (tcRate / 100)), precisionUnit);

	    }

		return amount;

	}

    // SessionBean methods

    /**
     * @ejb:create-method view-type="remote"
     **/
    public void ejbCreate() throws CreateException {

       Debug.print("ApRepReceivingReportPrintControllerBean ejbCreate");

    }
}
