
/*
 * ApRepSupplierClassListControllerBean.java
 *
 * Created on March 02, 2005, 10:07 AM
 *
 * @author  Neil Andrew M. Ajero
 */

package com.ejb.txn;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.naming.NamingException;

import com.ejb.ad.LocalAdCompany;
import com.ejb.ad.LocalAdCompanyHome;
import com.ejb.ap.LocalApSupplierClass;
import com.ejb.ap.LocalApSupplierClassHome;
import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.gl.LocalGlChartOfAccount;
import com.ejb.gl.LocalGlChartOfAccountHome;
import com.util.AbstractSessionBean;
import com.util.AdCompanyDetails;
import com.util.ApRepSupplierClassListDetails;
import com.util.Debug;
import com.util.EJBHomeFactory;

/**
 * @ejb:bean name="ApRepSupplierClassListControllerEJB"
 *           display-name="Used for viewing supplier class lists"
 *           type="Stateless"
 *           view-type="remote"
 *           jndi-name="ejb/ApRepSupplierClassListControllerEJB"
 *
 * @ejb:interface remote-class="com.ejb.txn.ApRepSupplierClassListController"
 *                extends="javax.ejb.EJBObject"
 *
 * @ejb:home remote-class="com.ejb.txn.ApRepSupplierClassListControllerHome"
 *           extends="javax.ejb.EJBHome"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="apuser"
 *                        role-link="apuserlink"
 *
 * @ejb:permission role-name="apuser"
 * 
*/

public class ApRepSupplierClassListControllerBean extends AbstractSessionBean {


    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList executeApRepSupplierClassList(HashMap criteria, String ORDER_BY, Integer AD_CMPNY) 
        throws GlobalNoRecordFoundException {
                    
        Debug.print("ApRepSupplierClassListControllerBean executeApRepSupplierClassList");
        
        LocalApSupplierClassHome apSupplierClassHome = null;
        LocalGlChartOfAccountHome glChartOfAccountHome = null;
        
        ArrayList list = new ArrayList();
        
        //initialized EJB Home
        
        try {
            
        	apSupplierClassHome = (LocalApSupplierClassHome)EJBHomeFactory.
                lookUpLocalHome(LocalApSupplierClassHome.JNDI_NAME, LocalApSupplierClassHome.class);
        	glChartOfAccountHome = (LocalGlChartOfAccountHome)EJBHomeFactory.
            	lookUpLocalHome(LocalGlChartOfAccountHome.JNDI_NAME, LocalGlChartOfAccountHome.class);
         
        } catch (NamingException ex) 	{
            
            throw new EJBException(ex.getMessage());
            
        }
        
		try { 
           
		  StringBuffer jbossQl = new StringBuffer();
		  jbossQl.append("SELECT OBJECT(sc) FROM ApSupplierClass sc ");
		  
	      boolean firstArgument = true;
	      short ctr = 0;
		  int criteriaSize = criteria.size();	      
	      
	      Object obj[];	      
		
		  // Allocate the size of the object parameter
		
	      if (criteria.containsKey("supplierClassName")) {
	      	
	      	 criteriaSize--;
	      	 
	      }

	      obj = new Object[criteriaSize];
		       	      
		  if (criteria.containsKey("supplierClassName")) {
		  	
		  	 if (!firstArgument) {
		  	 
		  	    jbossQl.append("AND ");	
		  	 	
		     } else {
		     	
		     	firstArgument = false;
		     	jbossQl.append("WHERE ");
		     	
		     }
		     
		  	 jbossQl.append("sc.scName LIKE '%" + (String)criteria.get("supplierClassName") + "%' ");
		  	 
		  }
		  
	      if (!firstArgument) {		       	  	
	   	     jbossQl.append("AND ");		       	     
	   	  } else {		       	  	
	   	  	 firstArgument = false;
	   	  	 jbossQl.append("WHERE ");		       	  	 
	   	  }	     
	   	  
	   	  jbossQl.append("sc.scAdCompany=" + AD_CMPNY + " ");

		  String orderBy = null;
          
          if (ORDER_BY.equals("SUPPLIER CLASS NAME")) {
	      	 
	      	  orderBy = "sc.scName";
	      	  
	      } 

		  if (orderBy != null) {
		  
		  	jbossQl.append("ORDER BY " + orderBy);
		  	
		  }     
			  	      	
	      Collection apSupplierClasses = apSupplierClassHome.getScByCriteria(jbossQl.toString(), obj);	         
		  
		  if (apSupplierClasses.size() == 0)
		     throw new GlobalNoRecordFoundException();
		     
		  Iterator i = apSupplierClasses.iterator();
		
		  while (i.hasNext()) {
		  	
		  	  LocalApSupplierClass apSupplierClass = (LocalApSupplierClass)i.next();   	  
		  	  
		  	  ApRepSupplierClassListDetails details = new ApRepSupplierClassListDetails();
		  	  details.setSclScName(apSupplierClass.getScName());
		  	  details.setSclScDescription(apSupplierClass.getScDescription());
		  	  details.setSclTaxName(apSupplierClass.getApTaxCode().getTcName());
		  	  details.setSclWithholdingTaxName(apSupplierClass.getApWithholdingTaxCode().getWtcName());
		  	  
		  	  LocalGlChartOfAccount glPayableAccount = glChartOfAccountHome.findByPrimaryKey(apSupplierClass.getScGlCoaPayableAccount());
		  	  LocalGlChartOfAccount glExpenseAccount = null;
		  	  
		  	  if (apSupplierClass.getScGlCoaExpenseAccount() != null) {
		  	  	
		  	  	glExpenseAccount = glChartOfAccountHome.findByPrimaryKey(apSupplierClass.getScGlCoaExpenseAccount());
		  	  	
		  	  }
		  	  
		  	  details.setSclPayableAccountNumber(glPayableAccount.getCoaAccountNumber());
		  	  details.setSclExpenseAccountNumber(glExpenseAccount != null ? glExpenseAccount.getCoaAccountNumber() : null);  
		  	  details.setSclPayableAccountDescription(glPayableAccount.getCoaAccountDescription());		
		  	  
		  	  if (glExpenseAccount != null) {
		  	  	
		  	  	details.setSclExpenseAccountDescription(glExpenseAccount.getCoaAccountDescription());
		  	  	
		  	  }
		  	  
		  	  details.setSclEnable(apSupplierClass.getScEnable());

			  list.add(details);
			         
		  }
			     
		  return list;
		      
	  } catch (GlobalNoRecordFoundException ex) {
	  	 
	  	  throw ex;
	  	
	  } catch (Exception ex) {
	  	

	  	  ex.printStackTrace();
	  	  throw new EJBException(ex.getMessage());
	  	
	  }
        
    }
    
	/**
	* @ejb:interface-method view-type="remote"
	* @jboss:method-attributes read-only="true"
	**/
	public com.util.AdCompanyDetails getAdCompany(Integer AD_CMPNY) {
	
	  Debug.print("ApRepSupplierClassListControllerBean getAdCompany");      
	  
	  LocalAdCompanyHome adCompanyHome = null;
	        
	  // Initialize EJB Home
	    
	  try {
	        
	       adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
	           lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
	        
	  } catch (NamingException ex) {
	        
	      throw new EJBException(ex.getMessage());
	        
	  }
	
	  try {
	      
	     LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
	     
	     AdCompanyDetails details = new AdCompanyDetails();
	     details.setCmpName(adCompany.getCmpName());
	     
	     return details;  	
	  	       	
	  } catch (Exception ex) {
	  	
	  	 Debug.printStackTrace(ex);
	  	 throw new EJBException(ex.getMessage());
	  	  
	  }
	  
	}    

    // SessionBean methods

    /**
     * @ejb:create-method view-type="remote"
     **/
    public void ejbCreate() throws CreateException {

       Debug.print("ApRepSupplierClassListControllerBean ejbCreate");
      
    }
}
