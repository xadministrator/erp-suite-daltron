
/*
 * ApRepSupplierListControllerBean.java
 *
 * Created on March 02, 2004, 9:38 AM
 *
 * @author  Neil Andrew M. Ajero
 */

package com.ejb.txn;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;
import javax.naming.NamingException;

import com.ejb.ad.LocalAdBranch;
import com.ejb.ad.LocalAdBranchHome;
import com.ejb.ad.LocalAdBranchResponsibility;
import com.ejb.ad.LocalAdBranchResponsibilityHome;
import com.ejb.ad.LocalAdCompany;
import com.ejb.ad.LocalAdCompanyHome;
import com.ejb.ap.LocalApCheck;
import com.ejb.ap.LocalApDistributionRecord;
import com.ejb.ap.LocalApDistributionRecordHome;
import com.ejb.ap.LocalApSupplier;
import com.ejb.ap.LocalApSupplierBalance;
import com.ejb.ap.LocalApSupplierBalanceHome;
import com.ejb.ap.LocalApSupplierClass;
import com.ejb.ap.LocalApSupplierClassHome;
import com.ejb.ap.LocalApSupplierHome;
import com.ejb.ap.LocalApSupplierType;
import com.ejb.ap.LocalApSupplierTypeHome;
import com.ejb.ap.LocalApVoucher;
import com.ejb.ap.LocalApVoucherHome;
import com.ejb.exception.GlobalAmountInvalidException;
import com.ejb.exception.GlobalNoRecordFoundException;
import com.util.AbstractSessionBean;
import com.util.AdBranchDetails;
import com.util.AdCompanyDetails;
import com.util.ApRepSupplierListDetails;
import com.util.Debug;
import com.util.EJBCommon;
import com.util.EJBHomeFactory;

/**
 * @ejb:bean name="ApRepSupplierListControllerEJB"
 *           display-name="Used for viewing supplier lists"
 *           type="Stateless"
 *           view-type="remote"
 *           jndi-name="ejb/ApRepSupplierListControllerEJB"
 *
 * @ejb:interface remote-class="com.ejb.txn.ApRepSupplierListController"
 *                extends="javax.ejb.EJBObject"
 *
 * @ejb:home remote-class="com.ejb.txn.ApRepSupplierListControllerHome"
 *           extends="javax.ejb.EJBHome"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="apuser"
 *                        role-link="apuserlink"
 *
 * @ejb:permission role-name="apuser"
 *
*/

public class ApRepSupplierListControllerBean extends AbstractSessionBean {

    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList getApScAll(Integer AD_CMPNY) {

        Debug.print("ApRepSupplierListControllerBean getApScAll");

        LocalApSupplierClassHome apSupplierClassHome = null;

        ArrayList list = new ArrayList();

        // Initialize EJB Home

        try {

            apSupplierClassHome = (LocalApSupplierClassHome)EJBHomeFactory.
                lookUpLocalHome(LocalApSupplierClassHome.JNDI_NAME, LocalApSupplierClassHome.class);

        } catch (NamingException ex) {

            throw new EJBException(ex.getMessage());

        }

        try {

            Collection apSupplierClasses = apSupplierClassHome.findEnabledScAll(AD_CMPNY);

	        Iterator i = apSupplierClasses.iterator();

	        while (i.hasNext()) {

	        	LocalApSupplierClass apSupplierClass = (LocalApSupplierClass)i.next();

	        	list.add(apSupplierClass.getScName());

	        }

	        return list;

        } catch (Exception ex) {

        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());

        }

    }

    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList getApStAll(Integer AD_CMPNY) {

        Debug.print("ApRepSupplierListControllerBean getApStAll");

        LocalApSupplierTypeHome apSupplierTypeHome = null;

        ArrayList list = new ArrayList();

        // Initialize EJB Home

        try {

            apSupplierTypeHome = (LocalApSupplierTypeHome)EJBHomeFactory.
                lookUpLocalHome(LocalApSupplierTypeHome.JNDI_NAME, LocalApSupplierTypeHome.class);

        } catch (NamingException ex) {

            throw new EJBException(ex.getMessage());

        }

        try {

            Collection apSupplierTypes = apSupplierTypeHome.findEnabledStAll(AD_CMPNY);

	        Iterator i = apSupplierTypes.iterator();

	        while (i.hasNext()) {

	        	LocalApSupplierType apSupplierType = (LocalApSupplierType)i.next();

	        	list.add(apSupplierType.getStName());

	        }

	        return list;

        } catch (Exception ex) {

        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());

        }

    }

    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList executeApRepSupplierList(HashMap criteria, String ORDER_BY, boolean includeDC, ArrayList adBrnchList, Integer AD_CMPNY)
        throws GlobalNoRecordFoundException {

        Debug.print("ApRepSupplierListControllerBean executeApRepSupplierList");

        LocalApSupplierHome apSupplierHome = null;
        LocalApSupplierBalanceHome apSupplierBalanceHome = null;
        LocalApVoucherHome apVoucherHome = null;
        LocalApDistributionRecordHome apDistributionRecordHome = null;

        ArrayList list = new ArrayList();

        //initialized EJB Home

        try {

            apSupplierHome = (LocalApSupplierHome)EJBHomeFactory.
                lookUpLocalHome(LocalApSupplierHome.JNDI_NAME, LocalApSupplierHome.class);
            apSupplierBalanceHome = (LocalApSupplierBalanceHome)EJBHomeFactory.
                lookUpLocalHome(LocalApSupplierBalanceHome.JNDI_NAME, LocalApSupplierBalanceHome.class);
            apVoucherHome = (LocalApVoucherHome)EJBHomeFactory.
            	lookUpLocalHome(LocalApVoucherHome.JNDI_NAME, LocalApVoucherHome.class);
            apDistributionRecordHome = (LocalApDistributionRecordHome)EJBHomeFactory.
            	lookUpLocalHome(LocalApDistributionRecordHome.JNDI_NAME, LocalApDistributionRecordHome.class);

        } catch (NamingException ex) 	{

            throw new EJBException(ex.getMessage());

        }

		try {

		  StringBuffer jbossQl = new StringBuffer();
		  jbossQl.append("SELECT DISTINCT OBJECT(spl) FROM ApSupplier spl ");

	      boolean firstArgument = true;
	      short ctr = 0;
		  int criteriaSize = criteria.size();

	      Object obj[];

		  // Allocate the size of the object parameter

	      if (criteria.containsKey("supplierCode")) {

	      	 criteriaSize--;

	      }

	      if (criteria.containsKey("date")) {

	      	 criteriaSize--;

	      }

	      obj = new Object[criteriaSize];

	      if(adBrnchList.isEmpty()) {

	      	  throw new GlobalNoRecordFoundException();

	      } else {

	          jbossQl.append(", in (spl.adBranchSuppliers) bspl WHERE bspl.adBranch.brCode in (");

	          boolean firstLoop = true;

	          Iterator i = adBrnchList.iterator();

	          while(i.hasNext()) {

	              if(firstLoop == false) {
	                  jbossQl.append(", ");
	              }
	              else {
	                  firstLoop = false;
	              }

	              AdBranchDetails mdetails = (AdBranchDetails) i.next();

	              jbossQl.append(mdetails.getBrCode());

	          }

	          jbossQl.append(") ");

	          firstArgument = false;

	      }

		  if (criteria.containsKey("supplierCode")) {

		  	 if (!firstArgument) {

		  	    jbossQl.append("AND ");

		     } else {

		     	firstArgument = false;
		     	jbossQl.append("WHERE ");

		     }

		  	 jbossQl.append("spl.splSupplierCode LIKE '%" + (String)criteria.get("supplierCode") + "%' ");

		  }

		  if (criteria.containsKey("supplierType")) {

		   	  if (!firstArgument) {
		   	     jbossQl.append("AND ");
		   	  } else {
		   	  	 firstArgument = false;
		   	  	 jbossQl.append("WHERE ");
		   	  }

		   	  jbossQl.append("spl.apSupplierType.stName=?" + (ctr+1) + " ");
		   	  obj[ctr] = (String)criteria.get("supplierType");
		   	  ctr++;

	      }

		  if (criteria.containsKey("supplierClass")) {

		   	  if (!firstArgument) {
		   	     jbossQl.append("AND ");
		   	  } else {
		   	  	 firstArgument = false;
		   	  	 jbossQl.append("WHERE ");
		   	  }

		   	  jbossQl.append("spl.apSupplierClass.scName=?" + (ctr+1) + " ");
		   	  obj[ctr] = (String)criteria.get("supplierClass");
		   	  ctr++;

	      }

	      if (!firstArgument) {
	   	     jbossQl.append("AND ");
	   	  } else {
	   	  	 firstArgument = false;
	   	  	 jbossQl.append("WHERE ");
	   	  }

	   	  jbossQl.append("spl.splAdCompany=" + AD_CMPNY + " ");

		  String orderBy = null;

          if (ORDER_BY.equals("SUPPLIER CODE")) {

	      	  orderBy = "spl.splSupplierCode";

	      } else if (ORDER_BY.equals("SUPPLIER TYPE")) {

	      	  orderBy = "spl.apSupplierType.stName";

	      } else if (ORDER_BY.equals("SUPPLIER CLASS")) {

	      	  orderBy = "spl.apSupplierClass.scName";

	      }

		  if (orderBy != null) {

		  	jbossQl.append("ORDER BY " + orderBy);

		  }

	      Collection apSuppliers = apSupplierHome.getSplByCriteria(jbossQl.toString(), obj);

		  if (apSuppliers.size() == 0)
		     throw new GlobalNoRecordFoundException();

		  GregorianCalendar lastDateGc = new GregorianCalendar();
		  lastDateGc.setTime((Date)criteria.get("date"));

		  GregorianCalendar firstDateGc = new GregorianCalendar(
				  lastDateGc.get(Calendar.YEAR),
				  lastDateGc.get(Calendar.MONTH),
				  lastDateGc.getActualMinimum(Calendar.DATE),
				  0, 0, 0);

		  GregorianCalendar lastMonthDateGc2 = null;

		  Iterator i = apSuppliers.iterator();
		  double drAmnt = 0d;
		  double SplBalance = 0d;
		  while (i.hasNext()) {

			  boolean issetBegBal = true;

			  LocalApSupplier apSupplier = (LocalApSupplier)i.next();

			  ApRepSupplierListDetails details = new ApRepSupplierListDetails();
			  details.setSlSplSupplierCode(apSupplier.getSplSupplierCode());
			  details.setSlSplName(apSupplier.getSplName());
			  details.setSlSplContact(apSupplier.getSplContact());
			  details.setSlSplPhone(apSupplier.getSplPhone());
			  details.setSlSplTin(apSupplier.getSplTin());
			  details.setSlSplAddress(apSupplier.getSplAddress());

			  if(includeDC){
				  Iterator d = adBrnchList.iterator();

		          while(d.hasNext()) {
		        	  AdBranchDetails mdetails = (AdBranchDetails) d.next();
		        	  System.out.println("mdetails.getBrBranchCode(): "+mdetails.getBrCode());
		        	  Collection apDistributionRecords =
						  apDistributionRecordHome.findChkByDateAndCoaAccountDescriptionAndSupplier((Date)criteria.get("date"), apSupplier.getSplCode(), mdetails.getBrCode(), AD_CMPNY);

					  if (apDistributionRecords.isEmpty()) {
						  System.out.println("WALA!");
					  }else{
						  Iterator x = apDistributionRecords.iterator();

						  while (x.hasNext())
						  {

							  // && apDistributionRecord.getApCheck().getChkAdBranch()=
							  LocalApDistributionRecord apDistributionRecord = (LocalApDistributionRecord)x.next();
							  if(apDistributionRecord.getDrDebit()!=0){
								  System.out.println("Supplier Name!: "+apDistributionRecord.getApCheck().getApSupplier().getSplName());
								  System.out.println("Account!: "+apDistributionRecord.getGlChartOfAccount().getCoaAccountDescription());
								  System.out.println("Check!: "+apDistributionRecord.getApCheck().getChkDocumentNumber());
								  System.out.println("Check Date!: "+apDistributionRecord.getApCheck().getChkDate());
								  System.out.println("Amount!: "+apDistributionRecord.getDrAmount());
								  drAmnt = drAmnt + apDistributionRecord.getDrAmount();
							  } else {
									  drAmnt = drAmnt - apDistributionRecord.getDrAmount();
							  }

						  }
					  }
		          }

			  }

			  // get balance OK

			  Collection apSupplierBalances =
				  apSupplierBalanceHome.findByBeforeOrEqualSbDateAndSplCode((Date)criteria.get("date"), apSupplier.getSplCode(), AD_CMPNY);

			  if (apSupplierBalances.isEmpty()) {

				  details.setSlSplBalance(0d);
				  details.setSlSplPtdBalance(0d);
				  details.setSlSplBegBalance(0d);

			  } else {

				  ArrayList apSupplierBalanceList = new ArrayList(apSupplierBalances);

				  LocalApSupplierBalance apSupplierBalance = (LocalApSupplierBalance)apSupplierBalanceList.get(apSupplierBalanceList.size() - 1);

				  if(drAmnt<0){
					  details.setSlSplBalance(apSupplierBalance.getSbBalance()-(drAmnt*-1));
				  }else{
					  details.setSlSplBalance(apSupplierBalance.getSbBalance()-(drAmnt));
				  }


				  //get beginning balance OK
				  if(issetBegBal)
				  {
					  GregorianCalendar lastMonthDateGc = new GregorianCalendar();
					  lastMonthDateGc.setTime((Date)criteria.get("date"));
					  lastMonthDateGc.add(Calendar.MONTH, -1);

					  lastMonthDateGc2 = new GregorianCalendar(
			    				lastMonthDateGc.get(Calendar.YEAR),
			    				lastMonthDateGc.get(Calendar.MONTH),
			    				lastMonthDateGc.getActualMaximum(Calendar.DATE),
			    				0, 0, 0);

					  Collection apBegBalances = apSupplierBalanceHome.findByBeforeOrEqualSbDateAndSplCode(lastMonthDateGc2.getTime(), apSupplier.getSplCode(), AD_CMPNY);

					  Iterator x = apBegBalances.iterator();

					  while (x.hasNext())
					  {
						  LocalApSupplierBalance apBegBalance = (LocalApSupplierBalance)x.next();

						  double begBalance = apBegBalance.getSbBalance();

						  details.setSlSplBegBalance(begBalance);

						  System.out.println("Supplier: " + apSupplier.getSplName() + "\tDate" + lastMonthDateGc2.getTime() + "\tBeginning Balance: " + apBegBalance.getSbBalance());
					  }

					  issetBegBal = false;

				  }


				  // get ptd balance OK (same as Net Transactions)

				  double priorBalance = 0d;

				  Collection apSupplierPriorBalances = apSupplierBalanceHome.findByBeforeSbDateAndSplCode(firstDateGc.getTime(), apSupplier.getSplCode(), AD_CMPNY);

				  if (!apSupplierPriorBalances.isEmpty()) {

					  ArrayList apSupplierPriorBalanceList = new ArrayList(apSupplierPriorBalances);

					  LocalApSupplierBalance apSupplierPriorBalance = (LocalApSupplierBalance)apSupplierPriorBalanceList.get(apSupplierPriorBalanceList.size() - 1);

					  priorBalance = apSupplierPriorBalance.getSbBalance();

				  }

				  details.setSlSplPtdBalance((apSupplierBalance.getSbBalance() - priorBalance)-drAmnt);
				  System.out.println("details.setSlSplPtdBalance: " + (apSupplierBalance.getSbBalance() - priorBalance));
			  }




			  //get Interest Income

			  double interestIncome = 0d;

			  GregorianCalendar vouDateGc = new GregorianCalendar();
			  vouDateGc.setTime((Date)criteria.get("date"));

			  GregorianCalendar firstDayOfMonth = new GregorianCalendar(
					  vouDateGc.get(Calendar.YEAR),
					  vouDateGc.get(Calendar.MONTH),
					  vouDateGc.getActualMinimum(Calendar.DATE),
	    				0, 0, 0);

			  String referenceNumber = "INT-" + EJBCommon.convertSQLDateToString(vouDateGc.getTime());

			  Collection apInterestIncomes = apVoucherHome.findByVouReferenceNumberAndSplName(
					  referenceNumber, apSupplier.getSplName(), AD_CMPNY);

			  Iterator itr = apInterestIncomes.iterator();

			  while (itr.hasNext()) {

				  LocalApVoucher apInterestIncome = (LocalApVoucher) itr.next();

				  System.out.println(apInterestIncome.getVouDate());
				  interestIncome = apInterestIncome.getVouAmountDue();

			  }
			  details.setSlSplAmntDC(drAmnt);
			  details.setSlSplInterestIncome(interestIncome);
			  System.out.println("drAmnt: "+drAmnt);
			  SplBalance = (details.getSlSplBalance() + interestIncome);
			  System.out.println(SplBalance);
			  details.setSlSplBalance(SplBalance);
			  System.out.println("details.getSlSplBalance2: " + ((details.getSlSplBalance() + interestIncome)));
			  details.setSlSplPaymentTerm(apSupplier.getAdPaymentTerm().getPytName());
			  list.add(details);
			  drAmnt=0;
		  }

		  return list;

	  } catch (GlobalNoRecordFoundException ex) {

	  	  throw ex;

	  } catch (Exception ex) {


	  	  ex.printStackTrace();
	  	  throw new EJBException(ex.getMessage());

	  }

    }

	/**
	* @ejb:interface-method view-type="remote"
	* @jboss:method-attributes read-only="true"
	**/
	public com.util.AdCompanyDetails getAdCompany(Integer AD_CMPNY) {

	  Debug.print("ApRepSupplierListControllerBean getAdCompany");

	  LocalAdCompanyHome adCompanyHome = null;

	  // Initialize EJB Home

	  try {

	       adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
	           lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);

	  } catch (NamingException ex) {

	      throw new EJBException(ex.getMessage());

	  }

	  try {

	     LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);

	     AdCompanyDetails details = new AdCompanyDetails();
	     details.setCmpName(adCompany.getCmpName());

	     return details;

	  } catch (Exception ex) {

	      Debug.printStackTrace(ex);
	      throw new EJBException(ex.getMessage());

	  }

	}

	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public ArrayList getAdBrResAll(Integer RS_CODE, Integer AD_CMPNY)
		throws GlobalNoRecordFoundException{

	    Debug.print("ApRepSupplierListControllerBean getAdBrResAll");

	    LocalAdBranchResponsibilityHome adBranchResponsibilityHome = null;
	    LocalAdBranchHome adBranchHome = null;

	    LocalAdBranchResponsibility adBranchResponsibility = null;
	    LocalAdBranch adBranch = null;

	    Collection adBranchResponsibilities = null;

	    ArrayList list = new ArrayList();

	    // Initialize EJB Home

	    try {

	        adBranchResponsibilityHome = (LocalAdBranchResponsibilityHome)EJBHomeFactory.
	        lookUpLocalHome(LocalAdBranchResponsibilityHome.JNDI_NAME, LocalAdBranchResponsibilityHome.class);
	        adBranchHome = (LocalAdBranchHome)EJBHomeFactory.
	        lookUpLocalHome(LocalAdBranchHome.JNDI_NAME, LocalAdBranchHome.class);

	    } catch (NamingException ex) {

	        throw new EJBException(ex.getMessage());

	    }

	    try {

	        adBranchResponsibilities = adBranchResponsibilityHome.findByAdResponsibility(RS_CODE, AD_CMPNY);

	    } catch (FinderException ex) {

	    } catch (Exception ex) {

	        throw new EJBException(ex.getMessage());
	    }

	    if (adBranchResponsibilities.isEmpty()) {

	        throw new GlobalNoRecordFoundException();

	    }

	    try {

	        Iterator i = adBranchResponsibilities.iterator();

	        while(i.hasNext()) {

	            adBranchResponsibility = (LocalAdBranchResponsibility)i.next();

	            adBranch = adBranchResponsibility.getAdBranch();

	            AdBranchDetails details = new AdBranchDetails();

	            details.setBrCode(adBranch.getBrCode());
	            details.setBrBranchCode(adBranch.getBrBranchCode());
	            details.setBrName(adBranch.getBrName());
	            details.setBrHeadQuarter(adBranch.getBrHeadQuarter());

	            list.add(details);

	        }

	    } catch (Exception ex) {

	        throw new EJBException(ex.getMessage());
	    }

	    return list;

	}


    // SessionBean methods

    /**
     * @ejb:create-method view-type="remote"
     **/
    public void ejbCreate() throws CreateException {

       Debug.print("ApRepSupplierListControllerBean ejbCreate");

    }
}
