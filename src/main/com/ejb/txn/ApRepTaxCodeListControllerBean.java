
/*
 * ApRepTaxCodeListControllerBean.java
 *
 * Created on March 01, 2005, 01:23 PM
 *
 * @author  Neil Andrew M. Ajero
 */

package com.ejb.txn;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.naming.NamingException;

import com.ejb.ad.LocalAdCompany;
import com.ejb.ad.LocalAdCompanyHome;
import com.ejb.ap.LocalApTaxCode;
import com.ejb.ap.LocalApTaxCodeHome;
import com.ejb.exception.GlobalNoRecordFoundException;
import com.util.AbstractSessionBean;
import com.util.AdCompanyDetails;
import com.util.ApRepTaxCodeListDetails;
import com.util.Debug;
import com.util.EJBHomeFactory;

/**
 * @ejb:bean name="ApRepTaxCodeListControllerEJB"
 *           display-name="Used for viewing tax code lists"
 *           type="Stateless"
 *           view-type="remote"
 *           jndi-name="ejb/ApRepTaxCodeListControllerEJB"
 *
 * @ejb:interface remote-class="com.ejb.txn.ApRepTaxCodeListController"
 *                extends="javax.ejb.EJBObject"
 *
 * @ejb:home remote-class="com.ejb.txn.ApRepTaxCodeListControllerHome"
 *           extends="javax.ejb.EJBHome"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="apuser"
 *                        role-link="apuserlink"
 *
 * @ejb:permission role-name="apuser"
 * 
*/

public class ApRepTaxCodeListControllerBean extends AbstractSessionBean {


    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList executeApRepTaxCodeList(HashMap criteria, String ORDER_BY, Integer AD_CMPNY) 
        throws GlobalNoRecordFoundException {
                    
        Debug.print("ApRepTaxCodeListControllerBean executeApRepTaxCodeList");
        
        LocalApTaxCodeHome apTaxCodeHome = null;
        
        ArrayList list = new ArrayList();
        
        //initialized EJB Home
        
        try {
            
        	apTaxCodeHome = (LocalApTaxCodeHome)EJBHomeFactory.
                lookUpLocalHome(LocalApTaxCodeHome.JNDI_NAME, LocalApTaxCodeHome.class);
         
        } catch (NamingException ex) 	{
            
            throw new EJBException(ex.getMessage());
            
        }
        
		try { 
           
		  StringBuffer jbossQl = new StringBuffer();
		  jbossQl.append("SELECT OBJECT(tc) FROM ApTaxCode tc ");
		  
	      boolean firstArgument = true;
	      short ctr = 0;
		  int criteriaSize = criteria.size();	      
	      
	      Object obj[];	      
		
		  // Allocate the size of the object parameter
		
	      if (criteria.containsKey("taxName")) {
	      	
	      	 criteriaSize--;
	      	 
	      }

	      obj = new Object[criteriaSize];
		       	      
		  if (criteria.containsKey("taxName")) {
		  	
		  	 if (!firstArgument) {
		  	 
		  	    jbossQl.append("AND ");	
		  	 	
		     } else {
		     	
		     	firstArgument = false;
		     	jbossQl.append("WHERE ");
		     	
		     }
		     
		  	 jbossQl.append("tc.tcName LIKE '%" + (String)criteria.get("taxName") + "%' ");
		  	 
		  }
		  
		  if (criteria.containsKey("type")) {
	   	
		   	  if (!firstArgument) {		       	  	
		   	     jbossQl.append("AND ");		       	     
		   	  } else {		       	  	
		   	  	 firstArgument = false;
		   	  	 jbossQl.append("WHERE ");		       	  	 
		   	  }
		   	  
		   	  jbossQl.append("tc.tcType=?" + (ctr+1) + " ");
		   	  obj[ctr] = (String)criteria.get("type");
		   	  ctr++;
	   	  
	      }	
	      
	      if (!firstArgument) {		       	  	
	   	     jbossQl.append("AND ");		       	     
	   	  } else {		       	  	
	   	  	 firstArgument = false;
	   	  	 jbossQl.append("WHERE ");		       	  	 
	   	  }	     
	   	  
	   	  jbossQl.append("tc.tcAdCompany=" + AD_CMPNY + " ");

		  String orderBy = null;
          
          if (ORDER_BY.equals("TAX NAME")) {
	      	 
	      	  orderBy = "tc.tcName";
	      	  
	      } else if (ORDER_BY.equals("TAX TYPE")) {
	      	
	      	  orderBy = "tc.tcType";
	      	
	      }

		  if (orderBy != null) {
		  
		  	jbossQl.append("ORDER BY " + orderBy);
		  	
		  }     
			  	      	
	      Collection apTaxCodes = apTaxCodeHome.getTcByCriteria(jbossQl.toString(), obj);	         
		  
		  if (apTaxCodes.size() == 0)
		     throw new GlobalNoRecordFoundException();
		     
		  Iterator i = apTaxCodes.iterator();
		
		  while (i.hasNext()) {
		  	
		  	  LocalApTaxCode apTaxCode = (LocalApTaxCode)i.next();   	  
		  	  
		  	  ApRepTaxCodeListDetails details = new ApRepTaxCodeListDetails();
		  	  details.setTclTaxName(apTaxCode.getTcName());
		  	  details.setTclTaxDescription(apTaxCode.getTcDescription());
		  	  details.setTclTaxType(apTaxCode.getTcType());
		  	  details.setTclTaxRate(apTaxCode.getTcRate());
		  	  
		  	  if (apTaxCode.getGlChartOfAccount() != null) {
		  	  	
		  	  	 details.setTclCoaGlTaxAccountNumber(apTaxCode.getGlChartOfAccount().getCoaAccountNumber());	                		                	    
		  	  	 details.setTclCoaGlTaxAccountDescription(apTaxCode.getGlChartOfAccount().getCoaAccountDescription());
		  	  	
		  	  }
		  	  
		  	  details.setTclEnable(apTaxCode.getTcEnable());

			  list.add(details);
			         
		  }
			     
		  return list;
		      
	  } catch (GlobalNoRecordFoundException ex) {
	  	 
	  	  throw ex;
	  	
	  } catch (Exception ex) {
	  	

	  	  ex.printStackTrace();
	  	  throw new EJBException(ex.getMessage());
	  	
	  }
        
    }
    
	/**
	* @ejb:interface-method view-type="remote"
	* @jboss:method-attributes read-only="true"
	**/
	public com.util.AdCompanyDetails getAdCompany(Integer AD_CMPNY) {
	
	  Debug.print("ApRepTaxCodeListControllerBean getAdCompany");      
	  
	  LocalAdCompanyHome adCompanyHome = null;
	        
	  // Initialize EJB Home
	    
	  try {
	        
	       adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
	           lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
	        
	  } catch (NamingException ex) {
	        
	      throw new EJBException(ex.getMessage());
	        
	  }
	
	  try {
	      
	     LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
	     
	     AdCompanyDetails details = new AdCompanyDetails();
	     details.setCmpName(adCompany.getCmpName());
	     
	     return details;  	
	  	       	
	  } catch (Exception ex) {
	  	
	  	 Debug.printStackTrace(ex);
	  	 throw new EJBException(ex.getMessage());
	  	  
	  }
	  
	}    

    // SessionBean methods

    /**
     * @ejb:create-method view-type="remote"
     **/
    public void ejbCreate() throws CreateException {

       Debug.print("ApRepTaxCodeListControllerBean ejbCreate");
      
    }
}
