
/*
 * ApRepVoucherEditListControllerBean.java
 *
 * Created on June 22, 2004, 10:37 AM
 *
 * @author  Neil Andrew M. Ajero
 */

package com.ejb.txn;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;
import javax.naming.NamingException;

import com.ejb.ad.LocalAdCompany;
import com.ejb.ad.LocalAdCompanyHome;
import com.ejb.ad.LocalAdPreference;
import com.ejb.ad.LocalAdPreferenceHome;
import com.ejb.ap.LocalApDistributionRecord;
import com.ejb.ap.LocalApVoucher;
import com.ejb.ap.LocalApVoucherHome;
import com.ejb.exception.GlobalNoRecordFoundException;
import com.util.AbstractSessionBean;
import com.util.AdCompanyDetails;
import com.util.ApRepVoucherEditListDetails;
import com.util.Debug;
import com.util.EJBCommon;
import com.util.EJBHomeFactory;

/**
 * @ejb:bean name="ApRepVoucherEditListControllerEJB"
 *           display-name="Used for editing voucher list transactions"
 *           type="Stateless"
 *           view-type="remote"
 *           jndi-name="ejb/ApRepVoucherEditListControllerEJB"
 *
 * @ejb:interface remote-class="com.ejb.txn.ApRepVoucherEditListController"
 *                extends="javax.ejb.EJBObject"
 *
 * @ejb:home remote-class="com.ejb.txn.ApRepVoucherEditListControllerHome"
 *           extends="javax.ejb.EJBHome"
 *
 * @ejb:transaction type="Required"
 * 
*/

public class ApRepVoucherEditListControllerBean extends AbstractSessionBean {
    
    
    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList executeApRepVoucherEditList(ArrayList vouCodeList, Integer AD_CMPNY) 
        throws GlobalNoRecordFoundException {
                    
        Debug.print("ApRepVoucherPrintControllerBean executeApRepVoucherEditList");
        
        LocalApVoucherHome apVoucherHome = null;       
        LocalAdPreferenceHome adPreferenceHome = null;
        
        ArrayList list = new ArrayList();
                
        // Initialize EJB Home
        
        try {
            
            apVoucherHome = (LocalApVoucherHome)EJBHomeFactory.
                lookUpLocalHome(LocalApVoucherHome.JNDI_NAME, LocalApVoucherHome.class);
            adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
			  lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);
            
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }        
        
        try {
        	
        	Iterator i = vouCodeList.iterator();
        	
        	while (i.hasNext()) {
        	
        	    Integer VOU_CODE = (Integer) i.next();
        	
	        	LocalApVoucher apVoucher = null;

	        	try {
	        		
	        		apVoucher = apVoucherHome.findByPrimaryKey(VOU_CODE);

	        	} catch (FinderException ex) {
	        		
	        		continue;
	        		
	        	}
	        		        	
	        	// get voucher distribution records
	        	
	        	Collection apDistributionRecords = apVoucher.getApDistributionRecords();        	            
	        	
	        	Iterator drIter = apDistributionRecords.iterator();
	        	LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);
	        	
	        	while (drIter.hasNext()) {
	        		
	        		LocalApDistributionRecord apDistributionRecord = (LocalApDistributionRecord)drIter.next();
	        		
	        		ApRepVoucherEditListDetails details = new ApRepVoucherEditListDetails();
	        			        			        		
	        		if (apVoucher.getVouDebitMemo() == EJBCommon.FALSE) {
	        			
	        			details.setVelVouType("VOU");
						details.setVelVouAmountDue(apVoucher.getVouAmountDue());
						details.setVelWithholdingTaxCode(apVoucher.getApWithholdingTaxCode().getWtcName());
						details.setVelCurrencySymbol(apVoucher.getGlFunctionalCurrency().getFcSymbol());
	        			
	        		} else {
	        			
	        			details.setVelVouType("DM");
						details.setVelVouAmountDue(apVoucher.getVouBillAmount());						
						
	        		}
	        		
	        		details.setVelVouDateCreated(apVoucher.getVouDateCreated());
	        		details.setVelVouCreatedBy(apVoucher.getVouCreatedBy());
	        		details.setVelVouDocumentNumber(apVoucher.getVouDocumentNumber());
	        		details.setVelVouReferenceNumber(apVoucher.getVouReferenceNumber());
	        		details.setVelVouDate(apVoucher.getVouDate());
	        		details.setVelSplSupplierCode(apVoucher.getApSupplier().getSplSupplierCode());
	        		details.setVelVouBatchName(apVoucher.getApVoucherBatch() != null ? apVoucher.getApVoucherBatch().getVbName() : "N/A");
	        		details.setVelVouBatchDescription(apVoucher.getApVoucherBatch() != null ? apVoucher.getApVoucherBatch().getVbDescription() :"N/A");
	        		details.setVelVouTransactionTotal(apVoucher.getApVoucherBatch() != null ? apVoucher.getApVoucherBatch().getApVouchers().size() : 0);
	        		details.setVelDrAccountNumber(apDistributionRecord.getGlChartOfAccount().getCoaAccountNumber());
	        		details.setVelDrClass(apDistributionRecord.getDrClass());
	        		details.setVelDrDebit(apDistributionRecord.getDrDebit());
	        		details.setVelDrAmount(apDistributionRecord.getDrAmount());	        		
				    details.setVelDrAccountDescription(apDistributionRecord.getGlChartOfAccount().getCoaAccountDescription());
				    details.setVelVouDescription(apVoucher.getVouDescription());
				    
				    details.setVelSplSupplierName(apVoucher.getApSupplier().getSplName());
				    
				    
	        		if(apVoucher.getVouApprovedRejectedBy() == null || apVoucher.getVouApprovedRejectedBy().equals("")) {
	        			
	        			details.setVelApprovedBy(adPreference.getPrfApDefaultApprover());
	        			
	        		} else {
	        			
	        			details.setVelApprovedBy(apVoucher.getVouApprovedRejectedBy());
	        			
	        		}
				    
	        		list.add(details);

	        	}
	        	
	        }
	        
	        if (list.isEmpty()) {
	        	
	        	throw new GlobalNoRecordFoundException();
	        	
	        }        	       	
        	
        	return list;        	
        	        	
        } catch (GlobalNoRecordFoundException ex) {
        	
        	throw ex;
        	        	
        } catch (Exception ex) {
        	
        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());
        	
        }
            
    }
    
	/**
	* @ejb:interface-method view-type="remote"
	* @jboss:method-attributes read-only="true"
	**/
	public com.util.AdCompanyDetails getAdCompany(Integer AD_CMPNY) {
	
	  Debug.print("ApRepVoucherPrintControllerBean getAdCompany");      
	  
	  LocalAdCompanyHome adCompanyHome = null;
	        
	  // Initialize EJB Home
	    
	  try {
	        
	       adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
	           lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
	        
	  } catch (NamingException ex) {
	        
	      throw new EJBException(ex.getMessage());
	        
	  }
	
	  try {
	      
	     LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
	     
	     AdCompanyDetails details = new AdCompanyDetails();
	     details.setCmpName(adCompany.getCmpName());
	     details.setCmpAddress(adCompany.getCmpAddress());
	     details.setCmpCity(adCompany.getCmpCity());
	     details.setCmpCountry(adCompany.getCmpCountry());
	     details.setCmpPhone(adCompany.getCmpPhone());
	     
	     return details;  	
	  	       	
	  } catch (Exception ex) {
	  	
	  	 Debug.printStackTrace(ex);
	  	 throw new EJBException(ex.getMessage());
	  	  
	  }
	  
	}    

    // SessionBean methods

    /**
     * @ejb:create-method view-type="remote"
     **/
    public void ejbCreate() throws CreateException {

       Debug.print("ApRepVoucherPrintControllerBean ejbCreate");
      
    }
}
