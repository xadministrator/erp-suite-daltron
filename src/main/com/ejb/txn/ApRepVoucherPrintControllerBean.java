
/*
 * ApRepVoucherPrintControllerBean.java
 *
 * Created on February 24, 2004, 9:02 AM
 *
 * @author  Neil Andrew M. Ajero
 */

package com.ejb.txn;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.StringTokenizer;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;
import javax.naming.NamingException;

import com.ejb.ad.LocalAdApproval;
import com.ejb.ad.LocalAdApprovalDocument;
import com.ejb.ad.LocalAdApprovalDocumentHome;
import com.ejb.ad.LocalAdApprovalHome;
import com.ejb.ad.LocalAdCompany;
import com.ejb.ad.LocalAdCompanyHome;
import com.ejb.ad.LocalAdPreference;
import com.ejb.ad.LocalAdPreferenceHome;
import com.ejb.ad.LocalAdUser;
import com.ejb.ad.LocalAdUserHome;
import com.ejb.ap.LocalApDistributionRecord;
import com.ejb.ap.LocalApDistributionRecordHome;
import com.ejb.ap.LocalApTaxCode;
import com.ejb.ap.LocalApVoucher;
import com.ejb.ap.LocalApVoucherHome;
import com.ejb.ap.LocalApVoucherLineItem;
import com.ejb.ap.LocalApPurchaseOrder;
import com.ejb.ap.LocalApPurchaseOrderHome;
import com.ejb.ap.LocalApPurchaseOrderLine;
import com.ejb.ap.LocalApPurchaseOrderLineHome;
import com.ejb.ap.LocalApVoucherPaymentSchedule;
import com.ejb.ar.ArDistributionRecordBean;
import com.ejb.exception.GenVSVNoValueSetValueFoundException;
import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.genfld.LocalGenField;
import com.ejb.genfld.LocalGenSegment;
import com.ejb.genfld.LocalGenSegmentHome;
import com.ejb.genfld.LocalGenValueSet;
import com.ejb.genfld.LocalGenValueSetValue;
import com.ejb.genfld.LocalGenValueSetValueHome;
import com.ejb.inv.LocalInvTag;
import com.ejb.inv.LocalInvTagHome;
import com.ejb.pm.LocalPmProjectHome;
import com.ejb.pm.LocalPmProjectPhase;
import com.ejb.pm.LocalPmProjectPhaseHome;
import com.ejb.pm.LocalPmProjectTypeHome;
import com.util.AbstractSessionBean;
import com.util.AdCompanyDetails;
import com.util.ApRepCheckVoucherPrintDetails;
import com.util.ApRepVoucherPrintDetails;
import com.util.Debug;
import com.util.EJBCommon;
import com.util.EJBHomeFactory;
import com.ejb.ad.LocalAdBranch;
import com.ejb.ad.LocalAdBranchHome;

/**
 * @ejb:bean name="ApRepVoucherPrintControllerEJB"
 *           display-name="Used for printing voucher transactions"
 *           type="Stateless"
 *           view-type="remote"
 *           jndi-name="ejb/ApRepVoucherPrintControllerEJB"
 *
 * @ejb:interface remote-class="com.ejb.txn.ApRepVoucherPrintController"
 *                extends="javax.ejb.EJBObject"
 *
 * @ejb:home remote-class="com.ejb.txn.ApRepVoucherPrintControllerHome"
 *           extends="javax.ejb.EJBHome"
 *
 * @ejb:transaction type="Required"
 *
 */

public class ApRepVoucherPrintControllerBean extends AbstractSessionBean {


	/**
	 * @ejb:interface-method view-type="remote"
	 **/
	public ArrayList executeApRepVoucherPrint(ArrayList vouCodeList, Integer AD_CMPNY)
	throws GlobalNoRecordFoundException,
	GenVSVNoValueSetValueFoundException {

		Debug.print("ApRepVoucherPrintControllerBean executeApRepVoucherPrint");

		LocalApVoucherHome apVoucherHome = null;
		LocalAdCompanyHome adCompanyHome = null;
		LocalGenValueSetValueHome genValueSetValueHome = null;
		LocalGenSegmentHome genSegmentHome = null;
		LocalAdApprovalHome adApprovalHome = null;
		LocalAdApprovalDocumentHome adApprovalDocumentHome = null;
		LocalApDistributionRecordHome apDistributionRecordHome = null;
		LocalAdPreferenceHome adPreferenceHome = null;
		LocalAdBranchHome adBranchHome = null;
		LocalAdUserHome adUserHome = null;
		LocalApPurchaseOrderHome apPurchaseOrderHome = null;
		LocalApPurchaseOrderLineHome apPurchaseOrderLineHome = null;
		LocalPmProjectHome pmProjectHome = null;
		LocalPmProjectTypeHome pmProjectTypeHome = null;
		LocalPmProjectPhaseHome pmProjectPhaseHome = null;
		LocalInvTagHome invTagHome = null;

		ArrayList list = new ArrayList();

		// Initialize EJB Home

		try {

			apVoucherHome = (LocalApVoucherHome)EJBHomeFactory.
			lookUpLocalHome(LocalApVoucherHome.JNDI_NAME, LocalApVoucherHome.class);
			adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
			genValueSetValueHome = (LocalGenValueSetValueHome)EJBHomeFactory.
			lookUpLocalHome(LocalGenValueSetValueHome.JNDI_NAME, LocalGenValueSetValueHome.class);
			genSegmentHome = (LocalGenSegmentHome)EJBHomeFactory.
			lookUpLocalHome(LocalGenSegmentHome.JNDI_NAME, LocalGenSegmentHome.class);
			adApprovalHome = (LocalAdApprovalHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdApprovalHome.JNDI_NAME, LocalAdApprovalHome.class);
			adApprovalDocumentHome = (LocalAdApprovalDocumentHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdApprovalDocumentHome.JNDI_NAME, LocalAdApprovalDocumentHome.class);
			apDistributionRecordHome = (LocalApDistributionRecordHome)EJBHomeFactory.
			lookUpLocalHome(LocalApDistributionRecordHome.JNDI_NAME, LocalApDistributionRecordHome.class);
			adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);
			adBranchHome = (LocalAdBranchHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdBranchHome.JNDI_NAME, LocalAdBranchHome.class);
			adUserHome = (LocalAdUserHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdUserHome.JNDI_NAME, LocalAdUserHome.class);
			apPurchaseOrderLineHome = (LocalApPurchaseOrderLineHome)EJBHomeFactory.
			lookUpLocalHome(LocalApPurchaseOrderLineHome.JNDI_NAME, LocalApPurchaseOrderLineHome.class);
			apPurchaseOrderHome = (LocalApPurchaseOrderHome)EJBHomeFactory.
			lookUpLocalHome(LocalApPurchaseOrderHome.JNDI_NAME, LocalApPurchaseOrderHome.class);
			pmProjectHome = (LocalPmProjectHome)EJBHomeFactory.
			lookUpLocalHome(LocalPmProjectHome.JNDI_NAME, LocalPmProjectHome.class);
			pmProjectTypeHome = (LocalPmProjectTypeHome)EJBHomeFactory.
			lookUpLocalHome(LocalPmProjectTypeHome.JNDI_NAME, LocalPmProjectTypeHome.class);
			pmProjectPhaseHome = (LocalPmProjectPhaseHome)EJBHomeFactory.
			lookUpLocalHome(LocalPmProjectPhaseHome.JNDI_NAME, LocalPmProjectPhaseHome.class);
			invTagHome = (LocalInvTagHome)EJBHomeFactory.
			lookUpLocalHome(LocalInvTagHome.JNDI_NAME, LocalInvTagHome.class);


		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}

		try {

			Iterator i = vouCodeList.iterator();

			while (i.hasNext()) {

				Integer VOU_CODE = (Integer) i.next();

				LocalApVoucher apVoucher = null;

				try {

					apVoucher = apVoucherHome.findByPrimaryKey(VOU_CODE);

				} catch (FinderException ex) {

					continue;

				}

				LocalAdApproval adApproval = adApprovalHome.findByAprAdCompany(AD_CMPNY);
				LocalAdApprovalDocument adApprovalDocument = adApprovalDocumentHome.findByAdcType("AP VOUCHER", AD_CMPNY);

				if (adApprovalDocument.getAdcPrintOption().equals("PRINT APPROVED ONLY")) {

					if (apVoucher.getVouApprovalStatus() == null ||
							apVoucher.getVouApprovalStatus().equals("PENDING")) {

						continue;

					}


				} else if (adApprovalDocument.getAdcPrintOption().equals("PRINT UNAPPROVED ONLY")) {

					if (apVoucher.getVouApprovalStatus() != null &&
							(apVoucher.getVouApprovalStatus().equals("N/A") ||
									apVoucher.getVouApprovalStatus().equals("APPROVED"))) {

						continue;

					}

				}

				if (adApprovalDocument.getAdcAllowDuplicate() == EJBCommon.FALSE &&
						apVoucher.getVouPrinted() == EJBCommon.TRUE){

					continue;

				}

				// show duplicate

				boolean showDuplicate = false;

				if (adApprovalDocument.getAdcTrackDuplicate() == EJBCommon.TRUE &&
						apVoucher.getVouPrinted() == EJBCommon.TRUE) {

					showDuplicate = true;

				}

				// set printed

				apVoucher.setVouPrinted(EJBCommon.TRUE);

				String voucherType = apVoucher.getVouType();



				if(voucherType.equals("ITEMS")) {

					// get voucher line items
					
					Collection voucherLineItems = apVoucher.getApVoucherLineItems();

					Iterator vliIter = voucherLineItems.iterator();

					LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);

					while (vliIter.hasNext()) {

						LocalApVoucherLineItem apVoucherLineItem = (LocalApVoucherLineItem)vliIter.next();

						ApRepVoucherPrintDetails details = new ApRepVoucherPrintDetails();

						details.setVpVouType(apVoucher.getVouType());
						details.setVpVouSplName(apVoucher.getApSupplier().getSplName());
						details.setVpScSupplierClassName(apVoucher.getApSupplier().getApSupplierClass().getScName());
						details.setVpVouDate(apVoucher.getVouDate());
						System.out.println("getVouDateCreated(): "+apVoucher.getVouDateCreated());
						details.setVpVouDateCreated(apVoucher.getVouDateCreated());
						details.setVpVouSplAddress(apVoucher.getApSupplier().getSplAddress());
						details.setVpVouDocumentNumber(apVoucher.getVouDocumentNumber());
						details.setVpVouSplTin(apVoucher.getApSupplier().getSplTin());
						details.setVpVouReferenceNumber(apVoucher.getVouReferenceNumber());
						details.setVpVouDescription(apVoucher.getVouDescription());
						details.setVpVouAmountDue(apVoucher.getVouAmountDue()*apVoucher.getVouConversionRate());
						LocalAdUser adUsers = adUserHome.findByUsrName(apVoucher.getVouCreatedBy(), AD_CMPNY);
						details.setVpVouCreatedBy(adUsers.getUsrName());
						details.setVpVouSplCode(apVoucher.getApSupplier().getSplSupplierCode());
						details.setVpVouScVatReliefVoucherItem(apVoucher.getApSupplier().getApSupplierClass().getScIsVatReliefVoucherItem());;
						details.setVpVliItemCode(apVoucherLineItem.getInvItemLocation().getInvItem().getIiName());
						details.setVpVliItemDescription(apVoucherLineItem.getInvItemLocation().getInvItem().getIiDescription());
						details.setVpVliLocation(apVoucherLineItem.getInvItemLocation().getInvLocation().getLocName());
						details.setVpVliUnit(apVoucherLineItem.getInvUnitOfMeasure().getUomShortName());
						details.setVpVliUnitAmount(apVoucherLineItem.getVliUnitCost());
						details.setVpVliAmount(apVoucherLineItem.getVliAmount());
						details.setVpVliDiscount(apVoucherLineItem.getVliTotalDiscount());

						details.setVpVliVatRelief(apVoucherLineItem.getInvItemLocation().getInvItem().getIiIsVatRelief());
						details.setVpVliSupplierName(apVoucherLineItem.getVliSplName());
						details.setVpVliTin(apVoucherLineItem.getVliSplTin());
						details.setVpVliAddress(apVoucherLineItem.getVliSplAddress());

						if(apVoucherLineItem.getPmProject()!=null) {
							details.setVpVliProjectCode(apVoucherLineItem.getPmProject().getPrjProjectCode());
							details.setVpVliProjectName(apVoucherLineItem.getPmProject().getPrjName());
							details.setVpVliProjectClientId(apVoucherLineItem.getPmProject().getPrjClientID());
						}



						details.setVpVliProjectTypeCode(apVoucherLineItem.getPmProjectTypeType() != null ? apVoucherLineItem.getPmProjectTypeType().getPmProjectType().getPtProjectTypeCode(): "");
						details.setVpVliProjectTypeValue(apVoucherLineItem.getPmProjectTypeType() != null ? apVoucherLineItem.getPmProjectTypeType().getPmProjectType().getPtValue(): "");
						details.setVpVliProjectPhaseName(apVoucherLineItem.getPmProjectPhase() != null ? apVoucherLineItem.getPmProjectPhase().getPpName() : "");

						details.setVpVliTax(apVoucherLineItem.getVliTax());



						if(apVoucher.getVouApprovedRejectedBy() == null || apVoucher.getVouApprovedRejectedBy().equals("")) {

							details.setVpVouApprovedBy(adPreference.getPrfApDefaultApprover());

						} else {

							details.setVpVouApprovedBy(apVoucher.getVouApprovedRejectedBy());

						}

						double tax = 0d;
						double quantity = 0d;

						// get purchase order lines

						Collection apPurchaseOrderLines = apVoucher.getApPurchaseOrderLines();

						Iterator ilIter = apPurchaseOrderLines.iterator();

						while (ilIter.hasNext()) {

							LocalApPurchaseOrderLine apPurchaseOrderLine = (LocalApPurchaseOrderLine)ilIter.next();
							System.out.println("AP POcode: " + apPurchaseOrderLine.getApPurchaseOrder().getPoCode().toString());
							details.setVpPoReferenceNumber(apPurchaseOrderLine.getApPurchaseOrder().getPoReferenceNumber());

							if (apPurchaseOrderLine.getApVoucher().getVouCode()!=null) {
								if (apVoucher.getVouCode().toString().equals(apPurchaseOrderLine.getApVoucher().getVouCode().toString())) {
									quantity = apPurchaseOrderLine.getPlQuantity();
									System.out.println("quantity: " + apPurchaseOrderLine.getPlQuantity());
									details.setVpPlAmount(apPurchaseOrderLine.getPlAmount());
									tax += apPurchaseOrderLine.getPlTaxAmount();
								}

							} else {
								details.setVpPlAmount(0d);
								details.setVpPlTaxAmount(0d);
							}


						}



						details.setVpPlQuantity(quantity);
						details.setVpPlTaxAmount(tax);


						details.setVpVouCheckedBy(adPreference.getPrfApDefaultChecker());

						details.setVpVouTerms(apVoucher.getAdPaymentTerm().getPytName());
						details.setVpShowDuplicate(showDuplicate ? EJBCommon.TRUE : EJBCommon.FALSE);
						details.setVpPoNumber(apVoucher.getVouPoNumber());

						Collection apVoucherPaymentSchedules = apVoucher.getApVoucherPaymentSchedules();
						ArrayList apVoucherPaymentScheduleList = new ArrayList(apVoucherPaymentSchedules);

						LocalApVoucherPaymentSchedule apVoucherPaymentSchedule =
							(LocalApVoucherPaymentSchedule)apVoucherPaymentScheduleList.get(apVoucherPaymentScheduleList.size() - 1);

						details.setVpVouDueDate(apVoucherPaymentSchedule.getVpsDueDate());
						details.setVpApprovalStatus(apVoucher.getVouApprovalStatus());
						details.setVpPosted(apVoucher.getVouPosted());

						// amount w/o tax
						double NET_AMOUNT = 0d;
						LocalApTaxCode apTaxCode = apVoucher.getApTaxCode();

						if(apTaxCode.getTcType().equals("INCLUSIVE") || apTaxCode.getTcType().equals("EXCLUSIVE")) {

							NET_AMOUNT = EJBCommon.roundIt(apVoucher.getVouAmountDue() / (1 + (apTaxCode.getTcRate() / 100)), this.getGlFcPrecisionUnit(AD_CMPNY));

						} else {

							NET_AMOUNT = apVoucher.getVouAmountDue();

						}

						details.setVpAmountWoTax(NET_AMOUNT);
						details.setVpVouWithholdingTaxName(apVoucher.getApWithholdingTaxCode().getWtcName());
						details.setVpVouTaxName(apVoucher.getApTaxCode().getTcName());
						//details.setVpVouPostedBy(apVoucher.getVouPostedBy());



						details.setVpVouFcSymbol(apVoucher.getGlFunctionalCurrency().getFcSymbol());

						//Include Branch
						LocalAdBranch adBranch = adBranchHome.findByPrimaryKey(apVoucher.getVouAdBranch());
						details.setVpBrName(adBranch.getBrName());
						details.setVpBrCode(adBranch.getBrBranchCode());

						// get user name description

						System.out.println("apVoucher.getChkCreatedBy(): "+apVoucher.getVouCreatedBy());
						try{
							LocalAdUser adUser = adUserHome.findByUsrName(apVoucher.getVouCreatedBy(), AD_CMPNY);
							details.setVpChkCreatedByDescription(adUser.getUsrName());
						}catch(Exception e){
							details.setVpChkCreatedByDescription("");
						}

						try{
							System.out.println("adPreference.getPrfApDefaultChecker(): "+adPreference.getPrfApDefaultChecker());
							LocalAdUser adUser2 = adUserHome.findByUsrName(adPreference.getPrfApDefaultChecker(), AD_CMPNY);
							details.setVpChkCheckByDescription(adUser2.getUsrDescription());
						}catch(Exception e){
							details.setVpChkCheckByDescription("");
						}

						try{
							System.out.println("apVoucher.getVouApprovedRejectedBy(): "+apVoucher.getVouApprovedRejectedBy());
							LocalAdUser adUser3 = adUserHome.findByUsrName(apVoucher.getVouApprovedRejectedBy(), AD_CMPNY);
							details.setVpChkApprovedRejectedByDescription(adUser3.getUsrDescription());

						}catch(Exception e){
							details.setVpChkApprovedRejectedByDescription("");
						}

	    				try{
	    					System.out.println("4: " + apVoucher.getVouPostedBy());
	    					LocalAdUser adUser4 = adUserHome.findByUsrName(apVoucher.getVouPostedBy(), AD_CMPNY);
	        				details.setVpVouPostedBy(adUser4.getUsrDescription());
	        				System.out.println("postedBy: " + adUser4.getUsrDescription());
	    				}catch(Exception e){
	    					details.setVpVouPostedBy("");
	    				}



	    				//trace misc
            			if(apVoucherLineItem.getInvItemLocation().getInvItem().getIiTraceMisc()==EJBCommon.TRUE) {



    						if(apVoucherLineItem.getInvTags().size()>0) {
    							System.out.println("new code");
    							StringBuilder strBProperty = new StringBuilder();
    							StringBuilder strBSerial = new StringBuilder();
    							StringBuilder strBSpecs = new StringBuilder();
    							StringBuilder strBCustodian= new StringBuilder();
    							StringBuilder strBExpirationDate = new StringBuilder();
    							Iterator it = apVoucherLineItem.getInvTags().iterator();


    							while(it.hasNext()) {

    								LocalInvTag invTag = (LocalInvTag)it.next();

    								//property code
    								if(!invTag.getTgPropertyCode().trim().equals("")) {
    									strBProperty.append(invTag.getTgPropertyCode());
    									strBProperty.append(System.getProperty("line.separator"));
    								}

    								//serial

    								if(!invTag.getTgSerialNumber().trim().equals("")) {
    									strBSerial.append(invTag.getTgSerialNumber());
    									strBSerial.append(System.getProperty("line.separator"));
    								}

    								//spec

    								if(!invTag.getTgSpecs().trim().equals("")) {
    									strBSpecs.append(invTag.getTgSpecs());
    									strBSpecs.append(System.getProperty("line.separator"));
    								}

    								//custodian

    								if(invTag.getAdUser()!= null) {
    									strBCustodian.append(invTag.getAdUser().getUsrName());
    									strBCustodian.append(System.getProperty("line.separator"));
    								}

    								//exp date

    								if(invTag.getTgExpiryDate()!=null) {
    									strBExpirationDate.append(invTag.getTgExpiryDate());
    									strBExpirationDate.append(System.getProperty("line.separator"));

    								}



    							}
    							//property code
            					details.setVpVliPropertyCode(strBProperty.toString());
            					//serial number
            					details.setVpVliSerialNumber(strBSerial.toString());
            					//specs
            					details.setVpVliSpecs(strBSpecs.toString());
            					//custodian
            					details.setVpVliCustodian(strBCustodian.toString());
            					//expiration date
            					details.setVpVliExpiryDate(strBExpirationDate.toString());



    						}


            			}






						list.add(details);

					}










				}else {

					// get distribution records

					Collection apDistributionRecords = apDistributionRecordHome.findByVouCode(apVoucher.getVouCode(), AD_CMPNY);

					Iterator drIter = apDistributionRecords.iterator();

					LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);

					while (drIter.hasNext()) {

						LocalApDistributionRecord apDistributionRecord = (LocalApDistributionRecord)drIter.next();

						ApRepVoucherPrintDetails details = new ApRepVoucherPrintDetails();
						details.setVpVouType(apVoucher.getVouType());
						details.setVpVouSplName(apVoucher.getApSupplier().getSplName());
						details.setVpScSupplierClassName(apVoucher.getApSupplier().getApSupplierClass().getScName());
						details.setVpVouDate(apVoucher.getVouDate());
						System.out.println("getVouDateCreated(): "+apVoucher.getVouDateCreated());
						details.setVpVouDateCreated(apVoucher.getVouDateCreated());
						details.setVpVouSplAddress(apVoucher.getApSupplier().getSplAddress());
						details.setVpVouDocumentNumber(apVoucher.getVouDocumentNumber());
						details.setVpVouSplTin(apVoucher.getApSupplier().getSplTin());
						details.setVpVouReferenceNumber(apVoucher.getVouReferenceNumber());
						details.setVpVouDescription(apVoucher.getVouDescription());
				//		details.setVpVouAmountDue(apVoucher.getVouAmountDue()*apVoucher.getVouConversionRate());
						
						details.setVpVouAmountDue(apVoucher.getVouAmountDue());
						LocalAdUser adUsers = adUserHome.findByUsrName(apVoucher.getVouCreatedBy(), AD_CMPNY);
						details.setVpVouCreatedBy(adUsers.getUsrName());
						details.setVpVouSplCode(apVoucher.getApSupplier().getSplSupplierCode());

						if(apVoucher.getVouApprovedRejectedBy() == null || apVoucher.getVouApprovedRejectedBy().equals("")) {

							details.setVpVouApprovedBy(adPreference.getPrfApDefaultApprover());

						} else {

							details.setVpVouApprovedBy(apVoucher.getVouApprovedRejectedBy());

						}

							double tax = 0d;
							double quantity = 0d;

								// get purchase order lines

								Collection apPurchaseOrderLines = apVoucher.getApPurchaseOrderLines();

								Iterator ilIter = apPurchaseOrderLines.iterator();

								while (ilIter.hasNext()) {

									LocalApPurchaseOrderLine apPurchaseOrderLine = (LocalApPurchaseOrderLine)ilIter.next();
									System.out.println("AP POcode: " + apPurchaseOrderLine.getApPurchaseOrder().getPoCode().toString());
									details.setVpPoReferenceNumber(apPurchaseOrderLine.getApPurchaseOrder().getPoReferenceNumber());

									if (apPurchaseOrderLine.getApVoucher().getVouCode()!=null) {
										if (apVoucher.getVouCode().toString().equals(apPurchaseOrderLine.getApVoucher().getVouCode().toString())) {
											quantity = apPurchaseOrderLine.getPlQuantity();
											System.out.println("quantity: " + apPurchaseOrderLine.getPlQuantity());
											details.setVpPlAmount(apPurchaseOrderLine.getPlAmount());
											tax += apPurchaseOrderLine.getPlTaxAmount();
										}

									} else {
										details.setVpPlAmount(0d);
										details.setVpPlTaxAmount(0d);
									}

									if (apDistributionRecord.getApPurchaseOrder()!=null && (apPurchaseOrderLine.getApPurchaseOrder().getPoCode().equals(apDistributionRecord.getApPurchaseOrder().getPoCode()))) {
										System.out.println("aaaaaaaaaa");
										details.setVpDrTaxCoaAccountDescription(apDistributionRecord.getGlChartOfAccount().getCoaAccountDescription());
									}
								}



							details.setVpPlQuantity(quantity);
							details.setVpPlTaxAmount(tax);


						details.setVpVouCheckedBy(adPreference.getPrfApDefaultChecker());
						details.setVpDrCoaAccountNumber(apDistributionRecord.getGlChartOfAccount().getCoaAccountNumber());
						details.setVpDrCoaAccountDescription(apDistributionRecord.getGlChartOfAccount().getCoaAccountDescription());

						details.setVpDrCoaAccountDescription(apDistributionRecord.getGlChartOfAccount().getCoaAccountDescription());
						details.setVpDrDebit(apDistributionRecord.getDrDebit());
					//	details.setVpDrAmount(apDistributionRecord.getDrAmount());
						
						details.setVpDrAmount(apDistributionRecord.getDrAmount()*apVoucher.getVouConversionRate());
						details.setVpVouTerms(apVoucher.getAdPaymentTerm().getPytName());
						details.setVpShowDuplicate(showDuplicate ? EJBCommon.TRUE : EJBCommon.FALSE);
						details.setVpPoNumber(apVoucher.getVouPoNumber());

						Collection apVoucherPaymentSchedules = apVoucher.getApVoucherPaymentSchedules();
						ArrayList apVoucherPaymentScheduleList = new ArrayList(apVoucherPaymentSchedules);

						LocalApVoucherPaymentSchedule apVoucherPaymentSchedule =
							(LocalApVoucherPaymentSchedule)apVoucherPaymentScheduleList.get(apVoucherPaymentScheduleList.size() - 1);

						details.setVpVouDueDate(apVoucherPaymentSchedule.getVpsDueDate());
						details.setVpApprovalStatus(apVoucher.getVouApprovalStatus());
						details.setVpPosted(apVoucher.getVouPosted());

						// amount w/o tax
						double NET_AMOUNT = 0d;
						LocalApTaxCode apTaxCode = apVoucher.getApTaxCode();

						if(apTaxCode.getTcType().equals("INCLUSIVE") || apTaxCode.getTcType().equals("EXCLUSIVE")) {

							NET_AMOUNT = EJBCommon.roundIt(apVoucher.getVouAmountDue() / (1 + (apTaxCode.getTcRate() / 100)), this.getGlFcPrecisionUnit(AD_CMPNY));

						} else {

							NET_AMOUNT = apVoucher.getVouAmountDue();

						}

						details.setVpAmountWoTax(NET_AMOUNT);
						details.setVpVouWithholdingTaxName(apVoucher.getApWithholdingTaxCode().getWtcName());
						details.setVpVouTaxName(apVoucher.getApTaxCode().getTcName());
						//details.setVpVouPostedBy(apVoucher.getVouPostedBy());

						//get natural account desc
						LocalGenValueSetValue genValueSetValue =
							this.getGenValueSetValue(apDistributionRecord.getGlChartOfAccount().getCoaAccountNumber(), AD_CMPNY);
						details.setVpDrCoaNaturalDesc(genValueSetValue.getVsvDescription());

						details.setVpVouFcSymbol(apVoucher.getGlFunctionalCurrency().getFcSymbol());

						//Include Branch
						LocalAdBranch adBranch = adBranchHome.findByPrimaryKey(apVoucher.getVouAdBranch());
						details.setVpBrName(adBranch.getBrName());
						details.setVpBrCode(adBranch.getBrBranchCode());

						// get user name description

						System.out.println("apVoucher.getChkCreatedBy(): "+apVoucher.getVouCreatedBy());
						try{
							LocalAdUser adUser = adUserHome.findByUsrName(apVoucher.getVouCreatedBy(), AD_CMPNY);
							details.setVpChkCreatedByDescription(adUser.getUsrName());
						}catch(Exception e){
							details.setVpChkCreatedByDescription("");
						}

						try{
							System.out.println("adPreference.getPrfApDefaultChecker(): "+adPreference.getPrfApDefaultChecker());
							LocalAdUser adUser2 = adUserHome.findByUsrName(adPreference.getPrfApDefaultChecker(), AD_CMPNY);
							details.setVpChkCheckByDescription(adUser2.getUsrDescription());
						}catch(Exception e){
							details.setVpChkCheckByDescription("");
						}

						try{
							System.out.println("apVoucher.getVouApprovedRejectedBy(): "+apVoucher.getVouApprovedRejectedBy());
							LocalAdUser adUser3 = adUserHome.findByUsrName(apVoucher.getVouApprovedRejectedBy(), AD_CMPNY);
							details.setVpChkApprovedRejectedByDescription(adUser3.getUsrDescription());

						}catch(Exception e){
							details.setVpChkApprovedRejectedByDescription("");
						}

	    				try{
	    					System.out.println("4: " + apVoucher.getVouPostedBy());
	    					LocalAdUser adUser4 = adUserHome.findByUsrName(apVoucher.getVouPostedBy(), AD_CMPNY);
	        				details.setVpVouPostedBy(adUser4.getUsrDescription());
	        				System.out.println("postedBy: " + adUser4.getUsrDescription());
	    				}catch(Exception e){
	    					details.setVpVouPostedBy("");
	    				}

						list.add(details);

					}
				}







			}

			if (list.isEmpty()) {

				throw new GlobalNoRecordFoundException();

			}


			Collections.sort(list, ApRepVoucherPrintDetails.sortByAccount);

			return list;

		} catch (GlobalNoRecordFoundException ex) {

			throw ex;

		} catch (GenVSVNoValueSetValueFoundException ex){

			throw ex;

		} catch (Exception ex) {

			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());

		}

	}

	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public com.util.AdCompanyDetails getAdCompany(Integer AD_CMPNY) {

		Debug.print("ApRepVoucherPrintControllerBean getAdCompany");

		LocalAdCompanyHome adCompanyHome = null;

		// Initialize EJB Home

		try {

			adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}

		try {

			LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);

			AdCompanyDetails details = new AdCompanyDetails();
			details.setCmpName(adCompany.getCmpName());
			details.setCmpAddress(adCompany.getCmpAddress());
			details.setCmpCity(adCompany.getCmpCity());
			details.setCmpCountry(adCompany.getCmpCountry());
			details.setCmpPhone(adCompany.getCmpPhone());

			return details;

		} catch (Exception ex) {

			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());

		}

	}

	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public short getGlFcPrecisionUnit(Integer AD_CMPNY) {

		Debug.print("ApRepVoucherPrintControllerBean getGlFcPrecisionUnit");


		LocalAdCompanyHome adCompanyHome = null;


		// Initialize EJB Home

		try {

			adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}

		try {

			LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);

			return  adCompany.getGlFunctionalCurrency().getFcPrecision();

		} catch (Exception ex) {

			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());

		}

	}

	private LocalGenValueSetValue getGenValueSetValue(String COA, Integer AD_CMPNY)
	throws GlobalNoRecordFoundException,
	GenVSVNoValueSetValueFoundException {

		Debug.print("ApRepVoucherPrintControllerBean getGenValueSetValue");

		LocalAdCompanyHome adCompanyHome = null;
		LocalGenSegmentHome genSegmentHome = null;
		LocalGenValueSetValueHome genValueSetValueHome = null;

		LocalAdCompany adCompany = null;
		LocalGenValueSetValue genValueSetValue = null;

		try {

			adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
			genSegmentHome = (LocalGenSegmentHome)EJBHomeFactory.
			lookUpLocalHome(LocalGenSegmentHome.JNDI_NAME, LocalGenSegmentHome.class);
			genValueSetValueHome = (LocalGenValueSetValueHome)EJBHomeFactory.
			lookUpLocalHome(LocalGenValueSetValueHome.JNDI_NAME, LocalGenValueSetValueHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}

		try {

			adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);

		} catch (Exception ex) {

			throw new EJBException(ex.getMessage());

		}

		try {

			// get coa separator
			LocalGenField genField = adCompany.getGenField();
			char chrSeparator = genField.getFlSegmentSeparator();
			String strSeparator =  String.valueOf(chrSeparator);

			// get natural account segment
			Collection genSegments = genSegmentHome.findByFlCode(genField.getFlCode(), AD_CMPNY);
			Iterator i = genSegments.iterator();
			LocalGenValueSet genValueSet = null;
			while(i.hasNext()){

				LocalGenSegment genSegment = (LocalGenSegment)i.next();
				if(genSegment.getSgSegmentType() == 'N') {
					genValueSet = genSegment.getGenValueSet();
					break;
				}

			}

			// get value set value
			StringTokenizer st = new StringTokenizer(COA, strSeparator);
			while (st.hasMoreTokens()) {

				try {
					genValueSetValue = genValueSetValueHome.findByVsCodeAndVsvValue(genValueSet.getVsCode(), st.nextToken(), AD_CMPNY);
				} catch (Exception ex) {

				}

				if(genValueSetValue != null)
					break;

			}

			if(genValueSetValue == null)
				throw new GenVSVNoValueSetValueFoundException();

			return  genValueSetValue;

		} catch (Exception ex) {

			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());

		}

	}

	// SessionBean methods

	/**
	 * @ejb:create-method view-type="remote"
	 **/
	public void ejbCreate() throws CreateException {

		Debug.print("ApRepVoucherPrintControllerBean ejbCreate");

	}
}
