
/*
 * ApRepWithholdingTaxCodeListControllerBean.java
 *
 * Created on March 01, 2005, 05:54 PM
 *
 * @author  Neil Andrew M. Ajero
 */

package com.ejb.txn;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.naming.NamingException;

import com.ejb.ad.LocalAdCompany;
import com.ejb.ad.LocalAdCompanyHome;
import com.ejb.ap.LocalApWithholdingTaxCode;
import com.ejb.ap.LocalApWithholdingTaxCodeHome;
import com.ejb.exception.GlobalNoRecordFoundException;
import com.util.AbstractSessionBean;
import com.util.AdCompanyDetails;
import com.util.ApRepWithholdingTaxCodeListDetails;
import com.util.Debug;
import com.util.EJBHomeFactory;

/**
 * @ejb:bean name="ApRepWithholdingTaxCodeListControllerEJB"
 *           display-name="Used for viewing withholding tax code lists"
 *           type="Stateless"
 *           view-type="remote"
 *           jndi-name="ejb/ApRepWithholdingTaxCodeListControllerEJB"
 *
 * @ejb:interface remote-class="com.ejb.txn.ApRepWithholdingTaxCodeListController"
 *                extends="javax.ejb.EJBObject"
 *
 * @ejb:home remote-class="com.ejb.txn.ApRepWithholdingTaxCodeListControllerHome"
 *           extends="javax.ejb.EJBHome"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="apuser"
 *                        role-link="apuserlink"
 *
 * @ejb:permission role-name="apuser"
 * 
*/

public class ApRepWithholdingTaxCodeListControllerBean extends AbstractSessionBean {


    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList executeApRepWithholdingTaxCodeList(HashMap criteria, String ORDER_BY, Integer AD_CMPNY) 
        throws GlobalNoRecordFoundException {
                    
        Debug.print("ApRepWithholdingTaxCodeListControllerBean executeApRepWithholdingTaxCodeList");
        
        LocalApWithholdingTaxCodeHome apWithholdingTaxCodeHome = null;
        
        ArrayList list = new ArrayList();
        
        //initialized EJB Home
        
        try {
            
        	apWithholdingTaxCodeHome = (LocalApWithholdingTaxCodeHome)EJBHomeFactory.
                lookUpLocalHome(LocalApWithholdingTaxCodeHome.JNDI_NAME, LocalApWithholdingTaxCodeHome.class);
         
        } catch (NamingException ex) 	{
            
            throw new EJBException(ex.getMessage());
            
        }
        
		try { 
           
		  StringBuffer jbossQl = new StringBuffer();
		  jbossQl.append("SELECT OBJECT(wtc) FROM ApWithholdingTaxCode wtc ");
		  
	      boolean firstArgument = true;
	      short ctr = 0;
		  int criteriaSize = criteria.size();	      
	      
	      Object obj[];	      
		
		  // Allocate the size of the object parameter
		
	      if (criteria.containsKey("taxName")) {
	      	
	      	 criteriaSize--;
	      	 
	      }

	      obj = new Object[criteriaSize];
		       	      
		  if (criteria.containsKey("taxName")) {
		  	
		  	 if (!firstArgument) {
		  	 
		  	    jbossQl.append("AND ");	
		  	 	
		     } else {
		     	
		     	firstArgument = false;
		     	jbossQl.append("WHERE ");
		     	
		     }
		     
		  	 jbossQl.append("wtc.wtcName LIKE '%" + (String)criteria.get("taxName") + "%' ");
		  	 
		  }
		  
	      if (!firstArgument) {		       	  	
	   	     jbossQl.append("AND ");		       	     
	   	  } else {		       	  	
	   	  	 firstArgument = false;
	   	  	 jbossQl.append("WHERE ");		       	  	 
	   	  }	     
	   	  
	   	  jbossQl.append("wtc.wtcAdCompany=" + AD_CMPNY + " ");

		  String orderBy = null;
          
          if (ORDER_BY.equals("TAX NAME")) {
	      	 
	      	  orderBy = "wtc.wtcName";
	      	  
	      } 

		  if (orderBy != null) {
		  
		  	jbossQl.append("ORDER BY " + orderBy);
		  	
		  }     
			  	      	
	      Collection apWithholdingTaxCodes = apWithholdingTaxCodeHome.getWtcByCriteria(jbossQl.toString(), obj);	         
		  
		  if (apWithholdingTaxCodes.size() == 0)
		     throw new GlobalNoRecordFoundException();
		     
		  Iterator i = apWithholdingTaxCodes.iterator();
		
		  while (i.hasNext()) {
		  	
		  	  LocalApWithholdingTaxCode apWithholdingTaxCode = (LocalApWithholdingTaxCode)i.next();   	  
		  	  
		  	  ApRepWithholdingTaxCodeListDetails details = new ApRepWithholdingTaxCodeListDetails();
		  	  details.setWtlTaxName(apWithholdingTaxCode.getWtcName());
		  	  details.setWtlTaxDescription(apWithholdingTaxCode.getWtcDescription());
		  	  details.setWtlTaxRate(apWithholdingTaxCode.getWtcRate());
		  	  
		  	  if (apWithholdingTaxCode.getGlChartOfAccount() != null) {
		  	  	
		  	  	 details.setWtlCoaGlTaxAccountNumber(apWithholdingTaxCode.getGlChartOfAccount().getCoaAccountNumber());	                		                	    
		  	  	 details.setWtlCoaGlTaxAccountDescription(apWithholdingTaxCode.getGlChartOfAccount().getCoaAccountDescription());
		  	  	
		  	  }
		  	  
		  	  details.setWtlEnable(apWithholdingTaxCode.getWtcEnable());

			  list.add(details);
			         
		  }
			     
		  return list;
		      
	  } catch (GlobalNoRecordFoundException ex) {
	  	 
	  	  throw ex;
	  	
	  } catch (Exception ex) {
	  	

	  	  ex.printStackTrace();
	  	  throw new EJBException(ex.getMessage());
	  	
	  }
        
    }
    
	/**
	* @ejb:interface-method view-type="remote"
	* @jboss:method-attributes read-only="true"
	**/
	public com.util.AdCompanyDetails getAdCompany(Integer AD_CMPNY) {
	
	  Debug.print("ApRepWithholdingTaxCodeListControllerBean getAdCompany");      
	  
	  LocalAdCompanyHome adCompanyHome = null;
	        
	  // Initialize EJB Home
	    
	  try {
	        
	       adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
	           lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
	        
	  } catch (NamingException ex) {
	        
	      throw new EJBException(ex.getMessage());
	        
	  }
	
	  try {
	      
	     LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
	     
	     AdCompanyDetails details = new AdCompanyDetails();
	     details.setCmpName(adCompany.getCmpName());
	     
	     return details;  	
	  	       	
	  } catch (Exception ex) {
	  	
	  	 Debug.printStackTrace(ex);
	  	 throw new EJBException(ex.getMessage());
	  	  
	  }
	  
	}    

    // SessionBean methods

    /**
     * @ejb:create-method view-type="remote"
     **/
    public void ejbCreate() throws CreateException {

       Debug.print("ApRepWithholdingTaxCodeListControllerBean ejbCreate");
      
    }
}
