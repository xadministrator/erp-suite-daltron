
/*
 * ApRepWithholdingTaxExpandedControllerBean.java
 *
 * Created on March 25, 2004, 4:03 PM
 *
 * @author  Neil Andrew M. Ajero
 */

package com.ejb.txn;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;
import javax.naming.NamingException;

import com.ejb.ad.LocalAdBranch;
import com.ejb.ad.LocalAdBranchHome;
import com.ejb.ad.LocalAdBranchResponsibility;
import com.ejb.ad.LocalAdBranchResponsibilityHome;
import com.ejb.ad.LocalAdCompany;
import com.ejb.ad.LocalAdCompanyHome;
import com.ejb.ap.LocalApDistributionRecord;
import com.ejb.ap.LocalApDistributionRecordHome;
import com.ejb.ap.LocalApWithholdingTaxCode;
import com.ejb.ap.LocalApWithholdingTaxCodeHome;
import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.gl.LocalGlFunctionalCurrencyRate;
import com.ejb.gl.LocalGlFunctionalCurrencyRateHome;
import com.ejb.gl.LocalGlJournalLine;
import com.ejb.gl.LocalGlJournalLineHome;
import com.ejb.gl.LocalGlTaxInterface;
import com.ejb.gl.LocalGlTaxInterfaceHome;
import com.util.AbstractSessionBean;
import com.util.AdBranchDetails;
import com.util.AdCompanyDetails;
import com.util.ApRepWithholdingTaxExpandedDetails;
import com.util.Debug;
import com.util.EJBCommon;
import com.util.EJBHomeFactory;

/**
 * @ejb:bean name="ApRepWithholdingTaxExpandedControllerEJB"
 *           display-name="Used for viewing withholding tax expanded summary"
 *           type="Stateless"
 *           view-type="remote"
 *           jndi-name="ejb/ApRepWithholdingTaxExpandedControllerEJB"
 *
 * @ejb:interface remote-class="com.ejb.txn.ApRepWithholdingTaxExpandedController"
 *                extends="javax.ejb.EJBObject"
 *
 * @ejb:home remote-class="com.ejb.txn.ApRepWithholdingTaxExpandedControllerHome"
 *           extends="javax.ejb.EJBHome"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="apuser"
 *                        role-link="apuserlink"
 *
 * @ejb:permission role-name="apuser"
 * 
*/

public class ApRepWithholdingTaxExpandedControllerBean extends AbstractSessionBean {
           
    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList executeApRepWithholdingTaxExpanded(HashMap criteria, ArrayList adBrnchList, Integer AD_CMPNY) 
        throws GlobalNoRecordFoundException {
    	
    	Debug.print("ApRepWithholdingTaxExpandedControllerBean executeApRepWithholdingTaxExpanded");
                
        LocalApDistributionRecordHome apDistributionRecordHome = null;
        LocalApWithholdingTaxCodeHome apWithholdingTaxCodeHome = null;
        LocalGlTaxInterfaceHome glTaxInterfaceHome = null;        
        LocalGlJournalLineHome glJournalHome = null;                        
        
        ArrayList list = new ArrayList();
        
        try {
        	        
            apDistributionRecordHome = (LocalApDistributionRecordHome)EJBHomeFactory.
				lookUpLocalHome(LocalApDistributionRecordHome.JNDI_NAME, LocalApDistributionRecordHome.class);
            apWithholdingTaxCodeHome = (LocalApWithholdingTaxCodeHome)EJBHomeFactory.
				lookUpLocalHome(LocalApWithholdingTaxCodeHome.JNDI_NAME,LocalApWithholdingTaxCodeHome.class);
            glTaxInterfaceHome = (LocalGlTaxInterfaceHome)EJBHomeFactory.
        		lookUpLocalHome(LocalGlTaxInterfaceHome.JNDI_NAME, LocalGlTaxInterfaceHome.class);
            glJournalHome = (LocalGlJournalLineHome)EJBHomeFactory.
				lookUpLocalHome(LocalGlJournalLineHome.JNDI_NAME,LocalGlJournalLineHome.class);                        
                                   
        } catch (NamingException ex) 	{
            
            throw new EJBException(ex.getMessage());
            
        }                        
        
        try { 
        	//get all available records
        	
        	StringBuffer jbossQl = new StringBuffer();
  		    
        	jbossQl.append("SELECT OBJECT(ti) FROM GlTaxInterface ti ");
  		  
  	        boolean firstArgument = true;
  	        short ctr = 0;
  		    int criteriaSize = criteria.size();
  		    Date dateFrom = null;
  		    Date dateTo = null;
  		    int SQNC_NMBR = 0;
  	      
  	        Object obj[];	      
  		
  		    // Allocate the size of the object parameter
  		  
  		    if (criteria.containsKey("supplierCode")) {
  	      	
  	      	   criteriaSize--;
  	      	 
  	        }  		    		    
  		    
  		    obj = new Object[criteriaSize];
        	
  		    if (criteria.containsKey("supplierCode")) {
		  	
		  	   if (!firstArgument) {
		  	 
		  	      jbossQl.append("AND ");	
		  	 	
		       } else {
		     	
		     	firstArgument = false;
		     	jbossQl.append("WHERE ");
		     	
		       }
		     
		  	   jbossQl.append("ti.tiSlSubledgerCode LIKE '%" + (String)criteria.get("supplierCode") + "%' ");
		  	 
		    }
        	        		        	
        	if (criteria.containsKey("dateFrom")) {
		      	
		  	   if (!firstArgument) {
		  	 	  jbossQl.append("AND ");
		  	   } else {
		  	 	  firstArgument = false;
		  	 	  jbossQl.append("WHERE ");
		  	   }
		  	   jbossQl.append("ti.tiTxnDate>=?" + (ctr+1) + " ");
		  	   obj[ctr] = (Date)criteria.get("dateFrom");
		  	   dateFrom = (Date)criteria.get("dateFrom");
		  	   ctr++;
		    }  
		      
		    if (criteria.containsKey("dateTo")) {
		  	
		  	   if (!firstArgument) {
		  	 	  jbossQl.append("AND ");
		  	   } else {
		  	 	  firstArgument = false;
		  	 	  jbossQl.append("WHERE ");
		  	   }
		  	   jbossQl.append("ti.tiTxnDate<=?" + (ctr+1) + " ");
		  	   obj[ctr] = (Date)criteria.get("dateTo");
		  	   dateTo = (Date)criteria.get("dateTo");
		  	   ctr++;
		  	 
		    }  
		    
		    if(adBrnchList.isEmpty()) {
		    	
		    	throw new GlobalNoRecordFoundException();
		    	
		    } else {
		        
		        if (!firstArgument) {
		            
		            jbossQl.append("AND ");
		            
		        } else {
		            
		            firstArgument = false;
		            jbossQl.append("WHERE ");
		            
		        }	      
		        
		        jbossQl.append("ti.tiAdBranch in (");
		        
		        boolean firstLoop = true;
		        
		        Iterator i = adBrnchList.iterator();
		        
		        while(i.hasNext()) {
		            
		            if(firstLoop == false) { 
		                jbossQl.append(", "); 
		            }
		            else { 
		                firstLoop = false; 
		            }
		            
		            AdBranchDetails mdetails = (AdBranchDetails) i.next();
		            
		            jbossQl.append(mdetails.getBrCode());
		            
		        }
		        
		        jbossQl.append(") ");
		        
		        firstArgument = false;
		        
		    }
		  
		    if (!firstArgument) {
		       	  	
	   	       jbossQl.append("AND ");
	   	     
	   	    } else {
	   	  	
	   	  	   firstArgument = false;
	   	  	   jbossQl.append("WHERE ");
	   	  	 
	   	    }

		    jbossQl.append("(ti.tiDocumentType='AP VOUCHER' OR ti.tiDocumentType='AP DEBIT MEMO' OR " +
		    				"ti.tiDocumentType='AP CHECK' OR ti.tiDocumentType='GL JOURNAL') " +
							"AND ti.tiIsArDocument=0 AND ti.tiTcCode IS NULL AND ti.tiWtcCode IS NOT NULL " +
							"AND ti.tiAdCompany=" + AD_CMPNY + " ORDER BY ti.tiSlSubledgerCode");
		    		   	    
		    short PRECISION_UNIT = this.getGlFcPrecisionUnit(AD_CMPNY);
		    			    
		    Collection glTaxInterfaces = glTaxInterfaceHome.getTiByCriteria(jbossQl.toString(), obj);
		    
		    ArrayList apSupplierList = new ArrayList();		    		 
		    		      		        		        	
        	Iterator i = glTaxInterfaces.iterator();	        		        	
        	        	
        	while(i.hasNext()) {
        		
        		//get available record per supplier
        		
        		LocalGlTaxInterface glTaxInterface = (LocalGlTaxInterface) i.next();
        		
        		if (apSupplierList.contains(glTaxInterface.getTiSlSubledgerCode())) {
        			
        			continue;
        			
        		}         		        		
        			        		
    			apSupplierList.add(glTaxInterface.getTiSlSubledgerCode());    			    			
        		        		
        		jbossQl = new StringBuffer();
        		
        		jbossQl.append("SELECT DISTINCT OBJECT(ti) FROM GlTaxInterface ti " + 
        					   "WHERE (ti.tiDocumentType='AP VOUCHER' OR ti.tiDocumentType='AP DEBIT MEMO' " +
        					   "OR ti.tiDocumentType='AP CHECK' OR ti.tiDocumentType='GL JOURNAL') ");
        		
        		ctr = 0;        		        		
        		criteriaSize = 0;
        		String sbldgrCode = glTaxInterface.getTiSlSubledgerCode(); 
        		
        		if(dateFrom != null ) { 
        			criteriaSize++;        			
        		}
        		
        		if(dateTo != null) {
        			criteriaSize++;        		
        		}
        		
        		if(sbldgrCode != null) {
        			criteriaSize++;        		
        		}
        		
        		obj = new Object[criteriaSize];
        		
        		if(sbldgrCode != null) {
        			    		       		     
    			   jbossQl.append("AND ti.tiSlSubledgerCode = ?"  + (ctr+1) + " ");
    			   obj[ctr] = sbldgrCode;
				   ctr++;
    			   
    			   
        		} else {
        			
        			jbossQl.append("AND ti.tiSlSubledgerCode IS NULL ");
        			
        		}
        		
        		if (dateFrom != null) {
        		  	    		  	    		    
    		  	   jbossQl.append("AND ti.tiTxnDate >=?" + (ctr+1) + " ");
        		   obj[ctr] = dateFrom;
        		   ctr++;
        		   
    		    }
        		
        		if(dateTo != null) {
        			    			  
       		  	   jbossQl.append("AND ti.tiTxnDate <=?" + (ctr+1) + " ");
           		   obj[ctr] = dateTo;
           		   ctr++;
           		   
        		}   
        		
        		if(! adBrnchList.isEmpty()) {
    		        
    		        if (!firstArgument) {
    		            
    		            jbossQl.append("AND ");
    		            
    		        } else {
    		            
    		            firstArgument = false;
    		            jbossQl.append("WHERE ");
    		            
    		        }	      
    		        
    		        jbossQl.append("ti.tiAdBranch in (");
    		        
    		        boolean firstLoop = true;
    		        
    		        Iterator k = adBrnchList.iterator();
    		        
    		        while(k.hasNext()) {
    		            
    		            if(firstLoop == false) { 
    		                jbossQl.append(", "); 
    		            }
    		            else { 
    		                firstLoop = false; 
    		            }
    		            
    		            AdBranchDetails mdetails = (AdBranchDetails) k.next();
    		            
    		            jbossQl.append(mdetails.getBrCode());
    		            
    		        }
    		        
    		        jbossQl.append(") ");
    		        
    		        firstArgument = false;
    		        
    		    }
    	   	    
    		    jbossQl.append("AND ti.tiIsArDocument=0 AND ti.tiTcCode IS NULL AND ti.tiWtcCode IS NOT NULL AND " + 
    		    				"ti.tiAdCompany=" + AD_CMPNY +  " ORDER BY ti.tiTxnDate");    		        						    
			    
			    Collection glTaxInterfaceDocs = glTaxInterfaceHome.getTiByCriteria(jbossQl.toString(),obj);
			            		
        		Iterator j = glTaxInterfaceDocs.iterator();  
        		
        		ArrayList apWtcList = new ArrayList();
        		        	
        		while(j.hasNext()) {	
        			
        			//get available record per wtc code
        			
        			LocalGlTaxInterface glTaxInterfaceDoc = (LocalGlTaxInterface) j.next();
        			
        			if (apWtcList.contains(glTaxInterfaceDoc.getTiWtcCode())) {
            			
            			continue;
            			
            		}         		        		
            			        		
        			apWtcList.add(glTaxInterfaceDoc.getTiWtcCode());
        			        			        			        			
        			jbossQl = new StringBuffer();
            		
            		jbossQl.append("SELECT OBJECT(ti) FROM GlTaxInterface ti " + 
            					   "WHERE (ti.tiDocumentType='AP VOUCHER' OR ti.tiDocumentType='AP DEBIT MEMO' " +
            					   "OR ti.tiDocumentType='AP CHECK' OR ti.tiDocumentType='GL JOURNAL') ");
            		
            		ctr = 0;        		        		
            		criteriaSize = 0;            		
            		Integer wtcCode = glTaxInterfaceDoc.getTiWtcCode();
            		            		
            		if(dateFrom != null ) { 
            			criteriaSize++;        			
            		}
            		
            		if(dateTo != null) {
            			criteriaSize++;        		
            		}
            		
            		if(wtcCode != null) {
            			criteriaSize++;
            		}
            		
            		if(sbldgrCode != null) {
            			criteriaSize++;        		
            		}
            		
            		obj = new Object[criteriaSize];
            		
            		if(sbldgrCode != null) {
            			    		       		     
        			   jbossQl.append("AND ti.tiSlSubledgerCode = ?"  + (ctr+1) + " ");
        			   obj[ctr] = sbldgrCode;
    				   ctr++;
        			   
            		} else {
            			
            			jbossQl.append("AND ti.tiSlSubledgerCode IS NULL ");
            			
            		}
            		
            		if (dateFrom != null) {
            		  	    		  	    		    
        		  	   jbossQl.append("AND ti.tiTxnDate >=?" + (ctr+1) + " ");
            		   obj[ctr] = dateFrom;
            		   ctr++;
            		   
        		    }
            		
            		if(dateTo != null) {
            			    			  
           		  	   jbossQl.append("AND ti.tiTxnDate <=?" + (ctr+1) + " ");
               		   obj[ctr] = dateTo;
               		   ctr++;
               		   
            		}
            		
            		if(wtcCode != null) {
            			
        			   jbossQl.append("AND ti.tiWtcCode =?" + (ctr+1) + " ");
               		   obj[ctr] = (Integer) wtcCode;
               		   ctr++;
               		   
            		}
            		
            		if(! adBrnchList.isEmpty()) {
            		    
            		    if (!firstArgument) {
            		        
            		        jbossQl.append("AND ");
            		        
            		    } else {
            		        
            		        firstArgument = false;
            		        jbossQl.append("WHERE ");
            		        
            		    }	      
            		    
            		    jbossQl.append("ti.tiAdBranch in (");
            		    
            		    boolean firstLoop = true;
            		    
            		    Iterator k = adBrnchList.iterator();
            		    
            		    while(k.hasNext()) {
            		        
            		        if(firstLoop == false) { 
            		            jbossQl.append(", "); 
            		        }
            		        else { 
            		            firstLoop = false; 
            		        }
            		        
            		        AdBranchDetails mdetails = (AdBranchDetails) k.next();
            		        
            		        jbossQl.append(mdetails.getBrCode());
            		        
            		    }
            		    
            		    jbossQl.append(") ");
            		    
            		    firstArgument = false;
            		    
            		}
        	   	    
        		    jbossQl.append("AND ti.tiIsArDocument=0 AND ti.tiAdCompany=" + AD_CMPNY +  " ORDER BY ti.tiTxnDate");        		    				
    			    
    			    Collection glTaxInterfaceWtcs = glTaxInterfaceHome.getTiByCriteria(jbossQl.toString(),obj);        			    			        			        			        			   
    			    
    			    Iterator k = glTaxInterfaceWtcs.iterator();    			        			    
    			    
    				double WITHHOLDING_TAX = 0d;		    
    			    double NET_AMOUNT = 0d;
    			    
    			    while(k.hasNext()) {
    			    	
    			    	LocalGlTaxInterface glTaxInterfaceWtc = (LocalGlTaxInterface) k.next();
    			    	
    			    	if(glTaxInterfaceWtc.getTiDocumentType().equals("GL JOURNAL")) {
            				
            				LocalGlJournalLine glJournal = glJournalHome.findByPrimaryKey( 
            									glTaxInterfaceWtc.getTiTxlCode());
            				
            				if(glJournal.getJlDebit() == EJBCommon.TRUE) {
            						
            					WITHHOLDING_TAX = EJBCommon.roundIt(glJournal.getJlAmount(), PRECISION_UNIT) * -1;        					
            					NET_AMOUNT = EJBCommon.roundIt(glTaxInterfaceWtc.getTiNetAmount(), PRECISION_UNIT) * -1;
            					
            				} else {

            					WITHHOLDING_TAX = EJBCommon.roundIt(glJournal.getJlAmount(), PRECISION_UNIT);        					
            					NET_AMOUNT = EJBCommon.roundIt(glTaxInterfaceWtc.getTiNetAmount(), PRECISION_UNIT);
            					
            				}
            				
            			} else {
            				
    						LocalApDistributionRecord apDistributionRecord = apDistributionRecordHome.findByPrimaryKey(
    											glTaxInterfaceWtc.getTiTxlCode());
    						
    						if(apDistributionRecord.getDrDebit() == EJBCommon.TRUE) {

    							WITHHOLDING_TAX = EJBCommon.roundIt(apDistributionRecord.getDrAmount(), PRECISION_UNIT) * -1;							
    							NET_AMOUNT = EJBCommon.roundIt(glTaxInterfaceWtc.getTiNetAmount(), PRECISION_UNIT) * -1;							
    						
    						} else {
    							
    							WITHHOLDING_TAX = EJBCommon.roundIt(apDistributionRecord.getDrAmount(), PRECISION_UNIT);							
    							NET_AMOUNT = EJBCommon.roundIt(glTaxInterfaceWtc.getTiNetAmount(), PRECISION_UNIT);							
    						
    						}												
    						
            			}    
    			    	LocalApWithholdingTaxCode apWithholdingTaxCode = apWithholdingTaxCodeHome.findByPrimaryKey(glTaxInterfaceDoc.getTiWtcCode());
    	    			
    	    			ApRepWithholdingTaxExpandedDetails details = new ApRepWithholdingTaxExpandedDetails();
    	        		details.setWteSequenceNumber(++SQNC_NMBR);
    	        		details.setWteSplTinNumber(glTaxInterfaceWtc.getTiSlTin());
    	        		details.setWteSplSupplierName(glTaxInterfaceWtc.getTiSlName());        		
    	        		details.setWteNatureOfIncomePayment(apWithholdingTaxCode.getWtcDescription());
    	        		details.setWteAtcCode(apWithholdingTaxCode.getWtcName());        		
    	        		details.setWteTaxBase(NET_AMOUNT);
    	        		details.setWteRate(apWithholdingTaxCode.getWtcRate());
    	        		details.setWteTaxWithheld(WITHHOLDING_TAX);
    	        		details.setWteTxnDate(glTaxInterfaceWtc.getTiTxnDate());
    	        		
    	        		list.add(details);
    			    	
    			    }
    			    
    				
            			        		        			        				        			        		
        		}         		        		
        		        		         		        		        		        		        	
        	}
	                	        	
        	if (list.isEmpty()) {
    		  	
    		  	  throw new GlobalNoRecordFoundException();
    		  	
    		}	      
        
        } catch (GlobalNoRecordFoundException ex) {
   	  	 
        	Debug.printStackTrace(ex);
        	throw ex;
   	  	
        } catch (Exception ex) {
   	  	
			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());
   	  	
   	  	}
        
        return list;        
    }
    
	/**
	* @ejb:interface-method view-type="remote"
	* @jboss:method-attributes read-only="true"
	**/
	public com.util.AdCompanyDetails getAdCompany(Integer AD_CMPNY) {
	
	  Debug.print("ApRepWithholdingTaxExpandedControllerBean getAdCompany");      
	  
	  LocalAdCompanyHome adCompanyHome = null;
	        
	  // Initialize EJB Home
	    
	  try {
	        
	       adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
	           lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
	        
	  } catch (NamingException ex) {
	        
	      throw new EJBException(ex.getMessage());
	        
	  }
	
	  try {
	      
	     LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
	     
	     AdCompanyDetails details = new AdCompanyDetails();
	     details.setCmpName(adCompany.getCmpName());
	     
	     return details;  	
	  	       	
	  } catch (Exception ex) {
	  	
	  	 Debug.printStackTrace(ex);
	  	 throw new EJBException(ex.getMessage());
	  	  
	  }
	  
	}			
   
   public short getGlFcPrecisionUnit(Integer AD_CMPNY) {

       Debug.print("ApVoucherEntryControllerBean getGlFcPrecisionUnit");

      
       LocalAdCompanyHome adCompanyHome = null;
      
     
       // Initialize EJB Home
        
       try {
            
           adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
              lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);       
            
       } catch (NamingException ex) {
            
           throw new EJBException(ex.getMessage());
            
       }

       try {
       	
         LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
            
         return  adCompany.getGlFunctionalCurrency().getFcPrecision();
                                     
       } catch (Exception ex) {
       	 
       	 Debug.printStackTrace(ex);
         throw new EJBException(ex.getMessage());
         
       }

    }  
   
   /**
    * @ejb:interface-method view-type="remote"
    * @jboss:method-attributes read-only="true"
    **/   
   public ArrayList getAdBrResAll(Integer RS_CODE, Integer AD_CMPNY) 
   	   throws GlobalNoRecordFoundException{
       
       Debug.print("ApRepInputTaxControllerBean getAdBrResAll");
       
       LocalAdBranchResponsibilityHome adBranchResponsibilityHome = null;
       LocalAdBranchHome adBranchHome = null;
       
       LocalAdBranchResponsibility adBranchResponsibility = null;
       LocalAdBranch adBranch = null;
       
       Collection adBranchResponsibilities = null;
       
       ArrayList list = new ArrayList();
       
       // Initialize EJB Home
       
       try {
           
           adBranchResponsibilityHome = (LocalAdBranchResponsibilityHome)EJBHomeFactory.
           lookUpLocalHome(LocalAdBranchResponsibilityHome.JNDI_NAME, LocalAdBranchResponsibilityHome.class);
           adBranchHome = (LocalAdBranchHome)EJBHomeFactory.
           lookUpLocalHome(LocalAdBranchHome.JNDI_NAME, LocalAdBranchHome.class);
           
       } catch (NamingException ex) {
           
           throw new EJBException(ex.getMessage());
           
       }
       
       try {
           
           adBranchResponsibilities = adBranchResponsibilityHome.findByAdResponsibility(RS_CODE, AD_CMPNY);
           
       } catch (FinderException ex) {
           
       } catch (Exception ex) {
           
           throw new EJBException(ex.getMessage());
       }
       
       if (adBranchResponsibilities.isEmpty()) {
           
           throw new GlobalNoRecordFoundException();
           
       }
       
       try {
           
           Iterator i = adBranchResponsibilities.iterator();
           
           while(i.hasNext()) {
               
               adBranchResponsibility = (LocalAdBranchResponsibility)i.next();
               
               adBranch = adBranchResponsibility.getAdBranch();
               
               AdBranchDetails details = new AdBranchDetails();
               
               details.setBrCode(adBranch.getBrCode());
               details.setBrBranchCode(adBranch.getBrBranchCode());
               details.setBrName(adBranch.getBrName());
               details.setBrHeadQuarter(adBranch.getBrHeadQuarter());
               
               list.add(details);
               
           }	               
           
       } catch (Exception ex) {
           
           throw new EJBException(ex.getMessage());
       }
       
       return list;
       
   }    
    
   // private
    
    private double convertForeignToFunctionalCurrency(Integer FC_CODE, String FC_NM, 
	    Date CONVERSION_DATE, double CONVERSION_RATE, double AMOUNT, Integer AD_CMPNY) {
	    	  
	    
        LocalGlFunctionalCurrencyRateHome glFunctionalCurrencyRateHome = null;
        LocalAdCompanyHome adCompanyHome = null;
         
        LocalAdCompany adCompany = null;
                 
        // Initialize EJB Homes
         
        try {
         	
            glFunctionalCurrencyRateHome = (LocalGlFunctionalCurrencyRateHome)EJBHomeFactory.
               lookUpLocalHome(LocalGlFunctionalCurrencyRateHome.JNDI_NAME, LocalGlFunctionalCurrencyRateHome.class);                         
            adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
               lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);             
                   
         } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
         }
         
         // get company and extended precision
         
         try {
         	
             adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
         	             
         } catch (Exception ex) {
         	
             throw new EJBException(ex.getMessage());
         	
         }	     
        
         
         // Convert to functional currency if necessary
         
         if (CONVERSION_RATE != 1 && CONVERSION_RATE != 0 ) {
         	         
             AMOUNT = AMOUNT * CONVERSION_RATE;
             	
         }
         return EJBCommon.roundIt(AMOUNT, adCompany.getGlFunctionalCurrency().getFcPrecision());
         
	}

    // SessionBean methods

    /**
     * @ejb:create-method view-type="remote"
     **/
    public void ejbCreate() throws CreateException {

       Debug.print("ApRepWithholdingTaxExpandedControllerBean ejbCreate");
      
    }
}
