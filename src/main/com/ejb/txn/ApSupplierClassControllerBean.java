package com.ejb.txn;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;
import javax.naming.NamingException;

import com.ejb.ap.LocalApSupplier;
import com.ejb.ap.LocalApSupplierClass;
import com.ejb.ap.LocalApSupplierClassHome;
import com.ejb.ap.LocalApSupplierHome;
import com.ejb.ap.LocalApTaxCode;
import com.ejb.ap.LocalApTaxCodeHome;
import com.ejb.ap.LocalApWithholdingTaxCode;
import com.ejb.ap.LocalApWithholdingTaxCodeHome;
import com.ejb.exception.ApSCCoaGlExpenseAccountNotFoundException;
import com.ejb.exception.ApSCCoaGlPayableAccountNotFoundException;
import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.exception.GlobalRecordAlreadyAssignedException;
import com.ejb.exception.GlobalRecordAlreadyDeletedException;
import com.ejb.exception.GlobalRecordAlreadyExistException;
import com.ejb.gl.LocalGlAccountingCalendarValue;
import com.ejb.gl.LocalGlChartOfAccount;
import com.ejb.gl.LocalGlChartOfAccountHome;
import com.ejb.gl.LocalGlInvestorAccountBalance;
import com.ejb.gl.LocalGlInvestorAccountBalanceHome;
import com.ejb.gl.LocalGlSetOfBook;
import com.ejb.gl.LocalGlSetOfBookHome;
import com.util.AbstractSessionBean;
import com.util.ApModSupplierClassDetails;
import com.util.Debug;
import com.util.EJBCommon;
import com.util.EJBHomeFactory;

/**
 * @ejb:bean name="ApSupplierClassControllerEJB"
 *           display-name="Used for entering supplier classes"
 *           type="Stateless"
 *           view-type="remote"
 *           jndi-name="ejb/ApSupplierClassControllerEJB"
 *
 * @ejb:interface remote-class="com.ejb.txn.ApSupplierClassController"
 *                extends="javax.ejb.EJBObject"
 *
 * @ejb:home remote-class="com.ejb.txn.ApSupplierClassControllerHome"
 *           extends="javax.ejb.EJBHome"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="apuser"
 *                        role-link="apuserlink"
 *
 * @ejb:permission role-name="apuser"
 *
*/

public class ApSupplierClassControllerBean extends AbstractSessionBean {

    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList getApScAll(Integer AD_CMPNY)
        throws GlobalNoRecordFoundException {

        Debug.print("ApSupplierClassControllerBean getApScAll");

        LocalApSupplierClassHome apSupplierClassHome = null;
        LocalGlChartOfAccountHome glChartOfAccountHome = null;

        ArrayList list = new ArrayList();

        // Initialize EJB Home

        try {

            apSupplierClassHome = (LocalApSupplierClassHome)EJBHomeFactory.
                lookUpLocalHome(LocalApSupplierClassHome.JNDI_NAME, LocalApSupplierClassHome.class);
            glChartOfAccountHome = (LocalGlChartOfAccountHome)EJBHomeFactory.
                lookUpLocalHome(LocalGlChartOfAccountHome.JNDI_NAME, LocalGlChartOfAccountHome.class);

        } catch (NamingException ex) {

            throw new EJBException(ex.getMessage());

        }

        try {

	        Collection apSupplierClasses = apSupplierClassHome.findScAll(AD_CMPNY);

	        if (apSupplierClasses.isEmpty()) {

	            throw new GlobalNoRecordFoundException();

	        }

	        Iterator i = apSupplierClasses.iterator();

	        while (i.hasNext()) {

	        	LocalApSupplierClass apSupplierClass = (LocalApSupplierClass)i.next();

		        LocalGlChartOfAccount glPayableAccount = glChartOfAccountHome.findByPrimaryKey(apSupplierClass.getScGlCoaPayableAccount());
		        LocalGlChartOfAccount glExpenseAccount = null;

		        if (apSupplierClass.getScGlCoaExpenseAccount() != null) {

		        	glExpenseAccount = glChartOfAccountHome.findByPrimaryKey(apSupplierClass.getScGlCoaExpenseAccount());

		        }


	        		ApModSupplierClassDetails mdetails = new ApModSupplierClassDetails();
                        mdetails.setScCode(apSupplierClass.getScCode());
                        mdetails.setScName(apSupplierClass.getScName());
                        mdetails.setScDescription(apSupplierClass.getScDescription());

                        mdetails.setScTcName(apSupplierClass.getApTaxCode().getTcName());
                        mdetails.setScWtcName(apSupplierClass.getApWithholdingTaxCode().getWtcName());
                        mdetails.setScCoaGlPayableAccountNumber(glPayableAccount.getCoaAccountNumber());
                        mdetails.setScCoaGlExpenseAccountNumber(glExpenseAccount != null ? glExpenseAccount.getCoaAccountNumber() : null);
                        mdetails.setScCoaGlPayableAccountDescription(glPayableAccount.getCoaAccountDescription());
                        mdetails.setScInvestorBonusRate(apSupplierClass.getScInvestorBonusRate());
                        mdetails.setScInvestorInterestRate(apSupplierClass.getScInvestorInterestRate());
                        if (glExpenseAccount != null) {

                            mdetails.setScCoaGlExpenseAccountDescription(glExpenseAccount.getCoaAccountDescription());

                        }

                        mdetails.setScEnable(apSupplierClass.getScEnable());
                        mdetails.setScLedger(apSupplierClass.getScLedger());
                        mdetails.setScIsInvestment(apSupplierClass.getScIsInvestment());
                        mdetails.setScIsLoan(apSupplierClass.getScIsLoan());
                        mdetails.setScIsVatReliefVoucherItem(apSupplierClass.getScIsVatReliefVoucherItem());
                        mdetails.setScLastModifiedBy(apSupplierClass.getScLastModifiedBy());
                        mdetails.setScDateLastModified(apSupplierClass.getScDateLastModified());

	        		list.add(mdetails);

	        }

	        return list;

        } catch (GlobalNoRecordFoundException ex) {

        	throw ex;

        } catch (Exception ex) {

        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());

        }

    }

    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList getApTcAll(Integer AD_CMPNY) {

        Debug.print("ApSupplierClassControllerBean getApTcAll");

        LocalApTaxCodeHome apTaxCodeHome = null;

        Collection apTaxCodes = null;

        LocalApTaxCode apTaxCode = null;

        ArrayList list = new ArrayList();

        // Initialize EJB Home

        try {

            apTaxCodeHome = (LocalApTaxCodeHome)EJBHomeFactory.
                lookUpLocalHome(LocalApTaxCodeHome.JNDI_NAME, LocalApTaxCodeHome.class);

        } catch (NamingException ex) {

            throw new EJBException(ex.getMessage());

        }

        try {

            apTaxCodes = apTaxCodeHome.findEnabledTcAll(AD_CMPNY);

        } catch (FinderException ex) {

        } catch (Exception ex) {

            throw new EJBException(ex.getMessage());
        }

        if (apTaxCodes.isEmpty()) {

            return null;

        }

        Iterator i = apTaxCodes.iterator();

        while (i.hasNext()) {

            apTaxCode = (LocalApTaxCode)i.next();

            list.add(apTaxCode.getTcName());

        }

        return list;

    }

    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList getApWtcAll(Integer AD_CMPNY) {

        Debug.print("ApSupplierClassControllerBean getApWtcAll");

        LocalApWithholdingTaxCodeHome apWithholdingTaxCodeHome = null;

        Collection apWithholdingTaxCodes = null;

        LocalApWithholdingTaxCode apWithholdingTaxCode = null;

        ArrayList list = new ArrayList();

        // Initialize EJB Home

        try {

            apWithholdingTaxCodeHome = (LocalApWithholdingTaxCodeHome)EJBHomeFactory.
                lookUpLocalHome(LocalApWithholdingTaxCodeHome.JNDI_NAME, LocalApWithholdingTaxCodeHome.class);

        } catch (NamingException ex) {

            throw new EJBException(ex.getMessage());

        }

        try {

            apWithholdingTaxCodes = apWithholdingTaxCodeHome.findEnabledWtcAll(AD_CMPNY);

        } catch (FinderException ex) {

        } catch (Exception ex) {

            throw new EJBException(ex.getMessage());
        }

        if (apWithholdingTaxCodes.isEmpty()) {

            return null;

        }

        Iterator i = apWithholdingTaxCodes.iterator();

        while (i.hasNext()) {

            apWithholdingTaxCode = (LocalApWithholdingTaxCode)i.next();

            list.add(apWithholdingTaxCode.getWtcName());

        }

        return list;

    }

    /**
     * @ejb:interface-method view-type="remote"
     **/
    public void addApScEntry(com.util.ApModSupplierClassDetails mdetails, String SC_TC_NM, String SC_WTC_NM, Integer AD_CMPNY)
        throws GlobalRecordAlreadyExistException,
               ApSCCoaGlPayableAccountNotFoundException,
               ApSCCoaGlExpenseAccountNotFoundException {

        Debug.print("ApSupplierClassControllerBean addApTcEntry");

        LocalApSupplierClassHome apSupplierClassHome = null;
        LocalGlChartOfAccountHome glChartOfAccountHome = null;
        LocalApTaxCodeHome apTaxCodeHome = null;
        LocalApWithholdingTaxCodeHome apWithholdingTaxCodeHome = null;

        LocalGlChartOfAccount glPayableAccount = null;
        LocalGlChartOfAccount glExpenseAccount = null;

        LocalApSupplierHome apSupplierHome = null;


        ArrayList list = new ArrayList();

        // Initialize EJB Home

        try {

            apSupplierClassHome = (LocalApSupplierClassHome)EJBHomeFactory.
                lookUpLocalHome(LocalApSupplierClassHome.JNDI_NAME, LocalApSupplierClassHome.class);
            glChartOfAccountHome = (LocalGlChartOfAccountHome)EJBHomeFactory.
                lookUpLocalHome(LocalGlChartOfAccountHome.JNDI_NAME, LocalGlChartOfAccountHome.class);
            apTaxCodeHome = (LocalApTaxCodeHome)EJBHomeFactory.
                lookUpLocalHome(LocalApTaxCodeHome.JNDI_NAME, LocalApTaxCodeHome.class);
            apWithholdingTaxCodeHome = (LocalApWithholdingTaxCodeHome)EJBHomeFactory.
                lookUpLocalHome(LocalApWithholdingTaxCodeHome.JNDI_NAME, LocalApWithholdingTaxCodeHome.class);
            apSupplierHome = (LocalApSupplierHome)EJBHomeFactory.
                    lookUpLocalHome(LocalApSupplierHome.JNDI_NAME, LocalApSupplierHome.class);


        } catch (NamingException ex) {

            throw new EJBException(ex.getMessage());

        }

        try {

            LocalApSupplierClass apSupplierClass = null;
            LocalApTaxCode apTaxCode = null;
            LocalApWithholdingTaxCode apWithholdingTaxCode = null;


	        try {

	            apSupplierClass = apSupplierClassHome.findByScName(mdetails.getScName(), AD_CMPNY);

	            throw new GlobalRecordAlreadyExistException();

	        } catch (FinderException ex) {


	        }

	        // get payable and expense account to validate accounts

	        try {

	            glPayableAccount = glChartOfAccountHome.findByCoaAccountNumber(mdetails.
	            	getScCoaGlPayableAccountNumber(), AD_CMPNY);

	        } catch (FinderException ex) {

	            throw new ApSCCoaGlPayableAccountNotFoundException();

	        }

	        if (mdetails.getScCoaGlExpenseAccountNumber() != null && mdetails.getScCoaGlExpenseAccountNumber().length() > 0) {

		        try {

					glExpenseAccount = glChartOfAccountHome.findByCoaAccountNumber(mdetails.
		            	getScCoaGlExpenseAccountNumber(), AD_CMPNY);

		        } catch (FinderException ex) {

		            throw new ApSCCoaGlExpenseAccountNotFoundException();

		        }

	        }

	    	// create new supplier class

	    	apSupplierClass = apSupplierClassHome.create(
	    			mdetails.getScName(),
	    	        mdetails.getScDescription(),
	    	        glPayableAccount.getCoaCode(),
	    	        glExpenseAccount != null ?
	    	        glExpenseAccount.getCoaCode() : null,
                    mdetails.getScInvestorBonusRate(),
                    mdetails.getScInvestorInterestRate(),
                    mdetails.getScEnable(),
                    mdetails.getScLedger(),
                    mdetails.getScIsInvestment(),
                    mdetails.getScIsLoan(),
                    mdetails.getScIsVatReliefVoucherItem(),
                    mdetails.getScLastModifiedBy(),
                    mdetails.getScDateLastModified(),
                    AD_CMPNY);

	        apTaxCode = apTaxCodeHome.findByTcName(SC_TC_NM, AD_CMPNY);
	        apTaxCode.addApSupplierClass(apSupplierClass);

	        apWithholdingTaxCode = apWithholdingTaxCodeHome.findByWtcName(SC_WTC_NM, AD_CMPNY);
	        apWithholdingTaxCode.addApSupplierClass(apSupplierClass);

	        if (apSupplierClass.getScLedger() == EJBCommon.TRUE) {
	        	Collection suppliers = apSupplierHome.findAllSplByScCode(apSupplierClass.getScCode(), AD_CMPNY);

	        	Iterator i = suppliers.iterator();

	        	while(i.hasNext()){

	        		LocalApSupplier apSupplier = (LocalApSupplier)i.next();

	        			if(apSupplier.getGlInvestorAccountBalances().size() == 0)
		        			this.generateGlInvtrBalance(apSupplier, AD_CMPNY);

	        	}
	        }


        } catch (GlobalRecordAlreadyExistException ex) {

        	getSessionContext().setRollbackOnly();
        	throw ex;

        } catch (ApSCCoaGlPayableAccountNotFoundException ex) {

        	getSessionContext().setRollbackOnly();
        	throw ex;

        } catch (ApSCCoaGlExpenseAccountNotFoundException ex) {

        	getSessionContext().setRollbackOnly();
        	throw ex;

        } catch (Exception ex) {

        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());

        }

    }

    /**
     * @ejb:interface-method view-type="remote"
     **/
    public void updateApScEntry(com.util.ApModSupplierClassDetails mdetails, String SC_TC_NM, String SC_WTC_NM, Integer AD_CMPNY)
        throws GlobalRecordAlreadyExistException,
               ApSCCoaGlPayableAccountNotFoundException,
               ApSCCoaGlExpenseAccountNotFoundException {

        Debug.print("ApSupplierClassControllerBean updateApTcEntry");

        LocalApSupplierClassHome apSupplierClassHome = null;
        LocalGlChartOfAccountHome glChartOfAccountHome = null;
        LocalApTaxCodeHome apTaxCodeHome = null;
        LocalApWithholdingTaxCodeHome apWithholdingTaxCodeHome = null;
        LocalApSupplierHome apSupplierHome = null;

        LocalApSupplierClass apSupplierClass = null;
        LocalApTaxCode apTaxCode = null;
        LocalApWithholdingTaxCode apWithholdingTaxCode = null;
        LocalGlChartOfAccount glPayableAccount = null;
        LocalGlChartOfAccount glExpenseAccount = null;

        ArrayList list = new ArrayList();

        // Initialize EJB Home

        try {

            apSupplierClassHome = (LocalApSupplierClassHome)EJBHomeFactory.
                lookUpLocalHome(LocalApSupplierClassHome.JNDI_NAME, LocalApSupplierClassHome.class);
            glChartOfAccountHome = (LocalGlChartOfAccountHome)EJBHomeFactory.
                lookUpLocalHome(LocalGlChartOfAccountHome.JNDI_NAME, LocalGlChartOfAccountHome.class);
            apTaxCodeHome = (LocalApTaxCodeHome)EJBHomeFactory.
                lookUpLocalHome(LocalApTaxCodeHome.JNDI_NAME, LocalApTaxCodeHome.class);
            apWithholdingTaxCodeHome = (LocalApWithholdingTaxCodeHome)EJBHomeFactory.
                lookUpLocalHome(LocalApWithholdingTaxCodeHome.JNDI_NAME, LocalApWithholdingTaxCodeHome.class);

            apSupplierHome = (LocalApSupplierHome)EJBHomeFactory.
                    lookUpLocalHome(LocalApSupplierHome.JNDI_NAME, LocalApSupplierHome.class);


        } catch (NamingException ex) {

            throw new EJBException(ex.getMessage());

        }

        try {

        	try {

	            LocalApSupplierClass apExistingSupplierClass = apSupplierClassHome.findByScName(mdetails.getScName(), AD_CMPNY);

	            if (!apExistingSupplierClass.getScCode().equals(mdetails.getScCode())) {

	                 throw new GlobalRecordAlreadyExistException();

	            }

	        } catch (FinderException ex) {

	        }

	        // get payable and expense account to validate accounts

	        try {

	            glPayableAccount = glChartOfAccountHome.findByCoaAccountNumber(mdetails.
	            	getScCoaGlPayableAccountNumber(), AD_CMPNY);

	        } catch (FinderException ex) {

	            throw new ApSCCoaGlPayableAccountNotFoundException();

	        }

	        if (mdetails.getScCoaGlExpenseAccountNumber() != null && mdetails.getScCoaGlExpenseAccountNumber().length() > 0) {

		        try {

					glExpenseAccount = glChartOfAccountHome.findByCoaAccountNumber(mdetails.
		            	getScCoaGlExpenseAccountNumber(), AD_CMPNY);

		        } catch (FinderException ex) {

		            throw new ApSCCoaGlExpenseAccountNotFoundException();

		        }

	        }

				// find and update supplier class

			apSupplierClass = apSupplierClassHome.findByPrimaryKey(mdetails.getScCode());

				apSupplierClass.setScName(mdetails.getScName());
				apSupplierClass.setScDescription(mdetails.getScDescription());
				apSupplierClass.setScGlCoaPayableAccount(glPayableAccount.getCoaCode());
				apSupplierClass.setScGlCoaExpenseAccount(glExpenseAccount != null ? glExpenseAccount.getCoaCode() : null);
                apSupplierClass.setScInvestorBonusRate(mdetails.getScInvestorBonusRate());
                apSupplierClass.setScInvestorInterestRate(mdetails.getScInvestorInterestRate());
				apSupplierClass.setScEnable(mdetails.getScEnable());
				apSupplierClass.setScLedger(mdetails.getScLedger());
				apSupplierClass.setScIsInvestment(mdetails.getScIsInvestment());
				apSupplierClass.setScIsLoan(mdetails.getScIsLoan());
				apSupplierClass.setScIsVatReliefVoucherItem(mdetails.getScIsVatReliefVoucherItem());
				apSupplierClass.setScLastModifiedBy(mdetails.getScLastModifiedBy());
				apSupplierClass.setScDateLastModified(mdetails.getScDateLastModified());

				apTaxCode = apTaxCodeHome.findByTcName(SC_TC_NM, AD_CMPNY);
				apTaxCode.addApSupplierClass(apSupplierClass);

				apWithholdingTaxCode = apWithholdingTaxCodeHome.findByWtcName(SC_WTC_NM, AD_CMPNY);
				apWithholdingTaxCode.addApSupplierClass(apSupplierClass);


				if (apSupplierClass.getScLedger() == EJBCommon.TRUE) {
		        	Collection suppliers = apSupplierHome.findAllSplByScCode(apSupplierClass.getScCode(), AD_CMPNY);

		        	Iterator i = suppliers.iterator();

		        	while(i.hasNext()){

		        		LocalApSupplier apSupplier = (LocalApSupplier)i.next();

		        		if(apSupplier.getGlInvestorAccountBalances().size() == 0)
		        			this.generateGlInvtrBalance(apSupplier, AD_CMPNY);

		        	}
		        }

        } catch (GlobalRecordAlreadyExistException ex) {

        	getSessionContext().setRollbackOnly();
        	throw ex;

        } catch (ApSCCoaGlPayableAccountNotFoundException ex) {

        	getSessionContext().setRollbackOnly();
        	throw ex;

        } catch (ApSCCoaGlExpenseAccountNotFoundException ex) {

        	getSessionContext().setRollbackOnly();
        	throw ex;

        } catch (Exception ex) {

        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());

        }

    }

   /**
    * @ejb:interface-method view-type="remote"
    **/
    public void deleteApScEntry(Integer SC_CODE, Integer AD_CMPNY)
        throws GlobalRecordAlreadyDeletedException,
                GlobalRecordAlreadyAssignedException {

        Debug.print("ApSupplierClassControllerBean deleteApTcEntry");

        LocalApSupplierClassHome apSupplierClassHome = null;

        // Initialize EJB Home

        try {

            apSupplierClassHome = (LocalApSupplierClassHome)EJBHomeFactory.
                lookUpLocalHome(LocalApSupplierClassHome.JNDI_NAME, LocalApSupplierClassHome.class);

        } catch (NamingException ex) {

            throw new EJBException(ex.getMessage());

        }

        try {

      	    LocalApSupplierClass apSupplierClass = null;

	        try {

	            apSupplierClass = apSupplierClassHome.findByPrimaryKey(SC_CODE);

	        } catch (FinderException ex) {

	            throw new GlobalRecordAlreadyDeletedException();

	        }

	        if (!apSupplierClass.getApSuppliers().isEmpty()) {

	            throw new GlobalRecordAlreadyAssignedException();

	        }

		    apSupplierClass.remove();

        } catch (GlobalRecordAlreadyDeletedException ex) {

        	getSessionContext().setRollbackOnly();
        	throw ex;

        } catch (GlobalRecordAlreadyAssignedException ex) {

        	getSessionContext().setRollbackOnly();
        	throw ex;

        } catch (Exception ex) {

        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());

        }

    }


    private void generateGlInvtrBalance(LocalApSupplier apSupplier, Integer AD_CMPNY)
 	{

            Debug.print("ApSupplierEntryControllerBean generateGlInvtrBalance");

            LocalGlSetOfBookHome glSetOfBookHome = null;
            LocalApSupplierHome apSupplierHome = null;
            LocalGlInvestorAccountBalanceHome glInvestorAccountBalanceHome = null;

            // Initialize EJB Home

            try {
                    apSupplierHome = (LocalApSupplierHome)EJBHomeFactory.
                                    lookUpLocalHome(LocalApSupplierHome.JNDI_NAME, LocalApSupplierHome.class);
                    glInvestorAccountBalanceHome = (LocalGlInvestorAccountBalanceHome)EJBHomeFactory.
                                    lookUpLocalHome(LocalGlInvestorAccountBalanceHome.JNDI_NAME, LocalGlInvestorAccountBalanceHome.class);
                    glSetOfBookHome = (LocalGlSetOfBookHome)EJBHomeFactory.
                                    lookUpLocalHome(LocalGlSetOfBookHome.JNDI_NAME, LocalGlSetOfBookHome.class);

            } catch (NamingException ex) {

                    throw new EJBException(ex.getMessage());

            }


            try {

                // create balances to all existing customer


                Collection glSetOfBooks = glSetOfBookHome.findSobAll(AD_CMPNY);


                Iterator i = glSetOfBooks.iterator();

                while (i.hasNext()) {

                    LocalGlSetOfBook glSetOfBook = (LocalGlSetOfBook)i.next();



                    Collection glAccountingCalendarValues =
                            glSetOfBook.getGlAccountingCalendar().getGlAccountingCalendarValues();

                    Iterator iterAcv = glAccountingCalendarValues.iterator();

                    while (iterAcv.hasNext()) {

                            LocalGlAccountingCalendarValue glAccountingCalendarValue =
                                    (LocalGlAccountingCalendarValue) iterAcv.next();


                            LocalGlInvestorAccountBalance glInvestorAccountBalance = null;
                            try {

                                    glInvestorAccountBalance = glInvestorAccountBalanceHome.create(0d, 0d,(byte)0,(byte)0, 0d, 0d, 0d, 0d, 0d, 0d, AD_CMPNY);
                                    glInvestorAccountBalance.setGlAccountingCalendarValue(glAccountingCalendarValue);

                                    glInvestorAccountBalance.setApSupplier(apSupplier);
                            } catch (Exception ex) {

                                    getSessionContext().setRollbackOnly();
                                    throw new EJBException(ex.getMessage());

                            }
                    }

                }

            } catch (Exception ex) {

                    getSessionContext().setRollbackOnly();
                    throw new EJBException(ex.getMessage());
            }


 	}

    // SessionBean methods

    /**
     * @ejb:create-method view-type="remote"
     **/
    public void ejbCreate() throws CreateException {

        Debug.print("ApSupplierClassControllerBean ejbCreate");

    }

}

