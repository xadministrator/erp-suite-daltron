
/*
 * ApSupplierEntryControllerBean.java
 *
 * Created on February 16, 2004, 10:44 AM
 *
 * @author  Neil Andrew M. Ajero
 */

package com.ejb.txn;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;
import javax.naming.NamingException;

import com.ejb.ad.LocalAdBankAccount;
import com.ejb.ad.LocalAdBankAccountHome;
import com.ejb.ad.LocalAdBranch;
import com.ejb.ad.LocalAdBranchHome;
import com.ejb.ad.LocalAdBranchSupplier;
import com.ejb.ad.LocalAdBranchSupplierHome;
import com.ejb.ad.LocalAdPaymentTerm;
import com.ejb.ad.LocalAdPaymentTermHome;
import com.ejb.ad.LocalAdPreference;
import com.ejb.ad.LocalAdPreferenceHome;
import com.ejb.ad.LocalAdResponsibility;
import com.ejb.ad.LocalAdResponsibilityHome;
import com.ejb.ap.LocalApSupplier;
import com.ejb.ap.LocalApSupplierClass;
import com.ejb.ap.LocalApSupplierClassHome;
import com.ejb.ap.LocalApSupplierHome;
import com.ejb.ap.LocalApSupplierType;
import com.ejb.ap.LocalApSupplierTypeHome;
import com.ejb.ar.LocalArCustomer;
import com.ejb.ar.LocalArCustomerHome;
import com.ejb.exception.ApSCCoaGlExpenseAccountNotFoundException;
import com.ejb.exception.ApSCCoaGlPayableAccountNotFoundException;
import com.ejb.exception.GlobalNameAndAddressAlreadyExistsException;
import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.exception.GlobalRecordAlreadyExistException;
import com.ejb.gl.LocalGlAccountingCalendarValue;
import com.ejb.gl.LocalGlChartOfAccount;
import com.ejb.gl.LocalGlChartOfAccountHome;
import com.ejb.gl.LocalGlInvestorAccountBalance;
import com.ejb.gl.LocalGlInvestorAccountBalanceHome;
import com.ejb.gl.LocalGlSetOfBook;
import com.ejb.gl.LocalGlSetOfBookHome;
import com.ejb.hr.LocalHrDeployedBranch;
import com.ejb.hr.LocalHrDeployedBranchHome;
import com.ejb.hr.LocalHrEmployee;
import com.ejb.hr.LocalHrEmployeeHome;
import com.ejb.inv.LocalInvLineItemTemplate;
import com.ejb.inv.LocalInvLineItemTemplateHome;
import com.util.AbstractSessionBean;
import com.util.AdBranchDetails;
import com.util.AdBranchSupplierDetails;
import com.util.AdModBranchSupplierDetails;
import com.util.AdResponsibilityDetails;
import com.util.ApModSupplierClassDetails;
import com.util.ApModSupplierDetails;
import com.util.ApModSupplierTypeDetails;
import com.util.ApSupplierDetails;
import com.util.Debug;
import com.util.EJBCommon;
import com.util.EJBHomeFactory;

/**
 * @ejb:bean name="ApSupplierEntryControllerEJB"
 *           display-name="Used for entering suppliers"
 *           type="Stateless"
 *           view-type="remote"
 *           jndi-name="ejb/ApSupplierEntryControllerEJB"
 *
 * @ejb:interface remote-class="com.ejb.txn.ApSupplierEntryController"
 *                extends="javax.ejb.EJBObject"
 *
 * @ejb:home remote-class="com.ejb.txn.ApSupplierEntryControllerHome"
 *           extends="javax.ejb.EJBHome"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="apuser"
 *                        role-link="apuserlink"
 *
 * @ejb:permission role-name="apuser"
 * 
*/

public class ApSupplierEntryControllerBean extends AbstractSessionBean {
	
	
 	private void generateGlInvtrBalance(LocalApSupplier apSupplier, Integer AD_CMPNY) 
 	{
 		
            Debug.print("ApSupplierEntryControllerBean generateGlInvtrBalance");

            LocalGlSetOfBookHome glSetOfBookHome = null;
            LocalApSupplierHome apSupplierHome = null;
            LocalGlInvestorAccountBalanceHome glInvestorAccountBalanceHome = null;

            // Initialize EJB Home

            try {
                    apSupplierHome = (LocalApSupplierHome)EJBHomeFactory.
                                    lookUpLocalHome(LocalApSupplierHome.JNDI_NAME, LocalApSupplierHome.class);
                    glInvestorAccountBalanceHome = (LocalGlInvestorAccountBalanceHome)EJBHomeFactory.
                                    lookUpLocalHome(LocalGlInvestorAccountBalanceHome.JNDI_NAME, LocalGlInvestorAccountBalanceHome.class);
                    glSetOfBookHome = (LocalGlSetOfBookHome)EJBHomeFactory.
                                    lookUpLocalHome(LocalGlSetOfBookHome.JNDI_NAME, LocalGlSetOfBookHome.class);

            } catch (NamingException ex) {

                    throw new EJBException(ex.getMessage());

            }	


            try {

                // create balances to all existing customer

            	
                Collection glSetOfBooks = glSetOfBookHome.findSobAll(AD_CMPNY);

                
                Iterator i = glSetOfBooks.iterator();

                while (i.hasNext()) {

                    LocalGlSetOfBook glSetOfBook = (LocalGlSetOfBook)i.next();
                    
                    

                    Collection glAccountingCalendarValues = 
                            glSetOfBook.getGlAccountingCalendar().getGlAccountingCalendarValues();

                    Iterator iterAcv = glAccountingCalendarValues.iterator();

                    while (iterAcv.hasNext()) {

                            LocalGlAccountingCalendarValue glAccountingCalendarValue =
                                    (LocalGlAccountingCalendarValue) iterAcv.next();


                            LocalGlInvestorAccountBalance glInvestorAccountBalance = null;
                            try {

                                    glInvestorAccountBalance = glInvestorAccountBalanceHome.create(0d, 0d,(byte)0,(byte)0, 0d, 0d, 0d, 0d, 0d, 0d, AD_CMPNY);
                                    glInvestorAccountBalance.setGlAccountingCalendarValue(glAccountingCalendarValue);

                                    glInvestorAccountBalance.setApSupplier(apSupplier);
                            } catch (Exception ex) {

                                    getSessionContext().setRollbackOnly();
                                    throw new EJBException(ex.getMessage());

                            }
                    }

                }

            } catch (Exception ex) {

                    getSessionContext().setRollbackOnly();
                    throw new EJBException(ex.getMessage());
            }
 		
 		 
 	}
	
    
    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList getApStAll(Integer AD_CMPNY) {
    	
    	
        
        Debug.print("ApSupplierEntryControllerBean getApStAll");
        
        LocalApSupplierTypeHome apSupplierTypeHome = null;
        LocalApSupplierType apSupplierType = null;

        ArrayList list = new ArrayList();
        
        // Initialize EJB Home
        
        try {
            
            apSupplierTypeHome = (LocalApSupplierTypeHome)EJBHomeFactory.
                lookUpLocalHome(LocalApSupplierTypeHome.JNDI_NAME, LocalApSupplierTypeHome.class);
            
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {
            
            Collection apSupplierTypes = apSupplierTypeHome.findEnabledStAll(AD_CMPNY);

	        Iterator i = apSupplierTypes.iterator();
	               
	        while (i.hasNext()) {
	        	
	        	apSupplierType = (LocalApSupplierType)i.next();
	        	
	        	list.add(apSupplierType.getStName());
	        	
	        }
	        
	        return list;
            
        } catch (Exception ex) {
        	
        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());
        	
        }
            
    }
    
    
    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public byte getAdPrfArUseCustomerPulldown(Integer AD_CMPNY) {
    	
    	Debug.print("ApSupplierEntryControllerBean getAdPrfArUseCustomerPulldown");
    	
    	LocalAdPreferenceHome adPreferenceHome = null;         
    	
    	// Initialize EJB Home
    	
    	try {
    		
    		adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);       
    		
    	} catch (NamingException ex) {
    		
    		throw new EJBException(ex.getMessage());
    		
    	}
    	
    	
    	try {
    		
    		LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);
    		
    		return adPreference.getPrfArUseCustomerPulldown();
    		
    	} catch (Exception ex) {
    		
    		Debug.printStackTrace(ex);
    		throw new EJBException(ex.getMessage());
    		
    	}
    	
    }
    
    
    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList getArCstAll(Integer AD_BRNCH, Integer AD_CMPNY) {
                    
        Debug.print("ApSupplierEntryControllerBean getArCstAll");
        
        LocalArCustomerHome arCustomerHome = null;               
        
        ArrayList list = new ArrayList();
        
        // Initialize EJB Home
        
        try {
            
            arCustomerHome = (LocalArCustomerHome)EJBHomeFactory.
                lookUpLocalHome(LocalArCustomerHome.JNDI_NAME, LocalArCustomerHome.class);
            
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }        
        
        try {
        	
        	Collection arCustomers = arCustomerHome.findEnabledCstAll(AD_BRNCH, AD_CMPNY);
        	
        	Iterator i = arCustomers.iterator();
        	
        	while (i.hasNext()) {
        		
        		LocalArCustomer arCustomer = (LocalArCustomer)i.next();
        		
        		list.add(arCustomer.getCstCustomerCode());
        		
        	}
        	
        	return list;
        	
        } catch (Exception ex) {
        	
        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());
        	
        }
            
    }
    
          
    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList getApPytAll(Integer AD_CMPNY) {
                    
        Debug.print("ApSupplierEntryControllerBean getApPytAll");
        
        LocalAdPaymentTermHome adPaymentTermHome = null;
        LocalAdPaymentTerm adPaymentTerm = null;
        
        ArrayList list = new ArrayList();
        
        // Initialize EJB Home
        
        try {
            
            adPaymentTermHome = (LocalAdPaymentTermHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdPaymentTermHome.JNDI_NAME, LocalAdPaymentTermHome.class);
            
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {
            
            Collection adPaymentTerms = adPaymentTermHome.findEnabledPytAll(AD_CMPNY);

	        Iterator i = adPaymentTerms.iterator();
	               
	        while (i.hasNext()) {
	        	
	        	adPaymentTerm = (LocalAdPaymentTerm)i.next();
	        	
	        	list.add(adPaymentTerm.getPytName());
	        	
	        }
	        
	        return list;
            
        } catch (Exception ex) {
        	
        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());
        	
        }
            
    }
    
    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList getApScAll(Integer AD_CMPNY) {
                    
        Debug.print("ApSupplierEntryControllerBean getApScAll");
        
        LocalApSupplierClassHome apSupplierClassHome = null;
        LocalApSupplierClass apSupplierClass = null;

        ArrayList list = new ArrayList();
        
        // Initialize EJB Home
        
        try {
            
            apSupplierClassHome = (LocalApSupplierClassHome)EJBHomeFactory.
                lookUpLocalHome(LocalApSupplierClassHome.JNDI_NAME, LocalApSupplierClassHome.class);
            
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {
            
            Collection apSupplierClasses = apSupplierClassHome.findEnabledScAll(AD_CMPNY);
	        
	        Iterator i = apSupplierClasses.iterator();
	               
	        while (i.hasNext()) {
	        	
	        	apSupplierClass = (LocalApSupplierClass)i.next();
	        	
	        	list.add(apSupplierClass.getScName());
	        	
	        }
	        
	        return list;
          
        } catch (Exception ex) {
        	
        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());
        	
        }
            
    }
    
    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public byte getPrfApAutoGenerateSupplierCode(Integer AD_CMPNY) {
        
        Debug.print("ApSupplierEntryControllerBean getPrfApAutoGenerateSupplierCode");
        
        LocalAdPreferenceHome adPreferenceHome = null;
                
         // Initialize EJB Home
          
         try {
              
            adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);       
              
         } catch (NamingException ex) {
              
            throw new EJBException(ex.getMessage());
              
         }
        

         try {
         	
            LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);
            
            return adPreference.getPrfApAutoGenerateSupplierCode();
           
         } catch (Exception ex) {
          	 
            Debug.printStackTrace(ex);
            throw new EJBException(ex.getMessage());
           
         }
    }
    
    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public String getPrfApNextSupplierCode(Integer AD_CMPNY) {
        
        Debug.print("ApSupplierEntryControllerBean getPrfApNextSupplierCode");
        
        LocalAdPreferenceHome adPreferenceHome = null;
                
         // Initialize EJB Home
          
         try {
              
            adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);       
              
         } catch (NamingException ex) {
              
            throw new EJBException(ex.getMessage());
              
         }
        

         try {
         	
            LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);
            return adPreference.getPrfApNextSupplierCode();
           
         } catch (Exception ex) {
          	 
            Debug.printStackTrace(ex);
            throw new EJBException(ex.getMessage());
           
         }
    }
    
    /**
     * @ejb:interface-method view-type="remote"
     **/
    public void saveApSplEntry(com.util.ApModSupplierDetails details, String ST_NM,
        String PYT_NM, String SC_NM, String SPL_COA_GL_PYBL_ACCNT, String SPL_COA_GL_EXPNS_ACCNT, String BA_NM, 
		String RS_NM, ArrayList branchList, String LIT_NM, Integer AD_CMPNY)
        throws GlobalRecordAlreadyExistException,
        GlobalNameAndAddressAlreadyExistsException,
        ApSCCoaGlPayableAccountNotFoundException,
        ApSCCoaGlExpenseAccountNotFoundException {
        	
        Debug.print("ApSupplierEntryControllerBean saveApSplEntry");
        
        LocalHrEmployeeHome hrEmployeeHome = null;
        LocalHrDeployedBranchHome hrDeployedBranchHome = null;
        
        LocalApSupplierHome apSupplierHome = null;
        LocalAdPaymentTermHome adPaymentTermHome = null; 
        LocalArCustomerHome arCustomerHome = null;
        LocalApSupplierTypeHome apSupplierTypeHome = null;
        LocalApSupplierClassHome apSupplierClassHome = null;
        LocalGlChartOfAccountHome glChartOfAccountHome = null;
        LocalAdBankAccountHome adBankAccountHome = null;
        LocalAdPreferenceHome adPreferenceHome = null;
        LocalAdBranchHome adBranchHome = null;
    	LocalAdBranchSupplierHome adBranchSupplierHome = null;
    	LocalInvLineItemTemplateHome invLineItemTemplateHome = null;

        LocalApSupplier apSupplier = null;
        LocalAdPreference adPreference = null;
    	LocalAdBranch adBranch = null;
    	LocalAdBranchSupplier adBranchSupplier = null;
    	
        try {
            
        	hrDeployedBranchHome = (LocalHrDeployedBranchHome)EJBHomeFactory.
        			lookUpLocalHome(LocalHrDeployedBranchHome.JNDI_NAME, LocalHrDeployedBranchHome.class);
        	
            hrEmployeeHome = (LocalHrEmployeeHome)EJBHomeFactory.
                    lookUpLocalHome(LocalHrEmployeeHome.JNDI_NAME, LocalHrEmployeeHome.class);
        	
            
            apSupplierHome = (LocalApSupplierHome)EJBHomeFactory.
                lookUpLocalHome(LocalApSupplierHome.JNDI_NAME, LocalApSupplierHome.class);
            adPaymentTermHome = (LocalAdPaymentTermHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdPaymentTermHome.JNDI_NAME, LocalAdPaymentTermHome.class);
            arCustomerHome = (LocalArCustomerHome)EJBHomeFactory.
                    lookUpLocalHome(LocalArCustomerHome.JNDI_NAME, LocalArCustomerHome.class);
            
            apSupplierTypeHome = (LocalApSupplierTypeHome)EJBHomeFactory.
                lookUpLocalHome(LocalApSupplierTypeHome.JNDI_NAME, LocalApSupplierTypeHome.class);
            apSupplierClassHome = (LocalApSupplierClassHome)EJBHomeFactory.
                lookUpLocalHome(LocalApSupplierClassHome.JNDI_NAME, LocalApSupplierClassHome.class);                
            glChartOfAccountHome = (LocalGlChartOfAccountHome)EJBHomeFactory.
            	lookUpLocalHome(LocalGlChartOfAccountHome.JNDI_NAME, LocalGlChartOfAccountHome.class);
            adBankAccountHome = (LocalAdBankAccountHome)EJBHomeFactory.
        		lookUpLocalHome(LocalAdBankAccountHome.JNDI_NAME, LocalAdBankAccountHome.class);
            adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
            	lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME,LocalAdPreferenceHome.class);
            adBranchSupplierHome = (LocalAdBranchSupplierHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdBranchSupplierHome.JNDI_NAME, LocalAdBranchSupplierHome.class);
            adBranchHome = (LocalAdBranchHome)EJBHomeFactory.
        		lookUpLocalHome(LocalAdBranchHome.JNDI_NAME, LocalAdBranchHome.class);
            invLineItemTemplateHome = (LocalInvLineItemTemplateHome)EJBHomeFactory.
    			lookUpLocalHome(LocalInvLineItemTemplateHome.JNDI_NAME, LocalInvLineItemTemplateHome.class);
            
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {
            
            adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);
            
        } catch (Exception ex) {
            
            Debug.printStackTrace(ex);
            throw new EJBException(ex.getMessage());
        }
                
        	
        try {
        	
        	 LocalGlChartOfAccount glPayableChartOfAccount = null;
        	 LocalGlChartOfAccount glExpenseChartOfAccount = null;         	         	         	 
        	 
        	 // autoGenerate  
        	 if(adPreference.getPrfApAutoGenerateSupplierCode() == EJBCommon.TRUE && 
        	    (details.getSplSupplierCode() == null || details.getSplSupplierCode().length() == 0 )) {
        	     
        	     while(true) {
        	         
        	         try {
        	             
        	             apSupplierHome.findBySplSupplierCode(adPreference.getPrfApNextSupplierCode(), AD_CMPNY);       	             
        	             adPreference.setPrfApNextSupplierCode(
        	                     EJBCommon.incrementStringNumber(adPreference.getPrfApNextSupplierCode()));
        	             
        	         } catch(FinderException ex) {
        	             
        	             details.setSplSupplierCode(adPreference.getPrfApNextSupplierCode());
        	             adPreference.setPrfApNextSupplierCode(
        	                     EJBCommon.incrementStringNumber(adPreference.getPrfApNextSupplierCode()));
        	             break;
        	             
        	         }
        	     }
        	     
        	 } 
        	 
         	 // validate if supplier code already exists
         	
         	 try {
      	
         		 
	            apSupplier = apSupplierHome.findBySplSupplierCode(details.getSplSupplierCode(), AD_CMPNY);
	            
	            System.out.println("details.getSplSupplierCode()="+details.getSplSupplierCode());
	            if(details.getSplCode() == null || details.getSplCode() != null && 
	                  !apSupplier.getSplCode().equals(details.getSplCode())) {          
	                    
	            	
	            	System.out.println("apSupplier.getSplCode()="+apSupplier.getSplCode());
		            System.out.println("details.getSplCode()="+details.getSplCode());
		            
	                throw new GlobalRecordAlreadyExistException();
	                
	            }   
	                                             
	         } catch (GlobalRecordAlreadyExistException ex) {
	        	
	        	throw ex;
	        	
	         } catch (FinderException ex) {
	        	 	
	         }
	         System.out.println("---------------------------------->");
	         //validate if supplier with this name and address already exists
	         
	         try {
	         	
	         	apSupplier = apSupplierHome.findBySplNameAndAddress(details.getSplName(), details.getSplAddress(), AD_CMPNY);
	         	
	         	if(apSupplier != null && !apSupplier.getSplCode().equals(details.getSplCode())) {
	         		
	         		throw new GlobalNameAndAddressAlreadyExistsException();
	         		
	         	}
	         	
	         } catch (GlobalNameAndAddressAlreadyExistsException ex) {
	         	
	         	throw ex;
	         	
	         } catch (FinderException ex) {
	         	
	         	
	         }
	         
	         try {
				
				glPayableChartOfAccount = glChartOfAccountHome.findByCoaAccountNumber(SPL_COA_GL_PYBL_ACCNT, AD_CMPNY);
				
			 } catch (FinderException ex) {
				
				throw new ApSCCoaGlPayableAccountNotFoundException();
				
			 }
			
			 try {
				
				glExpenseChartOfAccount = glChartOfAccountHome.findByCoaAccountNumber(SPL_COA_GL_EXPNS_ACCNT, AD_CMPNY);
				
			 } catch (FinderException ex) {
				
				throw new ApSCCoaGlExpenseAccountNotFoundException();
				
			 }	
	        	
		     // create new supplier
		     
	         if (details.getSplCode() == null) { 
	        	 System.out.println("------------------------------------------------>NEW SUPPLIER MODE");
		    	apSupplier = apSupplierHome.create(details.getSplSupplierCode(), details.getSplAccountNumber(), 
		            details.getSplName(), details.getSplAddress(),
		            details.getSplCity(), details.getSplStateProvince(), details.getSplPostalCode(),
		            details.getSplCountry(), details.getSplContact(), details.getSplPhone(),
		            details.getSplFax(), details.getSplAlternatePhone(), details.getSplAlternateContact(),
		            details.getSplEmail(), details.getSplTin(), glPayableChartOfAccount.getCoaCode(), 
					glExpenseChartOfAccount.getCoaCode(), details.getSplEnable(), details.getSplRemarks(), AD_CMPNY);
		
		    	if (ST_NM != null && ST_NM.length() > 0) {
		        	
			        LocalApSupplierType apSupplierType = apSupplierTypeHome.findByStName(ST_NM, AD_CMPNY);
			        //apSupplierType.addApSupplier(apSupplier);
			        apSupplier.setApSupplierType(apSupplierType);
				    
		        }
		
			    LocalAdPaymentTerm adPaymentTerm = adPaymentTermHome.findByPytName(PYT_NM, AD_CMPNY);
			    //adPaymentTerm.addApSupplier(apSupplier);
			    apSupplier.setAdPaymentTerm(adPaymentTerm);
 	
		        LocalApSupplierClass apSupplierClass = apSupplierClassHome.findByScName(SC_NM, AD_CMPNY);
		        //apSupplierClass.addApSupplier(apSupplier);
		        apSupplier.setApSupplierClass(apSupplierClass);
			    
		        LocalAdBankAccount adBankAccount = adBankAccountHome.findByBaName(BA_NM, AD_CMPNY);
		        //adBankAccount.addApSupplier(apSupplier);
		        apSupplier.setAdBankAccount(adBankAccount);

		        // create new BranchSupplier
	        	Iterator i = branchList.iterator();
	        	
	        	while(i.hasNext()) {
	        		
	        		AdModBranchSupplierDetails mdetails = (AdModBranchSupplierDetails)i.next();
	        		try {
	        			glPayableChartOfAccount = glChartOfAccountHome.findByCoaAccountNumber(mdetails.getBSplPayableAccountNumber(), AD_CMPNY);
	        		} catch (FinderException ex) {
	        			throw new ApSCCoaGlPayableAccountNotFoundException();
	        		}
	        		
	        		try {
	        			glExpenseChartOfAccount = glChartOfAccountHome.findByCoaAccountNumber(mdetails.getBSplExpenseAccountNumber(), AD_CMPNY);
	        		} catch (FinderException ex) {
	        			throw new ApSCCoaGlExpenseAccountNotFoundException();
	        		}
	        		adBranchSupplier = adBranchSupplierHome.create( glPayableChartOfAccount.getCoaCode(), glExpenseChartOfAccount.getCoaCode(), 'N',AD_CMPNY );
        		
	        		//apSupplier.addAdBranchSupplier(adBranchSupplier);
	        		adBranchSupplier.setApSupplier(apSupplier);
	        		
	        		adBranch = adBranchHome.findByPrimaryKey(mdetails.getBSplBranchCode());
	        		//adBranch.addAdBranchSupplier(adBranchSupplier);
	        		adBranchSupplier.setAdBranch(adBranch);
	        		

	        	}
	        	
	        	if (LIT_NM != null && LIT_NM.length() > 0) {
		        	
		        	LocalInvLineItemTemplate invLineItemTemplate = invLineItemTemplateHome.findByLitName(LIT_NM, AD_CMPNY);
			        //invLineItemTemplate.addApSupplier(apSupplier);
			        apSupplier.setInvLineItemTemplate(invLineItemTemplate);
				    
		        }
	        	
	        	// generate GL Investor Account Balance default value
	        	if(apSupplier.getApSupplierClass().getScLedger() == EJBCommon.TRUE){
	        		this.generateGlInvtrBalance(apSupplier, AD_CMPNY);
	        	}
	        	
		        
		    } else {
		    	
		    	
		    	// update supplier 
		    	System.out.println("------------------------------------------------>UPDATE SUPPLIER MODE");
		    	apSupplier = apSupplierHome.findByPrimaryKey(details.getSplCode());
		
					apSupplier.setSplSupplierCode(details.getSplSupplierCode());
                                        apSupplier.setSplAccountNumber(details.getSplAccountNumber());
					apSupplier.setSplName(details.getSplName());
					apSupplier.setSplAddress(details.getSplAddress());
					apSupplier.setSplCity(details.getSplCity());
					apSupplier.setSplStateProvince(details.getSplStateProvince());
					apSupplier.setSplPostalCode(details.getSplPostalCode()); 
					apSupplier.setSplCountry(details.getSplCountry()); 
					apSupplier.setSplContact(details.getSplContact());
					apSupplier.setSplPhone(details.getSplPhone());
					apSupplier.setSplFax(details.getSplFax());
					apSupplier.setSplAlternatePhone(details.getSplAlternatePhone()); 
					apSupplier.setSplAlternateContact(details.getSplAlternateContact());
					apSupplier.setSplEmail(details.getSplEmail());
					apSupplier.setSplTin(details.getSplTin());
					apSupplier.setSplEnable(details.getSplEnable());
					apSupplier.setSplCoaGlPayableAccount(glPayableChartOfAccount.getCoaCode());
					apSupplier.setSplCoaGlExpenseAccount(glExpenseChartOfAccount.getCoaCode());
					apSupplier.setSplRemarks(details.getSplRemarks());
										
				
		        if (ST_NM != null && ST_NM.length() > 0) {
		        	
			        LocalApSupplierType apSupplierType = apSupplierTypeHome.findByStName(ST_NM, AD_CMPNY);
			        //apSupplierType.addApSupplier(apSupplier);
			        apSupplier.setApSupplierType(apSupplierType);
				    
		        }

	
			    LocalAdPaymentTerm adPaymentTerm = adPaymentTermHome.findByPytName(PYT_NM, AD_CMPNY);
			    //adPaymentTerm.addApSupplier(apSupplier);
			    apSupplier.setAdPaymentTerm(adPaymentTerm);
			    
		        LocalApSupplierClass apSupplierClass = apSupplierClassHome.findByScName(SC_NM, AD_CMPNY);
		        //apSupplierClass.addApSupplier(apSupplier);
		        apSupplier.setApSupplierClass(apSupplierClass);
		        
		        // generate GL Investor Account Balance default value
	        	if(apSupplier.getApSupplierClass().getScLedger() == EJBCommon.TRUE){
	        		if(apSupplier.getGlInvestorAccountBalances().size() == 0) this.generateGlInvtrBalance(apSupplier, AD_CMPNY);
	        	}
	        	
			    
		        LocalAdBankAccount adBankAccount = adBankAccountHome.findByBaName(BA_NM, AD_CMPNY);
		        //adBankAccount.addApSupplier(apSupplier);
		        apSupplier.setAdBankAccount(adBankAccount);
			    
		        // remove all BranchSupplier
		        Collection adBranchSuppliers = adBranchSupplierHome.findBSplBySplCodeAndRsName(apSupplier.getSplCode(), RS_NM, AD_CMPNY);
	            
	            Iterator k = adBranchSuppliers.iterator();
	            
	            while(k.hasNext()) {
	            	            	
	            	adBranchSupplier = (LocalAdBranchSupplier)k.next();
	            	apSupplier.dropAdBranchSupplier(adBranchSupplier);
	            	
	            	adBranch = adBranchHome.findByPrimaryKey(adBranchSupplier.getAdBranch().getBrCode());
	            	adBranch.dropAdBranchSupplier(adBranchSupplier);
	            	adBranchSupplier.remove();
	            }
		        
		        // create new BranchSupplier
	        	Iterator i = branchList.iterator();
	        	
	        	while(i.hasNext()) {
	        		
	        		AdModBranchSupplierDetails mdetails = (AdModBranchSupplierDetails)i.next();
	        		try {
		        		glPayableChartOfAccount = glChartOfAccountHome.findByCoaAccountNumber(mdetails.getBSplPayableAccountNumber(), AD_CMPNY);	        			
	        		} catch (FinderException ex) {
	        			throw new ApSCCoaGlPayableAccountNotFoundException();
	        		}

	        		try {
		        		glExpenseChartOfAccount = glChartOfAccountHome.findByCoaAccountNumber(mdetails.getBSplExpenseAccountNumber(), AD_CMPNY);
	        		} catch (FinderException ex) {
	        			throw new ApSCCoaGlExpenseAccountNotFoundException();
	        		}
	        		adBranchSupplier = adBranchSupplierHome.create( glPayableChartOfAccount.getCoaCode(), glExpenseChartOfAccount.getCoaCode(), 'N',AD_CMPNY );
        		
	        		//apSupplier.addAdBranchSupplier(adBranchSupplier);
	        		adBranchSupplier.setApSupplier(apSupplier);
	        		
	        		adBranch = adBranchHome.findByPrimaryKey(mdetails.getBSplBranchCode());
	        		//adBranch.addAdBranchSupplier(adBranchSupplier);
	        		adBranchSupplier.setAdBranch(adBranch);
	        	}
	        	
	        	LocalInvLineItemTemplate invLineItemTemplate = null;
	        	
	        	if(LIT_NM != null && LIT_NM.length() > 0 && !LIT_NM.equalsIgnoreCase("NO RECORD FOUND")) {
		        	
	        		invLineItemTemplate = invLineItemTemplateHome.findByLitName(LIT_NM, AD_CMPNY);
	        		//invLineItemTemplate.addApSupplier(apSupplier);
	        		apSupplier.setInvLineItemTemplate(invLineItemTemplate);
				    
		        } else {
		        	
		        	if (apSupplier.getInvLineItemTemplate() != null) {
	        			
		        		invLineItemTemplate = invLineItemTemplateHome.findByLitName(apSupplier.getInvLineItemTemplate().getLitName(), AD_CMPNY);
		        		invLineItemTemplate.dropApSupplier(apSupplier);
	        			
	        		}
		        	
		        }
	        }
	        
	 	} catch (GlobalRecordAlreadyExistException ex) {
	 		
	 		getSessionContext().setRollbackOnly();
	 		throw ex;	
	 		
	 	} catch (GlobalNameAndAddressAlreadyExistsException ex) {
	 		
	 		getSessionContext().setRollbackOnly();
	 		throw ex;	
	 		
	 	} catch (ApSCCoaGlPayableAccountNotFoundException ex) {
	 		
	 		getSessionContext().setRollbackOnly();
	 		throw ex;
	 		
	 	} catch (ApSCCoaGlExpenseAccountNotFoundException ex) {
	 		
	 		getSessionContext().setRollbackOnly();
	 		throw ex;
           
	    } catch (Exception ex) {
	            
	    	Debug.printStackTrace(ex);
	    	getSessionContext().setRollbackOnly();
	    	throw new EJBException(ex.getMessage());
	    	
	    }   
	 }
	 
    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ApModSupplierDetails getApSplBySplCode(Integer SPL_CODE, Integer AD_CMPNY)
    throws GlobalNoRecordFoundException  {

    	Debug.print("ApSupplierEntryControllerBean getApSplBySplCode");

    	LocalApSupplierHome apSupplierHome = null;
    	LocalGlChartOfAccountHome glChartOfAccountHome = null;

    	LocalApSupplier apSupplier = null;

    	// Initialize EJB Home

    	try {

    		apSupplierHome = (LocalApSupplierHome)EJBHomeFactory.
    		lookUpLocalHome(LocalApSupplierHome.JNDI_NAME, LocalApSupplierHome.class);
    		glChartOfAccountHome = (LocalGlChartOfAccountHome)EJBHomeFactory.
    		lookUpLocalHome(LocalGlChartOfAccountHome.JNDI_NAME, LocalGlChartOfAccountHome.class);

    	} catch (NamingException ex) {

    		throw new EJBException(ex.getMessage());

    	}

    	try {

    		try {

    			apSupplier = apSupplierHome.findByPrimaryKey(SPL_CODE);      

    		} catch (FinderException ex) {

    			throw new GlobalNoRecordFoundException();

    		}

    		ApModSupplierDetails splDetails = new ApModSupplierDetails();
    		splDetails.setSplCode(apSupplier.getSplCode());
    		splDetails.setSplSupplierCode(apSupplier.getSplSupplierCode());
                splDetails.setSplAccountNumber(apSupplier.getSplAccountNumber());
    		splDetails.setSplName(apSupplier.getSplName());
    		splDetails.setSplAddress(apSupplier.getSplAddress());
    		splDetails.setSplCity(apSupplier.getSplCity());
    		splDetails.setSplStateProvince(apSupplier.getSplStateProvince());
    		splDetails.setSplPostalCode(apSupplier.getSplPostalCode()); 
    		splDetails.setSplCountry(apSupplier.getSplCountry()); 
    		splDetails.setSplContact(apSupplier.getSplContact());
    		splDetails.setSplPhone(apSupplier.getSplPhone());
    		splDetails.setSplFax(apSupplier.getSplFax());
    		splDetails.setSplAlternatePhone(apSupplier.getSplAlternatePhone()); 
    		splDetails.setSplAlternateContact(apSupplier.getSplAlternateContact());
    		splDetails.setSplEmail(apSupplier.getSplEmail());
    		splDetails.setSplTin(apSupplier.getSplTin()); 
    		splDetails.setSplEnable(apSupplier.getSplEnable());	 

    		splDetails.setSplStName(apSupplier.getApSupplierType() != null ? 
    				apSupplier.getApSupplierType().getStName() :
    					null);

    		splDetails.setSplPytName(apSupplier.getAdPaymentTerm() != null ? 
    				apSupplier.getAdPaymentTerm().getPytName() :
    					null);

    		splDetails.setSplScName(apSupplier.getApSupplierClass() != null ? 
    				apSupplier.getApSupplierClass().getScName() :
    					null);	        

    		LocalGlChartOfAccount glChartOfAccount = glChartOfAccountHome.findByPrimaryKey(apSupplier.getSplCoaGlPayableAccount());		        		        

    		splDetails.setSplCoaGlPayableAccountNumber(glChartOfAccount.getCoaAccountNumber());
    		splDetails.setSplCoaGlPayableAccountDescription(glChartOfAccount.getCoaAccountDescription());

    		glChartOfAccount = glChartOfAccountHome.findByPrimaryKey(apSupplier.getSplCoaGlExpenseAccount());

    		splDetails.setSplCoaGlExpenseAccountNumber(glChartOfAccount.getCoaAccountNumber());
    		splDetails.setSplCoaGlExpenseAccountDescription(glChartOfAccount.getCoaAccountDescription());

    		splDetails.setSplBaName(apSupplier.getAdBankAccount().getBaName());

    		splDetails.setSplLitName(apSupplier.getInvLineItemTemplate() != null ?
    				apSupplier.getInvLineItemTemplate().getLitName() : null);

    		splDetails.setSplRemarks(apSupplier.getSplRemarks());

    		return splDetails;              

    	} catch (GlobalNoRecordFoundException ex) {

    		throw ex;

    	} catch (Exception ex) {

    		Debug.printStackTrace(ex);
    		throw new EJBException(ex.getMessage());

    	}

    }	 
    
    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList getAdBaAll(Integer AD_CMPNY) {
                    
        Debug.print("ApSupplierEntryControllerBean getAdBaAll");
        
        LocalAdBankAccountHome adBankAccountHome = null;

        ArrayList list = new ArrayList();
        
        // Initialize EJB Home
        
        try {
            
        	adBankAccountHome = (LocalAdBankAccountHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdBankAccountHome.JNDI_NAME, LocalAdBankAccountHome.class);
            
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {
            
            Collection adBankAccounts = adBankAccountHome.findEnabledBaAll(AD_CMPNY);
	        
	        Iterator i = adBankAccounts.iterator();
	               
	        while (i.hasNext()) {
	        	
	        	LocalAdBankAccount adBankAccount = (LocalAdBankAccount)i.next();
	        	
	        	list.add(adBankAccount.getBaName());
	        	
	        }
	        
	        return list;
          
        } catch (Exception ex) {
        	
        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());
        	
        }
            
    }
    
    
    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public com.util.ApModSupplierClassDetails getApScByScName(String SC_NM, Integer AD_CMPNY) {
                    
        Debug.print("ApSupplierEntryControllerBean getApScByScName");
        
        LocalApSupplierClassHome apSupplierClassHome = null;
        LocalGlChartOfAccountHome glChartOfAccountHome = null;
       
        ArrayList list = new ArrayList();
        
        // Initialize EJB Home
        
        try {
            
        	apSupplierClassHome = (LocalApSupplierClassHome)EJBHomeFactory.
                lookUpLocalHome(LocalApSupplierClassHome.JNDI_NAME, LocalApSupplierClassHome.class);
        	glChartOfAccountHome = (LocalGlChartOfAccountHome)EJBHomeFactory.
            	lookUpLocalHome(LocalGlChartOfAccountHome.JNDI_NAME, LocalGlChartOfAccountHome.class);
            
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {
            
            LocalApSupplierClass apSupplierClass = apSupplierClassHome.findByScName(SC_NM, AD_CMPNY);
	        ApModSupplierClassDetails mdetails = new ApModSupplierClassDetails();
            
            LocalGlChartOfAccount glChartOfAccount = glChartOfAccountHome.findByPrimaryKey(apSupplierClass.getScGlCoaPayableAccount());     
	        mdetails.setScCoaGlPayableAccountNumber(glChartOfAccount.getCoaAccountNumber());
	        mdetails.setScCoaGlPayableAccountDescription(glChartOfAccount.getCoaAccountDescription());
	        
	        if (apSupplierClass.getScGlCoaExpenseAccount() != null) {
	        
		        glChartOfAccount = glChartOfAccountHome.findByPrimaryKey(apSupplierClass.getScGlCoaExpenseAccount());
		        mdetails.setScCoaGlExpenseAccountNumber(glChartOfAccount.getCoaAccountNumber());
		        mdetails.setScCoaGlExpenseAccountDescription(glChartOfAccount.getCoaAccountDescription());
		        
	        }
	        
	        return mdetails;
          
        } catch (Exception ex) {
        	
        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());
        	
        }
            
    }
    
    
    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public com.util.ApModSupplierTypeDetails getApStByStName(String ST_NM, Integer AD_CMPNY) {
                    
        Debug.print("ApSupplierEntryControllerBean getApScByScName");
        
        LocalApSupplierTypeHome apSupplierTypeHome = null;

        ArrayList list = new ArrayList();
        
        // Initialize EJB Home
        
        try {
            
        	apSupplierTypeHome = (LocalApSupplierTypeHome)EJBHomeFactory.
                lookUpLocalHome(LocalApSupplierTypeHome.JNDI_NAME, LocalApSupplierTypeHome.class);
        	
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {
            
            LocalApSupplierType apSupplierType = apSupplierTypeHome.findByStName(ST_NM, AD_CMPNY);
	        ApModSupplierTypeDetails mdetails = new ApModSupplierTypeDetails();
	        
	        mdetails.setStBaName(apSupplierType.getAdBankAccount().getBaName());
            	        
	        return mdetails;
          
        } catch (Exception ex) {
        	
        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());
        	
        }
            
    }

    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList getAdBrAll(Integer AD_CMPNY) 
    	throws GlobalNoRecordFoundException{
    	
    	Debug.print("ApSupplierEntryControllerBean getAdBrAll");

    	LocalAdBranchHome adBranchHome = null;
    	LocalAdBranch adBranch = null;

    	Collection adBranches = null;
    	
        ArrayList list = new ArrayList();
        
        // Initialize EJB Home

        try {
            
            adBranchHome = (LocalAdBranchHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdBranchHome.JNDI_NAME, LocalAdBranchHome.class);

        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {
        	
        	adBranches = adBranchHome.findBrAll(AD_CMPNY);
		
        } catch (FinderException ex) {
            
        } catch (Exception ex) {
            
            throw new EJBException(ex.getMessage());
        }
        
        if (adBranches.isEmpty()) {
        	
            throw new GlobalNoRecordFoundException();
        	
        }
        
        Iterator i = adBranches.iterator();
        
        while(i.hasNext()) {
        	
        	adBranch = (LocalAdBranch)i.next();
        	
        	AdBranchDetails details = new AdBranchDetails();
        	details.setBrBranchCode(adBranch.getBrBranchCode());
        	details.setBrCode(adBranch.getBrCode());
        	details.setBrName(adBranch.getBrName());
        	
        	
        	list.add(details);
        	
        }
        
        return list;
    	
    }
    
    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList getAdBrSplAll(Integer BSPL_CODE, Integer AD_CMPNY) 
    	throws GlobalNoRecordFoundException{
    	
    	Debug.print("ApSupplierEntryControllerBean getBrSplAll");

    	LocalAdBranchSupplierHome adBranchSupplierHome = null;
    	LocalAdBranchHome adBranchHome = null;
    	LocalGlChartOfAccountHome glChartOfAccountHome = null;
    	
    	LocalAdBranchSupplier adBranchSupplier = null;
    	LocalAdBranch adBranch = null;
    	LocalGlChartOfAccount glChartOfAccount = null;
    	
    	Collection adBranchSuppliers = null;
    	
        ArrayList branchList = new ArrayList();
        ArrayList glCoaAccntList = new ArrayList();
        
        // Initialize EJB Home

        try {
            
        	adBranchSupplierHome = (LocalAdBranchSupplierHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdBranchSupplierHome.JNDI_NAME, LocalAdBranchSupplierHome.class);
            adBranchHome = (LocalAdBranchHome)EJBHomeFactory.
            	lookUpLocalHome(LocalAdBranchHome.JNDI_NAME, LocalAdBranchHome.class);
            glChartOfAccountHome = (LocalGlChartOfAccountHome)EJBHomeFactory.
				lookUpLocalHome(LocalGlChartOfAccountHome.JNDI_NAME, LocalGlChartOfAccountHome.class);

        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {
        	
        	adBranchSuppliers = adBranchSupplierHome.findBSplBySpl(BSPL_CODE, AD_CMPNY);
		
        } catch (FinderException ex) {
            
        } catch (Exception ex) {
            
            throw new EJBException(ex.getMessage());
        }
        
        if (adBranchSuppliers.isEmpty()) {
        	
            throw new GlobalNoRecordFoundException();
        	
        }
        
        try {
        
	        Iterator i = adBranchSuppliers.iterator();
	        
	        while(i.hasNext()) {
	        	
	        	adBranchSupplier = (LocalAdBranchSupplier)i.next();
	        	
		    	adBranch = adBranchHome.findByPrimaryKey(adBranchSupplier.getAdBranch().getBrCode());
		    	
		    	AdBranchDetails details = new AdBranchDetails();
		    	
	        	details.setBrBranchCode(adBranch.getBrBranchCode());
	        	details.setBrCode(adBranch.getBrCode());
	        	details.setBrName(adBranch.getBrName());
		    	
		    	// get the gl chart of account for this bspl_code
		    	ApSupplierDetails apSplDetails = new ApSupplierDetails(); 
		    	
		    	glChartOfAccount = glChartOfAccountHome.findByPrimaryKey(adBranchSupplier.getBsplGlCoaPayableAccount());		    		
		    	apSplDetails.setSplCoaGlPayableAccount(glChartOfAccount.getCoaCode());
		    	
		    	glChartOfAccount = glChartOfAccountHome.findByPrimaryKey(adBranchSupplier.getBsplGlCoaExpenseAccount());
		    	apSplDetails.setSplCoaGlExpenseAccount(glChartOfAccount.getCoaCode());
		    	
		    	branchList.add(details);
	            glCoaAccntList.add(apSplDetails);
	        }
	        
        } catch (FinderException ex) {
            
        } catch (Exception ex) {
            
            throw new EJBException(ex.getMessage());
        }
        
        ArrayList branchAndGlAccnts = new ArrayList();
        branchAndGlAccnts.add(branchList);
        branchAndGlAccnts.add(glCoaAccntList);
        
        return branchAndGlAccnts;
    }

    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public AdResponsibilityDetails getAdRsByRsCode(Integer RS_CODE) 
    	throws GlobalNoRecordFoundException {
    	
    	LocalAdResponsibilityHome adResHome = null;
    	LocalAdResponsibility adRes = null;
   	
    	try {
    		adResHome = (LocalAdResponsibilityHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdResponsibilityHome.JNDI_NAME, LocalAdResponsibilityHome.class);
    	} catch (NamingException ex) {
    		
    	}
    	
    	try {
        	adRes = adResHome.findByPrimaryKey(RS_CODE);    		
    	} catch (FinderException ex) {
    		
    	}
    	
    	AdResponsibilityDetails details = new AdResponsibilityDetails();
    	details.setRsName(adRes.getRsName());
    	
    	return details;
    }
    
    // SessionBean methods

    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList getAdBrSplBySplCode(Integer SPL_CODE, Integer AD_CMPNY) 
    	throws GlobalNoRecordFoundException {

    	LocalAdBranchSupplierHome adBrSplHome = null;
    	LocalGlChartOfAccountHome glCoaHome = null;
    	
    	LocalAdBranchSupplier adBrSpl = null;
    	LocalGlChartOfAccount glPayableCoa = null;
    	LocalGlChartOfAccount glExpenseCoa = null;

    	Collection adBrSpls = null;
    	ArrayList branchSuppliers = new ArrayList();
    	
    	try {
    		adBrSplHome = (LocalAdBranchSupplierHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdBranchSupplierHome.JNDI_NAME, LocalAdBranchSupplierHome.class);
    		glCoaHome = (LocalGlChartOfAccountHome)EJBHomeFactory.
				lookUpLocalHome(LocalGlChartOfAccountHome.JNDI_NAME, LocalGlChartOfAccountHome.class);
    	} catch (NamingException ex) {
    		
    	}
    	
    	try {
    		adBrSpls = adBrSplHome.findBSplBySpl(SPL_CODE, AD_CMPNY);    		
    	} catch (FinderException ex) {
    		
    	}
    	
    	Iterator i = adBrSpls.iterator();
    	
    	while ( i.hasNext() ) {
    		
    		adBrSpl = (LocalAdBranchSupplier)i.next();
    		
    		try {
    			glPayableCoa = glCoaHome.findByPrimaryKey( adBrSpl.getBsplGlCoaPayableAccount() );
    		} catch (FinderException ex) {
    			throw new GlobalNoRecordFoundException();
    		}

    		try {
        		glExpenseCoa = glCoaHome.findByPrimaryKey( adBrSpl.getBsplGlCoaExpenseAccount() );
    		} catch (FinderException ex) {
    			throw new GlobalNoRecordFoundException();
    		}
    		
        	AdModBranchSupplierDetails mdetails = new AdModBranchSupplierDetails();
        	mdetails.setBSplBranchCode( adBrSpl.getAdBranch().getBrCode() );
        	mdetails.setBSplBranchName( adBrSpl.getAdBranch().getBrName() );
        	mdetails.setBSplPayableAccountNumber( glPayableCoa.getCoaAccountNumber() );
        	mdetails.setBSplPayableAccountDesc( glPayableCoa.getCoaAccountDescription() );
        	mdetails.setBSplExpenseAccountNumber( glExpenseCoa.getCoaAccountNumber() );
        	mdetails.setBSplExpenseAccountDesc( glExpenseCoa.getCoaAccountDescription() );
        	branchSuppliers.add( mdetails );
    	}
    	
    	return branchSuppliers;
    }
    
    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList getInvLitAll(Integer AD_CMPNY) {
                    
        Debug.print("ApSupplierEntryControllerBean getInvLitAll");
        
        LocalInvLineItemTemplateHome invLineItemTemplateHome = null;
        LocalInvLineItemTemplate invLineItemTemplate = null;

        ArrayList list = new ArrayList();
        
        // Initialize EJB Home
        
        try {
            
        	invLineItemTemplateHome = (LocalInvLineItemTemplateHome)EJBHomeFactory.
                lookUpLocalHome(LocalInvLineItemTemplateHome.JNDI_NAME, LocalInvLineItemTemplateHome.class);
            
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {
            
            Collection invLineItemTemplates = invLineItemTemplateHome.findLitAll(AD_CMPNY);
	        
	        Iterator i = invLineItemTemplates.iterator();
	               
	        while (i.hasNext()) {
	        	
	        	invLineItemTemplate = (LocalInvLineItemTemplate)i.next();
	        	
	        	list.add(invLineItemTemplate.getLitName());
	        	
	        }
	        
	        return list;
          
        } catch (Exception ex) {
        	
        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());
        	
        }
            
    }

    // SessionBean methods
    
    /**
     * @ejb:create-method view-type="remote"
     **/
    public void ejbCreate() throws CreateException {

       Debug.print("ApSupplierEntryControllerBean ejbCreate");
      
    }
}