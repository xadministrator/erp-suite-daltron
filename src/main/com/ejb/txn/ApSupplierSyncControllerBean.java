
/*
 * ApSupplierSyncControllerBean.java
 *
 * Created on Sometime December 2007
 *
 * @author  BONBON
 */

package com.ejb.txn;

import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.SessionBean;
import javax.ejb.SessionContext;
import javax.naming.NamingException;

import com.ejb.ad.LocalAdBranch;
import com.ejb.ad.LocalAdBranchSupplier;
import com.ejb.ad.LocalAdBranchSupplierHome;
import com.ejb.ad.LocalAdBranchHome;
import com.ejb.ap.LocalApSupplier;
import com.ejb.ap.LocalApSupplierHome;
import com.ejb.ap.LocalApTaxCode;
import com.ejb.ap.LocalApTaxCodeHome;
import com.ejb.ap.LocalApWithholdingTaxCode;
import com.ejb.ap.LocalApWithholdingTaxCodeHome;
import com.ejb.gl.LocalGlChartOfAccount;
import com.ejb.gl.LocalGlChartOfAccountHome;
import com.util.Debug;
import com.util.EJBCommon;
import com.util.EJBHomeFactory;

/**
 * @ejb:bean name="ApSupplierSyncControllerBean"
 *           type="Stateless"
 *           view-type="service-endpoint"
 *           
 * @wsee:port-component name="ApSupplierSync"
 * 
 * @jboss:port-component uri="omega-ejb/ApSupplierSyncWS"
 *
 * @ejb:interface service-endpoint-class="com.ejb.txn.ApSupplierSyncWS"
 * 
*/

public class ApSupplierSyncControllerBean implements SessionBean {
	
	private SessionContext ctx;
	
	/**
     * @ejb:interface-method
     **/
    public int getApSupplierAllNewLength(String BR_BRNCH_CODE, Integer AD_CMPNY) {    	
    
    	Debug.print("ApSupplierSyncControllerBean getApSupplierAllNewLength");
    	
    	LocalApSupplierHome apSupplierHome = null;
    	LocalAdBranchHome adBranchHome = null;
    	
    	// Initialize EJB Home
        
        try {
            
        	apSupplierHome = (LocalApSupplierHome)EJBHomeFactory.
            	lookUpLocalHome(LocalApSupplierHome.JNDI_NAME, LocalApSupplierHome.class);
        	adBranchHome = (LocalAdBranchHome)EJBHomeFactory.
        		lookUpLocalHome(LocalAdBranchHome.JNDI_NAME, LocalAdBranchHome.class);
            
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {
        	
        	LocalAdBranch adBranch = adBranchHome.findByBrBranchCode(BR_BRNCH_CODE, AD_CMPNY);
        	
        	Collection apSupplier = apSupplierHome.findSplBySplNewAndUpdated(adBranch.getBrCode(), AD_CMPNY, 'N', 'N', 'N');        	
        	
        	return apSupplier.size();
        	        	
        } catch (Exception ex) {
        	
        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());
        	
        }
    }
    
    /**
     * @ejb:interface-method
     **/
    public int getApSupplierAllUpdatedLength(String BR_BRNCH_CODE, Integer AD_CMPNY) {    	
    
    	Debug.print("ApSupplierSyncControllerBean getApSupplierAllUpdatedLength");

    	LocalApSupplierHome apSupplierHome = null;
    	LocalAdBranchHome adBranchHome = null;
    	
    	// Initialize EJB Home
        
        try {
            
        	apSupplierHome = (LocalApSupplierHome)EJBHomeFactory.
            	lookUpLocalHome(LocalApSupplierHome.JNDI_NAME, LocalApSupplierHome.class);
        	adBranchHome = (LocalAdBranchHome)EJBHomeFactory.
        		lookUpLocalHome(LocalAdBranchHome.JNDI_NAME, LocalAdBranchHome.class);
        	
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {
        	
        	LocalAdBranch adBranch = adBranchHome.findByBrBranchCode(BR_BRNCH_CODE, AD_CMPNY);
        	
        	Collection apSuppplier = apSupplierHome.findSplBySplNewAndUpdated(adBranch.getBrCode(), AD_CMPNY, 'U','U','X');        	
        	
        	return apSuppplier.size();
        	
        } catch (Exception ex) {
        	
        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());
        	
        }
    }
	
	/**
     * @ejb:interface-method
     **/
    public String[] getApSupplierAllNewAndUpdated(String BR_BRNCH_CODE, Integer AD_CMPNY) {    	
    
    	Debug.print("ApSupplierSyncControllerBean getApSupplierAllNewAndUpdated");
    	
    	LocalAdBranchSupplierHome adBranchSupplierHome = null;
    	LocalApSupplierHome apSupplierHome = null;
    	LocalAdBranchSupplier adBranchSupplier = null;
    	LocalAdBranchHome adBranchHome = null;    	
    	
    	// Initialize EJB Home    	
        try {
            
        	apSupplierHome = (LocalApSupplierHome)EJBHomeFactory.
                lookUpLocalHome(LocalApSupplierHome.JNDI_NAME, LocalApSupplierHome.class);
        	adBranchHome = (LocalAdBranchHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdBranchHome.JNDI_NAME, LocalAdBranchHome.class);
            
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        try {
        	LocalAdBranch adBranch = adBranchHome.findByBrBranchCode(BR_BRNCH_CODE, AD_CMPNY);        	
        	Collection apSuppliers = apSupplierHome.findSplBySplNewAndUpdated(adBranch.getBrCode(), AD_CMPNY, 'N', 'N', 'N');        	
        	Collection arUpdatedSuppliers = apSupplierHome.findSplBySplNewAndUpdated(adBranch.getBrCode(), AD_CMPNY, 'U', 'U', 'X');
        	        	        	
        	String[] results = new String[apSuppliers.size() + arUpdatedSuppliers.size()];
        	Iterator i = apSuppliers.iterator();
        	int ctr = 0;
	        while (i.hasNext()) {
	        	LocalApSupplier apSupplier = (LocalApSupplier)i.next();
	        	results[ctr] = supplierRowEncode(apSupplier);
	        	ctr++;	        	
	        }
	        i = arUpdatedSuppliers.iterator();
	        while (i.hasNext()) {
	        	LocalApSupplier apSupplier = (LocalApSupplier)i.next();
	        	results[ctr] = supplierRowEncode(apSupplier);
	        	ctr++;	        	
	        }
	        
	        return results;
        	
        } catch (Exception ex) {
        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());
        	
        }
    }
    
    /**
     * @ejb:interface-method
     **/
    public int setApSupplierAllNewAndUpdatedSuccessConfirmation(String BR_BRNCH_CODE, Integer AD_CMPNY) {    	
    
    	Debug.print("ApSupplierSyncControllerBean setApSupplierAllNewAndUpdatedSuccessConfirmation");
    	
    	LocalAdBranchSupplierHome adBranchSupplierHome = null;
    	LocalAdBranchSupplier adBranchSupplier = null;
    	LocalAdBranchHome adBranchHome = null;
    	
    	// Initialize EJB Home
        
        try {
            
        	adBranchSupplierHome = (LocalAdBranchSupplierHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdBranchSupplierHome.JNDI_NAME, LocalAdBranchSupplierHome.class);
        	adBranchHome = (LocalAdBranchHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdBranchHome.JNDI_NAME, LocalAdBranchHome.class);

        	
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {
        	LocalAdBranch adBranch = adBranchHome.findByBrBranchCode(BR_BRNCH_CODE, AD_CMPNY);
        	
        	Collection adBranchSuppliers = adBranchSupplierHome.findSplBySplNewAndUpdated(adBranch.getBrCode(), AD_CMPNY, 'N', 'U', 'X');
        	
        	Iterator i = adBranchSuppliers.iterator();        	
	        while (i.hasNext()) {
	        	 
	        	adBranchSupplier = (LocalAdBranchSupplier)i.next();	        	
	        	adBranchSupplier.setBsplSupplierDownloadStatus('D');
	        	
	        }
        	
        } catch (Exception ex) {
        	
        	ctx.setRollbackOnly();
        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());
        	
        }
        
        return 0;
        
    }
    
    private String supplierRowEncode(LocalApSupplier apSupplier) {
    	
    	char separator = EJBCommon.SEPARATOR;
    	StringBuffer tempResult = new StringBuffer();
    	String encodedResult = new String();

    	// Start separator
    	tempResult.append(separator);

    	// Primary Key
    	tempResult.append(apSupplier.getSplCode());
    	tempResult.append(separator);
    	
    	// Suppplier Code
    	tempResult.append(apSupplier.getSplSupplierCode());
    	tempResult.append(separator);
    	
    	// Name
    	tempResult.append(apSupplier.getSplName());
    	tempResult.append(separator);
    	
    	// Tin
    	tempResult.append(apSupplier.getSplTin());
    	tempResult.append(separator);
    	
    	// Address
    	if (apSupplier.getSplAddress().length() < 1) {
    		
    		tempResult.append("not specified");
    		tempResult.append(separator);
    		
    	} else {
    		
    		tempResult.append(apSupplier.getSplAddress());
    		tempResult.append(separator);
    		
    	}    	
    	
    	// Bank Account Name
    	tempResult.append(apSupplier.getAdBankAccount().getBaName());
    	tempResult.append(separator);
    	
    	// Tax Code
    	tempResult.append(apSupplier.getApSupplierClass().getApTaxCode().getTcName());
    	tempResult.append(separator);  	
    	
    	// Withholding Tax Code
    	tempResult.append(apSupplier.getApSupplierClass().getApWithholdingTaxCode().getWtcName());
    	tempResult.append(separator);   	
    	
    	// remove unwanted chars from encodedResult;
    	encodedResult = tempResult.toString();    	
    	    	
    	encodedResult = encodedResult.replace("'", "\'");    	
    	
    	return encodedResult;
    	
    }
    
    public void ejbCreate() throws CreateException {

       Debug.print("ApSupplierEntryControllerBean ejbCreate");
      
    }
    
    public void ejbRemove() {};

    public void ejbActivate() {}
    public void ejbPassivate() {}

    public void setSessionContext(SessionContext ctx) {}

}