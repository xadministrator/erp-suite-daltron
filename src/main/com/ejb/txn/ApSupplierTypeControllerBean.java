package com.ejb.txn;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;
import javax.naming.NamingException;

import com.ejb.ad.LocalAdBankAccount;
import com.ejb.ad.LocalAdBankAccountHome;
import com.ejb.ap.LocalApSupplierType;
import com.ejb.ap.LocalApSupplierTypeHome;
import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.exception.GlobalRecordAlreadyAssignedException;
import com.ejb.exception.GlobalRecordAlreadyDeletedException;
import com.ejb.exception.GlobalRecordAlreadyExistException;
import com.util.AbstractSessionBean;
import com.util.ApModSupplierTypeDetails;
import com.util.Debug;
import com.util.EJBHomeFactory;

/**
 * @ejb:bean name="ApSupplierTypeControllerEJB"
 *           display-name="Used for entering supplier types"
 *           type="Stateless"
 *           view-type="remote"
 *           jndi-name="ejb/ApSupplierTypeControllerEJB"
 *
 * @ejb:interface remote-class="com.ejb.txn.ApSupplierTypeController"
 *                extends="javax.ejb.EJBObject"
 *
 * @ejb:home remote-class="com.ejb.txn.ApSupplierTypeControllerHome"
 *           extends="javax.ejb.EJBHome"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="apuser"
 *                        role-link="apuserlink"
 *
 * @ejb:permission role-name="apuser"
 * 
*/

public class ApSupplierTypeControllerBean extends AbstractSessionBean {

    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList getApStAll(Integer AD_CMPNY) 
        throws GlobalNoRecordFoundException {
                    
        Debug.print("ApSupplierTypeControllerBean getApStAll");

        LocalApSupplierTypeHome apSupplierTypeHome = null;
        LocalAdBankAccountHome adBankAccountHome = null;

        ArrayList list = new ArrayList();
        
        // Initialize EJB Home

        try {

            apSupplierTypeHome = (LocalApSupplierTypeHome)EJBHomeFactory.
                lookUpLocalHome(LocalApSupplierTypeHome.JNDI_NAME, LocalApSupplierTypeHome.class);
            adBankAccountHome = (LocalAdBankAccountHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdBankAccountHome.JNDI_NAME, LocalAdBankAccountHome.class);                

        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {
        	        	
	        Collection apSupplierTypes = apSupplierTypeHome.findStAll(AD_CMPNY);
	
	        if (apSupplierTypes.isEmpty()) {
	
	            throw new GlobalNoRecordFoundException();
	        	
	        }
	            
	        Iterator i = apSupplierTypes.iterator();
	               
	        while (i.hasNext()) {
	        	
	        	LocalApSupplierType apSupplierType = (LocalApSupplierType)i.next();
	        	
	        	ApModSupplierTypeDetails mdetails = new ApModSupplierTypeDetails();
	        		mdetails.setStCode(apSupplierType.getStCode());        		
	                mdetails.setStName(apSupplierType.getStName());                
	                mdetails.setStDescription(apSupplierType.getStDescription());
                    mdetails.setStEnable(apSupplierType.getStEnable());
                    mdetails.setStBaName(apSupplierType.getAdBankAccount().getBaName());                                        
	                	                	                
	        		list.add(mdetails);
	        		
	        }              
	                                                        		        		        
	        return list;
	        
        } catch (GlobalNoRecordFoundException ex) {
        	
        	throw ex;
        	        	
        } catch (Exception ex) {
        	
        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());
        	
        }
            
    }
    
    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList getAdBaAll(Integer AD_BRNCH, Integer AD_CMPNY) {
                    
        Debug.print("ApSupplierTypeControllerBean getAdBaAll");
        
        LocalAdBankAccountHome adBankAccountHome = null;
                
        LocalAdBankAccount adBankAccount = null;
        
        Collection adBankAccounts = null;
        
        ArrayList list = new ArrayList();
        
        // Initialize EJB Home
        
        try {
            
            adBankAccountHome = (LocalAdBankAccountHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdBankAccountHome.JNDI_NAME, LocalAdBankAccountHome.class);
            
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {
            
            adBankAccounts = adBankAccountHome.findEnabledBaAll(AD_BRNCH, AD_CMPNY);
            
        } catch (FinderException ex) {
            
        } catch (Exception ex) {
            
            throw new EJBException(ex.getMessage());
        }
        
        if (adBankAccounts.isEmpty()) {
        	
            return null;
        	
        }
        
        Iterator i = adBankAccounts.iterator();
               
        while (i.hasNext()) {
        	
            adBankAccount = (LocalAdBankAccount)i.next();
        	
            list.add(adBankAccount.getBaName());
        	
        }
        
        return list;
            
    }  
    
    /**
     * @ejb:interface-method view-type="remote"
     **/    
    public void addApStEntry(com.util.ApSupplierTypeDetails details, String ST_BA_NM, Integer AD_CMPNY) 
        throws GlobalRecordAlreadyExistException {
                    
        Debug.print("ApSupplierTypeControllerBean addApStEntry");
        
        LocalApSupplierTypeHome apSupplierTypeHome = null;
        LocalAdBankAccountHome adBankAccountHome = null;
            
        ArrayList list = new ArrayList();
        
        // Initialize EJB Home
        
        try {
            
            apSupplierTypeHome = (LocalApSupplierTypeHome)EJBHomeFactory.
                lookUpLocalHome(LocalApSupplierTypeHome.JNDI_NAME, LocalApSupplierTypeHome.class);
            adBankAccountHome = (LocalAdBankAccountHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdBankAccountHome.JNDI_NAME, LocalAdBankAccountHome.class);                

        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {
        	
            LocalApSupplierType apSupplierType = null;
			LocalAdBankAccount adBankAccount = null;
        
	        try { 
	            
	            apSupplierType = apSupplierTypeHome.findByStName(details.getStName(), AD_CMPNY);
	            
	            throw new GlobalRecordAlreadyExistException();
	            
	        } catch (FinderException ex) {
	         	
	        	 	
	        }
	         
	    	// create new supplier class
	    				    	        	        	    	
	    	apSupplierType = apSupplierTypeHome.create(details.getStName(),
	    	        details.getStDescription(), details.getStEnable(), AD_CMPNY);

			adBankAccount = adBankAccountHome.findByBaName(ST_BA_NM, AD_CMPNY);
			adBankAccount.addApSupplierType(apSupplierType);
	        
        } catch (GlobalRecordAlreadyExistException ex) {
        	
        	getSessionContext().setRollbackOnly();
        	throw ex;
        	
        } catch (Exception ex) {
        	
        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());
        	
        }
            
    }    
    
    /**
     * @ejb:interface-method view-type="remote"
     **/
    public void updateApStEntry(com.util.ApSupplierTypeDetails details, String ST_BA_NM, Integer AD_CMPNY) 
        throws GlobalRecordAlreadyExistException {
                    
        Debug.print("ApSupplierTypeControllerBean updateApTcEntry");
        
        LocalApSupplierTypeHome apSupplierTypeHome = null;
		LocalAdBankAccountHome adBankAccountHome = null;
        
        LocalApSupplierType apSupplierType = null;
        LocalAdBankAccount adBankAccount = null;
                
        ArrayList list = new ArrayList();
        
        // Initialize EJB Home
        
        try {

            apSupplierTypeHome = (LocalApSupplierTypeHome)EJBHomeFactory.
                lookUpLocalHome(LocalApSupplierTypeHome.JNDI_NAME, LocalApSupplierTypeHome.class);
            adBankAccountHome = (LocalAdBankAccountHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdBankAccountHome.JNDI_NAME, LocalAdBankAccountHome.class);                
                         
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {
        	
        	try {
        	                   
	            LocalApSupplierType apExistingSupplierType = apSupplierTypeHome.findByStName(details.getStName(), AD_CMPNY);
	            
	            if (!apExistingSupplierType.getStCode().equals(details.getStCode())) {
	            
	                 throw new GlobalRecordAlreadyExistException();
	                 
	            }
	            
	        } catch (FinderException ex) {
	        	
	        }
            
			// find and update supplier class

			apSupplierType = apSupplierTypeHome.findByPrimaryKey(details.getStCode());

				apSupplierType.setStName(details.getStName());
				apSupplierType.setStDescription(details.getStDescription());
				apSupplierType.setStEnable(details.getStEnable());
          										    		    		        			    		    		        
 			adBankAccount = adBankAccountHome.findByBaName(ST_BA_NM, AD_CMPNY);
 			adBankAccount.addApSupplierType(apSupplierType);
		    		
        } catch (GlobalRecordAlreadyExistException ex) {
        	
        	getSessionContext().setRollbackOnly();
        	throw ex;
        	
        } catch (Exception ex) {
        	
        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());
        	
        }
            
    }    
    
   /**
    * @ejb:interface-method view-type="remote"
    **/
    public void deleteApStEntry(Integer ST_CODE, Integer AD_CMPNY) 
        throws GlobalRecordAlreadyDeletedException,
                GlobalRecordAlreadyAssignedException {

        Debug.print("ApSupplierTypeControllerBean deleteApStEntry");
      
        LocalApSupplierTypeHome apSupplierTypeHome = null;

        // Initialize EJB Home
        
        try {

            apSupplierTypeHome = (LocalApSupplierTypeHome)EJBHomeFactory.
                lookUpLocalHome(LocalApSupplierTypeHome.JNDI_NAME, LocalApSupplierTypeHome.class);           
      
        } catch (NamingException ex) {

            throw new EJBException(ex.getMessage());

        }
      
        try {
      	
      	    LocalApSupplierType apSupplierType = null;                
      
	        try {
	      	
	            apSupplierType = apSupplierTypeHome.findByPrimaryKey(ST_CODE);
	         
	        } catch (FinderException ex) {
	      	
	            throw new GlobalRecordAlreadyDeletedException();
	            
	        }
	         
	        if (!apSupplierType.getApSuppliers().isEmpty()) {
	  
	            throw new GlobalRecordAlreadyAssignedException();
	         
	        }
	                            	
		    apSupplierType.remove();
	    
        } catch (GlobalRecordAlreadyDeletedException ex) {
        	
        	getSessionContext().setRollbackOnly();
        	throw ex;
        	
        } catch (GlobalRecordAlreadyAssignedException ex) {
        	
        	getSessionContext().setRollbackOnly();
        	throw ex;
        	        	
        } catch (Exception ex) {
        	
        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());
        	
        }
            
    }         
   
    // SessionBean methods

    /**
     * @ejb:create-method view-type="remote"
     **/
    public void ejbCreate() throws CreateException {

        Debug.print("ApSupplierTypeControllerBean ejbCreate");
      
    }
    
}

