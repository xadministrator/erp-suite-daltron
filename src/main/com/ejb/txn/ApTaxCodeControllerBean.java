package com.ejb.txn;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;
import javax.naming.NamingException;

import com.ejb.ad.LocalAdBranch;
import com.ejb.ad.LocalAdBranchApTaxCode;
import com.ejb.ad.LocalAdBranchApTaxCodeHome;
import com.ejb.ad.LocalAdBranchHome;
import com.ejb.ad.LocalAdBranchResponsibility;
import com.ejb.ad.LocalAdBranchResponsibilityHome;
import com.ejb.ad.LocalAdResponsibility;
import com.ejb.ad.LocalAdResponsibilityHome;
import com.ejb.ap.LocalApTaxCode;
import com.ejb.ap.LocalApTaxCodeHome;
import com.ejb.exception.ArTCCoaGlTaxCodeAccountNotFoundException;
import com.ejb.exception.GlobalAccountNumberInvalidException;
import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.exception.GlobalRecordAlreadyAssignedException;
import com.ejb.exception.GlobalRecordAlreadyDeletedException;
import com.ejb.exception.GlobalRecordAlreadyExistException;
import com.ejb.gl.LocalGlChartOfAccount;
import com.ejb.gl.LocalGlChartOfAccountHome;
import com.util.AbstractSessionBean;
import com.util.AdBranchDetails;
import com.util.AdModBranchApTaxCodeDetails;
import com.util.AdModBranchArTaxCodeDetails;
import com.util.AdResponsibilityDetails;
import com.util.ApModTaxCodeDetails;
import com.util.Debug;
import com.util.EJBHomeFactory;

/**
 * @ejb:bean name="ApTaxCodeControllerEJB"
 *           display-name="Used for entering tax codes"
 *           type="Stateless"
 *           view-type="remote"
 *           jndi-name="ejb/ApTaxCodeControllerEJB"
 *
 * @ejb:interface remote-class="com.ejb.txn.ApTaxCodeController"
 *                extends="javax.ejb.EJBObject"
 *
 * @ejb:home remote-class="com.ejb.txn.ApTaxCodeControllerHome"
 *           extends="javax.ejb.EJBHome"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="apuser"
 *                        role-link="apuserlink"
 *
 * @ejb:permission role-name="apuser"
 * 
*/

public class ApTaxCodeControllerBean extends AbstractSessionBean {
	
	
	/**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public AdResponsibilityDetails getAdRsByRsCode(Integer RS_CODE) 
    	throws GlobalNoRecordFoundException {
    	
    	Debug.print("ApTaxCodeControllerBean getAdRsByRsCode");
    	
    	LocalAdResponsibilityHome adResHome = null;
    	LocalAdResponsibility adRes = null;
    	
    	try {
    		adResHome = (LocalAdResponsibilityHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdResponsibilityHome.JNDI_NAME, LocalAdResponsibilityHome.class);
    	} catch (NamingException ex) {
    		
    	}
    	
    	try {
        	adRes = adResHome.findByPrimaryKey(RS_CODE);    		
    	} catch (FinderException ex) {
    		
    	}
    	
    	AdResponsibilityDetails details = new AdResponsibilityDetails();
    	details.setRsName(adRes.getRsName());
    	
    	return details;
    }
    
    
    
    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList getAdBrTcAll(Integer BTC_CODE, String RS_NM, Integer AD_CMPNY) 
    	throws GlobalNoRecordFoundException{
    	
    	Debug.print("ApTaxCodeControllerBean getAdBrSMLAll");

    	
    	LocalAdBranchApTaxCodeHome adBranchApTaxCodeHome = null;
    	LocalAdBranchHome adBranchHome = null;
    	LocalGlChartOfAccountHome glChartOfAccountHome = null;
    	
    	
    	LocalAdBranchApTaxCode adBranchApTaxCode = null;
    	LocalAdBranch adBranch = null;
    	LocalGlChartOfAccount glChartOfAccount = null;
    	
    	Collection adBranchApTaxCodes = null;
    	
        ArrayList branchList = new ArrayList();
        
        // Initialize EJB Home

        try {
            
        	adBranchApTaxCodeHome = (LocalAdBranchApTaxCodeHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdBranchApTaxCodeHome.JNDI_NAME, LocalAdBranchApTaxCodeHome.class);
            adBranchHome = (LocalAdBranchHome)EJBHomeFactory.
            	lookUpLocalHome(LocalAdBranchHome.JNDI_NAME, LocalAdBranchHome.class);
            glChartOfAccountHome = (LocalGlChartOfAccountHome)EJBHomeFactory.
				lookUpLocalHome(LocalGlChartOfAccountHome.JNDI_NAME, LocalGlChartOfAccountHome.class);

        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {
        	
        	adBranchApTaxCodes = adBranchApTaxCodeHome.findBBTCByTcCodeAndRsName(BTC_CODE, RS_NM, AD_CMPNY);
		
        } catch (FinderException ex) {
            
        } catch (Exception ex) {
            
            throw new EJBException(ex.getMessage());
        }
        
        if (adBranchApTaxCodes == null || adBranchApTaxCodes.isEmpty()) {
        	
            throw new GlobalNoRecordFoundException();
        	
        }
        
        try {
        
	        Iterator i = adBranchApTaxCodes.iterator();
	        
	        while(i.hasNext()) {
	        	
	        	adBranchApTaxCode = (LocalAdBranchApTaxCode)i.next();

	        	adBranch = adBranchHome.findByPrimaryKey(adBranchApTaxCode.getAdBranch().getBrCode());
	        	
	        	AdModBranchApTaxCodeDetails mdetails = new AdModBranchApTaxCodeDetails();
	        	
	        	mdetails.setBtcBranchCode(adBranch.getBrCode());
	        	mdetails.setBtcBranchName(adBranch.getBrName());
	        	
	        	glChartOfAccount = glChartOfAccountHome.findByPrimaryKey(adBranchApTaxCode.getBtcGlCoaTaxCode());
	        	mdetails.setBtcTaxCodeAccountNumber(glChartOfAccount.getCoaAccountNumber());
	        	mdetails.setBtcTaxCodeAccountDescription(glChartOfAccount.getCoaAccountDescription());
	        	
		    	branchList.add(mdetails);
	        }
	        
        } catch (FinderException ex) {

        } catch (Exception ex) {
            
            throw new EJBException(ex.getMessage());
        }
        
        return branchList;
    	
    }

    
    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/   
    public ArrayList getAdBrResAll(Integer RS_CODE, Integer AD_CMPNY) 
    	throws GlobalNoRecordFoundException{
        
        Debug.print("ApTaxCodeControllerBean getAdBrResAll");
        
        LocalAdBranchResponsibilityHome adBranchResponsibilityHome = null;
        LocalAdBranchHome adBranchHome = null;
        
        LocalAdBranchResponsibility adBranchResponsibility = null;
        LocalAdBranch adBranch = null;
        
        Collection adBranchResponsibilities = null;
        
        ArrayList list = new ArrayList();
        
        // Initialize EJB Home
        
        try {
            
            adBranchResponsibilityHome = (LocalAdBranchResponsibilityHome)EJBHomeFactory.
            lookUpLocalHome(LocalAdBranchResponsibilityHome.JNDI_NAME, LocalAdBranchResponsibilityHome.class);
            adBranchHome = (LocalAdBranchHome)EJBHomeFactory.
            lookUpLocalHome(LocalAdBranchHome.JNDI_NAME, LocalAdBranchHome.class);
            
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {
            
            adBranchResponsibilities = adBranchResponsibilityHome.findByAdResponsibility(RS_CODE, AD_CMPNY);
            
        } catch (FinderException ex) {
            
        } catch (Exception ex) {
            
            throw new EJBException(ex.getMessage());
        }
        
        if (adBranchResponsibilities.isEmpty()) {
            
            throw new GlobalNoRecordFoundException();
            
        }
        
        try {
            
            Iterator i = adBranchResponsibilities.iterator();
            
            while(i.hasNext()) {
                
                adBranchResponsibility = (LocalAdBranchResponsibility)i.next();
                
                adBranch = adBranchResponsibility.getAdBranch();
                
                AdBranchDetails details = new AdBranchDetails();
                
                details.setBrCode(adBranch.getBrCode());
                details.setBrName(adBranch.getBrName());
                
                list.add(details);
                
            }	               
            
        } catch (Exception ex) {
            
            throw new EJBException(ex.getMessage());
        }
        
        return list;
        
    }
    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList getApTcAll(Integer AD_CMPNY) 
        throws GlobalNoRecordFoundException {
                    
        Debug.print("ApTaxCodeControllerBean getApTcAll");

        LocalApTaxCodeHome apTaxCodeHome = null;

        ArrayList list = new ArrayList();
        
        // Initialize EJB Home

        try {

            apTaxCodeHome = (LocalApTaxCodeHome)EJBHomeFactory.
                lookUpLocalHome(LocalApTaxCodeHome.JNDI_NAME, LocalApTaxCodeHome.class);

        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {
        	
	        Collection apTaxCodes = apTaxCodeHome.findTcAll(AD_CMPNY);
	
	        if (apTaxCodes.isEmpty()) {
	
	            throw new GlobalNoRecordFoundException();
	        	
	        }
	            
	        Iterator i = apTaxCodes.iterator();
	               
	        while (i.hasNext()) {
	        	
	        	LocalApTaxCode apTaxCode = (LocalApTaxCode)i.next();
	                                                                        
	        	ApModTaxCodeDetails mdetails = new ApModTaxCodeDetails();
	        		mdetails.setTcCode(apTaxCode.getTcCode());        		
	                mdetails.setTcName(apTaxCode.getTcName());                
	                mdetails.setTcDescription(apTaxCode.getTcDescription());
	                mdetails.setTcType(apTaxCode.getTcType());
	                mdetails.setTcRate(apTaxCode.getTcRate());
	                mdetails.setTcEnable(apTaxCode.getTcEnable());
	                
	                if (apTaxCode.getGlChartOfAccount() != null) {
	                	
	                	mdetails.setTcCoaGlTaxAccountNumber(apTaxCode.getGlChartOfAccount().getCoaAccountNumber());	                		                	    
					    mdetails.setTcCoaGlTaxDescription(apTaxCode.getGlChartOfAccount().getCoaAccountDescription());
					    
                    }					    		    	                          
				    
	        		list.add(mdetails);
	        		
	        }              
	                                                        		        		        
	        return list;
	        
        } catch (GlobalNoRecordFoundException ex) {
        	
        	throw ex;
        	        	
        } catch (Exception ex) {
        	
        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());
        	
        }
            
    }    
    
    /**
     * @ejb:interface-method view-type="remote"
     **/    
    public void addApTcEntry(com.util.ApTaxCodeDetails details, String TC_COA_ACCNT_NMBR, Integer AD_CMPNY) 
        throws GlobalRecordAlreadyExistException,
               GlobalAccountNumberInvalidException {
                    
        Debug.print("ApTaxCodeControllerBean addApTcEntry");
        
        LocalApTaxCodeHome apTaxCodeHome = null;
        LocalGlChartOfAccountHome glChartOfAccountHome = null;

        ArrayList list = new ArrayList();
        
        // Initialize EJB Home
        
        try {
            
            apTaxCodeHome = (LocalApTaxCodeHome)EJBHomeFactory.
                lookUpLocalHome(LocalApTaxCodeHome.JNDI_NAME, LocalApTaxCodeHome.class);
            glChartOfAccountHome = (LocalGlChartOfAccountHome)EJBHomeFactory.
                lookUpLocalHome(LocalGlChartOfAccountHome.JNDI_NAME, LocalGlChartOfAccountHome.class);
                                                      
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {
        	
            LocalApTaxCode apTaxCode = null;	
            LocalGlChartOfAccount glChartOfAccount = null;
        
	        try { 
	            
	            apTaxCode = apTaxCodeHome.findByTcName(details.getTcName(), AD_CMPNY);
	            
	            throw new GlobalRecordAlreadyExistException();
	            
	         } catch (FinderException ex) {
	         	
	        	 	
	         }
	                            
	        // get glChartOfAccount to validate accounts 
	        
	        try {
	        	
	        	if (TC_COA_ACCNT_NMBR != null && TC_COA_ACCNT_NMBR.length() > 0) {
	       
		            glChartOfAccount = glChartOfAccountHome.findByCoaAccountNumber(
		               TC_COA_ACCNT_NMBR, AD_CMPNY);
		               
                }		               
	               
	        } catch (FinderException ex) {
	        
	            throw new GlobalAccountNumberInvalidException();
	                       
	        }
	        
	    	// create new tax code
	    	
	    	apTaxCode = apTaxCodeHome.create(details.getTcName(),
	    	        details.getTcDescription(), details.getTcType(), details.getTcRate(),
	    	        details.getTcEnable(), AD_CMPNY);
	    	        
	    	if (TC_COA_ACCNT_NMBR != null && TC_COA_ACCNT_NMBR.length() > 0) {
	    		        
	    		glChartOfAccount.addApTaxCode(apTaxCode);
	    		
	        }
	        	        
        } catch (GlobalRecordAlreadyExistException ex) {
        	
        	getSessionContext().setRollbackOnly();
        	throw ex;
        	
        } catch (GlobalAccountNumberInvalidException ex) {
        	
        	getSessionContext().setRollbackOnly();
        	throw ex;
        	        	
        } catch (Exception ex) {
        	
        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());
        	
        }
            
    }    
    
    /**
     * @ejb:interface-method view-type="remote"
     **/
    public void updateApTcEntry(
    		com.util.ApTaxCodeDetails details, 
    		String TC_COA_ACCNT_NMBR, 
    		String RS_NM, ArrayList branchList,Integer AD_CMPNY)
        throws GlobalRecordAlreadyExistException, 
               GlobalAccountNumberInvalidException,
               GlobalRecordAlreadyAssignedException{
                    
        Debug.print("ApTaxCodeControllerBean updateApTcEntry");
        
        LocalApTaxCodeHome apTaxCodeHome = null;                
        LocalGlChartOfAccountHome glChartOfAccountHome = null;                               
              
        LocalAdBranchApTaxCodeHome adBranchApTaxCodeHome = null;
        LocalAdBranchHome adBranchHome = null;
        
        LocalAdBranch adBranch = null;
        LocalAdBranchApTaxCode adBranchApTaxCode = null;
        
        ArrayList list = new ArrayList();
        
        // Initialize EJB Home
        
        try {

            apTaxCodeHome = (LocalApTaxCodeHome)EJBHomeFactory.
                lookUpLocalHome(LocalApTaxCodeHome.JNDI_NAME, LocalApTaxCodeHome.class);           
            glChartOfAccountHome = (LocalGlChartOfAccountHome)EJBHomeFactory.
                lookUpLocalHome(LocalGlChartOfAccountHome.JNDI_NAME, LocalGlChartOfAccountHome.class);
            adBranchApTaxCodeHome = (LocalAdBranchApTaxCodeHome)EJBHomeFactory.
                    lookUpLocalHome(LocalAdBranchApTaxCodeHome.JNDI_NAME, LocalAdBranchApTaxCodeHome.class);
            adBranchHome = (LocalAdBranchHome)EJBHomeFactory.
            		lookUpLocalHome(LocalAdBranchHome.JNDI_NAME, LocalAdBranchHome.class);
             
                         
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {
        	
        	LocalApTaxCode apTaxCode = null;
        	LocalApTaxCode apExistingTaxCode = null;
        	LocalGlChartOfAccount glChartOfAccount = null;
        	
        	try {
                   
	            apExistingTaxCode = apTaxCodeHome.findByTcName(details.getTcName(), AD_CMPNY);
	            
	            if (!apExistingTaxCode.getTcCode().equals(details.getTcCode())) {
	            
	                 throw new GlobalRecordAlreadyExistException();
	                 
	            }
                 
            } catch (FinderException ex) {
            	
            }
            	                                             
	        // get glChartOfAccount to validate accounts 
	        
	        try {
	       
	        	if (TC_COA_ACCNT_NMBR != null && TC_COA_ACCNT_NMBR.length() > 0) {
	       
		            glChartOfAccount = glChartOfAccountHome.findByCoaAccountNumber(
		               TC_COA_ACCNT_NMBR, AD_CMPNY);
		               
                }		               
	               
	        } catch (FinderException ex) {
	        
	            throw new GlobalAccountNumberInvalidException();
	            
	        }
	
			// find and update tax code
			
			apTaxCode = apTaxCodeHome.findByPrimaryKey(details.getTcCode());
	        
	        try {
	        	
	        	if ((!apTaxCode.getApVouchers().isEmpty() ||
	        		!apTaxCode.getApChecks().isEmpty() ||
					!apTaxCode.getApRecurringVouchers().isEmpty())&&(details.getTcRate() != apTaxCode.getTcRate())) {
	        		
	        		throw new GlobalRecordAlreadyAssignedException();
	        		
	        	}        	
	        	
	        } catch (GlobalRecordAlreadyAssignedException ex){
	        	
	        	throw ex;
	        	
	        }
				
			    apTaxCode.setTcName(details.getTcName());
			    apTaxCode.setTcDescription(details.getTcDescription());
			    apTaxCode.setTcType(details.getTcType());
			    apTaxCode.setTcRate(details.getTcRate());
			    apTaxCode.setTcEnable(details.getTcEnable());
			    
			    if (apTaxCode.getGlChartOfAccount() != null) {
			    	
			    	apTaxCode.getGlChartOfAccount().dropApTaxCode(apTaxCode);
			    	
			    }
			    
		    	if (TC_COA_ACCNT_NMBR != null && TC_COA_ACCNT_NMBR.length() > 0) {
		    		        
		    		glChartOfAccount.addApTaxCode(apTaxCode);
		    		
		        }		
		    	
		    	
		    	Collection adBranchApTaxCodes = adBranchApTaxCodeHome.findBBTCByTcCodeAndRsName(apTaxCode.getTcCode(), RS_NM, AD_CMPNY);
				
		        
		  	      Iterator i = adBranchApTaxCodes.iterator();
		  	      
		  	      //remove all adBranchDSA lines
		  	      while(i.hasNext()) {
		  	      	
		  	    	  
		  	    	adBranchApTaxCode = (LocalAdBranchApTaxCode) i.next();
		  	      	
		  	    	apTaxCode.dropAdBranchApTaxCode(adBranchApTaxCode);
		  	    	
		  	      	adBranch = adBranchHome.findByPrimaryKey(adBranchApTaxCode.getAdBranch().getBrCode());
		  	      	adBranch.dropAdBranchApTaxCode(adBranchApTaxCode);
		  	      	adBranchApTaxCode.remove();
		  	      }
		    	
		    	
		    	
		    	
		    	Iterator x = branchList.iterator();
		  	      System.out.println("branchList="+branchList.size());
		  	    System.out.println("--------------------->5");
		        
		  	      while(x.hasNext()) {	          	          
		  
		  	    	
		  	    	AdModBranchApTaxCodeDetails brTcDetails = (AdModBranchApTaxCodeDetails)x.next();
		  	    	
		  	    	LocalGlChartOfAccount glTaxCodeCOA = null;
		  	    	LocalGlChartOfAccount glBankCOA = null;
		  	    	

		  	    	if(brTcDetails.getBtcTaxCodeAccountNumber() != null && brTcDetails.getBtcTaxCodeAccountNumber().length() > 0){
		  	    		
		  	    		try {
		  	  		       
		  	    			glTaxCodeCOA = glChartOfAccountHome.findByCoaAccountNumber(
		  	    					brTcDetails.getBtcTaxCodeAccountNumber(), AD_CMPNY);
		  		               
		  		        } catch (FinderException ex) {
		  		        
		  		            throw new ArTCCoaGlTaxCodeAccountNotFoundException();
		  		            
		  		        }
		  	    		
		  	    	}

		  	    	

		  	    	adBranchApTaxCode = adBranchApTaxCodeHome.create(
		  	    			glTaxCodeCOA != null ? glTaxCodeCOA.getCoaCode() : null, 'N',AD_CMPNY);

		  	        apTaxCode.addAdBranchApTaxCode(adBranchApTaxCode);
		  	        adBranch = adBranchHome.findByPrimaryKey(brTcDetails.getBtcBranchCode());
		  	        adBranch.addAdBranchApTaxCode(adBranchApTaxCode);
		  	          
		  	      }
	                
        } catch (GlobalRecordAlreadyExistException ex) {
        	
        	getSessionContext().setRollbackOnly();
        	throw ex;
        	
        } catch (GlobalAccountNumberInvalidException ex) {
        	
        	getSessionContext().setRollbackOnly();
        	throw ex;
        	
        } catch (GlobalRecordAlreadyAssignedException ex) {
        	
        	getSessionContext().setRollbackOnly();
        	throw ex;
        	                	        	
        } catch (Exception ex) {
        	
        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());
        	
        }
            
    }    
    
   /**
    * @ejb:interface-method view-type="remote"
    **/
    public void deleteApTcEntry(Integer TC_CODE, Integer AD_CMPNY) 
        throws GlobalRecordAlreadyDeletedException,
                GlobalRecordAlreadyAssignedException {

        Debug.print("ApTaxCodeControllerBean deleteApTcEntry");
      
        LocalApTaxCodeHome apTaxCodeHome = null;

        // Initialize EJB Home
        
        try {

            apTaxCodeHome = (LocalApTaxCodeHome)EJBHomeFactory.
                lookUpLocalHome(LocalApTaxCodeHome.JNDI_NAME, LocalApTaxCodeHome.class);           
      
        } catch (NamingException ex) {

            throw new EJBException(ex.getMessage());

        }
      
        try {
      	
      	    LocalApTaxCode apTaxCode = null;                
      
	        try {
	      	
	            apTaxCode = apTaxCodeHome.findByPrimaryKey(TC_CODE);
	         
	        } catch (FinderException ex) {
	      	
	            throw new GlobalRecordAlreadyDeletedException();
	            
	        }
	         
	        if (!apTaxCode.getApSupplierClasses().isEmpty() || 
	        	!apTaxCode.getApVouchers().isEmpty() || 
				!apTaxCode.getApChecks().isEmpty() || 
				!apTaxCode.getApRecurringVouchers().isEmpty()||
				!apTaxCode.getApPurchaseOrders().isEmpty()||
				!apTaxCode.getApPurchaseRequisitions().isEmpty()) {
	            
	            throw new GlobalRecordAlreadyAssignedException();
	            
	        }
	                            	
		    apTaxCode.remove();
	    
        } catch (GlobalRecordAlreadyDeletedException ex) {
        	
        	getSessionContext().setRollbackOnly();
        	throw ex;
        	
        } catch (GlobalRecordAlreadyAssignedException ex) {
        	
        	getSessionContext().setRollbackOnly();
        	throw ex;
        	        	
        } catch (Exception ex) {
        	
        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());
        	
        }
            
    }         
   
    // SessionBean methods

    /**
     * @ejb:create-method view-type="remote"
     **/
    public void ejbCreate() throws CreateException {

        Debug.print("ApTaxCodeControllerBean ejbCreate");
      
    }
    
}

