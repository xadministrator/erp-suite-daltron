
/*
 * ApVoucherBatchCopyControllerBean.java
 *
 * Created on May 26, 2004, 9:13 AM
 *
 * @author  Neil Andrew M. Ajero
 */

package com.ejb.txn;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.naming.NamingException;

import com.ejb.ad.LocalAdCompany;
import com.ejb.ad.LocalAdCompanyHome;
import com.ejb.ap.LocalApVoucher;
import com.ejb.ap.LocalApVoucherBatch;
import com.ejb.ap.LocalApVoucherBatchHome;
import com.ejb.ap.LocalApVoucherHome;
import com.ejb.exception.GlobalBatchCopyInvalidException;
import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.exception.GlobalTransactionBatchCloseException;
import com.util.AbstractSessionBean;
import com.util.ApModVoucherDetails;
import com.util.Debug;
import com.util.EJBCommon;
import com.util.EJBHomeFactory;

/**
 * @ejb:bean name="ApVoucherBatchCopyControllerEJB"
 *           display-name="Used for copying voucher batch"
 *           type="Stateless"
 *           view-type="remote"
 *           jndi-name="ejb/ApVoucherBatchCopyControllerEJB"
 *
 * @ejb:interface remote-class="com.ejb.txn.ApVoucherBatchCopyController"
 *                extends="javax.ejb.EJBObject"
 *
 * @ejb:home remote-class="com.ejb.txn.ApVoucherBatchCopyControllerHome"
 *           extends="javax.ejb.EJBHome"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="apuser"
 *                        role-link="apuserlink"
 *
 * @ejb:permission role-name="apuser"
 * 
*/

public class ApVoucherBatchCopyControllerBean extends AbstractSessionBean {

    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList getApOpenVbAll(Integer AD_BRNCH, Integer AD_CMPNY) {
                    
        Debug.print("ApVoucherBatchCopyControllerBean getApOpenVbAll");
        
        LocalApVoucherBatchHome apVoucherBatchHome = null;               
        
        ArrayList list = new ArrayList();
        
        // Initialize EJB Home
        
        try {
            
        	apVoucherBatchHome = (LocalApVoucherBatchHome)EJBHomeFactory.
                lookUpLocalHome(LocalApVoucherBatchHome.JNDI_NAME, LocalApVoucherBatchHome.class);
            
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }        
        
        try {
        	
        	Collection apVoucherBatches = apVoucherBatchHome.findOpenVbAll(AD_BRNCH, AD_CMPNY);
        	
        	Iterator i = apVoucherBatches.iterator();
        	
        	while (i.hasNext()) {
        		
        		LocalApVoucherBatch apVoucherBatch = (LocalApVoucherBatch)i.next();
        		
        		list.add(apVoucherBatch.getVbName());
        		
        	}
        	
        	return list;
        	
        } catch (Exception ex) {
        	
        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());
        	
        }
            
    }  
    
    /**
     * @ejb:interface-method view-type="remote"
     **/
    public void executeApVouBatchCopy(ArrayList list, String VB_NM_TO, Integer AD_BRNCH, Integer AD_CMPNY) throws
        GlobalTransactionBatchCloseException,
		GlobalBatchCopyInvalidException {
                    
        Debug.print("ApVoucherBatchCopyControllerBean executeApVouBatchCopy");
        
        LocalApVoucherHome apVoucherHome = null;
        LocalApVoucherBatchHome apVoucherBatchHome = null; 

        // Initialize EJB Home
        
        try {
            
        	apVoucherHome = (LocalApVoucherHome)EJBHomeFactory.
			    lookUpLocalHome(LocalApVoucherHome.JNDI_NAME, LocalApVoucherHome.class);
        	apVoucherBatchHome = (LocalApVoucherBatchHome)EJBHomeFactory.
                lookUpLocalHome(LocalApVoucherBatchHome.JNDI_NAME, LocalApVoucherBatchHome.class);
                         
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }

        try {

        	// find voucher batch to
        	
        	LocalApVoucherBatch apVoucherBatchTo = apVoucherBatchHome.findByVbName(VB_NM_TO, AD_BRNCH, AD_CMPNY);

        	// validate if batch to is closed
        	
        	if(apVoucherBatchTo.getVbStatus().equals("CLOSED")) {
        		
        		throw new GlobalTransactionBatchCloseException();
        		
        	}
        	
        	Iterator i = list.iterator();
        	
        	while (i.hasNext()) {
        		
        		LocalApVoucher apVoucher = apVoucherHome.findByPrimaryKey((Integer)i.next());
        		
        		if (!apVoucher.getApVoucherBatch().getVbType().equals(apVoucherBatchTo.getVbType())) {
        			
        			throw new GlobalBatchCopyInvalidException();
        			
        		}
        		
        		apVoucher.getApVoucherBatch().dropApVoucher(apVoucher);
        		apVoucherBatchTo.addApVoucher(apVoucher);

        	}

        } catch (GlobalTransactionBatchCloseException ex) {
       	   
       	   getSessionContext().setRollbackOnly();    	
       	   throw ex;
       	   
        } catch (GlobalBatchCopyInvalidException ex) {
        	   
    	   getSessionContext().setRollbackOnly();    	
    	   throw ex;
                                    
        } catch (Exception ex) {
       	 
       	   Debug.printStackTrace(ex);
       	   getSessionContext().setRollbackOnly();
           throw new EJBException(ex.getMessage());
         
        }

    }
    
    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList getApVouByVbName(String VB_NM, Integer AD_CMPNY) 
        throws GlobalNoRecordFoundException {
                    
        Debug.print("ApVoucherBatchCopyControllerBean getApVouByVbName");
        
        LocalApVoucherHome apVoucherHome = null;
        
        ArrayList list = new ArrayList();
        
        //initialized EJB Home
        
        try {
            
            apVoucherHome = (LocalApVoucherHome)EJBHomeFactory.
                lookUpLocalHome(LocalApVoucherHome.JNDI_NAME, LocalApVoucherHome.class);
            
        } catch (NamingException ex) 	{
            
            throw new EJBException(ex.getMessage());
            
        }
        
		try { 
           
		  Collection apVouchers = apVoucherHome.findByVbName(VB_NM, AD_CMPNY); 
		  	
		  if (apVouchers.size() == 0)
		     throw new GlobalNoRecordFoundException();
		     
		  Iterator i = apVouchers.iterator();
		
		  while (i.hasNext()) {
		  	
		  	  LocalApVoucher apVoucher = (LocalApVoucher)i.next();   	  
		  	  
		  	  ApModVoucherDetails mdetails = new ApModVoucherDetails();
		  	  mdetails.setVouCode(apVoucher.getVouCode());
		  	  mdetails.setVouSplSupplierCode(apVoucher.getApSupplier().getSplSupplierCode());
		  	  mdetails.setVouDate(apVoucher.getVouDate());
		  	  mdetails.setVouDocumentNumber(apVoucher.getVouDocumentNumber());
		  	  mdetails.setVouReferenceNumber(apVoucher.getVouReferenceNumber());
		  	  
		  	  if (apVoucher.getVouDebitMemo() == EJBCommon.FALSE) {
		  	  	
		  	  	  mdetails.setVouAmountDue(apVoucher.getVouAmountDue());
		  	  	
		  	  } else {
		  	  	
		  	  	  mdetails.setVouAmountDue(apVoucher.getVouBillAmount());
		  	  	
		  	  }

		  	  mdetails.setVouAmountPaid(apVoucher.getVouAmountPaid());
		  	  mdetails.setVouDebitMemo(apVoucher.getVouDebitMemo());      	  
			      	  	      	  		     
			  list.add(mdetails);
			         
		  }
			     
		  return list;
		      
	  } catch (GlobalNoRecordFoundException ex) {
	  	 
	  	  throw ex;
	  	
	  } catch (Exception ex) {
	  	

	  	  ex.printStackTrace();
	  	  throw new EJBException(ex.getMessage());
	  	
	  }
        
    }
    
    
    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
     public short getGlFcPrecisionUnit(Integer AD_CMPNY) {

        Debug.print("ApVoucherBatchCopyControllerBean getGlFcPrecisionUnit");

       
        LocalAdCompanyHome adCompanyHome = null;
       
      
        // Initialize EJB Home
         
        try {
             
            adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
               lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);       
             
        } catch (NamingException ex) {
             
            throw new EJBException(ex.getMessage());
             
        }

        try {
        	
          LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
             
          return  adCompany.getGlFunctionalCurrency().getFcPrecision();
                                      
        } catch (Exception ex) {
        	 
        	 Debug.printStackTrace(ex);
          throw new EJBException(ex.getMessage());
          
        }

     }


    // SessionBean methods

    /**
     * @ejb:create-method view-type="remote"
     **/
    public void ejbCreate() throws CreateException {

       Debug.print("ApVoucherBatchCopyControllerBean ejbCreate");
      
    }
}
