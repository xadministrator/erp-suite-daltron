package com.ejb.txn;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;
import javax.naming.NamingException;

import com.ejb.ad.LocalAdLookUpValue;
import com.ejb.ad.LocalAdLookUpValueHome;
import com.ejb.ap.LocalApSupplierType;
import com.ejb.ap.LocalApSupplierTypeHome;
import com.ejb.ap.LocalApVoucherBatch;
import com.ejb.ap.LocalApVoucherBatchHome;
import com.ejb.ap.LocalApVoucherHome;
import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.exception.GlobalRecordAlreadyAssignedException;
import com.ejb.exception.GlobalRecordAlreadyDeletedException;
import com.ejb.exception.GlobalRecordAlreadyExistException;
import com.ejb.exception.GlobalTransactionBatchCloseException;
import com.util.AbstractSessionBean;
import com.util.AdBranchSupplierDetails;
import com.util.ApVoucherBatchDetails;
import com.util.Debug;
import com.util.EJBCommon;
import com.util.EJBHomeFactory;

/**
 * @ejb:bean name="ApVoucherBatchEntryControllerEJB"
 *           display-name="used for entering journal batches"
 *           type="Stateless"
 *           view-type="remote"
 *           jndi-name="ejb/ApVoucherBatchEntryControllerEJB"
 *
 * @ejb:interface remote-class="com.ejb.txn.ApVoucherBatchEntryController"
 *                extends="javax.ejb.EJBObject"
 *
 * @ejb:home remote-class="com.ejb.txn.ApVoucherBatchEntryControllerHome"
 *           extends="javax.ejb.EJBHome"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="apuser"
 *                        role-link="apuserlink"
 *
 * @ejb:permission role-name="apuser"
 * 
*/

public class ApVoucherBatchEntryControllerBean extends AbstractSessionBean {

    
	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public ArrayList getAdLvDEPARTMENT(Integer AD_CMPNY) {
		
		Debug.print("ApVoucherBatchEntryControllerBean getAdLvDEPARTMENT");
		
		LocalAdLookUpValueHome adLookUpValueHome = null;               
		
		ArrayList list = new ArrayList();
		
		// Initialize EJB Home
		
		try {
			
			adLookUpValueHome = (LocalAdLookUpValueHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdLookUpValueHome.JNDI_NAME, LocalAdLookUpValueHome.class);
			
		} catch (NamingException ex) {
			
			throw new EJBException(ex.getMessage());
			
		}        
		
		try {
			
			Collection adLookUpValues = adLookUpValueHome.findByLuName("AD DEPARTMENT", AD_CMPNY);
			
			Iterator i = adLookUpValues.iterator();
			
			while (i.hasNext()) {
				
				LocalAdLookUpValue adLookUpValue = (LocalAdLookUpValue)i.next();
				
				list.add(adLookUpValue.getLvName());
				
			}
			
			return list;
			
		} catch (Exception ex) {
			
			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());
			
		}
		
	}
    
    
    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public com.util.ApVoucherBatchDetails getApVbByVbCode(Integer VB_CODE, Integer AD_CMPNY) 
        throws GlobalNoRecordFoundException {
                    
        Debug.print("ApVoucherBatchEntryControllerBean getApVbByVbCode");
        
        LocalApVoucherBatchHome apVoucherBatchHome = null;        
                
        // Initialize EJB Home
        
        try {
            
            apVoucherBatchHome = (LocalApVoucherBatchHome)EJBHomeFactory.
                lookUpLocalHome(LocalApVoucherBatchHome.JNDI_NAME, LocalApVoucherBatchHome.class);
            
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }        
        
        try {
        	
        	LocalApVoucherBatch apVoucherBatch = null;
        	
        	
        	try {
        		
        		apVoucherBatch = apVoucherBatchHome.findByPrimaryKey(VB_CODE);
        		
        	} catch (FinderException ex) {
        		
        		throw new GlobalNoRecordFoundException();
        		
        	}
        	
        	ApVoucherBatchDetails details = new ApVoucherBatchDetails();
        	details.setVbCode(apVoucherBatch.getVbCode());
        	details.setVbName(apVoucherBatch.getVbName());
        	details.setVbDescription(apVoucherBatch.getVbDescription());
        	details.setVbStatus(apVoucherBatch.getVbStatus());
        	details.setVbType(apVoucherBatch.getVbType());
        	details.setVbDateCreated(apVoucherBatch.getVbDateCreated());
        	details.setVbDepartment(apVoucherBatch.getVbDepartment());
        	details.setVbCreatedBy(apVoucherBatch.getVbCreatedBy());
        	
        	return details;
        	
        	
        } catch (GlobalNoRecordFoundException ex) {
        	
        	throw ex;
        	        	
        } catch (Exception ex) {
        	
        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());
        	
        }
            
    }
    
    
    /**
     * @ejb:interface-method view-type="remote"
     **/
    public Integer saveApVbEntry(com.util.ApVoucherBatchDetails details, Integer AD_BRNCH, Integer AD_CMPNY) throws
        GlobalRecordAlreadyExistException,
        GlobalRecordAlreadyDeletedException,
		GlobalTransactionBatchCloseException,
		GlobalRecordAlreadyAssignedException {
                    
        Debug.print("ApVoucherBatchEntryControllerBean saveApVbEntry");
        
        LocalApVoucherBatchHome apVoucherBatchHome = null;  
        LocalApVoucherHome apVoucherHome = null;       
        
        LocalApVoucherBatch apVoucherBatch = null;
         
                
        // Initialize EJB Home
        
        try {
            
            apVoucherBatchHome = (LocalApVoucherBatchHome)EJBHomeFactory.
                lookUpLocalHome(LocalApVoucherBatchHome.JNDI_NAME, LocalApVoucherBatchHome.class);     
            apVoucherHome = (LocalApVoucherHome)EJBHomeFactory.
                lookUpLocalHome(LocalApVoucherHome.JNDI_NAME, LocalApVoucherHome.class);       
                        
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        } 
        
        try {
 
                		
			// validate if voucher batch is already deleted
			
			try {
				
				if (details.getVbCode() != null) {
					
					apVoucherBatch = apVoucherBatchHome.findByPrimaryKey(details.getVbCode());
					
				}
				
		 	} catch (FinderException ex) {
		 		
		 		throw new GlobalRecordAlreadyDeletedException();	 		
				
			} 
	        
	        // validate if voucher batch exists
	        		
			try {
				
			    LocalApVoucherBatch apExistingVoucherBatch = apVoucherBatchHome.findByVbName(details.getVbName(), AD_BRNCH, AD_CMPNY);
			
			    if (details.getVbCode() == null ||
			        details.getVbCode() != null && !apExistingVoucherBatch.getVbCode().equals(details.getVbCode())) {
			    	
			        throw new GlobalRecordAlreadyExistException();
			        
			    }
			 
			} catch (GlobalRecordAlreadyExistException ex) {
				
				throw ex;
			
			} catch (FinderException ex) {
			    
			} 	
			
			// validate if voucher batch closing
			
			if (details.getVbStatus().equals("CLOSED")) {
				
				Collection apVouchers = apVoucherHome.findByVouPostedAndVouVoidAndVbName(EJBCommon.FALSE, EJBCommon.FALSE, details.getVbName(), AD_CMPNY);
				
				if (!apVouchers.isEmpty()) {
					
					throw new GlobalTransactionBatchCloseException();
					
				}
				
			}
			
			// validate if voucher already assigned
			
			if (details.getVbCode() != null) {
				
				Collection apVouchers = apVoucherBatch.getApVouchers();
				
				if (!apVoucherBatch.getVbType().equals(details.getVbType()) &&
				    !apVouchers.isEmpty()) {
				    	
				    throw new GlobalRecordAlreadyAssignedException();
				    	
				}
				
			}											
			
			
			if (details.getVbCode() == null) {
				
				apVoucherBatch = apVoucherBatchHome.create(details.getVbName(),
				   details.getVbDescription(), details.getVbStatus(), details.getVbType(),
				   details.getVbDateCreated(), details.getVbCreatedBy(), details.getVbDepartment(), AD_BRNCH, AD_CMPNY);
				
			} else {
				
				apVoucherBatch.setVbName(details.getVbName());
				apVoucherBatch.setVbDescription(details.getVbDescription());
				apVoucherBatch.setVbStatus(details.getVbStatus());
				apVoucherBatch.setVbType(details.getVbType());
				apVoucherBatch.setVbDepartment(details.getVbDepartment());
				
			}
			
			return apVoucherBatch.getVbCode();				
		      	            	
        } catch (GlobalRecordAlreadyDeletedException ex) {
        	 
        	getSessionContext().setRollbackOnly();      	  
      	    throw ex;
        
        } catch (GlobalRecordAlreadyExistException ex) {	 	
        
            getSessionContext().setRollbackOnly();      	  
      	    throw ex;
      	    
      	} catch (GlobalTransactionBatchCloseException ex) {
        	
        	getSessionContext().setRollbackOnly();
        	throw ex;
        	
        } catch (GlobalRecordAlreadyAssignedException ex) {
        	
        	getSessionContext().setRollbackOnly();
        	throw ex;
                	
        } catch (Exception ex) {
        	
        	Debug.printStackTrace(ex);
        	getSessionContext().setRollbackOnly();
        	throw new EJBException(ex.getMessage());
        	
        }
            
    }    
            
    
    /**
     * @ejb:interface-method view-type="remote"
     **/
    public void deleteApVbEntry(Integer VB_CODE, Integer AD_CMPNY) throws 
        GlobalRecordAlreadyDeletedException,
        GlobalRecordAlreadyAssignedException {
                    
        Debug.print("ApVoucherBatchEntryControllerBean deleteApVbEntry");
        
        LocalApVoucherBatchHome apVoucherBatchHome = null;  
        LocalApVoucherHome apVoucherHome = null;               
                
        // Initialize EJB Home
        
        try {
            
            apVoucherBatchHome = (LocalApVoucherBatchHome)EJBHomeFactory.
                lookUpLocalHome(LocalApVoucherBatchHome.JNDI_NAME, LocalApVoucherBatchHome.class);     
            apVoucherHome = (LocalApVoucherHome)EJBHomeFactory.
                lookUpLocalHome(LocalApVoucherHome.JNDI_NAME, LocalApVoucherHome.class);       
                        
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }        
        
        try {
        	
        	LocalApVoucherBatch apVoucherBatch = apVoucherBatchHome.findByPrimaryKey(VB_CODE);
        	
        	Collection apVouchers = apVoucherBatch.getApVouchers();
        	
        	if (!apVouchers.isEmpty()) {
        		
        		throw new GlobalRecordAlreadyAssignedException();
        		
        	}
        	
        	apVoucherBatch.remove();
        	
        } catch (FinderException ex) {	
            
            getSessionContext().setRollbackOnly();
            throw new GlobalRecordAlreadyDeletedException();      	
        	
        } catch (GlobalRecordAlreadyAssignedException ex) {	
            
            getSessionContext().setRollbackOnly();
            throw ex; 
        	
        } catch (Exception ex) {
        	
        	Debug.printStackTrace(ex);
        	getSessionContext().setRollbackOnly();
        	throw new EJBException(ex.getMessage());
        	
        }
            
    }
    
              
	// SessionBean methods

    /**
     * @ejb:create-method view-type="remote"
     **/
    public void ejbCreate() throws CreateException {

       Debug.print("ApVoucherBatchEntryControllerBean ejbCreate");
      
    }
    
   // private methods

   

}