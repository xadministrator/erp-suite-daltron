package com.ejb.txn;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.GregorianCalendar;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;
import javax.naming.NamingException;

import com.ejb.ad.LocalAdAmountLimitHome;
import com.ejb.ad.LocalAdApprovalHome;
import com.ejb.ad.LocalAdApprovalQueueHome;
import com.ejb.ad.LocalAdApprovalUserHome;
import com.ejb.ad.LocalAdCompany;
import com.ejb.ad.LocalAdCompanyHome;
import com.ejb.ad.LocalAdPaymentSchedule;
import com.ejb.ad.LocalAdPaymentTerm;
import com.ejb.ad.LocalAdPaymentTermHome;
import com.ejb.ad.LocalAdPreference;
import com.ejb.ad.LocalAdPreferenceHome;
import com.ejb.ap.LocalApDistributionRecord;
import com.ejb.ap.LocalApDistributionRecordHome;
import com.ejb.ap.LocalApSupplier;
import com.ejb.ap.LocalApSupplierHome;
import com.ejb.ap.LocalApTaxCode;
import com.ejb.ap.LocalApTaxCodeHome;
import com.ejb.ap.LocalApVoucher;
import com.ejb.ap.LocalApVoucherBatch;
import com.ejb.ap.LocalApVoucherBatchHome;
import com.ejb.ap.LocalApVoucherHome;
import com.ejb.ap.LocalApVoucherLineItem;
import com.ejb.ap.LocalApVoucherLineItemHome;
import com.ejb.ap.LocalApVoucherPaymentSchedule;
import com.ejb.ap.LocalApVoucherPaymentScheduleHome;
import com.ejb.ap.LocalApWithholdingTaxCode;
import com.ejb.ap.LocalApWithholdingTaxCodeHome;
import com.ejb.exception.GlobalDocumentNumberNotUniqueException;
import com.ejb.exception.GlobalInvItemLocationNotFoundException;
import com.ejb.exception.GlobalJournalNotBalanceException;
import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.exception.GlobalPaymentTermInvalidException;
import com.ejb.exception.GlobalReferenceNumberNotUniqueException;
import com.ejb.gl.LocalAdDocumentSequenceAssignmentHome;
import com.ejb.gl.LocalGlChartOfAccount;
import com.ejb.gl.LocalGlChartOfAccountHome;
import com.ejb.gl.LocalGlFunctionalCurrency;
import com.ejb.gl.LocalGlFunctionalCurrencyHome;
import com.ejb.gl.LocalGlFunctionalCurrencyRateHome;
import com.ejb.inv.LocalInvCostingHome;
import com.ejb.inv.LocalInvItemLocation;
import com.ejb.inv.LocalInvItemLocationHome;
import com.ejb.inv.LocalInvUnitOfMeasure;
import com.ejb.inv.LocalInvUnitOfMeasureHome;
import com.util.AbstractSessionBean;
import com.util.ApModVoucherDetails;
import com.util.ApModVoucherLineItemDetails;
import com.util.Debug;
import com.util.EJBCommon;
import com.util.EJBHomeFactory;

/**
 * @ejb:bean name="ApVoucherImportControllerEJB"
 *           display-name="used for importing vouchers CSV"
 *           type="Stateless"
 *           view-type="remote"
 *           jndi-name="ejb/ApVoucherImportControllerEJB"
 *
 * @ejb:interface remote-class="com.ejb.txn.ApVoucherImportController"
 *                extends="javax.ejb.EJBObject"
 *
 * @ejb:home remote-class="com.ejb.txn.ApVoucherImportControllerHome"
 *           extends="javax.ejb.EJBHome"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="apuser"
 *                        role-link="apuserlink"
 *
 * @ejb:permission role-name="apuser"
 *
*/

public class ApVoucherImportControllerBean extends AbstractSessionBean {

	/**
	 * @ejb:interface-method view-type="remote"
	 **/
	public void importVoucher(ArrayList list, String PYT_NM, String TC_NM, String WTC_NM, String FC_NM, String VB_NM, Integer AD_BRNCH, Integer AD_CMPNY) throws
		GlobalDocumentNumberNotUniqueException,
		GlobalPaymentTermInvalidException,
		GlobalInvItemLocationNotFoundException,
		GlobalReferenceNumberNotUniqueException,
		GlobalNoRecordFoundException,
		GlobalJournalNotBalanceException {

		Debug.print("ApVoucherImportControllerBean saveApVouVliEntry");

		LocalApVoucherHome apVoucherHome = null;
		LocalApVoucherBatchHome apVoucherBatchHome = null;
		LocalAdCompanyHome adCompanyHome = null;
		LocalGlFunctionalCurrencyHome glFunctionalCurrencyHome = null;
		LocalAdDocumentSequenceAssignmentHome adDocumentSequenceAssignmentHome = null;
		LocalAdPaymentTermHome adPaymentTermHome = null;
		LocalApTaxCodeHome apTaxCodeHome = null;
		LocalApWithholdingTaxCodeHome apWithholdingTaxCodeHome = null;
		LocalApSupplierHome apSupplierHome = null;
		LocalGlFunctionalCurrencyRateHome glFunctionalCurrencyRateHome = null;
		LocalApVoucherPaymentScheduleHome apVoucherPaymentScheduleHome = null;
		LocalAdApprovalHome adApprovalHome = null;
		LocalAdAmountLimitHome adAmountLimitHome = null;
		LocalAdApprovalUserHome adApprovalUserHome = null;
		LocalAdApprovalQueueHome adApprovalQueueHome = null;
		LocalAdPreferenceHome adPreferenceHome = null;
		LocalInvItemLocationHome invItemLocationHome = null;
		LocalInvCostingHome invCostingHome = null;

		LocalApVoucher apVoucher = null;

		// Initialize EJB Home

		try {

			apVoucherHome = (LocalApVoucherHome)EJBHomeFactory.
				lookUpLocalHome(LocalApVoucherHome.JNDI_NAME, LocalApVoucherHome.class);
			apVoucherBatchHome = (LocalApVoucherBatchHome)EJBHomeFactory.
				lookUpLocalHome(LocalApVoucherBatchHome.JNDI_NAME, LocalApVoucherBatchHome.class);
			adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
			glFunctionalCurrencyHome = (LocalGlFunctionalCurrencyHome)EJBHomeFactory.
				lookUpLocalHome(LocalGlFunctionalCurrencyHome.JNDI_NAME, LocalGlFunctionalCurrencyHome.class);
			adDocumentSequenceAssignmentHome = (LocalAdDocumentSequenceAssignmentHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdDocumentSequenceAssignmentHome.JNDI_NAME, LocalAdDocumentSequenceAssignmentHome.class);
			adPaymentTermHome = (LocalAdPaymentTermHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdPaymentTermHome.JNDI_NAME, LocalAdPaymentTermHome.class);
			apTaxCodeHome = (LocalApTaxCodeHome)EJBHomeFactory.
				lookUpLocalHome(LocalApTaxCodeHome.JNDI_NAME, LocalApTaxCodeHome.class);
			apWithholdingTaxCodeHome = (LocalApWithholdingTaxCodeHome)EJBHomeFactory.
				lookUpLocalHome(LocalApWithholdingTaxCodeHome.JNDI_NAME, LocalApWithholdingTaxCodeHome.class);
			apSupplierHome = (LocalApSupplierHome)EJBHomeFactory.
				lookUpLocalHome(LocalApSupplierHome.JNDI_NAME, LocalApSupplierHome.class);
			glFunctionalCurrencyRateHome = (LocalGlFunctionalCurrencyRateHome)EJBHomeFactory.
				lookUpLocalHome(LocalGlFunctionalCurrencyRateHome.JNDI_NAME, LocalGlFunctionalCurrencyRateHome.class);
			apVoucherPaymentScheduleHome = (LocalApVoucherPaymentScheduleHome)EJBHomeFactory.
				lookUpLocalHome(LocalApVoucherPaymentScheduleHome.JNDI_NAME, LocalApVoucherPaymentScheduleHome.class);
			adApprovalHome = (LocalAdApprovalHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdApprovalHome.JNDI_NAME, LocalAdApprovalHome.class);
			adAmountLimitHome = (LocalAdAmountLimitHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdAmountLimitHome.JNDI_NAME, LocalAdAmountLimitHome.class);
			adApprovalUserHome = (LocalAdApprovalUserHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdApprovalUserHome.JNDI_NAME, LocalAdApprovalUserHome.class);
			adApprovalQueueHome = (LocalAdApprovalQueueHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdApprovalQueueHome.JNDI_NAME, LocalAdApprovalQueueHome.class);
			adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);
			invItemLocationHome = (LocalInvItemLocationHome)EJBHomeFactory.
				lookUpLocalHome(LocalInvItemLocationHome.JNDI_NAME, LocalInvItemLocationHome.class);
			invCostingHome = (LocalInvCostingHome)EJBHomeFactory.
				lookUpLocalHome(LocalInvCostingHome.JNDI_NAME, LocalInvCostingHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}

		try {

			Iterator itr = list.iterator();

			while (itr.hasNext()) {

				ApModVoucherDetails mdetails = (ApModVoucherDetails)itr.next();

				// validate if document number is unique

				if (mdetails.getVouCode() == null) {

					LocalApVoucher apExistingVoucher = null;

					try {

						apExistingVoucher = apVoucherHome.findByVouDocumentNumberAndVouDebitMemoAndBrCode(
								mdetails.getVouDocumentNumber(), EJBCommon.FALSE, AD_BRNCH, AD_CMPNY);

					} catch (FinderException ex) {
					}

					if (apExistingVoucher != null) {

						throw new GlobalDocumentNumberNotUniqueException(mdetails.getVouDocumentNumber());

					}

				}

				// validate if payment term has at least one payment schedule

				if (adPaymentTermHome.findByPytName(PYT_NM, AD_CMPNY).getAdPaymentSchedules().isEmpty()) {

					throw new GlobalPaymentTermInvalidException();

				}

				LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);

				// validate reference number

				if(adPreference.getPrfApReferenceNumberValidation() == EJBCommon.TRUE) {

					Collection apExistingVouchers = null;

					try {

						apExistingVouchers = apVoucherHome.findByVouReferenceNumber(mdetails.getVouReferenceNumber(), AD_BRNCH, AD_CMPNY);

					} catch (FinderException ex) {
					}

					if (apExistingVouchers != null && !apExistingVouchers.isEmpty()) {

						Iterator i = apExistingVouchers.iterator();

						while(i.hasNext()) {

							LocalApVoucher apExistingVoucher = (LocalApVoucher)i.next();

							if (!apExistingVoucher.getVouCode().equals(mdetails.getVouCode()))

								throw new GlobalReferenceNumberNotUniqueException();

						}

					}

				}

				// used in checking if voucher should re-generate distribution records and re-calculate taxes
				boolean isRecalculate = true;

				// create voucher

				if (mdetails.getVouCode() == null) {
					apVoucher = apVoucherHome.create(EJBCommon.FALSE,
							mdetails.getVouDescription(), mdetails.getVouDate(),
							mdetails.getVouDocumentNumber(), mdetails.getVouReferenceNumber(), null,
							mdetails.getVouConversionDate(), mdetails.getVouConversionRate(),
							0d, 0d, 0d, null, null, EJBCommon.FALSE, EJBCommon.FALSE, EJBCommon.FALSE,
							mdetails.getVouCreatedBy(), mdetails.getVouDateCreated(),
							mdetails.getVouLastModifiedBy(), mdetails.getVouDateLastModified(),
							null, null, null, null, EJBCommon.FALSE, mdetails.getVouPoNumber(),
							EJBCommon.FALSE, EJBCommon.FALSE,
							AD_BRNCH, AD_CMPNY);

				}

				LocalAdPaymentTerm adPaymentTerm = adPaymentTermHome.findByPytName(PYT_NM, AD_CMPNY);
				apVoucher.setAdPaymentTerm(adPaymentTerm);

				LocalApTaxCode apTaxCode = apTaxCodeHome.findByTcName(TC_NM, AD_CMPNY);
				apVoucher.setApTaxCode(apTaxCode);

				LocalApWithholdingTaxCode apWithholdingTaxCode = apWithholdingTaxCodeHome.findByWtcName(WTC_NM, AD_CMPNY);
				apVoucher.setApWithholdingTaxCode(apWithholdingTaxCode);

				LocalGlFunctionalCurrency glFunctionalCurrency = glFunctionalCurrencyHome.findByFcName(FC_NM, AD_CMPNY);
				apVoucher.setGlFunctionalCurrency(glFunctionalCurrency);

				try {

					LocalApSupplier apSupplier = apSupplierHome.findBySplName(mdetails.getVouSplName(), AD_BRNCH, AD_CMPNY);
					apVoucher.setApSupplier(apSupplier);

				} catch (FinderException ex) {

					throw new GlobalNoRecordFoundException(mdetails.getVouSplName());

				}

				LocalApVoucherBatch apVoucherBatch = null;

				try {

					apVoucherBatch = apVoucherBatchHome.findByVbName(VB_NM, AD_BRNCH, AD_CMPNY);
					apVoucher.setApVoucherBatch(apVoucherBatch);

				} catch (FinderException ex) {

					// create new batch and validate

					apVoucherBatch = apVoucherBatchHome.create(VB_NM, null, "OPEN", "VOUCHER",
							mdetails.getVouDateCreated(), mdetails.getVouCreatedBy(),"", AD_BRNCH, AD_CMPNY);
					apVoucherBatch.addApVoucher(apVoucher);

				}

				if (isRecalculate) {

					// remove all voucher line items

					Collection apVoucherLineItems = apVoucher.getApVoucherLineItems();

					Iterator i = apVoucherLineItems.iterator();

					while (i.hasNext()) {

						LocalApVoucherLineItem apVoucherLineItem = (LocalApVoucherLineItem)i.next();

						i.remove();

						apVoucherLineItem.remove();

					}

					// remove all distribution records

					Collection apDistributionRecords = apVoucher.getApDistributionRecords();

					i = apDistributionRecords.iterator();

					while (i.hasNext()) {

						LocalApDistributionRecord apDistributionRecord = (LocalApDistributionRecord)i.next();

						i.remove();

						apDistributionRecord.remove();

					}

					// add new voucher line item and distribution record

					double TOTAL_TAX = 0d;
					double TOTAL_LINE = 0d;

					i = mdetails.getVouVliList().iterator();

					while (i.hasNext()) {

						ApModVoucherLineItemDetails mVliDetails = (ApModVoucherLineItemDetails) i.next();

						Collection invItemLocations = null;

						try {

							invItemLocations = invItemLocationHome.findByIiName(mVliDetails.getVliIiName(), AD_CMPNY);

						} catch (FinderException ex) {
						}

						if(invItemLocations == null || invItemLocations.isEmpty()) {

							throw new GlobalInvItemLocationNotFoundException(mVliDetails.getVliIiName());

						}

						LocalInvItemLocation invItemLocation = null;

						Iterator j = invItemLocations.iterator();

						while (j.hasNext()) {

							 invItemLocation = (LocalInvItemLocation)j.next();
							 break;

						}

						// set unit of measure

						LocalInvUnitOfMeasure invUnitOfMeasure = invItemLocation.getInvItem().getInvUnitOfMeasure();
						mVliDetails.setVliUomName(invUnitOfMeasure.getUomName());


						LocalApVoucherLineItem apVoucherLineItem = this.addApVliEntry(mVliDetails, apVoucher, invItemLocation, AD_CMPNY);

						// add inventory distributions

						this.addApDrVliEntry(apVoucher.getApDrNextLine(),
								"EXPENSE", EJBCommon.TRUE, apVoucherLineItem.getVliAmount(),
								apVoucherLineItem.getInvItemLocation().getIlGlCoaInventoryAccount(), apVoucher, AD_CMPNY);

						TOTAL_LINE += apVoucherLineItem.getVliAmount();
						TOTAL_TAX += apVoucherLineItem.getVliTaxAmount();

					}

					// check amount if balance

					if(EJBCommon.roundIt(TOTAL_LINE + TOTAL_TAX, (short)(10)) != mdetails.getVouAmountDue()) {

	    				throw new GlobalJournalNotBalanceException(mdetails.getVouDocumentNumber());

	    			}


					// add tax distribution if necessary

					if (!apTaxCode.getTcType().equals("NONE") &&
							!apTaxCode.getTcType().equals("EXEMPT")) {

						this.addApDrVliEntry(apVoucher.getApDrNextLine(),
								"TAX", EJBCommon.TRUE, TOTAL_TAX, apTaxCode.getGlChartOfAccount().getCoaCode(),
								apVoucher, AD_CMPNY);

					}

					// add wtax distribution if necessary

					double W_TAX_AMOUNT = 0d;

					if (apWithholdingTaxCode.getWtcRate() != 0 && adPreference.getPrfApWTaxRealization().equals("VOUCHER")) {

						W_TAX_AMOUNT = EJBCommon.roundIt(TOTAL_LINE * (apWithholdingTaxCode.getWtcRate() / 100), (short)(10));

						this.addApDrVliEntry(apVoucher.getApDrNextLine(), "W-TAX",
								EJBCommon.FALSE, W_TAX_AMOUNT, apWithholdingTaxCode.getGlChartOfAccount().getCoaCode(),
								apVoucher, AD_CMPNY);

					}


					// add payable distribution

					this.addApDrVliEntry(apVoucher.getApDrNextLine(), "PAYABLE",
							EJBCommon.FALSE, TOTAL_LINE + TOTAL_TAX - W_TAX_AMOUNT,
							apVoucher.getApSupplier().getSplCoaGlPayableAccount(),
							apVoucher, AD_CMPNY);


					// set voucher amount due

					apVoucher.setVouAmountDue(mdetails.getVouAmountDue());


					// remove all payment schedule

					Collection apVoucherPaymentSchedules = apVoucher.getApVoucherPaymentSchedules();

					i = apVoucherPaymentSchedules.iterator();

					while (i.hasNext()) {

						LocalApVoucherPaymentSchedule apVoucherPaymentSchedule = (LocalApVoucherPaymentSchedule)i.next();

						i.remove();

						apVoucherPaymentSchedule.remove();

					}

					// create voucher payment schedule

					short precisionUnit = (short)(10);
					double TOTAL_PAYMENT_SCHEDULE =  0d;

					Collection adPaymentSchedules = adPaymentTerm.getAdPaymentSchedules();

					i = adPaymentSchedules.iterator();

					while (i.hasNext()) {

						LocalAdPaymentSchedule adPaymentSchedule = (LocalAdPaymentSchedule)i.next();

						// get date due

						GregorianCalendar gcDateDue = new GregorianCalendar();
						gcDateDue.setTime(apVoucher.getVouDate());
						gcDateDue.add(Calendar.DATE, adPaymentSchedule.getPsDueDay());

						// create a payment schedule

						double PAYMENT_SCHEDULE_AMOUNT = 0;

						// if last payment schedule subtract to avoid rounding difference error

						if (i.hasNext()) {

							PAYMENT_SCHEDULE_AMOUNT = EJBCommon.roundIt((adPaymentSchedule.getPsRelativeAmount() / adPaymentTerm.getPytBaseAmount()) * apVoucher.getVouAmountDue(), precisionUnit);

						} else {

							PAYMENT_SCHEDULE_AMOUNT = apVoucher.getVouAmountDue() - TOTAL_PAYMENT_SCHEDULE;

						}

						LocalApVoucherPaymentSchedule apVoucherPaymentSchedule =
							apVoucherPaymentScheduleHome.create(gcDateDue.getTime(),
									adPaymentSchedule.getPsLineNumber(),
									PAYMENT_SCHEDULE_AMOUNT,
									0d, EJBCommon.FALSE, AD_CMPNY);

						apVoucher.addApVoucherPaymentSchedule(apVoucherPaymentSchedule);

						TOTAL_PAYMENT_SCHEDULE += PAYMENT_SCHEDULE_AMOUNT;

					}

				}

				// set voucher approval status

				apVoucher.setVouApprovalStatus(null);

			}


		} catch (GlobalDocumentNumberNotUniqueException ex) {

			getSessionContext().setRollbackOnly();
			throw ex;

		} catch (GlobalPaymentTermInvalidException ex) {

			getSessionContext().setRollbackOnly();
			throw ex;

		} catch (GlobalInvItemLocationNotFoundException ex) {

			getSessionContext().setRollbackOnly();
			throw ex;

		} catch (GlobalReferenceNumberNotUniqueException ex) {

			getSessionContext().setRollbackOnly();
			throw ex;

		} catch (GlobalNoRecordFoundException ex) {

			getSessionContext().setRollbackOnly();
			throw ex;

		} catch (GlobalJournalNotBalanceException ex) {

			getSessionContext().setRollbackOnly();
			throw ex;

		} catch (Exception ex) {

			Debug.printStackTrace(ex);
			getSessionContext().setRollbackOnly();
			throw new EJBException(ex.getMessage());

		}

	}

	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public short getGlFcPrecisionUnit(Integer AD_CMPNY) {

		Debug.print("ApVoucherImportControllerBean getGlFcPrecisionUnit");


		LocalAdCompanyHome adCompanyHome = null;


		// Initialize EJB Home

		try {

			adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}

		try {

			LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);

			return  (short)(10);

		} catch (Exception ex) {

			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());

		}

	}

	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public short getInvGpQuantityPrecisionUnit(Integer AD_CMPNY) {

		Debug.print("ApVoucherImportControllerBean getInvGpQuantityPrecisionUnit");

		LocalAdPreferenceHome adPreferenceHome = null;

		// Initialize EJB Home

		try {

			adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}


		try {

			LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);

			return (short)(10);

		} catch (Exception ex) {

			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());

		}

	}

	private void addApDrVliEntry(short DR_LN, String DR_CLSS, byte DR_DBT, double DR_AMNT, Integer COA_CODE, LocalApVoucher apVoucher, Integer AD_CMPNY) {

		Debug.print("ApVoucherImportControllerBean addApDrVliEntry");

		LocalApDistributionRecordHome apDistributionRecordHome = null;
		LocalGlChartOfAccountHome glChartOfAccountHome = null;
		LocalAdCompanyHome adCompanyHome = null;


		// Initialize EJB Home

		try {

			apDistributionRecordHome = (LocalApDistributionRecordHome)EJBHomeFactory.
			lookUpLocalHome(LocalApDistributionRecordHome.JNDI_NAME, LocalApDistributionRecordHome.class);
			glChartOfAccountHome = (LocalGlChartOfAccountHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlChartOfAccountHome.JNDI_NAME, LocalGlChartOfAccountHome.class);
			adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}

		try {

			// get company

			LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);

			LocalGlChartOfAccount glChartOfAccount = glChartOfAccountHome.findByPrimaryKey(COA_CODE);

			// create distribution record

			LocalApDistributionRecord apDistributionRecord = apDistributionRecordHome.create(
					DR_LN, DR_CLSS, EJBCommon.roundIt(DR_AMNT, (short)(10)),
					DR_DBT, EJBCommon.FALSE, EJBCommon.FALSE, AD_CMPNY);

			apVoucher.addApDistributionRecord(apDistributionRecord);
			glChartOfAccount.addApDistributionRecord(apDistributionRecord);

		} catch (Exception ex) {

			Debug.printStackTrace(ex);
			getSessionContext().setRollbackOnly();
			throw new EJBException(ex.getMessage());

		}

	}

	private LocalApVoucherLineItem addApVliEntry(ApModVoucherLineItemDetails mdetails, LocalApVoucher apVoucher, LocalInvItemLocation invItemLocation, Integer AD_CMPNY) {

		Debug.print("ApVoucherImportControllerBean addApVliEntry");

		LocalApVoucherLineItemHome apVoucherLineItemHome = null;
		LocalInvItemLocationHome invItemLocationHome = null;
		LocalInvUnitOfMeasureHome invUnitOfMeasureHome = null;

		// Initialize EJB Home

		try {

			apVoucherLineItemHome = (LocalApVoucherLineItemHome)EJBHomeFactory.
				lookUpLocalHome(LocalApVoucherLineItemHome.JNDI_NAME, LocalApVoucherLineItemHome.class);
			invItemLocationHome = (LocalInvItemLocationHome)EJBHomeFactory.
				lookUpLocalHome(LocalInvItemLocationHome.JNDI_NAME, LocalInvItemLocationHome.class);
			invUnitOfMeasureHome = (LocalInvUnitOfMeasureHome)EJBHomeFactory.
				lookUpLocalHome(LocalInvUnitOfMeasureHome.JNDI_NAME, LocalInvUnitOfMeasureHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}

		try {

			short precisionUnit = (short)(10);

			double VLI_AMNT = 0d;
			double VLI_TAX_AMNT = 0d;

			// calculate net amount

			LocalApTaxCode apTaxCode = apVoucher.getApTaxCode();

			if (apTaxCode.getTcType().equals("INCLUSIVE")) {

				VLI_AMNT = EJBCommon.roundIt(mdetails.getVliAmount() / (1 + (apTaxCode.getTcRate() / 100)), precisionUnit);

			} else {

				// tax exclusive, none, zero rated or exempt

				VLI_AMNT = mdetails.getVliAmount();

			}

			// calculate tax

			if (!apTaxCode.getTcType().equals("NONE") &&
					!apTaxCode.getTcType().equals("EXEMPT")) {


				if (apTaxCode.getTcType().equals("INCLUSIVE")) {

					VLI_TAX_AMNT = EJBCommon.roundIt(mdetails.getVliAmount() - VLI_AMNT, precisionUnit);

				} else if (apTaxCode.getTcType().equals("EXCLUSIVE")) {

					VLI_TAX_AMNT = EJBCommon.roundIt(mdetails.getVliAmount() * apTaxCode.getTcRate() / 100, precisionUnit);

				} else {

					// tax none zero-rated or exempt

				}

			}

			LocalApVoucherLineItem apVoucherLineItem = apVoucherLineItemHome.create(
					mdetails.getVliLine(), mdetails.getVliQuantity(), mdetails.getVliUnitCost(),
					VLI_AMNT, VLI_TAX_AMNT, 0, 0, 0, 0, 0,
					null,null,null, mdetails.getVliTax(),
					AD_CMPNY);

			apVoucher.addApVoucherLineItem(apVoucherLineItem);

			invItemLocation.addApVoucherLineItem(apVoucherLineItem);

			LocalInvUnitOfMeasure invUnitOfMeasure = invUnitOfMeasureHome.findByUomName(mdetails.getVliUomName(), AD_CMPNY);
			invUnitOfMeasure.addApVoucherLineItem(apVoucherLineItem);

			return apVoucherLineItem;

		} catch (Exception ex) {

			Debug.printStackTrace(ex);
			getSessionContext().setRollbackOnly();
			throw new EJBException(ex.getMessage());

		}

	}

	// SessionBean methods

	/**
	 * @ejb:create-method view-type="remote"
	 **/
	public void ejbCreate() throws CreateException {

		Debug.print("ApVoucherImportControllerBean ejbCreate");

	}

}