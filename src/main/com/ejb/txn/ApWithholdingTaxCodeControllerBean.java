package com.ejb.txn;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;
import javax.naming.NamingException;

import com.ejb.ap.LocalApWithholdingTaxCode;
import com.ejb.ap.LocalApWithholdingTaxCodeHome;
import com.ejb.exception.GlobalAccountNumberInvalidException;
import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.exception.GlobalRecordAlreadyAssignedException;
import com.ejb.exception.GlobalRecordAlreadyDeletedException;
import com.ejb.exception.GlobalRecordAlreadyExistException;
import com.ejb.gl.LocalGlChartOfAccount;
import com.ejb.gl.LocalGlChartOfAccountHome;
import com.util.AbstractSessionBean;
import com.util.ApModWithholdingTaxCodeDetails;
import com.util.Debug;
import com.util.EJBHomeFactory;

/**
 * @ejb:bean name="ApWithholdingTaxCodeControllerEJB"
 *           display-name="Used for entering withholding tax codes"
 *           type="Stateless"
 *           view-type="remote"
 *           jndi-name="ejb/ApWithholdingTaxCodeControllerEJB"
 *
 * @ejb:interface remote-class="com.ejb.txn.ApWithholdingTaxCodeController"
 *                extends="javax.ejb.EJBObject"
 *
 * @ejb:home remote-class="com.ejb.txn.ApWithholdingTaxCodeControllerHome"
 *           extends="javax.ejb.EJBHome"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="apuser"
 *                        role-link="apuserlink"
 *
 * @ejb:permission role-name="apuser"
 * 
*/

public class ApWithholdingTaxCodeControllerBean extends AbstractSessionBean {

    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList getApWtcAll(Integer AD_CMPNY) 
        throws GlobalNoRecordFoundException {
                    
        Debug.print("ApWithholdingTaxCodeControllerBean getApWtcAll");

        LocalApWithholdingTaxCodeHome apWithholdingTaxCodeHome = null;

        ArrayList list = new ArrayList();
        
        // Initialize EJB Home

        try {

            apWithholdingTaxCodeHome = (LocalApWithholdingTaxCodeHome)EJBHomeFactory.
                lookUpLocalHome(LocalApWithholdingTaxCodeHome.JNDI_NAME, LocalApWithholdingTaxCodeHome.class);

        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {
        	
	        Collection apWithholdingTaxCodes = apWithholdingTaxCodeHome.findWtcAll(AD_CMPNY);
	
	        if (apWithholdingTaxCodes.isEmpty()) {
	
	            throw new GlobalNoRecordFoundException();
	        	
	        }
	            
	        Iterator i = apWithholdingTaxCodes.iterator();
	               
	        while (i.hasNext()) {
	        	
	        	LocalApWithholdingTaxCode apWithholdingTaxCode = (LocalApWithholdingTaxCode)i.next();
	        	
	        	ApModWithholdingTaxCodeDetails mdetails = new ApModWithholdingTaxCodeDetails();
	        	mdetails.setWtcCode(apWithholdingTaxCode.getWtcCode());        		
	        	mdetails.setWtcName(apWithholdingTaxCode.getWtcName());                
	        	mdetails.setWtcDescription(apWithholdingTaxCode.getWtcDescription());
	        	mdetails.setWtcRate(apWithholdingTaxCode.getWtcRate());
	        	mdetails.setWtcEnable(apWithholdingTaxCode.getWtcEnable());
	        	
	        	if (apWithholdingTaxCode.getGlChartOfAccount() != null) {
	        		
	        		mdetails.setWtcCoaGlWithholdingTaxAccountNumber(apWithholdingTaxCode.getGlChartOfAccount().getCoaAccountNumber());	                		                	    
	        		mdetails.setWtcCoaGlWithholdingTaxAccountDescription(apWithholdingTaxCode.getGlChartOfAccount().getCoaAccountDescription());
	        		
	        	}					    		    	                          
	        	
	        	list.add(mdetails);
	        	
	        }              
	                                                        		        		        
	        return list;
	        
        } catch (GlobalNoRecordFoundException ex) {
        	
        	throw ex;
        	        	
        } catch (Exception ex) {
        	
        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());
        	
        }
            
    }    
    
    /**
     * @ejb:interface-method view-type="remote"
     **/    
    public void addApWtcEntry(com.util.ApWithholdingTaxCodeDetails details, String WTC_COA_ACCNT_NMBR, Integer AD_CMPNY) 
        throws GlobalRecordAlreadyExistException,
               GlobalAccountNumberInvalidException {
                    
        Debug.print("ApWithholdingTaxCodeControllerBean addApWtcEntry");
        
        LocalApWithholdingTaxCodeHome apWithholdingTaxCodeHome = null;
        LocalGlChartOfAccountHome glChartOfAccountHome = null;

        ArrayList list = new ArrayList();
        
        // Initialize EJB Home
        
        try {
            
            apWithholdingTaxCodeHome = (LocalApWithholdingTaxCodeHome)EJBHomeFactory.
                lookUpLocalHome(LocalApWithholdingTaxCodeHome.JNDI_NAME, LocalApWithholdingTaxCodeHome.class);
            glChartOfAccountHome = (LocalGlChartOfAccountHome)EJBHomeFactory.
                lookUpLocalHome(LocalGlChartOfAccountHome.JNDI_NAME, LocalGlChartOfAccountHome.class);
                                                      
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {
        	
            LocalApWithholdingTaxCode apWithholdingTaxCode = null;	
            LocalGlChartOfAccount glChartOfAccount = null;
        
	        try { 
	            
	            apWithholdingTaxCode = apWithholdingTaxCodeHome.findByWtcName(details.getWtcName(), AD_CMPNY);
	            
	            throw new GlobalRecordAlreadyExistException();
	            
	         } catch (FinderException ex) {
	         	
	        	 	
	         }
	                            
	        // get glChartOfAccount to validate accounts 
	        
	        try {
	        	
	        	if (WTC_COA_ACCNT_NMBR != null && WTC_COA_ACCNT_NMBR.length() > 0) {
	       
		            glChartOfAccount = glChartOfAccountHome.findByCoaAccountNumber(
		               WTC_COA_ACCNT_NMBR, AD_CMPNY);
		               
                }		               
	               
	        } catch (FinderException ex) {
	        
	            throw new GlobalAccountNumberInvalidException();
	                       
	        }
	        
	    	// create new tax code
	    	
	    	apWithholdingTaxCode = apWithholdingTaxCodeHome.create(details.getWtcName(),
	    	        details.getWtcDescription(), details.getWtcRate(),
	    	        details.getWtcEnable(), AD_CMPNY);
	    	        
	    	if (WTC_COA_ACCNT_NMBR != null && WTC_COA_ACCNT_NMBR.length() > 0) {
	    		        
	    		glChartOfAccount.addApWithholdingTaxCode(apWithholdingTaxCode);
	    		
	        }
	        	        
        } catch (GlobalRecordAlreadyExistException ex) {
        	
        	getSessionContext().setRollbackOnly();
        	throw ex;
        	
        } catch (GlobalAccountNumberInvalidException ex) {
        	
        	getSessionContext().setRollbackOnly();
        	throw ex;
        	        	
        } catch (Exception ex) {
        	
        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());
        	
        }
            
    }    
    
    /**
     * @ejb:interface-method view-type="remote"
     **/
    public void updateApWtcEntry(com.util.ApWithholdingTaxCodeDetails details, String WTC_COA_ACCNT_NMBR, Integer AD_CMPNY)
        throws GlobalRecordAlreadyExistException, 
               GlobalAccountNumberInvalidException,
               GlobalRecordAlreadyAssignedException{
                    
        Debug.print("ApWithholdingTaxCodeControllerBean updateApWtcEntry");
        
        LocalApWithholdingTaxCodeHome apWithholdingTaxCodeHome = null;                
        LocalGlChartOfAccountHome glChartOfAccountHome = null;                               
                
        ArrayList list = new ArrayList();
        
        // Initialize EJB Home
        
        try {

            apWithholdingTaxCodeHome = (LocalApWithholdingTaxCodeHome)EJBHomeFactory.
                lookUpLocalHome(LocalApWithholdingTaxCodeHome.JNDI_NAME, LocalApWithholdingTaxCodeHome.class);           
            glChartOfAccountHome = (LocalGlChartOfAccountHome)EJBHomeFactory.
                lookUpLocalHome(LocalGlChartOfAccountHome.JNDI_NAME, LocalGlChartOfAccountHome.class);
                         
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {
        	
        	LocalApWithholdingTaxCode apWithholdingTaxCode = null;
        	LocalApWithholdingTaxCode apExistingTaxCode = null;
        	LocalGlChartOfAccount glChartOfAccount = null;

        	try {
                   
	            apExistingTaxCode = apWithholdingTaxCodeHome.findByWtcName(details.getWtcName(), AD_CMPNY);
	            
	            if (!apExistingTaxCode.getWtcCode().equals(details.getWtcCode())) {
	            
	                 throw new GlobalRecordAlreadyExistException();
	                 
	            }
                 
            } catch (FinderException ex) {
            	
            }
            	                                             
	        // get glChartOfAccount to validate accounts 
	        
	        try {
	       
	        	if (WTC_COA_ACCNT_NMBR != null && WTC_COA_ACCNT_NMBR.length() > 0) {
	       
		            glChartOfAccount = glChartOfAccountHome.findByCoaAccountNumber(
		               WTC_COA_ACCNT_NMBR, AD_CMPNY);
		               
                }		               
	               
	        } catch (FinderException ex) {
	        
	            throw new GlobalAccountNumberInvalidException();
	            
	        }

            // find and update withholding tax code 
        	
        	apWithholdingTaxCode = apWithholdingTaxCodeHome.findByPrimaryKey(details.getWtcCode());

	        try {
	        	
	        	if((!apWithholdingTaxCode.getApVouchers().isEmpty() ||
	        	   !apWithholdingTaxCode.getApChecks().isEmpty() ||
				   !apWithholdingTaxCode.getApRecurringVouchers().isEmpty())&&(details.getWtcRate() != apWithholdingTaxCode.getWtcRate())) {
	        		
	        		throw new GlobalRecordAlreadyAssignedException();
	        		
	        	}
	        	
	        } catch (GlobalRecordAlreadyAssignedException ex) {
	        	
	        	throw ex;
	        	
	        }

			    apWithholdingTaxCode.setWtcName(details.getWtcName());
			    apWithholdingTaxCode.setWtcDescription(details.getWtcDescription());
			    apWithholdingTaxCode.setWtcRate(details.getWtcRate());
			    apWithholdingTaxCode.setWtcEnable(details.getWtcEnable());
			    
			    if (apWithholdingTaxCode.getGlChartOfAccount() != null) {
			    	
			    	apWithholdingTaxCode.getGlChartOfAccount().dropApWithholdingTaxCode(apWithholdingTaxCode);
			    	
			    }
			    
		    	if (WTC_COA_ACCNT_NMBR != null && WTC_COA_ACCNT_NMBR.length() > 0) {
		    		        
		    		glChartOfAccount.addApWithholdingTaxCode(apWithholdingTaxCode);
		    		
		        }			    
	                
        } catch (GlobalRecordAlreadyExistException ex) {
        	
        	getSessionContext().setRollbackOnly();
        	throw ex;
        	
        } catch (GlobalAccountNumberInvalidException ex) {
        	
        	getSessionContext().setRollbackOnly();
        	throw ex;
        	
        } catch (GlobalRecordAlreadyAssignedException ex) {
        	
        	getSessionContext().setRollbackOnly();
        	throw ex;
        	                	        	
        } catch (Exception ex) {
        	
        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());
        	
        }
            
    }    
    
   /**
    * @ejb:interface-method view-type="remote"
    **/
    public void deleteApWtcEntry(Integer WTC_CODE, Integer AD_CMPNY) 
        throws GlobalRecordAlreadyDeletedException,
                GlobalRecordAlreadyAssignedException {

        Debug.print("ApWithholdingTaxCodeControllerBean deleteApWtcEntry");
      
        LocalApWithholdingTaxCodeHome apWithholdingTaxCodeHome = null;

        // Initialize EJB Home
        
        try {

            apWithholdingTaxCodeHome = (LocalApWithholdingTaxCodeHome)EJBHomeFactory.
                lookUpLocalHome(LocalApWithholdingTaxCodeHome.JNDI_NAME, LocalApWithholdingTaxCodeHome.class);           
      
        } catch (NamingException ex) {

            throw new EJBException(ex.getMessage());

        }
      
        try {
      	
      	    LocalApWithholdingTaxCode apWithholdingTaxCode = null;                
      
	        try {
	      	
	            apWithholdingTaxCode = apWithholdingTaxCodeHome.findByPrimaryKey(WTC_CODE);
	         
	        } catch (FinderException ex) {
	      	
	            throw new GlobalRecordAlreadyDeletedException();
	            
	        }
	         
	        if (!apWithholdingTaxCode.getApSupplierClasses().isEmpty() ||
	            !apWithholdingTaxCode.getApVouchers().isEmpty() || 
	            !apWithholdingTaxCode.getApChecks().isEmpty() || 
	            !apWithholdingTaxCode.getApRecurringVouchers().isEmpty()) {
	  
	            throw new GlobalRecordAlreadyAssignedException();
	            
	        }
	                            	
		    apWithholdingTaxCode.remove();
	    
        } catch (GlobalRecordAlreadyDeletedException ex) {
        	
        	getSessionContext().setRollbackOnly();
        	throw ex;
        	
        } catch (GlobalRecordAlreadyAssignedException ex) {
        	
        	getSessionContext().setRollbackOnly();
        	throw ex;
        	        	
        } catch (Exception ex) {
        	
        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());
        	
        }
            
    }         
   
    // SessionBean methods

    /**
     * @ejb:create-method view-type="remote"
     **/
    public void ejbCreate() throws CreateException {

        Debug.print("ApWithholdingTaxCodeControllerBean ejbCreate");
      
    }
    
}

