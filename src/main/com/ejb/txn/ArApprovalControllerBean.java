
/*
 * ArApprovalControllerBean.java
 *
 * Created on March 24, 2004, 8:37 PM
 *
 * @author  Dennis M. Hilario
 */

package com.ejb.txn;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;
import javax.naming.NamingException;

import com.ejb.ad.LocalAdAmountLimit;
import com.ejb.ad.LocalAdAmountLimitHome;
import com.ejb.ad.LocalAdApprovalHome;
import com.ejb.ad.LocalAdApprovalQueue;
import com.ejb.ad.LocalAdApprovalQueueHome;
import com.ejb.ad.LocalAdApprovalUser;
import com.ejb.ad.LocalAdApprovalUserHome;
import com.ejb.ad.LocalAdBankAccount;
import com.ejb.ad.LocalAdBankAccountBalance;
import com.ejb.ad.LocalAdBankAccountBalanceHome;
import com.ejb.ad.LocalAdBankAccountHome;
import com.ejb.ad.LocalAdBranchDocumentSequenceAssignment;
import com.ejb.ad.LocalAdBranchDocumentSequenceAssignmentHome;
import com.ejb.ad.LocalAdBranchItemLocation;
import com.ejb.ad.LocalAdBranchItemLocationHome;
import com.ejb.ad.LocalAdCompany;
import com.ejb.ad.LocalAdCompanyHome;
import com.ejb.ad.LocalAdPreference;
import com.ejb.ad.LocalAdPreferenceHome;
import com.ejb.ad.LocalAdUser;
import com.ejb.ad.LocalAdUserHome;
import com.ejb.ap.LocalApPurchaseRequisition;
import com.ejb.ap.LocalApPurchaseRequisitionLine;
import com.ejb.ar.LocalArAppliedCredit;
import com.ejb.ar.LocalArAppliedInvoice;
import com.ejb.ar.LocalArCustomer;
import com.ejb.ar.LocalArCustomerBalance;
import com.ejb.ar.LocalArCustomerBalanceHome;
import com.ejb.ar.LocalArCustomerHome;
import com.ejb.ar.LocalArDistributionRecord;
import com.ejb.ar.LocalArDistributionRecordHome;
import com.ejb.ar.LocalArInvoice;
import com.ejb.ar.LocalArInvoiceHome;
import com.ejb.ar.LocalArInvoiceLineItem;
import com.ejb.ar.LocalArInvoicePaymentSchedule;
import com.ejb.ar.LocalArReceipt;
import com.ejb.ar.LocalArReceiptHome;
import com.ejb.ar.LocalArSalesOrder;
import com.ejb.ar.LocalArSalesOrderHome;
import com.ejb.ar.LocalArSalesOrderInvoiceLine;
import com.ejb.ar.LocalArSalesOrderLine;
import com.ejb.cm.LocalCmAdjustment;
import com.ejb.cm.LocalCmAdjustmentHome;
import com.ejb.cm.LocalCmDistributionRecord;
import com.ejb.cm.LocalCmDistributionRecordHome;
import com.ejb.exception.AdPRFCoaGlVarianceAccountNotFoundException;
import com.ejb.exception.GlJREffectiveDateNoPeriodExistException;
import com.ejb.exception.GlJREffectiveDatePeriodClosedException;
import com.ejb.exception.GlobalBranchAccountNumberInvalidException;
import com.ejb.exception.GlobalConversionDateNotExistException;
import com.ejb.exception.GlobalDocumentNumberNotUniqueException;
import com.ejb.exception.GlobalExpiryDateNotFoundException;
import com.ejb.exception.GlobalInvItemLocationNotFoundException;
import com.ejb.exception.GlobalInventoryDateException;
import com.ejb.exception.GlobalJournalNotBalanceException;
import com.ejb.exception.GlobalNoApprovalApproverFoundException;
import com.ejb.exception.GlobalNoApprovalRequesterFoundException;
import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.exception.GlobalRecordAlreadyDeletedException;
import com.ejb.exception.GlobalTransactionAlreadyPostedException;
import com.ejb.exception.GlobalTransactionAlreadyVoidException;
import com.ejb.exception.GlobalTransactionAlreadyVoidPostedException;
import com.ejb.gl.LocalAdDocumentSequenceAssignment;
import com.ejb.gl.LocalAdDocumentSequenceAssignmentHome;
import com.ejb.gl.LocalGlAccountingCalendarValue;
import com.ejb.gl.LocalGlAccountingCalendarValueHome;
import com.ejb.gl.LocalGlChartOfAccount;
import com.ejb.gl.LocalGlChartOfAccountBalance;
import com.ejb.gl.LocalGlChartOfAccountBalanceHome;
import com.ejb.gl.LocalGlChartOfAccountHome;
import com.ejb.gl.LocalGlForexLedger;
import com.ejb.gl.LocalGlForexLedgerHome;
import com.ejb.gl.LocalGlFunctionalCurrency;
import com.ejb.gl.LocalGlFunctionalCurrencyHome;
import com.ejb.gl.LocalGlFunctionalCurrencyRate;
import com.ejb.gl.LocalGlFunctionalCurrencyRateHome;
import com.ejb.gl.LocalGlJournal;
import com.ejb.gl.LocalGlJournalBatch;
import com.ejb.gl.LocalGlJournalBatchHome;
import com.ejb.gl.LocalGlJournalCategory;
import com.ejb.gl.LocalGlJournalCategoryHome;
import com.ejb.gl.LocalGlJournalHome;
import com.ejb.gl.LocalGlJournalLine;
import com.ejb.gl.LocalGlJournalLineHome;
import com.ejb.gl.LocalGlJournalSource;
import com.ejb.gl.LocalGlJournalSourceHome;
import com.ejb.gl.LocalGlSetOfBook;
import com.ejb.gl.LocalGlSetOfBookHome;
import com.ejb.gl.LocalGlSuspenseAccount;
import com.ejb.gl.LocalGlSuspenseAccountHome;
import com.ejb.inv.LocalInvAdjustment;
import com.ejb.inv.LocalInvAdjustmentHome;
import com.ejb.inv.LocalInvAdjustmentLine;
import com.ejb.inv.LocalInvAdjustmentLineHome;
import com.ejb.inv.LocalInvBillOfMaterial;
import com.ejb.inv.LocalInvCosting;
import com.ejb.inv.LocalInvCostingHome;
import com.ejb.inv.LocalInvDistributionRecord;
import com.ejb.inv.LocalInvDistributionRecordHome;
import com.ejb.inv.LocalInvItem;
import com.ejb.inv.LocalInvItemHome;
import com.ejb.inv.LocalInvItemLocation;
import com.ejb.inv.LocalInvItemLocationHome;
import com.ejb.inv.LocalInvUnitOfMeasure;
import com.ejb.inv.LocalInvUnitOfMeasureConversion;
import com.ejb.inv.LocalInvUnitOfMeasureConversionHome;
import com.util.AbstractSessionBean;
import com.util.AdModApprovalQueueDetails;
import com.util.CmAdjustmentDetails;
import com.util.Debug;
import com.util.EJBCommon;
import com.util.EJBHomeFactory;

/**
 * @ejb:bean name="ArApprovalControllerEJB"
 *           display-name="Used for approving ar documents"
 *           type="Stateless"
 *           view-type="remote"
 *           jndi-name="ejb/ArApprovalControllerEJB"
 *
 * @ejb:interface remote-class="com.ejb.txn.ArApprovalController"
 *                extends="javax.ejb.EJBObject"
 *
 * @ejb:home remote-class="com.ejb.txn.ArApprovalControllerHome"
 *           extends="javax.ejb.EJBHome"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="aruser"
 *                        role-link="aruserlink"
 *
 * @ejb:permission role-name="aruser"
 *
*/

public class ArApprovalControllerBean extends AbstractSessionBean {


	
	
	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public String getApprovalStatus(String USR_DEPT, String USR_NM, String USR_DESC, String AQ_DCMNT, Integer AQ_DCMNT_CODE, String AQ_DCMNT_NMBR, Date AQ_DT, Integer AQ_AD_BRNCH, Integer AQ_AD_CMPNY) throws 
	 GlobalNoApprovalRequesterFoundException,
	 GlobalNoApprovalApproverFoundException{
	
	
		Debug.print("ArApprovalControllerBean getApprovalStatus");
		
		
		
		LocalAdApprovalQueueHome adApprovalQueueHome = null;
		LocalAdPreferenceHome adPreferenceHome = null;
		LocalAdAmountLimitHome adAmountLimitHome  = null;
		LocalAdApprovalUserHome adApprovalUserHome  = null;

		
		
		// Initialize EJB Home
		
		try {
			
			adApprovalQueueHome = (LocalAdApprovalQueueHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdApprovalQueueHome.JNDI_NAME, LocalAdApprovalQueueHome.class); 
		
			adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
					lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);  
					
			adAmountLimitHome = (LocalAdAmountLimitHome)EJBHomeFactory.
					lookUpLocalHome(LocalAdAmountLimitHome.JNDI_NAME, LocalAdAmountLimitHome.class); 
			
			adApprovalUserHome = (LocalAdApprovalUserHome)EJBHomeFactory.
					lookUpLocalHome(LocalAdApprovalUserHome.JNDI_NAME, LocalAdApprovalUserHome.class);                   
					
		} catch (NamingException ex) {
			
			throw new EJBException(ex.getMessage());
			
		}
		
		String PR_APPRVL_STATUS = null;
		Double ABS_TOTAL_AMOUNT = 0d;
		
		try{
			LocalAdAmountLimit adAmountLimit = null;
			
			try {

				adAmountLimit = adAmountLimitHome.findByCalDeptAdcTypeAndAuTypeAndUsrName(USR_DEPT, AQ_DCMNT, "REQUESTER", USR_NM, AQ_AD_CMPNY);

			} catch (FinderException ex) {

				throw new GlobalNoApprovalRequesterFoundException();

			}
			
			
			if (!(ABS_TOTAL_AMOUNT <= adAmountLimit.getCalAmountLimit())) {

				PR_APPRVL_STATUS = "N/A";

			} else {
				
				// for approval, create approval queue
				Collection adAmountLimits = adAmountLimitHome.findByAdcTypeAndGreaterThanCalDeptAndCalAmountLimit(USR_DEPT,AQ_DCMNT, adAmountLimit.getCalAmountLimit(), AQ_AD_CMPNY);

				
				if (adAmountLimits.isEmpty()) {

					Collection adApprovalUsers = adApprovalUserHome.findByCalDeptAndAuTypeAndCalCode(USR_DEPT,"APPROVER", adAmountLimit.getCalCode(), AQ_AD_CMPNY);

					if (adApprovalUsers.isEmpty()) {

						throw new GlobalNoApprovalApproverFoundException();

					}

					Iterator j = adApprovalUsers.iterator();

					while (j.hasNext()) {

						LocalAdApprovalUser adApprovalUser = (LocalAdApprovalUser)j.next();

						LocalAdApprovalQueue adApprovalQueue = adApprovalQueueHome.create(USR_DEPT,adApprovalUser.getAuLevel(), EJBCommon.FALSE, EJBCommon.incrementStringNumber(adApprovalUser.getAuLevel()),AQ_DCMNT, AQ_DCMNT_CODE,
								AQ_DCMNT_NMBR, AQ_DT, null, adAmountLimit.getCalAndOr(), adApprovalUser.getAuOr(), EJBCommon.FALSE, USR_DESC,
										AQ_AD_BRNCH, AQ_AD_CMPNY);

						adApprovalUser.getAdUser().addAdApprovalQueue(adApprovalQueue);

						if(adApprovalUser.getAuLevel().equals("LEVEL 1")) {
							adApprovalQueue.setAqForApproval(EJBCommon.TRUE);
							
							/*
							 *
							 email for future use
							if((!adApprovalQueue.getAdUser().getUsrEmailAddress().equals("")) && (adPreference.getPrfAdEnableEmailNotification() == EJBCommon.TRUE)) {
								
								
								this.sendEmail(adApprovalQueue, AD_CMPNY);
							}
							*/

						}

					}

				} else {
					

					boolean isApprovalUsersFound = false;

					Iterator i = adAmountLimits.iterator();

					while (i.hasNext()) {

						LocalAdAmountLimit adNextAmountLimit = (LocalAdAmountLimit)i.next();

						if (ABS_TOTAL_AMOUNT <= adNextAmountLimit.getCalAmountLimit()) {

							Collection adApprovalUsers = adApprovalUserHome.findByCalDeptAndAuTypeAndCalCode(USR_DEPT,"APPROVER", adAmountLimit.getCalCode(), AQ_AD_CMPNY);

							Iterator j = adApprovalUsers.iterator();

							while (j.hasNext()) {

								isApprovalUsersFound = true;

								LocalAdApprovalUser adApprovalUser = (LocalAdApprovalUser)j.next();

								LocalAdApprovalQueue adApprovalQueue = adApprovalQueueHome.create(USR_DEPT, adApprovalUser.getAuLevel(), EJBCommon.FALSE, EJBCommon.incrementStringNumber(adApprovalUser.getAuLevel()), AQ_DCMNT, AQ_DCMNT_CODE,
										AQ_DCMNT_NMBR, AQ_DT , null, adAmountLimit.getCalAndOr(), adApprovalUser.getAuOr(), EJBCommon.FALSE, USR_DESC,
												AQ_AD_BRNCH, AQ_AD_CMPNY);

								adApprovalUser.getAdUser().addAdApprovalQueue(adApprovalQueue);

								if(adApprovalUser.getAuLevel().equals("LEVEL 1")) {
									adApprovalQueue.setAqForApproval(EJBCommon.TRUE);
									
									/*
									 *
									 email for future use
									if((!adApprovalQueue.getAdUser().getUsrEmailAddress().equals("")) && (adPreference.getPrfAdEnableEmailNotification() == EJBCommon.TRUE))  {
										
										this.sendEmail(adApprovalQueue, AD_CMPNY);
									}
									
									*/

								}
							}

							break;

						} else if (!i.hasNext()) {

							Collection adApprovalUsers = adApprovalUserHome.findByCalDeptAndAuTypeAndCalCode(USR_DEPT,"APPROVER", adNextAmountLimit.getCalCode(), AQ_AD_CMPNY);

							Iterator j = adApprovalUsers.iterator();

							while (j.hasNext()) {

								isApprovalUsersFound = true;

								LocalAdApprovalUser adApprovalUser = (LocalAdApprovalUser)j.next();

								LocalAdApprovalQueue adApprovalQueue = adApprovalQueueHome.create(USR_DEPT, adApprovalUser.getAuLevel(), EJBCommon.FALSE, EJBCommon.incrementStringNumber(adApprovalUser.getAuLevel()) ,AQ_DCMNT, AQ_DCMNT_CODE,
										AQ_DCMNT_NMBR, AQ_DT, null, adNextAmountLimit.getCalAndOr(), adApprovalUser.getAuOr(), EJBCommon.FALSE, USR_DESC,
												AQ_AD_BRNCH, AQ_AD_CMPNY);

								adApprovalUser.getAdUser().addAdApprovalQueue(adApprovalQueue);

								if(adApprovalUser.getAuLevel().equals("LEVEL 1")) {
									adApprovalQueue.setAqForApproval(EJBCommon.TRUE);
									/*
									if((!adApprovalQueue.getAdUser().getUsrEmailAddress().equals("")) && (adPreference.getPrfAdEnableEmailNotification() == EJBCommon.TRUE)) {

											this.sendEmail(adApprovalQueue, AD_CMPNY);
									
										
									}
									*/

								}
							}

							break;

						}

						adAmountLimit = adNextAmountLimit;

					}

					if (!isApprovalUsersFound) {

						throw new GlobalNoApprovalApproverFoundException();

					}

				
				}
	
				PR_APPRVL_STATUS = "PENDING";
			
			}
			
			
			
			
			
			return PR_APPRVL_STATUS;
		}catch(GlobalNoApprovalApproverFoundException ex){
				throw new GlobalNoApprovalApproverFoundException();
		
			
			
		}catch(GlobalNoApprovalRequesterFoundException ex){
				throw new GlobalNoApprovalRequesterFoundException();
		
		
		
		}catch(Exception ex){
				Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());
		
		}
	
	
	}
	
	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public ArrayList getArApproverList(String AQ_DCMNT, Integer AQ_DCMNT_CODE, Integer AQ_AD_CMPNY){
	
		Debug.print("ArApprovalControllerBean getApApproverList");
		
		
		LocalAdApprovalQueueHome adApprovalQueueHome = null;
		LocalAdPreferenceHome adPreferenceHome = null;
		LocalAdAmountLimitHome adAmountLimitHome  = null;
		LocalAdApprovalUserHome adApprovalUserHome  = null;

		
		
		// Initialize EJB Home
		
		try {
			
			adApprovalQueueHome = (LocalAdApprovalQueueHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdApprovalQueueHome.JNDI_NAME, LocalAdApprovalQueueHome.class); 
		
			adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
					lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);  
					
			adAmountLimitHome = (LocalAdAmountLimitHome)EJBHomeFactory.
					lookUpLocalHome(LocalAdAmountLimitHome.JNDI_NAME, LocalAdAmountLimitHome.class); 
			
			adApprovalUserHome = (LocalAdApprovalUserHome)EJBHomeFactory.
					lookUpLocalHome(LocalAdApprovalUserHome.JNDI_NAME, LocalAdApprovalUserHome.class);                   
					
		} catch (NamingException ex) {
			
			throw new EJBException(ex.getMessage());
			
		}
		
		ArrayList list = new ArrayList();
		try{

			Collection adApprovalQueues = adApprovalQueueHome.findByAqDocumentAndAqDocumentCodeAll(AQ_DCMNT, AQ_DCMNT_CODE, AQ_AD_CMPNY);
	
			System.out.println("adApprovalQueues="+adApprovalQueues.size());
	
	
			Iterator i = adApprovalQueues.iterator();
	
			while (i.hasNext()) {
				LocalAdApprovalQueue adApprovalQueue = (LocalAdApprovalQueue)i.next();
	
				AdModApprovalQueueDetails adModApprovalQueueDetails = new AdModApprovalQueueDetails();
				adModApprovalQueueDetails.setAqApprovedDate(adApprovalQueue.getAqApprovedDate());
				adModApprovalQueueDetails.setAqApproverName(adApprovalQueue.getAdUser().getUsrDescription());
				adModApprovalQueueDetails.setAqStatus(adApprovalQueue.getAqApproved() == EJBCommon.TRUE ? adApprovalQueue.getAqLevel()+ " APPROVED" : adApprovalQueue.getAqLevel()+" PENDING");
	
				list.add(adModApprovalQueueDetails);
			}
			
			return list;
		
		}catch(Exception ex){
			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());
		
		}

	}
	
	
	
	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public ArrayList getAdApprovalNotifiedUsersByAqCode(Integer AQ_CODE, Integer AD_CMPNY) {
		
		Debug.print("ArApprovalControllerBean getAdApprovalNotifiedUsersByAqCode");
		
		LocalAdApprovalQueueHome adApprovalQueueHome = null;
		LocalArInvoiceHome arInvoiceHome = null;
		LocalArSalesOrderHome arSalesOrderHome = null;
		LocalArReceiptHome arReceiptHome = null;

		LocalAdPreferenceHome adPreferenceHome = null;
		
		ArrayList list = new ArrayList();
		
		
		// Initialize EJB Home
		
		try {
			
			adApprovalQueueHome = (LocalAdApprovalQueueHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdApprovalQueueHome.JNDI_NAME, LocalAdApprovalQueueHome.class); 
				
			arInvoiceHome = (LocalArInvoiceHome)EJBHomeFactory.
				lookUpLocalHome(LocalArInvoiceHome.JNDI_NAME, LocalArInvoiceHome.class); 
				
			arSalesOrderHome = (LocalArSalesOrderHome)EJBHomeFactory.
				lookUpLocalHome(LocalArSalesOrderHome.JNDI_NAME, LocalArSalesOrderHome.class); 
				
			arReceiptHome = (LocalArReceiptHome)EJBHomeFactory.
				lookUpLocalHome(LocalArReceiptHome.JNDI_NAME, LocalArReceiptHome.class); 
			
			 
			adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
					lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);      
					
		} catch (NamingException ex) {
			
			throw new EJBException(ex.getMessage());
			
		}
		
		try {

			LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);
			LocalAdApprovalQueue adApprovalQueue = null;
			try {
				System.out.println("approval queue code is: "  + AQ_CODE);
				adApprovalQueue = adApprovalQueueHome.findByPrimaryKey(AQ_CODE);


				if(adApprovalQueue.getAqApproved()==EJBCommon.TRUE) {
					list.add("DOCUMENT POSTED");
				}

			}catch (Exception e) {

				System.out.println("rejected");
				list.add("DOCUMENT REJECTED");
				return list;
			}

		
			


			if(adApprovalQueue.getAqDocument().equals("AR INVOICE") || adApprovalQueue.getAqDocument().equals("AR CREDIT MEMO")){

				LocalArInvoice arInvoice = arInvoiceHome.findByPrimaryKey(adApprovalQueue.getAqDocumentCode());
				if (arInvoice.getInvPosted() == EJBCommon.TRUE) {

	                list.add("DOCUMENT POSTED");
	                return list;

	            }
			}

			if(adApprovalQueue.getAqDocument().equals("AR SALES ORDER")){

				LocalArSalesOrder arSalesOrder = arSalesOrderHome.findByPrimaryKey(adApprovalQueue.getAqDocumentCode());
				if (arSalesOrder.getSoPosted() == EJBCommon.TRUE) {

	                list.add("DOCUMENT POSTED");
	                return list;

	            }

			}

			if(adApprovalQueue.getAqDocument().equals("AR RECEIPT")){

				LocalArReceipt arReceipt = arReceiptHome.findByPrimaryKey(adApprovalQueue.getAqDocumentCode());
				if (arReceipt.getRctPosted() == EJBCommon.TRUE) {

	                list.add("DOCUMENT POSTED");
	                return list;

	            }

			}

			String messageUser = "";
			LocalAdApprovalQueue adNextApprovalQueue = adApprovalQueueHome.findByAqDeptAndAqLevelAndAqDocumentCode(
					adApprovalQueue.getAqDepartment(), adApprovalQueue.getAqNextLevel(), adApprovalQueue.getAqDocumentCode(), AD_CMPNY);
		//	list.add(adNextApprovalQueue.getAqLevel() +" APPROVER - "+ adNextApprovalQueue.getAdUser().getUsrDescription());

			messageUser = adNextApprovalQueue.getAqLevel() +" APPROVER - "+ adNextApprovalQueue.getAdUser().getUsrDescription();
			System.out.println("adNextApprovalQueue.getAdUser().getUsrDescription()="+adNextApprovalQueue.getAdUser().getUsrDescription());

			try {


				adNextApprovalQueue.setAqForApproval(EJBCommon.TRUE);
				

			} catch (Exception e) {
				System.out.println("got problem");
				messageUser += " [Email Notification Not Sent. Cannot connect host or no internet connection.]";
			}

			list.add(messageUser);
			return list;

		} catch (Exception ex) {

			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());

		}
		
	}
	
    







    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList getAdAqByAqDocumentAndUserName(HashMap criteria,
        String USR_NM, Integer OFFSET, Integer LIMIT, String ORDER_BY, Integer AD_BRNCH, Integer AD_CMPNY)
        throws GlobalNoRecordFoundException {

        Debug.print("ArApprovalControllerBean getAdAqByAqDocumentAndUserName");

        LocalAdApprovalQueueHome adApprovalQueueHome = null;
        LocalArInvoiceHome arInvoiceHome = null;
        LocalArReceiptHome arReceiptHome = null;
        LocalArSalesOrderHome arSalesOrderHome = null;
        LocalArCustomerHome arCustomerHome = null;

        ArrayList list = new ArrayList();

        //initialized EJB Home

        try {

        	arCustomerHome = (LocalArCustomerHome)EJBHomeFactory.
                     lookUpLocalHome(LocalArCustomerHome.JNDI_NAME, LocalArCustomerHome.class);
            adApprovalQueueHome = (LocalAdApprovalQueueHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdApprovalQueueHome.JNDI_NAME, LocalAdApprovalQueueHome.class);
            arInvoiceHome = (LocalArInvoiceHome)EJBHomeFactory.
                lookUpLocalHome(LocalArInvoiceHome.JNDI_NAME, LocalArInvoiceHome.class);
            arReceiptHome = (LocalArReceiptHome)EJBHomeFactory.
                lookUpLocalHome(LocalArReceiptHome.JNDI_NAME, LocalArReceiptHome.class);
            arSalesOrderHome = (LocalArSalesOrderHome)EJBHomeFactory.
            	lookUpLocalHome(LocalArSalesOrderHome.JNDI_NAME, LocalArSalesOrderHome.class);

        } catch (NamingException ex) 	{

            throw new EJBException(ex.getMessage());

        }

		try {

			StringBuffer jbossQl = new StringBuffer();
        	jbossQl.append("SELECT OBJECT(aq) FROM AdApprovalQueue aq ");

        	boolean firstArgument = true;
        	short ctr = 0;
        	int criteriaSize = criteria.size() + 2;

        	Object obj[];

        	// Allocate the size of the object parameter

        	obj = new Object[criteriaSize];

        	if (criteria.containsKey("document")) {

        		if (!firstArgument) {
        			jbossQl.append("AND ");
        		} else {
        			firstArgument = false;
        			jbossQl.append("WHERE ");
        		}
        		jbossQl.append("aq.aqDocument=?" + (ctr+1) + " ");
        		obj[ctr] = (String)criteria.get("document");
        		ctr++;
        	}

        	if (criteria.containsKey("dateFrom")) {

        		if (!firstArgument) {
        			jbossQl.append("AND ");
        		} else {
        			firstArgument = false;
        			jbossQl.append("WHERE ");
        		}
        		jbossQl.append("aq.aqDate>=?" + (ctr+1) + " ");
        		obj[ctr] = (Date)criteria.get("dateFrom");
        		ctr++;
        	}

        	if (criteria.containsKey("dateTo")) {

        		if (!firstArgument) {
        			jbossQl.append("AND ");
        		} else {
        			firstArgument = false;
        			jbossQl.append("WHERE ");
        		}
        		jbossQl.append("aq.aqDate<=?" + (ctr+1) + " ");
        		obj[ctr] = (Date)criteria.get("dateTo");
        		ctr++;

        	}

        	if (criteria.containsKey("documentNumberFrom")) {

        		if (!firstArgument) {
        			jbossQl.append("AND ");
        		} else {
        			firstArgument = false;
        			jbossQl.append("WHERE ");
        		}
        		jbossQl.append("aq.aqDocumentNumber>=?" + (ctr+1) + " ");
        		obj[ctr] = (String)criteria.get("documentNumberFrom");
        		ctr++;
        	}

        	if (criteria.containsKey("documentNumberTo")) {

        		if (!firstArgument) {
        			jbossQl.append("AND ");
        		} else {
        			firstArgument = false;
        			jbossQl.append("WHERE ");
        		}
        		jbossQl.append("aq.aqDocumentNumber<=?" + (ctr+1) + " ");
        		obj[ctr] = (String)criteria.get("documentNumberTo");
        		ctr++;

        	}

        	if (!firstArgument) {

        		jbossQl.append("AND ");

        	} else {

        		firstArgument = false;
        		jbossQl.append("WHERE ");

        	}

        	//jbossQl.append("aq.aqAdBranch=" + AD_BRNCH + " AND aq.aqAdCompany=" + AD_CMPNY + " " + "AND aq.adUser.usrName='" + USR_NM + "'");
		    jbossQl.append("aq.aqForApproval = 1 AND aq.aqApproved = 0 AND aq.aqAdCompany=" + AD_CMPNY + " " + "AND aq.adUser.usrName='" + USR_NM + "'");
		


        	String orderBy = null;

        	if (ORDER_BY.equals("DOCUMENT NUMBER")) {

        		orderBy = "aq.aqDocumentNumber";

        	} else if (ORDER_BY.equals("DATE")) {

        		orderBy = "aq.aqDate";

        	}

        	if (orderBy != null) {

        		jbossQl.append("ORDER BY " + orderBy + ", aq.aqDate");

        	} else {

        		jbossQl.append("ORDER BY aq.aqDate");

        	}

        	jbossQl.append(" OFFSET ?" + (ctr + 1));
        	obj[ctr] = OFFSET;
        	ctr++;

        	jbossQl.append(" LIMIT ?" + (ctr + 1));
        	obj[ctr] = LIMIT;
        	ctr++;

		    Collection adApprovalQueues = adApprovalQueueHome.getAqByCriteria(jbossQl.toString(), obj);

		    String AQ_DCMNT = (String)criteria.get("document");

		    if (adApprovalQueues.size() == 0)
        		throw new GlobalNoRecordFoundException();

		    Iterator i = adApprovalQueues.iterator();

		    while (i.hasNext()) {

		    	LocalAdApprovalQueue adApprovalQueue = (LocalAdApprovalQueue)i.next();

		    	AdModApprovalQueueDetails details = new AdModApprovalQueueDetails();

				System.out.println("aq code is " + adApprovalQueue.getAqCode());
				details.setAqCode(adApprovalQueue.getAqCode());
		    	details.setAqDocument(adApprovalQueue.getAqDocument());
		    	details.setAqDocumentCode(adApprovalQueue.getAqDocumentCode());

		    	if (AQ_DCMNT.equals("AR INVOICE") || AQ_DCMNT.equals("AR CREDIT MEMO")) {

		    		LocalArInvoice arInvoice = arInvoiceHome.findByPrimaryKey(adApprovalQueue.getAqDocumentCode());

		    		details.setAqDate(arInvoice.getInvDate());
		    		details.setAqDocumentNumber(arInvoice.getInvNumber());
		    		details.setAqAmount(arInvoice.getInvAmountDue());
		    		details.setAqCustomerCode(arInvoice.getArCustomer().getCstCustomerCode());
		    		details.setAqCustomerName(arInvoice.getArCustomer().getCstName());

		    		if(!arInvoice.getArInvoiceLineItems().isEmpty()) {

		    			details.setAqType("ITEMS");

		    		} else {

		    			details.setAqType("MEMO LINES");

		    		}

		    	} else if (AQ_DCMNT.equals("AR RECEIPT")){

		    		LocalArReceipt arReceipt = arReceiptHome.findByPrimaryKey(adApprovalQueue.getAqDocumentCode());

		    		details.setAqDate(arReceipt.getRctDate());
		    		details.setAqDocumentNumber(arReceipt.getRctNumber());
		    		details.setAqAmount(arReceipt.getRctAmount());
		    		details.setAqCustomerCode(arReceipt.getArCustomer().getCstCustomerCode());
		    		details.setAqCustomerName(arReceipt.getArCustomer().getCstName());
		    		details.setAqDocumentType(arReceipt.getRctType());

		    		if(!arReceipt.getArInvoiceLineItems().isEmpty()) {

		    			details.setAqType("ITEMS");

		    		}else {

		    			details.setAqType("MEMO LINES");

		    		}


		    	} else if (AQ_DCMNT.equals("AR CUSTOMER")) {


        			LocalArCustomer arCustomer = arCustomerHome.findByPrimaryKey(adApprovalQueue.getAqDocumentCode());

        			details.setAqDate(arCustomer.getCstDateLastModified());
        			details.setAqDocumentNumber(arCustomer.getCstName());


        			//details.setAqAmount(0d);
        			details.setAqCustomerCode(arCustomer.getCstCustomerCode());
        			//details.setAqDocumentType(arCustomer.getCstCustomerBatch());

        			details.setAqReferenceNumber(null);
        			details.setAqCustomerName(arCustomer.getCstCustomerCode());

        		} else {

		    		LocalArSalesOrder arSalesOrder = arSalesOrderHome.findByPrimaryKey(adApprovalQueue.getAqDocumentCode());
		    		System.out.println(" here it is: " + adApprovalQueue.getAqDocumentCode());
		    		details.setAqDate(arSalesOrder.getSoDate());
		    		details.setAqDocumentNumber(arSalesOrder.getSoDocumentNumber());
		    		details.setAqCustomerCode(arSalesOrder.getArCustomer().getCstCustomerCode());
		    		details.setAqCustomerName(arSalesOrder.getArCustomer().getCstName());
	    			details.setAqType("ITEMS");

	    			Collection arSalesOrderLines = arSalesOrder.getArSalesOrderLines();
	    			Iterator j = arSalesOrderLines.iterator();

	    			double SO_TTL_AMOUNT = 0;

	    			while (j.hasNext()) {

	    				LocalArSalesOrderLine arSalesOrderLine = (LocalArSalesOrderLine) j.next();

	    				SO_TTL_AMOUNT = SO_TTL_AMOUNT + arSalesOrderLine.getSolAmount();


	    			}

		    		details.setAqAmount(SO_TTL_AMOUNT);

		    	}

		    	list.add(details);

		    }

		    return list;

		} catch (GlobalNoRecordFoundException ex) {

			throw ex;

		} catch (Exception ex) {


		  	ex.printStackTrace();
		  	throw new EJBException(ex.getMessage());

		}

    }

    /**
     * @ejb:interface-method view-type="remote"
     **/
    public void executeArApproval(String AQ_DCMNT, Integer AQ_DCMNT_CODE, String USR_NM, boolean isApproved, String MEMO, String RSN_FR_RJCTN,
    		Integer AD_BRNCH, Integer AD_CMPNY) throws
        GlobalRecordAlreadyDeletedException,
        GlobalTransactionAlreadyPostedException,
		GlobalTransactionAlreadyVoidException,
		GlobalTransactionAlreadyVoidPostedException,
		GlJREffectiveDateNoPeriodExistException,
		GlJREffectiveDatePeriodClosedException,
		GlobalJournalNotBalanceException,
		GlobalInventoryDateException,
		GlobalBranchAccountNumberInvalidException,
		AdPRFCoaGlVarianceAccountNotFoundException {

        Debug.print("ArApprovalControllerBean executeArApproval");

        LocalAdApprovalQueueHome adApprovalQueueHome = null;
        LocalArInvoiceHome arInvoiceHome = null;
        LocalArReceiptHome arReceiptHome = null;
        LocalAdPreferenceHome adPreferenceHome = null;
        LocalInvCostingHome invCostingHome = null;
        LocalInvItemLocationHome invItemLocationHome = null;
        LocalArSalesOrderHome arSalesOrderHome = null;
        LocalArCustomerHome arCustomerHome = null;
        LocalAdUserHome adUserHome = null;

        LocalAdApprovalQueue adApprovalQueue = null;

        // Initialize EJB Home

        try {

        	adUserHome = (LocalAdUserHome)EJBHomeFactory.
                    lookUpLocalHome(LocalAdUserHome.JNDI_NAME, LocalAdUserHome.class);
        	arCustomerHome = (LocalArCustomerHome)EJBHomeFactory.
                    lookUpLocalHome(LocalArCustomerHome.JNDI_NAME, LocalArCustomerHome.class);
            adApprovalQueueHome = (LocalAdApprovalQueueHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdApprovalQueueHome.JNDI_NAME, LocalAdApprovalQueueHome.class);
            arInvoiceHome = (LocalArInvoiceHome)EJBHomeFactory.
                lookUpLocalHome(LocalArInvoiceHome.JNDI_NAME, LocalArInvoiceHome.class);
            arReceiptHome = (LocalArReceiptHome)EJBHomeFactory.
                lookUpLocalHome(LocalArReceiptHome.JNDI_NAME, LocalArReceiptHome.class);
            adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);
            invCostingHome = (LocalInvCostingHome)EJBHomeFactory.
            	lookUpLocalHome(LocalInvCostingHome.JNDI_NAME, LocalInvCostingHome.class);
            invItemLocationHome = (LocalInvItemLocationHome)EJBHomeFactory.
            	lookUpLocalHome(LocalInvItemLocationHome.JNDI_NAME, LocalInvItemLocationHome.class);
            arSalesOrderHome =(LocalArSalesOrderHome)EJBHomeFactory.
        		lookUpLocalHome(LocalArSalesOrderHome.JNDI_NAME, LocalArSalesOrderHome.class);

        } catch (NamingException ex) {

            throw new EJBException(ex.getMessage());

        }

        try {


        	// validate if approval queue is already deleted

        	try {

        		adApprovalQueue = adApprovalQueueHome.findByAqDocumentAndAqDocumentCodeAndUsrName(AQ_DCMNT, AQ_DCMNT_CODE, USR_NM, AD_CMPNY);

        	} catch (FinderException ex) {

        		throw new GlobalRecordAlreadyDeletedException();

        	}

        	// approve/reject

        	Collection adApprovalQueues = adApprovalQueueHome.findByAqDocumentAndAqDocumentCode(AQ_DCMNT, AQ_DCMNT_CODE, AD_CMPNY);

        	Collection adApprovalQueuesDesc = adApprovalQueueHome.findByAqDocumentAndAqDocumentCodeLessThanDesc(AQ_DCMNT, AQ_DCMNT_CODE, adApprovalQueue.getAqCode(), AD_CMPNY);
        	Collection adApprovalQueuesAsc = adApprovalQueueHome.findByAqDocumentAndAqDocumentCodeGreaterThanAsc(AQ_DCMNT, AQ_DCMNT_CODE, adApprovalQueue.getAqCode(), AD_CMPNY);

			Collection adAllApprovalQueues = adApprovalQueueHome.findAllByAqDocumentAndAqDocumentCode(AQ_DCMNT, AQ_DCMNT_CODE, AD_CMPNY);

        	LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);

        	if (AQ_DCMNT.equals("AR INVOICE") || AQ_DCMNT.equals("AR CREDIT MEMO")) {

        		LocalArInvoice arInvoice = arInvoiceHome.findByPrimaryKey(AQ_DCMNT_CODE);

        		Collection arInvoiceLineItems = arInvoice.getArInvoiceLineItems();

        		Iterator i = arInvoiceLineItems.iterator();

        		while (i.hasNext()) {

        			LocalArInvoiceLineItem arInvoiceLineItem = (LocalArInvoiceLineItem) i.next();

	                // start date validation
        			System.out.println("adPreference.getPrfArAllowPriorDate(): " + adPreference.getPrfArAllowPriorDate());
        			System.out.println("EJBCommon.FALSE: " + EJBCommon.FALSE);
        			if (adPreference.getPrfArAllowPriorDate() == EJBCommon.FALSE) {

		  	    		Collection invNegTxnCosting = invCostingHome.findNegTxnByGreaterThanCstDateAndIiNameAndLocName(
		  	    			arInvoice.getInvDate(), arInvoiceLineItem.getInvItemLocation().getInvItem().getIiName(),
		  	    			arInvoiceLineItem.getInvItemLocation().getInvLocation().getLocName(), AD_BRNCH, AD_CMPNY);
		  	    		if(!invNegTxnCosting.isEmpty()) throw new GlobalInventoryDateException(arInvoiceLineItem.getInvItemLocation().getInvItem().getIiName());
        			}

	  	    		if(arInvoiceLineItem.getIliEnableAutoBuild() == EJBCommon.TRUE && arInvoiceLineItem.getInvItemLocation().getInvItem().getIiClass().equals("Assembly") &&
	  	    				adPreference.getPrfArAllowPriorDate() == EJBCommon.FALSE) {

	  	    			Collection invBillOfMaterials = arInvoiceLineItem.getInvItemLocation().getInvItem().getInvBillOfMaterials();

	  	    			Iterator j = invBillOfMaterials.iterator();

	  	    			while (j.hasNext()) {

	  	    				LocalInvBillOfMaterial invBillOfMaterial = (LocalInvBillOfMaterial) j.next();

	  	    				LocalInvItemLocation invIlRawMaterial = null;

	  	    				try {

	  	    					invIlRawMaterial = invItemLocationHome.findByLocNameAndIiName(
	  	    							invBillOfMaterial.getBomLocName(), invBillOfMaterial.getBomIiName(), AD_CMPNY);

	  	    				} catch(FinderException ex) {

	  	    					throw new GlobalInvItemLocationNotFoundException(
	  	    						arInvoiceLineItem.getInvItemLocation().getInvItem().getIiName() +
	  	    							" - Raw Mat. (" +invBillOfMaterial.getBomIiName() + ")");

	  	    				}

	  	    				// start date validation

	  	    				Collection invIlRawMatNegTxnCosting = invCostingHome.findNegTxnByGreaterThanCstDateAndIiNameAndLocName(
	  	    						arInvoice.getInvDate(), invIlRawMaterial.getInvItem().getIiName(),
									invIlRawMaterial.getInvLocation().getLocName(),	AD_BRNCH, AD_CMPNY);
	  	    				if(!invIlRawMatNegTxnCosting.isEmpty()) throw new GlobalInventoryDateException(
	  	    						arInvoiceLineItem.getInvItemLocation().getInvItem().getIiName() + " - Raw Mat. (" + invIlRawMaterial.getInvItem().getIiName() + ")");

	  	    			}

	  	    		}

        		}

        		if (isApproved) {

        			if (adApprovalQueue.getAqAndOr().equals("AND")) {

        				if (adApprovalQueues.size() == 1) {

        					arInvoice.setInvApprovalStatus("APPROVED");
        					arInvoice.setInvApprovedRejectedBy(USR_NM);
        					arInvoice.setInvDateApprovedRejected(EJBCommon.getGcCurrentDateWoTime().getTime());

        					if (adPreference.getPrfArGlPostingType().equals("AUTO-POST UPON APPROVAL")) {

        		        		this.executeArInvPost(arInvoice.getInvCode(), USR_NM, AD_BRNCH, AD_CMPNY);

        		        	}

        		        	adApprovalQueue.setAqApproved(EJBCommon.TRUE);
        					adApprovalQueue.setAqApprovedDate(new java.util.Date());
//        					adApprovalQueue.entityRemove();
        					//em.remove(adApprovalQueue);

        				} else {

        					// looping up
        					i = adApprovalQueuesDesc.iterator();

            				while (i.hasNext()) {

            					LocalAdApprovalQueue adRemoveApprovalQueue = (LocalAdApprovalQueue)i.next();

            					if (adRemoveApprovalQueue.getAqUserOr() == (byte)1) {

        							i.remove();
//                					adRemoveApprovalQueue.entityRemove();

		        		        	adRemoveApprovalQueue.setAqApproved(EJBCommon.TRUE);
		        					adRemoveApprovalQueue.setAqApprovedDate(new java.util.Date());
                					//em.remove(adRemoveApprovalQueue);

        						} else {

        							break;

        						}

            				}

            				// looping down
            				if(adApprovalQueue.getAqUserOr() == (byte)1) {

            					boolean first = true;

            					i = adApprovalQueuesAsc.iterator();

                				while (i.hasNext()) {

                					LocalAdApprovalQueue adRemoveApprovalQueue = (LocalAdApprovalQueue)i.next();

                					if (first || adRemoveApprovalQueue.getAqUserOr() == (byte)1) {

            							i.remove();
//                    					adRemoveApprovalQueue.entityRemove();
										adRemoveApprovalQueue.setAqApproved(EJBCommon.TRUE);
		        						adRemoveApprovalQueue.setAqApprovedDate(new java.util.Date());
                    				//	em.remove(adRemoveApprovalQueue);

                    					if(first)
                    						first = false;

            						} else {

            							break;

            						}

                				}

            				}

//            				adApprovalQueue.entityRemove();
							adApprovalQueue.setAqApproved(EJBCommon.TRUE);
        					adApprovalQueue.setAqApprovedDate(new java.util.Date());
            			//	em.remove(adApprovalQueue);

        					Collection adRemoveApprovalQueues = adApprovalQueueHome.findByAqDocumentAndAqDocumentCode(AQ_DCMNT, AQ_DCMNT_CODE, AD_CMPNY);

        					if(adRemoveApprovalQueues.size() == 0) {

        						arInvoice.setInvApprovalStatus("APPROVED");
            					arInvoice.setInvApprovedRejectedBy(USR_NM);
            					arInvoice.setInvDateApprovedRejected(EJBCommon.getGcCurrentDateWoTime().getTime());
						
            					if (adPreference.getPrfArGlPostingType().equals("AUTO-POST UPON APPROVAL")) {

            		        		this.executeArInvPost(arInvoice.getInvCode(), USR_NM, AD_BRNCH, AD_CMPNY);

            		        	}

            	        	}

        				}

        			} else if (adApprovalQueue.getAqAndOr().equals("OR")) {

        				arInvoice.setInvApprovalStatus("APPROVED");
        				arInvoice.setInvApprovedRejectedBy(USR_NM);
        				arInvoice.setInvDateApprovedRejected(EJBCommon.getGcCurrentDateWoTime().getTime());

        				i = adApprovalQueues.iterator();

        				while (i.hasNext()) {

        					LocalAdApprovalQueue adRemoveApprovalQueue = (LocalAdApprovalQueue)i.next();
							adRemoveApprovalQueue.setAqApproved(EJBCommon.TRUE);
    						adRemoveApprovalQueue.setAqApprovedDate(new java.util.Date());
        					i.remove();
//        					adRemoveApprovalQueue.entityRemove();
        				//	em.remove(adRemoveApprovalQueue);

        				}

        				if (adPreference.getPrfArGlPostingType().equals("AUTO-POST UPON APPROVAL")) {

    		        		this.executeArInvPost(arInvoice.getInvCode(), USR_NM, AD_BRNCH, AD_CMPNY);

    		        		// Set SO Lock if Fully Served

    		        		LocalArSalesOrder arExistingSalesOrder = null;

    		        		try{

    		        			if(arInvoice.getArSalesOrderInvoiceLines().size()>0)
    		        				arExistingSalesOrder = ((LocalArSalesOrderLine)arInvoice.getArSalesOrderInvoiceLines().toArray()[0]).getArSalesOrder();

    		        			Iterator solIter = arExistingSalesOrder.getArSalesOrderLines().iterator();

    		        			boolean isOpenSO = false;

    		                    while(solIter.hasNext()) {

    		                    	LocalArSalesOrderLine arSalesOrderLine = (LocalArSalesOrderLine) solIter.next();

    		                    	Iterator soInvLnIter = arSalesOrderLine.getArSalesOrderInvoiceLines().iterator();
    		                        double QUANTITY_SOLD = 0d;

    		                        while(soInvLnIter.hasNext()) {

    		                            LocalArSalesOrderInvoiceLine arSalesOrderInvoiceLine = (LocalArSalesOrderInvoiceLine) soInvLnIter.next();

    		                            if (arSalesOrderInvoiceLine.getArInvoice().getInvPosted() == EJBCommon.TRUE) {

    		                                QUANTITY_SOLD += arSalesOrderInvoiceLine.getSilQuantityDelivered();

    		                            }

    		                        }

    		                        double TOTAL_REMAINING_QTY = arSalesOrderLine.getSolQuantity() - QUANTITY_SOLD;

    		                        if(TOTAL_REMAINING_QTY > 0) {
    		                        	isOpenSO = true;
    		                        	break;
    		                        }

    		                    }

    		                    if(isOpenSO)
    		                    	arExistingSalesOrder.setSoLock(EJBCommon.FALSE);
    		                    else
    		                    	arExistingSalesOrder.setSoLock(EJBCommon.TRUE);


    		        		}catch(Exception ex){
    		        			// Do Nothing
    		        		}
    		        	}


        			}

        		} else if (!isApproved) {

        			arInvoice.setInvApprovalStatus(null);
    				arInvoice.setInvApprovedRejectedBy(USR_NM);
    				arInvoice.setInvReasonForRejection(RSN_FR_RJCTN);
    				arInvoice.setInvDateApprovedRejected(EJBCommon.getGcCurrentDateWoTime().getTime());

    				i = adAllApprovalQueues.iterator();

    				while (i.hasNext()) {

    					LocalAdApprovalQueue adRemoveApprovalQueue = (LocalAdApprovalQueue)i.next();

    					i.remove();
   						adRemoveApprovalQueue.remove();
    					
    				}

        		}


        	}else if (AQ_DCMNT.equals("AR CUSTOMER")) {

        		LocalArCustomer arCustomer = arCustomerHome.findByPrimaryKey(AQ_DCMNT_CODE);

        		if (isApproved) {
        			if (adApprovalQueue.getAqAndOr().equals("AND")) {
        				if (adApprovalQueues.size() == 1) {
        					arCustomer.setCstApprovalStatus("APPROVED");

            				LocalAdUser approver=null;
            				try {
            					approver=adUserHome.findByUsrName(USR_NM,AD_CMPNY);
            					approver.setUsrPurchaseOrderApprovalCounter(EJBCommon.incrementStringNumber(approver.getUsrPurchaseOrderApprovalCounter()));
            				}
            				catch (Exception ex) {
    							Debug.printStackTrace(ex);
    						}

        					arCustomer.setCstApprovedRejectedBy(USR_NM);
        					arCustomer.setCstDateApprovedRejected(EJBCommon.getGcCurrentDateWoTime().getTime());
        					arCustomer.setCstPosted(EJBCommon.TRUE);
        					//arCustomer.setCstEnable(EJBCommon.TRUE);

        					arCustomer.setCstPostedBy(arCustomer.getCstLastModifiedBy());
        					arCustomer.setCstDatePosted(EJBCommon.getGcCurrentDateWoTime().getTime());
        					
        					adApprovalQueue.setAqApproved(EJBCommon.TRUE);
    						adApprovalQueue.setAqApprovedDate(new java.util.Date());
//        					adApprovalQueue.entityRemove();
        				//	em.remove(adApprovalQueue);

        				} else {
        					// looping up
        					Iterator i = adApprovalQueuesDesc.iterator();
        					while (i.hasNext()) {
        						LocalAdApprovalQueue adRemoveApprovalQueue = (LocalAdApprovalQueue)i.next();
        						if (adRemoveApprovalQueue.getAqUserOr() == (byte)1) {
        							i.remove();
//        							adRemoveApprovalQueue.entityRemove();
									adRemoveApprovalQueue.setAqApproved(EJBCommon.TRUE);
    								adRemoveApprovalQueue.setAqApprovedDate(new java.util.Date());
        						//	em.remove(adRemoveApprovalQueue);
        						} else {
        							break;
        						}
        					}

        					// looping down
        					if(adApprovalQueue.getAqUserOr() == (byte)1) {
        						boolean first = true;
        						i = adApprovalQueuesAsc.iterator();
        						while (i.hasNext()) {
        							LocalAdApprovalQueue adRemoveApprovalQueue = (LocalAdApprovalQueue)i.next();
        							if (first || adRemoveApprovalQueue.getAqUserOr() == (byte)1) {
        								i.remove();
//        								adRemoveApprovalQueue.entityRemove();
        								
        								adRemoveApprovalQueue.setAqApproved(EJBCommon.TRUE);
    									adRemoveApprovalQueue.setAqApprovedDate(new java.util.Date());
        								//em.remove(adRemoveApprovalQueue);
        								if(first)
        									first = false;
        							} else {
        								break;
        							}
        						}
        					}

//        					adApprovalQueue.entityRemove();
 							adApprovalQueue.setAqApproved(EJBCommon.TRUE);
							adApprovalQueue.setAqApprovedDate(new java.util.Date());
        				//	em.remove(adApprovalQueue);
        					Collection adRemoveApprovalQueues = adApprovalQueueHome.findByAqDocumentAndAqDocumentCode(AQ_DCMNT, AQ_DCMNT_CODE, AD_CMPNY);
        					if(adRemoveApprovalQueues.size() == 0) {
        						arCustomer.setCstApprovalStatus("APPROVED");

                				LocalAdUser approver=null;
                				try {
                					approver=adUserHome.findByUsrName(USR_NM,AD_CMPNY);
                					approver.setUsrPurchaseOrderApprovalCounter(EJBCommon.incrementStringNumber(approver.getUsrPurchaseOrderApprovalCounter()));
                				}
                				catch (Exception ex) {
        							Debug.printStackTrace(ex);
        						}

        						arCustomer.setCstApprovedRejectedBy(USR_NM);
        						arCustomer.setCstDateApprovedRejected(EJBCommon.getGcCurrentDateWoTime().getTime());
        						arCustomer.setCstPosted(EJBCommon.TRUE);
        						//arCustomer.setCstEnable(EJBCommon.TRUE);
        						arCustomer.setCstPostedBy(arCustomer.getCstLastModifiedBy());
        						arCustomer.setCstDatePosted(EJBCommon.getGcCurrentDateWoTime().getTime());
        					}
        				}
        			} else if (adApprovalQueue.getAqAndOr().equals("OR")) {

        				arCustomer.setCstApprovalStatus("APPROVED");

        				LocalAdUser approver=null;
        				try {
        					approver=adUserHome.findByUsrName(USR_NM,AD_CMPNY);
        					approver.setUsrPurchaseOrderApprovalCounter(EJBCommon.incrementStringNumber(approver.getUsrPurchaseOrderApprovalCounter()));
        				}
        				catch (Exception ex) {
							Debug.printStackTrace(ex);
						}

        				arCustomer.setCstApprovedRejectedBy(USR_NM);
        				arCustomer.setCstDateApprovedRejected(EJBCommon.getGcCurrentDateWoTime().getTime());

        				Iterator i = adApprovalQueues.iterator();

        				while (i.hasNext()) {
        					LocalAdApprovalQueue adRemoveApprovalQueue = (LocalAdApprovalQueue)i.next();
        					i.remove();
//        					adRemoveApprovalQueue.entityRemove();
							adRemoveApprovalQueue.setAqApproved(EJBCommon.TRUE);
							adRemoveApprovalQueue.setAqApprovedDate(new java.util.Date());
        					//em.remove(adRemoveApprovalQueue);

        				}
        				arCustomer.setCstPosted(EJBCommon.TRUE);
        				//arCustomer.setCstEnable(EJBCommon.TRUE);
        				arCustomer.setCstPostedBy(arCustomer.getCstLastModifiedBy());
        				arCustomer.setCstDatePosted(EJBCommon.getGcCurrentDateWoTime().getTime());
        			}
        		} else if (!isApproved) {
        			arCustomer.setCstApprovalStatus(null);
        			arCustomer.setCstReasonForRejection(RSN_FR_RJCTN);
        			arCustomer.setCstApprovedRejectedBy(USR_NM);
        			arCustomer.setCstDateApprovedRejected(EJBCommon.getGcCurrentDateWoTime().getTime());
        			Iterator i = adAllApprovalQueues.iterator();

        			while (i.hasNext()) {
        				LocalAdApprovalQueue adRemoveApprovalQueue = (LocalAdApprovalQueue)i.next();
        				i.remove();
        				adRemoveApprovalQueue.remove();
        				
        			}
        		}


        	} else if (AQ_DCMNT.equals("AR RECEIPT")){

        		LocalArReceipt arReceipt = arReceiptHome.findByPrimaryKey(AQ_DCMNT_CODE);

        		Collection arInvoiceLineItems = arReceipt.getArInvoiceLineItems();

        		Iterator i = arInvoiceLineItems.iterator();

        		while (i.hasNext()) {

        			LocalArInvoiceLineItem arInvoiceLineItem = (LocalArInvoiceLineItem) i.next();

	                // start date validation

        			if (adPreference.getPrfArAllowPriorDate() == EJBCommon.FALSE) {
        				Collection invNegTxnCosting = invCostingHome.findNegTxnByGreaterThanCstDateAndIiNameAndLocName(
        						arReceipt.getRctDate(), arInvoiceLineItem.getInvItemLocation().getInvItem().getIiName(),
        						arInvoiceLineItem.getInvItemLocation().getInvLocation().getLocName(), AD_BRNCH, AD_CMPNY);
        				if(!invNegTxnCosting.isEmpty()) throw new GlobalInventoryDateException(arInvoiceLineItem.getInvItemLocation().getInvItem().getIiName());
        			}
	  	    		if(arInvoiceLineItem.getIliEnableAutoBuild() == EJBCommon.TRUE && arInvoiceLineItem.getInvItemLocation().getInvItem().getIiClass().equals("Assembly")) {

	  	    			Collection invBillOfMaterials = arInvoiceLineItem.getInvItemLocation().getInvItem().getInvBillOfMaterials();

	  	    			Iterator j = invBillOfMaterials.iterator();

	  	    			while (j.hasNext()) {

	  	    				LocalInvBillOfMaterial invBillOfMaterial = (LocalInvBillOfMaterial) j.next();

	  	    				LocalInvItemLocation invIlRawMaterial = null;

	  	    				try {

	  	    					invIlRawMaterial = invItemLocationHome.findByLocNameAndIiName(
	  	    							invBillOfMaterial.getBomLocName(), invBillOfMaterial.getBomIiName(), AD_CMPNY);

	  	    				} catch(FinderException ex) {

	  	    					throw new GlobalInvItemLocationNotFoundException(
	  	    						arInvoiceLineItem.getInvItemLocation().getInvItem().getIiName() +
	  	    							" - Raw Mat. (" +invBillOfMaterial.getBomIiName() + ")");

	  	    				}

	  	    				// start date validation

	  	    				Collection invIlRawMatNegTxnCosting = invCostingHome.findNegTxnByGreaterThanCstDateAndIiNameAndLocName(
	  	    						arReceipt.getRctDate(), invIlRawMaterial.getInvItem().getIiName(),
									invIlRawMaterial.getInvLocation().getLocName(),	AD_BRNCH, AD_CMPNY);
	  	    				if(!invIlRawMatNegTxnCosting.isEmpty()) throw new GlobalInventoryDateException(
	  	    						arInvoiceLineItem.getInvItemLocation().getInvItem().getIiName() + " - Raw Mat. (" + invIlRawMaterial.getInvItem().getIiName() + ")");

	  	    			}

	  	    		}

        		}

        		if (isApproved) {

        			if (adApprovalQueue.getAqAndOr().equals("AND")) {

        				if (adApprovalQueues.size() == 1) {

        					if (arReceipt.getRctVoid() == EJBCommon.FALSE) {

        						arReceipt.setRctApprovalStatus("APPROVED");

        					} else {

        						arReceipt.setRctVoidApprovalStatus("APPROVED");

        					}

							adApprovalQueue.setAqApproved(EJBCommon.TRUE);
							adApprovalQueue.setAqApprovedDate(new java.util.Date());
							
        					arReceipt.setRctApprovedRejectedBy(USR_NM);
        					arReceipt.setRctDateApprovedRejected(EJBCommon.getGcCurrentDateWoTime().getTime());

        					if (adPreference.getPrfArGlPostingType().equals("AUTO-POST UPON APPROVAL")) {

        						//create cm adjustment for inserting advance payment
        			      		  if(arReceipt.getRctEnableAdvancePayment()!=0){

        			      			  this.postAdvncPymntByCmAdj(arReceipt ,arReceipt.getAdBankAccount().getBaName(), AD_BRNCH, AD_CMPNY);

        			      		  }

        		        		this.executeArRctPost(arReceipt.getRctCode(), USR_NM, AD_BRNCH, AD_CMPNY);

        		        	}

//        					adApprovalQueue.entityRemove();
        				//	em.remove(adApprovalQueue);

        				} else {

        					// looping up
        					i = adApprovalQueuesDesc.iterator();

            				while (i.hasNext()) {

            					LocalAdApprovalQueue adRemoveApprovalQueue = (LocalAdApprovalQueue)i.next();

            					if (adRemoveApprovalQueue.getAqUserOr() == (byte)1) {

        							i.remove();
//                					adRemoveApprovalQueue.entityRemove();
									adRemoveApprovalQueue.setAqApproved(EJBCommon.TRUE);
									adRemoveApprovalQueue.setAqApprovedDate(new java.util.Date());
                				//	em.remove(adRemoveApprovalQueue);

        						} else {

        							break;

        						}

            				}

            				// looping down
            				if(adApprovalQueue.getAqUserOr() == (byte)1) {

            					boolean first = true;

            					i = adApprovalQueuesAsc.iterator();

                				while (i.hasNext()) {

                					LocalAdApprovalQueue adRemoveApprovalQueue = (LocalAdApprovalQueue)i.next();

                					if (first || adRemoveApprovalQueue.getAqUserOr() == (byte)1) {

            							i.remove();
//                    					adRemoveApprovalQueue.entityRemove();
										adRemoveApprovalQueue.setAqApproved(EJBCommon.TRUE);
										adRemoveApprovalQueue.setAqApprovedDate(new java.util.Date());
                    					//em.remove(adRemoveApprovalQueue);

                    					if(first)
                    						first = false;

            						} else {

            							break;

            						}

                				}

            				}
							adApprovalQueue.setAqApproved(EJBCommon.TRUE);
							adApprovalQueue.setAqApprovedDate(new java.util.Date());
//            				adApprovalQueue.entityRemove();
            			//	em.remove(adApprovalQueue);

        					Collection adRemoveApprovalQueues = adApprovalQueueHome.findByAqDocumentAndAqDocumentCode(AQ_DCMNT, AQ_DCMNT_CODE, AD_CMPNY);

        					if(adRemoveApprovalQueues.size() == 0) {

            					if (arReceipt.getRctVoid() == EJBCommon.FALSE) {

            						arReceipt.setRctApprovalStatus("APPROVED");

            					} else {

            						arReceipt.setRctVoidApprovalStatus("APPROVED");

            					}

            					arReceipt.setRctApprovedRejectedBy(USR_NM);
            					arReceipt.setRctDateApprovedRejected(EJBCommon.getGcCurrentDateWoTime().getTime());

            					if (adPreference.getPrfArGlPostingType().equals("AUTO-POST UPON APPROVAL")) {

            						//create cm adjustment for inserting advance payment
            			      		  if(arReceipt.getRctEnableAdvancePayment()!=0){

            			      			  this.postAdvncPymntByCmAdj(arReceipt ,arReceipt.getAdBankAccount().getBaName(), AD_BRNCH, AD_CMPNY);

            			      		  }


            		        		this.executeArRctPost(arReceipt.getRctCode(), USR_NM, AD_BRNCH, AD_CMPNY);

            		        	}

        					}

        				}

        			} else if (adApprovalQueue.getAqAndOr().equals("OR")) {

        				if (arReceipt.getRctVoid() == EJBCommon.FALSE) {

    						arReceipt.setRctApprovalStatus("APPROVED");

    					} else {

    						arReceipt.setRctVoidApprovalStatus("APPROVED");

    					}

        				arReceipt.setRctApprovedRejectedBy(USR_NM);
        				arReceipt.setRctDateApprovedRejected(EJBCommon.getGcCurrentDateWoTime().getTime());

        				i = adApprovalQueues.iterator();

        				while (i.hasNext()) {

        					LocalAdApprovalQueue adRemoveApprovalQueue = (LocalAdApprovalQueue)i.next();

        					i.remove();
//        					adRemoveApprovalQueue.entityRemove();
							adRemoveApprovalQueue.setAqApproved(EJBCommon.TRUE);
							adRemoveApprovalQueue.setAqApprovedDate(new java.util.Date());
        					//em.remove(adRemoveApprovalQueue);

        				}

        				if (adPreference.getPrfArGlPostingType().equals("AUTO-POST UPON APPROVAL")) {


        					//create cm adjustment for inserting advance payment
        		      		  if(arReceipt.getRctEnableAdvancePayment()!=0){

        		      			  this.postAdvncPymntByCmAdj(arReceipt ,arReceipt.getAdBankAccount().getBaName(), AD_BRNCH, AD_CMPNY);

        		      		  }

    		        		this.executeArRctPost(arReceipt.getRctCode(), USR_NM, AD_BRNCH, AD_CMPNY);

    		        	}


        			}

        		} else if (!isApproved) {

        			if (arReceipt.getRctVoid() == EJBCommon.FALSE) {

						arReceipt.setRctApprovalStatus(null);

					} else {

						arReceipt.setRctVoidApprovalStatus(null);

					}

        			arReceipt.setRctApprovedRejectedBy(USR_NM);
        			arReceipt.setRctDateApprovedRejected(EJBCommon.getGcCurrentDateWoTime().getTime());
        			arReceipt.setRctReasonForRejection(RSN_FR_RJCTN);

    				i = adAllApprovalQueues.iterator();

    				while (i.hasNext()) {

    					LocalAdApprovalQueue adRemoveApprovalQueue = (LocalAdApprovalQueue)i.next();

    					i.remove();
   					adRemoveApprovalQueue.remove();
    			

    				}

        		}

        	} else {

        		LocalArSalesOrder arSalesOrder = arSalesOrderHome.findByPrimaryKey(AQ_DCMNT_CODE);

        		if (isApproved) {

        			if (adApprovalQueue.getAqAndOr().equals("AND")) {

        				if (adApprovalQueues.size() == 1) {

        					arSalesOrder.setSoMemo(arSalesOrder.getSoMemo() + "\n" + MEMO);
        					arSalesOrder.setSoApprovalStatus("APPROVED");
        					arSalesOrder.setSoApprovedRejectedBy(USR_NM);
        					arSalesOrder.setSoDateApprovedRejected(EJBCommon.getGcCurrentDateWoTime().getTime());
        					arSalesOrder.setSoPosted(EJBCommon.TRUE);
        					arSalesOrder.setSoPostedBy(arSalesOrder.getSoLastModifiedBy());
        					arSalesOrder.setSoDatePosted(EJBCommon.getGcCurrentDateWoTime().getTime());
        					arSalesOrder.setSoOrderStatus("Good");
//        					adApprovalQueue.entityRemove();
							adApprovalQueue.setAqApproved(EJBCommon.TRUE);
							adApprovalQueue.setAqApprovedDate(new java.util.Date());
        				//	em.remove(adApprovalQueue);

        				} else {

        					// looping up
        					Iterator i = adApprovalQueuesDesc.iterator();

            				while (i.hasNext()) {

            					LocalAdApprovalQueue adRemoveApprovalQueue = (LocalAdApprovalQueue)i.next();

            					if (adRemoveApprovalQueue.getAqUserOr() == (byte)1) {

        							i.remove();
//                					adRemoveApprovalQueue.entityRemove();
									adRemoveApprovalQueue.setAqApproved(EJBCommon.TRUE);
        							adRemoveApprovalQueue.setAqApprovedDate(new java.util.Date());
                				//	em.remove(adRemoveApprovalQueue);

        						} else {

        							break;

        						}

            				}

            				// looping down
            				if(adApprovalQueue.getAqUserOr() == (byte)1) {

            					boolean first = true;

            					i = adApprovalQueuesAsc.iterator();

                				while (i.hasNext()) {

                					LocalAdApprovalQueue adRemoveApprovalQueue = (LocalAdApprovalQueue)i.next();

                					if (first || adRemoveApprovalQueue.getAqUserOr() == (byte)1) {

            							i.remove();
//                    					adRemoveApprovalQueue.entityRemove();
										adRemoveApprovalQueue.setAqApproved(EJBCommon.TRUE);
        								adRemoveApprovalQueue.setAqApprovedDate(new java.util.Date());
                    				//	em.remove(adRemoveApprovalQueue);

                    					if(first)
                    						first = false;

            						} else {

            							break;

            						}

                				}

            				}

//            				adApprovalQueue.entityRemove();
							adApprovalQueue.setAqApproved(EJBCommon.TRUE);
							adApprovalQueue.setAqApprovedDate(new java.util.Date());
            				//em.remove(adApprovalQueue);

        					Collection adRemoveApprovalQueues = adApprovalQueueHome.findByAqDocumentAndAqDocumentCode(AQ_DCMNT, AQ_DCMNT_CODE, AD_CMPNY);

        					if(adRemoveApprovalQueues.size() == 0) {

        						arSalesOrder.setSoMemo(arSalesOrder.getSoMemo() + "\n" + MEMO);
            					arSalesOrder.setSoApprovalStatus("APPROVED");
            					arSalesOrder.setSoApprovedRejectedBy(USR_NM);
            					arSalesOrder.setSoDateApprovedRejected(EJBCommon.getGcCurrentDateWoTime().getTime());
            					arSalesOrder.setSoPosted(EJBCommon.TRUE);
            					arSalesOrder.setSoPostedBy(arSalesOrder.getSoLastModifiedBy());
            					arSalesOrder.setSoDatePosted(EJBCommon.getGcCurrentDateWoTime().getTime());
            					arSalesOrder.setSoOrderStatus("Good");
            	        	}

        				}

        			} else if (adApprovalQueue.getAqAndOr().equals("OR")) {

        				arSalesOrder.setSoMemo(arSalesOrder.getSoMemo() + "\n" + MEMO);
    					arSalesOrder.setSoApprovalStatus("APPROVED");
        				arSalesOrder.setSoApprovedRejectedBy(USR_NM);
        				arSalesOrder.setSoDateApprovedRejected(EJBCommon.getGcCurrentDateWoTime().getTime());
        				arSalesOrder.setSoOrderStatus("Good");
        				Iterator i = adApprovalQueues.iterator();

        				while (i.hasNext()) {

        					LocalAdApprovalQueue adRemoveApprovalQueue = (LocalAdApprovalQueue)i.next();

        					i.remove();
//        					adRemoveApprovalQueue.entityRemove();
							adRemoveApprovalQueue.setAqApproved(EJBCommon.TRUE);
    						adRemoveApprovalQueue.setAqApprovedDate(new java.util.Date());
        				//	em.remove(adRemoveApprovalQueue);

        				}

    					arSalesOrder.setSoPosted(EJBCommon.TRUE);
    					arSalesOrder.setSoPostedBy(arSalesOrder.getSoLastModifiedBy());
    					arSalesOrder.setSoDatePosted(EJBCommon.getGcCurrentDateWoTime().getTime());


        			}

        		} else if (!isApproved) {

					arSalesOrder.setSoApprovalStatus(null);
        			arSalesOrder.setSoApprovedRejectedBy(USR_NM);
        			arSalesOrder.setSoDateApprovedRejected(EJBCommon.getGcCurrentDateWoTime().getTime());
        			arSalesOrder.setSoReasonForRejection(RSN_FR_RJCTN);
        			//arSalesOrder.setSoOrderStatus("Good");

        			Iterator i = adAllApprovalQueues.iterator();

        			while (i.hasNext()) {

        				LocalAdApprovalQueue adRemoveApprovalQueue = (LocalAdApprovalQueue)i.next();

        				i.remove();
        				adRemoveApprovalQueue.remove();
        			

        			}

        		}

        	}

        } catch (GlobalRecordAlreadyDeletedException ex) {

        	getSessionContext().setRollbackOnly();
        	throw ex;

        } catch (GlobalTransactionAlreadyPostedException ex) {

        	getSessionContext().setRollbackOnly();
        	throw ex;

        } catch (GlobalTransactionAlreadyVoidException ex) {

        	getSessionContext().setRollbackOnly();
        	throw ex;

        } catch (GlobalTransactionAlreadyVoidPostedException ex) {

        	getSessionContext().setRollbackOnly();
        	throw ex;

        } catch (GlJREffectiveDateNoPeriodExistException ex) {

        	getSessionContext().setRollbackOnly();
        	throw ex;

        } catch (GlJREffectiveDatePeriodClosedException ex) {

        	getSessionContext().setRollbackOnly();
        	throw ex;

        } catch (GlobalJournalNotBalanceException ex) {

        	getSessionContext().setRollbackOnly();
        	throw ex;

        } catch (GlobalInventoryDateException ex) {

        	getSessionContext().setRollbackOnly();
        	throw ex;

        } catch (GlobalBranchAccountNumberInvalidException ex) {

        	getSessionContext().setRollbackOnly();
        	throw ex;

        } catch (AdPRFCoaGlVarianceAccountNotFoundException ex){

            getSessionContext().setRollbackOnly();
            throw ex;

    	} catch (Exception ex) {

    		Debug.printStackTrace(ex);
    		getSessionContext().setRollbackOnly();
    		throw new EJBException(ex.getMessage());

    	}

    }


   /**
    * @ejb:interface-method view-type="remote"
    * @jboss:method-attributes read-only="true"
    **/
    public short getGlFcPrecisionUnit(Integer AD_CMPNY) {

       Debug.print("ArApprovalControllerBean getGlFcPrecisionUnit");


       LocalAdCompanyHome adCompanyHome = null;


       // Initialize EJB Home

       try {

           adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
              lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);

       } catch (NamingException ex) {

           throw new EJBException(ex.getMessage());

       }

       try {

         LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);

         return  adCompany.getGlFunctionalCurrency().getFcPrecision();

       } catch (Exception ex) {

       	 Debug.printStackTrace(ex);
         throw new EJBException(ex.getMessage());

       }

    }

    // private methods

    private void postAdvncPymntByCmAdj(LocalArReceipt arReceipt, String BA_NM, Integer AD_BRNCH, Integer AD_CMPNY) throws
    Exception{

	 Debug.print("CmAdjustmentEntryControllerBean saveCmAdjEntry");

     LocalCmAdjustmentHome cmAdjustmentHome = null;
     LocalAdBankAccountHome adBankAccountHome = null;
     LocalArCustomerHome arCustomerHome = null;
     LocalAdApprovalHome adApprovalHome = null;
     LocalAdAmountLimitHome adAmountLimitHome = null;
     LocalAdApprovalUserHome adApprovalUserHome = null;
     LocalAdApprovalQueueHome adApprovalQueueHome = null;
     LocalGlFunctionalCurrencyHome glFunctionalCurrencyHome = null;
     LocalGlFunctionalCurrencyRateHome glFunctionalCurrencyRateHome = null;
     LocalAdPreferenceHome adPreferenceHome = null;
     LocalAdDocumentSequenceAssignmentHome adDocumentSequenceAssignmentHome = null;
     LocalAdBranchDocumentSequenceAssignmentHome adBranchDocumentSequenceAssignmentHome = null;
     LocalAdCompanyHome adCompanyHome = null;

     //initialized EJB Home

     try {

         cmAdjustmentHome = (LocalCmAdjustmentHome)EJBHomeFactory.
             lookUpLocalHome(LocalCmAdjustmentHome.JNDI_NAME, LocalCmAdjustmentHome.class);
         adBankAccountHome = (LocalAdBankAccountHome)EJBHomeFactory.
             lookUpLocalHome(LocalAdBankAccountHome.JNDI_NAME, LocalAdBankAccountHome.class);
         arCustomerHome = (LocalArCustomerHome)EJBHomeFactory.
                 lookUpLocalHome(LocalArCustomerHome.JNDI_NAME, LocalArCustomerHome.class);
         adApprovalHome = (LocalAdApprovalHome)EJBHomeFactory.
             lookUpLocalHome(LocalAdApprovalHome.JNDI_NAME, LocalAdApprovalHome.class);
         adAmountLimitHome = (LocalAdAmountLimitHome)EJBHomeFactory.
             lookUpLocalHome(LocalAdAmountLimitHome.JNDI_NAME, LocalAdAmountLimitHome.class);
         adApprovalUserHome = (LocalAdApprovalUserHome)EJBHomeFactory.
             lookUpLocalHome(LocalAdApprovalUserHome.JNDI_NAME, LocalAdApprovalUserHome.class);
         adApprovalQueueHome = (LocalAdApprovalQueueHome)EJBHomeFactory.
             lookUpLocalHome(LocalAdApprovalQueueHome.JNDI_NAME, LocalAdApprovalQueueHome.class);
         glFunctionalCurrencyHome = (LocalGlFunctionalCurrencyHome)EJBHomeFactory.
             lookUpLocalHome(LocalGlFunctionalCurrencyHome.JNDI_NAME, LocalGlFunctionalCurrencyHome.class);
         glFunctionalCurrencyRateHome = (LocalGlFunctionalCurrencyRateHome)EJBHomeFactory.
             lookUpLocalHome(LocalGlFunctionalCurrencyRateHome.JNDI_NAME, LocalGlFunctionalCurrencyRateHome.class);
         adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
             lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);
         adDocumentSequenceAssignmentHome = (LocalAdDocumentSequenceAssignmentHome)EJBHomeFactory.
         	lookUpLocalHome(LocalAdDocumentSequenceAssignmentHome.JNDI_NAME, LocalAdDocumentSequenceAssignmentHome.class);
			adBranchDocumentSequenceAssignmentHome = (LocalAdBranchDocumentSequenceAssignmentHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdBranchDocumentSequenceAssignmentHome.JNDI_NAME, LocalAdBranchDocumentSequenceAssignmentHome.class);
			adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME,LocalAdCompanyHome.class);

     } catch (NamingException ex) {

         throw new EJBException(ex.getMessage());

     }

     CmAdjustmentDetails details = new CmAdjustmentDetails();


     details.setAdjCode(null);
     details.setAdjType("ADVANCE");
     details.setAdjDate(arReceipt.getRctDate());
     details.setAdjDocumentNumber("");
     details.setAdjReferenceNumber(arReceipt.getRctNumber());
     details.setAdjAmount(arReceipt.getRctExcessAmount());
     details.setAdjConversionDate(arReceipt.getRctConversionDate());
     details.setAdjConversionRate(arReceipt.getRctConversionRate());
     details.setAdjMemo("");
     details.setAdjVoid(arReceipt.getRctVoid());

     details.setAdjCreatedBy(arReceipt.getRctCreatedBy());
     details.setAdjDateCreated(new java.util.Date());


     details.setAdjLastModifiedBy(arReceipt.getRctLastModifiedBy());
     details.setAdjDateLastModified(new java.util.Date());



	 ///validate document number is unique

     LocalAdBranchDocumentSequenceAssignment adBranchDocumentSequenceAssignment = null;
		LocalAdDocumentSequenceAssignment adDocumentSequenceAssignment = null;

		try {

			adDocumentSequenceAssignment = adDocumentSequenceAssignmentHome.findByDcName("CM ADJUSTMENT", AD_CMPNY);

		} catch (FinderException ex) {

		}

		try {

			adBranchDocumentSequenceAssignment = adBranchDocumentSequenceAssignmentHome.findBdsByDsaCodeAndBrCode(adDocumentSequenceAssignment.getDsaCode(), AD_BRNCH, AD_CMPNY);

		} catch (FinderException ex) {

		}

		LocalCmAdjustment cmExistingAdjustment = null;

		try {

		    cmExistingAdjustment = cmAdjustmentHome.findByAdjDocumentNumberAndBrCode(details.getAdjDocumentNumber(), AD_BRNCH, AD_CMPNY);

 	} catch (FinderException ex) {
 	}

     if (cmExistingAdjustment != null) {

     	throw new GlobalDocumentNumberNotUniqueException();

     }

        if (adDocumentSequenceAssignment.getAdDocumentSequence().getDsNumberingType() == 'A' &&
            (details.getAdjDocumentNumber() == null || details.getAdjDocumentNumber().trim().length() == 0)) {

            while (true) {

            	if (adBranchDocumentSequenceAssignment == null || adBranchDocumentSequenceAssignment.getBdsNextSequence() == null) {

	            	try {

	            		cmAdjustmentHome.findByAdjDocumentNumberAndBrCode(adDocumentSequenceAssignment.getDsaNextSequence(), AD_BRNCH, AD_CMPNY);
	            		adDocumentSequenceAssignment.setDsaNextSequence(EJBCommon.incrementStringNumber(adDocumentSequenceAssignment.getDsaNextSequence()));

	            	} catch (FinderException ex) {

	            		details.setAdjDocumentNumber(adDocumentSequenceAssignment.getDsaNextSequence());
	            		adDocumentSequenceAssignment.setDsaNextSequence(EJBCommon.incrementStringNumber(adDocumentSequenceAssignment.getDsaNextSequence()));
			            break;

	            	}

            	} else {

            		try {

	            		cmAdjustmentHome.findByAdjDocumentNumberAndBrCode(adBranchDocumentSequenceAssignment.getBdsNextSequence(), AD_BRNCH, AD_CMPNY);
	            		adBranchDocumentSequenceAssignment.setBdsNextSequence(EJBCommon.incrementStringNumber(adBranchDocumentSequenceAssignment.getBdsNextSequence()));

	            	} catch (FinderException ex) {

	            		details.setAdjDocumentNumber(adBranchDocumentSequenceAssignment.getBdsNextSequence());
	            		adBranchDocumentSequenceAssignment.setBdsNextSequence(EJBCommon.incrementStringNumber(adBranchDocumentSequenceAssignment.getBdsNextSequence()));
			            break;

	            	}

            	}

            }

        }


     // validate if conversion date exists

        try {

      	    if (details.getAdjConversionDate() != null) {

      	    	LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
      	    	LocalGlFunctionalCurrency glValidateFunctionalCurrency = glFunctionalCurrencyHome.findByFcName(
      	    			adCompany.getGlFunctionalCurrency().getFcName(), AD_CMPNY);

 				if (!glValidateFunctionalCurrency.getFcName().equals("USD")) {

 					LocalGlFunctionalCurrencyRate glFunctionalCurrencyRate =
 						glFunctionalCurrencyRateHome.findByFcCodeAndDate(glValidateFunctionalCurrency.getFcCode(),
 								details.getAdjConversionDate(), AD_CMPNY);

 				} else if (!adCompany.getGlFunctionalCurrency().getFcName().equals("USD")){

 					LocalGlFunctionalCurrencyRate glFunctionalCurrencyRate =
 						glFunctionalCurrencyRateHome.findByFcCodeAndDate(
 							adCompany.getGlFunctionalCurrency().getFcCode(), details.getAdjConversionDate(), AD_CMPNY);

 				}

	        }

        } catch (FinderException ex) {

        	 throw new GlobalConversionDateNotExistException();

        }


        LocalCmAdjustment cmAdjustment = null;
        try{

        	 cmAdjustment = cmAdjustmentHome.create(
	            	 details.getAdjType(), details.getAdjDate(),
	            	 details.getAdjDocumentNumber(), details.getAdjReferenceNumber(), details.getAdjCheckNumber(), details.getAdjAmount(), 0d,
	            	 details.getAdjConversionDate(), details.getAdjConversionRate(),
	             	 details.getAdjMemo(),null, EJBCommon.FALSE, EJBCommon.FALSE,
	             	 EJBCommon.FALSE, 0d, null,

	             	 EJBCommon.FALSE, null,
	             	 null, EJBCommon.FALSE, details.getAdjCreatedBy(), details.getAdjDateCreated(),
       	    	details.getAdjLastModifiedBy(), details.getAdjDateLastModified(), null, null, null, null, null, AD_BRNCH, AD_CMPNY);



	        LocalAdBankAccount adBankAccount = adBankAccountHome.findByBaName(BA_NM, AD_CMPNY);
	        adBankAccount.addCmAdjustment(cmAdjustment);


	        // remove all distribution records
        	System.out.println("flag 3");
	  	    Collection cmDistributionRecords = cmAdjustment.getCmDistributionRecords();

	  	    Iterator i = cmDistributionRecords.iterator();

	  	    while (i.hasNext()) {

	  	   	    LocalCmDistributionRecord cmDistributionRecord = (LocalCmDistributionRecord)i.next();

	  	  	    i.remove();

	  	  	    cmDistributionRecord.remove();

	  	    }

	  	   LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);



	    	this.addCmDrEntry(cmAdjustment.getCmDrNextLine(),
    	  	    "ADJUSTMENT", EJBCommon.TRUE, cmAdjustment.getAdjAmount(),
    	  	    adBankAccount.getBaCoaGlAdjustmentAccount(), cmAdjustment, AD_BRNCH, AD_CMPNY);

	    	this.addCmDrEntry(cmAdjustment.getCmDrNextLine(),
    	  	    "CASH", EJBCommon.FALSE, cmAdjustment.getAdjAmount(),
    	  	adPreference.getPrfArGlCoaCustomerDepositAccount(), cmAdjustment, AD_BRNCH, AD_CMPNY);

	    	LocalArCustomer arCustomer = arCustomerHome.findByCstCustomerCode(arReceipt.getArCustomer().getCstCustomerCode(),AD_CMPNY);

  	    	arCustomer.addCmAdjustment(cmAdjustment);

  	    	String ADJ_APPRVL_STATUS = "N/A";


  	    	this.executeCmAdjPost(cmAdjustment.getAdjCode(), cmAdjustment.getAdjLastModifiedBy(), AD_BRNCH, AD_CMPNY);
        }catch(Exception ex){
        	throw  ex;
        }
}



private void executeCmAdjPost(Integer ADJ_CODE, String USR_NM, Integer AD_BRNCH, Integer AD_CMPNY) throws
    GlobalRecordAlreadyDeletedException,
	GlobalTransactionAlreadyPostedException,
	GlobalTransactionAlreadyVoidException,
	GlJREffectiveDateNoPeriodExistException,
	GlJREffectiveDatePeriodClosedException,
	GlobalJournalNotBalanceException {

    Debug.print("CmAdjustmentEntryControllerBean executeCmAdjPost");

    LocalCmAdjustmentHome cmAdjustmentHome = null;
    LocalAdCompanyHome adCompanyHome = null;
    LocalAdPreferenceHome adPreferenceHome = null;
    LocalGlSetOfBookHome glSetOfBookHome = null;
    LocalGlAccountingCalendarValueHome glAccountingCalendarValueHome = null;
    LocalGlJournalHome glJournalHome = null;
    LocalGlJournalBatchHome glJournalBatchHome = null;
    LocalGlSuspenseAccountHome glSuspenseAccountHome = null;
    LocalGlJournalLineHome glJournalLineHome = null;
    LocalGlJournalSourceHome glJournalSourceHome = null;
    LocalGlJournalCategoryHome glJournalCategoryHome = null;
    LocalGlFunctionalCurrencyHome glFunctionalCurrencyHome = null;
    LocalCmDistributionRecordHome cmDistributionRecordHome = null;
    LocalGlChartOfAccountHome glChartOfAccountHome = null;
    LocalAdBankAccountHome adBankAccountHome = null;
    LocalAdBankAccountBalanceHome adBankAccountBalanceHome = null;
    LocalGlForexLedgerHome glForexLedgerHome = null;
		LocalGlFunctionalCurrencyRateHome glFunctionalCurrencyRateHome = null;

    LocalCmAdjustment cmAdjustment = null;

    // Initialize EJB Home

    try {

        cmAdjustmentHome = (LocalCmAdjustmentHome)EJBHomeFactory.
            lookUpLocalHome(LocalCmAdjustmentHome.JNDI_NAME, LocalCmAdjustmentHome.class);
        adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
            lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
        adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
        	lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);
        glSetOfBookHome = (LocalGlSetOfBookHome)EJBHomeFactory.
        	lookUpLocalHome(LocalGlSetOfBookHome.JNDI_NAME, LocalGlSetOfBookHome.class);
        glAccountingCalendarValueHome = (LocalGlAccountingCalendarValueHome)EJBHomeFactory.
        	lookUpLocalHome(LocalGlAccountingCalendarValueHome.JNDI_NAME, LocalGlAccountingCalendarValueHome.class);
        glJournalHome = (LocalGlJournalHome)EJBHomeFactory.
        	lookUpLocalHome(LocalGlJournalHome.JNDI_NAME, LocalGlJournalHome.class);
        glJournalBatchHome = (LocalGlJournalBatchHome)EJBHomeFactory.
        	lookUpLocalHome(LocalGlJournalBatchHome.JNDI_NAME, LocalGlJournalBatchHome.class);
        glSuspenseAccountHome = (LocalGlSuspenseAccountHome)EJBHomeFactory.
        	lookUpLocalHome(LocalGlSuspenseAccountHome.JNDI_NAME, LocalGlSuspenseAccountHome.class);
        glJournalLineHome = (LocalGlJournalLineHome)EJBHomeFactory.
        	lookUpLocalHome(LocalGlJournalLineHome.JNDI_NAME, LocalGlJournalLineHome.class);
        glJournalSourceHome = (LocalGlJournalSourceHome)EJBHomeFactory.
        	lookUpLocalHome(LocalGlJournalSourceHome.JNDI_NAME, LocalGlJournalSourceHome.class);
        glJournalCategoryHome = (LocalGlJournalCategoryHome)EJBHomeFactory.
        	lookUpLocalHome(LocalGlJournalCategoryHome.JNDI_NAME, LocalGlJournalCategoryHome.class);
        glFunctionalCurrencyHome = (LocalGlFunctionalCurrencyHome)EJBHomeFactory.
        	lookUpLocalHome(LocalGlFunctionalCurrencyHome.JNDI_NAME, LocalGlFunctionalCurrencyHome.class);
        cmDistributionRecordHome = (LocalCmDistributionRecordHome)EJBHomeFactory.
        	lookUpLocalHome(LocalCmDistributionRecordHome.JNDI_NAME, LocalCmDistributionRecordHome.class);
        glChartOfAccountHome = (LocalGlChartOfAccountHome)EJBHomeFactory.
        	lookUpLocalHome(LocalGlChartOfAccountHome.JNDI_NAME, LocalGlChartOfAccountHome.class);
        adBankAccountHome = (LocalAdBankAccountHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdBankAccountHome.JNDI_NAME, LocalAdBankAccountHome.class);
        adBankAccountBalanceHome = (LocalAdBankAccountBalanceHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdBankAccountBalanceHome.JNDI_NAME, LocalAdBankAccountBalanceHome.class);
        glForexLedgerHome  = (LocalGlForexLedgerHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlForexLedgerHome.JNDI_NAME, LocalGlForexLedgerHome.class);
			glFunctionalCurrencyRateHome = (LocalGlFunctionalCurrencyRateHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlFunctionalCurrencyRateHome.JNDI_NAME, LocalGlFunctionalCurrencyRateHome.class);

    } catch (NamingException ex) {

        throw new EJBException(ex.getMessage());

    }

    try {

    	// validate if adjustment is already deleted

    	try {

    		cmAdjustment = cmAdjustmentHome.findByPrimaryKey(ADJ_CODE);

    	} catch (FinderException ex) {

    		throw new GlobalRecordAlreadyDeletedException();

    	}

    	// validate if adjustment is already posted or void

    	if (cmAdjustment.getAdjPosted() == EJBCommon.TRUE) {

    		throw new GlobalTransactionAlreadyPostedException();

    	} else if (cmAdjustment.getAdjVoid() == EJBCommon.TRUE) {

    		throw new GlobalTransactionAlreadyVoidException();
    	}


    	//post advance payment



    	// post adjustment

    	if (cmAdjustment.getAdjVoid() == EJBCommon.FALSE && cmAdjustment.getAdjPosted() == EJBCommon.FALSE) {

    		if(cmAdjustment.getAdjType().equals("INTEREST") || cmAdjustment.getAdjType().equals("DEBIT MEMO")) {

    			// increase bank account balances

				LocalAdBankAccount adBankAccount = adBankAccountHome.findByPrimaryKey(cmAdjustment.getAdBankAccount().getBaCode());

				try {

					// find bankaccount balance before or equal receipt date

					Collection adBankAccountBalances = adBankAccountBalanceHome.findByBeforeOrEqualDateAndBaCodeAndType(cmAdjustment.getAdjDate(), cmAdjustment.getAdBankAccount().getBaCode(), "BOOK", AD_CMPNY);

					if (!adBankAccountBalances.isEmpty()) {

						// get last check

						ArrayList adBankAccountBalanceList = new ArrayList(adBankAccountBalances);

						LocalAdBankAccountBalance adBankAccountBalance = (LocalAdBankAccountBalance)adBankAccountBalanceList.get(adBankAccountBalanceList.size() - 1);

						if (adBankAccountBalance.getBabDate().before(cmAdjustment.getAdjDate())) {

							// create new balance

							LocalAdBankAccountBalance adNewBankAccountBalance = adBankAccountBalanceHome.create(
									cmAdjustment.getAdjDate(), adBankAccountBalance.getBabBalance() + cmAdjustment.getAdjAmount(), "BOOK", AD_CMPNY);

							adBankAccount.addAdBankAccountBalance(adNewBankAccountBalance);

						} else { // equals to check date

							adBankAccountBalance.setBabBalance(adBankAccountBalance.getBabBalance() + cmAdjustment.getAdjAmount());

						}

					} else {

						// create new balance

						LocalAdBankAccountBalance adNewBankAccountBalance = adBankAccountBalanceHome.create(
								cmAdjustment.getAdjDate(), (cmAdjustment.getAdjAmount()), "BOOK", AD_CMPNY);

						adBankAccount.addAdBankAccountBalance(adNewBankAccountBalance);

					}

					// propagate to subsequent balances if necessary

					adBankAccountBalances = adBankAccountBalanceHome.findByAfterDateAndBaCodeAndType(cmAdjustment.getAdjDate(), cmAdjustment.getAdBankAccount().getBaCode(), "BOOK", AD_CMPNY);

					Iterator i = adBankAccountBalances.iterator();

					while (i.hasNext()) {

						LocalAdBankAccountBalance adBankAccountBalance = (LocalAdBankAccountBalance)i.next();

						adBankAccountBalance.setBabBalance(adBankAccountBalance.getBabBalance() + cmAdjustment.getAdjAmount());

					}

				} catch (Exception ex) {

					ex.printStackTrace();

				}

    		} else {

    			// decrease bank account balances

				LocalAdBankAccount adBankAccount = adBankAccountHome.findByPrimaryKey(cmAdjustment.getAdBankAccount().getBaCode());

				try {

					// find bankaccount balance before or equal receipt date

					Collection adBankAccountBalances = adBankAccountBalanceHome.findByBeforeOrEqualDateAndBaCodeAndType(cmAdjustment.getAdjDate(), cmAdjustment.getAdBankAccount().getBaCode(), "BOOK", AD_CMPNY);

					if (!adBankAccountBalances.isEmpty()) {

						// get last check

						ArrayList adBankAccountBalanceList = new ArrayList(adBankAccountBalances);

						LocalAdBankAccountBalance adBankAccountBalance = (LocalAdBankAccountBalance)adBankAccountBalanceList.get(adBankAccountBalanceList.size() - 1);

						if (adBankAccountBalance.getBabDate().before(cmAdjustment.getAdjDate())) {

							// create new balance

							LocalAdBankAccountBalance adNewBankAccountBalance = adBankAccountBalanceHome.create(
									cmAdjustment.getAdjDate(), adBankAccountBalance.getBabBalance() - cmAdjustment.getAdjAmount(), "BOOK", AD_CMPNY);

							adBankAccount.addAdBankAccountBalance(adNewBankAccountBalance);

						} else { // equals to check date

							adBankAccountBalance.setBabBalance(adBankAccountBalance.getBabBalance() - cmAdjustment.getAdjAmount());

						}

					} else {

						// create new balance

						LocalAdBankAccountBalance adNewBankAccountBalance = adBankAccountBalanceHome.create(
								cmAdjustment.getAdjDate(), (0 - cmAdjustment.getAdjAmount()), "BOOK", AD_CMPNY);

						adBankAccount.addAdBankAccountBalance(adNewBankAccountBalance);

					}

					// propagate to subsequent balances if necessary

					adBankAccountBalances = adBankAccountBalanceHome.findByAfterDateAndBaCodeAndType(cmAdjustment.getAdjDate(), cmAdjustment.getAdBankAccount().getBaCode(), "BOOK", AD_CMPNY);

					Iterator i = adBankAccountBalances.iterator();

					while (i.hasNext()) {

						LocalAdBankAccountBalance adBankAccountBalance = (LocalAdBankAccountBalance)i.next();

						adBankAccountBalance.setBabBalance(adBankAccountBalance.getBabBalance() - cmAdjustment.getAdjAmount());

					}

				} catch (Exception ex) {

					ex.printStackTrace();

				}

    		}

    	}

    	// set adjcher post status

    	cmAdjustment.setAdjPosted(EJBCommon.TRUE);
    	cmAdjustment.setAdjPostedBy(USR_NM);
    	cmAdjustment.setAdjDatePosted(EJBCommon.getGcCurrentDateWoTime().getTime());


    	// post to gl if necessary

    	LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);
    	LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);

    	if (adPreference.getPrfCmGlPostingType().equals("AUTO-POST UPON APPROVAL")) {

    		// validate if date has no period and period is closed

    		LocalGlSetOfBook glJournalSetOfBook = null;

    		try {

    			glJournalSetOfBook = glSetOfBookHome.findByDate(cmAdjustment.getAdjDate(), AD_CMPNY);

    		} catch (FinderException ex) {

    			throw new GlJREffectiveDateNoPeriodExistException();

    		}

    		LocalGlAccountingCalendarValue glAccountingCalendarValue =
    			glAccountingCalendarValueHome.findByAcCodeAndDate(
    					glJournalSetOfBook.getGlAccountingCalendar().getAcCode(), cmAdjustment.getAdjDate(), AD_CMPNY);


    		if (glAccountingCalendarValue.getAcvStatus() == 'N' ||
    				glAccountingCalendarValue.getAcvStatus() == 'C' ||
					glAccountingCalendarValue.getAcvStatus() == 'P') {

    			throw new GlJREffectiveDatePeriodClosedException();

    		}

    		// check if invoice is balance if not check suspense posting

    		LocalGlJournalLine glOffsetJournalLine = null;

    		Collection cmDistributionRecords = cmDistributionRecordHome.findByDrReversalAndDrImportedAndAdjCode(EJBCommon.FALSE, EJBCommon.FALSE, cmAdjustment.getAdjCode(), AD_CMPNY);

    		Iterator j = cmDistributionRecords.iterator();

    		double TOTAL_DEBIT = 0d;
    		double TOTAL_CREDIT = 0d;

    		while (j.hasNext()) {

    			LocalCmDistributionRecord cmDistributionRecord = (LocalCmDistributionRecord)j.next();

    			double DR_AMNT = this.convertForeignToFunctionalCurrency(cmAdjustment.getAdBankAccount().getGlFunctionalCurrency().getFcCode(),
    					cmAdjustment.getAdBankAccount().getGlFunctionalCurrency().getFcName(),
						cmAdjustment.getAdjConversionDate(),
						cmAdjustment.getAdjConversionRate(),
						cmDistributionRecord.getDrAmount(), AD_CMPNY);

    			if (cmDistributionRecord.getDrDebit() == EJBCommon.TRUE) {

    				TOTAL_DEBIT += DR_AMNT;

    			} else {

    				TOTAL_CREDIT += DR_AMNT;

    			}

    		}

    		TOTAL_DEBIT = EJBCommon.roundIt(TOTAL_DEBIT, adCompany.getGlFunctionalCurrency().getFcPrecision());
    		TOTAL_CREDIT = EJBCommon.roundIt(TOTAL_CREDIT, adCompany.getGlFunctionalCurrency().getFcPrecision());

    		if (adPreference.getPrfAllowSuspensePosting() == EJBCommon.TRUE &&
    				TOTAL_DEBIT != TOTAL_CREDIT) {

    			LocalGlSuspenseAccount glSuspenseAccount = null;

    			try {

    				glSuspenseAccount = glSuspenseAccountHome.findByJsNameAndJcName("CASH MANAGEMENT", "BANK ADJUSTMENTS", AD_CMPNY);

    			} catch (FinderException ex) {

    				throw new GlobalJournalNotBalanceException();

    			}

    			if (TOTAL_DEBIT - TOTAL_CREDIT < 0) {

    				glOffsetJournalLine = glJournalLineHome.create(
    						(short)(cmDistributionRecords.size() + 1),
							EJBCommon.TRUE,
							TOTAL_CREDIT - TOTAL_DEBIT, "", AD_CMPNY);

    			} else {

    				glOffsetJournalLine = glJournalLineHome.create(
    						(short)(cmDistributionRecords.size() + 1),
							EJBCommon.FALSE,
							TOTAL_DEBIT - TOTAL_CREDIT, "", AD_CMPNY);

    			}

    			LocalGlChartOfAccount glChartOfAccount = glSuspenseAccount.getGlChartOfAccount();
    			glChartOfAccount.addGlJournalLine(glOffsetJournalLine);


    		} else if (adPreference.getPrfAllowSuspensePosting() == EJBCommon.FALSE &&
    				TOTAL_DEBIT != TOTAL_CREDIT) {

    			throw new GlobalJournalNotBalanceException();

    		}

    		// create journal batch if necessary

    		LocalGlJournalBatch glJournalBatch = null;
    		java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat("MM/dd/yyyy");

    		try {

    			glJournalBatch = glJournalBatchHome.findByJbName("JOURNAL IMPORT " + formatter.format(new Date()) + " BANK ADJUSTMENTS", AD_BRNCH, AD_CMPNY);

    		} catch (FinderException ex) {
    		}

    		if (adPreference.getPrfEnableGlJournalBatch() ==  EJBCommon.TRUE &&
    				glJournalBatch == null) {

    			glJournalBatch = glJournalBatchHome.create("JOURNAL IMPORT " + formatter.format(new Date()) + " BANK ADJUSTMENTS", "JOURNAL IMPORT", "CLOSED", EJBCommon.getGcCurrentDateWoTime().getTime(), USR_NM, AD_BRNCH, AD_CMPNY);

    		}

    		// create journal entry

    		LocalGlJournal glJournal = glJournalHome.create(cmAdjustment.getAdjReferenceNumber(),
    				cmAdjustment.getAdjMemo(), cmAdjustment.getAdjDate(),
					0.0d, null, cmAdjustment.getAdjDocumentNumber(), null, 1d, "N/A", null,
					'N', EJBCommon.TRUE, EJBCommon.FALSE,
					USR_NM, new Date(),
					USR_NM, new Date(),
					null, null,
					USR_NM, EJBCommon.getGcCurrentDateWoTime().getTime(),
					null, null, EJBCommon.FALSE, null,
					AD_BRNCH, AD_CMPNY);

    		LocalGlJournalSource glJournalSource = glJournalSourceHome.findByJsName("CASH MANAGEMENT", AD_CMPNY);
    		glJournalSource.addGlJournal(glJournal);

    		LocalGlFunctionalCurrency glFunctionalCurrency = glFunctionalCurrencyHome.findByFcName(adCompany.getGlFunctionalCurrency().getFcName(), AD_CMPNY);
    		glFunctionalCurrency.addGlJournal(glJournal);

    		LocalGlJournalCategory glJournalCategory = glJournalCategoryHome.findByJcName("BANK ADJUSTMENTS", AD_CMPNY);
    		glJournalCategory.addGlJournal(glJournal);

    		if (glJournalBatch != null) {

    			glJournalBatch.addGlJournal(glJournal);

    		}


    		// create journal lines

    		j = cmDistributionRecords.iterator();

    		while (j.hasNext()) {

    			LocalCmDistributionRecord cmDistributionRecord = (LocalCmDistributionRecord)j.next();

    			double DR_AMNT = this.convertForeignToFunctionalCurrency(cmAdjustment.getAdBankAccount().getGlFunctionalCurrency().getFcCode(),
    					cmAdjustment.getAdBankAccount().getGlFunctionalCurrency().getFcName(),
						cmAdjustment.getAdjConversionDate(),
						cmAdjustment.getAdjConversionRate(),
						cmDistributionRecord.getDrAmount(), AD_CMPNY);

    			LocalGlJournalLine glJournalLine = glJournalLineHome.create(
    					cmDistributionRecord.getDrLine(),
						cmDistributionRecord.getDrDebit(),
						DR_AMNT, "", AD_CMPNY);

    			cmDistributionRecord.getGlChartOfAccount().addGlJournalLine(glJournalLine);

    			glJournal.addGlJournalLine(glJournalLine);

    			cmDistributionRecord.setDrImported(EJBCommon.TRUE);

		  	  	// for FOREX revaluation
		  	  	if((cmAdjustment.getAdBankAccount().getGlFunctionalCurrency().getFcCode() !=
		  	  			adCompany.getGlFunctionalCurrency().getFcCode()) &&
					glJournalLine.getGlChartOfAccount().getGlFunctionalCurrency() != null &&
		  	  		(glJournalLine.getGlChartOfAccount().getGlFunctionalCurrency().getFcCode().equals(
		  	  			cmAdjustment.getAdBankAccount().getGlFunctionalCurrency().getFcCode()))){

		  	  		double CONVERSION_RATE = 1;

		  	  		if (cmAdjustment.getAdjConversionRate() != 0 && cmAdjustment.getAdjConversionRate() != 1) {

		  	  			CONVERSION_RATE = cmAdjustment.getAdjConversionRate();

		  	  		} else if (cmAdjustment.getAdjConversionDate() != null){

		  	  			CONVERSION_RATE = this.getFrRateByFrNameAndFrDate(
			  	  				glJournalLine.getGlChartOfAccount().getGlFunctionalCurrency().getFcName(),
								glJournal.getJrConversionDate(), AD_CMPNY);

		  	  		}

		  	  		Collection glForexLedgers = null;

		  	  		try {

		  	  			glForexLedgers = glForexLedgerHome.findLatestGlFrlByFrlDateAndByCoaCode(
		  	  				cmAdjustment.getAdjDate(), glJournalLine.getGlChartOfAccount().getCoaCode(), AD_CMPNY);

		  	  		} catch(FinderException ex) {

		  	  		}

		  	  		LocalGlForexLedger glForexLedger =
		  	  			(glForexLedgers.isEmpty() || glForexLedgers == null) ? null :
		  	  				(LocalGlForexLedger) glForexLedgers.iterator().next();

		  	  		int FRL_LN = (glForexLedger != null &&
		  	  				glForexLedger.getFrlDate().compareTo(cmAdjustment.getAdjDate()) == 0) ?
		  	  						glForexLedger.getFrlLine().intValue() + 1 : 1;

		  	  		// compute balance
		  	  		double COA_FRX_BLNC = glForexLedger == null ? 0 : glForexLedger.getFrlBalance();
		  	  		double FRL_AMNT = cmDistributionRecord.getDrAmount();

		  	  		if(glJournalLine.getGlChartOfAccount().getCoaAccountType().equalsIgnoreCase("ASSET"))
		  	  			FRL_AMNT = (glJournalLine.getJlDebit() == EJBCommon.TRUE  ? FRL_AMNT : (- 1 * FRL_AMNT));
		  	  		else
		  	  			FRL_AMNT = (glJournalLine.getJlDebit() == EJBCommon.TRUE  ? (- 1 * FRL_AMNT) : FRL_AMNT);

		  	  		COA_FRX_BLNC = COA_FRX_BLNC + FRL_AMNT;

		  	  		glForexLedger = glForexLedgerHome.create(cmAdjustment.getAdjDate(), new Integer (FRL_LN),
		  	  			"OTH", FRL_AMNT, CONVERSION_RATE, COA_FRX_BLNC, 0d, AD_CMPNY);

		  	  		glJournalLine.getGlChartOfAccount().addGlForexLedger(glForexLedger);

		  	  		// propagate balances
		  	  		try{

		  	  			glForexLedgers = glForexLedgerHome.findByGreaterThanFrlDateAndCoaCode(
		  	  				glForexLedger.getFrlDate(), glForexLedger.getGlChartOfAccount().getCoaCode(),
							glForexLedger.getFrlAdCompany());

		  	  		} catch (FinderException ex) {

		  	  		}

		  	  		Iterator itrFrl = glForexLedgers.iterator();

		  	  		while (itrFrl.hasNext()) {

		  	  			glForexLedger = (LocalGlForexLedger) itrFrl.next();
		  	  			FRL_AMNT = cmDistributionRecord.getDrAmount();

		  	  			if(glJournalLine.getGlChartOfAccount().getCoaAccountType().equalsIgnoreCase("ASSET"))
		  	  				FRL_AMNT = (glJournalLine.getJlDebit() == EJBCommon.TRUE  ? FRL_AMNT :
		  	  					(- 1 * FRL_AMNT));
		  	  			else
		  	  				FRL_AMNT = (glJournalLine.getJlDebit() == EJBCommon.TRUE  ? (- 1 * FRL_AMNT) :
		  	  					FRL_AMNT);

		  	  			glForexLedger.setFrlBalance(glForexLedger.getFrlBalance() + FRL_AMNT);

		  	  		}

		  	  	}

    		}

    		if (glOffsetJournalLine != null) {

    			glJournal.addGlJournalLine(glOffsetJournalLine);

    		}

    		// post journal to gl

    		Collection glJournalLines = glJournal.getGlJournalLines();

    		Iterator i = glJournalLines.iterator();

    		while (i.hasNext()) {

    			LocalGlJournalLine glJournalLine = (LocalGlJournalLine)i.next();

    			// post current to current acv

    			this.postToGl(glAccountingCalendarValue,
    					glJournalLine.getGlChartOfAccount(),
						true, glJournalLine.getJlDebit(), glJournalLine.getJlAmount(), AD_CMPNY);


    			// post to subsequent acvs (propagate)

    			Collection glSubsequentAccountingCalendarValues =
    				glAccountingCalendarValueHome.findSubsequentAcvByAcCodeAndAcvPeriodNumber(
    						glJournalSetOfBook.getGlAccountingCalendar().getAcCode(),
							glAccountingCalendarValue.getAcvPeriodNumber(), AD_CMPNY);

    			Iterator acvsIter = glSubsequentAccountingCalendarValues.iterator();

    			while (acvsIter.hasNext()) {

    				LocalGlAccountingCalendarValue glSubsequentAccountingCalendarValue =
    					(LocalGlAccountingCalendarValue)acvsIter.next();

    				this.postToGl(glSubsequentAccountingCalendarValue,
    						glJournalLine.getGlChartOfAccount(),
							false, glJournalLine.getJlDebit(), glJournalLine.getJlAmount(), AD_CMPNY);

    			}

    			// post to subsequent years if necessary

    			Collection glSubsequentSetOfBooks = glSetOfBookHome.findSubsequentSobByAcYear(glJournalSetOfBook.getGlAccountingCalendar().getAcYear(), AD_CMPNY);

    			if (!glSubsequentSetOfBooks.isEmpty() && glJournalSetOfBook.getSobYearEndClosed() == 1) {

    				adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
    				LocalGlChartOfAccount glRetainedEarningsAccount = glChartOfAccountHome.findByCoaAccountNumber(adCompany.getCmpRetainedEarnings(), AD_CMPNY);

    				Iterator sobIter = glSubsequentSetOfBooks.iterator();

    				while (sobIter.hasNext()) {

    					LocalGlSetOfBook glSubsequentSetOfBook = (LocalGlSetOfBook)sobIter.next();

    					String ACCOUNT_TYPE = glJournalLine.getGlChartOfAccount().getCoaAccountType();

    					// post to subsequent acvs of subsequent set of book(propagate)

    					Collection glAccountingCalendarValues =
    						glAccountingCalendarValueHome.findByAcCode(glSubsequentSetOfBook.getGlAccountingCalendar().getAcCode(), AD_CMPNY);

    					Iterator acvIter = glAccountingCalendarValues.iterator();

    					while (acvIter.hasNext()) {

    						LocalGlAccountingCalendarValue glSubsequentAccountingCalendarValue =
    							(LocalGlAccountingCalendarValue)acvIter.next();

    						if (ACCOUNT_TYPE.equals("ASSET") || ACCOUNT_TYPE.equals("LIABILITY") ||
    								ACCOUNT_TYPE.equals("OWNERS EQUITY")) {

    							this.postToGl(glSubsequentAccountingCalendarValue,
    									glJournalLine.getGlChartOfAccount(),
										false, glJournalLine.getJlDebit(), glJournalLine.getJlAmount(), AD_CMPNY);

    						} else { // revenue & expense

    							this.postToGl(glSubsequentAccountingCalendarValue,
    									glRetainedEarningsAccount,
										false, glJournalLine.getJlDebit(), glJournalLine.getJlAmount(), AD_CMPNY);

    						}

    					}

    					if (glSubsequentSetOfBook.getSobYearEndClosed() == 0) break;

    				}

    			}

    		}

    	}

    } catch (GlJREffectiveDateNoPeriodExistException ex) {

    	getSessionContext().setRollbackOnly();
    	throw ex;

    } catch (GlJREffectiveDatePeriodClosedException ex) {

    	getSessionContext().setRollbackOnly();
    	throw ex;

    } catch (GlobalJournalNotBalanceException ex) {

    	getSessionContext().setRollbackOnly();
    	throw ex;

    } catch (GlobalRecordAlreadyDeletedException ex) {

    	getSessionContext().setRollbackOnly();
    	throw ex;

    } catch (GlobalTransactionAlreadyPostedException ex) {

    	getSessionContext().setRollbackOnly();
    	throw ex;

    } catch (GlobalTransactionAlreadyVoidException ex) {

    	getSessionContext().setRollbackOnly();
    	throw ex;

	} catch (Exception ex) {

		Debug.printStackTrace(ex);
		getSessionContext().setRollbackOnly();
		throw new EJBException(ex.getMessage());

	}

}


private void postToInvSo(LocalArSalesOrderInvoiceLine arSalesOrderInvoiceLine, Date CST_DT, double CST_QTY_SLD,
		double CST_CST_OF_SLS, double CST_RMNNG_QTY, double CST_RMNNG_VL,  double CST_VRNC_VL, String USR_NM, Integer AD_BRNCH,
		Integer AD_CMPNY) throws
		AdPRFCoaGlVarianceAccountNotFoundException {

    Debug.print("ArInvoiceEntryControllerBean postToInvSo");

    LocalInvCostingHome invCostingHome = null;
    LocalAdPreferenceHome adPreferenceHome = null;
    LocalAdCompanyHome adCompanyHome = null;
    LocalInvAdjustmentLineHome invAdjustmentLineHome = null;

    // Initialize EJB Home

    try {

        invCostingHome = (LocalInvCostingHome)EJBHomeFactory.
        lookUpLocalHome(LocalInvCostingHome.JNDI_NAME, LocalInvCostingHome.class);
        adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
        lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);
        adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
        lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
        invAdjustmentLineHome = (LocalInvAdjustmentLineHome)EJBHomeFactory.
		lookUpLocalHome(LocalInvAdjustmentLineHome.JNDI_NAME, LocalInvAdjustmentLineHome.class);

    } catch (NamingException ex) {

        throw new EJBException(ex.getMessage());

    }

    try {

        LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);
        LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
        LocalArSalesOrderLine arSalesOrderLine = arSalesOrderInvoiceLine.getArSalesOrderLine();
        LocalInvItemLocation invItemLocation = arSalesOrderLine.getInvItemLocation();
        int CST_LN_NMBR = 0;

        CST_QTY_SLD = EJBCommon.roundIt(CST_QTY_SLD, adPreference.getPrfInvQuantityPrecisionUnit());
        CST_CST_OF_SLS = EJBCommon.roundIt(CST_CST_OF_SLS, adCompany.getGlFunctionalCurrency().getFcPrecision());
        CST_RMNNG_QTY = EJBCommon.roundIt(CST_RMNNG_QTY, adPreference.getPrfInvQuantityPrecisionUnit());
        CST_RMNNG_VL = EJBCommon.roundIt(CST_RMNNG_VL, adCompany.getGlFunctionalCurrency().getFcPrecision());

        try {

            // generate line number

            LocalInvCosting invCurrentCosting = invCostingHome.getByMaxCstLineNumberAndCstDateToLongAndIiNameAndLocName(CST_DT.getTime(), invItemLocation.getInvItem().getIiName(), invItemLocation.getInvLocation().getLocName(), AD_BRNCH, AD_CMPNY);
            CST_LN_NMBR = invCurrentCosting.getCstLineNumber() + 1;

        } catch (FinderException ex) {

            CST_LN_NMBR = 1;

        }

        if (adPreference.getPrfArAllowPriorDate() == EJBCommon.FALSE) {
            //void subsequent cost variance adjustments
            Collection invAdjustmentLines = invAdjustmentLineHome.findUnvoidAndIsCostVarianceGreaterThanAdjDateAndIlCodeAndBrCode(
            		CST_DT, invItemLocation.getIlCode(), AD_BRNCH, AD_CMPNY);
            Iterator i = invAdjustmentLines.iterator();

            while (i.hasNext()){

            	LocalInvAdjustmentLine invAdjustmentLine = (LocalInvAdjustmentLine) i.next();
            	this.voidInvAdjustment(invAdjustmentLine.getInvAdjustment(), AD_BRNCH, AD_CMPNY);

            }
        }

        // create costing
        LocalInvCosting invCosting = invCostingHome.create(CST_DT, CST_DT.getTime(), CST_LN_NMBR, 0d, 0d, 0d, 0d, 0d, 0d, CST_QTY_SLD, CST_CST_OF_SLS, CST_RMNNG_QTY, CST_RMNNG_VL, 0d, 0d, 0d, AD_BRNCH, AD_CMPNY);
        //invItemLocation.addInvCosting(invCosting);
        invCosting.setInvItemLocation(invItemLocation);
        invCosting.setArSalesOrderInvoiceLine(arSalesOrderInvoiceLine);

          // if cost variance is not 0, generate cost variance for the transaction
          if(CST_VRNC_VL != 0 && adPreference.getPrfArAllowPriorDate() == EJBCommon.FALSE) {

          	this.generateCostVariance(invCosting.getInvItemLocation(), CST_VRNC_VL,
          			"ARCM" + arSalesOrderInvoiceLine.getArInvoice().getInvNumber(),
          			arSalesOrderInvoiceLine.getArInvoice().getInvDescription(),
          			arSalesOrderInvoiceLine.getArInvoice().getInvDate(), USR_NM, AD_BRNCH, AD_CMPNY);

          }

        // propagate balance if necessary
        Collection invCostings = invCostingHome.findByGreaterThanCstDateAndIiNameAndLocName(CST_DT, invItemLocation.getInvItem().getIiName(), invItemLocation.getInvLocation().getLocName(), AD_BRNCH, AD_CMPNY);

        Iterator i = invCostings.iterator();

        while (i.hasNext()) {

            LocalInvCosting invPropagatedCosting = (LocalInvCosting)i.next();

            invPropagatedCosting.setCstRemainingQuantity(invPropagatedCosting.getCstRemainingQuantity() + CST_QTY_SLD);
            invPropagatedCosting.setCstRemainingValue(invPropagatedCosting.getCstRemainingValue() + CST_CST_OF_SLS);

        }

        // regenerate cost varaince
        if (adPreference.getPrfArAllowPriorDate() == EJBCommon.FALSE) {
        	this.regenerateCostVariance(invCostings, invCosting, AD_BRNCH, AD_CMPNY);
        }

    } catch (AdPRFCoaGlVarianceAccountNotFoundException ex) {

    	getSessionContext().setRollbackOnly();
    	throw ex;

    } catch (Exception ex) {

        Debug.printStackTrace(ex);
        getSessionContext().setRollbackOnly();
        throw new EJBException(ex.getMessage());

    }

}


private void addCmDrEntry(short DR_LN, String DR_CLSS,
	    byte DR_DBT, double DR_AMNT, Integer COA_CODE, LocalCmAdjustment cmAdjustment, Integer AD_BRNCH, Integer AD_CMPNY)
    throws GlobalBranchAccountNumberInvalidException {

		Debug.print("ArInvoiceEntryControllerBean addCmDrEntry");

		LocalCmDistributionRecordHome cmDistributionRecordHome = null;
		LocalGlChartOfAccountHome glChartOfAccountHome = null;
		LocalAdCompanyHome adCompanyHome = null;


        // Initialize EJB Home

        try {

            cmDistributionRecordHome = (LocalCmDistributionRecordHome)EJBHomeFactory.
                lookUpLocalHome(LocalCmDistributionRecordHome.JNDI_NAME, LocalCmDistributionRecordHome.class);
            glChartOfAccountHome = (LocalGlChartOfAccountHome)EJBHomeFactory.
                lookUpLocalHome(LocalGlChartOfAccountHome.JNDI_NAME, LocalGlChartOfAccountHome.class);
            adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);


        } catch (NamingException ex) {

            throw new EJBException(ex.getMessage());

        }

        try {

        	// get company

        	LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);

        	LocalGlChartOfAccount glChartOfAccount = glChartOfAccountHome.findByCoaCodeAndBranchCode(COA_CODE, AD_BRNCH, AD_CMPNY);

		    // create distribution record

		    LocalCmDistributionRecord cmDistributionRecord = cmDistributionRecordHome.create(
			    DR_LN, DR_CLSS, EJBCommon.roundIt(DR_AMNT, adCompany.getGlFunctionalCurrency().getFcPrecision()), DR_DBT,
			    EJBCommon.FALSE, EJBCommon.FALSE, AD_CMPNY);

			cmAdjustment.addCmDistributionRecord(cmDistributionRecord);
			glChartOfAccount.addCmDistributionRecord(cmDistributionRecord);
        } catch (FinderException ex) {

        	throw new GlobalBranchAccountNumberInvalidException();

        } catch (Exception ex) {

        	Debug.printStackTrace(ex);
        	getSessionContext().setRollbackOnly();
        	throw new EJBException(ex.getMessage());

        }

	}








    private void executeArInvPost(Integer INV_CODE, String USR_NM, Integer AD_BRNCH, Integer AD_CMPNY) throws
        GlobalRecordAlreadyDeletedException,
		GlobalTransactionAlreadyPostedException,
		GlobalTransactionAlreadyVoidException,
		GlJREffectiveDateNoPeriodExistException,
		GlJREffectiveDatePeriodClosedException,
		GlobalJournalNotBalanceException,
		GlobalBranchAccountNumberInvalidException,
		AdPRFCoaGlVarianceAccountNotFoundException {

        Debug.print("ArApprovalControllerBean executeApInvPost");

        LocalArInvoiceHome arInvoiceHome = null;
        LocalAdCompanyHome adCompanyHome = null;
        LocalAdPreferenceHome adPreferenceHome = null;
        LocalGlSetOfBookHome glSetOfBookHome = null;
        LocalGlAccountingCalendarValueHome glAccountingCalendarValueHome = null;
        LocalGlJournalHome glJournalHome = null;
        LocalGlJournalBatchHome glJournalBatchHome = null;
        LocalGlSuspenseAccountHome glSuspenseAccountHome = null;
        LocalGlJournalLineHome glJournalLineHome = null;
        LocalGlJournalSourceHome glJournalSourceHome = null;
        LocalGlJournalCategoryHome glJournalCategoryHome = null;
        LocalGlFunctionalCurrencyHome glFunctionalCurrencyHome = null;
        LocalArDistributionRecordHome arDistributionRecordHome = null;
        LocalInvCostingHome invCostingHome = null;
        LocalInvItemHome invItemHome = null;
        LocalGlChartOfAccountHome glChartOfAccountHome = null;
 		LocalGlForexLedgerHome glForexLedgerHome = null;
 		LocalGlFunctionalCurrencyRateHome glFunctionalCurrencyRateHome = null;

        LocalArInvoice arInvoice = null;
        LocalArInvoice arCreditMemo = null;
        LocalArInvoice arCreditedInvoice = null;

        LocalInvItemLocationHome invItemLocationHome = null;


        // Initialize EJB Home

        try {

            arInvoiceHome = (LocalArInvoiceHome)EJBHomeFactory.
                lookUpLocalHome(LocalArInvoiceHome.JNDI_NAME, LocalArInvoiceHome.class);
            adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
            adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
            	lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);
            glSetOfBookHome = (LocalGlSetOfBookHome)EJBHomeFactory.
            	lookUpLocalHome(LocalGlSetOfBookHome.JNDI_NAME, LocalGlSetOfBookHome.class);
            glAccountingCalendarValueHome = (LocalGlAccountingCalendarValueHome)EJBHomeFactory.
            	lookUpLocalHome(LocalGlAccountingCalendarValueHome.JNDI_NAME, LocalGlAccountingCalendarValueHome.class);
            glJournalHome = (LocalGlJournalHome)EJBHomeFactory.
            	lookUpLocalHome(LocalGlJournalHome.JNDI_NAME, LocalGlJournalHome.class);
            glJournalBatchHome = (LocalGlJournalBatchHome)EJBHomeFactory.
            	lookUpLocalHome(LocalGlJournalBatchHome.JNDI_NAME, LocalGlJournalBatchHome.class);
            glSuspenseAccountHome = (LocalGlSuspenseAccountHome)EJBHomeFactory.
            	lookUpLocalHome(LocalGlSuspenseAccountHome.JNDI_NAME, LocalGlSuspenseAccountHome.class);
            glJournalLineHome = (LocalGlJournalLineHome)EJBHomeFactory.
            	lookUpLocalHome(LocalGlJournalLineHome.JNDI_NAME, LocalGlJournalLineHome.class);
            glJournalSourceHome = (LocalGlJournalSourceHome)EJBHomeFactory.
            	lookUpLocalHome(LocalGlJournalSourceHome.JNDI_NAME, LocalGlJournalSourceHome.class);
            glJournalCategoryHome = (LocalGlJournalCategoryHome)EJBHomeFactory.
            	lookUpLocalHome(LocalGlJournalCategoryHome.JNDI_NAME, LocalGlJournalCategoryHome.class);
            glFunctionalCurrencyHome = (LocalGlFunctionalCurrencyHome)EJBHomeFactory.
            	lookUpLocalHome(LocalGlFunctionalCurrencyHome.JNDI_NAME, LocalGlFunctionalCurrencyHome.class);
            arDistributionRecordHome = (LocalArDistributionRecordHome)EJBHomeFactory.
            	lookUpLocalHome(LocalArDistributionRecordHome.JNDI_NAME, LocalArDistributionRecordHome.class);
            invCostingHome = (LocalInvCostingHome)EJBHomeFactory.
            	lookUpLocalHome(LocalInvCostingHome.JNDI_NAME, LocalInvCostingHome.class);
            invItemHome = (LocalInvItemHome)EJBHomeFactory.
            	lookUpLocalHome(LocalInvItemHome.JNDI_NAME, LocalInvItemHome.class);
            glChartOfAccountHome = (LocalGlChartOfAccountHome)EJBHomeFactory.
              	lookUpLocalHome(LocalGlChartOfAccountHome.JNDI_NAME, LocalGlChartOfAccountHome.class);
 			glForexLedgerHome  = (LocalGlForexLedgerHome)EJBHomeFactory.
				lookUpLocalHome(LocalGlForexLedgerHome.JNDI_NAME, LocalGlForexLedgerHome.class);
 			glFunctionalCurrencyRateHome = (LocalGlFunctionalCurrencyRateHome)EJBHomeFactory.
				lookUpLocalHome(LocalGlFunctionalCurrencyRateHome.JNDI_NAME, LocalGlFunctionalCurrencyRateHome.class);
 			invItemLocationHome = (LocalInvItemLocationHome)EJBHomeFactory.
 				lookUpLocalHome(LocalInvItemLocationHome.JNDI_NAME, LocalInvItemLocationHome.class);

        } catch (NamingException ex) {

            throw new EJBException(ex.getMessage());

        }

        try {

        	LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
        	LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);

        	// validate if invoice/credit memo is already deleted

        	try {

        		arInvoice = arInvoiceHome.findByPrimaryKey(INV_CODE);
        		arCreditMemo = arInvoiceHome.findByPrimaryKey(INV_CODE);
        	} catch (FinderException ex) {

        		throw new GlobalRecordAlreadyDeletedException();

        	}

        	// validate if invoice/credit memo is already posted or void

        	if (arInvoice.getInvPosted() == EJBCommon.TRUE) {

        		throw new GlobalTransactionAlreadyPostedException();

        	} else if (arInvoice.getInvVoid() == EJBCommon.TRUE) {

        		throw new GlobalTransactionAlreadyVoidException();
        	}

        	// regenerate cogs & inventory dr
    		if (adPreference.getPrfArAutoComputeCogs() == EJBCommon.TRUE) {
    			this.regenerateInventoryDr(arInvoice, AD_BRNCH, AD_CMPNY);
    		}

        	// post invoice/credit memo

        	if (arInvoice.getInvCreditMemo() == EJBCommon.FALSE) {

        		// increase customer balance

        		double INV_AMNT = this.convertForeignToFunctionalCurrency(arInvoice.getGlFunctionalCurrency().getFcCode(),
        				arInvoice.getGlFunctionalCurrency().getFcName(),
						arInvoice.getInvConversionDate(), arInvoice.getInvConversionRate(),
						arInvoice.getInvAmountDue(), AD_CMPNY);

        		this.post(arInvoice.getInvDate(), INV_AMNT, arInvoice.getArCustomer(), AD_CMPNY);

        		Collection arInvoiceLineItems = arInvoice.getArInvoiceLineItems();
        		Collection arSalesOrderInvoiceLines = arInvoice.getArSalesOrderInvoiceLines();

        		if (arInvoiceLineItems != null && !arInvoiceLineItems.isEmpty()) {

                    Iterator c = arInvoiceLineItems.iterator();

                    while(c.hasNext()) {

                        LocalArInvoiceLineItem arInvoiceLineItem = (LocalArInvoiceLineItem) c.next();


                            String II_NM = arInvoiceLineItem.getInvItemLocation().getInvItem().getIiName();
                            String LOC_NM = arInvoiceLineItem.getInvItemLocation().getInvLocation().getLocName();

                            double QTY_SLD = this.convertByUomFromAndItemAndQuantity(arInvoiceLineItem.getInvUnitOfMeasure(),
                                    arInvoiceLineItem.getInvItemLocation().getInvItem(), arInvoiceLineItem.getIliQuantity(), AD_CMPNY);

                            LocalInvCosting invCosting = null;

                            try {

                                invCosting  = invCostingHome.getByMaxCstDateToLongAndMaxCstLineNumberAndLessThanEqualCstDateAndIiNameAndLocName(arInvoice.getInvDate(), II_NM, LOC_NM, AD_BRNCH, AD_CMPNY);

                            } catch (FinderException ex) {

                            }

                            double COST = arInvoiceLineItem.getInvItemLocation().getInvItem().getIiUnitCost();

                            if (invCosting == null  ) {

                                this.postToInv(arInvoiceLineItem, arInvoice.getInvDate(),
                                		QTY_SLD, QTY_SLD * COST,
                                		-QTY_SLD, -QTY_SLD * COST,
                                		0d, null, AD_BRNCH, AD_CMPNY);

                            } else {

                                if(invCosting.getInvItemLocation().getInvItem().getIiCostMethod().equals("Average")) {

	                                double avgCost = invCosting.getCstRemainingQuantity() == 0 ? COST :
	                                		Math.abs(invCosting.getCstRemainingValue() / invCosting.getCstRemainingQuantity());

	                                	this.postToInv(arInvoiceLineItem, arInvoice.getInvDate(),
	                                			QTY_SLD, avgCost * QTY_SLD,
		                                		invCosting.getCstRemainingQuantity() - QTY_SLD, invCosting.getCstRemainingValue() - (avgCost * QTY_SLD),
		                                		0d, null, AD_BRNCH, AD_CMPNY);


                                } else if(invCosting.getInvItemLocation().getInvItem().getIiCostMethod().equals("FIFO")) {

            	        			double fifoCost = invCosting.getCstRemainingQuantity() == 0 ? COST :
            	        					this.getInvFifoCost(invCosting.getCstDate(), invCosting.getInvItemLocation().getIlCode(),
            	        					QTY_SLD, arInvoiceLineItem.getIliUnitPrice(), true, AD_BRNCH, AD_CMPNY);

            	        				this.postToInv(arInvoiceLineItem, arInvoice.getInvDate(),
            	        						QTY_SLD, fifoCost * QTY_SLD,
                	        					invCosting.getCstRemainingQuantity() - QTY_SLD, invCosting.getCstRemainingValue() - (fifoCost * QTY_SLD),
                	        					0d, null, AD_BRNCH, AD_CMPNY);

                                } else if(invCosting.getInvItemLocation().getInvItem().getIiCostMethod().equals("Standard")) {

            	        			double standardCost = invCosting.getInvItemLocation().getInvItem().getIiUnitCost();

            	        				this.postToInv(arInvoiceLineItem, arInvoice.getInvDate(),
            	        						QTY_SLD, standardCost * QTY_SLD,
                	        					invCosting.getCstRemainingQuantity() - QTY_SLD, invCosting.getCstRemainingValue() - (standardCost * QTY_SLD),
                	        					0d, null, AD_BRNCH, AD_CMPNY);


                                }

                            }

                        }



        		} else if (arSalesOrderInvoiceLines != null && !arSalesOrderInvoiceLines.isEmpty()) {

                    Iterator c = arSalesOrderInvoiceLines.iterator();

                    while(c.hasNext()) {

                        LocalArSalesOrderInvoiceLine arSalesOrderInvoiceLine = (LocalArSalesOrderInvoiceLine) c.next();
                        LocalArSalesOrderLine arSalesOrderLine = arSalesOrderInvoiceLine.getArSalesOrderLine();

                        String II_NM = arSalesOrderLine.getInvItemLocation().getInvItem().getIiName();
                        String LOC_NM = arSalesOrderLine.getInvItemLocation().getInvLocation().getLocName();
                        double QTY_SLD = this.convertByUomFromAndItemAndQuantity(arSalesOrderLine.getInvUnitOfMeasure(),
                                arSalesOrderLine.getInvItemLocation().getInvItem(), arSalesOrderInvoiceLine.getSilQuantityDelivered(), AD_CMPNY);

                        LocalInvCosting invCosting = null;

                        try {

                            invCosting  = invCostingHome.getByMaxCstDateToLongAndMaxCstLineNumberAndLessThanEqualCstDateAndIiNameAndLocName(arInvoice.getInvDate(), II_NM, LOC_NM, AD_BRNCH, AD_CMPNY);

                        } catch (FinderException ex) {

                        }

                        double COST = arSalesOrderLine.getInvItemLocation().getInvItem().getIiUnitCost();

                        if (invCosting == null  ) {


                            this.postToInvSo(arSalesOrderInvoiceLine, arInvoice.getInvDate(),
                            		QTY_SLD, COST * QTY_SLD,
                            		-QTY_SLD, -(COST * QTY_SLD), 0d, null, AD_BRNCH, AD_CMPNY);

                        } else {


                            if(invCosting.getInvItemLocation().getInvItem().getIiCostMethod().equals("Average")) {

	                            double avgCost = invCosting.getCstRemainingQuantity() == 0 ? COST:
	                            	Math.abs(invCosting.getCstRemainingValue() / invCosting.getCstRemainingQuantity());


	                            	this.postToInvSo(arSalesOrderInvoiceLine, arInvoice.getInvDate(),
	                            			QTY_SLD, avgCost * QTY_SLD,
		                            		invCosting.getCstRemainingQuantity() - QTY_SLD, invCosting.getCstRemainingValue() - (avgCost * QTY_SLD),
		                            		0d, null, AD_BRNCH, AD_CMPNY);


                            } else if(invCosting.getInvItemLocation().getInvItem().getIiCostMethod().equals("FIFO")) {

        	        			double fifoCost = this.getInvFifoCost(invCosting.getCstDate(), invCosting.getInvItemLocation().getIlCode(),
        	        					QTY_SLD, arSalesOrderInvoiceLine.getArSalesOrderLine().getSolUnitPrice(), true, AD_BRNCH, AD_CMPNY);

        	        				this.postToInvSo(arSalesOrderInvoiceLine, arInvoice.getInvDate(),
        	        						QTY_SLD, fifoCost * QTY_SLD,
            	        					invCosting.getCstRemainingQuantity() - QTY_SLD, invCosting.getCstRemainingValue() - (fifoCost * QTY_SLD),
            	        					0d, null, AD_BRNCH, AD_CMPNY);

                            } else if(invCosting.getInvItemLocation().getInvItem().getIiCostMethod().equals("Standard")) {

                            	double standardCost = invCosting.getInvItemLocation().getInvItem().getIiUnitCost();

        	        				this.postToInvSo(arSalesOrderInvoiceLine, arInvoice.getInvDate(),
        	        						QTY_SLD, standardCost * QTY_SLD,
            	        					invCosting.getCstRemainingQuantity() - QTY_SLD, invCosting.getCstRemainingValue() - (standardCost * QTY_SLD),
            	        					0d, null, AD_BRNCH, AD_CMPNY);

                            }

                        }
                    }
        		}


        	} else {



        		if (arCreditMemo.getInvVoid() == EJBCommon.TRUE && arCreditMemo.getInvVoidPosted() == EJBCommon.FALSE) {
        		System.out.println("VOID CREDIT MEMO---------------------------->");

        		// get credited invoice

        		arCreditedInvoice = arInvoiceHome.findByInvNumberAndInvCreditMemoAndBrCode(
        				arCreditMemo.getInvCmInvoiceNumber(), EJBCommon.FALSE, AD_BRNCH, AD_CMPNY);

        		// increase customer balance

        		double INV_AMNT = this.convertForeignToFunctionalCurrency(arCreditedInvoice.getGlFunctionalCurrency().getFcCode(),
        				arCreditedInvoice.getGlFunctionalCurrency().getFcName(),
						arCreditedInvoice.getInvConversionDate(), arCreditedInvoice.getInvConversionRate(),
						arCreditMemo.getInvAmountDue(), AD_CMPNY) ;

        		this.post(arCreditMemo.getInvDate(), INV_AMNT, arCreditMemo.getArCustomer(), AD_CMPNY);

        		// decrease invoice and ips amounts and release lock

        		double CREDIT_PERCENT = EJBCommon.roundIt(arCreditMemo.getInvAmountDue() / arCreditedInvoice.getInvAmountDue(), (short)6);

        		arCreditedInvoice.setInvAmountPaid(arCreditedInvoice.getInvAmountPaid() - arCreditMemo.getInvAmountDue());

        		double TOTAL_INVOICE_PAYMENT_SCHEDULE =  0d;

        		Collection arInvoicePaymentSchedules = arCreditedInvoice.getArInvoicePaymentSchedules();

        		Iterator i = arInvoicePaymentSchedules.iterator();

        		while (i.hasNext()) {

        			LocalArInvoicePaymentSchedule arInvoicePaymentSchedule =
        				(LocalArInvoicePaymentSchedule)i.next();

        			double INVOICE_PAYMENT_SCHEDULE_AMOUNT = 0;

        			// if last payment schedule subtract to avoid rounding difference error

        			if (i.hasNext()) {

        				INVOICE_PAYMENT_SCHEDULE_AMOUNT = EJBCommon.roundIt(arInvoicePaymentSchedule.getIpsAmountDue() * CREDIT_PERCENT, this.getGlFcPrecisionUnit(AD_CMPNY));

        			} else {

        				INVOICE_PAYMENT_SCHEDULE_AMOUNT = arCreditMemo.getInvAmountDue() - TOTAL_INVOICE_PAYMENT_SCHEDULE;

        			}

        			arInvoicePaymentSchedule.setIpsAmountPaid(arInvoicePaymentSchedule.getIpsAmountPaid() - INVOICE_PAYMENT_SCHEDULE_AMOUNT);

        			arInvoicePaymentSchedule.setIpsLock(EJBCommon.FALSE);

        			TOTAL_INVOICE_PAYMENT_SCHEDULE += INVOICE_PAYMENT_SCHEDULE_AMOUNT;

        		}

        		Collection arInvoiceLineItems = arCreditMemo.getArInvoiceLineItems();

        		if (arInvoiceLineItems != null && !arInvoiceLineItems.isEmpty()) {

        			Iterator c = arInvoiceLineItems.iterator();

        			while(c.hasNext()) {

        				LocalArInvoiceLineItem arInvoiceLineItem = (LocalArInvoiceLineItem) c.next();

    					String II_NM = arInvoiceLineItem.getInvItemLocation().getInvItem().getIiName();
    					String LOC_NM = arInvoiceLineItem.getInvItemLocation().getInvLocation().getLocName();

    					double QTY_SLD = this.convertByUomFromAndItemAndQuantity(arInvoiceLineItem.getInvUnitOfMeasure(),
    							arInvoiceLineItem.getInvItemLocation().getInvItem(), arInvoiceLineItem.getIliQuantity(), AD_CMPNY);

    					LocalInvCosting invCosting = null;

    					try {

    						invCosting  = invCostingHome.getByMaxCstDateToLongAndMaxCstLineNumberAndLessThanEqualCstDateAndIiNameAndLocName(arCreditMemo.getInvDate(), II_NM, LOC_NM, AD_BRNCH, AD_CMPNY);

    					} catch (FinderException ex) {

    					}

    					double COST = arInvoiceLineItem.getInvItemLocation().getInvItem().getIiUnitCost();

    					if (invCosting == null) {

    						this.postToInv(arInvoiceLineItem, arCreditMemo.getInvDate(),
    								QTY_SLD, COST * QTY_SLD,
    								QTY_SLD, COST * QTY_SLD,
    								0d, null, AD_BRNCH, AD_CMPNY);

    					} else {

    						if(invCosting.getInvItemLocation().getInvItem().getIiCostMethod().equals("Average")) {

    							double avgCost = invCosting.getCstRemainingQuantity() == 0 ? COST :
    									Math.abs(invCosting.getCstRemainingValue() / invCosting.getCstRemainingQuantity());

                        		this.postToInv(arInvoiceLineItem, arCreditMemo.getInvDate(),
                        				QTY_SLD, (avgCost * QTY_SLD),
                                		invCosting.getCstRemainingQuantity() + QTY_SLD, invCosting.getCstRemainingValue() + (avgCost * QTY_SLD),
                                		0d, null, AD_BRNCH, AD_CMPNY);

                            } else if(invCosting.getInvItemLocation().getInvItem().getIiCostMethod().equals("FIFO")) {

        	        			double fifoCost = invCosting.getCstRemainingQuantity() == 0 ? COST :
        	        				this.getInvFifoCost(invCosting.getCstDate(), invCosting.getInvItemLocation().getIlCode(),
        	        					QTY_SLD, arInvoiceLineItem.getIliUnitPrice(), true, AD_BRNCH, AD_CMPNY);

        	        			this.postToInv(arInvoiceLineItem, arCreditMemo.getInvDate(),
        	        					QTY_SLD, (fifoCost * QTY_SLD),
        	        					invCosting.getCstRemainingQuantity() + QTY_SLD, invCosting.getCstRemainingValue() + (fifoCost * QTY_SLD),
        	        					0d, null, AD_BRNCH, AD_CMPNY);

                            } else if(invCosting.getInvItemLocation().getInvItem().getIiCostMethod().equals("Standard")) {

        	        			double standardCost = invCosting.getInvItemLocation().getInvItem().getIiUnitCost();

        	        			this.postToInv(arInvoiceLineItem, arCreditMemo.getInvDate(),
        	        					QTY_SLD, (standardCost * QTY_SLD),
        	        					invCosting.getCstRemainingQuantity() + QTY_SLD, invCosting.getCstRemainingValue() + (standardCost * QTY_SLD),
        	        					0d, null, AD_BRNCH, AD_CMPNY);

                            }
    					}


        			}



        		}

        		// set cmAdjustment post status

  	           arCreditMemo.setInvVoidPosted(EJBCommon.TRUE);
  	           arCreditMemo.setInvPostedBy(USR_NM);
  	           arCreditMemo.setInvDatePosted(EJBCommon.getGcCurrentDateWoTime().getTime());


        	} else if(arCreditMemo.getInvVoid() == EJBCommon.FALSE && arCreditMemo.getInvPosted() == EJBCommon.FALSE) {
        		System.out.println("POST CREDIT MEMO---------------------------->");
        		// get credited invoice

        		arCreditedInvoice = arInvoiceHome.findByInvNumberAndInvCreditMemoAndBrCode(
        				arCreditMemo.getInvCmInvoiceNumber(), EJBCommon.FALSE, AD_BRNCH, AD_CMPNY);

        		// decrease customer balance

        		double INV_AMNT = this.convertForeignToFunctionalCurrency(arCreditedInvoice.getGlFunctionalCurrency().getFcCode(),
        				arCreditedInvoice.getGlFunctionalCurrency().getFcName(),
						arCreditedInvoice.getInvConversionDate(), arCreditedInvoice.getInvConversionRate(),
						arCreditMemo.getInvAmountDue(), AD_CMPNY) ;

        		this.post(arCreditMemo.getInvDate(), -INV_AMNT, arCreditMemo.getArCustomer(), AD_CMPNY);

        		// decrease invoice and ips amounts and release lock

        		double CREDIT_PERCENT = EJBCommon.roundIt(arCreditMemo.getInvAmountDue() / arCreditedInvoice.getInvAmountDue(), (short)6);

        		arCreditedInvoice.setInvAmountPaid(arCreditedInvoice.getInvAmountPaid() + arCreditMemo.getInvAmountDue());

        		double TOTAL_INVOICE_PAYMENT_SCHEDULE =  0d;

        		Collection arInvoicePaymentSchedules = arCreditedInvoice.getArInvoicePaymentSchedules();

        		Iterator i = arInvoicePaymentSchedules.iterator();

        		while (i.hasNext()) {

        			LocalArInvoicePaymentSchedule arInvoicePaymentSchedule =
        				(LocalArInvoicePaymentSchedule)i.next();

        			double INVOICE_PAYMENT_SCHEDULE_AMOUNT = 0;

        			// if last payment schedule subtract to avoid rounding difference error

        			if (i.hasNext()) {

        				INVOICE_PAYMENT_SCHEDULE_AMOUNT = EJBCommon.roundIt(arInvoicePaymentSchedule.getIpsAmountDue() * CREDIT_PERCENT, this.getGlFcPrecisionUnit(AD_CMPNY));

        			} else {

        				INVOICE_PAYMENT_SCHEDULE_AMOUNT = arCreditMemo.getInvAmountDue() - TOTAL_INVOICE_PAYMENT_SCHEDULE;

        			}

        			arInvoicePaymentSchedule.setIpsAmountPaid(arInvoicePaymentSchedule.getIpsAmountPaid() + INVOICE_PAYMENT_SCHEDULE_AMOUNT);

        			arInvoicePaymentSchedule.setIpsLock(EJBCommon.FALSE);

        			TOTAL_INVOICE_PAYMENT_SCHEDULE += INVOICE_PAYMENT_SCHEDULE_AMOUNT;

        		}

        		Collection arInvoiceLineItems = arCreditMemo.getArInvoiceLineItems();

        		if (arInvoiceLineItems != null && !arInvoiceLineItems.isEmpty()) {

        			Iterator c = arInvoiceLineItems.iterator();

        			while(c.hasNext()) {

        				LocalArInvoiceLineItem arInvoiceLineItem = (LocalArInvoiceLineItem) c.next();

        					String II_NM = arInvoiceLineItem.getInvItemLocation().getInvItem().getIiName();
        					String LOC_NM = arInvoiceLineItem.getInvItemLocation().getInvLocation().getLocName();

        					double QTY_SLD = this.convertByUomFromAndItemAndQuantity(arInvoiceLineItem.getInvUnitOfMeasure(),
        							arInvoiceLineItem.getInvItemLocation().getInvItem(), arInvoiceLineItem.getIliQuantity(), AD_CMPNY);

        					LocalInvCosting invCosting = null;

        					try {

        						invCosting  = invCostingHome.getByMaxCstDateToLongAndMaxCstLineNumberAndLessThanEqualCstDateAndIiNameAndLocName(arCreditMemo.getInvDate(), II_NM, LOC_NM, AD_BRNCH, AD_CMPNY);

        					} catch (FinderException ex) { }

        					double COST = arInvoiceLineItem.getInvItemLocation().getInvItem().getIiUnitCost();

        					if (invCosting == null) {


        						this.postToInv(arInvoiceLineItem, arCreditMemo.getInvDate(),
        								-QTY_SLD, -COST * QTY_SLD,
        								QTY_SLD, COST * QTY_SLD,
        								0d, null, AD_BRNCH, AD_CMPNY);

        					} else {


        						if(invCosting.getInvItemLocation().getInvItem().getIiCostMethod().equals("Average")) {

            						double avgCost = invCosting.getCstRemainingQuantity() == 0 ? COST :
            								Math.abs(invCosting.getCstRemainingValue() / invCosting.getCstRemainingQuantity());

	            					this.postToInv(arInvoiceLineItem, arCreditMemo.getInvDate(),
	            							-QTY_SLD, -avgCost * QTY_SLD,
	            							invCosting.getCstRemainingQuantity() + QTY_SLD, invCosting.getCstRemainingValue() + (avgCost * QTY_SLD),
	            							0d, null, AD_BRNCH, AD_CMPNY);


        						} else if(invCosting.getInvItemLocation().getInvItem().getIiCostMethod().equals("FIFO")) {

        		        			double fifoCost = invCosting.getCstRemainingQuantity() == 0 ? COST :
        		        					this.getInvFifoCost(invCosting.getCstDate(), invCosting.getInvItemLocation().getIlCode(),
        		        					QTY_SLD, arInvoiceLineItem.getIliUnitPrice() * QTY_SLD, true, AD_BRNCH, AD_CMPNY);

        		        			//post entries to database
        		        			this.postToInv(arInvoiceLineItem, arCreditMemo.getInvDate(),
        		        					-QTY_SLD, -fifoCost * QTY_SLD,
        		        					invCosting.getCstRemainingQuantity() + QTY_SLD, invCosting.getCstRemainingValue() + (fifoCost * QTY_SLD),
        		        					0d, null, AD_BRNCH, AD_CMPNY);


        						} else if(invCosting.getInvItemLocation().getInvItem().getIiCostMethod().equals("Standard")) {
        		        			double standardCost = invCosting.getInvItemLocation().getInvItem().getIiUnitCost();

        		        			//post entries to database
        		        			this.postToInv(arInvoiceLineItem, arCreditMemo.getInvDate(),
        		        					-QTY_SLD, -standardCost * QTY_SLD,
        		        					invCosting.getCstRemainingQuantity() + QTY_SLD, invCosting.getCstRemainingValue() + (standardCost * QTY_SLD),
        		        					0d, null, AD_BRNCH, AD_CMPNY);
        						}
        					}

        			}

        		}

        	}

        	}

        	// set invoice post status

        	arInvoice.setInvPosted(EJBCommon.TRUE);
        	arInvoice.setInvPostedBy(USR_NM);
        	arInvoice.setInvDatePosted(EJBCommon.getGcCurrentDateWoTime().getTime());


        	// post to gl if necessary

        	if (adPreference.getPrfArGlPostingType().equals("AUTO-POST UPON APPROVAL")) {

        		// validate if date has no period and period is closed

        		LocalGlSetOfBook glJournalSetOfBook = null;

        		try {

        			glJournalSetOfBook = glSetOfBookHome.findByDate(arInvoice.getInvDate(), AD_CMPNY);

        		} catch (FinderException ex) {

        			throw new GlJREffectiveDateNoPeriodExistException();

        		}

        		LocalGlAccountingCalendarValue glAccountingCalendarValue =
        			glAccountingCalendarValueHome.findByAcCodeAndDate(
        					glJournalSetOfBook.getGlAccountingCalendar().getAcCode(), arInvoice.getInvDate(), AD_CMPNY);


        		if (glAccountingCalendarValue.getAcvStatus() == 'N' ||
        				glAccountingCalendarValue.getAcvStatus() == 'C' ||
						glAccountingCalendarValue.getAcvStatus() == 'P') {

        			throw new GlJREffectiveDatePeriodClosedException();

        		}

        		// check if invoice is balance if not check suspense posting

        		LocalGlJournalLine glOffsetJournalLine = null;

        		Collection arDistributionRecords = arDistributionRecordHome.findImportableDrByInvCode(arInvoice.getInvCode(), AD_CMPNY);

        		Iterator j = arDistributionRecords.iterator();

        		double TOTAL_DEBIT = 0d;
        		double TOTAL_CREDIT = 0d;

        		while (j.hasNext()) {

        			LocalArDistributionRecord arDistributionRecord = (LocalArDistributionRecord)j.next();

        			double DR_AMNT = 0d;

        			if (arInvoice.getInvCreditMemo() == EJBCommon.FALSE) {

        				DR_AMNT = this.convertForeignToFunctionalCurrency(arInvoice.getGlFunctionalCurrency().getFcCode(),
        						arInvoice.getGlFunctionalCurrency().getFcName(),
								arInvoice.getInvConversionDate(),
								arInvoice.getInvConversionRate(),
								arDistributionRecord.getDrAmount(), AD_CMPNY);

        			} else {

        				DR_AMNT = this.convertForeignToFunctionalCurrency(arCreditedInvoice.getGlFunctionalCurrency().getFcCode(),
        						arCreditedInvoice.getGlFunctionalCurrency().getFcName(),
								arCreditedInvoice.getInvConversionDate(),
								arCreditedInvoice.getInvConversionRate(),
								arDistributionRecord.getDrAmount(), AD_CMPNY);

        			}

        			if (arDistributionRecord.getDrDebit() == EJBCommon.TRUE) {

        				TOTAL_DEBIT += DR_AMNT;

        			} else {

        				TOTAL_CREDIT += DR_AMNT;

        			}

        		}

        		TOTAL_DEBIT = EJBCommon.roundIt(TOTAL_DEBIT, adCompany.getGlFunctionalCurrency().getFcPrecision());
        		TOTAL_CREDIT = EJBCommon.roundIt(TOTAL_CREDIT, adCompany.getGlFunctionalCurrency().getFcPrecision());

        		if (adPreference.getPrfAllowSuspensePosting() == EJBCommon.TRUE &&
        				TOTAL_DEBIT != TOTAL_CREDIT) {

        			LocalGlSuspenseAccount glSuspenseAccount = null;

        			try {

        				glSuspenseAccount = glSuspenseAccountHome.findByJsNameAndJcName("ACCOUNTS RECEIVABLES", arInvoice.getInvCreditMemo() == EJBCommon.FALSE ? "SALES INVOICES" : "CREDIT MEMOS", AD_CMPNY);

        			} catch (FinderException ex) {

        				throw new GlobalJournalNotBalanceException();

        			}

        			if (TOTAL_DEBIT - TOTAL_CREDIT < 0) {

        				glOffsetJournalLine = glJournalLineHome.create((short)(arDistributionRecords.size() + 1),
								EJBCommon.TRUE, TOTAL_CREDIT - TOTAL_DEBIT, "", AD_CMPNY);

        			} else {

        				glOffsetJournalLine = glJournalLineHome.create((short)(arDistributionRecords.size() + 1),
								EJBCommon.FALSE, TOTAL_DEBIT - TOTAL_CREDIT, "", AD_CMPNY);

        			}

        			LocalGlChartOfAccount glChartOfAccount = glSuspenseAccount.getGlChartOfAccount();
        			glChartOfAccount.addGlJournalLine(glOffsetJournalLine);


        		} else if (adPreference.getPrfAllowSuspensePosting() == EJBCommon.FALSE &&
        				TOTAL_DEBIT != TOTAL_CREDIT) {

        			throw new GlobalJournalNotBalanceException();

        		}

        		// create journal batch if necessary

        		LocalGlJournalBatch glJournalBatch = null;
        		java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat("MM/dd/yyyy");

        		try {

        			if (adPreference.getPrfEnableArInvoiceBatch() == EJBCommon.TRUE) {

        				glJournalBatch = glJournalBatchHome.findByJbName("JOURNAL IMPORT " + formatter.format(new Date()) + " " + arInvoice.getArInvoiceBatch().getIbName(), AD_BRNCH, AD_CMPNY);

        			} else {

        				glJournalBatch = glJournalBatchHome.findByJbName("JOURNAL IMPORT " + formatter.format(new Date()) + " SALES INVOICES", AD_BRNCH, AD_CMPNY);

        			}


        		} catch (FinderException ex) {
        		}

        		if (adPreference.getPrfEnableGlJournalBatch() ==  EJBCommon.TRUE &&
        				glJournalBatch == null) {

        			if (adPreference.getPrfEnableArInvoiceBatch() == EJBCommon.TRUE) {

        				glJournalBatch = glJournalBatchHome.create("JOURNAL IMPORT " + formatter.format(new Date()) + " " + arInvoice.getArInvoiceBatch().getIbName(), "JOURNAL IMPORT", "CLOSED", EJBCommon.getGcCurrentDateWoTime().getTime(), USR_NM, AD_BRNCH, AD_CMPNY);

        			} else {

        				glJournalBatch = glJournalBatchHome.create("JOURNAL IMPORT " + formatter.format(new Date()) + " SALES INVOICES", "JOURNAL IMPORT", "CLOSED", EJBCommon.getGcCurrentDateWoTime().getTime(), USR_NM, AD_BRNCH, AD_CMPNY);

        			}


        		}

        		// create journal entry

        		LocalGlJournal glJournal = glJournalHome.create(arInvoice.getInvCreditMemo() == EJBCommon.FALSE ? arInvoice.getInvReferenceNumber() : arInvoice.getInvCmInvoiceNumber(),
        				arInvoice.getInvDescription(), arInvoice.getInvDate(),
						0.0d, null, arInvoice.getInvNumber(), null, 1d, "N/A", null,
						'N', EJBCommon.TRUE, EJBCommon.FALSE,
						USR_NM, new Date(),
						USR_NM, new Date(),
						null, null,
						USR_NM, EJBCommon.getGcCurrentDateWoTime().getTime(),
						arInvoice.getArCustomer().getCstTin(),
						arInvoice.getArCustomer().getCstName(), EJBCommon.FALSE,
						null, AD_BRNCH, AD_CMPNY);

        		LocalGlJournalSource glJournalSource = glJournalSourceHome.findByJsName("ACCOUNTS RECEIVABLES", AD_CMPNY);
        		glJournal.setGlJournalSource(glJournalSource);

        		LocalGlFunctionalCurrency glFunctionalCurrency = glFunctionalCurrencyHome.findByFcName(adCompany.getGlFunctionalCurrency().getFcName(), AD_CMPNY);
        		glJournal.setGlFunctionalCurrency(glFunctionalCurrency);

        		LocalGlJournalCategory glJournalCategory = glJournalCategoryHome.findByJcName(arInvoice.getInvCreditMemo() == EJBCommon.FALSE ? "SALES INVOICES" : "CREDIT MEMOS", AD_CMPNY);
        		glJournal.setGlJournalCategory(glJournalCategory);

        		if (glJournalBatch != null) {
        			
        			glJournal.setGlJournalBatch(glJournalBatch);

        		}


        		// create journal lines

        		j = arDistributionRecords.iterator();

        		while (j.hasNext()) {

        			LocalArDistributionRecord arDistributionRecord = (LocalArDistributionRecord)j.next();

        			double DR_AMNT = 0d;

        			if (arInvoice.getInvCreditMemo() == EJBCommon.FALSE) {

        				DR_AMNT = this.convertForeignToFunctionalCurrency(arInvoice.getGlFunctionalCurrency().getFcCode(),
        						arInvoice.getGlFunctionalCurrency().getFcName(),
								arInvoice.getInvConversionDate(),
								arInvoice.getInvConversionRate(),
								arDistributionRecord.getDrAmount(), AD_CMPNY);

        			} else {

        				DR_AMNT = this.convertForeignToFunctionalCurrency(arCreditedInvoice.getGlFunctionalCurrency().getFcCode(),
        						arCreditedInvoice.getGlFunctionalCurrency().getFcName(),
								arCreditedInvoice.getInvConversionDate(),
								arCreditedInvoice.getInvConversionRate(),
								arDistributionRecord.getDrAmount(), AD_CMPNY);

        			}

        			LocalGlJournalLine glJournalLine = glJournalLineHome.create(arDistributionRecord.getDrLine(),
							arDistributionRecord.getDrDebit(), DR_AMNT, "", AD_CMPNY);

        			arDistributionRecord.getGlChartOfAccount().addGlJournalLine(glJournalLine);

        			glJournal.addGlJournalLine(glJournalLine);

        			arDistributionRecord.setDrImported(EJBCommon.TRUE);

    		  	  	// for FOREX revaluation

            	    LocalArInvoice arInvoiceTemp = arInvoice.getInvCreditMemo() == EJBCommon.FALSE ?
            	    	arInvoice: arCreditedInvoice;

    		  	  	if((arInvoiceTemp.getGlFunctionalCurrency().getFcCode() !=
    		  	  			adCompany.getGlFunctionalCurrency().getFcCode()) &&
    					glJournalLine.getGlChartOfAccount().getGlFunctionalCurrency() != null &&
    		  	  		(glJournalLine.getGlChartOfAccount().getGlFunctionalCurrency().getFcCode().equals(
    		  	  		arInvoiceTemp.getGlFunctionalCurrency().getFcCode()))){

    		  	  		double CONVERSION_RATE = 1;

    		  	  		if (arInvoiceTemp.getInvConversionRate() != 0 && arInvoiceTemp.getInvConversionRate() != 1) {

    		  	  			CONVERSION_RATE = arInvoiceTemp.getInvConversionRate();

    		  	  		} else if (arInvoice.getInvConversionDate() != null){

    		  	  			CONVERSION_RATE = this.getFrRateByFrNameAndFrDate(
    			  	  				glJournalLine.getGlChartOfAccount().getGlFunctionalCurrency().getFcName(),
    								glJournal.getJrConversionDate(), AD_CMPNY);

    		  	  		}

    		  	  		Collection glForexLedgers = null;

    		  	  		try {

    		  	  			glForexLedgers = glForexLedgerHome.findLatestGlFrlByFrlDateAndByCoaCode(
    		  	  				arInvoiceTemp.getInvDate(), glJournalLine.getGlChartOfAccount().getCoaCode(), AD_CMPNY);

    		  	  		} catch(FinderException ex) {

    		  	  		}

    		  	  		LocalGlForexLedger glForexLedger =
    		  	  			(glForexLedgers.isEmpty() || glForexLedgers == null) ? null :
    		  	  				(LocalGlForexLedger) glForexLedgers.iterator().next();

    		  	  		int FRL_LN = (glForexLedger != null &&
    		  	  				glForexLedger.getFrlDate().compareTo(arInvoiceTemp.getInvDate()) == 0) ?
    		  	  						glForexLedger.getFrlLine().intValue() + 1 : 1;

    		  	  		// compute balance
    		  	  		double COA_FRX_BLNC = glForexLedger == null ? 0 : glForexLedger.getFrlBalance();
    		  	  		double FRL_AMNT = arDistributionRecord.getDrAmount();

    		  	  		if(glJournalLine.getGlChartOfAccount().getCoaAccountType().equalsIgnoreCase("ASSET"))
    		  	  			FRL_AMNT = (glJournalLine.getJlDebit() == EJBCommon.TRUE ? FRL_AMNT : (- 1 * FRL_AMNT));
    		  	  		else
    		  	  			FRL_AMNT = (glJournalLine.getJlDebit() == EJBCommon.TRUE ? (- 1 * FRL_AMNT) : FRL_AMNT);

    		  	  		COA_FRX_BLNC = COA_FRX_BLNC + FRL_AMNT;

    		  	  		glForexLedger = glForexLedgerHome.create(arInvoiceTemp.getInvDate(), new Integer (FRL_LN),
    		  	  			arInvoice.getInvCreditMemo() == EJBCommon.FALSE ? "SI" : "CM", FRL_AMNT, CONVERSION_RATE,
    		  	  			COA_FRX_BLNC, 0d, AD_CMPNY);

    		  	  		glJournalLine.getGlChartOfAccount().addGlForexLedger(glForexLedger);

    		  	  		// propagate balances
    		  	  		try{

    		  	  			glForexLedgers = glForexLedgerHome.findByGreaterThanFrlDateAndCoaCode(
    		  	  				glForexLedger.getFrlDate(), glForexLedger.getGlChartOfAccount().getCoaCode(),
								glForexLedger.getFrlAdCompany());

    		  	  		} catch (FinderException ex) {

    		  	  		}

    		  	  		Iterator itrFrl = glForexLedgers.iterator();

    		  	  		while (itrFrl.hasNext()) {

    		  	  			glForexLedger = (LocalGlForexLedger) itrFrl.next();
    		  	  			FRL_AMNT = arDistributionRecord.getDrAmount();

    		  	  			if(glJournalLine.getGlChartOfAccount().getCoaAccountType().equalsIgnoreCase("ASSET"))
    		  	  				FRL_AMNT = (glJournalLine.getJlDebit() == EJBCommon.TRUE ? FRL_AMNT :
    		  	  					(- 1 * FRL_AMNT));
    		  	  			else
    		  	  				FRL_AMNT = (glJournalLine.getJlDebit() == EJBCommon.TRUE ? (- 1 * FRL_AMNT) :
    		  	  					FRL_AMNT);

    		  	  			glForexLedger.setFrlBalance(glForexLedger.getFrlBalance() + FRL_AMNT);

    		  	  		}

    		  	  	}

        		}

        		if (glOffsetJournalLine != null) {

        			glJournal.addGlJournalLine(glOffsetJournalLine);

        		}

        		// post journal to gl

        		Collection glJournalLines = glJournal.getGlJournalLines();

        		Iterator i = glJournalLines.iterator();

        		while (i.hasNext()) {

        			LocalGlJournalLine glJournalLine = (LocalGlJournalLine)i.next();

        			// post current to current acv

        			this.postToGl(glAccountingCalendarValue,
        					glJournalLine.getGlChartOfAccount(),
							true, glJournalLine.getJlDebit(), glJournalLine.getJlAmount(), AD_CMPNY);


        			// post to subsequent acvs (propagate)

        			Collection glSubsequentAccountingCalendarValues =
        				glAccountingCalendarValueHome.findSubsequentAcvByAcCodeAndAcvPeriodNumber(
        						glJournalSetOfBook.getGlAccountingCalendar().getAcCode(),
								glAccountingCalendarValue.getAcvPeriodNumber(), AD_CMPNY);

        			Iterator acvsIter = glSubsequentAccountingCalendarValues.iterator();

        			while (acvsIter.hasNext()) {

        				LocalGlAccountingCalendarValue glSubsequentAccountingCalendarValue =
        					(LocalGlAccountingCalendarValue)acvsIter.next();

        				this.postToGl(glSubsequentAccountingCalendarValue,
        						glJournalLine.getGlChartOfAccount(),
								false, glJournalLine.getJlDebit(), glJournalLine.getJlAmount(), AD_CMPNY);

        			}

        			// post to subsequent years if necessary

        			Collection glSubsequentSetOfBooks = glSetOfBookHome.findSubsequentSobByAcYear(glJournalSetOfBook.getGlAccountingCalendar().getAcYear(), AD_CMPNY);

        			if (!glSubsequentSetOfBooks.isEmpty() && glJournalSetOfBook.getSobYearEndClosed() == 1) {

        				adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
        				LocalGlChartOfAccount glRetainedEarningsAccount = glChartOfAccountHome.findByCoaAccountNumber(adCompany.getCmpRetainedEarnings(), AD_CMPNY);

        				Iterator sobIter = glSubsequentSetOfBooks.iterator();

        				while (sobIter.hasNext()) {

        					LocalGlSetOfBook glSubsequentSetOfBook = (LocalGlSetOfBook)sobIter.next();

        					String ACCOUNT_TYPE = glJournalLine.getGlChartOfAccount().getCoaAccountType();

        					// post to subsequent acvs of subsequent set of book(propagate)

        					Collection glAccountingCalendarValues =
        						glAccountingCalendarValueHome.findByAcCode(glSubsequentSetOfBook.getGlAccountingCalendar().getAcCode(), AD_CMPNY);

        					Iterator acvIter = glAccountingCalendarValues.iterator();

        					while (acvIter.hasNext()) {

        						LocalGlAccountingCalendarValue glSubsequentAccountingCalendarValue =
        							(LocalGlAccountingCalendarValue)acvIter.next();

        						if (ACCOUNT_TYPE.equals("ASSET") || ACCOUNT_TYPE.equals("LIABILITY") ||
        								ACCOUNT_TYPE.equals("OWNERS EQUITY")) {

        							this.postToGl(glSubsequentAccountingCalendarValue,
        									glJournalLine.getGlChartOfAccount(),
											false, glJournalLine.getJlDebit(), glJournalLine.getJlAmount(), AD_CMPNY);

        						} else { // revenue & expense

        							this.postToGl(glSubsequentAccountingCalendarValue,
        									glRetainedEarningsAccount,
											false, glJournalLine.getJlDebit(), glJournalLine.getJlAmount(), AD_CMPNY);

        						}

        					}

        					if (glSubsequentSetOfBook.getSobYearEndClosed() == 0) break;

        				}

        			}

        		}

        	}

        } catch (GlJREffectiveDateNoPeriodExistException ex) {

        	getSessionContext().setRollbackOnly();
        	throw ex;

        } catch (GlJREffectiveDatePeriodClosedException ex) {

        	getSessionContext().setRollbackOnly();
        	throw ex;

        } catch (GlobalJournalNotBalanceException ex) {

        	getSessionContext().setRollbackOnly();
        	throw ex;

        } catch (GlobalRecordAlreadyDeletedException ex) {

        	getSessionContext().setRollbackOnly();
        	throw ex;

        } catch (GlobalTransactionAlreadyPostedException ex) {

        	getSessionContext().setRollbackOnly();
        	throw ex;

        } catch (GlobalTransactionAlreadyVoidException ex) {

        	getSessionContext().setRollbackOnly();
        	throw ex;

        } catch (GlobalBranchAccountNumberInvalidException ex) {

        	getSessionContext().setRollbackOnly();
        	throw ex;

        } catch (AdPRFCoaGlVarianceAccountNotFoundException ex) {

        	getSessionContext().setRollbackOnly();
        	throw ex;

        } catch (Exception ex) {

    		Debug.printStackTrace(ex);
    		getSessionContext().setRollbackOnly();
    		throw new EJBException(ex.getMessage());

    	}

    }

    private void executeArRctPost(Integer RCT_CODE, String USR_NM, Integer AD_BRNCH, Integer AD_CMPNY) throws
        GlobalRecordAlreadyDeletedException,
        GlobalTransactionAlreadyPostedException,
		GlobalTransactionAlreadyVoidPostedException,
		GlJREffectiveDateNoPeriodExistException,
		GlJREffectiveDatePeriodClosedException,
		GlobalJournalNotBalanceException,
		GlobalBranchAccountNumberInvalidException,
		AdPRFCoaGlVarianceAccountNotFoundException {

        Debug.print("ArApprovalControllerBean executeArRctPost");

        LocalArReceiptHome arReceiptHome = null;
        LocalAdCompanyHome adCompanyHome = null;
        LocalAdPreferenceHome adPreferenceHome = null;
        LocalGlSetOfBookHome glSetOfBookHome = null;
        LocalGlAccountingCalendarValueHome glAccountingCalendarValueHome = null;
        LocalGlJournalHome glJournalHome = null;
        LocalGlJournalBatchHome glJournalBatchHome = null;
        LocalGlSuspenseAccountHome glSuspenseAccountHome = null;
        LocalGlJournalLineHome glJournalLineHome = null;
        LocalGlJournalSourceHome glJournalSourceHome = null;
        LocalGlJournalCategoryHome glJournalCategoryHome = null;
        LocalGlFunctionalCurrencyHome glFunctionalCurrencyHome = null;
        LocalArDistributionRecordHome arDistributionRecordHome = null;
        LocalInvCostingHome invCostingHome = null;
        LocalInvItemHome invItemHome = null;
        LocalGlChartOfAccountHome glChartOfAccountHome = null;
        LocalAdBankAccountHome adBankAccountHome = null;
        LocalAdBankAccountBalanceHome adBankAccountBalanceHome = null;
 		LocalGlForexLedgerHome glForexLedgerHome = null;
 		LocalGlFunctionalCurrencyRateHome glFunctionalCurrencyRateHome = null;

        LocalArReceipt arReceipt = null;

        // Initialize EJB Home

        try {

            arReceiptHome = (LocalArReceiptHome)EJBHomeFactory.
                lookUpLocalHome(LocalArReceiptHome.JNDI_NAME, LocalArReceiptHome.class);
            adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
            adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
            	lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);
            glSetOfBookHome = (LocalGlSetOfBookHome)EJBHomeFactory.
            	lookUpLocalHome(LocalGlSetOfBookHome.JNDI_NAME, LocalGlSetOfBookHome.class);
            glAccountingCalendarValueHome = (LocalGlAccountingCalendarValueHome)EJBHomeFactory.
            	lookUpLocalHome(LocalGlAccountingCalendarValueHome.JNDI_NAME, LocalGlAccountingCalendarValueHome.class);
            glJournalHome = (LocalGlJournalHome)EJBHomeFactory.
            	lookUpLocalHome(LocalGlJournalHome.JNDI_NAME, LocalGlJournalHome.class);
            glJournalBatchHome = (LocalGlJournalBatchHome)EJBHomeFactory.
            	lookUpLocalHome(LocalGlJournalBatchHome.JNDI_NAME, LocalGlJournalBatchHome.class);
            glSuspenseAccountHome = (LocalGlSuspenseAccountHome)EJBHomeFactory.
            	lookUpLocalHome(LocalGlSuspenseAccountHome.JNDI_NAME, LocalGlSuspenseAccountHome.class);
            glJournalLineHome = (LocalGlJournalLineHome)EJBHomeFactory.
            	lookUpLocalHome(LocalGlJournalLineHome.JNDI_NAME, LocalGlJournalLineHome.class);
            glJournalSourceHome = (LocalGlJournalSourceHome)EJBHomeFactory.
            	lookUpLocalHome(LocalGlJournalSourceHome.JNDI_NAME, LocalGlJournalSourceHome.class);
            glJournalCategoryHome = (LocalGlJournalCategoryHome)EJBHomeFactory.
            	lookUpLocalHome(LocalGlJournalCategoryHome.JNDI_NAME, LocalGlJournalCategoryHome.class);
            glFunctionalCurrencyHome = (LocalGlFunctionalCurrencyHome)EJBHomeFactory.
            	lookUpLocalHome(LocalGlFunctionalCurrencyHome.JNDI_NAME, LocalGlFunctionalCurrencyHome.class);
            arDistributionRecordHome = (LocalArDistributionRecordHome)EJBHomeFactory.
            	lookUpLocalHome(LocalArDistributionRecordHome.JNDI_NAME, LocalArDistributionRecordHome.class);
            invCostingHome = (LocalInvCostingHome)EJBHomeFactory.
            	lookUpLocalHome(LocalInvCostingHome.JNDI_NAME, LocalInvCostingHome.class);
            invItemHome = (LocalInvItemHome)EJBHomeFactory.
        		lookUpLocalHome(LocalInvItemHome.JNDI_NAME, LocalInvItemHome.class);
            glChartOfAccountHome = (LocalGlChartOfAccountHome)EJBHomeFactory.
              	lookUpLocalHome(LocalGlChartOfAccountHome.JNDI_NAME, LocalGlChartOfAccountHome.class);
            adBankAccountHome = (LocalAdBankAccountHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdBankAccountHome.JNDI_NAME, LocalAdBankAccountHome.class);
            adBankAccountBalanceHome = (LocalAdBankAccountBalanceHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdBankAccountBalanceHome.JNDI_NAME, LocalAdBankAccountBalanceHome.class);
 			glForexLedgerHome  = (LocalGlForexLedgerHome)EJBHomeFactory.
				lookUpLocalHome(LocalGlForexLedgerHome.JNDI_NAME, LocalGlForexLedgerHome.class);
 			glFunctionalCurrencyRateHome = (LocalGlFunctionalCurrencyRateHome)EJBHomeFactory.
				lookUpLocalHome(LocalGlFunctionalCurrencyRateHome.JNDI_NAME, LocalGlFunctionalCurrencyRateHome.class);

        } catch (NamingException ex) {

            throw new EJBException(ex.getMessage());

        }

        try {

        	LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);
        	LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);

        	// validate if receipt is already deleted

        	try {

        		arReceipt = arReceiptHome.findByPrimaryKey(RCT_CODE);

        	} catch (FinderException ex) {

        		throw new GlobalRecordAlreadyDeletedException();

        	}

        	// validate if receipt is already posted

        	if (arReceipt.getRctVoid() == EJBCommon.FALSE && arReceipt.getRctPosted() == EJBCommon.TRUE) {

        		throw new GlobalTransactionAlreadyPostedException();

        		// validate if receipt void is already posted

        	} else if (arReceipt.getRctVoid() == EJBCommon.TRUE && arReceipt.getRctVoidPosted() == EJBCommon.TRUE) {

        		throw new GlobalTransactionAlreadyVoidPostedException();

        	}

        	if (arReceipt.getRctVoid() == EJBCommon.FALSE && arReceipt.getRctType().equals("MISC") && !arReceipt.getArInvoiceLineItems().isEmpty()) {

        		// regenerate inventory dr

        		this.regenerateInventoryDr(arReceipt, AD_BRNCH, AD_CMPNY);

        	}

        	// post receipt

        	if (arReceipt.getRctVoid() == EJBCommon.FALSE && arReceipt.getRctPosted() == EJBCommon.FALSE) {


        		if (arReceipt.getRctType().equals("COLLECTION")) {

        			double RCT_CRDTS = 0d;
        			double RCT_AI_APPLD_DPSTS = 0d;
        			double RCT_EXCSS_AMNT = arReceipt.getRctExcessAmount();

        			//create adjustment for advance payment
        			if(arReceipt.getRctEnableAdvancePayment()!=0){


        			}


        			// increase amount paid in invoice payment schedules and invoice

        			Collection arAppliedInvoices = arReceipt.getArAppliedInvoices();

        			Iterator i = arAppliedInvoices.iterator();

        			while (i.hasNext()) {

        				LocalArAppliedInvoice arAppliedInvoice = (LocalArAppliedInvoice)i.next();

        				LocalArInvoicePaymentSchedule arInvoicePaymentSchedule = arAppliedInvoice.getArInvoicePaymentSchedule();

        				double AMOUNT_PAID = arAppliedInvoice.getAiApplyAmount() + arAppliedInvoice.getAiCreditableWTax() + arAppliedInvoice.getAiDiscountAmount() + arAppliedInvoice.getAiAppliedDeposit() + arAppliedInvoice.getAiCreditBalancePaid() + arAppliedInvoice.getAiRebate();

        				double PENALTY_PAID = arAppliedInvoice.getAiPenaltyApplyAmount();


        				RCT_CRDTS += this.convertForeignToFunctionalCurrency(arAppliedInvoice.getArInvoicePaymentSchedule().getArInvoice().getGlFunctionalCurrency().getFcCode(),
        						arAppliedInvoice.getArInvoicePaymentSchedule().getArInvoice().getGlFunctionalCurrency().getFcName(),
								arAppliedInvoice.getArInvoicePaymentSchedule().getArInvoice().getInvConversionDate(),
								arAppliedInvoice.getArInvoicePaymentSchedule().getArInvoice().getInvConversionRate(),
								arAppliedInvoice.getAiDiscountAmount() + arAppliedInvoice.getAiCreditableWTax(), AD_CMPNY);

        				RCT_AI_APPLD_DPSTS += this.convertForeignToFunctionalCurrency(arAppliedInvoice.getArInvoicePaymentSchedule().getArInvoice().getGlFunctionalCurrency().getFcCode(),
        						arAppliedInvoice.getArInvoicePaymentSchedule().getArInvoice().getGlFunctionalCurrency().getFcName(),
								arAppliedInvoice.getArInvoicePaymentSchedule().getArInvoice().getInvConversionDate(),
								arAppliedInvoice.getArInvoicePaymentSchedule().getArInvoice().getInvConversionRate(),
								arAppliedInvoice.getAiAppliedDeposit(), AD_CMPNY);

        				arInvoicePaymentSchedule.setIpsAmountPaid(EJBCommon.roundIt(arInvoicePaymentSchedule.getIpsAmountPaid() + AMOUNT_PAID, this.getGlFcPrecisionUnit(AD_CMPNY)));
        				arInvoicePaymentSchedule.setIpsPenaltyPaid(EJBCommon.roundIt(arInvoicePaymentSchedule.getIpsPenaltyPaid() + PENALTY_PAID, this.getGlFcPrecisionUnit(AD_CMPNY)));

        				arInvoicePaymentSchedule.getArInvoice().setInvAmountPaid(EJBCommon.roundIt(arInvoicePaymentSchedule.getArInvoice().getInvAmountPaid() + AMOUNT_PAID, this.getGlFcPrecisionUnit(AD_CMPNY)));

        				arInvoicePaymentSchedule.getArInvoice().setInvPenaltyPaid(EJBCommon.roundIt(arInvoicePaymentSchedule.getArInvoice().getInvPenaltyPaid() + PENALTY_PAID, this.getGlFcPrecisionUnit(AD_CMPNY)));

        				/*// Remove penalty
        				if(arInvoicePaymentSchedule.getIpsAmountDue() ==  arInvoicePaymentSchedule.getIpsAmountPaid() ){
        					arInvoicePaymentSchedule.getArInvoice().setInvInterest(EJBCommon.TRUE);
        				}*/
        				// release invoice lock

        				arInvoicePaymentSchedule.setIpsLock(EJBCommon.FALSE);

        			}


    		     	// decrease customer balance

        			double RCT_AMNT = this.convertForeignToFunctionalCurrency(arReceipt.getGlFunctionalCurrency().getFcCode(),
        					arReceipt.getGlFunctionalCurrency().getFcName(),
							arReceipt.getRctConversionDate(), arReceipt.getRctConversionRate(),
							arReceipt.getRctAmount(), AD_CMPNY);

        			this.post(arReceipt.getRctDate(), (RCT_AMNT + RCT_CRDTS + RCT_AI_APPLD_DPSTS) * -1, arReceipt.getArCustomer(), AD_CMPNY);


        		} else if (arReceipt.getRctType().equals("MISC") && !arReceipt.getArInvoiceLineItems().isEmpty()) {

    	        		Iterator c = arReceipt.getArInvoiceLineItems().iterator();

    	    			while(c.hasNext()) {

    	    				LocalArInvoiceLineItem arInvoiceLineItem = (LocalArInvoiceLineItem) c.next();

    	    				String II_NM = arInvoiceLineItem.getInvItemLocation().getInvItem().getIiName();
    	    				String LOC_NM = arInvoiceLineItem.getInvItemLocation().getInvLocation().getLocName();

    	    				double QTY_SLD = this.convertByUomFromAndItemAndQuantity(arInvoiceLineItem.getInvUnitOfMeasure(),
    	    						arInvoiceLineItem.getInvItemLocation().getInvItem(), arInvoiceLineItem.getIliQuantity(), AD_CMPNY);

    	    				LocalInvCosting invCosting = null;

    	    				try {

    	    					invCosting  = invCostingHome.getByMaxCstDateToLongAndMaxCstLineNumberAndLessThanEqualCstDateAndIiNameAndLocName(arReceipt.getRctDate(), II_NM, LOC_NM, AD_BRNCH, AD_CMPNY);

    	    				} catch (FinderException ex) {

    	    				}

    	    				double COST = arInvoiceLineItem.getInvItemLocation().getInvItem().getIiUnitCost();

    	    				if (invCosting == null) {

    							this.postToInv(arInvoiceLineItem, arReceipt.getRctDate(),
    									QTY_SLD, QTY_SLD * COST,
    									- QTY_SLD, -COST * QTY_SLD,
    									0d, null, AD_BRNCH, AD_CMPNY);
    	    				} else {

    	    					if(invCosting.getInvItemLocation().getInvItem().getIiCostMethod().equals("Average")) {

    		    					double avgCost = invCosting.getCstRemainingQuantity() == 0 ? COST :
    		    							Math.abs(invCosting.getCstRemainingValue() / invCosting.getCstRemainingQuantity());

    								this.postToInv(arInvoiceLineItem, arReceipt.getRctDate(),
    										QTY_SLD, avgCost * QTY_SLD ,
    										invCosting.getCstRemainingQuantity() - QTY_SLD, invCosting.getCstRemainingValue() - (avgCost * QTY_SLD),
    										0d, null, AD_BRNCH, AD_CMPNY);

    	    					} else if(invCosting.getInvItemLocation().getInvItem().getIiCostMethod().equals("FIFO")) {

    	    	        			double fifoCost = invCosting.getCstRemainingQuantity() == 0 ? COST :
    	    	        					this.getInvFifoCost(invCosting.getCstDate(), invCosting.getInvItemLocation().getIlCode(),
    	    	        					QTY_SLD, arInvoiceLineItem.getIliUnitPrice(), true, AD_BRNCH, AD_CMPNY);

    	    	        			this.postToInv(arInvoiceLineItem, arReceipt.getRctDate(),
    	    	        					QTY_SLD, fifoCost * QTY_SLD,
    	    	        					invCosting.getCstRemainingQuantity() - QTY_SLD, invCosting.getCstRemainingValue() - (fifoCost * QTY_SLD),
    	    	        					0d, null, AD_BRNCH, AD_CMPNY);

    	    					} else if(invCosting.getInvItemLocation().getInvItem().getIiCostMethod().equals("Standard")) {

    	    						double standardCost = invCosting.getInvItemLocation().getInvItem().getIiUnitCost();

    	    	        			this.postToInv(arInvoiceLineItem, arReceipt.getRctDate(),
    	    	        					QTY_SLD, standardCost * QTY_SLD,
    	    	        					invCosting.getCstRemainingQuantity() - QTY_SLD, invCosting.getCstRemainingValue() - (standardCost * QTY_SLD),
    	    	        					0d, null, AD_BRNCH, AD_CMPNY);
    	    					}

    	    				}



    	    			}


        		}
            		// increase bank balance CASH
            		if(arReceipt.getRctAmountCash() > 0){

    				LocalAdBankAccount adBankAccount = adBankAccountHome.findByPrimaryKey(arReceipt.getAdBankAccount().getBaCode());

    				try {

    					// find bankaccount balance before or equal receipt date

    					Collection adBankAccountBalances = adBankAccountBalanceHome.findByBeforeOrEqualDateAndBaCodeAndType(arReceipt.getRctDate(), arReceipt.getAdBankAccount().getBaCode(), "BOOK", AD_CMPNY);

    					if (!adBankAccountBalances.isEmpty()) {

    						// get last check

    						ArrayList adBankAccountBalanceList = new ArrayList(adBankAccountBalances);

    						LocalAdBankAccountBalance adBankAccountBalance = (LocalAdBankAccountBalance)adBankAccountBalanceList.get(adBankAccountBalanceList.size() - 1);

    						if (adBankAccountBalance.getBabDate().before(arReceipt.getRctDate())) {

    							// create new balance

    							LocalAdBankAccountBalance adNewBankAccountBalance = adBankAccountBalanceHome.create(
    									arReceipt.getRctDate(), adBankAccountBalance.getBabBalance() + arReceipt.getRctAmountCash(), "BOOK", AD_CMPNY);

    							//adBankAccount.addAdBankAccountBalance(adNewBankAccountBalance);
    							adNewBankAccountBalance.setAdBankAccount(adBankAccount);
    						} else { // equals to check date

    							adBankAccountBalance.setBabBalance(adBankAccountBalance.getBabBalance() + arReceipt.getRctAmountCash());

    						}

    					} else {

    						// create new balance
    						System.out.println("arReceipt.getRctAmountCash()="+arReceipt.getRctAmountCash());
    						LocalAdBankAccountBalance adNewBankAccountBalance = adBankAccountBalanceHome.create(
    								arReceipt.getRctDate(), (arReceipt.getRctAmountCash()), "BOOK", AD_CMPNY);

    						//adBankAccount.addAdBankAccountBalance(adNewBankAccountBalance);
    						adNewBankAccountBalance.setAdBankAccount(adBankAccount);

    					}

    					// propagate to subsequent balances if necessary

    					adBankAccountBalances = adBankAccountBalanceHome.findByAfterDateAndBaCodeAndType(arReceipt.getRctDate(), arReceipt.getAdBankAccount().getBaCode(), "BOOK", AD_CMPNY);

    					Iterator i = adBankAccountBalances.iterator();

    					while (i.hasNext()) {

    						LocalAdBankAccountBalance adBankAccountBalance = (LocalAdBankAccountBalance)i.next();

    						adBankAccountBalance.setBabBalance(adBankAccountBalance.getBabBalance() + arReceipt.getRctAmountCash());

    					}

    				} catch (Exception ex) {

    					ex.printStackTrace();

    				}
            		}

    				// increase bank balance CARD 1
            		if(arReceipt.getRctAmountCard1() > 0){


            			LocalAdBankAccount adBankAccount = adBankAccountHome.findByPrimaryKey(arReceipt.getAdBankAccountCard1().getBaCode());

    				try {

    					// find bankaccount balance before or equal receipt date

    					Collection adBankAccountBalances = adBankAccountBalanceHome.findByBeforeOrEqualDateAndBaCodeAndType(arReceipt.getRctDate(), arReceipt.getAdBankAccountCard1().getBaCode(), "BOOK", AD_CMPNY);

    					if (!adBankAccountBalances.isEmpty()) {

    						// get last check

    						ArrayList adBankAccountBalanceList = new ArrayList(adBankAccountBalances);

    						LocalAdBankAccountBalance adBankAccountBalance = (LocalAdBankAccountBalance)adBankAccountBalanceList.get(adBankAccountBalanceList.size() - 1);

    						if (adBankAccountBalance.getBabDate().before(arReceipt.getRctDate())) {

    							// create new balance

    							LocalAdBankAccountBalance adNewBankAccountBalance = adBankAccountBalanceHome.create(
    									arReceipt.getRctDate(), adBankAccountBalance.getBabBalance() + arReceipt.getRctAmountCard1(), "BOOK", AD_CMPNY);

    							//adBankAccount.addAdBankAccountBalance(adNewBankAccountBalance);
    							adNewBankAccountBalance.setAdBankAccount(adBankAccount);
    						} else { // equals to check date

    							adBankAccountBalance.setBabBalance(adBankAccountBalance.getBabBalance() + arReceipt.getRctAmountCard1());

    						}

    					} else {

    						// create new balance
    						System.out.println("arReceipt.getRctAmountCard1()="+arReceipt.getRctAmountCard1());
    						LocalAdBankAccountBalance adNewBankAccountBalance = adBankAccountBalanceHome.create(
    								arReceipt.getRctDate(), (arReceipt.getRctAmountCard1()), "BOOK", AD_CMPNY);

    						//adBankAccount.addAdBankAccountBalance(adNewBankAccountBalance);
    						adNewBankAccountBalance.setAdBankAccount(adBankAccount);

    					}

    					// propagate to subsequent balances if necessary

    					adBankAccountBalances = adBankAccountBalanceHome.findByAfterDateAndBaCodeAndType(arReceipt.getRctDate(), arReceipt.getAdBankAccountCard1().getBaCode(), "BOOK", AD_CMPNY);

    					Iterator i = adBankAccountBalances.iterator();

    					while (i.hasNext()) {

    						LocalAdBankAccountBalance adBankAccountBalance = (LocalAdBankAccountBalance)i.next();

    						adBankAccountBalance.setBabBalance(adBankAccountBalance.getBabBalance() + arReceipt.getRctAmountCard1());

    					}

    				} catch (Exception ex) {

    					ex.printStackTrace();

    				}

            		}

    // increase bank balance CARD 2
            		if(arReceipt.getRctAmountCard2() > 0){
            			LocalAdBankAccount adBankAccount = adBankAccountHome.findByPrimaryKey(arReceipt.getAdBankAccountCard2().getBaCode());

    				try {

    					// find bankaccount balance before or equal receipt date

    					Collection adBankAccountBalances = adBankAccountBalanceHome.findByBeforeOrEqualDateAndBaCodeAndType(arReceipt.getRctDate(), arReceipt.getAdBankAccountCard2().getBaCode(), "BOOK", AD_CMPNY);

    					if (!adBankAccountBalances.isEmpty()) {

    						// get last check

    						ArrayList adBankAccountBalanceList = new ArrayList(adBankAccountBalances);

    						LocalAdBankAccountBalance adBankAccountBalance = (LocalAdBankAccountBalance)adBankAccountBalanceList.get(adBankAccountBalanceList.size() - 1);

    						if (adBankAccountBalance.getBabDate().before(arReceipt.getRctDate())) {

    							// create new balance

    							LocalAdBankAccountBalance adNewBankAccountBalance = adBankAccountBalanceHome.create(
    									arReceipt.getRctDate(), adBankAccountBalance.getBabBalance() + arReceipt.getRctAmountCard2(), "BOOK", AD_CMPNY);

    							//adBankAccount.addAdBankAccountBalance(adNewBankAccountBalance);
    							adNewBankAccountBalance.setAdBankAccount(adBankAccount);
    						} else { // equals to check date

    							adBankAccountBalance.setBabBalance(adBankAccountBalance.getBabBalance() + arReceipt.getRctAmountCard2());

    						}

    					} else {

    						// create new balance
    						System.out.println("arReceipt.getRctAmountCard2()="+arReceipt.getRctAmountCard2());
    						LocalAdBankAccountBalance adNewBankAccountBalance = adBankAccountBalanceHome.create(
    								arReceipt.getRctDate(), (arReceipt.getRctAmountCard2()), "BOOK", AD_CMPNY);

    						//adBankAccount.addAdBankAccountBalance(adNewBankAccountBalance);
    						adNewBankAccountBalance.setAdBankAccount(adBankAccount);

    					}

    					// propagate to subsequent balances if necessary

    					adBankAccountBalances = adBankAccountBalanceHome.findByAfterDateAndBaCodeAndType(arReceipt.getRctDate(), arReceipt.getAdBankAccountCard2().getBaCode(), "BOOK", AD_CMPNY);

    					Iterator i = adBankAccountBalances.iterator();

    					while (i.hasNext()) {

    						LocalAdBankAccountBalance adBankAccountBalance = (LocalAdBankAccountBalance)i.next();

    						adBankAccountBalance.setBabBalance(adBankAccountBalance.getBabBalance() + arReceipt.getRctAmountCard2());

    					}

    				} catch (Exception ex) {

    					ex.printStackTrace();

    				}
            		}

    // increase bank balance CARD 3
            		if(arReceipt.getRctAmountCard3() > 0){
            			LocalAdBankAccount adBankAccount = adBankAccountHome.findByPrimaryKey(arReceipt.getAdBankAccountCard3().getBaCode());

    				try {

    					// find bankaccount balance before or equal receipt date

    					Collection adBankAccountBalances = adBankAccountBalanceHome.findByBeforeOrEqualDateAndBaCodeAndType(arReceipt.getRctDate(), arReceipt.getAdBankAccountCard3().getBaCode(), "BOOK", AD_CMPNY);

    					if (!adBankAccountBalances.isEmpty()) {

    						// get last check

    						ArrayList adBankAccountBalanceList = new ArrayList(adBankAccountBalances);

    						LocalAdBankAccountBalance adBankAccountBalance = (LocalAdBankAccountBalance)adBankAccountBalanceList.get(adBankAccountBalanceList.size() - 1);

    						if (adBankAccountBalance.getBabDate().before(arReceipt.getRctDate())) {

    							// create new balance

    							LocalAdBankAccountBalance adNewBankAccountBalance = adBankAccountBalanceHome.create(
    									arReceipt.getRctDate(), adBankAccountBalance.getBabBalance() + arReceipt.getRctAmountCard3(), "BOOK", AD_CMPNY);

    							//adBankAccount.addAdBankAccountBalance(adNewBankAccountBalance);
    							adNewBankAccountBalance.setAdBankAccount(adBankAccount);
    						} else { // equals to check date

    							adBankAccountBalance.setBabBalance(adBankAccountBalance.getBabBalance() + arReceipt.getRctAmountCard3());

    						}

    					} else {

    						// create new balance
    						System.out.println("arReceipt.getRctAmountCard3()="+arReceipt.getRctAmountCard3());
    						LocalAdBankAccountBalance adNewBankAccountBalance = adBankAccountBalanceHome.create(
    								arReceipt.getRctDate(), (arReceipt.getRctAmountCard3()), "BOOK", AD_CMPNY);

    						//adBankAccount.addAdBankAccountBalance(adNewBankAccountBalance);
    						adNewBankAccountBalance.setAdBankAccount(adBankAccount);

    					}

    					// propagate to subsequent balances if necessary

    					adBankAccountBalances = adBankAccountBalanceHome.findByAfterDateAndBaCodeAndType(arReceipt.getRctDate(), arReceipt.getAdBankAccountCard3().getBaCode(), "BOOK", AD_CMPNY);

    					Iterator i = adBankAccountBalances.iterator();

    					while (i.hasNext()) {

    						LocalAdBankAccountBalance adBankAccountBalance = (LocalAdBankAccountBalance)i.next();

    						adBankAccountBalance.setBabBalance(adBankAccountBalance.getBabBalance() + arReceipt.getRctAmountCard3());

    					}

    				} catch (Exception ex) {

    					ex.printStackTrace();

    				}
            		}
            		// increase bank balance CHEQUE
            		if(arReceipt.getRctAmountCheque() > 0){
            			LocalAdBankAccount adBankAccount = adBankAccountHome.findByPrimaryKey(arReceipt.getAdBankAccount().getBaCode());

    				try {

    					// find bankaccount balance before or equal receipt date

    					Collection adBankAccountBalances = adBankAccountBalanceHome.findByBeforeOrEqualDateAndBaCodeAndType(arReceipt.getRctDate(), arReceipt.getAdBankAccount().getBaCode(), "BOOK", AD_CMPNY);

    					if (!adBankAccountBalances.isEmpty()) {

    						// get last check

    						ArrayList adBankAccountBalanceList = new ArrayList(adBankAccountBalances);

    						LocalAdBankAccountBalance adBankAccountBalance = (LocalAdBankAccountBalance)adBankAccountBalanceList.get(adBankAccountBalanceList.size() - 1);

    						if (adBankAccountBalance.getBabDate().before(arReceipt.getRctDate())) {

    							// create new balance

    							LocalAdBankAccountBalance adNewBankAccountBalance = adBankAccountBalanceHome.create(
    									arReceipt.getRctDate(), adBankAccountBalance.getBabBalance() + arReceipt.getRctAmountCheque(), "BOOK", AD_CMPNY);

    							//adBankAccount.addAdBankAccountBalance(adNewBankAccountBalance);
    							adNewBankAccountBalance.setAdBankAccount(adBankAccount);
    						} else { // equals to check date

    							adBankAccountBalance.setBabBalance(adBankAccountBalance.getBabBalance() + arReceipt.getRctAmountCheque());

    						}

    					} else {

    						// create new balance
    						System.out.println("arReceipt.getRctAmountCheque()="+arReceipt.getRctAmountCheque());
    						LocalAdBankAccountBalance adNewBankAccountBalance = adBankAccountBalanceHome.create(
    								arReceipt.getRctDate(), (arReceipt.getRctAmountCheque()), "BOOK", AD_CMPNY);

    						//adBankAccount.addAdBankAccountBalance(adNewBankAccountBalance);
    						adNewBankAccountBalance.setAdBankAccount(adBankAccount);

    					}

    					// propagate to subsequent balances if necessary

    					adBankAccountBalances = adBankAccountBalanceHome.findByAfterDateAndBaCodeAndType(arReceipt.getRctDate(), arReceipt.getAdBankAccount().getBaCode(), "BOOK", AD_CMPNY);

    					Iterator i = adBankAccountBalances.iterator();

    					while (i.hasNext()) {

    						LocalAdBankAccountBalance adBankAccountBalance = (LocalAdBankAccountBalance)i.next();

    						adBankAccountBalance.setBabBalance(adBankAccountBalance.getBabBalance() + arReceipt.getRctAmountCheque());

    					}

    				} catch (Exception ex) {

    					ex.printStackTrace();

    				}
            		}




            	   // set receipt post status

    	           arReceipt.setRctPosted(EJBCommon.TRUE);
    	           arReceipt.setRctPostedBy(USR_NM);
    	           arReceipt.setRctDatePosted(EJBCommon.getGcCurrentDateWoTime().getTime());





        		// increase bank balance

				LocalAdBankAccount adBankAccount = adBankAccountHome.findByPrimaryKey(arReceipt.getAdBankAccount().getBaCode());

				try {

					// find bankaccount balance before or equal receipt date

					Collection adBankAccountBalances = adBankAccountBalanceHome.findByBeforeOrEqualDateAndBaCodeAndType(arReceipt.getRctDate(), arReceipt.getAdBankAccount().getBaCode(), "BOOK", AD_CMPNY);

					if (!adBankAccountBalances.isEmpty()) {

						// get last check

						ArrayList adBankAccountBalanceList = new ArrayList(adBankAccountBalances);

						LocalAdBankAccountBalance adBankAccountBalance = (LocalAdBankAccountBalance)adBankAccountBalanceList.get(adBankAccountBalanceList.size() - 1);

						if (adBankAccountBalance.getBabDate().before(arReceipt.getRctDate())) {

							// create new balance

							LocalAdBankAccountBalance adNewBankAccountBalance = adBankAccountBalanceHome.create(
									arReceipt.getRctDate(), adBankAccountBalance.getBabBalance() + arReceipt.getRctAmount(), "BOOK", AD_CMPNY);

							adBankAccount.addAdBankAccountBalance(adNewBankAccountBalance);

						} else { // equals to check date

							adBankAccountBalance.setBabBalance(adBankAccountBalance.getBabBalance() + arReceipt.getRctAmount());

						}

					} else {

						// create new balance

						LocalAdBankAccountBalance adNewBankAccountBalance = adBankAccountBalanceHome.create(
								arReceipt.getRctDate(), (arReceipt.getRctAmount()), "BOOK", AD_CMPNY);

						adBankAccount.addAdBankAccountBalance(adNewBankAccountBalance);

					}

					// propagate to subsequent balances if necessary

					adBankAccountBalances = adBankAccountBalanceHome.findByAfterDateAndBaCodeAndType(arReceipt.getRctDate(), arReceipt.getAdBankAccount().getBaCode(), "BOOK", AD_CMPNY);

					Iterator i = adBankAccountBalances.iterator();

					while (i.hasNext()) {

						LocalAdBankAccountBalance adBankAccountBalance = (LocalAdBankAccountBalance)i.next();

						adBankAccountBalance.setBabBalance(adBankAccountBalance.getBabBalance() + arReceipt.getRctAmount());

					}

				} catch (Exception ex) {

					ex.printStackTrace();

				}

        		// set receipt post status

        		arReceipt.setRctPosted(EJBCommon.TRUE);
        		arReceipt.setRctPostedBy(USR_NM);
        		arReceipt.setRctDatePosted(EJBCommon.getGcCurrentDateWoTime().getTime());


        	} else if (arReceipt.getRctVoid() == EJBCommon.TRUE && arReceipt.getRctVoidPosted() == EJBCommon.FALSE) {



        		if (arReceipt.getRctType().equals("COLLECTION")) {

        			double RCT_CRDTS = 0d;
        			double RCT_AI_APPLD_DPSTS = 0d;

        			// decrease amount paid in invoice payment schedules and invoice

        			Collection arAppliedInvoices = arReceipt.getArAppliedInvoices();

        			Iterator i = arAppliedInvoices.iterator();

        			while (i.hasNext()) {

        				LocalArAppliedInvoice arAppliedInvoice = (LocalArAppliedInvoice)i.next();

        				LocalArInvoicePaymentSchedule arInvoicePaymentSchedule = arAppliedInvoice.getArInvoicePaymentSchedule();

        				double AMOUNT_PAID = arAppliedInvoice.getAiApplyAmount() + arAppliedInvoice.getAiCreditableWTax() + arAppliedInvoice.getAiDiscountAmount() + arAppliedInvoice.getAiAppliedDeposit() + arAppliedInvoice.getAiCreditBalancePaid() + arAppliedInvoice.getAiRebate();

        				double PENALTY_PAID = arAppliedInvoice.getAiPenaltyApplyAmount();


        				RCT_CRDTS += this.convertForeignToFunctionalCurrency(arAppliedInvoice.getArInvoicePaymentSchedule().getArInvoice().getGlFunctionalCurrency().getFcCode(),
        						arAppliedInvoice.getArInvoicePaymentSchedule().getArInvoice().getGlFunctionalCurrency().getFcName(),
								arAppliedInvoice.getArInvoicePaymentSchedule().getArInvoice().getInvConversionDate(),
								arAppliedInvoice.getArInvoicePaymentSchedule().getArInvoice().getInvConversionRate(),
								arAppliedInvoice.getAiDiscountAmount() + arAppliedInvoice.getAiCreditableWTax(), AD_CMPNY);

        				RCT_AI_APPLD_DPSTS += this.convertForeignToFunctionalCurrency(arAppliedInvoice.getArInvoicePaymentSchedule().getArInvoice().getGlFunctionalCurrency().getFcCode(),
        						arAppliedInvoice.getArInvoicePaymentSchedule().getArInvoice().getGlFunctionalCurrency().getFcName(),
								arAppliedInvoice.getArInvoicePaymentSchedule().getArInvoice().getInvConversionDate(),
								arAppliedInvoice.getArInvoicePaymentSchedule().getArInvoice().getInvConversionRate(),
								arAppliedInvoice.getAiAppliedDeposit(), AD_CMPNY);



        				try {

        	    	   		Collection arAppliedCredits = arAppliedInvoice.getArAppliedCredits();

        	    	   		System.out.println("arAppliedCredits="+arAppliedCredits.size());
        			  	    Iterator x = arAppliedCredits.iterator();

        			  	    while (x.hasNext()) {

        			  	   	    LocalArAppliedCredit arAppliedCredit = (LocalArAppliedCredit)x.next();

        			  	  	    x.remove();

        			  	  	    arAppliedCredit.remove();

        			  	    }

        	    	   	   }catch(Exception ex){


        	    	   	   }

        				arInvoicePaymentSchedule.setIpsAmountPaid(EJBCommon.roundIt(arInvoicePaymentSchedule.getIpsAmountPaid() - AMOUNT_PAID, this.getGlFcPrecisionUnit(AD_CMPNY)));
        				arInvoicePaymentSchedule.setIpsPenaltyPaid(EJBCommon.roundIt(arInvoicePaymentSchedule.getIpsPenaltyPaid() - PENALTY_PAID, this.getGlFcPrecisionUnit(AD_CMPNY)));

        				/*arInvoicePaymentSchedule.getArInvoice().setInvInterest(EJBCommon.TRUE);*/
        				arInvoicePaymentSchedule.getArInvoice().setInvAmountPaid(EJBCommon.roundIt(arInvoicePaymentSchedule.getArInvoice().getInvAmountPaid() - AMOUNT_PAID, this.getGlFcPrecisionUnit(AD_CMPNY)));
        				arInvoicePaymentSchedule.getArInvoice().setInvPenaltyPaid(EJBCommon.roundIt(arInvoicePaymentSchedule.getArInvoice().getInvPenaltyPaid() - PENALTY_PAID, this.getGlFcPrecisionUnit(AD_CMPNY)));


        			}


        			// increase customer balance

        			double RCT_AMNT = this.convertForeignToFunctionalCurrency(arReceipt.getGlFunctionalCurrency().getFcCode(),
        					arReceipt.getGlFunctionalCurrency().getFcName(),
							arReceipt.getRctConversionDate(), arReceipt.getRctConversionRate(),
							arReceipt.getRctAmount(), AD_CMPNY);

        			this.post(arReceipt.getRctDate(), RCT_AMNT + RCT_CRDTS + RCT_AI_APPLD_DPSTS, arReceipt.getArCustomer(), AD_CMPNY);

        		} else if (arReceipt.getRctType().equals("MISC") && !arReceipt.getArInvoiceLineItems().isEmpty()) {

        			Iterator c = arReceipt.getArInvoiceLineItems().iterator();

	    			while(c.hasNext()) {

	    				LocalArInvoiceLineItem arInvoiceLineItem = (LocalArInvoiceLineItem) c.next();

	    				String II_NM = arInvoiceLineItem.getInvItemLocation().getInvItem().getIiName();
	    				String LOC_NM = arInvoiceLineItem.getInvItemLocation().getInvLocation().getLocName();

	    				double QTY_SLD = this.convertByUomFromAndItemAndQuantity(arInvoiceLineItem.getInvUnitOfMeasure(),
	    						arInvoiceLineItem.getInvItemLocation().getInvItem(), arInvoiceLineItem.getIliQuantity(), AD_CMPNY);

	    				LocalInvCosting invCosting = null;

	    				try {

	    					invCosting  = invCostingHome.getByMaxCstDateToLongAndMaxCstLineNumberAndLessThanEqualCstDateAndIiNameAndLocName(arReceipt.getRctDate(), II_NM, LOC_NM, AD_BRNCH, AD_CMPNY);

	    				} catch (FinderException ex) {

	    				}

	    				double COST = arInvoiceLineItem.getInvItemLocation().getInvItem().getIiUnitCost();

	    				if (invCosting == null) {

							this.postToInv(arInvoiceLineItem, arReceipt.getRctDate(),
									-QTY_SLD,-QTY_SLD * COST,
									QTY_SLD, QTY_SLD * COST,
									0d, null, AD_BRNCH, AD_CMPNY);

	    				} else {


        					if(invCosting.getInvItemLocation().getInvItem().getIiCostMethod().equals("Average")) {

		    					double avgCost = Math.abs(invCosting.getCstRemainingValue() / invCosting.getCstRemainingQuantity());

								this.postToInv(arInvoiceLineItem, arReceipt.getRctDate(),
										-QTY_SLD, -COST * QTY_SLD,
										invCosting.getCstRemainingQuantity() + QTY_SLD, invCosting.getCstRemainingValue() + (COST * QTY_SLD),
										0d, USR_NM, AD_BRNCH, AD_CMPNY);

        					} else if(invCosting.getInvItemLocation().getInvItem().getIiCostMethod().equals("FIFO")) {

        						double fifoCost = this.getInvFifoCost(invCosting.getCstDate(), invCosting.getInvItemLocation().getIlCode(),
        	        					QTY_SLD, arInvoiceLineItem.getIliUnitPrice(), true, AD_BRNCH, AD_CMPNY);

        	        			this.postToInv(arInvoiceLineItem, arReceipt.getRctDate(),
        	        					-QTY_SLD, -fifoCost * QTY_SLD,
        	        					invCosting.getCstRemainingQuantity() + QTY_SLD, invCosting.getCstRemainingValue() + (fifoCost * QTY_SLD),
        	        					0d, USR_NM, AD_BRNCH, AD_CMPNY);

        					} else if(invCosting.getInvItemLocation().getInvItem().getIiCostMethod().equals("Standard")) {

        	        			double standardCost = invCosting.getInvItemLocation().getInvItem().getIiUnitCost();

        	        			this.postToInv(arInvoiceLineItem, arReceipt.getRctDate(),
        	        					-QTY_SLD, -standardCost * QTY_SLD,
        	        					invCosting.getCstRemainingQuantity() + QTY_SLD, invCosting.getCstRemainingValue() + (standardCost * QTY_SLD),
        	        					0d, USR_NM, AD_BRNCH, AD_CMPNY);

        					}

	    				}

	    			}


        		}

        		// decrease bank balance cash

         		if(arReceipt.getRctAmountCash() > 0){

 				LocalAdBankAccount adBankAccount = adBankAccountHome.findByPrimaryKey(arReceipt.getAdBankAccount().getBaCode());

 				try {

 					// find bankaccount balance before or equal receipt date

 					Collection adBankAccountBalances = adBankAccountBalanceHome.findByBeforeOrEqualDateAndBaCodeAndType(arReceipt.getRctDate(), arReceipt.getAdBankAccount().getBaCode(), "BOOK", AD_CMPNY);

 					if (!adBankAccountBalances.isEmpty()) {

 						// get last check

 						ArrayList adBankAccountBalanceList = new ArrayList(adBankAccountBalances);

 						LocalAdBankAccountBalance adBankAccountBalance = (LocalAdBankAccountBalance)adBankAccountBalanceList.get(adBankAccountBalanceList.size() - 1);

 						if (adBankAccountBalance.getBabDate().before(arReceipt.getRctDate())) {

 							// create new balance

 							LocalAdBankAccountBalance adNewBankAccountBalance = adBankAccountBalanceHome.create(
 									arReceipt.getRctDate(), adBankAccountBalance.getBabBalance() - arReceipt.getRctAmountCash(), "BOOK", AD_CMPNY);

 							//adBankAccount.addAdBankAccountBalance(adNewBankAccountBalance);
 							adNewBankAccountBalance.setAdBankAccount(adBankAccount);
 						} else { // equals to check date

 							adBankAccountBalance.setBabBalance(adBankAccountBalance.getBabBalance() - arReceipt.getRctAmountCash());

 						}

 					} else {

 						// create new balance
 						System.out.println("arReceipt.getRctAmountCash()="+arReceipt.getRctAmountCash());
 						LocalAdBankAccountBalance adNewBankAccountBalance = adBankAccountBalanceHome.create(
 								arReceipt.getRctDate(), (-arReceipt.getRctAmountCash()), "BOOK", AD_CMPNY);

 						//adBankAccount.addAdBankAccountBalance(adNewBankAccountBalance);
 						adNewBankAccountBalance.setAdBankAccount(adBankAccount);

 					}

 					// propagate to subsequent balances if necessary

 					adBankAccountBalances = adBankAccountBalanceHome.findByAfterDateAndBaCodeAndType(arReceipt.getRctDate(), arReceipt.getAdBankAccount().getBaCode(), "BOOK", AD_CMPNY);

 					Iterator i = adBankAccountBalances.iterator();

 					while (i.hasNext()) {

 						LocalAdBankAccountBalance adBankAccountBalance = (LocalAdBankAccountBalance)i.next();

 						adBankAccountBalance.setBabBalance(adBankAccountBalance.getBabBalance() - arReceipt.getRctAmountCash());

 					}

 				} catch (Exception ex) {

 					ex.printStackTrace();

 				}
         		}

 				// decrease bank balance CARD 1
         		if(arReceipt.getRctAmountCard1() > 0){


         			LocalAdBankAccount adBankAccount = adBankAccountHome.findByPrimaryKey(arReceipt.getAdBankAccountCard1().getBaCode());

 				try {

 					// find bankaccount balance before or equal receipt date

 					Collection adBankAccountBalances = adBankAccountBalanceHome.findByBeforeOrEqualDateAndBaCodeAndType(arReceipt.getRctDate(), arReceipt.getAdBankAccountCard1().getBaCode(), "BOOK", AD_CMPNY);

 					if (!adBankAccountBalances.isEmpty()) {

 						// get last check

 						ArrayList adBankAccountBalanceList = new ArrayList(adBankAccountBalances);

 						LocalAdBankAccountBalance adBankAccountBalance = (LocalAdBankAccountBalance)adBankAccountBalanceList.get(adBankAccountBalanceList.size() - 1);

 						if (adBankAccountBalance.getBabDate().before(arReceipt.getRctDate())) {

 							// create new balance

 							LocalAdBankAccountBalance adNewBankAccountBalance = adBankAccountBalanceHome.create(
 									arReceipt.getRctDate(), adBankAccountBalance.getBabBalance() - arReceipt.getRctAmountCard1(), "BOOK", AD_CMPNY);

 							//adBankAccount.addAdBankAccountBalance(adNewBankAccountBalance);
 							adNewBankAccountBalance.setAdBankAccount(adBankAccount);
 						} else { // equals to check date

 							adBankAccountBalance.setBabBalance(adBankAccountBalance.getBabBalance() - arReceipt.getRctAmountCard1());

 						}

 					} else {

 						// create new balance
 						System.out.println("arReceipt.getRctAmountCard1()="+arReceipt.getRctAmountCard1());
 						LocalAdBankAccountBalance adNewBankAccountBalance = adBankAccountBalanceHome.create(
 								arReceipt.getRctDate(), (-arReceipt.getRctAmountCard1()), "BOOK", AD_CMPNY);

 						//adBankAccount.addAdBankAccountBalance(adNewBankAccountBalance);
 						adNewBankAccountBalance.setAdBankAccount(adBankAccount);

 					}

 					// propagate to subsequent balances if necessary

 					adBankAccountBalances = adBankAccountBalanceHome.findByAfterDateAndBaCodeAndType(arReceipt.getRctDate(), arReceipt.getAdBankAccountCard1().getBaCode(), "BOOK", AD_CMPNY);

 					Iterator i = adBankAccountBalances.iterator();

 					while (i.hasNext()) {

 						LocalAdBankAccountBalance adBankAccountBalance = (LocalAdBankAccountBalance)i.next();

 						adBankAccountBalance.setBabBalance(adBankAccountBalance.getBabBalance() - arReceipt.getRctAmountCard1());

 					}

 				} catch (Exception ex) {

 					ex.printStackTrace();

 				}

         		}

         		// decrease bank balance CARD 2
         		if(arReceipt.getRctAmountCard2() > 0){
         			LocalAdBankAccount adBankAccount = adBankAccountHome.findByPrimaryKey(arReceipt.getAdBankAccountCard2().getBaCode());

 				try {

 					// find bankaccount balance before or equal receipt date

 					Collection adBankAccountBalances = adBankAccountBalanceHome.findByBeforeOrEqualDateAndBaCodeAndType(arReceipt.getRctDate(), arReceipt.getAdBankAccountCard2().getBaCode(), "BOOK", AD_CMPNY);

 					if (!adBankAccountBalances.isEmpty()) {

 						// get last check

 						ArrayList adBankAccountBalanceList = new ArrayList(adBankAccountBalances);

 						LocalAdBankAccountBalance adBankAccountBalance = (LocalAdBankAccountBalance)adBankAccountBalanceList.get(adBankAccountBalanceList.size() - 1);

 						if (adBankAccountBalance.getBabDate().before(arReceipt.getRctDate())) {

 							// create new balance

 							LocalAdBankAccountBalance adNewBankAccountBalance = adBankAccountBalanceHome.create(
 									arReceipt.getRctDate(), adBankAccountBalance.getBabBalance() - arReceipt.getRctAmountCard2(), "BOOK", AD_CMPNY);

 							//adBankAccount.addAdBankAccountBalance(adNewBankAccountBalance);
 							adNewBankAccountBalance.setAdBankAccount(adBankAccount);
 						} else { // equals to check date

 							adBankAccountBalance.setBabBalance(adBankAccountBalance.getBabBalance() - arReceipt.getRctAmountCard2());

 						}

 					} else {

 						// create new balance
 						System.out.println("arReceipt.getRctAmountCard2()="+arReceipt.getRctAmountCard2());
 						LocalAdBankAccountBalance adNewBankAccountBalance = adBankAccountBalanceHome.create(
 								arReceipt.getRctDate(), (-arReceipt.getRctAmountCard2()), "BOOK", AD_CMPNY);

 						//adBankAccount.addAdBankAccountBalance(adNewBankAccountBalance);
 						adNewBankAccountBalance.setAdBankAccount(adBankAccount);

 					}

 					// propagate to subsequent balances if necessary

 					adBankAccountBalances = adBankAccountBalanceHome.findByAfterDateAndBaCodeAndType(arReceipt.getRctDate(), arReceipt.getAdBankAccountCard2().getBaCode(), "BOOK", AD_CMPNY);

 					Iterator i = adBankAccountBalances.iterator();

 					while (i.hasNext()) {

 						LocalAdBankAccountBalance adBankAccountBalance = (LocalAdBankAccountBalance)i.next();

 						adBankAccountBalance.setBabBalance(adBankAccountBalance.getBabBalance() - arReceipt.getRctAmountCard2());

 					}

 				} catch (Exception ex) {

 					ex.printStackTrace();

 				}
         		}

         		// decrease bank balance CARD 3
         		if(arReceipt.getRctAmountCard3() > 0){
         			LocalAdBankAccount adBankAccount = adBankAccountHome.findByPrimaryKey(arReceipt.getAdBankAccountCard3().getBaCode());

 				try {

 					// find bankaccount balance before or equal receipt date

 					Collection adBankAccountBalances = adBankAccountBalanceHome.findByBeforeOrEqualDateAndBaCodeAndType(arReceipt.getRctDate(), arReceipt.getAdBankAccountCard3().getBaCode(), "BOOK", AD_CMPNY);

 					if (!adBankAccountBalances.isEmpty()) {

 						// get last check

 						ArrayList adBankAccountBalanceList = new ArrayList(adBankAccountBalances);

 						LocalAdBankAccountBalance adBankAccountBalance = (LocalAdBankAccountBalance)adBankAccountBalanceList.get(adBankAccountBalanceList.size() - 1);

 						if (adBankAccountBalance.getBabDate().before(arReceipt.getRctDate())) {

 							// create new balance

 							LocalAdBankAccountBalance adNewBankAccountBalance = adBankAccountBalanceHome.create(
 									arReceipt.getRctDate(), adBankAccountBalance.getBabBalance() - arReceipt.getRctAmountCard3(), "BOOK", AD_CMPNY);

 							//adBankAccount.addAdBankAccountBalance(adNewBankAccountBalance);
 							adNewBankAccountBalance.setAdBankAccount(adBankAccount);
 						} else { // equals to check date

 							adBankAccountBalance.setBabBalance(adBankAccountBalance.getBabBalance() + arReceipt.getRctAmountCard3());

 						}

 					} else {

 						// create new balance
 						System.out.println("arReceipt.getRctAmountCard3()="+arReceipt.getRctAmountCard3());
 						LocalAdBankAccountBalance adNewBankAccountBalance = adBankAccountBalanceHome.create(
 								arReceipt.getRctDate(), (-arReceipt.getRctAmountCard3()), "BOOK", AD_CMPNY);

 						//adBankAccount.addAdBankAccountBalance(adNewBankAccountBalance);
 						adNewBankAccountBalance.setAdBankAccount(adBankAccount);

 					}

 					// propagate to subsequent balances if necessary

 					adBankAccountBalances = adBankAccountBalanceHome.findByAfterDateAndBaCodeAndType(arReceipt.getRctDate(), arReceipt.getAdBankAccountCard3().getBaCode(), "BOOK", AD_CMPNY);

 					Iterator i = adBankAccountBalances.iterator();

 					while (i.hasNext()) {

 						LocalAdBankAccountBalance adBankAccountBalance = (LocalAdBankAccountBalance)i.next();

 						adBankAccountBalance.setBabBalance(adBankAccountBalance.getBabBalance() - arReceipt.getRctAmountCard3());

 					}

 				} catch (Exception ex) {

 					ex.printStackTrace();

 				}
         		}
         		// decrease bank balance CHEQUE
         		if(arReceipt.getRctAmountCheque() > 0){
         			LocalAdBankAccount adBankAccount = adBankAccountHome.findByPrimaryKey(arReceipt.getAdBankAccount().getBaCode());

 				try {

 					// find bankaccount balance before or equal receipt date

 					Collection adBankAccountBalances = adBankAccountBalanceHome.findByBeforeOrEqualDateAndBaCodeAndType(arReceipt.getRctDate(), arReceipt.getAdBankAccount().getBaCode(), "BOOK", AD_CMPNY);

 					if (!adBankAccountBalances.isEmpty()) {

 						// get last check

 						ArrayList adBankAccountBalanceList = new ArrayList(adBankAccountBalances);

 						LocalAdBankAccountBalance adBankAccountBalance = (LocalAdBankAccountBalance)adBankAccountBalanceList.get(adBankAccountBalanceList.size() - 1);

 						if (adBankAccountBalance.getBabDate().before(arReceipt.getRctDate())) {

 							// create new balance

 							LocalAdBankAccountBalance adNewBankAccountBalance = adBankAccountBalanceHome.create(
 									arReceipt.getRctDate(), adBankAccountBalance.getBabBalance() - arReceipt.getRctAmountCheque(), "BOOK", AD_CMPNY);

 							//adBankAccount.addAdBankAccountBalance(adNewBankAccountBalance);
 							adNewBankAccountBalance.setAdBankAccount(adBankAccount);
 						} else { // equals to check date

 							adBankAccountBalance.setBabBalance(adBankAccountBalance.getBabBalance() - arReceipt.getRctAmountCheque());

 						}

 					} else {

 						// create new balance
 						System.out.println("arReceipt.getRctAmountCheque()="+arReceipt.getRctAmountCheque());
 						LocalAdBankAccountBalance adNewBankAccountBalance = adBankAccountBalanceHome.create(
 								arReceipt.getRctDate(), (-arReceipt.getRctAmountCheque()), "BOOK", AD_CMPNY);

 						//adBankAccount.addAdBankAccountBalance(adNewBankAccountBalance);
 						adNewBankAccountBalance.setAdBankAccount(adBankAccount);

 					}

 					// propagate to subsequent balances if necessary

 					adBankAccountBalances = adBankAccountBalanceHome.findByAfterDateAndBaCodeAndType(arReceipt.getRctDate(), arReceipt.getAdBankAccount().getBaCode(), "BOOK", AD_CMPNY);

 					Iterator i = adBankAccountBalances.iterator();

 					while (i.hasNext()) {

 						LocalAdBankAccountBalance adBankAccountBalance = (LocalAdBankAccountBalance)i.next();

 						adBankAccountBalance.setBabBalance(adBankAccountBalance.getBabBalance() - arReceipt.getRctAmountCheque());

 					}

 				} catch (Exception ex) {

 					ex.printStackTrace();

 				}
         		}




         	   // set receipt post status

 	           arReceipt.setRctPosted(EJBCommon.TRUE);
 	           arReceipt.setRctPostedBy(USR_NM);
 	           arReceipt.setRctDatePosted(EJBCommon.getGcCurrentDateWoTime().getTime());

        	}

        	// post to gl if necessary

        	if (adPreference.getPrfArGlPostingType().equals("AUTO-POST UPON APPROVAL")) {

        		// validate if date has no period and period is closed

        		LocalGlSetOfBook glJournalSetOfBook = null;

        		try {

        			glJournalSetOfBook = glSetOfBookHome.findByDate(arReceipt.getRctDate(), AD_CMPNY);

        		} catch (FinderException ex) {

        			throw new GlJREffectiveDateNoPeriodExistException();

        		}

        		LocalGlAccountingCalendarValue glAccountingCalendarValue =
        			glAccountingCalendarValueHome.findByAcCodeAndDate(
        					glJournalSetOfBook.getGlAccountingCalendar().getAcCode(), arReceipt.getRctDate(), AD_CMPNY);


        		if (glAccountingCalendarValue.getAcvStatus() == 'N' ||
        				glAccountingCalendarValue.getAcvStatus() == 'C' ||
						glAccountingCalendarValue.getAcvStatus() == 'P') {

        			throw new GlJREffectiveDatePeriodClosedException();

        		}

        		// check if invoice is balance if not check suspense posting

        		LocalGlJournalLine glOffsetJournalLine = null;

        		Collection arDistributionRecords = null;

        		if (arReceipt.getRctVoid() == EJBCommon.FALSE) {

        			arDistributionRecords = arDistributionRecordHome.findImportableDrByDrReversedAndRctCode(EJBCommon.FALSE, arReceipt.getRctCode(), AD_CMPNY);

        		} else {

        			arDistributionRecords = arDistributionRecordHome.findImportableDrByDrReversedAndRctCode(EJBCommon.TRUE, arReceipt.getRctCode(), AD_CMPNY);

        		}


        		Iterator j = arDistributionRecords.iterator();

        		double TOTAL_DEBIT = 0d;
        		double TOTAL_CREDIT = 0d;

        		while (j.hasNext()) {

        			LocalArDistributionRecord arDistributionRecord = (LocalArDistributionRecord)j.next();

        			double DR_AMNT = 0d;

        			if (arDistributionRecord.getArAppliedInvoice() != null) {

        				LocalArInvoice arInvoice = arDistributionRecord.getArAppliedInvoice().getArInvoicePaymentSchedule().getArInvoice();

        				DR_AMNT = this.convertForeignToFunctionalCurrency(arInvoice.getGlFunctionalCurrency().getFcCode(),
        						arInvoice.getGlFunctionalCurrency().getFcName(),
								arInvoice.getInvConversionDate(),
								arInvoice.getInvConversionRate(),
								arDistributionRecord.getDrAmount(), AD_CMPNY);

        			} else {

        				DR_AMNT = this.convertForeignToFunctionalCurrency(arReceipt.getGlFunctionalCurrency().getFcCode(),
        						arReceipt.getGlFunctionalCurrency().getFcName(),
								arReceipt.getRctConversionDate(),
								arReceipt.getRctConversionRate(),
								arDistributionRecord.getDrAmount(), AD_CMPNY);

        			}

        			if (arDistributionRecord.getDrDebit() == EJBCommon.TRUE) {

        				TOTAL_DEBIT += DR_AMNT;

        			} else {

        				TOTAL_CREDIT += DR_AMNT;

        			}

        		}

        		TOTAL_DEBIT = EJBCommon.roundIt(TOTAL_DEBIT, adCompany.getGlFunctionalCurrency().getFcPrecision());
        		TOTAL_CREDIT = EJBCommon.roundIt(TOTAL_CREDIT, adCompany.getGlFunctionalCurrency().getFcPrecision());

        		if (adPreference.getPrfAllowSuspensePosting() == EJBCommon.TRUE &&
        				TOTAL_DEBIT != TOTAL_CREDIT) {

        			LocalGlSuspenseAccount glSuspenseAccount = null;

        			try {

        				glSuspenseAccount = glSuspenseAccountHome.findByJsNameAndJcName("ACCOUNTS RECEIVABLES", "SALES RECEIPTS", AD_CMPNY);

        			} catch (FinderException ex) {

        				throw new GlobalJournalNotBalanceException();

        			}

        			if (TOTAL_DEBIT - TOTAL_CREDIT < 0) {

        				glOffsetJournalLine = glJournalLineHome.create((short)(arDistributionRecords.size() + 1),
								EJBCommon.TRUE, TOTAL_CREDIT - TOTAL_DEBIT, "", AD_CMPNY);

        			} else {

        				glOffsetJournalLine = glJournalLineHome.create((short)(arDistributionRecords.size() + 1),
								EJBCommon.FALSE, TOTAL_DEBIT - TOTAL_CREDIT, "", AD_CMPNY);

        			}

        			LocalGlChartOfAccount glChartOfAccount = glSuspenseAccount.getGlChartOfAccount();
        			glChartOfAccount.addGlJournalLine(glOffsetJournalLine);


        		} else if (adPreference.getPrfAllowSuspensePosting() == EJBCommon.FALSE &&
        				TOTAL_DEBIT != TOTAL_CREDIT) {

        			throw new GlobalJournalNotBalanceException();

        		}

        		// create journal batch if necessary

        		LocalGlJournalBatch glJournalBatch = null;
        		java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat("MM/dd/yyyy");

        		try {

        			if (adPreference.getPrfEnableArReceiptBatch() == EJBCommon.TRUE) {

        				glJournalBatch = glJournalBatchHome.findByJbName("JOURNAL IMPORT " + formatter.format(new Date()) + " " + arReceipt.getArReceiptBatch().getRbName(), AD_BRNCH, AD_CMPNY);

        			} else {

        				glJournalBatch = glJournalBatchHome.findByJbName("JOURNAL IMPORT " + formatter.format(new Date()) + " SALES RECEIPTS", AD_BRNCH, AD_CMPNY);

        			}


        		} catch (FinderException ex) {
        		}

        		if (adPreference.getPrfEnableGlJournalBatch() ==  EJBCommon.TRUE &&
        				glJournalBatch == null) {

        			if (adPreference.getPrfEnableArReceiptBatch() == EJBCommon.TRUE) {

        				glJournalBatch = glJournalBatchHome.create("JOURNAL IMPORT " + formatter.format(new Date()) + " " + arReceipt.getArReceiptBatch().getRbName(), "JOURNAL IMPORT", "CLOSED", EJBCommon.getGcCurrentDateWoTime().getTime(), USR_NM, AD_BRNCH, AD_CMPNY);

        			} else {

        				glJournalBatch = glJournalBatchHome.create("JOURNAL IMPORT " + formatter.format(new Date()) + " SALES RECEIPTS", "JOURNAL IMPORT", "CLOSED", EJBCommon.getGcCurrentDateWoTime().getTime(), USR_NM, AD_BRNCH, AD_CMPNY);

        			}


        		}

        		// create journal entry

        		LocalGlJournal glJournal = glJournalHome.create(arReceipt.getRctReferenceNumber(),
        				arReceipt.getRctDescription(), arReceipt.getRctDate(),
						0.0d, null, arReceipt.getRctNumber(), null, 1d, "N/A", null,
						'N', EJBCommon.TRUE, EJBCommon.FALSE,
						USR_NM, new Date(),
						USR_NM, new Date(),
						null, null,
						USR_NM, EJBCommon.getGcCurrentDateWoTime().getTime(),
						arReceipt.getArCustomer().getCstTin(),
						arReceipt.getArCustomer().getCstName(), EJBCommon.FALSE,
						null,
						AD_BRNCH, AD_CMPNY);

        		LocalGlJournalSource glJournalSource = glJournalSourceHome.findByJsName("ACCOUNTS RECEIVABLES", AD_CMPNY);
        		glJournal.setGlJournalSource(glJournalSource);

        		LocalGlFunctionalCurrency glFunctionalCurrency = glFunctionalCurrencyHome.findByFcName(adCompany.getGlFunctionalCurrency().getFcName(), AD_CMPNY);
        		glJournal.setGlFunctionalCurrency(glFunctionalCurrency);

        		LocalGlJournalCategory glJournalCategory = glJournalCategoryHome.findByJcName("SALES RECEIPTS", AD_CMPNY);
        		glJournal.setGlJournalCategory(glJournalCategory);

        		if (glJournalBatch != null) {

        			glJournal.setGlJournalBatch(glJournalBatch);

        		}


        		// create journal lines

        		j = arDistributionRecords.iterator();
        		boolean firstFlag = true;

        		while (j.hasNext()) {

        			LocalArDistributionRecord arDistributionRecord = (LocalArDistributionRecord)j.next();

        			double DR_AMNT = 0d;
        			LocalArInvoice arInvoice = null;

        			if (arDistributionRecord.getArAppliedInvoice() != null) {

        				arInvoice = arDistributionRecord.getArAppliedInvoice().getArInvoicePaymentSchedule().getArInvoice();

        				DR_AMNT = this.convertForeignToFunctionalCurrency(arInvoice.getGlFunctionalCurrency().getFcCode(),
        						arInvoice.getGlFunctionalCurrency().getFcName(),
								arInvoice.getInvConversionDate(),
								arInvoice.getInvConversionRate(),
								arDistributionRecord.getDrAmount(), AD_CMPNY);

        			} else {

        				DR_AMNT = this.convertForeignToFunctionalCurrency(arReceipt.getGlFunctionalCurrency().getFcCode(),
        						arReceipt.getGlFunctionalCurrency().getFcName(),
								arReceipt.getRctConversionDate(),
								arReceipt.getRctConversionRate(),
								arDistributionRecord.getDrAmount(), AD_CMPNY);

        			}

        			LocalGlJournalLine glJournalLine = glJournalLineHome.create(arDistributionRecord.getDrLine(),
							arDistributionRecord.getDrDebit(), DR_AMNT, "", AD_CMPNY);

        			arDistributionRecord.getGlChartOfAccount().addGlJournalLine(glJournalLine);

        			glJournal.addGlJournalLine(glJournalLine);

        			arDistributionRecord.setDrImported(EJBCommon.TRUE);

    		       	// for FOREX revaluation

            	    int FC_CODE = arDistributionRecord.getArAppliedInvoice() != null ?
            	    	arInvoice.getGlFunctionalCurrency().getFcCode().intValue() :
            	    	arReceipt.getGlFunctionalCurrency().getFcCode().intValue();

    		       	if((FC_CODE != adCompany.getGlFunctionalCurrency().getFcCode().intValue()) &&
    					glJournalLine.getGlChartOfAccount().getGlFunctionalCurrency() != null && (FC_CODE ==
    						glJournalLine.getGlChartOfAccount().getGlFunctionalCurrency().getFcCode().intValue())){

    		       		double CONVERSION_RATE = arDistributionRecord.getArAppliedInvoice() != null ?
    		            	    arInvoice.getInvConversionRate() : arReceipt.getRctConversionRate();

    		            Date DATE = arDistributionRecord.getArAppliedInvoice() != null ?
    	    		    	arInvoice.getInvConversionDate() : arReceipt.getRctConversionDate();

    		            if (DATE != null && (CONVERSION_RATE == 0 || CONVERSION_RATE == 1)){

    		  	  			CONVERSION_RATE = this.getFrRateByFrNameAndFrDate(
    			  	  				glJournalLine.getGlChartOfAccount().getGlFunctionalCurrency().getFcName(),
    								glJournal.getJrConversionDate(), AD_CMPNY);

    		            } else if (CONVERSION_RATE == 0) {

    		            	CONVERSION_RATE = 1;

    		       		}

    		       		Collection glForexLedgers = null;

    		       		DATE = arDistributionRecord.getArAppliedInvoice() != null ?
        	    		    arInvoice.getInvDate() : arReceipt.getRctDate();

    		       		try {

    		       			glForexLedgers = glForexLedgerHome.findLatestGlFrlByFrlDateAndByCoaCode(
    		       					DATE, glJournalLine.getGlChartOfAccount().getCoaCode(),
    								AD_CMPNY);

    		       		} catch(FinderException ex) {

    		       		}

    		       		LocalGlForexLedger glForexLedger =
    		       			(glForexLedgers.isEmpty() || glForexLedgers == null) ? null :
    		       				(LocalGlForexLedger) glForexLedgers.iterator().next();

    		       		int FRL_LN = (glForexLedger != null &&
    		       			glForexLedger.getFrlDate().compareTo(DATE) == 0) ?
    		       				glForexLedger.getFrlLine().intValue() + 1 : 1;

    		       		// compute balance
    		       		double COA_FRX_BLNC = glForexLedger == null ? 0 : glForexLedger.getFrlBalance();
    		       		double FRL_AMNT = arDistributionRecord.getDrAmount();

    		       		if(glJournalLine.getGlChartOfAccount().getCoaAccountType().equalsIgnoreCase("ASSET"))
    		       			FRL_AMNT = (glJournalLine.getJlDebit() == EJBCommon.TRUE ? FRL_AMNT : (- 1 * FRL_AMNT));
    		       		else
    		       			FRL_AMNT = (glJournalLine.getJlDebit() == EJBCommon.TRUE ? (- 1 * FRL_AMNT) : FRL_AMNT);

    		       		COA_FRX_BLNC = COA_FRX_BLNC + FRL_AMNT;

    		       		double FRX_GN_LSS = 0d;

    		       		if (glOffsetJournalLine != null && firstFlag) {

    		       			if(glOffsetJournalLine.getGlChartOfAccount().getCoaAccountType().equalsIgnoreCase("ASSET"))

    		       				FRX_GN_LSS = (glOffsetJournalLine.getJlDebit() == EJBCommon.TRUE ?
    		       						glOffsetJournalLine.getJlAmount() : (- 1 * glOffsetJournalLine.getJlAmount()));

    		       			else

    		       				FRX_GN_LSS = (glOffsetJournalLine.getJlDebit() == EJBCommon.TRUE ?
    		       						(- 1 * glOffsetJournalLine.getJlAmount()) : glOffsetJournalLine.getJlAmount());

    		       			firstFlag = false;

    		       		}

    		       		glForexLedger = glForexLedgerHome.create(DATE, new Integer (FRL_LN), "OR", FRL_AMNT,
    		       				CONVERSION_RATE, COA_FRX_BLNC, FRX_GN_LSS, AD_CMPNY);

    		       		glJournalLine.getGlChartOfAccount().addGlForexLedger(glForexLedger);

    		       		// propagate balances
    		       		try{

    		       			glForexLedgers = glForexLedgerHome.findByGreaterThanFrlDateAndCoaCode(
    		       					glForexLedger.getFrlDate(), glForexLedger.getGlChartOfAccount().getCoaCode(),
    								glForexLedger.getFrlAdCompany());

    		       		} catch (FinderException ex) {

    		       		}

    		       		Iterator itrFrl = glForexLedgers.iterator();

    		       		while (itrFrl.hasNext()) {

    		       			glForexLedger = (LocalGlForexLedger) itrFrl.next();
    		       			FRL_AMNT = arDistributionRecord.getDrAmount();

    		       			if(glJournalLine.getGlChartOfAccount().getCoaAccountType().equalsIgnoreCase("ASSET"))
    		       				FRL_AMNT = (glJournalLine.getJlDebit() == EJBCommon.TRUE ? FRL_AMNT :
    		       					(- 1 * FRL_AMNT));
    		       			else
    		       				FRL_AMNT = (glJournalLine.getJlDebit() == EJBCommon.TRUE ? (- 1 * FRL_AMNT) :
    		       					FRL_AMNT);

    		       			glForexLedger.setFrlBalance(glForexLedger.getFrlBalance() + FRL_AMNT);

    		       		}

    		       	}

        		}

        		if (glOffsetJournalLine != null) {

        			glJournal.addGlJournalLine(glOffsetJournalLine);

        		}

        		// post journal to gl

        		Collection glJournalLines = glJournal.getGlJournalLines();

        		Iterator i = glJournalLines.iterator();

        		while (i.hasNext()) {

        			LocalGlJournalLine glJournalLine = (LocalGlJournalLine)i.next();

        			// post current to current acv

        			this.postToGl(glAccountingCalendarValue,
        					glJournalLine.getGlChartOfAccount(),
							true, glJournalLine.getJlDebit(), glJournalLine.getJlAmount(), AD_CMPNY);


        			// post to subsequent acvs (propagate)

        			Collection glSubsequentAccountingCalendarValues =
        				glAccountingCalendarValueHome.findSubsequentAcvByAcCodeAndAcvPeriodNumber(
        						glJournalSetOfBook.getGlAccountingCalendar().getAcCode(),
								glAccountingCalendarValue.getAcvPeriodNumber(), AD_CMPNY);

        			Iterator acvsIter = glSubsequentAccountingCalendarValues.iterator();

        			while (acvsIter.hasNext()) {

        				LocalGlAccountingCalendarValue glSubsequentAccountingCalendarValue =
        					(LocalGlAccountingCalendarValue)acvsIter.next();

        				this.postToGl(glSubsequentAccountingCalendarValue,
        						glJournalLine.getGlChartOfAccount(),
								false, glJournalLine.getJlDebit(), glJournalLine.getJlAmount(), AD_CMPNY);

        			}

        			// post to subsequent years if necessary

        			Collection glSubsequentSetOfBooks = glSetOfBookHome.findSubsequentSobByAcYear(glJournalSetOfBook.getGlAccountingCalendar().getAcYear(), AD_CMPNY);

        			if (!glSubsequentSetOfBooks.isEmpty() && glJournalSetOfBook.getSobYearEndClosed() == 1) {

        				adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
        				LocalGlChartOfAccount glRetainedEarningsAccount = glChartOfAccountHome.findByCoaAccountNumber(adCompany.getCmpRetainedEarnings(), AD_CMPNY);

        				Iterator sobIter = glSubsequentSetOfBooks.iterator();

        				while (sobIter.hasNext()) {

        					LocalGlSetOfBook glSubsequentSetOfBook = (LocalGlSetOfBook)sobIter.next();

        					String ACCOUNT_TYPE = glJournalLine.getGlChartOfAccount().getCoaAccountType();

        					// post to subsequent acvs of subsequent set of book(propagate)

        					Collection glAccountingCalendarValues =
        						glAccountingCalendarValueHome.findByAcCode(glSubsequentSetOfBook.getGlAccountingCalendar().getAcCode(), AD_CMPNY);

        					Iterator acvIter = glAccountingCalendarValues.iterator();

        					while (acvIter.hasNext()) {

        						LocalGlAccountingCalendarValue glSubsequentAccountingCalendarValue =
        							(LocalGlAccountingCalendarValue)acvIter.next();

        						if (ACCOUNT_TYPE.equals("ASSET") || ACCOUNT_TYPE.equals("LIABILITY") ||
        								ACCOUNT_TYPE.equals("OWNERS EQUITY")) {

        							this.postToGl(glSubsequentAccountingCalendarValue,
        									glJournalLine.getGlChartOfAccount(),
											false, glJournalLine.getJlDebit(), glJournalLine.getJlAmount(), AD_CMPNY);

        						} else { // revenue & expense

        							this.postToGl(glSubsequentAccountingCalendarValue,
        									glRetainedEarningsAccount,
											false, glJournalLine.getJlDebit(), glJournalLine.getJlAmount(), AD_CMPNY);

        						}

        					}

        					if (glSubsequentSetOfBook.getSobYearEndClosed() == 0) break;

        				}

        			}

        		}

        	}

        } catch (GlJREffectiveDateNoPeriodExistException ex) {

        	getSessionContext().setRollbackOnly();
        	throw ex;

        } catch (GlJREffectiveDatePeriodClosedException ex) {

        	getSessionContext().setRollbackOnly();
        	throw ex;

        } catch (GlobalJournalNotBalanceException ex) {

        	getSessionContext().setRollbackOnly();
        	throw ex;

        } catch (GlobalRecordAlreadyDeletedException ex) {

        	getSessionContext().setRollbackOnly();
        	throw ex;

        } catch (GlobalTransactionAlreadyPostedException ex) {

        	getSessionContext().setRollbackOnly();
        	throw ex;

        } catch (GlobalTransactionAlreadyVoidPostedException ex) {

        	getSessionContext().setRollbackOnly();
        	throw ex;

        } catch (GlobalBranchAccountNumberInvalidException ex) {

        	getSessionContext().setRollbackOnly();
        	throw ex;

        } catch (AdPRFCoaGlVarianceAccountNotFoundException ex) {

        	getSessionContext().setRollbackOnly();
        	throw ex;

    	} catch (Exception ex) {

    		Debug.printStackTrace(ex);
    		getSessionContext().setRollbackOnly();
    		throw new EJBException(ex.getMessage());

    	}

    }



    private double getInvFifoCost(Date CST_DT, Integer IL_CODE, double CST_QTY, double CST_COST,
   		 boolean isAdjustFifo, Integer AD_BRNCH, Integer AD_CMPNY)
	 {

		 LocalInvCostingHome invCostingHome = null;
	  	 LocalInvItemLocationHome invItemLocationHome = null;

	     // Initialize EJB Home

	     try {

	    	 invCostingHome = (LocalInvCostingHome)EJBHomeFactory.
	    	 	lookUpLocalHome(LocalInvCostingHome.JNDI_NAME, LocalInvCostingHome.class);
	    	 invItemLocationHome = (LocalInvItemLocationHome)EJBHomeFactory.
	    	 	lookUpLocalHome(LocalInvItemLocationHome.JNDI_NAME, LocalInvItemLocationHome.class);
	     }
	     catch (NamingException ex) {

	    	 throw new EJBException(ex.getMessage());
	     }

		try {

			Collection invFifoCostings = invCostingHome.findFifoRemainingQuantityByLessThanOrEqualCstDateAndIlCodeAndBrCode(CST_DT, IL_CODE, AD_BRNCH, AD_CMPNY);

			if (invFifoCostings.size() > 0) {

				Iterator x = invFifoCostings.iterator();

	  			if (isAdjustFifo) {

	  				//executed during POST transaction

	  				double totalCost = 0d;
	  				double cost;

	  				if(CST_QTY < 0) {

	  					//for negative quantities
	 	  				double neededQty = -(CST_QTY);

	 	  				while(x.hasNext() && neededQty != 0) {

	 	 	  				LocalInvCosting invFifoCosting = (LocalInvCosting)x.next();

	 		 	  			if (invFifoCosting.getApPurchaseOrderLine() != null || invFifoCosting.getApVoucherLineItem() != null) {
	 		 	  				cost = invFifoCosting.getCstItemCost() / invFifoCosting.getCstQuantityReceived();
	 		 	  			} else if(invFifoCosting.getArInvoiceLineItem() != null) {
	 		 	  				cost = invFifoCosting.getCstCostOfSales() / invFifoCosting.getCstQuantitySold();
	 		 	  			} else if (invFifoCosting.getInvBuildUnbuildAssemblyLine() != null) {
	 		 	  				cost = invFifoCosting.getCstAssemblyCost() / invFifoCosting.getCstAssemblyQuantity();
	 		 	  			} else {
	 		 	  				cost = invFifoCosting.getCstAdjustCost() /invFifoCosting.getCstAdjustQuantity();
	 		 	  			}

	 	  					if(neededQty <= invFifoCosting.getCstRemainingLifoQuantity()) {

	 			  				invFifoCosting.setCstRemainingLifoQuantity(invFifoCosting.getCstRemainingLifoQuantity() - neededQty);
	 			  				totalCost += (neededQty * cost);
	 			  				neededQty = 0d;
	 	  					} else {

	 	  						neededQty -= invFifoCosting.getCstRemainingLifoQuantity();
	 	  						totalCost += (invFifoCosting.getCstRemainingLifoQuantity() * cost);
	 	  						invFifoCosting.setCstRemainingLifoQuantity(0);
	 	  					}
	 	  				}

	 	  				//if needed qty is not yet satisfied but no more quantities to fetch, get the default cost
	 	  				if(neededQty != 0) {

	 	  					LocalInvItemLocation invItemLocation = invItemLocationHome.findByPrimaryKey(IL_CODE);
	 	  					totalCost += (neededQty * invItemLocation.getInvItem().getIiUnitCost());
	 	  				}

	 	  				cost = totalCost / -CST_QTY;
	  				}

	  				else {

	  					//for positive quantities
	  					cost = CST_COST;
	  				}
	  				return cost;
	  			}

	  			else {

	  				//executed during ENTRY transaction

	  				LocalInvCosting invFifoCosting = (LocalInvCosting)x.next();

	 	  			if (invFifoCosting.getApPurchaseOrderLine() != null || invFifoCosting.getApVoucherLineItem() != null) {
	 	  				return EJBCommon.roundIt(invFifoCosting.getCstItemCost() / invFifoCosting.getCstQuantityReceived(), this.getGlFcPrecisionUnit(AD_CMPNY));
	 	  			} else if(invFifoCosting.getArInvoiceLineItem() != null) {
		 	  			return  EJBCommon.roundIt(invFifoCosting.getCstCostOfSales() / invFifoCosting.getCstQuantitySold(), this.getGlFcPrecisionUnit(AD_CMPNY));
	 	  			} else if (invFifoCosting.getInvBuildUnbuildAssemblyLine() != null) {
	 	  				return EJBCommon.roundIt(invFifoCosting.getCstAssemblyCost() / invFifoCosting.getCstAssemblyQuantity(), this.getGlFcPrecisionUnit(AD_CMPNY));
	 	  			} else {
	 	  				return EJBCommon.roundIt(invFifoCosting.getCstAdjustCost() /invFifoCosting.getCstAdjustQuantity(), this.getGlFcPrecisionUnit(AD_CMPNY));
	 	  			}
	  			}
			}
			else {

				//most applicable in 1st entries of data
				LocalInvItemLocation invItemLocation = invItemLocationHome.findByPrimaryKey(IL_CODE);
				return invItemLocation.getInvItem().getIiUnitCost();
			}

		}
		catch (Exception ex) {
			Debug.printStackTrace(ex);
		    throw new EJBException(ex.getMessage());
		}
	}


    private void post(Date INV_DT, double INV_AMNT, LocalArCustomer arCustomer, Integer AD_CMPNY) {

       Debug.print("ArApprovalControllerBean post");

       LocalArCustomerBalanceHome arCustomerBalanceHome = null;

       // Initialize EJB Home

       try {

           arCustomerBalanceHome = (LocalArCustomerBalanceHome)EJBHomeFactory.
               lookUpLocalHome(LocalArCustomerBalanceHome.JNDI_NAME, LocalArCustomerBalanceHome.class);

       } catch (NamingException ex) {

           getSessionContext().setRollbackOnly();
           throw new EJBException(ex.getMessage());

       }

       try {

	       // find customer balance before or equal invoice date

	       Collection arCustomerBalances = arCustomerBalanceHome.findByBeforeOrEqualInvDateAndCstCustomerCode(INV_DT, arCustomer.getCstCustomerCode(), AD_CMPNY);

	       if (!arCustomerBalances.isEmpty()) {

	    	   // get last invoice

	    	   ArrayList arCustomerBalanceList = new ArrayList(arCustomerBalances);

	    	   LocalArCustomerBalance arCustomerBalance = (LocalArCustomerBalance)arCustomerBalanceList.get(arCustomerBalanceList.size() - 1);

	    	   if (arCustomerBalance.getCbDate().before(INV_DT)) {

	    	       // create new balance

	    	       LocalArCustomerBalance apNewCustomerBalance = arCustomerBalanceHome.create(
		    	       INV_DT, arCustomerBalance.getCbBalance() + INV_AMNT, AD_CMPNY);

		           arCustomer.addArCustomerBalance(apNewCustomerBalance);

	    	   } else { // equals to invoice date

	    	       arCustomerBalance.setCbBalance(arCustomerBalance.getCbBalance() + INV_AMNT);

	    	   }

	    	} else {

	    	    // create new balance

		    	LocalArCustomerBalance apNewCustomerBalance = arCustomerBalanceHome.create(
		    		INV_DT, INV_AMNT, AD_CMPNY);

		        arCustomer.addArCustomerBalance(apNewCustomerBalance);

	     	}

	     	// propagate to subsequent balances if necessary

	     	arCustomerBalances = arCustomerBalanceHome.findByAfterInvDateAndCstCustomerCode(INV_DT, arCustomer.getCstCustomerCode(), AD_CMPNY);

	     	Iterator i = arCustomerBalances.iterator();

	     	while (i.hasNext()) {

	     		LocalArCustomerBalance arCustomerBalance = (LocalArCustomerBalance)i.next();

	     		arCustomerBalance.setCbBalance(arCustomerBalance.getCbBalance() + INV_AMNT);

	     	}

        } catch (Exception ex) {

        	Debug.printStackTrace(ex);
        	getSessionContext().setRollbackOnly();
            throw new EJBException(ex.getMessage());

        }

	}

    private double convertForeignToFunctionalCurrency(Integer FC_CODE, String FC_NM,
	    Date CONVERSION_DATE, double CONVERSION_RATE, double AMOUNT, Integer AD_CMPNY) {

	    Debug.print("ArApprovalControllerBean convertForeignToFunctionalCurrency");


        LocalGlFunctionalCurrencyRateHome glFunctionalCurrencyRateHome = null;
        LocalAdCompanyHome adCompanyHome = null;

        LocalAdCompany adCompany = null;

        // Initialize EJB Homes

        try {

            glFunctionalCurrencyRateHome = (LocalGlFunctionalCurrencyRateHome)EJBHomeFactory.
               lookUpLocalHome(LocalGlFunctionalCurrencyRateHome.JNDI_NAME, LocalGlFunctionalCurrencyRateHome.class);
            adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
               lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);

         } catch (NamingException ex) {

            throw new EJBException(ex.getMessage());

         }

         // get company and extended precision

         try {

             adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);

         } catch (Exception ex) {

             throw new EJBException(ex.getMessage());

         }


         // Convert to functional currency if necessary

         if (CONVERSION_RATE != 1 && CONVERSION_RATE != 0 ) {

             AMOUNT = AMOUNT / CONVERSION_RATE;

         }
         return EJBCommon.roundIt(AMOUNT, adCompany.getGlFunctionalCurrency().getFcPrecision());

	}

   private void postToGl(LocalGlAccountingCalendarValue glAccountingCalendarValue,
      LocalGlChartOfAccount glChartOfAccount,
      boolean isCurrentAcv, byte isDebit, double JL_AMNT, Integer AD_CMPNY) {

      Debug.print("ArApprovalControllerBean postToGl");

      LocalGlChartOfAccountBalanceHome glChartOfAccountBalanceHome = null;
      LocalAdCompanyHome adCompanyHome = null;


       // Initialize EJB Home

       try {

           glChartOfAccountBalanceHome = (LocalGlChartOfAccountBalanceHome)EJBHomeFactory.
              lookUpLocalHome(LocalGlChartOfAccountBalanceHome.JNDI_NAME, LocalGlChartOfAccountBalanceHome.class);
           adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
              lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);


       } catch (NamingException ex) {

           throw new EJBException(ex.getMessage());

       }

       try {

               LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);

	       	   LocalGlChartOfAccountBalance glChartOfAccountBalance =
		            glChartOfAccountBalanceHome.findByAcvCodeAndCoaCode(
		               	  glAccountingCalendarValue.getAcvCode(),
		               	  glChartOfAccount.getCoaCode(), AD_CMPNY);

	           String ACCOUNT_TYPE = glChartOfAccount.getCoaAccountType();
	           short FC_EXTNDD_PRCSN = adCompany.getGlFunctionalCurrency().getFcPrecision();



	           if (((ACCOUNT_TYPE.equals("ASSET") || ACCOUNT_TYPE.equals("EXPENSE")) &&
				    isDebit == EJBCommon.TRUE) ||
				    (!ACCOUNT_TYPE.equals("ASSET") && !ACCOUNT_TYPE.equals("EXPENSE") &&
				    isDebit == EJBCommon.FALSE)) {

 			        glChartOfAccountBalance.setCoabEndingBalance(
				       EJBCommon.roundIt(glChartOfAccountBalance.getCoabEndingBalance() + JL_AMNT, FC_EXTNDD_PRCSN));

				    if (!isCurrentAcv) {

				    	glChartOfAccountBalance.setCoabBeginningBalance(
				       		EJBCommon.roundIt(glChartOfAccountBalance.getCoabBeginningBalance() + JL_AMNT, FC_EXTNDD_PRCSN));

					}


			  } else {

				    glChartOfAccountBalance.setCoabEndingBalance(
				       EJBCommon.roundIt(glChartOfAccountBalance.getCoabEndingBalance() - JL_AMNT, FC_EXTNDD_PRCSN));

				    if (!isCurrentAcv) {

				    	glChartOfAccountBalance.setCoabBeginningBalance(
				       		EJBCommon.roundIt(glChartOfAccountBalance.getCoabBeginningBalance() - JL_AMNT, FC_EXTNDD_PRCSN));

					}

		 	  }

		 	  if (isCurrentAcv) {

			 	 if (isDebit == EJBCommon.TRUE) {

		 			glChartOfAccountBalance.setCoabTotalDebit(
		 				EJBCommon.roundIt(glChartOfAccountBalance.getCoabTotalDebit() + JL_AMNT, FC_EXTNDD_PRCSN));

		 		 } else {

		 			glChartOfAccountBalance.setCoabTotalCredit(
		 				EJBCommon.roundIt(glChartOfAccountBalance.getCoabTotalCredit() + JL_AMNT, FC_EXTNDD_PRCSN));
		 		 }

		 	}

       } catch (Exception ex) {

       	   Debug.printStackTrace(ex);
       	   throw new EJBException(ex.getMessage());

       }


   }

   private void postToInv(LocalArInvoiceLineItem arInvoiceLineItem, Date CST_DT, double CST_QTY_SLD, double CST_CST_OF_SLS,
   		double CST_RMNNG_QTY, double CST_RMNNG_VL,  double CST_VRNC_VL, String USR_NM, Integer AD_BRNCH, Integer AD_CMPNY) throws
		AdPRFCoaGlVarianceAccountNotFoundException {

    	Debug.print("ArApprovalControllerBean postToInv");

    	LocalInvCostingHome invCostingHome = null;
    	LocalAdPreferenceHome adPreferenceHome = null;
    	LocalAdCompanyHome adCompanyHome = null;
    	LocalInvAdjustmentLineHome invAdjustmentLineHome = null;

        // Initialize EJB Home

        try {

          invCostingHome = (LocalInvCostingHome)EJBHomeFactory.
              lookUpLocalHome(LocalInvCostingHome.JNDI_NAME, LocalInvCostingHome.class);
          adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
              lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);
          adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
              lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
          invAdjustmentLineHome = (LocalInvAdjustmentLineHome)EJBHomeFactory.
	  	      lookUpLocalHome(LocalInvAdjustmentLineHome.JNDI_NAME, LocalInvAdjustmentLineHome.class);

        } catch (NamingException ex) {

          throw new EJBException(ex.getMessage());

        }

        try {

           LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);
           LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
           LocalInvItemLocation invItemLocation = arInvoiceLineItem.getInvItemLocation();
           int CST_LN_NMBR = 0;

           CST_QTY_SLD = EJBCommon.roundIt(CST_QTY_SLD, adPreference.getPrfInvQuantityPrecisionUnit());
           CST_CST_OF_SLS = EJBCommon.roundIt(CST_CST_OF_SLS, adCompany.getGlFunctionalCurrency().getFcPrecision());
           CST_RMNNG_QTY = EJBCommon.roundIt(CST_RMNNG_QTY, adPreference.getPrfInvQuantityPrecisionUnit());
           CST_RMNNG_VL = EJBCommon.roundIt(CST_RMNNG_VL, adCompany.getGlFunctionalCurrency().getFcPrecision());

           if (CST_QTY_SLD > 0 && arInvoiceLineItem.getIliEnableAutoBuild() == EJBCommon.FALSE) {

       	       	invItemLocation.setIlCommittedQuantity(invItemLocation.getIlCommittedQuantity() - CST_QTY_SLD);

           }

           // create costing

           try {

           	   // generate line number

               LocalInvCosting invCurrentCosting = invCostingHome.getByMaxCstLineNumberAndCstDateToLongAndIiNameAndLocName(CST_DT.getTime(), invItemLocation.getInvItem().getIiName(), invItemLocation.getInvLocation().getLocName(), AD_BRNCH, AD_CMPNY);
               CST_LN_NMBR = invCurrentCosting.getCstLineNumber() + 1;

           } catch (FinderException ex) {

           	   CST_LN_NMBR = 1;

           }

           if (adPreference.getPrfArAllowPriorDate() == EJBCommon.FALSE) {
	           //void subsequent cost variance adjustments
	           Collection invAdjustmentLines =invAdjustmentLineHome.findUnvoidAndIsCostVarianceGreaterThanAdjDateAndIlCodeAndBrCode(
	           		CST_DT, invItemLocation.getIlCode(), AD_BRNCH, AD_CMPNY);
	           Iterator i = invAdjustmentLines.iterator();

	           while (i.hasNext()){

	           	LocalInvAdjustmentLine invAdjustmentLine = (LocalInvAdjustmentLine) i.next();
	           	this.voidInvAdjustment(invAdjustmentLine.getInvAdjustment(), AD_BRNCH, AD_CMPNY);

	           }
           }

           String prevExpiryDates = "";
           String miscListPrpgt ="";
           double qtyPrpgt = 0;
           double qtyPrpgt2 = 0;
           try {
        	   LocalInvCosting prevCst = invCostingHome.getByMaxCstDateToLongAndMaxCstLineNumberAndLessThanEqualCstDateAndRemainingQuantityNotEqualToZeroAndIlCode(
        			   CST_DT, invItemLocation.getIlCode(), AD_BRNCH, AD_CMPNY);

        	   prevExpiryDates = prevCst.getCstExpiryDate();
        	   qtyPrpgt = prevCst.getCstRemainingQuantity();

        	   if (prevExpiryDates==null){
        		   prevExpiryDates="";
        	   }

           }catch (Exception ex){

           }

           // create costing
           LocalInvCosting invCosting = invCostingHome.create(CST_DT, CST_DT.getTime(), CST_LN_NMBR, 0d, 0d, 0d, 0d, 0d, 0d, CST_QTY_SLD, CST_CST_OF_SLS, CST_RMNNG_QTY, CST_RMNNG_VL, 0d, 0d, 0d, AD_BRNCH, AD_CMPNY);
           invItemLocation.addInvCosting(invCosting);
           invCosting.setArInvoiceLineItem(arInvoiceLineItem);

//         Get Latest Expiry Dates
           if(arInvoiceLineItem.getInvItemLocation().getInvItem().getIiTraceMisc()!=0){
        	   if(prevExpiryDates!=null && prevExpiryDates!="" && prevExpiryDates.length()!=0){
        		   if(arInvoiceLineItem.getArInvoice().getInvCreditMemo() == EJBCommon.FALSE || arInvoiceLineItem.getArReceipt().getRctType().equals("MISC")){

        			   if(prevExpiryDates!=null && prevExpiryDates!="" && prevExpiryDates.length()!=0){
        				   if(arInvoiceLineItem.getIliMisc()!=null && arInvoiceLineItem.getIliMisc()!="" && arInvoiceLineItem.getIliMisc().length()!=0){
        					   int qty2Prpgt = Integer.parseInt(this.getQuantityExpiryDates(arInvoiceLineItem.getIliMisc()));
        					   ArrayList miscList = this.expiryDates(arInvoiceLineItem.getIliMisc(), qty2Prpgt);
        					   String propagateMiscPrpgt = "";
        					   String ret = "";

        					   System.out.println("qty2Prpgt: " + qty2Prpgt);
        					   Iterator mi = miscList.iterator();
        					   propagateMiscPrpgt = prevExpiryDates;
        					   ret = propagateMiscPrpgt;
        					   System.out.println("ret: " + ret);
        					   String Checker = "";
        					   while(mi.hasNext()){
        						   String miscStr = (String)mi.next();

        						   ArrayList miscList2 = this.expiryDates("$"+ret, qtyPrpgt);
        						   Iterator m2 = miscList2.iterator();
        						   ret = "";
        						   String ret2 = "false";
        						   int a = 0;

        						   while(m2.hasNext()){
        							   String miscStr2 = (String)m2.next();

        							   if(ret2=="1st"){
        								   ret2 = "false";
        							   }

        							   if(miscStr2.equals(miscStr)){
        								   if(a==0){
        									   a = 1;
        									   ret2 = "1st";
        									   Checker = "true";
        								   }else{
        									   a = a+1;
        									   ret2 = "true";
        								   }
        							   }

        							   if(!miscStr2.equals(miscStr) || a>1){
        								   if((ret2!="1st")||(ret2=="false")||(ret2=="true")){
        									   if (miscStr2!=""){
        										   miscStr2 = "$" + miscStr2;
        										   ret = ret + miscStr2;
        										   ret2 = "false";
        									   }
        								   }
        							   }

        						   }
        						   ret = ret + "$";
        						   qtyPrpgt= qtyPrpgt -1;
        						   if(Checker!="true"){
        							   throw new GlobalExpiryDateNotFoundException();
        						   }
        					   }

        					   propagateMiscPrpgt = ret;
        					   System.out.println("propagateMiscPrpgt: " + propagateMiscPrpgt);
        					   //propagateMiscPrpgt = this.propagateExpiryDates(propagateMiscPrpgt, a, "True");

        					   invCosting.setCstExpiryDate(propagateMiscPrpgt);
        				   }else{
        					   invCosting.setCstExpiryDate(prevExpiryDates);
        				   }
        			   }
        		   }else{
        			   if(arInvoiceLineItem.getIliMisc()!=null && arInvoiceLineItem.getIliMisc()!="" && arInvoiceLineItem.getIliMisc().length()!=0){
        				   int qty2Prpgt = Integer.parseInt(this.getQuantityExpiryDates(arInvoiceLineItem.getIliMisc()));
        				   String miscList2Prpgt = this.propagateExpiryDates(arInvoiceLineItem.getIliMisc(), qty2Prpgt);
        				   prevExpiryDates = prevExpiryDates.substring(1);
        				   String propagateMiscPrpgt = miscList2Prpgt + prevExpiryDates;

        				   invCosting.setCstExpiryDate(propagateMiscPrpgt);
        			   }else{
        				   invCosting.setCstExpiryDate(prevExpiryDates);
        			   }

        		   }
        	   } else{
            	   if(arInvoiceLineItem.getIliMisc()!=null && arInvoiceLineItem.getIliMisc()!="" && arInvoiceLineItem.getIliMisc().length()!=0){
            		   int initialQty = Integer.parseInt(this.getQuantityExpiryDates(arInvoiceLineItem.getIliMisc()));
        			   String initialPrpgt = this.propagateExpiryDates(arInvoiceLineItem.getIliMisc(), initialQty);

                	   invCosting.setCstExpiryDate(initialPrpgt);
            	   }else{
            		   invCosting.setCstExpiryDate(prevExpiryDates);
            	   }

               }
           }



			// if cost variance is not 0, generate cost variance for the transaction
			if(CST_VRNC_VL != 0 && adPreference.getPrfArAllowPriorDate() == EJBCommon.FALSE) {

				if (arInvoiceLineItem.getArInvoice() != null)
					this.generateCostVariance(invCosting.getInvItemLocation(), CST_VRNC_VL,
							"ARCM" + arInvoiceLineItem.getArInvoice().getInvNumber(),
							arInvoiceLineItem.getArInvoice().getInvDescription(),
							arInvoiceLineItem.getArInvoice().getInvDate(), USR_NM, AD_BRNCH, AD_CMPNY);
				else if (arInvoiceLineItem.getArReceipt() != null)
					this.generateCostVariance(invCosting.getInvItemLocation(), CST_VRNC_VL,
							"ARMR" + arInvoiceLineItem.getArReceipt().getRctNumber(),
							arInvoiceLineItem.getArReceipt().getRctDescription(),
							arInvoiceLineItem.getArReceipt().getRctDate(), USR_NM, AD_BRNCH, AD_CMPNY);

			}

           // propagate balance if necessary

           Collection invCostings = invCostingHome.findByGreaterThanCstDateAndIiNameAndLocName(CST_DT, invItemLocation.getInvItem().getIiName(), invItemLocation.getInvLocation().getLocName(), AD_BRNCH, AD_CMPNY);

           Iterator i = invCostings.iterator();

           String miscList = "";
           ArrayList miscList2 = null;

           String propagateMisc ="";
           String ret = "";

           System.out.println("CST_ST_QTY: " + CST_QTY_SLD);

           while (i.hasNext()) {
        	   String Checker = "";
        	   String Checker2 = "";

               LocalInvCosting invPropagatedCosting = (LocalInvCosting)i.next();

               invPropagatedCosting.setCstRemainingQuantity(invPropagatedCosting.getCstRemainingQuantity() - CST_QTY_SLD);
               invPropagatedCosting.setCstRemainingValue(invPropagatedCosting.getCstRemainingValue() - CST_CST_OF_SLS);

               if(arInvoiceLineItem.getInvItemLocation().getInvItem().getIiTraceMisc()!=0){
            	   if(arInvoiceLineItem.getIliMisc()!=null && arInvoiceLineItem.getIliMisc()!="" && arInvoiceLineItem.getIliMisc().length()!=0){
                	   System.out.println("BAGO ANG MALI: " + arInvoiceLineItem.getIliMisc());

                	   double qty = Double.parseDouble(this.getQuantityExpiryDates(arInvoiceLineItem.getIliMisc()));
                	   //double qty2 = this.checkExpiryDates2(arInvoiceLineItem.getIliMisc());
                	   miscList = this.propagateExpiryDates(arInvoiceLineItem.getIliMisc(), qty, "False");
                	   miscList2 = this.expiryDates(arInvoiceLineItem.getIliMisc(), qty);

                	   System.out.println("invAdjustmentLine.getAlMisc(): "+arInvoiceLineItem.getIliMisc());
         	           System.out.println("getAlAdjustQuantity(): "+arInvoiceLineItem.getIliQuantity());

         	           if(arInvoiceLineItem.getIliQuantity()<0){
         	        	   Iterator mi = miscList2.iterator();

         	        	   propagateMisc = invPropagatedCosting.getCstExpiryDate();
         	        	   ret = invPropagatedCosting.getCstExpiryDate();
         	        	   while(mi.hasNext()){
         	        		   String miscStr = (String)mi.next();

         	        		   Integer qTest = this.checkExpiryDates(ret+"fin$");
         	        		   ArrayList miscList3 = this.expiryDates("$" + ret, Double.parseDouble(qTest.toString()));

         	        		   // ArrayList miscList3 = this.expiryDates("$" + ret, qtyPrpgt);
         	        		   System.out.println("ret: " + ret);
         	        		   Iterator m2 = miscList3.iterator();
         	        		   ret = "";
         	        		   String ret2 = "false";
         	        		   int a = 0;
         	        		   while(m2.hasNext()){
         	        			   String miscStr2 = (String)m2.next();

         	        			   if(ret2=="1st"){
         	        				   ret2 = "false";
         	        			   }
         	        			   System.out.println("2 miscStr: "+miscStr);
         	        			   System.out.println("2 miscStr2: "+miscStr2);
         	        			   if(miscStr2.equals(miscStr)){
         	        				   if(a==0){
         	        					   a = 1;
         	        					   ret2 = "1st";
         	        					   Checker2 = "true";
         	        				   }else{
         	        					   a = a+1;
         	        					   ret2 = "true";
         	        				   }
         	        			   }
         	        			   System.out.println("Checker: "+Checker2);
         	        			   if(!miscStr2.equals(miscStr) || a>1){
         	        				   if((ret2!="1st")||(ret2=="false")||(ret2=="true")){
         	        					   if (miscStr2!=""){
         	        						   miscStr2 = "$" + miscStr2;
         	        						   ret = ret + miscStr2;
         	        						   ret2 = "false";
         	        					   }
         	        				   }
         	        			   }

         	        		   }
         	        		   if(Checker2!="true"){
         	        			   throw new GlobalExpiryDateNotFoundException(arInvoiceLineItem.getInvItemLocation().getInvItem().getIiName());
         	        		   }else{
         	        			   System.out.println("TAE");
         	        		   }

         	        		   ret = ret + "$";
         	        		   qtyPrpgt= qtyPrpgt -1;
         	        	   }
         	           }
                   }

            	   if(prevExpiryDates!=null && prevExpiryDates!="" && prevExpiryDates.length()!=0){
            		   if(arInvoiceLineItem.getArInvoice().getInvCreditMemo() == EJBCommon.FALSE || arInvoiceLineItem.getArReceipt().getRctType().equals("MISC")){

                    	   if(prevExpiryDates!=null && prevExpiryDates!="" && prevExpiryDates.length()!=0){
                    		   if(arInvoiceLineItem.getIliMisc()!=null && arInvoiceLineItem.getIliMisc()!="" && arInvoiceLineItem.getIliMisc().length()!=0){
                           		if(CST_QTY_SLD<0){
                        			//miscList = miscList.substring(1);
                                	//propagateMisc = invPropagatedCosting.getCstExpiryDate() + miscList;
                                	propagateMisc = miscList+invPropagatedCosting.getCstExpiryDate().substring(1, invPropagatedCosting.getCstExpiryDate().length());
                            		System.out.println("propagateMiscPrpgt : "+propagateMisc);

                            	}else{
                            		Iterator mi = miscList2.iterator();

                            		propagateMisc = prevExpiryDates;
                            		ret = propagateMisc;
                            		while(mi.hasNext()){
                            			String miscStr = (String)mi.next();
                            			System.out.println("ret123: " + ret);
                            			System.out.println("qtyPrpgt123: " + qtyPrpgt);
                            			System.out.println("qtyPrpgt2: "+qtyPrpgt2);
                            			if(qtyPrpgt<=0){
                            				qtyPrpgt = qtyPrpgt2;
                            			}

                            			Integer qTest = this.checkExpiryDates(ret+"fin$");
                            			ArrayList miscList3 = this.expiryDates("$" + ret, Double.parseDouble(qTest.toString()));
                            			Iterator m2 = miscList3.iterator();
                            			ret = "";
                            			String ret2 = "false";
                            			int a = 0;
                            			while(m2.hasNext()){
                            				String miscStr2 = (String)m2.next();

                            				if(ret2=="1st"){
                            					ret2 = "false";
                            				}

                            				System.out.println("miscStr2: " + miscStr2);
               							   System.out.println("miscStr: " + miscStr);
                            				if(miscStr2.trim().equals(miscStr.trim())){
                            					if(a==0){
                            						a = 1;
                            						ret2 = "1st";
                            						Checker = "true";
                            					}else{
                            						a = a+1;
                            						ret2 = "true";
                            					}
                            				}

                            				if(!miscStr2.trim().equals(miscStr.trim()) || a>1){
                            					if((ret2!="1st")||(ret2=="false")||(ret2=="true")){
                            						if (miscStr2!=""){
                            							miscStr2 = "$" + miscStr2.trim();
                            							ret = ret + miscStr2;
                            							ret2 = "false";
                            						}
                            					}
                            				}

                            			}
                            			ret = ret + "$";
                            			qtyPrpgt= qtyPrpgt -1;
                            		}
                            		propagateMisc = ret;
                            		System.out.println("propagateMiscPrpgt: " + propagateMisc);

                            		if(Checker=="true"){
                            			//invPropagatedCosting.setCstExpiryDate(propagateMisc);
                            			System.out.println("Yes");
                            		}else{
                            			System.out.println("ex1");
                            			//throw new GlobalExpiryDateNotFoundException(invBranchStockTransferLine.getInvItemLocation().getInvItem().getIiName());

                            		}
                            	}

                            	invPropagatedCosting.setCstExpiryDate(propagateMisc);
                        	}else{
                        		   invPropagatedCosting.setCstExpiryDate(prevExpiryDates);
                        	   }
                    	   }
            		   }else{
            			   if(arInvoiceLineItem.getIliMisc()!=null && arInvoiceLineItem.getIliMisc()!="" && arInvoiceLineItem.getIliMisc().length()!=0){
            				   int qty2Prpgt = Integer.parseInt(this.getQuantityExpiryDates(arInvoiceLineItem.getIliMisc()));
            				   String miscList2Prpgt = this.propagateExpiryDates(arInvoiceLineItem.getIliMisc(), qty2Prpgt);
            				   prevExpiryDates = prevExpiryDates.substring(1);
            				   String propagateMiscPrpgt = miscList2Prpgt + prevExpiryDates;

            				   invPropagatedCosting.setCstExpiryDate(propagateMiscPrpgt);
            			   }else{
            				   invPropagatedCosting.setCstExpiryDate(prevExpiryDates);
            			   }

            		   }
            	   } else{
                	   if(arInvoiceLineItem.getIliMisc()!=null && arInvoiceLineItem.getIliMisc()!="" && arInvoiceLineItem.getIliMisc().length()!=0){
                		   int initialQty = Integer.parseInt(this.getQuantityExpiryDates(arInvoiceLineItem.getIliMisc()));
            			   String initialPrpgt = this.propagateExpiryDates(arInvoiceLineItem.getIliMisc(), initialQty);

            			   invPropagatedCosting.setCstExpiryDate(initialPrpgt);
                	   }else{
                		   invPropagatedCosting.setCstExpiryDate(prevExpiryDates);
                	   }

                   }
               }




           }

           if (adPreference.getPrfArAllowPriorDate() == EJBCommon.FALSE) {
				// regenerate cost variance
	           this.regenerateCostVariance(invCostings, invCosting, AD_BRNCH, AD_CMPNY);
          }


        } catch (AdPRFCoaGlVarianceAccountNotFoundException ex) {

        	getSessionContext().setRollbackOnly();
        	throw ex;

        } catch (Exception ex) {

      	   Debug.printStackTrace(ex);
      	   getSessionContext().setRollbackOnly();
           throw new EJBException(ex.getMessage());

        }

    }
   public static int checkExpiryDates(String misc) throws Exception {

		String separator ="$";

		// Remove first $ character
		misc = misc.substring(1);
		//System.out.println("misc: " + misc);
		// Counter
		int start = 0;
		int nextIndex = misc.indexOf(separator, start);
		int length = nextIndex - start;
		int numberExpry=0;
		String miscList = new String();
		String miscList2 = "";
		String g= "";
		try{
			while(g!="fin"){
				// Date
				start = nextIndex + 1;
				nextIndex = misc.indexOf(separator, start);
				length = nextIndex - start;
				g= misc.substring(start, start + length);
				if(g.length()!=0){
					if(g!=null || g!="" || g!="null"){
						if(g.contains("null")){
							miscList2 = "Error";
						}else{
							miscList = miscList + "$" + g;
							numberExpry++;
						}
					}else{
						miscList2 = "Error";
					}

				}else{
					miscList2 = "Error";
				}
			}
		}catch(Exception e){

		}

		if(miscList2==""){
			miscList = miscList+"$";
		}else{
			miscList = miscList2;
		}

		return (numberExpry);
	}

   public String propagateExpiryDates(String misc, double qty, String reverse) throws Exception {
	   //ActionErrors errors = new ActionErrors();
	   Debug.print("ApReceivingItemControllerBean getExpiryDates");
	   System.out.println("misc: " + misc);
	   String miscList = new String();
	   try{
		   String separator = "";
		   if(reverse=="False"){
			   separator ="$";
		   }else{
			   separator =" ";
		   }

		   // Remove first $ character
		   misc = misc.substring(1);
		   System.out.println("misc: " + misc);
		   // Counter
		   int start = 0;
		   int nextIndex = misc.indexOf(separator, start);
		   int length = nextIndex - start;



		   for(int x=0; x<qty; x++) {

			   // Date
			   start = nextIndex + 1;
			   nextIndex = misc.indexOf(separator, start);
			   length = nextIndex - start;
			   String g= misc.substring(start, start + length);
			   System.out.println("g: " + g);
			   System.out.println("g length: " + g.length());
			   if(g.length()!=0){
				   miscList = miscList + "$" + g;
				   System.out.println("miscList G: " + miscList);
			   }
		   }

		   miscList = miscList+"$";
	   }catch(Exception e){
		   miscList="";
	   }

	   System.out.println("miscList :" + miscList);
	   return (miscList);
   }

   private ArrayList expiryDates(String misc, double qty) throws Exception{
	   	Debug.print("ApReceivingItemControllerBean getExpiryDates");
	   	System.out.println("misc: " + misc);
	   	String separator ="$";


	   	// Remove first $ character
	   	misc = misc.substring(1);

	   	// Counter
	   	int start = 0;
	   	int nextIndex = misc.indexOf(separator, start);
	   	int length = nextIndex - start;

	   	System.out.println("qty" + qty);
	   	ArrayList miscList = new ArrayList();

	   	for(int x=0; x<qty; x++) {

	   		// Date
	   		start = nextIndex + 1;
	   		nextIndex = misc.indexOf(separator, start);
	   		length = nextIndex - start;
	   		System.out.println("x"+x);
	   		String checker = misc.substring(start, start + length);
	   		System.out.println("checker"+checker);
	   		if(checker.length()!=0 || checker!="null"){
	   			miscList.add(checker);
	   		}else{
	   			miscList.add("null");
	   		}
	   	}

			System.out.println("miscList :" + miscList);
			return miscList;
	   }

   public String getQuantityExpiryDates(String qntty){
 	   String separator = "$";

 	   // Remove first $ character
 	   qntty = qntty.substring(1);

 	   // Counter
 	   int start = 0;
 	   int nextIndex = qntty.indexOf(separator, start);
 	   int length = nextIndex - start;
 	   String y;
 	   y = (qntty.substring(start, start + length));
 	   System.out.println("Y " + y);

 	   return y;
    }

    public String propagateExpiryDates(String misc, double qty) throws Exception {
 	   //ActionErrors errors = new ActionErrors();

 	   Debug.print("ApReceivingItemControllerBean getExpiryDates");

 	   String separator = "$";

 	   // Remove first $ character
 	   misc = misc.substring(1);

 	   // Counter
 	   int start = 0;
 	   int nextIndex = misc.indexOf(separator, start);
 	   int length = nextIndex - start;

 	   System.out.println("qty" + qty);
 	   String miscList = new String();

 	   for(int x=0; x<qty; x++) {

 		   // Date
 		   start = nextIndex + 1;
 		   nextIndex = misc.indexOf(separator, start);
 		   length = nextIndex - start;
 		   /*SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
 	        sdf.setLenient(false);*/
 		   try {

 			   miscList = miscList + "$" +(misc.substring(start, start + length));
 		   } catch (Exception ex) {

 			   throw ex;
 		   }


 	   }

 	   miscList = miscList+"$";
 	   System.out.println("miscList :" + miscList);
 	   return (miscList);
    }


   private void postToInv(LocalArSalesOrderInvoiceLine arSalesOrderInvoiceLine, Date CST_DT, double CST_QTY_SLD,
   		double CST_CST_OF_SLS, double CST_RMNNG_QTY, double CST_RMNNG_VL, double CST_VRNC_VL, String USR_NM, Integer AD_BRNCH,
		Integer AD_CMPNY) throws
		AdPRFCoaGlVarianceAccountNotFoundException {

   	Debug.print("ArApprovalControllerBean postToInv");

   	LocalInvCostingHome invCostingHome = null;
   	LocalAdPreferenceHome adPreferenceHome = null;
   	LocalAdCompanyHome adCompanyHome = null;
	LocalInvAdjustmentLineHome invAdjustmentLineHome = null;

   	// Initialize EJB Home

   	try {

   		invCostingHome = (LocalInvCostingHome)EJBHomeFactory.
		lookUpLocalHome(LocalInvCostingHome.JNDI_NAME, LocalInvCostingHome.class);
   		adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
		lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);
   		adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
		lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
   		invAdjustmentLineHome = (LocalInvAdjustmentLineHome)EJBHomeFactory.
  	    lookUpLocalHome(LocalInvAdjustmentLineHome.JNDI_NAME, LocalInvAdjustmentLineHome.class);

   	} catch (NamingException ex) {

   		throw new EJBException(ex.getMessage());

   	}

   	try {

   		LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);
   		LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
   		LocalArSalesOrderLine arSalesOrderLine = arSalesOrderInvoiceLine.getArSalesOrderLine();
   		LocalInvItemLocation invItemLocation = arSalesOrderLine.getInvItemLocation();
   		int CST_LN_NMBR = 0;

   		CST_QTY_SLD = EJBCommon.roundIt(CST_QTY_SLD, adPreference.getPrfInvQuantityPrecisionUnit());
   		CST_CST_OF_SLS = EJBCommon.roundIt(CST_CST_OF_SLS, adCompany.getGlFunctionalCurrency().getFcPrecision());
   		CST_RMNNG_QTY = EJBCommon.roundIt(CST_RMNNG_QTY, adPreference.getPrfInvQuantityPrecisionUnit());
   		CST_RMNNG_VL = EJBCommon.roundIt(CST_RMNNG_VL, adCompany.getGlFunctionalCurrency().getFcPrecision());

   		// create costing

   		try {

   			// generate line number

   			LocalInvCosting invCurrentCosting = invCostingHome.getByMaxCstLineNumberAndCstDateToLongAndIiNameAndLocName(CST_DT.getTime(), invItemLocation.getInvItem().getIiName(), invItemLocation.getInvLocation().getLocName(), AD_BRNCH, AD_CMPNY);
   			CST_LN_NMBR = invCurrentCosting.getCstLineNumber() + 1;

   		} catch (FinderException ex) {

   			CST_LN_NMBR = 1;

   		}

   		if (adPreference.getPrfArAllowPriorDate() == EJBCommon.FALSE) {
	        //void subsequent cost variance adjustments
	        Collection invAdjustmentLines = invAdjustmentLineHome.findUnvoidAndIsCostVarianceGreaterThanAdjDateAndIlCodeAndBrCode(
	        		CST_DT, invItemLocation.getIlCode(), AD_BRNCH, AD_CMPNY);
	        Iterator i = invAdjustmentLines.iterator();

	        while (i.hasNext()){

	        	LocalInvAdjustmentLine invAdjustmentLine = (LocalInvAdjustmentLine) i.next();
	        	this.voidInvAdjustment(invAdjustmentLine.getInvAdjustment(), AD_BRNCH, AD_CMPNY);

	        }
   		}


        // create costing
   		LocalInvCosting invCosting = invCostingHome.create(CST_DT, CST_DT.getTime(), CST_LN_NMBR, 0d, 0d, 0d, 0d, 0d, 0d, CST_QTY_SLD, CST_CST_OF_SLS, CST_RMNNG_QTY, CST_RMNNG_VL, 0d, 0d, 0d, AD_BRNCH, AD_CMPNY);
   		invItemLocation.addInvCosting(invCosting);
   		invCosting.setArSalesOrderInvoiceLine(arSalesOrderInvoiceLine);

		// if cost variance is not 0, generate cost variance for the transaction
		if(CST_VRNC_VL != 0 && adPreference.getPrfArAllowPriorDate() == EJBCommon.FALSE) {

			this.generateCostVariance(invCosting.getInvItemLocation(), CST_VRNC_VL,
					"ARCM" + arSalesOrderInvoiceLine.getArInvoice().getInvNumber(),
					arSalesOrderInvoiceLine.getArInvoice().getInvDescription(),
					arSalesOrderInvoiceLine.getArInvoice().getInvDate(), USR_NM, AD_BRNCH, AD_CMPNY);

		}

   		// propagate balance if necessary

   		Collection invCostings = invCostingHome.findByGreaterThanCstDateAndIiNameAndLocName(CST_DT, invItemLocation.getInvItem().getIiName(), invItemLocation.getInvLocation().getLocName(), AD_BRNCH, AD_CMPNY);

   		Iterator i = invCostings.iterator();

   		while (i.hasNext()) {

   			LocalInvCosting invPropagatedCosting = (LocalInvCosting)i.next();

   			invPropagatedCosting.setCstRemainingQuantity(invPropagatedCosting.getCstRemainingQuantity() + CST_QTY_SLD);
   			invPropagatedCosting.setCstRemainingValue(invPropagatedCosting.getCstRemainingValue() + CST_CST_OF_SLS);

   		}

        // regenerate cost varaince
   		if (adPreference.getPrfArAllowPriorDate() == EJBCommon.FALSE) {
   			this.regenerateCostVariance(invCostings, invCosting, AD_BRNCH, AD_CMPNY);
   		}

     } catch (AdPRFCoaGlVarianceAccountNotFoundException ex) {

     	getSessionContext().setRollbackOnly();
     	throw ex;

   	} catch (Exception ex) {

   		Debug.printStackTrace(ex);
   		getSessionContext().setRollbackOnly();
   		throw new EJBException(ex.getMessage());

   	}

   }

   private void postToBua(LocalArInvoiceLineItem arInvoiceLineItem, Date CST_DT, double CST_ASSMBLY_QTY, double CST_ASSMBLY_CST,
   		double CST_RMNNG_QTY, double CST_RMNNG_VL, String II_NM, String LOC_NM, double CST_VRNC_VL, String USR_NM, Integer AD_BRNCH,
		Integer AD_CMPNY) throws
		AdPRFCoaGlVarianceAccountNotFoundException {

   	Debug.print("ArApprovalControllerBean postToBua");

   	LocalInvCostingHome invCostingHome = null;
   	LocalAdPreferenceHome adPreferenceHome = null;
   	LocalAdCompanyHome adCompanyHome = null;
   	LocalInvItemLocationHome invItemLocationHome = null;
	LocalInvAdjustmentLineHome invAdjustmentLineHome = null;

   	// Initialize EJB Home

   	try {

   		invCostingHome = (LocalInvCostingHome)EJBHomeFactory.
		lookUpLocalHome(LocalInvCostingHome.JNDI_NAME, LocalInvCostingHome.class);
   		adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
		lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);
   		adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
		lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
   		invItemLocationHome = (LocalInvItemLocationHome)EJBHomeFactory.
		lookUpLocalHome(LocalInvItemLocationHome.JNDI_NAME, LocalInvItemLocationHome.class);
   		invAdjustmentLineHome = (LocalInvAdjustmentLineHome)EJBHomeFactory.
  	    lookUpLocalHome(LocalInvAdjustmentLineHome.JNDI_NAME, LocalInvAdjustmentLineHome.class);

   	} catch (NamingException ex) {

   		throw new EJBException(ex.getMessage());

   	}

   	try {

   		LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);
   		LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
   		LocalInvItemLocation invItemLocation = invItemLocationHome.findByLocNameAndIiName(LOC_NM, II_NM, AD_CMPNY);
   		int CST_LN_NMBR = 0;

   		CST_ASSMBLY_QTY = EJBCommon.roundIt(CST_ASSMBLY_QTY, adPreference.getPrfInvQuantityPrecisionUnit());
   		CST_ASSMBLY_CST = EJBCommon.roundIt(CST_ASSMBLY_CST, adCompany.getGlFunctionalCurrency().getFcPrecision());
   		CST_RMNNG_QTY = EJBCommon.roundIt(CST_RMNNG_QTY, adPreference.getPrfInvQuantityPrecisionUnit());
   		CST_RMNNG_VL = EJBCommon.roundIt(CST_RMNNG_VL, adCompany.getGlFunctionalCurrency().getFcPrecision());

   		if ((CST_ASSMBLY_QTY < 0 && arInvoiceLineItem.getIliEnableAutoBuild() == 0) ||
           		(CST_ASSMBLY_QTY < 0 && arInvoiceLineItem.getIliEnableAutoBuild() == 1 && !arInvoiceLineItem.getInvItemLocation().getInvItem().equals(invItemLocation.getInvItem()))) {

   			invItemLocation.setIlCommittedQuantity(invItemLocation.getIlCommittedQuantity() - Math.abs(CST_ASSMBLY_QTY));

   		}

   		// create costing

   		try {

   			// generate line number

   			LocalInvCosting invCurrentCosting = invCostingHome.getByMaxCstLineNumberAndCstDateToLongAndIiNameAndLocName(CST_DT.getTime(), invItemLocation.getInvItem().getIiName(), invItemLocation.getInvLocation().getLocName(), AD_BRNCH, AD_CMPNY);
   			CST_LN_NMBR = invCurrentCosting.getCstLineNumber() + 1;

   		} catch (FinderException ex) {

   			CST_LN_NMBR = 1;

   		}

   		if (adPreference.getPrfArAllowPriorDate() == EJBCommon.FALSE) {
	        //void subsequent cost variance adjustments
	        Collection invAdjustmentLines = invAdjustmentLineHome.findUnvoidAndIsCostVarianceGreaterThanAdjDateAndIlCodeAndBrCode(
	        		CST_DT, invItemLocation.getIlCode(), AD_BRNCH, AD_CMPNY);
	        Iterator i = invAdjustmentLines.iterator();

	        while (i.hasNext()){

	        	LocalInvAdjustmentLine invAdjustmentLine = (LocalInvAdjustmentLine) i.next();
	        	this.voidInvAdjustment(invAdjustmentLine.getInvAdjustment(), AD_BRNCH, AD_CMPNY);

	        }
   		}

        // create costing
   		LocalInvCosting invCosting = invCostingHome.create(CST_DT, CST_DT.getTime(), CST_LN_NMBR, 0d, 0d, CST_ASSMBLY_QTY, CST_ASSMBLY_CST, 0d, 0d, 0d, 0d, CST_RMNNG_QTY, CST_RMNNG_VL, 0d, 0d, CST_ASSMBLY_QTY > 0 ? CST_ASSMBLY_QTY : 0, AD_BRNCH, AD_CMPNY);
   		invItemLocation.addInvCosting(invCosting);
   		invCosting.setArInvoiceLineItem(arInvoiceLineItem);

		// if cost variance is not 0, generate cost variance for the transaction
		if(CST_VRNC_VL != 0 && adPreference.getPrfArAllowPriorDate() == EJBCommon.FALSE) {

			if (arInvoiceLineItem.getArInvoice() != null)
				this.generateCostVariance(invCosting.getInvItemLocation(), CST_VRNC_VL,
						"ARCM" + arInvoiceLineItem.getArInvoice().getInvNumber(),
						arInvoiceLineItem.getArInvoice().getInvDescription(),
						arInvoiceLineItem.getArInvoice().getInvDate(), USR_NM, AD_BRNCH, AD_CMPNY);
			else if (arInvoiceLineItem.getArReceipt() != null)
				this.generateCostVariance(invCosting.getInvItemLocation(), CST_VRNC_VL,
						"ARMR" + arInvoiceLineItem.getArReceipt().getRctNumber(),
						arInvoiceLineItem.getArReceipt().getRctDescription(),
						arInvoiceLineItem.getArReceipt().getRctDate(), USR_NM, AD_BRNCH, AD_CMPNY);

		}

   		// propagate balance if necessary

   		Collection invCostings = invCostingHome.findByGreaterThanCstDateAndIiNameAndLocName(CST_DT, invItemLocation.getInvItem().getIiName(), invItemLocation.getInvLocation().getLocName(), AD_BRNCH, AD_CMPNY);

   		Iterator i = invCostings.iterator();

   		while (i.hasNext()) {

   			LocalInvCosting invPropagatedCosting = (LocalInvCosting)i.next();

   			invPropagatedCosting.setCstRemainingQuantity(invPropagatedCosting.getCstRemainingQuantity() + CST_ASSMBLY_QTY);
   			invPropagatedCosting.setCstRemainingValue(invPropagatedCosting.getCstRemainingValue() + CST_ASSMBLY_CST);

   		}

   		if (adPreference.getPrfArAllowPriorDate() == EJBCommon.FALSE) {
			// regenerate cost variance
           this.regenerateCostVariance(invCostings, invCosting, AD_BRNCH, AD_CMPNY);
       }

     } catch (AdPRFCoaGlVarianceAccountNotFoundException ex) {

     	getSessionContext().setRollbackOnly();
     	throw ex;

   	} catch (Exception ex) {

   		Debug.printStackTrace(ex);
   		getSessionContext().setRollbackOnly();
   		throw new EJBException(ex.getMessage());

   	}

   }

    private double convertByUomFromAndItemAndQuantity(LocalInvUnitOfMeasure invFromUnitOfMeasure, LocalInvItem invItem, double QTY_SLD, Integer AD_CMPNY) {

		Debug.print("ArApprovalControllerBean convertByUomFromAndItemAndQuantity");

		LocalAdPreferenceHome adPreferenceHome = null;
		LocalInvUnitOfMeasureConversionHome invUnitOfMeasureConversionHome = null;

        // Initialize EJB Home

        try {

            adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
                 lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);
            invUnitOfMeasureConversionHome = (LocalInvUnitOfMeasureConversionHome)EJBHomeFactory.
        		lookUpLocalHome(LocalInvUnitOfMeasureConversionHome.JNDI_NAME, LocalInvUnitOfMeasureConversionHome.class);

        } catch (NamingException ex) {

            throw new EJBException(ex.getMessage());

        }

        try {

            LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);

            LocalInvUnitOfMeasureConversion invUnitOfMeasureConversion = invUnitOfMeasureConversionHome.findUmcByIiNameAndUomName(invItem.getIiName(), invFromUnitOfMeasure.getUomName(), AD_CMPNY);
            LocalInvUnitOfMeasureConversion invDefaultUomConversion = invUnitOfMeasureConversionHome.findUmcByIiNameAndUomName(invItem.getIiName(), invItem.getInvUnitOfMeasure().getUomName(), AD_CMPNY);

            return EJBCommon.roundIt(QTY_SLD * invDefaultUomConversion.getUmcConversionFactor() / invUnitOfMeasureConversion.getUmcConversionFactor(), adPreference.getPrfInvQuantityPrecisionUnit());

        } catch (Exception ex) {

        	Debug.printStackTrace(ex);
        	getSessionContext().setRollbackOnly();
        	throw new EJBException(ex.getMessage());

        }

	}

    private void regenerateInventoryDr(LocalArInvoice arInvoice, Integer AD_BRNCH, Integer AD_CMPNY)
    throws GlobalInventoryDateException, GlobalBranchAccountNumberInvalidException {

    	Debug.print("ArApprovalControllerBean regenerateInventoryDr");

    	LocalArDistributionRecordHome arDistributionRecordHome = null;
    	LocalGlChartOfAccountHome glChartOfAccountHome = null;
    	LocalInvCostingHome invCostingHome = null;
    	LocalInvItemHome invItemHome = null;
    	LocalInvItemLocationHome invItemLocationHome = null;
    	LocalAdBranchItemLocationHome adBranchItemLocationHome = null;
        LocalAdPreferenceHome adPreferenceHome = null;

    	// Initialize EJB Home

    	try {

    		arDistributionRecordHome = (LocalArDistributionRecordHome)EJBHomeFactory.
				lookUpLocalHome(LocalArDistributionRecordHome.JNDI_NAME, LocalArDistributionRecordHome.class);
    		glChartOfAccountHome = (LocalGlChartOfAccountHome)EJBHomeFactory.
				lookUpLocalHome(LocalGlChartOfAccountHome.JNDI_NAME, LocalGlChartOfAccountHome.class);
    		invCostingHome = (LocalInvCostingHome)EJBHomeFactory.
				lookUpLocalHome(LocalInvCostingHome.JNDI_NAME, LocalInvCostingHome.class);
    		invItemHome = (LocalInvItemHome)EJBHomeFactory.
				lookUpLocalHome(LocalInvItemHome.JNDI_NAME, LocalInvItemHome.class);
    		invItemLocationHome = (LocalInvItemLocationHome)EJBHomeFactory.
				lookUpLocalHome(LocalInvItemLocationHome.JNDI_NAME, LocalInvItemLocationHome.class);
    		adBranchItemLocationHome = (LocalAdBranchItemLocationHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdBranchItemLocationHome.JNDI_NAME, LocalAdBranchItemLocationHome.class);
    		adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
    		lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);

    	} catch (NamingException ex) {

    		throw new EJBException(ex.getMessage());

    	}

    	try {

    		LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);

    		// regenerate inventory distribution records

    		Collection arDistributionRecords = arDistributionRecordHome.findImportableDrByInvCode(arInvoice.getInvCode(), AD_CMPNY);

    		Iterator i = arDistributionRecords.iterator();

    		while (i.hasNext()) {

    			LocalArDistributionRecord arDistributionRecord = (LocalArDistributionRecord)i.next();

    			if(arDistributionRecord.getDrClass().equals("COGS") || arDistributionRecord.getDrClass().equals("INVENTORY")){

    				i.remove();
    				arDistributionRecord.remove();

    			}

    		}

    		Collection arInvoiceLineItems = arInvoice.getArInvoiceLineItems();

    		if(arInvoiceLineItems != null && !arInvoiceLineItems.isEmpty()) {

    			i = arInvoiceLineItems.iterator();

    			while(i.hasNext()) {

    				LocalArInvoiceLineItem arInvoiceLineItem = (LocalArInvoiceLineItem)i.next();
    				LocalInvItemLocation invItemLocation = arInvoiceLineItem.getInvItemLocation();

    				// start date validation
    				if (adPreference.getPrfArAllowPriorDate() == EJBCommon.FALSE) {
    					Collection invNegTxnCosting = invCostingHome.findNegTxnByGreaterThanCstDateAndIiNameAndLocName(
    							arInvoice.getInvDate(), invItemLocation.getInvItem().getIiName(),
    							invItemLocation.getInvLocation().getLocName(), AD_BRNCH, AD_CMPNY);
    					if(!invNegTxnCosting.isEmpty()) throw new GlobalInventoryDateException(invItemLocation.getInvItem().getIiName());

    				}

    				// 	add cost of sales distribution and inventory
    				double COST = 0d;

    				try {

    					LocalInvCosting invCosting  = invCostingHome.getByMaxCstDateToLongAndMaxCstLineNumberAndLessThanEqualCstDateAndIiNameAndLocName(
    							arInvoice.getInvDate(), invItemLocation.getInvItem().getIiName(), invItemLocation.getInvLocation().getLocName(), AD_BRNCH, AD_CMPNY);

    					if(invCosting.getInvItemLocation().getInvItem().getIiCostMethod().equals("Average"))

 	  	    				COST = Math.abs(invCosting.getCstRemainingValue() / invCosting.getCstRemainingQuantity());

 	  	    			else if(invCosting.getInvItemLocation().getInvItem().getIiCostMethod().equals("FIFO"))

 	  	    				COST = Math.abs(this.getInvFifoCost(invCosting.getCstDate(), invCosting.getInvItemLocation().getIlCode(),
 	  	    						arInvoiceLineItem.getIliQuantity(), arInvoiceLineItem.getIliUnitPrice(), false, AD_BRNCH, AD_CMPNY));

 	  	    			else if(invCosting.getInvItemLocation().getInvItem().getIiCostMethod().equals("Standard"))

 	  	    				COST = invCosting.getInvItemLocation().getInvItem().getIiUnitCost();

    				} catch (FinderException ex) {

    					COST = arInvoiceLineItem.getInvItemLocation().getInvItem().getIiUnitCost();

    				}

    				double QTY_SLD = this.convertByUomFromAndItemAndQuantity(arInvoiceLineItem.getInvUnitOfMeasure(),
    						arInvoiceLineItem.getInvItemLocation().getInvItem(), arInvoiceLineItem.getIliQuantity(), AD_CMPNY);

    				LocalAdBranchItemLocation adBranchItemLocation = null;

    				try {

    					adBranchItemLocation = adBranchItemLocationHome.findBilByIlCodeAndBrCode(arInvoiceLineItem.getInvItemLocation().getIlCode(), AD_BRNCH, AD_CMPNY);

    				} catch(FinderException ex) {

    				}

    				if (arInvoiceLineItem.getIliEnableAutoBuild() == EJBCommon.FALSE) {

    					if(arInvoice.getInvCreditMemo() == EJBCommon.FALSE) {

    						if (adBranchItemLocation != null) {

    							// Use AdBranchItemLocation Cost of Sales & Inventory Account
    							this.addArDrIliEntry(arInvoice.getArDrNextLine(),
    									"COGS", EJBCommon.TRUE, COST * QTY_SLD,
										adBranchItemLocation.getBilCoaGlCostOfSalesAccount(), arInvoice, AD_BRNCH, AD_CMPNY);

    							this.addArDrIliEntry(arInvoice.getArDrNextLine(),
    									"INVENTORY", EJBCommon.FALSE, COST * QTY_SLD,
    									adBranchItemLocation.getBilCoaGlInventoryAccount(), arInvoice, AD_BRNCH, AD_CMPNY);

    						} else {

    							// Use default accounts
    							this.addArDrIliEntry(arInvoice.getArDrNextLine(),
    									"COGS", EJBCommon.TRUE, COST * QTY_SLD,
										arInvoiceLineItem.getInvItemLocation().getIlGlCoaCostOfSalesAccount(), arInvoice, AD_BRNCH, AD_CMPNY);

    							this.addArDrIliEntry(arInvoice.getArDrNextLine(),
    									"INVENTORY", EJBCommon.FALSE, COST * QTY_SLD,
										arInvoiceLineItem.getInvItemLocation().getIlGlCoaInventoryAccount(), arInvoice, AD_BRNCH, AD_CMPNY);
    						}

    					} else {

    						if (adBranchItemLocation != null) {

    							// Use AdBranchItemLocation Accounts
    							this.addArDrIliEntry(arInvoice.getArDrNextLine(),
    									"COGS", EJBCommon.FALSE, COST * QTY_SLD,
    									adBranchItemLocation.getBilCoaGlCostOfSalesAccount(), arInvoice, AD_BRNCH, AD_CMPNY);

    							this.addArDrIliEntry(arInvoice.getArDrNextLine(),
    									"INVENTORY", EJBCommon.TRUE, COST * QTY_SLD,
    									adBranchItemLocation.getBilCoaGlInventoryAccount(), arInvoice, AD_BRNCH, AD_CMPNY);

    						} else {

    							// Use default Accounts
    							this.addArDrIliEntry(arInvoice.getArDrNextLine(),
    									"COGS", EJBCommon.FALSE, COST * QTY_SLD,
										arInvoiceLineItem.getInvItemLocation().getIlGlCoaCostOfSalesAccount(), arInvoice, AD_BRNCH, AD_CMPNY);

    							this.addArDrIliEntry(arInvoice.getArDrNextLine(),
    									"INVENTORY", EJBCommon.TRUE, COST * QTY_SLD,
										arInvoiceLineItem.getInvItemLocation().getIlGlCoaInventoryAccount(), arInvoice, AD_BRNCH, AD_CMPNY);

    						}
    					}

    				}

    				if (arInvoiceLineItem.getIliEnableAutoBuild() == EJBCommon.TRUE && arInvoiceLineItem.getInvItemLocation().getInvItem().getIiClass().equals("Assembly")) {

    					byte DEBIT = EJBCommon.TRUE;

    					double TOTAL_AMOUNT = 0;
    					double ABS_TOTAL_AMOUNT = 0;

    					if (adBranchItemLocation != null) {

    						LocalGlChartOfAccount glInventoryChartOfAccount = glChartOfAccountHome.findByPrimaryKey(
        							adBranchItemLocation.getBilCoaGlInventoryAccount());

    					} else {

    						LocalGlChartOfAccount glInventoryChartOfAccount = glChartOfAccountHome.findByPrimaryKey(
        							arInvoiceLineItem.getInvItemLocation().getIlGlCoaInventoryAccount());

    					}

    					Collection invBillOfMaterials = arInvoiceLineItem.getInvItemLocation().getInvItem().getInvBillOfMaterials();

    					Iterator j = invBillOfMaterials.iterator();

    					while (j.hasNext()) {

    						LocalInvBillOfMaterial invBillOfMaterial = (LocalInvBillOfMaterial) j.next();

    						// get raw material
    						LocalInvItemLocation invIlRawMaterial = null;

    						try {

    							invIlRawMaterial = invItemLocationHome.findByLocNameAndIiName(
    									invBillOfMaterial.getBomLocName(), invBillOfMaterial.getBomIiName(), AD_CMPNY);

    						} catch(FinderException ex) {

    							throw new GlobalInvItemLocationNotFoundException(String.valueOf(arInvoiceLineItem.getIliLine()) +
    									" - Raw Mat. (" +invBillOfMaterial.getBomIiName() + ")");

    						}

    						// add bill of material quantity needed to item location

    						double convertedQuantity = this.convertByUomFromAndItemAndQuantity(
                                    invBillOfMaterial.getInvUnitOfMeasure(), invIlRawMaterial.getInvItem(),
                                    EJBCommon.roundIt(invBillOfMaterial.getBomQuantityNeeded() * QTY_SLD,
                                    this.getInvGpQuantityPrecisionUnit(AD_CMPNY)), AD_CMPNY);

    						// start date validation
    						if (adPreference.getPrfArAllowPriorDate() == EJBCommon.FALSE) {
    							Collection invIlRawMatNegTxnCosting = invCostingHome.findNegTxnByGreaterThanCstDateAndIiNameAndLocName(
    									arInvoice.getInvDate(), invIlRawMaterial.getInvItem().getIiName(),
    									invIlRawMaterial.getInvLocation().getLocName(),	AD_BRNCH, AD_CMPNY);
    							if(!invIlRawMatNegTxnCosting.isEmpty()) throw new GlobalInventoryDateException(
    									invItemLocation.getInvItem().getIiName() + " - Raw Mat. (" + invIlRawMaterial.getInvItem().getIiName() + ")");
    						}
    						LocalInvItem invItem = invItemHome.findByIiName(invBillOfMaterial.getBomIiName(), AD_CMPNY);

    						double COSTING = 0d;

    						try {

    							LocalInvCosting invCosting  = invCostingHome.getByMaxCstDateToLongAndMaxCstLineNumberAndLessThanEqualCstDateAndIiNameAndLocName(
    									arInvoice.getInvDate(), invItem.getIiName(), invBillOfMaterial.getBomLocName(), AD_BRNCH, AD_CMPNY);

    							COSTING = Math.abs(invCosting.getCstRemainingValue() / invCosting.getCstRemainingQuantity());

    						} catch (FinderException ex) {

    							COSTING = invItem.getIiUnitCost();

    						}

    						// bom conversion
                            COSTING = this.convertCostByUom(invIlRawMaterial.getInvItem().getIiName(),
                                    invBillOfMaterial.getInvUnitOfMeasure().getUomName(), COSTING, true, AD_CMPNY);

    						double BOM_AMOUNT = EJBCommon.roundIt(
    								(QTY_SLD * (invBillOfMaterial.getBomQuantityNeeded() * COSTING)),
									this.getGlFcPrecisionUnit(AD_CMPNY));

    						TOTAL_AMOUNT += BOM_AMOUNT;
    						ABS_TOTAL_AMOUNT += Math.abs(BOM_AMOUNT);

    						// add inventory account

    						LocalAdBranchItemLocation adBranchItemLocationRM = null;

    						try {

    							adBranchItemLocationRM = adBranchItemLocationHome.findBilByIlCodeAndBrCode(invIlRawMaterial.getIlCode(), AD_BRNCH, AD_CMPNY);

    						} catch(FinderException ex) {

    						}

    						if (adBranchItemLocationRM != null) {

    							if(arInvoice.getInvCreditMemo() == EJBCommon.FALSE) {

    								this.addArDrIliEntry(arInvoice.getArDrNextLine(), "INVENTORY", EJBCommon.FALSE, Math.abs(BOM_AMOUNT),
    										adBranchItemLocationRM.getBilCoaGlInventoryAccount(), arInvoice, AD_BRNCH, AD_CMPNY);

    							} else {

    								this.addArDrIliEntry(arInvoice.getArDrNextLine(), "INVENTORY", EJBCommon.TRUE, Math.abs(BOM_AMOUNT),
    										adBranchItemLocationRM.getBilCoaGlInventoryAccount(), arInvoice, AD_BRNCH, AD_CMPNY);

    							}

    						} else {

    							if(arInvoice.getInvCreditMemo() == EJBCommon.FALSE) {

    								this.addArDrIliEntry(arInvoice.getArDrNextLine(), "INVENTORY", EJBCommon.FALSE, Math.abs(BOM_AMOUNT),
    										invIlRawMaterial.getIlGlCoaInventoryAccount(), arInvoice, AD_BRNCH, AD_CMPNY);

    							} else {

    								this.addArDrIliEntry(arInvoice.getArDrNextLine(), "INVENTORY", EJBCommon.TRUE, Math.abs(BOM_AMOUNT),
    										invIlRawMaterial.getIlGlCoaInventoryAccount(), arInvoice, AD_BRNCH, AD_CMPNY);

    							}

    						}

    					}

    					// add cost of sales account

    					if (adBranchItemLocation != null) {

    						// Use AdBranchItemLocation Accounts
    						if(arInvoice.getInvCreditMemo() == EJBCommon.FALSE) {

    							this.addArDrIliEntry(arInvoice.getArDrNextLine(),
    									"COGS", EJBCommon.TRUE, Math.abs(TOTAL_AMOUNT),
										adBranchItemLocation.getBilCoaGlCostOfSalesAccount(), arInvoice, AD_BRNCH, AD_CMPNY);

    						} else {

    							this.addArDrIliEntry(arInvoice.getArDrNextLine(),
    									"COGS", EJBCommon.FALSE, Math.abs(TOTAL_AMOUNT),
    									adBranchItemLocation.getBilCoaGlCostOfSalesAccount(), arInvoice, AD_BRNCH, AD_CMPNY);

    						}

    					} else {

    						// Use default Accounts
    						if(arInvoice.getInvCreditMemo() == EJBCommon.FALSE) {

    							this.addArDrIliEntry(arInvoice.getArDrNextLine(),
    									"COGS", EJBCommon.TRUE, Math.abs(TOTAL_AMOUNT),
										arInvoiceLineItem.getInvItemLocation().getIlGlCoaCostOfSalesAccount(), arInvoice, AD_BRNCH, AD_CMPNY);

    						} else {

    							this.addArDrIliEntry(arInvoice.getArDrNextLine(),
    									"COGS", EJBCommon.FALSE, Math.abs(TOTAL_AMOUNT),
										arInvoiceLineItem.getInvItemLocation().getIlGlCoaCostOfSalesAccount(), arInvoice, AD_BRNCH, AD_CMPNY);

    						}
    					}

    				}

    			}
    		}

    	} catch (GlobalInventoryDateException ex) {

    		getSessionContext().setRollbackOnly();
    		throw ex;

    	} catch (GlobalBranchAccountNumberInvalidException ex) {

    		getSessionContext().setRollbackOnly();
    		throw ex;

    	} catch (Exception ex) {

    		Debug.printStackTrace(ex);
    		getSessionContext().setRollbackOnly();
    		throw new EJBException(ex.getMessage());

    	}

    }

    private void addArDrIliEntry(short DR_LN, String DR_CLSS, byte DR_DBT, double DR_AMNT, Integer COA_CODE, LocalArInvoice arInvoice, Integer AD_BRNCH, Integer AD_CMPNY)
    	throws GlobalBranchAccountNumberInvalidException {

    	Debug.print("ArApprovalControllerBean addArDrIliEntry");

    	LocalArDistributionRecordHome arDistributionRecordHome = null;
    	LocalGlChartOfAccountHome glChartOfAccountHome = null;
    	LocalAdCompanyHome adCompanyHome = null;


    	// Initialize EJB Home

    	try {

    		arDistributionRecordHome = (LocalArDistributionRecordHome)EJBHomeFactory.
				lookUpLocalHome(LocalArDistributionRecordHome.JNDI_NAME, LocalArDistributionRecordHome.class);
    		glChartOfAccountHome = (LocalGlChartOfAccountHome)EJBHomeFactory.
				lookUpLocalHome(LocalGlChartOfAccountHome.JNDI_NAME, LocalGlChartOfAccountHome.class);
    		adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);

    	} catch (NamingException ex) {

    		throw new EJBException(ex.getMessage());

    	}

    	try {

    		// get company

    		LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);

    		LocalGlChartOfAccount glChartOfAccount = glChartOfAccountHome.findByCoaCodeAndBranchCode(COA_CODE, AD_BRNCH, AD_CMPNY);

    		// create distribution record

    		LocalArDistributionRecord arDistributionRecord = arDistributionRecordHome.create(
    				DR_LN, DR_CLSS, DR_DBT, EJBCommon.roundIt(DR_AMNT, adCompany.getGlFunctionalCurrency().getFcPrecision()),
					EJBCommon.FALSE, EJBCommon.FALSE, AD_CMPNY);

    		arInvoice.addArDistributionRecord(arDistributionRecord);
    		glChartOfAccount.addArDistributionRecord(arDistributionRecord);

    	} catch (FinderException ex) {

			throw new GlobalBranchAccountNumberInvalidException(ex.getMessage());

		} catch (Exception ex) {

    		Debug.printStackTrace(ex);
    		getSessionContext().setRollbackOnly();
    		throw new EJBException(ex.getMessage());

    	}

    }

    private void regenerateInventoryDr(LocalArReceipt arReceipt, Integer AD_BRNCH, Integer AD_CMPNY)
    throws GlobalInventoryDateException, GlobalBranchAccountNumberInvalidException {

    	Debug.print("ArApprovalControllerBean regenerateInventoryDr");

    	LocalArDistributionRecordHome arDistributionRecordHome = null;
    	LocalGlChartOfAccountHome glChartOfAccountHome = null;
    	LocalInvCostingHome invCostingHome = null;
    	LocalInvItemHome invItemHome = null;
    	LocalInvItemLocationHome invItemLocationHome = null;
    	LocalAdBranchItemLocationHome adBranchItemLocationHome = null;
    	LocalAdPreferenceHome adPreferenceHome = null;

    	// Initialize EJB Home

    	try {

    		arDistributionRecordHome = (LocalArDistributionRecordHome)EJBHomeFactory.
				lookUpLocalHome(LocalArDistributionRecordHome.JNDI_NAME, LocalArDistributionRecordHome.class);
    		glChartOfAccountHome = (LocalGlChartOfAccountHome)EJBHomeFactory.
				lookUpLocalHome(LocalGlChartOfAccountHome.JNDI_NAME, LocalGlChartOfAccountHome.class);
    		invCostingHome = (LocalInvCostingHome)EJBHomeFactory.
				lookUpLocalHome(LocalInvCostingHome.JNDI_NAME, LocalInvCostingHome.class);
    		invItemHome = (LocalInvItemHome)EJBHomeFactory.
				lookUpLocalHome(LocalInvItemHome.JNDI_NAME, LocalInvItemHome.class);
    		invItemLocationHome = (LocalInvItemLocationHome)EJBHomeFactory.
				lookUpLocalHome(LocalInvItemLocationHome.JNDI_NAME, LocalInvItemLocationHome.class);
    		adBranchItemLocationHome = (LocalAdBranchItemLocationHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdBranchItemLocationHome.JNDI_NAME, LocalAdBranchItemLocationHome.class);
    		adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
 				lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);

    	} catch (NamingException ex) {

    		throw new EJBException(ex.getMessage());

    	}

    	try {

    		LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);

    		// regenerate inventory distribution records

    		Collection arDistributionRecords = null;

    		if (arReceipt.getRctVoid() == EJBCommon.FALSE) {

    			arDistributionRecords = arDistributionRecordHome.findImportableDrByDrReversedAndRctCode(EJBCommon.FALSE, arReceipt.getRctCode(), AD_CMPNY);

    		} else {

    			arDistributionRecords = arDistributionRecordHome.findImportableDrByDrReversedAndRctCode(EJBCommon.TRUE, arReceipt.getRctCode(), AD_CMPNY);

    		}

    		Iterator i = arDistributionRecords.iterator();

    		while (i.hasNext()) {

    			LocalArDistributionRecord arDistributionRecord = (LocalArDistributionRecord)i.next();

    			if(arDistributionRecord.getDrClass().equals("COGS") || arDistributionRecord.getDrClass().equals("INVENTORY")){

    				i.remove();
    				arDistributionRecord.remove();

    			}

    		}

    		Collection arInvoiceLineItems = arReceipt.getArInvoiceLineItems();

    		if(arInvoiceLineItems != null && !arInvoiceLineItems.isEmpty()) {

    			i = arInvoiceLineItems.iterator();

    			while(i.hasNext()) {

    				LocalArInvoiceLineItem arInvoiceLineItem = (LocalArInvoiceLineItem)i.next();
    				LocalInvItemLocation invItemLocation = arInvoiceLineItem.getInvItemLocation();
    				if (adPreference.getPrfArAllowPriorDate() == EJBCommon.FALSE) {
	    				Collection invNegTxnCosting = invCostingHome.findNegTxnByGreaterThanCstDateAndIiNameAndLocName(
	    						arReceipt.getRctDate(), invItemLocation.getInvItem().getIiName(),
								invItemLocation.getInvLocation().getLocName(), AD_BRNCH, AD_CMPNY);
	    				if(!invNegTxnCosting.isEmpty()) throw new GlobalInventoryDateException(invItemLocation.getInvItem().getIiName());
    				}

    				// add cost of sales distribution and inventory

    				double COST = 0d;

    				try {

    					LocalInvCosting invCosting  = invCostingHome.getByMaxCstDateToLongAndMaxCstLineNumberAndLessThanEqualCstDateAndIiNameAndLocName(
    							arReceipt.getRctDate(), invItemLocation.getInvItem().getIiName(), invItemLocation.getInvLocation().getLocName(), AD_BRNCH, AD_CMPNY);

    					if(invCosting.getInvItemLocation().getInvItem().getIiCostMethod().equals("Average"))

 	  	    				COST = Math.abs(invCosting.getCstRemainingValue() / invCosting.getCstRemainingQuantity());

 	  	    			else if(invCosting.getInvItemLocation().getInvItem().getIiCostMethod().equals("FIFO"))

 	  	    				COST = Math.abs(this.getInvFifoCost(invCosting.getCstDate(), invCosting.getInvItemLocation().getIlCode(),
 	  	    						arInvoiceLineItem.getIliQuantity(), arInvoiceLineItem.getIliUnitPrice(), false, AD_BRNCH, AD_CMPNY));

 	  	    			else if(invCosting.getInvItemLocation().getInvItem().getIiCostMethod().equals("Standard"))

 	  	    				COST = invCosting.getInvItemLocation().getInvItem().getIiUnitCost();

    				} catch (FinderException ex) {

    					COST = arInvoiceLineItem.getInvItemLocation().getInvItem().getIiUnitCost();

    				}

    				double QTY_SLD = this.convertByUomFromAndItemAndQuantity(arInvoiceLineItem.getInvUnitOfMeasure(),
    						arInvoiceLineItem.getInvItemLocation().getInvItem(), arInvoiceLineItem.getIliQuantity(), AD_CMPNY);

    				LocalAdBranchItemLocation adBranchItemLocation = null;

    				try {

    					adBranchItemLocation = adBranchItemLocationHome.findBilByIlCodeAndBrCode(arInvoiceLineItem.getInvItemLocation().getIlCode(), AD_BRNCH, AD_CMPNY);

    				} catch(FinderException ex) {

    				}

    				if (arInvoiceLineItem.getIliEnableAutoBuild() == EJBCommon.FALSE &&
                    		arInvoiceLineItem.getInvItemLocation().getInvItem().getIiNonInventoriable() == EJBCommon.FALSE) {

    					if (adBranchItemLocation != null) {

    						// Use AdBranchItemLocation Accounts
    						this.addArDrIliEntry(arReceipt.getArDrNextLine(),
    								"COGS", EJBCommon.TRUE, COST * QTY_SLD,
									adBranchItemLocation.getBilCoaGlCostOfSalesAccount(), arReceipt, AD_BRNCH, AD_CMPNY);

    						this.addArDrIliEntry(arReceipt.getArDrNextLine(),
    								"INVENTORY", EJBCommon.FALSE, COST * QTY_SLD,
    								adBranchItemLocation.getBilCoaGlInventoryAccount(), arReceipt, AD_BRNCH, AD_CMPNY);

    					} else {

    						// Use default Accounts
    						this.addArDrIliEntry(arReceipt.getArDrNextLine(),
    								"COGS", EJBCommon.TRUE, COST * QTY_SLD,
									arInvoiceLineItem.getInvItemLocation().getIlGlCoaCostOfSalesAccount(), arReceipt, AD_BRNCH, AD_CMPNY);

    						this.addArDrIliEntry(arReceipt.getArDrNextLine(),
    								"INVENTORY", EJBCommon.FALSE, COST * QTY_SLD,
									arInvoiceLineItem.getInvItemLocation().getIlGlCoaInventoryAccount(), arReceipt, AD_BRNCH, AD_CMPNY);

    					}
    				}

    				if (arInvoiceLineItem.getIliEnableAutoBuild() == EJBCommon.TRUE && arInvoiceLineItem.getInvItemLocation().getInvItem().getIiClass().equals("Assembly")) {

    					byte DEBIT = EJBCommon.TRUE;

    					double TOTAL_AMOUNT = 0;
    					double ABS_TOTAL_AMOUNT = 0;

    					Collection invBillOfMaterials = arInvoiceLineItem.getInvItemLocation().getInvItem().getInvBillOfMaterials();

    					Iterator j = invBillOfMaterials.iterator();

    					while (j.hasNext()) {

    						LocalInvBillOfMaterial invBillOfMaterial = (LocalInvBillOfMaterial) j.next();

    						// get raw material
    						LocalInvItemLocation invIlRawMaterial = null;

    						try {

    							invIlRawMaterial = invItemLocationHome.findByLocNameAndIiName(
    									invBillOfMaterial.getBomLocName(), invBillOfMaterial.getBomIiName(), AD_CMPNY);

    						} catch(FinderException ex) {

    							throw new GlobalInvItemLocationNotFoundException(String.valueOf(arInvoiceLineItem.getIliLine()) +
    									" - Raw Mat. (" +invBillOfMaterial.getBomIiName() + ")");

    						}

    						// add bill of material quantity needed to item location

    						double quantityNeeded = this.convertByUomFromAndItemAndQuantity(
                                    invBillOfMaterial.getInvUnitOfMeasure(), invIlRawMaterial.getInvItem(),
                                    EJBCommon.roundIt(invBillOfMaterial.getBomQuantityNeeded() *  QTY_SLD,
                                    this.getInvGpQuantityPrecisionUnit(AD_CMPNY)), AD_CMPNY);

    						// start date validation
    						if (adPreference.getPrfArAllowPriorDate() == EJBCommon.FALSE) {
	    						Collection invIlRawMatNegTxnCosting = invCostingHome.findNegTxnByGreaterThanCstDateAndIiNameAndLocName(
	    								arReceipt.getRctDate(), invIlRawMaterial.getInvItem().getIiName(),
										invIlRawMaterial.getInvLocation().getLocName(),	AD_BRNCH, AD_CMPNY);
	    						if(!invIlRawMatNegTxnCosting.isEmpty()) throw new GlobalInventoryDateException(
	    								invItemLocation.getInvItem().getIiName() + " - Raw Mat. (" + invIlRawMaterial.getInvItem().getIiName() + ")");
    						}

    						LocalInvItem invItem = invItemHome.findByIiName(invBillOfMaterial.getBomIiName(), AD_CMPNY);

    						double COSTING = 0d;

    						try {

    							LocalInvCosting invCosting  = invCostingHome.getByMaxCstDateToLongAndMaxCstLineNumberAndLessThanEqualCstDateAndIiNameAndLocName(
    									arReceipt.getRctDate(), invItem.getIiName(), invBillOfMaterial.getBomLocName(), AD_BRNCH, AD_CMPNY);

    							COSTING = Math.abs(invCosting.getCstRemainingValue() / invCosting.getCstRemainingQuantity());

    						} catch (FinderException ex) {

    							COSTING = invItem.getIiUnitCost();

    						}

    						// bom conversion
                            COSTING = this.convertCostByUom(invIlRawMaterial.getInvItem().getIiName(),
                                    invBillOfMaterial.getInvUnitOfMeasure().getUomName(), COSTING, true, AD_CMPNY);

    						double BOM_AMOUNT = EJBCommon.roundIt(
    								(QTY_SLD * (invBillOfMaterial.getBomQuantityNeeded() * COSTING)),
									this.getGlFcPrecisionUnit(AD_CMPNY));

    						TOTAL_AMOUNT += BOM_AMOUNT;
    						ABS_TOTAL_AMOUNT += Math.abs(BOM_AMOUNT);

    						// add inventory account

    						LocalAdBranchItemLocation adBranchItemLocationRM = null;

    						try {

    							adBranchItemLocationRM = adBranchItemLocationHome.findBilByIlCodeAndBrCode(invIlRawMaterial.getIlCode(), AD_BRNCH, AD_CMPNY);

    						} catch(FinderException ex) {

    						}

    						if (adBranchItemLocationRM != null) {

    							this.addArDrIliEntry(arReceipt.getArDrNextLine(), "INVENTORY", EJBCommon.FALSE, Math.abs(BOM_AMOUNT),
        								adBranchItemLocationRM.getBilCoaGlInventoryAccount(), arReceipt, AD_BRNCH, AD_CMPNY);

    						} else {

    							this.addArDrIliEntry(arReceipt.getArDrNextLine(), "INVENTORY", EJBCommon.FALSE, Math.abs(BOM_AMOUNT),
        								invIlRawMaterial.getIlGlCoaInventoryAccount(), arReceipt, AD_BRNCH, AD_CMPNY);

    						}

    					}

    					// add cost of sales account

    					if (adBranchItemLocation != null) {

    						this.addArDrIliEntry(arReceipt.getArDrNextLine(),
    								"COGS", EJBCommon.TRUE, Math.abs(TOTAL_AMOUNT),
									adBranchItemLocation.getBilCoaGlCostOfSalesAccount(), arReceipt, AD_BRNCH, AD_CMPNY);

    					} else {

    						this.addArDrIliEntry(arReceipt.getArDrNextLine(),
    								"COGS", EJBCommon.TRUE, Math.abs(TOTAL_AMOUNT),
									arInvoiceLineItem.getInvItemLocation().getIlGlCoaCostOfSalesAccount(), arReceipt, AD_BRNCH, AD_CMPNY);

    					}

    				}

    			}
    		}

    	} catch (GlobalInventoryDateException ex) {

    		getSessionContext().setRollbackOnly();
    		throw ex;

    	} catch (GlobalBranchAccountNumberInvalidException ex) {

    		getSessionContext().setRollbackOnly();
    		throw ex;

    	} catch (Exception ex) {

    		Debug.printStackTrace(ex);
    		getSessionContext().setRollbackOnly();
    		throw new EJBException(ex.getMessage());

    	}

    }

    private void addArDrIliEntry(short DR_LN, String DR_CLSS, byte DR_DBT, double DR_AMNT, Integer COA_CODE, LocalArReceipt arReceipt, Integer AD_BRNCH, Integer AD_CMPNY)
    	throws GlobalBranchAccountNumberInvalidException {

    	Debug.print("ArApprovalControllerBean addArDrIliEntry");

    	LocalArDistributionRecordHome arDistributionRecordHome = null;
    	LocalGlChartOfAccountHome glChartOfAccountHome = null;
    	LocalAdCompanyHome adCompanyHome = null;


    	// Initialize EJB Home

    	try {

    		arDistributionRecordHome = (LocalArDistributionRecordHome)EJBHomeFactory.
			lookUpLocalHome(LocalArDistributionRecordHome.JNDI_NAME, LocalArDistributionRecordHome.class);
    		glChartOfAccountHome = (LocalGlChartOfAccountHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlChartOfAccountHome.JNDI_NAME, LocalGlChartOfAccountHome.class);
    		adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);

    	} catch (NamingException ex) {

    		throw new EJBException(ex.getMessage());

    	}

    	try {

    		// get company

    		LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);

   			LocalGlChartOfAccount glChartOfAccount = glChartOfAccountHome.findByCoaCodeAndBranchCode(COA_CODE, AD_BRNCH, AD_CMPNY);

    		// create distribution record

    		LocalArDistributionRecord arDistributionRecord = arDistributionRecordHome.create(
    				DR_LN, DR_CLSS, DR_DBT, EJBCommon.roundIt(DR_AMNT, adCompany.getGlFunctionalCurrency().getFcPrecision()),
					EJBCommon.FALSE, EJBCommon.FALSE, AD_CMPNY);

    		arReceipt.addArDistributionRecord(arDistributionRecord);
    		glChartOfAccount.addArDistributionRecord(arDistributionRecord);

    	} catch (FinderException ex) {

			throw new GlobalBranchAccountNumberInvalidException(ex.getMessage());

		} catch (Exception ex) {

    		Debug.printStackTrace(ex);
    		getSessionContext().setRollbackOnly();
    		throw new EJBException(ex.getMessage());

    	}

    }

    private short getInvGpQuantityPrecisionUnit(Integer AD_CMPNY) {

    	Debug.print("ArApprovalControllerBean getInvGpQuantityPrecisionUnit");

    	LocalAdPreferenceHome adPreferenceHome = null;

    	// Initialize EJB Home

    	try {

    		adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);

    	} catch (NamingException ex) {

    		throw new EJBException(ex.getMessage());

    	}


    	try {

    		LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);

    		return adPreference.getPrfInvQuantityPrecisionUnit();

    	} catch (Exception ex) {

    		Debug.printStackTrace(ex);
    		throw new EJBException(ex.getMessage());

    	}

    }

    private double convertCostByUom(String II_NM, String UOM_NM, double unitCost, boolean isFromDefault, Integer AD_CMPNY) {

        Debug.print("ArInvoiceEntryControllerBean convertCostByUom");

        LocalInvItemHome invItemHome = null;
        LocalInvUnitOfMeasureConversionHome invUnitOfMeasureConversionHome = null;

        // Initialize EJB Home

        try {

            invUnitOfMeasureConversionHome = (LocalInvUnitOfMeasureConversionHome)EJBHomeFactory.
            lookUpLocalHome(LocalInvUnitOfMeasureConversionHome.JNDI_NAME, LocalInvUnitOfMeasureConversionHome.class);
            invItemHome = (LocalInvItemHome)EJBHomeFactory.
            lookUpLocalHome(LocalInvItemHome.JNDI_NAME, LocalInvItemHome.class);

        } catch (NamingException ex) {

            throw new EJBException(ex.getMessage());

        }

        try {

            LocalInvItem invItem = invItemHome.findByIiName(II_NM, AD_CMPNY);
            LocalInvUnitOfMeasureConversion invUnitOfMeasureConversion = invUnitOfMeasureConversionHome.findUmcByIiNameAndUomName(II_NM, UOM_NM, AD_CMPNY);
            LocalInvUnitOfMeasureConversion invDefaultUomConversion = invUnitOfMeasureConversionHome.findUmcByIiNameAndUomName(II_NM, invItem.getInvUnitOfMeasure().getUomName(), AD_CMPNY);

            if (isFromDefault) {

                return unitCost * invDefaultUomConversion.getUmcConversionFactor() / invUnitOfMeasureConversion.getUmcConversionFactor();

            } else {

                return unitCost * invUnitOfMeasureConversion.getUmcConversionFactor() / invDefaultUomConversion.getUmcConversionFactor();

            }

        } catch (Exception ex) {

            Debug.printStackTrace(ex);
            getSessionContext().setRollbackOnly();
            throw new EJBException(ex.getMessage());

        }

    }

    private double getFrRateByFrNameAndFrDate(String FC_NM, Date CONVERSION_DATE, Integer AD_CMPNY)
    throws GlobalConversionDateNotExistException {

    	Debug.print("ArApprovalControllerBean getFrRateByFrNameAndFrDate");

    	LocalGlFunctionalCurrencyRateHome glFunctionalCurrencyRateHome = null;
    	LocalGlFunctionalCurrencyHome glFunctionalCurrencyHome = null;
    	LocalAdCompanyHome adCompanyHome = null;

    	// Initialize EJB Home

    	try {

    		glFunctionalCurrencyRateHome = (LocalGlFunctionalCurrencyRateHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlFunctionalCurrencyRateHome.JNDI_NAME, LocalGlFunctionalCurrencyRateHome.class);
    		glFunctionalCurrencyHome = (LocalGlFunctionalCurrencyHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlFunctionalCurrencyHome.JNDI_NAME, LocalGlFunctionalCurrencyHome.class);
    		adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);

    	} catch (NamingException ex) {

    		throw new EJBException(ex.getMessage());

    	}

    	try {

    		LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
    		LocalGlFunctionalCurrency glFunctionalCurrency = glFunctionalCurrencyHome.findByFcName(FC_NM, AD_CMPNY);

    		double CONVERSION_RATE = 1;

    		// Get functional currency rate

    		if (!FC_NM.equals("USD")) {

    			LocalGlFunctionalCurrencyRate glFunctionalCurrencyRate =
    				glFunctionalCurrencyRateHome.findByFcCodeAndDate(glFunctionalCurrency.getFcCode(),
    						CONVERSION_DATE, AD_CMPNY);

    			CONVERSION_RATE = glFunctionalCurrencyRate.getFrXToUsd();

    		}

    		// Get set of book functional currency rate if necessary

    		if (!adCompany.getGlFunctionalCurrency().getFcName().equals("USD")) {

    			LocalGlFunctionalCurrencyRate glCompanyFunctionalCurrencyRate =
    				glFunctionalCurrencyRateHome.findByFcCodeAndDate(adCompany.getGlFunctionalCurrency(). getFcCode(),
    						CONVERSION_DATE, AD_CMPNY);

    			CONVERSION_RATE = CONVERSION_RATE / glCompanyFunctionalCurrencyRate.getFrXToUsd();

    		}

    		return CONVERSION_RATE;

    	} catch (FinderException ex) {

    		getSessionContext().setRollbackOnly();
    		throw new GlobalConversionDateNotExistException();

    	} catch (Exception ex) {

    		Debug.printStackTrace(ex);
    		throw new EJBException(ex.getMessage());

    	}

    }

    private void voidInvAdjustment(LocalInvAdjustment invAdjustment, Integer AD_BRNCH, Integer AD_CMPNY){

    	Debug.print("ArApprovalControllerBean voidInvAdjustment");

    	try{

    			Collection invDistributionRecords = invAdjustment.getInvDistributionRecords();
    			ArrayList list = new ArrayList();

    			Iterator i = invDistributionRecords.iterator();

    			while (i.hasNext()) {

    				LocalInvDistributionRecord invDistributionRecord = (LocalInvDistributionRecord)i.next();

    				list.add(invDistributionRecord);

    			}

    			i = list.iterator();

    			while (i.hasNext()) {

    				LocalInvDistributionRecord invDistributionRecord = (LocalInvDistributionRecord)i.next();

    				this.addInvDrEntry(invAdjustment.getInvDrNextLine(), invDistributionRecord.getDrClass(),
    						invDistributionRecord.getDrDebit() == EJBCommon.TRUE ? EJBCommon.FALSE : EJBCommon.TRUE,
    								invDistributionRecord.getDrAmount(), EJBCommon.TRUE,
									invDistributionRecord.getInvChartOfAccount().getCoaCode(), invAdjustment, AD_BRNCH, AD_CMPNY);

    			}

    			Collection invAdjustmentLines = invAdjustment.getInvAdjustmentLines();
    			i = invAdjustmentLines.iterator();
    			list.clear();

    			while (i.hasNext()) {

    				LocalInvAdjustmentLine invAdjustmentLine = (LocalInvAdjustmentLine)i.next();

    				list.add(invAdjustmentLine);

    			}

    			i = list.iterator();

    			while (i.hasNext()) {

    				LocalInvAdjustmentLine invAdjustmentLine = (LocalInvAdjustmentLine)i.next();

    				this.addInvAlEntry(invAdjustmentLine.getInvItemLocation(),
    						invAdjustment, (invAdjustmentLine.getAlUnitCost()) * - 1, EJBCommon.TRUE, AD_CMPNY);

    			}


    			invAdjustment.setAdjVoid(EJBCommon.TRUE);

    			this.executeInvAdjPost(invAdjustment.getAdjCode(), invAdjustment.getAdjLastModifiedBy(), AD_BRNCH, AD_CMPNY);

    	} catch (Exception ex) {

    		Debug.printStackTrace(ex);
    		getSessionContext().setRollbackOnly();
    		throw new EJBException(ex.getMessage());

    	}

    }

    private void generateCostVariance(LocalInvItemLocation invItemLocation, double CST_VRNC_VL, String ADJ_RFRNC_NMBR,
    		String ADJ_DSCRPTN, Date ADJ_DT, String USR_NM, Integer AD_BRNCH, Integer AD_CMPNY) throws
			AdPRFCoaGlVarianceAccountNotFoundException {
    	/*
    	Debug.print("ArApprovalControllerBean generateCostVariance");

    	LocalAdPreferenceHome adPreferenceHome = null;
    	LocalGlChartOfAccountHome glChartOfAccountHome = null;
    	LocalAdBranchItemLocationHome adBranchItemLocationHome = null;

    	// Initialize EJB Home

    	try {

    		adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);
    		glChartOfAccountHome = (LocalGlChartOfAccountHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlChartOfAccountHome.JNDI_NAME, LocalGlChartOfAccountHome.class);
    		adBranchItemLocationHome = (LocalAdBranchItemLocationHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdBranchItemLocationHome.JNDI_NAME, LocalAdBranchItemLocationHome.class);


    	} catch (NamingException ex) {

    		throw new EJBException(ex.getMessage());

    	}

    	try{



    		LocalInvAdjustment newInvAdjustment = this.saveInvAdjustment(ADJ_RFRNC_NMBR, ADJ_DSCRPTN, ADJ_DT, USR_NM, AD_BRNCH,
    				AD_CMPNY);
    		LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);
    		LocalGlChartOfAccount glCoaVarianceAccount = null;


    		if(adPreference.getPrfInvGlCoaVarianceAccount() == null)
    			throw new AdPRFCoaGlVarianceAccountNotFoundException();

    		try{

    			glCoaVarianceAccount = glChartOfAccountHome.findByPrimaryKey(adPreference.getPrfInvGlCoaVarianceAccount());
    			glCoaVarianceAccount.addInvAdjustment(newInvAdjustment);

    		} catch (FinderException ex) {

    			throw new AdPRFCoaGlVarianceAccountNotFoundException();

    		}

    		LocalInvAdjustmentLine invAdjustmentLine = this.addInvAlEntry(invItemLocation, newInvAdjustment,
    				CST_VRNC_VL, EJBCommon.FALSE, AD_CMPNY);

    		// check for branch mapping

    		LocalAdBranchItemLocation adBranchItemLocation = null;

    		try{

    			adBranchItemLocation = adBranchItemLocationHome.findBilByIlCodeAndBrCode(
    					invAdjustmentLine.getInvItemLocation().getIlCode(), AD_BRNCH, AD_CMPNY);

    		} catch (FinderException ex) {

    		}

    		LocalGlChartOfAccount glInventoryChartOfAccount = null;

    		if (adBranchItemLocation == null) {

    			glInventoryChartOfAccount = glChartOfAccountHome.findByPrimaryKey(
    					invAdjustmentLine.getInvItemLocation().getIlGlCoaInventoryAccount());
    		} else {

    			glInventoryChartOfAccount = glChartOfAccountHome.findByPrimaryKey(
    					adBranchItemLocation.getBilCoaGlInventoryAccount());

    		}


    		boolean isDebit = CST_VRNC_VL < 0 ? false : true;

    		//inventory dr
    		this.addInvDrEntry(newInvAdjustment.getInvDrNextLine(), "INVENTORY",
    				isDebit == true ? EJBCommon.TRUE : EJBCommon.FALSE, Math.abs(CST_VRNC_VL), EJBCommon.FALSE,
    						glInventoryChartOfAccount.getCoaCode(), newInvAdjustment, AD_BRNCH, AD_CMPNY);

    		//variance dr
    		this.addInvDrEntry(newInvAdjustment.getInvDrNextLine(),"VARIANCE",
    				!isDebit == true ? EJBCommon.TRUE : EJBCommon.FALSE, Math.abs(CST_VRNC_VL), EJBCommon.FALSE,
    						glCoaVarianceAccount.getCoaCode(), newInvAdjustment, AD_BRNCH, AD_CMPNY);

    		this.executeInvAdjPost(newInvAdjustment.getAdjCode(), newInvAdjustment.getAdjLastModifiedBy(), AD_BRNCH,
    				AD_CMPNY);

    	} catch (AdPRFCoaGlVarianceAccountNotFoundException ex) {

    		getSessionContext().setRollbackOnly();
    		throw ex;

    	} catch (Exception ex) {

    		Debug.printStackTrace(ex);
    		getSessionContext().setRollbackOnly();
    		throw new EJBException(ex.getMessage());

    	}
    	*/
    }

    private void regenerateCostVariance(Collection invCostings, LocalInvCosting invCosting, Integer AD_BRNCH, Integer AD_CMPNY)
    throws AdPRFCoaGlVarianceAccountNotFoundException {
    	/*
    	Debug.print("ArApprovalControllerBean regenerateCostVariance");

    	try {

    		Iterator i = invCostings.iterator();
    		LocalInvCosting prevInvCosting = invCosting;

    		while (i.hasNext()) {

    			LocalInvCosting invPropagatedCosting = (LocalInvCosting)i.next();

    			if(prevInvCosting.getCstRemainingQuantity() < 0) {

    				double TTL_CST = 0;
    				double QNTY = 0;
    				String ADJ_RFRNC_NMBR = "";
    				String ADJ_DSCRPTN = "";
    				String ADJ_CRTD_BY = "";

    				// get unit cost adjusment, document number and unit of measure
    				if (invPropagatedCosting.getApPurchaseOrderLine() != null) {

    					TTL_CST = invPropagatedCosting.getApPurchaseOrderLine().getPlAmount();
						QNTY =  this.convertByUomFromAndItemAndQuantity(
								invPropagatedCosting.getApPurchaseOrderLine().getInvUnitOfMeasure(),
								invPropagatedCosting.getApPurchaseOrderLine().getInvItemLocation().getInvItem(),
								invPropagatedCosting.getApPurchaseOrderLine().getPlQuantity(), AD_CMPNY);

    					ADJ_DSCRPTN = invPropagatedCosting.getApPurchaseOrderLine().getApPurchaseOrder().getPoDescription();
    					ADJ_CRTD_BY = invPropagatedCosting.getApPurchaseOrderLine().getApPurchaseOrder().getPoPostedBy();
    					ADJ_RFRNC_NMBR = "APRI" +
						invPropagatedCosting.getApPurchaseOrderLine().getApPurchaseOrder().getPoDocumentNumber();

    				} else if (invPropagatedCosting.getApVoucherLineItem() != null){

    					TTL_CST = invPropagatedCosting.getApVoucherLineItem().getVliAmount();
						QNTY = this.convertByUomFromAndItemAndQuantity(
								invPropagatedCosting.getApVoucherLineItem().getInvUnitOfMeasure(),
								invPropagatedCosting.getApVoucherLineItem().getInvItemLocation().getInvItem(),
								invPropagatedCosting.getApVoucherLineItem().getVliQuantity(), AD_CMPNY);

    					if (invPropagatedCosting.getApVoucherLineItem().getApVoucher() != null) {

    						ADJ_DSCRPTN = invPropagatedCosting.getApVoucherLineItem().getApVoucher().getVouDescription();
    						ADJ_CRTD_BY = invPropagatedCosting.getApVoucherLineItem().getApVoucher().getVouPostedBy();
    						ADJ_RFRNC_NMBR = "APVOU" +
							invPropagatedCosting.getApVoucherLineItem().getApVoucher().getVouDocumentNumber();

    					} else if (invPropagatedCosting.getApVoucherLineItem().getApCheck() != null) {

    						ADJ_DSCRPTN = invPropagatedCosting.getApVoucherLineItem().getApCheck().getChkDescription();
    						ADJ_CRTD_BY = invPropagatedCosting.getApVoucherLineItem().getApCheck().getChkPostedBy();
    						ADJ_RFRNC_NMBR = "APCHK" +
							invPropagatedCosting.getApVoucherLineItem().getApCheck().getChkDocumentNumber();

    					}

    				} else if (invPropagatedCosting.getArInvoiceLineItem() != null){

    					QNTY = this.convertByUomFromAndItemAndQuantity(
								invPropagatedCosting.getArInvoiceLineItem().getInvUnitOfMeasure(),
								invPropagatedCosting.getArInvoiceLineItem().getInvItemLocation().getInvItem(),
								invPropagatedCosting.getArInvoiceLineItem().getIliQuantity(), AD_CMPNY);
   						TTL_CST = prevInvCosting.getCstRemainingValue() - invPropagatedCosting.getCstRemainingValue();

   						if(invPropagatedCosting.getArInvoiceLineItem().getArInvoice() != null){

   	   							ADJ_DSCRPTN = invPropagatedCosting.getArInvoiceLineItem().getArInvoice().getInvDescription();
   	   							ADJ_CRTD_BY = invPropagatedCosting.getArInvoiceLineItem().getArInvoice().getInvPostedBy();
   	   							ADJ_RFRNC_NMBR = "ARCM" +
   								invPropagatedCosting.getArInvoiceLineItem().getArInvoice().getInvNumber();

   	   						} else if(invPropagatedCosting.getArInvoiceLineItem().getArReceipt() != null){

   	   							ADJ_DSCRPTN = invPropagatedCosting.getArInvoiceLineItem().getArReceipt().getRctDescription();
   	   							ADJ_CRTD_BY = invPropagatedCosting.getArInvoiceLineItem().getArReceipt().getRctPostedBy();
   	   							ADJ_RFRNC_NMBR = "ARMR" +
   								invPropagatedCosting.getArInvoiceLineItem().getArReceipt().getRctNumber();

   	   						}

    				} else if (invPropagatedCosting.getArSalesOrderInvoiceLine() != null){

   						TTL_CST = prevInvCosting.getCstRemainingValue() - invPropagatedCosting.getCstRemainingValue();
    					QNTY = this.convertByUomFromAndItemAndQuantity(
								invPropagatedCosting.getArSalesOrderInvoiceLine().getArSalesOrderLine().getInvUnitOfMeasure(),
								invPropagatedCosting.getArSalesOrderInvoiceLine().getArSalesOrderLine().getInvItemLocation().getInvItem(),
								invPropagatedCosting.getArSalesOrderInvoiceLine().getSilQuantityDelivered(), AD_CMPNY);

    					ADJ_DSCRPTN = invPropagatedCosting.getArSalesOrderInvoiceLine().getArInvoice().getInvDescription();
    					ADJ_CRTD_BY = invPropagatedCosting.getArSalesOrderInvoiceLine().getArInvoice().getInvPostedBy();
    					ADJ_RFRNC_NMBR = "ARCM" +
						invPropagatedCosting.getArSalesOrderInvoiceLine().getArInvoice().getInvNumber();

    				} else if (invPropagatedCosting.getInvAdjustmentLine() != null){

    					ADJ_DSCRPTN = invPropagatedCosting.getInvAdjustmentLine().getInvAdjustment().getAdjDescription();
    					ADJ_CRTD_BY = invPropagatedCosting.getInvAdjustmentLine().getInvAdjustment().getAdjPostedBy();
    					ADJ_RFRNC_NMBR = "INVADJ" +
						invPropagatedCosting.getInvAdjustmentLine().getInvAdjustment().getAdjDocumentNumber();

    					if(invPropagatedCosting.getInvAdjustmentLine().getAlAdjustQuantity() != 0) {

    						TTL_CST = (invPropagatedCosting.getInvAdjustmentLine().getAlUnitCost() *
    								invPropagatedCosting.getInvAdjustmentLine().getAlAdjustQuantity());
    						QNTY =  this.convertByUomFromAndItemAndQuantity(
    								invPropagatedCosting.getInvAdjustmentLine().getInvUnitOfMeasure(),
									invPropagatedCosting.getInvAdjustmentLine().getInvItemLocation().getInvItem(),
									invPropagatedCosting.getInvAdjustmentLine().getAlAdjustQuantity(), AD_CMPNY);

    					}

    				} else if (invPropagatedCosting.getInvAssemblyTransferLine() != null){

    					TTL_CST = invPropagatedCosting.getInvAssemblyTransferLine().getAtlAssembleCost();
    					QNTY = invPropagatedCosting.getInvAssemblyTransferLine().getAtlAssembleQuantity();
    					ADJ_DSCRPTN = invPropagatedCosting.getInvAssemblyTransferLine().getInvAssemblyTransfer().getAtrDescription();
    					ADJ_CRTD_BY = invPropagatedCosting.getInvAssemblyTransferLine().getInvAssemblyTransfer().getAtrPostedBy();
    					ADJ_RFRNC_NMBR = "INVAT" +
						invPropagatedCosting.getInvAssemblyTransferLine().getInvAssemblyTransfer().getAtrDocumentNumber();

    				} else if (invPropagatedCosting.getInvBranchStockTransferLine() != null){

    					if(invPropagatedCosting.getInvBranchStockTransferLine().getInvBranchStockTransfer().getBstTransferOutNumber()
    							!= null) {

    						TTL_CST = invPropagatedCosting.getInvBranchStockTransferLine().getBslAmount();
							QNTY =  this.convertByUomFromAndItemAndQuantity(
									invPropagatedCosting.getInvBranchStockTransferLine().getInvUnitOfMeasure(),
									invPropagatedCosting.getInvBranchStockTransferLine().getInvItemLocation().getInvItem(),
									invPropagatedCosting.getInvBranchStockTransferLine().getBslQuantityReceived(), AD_CMPNY);

    					} else {

    						TTL_CST = invPropagatedCosting.getInvBranchStockTransferLine().getBslAmount();
							QNTY =  this.convertByUomFromAndItemAndQuantity(
									invPropagatedCosting.getInvBranchStockTransferLine().getInvUnitOfMeasure(),
									invPropagatedCosting.getInvBranchStockTransferLine().getInvItemLocation().getInvItem(),
									invPropagatedCosting.getInvBranchStockTransferLine().getBslQuantity(), AD_CMPNY);
    					}

    					ADJ_DSCRPTN =
    						invPropagatedCosting.getInvBranchStockTransferLine().getInvBranchStockTransfer().getBstDescription();
    					ADJ_CRTD_BY =
    						invPropagatedCosting.getInvBranchStockTransferLine().getInvBranchStockTransfer().getBstPostedBy();
    					ADJ_RFRNC_NMBR = "INVBST" +
						invPropagatedCosting.getInvBranchStockTransferLine().getInvBranchStockTransfer().getBstNumber();

    				} else if (invPropagatedCosting.getInvBuildUnbuildAssemblyLine() != null){

   						TTL_CST = prevInvCosting.getCstRemainingValue() - invPropagatedCosting.getCstRemainingValue();
    					QNTY =  invPropagatedCosting.getInvBuildUnbuildAssemblyLine().getBlBuildQuantity();
    					ADJ_DSCRPTN =
    						invPropagatedCosting.getInvBuildUnbuildAssemblyLine().getInvBuildUnbuildAssembly().getBuaDescription();
    					ADJ_CRTD_BY =
    						invPropagatedCosting.getInvBuildUnbuildAssemblyLine().getInvBuildUnbuildAssembly().getBuaPostedBy();
    					ADJ_RFRNC_NMBR = "INVBUA" +
						invPropagatedCosting.getInvBuildUnbuildAssemblyLine().getInvBuildUnbuildAssembly().getBuaDocumentNumber();

    				} else if (invPropagatedCosting.getInvStockIssuanceLine()!= null){

    					TTL_CST = invPropagatedCosting.getInvStockIssuanceLine().getSilIssueCost();
						QNTY =  this.convertByUomFromAndItemAndQuantity(
								invPropagatedCosting.getInvStockIssuanceLine().getInvUnitOfMeasure(),
								invPropagatedCosting.getInvStockIssuanceLine().getInvItemLocation().getInvItem(),
								invPropagatedCosting.getInvStockIssuanceLine().getSilIssueQuantity(), AD_CMPNY);
    					ADJ_DSCRPTN = invPropagatedCosting.getInvStockIssuanceLine().getInvStockIssuance().getSiDescription();
    					ADJ_CRTD_BY = invPropagatedCosting.getInvStockIssuanceLine().getInvStockIssuance().getSiPostedBy();
    					ADJ_RFRNC_NMBR = "INVSI" +
						invPropagatedCosting.getInvStockIssuanceLine().getInvStockIssuance().getSiDocumentNumber();

    				} else if (invPropagatedCosting.getInvStockTransferLine()!= null) {

    					TTL_CST = invPropagatedCosting.getInvStockTransferLine().getStlAmount();
						QNTY =  this.convertByUomFromAndItemAndQuantity(
								invPropagatedCosting.getInvStockTransferLine().getInvUnitOfMeasure(),
								invPropagatedCosting.getInvStockTransferLine().getInvItem(),
								invPropagatedCosting.getInvStockTransferLine().getStlQuantityDelivered(), AD_CMPNY);
    					ADJ_DSCRPTN = invPropagatedCosting.getInvStockTransferLine().getInvStockTransfer().getStDescription();
    					ADJ_CRTD_BY = invPropagatedCosting.getInvStockTransferLine().getInvStockTransfer().getStPostedBy();
    					ADJ_RFRNC_NMBR = "INVST" +
						invPropagatedCosting.getInvStockTransferLine().getInvStockTransfer().getStDocumentNumber();

    				} else {

    					prevInvCosting = invPropagatedCosting;
    					continue;

    				}

    				// if quantity is equal 0, no variance.
    				if(QNTY == 0) continue;

    				// compute new cost variance
    				double UNT_CST = TTL_CST/QNTY;
    				double CST_VRNC_VL = (invPropagatedCosting.getCstRemainingQuantity() * UNT_CST -
    						invPropagatedCosting.getCstRemainingValue());

    				if(CST_VRNC_VL != 0)
    					this.generateCostVariance(invPropagatedCosting.getInvItemLocation(), CST_VRNC_VL, ADJ_RFRNC_NMBR,
    							ADJ_DSCRPTN, invPropagatedCosting.getCstDate(), ADJ_CRTD_BY, AD_BRNCH, AD_CMPNY);

    			}

    			// set previous costing
    			prevInvCosting = invPropagatedCosting;

    		}

    	} catch (AdPRFCoaGlVarianceAccountNotFoundException ex){

    		throw ex;

    	} catch (Exception ex) {

    		Debug.printStackTrace(ex);
    		getSessionContext().setRollbackOnly();
    		throw new EJBException(ex.getMessage());

    	}   */
    }

    private void addInvDrEntry(short DR_LN, String DR_CLSS, byte DR_DBT, double DR_AMNT, byte DR_RVRSL, Integer COA_CODE,
    		LocalInvAdjustment invAdjustment, Integer AD_BRNCH, Integer AD_CMPNY)

    throws GlobalBranchAccountNumberInvalidException {

    	Debug.print("ArApprovalControllerBean addInvDrEntry");

    	LocalAdCompanyHome adCompanyHome = null;
    	LocalInvDistributionRecordHome invDistributionRecordHome = null;
    	LocalGlChartOfAccountHome glChartOfAccountHome = null;

    	// Initialize EJB Home

    	try {

    		adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
    		invDistributionRecordHome = (LocalInvDistributionRecordHome)EJBHomeFactory.
			lookUpLocalHome(LocalInvDistributionRecordHome.JNDI_NAME, LocalInvDistributionRecordHome.class);
    		glChartOfAccountHome = (LocalGlChartOfAccountHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlChartOfAccountHome.JNDI_NAME, LocalGlChartOfAccountHome.class);


    	} catch (NamingException ex) {

    		throw new EJBException(ex.getMessage());

    	}

    	try {

    		// get company

    		LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);

    		// validate coa

    		LocalGlChartOfAccount glChartOfAccount = null;

    		try {

    			glChartOfAccount = glChartOfAccountHome.findByCoaCodeAndBranchCode(COA_CODE, AD_BRNCH, AD_CMPNY);

    		} catch(FinderException ex) {

    			throw new GlobalBranchAccountNumberInvalidException ();

    		}

    		// create distribution record

    		LocalInvDistributionRecord invDistributionRecord = invDistributionRecordHome.create(DR_LN, DR_CLSS, DR_DBT,
    				EJBCommon.roundIt(DR_AMNT, adCompany.getGlFunctionalCurrency().getFcPrecision()), DR_RVRSL, EJBCommon.FALSE,
					 AD_CMPNY);

    		invAdjustment.addInvDistributionRecord(invDistributionRecord);
    		glChartOfAccount.addInvDistributionRecord(invDistributionRecord);

    	} catch(GlobalBranchAccountNumberInvalidException ex) {

    		throw new GlobalBranchAccountNumberInvalidException ();

    	} catch (Exception ex) {

    		Debug.printStackTrace(ex);
    		getSessionContext().setRollbackOnly();
    		throw new EJBException(ex.getMessage());

    	}

    }

    private void executeInvAdjPost(Integer ADJ_CODE, String USR_NM, Integer AD_BRNCH, Integer AD_CMPNY) throws
	GlobalRecordAlreadyDeletedException,
	GlobalTransactionAlreadyPostedException,
	GlJREffectiveDateNoPeriodExistException,
	GlJREffectiveDatePeriodClosedException,
	GlobalJournalNotBalanceException,
	GlobalBranchAccountNumberInvalidException {

    	Debug.print("ArApprovalControllerBean executeInvAdjPost");

    	LocalInvAdjustmentHome invAdjustmentHome = null;
    	LocalAdCompanyHome adCompanyHome = null;
    	LocalAdPreferenceHome adPreferenceHome = null;
    	LocalGlSetOfBookHome glSetOfBookHome = null;
    	LocalGlAccountingCalendarValueHome glAccountingCalendarValueHome = null;
    	LocalGlJournalHome glJournalHome = null;
    	LocalGlJournalBatchHome glJournalBatchHome = null;
    	LocalGlSuspenseAccountHome glSuspenseAccountHome = null;
    	LocalGlJournalLineHome glJournalLineHome = null;
    	LocalGlJournalSourceHome glJournalSourceHome = null;
    	LocalGlJournalCategoryHome glJournalCategoryHome = null;
    	LocalGlFunctionalCurrencyHome glFunctionalCurrencyHome = null;
    	LocalInvDistributionRecordHome invDistributionRecordHome = null;
    	LocalInvCostingHome invCostingHome = null;
    	LocalGlChartOfAccountHome glChartOfAccountHome = null;
    	LocalInvAdjustmentLineHome invAdjustmentLineHome = null;


    	// Initialize EJB Home

    	try {

    		invAdjustmentHome = (LocalInvAdjustmentHome)EJBHomeFactory.
			lookUpLocalHome(LocalInvAdjustmentHome.JNDI_NAME, LocalInvAdjustmentHome.class);
    		adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
    		adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);
    		glSetOfBookHome = (LocalGlSetOfBookHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlSetOfBookHome.JNDI_NAME, LocalGlSetOfBookHome.class);
    		glAccountingCalendarValueHome = (LocalGlAccountingCalendarValueHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlAccountingCalendarValueHome.JNDI_NAME, LocalGlAccountingCalendarValueHome.class);
    		glJournalHome = (LocalGlJournalHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlJournalHome.JNDI_NAME, LocalGlJournalHome.class);
    		glJournalBatchHome = (LocalGlJournalBatchHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlJournalBatchHome.JNDI_NAME, LocalGlJournalBatchHome.class);
    		glSuspenseAccountHome = (LocalGlSuspenseAccountHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlSuspenseAccountHome.JNDI_NAME, LocalGlSuspenseAccountHome.class);
    		glJournalLineHome = (LocalGlJournalLineHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlJournalLineHome.JNDI_NAME, LocalGlJournalLineHome.class);
    		glJournalSourceHome = (LocalGlJournalSourceHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlJournalSourceHome.JNDI_NAME, LocalGlJournalSourceHome.class);
    		glJournalCategoryHome = (LocalGlJournalCategoryHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlJournalCategoryHome.JNDI_NAME, LocalGlJournalCategoryHome.class);
    		glFunctionalCurrencyHome = (LocalGlFunctionalCurrencyHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlFunctionalCurrencyHome.JNDI_NAME, LocalGlFunctionalCurrencyHome.class);
    		invDistributionRecordHome = (LocalInvDistributionRecordHome)EJBHomeFactory.
			lookUpLocalHome(LocalInvDistributionRecordHome.JNDI_NAME, LocalInvDistributionRecordHome.class);
    		invCostingHome = (LocalInvCostingHome)EJBHomeFactory.
			lookUpLocalHome(LocalInvCostingHome.JNDI_NAME, LocalInvCostingHome.class);
    		glChartOfAccountHome = (LocalGlChartOfAccountHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlChartOfAccountHome.JNDI_NAME, LocalGlChartOfAccountHome.class);
    		invAdjustmentLineHome = (LocalInvAdjustmentLineHome)EJBHomeFactory.
			lookUpLocalHome(LocalInvAdjustmentLineHome.JNDI_NAME, LocalInvAdjustmentLineHome.class);


    	} catch (NamingException ex) {

    		throw new EJBException(ex.getMessage());

    	}

    	try {

    		// validate if adjustment is already deleted

    		LocalInvAdjustment invAdjustment = null;

    		try {

    			invAdjustment = invAdjustmentHome.findByPrimaryKey(ADJ_CODE);

    		} catch (FinderException ex) {

    			throw new GlobalRecordAlreadyDeletedException();

    		}

    		// validate if adjustment is already posted or void

    		if (invAdjustment.getAdjPosted() == EJBCommon.TRUE) {

    			if (invAdjustment.getAdjVoid() != EJBCommon.TRUE)
    			throw new GlobalTransactionAlreadyPostedException();

    		}


    		Collection invAdjustmentLines = null;

    		if(invAdjustment.getAdjVoid() == EJBCommon.FALSE)
    			invAdjustmentLines = invAdjustmentLineHome.findByAlVoidAndAdjCode(EJBCommon.FALSE, invAdjustment.getAdjCode(), AD_CMPNY);
    		else
    			invAdjustmentLines = invAdjustmentLineHome.findByAlVoidAndAdjCode(EJBCommon.TRUE, invAdjustment.getAdjCode(), AD_CMPNY);


    		Iterator i = invAdjustmentLines.iterator();

    		while(i.hasNext()) {


    			LocalInvAdjustmentLine invAdjustmentLine = (LocalInvAdjustmentLine) i.next();

    			LocalInvCosting invCosting =
    				invCostingHome.getByMaxCstDateToLongAndMaxCstLineNumberAndLessThanEqualCstDateAndIiNameAndLocName(
    						invAdjustment.getAdjDate(), invAdjustmentLine.getInvItemLocation().getInvItem().getIiName(),
    						invAdjustmentLine.getInvItemLocation().getInvLocation().getLocName(), AD_BRNCH, AD_CMPNY);

				this.postInvAdjustmentToInventory(invAdjustmentLine, invAdjustment.getAdjDate(), 0,
						invAdjustmentLine.getAlUnitCost(), invCosting.getCstRemainingQuantity(),
						invCosting.getCstRemainingValue() + invAdjustmentLine.getAlUnitCost(), AD_BRNCH, AD_CMPNY);

    		}

    		// post to gl if necessary

    		LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);
    		LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);

    			// validate if date has no period and period is closed

    			LocalGlSetOfBook glJournalSetOfBook = null;

    			try {

    				glJournalSetOfBook = glSetOfBookHome.findByDate(invAdjustment.getAdjDate(), AD_CMPNY);

    			} catch (FinderException ex) {

    				throw new GlJREffectiveDateNoPeriodExistException();

    			}

    			LocalGlAccountingCalendarValue glAccountingCalendarValue =
    				glAccountingCalendarValueHome.findByAcCodeAndDate(
    						glJournalSetOfBook.getGlAccountingCalendar().getAcCode(), invAdjustment.getAdjDate(), AD_CMPNY);


    			if (glAccountingCalendarValue.getAcvStatus() == 'N' ||
    					glAccountingCalendarValue.getAcvStatus() == 'C' ||
						glAccountingCalendarValue.getAcvStatus() == 'P') {

    				throw new GlJREffectiveDatePeriodClosedException();

    			}

    			// check if invoice is balance if not check suspense posting

    			LocalGlJournalLine glOffsetJournalLine = null;

    			Collection invDistributionRecords = null;

    			if (invAdjustment.getAdjVoid() == EJBCommon.FALSE) {

    				invDistributionRecords = invDistributionRecordHome.findImportableDrByDrReversedAndAdjCode(EJBCommon.FALSE,
    						invAdjustment.getAdjCode(), AD_CMPNY);

    			} else {

    				invDistributionRecords = invDistributionRecordHome.findImportableDrByDrReversedAndAdjCode(EJBCommon.TRUE,
    						invAdjustment.getAdjCode(), AD_CMPNY);

    			}


    			Iterator j = invDistributionRecords.iterator();

    			double TOTAL_DEBIT = 0d;
    			double TOTAL_CREDIT = 0d;

    			while (j.hasNext()) {

    				LocalInvDistributionRecord invDistributionRecord = (LocalInvDistributionRecord)j.next();

    				double DR_AMNT = 0d;

    				DR_AMNT = invDistributionRecord.getDrAmount();

    				if (invDistributionRecord.getDrDebit() == EJBCommon.TRUE) {

    					TOTAL_DEBIT += DR_AMNT;

    				} else {

    					TOTAL_CREDIT += DR_AMNT;

    				}

    			}

    			TOTAL_DEBIT = EJBCommon.roundIt(TOTAL_DEBIT, adCompany.getGlFunctionalCurrency().getFcPrecision());
    			TOTAL_CREDIT = EJBCommon.roundIt(TOTAL_CREDIT, adCompany.getGlFunctionalCurrency().getFcPrecision());

    			if (adPreference.getPrfAllowSuspensePosting() == EJBCommon.TRUE &&
    					TOTAL_DEBIT != TOTAL_CREDIT) {

    				LocalGlSuspenseAccount glSuspenseAccount = null;

    				try {

    					glSuspenseAccount = glSuspenseAccountHome.findByJsNameAndJcName("INVENTORY", "INVENTORY ADJUSTMENTS",
    							AD_CMPNY);

    				} catch (FinderException ex) {

    					throw new GlobalJournalNotBalanceException();

    				}

    				if (TOTAL_DEBIT - TOTAL_CREDIT < 0) {

    					glOffsetJournalLine = glJournalLineHome.create((short)(invDistributionRecords.size() + 1), EJBCommon.TRUE,
    							TOTAL_CREDIT - TOTAL_DEBIT, "", AD_CMPNY);

    				} else {

    					glOffsetJournalLine = glJournalLineHome.create((short)(invDistributionRecords.size() + 1), EJBCommon.FALSE,
    							TOTAL_DEBIT - TOTAL_CREDIT, "", AD_CMPNY);

    				}

    				LocalGlChartOfAccount glChartOfAccount = glSuspenseAccount.getGlChartOfAccount();
    				glChartOfAccount.addGlJournalLine(glOffsetJournalLine);


    			} else if (adPreference.getPrfAllowSuspensePosting() == EJBCommon.FALSE &&
    					TOTAL_DEBIT != TOTAL_CREDIT) {

    				throw new GlobalJournalNotBalanceException();

    			}

    			// create journal batch if necessary

    			LocalGlJournalBatch glJournalBatch = null;
    			java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat("MM/dd/yyyy");

    			try {

    				glJournalBatch = glJournalBatchHome.findByJbName("JOURNAL IMPORT " + formatter.format(new Date()) +
    						" INVENTORY ADJUSTMENTS", AD_BRNCH, AD_CMPNY);

    			} catch (FinderException ex) {

    			}

    			if (glJournalBatch == null) {

    				glJournalBatch = glJournalBatchHome.create("JOURNAL IMPORT " + formatter.format(new Date()) +
    						" INVENTORY ADJUSTMENTS", "JOURNAL IMPORT", "CLOSED", EJBCommon.getGcCurrentDateWoTime().getTime(),
							USR_NM, AD_BRNCH, AD_CMPNY);

    			}

    			// create journal entry

    			LocalGlJournal glJournal = glJournalHome.create(invAdjustment.getAdjReferenceNumber(),
    					invAdjustment.getAdjDescription(), invAdjustment.getAdjDate(),
						0.0d, null, invAdjustment.getAdjDocumentNumber(), null, 1d, "N/A", null,
						'N', EJBCommon.TRUE, EJBCommon.FALSE,
						USR_NM, new Date(),
						USR_NM, new Date(),
						null, null,
						USR_NM, EJBCommon.getGcCurrentDateWoTime().getTime(),
						null, null, EJBCommon.FALSE, null,
						AD_BRNCH, AD_CMPNY);

    			LocalGlJournalSource glJournalSource = glJournalSourceHome.findByJsName("INVENTORY", AD_CMPNY);
    			glJournal.setGlJournalSource(glJournalSource);

    			LocalGlFunctionalCurrency glFunctionalCurrency = glFunctionalCurrencyHome.findByFcName(adCompany.getGlFunctionalCurrency().getFcName(), AD_CMPNY);
    			glJournal.setGlFunctionalCurrency(glFunctionalCurrency);

    			LocalGlJournalCategory glJournalCategory = glJournalCategoryHome.findByJcName("INVENTORY ADJUSTMENTS", AD_CMPNY);
    			glJournal.setGlJournalCategory(glJournalCategory);

    			if (glJournalBatch != null) {

    				glJournal.setGlJournalBatch(glJournalBatch);

    			}

    			// create journal lines

    			j = invDistributionRecords.iterator();

    			while (j.hasNext()) {

    				LocalInvDistributionRecord invDistributionRecord = (LocalInvDistributionRecord)j.next();

    				double DR_AMNT = 0d;

    				DR_AMNT = invDistributionRecord.getDrAmount();

    				LocalGlJournalLine glJournalLine = glJournalLineHome.create(invDistributionRecord.getDrLine(),
    						invDistributionRecord.getDrDebit(), DR_AMNT, "", AD_CMPNY);

    				invDistributionRecord.getInvChartOfAccount().addGlJournalLine(glJournalLine);

    				glJournal.addGlJournalLine(glJournalLine);

    				invDistributionRecord.setDrImported(EJBCommon.TRUE);


    			}

    			if (glOffsetJournalLine != null) {

    				glJournal.addGlJournalLine(glOffsetJournalLine);

    			}

    			// post journal to gl

    			Collection glJournalLines = glJournal.getGlJournalLines();

    			i = glJournalLines.iterator();

    			while (i.hasNext()) {

    				LocalGlJournalLine glJournalLine = (LocalGlJournalLine)i.next();

    				// post current to current acv

    				this.postToGl(glAccountingCalendarValue,
    						glJournalLine.getGlChartOfAccount(),
							true, glJournalLine.getJlDebit(), glJournalLine.getJlAmount(), AD_CMPNY);


    				// post to subsequent acvs (propagate)

    				Collection glSubsequentAccountingCalendarValues =
    					glAccountingCalendarValueHome.findSubsequentAcvByAcCodeAndAcvPeriodNumber(
    							glJournalSetOfBook.getGlAccountingCalendar().getAcCode(),
								glAccountingCalendarValue.getAcvPeriodNumber(), AD_CMPNY);

    				Iterator acvsIter = glSubsequentAccountingCalendarValues.iterator();

    				while (acvsIter.hasNext()) {

    					LocalGlAccountingCalendarValue glSubsequentAccountingCalendarValue =
    						(LocalGlAccountingCalendarValue)acvsIter.next();

    					this.postToGl(glSubsequentAccountingCalendarValue,
    							glJournalLine.getGlChartOfAccount(),
								false, glJournalLine.getJlDebit(), glJournalLine.getJlAmount(), AD_CMPNY);

    				}

    				// post to subsequent years if necessary

    				Collection glSubsequentSetOfBooks = glSetOfBookHome.findSubsequentSobByAcYear(
    						glJournalSetOfBook.getGlAccountingCalendar().getAcYear(), AD_CMPNY);

    				if (!glSubsequentSetOfBooks.isEmpty() && glJournalSetOfBook.getSobYearEndClosed() == 1) {

    					adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
    					LocalGlChartOfAccount glRetainedEarningsAccount =
    						glChartOfAccountHome.findByCoaAccountNumberAndBranchCode(adCompany.getCmpRetainedEarnings(),
    								AD_BRNCH, AD_CMPNY);

    					Iterator sobIter = glSubsequentSetOfBooks.iterator();

    					while (sobIter.hasNext()) {

    						LocalGlSetOfBook glSubsequentSetOfBook = (LocalGlSetOfBook)sobIter.next();

    						String ACCOUNT_TYPE = glJournalLine.getGlChartOfAccount().getCoaAccountType();

    						// post to subsequent acvs of subsequent set of book(propagate)

    						Collection glAccountingCalendarValues =
    							glAccountingCalendarValueHome.findByAcCode(
    									glSubsequentSetOfBook.getGlAccountingCalendar().getAcCode(), AD_CMPNY);

    						Iterator acvIter = glAccountingCalendarValues.iterator();

    						while (acvIter.hasNext()) {

    							LocalGlAccountingCalendarValue glSubsequentAccountingCalendarValue =
    								(LocalGlAccountingCalendarValue)acvIter.next();

    							if (ACCOUNT_TYPE.equals("ASSET") || ACCOUNT_TYPE.equals("LIABILITY") ||
    									ACCOUNT_TYPE.equals("OWNERS EQUITY")) {

    								this.postToGl(glSubsequentAccountingCalendarValue,
    										glJournalLine.getGlChartOfAccount(),
											false, glJournalLine.getJlDebit(), glJournalLine.getJlAmount(), AD_CMPNY);

    							} else { // revenue & expense

    								this.postToGl(glSubsequentAccountingCalendarValue,
    										glRetainedEarningsAccount,
											false, glJournalLine.getJlDebit(), glJournalLine.getJlAmount(), AD_CMPNY);

    							}

    						}

    						if (glSubsequentSetOfBook.getSobYearEndClosed() == 0) break;

    					}

    				}

    			}

			invAdjustment.setAdjPosted(EJBCommon.TRUE);

    	} catch (GlJREffectiveDateNoPeriodExistException ex) {

    		getSessionContext().setRollbackOnly();
    		throw ex;

    	} catch (GlJREffectiveDatePeriodClosedException ex) {

    		getSessionContext().setRollbackOnly();
    		throw ex;

    	} catch (GlobalJournalNotBalanceException ex) {

    		getSessionContext().setRollbackOnly();
    		throw ex;

    	} catch (GlobalRecordAlreadyDeletedException ex) {

    		getSessionContext().setRollbackOnly();
    		throw ex;

    	} catch (GlobalTransactionAlreadyPostedException ex) {

    		getSessionContext().setRollbackOnly();
    		throw ex;

    	} catch (Exception ex) {

    		Debug.printStackTrace(ex);
    		getSessionContext().setRollbackOnly();
    		throw new EJBException(ex.getMessage());

    	}

    }

    private LocalInvAdjustmentLine addInvAlEntry(LocalInvItemLocation invItemLocation, LocalInvAdjustment invAdjustment,
    		double CST_VRNC_VL, byte AL_VD, Integer AD_CMPNY) {

    	Debug.print("ArApprovalControllerBean addInvAlEntry");

    	LocalInvAdjustmentLineHome invAdjustmentLineHome = null;

    	// Initialize EJB Home

    	try {

    		invAdjustmentLineHome = (LocalInvAdjustmentLineHome)EJBHomeFactory.
			lookUpLocalHome(LocalInvAdjustmentLineHome.JNDI_NAME, LocalInvAdjustmentLineHome.class);

    	} catch (NamingException ex) {

    		throw new EJBException(ex.getMessage());

    	}

    	try {

    		// create dr entry
    		LocalInvAdjustmentLine invAdjustmentLine = null;
    		invAdjustmentLine = invAdjustmentLineHome.create(CST_VRNC_VL,null, null, 0,0, AL_VD, AD_CMPNY);

    		// map adjustment, unit of measure, item location
    		invAdjustment.addInvAdjustmentLine(invAdjustmentLine);
    		invItemLocation.getInvItem().getInvUnitOfMeasure().addInvAdjustmentLine(invAdjustmentLine);
    		invItemLocation.addInvAdjustmentLine(invAdjustmentLine);

    		return invAdjustmentLine;

    	} catch (Exception ex) {

    		Debug.printStackTrace(ex);
    		getSessionContext().setRollbackOnly();
    		throw new EJBException(ex.getMessage());

    	}

    }

    private LocalInvAdjustment saveInvAdjustment(String ADJ_RFRNC_NMBR, String ADJ_DSCRPTN,
    		Date ADJ_DATE, String USR_NM, Integer AD_BRNCH, Integer AD_CMPNY){

    	Debug.print("ArApprovalControllerBean saveInvAdjustment");

    	LocalInvAdjustmentHome invAdjustmentHome = null;
    	LocalAdDocumentSequenceAssignmentHome adDocumentSequenceAssignmentHome = null;
    	LocalAdBranchDocumentSequenceAssignmentHome adBranchDocumentSequenceAssignmentHome = null;
    	// Initialize EJB Home

    	try{

    		invAdjustmentHome = (LocalInvAdjustmentHome)EJBHomeFactory.
			lookUpLocalHome(LocalInvAdjustmentHome.JNDI_NAME, LocalInvAdjustmentHome.class);
			adDocumentSequenceAssignmentHome = (LocalAdDocumentSequenceAssignmentHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdDocumentSequenceAssignmentHome.JNDI_NAME, LocalAdDocumentSequenceAssignmentHome.class);
			adBranchDocumentSequenceAssignmentHome = (LocalAdBranchDocumentSequenceAssignmentHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdBranchDocumentSequenceAssignmentHome.JNDI_NAME, LocalAdBranchDocumentSequenceAssignmentHome.class);

    	} catch (NamingException ex) {

    		throw new EJBException(ex.getMessage());

    	}

    	try{

    		// generate adj document number
    		String ADJ_DCMNT_NMBR = null;

    		LocalAdBranchDocumentSequenceAssignment adBranchDocumentSequenceAssignment = null;
    		LocalAdDocumentSequenceAssignment adDocumentSequenceAssignment = null;

    		try {

    			adDocumentSequenceAssignment = adDocumentSequenceAssignmentHome.findByDcName("INV ADJUSTMENT", AD_CMPNY);

    		} catch (FinderException ex) {

    		}

    		try {

    			adBranchDocumentSequenceAssignment = adBranchDocumentSequenceAssignmentHome.findBdsByDsaCodeAndBrCode(
    					adDocumentSequenceAssignment.getDsaCode(), AD_BRNCH, AD_CMPNY);

    		} catch (FinderException ex) {

    		}

    		while (true) {

    			if (adBranchDocumentSequenceAssignment == null || adBranchDocumentSequenceAssignment.getBdsNextSequence() == null) {

    				try {

    					invAdjustmentHome.findByAdjDocumentNumberAndBrCode(adDocumentSequenceAssignment.getDsaNextSequence(),
    							AD_BRNCH, AD_CMPNY);
    					adDocumentSequenceAssignment.setDsaNextSequence(EJBCommon.incrementStringNumber(
    							adDocumentSequenceAssignment.getDsaNextSequence()));

    				} catch (FinderException ex) {

    					ADJ_DCMNT_NMBR = adDocumentSequenceAssignment.getDsaNextSequence();
    					adDocumentSequenceAssignment.setDsaNextSequence(EJBCommon.incrementStringNumber(
    							adDocumentSequenceAssignment.getDsaNextSequence()));
    					break;

    				}

    			} else {

    				try {

    					invAdjustmentHome.findByAdjDocumentNumberAndBrCode(
    							adBranchDocumentSequenceAssignment.getBdsNextSequence(), AD_BRNCH, AD_CMPNY);
    					adBranchDocumentSequenceAssignment.setBdsNextSequence(EJBCommon.incrementStringNumber(
    							adBranchDocumentSequenceAssignment.getBdsNextSequence()));

    				} catch (FinderException ex) {

    					ADJ_DCMNT_NMBR = adBranchDocumentSequenceAssignment.getBdsNextSequence();
    					adBranchDocumentSequenceAssignment.setBdsNextSequence(EJBCommon.incrementStringNumber(
    							adBranchDocumentSequenceAssignment.getBdsNextSequence()));
    					break;

    				}

    			}

    		}

    		LocalInvAdjustment invAdjustment = invAdjustmentHome.create(ADJ_DCMNT_NMBR, ADJ_RFRNC_NMBR,
    				ADJ_DSCRPTN, ADJ_DATE, "COST-VARIANCE", "N/A", EJBCommon.FALSE, USR_NM, ADJ_DATE, USR_NM, ADJ_DATE, null, null,
					USR_NM, ADJ_DATE, null, null, EJBCommon.TRUE, EJBCommon.FALSE, AD_BRNCH, AD_CMPNY);

    		return invAdjustment;

    	} catch (Exception ex) {

    		Debug.printStackTrace(ex);
    		getSessionContext().setRollbackOnly();
    		throw new EJBException(ex.getMessage());

    	}


    }

    private void postInvAdjustmentToInventory(LocalInvAdjustmentLine invAdjustmentLine, Date CST_DT, double CST_ADJST_QTY,
    		double CST_ADJST_CST, double CST_RMNNG_QTY, double CST_RMNNG_VL, Integer AD_BRNCH,Integer AD_CMPNY) {

    	Debug.print("ArApprovalControllerBean postInvAdjustmentToInventory");

    	LocalInvCostingHome invCostingHome = null;
    	LocalAdPreferenceHome adPreferenceHome = null;
    	LocalAdCompanyHome adCompanyHome = null;

    	// Initialize EJB Home

    	try {

    		invCostingHome = (LocalInvCostingHome)EJBHomeFactory.
			lookUpLocalHome(LocalInvCostingHome.JNDI_NAME, LocalInvCostingHome.class);
    		adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);
    		adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);

    	} catch (NamingException ex) {

    		getSessionContext().setRollbackOnly();
    		throw new EJBException(ex.getMessage());

    	}

    	try {

    		LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);
    		LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
    		LocalInvItemLocation invItemLocation = invAdjustmentLine.getInvItemLocation();
    		int CST_LN_NMBR = 0;

    		CST_ADJST_QTY = EJBCommon.roundIt(CST_ADJST_QTY, adPreference.getPrfInvQuantityPrecisionUnit());
    		CST_ADJST_CST = EJBCommon.roundIt(CST_ADJST_CST, adCompany.getGlFunctionalCurrency().getFcPrecision());
    		CST_RMNNG_QTY = EJBCommon.roundIt(CST_RMNNG_QTY, adPreference.getPrfInvQuantityPrecisionUnit());
    		CST_RMNNG_VL = EJBCommon.roundIt(CST_RMNNG_VL, adCompany.getGlFunctionalCurrency().getFcPrecision());

    		if (CST_ADJST_QTY < 0) {

    			invItemLocation.setIlCommittedQuantity(invItemLocation.getIlCommittedQuantity() - Math.abs(CST_ADJST_QTY));

    		}

    		// create costing

    		try {

    			// generate line number

    			LocalInvCosting invCurrentCosting = invCostingHome.getByMaxCstLineNumberAndCstDateToLongAndIiNameAndLocName(
    					CST_DT.getTime(), invItemLocation.getInvItem().getIiName(), invItemLocation.getInvLocation().getLocName(),
						AD_BRNCH, AD_CMPNY);
    			CST_LN_NMBR = invCurrentCosting.getCstLineNumber() + 1;

    		} catch (FinderException ex) {

    			CST_LN_NMBR = 1;

    		}

    		LocalInvCosting invCosting = invCostingHome.create(CST_DT, CST_DT.getTime(), CST_LN_NMBR, 0d, 0d, 0d, 0d,
    				CST_ADJST_QTY, CST_ADJST_CST, 0d, 0d, CST_RMNNG_QTY, CST_RMNNG_VL, 0d, 0d, CST_ADJST_QTY > 0 ? CST_ADJST_QTY : 0, AD_BRNCH, AD_CMPNY);
    		invItemLocation.addInvCosting(invCosting);
    		invCosting.setInvAdjustmentLine(invAdjustmentLine);

    		// propagate balance if necessary

    		Collection invCostings = invCostingHome.findByGreaterThanCstDateAndIiNameAndLocName(CST_DT, invItemLocation.getInvItem().getIiName(), invItemLocation.getInvLocation().getLocName(), AD_BRNCH, AD_CMPNY);

    		Iterator i = invCostings.iterator();

    		while (i.hasNext()) {

    			LocalInvCosting invPropagatedCosting = (LocalInvCosting)i.next();

    			invPropagatedCosting.setCstRemainingQuantity(invPropagatedCosting.getCstRemainingQuantity() + CST_ADJST_QTY);
    			invPropagatedCosting.setCstRemainingValue(invPropagatedCosting.getCstRemainingValue() + CST_ADJST_CST);

    		}


    	} catch (Exception ex) {

    		Debug.printStackTrace(ex);
    		getSessionContext().setRollbackOnly();
    		throw new EJBException(ex.getMessage());

    	}



    }

    // SessionBean methods

    /**
     * @ejb:create-method view-type="remote"
     **/
    public void ejbCreate() throws CreateException {

       Debug.print("ArApprovalControllerBean ejbCreate");

    }
}
