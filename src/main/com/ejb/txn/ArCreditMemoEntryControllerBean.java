 package com.ejb.txn;

import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;
import javax.naming.NamingException;

import com.ejb.ad.LocalAdAmountLimit;
import com.ejb.ad.LocalAdAmountLimitHome;
import com.ejb.ad.LocalAdApproval;
import com.ejb.ad.LocalAdApprovalHome;
import com.ejb.ad.LocalAdApprovalQueue;
import com.ejb.ad.LocalAdApprovalQueueHome;
import com.ejb.ad.LocalAdApprovalUser;
import com.ejb.ad.LocalAdApprovalUserHome;
import com.ejb.ad.LocalAdBranchArTaxCode;
import com.ejb.ad.LocalAdBranchArTaxCodeHome;
import com.ejb.ad.LocalAdBranchCustomer;
import com.ejb.ad.LocalAdBranchCustomerHome;
import com.ejb.ad.LocalAdBranchDocumentSequenceAssignment;
import com.ejb.ad.LocalAdBranchDocumentSequenceAssignmentHome;
import com.ejb.ad.LocalAdBranchItemLocation;
import com.ejb.ad.LocalAdBranchItemLocationHome;
import com.ejb.ad.LocalAdCompany;
import com.ejb.ad.LocalAdCompanyHome;
import com.ejb.ad.LocalAdDeleteAuditTrailHome;
import com.ejb.ad.LocalAdLookUpValue;
import com.ejb.ad.LocalAdLookUpValueHome;
import com.ejb.ad.LocalAdPreference;
import com.ejb.ad.LocalAdPreferenceHome;
import com.ejb.ad.LocalAdUser;
import com.ejb.ad.LocalAdUserHome;
import com.ejb.ar.LocalArCustomer;
import com.ejb.ar.LocalArCustomerBalance;
import com.ejb.ar.LocalArCustomerBalanceHome;
import com.ejb.ar.LocalArCustomerHome;
import com.ejb.ar.LocalArDistributionRecord;
import com.ejb.ar.LocalArDistributionRecordHome;
import com.ejb.ar.LocalArInvoice;
import com.ejb.ar.LocalArInvoiceBatch;
import com.ejb.ar.LocalArInvoiceBatchHome;
import com.ejb.ar.LocalArInvoiceHome;
import com.ejb.ar.LocalArInvoiceLineHome;
import com.ejb.ar.LocalArInvoiceLineItem;
import com.ejb.ar.LocalArInvoiceLineItemHome;
import com.ejb.ar.LocalArInvoicePaymentSchedule;
import com.ejb.ar.LocalArInvoicePaymentScheduleHome;
import com.ejb.ar.LocalArJobOrderInvoiceLine;
import com.ejb.ar.LocalArJobOrderInvoiceLineHome;
import com.ejb.ar.LocalArJobOrderLineHome;
import com.ejb.ar.LocalArSalesOrderInvoiceLine;
import com.ejb.ar.LocalArSalesOrderInvoiceLineHome;
import com.ejb.ar.LocalArSalesOrderLineHome;
import com.ejb.ar.LocalArSalesperson;
import com.ejb.ar.LocalArTaxCode;
import com.ejb.ar.LocalArWithholdingTaxCode;
import com.ejb.cm.LocalCmDistributionRecord;
import com.ejb.exception.AdPRFCoaGlVarianceAccountNotFoundException;
import com.ejb.exception.ArINVOverapplicationNotAllowedException;
import com.ejb.exception.GlJREffectiveDateNoPeriodExistException;
import com.ejb.exception.GlJREffectiveDatePeriodClosedException;
import com.ejb.exception.GlobalAccountNumberInvalidException;
import com.ejb.exception.GlobalBranchAccountNumberInvalidException;
import com.ejb.exception.GlobalDocumentNumberNotUniqueException;
import com.ejb.exception.GlobalInvItemLocationNotFoundException;
import com.ejb.exception.GlobalInventoryDateException;
import com.ejb.exception.GlobalJournalNotBalanceException;
import com.ejb.exception.GlobalNoApprovalApproverFoundException;
import com.ejb.exception.GlobalNoApprovalRequesterFoundException;
import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.exception.GlobalRecordAlreadyDeletedException;
import com.ejb.exception.GlobalTransactionAlreadyApprovedException;
import com.ejb.exception.GlobalTransactionAlreadyLockedException;
import com.ejb.exception.GlobalTransactionAlreadyPendingException;
import com.ejb.exception.GlobalTransactionAlreadyPostedException;
import com.ejb.exception.GlobalTransactionAlreadyVoidException;
import com.ejb.exception.GlobalTransactionAlreadyVoidPostedException;
import com.ejb.gl.LocalAdDocumentSequenceAssignment;
import com.ejb.gl.LocalAdDocumentSequenceAssignmentHome;
import com.ejb.gl.LocalGlAccountingCalendarValue;
import com.ejb.gl.LocalGlAccountingCalendarValueHome;
import com.ejb.gl.LocalGlChartOfAccount;
import com.ejb.gl.LocalGlChartOfAccountBalance;
import com.ejb.gl.LocalGlChartOfAccountBalanceHome;
import com.ejb.gl.LocalGlChartOfAccountHome;
import com.ejb.gl.LocalGlFunctionalCurrency;
import com.ejb.gl.LocalGlFunctionalCurrencyHome;
import com.ejb.gl.LocalGlFunctionalCurrencyRate;
import com.ejb.gl.LocalGlFunctionalCurrencyRateHome;
import com.ejb.gl.LocalGlJournal;
import com.ejb.gl.LocalGlJournalBatch;
import com.ejb.gl.LocalGlJournalBatchHome;
import com.ejb.gl.LocalGlJournalCategory;
import com.ejb.gl.LocalGlJournalCategoryHome;
import com.ejb.gl.LocalGlJournalHome;
import com.ejb.gl.LocalGlJournalLine;
import com.ejb.gl.LocalGlJournalLineHome;
import com.ejb.gl.LocalGlJournalSource;
import com.ejb.gl.LocalGlJournalSourceHome;
import com.ejb.gl.LocalGlSetOfBook;
import com.ejb.gl.LocalGlSetOfBookHome;
import com.ejb.gl.LocalGlSuspenseAccount;
import com.ejb.gl.LocalGlSuspenseAccountHome;
import com.ejb.inv.LocalInvAdjustment;
import com.ejb.inv.LocalInvAdjustmentHome;
import com.ejb.inv.LocalInvAdjustmentLine;
import com.ejb.inv.LocalInvAdjustmentLineHome;
import com.ejb.inv.LocalInvBillOfMaterial;
import com.ejb.inv.LocalInvCosting;
import com.ejb.inv.LocalInvCostingHome;
import com.ejb.inv.LocalInvDistributionRecord;
import com.ejb.inv.LocalInvDistributionRecordHome;
import com.ejb.inv.LocalInvItem;
import com.ejb.inv.LocalInvItemHome;
import com.ejb.inv.LocalInvItemLocation;
import com.ejb.inv.LocalInvItemLocationHome;
import com.ejb.inv.LocalInvLocation;
import com.ejb.inv.LocalInvLocationHome;
import com.ejb.inv.LocalInvPriceLevel;
import com.ejb.inv.LocalInvPriceLevelHome;
import com.ejb.inv.LocalInvUnitOfMeasure;
import com.ejb.inv.LocalInvUnitOfMeasureConversion;
import com.ejb.inv.LocalInvUnitOfMeasureConversionHome;
import com.ejb.inv.LocalInvUnitOfMeasureHome;
import com.util.AbstractSessionBean;
import com.util.ArInvoiceDetails;
import com.util.ArModDistributionRecordDetails;
import com.util.ArModInvoiceDetails;
import com.util.ArModInvoiceLineItemDetails;
import com.util.Debug;
import com.util.EJBCommon;
import com.util.EJBHomeFactory;
import com.util.InvModUnitOfMeasureDetails;

/**
 * @ejb:bean name="ArCreditMemoEntryControllerEJB"
 *           display-name="used for entering credit memo"
 *           type="Stateless"
 *           view-type="remote"
 *           jndi-name="ejb/ArCreditMemoEntryControllerEJB"
 *
 * @ejb:interface remote-class="com.ejb.txn.ArCreditMemoEntryController"
 *                extends="javax.ejb.EJBObject"
 *
 * @ejb:home remote-class="com.ejb.txn.ArCreditMemoEntryControllerHome"
 *           extends="javax.ejb.EJBHome"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="aruser"
 *                        role-link="aruserlink"
 *
 * @ejb:permission role-name="aruser"
 *
*/

public class ArCreditMemoEntryControllerBean extends AbstractSessionBean {



	/**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public Date getArInvoiceDateByInvoiceNumber(String INV_NMBR,Integer INV_AD_BRNCH, Integer INV_AD_CMPNY) {

        Debug.print("ArCreditMemoEntryControllerBean getArInvoiceDateByInvoiceNumber");

        LocalArInvoiceHome arInvoiceHome = null;
        // Initialize EJB Home

        try {

        	arInvoiceHome = (LocalArInvoiceHome)EJBHomeFactory.
            lookUpLocalHome(LocalArInvoiceHome.JNDI_NAME, LocalArInvoiceHome.class);

        } catch (NamingException ex) {

            throw new EJBException(ex.getMessage());
        }

        try {

            LocalArInvoice arInvoice = arInvoiceHome.findByInvNumberAndInvCreditMemoAndBrCode(INV_NMBR, (byte)0, INV_AD_BRNCH, INV_AD_CMPNY);

            return arInvoice.getInvDate();

        } catch (Exception ex) {

            Debug.printStackTrace(ex);
            throw new EJBException(ex.getMessage());

        }

    }

    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public double getArInvoiceAmountDueByInvoiceNumber(String INV_NMBR,Integer INV_AD_BRNCH, Integer INV_AD_CMPNY) {

        Debug.print("ArCreditMemoEntryControllerBean getArInvoiceAmountDueByInvoiceNumber");

        LocalArInvoiceHome arInvoiceHome = null;
        // Initialize EJB Home

        try {

        	arInvoiceHome = (LocalArInvoiceHome)EJBHomeFactory.
            lookUpLocalHome(LocalArInvoiceHome.JNDI_NAME, LocalArInvoiceHome.class);

        } catch (NamingException ex) {

            throw new EJBException(ex.getMessage());
        }

        try {

            LocalArInvoice arInvoice = arInvoiceHome.findByInvNumberAndInvCreditMemoAndBrCode(INV_NMBR, (byte)0, INV_AD_BRNCH, INV_AD_CMPNY);

            return arInvoice.getInvAmountDue() - arInvoice.getInvAmountPaid();

        } catch (Exception ex) {

            Debug.printStackTrace(ex);
            throw new EJBException(ex.getMessage());

        }

    }

    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList getArCstAll(Integer AD_BRNCH, Integer AD_CMPNY) {

        Debug.print("ArCreditMemoEntryControllerBean getArCstAll");

        LocalArCustomerHome arCustomerHome = null;

        ArrayList list = new ArrayList();

        // Initialize EJB Home

        try {

            arCustomerHome = (LocalArCustomerHome)EJBHomeFactory.
                lookUpLocalHome(LocalArCustomerHome.JNDI_NAME, LocalArCustomerHome.class);

        } catch (NamingException ex) {

            throw new EJBException(ex.getMessage());

        }

        try {

        	Collection arCustomers = arCustomerHome.findEnabledCstAll(AD_BRNCH, AD_CMPNY);

        	Iterator i = arCustomers.iterator();

        	while (i.hasNext()) {

        		LocalArCustomer arCustomer = (LocalArCustomer)i.next();

        		list.add(arCustomer.getCstCustomerCode());

        	}

        	return list;

        } catch (Exception ex) {

        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());

        }

    }


    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList getInvLocAll(Integer AD_CMPNY) {

        Debug.print("ArCreditMemoEntryControllerBean getInvLocAll");

        LocalInvLocationHome invLocationHome = null;
        Collection invLocations = null;
        ArrayList list = new ArrayList();

        // Initialize EJB Home

        try {

        	invLocationHome = (LocalInvLocationHome)EJBHomeFactory.
                lookUpLocalHome(LocalInvLocationHome.JNDI_NAME, LocalInvLocationHome.class);

        } catch (NamingException ex) {

            throw new EJBException(ex.getMessage());

        }

        try {

            invLocations = invLocationHome.findLocAll(AD_CMPNY);

	        if (invLocations.isEmpty()) {

	        	return null;

	        }

	        Iterator i = invLocations.iterator();

	        while (i.hasNext()) {

	        	LocalInvLocation invLocation = (LocalInvLocation)i.next();
	        	String details = invLocation.getLocName();

	        	list.add(details);

	        }

	        return list;

    	} catch (Exception ex) {

    		Debug.printStackTrace(ex);
    		throw new EJBException(ex.getMessage());

    	}

    }

    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList getInvUomByIiName(String II_NM, Integer AD_CMPNY) {

    	Debug.print("ArCreditMemoEntryControllerBean getInvUomByIiName");

        LocalInvUnitOfMeasureHome invUnitOfMeasureHome = null;
        LocalInvItemHome invItemHome = null;
        ArrayList list = new ArrayList();

        // Initialize EJB Home

        try {

        	invUnitOfMeasureHome = (LocalInvUnitOfMeasureHome)EJBHomeFactory.
                lookUpLocalHome(LocalInvUnitOfMeasureHome.JNDI_NAME, LocalInvUnitOfMeasureHome.class);
        	invItemHome = (LocalInvItemHome)EJBHomeFactory.
            	lookUpLocalHome(LocalInvItemHome.JNDI_NAME, LocalInvItemHome.class);

        } catch (NamingException ex) {

            throw new EJBException(ex.getMessage());
        }

        try {

        	LocalInvItem invItem = null;
        	LocalInvUnitOfMeasure invItemUnitOfMeasure = null;

        	invItem = invItemHome.findByIiName(II_NM, AD_CMPNY);
        	invItemUnitOfMeasure = invItem.getInvUnitOfMeasure();

        	Collection invUnitOfMeasures = null;
        	Iterator i = invUnitOfMeasureHome.findByUomAdLvClass(
        			invItemUnitOfMeasure.getUomAdLvClass(), AD_CMPNY).iterator();
        	while (i.hasNext()) {

        		LocalInvUnitOfMeasure invUnitOfMeasure = (LocalInvUnitOfMeasure) i.next();
        		InvModUnitOfMeasureDetails details = new InvModUnitOfMeasureDetails();
        		details.setUomName(invUnitOfMeasure.getUomName());

        		if (invUnitOfMeasure.getUomName().equals(invItemUnitOfMeasure.getUomName())) {

        			details.setDefault(true);

        		}

        		list.add(details);

        	}

        	return list;

        } catch (Exception ex) {

        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());

        }

    }

    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
     public double getIiSalesPriceByInvCstCustomerCodeAndIiNameAndUomName(String CST_CSTMR_CODE, String II_NM, String UOM_NM, Integer AD_CMPNY) {

        Debug.print("ArCreditMemoEntryControllerBean getInvIiSalesPriceByIiNameAndUomName");

        LocalInvItemHome invItemHome = null;
        LocalInvPriceLevelHome invPriceLevelHome = null;
        LocalArCustomerHome arCustomerHome = null;
        LocalInvUnitOfMeasureConversionHome invUnitOfMeasureConversionHome = null;

        // Initialize EJB Home

        try {

        	invItemHome = (LocalInvItemHome)EJBHomeFactory.
               lookUpLocalHome(LocalInvItemHome.JNDI_NAME, LocalInvItemHome.class);
        	invPriceLevelHome = (LocalInvPriceLevelHome)EJBHomeFactory.
    			lookUpLocalHome(LocalInvPriceLevelHome.JNDI_NAME, LocalInvPriceLevelHome.class);
        	arCustomerHome = (LocalArCustomerHome)EJBHomeFactory.
				lookUpLocalHome(LocalArCustomerHome.JNDI_NAME, LocalArCustomerHome.class);
        	invUnitOfMeasureConversionHome = (LocalInvUnitOfMeasureConversionHome)EJBHomeFactory.
    			lookUpLocalHome(LocalInvUnitOfMeasureConversionHome.JNDI_NAME, LocalInvUnitOfMeasureConversionHome.class);

        } catch (NamingException ex) {

           throw new EJBException(ex.getMessage());

        }

        try {

        	LocalArCustomer arCustomer = null;

        	try {

        		arCustomer = arCustomerHome.findByCstCustomerCode(CST_CSTMR_CODE, AD_CMPNY);

        	} catch (FinderException ex) {

        		return 0d;

        	}

        	double unitPrice = 0d;

        	LocalInvItem invItem = invItemHome.findByIiName(II_NM, AD_CMPNY);

        	LocalInvPriceLevel invPriceLevel = null;

        	try {

        		invPriceLevel = invPriceLevelHome.findByIiNameAndAdLvPriceLevel(II_NM, arCustomer.getCstDealPrice(), AD_CMPNY);

        		if (invPriceLevel.getPlAmount() == 0){

            		unitPrice = invItem.getIiSalesPrice();

            	} else {

            		unitPrice = invPriceLevel.getPlAmount();

            	}

        	} catch (FinderException ex) {

        		unitPrice = invItem.getIiSalesPrice();

        	}

        	LocalInvUnitOfMeasureConversion invUnitOfMeasureConversion = invUnitOfMeasureConversionHome.findUmcByIiNameAndUomName(II_NM, UOM_NM, AD_CMPNY);
        	LocalInvUnitOfMeasureConversion invDefaultUomConversion = invUnitOfMeasureConversionHome.findUmcByIiNameAndUomName(II_NM, invItem.getInvUnitOfMeasure().getUomName(), AD_CMPNY);

        	return EJBCommon.roundIt(unitPrice * invDefaultUomConversion.getUmcConversionFactor() / invUnitOfMeasureConversion.getUmcConversionFactor(), this.getGlFcPrecisionUnit(AD_CMPNY));

        } catch (Exception ex) {

           Debug.printStackTrace(ex);
           throw new EJBException(ex.getMessage());

        }

     }

     /**
      * @ejb:interface-method view-type="remote"
      * @jboss:method-attributes read-only="true"
      **/
      public short getInvGpQuantityPrecisionUnit(Integer AD_CMPNY) {

         Debug.print("ArCreditMemoEntryControllerBean getInvGpQuantityPrecisionUnit");

         LocalAdPreferenceHome adPreferenceHome = null;

          // Initialize EJB Home

          try {

          	adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
                 lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);

          } catch (NamingException ex) {

             throw new EJBException(ex.getMessage());

          }


          try {

             LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);

             return adPreference.getPrfInvQuantityPrecisionUnit();

          } catch (Exception ex) {

             Debug.printStackTrace(ex);
             throw new EJBException(ex.getMessage());

          }

      }

   /**
    * @ejb:interface-method view-type="remote"
    * @jboss:method-attributes read-only="true"
    **/
    public short getAdPrfArInvoiceLineNumber(Integer AD_CMPNY) {

       Debug.print("ArCreditMemoEntryControllerBean getAdPrfArInvoiceLineNumber");

       LocalAdPreferenceHome adPreferenceHome = null;


       // Initialize EJB Home

       try {

          adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
              lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);

       } catch (NamingException ex) {

          throw new EJBException(ex.getMessage());

       }


       try {

          LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);

          return adPreference.getPrfArInvoiceLineNumber();

       } catch (Exception ex) {

          Debug.printStackTrace(ex);
          throw new EJBException(ex.getMessage());

       }

    }

    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArModInvoiceDetails getArInvByInvCode(Integer INV_CODE, Integer AD_BRNCH, Integer AD_CMPNY)
        throws GlobalNoRecordFoundException {

    	Debug.print("ArCreditMemoEntryControllerBean getArInvByInvCode");

        LocalArInvoiceHome arInvoiceHome = null;
        LocalArDistributionRecordHome arDistributionRecordHome = null;

        // Initialize EJB Home

        try {

            arInvoiceHome = (LocalArInvoiceHome)EJBHomeFactory.
                lookUpLocalHome(LocalArInvoiceHome.JNDI_NAME, LocalArInvoiceHome.class);
            arDistributionRecordHome = (LocalArDistributionRecordHome)EJBHomeFactory.
				lookUpLocalHome(LocalArDistributionRecordHome.JNDI_NAME, LocalArDistributionRecord.class);

        } catch (NamingException ex) {

            throw new EJBException(ex.getMessage());

        }

        try {

        	LocalArInvoice arCreditMemo = null;
        	LocalArInvoice arInvoiceLink = null;
        	LocalArTaxCode arTaxCode = null;
        	LocalArWithholdingTaxCode arWTaxCode = null;


        	try {

        		arCreditMemo = arInvoiceHome.findByPrimaryKey(INV_CODE);
        		arInvoiceLink = arInvoiceHome.findByInvNumberAndInvCreditMemoAndBrCode(arCreditMemo.getInvCmInvoiceNumber(), EJBCommon.FALSE, AD_BRNCH, AD_CMPNY);
        		arTaxCode = arCreditMemo.getArTaxCode();
        		if(arTaxCode==null) {
        			
        			arTaxCode = arInvoiceLink.getArTaxCode();
        		}
        		
        		
        		arWTaxCode = arCreditMemo.getArWithholdingTaxCode();
        		
        		if(arWTaxCode ==null) {
        			arWTaxCode = arInvoiceLink.getArWithholdingTaxCode();
        		}
        		
        	} catch (FinderException ex) {

        		throw new GlobalNoRecordFoundException();

        	}

        	ArrayList list = new ArrayList();

        	// get invoice line items if any

        	Collection arInvoiceLineItems = arCreditMemo.getArInvoiceLineItems();

        	double TOTAL_DEBIT = 0d;
        	double TOTAL_CREDIT = 0d;

        	if (!arInvoiceLineItems.isEmpty()) {

        		Iterator i = arInvoiceLineItems.iterator();

        		while (i.hasNext()) {

        			LocalArInvoiceLineItem arInvoiceLineItem = (LocalArInvoiceLineItem) i.next();

        			ArModInvoiceLineItemDetails iliDetails = new ArModInvoiceLineItemDetails();

        			iliDetails.setIliCode(arInvoiceLineItem.getIliCode());
        			iliDetails.setIliLine(arInvoiceLineItem.getIliLine());
        			iliDetails.setIliQuantity(arInvoiceLineItem.getIliQuantity());
        			iliDetails.setIliUnitPrice(arInvoiceLineItem.getIliUnitPrice());
        			iliDetails.setIliIiName(arInvoiceLineItem.getInvItemLocation().getInvItem().getIiName());
        			iliDetails.setIliLocName(arInvoiceLineItem.getInvItemLocation().getInvLocation().getLocName());
        			iliDetails.setIliUomName(arInvoiceLineItem.getInvUnitOfMeasure().getUomName());
        			iliDetails.setIliIiDescription(arInvoiceLineItem.getInvItemLocation().getInvItem().getIiDescription());
        			iliDetails.setIliEnableAutoBuild(arInvoiceLineItem.getIliEnableAutoBuild());
        			iliDetails.setIliTax(arInvoiceLineItem.getIliTax());
        			
  
        			iliDetails.setIliMisc(arInvoiceLineItem.getIliMisc());
        			list.add(iliDetails);

        		}

       		} else {

	        	// get distribution records

       			System.out.println("cm code is: " + arCreditMemo.getInvCode());
       			Collection arDistributionRecords = arDistributionRecordHome.findByInvCode(arCreditMemo.getInvCode(), AD_CMPNY);
	        	short lineNumber = 1;
	        	
	        	

	        	Iterator i = arDistributionRecords.iterator();

	        	TOTAL_DEBIT = 0d;
	        	TOTAL_CREDIT = 0d;

	        	while (i.hasNext()) {

	        		LocalArDistributionRecord arDistributionRecord = (LocalArDistributionRecord)i.next();

	        		ArModDistributionRecordDetails mdetails = new ArModDistributionRecordDetails();

	        		mdetails.setDrCode(arDistributionRecord.getDrCode());
	        		mdetails.setDrLine(lineNumber);
	        		mdetails.setDrClass(arDistributionRecord.getDrClass());
	        		mdetails.setDrDebit(arDistributionRecord.getDrDebit());
	        		mdetails.setDrAmount(arDistributionRecord.getDrAmount());
	        		mdetails.setDrCoaAccountNumber(arDistributionRecord.getGlChartOfAccount().getCoaAccountNumber());
				    mdetails.setDrCoaAccountDescription(arDistributionRecord.getGlChartOfAccount().getCoaAccountDescription());

				    if(arDistributionRecord.getDrDebit() == EJBCommon.TRUE) {

				    	TOTAL_DEBIT += arDistributionRecord.getDrAmount();

				    } else {

				    	TOTAL_CREDIT += arDistributionRecord.getDrAmount();

				    }

	        		list.add(mdetails);

	        		lineNumber++;
	        	}

	        }

        	ArModInvoiceDetails mInvDetails = new ArModInvoiceDetails();

        	System.out.println("inv type is: " + arCreditMemo.getInvType());
        	mInvDetails.setInvType(arCreditMemo.getInvType());
        	mInvDetails.setInvCode(arCreditMemo.getInvCode());
        	mInvDetails.setInvDescription(arCreditMemo.getInvDescription());
        	mInvDetails.setInvDate(arCreditMemo.getInvDate());
        	mInvDetails.setInvNumber(arCreditMemo.getInvNumber());
        	mInvDetails.setInvCmInvoiceNumber(arCreditMemo.getInvCmInvoiceNumber());
        	mInvDetails.setInvCmReferenceNumber(arCreditMemo.getInvCmReferenceNumber());
        	mInvDetails.setInvAmountDue(arCreditMemo.getInvAmountDue());
        	mInvDetails.setInvApprovalStatus(arCreditMemo.getInvApprovalStatus());
        	mInvDetails.setInvVoidApprovalStatus(arCreditMemo.getInvVoidApprovalStatus());
        	mInvDetails.setInvReasonForRejection(arCreditMemo.getInvReasonForRejection());
        	mInvDetails.setInvPosted(arCreditMemo.getInvPosted());
        	mInvDetails.setInvVoidPosted(arCreditMemo.getInvVoidPosted());
        	mInvDetails.setInvVoid(arCreditMemo.getInvVoid());
        	mInvDetails.setInvTotalDebit(TOTAL_DEBIT);
        	mInvDetails.setInvTotalCredit(TOTAL_CREDIT);
        	mInvDetails.setInvCreatedBy(arCreditMemo.getInvCreatedBy());
        	mInvDetails.setInvDateCreated(arCreditMemo.getInvDateCreated());
        	mInvDetails.setInvLastModifiedBy(arCreditMemo.getInvLastModifiedBy());
        	mInvDetails.setInvDateLastModified(arCreditMemo.getInvDateLastModified());
        	mInvDetails.setInvApprovedRejectedBy(arCreditMemo.getInvApprovedRejectedBy());
        	mInvDetails.setInvDateApprovedRejected(arCreditMemo.getInvDateApprovedRejected());
        	mInvDetails.setInvPostedBy(arCreditMemo.getInvPostedBy());
        	mInvDetails.setInvDatePosted(arCreditMemo.getInvDatePosted());
        	mInvDetails.setInvCstCustomerCode(arCreditMemo.getArCustomer().getCstCustomerCode());
        	mInvDetails.setInvIbName(arCreditMemo.getArInvoiceBatch() != null ? arCreditMemo.getArInvoiceBatch().getIbName() : null);
        	mInvDetails.setInvLvShift(arCreditMemo.getInvLvShift());
        	mInvDetails.setInvSubjectToCommission(arCreditMemo.getInvSubjectToCommission());
        	mInvDetails.setInvCstName(arCreditMemo.getArCustomer().getCstName());
        	mInvDetails.setInvTaxCodeName(arTaxCode==null?"":arTaxCode.getTcName());
        	mInvDetails.setInvWTaxCodeName(arWTaxCode==null?"":arWTaxCode.getWtcName());
        	mInvDetails.setReportParameter(arCreditMemo.getReportParameter());
        	mInvDetails.setInvPartialCm(arCreditMemo.getInvPartialCm());
        	if (!arInvoiceLineItems.isEmpty()) {

        		mInvDetails.setInvIliList(list);
        		mInvDetails.setInvAmountDue(arCreditMemo.getInvAmountDue());

        	} else {

        		mInvDetails.setInvDrList(list);

        	}

        	return mInvDetails;

        } catch (GlobalNoRecordFoundException ex) {

        	throw ex;

        } catch (Exception ex) {

        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());

        }

    }

    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
     public byte getAdPrfEnableInvShift(Integer AD_CMPNY) {

        Debug.print("ArCreditMemoEntryControllerBean getAdPrfEnableInvShift");

        LocalAdPreferenceHome adPreferenceHome = null;


        // Initialize EJB Home

        try {

           adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
               lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);

        } catch (NamingException ex) {

           throw new EJBException(ex.getMessage());

        }


        try {

           LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);

           return adPreference.getPrfInvEnableShift();

        } catch (Exception ex) {

           Debug.printStackTrace(ex);
           throw new EJBException(ex.getMessage());

        }

     }

     /**
      * @ejb:interface-method view-type="remote"
      * @jboss:method-attributes read-only="true"
      **/
     public ArrayList getAdLvInvShiftAll(Integer AD_CMPNY) {

         Debug.print("ArCreditMemoEntryControllerBean getAdLvInvShiftAll");

         LocalAdLookUpValueHome adLookUpValueHome = null;

         ArrayList list = new ArrayList();

         // Initialize EJB Home

         try {

             adLookUpValueHome = (LocalAdLookUpValueHome)EJBHomeFactory.
                 lookUpLocalHome(LocalAdLookUpValueHome.JNDI_NAME, LocalAdLookUpValueHome.class);

         } catch (NamingException ex) {

             throw new EJBException(ex.getMessage());

         }

         try {

         	Collection adLookUpValues = adLookUpValueHome.findByLuName("INV SHIFT", AD_CMPNY);

         	Iterator i = adLookUpValues.iterator();

         	while (i.hasNext()) {

         		LocalAdLookUpValue adLookUpValue = (LocalAdLookUpValue)i.next();

         		list.add(adLookUpValue.getLvName());

         	}

         	return list;

         } catch (Exception ex) {

         	Debug.printStackTrace(ex);
         	throw new EJBException(ex.getMessage());

         }

     }

    /**
     * @ejb:interface-method view-type="remote"
     **/
    public Integer saveArInvEntry(com.util.ArInvoiceDetails details, String CST_CSTMR_CODE, String IB_NM, ArrayList drList, boolean isDraft, Integer AD_BRNCH, Integer AD_CMPNY) throws
        GlobalRecordAlreadyDeletedException,
        GlobalNoRecordFoundException,
		GlobalDocumentNumberNotUniqueException,
		GlobalTransactionAlreadyApprovedException,
		GlobalTransactionAlreadyPendingException,
		GlobalTransactionAlreadyPostedException,
		GlobalTransactionAlreadyVoidPostedException,
		ArINVOverapplicationNotAllowedException,
		GlobalTransactionAlreadyLockedException,
		GlobalNoApprovalRequesterFoundException,
		GlobalNoApprovalApproverFoundException,
		GlJREffectiveDateNoPeriodExistException,
		GlJREffectiveDatePeriodClosedException,
		GlobalJournalNotBalanceException,
		GlobalAccountNumberInvalidException,
		GlobalBranchAccountNumberInvalidException {

        Debug.print("ArCreditMemoEntryControllerBean saveArInvEntry");

        LocalArInvoiceHome arInvoiceHome = null;
        LocalArInvoiceBatchHome arInvoiceBatchHome = null;
        LocalAdCompanyHome adCompanyHome = null;
        LocalAdDocumentSequenceAssignmentHome adDocumentSequenceAssignmentHome = null;
        LocalAdBranchDocumentSequenceAssignmentHome adBranchDocumentSequenceAssignmentHome = null;
        LocalArCustomerHome arCustomerHome = null;
        LocalArInvoicePaymentScheduleHome arInvoicePaymentScheduleHome = null;
        LocalAdApprovalHome adApprovalHome = null;
        LocalAdAmountLimitHome adAmountLimitHome = null;
        LocalAdApprovalUserHome adApprovalUserHome = null;
        LocalAdApprovalQueueHome adApprovalQueueHome = null;
        LocalAdPreferenceHome adPreferenceHome = null;
        LocalGlChartOfAccountHome glChartOfAccountHome = null;
        LocalArInvoice arCreditMemo = null;
        LocalArInvoice arInvoice = null;
       	LocalAdUserHome adUserHome = null;
        
        ArApprovalControllerHome homeAA = null;
        ArApprovalController ejbAA = null;
       


        // Initialize EJB Home

        try {

            arInvoiceHome = (LocalArInvoiceHome)EJBHomeFactory.
                lookUpLocalHome(LocalArInvoiceHome.JNDI_NAME, LocalArInvoiceHome.class);
            arInvoiceBatchHome = (LocalArInvoiceBatchHome)EJBHomeFactory.
				lookUpLocalHome(LocalArInvoiceBatchHome.JNDI_NAME, LocalArInvoiceBatchHome.class);
            adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
            adDocumentSequenceAssignmentHome = (LocalAdDocumentSequenceAssignmentHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdDocumentSequenceAssignmentHome.JNDI_NAME, LocalAdDocumentSequenceAssignmentHome.class);
            adBranchDocumentSequenceAssignmentHome = (LocalAdBranchDocumentSequenceAssignmentHome)EJBHomeFactory.
            	lookUpLocalHome(LocalAdBranchDocumentSequenceAssignmentHome.JNDI_NAME, LocalAdBranchDocumentSequenceAssignmentHome.class);
            arCustomerHome = (LocalArCustomerHome)EJBHomeFactory.
                lookUpLocalHome(LocalArCustomerHome.JNDI_NAME, LocalArCustomerHome.class);
            arInvoicePaymentScheduleHome = (LocalArInvoicePaymentScheduleHome)EJBHomeFactory.
                lookUpLocalHome(LocalArInvoicePaymentScheduleHome.JNDI_NAME, LocalArInvoicePaymentScheduleHome.class);
            adApprovalHome = (LocalAdApprovalHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdApprovalHome.JNDI_NAME, LocalAdApprovalHome.class);
            adAmountLimitHome = (LocalAdAmountLimitHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdAmountLimitHome.JNDI_NAME, LocalAdAmountLimitHome.class);
            adApprovalUserHome = (LocalAdApprovalUserHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdApprovalUserHome.JNDI_NAME, LocalAdApprovalUserHome.class);
            adApprovalQueueHome = (LocalAdApprovalQueueHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdApprovalQueueHome.JNDI_NAME, LocalAdApprovalQueueHome.class);
            adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);
            glChartOfAccountHome = (LocalGlChartOfAccountHome)EJBHomeFactory.
            	lookUpLocalHome(LocalGlChartOfAccountHome.JNDI_NAME, LocalGlChartOfAccountHome.class);
           	adUserHome = (LocalAdUserHome)EJBHomeFactory.
            	lookUpLocalHome(LocalAdUserHome.JNDI_NAME, LocalAdUserHome.class);
            	
            ejbAA = (ArApprovalController)com.util.EJBHomeFactory.
                lookUpHome("ArApprovalControllerEJB", ArApprovalController.class);

        } catch (NamingException ex) {

            throw new EJBException(ex.getMessage());

        }
        
        
      try {
        
        	ejbAA = homeAA.create();
		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (CreateException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

        try {

        	// validate if credit memo is already deleted

        	try {

        		if (details.getInvCode() != null) {

        			arCreditMemo = arInvoiceHome.findByPrimaryKey(details.getInvCode());

        		}

        	} catch (FinderException ex) {

        		throw new GlobalRecordAlreadyDeletedException();

        	}

        	// validate if credit memo is already posted, void, arproved or pending

        	if (details.getInvCode() != null && details.getInvVoid()  == EJBCommon.FALSE ) {

	        	if (arCreditMemo.getInvApprovalStatus() != null) {

	        		if (arCreditMemo.getInvApprovalStatus().equals("APPROVED") ||
	        		    arCreditMemo.getInvApprovalStatus().equals("N/A")) {

	        		    throw new GlobalTransactionAlreadyApprovedException();

	        		} else if (arCreditMemo.getInvApprovalStatus().equals("PENDING")) {

	        			throw new GlobalTransactionAlreadyPendingException();

	        		}

	        	}

        		if (arCreditMemo.getInvPosted() == EJBCommon.TRUE) {

        			throw new GlobalTransactionAlreadyPostedException();

        		} else if (arCreditMemo.getInvVoid() == EJBCommon.TRUE) {

        			throw new GlobalTransactionAlreadyVoidException();

        		}

        	}

        	// invoice void

	    	if (details.getInvCode() != null && details.getInvVoid() == EJBCommon.TRUE ) {

			
	    		if (arCreditMemo.getInvVoid() == EJBCommon.TRUE) {

        			throw new GlobalTransactionAlreadyVoidException();

        		}

        		if (arCreditMemo.getInvPosted() == EJBCommon.TRUE) {

        			// generate approval status
					  String INV_APPRVL_STATUS = null;
                  

                	if (!isDraft) {

                		LocalAdApproval adApproval = adApprovalHome.findByAprAdCompany(AD_CMPNY);

                		// check if ar credit memo approval is enabled

                		if (adApproval.getAprEnableArCreditMemo() == EJBCommon.FALSE) {

                			INV_APPRVL_STATUS = "N/A";

                		} else {

                			// check if credit memo is self approved

                			LocalAdUser adUser2 = adUserHome.findByUsrName(details.getInvLastModifiedBy(), AD_CMPNY);


							//	INV_APPRVL_STATUS = ejbIA.getApprovalStatus(adUser2.getUsrDept(), invAdjustment.getAdjLastModifiedBy(), adUser2.getUsrDescription(), "INV ADJUSTMENT", invAdjustment.getAdjCode(), invAdjustment.getAdjDocumentNumber(), invAdjustment.getAdjDate(), AD_BRNCH, AD_CMPNY);
								INV_APPRVL_STATUS = ejbAA.getApprovalStatus(adUser2.getUsrDept(), arCreditMemo.getInvLastModifiedBy(), adUser2.getUsrDescription(), "AR CREDIT MEMO", arCreditMemo.getInvCode(), arCreditMemo.getInvNumber(), arCreditMemo.getInvDate(), AD_BRNCH, AD_CMPNY);

            			}
            		}
                	


                	// reverse distribution records

		    		Collection arDistributionRecords = arCreditMemo.getArDistributionRecords();
		    		ArrayList list = new ArrayList();

		    		Iterator i = arDistributionRecords.iterator();

		    		while (i.hasNext()) {

		    			LocalArDistributionRecord arDistributionRecord = (LocalArDistributionRecord)i.next();

		    			list.add(arDistributionRecord);

		    		}

		    		i = list.iterator();

		    		while (i.hasNext()) {

		    			LocalArDistributionRecord arDistributionRecord = (LocalArDistributionRecord)i.next();
		    			System.out.println("Check Point A");
		    			this.addArDrEntry(arCreditMemo.getArDrNextLine(), arDistributionRecord.getDrClass(),
		    					arDistributionRecord.getDrDebit() == EJBCommon.TRUE ? EJBCommon.FALSE : EJBCommon.TRUE,
		    							arDistributionRecord.getDrAmount(), arDistributionRecord.getGlChartOfAccount().getCoaCode(),
		    							arCreditMemo,  AD_BRNCH, AD_CMPNY);

		    		}

                	LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);

                	if (INV_APPRVL_STATUS != null && INV_APPRVL_STATUS.equals("N/A") && adPreference.getPrfArGlPostingType().equals("AUTO-POST UPON APPROVAL")) {

                		arCreditMemo.setInvVoid(EJBCommon.TRUE);
                		this.executeArInvCreditMemoPost(arCreditMemo.getInvCode(), arCreditMemo.getInvLastModifiedBy(), AD_BRNCH, AD_CMPNY);

                	}

                	// set credit memo void approval status

                	arCreditMemo.setInvVoidApprovalStatus(INV_APPRVL_STATUS);




        		} else {

        			System.out.println("5--------------------------");
        			LocalArInvoice arLockedInvoice = arInvoiceHome.findByInvNumberAndInvCreditMemoAndBrCode(arCreditMemo.getInvCmInvoiceNumber(),
	        		    EJBCommon.FALSE, AD_BRNCH, AD_CMPNY);

	        		Collection arLockedInvoicePaymentSchedules = arLockedInvoice.getArInvoicePaymentSchedules();

	        		Iterator ipsIter = arLockedInvoicePaymentSchedules.iterator();

	        		while (ipsIter.hasNext()) {

	        			LocalArInvoicePaymentSchedule arInvoicePaymentSchedule =
	        			    (LocalArInvoicePaymentSchedule)ipsIter.next();

	        			arInvoicePaymentSchedule.setIpsLock(EJBCommon.FALSE);

	        		}


	    		}

	    		arCreditMemo.setInvVoid(EJBCommon.TRUE);
	    		arCreditMemo.setInvLastModifiedBy(details.getInvLastModifiedBy());
	    		arCreditMemo.setInvDateLastModified(details.getInvDateLastModified());
	    		System.out.println("6--------------------------");
        		return arCreditMemo.getInvCode();

	    	}

	    	// validate if invoice number exists

        	try {

        		arInvoice = arInvoiceHome.findByInvNumberAndInvCreditMemoAndBrCode(details.getInvCmInvoiceNumber(), EJBCommon.FALSE, AD_BRNCH, AD_CMPNY);

        		if (arInvoice.getInvPosted() != EJBCommon.TRUE) {

        			throw new GlobalNoRecordFoundException();

        		}

        	} catch (FinderException ex) {

        		throw new GlobalNoRecordFoundException();

        	}

        	// validate if document number is unique document number is automatic then set next sequence

        	LocalAdBranchDocumentSequenceAssignment adBranchDocumentSequenceAssignment = null;
 			LocalAdDocumentSequenceAssignment adDocumentSequenceAssignment = null;

        	if (details.getInvCode() == null) {

        		try {

 					adDocumentSequenceAssignment = adDocumentSequenceAssignmentHome.findByDcName("AR CREDIT MEMO", AD_CMPNY);

 				} catch (FinderException ex) {

 				}

 				try {

 					adBranchDocumentSequenceAssignment = adBranchDocumentSequenceAssignmentHome.findBdsByDsaCodeAndBrCode(adDocumentSequenceAssignment.getDsaCode(), AD_BRNCH, AD_CMPNY);

 				} catch (FinderException ex) {

 				}

	    		LocalArInvoice arExistingCreditMemo = null;

	    		try {

	    		    arExistingCreditMemo = arInvoiceHome.findByInvNumberAndInvCreditMemoAndBrCode(
	        		    details.getInvNumber(), EJBCommon.TRUE, AD_BRNCH, AD_CMPNY);

	        	} catch (FinderException ex) {
	        	}

	            if (arExistingCreditMemo != null) {

	            	throw new GlobalDocumentNumberNotUniqueException();

	            }

	            if (adDocumentSequenceAssignment.getAdDocumentSequence().getDsNumberingType() == 'A' &&
		            (details.getInvNumber() == null || details.getInvNumber().trim().length() == 0)) {

		            while (true) {

		            	if (adBranchDocumentSequenceAssignment == null || adBranchDocumentSequenceAssignment.getBdsNextSequence() == null) {

		            		try {

			            		arInvoiceHome.findByInvNumberAndInvCreditMemoAndBrCode(adDocumentSequenceAssignment.getDsaNextSequence(), EJBCommon.TRUE, AD_BRNCH, AD_CMPNY);
			            		adDocumentSequenceAssignment.setDsaNextSequence(EJBCommon.incrementStringNumber(adDocumentSequenceAssignment.getDsaNextSequence()));

			            	} catch (FinderException ex) {

			            		details.setInvNumber(adDocumentSequenceAssignment.getDsaNextSequence());
			            		adDocumentSequenceAssignment.setDsaNextSequence(EJBCommon.incrementStringNumber(adDocumentSequenceAssignment.getDsaNextSequence()));
					            break;

			            	}

		            	} else {

		            		try {

			            		arInvoiceHome.findByInvNumberAndInvCreditMemoAndBrCode(adBranchDocumentSequenceAssignment.getBdsNextSequence(), EJBCommon.TRUE, AD_BRNCH, AD_CMPNY);
			            		adBranchDocumentSequenceAssignment.setBdsNextSequence(EJBCommon.incrementStringNumber(adBranchDocumentSequenceAssignment.getBdsNextSequence()));

			            	} catch (FinderException ex) {

			            		details.setInvNumber(adBranchDocumentSequenceAssignment.getBdsNextSequence());
			            		adBranchDocumentSequenceAssignment.setBdsNextSequence(EJBCommon.incrementStringNumber(adBranchDocumentSequenceAssignment.getBdsNextSequence()));
					            break;

			            	}

		            	}

		            }

		        }

		    } else {

		    	LocalArInvoice arExistingCreditMemo = null;

		    	try {

	    		    arExistingCreditMemo = arInvoiceHome.findByInvNumberAndInvCreditMemoAndBrCode(
	        		    details.getInvNumber(), EJBCommon.TRUE, AD_BRNCH, AD_CMPNY);

	        	} catch (FinderException ex) {
	        	}

	        	if (arExistingCreditMemo != null &&
	                !arExistingCreditMemo.getInvCode().equals(details.getInvCode())) {

	            	throw new GlobalDocumentNumberNotUniqueException();

	            }

	            if (arCreditMemo.getInvNumber() != details.getInvNumber() &&
	                (details.getInvNumber() == null || details.getInvNumber().trim().length() == 0)) {

	                details.setInvNumber(arCreditMemo.getInvNumber());

	         	}

		    }

		    // validate if invoice entered is already locked by cm or receipt

        	Collection apValidateInvoicePaymentSchedules =
        	        arInvoicePaymentScheduleHome.findByIpsLockAndInvCode(EJBCommon.TRUE, arInvoice.getInvCode(), AD_CMPNY);

            if (details.getInvCode() == null && !apValidateInvoicePaymentSchedules.isEmpty() ||
                details.getInvCode() != null && !details.getInvCmInvoiceNumber().equals(arCreditMemo.getInvCmInvoiceNumber()) &&
                !apValidateInvoicePaymentSchedules.isEmpty()) {

                throw new GlobalTransactionAlreadyLockedException();

            }


        	// create invoice

        	if (details.getInvCode() == null) {

				arCreditMemo = arInvoiceHome.create(details.getInvType(), EJBCommon.TRUE,
					    details.getInvDescription(), details.getInvDate(),
					    details.getInvNumber(), details.getInvReferenceNumber(),details.getInvUploadNumber(), details.getInvCmInvoiceNumber(),details.getInvCmReferenceNumber(),
					    details.getInvAmountDue(),0d,0d,0d,0d, 0d, details.getInvConversionDate(), details.getInvConversionRate(), details.getInvMemo(),
					    0d, 0d, null, null, null, null, null, null, null, null, null, null, null, null, null, null,
					    null, null, null, null, null, null,
					    EJBCommon.FALSE,
					    null, EJBCommon.FALSE, EJBCommon.FALSE,
					    EJBCommon.FALSE,  EJBCommon.FALSE,
					    EJBCommon.FALSE, null, 0d, null, null, null, null,
					    details.getInvCreatedBy(),
						details.getInvDateCreated(), details.getInvLastModifiedBy(), details.getInvDateLastModified(),
	        	    	null, null, null, null, EJBCommon.FALSE, null, null, null, EJBCommon.FALSE, details.getInvSubjectToCommission(),
	        	    	details.getInvClientPO(), details.getInvEffectivityDate(), AD_BRNCH, AD_CMPNY);

				LocalArInvoice arCMInvoice = arInvoiceHome.findByInvNumberAndInvCreditMemoAndBrCode(arCreditMemo.getInvCmInvoiceNumber(),
	        		    EJBCommon.FALSE, AD_BRNCH, AD_CMPNY);

				arCreditMemo.setArSalesperson(arCMInvoice.getArSalesperson());

        	} else {

        		// release lock

	    	    LocalArInvoice arLockedInvoice = arInvoiceHome.findByInvNumberAndInvCreditMemoAndBrCode(arCreditMemo.getInvCmInvoiceNumber(),
        		    EJBCommon.FALSE, AD_BRNCH, AD_CMPNY);

        		Collection arLockedInvoicePaymentSchedules = arLockedInvoice.getArInvoicePaymentSchedules();

        		Iterator ipsIter = arLockedInvoicePaymentSchedules.iterator();

        		while (ipsIter.hasNext()) {

        			LocalArInvoicePaymentSchedule arInvoicePaymentSchedule =
        			    (LocalArInvoicePaymentSchedule)ipsIter.next();

        			arInvoicePaymentSchedule.setIpsLock(EJBCommon.FALSE);

        		}

        		arCreditMemo.setInvType(details.getInvType());
        		arCreditMemo.setInvDescription(details.getInvDescription());
        		arCreditMemo.setInvDate(details.getInvDate());
        		arCreditMemo.setInvNumber(details.getInvNumber());
        		arCreditMemo.setInvAmountDue(details.getInvAmountDue());
        		arCreditMemo.setInvCmInvoiceNumber(details.getInvCmInvoiceNumber());
        		arCreditMemo.setInvCmReferenceNumber(details.getInvCmReferenceNumber());
        		arCreditMemo.setInvLastModifiedBy(details.getInvLastModifiedBy());
        		arCreditMemo.setInvDateLastModified(details.getInvDateLastModified());
        		arCreditMemo.setInvReasonForRejection(null);
        		arCreditMemo.setInvSubjectToCommission(details.getInvSubjectToCommission());
        		arCreditMemo.setArSalesperson(arLockedInvoice.getArSalesperson());

        	}

        	arCreditMemo.setReportParameter(details.getReportParameter());

        	LocalArCustomer arCustomer = arCustomerHome.findByCstCustomerCode(CST_CSTMR_CODE, AD_CMPNY);
        	System.out.println("CUSTOMER_CODE="+arCustomer.getCstCustomerCode());
        	arCustomer.addArInvoice(arCreditMemo);

        	LocalArTaxCode arTaxCode = arInvoice.getArTaxCode();

        	LocalArWithholdingTaxCode arWithholdingTaxCode = arInvoice.getArWithholdingTaxCode();

        	try {

        		LocalArInvoiceBatch arInvoiceBatch = arInvoiceBatchHome.findByIbName(IB_NM, AD_BRNCH, AD_CMPNY);
        		arInvoiceBatch.addArInvoice(arCreditMemo);

        	} catch (FinderException ex) {

        	}

        	// remove all credit memo lines

        	Collection arInvoiceLineItems = arCreditMemo.getArInvoiceLineItems();

        	Iterator i = arInvoiceLineItems.iterator();

        	while (i.hasNext()) {

        		LocalArInvoiceLineItem arInvoiceLineItem = (LocalArInvoiceLineItem)i.next();

        		i.remove();

        		arInvoiceLineItem.remove();

        	}

        	// remove all distribution records

	  	    Collection arDistributionRecords = arCreditMemo.getArDistributionRecords();

	  	    i = arDistributionRecords.iterator();

	  	    while (i.hasNext()) {

	  	   	    LocalArDistributionRecord arDistributionRecord = (LocalArDistributionRecord)i.next();

	  	  	    i.remove();

	  	  	    arDistributionRecord.remove();

	  	    }



           // add new distribution records

	  	   i = drList.iterator();

	  	   while (i.hasNext()) {

		  	   	ArModDistributionRecordDetails mDrDetails = (ArModDistributionRecordDetails) i.next();

		  	   	if (mDrDetails.getDrClass().equals("RECEIVABLE")) {

		  	   		if (EJBCommon.roundIt(arCreditMemo.getInvAmountDue(), this.getGlFcPrecisionUnit(AD_CMPNY)) > EJBCommon.roundIt(arInvoice.getInvAmountDue() - arInvoice.getInvAmountPaid(), this.getGlFcPrecisionUnit(AD_CMPNY))) {
		  	   			System.out.println("arCreditMemo.getInvAmountDue()="+arCreditMemo.getInvAmountDue());
		  	   			System.out.println("arInvoice.getInvAmountDue()"+EJBCommon.roundIt(arInvoice.getInvAmountDue() - arInvoice.getInvAmountPaid(), this.getGlFcPrecisionUnit(AD_CMPNY)));

		  	   			throw new ArINVOverapplicationNotAllowedException();

		  	   		}

		  	   	}

		  	   	LocalGlChartOfAccount glChartOfAccount = null;

		  	   	try {

		  	   		glChartOfAccount = glChartOfAccountHome.findByCoaAccountNumberAndBranchCode(mDrDetails.getDrCoaAccountNumber(), AD_BRNCH, AD_CMPNY);

		  	   	} catch (FinderException ex) {

		  	   		throw new GlobalAccountNumberInvalidException(String.valueOf(mDrDetails.getDrLine()));
		  	   	}

		  	   	this.addArDrEntry(mDrDetails.getDrLine(), mDrDetails.getDrClass(), mDrDetails.getDrDebit(), mDrDetails.getDrAmount(), glChartOfAccount.getCoaCode(), arCreditMemo, AD_BRNCH, AD_CMPNY);

	  	   }

      	    // create new invoice payment schedule lock

      	    Collection arInvoicePaymentSchedules =
      	       arInvoice.getArInvoicePaymentSchedules();

      	    i = arInvoicePaymentSchedules.iterator();

      	    while (i.hasNext()) {

      	   	    LocalArInvoicePaymentSchedule arInvoicePaymentSchedule =
        		    (LocalArInvoicePaymentSchedule)i.next();

        	    arInvoicePaymentSchedule.setIpsLock(EJBCommon.TRUE);

      	    }



      	    // generate approval status

            String INV_APPRVL_STATUS = null;

        	if (!isDraft) {

        		LocalAdApproval adApproval = adApprovalHome.findByAprAdCompany(AD_CMPNY);

        		// check if ar credit memo approval is enabled

        		if (adApproval.getAprEnableArCreditMemo() == EJBCommon.FALSE) {

        			INV_APPRVL_STATUS = "N/A";

        		} else {

        			// check if credit memo is self approved

    				LocalAdUser adUser2 = adUserHome.findByUsrName(details.getInvLastModifiedBy(), AD_CMPNY);


				//	INV_APPRVL_STATUS = ejbIA.getApprovalStatus(adUser2.getUsrDept(), invAdjustment.getAdjLastModifiedBy(), adUser2.getUsrDescription(), "INV ADJUSTMENT", invAdjustment.getAdjCode(), invAdjustment.getAdjDocumentNumber(), invAdjustment.getAdjDate(), AD_BRNCH, AD_CMPNY);
					INV_APPRVL_STATUS = ejbAA.getApprovalStatus(adUser2.getUsrDept(), arCreditMemo.getInvLastModifiedBy(), adUser2.getUsrDescription(), "AR CREDIT MEMO", arCreditMemo.getInvCode(), arCreditMemo.getInvNumber(), arCreditMemo.getInvDate(), AD_BRNCH, AD_CMPNY);

        		}
        	}

        	LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);

        	if (INV_APPRVL_STATUS != null && INV_APPRVL_STATUS.equals("N/A") && adPreference.getPrfArGlPostingType().equals("AUTO-POST UPON APPROVAL")) {

        		this.executeArInvCreditMemoPost(arCreditMemo.getInvCode(), arCreditMemo.getInvLastModifiedBy(), AD_BRNCH, AD_CMPNY);

        	}

        	// set credit memo approval status

        	arCreditMemo.setInvApprovalStatus(INV_APPRVL_STATUS);

      	    return arCreditMemo.getInvCode();


        } catch (GlobalRecordAlreadyDeletedException ex) {

        	getSessionContext().setRollbackOnly();
        	throw ex;

        } catch (GlobalNoRecordFoundException ex) {

        	getSessionContext().setRollbackOnly();
        	throw ex;

        } catch (GlobalDocumentNumberNotUniqueException ex) {

        	getSessionContext().setRollbackOnly();
        	throw ex;

        } catch (GlobalTransactionAlreadyApprovedException ex) {

        	getSessionContext().setRollbackOnly();
        	throw ex;

        } catch (GlobalTransactionAlreadyPendingException ex) {

        	getSessionContext().setRollbackOnly();
        	throw ex;

        } catch (GlobalTransactionAlreadyPostedException ex) {

        	getSessionContext().setRollbackOnly();
        	throw ex;

        } catch (GlobalTransactionAlreadyVoidPostedException ex) {

        	getSessionContext().setRollbackOnly();
        	throw ex;

        } catch (ArINVOverapplicationNotAllowedException ex) {

        	getSessionContext().setRollbackOnly();
        	throw ex;

        } catch (GlobalTransactionAlreadyLockedException ex) {

        	getSessionContext().setRollbackOnly();
        	throw ex;

         } catch (GlobalNoApprovalRequesterFoundException ex) {

        	getSessionContext().setRollbackOnly();
        	throw ex;

        } catch (GlobalNoApprovalApproverFoundException ex) {

        	getSessionContext().setRollbackOnly();
        	throw ex;

        } catch (GlJREffectiveDateNoPeriodExistException ex) {

        	getSessionContext().setRollbackOnly();
        	throw ex;

        } catch (GlJREffectiveDatePeriodClosedException ex) {

        	getSessionContext().setRollbackOnly();
        	throw ex;

        } catch (GlobalJournalNotBalanceException ex) {

        	getSessionContext().setRollbackOnly();
        	throw ex;

        } catch (GlobalAccountNumberInvalidException ex) {

        	getSessionContext().setRollbackOnly();
        	throw ex;

        } catch (GlobalBranchAccountNumberInvalidException ex) {

        	getSessionContext().setRollbackOnly();
        	throw ex;

        } catch (Exception ex) {

        	Debug.printStackTrace(ex);
        	getSessionContext().setRollbackOnly();
        	throw new EJBException(ex.getMessage());

        }


    }

   /**
     * @ejb:interface-method view-type="remote"
     **/
    public Integer saveArInvIliEntry(com.util.ArInvoiceDetails details,
        String CST_CSTMR_CODE, String IB_NM, ArrayList iliList, boolean isDraft, Integer AD_BRNCH, Integer AD_CMPNY) throws
        GlobalRecordAlreadyDeletedException,
        GlobalNoRecordFoundException,
		GlobalDocumentNumberNotUniqueException,
		GlobalTransactionAlreadyApprovedException,
		GlobalTransactionAlreadyPendingException,
		GlobalTransactionAlreadyPostedException,
		GlobalTransactionAlreadyVoidPostedException,
		ArINVOverapplicationNotAllowedException,
		GlobalTransactionAlreadyLockedException,
		GlobalNoApprovalRequesterFoundException,
		GlobalNoApprovalApproverFoundException,
		GlobalInvItemLocationNotFoundException,
		GlJREffectiveDateNoPeriodExistException,
		GlJREffectiveDatePeriodClosedException,
		GlobalJournalNotBalanceException,
		GlobalInventoryDateException,
		GlobalBranchAccountNumberInvalidException,
		AdPRFCoaGlVarianceAccountNotFoundException {

        Debug.print("ArCreditMemoEntryControllerBean saveArInvIliEntry");

        LocalArInvoiceHome arInvoiceHome = null;
        LocalArInvoiceBatchHome arInvoiceBatchHome = null;
        LocalAdCompanyHome adCompanyHome = null;
        LocalAdDocumentSequenceAssignmentHome adDocumentSequenceAssignmentHome = null;
        LocalAdBranchDocumentSequenceAssignmentHome adBranchDocumentSequenceAssignmentHome = null;
        LocalArCustomerHome arCustomerHome = null;
        LocalArInvoicePaymentScheduleHome arInvoicePaymentScheduleHome = null;
        LocalAdApprovalHome adApprovalHome = null;
        LocalAdAmountLimitHome adAmountLimitHome = null;
        LocalAdApprovalUserHome adApprovalUserHome = null;
        LocalAdApprovalQueueHome adApprovalQueueHome = null;
        LocalAdPreferenceHome adPreferenceHome = null;
        LocalInvItemLocationHome invItemLocationHome = null;
        LocalInvCostingHome invCostingHome = null;
        LocalInvItemHome invItemHome = null;
        LocalGlChartOfAccountHome glChartOfAccountHome = null;
        LocalAdBranchItemLocationHome adBranchItemLocationHome = null;
        LocalAdBranchCustomerHome adBranchCustomerHome = null;
        LocalAdBranchArTaxCodeHome adBranchArTaxCodeHome = null;
        
        LocalAdUserHome adUserHome = null;
       
       	ArApprovalControllerHome homeAA = null;
       	ArApprovalController ejbAA = null;

        LocalArInvoice arCreditMemo = null;
        LocalArInvoice arInvoice = null;

        // Initialize EJB Home

        try {
        	adBranchArTaxCodeHome = (LocalAdBranchArTaxCodeHome)EJBHomeFactory.
                    lookUpLocalHome(LocalAdBranchArTaxCodeHome.JNDI_NAME, LocalAdBranchArTaxCodeHome.class);

            arInvoiceHome = (LocalArInvoiceHome)EJBHomeFactory.
                lookUpLocalHome(LocalArInvoiceHome.JNDI_NAME, LocalArInvoiceHome.class);
            arInvoiceBatchHome = (LocalArInvoiceBatchHome)EJBHomeFactory.
				lookUpLocalHome(LocalArInvoiceBatchHome.JNDI_NAME, LocalArInvoiceBatchHome.class);
            adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
            adDocumentSequenceAssignmentHome = (LocalAdDocumentSequenceAssignmentHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdDocumentSequenceAssignmentHome.JNDI_NAME, LocalAdDocumentSequenceAssignmentHome.class);
            adBranchDocumentSequenceAssignmentHome = (LocalAdBranchDocumentSequenceAssignmentHome)EJBHomeFactory.
            	lookUpLocalHome(LocalAdBranchDocumentSequenceAssignmentHome.JNDI_NAME, LocalAdBranchDocumentSequenceAssignmentHome.class);
            arCustomerHome = (LocalArCustomerHome)EJBHomeFactory.
                lookUpLocalHome(LocalArCustomerHome.JNDI_NAME, LocalArCustomerHome.class);
            arInvoicePaymentScheduleHome = (LocalArInvoicePaymentScheduleHome)EJBHomeFactory.
                lookUpLocalHome(LocalArInvoicePaymentScheduleHome.JNDI_NAME, LocalArInvoicePaymentScheduleHome.class);
            adApprovalHome = (LocalAdApprovalHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdApprovalHome.JNDI_NAME, LocalAdApprovalHome.class);
            adAmountLimitHome = (LocalAdAmountLimitHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdAmountLimitHome.JNDI_NAME, LocalAdAmountLimitHome.class);
            adApprovalUserHome = (LocalAdApprovalUserHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdApprovalUserHome.JNDI_NAME, LocalAdApprovalUserHome.class);
            adApprovalQueueHome = (LocalAdApprovalQueueHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdApprovalQueueHome.JNDI_NAME, LocalAdApprovalQueueHome.class);
            adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);
            invItemLocationHome = (LocalInvItemLocationHome)EJBHomeFactory.
                lookUpLocalHome(LocalInvItemLocationHome.JNDI_NAME, LocalInvItemLocationHome.class);
            invCostingHome = (LocalInvCostingHome)EJBHomeFactory.
                lookUpLocalHome(LocalInvCostingHome.JNDI_NAME, LocalInvCostingHome.class);
            invItemHome = (LocalInvItemHome)EJBHomeFactory.
                lookUpLocalHome(LocalInvItemHome.JNDI_NAME, LocalInvItemHome.class);
            glChartOfAccountHome = (LocalGlChartOfAccountHome)EJBHomeFactory.
                lookUpLocalHome(LocalGlChartOfAccountHome.JNDI_NAME, LocalGlChartOfAccountHome.class);
            adBranchItemLocationHome = (LocalAdBranchItemLocationHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdBranchItemLocationHome.JNDI_NAME, LocalAdBranchItemLocationHome.class);
            adBranchCustomerHome = (LocalAdBranchCustomerHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdBranchCustomerHome.JNDI_NAME, LocalAdBranchCustomerHome.class);
			adUserHome = (LocalAdUserHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdUserHome.JNDI_NAME, LocalAdUserHome.class);

			homeAA = (ArApprovalControllerHome)com.util.EJBHomeFactory.
                lookUpHome("ejb/ArApprovalControllerEJB", ArApprovalControllerHome.class);
                
        } catch (NamingException ex) {

            throw new EJBException(ex.getMessage());

        }
        
         try {
 
        	ejbAA = homeAA.create();
		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (CreateException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

        try {

        	// validate if credit memo is already deleted

        	try {

        		if (details.getInvCode() != null) {

        			arCreditMemo = arInvoiceHome.findByPrimaryKey(details.getInvCode());

        		}

        	} catch (FinderException ex) {

        		throw new GlobalRecordAlreadyDeletedException();

        	}

        	// validate if credit memo is already posted, void, arproved or pending

        	if (details.getInvCode() != null && details.getInvVoid()  == EJBCommon.FALSE ) {

	        	if (arCreditMemo.getInvApprovalStatus() != null) {

	        		if (arCreditMemo.getInvApprovalStatus().equals("APPROVED") ||
	        		    arCreditMemo.getInvApprovalStatus().equals("N/A")) {

	        		    throw new GlobalTransactionAlreadyApprovedException();

	        		} else if (arCreditMemo.getInvApprovalStatus().equals("PENDING")) {

	        			throw new GlobalTransactionAlreadyPendingException();

	        		}

	        	}

        		if (arCreditMemo.getInvPosted() == EJBCommon.TRUE) {

        			throw new GlobalTransactionAlreadyPostedException();

        		} else if (arCreditMemo.getInvVoid() == EJBCommon.TRUE) {

        			throw new GlobalTransactionAlreadyVoidException();

        		}

        	}

        	// invoice void
	        System.out.println("VOID CREDIT MEMO ");
	    	if (details.getInvCode() != null && details.getInvVoid() == EJBCommon.TRUE ) {

	    		return this.executeVoid(details, isDraft, AD_BRNCH, AD_CMPNY, arInvoiceHome, adApprovalHome,
              adPreferenceHome, adUserHome, ejbAA, arCreditMemo);

	    	}

	    	// validate if invoice number exists

        	try {

        		arInvoice = arInvoiceHome.findByInvNumberAndInvCreditMemoAndBrCode(details.getInvCmInvoiceNumber(), EJBCommon.FALSE, AD_BRNCH, AD_CMPNY);

        		if (arInvoice.getInvPosted() != EJBCommon.TRUE) {

        			throw new GlobalNoRecordFoundException();

        		}

        	} catch (FinderException ex) {

        		throw new GlobalNoRecordFoundException();

        	}

        	// validate if document number is unique document number is automatic then set next sequence

        	LocalAdBranchDocumentSequenceAssignment adBranchDocumentSequenceAssignment = null;
 			LocalAdDocumentSequenceAssignment adDocumentSequenceAssignment = null;

        	if (details.getInvCode() == null) {

        		try {

 					adDocumentSequenceAssignment = adDocumentSequenceAssignmentHome.findByDcName("AR CREDIT MEMO", AD_CMPNY);

 				} catch (FinderException ex) {

 				}

 				try {

 					adBranchDocumentSequenceAssignment = adBranchDocumentSequenceAssignmentHome.findBdsByDsaCodeAndBrCode(adDocumentSequenceAssignment.getDsaCode(), AD_BRNCH, AD_CMPNY);

 				} catch (FinderException ex) {

 				}

	    		LocalArInvoice arExistingCreditMemo = null;

	    		try {

	    		    arExistingCreditMemo = arInvoiceHome.findByInvNumberAndInvCreditMemoAndBrCode(
	        		    details.getInvNumber(), EJBCommon.TRUE, AD_BRNCH, AD_CMPNY);

	        	} catch (FinderException ex) {
	        	}

	            if (arExistingCreditMemo != null) {

	            	throw new GlobalDocumentNumberNotUniqueException();

	            }

	            if (adDocumentSequenceAssignment.getAdDocumentSequence().getDsNumberingType() == 'A' &&
		            (details.getInvNumber() == null || details.getInvNumber().trim().length() == 0)) {

		            while (true) {

		            	if (adBranchDocumentSequenceAssignment == null || adBranchDocumentSequenceAssignment.getBdsNextSequence() == null) {

		            		try {

			            		arInvoiceHome.findByInvNumberAndInvCreditMemoAndBrCode(adDocumentSequenceAssignment.getDsaNextSequence(), EJBCommon.TRUE, AD_BRNCH, AD_CMPNY);
			            		adDocumentSequenceAssignment.setDsaNextSequence(EJBCommon.incrementStringNumber(adDocumentSequenceAssignment.getDsaNextSequence()));

			            	} catch (FinderException ex) {

			            		details.setInvNumber(adDocumentSequenceAssignment.getDsaNextSequence());
			            		adDocumentSequenceAssignment.setDsaNextSequence(EJBCommon.incrementStringNumber(adDocumentSequenceAssignment.getDsaNextSequence()));
					            break;

			            	}

		            	} else {

		            		try {

			            		arInvoiceHome.findByInvNumberAndInvCreditMemoAndBrCode(adBranchDocumentSequenceAssignment.getBdsNextSequence(), EJBCommon.TRUE, AD_BRNCH, AD_CMPNY);
			            		adBranchDocumentSequenceAssignment.setBdsNextSequence(EJBCommon.incrementStringNumber(adBranchDocumentSequenceAssignment.getBdsNextSequence()));

			            	} catch (FinderException ex) {

			            		details.setInvNumber(adBranchDocumentSequenceAssignment.getBdsNextSequence());
			            		adBranchDocumentSequenceAssignment.setBdsNextSequence(EJBCommon.incrementStringNumber(adBranchDocumentSequenceAssignment.getBdsNextSequence()));
					            break;

			            	}

		            	}

		            }

		        }

		    } else {

		    	LocalArInvoice arExistingCreditMemo = null;

		    	try {

	    		    arExistingCreditMemo = arInvoiceHome.findByInvNumberAndInvCreditMemoAndBrCode(
	        		    details.getInvNumber(), EJBCommon.TRUE, AD_BRNCH, AD_CMPNY);

	        	} catch (FinderException ex) {
	        	}

	        	if (arExistingCreditMemo != null &&
	                !arExistingCreditMemo.getInvCode().equals(details.getInvCode())) {

	            	throw new GlobalDocumentNumberNotUniqueException();

	            }

	            if (arCreditMemo.getInvNumber() != details.getInvNumber() &&
	                (details.getInvNumber() == null || details.getInvNumber().trim().length() == 0)) {

	                details.setInvNumber(arCreditMemo.getInvNumber());

	         	}

		    }

		    // validate if invoice entered is already locked by cm or receipt

        	Collection apValidateInvoicePaymentSchedules =
        	        arInvoicePaymentScheduleHome.findByIpsLockAndInvCode(EJBCommon.TRUE, arInvoice.getInvCode(), AD_CMPNY);

            if (details.getInvCode() == null && !apValidateInvoicePaymentSchedules.isEmpty() ||
                details.getInvCode() != null && !details.getInvCmInvoiceNumber().equals(arCreditMemo.getInvCmInvoiceNumber()) &&
                !apValidateInvoicePaymentSchedules.isEmpty()) {

                throw new GlobalTransactionAlreadyLockedException();

            }

            // used in checking if credit memo should re-generate distribution records and re-calculate taxes

	        boolean isRecalculate = true;

        	// create invoice

        	if (details.getInvCode() == null) {

				arCreditMemo = arInvoiceHome.create(details.getInvType(),EJBCommon.TRUE,
					    details.getInvDescription(), details.getInvDate(),
					    details.getInvNumber(), details.getInvReferenceNumber(),details.getInvUploadNumber(), details.getInvCmInvoiceNumber(),details.getInvCmReferenceNumber(),
					    0d,0d, 0d,0d, 0d,0d, details.getInvConversionDate(), details.getInvConversionRate(), details.getInvMemo(),
					    0d, 0d, null, null, null, null, null, null, null, null, null, null, null, null, null, null,
					    null, null, null, null, null, null,
					    EJBCommon.FALSE,
					    null, EJBCommon.FALSE, EJBCommon.FALSE,
					    EJBCommon.FALSE,  EJBCommon.FALSE,
					    EJBCommon.FALSE, null, 0d, null, null, null, null,
					    details.getInvCreatedBy(),
						details.getInvDateCreated(), details.getInvLastModifiedBy(), details.getInvDateLastModified(),
	        	    	null, null, null, null, EJBCommon.FALSE, details.getInvLvShift(), null, null, EJBCommon.FALSE, details.getInvSubjectToCommission(),
	        	    	details.getInvClientPO(), details.getInvEffectivityDate(), AD_BRNCH, AD_CMPNY);

        	} else {

        		// release lock

	    	    LocalArInvoice arLockedInvoice = arInvoiceHome.findByInvNumberAndInvCreditMemoAndBrCode(arCreditMemo.getInvCmInvoiceNumber(),
        		    EJBCommon.FALSE, AD_BRNCH, AD_CMPNY);

        		Collection arLockedInvoicePaymentSchedules = arLockedInvoice.getArInvoicePaymentSchedules();

        		Iterator ipsIter = arLockedInvoicePaymentSchedules.iterator();

        		while (ipsIter.hasNext()) {

        			LocalArInvoicePaymentSchedule arInvoicePaymentSchedule =
        			    (LocalArInvoicePaymentSchedule)ipsIter.next();

        			arInvoicePaymentSchedule.setIpsLock(EJBCommon.FALSE);

        		}

        		// check if critical fields are changed

        		if (!arCreditMemo.getArCustomer().getCstCustomerCode().equals(CST_CSTMR_CODE) ||
        		    !arCreditMemo.getInvCmInvoiceNumber().equals(details.getInvCmInvoiceNumber()) ||
					iliList.size() != arCreditMemo.getArInvoiceLineItems().size() ||
					!(arCreditMemo.getInvDate().equals(details.getInvDate()))) {

        			isRecalculate = true;

        		} else if (iliList.size() == arCreditMemo.getArInvoiceLineItems().size()) {

        			Iterator ilIter = arCreditMemo.getArInvoiceLineItems().iterator();
        			Iterator ilListIter = iliList.iterator();

        			while (ilIter.hasNext()) {

        				LocalArInvoiceLineItem arInvoiceLineItem = (LocalArInvoiceLineItem)ilIter.next();
        				ArModInvoiceLineItemDetails mdetails = (ArModInvoiceLineItemDetails)ilListIter.next();

        				if (!arInvoiceLineItem.getInvItemLocation().getInvItem().getIiName().equals(mdetails.getIliIiName()) ||
        				    !arInvoiceLineItem.getInvItemLocation().getInvLocation().getLocName().equals(mdetails.getIliLocName()) ||
        				    !arInvoiceLineItem.getInvUnitOfMeasure().getUomName().equals(mdetails.getIliUomName()) ||
							arInvoiceLineItem.getIliQuantity() != mdetails.getIliQuantity() ||
							arInvoiceLineItem.getIliUnitPrice() != mdetails.getIliUnitPrice() ||
							arInvoiceLineItem.getIliEnableAutoBuild() != mdetails.getIliEnableAutoBuild()) {

        					isRecalculate = true;
        					break;

        				}

        			//	isRecalculate = false;

        			}

        		} else {

        		//	isRecalculate = false;

        		}

        		arCreditMemo.setInvType(details.getInvType());
        		arCreditMemo.setInvDescription(details.getInvDescription());

        		arCreditMemo.setInvDate(details.getInvDate());
        		arCreditMemo.setInvNumber(details.getInvNumber());
        		arCreditMemo.setInvCmInvoiceNumber(details.getInvCmInvoiceNumber());
        		arCreditMemo.setInvCmReferenceNumber(details.getInvCmReferenceNumber());
        		arCreditMemo.setInvLastModifiedBy(details.getInvLastModifiedBy());
        		arCreditMemo.setInvDateLastModified(details.getInvDateLastModified());
        		arCreditMemo.setInvReasonForRejection(null);
        		arCreditMemo.setInvLvShift(details.getInvLvShift());
        		arCreditMemo.setInvSubjectToCommission(details.getInvSubjectToCommission());

        	}
        	
        	arCreditMemo.setInvPartialCm(details.getInvPartialCm());

        	arCreditMemo.setReportParameter(details.getReportParameter());

        	LocalArCustomer arCustomer = arCustomerHome.findByCstCustomerCode(CST_CSTMR_CODE, AD_CMPNY);
        	arCustomer.addArInvoice(arCreditMemo);

        	LocalArTaxCode arTaxCode = arInvoice.getArTaxCode();

        	arCreditMemo.setArTaxCode(arTaxCode);
        	
        	
        	LocalArWithholdingTaxCode arWithholdingTaxCode = arInvoice.getArWithholdingTaxCode();
        	
        	arCreditMemo.setArWithholdingTaxCode(arWithholdingTaxCode);

        	try {

        		LocalArInvoiceBatch arInvoiceBatch = arInvoiceBatchHome.findByIbName(IB_NM, AD_BRNCH, AD_CMPNY);
        		arInvoiceBatch.addArInvoice(arCreditMemo);

        	} catch (FinderException ex) {

        	}
        	double TOTAL_LINE = 0d;
        	LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);

        	if (isRecalculate) {

	        	// remove all credit memo lines

	        	Collection arInvoiceLineItems = arCreditMemo.getArInvoiceLineItems();

		  	    Iterator i = arInvoiceLineItems.iterator();

		  	    while (i.hasNext()) {

		  	   	    LocalArInvoiceLineItem arInvoiceLineItem = (LocalArInvoiceLineItem)i.next();

		  	  	    i.remove();

		  	  	    arInvoiceLineItem.remove();

		  	    }

	        	// remove all distribution records

		  	    Collection arDistributionRecords = arCreditMemo.getArDistributionRecords();

		  	    i = arDistributionRecords.iterator();

		  	    while (i.hasNext()) {

		  	   	    LocalArDistributionRecord arDistributionRecord = (LocalArDistributionRecord)i.next();

		  	  	    i.remove();

		  	  	    arDistributionRecord.remove();

		  	    }

		  	    // add new invoice lines and distribution record

		  	    double TOTAL_TAX = 0d;
		  	 
		  	    double TOTAL_SALES_ACCOUNT_CREDIT = 0d;

		  	  System.out.println("size line is: " + iliList.size());
		  	    
	      	    i = iliList.iterator();

	      	    LocalInvItemLocation invItemLocation = null;

	      	    while (i.hasNext()) {

	      	  	    ArModInvoiceLineItemDetails mIliDetails = (ArModInvoiceLineItemDetails) i.next();

	      	  	    try {

	 	  	    		invItemLocation = invItemLocationHome.findByLocNameAndIiName(mIliDetails.getIliLocName(), mIliDetails.getIliIiName(), AD_CMPNY);

	 	  	    	} catch (FinderException ex) {

	 	  	    		throw new GlobalInvItemLocationNotFoundException(String.valueOf(mIliDetails.getIliLine()));

	 	  	    	}

	      	  	    
	      	  	   
	      	  	    
	 	  	        //	start date validation
	 	  	    	if (adPreference.getPrfArAllowPriorDate() == EJBCommon.FALSE) {
		 	  	    	Collection invNegTxnCosting = invCostingHome.findNegTxnByGreaterThanCstDateAndIiNameAndLocName(
			  	    		arCreditMemo.getInvDate(), invItemLocation.getInvItem().getIiName(),
			  	    		invItemLocation.getInvLocation().getLocName(), AD_BRNCH, AD_CMPNY);
			 	  	    if(!invNegTxnCosting.isEmpty()) throw new GlobalInventoryDateException(invItemLocation.getInvItem().getIiName());
	 	  	    	}




	      	  	    LocalArInvoiceLineItem arInvoiceLineItem = this.addArIliEntry(mIliDetails, arCreditMemo, arTaxCode, invItemLocation, arInvoice, AD_BRNCH, AD_CMPNY);

	      	  	    // add cost of sales distribution and inventory

	 	  	    	double COST = 0d;

					try {

						LocalInvCosting invCosting  = invCostingHome.getByMaxCstDateToLongAndMaxCstLineNumberAndLessThanEqualCstDateAndIiNameAndLocName(
						    arCreditMemo.getInvDate(), invItemLocation.getInvItem().getIiName(), invItemLocation.getInvLocation().getLocName(), AD_BRNCH, AD_CMPNY);

							if( invCosting.getCstRemainingQuantity()<= 0){
								COST = Math.abs(invItemLocation.getInvItem().getIiUnitCost());
							}else {
								COST = Math.abs(invCosting.getCstRemainingValue() / invCosting.getCstRemainingQuantity());
							}
						
						

					} catch (FinderException ex) {

						COST = arInvoiceLineItem.getInvItemLocation().getInvItem().getIiUnitCost();

					}

					double QTY_SLD = this.convertByUomFromAndItemAndQuantity(arInvoiceLineItem.getInvUnitOfMeasure(),
	    						arInvoiceLineItem.getInvItemLocation().getInvItem(), arInvoiceLineItem.getIliQuantity(), AD_CMPNY);

					LocalAdBranchItemLocation adBranchItemLocation = null;

					try {

						adBranchItemLocation = adBranchItemLocationHome.findBilByIlCodeAndBrCode(arInvoiceLineItem.getInvItemLocation().getIlCode(), AD_BRNCH, AD_CMPNY);

					} catch(FinderException ex) {

					}

					if (adPreference.getPrfArAutoComputeCogs() == EJBCommon.TRUE && arInvoiceLineItem.getInvItemLocation().getInvItem().getIiNonInventoriable() == EJBCommon.FALSE) {

						if(adBranchItemLocation != null) {

							this.addArDrEntry(arCreditMemo.getArDrNextLine(),
				 	  	    		"COGS", EJBCommon.FALSE, COST * QTY_SLD,
									adBranchItemLocation.getBilCoaGlCostOfSalesAccount(), arCreditMemo, AD_BRNCH, AD_CMPNY);

							this.addArDrEntry(arCreditMemo.getArDrNextLine(),
									"INVENTORY", EJBCommon.TRUE, COST * QTY_SLD,
									adBranchItemLocation.getBilCoaGlInventoryAccount(), arCreditMemo, AD_BRNCH, AD_CMPNY);

						} else {

							this.addArDrEntry(arCreditMemo.getArDrNextLine(),
									"COGS", EJBCommon.FALSE, COST * QTY_SLD,
									arInvoiceLineItem.getInvItemLocation().getIlGlCoaCostOfSalesAccount(), arCreditMemo, AD_BRNCH, AD_CMPNY);

							this.addArDrEntry(arCreditMemo.getArDrNextLine(),
									"INVENTORY", EJBCommon.TRUE, COST * QTY_SLD,
									arInvoiceLineItem.getInvItemLocation().getIlGlCoaInventoryAccount(), arCreditMemo, AD_BRNCH, AD_CMPNY);

						}

					}

	      	  	    // add revenue/credit distributions

					if(adBranchItemLocation != null) {

						if(adBranchItemLocation.getInvItemLocation().getInvItem().getIiServices() == EJBCommon.TRUE) {
							//this will trigger by services(DEBIT)
							this.addArDrEntry(arCreditMemo.getArDrNextLine(),
				      	  	        "REVENUE", EJBCommon.FALSE, arInvoiceLineItem.getIliAmount(),
				      	  	        adBranchItemLocation.getBilCoaGlSalesAccount(), arCreditMemo, AD_BRNCH, AD_CMPNY);

							this.addArDrEntry(arCreditMemo.getArDrNextLine(),
				      	  	        "OTHER", EJBCommon.TRUE, arInvoiceLineItem.getIliAmount(),
				      	  	        adBranchItemLocation.getBilCoaGlSalesReturnAccount(), arCreditMemo, AD_BRNCH, AD_CMPNY);

							TOTAL_SALES_ACCOUNT_CREDIT += arInvoiceLineItem.getIliAmount();

						}else {
							this.addArDrEntry(arCreditMemo.getArDrNextLine(),
				      	  	        "REVENUE", EJBCommon.TRUE, arInvoiceLineItem.getIliAmount(),
				      	  	        adBranchItemLocation.getBilCoaGlSalesAccount(), arCreditMemo, AD_BRNCH, AD_CMPNY);

						}

					} else {

						if( arInvoiceLineItem.getInvItemLocation().getInvItem().getIiServices() == EJBCommon.TRUE) {

							//this will trigger by services(DEBIT)
							this.addArDrEntry(arCreditMemo.getArDrNextLine(),
				      	  	        "REVENUE", EJBCommon.FALSE, arInvoiceLineItem.getIliAmount(),
				      	  	        arInvoiceLineItem.getInvItemLocation().getIlGlCoaSalesAccount(), arCreditMemo, AD_BRNCH, AD_CMPNY);

							TOTAL_SALES_ACCOUNT_CREDIT += arInvoiceLineItem.getIliAmount();
						}else {
							this.addArDrEntry(arCreditMemo.getArDrNextLine(),
				      	  	        "REVENUE", EJBCommon.TRUE, arInvoiceLineItem.getIliAmount(),
				      	  	        arInvoiceLineItem.getInvItemLocation().getIlGlCoaSalesAccount(), arCreditMemo, AD_BRNCH, AD_CMPNY);

						}



					}

	      	  	    TOTAL_LINE += arInvoiceLineItem.getIliAmount();
	      	  	    TOTAL_TAX += arInvoiceLineItem.getIliTaxAmount();

	      	  	    // if auto build is enable

	      	  	    if (arInvoiceLineItem.getIliEnableAutoBuild() == EJBCommon.TRUE && arInvoiceLineItem.getInvItemLocation().getInvItem().getIiClass().equals("Assembly")) {

						byte DEBIT = EJBCommon.TRUE;

						double TOTAL_AMOUNT = 0;
					    double ABS_TOTAL_AMOUNT = 0;

						Collection invBillOfMaterials = arInvoiceLineItem.getInvItemLocation().getInvItem().getInvBillOfMaterials();

						Iterator j = invBillOfMaterials.iterator();

						while (j.hasNext()) {

							LocalInvBillOfMaterial invBillOfMaterial = (LocalInvBillOfMaterial) j.next();

							// add bill of material quantity needed to item location

                            LocalInvItemLocation invIlRawMaterial = null;

							try {

								invIlRawMaterial = invItemLocationHome.findByLocNameAndIiName(
									invBillOfMaterial.getBomLocName(), invBillOfMaterial.getBomIiName(), AD_CMPNY);

							} catch(FinderException ex) {

								throw new GlobalInvItemLocationNotFoundException(String.valueOf(mIliDetails.getIliLine()) +
									" - Raw Mat. (" +invBillOfMaterial.getBomIiName() + ")");

							}

							// bom conversion
                            double convertedQuantity = this.convertByUomFromAndItemAndQuantity(
                                    invBillOfMaterial.getInvUnitOfMeasure(), invIlRawMaterial.getInvItem(),
                                    EJBCommon.roundIt(invBillOfMaterial.getBomQuantityNeeded() * QTY_SLD,
                                    this.getInvGpQuantityPrecisionUnit(AD_CMPNY)), AD_CMPNY);


							// start date validation
                            if (adPreference.getPrfArAllowPriorDate() == EJBCommon.FALSE) {
					    		Collection invIlRawMatNegTxnCosting = invCostingHome.findNegTxnByGreaterThanCstDateAndIiNameAndLocName(
					    			arCreditMemo.getInvDate(), invIlRawMaterial.getInvItem().getIiName(),
					    			invIlRawMaterial.getInvLocation().getLocName(),	AD_BRNCH, AD_CMPNY);
					    		if(!invIlRawMatNegTxnCosting.isEmpty()) throw new GlobalInventoryDateException(
			      	    			invItemLocation.getInvItem().getIiName() + " - Raw Mat. (" + invIlRawMaterial.getInvItem().getIiName() + ")");
                            }

				    		LocalInvItem invItem = invItemHome.findByIiName(invBillOfMaterial.getBomIiName(), AD_CMPNY);

							double COSTING = 0d;

		 	  	    		try {

		 	  	    			LocalInvCosting invCosting  = invCostingHome.getByMaxCstDateToLongAndMaxCstLineNumberAndLessThanEqualCstDateAndIiNameAndLocName(
								    arCreditMemo.getInvDate(), invItem.getIiName(), invBillOfMaterial.getBomLocName(), AD_BRNCH, AD_CMPNY);

			 	  	    		COSTING = Math.abs(invCosting.getCstRemainingValue() / invCosting.getCstRemainingQuantity());

			 	  	        } catch (FinderException ex) {

			 	  	            COSTING = invItem.getIiUnitCost();

			 	  	        }

			 	  	        // bom conversion
                            COSTING = this.convertCostByUom(invIlRawMaterial.getInvItem().getIiName(),
                                    invBillOfMaterial.getInvUnitOfMeasure().getUomName(), COSTING, true, AD_CMPNY);

	                        double BOM_AMOUNT = EJBCommon.roundIt(
									(QTY_SLD * (invBillOfMaterial.getBomQuantityNeeded() * COSTING)),
									this.getGlFcPrecisionUnit(AD_CMPNY));

							TOTAL_AMOUNT += BOM_AMOUNT;
							ABS_TOTAL_AMOUNT += Math.abs(BOM_AMOUNT);

							// add inventory account

							try {

								LocalAdBranchItemLocation adBilRawMaterial = adBranchItemLocationHome.findBilByIlCodeAndBrCode(invIlRawMaterial.getIlCode(), AD_BRNCH, AD_CMPNY);

								this.addArDrIliEntry(arCreditMemo.getArDrNextLine(), "INVENTORY", EJBCommon.TRUE, Math.abs(BOM_AMOUNT),
										adBilRawMaterial.getBilCoaGlInventoryAccount(), arCreditMemo, AD_BRNCH, AD_CMPNY);

							} catch(FinderException ex) {

								this.addArDrIliEntry(arCreditMemo.getArDrNextLine(), "INVENTORY", EJBCommon.TRUE, Math.abs(BOM_AMOUNT),
										invIlRawMaterial.getIlGlCoaInventoryAccount(), arCreditMemo, AD_BRNCH, AD_CMPNY);

							}

						}

						// add cost of sales account

						if(adBranchItemLocation != null) {

							this.addArDrIliEntry(arCreditMemo.getArDrNextLine(),
									"COGS", EJBCommon.FALSE, Math.abs(TOTAL_AMOUNT),
									adBranchItemLocation.getBilCoaGlCostOfSalesAccount(), arCreditMemo, AD_BRNCH, AD_CMPNY);

						} else {

							this.addArDrIliEntry(arCreditMemo.getArDrNextLine(),
			 	  	    			"COGS", EJBCommon.FALSE, Math.abs(TOTAL_AMOUNT),
									 arInvoiceLineItem.getInvItemLocation().getIlGlCoaCostOfSalesAccount(), arCreditMemo, AD_BRNCH, AD_CMPNY);

						}

					}

	      	    }




	      	    // add tax distribution if necessary

	      	    if (!arTaxCode.getTcType().equals("NONE") &&
	        	    !arTaxCode.getTcType().equals("EXEMPT")) {

	      	    	if (arTaxCode.getTcInterimAccount() == null) {
			   //   	if (arTaxCode.getTcInterimAccount() == null) {

			      		// add branch tax code
          	      	  LocalAdBranchArTaxCode adBranchTaxCode = null;
                          Debug.print("ArInvoiceEntryControllerBean saveArInvIliEntry J");
                          try {
                        	  adBranchTaxCode = adBranchArTaxCodeHome.findBtcByTcCodeAndBrCode(arInvoice.getArTaxCode().getTcCode(), AD_BRNCH, AD_CMPNY);

                          } catch(FinderException ex) {

                          }

                      System.out.println("tax to journal: "+ TOTAL_TAX);
                          
		  	    	    this.addArDrEntry(arCreditMemo.getArDrNextLine(),
			      	        "TAX", EJBCommon.TRUE, TOTAL_TAX,  adBranchTaxCode.getBtcGlCoaTaxCode(),
			      	        arCreditMemo, AD_BRNCH, AD_CMPNY);

		  	    	} else {

		  	    		this.addArDrEntry(arCreditMemo.getArDrNextLine(),
			      	        "DEFERRED TAX", EJBCommon.TRUE, TOTAL_TAX, arTaxCode.getTcInterimAccount(),
			      	        arCreditMemo, AD_BRNCH, AD_CMPNY);

		  	    	}

		        }

	      	// add un earned interest
                double UNEARNED_INT_AMOUNT = 0d;
                double TOTAL_DOWN_PAYMENT = arInvoice.getInvDownPayment();

                if(arInvoice.getArCustomer().getCstAutoComputeInterest() == EJBCommon.TRUE
                		&& arInvoice.getArCustomer().getCstMonthlyInterestRate() > 0 && arInvoice.getAdPaymentTerm().getPytEnableInterest() == EJBCommon.TRUE && arInvoice.getInvAmountUnearnedInterest() > 0){

                	try{

                		LocalAdBranchCustomer adBranchCustomer = adBranchCustomerHome.findBcstByCstCodeAndBrCode(arInvoice.getArCustomer().getCstCode(), AD_BRNCH, AD_CMPNY);

                        System.out.println("adPaymentTerm.getAdPaymentSchedules().size()="+arInvoice.getAdPaymentTerm().getAdPaymentSchedules().size());

                        UNEARNED_INT_AMOUNT = EJBCommon.roundIt((TOTAL_LINE + TOTAL_TAX - TOTAL_DOWN_PAYMENT) * arInvoice.getAdPaymentTerm().getAdPaymentSchedules().size() * (arInvoice.getArCustomer().getCstMonthlyInterestRate()/ 100) , this.getGlFcPrecisionUnit(AD_CMPNY));

                        this.addArDrIliEntry(arCreditMemo.getArDrNextLine(), "UNINTEREST",
                                EJBCommon.TRUE, UNEARNED_INT_AMOUNT , adBranchCustomer.getBcstGlCoaUnEarnedInterestAccount(),
                                arCreditMemo, AD_BRNCH, AD_CMPNY);

                        this.addArDrIliEntry(arInvoice.getArDrNextLine(), "RECEIVABLE INTEREST",
                                EJBCommon.FALSE, UNEARNED_INT_AMOUNT,
                                adBranchCustomer.getBcstGlCoaReceivableAccount(),
                                arCreditMemo, AD_BRNCH, AD_CMPNY);

                	}catch (FinderException ex){

                	}


                }

	      	    // add wtax distribution if necessary

	      	    double W_TAX_AMOUNT = 0d;

	      	    if (arWithholdingTaxCode.getWtcRate() != 0 && adPreference.getPrfArWTaxRealization().equals("INVOICE")) {

	      	    	W_TAX_AMOUNT = EJBCommon.roundIt(TOTAL_LINE * (arWithholdingTaxCode.getWtcRate() / 100), this.getGlFcPrecisionUnit(AD_CMPNY));

	      	    	this.addArDrEntry(arCreditMemo.getArDrNextLine(), "W-TAX",
	      	    	    EJBCommon.FALSE, W_TAX_AMOUNT, arWithholdingTaxCode.getGlChartOfAccount().getCoaCode(),
	      	    	    arCreditMemo, AD_BRNCH, AD_CMPNY);

	      	    }


	      	    // add receivable distribution

	      	    try {

	      	    	LocalAdBranchCustomer adBranchCustomer = adBranchCustomerHome.findBcstByCstCodeAndBrCode(arCreditMemo.getArCustomer().getCstCode(), AD_BRNCH, AD_CMPNY);

	      	    	System.out.println("TOTAL_LINE="+TOTAL_LINE);
	      	    	System.out.println("TOTAL_TAX="+TOTAL_TAX);
	      	    	System.out.println("W_TAX_AMOUNT="+W_TAX_AMOUNT);
	      	    	System.out.println("UNEARNED_INT_AMOUNT="+UNEARNED_INT_AMOUNT);

	      	    	//this is for Services (DEBIT)
	      	    	this.addArDrEntry(arCreditMemo.getArDrNextLine(), "RECEIVABLE",
	    	      	        EJBCommon.FALSE, TOTAL_LINE + TOTAL_TAX - W_TAX_AMOUNT - TOTAL_SALES_ACCOUNT_CREDIT,
	    	      	        adBranchCustomer.getBcstGlCoaReceivableAccount(),
	    	      	        arCreditMemo, AD_BRNCH, AD_CMPNY);
	      	    	/*
	      	    	this.addArDrEntry(arCreditMemo.getArDrNextLine(), "RECEIVABLE",
	    	      	        EJBCommon.FALSE, TOTAL_LINE + TOTAL_TAX - W_TAX_AMOUNT,
	    	      	        adBranchCustomer.getBcstGlCoaReceivableAccount(),
	    	      	        arCreditMemo, AD_BRNCH, AD_CMPNY);
					*/
	      	    } catch (FinderException ex) {

	      	    }

	      	    // set invoice amount due

	      	    arCreditMemo.setInvAmountDue(TOTAL_LINE + TOTAL_TAX - W_TAX_AMOUNT  + UNEARNED_INT_AMOUNT );
            
                //TODO: SETTINGS OF RESTRICTION CM
                
	      	    if (EJBCommon.roundIt(arCreditMemo.getInvAmountDue(), this.getGlFcPrecisionUnit(AD_CMPNY)) > EJBCommon.roundIt(arInvoice.getInvAmountDue() - arInvoice.getInvAmountPaid(), this.getGlFcPrecisionUnit(AD_CMPNY)) ) {


	      	    	Double amountPaidAmountDue = EJBCommon.roundIt(arInvoice.getInvAmountDue() - arInvoice.getInvAmountPaid(), this.getGlFcPrecisionUnit(AD_CMPNY));
	      	    	Double amountPaidOnly =EJBCommon.roundIt( arCreditMemo.getInvAmountDue(), this.getGlFcPrecisionUnit(AD_CMPNY));


	      	    	System.out.println("amonut paid:" + arInvoice.getInvAmountPaid());
	      	    	System.out.println("amonut due:" + amountPaidOnly);

	      	    	System.out.println(amountPaidOnly - arInvoice.getInvAmountPaid());
	      	    	System.out.println(EJBCommon.roundIt(arCreditMemo.getInvAmountDue(), this.getGlFcPrecisionUnit(AD_CMPNY)) + " " + amountPaidAmountDue);



	      	    	System.out.println("arCreditMemo.getInvAmountDue()="+EJBCommon.roundIt(arCreditMemo.getInvAmountDue(), this.getGlFcPrecisionUnit(AD_CMPNY)));
	  	   			System.out.println("arInvoice.getInvAmountDue()"+EJBCommon.roundIt(arInvoice.getInvAmountDue() - arInvoice.getInvAmountPaid(), this.getGlFcPrecisionUnit(AD_CMPNY)));

	      	    	throw new ArINVOverapplicationNotAllowedException();

	      	    }
                
                
	      	    // create new invoice payment schedule lock

	      	    Collection arInvoicePaymentSchedules =
	      	       arInvoice.getArInvoicePaymentSchedules();

	      	    i = arInvoicePaymentSchedules.iterator();

	      	    while (i.hasNext()) {

	      	   	    LocalArInvoicePaymentSchedule arInvoicePaymentSchedule =
	        		    (LocalArInvoicePaymentSchedule)i.next();

	        	    arInvoicePaymentSchedule.setIpsLock(EJBCommon.TRUE);

	      	    }

        	} else {

        		Iterator i = iliList.iterator();

	      	    LocalInvItemLocation invItemLocation = null;

	      	    while (i.hasNext()) {

	      	    	ArModInvoiceLineItemDetails mIliDetails = (ArModInvoiceLineItemDetails) i.next();

	      	    	try {

	      	    		invItemLocation = invItemLocationHome.findByLocNameAndIiName(mIliDetails.getIliLocName(), mIliDetails.getIliIiName(), AD_CMPNY);

	      	    	} catch (FinderException ex) {

	      	    		throw new GlobalInvItemLocationNotFoundException(String.valueOf(mIliDetails.getIliLine()));

	      	    	}

	      	    	//	start date validation
	      	    	if (adPreference.getPrfArAllowPriorDate() == EJBCommon.FALSE) {
		      	    	Collection invNegTxnCosting = invCostingHome.findNegTxnByGreaterThanCstDateAndIiNameAndLocName(
		      	    		arCreditMemo.getInvDate(), invItemLocation.getInvItem().getIiName(),
							invItemLocation.getInvLocation().getLocName(), AD_BRNCH, AD_CMPNY);
		      	    	if(!invNegTxnCosting.isEmpty()) throw new GlobalInventoryDateException(invItemLocation.getInvItem().getIiName());

	      	    	}

	      	    	if(mIliDetails.getIliEnableAutoBuild() == EJBCommon.TRUE && invItemLocation.getInvItem().getIiClass().equals("Assembly")) {

	      	    		Collection invBillOfMaterials = invItemLocation.getInvItem().getInvBillOfMaterials();

	      	    		Iterator j = invBillOfMaterials.iterator();

	      	    		while (j.hasNext()) {

	      	    			LocalInvBillOfMaterial invBillOfMaterial = (LocalInvBillOfMaterial) j.next();

	      	    			LocalInvItemLocation invIlRawMaterial = null;

	      	    			try {

	      	    				invIlRawMaterial = invItemLocationHome.findByLocNameAndIiName(
	      	    						invBillOfMaterial.getBomLocName(), invBillOfMaterial.getBomIiName(), AD_CMPNY);

	      	    			} catch(FinderException ex) {

	      	    				throw new GlobalInvItemLocationNotFoundException(String.valueOf(mIliDetails.getIliLine()) +
	      	    						" - Raw Mat. (" +invBillOfMaterial.getBomIiName() + ")");

	      	    			}

	      	    			// start date validation
	      	    			if (adPreference.getPrfArAllowPriorDate() == EJBCommon.FALSE) {
		      	    			Collection invIlRawMatNegTxnCosting = invCostingHome.findNegTxnByGreaterThanCstDateAndIiNameAndLocName(
		      	    					arCreditMemo.getInvDate(), invIlRawMaterial.getInvItem().getIiName(),
										invIlRawMaterial.getInvLocation().getLocName(),	AD_BRNCH, AD_CMPNY);
		      	    			if(!invIlRawMatNegTxnCosting.isEmpty()) throw new GlobalInventoryDateException(
		      	    					invItemLocation.getInvItem().getIiName() + " - Raw Mat. (" + invIlRawMaterial.getInvItem().getIiName() + ")");
	      	    			}

	      	    		}

	      	    	}

	      	    }

        	}

        	
        	
        	if(details.getInvPartialCm()==EJBCommon.FALSE) {
        		System.out.println("RECOMPUTER------------------------------------------->");
            	// remove all distribution records

    	  	    Collection arDistributionRecords = arCreditMemo.getArDistributionRecords();

    	  	    Iterator iDr = arDistributionRecords.iterator();

    	  	    while (iDr.hasNext()) {

    	  	   	    LocalArDistributionRecord arDistributionRecord = (LocalArDistributionRecord)iDr.next();
    	  	   	    iDr.remove();
    	  	  	    arDistributionRecord.remove();

    	  	    }

    	  	    LocalArInvoice arInvoice2 = arInvoiceHome.findByInvNumberAndInvCreditMemoAndBrCode(
    	  			  arCreditMemo.getInvCmInvoiceNumber(), EJBCommon.FALSE, AD_BRNCH, AD_CMPNY);

    	  	    System.out.println("arInvoice2="+arInvoice2.getInvNumber());
    	  	    System.out.println("DR SIZE="+arInvoice2.getArDistributionRecords().size());
    	  	    
    	  	    double TOTAL_TAX =0 ;
    	  	    double W_TAX_AMOUNT = 0;
    	  	  double UNEARNED_INT_AMOUNT = 0;
    		  	Collection arDr = arInvoice2.getArDistributionRecords();

    			Iterator x = arDr.iterator();

    			while (x.hasNext()) {

    				LocalArDistributionRecord arDistributionRecord = (LocalArDistributionRecord)x.next();

    				if(arDistributionRecord.getDrClass().equals("TAX")) {
    					TOTAL_TAX += arDistributionRecord.getDrAmount();
    				}
    				
    				if(arDistributionRecord.getDrClass().equals("W-TAX")) {
    					W_TAX_AMOUNT += arDistributionRecord.getDrAmount();
    				}
    				
    				System.out.println(arDistributionRecord.getDrCode()+" "+arDistributionRecord.getDrClass()+" "+arDistributionRecord.getDrAmount());
    				this.addArDrEntry(arCreditMemo.getArDrNextLine(), arDistributionRecord.getDrClass(),
    						arDistributionRecord.getDrDebit() == EJBCommon.TRUE ? EJBCommon.FALSE : EJBCommon.TRUE,
    								arDistributionRecord.getDrAmount(), arDistributionRecord.getGlChartOfAccount().getCoaCode(),
    								arCreditMemo,  AD_BRNCH, AD_CMPNY);

    			}

    			if(arInvoice.getArCustomer().getCstAutoComputeInterest() == EJBCommon.TRUE) {
    				UNEARNED_INT_AMOUNT = EJBCommon.roundIt((TOTAL_LINE + TOTAL_TAX-  arInvoice.getInvDownPayment()) * arInvoice.getAdPaymentTerm().getAdPaymentSchedules().size() * (arInvoice.getArCustomer().getCstMonthlyInterestRate()/ 100) , this.getGlFcPrecisionUnit(AD_CMPNY));
    			
    			}
    			
    			//arCreditMemo.setInvAmountDue(TOTAL_LINE+ TOTAL_TAX - W_TAX_AMOUNT  + UNEARNED_INT_AMOUNT );
    			
        	}
        	
			
      	    // generate approval status

            String INV_APPRVL_STATUS = null;

        	if (!isDraft) {

        		LocalAdApproval adApproval = adApprovalHome.findByAprAdCompany(AD_CMPNY);

        		// check if ar credit memo approval is enabled

        		if (adApproval.getAprEnableArCreditMemo() == EJBCommon.FALSE) {

        			INV_APPRVL_STATUS = "N/A";

        		} else {

        			// check if credit memo is self approved

    				LocalAdUser adUser2 = adUserHome.findByUsrName(details.getInvLastModifiedBy(), AD_CMPNY);


				//	INV_APPRVL_STATUS = ejbIA.getApprovalStatus(adUser2.getUsrDept(), invAdjustment.getAdjLastModifiedBy(), adUser2.getUsrDescription(), "INV ADJUSTMENT", invAdjustment.getAdjCode(), invAdjustment.getAdjDocumentNumber(), invAdjustment.getAdjDate(), AD_BRNCH, AD_CMPNY);
					INV_APPRVL_STATUS = ejbAA.getApprovalStatus(adUser2.getUsrDept(), arCreditMemo.getInvLastModifiedBy(), adUser2.getUsrDescription(), "AR CREDIT MEMO", arCreditMemo.getInvCode(), arCreditMemo.getInvNumber(), arCreditMemo.getInvDate(), AD_BRNCH, AD_CMPNY);

        		}
        	}

        	if (INV_APPRVL_STATUS != null && INV_APPRVL_STATUS.equals("N/A") && adPreference.getPrfArGlPostingType().equals("AUTO-POST UPON APPROVAL")) {

        		this.executeArInvCreditMemoPost(arCreditMemo.getInvCode(), arCreditMemo.getInvLastModifiedBy(), AD_BRNCH, AD_CMPNY);

        	}

        	// set credit memo approval status

        	arCreditMemo.setInvApprovalStatus(INV_APPRVL_STATUS);

      	    return arCreditMemo.getInvCode();


        } catch (GlobalRecordAlreadyDeletedException ex) {

        	getSessionContext().setRollbackOnly();
        	throw ex;

        } catch (GlobalNoRecordFoundException ex) {

        	getSessionContext().setRollbackOnly();
        	throw ex;

        } catch (GlobalDocumentNumberNotUniqueException ex) {

        	getSessionContext().setRollbackOnly();
        	throw ex;

        } catch (GlobalTransactionAlreadyApprovedException ex) {

        	getSessionContext().setRollbackOnly();
        	throw ex;

        } catch (GlobalTransactionAlreadyPendingException ex) {

        	getSessionContext().setRollbackOnly();
        	throw ex;

        } catch (GlobalTransactionAlreadyPostedException ex) {

        	getSessionContext().setRollbackOnly();
        	throw ex;

        } catch (GlobalTransactionAlreadyVoidPostedException ex) {

        	getSessionContext().setRollbackOnly();
        	throw ex;
        /*
        } catch (ArINVOverapplicationNotAllowedException ex) {

        	getSessionContext().setRollbackOnly();
        	throw ex;
        */
        } catch (GlobalTransactionAlreadyLockedException ex) {

        	getSessionContext().setRollbackOnly();
        	throw ex;

         } catch (GlobalNoApprovalRequesterFoundException ex) {

        	getSessionContext().setRollbackOnly();
        	throw ex;

        } catch (GlobalNoApprovalApproverFoundException ex) {

        	getSessionContext().setRollbackOnly();
        	throw ex;

        } catch (GlJREffectiveDateNoPeriodExistException ex) {

        	getSessionContext().setRollbackOnly();
        	throw ex;

        } catch (GlJREffectiveDatePeriodClosedException ex) {

        	getSessionContext().setRollbackOnly();
        	throw ex;

        } catch (GlobalJournalNotBalanceException ex) {

        	getSessionContext().setRollbackOnly();
        	throw ex;

        } catch (GlobalInventoryDateException ex) {

        	getSessionContext().setRollbackOnly();
        	throw ex;

        } catch (GlobalBranchAccountNumberInvalidException ex) {

        	getSessionContext().setRollbackOnly();
        	throw ex;

        } catch (AdPRFCoaGlVarianceAccountNotFoundException ex){

            getSessionContext().setRollbackOnly();
            throw ex;

        } catch (Exception ex) {

        	Debug.printStackTrace(ex);
        	getSessionContext().setRollbackOnly();
        	throw new EJBException(ex.getMessage());

        }


    }

  private Integer executeVoid(
      com.util.ArInvoiceDetails details, boolean isDraft, Integer AD_BRNCH, Integer AD_CMPNY,
      LocalArInvoiceHome arInvoiceHome, LocalAdApprovalHome adApprovalHome,
      LocalAdPreferenceHome adPreferenceHome, LocalAdUserHome adUserHome,
      ArApprovalController ejbAA, LocalArInvoice arCreditMemo
  ) throws GlobalTransactionAlreadyVoidException, FinderException,
      GlobalNoApprovalRequesterFoundException, GlobalNoApprovalApproverFoundException,
      RemoteException, GlobalBranchAccountNumberInvalidException,
      GlobalRecordAlreadyDeletedException, GlobalTransactionAlreadyPostedException,
      GlobalTransactionAlreadyVoidPostedException, GlJREffectiveDateNoPeriodExistException,
      GlJREffectiveDatePeriodClosedException, GlobalJournalNotBalanceException,
      AdPRFCoaGlVarianceAccountNotFoundException {
    if (arCreditMemo.getInvVoid() == EJBCommon.TRUE) {

    		throw new GlobalTransactionAlreadyVoidException();

    	}

    System.out.println("1--------------------------");

    	if (arCreditMemo.getInvPosted() == EJBCommon.TRUE) {
    		System.out.println("2--------------------------");


    		// generate approval status

              String INV_APPRVL_STATUS = null;

          	if (!isDraft) {

          		LocalAdApproval adApproval = adApprovalHome.findByAprAdCompany(AD_CMPNY);

          		// check if ar credit memo approval is enabled

          		if (adApproval.getAprEnableArCreditMemo() == EJBCommon.FALSE) {

          			INV_APPRVL_STATUS = "N/A";

          		} else {

      				// check if credit memo is self approved

    			LocalAdUser adUser2 = adUserHome.findByUsrName(details.getInvLastModifiedBy(), AD_CMPNY);


    	//	INV_APPRVL_STATUS = ejbIA.getApprovalStatus(adUser2.getUsrDept(), invAdjustment.getAdjLastModifiedBy(), adUser2.getUsrDescription(), "INV ADJUSTMENT", invAdjustment.getAdjCode(), invAdjustment.getAdjDocumentNumber(), invAdjustment.getAdjDate(), AD_BRNCH, AD_CMPNY);
    		INV_APPRVL_STATUS = ejbAA.getApprovalStatus(adUser2.getUsrDept(), arCreditMemo.getInvLastModifiedBy(), adUser2.getUsrDescription(), "AR CREDIT MEMO", arCreditMemo.getInvCode(), arCreditMemo.getInvNumber(), arCreditMemo.getInvDate(), AD_BRNCH, AD_CMPNY);

      			}
          		
          	}

          	
          	
          	// reverse distribution records
    	System.out.println("reverse distribution records = "+arCreditMemo.getInvNumber());
    	Collection arDistributionRecords = arCreditMemo.getArDistributionRecords();
    	ArrayList list = new ArrayList();

    	Iterator i = arDistributionRecords.iterator();

    	while (i.hasNext()) {

    		LocalArDistributionRecord arDistributionRecord = (LocalArDistributionRecord)i.next();

    		list.add(arDistributionRecord);

    	}

    	i = list.iterator();

    	while (i.hasNext()) {

    		LocalArDistributionRecord arDistributionRecord = (LocalArDistributionRecord)i.next();

    		System.out.println(arDistributionRecord.getDrCode()+" "+arDistributionRecord.getDrClass()+" "+arDistributionRecord.getDrAmount());
    		this.addArDrEntry(arCreditMemo.getArDrNextLine(), arDistributionRecord.getDrClass(),
    				arDistributionRecord.getDrDebit() == EJBCommon.TRUE ? EJBCommon.FALSE : EJBCommon.TRUE,
    						arDistributionRecord.getDrAmount(), arDistributionRecord.getGlChartOfAccount().getCoaCode(),
    						arCreditMemo,  AD_BRNCH, AD_CMPNY);

    	}
    	System.out.println("3--------------------------");
          	LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);

          	if (INV_APPRVL_STATUS != null && INV_APPRVL_STATUS.equals("N/A") && adPreference.getPrfArGlPostingType().equals("AUTO-POST UPON APPROVAL")) {
          		System.out.println("4--------------------------");
          		arCreditMemo.setInvVoid(EJBCommon.TRUE);
          		this.executeArInvCreditMemoPost(arCreditMemo.getInvCode(), arCreditMemo.getInvLastModifiedBy(), AD_BRNCH, AD_CMPNY);

          	}

          	// set credit memo approval status

          	arCreditMemo.setInvVoidApprovalStatus(INV_APPRVL_STATUS);



    	} else {

    		System.out.println("5--------------------------");
    		LocalArInvoice arLockedInvoice = arInvoiceHome.findByInvNumberAndInvCreditMemoAndBrCode(arCreditMemo.getInvCmInvoiceNumber(),
    		    EJBCommon.FALSE, AD_BRNCH, AD_CMPNY);

    		Collection arLockedInvoicePaymentSchedules = arLockedInvoice.getArInvoicePaymentSchedules();

    		Iterator ipsIter = arLockedInvoicePaymentSchedules.iterator();

    		while (ipsIter.hasNext()) {

    			LocalArInvoicePaymentSchedule arInvoicePaymentSchedule =
    			    (LocalArInvoicePaymentSchedule)ipsIter.next();

    			arInvoicePaymentSchedule.setIpsLock(EJBCommon.FALSE);

    		}


    }

    arCreditMemo.setInvVoid(EJBCommon.TRUE);
    arCreditMemo.setInvLastModifiedBy(details.getInvLastModifiedBy());
    arCreditMemo.setInvDateLastModified(details.getInvDateLastModified());
    System.out.println("6--------------------------");
    	return arCreditMemo.getInvCode();
  }

    /**
     * @ejb:interface-method view-type="remote"
     **/
    public void deleteArInvEntry(Integer INV_CODE, String AD_USR, Integer AD_BRNCH, Integer AD_CMPNY) throws
        GlobalRecordAlreadyDeletedException {

        Debug.print("ArCreditMemoEntryControllerBean deleteArInvEntry");

        LocalArInvoiceHome arInvoiceHome = null;
        LocalAdApprovalQueueHome adApprovalQueueHome = null;
        LocalAdDeleteAuditTrailHome adDeleteAuditTrailHome = null;

        // Initialize EJB Home

        try {

            arInvoiceHome = (LocalArInvoiceHome)EJBHomeFactory.
                lookUpLocalHome(LocalArInvoiceHome.JNDI_NAME, LocalArInvoiceHome.class);
            adApprovalQueueHome = (LocalAdApprovalQueueHome)EJBHomeFactory.
			    lookUpLocalHome(LocalAdApprovalQueueHome.JNDI_NAME, LocalAdApprovalQueueHome.class);
            adDeleteAuditTrailHome = (LocalAdDeleteAuditTrailHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdDeleteAuditTrailHome.JNDI_NAME, LocalAdDeleteAuditTrailHome.class);

        } catch (NamingException ex) {

            throw new EJBException(ex.getMessage());

        }

        try {

        	LocalArInvoice arCreditMemo = arInvoiceHome.findByPrimaryKey(INV_CODE);

        	if (arCreditMemo.getInvApprovalStatus() != null && arCreditMemo.getInvApprovalStatus().equals("PENDING")) {

        		Collection adApprovalQueues = adApprovalQueueHome.findByAqDocumentAndAqDocumentCode("AR CREDIT MEMO", arCreditMemo.getInvCode(), AD_CMPNY);

        		Iterator i = adApprovalQueues.iterator();

        		while(i.hasNext()) {

        			LocalAdApprovalQueue adApprovalQueue = (LocalAdApprovalQueue)i.next();

        			i.remove();

        			adApprovalQueue.remove();

        		}

        	}

        	// release lock

        	LocalArInvoice arInvoice = arInvoiceHome.findByInvNumberAndInvCreditMemoAndBrCode(arCreditMemo.getInvCmInvoiceNumber(), EJBCommon.FALSE, AD_BRNCH, AD_CMPNY);

      	    Collection arInvoicePaymentSchedules = arInvoice.getArInvoicePaymentSchedules();

  	   		Iterator i = arInvoicePaymentSchedules.iterator();

  	   		while (i.hasNext()) {

      	   	   LocalArInvoicePaymentSchedule arInvoicePaymentSchedule = (LocalArInvoicePaymentSchedule)i.next();

    	   	   arInvoicePaymentSchedule.setIpsLock(EJBCommon.FALSE);

  	        }


  	   		adDeleteAuditTrailHome.create("AR CREDIT MEMO", arCreditMemo.getInvDate(), arCreditMemo.getInvNumber(), arCreditMemo.getInvReferenceNumber(),
  	   			arCreditMemo.getInvAmountDue(), AD_USR, new Date(), AD_CMPNY);

        	arCreditMemo.remove();

        } catch (FinderException ex) {

            getSessionContext().setRollbackOnly();
            throw new GlobalRecordAlreadyDeletedException();

        } catch (Exception ex) {

        	Debug.printStackTrace(ex);
        	getSessionContext().setRollbackOnly();
        	throw new EJBException(ex.getMessage());

        }

    }

   /**
    * @ejb:interface-method view-type="remote"
    * @jboss:method-attributes read-only="true"
    **/
    public short getGlFcPrecisionUnit(Integer AD_CMPNY) {

       Debug.print("ArCreditMemoEntryControllerBean getGlFcPrecisionUnit");


       LocalAdCompanyHome adCompanyHome = null;


       // Initialize EJB Home

       try {

           adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
              lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);

       } catch (NamingException ex) {

           throw new EJBException(ex.getMessage());

       }

       try {

         LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);

         return  adCompany.getGlFunctionalCurrency().getFcPrecision();

       } catch (Exception ex) {

       	 Debug.printStackTrace(ex);
         throw new EJBException(ex.getMessage());

       }

    }

    /**
    * @ejb:interface-method view-type="remote"
    * @jboss:method-attributes read-only="true"
    **/
    public ArrayList getAdApprovalNotifiedUsersByInvCode(Integer INV_CODE, Integer AD_CMPNY) {

       Debug.print("ArCreditMemoEntryControllerBean getAdApprovalNotifiedUsersByInvCode");


       LocalAdApprovalQueueHome adApprovalQueueHome = null;
       LocalArInvoiceHome arInvoiceHome = null;

       ArrayList list = new ArrayList();


       // Initialize EJB Home

       try {

           adApprovalQueueHome = (LocalAdApprovalQueueHome)EJBHomeFactory.
              lookUpLocalHome(LocalAdApprovalQueueHome.JNDI_NAME, LocalAdApprovalQueueHome.class);
           arInvoiceHome = (LocalArInvoiceHome)EJBHomeFactory.
              lookUpLocalHome(LocalArInvoiceHome.JNDI_NAME, LocalArInvoiceHome.class);

       } catch (NamingException ex) {

           throw new EJBException(ex.getMessage());

       }

       try {

         LocalArInvoice arInvoice = arInvoiceHome.findByPrimaryKey(INV_CODE);

         if (arInvoice.getInvPosted() == EJBCommon.TRUE) {

         	list.add("DOCUMENT POSTED");
         	return list;

         }

         Collection adApprovalQueues = adApprovalQueueHome.findByAqDocumentAndAqDocumentCode("AR CREDIT MEMO", INV_CODE, AD_CMPNY);

         Iterator i = adApprovalQueues.iterator();

         while(i.hasNext()) {

         	LocalAdApprovalQueue adApprovalQueue = (LocalAdApprovalQueue)i.next();

         	list.add(adApprovalQueue.getAdUser().getUsrDescription());

         }

         return list;

       } catch (Exception ex) {

       	 Debug.printStackTrace(ex);
         throw new EJBException(ex.getMessage());

       }

    }


    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
     public byte getAdPrfEnableArCreditMemoBatch(Integer AD_CMPNY) {

        Debug.print("ArCreditMemoEntryControllerBean getAdPrfEnableArCreditMemoBatch");

        LocalAdPreferenceHome adPreferenceHome = null;


        // Initialize EJB Home

        try {

           adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
               lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);

        } catch (NamingException ex) {

           throw new EJBException(ex.getMessage());

        }


        try {

           LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);

           return adPreference.getPrfEnableArInvoiceBatch();

        } catch (Exception ex) {

           Debug.printStackTrace(ex);
           throw new EJBException(ex.getMessage());

        }

     }


     /**
      * @ejb:interface-method view-type="remote"
      * @jboss:method-attributes read-only="true"
      **/
     public ArrayList getArOpenIbAll(Integer AD_BRNCH, Integer AD_CMPNY) {

         Debug.print("ArCreditMemoEntryControllerBean getArOpenIbAll");

         LocalArInvoiceBatchHome arInvoiceBatchHome = null;

         ArrayList list = new ArrayList();

         // Initialize EJB Home

         try {

         	arInvoiceBatchHome = (LocalArInvoiceBatchHome)EJBHomeFactory.
                 lookUpLocalHome(LocalArInvoiceBatchHome.JNDI_NAME, LocalArInvoiceBatchHome.class);

         } catch (NamingException ex) {

             throw new EJBException(ex.getMessage());

         }

         try {

         	Collection arInvoiceBatches = arInvoiceBatchHome.findOpenIbByIbType("CREDIT MEMO", AD_BRNCH, AD_CMPNY);

         	Iterator i = arInvoiceBatches.iterator();

         	while (i.hasNext()) {

         		LocalArInvoiceBatch arInvoiceBatch = (LocalArInvoiceBatch)i.next();

         		list.add(arInvoiceBatch.getIbName());

         	}

         	return list;

         } catch (Exception ex) {

         	Debug.printStackTrace(ex);
         	throw new EJBException(ex.getMessage());

         }

     }

     
     
     /**
      * @ejb:interface-method view-type="remote"
      * @jboss:method-attributes read-only="true"
      **/
     public ArrayList getArSoInvLnItmByArInvNmbr(String AR_INV_NMBR, Integer AD_BRNCH, Integer AD_CMPNY)
 	    throws GlobalNoRecordFoundException {


    	  Debug.print("ArCreditMemoEntryControllerBean getArSoInvLnItmByArInvNmbr");
    	  LocalArSalesOrderInvoiceLineHome arSalesOrderInvoiceLineHome = null;
    	  LocalArSalesOrderLineHome arSalesOrderLineHome = null;

    	 LocalArInvoiceHome arInvoiceHome = null;

    	 try {

    		 arSalesOrderInvoiceLineHome = (LocalArSalesOrderInvoiceLineHome)EJBHomeFactory.
                     lookUpLocalHome(LocalArSalesOrderInvoiceLineHome.JNDI_NAME, LocalArSalesOrderInvoiceLineHome.class);

    		 arSalesOrderLineHome = (LocalArSalesOrderLineHome)EJBHomeFactory.
                     lookUpLocalHome(LocalArSalesOrderLineHome.JNDI_NAME, LocalArSalesOrderLineHome.class);

             arInvoiceHome = (LocalArInvoiceHome)EJBHomeFactory.
                     lookUpLocalHome(LocalArInvoiceHome.JNDI_NAME, LocalArInvoiceHome.class);

         } catch (NamingException ex) 	{

             throw new EJBException(ex.getMessage());

         }




    	 try{
    		 ArrayList iliList = new ArrayList();

    		 LocalArInvoice arInvoice = null;
    		 Collection ar_so_inv_ln ;
    		 try{
    			 System.out.println("find invoice");
    			  arInvoice = (LocalArInvoice) arInvoiceHome.findByInvNumberAndInvCreditMemoAndBrCode(AR_INV_NMBR,EJBCommon.FALSE,AD_BRNCH, AD_CMPNY);
    			  System.out.println("find line items");
    			  ar_so_inv_ln = arSalesOrderInvoiceLineHome.findByInvCode(arInvoice.getInvCode(), AD_CMPNY);
    			  System.out.println("find line items done");
    		 }catch(Exception ex){
    			 throw new GlobalNoRecordFoundException();
    		 }

    		 Iterator i = ar_so_inv_ln.iterator();

          	 while (i.hasNext()) {
          	

          		LocalArSalesOrderInvoiceLine arSalesOrderInvoiceLine = (LocalArSalesOrderInvoiceLine)i.next();

          		
          		ArModInvoiceLineItemDetails mdetails = new ArModInvoiceLineItemDetails();

          		   mdetails.setIliCode(arSalesOrderInvoiceLine.getSilCode());
          		   mdetails.setIliLine(arSalesOrderInvoiceLine.getArSalesOrderLine().getSolLine());
          		   mdetails.setIliIiDescription(arSalesOrderInvoiceLine.getArSalesOrderLine().getInvItemLocation().getInvItem().getIiDescription());
            	   mdetails.setIliIiName(arSalesOrderInvoiceLine.getArSalesOrderLine().getInvItemLocation().getInvItem().getIiName());
            	   mdetails.setIliLocName(arSalesOrderInvoiceLine.getArSalesOrderLine().getInvItemLocation().getInvLocation().getLocName());
            	   mdetails.setIliQuantity(arSalesOrderInvoiceLine.getSilQuantityDelivered());
            	   mdetails.setIliUomName(arSalesOrderInvoiceLine.getArSalesOrderLine().getInvUnitOfMeasure().getUomName());
            	   mdetails.setIliUnitPrice(arSalesOrderInvoiceLine.getArSalesOrderLine().getSolUnitPrice());
            	   mdetails.setIliTax(arSalesOrderInvoiceLine.getSilTax());
            	   mdetails.setIliAmount(
            			   arSalesOrderInvoiceLine.getArInvoice().getArTaxCode().getTcName().contains("EXCLUSIVE") ?
            					   arSalesOrderInvoiceLine.getSilAmount()
            					   :
            						   arSalesOrderInvoiceLine.getSilAmount() + arSalesOrderInvoiceLine.getSilTaxAmount()

            			   );
            	   mdetails.setIliEnableAutoBuild((byte)0);
            	   mdetails.setIliMisc(arSalesOrderInvoiceLine.getArSalesOrderLine().getSolMisc());
            	   mdetails.setIliTaxCodeName(arSalesOrderInvoiceLine.getArInvoice().getArTaxCode().getTcName());
            	   mdetails.setIliWTaxCodeName(arSalesOrderInvoiceLine.getArInvoice().getArTaxCode().getTcName());
            	   mdetails.setIliIiClass( arSalesOrderInvoiceLine.getArSalesOrderLine().getInvItemLocation().getInvItem().getIiClass());

            	   iliList.add(mdetails);

          	}



          	return iliList;


    	 } catch (GlobalNoRecordFoundException ex) {

	   	  	  throw ex;

	   	  } catch (Exception ex) {


	   	  	  ex.printStackTrace();
	   	  	  throw new EJBException(ex.getMessage());

	   	  }


     }
     
     
     
     
     /**
      * @ejb:interface-method view-type="remote"
      * @jboss:method-attributes read-only="true"
      **/
     public ArrayList getArJoInvLnItmByArInvNmbr(String AR_INV_NMBR, Integer AD_BRNCH, Integer AD_CMPNY)
 	    throws GlobalNoRecordFoundException {


    	  Debug.print("ArCreditMemoEntryControllerBean getArJoInvLnItmByArInvNmbr");
    	
    	  
    	  LocalArJobOrderInvoiceLineHome arJobOrderInvoiceLineHome = null;
    	  LocalArJobOrderLineHome arJobOrderLineHome = null;

    	 LocalArInvoiceHome arInvoiceHome = null;

    	 try {

    		 arJobOrderInvoiceLineHome = (LocalArJobOrderInvoiceLineHome)EJBHomeFactory.
                     lookUpLocalHome(LocalArJobOrderInvoiceLineHome.JNDI_NAME, LocalArJobOrderInvoiceLineHome.class);

    		 arJobOrderLineHome = (LocalArJobOrderLineHome)EJBHomeFactory.
                     lookUpLocalHome(LocalArJobOrderLineHome.JNDI_NAME, LocalArJobOrderLineHome.class);

             arInvoiceHome = (LocalArInvoiceHome)EJBHomeFactory.
                     lookUpLocalHome(LocalArInvoiceHome.JNDI_NAME, LocalArInvoiceHome.class);

         } catch (NamingException ex) 	{

             throw new EJBException(ex.getMessage());

         }




    	 try{
    		 ArrayList iliList = new ArrayList();

    		 LocalArInvoice arInvoice = null;
    		 Collection ar_jo_inv_ln ;
    		 try{
    			 System.out.println("find invoice");
    			  arInvoice = (LocalArInvoice) arInvoiceHome.findByInvNumberAndInvCreditMemoAndBrCode(AR_INV_NMBR,EJBCommon.FALSE,AD_BRNCH, AD_CMPNY);
    			  System.out.println("find line items");
    			  ar_jo_inv_ln = arJobOrderInvoiceLineHome.findByInvCode(arInvoice.getInvCode(), AD_CMPNY);
    			  System.out.println("find line items done");
    		 }catch(Exception ex){
    			 throw new GlobalNoRecordFoundException();
    		 }

    		 Iterator i = ar_jo_inv_ln.iterator();

          	 while (i.hasNext()) {
          	

          	
          		LocalArJobOrderInvoiceLine arJobOrderInvoiceLine = (LocalArJobOrderInvoiceLine)i.next();
          		
          		ArModInvoiceLineItemDetails mdetails = new ArModInvoiceLineItemDetails();

          		   mdetails.setIliCode(arJobOrderInvoiceLine.getJilCode());
          		   mdetails.setIliLine(arJobOrderInvoiceLine.getArJobOrderLine().getJolLine());
          		   mdetails.setIliIiDescription(arJobOrderInvoiceLine.getArJobOrderLine().getInvItemLocation().getInvItem().getIiDescription());
            	   mdetails.setIliIiName(arJobOrderInvoiceLine.getArJobOrderLine().getInvItemLocation().getInvItem().getIiName());
            	   mdetails.setIliLocName(arJobOrderInvoiceLine.getArJobOrderLine().getInvItemLocation().getInvLocation().getLocName());
            	   mdetails.setIliQuantity(arJobOrderInvoiceLine.getJilQuantityDelivered());
            	   mdetails.setIliUomName(arJobOrderInvoiceLine.getArJobOrderLine().getInvUnitOfMeasure().getUomName());
            	   mdetails.setIliUnitPrice(arJobOrderInvoiceLine.getArJobOrderLine().getJolUnitPrice());
            	   mdetails.setIliTax(arJobOrderInvoiceLine.getJilTax());
            	   mdetails.setIliAmount(
            			   arJobOrderInvoiceLine.getArInvoice().getArTaxCode().getTcName().contains("EXCLUSIVE") ?
            					   arJobOrderInvoiceLine.getJilAmount()
            					   :
            						   arJobOrderInvoiceLine.getJilAmount() + arJobOrderInvoiceLine.getJilTaxAmount()

            			   );
            	   mdetails.setIliEnableAutoBuild((byte)0);
            	   mdetails.setIliMisc(arJobOrderInvoiceLine.getArJobOrderLine().getJolMisc());
            	   mdetails.setIliTaxCodeName(arJobOrderInvoiceLine.getArInvoice().getArTaxCode().getTcName());
            	   mdetails.setIliWTaxCodeName(arJobOrderInvoiceLine.getArInvoice().getArTaxCode().getTcName());
            	   mdetails.setIliIiClass( arJobOrderInvoiceLine.getArJobOrderLine().getInvItemLocation().getInvItem().getIiClass());

            	   iliList.add(mdetails);

          	}



          	return iliList;


    	 } catch (GlobalNoRecordFoundException ex) {

	   	  	  throw ex;

	   	  } catch (Exception ex) {


	   	  	  ex.printStackTrace();
	   	  	  throw new EJBException(ex.getMessage());

	   	  }


     }
     
     
     
     
     
     
     
     
     /**
      * @ejb:interface-method view-type="remote"
      * @jboss:method-attributes read-only="true"
      **/
     public ArModInvoiceDetails getArInvByArInvNmbr(String AR_INV_NMBR, Integer AD_BRNCH, Integer AD_CMPNY)
 	    throws GlobalNoRecordFoundException {


    	  Debug.print("ArCreditMemoEntryControllerBean getArInvByArInvNmbr");
    	 LocalArInvoiceLineItemHome arInvoiceLineItemHome = null;
    	 LocalArInvoiceHome arInvoiceHome = null;

    	 try {


           
             arInvoiceHome = (LocalArInvoiceHome)EJBHomeFactory.
                     lookUpLocalHome(LocalArInvoiceHome.JNDI_NAME, LocalArInvoiceHome.class);

         } catch (NamingException ex) 	{

             throw new EJBException(ex.getMessage());

         }




    	 try{
    		 ArrayList iliList = new ArrayList();

    		 LocalArInvoice arInvoice = null;
    		 Collection ar_inv_ln_itm ;
    		 try{
    			 System.out.println("find invoice");
    			  arInvoice = arInvoiceHome.findByInvNumberAndInvCreditMemoAndBrCode(AR_INV_NMBR,EJBCommon.FALSE,AD_BRNCH, AD_CMPNY);
    			
    		//	  ar_inv_ln_itm = arInvoiceLineItemHome.findByInvCode(arInvoice.getInvCode(), AD_CMPNY);
    			  System.out.println("find line items done");
    		 }catch(Exception ex){
    			 throw new GlobalNoRecordFoundException();
    		 }
    		 ArModInvoiceDetails mInvDetails = new ArModInvoiceDetails();

    		 mInvDetails.setInvType(arInvoice.getInvType());
         	mInvDetails.setInvCode(arInvoice.getInvCode());
         	mInvDetails.setInvDescription(arInvoice.getInvDescription());
         	mInvDetails.setInvDate(arInvoice.getInvDate());
         	mInvDetails.setInvNumber(arInvoice.getInvNumber());
         	mInvDetails.setInvCmInvoiceNumber(arInvoice.getInvCmInvoiceNumber());
         	mInvDetails.setInvCmReferenceNumber(arInvoice.getInvCmReferenceNumber());
         	mInvDetails.setInvAmountDue(arInvoice.getInvAmountDue());
         	mInvDetails.setInvApprovalStatus(arInvoice.getInvApprovalStatus());
         	mInvDetails.setInvVoidApprovalStatus(arInvoice.getInvVoidApprovalStatus());
         	mInvDetails.setInvReasonForRejection(arInvoice.getInvReasonForRejection());
         	mInvDetails.setInvPosted(arInvoice.getInvPosted());
         	mInvDetails.setInvVoidPosted(arInvoice.getInvVoidPosted());
         	mInvDetails.setInvVoid(arInvoice.getInvVoid());

         	mInvDetails.setInvCreatedBy(arInvoice.getInvCreatedBy());
         	mInvDetails.setInvDateCreated(arInvoice.getInvDateCreated());
         	mInvDetails.setInvLastModifiedBy(arInvoice.getInvLastModifiedBy());
         	mInvDetails.setInvDateLastModified(arInvoice.getInvDateLastModified());
         	mInvDetails.setInvApprovedRejectedBy(arInvoice.getInvApprovedRejectedBy());
         	mInvDetails.setInvDateApprovedRejected(arInvoice.getInvDateApprovedRejected());
         	mInvDetails.setInvPostedBy(arInvoice.getInvPostedBy());
         	mInvDetails.setInvDatePosted(arInvoice.getInvDatePosted());
         	mInvDetails.setInvCstCustomerCode(arInvoice.getArCustomer().getCstCustomerCode());
         	mInvDetails.setInvIbName(arInvoice.getArInvoiceBatch() != null ? arInvoice.getArInvoiceBatch().getIbName() : null);
         	mInvDetails.setInvLvShift(arInvoice.getInvLvShift());
         	mInvDetails.setInvSubjectToCommission(arInvoice.getInvSubjectToCommission());
         	mInvDetails.setInvCstName(arInvoice.getArCustomer().getCstName());

         
         	return mInvDetails;
    		 
    		 

    		



    	 } catch (GlobalNoRecordFoundException ex) {

	   	  	  throw ex;

	   	  } catch (Exception ex) {


	   	  	  ex.printStackTrace();
	   	  	  throw new EJBException(ex.getMessage());

	   	  }


     }
     
     
     
     
     
     
     
     
     
     
     
     
     /**
      * @ejb:interface-method view-type="remote"
      * @jboss:method-attributes read-only="true"
      **/
     public ArrayList getArInvLnItmByArInvNmbr(String AR_INV_NMBR, Integer AD_BRNCH, Integer AD_CMPNY)
 	    throws GlobalNoRecordFoundException {


    	  Debug.print("ArCreditMemoEntryControllerBean getArInvLnItmByArInvNmbr");
    	 LocalArInvoiceLineItemHome arInvoiceLineItemHome = null;
    	 LocalArInvoiceHome arInvoiceHome = null;

    	 try {


             arInvoiceLineItemHome = (LocalArInvoiceLineItemHome)EJBHomeFactory.
                     lookUpLocalHome(LocalArInvoiceLineItemHome.JNDI_NAME, LocalArInvoiceLineItemHome.class);

             arInvoiceHome = (LocalArInvoiceHome)EJBHomeFactory.
                     lookUpLocalHome(LocalArInvoiceHome.JNDI_NAME, LocalArInvoiceHome.class);

         } catch (NamingException ex) 	{

             throw new EJBException(ex.getMessage());

         }




    	 try{
    		 ArrayList iliList = new ArrayList();

    		 LocalArInvoice arInvoice = null;
    		 Collection ar_inv_ln_itm ;
    		 try{
    			 System.out.println("find invoice");
    			  arInvoice = (LocalArInvoice) arInvoiceHome.findByInvNumberAndInvCreditMemoAndBrCode(AR_INV_NMBR,EJBCommon.FALSE,AD_BRNCH, AD_CMPNY);
    			  System.out.println("find line items");
    			  ar_inv_ln_itm = arInvoiceLineItemHome.findByInvCode(arInvoice.getInvCode(), AD_CMPNY);
    			  System.out.println("find line items done");
    		 }catch(Exception ex){
    			 throw new GlobalNoRecordFoundException();
    		 }

    		 Iterator i = ar_inv_ln_itm.iterator();

          	 while (i.hasNext()) {
          		LocalArInvoiceLineItem arInvoiceLineItem = (LocalArInvoiceLineItem)i.next();



          		ArModInvoiceLineItemDetails mdetails = new ArModInvoiceLineItemDetails();

          		   mdetails.setIliCode(arInvoiceLineItem.getIliCode());
          		   mdetails.setIliLine(arInvoiceLineItem.getIliLine());
          		   mdetails.setIliIiDescription(arInvoiceLineItem.getInvItemLocation().getInvItem().getIiDescription());
            	   mdetails.setIliIiName(arInvoiceLineItem.getInvItemLocation().getInvItem().getIiName());
            	   mdetails.setIliLocName(arInvoiceLineItem.getInvItemLocation().getInvLocation().getLocName());
            	   mdetails.setIliQuantity(arInvoiceLineItem.getIliQuantity());
            	   mdetails.setIliUomName(arInvoiceLineItem.getInvUnitOfMeasure().getUomName());
            	   mdetails.setIliUnitPrice(arInvoiceLineItem.getIliUnitPrice());
            	   mdetails.setIliTax(arInvoiceLineItem.getIliTax());
            	   mdetails.setIliAmount(
            			   arInvoiceLineItem.getArInvoice().getArTaxCode().getTcName().contains("EXCLUSIVE") ?
            					   arInvoiceLineItem.getIliAmount()
            					   :
            						   arInvoiceLineItem.getIliAmount() + arInvoiceLineItem.getIliTaxAmount()

            			   );
            	   mdetails.setIliEnableAutoBuild(arInvoiceLineItem.getIliEnableAutoBuild());
            	   mdetails.setIliMisc(arInvoiceLineItem.getIliMisc());
            	   mdetails.setIliTaxCodeName(arInvoiceLineItem.getArInvoice().getArTaxCode().getTcName());
            	   mdetails.setIliWTaxCodeName(arInvoiceLineItem.getArInvoice().getArTaxCode().getTcName());
            	   mdetails.setIliIiClass( arInvoiceLineItem.getInvItemLocation().getInvItem().getIiClass());

            	   iliList.add(mdetails);

          	}



          	return iliList;


    	 } catch (GlobalNoRecordFoundException ex) {

	   	  	  throw ex;

	   	  } catch (Exception ex) {


	   	  	  ex.printStackTrace();
	   	  	  throw new EJBException(ex.getMessage());

	   	  }


     }





     /**
      * @ejb:interface-method view-type="remote"
      * @jboss:method-attributes read-only="true"
      **/
     public ArrayList getArDrByInvCmInvoiceNumberAndInvBillAmountAndCstCustomerCode(String INV_CM_INVC_NMBR, double INV_BLL_AMNT, String CST_CSTMR_CODE, Integer AD_CMPNY)
 	    throws GlobalNoRecordFoundException,
         ArINVOverapplicationNotAllowedException {

     	Debug.print("ArCreditMemoEntryControllerBean getArDrByInvCmInvoiceNumberAndInvBillAmountAndCstCustomerCode");

     	LocalArCustomerHome arCustomerHome = null;
     	LocalGlChartOfAccountHome glChartOfAccountHome = null;
     	LocalArInvoiceHome arInvoiceHome = null;
     	LocalArInvoicePaymentScheduleHome arInvoicePaymentScheduleHome = null;

     	ArrayList list = new ArrayList();

     	// Initialize EJB Home

     	try {

     		arCustomerHome = (LocalArCustomerHome)EJBHomeFactory.
 			   lookUpLocalHome(LocalArCustomerHome.JNDI_NAME, LocalArCustomerHome.class);
     		arInvoiceHome = (LocalArInvoiceHome)EJBHomeFactory.
 			   lookUpLocalHome(LocalArInvoiceHome.JNDI_NAME, LocalArInvoiceHome.class);
     		glChartOfAccountHome = (LocalGlChartOfAccountHome)EJBHomeFactory.
 			   lookUpLocalHome(LocalGlChartOfAccountHome.JNDI_NAME, LocalGlChartOfAccountHome.class);
     		arInvoicePaymentScheduleHome = (LocalArInvoicePaymentScheduleHome)EJBHomeFactory.
 			   lookUpLocalHome(LocalArInvoicePaymentScheduleHome.JNDI_NAME, LocalArInvoicePaymentScheduleHome.class);

     	} catch (NamingException ex) {

     		throw new EJBException(ex.getMessage());

     	}

     	try {

     		LocalArInvoice arInvoice = null;

     		short precisionUnit = this.getGlFcPrecisionUnit(AD_CMPNY);


     		// validate if invoice exist or overapplied

     		try {

     			arInvoice = arInvoiceHome.findByInvNumberAndInvCreditMemoAndCstCustomerCode(INV_CM_INVC_NMBR,EJBCommon.FALSE, CST_CSTMR_CODE, AD_CMPNY);

     			System.out.println("INV_BLL_AMNT="+INV_BLL_AMNT);
     			System.out.println("arInvoice.getInvAmountDue() - arInvoice.getInvAmountPaid()="+EJBCommon.roundIt(arInvoice.getInvAmountDue() - arInvoice.getInvAmountPaid(), precisionUnit));
     			if (arInvoice.getInvPosted() != EJBCommon.TRUE) {

     				throw new GlobalNoRecordFoundException();

     			} else if (INV_BLL_AMNT > EJBCommon.roundIt(arInvoice.getInvAmountDue() - arInvoice.getInvAmountPaid(), precisionUnit)) {
     				System.out.println("ArINVOverapplicationNotAllowedException----------->");
     				throw new ArINVOverapplicationNotAllowedException();

     			}


     		} catch (FinderException ex) {

     			throw new GlobalNoRecordFoundException();

     		}


     		// get amount percent

     		double AMOUNT_PERCENT = INV_BLL_AMNT / arInvoice.getInvAmountDue();

     		Collection arDistributionRecords = arInvoice.getArDistributionRecords();


     		// get total debit and credit for rounding difference calculation

     		double TOTAL_DEBIT = 0d;
     		double TOTAL_CREDIT = 0d;
     		boolean isRoundingDifferenceCalculated = false;

     		Iterator i = arDistributionRecords.iterator();

     		while (i.hasNext()) {

     			LocalArDistributionRecord arDistributionRecord = (LocalArDistributionRecord)i.next();

     			if (arDistributionRecord.getDrDebit() == EJBCommon.TRUE) {

     				TOTAL_DEBIT += EJBCommon.roundIt(arDistributionRecord.getDrAmount() * AMOUNT_PERCENT, precisionUnit);

     			} else {

     				TOTAL_CREDIT += EJBCommon.roundIt(arDistributionRecord.getDrAmount() * AMOUNT_PERCENT, precisionUnit);

     			}

     		}

     		System.out.println("TOTAL_DEBIT="+TOTAL_DEBIT);
     		System.out.println("TOTAL_CREDIT="+TOTAL_CREDIT);

     		// get default debit memo lines

     		short lineNumber = 1;

     		i = arDistributionRecords.iterator();

     		while (i.hasNext()) {

     			LocalArDistributionRecord arDistributionRecord = (LocalArDistributionRecord)i.next();

     			ArModDistributionRecordDetails mdetails = new ArModDistributionRecordDetails();

     			mdetails.setDrLine(lineNumber++);
     			mdetails.setDrClass(arDistributionRecord.getDrClass());
     			mdetails.setDrDebit(arDistributionRecord.getDrDebit() == EJBCommon.TRUE ?
     					EJBCommon.FALSE : EJBCommon.TRUE);

     			double DR_AMNT = EJBCommon.roundIt(arDistributionRecord.getDrAmount() * AMOUNT_PERCENT, precisionUnit);

     			// calculate rounding difference if necessary

     			if (arDistributionRecord.getDrDebit() == EJBCommon.FALSE &&
     					TOTAL_DEBIT != TOTAL_CREDIT && !isRoundingDifferenceCalculated) {

     				DR_AMNT = DR_AMNT + TOTAL_DEBIT - TOTAL_CREDIT;

     				isRoundingDifferenceCalculated = true;

     			}

     			mdetails.setDrAmount(DR_AMNT);
     			mdetails.setDrCoaAccountNumber(arDistributionRecord.getGlChartOfAccount().getCoaAccountNumber());
     			mdetails.setDrCoaAccountDescription(arDistributionRecord.getGlChartOfAccount().getCoaAccountDescription());

     			list.add(mdetails);

     		}


     		return list;

     	} catch (GlobalNoRecordFoundException ex) {

     		throw ex;

     	} catch (ArINVOverapplicationNotAllowedException ex) {

     		throw ex;

     	} catch (Exception ex) {

     		Debug.printStackTrace(ex);
     		throw new EJBException(ex.getMessage());

     	}


     }

     /**
      * @ejb:interface-method view-type="remote"
      * @jboss:method-attributes read-only="true"
      **/
     public byte getAdPrfArUseCustomerPulldown(Integer AD_CMPNY) {

     	Debug.print("ArCreditMemoEntryControllerBean getAdPrfArUseCustomerPulldown");

     	LocalAdPreferenceHome adPreferenceHome = null;

     	// Initialize EJB Home

     	try {

     		adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);

     	} catch (NamingException ex) {

     		throw new EJBException(ex.getMessage());

     	}


     	try {

     		LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);

     		return adPreference.getPrfArUseCustomerPulldown();

     	} catch (Exception ex) {

     		Debug.printStackTrace(ex);
     		throw new EJBException(ex.getMessage());

     	}

     }

     /**
      * @ejb:interface-method view-type="remote"
      */
     public String getInvItemClassByIiName(String II_NM, Integer AD_CMPNY)
     	throws GlobalNoRecordFoundException {

     	Debug.print("ArCreditMemoEntryControllerBean getInvItemClassByIiName");

     	LocalInvItemHome invItemHome = null;

     	try {
 	    	invItemHome = (LocalInvItemHome)EJBHomeFactory.
 	    	lookUpLocalHome(LocalInvItemHome.JNDI_NAME, LocalInvItemHome.class);
     	} catch(NamingException ex) {

     		throw new EJBException(ex.getMessage());

     	}

     	try {

 	    	try {

 	    		LocalInvItem invItem = invItemHome.findByIiName(II_NM, AD_CMPNY);

 	    		return invItem.getIiClass();

 	    	} catch(FinderException ex) {

 	    		throw new GlobalNoRecordFoundException();

 	    	}

     	} catch(GlobalNoRecordFoundException ex) {

     		throw ex;

     	}
     }

     /**
      * @ejb:interface-method view-type="remote"
      */
     public boolean getInvAutoBuildEnabledByIiName(String II_NM, Integer AD_CMPNY)
     	throws GlobalNoRecordFoundException {

     	Debug.print("ArCreditMemoEntryControllerBean getInvAutoBuildEnabledByIiCode");

     	LocalInvItemHome invItemHome = null;

     	try {
 	    	invItemHome = (LocalInvItemHome)EJBHomeFactory.
 	    	lookUpLocalHome(LocalInvItemHome.JNDI_NAME, LocalInvItemHome.class);
     	} catch(NamingException ex) {

     		throw new EJBException(ex.getMessage());

     	}

     	try {

 	    	try {

 	    		LocalInvItem invItem = invItemHome.findByIiName(II_NM, AD_CMPNY);

 	    		if(invItem.getIiEnableAutoBuild() == EJBCommon.TRUE)
 	    			return  true;

 	    		return false;

 	    	} catch(FinderException ex) {

 	    		throw new GlobalNoRecordFoundException();

 	    	}

     	} catch(GlobalNoRecordFoundException ex) {

     		throw ex;

     	}
     }

     /**
      * @ejb:interface-method view-type="remote"
      * @jboss:method-attributes read-only="true"
      **/
     public String getArCstCustomerCodeByArInvNmbr(String AR_INV_NMBR, Integer AD_BRNCH,Integer AD_CMPNY)
           throws GlobalNoRecordFoundException{
    	 Debug.print("ArCreditMemoEntryControllerBean getArCstCodeByArInvNmbr");

    	 LocalArInvoiceHome arInvoiceHome = null;

         // Initialize EJB Home

         try {

             arInvoiceHome = (LocalArInvoiceHome)EJBHomeFactory.
                 lookUpLocalHome(LocalArInvoiceHome.JNDI_NAME, LocalArInvoiceHome.class);

         } catch (NamingException ex) {

             throw new EJBException(ex.getMessage());

         }

         try {

          	LocalArInvoice arInvoice = null;


          	try {

          		arInvoice = arInvoiceHome.findByInvNumberAndInvCreditMemoAndBrCode(AR_INV_NMBR,EJBCommon.FALSE,AD_BRNCH, AD_CMPNY);

          		return arInvoice.getArCustomer().getCstCustomerCode();



          	} catch (FinderException ex) {

          		throw new GlobalNoRecordFoundException();

          	}



          } catch (GlobalNoRecordFoundException ex) {

          	throw ex;

          } catch (Exception ex) {

          	Debug.printStackTrace(ex);
          	throw new EJBException(ex.getMessage());

          }
         //findByInvNumberAndInvCreditMemoAndBrCode
     }




     /**
      * @ejb:interface-method view-type="remote"
      * @jboss:method-attributes read-only="true"
      **/
     public String getArCstNameByCstCustomerCode(String CST_CSTMR_CODE, Integer AD_CMPNY)
         throws GlobalNoRecordFoundException {

         Debug.print("ArCreditMemoEntryControllerBean getArCstNameByCstCustomerCode");

         LocalArCustomerHome arCustomerHome = null;

         // Initialize EJB Home

         try {

             arCustomerHome = (LocalArCustomerHome)EJBHomeFactory.
                 lookUpLocalHome(LocalArCustomerHome.JNDI_NAME, LocalArCustomerHome.class);

         } catch (NamingException ex) {

             throw new EJBException(ex.getMessage());

         }

         try {

         	LocalArCustomer arCustomer = null;


         	try {

         		arCustomer = arCustomerHome.findByCstCustomerCode(CST_CSTMR_CODE, AD_CMPNY);

         	} catch (FinderException ex) {

         		throw new GlobalNoRecordFoundException();

         	}

         	return arCustomer.getCstName();

         } catch (GlobalNoRecordFoundException ex) {

         	throw ex;

         } catch (Exception ex) {

         	Debug.printStackTrace(ex);
         	throw new EJBException(ex.getMessage());

         }

     }


    // private methods

    private void addArDrEntry(short DR_LN, String DR_CLSS,
	    byte DR_DBT, double DR_AMNT, Integer COA_CODE, LocalArInvoice arInvoice, Integer AD_BRNCH, Integer AD_CMPNY)
     	throws GlobalBranchAccountNumberInvalidException {

		Debug.print("ArCreditMemoEntryControllerBean addArDrEntry");

		LocalArDistributionRecordHome arDistributionRecordHome = null;
		LocalGlChartOfAccountHome glChartOfAccountHome = null;
		LocalAdCompanyHome adCompanyHome = null;


        // Initialize EJB Home

        try {

            arDistributionRecordHome = (LocalArDistributionRecordHome)EJBHomeFactory.
                lookUpLocalHome(LocalArDistributionRecordHome.JNDI_NAME, LocalArDistributionRecordHome.class);
            glChartOfAccountHome = (LocalGlChartOfAccountHome)EJBHomeFactory.
                lookUpLocalHome(LocalGlChartOfAccountHome.JNDI_NAME, LocalGlChartOfAccountHome.class);
            adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);


        } catch (NamingException ex) {

            throw new EJBException(ex.getMessage());

        }

        try {

        	// get company

        	LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);

       		LocalGlChartOfAccount glChartOfAccount = glChartOfAccountHome.findByCoaCodeAndBranchCode(COA_CODE, AD_BRNCH, AD_CMPNY);

		    // create distribution record

		    LocalArDistributionRecord arDistributionRecord = arDistributionRecordHome.create(
			    DR_LN, DR_CLSS, DR_DBT, EJBCommon.roundIt(DR_AMNT, adCompany.getGlFunctionalCurrency().getFcPrecision()),
			    EJBCommon.FALSE, EJBCommon.FALSE, AD_CMPNY);

			arInvoice.addArDistributionRecord(arDistributionRecord);
			glChartOfAccount.addArDistributionRecord(arDistributionRecord);

        } catch (FinderException ex) {

    		throw new GlobalBranchAccountNumberInvalidException(ex.getMessage());

    	} catch (Exception ex) {

        	Debug.printStackTrace(ex);
        	getSessionContext().setRollbackOnly();
        	throw new EJBException(ex.getMessage());

        }

	}

	private void executeArInvCreditMemoPost(Integer INV_CODE, String USR_NM, Integer AD_BRNCH, Integer AD_CMPNY) throws
        GlobalRecordAlreadyDeletedException,
		GlobalTransactionAlreadyPostedException,
		GlobalTransactionAlreadyVoidPostedException,
		GlJREffectiveDateNoPeriodExistException,
		GlJREffectiveDatePeriodClosedException,
		GlobalJournalNotBalanceException,
		AdPRFCoaGlVarianceAccountNotFoundException {

        Debug.print("ArCreditMemoEntryControllerBean executeArInvCreditMemoPost");

        LocalArInvoiceHome arInvoiceHome = null;
        LocalAdCompanyHome adCompanyHome = null;
        LocalAdPreferenceHome adPreferenceHome = null;
        LocalGlSetOfBookHome glSetOfBookHome = null;
        LocalGlAccountingCalendarValueHome glAccountingCalendarValueHome = null;
        LocalGlJournalHome glJournalHome = null;
        LocalGlJournalBatchHome glJournalBatchHome = null;
        LocalGlSuspenseAccountHome glSuspenseAccountHome = null;
        LocalGlJournalLineHome glJournalLineHome = null;
        LocalGlJournalSourceHome glJournalSourceHome = null;
        LocalGlJournalCategoryHome glJournalCategoryHome = null;
        LocalGlFunctionalCurrencyHome glFunctionalCurrencyHome = null;
        LocalArDistributionRecordHome arDistributionRecordHome = null;
        LocalInvCostingHome invCostingHome = null;
        LocalInvItemHome invItemHome = null;
        LocalGlChartOfAccountHome glChartOfAccountHome = null;

        LocalArInvoice arCreditMemo = null;
        LocalArInvoice arCreditedInvoice = null;

        // Initialize EJB Home

        try {

            arInvoiceHome = (LocalArInvoiceHome)EJBHomeFactory.
                lookUpLocalHome(LocalArInvoiceHome.JNDI_NAME, LocalArInvoiceHome.class);
            adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
            adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
            	lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);
            glSetOfBookHome = (LocalGlSetOfBookHome)EJBHomeFactory.
            	lookUpLocalHome(LocalGlSetOfBookHome.JNDI_NAME, LocalGlSetOfBookHome.class);
            glAccountingCalendarValueHome = (LocalGlAccountingCalendarValueHome)EJBHomeFactory.
            	lookUpLocalHome(LocalGlAccountingCalendarValueHome.JNDI_NAME, LocalGlAccountingCalendarValueHome.class);
            glJournalHome = (LocalGlJournalHome)EJBHomeFactory.
            	lookUpLocalHome(LocalGlJournalHome.JNDI_NAME, LocalGlJournalHome.class);
            glJournalBatchHome = (LocalGlJournalBatchHome)EJBHomeFactory.
            	lookUpLocalHome(LocalGlJournalBatchHome.JNDI_NAME, LocalGlJournalBatchHome.class);
            glSuspenseAccountHome = (LocalGlSuspenseAccountHome)EJBHomeFactory.
            	lookUpLocalHome(LocalGlSuspenseAccountHome.JNDI_NAME, LocalGlSuspenseAccountHome.class);
            glJournalLineHome = (LocalGlJournalLineHome)EJBHomeFactory.
            	lookUpLocalHome(LocalGlJournalLineHome.JNDI_NAME, LocalGlJournalLineHome.class);
            glJournalSourceHome = (LocalGlJournalSourceHome)EJBHomeFactory.
            	lookUpLocalHome(LocalGlJournalSourceHome.JNDI_NAME, LocalGlJournalSourceHome.class);
            glJournalCategoryHome = (LocalGlJournalCategoryHome)EJBHomeFactory.
            	lookUpLocalHome(LocalGlJournalCategoryHome.JNDI_NAME, LocalGlJournalCategoryHome.class);
            glFunctionalCurrencyHome = (LocalGlFunctionalCurrencyHome)EJBHomeFactory.
            	lookUpLocalHome(LocalGlFunctionalCurrencyHome.JNDI_NAME, LocalGlFunctionalCurrencyHome.class);
            arDistributionRecordHome = (LocalArDistributionRecordHome)EJBHomeFactory.
            	lookUpLocalHome(LocalArDistributionRecordHome.JNDI_NAME, LocalArDistributionRecordHome.class);
            invCostingHome = (LocalInvCostingHome)EJBHomeFactory.
            	lookUpLocalHome(LocalInvCostingHome.JNDI_NAME, LocalInvCostingHome.class);
            invItemHome = (LocalInvItemHome)EJBHomeFactory.
            	lookUpLocalHome(LocalInvItemHome.JNDI_NAME, LocalInvItemHome.class);
            glChartOfAccountHome = (LocalGlChartOfAccountHome)EJBHomeFactory.
                lookUpLocalHome(LocalGlChartOfAccountHome.JNDI_NAME, LocalGlChartOfAccountHome.class);

        } catch (NamingException ex) {

            throw new EJBException(ex.getMessage());

        }

        try {

        	LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
        	LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);

        	// validate if invoice/credit memo is already deleted

        	try {

        		arCreditMemo = arInvoiceHome.findByPrimaryKey(INV_CODE);

        	} catch (FinderException ex) {

        		throw new GlobalRecordAlreadyDeletedException();

        	}

        	// validate if invoice/credit memo is already posted or void

        	if (arCreditMemo.getInvVoid() == EJBCommon.FALSE && arCreditMemo.getInvPosted() == EJBCommon.TRUE) {

        		throw new GlobalTransactionAlreadyPostedException();

        	} else if (arCreditMemo.getInvVoid() == EJBCommon.TRUE && arCreditMemo.getInvVoidPosted() == EJBCommon.TRUE) {

        		throw new GlobalTransactionAlreadyVoidPostedException();
        	}

        	// regenerate inventory dr
        	//if (!arCreditMemo.getArInvoiceLineItems().isEmpty()) this.regenerateInventoryDr(arCreditMemo, AD_BRNCH, AD_CMPNY);


        	// post invoice/credit memo

        	if (arCreditMemo.getInvVoid() == EJBCommon.TRUE && arCreditMemo.getInvVoidPosted() == EJBCommon.FALSE) {
        		System.out.println("VOID CREDIT MEMO---------------------------->");

        		// get credited invoice

        		System.out.println("arCreditMemo.getInvCmInvoiceNumber()="+arCreditMemo.getInvCmInvoiceNumber());


        		arCreditedInvoice = arInvoiceHome.findByInvNumberAndInvCreditMemoAndBrCode(
        				arCreditMemo.getInvCmInvoiceNumber(), EJBCommon.FALSE, AD_BRNCH, AD_CMPNY);

        		// increase customer balance

        		double INV_AMNT = this.convertForeignToFunctionalCurrency(arCreditedInvoice.getGlFunctionalCurrency().getFcCode(),
        				arCreditedInvoice.getGlFunctionalCurrency().getFcName(),
						arCreditedInvoice.getInvConversionDate(), arCreditedInvoice.getInvConversionRate(),
						arCreditMemo.getInvAmountDue(), AD_CMPNY) ;

        		this.post(arCreditMemo.getInvDate(), INV_AMNT, arCreditMemo.getArCustomer(), AD_CMPNY);

        		// decrease invoice and ips amounts and release lock

        		double CREDIT_PERCENT = EJBCommon.roundIt(arCreditMemo.getInvAmountDue() / arCreditedInvoice.getInvAmountDue(), (short)6);

        		arCreditedInvoice.setInvAmountPaid(arCreditedInvoice.getInvAmountPaid() - arCreditMemo.getInvAmountDue());

        		double TOTAL_INVOICE_PAYMENT_SCHEDULE =  0d;

        		Collection arInvoicePaymentSchedules = arCreditedInvoice.getArInvoicePaymentSchedules();

        		Iterator i = arInvoicePaymentSchedules.iterator();

        		while (i.hasNext()) {

        			LocalArInvoicePaymentSchedule arInvoicePaymentSchedule =
        				(LocalArInvoicePaymentSchedule)i.next();

        			double INVOICE_PAYMENT_SCHEDULE_AMOUNT = 0;

        			// if last payment schedule subtract to avoid rounding difference error

        			if (i.hasNext()) {

        				INVOICE_PAYMENT_SCHEDULE_AMOUNT = EJBCommon.roundIt(arInvoicePaymentSchedule.getIpsAmountDue() * CREDIT_PERCENT, this.getGlFcPrecisionUnit(AD_CMPNY));

        			} else {

        				INVOICE_PAYMENT_SCHEDULE_AMOUNT = arCreditMemo.getInvAmountDue() - TOTAL_INVOICE_PAYMENT_SCHEDULE;

        			}

        			arInvoicePaymentSchedule.setIpsAmountPaid(arInvoicePaymentSchedule.getIpsAmountPaid() - INVOICE_PAYMENT_SCHEDULE_AMOUNT);

        			arInvoicePaymentSchedule.setIpsLock(EJBCommon.FALSE);

        			TOTAL_INVOICE_PAYMENT_SCHEDULE += INVOICE_PAYMENT_SCHEDULE_AMOUNT;

        		}

        		Collection arInvoiceLineItems = arCreditMemo.getArInvoiceLineItems();

        		if (arInvoiceLineItems != null && !arInvoiceLineItems.isEmpty()) {

        			Iterator c = arInvoiceLineItems.iterator();

        			while(c.hasNext()) {

        				LocalArInvoiceLineItem arInvoiceLineItem = (LocalArInvoiceLineItem) c.next();

    					String II_NM = arInvoiceLineItem.getInvItemLocation().getInvItem().getIiName();
    					String LOC_NM = arInvoiceLineItem.getInvItemLocation().getInvLocation().getLocName();

    					double QTY_SLD = this.convertByUomFromAndItemAndQuantity(arInvoiceLineItem.getInvUnitOfMeasure(),
    							arInvoiceLineItem.getInvItemLocation().getInvItem(), arInvoiceLineItem.getIliQuantity(), AD_CMPNY);

    					LocalInvCosting invCosting = null;

    					try {

    						invCosting  = invCostingHome.getByMaxCstDateToLongAndMaxCstLineNumberAndLessThanEqualCstDateAndIiNameAndLocName(arCreditMemo.getInvDate(), II_NM, LOC_NM, AD_BRNCH, AD_CMPNY);

    					} catch (FinderException ex) {

    					}

    					double COST = arInvoiceLineItem.getInvItemLocation().getInvItem().getIiUnitCost();

    					if (invCosting == null) {

    						this.postToInv(arInvoiceLineItem, arCreditMemo.getInvDate(),
    								QTY_SLD, COST * QTY_SLD,
    								-QTY_SLD, -COST * QTY_SLD,
    								0d, null, AD_BRNCH, AD_CMPNY);

    					} else {


    						if(invCosting.getInvItemLocation().getInvItem().getIiCostMethod().equals("Average")) {

    							double avgCost = invCosting.getCstRemainingQuantity() <= 0 ? COST :
    									Math.abs(invCosting.getCstRemainingValue() / invCosting.getCstRemainingQuantity());

                        		this.postToInv(arInvoiceLineItem, arCreditMemo.getInvDate(),
                        				QTY_SLD, avgCost * QTY_SLD,
                                		invCosting.getCstRemainingQuantity() - QTY_SLD, invCosting.getCstRemainingValue() - (avgCost * QTY_SLD),
                                		0d, null, AD_BRNCH, AD_CMPNY);

                            } else if(invCosting.getInvItemLocation().getInvItem().getIiCostMethod().equals("FIFO")) {

        	        			double fifoCost = invCosting.getCstRemainingQuantity() == 0 ? COST :
        	        				this.getInvFifoCost(invCosting.getCstDate(), invCosting.getInvItemLocation().getIlCode(),
        	        					QTY_SLD, arInvoiceLineItem.getIliUnitPrice(), true, AD_BRNCH, AD_CMPNY);


        	        			this.postToInv(arInvoiceLineItem, arCreditMemo.getInvDate(),
        	        					QTY_SLD, fifoCost * QTY_SLD,
        	        					invCosting.getCstRemainingQuantity() - QTY_SLD, invCosting.getCstRemainingValue() - (fifoCost * QTY_SLD),
        	        					0d, null, AD_BRNCH, AD_CMPNY);

                            } else if(invCosting.getInvItemLocation().getInvItem().getIiCostMethod().equals("Standard")) {

        	        			double standardCost = invCosting.getInvItemLocation().getInvItem().getIiUnitCost();


        	        			this.postToInv(arInvoiceLineItem, arCreditMemo.getInvDate(),
        	        					QTY_SLD, standardCost * QTY_SLD,
        	        					invCosting.getCstRemainingQuantity() - QTY_SLD, invCosting.getCstRemainingValue() - (standardCost * QTY_SLD),
        	        					0d, null, AD_BRNCH, AD_CMPNY);

                            }
    					}


        			}



        		}

        		// set cmAdjustment post status

  	           arCreditMemo.setInvVoidPosted(EJBCommon.TRUE);
  	           arCreditMemo.setInvPostedBy(USR_NM);
  	           arCreditMemo.setInvDatePosted(EJBCommon.getGcCurrentDateWoTime().getTime());


        	} else if(arCreditMemo.getInvVoid() == EJBCommon.FALSE && arCreditMemo.getInvPosted() == EJBCommon.FALSE) {
        		System.out.println("POST CREDIT MEMO---------------------------->");
        		// get credited invoice

        		arCreditedInvoice = arInvoiceHome.findByInvNumberAndInvCreditMemoAndBrCode(
        				arCreditMemo.getInvCmInvoiceNumber(), EJBCommon.FALSE, AD_BRNCH, AD_CMPNY);

        		// decrease customer balance

        		double INV_AMNT = this.convertForeignToFunctionalCurrency(arCreditedInvoice.getGlFunctionalCurrency().getFcCode(),
        				arCreditedInvoice.getGlFunctionalCurrency().getFcName(),
						arCreditedInvoice.getInvConversionDate(), arCreditedInvoice.getInvConversionRate(),
						arCreditMemo.getInvAmountDue(), AD_CMPNY) ;

        		this.post(arCreditMemo.getInvDate(), -INV_AMNT, arCreditMemo.getArCustomer(), AD_CMPNY);

        		// decrease invoice and ips amounts and release lock

        		double CREDIT_PERCENT = EJBCommon.roundIt(arCreditMemo.getInvAmountDue() / arCreditedInvoice.getInvAmountDue(), (short)6);

        		arCreditedInvoice.setInvAmountPaid(arCreditedInvoice.getInvAmountPaid() + arCreditMemo.getInvAmountDue());

        		double TOTAL_INVOICE_PAYMENT_SCHEDULE =  0d;

        		Collection arInvoicePaymentSchedules = arCreditedInvoice.getArInvoicePaymentSchedules();

        		Iterator i = arInvoicePaymentSchedules.iterator();

        		while (i.hasNext()) {

        			LocalArInvoicePaymentSchedule arInvoicePaymentSchedule =
        				(LocalArInvoicePaymentSchedule)i.next();

        			double INVOICE_PAYMENT_SCHEDULE_AMOUNT = 0;

        			// if last payment schedule subtract to avoid rounding difference error

        			if (i.hasNext()) {

        				INVOICE_PAYMENT_SCHEDULE_AMOUNT = EJBCommon.roundIt(arInvoicePaymentSchedule.getIpsAmountDue() * CREDIT_PERCENT, this.getGlFcPrecisionUnit(AD_CMPNY));

        			} else {

        				INVOICE_PAYMENT_SCHEDULE_AMOUNT = arCreditMemo.getInvAmountDue() - TOTAL_INVOICE_PAYMENT_SCHEDULE;

        			}

        			arInvoicePaymentSchedule.setIpsAmountPaid(arInvoicePaymentSchedule.getIpsAmountPaid() + INVOICE_PAYMENT_SCHEDULE_AMOUNT);

        			arInvoicePaymentSchedule.setIpsLock(EJBCommon.FALSE);

        			TOTAL_INVOICE_PAYMENT_SCHEDULE += INVOICE_PAYMENT_SCHEDULE_AMOUNT;

        		}

        		Collection arInvoiceLineItems = arCreditMemo.getArInvoiceLineItems();

        		if (arInvoiceLineItems != null && !arInvoiceLineItems.isEmpty()) {

        			Iterator c = arInvoiceLineItems.iterator();

        			while(c.hasNext()) {

        				LocalArInvoiceLineItem arInvoiceLineItem = (LocalArInvoiceLineItem) c.next();

        					String II_NM = arInvoiceLineItem.getInvItemLocation().getInvItem().getIiName();
        					String LOC_NM = arInvoiceLineItem.getInvItemLocation().getInvLocation().getLocName();

        					double QTY_SLD = this.convertByUomFromAndItemAndQuantity(arInvoiceLineItem.getInvUnitOfMeasure(),
        							arInvoiceLineItem.getInvItemLocation().getInvItem(), arInvoiceLineItem.getIliQuantity(), AD_CMPNY);

        					LocalInvCosting invCosting = null;

        					try {

        						invCosting  = invCostingHome.getByMaxCstDateToLongAndMaxCstLineNumberAndLessThanEqualCstDateAndIiNameAndLocName(arCreditMemo.getInvDate(), II_NM, LOC_NM, AD_BRNCH, AD_CMPNY);

        					} catch (FinderException ex) { }

        					double COST = arInvoiceLineItem.getInvItemLocation().getInvItem().getIiUnitCost();

        					if (invCosting == null) {


        						this.postToInv(arInvoiceLineItem, arCreditMemo.getInvDate(),
        								-QTY_SLD, -COST * QTY_SLD,
        								 QTY_SLD, COST * QTY_SLD,
        								 0d, null, AD_BRNCH, AD_CMPNY);

        					} else {


        						if(invCosting.getInvItemLocation().getInvItem().getIiCostMethod().equals("Average")) {

            						double avgCost = invCosting.getCstRemainingQuantity() <= 0 ? COST :
            								Math.abs(invCosting.getCstRemainingValue() / invCosting.getCstRemainingQuantity());

	            					this.postToInv(arInvoiceLineItem, arCreditMemo.getInvDate(),
	            							-QTY_SLD, -avgCost * QTY_SLD,
	            							invCosting.getCstRemainingQuantity() + QTY_SLD, invCosting.getCstRemainingValue() + (avgCost * QTY_SLD),
	            							0d, null, AD_BRNCH, AD_CMPNY);


        						} else if(invCosting.getInvItemLocation().getInvItem().getIiCostMethod().equals("FIFO")) {

        		        			double fifoCost = invCosting.getCstRemainingQuantity() == 0 ? COST :
        		        					this.getInvFifoCost(invCosting.getCstDate(), invCosting.getInvItemLocation().getIlCode(),
        		        					QTY_SLD, arInvoiceLineItem.getIliUnitPrice() * QTY_SLD, true, AD_BRNCH, AD_CMPNY);

        		        			//post entries to database
        		        			this.postToInv(arInvoiceLineItem, arCreditMemo.getInvDate(),
        		        					-QTY_SLD, -fifoCost * QTY_SLD,
        		        					invCosting.getCstRemainingQuantity() + QTY_SLD, invCosting.getCstRemainingValue() + (fifoCost * QTY_SLD),
        		        					0d, null, AD_BRNCH, AD_CMPNY);


        						} else if(invCosting.getInvItemLocation().getInvItem().getIiCostMethod().equals("Standard")) {
        		        			double standardCost = invCosting.getInvItemLocation().getInvItem().getIiUnitCost();

        		        			//post entries to database
        		        			this.postToInv(arInvoiceLineItem, arCreditMemo.getInvDate(),
        		        					-QTY_SLD, -standardCost * QTY_SLD,
        		        					invCosting.getCstRemainingQuantity() + QTY_SLD, invCosting.getCstRemainingValue() + (standardCost * QTY_SLD),
        		        					0d, null, AD_BRNCH, AD_CMPNY);
        						}
        					}

        			}

        		}

        	}

        	// set invoice post status

        	arCreditMemo.setInvPosted(EJBCommon.TRUE);
        	arCreditMemo.setInvPostedBy(USR_NM);
        	arCreditMemo.setInvDatePosted(EJBCommon.getGcCurrentDateWoTime().getTime());


        	// post to gl if necessary

        	if (adPreference.getPrfArGlPostingType().equals("AUTO-POST UPON APPROVAL")) {

        		// validate if date has no period and period is closed

        		LocalGlSetOfBook glJournalSetOfBook = null;

        		try {

        			glJournalSetOfBook = glSetOfBookHome.findByDate(arCreditMemo.getInvDate(), AD_CMPNY);

        		} catch (FinderException ex) {

        			throw new GlJREffectiveDateNoPeriodExistException();

        		}

        		LocalGlAccountingCalendarValue glAccountingCalendarValue =
        			glAccountingCalendarValueHome.findByAcCodeAndDate(
        					glJournalSetOfBook.getGlAccountingCalendar().getAcCode(), arCreditMemo.getInvDate(), AD_CMPNY);


        		if (glAccountingCalendarValue.getAcvStatus() == 'N' ||
        				glAccountingCalendarValue.getAcvStatus() == 'C' ||
						glAccountingCalendarValue.getAcvStatus() == 'P') {

        			throw new GlJREffectiveDatePeriodClosedException();

        		}

        		// check if invoice is balance if not check suspense posting

        		LocalGlJournalLine glOffsetJournalLine = null;

        		Collection arDistributionRecords = arDistributionRecordHome.findImportableDrByInvCode(arCreditMemo.getInvCode(), AD_CMPNY);

        		Iterator j = arDistributionRecords.iterator();

        		double TOTAL_DEBIT = 0d;
        		double TOTAL_CREDIT = 0d;

        		while (j.hasNext()) {

        			LocalArDistributionRecord arDistributionRecord = (LocalArDistributionRecord)j.next();

        			if(arDistributionRecord.getDrClass().equals("COGS") ||
        					arDistributionRecord.getDrClass().equals("INVENTORY")) continue;

        			double DR_AMNT = 0d;

        			if (arCreditMemo.getInvCreditMemo() == EJBCommon.FALSE) {

        				DR_AMNT = this.convertForeignToFunctionalCurrency(arCreditMemo.getGlFunctionalCurrency().getFcCode(),
        						arCreditMemo.getGlFunctionalCurrency().getFcName(),
        						arCreditMemo.getInvConversionDate(),
        						arCreditMemo.getInvConversionRate(),
								arDistributionRecord.getDrAmount(), AD_CMPNY);

        			} else {

        				DR_AMNT = this.convertForeignToFunctionalCurrency(arCreditedInvoice.getGlFunctionalCurrency().getFcCode(),
        						arCreditedInvoice.getGlFunctionalCurrency().getFcName(),
								arCreditedInvoice.getInvConversionDate(),
								arCreditedInvoice.getInvConversionRate(),
								arDistributionRecord.getDrAmount(), AD_CMPNY);

        			}

        			if (arDistributionRecord.getDrDebit() == EJBCommon.TRUE) {

        				TOTAL_DEBIT += DR_AMNT;

        			} else {

        				TOTAL_CREDIT += DR_AMNT;

        			}

        		}

        		TOTAL_DEBIT = EJBCommon.roundIt(TOTAL_DEBIT, adCompany.getGlFunctionalCurrency().getFcPrecision());
        		TOTAL_CREDIT = EJBCommon.roundIt(TOTAL_CREDIT, adCompany.getGlFunctionalCurrency().getFcPrecision());

        		if (adPreference.getPrfAllowSuspensePosting() == EJBCommon.TRUE &&
        				TOTAL_DEBIT != TOTAL_CREDIT) {

        			LocalGlSuspenseAccount glSuspenseAccount = null;

        			try {

        				glSuspenseAccount = glSuspenseAccountHome.findByJsNameAndJcName("ACCOUNTS RECEIVABLES", arCreditMemo.getInvCreditMemo() == EJBCommon.FALSE ? "SALES INVOICES" : "CREDIT MEMOS", AD_CMPNY);

        			} catch (FinderException ex) {

        				throw new GlobalJournalNotBalanceException();

        			}

        			if (TOTAL_DEBIT - TOTAL_CREDIT < 0) {

        				glOffsetJournalLine = glJournalLineHome.create((short)(arDistributionRecords.size() + 1), EJBCommon.TRUE,
								TOTAL_CREDIT - TOTAL_DEBIT, "", AD_CMPNY);

        			} else {

        				glOffsetJournalLine = glJournalLineHome.create((short)(arDistributionRecords.size() + 1), EJBCommon.FALSE,
        						TOTAL_DEBIT - TOTAL_CREDIT, "", AD_CMPNY);

        			}

        			LocalGlChartOfAccount glChartOfAccount = glSuspenseAccount.getGlChartOfAccount();
        			glChartOfAccount.addGlJournalLine(glOffsetJournalLine);


        		} else if (adPreference.getPrfAllowSuspensePosting() == EJBCommon.FALSE &&
        				TOTAL_DEBIT != TOTAL_CREDIT) {

        			throw new GlobalJournalNotBalanceException();

        		}

        		// create journal batch if necessary

        		LocalGlJournalBatch glJournalBatch = null;
        		java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat("MM/dd/yyyy");

        		try {

        			if (adPreference.getPrfEnableArInvoiceBatch() == EJBCommon.TRUE) {

        				glJournalBatch = glJournalBatchHome.findByJbName("JOURNAL IMPORT " + formatter.format(new Date()) + " " + arCreditMemo.getArInvoiceBatch().getIbName(), AD_BRNCH, AD_CMPNY);

        			} else {

        				glJournalBatch = glJournalBatchHome.findByJbName("JOURNAL IMPORT " + formatter.format(new Date()) + " SALES INVOICES", AD_BRNCH, AD_CMPNY);

        			}


        		} catch (FinderException ex) {
        		}

        		if (adPreference.getPrfEnableGlJournalBatch() ==  EJBCommon.TRUE &&
        				glJournalBatch == null) {

        			if (adPreference.getPrfEnableArInvoiceBatch() == EJBCommon.TRUE) {

        				glJournalBatch = glJournalBatchHome.create("JOURNAL IMPORT " + formatter.format(new Date()) + " " + arCreditMemo.getArInvoiceBatch().getIbName(), "JOURNAL IMPORT", "CLOSED", EJBCommon.getGcCurrentDateWoTime().getTime(), USR_NM, AD_BRNCH, AD_CMPNY);

        			} else {

        				glJournalBatch = glJournalBatchHome.create("JOURNAL IMPORT " + formatter.format(new Date()) + " SALES INVOICES", "JOURNAL IMPORT", "CLOSED", EJBCommon.getGcCurrentDateWoTime().getTime(), USR_NM, AD_BRNCH, AD_CMPNY);

        			}


        		}

        		// create journal entry
        		String journalDesc =  arCreditMemo.getInvVoid() == (byte) 1 ? " VOID" : " POST";

        		LocalGlJournal glJournal = glJournalHome.create(arCreditMemo.getInvCmInvoiceNumber(),
        				arCreditMemo.getInvDescription() + journalDesc, arCreditMemo.getInvDate(),
						0.0d, null, arCreditMemo.getInvNumber(), null, 1d, "N/A", null,
						'N', EJBCommon.TRUE, EJBCommon.FALSE,
						USR_NM, new Date(),
						USR_NM, new Date(),
						null, null,
						USR_NM, EJBCommon.getGcCurrentDateWoTime().getTime(),
						arCreditMemo.getArCustomer().getCstTin(), arCreditMemo.getArCustomer().getCstName(), EJBCommon.FALSE,
						null,
						AD_BRNCH, AD_CMPNY);

        		LocalGlJournalSource glJournalSource = glJournalSourceHome.findByJsName("ACCOUNTS RECEIVABLES", AD_CMPNY);
        		glJournal.setGlJournalSource(glJournalSource);

        		LocalGlFunctionalCurrency glFunctionalCurrency = glFunctionalCurrencyHome.findByFcName(adCompany.getGlFunctionalCurrency().getFcName(), AD_CMPNY);
        		glJournal.setGlFunctionalCurrency(glFunctionalCurrency);

        		LocalGlJournalCategory glJournalCategory = glJournalCategoryHome.findByJcName(arCreditMemo.getInvCreditMemo() == EJBCommon.FALSE ? "SALES INVOICES" : "CREDIT MEMOS", AD_CMPNY);
        		glJournal.setGlJournalCategory(glJournalCategory);


        		if (glJournalBatch != null) {

        			glJournal.setGlJournalBatch(glJournalBatch);

        		}


        		// create journal lines

        		j = arDistributionRecords.iterator();

        		while (j.hasNext()) {

        			LocalArDistributionRecord arDistributionRecord = (LocalArDistributionRecord)j.next();

        			double DR_AMNT = 0d;

        			if (arCreditMemo.getInvCreditMemo() == EJBCommon.FALSE) {

        				DR_AMNT = this.convertForeignToFunctionalCurrency(arCreditMemo.getGlFunctionalCurrency().getFcCode(),
        						arCreditMemo.getGlFunctionalCurrency().getFcName(),
        						arCreditMemo.getInvConversionDate(),
        						arCreditMemo.getInvConversionRate(),
								arDistributionRecord.getDrAmount(), AD_CMPNY);

        			} else {

        				DR_AMNT = this.convertForeignToFunctionalCurrency(arCreditedInvoice.getGlFunctionalCurrency().getFcCode(),
        						arCreditedInvoice.getGlFunctionalCurrency().getFcName(),
								arCreditedInvoice.getInvConversionDate(),
								arCreditedInvoice.getInvConversionRate(),
								arDistributionRecord.getDrAmount(), AD_CMPNY);

        			}

        			LocalGlJournalLine glJournalLine = glJournalLineHome.create(arDistributionRecord.getDrLine(),
							arDistributionRecord.getDrDebit(), DR_AMNT, "", AD_CMPNY);

        			arDistributionRecord.getGlChartOfAccount().addGlJournalLine(glJournalLine);

        			glJournal.addGlJournalLine(glJournalLine);

        			arDistributionRecord.setDrImported(EJBCommon.TRUE);


        		}

        		if (glOffsetJournalLine != null) {

        			glJournal.addGlJournalLine(glOffsetJournalLine);

        		}

        		// post journal to gl

        		Collection glJournalLines = glJournal.getGlJournalLines();

        		Iterator i = glJournalLines.iterator();

        		while (i.hasNext()) {

        			LocalGlJournalLine glJournalLine = (LocalGlJournalLine)i.next();

        			// post current to current acv

        			this.postToGl(glAccountingCalendarValue,
        					glJournalLine.getGlChartOfAccount(),
							true, glJournalLine.getJlDebit(), glJournalLine.getJlAmount(), AD_CMPNY);


        			// post to subsequent acvs (propagate)

        			Collection glSubsequentAccountingCalendarValues =
        				glAccountingCalendarValueHome.findSubsequentAcvByAcCodeAndAcvPeriodNumber(
        						glJournalSetOfBook.getGlAccountingCalendar().getAcCode(),
								glAccountingCalendarValue.getAcvPeriodNumber(), AD_CMPNY);

        			Iterator acvsIter = glSubsequentAccountingCalendarValues.iterator();

        			while (acvsIter.hasNext()) {

        				LocalGlAccountingCalendarValue glSubsequentAccountingCalendarValue =
        					(LocalGlAccountingCalendarValue)acvsIter.next();

        				this.postToGl(glSubsequentAccountingCalendarValue,
        						glJournalLine.getGlChartOfAccount(),
								false, glJournalLine.getJlDebit(), glJournalLine.getJlAmount(), AD_CMPNY);

        			}

        			// post to subsequent years if necessary

        			Collection glSubsequentSetOfBooks = glSetOfBookHome.findSubsequentSobByAcYear(glJournalSetOfBook.getGlAccountingCalendar().getAcYear(), AD_CMPNY);

        			if (!glSubsequentSetOfBooks.isEmpty() && glJournalSetOfBook.getSobYearEndClosed() == 1) {

        				adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
        				LocalGlChartOfAccount glRetainedEarningsAccount = glChartOfAccountHome.findByCoaAccountNumber(adCompany.getCmpRetainedEarnings(), AD_CMPNY);

        				Iterator sobIter = glSubsequentSetOfBooks.iterator();

        				while (sobIter.hasNext()) {

        					LocalGlSetOfBook glSubsequentSetOfBook = (LocalGlSetOfBook)sobIter.next();

        					String ACCOUNT_TYPE = glJournalLine.getGlChartOfAccount().getCoaAccountType();

        					// post to subsequent acvs of subsequent set of book(propagate)

        					Collection glAccountingCalendarValues =
        						glAccountingCalendarValueHome.findByAcCode(glSubsequentSetOfBook.getGlAccountingCalendar().getAcCode(), AD_CMPNY);

        					Iterator acvIter = glAccountingCalendarValues.iterator();

        					while (acvIter.hasNext()) {

        						LocalGlAccountingCalendarValue glSubsequentAccountingCalendarValue =
        							(LocalGlAccountingCalendarValue)acvIter.next();

        						if (ACCOUNT_TYPE.equals("ASSET") || ACCOUNT_TYPE.equals("LIABILITY") ||
        								ACCOUNT_TYPE.equals("OWNERS EQUITY")) {

        							this.postToGl(glSubsequentAccountingCalendarValue,
        									glJournalLine.getGlChartOfAccount(),
											false, glJournalLine.getJlDebit(), glJournalLine.getJlAmount(), AD_CMPNY);

        						} else { // revenue & expense

        							this.postToGl(glSubsequentAccountingCalendarValue,
        									glRetainedEarningsAccount,
											false, glJournalLine.getJlDebit(), glJournalLine.getJlAmount(), AD_CMPNY);

        						}

        					}

        					if (glSubsequentSetOfBook.getSobYearEndClosed() == 0) break;

        				}

        			}

        		}

        	}

        } catch (GlJREffectiveDateNoPeriodExistException ex) {

        	getSessionContext().setRollbackOnly();
        	throw ex;

        } catch (GlJREffectiveDatePeriodClosedException ex) {

        	getSessionContext().setRollbackOnly();
        	throw ex;

        } catch (GlobalJournalNotBalanceException ex) {

        	getSessionContext().setRollbackOnly();
        	throw ex;

        } catch (GlobalRecordAlreadyDeletedException ex) {

        	getSessionContext().setRollbackOnly();
        	throw ex;

        } catch (GlobalTransactionAlreadyPostedException ex) {

        	getSessionContext().setRollbackOnly();
        	throw ex;

        } catch (GlobalTransactionAlreadyVoidPostedException ex) {

        	getSessionContext().setRollbackOnly();
        	throw ex;

        } catch (AdPRFCoaGlVarianceAccountNotFoundException ex) {

        	getSessionContext().setRollbackOnly();
        	throw ex;

    	} catch (Exception ex) {

    		Debug.printStackTrace(ex);
    		getSessionContext().setRollbackOnly();
    		throw new EJBException(ex.getMessage());

    	}

    }


    private double getInvFifoCost(Date CST_DT, Integer IL_CODE, double CST_QTY, double CST_COST,
   		 boolean isAdjustFifo, Integer AD_BRNCH, Integer AD_CMPNY)
	 {

		 LocalInvCostingHome invCostingHome = null;
	  	 LocalInvItemLocationHome invItemLocationHome = null;

	     // Initialize EJB Home

	     try {

	    	 invCostingHome = (LocalInvCostingHome)EJBHomeFactory.
	    	 	lookUpLocalHome(LocalInvCostingHome.JNDI_NAME, LocalInvCostingHome.class);
	    	 invItemLocationHome = (LocalInvItemLocationHome)EJBHomeFactory.
	    	 	lookUpLocalHome(LocalInvItemLocationHome.JNDI_NAME, LocalInvItemLocationHome.class);
	     }
	     catch (NamingException ex) {

	    	 throw new EJBException(ex.getMessage());
	     }

		try {

			Collection invFifoCostings = invCostingHome.findFifoRemainingQuantityByLessThanOrEqualCstDateAndIlCodeAndBrCode(CST_DT, IL_CODE, AD_BRNCH, AD_CMPNY);

			if (invFifoCostings.size() > 0) {

				Iterator x = invFifoCostings.iterator();

	  			if (isAdjustFifo) {

	  				//executed during POST transaction

	  				double totalCost = 0d;
	  				double cost;

	  				if(CST_QTY < 0) {

	  					//for negative quantities
	 	  				double neededQty = -(CST_QTY);

	 	  				while(x.hasNext() && neededQty != 0) {

	 	 	  				LocalInvCosting invFifoCosting = (LocalInvCosting)x.next();

	 		 	  			if (invFifoCosting.getApPurchaseOrderLine() != null || invFifoCosting.getApVoucherLineItem() != null) {
	 		 	  				cost = invFifoCosting.getCstItemCost() / invFifoCosting.getCstQuantityReceived();
	 		 	  			} else if(invFifoCosting.getArInvoiceLineItem() != null) {
	 		 	  				cost = invFifoCosting.getCstCostOfSales() / invFifoCosting.getCstQuantitySold();
	 		 	  			} else if (invFifoCosting.getInvBuildUnbuildAssemblyLine() != null) {
	 		 	  				cost = invFifoCosting.getCstAssemblyCost() / invFifoCosting.getCstAssemblyQuantity();
	 		 	  			} else {
	 		 	  				cost = invFifoCosting.getCstAdjustCost() /invFifoCosting.getCstAdjustQuantity();
	 		 	  			}

	 	  					if(neededQty <= invFifoCosting.getCstRemainingLifoQuantity()) {

	 			  				invFifoCosting.setCstRemainingLifoQuantity(invFifoCosting.getCstRemainingLifoQuantity() - neededQty);
	 			  				totalCost += (neededQty * cost);
	 			  				neededQty = 0d;
	 	  					} else {

	 	  						neededQty -= invFifoCosting.getCstRemainingLifoQuantity();
	 	  						totalCost += (invFifoCosting.getCstRemainingLifoQuantity() * cost);
	 	  						invFifoCosting.setCstRemainingLifoQuantity(0);
	 	  					}
	 	  				}

	 	  				//if needed qty is not yet satisfied but no more quantities to fetch, get the default cost
	 	  				if(neededQty != 0) {

	 	  					LocalInvItemLocation invItemLocation = invItemLocationHome.findByPrimaryKey(IL_CODE);
	 	  					totalCost += (neededQty * invItemLocation.getInvItem().getIiUnitCost());
	 	  				}

	 	  				cost = totalCost / -CST_QTY;
	  				}

	  				else {

	  					//for positive quantities
	  					cost = CST_COST;
	  				}
	  				return cost;
	  			}

	  			else {

	  				//executed during ENTRY transaction

	  				LocalInvCosting invFifoCosting = (LocalInvCosting)x.next();

	 	  			if (invFifoCosting.getApPurchaseOrderLine() != null || invFifoCosting.getApVoucherLineItem() != null) {
	 	  				return EJBCommon.roundIt(invFifoCosting.getCstItemCost() / invFifoCosting.getCstQuantityReceived(), this.getGlFcPrecisionUnit(AD_CMPNY));
	 	  			} else if(invFifoCosting.getArInvoiceLineItem() != null) {
		 	  			return  EJBCommon.roundIt(invFifoCosting.getCstCostOfSales() / invFifoCosting.getCstQuantitySold(), this.getGlFcPrecisionUnit(AD_CMPNY));
	 	  			} else if (invFifoCosting.getInvBuildUnbuildAssemblyLine() != null) {
	 	  				return EJBCommon.roundIt(invFifoCosting.getCstAssemblyCost() / invFifoCosting.getCstAssemblyQuantity(), this.getGlFcPrecisionUnit(AD_CMPNY));
	 	  			} else {
	 	  				return EJBCommon.roundIt(invFifoCosting.getCstAdjustCost() /invFifoCosting.getCstAdjustQuantity(), this.getGlFcPrecisionUnit(AD_CMPNY));
	 	  			}
	  			}
			}
			else {

				//most applicable in 1st entries of data
				LocalInvItemLocation invItemLocation = invItemLocationHome.findByPrimaryKey(IL_CODE);
				return invItemLocation.getInvItem().getIiUnitCost();
			}

		}
		catch (Exception ex) {
			Debug.printStackTrace(ex);
		    throw new EJBException(ex.getMessage());
		}
	}


    private void post(Date INV_DT, double INV_AMNT, LocalArCustomer arCustomer, Integer AD_CMPNY) {

       Debug.print("ArCreditMemoEntryControllerBean post");

       LocalArCustomerBalanceHome arCustomerBalanceHome = null;

       // Initialize EJB Home

       try {

           arCustomerBalanceHome = (LocalArCustomerBalanceHome)EJBHomeFactory.
               lookUpLocalHome(LocalArCustomerBalanceHome.JNDI_NAME, LocalArCustomerBalanceHome.class);

       } catch (NamingException ex) {

           getSessionContext().setRollbackOnly();
           throw new EJBException(ex.getMessage());

       }

       try {

	       // find customer balance before or equal invoice date

	       Collection arCustomerBalances = arCustomerBalanceHome.findByBeforeOrEqualInvDateAndCstCustomerCode(INV_DT, arCustomer.getCstCustomerCode(), AD_CMPNY);

	       if (!arCustomerBalances.isEmpty()) {

	    	   // get last invoice

	    	   ArrayList arCustomerBalanceList = new ArrayList(arCustomerBalances);

	    	   LocalArCustomerBalance arCustomerBalance = (LocalArCustomerBalance)arCustomerBalanceList.get(arCustomerBalanceList.size() - 1);

	    	   if (arCustomerBalance.getCbDate().before(INV_DT)) {

	    	       // create new balance

	    	       LocalArCustomerBalance apNewCustomerBalance = arCustomerBalanceHome.create(
		    	       INV_DT, arCustomerBalance.getCbBalance() + INV_AMNT, AD_CMPNY);

		           arCustomer.addArCustomerBalance(apNewCustomerBalance);

	    	   } else { // equals to invoice date

	    	       arCustomerBalance.setCbBalance(arCustomerBalance.getCbBalance() + INV_AMNT);

	    	   }

	    	} else {

	    	    // create new balance

		    	LocalArCustomerBalance apNewCustomerBalance = arCustomerBalanceHome.create(
		    		INV_DT, INV_AMNT, AD_CMPNY);

		        arCustomer.addArCustomerBalance(apNewCustomerBalance);

	     	}

	     	// propagate to subsequent balances if necessary

	     	arCustomerBalances = arCustomerBalanceHome.findByAfterInvDateAndCstCustomerCode(INV_DT, arCustomer.getCstCustomerCode(), AD_CMPNY);

	     	Iterator i = arCustomerBalances.iterator();

	     	while (i.hasNext()) {

	     		LocalArCustomerBalance arCustomerBalance = (LocalArCustomerBalance)i.next();

	     		arCustomerBalance.setCbBalance(arCustomerBalance.getCbBalance() + INV_AMNT);

	     	}

        } catch (Exception ex) {

        	Debug.printStackTrace(ex);
        	getSessionContext().setRollbackOnly();
            throw new EJBException(ex.getMessage());

        }

	}

    private double convertForeignToFunctionalCurrency(Integer FC_CODE, String FC_NM,
	    Date CONVERSION_DATE, double CONVERSION_RATE, double AMOUNT, Integer AD_CMPNY) {

	    Debug.print("ArCreditMemoEntryControllerBean convertForeignToFunctionalCurrency");


        LocalGlFunctionalCurrencyRateHome glFunctionalCurrencyRateHome = null;
        LocalAdCompanyHome adCompanyHome = null;

        LocalAdCompany adCompany = null;

        // Initialize EJB Homes

        try {

            glFunctionalCurrencyRateHome = (LocalGlFunctionalCurrencyRateHome)EJBHomeFactory.
               lookUpLocalHome(LocalGlFunctionalCurrencyRateHome.JNDI_NAME, LocalGlFunctionalCurrencyRateHome.class);
            adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
               lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);

         } catch (NamingException ex) {

            throw new EJBException(ex.getMessage());

         }

         // get company and extended precision

         try {

             adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);

         } catch (Exception ex) {

             throw new EJBException(ex.getMessage());

         }


         // Convert to functional currency if necessary

         if (CONVERSION_RATE != 1 && CONVERSION_RATE != 0 ) {

             AMOUNT = AMOUNT / CONVERSION_RATE;

         }
         return EJBCommon.roundIt(AMOUNT, adCompany.getGlFunctionalCurrency().getFcPrecision());

	}

	private void addArDrIliEntry(short DR_LN, String DR_CLSS, byte DR_DBT, double DR_AMNT, Integer COA_CODE, LocalArInvoice arInvoice,
		Integer AD_BRNCH, Integer AD_CMPNY)
		throws GlobalBranchAccountNumberInvalidException {

    	Debug.print("ArInvoiceEntryControllerBean addArDrIliEntry");

    	LocalArDistributionRecordHome arDistributionRecordHome = null;
    	LocalGlChartOfAccountHome glChartOfAccountHome = null;
    	LocalAdCompanyHome adCompanyHome = null;


    	// Initialize EJB Home

    	try {

    		arDistributionRecordHome = (LocalArDistributionRecordHome)EJBHomeFactory.
				lookUpLocalHome(LocalArDistributionRecordHome.JNDI_NAME, LocalArDistributionRecordHome.class);
    		glChartOfAccountHome = (LocalGlChartOfAccountHome)EJBHomeFactory.
				lookUpLocalHome(LocalGlChartOfAccountHome.JNDI_NAME, LocalGlChartOfAccountHome.class);
    		adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);

    	} catch (NamingException ex) {

    		throw new EJBException(ex.getMessage());

    	}

    	try {

    		// get company

    		LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);

   			LocalGlChartOfAccount glChartOfAccount = glChartOfAccountHome.findByCoaCodeAndBranchCode(COA_CODE, AD_BRNCH, AD_CMPNY);

    		// create distribution record

    		LocalArDistributionRecord arDistributionRecord = arDistributionRecordHome.create(
    				DR_LN, DR_CLSS, DR_DBT, EJBCommon.roundIt(DR_AMNT, adCompany.getGlFunctionalCurrency().getFcPrecision()),
					EJBCommon.FALSE, EJBCommon.FALSE, AD_CMPNY);

    		arInvoice.addArDistributionRecord(arDistributionRecord);
    		glChartOfAccount.addArDistributionRecord(arDistributionRecord);

    	} catch (FinderException ex) {

			throw new GlobalBranchAccountNumberInvalidException(ex.getMessage());

		} catch (Exception ex) {

    		Debug.printStackTrace(ex);
    		getSessionContext().setRollbackOnly();
    		throw new EJBException(ex.getMessage());

    	}

    }

   private void postToGl(LocalGlAccountingCalendarValue glAccountingCalendarValue,
      LocalGlChartOfAccount glChartOfAccount,
      boolean isCurrentAcv, byte isDebit, double JL_AMNT, Integer AD_CMPNY) {

      Debug.print("ArCreditMemoEntryControllerBean postToGl");

      LocalGlChartOfAccountBalanceHome glChartOfAccountBalanceHome = null;
      LocalAdCompanyHome adCompanyHome = null;


       // Initialize EJB Home

       try {

           glChartOfAccountBalanceHome = (LocalGlChartOfAccountBalanceHome)EJBHomeFactory.
              lookUpLocalHome(LocalGlChartOfAccountBalanceHome.JNDI_NAME, LocalGlChartOfAccountBalanceHome.class);
           adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
              lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);


       } catch (NamingException ex) {

           throw new EJBException(ex.getMessage());

       }

       try {

               LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);

	       	   LocalGlChartOfAccountBalance glChartOfAccountBalance =
		            glChartOfAccountBalanceHome.findByAcvCodeAndCoaCode(
		               	  glAccountingCalendarValue.getAcvCode(),
		               	  glChartOfAccount.getCoaCode(), AD_CMPNY);

	           String ACCOUNT_TYPE = glChartOfAccount.getCoaAccountType();
	           short FC_EXTNDD_PRCSN = adCompany.getGlFunctionalCurrency().getFcPrecision();



	           if (((ACCOUNT_TYPE.equals("ASSET") || ACCOUNT_TYPE.equals("EXPENSE")) &&
				    isDebit == EJBCommon.TRUE) ||
				    (!ACCOUNT_TYPE.equals("ASSET") && !ACCOUNT_TYPE.equals("EXPENSE") &&
				    isDebit == EJBCommon.FALSE)) {

 			        glChartOfAccountBalance.setCoabEndingBalance(
				       EJBCommon.roundIt(glChartOfAccountBalance.getCoabEndingBalance() + JL_AMNT, FC_EXTNDD_PRCSN));

				    if (!isCurrentAcv) {

				    	glChartOfAccountBalance.setCoabBeginningBalance(
				       		EJBCommon.roundIt(glChartOfAccountBalance.getCoabBeginningBalance() + JL_AMNT, FC_EXTNDD_PRCSN));

					}


			  } else {

				    glChartOfAccountBalance.setCoabEndingBalance(
				       EJBCommon.roundIt(glChartOfAccountBalance.getCoabEndingBalance() - JL_AMNT, FC_EXTNDD_PRCSN));

				    if (!isCurrentAcv) {

				    	glChartOfAccountBalance.setCoabBeginningBalance(
				       		EJBCommon.roundIt(glChartOfAccountBalance.getCoabBeginningBalance() - JL_AMNT, FC_EXTNDD_PRCSN));

					}

		 	  }

		 	  if (isCurrentAcv) {

			 	 if (isDebit == EJBCommon.TRUE) {

		 			glChartOfAccountBalance.setCoabTotalDebit(
		 				EJBCommon.roundIt(glChartOfAccountBalance.getCoabTotalDebit() + JL_AMNT, FC_EXTNDD_PRCSN));

		 		 } else {

		 			glChartOfAccountBalance.setCoabTotalCredit(
		 				EJBCommon.roundIt(glChartOfAccountBalance.getCoabTotalCredit() + JL_AMNT, FC_EXTNDD_PRCSN));
		 		 }

		 	}

       } catch (Exception ex) {

       	   Debug.printStackTrace(ex);
       	   throw new EJBException(ex.getMessage());

       }


   }

   private LocalArInvoiceLineItem addArIliEntry(ArModInvoiceLineItemDetails mdetails, LocalArInvoice arInvoice, LocalArTaxCode arTaxCode, LocalInvItemLocation invItemLocation, LocalArInvoice arCreditedInvoice, Integer AD_BRNCH, Integer AD_CMPNY) {

		Debug.print("ArCreditMemoEntryControllerBean addArIliEntry");

		LocalArInvoiceLineItemHome arInvoiceLineItemHome = null;
		LocalInvItemLocationHome invItemLocationHome = null;
		LocalInvUnitOfMeasureHome invUnitOfMeasureHome = null;

        // Initialize EJB Home

        try {

            arInvoiceLineItemHome = (LocalArInvoiceLineItemHome)EJBHomeFactory.
                lookUpLocalHome(LocalArInvoiceLineItemHome.JNDI_NAME, LocalArInvoiceLineItemHome.class);
            invItemLocationHome = (LocalInvItemLocationHome)EJBHomeFactory.
                lookUpLocalHome(LocalInvItemLocationHome.JNDI_NAME, LocalInvItemLocationHome.class);
            invUnitOfMeasureHome = (LocalInvUnitOfMeasureHome)EJBHomeFactory.
            	lookUpLocalHome(LocalInvUnitOfMeasureHome.JNDI_NAME, LocalInvUnitOfMeasureHome.class);

        } catch (NamingException ex) {

            throw new EJBException(ex.getMessage());

        }

        try {

        	short precisionUnit = this.getGlFcPrecisionUnit(AD_CMPNY);

        	double ILI_AMNT = 0d;
        	double ILI_TAX_AMNT = 0d;
        	double ILI_DSCNT_1 = 0d;
        	double ILI_DSCNT_2 = 0d;
        	double ILI_DSCNT_3 = 0d;
        	double ILI_DSCNT_4 = 0d;
        	double ILI_TTL_DSCNT = 0d;

			// calculate net amount

			if (arTaxCode.getTcType().equals("INCLUSIVE")) {

				ILI_AMNT = EJBCommon.roundIt(mdetails.getIliAmount() / (1 + (arTaxCode.getTcRate() / 100)), precisionUnit);

	        } else {

	            // tax exclusive, none, zero rated or exempt

	            ILI_AMNT = mdetails.getIliAmount();

	    	}

        	// check discount details from credited invoice
        	// calculate discount, if applicable

        	Collection arCreditedInvoiceLineItems = arInvoiceLineItemHome.findByInvNumberAndInvCreditMemoAndInvPostedAndIlCodeAndBrCode(
        			arCreditedInvoice.getInvCode(), EJBCommon.FALSE, EJBCommon.TRUE, invItemLocation.getIlCode(), AD_BRNCH, AD_CMPNY);

        	Iterator i = arCreditedInvoiceLineItems.iterator();

        	if (i.hasNext()) {

        		LocalArInvoiceLineItem arCreditedInvoiceLineItem = (LocalArInvoiceLineItem) i.next();

        		ILI_DSCNT_1 = arCreditedInvoiceLineItem.getIliDiscount1();
        		ILI_DSCNT_2 = arCreditedInvoiceLineItem.getIliDiscount2();
        		ILI_DSCNT_3 = arCreditedInvoiceLineItem.getIliDiscount3();
        		ILI_DSCNT_4 = arCreditedInvoiceLineItem.getIliDiscount4();

        		ILI_TTL_DSCNT = (arCreditedInvoiceLineItem.getIliTotalDiscount() / arCreditedInvoiceLineItem.getIliQuantity()) * mdetails.getIliQuantity();

        	}
        	
        	System.out.println("AMOUNT LINE IS: "+ ILI_AMNT);
	    	// calculate tax

	    	if (!arTaxCode.getTcType().equals("NONE") &&
	    	    !arTaxCode.getTcType().equals("EXEMPT")) {

	    		if(mdetails.getIliTax()==EJBCommon.TRUE) {
	    			
	    			System.out.println("tax in " + ILI_AMNT + " got");
	    			if (arTaxCode.getTcType().equals("INCLUSIVE")) {

		        		ILI_TAX_AMNT = EJBCommon.roundIt(mdetails.getIliAmount() - ILI_AMNT, precisionUnit);

		        	} else if (arTaxCode.getTcType().equals("EXCLUSIVE")) {

		        		ILI_TAX_AMNT = EJBCommon.roundIt(mdetails.getIliAmount() * arTaxCode.getTcRate() / 100, precisionUnit);

		            } else {

		            	// tax none zero-rated or exempt

		        	}
	    		}



		    }

        	LocalArInvoiceLineItem arInvoiceLineItem = arInvoiceLineItemHome.create(
        		mdetails.getIliLine(), mdetails.getIliQuantity(), mdetails.getIliUnitPrice(),
        		ILI_AMNT, ILI_TAX_AMNT, mdetails.getIliEnableAutoBuild(), ILI_DSCNT_1,
        		ILI_DSCNT_2, ILI_DSCNT_3, ILI_DSCNT_4, ILI_TTL_DSCNT, mdetails.getIliTax(), AD_CMPNY);

            arInvoice.addArInvoiceLineItem(arInvoiceLineItem);

            
            arInvoiceLineItem.setIliTax(mdetails.getIliTax());
            invItemLocation.addArInvoiceLineItem(arInvoiceLineItem);

            LocalInvUnitOfMeasure invUnitOfMeasure = invUnitOfMeasureHome.findByUomName(mdetails.getIliUomName(), AD_CMPNY);
            invUnitOfMeasure.addArInvoiceLineItem(arInvoiceLineItem);

            //validate misc

            arInvoiceLineItem.setIliMisc(mdetails.getIliMisc());
        System.out.println("mdetails.getIliMisc() : "+mdetails.getIliMisc());
         	return arInvoiceLineItem;

        } catch (Exception ex) {

        	Debug.printStackTrace(ex);
        	getSessionContext().setRollbackOnly();
        	throw new EJBException(ex.getMessage());

        }

	}

   private void postToInv(LocalArInvoiceLineItem arInvoiceLineItem, Date CST_DT, double CST_QTY_SLD, double CST_CST_OF_SLS,
		   double CST_RMNNG_QTY, double CST_RMNNG_VL, double CST_VRNC_VL, String USR_NM, Integer AD_BRNCH, Integer AD_CMPNY) throws
		   AdPRFCoaGlVarianceAccountNotFoundException {

    	Debug.print("ArCreditMemoEntryControllerBean postToInv");

    	LocalInvCostingHome invCostingHome = null;
    	LocalAdPreferenceHome adPreferenceHome = null;
    	LocalAdCompanyHome adCompanyHome = null;
    	LocalInvAdjustmentLineHome invAdjustmentLineHome = null;

        // Initialize EJB Home

        try {

          invCostingHome = (LocalInvCostingHome)EJBHomeFactory.
              lookUpLocalHome(LocalInvCostingHome.JNDI_NAME, LocalInvCostingHome.class);
          adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
              lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);
          adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
              lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
          invAdjustmentLineHome = (LocalInvAdjustmentLineHome)EJBHomeFactory.
  	          lookUpLocalHome(LocalInvAdjustmentLineHome.JNDI_NAME, LocalInvAdjustmentLineHome.class);

        } catch (NamingException ex) {

          throw new EJBException(ex.getMessage());

        }

        try {

           LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);
           LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
           LocalInvItemLocation invItemLocation = arInvoiceLineItem.getInvItemLocation();
           int CST_LN_NMBR = 0;

           CST_QTY_SLD = EJBCommon.roundIt(CST_QTY_SLD, adPreference.getPrfInvQuantityPrecisionUnit());
           CST_CST_OF_SLS = EJBCommon.roundIt(CST_CST_OF_SLS, adCompany.getGlFunctionalCurrency().getFcPrecision());
           CST_RMNNG_QTY = EJBCommon.roundIt(CST_RMNNG_QTY, adPreference.getPrfInvQuantityPrecisionUnit());
           CST_RMNNG_VL = EJBCommon.roundIt(CST_RMNNG_VL, adCompany.getGlFunctionalCurrency().getFcPrecision());

           if (CST_QTY_SLD > 0 && arInvoiceLineItem.getIliEnableAutoBuild() == EJBCommon.FALSE) {

           	   invItemLocation.setIlCommittedQuantity(invItemLocation.getIlCommittedQuantity() - CST_QTY_SLD);

           }

           try {

           	   // generate line number

               LocalInvCosting invCurrentCosting = invCostingHome.getByMaxCstLineNumberAndCstDateToLongAndIiNameAndLocName(CST_DT.getTime(), invItemLocation.getInvItem().getIiName(), invItemLocation.getInvLocation().getLocName(), AD_BRNCH, AD_CMPNY);
               CST_LN_NMBR = invCurrentCosting.getCstLineNumber() + 1;

           } catch (FinderException ex) {

           	   CST_LN_NMBR = 1;

           }

           //void subsequent cost variance adjustments
           Collection invAdjustmentLines = invAdjustmentLineHome.findUnvoidAndIsCostVarianceGreaterThanAdjDateAndIlCodeAndBrCode(
           		CST_DT, invItemLocation.getIlCode(), AD_BRNCH, AD_CMPNY);
           Iterator i = invAdjustmentLines.iterator();

           while (i.hasNext()){

           	LocalInvAdjustmentLine invAdjustmentLine = (LocalInvAdjustmentLine) i.next();
           	this.voidInvAdjustment(invAdjustmentLine.getInvAdjustment(), AD_BRNCH, AD_CMPNY);

           }

           String prevExpiryDates = "";
           String miscListPrpgt ="";
           double qtyPrpgt = 0;
           try {
        	   LocalInvCosting prevCst = invCostingHome.getByMaxCstDateToLongAndMaxCstLineNumberAndLessThanEqualCstDateAndRemainingQuantityNotEqualToZeroAndIlCode(
        			   CST_DT, invItemLocation.getIlCode(), AD_BRNCH, AD_CMPNY);

        	   prevExpiryDates = prevCst.getCstExpiryDate();
        	   qtyPrpgt = prevCst.getCstRemainingQuantity();

        	   if (prevExpiryDates==null){
        		   prevExpiryDates="";
        	   }

           }catch (Exception ex){

           }

           System.out.println("INV COSTING CREATE CST_RMNNG_QTY="+CST_RMNNG_QTY);
           // create costing
           LocalInvCosting invCosting =
        		   invCostingHome.create(CST_DT, CST_DT.getTime(), CST_LN_NMBR, 0d, 0d, 0d, 0d, 0d, 0d, CST_QTY_SLD, CST_CST_OF_SLS, CST_RMNNG_QTY, CST_RMNNG_VL, 0d, 0d,  0d, AD_BRNCH, AD_CMPNY);
           //invItemLocation.addInvCosting(invCosting);
           invCosting.setArInvoiceLineItem(arInvoiceLineItem);
           invCosting.setInvItemLocation(invItemLocation);
//         Get Latest Expiry Dates

           /*
           if(prevExpiryDates!=null && prevExpiryDates!="" && prevExpiryDates.length()!=0){
			   System.out.println("apPurchaseOrderLine.getVliMisc(): "+arInvoiceLineItem.getIliMisc().length());
			   if(arInvoiceLineItem.getIliMisc()!=null && arInvoiceLineItem.getIliMisc()!="" && arInvoiceLineItem.getIliMisc().length()!=0){
				   int qty2Prpgt = Integer.parseInt(this.getQuantityExpiryDates(arInvoiceLineItem.getIliMisc()));
				   String miscList2Prpgt = this.propagateExpiryDates(arInvoiceLineItem.getIliMisc(), qty2Prpgt);

				   String propagateMiscPrpgt = miscList2Prpgt + prevExpiryDates;

				   invCosting.setCstExpiryDate(propagateMiscPrpgt);
			   }else{
				   invCosting.setCstExpiryDate(prevExpiryDates);
			   }


           }else{
        	   if(arInvoiceLineItem.getIliMisc()!=null && arInvoiceLineItem.getIliMisc()!="" && arInvoiceLineItem.getIliMisc().length()!=0){
        		   int initialQty = Integer.parseInt(this.getQuantityExpiryDates(arInvoiceLineItem.getIliMisc()));
    			   String initialPrpgt = this.propagateExpiryDates(arInvoiceLineItem.getIliMisc(), initialQty);

            	   invCosting.setCstExpiryDate(initialPrpgt);
        	   }else{
				   invCosting.setCstExpiryDate(prevExpiryDates);
			   }

           }
	*/
			// if cost variance is not 0, generate cost variance for the transaction
			if(CST_VRNC_VL != 0) {

				this.generateCostVariance(invCosting.getInvItemLocation(), CST_VRNC_VL,
						"ARCR" + arInvoiceLineItem.getArReceipt().getRctNumber(),
						arInvoiceLineItem.getArReceipt().getRctDescription(), arInvoiceLineItem.getArReceipt().getRctDate(),
						USR_NM, AD_BRNCH, AD_CMPNY);

			}

           // propagate balance if necessary
           Collection invCostings = invCostingHome.findByGreaterThanCstDateAndIiNameAndLocName(CST_DT, invItemLocation.getInvItem().getIiName(), invItemLocation.getInvLocation().getLocName(), AD_BRNCH, AD_CMPNY);

           i = invCostings.iterator();
           String miscList = "";
           /*
           if(arInvoiceLineItem.getIliMisc()!=null && arInvoiceLineItem.getIliMisc()!="" && arInvoiceLineItem.getIliMisc().length()!=0){
        	   double qty = Double.parseDouble(this.getQuantityExpiryDates(arInvoiceLineItem.getIliMisc()));
			   miscList = this.propagateExpiryDates(arInvoiceLineItem.getIliMisc(), qty);

			   System.out.println("miscList Propagate:" + miscList);
           }
           String propagateMisc ="";

           while (i.hasNext()) {

               LocalInvCosting invPropagatedCosting = (LocalInvCosting)i.next();

               invPropagatedCosting.setCstRemainingQuantity(invPropagatedCosting.getCstRemainingQuantity() - CST_QTY_SLD);
               invPropagatedCosting.setCstRemainingValue(invPropagatedCosting.getCstRemainingValue() - CST_CST_OF_SLS);
               if(arInvoiceLineItem.getIliMisc()!=null && arInvoiceLineItem.getIliMisc()!="" && arInvoiceLineItem.getIliMisc().length()!=0){
//                 String miscList2 = propagateMisc;//this.propagateExpiryDates(invCosting.getCstExpiryDate(), qty);
     			   System.out.println("invPropagatedCosting.getCstExpiryDate() : " + invPropagatedCosting.getCstExpiryDate());
     			   propagateMisc = invPropagatedCosting.getCstExpiryDate() + miscList;
               }else{
            	   propagateMisc = invPropagatedCosting.getCstExpiryDate() + miscList;
               }
               invPropagatedCosting.setCstExpiryDate(propagateMisc);
           }
	*/
			// regenerate cost variance
           this.regenerateCostVariance(invCostings, invCosting, AD_BRNCH, AD_CMPNY);

        } catch (AdPRFCoaGlVarianceAccountNotFoundException ex){

        	throw ex;

        } catch (Exception ex) {

      	   Debug.printStackTrace(ex);
      	   getSessionContext().setRollbackOnly();
           throw new EJBException(ex.getMessage());

        }

    }

   public String getQuantityExpiryDates(String qntty){
	   String separator = "$";

	   // Remove first $ character
	   qntty = qntty.substring(1);

	   // Counter
	   int start = 0;
	   int nextIndex = qntty.indexOf(separator, start);
	   int length = nextIndex - start;
	   String y;
	   y = (qntty.substring(start, start + length));
	   System.out.println("Y " + y);

	   return y;
   }

   public String propagateExpiryDates(String misc, double qty) throws Exception {
	   //ActionErrors errors = new ActionErrors();

	   Debug.print("ApReceivingItemControllerBean getExpiryDates");

	   String separator = "$";

	   // Remove first $ character
	   misc = misc.substring(1);

	   // Counter
	   int start = 0;
	   int nextIndex = misc.indexOf(separator, start);
	   int length = nextIndex - start;

	   System.out.println("qty" + qty);
	   String miscList = new String();

	   for(int x=0; x<qty; x++) {

		   // Date
		   start = nextIndex + 1;
		   nextIndex = misc.indexOf(separator, start);
		   length = nextIndex - start;
		   /*SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
	        sdf.setLenient(false);*/
		   try {

			   miscList = miscList + "$" +(misc.substring(start, start + length)+"$");
		   } catch (Exception ex) {

			   throw ex;
		   }


	   }

	   miscList = miscList+"$";
	   System.out.println("miscList :" + miscList);
	   return (miscList);
   }

   private void postToBua(LocalArInvoiceLineItem arInvoiceLineItem, Date CST_DT, double CST_ASSMBLY_QTY, double CST_ASSMBLY_CST,
		   double CST_RMNNG_QTY, double CST_RMNNG_VL, String II_NM, String LOC_NM, double CST_VRNC_VL, String USR_NM,
		   Integer AD_BRNCH, Integer AD_CMPNY) throws
		   AdPRFCoaGlVarianceAccountNotFoundException {

    	Debug.print("ArInvoiceEntryControllerBean postToBua");

    	LocalInvCostingHome invCostingHome = null;
    	LocalAdPreferenceHome adPreferenceHome = null;
    	LocalAdCompanyHome adCompanyHome = null;
    	LocalInvItemLocationHome invItemLocationHome = null;
    	LocalInvAdjustmentLineHome invAdjustmentLineHome = null;

        // Initialize EJB Home

        try {

          invCostingHome = (LocalInvCostingHome)EJBHomeFactory.
              lookUpLocalHome(LocalInvCostingHome.JNDI_NAME, LocalInvCostingHome.class);
          adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
              lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);
          adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
              lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
          invItemLocationHome = (LocalInvItemLocationHome)EJBHomeFactory.
              lookUpLocalHome(LocalInvItemLocationHome.JNDI_NAME, LocalInvItemLocationHome.class);
          invAdjustmentLineHome = (LocalInvAdjustmentLineHome)EJBHomeFactory.
	          lookUpLocalHome(LocalInvAdjustmentLineHome.JNDI_NAME, LocalInvAdjustmentLineHome.class);

        } catch (NamingException ex) {

          throw new EJBException(ex.getMessage());

        }

        try {

           LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);
           LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
           LocalInvItemLocation invItemLocation = invItemLocationHome.findByLocNameAndIiName(LOC_NM, II_NM, AD_CMPNY);
           int CST_LN_NMBR = 0;

           CST_ASSMBLY_QTY = EJBCommon.roundIt(CST_ASSMBLY_QTY, adPreference.getPrfInvQuantityPrecisionUnit());
           CST_ASSMBLY_CST = EJBCommon.roundIt(CST_ASSMBLY_CST, adCompany.getGlFunctionalCurrency().getFcPrecision());
           CST_RMNNG_QTY = EJBCommon.roundIt(CST_RMNNG_QTY, adPreference.getPrfInvQuantityPrecisionUnit());
           CST_RMNNG_VL = EJBCommon.roundIt(CST_RMNNG_VL, adCompany.getGlFunctionalCurrency().getFcPrecision());

           if ((CST_ASSMBLY_QTY < 0 && arInvoiceLineItem.getIliEnableAutoBuild() == 0) ||
           		(CST_ASSMBLY_QTY < 0 && arInvoiceLineItem.getIliEnableAutoBuild() == 1 && !arInvoiceLineItem.getInvItemLocation().getInvItem().equals(invItemLocation.getInvItem()))) {

           	   invItemLocation.setIlCommittedQuantity(invItemLocation.getIlCommittedQuantity() - Math.abs(CST_ASSMBLY_QTY));

           }

           try {

           	   // generate line number
               LocalInvCosting invCurrentCosting = invCostingHome.getByMaxCstLineNumberAndCstDateToLongAndIiNameAndLocName(CST_DT.getTime(), invItemLocation.getInvItem().getIiName(), invItemLocation.getInvLocation().getLocName(), AD_BRNCH, AD_CMPNY);
               CST_LN_NMBR = invCurrentCosting.getCstLineNumber() + 1;

           } catch (FinderException ex) {

           	   CST_LN_NMBR = 1;

           }

           //void subsequent cost variance adjustments
           Collection invAdjustmentLines = invAdjustmentLineHome.findUnvoidAndIsCostVarianceGreaterThanAdjDateAndIlCodeAndBrCode(
           		CST_DT, invItemLocation.getIlCode(), AD_BRNCH, AD_CMPNY);
           Iterator i = invAdjustmentLines.iterator();

           while (i.hasNext()){

           	LocalInvAdjustmentLine invAdjustmentLine = (LocalInvAdjustmentLine) i.next();
           	this.voidInvAdjustment(invAdjustmentLine.getInvAdjustment(), AD_BRNCH, AD_CMPNY);

           }

           // create costing
           LocalInvCosting invCosting = invCostingHome.create(CST_DT, CST_DT.getTime(), CST_LN_NMBR, 0d, 0d, CST_ASSMBLY_QTY, CST_ASSMBLY_CST, 0d, 0d, 0d, 0d, CST_RMNNG_QTY, CST_RMNNG_VL, 0d, 0d, CST_ASSMBLY_QTY > 0 ? CST_ASSMBLY_QTY : 0, AD_BRNCH, AD_CMPNY);
           invItemLocation.addInvCosting(invCosting);
           invCosting.setArInvoiceLineItem(arInvoiceLineItem);

			// if cost variance is not 0, generate cost variance for the transaction
			if(CST_VRNC_VL != 0) {

				this.generateCostVariance(invCosting.getInvItemLocation(), CST_VRNC_VL,
						"ARCR" + arInvoiceLineItem.getArReceipt().getRctNumber(),
						arInvoiceLineItem.getArReceipt().getRctDescription(), arInvoiceLineItem.getArReceipt().getRctDate(),
						USR_NM, AD_BRNCH, AD_CMPNY);

			}

           // propagate balance if necessary
           Collection invCostings = invCostingHome.findByGreaterThanCstDateAndIiNameAndLocName(CST_DT, invItemLocation.getInvItem().getIiName(), invItemLocation.getInvLocation().getLocName(), AD_BRNCH, AD_CMPNY);

           i = invCostings.iterator();

           while (i.hasNext()) {

               LocalInvCosting invPropagatedCosting = (LocalInvCosting)i.next();

               invPropagatedCosting.setCstRemainingQuantity(invPropagatedCosting.getCstRemainingQuantity() + CST_ASSMBLY_QTY);
               invPropagatedCosting.setCstRemainingValue(invPropagatedCosting.getCstRemainingValue() + CST_ASSMBLY_CST);

           }

           // regenerate cost variance
           this.regenerateCostVariance(invCostings, invCosting, AD_BRNCH, AD_CMPNY);

       } catch (AdPRFCoaGlVarianceAccountNotFoundException ex){

       	throw ex;

        } catch (Exception ex) {

      	   Debug.printStackTrace(ex);
      	   getSessionContext().setRollbackOnly();
           throw new EJBException(ex.getMessage());

        }



    }

    private double convertByUomFromAndItemAndQuantity(LocalInvUnitOfMeasure invFromUnitOfMeasure, LocalInvItem invItem, double QTY_SLD, Integer AD_CMPNY) {

		Debug.print("ArCreditMemoEntryControllerBean convertByUomFromAndItemAndQuantity");

		LocalAdPreferenceHome adPreferenceHome = null;
		LocalInvUnitOfMeasureConversionHome invUnitOfMeasureConversionHome = null;

        // Initialize EJB Home

        try {

            adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);
            invUnitOfMeasureConversionHome = (LocalInvUnitOfMeasureConversionHome)EJBHomeFactory.
        		lookUpLocalHome(LocalInvUnitOfMeasureConversionHome.JNDI_NAME, LocalInvUnitOfMeasureConversionHome.class);

        } catch (NamingException ex) {

            throw new EJBException(ex.getMessage());

        }

        try {

            LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);

            LocalInvUnitOfMeasureConversion invUnitOfMeasureConversion = invUnitOfMeasureConversionHome.findUmcByIiNameAndUomName(invItem.getIiName(), invFromUnitOfMeasure.getUomName(), AD_CMPNY);
            LocalInvUnitOfMeasureConversion invDefaultUomConversion = invUnitOfMeasureConversionHome.findUmcByIiNameAndUomName(invItem.getIiName(), invItem.getInvUnitOfMeasure().getUomName(), AD_CMPNY);

        	return EJBCommon.roundIt(QTY_SLD * invDefaultUomConversion.getUmcConversionFactor() / invUnitOfMeasureConversion.getUmcConversionFactor(), adPreference.getPrfInvQuantityPrecisionUnit());

        } catch (Exception ex) {

        	Debug.printStackTrace(ex);
        	getSessionContext().setRollbackOnly();
        	throw new EJBException(ex.getMessage());

        }

	}

    private void regenerateInventoryDr(LocalArInvoice arInvoice, Integer AD_BRNCH, Integer AD_CMPNY)
    throws GlobalInventoryDateException, GlobalBranchAccountNumberInvalidException {

    	Debug.print("ArCreditMemoEntryControllerBean regenerateInventoryDr");

    	LocalArDistributionRecordHome arDistributionRecordHome = null;
    	LocalGlChartOfAccountHome glChartOfAccountHome = null;
    	LocalInvCostingHome invCostingHome = null;
    	LocalInvItemHome invItemHome = null;
    	LocalInvItemLocationHome invItemLocationHome = null;
    	LocalAdPreferenceHome adPreferenceHome = null;

    	// Initialize EJB Home

    	try {

    		arDistributionRecordHome = (LocalArDistributionRecordHome)EJBHomeFactory.
			lookUpLocalHome(LocalArDistributionRecordHome.JNDI_NAME, LocalArDistributionRecordHome.class);
    		glChartOfAccountHome = (LocalGlChartOfAccountHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlChartOfAccountHome.JNDI_NAME, LocalGlChartOfAccountHome.class);
    		invCostingHome = (LocalInvCostingHome)EJBHomeFactory.
			lookUpLocalHome(LocalInvCostingHome.JNDI_NAME, LocalInvCostingHome.class);
    		invItemHome = (LocalInvItemHome)EJBHomeFactory.
			lookUpLocalHome(LocalInvItemHome.JNDI_NAME, LocalInvItemHome.class);
    		invItemLocationHome = (LocalInvItemLocationHome)EJBHomeFactory.
			lookUpLocalHome(LocalInvItemLocationHome.JNDI_NAME, LocalInvItemLocationHome.class);
    		adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
    		lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);

    	} catch (NamingException ex) {

    		throw new EJBException(ex.getMessage());

    	}

    	try {

    		LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);

    		// regenerate inventory distribution records

    		Collection arDistributionRecords = arDistributionRecordHome.findImportableDrByInvCode(arInvoice.getInvCode(), AD_CMPNY);

    		Iterator i = arDistributionRecords.iterator();

    		while (i.hasNext()) {

    			LocalArDistributionRecord arDistributionRecord = (LocalArDistributionRecord)i.next();

    			if(arDistributionRecord.getDrClass().equals("COGS") || arDistributionRecord.getDrClass().equals("INVENTORY")){

    				i.remove();
    				arDistributionRecord.remove();

    			}

    		}

    		Collection arInvoiceLineItems = arInvoice.getArInvoiceLineItems();

    		if(arInvoiceLineItems != null && !arInvoiceLineItems.isEmpty()) {

    			i = arInvoiceLineItems.iterator();

    			while(i.hasNext()) {

    				LocalArInvoiceLineItem arInvoiceLineItem = (LocalArInvoiceLineItem)i.next();
    				LocalInvItemLocation invItemLocation = arInvoiceLineItem.getInvItemLocation();

    				if (adPreference.getPrfArAllowPriorDate() == EJBCommon.FALSE) {
    					Collection invNegTxnCosting = invCostingHome.findNegTxnByGreaterThanCstDateAndIiNameAndLocName(
    							arInvoice.getInvDate(), invItemLocation.getInvItem().getIiName(),
    							invItemLocation.getInvLocation().getLocName(), AD_BRNCH, AD_CMPNY);
    					if(!invNegTxnCosting.isEmpty()) throw new GlobalInventoryDateException(invItemLocation.getInvItem().getIiName());
    				}
    				// add cost of sales distribution and inventory

    				double COST = arInvoiceLineItem.getInvItemLocation().getInvItem().getIiUnitCost();

    				try {

    					LocalInvCosting invCosting  = invCostingHome.getByMaxCstDateToLongAndMaxCstLineNumberAndLessThanEqualCstDateAndIiNameAndLocName(
    						arInvoice.getInvDate(), invItemLocation.getInvItem().getIiName(), invItemLocation.getInvLocation().getLocName(), AD_BRNCH, AD_CMPNY);


    					if(invCosting.getInvItemLocation().getInvItem().getIiCostMethod().equals("Average"))

 	  	    				COST = invCosting.getCstRemainingQuantity() <= 0 ? COST :
 	  	    					Math.abs(invCosting.getCstRemainingValue() / invCosting.getCstRemainingQuantity());

                            if(COST<=0){
                                COST = arInvoiceLineItem.getInvItemLocation().getInvItem().getIiUnitCost();
                            }

 	  	    			else if(invCosting.getInvItemLocation().getInvItem().getIiCostMethod().equals("FIFO"))

 	  	    				COST =  invCosting.getCstRemainingQuantity() == 0 ? COST :
 	  	    					Math.abs(this.getInvFifoCost(invCosting.getCstDate(), invCosting.getInvItemLocation().getIlCode(),
 	  	    						arInvoiceLineItem.getIliQuantity(), arInvoiceLineItem.getIliUnitPrice(), false, AD_BRNCH, AD_CMPNY));

 	  	    			else if(invCosting.getInvItemLocation().getInvItem().getIiCostMethod().equals("Standard"))

 	  	    				COST = invCosting.getInvItemLocation().getInvItem().getIiUnitCost();

    				} catch (FinderException ex) { 
                         COST = arInvoiceLineItem.getInvItemLocation().getInvItem().getIiUnitCost();
                    }

    				double QTY_SLD = this.convertByUomFromAndItemAndQuantity(arInvoiceLineItem.getInvUnitOfMeasure(),
    						arInvoiceLineItem.getInvItemLocation().getInvItem(), arInvoiceLineItem.getIliQuantity(), AD_CMPNY);

    				if (arInvoiceLineItem.getIliEnableAutoBuild() == EJBCommon.FALSE &&
    						arInvoiceLineItem.getInvItemLocation().getInvItem().getIiNonInventoriable() == EJBCommon.FALSE) {



    						this.addArDrIliEntry(arInvoice.getArDrNextLine(),
    								"COGS", EJBCommon.FALSE, COST * QTY_SLD,
									arInvoiceLineItem.getInvItemLocation().getIlGlCoaCostOfSalesAccount(), arInvoice, AD_BRNCH, AD_CMPNY);

    						this.addArDrIliEntry(arInvoice.getArDrNextLine(),
    								"INVENTORY", EJBCommon.TRUE, COST * QTY_SLD,
									arInvoiceLineItem.getInvItemLocation().getIlGlCoaInventoryAccount(), arInvoice, AD_BRNCH, AD_CMPNY);


    				}



    			}
    		}

    	} catch (GlobalInventoryDateException ex) {

    		getSessionContext().setRollbackOnly();
    		throw ex;

    	} catch (GlobalBranchAccountNumberInvalidException ex) {

    		getSessionContext().setRollbackOnly();
    		throw ex;

    	} catch (Exception ex) {

    		Debug.printStackTrace(ex);
    		getSessionContext().setRollbackOnly();
    		throw new EJBException(ex.getMessage());

    	}

    }

    private double convertCostByUom(String II_NM, String UOM_NM, double unitCost, boolean isFromDefault, Integer AD_CMPNY) {

        Debug.print("ArInvoiceEntryControllerBean convertCostByUom");

        LocalInvItemHome invItemHome = null;
        LocalInvUnitOfMeasureConversionHome invUnitOfMeasureConversionHome = null;

        // Initialize EJB Home

        try {

            invUnitOfMeasureConversionHome = (LocalInvUnitOfMeasureConversionHome)EJBHomeFactory.
            lookUpLocalHome(LocalInvUnitOfMeasureConversionHome.JNDI_NAME, LocalInvUnitOfMeasureConversionHome.class);
            invItemHome = (LocalInvItemHome)EJBHomeFactory.
            lookUpLocalHome(LocalInvItemHome.JNDI_NAME, LocalInvItemHome.class);

        } catch (NamingException ex) {

            throw new EJBException(ex.getMessage());

        }

        try {

            LocalInvItem invItem = invItemHome.findByIiName(II_NM, AD_CMPNY);
            LocalInvUnitOfMeasureConversion invUnitOfMeasureConversion = invUnitOfMeasureConversionHome.findUmcByIiNameAndUomName(II_NM, UOM_NM, AD_CMPNY);
            LocalInvUnitOfMeasureConversion invDefaultUomConversion = invUnitOfMeasureConversionHome.findUmcByIiNameAndUomName(II_NM, invItem.getInvUnitOfMeasure().getUomName(), AD_CMPNY);

            if (isFromDefault) {

                return unitCost * invDefaultUomConversion.getUmcConversionFactor() / invUnitOfMeasureConversion.getUmcConversionFactor();

            } else {

                return unitCost * invUnitOfMeasureConversion.getUmcConversionFactor() / invDefaultUomConversion.getUmcConversionFactor();

            }

        } catch (Exception ex) {

            Debug.printStackTrace(ex);
            getSessionContext().setRollbackOnly();
            throw new EJBException(ex.getMessage());

        }

    }

    private void voidInvAdjustment(LocalInvAdjustment invAdjustment, Integer AD_BRNCH, Integer AD_CMPNY){

    	Debug.print("ArMiscReceiptEntryControllerBean voidInvAdjustment");

    	try{

    			Collection invDistributionRecords = invAdjustment.getInvDistributionRecords();
    			ArrayList list = new ArrayList();

    			Iterator i = invDistributionRecords.iterator();

    			while (i.hasNext()) {

    				LocalInvDistributionRecord invDistributionRecord = (LocalInvDistributionRecord)i.next();

    				list.add(invDistributionRecord);

    			}

    			i = list.iterator();

    			while (i.hasNext()) {

    				LocalInvDistributionRecord invDistributionRecord = (LocalInvDistributionRecord)i.next();

    				this.addInvDrEntry(invAdjustment.getInvDrNextLine(), invDistributionRecord.getDrClass(),
    						invDistributionRecord.getDrDebit() == EJBCommon.TRUE ? EJBCommon.FALSE : EJBCommon.TRUE,
    								invDistributionRecord.getDrAmount(), EJBCommon.TRUE,
									invDistributionRecord.getInvChartOfAccount().getCoaCode(), invAdjustment, AD_BRNCH, AD_CMPNY);

    			}

    			Collection invAdjustmentLines = invAdjustment.getInvAdjustmentLines();
    			i = invAdjustmentLines.iterator();
    			list.clear();

    			while (i.hasNext()) {

    				LocalInvAdjustmentLine invAdjustmentLine = (LocalInvAdjustmentLine)i.next();

    				list.add(invAdjustmentLine);

    			}

    			i = list.iterator();

    			while (i.hasNext()) {

    				LocalInvAdjustmentLine invAdjustmentLine = (LocalInvAdjustmentLine)i.next();

    				this.addInvAlEntry(invAdjustmentLine.getInvItemLocation(),
    						invAdjustment, (invAdjustmentLine.getAlUnitCost()) * - 1, EJBCommon.TRUE, AD_CMPNY);

    			}

    			invAdjustment.setAdjVoid(EJBCommon.TRUE);

    			this.executeInvAdjPost(invAdjustment.getAdjCode(), invAdjustment.getAdjLastModifiedBy(), AD_BRNCH, AD_CMPNY);

    	} catch (Exception ex) {

    		Debug.printStackTrace(ex);
    		getSessionContext().setRollbackOnly();
    		throw new EJBException(ex.getMessage());

    	}

    }

    private void generateCostVariance(LocalInvItemLocation invItemLocation, double CST_VRNC_VL, String ADJ_RFRNC_NMBR, String ADJ_DSCRPTN, Date ADJ_DT, String USR_NM, Integer AD_BRNCH,
			Integer AD_CMPNY) throws
			AdPRFCoaGlVarianceAccountNotFoundException {
    	/*
    	Debug.print("ArMiscReceiptEntryControllerBean generateCostVariance");

    	LocalAdPreferenceHome adPreferenceHome = null;
    	LocalGlChartOfAccountHome glChartOfAccountHome = null;
    	LocalAdBranchItemLocationHome adBranchItemLocationHome = null;

    	// Initialize EJB Home

    	try {

    		adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);
    		glChartOfAccountHome = (LocalGlChartOfAccountHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlChartOfAccountHome.JNDI_NAME, LocalGlChartOfAccountHome.class);
    		adBranchItemLocationHome = (LocalAdBranchItemLocationHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdBranchItemLocationHome.JNDI_NAME, LocalAdBranchItemLocationHome.class);


    	} catch (NamingException ex) {

    		throw new EJBException(ex.getMessage());

    	}

    	try{

    		LocalInvAdjustment newInvAdjustment = this.saveInvAdjustment(ADJ_RFRNC_NMBR, ADJ_DSCRPTN, ADJ_DT, USR_NM, AD_BRNCH,
    				AD_CMPNY);
    		LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);
    		LocalGlChartOfAccount glCoaVarianceAccount = null;

    		if(adPreference.getPrfInvGlCoaVarianceAccount() == null)
    			throw new AdPRFCoaGlVarianceAccountNotFoundException();

    		try{

    			glCoaVarianceAccount = glChartOfAccountHome.findByPrimaryKey(adPreference.getPrfInvGlCoaVarianceAccount());
    			glCoaVarianceAccount.addInvAdjustment(newInvAdjustment);

    		} catch (FinderException ex) {

    			throw new AdPRFCoaGlVarianceAccountNotFoundException();

    		}

    		LocalInvAdjustmentLine invAdjustmentLine = this.addInvAlEntry(invItemLocation, newInvAdjustment, CST_VRNC_VL,
    				EJBCommon.FALSE, AD_CMPNY);

    		// check for branch mapping

    		LocalAdBranchItemLocation adBranchItemLocation = null;

    		try{

    			adBranchItemLocation = adBranchItemLocationHome.findBilByIlCodeAndBrCode(
    					invAdjustmentLine.getInvItemLocation().getIlCode(), AD_BRNCH, AD_CMPNY);

    		} catch (FinderException ex) {

    		}

    		LocalGlChartOfAccount glInventoryChartOfAccount = null;

    		if (adBranchItemLocation == null) {

    			glInventoryChartOfAccount = glChartOfAccountHome.findByPrimaryKey(
    					invAdjustmentLine.getInvItemLocation().getIlGlCoaInventoryAccount());
    		} else {

    			glInventoryChartOfAccount = glChartOfAccountHome.findByPrimaryKey(
    					adBranchItemLocation.getBilCoaGlInventoryAccount());

    		}


    		boolean isDebit = CST_VRNC_VL < 0 ? false : true;

    		//inventory dr
    		this.addInvDrEntry(newInvAdjustment.getInvDrNextLine(), "INVENTORY",
    				isDebit == true ? EJBCommon.TRUE : EJBCommon.FALSE, Math.abs(CST_VRNC_VL), EJBCommon.FALSE,
    						glInventoryChartOfAccount.getCoaCode(), newInvAdjustment, AD_BRNCH, AD_CMPNY);

    		//variance dr
    		this.addInvDrEntry(newInvAdjustment.getInvDrNextLine(),"VARIANCE",
    				!isDebit == true ? EJBCommon.TRUE : EJBCommon.FALSE, Math.abs(CST_VRNC_VL), EJBCommon.FALSE,
    						glCoaVarianceAccount.getCoaCode(), newInvAdjustment, AD_BRNCH, AD_CMPNY);

    		this.executeInvAdjPost(newInvAdjustment.getAdjCode(), newInvAdjustment.getAdjLastModifiedBy(), AD_BRNCH,
    				AD_CMPNY);

    	} catch (AdPRFCoaGlVarianceAccountNotFoundException ex) {

    		getSessionContext().setRollbackOnly();
    		throw ex;

    	} catch (Exception ex) {

    		Debug.printStackTrace(ex);
    		getSessionContext().setRollbackOnly();
    		throw new EJBException(ex.getMessage());

    	}
    	*/
    }

    private void regenerateCostVariance(Collection invCostings, LocalInvCosting invCosting, Integer AD_BRNCH, Integer AD_CMPNY)
    throws AdPRFCoaGlVarianceAccountNotFoundException {
    	/*
    	Debug.print("ArMiscReceiptEntryControllerBean regenerateCostVariance");

    	try {

    		Iterator i = invCostings.iterator();
    		LocalInvCosting prevInvCosting = invCosting;

    		while (i.hasNext()) {

    			LocalInvCosting invPropagatedCosting = (LocalInvCosting)i.next();

    			if(prevInvCosting.getCstRemainingQuantity() < 0) {

    				double TTL_CST = 0;
    				double QNTY = 0;
    				String ADJ_RFRNC_NMBR = "";
    				String ADJ_DSCRPTN = "";
    				String ADJ_CRTD_BY = "";

    				// get unit cost adjusment, document number and unit of measure
    				if (invPropagatedCosting.getApPurchaseOrderLine() != null) {

    					TTL_CST = invPropagatedCosting.getApPurchaseOrderLine().getPlAmount();
						QNTY =  this.convertByUomFromAndItemAndQuantity(
								invPropagatedCosting.getApPurchaseOrderLine().getInvUnitOfMeasure(),
								invPropagatedCosting.getApPurchaseOrderLine().getInvItemLocation().getInvItem(),
								invPropagatedCosting.getApPurchaseOrderLine().getPlQuantity(), AD_CMPNY);

    					ADJ_DSCRPTN = invPropagatedCosting.getApPurchaseOrderLine().getApPurchaseOrder().getPoDescription();
    					ADJ_CRTD_BY = invPropagatedCosting.getApPurchaseOrderLine().getApPurchaseOrder().getPoPostedBy();
    					ADJ_RFRNC_NMBR = "APRI" +
						invPropagatedCosting.getApPurchaseOrderLine().getApPurchaseOrder().getPoDocumentNumber();

    				} else if (invPropagatedCosting.getApVoucherLineItem() != null){

    					TTL_CST = invPropagatedCosting.getApVoucherLineItem().getVliAmount();
						QNTY = this.convertByUomFromAndItemAndQuantity(
								invPropagatedCosting.getApVoucherLineItem().getInvUnitOfMeasure(),
								invPropagatedCosting.getApVoucherLineItem().getInvItemLocation().getInvItem(),
								invPropagatedCosting.getApVoucherLineItem().getVliQuantity(), AD_CMPNY);

    					if (invPropagatedCosting.getApVoucherLineItem().getApVoucher() != null) {

    						ADJ_DSCRPTN = invPropagatedCosting.getApVoucherLineItem().getApVoucher().getVouDescription();
    						ADJ_CRTD_BY = invPropagatedCosting.getApVoucherLineItem().getApVoucher().getVouPostedBy();
    						ADJ_RFRNC_NMBR = "APVOU" +
							invPropagatedCosting.getApVoucherLineItem().getApVoucher().getVouDocumentNumber();

    					} else if (invPropagatedCosting.getApVoucherLineItem().getApCheck() != null) {

    						ADJ_DSCRPTN = invPropagatedCosting.getApVoucherLineItem().getApCheck().getChkDescription();
    						ADJ_CRTD_BY = invPropagatedCosting.getApVoucherLineItem().getApCheck().getChkPostedBy();
    						ADJ_RFRNC_NMBR = "APCHK" +
							invPropagatedCosting.getApVoucherLineItem().getApCheck().getChkDocumentNumber();

    					}

    				} else if (invPropagatedCosting.getArInvoiceLineItem() != null){

    					QNTY = this.convertByUomFromAndItemAndQuantity(
								invPropagatedCosting.getArInvoiceLineItem().getInvUnitOfMeasure(),
								invPropagatedCosting.getArInvoiceLineItem().getInvItemLocation().getInvItem(),
								invPropagatedCosting.getArInvoiceLineItem().getIliQuantity(), AD_CMPNY);
   						TTL_CST = prevInvCosting.getCstRemainingValue() - invPropagatedCosting.getCstRemainingValue();

   						if(invPropagatedCosting.getArInvoiceLineItem().getArInvoice() != null){

   							ADJ_DSCRPTN = invPropagatedCosting.getArInvoiceLineItem().getArInvoice().getInvDescription();
   							ADJ_CRTD_BY = invPropagatedCosting.getArInvoiceLineItem().getArInvoice().getInvPostedBy();
   							ADJ_RFRNC_NMBR = "ARCM" +
							invPropagatedCosting.getArInvoiceLineItem().getArInvoice().getInvNumber();

   						} else if(invPropagatedCosting.getArInvoiceLineItem().getArReceipt() != null){

   							ADJ_DSCRPTN = invPropagatedCosting.getArInvoiceLineItem().getArReceipt().getRctDescription();
   							ADJ_CRTD_BY = invPropagatedCosting.getArInvoiceLineItem().getArReceipt().getRctPostedBy();
   							ADJ_RFRNC_NMBR = "ARMR" +
							invPropagatedCosting.getArInvoiceLineItem().getArReceipt().getRctNumber();

   						}

    				} else if (invPropagatedCosting.getArSalesOrderInvoiceLine() != null){

   						TTL_CST = prevInvCosting.getCstRemainingValue() - invPropagatedCosting.getCstRemainingValue();
    					QNTY = this.convertByUomFromAndItemAndQuantity(
								invPropagatedCosting.getArSalesOrderInvoiceLine().getArSalesOrderLine().getInvUnitOfMeasure(),
								invPropagatedCosting.getArSalesOrderInvoiceLine().getArSalesOrderLine().getInvItemLocation().getInvItem(),
								invPropagatedCosting.getArSalesOrderInvoiceLine().getSilQuantityDelivered(), AD_CMPNY);

    					ADJ_DSCRPTN = invPropagatedCosting.getArSalesOrderInvoiceLine().getArInvoice().getInvDescription();
    					ADJ_CRTD_BY = invPropagatedCosting.getArSalesOrderInvoiceLine().getArInvoice().getInvPostedBy();
    					ADJ_RFRNC_NMBR = "ARCM" +
						invPropagatedCosting.getArSalesOrderInvoiceLine().getArInvoice().getInvNumber();

    				} else if (invPropagatedCosting.getInvAdjustmentLine() != null){

    					ADJ_DSCRPTN = invPropagatedCosting.getInvAdjustmentLine().getInvAdjustment().getAdjDescription();
    					ADJ_CRTD_BY = invPropagatedCosting.getInvAdjustmentLine().getInvAdjustment().getAdjPostedBy();
    					ADJ_RFRNC_NMBR = "INVADJ" +
						invPropagatedCosting.getInvAdjustmentLine().getInvAdjustment().getAdjDocumentNumber();

    					if(invPropagatedCosting.getInvAdjustmentLine().getAlAdjustQuantity() != 0) {

    						TTL_CST = (invPropagatedCosting.getInvAdjustmentLine().getAlUnitCost() *
    								invPropagatedCosting.getInvAdjustmentLine().getAlAdjustQuantity());
    						QNTY =  this.convertByUomFromAndItemAndQuantity(
    								invPropagatedCosting.getInvAdjustmentLine().getInvUnitOfMeasure(),
									invPropagatedCosting.getInvAdjustmentLine().getInvItemLocation().getInvItem(),
									invPropagatedCosting.getInvAdjustmentLine().getAlAdjustQuantity(), AD_CMPNY);

    					}

    				} else if (invPropagatedCosting.getInvAssemblyTransferLine() != null){

    					QNTY = invPropagatedCosting.getInvAssemblyTransferLine().getAtlAssembleQuantity();
    					TTL_CST = invPropagatedCosting.getInvAssemblyTransferLine().getAtlAssembleCost();

    					ADJ_DSCRPTN = invPropagatedCosting.getInvAssemblyTransferLine().getInvAssemblyTransfer().getAtrDescription();
    					ADJ_CRTD_BY = invPropagatedCosting.getInvAssemblyTransferLine().getInvAssemblyTransfer().getAtrPostedBy();
    					ADJ_RFRNC_NMBR = "INVAT" +
						invPropagatedCosting.getInvAssemblyTransferLine().getInvAssemblyTransfer().getAtrDocumentNumber();

    				} else if (invPropagatedCosting.getInvBranchStockTransferLine() != null){

    					if(invPropagatedCosting.getInvBranchStockTransferLine().getInvBranchStockTransfer().getBstTransferOutNumber()
    							!= null) {

    						TTL_CST = invPropagatedCosting.getInvBranchStockTransferLine().getBslAmount();
							QNTY =  this.convertByUomFromAndItemAndQuantity(
									invPropagatedCosting.getInvBranchStockTransferLine().getInvUnitOfMeasure(),
									invPropagatedCosting.getInvBranchStockTransferLine().getInvItemLocation().getInvItem(),
									invPropagatedCosting.getInvBranchStockTransferLine().getBslQuantityReceived(), AD_CMPNY);

    					} else {

    						TTL_CST = invPropagatedCosting.getInvBranchStockTransferLine().getBslAmount();
							QNTY =  this.convertByUomFromAndItemAndQuantity(
									invPropagatedCosting.getInvBranchStockTransferLine().getInvUnitOfMeasure(),
									invPropagatedCosting.getInvBranchStockTransferLine().getInvItemLocation().getInvItem(),
									invPropagatedCosting.getInvBranchStockTransferLine().getBslQuantity(), AD_CMPNY);
    					}

    					ADJ_DSCRPTN =
    						invPropagatedCosting.getInvBranchStockTransferLine().getInvBranchStockTransfer().getBstDescription();
    					ADJ_CRTD_BY =
    						invPropagatedCosting.getInvBranchStockTransferLine().getInvBranchStockTransfer().getBstPostedBy();
    					ADJ_RFRNC_NMBR = "INVBST" +
						invPropagatedCosting.getInvBranchStockTransferLine().getInvBranchStockTransfer().getBstNumber();

    				} else if (invPropagatedCosting.getInvBuildUnbuildAssemblyLine() != null){

   						TTL_CST = prevInvCosting.getCstRemainingValue() - invPropagatedCosting.getCstRemainingValue();
    					QNTY =  invPropagatedCosting.getInvBuildUnbuildAssemblyLine().getBlBuildQuantity();
    					ADJ_DSCRPTN =
    						invPropagatedCosting.getInvBuildUnbuildAssemblyLine().getInvBuildUnbuildAssembly().getBuaDescription();
    					ADJ_CRTD_BY =
    						invPropagatedCosting.getInvBuildUnbuildAssemblyLine().getInvBuildUnbuildAssembly().getBuaPostedBy();
    					ADJ_RFRNC_NMBR = "INVBUA" +
						invPropagatedCosting.getInvBuildUnbuildAssemblyLine().getInvBuildUnbuildAssembly().getBuaDocumentNumber();

    				} else if (invPropagatedCosting.getInvStockIssuanceLine()!= null){

    					TTL_CST = invPropagatedCosting.getInvStockIssuanceLine().getSilIssueCost();
						QNTY =  this.convertByUomFromAndItemAndQuantity(
								invPropagatedCosting.getInvStockIssuanceLine().getInvUnitOfMeasure(),
								invPropagatedCosting.getInvStockIssuanceLine().getInvItemLocation().getInvItem(),
								invPropagatedCosting.getInvStockIssuanceLine().getSilIssueQuantity(), AD_CMPNY);
    					ADJ_DSCRPTN = invPropagatedCosting.getInvStockIssuanceLine().getInvStockIssuance().getSiDescription();
    					ADJ_CRTD_BY = invPropagatedCosting.getInvStockIssuanceLine().getInvStockIssuance().getSiPostedBy();
    					ADJ_RFRNC_NMBR = "INVSI" +
						invPropagatedCosting.getInvStockIssuanceLine().getInvStockIssuance().getSiDocumentNumber();

    				} else if (invPropagatedCosting.getInvStockTransferLine()!= null) {

    					TTL_CST = invPropagatedCosting.getInvStockTransferLine().getStlAmount();
						QNTY =  this.convertByUomFromAndItemAndQuantity(
								invPropagatedCosting.getInvStockTransferLine().getInvUnitOfMeasure(),
								invPropagatedCosting.getInvStockTransferLine().getInvItem(),
								invPropagatedCosting.getInvStockTransferLine().getStlQuantityDelivered(), AD_CMPNY);
    					ADJ_DSCRPTN = invPropagatedCosting.getInvStockTransferLine().getInvStockTransfer().getStDescription();
    					ADJ_CRTD_BY = invPropagatedCosting.getInvStockTransferLine().getInvStockTransfer().getStPostedBy();
    					ADJ_RFRNC_NMBR = "INVST" +
						invPropagatedCosting.getInvStockTransferLine().getInvStockTransfer().getStDocumentNumber();

    				} else {

    					prevInvCosting = invPropagatedCosting;
    					continue;

    				}

    				// if quantity is equal 0, no variance.
    				if(QNTY == 0) continue;

    				// compute new cost variance
    				double UNT_CST = TTL_CST/QNTY;
    				double CST_VRNC_VL = (invPropagatedCosting.getCstRemainingQuantity() * UNT_CST -
    						invPropagatedCosting.getCstRemainingValue());

    				if(CST_VRNC_VL != 0)
    					this.generateCostVariance(invPropagatedCosting.getInvItemLocation(), CST_VRNC_VL, ADJ_RFRNC_NMBR,
    							ADJ_DSCRPTN, invPropagatedCosting.getCstDate(), ADJ_CRTD_BY, AD_BRNCH, AD_CMPNY);

    			}

    			// set previous costing
    			prevInvCosting = invPropagatedCosting;

    		}

    	} catch (AdPRFCoaGlVarianceAccountNotFoundException ex){

    		throw ex;

    	} catch (Exception ex) {

    		Debug.printStackTrace(ex);
    		getSessionContext().setRollbackOnly();
    		throw new EJBException(ex.getMessage());

    	}     */
    }

    private void addInvDrEntry(short DR_LN, String DR_CLSS, byte DR_DBT, double DR_AMNT, byte DR_RVRSL, Integer COA_CODE,
    		LocalInvAdjustment invAdjustment, Integer AD_BRNCH, Integer AD_CMPNY)

    throws GlobalBranchAccountNumberInvalidException {

    	Debug.print("ArMiscReceiptEntryControllerBean addInvDrEntry");

    	LocalAdCompanyHome adCompanyHome = null;
    	LocalInvDistributionRecordHome invDistributionRecordHome = null;
    	LocalGlChartOfAccountHome glChartOfAccountHome = null;

    	// Initialize EJB Home

    	try {

    		adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
    		invDistributionRecordHome = (LocalInvDistributionRecordHome)EJBHomeFactory.
			lookUpLocalHome(LocalInvDistributionRecordHome.JNDI_NAME, LocalInvDistributionRecordHome.class);
    		glChartOfAccountHome = (LocalGlChartOfAccountHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlChartOfAccountHome.JNDI_NAME, LocalGlChartOfAccountHome.class);


    	} catch (NamingException ex) {

    		throw new EJBException(ex.getMessage());

    	}

    	try {

    		// get company

    		LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);

    		// validate coa

    		LocalGlChartOfAccount glChartOfAccount = null;

    		try {

    			glChartOfAccount = glChartOfAccountHome.findByCoaCodeAndBranchCode(COA_CODE, AD_BRNCH, AD_CMPNY);

    		} catch(FinderException ex) {

    			throw new GlobalBranchAccountNumberInvalidException ();

    		}

    		// create distribution record

    		LocalInvDistributionRecord invDistributionRecord = invDistributionRecordHome.create(DR_LN, DR_CLSS, DR_DBT,
    				EJBCommon.roundIt(DR_AMNT, adCompany.getGlFunctionalCurrency().getFcPrecision()), DR_RVRSL, EJBCommon.FALSE,
					 AD_CMPNY);

    		invAdjustment.addInvDistributionRecord(invDistributionRecord);
    		glChartOfAccount.addInvDistributionRecord(invDistributionRecord);

    	} catch(GlobalBranchAccountNumberInvalidException ex) {

    		throw new GlobalBranchAccountNumberInvalidException ();

    	} catch (Exception ex) {

    		Debug.printStackTrace(ex);
    		getSessionContext().setRollbackOnly();
    		throw new EJBException(ex.getMessage());

    	}

    }

    private void executeInvAdjPost(Integer ADJ_CODE, String USR_NM, Integer AD_BRNCH, Integer AD_CMPNY) throws
	GlobalRecordAlreadyDeletedException,
	GlobalTransactionAlreadyPostedException,
	GlJREffectiveDateNoPeriodExistException,
	GlJREffectiveDatePeriodClosedException,
	GlobalJournalNotBalanceException,
	GlobalBranchAccountNumberInvalidException {

    	Debug.print("ArMiscReceiptEntryControllerBean executeInvAdjPost");

    	LocalInvAdjustmentHome invAdjustmentHome = null;
    	LocalAdCompanyHome adCompanyHome = null;
    	LocalAdPreferenceHome adPreferenceHome = null;
    	LocalGlSetOfBookHome glSetOfBookHome = null;
    	LocalGlAccountingCalendarValueHome glAccountingCalendarValueHome = null;
    	LocalGlJournalHome glJournalHome = null;
    	LocalGlJournalBatchHome glJournalBatchHome = null;
    	LocalGlSuspenseAccountHome glSuspenseAccountHome = null;
    	LocalGlJournalLineHome glJournalLineHome = null;
    	LocalGlJournalSourceHome glJournalSourceHome = null;
    	LocalGlJournalCategoryHome glJournalCategoryHome = null;
    	LocalGlFunctionalCurrencyHome glFunctionalCurrencyHome = null;
    	LocalInvDistributionRecordHome invDistributionRecordHome = null;
    	LocalInvCostingHome invCostingHome = null;
    	LocalGlChartOfAccountHome glChartOfAccountHome = null;
    	LocalInvAdjustmentLineHome invAdjustmentLineHome = null;


    	// Initialize EJB Home

    	try {

    		invAdjustmentHome = (LocalInvAdjustmentHome)EJBHomeFactory.
			lookUpLocalHome(LocalInvAdjustmentHome.JNDI_NAME, LocalInvAdjustmentHome.class);
    		adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
    		adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);
    		glSetOfBookHome = (LocalGlSetOfBookHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlSetOfBookHome.JNDI_NAME, LocalGlSetOfBookHome.class);
    		glAccountingCalendarValueHome = (LocalGlAccountingCalendarValueHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlAccountingCalendarValueHome.JNDI_NAME, LocalGlAccountingCalendarValueHome.class);
    		glJournalHome = (LocalGlJournalHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlJournalHome.JNDI_NAME, LocalGlJournalHome.class);
    		glJournalBatchHome = (LocalGlJournalBatchHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlJournalBatchHome.JNDI_NAME, LocalGlJournalBatchHome.class);
    		glSuspenseAccountHome = (LocalGlSuspenseAccountHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlSuspenseAccountHome.JNDI_NAME, LocalGlSuspenseAccountHome.class);
    		glJournalLineHome = (LocalGlJournalLineHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlJournalLineHome.JNDI_NAME, LocalGlJournalLineHome.class);
    		glJournalSourceHome = (LocalGlJournalSourceHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlJournalSourceHome.JNDI_NAME, LocalGlJournalSourceHome.class);
    		glJournalCategoryHome = (LocalGlJournalCategoryHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlJournalCategoryHome.JNDI_NAME, LocalGlJournalCategoryHome.class);
    		glFunctionalCurrencyHome = (LocalGlFunctionalCurrencyHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlFunctionalCurrencyHome.JNDI_NAME, LocalGlFunctionalCurrencyHome.class);
    		invDistributionRecordHome = (LocalInvDistributionRecordHome)EJBHomeFactory.
			lookUpLocalHome(LocalInvDistributionRecordHome.JNDI_NAME, LocalInvDistributionRecordHome.class);
    		invCostingHome = (LocalInvCostingHome)EJBHomeFactory.
			lookUpLocalHome(LocalInvCostingHome.JNDI_NAME, LocalInvCostingHome.class);
    		glChartOfAccountHome = (LocalGlChartOfAccountHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlChartOfAccountHome.JNDI_NAME, LocalGlChartOfAccountHome.class);
    		invAdjustmentLineHome = (LocalInvAdjustmentLineHome)EJBHomeFactory.
			lookUpLocalHome(LocalInvAdjustmentLineHome.JNDI_NAME, LocalInvAdjustmentLineHome.class);


    	} catch (NamingException ex) {

    		throw new EJBException(ex.getMessage());

    	}

    	try {

    		// validate if adjustment is already deleted

    		LocalInvAdjustment invAdjustment = null;

    		try {

    			invAdjustment = invAdjustmentHome.findByPrimaryKey(ADJ_CODE);

    		} catch (FinderException ex) {

    			throw new GlobalRecordAlreadyDeletedException();

    		}

    		// validate if adjustment is already posted or void

    		if (invAdjustment.getAdjPosted() == EJBCommon.TRUE) {

    			if (invAdjustment.getAdjVoid() != EJBCommon.TRUE)
    			throw new GlobalTransactionAlreadyPostedException();

    		}

    		Collection invAdjustmentLines = null;

    		if(invAdjustment.getAdjVoid() == EJBCommon.FALSE)
    			invAdjustmentLines = invAdjustmentLineHome.findByAlVoidAndAdjCode(EJBCommon.FALSE, invAdjustment.getAdjCode(), AD_CMPNY);
    		else
    			invAdjustmentLines = invAdjustmentLineHome.findByAlVoidAndAdjCode(EJBCommon.TRUE, invAdjustment.getAdjCode(), AD_CMPNY);


    		Iterator i = invAdjustmentLines.iterator();

    		while(i.hasNext()) {


    			LocalInvAdjustmentLine invAdjustmentLine = (LocalInvAdjustmentLine) i.next();

    			LocalInvCosting invCosting =
    				invCostingHome.getByMaxCstDateToLongAndMaxCstLineNumberAndLessThanEqualCstDateAndIiNameAndLocName(
    						invAdjustment.getAdjDate(), invAdjustmentLine.getInvItemLocation().getInvItem().getIiName(),
    						invAdjustmentLine.getInvItemLocation().getInvLocation().getLocName(), AD_BRNCH, AD_CMPNY);

				this.postInvAdjustmentToInventory(invAdjustmentLine, invAdjustment.getAdjDate(), 0,
						invAdjustmentLine.getAlUnitCost(), invCosting.getCstRemainingQuantity(),
						invCosting.getCstRemainingValue() + invAdjustmentLine.getAlUnitCost(), AD_BRNCH, AD_CMPNY);

    		}

    		// post to gl if necessary

    		LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);
    		LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);

    			// validate if date has no period and period is closed

    			LocalGlSetOfBook glJournalSetOfBook = null;

    			try {

    				glJournalSetOfBook = glSetOfBookHome.findByDate(invAdjustment.getAdjDate(), AD_CMPNY);

    			} catch (FinderException ex) {

    				throw new GlJREffectiveDateNoPeriodExistException();

    			}

    			LocalGlAccountingCalendarValue glAccountingCalendarValue =
    				glAccountingCalendarValueHome.findByAcCodeAndDate(
    						glJournalSetOfBook.getGlAccountingCalendar().getAcCode(), invAdjustment.getAdjDate(), AD_CMPNY);


    			if (glAccountingCalendarValue.getAcvStatus() == 'N' ||
    					glAccountingCalendarValue.getAcvStatus() == 'C' ||
						glAccountingCalendarValue.getAcvStatus() == 'P') {

    				throw new GlJREffectiveDatePeriodClosedException();

    			}

    			// check if invoice is balance if not check suspense posting

    			LocalGlJournalLine glOffsetJournalLine = null;

    			Collection invDistributionRecords = null;

    			if (invAdjustment.getAdjVoid() == EJBCommon.FALSE) {

    				invDistributionRecords = invDistributionRecordHome.findImportableDrByDrReversedAndAdjCode(EJBCommon.FALSE,
    						invAdjustment.getAdjCode(), AD_CMPNY);

    			} else {

    				invDistributionRecords = invDistributionRecordHome.findImportableDrByDrReversedAndAdjCode(EJBCommon.TRUE,
    						invAdjustment.getAdjCode(), AD_CMPNY);

    			}


    			Iterator j = invDistributionRecords.iterator();

    			double TOTAL_DEBIT = 0d;
    			double TOTAL_CREDIT = 0d;

    			while (j.hasNext()) {

    				LocalInvDistributionRecord invDistributionRecord = (LocalInvDistributionRecord)j.next();

    				double DR_AMNT = 0d;

    				DR_AMNT = invDistributionRecord.getDrAmount();

    				if (invDistributionRecord.getDrDebit() == EJBCommon.TRUE) {

    					TOTAL_DEBIT += DR_AMNT;

    				} else {

    					TOTAL_CREDIT += DR_AMNT;

    				}

    			}

    			TOTAL_DEBIT = EJBCommon.roundIt(TOTAL_DEBIT, adCompany.getGlFunctionalCurrency().getFcPrecision());
    			TOTAL_CREDIT = EJBCommon.roundIt(TOTAL_CREDIT, adCompany.getGlFunctionalCurrency().getFcPrecision());

    			if (adPreference.getPrfAllowSuspensePosting() == EJBCommon.TRUE &&
    					TOTAL_DEBIT != TOTAL_CREDIT) {

    				LocalGlSuspenseAccount glSuspenseAccount = null;

    				try {

    					glSuspenseAccount = glSuspenseAccountHome.findByJsNameAndJcName("INVENTORY", "INVENTORY ADJUSTMENTS",
    							AD_CMPNY);

    				} catch (FinderException ex) {

    					throw new GlobalJournalNotBalanceException();

    				}

    				if (TOTAL_DEBIT - TOTAL_CREDIT < 0) {

    					glOffsetJournalLine = glJournalLineHome.create((short)(invDistributionRecords.size() + 1), EJBCommon.TRUE,
    							TOTAL_CREDIT - TOTAL_DEBIT, "", AD_CMPNY);

    				} else {

    					glOffsetJournalLine = glJournalLineHome.create((short)(invDistributionRecords.size() + 1), EJBCommon.FALSE,
    							TOTAL_DEBIT - TOTAL_CREDIT, "", AD_CMPNY);

    				}

    				LocalGlChartOfAccount glChartOfAccount = glSuspenseAccount.getGlChartOfAccount();
    				glChartOfAccount.addGlJournalLine(glOffsetJournalLine);


    			} else if (adPreference.getPrfAllowSuspensePosting() == EJBCommon.FALSE &&
    					TOTAL_DEBIT != TOTAL_CREDIT) {

    				throw new GlobalJournalNotBalanceException();

    			}

    			// create journal batch if necessary

    			LocalGlJournalBatch glJournalBatch = null;
    			java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat("MM/dd/yyyy");

    			try {

    				glJournalBatch = glJournalBatchHome.findByJbName("JOURNAL IMPORT " + formatter.format(new Date()) +
    						" INVENTORY ADJUSTMENTS", AD_BRNCH, AD_CMPNY);

    			} catch (FinderException ex) {

    			}

    			if (glJournalBatch == null) {

    				glJournalBatch = glJournalBatchHome.create("JOURNAL IMPORT " + formatter.format(new Date()) +
    						" INVENTORY ADJUSTMENTS", "JOURNAL IMPORT", "CLOSED", EJBCommon.getGcCurrentDateWoTime().getTime(),
							USR_NM, AD_BRNCH, AD_CMPNY);

    			}

    			// create journal entry

    			LocalGlJournal glJournal = glJournalHome.create(invAdjustment.getAdjReferenceNumber(),
    					invAdjustment.getAdjDescription(), invAdjustment.getAdjDate(),
						0.0d, null, invAdjustment.getAdjDocumentNumber(), null, 1d, "N/A", null,
						'N', EJBCommon.TRUE, EJBCommon.FALSE,
						USR_NM, new Date(),
						USR_NM, new Date(),
						null, null,
						USR_NM, EJBCommon.getGcCurrentDateWoTime().getTime(),
						null, null, EJBCommon.FALSE, null,
						AD_BRNCH, AD_CMPNY);

    			LocalGlJournalSource glJournalSource = glJournalSourceHome.findByJsName("INVENTORY", AD_CMPNY);
    			glJournal.setGlJournalSource(glJournalSource);

    			LocalGlFunctionalCurrency glFunctionalCurrency = glFunctionalCurrencyHome.findByFcName(adCompany.getGlFunctionalCurrency().getFcName(), AD_CMPNY);
    			glJournal.setGlFunctionalCurrency(glFunctionalCurrency);

    			LocalGlJournalCategory glJournalCategory = glJournalCategoryHome.findByJcName("INVENTORY ADJUSTMENTS", AD_CMPNY);
    			glJournal.setGlJournalCategory(glJournalCategory);


    			if (glJournalBatch != null) {

                    glJournal.setGlJournalBatch(glJournalBatch);
                }

    			// create journal lines

    			j = invDistributionRecords.iterator();

    			while (j.hasNext()) {

    				LocalInvDistributionRecord invDistributionRecord = (LocalInvDistributionRecord)j.next();

    				double DR_AMNT = 0d;

    				DR_AMNT = invDistributionRecord.getDrAmount();

    				LocalGlJournalLine glJournalLine = glJournalLineHome.create(invDistributionRecord.getDrLine(),
    						invDistributionRecord.getDrDebit(), DR_AMNT, "", AD_CMPNY);

    				invDistributionRecord.getInvChartOfAccount().addGlJournalLine(glJournalLine);

    				glJournal.addGlJournalLine(glJournalLine);

    				invDistributionRecord.setDrImported(EJBCommon.TRUE);


    			}

    			if (glOffsetJournalLine != null) {

    				glJournal.addGlJournalLine(glOffsetJournalLine);

    			}

    			// post journal to gl

    			Collection glJournalLines = glJournal.getGlJournalLines();

    			i = glJournalLines.iterator();

    			while (i.hasNext()) {

    				LocalGlJournalLine glJournalLine = (LocalGlJournalLine)i.next();

    				// post current to current acv

    				this.postToGl(glAccountingCalendarValue,
    						glJournalLine.getGlChartOfAccount(),
							true, glJournalLine.getJlDebit(), glJournalLine.getJlAmount(), AD_CMPNY);


    				// post to subsequent acvs (propagate)

    				Collection glSubsequentAccountingCalendarValues =
    					glAccountingCalendarValueHome.findSubsequentAcvByAcCodeAndAcvPeriodNumber(
    							glJournalSetOfBook.getGlAccountingCalendar().getAcCode(),
								glAccountingCalendarValue.getAcvPeriodNumber(), AD_CMPNY);

    				Iterator acvsIter = glSubsequentAccountingCalendarValues.iterator();

    				while (acvsIter.hasNext()) {

    					LocalGlAccountingCalendarValue glSubsequentAccountingCalendarValue =
    						(LocalGlAccountingCalendarValue)acvsIter.next();

    					this.postToGl(glSubsequentAccountingCalendarValue,
    							glJournalLine.getGlChartOfAccount(),
								false, glJournalLine.getJlDebit(), glJournalLine.getJlAmount(), AD_CMPNY);

    				}

    				// post to subsequent years if necessary

    				Collection glSubsequentSetOfBooks = glSetOfBookHome.findSubsequentSobByAcYear(
    						glJournalSetOfBook.getGlAccountingCalendar().getAcYear(), AD_CMPNY);

    				if (!glSubsequentSetOfBooks.isEmpty() && glJournalSetOfBook.getSobYearEndClosed() == 1) {

    					adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
    					LocalGlChartOfAccount glRetainedEarningsAccount =
    						glChartOfAccountHome.findByCoaAccountNumberAndBranchCode(adCompany.getCmpRetainedEarnings(),
    								AD_BRNCH, AD_CMPNY);

    					Iterator sobIter = glSubsequentSetOfBooks.iterator();

    					while (sobIter.hasNext()) {

    						LocalGlSetOfBook glSubsequentSetOfBook = (LocalGlSetOfBook)sobIter.next();

    						String ACCOUNT_TYPE = glJournalLine.getGlChartOfAccount().getCoaAccountType();

    						// post to subsequent acvs of subsequent set of book(propagate)

    						Collection glAccountingCalendarValues =
    							glAccountingCalendarValueHome.findByAcCode(
    									glSubsequentSetOfBook.getGlAccountingCalendar().getAcCode(), AD_CMPNY);

    						Iterator acvIter = glAccountingCalendarValues.iterator();

    						while (acvIter.hasNext()) {

    							LocalGlAccountingCalendarValue glSubsequentAccountingCalendarValue =
    								(LocalGlAccountingCalendarValue)acvIter.next();

    							if (ACCOUNT_TYPE.equals("ASSET") || ACCOUNT_TYPE.equals("LIABILITY") ||
    									ACCOUNT_TYPE.equals("OWNERS EQUITY")) {

    								this.postToGl(glSubsequentAccountingCalendarValue,
    										glJournalLine.getGlChartOfAccount(),
											false, glJournalLine.getJlDebit(), glJournalLine.getJlAmount(), AD_CMPNY);

    							} else { // revenue & expense

    								this.postToGl(glSubsequentAccountingCalendarValue,
    										glRetainedEarningsAccount,
											false, glJournalLine.getJlDebit(), glJournalLine.getJlAmount(), AD_CMPNY);

    							}

    						}

    						if (glSubsequentSetOfBook.getSobYearEndClosed() == 0) break;

    					}

    				}

    			}

			invAdjustment.setAdjPosted(EJBCommon.TRUE);

    	} catch (GlJREffectiveDateNoPeriodExistException ex) {

    		getSessionContext().setRollbackOnly();
    		throw ex;

    	} catch (GlJREffectiveDatePeriodClosedException ex) {

    		getSessionContext().setRollbackOnly();
    		throw ex;

    	} catch (GlobalJournalNotBalanceException ex) {

    		getSessionContext().setRollbackOnly();
    		throw ex;

    	} catch (GlobalRecordAlreadyDeletedException ex) {

    		getSessionContext().setRollbackOnly();
    		throw ex;

    	} catch (GlobalTransactionAlreadyPostedException ex) {

    		getSessionContext().setRollbackOnly();
    		throw ex;

    	} catch (Exception ex) {

    		Debug.printStackTrace(ex);
    		getSessionContext().setRollbackOnly();
    		throw new EJBException(ex.getMessage());

    	}

    }

    private LocalInvAdjustmentLine addInvAlEntry(LocalInvItemLocation invItemLocation, LocalInvAdjustment invAdjustment,
    		double CST_VRNC_VL, byte AL_VD, Integer AD_CMPNY) {

    	Debug.print("ArMiscReceiptEntryControllerBean addInvAlEntry");

    	LocalInvAdjustmentLineHome invAdjustmentLineHome = null;

    	// Initialize EJB Home

    	try {

    		invAdjustmentLineHome = (LocalInvAdjustmentLineHome)EJBHomeFactory.
			lookUpLocalHome(LocalInvAdjustmentLineHome.JNDI_NAME, LocalInvAdjustmentLineHome.class);

    	} catch (NamingException ex) {

    		throw new EJBException(ex.getMessage());

    	}

    	try {

    		// create dr entry
    		LocalInvAdjustmentLine invAdjustmentLine = null;
    		invAdjustmentLine = invAdjustmentLineHome.create(CST_VRNC_VL,null, null, 0,0, AL_VD, AD_CMPNY);

    		// map adjustment, unit of measure, item location
    		invAdjustment.addInvAdjustmentLine(invAdjustmentLine);
    		invItemLocation.getInvItem().getInvUnitOfMeasure().addInvAdjustmentLine(invAdjustmentLine);
    		invItemLocation.addInvAdjustmentLine(invAdjustmentLine);

    		return invAdjustmentLine;

    	} catch (Exception ex) {

    		Debug.printStackTrace(ex);
    		getSessionContext().setRollbackOnly();
    		throw new EJBException(ex.getMessage());

    	}

    }

    private LocalInvAdjustment saveInvAdjustment(String ADJ_RFRNC_NMBR, String ADJ_DSCRPTN,
    		Date ADJ_DATE, String USR_NM, Integer AD_BRNCH, Integer AD_CMPNY){

    	Debug.print("ArMiscReceiptEntryControllerBean saveInvAdjustment");

    	LocalInvAdjustmentHome invAdjustmentHome = null;
    	LocalAdDocumentSequenceAssignmentHome adDocumentSequenceAssignmentHome = null;
    	LocalAdBranchDocumentSequenceAssignmentHome adBranchDocumentSequenceAssignmentHome = null;
    	// Initialize EJB Home

    	try{

    		invAdjustmentHome = (LocalInvAdjustmentHome)EJBHomeFactory.
			lookUpLocalHome(LocalInvAdjustmentHome.JNDI_NAME, LocalInvAdjustmentHome.class);
			adDocumentSequenceAssignmentHome = (LocalAdDocumentSequenceAssignmentHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdDocumentSequenceAssignmentHome.JNDI_NAME, LocalAdDocumentSequenceAssignmentHome.class);
			adBranchDocumentSequenceAssignmentHome = (LocalAdBranchDocumentSequenceAssignmentHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdBranchDocumentSequenceAssignmentHome.JNDI_NAME, LocalAdBranchDocumentSequenceAssignmentHome.class);

    	} catch (NamingException ex) {

    		throw new EJBException(ex.getMessage());

    	}

    	try{

    		// generate adj document number
    		String ADJ_DCMNT_NMBR = null;

    		LocalAdBranchDocumentSequenceAssignment adBranchDocumentSequenceAssignment = null;
    		LocalAdDocumentSequenceAssignment adDocumentSequenceAssignment = null;

    		try {

    			adDocumentSequenceAssignment = adDocumentSequenceAssignmentHome.findByDcName("INV ADJUSTMENT", AD_CMPNY);

    		} catch (FinderException ex) {

    		}

    		try {

    			adBranchDocumentSequenceAssignment = adBranchDocumentSequenceAssignmentHome.findBdsByDsaCodeAndBrCode(
    					adDocumentSequenceAssignment.getDsaCode(), AD_BRNCH, AD_CMPNY);

    		} catch (FinderException ex) {

    		}

    		while (true) {

    			if (adBranchDocumentSequenceAssignment == null || adBranchDocumentSequenceAssignment.getBdsNextSequence() == null) {

    				try {

    					invAdjustmentHome.findByAdjDocumentNumberAndBrCode(adDocumentSequenceAssignment.getDsaNextSequence(),
    							AD_BRNCH, AD_CMPNY);
    					adDocumentSequenceAssignment.setDsaNextSequence(EJBCommon.incrementStringNumber(
    							adDocumentSequenceAssignment.getDsaNextSequence()));

    				} catch (FinderException ex) {

    					ADJ_DCMNT_NMBR = adDocumentSequenceAssignment.getDsaNextSequence();
    					adDocumentSequenceAssignment.setDsaNextSequence(EJBCommon.incrementStringNumber(
    							adDocumentSequenceAssignment.getDsaNextSequence()));
    					break;

    				}

    			} else {

    				try {

    					invAdjustmentHome.findByAdjDocumentNumberAndBrCode(
    							adBranchDocumentSequenceAssignment.getBdsNextSequence(), AD_BRNCH, AD_CMPNY);
    					adBranchDocumentSequenceAssignment.setBdsNextSequence(EJBCommon.incrementStringNumber(
    							adBranchDocumentSequenceAssignment.getBdsNextSequence()));

    				} catch (FinderException ex) {

    					ADJ_DCMNT_NMBR = adBranchDocumentSequenceAssignment.getBdsNextSequence();
    					adBranchDocumentSequenceAssignment.setBdsNextSequence(EJBCommon.incrementStringNumber(
    							adBranchDocumentSequenceAssignment.getBdsNextSequence()));
    					break;

    				}

    			}

    		}

    		LocalInvAdjustment invAdjustment = invAdjustmentHome.create(ADJ_DCMNT_NMBR, ADJ_RFRNC_NMBR,
    				ADJ_DSCRPTN, ADJ_DATE, "COST-VARIANCE", "N/A", EJBCommon.FALSE, USR_NM, ADJ_DATE, USR_NM, ADJ_DATE, null, null,
					USR_NM, ADJ_DATE, null, null, EJBCommon.TRUE, EJBCommon.FALSE, AD_BRNCH, AD_CMPNY);

    		return invAdjustment;

    	} catch (Exception ex) {

    		Debug.printStackTrace(ex);
    		getSessionContext().setRollbackOnly();
    		throw new EJBException(ex.getMessage());

    	}


    }

    private void postInvAdjustmentToInventory(LocalInvAdjustmentLine invAdjustmentLine, Date CST_DT, double CST_ADJST_QTY,
    		double CST_ADJST_CST, double CST_RMNNG_QTY, double CST_RMNNG_VL, Integer AD_BRNCH,Integer AD_CMPNY) {

    	Debug.print("ArMiscReceiptEntryControllerBean postInvAdjustmentToInventory");

    	LocalInvCostingHome invCostingHome = null;
    	LocalAdPreferenceHome adPreferenceHome = null;
    	LocalAdCompanyHome adCompanyHome = null;

    	// Initialize EJB Home

    	try {

    		invCostingHome = (LocalInvCostingHome)EJBHomeFactory.
			lookUpLocalHome(LocalInvCostingHome.JNDI_NAME, LocalInvCostingHome.class);
    		adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);
    		adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);

    	} catch (NamingException ex) {

    		getSessionContext().setRollbackOnly();
    		throw new EJBException(ex.getMessage());

    	}

    	try {

    		LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);
    		LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
    		LocalInvItemLocation invItemLocation = invAdjustmentLine.getInvItemLocation();
    		int CST_LN_NMBR = 0;

    		CST_ADJST_QTY = EJBCommon.roundIt(CST_ADJST_QTY, adPreference.getPrfInvQuantityPrecisionUnit());
    		CST_ADJST_CST = EJBCommon.roundIt(CST_ADJST_CST, adCompany.getGlFunctionalCurrency().getFcPrecision());
    		CST_RMNNG_QTY = EJBCommon.roundIt(CST_RMNNG_QTY, adPreference.getPrfInvQuantityPrecisionUnit());
    		CST_RMNNG_VL = EJBCommon.roundIt(CST_RMNNG_VL, adCompany.getGlFunctionalCurrency().getFcPrecision());

    		if (CST_ADJST_QTY < 0) {

    			invItemLocation.setIlCommittedQuantity(invItemLocation.getIlCommittedQuantity() - Math.abs(CST_ADJST_QTY));

    		}

    		// create costing

    		try {

    			// generate line number

    			LocalInvCosting invCurrentCosting = invCostingHome.getByMaxCstLineNumberAndCstDateToLongAndIiNameAndLocName(
    					CST_DT.getTime(), invItemLocation.getInvItem().getIiName(), invItemLocation.getInvLocation().getLocName(),
						AD_BRNCH, AD_CMPNY);
    			CST_LN_NMBR = invCurrentCosting.getCstLineNumber() + 1;

    		} catch (FinderException ex) {

    			CST_LN_NMBR = 1;

    		}

    		LocalInvCosting invCosting = invCostingHome.create(CST_DT, CST_DT.getTime(), CST_LN_NMBR, 0d, 0d, 0d, 0d,
    				CST_ADJST_QTY, CST_ADJST_CST, 0d, 0d, CST_RMNNG_QTY, CST_RMNNG_VL, 0d, 0d, CST_ADJST_QTY > 0 ? CST_ADJST_QTY : 0, AD_BRNCH, AD_CMPNY);
    		invItemLocation.addInvCosting(invCosting);
    		invCosting.setInvAdjustmentLine(invAdjustmentLine);

    		// propagate balance if necessary

    		Collection invCostings = invCostingHome.findByGreaterThanCstDateAndIiNameAndLocName(CST_DT, invItemLocation.getInvItem().getIiName(), invItemLocation.getInvLocation().getLocName(), AD_BRNCH, AD_CMPNY);

    		Iterator i = invCostings.iterator();

    		while (i.hasNext()) {

    			LocalInvCosting invPropagatedCosting = (LocalInvCosting)i.next();

    			invPropagatedCosting.setCstRemainingQuantity(invPropagatedCosting.getCstRemainingQuantity() + CST_ADJST_QTY);
    			invPropagatedCosting.setCstRemainingValue(invPropagatedCosting.getCstRemainingValue() + CST_ADJST_CST);

    		}


    	} catch (Exception ex) {

    		Debug.printStackTrace(ex);
    		getSessionContext().setRollbackOnly();
    		throw new EJBException(ex.getMessage());

    	}



    }


    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList getArCreditMemoReportParameters(Integer AD_CMPNY) {

        Debug.print("ArCreditMemoEntryControllerBean getArCreditMemoReportParameters");

        ArrayList list = new ArrayList();
        LocalAdLookUpValueHome adLookUpValueHome = null;


        // Initialize EJB Home

        try {

        	adLookUpValueHome = (LocalAdLookUpValueHome)EJBHomeFactory.
            lookUpLocalHome(LocalAdLookUpValueHome.JNDI_NAME, LocalAdLookUpValueHome.class);

        } catch (NamingException ex) {

            throw new EJBException(ex.getMessage());

        }


        try {

        	Collection adLookUpValues = adLookUpValueHome.findByLuName("AR PRINT INVOICE PARAMETER", AD_CMPNY);

        	if(adLookUpValues.size() <=0 ) {
        		return list;
        	}

        	Iterator i = adLookUpValues.iterator();

        	while(i.hasNext()) {

        		LocalAdLookUpValue adLookUpValue = (LocalAdLookUpValue)i.next();
        		System.out.println(adLookUpValue.getLvName());
        		list.add(adLookUpValue.getLvName());
        	}


        	return list;



        } catch (Exception ex) {

            Debug.printStackTrace(ex);
            throw new EJBException(ex.getMessage());

        }

    }


	// SessionBean methods

    /**
     * @ejb:create-method view-type="remote"
     **/
    public void ejbCreate() throws CreateException {

       Debug.print("ArCreditMemoEntryControllerBean ejbCreate");

    }


}