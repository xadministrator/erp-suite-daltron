
/*
 * ArCustomerClassControllerBean.java
 *
 * Created on March 08, 2004, 9:18 AM
 *
 * @author  Neil Andrew M. Ajero
 */
 
package com.ejb.txn;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;
import javax.naming.NamingException;

import com.ejb.ad.LocalAdCompany;
import com.ejb.ad.LocalAdCompanyHome;
import com.ejb.ad.LocalAdLookUpValue;
import com.ejb.ad.LocalAdLookUpValueHome;
import com.ejb.ar.LocalArAutoAccountingSegmentHome;
import com.ejb.ar.LocalArCustomerClass;
import com.ejb.ar.LocalArCustomerClassHome;
import com.ejb.ar.LocalArTaxCode;
import com.ejb.ar.LocalArTaxCodeHome;
import com.ejb.ar.LocalArWithholdingTaxCode;
import com.ejb.ar.LocalArWithholdingTaxCodeHome;
import com.ejb.exception.ArCCCoaGlEarnedInterestAccountNotFoundException;
import com.ejb.exception.ArCCCoaGlEarnedPenaltyAccountNotFoundException;
import com.ejb.exception.ArCCCoaGlReceivableAccountNotFoundException;
import com.ejb.exception.ArCCCoaGlRevenueAccountNotFoundException;
import com.ejb.exception.ArCCCoaGlUnEarnedInterestAccountNotFoundException;
import com.ejb.exception.ArCCCoaGlUnEarnedPenaltyAccountNotFoundException;
import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.exception.GlobalRecordAlreadyAssignedException;
import com.ejb.exception.GlobalRecordAlreadyDeletedException;
import com.ejb.exception.GlobalRecordAlreadyExistException;
import com.ejb.gl.LocalGlChartOfAccount;
import com.ejb.gl.LocalGlChartOfAccountHome;
import com.util.AbstractSessionBean;
import com.util.ArModCustomerClassDetails;
import com.util.Debug;
import com.util.EJBCommon;
import com.util.EJBHomeFactory;

/**
 * @ejb:bean name="ArCustomerClassControllerEJB"
 *           display-name="Used for entering customer classes"
 *           type="Stateless"
 *           view-type="remote"
 *           jndi-name="ejb/ArCustomerClassControllerEJB"
 *
 * @ejb:interface remote-class="com.ejb.txn.ArCustomerClassController"
 *                extends="javax.ejb.EJBObject"
 *
 * @ejb:home remote-class="com.ejb.txn.ArCustomerClassControllerHome"
 *           extends="javax.ejb.EJBHome"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="aruser"
 *                        role-link="aruserlink"
 *
 * @ejb:permission role-name="aruser"
 * 
*/

public class ArCustomerClassControllerBean extends AbstractSessionBean {
	
	
	
	/**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
	public ArrayList getAdLvInvPriceLevelAll(Integer AD_CMPNY) {
		
		Debug.print("ArCustomerClassControllerBean getAdLvInvPriceLevelAll");
		
		LocalAdLookUpValueHome adLookUpValueHome = null;
		
		ArrayList list = new ArrayList();
		
		//Initialize EJB Home
		
		try {
			
			adLookUpValueHome = (LocalAdLookUpValueHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdLookUpValueHome.JNDI_NAME, LocalAdLookUpValueHome.class);
			
		} catch (NamingException ex) {
			
			throw new EJBException(ex.getMessage());
			
		}
		
		try {
			
			Collection adLookUpValues = adLookUpValueHome.findByLuName("INV PRICE LEVEL", AD_CMPNY);
			
			Iterator i = adLookUpValues.iterator();
			
			while (i.hasNext()) {
				
				LocalAdLookUpValue adLookUpValue = (LocalAdLookUpValue)i.next();
				
				list.add(adLookUpValue.getLvName());
				
			}
			
			return list;
			
		} catch (Exception ex) {
			
			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());
			
		}
		
		
	}




/**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
	public ArrayList getAdLvCustomerBatchAll(Integer AD_CMPNY) {
		
		Debug.print("ArCustomerClassControllerBean getAdLvCustomerBatchAll");
		
		LocalAdLookUpValueHome adLookUpValueHome = null;               
		
		ArrayList list = new ArrayList();
		
		// Initialize EJB Home
		
		try {
			
			adLookUpValueHome = (LocalAdLookUpValueHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdLookUpValueHome.JNDI_NAME, LocalAdLookUpValueHome.class);
			
		} catch (NamingException ex) {
			
			throw new EJBException(ex.getMessage());
			
		}        
		
		try {
			
			Collection adLookUpValues = adLookUpValueHome.findByLuName("AR CUSTOMER BATCH - SOA", AD_CMPNY);
			
			Iterator i = adLookUpValues.iterator();
			
			while (i.hasNext()) {
				
				LocalAdLookUpValue adLookUpValue = (LocalAdLookUpValue)i.next();
				
				list.add(adLookUpValue.getLvName());
				
			}
			
			return list;
			
		} catch (Exception ex) {
			
			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());
			
		}
		
	}
	

    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList getArCcAll(Integer AD_CMPNY) 
        throws GlobalNoRecordFoundException {
                    
        Debug.print("ArCustomerClassControllerBean getArCcAll");

        LocalArCustomerClassHome arCustomerClassHome = null;
        LocalGlChartOfAccountHome glChartOfAccountHome = null;


        ArrayList list = new ArrayList();
        
        // Initialize EJB Home

        try {

            arCustomerClassHome = (LocalArCustomerClassHome)EJBHomeFactory.
                lookUpLocalHome(LocalArCustomerClassHome.JNDI_NAME, LocalArCustomerClassHome.class);
            glChartOfAccountHome = (LocalGlChartOfAccountHome)EJBHomeFactory.
                lookUpLocalHome(LocalGlChartOfAccountHome.JNDI_NAME, LocalGlChartOfAccountHome.class);                

        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {
        	        	
	        Collection arCustomerClasses = arCustomerClassHome.findCcAll(AD_CMPNY);
	
	        if (arCustomerClasses.isEmpty()) {
	
	            throw new GlobalNoRecordFoundException();
	        	
	        }
	            
	        Iterator i = arCustomerClasses.iterator();
	               
	        while (i.hasNext()) {
	        	
	        	LocalArCustomerClass arCustomerClass = (LocalArCustomerClass)i.next();
	        	
		        LocalGlChartOfAccount glReceivableAccount = glChartOfAccountHome.findByPrimaryKey(arCustomerClass.getCcGlCoaReceivableAccount());		        		        
		        
		        LocalGlChartOfAccount glRevenueAccount = null;
		        LocalGlChartOfAccount glUnEarnedInterestAccount = null;
		        LocalGlChartOfAccount glEarnedInterestAccount = null;
		        LocalGlChartOfAccount glUnEarnedPenaltyAccount = null;
		        LocalGlChartOfAccount glEarnedPenaltyAccount = null;
		        
		        if (arCustomerClass.getCcGlCoaRevenueAccount() != null) {               
		        			        	                             		        	
		        	glRevenueAccount = glChartOfAccountHome.findByPrimaryKey(arCustomerClass.getCcGlCoaRevenueAccount());                    
		        	
		        }
		        
		        if (arCustomerClass.getCcGlCoaUnEarnedInterestAccount() != null) {               
 		        	
		        	glUnEarnedInterestAccount = glChartOfAccountHome.findByPrimaryKey(arCustomerClass.getCcGlCoaUnEarnedInterestAccount());                    
		        	
		        }
		        
		        if (arCustomerClass.getCcGlCoaEarnedInterestAccount() != null) {               
 		        	
		        	glEarnedInterestAccount = glChartOfAccountHome.findByPrimaryKey(arCustomerClass.getCcGlCoaEarnedInterestAccount());                    
		        	
		        }
		        
		        
		        if (arCustomerClass.getCcGlCoaUnEarnedPenaltyAccount() != null) {               
 		        	
		        	glUnEarnedPenaltyAccount = glChartOfAccountHome.findByPrimaryKey(arCustomerClass.getCcGlCoaUnEarnedPenaltyAccount());                    
		        	
		        }
		        
		        if (arCustomerClass.getCcGlCoaEarnedPenaltyAccount() != null) {               
 		        	
		        	glEarnedPenaltyAccount = glChartOfAccountHome.findByPrimaryKey(arCustomerClass.getCcGlCoaEarnedPenaltyAccount());                    
		        	
		        }
		        
		        			        	                             
	        	ArModCustomerClassDetails mdetails = new ArModCustomerClassDetails();
	        		mdetails.setCcCode(arCustomerClass.getCcCode());        		
	                mdetails.setCcName(arCustomerClass.getCcName());                
	                mdetails.setCcDescription(arCustomerClass.getCcDescription());
	                mdetails.setCcNextCustomerCode(arCustomerClass.getCcNextCustomerCode());
	                mdetails.setCcCustomerBatch(arCustomerClass.getCcCustomerBatch());
	                mdetails.setCcDealPrice(arCustomerClass.getCcDealPrice());
	                mdetails.setCcMonthlyInterestRate(arCustomerClass.getCcMonthlyInterestRate());
	                mdetails.setCcMinimumFinanceCharge(arCustomerClass.getCcMinimumFinanceCharge());
	                mdetails.setCcGracePeriodDay(arCustomerClass.getCcGracePeriodDay());
	                mdetails.setCcDaysInPeriod(arCustomerClass.getCcDaysInPeriod());
	                mdetails.setCcChargeByDueDate(arCustomerClass.getCcChargeByDueDate());
                    
                    mdetails.setCcTcName(arCustomerClass.getArTaxCode().getTcName());
                    mdetails.setCcWtcName(arCustomerClass.getArWithholdingTaxCode().getWtcName());                                        
                    mdetails.setCcGlCoaFinanceChargeAccountNumber(null);
                    mdetails.setCcGlCoaReceivableAccountNumber(glReceivableAccount.getCoaAccountNumber());
                    mdetails.setCcGlCoaRevenueAccountNumber(glRevenueAccount != null ? glRevenueAccount.getCoaAccountNumber() : null);     		    
                    
                    mdetails.setCcGlCoaUnEarnedInterestAccountNumber(glUnEarnedInterestAccount != null ? glUnEarnedInterestAccount.getCoaAccountNumber() : null);
                    mdetails.setCcGlCoaEarnedInterestAccountNumber(glEarnedInterestAccount != null ? glEarnedInterestAccount.getCoaAccountNumber() : null);
                    mdetails.setCcGlCoaUnEarnedPenaltyAccountNumber(glUnEarnedPenaltyAccount != null ? glUnEarnedPenaltyAccount.getCoaAccountNumber() : null);
                    mdetails.setCcGlCoaEarnedPenaltyAccountNumber(glEarnedPenaltyAccount != null ? glEarnedPenaltyAccount.getCoaAccountNumber() : null);
                    
                    
                    mdetails.setCcGlCoaReceivableAccountDescription(glReceivableAccount.getCoaAccountDescription());
                    mdetails.setCcGlCoaRevenueAccountDescription(glRevenueAccount != null ? glRevenueAccount.getCoaAccountDescription() : null);
                    
                    mdetails.setCcGlCoaUnEarnedInterestAccountDescription(glUnEarnedInterestAccount != null ? glUnEarnedInterestAccount.getCoaAccountDescription() : null);
                    mdetails.setCcGlCoaEarnedInterestAccountDescription(glEarnedInterestAccount != null ? glEarnedInterestAccount.getCoaAccountDescription() : null);
                    mdetails.setCcGlCoaUnEarnedPenaltyAccountDescription(glUnEarnedPenaltyAccount != null ? glUnEarnedPenaltyAccount.getCoaAccountDescription() : null);
                    mdetails.setCcGlCoaEarnedPenaltyAccountDescription(glEarnedPenaltyAccount != null ? glEarnedPenaltyAccount.getCoaAccountDescription() : null);
                    
                    mdetails.setCcEnable(arCustomerClass.getCcEnable());
                    mdetails.setCcEnableRebate(arCustomerClass.getCcEnableRebate());
                    mdetails.setCcAutoComputeInterest(arCustomerClass.getCcAutoComputeInterest());
                    mdetails.setCcAutoComputePenalty(arCustomerClass.getCcAutoComputePenalty());
                    
				    mdetails.setCcCreditLimit(arCustomerClass.getCcCreditLimit());
				    // get revenue account description
				    
				    if (glRevenueAccount != null) {

					    mdetails.setCcGlCoaRevenueAccountDescription(glReceivableAccount.getCoaAccountDescription());
				    	    
				    }	    
				    	    
	        		list.add(mdetails);
	        		
	        }              
	                                                        		        		        
	        return list;
	        
        } catch (GlobalNoRecordFoundException ex) {
        	
        	throw ex;
        	        	
        } catch (Exception ex) {
        	
        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());
        	
        }
            
    }
    
    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList getArTcAll(Integer AD_CMPNY) {
                    
        Debug.print("ArCustomerClassControllerBean getArTcAll");
        
        LocalArTaxCodeHome arTaxCodeHome = null;
        
        Collection arTaxCodes = null;
        
        LocalArTaxCode arTaxCode = null;
        
        ArrayList list = new ArrayList();
        
        // Initialize EJB Home
        
        try {
            
            arTaxCodeHome = (LocalArTaxCodeHome)EJBHomeFactory.
                lookUpLocalHome(LocalArTaxCodeHome.JNDI_NAME, LocalArTaxCodeHome.class);
            
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {
            
            arTaxCodes = arTaxCodeHome.findEnabledTcAll(AD_CMPNY);
            
	        Iterator i = arTaxCodes.iterator();
	               
	        while (i.hasNext()) {
	        	
	            arTaxCode = (LocalArTaxCode)i.next();
	        	
	            list.add(arTaxCode.getTcName());
	        	
	        }
	        
	        return list;
	        
        } catch (Exception ex) {
        	
        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());
        	
        }	        
            
    }  
    
    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList getArWtcAll(Integer AD_CMPNY) {
                    
        Debug.print("ArCustomerClassControllerBean getArWtcAll");
        
        LocalArWithholdingTaxCodeHome arWithholdingTaxCodeHome = null;
        
        Collection arWithholdingTaxCodes = null;
        
        LocalArWithholdingTaxCode arWithholdingTaxCode = null;
        
        ArrayList list = new ArrayList();
        
        // Initialize EJB Home
        
        try {
            
            arWithholdingTaxCodeHome = (LocalArWithholdingTaxCodeHome)EJBHomeFactory.
                lookUpLocalHome(LocalArWithholdingTaxCodeHome.JNDI_NAME, LocalArWithholdingTaxCodeHome.class);
            
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {
            
            arWithholdingTaxCodes = arWithholdingTaxCodeHome.findEnabledWtcAll(AD_CMPNY);

	        Iterator i = arWithholdingTaxCodes.iterator();
	               
	        while (i.hasNext()) {
	        	
	            arWithholdingTaxCode = (LocalArWithholdingTaxCode)i.next();
	        	
	            list.add(arWithholdingTaxCode.getWtcName());
	        	
	        }
        
	        return list;
	        
        } catch (Exception ex) {
        	
        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());
        	
        }        
            
    }            
    
    /**
     * @ejb:interface-method view-type="remote"
     **/    
    public void addArCcEntry(com.util.ArModCustomerClassDetails mdetails, String TC_NM, String WTC_NM, Integer AD_CMPNY) 
        throws GlobalRecordAlreadyExistException,               
			   ArCCCoaGlReceivableAccountNotFoundException,
			   ArCCCoaGlRevenueAccountNotFoundException, 
			   ArCCCoaGlUnEarnedInterestAccountNotFoundException, 
			   ArCCCoaGlEarnedInterestAccountNotFoundException, 
			   ArCCCoaGlUnEarnedPenaltyAccountNotFoundException, 
			   ArCCCoaGlEarnedPenaltyAccountNotFoundException
    		{
                    
        Debug.print("ArCustomerClassControllerBean addArCcEntry");
        
        LocalArCustomerClassHome arCustomerClassHome = null;
        LocalGlChartOfAccountHome glChartOfAccountHome = null;
        LocalArTaxCodeHome arTaxCodeHome = null;
        LocalArWithholdingTaxCodeHome arWithholdingTaxCodeHome = null;
            
        ArrayList list = new ArrayList();
        
        // Initialize EJB Home
        
        try {
            
            arCustomerClassHome = (LocalArCustomerClassHome)EJBHomeFactory.
                lookUpLocalHome(LocalArCustomerClassHome.JNDI_NAME, LocalArCustomerClassHome.class);
            glChartOfAccountHome = (LocalGlChartOfAccountHome)EJBHomeFactory.
                lookUpLocalHome(LocalGlChartOfAccountHome.JNDI_NAME, LocalGlChartOfAccountHome.class);
            arTaxCodeHome = (LocalArTaxCodeHome)EJBHomeFactory.
                lookUpLocalHome(LocalArTaxCodeHome.JNDI_NAME, LocalArTaxCodeHome.class);
            arWithholdingTaxCodeHome = (LocalArWithholdingTaxCodeHome)EJBHomeFactory.
                lookUpLocalHome(LocalArWithholdingTaxCodeHome.JNDI_NAME, LocalArWithholdingTaxCodeHome.class);                
                
                                                      
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {
        	
            LocalArCustomerClass arCustomerClass = null;
	        LocalGlChartOfAccount glReceivableAccount = null;
	        LocalGlChartOfAccount glRevenueAccount = null;
	        
	        LocalGlChartOfAccount glUnEarnedInterestAccount = null;
	        LocalGlChartOfAccount glEarnedInterestAccount = null;
	        LocalGlChartOfAccount glUnEarnedPenaltyAccount = null;
	        LocalGlChartOfAccount glEarnedPenaltyAccount = null;

	        try { 
	            
	            arCustomerClass = arCustomerClassHome.findByCcName(mdetails.getCcName(), AD_CMPNY);
	            
	            throw new GlobalRecordAlreadyExistException();
	            
	        } catch (FinderException ex) {
	         	
	        	 	
	        }
	         
	        // get receivable and revenue account to validate accounts 
	        
	        
	        try {

				glReceivableAccount = glChartOfAccountHome.findByCoaAccountNumber(mdetails.
	            	getCcGlCoaReceivableAccountNumber(), AD_CMPNY);	            	
	            	
	        } catch (FinderException ex) {
	        
	            throw new ArCCCoaGlReceivableAccountNotFoundException();
	                       
	        }
	        
	        try {

				glRevenueAccount = glChartOfAccountHome.findByCoaAccountNumber(mdetails.
	            	getCcGlCoaRevenueAccountNumber(), AD_CMPNY);	            	
	            	
	        } catch (FinderException ex) {
	            
	            if (this.getArCcGlCoaRevenueAccountEnable(AD_CMPNY)) {
	            	
	            	throw new ArCCCoaGlRevenueAccountNotFoundException();
	            	
	            }
	                       
	        }
	    
	        if(mdetails.getCcAutoComputeInterest() == EJBCommon.TRUE){
	        	
                     try {

							glUnEarnedInterestAccount = glChartOfAccountHome.findByCoaAccountNumber(mdetails.
				            	getCcGlCoaUnEarnedInterestAccountNumber(), AD_CMPNY);	            	
	            	
                    } catch (FinderException ex) {

                            throw new ArCCCoaGlUnEarnedInterestAccountNotFoundException(); 

                    }

                    try {

                           	glEarnedInterestAccount = glChartOfAccountHome.findByCoaAccountNumber(mdetails.
                            getCcGlCoaEarnedInterestAccountNumber(), AD_CMPNY);	            	

                    } catch (FinderException ex) {

                            throw new ArCCCoaGlEarnedInterestAccountNotFoundException(); 

                    }
                }
                
            if(mdetails.getCcAutoComputePenalty() == EJBCommon.TRUE){
                	
                	
                      try {

							glUnEarnedPenaltyAccount = glChartOfAccountHome.findByCoaAccountNumber(mdetails.
				            	getCcGlCoaUnEarnedPenaltyAccountNumber(), AD_CMPNY);	            	
	            	
				        } catch (FinderException ex) {
				            
				            	throw new ArCCCoaGlUnEarnedPenaltyAccountNotFoundException(); 
				                       
				        }
	        
				        try {
			
							glEarnedPenaltyAccount = glChartOfAccountHome.findByCoaAccountNumber(mdetails.
				            	getCcGlCoaEarnedPenaltyAccountNumber(), AD_CMPNY);	            	
				            	
				        } catch (FinderException ex) {
				            
				            	throw new ArCCCoaGlEarnedPenaltyAccountNotFoundException(); 
				                       
				        }
            }
	       
	        
	      
	            		            	
	    	// create new customer class
	    				    	        	        	    	
	    	arCustomerClass = arCustomerClassHome.create(mdetails.getCcName(),
	    	        mdetails.getCcDescription(),
	    	        mdetails.getCcNextCustomerCode(),
	    	        mdetails.getCcCustomerBatch(),
	    	        mdetails.getCcDealPrice(),
	    	        mdetails.getCcMonthlyInterestRate(),
	    	        mdetails.getCcMinimumFinanceCharge(), mdetails.getCcGracePeriodDay(),
	    	        mdetails.getCcDaysInPeriod(), null,
	    	        mdetails.getCcChargeByDueDate(), 
	    	        glReceivableAccount.getCoaCode(), 
	    	        glRevenueAccount != null ? glRevenueAccount.getCoaCode() : null, 
	    	        glUnEarnedInterestAccount != null ? glUnEarnedInterestAccount.getCoaCode() : null, 		
	    	        glEarnedInterestAccount != null ? glEarnedInterestAccount.getCoaCode() : null, 		
	    	        glUnEarnedPenaltyAccount != null ? glUnEarnedPenaltyAccount.getCoaCode() : null, 		
	    	    	glEarnedPenaltyAccount != null ? glEarnedPenaltyAccount.getCoaCode() : null, 		   	    	    	        		
	    	        mdetails.getCcEnable(),
	    	        mdetails.getCcEnableRebate(),
	    	        mdetails.getCcAutoComputeInterest(),
	    	        mdetails.getCcAutoComputePenalty(),
	    	        mdetails.getCcCreditLimit(), AD_CMPNY);

	        LocalArTaxCode arTaxCode = arTaxCodeHome.findByTcName(TC_NM, AD_CMPNY);
	        arTaxCode.addArCustomerClass(arCustomerClass);
	        
	        LocalArWithholdingTaxCode arWithholdingTaxCode = arWithholdingTaxCodeHome.findByWtcName(WTC_NM, AD_CMPNY);	    	        
	        arWithholdingTaxCode.addArCustomerClass(arCustomerClass); 	
	    	        	    		        	    		    		    			        	        
        } catch (GlobalRecordAlreadyExistException ex) {
        	
        	getSessionContext().setRollbackOnly();
        	throw ex;
        	        	
        } catch (ArCCCoaGlReceivableAccountNotFoundException ex) {
        	
        	getSessionContext().setRollbackOnly();
        	throw ex;

        } catch (ArCCCoaGlRevenueAccountNotFoundException ex) {
        	
        	getSessionContext().setRollbackOnly();
        	throw ex;
        	
        } catch (ArCCCoaGlUnEarnedInterestAccountNotFoundException ex) {
        	
        	getSessionContext().setRollbackOnly();
        	throw ex;
        
        } catch (ArCCCoaGlEarnedInterestAccountNotFoundException ex) {
        	
        	getSessionContext().setRollbackOnly();
        	throw ex;
        
        } catch (ArCCCoaGlUnEarnedPenaltyAccountNotFoundException ex) {
        	
        	getSessionContext().setRollbackOnly();
        	throw ex;
        
        } catch (ArCCCoaGlEarnedPenaltyAccountNotFoundException ex) {
        	
        	getSessionContext().setRollbackOnly();
        	throw ex;
        	
        	        	
        } catch (Exception ex) {
        	
        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());
        	
        }
            
    }    
    
    /**
     * @ejb:interface-method view-type="remote"
     **/
    public void updateArCcEntry(com.util.ArModCustomerClassDetails mdetails, String TC_NM, String WTC_NM, Integer AD_CMPNY) 
        throws GlobalRecordAlreadyExistException,                
			   ArCCCoaGlReceivableAccountNotFoundException,
			   ArCCCoaGlReceivableAccountNotFoundException,
			   ArCCCoaGlRevenueAccountNotFoundException, 
			   ArCCCoaGlUnEarnedInterestAccountNotFoundException, 
			   ArCCCoaGlEarnedInterestAccountNotFoundException, 
			   ArCCCoaGlUnEarnedPenaltyAccountNotFoundException, 
			   ArCCCoaGlEarnedPenaltyAccountNotFoundException
			   
    	{
                    
        Debug.print("ArCustomerClassControllerBean updateArCcEntry");
        
        LocalArCustomerClassHome arCustomerClassHome = null;
        LocalGlChartOfAccountHome glChartOfAccountHome = null;
        LocalArTaxCodeHome arTaxCodeHome = null;
        LocalArWithholdingTaxCodeHome arWithholdingTaxCodeHome = null;
                
        ArrayList list = new ArrayList();
        
        // Initialize EJB Home
        
        try {

            arCustomerClassHome = (LocalArCustomerClassHome)EJBHomeFactory.
                lookUpLocalHome(LocalArCustomerClassHome.JNDI_NAME, LocalArCustomerClassHome.class);
            glChartOfAccountHome = (LocalGlChartOfAccountHome)EJBHomeFactory.
                lookUpLocalHome(LocalGlChartOfAccountHome.JNDI_NAME, LocalGlChartOfAccountHome.class);
            arTaxCodeHome = (LocalArTaxCodeHome)EJBHomeFactory.
                lookUpLocalHome(LocalArTaxCodeHome.JNDI_NAME, LocalArTaxCodeHome.class);
            arWithholdingTaxCodeHome = (LocalArWithholdingTaxCodeHome)EJBHomeFactory.
                lookUpLocalHome(LocalArWithholdingTaxCodeHome.JNDI_NAME, LocalArWithholdingTaxCodeHome.class);                
                         
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {
        
            LocalArCustomerClass arCustomerClass = null;
	        LocalGlChartOfAccount glFinanceChargeAccount = null;
	        LocalGlChartOfAccount glReceivableAccount = null;
	        LocalGlChartOfAccount glRevenueAccount = null;
	        LocalGlChartOfAccount glUnEarnedInterestAccount = null;
	        LocalGlChartOfAccount glEarnedInterestAccount = null;
	        LocalGlChartOfAccount glUnEarnedPenaltyAccount = null;
	        LocalGlChartOfAccount glEarnedPenaltyAccount = null;
        	
        	try {
        	                   
	            LocalArCustomerClass arExistingCustomerClass = arCustomerClassHome.findByCcName(mdetails.getCcName(), AD_CMPNY);
	            
	            if (!arExistingCustomerClass.getCcCode().equals(mdetails.getCcCode())) {
	            
	                 throw new GlobalRecordAlreadyExistException();
	                 
	            }
	            
	        } catch (FinderException ex) {
	        	
	        }
            
	        // get receivable and revenue account to validate accounts 
	        	        
	        try {

				glReceivableAccount = glChartOfAccountHome.findByCoaAccountNumber(mdetails.
	            	getCcGlCoaReceivableAccountNumber(), AD_CMPNY);	            	
	            	
	        } catch (FinderException ex) {
	        
	            throw new ArCCCoaGlReceivableAccountNotFoundException();
	                       
	        }
	        
	        try {

				glRevenueAccount = glChartOfAccountHome.findByCoaAccountNumber(mdetails.
	            	getCcGlCoaRevenueAccountNumber(), AD_CMPNY);	            	
	            	
	        } catch (FinderException ex) {
	        
	            if (this.getArCcGlCoaRevenueAccountEnable(AD_CMPNY)) {
	            	
	            	throw new ArCCCoaGlRevenueAccountNotFoundException();
	            	
	            }
	            
	        }
	        
	        if(mdetails.getCcAutoComputeInterest() == EJBCommon.TRUE){
	        	
	        	try {

					glUnEarnedInterestAccount = glChartOfAccountHome.findByCoaAccountNumber(mdetails.
		            	getCcGlCoaUnEarnedInterestAccountNumber(), AD_CMPNY);	            	
		            	
		        } catch (FinderException ex) {
		            
		            	throw new ArCCCoaGlUnEarnedInterestAccountNotFoundException(); 
		                       
		        }
		        
		        try {

					glEarnedInterestAccount = glChartOfAccountHome.findByCoaAccountNumber(mdetails.
		            	getCcGlCoaEarnedInterestAccountNumber(), AD_CMPNY);	            	
		            	
		        } catch (FinderException ex) {
		            
		            	throw new ArCCCoaGlEarnedInterestAccountNotFoundException(); 
		                       
		        }
	        	
	        }
	        
	        
	        if(mdetails.getCcAutoComputePenalty() == EJBCommon.TRUE){
	        	
	        	
	        	try {

					glUnEarnedPenaltyAccount = glChartOfAccountHome.findByCoaAccountNumber(mdetails.
		            	getCcGlCoaUnEarnedPenaltyAccountNumber(), AD_CMPNY);	            	
		            	
		        } catch (FinderException ex) {
		            
		            	throw new ArCCCoaGlUnEarnedPenaltyAccountNotFoundException(); 
		                       
		        }
		        
		        try {

					glEarnedPenaltyAccount = glChartOfAccountHome.findByCoaAccountNumber(mdetails.
		            	getCcGlCoaEarnedPenaltyAccountNumber(), AD_CMPNY);	            	
		            	
		        } catch (FinderException ex) {
		            
		            	throw new ArCCCoaGlEarnedPenaltyAccountNotFoundException(); 
		                       
		        }
	        	
	        	
	        }
	        

			// find and update customer class
	        System.out.println("--------------------------->");
			arCustomerClass = arCustomerClassHome.findByPrimaryKey(mdetails.getCcCode());

				arCustomerClass.setCcName(mdetails.getCcName());
				arCustomerClass.setCcDescription(mdetails.getCcDescription());
				arCustomerClass.setCcNextCustomerCode(mdetails.getCcNextCustomerCode());
				arCustomerClass.setCcCustomerBatch(mdetails.getCcCustomerBatch());
				arCustomerClass.setCcDealPrice(mdetails.getCcDealPrice());
				arCustomerClass.setCcMonthlyInterestRate(mdetails.getCcMonthlyInterestRate());
				arCustomerClass.setCcMinimumFinanceCharge(mdetails.getCcMinimumFinanceCharge());
				arCustomerClass.setCcGracePeriodDay(mdetails.getCcGracePeriodDay());
				arCustomerClass.setCcDaysInPeriod(mdetails.getCcDaysInPeriod());
				arCustomerClass.setCcGlCoaChargeAccount(null);
				arCustomerClass.setCcChargeByDueDate(mdetails.getCcChargeByDueDate());
				arCustomerClass.setCcGlCoaReceivableAccount(glReceivableAccount.getCoaCode());
				arCustomerClass.setCcGlCoaRevenueAccount(glRevenueAccount != null ? glRevenueAccount.getCoaCode() : null);
				
				arCustomerClass.setCcGlCoaUnEarnedInterestAccount(glUnEarnedInterestAccount != null ? glUnEarnedInterestAccount.getCoaCode() : null); 		
				arCustomerClass.setCcGlCoaEarnedInterestAccount(glEarnedInterestAccount != null ? glEarnedInterestAccount.getCoaCode() : null); 		
				arCustomerClass.setCcGlCoaUnEarnedPenaltyAccount(glUnEarnedPenaltyAccount != null ? glUnEarnedPenaltyAccount.getCoaCode() : null); 		
				arCustomerClass.setCcGlCoaEarnedPenaltyAccount(glEarnedPenaltyAccount != null ? glEarnedPenaltyAccount.getCoaCode() : null);
				
				System.out.println("mdetails.getCcAutoComputeInterest()="+mdetails.getCcAutoComputeInterest());
				System.out.println("mdetails.getCcAutoComputePenalty()="+mdetails.getCcAutoComputePenalty());
				arCustomerClass.setCcEnable(mdetails.getCcEnable());
				arCustomerClass.setCcEnableRebate(mdetails.getCcEnableRebate());
				arCustomerClass.setCcAutoComputeInterest(mdetails.getCcAutoComputeInterest());
				arCustomerClass.setCcAutoComputePenalty(mdetails.getCcAutoComputePenalty());
				arCustomerClass.setCcCreditLimit(mdetails.getCcCreditLimit());
          										    		    		        			    		    		        
			LocalArTaxCode arTaxCode = arTaxCodeHome.findByTcName(TC_NM, AD_CMPNY);
			arTaxCode.addArCustomerClass(arCustomerClass);

			LocalArWithholdingTaxCode arWithholdingTaxCode = arWithholdingTaxCodeHome.findByWtcName(WTC_NM, AD_CMPNY);			    		    		        
			arWithholdingTaxCode.addArCustomerClass(arCustomerClass);
		    		
        } catch (GlobalRecordAlreadyExistException ex) {
        	
        	getSessionContext().setRollbackOnly();
        	throw ex;
        	        	
        } catch (ArCCCoaGlReceivableAccountNotFoundException ex) {
        	
        	getSessionContext().setRollbackOnly();
        	throw ex;

        } catch (ArCCCoaGlRevenueAccountNotFoundException ex) {
        	
        	getSessionContext().setRollbackOnly();
        	throw ex;
        	
        	
        } catch (ArCCCoaGlUnEarnedInterestAccountNotFoundException ex) {
        	
        	getSessionContext().setRollbackOnly();
        	throw ex;
        
        } catch (ArCCCoaGlEarnedInterestAccountNotFoundException ex) {
        	
        	getSessionContext().setRollbackOnly();
        	throw ex;
        
        } catch (ArCCCoaGlUnEarnedPenaltyAccountNotFoundException ex) {
        	
        	getSessionContext().setRollbackOnly();
        	throw ex;
        
        } catch (ArCCCoaGlEarnedPenaltyAccountNotFoundException ex) {
        	
        	getSessionContext().setRollbackOnly();
        	throw ex;
        
        	        	        	
        } catch (Exception ex) {
        	
        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());
        	
        }
            
    }    
    
   /**
    * @ejb:interface-method view-type="remote"
    **/
    public void deleteArCcEntry(Integer CC_CODE, Integer AD_CMPNY) 
        throws GlobalRecordAlreadyDeletedException,
                GlobalRecordAlreadyAssignedException {

        Debug.print("ArCustomerClassControllerBean deleteArCcEntry");
      
        LocalArCustomerClassHome arCustomerClassHome = null;

        // Initialize EJB Home
        
        try {

            arCustomerClassHome = (LocalArCustomerClassHome)EJBHomeFactory.
                lookUpLocalHome(LocalArCustomerClassHome.JNDI_NAME, LocalArCustomerClassHome.class);           
      
        } catch (NamingException ex) {

            throw new EJBException(ex.getMessage());

        }
      
        try {
      	
      	    LocalArCustomerClass arCustomerClass = null;                
      
	        try {
	      	
	            arCustomerClass = arCustomerClassHome.findByPrimaryKey(CC_CODE);
	         
	        } catch (FinderException ex) {
	      	
	            throw new GlobalRecordAlreadyDeletedException();
	            
	        }
	         
	        if (!arCustomerClass.getArCustomers().isEmpty()) {
	  
	            throw new GlobalRecordAlreadyAssignedException();
	         
	        }
	                            	
		    arCustomerClass.remove();
	    
        } catch (GlobalRecordAlreadyDeletedException ex) {
        	
        	getSessionContext().setRollbackOnly();
        	throw ex;
        	
        } catch (GlobalRecordAlreadyAssignedException ex) {
        	
        	getSessionContext().setRollbackOnly();
        	throw ex;
        	        	
        } catch (Exception ex) {
        	
        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());
        	
        }
            
    }
    
   /**
    * @ejb:interface-method view-type="remote"
    * @jboss:method-attributes read-only="true"
    **/
    public short getGlFcPrecisionUnit(Integer AD_CMPNY) {

       Debug.print("ArCustomerClassControllerBean getGlFcPrecisionUnit");

      
       LocalAdCompanyHome adCompanyHome = null;
      
     
       // Initialize EJB Home
        
       try {
            
           adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
              lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);       
            
       } catch (NamingException ex) {
            
           throw new EJBException(ex.getMessage());
            
       }

       try {
       	
         LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
            
         return  adCompany.getGlFunctionalCurrency().getFcPrecision();
                                     
       } catch (Exception ex) {
       	 
       	 Debug.printStackTrace(ex);
         throw new EJBException(ex.getMessage());
         
       }

    }
    
    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public boolean getArCcGlCoaRevenueAccountEnable(Integer AD_CMPNY) {
                    
        Debug.print("ArCustomerClassControllerBean getArCcGlCoaRevenueAccountEnable");
        
        LocalArAutoAccountingSegmentHome arAutoAccountingSegmentHome = null;
        
        // Initialize EJB Home
        
        try {
            
            arAutoAccountingSegmentHome = (LocalArAutoAccountingSegmentHome)EJBHomeFactory.
                lookUpLocalHome(LocalArAutoAccountingSegmentHome.JNDI_NAME, LocalArAutoAccountingSegmentHome.class);
            
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {
        	
        	Collection arAutoAccountingSegments = 
        	   arAutoAccountingSegmentHome.findByAasClassTypeAndAaAccountType("AR CUSTOMER", "REVENUE", AD_CMPNY);

            if (!arAutoAccountingSegments.isEmpty()) {
            	
            	return true;
            	
            } else {
            	
            	return false;
            	
            }

	        
        } catch (Exception ex) {
        	
        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());
        	
        }	        
            
    }                  
   
    // SessionBean methods

    /**
     * @ejb:create-method view-type="remote"
     **/
    public void ejbCreate() throws CreateException {

        Debug.print("ArCustomerClassControllerBean ejbCreate");
      
    }
    
}

