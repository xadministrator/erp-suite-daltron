
/*
 * ArCustomerEntryControllerBean.java
 *
 * Created on March 04, 2004, 9:36 AM
 *
 * @author  Neil Andrew M. Ajero
 */

package com.ejb.txn;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.StringTokenizer;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;
import javax.naming.NamingException;

import com.ejb.ad.LocalAdApproval;
import com.ejb.ad.LocalAdApprovalCoaLineHome;
import com.ejb.ad.LocalAdApprovalHome;
import com.ejb.ad.LocalAdApprovalQueue;
import com.ejb.ad.LocalAdApprovalQueueHome;
import com.ejb.ad.LocalAdBankAccount;
import com.ejb.ad.LocalAdBankAccountHome;
import com.ejb.ad.LocalAdBranch;
import com.ejb.ad.LocalAdBranchCoa;
import com.ejb.ad.LocalAdBranchCoaHome;
import com.ejb.ad.LocalAdBranchCustomer;
import com.ejb.ad.LocalAdBranchCustomerHome;
import com.ejb.ad.LocalAdBranchHome;
import com.ejb.ad.LocalAdBranchItemLocationHome;
import com.ejb.ad.LocalAdBranchOverheadMemoLineHome;
import com.ejb.ad.LocalAdBranchResponsibility;
import com.ejb.ad.LocalAdBranchResponsibilityHome;
import com.ejb.ad.LocalAdBranchStandardMemoLineHome;
import com.ejb.ad.LocalAdBranchSupplierHome;
import com.ejb.ad.LocalAdCompany;
import com.ejb.ad.LocalAdCompanyHome;
import com.ejb.ad.LocalAdLookUpValue;
import com.ejb.ad.LocalAdLookUpValueHome;
import com.ejb.ad.LocalAdPaymentTerm;
import com.ejb.ad.LocalAdPaymentTermHome;
import com.ejb.ad.LocalAdPreference;
import com.ejb.ad.LocalAdPreferenceHome;
import com.ejb.ad.LocalAdResponsibility;
import com.ejb.ad.LocalAdResponsibilityHome;
import com.ejb.ad.LocalAdUser;
import com.ejb.ad.LocalAdUserHome;
import com.ejb.ap.LocalApDistributionRecordHome;
import com.ejb.ap.LocalApPurchaseRequisition;
import com.ejb.ap.LocalApPurchaseRequisitionHome;
import com.ejb.ap.LocalApSupplier;
import com.ejb.ap.LocalApSupplierClassHome;
import com.ejb.ap.LocalApSupplierHome;
import com.ejb.ap.LocalApTaxCodeHome;
import com.ejb.ap.LocalApWithholdingTaxCodeHome;
import com.ejb.ar.LocalArAutoAccountingSegmentHome;
import com.ejb.ar.LocalArCustomer;
import com.ejb.ar.LocalArCustomerBalanceHome;
import com.ejb.ar.LocalArCustomerClass;
import com.ejb.ar.LocalArCustomerClassHome;
import com.ejb.ar.LocalArCustomerHome;
import com.ejb.ar.LocalArCustomerType;
import com.ejb.ar.LocalArCustomerTypeHome;
import com.ejb.ar.LocalArDistributionRecordHome;
import com.ejb.ar.LocalArInvoiceHome;
import com.ejb.ar.LocalArSalesperson;
import com.ejb.ar.LocalArSalespersonHome;
import com.ejb.ar.LocalArStandardMemoLineHome;
import com.ejb.ar.LocalArTaxCodeHome;
import com.ejb.ar.LocalArWithholdingTaxCodeHome;
import com.ejb.cm.LocalCmDistributionRecordHome;
import com.ejb.exception.ArCCCoaGlEarnedPenaltyAccountNotFoundException;
import com.ejb.exception.ArCCCoaGlReceivableAccountNotFoundException;
import com.ejb.exception.ArCCCoaGlRevenueAccountNotFoundException;
import com.ejb.exception.ArCCCoaGlUnEarnedInterestAccountNotFoundException;
import com.ejb.exception.ArCCCoaGlEarnedInterestAccountNotFoundException;
import com.ejb.exception.ArCCCoaGlUnEarnedPenaltyAccountNotFoundException;
import com.ejb.exception.GlCOAAccountNumberAlreadyAssignedException;
import com.ejb.exception.GlCOAAccountNumberHasParentValueException;
import com.ejb.exception.GlCOAAccountNumberIsInvalidException;
import com.ejb.exception.GlFCFunctionalCurrencyAlreadyAssignedException;
import com.ejb.exception.GlFCNoFunctionalCurrencyFoundException;
import com.ejb.exception.GlobalNameAndAddressAlreadyExistsException;
import com.ejb.exception.GlobalNoApprovalApproverFoundException;
import com.ejb.exception.GlobalNoApprovalRequesterFoundException;
import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.exception.GlobalRecordAlreadyExistException;
import com.ejb.genfld.LocalGenField;
import com.ejb.genfld.LocalGenFieldHome;
import com.ejb.gl.LocalGlAccountingCalendarValue;
import com.ejb.gl.LocalGlAccountingCalendarValueHome;
import com.ejb.gl.LocalGlChartOfAccount;
import com.ejb.gl.LocalGlChartOfAccountBalance;
import com.ejb.gl.LocalGlChartOfAccountBalanceHome;
import com.ejb.gl.LocalGlChartOfAccountHome;
import com.ejb.gl.LocalGlFunctionalCurrency;
import com.ejb.gl.LocalGlFunctionalCurrencyHome;
import com.ejb.gl.LocalGlInvestorAccountBalance;
import com.ejb.gl.LocalGlInvestorAccountBalanceHome;
import com.ejb.gl.LocalGlJournalHome;
import com.ejb.gl.LocalGlRecurringJournalHome;
import com.ejb.gl.LocalGlSetOfBook;
import com.ejb.gl.LocalGlSetOfBookHome;
import com.ejb.gl.LocalGlSuspenseAccountHome;
import com.ejb.hr.LocalHrDeployedBranch;
import com.ejb.hr.LocalHrDeployedBranchHome;
import com.ejb.hr.LocalHrEmployee;
import com.ejb.hr.LocalHrEmployeeHome;
import com.ejb.inv.LocalInvAdjustmentHome;
import com.ejb.inv.LocalInvDistributionRecordHome;
import com.ejb.inv.LocalInvItemLocationHome;
import com.ejb.inv.LocalInvLineItemTemplate;
import com.ejb.inv.LocalInvLineItemTemplateHome;
import com.ejb.inv.LocalInvOverheadMemoLineHome;
import com.ejb.pm.LocalPmUser;
import com.ejb.pm.LocalPmUserHome;
import com.util.AbstractSessionBean;
import com.util.AdBranchDetails;
import com.util.AdModBranchCustomerDetails;
import com.util.ApSupplierDetails;
import com.util.ArCustomerDetails;
import com.util.ArModCustomerClassDetails;
import com.util.ArModCustomerDetails;
import com.util.ArModCustomerTypeDetails;
import com.util.Debug;
import com.util.EJBCommon;
import com.util.EJBHomeFactory;
import com.util.GlChartOfAccountDetails;
import com.util.GlModChartOfAccountDetails;

/**
 * @ejb:bean name="ArCustomerEntryControllerEJB"
 *           display-name="Used for entering customers"
 *           type="Stateless"
 *           view-type="remote"
 *           jndi-name="ejb/ArCustomerEntryControllerEJB"
 *
 * @ejb:interface remote-class="com.ejb.txn.ArCustomerEntryController"
 *                extends="javax.ejb.EJBObject"
 *
 * @ejb:home remote-class="com.ejb.txn.ArCustomerEntryControllerHome"
 *           extends="javax.ejb.EJBHome"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="aruser"
 *                        role-link="aruserlink"
 *
 * @ejb:permission role-name="aruser"
 * 
*/

public class ArCustomerEntryControllerBean extends AbstractSessionBean {
	
	
	
	/**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList getHrOpenDbAll(Integer AD_BRNCH, Integer AD_CMPNY) {
        
        Debug.print("ArCustomerEntryControllerBean getHrOpenDbAll");
        
        LocalHrDeployedBranchHome hrDeployedBranchHome = null;
        
        ArrayList list = new ArrayList();
        
        // Initialize EJB Home
        
        try {
            
        	hrDeployedBranchHome = (LocalHrDeployedBranchHome)EJBHomeFactory.
            lookUpLocalHome(LocalHrDeployedBranchHome.JNDI_NAME, LocalHrDeployedBranchHome.class);
            
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }        
        
        try {
        	
            Collection hrDeployedBranches = hrDeployedBranchHome.findDbAll( AD_CMPNY);
            
            Iterator i = hrDeployedBranches.iterator();
            
            while (i.hasNext()) {
                
                LocalHrDeployedBranch hrDeployedBranch = (LocalHrDeployedBranch)i.next();
                
                list.add(hrDeployedBranch.getDbClientCode());
                
            }
            
            return list;
            
        } catch (Exception ex) {
            
            Debug.printStackTrace(ex);
            throw new EJBException(ex.getMessage());
            
        }
        
    }
    
    
    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList getHrOpenEmpAll(Integer AD_BRNCH, Integer AD_CMPNY) {
        
        Debug.print("ArCustomerEntryControllerBean getHrOpenEmpAll");
        
        LocalHrEmployeeHome hrEmployeeHome = null;
        
        ArrayList list = new ArrayList();
        
        // Initialize EJB Home
        
        try {
            
        	hrEmployeeHome = (LocalHrEmployeeHome)EJBHomeFactory.
            lookUpLocalHome(LocalHrEmployeeHome.JNDI_NAME, LocalHrEmployeeHome.class);
            
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }        
        
        try {
        	
            Collection hrEmployeees = hrEmployeeHome.findEmpAll( AD_CMPNY);
            
            Iterator i = hrEmployeees.iterator();
            
            while (i.hasNext()) {
                
                LocalHrEmployee hrEmployee = (LocalHrEmployee)i.next();
                
                list.add(hrEmployee.getEmpEmployeeNumber());
                
            }
            
            return list;
            
        } catch (Exception ex) {
            
            Debug.printStackTrace(ex);
            throw new EJBException(ex.getMessage());
            
        }
        
    }
	
	
	
	/**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public byte getAdPrfApUseSupplierPulldown(Integer AD_CMPNY) {
    	
    	Debug.print("ArCustomerEntryControllerBean getAdPrfApUseSupplierPulldown");
    	
    	LocalAdPreferenceHome adPreferenceHome = null;         
    	
    	// Initialize EJB Home
    	
    	try {
    		
    		adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);       
    		
    	} catch (NamingException ex) {
    		
    		throw new EJBException(ex.getMessage());
    		
    	}
    	
    	
    	try {
    		
    		LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);
    		
    		return adPreference.getPrfApUseSupplierPulldown();
    		
    	} catch (Exception ex) {
    		
    		Debug.printStackTrace(ex);
    		throw new EJBException(ex.getMessage());
    		
    	}
    	
    }
    
    
    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList getApSplAll(Integer AD_BRNCH, Integer AD_CMPNY) {
                    
        Debug.print("ArCustomerEntryControllerBean getApSplAll");
        
        LocalApSupplierHome apSupplierHome = null;               
        
        ArrayList list = new ArrayList();
        
        // Initialize EJB Home
        
        try {
            
        	apSupplierHome = (LocalApSupplierHome)EJBHomeFactory.
                lookUpLocalHome(LocalApSupplierHome.JNDI_NAME, LocalApSupplierHome.class);
            
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }        
        
        try {
        	
        	
        	
        	Collection apSuppliers = apSupplierHome.findEnabledSplAll(AD_BRNCH, AD_CMPNY);
        	
        	Iterator i = apSuppliers.iterator();
        	
        	while (i.hasNext()) {
        		
        		LocalApSupplier apSupplier = (LocalApSupplier)i.next();
        		
        		list.add(apSupplier.getSplSupplierCode());
        		
        	}
        	
        	return list;
        	
        } catch (Exception ex) {
        	
        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());
        	
        }
            
    }
    
	

    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList getArCtAll(Integer AD_CMPNY) {
                    
        Debug.print("ArCustomerEntryControllerBean getArCtAll");
        
        LocalArCustomerTypeHome arCustomerTypeHome = null;
        LocalArCustomerType arCustomerType = null;

        ArrayList list = new ArrayList();
        
        // Initialize EJB Home
        
        try {
            
            arCustomerTypeHome = (LocalArCustomerTypeHome)EJBHomeFactory.
                lookUpLocalHome(LocalArCustomerTypeHome.JNDI_NAME, LocalArCustomerTypeHome.class);
            
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {
            
            Collection arCustomerTypes = arCustomerTypeHome.findEnabledCtAll(AD_CMPNY);

	        Iterator i = arCustomerTypes.iterator();
	               
	        while (i.hasNext()) {
	        	
	        	arCustomerType = (LocalArCustomerType)i.next();
	        	
	        	list.add(arCustomerType.getCtName());
	        	
	        }
	        
	        return list;
            
        } catch (Exception ex) {
        	
        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());
        	
        }
            
    }
    
    
    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
	public ArrayList getAdLvCustomerBatchAll(Integer AD_CMPNY) {
		
		Debug.print("ArCustomerEntryControllerBean getAdLvCustomerBatchAll");
		
		LocalAdLookUpValueHome adLookUpValueHome = null;               
		
		ArrayList list = new ArrayList();
		
		// Initialize EJB Home
		
		try {
			
			adLookUpValueHome = (LocalAdLookUpValueHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdLookUpValueHome.JNDI_NAME, LocalAdLookUpValueHome.class);
			
		} catch (NamingException ex) {
			
			throw new EJBException(ex.getMessage());
			
		}        
		
		try {
			
			Collection adLookUpValues = adLookUpValueHome.findByLuName("AR CUSTOMER BATCH - SOA", AD_CMPNY);
			
			Iterator i = adLookUpValues.iterator();
			
			while (i.hasNext()) {
				
				LocalAdLookUpValue adLookUpValue = (LocalAdLookUpValue)i.next();
				
				list.add(adLookUpValue.getLvName());
				
			}
			
			return list;
			
		} catch (Exception ex) {
			
			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());
			
		}
		
	}
	
	
	/**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
	public ArrayList getAdLvCustomerDepartmentAll(Integer AD_CMPNY) {
		
		Debug.print("ArCustomerEntryControllerBean getAdLvCustomerDepartmentAll");
		
		LocalAdLookUpValueHome adLookUpValueHome = null;               
		
		ArrayList list = new ArrayList();
		
		// Initialize EJB Home
		
		try {
			
			adLookUpValueHome = (LocalAdLookUpValueHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdLookUpValueHome.JNDI_NAME, LocalAdLookUpValueHome.class);
			
		} catch (NamingException ex) {
			
			throw new EJBException(ex.getMessage());
			
		}        
		
		try {
			
			Collection adLookUpValues = adLookUpValueHome.findByLuName("AR CUSTOMER DEPARTMENT - SOA", AD_CMPNY);
			
			Iterator i = adLookUpValues.iterator();
			
			while (i.hasNext()) {
				
				LocalAdLookUpValue adLookUpValue = (LocalAdLookUpValue)i.next();
				
				list.add(adLookUpValue.getLvName());
				
			}
			
			return list;
			
		} catch (Exception ex) {
			
			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());
			
		}
		
	}
    
          
    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList getAdPytAll(Integer AD_CMPNY) {
                    
        Debug.print("ArCustomerEntryControllerBean getAdPytAll");
        
        LocalAdPaymentTermHome adPaymentTermHome = null;
        LocalAdPaymentTerm adPaymentTerm = null;
        
        ArrayList list = new ArrayList();
        
        // Initialize EJB Home
        
        try {
            
            adPaymentTermHome = (LocalAdPaymentTermHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdPaymentTermHome.JNDI_NAME, LocalAdPaymentTermHome.class);
            
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {
            
            Collection adPaymentTerms = adPaymentTermHome.findEnabledPytAll(AD_CMPNY);

	        Iterator i = adPaymentTerms.iterator();
	               
	        while (i.hasNext()) {
	        	
	        	adPaymentTerm = (LocalAdPaymentTerm)i.next();
	        	
	        	list.add(adPaymentTerm.getPytName());
	        	
	        }
	        
	        return list;
            
        } catch (Exception ex) {
        	
        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());
        	
        }
            
    }
    
    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList getArCcAll(Integer AD_CMPNY) {
                    
        Debug.print("ArCustomerEntryControllerBean getArCcAll");
        
        LocalArCustomerClassHome arCustomerClassHome = null;
        LocalArCustomerClass arCustomerClass = null;

        ArrayList list = new ArrayList();
        
        // Initialize EJB Home
        
        try {
            
            arCustomerClassHome = (LocalArCustomerClassHome)EJBHomeFactory.
                lookUpLocalHome(LocalArCustomerClassHome.JNDI_NAME, LocalArCustomerClassHome.class);
            
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {
            
            Collection arCustomerClasses = arCustomerClassHome.findEnabledCcAll(AD_CMPNY);
	        
	        Iterator i = arCustomerClasses.iterator();
	               
	        while (i.hasNext()) {
	        	
	        	arCustomerClass = (LocalArCustomerClass)i.next();
	        	
	        	list.add(arCustomerClass.getCcName());
	        	
	        }
	        
	        return list;
          
        } catch (Exception ex) {
        	
        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());
        	
        }
            
    }
    
    
    /**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public ArrayList getAdApprovalNotifiedUsersByCstCode(Integer CST_CODE, Integer AD_CMPNY) {
		
		Debug.print("ArCustomerEntryControllerBean getAdApprovalNotifiedUsersByCstCode");
		
		LocalAdApprovalQueueHome adApprovalQueueHome = null;
		LocalAdApprovalHome adApprovalHome = null;
		LocalArCustomerHome arCustomerHome = null;
		
		ArrayList list = new ArrayList();
		
		
		// Initialize EJB Home
		
		try {
			
			adApprovalQueueHome = (LocalAdApprovalQueueHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdApprovalQueueHome.JNDI_NAME, LocalAdApprovalQueueHome.class); 
					
			adApprovalHome = (LocalAdApprovalHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdApprovalHome.JNDI_NAME, LocalAdApprovalHome.class); 
			
			arCustomerHome = (LocalArCustomerHome)EJBHomeFactory.
					lookUpLocalHome(LocalArCustomerHome.JNDI_NAME, LocalArCustomerHome.class);      
					
		} catch (NamingException ex) {
			
			throw new EJBException(ex.getMessage());
			
		}
		
		try {

			LocalAdApproval adApproval = adApprovalHome.findByAprAdCompany(AD_CMPNY);
			
			LocalArCustomer arCustomer = arCustomerHome.findByPrimaryKey(CST_CODE);
			
			if (arCustomer.getCstPosted() == EJBCommon.TRUE && adApproval.getAprEnableArCustomer() == EJBCommon.TRUE) {
				
				arCustomer.setCstPosted(EJBCommon.FALSE);
				arCustomer.setCstEnable(EJBCommon.FALSE);
				/*list.add("DOCUMENT POSTED");
				return list;*/
				
			}
			
			Collection adApprovalQueues = adApprovalQueueHome.findByAqDocumentAndAqDocumentCode("AR CUSTOMER", CST_CODE, AD_CMPNY);
			
			Iterator i = adApprovalQueues.iterator();
			
			while(i.hasNext()) {
				
				LocalAdApprovalQueue adApprovalQueue = (LocalAdApprovalQueue)i.next();
				
				list.add("User: "+adApprovalQueue.getAdUser().getUsrName()+" - "+adApprovalQueue.getAdUser().getUsrDescription());
				
			}
			
			return list;
			
		} catch (Exception ex) {
			
			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());
			
		}
		
	}
    
    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public byte getPrfArAutoGenerateCustomerCode(Integer AD_CMPNY) {
        
        Debug.print("ApCustomerEntryControllerBean getPrfApAutoGenerateCustomerCode");
        
        LocalAdPreferenceHome adPreferenceHome = null;
                
         // Initialize EJB Home
          
         try {
              
            adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);       
              
         } catch (NamingException ex) {
              
            throw new EJBException(ex.getMessage());
              
         }
        

         try {
         	
            LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);
            
            return adPreference.getPrfArAutoGenerateCustomerCode();
           
         } catch (Exception ex) {
          	 
            Debug.printStackTrace(ex);
            throw new EJBException(ex.getMessage());
           
         }

    }
    
    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public String getPrfArNextCustomerCode(Integer AD_CMPNY) {
        
        Debug.print("ApCustomerEntryControllerBean getPrfArNextCustomerCode");
        
        LocalAdPreferenceHome adPreferenceHome = null;
                
         // Initialize EJB Home
          
         try {
              
            adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);       
              
         } catch (NamingException ex) {
              
            throw new EJBException(ex.getMessage());
              
         }
        

         try {
         	
            LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);
            return adPreference.getPrfArNextCustomerCode();
           
         } catch (Exception ex) {
          	 
            Debug.printStackTrace(ex);
            throw new EJBException(ex.getMessage());
           
         }

    }
    
    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public byte getPrfValidateCustomerEmail(Integer AD_CMPNY) {
        
        Debug.print("ApCustomerEntryControllerBean getPrfArNextCustomerCode");
        
        LocalAdPreferenceHome adPreferenceHome = null;
                
         // Initialize EJB Home
          
         try {
              
            adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);       
              
         } catch (NamingException ex) {
              
            throw new EJBException(ex.getMessage());
              
         }
        

         try {
         	
            LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);
            return adPreference.getPrfArValidateCustomerEmail();
           
         } catch (Exception ex) {
          	 
            Debug.printStackTrace(ex);
            throw new EJBException(ex.getMessage());
           
         }

    }
    
    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
     public short getGlFcPrecisionUnit(Integer AD_CMPNY) {

        Debug.print("ArCustomerEntryControllerBean getGlFcPrecisionUnit");

       
        LocalAdCompanyHome adCompanyHome = null;
       
      
        // Initialize EJB Home
         
        try {
             
            adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
               lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);       
             
        } catch (NamingException ex) {
             
            throw new EJBException(ex.getMessage());
             
        }

        try {
        	
          LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
             
          return  adCompany.getGlFunctionalCurrency().getFcPrecision();
                                      
        } catch (Exception ex) {
        	 
        	 Debug.printStackTrace(ex);
          throw new EJBException(ex.getMessage());
          
        }

     }
    
    /**
     * @ejb:interface-method view-type="remote"
     **/
    public Integer saveArCstEntry(com.util.ArModCustomerDetails details, String CT_NM,
        String PYT_NM, 
        String SPL_SPPLR_CODE, 
        String CC_NM, 
        String CST_GL_COA_RCVBL_ACCNT, 
        String CST_GL_COA_RVNUE_ACCNT, 
        String CST_GL_COA_UNERND_INT_ACCNT, 
        String CST_GL_COA_ERND_INT_ACCNT,
        String CST_GL_COA_UNERND_PNT_ACCNT, 
        String CST_GL_COA_ERND_PNT_ACCNT,
        String BA_NM, Integer RS_CODE, 
        String SLP_SLSPRSN_CODE, 
        String SLP_SLSPRSN_CODE2, 
        String LIT_NM, 
        String HR_DB_NM,
        String HR_EMP_BIO_NMBR,
        
        String PM_USR_EMP_NMBR,
        
        boolean isDraft,
        Integer AD_BRNCH,
        Integer AD_CMPNY)
    
        throws GlobalRecordAlreadyExistException,
        GlobalNameAndAddressAlreadyExistsException,
        ArCCCoaGlReceivableAccountNotFoundException,
		ArCCCoaGlRevenueAccountNotFoundException,
		ArCCCoaGlUnEarnedInterestAccountNotFoundException,
		ArCCCoaGlEarnedInterestAccountNotFoundException,
		ArCCCoaGlUnEarnedPenaltyAccountNotFoundException,
		GlobalNoApprovalApproverFoundException,
		GlobalNoApprovalRequesterFoundException,
		ArCCCoaGlEarnedPenaltyAccountNotFoundException
    {
        	
        Debug.print("ArCustomerEntryControllerBean saveArCstEntry");
        
        LocalHrEmployeeHome hrEmployeeHome = null;
        LocalHrDeployedBranchHome hrDeployedBranchHome = null;

        LocalPmUserHome pmUserHome = null;
        
        
        LocalArCustomerHome arCustomerHome = null;
        LocalApSupplierHome apSupplierHome = null;
        LocalAdPaymentTermHome adPaymentTermHome = null;
        LocalArCustomerTypeHome arCustomerTypeHome = null;
        LocalArCustomerClassHome arCustomerClassHome = null;
        LocalGlChartOfAccountHome glChartOfAccountHome = null;
        LocalAdBankAccountHome adBankAccountHome = null;
        LocalAdPreferenceHome adPreferenceHome = null;
        LocalArCustomerBalanceHome arCustomerBalanceHome = null;
        LocalArInvoiceHome arInvoiceHome = null;
        LocalAdBranchHome adBranchHome = null;
        LocalAdBranchCustomerHome adBranchCustomerHome = null;
        LocalAdResponsibilityHome adResponsibiltyHome = null;
        LocalArSalespersonHome arSalespersonHome = null;
        LocalInvLineItemTemplateHome invLineItemTemplateHome = null;
        LocalAdApprovalHome adApprovalHome = null;
        LocalAdUserHome adUserHome = null;
        
        LocalArCustomer arCustomer = null;
        LocalAdPreference adPreference = null;
        LocalAdApprovalQueueHome adApprovalQueueHome = null;
        
                
        try {
        	
        	hrDeployedBranchHome = (LocalHrDeployedBranchHome)EJBHomeFactory.
        			lookUpLocalHome(LocalHrDeployedBranchHome.JNDI_NAME, LocalHrDeployedBranchHome.class);
        	
            hrEmployeeHome = (LocalHrEmployeeHome)EJBHomeFactory.
                    lookUpLocalHome(LocalHrEmployeeHome.JNDI_NAME, LocalHrEmployeeHome.class);
            
            pmUserHome = (LocalPmUserHome)EJBHomeFactory.
                    lookUpLocalHome(LocalPmUserHome.JNDI_NAME, LocalPmUserHome.class);
        	
        	adUserHome = (LocalAdUserHome)EJBHomeFactory.
                    lookUpLocalHome(LocalAdUserHome.JNDI_NAME, LocalAdUserHome.class);
        	adApprovalQueueHome = (LocalAdApprovalQueueHome)EJBHomeFactory.
                    lookUpLocalHome(LocalAdApprovalQueueHome.JNDI_NAME, LocalAdApprovalQueueHome.class);
            
        	
        	adApprovalHome = (LocalAdApprovalHome)EJBHomeFactory.
                    lookUpLocalHome(LocalAdApprovalHome.JNDI_NAME, LocalAdApprovalHome.class);
            
            arCustomerHome = (LocalArCustomerHome)EJBHomeFactory.
                lookUpLocalHome(LocalArCustomerHome.JNDI_NAME, LocalArCustomerHome.class);
            apSupplierHome = (LocalApSupplierHome)EJBHomeFactory.
                    lookUpLocalHome(LocalApSupplierHome.JNDI_NAME, LocalApSupplierHome.class);
            adPaymentTermHome = (LocalAdPaymentTermHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdPaymentTermHome.JNDI_NAME, LocalAdPaymentTermHome.class);             
            arCustomerTypeHome = (LocalArCustomerTypeHome)EJBHomeFactory.
                lookUpLocalHome(LocalArCustomerTypeHome.JNDI_NAME, LocalArCustomerTypeHome.class);
            arCustomerClassHome = (LocalArCustomerClassHome)EJBHomeFactory.
                lookUpLocalHome(LocalArCustomerClassHome.JNDI_NAME, LocalArCustomerClassHome.class);
            glChartOfAccountHome = (LocalGlChartOfAccountHome)EJBHomeFactory.
            	lookUpLocalHome(LocalGlChartOfAccountHome.JNDI_NAME, LocalGlChartOfAccountHome.class);
            adBankAccountHome = (LocalAdBankAccountHome)EJBHomeFactory.
        		lookUpLocalHome(LocalAdBankAccountHome.JNDI_NAME, LocalAdBankAccountHome.class);
            adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
            	lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);
            arCustomerBalanceHome = (LocalArCustomerBalanceHome)EJBHomeFactory.
				lookUpLocalHome(LocalArCustomerBalanceHome.JNDI_NAME, LocalArCustomerBalanceHome.class);
            arInvoiceHome = (LocalArInvoiceHome)EJBHomeFactory.
				lookUpLocalHome(LocalArInvoiceHome.JNDI_NAME, LocalArInvoiceHome.class);
            adBranchCustomerHome = (LocalAdBranchCustomerHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdBranchCustomerHome.JNDI_NAME, LocalAdBranchCustomerHome.class);
            adBranchHome = (LocalAdBranchHome)EJBHomeFactory.
    			lookUpLocalHome(LocalAdBranchHome.JNDI_NAME, LocalAdBranchHome.class);            
            adResponsibiltyHome = (LocalAdResponsibilityHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdResponsibilityHome.JNDI_NAME, LocalAdResponsibilityHome.class);            
            arSalespersonHome = (LocalArSalespersonHome)EJBHomeFactory.
				lookUpLocalHome(LocalArSalespersonHome.JNDI_NAME, LocalArSalespersonHome.class);            
            invLineItemTemplateHome = (LocalInvLineItemTemplateHome)EJBHomeFactory.
				lookUpLocalHome(LocalInvLineItemTemplateHome.JNDI_NAME, LocalInvLineItemTemplateHome.class);
        
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {
            
            adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);
            
        } catch (Exception ex) {
            
            Debug.printStackTrace(ex);
            throw new EJBException(ex.getMessage());
        }
                        	
        try {
             
             LocalGlChartOfAccount glReceivableChartOfAccount = null;
        	 LocalGlChartOfAccount glRevenueChartOfAccount = null;
        	 LocalGlChartOfAccount glUnEarnedInterestChartOfAccount = null;
        	 LocalGlChartOfAccount glEarnedInterestChartOfAccount = null;
        	 LocalGlChartOfAccount glUnEarnedPenaltyChartOfAccount = null;
        	 LocalGlChartOfAccount glEarnedPenaltyChartOfAccount = null;
         	 
        	 LocalArCustomerClass arCustomerClass = arCustomerClassHome.findByCcName(CC_NM, AD_CMPNY);
        	 // autoGenerate
        	 if(adPreference.getPrfArAutoGenerateCustomerCode() == EJBCommon.TRUE && (
        	         details.getCstCustomerCode() == null || details.getCstCustomerCode().length() == 0)) {
        	     
        	     while(true) {
        	         
        	         try {
        	             
        	             arCustomerHome.findByCstCustomerCode(arCustomerClass.getCcNextCustomerCode(), AD_CMPNY);
        	             
        	             arCustomerClass.setCcNextCustomerCode(EJBCommon.incrementStringNumber(
        	            		 arCustomerClass.getCcNextCustomerCode()));
        	             /*adPreference.setPrfArNextCustomerCode(EJBCommon.incrementStringNumber(
        	                     adPreference.getPrfArNextCustomerCode()));
        	             */
        	         } catch(FinderException ex) {
        	             
        	             details.setCstCustomerCode(arCustomerClass.getCcNextCustomerCode());
        	             arCustomerClass.setCcNextCustomerCode(EJBCommon.incrementStringNumber(
        	            		 arCustomerClass.getCcNextCustomerCode()));
        	             
        	             /*adPreference.setPrfArNextCustomerCode(EJBCommon.incrementStringNumber(
        	                     adPreference.getPrfArNextCustomerCode()));
        	             */
        	             break;
        	         }
        	     }
        	 }
        	 
         	 // validate if customer already exists
         	
         	 try {
      	
         	 	arCustomer = arCustomerHome.findByCstCustomerCode(details.getCstCustomerCode(), AD_CMPNY);
	            
	            if(details.getCstCode() == null || details.getCstCode() != null && 
	                  !arCustomer.getCstCode().equals(details.getCstCode())) {          
	                     
	                throw new GlobalRecordAlreadyExistException();
	                
	            }
	            
         	 } catch (GlobalRecordAlreadyExistException ex) {
	        	
	        	throw ex;
	       
         	 } catch (FinderException ex) {
        	 	
         	 } 
	            
	         
         	 //validate if customer with this name and address already exists
	
         	 try {
         	 	
	            arCustomer = arCustomerHome.findByCstNameAndAddress(details.getCstName(), details.getCstAddress(), AD_CMPNY);
	            
	            if(arCustomer != null && !arCustomer.getCstCode().equals(details.getCstCode())) {
	            	
	            	throw new GlobalNameAndAddressAlreadyExistsException();
	            	
	            }
	            
         	 } catch (GlobalNameAndAddressAlreadyExistsException ex) {
	         	
	        	throw ex;
	         	
	         } catch (FinderException ex) {
	        	 	
	         }
	         
	         try {
				
				glReceivableChartOfAccount = glChartOfAccountHome.findByCoaAccountNumber( CST_GL_COA_RCVBL_ACCNT, AD_CMPNY);
				
			 } catch (FinderException ex) {
				
				throw new ArCCCoaGlReceivableAccountNotFoundException();
				
			 }
			 
			 if (CST_GL_COA_RVNUE_ACCNT != null && CST_GL_COA_RVNUE_ACCNT.length() > 0) {
			
				 try {
					
					glRevenueChartOfAccount = glChartOfAccountHome.findByCoaAccountNumber( CST_GL_COA_RVNUE_ACCNT, AD_CMPNY);
					
				 } catch (FinderException ex) {
					
					throw new ArCCCoaGlRevenueAccountNotFoundException();
					
				 }
				 
			 }
			 
			 if (CST_GL_COA_UNERND_INT_ACCNT != null && CST_GL_COA_UNERND_INT_ACCNT.length() > 0) {
					
				 try {
					
					 glUnEarnedInterestChartOfAccount = glChartOfAccountHome.findByCoaAccountNumber( CST_GL_COA_UNERND_INT_ACCNT, AD_CMPNY);
					
				 } catch (FinderException ex) {
					
					throw new ArCCCoaGlUnEarnedInterestAccountNotFoundException();
					
				 }
				 
			 }
			 
			 if (CST_GL_COA_ERND_INT_ACCNT != null && CST_GL_COA_ERND_INT_ACCNT.length() > 0) {
					
				 try {
					
					 glEarnedInterestChartOfAccount = glChartOfAccountHome.findByCoaAccountNumber( CST_GL_COA_ERND_INT_ACCNT, AD_CMPNY);
					
				 } catch (FinderException ex) {
					
					throw new ArCCCoaGlEarnedInterestAccountNotFoundException();
					
				 }
				 
			 }
			 
			 
			 if (CST_GL_COA_UNERND_PNT_ACCNT != null && CST_GL_COA_UNERND_PNT_ACCNT.length() > 0) {
					
				 try {
					
					 glUnEarnedPenaltyChartOfAccount = glChartOfAccountHome.findByCoaAccountNumber( CST_GL_COA_UNERND_PNT_ACCNT, AD_CMPNY);
					
				 } catch (FinderException ex) {
					
					throw new ArCCCoaGlUnEarnedPenaltyAccountNotFoundException();
					
				 }
				 
			 }
			 
			 if (CST_GL_COA_ERND_PNT_ACCNT != null && CST_GL_COA_ERND_PNT_ACCNT.length() > 0) {
					
				 try {
					
					 glEarnedPenaltyChartOfAccount = glChartOfAccountHome.findByCoaAccountNumber( CST_GL_COA_ERND_PNT_ACCNT, AD_CMPNY);
					
				 } catch (FinderException ex) {
					
					throw new ArCCCoaGlEarnedPenaltyAccountNotFoundException();
					
				 }
				 
			 }
			 	
		     // create new customer
		     
	         if (details.getCstCode() == null) { 
	        	 System.out.println("------------------------------------------------>NEW CUSTOMER MODE");
		    	arCustomer = arCustomerHome.create(details.getCstCustomerCode(), 
			    	details.getCstName(), details.getCstDescription(), details.getCstPaymentMethod(), details.getCstCreditLimit(),
			    	details.getCstAddress(), details.getCstCity(), details.getCstStateProvince(), details.getCstPostalCode(),
		            details.getCstCountry(), details.getCstContact(), details.getCstEmployeeID(), details.getCstAccountNumber(), details.getCstPhone(), details.getCstMobilePhone(),
		            details.getCstFax(), details.getCstAlternatePhone(), details.getCstAlternateMobilePhone(), details.getCstAlternateContact(),
		            details.getCstEmail(), details.getCstBillToAddress(), details.getCstBillToContact(), 
		            details.getCstBillToAltContact(), details.getCstBillToPhone(), details.getCstBillingHeader(), 
		            details.getCstBillingFooter(), details.getCstBillingHeader2(), details.getCstBillingFooter2(), 
		            details.getCstBillingHeader3(), details.getCstBillingFooter3(), details.getCstBillingSignatory(),
					details.getCstSignatoryTitle(), details.getCstShipToAddress(), details.getCstShipToContact(), 
					details.getCstShipToAltContact(), details.getCstShipToPhone(), details.getCstTin(), details.getCstNumbersParking(), details.getCstMonthlyInterestRate(),
					details.getCstParkingID(), details.getCstAssociationDuesRate(), details.getCstRealPropertyTaxRate(), details.getCstWordPressCustomerID(),
					glReceivableChartOfAccount.getCoaCode(), 
					glRevenueChartOfAccount != null ? glRevenueChartOfAccount.getCoaCode() : null,
					glUnEarnedInterestChartOfAccount != null ? glUnEarnedInterestChartOfAccount.getCoaCode() : null,
					glEarnedInterestChartOfAccount != null ? glEarnedInterestChartOfAccount.getCoaCode() : null,
					glUnEarnedPenaltyChartOfAccount != null ? glUnEarnedPenaltyChartOfAccount.getCoaCode() : null,
					glEarnedPenaltyChartOfAccount != null ? glEarnedPenaltyChartOfAccount.getCoaCode() : null,
					details.getCstEnable(), details.getCstEnablePayroll(), details.getCstEnableRetailCashier(), details.getCstEnableRebate(), details.getCstAutoComputeInterest(), details.getCstAutoComputePenalty(), 
		            details.getCstBirthday(), details.getCstDealPrice(), details.getCstArea(), details.getCstSquareMeter(), details.getCstEntryDate(), details.getCstEffectivityDays(), 
		            details.getCstAdLvRegion(), details.getCstMemo(), details.getCstCustomerBatch(), details.getCstCustomerDepartment(),
		            null, null, EJBCommon.FALSE, details.getCstCreatedBy(), details.getCstDateCreated(),
        			details.getCstLastModifiedBy(), details.getCstDateLastModified(),
        			null, null, null, null,
        			AD_BRNCH,
		            AD_CMPNY);
		    	

		    	arCustomer.setCstHrEnableCashBond(EJBCommon.TRUE);
		    	arCustomer.setCstHrCashBondAmount(100);
		    	arCustomer.setCstHrEnableInsMisc(EJBCommon.TRUE);
		    	arCustomer.setCstHrInsMiscAmount(100);
		        
		        if (CT_NM != null && CT_NM.length() > 0) {
		        
			        LocalArCustomerType arCustomerType = arCustomerTypeHome.findByCtName(CT_NM, AD_CMPNY);
				    arCustomer.setArCustomerType(arCustomerType);
			        
			    }
		        
		        // Link to AP Investor
		        if (SPL_SPPLR_CODE != null && SPL_SPPLR_CODE.length() > 0) {
			    	
		        	
		        	LocalApSupplier apSupplier = apSupplierHome .findBySplSupplierCode(SPL_SPPLR_CODE, AD_CMPNY);
		        	arCustomer.setApSupplier(apSupplier);
			
			    }
		        
		        

		        // create new BranchCustomer
		        
		        ArrayList bCstlist = details.getBcstList();
		        
		        Iterator i = bCstlist.iterator();
	        	
	        	while(i.hasNext()) {
	        		
	        		AdModBranchCustomerDetails bCstDetails = (AdModBranchCustomerDetails)i.next();
	        		
					try {

						glReceivableChartOfAccount = glChartOfAccountHome.findByCoaAccountNumber(
								bCstDetails.getBcstReceivableAccountNumber(), AD_CMPNY);
						
					} catch (FinderException ex) {
						
						throw new ArCCCoaGlReceivableAccountNotFoundException();
						
					}

					Integer glRevChrtOfAccntCode = null;
					
					if ( bCstDetails.getBcstRevenueAccountNumber()!= null &&
							bCstDetails.getBcstRevenueAccountNumber().length() > 0){
						
						try {
							
							glRevenueChartOfAccount = glChartOfAccountHome.findByCoaAccountNumber(
									bCstDetails.getBcstRevenueAccountNumber(), AD_CMPNY);
							
							glRevChrtOfAccntCode = glRevenueChartOfAccount.getCoaCode();
							
						} catch (FinderException ex) {
							
							throw new ArCCCoaGlRevenueAccountNotFoundException();
							
						}
						
					}
					
					
					Integer glUnEarnedInterestChrtOfAccntCode = null;
					
					if ( bCstDetails.getBcstUnEarnedInterestAccountNumber()!= null &&
							bCstDetails.getBcstUnEarnedInterestAccountNumber().length() > 0){
						
						try {
							
							glUnEarnedInterestChartOfAccount = glChartOfAccountHome.findByCoaAccountNumber(
									bCstDetails.getBcstUnEarnedInterestAccountNumber(), AD_CMPNY);
							
							glUnEarnedInterestChrtOfAccntCode = glUnEarnedInterestChartOfAccount.getCoaCode();
							
						} catch (FinderException ex) {
							
							throw new ArCCCoaGlUnEarnedInterestAccountNotFoundException();
							
						}
						
					}
					
					Integer glEarnedInterestChrtOfAccntCode = null;
					
					if ( bCstDetails.getBcstEarnedInterestAccountNumber()!= null &&
							bCstDetails.getBcstEarnedInterestAccountNumber().length() > 0){
						
						try {
							
							glEarnedInterestChartOfAccount = glChartOfAccountHome.findByCoaAccountNumber(
									bCstDetails.getBcstEarnedInterestAccountNumber(), AD_CMPNY);
							
							glEarnedInterestChrtOfAccntCode = glEarnedInterestChartOfAccount.getCoaCode();
							
						} catch (FinderException ex) {
							
							throw new ArCCCoaGlEarnedInterestAccountNotFoundException();
							
						}
						
					}
					
					
Integer glUnEarnedPenaltyChrtOfAccntCode = null;
					
					if ( bCstDetails.getBcstUnEarnedPenaltyAccountNumber()!= null &&
							bCstDetails.getBcstUnEarnedPenaltyAccountNumber().length() > 0){
						
						try {
							
							glUnEarnedPenaltyChartOfAccount = glChartOfAccountHome.findByCoaAccountNumber(
									bCstDetails.getBcstUnEarnedPenaltyAccountNumber(), AD_CMPNY);
							
							glUnEarnedPenaltyChrtOfAccntCode = glUnEarnedPenaltyChartOfAccount.getCoaCode();
							
						} catch (FinderException ex) {
							
							throw new ArCCCoaGlUnEarnedPenaltyAccountNotFoundException();
							
						}
						
					}
					
					Integer glEarnedPenaltyChrtOfAccntCode = null;
					
					if ( bCstDetails.getBcstEarnedPenaltyAccountNumber()!= null &&
							bCstDetails.getBcstEarnedPenaltyAccountNumber().length() > 0){
						
						try {
							
							glEarnedPenaltyChartOfAccount = glChartOfAccountHome.findByCoaAccountNumber(
									bCstDetails.getBcstEarnedPenaltyAccountNumber(), AD_CMPNY);
							
							glEarnedPenaltyChrtOfAccntCode = glEarnedPenaltyChartOfAccount.getCoaCode();
							
						} catch (FinderException ex) {
							
							throw new ArCCCoaGlEarnedPenaltyAccountNotFoundException();
							
						}
						
					}
					
	        		LocalAdBranchCustomer adBranchCustomer = adBranchCustomerHome.create(
	        			glReceivableChartOfAccount.getCoaCode(), glRevChrtOfAccntCode,
	        			glUnEarnedInterestChrtOfAccntCode,glEarnedInterestChrtOfAccntCode,
	        			glUnEarnedPenaltyChrtOfAccntCode,glEarnedPenaltyChrtOfAccntCode,
	        			'N', AD_CMPNY);
	        		//arCustomer.addAdBranchCustomer(adBranchCustomer);
	        		adBranchCustomer.setArCustomer(arCustomer);
	        		
					LocalAdBranch adBranch = adBranchHome.findByBrName(bCstDetails.getBcstBranchName(), AD_CMPNY);
					//adBranch.addAdBranchCustomer(adBranchCustomer);
					adBranchCustomer.setAdBranch(adBranch);

	        	}		        
		        
	        	
			    LocalAdPaymentTerm adPaymentTerm = adPaymentTermHome.findByPytName(PYT_NM, AD_CMPNY);
			    arCustomer.setAdPaymentTerm(adPaymentTerm);
                
		        //LocalArCustomerClass arCustomerClass = arCustomerClassHome.findByCcName(CC_NM, AD_CMPNY);		        
		        arCustomer.setArCustomerClass(arCustomerClass);
		        
		        LocalAdBankAccount adBankAccount = adBankAccountHome.findByBaName(BA_NM, AD_CMPNY);
		        arCustomer.setAdBankAccount(adBankAccount);
		        
		        
		        
	        	if (HR_DB_NM != null && HR_DB_NM.length() > 0) {
		        	
		        	LocalHrDeployedBranch hrDeployedBranch = hrDeployedBranchHome.findByDbClientName(HR_DB_NM, AD_CMPNY);
		        	arCustomer.setHrDeployedBranch(hrDeployedBranch);
		        	
		        }
	        	
	        	
	        	if (HR_EMP_BIO_NMBR != null && HR_EMP_BIO_NMBR.length() > 0) {
		        	System.out.println("HR_EMP_BIO_NMBR="+HR_EMP_BIO_NMBR);
	        		LocalHrEmployee hrEmployee = hrEmployeeHome.findByEmpBioNumber(HR_EMP_BIO_NMBR, AD_CMPNY);
	        		
            		arCustomer.setHrEmployee(hrEmployee);
		        	
		        }
	        	
	        	
	        	if (PM_USR_EMP_NMBR != null && PM_USR_EMP_NMBR.length() > 0) {
		        	System.out.println("PM_USR_EMP_NMBR="+PM_USR_EMP_NMBR);
		        		LocalPmUser pmUser = pmUserHome.findUsrByEmployeeNumber(PM_USR_EMP_NMBR, AD_CMPNY);
		        		
	            		arCustomer.setPmUser(pmUser);

		        }
	        	
	        	

	            
		        
	        	LocalArSalesperson arSalesperson = null;
	        	
	        	if (SLP_SLSPRSN_CODE != null && SLP_SLSPRSN_CODE.length() > 0 && !SLP_SLSPRSN_CODE.equalsIgnoreCase("NO RECORD FOUND")) {
                                
	        		// if he tagged a salesperson for this customer
	        		arSalesperson = arSalespersonHome.findBySlpSalespersonCode(SLP_SLSPRSN_CODE, AD_CMPNY);
			        arCustomer.setArSalesperson(arSalesperson);
	        		

	        	} else {
                           
	        		// if he untagged a salesperson for this customer
	        		if (arCustomer.getArSalesperson() != null) {
	        			
	        			arSalesperson = arSalespersonHome.findBySlpSalespersonCode(arCustomer.getArSalesperson().getSlpSalespersonCode(), AD_CMPNY);
	        			arSalesperson.dropArCustomer(arCustomer);
	        			
	        		}
	        		
	        	}
	        	
	        	LocalArSalesperson arSalesperson2 = null;
	        	if (SLP_SLSPRSN_CODE2 != null && SLP_SLSPRSN_CODE2.length() > 0 && !SLP_SLSPRSN_CODE2.equalsIgnoreCase("NO RECORD FOUND")) {

	        		// if he tagged a salesperson for this customer
	        		arSalesperson2 = arSalespersonHome.findBySlpSalespersonCode(SLP_SLSPRSN_CODE2, AD_CMPNY);
			        arCustomer.setCstArSalesperson2(arSalesperson2.getSlpCode());
	        		

	        	} else {
	        	
	        		// if he untagged a salesperson for this customer
	        		arCustomer.setCstArSalesperson2(null);	        		
	        	}
	        	
	        	if(LIT_NM != null && LIT_NM.length() > 0) {
		        	
		        	LocalInvLineItemTemplate invLineItemTemplate = invLineItemTemplateHome.findByLitName(LIT_NM, AD_CMPNY);
			        arCustomer.setInvLineItemTemplate(invLineItemTemplate);
		        	
		        }
		        
		    } else {
		    	
		    	
		    	System.out.println("------------------------------------------------>UPDATE CUSTOMER MODE");
		    	arCustomer = arCustomerHome.findByPrimaryKey(details.getCstCode());
		
					arCustomer.setCstCustomerCode(details.getCstCustomerCode());
					arCustomer.setCstName(details.getCstName());
					arCustomer.setCstDescription(details.getCstDescription());
					arCustomer.setCstPaymentMethod(details.getCstPaymentMethod());
					arCustomer.setCstCreditLimit(details.getCstCreditLimit());
					arCustomer.setCstAddress(details.getCstAddress());
					arCustomer.setCstCity(details.getCstCity());
					arCustomer.setCstStateProvince(details.getCstStateProvince());
					arCustomer.setCstPostalCode(details.getCstPostalCode()); 
					arCustomer.setCstCountry(details.getCstCountry());
					arCustomer.setCstEmployeeID(details.getCstEmployeeID());
					arCustomer.setCstAccountNumber(details.getCstAccountNumber());
					arCustomer.setCstContact(details.getCstContact());
					arCustomer.setCstPhone(details.getCstPhone());
					arCustomer.setCstMobilePhone(details.getCstMobilePhone());
					arCustomer.setCstFax(details.getCstFax());
					arCustomer.setCstAlternatePhone(details.getCstAlternatePhone()); 
					arCustomer.setCstAlternateMobilePhone(details.getCstAlternateMobilePhone());
					arCustomer.setCstAlternateContact(details.getCstAlternateContact());
					arCustomer.setCstEmail(details.getCstEmail());
					arCustomer.setCstBillToAddress(details.getCstBillToAddress());
					arCustomer.setCstBillToContact(details.getCstBillToContact());
					arCustomer.setCstBillToAltContact(details.getCstBillToAltContact());
					arCustomer.setCstBillToPhone(details.getCstBillToPhone());
					arCustomer.setCstBillingHeader(details.getCstBillingHeader());
					arCustomer.setCstBillingFooter(details.getCstBillingFooter());
					arCustomer.setCstBillingHeader2(details.getCstBillingHeader2());
					arCustomer.setCstBillingFooter2(details.getCstBillingFooter2());
					arCustomer.setCstBillingHeader3(details.getCstBillingHeader3());
					arCustomer.setCstBillingFooter3(details.getCstBillingFooter3());
					arCustomer.setCstBillingSignatory(details.getCstBillingSignatory());
					arCustomer.setCstSignatoryTitle(details.getCstSignatoryTitle());
					arCustomer.setCstShipToAddress(details.getCstShipToAddress());
					arCustomer.setCstShipToContact(details.getCstShipToContact());
					arCustomer.setCstShipToAltContact(details.getCstShipToAltContact());
					arCustomer.setCstShipToPhone(details.getCstShipToPhone());
					arCustomer.setCstTin(details.getCstTin());
					arCustomer.setCstNumbersParking(details.getCstNumbersParking());
					arCustomer.setCstMonthlyInterestRate(details.getCstMonthlyInterestRate());
					arCustomer.setCstParkingID(details.getCstParkingID());
					arCustomer.setCstRealPropertyTaxRate(details.getCstRealPropertyTaxRate());
					arCustomer.setCstAssociationDuesRate(details.getCstAssociationDuesRate());
					arCustomer.setCstWordPressCustomerID(details.getCstWordPressCustomerID());
					arCustomer.setCstGlCoaReceivableAccount(glReceivableChartOfAccount.getCoaCode());
					arCustomer.setCstGlCoaRevenueAccount(glRevenueChartOfAccount != null ? glRevenueChartOfAccount.getCoaCode() : null);
					
					arCustomer.setCstGlCoaUnEarnedInterestAccount(glUnEarnedInterestChartOfAccount != null ? glUnEarnedInterestChartOfAccount.getCoaCode() : null);
					arCustomer.setCstGlCoaEarnedInterestAccount(glEarnedInterestChartOfAccount != null ? glEarnedInterestChartOfAccount.getCoaCode() : null);
					arCustomer.setCstGlCoaUnEarnedPenaltyAccount(glUnEarnedPenaltyChartOfAccount != null ? glUnEarnedPenaltyChartOfAccount.getCoaCode() : null);
					arCustomer.setCstGlCoaEarnedPenaltyAccount(glEarnedPenaltyChartOfAccount != null ? glEarnedPenaltyChartOfAccount.getCoaCode() : null);
					
					
					arCustomer.setCstEnable(details.getCstEnable());
					arCustomer.setCstEnableRetailCashier(details.getCstEnableRetailCashier());
					arCustomer.setCstEnableRebate(details.getCstEnableRebate());
					arCustomer.setCstAutoComputeInterest(details.getCstAutoComputeInterest());
					arCustomer.setCstAutoComputePenalty(details.getCstAutoComputePenalty());
					arCustomer.setCstBirthday(details.getCstBirthday());
					arCustomer.setCstDealPrice(details.getCstDealPrice());
					arCustomer.setCstArea(details.getCstArea());
					arCustomer.setCstSquareMeter(details.getCstSquareMeter());
					arCustomer.setCstEntryDate(details.getCstEntryDate());
					arCustomer.setCstEffectivityDays(details.getCstEffectivityDays());
					arCustomer.setCstAdLvRegion(details.getCstAdLvRegion());
					arCustomer.setCstMemo(details.getCstMemo());
					arCustomer.setCstCustomerBatch(details.getCstCustomerBatch());
					arCustomer.setCstCustomerDepartment(details.getCstCustomerDepartment());
					
					arCustomer.setCstLastModifiedBy(details.getCstLastModifiedBy());
	                arCustomer.setCstDateLastModified(details.getCstDateLastModified());
	                arCustomer.setCstReasonForRejection(null);
	                arCustomer.setCstAdBranch(AD_BRNCH);
	               
	                
			    	arCustomer.setCstHrEnableCashBond(EJBCommon.TRUE);
			    	arCustomer.setCstHrCashBondAmount(100);
			    	arCustomer.setCstHrEnableInsMisc(EJBCommon.TRUE);
			    	arCustomer.setCstHrInsMiscAmount(100);
			    	
		        if (CT_NM != null && CT_NM.length() > 0) {
		         
			        LocalArCustomerType arCustomerType = arCustomerTypeHome.findByCtName(CT_NM, AD_CMPNY);
				    arCustomer.setArCustomerType(arCustomerType);
			        
			    }

		        // Link to AP Investor
		        if (SPL_SPPLR_CODE != null && SPL_SPPLR_CODE.length() > 0) {
			    	
		        	
		        	LocalApSupplier apSupplier = apSupplierHome .findBySplSupplierCode(SPL_SPPLR_CODE, AD_CMPNY);
		        	arCustomer.setApSupplier(apSupplier);
			
			    }
		        
	        	System.out.println("PANK ELSE");
		        
			    LocalAdPaymentTerm adPaymentTerm = adPaymentTermHome.findByPytName(PYT_NM, AD_CMPNY);
			    arCustomer.setAdPaymentTerm(adPaymentTerm);
			    System.out.println("adPaymentTerm:"+adPaymentTerm);
			    System.out.println("CC_NM--"+CC_NM);
		        //LocalArCustomerClass arCustomerClass = arCustomerClassHome.findByCcName(CC_NM, AD_CMPNY);
			    arCustomer.setArCustomerClass(arCustomerClass);
			    System.out.println("arCustomerClass:"+arCustomerClass);
		        LocalAdBankAccount adBankAccount = adBankAccountHome.findByBaName(BA_NM, AD_CMPNY);
			    arCustomer.setAdBankAccount(adBankAccount);
			    System.out.println("adBankAccount:"+adBankAccount);
	        	LocalArSalesperson arSalesperson = null;
	        	
	        	
	        		
	        	if (HR_DB_NM != null && HR_DB_NM.length() > 0) {
	        		System.out.println("HR_DB_NM="+HR_DB_NM);
		        	LocalHrDeployedBranch hrDeployedBranch = hrDeployedBranchHome.findByDbClientName(HR_DB_NM, AD_CMPNY);
		        	arCustomer.setHrDeployedBranch(hrDeployedBranch);
		        	
		        }
	        	
	        	
	        	if (HR_EMP_BIO_NMBR != null && HR_EMP_BIO_NMBR.length() > 0) {
		        	System.out.println("HR_EMP_BIO_NMBR="+HR_EMP_BIO_NMBR);
	        		LocalHrEmployee hrEmployee = hrEmployeeHome.findByEmpBioNumber(HR_EMP_BIO_NMBR, AD_CMPNY);
	        		
            		arCustomer.setHrEmployee(hrEmployee);
		        	
		        }
	        	
	        	if (PM_USR_EMP_NMBR != null && PM_USR_EMP_NMBR.length() > 0) {
		        	System.out.println("PM_USR_EMP_NMBR="+PM_USR_EMP_NMBR);
	        		LocalPmUser pmUser = pmUserHome.findUsrByEmployeeNumber(PM_USR_EMP_NMBR, AD_CMPNY);
	        		
            		arCustomer.setPmUser(pmUser);
		        }

	           
	        	
	        	if (SLP_SLSPRSN_CODE != null && SLP_SLSPRSN_CODE.length() > 0 && !SLP_SLSPRSN_CODE.equalsIgnoreCase("NO RECORD FOUND")) {

	        		// if he tagged a salesperson for this customer
	        		arSalesperson = arSalespersonHome.findBySlpSalespersonCode(SLP_SLSPRSN_CODE, AD_CMPNY);
				    arCustomer.setArSalesperson(arSalesperson);

	        	} else {
	        	
	        		// if he untagged a salesperson for this customer
	        		if (arCustomer.getArSalesperson() != null) {
	        			
	        			arSalesperson = arSalespersonHome.findBySlpSalespersonCode(arCustomer.getArSalesperson().getSlpSalespersonCode(), AD_CMPNY);
	        			arSalesperson.dropArCustomer(arCustomer);
	        			
	        		}
	        		
	        	}

	        	LocalArSalesperson arSalesperson2 = null;
	        	if (SLP_SLSPRSN_CODE2 != null && SLP_SLSPRSN_CODE2.length() > 0 && !SLP_SLSPRSN_CODE2.equalsIgnoreCase("NO RECORD FOUND")) {

	        		// if he tagged a salesperson for this customer
	        		arSalesperson2 = arSalespersonHome.findBySlpSalespersonCode(SLP_SLSPRSN_CODE2, AD_CMPNY);
			        arCustomer.setCstArSalesperson2(arSalesperson2.getSlpCode());
	        		

	        	} else {
	        	
	        		// if he untagged a salesperson for this customer
	        		arCustomer.setCstArSalesperson2(null);	        		
	        	}
	        	

	        	//Set download status	        	
	        	AdModBranchCustomerDetails bCstDetails= null;
	        	ArrayList newbcstList = new ArrayList();
	        	ArrayList bcstList = details.getBcstList();
	        	Iterator iterBcst = bcstList.iterator();
	        	
	        	int ctr = 0;
	        	
	        	while (iterBcst.hasNext()){
	        		
	        		bCstDetails = (AdModBranchCustomerDetails)iterBcst.next();
	        		
	        		LocalAdBranch adBranch = adBranchHome.findByBrName(bCstDetails.getBcstBranchName(), AD_CMPNY);
	        		
	        		LocalAdBranchCustomer adBranchCustomer = null;
	        		
	        		try{
	        			adBranchCustomer = adBranchCustomerHome.findBcstByCstCodeAndBrCode(arCustomer.getCstCode(), adBranch.getBrCode(),AD_CMPNY);
	        		} catch (FinderException ex){
	        			
	        		}
	        		
	        		bCstDetails.setBcstCustomerDownloadStatus('N');	        		
	        		if (adBranchCustomer != null){
	        			if(adBranchCustomer.getBcstCustomerDownloadStatus()=='N'){
	        				bCstDetails.setBcstCustomerDownloadStatus('N');	        				
	        			}else if(adBranchCustomer.getBcstCustomerDownloadStatus()=='D'){
	        				bCstDetails.setBcstCustomerDownloadStatus('X');
	        			}else if(adBranchCustomer.getBcstCustomerDownloadStatus()=='U'){
	        				bCstDetails.setBcstCustomerDownloadStatus('U');
	        			}else if(adBranchCustomer.getBcstCustomerDownloadStatus()=='X'){
	        				bCstDetails.setBcstCustomerDownloadStatus('X');
	        			}
	        			
	        		}
	        		
	        		newbcstList.add(bCstDetails);	        		
	        	}
	        	
	        	details.setBcstList(newbcstList);	        	
	        	

		        // remove all BranchCustomer
		        
		        LocalAdResponsibility adResponsibility = adResponsibiltyHome.findByPrimaryKey(RS_CODE);
		        
				Collection adBranchCustomers = adBranchCustomerHome.findBcstByCstCodeAndRsName(arCustomer.getCstCode(),
						adResponsibility.getRsName(), AD_CMPNY);

				Iterator i = adBranchCustomers.iterator();

	            while(i.hasNext()) {
	            	
	            	LocalAdBranchCustomer adBranchCustomer = (LocalAdBranchCustomer)i.next();
	            	LocalAdBranch adBranch = adBranchHome.findByPrimaryKey(adBranchCustomer.getAdBranch().getBrCode());

	            	arCustomer.dropAdBranchCustomer(adBranchCustomer);
	            	adBranch.dropAdBranchCustomer(adBranchCustomer);
	            	adBranchCustomer.remove();
	            	
	            }

		        // create new BranchCustomer
		        ArrayList bCstlist = details.getBcstList();
	        	i = bCstlist.iterator();
	        	
	        	while(i.hasNext()) {
	        		
	        		bCstDetails = (AdModBranchCustomerDetails)i.next();
	        		
					try {
						
						glReceivableChartOfAccount = glChartOfAccountHome.findByCoaAccountNumber(
								bCstDetails.getBcstReceivableAccountNumber(), AD_CMPNY);
						
					} catch (FinderException ex) {
						
						throw new ArCCCoaGlReceivableAccountNotFoundException();
						
					}

					Integer glRevChrtOfAccntCode = null;
					
					if ( bCstDetails.getBcstRevenueAccountNumber()!= null &&
							bCstDetails.getBcstRevenueAccountNumber().length() > 0){
						
						try {
							
							glRevenueChartOfAccount = glChartOfAccountHome.findByCoaAccountNumber(
									bCstDetails.getBcstRevenueAccountNumber(), AD_CMPNY);
							
							glRevChrtOfAccntCode = glRevenueChartOfAccount.getCoaCode();
							
						} catch (FinderException ex) {
							
							throw new ArCCCoaGlRevenueAccountNotFoundException();
							
						}
						
					}
					
					Integer glUnEarnedInterestChrtOfAccntCode = null;
					
					if ( bCstDetails.getBcstUnEarnedInterestAccountNumber()!= null &&
							bCstDetails.getBcstUnEarnedInterestAccountNumber().length() > 0){
						
						try {
							
							glUnEarnedInterestChartOfAccount = glChartOfAccountHome.findByCoaAccountNumber(
									bCstDetails.getBcstUnEarnedInterestAccountNumber(), AD_CMPNY);
							
							glUnEarnedInterestChrtOfAccntCode = glUnEarnedInterestChartOfAccount.getCoaCode();
							
						} catch (FinderException ex) {
							
							throw new ArCCCoaGlUnEarnedInterestAccountNotFoundException();
							
						}
						
					}
					
					
					Integer glEarnedInterestChrtOfAccntCode = null;
					
					if ( bCstDetails.getBcstEarnedInterestAccountNumber()!= null &&
							bCstDetails.getBcstEarnedInterestAccountNumber().length() > 0){
						
						try {
							
							glEarnedInterestChartOfAccount = glChartOfAccountHome.findByCoaAccountNumber(
									bCstDetails.getBcstEarnedInterestAccountNumber(), AD_CMPNY);
							
							glEarnedInterestChrtOfAccntCode = glEarnedInterestChartOfAccount.getCoaCode();
							
						} catch (FinderException ex) {
							
							throw new ArCCCoaGlEarnedInterestAccountNotFoundException();
							
						}
						
					}

					
Integer glUnEarnedPenaltyChrtOfAccntCode = null;
					
					if ( bCstDetails.getBcstUnEarnedPenaltyAccountNumber()!= null &&
							bCstDetails.getBcstUnEarnedPenaltyAccountNumber().length() > 0){
						
						try {
							
							glUnEarnedPenaltyChartOfAccount = glChartOfAccountHome.findByCoaAccountNumber(
									bCstDetails.getBcstUnEarnedPenaltyAccountNumber(), AD_CMPNY);
							
							glUnEarnedPenaltyChrtOfAccntCode = glUnEarnedPenaltyChartOfAccount.getCoaCode();
							
						} catch (FinderException ex) {
							
							throw new ArCCCoaGlUnEarnedPenaltyAccountNotFoundException();
							
						}
						
					}
					
					
					Integer glEarnedPenaltyChrtOfAccntCode = null;
					
					if ( bCstDetails.getBcstEarnedPenaltyAccountNumber()!= null &&
							bCstDetails.getBcstEarnedPenaltyAccountNumber().length() > 0){
						
						try {
							
							glEarnedPenaltyChartOfAccount = glChartOfAccountHome.findByCoaAccountNumber(
									bCstDetails.getBcstEarnedPenaltyAccountNumber(), AD_CMPNY);
							
							glEarnedPenaltyChrtOfAccntCode = glEarnedPenaltyChartOfAccount.getCoaCode();
							
						} catch (FinderException ex) {
							
							throw new ArCCCoaGlEarnedPenaltyAccountNotFoundException();
							
						}
						
					}
					
					System.out.println("Customer DL Stat: " + bCstDetails.getBcstCustomerDownloadStatus());
					
	        		LocalAdBranchCustomer adBranchCustomer = adBranchCustomerHome.create(
	        				glReceivableChartOfAccount.getCoaCode(), 
	        				glRevChrtOfAccntCode, 
	        				glUnEarnedInterestChrtOfAccntCode,
	        				glEarnedInterestChrtOfAccntCode,
	        				glUnEarnedPenaltyChrtOfAccntCode,
	        				glEarnedPenaltyChrtOfAccntCode,
	        				bCstDetails.getBcstCustomerDownloadStatus(), AD_CMPNY);
	        		//arCustomer.addAdBranchCustomer(adBranchCustomer);
	        		adBranchCustomer.setArCustomer(arCustomer);
	        		
					LocalAdBranch adBranch = adBranchHome.findByBrName(bCstDetails.getBcstBranchName(), AD_CMPNY);
					//adBranch.addAdBranchCustomer(adBranchCustomer);
					adBranchCustomer.setAdBranch(adBranch);

	        	}		        

	        	LocalInvLineItemTemplate invLineItemTemplate = null;
	        	
	        	if(LIT_NM != null && LIT_NM.length() > 0 && !LIT_NM.equalsIgnoreCase("NO RECORD FOUND")) {
		        	
	        		invLineItemTemplate = invLineItemTemplateHome.findByLitName(LIT_NM, AD_CMPNY);
				    arCustomer.setInvLineItemTemplate(invLineItemTemplate);

			        	
		        } else {
		        	
		        	if (arCustomer.getInvLineItemTemplate() != null) {
	        			
		        		invLineItemTemplate = invLineItemTemplateHome.findByLitName(arCustomer.getInvLineItemTemplate().getLitName(), AD_CMPNY);
		        		invLineItemTemplate.dropArCustomer(arCustomer);
	        			
	        		}
		        	
		        }
	        	
	        }
	         System.out.println("trace 5");
	      // set purchase requisition approval status
				
				String CST_APPRVL_STATUS = null;
				
				//if(!isDraft) {
					
					
					LocalAdApproval adApproval = adApprovalHome.findByAprAdCompany(AD_CMPNY);
					
					// check if ap voucher approval is enabled
					if (adApproval.getAprEnableArCustomer() == EJBCommon.FALSE) {
						CST_APPRVL_STATUS = "N/A";
					
					} else {
						// for approval, create approval queue
						
						// get user who is requesting details.getPrLastModifiedBy() adUser
						
							
						LocalAdUser adUser = adUserHome.findByUsrName(details.getCstLastModifiedBy(), AD_CMPNY);
						
						
						Collection adUsers;
						
						try{
							
						adUsers = adUserHome.findUsrByDepartmentHead(adUser.getUsrDept(), (byte)0,AD_CMPNY);
						
						} catch (FinderException ex) {
							
							throw new GlobalNoApprovalRequesterFoundException();
							
						}
						
						
						try{
								
						adUsers = adUserHome.findUsrByDepartmentHead(adUser.getUsrDept(), (byte)1,AD_CMPNY);
						
						} catch (FinderException ex) {
							
							throw new GlobalNoApprovalApproverFoundException();
							
						}
						if (adUsers.isEmpty()) {
				
							throw new GlobalNoApprovalApproverFoundException();
							
						} else {
							
							Iterator j = adUsers.iterator();
							while (j.hasNext()) {		
								LocalAdUser adUserHead = (LocalAdUser)j.next();
								LocalAdApprovalQueue adApprovalQueue = adApprovalQueueHome.create(EJBCommon.TRUE, "AR CUSTOMER", arCustomer.getCstCode(), 
										arCustomer.getCstCustomerCode(), arCustomer.getCstEntryDate(), "OR", (byte) 1, AD_BRNCH, AD_CMPNY);
								adUserHead.addAdApprovalQueue(adApprovalQueue);	      				 	
							}    				 
						}
						CST_APPRVL_STATUS = "PENDING";
					} 
						
					   
					
					arCustomer.setCstApprovalStatus(CST_APPRVL_STATUS);
					
					// set post purchase order
					
					if(CST_APPRVL_STATUS.equals("N/A")) {
					
						 arCustomer.setCstPosted(EJBCommon.TRUE);
						 //arCustomer.setCstEnable(EJBCommon.TRUE);
						 arCustomer.setCstPostedBy(arCustomer.getCstLastModifiedBy());
						 arCustomer.setCstDatePosted(EJBCommon.getGcCurrentDateWoTime().getTime());
					 
					 }
					
					
					
			//	}
	        
	 	} catch (GlobalRecordAlreadyExistException ex) {
	 		
	 		getSessionContext().setRollbackOnly();
	 		throw ex;
	 		
	 	} catch (GlobalNoApprovalApproverFoundException ex) {
	 			
 			getSessionContext().setRollbackOnly();
 			throw ex;
 			
 		} catch (GlobalNoApprovalRequesterFoundException ex) {
 			
 			getSessionContext().setRollbackOnly();
 			throw ex;
	 		
	 	} catch(GlobalNameAndAddressAlreadyExistsException ex) {
	 		
	 		getSessionContext().setRollbackOnly();
	 		throw ex;
	 		
	 	} catch (ArCCCoaGlReceivableAccountNotFoundException ex) {
	 		
	 		getSessionContext().setRollbackOnly();
	 		throw ex;
	 		
	 	} catch (ArCCCoaGlRevenueAccountNotFoundException ex) {
	 		
	 		getSessionContext().setRollbackOnly();
	 		throw ex;
	 		
	 	} catch (ArCCCoaGlUnEarnedInterestAccountNotFoundException ex) {
	 		
	 		getSessionContext().setRollbackOnly();
	 		throw ex;
	 		
	 	} catch (ArCCCoaGlEarnedInterestAccountNotFoundException ex) {
	 		
	 		getSessionContext().setRollbackOnly();
	 		throw ex;
	 		
	 	} catch (Exception ex) {
	            
	    	Debug.printStackTrace(ex);
	    	getSessionContext().setRollbackOnly();
	    	throw new EJBException(ex.getMessage());
	    	
	    }
		return arCustomer.getCstCode();   
	 }
	 
    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArModCustomerDetails getArCstByCstCode(Integer CST_CODE, Integer RS_CODE, Integer AD_CMPNY)
        throws GlobalNoRecordFoundException  {
         
        Debug.print("ArCustomerEntryControllerBean getArCstByCstCode");
        
        LocalArCustomerHome arCustomerHome = null;
        LocalGlChartOfAccountHome glChartOfAccountHome = null;
        LocalAdBranchCustomerHome adBranchCustomerHome = null;
        LocalAdResponsibilityHome adResponsibilityHome = null;
        LocalArSalespersonHome arSalespersonHome = null;
    
        LocalArCustomer arCustomer = null;
        LocalGlChartOfAccount glChartOfAccount = null;
        LocalArSalesperson arSalesperson = null;

        // Initialize EJB Home
        
        try {
            
            arCustomerHome = (LocalArCustomerHome)EJBHomeFactory.
                lookUpLocalHome(LocalArCustomerHome.JNDI_NAME, LocalArCustomerHome.class);
            glChartOfAccountHome = (LocalGlChartOfAccountHome)EJBHomeFactory.
                lookUpLocalHome(LocalGlChartOfAccountHome.JNDI_NAME, LocalGlChartOfAccountHome.class);
            adBranchCustomerHome = (LocalAdBranchCustomerHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdBranchCustomerHome.JNDI_NAME, LocalAdBranchCustomerHome.class);
            adResponsibilityHome = (LocalAdResponsibilityHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdResponsibilityHome.JNDI_NAME, LocalAdResponsibilityHome.class);
            arSalespersonHome = (LocalArSalespersonHome)EJBHomeFactory.
            	lookUpLocalHome(LocalArSalespersonHome.JNDI_NAME, LocalArSalespersonHome.class);


        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
                
        try {
        	
        	try {
        	
        		arCustomer = arCustomerHome.findByPrimaryKey(CST_CODE);      

        	} catch (FinderException ex) {
        		
        		throw new GlobalNoRecordFoundException();
        		
        	}
        	
        	LocalAdResponsibility adResponsibility = null;
        	
        	try {
            	
        		adResponsibility = adResponsibilityHome.findByPrimaryKey(RS_CODE);      

            } catch (FinderException ex) {
            		
            	throw new GlobalNoRecordFoundException();
            		
            }
        	
			ArrayList list = new ArrayList();
			
			// get Branch Customers lines if any
			
			Collection adBranchCustomers = adBranchCustomerHome.findBcstByCstCodeAndRsName(arCustomer.getCstCode(),
					adResponsibility.getRsName(), AD_CMPNY);
			
			Iterator i = adBranchCustomers.iterator();
			
			while (i.hasNext()) {
				
				LocalAdBranchCustomer adBranchCustomer = (LocalAdBranchCustomer) i.next();
				
				AdModBranchCustomerDetails bCstDetails = new AdModBranchCustomerDetails();
				
				try {
					
					glChartOfAccount = glChartOfAccountHome.findByPrimaryKey(adBranchCustomer.getBcstGlCoaReceivableAccount());
				
				} catch (FinderException ex){
					
					throw new GlobalNoRecordFoundException();
					
				}
				
				bCstDetails.setBcstReceivableAccountDescription(glChartOfAccount.getCoaAccountDescription());
				bCstDetails.setBcstReceivableAccountNumber(glChartOfAccount.getCoaAccountNumber());
				
				if (adBranchCustomer.getBcstGlCoaRevenueAccount() != null) {
					
					try {
						
						glChartOfAccount = glChartOfAccountHome.findByPrimaryKey(adBranchCustomer.getBcstGlCoaRevenueAccount());
						
						bCstDetails.setBcstRevenueAccountDescription(glChartOfAccount.getCoaAccountDescription());
						bCstDetails.setBcstRevenueAccountNumber(glChartOfAccount.getCoaAccountNumber());

						
					} catch (FinderException ex){
						
						throw new GlobalNoRecordFoundException();
						
					}
					
				}
				
				
				if (adBranchCustomer.getBcstGlCoaUnEarnedInterestAccount() != null) {
					
					try {
						
						glChartOfAccount = glChartOfAccountHome.findByPrimaryKey(adBranchCustomer.getBcstGlCoaUnEarnedInterestAccount());
						
						bCstDetails.setBcstUnEarnedInterestAccountDescription(glChartOfAccount.getCoaAccountDescription());
						bCstDetails.setBcstUnEarnedInterestAccountNumber(glChartOfAccount.getCoaAccountNumber());

						
					} catch (FinderException ex){
						
						throw new GlobalNoRecordFoundException();
						
					}
					
				}
				
				if (adBranchCustomer.getBcstGlCoaEarnedInterestAccount() != null) {
					
					try {
						
						glChartOfAccount = glChartOfAccountHome.findByPrimaryKey(adBranchCustomer.getBcstGlCoaEarnedInterestAccount());
						
						bCstDetails.setBcstEarnedInterestAccountDescription(glChartOfAccount.getCoaAccountDescription());
						bCstDetails.setBcstEarnedInterestAccountNumber(glChartOfAccount.getCoaAccountNumber());

						
					} catch (FinderException ex){
						
						throw new GlobalNoRecordFoundException();
						
					}
					
				}
				
if (adBranchCustomer.getBcstGlCoaUnEarnedPenaltyAccount() != null) {
					
					try {
						
						glChartOfAccount = glChartOfAccountHome.findByPrimaryKey(adBranchCustomer.getBcstGlCoaUnEarnedPenaltyAccount());
						
						bCstDetails.setBcstUnEarnedPenaltyAccountDescription(glChartOfAccount.getCoaAccountDescription());
						bCstDetails.setBcstUnEarnedPenaltyAccountNumber(glChartOfAccount.getCoaAccountNumber());

						
					} catch (FinderException ex){
						
						throw new GlobalNoRecordFoundException();
						
					}
					
				}
				
				if (adBranchCustomer.getBcstGlCoaEarnedPenaltyAccount() != null) {
					
					try {
						
						glChartOfAccount = glChartOfAccountHome.findByPrimaryKey(adBranchCustomer.getBcstGlCoaEarnedPenaltyAccount());
						
						bCstDetails.setBcstEarnedPenaltyAccountDescription(glChartOfAccount.getCoaAccountDescription());
						bCstDetails.setBcstEarnedPenaltyAccountNumber(glChartOfAccount.getCoaAccountNumber());

						
					} catch (FinderException ex){
						
						throw new GlobalNoRecordFoundException();
						
					}
					
				}
				
				bCstDetails.setBcstBranchName(adBranchCustomer.getAdBranch().getBrName());
				bCstDetails.setBcstBranchCode(adBranchCustomer.getAdBranch().getBrBranchCode());
				
				list.add(bCstDetails);
				
			}
        	
            ArModCustomerDetails cstDetails = new ArModCustomerDetails();
				cstDetails.setCstCode(arCustomer.getCstCode());
				cstDetails.setCstCustomerCode(arCustomer.getCstCustomerCode());
				cstDetails.setCstName(arCustomer.getCstName());
				cstDetails.setCstDescription(arCustomer.getCstDescription());
				cstDetails.setCstPaymentMethod(arCustomer.getCstPaymentMethod());
				cstDetails.setCstCreditLimit(arCustomer.getCstCreditLimit());
				cstDetails.setCstAddress(arCustomer.getCstAddress());
				cstDetails.setCstCity(arCustomer.getCstCity());
				cstDetails.setCstStateProvince(arCustomer.getCstStateProvince());
				cstDetails.setCstPostalCode(arCustomer.getCstPostalCode()); 
				cstDetails.setCstCountry(arCustomer.getCstCountry()); 
				cstDetails.setCstEmployeeID(arCustomer.getCstEmployeeID());
				cstDetails.setCstAccountNumber(arCustomer.getCstAccountNumber());
				cstDetails.setCstContact(arCustomer.getCstContact());
				cstDetails.setCstPhone(arCustomer.getCstPhone());
				cstDetails.setCstFax(arCustomer.getCstFax());
				cstDetails.setCstAlternatePhone(arCustomer.getCstAlternatePhone()); 
				cstDetails.setCstAlternateContact(arCustomer.getCstAlternateContact());
				cstDetails.setCstEmail(arCustomer.getCstEmail());
				cstDetails.setCstBillToAddress(arCustomer.getCstBillToAddress());
				cstDetails.setCstBillToContact(arCustomer.getCstBillToContact());
				cstDetails.setCstBillToAltContact(arCustomer.getCstBillToAltContact());
				cstDetails.setCstBillToPhone(arCustomer.getCstBillToPhone());
				cstDetails.setCstBillingHeader(arCustomer.getCstBillingHeader());
				cstDetails.setCstBillingFooter(arCustomer.getCstBillingFooter());
				cstDetails.setCstBillingHeader2(arCustomer.getCstBillingHeader2());
				cstDetails.setCstBillingFooter2(arCustomer.getCstBillingFooter2());
				cstDetails.setCstBillingHeader3(arCustomer.getCstBillingHeader3());
				cstDetails.setCstBillingFooter3(arCustomer.getCstBillingFooter3());
				cstDetails.setCstBillingSignatory(arCustomer.getCstBillingSignatory());
				cstDetails.setCstSignatoryTitle(arCustomer.getCstSignatoryTitle());
				cstDetails.setCstShipToAddress(arCustomer.getCstShipToAddress());
				cstDetails.setCstShipToContact(arCustomer.getCstShipToContact());
				cstDetails.setCstShipToAltContact(arCustomer.getCstShipToAltContact());
				cstDetails.setCstShipToPhone(arCustomer.getCstShipToPhone());				
				cstDetails.setCstTin(arCustomer.getCstTin()); 
				cstDetails.setCstEnable(arCustomer.getCstEnable());	 
				cstDetails.setCstEnableRetailCashier(arCustomer.getCstEnableRetailCashier());
				cstDetails.setCstEnableRebate(arCustomer.getCstEnableRebate());
				cstDetails.setCstAutoComputeInterest(arCustomer.getCstAutoComputeInterest());
				cstDetails.setCstAutoComputePenalty(arCustomer.getCstAutoComputePenalty());
				cstDetails.setCstMobilePhone(arCustomer.getCstMobilePhone());
				cstDetails.setCstAlternateMobilePhone(arCustomer.getCstAlternateMobilePhone());
				cstDetails.setCstBirthday(arCustomer.getCstBirthday());
				cstDetails.setCstDealPrice(arCustomer.getCstDealPrice());
				cstDetails.setCstArea(arCustomer.getCstArea());
				cstDetails.setCstSquareMeter(arCustomer.getCstSquareMeter());
				cstDetails.setCstNumbersParking(arCustomer.getCstNumbersParking());
				cstDetails.setCstMonthlyInterestRate(arCustomer.getCstMonthlyInterestRate());
				cstDetails.setCstParkingID(arCustomer.getCstParkingID());
				cstDetails.setCstAssociationDuesRate(arCustomer.getCstAssociationDuesRate());
				cstDetails.setCstRealPropertyTaxRate(arCustomer.getCstRealPropertyTaxRate());
				cstDetails.setCstWordPressCustomerID(arCustomer.getCstWordPressCustomerID()); 
				cstDetails.setCstEntryDate(arCustomer.getCstEntryDate());
				cstDetails.setCstEffectivityDays(arCustomer.getCstEffectivityDays());
				cstDetails.setCstAdLvRegion(arCustomer.getCstAdLvRegion());
				cstDetails.setCstMemo(arCustomer.getCstMemo());
				cstDetails.setCstCustomerBatch(arCustomer.getCstCustomerBatch());
				cstDetails.setCstCustomerDepartment(arCustomer.getCstCustomerDepartment());
		        cstDetails.setCstSlpSalespersonCode(arCustomer.getArSalesperson() != null ? 
			            arCustomer.getArSalesperson().getSlpSalespersonCode() :
			            null);
		        cstDetails.setCstApprovalStatus(arCustomer.getCstApprovalStatus());
	            cstDetails.setCstReasonForRejection(arCustomer.getCstReasonForRejection());
	            cstDetails.setCstPosted(arCustomer.getCstPosted());

	            cstDetails.setCstCreatedBy(arCustomer.getCstCreatedBy());
	            cstDetails.setCstDateCreated(arCustomer.getCstDateCreated());
	            cstDetails.setCstLastModifiedBy(arCustomer.getCstLastModifiedBy());
	            cstDetails.setCstDateLastModified(arCustomer.getCstDateLastModified());
	            cstDetails.setCstApprovedRejectedBy(arCustomer.getCstApprovedRejectedBy());
	            cstDetails.setCstDateApprovedRejected(arCustomer.getCstDateApprovedRejected());
	            cstDetails.setCstPostedBy(arCustomer.getCstPostedBy());
	            cstDetails.setCstDatePosted(arCustomer.getCstDatePosted());
		        
	         // HRIS
	            if(arCustomer.getHrEmployee() != null) {
	            	
	            	cstDetails.setCstHrEmployeeNumber(arCustomer.getHrEmployee().getEmpEmployeeNumber());
	            	cstDetails.setCstHrBioNumber(arCustomer.getHrEmployee().getEmpBioNumber());
	            	cstDetails.setCstHrManagingBranch(arCustomer.getHrEmployee().getEmpManagingBranch());
	            	cstDetails.setCstHrCurrentJobPosition(arCustomer.getHrEmployee().getEmpCurrentJobPosition());

	            }
	            
	            if(arCustomer.getHrDeployedBranch() != null) {
	            	
	            	cstDetails.setCstHrDeployedBranchName(arCustomer.getHrDeployedBranch().getDbClientName());

	            }
	            
		        
		        LocalArSalesperson arSalesperson2 = null;
		        
		        if(arCustomer.getCstArSalesperson2() != null) {
		        	
			        try {
			        	arSalesperson2 = arSalespersonHome.findByPrimaryKey(arCustomer.getCstArSalesperson2());
			        	
			        } catch(Exception ex) {
			        	
			        }
		        
			        cstDetails.setCstSlpSalespersonCode2(arSalesperson2.getSlpSalespersonCode());
			        
		        } else
		        	cstDetails.setCstSlpSalespersonCode2(null);
		        
		        System.out.println("pank sampol : " + cstDetails.getCstSlpSalespersonCode2());
		        
	        	
		        cstDetails.setCstCtName(arCustomer.getArCustomerType() != null ? 
		            arCustomer.getArCustomerType().getCtName() :
		            null);
				
		        cstDetails.setCstPytName(arCustomer.getAdPaymentTerm() != null ? 
		            arCustomer.getAdPaymentTerm().getPytName() :
		            null);	 
		            
		        cstDetails.setCstCcName(arCustomer.getArCustomerClass() != null ? 
		            arCustomer.getArCustomerClass().getCcName() :
		            null);
		        
		        cstDetails.setCstCcName(arCustomer.getArCustomerClass() != null ? 
			            arCustomer.getArCustomerClass().getCcName() :
			            null);
		        
		        cstDetails.setCstSupplierCode(arCustomer.getApSupplier()!= null ? 
		        		arCustomer.getApSupplier().getSplSupplierCode() :
	    					null);
		            
		        glChartOfAccount = glChartOfAccountHome.findByPrimaryKey(arCustomer.getCstGlCoaReceivableAccount());
		       	        		        
		        cstDetails.setCstGlCoaReceivableAccountNumber(glChartOfAccount.getCoaAccountNumber());
		        cstDetails.setCstGlCoaReceivableAccountDescription(glChartOfAccount.getCoaAccountDescription());
		        
		        if (arCustomer.getCstGlCoaRevenueAccount() != null) {
		        
			        glChartOfAccount = glChartOfAccountHome.findByPrimaryKey(arCustomer.getCstGlCoaRevenueAccount());
			          		        
			        cstDetails.setCstGlCoaRevenueAccountNumber(glChartOfAccount.getCoaAccountNumber());
			        cstDetails.setCstGlCoaRevenueAccountDescription(glChartOfAccount.getCoaAccountDescription());
			        
			    }
		        
		        
		        if (arCustomer.getCstGlCoaUnEarnedInterestAccount() != null) {
			        
			        glChartOfAccount = glChartOfAccountHome.findByPrimaryKey(arCustomer.getCstGlCoaUnEarnedInterestAccount());
			          		        
			        cstDetails.setCstGlCoaUnEarnedInterestAccountNumber(glChartOfAccount.getCoaAccountNumber());
			        cstDetails.setCstGlCoaUnEarnedInterestAccountDescription(glChartOfAccount.getCoaAccountDescription());
			        
			    }
		        
		        if (arCustomer.getCstGlCoaEarnedInterestAccount() != null) {
			        
			        glChartOfAccount = glChartOfAccountHome.findByPrimaryKey(arCustomer.getCstGlCoaEarnedInterestAccount());
			          		        
			        cstDetails.setCstGlCoaEarnedInterestAccountNumber(glChartOfAccount.getCoaAccountNumber());
			        cstDetails.setCstGlCoaEarnedInterestAccountDescription(glChartOfAccount.getCoaAccountDescription());
			        
			    }
		        
if (arCustomer.getCstGlCoaUnEarnedPenaltyAccount() != null) {
			        
			        glChartOfAccount = glChartOfAccountHome.findByPrimaryKey(arCustomer.getCstGlCoaUnEarnedPenaltyAccount());
			          		        
			        cstDetails.setCstGlCoaUnEarnedPenaltyAccountNumber(glChartOfAccount.getCoaAccountNumber());
			        cstDetails.setCstGlCoaUnEarnedPenaltyAccountDescription(glChartOfAccount.getCoaAccountDescription());
			        
			    }
		        
		        if (arCustomer.getCstGlCoaEarnedPenaltyAccount() != null) {
			        
			        glChartOfAccount = glChartOfAccountHome.findByPrimaryKey(arCustomer.getCstGlCoaEarnedPenaltyAccount());
			          		        
			        cstDetails.setCstGlCoaEarnedPenaltyAccountNumber(glChartOfAccount.getCoaAccountNumber());
			        cstDetails.setCstGlCoaEarnedPenaltyAccountDescription(glChartOfAccount.getCoaAccountDescription());
			        
			    }
		        
		        cstDetails.setCstBaName(arCustomer.getAdBankAccount().getBaName());
		        
		        cstDetails.setCstLitName(arCustomer.getInvLineItemTemplate() != null ?
	        			arCustomer.getInvLineItemTemplate().getLitName() : null);
	        	
		        cstDetails.setBcstList(list);
        		
               return cstDetails;              

        } catch (GlobalNoRecordFoundException ex) {

            throw ex;
        	
        } catch (Exception ex) {
        	
        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());
        	
        }
        
    }	 
    
    
    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList getAdBaAll(Integer AD_CMPNY) {
                    
        Debug.print("ArCustomerEntryControllerBean getAdBaAll");
        
        LocalAdBankAccountHome adBankAccountHome = null;

        ArrayList list = new ArrayList();
        
        // Initialize EJB Home
        
        try {
            
        	adBankAccountHome = (LocalAdBankAccountHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdBankAccountHome.JNDI_NAME, LocalAdBankAccountHome.class);
            
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {
            
            Collection adBankAccounts = adBankAccountHome.findEnabledBaAll(AD_CMPNY);
	        
	        Iterator i = adBankAccounts.iterator();
	               
	        while (i.hasNext()) {
	        	
	        	LocalAdBankAccount adBankAccount = (LocalAdBankAccount)i.next();
	        	
	        	list.add(adBankAccount.getBaName());
	        	
	        }
	        
	        return list;
          
        } catch (Exception ex) {
        	
        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());
        	
        }
            
    }
    
    
    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public boolean getArCstGlCoaRevenueAccountEnable(Integer AD_CMPNY) {
                    
        Debug.print("ArCustomerEntryControllerBean getArCstGlCoaRevenueAccountEnable");
        
        LocalArAutoAccountingSegmentHome arAutoAccountingSegmentHome = null;
        
        // Initialize EJB Home
        
        try {
            
            arAutoAccountingSegmentHome = (LocalArAutoAccountingSegmentHome)EJBHomeFactory.
                lookUpLocalHome(LocalArAutoAccountingSegmentHome.JNDI_NAME, LocalArAutoAccountingSegmentHome.class);
            
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {
        	
        	Collection arAutoAccountingSegments = 
        	   arAutoAccountingSegmentHome.findByAasClassTypeAndAaAccountType("AR CUSTOMER", "REVENUE", AD_CMPNY);

            if (!arAutoAccountingSegments.isEmpty()) {
            	
            	return true;
            	
            } else {
            	
            	return false;
            	
            }

	        
        } catch (Exception ex) {
        	
        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());
        	
        }	        
            
    }    
    
    
    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArModCustomerClassDetails getArCcByCcName(String CC_NM, Integer AD_CMPNY) {
                    
        Debug.print("ArCustomerEntryControllerBean getArCcByCcName");
        
        LocalArCustomerClassHome arCustomerClassHome = null;
        LocalGlChartOfAccountHome glChartOfAccountHome = null;

        // Initialize EJB Home
        
        try {
            
        	arCustomerClassHome = (LocalArCustomerClassHome)EJBHomeFactory.
                lookUpLocalHome(LocalArCustomerClassHome.JNDI_NAME, LocalArCustomerClassHome.class);
            glChartOfAccountHome = (LocalGlChartOfAccountHome)EJBHomeFactory.
                lookUpLocalHome(LocalGlChartOfAccountHome.JNDI_NAME, LocalGlChartOfAccountHome.class);
            
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {
            
            LocalArCustomerClass arCustomerClass = arCustomerClassHome.findByCcName(CC_NM, AD_CMPNY);
            ArModCustomerClassDetails mdetails = new ArModCustomerClassDetails();
            
            LocalGlChartOfAccount glChartOfAccount = glChartOfAccountHome.findByPrimaryKey(arCustomerClass.getCcGlCoaReceivableAccount());
            
            LocalGlChartOfAccount glRevenueAccount = null;
	        LocalGlChartOfAccount glUnEarnedInterestAccount = null;
	        LocalGlChartOfAccount glEarnedInterestAccount = null;
	        LocalGlChartOfAccount glUnEarnedPenaltyAccount = null;
	        LocalGlChartOfAccount glEarnedPenaltyAccount = null;
	        
	        if (arCustomerClass.getCcGlCoaRevenueAccount() != null) {               
		        	
	        	glRevenueAccount = glChartOfAccountHome.findByPrimaryKey(arCustomerClass.getCcGlCoaRevenueAccount());                    
	        	
	        }
	        
	        if (arCustomerClass.getCcGlCoaUnEarnedInterestAccount() != null) {               
		        	
	        	glUnEarnedInterestAccount = glChartOfAccountHome.findByPrimaryKey(arCustomerClass.getCcGlCoaUnEarnedInterestAccount());                    
	        	
	        }
	        
	        if (arCustomerClass.getCcGlCoaEarnedInterestAccount() != null) {               
		        	
	        	glEarnedInterestAccount = glChartOfAccountHome.findByPrimaryKey(arCustomerClass.getCcGlCoaEarnedInterestAccount());                    
	        	
	        }
	        
	        
	        if (arCustomerClass.getCcGlCoaUnEarnedPenaltyAccount() != null) {               
		        	
	        	glUnEarnedPenaltyAccount = glChartOfAccountHome.findByPrimaryKey(arCustomerClass.getCcGlCoaUnEarnedPenaltyAccount());                    
	        	
	        }
	        
	        if (arCustomerClass.getCcGlCoaEarnedPenaltyAccount() != null) {               
		        	
	        	glEarnedPenaltyAccount = glChartOfAccountHome.findByPrimaryKey(arCustomerClass.getCcGlCoaEarnedPenaltyAccount());                    
	        	
	        }
            
            mdetails.setCcGlCoaReceivableAccountNumber(glChartOfAccount != null ? glChartOfAccount.getCoaAccountNumber() : null);
            mdetails.setCcGlCoaRevenueAccountNumber(glRevenueAccount != null ? glRevenueAccount.getCoaAccountNumber() : null);
            
            mdetails.setCcGlCoaUnEarnedInterestAccountNumber(glUnEarnedInterestAccount != null ? glUnEarnedInterestAccount.getCoaAccountNumber() : null);
            mdetails.setCcGlCoaEarnedInterestAccountNumber(glEarnedInterestAccount != null ? glEarnedInterestAccount.getCoaAccountNumber() : null);
            
            mdetails.setCcGlCoaUnEarnedPenaltyAccountNumber(glUnEarnedPenaltyAccount!=null?glUnEarnedPenaltyAccount.getCoaAccountNumber():null);
            mdetails.setCcGlCoaEarnedPenaltyAccountNumber(glEarnedPenaltyAccount!=null?glEarnedPenaltyAccount.getCoaAccountNumber():null);

            
            mdetails.setCcGlCoaReceivableAccountDescription(glChartOfAccount != null ? glChartOfAccount.getCoaAccountDescription() : null);
            mdetails.setCcGlCoaRevenueAccountDescription(glRevenueAccount != null ? glRevenueAccount.getCoaAccountDescription() : null);
            
            mdetails.setCcGlCoaUnEarnedInterestAccountDescription(glUnEarnedInterestAccount != null ? glUnEarnedInterestAccount.getCoaAccountDescription() : null);
            mdetails.setCcGlCoaEarnedInterestAccountDescription(glEarnedInterestAccount != null ? glEarnedInterestAccount.getCoaAccountDescription() : null);
            
            mdetails.setCcGlCoaUnEarnedPenaltyAccountDescription(glUnEarnedPenaltyAccount != null ? glUnEarnedPenaltyAccount.getCoaAccountDescription() : null);
            mdetails.setCcGlCoaEarnedPenaltyAccountDescription(glEarnedPenaltyAccount != null ? glEarnedPenaltyAccount.getCoaAccountDescription() : null);
            
            
           /* if (arCustomerClass.getCcGlCoaRevenueAccount() != null) {
            	
            	glChartOfAccount = glChartOfAccountHome.findByPrimaryKey(arCustomerClass.getCcGlCoaRevenueAccount());
            	
                mdetails.setCcGlCoaRevenueAccountNumber(glChartOfAccount.getCoaAccountNumber());
                mdetails.setCcGlCoaRevenueAccountDescription(glChartOfAccount.getCoaAccountDescription());
            	
            } */           
            mdetails.setCcNextCustomerCode(arCustomerClass.getCcNextCustomerCode());
            mdetails.setCcCustomerBatch(arCustomerClass.getCcCustomerBatch());
            mdetails.setCcDealPrice(arCustomerClass.getCcDealPrice());
            mdetails.setCcMonthlyInterestRate(arCustomerClass.getCcMonthlyInterestRate());
            mdetails.setCcCreditLimit(arCustomerClass.getCcCreditLimit());
            
            mdetails.setCcEnableRebate(arCustomerClass.getCcEnableRebate());
            mdetails.setCcAutoComputeInterest(arCustomerClass.getCcAutoComputeInterest());
            mdetails.setCcAutoComputePenalty(arCustomerClass.getCcAutoComputePenalty());
            
            return mdetails;
          
        } catch (Exception ex) {
        	
        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());
        	
        }
            
    }  
    
    
    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArModCustomerTypeDetails getArCtByCtName(String CT_NM, Integer AD_CMPNY) {
                    
        Debug.print("ArCustomerEntryControllerBean getArCtByCtName");
        
        LocalArCustomerTypeHome arCustomerTypeHome = null;
        
        // Initialize EJB Home
        
        try {
            
        	arCustomerTypeHome = (LocalArCustomerTypeHome)EJBHomeFactory.
                lookUpLocalHome(LocalArCustomerTypeHome.JNDI_NAME, LocalArCustomerTypeHome.class);
                
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {
        
            LocalArCustomerType arCustomerType = arCustomerTypeHome.findByCtName(CT_NM, AD_CMPNY);
        
            ArModCustomerTypeDetails mdetails = new ArModCustomerTypeDetails();
            mdetails.setCtBaName(arCustomerType.getAdBankAccount().getBaName());
            
            return mdetails;           
          
        } catch (Exception ex) {
        	
        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());
        	
        }
            
    }            

    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList getAdBrByRspnsblty(Integer RS_CODE, Integer AD_CMPNY) 
    	throws GlobalNoRecordFoundException{
    	
    	Debug.print("AdStandardMemoLineControllerBean getBrAll");

    	LocalAdBranchHome adBranchHome = null;
    	LocalAdBranch adBranch = null;

    	Collection adBranches = null;
    	
        ArrayList list = new ArrayList();
        
        // Initialize EJB Home

        try {
            
            adBranchHome = (LocalAdBranchHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdBranchHome.JNDI_NAME, LocalAdBranchHome.class);

        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {
        	
        	adBranches = adBranchHome.findBrAll(AD_CMPNY);
		
        } catch (FinderException ex) {
            
        } catch (Exception ex) {
            
            throw new EJBException(ex.getMessage());
        }
        
        if (adBranches.isEmpty()) {
        	
            throw new GlobalNoRecordFoundException();
        	
        }
        
        Iterator i = adBranches.iterator();
        
        while(i.hasNext()) {
        	
        	adBranch = (LocalAdBranch)i.next();
        	
        	AdBranchDetails details = new AdBranchDetails();
        	details.setBrBranchCode(adBranch.getBrBranchCode());
        	details.setBrCode(adBranch.getBrCode());
        	details.setBrName(adBranch.getBrName());

        	list.add(details);
        	
        }
        
        return list;
    	
    }

    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList getAdBrCstAll(Integer BCST_CODE, Integer AD_CMPNY) 
    	throws GlobalNoRecordFoundException{
    	
    	Debug.print("ApCustomerEntryControllerBean getBrCstAll");

    	LocalAdBranchCustomerHome adBranchCustomerHome = null;
    	LocalAdBranchHome adBranchHome = null;
    	LocalGlChartOfAccountHome glChartOfAccountHome = null;
    	
    	LocalAdBranchCustomer adBranchCustomer = null;
    	LocalAdBranch adBranch = null;
    	LocalGlChartOfAccount glChartOfAccount = null;
    	
    	Collection adBranchCustomers = null;
    	
        ArrayList branchList = new ArrayList();
        ArrayList glCoaAccntList = new ArrayList();
        
        // Initialize EJB Home

        try {
            
        	adBranchCustomerHome = (LocalAdBranchCustomerHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdBranchCustomerHome.JNDI_NAME, LocalAdBranchCustomerHome.class);
            adBranchHome = (LocalAdBranchHome)EJBHomeFactory.
            	lookUpLocalHome(LocalAdBranchHome.JNDI_NAME, LocalAdBranchHome.class);
            glChartOfAccountHome = (LocalGlChartOfAccountHome)EJBHomeFactory.
				lookUpLocalHome(LocalGlChartOfAccountHome.JNDI_NAME, LocalGlChartOfAccountHome.class);

        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {
        	
        	adBranchCustomers = adBranchCustomerHome.findBcstByCstCode(BCST_CODE, AD_CMPNY);
		
        } catch (FinderException ex) {
            
        } catch (Exception ex) {
            
            throw new EJBException(ex.getMessage());
        }
        
        if (adBranchCustomers.isEmpty()) {
        	
            throw new GlobalNoRecordFoundException();
        	
        }
        
        try {
        
	        Iterator i = adBranchCustomers.iterator();
	        
	        while(i.hasNext()) {
	        	
	        	adBranchCustomer = (LocalAdBranchCustomer)i.next();
	        	
		    	adBranch = adBranchHome.findByPrimaryKey(adBranchCustomer.getAdBranch().getBrCode());
		    	
		    	AdBranchDetails details = new AdBranchDetails(); 

		    	details.setBrBranchCode(adBranch.getBrBranchCode());
	        	details.setBrCode(adBranch.getBrCode());
	        	details.setBrName(adBranch.getBrName());
		    	
		    	// get the gl chart of account for this bcst_code
		    	
		    	ArCustomerDetails arCstDetails = new ArCustomerDetails(); 
		    	
		    	glChartOfAccount = glChartOfAccountHome.findByPrimaryKey(adBranchCustomer.getBcstGlCoaReceivableAccount());		    		
		    	arCstDetails.setCstGlCoaReceivableAccount(glChartOfAccount.getCoaCode());
		    	
		    	glChartOfAccount = glChartOfAccountHome.findByPrimaryKey(adBranchCustomer.getBcstGlCoaRevenueAccount());
		    	arCstDetails.setCstGlCoaRevenueAccount(glChartOfAccount.getCoaCode());
		    	
		    	glChartOfAccount = glChartOfAccountHome.findByPrimaryKey(adBranchCustomer.getBcstGlCoaUnEarnedInterestAccount());
		    	arCstDetails.setCstGlCoaUnEarnedInterestAccount(glChartOfAccount.getCoaCode());
		    	
		    	glChartOfAccount = glChartOfAccountHome.findByPrimaryKey(adBranchCustomer.getBcstGlCoaEarnedInterestAccount());
		    	arCstDetails.setCstGlCoaEarnedInterestAccount(glChartOfAccount.getCoaCode());
		    	
		    	
		    	glChartOfAccount = glChartOfAccountHome.findByPrimaryKey(adBranchCustomer.getBcstGlCoaUnEarnedPenaltyAccount());
		    	arCstDetails.setCstGlCoaUnEarnedPenaltyAccount(glChartOfAccount.getCoaCode());
		    	
		    	glChartOfAccount = glChartOfAccountHome.findByPrimaryKey(adBranchCustomer.getBcstGlCoaEarnedPenaltyAccount());
		    	arCstDetails.setCstGlCoaEarnedPenaltyAccount(glChartOfAccount.getCoaCode());
		    	
		    	branchList.add(details);
	            glCoaAccntList.add(arCstDetails);
	        }
	        
        } catch (FinderException ex) {
            
        } catch (Exception ex) {
            
            throw new EJBException(ex.getMessage());
        }
        
        ArrayList branchAndGlAccnts = new ArrayList();
        branchAndGlAccnts.add(branchList);
        branchAndGlAccnts.add(glCoaAccntList);
        
        return branchAndGlAccnts;
    }    

    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList getAdBrResAll(int resCode, Integer AD_CMPNY) 
        throws GlobalNoRecordFoundException {
                    
        Debug.print("AdBranchResponsibilityController getAdBrnchRspnsbltyAll");
        
        LocalAdBranchResponsibilityHome adBrResHome = null;
        LocalAdBranchResponsibility adBrRes = null;

        
        Collection adBranches = null;
        ArrayList list = new ArrayList();
        
        // Initialize EJB Home
        
        try {
            
        	adBrResHome = (LocalAdBranchResponsibilityHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdBranchResponsibilityHome.JNDI_NAME, LocalAdBranchResponsibilityHome.class);
            
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {
            
        	adBranches = adBrResHome.findByAdResponsibility(new Integer(resCode), AD_CMPNY);
            
        } catch (FinderException ex) {
            
        } catch (Exception ex) {
            throw new EJBException(ex.getMessage());
        }
        
        if (adBranches.isEmpty()) {
            throw new GlobalNoRecordFoundException();
        	
        }

    	Iterator i = adBranches.iterator();
    	
    	while ( i.hasNext() ) {
    		
    		adBrRes = (LocalAdBranchResponsibility)i.next();
    		AdBranchDetails details = new AdBranchDetails();
    		    		
    		details.setBrBranchCode(adBrRes.getAdBranch().getBrBranchCode());
    		details.setBrName(adBrRes.getAdBranch().getBrName());
    		list.add(details);
    	}
        
    	
        return list;
            
    }


	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public ArrayList getArSlpAll(Integer AD_CMPNY) {
	                
	    Debug.print("ArInvoiceEntryControllerBean getArSlpAll");
	
	    LocalArSalespersonHome arSalespersonHome = null;
	
	    ArrayList list = new ArrayList();
	    
	    // Initialize EJB Home
	
	    try {
	
	        arSalespersonHome = (LocalArSalespersonHome)EJBHomeFactory.
	            lookUpLocalHome(LocalArSalespersonHome.JNDI_NAME, LocalArSalespersonHome.class);
	
	    } catch (NamingException ex) {
	        
	        throw new EJBException(ex.getMessage());
	        
	    }
	    
	    try {
	    	        	
	        Collection arSalespersons = arSalespersonHome.findSlpAll(AD_CMPNY);
	
	        Iterator i = arSalespersons.iterator();
	               
	        while (i.hasNext()) {
	        	
	        	LocalArSalesperson arSalesperson = (LocalArSalesperson)i.next();
	
	        	list.add(arSalesperson.getSlpSalespersonCode());
	        		
	        }              
	                                                        		        		        
	        return list;
	        
	    } catch (Exception ex) {
	    	
	    	Debug.printStackTrace(ex);
	    	throw new EJBException(ex.getMessage());
	    	
	    }
	        
	}
	
	 /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
	public ArrayList getAdLvInvPriceLevelAll(Integer AD_CMPNY) {
		
		Debug.print("ArCustomerEntryControllerBean getAdLvInvPriceLevelAll");
		
		LocalAdLookUpValueHome adLookUpValueHome = null;
		
		ArrayList list = new ArrayList();
		
		//Initialize EJB Home
		
		try {
			
			adLookUpValueHome = (LocalAdLookUpValueHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdLookUpValueHome.JNDI_NAME, LocalAdLookUpValueHome.class);
			
		} catch (NamingException ex) {
			
			throw new EJBException(ex.getMessage());
			
		}
		
		try {
			
			Collection adLookUpValues = adLookUpValueHome.findByLuName("INV PRICE LEVEL", AD_CMPNY);
			
			Iterator i = adLookUpValues.iterator();
			
			while (i.hasNext()) {
				
				LocalAdLookUpValue adLookUpValue = (LocalAdLookUpValue)i.next();
				
				list.add(adLookUpValue.getLvName());
				
			}
			
			return list;
			
		} catch (Exception ex) {
			
			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());
			
		}
		
		
	}
	
	/**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList getInvLitAll(Integer AD_CMPNY) {
                    
        Debug.print("ArCustomerEntryControllerBean getInvLitAll");
        
        LocalInvLineItemTemplateHome invLineItemTemplateHome = null;
        LocalInvLineItemTemplate invLineItemTemplate = null;

        ArrayList list = new ArrayList();
        
        // Initialize EJB Home
        
        try {
            
        	invLineItemTemplateHome = (LocalInvLineItemTemplateHome)EJBHomeFactory.
                lookUpLocalHome(LocalInvLineItemTemplateHome.JNDI_NAME, LocalInvLineItemTemplateHome.class);
            
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {
            
            Collection invLineItemTemplates = invLineItemTemplateHome.findLitAll(AD_CMPNY);
	        
	        Iterator i = invLineItemTemplates.iterator();
	               
	        while (i.hasNext()) {
	        	
	        	invLineItemTemplate = (LocalInvLineItemTemplate)i.next();
	        	
	        	list.add(invLineItemTemplate.getLitName());
	        	
	        }
	        
	        return list;
          
        } catch (Exception ex) {
        	
        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());
        	
        }
            
    }
    
	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public ArrayList getAdLvArRegionAll(Integer AD_CMPNY) {
		
		Debug.print("ArCustomerEntryControllerBean getAdLvArRegionAll");
		
		LocalAdLookUpValueHome adLookUpValueHome = null;               
		
		ArrayList list = new ArrayList();
		
		// Initialize EJB Home
		
		try {
			
			adLookUpValueHome = (LocalAdLookUpValueHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdLookUpValueHome.JNDI_NAME, LocalAdLookUpValueHome.class);
			
		} catch (NamingException ex) {
			
			throw new EJBException(ex.getMessage());
			
		}        
		
		try {
			
			Collection adLookUpValues = adLookUpValueHome.findByLuName("AR REGION", AD_CMPNY);
			
			Iterator i = adLookUpValues.iterator();
			
			while (i.hasNext()) {
				
				LocalAdLookUpValue adLookUpValue = (LocalAdLookUpValue)i.next();
				
				list.add(adLookUpValue.getLvName());
				
			}
			
			return list;
			
		} catch (Exception ex) {
			
			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());
			
		}
		
	}		


    // SessionBean methods

    /**
     * @ejb:create-method view-type="remote"
     **/
    public void ejbCreate() throws CreateException {

       Debug.print("ArCustomerEntryControllerBean ejbCreate");
      
    }
}