/*
 * ArCustomerEntryControllerBean.java
 *
 * Created on October 25, 2005, 2:01 PM
 *
 * @author  Dann Ryan M. Hilario
 */

package com.ejb.txn;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;
import javax.ejb.SessionBean;
import javax.ejb.SessionContext;
import javax.naming.NamingException;

import com.ejb.ad.LocalAdBranch;
import com.ejb.ad.LocalAdBranchCustomer;
import com.ejb.ad.LocalAdBranchCustomerHome;
import com.ejb.ad.LocalAdBranchHome;
import com.ejb.ar.LocalArCustomer;
import com.ejb.ar.LocalArCustomerBalance;
import com.ejb.ar.LocalArCustomerBalanceHome;
import com.ejb.ar.LocalArCustomerHome;
import com.ejb.ar.LocalArInvoice;
import com.ejb.ar.LocalArInvoiceHome;
import com.ejb.ar.LocalArSalesperson;
import com.ejb.ar.LocalArSalesOrder;
import com.ejb.ar.LocalArSalesOrderLine;
import com.ejb.ar.LocalArSalesOrderHome;
import com.ejb.ar.LocalArSalespersonHome;
//import com.sun.org.apache.bcel.internal.generic.SALOAD;
import com.util.Debug;
import com.util.EJBCommon;
import com.util.EJBHomeFactory;

/**
 * @ejb:bean name="ArCustomerSyncControllerBean"
 *           type="Stateless"
 *           view-type="service-endpoint"
 *           
 * @wsee:port-component name="ArCustomerSync"
 * 
 * @jboss:port-component uri="omega-ejb/ArCustomerSyncWS"
 *
 * @ejb:interface service-endpoint-class="com.ejb.txn.ArCustomerSyncWS"
 * 
*/

public class ArCustomerSyncControllerBean implements SessionBean {
	
	private SessionContext ctx;
	
	
	/**
     * @ejb:interface-method
     **/
    public String[] getArCSTAreaAll(Integer AD_CMPNY) {    	
    	String[] results;
    	
    	Debug.print("ArCustomerSyncControllerBean getArCSTAreaAll");

    	LocalArCustomerHome arCustomerHome = null;
    	
    	
    	// Initialize EJB Home
        
        try {
            
        	arCustomerHome = (LocalArCustomerHome)EJBHomeFactory.
            	lookUpLocalHome(LocalArCustomerHome.JNDI_NAME, LocalArCustomerHome.class);
        
        	Collection cstArea=null;
        	try {
        		cstArea = arCustomerHome.findCstArea(AD_CMPNY);
        	}
        	catch (FinderException ex) {
        		
        	}
        	ArrayList cstAreaList = new ArrayList();
			
			Iterator i = cstArea.iterator();
			int ctr = 0;
			while (i.hasNext()) {
				
				LocalArCustomer arCst = (LocalArCustomer)i.next();
				
				if (arCst.getCstArea()==null || arCst.getCstArea().equals("")) continue;
				
				if (!cstAreaList.contains(arCst.getCstArea().toUpperCase())) {
					cstAreaList.add(arCst.getCstArea().toUpperCase());
					
				}
			}
			
			results = new String[cstAreaList.size()];

			Iterator j = cstAreaList.iterator();
			ctr=0;
			while (j.hasNext()) {
				results[ctr] = j.next().toString();
				ctr++;
			}
			
			
            
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
    	
        return results;
    }
	
	/**
     * @ejb:interface-method
     **/
    public String[] getArSoPostedAllByCstArea(String CST_AREA, Integer AD_CMPNY) {    	
    	String[] results = null;
    	
    	Debug.print("ArCustomerSyncControllerBean getArSoPostedAllByCstArea");

    	LocalArSalesOrderHome arSalesOrderHome = null;
    	LocalArInvoiceHome arInvoiceHome = null;

    	// Initialize EJB Home
        
        try {

        	arSalesOrderHome = (LocalArSalesOrderHome)EJBHomeFactory.
            	lookUpLocalHome(LocalArSalesOrderHome.JNDI_NAME, LocalArSalesOrderHome.class);
        	arInvoiceHome = (LocalArInvoiceHome)EJBHomeFactory.
                	lookUpLocalHome(LocalArInvoiceHome.JNDI_NAME, LocalArInvoiceHome.class);
        	
        	java.util.Calendar calendar = java.util.Calendar.getInstance();
        	calendar.setTime(new java.util.Date());
        	calendar.add(java.util.Calendar.MONTH, -3);
        	calendar.set(java.util.Calendar.DAY_OF_MONTH, 1);
        	String previousDate = EJBCommon.convertSQLDateToString(calendar.getTime());
        	
        	calendar = java.util.Calendar.getInstance();
        	calendar.setTime(new java.util.Date());
        	calendar.set(java.util.Calendar.DAY_OF_MONTH, calendar.getActualMaximum(java.util.Calendar.DAY_OF_MONTH));
        	String nextDate = EJBCommon.convertSQLDateToString(calendar.getTime());
        	
        	Collection soPostedColl = null;
        	try {
        		soPostedColl = arSalesOrderHome.findSoPostedAndApprovedByDateRangeAndAdCompany(EJBCommon.convertStringToSQLDate(previousDate),EJBCommon.convertStringToSQLDate(nextDate),AD_CMPNY);
        	}
        	catch (FinderException ex) {}
        	
        	ArrayList soList = new ArrayList();
        	
        	Iterator i = soPostedColl.iterator();
        	while (i.hasNext()) {
        		LocalArSalesOrder salesOrder = (LocalArSalesOrder)i.next();
        		if (salesOrder.getArCustomer().getCstArea().equals(CST_AREA)) {
        			String str="";
        			LocalArInvoice invoice=null;
        			
    				Collection arSOLineColl = salesOrder.getArSalesOrderLines();
    				Iterator j = arSOLineColl.iterator();

    				double ctr=0;
    				while (j.hasNext()) {
    					LocalArSalesOrderLine arSOLine = (LocalArSalesOrderLine)j.next();
    					if (arSOLine.getInvUnitOfMeasure().getUomName().equals("CTN")) ctr += arSOLine.getSolQuantity();
    				}
    				
        			if (salesOrder.getSoLock()==(byte)1) {
        				str += EJBCommon.convertSQLDateToString(salesOrder.getSoDateApprovedRejected()) +"$"+salesOrder.getArCustomer().getCstName()+"$"+salesOrder.getSoDocumentNumber()+"$"+"1"+"$"+ctr;
            			soList.add(str);
        			}
        			else 
        			{
        				str += EJBCommon.convertSQLDateToString(salesOrder.getSoDateApprovedRejected()) +"$"+salesOrder.getArCustomer().getCstName()+"$"+salesOrder.getSoDocumentNumber()+"$"+"0"+"$"+ctr;
        				soList.add(str);
        			}
        		}
        	}
        	
			results = new String[soList.size()];
			Iterator j = soList.iterator();
			int ctr=0;
			while (j.hasNext()) {
				results[ctr++] = j.next().toString();
			}
			
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
    	
        return results;
    }
	
    
	/**
     * @ejb:interface-method
     **/
    public int getArCustomersAllNewLength(String BR_BRNCH_CODE, Integer AD_CMPNY) {    	
    
    	Debug.print("ArCustomerSyncControllerBean getArCustomersAllNewLength");
    	
    	LocalArCustomerHome arCustomerHome = null;
    	LocalAdBranchHome adBranchHome = null;
    	
    	
    	
    	System.out.println(BR_BRNCH_CODE + " " +  AD_CMPNY);
    	
    	// Initialize EJB Home
        
        try {
            
        	arCustomerHome = (LocalArCustomerHome)EJBHomeFactory.
            	lookUpLocalHome(LocalArCustomerHome.JNDI_NAME, LocalArCustomerHome.class);
        	adBranchHome = (LocalAdBranchHome)EJBHomeFactory.
        		lookUpLocalHome(LocalAdBranchHome.JNDI_NAME, LocalAdBranchHome.class);
        
            
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {
        	
        	LocalAdBranch adBranch = adBranchHome.findByBrBranchCode(BR_BRNCH_CODE, AD_CMPNY);
        	
        	Collection arCustomers = arCustomerHome.findCstByCstNewAndUpdated(adBranch.getBrCode(), AD_CMPNY, 'N', 'N', 'N');
        	
        	System.out.println(arCustomers.size() + " size:");
        	
        	return arCustomers.size();
        	        	
        } catch (Exception ex) {
        	
        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());
        	
        }
    }
    
    /**
     * @ejb:interface-method
     **/
    public int getArCustomersAllUpdatedLength(String BR_BRNCH_CODE, Integer AD_CMPNY) {    	
    
    	Debug.print("ArCustomerSyncControllerBean getArCustomersAllUpdatedLength");

    	LocalArCustomerHome arCustomerHome = null;
    	LocalAdBranchHome adBranchHome = null;
    	
    	// Initialize EJB Home
        
        try {
            
        	arCustomerHome = (LocalArCustomerHome)EJBHomeFactory.
            	lookUpLocalHome(LocalArCustomerHome.JNDI_NAME, LocalArCustomerHome.class);
        	adBranchHome = (LocalAdBranchHome)EJBHomeFactory.
        		lookUpLocalHome(LocalAdBranchHome.JNDI_NAME, LocalAdBranchHome.class);
        	
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {
        	
        	LocalAdBranch adBranch = adBranchHome.findByBrBranchCode(BR_BRNCH_CODE, AD_CMPNY);
        	
        	Collection arCustomers = arCustomerHome.findCstByCstNewAndUpdated(adBranch.getBrCode(), AD_CMPNY, 'U','U','X');
        	
        	return arCustomers.size();
        	
        } catch (Exception ex) {
        	
        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());
        	
        }
    }
	
    /**
     * @ejb:interface-method
     **/
    public String[] getArSalespersonAll(String BR_BRNCH_CODE, Integer AD_CMPNY) {    	
    
    	Debug.print("ArCustomerSyncControllerBean getArSalespersonAll");
    	
    	LocalArSalespersonHome arSalespersonHome = null; 
    	LocalAdBranchHome adBranchHome = null;
    	
    	// Initialize EJB Home
        
        try {
            
        	arSalespersonHome = (LocalArSalespersonHome)EJBHomeFactory.
                lookUpLocalHome(LocalArSalespersonHome.JNDI_NAME, LocalArSalespersonHome.class);
        	adBranchHome = (LocalAdBranchHome)EJBHomeFactory.
    			lookUpLocalHome(LocalAdBranchHome.JNDI_NAME, LocalAdBranchHome.class);
        	
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {
        	LocalAdBranch adBranch = adBranchHome.findByBrBranchCode(BR_BRNCH_CODE, AD_CMPNY);
        	
        	Collection arSalespersons = arSalespersonHome.findSlpByBrCode(adBranch.getBrCode(), AD_CMPNY);        	
        	        	
        	String[] results = new String[arSalespersons.size()];
        	
        	Iterator i = arSalespersons.iterator();
        	int ctr = 0;
	        while (i.hasNext()) {
	        	
	        	LocalArSalesperson arSalesperson = (LocalArSalesperson)i.next();
	        	
	        	results[ctr] = salespersonRowEncode(arSalesperson);
	        	ctr++;
	        	
	        }
	        
	        return results;
        	
        } catch (Exception ex) {
        	
        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());
        	
        }
    }
    
    /**
     * @ejb:interface-method
     **/
    public String[] getArCustomerDraftBalances(String[] CST_CSTMR_CODES, Integer AD_CMPNY) {
    	
    	Debug.print("ArCustomerSyncControllerBean getArCustomerDraftBalances");
    	
    	LocalArInvoiceHome arInvoiceHome= null;
    	LocalArCustomerHome arCustomerHome = null;
    	LocalAdBranchCustomer adBranchCustomer = null;
    	LocalAdBranchHome adBranchHome = null;
    	
    	// Initialize EJB Home
        
        try {
            
        	arCustomerHome = (LocalArCustomerHome)EJBHomeFactory.
                lookUpLocalHome(LocalArCustomerHome.JNDI_NAME, LocalArCustomerHome.class);
        	adBranchHome = (LocalAdBranchHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdBranchHome.JNDI_NAME, LocalAdBranchHome.class);
        	arInvoiceHome = (LocalArInvoiceHome)EJBHomeFactory.
			lookUpLocalHome(LocalArInvoiceHome.JNDI_NAME, LocalArInvoiceHome.class);
            
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {
        	
        	String[] results = new String[CST_CSTMR_CODES.length];
        	
        	for(int i=0;i<CST_CSTMR_CODES.length;i++){
        		
        		Collection arInvoices = arInvoiceHome.findUnpostedInvByCstCustomerCode(CST_CSTMR_CODES[i],AD_CMPNY);
        		
        		double totalDraftBalance = 0;
        		Iterator iter = arInvoices.iterator();            	
    	        while (iter.hasNext()) {
    	        	
    	        	totalDraftBalance += ((LocalArInvoice)iter.next()).getInvAmountDue();
    	        	
    	        }
    	        
    	        results[i] = "" + totalDraftBalance;   	        
    	        
        	
        	}        	
        	
	        
	        return results;
        	
        } catch (Exception ex) {
        	
        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());
        	
        }
    }
    
    /**
     * @ejb:interface-method
     **/
    public String[] getArCustomersNameCodeAddressSlp(String BR_BRNCH_CODE, Integer AD_CMPNY) {    	
    
    	Debug.print("ArCustomerSyncControllerBean getArCustomersNameCodeAddressSlp");
    	
    	LocalArCustomerHome arCustomerHome = null;
    	LocalAdBranchCustomer adBranchCustomer = null;
    	LocalAdBranchHome adBranchHome = null;
    	
    	// Initialize EJB Home
        
        try {
            
        	arCustomerHome = (LocalArCustomerHome)EJBHomeFactory.
                lookUpLocalHome(LocalArCustomerHome.JNDI_NAME, LocalArCustomerHome.class);
        	adBranchHome = (LocalAdBranchHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdBranchHome.JNDI_NAME, LocalAdBranchHome.class);
            
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {
        	LocalAdBranch adBranch = adBranchHome.findByBrBranchCode(BR_BRNCH_CODE, AD_CMPNY);
        	
        	Collection arCustomers = arCustomerHome.findCstByCstNewAndUpdated(adBranch.getBrCode(), AD_CMPNY, 'N', 'N', 'N');
        	Collection arUpdatedCustomers = arCustomerHome.findCstByCstNewAndUpdated(adBranch.getBrCode(), AD_CMPNY, 'U', 'U', 'X');
        	        	
        	String[] results = new String[arCustomers.size() + arUpdatedCustomers.size()];
        	
        	Iterator i = arCustomers.iterator();
        	int ctr = 0;
	        while (i.hasNext()) {
	        	
	        	LocalArCustomer arCustomer = (LocalArCustomer)i.next();
	        	results[ctr] = customerNewRowEncode(arCustomer);
	        	ctr++;
	        	
	        }
	        
	        i = arUpdatedCustomers.iterator();
	        while (i.hasNext()) {
	        	
	        	LocalArCustomer arCustomer = (LocalArCustomer)i.next();
	        	results[ctr] = customerNewRowEncode(arCustomer);
	        	ctr++;
	        	
	        }
	        
	        return results;
        	
        } catch (Exception ex) {
        	
        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());
        	
        }    
    
    }
	/**
     * @ejb:interface-method
     **/
    public String[] getArCustomersAllNewAndUpdated(String BR_BRNCH_CODE, Integer AD_CMPNY) {    	
    
    	Debug.print("ArCustomerSyncControllerBean getArCustomersAllNewAndUpdated");
    	
    	LocalArCustomerHome arCustomerHome = null;
    	LocalAdBranchCustomer adBranchCustomer = null;
    	LocalAdBranchHome adBranchHome = null;
    	
    	// Initialize EJB Home
        
        try {
            
        	arCustomerHome = (LocalArCustomerHome)EJBHomeFactory.
                lookUpLocalHome(LocalArCustomerHome.JNDI_NAME, LocalArCustomerHome.class);
        	adBranchHome = (LocalAdBranchHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdBranchHome.JNDI_NAME, LocalAdBranchHome.class);
            
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {
        	LocalAdBranch adBranch = adBranchHome.findByBrBranchCode(BR_BRNCH_CODE, AD_CMPNY);
        	
        	Collection arCustomers = arCustomerHome.findCstByCstNewAndUpdated(adBranch.getBrCode(), AD_CMPNY, 'N', 'N', 'N');
        	Collection arUpdatedCustomers = arCustomerHome.findCstByCstNewAndUpdated(adBranch.getBrCode(), AD_CMPNY, 'U', 'U', 'X');
        	        	
        	String[] results = new String[arCustomers.size() + arUpdatedCustomers.size()];
        	
        	Iterator i = arCustomers.iterator();
        	int ctr = 0;
	        while (i.hasNext()) {
	        	
	        	LocalArCustomer arCustomer = (LocalArCustomer)i.next();
	        	results[ctr] = customerRowEncode(arCustomer);
	        	ctr++;
	        	
	        }
	        
	        i = arUpdatedCustomers.iterator();
	        while (i.hasNext()) {
	        	
	        	LocalArCustomer arCustomer = (LocalArCustomer)i.next();
	        	results[ctr] = customerRowEncode(arCustomer);
	        	ctr++;
	        	
	        }
	        
	        return results;
        	
        } catch (Exception ex) {
        	
        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());
        	
        }
    }
    
    /**
     * @ejb:interface-method
     **/
    public String[] getArCustomersAllNewAndUpdatedWithSalesperson(String BR_BRNCH_CODE, Integer AD_CMPNY) {    	
    
    	Debug.print("ArCustomerSyncControllerBean getArCustomersAllNewAndUpdatedWithSalesperson");
    	
    	LocalArCustomerHome arCustomerHome = null;
    	LocalAdBranchCustomer adBranchCustomer = null;
    	LocalAdBranchHome adBranchHome = null;
    	
    	// Initialize EJB Home
        
        try {
            
        	arCustomerHome = (LocalArCustomerHome)EJBHomeFactory.
                lookUpLocalHome(LocalArCustomerHome.JNDI_NAME, LocalArCustomerHome.class);
        	adBranchHome = (LocalAdBranchHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdBranchHome.JNDI_NAME, LocalAdBranchHome.class);
            
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {
        	LocalAdBranch adBranch = adBranchHome.findByBrBranchCode(BR_BRNCH_CODE, AD_CMPNY);
        	
        	Collection arCustomers = arCustomerHome.findCstByCstNewAndUpdated(adBranch.getBrCode(), AD_CMPNY, 'N', 'N', 'N');
        	Collection arUpdatedCustomers = arCustomerHome.findCstByCstNewAndUpdated(adBranch.getBrCode(), AD_CMPNY, 'U', 'U', 'X');
        	        	
        	String[] results = new String[arCustomers.size() + arUpdatedCustomers.size()];
        	
        	Iterator i = arCustomers.iterator();
        	int ctr = 0;
	        while (i.hasNext()) {
	        	
	        	LocalArCustomer arCustomer = (LocalArCustomer)i.next();
	        	results[ctr] = customerRowEncodeWithSalesPerson(arCustomer);
	        	ctr++;
	        	
	        }
	        
	        i = arUpdatedCustomers.iterator();
	        while (i.hasNext()) {
	        	
	        	LocalArCustomer arCustomer = (LocalArCustomer)i.next();
	        	results[ctr] = customerRowEncodeWithSalesPerson(arCustomer);
	        	ctr++;
	        	
	        }
	        
	        return results;
        	
        } catch (Exception ex) {
        	
        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());
        	
        }
    }
    
    
    /**
     * @ejb:interface-method
     **/
    public String[] getArCustomersAllNewAndUpdatedWithCustomerClass(String BR_BRNCH_CODE, Integer AD_CMPNY) {    	
    
    	Debug.print("ArCustomerSyncControllerBean getArCustomersAllNewAndUpdatedwithClass");
    	
    	LocalArCustomerHome arCustomerHome = null;
    	LocalAdBranchCustomer adBranchCustomer = null;
    	LocalAdBranchHome adBranchHome = null;
    	
    	// Initialize EJB Home
        
        try {
            
        	arCustomerHome = (LocalArCustomerHome)EJBHomeFactory.
                lookUpLocalHome(LocalArCustomerHome.JNDI_NAME, LocalArCustomerHome.class);
        	adBranchHome = (LocalAdBranchHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdBranchHome.JNDI_NAME, LocalAdBranchHome.class);
            
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {
        	LocalAdBranch adBranch = adBranchHome.findByBrBranchCode(BR_BRNCH_CODE, AD_CMPNY);
        	
        	Collection arCustomers = arCustomerHome.findCstByCstNewAndUpdated(adBranch.getBrCode(), AD_CMPNY, 'N', 'N', 'N');
        	Collection arUpdatedCustomers = arCustomerHome.findCstByCstNewAndUpdated(adBranch.getBrCode(), AD_CMPNY, 'U', 'U', 'X');
        	        	
        	String[] results = new String[arCustomers.size() + arUpdatedCustomers.size()];
        	
        	Iterator i = arCustomers.iterator();
        	int ctr = 0;
	        while (i.hasNext()) {
	        	
	        	LocalArCustomer arCustomer = (LocalArCustomer)i.next();
	        	results[ctr] = customerRowEncodewithClass(arCustomer);
	        	ctr++;
	        	
	        }
	        
	        i = arUpdatedCustomers.iterator();
	        while (i.hasNext()) {
	        	
	        	LocalArCustomer arCustomer = (LocalArCustomer)i.next();
	        	results[ctr] = customerRowEncodewithClass(arCustomer);
	        	ctr++;
	        	
	        }
	        
	        return results;
        	
        } catch (Exception ex) {
        	
        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());
        	
        }
    }
    
    
    
    
    /**
     * @ejb:interface-method
     **/
    public String getArCustomersBalanceAllDownloaded(String BR_BRNCH_CODE, Integer AD_CMPNY) {    	
    
    	Debug.print("ArCustomerSyncControllerBean getArCustomersBalanceAllDownloaded");
    	char separator = EJBCommon.SEPARATOR;
    	StringBuffer CustomerBalances = new StringBuffer();
    	LocalArCustomerHome arCustomerHome = null;
    	LocalAdBranchCustomer adBranchCustomer = null;
    	LocalAdBranchHome adBranchHome = null;
    	LocalArCustomerBalanceHome arCustomerBalanceHome = null;
    	
    	// Initialize EJB Home
        
        try {
            
        	arCustomerHome = (LocalArCustomerHome)EJBHomeFactory.
                lookUpLocalHome(LocalArCustomerHome.JNDI_NAME, LocalArCustomerHome.class);
        	adBranchHome = (LocalAdBranchHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdBranchHome.JNDI_NAME, LocalAdBranchHome.class);
        	arCustomerBalanceHome = (LocalArCustomerBalanceHome)EJBHomeFactory.
            	lookUpLocalHome(LocalArCustomerBalanceHome.JNDI_NAME, LocalArCustomerBalanceHome.class);
            
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {
        	LocalAdBranch adBranch = adBranchHome.findByBrBranchCode(BR_BRNCH_CODE, AD_CMPNY);
        	
        	Collection arCustomers = arCustomerHome.findCstByCstNewAndUpdated(adBranch.getBrCode(), AD_CMPNY, 'D', 'D', 'D');        	       	
        	        	
        	Iterator i = arCustomers.iterator();
        	int ctr = 0;
	        while (i.hasNext()) {
	        	
	        	LocalArCustomer arCustomer = (LocalArCustomer)i.next();
	        	double currentBalance = 0;
	        	// Current Balance    	 
	        		
    			Collection arCustomerBalances = arCustomerBalanceHome.findByBeforeOrEqualCbDateAndCstCode(EJBCommon.getGcCurrentDateWoTime().getTime(),arCustomer.getCstCode(),arCustomer.getCstAdCompany());
    			
    			if (!arCustomerBalances.isEmpty()) {
    				ArrayList arCustomerBalanceList = new ArrayList(arCustomerBalances);
    				LocalArCustomerBalance arCustomerBalance= (LocalArCustomerBalance)arCustomerBalanceList.get(arCustomerBalanceList.size() - 1);
    				currentBalance=arCustomerBalance.getCbBalance();
    			}
	        	CustomerBalances.append(separator);
	        	CustomerBalances.append(arCustomer.getCstCode().toString());
	        	CustomerBalances.append(separator);
	        	CustomerBalances.append(currentBalance);
	        	ctr++;	        	
	        }
	        CustomerBalances.append(separator);
	        return CustomerBalances.toString();
        	
        } catch (Exception ex) {
        	
        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());
        	
        }
    }
    /**
     * @ejb:interface-method
     **/
    public int setArCustomersAllNewAndUpdatedSuccessConfirmation(String BR_BRNCH_CODE, Integer AD_CMPNY) {    	
    
    	Debug.print("ArCustomerSyncControllerBean setArCustomersAllNewAndUpdatedSuccessConfirmation");
    	
    	LocalAdBranchCustomerHome adBranchCustomerHome = null;
    	LocalAdBranchCustomer adBranchCustomer = null;
    	LocalAdBranchHome adBranchHome = null;
    	
    	// Initialize EJB Home
        
        try {
            
        	adBranchCustomerHome = (LocalAdBranchCustomerHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdBranchCustomerHome.JNDI_NAME, LocalAdBranchCustomerHome.class);
        	adBranchHome = (LocalAdBranchHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdBranchHome.JNDI_NAME, LocalAdBranchHome.class);

        	
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {
        	LocalAdBranch adBranch = adBranchHome.findByBrBranchCode(BR_BRNCH_CODE, AD_CMPNY);
        	
        	Collection adBranchCustomers = adBranchCustomerHome.findCstByCstNewAndUpdated(adBranch.getBrCode(), AD_CMPNY, 'N', 'U', 'X');
        	
        	Iterator i = adBranchCustomers.iterator();        	
	        while (i.hasNext()) {
	        	
	        	adBranchCustomer = (LocalAdBranchCustomer)i.next();	        	
	        	adBranchCustomer.setBcstCustomerDownloadStatus('D');
	        	
	        }
        	
        } catch (Exception ex) {
        	
        	ctx.setRollbackOnly();
        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());
        	
        }
        
        return 0;
        
    }
    
    private String customerNewRowEncode(LocalArCustomer arCustomer) {
    	
    	char separator = EJBCommon.SEPARATOR;
    	StringBuffer tempResult = new StringBuffer();
    	String encodedResult = new String();

    	// Start separator
    	tempResult.append(separator);

    	// Primary Key
    	tempResult.append(arCustomer.getCstCode());
    	tempResult.append(separator);
    	
    	// Customer Code
    	tempResult.append(arCustomer.getCstCustomerCode());
    	tempResult.append(separator);
    	
    	// Name
    	tempResult.append(arCustomer.getCstName());
    	tempResult.append(separator);
    	
    	// Address
    	if (arCustomer.getCstAddress().length() < 1) {
    		
    		tempResult.append("not specified");
    		tempResult.append(separator);
    		
    	} else {

    		tempResult.append(arCustomer.getCstAddress());
    		tempResult.append(separator);
    	
    	}
    	//Credit Limit
    	
    	tempResult.append(arCustomer.getCstCreditLimit());
    	tempResult.append(separator);
    	
    	
    	// Sales Person
    	try{
    	tempResult.append(arCustomer.getArSalesperson().getSlpCode());
    	tempResult.append(separator);
    	}
    	catch(Exception ex)
    	{
    		tempResult.append(" ");
        	tempResult.append(separator);
    	}
    	
    	// remove unwanted chars from encodedResult;
    	encodedResult = tempResult.toString();
    	encodedResult = encodedResult.replace("\"", " ");
    	encodedResult = encodedResult.replace("'", " ");
    	encodedResult = encodedResult.replace(";", " ");
    	encodedResult = encodedResult.replace("\\", " ");
    	encodedResult = encodedResult.replace("|", " ");    	

    	return encodedResult;
    	
    }
    
    private String customerRowEncode(LocalArCustomer arCustomer) {
    	
    	LocalArSalespersonHome arSalespersonHome = null; 
    	
    	try{	
    	arSalespersonHome = (LocalArSalespersonHome)EJBHomeFactory.
        lookUpLocalHome(LocalArSalespersonHome.JNDI_NAME, LocalArSalespersonHome.class);
    	
    	} catch (NamingException ex) {
        
        throw new EJBException(ex.getMessage());
        
    	}
    	
    	char separator = EJBCommon.SEPARATOR;
		StringBuffer tempResult = new StringBuffer();
		String encodedResult = new String();
	
		// Start separator
		tempResult.append(separator);
	
		// Primary Key
		tempResult.append(arCustomer.getCstCode());
		tempResult.append(separator);
		
		// Customer Code
		tempResult.append(arCustomer.getCstCustomerCode());
		tempResult.append(separator);
		
		// Name
		tempResult.append(arCustomer.getCstName());
		tempResult.append(separator);
		
		// Credit Limit
		tempResult.append(Double.toString(arCustomer.getCstCreditLimit()));
		tempResult.append(separator);
		
		// Current Balance    	 
		try{
			
			LocalArCustomerBalanceHome arCustomerBalanceHome = (LocalArCustomerBalanceHome)EJBHomeFactory.
	        lookUpLocalHome(LocalArCustomerBalanceHome.JNDI_NAME, LocalArCustomerBalanceHome.class);
			
			try{
				
				Collection arCustomerBalances = arCustomerBalanceHome.findByBeforeOrEqualCbDateAndCstCode(EJBCommon.getGcCurrentDateWoTime().getTime(),arCustomer.getCstCode(),arCustomer.getCstAdCompany());
				LocalArCustomerBalance arCustomerBalance=null;
				Iterator i = arCustomerBalances.iterator();        	
				while (i.hasNext()){    	        	
		        	arCustomerBalance = (LocalArCustomerBalance)i.next();;
		        }
				if(arCustomerBalance!=null){
					tempResult.append(Double.toString(arCustomerBalance.getCbBalance()));
				}
				else{
					tempResult.append("0");
				}    	        
	        	tempResult.append(separator);
				
			} catch(Exception ex){
	    		
	    		Debug.printStackTrace(ex);
	        	throw new EJBException(ex.getMessage());
	    		
	    	}
			
			
		} catch(Exception ex){
			
			Debug.printStackTrace(ex);
	    	throw new EJBException(ex.getMessage());
			
		}
		
		// Area / State Province
		if (arCustomer.getCstStateProvince().length() < 1) {
			
			tempResult.append("not specified");
			tempResult.append(separator);
			
		} else {
			
			tempResult.append(arCustomer.getCstStateProvince());
			tempResult.append(separator);
			
		}
		
		// Contact
		if (arCustomer.getCstContact().length() < 1) {
			
			tempResult.append("not specified");
			tempResult.append(separator);
			
		} else {
	
			tempResult.append(arCustomer.getCstContact());
			tempResult.append(separator);
			
		}
		
		// Phone
		if (arCustomer.getCstPhone().length() < 1) {
			
			tempResult.append("0");
			tempResult.append(separator);
			
		} else {
			
			tempResult.append(arCustomer.getCstPhone());
			tempResult.append(separator);
		
		}
		
		// Fax
		if (arCustomer.getCstFax().length() < 1) {
			
			tempResult.append("0");
			tempResult.append(separator);
			
		} else {
			
			tempResult.append(arCustomer.getCstFax());
			tempResult.append(separator);
		
		}
		// Email
		if (arCustomer.getCstEmail().length() < 1) {
			
			tempResult.append("not specified");
			tempResult.append(separator);
			
		} else {
	
			tempResult.append(arCustomer.getCstEmail());
			tempResult.append(separator);
			
		}
		
		// Address
		if (arCustomer.getCstAddress().length() < 1) {
			
			tempResult.append("not specified");
			tempResult.append(separator);
			
		} else {
	
			tempResult.append(arCustomer.getCstAddress());
			tempResult.append(separator);
		
		}
		
		// Payment Term
		tempResult.append(arCustomer.getAdPaymentTerm().getPytName());
		tempResult.append(separator);
		
		// Deal
		if (arCustomer.getCstDealPrice()==null || arCustomer.getCstDealPrice().length() < 1) {
			
			tempResult.append("not specified");
			tempResult.append(separator);
			
		} else {
	
			tempResult.append(arCustomer.getCstDealPrice());
			tempResult.append(separator);
		
		}
		
		// ENABLE
		tempResult.append(arCustomer.getCstEnable());
		tempResult.append(separator);
		
		
		//sales person 
		if (arCustomer.getArSalesperson() == null){
		
			tempResult.append("null");
			tempResult.append(separator);
		
		}else{
		 if (arCustomer.getArSalesperson().getSlpName() != null){	
			tempResult.append(arCustomer.getArSalesperson().getSlpName());
			tempResult.append(separator);
		 }else
		  {
				tempResult.append("null");
				tempResult.append(separator);
		  }		 
		}
		
		
		//Tin 8/11/2011 jfb
		if (arCustomer.getCstTin().length() < 1) {
			
			tempResult.append("not specified");
			tempResult.append(separator);
			
		} else {
			
			tempResult.append(arCustomer.getCstTin());
			tempResult.append(separator);
			
		}
		
		
		//ShiptoAddress 8/11/2011 jfb
		if (arCustomer.getCstShipToAddress()!=null  ){
			if (arCustomer.getCstShipToAddress().length() < 1){
				
				tempResult.append("not specified");
				tempResult.append(separator);
				
			} else {
				
				tempResult.append(arCustomer.getCstShipToAddress());
				tempResult.append(separator);
				
			}
		}else{
			tempResult.append(arCustomer.getCstShipToAddress());
			tempResult.append(separator);
		}
		
	
		
		
		//sales person 2
		String slName = null;
		Integer slId = null;
		
		if (arCustomer.getCstArSalesperson2() != null){
			try{
				LocalArSalesperson arSalesperson = arSalespersonHome.findByPrimaryKey(arCustomer.getCstArSalesperson2());
	
				slName = arSalesperson.getSlpName();
				slId = arSalesperson.getSlpCode();
			} catch (Exception ex) {
			
			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());
			
			}
		}
		/*
		//start
		if (slName == null) {
			tempResult.append("null");
			tempResult.append(separator);			
		}else{
		tempResult.append(slName);
		tempResult.append(separator);
		}
		
		//end
		
		//salesman id
		
		if (arCustomer.getArSalesperson() == null){
			
			tempResult.append("null");
			tempResult.append(separator);
		
		}else{
		 if (arCustomer.getArSalesperson().getSlpCode() != null){	
			tempResult.append(arCustomer.getArSalesperson().getSlpCode());
			tempResult.append(separator);
		 }else
		  {
				tempResult.append("null");
				tempResult.append(separator);
		  }		 
		}
		
		
		//salesman2 id
		
		if (slName == null) {
			tempResult.append("null");
			tempResult.append(separator);			
		}else{
		tempResult.append(slId);
		tempResult.append(separator);
		}
		*/
		// remove unwanted chars from encodedResult;
		encodedResult = tempResult.toString();
		encodedResult = encodedResult.replace("\"", " ");
		encodedResult = encodedResult.replace("'", " ");
		encodedResult = encodedResult.replace(";", " ");
		encodedResult = encodedResult.replace("\\", " ");
		encodedResult = encodedResult.replace("|", " ");
		
		return encodedResult;
		
	}
    
 private String customerRowEncodeWithSalesPerson(LocalArCustomer arCustomer) {
    	
    	LocalArSalespersonHome arSalespersonHome = null; 
    	
    	try{	
    	arSalespersonHome = (LocalArSalespersonHome)EJBHomeFactory.
        lookUpLocalHome(LocalArSalespersonHome.JNDI_NAME, LocalArSalespersonHome.class);
    	
    	} catch (NamingException ex) {
    		/*arSalespersonHome=null;
    		System.out.println("ERROR");*/
    		
    		Debug.printStackTrace(ex);
	    	throw new EJBException(ex.getMessage());
        
    	}
    	
    	char separator = EJBCommon.SEPARATOR;
		StringBuffer tempResult = new StringBuffer();
		String encodedResult = new String();
	
		// Start separator
		tempResult.append(separator);
	
		// Primary Key
		tempResult.append(arCustomer.getCstCode());
		tempResult.append(separator);
		
		// Customer Code
		tempResult.append(arCustomer.getCstCustomerCode());
		tempResult.append(separator);
		
		// Name
		tempResult.append(arCustomer.getCstName());
		tempResult.append(separator);
		
		// Credit Limit
		tempResult.append(Double.toString(arCustomer.getCstCreditLimit()));
		tempResult.append(separator);
		
		// Current Balance    	 
		try{
			
			LocalArCustomerBalanceHome arCustomerBalanceHome = (LocalArCustomerBalanceHome)EJBHomeFactory.
	        lookUpLocalHome(LocalArCustomerBalanceHome.JNDI_NAME, LocalArCustomerBalanceHome.class);
			
			try{
				
				Collection arCustomerBalances = arCustomerBalanceHome.findByBeforeOrEqualCbDateAndCstCode(EJBCommon.getGcCurrentDateWoTime().getTime(),arCustomer.getCstCode(),arCustomer.getCstAdCompany());
				LocalArCustomerBalance arCustomerBalance=null;
				Iterator i = arCustomerBalances.iterator();        	
				while (i.hasNext()){    	        	
		        	arCustomerBalance = (LocalArCustomerBalance)i.next();;
		        }
				if(arCustomerBalance!=null){
					tempResult.append(Double.toString(arCustomerBalance.getCbBalance()));
				}
				else{
					tempResult.append("0");
				}    	        
	        	tempResult.append(separator);
				
			} catch(Exception ex){
	    		
	    		Debug.printStackTrace(ex);
	        	throw new EJBException(ex.getMessage());
	    		
	    	}
			
			
		} catch(Exception ex){
			
			Debug.printStackTrace(ex);
	    	throw new EJBException(ex.getMessage());
			
		}
		
		// Area / State Province
		if (arCustomer.getCstStateProvince().length() < 1) {
			
			tempResult.append("not specified");
			tempResult.append(separator);
			
		} else {
			
			tempResult.append(arCustomer.getCstStateProvince());
			tempResult.append(separator);
			
		}
		
		// Contact
		if (arCustomer.getCstContact().length() < 1) {
			
			tempResult.append("not specified");
			tempResult.append(separator);
			
		} else {
	
			tempResult.append(arCustomer.getCstContact());
			tempResult.append(separator);
			
		}
		
		// Phone
		if (arCustomer.getCstPhone().length() < 1) {
			
			tempResult.append("0");
			tempResult.append(separator);
			
		} else {
			
			tempResult.append(arCustomer.getCstPhone());
			tempResult.append(separator);
		
		}
		
		// Fax
		if (arCustomer.getCstFax().length() < 1) {
			
			tempResult.append("0");
			tempResult.append(separator);
			
		} else {
			
			tempResult.append(arCustomer.getCstFax());
			tempResult.append(separator);
		
		}
		// Email
		if (arCustomer.getCstEmail().length() < 1) {
			
			tempResult.append("not specified");
			tempResult.append(separator);
			
		} else {
	
			tempResult.append(arCustomer.getCstEmail());
			tempResult.append(separator);
			
		}
		
		// Address
		if (arCustomer.getCstAddress().length() < 1) {
			
			tempResult.append("not specified");
			tempResult.append(separator);
			
		} else {
	
			tempResult.append(arCustomer.getCstAddress());
			tempResult.append(separator);
		
		}
		
		// Payment Term
		tempResult.append(arCustomer.getAdPaymentTerm().getPytName());
		tempResult.append(separator);
		
		// Deal
		if (arCustomer.getCstDealPrice()==null || arCustomer.getCstDealPrice().length() < 1) {
			
			tempResult.append("not specified");
			tempResult.append(separator);
			
		} else {
	
			tempResult.append(arCustomer.getCstDealPrice());
			tempResult.append(separator);
		
		}
		
		//sales person 
		if (arCustomer.getArSalesperson() == null){
		
			tempResult.append("null");
			tempResult.append(separator);
		
		}else{
		 if (arCustomer.getArSalesperson().getSlpName() != null){	
			tempResult.append(arCustomer.getArSalesperson().getSlpName());
			tempResult.append(separator);
		 }else
		  {
				tempResult.append("null");
				tempResult.append(separator);
		  }		 
		}
		
		//sales person 2
		String slName = null;
		Integer slId = null;
		
		if (arCustomer.getCstArSalesperson2() != null){
			try{
				LocalArSalesperson arSalesperson = arSalespersonHome.findByPrimaryKey(arCustomer.getCstArSalesperson2());
	
				slName = arSalesperson.getSlpName();
				slId = arSalesperson.getSlpCode();
			} catch (Exception ex) {
			
			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());
			
			}
		}
		
		//start
		if (slName == null) {
			tempResult.append("null");
			tempResult.append(separator);			
		}else{
		tempResult.append(slName);
		tempResult.append(separator);
		}
		
		//end
		
		//salesman id
		
		if (arCustomer.getArSalesperson() == null){
			
			tempResult.append("null");
			tempResult.append(separator);
		
		}else{
		 if (arCustomer.getArSalesperson().getSlpCode() != null){	
			tempResult.append(arCustomer.getArSalesperson().getSlpCode());
			tempResult.append(separator);
		 }else
		  {
				tempResult.append("null");
				tempResult.append(separator);
		  }		 
		}
		
		
		//salesman2 id
		
		if (slName == null) {
			tempResult.append("null");
			tempResult.append(separator);			
		}else{
		tempResult.append(slId);
		tempResult.append(separator);
		}
		
		// remove unwanted chars from encodedResult;
		encodedResult = tempResult.toString();
		encodedResult = encodedResult.replace("\"", " ");
		encodedResult = encodedResult.replace("'", " ");
		encodedResult = encodedResult.replace(";", " ");
		encodedResult = encodedResult.replace("\\", " ");
		encodedResult = encodedResult.replace("|", " ");
		
		return encodedResult;
		
	}
    

private String customerRowEncodewithClass(LocalArCustomer arCustomer) {
    	
    	char separator = EJBCommon.SEPARATOR;
    	StringBuffer tempResult = new StringBuffer();
    	String encodedResult = new String();

    	// Start separator
    	tempResult.append(separator);

    	// Primary Key
    	tempResult.append(arCustomer.getCstCode());
    	tempResult.append(separator);
    	
    	// Customer Code
    	tempResult.append(arCustomer.getCstCustomerCode());
    	tempResult.append(separator);
    	
    	// Name
    	tempResult.append(arCustomer.getCstName());
    	tempResult.append(separator);
    	
    	// Credit Limit
    	tempResult.append(Double.toString(arCustomer.getCstCreditLimit()));
    	tempResult.append(separator);
    	
    	// Current Balance    	 
    	try{
    		
    		LocalArCustomerBalanceHome arCustomerBalanceHome = (LocalArCustomerBalanceHome)EJBHomeFactory.
            lookUpLocalHome(LocalArCustomerBalanceHome.JNDI_NAME, LocalArCustomerBalanceHome.class);
    		
    		try{
    			
    			Collection arCustomerBalances = arCustomerBalanceHome.findByBeforeOrEqualCbDateAndCstCode(EJBCommon.getGcCurrentDateWoTime().getTime(),arCustomer.getCstCode(),arCustomer.getCstAdCompany());
    			LocalArCustomerBalance arCustomerBalance=null;
    			Iterator i = arCustomerBalances.iterator();        	
    			while (i.hasNext()){    	        	
    	        	arCustomerBalance = (LocalArCustomerBalance)i.next();;
    	        }
    			if(arCustomerBalance!=null){
    				tempResult.append(Double.toString(arCustomerBalance.getCbBalance()));
    			}
    			else{
    				tempResult.append("0");
    			}    	        
	        	tempResult.append(separator);
    			
    		} catch(Exception ex){
        		
        		Debug.printStackTrace(ex);
            	throw new EJBException(ex.getMessage());
        		
        	}
    		
    		
    	} catch(Exception ex){
    		
    		Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());
    		
    	}
    	
    	// Area / State Province
    	if (arCustomer.getCstStateProvince().length() < 1) {
    		
    		tempResult.append("not specified");
    		tempResult.append(separator);
    		
    	} else {
    		
    		tempResult.append(arCustomer.getCstStateProvince());
    		tempResult.append(separator);
    		
    	}
    	
    	// Contact
    	if (arCustomer.getCstContact().length() < 1) {
    		
    		tempResult.append("not specified");
    		tempResult.append(separator);
    		
    	} else {

    		tempResult.append(arCustomer.getCstContact());
    		tempResult.append(separator);
    		
    	}
    	
    	// Phone
    	if (arCustomer.getCstPhone().length() < 1) {
    		
    		tempResult.append("0");
    		tempResult.append(separator);
    		
    	} else {
    		
    		tempResult.append(arCustomer.getCstPhone());
    		tempResult.append(separator);
    	
    	}
    	
    	// Fax
    	if (arCustomer.getCstFax().length() < 1) {
    		
    		tempResult.append("0");
    		tempResult.append(separator);
    		
    	} else {
    		
    		tempResult.append(arCustomer.getCstFax());
    		tempResult.append(separator);
    	
    	}
    	// Email
    	if (arCustomer.getCstEmail().length() < 1) {
    		
    		tempResult.append("not specified");
    		tempResult.append(separator);
    		
    	} else {

    		tempResult.append(arCustomer.getCstEmail());
    		tempResult.append(separator);
    		
    	}
    	
    	// Address
    	if (arCustomer.getCstAddress().length() < 1) {
    		
    		tempResult.append("not specified");
    		tempResult.append(separator);
    		
    	} else {

    		tempResult.append(arCustomer.getCstAddress());
    		tempResult.append(separator);
    	
    	}
    	 
    	
    	// Payment Term
    	tempResult.append(arCustomer.getAdPaymentTerm().getPytName());
    	tempResult.append(separator);
    	
    	// Deal
    	if (arCustomer.getCstDealPrice()==null || arCustomer.getCstDealPrice().length() < 1) {
    		
    		tempResult.append("not specified");
    		tempResult.append(separator);
    		
    	} else {

    		tempResult.append(arCustomer.getCstDealPrice());
    		tempResult.append(separator);
    	
    	}
    	
    	// Class
    	if (arCustomer.getArCustomerClass().getCcName()==null)  {
    		tempResult.append("null Class");
    		tempResult.append(separator);
    	}else {
    	tempResult.append(arCustomer.getArCustomerClass().getCcName());
    	tempResult.append(separator);
    	System.out.print("Class :" + arCustomer.getArCustomerClass().getCcName());
    	}
    	
    	// remove unwanted chars from encodedResult;
    	encodedResult = tempResult.toString();
    	encodedResult = encodedResult.replace("\"", " ");
    	encodedResult = encodedResult.replace("'", " ");
    	encodedResult = encodedResult.replace(";", " ");
    	encodedResult = encodedResult.replace("\\", " ");
    	encodedResult = encodedResult.replace("|", " ");
    	
    	return encodedResult;
    	
    }
    
    private String salespersonRowEncode(LocalArSalesperson arSalesPerson) {
    	
    	char separator = EJBCommon.SEPARATOR;
    	StringBuffer tempResult = new StringBuffer();
    	String encodedResult = new String();

    	// Start separator
    	tempResult.append(separator);

    	// Primary Key
    	tempResult.append(arSalesPerson.getSlpCode());
    	tempResult.append(separator);
    	
    	// Salesperson Code
    	tempResult.append(arSalesPerson.getSlpSalespersonCode());
    	tempResult.append(separator);
    	
    	// Salesperson Name
    	tempResult.append(arSalesPerson.getSlpName());
    	tempResult.append(separator);
    	
    	// Address
    	if (arSalesPerson.getSlpAddress().length() < 1) {
    		
    		tempResult.append("not specified");
    		tempResult.append(separator);
    		
    	} else {

    		tempResult.append(arSalesPerson.getSlpAddress());
    		tempResult.append(separator);
    	
    	}
    	
    	
    	// remove unwanted chars from encodedResult;
    	encodedResult = tempResult.toString();
    	encodedResult = encodedResult.replace("\"", " ");
    	encodedResult = encodedResult.replace("'", " ");
    	encodedResult = encodedResult.replace(";", " ");
    	encodedResult = encodedResult.replace("\\", " ");
    	encodedResult = encodedResult.replace("|", " ");
    	
    	return encodedResult;
    	
    }
    
    public void ejbCreate() throws CreateException {

       Debug.print("ArCustomerEntryControllerBean ejbCreate");
      
    }
    
    public void ejbRemove() {};

    public void ejbActivate() {}
    public void ejbPassivate() {}

    public void setSessionContext(SessionContext ctx) {}

}