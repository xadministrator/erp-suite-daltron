package com.ejb.txn;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;
import javax.naming.NamingException;

import com.ejb.ad.LocalAdBankAccount;
import com.ejb.ad.LocalAdBankAccountHome;
import com.ejb.ar.LocalArCustomerType;
import com.ejb.ar.LocalArCustomerTypeHome;
import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.exception.GlobalRecordAlreadyAssignedException;
import com.ejb.exception.GlobalRecordAlreadyDeletedException;
import com.ejb.exception.GlobalRecordAlreadyExistException;
import com.util.AbstractSessionBean;
import com.util.ArModCustomerTypeDetails;
import com.util.Debug;
import com.util.EJBHomeFactory;

/**
 * @ejb:bean name="ArCustomerTypeControllerEJB"
 *           display-name="Used for entering customer types"
 *           type="Stateless"
 *           view-type="remote"
 *           jndi-name="ejb/ArCustomerTypeControllerEJB"
 *
 * @ejb:interface remote-class="com.ejb.txn.ArCustomerTypeController"
 *                extends="javax.ejb.EJBObject"
 *
 * @ejb:home remote-class="com.ejb.txn.ArCustomerTypeControllerHome"
 *           extends="javax.ejb.EJBHome"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="aruser"
 *                        role-link="aruserlink"
 *
 * @ejb:permission role-name="aruser"
 * 
*/

public class ArCustomerTypeControllerBean extends AbstractSessionBean {

    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList getArCtAll(Integer AD_CMPNY) 
        throws GlobalNoRecordFoundException {
                    
        Debug.print("ArCustomerTypeControllerBean getArCtAll");

        LocalArCustomerTypeHome arCustomerTypeHome = null;
        LocalAdBankAccountHome adBankAccountHome = null;

        ArrayList list = new ArrayList();
        
        // Initialize EJB Home

        try {

            arCustomerTypeHome = (LocalArCustomerTypeHome)EJBHomeFactory.
                lookUpLocalHome(LocalArCustomerTypeHome.JNDI_NAME, LocalArCustomerTypeHome.class);
            adBankAccountHome = (LocalAdBankAccountHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdBankAccountHome.JNDI_NAME, LocalAdBankAccountHome.class);                

        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {
        	        	
	        Collection arCustomerTypes = arCustomerTypeHome.findCtAll(AD_CMPNY);
	
	        if (arCustomerTypes.isEmpty()) {
	
	            throw new GlobalNoRecordFoundException();
	        	
	        }
	            
	        Iterator i = arCustomerTypes.iterator();
	               
	        while (i.hasNext()) {
	        	
	        	LocalArCustomerType arCustomerType = (LocalArCustomerType)i.next();
	        	
	        	ArModCustomerTypeDetails mdetails = new ArModCustomerTypeDetails();
	        		mdetails.setCtCode(arCustomerType.getCtCode());        		
	                mdetails.setCtName(arCustomerType.getCtName());                
	                mdetails.setCtDescription(arCustomerType.getCtDescription());
                    mdetails.setCtEnable(arCustomerType.getCtEnable());
                    mdetails.setCtBaName(arCustomerType.getAdBankAccount().getBaName());                                        
	                	                	                
	        		list.add(mdetails);
	        		
	        }              
	                                                        		        		        
	        return list;
	        
        } catch (GlobalNoRecordFoundException ex) {
        	
        	throw ex;
        	        	
        } catch (Exception ex) {
        	
        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());
        	
        }
            
    }
    
    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList getAdBaAll(Integer AD_BRNCH, Integer AD_CMPNY) {
                    
        Debug.print("ArCustomerTypeControllerBean getAdBaAll");
        
        LocalAdBankAccountHome adBankAccountHome = null;
                
        LocalAdBankAccount adBankAccount = null;
        
        Collection adBankAccounts = null;
        
        ArrayList list = new ArrayList();
        
        // Initialize EJB Home
        
        try {
            
            adBankAccountHome = (LocalAdBankAccountHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdBankAccountHome.JNDI_NAME, LocalAdBankAccountHome.class);
            
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {
            
            adBankAccounts = adBankAccountHome.findEnabledBaAll(AD_BRNCH, AD_CMPNY);
            
        } catch (FinderException ex) {
            
        } catch (Exception ex) {
            
            throw new EJBException(ex.getMessage());
        }
        
        if (adBankAccounts.isEmpty()) {
        	
            return null;
        	
        }
        
        Iterator i = adBankAccounts.iterator();
               
        while (i.hasNext()) {
        	
            adBankAccount = (LocalAdBankAccount)i.next();
        	
            list.add(adBankAccount.getBaName());
        	
        }
        
        return list;
            
    }  
    
    /**
     * @ejb:interface-method view-type="remote"
     **/    
    public void addArCtEntry(com.util.ArCustomerTypeDetails details, String CT_BA_NM, Integer AD_CMPNY) 
        throws GlobalRecordAlreadyExistException {
                    
        Debug.print("ArCustomerTypeControllerBean addArCtEntry");
        
        LocalArCustomerTypeHome arCustomerTypeHome = null;
        LocalAdBankAccountHome adBankAccountHome = null;
            
        ArrayList list = new ArrayList();
        
        // Initialize EJB Home
        
        try {
            
            arCustomerTypeHome = (LocalArCustomerTypeHome)EJBHomeFactory.
                lookUpLocalHome(LocalArCustomerTypeHome.JNDI_NAME, LocalArCustomerTypeHome.class);
            adBankAccountHome = (LocalAdBankAccountHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdBankAccountHome.JNDI_NAME, LocalAdBankAccountHome.class);                

        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {
        	
            LocalArCustomerType arCustomerType = null;
			LocalAdBankAccount adBankAccount = null;
        
	        try { 
	            
	            arCustomerType = arCustomerTypeHome.findByCtName(details.getCtName(), AD_CMPNY);
	            
	            throw new GlobalRecordAlreadyExistException();
	            
	        } catch (FinderException ex) {
	         	
	        	 	
	        }
	         
	    	// create new supplier class
	    				    	        	        	    	
	    	arCustomerType = arCustomerTypeHome.create(details.getCtName(),
	    	        details.getCtDescription(), details.getCtEnable(), AD_CMPNY);

			adBankAccount = adBankAccountHome.findByBaName(CT_BA_NM, AD_CMPNY);
			adBankAccount.addArCustomerType(arCustomerType);
	        
        } catch (GlobalRecordAlreadyExistException ex) {
        	
        	getSessionContext().setRollbackOnly();
        	throw ex;
        	
        } catch (Exception ex) {
        	
        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());
        	
        }
            
    }    
    
    /**
     * @ejb:interface-method view-type="remote"
     **/
    public void updateArCtEntry(com.util.ArCustomerTypeDetails details, String CT_BA_NM, Integer AD_CMPNY) 
        throws GlobalRecordAlreadyExistException {
                    
        Debug.print("ArCustomerTypeControllerBean updateApTcEntry");
        
        LocalArCustomerTypeHome arCustomerTypeHome = null;
		LocalAdBankAccountHome adBankAccountHome = null;
        
        LocalArCustomerType arCustomerType = null;
        LocalAdBankAccount adBankAccount = null;
                
        ArrayList list = new ArrayList();
        
        // Initialize EJB Home
        
        try {

            arCustomerTypeHome = (LocalArCustomerTypeHome)EJBHomeFactory.
                lookUpLocalHome(LocalArCustomerTypeHome.JNDI_NAME, LocalArCustomerTypeHome.class);
            adBankAccountHome = (LocalAdBankAccountHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdBankAccountHome.JNDI_NAME, LocalAdBankAccountHome.class);                
                         
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {
        	
        	try {
        	                   
	            LocalArCustomerType apExistingSupplierType = arCustomerTypeHome.findByCtName(details.getCtName(), AD_CMPNY);
	            
	            if (!apExistingSupplierType.getCtCode().equals(details.getCtCode())) {
	            
	                 throw new GlobalRecordAlreadyExistException();
	                 
	            }
	            
	        } catch (FinderException ex) {
	        	
	        }
            
			// find and update supplier class

			arCustomerType = arCustomerTypeHome.findByPrimaryKey(details.getCtCode());

				arCustomerType.setCtName(details.getCtName());
				arCustomerType.setCtDescription(details.getCtDescription());
				arCustomerType.setCtEnable(details.getCtEnable());
          										    		    		        			    		    		        
 			adBankAccount = adBankAccountHome.findByBaName(CT_BA_NM, AD_CMPNY);
 			adBankAccount.addArCustomerType(arCustomerType);
		    		
        } catch (GlobalRecordAlreadyExistException ex) {
        	
        	getSessionContext().setRollbackOnly();
        	throw ex;
        	
        } catch (Exception ex) {
        	
        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());
        	
        }
            
    }    
    
   /**
    * @ejb:interface-method view-type="remote"
    **/
    public void deleteArCtEntry(Integer CT_CODE, Integer AD_CMPNY) 
        throws GlobalRecordAlreadyDeletedException,
                GlobalRecordAlreadyAssignedException {

        Debug.print("ArCustomerTypeControllerBean deleteArCtEntry");
      
        LocalArCustomerTypeHome arCustomerTypeHome = null;

        // Initialize EJB Home
        
        try {

            arCustomerTypeHome = (LocalArCustomerTypeHome)EJBHomeFactory.
                lookUpLocalHome(LocalArCustomerTypeHome.JNDI_NAME, LocalArCustomerTypeHome.class);           
      
        } catch (NamingException ex) {

            throw new EJBException(ex.getMessage());

        }
      
        try {
      	
      	    LocalArCustomerType arCustomerType = null;                
      
	        try {
	      	
	            arCustomerType = arCustomerTypeHome.findByPrimaryKey(CT_CODE);
	         
	        } catch (FinderException ex) {
	      	
	            throw new GlobalRecordAlreadyDeletedException();
	            
	        }
	         
	        if (!arCustomerType.getArCustomers().isEmpty()) {
	  
	            throw new GlobalRecordAlreadyAssignedException();
	         
	        }
	                            	
		    arCustomerType.remove();
	    
        } catch (GlobalRecordAlreadyDeletedException ex) {
        	
        	getSessionContext().setRollbackOnly();
        	throw ex;
        	
        } catch (GlobalRecordAlreadyAssignedException ex) {
        	
        	getSessionContext().setRollbackOnly();
        	throw ex;
        	        	
        } catch (Exception ex) {
        	
        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());
        	
        }
            
    }         
   
    // SessionBean methods

    /**
     * @ejb:create-method view-type="remote"
     **/
    public void ejbCreate() throws CreateException {

        Debug.print("ArCustomerTypeControllerBean ejbCreate");
      
    }
    
}

