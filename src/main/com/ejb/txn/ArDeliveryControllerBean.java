/*
 * ArDeliveryControllerBean.java
 *
 * Created on June 7, 2019 12:00 PM
 * @author  Ruben P. Lamberte
 */

package com.ejb.txn;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;
import javax.naming.NamingException;

import com.ejb.ad.LocalAdBranchDocumentSequenceAssignment;
import com.ejb.ad.LocalAdBranchDocumentSequenceAssignmentHome;
import com.ejb.ad.LocalAdCompany;
import com.ejb.ad.LocalAdCompanyHome;
import com.ejb.ad.LocalAdLookUpValue;
import com.ejb.ad.LocalAdLookUpValueHome;
import com.ejb.ad.LocalAdPreference;
import com.ejb.ad.LocalAdPreferenceHome;
import com.ejb.ar.LocalArDelivery;
import com.ejb.ar.LocalArDeliveryHome;
import com.ejb.ar.LocalArJobOrderLine;
import com.ejb.ar.LocalArJobOrderLineHome;
import com.ejb.ar.LocalArJobOrderType;
import com.ejb.ar.LocalArJobOrderTypeHome;
import com.ejb.ar.LocalArPersonel;
import com.ejb.ar.LocalArPersonelHome;
import com.ejb.ar.LocalArSalesOrder;
import com.ejb.ar.LocalArSalesOrderHome;
import com.ejb.exception.GlobalDrNumberNotUniqueException;
import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.exception.GlobalRecordAlreadyExistException;
import com.ejb.gl.LocalAdDocumentSequenceAssignment;
import com.ejb.gl.LocalAdDocumentSequenceAssignmentHome;
import com.ejb.exception.GlobalNoRecordFoundException;
import com.util.AbstractSessionBean;
import com.util.ArModDeliveryDetails;
import com.util.ArModJobOrderLineDetails;
import com.util.ArModSalesOrderDetails;
import com.util.ArPersonelDetails;
import com.util.Debug;
import com.util.EJBCommon;
import com.util.EJBHomeFactory;

/**
 * @ejb:bean name="ArDeliveryControllerEJB"
 *           display-name="enter view Delivery"
 *           type="Stateless"
 *           view-type="remote"
 *           jndi-name="ejb/ArDeliveryControllerEJB"
 *
 * @ejb:interface remote-class="com.ejb.txn.ArDeliveryController"
 *                extends="javax.ejb.EJBObject"
 *
 * @ejb:home remote-class="com.ejb.txn.ArDeliveryControllerHome"
 *           extends="javax.ejb.EJBHome"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="apuser"
 *                        role-link="apuserlink"
 *
 * @ejb:permission role-name="apuser"
 * 
 */

public class ArDeliveryControllerBean extends AbstractSessionBean {
	
	
	
	
	/**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public void saveArDelivery( java.util.ArrayList list,java.lang.String SO_DCMNT_NMBR,java.lang.Integer AD_BRNCH,java.lang.Integer AD_CMPNY ) {
    	
    	Debug.print("ArDeliveryControllerBean saveArDelivery");
		
		LocalArDeliveryHome arDeliveryHome = null;
		LocalArSalesOrderHome arSalesOrderHome = null;
		LocalAdDocumentSequenceAssignmentHome adDocumentSequenceAssignmentHome = null;
		LocalAdBranchDocumentSequenceAssignmentHome adBranchDocumentSequenceAssignmentHome = null;
		// Initialize EJB Home
		
		try {
			
			arDeliveryHome = (LocalArDeliveryHome)EJBHomeFactory.
				lookUpLocalHome(LocalArDeliveryHome.JNDI_NAME, LocalArDeliveryHome.class);
			arSalesOrderHome = (LocalArSalesOrderHome)EJBHomeFactory.
					lookUpLocalHome(LocalArSalesOrderHome.JNDI_NAME, LocalArSalesOrderHome.class);
			adDocumentSequenceAssignmentHome = (LocalAdDocumentSequenceAssignmentHome)EJBHomeFactory.
					lookUpLocalHome(LocalAdDocumentSequenceAssignmentHome.JNDI_NAME, LocalAdDocumentSequenceAssignmentHome.class);
			adBranchDocumentSequenceAssignmentHome = (LocalAdBranchDocumentSequenceAssignmentHome)EJBHomeFactory.
					lookUpLocalHome(LocalAdBranchDocumentSequenceAssignmentHome.JNDI_NAME, LocalAdBranchDocumentSequenceAssignmentHome.class);
			
		} catch (NamingException ex) {
			
			throw new EJBException(ex.getMessage());
			
		}
		
		
		
		
		
		try {
			
			Iterator i = list.iterator();
			
			
			while(i.hasNext()) {
				
				ArModDeliveryDetails details = (ArModDeliveryDetails)i.next();
				
				
		
				
				
				
				
				if(details.getDvCode()==null) {
					
					
					
					LocalAdBranchDocumentSequenceAssignment adBranchDocumentSequenceAssignmentDrNum = null;
		 			LocalAdDocumentSequenceAssignment adDocumentSequenceAssignmentDrNum = null;

		 			//DR NUMBER GENERATOR
		 			
		 		
		 		

					try {
						adDocumentSequenceAssignmentDrNum = adDocumentSequenceAssignmentHome.findByDcName("AR DELIVERY" , AD_CMPNY);
					} catch(FinderException ex) {
					
					}


						
					
		 				try {

		 					adBranchDocumentSequenceAssignmentDrNum = adBranchDocumentSequenceAssignmentHome.findBdsByDsaCodeAndBrCode(adDocumentSequenceAssignmentDrNum.getDsaCode(), AD_BRNCH, AD_CMPNY);

		 				} catch (FinderException ex) {

		 				}
						
						/*
		 				try {
		 					
		 					
		 					LocalArSalesOrder arSalesOrderDrNumberExist = arSalesOrderHome.findByDvDeliveryNumber(details.getDvDeliveryNumber());
		 					
		 					throw new GlobalDrNumberNotUniqueException();
		 				}catch(FinderException ex) {
		 					
		 				}
		 				
						 */

						if (adDocumentSequenceAssignmentDrNum.getAdDocumentSequence().getDsNumberingType() == 'A' &&
								(details.getDvDeliveryNumber() == null || details.getDvDeliveryNumber().trim().length() == 0)) {

							while (true) {

								if (adBranchDocumentSequenceAssignmentDrNum == null || adBranchDocumentSequenceAssignmentDrNum.getBdsNextSequence() == null) {

									try {

									    arDeliveryHome.findByDvDeliveryNumber(adDocumentSequenceAssignmentDrNum.getDsaNextSequence());
									    adDocumentSequenceAssignmentDrNum.setDsaNextSequence(EJBCommon.incrementStringNumber(adDocumentSequenceAssignmentDrNum.getDsaNextSequence()));

									} catch (FinderException ex) {

										details.setDvDeliveryNumber(adDocumentSequenceAssignmentDrNum.getDsaNextSequence());
										adDocumentSequenceAssignmentDrNum.setDsaNextSequence(EJBCommon.incrementStringNumber(adDocumentSequenceAssignmentDrNum.getDsaNextSequence()));
										break;

									}

								} else {

									try {

									    arSalesOrderHome.findBySoDocumentNumberAndBrCode(adBranchDocumentSequenceAssignmentDrNum.getBdsNextSequence(), AD_BRNCH, AD_CMPNY);
									    adBranchDocumentSequenceAssignmentDrNum.setBdsNextSequence(EJBCommon.incrementStringNumber(adBranchDocumentSequenceAssignmentDrNum.getBdsNextSequence()));

									} catch (FinderException ex) {

										details.setDvDeliveryNumber(adBranchDocumentSequenceAssignmentDrNum.getBdsNextSequence());
										adBranchDocumentSequenceAssignmentDrNum.setBdsNextSequence(EJBCommon.incrementStringNumber(adBranchDocumentSequenceAssignmentDrNum.getBdsNextSequence()));
										break;

									}

								}

							}

						}

					
					
					
					
					
					
					
					
					
					
					
					LocalArDelivery arDelivery = arDeliveryHome.create(details.getDvContainer(), details.getDvDeliveryNumber(), details.getDvBookingTime(), details.getDvTabsFeePullOut(), 
							details.getDvReleasedDate(), details.getDvDeliveredDate(), details.getDvDeliveredTo(),
							details.getDvPlateNo(), details.getDvDriverName(), details.getDvEmptyReturnDate(), details.getDvEmptyReturnTo(), 
							details.getDvTabsFeeReturn(), details.getDvStatus(), details.getDvRemarks());
					
					
					LocalArSalesOrder arSalesOrder = arSalesOrderHome.findBySoDocumentNumberAndBrCode(SO_DCMNT_NMBR, AD_BRNCH, AD_CMPNY);
					
					
					
					arDelivery.setArSalesOrder(arSalesOrder);
					
					arSalesOrder.setSoTransactionStatus(details.getSoTransactionStatus());
					
					
					
				}else {
					
					System.out.println("not null detected in :" + details.getDvCode());
					try{

						Integer dvCode = details.getDvCode();
						
						
						LocalArDelivery arDelivery = arDeliveryHome.findByPrimaryKey(dvCode);
								
								
						arDelivery.setDvContainer(details.getDvContainer());
						arDelivery.setDvDeliveryNumber(details.getDvDeliveryNumber());
						arDelivery.setDvBookingTime(details.getDvBookingTime());
						arDelivery.setDvTabsFeePullOut(details.getDvTabsFeePullOut());
						arDelivery.setDvReleasedDate(details.getDvReleasedDate());
						arDelivery.setDvDeliveredDate(details.getDvDeliveredDate());
						arDelivery.setDvDeliveredTo(details.getDvDeliveredTo());
						arDelivery.setDvPlateNo(details.getDvPlateNo());
						arDelivery.setDvDriverName(details.getDvDriverName());
						arDelivery.setDvEmptyReturnDate(details.getDvEmptyReturnDate());
						arDelivery.setDvEmptyReturnTo(details.getDvEmptyReturnTo());
						arDelivery.setDvTabsFeeReturn(details.getDvTabsFeeReturn());
						arDelivery.setDvStatus(details.getDvStatus());
						arDelivery.setDvRemarks(details.getDvRemarks());
						
						
						LocalArSalesOrder arSalesOrder = arDelivery.getArSalesOrder();
						
						
						arSalesOrder.setSoTransactionStatus(details.getSoTransactionStatus());
					}catch(FinderException ex) {
						
					}
					

				}
		
				
				
				
			
				
				
				
				
				
				
			}
			
		}catch(Exception ex) {
			Debug.printStackTrace(ex);
	    	getSessionContext().setRollbackOnly();
	    	throw new EJBException(ex.getMessage());
		}
		
		
		
		
		
		
    	
    }
		      
    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public void deleteDeliveryByDvCode( java.lang.Integer DV_CODE )
      throws javax.ejb.FinderException{
	   
	   
    	Debug.print("ArDeliveryControllerBean deleteDeliveryByDvCode");
		
		LocalArDeliveryHome arDeliveryHome = null;
		

		// Initialize EJB Home
		
		try {
			
			arDeliveryHome = (LocalArDeliveryHome)EJBHomeFactory.
				lookUpLocalHome(LocalArDeliveryHome.JNDI_NAME, LocalArDeliveryHome.class);
			
		} catch (NamingException ex) {
			
			throw new EJBException(ex.getMessage());
			
		}
		
		
		try {
			
			
			LocalArDelivery arDelivery = arDeliveryHome.findByPrimaryKey(DV_CODE);
			
			
			
			
			
			arDelivery.remove();
		}catch(FinderException ex) {
			Debug.printStackTrace(ex);
	    
	    	throw new FinderException();
		
		}catch(Exception ex) {
			Debug.printStackTrace(ex);
	    	getSessionContext().setRollbackOnly();
	    	throw new EJBException(ex.getMessage());
		}
		
    }
    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public com.util.ArModSalesOrderDetails getArSalesOrderByCode( java.lang.Integer SO_CODE ) throws FinderException
    {
    	Debug.print("ArDeliveryControllerBean deleteDeliveryByDvCode");
		
		LocalArDeliveryHome arDeliveryHome = null;
		LocalArSalesOrderHome arSalesOrderHome = null;

		// Initialize EJB Home
		
		try {
			
			arDeliveryHome = (LocalArDeliveryHome)EJBHomeFactory.
				lookUpLocalHome(LocalArDeliveryHome.JNDI_NAME, LocalArDeliveryHome.class);
			arSalesOrderHome = (LocalArSalesOrderHome)EJBHomeFactory.
					lookUpLocalHome(LocalArSalesOrderHome.JNDI_NAME, LocalArSalesOrderHome.class);
			
		} catch (NamingException ex) {
			
			throw new EJBException(ex.getMessage());
			
		}
		
		
		try {
			
			
			LocalArSalesOrder arSalesOrder = arSalesOrderHome.findByPrimaryKey(SO_CODE);
			
			ArModSalesOrderDetails details = new ArModSalesOrderDetails();
			
			
			details.setSoCode(arSalesOrder.getSoCode());
			details.setSoDocumentNumber(arSalesOrder.getSoDocumentNumber());
			details.setSoReferenceNumber(arSalesOrder.getSoReferenceNumber());
			details.setSoCstCustomerCode(arSalesOrder.getArCustomer().getCstCustomerCode());
			details.setSoTransactionStatus(arSalesOrder.getSoTransactionStatus());
			
			
			return details;
			
		}catch(FinderException ex) {
			Debug.printStackTrace(ex);
	    
	    	throw new FinderException();
		
		}catch(Exception ex) {
			Debug.printStackTrace(ex);
	    	getSessionContext().setRollbackOnly();
	    	throw new EJBException(ex.getMessage());
		}
    }
    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public java.util.ArrayList getDeliveryBySoCode( java.lang.Integer SO_CODE,java.lang.Integer AD_BRNCH,java.lang.Integer AD_CMPNY ) throws FinderException
    {
    	Debug.print("ArDeliveryControllerBean getDeliveryBySoCode");
		ArrayList list = new ArrayList();
		LocalArDeliveryHome arDeliveryHome = null;
		LocalArSalesOrderHome arSalesOrderHome = null;

		// Initialize EJB Home
		
		try {
			
			arDeliveryHome = (LocalArDeliveryHome)EJBHomeFactory.
				lookUpLocalHome(LocalArDeliveryHome.JNDI_NAME, LocalArDeliveryHome.class);
			arSalesOrderHome = (LocalArSalesOrderHome)EJBHomeFactory.
					lookUpLocalHome(LocalArSalesOrderHome.JNDI_NAME, LocalArSalesOrderHome.class);
			
		} catch (NamingException ex) {
			
			throw new EJBException(ex.getMessage());
			
		}
		
		
		try {
			
			
			Collection dbList = arDeliveryHome.findBySoCode(SO_CODE);
			
			
			Iterator i = dbList.iterator();
			
			
			while(i.hasNext()) {
				
				LocalArDelivery arDelivery = (LocalArDelivery)i.next();
				
				
				ArModDeliveryDetails details = new ArModDeliveryDetails();
				
				
				details.setDvCode(arDelivery.getDvCode());
				details.setDvContainer(arDelivery.getDvContainer());
				details.setDvDeliveryNumber(arDelivery.getDvDeliveryNumber());
				details.setDvBookingTime(arDelivery.getDvBookingTime());
				details.setDvTabsFeePullOut(arDelivery.getDvTabsFeePullOut());
				details.setDvReleasedDate(arDelivery.getDvReleasedDate());
				details.setDvDeliveredDate(arDelivery.getDvDeliveredDate());
				details.setDvDeliveredTo(arDelivery.getDvDeliveredTo());
				details.setDvPlateNo(arDelivery.getDvPlateNo());
				details.setDvDriverName(arDelivery.getDvDriverName());
				details.setDvEmptyReturnDate(arDelivery.getDvEmptyReturnDate());
				details.setDvEmptyReturnTo(arDelivery.getDvEmptyReturnTo());
				details.setDvTabsFeeReturn(arDelivery.getDvTabsFeeReturn());
				details.setDvStatus(arDelivery.getDvStatus());
				details.setDvRemarks(arDelivery.getDvRemarks());
				
				details.setCstName(arDelivery.getArSalesOrder().getArCustomer().getCstCustomerCode());
				details.setSoDocumentNumber(arDelivery.getArSalesOrder().getSoDocumentNumber());
				details.setSoReferenceNumber(arDelivery.getArSalesOrder().getSoReferenceNumber());
				details.setSoTransactionStatus(arDelivery.getArSalesOrder().getSoTransactionStatus());
				
				list.add(details);
			} 
			
			
			


			return list;
			
		}catch(FinderException ex) {
			Debug.printStackTrace(ex);
	    
	    	throw new FinderException();
		
		}catch(Exception ex) {
			Debug.printStackTrace(ex);
	    	getSessionContext().setRollbackOnly();
	    	throw new EJBException(ex.getMessage());
		}
    }
    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public java.util.ArrayList getDeliveryBySoDocumentNumber( java.lang.String SO_DCMNT_NMBR,java.lang.Integer AD_BRNCH,java.lang.Integer AD_CMPNY ) throws FinderException
    {
    	Debug.print("ArDeliveryControllerBean getDeliveryBySoDocumentNumber");
		ArrayList list = new ArrayList();
		LocalArDeliveryHome arDeliveryHome = null;
		LocalArSalesOrderHome arSalesOrderHome = null;

		// Initialize EJB Home
		
		try {
			
			arDeliveryHome = (LocalArDeliveryHome)EJBHomeFactory.
				lookUpLocalHome(LocalArDeliveryHome.JNDI_NAME, LocalArDeliveryHome.class);
			arSalesOrderHome = (LocalArSalesOrderHome)EJBHomeFactory.
					lookUpLocalHome(LocalArSalesOrderHome.JNDI_NAME, LocalArSalesOrderHome.class);
			
		} catch (NamingException ex) {
			
			throw new EJBException(ex.getMessage());
			
		}
		
		
		try {
			
			
			Collection dbList = arDeliveryHome.findBySoDocumentNumber(SO_DCMNT_NMBR);
			
			
			Iterator i = dbList.iterator();
			
			
			while(i.hasNext()) {
				
				LocalArDelivery arDelivery = (LocalArDelivery)i.next();
				
				
				ArModDeliveryDetails details = new ArModDeliveryDetails();
				
				
				details.setDvCode(arDelivery.getDvCode());
				details.setDvContainer(arDelivery.getDvContainer());
				details.setDvDeliveryNumber(arDelivery.getDvDeliveryNumber());
				details.setDvBookingTime(arDelivery.getDvBookingTime());
				details.setDvTabsFeePullOut(arDelivery.getDvTabsFeePullOut());
				details.setDvReleasedDate(arDelivery.getDvReleasedDate());
				details.setDvDeliveredDate(arDelivery.getDvDeliveredDate());
				details.setDvDeliveredTo(arDelivery.getDvDeliveredTo());
				details.setDvPlateNo(arDelivery.getDvPlateNo());
				details.setDvDriverName(arDelivery.getDvDriverName());
				details.setDvEmptyReturnDate(arDelivery.getDvEmptyReturnDate());
				details.setDvEmptyReturnTo(arDelivery.getDvEmptyReturnTo());
				details.setDvTabsFeeReturn(arDelivery.getDvTabsFeeReturn());
				details.setDvStatus(arDelivery.getDvStatus());
				details.setDvRemarks(arDelivery.getDvRemarks());
				
				details.setCstName(arDelivery.getArSalesOrder().getArCustomer().getCstCustomerCode());
				details.setSoDocumentNumber(arDelivery.getArSalesOrder().getSoDocumentNumber());
				details.setSoReferenceNumber(arDelivery.getArSalesOrder().getSoReferenceNumber());
				details.setSoTransactionStatus(arDelivery.getArSalesOrder().getSoTransactionStatus());
				
				list.add(details);
			} 
			
			
			


			return list;
			
		}catch(FinderException ex) {
			Debug.printStackTrace(ex);
	    
	    	throw new FinderException();
		
		}catch(Exception ex) {
			Debug.printStackTrace(ex);
	    	getSessionContext().setRollbackOnly();
	    	throw new EJBException(ex.getMessage());
		}
   
    }


    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
	public ArrayList getAdLvTransactionStatusAll(Integer AD_CMPNY) {

		Debug.print("ArDeliveryControllerBean getAdLvTransactionStatusAll");

		LocalAdLookUpValueHome adLookUpValueHome = null;

		ArrayList list = new ArrayList();

		// Initialize EJB Home

		try {

			adLookUpValueHome = (LocalAdLookUpValueHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdLookUpValueHome.JNDI_NAME, LocalAdLookUpValueHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}

		try {

			String lookUpName = "AR SALES ORDER TRANSACTION STATUS";



			Collection adLookUpValues = adLookUpValueHome.findByLuName(lookUpName, AD_CMPNY);

			Iterator i = adLookUpValues.iterator();

			while (i.hasNext()) {

				LocalAdLookUpValue adLookUpValue = (LocalAdLookUpValue)i.next();

				list.add(adLookUpValue.getLvName());

			}

			return list;

		} catch (Exception ex) {

			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());

		}

	}
	//	 SessionBean methods
	
	/**
	 * @ejb:create-method view-type="remote"
	 **/
	public void ejbCreate() throws CreateException {
		
		Debug.print("ArDeliveryControllerBean ejbCreate");
		
	}
	
}