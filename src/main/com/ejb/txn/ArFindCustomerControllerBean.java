
/*
 * ArFindCustomerControllerBean.java
 *
 * Created on March 4, 2004, 1:04 PM
 *
 * @author  Neil Andrew M. Ajero
 */

package com.ejb.txn;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;
import javax.naming.NamingException;

import com.ejb.ad.LocalAdBranchResponsibility;
import com.ejb.ad.LocalAdBranchResponsibilityHome;
import com.ejb.ad.LocalAdLookUpValue;
import com.ejb.ad.LocalAdLookUpValueHome;
import com.ejb.ar.LocalArCustomer;
import com.ejb.ar.LocalArCustomerClass;
import com.ejb.ar.LocalArCustomerClassHome;
import com.ejb.ar.LocalArCustomerHome;
import com.ejb.ar.LocalArCustomerType;
import com.ejb.ar.LocalArCustomerTypeHome;
import com.ejb.exception.GlobalNoRecordFoundException;
import com.util.AbstractSessionBean;
import com.util.AdBranchDetails;
import com.util.AdModBranchCustomerDetails;
import com.util.ArModCustomerDetails;
import com.util.Debug;
import com.util.EJBCommon;
import com.util.EJBHomeFactory;

/**
 * @ejb:bean name="ArFindCustomerControllerEJB"
 *           display-name="Used for finding customers"
 *           type="Stateless"
 *           view-type="remote"
 *           jndi-name="ejb/ArFindCustomerControllerEJB"
 *
 * @ejb:interface remote-class="com.ejb.txn.ArFindCustomerController"
 *                extends="javax.ejb.EJBObject"
 *
 * @ejb:home remote-class="com.ejb.txn.ArFindCustomerControllerHome"
 *           extends="javax.ejb.EJBHome"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="aruser"
 *                        role-link="aruserlink"
 *
 * @ejb:permission role-name="aruser"
 * 
*/

public class ArFindCustomerControllerBean extends AbstractSessionBean {
	
	
	/**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
	public ArrayList getAdLvCustomerBatchAll(Integer AD_CMPNY) {
		
		Debug.print("ArFindCustomerControllerBean getAdLvCustomerBatchAll");
		
		LocalAdLookUpValueHome adLookUpValueHome = null;               
		
		ArrayList list = new ArrayList();
		
		// Initialize EJB Home
		
		try {
			
			adLookUpValueHome = (LocalAdLookUpValueHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdLookUpValueHome.JNDI_NAME, LocalAdLookUpValueHome.class);
			
		} catch (NamingException ex) {
			
			throw new EJBException(ex.getMessage());
			
		}        
		
		try {
			
			Collection adLookUpValues = adLookUpValueHome.findByLuName("AR CUSTOMER BATCH - SOA", AD_CMPNY);
			
			Iterator i = adLookUpValues.iterator();
			
			while (i.hasNext()) {
				
				LocalAdLookUpValue adLookUpValue = (LocalAdLookUpValue)i.next();
				
				list.add(adLookUpValue.getLvName());
				
			}
			
			return list;
			
		} catch (Exception ex) {
			
			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());
			
		}
		
	}
	

    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList getArCtAll(Integer AD_CMPNY) {
                    
        Debug.print("ArFindCustomerControllerBean getArCtAll");
        
        LocalArCustomerTypeHome arCustomerTypeHome = null;
        LocalArCustomerType arCustomerType = null;

        ArrayList list = new ArrayList();
        
        // Initialize EJB Home
        
        try {
            
            arCustomerTypeHome = (LocalArCustomerTypeHome)EJBHomeFactory.
                lookUpLocalHome(LocalArCustomerTypeHome.JNDI_NAME, LocalArCustomerTypeHome.class);
            
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {
            
            Collection arCustomerTypes = arCustomerTypeHome.findEnabledCtAll(AD_CMPNY);

	        Iterator i = arCustomerTypes.iterator();
	               
	        while (i.hasNext()) {
	        	
	        	arCustomerType = (LocalArCustomerType)i.next();
	        	
	        	list.add(arCustomerType.getCtName());
	        	
	        }
	        
	        return list;
            
        } catch (Exception ex) {
        	
        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());
        	
        }
            
    }
    
    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList getArCcAll(Integer AD_CMPNY) {
                    
        Debug.print("ArFindCustomerControllerBean getArCcAll");
        
        LocalArCustomerClassHome arCustomerClassHome = null;
        LocalArCustomerClass arCustomerClass = null;

        ArrayList list = new ArrayList();
        
        // Initialize EJB Home
        
        try {
            
            arCustomerClassHome = (LocalArCustomerClassHome)EJBHomeFactory.
                lookUpLocalHome(LocalArCustomerClassHome.JNDI_NAME, LocalArCustomerClassHome.class);
            
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {
            
            Collection arCustomerClasses = arCustomerClassHome.findEnabledCcAll(AD_CMPNY);
	        
	        Iterator i = arCustomerClasses.iterator();
	               
	        while (i.hasNext()) {
	        	
	        	arCustomerClass = (LocalArCustomerClass)i.next();
	        	
	        	list.add(arCustomerClass.getCcName());
	        	
	        }
	        
	        return list;
          
        } catch (Exception ex) {
        	
        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());
        	
        }
            
    }
    
    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList getArCstByCriteria(HashMap criteria, ArrayList branchList,
        Integer OFFSET, Integer LIMIT, String ORDER_BY, Integer AD_CMPNY) 
        throws GlobalNoRecordFoundException {
                    
        Debug.print("ArFindCustomerControllerBean getArCstByCriteria");
        
        LocalArCustomerHome arCustomerHome = null;
        
        ArrayList list = new ArrayList();
        
        //initialized EJB Home
        
        try {
            
            arCustomerHome = (LocalArCustomerHome)EJBHomeFactory.
                lookUpLocalHome(LocalArCustomerHome.JNDI_NAME, LocalArCustomerHome.class);
            
        } catch (NamingException ex) 	{
            
            throw new EJBException(ex.getMessage());
            
        }
        
		try { 
           
		  StringBuffer jbossQl = new StringBuffer();
		  
			if(branchList.size() > 0) {
				
				jbossQl.append("SELECT DISTINCT OBJECT(cst) FROM ArCustomer cst, IN(cst.adBranchCustomers)bcst ");
				
			} else {
				
				jbossQl.append("SELECT OBJECT(cst) FROM ArCustomer cst ");
				
			}

		  boolean firstArgument = true;
		  short ctr = 0;
		  int criteriaSize = criteria.size() + 2 + branchList.size();
		  
		  
		  Object obj[] = null;		      
		
		  // Allocate the size of the object parameter
		
		   
		  if (criteria.containsKey("customerCode")) {
		  	
		  	 criteriaSize--;
		  	 
		  } 
		  
		  if (criteria.containsKey("name")) {
		  	
		  	 criteriaSize--;
		  	 
		  } 
		  
		  if (criteria.containsKey("employeeId")) {
			  	
			  	 criteriaSize--;
			  	 
		  } 
		  		  
		  if (criteria.containsKey("email")) {
			
		  	 criteriaSize--;
		  
		  }
		  
		  if (criteria.containsKey("approvalStatus")) {
		      	
		      	 String approvalStatus = (String)criteria.get("approvalStatus");
		      	
		      	 if (approvalStatus.equals("DRAFT") || approvalStatus.equals("REJECTED")) {
		      	 	
		      	 	 criteriaSize--;
		      	 	
		      	 }
		      	
		      }
		  
		  obj = new Object[criteriaSize];    
		       	      
		  if (criteria.containsKey("customerCode")) {
		  	
		  	 if (!firstArgument) {
		  	 
		  	    jbossQl.append("AND ");	
		  	 	
		     } else {
		     	
		     	firstArgument = false;
		     	jbossQl.append("WHERE ");
		     	
		     }
		     
		  	 jbossQl.append("cst.cstCustomerCode LIKE '%" + (String)criteria.get("customerCode") + "%' ");
		  	 
		  }

			if(branchList.size() > 0) {
				
					if (!firstArgument) {
						
						jbossQl.append("AND ");
						
					} else {
						
						firstArgument = false;
						jbossQl.append("WHERE ");
						
					}
					
					AdModBranchCustomerDetails details = null;
					
					Iterator i = branchList.iterator();
					
					details = (AdModBranchCustomerDetails)i.next();
					
					jbossQl.append(" ( bcst.adBranch.brBranchCode=?" + (ctr + 1) + " ");
					obj[ctr] = (String)details.getBcstBranchCode();
					
					ctr++;
					
					while(i.hasNext()) {
						
						details = (AdModBranchCustomerDetails)i.next();
						
						jbossQl.append("OR bcst.adBranch.brBranchCode=?" + (ctr + 1) + " ");
						obj[ctr] = (String)details.getBcstBranchCode();
						
						ctr++;
							
					}
					
					jbossQl.append(" ) ");
					
				}

	      if (criteria.containsKey("name")) {
	  	
		  	 if (!firstArgument) {
		  	 	jbossQl.append("AND ");
		  	 } else {
		  	 	firstArgument = false;
		  	 	jbossQl.append("WHERE ");
		  	 }
	  	 
	   	     jbossQl.append("cst.cstName LIKE '%" + (String)criteria.get("name") + "%' ");		
	  	 
	      } 
	      
	      if (criteria.containsKey("employeeId")) {
	  	  	
		  	 if (!firstArgument) {
		  	 	jbossQl.append("AND ");
		  	 } else {
		  	 	firstArgument = false;
		  	 	jbossQl.append("WHERE ");
		  	 }
	  	 
	   	     jbossQl.append("cst.cstEmployeeID = '" + (String)criteria.get("employeeId") + "' ");		
	  	 
	      } 
	      
	      
			
		  if (criteria.containsKey("email")) {
		  	
		  	 if (!firstArgument) {
		  	 	jbossQl.append("AND ");
		  	 } else {
		  	 	firstArgument = false;
		  	 	jbossQl.append("WHERE ");
		  	 }
		  	 
		   	 jbossQl.append("cst.cstEmail LIKE '%" + (String)criteria.get("email") + "%' ");
		
		  	 
		  }	
			
		  if (criteria.containsKey("customerType")) {
	   	
		   	  if (!firstArgument) {		       	  	
		   	     jbossQl.append("AND ");		       	     
		   	  } else {		       	  	
		   	  	 firstArgument = false;
		   	  	 jbossQl.append("WHERE ");		       	  	 
		   	  }
		   	  
		   	  jbossQl.append("cst.arCustomerType.ctName=?" + (ctr+1) + " ");
		   	  obj[ctr] = (String)criteria.get("customerType");
		   	  ctr++;
	   	  
	      }	
			
		  if (criteria.containsKey("customerClass")) {
	   	
		   	  if (!firstArgument) {		       	  	
		   	     jbossQl.append("AND ");		       	     
		   	  } else {		       	  	
		   	  	 firstArgument = false;
		   	  	 jbossQl.append("WHERE ");		       	  	 
		   	  }
	   	  
		   	  jbossQl.append("cst.arCustomerClass.ccName=?" + (ctr+1) + " ");
		   	  obj[ctr] = (String)criteria.get("customerClass");
		   	  ctr++;
	   	  
	      }
		  
		  if (criteria.containsKey("customerBatch")) {
			   	
		   	  if (!firstArgument) {		       	  	
		   	     jbossQl.append("AND ");		       	     
		   	  } else {		       	  	
		   	  	 firstArgument = false;
		   	  	 jbossQl.append("WHERE ");		       	  	 
		   	  }
	   	  
		   	  jbossQl.append("cst.cstCustomerBatch=?" + (ctr+1) + " ");
		   	  obj[ctr] = (String)criteria.get("customerBatch");
		   	  ctr++;
	   	  
	      }
		  
		  if (criteria.containsKey("approvalStatus")) {
		       	
	       	  if (!firstArgument) {
	       	  	
	       	     jbossQl.append("AND ");
	       	     
	       	  } else {
	       	  	
	       	  	 firstArgument = false;
	       	  	 jbossQl.append("WHERE ");
	       	  	 
	       	  }
	       	  
	       	  String approvalStatus = (String)criteria.get("approvalStatus");
       	  
	       	  if (approvalStatus.equals("DRAFT")) {
	       	      
		       	  jbossQl.append("cst.cstApprovalStatus IS NULL ");
	       	  	
	       	  } else if (approvalStatus.equals("REJECTED")) {
	       	  	
		       	  jbossQl.append("cst.cstReasonForRejection IS NOT NULL ");
	       	  	
	      	  } else {
	      	  	
		      	  jbossQl.append("cst.cstApprovalStatus=?" + (ctr+1) + " ");
		       	  obj[ctr] = approvalStatus;
		       	  ctr++;
	      	  	
	      	  }
	       	  
	      }
			  
		  if (criteria.containsKey("enable") &&
		      criteria.containsKey("disable")) {
		      	
		      if (!firstArgument) {
		  	 	jbossQl.append("AND ");
		  	 } else {
		  	 	firstArgument = false;
		  	 	jbossQl.append("WHERE ");
		  	 }
		  	 
		  	 jbossQl.append("(cst.cstEnable=?" + (ctr+1) + " OR ");
		  	 obj[ctr] = new Byte(EJBCommon.TRUE);
		     ctr++;
		     
		     jbossQl.append("cst.cstEnable=?" + (ctr+1) + ") ");
		  	 obj[ctr] = new Byte(EJBCommon.FALSE);
		     ctr++;
		     
		  } else {
			  	         
		      if (criteria.containsKey("enable")) {
		      	
		      	 if (!firstArgument) {
		      	 	jbossQl.append("AND ");
		      	 } else {
		      	 	firstArgument = false;
		      	 	jbossQl.append("WHERE ");
		      	 }
		      	 jbossQl.append("cst.cstEnable=?" + (ctr+1) + " ");
		      	 obj[ctr] = new Byte(EJBCommon.TRUE);
		      	 ctr++;
		      	 
		      } 
		      
		      if (criteria.containsKey("disable")) {
		      	
		      	 if (!firstArgument) {
		      	 	jbossQl.append("AND ");
		      	 } else {
		      	 	firstArgument = false;
		      	 	jbossQl.append("WHERE ");
		      	 }
		      	 jbossQl.append("cst.cstEnable=?" + (ctr+1) + " ");		      	 		      	 		      	 	      	 
		      	 obj[ctr] = new Byte(EJBCommon.FALSE);
		      	 ctr++;
		      	 
		      }
		      
		  }	   
		   if (criteria.containsKey("enablePayroll")){
                       
                        if (!firstArgument) {
		      	 	jbossQl.append("AND ");
		      	 } else {
		      	 	firstArgument = false;
		      	 	jbossQl.append("WHERE ");
		      	 }
                       
                       jbossQl.append("cst.cstEnablePayroll=?" + (ctr+1) + " ");		      	 		      	 		      	 	      	 
		      	 obj[ctr] = criteria.get("enablePayroll");
		      	 ctr++;
                       
                   }
		  if (!firstArgument) {
       	  	
       	     jbossQl.append("AND ");
       	     
       	  } else {
       	  	
       	  	 firstArgument = false;
       	  	 jbossQl.append("WHERE ");
       	  	 
       	  }
       	  
       	  jbossQl.append("cst.cstAdCompany=" + AD_CMPNY + " ");
		     	      			
		  String orderBy = null;
		  
		  if (ORDER_BY.equals("CUSTOMER CODE")) {	          
		      	      		
		  	  orderBy = "cst.cstCustomerCode";
		  	
		  } else {
		  	
		  	  orderBy = "cst.cstName";
		
		  } 
		
		  jbossQl.append("ORDER BY " + orderBy);
		  
		  jbossQl.append(" OFFSET ?" + (ctr + 1));	        
		  obj[ctr] = OFFSET;	      
		  ctr++;
		  
		  jbossQl.append(" LIMIT ?" + (ctr + 1));
		  obj[ctr] = LIMIT;
		  ctr++;		      
			  	      	
		  System.out.println(jbossQl.toString());
		  
	      Collection arCustomers = arCustomerHome.getCstByCriteria(jbossQl.toString(), obj);	         
		  
		  if (arCustomers.size() == 0)
		     throw new GlobalNoRecordFoundException();
		     
		  Iterator i = arCustomers.iterator();
		
		  while (i.hasNext()) {
		  	
		  	  LocalArCustomer arCustomer = (LocalArCustomer)i.next();   	  
		  	  
		  	  ArModCustomerDetails mdetails = new ArModCustomerDetails();
		  	  mdetails.setCstCode(arCustomer.getCstCode());
		  	  mdetails.setCstCustomerCode(arCustomer.getCstCustomerCode());
		  	  mdetails.setCstName(arCustomer.getCstName());
		  	  mdetails.setCstEmployeeID(arCustomer.getCstEmployeeID());
		  	  mdetails.setCstAddress(arCustomer.getCstAddress());
		  	  System.out.println("1");
		  	  mdetails.setCstArea(arCustomer.getArCustomerClass().getCcName());
		  	  System.out.println("2");
		  	  mdetails.setCstSlpName(arCustomer.getArSalesperson() != null ? arCustomer.getArSalesperson().getSlpName() : null);
		  	  System.out.println("3");
		  	  mdetails.setCstTin(arCustomer.getCstTin());
		  	  mdetails.setCstEmail(arCustomer.getCstEmail());
		  	  mdetails.setCstCtName(arCustomer.getArCustomerType() != null ? arCustomer.getArCustomerType().getCtName() : null);
		  	  mdetails.setCstCcName(arCustomer.getArCustomerClass().getCcName());
		  	  mdetails.setCstPytName(arCustomer.getAdPaymentTerm().getPytName());
		  	  mdetails.setCstEnable(arCustomer.getCstEnable());      	  

			  list.add(mdetails);
			  			         
		  }
			     
		  return list;
		      
	  } catch (GlobalNoRecordFoundException ex) {
	  	 
	  	  throw ex;
	  	
	  } catch (Exception ex) {
	  	

	  	  ex.printStackTrace();
	  	  throw new EJBException(ex.getMessage());
	  	
	  }
        
    }
    
    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public Integer getArCstSizeByCriteria(HashMap criteria, ArrayList branchList, Integer AD_CMPNY) 
        throws GlobalNoRecordFoundException {
                    
        Debug.print("ArFindCustomerControllerBean getArCstSizeByCriteria");
        
        LocalArCustomerHome arCustomerHome = null;
        
        //initialized EJB Home
        
        try {
            
            arCustomerHome = (LocalArCustomerHome)EJBHomeFactory.
                lookUpLocalHome(LocalArCustomerHome.JNDI_NAME, LocalArCustomerHome.class);
            
        } catch (NamingException ex) 	{
            
            throw new EJBException(ex.getMessage());
            
        }
        
		try { 
           
		  StringBuffer jbossQl = new StringBuffer();
		  
			if(branchList.size() > 0) {
				
				jbossQl.append("SELECT DISTINCT OBJECT(cst) FROM ArCustomer cst, IN(cst.adBranchCustomers)bcst ");
				
			} else {
				
				jbossQl.append("SELECT OBJECT(cst) FROM ArCustomer cst ");
				
			}

		  boolean firstArgument = true;
		  short ctr = 0;
		  int criteriaSize = criteria.size() + branchList.size();
		  
		  
		  Object obj[] = null;		      
		
		  // Allocate the size of the object parameter
		
		   
		  if (criteria.containsKey("customerCode")) {
		  	
		  	 criteriaSize--;
		  	 
		  } 
		  
		  if (criteria.containsKey("name")) {
		  	
		  	 criteriaSize--;
		  	 
		  } 
		  
		  if (criteria.containsKey("employeeId")) {
			  	
			  	 criteriaSize--;
			  	 
		  } 
		  		  
		  if (criteria.containsKey("email")) {
			
		  	 criteriaSize--;
		  
		  }
		  
		  if (criteria.containsKey("approvalStatus")) {
		      	
		      	 String approvalStatus = (String)criteria.get("approvalStatus");
		      	
		      	 if (approvalStatus.equals("DRAFT") || approvalStatus.equals("REJECTED")) {
		      	 	
		      	 	 criteriaSize--;
		      	 	
		      	 }
		      	
		      }
		  
		  obj = new Object[criteriaSize];    
		       	      
		  if (criteria.containsKey("customerCode")) {
		  	
		  	 if (!firstArgument) {
		  	 
		  	    jbossQl.append("AND ");	
		  	 	
		     } else {
		     	
		     	firstArgument = false;
		     	jbossQl.append("WHERE ");
		     	
		     }
		     
		  	 jbossQl.append("cst.cstCustomerCode LIKE '%" + (String)criteria.get("customerCode") + "%' ");
		  	 
		  }

			if(branchList.size() > 0) {
				
					if (!firstArgument) {
						
						jbossQl.append("AND ");
						
					} else {
						
						firstArgument = false;
						jbossQl.append("WHERE ");
						
					}
					
					AdModBranchCustomerDetails details = null;
					
					Iterator i = branchList.iterator();
					
					details = (AdModBranchCustomerDetails)i.next();
					
					jbossQl.append(" ( bcst.adBranch.brBranchCode=?" + (ctr + 1) + " ");
					obj[ctr] = (String)details.getBcstBranchCode();
					
					ctr++;
					
					while(i.hasNext()) {
						
						details = (AdModBranchCustomerDetails)i.next();
						
						jbossQl.append("OR bcst.adBranch.brBranchCode=?" + (ctr + 1) + " ");
						obj[ctr] = (String)details.getBcstBranchCode();
						
						ctr++;
							
					}
					
					jbossQl.append(" ) ");
					
				}

	      if (criteria.containsKey("name")) {
	  	
		  	 if (!firstArgument) {
		  	 	jbossQl.append("AND ");
		  	 } else {
		  	 	firstArgument = false;
		  	 	jbossQl.append("WHERE ");
		  	 }
	  	 
	   	     jbossQl.append("cst.cstName LIKE '%" + (String)criteria.get("name") + "%' ");		
	  	 
	      } 
	      
	      if (criteria.containsKey("employeeId")) {
	  	  	
		  	 if (!firstArgument) {
		  	 	jbossQl.append("AND ");
		  	 } else {
		  	 	firstArgument = false;
		  	 	jbossQl.append("WHERE ");
		  	 }
	  	 
	   	     jbossQl.append("cst.cstEmployeeID = '" + (String)criteria.get("employeeId") + "' ");		
	  	 
	      } 
			
		  if (criteria.containsKey("email")) {
		  	
		  	 if (!firstArgument) {
		  	 	jbossQl.append("AND ");
		  	 } else {
		  	 	firstArgument = false;
		  	 	jbossQl.append("WHERE ");
		  	 }
		  	 
		   	 jbossQl.append("cst.cstEmail LIKE '%" + (String)criteria.get("email") + "%' ");
		
		  	 
		  }	
			
		  if (criteria.containsKey("customerType")) {
	   	
		   	  if (!firstArgument) {		       	  	
		   	     jbossQl.append("AND ");		       	     
		   	  } else {		       	  	
		   	  	 firstArgument = false;
		   	  	 jbossQl.append("WHERE ");		       	  	 
		   	  }
		   	  
		   	  jbossQl.append("cst.arCustomerType.ctName=?" + (ctr+1) + " ");
		   	  obj[ctr] = (String)criteria.get("customerType");
		   	  ctr++;
	   	  
	      }	
			
		  if (criteria.containsKey("customerClass")) {
	   	
		   	  if (!firstArgument) {		       	  	
		   	     jbossQl.append("AND ");		       	     
		   	  } else {		       	  	
		   	  	 firstArgument = false;
		   	  	 jbossQl.append("WHERE ");		       	  	 
		   	  }
	   	  
		   	  jbossQl.append("cst.arCustomerClass.ccName=?" + (ctr+1) + " ");
		   	  obj[ctr] = (String)criteria.get("customerClass");
		   	  ctr++;
	   	  
	      }
		  
		  if (criteria.containsKey("customerBatch")) {
			   	
		   	  if (!firstArgument) {		       	  	
		   	     jbossQl.append("AND ");		       	     
		   	  } else {		       	  	
		   	  	 firstArgument = false;
		   	  	 jbossQl.append("WHERE ");		       	  	 
		   	  }
	   	  
		   	  jbossQl.append("cst.cstCustomerBatch=?" + (ctr+1) + " ");
		   	  obj[ctr] = (String)criteria.get("customerBatch");
		   	  ctr++;
	   	  
	      }
		  
		  if (criteria.containsKey("approvalStatus")) {
		       	
	       	  if (!firstArgument) {
	       	  	
	       	     jbossQl.append("AND ");
	       	     
	       	  } else {
	       	  	
	       	  	 firstArgument = false;
	       	  	 jbossQl.append("WHERE ");
	       	  	 
	       	  }
	       	  
	       	  String approvalStatus = (String)criteria.get("approvalStatus");
       	  
	       	  if (approvalStatus.equals("DRAFT")) {
	       	      
		       	  jbossQl.append("cst.cstApprovalStatus IS NULL ");
	       	  	
	       	  } else if (approvalStatus.equals("REJECTED")) {
	       	  	
		       	  jbossQl.append("cst.cstReasonForRejection IS NOT NULL ");
	       	  	
	      	  } else {
	      	  	
		      	  jbossQl.append("cst.cstApprovalStatus=?" + (ctr+1) + " ");
		       	  obj[ctr] = approvalStatus;
		       	  ctr++;
	      	  	
	      	  }
	       	  
	      }
			  
		  if (criteria.containsKey("enable") &&
		      criteria.containsKey("disable")) {
		      	
		      if (!firstArgument) {
		  	 	jbossQl.append("AND ");
		  	 } else {
		  	 	firstArgument = false;
		  	 	jbossQl.append("WHERE ");
		  	 }
		  	 
		  	 jbossQl.append("(cst.cstEnable=?" + (ctr+1) + " OR ");
		  	 obj[ctr] = new Byte(EJBCommon.TRUE);
		     ctr++;
		     
		     jbossQl.append("cst.cstEnable=?" + (ctr+1) + ") ");
		  	 obj[ctr] = new Byte(EJBCommon.FALSE);
		     ctr++;
		     
		  } else {
			  	         
		      if (criteria.containsKey("enable")) {
		      	
		      	 if (!firstArgument) {
		      	 	jbossQl.append("AND ");
		      	 } else {
		      	 	firstArgument = false;
		      	 	jbossQl.append("WHERE ");
		      	 }
		      	 jbossQl.append("cst.cstEnable=?" + (ctr+1) + " ");
		      	 obj[ctr] = new Byte(EJBCommon.TRUE);
		      	 ctr++;
		      	 
		      } 
		      
		      if (criteria.containsKey("disable")) {
		      	
		      	 if (!firstArgument) {
		      	 	jbossQl.append("AND ");
		      	 } else {
		      	 	firstArgument = false;
		      	 	jbossQl.append("WHERE ");
		      	 }
		      	 jbossQl.append("cst.cstEnable=?" + (ctr+1) + " ");		      	 		      	 		      	 	      	 
		      	 obj[ctr] = new Byte(EJBCommon.FALSE);
		      	 ctr++;
		      	 
		      }
		      
		  }	   
		  
		  if (!firstArgument) {
       	  	
       	     jbossQl.append("AND ");
       	     
       	  } else {
       	  	
       	  	 firstArgument = false;
       	  	 jbossQl.append("WHERE ");
       	  	 
       	  }
       	  
       	  jbossQl.append("cst.cstAdCompany=" + AD_CMPNY + " ");
		     	      			
		  System.out.println(jbossQl.toString());
		  
	      Collection arCustomers = arCustomerHome.getCstByCriteria(jbossQl.toString(), obj);	         
		  
		  if (arCustomers.size() == 0)
		     throw new GlobalNoRecordFoundException();
		 	     
		  return new Integer(arCustomers.size());
		      
	  } catch (GlobalNoRecordFoundException ex) {
	  	 
	  	  throw ex;
	  	
	  } catch (Exception ex) {
	  	

	  	  ex.printStackTrace();
	  	  throw new EJBException(ex.getMessage());
	  	
	  }
        
    }

    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList getAdBrResAll(int resCode, Integer AD_CMPNY) 
        throws GlobalNoRecordFoundException {
                    
        Debug.print("ArFindCustomerControllerBean getAdResAll");
        
        LocalAdBranchResponsibilityHome adBrResHome = null;
        LocalAdBranchResponsibility adBrRes = null;

        
        Collection adBranches = null;
        ArrayList list = new ArrayList();
        
        // Initialize EJB Home
        
        try {
            
        	adBrResHome = (LocalAdBranchResponsibilityHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdBranchResponsibilityHome.JNDI_NAME, LocalAdBranchResponsibilityHome.class);
            
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {
            
        	adBranches = adBrResHome.findByAdResponsibility(new Integer(resCode), AD_CMPNY);
            
        } catch (FinderException ex) {
            
        } catch (Exception ex) {
            throw new EJBException(ex.getMessage());
        }
        
        if (adBranches.isEmpty()) {
            throw new GlobalNoRecordFoundException();
        	
        }

    	Iterator i = adBranches.iterator();
    	
    	while ( i.hasNext() ) {
    		
    		adBrRes = (LocalAdBranchResponsibility)i.next();
    		AdBranchDetails details = new AdBranchDetails();
    		    		
    		details.setBrBranchCode(adBrRes.getAdBranch().getBrBranchCode());
    		details.setBrName(adBrRes.getAdBranch().getBrName());
    		list.add(details);
    	}
        
    	
        return list;
            
    }
    
    
    // SessionBean methods

    /**
     * @ejb:create-method view-type="remote"
     **/
    public void ejbCreate() throws CreateException {

       Debug.print("ArFindCustomerControllerBean ejbCreate");
      
    }
}
