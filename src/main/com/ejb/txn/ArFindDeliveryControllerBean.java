package com.ejb.txn;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.naming.NamingException;

import com.ejb.ad.LocalAdLookUpValue;
import com.ejb.ad.LocalAdLookUpValueHome;
import com.ejb.ad.LocalAdPreference;
import com.ejb.ad.LocalAdPreferenceHome;
import com.ejb.ar.LocalArCustomer;
import com.ejb.ar.LocalArCustomerHome;
import com.ejb.ar.LocalArDelivery;
import com.ejb.ar.LocalArDeliveryHome;

import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.gl.LocalGlFunctionalCurrency;
import com.ejb.gl.LocalGlFunctionalCurrencyHome;
import com.util.AbstractSessionBean;
import com.util.ArModDeliveryDetails;
import com.util.Debug;
import com.util.EJBCommon;
import com.util.EJBHomeFactory;

/**
 * @ejb:bean name="ArFindDeliveryControllerEJB"
 *           display-name="Used for finding sales orders"
 *           type="Stateless"
 *           view-type="remote"
 *           jndi-name="ejb/ArFindDeliveryControllerEJB"
 *
 * @ejb:interface remote-class="com.ejb.txn.ArFindDeliveryController"
 *                extends="javax.ejb.EJBObject"
 *
 * @ejb:home remote-class="com.ejb.txn.ArFindDeliveryControllerHome"
 *           extends="javax.ejb.EJBHome"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="invuser"
 *                        role-link="invuserlink"
 *
 * @ejb:permission role-name="invuser"
 *
 */

public class ArFindDeliveryControllerBean extends AbstractSessionBean {

    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList getArCstAll(Integer AD_BRNCH, Integer AD_CMPNY) {

        Debug.print("ArFindDeliveryControllerBean getArCstAll");

        LocalArCustomerHome arCustomerHome = null;

        ArrayList list = new ArrayList();

        // Initialize EJB Home

        try {

            arCustomerHome = (LocalArCustomerHome)EJBHomeFactory.
                lookUpLocalHome(LocalArCustomerHome.JNDI_NAME, LocalArCustomerHome.class);

        } catch (NamingException ex) {

            throw new EJBException(ex.getMessage());

        }

        try {

            Collection arCustomers = arCustomerHome.findEnabledCstAll(AD_BRNCH, AD_CMPNY);

	        Iterator i = arCustomers.iterator();

	        while (i.hasNext()) {

	        	LocalArCustomer arCustomer = (LocalArCustomer)i.next();

	        	list.add(arCustomer.getCstCustomerCode());

	        }

	        return list;

        } catch (Exception ex) {

        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());

        }

    }


  
    
    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList getArDvByCriteria(HashMap criteria, boolean isChild,
        Integer OFFSET, Integer LIMIT, String ORDER_BY, Integer AD_BRNCH, Integer AD_CMPNY)
        throws GlobalNoRecordFoundException {

        Debug.print("ArFindDeliveryControllerBean getArDvByCriteria");

        LocalArDeliveryHome arDeliveryHome = null;

        ArrayList list = new ArrayList();

        //initialized EJB Home

        try {

            arDeliveryHome = (LocalArDeliveryHome)EJBHomeFactory.
                lookUpLocalHome(LocalArDeliveryHome.JNDI_NAME, LocalArDeliveryHome.class);

        } catch (NamingException ex) {

            throw new EJBException(ex.getMessage());

        }

        try {

        	StringBuffer jbossQl = new StringBuffer();
        	jbossQl.append("SELECT OBJECT(dv) FROM ArDelivery dv ");

        	boolean firstArgument = true;
        	short ctr = 0;
        	int criteriaSize = criteria.size() + 2;

        	Object obj[];

        	// Allocate the size of the object parameter

        	if (criteria.containsKey("referenceNumber")) {

        		criteriaSize--;

        	}
        	
        

        
        	if (criteria.containsKey("transactionStatus")) {

        		criteriaSize--;

        	}
        	
        	
        	if (criteria.containsKey("container")) {

        		criteriaSize--;

        	}
        	
        

        

        	obj = new Object[criteriaSize];

        	if (criteria.containsKey("referenceNumber")) {

        		if (!firstArgument) {

        			jbossQl.append("AND ");

        		} else {

        			firstArgument = false;
        			jbossQl.append("WHERE ");

        		}

        		jbossQl.append("dv.arSalesOrder.soReferenceNumber LIKE '%" + (String)criteria.get("referenceNumber") + "%' ");

        	}
        	
        	
        	obj = new Object[criteriaSize];

        	if (criteria.containsKey("container")) {

        		if (!firstArgument) {

        			jbossQl.append("AND ");

        		} else {

        			firstArgument = false;
        			jbossQl.append("WHERE ");

        		}

        		jbossQl.append("dv.dvContainer LIKE '%" + (String)criteria.get("container") + "%' ");

        	}

        
        	

        	if (criteria.containsKey("transactionStatus")) {

        		if (!firstArgument) {

        			jbossQl.append("AND ");

        		} else {

        			firstArgument = false;
        			jbossQl.append("WHERE ");

        		}

        		jbossQl.append("dv.arSalesOrder.soTransactionStatus = '" + (String)criteria.get("transactionStatus") + "' ");

        	}




        	if (criteria.containsKey("customerCode")) {

			   	  if (!firstArgument) {
			   	     jbossQl.append("AND ");
			   	  } else {
			   	  	 firstArgument = false;
			   	  	 jbossQl.append("WHERE ");
			   	  }

			   	  jbossQl.append("dv.arSalesOrder.arCustomer.cstCustomerCode=?" + (ctr+1) + " ");
			   	  obj[ctr] = (String)criteria.get("customerCode");
			   	  ctr++;

	        }

	        if (!firstArgument) {
	  	 	  jbossQl.append("AND ");
	  	    } else {
	  	 	  firstArgument = false;
	  	 	  jbossQl.append("WHERE ");
	  	    }

	  	    jbossQl.append("dv.arSalesOrder.soVoid=?" + (ctr+1) + " ");
	  	    obj[ctr] = (Byte)criteria.get("salesOrderVoid");
	        ctr++;

	       

	    

	      if (criteria.containsKey("posted")) {

	       	  if (!firstArgument) {

	       	     jbossQl.append("AND ");

	       	  } else {

	       	  	 firstArgument = false;
	       	  	 jbossQl.append("WHERE ");

	       	  }

	       	  jbossQl.append("dv.arSalesOrder.soPosted=?" + (ctr+1) + " ");

	       	  String posted = (String)criteria.get("posted");

	       	  if (posted.equals("YES")) {

	       	  	obj[ctr] = new Byte(EJBCommon.TRUE);

	       	  } else {

	       	  	obj[ctr] = new Byte(EJBCommon.FALSE);

	       	  }

	       	  ctr++;

	      }

        	if (criteria.containsKey("documentNumberFrom")) {

        		if (!firstArgument) {

        			jbossQl.append("AND ");

        		} else {

        			firstArgument = false;
        			jbossQl.append("WHERE ");

        		}

        		jbossQl.append("dv.arSalesOrder.soDocumentNumber>=?" + (ctr+1) + " ");
        		obj[ctr] = (String)criteria.get("documentNumberFrom");
        		ctr++;

        	}

        	if (criteria.containsKey("documentNumberTo")) {

        		if (!firstArgument) {

        			jbossQl.append("AND ");

        		} else {

        			firstArgument = false;
        			jbossQl.append("WHERE ");

        		}

        		jbossQl.append("dv.arSalesOrder.soDocumentNumber<=?" + (ctr+1) + " ");
        		obj[ctr] = (String)criteria.get("documentNumberTo");
        		ctr++;

        	}

        	if (criteria.containsKey("deliveryNumberFrom")) {

        		if (!firstArgument) {

        			jbossQl.append("AND ");

        		} else {

        			firstArgument = false;
        			jbossQl.append("WHERE ");

        		}

        		jbossQl.append("dv.dvDeliveryNumber>=?" + (ctr+1) + " ");
        		obj[ctr] = (String)criteria.get("deliveryNumberFrom");
        		ctr++;

        	}

        	if (criteria.containsKey("deliveryNumberTo")) {

        		if (!firstArgument) {

        			jbossQl.append("AND ");

        		} else {

        			firstArgument = false;
        			jbossQl.append("WHERE ");

        		}

        		jbossQl.append("dv.dvDeliveryNumber<=?" + (ctr+1) + " ");
        		obj[ctr] = (String)criteria.get("deliveryNumberTo");
        		ctr++;

        	}

        	if (!firstArgument) {

        		jbossQl.append("AND ");

        	} else {

        		firstArgument = false;
        		jbossQl.append("WHERE ");

        	}

        	//e

        	jbossQl.append("dv.arSalesOrder.soAdBranch=" + AD_BRNCH + " AND dv.arSalesOrder.soAdCompany=" + AD_CMPNY + " ");

        	if(isChild){

        		jbossQl.append(" AND dv.arSalesOrder.soLock=0 ");

        	}

        	String orderBy = null;

        	if(ORDER_BY==null) {
        		ORDER_BY = "CUSTOMER CODE";
        	}
        	
        	
        	if (ORDER_BY.equals("CUSTOMER CODE")) {

		  	  orderBy = "dv.arSalesOrder.arCustomer.cstCustomerCode";

		    } else if (ORDER_BY.equals("DOCUMENT NUMBER")) {

		  	  orderBy = "dv.arSalesOrder.soDocumentNumber";

		    } else if (ORDER_BY.equals("REFERENCE NUMBER")) {

			  	  orderBy = "dv.arSalesOrder.soReferenceNumber";

		    } else if (ORDER_BY.equals("CONTAINER")) {

			  	  orderBy = "dv.dvContainer";

		    }  else if (ORDER_BY.equals("DELIVERY NUMBER")) {

			  	  orderBy = "dv.dvDeliveryNumber";

		    } else {
		    	  orderBy = "dv.arSalesOrder.arCustomer.cstCustomerCode";
		    }

        	if (orderBy != null) {

        		jbossQl.append("ORDER BY " + orderBy + ", dv.arSalesOrder.soDate");

        	} else {

        		jbossQl.append("ORDER BY dv.arSalesOrder.soDate");

        	}

        	jbossQl.append(" OFFSET ?" + (ctr + 1));
        	obj[ctr] = OFFSET;
        	ctr++;

        	jbossQl.append(" LIMIT ?" + (ctr + 1));
        	obj[ctr] = LIMIT;
        	ctr++;

       
        	
        	System.out.println("sql is: " + jbossQl.toString());
        	System.out.println("size obj is: " + obj.length);
        	for(int x=0;x<obj.length;x++) {
        		System.out.println("obj is: " + obj[x].toString());
        	}
        	Collection arDeliverys = arDeliveryHome.getDvByCriteria(jbossQl.toString(), obj);
    
        	if (arDeliverys.size() <= 0)
        		throw new GlobalNoRecordFoundException();

        	Iterator i = arDeliverys.iterator();

        	while (i.hasNext()) {
        		
        		LocalArDelivery arDelivery = (LocalArDelivery)i.next();

        		ArModDeliveryDetails details = new ArModDeliveryDetails();
        		
        		
        		details.setDvCode(arDelivery.getDvCode());
        		details.setSoCode(arDelivery.getArSalesOrder().getSoCode());
				details.setDvContainer(arDelivery.getDvContainer());
				details.setDvDeliveryNumber(arDelivery.getDvDeliveryNumber());
				details.setDvBookingTime(arDelivery.getDvBookingTime());
				details.setDvTabsFeePullOut(arDelivery.getDvTabsFeePullOut());
				details.setDvReleasedDate(arDelivery.getDvReleasedDate());
				details.setDvDeliveredDate(arDelivery.getDvDeliveredDate());
				details.setDvDeliveredTo(arDelivery.getDvDeliveredTo());
				details.setDvPlateNo(arDelivery.getDvPlateNo());
				details.setDvDriverName(arDelivery.getDvDriverName());
				details.setDvEmptyReturnDate(arDelivery.getDvEmptyReturnDate());
				details.setDvEmptyReturnTo(arDelivery.getDvEmptyReturnTo());
				details.setDvTabsFeeReturn(arDelivery.getDvTabsFeeReturn());
				details.setDvStatus(arDelivery.getDvStatus());
				details.setDvRemarks(arDelivery.getDvRemarks());
				
				details.setCstName(arDelivery.getArSalesOrder().getArCustomer().getCstCustomerCode());
				details.setSoDocumentNumber(arDelivery.getArSalesOrder().getSoDocumentNumber());
				details.setSoReferenceNumber(arDelivery.getArSalesOrder().getSoReferenceNumber());
				details.setSoTransactionStatus(arDelivery.getArSalesOrder().getSoTransactionStatus());
				

        		list.add(details);

        	}

        	return list;

        } catch (GlobalNoRecordFoundException ex) {

	  	  throw ex;

	  } catch (Exception ex) {


	  	  ex.printStackTrace();
	  	  throw new EJBException(ex.getMessage());

	  }

    }

   

    /**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public byte getAdPrfArUseCustomerPulldown(Integer AD_CMPNY) {

		Debug.print("ArFindDeliveryControllerBean getAdPrfArUseCustomerPulldown");

		LocalAdPreferenceHome adPreferenceHome = null;

		// Initialize EJB Home

		try {

			adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}

		try {

			LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);

			return  adPreference.getPrfArUseCustomerPulldown();

		} catch (Exception ex) {

			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());

		}

	}

	
	/**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList getArSoDocumentTypeList(Integer AD_CMPNY) {

        Debug.print("ArFindDeliveryEntryControllerBean getArSoDocumentTypeList");

        ArrayList list = new ArrayList();
        LocalAdLookUpValueHome adLookUpValueHome = null;


        // Initialize EJB Home

        try {

        	adLookUpValueHome = (LocalAdLookUpValueHome)EJBHomeFactory.
            lookUpLocalHome(LocalAdLookUpValueHome.JNDI_NAME, LocalAdLookUpValueHome.class);

        } catch (NamingException ex) {

            throw new EJBException(ex.getMessage());

        }


        try {

        	Collection adLookUpValues = adLookUpValueHome.findByLuName("AR SALES ORDER DOCUMENT TYPE", AD_CMPNY);

        	if(adLookUpValues.size() <=0 ) {
        		return list;
        	}

        	Iterator i = adLookUpValues.iterator();

        	while(i.hasNext()) {

        		LocalAdLookUpValue adLookUpValue = (LocalAdLookUpValue)i.next();
        		System.out.println(adLookUpValue.getLvName());
        		list.add(adLookUpValue.getLvName());
        	}


        	return list;



        } catch (Exception ex) {

            Debug.printStackTrace(ex);
            throw new EJBException(ex.getMessage());

        }

    }
	
	
	


    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList getArSoTransactionTypeList(Integer AD_CMPNY) {

        Debug.print("ArFindDeliveryEntryControllerBean getArDeliveryType");

        ArrayList list = new ArrayList();
        LocalAdLookUpValueHome adLookUpValueHome = null;


        // Initialize EJB Home

        try {

        	adLookUpValueHome = (LocalAdLookUpValueHome)EJBHomeFactory.
            lookUpLocalHome(LocalAdLookUpValueHome.JNDI_NAME, LocalAdLookUpValueHome.class);

        } catch (NamingException ex) {

            throw new EJBException(ex.getMessage());

        }


        try {

        	Collection adLookUpValues = adLookUpValueHome.findByLuName("AR SALES ORDER TRANSACTION TYPE", AD_CMPNY);

        	if(adLookUpValues.size() <=0 ) {
        		return list;
        	}

        	Iterator i = adLookUpValues.iterator();

        	while(i.hasNext()) {

        		LocalAdLookUpValue adLookUpValue = (LocalAdLookUpValue)i.next();
        		System.out.println(adLookUpValue.getLvName());
        		list.add(adLookUpValue.getLvName());
        	}


        	return list;



        } catch (Exception ex) {

            Debug.printStackTrace(ex);
            throw new EJBException(ex.getMessage());

        }

    }

    
	
	/**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
	public ArrayList getAdLvTransactionStatusAll(Integer AD_CMPNY) {

		Debug.print("ArDeliveryEntryControllerBean getAdLvTransactionStatusAll");

		LocalAdLookUpValueHome adLookUpValueHome = null;

		ArrayList list = new ArrayList();

		// Initialize EJB Home

		try {

			adLookUpValueHome = (LocalAdLookUpValueHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdLookUpValueHome.JNDI_NAME, LocalAdLookUpValueHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}

		try {

			String lookUpName = "AR SALES ORDER TRANSACTION STATUS";



			Collection adLookUpValues = adLookUpValueHome.findByLuName(lookUpName, AD_CMPNY);

			Iterator i = adLookUpValues.iterator();

			while (i.hasNext()) {

				LocalAdLookUpValue adLookUpValue = (LocalAdLookUpValue)i.next();

				list.add(adLookUpValue.getLvName());

			}

			return list;

		} catch (Exception ex) {

			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());

		}

	}
	

    // SessionBean methods

    /**
     * @ejb:create-method view-type="remote"
     **/
    public void ejbCreate() throws CreateException {

       Debug.print("ApFindDeliveryControllerBean ejbCreate");

    }
}
