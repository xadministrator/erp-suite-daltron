
/*
 * ArFindInvoiceBatchControllerBean.java
 *
 * Created on May 20, 2004, 2:36 PM
 *
 * @author  Neil Andrew M. Ajero
 */

package com.ejb.txn;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.naming.NamingException;

import com.ejb.ar.LocalArInvoiceBatch;
import com.ejb.ar.LocalArInvoiceBatchHome;
import com.ejb.exception.GlobalNoRecordFoundException;
import com.util.AbstractSessionBean;
import com.util.ArInvoiceBatchDetails;
import com.util.Debug;
import com.util.EJBHomeFactory;

/**
 * @ejb:bean name="ArFindInvoiceBatchControllerEJB"
 *           display-name="Used for searching invoice batches"
 *           type="Stateless"
 *           view-type="remote"
 *           jndi-name="ejb/ArFindInvoiceBatchControllerEJB"
 *
 * @ejb:interface remote-class="com.ejb.txn.ArFindInvoiceBatchController"
 *                extends="javax.ejb.EJBObject"
 *
 * @ejb:home remote-class="com.ejb.txn.ArFindInvoiceBatchControllerHome"
 *           extends="javax.ejb.EJBHome"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="aruser"
 *                        role-link="aruserlink"
 *
 * @ejb:permission role-name="aruser"
 * 
*/

public class ArFindInvoiceBatchControllerBean extends AbstractSessionBean {


   
   /**
   * @ejb:interface-method view-type="remote"
   * @jboss:method-attributes read-only="true"
   **/
   public ArrayList getArIbByCriteria(HashMap criteria, String ORDER_BY, Integer OFFSET, Integer LIMIT, Integer AD_BRNCH, Integer AD_CMPNY)
      throws GlobalNoRecordFoundException {

      Debug.print("ArFindInvoiceBatchControllerBean getArIbByCriteria");
      
      LocalArInvoiceBatchHome arInvoiceBatchHome = null;
      
      // Initialize EJB Home
        
      try {
      	
          arInvoiceBatchHome = (LocalArInvoiceBatchHome)EJBHomeFactory.
              lookUpLocalHome(LocalArInvoiceBatchHome.JNDI_NAME, LocalArInvoiceBatchHome.class);
           
      } catch (NamingException ex) {
            
          throw new EJBException(ex.getMessage());
            
      }
      
      try {
  
	      ArrayList list = new ArrayList();
	      
	      StringBuffer jbossQl = new StringBuffer();
	      jbossQl.append("SELECT OBJECT(ib) FROM ArInvoiceBatch ib ");
	      
	      boolean firstArgument = true;
	      short ctr = 0;
	      Object obj[];
	      
	       // Allocate the size of the object parameter
	
	       
	      if (criteria.containsKey("batchName")) {
	      	
	      	 obj = new Object[(criteria.size()-1) + 2];
	      	 
	      } else {
	      	
	      	 obj = new Object[criteria.size() + 2];
		
	      }
	      
	  
	         
	      
	      if (criteria.containsKey("batchName")) {
	      	
	      	 if (!firstArgument) {
	      	 
	      	    jbossQl.append("AND ");	
	      	 	
	         } else {
	         	
	         	firstArgument = false;
	         	jbossQl.append("WHERE ");
	         	
	         }
	         
	      	 jbossQl.append("ib.ibName LIKE '%" + (String)criteria.get("batchName") + "%' ");
	      	 
	      }
	      	 
	        
	      if (criteria.containsKey("status")) {
		      	
		  	 if (!firstArgument) {
		  	 	jbossQl.append("AND ");
		  	 } else {
		  	 	firstArgument = false;
		  	 	jbossQl.append("WHERE ");
		  	 }
		  	 jbossQl.append("ib.ibStatus=?" + (ctr+1) + " ");
		  	 obj[ctr] = (String)criteria.get("status");
		  	 ctr++;
		  }  
		  
		  if (criteria.containsKey("dateCreated")) {
		      	
		  	 if (!firstArgument) {
		  	 	jbossQl.append("AND ");
		  	 } else {
		  	 	firstArgument = false;
		  	 	jbossQl.append("WHERE ");
		  	 }
		  	 jbossQl.append("ib.ibDateCreated=?" + (ctr+1) + " ");
		  	 obj[ctr] = (Date)criteria.get("dateCreated");
		  	 ctr++;
		  } 
		  
		  if (!firstArgument) {
       	  	
       	     jbossQl.append("AND ");
       	     
       	  } else {
       	  	
       	  	 firstArgument = false;
       	  	 jbossQl.append("WHERE ");
       	  	 
       	  }
       	  
       	  jbossQl.append("ib.ibAdBranch=" + AD_BRNCH + " AND ib.ibAdCompany=" + AD_CMPNY + " ");
		      
	         
	      String orderBy = null;
		      
		  if (ORDER_BY.equals("BATCH NAME")) {
		
		  	  orderBy = "ib.ibName";
		  	  
		  } else if (ORDER_BY.equals("DATE CREATED")) {
		
		  	  orderBy = "ib.ibDateCreated";
		  	
		  }
		  
		  if (orderBy != null) {
		  
		  	jbossQl.append("ORDER BY " + orderBy);
		  	
		  }
	               
	      jbossQl.append(" OFFSET ?" + (ctr + 1));
	      obj[ctr] = OFFSET;
	      ctr++;
	      
	      jbossQl.append(" LIMIT ?" + (ctr + 1));
	      obj[ctr] = LIMIT;
	      
	      System.out.println("QL + " + jbossQl);

	      Collection arInvoiceBatches = arInvoiceBatchHome.getIbByCriteria(jbossQl.toString(), obj);
	         
	      if (arInvoiceBatches.isEmpty())
	         throw new GlobalNoRecordFoundException();
	         
	      Iterator i = arInvoiceBatches.iterator();
	      while (i.hasNext()) {
	      	      	
	         LocalArInvoiceBatch arInvoiceBatch = (LocalArInvoiceBatch) i.next();
	         
	         ArInvoiceBatchDetails details = new ArInvoiceBatchDetails();
	         
	         details.setIbCode(arInvoiceBatch.getIbCode());
	         details.setIbName(arInvoiceBatch.getIbName());
	         details.setIbDescription(arInvoiceBatch.getIbDescription());
	         details.setIbStatus(arInvoiceBatch.getIbStatus());
	         details.setIbDateCreated(arInvoiceBatch.getIbDateCreated());
	         details.setIbCreatedBy(arInvoiceBatch.getIbCreatedBy());
	         
	         list.add(details);
	      	
	      }
	         
	      return list;
  
	   } catch (GlobalNoRecordFoundException ex) {
		  	 
		  	  throw ex;
		  	
	   } catch (Exception ex) {
	  	
	
		  	  ex.printStackTrace();
		  	  throw new EJBException(ex.getMessage());
		  	
	   }
    
   }
   
   
   /**
    * @ejb:interface-method view-type="remote"
    * @jboss:method-attributes read-only="true"
    **/
    public Integer getArIbSizeByCriteria(HashMap criteria, Integer AD_BRNCH, Integer AD_CMPNY)
       throws GlobalNoRecordFoundException {

       Debug.print("ArFindInvoiceBatchControllerBean getArIbSizeByCriteria");
       
       LocalArInvoiceBatchHome arInvoiceBatchHome = null;
       
       // Initialize EJB Home
         
       try {
       	
           arInvoiceBatchHome = (LocalArInvoiceBatchHome)EJBHomeFactory.
               lookUpLocalHome(LocalArInvoiceBatchHome.JNDI_NAME, LocalArInvoiceBatchHome.class);
            
       } catch (NamingException ex) {
             
           throw new EJBException(ex.getMessage());
             
       }
       
       try {
   
 	      StringBuffer jbossQl = new StringBuffer();
 	      jbossQl.append("SELECT OBJECT(ib) FROM ArInvoiceBatch ib ");
 	      
 	      boolean firstArgument = true;
 	      short ctr = 0;
 	      Object obj[];
 	      
 	       // Allocate the size of the object parameter
 	
 	       
 	      if (criteria.containsKey("batchName")) {
 	      	
 	      	 obj = new Object[(criteria.size()-1)];
 	      	 
 	      } else {
 	      	
 	      	 obj = new Object[criteria.size()];
 		
 	      }
 	      
 	      
 	      if (criteria.containsKey("batchName")) {
 	      	
 	      	 if (!firstArgument) {
 	      	 
 	      	    jbossQl.append("AND ");	
 	      	 	
 	         } else {
 	         	
 	         	firstArgument = false;
 	         	jbossQl.append("WHERE ");
 	         	
 	         }
 	         
 	      	 jbossQl.append("ib.ibName LIKE '%" + (String)criteria.get("batchName") + "%' ");
 	      	 
 	      }
 	      	 
 	        
 	      if (criteria.containsKey("status")) {
 		      	
 		  	 if (!firstArgument) {
 		  	 	jbossQl.append("AND ");
 		  	 } else {
 		  	 	firstArgument = false;
 		  	 	jbossQl.append("WHERE ");
 		  	 }
 		  	 jbossQl.append("ib.ibStatus=?" + (ctr+1) + " ");
 		  	 obj[ctr] = (String)criteria.get("status");
 		  	 ctr++;
 		  }  
 		  
 		  if (criteria.containsKey("dateCreated")) {
 		      	
 		  	 if (!firstArgument) {
 		  	 	jbossQl.append("AND ");
 		  	 } else {
 		  	 	firstArgument = false;
 		  	 	jbossQl.append("WHERE ");
 		  	 }
 		  	 jbossQl.append("ib.ibDateCreated=?" + (ctr+1) + " ");
 		  	 obj[ctr] = (Date)criteria.get("dateCreated");
 		  	 ctr++;
 		  } 
 		  
 		  if (!firstArgument) {
 		  	
 		  	jbossQl.append("AND ");
 		  	
 		  } else {
 		  	
 		  	firstArgument = false;
 		  	jbossQl.append("WHERE ");
 		  	
 		  }
        	  
 		  jbossQl.append("ib.ibAdBranch=" + AD_BRNCH + " AND ib.ibAdCompany=" + AD_CMPNY + " ");
 		      
 	      Collection arInvoiceBatches = arInvoiceBatchHome.getIbByCriteria(jbossQl.toString(), obj);
 	         
 	      if (arInvoiceBatches.isEmpty())
 	         throw new GlobalNoRecordFoundException();
 	         
 	      return new Integer(arInvoiceBatches.size());
   
 	   } catch (GlobalNoRecordFoundException ex) {
 		  	 
 		  	  throw ex;
 		  	
 	   } catch (Exception ex) {
 	  	
 	
 		  	  ex.printStackTrace();
 		  	  throw new EJBException(ex.getMessage());
 		  	
 	   }
     
    }
   
   // SessionBean methods

   /**
    * @ejb:create-method view-type="remote"
    **/
   public void ejbCreate() throws CreateException {

      Debug.print("ArInvoiceBatchControllerBean ejbCreate");
      
   }

   // private methods

     
}
