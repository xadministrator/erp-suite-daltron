package com.ejb.txn;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.naming.NamingException;

import com.ejb.ad.LocalAdLookUpValue;
import com.ejb.ad.LocalAdLookUpValueHome;
import com.ejb.ad.LocalAdPreference;
import com.ejb.ad.LocalAdPreferenceHome;
import com.ejb.ar.LocalArCustomer;
import com.ejb.ar.LocalArCustomerHome;
import com.ejb.ar.LocalArJobOrder;
import com.ejb.ar.LocalArJobOrderHome;
import com.ejb.ar.LocalArJobOrderType;
import com.ejb.ar.LocalArJobOrderTypeHome;
import com.ejb.ar.LocalArSalesOrder;
import com.ejb.ar.LocalArSalesOrderHome;
import com.ejb.ar.LocalArSalesOrderInvoiceLine;
import com.ejb.ar.LocalArSalesOrderLine;
import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.gl.LocalGlFunctionalCurrency;
import com.ejb.gl.LocalGlFunctionalCurrencyHome;
import com.util.AbstractSessionBean;
import com.util.ArModJobOrderDetails;
import com.util.ArModSalesOrderDetails;
import com.util.Debug;
import com.util.EJBCommon;
import com.util.EJBHomeFactory;

/**
 * @ejb:bean name="ArFindJobOrderControllerEJB"
 *           display-name="Used for finding job orders"
 *           type="Stateless"
 *           view-type="remote"
 *           jndi-name="ejb/ArFindJobOrderControllerEJB"
 *
 * @ejb:interface remote-class="com.ejb.txn.ArFindJobOrderController"
 *                extends="javax.ejb.EJBObject"
 *
 * @ejb:home remote-class="com.ejb.txn.ArFindJobOrderControllerHome"
 *           extends="javax.ejb.EJBHome"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="invuser"
 *                        role-link="invuserlink"
 *
 * @ejb:permission role-name="invuser"
 *
 */

public class ArFindJobOrderControllerBean extends AbstractSessionBean {

    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList getArCstAll(Integer AD_BRNCH, Integer AD_CMPNY) {

        Debug.print("ArFindJobOrderControllerBean getArCstAll");

        LocalArCustomerHome arCustomerHome = null;

        ArrayList list = new ArrayList();

        // Initialize EJB Home

        try {

            arCustomerHome = (LocalArCustomerHome)EJBHomeFactory.
                lookUpLocalHome(LocalArCustomerHome.JNDI_NAME, LocalArCustomerHome.class);

        } catch (NamingException ex) {

            throw new EJBException(ex.getMessage());

        }

        try {

            Collection arCustomers = arCustomerHome.findEnabledCstAll(AD_BRNCH, AD_CMPNY);

	        Iterator i = arCustomers.iterator();

	        while (i.hasNext()) {

	        	LocalArCustomer arCustomer = (LocalArCustomer)i.next();

	        	list.add(arCustomer.getCstCustomerCode());

	        }

	        return list;

        } catch (Exception ex) {

        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());

        }

    }


    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList getArJoForProcessingByBrCode(Integer AD_BRNCH, Integer AD_CMPNY) {

        Debug.print("ArFindJobOrderControllerBean getArJoForProcessingByBrCode");

        LocalArJobOrderHome arJobOrderHome = null;

        ArrayList list = new ArrayList();

        // Initialize EJB Home

        try {

        	arJobOrderHome = (LocalArJobOrderHome)EJBHomeFactory.
                lookUpLocalHome(LocalArJobOrderHome.JNDI_NAME, LocalArJobOrderHome.class);

        } catch (NamingException ex) {

            throw new EJBException(ex.getMessage());

        }

        try {

            Collection arJobOrderColl = arJobOrderHome.findPostedJoByBrCode(AD_BRNCH, AD_CMPNY);

	        Iterator i = arJobOrderColl.iterator();

	        while (i.hasNext()) {

	        	LocalArJobOrder arJobOrder = (LocalArJobOrder)i.next();


	        	if (!((arJobOrder.getJoApprovalStatus().equals("APPROVED") || arJobOrder.getJoApprovalStatus().equals("N/A")) &&
	        			arJobOrder.getJoLock()==(byte)(0))) continue;

        		ArModJobOrderDetails mdetails = new ArModJobOrderDetails();
        		mdetails.setJoCode(arJobOrder.getJoCode());
                mdetails.setJoCstCustomerCode(arJobOrder.getArCustomer().getCstCustomerCode());
                mdetails.setJoCstName(arJobOrder.getArCustomer().getCstName());
                mdetails.setJoDate(arJobOrder.getJoDate());
                mdetails.setJoDocumentNumber(arJobOrder.getJoDocumentNumber());
                mdetails.setJoReferenceNumber(arJobOrder.getJoReferenceNumber());
                mdetails.setJoTransactionType(arJobOrder.getJoTransactionType());
                mdetails.setJoTechnician(arJobOrder.getJoTechnician());

        		list.add(mdetails);

	        }

	        return list;

        } catch (Exception ex) {

        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());

        }

    }

    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList getGlFcAll(Integer AD_CMPNY) {

        Debug.print("ApFindJobOrderControllerBean getGlFcAll");

        LocalGlFunctionalCurrencyHome glFunctionalCurrencyHome = null;

        ArrayList list = new ArrayList();

        // Initialize EJB Home

        try {

            glFunctionalCurrencyHome = (LocalGlFunctionalCurrencyHome)EJBHomeFactory.
                lookUpLocalHome(LocalGlFunctionalCurrencyHome.JNDI_NAME, LocalGlFunctionalCurrencyHome.class);

        } catch (NamingException ex) {

            throw new EJBException(ex.getMessage());

        }

        try {

            Collection glFunctionalCurrencies = glFunctionalCurrencyHome.findFcAll(AD_CMPNY);

	        Iterator i = glFunctionalCurrencies.iterator();

	        while (i.hasNext()) {

	        	LocalGlFunctionalCurrency glFunctionalCurrency = (LocalGlFunctionalCurrency)i.next();

	        	list.add(glFunctionalCurrency.getFcName());

	        }

	        return list;

        } catch (Exception ex) {

        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());

        }

    }

    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList getArJoByCriteria(HashMap criteria, boolean isChild,
        Integer OFFSET, Integer LIMIT, String ORDER_BY, Integer AD_BRNCH, Integer AD_CMPNY)
        throws GlobalNoRecordFoundException {

        Debug.print("ArFindJobOrderControllerBean getArJoByCriteria");

        LocalArJobOrderHome arJobOrderHome = null;

        ArrayList list = new ArrayList();

        //initialized EJB Home

        try {

        	arJobOrderHome = (LocalArJobOrderHome)EJBHomeFactory.
                lookUpLocalHome(LocalArJobOrderHome.JNDI_NAME, LocalArJobOrderHome.class);

        } catch (NamingException ex) {

            throw new EJBException(ex.getMessage());

        }

        try {

        	StringBuffer jbossQl = new StringBuffer();
        	jbossQl.append("SELECT OBJECT(jo) FROM ArJobOrder jo ");

        	boolean firstArgument = true;
        	short ctr = 0;
        	int criteriaSize = criteria.size() + 2;

        	Object obj[];

        	// Allocate the size of the object parameter
        	if (criteria.containsKey("jobOrderType")) {

        		criteriaSize--;

        	}
        	
        	if (criteria.containsKey("referenceNumber")) {

        		criteriaSize--;

        	}

        	if (criteria.containsKey("transactionType")) {

        		criteriaSize--;

        	}
        	
        	
        	if (criteria.containsKey("technician")) {

        		criteriaSize--;

        	}
        	
        	
        	if (criteria.containsKey("jobOrderStatus")) {

        		criteriaSize--;

        	}

        	if (criteria.containsKey("approvalStatus")) {

		      	 String approvalStatus = (String)criteria.get("approvalStatus");

		      	 if (approvalStatus.equals("DRAFT") || approvalStatus.equals("REJECTED")) {

		      	 	 criteriaSize--;

		      	 }

	       }

        	obj = new Object[criteriaSize];
        	
        	
        	if (criteria.containsKey("jobOrderType")) {

        		if (!firstArgument) {

        			jbossQl.append("AND ");

        		} else {

        			firstArgument = false;
        			jbossQl.append("WHERE ");

        		}

        		jbossQl.append("jo.arJobOrderType.jotName = '" + (String)criteria.get("jobOrderType") + "' ");

        	}
        	
        	

        	if (criteria.containsKey("referenceNumber")) {

        		if (!firstArgument) {

        			jbossQl.append("AND ");

        		} else {

        			firstArgument = false;
        			jbossQl.append("WHERE ");

        		}

        		jbossQl.append("jo.joReferenceNumber LIKE '%" + (String)criteria.get("referenceNumber") + "%' ");

        	}
        	
        	

        	if (criteria.containsKey("jobOrderStatus")) {

        		if (!firstArgument) {

        			jbossQl.append("AND ");

        		} else {

        			firstArgument = false;
        			jbossQl.append("WHERE ");

        		}

        		jbossQl.append("jo.joJobOrderStatus LIKE '%" + (String)criteria.get("jobOrderStatus") + "%' ");

        	}

        	if (criteria.containsKey("transactionType")) {

        		if (!firstArgument) {

        			jbossQl.append("AND ");

        		} else {

        			firstArgument = false;
        			jbossQl.append("WHERE ");

        		}

        		jbossQl.append("jo.joTransactionType = '" + (String)criteria.get("transactionType") + "' ");

        	}
        	
        	
        	if (criteria.containsKey("technician")) {

        		if (!firstArgument) {

        			jbossQl.append("AND ");

        		} else {

        			firstArgument = false;
        			jbossQl.append("WHERE ");

        		}

        		jbossQl.append("jo.joTechnician = '" + (String)criteria.get("technician") + "' ");

        	}



        	if (criteria.containsKey("customerCode")) {

			   	  if (!firstArgument) {
			   	     jbossQl.append("AND ");
			   	  } else {
			   	  	 firstArgument = false;
			   	  	 jbossQl.append("WHERE ");
			   	  }

			   	  jbossQl.append("jo.arCustomer.cstCustomerCode=?" + (ctr+1) + " ");
			   	  obj[ctr] = (String)criteria.get("customerCode");
			   	  ctr++;

	        }

	        if (!firstArgument) {
	  	 	  jbossQl.append("AND ");
	  	    } else {
	  	 	  firstArgument = false;
	  	 	  jbossQl.append("WHERE ");
	  	    }

	  	    jbossQl.append("jo.joVoid=?" + (ctr+1) + " ");
	  	    obj[ctr] = (Byte)criteria.get("jobOrderVoid");
	        ctr++;

	        if (criteria.containsKey("currency")) {

	       	  if (!firstArgument) {

	       	     jbossQl.append("AND ");

	       	  } else {

	       	  	 firstArgument = false;
	       	  	 jbossQl.append("WHERE ");

	       	  }

	       	  jbossQl.append("jo.glFunctionalCurrency.fcName=?" + (ctr+1) + " ");
	       	  obj[ctr] = (String)criteria.get("currency");
	       	  ctr++;

	      }

	      if (criteria.containsKey("approvalStatus")) {

	       	  if (!firstArgument) {

	       	     jbossQl.append("AND ");

	       	  } else {

	       	  	 firstArgument = false;
	       	  	 jbossQl.append("WHERE ");

	       	  }

	       	  String approvalStatus = (String)criteria.get("approvalStatus");

	       	  if (approvalStatus.equals("DRAFT")) {

		       	  jbossQl.append("jo.joApprovalStatus IS NULL ");

	       	  } else if (approvalStatus.equals("REJECTED")) {

		       	  jbossQl.append("jo.joReasonForRejection IS NOT NULL ");

	      	  } else {

		      	  jbossQl.append("jo.joApprovalStatus=?" + (ctr+1) + " ");
		       	  obj[ctr] = approvalStatus;
		       	  ctr++;

	      	  }

	      }


	      if (criteria.containsKey("posted")) {

	       	  if (!firstArgument) {

	       	     jbossQl.append("AND ");

	       	  } else {

	       	  	 firstArgument = false;
	       	  	 jbossQl.append("WHERE ");

	       	  }

	       	  jbossQl.append("jo.joPosted=?" + (ctr+1) + " ");

	       	  String posted = (String)criteria.get("posted");

	       	  if (posted.equals("YES")) {

	       	  	obj[ctr] = new Byte(EJBCommon.TRUE);

	       	  } else {

	       	  	obj[ctr] = new Byte(EJBCommon.FALSE);

	       	  }

	       	  ctr++;

	      }

        	if (criteria.containsKey("documentNumberFrom")) {

        		if (!firstArgument) {

        			jbossQl.append("AND ");

        		} else {

        			firstArgument = false;
        			jbossQl.append("WHERE ");

        		}

        		jbossQl.append("jo.joDocumentNumber>=?" + (ctr+1) + " ");
        		obj[ctr] = (String)criteria.get("documentNumberFrom");
        		ctr++;

        	}

        	if (criteria.containsKey("documentNumberTo")) {

        		if (!firstArgument) {

        			jbossQl.append("AND ");

        		} else {

        			firstArgument = false;
        			jbossQl.append("WHERE ");

        		}

        		jbossQl.append("jo.joDocumentNumber<=?" + (ctr+1) + " ");
        		obj[ctr] = (String)criteria.get("documentNumberTo");
        		ctr++;

        	}

        	if (criteria.containsKey("dateFrom")) {

        		if (!firstArgument) {
        			jbossQl.append("AND ");
        		} else {
        			firstArgument = false;
        			jbossQl.append("WHERE ");
        		}
        		jbossQl.append("jo.joDate>=?" + (ctr+1) + " ");
        		obj[ctr] = (Date)criteria.get("dateFrom");
        		ctr++;
        	}

        	if (criteria.containsKey("dateTo")) {

        		if (!firstArgument) {
        			jbossQl.append("AND ");
        		} else {
        			firstArgument = false;
        			jbossQl.append("WHERE ");
        		}
        		jbossQl.append("jo.joDate<=?" + (ctr+1) + " ");
        		obj[ctr] = (Date)criteria.get("dateTo");
        		ctr++;

        	}

//s

        	if (criteria.containsKey("approvalDateFrom")) {

        		if (!firstArgument) {
        			jbossQl.append("AND ");
        		} else {
        			firstArgument = false;
        			jbossQl.append("WHERE ");
        		}
        		jbossQl.append("jo.joDateApprovedRejected>=?" + (ctr+1) + " ");
        		obj[ctr] = (Date)criteria.get("approvalDateFrom");
        		Date sample1 = (Date)criteria.get("approvalDateFrom");
        		System.out.print("we: " + obj[ctr].toString());
        		ctr++;
        		System.out.print(sample1.toString());
        	}

        	if (criteria.containsKey("approvalDateTo")) {

        		if (!firstArgument) {
        			jbossQl.append("AND ");
        		} else {
        			firstArgument = false;
        			jbossQl.append("WHERE ");
        		}
        		jbossQl.append("jo.joDateApprovedRejected<=?" + (ctr+1) + " ");
        		obj[ctr] = (Date)criteria.get("approvalDateTo");
        		ctr++;

        	}

        	if (!firstArgument) {

        		jbossQl.append("AND ");

        	} else {

        		firstArgument = false;
        		jbossQl.append("WHERE ");

        	}

        	//e
        	System.out.println("jo branch is: "+ AD_BRNCH);

        	jbossQl.append("jo.joAdBranch=" + AD_BRNCH + " AND jo.joAdCompany=" + AD_CMPNY + " ");

        	if(isChild){

        		jbossQl.append(" AND jo.joLock=0 ");

        	}

        	String orderBy = null;

        	if (ORDER_BY.equals("CUSTOMER CODE")) {

		  	  orderBy = "jo.arCustomer.cstCustomerCode";

		    } else if (ORDER_BY.equals("DOCUMENT NUMBER")) {

		  	  orderBy = "jo.joDocumentNumber";

		    }

        	if (orderBy != null) {

        		jbossQl.append("ORDER BY " + orderBy + ", jo.joDate");

        	} else {

        		jbossQl.append("ORDER BY jo.joDate");

        	}

        	jbossQl.append(" OFFSET ?" + (ctr + 1));
        	obj[ctr] = OFFSET;
        	ctr++;

        	jbossQl.append(" LIMIT ?" + (ctr + 1));
        	obj[ctr] = LIMIT;
        	ctr++;

        	Collection arJobOrders = arJobOrderHome.getJOByCriteria(jbossQl.toString(), obj);

        	if (arJobOrders.size() == 0)
        		throw new GlobalNoRecordFoundException();

        	Iterator i = arJobOrders.iterator();

        	while (i.hasNext()) {

        		LocalArJobOrder arJobOrder = (LocalArJobOrder)i.next();

        		// Check If Sales Order If Completely Delivered
        		/*if(isChild){

        			Collection arSalesOrderLines = arSalesOrder.getArSalesOrderLines();

        			boolean isOpenSO = false;

                    Iterator slIter = arSalesOrderLines.iterator();

                    while(slIter.hasNext()) {

                    	LocalArSalesOrderLine arSalesOrderLine = (LocalArSalesOrderLine) slIter.next();

                    	Iterator soInvLnIter = arSalesOrderLine.getArSalesOrderInvoiceLines().iterator();
                        double QUANTITY_SOLD = 0d;

                        while(soInvLnIter.hasNext()) {

                            LocalArSalesOrderInvoiceLine arSalesOrderInvoiceLine = (LocalArSalesOrderInvoiceLine) soInvLnIter.next();

                            if (arSalesOrderInvoiceLine.getArInvoice().getInvPosted() == EJBCommon.TRUE) {

                                QUANTITY_SOLD += arSalesOrderInvoiceLine.getSilQuantityDelivered();

                            }

                        }

                        double TOTAL_REMAINING_QTY = arSalesOrderLine.getSolQuantity() - QUANTITY_SOLD;

                        if(TOTAL_REMAINING_QTY > 0) {
                        	isOpenSO = true;
                        	break;
                        }

                    }

                    if(!isOpenSO) continue;

        		}*/

        		ArModJobOrderDetails mdetails = new ArModJobOrderDetails();
        		mdetails.setJoCode(arJobOrder.getJoCode());
        		
                mdetails.setJoCstCustomerCode(arJobOrder.getArCustomer().getCstCustomerCode());
                mdetails.setJoCstName(arJobOrder.getArCustomer().getCstName());
                mdetails.setJoDate(arJobOrder.getJoDate());
                mdetails.setJoDocumentNumber(arJobOrder.getJoDocumentNumber());
                mdetails.setJoReferenceNumber(arJobOrder.getJoReferenceNumber());
                mdetails.setJoTransactionType(arJobOrder.getJoTransactionType());
                mdetails.setJoTechnician(arJobOrder.getJoTechnician());
                mdetails.setJoJobOrderStatus(arJobOrder.getJoJobOrderStatus());
                
                if(arJobOrder.getArJobOrderType()!=null) {
                	mdetails.setJoType(arJobOrder.getArJobOrderType().getJotName());
                }
                
        		list.add(mdetails);

        	}

        	return list;

        } catch (GlobalNoRecordFoundException ex) {

	  	  throw ex;

	  } catch (Exception ex) {


	  	  ex.printStackTrace();
	  	  throw new EJBException(ex.getMessage());

	  }

    }
    
    
    /**
   	 * @ejb:interface-method view-type="remote"
   	 **/
   	public ArrayList getAllJobOrderTypeName(Integer AD_CMPNY) {

   		Debug.print("ArFindJobOrderControllerBean getAllJobOrderType");

   		LocalArJobOrderTypeHome arJobOrderTypeHome = null;

   
   		
   		//Initialize EJB Home

   		try {

   			arJobOrderTypeHome = (LocalArJobOrderTypeHome)EJBHomeFactory.
   				lookUpLocalHome(LocalArJobOrderTypeHome.JNDI_NAME, LocalArJobOrderTypeHome.class);

   		} catch (NamingException ex) {

   			throw new EJBException(ex.getMessage());

   		}

   		try {

   			Collection arJobOrderTypes = arJobOrderTypeHome.findJotAll(AD_CMPNY);
   			ArrayList list = new ArrayList();


   			Iterator i = arJobOrderTypes.iterator();

   			while(i.hasNext()) {

   				LocalArJobOrderType arJobOrderType = (LocalArJobOrderType)i.next();

   				list.add(arJobOrderType.getJotName());

   			}

   			return list;

   		} catch (Exception ex) {

   			Debug.printStackTrace(ex);
   			throw new EJBException(ex.getMessage());

   		}

   	}


    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public Integer getArJoSizeByCriteria(HashMap criteria, Integer AD_BRNCH, Integer AD_CMPNY)
        throws GlobalNoRecordFoundException {

        Debug.print("ArFindJobOrderControllerBean getArJoSizeByCriteria");

        LocalArJobOrderHome arJobOrderHome = null;

        //initialized EJB Home

        try {

            arJobOrderHome = (LocalArJobOrderHome)EJBHomeFactory.
                lookUpLocalHome(LocalArJobOrderHome.JNDI_NAME, LocalArJobOrderHome.class);

        } catch (NamingException ex) {

            throw new EJBException(ex.getMessage());

        }

        try {

        	StringBuffer jbossQl = new StringBuffer();
        	jbossQl.append("SELECT OBJECT(jo) FROM ArJobOrder jo ");

        	boolean firstArgument = true;
        	short ctr = 0;
        	int criteriaSize = criteria.size();

        	Object obj[];

        	// Allocate the size of the object parameter

        	if (criteria.containsKey("referenceNumber")) {

        		criteriaSize--;

        	}

        	if (criteria.containsKey("transactionType")) {

        		criteriaSize--;

        	}
        	
        	if (criteria.containsKey("jobOrderStatus")) {

        		criteriaSize--;

        	}

        	if (criteria.containsKey("approvalStatus")) {

		      	 String approvalStatus = (String)criteria.get("approvalStatus");

		      	 if (approvalStatus.equals("DRAFT") || approvalStatus.equals("REJECTED")) {

		      	 	 criteriaSize--;

		      	 }

	       }

        	obj = new Object[criteriaSize];

        	if (criteria.containsKey("referenceNumber")) {

        		if (!firstArgument) {

        			jbossQl.append("AND ");

        		} else {

        			firstArgument = false;
        			jbossQl.append("WHERE ");

        		}

        		jbossQl.append("jo.joReferenceNumber LIKE '%" + (String)criteria.get("referenceNumber") + "%' ");

        	}
        	
        	
        	obj = new Object[criteriaSize];

        	if (criteria.containsKey("jobOrderStatus")) {

        		if (!firstArgument) {

        			jbossQl.append("AND ");

        		} else {

        			firstArgument = false;
        			jbossQl.append("WHERE ");

        		}

        		jbossQl.append("jo.joJobOrderStatus LIKE '%" + (String)criteria.get("jobOrderStatus") + "%' ");

        	}


        	if (criteria.containsKey("transactionType")) {

        		if (!firstArgument) {

        			jbossQl.append("AND ");

        		} else {

        			firstArgument = false;
        			jbossQl.append("WHERE ");

        		}

        		jbossQl.append("jo.joTransactionType  = '" + (String)criteria.get("transactionType") + "' ");

        	}

        	if (criteria.containsKey("customerCode")) {

			   	  if (!firstArgument) {
			   	     jbossQl.append("AND ");
			   	  } else {
			   	  	 firstArgument = false;
			   	  	 jbossQl.append("WHERE ");
			   	  }

			   	  jbossQl.append("jo.arCustomer.cstCustomerCode=?" + (ctr+1) + " ");
			   	  obj[ctr] = (String)criteria.get("customerCode");
			   	  ctr++;

	        }

	        if (!firstArgument) {
	  	 	  jbossQl.append("AND ");
	  	    } else {
	  	 	  firstArgument = false;
	  	 	  jbossQl.append("WHERE ");
	  	    }

	  	    jbossQl.append("jo.joVoid=?" + (ctr+1) + " ");
	  	    obj[ctr] = (Byte)criteria.get("jobOrderVoid");
	        ctr++;

	        if (criteria.containsKey("currency")) {

	       	  if (!firstArgument) {

	       	     jbossQl.append("AND ");

	       	  } else {

	       	  	 firstArgument = false;
	       	  	 jbossQl.append("WHERE ");

	       	  }

	       	  jbossQl.append("jo.glFunctionalCurrency.fcName=?" + (ctr+1) + " ");
	       	  obj[ctr] = (String)criteria.get("currency");
	       	  ctr++;

	      }

	      if (criteria.containsKey("approvalStatus")) {

	       	  if (!firstArgument) {

	       	     jbossQl.append("AND ");

	       	  } else {

	       	  	 firstArgument = false;
	       	  	 jbossQl.append("WHERE ");

	       	  }

	       	  String approvalStatus = (String)criteria.get("approvalStatus");

	       	  if (approvalStatus.equals("DRAFT")) {

		       	  jbossQl.append("jo.joApprovalStatus IS NULL ");

	       	  } else if (approvalStatus.equals("REJECTED")) {

		       	  jbossQl.append("jo.joReasonForRejection IS NOT NULL ");

	      	  } else {

		      	  jbossQl.append("jo.joApprovalStatus=?" + (ctr+1) + " ");
		       	  obj[ctr] = approvalStatus;
		       	  ctr++;

	      	  }

	      }


	      if (criteria.containsKey("posted")) {

	       	  if (!firstArgument) {

	       	     jbossQl.append("AND ");

	       	  } else {

	       	  	 firstArgument = false;
	       	  	 jbossQl.append("WHERE ");

	       	  }

	       	  jbossQl.append("jo.joPosted=?" + (ctr+1) + " ");

	       	  String posted = (String)criteria.get("posted");

	       	  if (posted.equals("YES")) {

	       	  	obj[ctr] = new Byte(EJBCommon.TRUE);

	       	  } else {

	       	  	obj[ctr] = new Byte(EJBCommon.FALSE);

	       	  }

	       	  ctr++;

	      }

        	if (criteria.containsKey("documentNumberFrom")) {

        		if (!firstArgument) {

        			jbossQl.append("AND ");

        		} else {

        			firstArgument = false;
        			jbossQl.append("WHERE ");

        		}

        		jbossQl.append("jo.joDocumentNumber>=?" + (ctr+1) + " ");
        		obj[ctr] = (String)criteria.get("documentNumberFrom");
        		ctr++;

        	}

        	if (criteria.containsKey("documentNumberTo")) {

        		if (!firstArgument) {

        			jbossQl.append("AND ");

        		} else {

        			firstArgument = false;
        			jbossQl.append("WHERE ");

        		}

        		jbossQl.append("jo.joDocumentNumber<=?" + (ctr+1) + " ");
        		obj[ctr] = (String)criteria.get("documentNumberTo");
        		ctr++;

        	}

        	if (criteria.containsKey("dateFrom")) {

        		if (!firstArgument) {
        			jbossQl.append("AND ");
        		} else {
        			firstArgument = false;
        			jbossQl.append("WHERE ");
        		}
        		jbossQl.append("jo.joDate>=?" + (ctr+1) + " ");
        		obj[ctr] = (Date)criteria.get("dateFrom");
        		ctr++;
        	}

        	if (criteria.containsKey("dateTo")) {

        		if (!firstArgument) {
        			jbossQl.append("AND ");
        		} else {
        			firstArgument = false;
        			jbossQl.append("WHERE ");
        		}
        		jbossQl.append("jo.joDate<=?" + (ctr+1) + " ");
        		obj[ctr] = (Date)criteria.get("dateTo");
        		ctr++;

        	}

//s

        	if (criteria.containsKey("approvalDateFrom")) {

        		if (!firstArgument) {
        			jbossQl.append("AND ");
        		} else {
        			firstArgument = false;
        			jbossQl.append("WHERE ");
        		}
        		jbossQl.append("jo.joDateApprovedRejected>=?" + (ctr+1) + " ");
        		obj[ctr] = (Date)criteria.get("approvalDateFrom");
        		Date sample1 = (Date)criteria.get("approvalDateFrom");
        		System.out.print("we: " + obj[ctr].toString());
        		ctr++;
        		System.out.print(sample1.toString());
        	}

        	if (criteria.containsKey("approvalDateTo")) {

        		if (!firstArgument) {
        			jbossQl.append("AND ");
        		} else {
        			firstArgument = false;
        			jbossQl.append("WHERE ");
        		}
        		jbossQl.append("jo.joDateApprovedRejected<=?" + (ctr+1) + " ");
        		obj[ctr] = (Date)criteria.get("approvalDateTo");
        		ctr++;

        	}

        	if (!firstArgument) {

        		jbossQl.append("AND ");

        	} else {

        		firstArgument = false;
        		jbossQl.append("WHERE ");

        	}

        	jbossQl.append("jo.joAdBranch=" + AD_BRNCH + " AND jo.joAdCompany=" + AD_CMPNY + " ");

        	Collection arJobOrders = arJobOrderHome.getJOByCriteria(jbossQl.toString(), obj);

        	if (arJobOrders.size() == 0)
        		throw new GlobalNoRecordFoundException();

        	return new Integer(arJobOrders.size());

        } catch (GlobalNoRecordFoundException ex) {

	  	  throw ex;

	  } catch (Exception ex) {


	  	  ex.printStackTrace();
	  	  throw new EJBException(ex.getMessage());

	  }

    }

    /**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public byte getAdPrfArUseCustomerPulldown(Integer AD_CMPNY) {

		Debug.print("ArFindJobOrderControllerBean getAdPrfArUseCustomerPulldown");

		LocalAdPreferenceHome adPreferenceHome = null;

		// Initialize EJB Home

		try {

			adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}

		try {

			LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);

			return  adPreference.getPrfArUseCustomerPulldown();

		} catch (Exception ex) {

			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());

		}

	}



    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList getArJoTransactionTypeList(Integer AD_CMPNY) {

        Debug.print("ArFindJobOrderEntryControllerBean getArJoTransactionTypeList");

        ArrayList list = new ArrayList();
        LocalAdLookUpValueHome adLookUpValueHome = null;


        // Initialize EJB Home

        try {

        	adLookUpValueHome = (LocalAdLookUpValueHome)EJBHomeFactory.
            lookUpLocalHome(LocalAdLookUpValueHome.JNDI_NAME, LocalAdLookUpValueHome.class);

        } catch (NamingException ex) {

            throw new EJBException(ex.getMessage());

        }


        try {

        	Collection adLookUpValues = adLookUpValueHome.findByLuName("AR JOB ORDER TRANSACTION TYPE", AD_CMPNY);

        	if(adLookUpValues.size() <=0 ) {
        		return list;
        	}

        	Iterator i = adLookUpValues.iterator();

        	while(i.hasNext()) {

        		LocalAdLookUpValue adLookUpValue = (LocalAdLookUpValue)i.next();
        		System.out.println(adLookUpValue.getLvName());
        		list.add(adLookUpValue.getLvName());
        	}


        	return list;



        } catch (Exception ex) {

            Debug.printStackTrace(ex);
            throw new EJBException(ex.getMessage());

        }

    }

    
    
    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList getAllTechnician(Integer AD_CMPNY) {

        Debug.print("ApFindSalesOrderControllerBean getAllTechnician");

        ArrayList list = new ArrayList();
        LocalAdLookUpValueHome adLookUpValueHome = null;


        // Initialize EJB Home

        try {

        	adLookUpValueHome = (LocalAdLookUpValueHome)EJBHomeFactory.
            lookUpLocalHome(LocalAdLookUpValueHome.JNDI_NAME, LocalAdLookUpValueHome.class);

        } catch (NamingException ex) {

            throw new EJBException(ex.getMessage());

        }


        try {

        	Collection adLookUpValues = adLookUpValueHome.findByLuName("AR JOB ORDER - TECHNICIANS", AD_CMPNY);

        	if(adLookUpValues.size() <=0 ) {
        		return list;
        	}

        	Iterator i = adLookUpValues.iterator();

        	while(i.hasNext()) {

        		LocalAdLookUpValue adLookUpValue = (LocalAdLookUpValue)i.next();
        		System.out.println(adLookUpValue.getLvName());
        		list.add(adLookUpValue.getLvName());
        	}


        	return list;



        } catch (Exception ex) {

            Debug.printStackTrace(ex);
            throw new EJBException(ex.getMessage());

        }

    }
    
    

    
    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
	public ArrayList getAdLvJobOrderStatus(Integer AD_CMPNY) {

		Debug.print("ApFindSalesOrderControllerBean getAdLvJobOrderStatus");

		LocalAdLookUpValueHome adLookUpValueHome = null;

		ArrayList list = new ArrayList();

		// Initialize EJB Home

		try {

			adLookUpValueHome = (LocalAdLookUpValueHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdLookUpValueHome.JNDI_NAME, LocalAdLookUpValueHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}

		try {

			String lookUpName = "AR JOB ORDER STATUS";



			Collection adLookUpValues = adLookUpValueHome.findByLuName(lookUpName, AD_CMPNY);

			Iterator i = adLookUpValues.iterator();

			while (i.hasNext()) {

				LocalAdLookUpValue adLookUpValue = (LocalAdLookUpValue)i.next();

				list.add(adLookUpValue.getLvName());

			}

			return list;

		} catch (Exception ex) {

			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());

		}

	}
	
    
    // SessionBean methods

    /**
     * @ejb:create-method view-type="remote"
     **/
    public void ejbCreate() throws CreateException {

       Debug.print("ApFindSalesOrderControllerBean ejbCreate");

    }
}
