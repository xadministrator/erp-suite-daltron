
/*
 * ArFindPdcControllerBean.java
 *
 * Created on June 30, 2005 11:58 AM
 *
 * @author  Enrico C. Yap
 */

package com.ejb.txn;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.naming.NamingException;

import com.ejb.ad.LocalAdCompany;
import com.ejb.ad.LocalAdCompanyHome;
import com.ejb.ad.LocalAdLookUpValue;
import com.ejb.ad.LocalAdLookUpValueHome;
import com.ejb.ad.LocalAdPreference;
import com.ejb.ad.LocalAdPreferenceHome;
import com.ejb.ar.LocalArCustomer;
import com.ejb.ar.LocalArCustomerHome;
import com.ejb.ar.LocalArPdc;
import com.ejb.ar.LocalArPdcHome;
import com.ejb.exception.GlobalNoRecordFoundException;
import com.util.AbstractSessionBean;
import com.util.ArModPdcDetails;
import com.util.Debug;
import com.util.EJBCommon;
import com.util.EJBHomeFactory;

/**
 * @ejb:bean name="ArFindPdcControllerEJB"
 *           display-name="Used for finding pdcs"
 *           type="Stateless"
 *           view-type="remote"
 *           jndi-name="ejb/ArFindPdcControllerEJB"
 *
 * @ejb:interface remote-class="com.ejb.txn.ArFindPdcController"
 *                extends="javax.ejb.EJBObject"
 *
 * @ejb:home remote-class="com.ejb.txn.ArFindPdcControllerHome"
 *           extends="javax.ejb.EJBHome"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="aruser"
 *                        role-link="aruserlink"
 *
 * @ejb:permission role-name="aruser"
 * 
*/

public class ArFindPdcControllerBean extends AbstractSessionBean {

    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList getArCstAll(Integer AD_BRNCH, Integer AD_CMPNY) {
                    
        Debug.print("ArFindPdcControllerBean getArCstAll");
        
        LocalArCustomerHome arCustomerHome = null;

        ArrayList list = new ArrayList();
        
        // Initialize EJB Home
        
        try {
            
            arCustomerHome = (LocalArCustomerHome)EJBHomeFactory.
                lookUpLocalHome(LocalArCustomerHome.JNDI_NAME, LocalArCustomerHome.class);
            
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {
            
            Collection arCustomers = arCustomerHome.findEnabledCstAll(AD_BRNCH, AD_CMPNY);

	        Iterator i = arCustomers.iterator();
	               
	        while (i.hasNext()) {
	        	
	        	LocalArCustomer arCustomer = (LocalArCustomer)i.next();
	        	
	        	list.add(arCustomer.getCstCustomerCode());
	        	
	        }
	        
	        return list;
            
        } catch (Exception ex) {
        	
        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());
        	
        }
            
    }
            
    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList getAdLvInvShiftAll(Integer AD_CMPNY) {
                    
        Debug.print("ArFindPdcControllerBean getAdLvInvShiftAll");
        
        LocalAdLookUpValueHome adLookUpValueHome = null;               
        
        ArrayList list = new ArrayList();
        
        // Initialize EJB Home
        
        try {
            
            adLookUpValueHome = (LocalAdLookUpValueHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdLookUpValueHome.JNDI_NAME, LocalAdLookUpValueHome.class);
            
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }        
        
        try {
        	
        	Collection adLookUpValues = adLookUpValueHome.findByLuName("INV SHIFT", AD_CMPNY);
        	
        	Iterator i = adLookUpValues.iterator();
        	
        	while (i.hasNext()) {
        		
        		LocalAdLookUpValue adLookUpValue = (LocalAdLookUpValue)i.next();
        		
        		list.add(adLookUpValue.getLvName());
        		
        	}
        	
        	return list;
        	
        } catch (Exception ex) {
        	
        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());
        	
        }
            
    }
     
    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
     public byte getAdPrfEnableInvShift(Integer AD_CMPNY) {

        Debug.print("ArFindPdcControllerBean getAdPrfEnableInvShift");
                   
        LocalAdPreferenceHome adPreferenceHome = null;
       
       
        // Initialize EJB Home
         
        try {
             
           adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
               lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);       
             
        } catch (NamingException ex) {
             
           throw new EJBException(ex.getMessage());
             
        }
       

        try {
       	
           LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);
          
           return adPreference.getPrfInvEnableShift();
          
        } catch (Exception ex) {
         	 
           Debug.printStackTrace(ex);
           throw new EJBException(ex.getMessage());
          
        }
       
     }    
	    
    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList getArPdcByCriteria(HashMap criteria,
        Integer OFFSET, Integer LIMIT, String ORDER_BY, Integer AD_BRNCH, Integer AD_CMPNY) 
        throws GlobalNoRecordFoundException {
                    
        Debug.print("ArFindPdcControllerBean getArPdcByCriteria");
        
        LocalArPdcHome arPdcHome = null;
        
        ArrayList list = new ArrayList();
        
        //initialized EJB Home
        
        try {
            
            arPdcHome = (LocalArPdcHome)EJBHomeFactory.
                lookUpLocalHome(LocalArPdcHome.JNDI_NAME, LocalArPdcHome.class);
            
        } catch (NamingException ex) 	{
            
            throw new EJBException(ex.getMessage());
            
        }
        
		try { 
           
		  StringBuffer jbossQl = new StringBuffer();
		  jbossQl.append("SELECT OBJECT(pdc) FROM ArPdc pdc ");
		  
	      boolean firstArgument = true;
	      short ctr = 0;
		  int criteriaSize = criteria.size() + 2;	      
	      
	      Object obj[];	      
		
		  // Allocate the size of the object parameter
		
	      if (criteria.containsKey("referenceNumber")) {
	      	
	      	 criteriaSize--;
	      	 
	      }
	      	
	      if (criteria.containsKey("checkNumber")) {
	      	
	      	 criteriaSize--;
	      	 
	      }	      	      
	      	      
	      obj = new Object[criteriaSize];
		     	      
		  if (criteria.containsKey("referenceNumber")) {
		  	
		  	 if (!firstArgument) {
		  	 
		  	    jbossQl.append("AND ");	
		  	 	
		     } else {
		     	
		     	firstArgument = false;
		     	jbossQl.append("WHERE ");
		     	
		     }
		     
		  	 jbossQl.append("pdc.pdcReferenceNumber LIKE '%" + (String)criteria.get("referenceNumber") + "%' ");
		  	 
		  }
		  
		  if (criteria.containsKey("checkNumber")) {
		  	
		  	 if (!firstArgument) {
		  	 
		  	    jbossQl.append("AND ");	
		  	 	
		     } else {
		     	
		     	firstArgument = false;
		     	jbossQl.append("WHERE ");
		     	
		     }
		     
		  	 jbossQl.append("pdc.pdcCheckNumber LIKE '%" + (String)criteria.get("checkNumber") + "%' ");
		  	 
		  }		  
		  
		  if (criteria.containsKey("shift")) {
		   	
		   	  if (!firstArgument) {		       	  	
		   	     jbossQl.append("AND ");		       	     
		   	  } else {		       	  	
		   	  	 firstArgument = false;
		   	  	 jbossQl.append("WHERE ");		       	  	 
		   	  }
		   	  
		   	  jbossQl.append("pdc.pdcLvShift=?" + (ctr+1) + " ");
		   	  obj[ctr] = (String)criteria.get("shift");
		   	  ctr++;
	   	  
	      }
		  	
		  if (criteria.containsKey("customerCode")) {
	   	
		   	  if (!firstArgument) {		       	  	
		   	     jbossQl.append("AND ");		       	     
		   	  } else {		       	  	
		   	  	 firstArgument = false;
		   	  	 jbossQl.append("WHERE ");		       	  	 
		   	  }
		   	  
		   	  jbossQl.append("pdc.arCustomer.cstCustomerCode=?" + (ctr+1) + " ");
		   	  obj[ctr] = (String)criteria.get("customerCode");
		   	  ctr++;
	   	  
	      }	
		  
	      if (!firstArgument) {
	  	 	jbossQl.append("AND ");
	  	  } else {
	  	 	firstArgument = false;
	  	 	jbossQl.append("WHERE ");
	  	  }
	  	 
	  	  jbossQl.append("pdc.pdcCancelled=?" + (ctr+1) + " ");
	  	  obj[ctr] = (Byte)criteria.get("cancelled");
	      ctr++;		     
	      
	      if (criteria.containsKey("dateReceivedFrom")) {
		      	
		  	 if (!firstArgument) {
		  	 	jbossQl.append("AND ");
		  	 } else {
		  	 	firstArgument = false;
		  	 	jbossQl.append("WHERE ");
		  	 }
		  	 jbossQl.append("pdc.pdcDateReceived>=?" + (ctr+1) + " ");
		  	 obj[ctr] = (Date)criteria.get("dateReceivedFrom");
		  	 ctr++;
		  }  
	            
		  if (criteria.containsKey("dateReceivedTo")) {
		  	
		  	 if (!firstArgument) {
		  	 	jbossQl.append("AND ");
		  	 } else {
		  	 	firstArgument = false;
		  	 	jbossQl.append("WHERE ");
		  	 }
		  	 jbossQl.append("pdc.pdcDateReceived<=?" + (ctr+1) + " ");
		  	 obj[ctr] = (Date)criteria.get("dateReceivedTo");
		  	 ctr++;
		  	 
		  }
		  	  
	      if (criteria.containsKey("maturityDateFrom")) {
	      	
			 if (!firstArgument) {
			 	jbossQl.append("AND ");
			 } else {
			 	firstArgument = false;
			 	jbossQl.append("WHERE ");
			 }
			 jbossQl.append("pdc.pdcMaturityDate>=?" + (ctr+1) + " ");
			 obj[ctr] = (Date)criteria.get("maturityDateFrom");
			  	 ctr++;
	      }  
	      	      
		  if (criteria.containsKey("maturityDateTo")) {
		
			 if (!firstArgument) {
			 	jbossQl.append("AND ");
			 } else {
			 	firstArgument = false;
			 	jbossQl.append("WHERE ");
			 }
			 jbossQl.append("pdc.pdcMaturityDate<=?" + (ctr+1) + " ");
			 obj[ctr] = (Date)criteria.get("maturityDateFrom");
			 ctr++;
		  	 
		  }		  
		    
	      if (criteria.containsKey("posted")) {
	       	
	       	  if (!firstArgument) {
	       	  	
	       	     jbossQl.append("AND ");
	       	     
	       	  } else {
	       	  	
	       	  	 firstArgument = false;
	       	  	 jbossQl.append("WHERE ");
	       	  	 
	       	  }
	       	  
	       	  jbossQl.append("pdc.pdcPosted=?" + (ctr+1) + " ");
	       	  
	       	  String posted = (String)criteria.get("posted");
	       	  
	       	  if (posted.equals("YES")) {
	       	  	
	       	  	obj[ctr] = new Byte(EJBCommon.TRUE);
	       	  	
	       	  } else {
	       	  	
	       	  	obj[ctr] = new Byte(EJBCommon.FALSE);
	       	  	
	       	  }       	  
	       	 
	       	  ctr++;
	       	  
	      }	      
	      
		  if (criteria.containsKey("status")) {				  	 
		      	
		     if (!firstArgument) {
		     	
		  	 	jbossQl.append("AND ");
		  	 	
		  	 } else {
		  	 	
		  	 	firstArgument = false;
		  	 	jbossQl.append("WHERE ");
		  	 	
		  	 }
		  	 
			 jbossQl.append("pdc.pdcStatus=?" + (ctr+1) + " ");
			 obj[ctr] = (String)criteria.get("status");
			 ctr++;
		  
		  }
		  
		  if (!firstArgument) {
       	  	
       	     jbossQl.append("AND ");
       	     
       	  } else {
       	  	
       	  	 firstArgument = false;
       	  	 jbossQl.append("WHERE ");
       	  	 
       	  }
       	  
       	  jbossQl.append("pdc.pdcAdBranch=" + AD_BRNCH + " AND pdc.pdcAdCompany=" + AD_CMPNY + " ");
			
		  String orderBy = null;
		  
		  if (ORDER_BY.equals("CUSTOMER CODE")) {	          
		      	      		
		  	  orderBy = "pdc.arCustomer.cstCustomerCode";

		  } else if (ORDER_BY.equals("CHECK NUMBER")) {
		
		  	  orderBy = "pdc.pdcCheckNumber";
		  	
		  }

	  	  if (orderBy != null) {
		
		  	  jbossQl.append("ORDER BY " + orderBy + ", pdc.pdcDateReceived");

		  } else {
		  	
		  	  jbossQl.append("ORDER BY pdc.pdcDateReceived");
		  	
		  }
		  
		  jbossQl.append(" OFFSET ?" + (ctr + 1));	        
		  obj[ctr] = OFFSET;	      
		  ctr++;
		  
		  jbossQl.append(" LIMIT ?" + (ctr + 1));
		  obj[ctr] = LIMIT;
		  ctr++;		      
	      		  
	      Collection arPdcs = arPdcHome.getPdcByCriteria(jbossQl.toString(), obj);	         
		  
		  if (arPdcs.size() == 0)
		     throw new GlobalNoRecordFoundException();
		     
		  Iterator i = arPdcs.iterator();
		
		  while (i.hasNext()) {
		  	
		  	  LocalArPdc arPdc = (LocalArPdc)i.next();   	  
		  	  
		  	  ArModPdcDetails mdetails = new ArModPdcDetails();
		  	  mdetails.setPdcCode(arPdc.getPdcCode());
		  	  mdetails.setPdcCstCustomerCode(arPdc.getArCustomer().getCstCustomerCode());
		  	  mdetails.setPdcDateReceived(arPdc.getPdcDateReceived());
		  	  mdetails.setPdcMaturityDate(arPdc.getPdcMaturityDate());
		  	  mdetails.setPdcCheckNumber(arPdc.getPdcCheckNumber());
		  	  mdetails.setPdcReferenceNumber(arPdc.getPdcReferenceNumber());
		  	  mdetails.setPdcAmount(arPdc.getPdcAmount());      	  
			      	  	      	  		     
			  list.add(mdetails);
			         
		  }
			     
		  return list;
		      
	  } catch (GlobalNoRecordFoundException ex) {
	  	 
	  	  throw ex;
	  	
	  } catch (Exception ex) {
	  	

	  	  ex.printStackTrace();
	  	  throw new EJBException(ex.getMessage());
	  	
	  }
        
    }
    
    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public Integer getArPdcSizeByCriteria(HashMap criteria, Integer AD_BRNCH, Integer AD_CMPNY) 
        throws GlobalNoRecordFoundException {
                    
        Debug.print("ArFindPdcControllerBean getArPdcSizeByCriteria");
        
        LocalArPdcHome arPdcHome = null;
        
        //initialized EJB Home
        
        try {
            
            arPdcHome = (LocalArPdcHome)EJBHomeFactory.
                lookUpLocalHome(LocalArPdcHome.JNDI_NAME, LocalArPdcHome.class);
            
        } catch (NamingException ex) 	{
            
            throw new EJBException(ex.getMessage());
            
        }
        
		try { 
           
		  StringBuffer jbossQl = new StringBuffer();
		  jbossQl.append("SELECT OBJECT(pdc) FROM ArPdc pdc ");
		  
	      boolean firstArgument = true;
	      short ctr = 0;
		  int criteriaSize = criteria.size();	      
	      
	      Object obj[];	      
		
		  // Allocate the size of the object parameter
		
	      if (criteria.containsKey("referenceNumber")) {
	      	
	      	 criteriaSize--;
	      	 
	      }
	      	
	      if (criteria.containsKey("checkNumber")) {
	      	
	      	 criteriaSize--;
	      	 
	      }	      	      
	      	      
	      obj = new Object[criteriaSize];
		     	      
		  if (criteria.containsKey("referenceNumber")) {
		  	
		  	 if (!firstArgument) {
		  	 
		  	    jbossQl.append("AND ");	
		  	 	
		     } else {
		     	
		     	firstArgument = false;
		     	jbossQl.append("WHERE ");
		     	
		     }
		     
		  	 jbossQl.append("pdc.pdcReferenceNumber LIKE '%" + (String)criteria.get("referenceNumber") + "%' ");
		  	 
		  }
		  
		  if (criteria.containsKey("checkNumber")) {
		  	
		  	 if (!firstArgument) {
		  	 
		  	    jbossQl.append("AND ");	
		  	 	
		     } else {
		     	
		     	firstArgument = false;
		     	jbossQl.append("WHERE ");
		     	
		     }
		     
		  	 jbossQl.append("pdc.pdcCheckNumber LIKE '%" + (String)criteria.get("checkNumber") + "%' ");
		  	 
		  }		  
		  
		  if (criteria.containsKey("shift")) {
		   	
		   	  if (!firstArgument) {		       	  	
		   	     jbossQl.append("AND ");		       	     
		   	  } else {		       	  	
		   	  	 firstArgument = false;
		   	  	 jbossQl.append("WHERE ");		       	  	 
		   	  }
		   	  
		   	  jbossQl.append("pdc.pdcLvShift=?" + (ctr+1) + " ");
		   	  obj[ctr] = (String)criteria.get("shift");
		   	  ctr++;
	   	  
	      }
		  	
		  if (criteria.containsKey("customerCode")) {
	   	
		   	  if (!firstArgument) {		       	  	
		   	     jbossQl.append("AND ");		       	     
		   	  } else {		       	  	
		   	  	 firstArgument = false;
		   	  	 jbossQl.append("WHERE ");		       	  	 
		   	  }
		   	  
		   	  jbossQl.append("pdc.arCustomer.cstCustomerCode=?" + (ctr+1) + " ");
		   	  obj[ctr] = (String)criteria.get("customerCode");
		   	  ctr++;
	   	  
	      }	
		  
	      if (!firstArgument) {
	  	 	jbossQl.append("AND ");
	  	  } else {
	  	 	firstArgument = false;
	  	 	jbossQl.append("WHERE ");
	  	  }
	  	 
	  	  jbossQl.append("pdc.pdcCancelled=?" + (ctr+1) + " ");
	  	  obj[ctr] = (Byte)criteria.get("cancelled");
	      ctr++;		     
	      
	      if (criteria.containsKey("dateReceivedFrom")) {
		      	
		  	 if (!firstArgument) {
		  	 	jbossQl.append("AND ");
		  	 } else {
		  	 	firstArgument = false;
		  	 	jbossQl.append("WHERE ");
		  	 }
		  	 jbossQl.append("pdc.pdcDateReceived>=?" + (ctr+1) + " ");
		  	 obj[ctr] = (Date)criteria.get("dateReceivedFrom");
		  	 ctr++;
		  }  
	            
		  if (criteria.containsKey("dateReceivedTo")) {
		  	
		  	 if (!firstArgument) {
		  	 	jbossQl.append("AND ");
		  	 } else {
		  	 	firstArgument = false;
		  	 	jbossQl.append("WHERE ");
		  	 }
		  	 jbossQl.append("pdc.pdcDateReceived<=?" + (ctr+1) + " ");
		  	 obj[ctr] = (Date)criteria.get("dateReceivedTo");
		  	 ctr++;
		  	 
		  }
		  	  
	      if (criteria.containsKey("maturityDateFrom")) {
	      	
			 if (!firstArgument) {
			 	jbossQl.append("AND ");
			 } else {
			 	firstArgument = false;
			 	jbossQl.append("WHERE ");
			 }
			 jbossQl.append("pdc.pdcMaturityDate>=?" + (ctr+1) + " ");
			 obj[ctr] = (Date)criteria.get("maturityDateFrom");
			  	 ctr++;
	      }  
	      	      
		  if (criteria.containsKey("maturityDateTo")) {
		
			 if (!firstArgument) {
			 	jbossQl.append("AND ");
			 } else {
			 	firstArgument = false;
			 	jbossQl.append("WHERE ");
			 }
			 jbossQl.append("pdc.pdcMaturityDate<=?" + (ctr+1) + " ");
			 obj[ctr] = (Date)criteria.get("maturityDateFrom");
			 ctr++;
		  	 
		  }		  
		    
	      if (criteria.containsKey("posted")) {
	       	
	       	  if (!firstArgument) {
	       	  	
	       	     jbossQl.append("AND ");
	       	     
	       	  } else {
	       	  	
	       	  	 firstArgument = false;
	       	  	 jbossQl.append("WHERE ");
	       	  	 
	       	  }
	       	  
	       	  jbossQl.append("pdc.pdcPosted=?" + (ctr+1) + " ");
	       	  
	       	  String posted = (String)criteria.get("posted");
	       	  
	       	  if (posted.equals("YES")) {
	       	  	
	       	  	obj[ctr] = new Byte(EJBCommon.TRUE);
	       	  	
	       	  } else {
	       	  	
	       	  	obj[ctr] = new Byte(EJBCommon.FALSE);
	       	  	
	       	  }       	  
	       	 
	       	  ctr++;
	       	  
	      }	      
	      
		  if (criteria.containsKey("status")) {				  	 
		      	
		     if (!firstArgument) {
		     	
		  	 	jbossQl.append("AND ");
		  	 	
		  	 } else {
		  	 	
		  	 	firstArgument = false;
		  	 	jbossQl.append("WHERE ");
		  	 	
		  	 }
		  	 
			 jbossQl.append("pdc.pdcStatus=?" + (ctr+1) + " ");
			 obj[ctr] = (String)criteria.get("status");
			 ctr++;
		  
		  }
		  
		  if (!firstArgument) {
       	  	
       	     jbossQl.append("AND ");
       	     
       	  } else {
       	  	
       	  	 firstArgument = false;
       	  	 jbossQl.append("WHERE ");
       	  	 
       	  }
       	  
       	  jbossQl.append("pdc.pdcAdBranch=" + AD_BRNCH + " AND pdc.pdcAdCompany=" + AD_CMPNY + " ");
			
	      Collection arPdcs = arPdcHome.getPdcByCriteria(jbossQl.toString(), obj);	         
		  
		  if (arPdcs.size() == 0)
		     throw new GlobalNoRecordFoundException();
		     
		  return new Integer(arPdcs.size());
		      
	  } catch (GlobalNoRecordFoundException ex) {
	  	 
	  	  throw ex;
	  	
	  } catch (Exception ex) {
	  	

	  	  ex.printStackTrace();
	  	  throw new EJBException(ex.getMessage());
	  	
	  }
        
    }

   /**
    * @ejb:interface-method view-type="remote"
    * @jboss:method-attributes read-only="true"
    **/
    public short getGlFcPrecisionUnit(Integer AD_CMPNY) {

       Debug.print("ArFindPdcControllerBean getGlFcPrecisionUnit");

       	
       LocalAdCompanyHome adCompanyHome = null;
      
     
       // Initialize EJB Home
        
       try {
            
           adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
              lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);       
            
       } catch (NamingException ex) {
            
           throw new EJBException(ex.getMessage());
            
       }

       try {
       	
         LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
            
         return  adCompany.getGlFunctionalCurrency().getFcPrecision();
                                     
       } catch (Exception ex) {
       	 
       	 Debug.printStackTrace(ex);
         throw new EJBException(ex.getMessage());
         
       }

    }
    
    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public byte getAdPrfArUseCustomerPulldown(Integer AD_CMPNY) {
    	
    	Debug.print("ArFindPdcControllerBean getAdPrfArUseCustomerPulldown");
    	
    	LocalAdPreferenceHome adPreferenceHome = null;         
    	
    	// Initialize EJB Home
    	
    	try {
    		
    		adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);       
    		
    	} catch (NamingException ex) {
    		
    		throw new EJBException(ex.getMessage());
    		
    	}
    	
    	
    	try {
    		
    		LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);
    		
    		return adPreference.getPrfArUseCustomerPulldown();
    		
    	} catch (Exception ex) {
    		
    		Debug.printStackTrace(ex);
    		throw new EJBException(ex.getMessage());
    		
    	}
    	
    }

    // SessionBean methods

    /**
     * @ejb:create-method view-type="remote"
     **/
    public void ejbCreate() throws CreateException {

       Debug.print("ArFindPdcControllerBean ejbCreate");
      
    }
}
