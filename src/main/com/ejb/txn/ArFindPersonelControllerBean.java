
/*
 * ArFindPersonelControllerBean.java
 *
 * Created on March 5, 2019, 11:00 AM
 *
 * @author  Ruben P. Lamberte
 */

package com.ejb.txn;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;
import javax.naming.NamingException;

import com.ejb.ad.LocalAdBranchResponsibility;
import com.ejb.ad.LocalAdBranchResponsibilityHome;
import com.ejb.ad.LocalAdLookUpValue;
import com.ejb.ad.LocalAdLookUpValueHome;

import com.ejb.ar.LocalArPersonel;
import com.ejb.ar.LocalArPersonelHome;
import com.ejb.ar.LocalArPersonelType;
import com.ejb.ar.LocalArPersonelTypeHome;
import com.ejb.exception.GlobalNoRecordFoundException;
import com.util.AbstractSessionBean;
import com.util.AdBranchDetails;

import com.util.ArModPersonelDetails;
import com.util.Debug;
import com.util.EJBCommon;
import com.util.EJBHomeFactory;

/**
 * @ejb:bean name="ArFindPersonelControllerEJB"
 *           display-name="Used for finding personel"
 *           type="Stateless"
 *           view-type="remote"
 *           jndi-name="ejb/ArFindPersonelControllerEJB"
 *
 * @ejb:interface remote-class="com.ejb.txn.ArFindPersonelController"
 *                extends="javax.ejb.EJBObject"
 *
 * @ejb:home remote-class="com.ejb.txn.ArFindPersonelControllerHome"
 *           extends="javax.ejb.EJBHome"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="aruser"
 *                        role-link="aruserlink"
 *
 * @ejb:permission role-name="aruser"
 * 
*/

public class ArFindPersonelControllerBean extends AbstractSessionBean {
	
	

    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList getArPtNmAll(Integer AD_CMPNY) {
                    
        Debug.print("ArFindPersonelControllerBean getArPtNmAll");
        
        LocalArPersonelTypeHome arPersonelTypeHome = null;
      

        ArrayList list = new ArrayList();
        
        // Initialize EJB Home
        
        try {
            
            arPersonelTypeHome = (LocalArPersonelTypeHome)EJBHomeFactory.
                lookUpLocalHome(LocalArPersonelTypeHome.JNDI_NAME, LocalArPersonelTypeHome.class);
            
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {
            
            Collection arPersonelTypes = arPersonelTypeHome.findPtAll(AD_CMPNY);

	        Iterator i = arPersonelTypes.iterator();
	               
	        while (i.hasNext()) {
	        	
	        	LocalArPersonelType arPersonelType = (LocalArPersonelType)i.next();
	        	
	        	list.add(arPersonelType.getPtName());
	        	
	        }
	        
	        return list;
            
        } catch (Exception ex) {
        	
        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());
        	
        }
            
    }
    
 
    
    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList getArPeByCriteria(HashMap criteria,
        Integer OFFSET, Integer LIMIT, String ORDER_BY, Integer AD_CMPNY) 
        throws GlobalNoRecordFoundException {
                    
        Debug.print("ArFindPersonelControllerBean getArPeByCriteria");
        
        LocalArPersonelHome arPersonelHome = null;
        
        ArrayList list = new ArrayList();
        
        //initialized EJB Home
        
        try {
            
            arPersonelHome = (LocalArPersonelHome)EJBHomeFactory.
                lookUpLocalHome(LocalArPersonelHome.JNDI_NAME, LocalArPersonelHome.class);
            
        } catch (NamingException ex) 	{
            
            throw new EJBException(ex.getMessage());
            
        }
        
		try { 
           
		  StringBuffer jbossQl = new StringBuffer();
		  
		  
		  jbossQl.append("SELECT OBJECT(pe) FROM ArPersonel pe ");
			

		  boolean firstArgument = true;
		  short ctr = 0;
		  int criteriaSize = criteria.size();
		  
		  System.out.println("crit size:" + criteriaSize);
		  
		  
		  Object obj[] = null;		      
		
		  // Allocate the size of the object parameter
		
		   
		  if (criteria.containsKey("idNumber")) {
		  	
		  	 criteriaSize--;
		  	 
		  } 
		  
		  if (criteria.containsKey("personelName")) {
		  	
		  	 criteriaSize--;
		  	 
		  } 
		   
		
	
		  
		  obj = new Object[criteriaSize + 2];    
		       	      
		  if (criteria.containsKey("idNumber")) {
		  	
		  	 if (!firstArgument) {
		  	 
		  	    jbossQl.append("AND ");	
		  	 	
		     } else {
		     	
		     	firstArgument = false;
		     	jbossQl.append("WHERE ");
		     	
		     }
		     
		  	 jbossQl.append("pe.peIdNumber LIKE '%" + (String)criteria.get("idNumber") + "%' ");
		  	 
		  }


	      if (criteria.containsKey("personelName")) {
	  	
		  	 if (!firstArgument) {
		  	 	jbossQl.append("AND ");
		  	 } else {
		  	 	firstArgument = false;
		  	 	jbossQl.append("WHERE ");
		  	 }
	  	 
	   	     jbossQl.append("pe.peName LIKE '%" + (String)criteria.get("personelName") + "%' ");		
	  	 
	      } 
	      
	
	      
		
			
		  if (criteria.containsKey("personelType")) {
	   	
		   	  if (!firstArgument) {		       	  	
		   	     jbossQl.append("AND ");		       	     
		   	  } else {		       	  	
		   	  	 firstArgument = false;
		   	  	 jbossQl.append("WHERE ");		       	  	 
		   	  }
		   	  
		   	  jbossQl.append("pe.arPersonelType.ptName=?" + (ctr+1) + " ");
		   	  obj[ctr] = (String)criteria.get("personelType");
		   	  ctr++;
	   	  
	      }	
		
		 if (!firstArgument) {
		  	 
	  	    jbossQl.append("AND ");	
	  	 	
	     } else {
	     	
	     	firstArgument = false;
	     	jbossQl.append("WHERE ");
	     	
	     }
		     
			  
		
       	  jbossQl.append("pe.peAdCompany=" + AD_CMPNY + " ");
		     	      			
		  String orderBy = null;
		  
		  if (ORDER_BY.equals("ID NUMBER")) {	          
		      	      		
		  	  orderBy = "pe.peIdNumber";
		  	
		  } else {
		  	
		  	  orderBy = "pe.peName";
		
		  } 
		
		  jbossQl.append("ORDER BY " + orderBy);
		  
		  jbossQl.append(" OFFSET ?" + (ctr + 1));	        
		  obj[ctr] = OFFSET;	      
		  ctr++;
		  
		  jbossQl.append(" LIMIT ?" + (ctr + 1));
		  obj[ctr] = LIMIT;
		  ctr++;		      
		  
		  System.out.println("obj size: " + obj.length);
		  
		  System.out.println("ctr size: " + ctr);
			  	      	
		  System.out.println(jbossQl.toString());
		  
	      Collection arPersonels = arPersonelHome.getPeByCriteria(jbossQl.toString(), obj);	         
		  
		  if (arPersonels.size() == 0)
		     throw new GlobalNoRecordFoundException();
		     
		  Iterator i = arPersonels.iterator();
		
		  while (i.hasNext()) {
		  	
			  LocalArPersonel arPersonel = (LocalArPersonel)i.next();
	
		  	  ArModPersonelDetails mdetails = new ArModPersonelDetails();
		  	  
		  	  

		  	  mdetails.setPeCode(arPersonel.getPeCode());
		  	  mdetails.setPeIdNumber(arPersonel.getPeIdNumber());
		  	  mdetails.setPeName(arPersonel.getPeName());
		  	  mdetails.setPeDescription(arPersonel.getPeDescription());
		  	  mdetails.setPeAddress(arPersonel.getPeAddress());
		  	  mdetails.setPeAdCompany(arPersonel.getPeAdCompany());
		  	  mdetails.setPePtShortName(arPersonel.getArPersonelType()==null?"":arPersonel.getArPersonelType().getPtShortName());
		  	  mdetails.setPePtName(arPersonel.getArPersonelType()==null?"":arPersonel.getArPersonelType().getPtName());
			  list.add(mdetails);
			  			         
		  }
			     
		  return list;
		      
	  } catch (GlobalNoRecordFoundException ex) {
	  	 
	  	  throw ex;
	  	
	  } catch (Exception ex) {
	  	

	  	  ex.printStackTrace();
	  	  throw new EJBException(ex.getMessage());
	  	
	  }
        
    }
    
    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public Integer getArPeSizeByCriteria(HashMap criteria,  Integer AD_CMPNY) 
        throws GlobalNoRecordFoundException {
                    
        Debug.print("ArFindPersonelControllerBean getArPeSizeByCriteria");
        
     
        LocalArPersonelHome arPersonelHome = null;
        //initialized EJB Home
        
        try {
            
        	arPersonelHome = (LocalArPersonelHome)EJBHomeFactory.
                lookUpLocalHome(LocalArPersonelHome.JNDI_NAME, LocalArPersonelHome.class);
            
        } catch (NamingException ex) 	{
            
            throw new EJBException(ex.getMessage());
            
        }
        
		try { 
           
			StringBuffer jbossQl = new StringBuffer();
			  
			  
			  jbossQl.append("SELECT OBJECT(pe) FROM ArPersonel pe ");
				

			  boolean firstArgument = true;
			  short ctr = 0;
			  int criteriaSize = criteria.size();
			  
			  
			  Object obj[] = null;		      
			
			  // Allocate the size of the object parameter
			
			   
			  if (criteria.containsKey("idNumber")) {
			  	
			  	 criteriaSize--;
			  	 
			  } 
			  
			  if (criteria.containsKey("personelName")) {
			  	
			  	 criteriaSize--;
			  	 
			  } 
			   
			
		
			  
			  obj = new Object[criteriaSize];    
			       	      
			  if (criteria.containsKey("idNumber")) {
			  	
			  	 if (!firstArgument) {
			  	 
			  	    jbossQl.append("AND ");	
			  	 	
			     } else {
			     	
			     	firstArgument = false;
			     	jbossQl.append("WHERE ");
			     	
			     }
			     
			  	 jbossQl.append("pe.peIdNumber LIKE '%" + (String)criteria.get("idNumber") + "%' ");
			  	 
			  }


		      if (criteria.containsKey("personelName")) {
		  	
			  	 if (!firstArgument) {
			  	 	jbossQl.append("AND ");
			  	 } else {
			  	 	firstArgument = false;
			  	 	jbossQl.append("WHERE ");
			  	 }
		  	 
		   	     jbossQl.append("pe.peName LIKE '%" + (String)criteria.get("personelName") + "%' ");		
		  	 
		      } 
		      
		
		      
			
				
			  if (criteria.containsKey("personelType")) {
		   	
			   	  if (!firstArgument) {		       	  	
			   	     jbossQl.append("AND ");		       	     
			   	  } else {		       	  	
			   	  	 firstArgument = false;
			   	  	 jbossQl.append("WHERE ");		       	  	 
			   	  }
			   	  
			   	  jbossQl.append("pe.arPersonelType.ptName=?" + (ctr+1) + " ");
			   	  obj[ctr] = (String)criteria.get("personelType");
			   	  ctr++;
		   	  
		      }	
			
			
				  
			
	       	  jbossQl.append("pe.peAdCompany=" + AD_CMPNY + " ");
		     	      			
		  System.out.println(jbossQl.toString());
		  
	      Collection arPersonels = arPersonelHome.getPeByCriteria(jbossQl.toString(), obj);	         
		  
		  if (arPersonels.size() == 0)
		     throw new GlobalNoRecordFoundException();
		 	     
		  return new Integer(arPersonels.size());
		      
	  } catch (GlobalNoRecordFoundException ex) {
	  	 
	  	  throw ex;
	  	
	  } catch (Exception ex) {
	  	

	  	  ex.printStackTrace();
	  	  throw new EJBException(ex.getMessage());
	  	
	  }
        
    }

  
    // SessionBean methods

    /**
     * @ejb:create-method view-type="remote"
     **/
    public void ejbCreate() throws CreateException {

       Debug.print("ArFindPersonelControllerBean ejbCreate");
      
    }
}
