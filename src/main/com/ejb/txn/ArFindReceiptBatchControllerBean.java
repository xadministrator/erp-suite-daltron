
/*
 * ArFindReceiptBatchControllerBean.java
 *
 * Created on May 20, 2004, 4:47 PM
 *
 * @author  Neil Andrew M. Ajero
 */

package com.ejb.txn;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.naming.NamingException;

import com.ejb.ar.LocalArReceiptBatch;
import com.ejb.ar.LocalArReceiptBatchHome;
import com.ejb.exception.GlobalNoRecordFoundException;
import com.util.AbstractSessionBean;
import com.util.ArReceiptBatchDetails;
import com.util.Debug;
import com.util.EJBHomeFactory;

/**
 * @ejb:bean name="ArFindReceiptBatchControllerEJB"
 *           display-name="Used for searching receipt batches"
 *           type="Stateless"
 *           view-type="remote"
 *           jndi-name="ejb/ArFindReceiptBatchControllerEJB"
 *
 * @ejb:interface remote-class="com.ejb.txn.ArFindReceiptBatchController"
 *                extends="javax.ejb.EJBObject"
 *
 * @ejb:home remote-class="com.ejb.txn.ArFindReceiptBatchControllerHome"
 *           extends="javax.ejb.EJBHome"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="aruser"
 *                        role-link="aruserlink"
 *
 * @ejb:permission role-name="aruser"
 * 
*/

public class ArFindReceiptBatchControllerBean extends AbstractSessionBean {


   
   /**
   * @ejb:interface-method view-type="remote"
   * @jboss:method-attributes read-only="true"
   **/
   public ArrayList getArRbByCriteria(HashMap criteria, String ORDER_BY, Integer OFFSET, Integer LIMIT, Integer AD_BRNCH, Integer AD_CMPNY)
      throws GlobalNoRecordFoundException {

      Debug.print("ArFindReceiptBatchControllerBean getArRbByCriteria");
      
      LocalArReceiptBatchHome arReceiptBatchHome = null;
      
      // Initialize EJB Home
        
      try {
      	
          arReceiptBatchHome = (LocalArReceiptBatchHome)EJBHomeFactory.
              lookUpLocalHome(LocalArReceiptBatchHome.JNDI_NAME, LocalArReceiptBatchHome.class);
           
      } catch (NamingException ex) {
            
          throw new EJBException(ex.getMessage());
            
      }
      
      try {
  
	      ArrayList list = new ArrayList();
	      
	      StringBuffer jbossQl = new StringBuffer();
	      jbossQl.append("SELECT OBJECT(rb) FROM ArReceiptBatch rb ");
	      
	      boolean firstArgument = true;
	      short ctr = 0;
	      Object obj[];
	      
	       // Allocate the size of the object parameter
	
	       
	      if (criteria.containsKey("batchName")) {
	      	
	      	 obj = new Object[(criteria.size()-1) + 2];
	      	 
	      } else {
	      	
	      	 obj = new Object[criteria.size() + 2];
		
	      }
	      
	  
	         
	      
	      if (criteria.containsKey("batchName")) {
	      	
	      	 if (!firstArgument) {
	      	 
	      	    jbossQl.append("AND ");	
	      	 	
	         } else {
	         	
	         	firstArgument = false;
	         	jbossQl.append("WHERE ");
	         	
	         }
	         
	      	 jbossQl.append("rb.rbName LIKE '%" + (String)criteria.get("batchName") + "%' ");
	      	 
	      }
	      	 
	        
	      if (criteria.containsKey("status")) {
		      	
		  	 if (!firstArgument) {
		  	 	jbossQl.append("AND ");
		  	 } else {
		  	 	firstArgument = false;
		  	 	jbossQl.append("WHERE ");
		  	 }
		  	 jbossQl.append("rb.rbStatus=?" + (ctr+1) + " ");
		  	 obj[ctr] = (String)criteria.get("status");
		  	 ctr++;
		  }  
		  
		  if (criteria.containsKey("dateCreated")) {
		      	
		  	 if (!firstArgument) {
		  	 	jbossQl.append("AND ");
		  	 } else {
		  	 	firstArgument = false;
		  	 	jbossQl.append("WHERE ");
		  	 }
		  	 jbossQl.append("rb.rbDateCreated=?" + (ctr+1) + " ");
		  	 obj[ctr] = (Date)criteria.get("dateCreated");
		  	 ctr++;
		  } 
		  
		  if (!firstArgument) {
       	  	
       	     jbossQl.append("AND ");
       	     
       	  } else {
       	  	
       	  	 firstArgument = false;
       	  	 jbossQl.append("WHERE ");
       	  	 
       	  }
       	  
       	  jbossQl.append("rb.rbAdBranch=" + AD_BRNCH + " AND rb.rbAdCompany=" + AD_CMPNY + " ");
		      
	         
	      String orderBy = null;
		      
		  if (ORDER_BY.equals("BATCH NAME")) {
		
		  	  orderBy = "rb.rbName";
		  	  
		  } else if (ORDER_BY.equals("DATE CREATED")) {
		
		  	  orderBy = "rb.rbDateCreated";
		  	
		  }
		  
		  if (orderBy != null) {
		  
		  	jbossQl.append("ORDER BY " + orderBy);
		  	
		  }
	               
	      jbossQl.append(" OFFSET ?" + (ctr + 1));
	      obj[ctr] = OFFSET;
	      ctr++;
	      
	      jbossQl.append(" LIMIT ?" + (ctr + 1));
	      obj[ctr] = LIMIT;
	      
	      System.out.println("QL + " + jbossQl);

	      Collection arReceiptBatches = arReceiptBatchHome.getRbByCriteria(jbossQl.toString(), obj);
	         
	      if (arReceiptBatches.isEmpty())
	         throw new GlobalNoRecordFoundException();
	         
	      Iterator i = arReceiptBatches.iterator();
	      while (i.hasNext()) {
	      	      	
	         LocalArReceiptBatch arReceiptBatch = (LocalArReceiptBatch) i.next();
	         
	         ArReceiptBatchDetails details = new ArReceiptBatchDetails();
	         
	         details.setRbCode(arReceiptBatch.getRbCode());
	         details.setRbName(arReceiptBatch.getRbName());
	         details.setRbDescription(arReceiptBatch.getRbDescription());
	         details.setRbStatus(arReceiptBatch.getRbStatus());
	         details.setRbDateCreated(arReceiptBatch.getRbDateCreated());
	         details.setRbCreatedBy(arReceiptBatch.getRbCreatedBy());
	         
	         list.add(details);
	      	
	      }
	         
	      return list;
  
	   } catch (GlobalNoRecordFoundException ex) {
		  	 
		  	  throw ex;
		  	
	   } catch (Exception ex) {
	  	
	
		  	  ex.printStackTrace();
		  	  throw new EJBException(ex.getMessage());
		  	
	   }
    
   }
   
   /**
    * @ejb:interface-method view-type="remote"
    * @jboss:method-attributes read-only="true"
    **/
    public Integer getArRbSizeByCriteria(HashMap criteria, Integer AD_BRNCH, Integer AD_CMPNY)
       throws GlobalNoRecordFoundException {

       Debug.print("ArFindReceiptBatchControllerBean getArRbSizeByCriteria");
       
       LocalArReceiptBatchHome arReceiptBatchHome = null;
       
       // Initialize EJB Home
         
       try {
       	
           arReceiptBatchHome = (LocalArReceiptBatchHome)EJBHomeFactory.
               lookUpLocalHome(LocalArReceiptBatchHome.JNDI_NAME, LocalArReceiptBatchHome.class);
            
       } catch (NamingException ex) {
             
           throw new EJBException(ex.getMessage());
             
       }
       
       try {
   
 	      StringBuffer jbossQl = new StringBuffer();
 	      jbossQl.append("SELECT OBJECT(rb) FROM ArReceiptBatch rb ");
 	      
 	      boolean firstArgument = true;
 	      short ctr = 0;
 	      Object obj[];
 	      
 	       // Allocate the size of the object parameter
 	
 	       
 	      if (criteria.containsKey("batchName")) {
 	      	
 	      	 obj = new Object[(criteria.size()-1)];
 	      	 
 	      } else {
 	      	
 	      	 obj = new Object[criteria.size()];
 		
 	      }
 	      
 	  
 	         
 	      
 	      if (criteria.containsKey("batchName")) {
 	      	
 	      	 if (!firstArgument) {
 	      	 
 	      	    jbossQl.append("AND ");	
 	      	 	
 	         } else {
 	         	
 	         	firstArgument = false;
 	         	jbossQl.append("WHERE ");
 	         	
 	         }
 	         
 	      	 jbossQl.append("rb.rbName LIKE '%" + (String)criteria.get("batchName") + "%' ");
 	      	 
 	      }
 	      	 
 	        
 	      if (criteria.containsKey("status")) {
 		      	
 		  	 if (!firstArgument) {
 		  	 	jbossQl.append("AND ");
 		  	 } else {
 		  	 	firstArgument = false;
 		  	 	jbossQl.append("WHERE ");
 		  	 }
 		  	 jbossQl.append("rb.rbStatus=?" + (ctr+1) + " ");
 		  	 obj[ctr] = (String)criteria.get("status");
 		  	 ctr++;
 		  }  
 		  
 		  if (criteria.containsKey("dateCreated")) {
 		      	
 		  	 if (!firstArgument) {
 		  	 	jbossQl.append("AND ");
 		  	 } else {
 		  	 	firstArgument = false;
 		  	 	jbossQl.append("WHERE ");
 		  	 }
 		  	 jbossQl.append("rb.rbDateCreated=?" + (ctr+1) + " ");
 		  	 obj[ctr] = (Date)criteria.get("dateCreated");
 		  	 ctr++;
 		  } 
 		  
 		  if (!firstArgument) {
 		  	
 		  	jbossQl.append("AND ");
 		  	
 		  } else {
 		  	
 		  	firstArgument = false;
 		  	jbossQl.append("WHERE ");
 		  	
 		  }
        	  
 		  jbossQl.append("rb.rbAdBranch=" + AD_BRNCH + " AND rb.rbAdCompany=" + AD_CMPNY + " ");
 		      
 	      Collection arReceiptBatches = arReceiptBatchHome.getRbByCriteria(jbossQl.toString(), obj);
 	         
 	      if (arReceiptBatches.isEmpty())
 	         throw new GlobalNoRecordFoundException();
 	         
 	      return new Integer(arReceiptBatches.size());
   
 	   } catch (GlobalNoRecordFoundException ex) {
 		  	 
 		  	  throw ex;
 		  	
 	   } catch (Exception ex) {
 	  	
 	
 		  	  ex.printStackTrace();
 		  	  throw new EJBException(ex.getMessage());
 		  	
 	   }
     
    }
   
   // SessionBean methods

   /**
    * @ejb:create-method view-type="remote"
    **/
   public void ejbCreate() throws CreateException {

      Debug.print("ArReceiptBatchControllerBean ejbCreate");
      
   }

   // private methods

     
}
