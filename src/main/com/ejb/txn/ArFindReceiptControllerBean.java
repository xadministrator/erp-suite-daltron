
/*
 * ArFindReceiptControllerBean.java
 *
 * Created on March 5, 2004, 9:43 AM
 *
 * @author  Neil Andrew M. Ajero
 */

package com.ejb.txn;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;
import javax.naming.NamingException;

import com.ejb.ad.LocalAdBankAccount;
import com.ejb.ad.LocalAdBankAccountHome;
import com.ejb.ad.LocalAdBranchItemLocation;
import com.ejb.ad.LocalAdBranchItemLocationHome;
import com.ejb.ad.LocalAdCompany;
import com.ejb.ad.LocalAdCompanyHome;
import com.ejb.ad.LocalAdLookUpValue;
import com.ejb.ad.LocalAdLookUpValueHome;
import com.ejb.ad.LocalAdPreference;
import com.ejb.ad.LocalAdPreferenceHome;
import com.ejb.ar.LocalArAppliedInvoice;
import com.ejb.ar.LocalArAppliedInvoiceHome;
import com.ejb.ar.LocalArCustomer;
import com.ejb.ar.LocalArCustomerHome;
import com.ejb.ar.LocalArDistributionRecord;
import com.ejb.ar.LocalArDistributionRecordHome;
import com.ejb.ar.LocalArInvoiceLineItem;
import com.ejb.ar.LocalArReceipt;
import com.ejb.ar.LocalArReceiptBatch;
import com.ejb.ar.LocalArReceiptBatchHome;
import com.ejb.ar.LocalArReceiptHome;
import com.ejb.exception.GlobalBranchAccountNumberInvalidException;
import com.ejb.exception.GlobalInvItemLocationNotFoundException;
import com.ejb.exception.GlobalInventoryDateException;
import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.inv.LocalInvBillOfMaterial;
import com.ejb.inv.LocalInvCosting;
import com.ejb.inv.LocalInvCostingHome;
import com.ejb.inv.LocalInvItem;
import com.ejb.inv.LocalInvItemHome;
import com.ejb.inv.LocalInvItemLocation;
import com.ejb.inv.LocalInvItemLocationHome;
import com.ejb.inv.LocalInvUnitOfMeasure;
import com.ejb.inv.LocalInvUnitOfMeasureConversion;
import com.ejb.inv.LocalInvUnitOfMeasureConversionHome;
import com.util.AbstractSessionBean;
import com.util.ArModReceiptDetails;
import com.util.Debug;
import com.util.EJBCommon;
import com.util.EJBHomeFactory;

/**
 * @ejb:bean name="ArFindReceiptControllerEJB"
 *           display-name="Used for finding receipts"
 *           type="Stateless"
 *           view-type="remote"
 *           jndi-name="ejb/ArFindReceiptControllerEJB"
 *
 * @ejb:interface remote-class="com.ejb.txn.ArFindReceiptController"
 *                extends="javax.ejb.EJBObject"
 *
 * @ejb:home remote-class="com.ejb.txn.ArFindReceiptControllerHome"
 *           extends="javax.ejb.EJBHome"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="aruser"
 *                        role-link="aruserlink"
 *
 * @ejb:permission role-name="aruser"
 * 
*/

public class ArFindReceiptControllerBean extends AbstractSessionBean {


    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList getArCstAll(Integer AD_BRNCH, Integer AD_CMPNY) {
                    
        Debug.print("ArFindReceiptControllerBean getArCstAll");
        
        LocalArCustomerHome arCustomerHome = null;               
        
        ArrayList list = new ArrayList();
        
        // Initialize EJB Home
        
        try {
            
            arCustomerHome = (LocalArCustomerHome)EJBHomeFactory.
                lookUpLocalHome(LocalArCustomerHome.JNDI_NAME, LocalArCustomerHome.class);
            
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }        
        
        try {
        	
        	Collection arCustomers = arCustomerHome.findEnabledCstAll(AD_BRNCH, AD_CMPNY);        	
        	        	
        	Iterator i = arCustomers.iterator();
        	
        	while (i.hasNext()) {
        		
        		LocalArCustomer arCustomer = (LocalArCustomer)i.next();
        		
        		list.add(arCustomer.getCstCustomerCode());
        		
        	}
        	
        	return list;
        	
        } catch (Exception ex) {
        	
        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());
        	
        }
            
    } 


    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList getAdBaAll(Integer AD_BRNCH, Integer AD_CMPNY) {
                    
        Debug.print("ArFindReceiptControllerBean getAdBaAll");
        
        LocalAdBankAccountHome adBankAccountHome = null;               
        
        ArrayList list = new ArrayList();
        
        // Initialize EJB Home
        
        try {
            
            adBankAccountHome = (LocalAdBankAccountHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdBankAccountHome.JNDI_NAME, LocalAdBankAccountHome.class);
            
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }        
        
        try {
        	
        	Collection adBankAccounts = adBankAccountHome.findEnabledBaAll(AD_BRNCH, AD_CMPNY);
        	
        	Iterator i = adBankAccounts.iterator();
        	
        	while (i.hasNext()) {
        		
        		LocalAdBankAccount adBankAccount = (LocalAdBankAccount)i.next();
        		
        		list.add(adBankAccount.getBaName());
        		
        	}
        	
        	return list;
        	
        } catch (Exception ex) {
        	
        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());
        	
        }
            
    }
    
    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList getArOpenRbAll(Integer AD_BRNCH, Integer AD_CMPNY) {
                    
        Debug.print("ArFindReceiptControllerBean getArOpenRbAll");
        
        LocalArReceiptBatchHome arReceiptBatchHome = null;               
        
        ArrayList list = new ArrayList();
        
        // Initialize EJB Home
        
        try {
            
        	arReceiptBatchHome = (LocalArReceiptBatchHome)EJBHomeFactory.
                lookUpLocalHome(LocalArReceiptBatchHome.JNDI_NAME, LocalArReceiptBatchHome.class);
            
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }        
        
        try {
        	
        	Collection arReceiptBatches = arReceiptBatchHome.findOpenRbAll(AD_BRNCH, AD_CMPNY);
        	
        	Iterator i = arReceiptBatches.iterator();
        	
        	while (i.hasNext()) {
        		
        		LocalArReceiptBatch arReceiptBatch = (LocalArReceiptBatch)i.next();
        		
        		list.add(arReceiptBatch.getRbName());
        		
        	}
        	
        	return list;
        	
        } catch (Exception ex) {
        	
        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());
        	
        }
            
    }
    
/**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList getAdLvInvShiftAll(Integer AD_CMPNY) {
                    
        Debug.print("ArFindReceiptControllerBean getAdLvInvShiftAll");
        
        LocalAdLookUpValueHome adLookUpValueHome = null;               
        
        ArrayList list = new ArrayList();
        
        // Initialize EJB Home
        
        try {
            
            adLookUpValueHome = (LocalAdLookUpValueHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdLookUpValueHome.JNDI_NAME, LocalAdLookUpValueHome.class);
            
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }        
        
        try {
        	
        	Collection adLookUpValues = adLookUpValueHome.findByLuName("INV SHIFT", AD_CMPNY);
        	
        	Iterator i = adLookUpValues.iterator();
        	
        	while (i.hasNext()) {
        		
        		LocalAdLookUpValue adLookUpValue = (LocalAdLookUpValue)i.next();
        		
        		list.add(adLookUpValue.getLvName());
        		
        	}
        	
        	return list;
        	
        } catch (Exception ex) {
        	
        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());
        	
        }
            
    }    
    
    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
     public byte getAdPrfEnableArReceiptBatch(Integer AD_CMPNY) {

        Debug.print("ArFindReceiptControllerBean getAdPrfEnableArReceiptBatch");
                   
        LocalAdPreferenceHome adPreferenceHome = null;
       
       
        // Initialize EJB Home
         
        try {
             
           adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
               lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);       
             
        } catch (NamingException ex) {
             
           throw new EJBException(ex.getMessage());
             
        }
       

        try {
       	
           LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);
          
           return adPreference.getPrfEnableArReceiptBatch();
          
        } catch (Exception ex) {
         	 
           Debug.printStackTrace(ex);
           throw new EJBException(ex.getMessage());
          
        }
       
     }
  
     /**
      * @ejb:interface-method view-type="remote"
      * @jboss:method-attributes read-only="true"
      **/
      public String getArPrfDefaultReceiptType(Integer AD_CMPNY) {

         Debug.print("ArFindReceiptControllerBean getArPrfDefaultReceiptType");
                    
         LocalAdPreferenceHome adPreferenceHome = null;
        
        
         // Initialize EJB Home
          
         try {
              
            adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);       
              
         } catch (NamingException ex) {
              
            throw new EJBException(ex.getMessage());
              
         }
        

         try {
        	
            LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);
           
            return adPreference.getPrfArFindReceiptDefaultType();
           
         } catch (Exception ex) {
          	 
            Debug.printStackTrace(ex);
            throw new EJBException(ex.getMessage());
           
         }
        
      }
     /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
     public byte getAdPrfEnableInvShift(Integer AD_CMPNY) {

        Debug.print("ArFindReceiptControllerBean getAdPrfEnableInvShift");
                   
        LocalAdPreferenceHome adPreferenceHome = null;
       
       
        // Initialize EJB Home
         
        try {
             
           adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
               lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);       
             
        } catch (NamingException ex) {
             
           throw new EJBException(ex.getMessage());
             
        }
       

        try {
       	
           LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);
          
           return adPreference.getPrfInvEnableShift();
          
        } catch (Exception ex) {
         	 
           Debug.printStackTrace(ex);
           throw new EJBException(ex.getMessage());
          
        }
       
     }
           
  /**
   * @ejb:interface-method view-type="remote"
   * @jboss:method-attributes read-only="true"
   **/
   public ArrayList getArRctByCriteria(HashMap criteria, String ORDER_BY, Integer OFFSET, Integer LIMIT, Integer AD_BRNCH, Integer AD_CMPNY)
	  throws GlobalNoRecordFoundException {
	
	  Debug.print("ArFindReceiptControllerBean getArRctByCriteria");
	  
	  LocalArReceiptHome arReceiptHome = null;
	  
	  //ArrayList list = new ArrayList();
	  
	  // Initialize EJB Home
	    
	  try {
	  	
	      arReceiptHome = (LocalArReceiptHome)EJBHomeFactory.
	          lookUpLocalHome(LocalArReceiptHome.JNDI_NAME, LocalArReceiptHome.class); 
	        
	  } catch (NamingException ex) {
	        
	      throw new EJBException(ex.getMessage());
	        
	  }
	  
	  try { 
	      
	      ArrayList rctList = new ArrayList();
	      
	      StringBuffer jbossQl = new StringBuffer();
	      jbossQl.append("SELECT OBJECT(rct) FROM ArReceipt rct ");
	      
	      boolean firstArgument = true;
	      short ctr = 0;
	      int criteriaSize = criteria.size() + 2;
		  Object obj[];
		  
		  if (criteria.containsKey("approvalStatus")) {
	      	
	      	 String approvalStatus = (String)criteria.get("approvalStatus");
	      	
	      	 if (approvalStatus.equals("DRAFT") || approvalStatus.equals("REJECTED")) {
	      	 	
	      	 	 criteriaSize--;
	      	 	
	      	 }
	      	
	      }
		  
		  if (criteria.containsKey("computeRatio")) {
			  
			  criteriaSize--;
			  
		  }
		  
		  
		  obj = new Object[criteriaSize];
		  
		  if (criteria.containsKey("batchName")) {
		   	
		  	  firstArgument = false;
		   	  
		   	  jbossQl.append("WHERE rct.arReceiptBatch.rbName=?" + (ctr+1) + " ");
		   	  obj[ctr] = (String)criteria.get("batchName");
		   	  ctr++;
	   	  
	      }
		  
		  if (criteria.containsKey("shift")) {
		   	
		  	  firstArgument = false;
		   	  
		   	  jbossQl.append("WHERE rct.rctLvShift=?" + (ctr+1) + " ");
		   	  obj[ctr] = (String)criteria.get("shift");
		   	  ctr++;
	   	  
	      }
		  	      
		  if (criteria.containsKey("bankAccount")) {

		  	  if (!firstArgument) {		       	  	
		   	     jbossQl.append("AND ");		       	     
		   	  } else {		       	  	
		   	  	 firstArgument = false;
		   	  	 jbossQl.append("WHERE ");		       	  	 
		   	  }
		  	 
		  	 jbossQl.append("rct.adBankAccount.baName=?" + (ctr+1) + " ");
		  	 obj[ctr] = (String)criteria.get("bankAccount");
		  	 ctr++;
		  	 
		  } 
		   
		  if (criteria.containsKey("receiptType")) {
		  	
		  	 if (!firstArgument) {
		  	 
		  	 	jbossQl.append("AND ");
		  	 
		  	 } else {
		  	 
		  	 	firstArgument = false;
		  	 	jbossQl.append("WHERE ");
		  	 
		  	 }
		  	 
		  	 jbossQl.append("rct.rctType=?" + (ctr+1) + " ");
		  	 obj[ctr] = (String)criteria.get("receiptType");
		  	 System.out.println(obj[ctr]);
		  	 ctr++;
		  }	
		  
		  if (criteria.containsKey("receiptVoid")) {
		  	
		  	 if (!firstArgument) {
		  	 
		  	 	jbossQl.append("AND ");
		  	 
		  	 } else {
		  	 
		  	 	firstArgument = false;
		  	 	jbossQl.append("WHERE ");
		  	 
		  	 }
		  	 
		  	 jbossQl.append("rct.rctVoid=?" + (ctr+1) + " ");
		  	 obj[ctr] = (Byte)criteria.get("receiptVoid");
		  	 ctr++;
		  }		   
		      
		  if (criteria.containsKey("customerCode")) {
		  	
		  	 if (!firstArgument) {
		  	 
		  	 	jbossQl.append("AND ");
		  	 
		  	 } else {
		  	 
		  	 	firstArgument = false;
		  	 	jbossQl.append("WHERE ");
		  	 
		  	 }
		  	 
		  	 jbossQl.append("rct.arCustomer.cstCustomerCode=?" + (ctr+1) + " ");
		  	 obj[ctr] = (String)criteria.get("customerCode");
		  	 ctr++;
		  }      
		      
		  if (criteria.containsKey("dateFrom")) {
		  	
		  	 if (!firstArgument) {
		  	 	jbossQl.append("AND ");
		  	 
		  	 } else {
		  	 
		  	 	firstArgument = false;
		  	 	jbossQl.append("WHERE ");
		  	 
		  	 }
		  	 
		  	 jbossQl.append("rct.rctDate>=?" + (ctr+1) + " ");
		  	 obj[ctr] = (Date)criteria.get("dateFrom");
		  	 ctr++;
		  }  
		      
		  if (criteria.containsKey("dateTo")) {
		  	
		  	 if (!firstArgument) {
		  	 
		  	 	jbossQl.append("AND ");
		  	 
		  	 } else {
		  	 
		  	 	firstArgument = false;
		  	 	jbossQl.append("WHERE ");
		  	 
		  	 }
		  	 
		  	 jbossQl.append("rct.rctDate<=?" + (ctr+1) + " ");
		  	 obj[ctr] = (Date)criteria.get("dateTo");
		  	 ctr++;
		  	 
		  }    
		      
		  if (criteria.containsKey("receiptNumberFrom")) {
		  	
		  	 if (!firstArgument) {
		  	 
		  	 	jbossQl.append("AND ");
		  	 
		  	 } else {
		  	 
		  	 	firstArgument = false;
		  	 	jbossQl.append("WHERE ");
		  	 
		  	 }
		  	 
		  	 jbossQl.append("rct.rctNumber>=?" + (ctr+1) + " ");
		  	 obj[ctr] = (String)criteria.get("receiptNumberFrom");
		  	 ctr++;
		  	 
		  }  
		      
		  if (criteria.containsKey("receiptNumberTo")) {
		  	
		  	 if (!firstArgument) {
		  	 
		  	 	jbossQl.append("AND ");
		  	 
		  	 } else {
		  	 
		  	 	firstArgument = false;
		  	 	jbossQl.append("WHERE ");
		  	 
		  	 }
		  	 
		  	 jbossQl.append("rct.rctNumber<=?" + (ctr+1) + " ");
		  	 obj[ctr] = (String)criteria.get("receiptNumberTo");
		  	 ctr++;
		  	 
		  }
		  
		  
		  if (criteria.containsKey("receiptReferenceNumberFrom")) {
	  		  	
	 		  	 if (!firstArgument) {
	 		  	 
	 		  	 	jbossQl.append("AND ");
	 		  	 
	 		  	 } else {
	 		  	 
	 		  	 	firstArgument = false;
	 		  	 	jbossQl.append("WHERE ");
	 		  	 
	 		  	 }
	 		  	 
	 		  	 jbossQl.append("rct.rctReferenceNumber>=?" + (ctr+1) + " ");
	 		  	 obj[ctr] = (String)criteria.get("receiptReferenceNumberFrom");
	 		  	 System.out.println("receiptReferenceNumberFrom="+(String)criteria.get("receiptReferenceNumberFrom"));
	 		  	 ctr++;
	 		  	 
	 		  }  
	 		      
	 		  if (criteria.containsKey("receiptReferenceNumberTo")) {
	 		  	
	 		  	 if (!firstArgument) {
	 		  	 
	 		  	 	jbossQl.append("AND ");
	 		  	 
	 		  	 } else {
	 		  	 
	 		  	 	firstArgument = false;
	 		  	 	jbossQl.append("WHERE ");
	 		  	 
	 		  	 }
	 		  	 
	 		  	 jbossQl.append("rct.rctReferenceNumber<=?" + (ctr+1) + " ");
	 		  	 obj[ctr] = (String)criteria.get("receiptReferenceNumberTo");
	 		  	System.out.println("receiptReferenceNumberTo="+(String)criteria.get("receiptReferenceNumberTo"));
	 		  	 ctr++;
	 		  	 
	 		  }
 
		  if (criteria.containsKey("approvalStatus")) {
		   	
		   	  if (!firstArgument) {
		   	  	
		   	     jbossQl.append("AND ");
		   	     
		   	  } else {
		   	  	
		   	  	 firstArgument = false;
		   	  	 jbossQl.append("WHERE ");
		   	  	 
		   	  }
		   	  
		   	  String approvalStatus = (String)criteria.get("approvalStatus");
       	  
	       	  if (approvalStatus.equals("DRAFT")) {
	       	      
		       	  jbossQl.append("rct.rctApprovalStatus IS NULL ");
	       	  	
	       	  } else if (approvalStatus.equals("REJECTED")) {
	       	  	
		       	  jbossQl.append("rct.rctReasonForRejection IS NOT NULL ");
	       	  	
	      	  } else {
	      	  	
		      	  jbossQl.append("rct.rctApprovalStatus=?" + (ctr+1) + " ");
		       	  obj[ctr] = approvalStatus;
		       	  ctr++;
	      	  	
	      	  }
		   	  
		  }
		  
		  if (criteria.containsKey("posted")) {
		   	
		   	  if (!firstArgument) {
		   	  	
		   	     jbossQl.append("AND ");
		   	     
		   	  } else {
		   	  	
		   	  	 firstArgument = false;
		   	  	 jbossQl.append("WHERE ");
		   	  	 
		   	  }
		   	  
		   	  jbossQl.append("rct.rctPosted=?" + (ctr+1) + " ");
		   	  
		   	  String posted = (String)criteria.get("posted");
		   	  
		   	  if (posted.equals("YES")) {
		   	  	
		   	  	obj[ctr] = new Byte(EJBCommon.TRUE);
		   	  	
		   	  } else {
		   	  	
		   	  	obj[ctr] = new Byte(EJBCommon.FALSE);
		   	  	
		   	  }       	  
		   	 
		   	  ctr++;
		   	  
		  }	  	       
		  
		  if (!firstArgument) {
       	  	
       	     jbossQl.append("AND ");
       	     
       	  } else {
       	  	
       	  	 firstArgument = false;
       	  	 jbossQl.append("WHERE ");
       	  	 
       	  }
       	  
       	  jbossQl.append("rct.rctAdBranch=" + AD_BRNCH + " AND rct.rctAdCompany=" + AD_CMPNY + " ");
	             	     
	      String orderBy = null;
		      
	      if (ORDER_BY.equals("BANK ACCOUNT")) {
	          
	          orderBy = "rct.adBankAccount.baName";
	          	
	      } else if (ORDER_BY.equals("CUSTOMER CODE")) {
	      	 
	      	  orderBy = "rct.arCustomer.cstCustomerCode";

	      } else if (ORDER_BY.equals("RECEIPT NUMBER")) {
	      	
	      	  orderBy = "rct.rctNumber";
	      	
	      }
	      
		  
		  if (orderBy != null) {
		  
		  	jbossQl.append("ORDER BY " + orderBy + ", rct.rctDate");
		  	
		  }  else {
		  	
		  	jbossQl.append("ORDER BY rct.rctDate");
		  	
		  }
		  
	               
	      jbossQl.append(" OFFSET ?" + (ctr + 1));
	      obj[ctr] = OFFSET;

	      ctr++;
	      
	      jbossQl.append(" LIMIT ?" + (ctr + 1));
	      obj[ctr] = LIMIT;

	
	      Collection arReceipts = null;
	      String computeRatio = "";
	      
	      if (criteria.containsKey("computeRatio")) {	    	  
	    	  
	    	  computeRatio = (String)criteria.get("computeRatio");
	    	  
	      }

	      try {
	    	  System.out.println("getArRctByCriteria jbossQl.toString()="+jbossQl.toString());
	    	  arReceipts = arReceiptHome.getRctByCriteria(jbossQl.toString(), obj);
	         
	      } catch (Exception ex) {
	      	
	      	 throw new EJBException(ex.getMessage());
	      	 
	      }
	      
	      if (arReceipts.isEmpty())
	         throw new GlobalNoRecordFoundException();
	         
	      Iterator i = arReceipts.iterator();
	      while (i.hasNext()) {
	
	         LocalArReceipt arReceipt = (LocalArReceipt) i.next();
	
		     ArModReceiptDetails mdetails = new ArModReceiptDetails();
		     Collection arAppliedInvoices = arReceipt.getArAppliedInvoices();
		     Iterator x = arAppliedInvoices.iterator();
		     double totalAmount = 0d;
		     double creditedBalance =0d;
		     double totalMiscReceipt = arReceipt.getRctAmountCash() 
		    		 + arReceipt.getRctAmountCard1()
		    		 + arReceipt.getRctAmountCard2()
		    		 + arReceipt.getRctAmountCard3();
		     
		     
		     while(x.hasNext()){
		    	 LocalArAppliedInvoice arAppliedInvoice = (LocalArAppliedInvoice)x.next();
		    	 totalAmount += (arAppliedInvoice.getAiApplyAmount()+arAppliedInvoice.getAiPenaltyApplyAmount() 
		    			 + arAppliedInvoice.getAiDiscountAmount() + arAppliedInvoice.getAiCreditBalancePaid() + arAppliedInvoice.getAiRebate());
		    	 creditedBalance += arAppliedInvoice.getAiCreditBalancePaid();
		     }
		     
		     mdetails.setRctCode(arReceipt.getRctCode());
             mdetails.setRctType(arReceipt.getRctType());
		     mdetails.setRctDate(arReceipt.getRctDate());
		     mdetails.setRctNumber(arReceipt.getRctNumber());
		     mdetails.setRctReferenceNumber(arReceipt.getRctReferenceNumber());
		     mdetails.setRctAmount(arReceipt.getRctType().equals("MISC")? totalMiscReceipt : totalAmount);
		     mdetails.setRctCreditedBalance(creditedBalance);
		     mdetails.setRctCstName(arReceipt.getRctCustomerName() == null || arReceipt.getRctCustomerName().equals("") ? arReceipt.getArCustomer().getCstName() : arReceipt.getRctCustomerName());
		     mdetails.setRctBaName(arReceipt.getAdBankAccount().getBaName());
		     
		     //call private method for computing ratio
		     mdetails.setRctRatio(computeRatio.equals("YES") ? this.getCogsRatio(arReceipt, AD_BRNCH, AD_CMPNY) : 0);

	      	 rctList.add(mdetails);
	      	
	      }
	         
	      return rctList;
  
	  } catch (GlobalNoRecordFoundException ex) {
	  	 
	  	  throw ex;
	  	
	  } catch (Exception ex) {
	  	

	  	  ex.printStackTrace();
	  	  throw new EJBException(ex.getMessage());
	  	
	  }
        
   }
   
   /**
    * @ejb:interface-method view-type="remote"
    * @jboss:method-attributes read-only="true"
    **/
    public Integer getArRctSizeByCriteria(HashMap criteria, Integer AD_BRNCH, Integer AD_CMPNY)
 	  throws GlobalNoRecordFoundException {

 	  Debug.print("ArFindReceiptControllerBean getArRctSizeByCriteria");

 	  LocalArReceiptHome arReceiptHome = null;
 	  
 	  // Initialize EJB Home
 	    
 	  try {
 	  	
 	      arReceiptHome = (LocalArReceiptHome)EJBHomeFactory.
 	          lookUpLocalHome(LocalArReceiptHome.JNDI_NAME, LocalArReceiptHome.class);          
 	        
 	  } catch (NamingException ex) {
 	        
 	      throw new EJBException(ex.getMessage());
 	        
 	  }
 	  
 	  try { 
 	      
 	      StringBuffer jbossQl = new StringBuffer();
 	      
 	      jbossQl.append("SELECT OBJECT(rct) FROM ArReceipt rct ");
 	      
 	      boolean firstArgument = true;
 	      short ctr = 0;
 	      int criteriaSize = criteria.size();
 		  Object obj[];
 		  
 		  if (criteria.containsKey("approvalStatus")) {
 	      	
 	      	 String approvalStatus = (String)criteria.get("approvalStatus");
 	      	
 	      	 if (approvalStatus.equals("DRAFT") || approvalStatus.equals("REJECTED")) {
 	      	 	
 	      	 	 criteriaSize--;
 	      	 	
 	      	 }
 	      	
 	      }
 		  
 		 if (criteria.containsKey("computeRatio")) {
  	      	
 			criteriaSize--;
 	      	 
 	      	
 	      }
 		  
 		  obj = new Object[criteriaSize];
 		  
 		  if (criteria.containsKey("batchName")) {
 		   	
 		  	  firstArgument = false;
 		   	  
 		   	  jbossQl.append("WHERE rct.arReceiptBatch.rbName=?" + (ctr+1) + " ");
 		   	  obj[ctr] = (String)criteria.get("batchName");
 		   	  
 		   	  ctr++;
 	   	  
 	      }
 		  
 		  if (criteria.containsKey("shift")) {
 		   	
 		  	  firstArgument = false;
 		   	  
 		   	  jbossQl.append("WHERE rct.rctLvShift=?" + (ctr+1) + " ");
 		   	  obj[ctr] = (String)criteria.get("shift");
 		   	  
 		   	  ctr++;
 	   	  
 	      }
 		  	      
 		  if (criteria.containsKey("bankAccount")) {

 		  	  if (!firstArgument) {		       	  	
 		   	     jbossQl.append("AND ");		       	     
 		   	  } else {		       	  	
 		   	  	 firstArgument = false;
 		   	  	 jbossQl.append("WHERE ");		       	  	 
 		   	  }
 		  	 
 		  	 jbossQl.append("rct.adBankAccount.baName=?" + (ctr+1) + " ");
 		  	 obj[ctr] = (String)criteria.get("bankAccount");
 		  	 ctr++;
 		  	 
 		  } 
 		   
 		  if (criteria.containsKey("receiptType")) {
 		  	
 		  	 if (!firstArgument) {
 		  	 
 		  	 	jbossQl.append("AND ");
 		  	 
 		  	 } else {
 		  	 
 		  	 	firstArgument = false;
 		  	 	jbossQl.append("WHERE ");
 		  	 
 		  	 }
 		  	 
 		  	 jbossQl.append("rct.rctType=?" + (ctr+1) + " ");

 		  	 obj[ctr] = (String)criteria.get("receiptType");

 		  	 ctr++;
 		  }	
 		  
 		  if (criteria.containsKey("receiptVoid")) {
 		  	
 		  	 if (!firstArgument) {
 		  	 
 		  	 	jbossQl.append("AND ");
 		  	 
 		  	 } else {
 		  	 
 		  	 	firstArgument = false;
 		  	 	jbossQl.append("WHERE ");
 		  	 
 		  	 }
 		  	 
 		  	 jbossQl.append("rct.rctVoid=?" + (ctr+1) + " ");

 		  	 obj[ctr] = (Byte)criteria.get("receiptVoid");

 		  	 ctr++;
 		  }		   
 		      
 		  if (criteria.containsKey("customerCode")) {
 		  	
 		  	 if (!firstArgument) {
 		  	 
 		  	 	jbossQl.append("AND ");
 		  	 
 		  	 } else {
 		  	 
 		  	 	firstArgument = false;
 		  	 	jbossQl.append("WHERE ");
 		  	 
 		  	 }
 		  	 
 		  	 jbossQl.append("rct.arCustomer.cstCustomerCode=?" + (ctr+1) + " ");
 		  	 obj[ctr] = (String)criteria.get("customerCode");

 		  	 ctr++;
 		  }      
 		      
 		  if (criteria.containsKey("dateFrom")) {
 		  	
 		  	 if (!firstArgument) {
 		  	 	jbossQl.append("AND ");
 		  	 
 		  	 } else {
 		  	 
 		  	 	firstArgument = false;
 		  	 	jbossQl.append("WHERE ");
 		  	 
 		  	 }
 		  	 
 		  	 jbossQl.append("rct.rctDate>=?" + (ctr+1) + " ");
 		  	 obj[ctr] = (Date)criteria.get("dateFrom");
 		  	 ctr++;
 		  }  
 		      
 		  if (criteria.containsKey("dateTo")) {
 		  	
 		  	 if (!firstArgument) {
 		  	 
 		  	 	jbossQl.append("AND ");
 		  	 
 		  	 } else {
 		  	 
 		  	 	firstArgument = false;
 		  	 	jbossQl.append("WHERE ");
 		  	 
 		  	 }
 		  	 
 		  	 jbossQl.append("rct.rctDate<=?" + (ctr+1) + " ");
 		  	 obj[ctr] = (Date)criteria.get("dateTo");
 		  	 ctr++;
 		  	 
 		  }    
 		      
 		  if (criteria.containsKey("receiptNumberFrom")) {
 		  	
 		  	 if (!firstArgument) {
 		  	 
 		  	 	jbossQl.append("AND ");
 		  	 
 		  	 } else {
 		  	 
 		  	 	firstArgument = false;
 		  	 	jbossQl.append("WHERE ");
 		  	 
 		  	 }
 		  	 
 		  	 jbossQl.append("rct.rctNumber>=?" + (ctr+1) + " ");
 		  	 obj[ctr] = (String)criteria.get("receiptNumberFrom");
 		  	 ctr++;
 		  	 
 		  }  
 		      
 		  if (criteria.containsKey("receiptNumberTo")) {
 		  	
 		  	 if (!firstArgument) {
 		  	 
 		  	 	jbossQl.append("AND ");
 		  	 
 		  	 } else {
 		  	 
 		  	 	firstArgument = false;
 		  	 	jbossQl.append("WHERE ");
 		  	 
 		  	 }
 		  	 
 		  	 jbossQl.append("rct.rctNumber<=?" + (ctr+1) + " ");
 		  	 obj[ctr] = (String)criteria.get("receiptNumberTo");
 		  	 ctr++;
 		  	 
 		  } 
 		  
 		  
 		 if (criteria.containsKey("receiptReferenceNumberFrom")) {
  		  	
 		  	 if (!firstArgument) {
 		  	 
 		  	 	jbossQl.append("AND ");
 		  	 
 		  	 } else {
 		  	 
 		  	 	firstArgument = false;
 		  	 	jbossQl.append("WHERE ");
 		  	 
 		  	 }
 		  	 
 		  	 jbossQl.append("rct.rctReferenceNumber>=?" + (ctr+1) + " ");
 		  	 obj[ctr] = (String)criteria.get("receiptReferenceNumberFrom");
 		  	 ctr++;
 		  	 
 		  }  
 		      
 		  if (criteria.containsKey("receiptReferenceNumberTo")) {
 		  	
 		  	 if (!firstArgument) {
 		  	 
 		  	 	jbossQl.append("AND ");
 		  	 
 		  	 } else {
 		  	 
 		  	 	firstArgument = false;
 		  	 	jbossQl.append("WHERE ");
 		  	 
 		  	 }
 		  	 
 		  	 jbossQl.append("rct.rctReferenceNumber<=?" + (ctr+1) + " ");
 		  	 obj[ctr] = (String)criteria.get("receiptReferenceNumberTo");
 		  	 ctr++;
 		  	 
 		  }
 		  
  
 		  if (criteria.containsKey("approvalStatus")) {
 		   	
 		   	  if (!firstArgument) {
 		   	  	
 		   	     jbossQl.append("AND ");
 		   	     
 		   	  } else {
 		   	  	
 		   	  	 firstArgument = false;
 		   	  	 jbossQl.append("WHERE ");
 		   	  	 
 		   	  }
 		   	  
 		   	  String approvalStatus = (String)criteria.get("approvalStatus");
        	  
 	       	  if (approvalStatus.equals("DRAFT")) {
 	       	      
 		       	  jbossQl.append("rct.rctApprovalStatus IS NULL ");
 	       	  	
 	       	  } else if (approvalStatus.equals("REJECTED")) {
 	       	  	
 		       	  jbossQl.append("rct.rctReasonForRejection IS NOT NULL ");
 	       	  	
 	      	  } else {
 	      	  	
 		      	  jbossQl.append("rct.rctApprovalStatus=?" + (ctr+1) + " ");
 		       	  obj[ctr] = approvalStatus;
 		       	  ctr++;
 	      	  	
 	      	  }
 		   	  
 		  }
 		  
 		  if (criteria.containsKey("posted")) {
 		   	
 		   	  if (!firstArgument) {
 		   	  	
 		   	     jbossQl.append("AND ");
 		   	     
 		   	  } else {
 		   	  	
 		   	  	 firstArgument = false;
 		   	  	 jbossQl.append("WHERE ");
 		   	  	 
 		   	  }
 		   	  
 		   	  jbossQl.append("rct.rctPosted=?" + (ctr+1) + " ");

 		   	  
 		   	  String posted = (String)criteria.get("posted");
 		   	  
 		   	  if (posted.equals("YES")) {
 		   	  	
 		   	  	obj[ctr] = new Byte(EJBCommon.TRUE);
 		   	  	
 		   	  } else {
 		   	  	
 		   	  	obj[ctr] = new Byte(EJBCommon.FALSE);
 		   	  	
 		   	  }       	  
 		   	 
 		   	  ctr++;
 		   	  
 		  }	  	 
 		  
 		  
 		  
 		  if (!firstArgument) {
        	  	
        	     jbossQl.append("AND ");
        	     
 		  } else {
 		  	
 		  	firstArgument = false;
 		  	jbossQl.append("WHERE ");
 		  	
 		  }
 		 
 		  jbossQl.append("rct.rctAdBranch=" + AD_BRNCH + " AND rct.rctAdCompany=" + AD_CMPNY + " ");
 		  
     	     
 	      Collection arReceipts = null;

 	      try {
 	    	  
 	    	 System.out.println("getArRctSizeByCriteria jbossQl.toString()="+jbossQl.toString());

 	         arReceipts = arReceiptHome.getRctByCriteria(jbossQl.toString(), obj);

 	        
 	      } catch (Exception ex) {
 	      	
 	      	 throw new EJBException(ex.getMessage());
 	      	 
 	      }
 	      
 	      if (arReceipts.isEmpty())
 	         throw new GlobalNoRecordFoundException();
 	         
 	      return new Integer(arReceipts.size());
   
 	  } catch (GlobalNoRecordFoundException ex) {
 	  	 
 	  	  throw ex;
 	  	
 	  } catch (Exception ex) {
 	  	

 	  	  ex.printStackTrace();
 	  	  throw new EJBException(ex.getMessage());
 	  	
 	  }
         
    }
   
   
   /**
    * @ejb:interface-method view-type="remote"
    * @jboss:method-attributes read-only="true"
    **/
    public short getGlFcPrecisionUnit(Integer AD_CMPNY) {

       Debug.print("ArFindReceiptControllerBean getGlFcPrecisionUnit");

      
       LocalAdCompanyHome adCompanyHome = null;
      
     
       // Initialize EJB Home
        
       try {
            
           adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
              lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);       
            
       } catch (NamingException ex) {
            
           throw new EJBException(ex.getMessage());
            
       }

       try {
       	
         LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
            
         return  adCompany.getGlFunctionalCurrency().getFcPrecision();
                                     
       } catch (Exception ex) {
       	 
       	 Debug.printStackTrace(ex);
         throw new EJBException(ex.getMessage());
         
       }

    }

    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public byte getAdPrfArUseCustomerPulldown(Integer AD_CMPNY) {
    	
    	Debug.print("ArFindReceiptControllerBean getAdPrfArUseCustomerPulldown");
    	
    	LocalAdPreferenceHome adPreferenceHome = null;         
    	
    	// Initialize EJB Home
    	
    	try {
    		
    		adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);       
    		
    	} catch (NamingException ex) {
    		
    		throw new EJBException(ex.getMessage());
    		
    	}
    	
    	
    	try {
    		
    		LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);
    		
    		return adPreference.getPrfArUseCustomerPulldown();
    		
    	} catch (Exception ex) {
    		
    		Debug.printStackTrace(ex);
    		throw new EJBException(ex.getMessage());
    		
    	}
    	
    }
    
    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
     public short getInvGpQuantityPrecisionUnit(Integer AD_CMPNY) {

        Debug.print("ArMiscReceiptEntryControllerBean getInvGpQuantityPrecisionUnit");
       
        LocalAdPreferenceHome adPreferenceHome = null;         
        
         // Initialize EJB Home
          
         try {
              
         	adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);       
              
         } catch (NamingException ex) {
              
            throw new EJBException(ex.getMessage());
              
         }
        

         try {
        	
            LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);
           
            return adPreference.getPrfInvQuantityPrecisionUnit();
           
         } catch (Exception ex) {
          	 
            Debug.printStackTrace(ex);
            throw new EJBException(ex.getMessage());
           
         }

     }

     
     // Private Methods
    
    private double getCogsRatio(LocalArReceipt arReceipt, Integer AD_BRNCH, Integer AD_CMPNY) {
    	
    	Debug.print("ArFindReceiptControllerBean regenerateInventoryDr");		        
    	
    	LocalArDistributionRecordHome arDistributionRecordHome = null;
    	LocalInvCostingHome invCostingHome = null;
    	LocalInvItemLocationHome invItemLocationHome = null;
    	LocalInvItemHome invItemHome = null;
   	 
   	    // Initialize EJB Home
    	
     	try {
    		
     		arDistributionRecordHome = (LocalArDistributionRecordHome)EJBHomeFactory.
 				lookUpLocalHome(LocalArDistributionRecordHome.JNDI_NAME, LocalArDistributionRecordHome.class); 
     		invCostingHome = (LocalInvCostingHome)EJBHomeFactory.
				lookUpLocalHome(LocalInvCostingHome.JNDI_NAME, LocalInvCostingHome.class); 
     		invItemLocationHome = (LocalInvItemLocationHome)EJBHomeFactory.
				lookUpLocalHome(LocalInvItemLocationHome.JNDI_NAME, LocalInvItemLocationHome.class); 
     		invItemHome = (LocalInvItemHome)EJBHomeFactory.
				lookUpLocalHome(LocalInvItemHome.JNDI_NAME, LocalInvItemHome.class); 

     	} catch (NamingException ex) {
    		
     		throw new EJBException(ex.getMessage());
    		
     	}
    	
     	try {
     		
     		double TOTAL_COGS = 0d;
             
         	if(arReceipt.getRctType().equals("MISC") && arReceipt.getArInvoiceLineItems().size() > 0) {
         		
         		Iterator i = arReceipt.getArInvoiceLineItems().iterator();
         		
         		while (i.hasNext()) {
         			
         			LocalArInvoiceLineItem arInvoiceLineItem = (LocalArInvoiceLineItem)i.next();
         			LocalInvItemLocation invItemLocation = arInvoiceLineItem.getInvItemLocation();
         			// add cost of sales distribution and inventory
         			
         			double COST = 0d;
         			
         			try {
         				
         				/*LocalInvCosting invCosting  = invCostingHome.getByMaxCstDateToLongAndMaxCstLineNumberAndLessThanEqualCstDateAndRemainingQuantityNotEqualToZeroAndIiNameAndLocName(
         						arReceipt.getRctDate(), invItemLocation.getInvItem().getIiName(), invItemLocation.getInvLocation().getLocName(), AD_BRNCH, AD_CMPNY);*/
         				
         				LocalInvCosting invCosting  = invCostingHome.getByMaxCstDateToLongAndMaxCstLineNumberAndLessThanEqualCstDateAndRemainingQuantityNotEqualToZeroAndIlCode(
         						arReceipt.getRctDate(), invItemLocation.getIlCode(), AD_BRNCH, AD_CMPNY);
         				
         				if(invCosting.getInvItemLocation().getInvItem().getIiCostMethod().equals("Average"))
 	  	    				
 	  	    				COST = Math.abs(invCosting.getCstRemainingValue() / invCosting.getCstRemainingQuantity());
 	  	    			
 	  	    			else if(invCosting.getInvItemLocation().getInvItem().getIiCostMethod().equals("FIFO"))
 	  	    				
 	  	    				COST = Math.abs(this.getInvFifoCost(invCosting.getCstDate(), invCosting.getInvItemLocation().getIlCode(),
 	  	    						arInvoiceLineItem.getIliQuantity(), arInvoiceLineItem.getIliUnitPrice(), false, AD_BRNCH, AD_CMPNY));
         				
 	  	    			else if(invCosting.getInvItemLocation().getInvItem().getIiCostMethod().equals("Standard"))
 	  	    				
 	  	    				COST = invCosting.getInvItemLocation().getInvItem().getIiUnitCost();
         				
         				
         			} catch (FinderException ex) {
         				
         				COST = arInvoiceLineItem.getInvItemLocation().getInvItem().getIiUnitCost();
         				
         			}
         			
         			double QTY_SLD = this.convertByUomFromAndItemAndQuantity(arInvoiceLineItem.getInvUnitOfMeasure(), 
         					arInvoiceLineItem.getInvItemLocation().getInvItem(), arInvoiceLineItem.getIliQuantity(), AD_CMPNY);
         			
         			
         			
         			
         			
         			if (arInvoiceLineItem.getIliEnableAutoBuild() == EJBCommon.FALSE) {
         				
         				TOTAL_COGS += COST * QTY_SLD;
         				
         			} 
         			
         			if (arInvoiceLineItem.getIliEnableAutoBuild() == EJBCommon.TRUE && arInvoiceLineItem.getInvItemLocation().getInvItem().getIiClass().equals("Assembly")) {
         				
         				
         				double TOTAL_AMOUNT = 0;
         				double ABS_TOTAL_AMOUNT = 0;
         				
         				Collection invBillOfMaterials = arInvoiceLineItem.getInvItemLocation().getInvItem().getInvBillOfMaterials();
         				
         				Iterator j = invBillOfMaterials.iterator();
         				
         				while (j.hasNext()) {
         					
         					LocalInvBillOfMaterial invBillOfMaterial = (LocalInvBillOfMaterial) j.next();

         					//get raw material
                            LocalInvItemLocation invIlRawMaterial = null;
							
							try {
								
								invIlRawMaterial = invItemLocationHome.findByLocNameAndIiName(
									invBillOfMaterial.getBomLocName(), invBillOfMaterial.getBomIiName(), AD_CMPNY);
								
							} catch(FinderException ex) {
								
								throw new GlobalInvItemLocationNotFoundException(String.valueOf(arInvoiceLineItem.getIliLine()) +
									" - Raw Mat. (" +invBillOfMaterial.getBomIiName() + ")");
								
							}
		 	  	    		
         					// add bill of material quantity needed to item location

         					double quantityNeeded = this.convertByUomFromAndItemAndQuantity(
                                    invBillOfMaterial.getInvUnitOfMeasure(), invIlRawMaterial.getInvItem(),
                                    EJBCommon.roundIt(invBillOfMaterial.getBomQuantityNeeded() *  QTY_SLD,
                                    this.getInvGpQuantityPrecisionUnit(AD_CMPNY)),
                                    AD_CMPNY);
                            

							LocalInvItem invItem = invItemHome.findByIiName(invBillOfMaterial.getBomIiName(), AD_CMPNY);												
         					
         					double COSTING = 0d;
         					
         					try {
         						
         						LocalInvCosting invCosting  = invCostingHome.getByMaxCstDateToLongAndMaxCstLineNumberAndLessThanEqualCstDateAndIiNameAndLocName(
    	      	    				arReceipt.getRctDate(), invItem.getIiName(), invBillOfMaterial.getBomLocName(), AD_BRNCH, AD_CMPNY);
         						
         							COSTING = Math.abs(invCosting.getCstRemainingValue() / invCosting.getCstRemainingQuantity());
         						
         					} catch (FinderException ex) {	 
         						
         						COSTING = invItem.getIiUnitCost();	  	        	 	  	        	
         						
         					}
         					
         					// bom conversion
                            COSTING = this.convertCostByUom(invIlRawMaterial.getInvItem().getIiName(),
                                    invBillOfMaterial.getInvUnitOfMeasure().getUomName(), COSTING, true, AD_CMPNY);
                            
                            double BOM_AMOUNT = EJBCommon.roundIt(
                                    (QTY_SLD * (invBillOfMaterial.getBomQuantityNeeded() * COSTING)),
                                    this.getGlFcPrecisionUnit(AD_CMPNY));
                           
         					TOTAL_COGS += BOM_AMOUNT;
         					
         				}
         			}
         		}
         		
             	return EJBCommon.roundIt((arReceipt.getRctAmount() / (TOTAL_COGS + arReceipt.getRctAmount())) * 100, (short)2);
         		
         	} else
         		
         		return 0d;
         	
         } catch (Exception ex) {
    		
         	Debug.printStackTrace(ex);
 	   		getSessionContext().setRollbackOnly();
 	   		throw new EJBException(ex.getMessage());
    		
         }
    }

    private double getInvFifoCost(Date CST_DT, Integer IL_CODE, double CST_QTY, double CST_COST, 
      		 boolean isAdjustFifo, Integer AD_BRNCH, Integer AD_CMPNY)
   	 {
   	  	 
   		 LocalInvCostingHome invCostingHome = null;
   	  	 LocalInvItemLocationHome invItemLocationHome = null;
   	       
   	     // Initialize EJB Home
   	        
   	     try {
   	         
   	    	 invCostingHome = (LocalInvCostingHome)EJBHomeFactory.
   	    	 	lookUpLocalHome(LocalInvCostingHome.JNDI_NAME, LocalInvCostingHome.class);
   	    	 invItemLocationHome = (LocalInvItemLocationHome)EJBHomeFactory.
   	    	 	lookUpLocalHome(LocalInvItemLocationHome.JNDI_NAME, LocalInvItemLocationHome.class);
   	     } 
   	     catch (NamingException ex) {
   	            
   	    	 throw new EJBException(ex.getMessage());
   	     }
   	    	
   		try {
   			
   			Collection invFifoCostings = invCostingHome.findFifoRemainingQuantityByLessThanOrEqualCstDateAndIlCodeAndBrCode(CST_DT, IL_CODE, AD_BRNCH, AD_CMPNY);
   			
   			if (invFifoCostings.size() > 0) {
   				
   				Iterator x = invFifoCostings.iterator();
   			
   	  			if (isAdjustFifo) {
   	  				
   	  				//executed during POST transaction
   	  				
   	  				double totalCost = 0d;
   	  				double cost;
   	  				
   	  				if(CST_QTY < 0) {
   	  					
   	  					//for negative quantities
   	 	  				double neededQty = -(CST_QTY);
   	 	  				
   	 	  				while(x.hasNext() && neededQty != 0) {
   	 	 	  				
   	 	 	  				LocalInvCosting invFifoCosting = (LocalInvCosting)x.next();
   	
   	 		 	  			if (invFifoCosting.getApPurchaseOrderLine() != null || invFifoCosting.getApVoucherLineItem() != null) {  				
   	 		 	  				cost = invFifoCosting.getCstItemCost() / invFifoCosting.getCstQuantityReceived();  				
   	 		 	  			} else if(invFifoCosting.getArInvoiceLineItem() != null) {
   	 		 	  				cost = invFifoCosting.getCstCostOfSales() / invFifoCosting.getCstQuantitySold();
   	 		 	  			} else if (invFifoCosting.getInvBuildUnbuildAssemblyLine() != null) {  				
   	 		 	  				cost = invFifoCosting.getCstAssemblyCost() / invFifoCosting.getCstAssemblyQuantity();
   	 		 	  			} else {
   	 		 	  				cost = invFifoCosting.getCstAdjustCost() /invFifoCosting.getCstAdjustQuantity();
   	 		 	  			}
   	
   	 	  					if(neededQty <= invFifoCosting.getCstRemainingLifoQuantity()) {
   	 	  						
   	 			  				invFifoCosting.setCstRemainingLifoQuantity(invFifoCosting.getCstRemainingLifoQuantity() - neededQty);
   	 			  				totalCost += (neededQty * cost);
   	 			  				neededQty = 0d;	 			  				 
   	 	  					} else {
   	 	  						
   	 	  						neededQty -= invFifoCosting.getCstRemainingLifoQuantity();
   	 	  						totalCost += (invFifoCosting.getCstRemainingLifoQuantity() * cost);
   	 	  						invFifoCosting.setCstRemainingLifoQuantity(0);
   	 	  					}
   	 	  				}
   	 	  				
   	 	  				//if needed qty is not yet satisfied but no more quantities to fetch, get the default cost
   	 	  				if(neededQty != 0) {
   	 	  					
   	 	  					LocalInvItemLocation invItemLocation = invItemLocationHome.findByPrimaryKey(IL_CODE);
   	 	  					totalCost += (neededQty * invItemLocation.getInvItem().getIiUnitCost());
   	 	  				}
   	 	  				
   	 	  				cost = totalCost / -CST_QTY;
   	  				} 
   	  				
   	  				else {
   	  					
   	  					//for positive quantities
   	  					cost = CST_COST;
   	  				}
   	  				return cost;
   	  			}
   	  			
   	  			else {
   	  				
   	  				//executed during ENTRY transaction
   	  				
   	  				LocalInvCosting invFifoCosting = (LocalInvCosting)x.next();
   	  				
   	 	  			if (invFifoCosting.getApPurchaseOrderLine() != null || invFifoCosting.getApVoucherLineItem() != null) {
   	 	  				return EJBCommon.roundIt(invFifoCosting.getCstItemCost() / invFifoCosting.getCstQuantityReceived(), this.getGlFcPrecisionUnit(AD_CMPNY));
   	 	  			} else if(invFifoCosting.getArInvoiceLineItem() != null) {
   		 	  			return  EJBCommon.roundIt(invFifoCosting.getCstCostOfSales() / invFifoCosting.getCstQuantitySold(), this.getGlFcPrecisionUnit(AD_CMPNY));
   	 	  			} else if (invFifoCosting.getInvBuildUnbuildAssemblyLine() != null) {
   	 	  				return EJBCommon.roundIt(invFifoCosting.getCstAssemblyCost() / invFifoCosting.getCstAssemblyQuantity(), this.getGlFcPrecisionUnit(AD_CMPNY));
   	 	  			} else {
   	 	  				return EJBCommon.roundIt(invFifoCosting.getCstAdjustCost() /invFifoCosting.getCstAdjustQuantity(), this.getGlFcPrecisionUnit(AD_CMPNY));
   	 	  			}
   	  			}
   			} 
   			else {
   				
   				//most applicable in 1st entries of data
   				LocalInvItemLocation invItemLocation = invItemLocationHome.findByPrimaryKey(IL_CODE);
   				return invItemLocation.getInvItem().getIiUnitCost();
   			}
   				
   		}
   		catch (Exception ex) {
   			Debug.printStackTrace(ex);
   		    throw new EJBException(ex.getMessage());
   		}
   	}     
    
    private double convertByUomFromAndItemAndQuantity(LocalInvUnitOfMeasure invFromUnitOfMeasure, LocalInvItem invItem, double ADJST_QTY, Integer AD_CMPNY) {
		
		Debug.print("ArMiscReceiptEntryControllerBean convertByUomFromAndItemAndQuantity");		        
        
		LocalAdPreferenceHome adPreferenceHome = null;
		LocalInvUnitOfMeasureConversionHome invUnitOfMeasureConversionHome = null;
                
        // Initialize EJB Home
        
        try {
            
            adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);   
            invUnitOfMeasureConversionHome = (LocalInvUnitOfMeasureConversionHome)EJBHomeFactory.
            	lookUpLocalHome(LocalInvUnitOfMeasureConversionHome.JNDI_NAME, LocalInvUnitOfMeasureConversionHome.class);   
                   
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
                
        try {
        
            LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);
            
           /* System.out.println(invItem.getIiName());
            System.out.println(invFromUnitOfMeasure.getUomName());*/
        	        	
            LocalInvUnitOfMeasureConversion invUnitOfMeasureConversion = invUnitOfMeasureConversionHome.findUmcByIiNameAndUomName(invItem.getIiName(), invFromUnitOfMeasure.getUomName(), AD_CMPNY);
            LocalInvUnitOfMeasureConversion invDefaultUomConversion = invUnitOfMeasureConversionHome.findUmcByIiNameAndUomName(invItem.getIiName(), invItem.getInvUnitOfMeasure().getUomName(), AD_CMPNY);
                        
        	return EJBCommon.roundIt(ADJST_QTY * invDefaultUomConversion.getUmcConversionFactor() / invUnitOfMeasureConversion.getUmcConversionFactor(), adPreference.getPrfInvQuantityPrecisionUnit());
        	       	        		        		       	
        						    		        		
        } catch (Exception ex) {
        	
        	Debug.printStackTrace(ex);
        	getSessionContext().setRollbackOnly();
        	throw new EJBException(ex.getMessage());
        	
        }
		
	}

    private double convertCostByUom(String II_NM, String UOM_NM, double unitCost, boolean isFromDefault, Integer AD_CMPNY) {
        
        Debug.print("ArInvoiceEntryControllerBean convertCostByUom");		        
        
        LocalInvItemHome invItemHome = null;
        LocalInvUnitOfMeasureConversionHome invUnitOfMeasureConversionHome = null;
        
        // Initialize EJB Home
        
        try {
            
            invUnitOfMeasureConversionHome = (LocalInvUnitOfMeasureConversionHome)EJBHomeFactory.
            lookUpLocalHome(LocalInvUnitOfMeasureConversionHome.JNDI_NAME, LocalInvUnitOfMeasureConversionHome.class);   
            invItemHome = (LocalInvItemHome)EJBHomeFactory.
            lookUpLocalHome(LocalInvItemHome.JNDI_NAME, LocalInvItemHome.class);
            
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {
            
            LocalInvItem invItem = invItemHome.findByIiName(II_NM, AD_CMPNY);
            LocalInvUnitOfMeasureConversion invUnitOfMeasureConversion = invUnitOfMeasureConversionHome.findUmcByIiNameAndUomName(II_NM, UOM_NM, AD_CMPNY);
            LocalInvUnitOfMeasureConversion invDefaultUomConversion = invUnitOfMeasureConversionHome.findUmcByIiNameAndUomName(II_NM, invItem.getInvUnitOfMeasure().getUomName(), AD_CMPNY);
            
            if (isFromDefault) {	        	
                
                return unitCost * invDefaultUomConversion.getUmcConversionFactor() / invUnitOfMeasureConversion.getUmcConversionFactor();
                
            } else {
                
                return unitCost * invUnitOfMeasureConversion.getUmcConversionFactor() / invDefaultUomConversion.getUmcConversionFactor();
                
            }
            
        } catch (Exception ex) {
            
            Debug.printStackTrace(ex);
            getSessionContext().setRollbackOnly();
            throw new EJBException(ex.getMessage());
            
        }
        
    }
    
   // SessionBean methods

   /**
    * @ejb:create-method view-type="remote"
    **/
   public void ejbCreate() throws CreateException {

      Debug.print("ArFindReceiptControllerBean ejbCreate");
      
   }

   // private methods
   	
		   
}
