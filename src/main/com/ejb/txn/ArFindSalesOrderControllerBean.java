package com.ejb.txn;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.naming.NamingException;

import com.ejb.ad.LocalAdLookUpValue;
import com.ejb.ad.LocalAdLookUpValueHome;
import com.ejb.ad.LocalAdPreference;
import com.ejb.ad.LocalAdPreferenceHome;
import com.ejb.ar.LocalArCustomer;
import com.ejb.ar.LocalArCustomerHome;
import com.ejb.ar.LocalArSalesOrder;
import com.ejb.ar.LocalArSalesOrderHome;
import com.ejb.ar.LocalArSalesOrderInvoiceLine;
import com.ejb.ar.LocalArSalesOrderLine;
import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.gl.LocalGlFunctionalCurrency;
import com.ejb.gl.LocalGlFunctionalCurrencyHome;
import com.util.AbstractSessionBean;
import com.util.ArModSalesOrderDetails;
import com.util.Debug;
import com.util.EJBCommon;
import com.util.EJBHomeFactory;

/**
 * @ejb:bean name="ArFindSalesOrderControllerEJB"
 *           display-name="Used for finding sales orders"
 *           type="Stateless"
 *           view-type="remote"
 *           jndi-name="ejb/ArFindSalesOrderControllerEJB"
 *
 * @ejb:interface remote-class="com.ejb.txn.ArFindSalesOrderController"
 *                extends="javax.ejb.EJBObject"
 *
 * @ejb:home remote-class="com.ejb.txn.ArFindSalesOrderControllerHome"
 *           extends="javax.ejb.EJBHome"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="invuser"
 *                        role-link="invuserlink"
 *
 * @ejb:permission role-name="invuser"
 *
 */

public class ArFindSalesOrderControllerBean extends AbstractSessionBean {

    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList getArCstAll(Integer AD_BRNCH, Integer AD_CMPNY) {

        Debug.print("ArFindSalesOrderControllerBean getArCstAll");

        LocalArCustomerHome arCustomerHome = null;

        ArrayList list = new ArrayList();

        // Initialize EJB Home

        try {

            arCustomerHome = (LocalArCustomerHome)EJBHomeFactory.
                lookUpLocalHome(LocalArCustomerHome.JNDI_NAME, LocalArCustomerHome.class);

        } catch (NamingException ex) {

            throw new EJBException(ex.getMessage());

        }

        try {

            Collection arCustomers = arCustomerHome.findEnabledCstAll(AD_BRNCH, AD_CMPNY);

	        Iterator i = arCustomers.iterator();

	        while (i.hasNext()) {

	        	LocalArCustomer arCustomer = (LocalArCustomer)i.next();

	        	list.add(arCustomer.getCstCustomerCode());

	        }

	        return list;

        } catch (Exception ex) {

        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());

        }

    }


    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList getArSoForProcessingByBrCode(Integer AD_BRNCH, Integer AD_CMPNY) {

        Debug.print("ArFindSalesOrderControllerBean getArSoForProcessingByBrCode");

        LocalArSalesOrderHome arSalesOrderHome = null;

        ArrayList list = new ArrayList();

        // Initialize EJB Home

        try {

        	arSalesOrderHome = (LocalArSalesOrderHome)EJBHomeFactory.
                lookUpLocalHome(LocalArSalesOrderHome.JNDI_NAME, LocalArSalesOrderHome.class);

        } catch (NamingException ex) {

            throw new EJBException(ex.getMessage());

        }

        try {

            Collection arSalesOrderColl = arSalesOrderHome.findPostedSoByBrCode(AD_BRNCH, AD_CMPNY);

	        Iterator i = arSalesOrderColl.iterator();

	        while (i.hasNext()) {

	        	LocalArSalesOrder arSalesOrder = (LocalArSalesOrder)i.next();


	        	if (!((arSalesOrder.getSoApprovalStatus().equals("APPROVED") || arSalesOrder.getSoApprovalStatus().equals("N/A")) &&
		    			  arSalesOrder.getSoLock()==(byte)(0))) continue;

        		ArModSalesOrderDetails mdetails = new ArModSalesOrderDetails();
        		mdetails.setSoCode(arSalesOrder.getSoCode());
        		mdetails.setSoDocumentType(arSalesOrder.getSoDocumentType());
        		mdetails.setSoCstCustomerCode(arSalesOrder.getArCustomer().getCstCustomerCode());
        		mdetails.setSoCstName(arSalesOrder.getArCustomer().getCstName());
        		mdetails.setSoDate(arSalesOrder.getSoDate());
        		mdetails.setSoDocumentNumber(arSalesOrder.getSoDocumentNumber());
        		mdetails.setSoReferenceNumber(arSalesOrder.getSoReferenceNumber());
        		mdetails.setSoTransactionType(arSalesOrder.getSoTransactionType());

        		list.add(mdetails);

	        }

	        return list;

        } catch (Exception ex) {

        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());

        }

    }

    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList getGlFcAll(Integer AD_CMPNY) {

        Debug.print("ApFindSalesOrderControllerBean getGlFcAll");

        LocalGlFunctionalCurrencyHome glFunctionalCurrencyHome = null;

        ArrayList list = new ArrayList();

        // Initialize EJB Home

        try {

            glFunctionalCurrencyHome = (LocalGlFunctionalCurrencyHome)EJBHomeFactory.
                lookUpLocalHome(LocalGlFunctionalCurrencyHome.JNDI_NAME, LocalGlFunctionalCurrencyHome.class);

        } catch (NamingException ex) {

            throw new EJBException(ex.getMessage());

        }

        try {

            Collection glFunctionalCurrencies = glFunctionalCurrencyHome.findFcAll(AD_CMPNY);

	        Iterator i = glFunctionalCurrencies.iterator();

	        while (i.hasNext()) {

	        	LocalGlFunctionalCurrency glFunctionalCurrency = (LocalGlFunctionalCurrency)i.next();

	        	list.add(glFunctionalCurrency.getFcName());

	        }

	        return list;

        } catch (Exception ex) {

        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());

        }

    }

    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList getArSoByCriteria(HashMap criteria, boolean isChild,
        Integer OFFSET, Integer LIMIT, String ORDER_BY, Integer AD_BRNCH, Integer AD_CMPNY)
        throws GlobalNoRecordFoundException {

        Debug.print("ArFindSalesOrderControllerBean getArSoByCriteria");

        LocalArSalesOrderHome arSalesOrderHome = null;

        ArrayList list = new ArrayList();

        //initialized EJB Home

        try {

            arSalesOrderHome = (LocalArSalesOrderHome)EJBHomeFactory.
                lookUpLocalHome(LocalArSalesOrderHome.JNDI_NAME, LocalArSalesOrderHome.class);

        } catch (NamingException ex) {

            throw new EJBException(ex.getMessage());

        }

        try {

        	StringBuffer jbossQl = new StringBuffer();
        	jbossQl.append("SELECT OBJECT(so) FROM ArSalesOrder so ");

        	boolean firstArgument = true;
        	short ctr = 0;
        	int criteriaSize = criteria.size() + 2;

        	Object obj[];

        	// Allocate the size of the object parameter

        	if (criteria.containsKey("referenceNumber")) {

        		criteriaSize--;

        	}
        	
        	if (criteria.containsKey("documentType")) {

        		criteriaSize--;

        	}

        	if (criteria.containsKey("transactionType")) {

        		criteriaSize--;

        	}

        	if (criteria.containsKey("approvalStatus")) {

		      	 String approvalStatus = (String)criteria.get("approvalStatus");

		      	 if (approvalStatus.equals("DRAFT") || approvalStatus.equals("REJECTED")) {

		      	 	 criteriaSize--;

		      	 }

	       }

        	obj = new Object[criteriaSize];

        	if (criteria.containsKey("referenceNumber")) {

        		if (!firstArgument) {

        			jbossQl.append("AND ");

        		} else {

        			firstArgument = false;
        			jbossQl.append("WHERE ");

        		}

        		jbossQl.append("so.soReferenceNumber LIKE '%" + (String)criteria.get("referenceNumber") + "%' ");

        	}
        	
        	if (criteria.containsKey("documentType")) {

        		if (!firstArgument) {

        			jbossQl.append("AND ");

        		} else {

        			firstArgument = false;
        			jbossQl.append("WHERE ");

        		}

        		jbossQl.append("so.soDocumentType = '" + (String)criteria.get("documentType") + "' ");

        	}

        	
        	

        	if (criteria.containsKey("transactionType")) {

        		if (!firstArgument) {

        			jbossQl.append("AND ");

        		} else {

        			firstArgument = false;
        			jbossQl.append("WHERE ");

        		}

        		jbossQl.append("so.soTransactionType = '" + (String)criteria.get("transactionType") + "' ");

        	}



        	if (criteria.containsKey("customerCode")) {

			   	  if (!firstArgument) {
			   	     jbossQl.append("AND ");
			   	  } else {
			   	  	 firstArgument = false;
			   	  	 jbossQl.append("WHERE ");
			   	  }

			   	  jbossQl.append("so.arCustomer.cstCustomerCode=?" + (ctr+1) + " ");
			   	  obj[ctr] = (String)criteria.get("customerCode");
			   	  ctr++;

	        }

	        if (!firstArgument) {
	  	 	  jbossQl.append("AND ");
	  	    } else {
	  	 	  firstArgument = false;
	  	 	  jbossQl.append("WHERE ");
	  	    }

	  	    jbossQl.append("so.soVoid=?" + (ctr+1) + " ");
	  	    obj[ctr] = (Byte)criteria.get("salesOrderVoid");
	        ctr++;

	        if (criteria.containsKey("currency")) {

	       	  if (!firstArgument) {

	       	     jbossQl.append("AND ");

	       	  } else {

	       	  	 firstArgument = false;
	       	  	 jbossQl.append("WHERE ");

	       	  }

	       	  jbossQl.append("so.glFunctionalCurrency.fcName=?" + (ctr+1) + " ");
	       	  obj[ctr] = (String)criteria.get("currency");
	       	  ctr++;

	      }

	      if (criteria.containsKey("approvalStatus")) {

	       	  if (!firstArgument) {

	       	     jbossQl.append("AND ");

	       	  } else {

	       	  	 firstArgument = false;
	       	  	 jbossQl.append("WHERE ");

	       	  }

	       	  String approvalStatus = (String)criteria.get("approvalStatus");

	       	  if (approvalStatus.equals("DRAFT")) {

		       	  jbossQl.append("so.soApprovalStatus IS NULL ");

	       	  } else if (approvalStatus.equals("REJECTED")) {

		       	  jbossQl.append("so.soReasonForRejection IS NOT NULL ");

	      	  } else {

		      	  jbossQl.append("so.soApprovalStatus=?" + (ctr+1) + " ");
		       	  obj[ctr] = approvalStatus;
		       	  ctr++;

	      	  }

	      }


	      if (criteria.containsKey("posted")) {

	       	  if (!firstArgument) {

	       	     jbossQl.append("AND ");

	       	  } else {

	       	  	 firstArgument = false;
	       	  	 jbossQl.append("WHERE ");

	       	  }

	       	  jbossQl.append("so.soPosted=?" + (ctr+1) + " ");

	       	  String posted = (String)criteria.get("posted");

	       	  if (posted.equals("YES")) {

	       	  	obj[ctr] = new Byte(EJBCommon.TRUE);

	       	  } else {

	       	  	obj[ctr] = new Byte(EJBCommon.FALSE);

	       	  }

	       	  ctr++;

	      }

        	if (criteria.containsKey("documentNumberFrom")) {

        		if (!firstArgument) {

        			jbossQl.append("AND ");

        		} else {

        			firstArgument = false;
        			jbossQl.append("WHERE ");

        		}

        		jbossQl.append("so.soDocumentNumber>=?" + (ctr+1) + " ");
        		obj[ctr] = (String)criteria.get("documentNumberFrom");
        		ctr++;

        	}

        	if (criteria.containsKey("documentNumberTo")) {

        		if (!firstArgument) {

        			jbossQl.append("AND ");

        		} else {

        			firstArgument = false;
        			jbossQl.append("WHERE ");

        		}

        		jbossQl.append("so.soDocumentNumber<=?" + (ctr+1) + " ");
        		obj[ctr] = (String)criteria.get("documentNumberTo");
        		ctr++;

        	}

        	if (criteria.containsKey("dateFrom")) {

        		if (!firstArgument) {
        			jbossQl.append("AND ");
        		} else {
        			firstArgument = false;
        			jbossQl.append("WHERE ");
        		}
        		jbossQl.append("so.soDate>=?" + (ctr+1) + " ");
        		obj[ctr] = (Date)criteria.get("dateFrom");
        		ctr++;
        	}

        	if (criteria.containsKey("dateTo")) {

        		if (!firstArgument) {
        			jbossQl.append("AND ");
        		} else {
        			firstArgument = false;
        			jbossQl.append("WHERE ");
        		}
        		jbossQl.append("so.soDate<=?" + (ctr+1) + " ");
        		obj[ctr] = (Date)criteria.get("dateTo");
        		ctr++;

        	}

//s

        	if (criteria.containsKey("approvalDateFrom")) {

        		if (!firstArgument) {
        			jbossQl.append("AND ");
        		} else {
        			firstArgument = false;
        			jbossQl.append("WHERE ");
        		}
        		jbossQl.append("so.soDateApprovedRejected>=?" + (ctr+1) + " ");
        		obj[ctr] = (Date)criteria.get("approvalDateFrom");
        		Date sample1 = (Date)criteria.get("approvalDateFrom");
        		System.out.print("we: " + obj[ctr].toString());
        		ctr++;
        		System.out.print(sample1.toString());
        	}

        	if (criteria.containsKey("approvalDateTo")) {

        		if (!firstArgument) {
        			jbossQl.append("AND ");
        		} else {
        			firstArgument = false;
        			jbossQl.append("WHERE ");
        		}
        		jbossQl.append("so.soDateApprovedRejected<=?" + (ctr+1) + " ");
        		obj[ctr] = (Date)criteria.get("approvalDateTo");
        		ctr++;

        	}

        	if (!firstArgument) {

        		jbossQl.append("AND ");

        	} else {

        		firstArgument = false;
        		jbossQl.append("WHERE ");

        	}

        	//e

        	jbossQl.append("so.soAdBranch=" + AD_BRNCH + " AND so.soAdCompany=" + AD_CMPNY + " ");

        	if(isChild){

        		jbossQl.append(" AND so.soLock=0 ");

        	}

        	String orderBy = null;

        	if (ORDER_BY.equals("CUSTOMER CODE")) {

		  	  orderBy = "so.arCustomer.cstCustomerCode";

		    } else if (ORDER_BY.equals("DOCUMENT NUMBER")) {

		  	  orderBy = "so.soDocumentNumber";

		    }

        	if (orderBy != null) {

        		jbossQl.append("ORDER BY " + orderBy + ", so.soDate");

        	} else {

        		jbossQl.append("ORDER BY so.soDate");

        	}

        	jbossQl.append(" OFFSET ?" + (ctr + 1));
        	obj[ctr] = OFFSET;
        	ctr++;

        	jbossQl.append(" LIMIT ?" + (ctr + 1));
        	obj[ctr] = LIMIT;
        	ctr++;

       
        	Collection arSalesOrders = arSalesOrderHome.getSOByCriteria(jbossQl.toString(), obj);
     ;
        	if (arSalesOrders.size() <= 0)
        		throw new GlobalNoRecordFoundException();

        	Iterator i = arSalesOrders.iterator();

        	while (i.hasNext()) {
        		
        		LocalArSalesOrder arSalesOrder = (LocalArSalesOrder)i.next();

        		// Check If Sales Order If Completely Delivered
        		/*if(isChild){

        			Collection arSalesOrderLines = arSalesOrder.getArSalesOrderLines();

        			boolean isOpenSO = false;

                    Iterator slIter = arSalesOrderLines.iterator();

                    while(slIter.hasNext()) {

                    	LocalArSalesOrderLine arSalesOrderLine = (LocalArSalesOrderLine) slIter.next();

                    	Iterator soInvLnIter = arSalesOrderLine.getArSalesOrderInvoiceLines().iterator();
                        double QUANTITY_SOLD = 0d;

                        while(soInvLnIter.hasNext()) {

                            LocalArSalesOrderInvoiceLine arSalesOrderInvoiceLine = (LocalArSalesOrderInvoiceLine) soInvLnIter.next();

                            if (arSalesOrderInvoiceLine.getArInvoice().getInvPosted() == EJBCommon.TRUE) {

                                QUANTITY_SOLD += arSalesOrderInvoiceLine.getSilQuantityDelivered();

                            }

                        }

                        double TOTAL_REMAINING_QTY = arSalesOrderLine.getSolQuantity() - QUANTITY_SOLD;

                        if(TOTAL_REMAINING_QTY > 0) {
                        	isOpenSO = true;
                        	break;
                        }

                    }

                    if(!isOpenSO) continue;

        		}*/
        	
        		ArModSalesOrderDetails mdetails = new ArModSalesOrderDetails();
        		mdetails.setSoDocumentType(arSalesOrder.getSoDocumentType());
        		mdetails.setSoCode(arSalesOrder.getSoCode());
        		mdetails.setSoCstCustomerCode(arSalesOrder.getArCustomer().getCstCustomerCode());
        		mdetails.setSoCstName(arSalesOrder.getArCustomer().getCstName());
        		mdetails.setSoDate(arSalesOrder.getSoDate());
        		mdetails.setSoDocumentNumber(arSalesOrder.getSoDocumentNumber());
        		mdetails.setSoReferenceNumber(arSalesOrder.getSoReferenceNumber());
        		mdetails.setSoTransactionType(arSalesOrder.getSoTransactionType());

        		list.add(mdetails);

        	}

        	return list;

        } catch (GlobalNoRecordFoundException ex) {

	  	  throw ex;

	  } catch (Exception ex) {


	  	  ex.printStackTrace();
	  	  throw new EJBException(ex.getMessage());

	  }

    }

    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public Integer getArSoSizeByCriteria(HashMap criteria, Integer AD_BRNCH, Integer AD_CMPNY)
        throws GlobalNoRecordFoundException {

        Debug.print("ArFindSalesOrderControllerBean getArSoSizeByCriteria");

        LocalArSalesOrderHome arSalesOrderHome = null;

        //initialized EJB Home

        try {

            arSalesOrderHome = (LocalArSalesOrderHome)EJBHomeFactory.
                lookUpLocalHome(LocalArSalesOrderHome.JNDI_NAME, LocalArSalesOrderHome.class);

        } catch (NamingException ex) {

            throw new EJBException(ex.getMessage());

        }

        try {

        	StringBuffer jbossQl = new StringBuffer();
        	jbossQl.append("SELECT OBJECT(so) FROM ArSalesOrder so ");

        	boolean firstArgument = true;
        	short ctr = 0;
        	int criteriaSize = criteria.size();

        	Object obj[];

        	// Allocate the size of the object parameter

        	if (criteria.containsKey("documentType")) {

        		criteriaSize--;

        	}
        	
        	if (criteria.containsKey("referenceNumber")) {

        		criteriaSize--;

        	}

        	if (criteria.containsKey("transactionType")) {

        		criteriaSize--;

        	}

        	if (criteria.containsKey("approvalStatus")) {

		      	 String approvalStatus = (String)criteria.get("approvalStatus");

		      	 if (approvalStatus.equals("DRAFT") || approvalStatus.equals("REJECTED")) {

		      	 	 criteriaSize--;

		      	 }

	       }

        	obj = new Object[criteriaSize];

        	if (criteria.containsKey("referenceNumber")) {

        		if (!firstArgument) {

        			jbossQl.append("AND ");

        		} else {

        			firstArgument = false;
        			jbossQl.append("WHERE ");

        		}

        		jbossQl.append("so.soReferenceNumber LIKE '%" + (String)criteria.get("referenceNumber") + "%' ");

        	}
        	
        	if (criteria.containsKey("documentType")) {

        		if (!firstArgument) {

        			jbossQl.append("AND ");

        		} else {

        			firstArgument = false;
        			jbossQl.append("WHERE ");

        		}

        		jbossQl.append("so.soDocumentType  = '" + (String)criteria.get("documentType") + "' ");

        	}
        	

        	if (criteria.containsKey("transactionType")) {

        		if (!firstArgument) {

        			jbossQl.append("AND ");

        		} else {

        			firstArgument = false;
        			jbossQl.append("WHERE ");

        		}

        		jbossQl.append("so.soTransactionType  = '" + (String)criteria.get("transactionType") + "' ");

        	}

        	if (criteria.containsKey("customerCode")) {

			   	  if (!firstArgument) {
			   	     jbossQl.append("AND ");
			   	  } else {
			   	  	 firstArgument = false;
			   	  	 jbossQl.append("WHERE ");
			   	  }

			   	  jbossQl.append("so.arCustomer.cstCustomerCode=?" + (ctr+1) + " ");
			   	  obj[ctr] = (String)criteria.get("customerCode");
			   	  ctr++;

	        }

	        if (!firstArgument) {
	  	 	  jbossQl.append("AND ");
	  	    } else {
	  	 	  firstArgument = false;
	  	 	  jbossQl.append("WHERE ");
	  	    }

	  	    jbossQl.append("so.soVoid=?" + (ctr+1) + " ");
	  	    obj[ctr] = (Byte)criteria.get("salesOrderVoid");
	        ctr++;

	        if (criteria.containsKey("currency")) {

	       	  if (!firstArgument) {

	       	     jbossQl.append("AND ");

	       	  } else {

	       	  	 firstArgument = false;
	       	  	 jbossQl.append("WHERE ");

	       	  }

	       	  jbossQl.append("so.glFunctionalCurrency.fcName=?" + (ctr+1) + " ");
	       	  obj[ctr] = (String)criteria.get("currency");
	       	  ctr++;

	      }

	      if (criteria.containsKey("approvalStatus")) {

	       	  if (!firstArgument) {

	       	     jbossQl.append("AND ");

	       	  } else {

	       	  	 firstArgument = false;
	       	  	 jbossQl.append("WHERE ");

	       	  }

	       	  String approvalStatus = (String)criteria.get("approvalStatus");

	       	  if (approvalStatus.equals("DRAFT")) {

		       	  jbossQl.append("so.soApprovalStatus IS NULL ");

	       	  } else if (approvalStatus.equals("REJECTED")) {

		       	  jbossQl.append("so.soReasonForRejection IS NOT NULL ");

	      	  } else {

		      	  jbossQl.append("so.soApprovalStatus=?" + (ctr+1) + " ");
		       	  obj[ctr] = approvalStatus;
		       	  ctr++;

	      	  }

	      }


	      if (criteria.containsKey("posted")) {

	       	  if (!firstArgument) {

	       	     jbossQl.append("AND ");

	       	  } else {

	       	  	 firstArgument = false;
	       	  	 jbossQl.append("WHERE ");

	       	  }

	       	  jbossQl.append("so.soPosted=?" + (ctr+1) + " ");

	       	  String posted = (String)criteria.get("posted");

	       	  if (posted.equals("YES")) {

	       	  	obj[ctr] = new Byte(EJBCommon.TRUE);

	       	  } else {

	       	  	obj[ctr] = new Byte(EJBCommon.FALSE);

	       	  }

	       	  ctr++;

	      }

        	if (criteria.containsKey("documentNumberFrom")) {

        		if (!firstArgument) {

        			jbossQl.append("AND ");

        		} else {

        			firstArgument = false;
        			jbossQl.append("WHERE ");

        		}

        		jbossQl.append("so.soDocumentNumber>=?" + (ctr+1) + " ");
        		obj[ctr] = (String)criteria.get("documentNumberFrom");
        		ctr++;

        	}

        	if (criteria.containsKey("documentNumberTo")) {

        		if (!firstArgument) {

        			jbossQl.append("AND ");

        		} else {

        			firstArgument = false;
        			jbossQl.append("WHERE ");

        		}

        		jbossQl.append("so.soDocumentNumber<=?" + (ctr+1) + " ");
        		obj[ctr] = (String)criteria.get("documentNumberTo");
        		ctr++;

        	}

        	if (criteria.containsKey("dateFrom")) {

        		if (!firstArgument) {
        			jbossQl.append("AND ");
        		} else {
        			firstArgument = false;
        			jbossQl.append("WHERE ");
        		}
        		jbossQl.append("so.soDate>=?" + (ctr+1) + " ");
        		obj[ctr] = (Date)criteria.get("dateFrom");
        		ctr++;
        	}

        	if (criteria.containsKey("dateTo")) {

        		if (!firstArgument) {
        			jbossQl.append("AND ");
        		} else {
        			firstArgument = false;
        			jbossQl.append("WHERE ");
        		}
        		jbossQl.append("so.soDate<=?" + (ctr+1) + " ");
        		obj[ctr] = (Date)criteria.get("dateTo");
        		ctr++;

        	}

//s

        	if (criteria.containsKey("approvalDateFrom")) {

        		if (!firstArgument) {
        			jbossQl.append("AND ");
        		} else {
        			firstArgument = false;
        			jbossQl.append("WHERE ");
        		}
        		jbossQl.append("so.soDateApprovedRejected>=?" + (ctr+1) + " ");
        		obj[ctr] = (Date)criteria.get("approvalDateFrom");
        		Date sample1 = (Date)criteria.get("approvalDateFrom");
        		System.out.print("we: " + obj[ctr].toString());
        		ctr++;
        		System.out.print(sample1.toString());
        	}

        	if (criteria.containsKey("approvalDateTo")) {

        		if (!firstArgument) {
        			jbossQl.append("AND ");
        		} else {
        			firstArgument = false;
        			jbossQl.append("WHERE ");
        		}
        		jbossQl.append("so.soDateApprovedRejected<=?" + (ctr+1) + " ");
        		obj[ctr] = (Date)criteria.get("approvalDateTo");
        		ctr++;

        	}

        	if (!firstArgument) {

        		jbossQl.append("AND ");

        	} else {

        		firstArgument = false;
        		jbossQl.append("WHERE ");

        	}

        	jbossQl.append("so.soAdBranch=" + AD_BRNCH + " AND so.soAdCompany=" + AD_CMPNY + " ");

        	Collection arSalesOrders = arSalesOrderHome.getSOByCriteria(jbossQl.toString(), obj);

        	if (arSalesOrders.size() == 0)
        		throw new GlobalNoRecordFoundException();

        	return new Integer(arSalesOrders.size());

        } catch (GlobalNoRecordFoundException ex) {

	  	  throw ex;

	  } catch (Exception ex) {


	  	  ex.printStackTrace();
	  	  throw new EJBException(ex.getMessage());

	  }

    }

    /**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public byte getAdPrfArUseCustomerPulldown(Integer AD_CMPNY) {

		Debug.print("ArFindSalesOrderControllerBean getAdPrfArUseCustomerPulldown");

		LocalAdPreferenceHome adPreferenceHome = null;

		// Initialize EJB Home

		try {

			adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}

		try {

			LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);

			return  adPreference.getPrfArUseCustomerPulldown();

		} catch (Exception ex) {

			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());

		}

	}

	
	/**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList getArSoDocumentTypeList(Integer AD_CMPNY) {

        Debug.print("ArFindSalesOrderEntryControllerBean getArSoDocumentTypeList");

        ArrayList list = new ArrayList();
        LocalAdLookUpValueHome adLookUpValueHome = null;


        // Initialize EJB Home

        try {

        	adLookUpValueHome = (LocalAdLookUpValueHome)EJBHomeFactory.
            lookUpLocalHome(LocalAdLookUpValueHome.JNDI_NAME, LocalAdLookUpValueHome.class);

        } catch (NamingException ex) {

            throw new EJBException(ex.getMessage());

        }


        try {

        	Collection adLookUpValues = adLookUpValueHome.findByLuName("AR SALES ORDER DOCUMENT TYPE", AD_CMPNY);

        	if(adLookUpValues.size() <=0 ) {
        		return list;
        	}

        	Iterator i = adLookUpValues.iterator();

        	while(i.hasNext()) {

        		LocalAdLookUpValue adLookUpValue = (LocalAdLookUpValue)i.next();
        		System.out.println(adLookUpValue.getLvName());
        		list.add(adLookUpValue.getLvName());
        	}


        	return list;



        } catch (Exception ex) {

            Debug.printStackTrace(ex);
            throw new EJBException(ex.getMessage());

        }

    }
	
	
	


    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList getArSoTransactionTypeList(Integer AD_CMPNY) {

        Debug.print("ArFindSalesOrderEntryControllerBean getArSalesOrderType");

        ArrayList list = new ArrayList();
        LocalAdLookUpValueHome adLookUpValueHome = null;


        // Initialize EJB Home

        try {

        	adLookUpValueHome = (LocalAdLookUpValueHome)EJBHomeFactory.
            lookUpLocalHome(LocalAdLookUpValueHome.JNDI_NAME, LocalAdLookUpValueHome.class);

        } catch (NamingException ex) {

            throw new EJBException(ex.getMessage());

        }


        try {

        	Collection adLookUpValues = adLookUpValueHome.findByLuName("AR SALES ORDER TRANSACTION TYPE", AD_CMPNY);

        	if(adLookUpValues.size() <=0 ) {
        		return list;
        	}

        	Iterator i = adLookUpValues.iterator();

        	while(i.hasNext()) {

        		LocalAdLookUpValue adLookUpValue = (LocalAdLookUpValue)i.next();
        		System.out.println(adLookUpValue.getLvName());
        		list.add(adLookUpValue.getLvName());
        	}


        	return list;



        } catch (Exception ex) {

            Debug.printStackTrace(ex);
            throw new EJBException(ex.getMessage());

        }

    }


    // SessionBean methods

    /**
     * @ejb:create-method view-type="remote"
     **/
    public void ejbCreate() throws CreateException {

       Debug.print("ApFindSalesOrderControllerBean ejbCreate");

    }
}
