
/*
 * ArGlJournalInterfaceControllerBean.java
 *
 * Created on March 10, 2004, 01:17 PM
 *
 * @author  Dennis M. Hilario
 */

package com.ejb.txn;

import java.util.Collection;
import java.util.Date;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.naming.NamingException;

import com.ejb.ad.LocalAdCompany;
import com.ejb.ad.LocalAdCompanyHome;
import com.ejb.ar.LocalArDistributionRecord;
import com.ejb.ar.LocalArDistributionRecordHome;
import com.ejb.ar.LocalArInvoice;
import com.ejb.ar.LocalArInvoiceHome;
import com.ejb.ar.LocalArReceipt;
import com.ejb.ar.LocalArReceiptHome;
import com.ejb.gl.LocalGlFunctionalCurrency;
import com.ejb.gl.LocalGlFunctionalCurrencyRate;
import com.ejb.gl.LocalGlFunctionalCurrencyRateHome;
import com.ejb.gl.LocalGlJournalInterface;
import com.ejb.gl.LocalGlJournalInterfaceHome;
import com.ejb.gl.LocalGlJournalLineInterface;
import com.ejb.gl.LocalGlJournalLineInterfaceHome;
import com.util.AbstractSessionBean;
import com.util.Debug;
import com.util.EJBCommon;
import com.util.EJBHomeFactory;

/**
 * @ejb:bean name="ArGlJournalInterfaceControllerEJB"
 *           display-name="Used for journal interface run"
 *           type="Stateless"
 *           view-type="remote"
 *           jndi-name="ejb/ArGlJournalInterfaceControllerEJB"
 *
 * @ejb:interface remote-class="com.ejb.txn.ArGlJournalInterfaceController"
 *                extends="javax.ejb.EJBObject"
 *
 * @ejb:home remote-class="com.ejb.txn.ArGlJournalInterfaceControllerHome"
 *           extends="javax.ejb.EJBHome"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="apuser"
 *                        role-link="apuserlink"
 *
 * @ejb:permission role-name="apuser"
 * 
*/

public class ArGlJournalInterfaceControllerBean extends AbstractSessionBean {

    /**
     * @ejb:interface-method view-type="remote"
     **/
    public long executeArGlJriRun(Date JRI_DT_FRM, Date JRI_DT_TO, Integer AD_BRNCH, Integer AD_CMPNY) {
                    
        Debug.print("ArGlJournalInterfaceControllerBean executeApRctPost");
        
        LocalGlJournalInterfaceHome glJournalInterfaceHome = null;
        LocalGlJournalLineInterfaceHome glJournalLineInterfaceHome = null;
        LocalArDistributionRecordHome arDistributionRecordHome = null;        
        LocalArInvoiceHome arInvoiceHome = null;
        LocalArReceiptHome arReceiptHome = null;
        LocalAdCompanyHome adCompanyHome = null;
        
        java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat("MM/dd/yyyy HH:mm:ss.SSSS"); 
        long IMPORTED_JOURNALS = 0L;
        short lineNumber = 0;
                       
                
        // Initialize EJB Home
        
        try {
            
            glJournalInterfaceHome = (LocalGlJournalInterfaceHome)EJBHomeFactory.
                lookUpLocalHome(LocalGlJournalInterfaceHome.JNDI_NAME, LocalGlJournalInterfaceHome.class);
            glJournalLineInterfaceHome = (LocalGlJournalLineInterfaceHome)EJBHomeFactory.
                lookUpLocalHome(LocalGlJournalLineInterfaceHome.JNDI_NAME, LocalGlJournalLineInterfaceHome.class);
            arDistributionRecordHome = (LocalArDistributionRecordHome)EJBHomeFactory.
                lookUpLocalHome(LocalArDistributionRecordHome.JNDI_NAME, LocalArDistributionRecordHome.class);
            arInvoiceHome = (LocalArInvoiceHome)EJBHomeFactory.
                lookUpLocalHome(LocalArInvoiceHome.JNDI_NAME, LocalArInvoiceHome.class);
            arReceiptHome = (LocalArReceiptHome)EJBHomeFactory.
                lookUpLocalHome(LocalArReceiptHome.JNDI_NAME, LocalArReceiptHome.class);    
            adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);    
                
                                    
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }       
        
        try {   
      
            LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
            
            LocalGlFunctionalCurrency glFunctionalCurrency = adCompany.getGlFunctionalCurrency();
                            
            // invoice
            
            Collection arInvoices = arInvoiceHome.findPostedInvByInvCreditMemoAndInvDateRange(EJBCommon.FALSE, JRI_DT_FRM, JRI_DT_TO, AD_CMPNY);
            
            Iterator i = arInvoices.iterator();
            
            while (i.hasNext()) {
            	
            	LocalArInvoice arInvoice = (LocalArInvoice)i.next();
            	
            	Collection arDistributionRecords = arDistributionRecordHome.findImportableDrByInvCode(arInvoice.getInvCode(), AD_CMPNY);
            	            	            	
            	if (!arDistributionRecords.isEmpty()) {
            		
            		LocalGlJournalInterface glJournalInterface = 
            		     glJournalInterfaceHome.create(arInvoice.getInvReferenceNumber(),
            		         arInvoice.getInvDescription(), arInvoice.getInvDate(), "SALES INVOICES", "ACCOUNTS RECEIVABLES",
            		         glFunctionalCurrency.getFcName(), null, arInvoice.getInvNumber(), 
            		         null, 1d, 'N', EJBCommon.FALSE, arInvoice.getArCustomer().getCstTin(),
							 arInvoice.getArCustomer().getCstName(), null, AD_BRNCH, AD_CMPNY);
            		         
            		IMPORTED_JOURNALS++;
            		lineNumber = 0;
            		
            		Iterator j = arDistributionRecords.iterator();
            	
	            	while (j.hasNext()) {
	            		
	            		LocalArDistributionRecord arDistributionRecord = (LocalArDistributionRecord)j.next();
	            	    
	            	    LocalGlJournalLineInterface glJournalLineInterface = glJournalLineInterfaceHome.create(
	        		    	++lineNumber,
	        		    	arDistributionRecord.getDrDebit(),
	        		    	this.convertForeignToFunctionalCurrency(arInvoice.getGlFunctionalCurrency().getFcCode(),
	        		    	    arInvoice.getGlFunctionalCurrency().getFcName(), 
	        		    	    arInvoice.getInvConversionDate(), arInvoice.getInvConversionRate(), arDistributionRecord.getDrAmount(), AD_CMPNY),
	        		    	arDistributionRecord.getGlChartOfAccount().getCoaAccountNumber(), AD_CMPNY);
	        		    	
	        		    glJournalInterface.addGlJournalLineInterface(glJournalLineInterface);
	        		    	
	        		    arDistributionRecord.setDrImported(EJBCommon.TRUE);	        		    	
	            	    		            			            		
	            			            		
	            	}
	            	
            	}
            	
            }            
            
            // credit memos
            
            arInvoices = arInvoiceHome.findPostedInvByInvCreditMemoAndInvDateRange(EJBCommon.TRUE, JRI_DT_FRM, JRI_DT_TO, AD_CMPNY);
            
            i = arInvoices.iterator();
            
            while (i.hasNext()) {
            	
            	LocalArInvoice arInvoice = (LocalArInvoice)i.next();
            	
            	LocalArInvoice arCreditedInvoice = arInvoiceHome.findByInvNumberAndInvCreditMemoAndBrCode(arInvoice.getInvCmInvoiceNumber(), EJBCommon.FALSE, AD_BRNCH, AD_CMPNY);
            	
            	Collection arDistributionRecords = arDistributionRecordHome.findImportableDrByInvCode(arInvoice.getInvCode(), AD_CMPNY);
            	            	            	
            	if (!arDistributionRecords.isEmpty()) {
            		
            		LocalGlJournalInterface glJournalInterface = 
            		     glJournalInterfaceHome.create(arInvoice.getInvReferenceNumber(),
            		         arInvoice.getInvDescription(), arInvoice.getInvDate(), "CREDIT MEMOS", "ACCOUNTS RECEIVABLES",
            		         glFunctionalCurrency.getFcName(), null, arInvoice.getInvNumber(), 
            		         null, 1d, 'N', EJBCommon.FALSE, arInvoice.getArCustomer().getCstTin(),
							 arInvoice.getArCustomer().getCstName(), null, AD_BRNCH, AD_CMPNY);
            		         
            		IMPORTED_JOURNALS++;
            		lineNumber = 0;
            		
            		Iterator j = arDistributionRecords.iterator();
            	
	            	while (j.hasNext()) {
	            		
	            		LocalArDistributionRecord arDistributionRecord = (LocalArDistributionRecord)j.next();
	            	    
	            	    LocalGlJournalLineInterface glJournalLineInterface = glJournalLineInterfaceHome.create(
	        		    	++lineNumber,
	        		    	arDistributionRecord.getDrDebit(),
	        		    	this.convertForeignToFunctionalCurrency(arCreditedInvoice.getGlFunctionalCurrency().getFcCode(),
	        		    	    arCreditedInvoice.getGlFunctionalCurrency().getFcName(), 
	        		    	    arCreditedInvoice.getInvConversionDate(), arCreditedInvoice.getInvConversionRate(), arDistributionRecord.getDrAmount(), AD_CMPNY),
	        		    	arDistributionRecord.getGlChartOfAccount().getCoaAccountNumber(), AD_CMPNY);
	        		    	
	        		    glJournalInterface.addGlJournalLineInterface(glJournalLineInterface);
	        		    	
	        		    arDistributionRecord.setDrImported(EJBCommon.TRUE);	        		    	
	            	    		            			            		
	            			            		
	            	}
	            	
            	}
            	
            }
            
            
            // receipts
            
            Collection arReceipts = arReceiptHome.findPostedRctByRctDateRange(JRI_DT_FRM, JRI_DT_TO, AD_CMPNY);
            
            i = arReceipts.iterator();
            
            while (i.hasNext()) {
            	
            	LocalArReceipt arReceipt = (LocalArReceipt)i.next();
            	            	
            	Collection arDistributionRecords = arDistributionRecordHome.findImportableDrByDrReversedAndRctCode(EJBCommon.FALSE, arReceipt.getRctCode(), AD_CMPNY);
            	            	            	
            	if (!arDistributionRecords.isEmpty()) {
            		
            		LocalGlJournalInterface glJournalInterface = 
            		     glJournalInterfaceHome.create(arReceipt.getRctReferenceNumber(),
            		         arReceipt.getRctDescription(), arReceipt.getRctDate(), "SALES RECEIPTS", "ACCOUNTS RECEIVABLES",
            		         glFunctionalCurrency.getFcName(), null, arReceipt.getRctNumber(), 
            		         null, 1d, 'N', EJBCommon.FALSE, arReceipt.getArCustomer().getCstTin(),
							 arReceipt.getArCustomer().getCstName(), null, AD_BRNCH, AD_CMPNY);
            		         
            		IMPORTED_JOURNALS++;
            		lineNumber = 0;
            		
            		Iterator j = arDistributionRecords.iterator();
            	
	            	while (j.hasNext()) {
	            		
	            		LocalArDistributionRecord arDistributionRecord = (LocalArDistributionRecord)j.next();
	            		
	            		double JLI_AMNT = 0;
	            		
	            		if (arDistributionRecord.getArAppliedInvoice() != null) {
	            			
	            			LocalArInvoice arInvoice = arDistributionRecord.getArAppliedInvoice().getArInvoicePaymentSchedule().getArInvoice();
	            			
	            			JLI_AMNT = this.convertForeignToFunctionalCurrency(arInvoice.getGlFunctionalCurrency().getFcCode(),
	        		    	    arInvoice.getGlFunctionalCurrency().getFcName(), 
	        		    	    arInvoice.getInvConversionDate(), arInvoice.getInvConversionRate(), arDistributionRecord.getDrAmount(), AD_CMPNY);
	            			
	            		} else {
	            			
	            			JLI_AMNT = this.convertForeignToFunctionalCurrency(arReceipt.getGlFunctionalCurrency().getFcCode(),
	        		    	    arReceipt.getGlFunctionalCurrency().getFcName(), 
	        		    	    arReceipt.getRctConversionDate(), arReceipt.getRctConversionRate(), arDistributionRecord.getDrAmount(), AD_CMPNY);
	            			
	            		}
	            	    
	            	    LocalGlJournalLineInterface glJournalLineInterface = glJournalLineInterfaceHome.create(
	        		    	++lineNumber, 
	        		    	arDistributionRecord.getDrDebit(),        		    	
	        		    	JLI_AMNT,
	        		    	arDistributionRecord.getGlChartOfAccount().getCoaAccountNumber(), AD_CMPNY);
	        		    	
	        		    glJournalInterface.addGlJournalLineInterface(glJournalLineInterface);
	        		    	
	        		    arDistributionRecord.setDrImported(EJBCommon.TRUE);	        		    	
	            	    		            			            		
	            			            		
	            	}
	            	
            	}
            	
            }
            
            
            // void/reversed receipts
            
            arReceipts = arReceiptHome.findVoidPostedRctByRctDateRange(JRI_DT_FRM, JRI_DT_TO, AD_CMPNY);
            
            i = arReceipts.iterator();
            
            while (i.hasNext()) {
            	
            	LocalArReceipt arReceipt = (LocalArReceipt)i.next();
            	            	
            	Collection arDistributionRecords = arDistributionRecordHome.findImportableDrByDrReversedAndRctCode(EJBCommon.TRUE, arReceipt.getRctCode(), AD_CMPNY);
            	            	            	
            	if (!arDistributionRecords.isEmpty()) {
            		
            		LocalGlJournalInterface glJournalInterface = 
            		     glJournalInterfaceHome.create("REVERSED " + arReceipt.getRctReferenceNumber(),
            		         arReceipt.getRctDescription(), arReceipt.getRctDate(), "SALES RECEIPTS", "ACCOUNTS RECEIVABLES",
            		         glFunctionalCurrency.getFcName(), null, arReceipt.getRctNumber(), 
            		         null, 1d, 'N', EJBCommon.FALSE, arReceipt.getArCustomer().getCstTin(),
							 arReceipt.getArCustomer().getCstName(), null, AD_BRNCH, AD_CMPNY);
            		         
            		IMPORTED_JOURNALS++;
            		lineNumber = 0;
            		
            		Iterator j = arDistributionRecords.iterator();
            	
	            	while (j.hasNext()) {
	            		
	            		LocalArDistributionRecord arDistributionRecord = (LocalArDistributionRecord)j.next();
	            		
	            		double JLI_AMNT = 0;
	            		
	            		if (arDistributionRecord.getArAppliedInvoice() != null) {
	            			
	            			LocalArInvoice arInvoice = arDistributionRecord.getArAppliedInvoice().getArInvoicePaymentSchedule().getArInvoice();
	            			
	            			JLI_AMNT = this.convertForeignToFunctionalCurrency(arInvoice.getGlFunctionalCurrency().getFcCode(),
	        		    	    arInvoice.getGlFunctionalCurrency().getFcName(), 
	        		    	    arInvoice.getInvConversionDate(), arInvoice.getInvConversionRate(), arDistributionRecord.getDrAmount(), AD_CMPNY);
	            			
	            		} else {
	            			
	            			JLI_AMNT = this.convertForeignToFunctionalCurrency(arReceipt.getGlFunctionalCurrency().getFcCode(),
	        		    	    arReceipt.getGlFunctionalCurrency().getFcName(), 
	        		    	    arReceipt.getRctConversionDate(), arReceipt.getRctConversionRate(), arDistributionRecord.getDrAmount(), AD_CMPNY);
	            			
	            		}
	            	    
	            	    LocalGlJournalLineInterface glJournalLineInterface = glJournalLineInterfaceHome.create(
	        		    	++lineNumber,
	        		    	arDistributionRecord.getDrDebit(),
	        		    	JLI_AMNT,
	        		    	arDistributionRecord.getGlChartOfAccount().getCoaAccountNumber(), AD_CMPNY);
	        		    	
	        		    glJournalInterface.addGlJournalLineInterface(glJournalLineInterface);
	        		    	
	        		    arDistributionRecord.setDrImported(EJBCommon.TRUE);	        		    	
	            	    		            			            		
	            			            		
	            	}
	            	
            	}
            	
            }
            
            return IMPORTED_JOURNALS;
                
    	} catch (Exception ex) {
    		
    		Debug.printStackTrace(ex);
    		getSessionContext().setRollbackOnly();
    		throw new EJBException(ex.getMessage());
    		
    	}
    	
    }
   		    
    private double convertForeignToFunctionalCurrency(Integer FC_CODE, String FC_NM, 
	    Date CONVERSION_DATE, double CONVERSION_RATE, double AMOUNT, Integer AD_CMPNY) {
	    	
	    Debug.print("ArGlJournalInterfaceControllerBean convertForeignToFunctionalCurrency");
	    
	    
        LocalGlFunctionalCurrencyRateHome glFunctionalCurrencyRateHome = null;
        LocalAdCompanyHome adCompanyHome = null;
         
        LocalAdCompany adCompany = null;
                 
        // Initialize EJB Homes
         
        try {
         	
            glFunctionalCurrencyRateHome = (LocalGlFunctionalCurrencyRateHome)EJBHomeFactory.
               lookUpLocalHome(LocalGlFunctionalCurrencyRateHome.JNDI_NAME, LocalGlFunctionalCurrencyRateHome.class);                         
            adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
               lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);             
                   
         } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
         }
         
         // get company and extended precision
         
         try {
         	
             adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
         	             
         } catch (Exception ex) {
         	
             throw new EJBException(ex.getMessage());
         	
         }	     
        
         
         // Convert to functional currency if necessary
         
         if (CONVERSION_RATE != 1 && CONVERSION_RATE != 0 ) {
         	         
             AMOUNT = AMOUNT * CONVERSION_RATE;
             	
         } else if (CONVERSION_DATE != null) {
         	
         	 try {
         	 	    
         	 	 // Get functional currency rate
         	 	 
         	     LocalGlFunctionalCurrencyRate glReceiptFunctionalCurrencyRate = null;
         	     
         	     if (!FC_NM.equals("USD")) {
         	     	
        	         glReceiptFunctionalCurrencyRate = 
	     	             glFunctionalCurrencyRateHome.findByFcCodeAndDate(FC_CODE, 
	     	             CONVERSION_DATE, AD_CMPNY);
	     	             
	     	         AMOUNT = AMOUNT * glReceiptFunctionalCurrencyRate.getFrXToUsd();
	     	             
         	     }
                 
                 // Get set of book functional currency rate if necessary
         	 	         
                 if (!adCompany.getGlFunctionalCurrency().getFcName().equals("USD")) {
                 
                     LocalGlFunctionalCurrencyRate glCompanyFunctionalCurrencyRate = 
                         glFunctionalCurrencyRateHome.findByFcCodeAndDate(adCompany.getGlFunctionalCurrency().
                             getFcCode(), CONVERSION_DATE, AD_CMPNY);
                     
                     AMOUNT = AMOUNT / glCompanyFunctionalCurrencyRate.getFrXToUsd();
                 
                  }
                                 
         	             
         	 } catch (Exception ex) {
         	 	
         	 	throw new EJBException(ex.getMessage());
         	 	
         	 }       
         	
         }
         
         return EJBCommon.roundIt(AMOUNT, adCompany.getGlFunctionalCurrency().getFcPrecision());
         
	}
	
   // SessionBean methods

   /**
    * @ejb:create-method view-type="remote"
    **/
   public void ejbCreate() throws CreateException {

      Debug.print("ArGlJournalInterfaceControllerBean ejbCreate");
      
   }

   // private methods
   	
		   
}
