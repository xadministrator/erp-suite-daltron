
/*
 * ArInvoiceBatchCopyControllerBean.java
 *
 * Created on May 27, 2004, 8:14 AM
 *
 * @author  Neil Andrew M. Ajero
 */

package com.ejb.txn;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.naming.NamingException;

import com.ejb.ad.LocalAdCompany;
import com.ejb.ad.LocalAdCompanyHome;
import com.ejb.ar.LocalArInvoice;
import com.ejb.ar.LocalArInvoiceBatch;
import com.ejb.ar.LocalArInvoiceBatchHome;
import com.ejb.ar.LocalArInvoiceHome;
import com.ejb.exception.GlobalBatchCopyInvalidException;
import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.exception.GlobalTransactionBatchCloseException;
import com.util.AbstractSessionBean;
import com.util.ArModInvoiceDetails;
import com.util.Debug;
import com.util.EJBHomeFactory;

/**
 * @ejb:bean name="ArInvoiceBatchCopyControllerEJB"
 *           display-name="Used for copying invoice batch"
 *           type="Stateless"
 *           view-type="remote"
 *           jndi-name="ejb/ArInvoiceBatchCopyControllerEJB"
 *
 * @ejb:interface remote-class="com.ejb.txn.ArInvoiceBatchCopyController"
 *                extends="javax.ejb.EJBObject"
 *
 * @ejb:home remote-class="com.ejb.txn.ArInvoiceBatchCopyControllerHome"
 *           extends="javax.ejb.EJBHome"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="aruser"
 *                        role-link="aruserlink"
 *
 * @ejb:permission role-name="aruser"
 * 
*/

public class ArInvoiceBatchCopyControllerBean extends AbstractSessionBean {

    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList getArOpenIbAll(Integer AD_BRNCH, Integer AD_CMPNY) {
                    
        Debug.print("ArInvoiceBatchCopyControllerBean getArOpenIbAll");
        
        LocalArInvoiceBatchHome arInvoiceBatchHome = null;               
        
        ArrayList list = new ArrayList();
        
        // Initialize EJB Home
        
        try {
            
        	arInvoiceBatchHome = (LocalArInvoiceBatchHome)EJBHomeFactory.
                lookUpLocalHome(LocalArInvoiceBatchHome.JNDI_NAME, LocalArInvoiceBatchHome.class);
            
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }        
        
        try {
        	
        	Collection arInvoiceBatches = arInvoiceBatchHome.findOpenIbAll(AD_BRNCH, AD_CMPNY);
        	
        	Iterator i = arInvoiceBatches.iterator();
        	
        	while (i.hasNext()) {
        		
        		LocalArInvoiceBatch arInvoiceBatch = (LocalArInvoiceBatch)i.next();
        		
        		list.add(arInvoiceBatch.getIbName());
        		
        	}
        	
        	return list;
        	
        } catch (Exception ex) {
        	
        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());
        	
        }
            
    }  
    
    /**
     * @ejb:interface-method view-type="remote"
     **/
    public void executeArInvcBatchCopy(ArrayList list, String IB_NM_TO, Integer AD_BRNCH, Integer AD_CMPNY) throws
        GlobalTransactionBatchCloseException,
		GlobalBatchCopyInvalidException{
                    
        Debug.print("ArInvoiceBatchCopyControllerBean executeArInvcBatchCopy");
        
        LocalArInvoiceHome arInvoiceHome = null;
        LocalArInvoiceBatchHome arInvoiceBatchHome = null; 

        // Initialize EJB Home
        
        try {
            
        	arInvoiceHome = (LocalArInvoiceHome)EJBHomeFactory.
			    lookUpLocalHome(LocalArInvoiceHome.JNDI_NAME, LocalArInvoiceHome.class);
        	arInvoiceBatchHome = (LocalArInvoiceBatchHome)EJBHomeFactory.
                lookUpLocalHome(LocalArInvoiceBatchHome.JNDI_NAME, LocalArInvoiceBatchHome.class);
                         
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }

        try {

        	// find invoice batch to
        	
        	LocalArInvoiceBatch arInvoiceBatchTo = arInvoiceBatchHome.findByIbName(IB_NM_TO, AD_BRNCH, AD_CMPNY);

        	// validate if batch to is closed
        	
        	if(arInvoiceBatchTo.getIbStatus().equals("CLOSED")) {
        		
        		throw new GlobalTransactionBatchCloseException();
        		
        	}
        	
        	Iterator i = list.iterator();
        	
        	while (i.hasNext()) {
        		
        		LocalArInvoice arInvoice = arInvoiceHome.findByPrimaryKey((Integer)i.next());
        		
        		if (!arInvoice.getArInvoiceBatch().getIbType().equals(arInvoiceBatchTo.getIbType())) {
        			
        			throw new GlobalBatchCopyInvalidException();
        			
        		}
        		
        		arInvoice.getArInvoiceBatch().dropArInvoice(arInvoice);
        		arInvoiceBatchTo.addArInvoice(arInvoice);

        	}

        } catch (GlobalTransactionBatchCloseException ex) {
       	   
       	   getSessionContext().setRollbackOnly();    	
       	   throw ex;
       	   
        } catch (GlobalBatchCopyInvalidException ex) {
        	   
    	   getSessionContext().setRollbackOnly();    	
    	   throw ex;
                                    
        } catch (Exception ex) {
       	 
       	   Debug.printStackTrace(ex);
       	   getSessionContext().setRollbackOnly();
           throw new EJBException(ex.getMessage());
         
        }

    }
    
    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList getArInvByIbName(String IB_NM, Integer AD_CMPNY) 
        throws GlobalNoRecordFoundException {
                    
        Debug.print("ArInvoiceBatchCopyControllerBean getArInvByIbName");
        
        LocalArInvoiceHome arInvoiceHome = null;
        
        ArrayList list = new ArrayList();
        
        //initialized EJB Home
        
        try {
            
            arInvoiceHome = (LocalArInvoiceHome)EJBHomeFactory.
                lookUpLocalHome(LocalArInvoiceHome.JNDI_NAME, LocalArInvoiceHome.class);
            
        } catch (NamingException ex) 	{
            
            throw new EJBException(ex.getMessage());
            
        }
        
		try {
          		  			  	      	
	      Collection arInvoices = arInvoiceHome.findByIbName(IB_NM, AD_CMPNY);	         
		  
		  if (arInvoices.size() == 0)
		     throw new GlobalNoRecordFoundException();
		     
		  Iterator i = arInvoices.iterator();
		
		  while (i.hasNext()) {
		  	
		  	  LocalArInvoice arInvoice = (LocalArInvoice)i.next();   	  
		  	  
		  	  ArModInvoiceDetails mdetails = new ArModInvoiceDetails();
		  	  mdetails.setInvCode(arInvoice.getInvCode());
		  	  mdetails.setInvCstCustomerCode(arInvoice.getArCustomer().getCstCustomerCode());
		  	  mdetails.setInvDate(arInvoice.getInvDate());
		  	  mdetails.setInvNumber(arInvoice.getInvNumber());
		  	  mdetails.setInvReferenceNumber(arInvoice.getInvReferenceNumber());
		  	  mdetails.setInvAmountDue(arInvoice.getInvAmountDue());
		  	  mdetails.setInvAmountPaid(arInvoice.getInvAmountPaid());
		  	  mdetails.setInvCreditMemo(arInvoice.getInvCreditMemo());      	  
			      	  	      	  		     
			  list.add(mdetails);
			         
		  }
			     
		  return list;
		      
	  } catch (GlobalNoRecordFoundException ex) {
	  	 
	  	  throw ex;
	  	
	  } catch (Exception ex) {
	  	

	  	  ex.printStackTrace();
	  	  throw new EJBException(ex.getMessage());
	  	
	  }
        
    }

    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
     public short getGlFcPrecisionUnit(Integer AD_CMPNY) {

        Debug.print("ArInvoiceBatchCopyControllerBean getGlFcPrecisionUnit");

       
        LocalAdCompanyHome adCompanyHome = null;
       
      
        // Initialize EJB Home
         
        try {
             
            adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
               lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);       
             
        } catch (NamingException ex) {
             
            throw new EJBException(ex.getMessage());
             
        }

        try {
        	
          LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
             
          return  adCompany.getGlFunctionalCurrency().getFcPrecision();
                                      
        } catch (Exception ex) {
        	 
        	 Debug.printStackTrace(ex);
          throw new EJBException(ex.getMessage());
          
        }

     }


    // SessionBean methods

    /**
     * @ejb:create-method view-type="remote"
     **/
    public void ejbCreate() throws CreateException {

       Debug.print("ArInvoiceBatchCopyControllerBean ejbCreate");
      
    }
}
