
/*
 * ArInvoiceBatchEntryControllerBean.java
 *
 * Created on May 20, 2004, 3:52 PM
 *
 * @author  Neil Andrew M. Ajero
 */
 
package com.ejb.txn;

import java.util.Collection;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;
import javax.naming.NamingException;

import com.ejb.ar.LocalArInvoiceBatch;
import com.ejb.ar.LocalArInvoiceBatchHome;
import com.ejb.ar.LocalArInvoiceHome;
import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.exception.GlobalRecordAlreadyAssignedException;
import com.ejb.exception.GlobalRecordAlreadyDeletedException;
import com.ejb.exception.GlobalRecordAlreadyExistException;
import com.ejb.exception.GlobalTransactionBatchCloseException;
import com.util.AbstractSessionBean;
import com.util.ArInvoiceBatchDetails;
import com.util.Debug;
import com.util.EJBCommon;
import com.util.EJBHomeFactory;

/**
 * @ejb:bean name="ArInvoiceBatchEntryControllerEJB"
 *           display-name="used for entering invoice batches"
 *           type="Stateless"
 *           view-type="remote"
 *           jndi-name="ejb/ArInvoiceBatchEntryControllerEJB"
 *
 * @ejb:interface remote-class="com.ejb.txn.ArInvoiceBatchEntryController"
 *                extends="javax.ejb.EJBObject"
 *
 * @ejb:home remote-class="com.ejb.txn.ArInvoiceBatchEntryControllerHome"
 *           extends="javax.ejb.EJBHome"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="aruser"
 *                        role-link="aruserlink"
 *
 * @ejb:permission role-name="aruser"
 * 
*/

public class ArInvoiceBatchEntryControllerBean extends AbstractSessionBean {

    
    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public com.util.ArInvoiceBatchDetails getArIbByIbCode(Integer IB_CODE, Integer AD_CMPNY) 
        throws GlobalNoRecordFoundException {
                    
        Debug.print("ArInvoiceBatchEntryControllerBean getArIbByIbCode");
        
        LocalArInvoiceBatchHome arInvoiceBatchHome = null;        
                
        // Initialize EJB Home
        
        try {
            
            arInvoiceBatchHome = (LocalArInvoiceBatchHome)EJBHomeFactory.
                lookUpLocalHome(LocalArInvoiceBatchHome.JNDI_NAME, LocalArInvoiceBatchHome.class);
            
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }        
        
        try {
        	
        	LocalArInvoiceBatch arInvoiceBatch = null;
        	
        	
        	try {
        		
        		arInvoiceBatch = arInvoiceBatchHome.findByPrimaryKey(IB_CODE);
        		
        	} catch (FinderException ex) {
        		
        		throw new GlobalNoRecordFoundException();
        		
        	}
        	
        	ArInvoiceBatchDetails details = new ArInvoiceBatchDetails();
        	details.setIbCode(arInvoiceBatch.getIbCode());
        	details.setIbName(arInvoiceBatch.getIbName());
        	details.setIbDescription(arInvoiceBatch.getIbDescription());
        	details.setIbStatus(arInvoiceBatch.getIbStatus());
        	details.setIbType(arInvoiceBatch.getIbType());
        	details.setIbDateCreated(arInvoiceBatch.getIbDateCreated());
        	details.setIbCreatedBy(arInvoiceBatch.getIbCreatedBy());
        	
        	return details;
        	
        	
        } catch (GlobalNoRecordFoundException ex) {
        	
        	throw ex;
        	        	
        } catch (Exception ex) {
        	
        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());
        	
        }
            
    }
    
    
    /**
     * @ejb:interface-method view-type="remote"
     **/
    public Integer saveArIbEntry(com.util.ArInvoiceBatchDetails details, Integer AD_BRNCH, Integer AD_CMPNY) throws
        GlobalRecordAlreadyExistException,
        GlobalRecordAlreadyDeletedException,
		GlobalTransactionBatchCloseException,
		GlobalRecordAlreadyAssignedException {
                    
        Debug.print("ArInvoiceBatchEntryControllerBean saveArIbEntry");
        
        LocalArInvoiceBatchHome arInvoiceBatchHome = null;  
        LocalArInvoiceHome arInvoiceHome = null;       
        
        LocalArInvoiceBatch arInvoiceBatch = null;
         
                
        // Initialize EJB Home
        
        try {
            
            arInvoiceBatchHome = (LocalArInvoiceBatchHome)EJBHomeFactory.
                lookUpLocalHome(LocalArInvoiceBatchHome.JNDI_NAME, LocalArInvoiceBatchHome.class);     
            arInvoiceHome = (LocalArInvoiceHome)EJBHomeFactory.
                lookUpLocalHome(LocalArInvoiceHome.JNDI_NAME, LocalArInvoiceHome.class);       
                        
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        } 
        
        try {
 
                		
			// validate if invoice batch is already deleted
			
			try {
				
				if (details.getIbCode() != null) {
					
					arInvoiceBatch = arInvoiceBatchHome.findByPrimaryKey(details.getIbCode());
					
				}
				
		 	} catch (FinderException ex) {
		 		
		 		throw new GlobalRecordAlreadyDeletedException();	 		
				
			} 
	        
	        // validate if invoice batch exists
	        		
			try {
				
			    LocalArInvoiceBatch arExistingInvoiceBatch = arInvoiceBatchHome.findByIbName(details.getIbName(), AD_BRNCH, AD_CMPNY);
			
			    if (details.getIbCode() == null ||
			        details.getIbCode() != null && !arExistingInvoiceBatch.getIbCode().equals(details.getIbCode())) {
			    	
			        throw new GlobalRecordAlreadyExistException();
			        
			    }
			 
			} catch (GlobalRecordAlreadyExistException ex) {
				
				throw ex;
			
			} catch (FinderException ex) {
			    
			} 	
			
			// validate if invoice batch closing
			
			if (details.getIbStatus().equals("CLOSED")) {
				
				Collection arInvoices = arInvoiceHome.findByInvPostedAndInvVoidAndIbName(EJBCommon.FALSE, EJBCommon.FALSE, details.getIbName(), AD_CMPNY);
				
				if (!arInvoices.isEmpty()) {
					
					throw new GlobalTransactionBatchCloseException();
					
				}
				
			}
			
			// validate if invoice already assigned
			
			if (details.getIbCode() != null) {
				
				Collection arInvoices = arInvoiceBatch.getArInvoices();
				
				if (!arInvoiceBatch.getIbType().equals(details.getIbType()) &&
				    !arInvoices.isEmpty()) {
				    	
				    throw new GlobalRecordAlreadyAssignedException();
				    	
				}
				
			}											
			
			
			if (details.getIbCode() == null) {
				
				arInvoiceBatch = arInvoiceBatchHome.create(details.getIbName(),
				   details.getIbDescription(), details.getIbStatus(), details.getIbType(),
				   details.getIbDateCreated(), details.getIbCreatedBy(), AD_BRNCH, AD_CMPNY);
				
			} else {
				
				arInvoiceBatch.setIbName(details.getIbName());
				arInvoiceBatch.setIbDescription(details.getIbDescription());
				arInvoiceBatch.setIbStatus(details.getIbStatus());
				arInvoiceBatch.setIbType(details.getIbType());
				
			}
			
			return arInvoiceBatch.getIbCode();				
		      	            	
        } catch (GlobalRecordAlreadyDeletedException ex) {
        	 
        	getSessionContext().setRollbackOnly();      	  
      	    throw ex;
        
        } catch (GlobalRecordAlreadyExistException ex) {	 	
        
            getSessionContext().setRollbackOnly();      	  
      	    throw ex;
      	    
      	} catch (GlobalTransactionBatchCloseException ex) {
        	
        	getSessionContext().setRollbackOnly();
        	throw ex;
        	
        } catch (GlobalRecordAlreadyAssignedException ex) {
        	
        	getSessionContext().setRollbackOnly();
        	throw ex;
                	
        } catch (Exception ex) {
        	
        	Debug.printStackTrace(ex);
        	getSessionContext().setRollbackOnly();
        	throw new EJBException(ex.getMessage());
        	
        }
            
    }     
            
    
    /**
     * @ejb:interface-method view-type="remote"
     **/
    public void deleteArIbEntry(Integer IB_CODE, Integer AD_CMPNY) throws 
        GlobalRecordAlreadyDeletedException,
        GlobalRecordAlreadyAssignedException {
                    
        Debug.print("ArInvoiceBatchEntryControllerBean deleteArIbEntry");
        
        LocalArInvoiceBatchHome arInvoiceBatchHome = null;  
        LocalArInvoiceHome arInvoiceHome = null;               
                
        // Initialize EJB Home
        
        try {
            
            arInvoiceBatchHome = (LocalArInvoiceBatchHome)EJBHomeFactory.
                lookUpLocalHome(LocalArInvoiceBatchHome.JNDI_NAME, LocalArInvoiceBatchHome.class);     
            arInvoiceHome = (LocalArInvoiceHome)EJBHomeFactory.
                lookUpLocalHome(LocalArInvoiceHome.JNDI_NAME, LocalArInvoiceHome.class);       
                        
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }        
        
        try {
        	
        	LocalArInvoiceBatch arInvoiceBatch = arInvoiceBatchHome.findByPrimaryKey(IB_CODE);
        	
        	Collection arInvoices = arInvoiceBatch.getArInvoices();
        	
        	if (!arInvoices.isEmpty()) {
        		
        		throw new GlobalRecordAlreadyAssignedException();
        		
        	}
        	
        	arInvoiceBatch.remove();
        	
        } catch (FinderException ex) {	
            
            getSessionContext().setRollbackOnly();
            throw new GlobalRecordAlreadyDeletedException();      	
        	
        } catch (GlobalRecordAlreadyAssignedException ex) {	
            
            getSessionContext().setRollbackOnly();
            throw ex; 
        	
        } catch (Exception ex) {
        	
        	Debug.printStackTrace(ex);
        	getSessionContext().setRollbackOnly();
        	throw new EJBException(ex.getMessage());
        	
        }
            
    }

    
              
	// SessionBean methods

    /**
     * @ejb:create-method view-type="remote"
     **/
    public void ejbCreate() throws CreateException {

       Debug.print("ArInvoiceBatchEntryControllerBean ejbCreate");
      
    }
    
   // private methods

   

}