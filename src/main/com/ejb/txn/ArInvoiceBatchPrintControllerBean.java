
/*
 * ArInvoiceBatchPrintControllerBean.java
 *
 * Created on May 14, 2004, 5:24 PM
 *
 * @author  Neil Andrew M. Ajero
 */

package com.ejb.txn;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.naming.NamingException;

import com.ejb.ad.LocalAdCompany;
import com.ejb.ad.LocalAdCompanyHome;
import com.ejb.ad.LocalAdPreference;
import com.ejb.ad.LocalAdPreferenceHome;
import com.ejb.ar.LocalArCustomer;
import com.ejb.ar.LocalArCustomerHome;
import com.ejb.ar.LocalArInvoice;
import com.ejb.ar.LocalArInvoiceBatch;
import com.ejb.ar.LocalArInvoiceBatchHome;
import com.ejb.ar.LocalArInvoiceHome;
import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.gl.LocalGlFunctionalCurrency;
import com.ejb.gl.LocalGlFunctionalCurrencyHome;
import com.util.AbstractSessionBean;
import com.util.ArModInvoiceDetails;
import com.util.Debug;
import com.util.EJBCommon;
import com.util.EJBHomeFactory;

/**
 * @ejb:bean name="ArInvoiceBatchPrintControllerEJB"
 *           display-name="Used for finding invoices"
 *           type="Stateless"
 *           view-type="remote"
 *           jndi-name="ejb/ArInvoiceBatchPrintControllerEJB"
 *
 * @ejb:interface remote-class="com.ejb.txn.ArInvoiceBatchPrintController"
 *                extends="javax.ejb.EJBObject"
 *
 * @ejb:home remote-class="com.ejb.txn.ArInvoiceBatchPrintControllerHome"
 *           extends="javax.ejb.EJBHome"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="aruser"
 *                        role-link="aruserlink"
 *
 * @ejb:permission role-name="aruser"
 * 
*/

public class ArInvoiceBatchPrintControllerBean extends AbstractSessionBean {

    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList getArCstAll(Integer AD_BRNCH, Integer AD_CMPNY) {
                    
        Debug.print("ArInvoiceBatchPrintControllerBean getArCstAll");
        
        LocalArCustomerHome arCustomerHome = null;

        ArrayList list = new ArrayList();
        
        // Initialize EJB Home
        
        try {
            
            arCustomerHome = (LocalArCustomerHome)EJBHomeFactory.
                lookUpLocalHome(LocalArCustomerHome.JNDI_NAME, LocalArCustomerHome.class);
            
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {
            
            Collection arCustomers = arCustomerHome.findEnabledCstAll(AD_BRNCH, AD_CMPNY);

	        Iterator i = arCustomers.iterator();
	               
	        while (i.hasNext()) {
	        	
	        	LocalArCustomer arCustomer = (LocalArCustomer)i.next();
	        	
	        	list.add(arCustomer.getCstCustomerCode());
	        	
	        }
	        
	        return list;
            
        } catch (Exception ex) {
        	
        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());
        	
        }
            
    }
    
    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList getGlFcAll(Integer AD_CMPNY) {
                    
        Debug.print("ArInvoiceBatchPrintControllerBean getGlFcAll");
        
        LocalGlFunctionalCurrencyHome glFunctionalCurrencyHome = null;

        ArrayList list = new ArrayList();
        
        // Initialize EJB Home
        
        try {
            
            glFunctionalCurrencyHome = (LocalGlFunctionalCurrencyHome)EJBHomeFactory.
                lookUpLocalHome(LocalGlFunctionalCurrencyHome.JNDI_NAME, LocalGlFunctionalCurrencyHome.class);
            
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {
            
            Collection glFunctionalCurrencies = glFunctionalCurrencyHome.findFcAll(AD_CMPNY);
	        
	        Iterator i = glFunctionalCurrencies.iterator();
	               
	        while (i.hasNext()) {
	        	
	        	LocalGlFunctionalCurrency glFunctionalCurrency = (LocalGlFunctionalCurrency)i.next();
	        	
	        	list.add(glFunctionalCurrency.getFcName());
	        	
	        }
	        
	        return list;
          
        } catch (Exception ex) {
        	
        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());
        	
        }
            
    }
    
    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList getArOpenIbAll(Integer AD_BRNCH, Integer AD_CMPNY) {
                    
        Debug.print("ArInvoiceBatchPrintControllerBean getArOpenIbAll");
        
        LocalArInvoiceBatchHome arInvoiceBatchHome = null;               
        
        ArrayList list = new ArrayList();
        
        // Initialize EJB Home
        
        try {
            
        	arInvoiceBatchHome = (LocalArInvoiceBatchHome)EJBHomeFactory.
                lookUpLocalHome(LocalArInvoiceBatchHome.JNDI_NAME, LocalArInvoiceBatchHome.class);
            
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }        
        
        try {
        	
        	Collection arInvoiceBatches = arInvoiceBatchHome.findOpenIbAll(AD_BRNCH, AD_CMPNY);
        	
        	Iterator i = arInvoiceBatches.iterator();
        	
        	while (i.hasNext()) {
        		
        		LocalArInvoiceBatch arInvoiceBatch = (LocalArInvoiceBatch)i.next();
        		
        		list.add(arInvoiceBatch.getIbName());
        		
        	}
        	
        	return list;
        	
        } catch (Exception ex) {
        	
        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());
        	
        }
            
    }  
    
    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
     public byte getAdPrfEnableArInvoiceBatch(Integer AD_CMPNY) {

        Debug.print("ArInvoiceBatchPrintControllerBean getAdPrfEnableArInvoiceBatch");
                   
        LocalAdPreferenceHome adPreferenceHome = null;
       
       
        // Initialize EJB Home
         
        try {
             
           adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
               lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);       
             
        } catch (NamingException ex) {
             
           throw new EJBException(ex.getMessage());
             
        }
       

        try {
       	
           LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);
          
           return adPreference.getPrfEnableArInvoiceBatch();
          
        } catch (Exception ex) {
         	 
           Debug.printStackTrace(ex);
           throw new EJBException(ex.getMessage());
          
        }
       
     } 
    
    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList getArInvByCriteria(HashMap criteria,
        Integer OFFSET, Integer LIMIT, String ORDER_BY, Integer AD_BRNCH, Integer AD_CMPNY) 
        throws GlobalNoRecordFoundException {
                    
        Debug.print("ArInvoiceBatchPrintControllerBean getArInvByCriteria");
        
        LocalArInvoiceHome arInvoiceHome = null;
                
        ArrayList list = new ArrayList();
        
        //initialized EJB Home
        
        try {
            
            arInvoiceHome = (LocalArInvoiceHome)EJBHomeFactory.
                lookUpLocalHome(LocalArInvoiceHome.JNDI_NAME, LocalArInvoiceHome.class);
                        
        } catch (NamingException ex) 	{
            
            throw new EJBException(ex.getMessage());
            
        }
        
		try { 
           
		  StringBuffer jbossQl = new StringBuffer();
		  jbossQl.append("SELECT OBJECT(inv) FROM ArInvoice inv ");
		  
	      boolean firstArgument = true;
	      short ctr = 0;
		  int criteriaSize = criteria.size() + 2;	      
	      
	      Object obj[];	      
		
		  // Allocate the size of the object parameter
		
	      if (criteria.containsKey("referenceNumber")) {
	      	
	      	 criteriaSize--;
	      	 
	      }
	      	
	      if (criteria.containsKey("paymentStatus")) {
	      	
	      	 criteriaSize--;
	      	 
	      }
	      
	      if (criteria.containsKey("approvalStatus")) {
	      	
	      	 String approvalStatus = (String)criteria.get("approvalStatus");
	      	
	      	 if (approvalStatus.equals("DRAFT") || approvalStatus.equals("REJECTED")) {
	      	 	
	      	 	 criteriaSize--;
	      	 	
	      	 }
	      	
	      }
	      
	      obj = new Object[criteriaSize];
		       	      
		  if (criteria.containsKey("referenceNumber")) {
		  	
		  	 if (!firstArgument) {
		  	 
		  	    jbossQl.append("AND ");	
		  	 	
		     } else {
		     	
		     	firstArgument = false;
		     	jbossQl.append("WHERE ");
		     	
		     }
		     
		  	 jbossQl.append("inv.invReferenceNumber LIKE '%" + (String)criteria.get("referenceNumber") + "%' ");
		  	 
		  }
		  
		  if (criteria.containsKey("batchName")) {
		   	
		   	  if (!firstArgument) {		       	  	
		   	     jbossQl.append("AND ");		       	     
		   	  } else {		       	  	
		   	  	 firstArgument = false;
		   	  	 jbossQl.append("WHERE ");		       	  	 
		   	  }
		   	  
		   	  jbossQl.append("inv.arInvoiceBatch.ibName=?" + (ctr+1) + " ");
		   	  obj[ctr] = (String)criteria.get("batchName");
		   	  ctr++;
	   	  
	      }
			
		  if (criteria.containsKey("customerCode")) {
	   	
		   	  if (!firstArgument) {		       	  	
		   	     jbossQl.append("AND ");		       	     
		   	  } else {		       	  	
		   	  	 firstArgument = false;
		   	  	 jbossQl.append("WHERE ");		       	  	 
		   	  }
		   	  
		   	  jbossQl.append("inv.arCustomer.cstCustomerCode=?" + (ctr+1) + " ");
		   	  obj[ctr] = (String)criteria.get("customerCode");
		   	  ctr++;
	   	  
	      }	

	      	
	      if (!firstArgument) {
	  	 	jbossQl.append("AND ");
	  	  } else {
	  	 	firstArgument = false;
	  	 	jbossQl.append("WHERE ");
	  	  }
	  	 
	  	  jbossQl.append("inv.invCreditMemo=?" + (ctr+1) + " ");
	  	  obj[ctr] = (Byte)criteria.get("creditMemo");
	      ctr++;
	      
	      if (!firstArgument) {
	  	 	jbossQl.append("AND ");
	  	  } else {
	  	 	firstArgument = false;
	  	 	jbossQl.append("WHERE ");
	  	  }
	  	 
	  	  jbossQl.append("inv.invVoid=?" + (ctr+1) + " ");
	  	  obj[ctr] = (Byte)criteria.get("invoiceVoid");
	      ctr++;		     
          			
	      if (criteria.containsKey("dateFrom")) {
		      	
		  	 if (!firstArgument) {
		  	 	jbossQl.append("AND ");
		  	 } else {
		  	 	firstArgument = false;
		  	 	jbossQl.append("WHERE ");
		  	 }
		  	 jbossQl.append("inv.invDate>=?" + (ctr+1) + " ");
		  	 obj[ctr] = (Date)criteria.get("dateFrom");
		  	 ctr++;
		  }  
		      
		  if (criteria.containsKey("dateTo")) {
		  	
		  	 if (!firstArgument) {
		  	 	jbossQl.append("AND ");
		  	 } else {
		  	 	firstArgument = false;
		  	 	jbossQl.append("WHERE ");
		  	 }
		  	 jbossQl.append("inv.invDate<=?" + (ctr+1) + " ");
		  	 obj[ctr] = (Date)criteria.get("dateTo");
		  	 ctr++;
		  	 
		  }    
		  
		  if (criteria.containsKey("invoiceNumberFrom")) {
		      	
		  	 if (!firstArgument) {
		  	 	jbossQl.append("AND ");
		  	 } else {
		  	 	firstArgument = false;
		  	 	jbossQl.append("WHERE ");
		  	 }
		  	 jbossQl.append("inv.invNumber>=?" + (ctr+1) + " ");
		  	 obj[ctr] = (String)criteria.get("invoiceNumberFrom");
		  	 ctr++;
		  }  
		      
		  if (criteria.containsKey("invoiceNumberTo")) {
		  	
		  	 if (!firstArgument) {
		  	 	jbossQl.append("AND ");
		  	 } else {
		  	 	firstArgument = false;
		  	 	jbossQl.append("WHERE ");
		  	 }
		  	 jbossQl.append("inv.invNumber<=?" + (ctr+1) + " ");
		  	 obj[ctr] = (String)criteria.get("invoiceNumberTo");
		  	 ctr++;
		  	 
		  }

	      if (criteria.containsKey("currency")) {
	       	
	       	  if (!firstArgument) {
	       	  	
	       	     jbossQl.append("AND ");
	       	     
	       	  } else {
	       	  	
	       	  	 firstArgument = false;
	       	  	 jbossQl.append("WHERE ");
	       	  	 
	       	  }
	       	  
	       	  jbossQl.append("inv.glFunctionalCurrency.fcName=?" + (ctr+1) + " ");
	       	  obj[ctr] = (String)criteria.get("currency");
	       	  ctr++;
	       	  
	      }
	      
	      if (criteria.containsKey("approvalStatus")) {
	       	
	       	  if (!firstArgument) {
	       	  	
	       	     jbossQl.append("AND ");
	       	     
	       	  } else {
	       	  	
	       	  	 firstArgument = false;
	       	  	 jbossQl.append("WHERE ");
	       	  	 
	       	  }
	       	  
	       	  String approvalStatus = (String)criteria.get("approvalStatus");
       	  
	       	  if (approvalStatus.equals("DRAFT")) {
	       	      
		       	  jbossQl.append("inv.invApprovalStatus IS NULL ");
	       	  	
	       	  } else if (approvalStatus.equals("REJECTED")) {
	       	  	
		       	  jbossQl.append("inv.invReasonForRejection IS NOT NULL ");
	       	  	
	      	  } else {
	      	  	
		      	  jbossQl.append("inv.invApprovalStatus=?" + (ctr+1) + " ");
		       	  obj[ctr] = approvalStatus;
		       	  ctr++;
	      	  	
	      	  }
	       	  
	      }
	      
	      
	      if (criteria.containsKey("posted")) {
	       	
	       	  if (!firstArgument) {
	       	  	
	       	     jbossQl.append("AND ");
	       	     
	       	  } else {
	       	  	
	       	  	 firstArgument = false;
	       	  	 jbossQl.append("WHERE ");
	       	  	 
	       	  }
	       	  
	       	  jbossQl.append("inv.invPosted=?" + (ctr+1) + " ");
	       	  
	       	  String posted = (String)criteria.get("posted");
	       	  
	       	  if (posted.equals("YES")) {
	       	  	
	       	  	obj[ctr] = new Byte(EJBCommon.TRUE);
	       	  	
	       	  } else {
	       	  	
	       	  	obj[ctr] = new Byte(EJBCommon.FALSE);
	       	  	
	       	  }       	  
	       	 
	       	  ctr++;
	       	  
	      }	      
      
		  if (criteria.containsKey("paymentStatus")) {
		
		  	 String paymentStatus = (String)criteria.get("paymentStatus");
		      	
		     if (!firstArgument) {
		     	
		  	 	jbossQl.append("AND ");
		  	 	
		  	 } else {
		  	 	
		  	 	firstArgument = false;
		  	 	jbossQl.append("WHERE ");
		  	 	
		  	 }
		  	 
			 if (paymentStatus.equals("PAID")) {	      	 
		
		      	 jbossQl.append("inv.invAmountDue=inv.invAmountPaid ");
			         
		     } else if (paymentStatus.equals("UNPAID")) {	         
	
		      	 jbossQl.append("inv.invAmountDue < inv.invAmountPaid OR inv.invAmountDue > inv.invAmountPaid ");
		         
		     }
			     
		  } 	      
		  
		  if (!firstArgument) {
       	  	
       	     jbossQl.append("AND ");
       	     
       	  } else {
       	  	
       	  	 firstArgument = false;
       	  	 jbossQl.append("WHERE ");
       	  	 
       	  }
       	  
       	  jbossQl.append("inv.invAdBranch=" + AD_BRNCH + " AND inv.invAdCompany=" + AD_CMPNY + " ");
			
		  String orderBy = null;
		  
		  if (ORDER_BY.equals("CUSTOMER CODE")) {	          
		      	      		
		  	  orderBy = "inv.arCustomer.cstCustomerCode";

		  } else if (ORDER_BY.equals("INVOICE NUMBER")) {
		
		  	  orderBy = "inv.invNumber";
		  	
		  }

	  	  if (orderBy != null) {
		
		  	  jbossQl.append("ORDER BY " + orderBy + ", inv.invDate");

		  } else {
		  	
		  	  jbossQl.append("ORDER BY inv.invDate");
		  	
		  }
		  
		  jbossQl.append(" OFFSET ?" + (ctr + 1));	        
		  obj[ctr] = OFFSET;	      
		  ctr++;
		  
		  jbossQl.append(" LIMIT ?" + (ctr + 1));
		  obj[ctr] = LIMIT;
		  ctr++;		      
			  	      	
	      Collection arInvoices = arInvoiceHome.getInvByCriteria(jbossQl.toString(), obj);	         
		  
		  if (arInvoices.size() == 0)
		     throw new GlobalNoRecordFoundException();		    
		    		     
		  Iterator i = arInvoices.iterator();
		
		  while (i.hasNext()) {
		  	
		  	  LocalArInvoice arInvoice = (LocalArInvoice)i.next();
		  	  		  	  
		  	  ArModInvoiceDetails mdetails = new ArModInvoiceDetails();
		  	  mdetails.setInvCode(arInvoice.getInvCode());
		  	  mdetails.setInvCstCustomerCode(arInvoice.getArCustomer().getCstCustomerCode());
		  	  mdetails.setInvDate(arInvoice.getInvDate());
		  	  mdetails.setInvNumber(arInvoice.getInvNumber());
		  	  mdetails.setInvReferenceNumber(arInvoice.getInvReferenceNumber());
		  	  mdetails.setInvAmountDue(arInvoice.getInvAmountDue());
		  	  mdetails.setInvAmountPaid(arInvoice.getInvAmountPaid());
		  	  mdetails.setInvCreditMemo(arInvoice.getInvCreditMemo());      	  
			      	  	      	  		     
			  list.add(mdetails);
			         
		  }
			     
		  return list;
		      
	  } catch (GlobalNoRecordFoundException ex) {
	  	 
	  	  throw ex;
	  	
	  } catch (Exception ex) {
	  	

	  	  ex.printStackTrace();
	  	  throw new EJBException(ex.getMessage());
	  	
	  }
        
    }

   /**
    * @ejb:interface-method view-type="remote"
    * @jboss:method-attributes read-only="true"
    **/
    public short getGlFcPrecisionUnit(Integer AD_CMPNY) {

       Debug.print("ArInvoiceBatchPrintControllerBean getGlFcPrecisionUnit");

       	
       LocalAdCompanyHome adCompanyHome = null;
      
     
       // Initialize EJB Home
        
       try {
            
           adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
              lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);       
            
       } catch (NamingException ex) {
            
           throw new EJBException(ex.getMessage());
            
       }

       try {
       	
         LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
            
         return  adCompany.getGlFunctionalCurrency().getFcPrecision();
                                     
       } catch (Exception ex) {
       	 
       	 Debug.printStackTrace(ex);
         throw new EJBException(ex.getMessage());
         
       }

    }
    
    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public byte getAdPrfArUseCustomerPulldown(Integer AD_CMPNY) {
    	
    	Debug.print("ArInvoiceBatchPrintControllerBean getAdPrfArUseCustomerPulldown");
    	
    	LocalAdPreferenceHome adPreferenceHome = null;         
    	
    	// Initialize EJB Home
    	
    	try {
    		
    		adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);       
    		
    	} catch (NamingException ex) {
    		
    		throw new EJBException(ex.getMessage());
    		
    	}
    	
    	
    	try {
    		
    		LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);
    		
    		return adPreference.getPrfArUseCustomerPulldown();
    		
    	} catch (Exception ex) {
    		
    		Debug.printStackTrace(ex);
    		throw new EJBException(ex.getMessage());
    		
    	}
    	
    }

    // SessionBean methods

    /**
     * @ejb:create-method view-type="remote"
     **/
    public void ejbCreate() throws CreateException {

       Debug.print("ArInvoiceBatchPrintControllerBean ejbCreate");
      
    }
}
