
/*
 * ArInvoiceBatchSubmitControllerBean.java
 *
 * Created on May 18, 2004, 2:39 PM
 *
 * @author Neil Andrew M. Ajero
 */

package com.ejb.txn;

import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;
import javax.naming.NamingException;

import com.ejb.ad.LocalAdAmountLimit;
import com.ejb.ad.LocalAdAmountLimitHome;
import com.ejb.ad.LocalAdApproval;
import com.ejb.ad.LocalAdApprovalCoaLineHome;
import com.ejb.ad.LocalAdApprovalHome;
import com.ejb.ad.LocalAdApprovalQueue;
import com.ejb.ad.LocalAdApprovalQueueHome;
import com.ejb.ad.LocalAdApprovalUser;
import com.ejb.ad.LocalAdApprovalUserHome;
import com.ejb.ad.LocalAdBranchDocumentSequenceAssignment;
import com.ejb.ad.LocalAdBranchDocumentSequenceAssignmentHome;
import com.ejb.ad.LocalAdBranchItemLocation;
import com.ejb.ad.LocalAdBranchItemLocationHome;
import com.ejb.ad.LocalAdCompany;
import com.ejb.ad.LocalAdCompanyHome;
import com.ejb.ad.LocalAdLookUpValue;
import com.ejb.ad.LocalAdLookUpValueHome;
import com.ejb.ad.LocalAdPreference;
import com.ejb.ad.LocalAdPreferenceHome;
import com.ejb.ar.LocalArCustomer;
import com.ejb.ar.LocalArCustomerBalance;
import com.ejb.ar.LocalArCustomerBalanceHome;
import com.ejb.ar.LocalArCustomerHome;
import com.ejb.ar.LocalArDistributionRecord;
import com.ejb.ar.LocalArDistributionRecordHome;
import com.ejb.ar.LocalArInvoice;
import com.ejb.ar.LocalArInvoiceBatch;
import com.ejb.ar.LocalArInvoiceBatchHome;
import com.ejb.ar.LocalArInvoiceHome;
import com.ejb.ar.LocalArInvoiceLineItem;
import com.ejb.ar.LocalArInvoicePaymentSchedule;
import com.ejb.ar.LocalArSalesOrder;
import com.ejb.ar.LocalArSalesOrderInvoiceLine;
import com.ejb.ar.LocalArSalesOrderLine;
import com.ejb.exception.AdPRFCoaGlVarianceAccountNotFoundException;
import com.ejb.exception.GlJREffectiveDateNoPeriodExistException;
import com.ejb.exception.GlJREffectiveDatePeriodClosedException;
import com.ejb.exception.GlobalBranchAccountNumberInvalidException;
import com.ejb.exception.GlobalConversionDateNotExistException;
import com.ejb.exception.GlobalInvItemLocationNotFoundException;
import com.ejb.exception.GlobalInventoryDateException;
import com.ejb.exception.GlobalJournalNotBalanceException;
import com.ejb.exception.GlobalNoApprovalApproverFoundException;
import com.ejb.exception.GlobalNoApprovalRequesterFoundException;
import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.exception.GlobalRecordAlreadyDeletedException;
import com.ejb.exception.GlobalTransactionAlreadyApprovedException;
import com.ejb.exception.GlobalTransactionAlreadyPendingException;
import com.ejb.exception.GlobalTransactionAlreadyPostedException;
import com.ejb.exception.GlobalTransactionAlreadyVoidException;
import com.ejb.gl.LocalAdDocumentSequenceAssignment;
import com.ejb.gl.LocalAdDocumentSequenceAssignmentHome;
import com.ejb.gl.LocalGlAccountingCalendarValue;
import com.ejb.gl.LocalGlAccountingCalendarValueHome;
import com.ejb.gl.LocalGlChartOfAccount;
import com.ejb.gl.LocalGlChartOfAccountBalance;
import com.ejb.gl.LocalGlChartOfAccountBalanceHome;
import com.ejb.gl.LocalGlChartOfAccountHome;
import com.ejb.gl.LocalGlForexLedger;
import com.ejb.gl.LocalGlForexLedgerHome;
import com.ejb.gl.LocalGlFunctionalCurrency;
import com.ejb.gl.LocalGlFunctionalCurrencyHome;
import com.ejb.gl.LocalGlFunctionalCurrencyRate;
import com.ejb.gl.LocalGlFunctionalCurrencyRateHome;
import com.ejb.gl.LocalGlJournal;
import com.ejb.gl.LocalGlJournalBatch;
import com.ejb.gl.LocalGlJournalBatchHome;
import com.ejb.gl.LocalGlJournalCategory;
import com.ejb.gl.LocalGlJournalCategoryHome;
import com.ejb.gl.LocalGlJournalHome;
import com.ejb.gl.LocalGlJournalLine;
import com.ejb.gl.LocalGlJournalLineHome;
import com.ejb.gl.LocalGlJournalSource;
import com.ejb.gl.LocalGlJournalSourceHome;
import com.ejb.gl.LocalGlSetOfBook;
import com.ejb.gl.LocalGlSetOfBookHome;
import com.ejb.gl.LocalGlSuspenseAccount;
import com.ejb.gl.LocalGlSuspenseAccountHome;
import com.ejb.inv.LocalInvAdjustment;
import com.ejb.inv.LocalInvAdjustmentHome;
import com.ejb.inv.LocalInvAdjustmentLine;
import com.ejb.inv.LocalInvAdjustmentLineHome;
import com.ejb.inv.LocalInvBillOfMaterial;
import com.ejb.inv.LocalInvCosting;
import com.ejb.inv.LocalInvCostingHome;
import com.ejb.inv.LocalInvDistributionRecord;
import com.ejb.inv.LocalInvDistributionRecordHome;
import com.ejb.inv.LocalInvItem;
import com.ejb.inv.LocalInvItemHome;
import com.ejb.inv.LocalInvItemLocation;
import com.ejb.inv.LocalInvItemLocationHome;
import com.ejb.inv.LocalInvUnitOfMeasure;
import com.ejb.inv.LocalInvUnitOfMeasureConversion;
import com.ejb.inv.LocalInvUnitOfMeasureConversionHome;
import com.util.AbstractSessionBean;
import com.util.AdBranchDetails;
import com.util.ArModInvoiceDetails;
import com.util.Debug;
import com.util.EJBCommon;
import com.util.EJBHomeFactory;

/**
 * @ejb:bean name="ArInvoiceBatchSubmitControllerEJB" display-name="Used for submitting invoices to
 *           be posted" type="Stateless" view-type="remote"
 *           jndi-name="ejb/ArInvoiceBatchSubmitControllerEJB"
 *
 * @ejb:interface remote-class="com.ejb.txn.ArInvoiceBatchSubmitController"
 *                extends="javax.ejb.EJBObject"
 *
 * @ejb:home remote-class="com.ejb.txn.ArInvoiceBatchSubmitControllerHome"
 *           extends="javax.ejb.EJBHome"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="aruser" role-link="aruserlink"
 *
 * @ejb:permission role-name="aruser"
 *
 */

public class ArInvoiceBatchSubmitControllerBean extends AbstractSessionBean {

  /**
   * @ejb:interface-method view-type="remote"
   * @jboss:method-attributes read-only="true"
   **/
  public ArrayList getAdLvCustomerBatchAll(
      Integer AD_CMPNY
  ) {

    Debug.print("ArInvoiceBatchSubmitControllerBean getAdLvCustomerBatchAll");

    LocalAdLookUpValueHome adLookUpValueHome = null;

    ArrayList list = new ArrayList();

    // Initialize EJB Home

    try {

      adLookUpValueHome = (LocalAdLookUpValueHome) EJBHomeFactory
          .lookUpLocalHome(LocalAdLookUpValueHome.JNDI_NAME, LocalAdLookUpValueHome.class);

    } catch (NamingException ex) {

      throw new EJBException(ex.getMessage());

    }

    try {

      Collection adLookUpValues =
          adLookUpValueHome.findByLuName("AR CUSTOMER BATCH - SOA", AD_CMPNY);

      Iterator i = adLookUpValues.iterator();

      while (i.hasNext()) {

        LocalAdLookUpValue adLookUpValue = (LocalAdLookUpValue) i.next();

        list.add(adLookUpValue.getLvName());

      }

      return list;

    } catch (Exception ex) {

      Debug.printStackTrace(ex);
      throw new EJBException(ex.getMessage());

    }

  }

  /**
   * @ejb:interface-method view-type="remote"
   * @jboss:method-attributes read-only="true"
   **/
  public ArrayList getArCstAll(
      Integer AD_BRNCH, Integer AD_CMPNY
  ) {

    Debug.print("ArInvoiceBatchSubmitControllerBean getArCstAll");

    LocalArCustomerHome arCustomerHome = null;

    ArrayList list = new ArrayList();

    // Initialize EJB Home

    try {

      arCustomerHome = (LocalArCustomerHome) EJBHomeFactory
          .lookUpLocalHome(LocalArCustomerHome.JNDI_NAME, LocalArCustomerHome.class);

    } catch (NamingException ex) {

      throw new EJBException(ex.getMessage());

    }

    try {

      Collection arCustomers = arCustomerHome.findEnabledCstAll(AD_BRNCH, AD_CMPNY);

      Iterator i = arCustomers.iterator();

      while (i.hasNext()) {

        LocalArCustomer arCustomer = (LocalArCustomer) i.next();

        list.add(arCustomer.getCstCustomerCode());

      }

      return list;

    } catch (Exception ex) {

      Debug.printStackTrace(ex);
      throw new EJBException(ex.getMessage());

    }

  }

  /**
   * @ejb:interface-method view-type="remote"
   * @jboss:method-attributes read-only="true"
   **/
  public ArrayList getGlFcAll(
      Integer AD_CMPNY
  ) {

    Debug.print("ArInvoiceBatchSubmitControllerBean getGlFcAll");

    LocalGlFunctionalCurrencyHome glFunctionalCurrencyHome = null;

    ArrayList list = new ArrayList();

    // Initialize EJB Home

    try {

      glFunctionalCurrencyHome = (LocalGlFunctionalCurrencyHome) EJBHomeFactory.lookUpLocalHome(
          LocalGlFunctionalCurrencyHome.JNDI_NAME, LocalGlFunctionalCurrencyHome.class);

    } catch (NamingException ex) {

      throw new EJBException(ex.getMessage());

    }

    try {

      Collection glFunctionalCurrencies = glFunctionalCurrencyHome.findFcAll(AD_CMPNY);

      Iterator i = glFunctionalCurrencies.iterator();

      while (i.hasNext()) {

        LocalGlFunctionalCurrency glFunctionalCurrency = (LocalGlFunctionalCurrency) i.next();

        list.add(glFunctionalCurrency.getFcName());

      }

      return list;

    } catch (Exception ex) {

      Debug.printStackTrace(ex);
      throw new EJBException(ex.getMessage());

    }

  }

  /**
   * @ejb:interface-method view-type="remote"
   * @jboss:method-attributes read-only="true"
   **/
  public ArrayList getArOpenIbAll(
      Integer AD_BRNCH, Integer AD_CMPNY
  ) {

    Debug.print("ArInvoiceBatchSubmitControllerBean getArOpenIbAll");

    LocalArInvoiceBatchHome arInvoiceBatchHome = null;

    ArrayList list = new ArrayList();

    // Initialize EJB Home

    try {

      arInvoiceBatchHome = (LocalArInvoiceBatchHome) EJBHomeFactory
          .lookUpLocalHome(LocalArInvoiceBatchHome.JNDI_NAME, LocalArInvoiceBatchHome.class);

    } catch (NamingException ex) {

      throw new EJBException(ex.getMessage());

    }

    try {

      Collection arInvoiceBatches = arInvoiceBatchHome.findOpenIbAll(AD_BRNCH, AD_CMPNY);

      Iterator i = arInvoiceBatches.iterator();

      while (i.hasNext()) {

        LocalArInvoiceBatch arInvoiceBatch = (LocalArInvoiceBatch) i.next();

        list.add(arInvoiceBatch.getIbName());

      }

      return list;

    } catch (Exception ex) {

      Debug.printStackTrace(ex);
      throw new EJBException(ex.getMessage());

    }

  }

  /**
   * @ejb:interface-method view-type="remote"
   * @jboss:method-attributes read-only="true"
   **/
  public byte getAdPrfEnableArInvoiceBatch(
      Integer AD_CMPNY
  ) {

    Debug.print("ArInvoiceBatchSubmitControllerBean getAdPrfEnableArInvoiceBatch");

    LocalAdPreferenceHome adPreferenceHome = null;


    // Initialize EJB Home

    try {

      adPreferenceHome = (LocalAdPreferenceHome) EJBHomeFactory
          .lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);

    } catch (NamingException ex) {

      throw new EJBException(ex.getMessage());

    }


    try {

      LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);

      return adPreference.getPrfEnableArInvoiceBatch();

    } catch (Exception ex) {

      Debug.printStackTrace(ex);
      throw new EJBException(ex.getMessage());

    }

  }

  /**
   * @ejb:interface-method view-type="remote"
   * @jboss:method-attributes read-only="true"
   **/
  public ArrayList getArInvByCriteria(
      HashMap criteria, Integer OFFSET, Integer LIMIT, String ORDER_BY, Integer AD_BRNCH,
      Integer AD_CMPNY
  ) throws GlobalNoRecordFoundException {

    Debug.print("ArInvoiceBatchSubmitControllerBean getArInvByCriteria");

    LocalArInvoiceHome arInvoiceHome = null;

    ArrayList list = new ArrayList();

    // initialized EJB Home

    try {

      arInvoiceHome = (LocalArInvoiceHome) EJBHomeFactory
          .lookUpLocalHome(LocalArInvoiceHome.JNDI_NAME, LocalArInvoiceHome.class);

    } catch (NamingException ex) {

      throw new EJBException(ex.getMessage());

    }

    try {

      StringBuffer jbossQl = new StringBuffer();
      jbossQl.append("SELECT OBJECT(inv) FROM ArInvoice inv ");

      boolean firstArgument = true;
      short ctr = 0;
      int criteriaSize = criteria.size() + 2;

      Object obj[];

      // Allocate the size of the object parameter

      if (criteria.containsKey("referenceNumber")) {

        criteriaSize--;

      }

      obj = new Object[criteriaSize];

      if (criteria.containsKey("referenceNumber")) {

        if (!firstArgument) {

          jbossQl.append("AND ");

        } else {

          firstArgument = false;
          jbossQl.append("WHERE ");

        }

        jbossQl.append(
            "inv.invReferenceNumber LIKE '%" + (String) criteria.get("referenceNumber") + "%' ");

      }

      if (criteria.containsKey("batchName")) {

        if (!firstArgument) {
          jbossQl.append("AND ");
        } else {
          firstArgument = false;
          jbossQl.append("WHERE ");
        }

        jbossQl.append("inv.arInvoiceBatch.ibName=?" + (ctr + 1) + " ");
        obj[ctr] = (String) criteria.get("batchName");
        ctr++;

      }

      if (criteria.containsKey("customerBatch")) {

        if (!firstArgument) {
          jbossQl.append("AND ");
        } else {
          firstArgument = false;
          jbossQl.append("WHERE ");
        }

        jbossQl.append("inv.arCustomer.cstCustomerBatch=?" + (ctr + 1) + " ");
        obj[ctr] = (String) criteria.get("customerBatch");
        ctr++;

      }

      if (criteria.containsKey("customerCode")) {

        if (!firstArgument) {
          jbossQl.append("AND ");
        } else {
          firstArgument = false;
          jbossQl.append("WHERE ");
        }

        jbossQl.append("inv.arCustomer.cstCustomerCode=?" + (ctr + 1) + " ");
        obj[ctr] = (String) criteria.get("customerCode");
        ctr++;

      }


      if (!firstArgument) {
        jbossQl.append("AND ");
      } else {
        firstArgument = false;
        jbossQl.append("WHERE ");
      }

      jbossQl.append("inv.invCreditMemo=?" + (ctr + 1) + " ");
      obj[ctr] = (Byte) criteria.get("creditMemo");
      ctr++;

      if (criteria.containsKey("dateFrom")) {

        if (!firstArgument) {
          jbossQl.append("AND ");
        } else {
          firstArgument = false;
          jbossQl.append("WHERE ");
        }
        jbossQl.append("inv.invDate>=?" + (ctr + 1) + " ");
        obj[ctr] = (Date) criteria.get("dateFrom");
        ctr++;
      }

      if (criteria.containsKey("dateTo")) {

        if (!firstArgument) {
          jbossQl.append("AND ");
        } else {
          firstArgument = false;
          jbossQl.append("WHERE ");
        }
        jbossQl.append("inv.invDate<=?" + (ctr + 1) + " ");
        obj[ctr] = (Date) criteria.get("dateTo");
        ctr++;

      }

      if (criteria.containsKey("invoiceNumberFrom")) {

        if (!firstArgument) {
          jbossQl.append("AND ");
        } else {
          firstArgument = false;
          jbossQl.append("WHERE ");
        }
        jbossQl.append("inv.invNumber>=?" + (ctr + 1) + " ");
        obj[ctr] = (String) criteria.get("invoiceNumberFrom");
        ctr++;
      }

      if (criteria.containsKey("invoiceNumberTo")) {

        if (!firstArgument) {
          jbossQl.append("AND ");
        } else {
          firstArgument = false;
          jbossQl.append("WHERE ");
        }
        jbossQl.append("inv.invNumber<=?" + (ctr + 1) + " ");
        obj[ctr] = (String) criteria.get("invoiceNumberTo");
        ctr++;

      }

      if (criteria.containsKey("currency")) {

        if (!firstArgument) {

          jbossQl.append("AND ");

        } else {

          firstArgument = false;
          jbossQl.append("WHERE ");

        }

        jbossQl.append("inv.glFunctionalCurrency.fcName=?" + (ctr + 1) + " ");
        obj[ctr] = (String) criteria.get("currency");
        ctr++;

      }

      if (!firstArgument) {

        jbossQl.append("AND ");

      } else {

        firstArgument = false;
        jbossQl.append("WHERE ");

      }

      jbossQl.append(
          "inv.invApprovalStatus IS NULL AND inv.invPosted = 0 AND inv.invVoid = 0 AND inv.invAdBranch ="
              + AD_BRNCH + " AND inv.invAdCompany=" + AD_CMPNY + " ");

      String orderBy = null;

      if (ORDER_BY.equals("CUSTOMER CODE")) {

        orderBy = "inv.arCustomer.cstCustomerCode";

      } else if (ORDER_BY.equals("INVOICE NUMBER")) {

        orderBy = "inv.invNumber";

      }

      if (orderBy != null) {

        jbossQl.append("ORDER BY " + orderBy + ", inv.invDate");

      } else {

        jbossQl.append("ORDER BY inv.invDate");

      }

      jbossQl.append(" OFFSET ?" + (ctr + 1));
      obj[ctr] = OFFSET;
      ctr++;

      jbossQl.append(" LIMIT ?" + (ctr + 1));
      obj[ctr] = LIMIT;
      ctr++;

      Collection arInvoices = arInvoiceHome.getInvByCriteria(jbossQl.toString(), obj);

      if (arInvoices.size() == 0)
        throw new GlobalNoRecordFoundException();

      Iterator i = arInvoices.iterator();

      while (i.hasNext()) {

        LocalArInvoice arInvoice = (LocalArInvoice) i.next();

        ArModInvoiceDetails mdetails = new ArModInvoiceDetails();
        mdetails.setInvCode(arInvoice.getInvCode());
        mdetails.setInvCstCustomerCode(arInvoice.getArCustomer().getCstCustomerCode());
        mdetails.setInvDate(arInvoice.getInvDate());
        mdetails.setInvNumber(arInvoice.getInvNumber());
        mdetails.setInvReferenceNumber(
            arInvoice.getInvCreditMemo() == EJBCommon.FALSE ? arInvoice.getInvReferenceNumber()
                : arInvoice.getInvCmInvoiceNumber());
        mdetails.setInvAmountDue(arInvoice.getInvAmountDue());

        if (!arInvoice.getArInvoiceLineItems().isEmpty()) {

          mdetails.setInvType("ITEMS");

        } else {

          mdetails.setInvType("MEMO LINES");

        }

        list.add(mdetails);

      }

      return list;

    } catch (GlobalNoRecordFoundException ex) {

      throw ex;

    } catch (Exception ex) {


      ex.printStackTrace();
      throw new EJBException(ex.getMessage());

    }

  }

  /**
   * @ejb:interface-method view-type="remote"
   **/
  public void executeArInvBatchSubmit(
      Integer INV_CODE, Integer AD_BRNCH, Integer AD_CMPNY
  ) throws GlobalRecordAlreadyDeletedException, GlobalTransactionAlreadyApprovedException,
      GlobalTransactionAlreadyPendingException, GlobalTransactionAlreadyPostedException,
      GlobalNoApprovalRequesterFoundException, GlobalNoApprovalApproverFoundException,
      GlobalTransactionAlreadyVoidException, GlJREffectiveDateNoPeriodExistException,
      GlJREffectiveDatePeriodClosedException, GlobalJournalNotBalanceException,
      GlobalInventoryDateException, GlobalBranchAccountNumberInvalidException,
      AdPRFCoaGlVarianceAccountNotFoundException {

    Debug.print("ArInvoiceBatchSubmitControllerBean executeArInvBatchSubmit");

    LocalArInvoiceHome arInvoiceHome = null;
    LocalAdApprovalHome adApprovalHome = null;
    LocalAdAmountLimitHome adAmountLimitHome = null;
    LocalAdApprovalUserHome adApprovalUserHome = null;
    LocalAdApprovalQueueHome adApprovalQueueHome = null;
    LocalAdApprovalCoaLineHome adApprovalCoaLineHome = null;
    LocalAdPreferenceHome adPreferenceHome = null;
    LocalInvCostingHome invCostingHome = null;
    LocalInvItemLocationHome invItemLocationHome = null;

    // Initialize EJB Home

    try {

      arInvoiceHome = (LocalArInvoiceHome) EJBHomeFactory
          .lookUpLocalHome(LocalArInvoiceHome.JNDI_NAME, LocalArInvoiceHome.class);
      adApprovalHome = (LocalAdApprovalHome) EJBHomeFactory
          .lookUpLocalHome(LocalAdApprovalHome.JNDI_NAME, LocalAdApprovalHome.class);
      adAmountLimitHome = (LocalAdAmountLimitHome) EJBHomeFactory
          .lookUpLocalHome(LocalAdAmountLimitHome.JNDI_NAME, LocalAdAmountLimitHome.class);
      adApprovalUserHome = (LocalAdApprovalUserHome) EJBHomeFactory
          .lookUpLocalHome(LocalAdApprovalUserHome.JNDI_NAME, LocalAdApprovalUserHome.class);
      adApprovalQueueHome = (LocalAdApprovalQueueHome) EJBHomeFactory
          .lookUpLocalHome(LocalAdApprovalQueueHome.JNDI_NAME, LocalAdApprovalQueueHome.class);
      adApprovalCoaLineHome = (LocalAdApprovalCoaLineHome) EJBHomeFactory
          .lookUpLocalHome(LocalAdApprovalCoaLineHome.JNDI_NAME, LocalAdApprovalCoaLineHome.class);
      adPreferenceHome = (LocalAdPreferenceHome) EJBHomeFactory
          .lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);
      invCostingHome = (LocalInvCostingHome) EJBHomeFactory
          .lookUpLocalHome(LocalInvCostingHome.JNDI_NAME, LocalInvCostingHome.class);
      invItemLocationHome = (LocalInvItemLocationHome) EJBHomeFactory
          .lookUpLocalHome(LocalInvItemLocationHome.JNDI_NAME, LocalInvItemLocationHome.class);

    } catch (NamingException ex) {

      throw new EJBException(ex.getMessage());

    }

    try {

      LocalArInvoice arInvoice = null;

      try {

        arInvoice = arInvoiceHome.findByPrimaryKey(INV_CODE);

      } catch (FinderException ex) {

        throw new GlobalRecordAlreadyDeletedException();

      }

      // validate invoice

      if (arInvoice.getInvApprovalStatus() != null) {

        if (arInvoice.getInvApprovalStatus().equals("APPROVED")
            || arInvoice.getInvApprovalStatus().equals("N/A")) {

          throw new GlobalTransactionAlreadyApprovedException();

        } else if (arInvoice.getInvApprovalStatus().equals("PENDING")) {

          throw new GlobalTransactionAlreadyPendingException();

        }

      }

      if (arInvoice.getInvPosted() == EJBCommon.TRUE) {

        throw new GlobalTransactionAlreadyPostedException();

      }


      // generate approval status

      String ADC_TYP =
          arInvoice.getInvCreditMemo() == EJBCommon.FALSE ? "AR INVOICE" : "AR CREDIT MEMO";

      String INV_APPRVL_STATUS = null;

      LocalAdApproval adApproval = adApprovalHome.findByAprAdCompany(AD_CMPNY);

      // check if ar invoice approval is enabled

      if (ADC_TYP.equals("AR INVOICE") && adApproval.getAprEnableArInvoice() == EJBCommon.FALSE) {

        INV_APPRVL_STATUS = "N/A";

      } else if (ADC_TYP.equals("AR CREDIT MEMO")
          && adApproval.getAprEnableArCreditMemo() == EJBCommon.FALSE) {

        INV_APPRVL_STATUS = "N/A";

      } else {

        // check if invoice is self approved

        LocalAdAmountLimit adAmountLimit = null;

        try {

          adAmountLimit = adAmountLimitHome.findByAdcTypeAndAuTypeAndUsrName(ADC_TYP, "REQUESTER",
              arInvoice.getInvLastModifiedBy(), AD_CMPNY);

        } catch (FinderException ex) {

          throw new GlobalNoApprovalRequesterFoundException();

        }

        if (arInvoice.getInvAmountDue() <= adAmountLimit.getCalAmountLimit()) {

          INV_APPRVL_STATUS = "N/A";

        } else {

          // for approval, create approval queue

          Collection adAmountLimits = adAmountLimitHome.findByAdcTypeAndGreaterThanCalAmountLimit(
              ADC_TYP, adAmountLimit.getCalAmountLimit(), AD_CMPNY);

          if (adAmountLimits.isEmpty()) {

            Collection adApprovalUsers = adApprovalUserHome.findByAuTypeAndCalCode("APPROVER",
                adAmountLimit.getCalCode(), AD_CMPNY);

            if (adApprovalUsers.isEmpty()) {

              throw new GlobalNoApprovalApproverFoundException();

            }

            Iterator j = adApprovalUsers.iterator();

            while (j.hasNext()) {

              LocalAdApprovalUser adApprovalUser = (LocalAdApprovalUser) j.next();

              LocalAdApprovalQueue adApprovalQueue = adApprovalQueueHome.create(EJBCommon.TRUE,
                  ADC_TYP, arInvoice.getInvCode(), arInvoice.getInvNumber(), arInvoice.getInvDate(),
                  adAmountLimit.getCalAndOr(), adApprovalUser.getAuOr(), AD_BRNCH, AD_CMPNY);

              adApprovalUser.getAdUser().addAdApprovalQueue(adApprovalQueue);

            }

          } else {

            boolean isApprovalUsersFound = false;

            Iterator i = adAmountLimits.iterator();

            while (i.hasNext()) {

              LocalAdAmountLimit adNextAmountLimit = (LocalAdAmountLimit) i.next();

              if (arInvoice.getInvAmountDue() <= adNextAmountLimit.getCalAmountLimit()) {

                Collection adApprovalUsers = adApprovalUserHome.findByAuTypeAndCalCode("APPROVER",
                    adAmountLimit.getCalCode(), AD_CMPNY);

                Iterator j = adApprovalUsers.iterator();

                while (j.hasNext()) {

                  isApprovalUsersFound = true;

                  LocalAdApprovalUser adApprovalUser = (LocalAdApprovalUser) j.next();

                  LocalAdApprovalQueue adApprovalQueue = adApprovalQueueHome.create(EJBCommon.TRUE,
                      ADC_TYP, arInvoice.getInvCode(), arInvoice.getInvNumber(),
                      arInvoice.getInvDate(), adAmountLimit.getCalAndOr(), adApprovalUser.getAuOr(),
                      AD_BRNCH, AD_CMPNY);

                  adApprovalUser.getAdUser().addAdApprovalQueue(adApprovalQueue);

                }

                break;

              } else if (!i.hasNext()) {

                Collection adApprovalUsers = adApprovalUserHome.findByAuTypeAndCalCode(ADC_TYP,
                    adNextAmountLimit.getCalCode(), AD_CMPNY);

                Iterator j = adApprovalUsers.iterator();

                while (j.hasNext()) {

                  isApprovalUsersFound = true;

                  LocalAdApprovalUser adApprovalUser = (LocalAdApprovalUser) j.next();

                  LocalAdApprovalQueue adApprovalQueue = adApprovalQueueHome.create(EJBCommon.TRUE,
                      ADC_TYP, arInvoice.getInvCode(), arInvoice.getInvNumber(),
                      arInvoice.getInvDate(), adNextAmountLimit.getCalAndOr(),
                      adApprovalUser.getAuOr(), AD_BRNCH, AD_CMPNY);

                  adApprovalUser.getAdUser().addAdApprovalQueue(adApprovalQueue);

                }

                break;

              }

              adAmountLimit = adNextAmountLimit;

            }

            if (!isApprovalUsersFound) {

              throw new GlobalNoApprovalApproverFoundException();

            }

          }

          INV_APPRVL_STATUS = "PENDING";
        }
      }

      LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);

      if (arInvoice != null) {

        // start date validation

        Collection arInvoiceLineItems = arInvoice.getArInvoiceLineItems();

        Iterator i = arInvoiceLineItems.iterator();

        while (i.hasNext()) {

          LocalArInvoiceLineItem arInvoiceLineItem = (LocalArInvoiceLineItem) i.next();

          if (adPreference.getPrfArAllowPriorDate() == EJBCommon.FALSE) {

            Collection invNegTxnCosting = invCostingHome
                .findNegTxnByGreaterThanCstDateAndIiNameAndLocName(arInvoice.getInvDate(),
                    arInvoiceLineItem.getInvItemLocation().getInvItem().getIiName(),
                    arInvoiceLineItem.getInvItemLocation().getInvLocation().getLocName(), AD_BRNCH,
                    AD_CMPNY);
            if (!invNegTxnCosting.isEmpty())
              throw new GlobalInventoryDateException(
                  arInvoiceLineItem.getInvItemLocation().getInvItem().getIiName());
          }


          if (arInvoiceLineItem.getIliEnableAutoBuild() == EJBCommon.TRUE
              && arInvoiceLineItem.getInvItemLocation().getInvItem().getIiClass().equals("Assembly")
              && adPreference.getPrfArAllowPriorDate() == EJBCommon.FALSE) {

            Collection invBillOfMaterials =
                arInvoiceLineItem.getInvItemLocation().getInvItem().getInvBillOfMaterials();

            Iterator j = invBillOfMaterials.iterator();

            while (j.hasNext()) {

              LocalInvBillOfMaterial invBillOfMaterial = (LocalInvBillOfMaterial) j.next();

              LocalInvItemLocation invIlRawMaterial = null;

              try {

                invIlRawMaterial = invItemLocationHome.findByLocNameAndIiName(
                    invBillOfMaterial.getBomLocName(), invBillOfMaterial.getBomIiName(), AD_CMPNY);

              } catch (FinderException ex) {

                throw new GlobalInvItemLocationNotFoundException(
                    String.valueOf(arInvoiceLineItem.getIliLine()) + " - Raw Mat. ("
                        + invBillOfMaterial.getBomIiName() + ")");

              }

              // start date validation
              if (adPreference.getPrfArAllowPriorDate() == EJBCommon.FALSE) {
                Collection invIlRawMatNegTxnCosting =
                    invCostingHome.findNegTxnByGreaterThanCstDateAndIiNameAndLocName(
                        arInvoice.getInvDate(), invIlRawMaterial.getInvItem().getIiName(),
                        invIlRawMaterial.getInvLocation().getLocName(), AD_BRNCH, AD_CMPNY);
                if (!invIlRawMatNegTxnCosting.isEmpty())
                  throw new GlobalInventoryDateException(
                      arInvoiceLineItem.getInvItemLocation().getInvItem().getIiName()
                          + " - Raw Mat. (" + invIlRawMaterial.getInvItem().getIiName() + ")");
              }

            }

          }
        }

      }

      if (INV_APPRVL_STATUS != null && INV_APPRVL_STATUS.equals("N/A")
          && adPreference.getPrfArGlPostingType().equals("AUTO-POST UPON APPROVAL")) {

        this.executeArInvPost(arInvoice.getInvCode(), arInvoice.getInvLastModifiedBy(), AD_BRNCH,
            AD_CMPNY);

        // Set SO Lock if Fully Served

        LocalArSalesOrder arExistingSalesOrder = null;

        try {

          if (arInvoice.getArSalesOrderInvoiceLines().size() > 0)
            arExistingSalesOrder =
                ((LocalArSalesOrderLine) arInvoice.getArSalesOrderInvoiceLines().toArray()[0])
                    .getArSalesOrder();

          Iterator solIter = arExistingSalesOrder.getArSalesOrderLines().iterator();

          boolean isOpenSO = false;

          while (solIter.hasNext()) {

            LocalArSalesOrderLine arSalesOrderLine = (LocalArSalesOrderLine) solIter.next();

            Iterator soInvLnIter = arSalesOrderLine.getArSalesOrderInvoiceLines().iterator();
            double QUANTITY_SOLD = 0d;

            while (soInvLnIter.hasNext()) {

              LocalArSalesOrderInvoiceLine arSalesOrderInvoiceLine =
                  (LocalArSalesOrderInvoiceLine) soInvLnIter.next();

              if (arSalesOrderInvoiceLine.getArInvoice().getInvPosted() == EJBCommon.TRUE) {

                QUANTITY_SOLD += arSalesOrderInvoiceLine.getSilQuantityDelivered();

              }

            }

            double TOTAL_REMAINING_QTY = arSalesOrderLine.getSolQuantity() - QUANTITY_SOLD;

            if (TOTAL_REMAINING_QTY > 0) {
              isOpenSO = true;
              break;
            }

          }

          if (isOpenSO)
            arExistingSalesOrder.setSoLock(EJBCommon.FALSE);
          else
            arExistingSalesOrder.setSoLock(EJBCommon.TRUE);


        } catch (Exception ex) {
          // Do Nothing
        }


      }

      // set invoice approval status

      arInvoice.setInvApprovalStatus(INV_APPRVL_STATUS);


    } catch (GlobalRecordAlreadyDeletedException ex) {

      getSessionContext().setRollbackOnly();
      throw ex;

    } catch (GlobalTransactionAlreadyApprovedException ex) {

      getSessionContext().setRollbackOnly();
      throw ex;

    } catch (GlobalTransactionAlreadyPendingException ex) {

      getSessionContext().setRollbackOnly();
      throw ex;

    } catch (GlobalTransactionAlreadyPostedException ex) {

      getSessionContext().setRollbackOnly();
      throw ex;

    } catch (GlobalNoApprovalRequesterFoundException ex) {

      getSessionContext().setRollbackOnly();
      throw ex;

    } catch (GlobalNoApprovalApproverFoundException ex) {

      getSessionContext().setRollbackOnly();
      throw ex;

    } catch (GlobalTransactionAlreadyVoidException ex) {

      getSessionContext().setRollbackOnly();
      throw ex;

    } catch (GlJREffectiveDateNoPeriodExistException ex) {

      getSessionContext().setRollbackOnly();
      throw ex;

    } catch (GlJREffectiveDatePeriodClosedException ex) {

      getSessionContext().setRollbackOnly();
      throw ex;

    } catch (GlobalJournalNotBalanceException ex) {

      getSessionContext().setRollbackOnly();
      throw ex;

    } catch (GlobalInventoryDateException ex) {

      getSessionContext().setRollbackOnly();
      throw ex;

    } catch (GlobalBranchAccountNumberInvalidException ex) {

      getSessionContext().setRollbackOnly();
      throw ex;

    } catch (AdPRFCoaGlVarianceAccountNotFoundException ex) {

      getSessionContext().setRollbackOnly();
      throw ex;

    } catch (Exception ex) {

      Debug.printStackTrace(ex);
      getSessionContext().setRollbackOnly();
      throw new EJBException(ex.getMessage());

    }


  }

  /**
   * @ejb:interface-method view-type="remote"
   * @jboss:method-attributes read-only="true"
   **/
  public short getGlFcPrecisionUnit(
      Integer AD_CMPNY
  ) {

    Debug.print("ArInvoiceBatchSubmitControllerBean getGlFcPrecisionUnit");


    LocalAdCompanyHome adCompanyHome = null;


    // Initialize EJB Home

    try {

      adCompanyHome = (LocalAdCompanyHome) EJBHomeFactory
          .lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);

    } catch (NamingException ex) {

      throw new EJBException(ex.getMessage());

    }

    try {

      LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);

      return adCompany.getGlFunctionalCurrency().getFcPrecision();

    } catch (Exception ex) {

      Debug.printStackTrace(ex);
      throw new EJBException(ex.getMessage());

    }

  }

  /**
   * @ejb:interface-method view-type="remote"
   * @jboss:method-attributes read-only="true"
   **/
  public byte getAdPrfArUseCustomerPulldown(
      Integer AD_CMPNY
  ) {

    Debug.print("ArInvoiceBatchSubmitControllerBean getAdPrfArUseCustomerPulldown");

    LocalAdPreferenceHome adPreferenceHome = null;

    // Initialize EJB Home

    try {

      adPreferenceHome = (LocalAdPreferenceHome) EJBHomeFactory
          .lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);

    } catch (NamingException ex) {

      throw new EJBException(ex.getMessage());

    }


    try {

      LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);

      return adPreference.getPrfArUseCustomerPulldown();

    } catch (Exception ex) {

      Debug.printStackTrace(ex);
      throw new EJBException(ex.getMessage());

    }

  }

  // private methods

  private void executeArInvPost(
      Integer INV_CODE, String USR_NM, Integer AD_BRNCH, Integer AD_CMPNY
  ) throws GlobalRecordAlreadyDeletedException, GlobalTransactionAlreadyPostedException,
      GlobalTransactionAlreadyVoidException, GlJREffectiveDateNoPeriodExistException,
      GlJREffectiveDatePeriodClosedException, GlobalJournalNotBalanceException,
      GlobalBranchAccountNumberInvalidException, AdPRFCoaGlVarianceAccountNotFoundException {

    Debug.print("ArInvoiceBatchSubmitControllerBean executeApInvPost");

    LocalArInvoiceHome arInvoiceHome = null;
    LocalAdCompanyHome adCompanyHome = null;
    LocalAdPreferenceHome adPreferenceHome = null;
    LocalGlSetOfBookHome glSetOfBookHome = null;
    LocalGlAccountingCalendarValueHome glAccountingCalendarValueHome = null;
    LocalGlJournalHome glJournalHome = null;
    LocalGlJournalBatchHome glJournalBatchHome = null;
    LocalGlSuspenseAccountHome glSuspenseAccountHome = null;
    LocalGlJournalLineHome glJournalLineHome = null;
    LocalGlJournalSourceHome glJournalSourceHome = null;
    LocalGlJournalCategoryHome glJournalCategoryHome = null;
    LocalGlFunctionalCurrencyHome glFunctionalCurrencyHome = null;
    LocalArDistributionRecordHome arDistributionRecordHome = null;
    LocalInvCostingHome invCostingHome = null;
    LocalInvItemHome invItemHome = null;
    LocalGlChartOfAccountHome glChartOfAccountHome = null;
    LocalGlForexLedgerHome glForexLedgerHome = null;
    LocalGlFunctionalCurrencyRateHome glFunctionalCurrencyRateHome = null;

    LocalArInvoice arInvoice = null;
    LocalArInvoice arCreditedInvoice = null;

    LocalInvItemLocationHome invItemLocationHome = null;

    InvItemEntryControllerHome homeII = null;
    InvItemEntryController ejbII = null;

    // Initialize EJB Home

    try {

      homeII = (InvItemEntryControllerHome) com.util.EJBHomeFactory
          .lookUpHome("ejb/InvItemEntryControllerEJB", InvItemEntryControllerHome.class);

      arInvoiceHome = (LocalArInvoiceHome) EJBHomeFactory
          .lookUpLocalHome(LocalArInvoiceHome.JNDI_NAME, LocalArInvoiceHome.class);
      adCompanyHome = (LocalAdCompanyHome) EJBHomeFactory
          .lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
      adPreferenceHome = (LocalAdPreferenceHome) EJBHomeFactory
          .lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);
      glSetOfBookHome = (LocalGlSetOfBookHome) EJBHomeFactory
          .lookUpLocalHome(LocalGlSetOfBookHome.JNDI_NAME, LocalGlSetOfBookHome.class);
      glAccountingCalendarValueHome = (LocalGlAccountingCalendarValueHome) EJBHomeFactory
          .lookUpLocalHome(LocalGlAccountingCalendarValueHome.JNDI_NAME,
              LocalGlAccountingCalendarValueHome.class);
      glJournalHome = (LocalGlJournalHome) EJBHomeFactory
          .lookUpLocalHome(LocalGlJournalHome.JNDI_NAME, LocalGlJournalHome.class);
      glJournalBatchHome = (LocalGlJournalBatchHome) EJBHomeFactory
          .lookUpLocalHome(LocalGlJournalBatchHome.JNDI_NAME, LocalGlJournalBatchHome.class);
      glSuspenseAccountHome = (LocalGlSuspenseAccountHome) EJBHomeFactory
          .lookUpLocalHome(LocalGlSuspenseAccountHome.JNDI_NAME, LocalGlSuspenseAccountHome.class);
      glJournalLineHome = (LocalGlJournalLineHome) EJBHomeFactory
          .lookUpLocalHome(LocalGlJournalLineHome.JNDI_NAME, LocalGlJournalLineHome.class);
      glJournalSourceHome = (LocalGlJournalSourceHome) EJBHomeFactory
          .lookUpLocalHome(LocalGlJournalSourceHome.JNDI_NAME, LocalGlJournalSourceHome.class);
      glJournalCategoryHome = (LocalGlJournalCategoryHome) EJBHomeFactory
          .lookUpLocalHome(LocalGlJournalCategoryHome.JNDI_NAME, LocalGlJournalCategoryHome.class);
      glFunctionalCurrencyHome = (LocalGlFunctionalCurrencyHome) EJBHomeFactory.lookUpLocalHome(
          LocalGlFunctionalCurrencyHome.JNDI_NAME, LocalGlFunctionalCurrencyHome.class);
      arDistributionRecordHome = (LocalArDistributionRecordHome) EJBHomeFactory.lookUpLocalHome(
          LocalArDistributionRecordHome.JNDI_NAME, LocalArDistributionRecordHome.class);
      invCostingHome = (LocalInvCostingHome) EJBHomeFactory
          .lookUpLocalHome(LocalInvCostingHome.JNDI_NAME, LocalInvCostingHome.class);
      invItemHome = (LocalInvItemHome) EJBHomeFactory.lookUpLocalHome(LocalInvItemHome.JNDI_NAME,
          LocalInvItemHome.class);
      glChartOfAccountHome = (LocalGlChartOfAccountHome) EJBHomeFactory
          .lookUpLocalHome(LocalGlChartOfAccountHome.JNDI_NAME, LocalGlChartOfAccountHome.class);
      glForexLedgerHome = (LocalGlForexLedgerHome) EJBHomeFactory
          .lookUpLocalHome(LocalGlForexLedgerHome.JNDI_NAME, LocalGlForexLedgerHome.class);
      glFunctionalCurrencyRateHome =
          (LocalGlFunctionalCurrencyRateHome) EJBHomeFactory.lookUpLocalHome(
              LocalGlFunctionalCurrencyRateHome.JNDI_NAME, LocalGlFunctionalCurrencyRateHome.class);
      invItemLocationHome = (LocalInvItemLocationHome) EJBHomeFactory
          .lookUpLocalHome(LocalInvItemLocationHome.JNDI_NAME, LocalInvItemLocationHome.class);

    } catch (NamingException ex) {

      throw new EJBException(ex.getMessage());

    }

    try {

      ejbII = homeII.create();
    } catch (RemoteException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    } catch (CreateException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }


    try {

      LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
      LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);

      // validate if invoice/credit memo is already deleted

      try {

        arInvoice = arInvoiceHome.findByPrimaryKey(INV_CODE);

      } catch (FinderException ex) {

        throw new GlobalRecordAlreadyDeletedException();

      }

      // validate if invoice/credit memo is already posted or void

      if (arInvoice.getInvPosted() == EJBCommon.TRUE) {

        throw new GlobalTransactionAlreadyPostedException();

      } else if (arInvoice.getInvVoid() == EJBCommon.TRUE) {

        throw new GlobalTransactionAlreadyVoidException();
      }

      // regenerate cogs & inventory dr
        /*
      if (adPreference.getPrfArAutoComputeCogs() == EJBCommon.TRUE) {
        this.regenerateInventoryDr(arInvoice, AD_BRNCH, AD_CMPNY);
      }
        */
      // post invoice/credit memo

      if (arInvoice.getInvCreditMemo() == EJBCommon.FALSE) {

        // increase customer balance

        double INV_AMNT =
            this.convertForeignToFunctionalCurrency(arInvoice.getGlFunctionalCurrency().getFcCode(),
                arInvoice.getGlFunctionalCurrency().getFcName(), arInvoice.getInvConversionDate(),
                arInvoice.getInvConversionRate(), arInvoice.getInvAmountDue(), AD_CMPNY);

        this.post(arInvoice.getInvDate(), INV_AMNT, arInvoice.getArCustomer(), AD_CMPNY);

        Collection arInvoiceLineItems = arInvoice.getArInvoiceLineItems();
        Collection arSalesOrderInvoiceLines = arInvoice.getArSalesOrderInvoiceLines();

        if (arInvoiceLineItems != null && !arInvoiceLineItems.isEmpty()) {

          Iterator c = arInvoiceLineItems.iterator();

          while (c.hasNext()) {

            LocalArInvoiceLineItem arInvoiceLineItem = (LocalArInvoiceLineItem) c.next();

            if (arInvoiceLineItem.getIliEnableAutoBuild() == 1 && arInvoiceLineItem
                .getInvItemLocation().getInvItem().getIiClass().equals("Assembly")) {

              String II_NM = arInvoiceLineItem.getInvItemLocation().getInvItem().getIiName();
              String LOC_NM = arInvoiceLineItem.getInvItemLocation().getInvLocation().getLocName();
              double QTY_SLD =
                  this.convertByUomFromAndItemAndQuantity(arInvoiceLineItem.getInvUnitOfMeasure(),
                      arInvoiceLineItem.getInvItemLocation().getInvItem(),
                      arInvoiceLineItem.getIliQuantity(), AD_CMPNY);

              LocalInvCosting invCosting = null;

              double TOTAL_AMOUNT = 0d;

              Collection invBillOfMaterials =
                  arInvoiceLineItem.getInvItemLocation().getInvItem().getInvBillOfMaterials();

              Iterator j = invBillOfMaterials.iterator();

              while (j.hasNext()) {

                LocalInvBillOfMaterial invBillOfMaterial = (LocalInvBillOfMaterial) j.next();

                LocalInvItemLocation invItemLocation = invItemLocationHome.findByLocNameAndIiName(
                    arInvoiceLineItem.getInvItemLocation().getInvLocation().getLocName(),
                    invBillOfMaterial.getBomIiName(), AD_CMPNY);

                LocalInvCosting invBomCosting = null;

                double BOM_QTY_NDD = 0d;
                double COST = 0d;
                double BOM_AMOUNT = 0d;

                LocalInvItem invItem =
                    invItemHome.findByIiName(invBillOfMaterial.getBomIiName(), AD_CMPNY);

                try {

                  invBomCosting = invCostingHome
                      .getByMaxCstDateToLongAndMaxCstLineNumberAndLessThanEqualCstDateAndIiNameAndLocName(
                          arInvoice.getInvDate(), invBillOfMaterial.getBomIiName(), LOC_NM,
                          AD_BRNCH, AD_CMPNY);


                } catch (FinderException ex) {

                }

                if (invBomCosting == null) {

                  // bom conversion
                  BOM_QTY_NDD = this.convertByUomFromAndItemAndQuantity(
                      invBillOfMaterial.getInvUnitOfMeasure(), invItem,
                      EJBCommon.roundIt(invBillOfMaterial.getBomQuantityNeeded() * QTY_SLD,
                          this.getInvGpQuantityPrecisionUnit(AD_CMPNY)),
                      AD_CMPNY);

                  BOM_AMOUNT = EJBCommon.roundIt(BOM_QTY_NDD * invItem.getIiUnitCost(),
                      adCompany.getGlFunctionalCurrency().getFcPrecision());

                  TOTAL_AMOUNT += Math.abs(BOM_AMOUNT);

                  this.postToBua(arInvoiceLineItem, arInvoice.getInvDate(), -BOM_QTY_NDD,
                      -BOM_AMOUNT, -BOM_QTY_NDD, -BOM_AMOUNT, invBillOfMaterial.getBomIiName(),
                      LOC_NM, 0d, null, AD_BRNCH, AD_CMPNY);


                } else {

                  // bom conversion
                  BOM_QTY_NDD = this.convertByUomFromAndItemAndQuantity(
                      invBillOfMaterial.getInvUnitOfMeasure(), invItem,
                      EJBCommon.roundIt(invBillOfMaterial.getBomQuantityNeeded() * QTY_SLD,
                          this.getInvGpQuantityPrecisionUnit(AD_CMPNY)),
                      AD_CMPNY);

                 // COST = Math.abs(invBomCosting.getCstRemainingValue()
                 //     / invBomCosting.getCstRemainingQuantity());

                   COST = ejbII.getInvIiUnitCostByIiNameAndUomName( arInvoiceLineItem.getInvItemLocation().getInvItem().getIiName(), arInvoiceLineItem.getInvUnitOfMeasure().getUomName(),  arInvoice.getInvDate(), AD_CMPNY);



                  BOM_AMOUNT = EJBCommon.roundIt(BOM_QTY_NDD * COST,
                      adCompany.getGlFunctionalCurrency().getFcPrecision());

                  TOTAL_AMOUNT += Math.abs(BOM_AMOUNT);

                  this.postToBua(arInvoiceLineItem, arInvoice.getInvDate(), -BOM_QTY_NDD,
                      -BOM_AMOUNT, invBomCosting.getCstRemainingQuantity() - BOM_QTY_NDD,
                      invBomCosting.getCstRemainingValue() - (BOM_AMOUNT),
                      invBillOfMaterial.getBomIiName(), LOC_NM, 0d, null, AD_BRNCH, AD_CMPNY);

                }

              }

              try {

                invCosting = invCostingHome
                    .getByMaxCstDateToLongAndMaxCstLineNumberAndLessThanEqualCstDateAndIiNameAndLocName(
                        arInvoice.getInvDate(), II_NM, LOC_NM, AD_BRNCH, AD_CMPNY);

              } catch (FinderException ex) {

              }

              if (invCosting == null) {

                this.postToBua(arInvoiceLineItem, arInvoice.getInvDate(), QTY_SLD, TOTAL_AMOUNT,
                    QTY_SLD, TOTAL_AMOUNT, II_NM, LOC_NM, 0d, null, AD_BRNCH, AD_CMPNY);

              } else {

                // compute cost variance
                double CST_VRNC_VL = 0d;

                if (invCosting.getCstRemainingQuantity() < 0)
                  CST_VRNC_VL = (invCosting.getCstRemainingQuantity() * (TOTAL_AMOUNT / QTY_SLD)
                      - invCosting.getCstRemainingValue());

                this.postToBua(arInvoiceLineItem, arInvoice.getInvDate(), QTY_SLD, TOTAL_AMOUNT,
                    invCosting.getCstRemainingQuantity() + QTY_SLD,
                    invCosting.getCstRemainingValue() + TOTAL_AMOUNT, II_NM, LOC_NM, CST_VRNC_VL,
                    USR_NM, AD_BRNCH, AD_CMPNY);

              }

              try {

                invCosting = invCostingHome
                    .getByMaxCstDateToLongAndMaxCstLineNumberAndLessThanEqualCstDateAndIiNameAndLocName(
                        arInvoice.getInvDate(), II_NM, LOC_NM, AD_BRNCH, AD_CMPNY);

              } catch (FinderException ex) {

              }

              if (invCosting == null) {

                this.postToInv(arInvoiceLineItem, arInvoice.getInvDate(), QTY_SLD, TOTAL_AMOUNT,
                    -QTY_SLD, -TOTAL_AMOUNT, 0d, null, AD_BRNCH, AD_CMPNY);

              } else {

                this.postToInv(arInvoiceLineItem, arInvoice.getInvDate(), QTY_SLD, TOTAL_AMOUNT,
                    invCosting.getCstRemainingQuantity() - QTY_SLD,
                    invCosting.getCstRemainingValue() - TOTAL_AMOUNT, 0d, null, AD_BRNCH, AD_CMPNY);

              }

            } else {

              String II_NM = arInvoiceLineItem.getInvItemLocation().getInvItem().getIiName();
              String LOC_NM = arInvoiceLineItem.getInvItemLocation().getInvLocation().getLocName();

              double QTY_SLD =
                  this.convertByUomFromAndItemAndQuantity(arInvoiceLineItem.getInvUnitOfMeasure(),
                      arInvoiceLineItem.getInvItemLocation().getInvItem(),
                      arInvoiceLineItem.getIliQuantity(), AD_CMPNY);

              LocalInvCosting invCosting = null;

              try {

                invCosting = invCostingHome
                    .getByMaxCstDateToLongAndMaxCstLineNumberAndLessThanEqualCstDateAndIiNameAndLocName(
                        arInvoice.getInvDate(), II_NM, LOC_NM, AD_BRNCH, AD_CMPNY);

              } catch (FinderException ex) {

              }

              double COST = 0d;

              if (invCosting == null) {

                COST = arInvoiceLineItem.getInvItemLocation().getInvItem().getIiUnitCost();

                this.postToInv(arInvoiceLineItem, arInvoice.getInvDate(), QTY_SLD, QTY_SLD * COST,
                    -QTY_SLD, -(QTY_SLD * COST), 0d, null, AD_BRNCH, AD_CMPNY);

              } else {


                if (invCosting.getInvItemLocation().getInvItem().getIiCostMethod()
                    .equals("Average")) {

                //  COST = Math.abs(
               //       invCosting.getCstRemainingValue() / invCosting.getCstRemainingQuantity());

                    COST = ejbII.getInvIiUnitCostByIiNameAndUomName( arInvoiceLineItem.getInvItemLocation().getInvItem().getIiName(), arInvoiceLineItem.getInvUnitOfMeasure().getUomName(),  arInvoice.getInvDate(), AD_CMPNY);


                  this.postToInv(arInvoiceLineItem, arInvoice.getInvDate(), QTY_SLD, QTY_SLD * COST,
                      invCosting.getCstRemainingQuantity() - QTY_SLD,
                      invCosting.getCstRemainingValue() - (QTY_SLD * COST), 0d, null, AD_BRNCH,
                      AD_CMPNY);
                } else if (invCosting.getInvItemLocation().getInvItem().getIiCostMethod()
                    .equals("FIFO")) {

                  double fifoCost = this.getInvFifoCost(invCosting.getCstDate(),
                      invCosting.getInvItemLocation().getIlCode(), -QTY_SLD,
                      arInvoiceLineItem.getIliUnitPrice(), true, AD_BRNCH, AD_CMPNY);

                  // post entries to database
                  this.postToInv(arInvoiceLineItem, arInvoice.getInvDate(), QTY_SLD,
                      fifoCost * QTY_SLD, invCosting.getCstRemainingQuantity() - QTY_SLD,
                      invCosting.getCstRemainingValue() - (fifoCost * QTY_SLD), 0d, null, AD_BRNCH,
                      AD_CMPNY);
                } else if (invCosting.getInvItemLocation().getInvItem().getIiCostMethod()
                    .equals("Standard")) {

                  double standardCost =
                      invCosting.getInvItemLocation().getInvItem().getIiUnitCost();

                  // post entries to database
                  this.postToInv(arInvoiceLineItem, arInvoice.getInvDate(), QTY_SLD,
                      standardCost * QTY_SLD, invCosting.getCstRemainingQuantity() - QTY_SLD,
                      invCosting.getCstRemainingValue() - (standardCost * QTY_SLD), 0d, null,
                      AD_BRNCH, AD_CMPNY);
                }

              }

            }

          }

        } else if (arSalesOrderInvoiceLines != null && !arSalesOrderInvoiceLines.isEmpty()) {

          Iterator c = arSalesOrderInvoiceLines.iterator();

          while (c.hasNext()) {

            LocalArSalesOrderInvoiceLine arSalesOrderInvoiceLine =
                (LocalArSalesOrderInvoiceLine) c.next();
            LocalArSalesOrderLine arSalesOrderLine = arSalesOrderInvoiceLine.getArSalesOrderLine();

            String II_NM = arSalesOrderLine.getInvItemLocation().getInvItem().getIiName();
            String LOC_NM = arSalesOrderLine.getInvItemLocation().getInvLocation().getLocName();
            double QTY_SLD =
                this.convertByUomFromAndItemAndQuantity(arSalesOrderLine.getInvUnitOfMeasure(),
                    arSalesOrderLine.getInvItemLocation().getInvItem(),
                    arSalesOrderInvoiceLine.getSilQuantityDelivered(), AD_CMPNY);

            LocalInvCosting invCosting = null;

            try {

              invCosting = invCostingHome
                  .getByMaxCstDateToLongAndMaxCstLineNumberAndLessThanEqualCstDateAndIiNameAndLocName(
                      arInvoice.getInvDate(), II_NM, LOC_NM, AD_BRNCH, AD_CMPNY);

            } catch (FinderException ex) {

            }

            double COST = 0d;

            if (invCosting == null) {

              COST = arSalesOrderLine.getInvItemLocation().getInvItem().getIiUnitCost();

              this.postToInvSo(arSalesOrderInvoiceLine, arInvoice.getInvDate(), QTY_SLD,
                  QTY_SLD * COST, -QTY_SLD, -(QTY_SLD * COST), 0d, null, AD_BRNCH, AD_CMPNY);

            } else {


              if (invCosting.getInvItemLocation().getInvItem().getIiCostMethod()
                  .equals("Average")) {
              //  COST = Math
             //       .abs(invCosting.getCstRemainingValue() / invCosting.getCstRemainingQuantity());


                COST = ejbII.getInvIiUnitCostByIiNameAndUomName( arSalesOrderLine.getInvItemLocation().getInvItem().getIiName(), arSalesOrderLine.getInvUnitOfMeasure().getUomName(),  arInvoice.getInvDate(), AD_CMPNY);



                this.postToInvSo(arSalesOrderInvoiceLine, arInvoice.getInvDate(), QTY_SLD,
                    QTY_SLD * COST, invCosting.getCstRemainingQuantity() - QTY_SLD,
                    invCosting.getCstRemainingValue() - (QTY_SLD * COST), 0d, null, AD_BRNCH,
                    AD_CMPNY);
              } else if (invCosting.getInvItemLocation().getInvItem().getIiCostMethod()
                  .equals("FIFO")) {
                double fifoCost = this.getInvFifoCost(invCosting.getCstDate(),
                    invCosting.getInvItemLocation().getIlCode(), -QTY_SLD,
                    arSalesOrderInvoiceLine.getArSalesOrderLine().getSolUnitPrice(), true, AD_BRNCH,
                    AD_CMPNY);

                // post entries to database
                this.postToInvSo(arSalesOrderInvoiceLine, arInvoice.getInvDate(), QTY_SLD,
                    fifoCost * QTY_SLD, invCosting.getCstRemainingQuantity() - QTY_SLD,
                    invCosting.getCstRemainingValue() - (fifoCost * QTY_SLD), 0d, null, AD_BRNCH,
                    AD_CMPNY);
              } else if (invCosting.getInvItemLocation().getInvItem().getIiCostMethod()
                  .equals("Standard")) {
                double standardCost = invCosting.getInvItemLocation().getInvItem().getIiUnitCost();

                // post entries to database
                this.postToInvSo(arSalesOrderInvoiceLine, arInvoice.getInvDate(), QTY_SLD,
                    standardCost * QTY_SLD, invCosting.getCstRemainingQuantity() - QTY_SLD,
                    invCosting.getCstRemainingValue() - (standardCost * QTY_SLD), 0d, null,
                    AD_BRNCH, AD_CMPNY);
              }
            }
          }
        }


      } else { // credit memo

        // get credited invoice

        arCreditedInvoice = arInvoiceHome.findByInvNumberAndInvCreditMemoAndBrCode(
            arInvoice.getInvCmInvoiceNumber(), EJBCommon.FALSE, AD_BRNCH, AD_CMPNY);

        // decrease customer balance

        double INV_AMNT = this.convertForeignToFunctionalCurrency(
            arCreditedInvoice.getGlFunctionalCurrency().getFcCode(),
            arCreditedInvoice.getGlFunctionalCurrency().getFcName(),
            arCreditedInvoice.getInvConversionDate(), arCreditedInvoice.getInvConversionRate(),
            arInvoice.getInvAmountDue(), AD_CMPNY) * -1;

        this.post(arInvoice.getInvDate(), INV_AMNT, arInvoice.getArCustomer(), AD_CMPNY);

        // decrease invoice and ips amounts and release lock

        double CREDIT_PERCENT = EJBCommon
            .roundIt(arInvoice.getInvAmountDue() / arCreditedInvoice.getInvAmountDue(), (short) 6);

        arCreditedInvoice
            .setInvAmountPaid(arCreditedInvoice.getInvAmountPaid() + arInvoice.getInvAmountDue());

        double TOTAL_INVOICE_PAYMENT_SCHEDULE = 0d;

        Collection arInvoicePaymentSchedules = arCreditedInvoice.getArInvoicePaymentSchedules();

        Iterator i = arInvoicePaymentSchedules.iterator();

        while (i.hasNext()) {

          LocalArInvoicePaymentSchedule arInvoicePaymentSchedule =
              (LocalArInvoicePaymentSchedule) i.next();

          double INVOICE_PAYMENT_SCHEDULE_AMOUNT = 0;

          // if last payment schedule subtract to avoid rounding difference error

          if (i.hasNext()) {

            INVOICE_PAYMENT_SCHEDULE_AMOUNT =
                EJBCommon.roundIt(arInvoicePaymentSchedule.getIpsAmountDue() * CREDIT_PERCENT,
                    this.getGlFcPrecisionUnit(AD_CMPNY));

          } else {

            INVOICE_PAYMENT_SCHEDULE_AMOUNT =
                arInvoice.getInvAmountDue() - TOTAL_INVOICE_PAYMENT_SCHEDULE;

          }

          arInvoicePaymentSchedule.setIpsAmountPaid(
              arInvoicePaymentSchedule.getIpsAmountPaid() + INVOICE_PAYMENT_SCHEDULE_AMOUNT);

          arInvoicePaymentSchedule.setIpsLock(EJBCommon.FALSE);

          TOTAL_INVOICE_PAYMENT_SCHEDULE += INVOICE_PAYMENT_SCHEDULE_AMOUNT;

        }

        Collection arInvoiceLineItems = arInvoice.getArInvoiceLineItems();

        if (arInvoiceLineItems != null && !arInvoiceLineItems.isEmpty()) {

          Iterator c = arInvoiceLineItems.iterator();

          while (c.hasNext()) {

            LocalArInvoiceLineItem arInvoiceLineItem = (LocalArInvoiceLineItem) c.next();

            if (arInvoiceLineItem.getIliEnableAutoBuild() == 1 && arInvoiceLineItem
                .getInvItemLocation().getInvItem().getIiClass().equals("Assembly")) {

              String II_NM = arInvoiceLineItem.getInvItemLocation().getInvItem().getIiName();
              String LOC_NM = arInvoiceLineItem.getInvItemLocation().getInvLocation().getLocName();
              double QTY_SLD =
                  this.convertByUomFromAndItemAndQuantity(arInvoiceLineItem.getInvUnitOfMeasure(),
                      arInvoiceLineItem.getInvItemLocation().getInvItem(),
                      arInvoiceLineItem.getIliQuantity(), AD_CMPNY);

              LocalInvCosting invCosting = null;

              double TOTAL_AMOUNT = 0d;

              Collection invBillOfMaterials =
                  arInvoiceLineItem.getInvItemLocation().getInvItem().getInvBillOfMaterials();

              Iterator j = invBillOfMaterials.iterator();

              while (j.hasNext()) {

                LocalInvBillOfMaterial invBillOfMaterial = (LocalInvBillOfMaterial) j.next();

                LocalInvCosting invBomCosting = null;

                double BOM_QTY_NDD = 0d;
                double COST = 0d;
                double BOM_AMOUNT = 0d;

                LocalInvItem invItem =
                    invItemHome.findByIiName(invBillOfMaterial.getBomIiName(), AD_CMPNY);

                try {

                  invBomCosting = invCostingHome
                      .getByMaxCstDateToLongAndMaxCstLineNumberAndLessThanEqualCstDateAndIiNameAndLocName(
                          arInvoice.getInvDate(), invBillOfMaterial.getBomIiName(), LOC_NM,
                          AD_BRNCH, AD_CMPNY);


                } catch (FinderException ex) {

                }

                if (invBomCosting == null) {

                  // bom conversion
                  BOM_QTY_NDD = this.convertByUomFromAndItemAndQuantity(
                      invBillOfMaterial.getInvUnitOfMeasure(), invItem,
                      EJBCommon.roundIt(invBillOfMaterial.getBomQuantityNeeded() * QTY_SLD,
                          this.getInvGpQuantityPrecisionUnit(AD_CMPNY)),
                      AD_CMPNY);

                  BOM_AMOUNT = EJBCommon.roundIt(BOM_QTY_NDD * invItem.getIiUnitCost(),
                      adCompany.getGlFunctionalCurrency().getFcPrecision());

                  TOTAL_AMOUNT += Math.abs(BOM_AMOUNT);

                  this.postToBua(arInvoiceLineItem, arInvoice.getInvDate(), BOM_QTY_NDD, BOM_AMOUNT,
                      BOM_QTY_NDD, BOM_AMOUNT, invBillOfMaterial.getBomIiName(), LOC_NM, 0d, null,
                      AD_BRNCH, AD_CMPNY);


                } else {

                  // bom conversion
                  BOM_QTY_NDD = this.convertByUomFromAndItemAndQuantity(
                      invBillOfMaterial.getInvUnitOfMeasure(), invItem,
                      EJBCommon.roundIt(invBillOfMaterial.getBomQuantityNeeded() * QTY_SLD,
                          this.getInvGpQuantityPrecisionUnit(AD_CMPNY)),
                      AD_CMPNY);

              //    COST = Math.abs(invBomCosting.getCstRemainingValue()
              //        / invBomCosting.getCstRemainingQuantity());
                  COST = ejbII.getInvIiUnitCostByIiNameAndUomName( arInvoiceLineItem.getInvItemLocation().getInvItem().getIiName(), arInvoiceLineItem.getInvUnitOfMeasure().getUomName(),  arInvoice.getInvDate(), AD_CMPNY);




                  BOM_AMOUNT = EJBCommon.roundIt(BOM_QTY_NDD * COST,
                      adCompany.getGlFunctionalCurrency().getFcPrecision());

                  TOTAL_AMOUNT += Math.abs(BOM_AMOUNT);

                  // compute cost variance
                  double CST_VRNC_VL = 0d;

                  if (invBomCosting.getCstRemainingQuantity() < 0)
                    CST_VRNC_VL =
                        (invBomCosting.getCstRemainingQuantity() * (BOM_QTY_NDD / BOM_QTY_NDD)
                            - invBomCosting.getCstRemainingValue());

                  this.postToBua(arInvoiceLineItem, arInvoice.getInvDate(), BOM_QTY_NDD,
                      BOM_QTY_NDD, invBomCosting.getCstRemainingQuantity() + BOM_QTY_NDD,
                      invBomCosting.getCstRemainingValue() + (BOM_AMOUNT),
                      invBillOfMaterial.getBomIiName(), LOC_NM, CST_VRNC_VL, USR_NM, AD_BRNCH,
                      AD_CMPNY);

                }

              }

              try {

                invCosting = invCostingHome
                    .getByMaxCstDateToLongAndMaxCstLineNumberAndLessThanEqualCstDateAndIiNameAndLocName(
                        arInvoice.getInvDate(), II_NM, LOC_NM, AD_BRNCH, AD_CMPNY);

              } catch (FinderException ex) {

              }

              if (invCosting == null) {

                this.postToBua(arInvoiceLineItem, arInvoice.getInvDate(), -QTY_SLD, -TOTAL_AMOUNT,
                    -QTY_SLD, -TOTAL_AMOUNT, II_NM, LOC_NM, 0d, null, AD_BRNCH, AD_CMPNY);

              } else {

                this.postToBua(arInvoiceLineItem, arInvoice.getInvDate(), -QTY_SLD, -TOTAL_AMOUNT,
                    invCosting.getCstRemainingQuantity() - QTY_SLD,
                    invCosting.getCstRemainingValue() - TOTAL_AMOUNT, II_NM, LOC_NM, 0d, null,
                    AD_BRNCH, AD_CMPNY);

              }

              try {

                invCosting = invCostingHome
                    .getByMaxCstDateToLongAndMaxCstLineNumberAndLessThanEqualCstDateAndIiNameAndLocName(
                        arInvoice.getInvDate(), II_NM, LOC_NM, AD_BRNCH, AD_CMPNY);

              } catch (FinderException ex) {

              }

              if (invCosting == null) {

                this.postToInv(arInvoiceLineItem, arInvoice.getInvDate(), -QTY_SLD, -TOTAL_AMOUNT,
                    QTY_SLD, TOTAL_AMOUNT, 0d, null, AD_BRNCH, AD_CMPNY);

              } else {

                // compute cost variance
                double CST_VRNC_VL = 0d;

                if (invCosting.getCstRemainingQuantity() < 0)
                  CST_VRNC_VL = (invCosting.getCstRemainingQuantity() * (TOTAL_AMOUNT / QTY_SLD)
                      - invCosting.getCstRemainingValue());

                this.postToInv(arInvoiceLineItem, arInvoice.getInvDate(), -QTY_SLD, -TOTAL_AMOUNT,
                    invCosting.getCstRemainingQuantity() + QTY_SLD,
                    invCosting.getCstRemainingValue() + TOTAL_AMOUNT, CST_VRNC_VL, USR_NM, AD_BRNCH,
                    AD_CMPNY);

              }

            } else {

              String II_NM = arInvoiceLineItem.getInvItemLocation().getInvItem().getIiName();
              String LOC_NM = arInvoiceLineItem.getInvItemLocation().getInvLocation().getLocName();
              double QTY_SLD =
                  this.convertByUomFromAndItemAndQuantity(arInvoiceLineItem.getInvUnitOfMeasure(),
                      arInvoiceLineItem.getInvItemLocation().getInvItem(),
                      arInvoiceLineItem.getIliQuantity(), AD_CMPNY);

              LocalInvCosting invCosting = null;

              try {

                invCosting = invCostingHome
                    .getByMaxCstDateToLongAndMaxCstLineNumberAndLessThanEqualCstDateAndIiNameAndLocName(
                        arInvoice.getInvDate(), II_NM, LOC_NM, AD_BRNCH, AD_CMPNY);

              } catch (FinderException ex) {

              }

              double COST = 0d;

              if (invCosting == null) {

                COST = arInvoiceLineItem.getInvItemLocation().getInvItem().getIiUnitCost();

                this.postToInv(arInvoiceLineItem, arInvoice.getInvDate(), -QTY_SLD,
                    -(QTY_SLD * COST), QTY_SLD, QTY_SLD * COST, 0d, null, AD_BRNCH, AD_CMPNY);

              } else {


                double CST_VRNC_VL = 0d;

                if (invCosting.getInvItemLocation().getInvItem().getIiCostMethod()
                    .equals("Average")) {

               //   COST = Math.abs(
            //          invCosting.getCstRemainingValue() / invCosting.getCstRemainingQuantity());

                   COST = ejbII.getInvIiUnitCostByIiNameAndUomName( arInvoiceLineItem.getInvItemLocation().getInvItem().getIiName(), arInvoiceLineItem.getInvUnitOfMeasure().getUomName(),  arInvoice.getInvDate(), AD_CMPNY);


                  // compute cost variance

                  if (invCosting.getCstRemainingQuantity() < 0)
                    CST_VRNC_VL = (invCosting.getCstRemainingQuantity() * COST
                        - invCosting.getCstRemainingValue());

                  this.postToInv(arInvoiceLineItem, arInvoice.getInvDate(), -QTY_SLD,
                      -(QTY_SLD * COST), invCosting.getCstRemainingQuantity() + QTY_SLD,
                      invCosting.getCstRemainingValue() + (QTY_SLD * COST), CST_VRNC_VL, USR_NM,
                      AD_BRNCH, AD_CMPNY);
                } else if (invCosting.getInvItemLocation().getInvItem().getIiCostMethod()
                    .equals("FIFO")) {
                  double fifoCost = this.getInvFifoCost(invCosting.getCstDate(),
                      invCosting.getInvItemLocation().getIlCode(), QTY_SLD,
                      arInvoiceLineItem.getIliUnitPrice(), true, AD_BRNCH, AD_CMPNY);

                  if (invCosting.getCstRemainingQuantity() < 0)
                    CST_VRNC_VL = (invCosting.getCstRemainingQuantity() * fifoCost
                        - invCosting.getCstRemainingValue());

                  // post entries to database
                  this.postToInv(arInvoiceLineItem, arInvoice.getInvDate(), -QTY_SLD,
                      fifoCost * -QTY_SLD, invCosting.getCstRemainingQuantity() + QTY_SLD,
                      invCosting.getCstRemainingValue() + (fifoCost * QTY_SLD), CST_VRNC_VL, USR_NM,
                      AD_BRNCH, AD_CMPNY);

                } else if (invCosting.getInvItemLocation().getInvItem().getIiCostMethod()
                    .equals("Standard")) {
                  double standardCost =
                      invCosting.getInvItemLocation().getInvItem().getIiUnitCost();

                  if (invCosting.getCstRemainingQuantity() < 0)
                    CST_VRNC_VL = (invCosting.getCstRemainingQuantity() * standardCost
                        - invCosting.getCstRemainingValue());

                  // post entries to database
                  this.postToInv(arInvoiceLineItem, arInvoice.getInvDate(), -QTY_SLD,
                      standardCost * -QTY_SLD, invCosting.getCstRemainingQuantity() + QTY_SLD,
                      invCosting.getCstRemainingValue() + (standardCost * QTY_SLD), CST_VRNC_VL,
                      USR_NM, AD_BRNCH, AD_CMPNY);

                }

              }

            }

          }

        }

      }

      // set invoice post status

      arInvoice.setInvPosted(EJBCommon.TRUE);
      arInvoice.setInvPostedBy(USR_NM);
      arInvoice.setInvDatePosted(EJBCommon.getGcCurrentDateWoTime().getTime());


      // post to gl if necessary

      if (adPreference.getPrfArGlPostingType().equals("AUTO-POST UPON APPROVAL")) {

        // validate if date has no period and period is closed

        LocalGlSetOfBook glJournalSetOfBook = null;

        try {

          glJournalSetOfBook = glSetOfBookHome.findByDate(arInvoice.getInvDate(), AD_CMPNY);

        } catch (FinderException ex) {

          throw new GlJREffectiveDateNoPeriodExistException();

        }

        LocalGlAccountingCalendarValue glAccountingCalendarValue = glAccountingCalendarValueHome
            .findByAcCodeAndDate(glJournalSetOfBook.getGlAccountingCalendar().getAcCode(),
                arInvoice.getInvDate(), AD_CMPNY);


        if (glAccountingCalendarValue.getAcvStatus() == 'N'
            || glAccountingCalendarValue.getAcvStatus() == 'C'
            || glAccountingCalendarValue.getAcvStatus() == 'P') {

          throw new GlJREffectiveDatePeriodClosedException();

        }

        // check if invoice is balance if not check suspense posting

        LocalGlJournalLine glOffsetJournalLine = null;

        Collection arDistributionRecords =
            arDistributionRecordHome.findImportableDrByInvCode(arInvoice.getInvCode(), AD_CMPNY);

        Iterator j = arDistributionRecords.iterator();

        double TOTAL_DEBIT = 0d;
        double TOTAL_CREDIT = 0d;

        while (j.hasNext()) {

          LocalArDistributionRecord arDistributionRecord = (LocalArDistributionRecord) j.next();

          double DR_AMNT = 0d;

          if (arInvoice.getInvCreditMemo() == EJBCommon.FALSE) {

            DR_AMNT = this.convertForeignToFunctionalCurrency(
                arInvoice.getGlFunctionalCurrency().getFcCode(),
                arInvoice.getGlFunctionalCurrency().getFcName(), arInvoice.getInvConversionDate(),
                arInvoice.getInvConversionRate(), arDistributionRecord.getDrAmount(), AD_CMPNY);

          } else {

            DR_AMNT = this.convertForeignToFunctionalCurrency(
                arCreditedInvoice.getGlFunctionalCurrency().getFcCode(),
                arCreditedInvoice.getGlFunctionalCurrency().getFcName(),
                arCreditedInvoice.getInvConversionDate(), arCreditedInvoice.getInvConversionRate(),
                arDistributionRecord.getDrAmount(), AD_CMPNY);

          }

          if (arDistributionRecord.getDrDebit() == EJBCommon.TRUE) {

            TOTAL_DEBIT += DR_AMNT;

          } else {

            TOTAL_CREDIT += DR_AMNT;

          }

        }

        TOTAL_DEBIT =
            EJBCommon.roundIt(TOTAL_DEBIT, adCompany.getGlFunctionalCurrency().getFcPrecision());
        TOTAL_CREDIT =
            EJBCommon.roundIt(TOTAL_CREDIT, adCompany.getGlFunctionalCurrency().getFcPrecision());

        if (adPreference.getPrfAllowSuspensePosting() == EJBCommon.TRUE
            && TOTAL_DEBIT != TOTAL_CREDIT) {

          LocalGlSuspenseAccount glSuspenseAccount = null;

          try {

            glSuspenseAccount = glSuspenseAccountHome.findByJsNameAndJcName("ACCOUNTS RECEIVABLES",
                arInvoice.getInvCreditMemo() == EJBCommon.FALSE ? "SALES INVOICES" : "CREDIT MEMOS",
                AD_CMPNY);

          } catch (FinderException ex) {

            throw new GlobalJournalNotBalanceException();

          }

          if (TOTAL_DEBIT - TOTAL_CREDIT < 0) {

            glOffsetJournalLine =
                glJournalLineHome.create((short) (arDistributionRecords.size() + 1), EJBCommon.TRUE,
                    TOTAL_CREDIT - TOTAL_DEBIT, "", AD_CMPNY);

          } else {

            glOffsetJournalLine =
                glJournalLineHome.create((short) (arDistributionRecords.size() + 1),
                    EJBCommon.FALSE, TOTAL_DEBIT - TOTAL_CREDIT, "", AD_CMPNY);

          }

          LocalGlChartOfAccount glChartOfAccount = glSuspenseAccount.getGlChartOfAccount();
          // glChartOfAccount.addGlJournalLine(glOffsetJournalLine);

          glOffsetJournalLine.setGlChartOfAccount(glChartOfAccount);


        } else if (adPreference.getPrfAllowSuspensePosting() == EJBCommon.FALSE
            && TOTAL_DEBIT != TOTAL_CREDIT) {

          throw new GlobalJournalNotBalanceException();

        }

        // create journal batch if necessary

        LocalGlJournalBatch glJournalBatch = null;
        java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat("MM/dd/yyyy");

        try {

          if (adPreference.getPrfEnableArInvoiceBatch() == EJBCommon.TRUE) {

            glJournalBatch =
                glJournalBatchHome.findByJbName("JOURNAL IMPORT " + formatter.format(new Date())
                    + " " + arInvoice.getArInvoiceBatch().getIbName(), AD_BRNCH, AD_CMPNY);

          } else {

            glJournalBatch = glJournalBatchHome.findByJbName(
                "JOURNAL IMPORT " + formatter.format(new Date()) + " SALES INVOICES", AD_BRNCH,
                AD_CMPNY);

          }


        } catch (FinderException ex) {
        }

        if (adPreference.getPrfEnableGlJournalBatch() == EJBCommon.TRUE && glJournalBatch == null) {

          if (adPreference.getPrfEnableArInvoiceBatch() == EJBCommon.TRUE) {

            glJournalBatch = glJournalBatchHome.create(
                "JOURNAL IMPORT " + formatter.format(new Date()) + " "
                    + arInvoice.getArInvoiceBatch().getIbName(),
                "JOURNAL IMPORT", "CLOSED", EJBCommon.getGcCurrentDateWoTime().getTime(), USR_NM,
                AD_BRNCH, AD_CMPNY);

          } else {

            glJournalBatch = glJournalBatchHome.create(
                "JOURNAL IMPORT " + formatter.format(new Date()) + " SALES INVOICES",
                "JOURNAL IMPORT", "CLOSED", EJBCommon.getGcCurrentDateWoTime().getTime(), USR_NM,
                AD_BRNCH, AD_CMPNY);

          }


        }

        // create journal entry

        LocalGlJournal glJournal = glJournalHome.create(arInvoice.getInvReferenceNumber(),
            arInvoice.getInvDescription(), arInvoice.getInvDate(), 0.0d, null,
            arInvoice.getInvNumber(), null, 1d, "N/A", null, 'N', EJBCommon.TRUE, EJBCommon.FALSE,
            USR_NM, new Date(), USR_NM, new Date(), null, null, USR_NM,
            EJBCommon.getGcCurrentDateWoTime().getTime(), arInvoice.getArCustomer().getCstTin(),
            arInvoice.getArCustomer().getCstName(), EJBCommon.FALSE, null, AD_BRNCH, AD_CMPNY);

        LocalGlJournalSource glJournalSource =
            glJournalSourceHome.findByJsName("ACCOUNTS RECEIVABLES", AD_CMPNY);
        glJournal.setGlJournalSource(glJournalSource);

        LocalGlFunctionalCurrency glFunctionalCurrency = glFunctionalCurrencyHome
            .findByFcName(adCompany.getGlFunctionalCurrency().getFcName(), AD_CMPNY);
        glJournal.setGlFunctionalCurrency(glFunctionalCurrency);

        LocalGlJournalCategory glJournalCategory = glJournalCategoryHome.findByJcName(
            arInvoice.getInvCreditMemo() == EJBCommon.FALSE ? "SALES INVOICES" : "CREDIT MEMOS",
            AD_CMPNY);
        glJournal.setGlJournalCategory(glJournalCategory);

        if (glJournalBatch != null) {

          glJournal.setGlJournalBatch(glJournalBatch);

        }


        // create journal lines

        j = arDistributionRecords.iterator();

        while (j.hasNext()) {

          LocalArDistributionRecord arDistributionRecord = (LocalArDistributionRecord) j.next();

          double DR_AMNT = 0d;

          if (arInvoice.getInvCreditMemo() == EJBCommon.FALSE) {

            DR_AMNT = this.convertForeignToFunctionalCurrency(
                arInvoice.getGlFunctionalCurrency().getFcCode(),
                arInvoice.getGlFunctionalCurrency().getFcName(), arInvoice.getInvConversionDate(),
                arInvoice.getInvConversionRate(), arDistributionRecord.getDrAmount(), AD_CMPNY);

          } else {

            DR_AMNT = this.convertForeignToFunctionalCurrency(
                arCreditedInvoice.getGlFunctionalCurrency().getFcCode(),
                arCreditedInvoice.getGlFunctionalCurrency().getFcName(),
                arCreditedInvoice.getInvConversionDate(), arCreditedInvoice.getInvConversionRate(),
                arDistributionRecord.getDrAmount(), AD_CMPNY);

          }

          LocalGlJournalLine glJournalLine =
              glJournalLineHome.create(arDistributionRecord.getDrLine(),
                  arDistributionRecord.getDrDebit(), DR_AMNT, "", AD_CMPNY);

          // arDistributionRecord.getGlChartOfAccount().addGlJournalLine(glJournalLine);



          glJournalLine.setGlChartOfAccount(arDistributionRecord.getGlChartOfAccount());

          arDistributionRecord.setDrImported(EJBCommon.TRUE);

          // for FOREX revaluation

          LocalArInvoice arInvoiceTemp =
              arInvoice.getInvCreditMemo() == EJBCommon.FALSE ? arInvoice : arCreditedInvoice;

          if ((arInvoiceTemp.getGlFunctionalCurrency().getFcCode() != adCompany
              .getGlFunctionalCurrency().getFcCode())
              && glJournalLine.getGlChartOfAccount().getGlFunctionalCurrency() != null
              && (glJournalLine.getGlChartOfAccount().getGlFunctionalCurrency().getFcCode()
                  .equals(arInvoiceTemp.getGlFunctionalCurrency().getFcCode()))) {

            double CONVERSION_RATE = 1;

            if (arInvoiceTemp.getInvConversionRate() != 0
                && arInvoiceTemp.getInvConversionRate() != 1) {

              CONVERSION_RATE = arInvoiceTemp.getInvConversionRate();

            } else if (arInvoiceTemp.getInvConversionDate() != null) {

              CONVERSION_RATE = this.getFrRateByFrNameAndFrDate(
                  glJournalLine.getGlChartOfAccount().getGlFunctionalCurrency().getFcName(),
                  glJournal.getJrConversionDate(), AD_CMPNY);
            }

            Collection glForexLedgers = null;

            try {

              glForexLedgers =
                  glForexLedgerHome.findLatestGlFrlByFrlDateAndByCoaCode(arInvoiceTemp.getInvDate(),
                      glJournalLine.getGlChartOfAccount().getCoaCode(), AD_CMPNY);

            } catch (FinderException ex) {

            }

            LocalGlForexLedger glForexLedger =
                (glForexLedgers.isEmpty() || glForexLedgers == null) ? null
                    : (LocalGlForexLedger) glForexLedgers.iterator().next();

            int FRL_LN = (glForexLedger != null
                && glForexLedger.getFrlDate().compareTo(arInvoiceTemp.getInvDate()) == 0)
                    ? glForexLedger.getFrlLine().intValue() + 1
                    : 1;

            // compute balance
            double COA_FRX_BLNC = glForexLedger == null ? 0 : glForexLedger.getFrlBalance();
            double FRL_AMNT = arDistributionRecord.getDrAmount();

            if (glJournalLine.getGlChartOfAccount().getCoaAccountType().equalsIgnoreCase("ASSET"))
              FRL_AMNT =
                  (glJournalLine.getJlDebit() == EJBCommon.TRUE ? FRL_AMNT : (-1 * FRL_AMNT));
            else
              FRL_AMNT =
                  (glJournalLine.getJlDebit() == EJBCommon.TRUE ? (-1 * FRL_AMNT) : FRL_AMNT);

            COA_FRX_BLNC = COA_FRX_BLNC + FRL_AMNT;

            glForexLedger = glForexLedgerHome.create(arInvoiceTemp.getInvDate(),
                new Integer(FRL_LN), "SI", FRL_AMNT, CONVERSION_RATE, COA_FRX_BLNC, 0d, AD_CMPNY);

            // glJournalLine.getGlChartOfAccount().addGlForexLedger(glForexLedger);


            glForexLedger.setGlChartOfAccount(glJournalLine.getGlChartOfAccount());

            // propagate balances
            try {

              glForexLedgers = glForexLedgerHome.findByGreaterThanFrlDateAndCoaCode(
                  glForexLedger.getFrlDate(), glForexLedger.getGlChartOfAccount().getCoaCode(),
                  glForexLedger.getFrlAdCompany());

            } catch (FinderException ex) {

            }

            Iterator itrFrl = glForexLedgers.iterator();

            while (itrFrl.hasNext()) {

              glForexLedger = (LocalGlForexLedger) itrFrl.next();
              FRL_AMNT = arDistributionRecord.getDrAmount();

              if (glJournalLine.getGlChartOfAccount().getCoaAccountType().equalsIgnoreCase("ASSET"))
                FRL_AMNT =
                    (glJournalLine.getJlDebit() == EJBCommon.TRUE ? FRL_AMNT : (-1 * FRL_AMNT));
              else
                FRL_AMNT =
                    (glJournalLine.getJlDebit() == EJBCommon.TRUE ? (-1 * FRL_AMNT) : FRL_AMNT);

              glForexLedger.setFrlBalance(glForexLedger.getFrlBalance() + FRL_AMNT);

            }

          }

        }

        if (glOffsetJournalLine != null) {

          // glJournal.addGlJournalLine(glOffsetJournalLine);

          glOffsetJournalLine.setGlJournal(glJournal);

        }

        // post journal to gl

        Collection glJournalLines = glJournal.getGlJournalLines();

        Iterator i = glJournalLines.iterator();

        while (i.hasNext()) {

          LocalGlJournalLine glJournalLine = (LocalGlJournalLine) i.next();

          // post current to current acv

          this.postToGl(glAccountingCalendarValue, glJournalLine.getGlChartOfAccount(), true,
              glJournalLine.getJlDebit(), glJournalLine.getJlAmount(), AD_CMPNY);


          // post to subsequent acvs (propagate)

          Collection glSubsequentAccountingCalendarValues =
              glAccountingCalendarValueHome.findSubsequentAcvByAcCodeAndAcvPeriodNumber(
                  glJournalSetOfBook.getGlAccountingCalendar().getAcCode(),
                  glAccountingCalendarValue.getAcvPeriodNumber(), AD_CMPNY);

          Iterator acvsIter = glSubsequentAccountingCalendarValues.iterator();

          while (acvsIter.hasNext()) {

            LocalGlAccountingCalendarValue glSubsequentAccountingCalendarValue =
                (LocalGlAccountingCalendarValue) acvsIter.next();

            this.postToGl(glSubsequentAccountingCalendarValue, glJournalLine.getGlChartOfAccount(),
                false, glJournalLine.getJlDebit(), glJournalLine.getJlAmount(), AD_CMPNY);

          }

          // post to subsequent years if necessary

          Collection glSubsequentSetOfBooks = glSetOfBookHome.findSubsequentSobByAcYear(
              glJournalSetOfBook.getGlAccountingCalendar().getAcYear(), AD_CMPNY);

          if (!glSubsequentSetOfBooks.isEmpty() && glJournalSetOfBook.getSobYearEndClosed() == 1) {

            adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
            LocalGlChartOfAccount glRetainedEarningsAccount = glChartOfAccountHome
                .findByCoaAccountNumber(adCompany.getCmpRetainedEarnings(), AD_CMPNY);

            Iterator sobIter = glSubsequentSetOfBooks.iterator();

            while (sobIter.hasNext()) {

              LocalGlSetOfBook glSubsequentSetOfBook = (LocalGlSetOfBook) sobIter.next();

              String ACCOUNT_TYPE = glJournalLine.getGlChartOfAccount().getCoaAccountType();

              // post to subsequent acvs of subsequent set of book(propagate)

              Collection glAccountingCalendarValues = glAccountingCalendarValueHome.findByAcCode(
                  glSubsequentSetOfBook.getGlAccountingCalendar().getAcCode(), AD_CMPNY);

              Iterator acvIter = glAccountingCalendarValues.iterator();

              while (acvIter.hasNext()) {

                LocalGlAccountingCalendarValue glSubsequentAccountingCalendarValue =
                    (LocalGlAccountingCalendarValue) acvIter.next();

                if (ACCOUNT_TYPE.equals("ASSET") || ACCOUNT_TYPE.equals("LIABILITY")
                    || ACCOUNT_TYPE.equals("OWNERS EQUITY")) {

                  this.postToGl(glSubsequentAccountingCalendarValue,
                      glJournalLine.getGlChartOfAccount(), false, glJournalLine.getJlDebit(),
                      glJournalLine.getJlAmount(), AD_CMPNY);

                } else { // revenue & expense

                  this.postToGl(glSubsequentAccountingCalendarValue, glRetainedEarningsAccount,
                      false, glJournalLine.getJlDebit(), glJournalLine.getJlAmount(), AD_CMPNY);

                }

              }

              if (glSubsequentSetOfBook.getSobYearEndClosed() == 0)
                break;

            }

          }

        }

      }

    } catch (GlJREffectiveDateNoPeriodExistException ex) {

      getSessionContext().setRollbackOnly();
      throw ex;

    } catch (GlJREffectiveDatePeriodClosedException ex) {

      getSessionContext().setRollbackOnly();
      throw ex;

    } catch (GlobalJournalNotBalanceException ex) {

      getSessionContext().setRollbackOnly();
      throw ex;

    } catch (GlobalRecordAlreadyDeletedException ex) {

      getSessionContext().setRollbackOnly();
      throw ex;

    } catch (GlobalTransactionAlreadyPostedException ex) {

      getSessionContext().setRollbackOnly();
      throw ex;

    } catch (GlobalTransactionAlreadyVoidException ex) {

      getSessionContext().setRollbackOnly();
      throw ex;

   
    } catch (AdPRFCoaGlVarianceAccountNotFoundException ex) {

      getSessionContext().setRollbackOnly();
      throw ex;

    } catch (Exception ex) {

      Debug.printStackTrace(ex);
      getSessionContext().setRollbackOnly();
      throw new EJBException(ex.getMessage());

    }

  }

  private double getInvFifoCost(
      Date CST_DT, Integer IL_CODE, double CST_QTY, double CST_COST, boolean isAdjustFifo,
      Integer AD_BRNCH, Integer AD_CMPNY
  ) {

    LocalInvCostingHome invCostingHome = null;
    LocalInvItemLocationHome invItemLocationHome = null;

    // Initialize EJB Home

    try {

      invCostingHome = (LocalInvCostingHome) EJBHomeFactory
          .lookUpLocalHome(LocalInvCostingHome.JNDI_NAME, LocalInvCostingHome.class);
      invItemLocationHome = (LocalInvItemLocationHome) EJBHomeFactory
          .lookUpLocalHome(LocalInvItemLocationHome.JNDI_NAME, LocalInvItemLocationHome.class);
    } catch (NamingException ex) {

      throw new EJBException(ex.getMessage());
    }

    try {

      Collection invFifoCostings =
          invCostingHome.findFifoRemainingQuantityByLessThanOrEqualCstDateAndIlCodeAndBrCode(CST_DT,
              IL_CODE, AD_BRNCH, AD_CMPNY);

      if (invFifoCostings.size() > 0) {

        Iterator x = invFifoCostings.iterator();

        if (isAdjustFifo) {

          // executed during POST transaction

          double totalCost = 0d;
          double cost;

          if (CST_QTY < 0) {

            // for negative quantities
            double neededQty = -(CST_QTY);

            while (x.hasNext() && neededQty != 0) {

              LocalInvCosting invFifoCosting = (LocalInvCosting) x.next();

              if (invFifoCosting.getApPurchaseOrderLine() != null
                  || invFifoCosting.getApVoucherLineItem() != null) {
                cost = invFifoCosting.getCstItemCost() / invFifoCosting.getCstQuantityReceived();
              } else if (invFifoCosting.getArInvoiceLineItem() != null) {
                cost = invFifoCosting.getCstCostOfSales() / invFifoCosting.getCstQuantitySold();
              } else if (invFifoCosting.getInvBuildUnbuildAssemblyLine() != null) {
                cost =
                    invFifoCosting.getCstAssemblyCost() / invFifoCosting.getCstAssemblyQuantity();
              } else {
                cost = invFifoCosting.getCstAdjustCost() / invFifoCosting.getCstAdjustQuantity();
              }

              if (neededQty <= invFifoCosting.getCstRemainingLifoQuantity()) {

                invFifoCosting.setCstRemainingLifoQuantity(
                    invFifoCosting.getCstRemainingLifoQuantity() - neededQty);
                totalCost += (neededQty * cost);
                neededQty = 0d;
              } else {

                neededQty -= invFifoCosting.getCstRemainingLifoQuantity();
                totalCost += (invFifoCosting.getCstRemainingLifoQuantity() * cost);
                invFifoCosting.setCstRemainingLifoQuantity(0);
              }
            }

            // if needed qty is not yet satisfied but no more quantities to fetch, get the default
            // cost
            if (neededQty != 0) {

              LocalInvItemLocation invItemLocation = invItemLocationHome.findByPrimaryKey(IL_CODE);
              totalCost += (neededQty * invItemLocation.getInvItem().getIiUnitCost());
            }

            cost = totalCost / -CST_QTY;
          }

          else {

            // for positive quantities
            cost = CST_COST;
          }
          return cost;
        }

        else {

          // executed during ENTRY transaction

          LocalInvCosting invFifoCosting = (LocalInvCosting) x.next();

          if (invFifoCosting.getApPurchaseOrderLine() != null
              || invFifoCosting.getApVoucherLineItem() != null) {
            return EJBCommon.roundIt(
                invFifoCosting.getCstItemCost() / invFifoCosting.getCstQuantityReceived(),
                this.getGlFcPrecisionUnit(AD_CMPNY));
          } else if (invFifoCosting.getArInvoiceLineItem() != null) {
            return EJBCommon.roundIt(
                invFifoCosting.getCstCostOfSales() / invFifoCosting.getCstQuantitySold(),
                this.getGlFcPrecisionUnit(AD_CMPNY));
          } else if (invFifoCosting.getInvBuildUnbuildAssemblyLine() != null) {
            return EJBCommon.roundIt(
                invFifoCosting.getCstAssemblyCost() / invFifoCosting.getCstAssemblyQuantity(),
                this.getGlFcPrecisionUnit(AD_CMPNY));
          } else {
            return EJBCommon.roundIt(
                invFifoCosting.getCstAdjustCost() / invFifoCosting.getCstAdjustQuantity(),
                this.getGlFcPrecisionUnit(AD_CMPNY));
          }
        }
      } else {

        // most applicable in 1st entries of data
        LocalInvItemLocation invItemLocation = invItemLocationHome.findByPrimaryKey(IL_CODE);
        return invItemLocation.getInvItem().getIiUnitCost();
      }

    } catch (Exception ex) {
      Debug.printStackTrace(ex);
      throw new EJBException(ex.getMessage());
    }
  }



  private void post(
      Date INV_DT, double INV_AMNT, LocalArCustomer arCustomer, Integer AD_CMPNY
  ) {

    Debug.print("ArInvoiceBatchSubmitControllerBean post");

    LocalArCustomerBalanceHome arCustomerBalanceHome = null;

    // Initialize EJB Home

    try {

      arCustomerBalanceHome = (LocalArCustomerBalanceHome) EJBHomeFactory
          .lookUpLocalHome(LocalArCustomerBalanceHome.JNDI_NAME, LocalArCustomerBalanceHome.class);

    } catch (NamingException ex) {

      getSessionContext().setRollbackOnly();
      throw new EJBException(ex.getMessage());

    }

    try {

      // find customer balance before or equal invoice date

      Collection arCustomerBalances =
          arCustomerBalanceHome.findByBeforeOrEqualInvDateAndCstCustomerCode(INV_DT,
              arCustomer.getCstCustomerCode(), AD_CMPNY);

      if (!arCustomerBalances.isEmpty()) {

        // get last invoice

        ArrayList arCustomerBalanceList = new ArrayList(arCustomerBalances);

        LocalArCustomerBalance arCustomerBalance =
            (LocalArCustomerBalance) arCustomerBalanceList.get(arCustomerBalanceList.size() - 1);

        if (arCustomerBalance.getCbDate().before(INV_DT)) {

          // create new balance

          LocalArCustomerBalance apNewCustomerBalance = arCustomerBalanceHome.create(INV_DT,
              arCustomerBalance.getCbBalance() + INV_AMNT, AD_CMPNY);

          // arCustomer.addArCustomerBalance(apNewCustomerBalance);

          apNewCustomerBalance.setArCustomer(arCustomer);

        } else { // equals to invoice date

          arCustomerBalance.setCbBalance(arCustomerBalance.getCbBalance() + INV_AMNT);

        }

      } else {

        // create new balance

        LocalArCustomerBalance apNewCustomerBalance =
            arCustomerBalanceHome.create(INV_DT, INV_AMNT, AD_CMPNY);

        // arCustomer.addArCustomerBalance(apNewCustomerBalance);

        apNewCustomerBalance.setArCustomer(arCustomer);

      }

      // propagate to subsequent balances if necessary

      arCustomerBalances = arCustomerBalanceHome.findByAfterInvDateAndCstCustomerCode(INV_DT,
          arCustomer.getCstCustomerCode(), AD_CMPNY);

      Iterator i = arCustomerBalances.iterator();

      while (i.hasNext()) {

        LocalArCustomerBalance arCustomerBalance = (LocalArCustomerBalance) i.next();

        arCustomerBalance.setCbBalance(arCustomerBalance.getCbBalance() + INV_AMNT);

      }

    } catch (Exception ex) {

      Debug.printStackTrace(ex);
      getSessionContext().setRollbackOnly();
      throw new EJBException(ex.getMessage());

    }

  }

  private double convertForeignToFunctionalCurrency(
      Integer FC_CODE, String FC_NM, Date CONVERSION_DATE, double CONVERSION_RATE, double AMOUNT,
      Integer AD_CMPNY
  ) {

    Debug.print("ArInvoiceBatchSubmitControllerBean convertForeignToFunctionalCurrency");


    LocalGlFunctionalCurrencyRateHome glFunctionalCurrencyRateHome = null;
    LocalAdCompanyHome adCompanyHome = null;

    LocalAdCompany adCompany = null;

    // Initialize EJB Homes

    try {

      glFunctionalCurrencyRateHome =
          (LocalGlFunctionalCurrencyRateHome) EJBHomeFactory.lookUpLocalHome(
              LocalGlFunctionalCurrencyRateHome.JNDI_NAME, LocalGlFunctionalCurrencyRateHome.class);
      adCompanyHome = (LocalAdCompanyHome) EJBHomeFactory
          .lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);

    } catch (NamingException ex) {

      throw new EJBException(ex.getMessage());

    }

    // get company and extended precision

    try {

      adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);

    } catch (Exception ex) {

      throw new EJBException(ex.getMessage());

    }


    // Convert to functional currency if necessary

    if (CONVERSION_RATE != 1 && CONVERSION_RATE != 0) {

      AMOUNT = AMOUNT / CONVERSION_RATE;

    }

    return EJBCommon.roundIt(AMOUNT, adCompany.getGlFunctionalCurrency().getFcPrecision());

  }

  private void postToGl(
      LocalGlAccountingCalendarValue glAccountingCalendarValue,
      LocalGlChartOfAccount glChartOfAccount, boolean isCurrentAcv, byte isDebit, double JL_AMNT,
      Integer AD_CMPNY
  ) {

    Debug.print("ArInvoiceBatchSubmitControllerBean postToGl");

    LocalGlChartOfAccountBalanceHome glChartOfAccountBalanceHome = null;
    LocalAdCompanyHome adCompanyHome = null;


    // Initialize EJB Home

    try {

      glChartOfAccountBalanceHome =
          (LocalGlChartOfAccountBalanceHome) EJBHomeFactory.lookUpLocalHome(
              LocalGlChartOfAccountBalanceHome.JNDI_NAME, LocalGlChartOfAccountBalanceHome.class);
      adCompanyHome = (LocalAdCompanyHome) EJBHomeFactory
          .lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);


    } catch (NamingException ex) {

      throw new EJBException(ex.getMessage());

    }

    try {

      LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);

      LocalGlChartOfAccountBalance glChartOfAccountBalance =
          glChartOfAccountBalanceHome.findByAcvCodeAndCoaCode(
              glAccountingCalendarValue.getAcvCode(), glChartOfAccount.getCoaCode(), AD_CMPNY);

      String ACCOUNT_TYPE = glChartOfAccount.getCoaAccountType();
      short FC_EXTNDD_PRCSN = adCompany.getGlFunctionalCurrency().getFcPrecision();



      if (((ACCOUNT_TYPE.equals("ASSET") || ACCOUNT_TYPE.equals("EXPENSE"))
          && isDebit == EJBCommon.TRUE)
          || (!ACCOUNT_TYPE.equals("ASSET") && !ACCOUNT_TYPE.equals("EXPENSE")
              && isDebit == EJBCommon.FALSE)) {

        glChartOfAccountBalance.setCoabEndingBalance(EJBCommon
            .roundIt(glChartOfAccountBalance.getCoabEndingBalance() + JL_AMNT, FC_EXTNDD_PRCSN));

        if (!isCurrentAcv) {

          glChartOfAccountBalance.setCoabBeginningBalance(EJBCommon.roundIt(
              glChartOfAccountBalance.getCoabBeginningBalance() + JL_AMNT, FC_EXTNDD_PRCSN));

        }


      } else {

        glChartOfAccountBalance.setCoabEndingBalance(EJBCommon
            .roundIt(glChartOfAccountBalance.getCoabEndingBalance() - JL_AMNT, FC_EXTNDD_PRCSN));

        if (!isCurrentAcv) {

          glChartOfAccountBalance.setCoabBeginningBalance(EJBCommon.roundIt(
              glChartOfAccountBalance.getCoabBeginningBalance() - JL_AMNT, FC_EXTNDD_PRCSN));

        }

      }

      if (isCurrentAcv) {

        if (isDebit == EJBCommon.TRUE) {

          glChartOfAccountBalance.setCoabTotalDebit(EJBCommon
              .roundIt(glChartOfAccountBalance.getCoabTotalDebit() + JL_AMNT, FC_EXTNDD_PRCSN));

        } else {

          glChartOfAccountBalance.setCoabTotalCredit(EJBCommon
              .roundIt(glChartOfAccountBalance.getCoabTotalCredit() + JL_AMNT, FC_EXTNDD_PRCSN));
        }

      }

    } catch (Exception ex) {

      Debug.printStackTrace(ex);
      throw new EJBException(ex.getMessage());

    }


  }

  private void postToInv(
      LocalArInvoiceLineItem arInvoiceLineItem, Date CST_DT, double CST_QTY_SLD,
      double CST_CST_OF_SLS, double CST_RMNNG_QTY, double CST_RMNNG_VL, double CST_VRNC_VL,
      String USR_NM, Integer AD_BRNCH, Integer AD_CMPNY
  ) throws AdPRFCoaGlVarianceAccountNotFoundException {

    Debug.print("ArInvoiceBatchSubmitControllerBean postToInv");

    LocalInvCostingHome invCostingHome = null;
    LocalAdPreferenceHome adPreferenceHome = null;
    LocalAdCompanyHome adCompanyHome = null;
    LocalInvAdjustmentLineHome invAdjustmentLineHome = null;

    // Initialize EJB Home

    try {

      invCostingHome = (LocalInvCostingHome) EJBHomeFactory
          .lookUpLocalHome(LocalInvCostingHome.JNDI_NAME, LocalInvCostingHome.class);
      adPreferenceHome = (LocalAdPreferenceHome) EJBHomeFactory
          .lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);
      adCompanyHome = (LocalAdCompanyHome) EJBHomeFactory
          .lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
      invAdjustmentLineHome = (LocalInvAdjustmentLineHome) EJBHomeFactory
          .lookUpLocalHome(LocalInvAdjustmentLineHome.JNDI_NAME, LocalInvAdjustmentLineHome.class);

    } catch (NamingException ex) {

      throw new EJBException(ex.getMessage());

    }

    try {

      LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);
      LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
      LocalInvItemLocation invItemLocation = arInvoiceLineItem.getInvItemLocation();
      int CST_LN_NMBR = 0;

      CST_QTY_SLD = EJBCommon.roundIt(CST_QTY_SLD, adPreference.getPrfInvQuantityPrecisionUnit());
      CST_CST_OF_SLS =
          EJBCommon.roundIt(CST_CST_OF_SLS, adCompany.getGlFunctionalCurrency().getFcPrecision());
      CST_RMNNG_QTY =
          EJBCommon.roundIt(CST_RMNNG_QTY, adPreference.getPrfInvQuantityPrecisionUnit());
      CST_RMNNG_VL =
          EJBCommon.roundIt(CST_RMNNG_VL, adCompany.getGlFunctionalCurrency().getFcPrecision());

      if (CST_QTY_SLD > 0 && arInvoiceLineItem.getIliEnableAutoBuild() == EJBCommon.FALSE) {

        invItemLocation
            .setIlCommittedQuantity(invItemLocation.getIlCommittedQuantity() - CST_QTY_SLD);

      }

      try {

        // generate line number
        LocalInvCosting invCurrentCosting =
            invCostingHome.getByMaxCstLineNumberAndCstDateToLongAndIiNameAndLocName(
                CST_DT.getTime(), invItemLocation.getInvItem().getIiName(),
                invItemLocation.getInvLocation().getLocName(), AD_BRNCH, AD_CMPNY);
        CST_LN_NMBR = invCurrentCosting.getCstLineNumber() + 1;

      } catch (FinderException ex) {

        CST_LN_NMBR = 1;

      }

      if (adPreference.getPrfArAllowPriorDate() == EJBCommon.FALSE) {
        // void subsequent cost variance adjustments
        Collection invAdjustmentLines =
            invAdjustmentLineHome.findUnvoidAndIsCostVarianceGreaterThanAdjDateAndIlCodeAndBrCode(
                CST_DT, invItemLocation.getIlCode(), AD_BRNCH, AD_CMPNY);
        Iterator i = invAdjustmentLines.iterator();

        while (i.hasNext()) {

          LocalInvAdjustmentLine invAdjustmentLine = (LocalInvAdjustmentLine) i.next();
          this.voidInvAdjustment(invAdjustmentLine.getInvAdjustment(), AD_BRNCH, AD_CMPNY);

        }
      }

      // create costing
      LocalInvCosting invCosting = invCostingHome.create(CST_DT, CST_DT.getTime(), CST_LN_NMBR, 0d,
          0d, 0d, 0d, 0d, 0d, CST_QTY_SLD, CST_CST_OF_SLS, CST_RMNNG_QTY, CST_RMNNG_VL, 0d, 0d,
          (CST_QTY_SLD < 0
              && (arInvoiceLineItem.getArInvoice().getInvCreditMemo() == EJBCommon.TRUE))
                  ? -CST_QTY_SLD
                  : 0d,
          AD_BRNCH, AD_CMPNY);
      // invItemLocation.addInvCosting(invCosting);


      invCosting.setInvItemLocation(invItemLocation);
      invCosting.setArInvoiceLineItem(arInvoiceLineItem);

      // if cost variance is not 0, generate cost variance for the transaction
      if (CST_VRNC_VL != 0 && adPreference.getPrfArAllowPriorDate() == EJBCommon.FALSE) {

        this.generateCostVariance(invCosting.getInvItemLocation(), CST_VRNC_VL,
            "ARCM" + arInvoiceLineItem.getArInvoice().getInvNumber(),
            arInvoiceLineItem.getArInvoice().getInvDescription(),
            arInvoiceLineItem.getArInvoice().getInvDate(), USR_NM, AD_BRNCH, AD_CMPNY);

      }


      // propagate balance if necessary
      Collection invCostings = invCostingHome.findByGreaterThanCstDateAndIiNameAndLocName(CST_DT,
          invItemLocation.getInvItem().getIiName(), invItemLocation.getInvLocation().getLocName(),
          AD_BRNCH, AD_CMPNY);

      Iterator i = invCostings.iterator();

      while (i.hasNext()) {

        LocalInvCosting invPropagatedCosting = (LocalInvCosting) i.next();

        invPropagatedCosting
            .setCstRemainingQuantity(invPropagatedCosting.getCstRemainingQuantity() - CST_QTY_SLD);
        invPropagatedCosting
            .setCstRemainingValue(invPropagatedCosting.getCstRemainingValue() - CST_CST_OF_SLS);

      }


      // regenerate cost varaince
      if (adPreference.getPrfArAllowPriorDate() == EJBCommon.FALSE) {
        this.regenerateCostVariance(invCostings, invCosting, AD_BRNCH, AD_CMPNY);
      }

    } catch (AdPRFCoaGlVarianceAccountNotFoundException ex) {

      getSessionContext().setRollbackOnly();
      throw ex;

    } catch (Exception ex) {

      Debug.printStackTrace(ex);
      getSessionContext().setRollbackOnly();
      throw new EJBException(ex.getMessage());

    }

  }

  private void postToBua(
      LocalArInvoiceLineItem arInvoiceLineItem, Date CST_DT, double CST_ASSMBLY_QTY,
      double CST_ASSMBLY_CST, double CST_RMNNG_QTY, double CST_RMNNG_VL, String II_NM,
      String LOC_NM, double CST_VRNC_VL, String USR_NM, Integer AD_BRNCH, Integer AD_CMPNY
  ) throws AdPRFCoaGlVarianceAccountNotFoundException {

    Debug.print("ArInvoiceBatchSubmitControllerBean postToBua");

    LocalInvCostingHome invCostingHome = null;
    LocalAdPreferenceHome adPreferenceHome = null;
    LocalAdCompanyHome adCompanyHome = null;
    LocalInvItemLocationHome invItemLocationHome = null;
    LocalInvAdjustmentLineHome invAdjustmentLineHome = null;

    // Initialize EJB Home

    try {

      invCostingHome = (LocalInvCostingHome) EJBHomeFactory
          .lookUpLocalHome(LocalInvCostingHome.JNDI_NAME, LocalInvCostingHome.class);
      adPreferenceHome = (LocalAdPreferenceHome) EJBHomeFactory
          .lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);
      adCompanyHome = (LocalAdCompanyHome) EJBHomeFactory
          .lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
      invItemLocationHome = (LocalInvItemLocationHome) EJBHomeFactory
          .lookUpLocalHome(LocalInvItemLocationHome.JNDI_NAME, LocalInvItemLocationHome.class);
      invAdjustmentLineHome = (LocalInvAdjustmentLineHome) EJBHomeFactory
          .lookUpLocalHome(LocalInvAdjustmentLineHome.JNDI_NAME, LocalInvAdjustmentLineHome.class);

    } catch (NamingException ex) {

      throw new EJBException(ex.getMessage());

    }

    try {

      LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);
      LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
      LocalInvItemLocation invItemLocation =
          invItemLocationHome.findByLocNameAndIiName(LOC_NM, II_NM, AD_CMPNY);
      int CST_LN_NMBR = 0;

      CST_ASSMBLY_QTY =
          EJBCommon.roundIt(CST_ASSMBLY_QTY, adPreference.getPrfInvQuantityPrecisionUnit());
      CST_ASSMBLY_CST =
          EJBCommon.roundIt(CST_ASSMBLY_CST, adCompany.getGlFunctionalCurrency().getFcPrecision());
      CST_RMNNG_QTY =
          EJBCommon.roundIt(CST_RMNNG_QTY, adPreference.getPrfInvQuantityPrecisionUnit());
      CST_RMNNG_VL =
          EJBCommon.roundIt(CST_RMNNG_VL, adCompany.getGlFunctionalCurrency().getFcPrecision());

      if ((CST_ASSMBLY_QTY < 0 && arInvoiceLineItem.getIliEnableAutoBuild() == 0)
          || (CST_ASSMBLY_QTY < 0 && arInvoiceLineItem.getIliEnableAutoBuild() == 1
              && !arInvoiceLineItem.getInvItemLocation().getInvItem()
                  .equals(invItemLocation.getInvItem()))) {

        invItemLocation.setIlCommittedQuantity(
            invItemLocation.getIlCommittedQuantity() - Math.abs(CST_ASSMBLY_QTY));

      }

      try {

        // generate line number
        LocalInvCosting invCurrentCosting =
            invCostingHome.getByMaxCstLineNumberAndCstDateToLongAndIiNameAndLocName(
                CST_DT.getTime(), invItemLocation.getInvItem().getIiName(),
                invItemLocation.getInvLocation().getLocName(), AD_BRNCH, AD_CMPNY);
        CST_LN_NMBR = invCurrentCosting.getCstLineNumber() + 1;

      } catch (FinderException ex) {

        CST_LN_NMBR = 1;

      }

      if (adPreference.getPrfArAllowPriorDate() == EJBCommon.FALSE) {
        // void subsequent cost variance adjustments
        Collection invAdjustmentLines =
            invAdjustmentLineHome.findUnvoidAndIsCostVarianceGreaterThanAdjDateAndIlCodeAndBrCode(
                CST_DT, invItemLocation.getIlCode(), AD_BRNCH, AD_CMPNY);
        Iterator i = invAdjustmentLines.iterator();

        while (i.hasNext()) {

          LocalInvAdjustmentLine invAdjustmentLine = (LocalInvAdjustmentLine) i.next();
          this.voidInvAdjustment(invAdjustmentLine.getInvAdjustment(), AD_BRNCH, AD_CMPNY);

        }
      }

      // create costing
      LocalInvCosting invCosting = invCostingHome.create(CST_DT, CST_DT.getTime(), CST_LN_NMBR, 0d,
          0d, CST_ASSMBLY_QTY, CST_ASSMBLY_CST, 0d, 0d, 0d, 0d, CST_RMNNG_QTY, CST_RMNNG_VL, 0d, 0d,
          CST_ASSMBLY_QTY > 0 ? CST_ASSMBLY_QTY : 0, AD_BRNCH, AD_CMPNY);
      // invItemLocation.addInvCosting(invCosting);

      invCosting.setInvItemLocation(invItemLocation);
      invCosting.setArInvoiceLineItem(arInvoiceLineItem);

      // if cost variance is not 0, generate cost variance for the transaction
      if (CST_VRNC_VL != 0 && adPreference.getPrfArAllowPriorDate() == EJBCommon.FALSE) {

        this.generateCostVariance(invCosting.getInvItemLocation(), CST_VRNC_VL,
            "ARCM" + arInvoiceLineItem.getArInvoice().getInvNumber(),
            arInvoiceLineItem.getArInvoice().getInvDescription(),
            arInvoiceLineItem.getArInvoice().getInvDate(), USR_NM, AD_BRNCH, AD_CMPNY);

      }

      // propagate balance if necessary
      Collection invCostings = invCostingHome.findByGreaterThanCstDateAndIiNameAndLocName(CST_DT,
          invItemLocation.getInvItem().getIiName(), invItemLocation.getInvLocation().getLocName(),
          AD_BRNCH, AD_CMPNY);

      Iterator i = invCostings.iterator();

      while (i.hasNext()) {

        LocalInvCosting invPropagatedCosting = (LocalInvCosting) i.next();

        invPropagatedCosting.setCstRemainingQuantity(
            invPropagatedCosting.getCstRemainingQuantity() + CST_ASSMBLY_QTY);
        invPropagatedCosting
            .setCstRemainingValue(invPropagatedCosting.getCstRemainingValue() + CST_ASSMBLY_CST);

      }


      // regenerate cost varaince
      if (adPreference.getPrfArAllowPriorDate() == EJBCommon.FALSE) {
        this.regenerateCostVariance(invCostings, invCosting, AD_BRNCH, AD_CMPNY);
      }

    } catch (AdPRFCoaGlVarianceAccountNotFoundException ex) {

      getSessionContext().setRollbackOnly();
      throw ex;

    } catch (Exception ex) {

      Debug.printStackTrace(ex);
      getSessionContext().setRollbackOnly();
      throw new EJBException(ex.getMessage());

    }

  }

  private void postToInvSo(
      LocalArSalesOrderInvoiceLine arSalesOrderInvoiceLine, Date CST_DT, double CST_QTY_SLD,
      double CST_CST_OF_SLS, double CST_RMNNG_QTY, double CST_RMNNG_VL, double CST_VRNC_VL,
      String USR_NM, Integer AD_BRNCH, Integer AD_CMPNY
  ) throws AdPRFCoaGlVarianceAccountNotFoundException {

    Debug.print("ArInvoiceBatchSubmitControllerBean postToInvSo");

    LocalInvCostingHome invCostingHome = null;
    LocalAdPreferenceHome adPreferenceHome = null;
    LocalAdCompanyHome adCompanyHome = null;
    LocalInvAdjustmentLineHome invAdjustmentLineHome = null;

    // Initialize EJB Home

    try {

      invCostingHome = (LocalInvCostingHome) EJBHomeFactory
          .lookUpLocalHome(LocalInvCostingHome.JNDI_NAME, LocalInvCostingHome.class);
      adPreferenceHome = (LocalAdPreferenceHome) EJBHomeFactory
          .lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);
      adCompanyHome = (LocalAdCompanyHome) EJBHomeFactory
          .lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
      invAdjustmentLineHome = (LocalInvAdjustmentLineHome) EJBHomeFactory
          .lookUpLocalHome(LocalInvAdjustmentLineHome.JNDI_NAME, LocalInvAdjustmentLineHome.class);

    } catch (NamingException ex) {

      throw new EJBException(ex.getMessage());

    }

    try {

      LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);
      LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
      LocalArSalesOrderLine arSalesOrderLine = arSalesOrderInvoiceLine.getArSalesOrderLine();
      LocalInvItemLocation invItemLocation = arSalesOrderLine.getInvItemLocation();
      int CST_LN_NMBR = 0;

      CST_QTY_SLD = EJBCommon.roundIt(CST_QTY_SLD, adPreference.getPrfInvQuantityPrecisionUnit());
      CST_CST_OF_SLS =
          EJBCommon.roundIt(CST_CST_OF_SLS, adCompany.getGlFunctionalCurrency().getFcPrecision());
      CST_RMNNG_QTY =
          EJBCommon.roundIt(CST_RMNNG_QTY, adPreference.getPrfInvQuantityPrecisionUnit());
      CST_RMNNG_VL =
          EJBCommon.roundIt(CST_RMNNG_VL, adCompany.getGlFunctionalCurrency().getFcPrecision());

      try {

        // generate line number
        LocalInvCosting invCurrentCosting =
            invCostingHome.getByMaxCstLineNumberAndCstDateToLongAndIiNameAndLocName(
                CST_DT.getTime(), invItemLocation.getInvItem().getIiName(),
                invItemLocation.getInvLocation().getLocName(), AD_BRNCH, AD_CMPNY);
        CST_LN_NMBR = invCurrentCosting.getCstLineNumber() + 1;

      } catch (FinderException ex) {

        CST_LN_NMBR = 1;

      }

      if (adPreference.getPrfArAllowPriorDate() == EJBCommon.FALSE) {
        // void subsequent cost variance adjustments
        Collection invAdjustmentLines =
            invAdjustmentLineHome.findUnvoidAndIsCostVarianceGreaterThanAdjDateAndIlCodeAndBrCode(
                CST_DT, invItemLocation.getIlCode(), AD_BRNCH, AD_CMPNY);
        Iterator i = invAdjustmentLines.iterator();

        while (i.hasNext()) {

          LocalInvAdjustmentLine invAdjustmentLine = (LocalInvAdjustmentLine) i.next();
          this.voidInvAdjustment(invAdjustmentLine.getInvAdjustment(), AD_BRNCH, AD_CMPNY);

        }
      }

      // create costing
      LocalInvCosting invCosting = invCostingHome.create(CST_DT, CST_DT.getTime(), CST_LN_NMBR, 0d,
          0d, 0d, 0d, 0d, 0d, CST_QTY_SLD, CST_CST_OF_SLS, CST_RMNNG_QTY, CST_RMNNG_VL, 0d, 0d, 0d,
          AD_BRNCH, AD_CMPNY);
      // invItemLocation.addInvCosting(invCosting);

      invCosting.setInvItemLocation(invItemLocation);
      invCosting.setArSalesOrderInvoiceLine(arSalesOrderInvoiceLine);

      // if cost variance is not 0, generate cost variance for the transaction
      if (CST_VRNC_VL != 0 && adPreference.getPrfArAllowPriorDate() == EJBCommon.FALSE) {

        this.generateCostVariance(invCosting.getInvItemLocation(), CST_VRNC_VL,
            "ARCM" + arSalesOrderInvoiceLine.getArInvoice().getInvNumber(),
            arSalesOrderInvoiceLine.getArInvoice().getInvDescription(),
            arSalesOrderInvoiceLine.getArInvoice().getInvDate(), USR_NM, AD_BRNCH, AD_CMPNY);

      }

      // propagate balance if necessary
      Collection invCostings = invCostingHome.findByGreaterThanCstDateAndIiNameAndLocName(CST_DT,
          invItemLocation.getInvItem().getIiName(), invItemLocation.getInvLocation().getLocName(),
          AD_BRNCH, AD_CMPNY);

      Iterator i = invCostings.iterator();

      while (i.hasNext()) {

        LocalInvCosting invPropagatedCosting = (LocalInvCosting) i.next();

        invPropagatedCosting
            .setCstRemainingQuantity(invPropagatedCosting.getCstRemainingQuantity() + CST_QTY_SLD);
        invPropagatedCosting
            .setCstRemainingValue(invPropagatedCosting.getCstRemainingValue() + CST_CST_OF_SLS);

      }

      // regenerate cost varaince
      if (adPreference.getPrfArAllowPriorDate() == EJBCommon.FALSE) {
        this.regenerateCostVariance(invCostings, invCosting, AD_BRNCH, AD_CMPNY);
      }

    } catch (AdPRFCoaGlVarianceAccountNotFoundException ex) {

      getSessionContext().setRollbackOnly();
      throw ex;

    } catch (Exception ex) {

      Debug.printStackTrace(ex);
      getSessionContext().setRollbackOnly();
      throw new EJBException(ex.getMessage());

    }

  }


  private double convertByUomFromAndItemAndQuantity(
      LocalInvUnitOfMeasure invFromUnitOfMeasure, LocalInvItem invItem, double QTY_SLD,
      Integer AD_CMPNY
  ) {

    Debug.print("ArInvoiceBatchSubmitControllerBean convertByUomFromAndItemAndQuantity");

    LocalAdPreferenceHome adPreferenceHome = null;
    LocalInvUnitOfMeasureConversionHome invUnitOfMeasureConversionHome = null;


    // Initialize EJB Home

    try {

      adPreferenceHome = (LocalAdPreferenceHome) EJBHomeFactory
          .lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);
      invUnitOfMeasureConversionHome = (LocalInvUnitOfMeasureConversionHome) EJBHomeFactory
          .lookUpLocalHome(LocalInvUnitOfMeasureConversionHome.JNDI_NAME,
              LocalInvUnitOfMeasureConversionHome.class);

    } catch (NamingException ex) {

      throw new EJBException(ex.getMessage());

    }

    try {

      LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);

      LocalInvUnitOfMeasureConversion invUnitOfMeasureConversion =
          invUnitOfMeasureConversionHome.findUmcByIiNameAndUomName(invItem.getIiName(),
              invFromUnitOfMeasure.getUomName(), AD_CMPNY);
      LocalInvUnitOfMeasureConversion invDefaultUomConversion =
          invUnitOfMeasureConversionHome.findUmcByIiNameAndUomName(invItem.getIiName(),
              invItem.getInvUnitOfMeasure().getUomName(), AD_CMPNY);

      return EJBCommon.roundIt(
          QTY_SLD * invDefaultUomConversion.getUmcConversionFactor()
              / invUnitOfMeasureConversion.getUmcConversionFactor(),
          adPreference.getPrfInvQuantityPrecisionUnit());


    } catch (Exception ex) {

      Debug.printStackTrace(ex);
      getSessionContext().setRollbackOnly();
      throw new EJBException(ex.getMessage());

    }

  }

  private void regenerateInventoryDr(
      LocalArInvoice arInvoice, Integer AD_BRNCH, Integer AD_CMPNY
  ) throws GlobalInventoryDateException, GlobalBranchAccountNumberInvalidException {

    Debug.print("ArInvoiceBatchSubmitControllerBean regenerateInventoryDr");

    LocalArDistributionRecordHome arDistributionRecordHome = null;
    LocalGlChartOfAccountHome glChartOfAccountHome = null;
    LocalInvCostingHome invCostingHome = null;
    LocalInvItemHome invItemHome = null;
    LocalInvItemLocationHome invItemLocationHome = null;
    LocalAdPreferenceHome adPreferenceHome = null;
    InvRepItemCostingControllerHome homeRIC = null;
    InvRepItemCostingController ejbRIC = null;

    // Initialize EJB Home

    try {

      arDistributionRecordHome = (LocalArDistributionRecordHome) EJBHomeFactory.lookUpLocalHome(
          LocalArDistributionRecordHome.JNDI_NAME, LocalArDistributionRecordHome.class);
      glChartOfAccountHome = (LocalGlChartOfAccountHome) EJBHomeFactory
          .lookUpLocalHome(LocalGlChartOfAccountHome.JNDI_NAME, LocalGlChartOfAccountHome.class);
      invCostingHome = (LocalInvCostingHome) EJBHomeFactory
          .lookUpLocalHome(LocalInvCostingHome.JNDI_NAME, LocalInvCostingHome.class);
      invItemHome = (LocalInvItemHome) EJBHomeFactory.lookUpLocalHome(LocalInvItemHome.JNDI_NAME,
          LocalInvItemHome.class);
      invItemLocationHome = (LocalInvItemLocationHome) EJBHomeFactory
          .lookUpLocalHome(LocalInvItemLocationHome.JNDI_NAME, LocalInvItemLocationHome.class);
      adPreferenceHome = (LocalAdPreferenceHome) EJBHomeFactory
          .lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);
      homeRIC = (InvRepItemCostingControllerHome) com.util.EJBHomeFactory
          .lookUpHome("ejb/InvRepItemCostingControllerEJB", InvRepItemCostingControllerHome.class);

    } catch (NamingException ex) {

      throw new EJBException(ex.getMessage());

    }

    try {
      ejbRIC = homeRIC.create();
    } catch (RemoteException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    } catch (CreateException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }

    try {

      LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);

      // regenerate inventory distribution records

      Collection arDistributionRecords =
          arDistributionRecordHome.findImportableDrByInvCode(arInvoice.getInvCode(), AD_CMPNY);

      Iterator i = arDistributionRecords.iterator();

      while (i.hasNext()) {

        LocalArDistributionRecord arDistributionRecord = (LocalArDistributionRecord) i.next();

        if (arDistributionRecord.getDrClass().equals("COGS")
            || arDistributionRecord.getDrClass().equals("INVENTORY")) {

          i.remove();
          arDistributionRecord.remove();

        }

      }

      Collection arInvoiceLineItems = arInvoice.getArInvoiceLineItems();

      if (arInvoiceLineItems != null && !arInvoiceLineItems.isEmpty()) {

        i = arInvoiceLineItems.iterator();

        while (i.hasNext()) {

          LocalArInvoiceLineItem arInvoiceLineItem = (LocalArInvoiceLineItem) i.next();
          LocalInvItemLocation invItemLocation = arInvoiceLineItem.getInvItemLocation();

          // start date validation
          if (adPreference.getPrfArAllowPriorDate() == EJBCommon.FALSE) {
            Collection invNegTxnCosting =
                invCostingHome.findNegTxnByGreaterThanCstDateAndIiNameAndLocName(
                    arInvoice.getInvDate(), invItemLocation.getInvItem().getIiName(),
                    invItemLocation.getInvLocation().getLocName(), AD_BRNCH, AD_CMPNY);
            if (!invNegTxnCosting.isEmpty())
              throw new GlobalInventoryDateException(invItemLocation.getInvItem().getIiName());
          }
          // add cost of sales distribution and inventory



          double COST = invItemLocation.getInvItem().getIiUnitCost();


          try {

            LocalInvCosting invCosting = invCostingHome
                .getByMaxCstDateToLongAndMaxCstLineNumberAndLessThanEqualCstDateAndIiNameAndLocName(
                    arInvoice.getInvDate(), invItemLocation.getInvItem().getIiName(),
                    invItemLocation.getInvLocation().getLocName(), AD_BRNCH, AD_CMPNY);


            if (invCosting.getCstRemainingQuantity() <= 0
                && invCosting.getCstRemainingValue() <= 0) {

              HashMap criteria = new HashMap();
              criteria.put("itemName", invItemLocation.getInvItem().getIiName());
              criteria.put("location", invItemLocation.getInvLocation().getLocName());

              ArrayList branchList = new ArrayList();

              AdBranchDetails mdetails = new AdBranchDetails();
              mdetails.setBrCode(AD_BRNCH);
              branchList.add(mdetails);

              ejbRIC.executeInvFixItemCosting(criteria, branchList, AD_CMPNY);
              invCosting = invCostingHome
                  .getByMaxCstDateToLongAndMaxCstLineNumberAndLessThanEqualCstDateAndIiNameAndLocName(
                      arInvoice.getInvDate(), invItemLocation.getInvItem().getIiName(),
                      invItemLocation.getInvLocation().getLocName(), AD_BRNCH, AD_CMPNY);
            }

            if (invCosting.getInvItemLocation().getInvItem().getIiCostMethod().equals("Average"))

              COST = invCosting.getCstRemainingQuantity() <= 0 ? COST
                  : Math.abs(
                      invCosting.getCstRemainingValue() / invCosting.getCstRemainingQuantity());

            if (COST <= 0) {
              COST = arInvoiceLineItem.getInvItemLocation().getInvItem().getIiUnitCost();
            }

            else if (invCosting.getInvItemLocation().getInvItem().getIiCostMethod().equals("FIFO"))

              COST = Math.abs(this.getInvFifoCost(invCosting.getCstDate(),
                  invCosting.getInvItemLocation().getIlCode(), arInvoiceLineItem.getIliQuantity(),
                  arInvoiceLineItem.getIliUnitPrice(), false, AD_BRNCH, AD_CMPNY));

            else if (invCosting.getInvItemLocation().getInvItem().getIiCostMethod()
                .equals("Standard"))

              COST = invCosting.getInvItemLocation().getInvItem().getIiUnitCost();

          } catch (FinderException ex) {

            COST = arInvoiceLineItem.getInvItemLocation().getInvItem().getIiUnitCost();

          }

          double QTY_SLD =
              this.convertByUomFromAndItemAndQuantity(arInvoiceLineItem.getInvUnitOfMeasure(),
                  arInvoiceLineItem.getInvItemLocation().getInvItem(),
                  arInvoiceLineItem.getIliQuantity(), AD_CMPNY);

          if (arInvoiceLineItem.getIliEnableAutoBuild() == EJBCommon.FALSE) {

            if (arInvoice.getInvCreditMemo() == EJBCommon.FALSE) {

              this.addArDrIliEntry(arInvoice.getArDrNextLine(), "COGS", EJBCommon.TRUE,
                  COST * QTY_SLD,
                  arInvoiceLineItem.getInvItemLocation().getIlGlCoaCostOfSalesAccount(), arInvoice,
                  AD_BRNCH, AD_CMPNY);

              this.addArDrIliEntry(arInvoice.getArDrNextLine(), "INVENTORY", EJBCommon.FALSE,
                  COST * QTY_SLD,
                  arInvoiceLineItem.getInvItemLocation().getIlGlCoaInventoryAccount(), arInvoice,
                  AD_BRNCH, AD_CMPNY);

            } else {

              this.addArDrIliEntry(arInvoice.getArDrNextLine(), "COGS", EJBCommon.FALSE,
                  COST * QTY_SLD,
                  arInvoiceLineItem.getInvItemLocation().getIlGlCoaCostOfSalesAccount(), arInvoice,
                  AD_BRNCH, AD_CMPNY);

              this.addArDrIliEntry(arInvoice.getArDrNextLine(), "INVENTORY", EJBCommon.TRUE,
                  COST * QTY_SLD,
                  arInvoiceLineItem.getInvItemLocation().getIlGlCoaInventoryAccount(), arInvoice,
                  AD_BRNCH, AD_CMPNY);

            }

          }

          if (arInvoiceLineItem.getIliEnableAutoBuild() == EJBCommon.TRUE && arInvoiceLineItem
              .getInvItemLocation().getInvItem().getIiClass().equals("Assembly")) {

            byte DEBIT = EJBCommon.TRUE;

            double TOTAL_AMOUNT = 0;
            double ABS_TOTAL_AMOUNT = 0;

            LocalGlChartOfAccount glInventoryChartOfAccount = glChartOfAccountHome.findByPrimaryKey(
                arInvoiceLineItem.getInvItemLocation().getIlGlCoaInventoryAccount());

            Collection invBillOfMaterials =
                arInvoiceLineItem.getInvItemLocation().getInvItem().getInvBillOfMaterials();

            Iterator j = invBillOfMaterials.iterator();

            while (j.hasNext()) {

              LocalInvBillOfMaterial invBillOfMaterial = (LocalInvBillOfMaterial) j.next();

              // add bill of material quantity needed to item location

              LocalInvItemLocation invIlRawMaterial = null;

              try {

                invIlRawMaterial = invItemLocationHome.findByLocNameAndIiName(
                    invBillOfMaterial.getBomLocName(), invBillOfMaterial.getBomIiName(), AD_CMPNY);

              } catch (FinderException ex) {

                throw new GlobalInvItemLocationNotFoundException(
                    String.valueOf(arInvoiceLineItem.getIliLine()) + " - Raw Mat. ("
                        + invBillOfMaterial.getBomIiName() + ")");

              }

              double quantityNeeded = this.convertByUomFromAndItemAndQuantity(
                  invBillOfMaterial.getInvUnitOfMeasure(), invIlRawMaterial.getInvItem(),
                  EJBCommon.roundIt(invBillOfMaterial.getBomQuantityNeeded() * QTY_SLD,
                      this.getInvGpQuantityPrecisionUnit(AD_CMPNY)),
                  AD_CMPNY);

              // start date validation
              if (adPreference.getPrfArAllowPriorDate() == EJBCommon.FALSE) {
                Collection invIlRawMatNegTxnCosting =
                    invCostingHome.findNegTxnByGreaterThanCstDateAndIiNameAndLocName(
                        arInvoice.getInvDate(), invIlRawMaterial.getInvItem().getIiName(),
                        invIlRawMaterial.getInvLocation().getLocName(), AD_BRNCH, AD_CMPNY);
                if (!invIlRawMatNegTxnCosting.isEmpty())
                  throw new GlobalInventoryDateException(invItemLocation.getInvItem().getIiName()
                      + " - Raw Mat. (" + invIlRawMaterial.getInvItem().getIiName() + ")");
              }

              LocalInvItem invItem =
                  invItemHome.findByIiName(invBillOfMaterial.getBomIiName(), AD_CMPNY);

              double COSTING = 0d;

              try {

                LocalInvCosting invCosting = invCostingHome
                    .getByMaxCstDateToLongAndMaxCstLineNumberAndLessThanEqualCstDateAndIiNameAndLocName(
                        arInvoice.getInvDate(), invItem.getIiName(),
                        invBillOfMaterial.getBomLocName(), AD_BRNCH, AD_CMPNY);

                COSTING = Math
                    .abs(invCosting.getCstRemainingValue() / invCosting.getCstRemainingQuantity());

              } catch (FinderException ex) {

                COSTING = invItem.getIiUnitCost();

              }

              // bom conversion
              COSTING = this.convertCostByUom(invIlRawMaterial.getInvItem().getIiName(),
                  invBillOfMaterial.getInvUnitOfMeasure().getUomName(), COSTING, true, AD_CMPNY);

              double BOM_AMOUNT = EJBCommon.roundIt(
                  (QTY_SLD * (invBillOfMaterial.getBomQuantityNeeded() * COSTING)),
                  this.getGlFcPrecisionUnit(AD_CMPNY));

              TOTAL_AMOUNT += BOM_AMOUNT;
              ABS_TOTAL_AMOUNT += Math.abs(BOM_AMOUNT);

              // add inventory account

              if (arInvoice.getInvCreditMemo() == EJBCommon.FALSE) {

                this.addArDrIliEntry(arInvoice.getArDrNextLine(), "INVENTORY", EJBCommon.FALSE,
                    Math.abs(BOM_AMOUNT), invIlRawMaterial.getIlGlCoaInventoryAccount(), arInvoice,
                    AD_BRNCH, AD_CMPNY);

              } else {

                this.addArDrIliEntry(arInvoice.getArDrNextLine(), "INVENTORY", EJBCommon.TRUE,
                    Math.abs(BOM_AMOUNT), invIlRawMaterial.getIlGlCoaInventoryAccount(), arInvoice,
                    AD_BRNCH, AD_CMPNY);

              }

            }

            // add cost of sales account

            if (arInvoice.getInvCreditMemo() == EJBCommon.FALSE) {

              this.addArDrIliEntry(arInvoice.getArDrNextLine(), "COGS", EJBCommon.TRUE,
                  Math.abs(TOTAL_AMOUNT),
                  arInvoiceLineItem.getInvItemLocation().getIlGlCoaCostOfSalesAccount(), arInvoice,
                  AD_BRNCH, AD_CMPNY);

            } else {

              this.addArDrIliEntry(arInvoice.getArDrNextLine(), "COGS", EJBCommon.FALSE,
                  Math.abs(TOTAL_AMOUNT),
                  arInvoiceLineItem.getInvItemLocation().getIlGlCoaCostOfSalesAccount(), arInvoice,
                  AD_BRNCH, AD_CMPNY);

            }

          }

        }
      }

    } catch (GlobalInventoryDateException ex) {

      getSessionContext().setRollbackOnly();
      throw ex;

    } catch (GlobalBranchAccountNumberInvalidException ex) {

      getSessionContext().setRollbackOnly();
      throw ex;

    } catch (Exception ex) {

      Debug.printStackTrace(ex);
      getSessionContext().setRollbackOnly();
      throw new EJBException(ex.getMessage());

    }

  }

  private void addArDrIliEntry(
      short DR_LN, String DR_CLSS, byte DR_DBT, double DR_AMNT, Integer COA_CODE,
      LocalArInvoice arInvoice, Integer AD_BRNCH, Integer AD_CMPNY
  ) throws GlobalBranchAccountNumberInvalidException {

    Debug.print("ArInvoiceBatchSubmitControllerBean addArDrIliEntry");

    LocalArDistributionRecordHome arDistributionRecordHome = null;
    LocalGlChartOfAccountHome glChartOfAccountHome = null;
    LocalAdCompanyHome adCompanyHome = null;


    // Initialize EJB Home

    try {

      arDistributionRecordHome = (LocalArDistributionRecordHome) EJBHomeFactory.lookUpLocalHome(
          LocalArDistributionRecordHome.JNDI_NAME, LocalArDistributionRecordHome.class);
      glChartOfAccountHome = (LocalGlChartOfAccountHome) EJBHomeFactory
          .lookUpLocalHome(LocalGlChartOfAccountHome.JNDI_NAME, LocalGlChartOfAccountHome.class);
      adCompanyHome = (LocalAdCompanyHome) EJBHomeFactory
          .lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);

    } catch (NamingException ex) {

      throw new EJBException(ex.getMessage());

    }

    try {

      // get company

      LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);

      LocalGlChartOfAccount glChartOfAccount =
          glChartOfAccountHome.findByCoaCodeAndBranchCode(COA_CODE, AD_BRNCH, AD_CMPNY);

      // create distribution record

      LocalArDistributionRecord arDistributionRecord =
          arDistributionRecordHome.create(DR_LN, DR_CLSS, DR_DBT,
              EJBCommon.roundIt(DR_AMNT, adCompany.getGlFunctionalCurrency().getFcPrecision()),
              EJBCommon.FALSE, EJBCommon.FALSE, AD_CMPNY);

      // arInvoice.addArDistributionRecord(arDistributionRecord);

      arDistributionRecord.setArInvoice(arInvoice);
      // glChartOfAccount.addArDistributionRecord(arDistributionRecord);

      arDistributionRecord.setGlChartOfAccount(glChartOfAccount);

    } catch (FinderException ex) {

      throw new GlobalBranchAccountNumberInvalidException();

    } catch (Exception ex) {

      Debug.printStackTrace(ex);
      getSessionContext().setRollbackOnly();
      throw new EJBException(ex.getMessage());

    }

  }

  private short getInvGpQuantityPrecisionUnit(
      Integer AD_CMPNY
  ) {

    Debug.print("ArInvoiceBatchSubmitControllerBean getInvGpQuantityPrecisionUnit");

    LocalAdPreferenceHome adPreferenceHome = null;

    // Initialize EJB Home

    try {

      adPreferenceHome = (LocalAdPreferenceHome) EJBHomeFactory
          .lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);

    } catch (NamingException ex) {

      throw new EJBException(ex.getMessage());

    }


    try {

      LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);

      return adPreference.getPrfInvQuantityPrecisionUnit();

    } catch (Exception ex) {

      Debug.printStackTrace(ex);
      throw new EJBException(ex.getMessage());

    }

  }

  private double convertCostByUom(
      String II_NM, String UOM_NM, double unitCost, boolean isFromDefault, Integer AD_CMPNY
  ) {

    Debug.print("ArInvoiceBatchSubmitControllerBean convertCostByUom");

    LocalInvItemHome invItemHome = null;
    LocalInvUnitOfMeasureConversionHome invUnitOfMeasureConversionHome = null;

    // Initialize EJB Home

    try {

      invUnitOfMeasureConversionHome = (LocalInvUnitOfMeasureConversionHome) EJBHomeFactory
          .lookUpLocalHome(LocalInvUnitOfMeasureConversionHome.JNDI_NAME,
              LocalInvUnitOfMeasureConversionHome.class);
      invItemHome = (LocalInvItemHome) EJBHomeFactory.lookUpLocalHome(LocalInvItemHome.JNDI_NAME,
          LocalInvItemHome.class);

    } catch (NamingException ex) {

      throw new EJBException(ex.getMessage());

    }

    try {

      LocalInvItem invItem = invItemHome.findByIiName(II_NM, AD_CMPNY);
      LocalInvUnitOfMeasureConversion invUnitOfMeasureConversion =
          invUnitOfMeasureConversionHome.findUmcByIiNameAndUomName(II_NM, UOM_NM, AD_CMPNY);
      LocalInvUnitOfMeasureConversion invDefaultUomConversion = invUnitOfMeasureConversionHome
          .findUmcByIiNameAndUomName(II_NM, invItem.getInvUnitOfMeasure().getUomName(), AD_CMPNY);

      if (isFromDefault) {

        return unitCost * invDefaultUomConversion.getUmcConversionFactor()
            / invUnitOfMeasureConversion.getUmcConversionFactor();

      } else {

        return unitCost * invUnitOfMeasureConversion.getUmcConversionFactor()
            / invDefaultUomConversion.getUmcConversionFactor();

      }

    } catch (Exception ex) {

      Debug.printStackTrace(ex);
      getSessionContext().setRollbackOnly();
      throw new EJBException(ex.getMessage());

    }

  }

  private double getFrRateByFrNameAndFrDate(
      String FC_NM, Date CONVERSION_DATE, Integer AD_CMPNY
  ) throws GlobalConversionDateNotExistException {

    Debug.print("ArInvoiceBatchSubmitControllerBean getFrRateByFrNameAndFrDate");

    LocalGlFunctionalCurrencyRateHome glFunctionalCurrencyRateHome = null;
    LocalGlFunctionalCurrencyHome glFunctionalCurrencyHome = null;
    LocalAdCompanyHome adCompanyHome = null;

    // Initialize EJB Home

    try {

      glFunctionalCurrencyRateHome =
          (LocalGlFunctionalCurrencyRateHome) EJBHomeFactory.lookUpLocalHome(
              LocalGlFunctionalCurrencyRateHome.JNDI_NAME, LocalGlFunctionalCurrencyRateHome.class);
      glFunctionalCurrencyHome = (LocalGlFunctionalCurrencyHome) EJBHomeFactory.lookUpLocalHome(
          LocalGlFunctionalCurrencyHome.JNDI_NAME, LocalGlFunctionalCurrencyHome.class);
      adCompanyHome = (LocalAdCompanyHome) EJBHomeFactory
          .lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);

    } catch (NamingException ex) {

      throw new EJBException(ex.getMessage());

    }

    try {

      LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
      LocalGlFunctionalCurrency glFunctionalCurrency =
          glFunctionalCurrencyHome.findByFcName(FC_NM, AD_CMPNY);

      double CONVERSION_RATE = 1;

      // Get functional currency rate

      if (!FC_NM.equals("USD")) {

        LocalGlFunctionalCurrencyRate glFunctionalCurrencyRate = glFunctionalCurrencyRateHome
            .findByFcCodeAndDate(glFunctionalCurrency.getFcCode(), CONVERSION_DATE, AD_CMPNY);

        CONVERSION_RATE = glFunctionalCurrencyRate.getFrXToUsd();

      }

      // Get set of book functional currency rate if necessary

      if (!adCompany.getGlFunctionalCurrency().getFcName().equals("USD")) {

        LocalGlFunctionalCurrencyRate glCompanyFunctionalCurrencyRate =
            glFunctionalCurrencyRateHome.findByFcCodeAndDate(
                adCompany.getGlFunctionalCurrency().getFcCode(), CONVERSION_DATE, AD_CMPNY);

        CONVERSION_RATE = CONVERSION_RATE / glCompanyFunctionalCurrencyRate.getFrXToUsd();

      }

      return CONVERSION_RATE;

    } catch (FinderException ex) {

      getSessionContext().setRollbackOnly();
      throw new GlobalConversionDateNotExistException();

    } catch (Exception ex) {

      Debug.printStackTrace(ex);
      throw new EJBException(ex.getMessage());

    }

  }

  private void voidInvAdjustment(
      LocalInvAdjustment invAdjustment, Integer AD_BRNCH, Integer AD_CMPNY
  ) {

    Debug.print("ArInvoiceBatchSubmitControllerBean voidInvAdjustment");

    try {

      Collection invDistributionRecords = invAdjustment.getInvDistributionRecords();
      ArrayList list = new ArrayList();

      Iterator i = invDistributionRecords.iterator();

      while (i.hasNext()) {

        LocalInvDistributionRecord invDistributionRecord = (LocalInvDistributionRecord) i.next();

        list.add(invDistributionRecord);

      }

      i = list.iterator();

      while (i.hasNext()) {

        LocalInvDistributionRecord invDistributionRecord = (LocalInvDistributionRecord) i.next();

        this.addInvDrEntry(invAdjustment.getInvDrNextLine(), invDistributionRecord.getDrClass(),
            invDistributionRecord.getDrDebit() == EJBCommon.TRUE ? EJBCommon.FALSE : EJBCommon.TRUE,
            invDistributionRecord.getDrAmount(), EJBCommon.TRUE,
            invDistributionRecord.getInvChartOfAccount().getCoaCode(), invAdjustment, AD_BRNCH,
            AD_CMPNY);

      }

      Collection invAdjustmentLines = invAdjustment.getInvAdjustmentLines();
      i = invAdjustmentLines.iterator();
      list.clear();

      while (i.hasNext()) {

        LocalInvAdjustmentLine invAdjustmentLine = (LocalInvAdjustmentLine) i.next();

        list.add(invAdjustmentLine);

      }

      i = list.iterator();

      while (i.hasNext()) {

        LocalInvAdjustmentLine invAdjustmentLine = (LocalInvAdjustmentLine) i.next();

        this.addInvAlEntry(invAdjustmentLine.getInvItemLocation(), invAdjustment,
            (invAdjustmentLine.getAlUnitCost()) * -1, EJBCommon.TRUE, AD_CMPNY);

      }


      invAdjustment.setAdjVoid(EJBCommon.TRUE);

      this.executeInvAdjPost(invAdjustment.getAdjCode(), invAdjustment.getAdjLastModifiedBy(),
          AD_BRNCH, AD_CMPNY);

    } catch (Exception ex) {

      Debug.printStackTrace(ex);
      getSessionContext().setRollbackOnly();
      throw new EJBException(ex.getMessage());

    }

  }

  private void generateCostVariance(
      LocalInvItemLocation invItemLocation, double CST_VRNC_VL, String ADJ_RFRNC_NMBR,
      String ADJ_DSCRPTN, Date ADJ_DT, String USR_NM, Integer AD_BRNCH, Integer AD_CMPNY
  ) throws AdPRFCoaGlVarianceAccountNotFoundException {

    Debug.print("ArInvoiceBatchSubmitControllerBean generateCostVariance");
    /*
     * LocalAdPreferenceHome adPreferenceHome = null; LocalGlChartOfAccountHome glChartOfAccountHome
     * = null; LocalAdBranchItemLocationHome adBranchItemLocationHome = null;
     * 
     * // Initialize EJB Home
     * 
     * try {
     * 
     * adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
     * lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);
     * glChartOfAccountHome = (LocalGlChartOfAccountHome)EJBHomeFactory.
     * lookUpLocalHome(LocalGlChartOfAccountHome.JNDI_NAME, LocalGlChartOfAccountHome.class);
     * adBranchItemLocationHome = (LocalAdBranchItemLocationHome)EJBHomeFactory.
     * lookUpLocalHome(LocalAdBranchItemLocationHome.JNDI_NAME,
     * LocalAdBranchItemLocationHome.class);
     * 
     * 
     * } catch (NamingException ex) {
     * 
     * throw new EJBException(ex.getMessage());
     * 
     * }
     * 
     * try{
     * 
     * 
     * 
     * LocalInvAdjustment newInvAdjustment = this.saveInvAdjustment(ADJ_RFRNC_NMBR, ADJ_DSCRPTN,
     * ADJ_DT, USR_NM, AD_BRNCH, AD_CMPNY); LocalAdPreference adPreference =
     * adPreferenceHome.findByPrfAdCompany(AD_CMPNY); LocalGlChartOfAccount glCoaVarianceAccount =
     * null;
     * 
     * 
     * if(adPreference.getPrfInvGlCoaVarianceAccount() == null) throw new
     * AdPRFCoaGlVarianceAccountNotFoundException();
     * 
     * try{
     * 
     * glCoaVarianceAccount =
     * glChartOfAccountHome.findByPrimaryKey(adPreference.getPrfInvGlCoaVarianceAccount());
     * glCoaVarianceAccount.addInvAdjustment(newInvAdjustment);
     * 
     * } catch (FinderException ex) {
     * 
     * throw new AdPRFCoaGlVarianceAccountNotFoundException();
     * 
     * }
     * 
     * LocalInvAdjustmentLine invAdjustmentLine = this.addInvAlEntry(invItemLocation,
     * newInvAdjustment, CST_VRNC_VL, EJBCommon.FALSE, AD_CMPNY);
     * 
     * // check for branch mapping
     * 
     * LocalAdBranchItemLocation adBranchItemLocation = null;
     * 
     * try{
     * 
     * adBranchItemLocation = adBranchItemLocationHome.findBilByIlCodeAndBrCode(
     * invAdjustmentLine.getInvItemLocation().getIlCode(), AD_BRNCH, AD_CMPNY);
     * 
     * } catch (FinderException ex) {
     * 
     * }
     * 
     * LocalGlChartOfAccount glInventoryChartOfAccount = null;
     * 
     * if (adBranchItemLocation == null) {
     * 
     * glInventoryChartOfAccount = glChartOfAccountHome.findByPrimaryKey(
     * invAdjustmentLine.getInvItemLocation().getIlGlCoaInventoryAccount()); } else {
     * 
     * glInventoryChartOfAccount = glChartOfAccountHome.findByPrimaryKey(
     * adBranchItemLocation.getBilCoaGlInventoryAccount());
     * 
     * }
     * 
     * 
     * boolean isDebit = CST_VRNC_VL < 0 ? false : true;
     * 
     * //inventory dr this.addInvDrEntry(newInvAdjustment.getInvDrNextLine(), "INVENTORY", isDebit
     * == true ? EJBCommon.TRUE : EJBCommon.FALSE, Math.abs(CST_VRNC_VL), EJBCommon.FALSE,
     * glInventoryChartOfAccount.getCoaCode(), newInvAdjustment, AD_BRNCH, AD_CMPNY);
     * 
     * //variance dr this.addInvDrEntry(newInvAdjustment.getInvDrNextLine(),"VARIANCE", !isDebit ==
     * true ? EJBCommon.TRUE : EJBCommon.FALSE, Math.abs(CST_VRNC_VL), EJBCommon.FALSE,
     * glCoaVarianceAccount.getCoaCode(), newInvAdjustment, AD_BRNCH, AD_CMPNY);
     * 
     * this.executeInvAdjPost(newInvAdjustment.getAdjCode(),
     * newInvAdjustment.getAdjLastModifiedBy(), AD_BRNCH, AD_CMPNY);
     * 
     * } catch (AdPRFCoaGlVarianceAccountNotFoundException ex) {
     * 
     * getSessionContext().setRollbackOnly(); throw ex;
     * 
     * } catch (Exception ex) {
     * 
     * Debug.printStackTrace(ex); getSessionContext().setRollbackOnly(); throw new
     * EJBException(ex.getMessage());
     * 
     * }
     */
  }

  private void regenerateCostVariance(
      Collection invCostings, LocalInvCosting invCosting, Integer AD_BRNCH, Integer AD_CMPNY
  ) throws AdPRFCoaGlVarianceAccountNotFoundException {

    Debug.print("ArInvoiceBatchSubmitControllerBean regenerateCostVariance");
    /*
     * try {
     * 
     * Iterator i = invCostings.iterator(); LocalInvCosting prevInvCosting = invCosting;
     * 
     * while (i.hasNext()) {
     * 
     * LocalInvCosting invPropagatedCosting = (LocalInvCosting)i.next();
     * 
     * if(prevInvCosting.getCstRemainingQuantity() < 0) {
     * 
     * double TTL_CST = 0; double QNTY = 0; String ADJ_RFRNC_NMBR = ""; String ADJ_DSCRPTN = "";
     * String ADJ_CRTD_BY = "";
     * 
     * // get unit cost adjusment, document number and unit of measure if
     * (invPropagatedCosting.getApPurchaseOrderLine() != null) {
     * 
     * TTL_CST = invPropagatedCosting.getApPurchaseOrderLine().getPlAmount(); QNTY =
     * this.convertByUomFromAndItemAndQuantity(
     * invPropagatedCosting.getApPurchaseOrderLine().getInvUnitOfMeasure(),
     * invPropagatedCosting.getApPurchaseOrderLine().getInvItemLocation().getInvItem(),
     * invPropagatedCosting.getApPurchaseOrderLine().getPlQuantity(), AD_CMPNY);
     * 
     * ADJ_DSCRPTN =
     * invPropagatedCosting.getApPurchaseOrderLine().getApPurchaseOrder().getPoDescription();
     * ADJ_CRTD_BY =
     * invPropagatedCosting.getApPurchaseOrderLine().getApPurchaseOrder().getPoPostedBy();
     * ADJ_RFRNC_NMBR = "APRI" +
     * invPropagatedCosting.getApPurchaseOrderLine().getApPurchaseOrder().getPoDocumentNumber();
     * 
     * } else if (invPropagatedCosting.getApVoucherLineItem() != null){
     * 
     * TTL_CST = invPropagatedCosting.getApVoucherLineItem().getVliAmount(); QNTY =
     * this.convertByUomFromAndItemAndQuantity(
     * invPropagatedCosting.getApVoucherLineItem().getInvUnitOfMeasure(),
     * invPropagatedCosting.getApVoucherLineItem().getInvItemLocation().getInvItem(),
     * invPropagatedCosting.getApVoucherLineItem().getVliQuantity(), AD_CMPNY);
     * 
     * if (invPropagatedCosting.getApVoucherLineItem().getApVoucher() != null) {
     * 
     * ADJ_DSCRPTN = invPropagatedCosting.getApVoucherLineItem().getApVoucher().getVouDescription();
     * ADJ_CRTD_BY = invPropagatedCosting.getApVoucherLineItem().getApVoucher().getVouPostedBy();
     * ADJ_RFRNC_NMBR = "APVOU" +
     * invPropagatedCosting.getApVoucherLineItem().getApVoucher().getVouDocumentNumber();
     * 
     * } else if (invPropagatedCosting.getApVoucherLineItem().getApCheck() != null) {
     * 
     * ADJ_DSCRPTN = invPropagatedCosting.getApVoucherLineItem().getApCheck().getChkDescription();
     * ADJ_CRTD_BY = invPropagatedCosting.getApVoucherLineItem().getApCheck().getChkPostedBy();
     * ADJ_RFRNC_NMBR = "APCHK" +
     * invPropagatedCosting.getApVoucherLineItem().getApCheck().getChkDocumentNumber();
     * 
     * }
     * 
     * } else if (invPropagatedCosting.getArInvoiceLineItem() != null){
     * 
     * QNTY = this.convertByUomFromAndItemAndQuantity(
     * invPropagatedCosting.getArInvoiceLineItem().getInvUnitOfMeasure(),
     * invPropagatedCosting.getArInvoiceLineItem().getInvItemLocation().getInvItem(),
     * invPropagatedCosting.getArInvoiceLineItem().getIliQuantity(), AD_CMPNY); TTL_CST =
     * prevInvCosting.getCstRemainingValue() - invPropagatedCosting.getCstRemainingValue();
     * 
     * if(invPropagatedCosting.getArInvoiceLineItem().getArInvoice() != null){
     * 
     * ADJ_DSCRPTN = invPropagatedCosting.getArInvoiceLineItem().getArInvoice().getInvDescription();
     * ADJ_CRTD_BY = invPropagatedCosting.getArInvoiceLineItem().getArInvoice().getInvPostedBy();
     * ADJ_RFRNC_NMBR = "ARCM" +
     * invPropagatedCosting.getArInvoiceLineItem().getArInvoice().getInvNumber();
     * 
     * } else if(invPropagatedCosting.getArInvoiceLineItem().getArReceipt() != null){
     * 
     * ADJ_DSCRPTN = invPropagatedCosting.getArInvoiceLineItem().getArReceipt().getRctDescription();
     * ADJ_CRTD_BY = invPropagatedCosting.getArInvoiceLineItem().getArReceipt().getRctPostedBy();
     * ADJ_RFRNC_NMBR = "ARMR" +
     * invPropagatedCosting.getArInvoiceLineItem().getArReceipt().getRctNumber();
     * 
     * }
     * 
     * } else if (invPropagatedCosting.getArSalesOrderInvoiceLine() != null){
     * 
     * TTL_CST = prevInvCosting.getCstRemainingValue() -
     * invPropagatedCosting.getCstRemainingValue(); QNTY = this.convertByUomFromAndItemAndQuantity(
     * invPropagatedCosting.getArSalesOrderInvoiceLine().getArSalesOrderLine().getInvUnitOfMeasure()
     * ,
     * invPropagatedCosting.getArSalesOrderInvoiceLine().getArSalesOrderLine().getInvItemLocation().
     * getInvItem(), invPropagatedCosting.getArSalesOrderInvoiceLine().getSilQuantityDelivered(),
     * AD_CMPNY);
     * 
     * ADJ_DSCRPTN =
     * invPropagatedCosting.getArSalesOrderInvoiceLine().getArInvoice().getInvDescription();
     * ADJ_CRTD_BY =
     * invPropagatedCosting.getArSalesOrderInvoiceLine().getArInvoice().getInvPostedBy();
     * ADJ_RFRNC_NMBR = "ARCM" +
     * invPropagatedCosting.getArSalesOrderInvoiceLine().getArInvoice().getInvNumber();
     * 
     * } else if (invPropagatedCosting.getInvAdjustmentLine() != null){
     * 
     * ADJ_DSCRPTN =
     * invPropagatedCosting.getInvAdjustmentLine().getInvAdjustment().getAdjDescription();
     * ADJ_CRTD_BY =
     * invPropagatedCosting.getInvAdjustmentLine().getInvAdjustment().getAdjPostedBy();
     * ADJ_RFRNC_NMBR = "INVADJ" +
     * invPropagatedCosting.getInvAdjustmentLine().getInvAdjustment().getAdjDocumentNumber();
     * 
     * if(invPropagatedCosting.getInvAdjustmentLine().getAlAdjustQuantity() != 0) {
     * 
     * TTL_CST = (invPropagatedCosting.getInvAdjustmentLine().getAlUnitCost() *
     * invPropagatedCosting.getInvAdjustmentLine().getAlAdjustQuantity()); QNTY =
     * this.convertByUomFromAndItemAndQuantity(
     * invPropagatedCosting.getInvAdjustmentLine().getInvUnitOfMeasure(),
     * invPropagatedCosting.getInvAdjustmentLine().getInvItemLocation().getInvItem(),
     * invPropagatedCosting.getInvAdjustmentLine().getAlAdjustQuantity(), AD_CMPNY);
     * 
     * }
     * 
     * } else if (invPropagatedCosting.getInvAssemblyTransferLine() != null){
     * 
     * TTL_CST = invPropagatedCosting.getInvAssemblyTransferLine().getAtlAssembleCost(); QNTY =
     * invPropagatedCosting.getInvAssemblyTransferLine().getAtlAssembleQuantity(); ADJ_DSCRPTN =
     * invPropagatedCosting.getInvAssemblyTransferLine().getInvAssemblyTransfer().getAtrDescription(
     * ); ADJ_CRTD_BY =
     * invPropagatedCosting.getInvAssemblyTransferLine().getInvAssemblyTransfer().getAtrPostedBy();
     * ADJ_RFRNC_NMBR = "INVAT" +
     * invPropagatedCosting.getInvAssemblyTransferLine().getInvAssemblyTransfer().
     * getAtrDocumentNumber();
     * 
     * } else if (invPropagatedCosting.getInvBranchStockTransferLine() != null){
     * 
     * if(invPropagatedCosting.getInvBranchStockTransferLine().getInvBranchStockTransfer().
     * getBstTransferOutNumber() != null) {
     * 
     * TTL_CST = invPropagatedCosting.getInvBranchStockTransferLine().getBslAmount(); QNTY =
     * this.convertByUomFromAndItemAndQuantity(
     * invPropagatedCosting.getInvBranchStockTransferLine().getInvUnitOfMeasure(),
     * invPropagatedCosting.getInvBranchStockTransferLine().getInvItemLocation().getInvItem(),
     * invPropagatedCosting.getInvBranchStockTransferLine().getBslQuantityReceived(), AD_CMPNY);
     * 
     * } else {
     * 
     * TTL_CST = invPropagatedCosting.getInvBranchStockTransferLine().getBslAmount(); QNTY =
     * this.convertByUomFromAndItemAndQuantity(
     * invPropagatedCosting.getInvBranchStockTransferLine().getInvUnitOfMeasure(),
     * invPropagatedCosting.getInvBranchStockTransferLine().getInvItemLocation().getInvItem(),
     * invPropagatedCosting.getInvBranchStockTransferLine().getBslQuantity(), AD_CMPNY); }
     * 
     * ADJ_DSCRPTN =
     * invPropagatedCosting.getInvBranchStockTransferLine().getInvBranchStockTransfer().
     * getBstDescription(); ADJ_CRTD_BY =
     * invPropagatedCosting.getInvBranchStockTransferLine().getInvBranchStockTransfer().
     * getBstPostedBy(); ADJ_RFRNC_NMBR = "INVBST" +
     * invPropagatedCosting.getInvBranchStockTransferLine().getInvBranchStockTransfer().getBstNumber
     * ();
     * 
     * } else if (invPropagatedCosting.getInvBuildUnbuildAssemblyLine() != null){
     * 
     * TTL_CST = prevInvCosting.getCstRemainingValue() -
     * invPropagatedCosting.getCstRemainingValue(); QNTY =
     * invPropagatedCosting.getInvBuildUnbuildAssemblyLine().getBlBuildQuantity(); ADJ_DSCRPTN =
     * invPropagatedCosting.getInvBuildUnbuildAssemblyLine().getInvBuildUnbuildAssembly().
     * getBuaDescription(); ADJ_CRTD_BY =
     * invPropagatedCosting.getInvBuildUnbuildAssemblyLine().getInvBuildUnbuildAssembly().
     * getBuaPostedBy(); ADJ_RFRNC_NMBR = "INVBUA" +
     * invPropagatedCosting.getInvBuildUnbuildAssemblyLine().getInvBuildUnbuildAssembly().
     * getBuaDocumentNumber();
     * 
     * } else if (invPropagatedCosting.getInvStockIssuanceLine()!= null){
     * 
     * TTL_CST = invPropagatedCosting.getInvStockIssuanceLine().getSilIssueCost(); QNTY =
     * this.convertByUomFromAndItemAndQuantity(
     * invPropagatedCosting.getInvStockIssuanceLine().getInvUnitOfMeasure(),
     * invPropagatedCosting.getInvStockIssuanceLine().getInvItemLocation().getInvItem(),
     * invPropagatedCosting.getInvStockIssuanceLine().getSilIssueQuantity(), AD_CMPNY); ADJ_DSCRPTN
     * = invPropagatedCosting.getInvStockIssuanceLine().getInvStockIssuance().getSiDescription();
     * ADJ_CRTD_BY =
     * invPropagatedCosting.getInvStockIssuanceLine().getInvStockIssuance().getSiPostedBy();
     * ADJ_RFRNC_NMBR = "INVSI" +
     * invPropagatedCosting.getInvStockIssuanceLine().getInvStockIssuance().getSiDocumentNumber();
     * 
     * } else if (invPropagatedCosting.getInvStockTransferLine()!= null) {
     * 
     * TTL_CST = invPropagatedCosting.getInvStockTransferLine().getStlAmount(); QNTY =
     * this.convertByUomFromAndItemAndQuantity(
     * invPropagatedCosting.getInvStockTransferLine().getInvUnitOfMeasure(),
     * invPropagatedCosting.getInvStockTransferLine().getInvItem(),
     * invPropagatedCosting.getInvStockTransferLine().getStlQuantityDelivered(), AD_CMPNY);
     * ADJ_DSCRPTN =
     * invPropagatedCosting.getInvStockTransferLine().getInvStockTransfer().getStDescription();
     * ADJ_CRTD_BY =
     * invPropagatedCosting.getInvStockTransferLine().getInvStockTransfer().getStPostedBy();
     * ADJ_RFRNC_NMBR = "INVST" +
     * invPropagatedCosting.getInvStockTransferLine().getInvStockTransfer().getStDocumentNumber();
     * 
     * } else {
     * 
     * prevInvCosting = invPropagatedCosting; continue;
     * 
     * }
     * 
     * // if quantity is equal 0, no variance. if(QNTY == 0) continue;
     * 
     * // compute new cost variance double UNT_CST = TTL_CST/QNTY; double CST_VRNC_VL =
     * (invPropagatedCosting.getCstRemainingQuantity() * UNT_CST -
     * invPropagatedCosting.getCstRemainingValue());
     * 
     * if(CST_VRNC_VL != 0) this.generateCostVariance(invPropagatedCosting.getInvItemLocation(),
     * CST_VRNC_VL, ADJ_RFRNC_NMBR, ADJ_DSCRPTN, invPropagatedCosting.getCstDate(), ADJ_CRTD_BY,
     * AD_BRNCH, AD_CMPNY);
     * 
     * }
     * 
     * // set previous costing prevInvCosting = invPropagatedCosting;
     * 
     * }
     * 
     * } catch (AdPRFCoaGlVarianceAccountNotFoundException ex){
     * 
     * throw ex;
     * 
     * } catch (Exception ex) {
     * 
     * Debug.printStackTrace(ex); getSessionContext().setRollbackOnly(); throw new
     * EJBException(ex.getMessage());
     * 
     * }
     */
  }

  private void addInvDrEntry(
      short DR_LN, String DR_CLSS, byte DR_DBT, double DR_AMNT, byte DR_RVRSL, Integer COA_CODE,
      LocalInvAdjustment invAdjustment, Integer AD_BRNCH, Integer AD_CMPNY
  )

      throws GlobalBranchAccountNumberInvalidException {

    Debug.print("ArInvoiceBatchSubmitControllerBean addInvDrEntry");

    LocalAdCompanyHome adCompanyHome = null;
    LocalInvDistributionRecordHome invDistributionRecordHome = null;
    LocalGlChartOfAccountHome glChartOfAccountHome = null;

    // Initialize EJB Home

    try {

      adCompanyHome = (LocalAdCompanyHome) EJBHomeFactory
          .lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
      invDistributionRecordHome = (LocalInvDistributionRecordHome) EJBHomeFactory.lookUpLocalHome(
          LocalInvDistributionRecordHome.JNDI_NAME, LocalInvDistributionRecordHome.class);
      glChartOfAccountHome = (LocalGlChartOfAccountHome) EJBHomeFactory
          .lookUpLocalHome(LocalGlChartOfAccountHome.JNDI_NAME, LocalGlChartOfAccountHome.class);


    } catch (NamingException ex) {

      throw new EJBException(ex.getMessage());

    }

    try {

      // get company

      LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);

      // validate coa

      LocalGlChartOfAccount glChartOfAccount = null;

      try {

        glChartOfAccount =
            glChartOfAccountHome.findByCoaCodeAndBranchCode(COA_CODE, AD_BRNCH, AD_CMPNY);

      } catch (FinderException ex) {

        throw new GlobalBranchAccountNumberInvalidException();

      }

      // create distribution record

      LocalInvDistributionRecord invDistributionRecord =
          invDistributionRecordHome.create(DR_LN, DR_CLSS, DR_DBT,
              EJBCommon.roundIt(DR_AMNT, adCompany.getGlFunctionalCurrency().getFcPrecision()),
              DR_RVRSL, EJBCommon.FALSE, AD_CMPNY);

      // invAdjustment.addInvDistributionRecord(invDistributionRecord);
      invDistributionRecord.setInvAdjustment(invAdjustment);

      // glChartOfAccount.addInvDistributionRecord(invDistributionRecord);

      invDistributionRecord.setInvChartOfAccount(glChartOfAccount);

    } catch (GlobalBranchAccountNumberInvalidException ex) {

      throw new GlobalBranchAccountNumberInvalidException();

    } catch (Exception ex) {

      Debug.printStackTrace(ex);
      getSessionContext().setRollbackOnly();
      throw new EJBException(ex.getMessage());

    }

  }

  private void executeInvAdjPost(
      Integer ADJ_CODE, String USR_NM, Integer AD_BRNCH, Integer AD_CMPNY
  ) throws GlobalRecordAlreadyDeletedException, GlobalTransactionAlreadyPostedException,
      GlJREffectiveDateNoPeriodExistException, GlJREffectiveDatePeriodClosedException,
      GlobalJournalNotBalanceException, GlobalBranchAccountNumberInvalidException {

    Debug.print("ArInvoiceBatchSubmitControllerBean executeInvAdjPost");

    LocalInvAdjustmentHome invAdjustmentHome = null;
    LocalAdCompanyHome adCompanyHome = null;
    LocalAdPreferenceHome adPreferenceHome = null;
    LocalGlSetOfBookHome glSetOfBookHome = null;
    LocalGlAccountingCalendarValueHome glAccountingCalendarValueHome = null;
    LocalGlJournalHome glJournalHome = null;
    LocalGlJournalBatchHome glJournalBatchHome = null;
    LocalGlSuspenseAccountHome glSuspenseAccountHome = null;
    LocalGlJournalLineHome glJournalLineHome = null;
    LocalGlJournalSourceHome glJournalSourceHome = null;
    LocalGlJournalCategoryHome glJournalCategoryHome = null;
    LocalGlFunctionalCurrencyHome glFunctionalCurrencyHome = null;
    LocalInvDistributionRecordHome invDistributionRecordHome = null;
    LocalInvCostingHome invCostingHome = null;
    LocalGlChartOfAccountHome glChartOfAccountHome = null;
    LocalInvAdjustmentLineHome invAdjustmentLineHome = null;


    // Initialize EJB Home

    try {

      invAdjustmentHome = (LocalInvAdjustmentHome) EJBHomeFactory
          .lookUpLocalHome(LocalInvAdjustmentHome.JNDI_NAME, LocalInvAdjustmentHome.class);
      adCompanyHome = (LocalAdCompanyHome) EJBHomeFactory
          .lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
      adPreferenceHome = (LocalAdPreferenceHome) EJBHomeFactory
          .lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);
      glSetOfBookHome = (LocalGlSetOfBookHome) EJBHomeFactory
          .lookUpLocalHome(LocalGlSetOfBookHome.JNDI_NAME, LocalGlSetOfBookHome.class);
      glAccountingCalendarValueHome = (LocalGlAccountingCalendarValueHome) EJBHomeFactory
          .lookUpLocalHome(LocalGlAccountingCalendarValueHome.JNDI_NAME,
              LocalGlAccountingCalendarValueHome.class);
      glJournalHome = (LocalGlJournalHome) EJBHomeFactory
          .lookUpLocalHome(LocalGlJournalHome.JNDI_NAME, LocalGlJournalHome.class);
      glJournalBatchHome = (LocalGlJournalBatchHome) EJBHomeFactory
          .lookUpLocalHome(LocalGlJournalBatchHome.JNDI_NAME, LocalGlJournalBatchHome.class);
      glSuspenseAccountHome = (LocalGlSuspenseAccountHome) EJBHomeFactory
          .lookUpLocalHome(LocalGlSuspenseAccountHome.JNDI_NAME, LocalGlSuspenseAccountHome.class);
      glJournalLineHome = (LocalGlJournalLineHome) EJBHomeFactory
          .lookUpLocalHome(LocalGlJournalLineHome.JNDI_NAME, LocalGlJournalLineHome.class);
      glJournalSourceHome = (LocalGlJournalSourceHome) EJBHomeFactory
          .lookUpLocalHome(LocalGlJournalSourceHome.JNDI_NAME, LocalGlJournalSourceHome.class);
      glJournalCategoryHome = (LocalGlJournalCategoryHome) EJBHomeFactory
          .lookUpLocalHome(LocalGlJournalCategoryHome.JNDI_NAME, LocalGlJournalCategoryHome.class);
      glFunctionalCurrencyHome = (LocalGlFunctionalCurrencyHome) EJBHomeFactory.lookUpLocalHome(
          LocalGlFunctionalCurrencyHome.JNDI_NAME, LocalGlFunctionalCurrencyHome.class);
      invDistributionRecordHome = (LocalInvDistributionRecordHome) EJBHomeFactory.lookUpLocalHome(
          LocalInvDistributionRecordHome.JNDI_NAME, LocalInvDistributionRecordHome.class);
      invCostingHome = (LocalInvCostingHome) EJBHomeFactory
          .lookUpLocalHome(LocalInvCostingHome.JNDI_NAME, LocalInvCostingHome.class);
      glChartOfAccountHome = (LocalGlChartOfAccountHome) EJBHomeFactory
          .lookUpLocalHome(LocalGlChartOfAccountHome.JNDI_NAME, LocalGlChartOfAccountHome.class);
      invAdjustmentLineHome = (LocalInvAdjustmentLineHome) EJBHomeFactory
          .lookUpLocalHome(LocalInvAdjustmentLineHome.JNDI_NAME, LocalInvAdjustmentLineHome.class);


    } catch (NamingException ex) {

      throw new EJBException(ex.getMessage());

    }

    try {

      // validate if adjustment is already deleted

      LocalInvAdjustment invAdjustment = null;

      try {

        invAdjustment = invAdjustmentHome.findByPrimaryKey(ADJ_CODE);

      } catch (FinderException ex) {

        throw new GlobalRecordAlreadyDeletedException();

      }

      // validate if adjustment is already posted or void

      if (invAdjustment.getAdjPosted() == EJBCommon.TRUE) {

        if (invAdjustment.getAdjVoid() != EJBCommon.TRUE)
          throw new GlobalTransactionAlreadyPostedException();

      }

      Collection invAdjustmentLines = null;

      if (invAdjustment.getAdjVoid() == EJBCommon.FALSE)
        invAdjustmentLines = invAdjustmentLineHome.findByAlVoidAndAdjCode(EJBCommon.FALSE,
            invAdjustment.getAdjCode(), AD_CMPNY);
      else
        invAdjustmentLines = invAdjustmentLineHome.findByAlVoidAndAdjCode(EJBCommon.TRUE,
            invAdjustment.getAdjCode(), AD_CMPNY);


      Iterator i = invAdjustmentLines.iterator();

      while (i.hasNext()) {


        LocalInvAdjustmentLine invAdjustmentLine = (LocalInvAdjustmentLine) i.next();

        LocalInvCosting invCosting = invCostingHome
            .getByMaxCstDateToLongAndMaxCstLineNumberAndLessThanEqualCstDateAndIiNameAndLocName(
                invAdjustment.getAdjDate(),
                invAdjustmentLine.getInvItemLocation().getInvItem().getIiName(),
                invAdjustmentLine.getInvItemLocation().getInvLocation().getLocName(), AD_BRNCH,
                AD_CMPNY);


        this.postInvAdjustmentToInventory(invAdjustmentLine, invAdjustment.getAdjDate(), 0,
            invAdjustmentLine.getAlUnitCost(), invCosting.getCstRemainingQuantity(),
            invCosting.getCstRemainingValue() + invAdjustmentLine.getAlUnitCost(), AD_BRNCH,
            AD_CMPNY);

      }

      // post to gl if necessary

      LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);
      LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);

      // validate if date has no period and period is closed

      LocalGlSetOfBook glJournalSetOfBook = null;

      try {

        glJournalSetOfBook = glSetOfBookHome.findByDate(invAdjustment.getAdjDate(), AD_CMPNY);

      } catch (FinderException ex) {

        throw new GlJREffectiveDateNoPeriodExistException();

      }

      LocalGlAccountingCalendarValue glAccountingCalendarValue = glAccountingCalendarValueHome
          .findByAcCodeAndDate(glJournalSetOfBook.getGlAccountingCalendar().getAcCode(),
              invAdjustment.getAdjDate(), AD_CMPNY);


      if (glAccountingCalendarValue.getAcvStatus() == 'N'
          || glAccountingCalendarValue.getAcvStatus() == 'C'
          || glAccountingCalendarValue.getAcvStatus() == 'P') {

        throw new GlJREffectiveDatePeriodClosedException();

      }

      // check if invoice is balance if not check suspense posting

      LocalGlJournalLine glOffsetJournalLine = null;

      Collection invDistributionRecords = null;

      if (invAdjustment.getAdjVoid() == EJBCommon.FALSE) {

        invDistributionRecords = invDistributionRecordHome.findImportableDrByDrReversedAndAdjCode(
            EJBCommon.FALSE, invAdjustment.getAdjCode(), AD_CMPNY);

      } else {

        invDistributionRecords = invDistributionRecordHome.findImportableDrByDrReversedAndAdjCode(
            EJBCommon.TRUE, invAdjustment.getAdjCode(), AD_CMPNY);

      }


      Iterator j = invDistributionRecords.iterator();

      double TOTAL_DEBIT = 0d;
      double TOTAL_CREDIT = 0d;

      while (j.hasNext()) {

        LocalInvDistributionRecord invDistributionRecord = (LocalInvDistributionRecord) j.next();

        double DR_AMNT = 0d;

        DR_AMNT = invDistributionRecord.getDrAmount();

        if (invDistributionRecord.getDrDebit() == EJBCommon.TRUE) {

          TOTAL_DEBIT += DR_AMNT;

        } else {

          TOTAL_CREDIT += DR_AMNT;

        }

      }

      TOTAL_DEBIT =
          EJBCommon.roundIt(TOTAL_DEBIT, adCompany.getGlFunctionalCurrency().getFcPrecision());
      TOTAL_CREDIT =
          EJBCommon.roundIt(TOTAL_CREDIT, adCompany.getGlFunctionalCurrency().getFcPrecision());

      if (adPreference.getPrfAllowSuspensePosting() == EJBCommon.TRUE
          && TOTAL_DEBIT != TOTAL_CREDIT) {

        LocalGlSuspenseAccount glSuspenseAccount = null;

        try {

          glSuspenseAccount = glSuspenseAccountHome.findByJsNameAndJcName("INVENTORY",
              "INVENTORY ADJUSTMENTS", AD_CMPNY);

        } catch (FinderException ex) {

          throw new GlobalJournalNotBalanceException();

        }

        if (TOTAL_DEBIT - TOTAL_CREDIT < 0) {

          glOffsetJournalLine =
              glJournalLineHome.create((short) (invDistributionRecords.size() + 1), EJBCommon.TRUE,
                  TOTAL_CREDIT - TOTAL_DEBIT, "", AD_CMPNY);

        } else {

          glOffsetJournalLine =
              glJournalLineHome.create((short) (invDistributionRecords.size() + 1), EJBCommon.FALSE,
                  TOTAL_DEBIT - TOTAL_CREDIT, "", AD_CMPNY);

        }

        LocalGlChartOfAccount glChartOfAccount = glSuspenseAccount.getGlChartOfAccount();
        // glChartOfAccount.addGlJournalLine(glOffsetJournalLine);

        glOffsetJournalLine.setGlChartOfAccount(glChartOfAccount);
      } else if (adPreference.getPrfAllowSuspensePosting() == EJBCommon.FALSE
          && TOTAL_DEBIT != TOTAL_CREDIT) {

        throw new GlobalJournalNotBalanceException();

      }

      // create journal batch if necessary

      LocalGlJournalBatch glJournalBatch = null;
      java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat("MM/dd/yyyy");

      try {

        glJournalBatch = glJournalBatchHome.findByJbName(
            "JOURNAL IMPORT " + formatter.format(new Date()) + " INVENTORY ADJUSTMENTS", AD_BRNCH,
            AD_CMPNY);

      } catch (FinderException ex) {

      }

      if (glJournalBatch == null) {

        glJournalBatch = glJournalBatchHome.create(
            "JOURNAL IMPORT " + formatter.format(new Date()) + " INVENTORY ADJUSTMENTS",
            "JOURNAL IMPORT", "CLOSED", EJBCommon.getGcCurrentDateWoTime().getTime(), USR_NM,
            AD_BRNCH, AD_CMPNY);

      }

      // create journal entry

      LocalGlJournal glJournal = glJournalHome.create(invAdjustment.getAdjReferenceNumber(),
          invAdjustment.getAdjDescription(), invAdjustment.getAdjDate(), 0.0d, null,
          invAdjustment.getAdjDocumentNumber(), null, 1d, "N/A", null, 'N', EJBCommon.TRUE,
          EJBCommon.FALSE, USR_NM, new Date(), USR_NM, new Date(), null, null, USR_NM,
          EJBCommon.getGcCurrentDateWoTime().getTime(), null, null, EJBCommon.FALSE, null, AD_BRNCH,
          AD_CMPNY);

      LocalGlJournalSource glJournalSource =
          glJournalSourceHome.findByJsName("INVENTORY", AD_CMPNY);
      glJournal.setGlJournalSource(glJournalSource);

      LocalGlFunctionalCurrency glFunctionalCurrency = glFunctionalCurrencyHome
          .findByFcName(adCompany.getGlFunctionalCurrency().getFcName(), AD_CMPNY);
      glJournal.setGlFunctionalCurrency(glFunctionalCurrency);

      LocalGlJournalCategory glJournalCategory =
          glJournalCategoryHome.findByJcName("INVENTORY ADJUSTMENTS", AD_CMPNY);
      glJournal.setGlJournalCategory(glJournalCategory);

      if (glJournalBatch != null) {

        glJournal.setGlJournalBatch(glJournalBatch);

      }

      // create journal lines

      j = invDistributionRecords.iterator();

      while (j.hasNext()) {

        LocalInvDistributionRecord invDistributionRecord = (LocalInvDistributionRecord) j.next();

        double DR_AMNT = 0d;

        DR_AMNT = invDistributionRecord.getDrAmount();

        LocalGlJournalLine glJournalLine =
            glJournalLineHome.create(invDistributionRecord.getDrLine(),
                invDistributionRecord.getDrDebit(), DR_AMNT, "", AD_CMPNY);

        glJournalLine.setGlChartOfAccount(invDistributionRecord.getInvChartOfAccount());
        glJournalLine.setGlJournal(glJournal);

        invDistributionRecord.setDrImported(EJBCommon.TRUE);


      }

      if (glOffsetJournalLine != null) {

        glOffsetJournalLine.setGlJournal(glJournal);

      }

      // post journal to gl

      Collection glJournalLines = glJournal.getGlJournalLines();

      i = glJournalLines.iterator();

      while (i.hasNext()) {

        LocalGlJournalLine glJournalLine = (LocalGlJournalLine) i.next();

        // post current to current acv

        this.postToGl(glAccountingCalendarValue, glJournalLine.getGlChartOfAccount(), true,
            glJournalLine.getJlDebit(), glJournalLine.getJlAmount(), AD_CMPNY);


        // post to subsequent acvs (propagate)

        Collection glSubsequentAccountingCalendarValues =
            glAccountingCalendarValueHome.findSubsequentAcvByAcCodeAndAcvPeriodNumber(
                glJournalSetOfBook.getGlAccountingCalendar().getAcCode(),
                glAccountingCalendarValue.getAcvPeriodNumber(), AD_CMPNY);

        Iterator acvsIter = glSubsequentAccountingCalendarValues.iterator();

        while (acvsIter.hasNext()) {

          LocalGlAccountingCalendarValue glSubsequentAccountingCalendarValue =
              (LocalGlAccountingCalendarValue) acvsIter.next();

          this.postToGl(glSubsequentAccountingCalendarValue, glJournalLine.getGlChartOfAccount(),
              false, glJournalLine.getJlDebit(), glJournalLine.getJlAmount(), AD_CMPNY);

        }

        // post to subsequent years if necessary

        Collection glSubsequentSetOfBooks = glSetOfBookHome.findSubsequentSobByAcYear(
            glJournalSetOfBook.getGlAccountingCalendar().getAcYear(), AD_CMPNY);

        if (!glSubsequentSetOfBooks.isEmpty() && glJournalSetOfBook.getSobYearEndClosed() == 1) {

          adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
          LocalGlChartOfAccount glRetainedEarningsAccount =
              glChartOfAccountHome.findByCoaAccountNumberAndBranchCode(
                  adCompany.getCmpRetainedEarnings(), AD_BRNCH, AD_CMPNY);

          Iterator sobIter = glSubsequentSetOfBooks.iterator();

          while (sobIter.hasNext()) {

            LocalGlSetOfBook glSubsequentSetOfBook = (LocalGlSetOfBook) sobIter.next();

            String ACCOUNT_TYPE = glJournalLine.getGlChartOfAccount().getCoaAccountType();

            // post to subsequent acvs of subsequent set of book(propagate)

            Collection glAccountingCalendarValues = glAccountingCalendarValueHome.findByAcCode(
                glSubsequentSetOfBook.getGlAccountingCalendar().getAcCode(), AD_CMPNY);

            Iterator acvIter = glAccountingCalendarValues.iterator();

            while (acvIter.hasNext()) {

              LocalGlAccountingCalendarValue glSubsequentAccountingCalendarValue =
                  (LocalGlAccountingCalendarValue) acvIter.next();

              if (ACCOUNT_TYPE.equals("ASSET") || ACCOUNT_TYPE.equals("LIABILITY")
                  || ACCOUNT_TYPE.equals("OWNERS EQUITY")) {

                this.postToGl(glSubsequentAccountingCalendarValue,
                    glJournalLine.getGlChartOfAccount(), false, glJournalLine.getJlDebit(),
                    glJournalLine.getJlAmount(), AD_CMPNY);

              } else { // revenue & expense

                this.postToGl(glSubsequentAccountingCalendarValue, glRetainedEarningsAccount, false,
                    glJournalLine.getJlDebit(), glJournalLine.getJlAmount(), AD_CMPNY);

              }

            }

            if (glSubsequentSetOfBook.getSobYearEndClosed() == 0)
              break;

          }

        }

      }

      invAdjustment.setAdjPosted(EJBCommon.TRUE);

    } catch (GlJREffectiveDateNoPeriodExistException ex) {

      getSessionContext().setRollbackOnly();
      throw ex;

    } catch (GlJREffectiveDatePeriodClosedException ex) {

      getSessionContext().setRollbackOnly();
      throw ex;

    } catch (GlobalJournalNotBalanceException ex) {

      getSessionContext().setRollbackOnly();
      throw ex;

    } catch (GlobalRecordAlreadyDeletedException ex) {

      getSessionContext().setRollbackOnly();
      throw ex;

    } catch (GlobalTransactionAlreadyPostedException ex) {

      getSessionContext().setRollbackOnly();
      throw ex;

    } catch (Exception ex) {

      Debug.printStackTrace(ex);
      getSessionContext().setRollbackOnly();
      throw new EJBException(ex.getMessage());

    }

  }

  private LocalInvAdjustmentLine addInvAlEntry(
      LocalInvItemLocation invItemLocation, LocalInvAdjustment invAdjustment, double CST_VRNC_VL,
      byte AL_VD, Integer AD_CMPNY
  ) {

    Debug.print("ArInvoiceBatchSubmitControllerBean addInvAlEntry");

    LocalInvAdjustmentLineHome invAdjustmentLineHome = null;

    // Initialize EJB Home

    try {

      invAdjustmentLineHome = (LocalInvAdjustmentLineHome) EJBHomeFactory
          .lookUpLocalHome(LocalInvAdjustmentLineHome.JNDI_NAME, LocalInvAdjustmentLineHome.class);

    } catch (NamingException ex) {

      throw new EJBException(ex.getMessage());

    }

    try {

      // create dr entry
      LocalInvAdjustmentLine invAdjustmentLine = null;
      invAdjustmentLine =
          invAdjustmentLineHome.create(CST_VRNC_VL, null, null, 0, 0, AL_VD, AD_CMPNY);

      // map adjustment, unit of measure, item location
      // invAdjustment.addInvAdjustmentLine(invAdjustmentLine);

      invAdjustmentLine.setInvAdjustment(invAdjustment);
      // invItemLocation.getInvItem().getInvUnitOfMeasure().addInvAdjustmentLine(invAdjustmentLine);

      invAdjustmentLine.setInvUnitOfMeasure(invItemLocation.getInvItem().getInvUnitOfMeasure());
      // invItemLocation.addInvAdjustmentLine(invAdjustmentLine);

      invAdjustmentLine.setInvItemLocation(invItemLocation);

      return invAdjustmentLine;

    } catch (Exception ex) {

      Debug.printStackTrace(ex);
      getSessionContext().setRollbackOnly();
      throw new EJBException(ex.getMessage());

    }

  }

  private LocalInvAdjustment saveInvAdjustment(
      String ADJ_RFRNC_NMBR, String ADJ_DSCRPTN, Date ADJ_DATE, String USR_NM, Integer AD_BRNCH,
      Integer AD_CMPNY
  ) {

    Debug.print("ArInvoiceBatchSubmitControllerBean saveInvAdjustment");

    LocalInvAdjustmentHome invAdjustmentHome = null;
    LocalAdDocumentSequenceAssignmentHome adDocumentSequenceAssignmentHome = null;
    LocalAdBranchDocumentSequenceAssignmentHome adBranchDocumentSequenceAssignmentHome = null;
    // Initialize EJB Home

    try {

      invAdjustmentHome = (LocalInvAdjustmentHome) EJBHomeFactory
          .lookUpLocalHome(LocalInvAdjustmentHome.JNDI_NAME, LocalInvAdjustmentHome.class);
      adDocumentSequenceAssignmentHome = (LocalAdDocumentSequenceAssignmentHome) EJBHomeFactory
          .lookUpLocalHome(LocalAdDocumentSequenceAssignmentHome.JNDI_NAME,
              LocalAdDocumentSequenceAssignmentHome.class);
      adBranchDocumentSequenceAssignmentHome =
          (LocalAdBranchDocumentSequenceAssignmentHome) EJBHomeFactory.lookUpLocalHome(
              LocalAdBranchDocumentSequenceAssignmentHome.JNDI_NAME,
              LocalAdBranchDocumentSequenceAssignmentHome.class);

    } catch (NamingException ex) {

      throw new EJBException(ex.getMessage());

    }

    try {

      // generate adj document number
      String ADJ_DCMNT_NMBR = null;

      LocalAdBranchDocumentSequenceAssignment adBranchDocumentSequenceAssignment = null;
      LocalAdDocumentSequenceAssignment adDocumentSequenceAssignment = null;

      try {

        adDocumentSequenceAssignment =
            adDocumentSequenceAssignmentHome.findByDcName("INV ADJUSTMENT", AD_CMPNY);

      } catch (FinderException ex) {

      }

      try {

        adBranchDocumentSequenceAssignment =
            adBranchDocumentSequenceAssignmentHome.findBdsByDsaCodeAndBrCode(
                adDocumentSequenceAssignment.getDsaCode(), AD_BRNCH, AD_CMPNY);

      } catch (FinderException ex) {

      }

      while (true) {

        if (adBranchDocumentSequenceAssignment == null
            || adBranchDocumentSequenceAssignment.getBdsNextSequence() == null) {

          try {

            invAdjustmentHome.findByAdjDocumentNumberAndBrCode(
                adDocumentSequenceAssignment.getDsaNextSequence(), AD_BRNCH, AD_CMPNY);
            adDocumentSequenceAssignment.setDsaNextSequence(
                EJBCommon.incrementStringNumber(adDocumentSequenceAssignment.getDsaNextSequence()));

          } catch (FinderException ex) {

            ADJ_DCMNT_NMBR = adDocumentSequenceAssignment.getDsaNextSequence();
            adDocumentSequenceAssignment.setDsaNextSequence(
                EJBCommon.incrementStringNumber(adDocumentSequenceAssignment.getDsaNextSequence()));
            break;

          }

        } else {

          try {

            invAdjustmentHome.findByAdjDocumentNumberAndBrCode(
                adBranchDocumentSequenceAssignment.getBdsNextSequence(), AD_BRNCH, AD_CMPNY);
            adBranchDocumentSequenceAssignment.setBdsNextSequence(EJBCommon
                .incrementStringNumber(adBranchDocumentSequenceAssignment.getBdsNextSequence()));

          } catch (FinderException ex) {

            ADJ_DCMNT_NMBR = adBranchDocumentSequenceAssignment.getBdsNextSequence();
            adBranchDocumentSequenceAssignment.setBdsNextSequence(EJBCommon
                .incrementStringNumber(adBranchDocumentSequenceAssignment.getBdsNextSequence()));
            break;

          }

        }

      }

      LocalInvAdjustment invAdjustment = invAdjustmentHome.create(ADJ_DCMNT_NMBR, ADJ_RFRNC_NMBR,
          ADJ_DSCRPTN, ADJ_DATE, "COST-VARIANCE", "N/A", EJBCommon.FALSE, USR_NM, ADJ_DATE, USR_NM,
          ADJ_DATE, null, null, USR_NM, ADJ_DATE, null, null, EJBCommon.TRUE, EJBCommon.FALSE,
          AD_BRNCH, AD_CMPNY);

      return invAdjustment;

    } catch (Exception ex) {

      Debug.printStackTrace(ex);
      getSessionContext().setRollbackOnly();
      throw new EJBException(ex.getMessage());

    }


  }

  private void postInvAdjustmentToInventory(
      LocalInvAdjustmentLine invAdjustmentLine, Date CST_DT, double CST_ADJST_QTY,
      double CST_ADJST_CST, double CST_RMNNG_QTY, double CST_RMNNG_VL, Integer AD_BRNCH,
      Integer AD_CMPNY
  ) {

    Debug.print("ArInvoiceBatchSubmitControllerBean postInvAdjustmentToInventory");

    LocalInvCostingHome invCostingHome = null;
    LocalAdPreferenceHome adPreferenceHome = null;
    LocalAdCompanyHome adCompanyHome = null;

    // Initialize EJB Home

    try {

      invCostingHome = (LocalInvCostingHome) EJBHomeFactory
          .lookUpLocalHome(LocalInvCostingHome.JNDI_NAME, LocalInvCostingHome.class);
      adPreferenceHome = (LocalAdPreferenceHome) EJBHomeFactory
          .lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);
      adCompanyHome = (LocalAdCompanyHome) EJBHomeFactory
          .lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);

    } catch (NamingException ex) {

      getSessionContext().setRollbackOnly();
      throw new EJBException(ex.getMessage());

    }

    try {

      LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);
      LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
      LocalInvItemLocation invItemLocation = invAdjustmentLine.getInvItemLocation();
      int CST_LN_NMBR = 0;

      CST_ADJST_QTY =
          EJBCommon.roundIt(CST_ADJST_QTY, adPreference.getPrfInvQuantityPrecisionUnit());
      CST_ADJST_CST =
          EJBCommon.roundIt(CST_ADJST_CST, adCompany.getGlFunctionalCurrency().getFcPrecision());
      CST_RMNNG_QTY =
          EJBCommon.roundIt(CST_RMNNG_QTY, adPreference.getPrfInvQuantityPrecisionUnit());
      CST_RMNNG_VL =
          EJBCommon.roundIt(CST_RMNNG_VL, adCompany.getGlFunctionalCurrency().getFcPrecision());

      if (CST_ADJST_QTY < 0) {

        invItemLocation.setIlCommittedQuantity(
            invItemLocation.getIlCommittedQuantity() - Math.abs(CST_ADJST_QTY));

      }

      // create costing

      try {

        // generate line number

        LocalInvCosting invCurrentCosting =
            invCostingHome.getByMaxCstLineNumberAndCstDateToLongAndIiNameAndLocName(
                CST_DT.getTime(), invItemLocation.getInvItem().getIiName(),
                invItemLocation.getInvLocation().getLocName(), AD_BRNCH, AD_CMPNY);
        CST_LN_NMBR = invCurrentCosting.getCstLineNumber() + 1;

      } catch (FinderException ex) {

        CST_LN_NMBR = 1;

      }

      LocalInvCosting invCosting = invCostingHome.create(CST_DT, CST_DT.getTime(), CST_LN_NMBR, 0d,
          0d, 0d, 0d, CST_ADJST_QTY, CST_ADJST_CST, 0d, 0d, CST_RMNNG_QTY, CST_RMNNG_VL, 0d, 0d,
          CST_ADJST_QTY > 0 ? CST_ADJST_QTY : 0, AD_BRNCH, AD_CMPNY);
      // invItemLocation.addInvCosting(invCosting);

      invCosting.setInvItemLocation(invItemLocation);
      invCosting.setInvAdjustmentLine(invAdjustmentLine);

      // propagate balance if necessary

      Collection invCostings = invCostingHome.findByGreaterThanCstDateAndIiNameAndLocName(CST_DT,
          invItemLocation.getInvItem().getIiName(), invItemLocation.getInvLocation().getLocName(),
          AD_BRNCH, AD_CMPNY);

      Iterator i = invCostings.iterator();

      while (i.hasNext()) {

        LocalInvCosting invPropagatedCosting = (LocalInvCosting) i.next();

        invPropagatedCosting.setCstRemainingQuantity(
            invPropagatedCosting.getCstRemainingQuantity() + CST_ADJST_QTY);
        invPropagatedCosting
            .setCstRemainingValue(invPropagatedCosting.getCstRemainingValue() + CST_ADJST_CST);

      }


    } catch (Exception ex) {

      Debug.printStackTrace(ex);
      getSessionContext().setRollbackOnly();
      throw new EJBException(ex.getMessage());

    }



  }

  // SessionBean methods

  /**
   * @ejb:create-method view-type="remote"
   **/
  public void ejbCreate(
  ) throws CreateException {

    Debug.print("ArInvoiceBatchSubmitControllerBean ejbCreate");

  }
}
