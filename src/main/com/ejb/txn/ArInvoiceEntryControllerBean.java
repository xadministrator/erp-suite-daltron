package com.ejb.txn;

import java.util.Date;
import java.rmi.RemoteException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;


import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.StringTokenizer;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;
import javax.naming.NamingException;
import com.util.EJBCommon;

import com.ejb.ad.*;
import com.ejb.ap.LocalApPurchaseOrderLine;
import com.ejb.ar.*;
import com.ejb.gl.*;
import com.ejb.hr.LocalHrDeployedBranch;
import com.ejb.hr.LocalHrDeployedBranchHome;
import com.ejb.hr.LocalHrEmployee;
import com.ejb.hr.LocalHrEmployeeHome;
import com.ejb.inv.*;
import com.ejb.pm.LocalPmContractTerm;
import com.ejb.pm.LocalPmContractTermHome;
import com.ejb.pm.LocalPmProject;
import com.ejb.pm.LocalPmProjectHome;
import com.ejb.pm.LocalPmProjectPhase;
import com.ejb.pm.LocalPmProjectPhaseHome;
import com.ejb.pm.LocalPmProjectType;
import com.ejb.pm.LocalPmProjectTypeHome;
import com.ejb.pm.LocalPmProjectTypeType;
import com.ejb.pm.LocalPmProjectTypeTypeHome;
import com.ejb.exception.AdPRFCoaGlVarianceAccountNotFoundException;
import com.ejb.exception.ArINVAmountExceedsCreditLimitException;
import com.ejb.exception.ArINVNoSalesOrderLinesFoundException;
import com.ejb.exception.ArInvDuplicateUploadNumberException;
import com.ejb.exception.GlJREffectiveDateNoPeriodExistException;
import com.ejb.exception.GlJREffectiveDatePeriodClosedException;
import com.ejb.exception.GlobalBranchAccountNumberInvalidException;
import com.ejb.exception.GlobalConversionDateNotExistException;
import com.ejb.exception.GlobalDocumentNumberNotUniqueException;
import com.ejb.exception.GlobalExpiryDateNotFoundException;
import com.ejb.exception.GlobalInvItemLocationNotFoundException;
import com.ejb.exception.GlobalInventoryDateException;
import com.ejb.exception.GlobalJournalNotBalanceException;
import com.ejb.exception.GlobalMiscInfoIsRequiredException;
import com.ejb.exception.GlobalNoApprovalApproverFoundException;
import com.ejb.exception.GlobalNoApprovalRequesterFoundException;
import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.exception.GlobalPaymentTermInvalidException;
import com.ejb.exception.GlobalRecordAlreadyDeletedException;
import com.ejb.exception.GlobalRecordInvalidException;
import com.ejb.exception.GlobalTransactionAlreadyApprovedException;
import com.ejb.exception.GlobalTransactionAlreadyLockedException;
import com.ejb.exception.GlobalTransactionAlreadyPendingException;
import com.ejb.exception.GlobalTransactionAlreadyPostedException;
import com.ejb.exception.GlobalTransactionAlreadyVoidException;
import com.ejb.genfld.LocalGenField;


import com.util.AbstractSessionBean;
import com.util.AdBranchDetails;
import com.util.ArInvoiceDetails;
import com.util.ArModCustomerDetails;
import com.util.ArModInvoiceDetails;
import com.util.ArModInvoiceLineDetails;
import com.util.ArModInvoiceLineItemDetails;
import com.util.ArModJobOrderDetails;
import com.util.ArModJobOrderLineDetails;
import com.util.ArModSalesOrderDetails;
import com.util.ArModSalesOrderLineDetails;
import com.util.ArSalespersonDetails;
import com.util.ArStandardMemoLineDetails;
import com.util.ArTaxCodeDetails;
import com.util.Debug;
import com.util.EJBHomeFactory;
import com.util.GlModFunctionalCurrencyDetails;
import com.util.InvModLineItemDetails;
import com.util.InvModTagListDetails;
import com.util.InvModUnitOfMeasureDetails;

/**
 * @ejb:bean name="ArInvoiceEntryControllerEJB" display-name="used for entering invoice"
 *           type="Stateless" view-type="remote" jndi-name="ejb/ArInvoiceEntryControllerEJB"
 *
 * @ejb:interface remote-class="com.ejb.txn.ArInvoiceEntryController" extends="javax.ejb.EJBObject"
 *
 * @ejb:home remote-class="com.ejb.txn.ArInvoiceEntryControllerHome" extends="javax.ejb.EJBHome"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="aruser" role-link="aruserlink"
 *
 * @ejb:permission role-name="aruser"
 *
 */

public class ArInvoiceEntryControllerBean extends AbstractSessionBean {

  /**
   * @ejb:interface-method view-type="remote"
   * @jboss:method-attributes read-only="true"
   **/
  public ArrayList getGlFcAllWithDefault(
      Integer AD_CMPNY
  ) {

    Debug.print("ArInvoiceEntryControllerBean getGlFcAllWithDefault");

    LocalGlFunctionalCurrencyHome glFunctionalCurrencyHome = null;
    LocalAdCompanyHome adCompanyHome = null;

    Collection glFunctionalCurrencies = null;

    LocalGlFunctionalCurrency glFunctionalCurrency = null;
    LocalAdCompany adCompany = null;


    ArrayList list = new ArrayList();

    // Initialize EJB Home

    try {

      glFunctionalCurrencyHome = (LocalGlFunctionalCurrencyHome) EJBHomeFactory.lookUpLocalHome(
          LocalGlFunctionalCurrencyHome.JNDI_NAME, LocalGlFunctionalCurrencyHome.class);
      adCompanyHome = (LocalAdCompanyHome) EJBHomeFactory
          .lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);

    } catch (NamingException ex) {

      throw new EJBException(ex.getMessage());

    }

    try {

      adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);

      glFunctionalCurrencies = glFunctionalCurrencyHome
          .findFcAllEnabled(EJBCommon.getGcCurrentDateWoTime().getTime(), AD_CMPNY);

    } catch (Exception ex) {

      throw new EJBException(ex.getMessage());
    }

    if (glFunctionalCurrencies.isEmpty()) {

      return null;

    }

    Iterator i = glFunctionalCurrencies.iterator();

    while (i.hasNext()) {

      glFunctionalCurrency = (LocalGlFunctionalCurrency) i.next();

      GlModFunctionalCurrencyDetails mdetails = new GlModFunctionalCurrencyDetails(
          glFunctionalCurrency.getFcCode(), glFunctionalCurrency.getFcName(),
          adCompany.getGlFunctionalCurrency().getFcName().equals(glFunctionalCurrency.getFcName())
              ? EJBCommon.TRUE
              : EJBCommon.FALSE);

      list.add(mdetails);

    }

    return list;

  }


  /**
   * @ejb:interface-method view-type="remote"
   * @jboss:method-attributes read-only="true"
   **/
  public ArrayList getAdPytAll(
      Integer AD_CMPNY
  ) {

    Debug.print("ArInvoiceEntryControllerBean getAdPytAll");

    LocalAdPaymentTermHome adPaymentTermHome = null;

    ArrayList list = new ArrayList();

    // Initialize EJB Home

    try {

      adPaymentTermHome = (LocalAdPaymentTermHome) EJBHomeFactory
          .lookUpLocalHome(LocalAdPaymentTermHome.JNDI_NAME, LocalAdPaymentTermHome.class);

    } catch (NamingException ex) {

      throw new EJBException(ex.getMessage());

    }

    try {

      Collection adPaymentTerms = adPaymentTermHome.findEnabledPytAll(AD_CMPNY);

      Iterator i = adPaymentTerms.iterator();

      while (i.hasNext()) {

        LocalAdPaymentTerm adPaymentTerm = (LocalAdPaymentTerm) i.next();

        list.add(adPaymentTerm.getPytName());

      }

      return list;

    } catch (Exception ex) {

      Debug.printStackTrace(ex);
      throw new EJBException(ex.getMessage());

    }

  }


  /**
   * @ejb:interface-method view-type="remote"
   * @jboss:method-attributes read-only="true"
   **/
  public ArrayList getArTcAll(
      Integer AD_CMPNY
  ) {

    Debug.print("ArInvoiceEntryControllerBean getArTcAll");

    LocalArTaxCodeHome arTaxCodeHome = null;

    ArrayList list = new ArrayList();

    // Initialize EJB Home

    try {

      arTaxCodeHome = (LocalArTaxCodeHome) EJBHomeFactory
          .lookUpLocalHome(LocalArTaxCodeHome.JNDI_NAME, LocalArTaxCodeHome.class);

    } catch (NamingException ex) {

      throw new EJBException(ex.getMessage());

    }

    try {

      Collection arTaxCodes = arTaxCodeHome.findEnabledTcAll(AD_CMPNY);

      Iterator i = arTaxCodes.iterator();

      while (i.hasNext()) {

        LocalArTaxCode arTaxCode = (LocalArTaxCode) i.next();

        list.add(arTaxCode.getTcName());

      }

      return list;

    } catch (Exception ex) {

      Debug.printStackTrace(ex);
      throw new EJBException(ex.getMessage());

    }

  }


  /**
   * @ejb:interface-method view-type="remote"
   * @jboss:method-attributes read-only="true"
   **/
  public ArTaxCodeDetails getArTcByTcName(
      String TC_NM, Integer AD_CMPNY
  ) {

    Debug.print("ArInvoiceEntryControllerBean getArTcByTcName");

    LocalArTaxCodeHome arTaxCodeHome = null;

    ArrayList list = new ArrayList();

    // Initialize EJB Home

    try {

      arTaxCodeHome = (LocalArTaxCodeHome) EJBHomeFactory
          .lookUpLocalHome(LocalArTaxCodeHome.JNDI_NAME, LocalArTaxCodeHome.class);

    } catch (NamingException ex) {

      throw new EJBException(ex.getMessage());

    }

    try {

      LocalArTaxCode arTaxCode = arTaxCodeHome.findByTcName(TC_NM, AD_CMPNY);

      ArTaxCodeDetails details = new ArTaxCodeDetails();
      details.setTcType(arTaxCode.getTcType());
      details.setTcRate(arTaxCode.getTcRate());

      return details;

    } catch (Exception ex) {

      Debug.printStackTrace(ex);
      throw new EJBException(ex.getMessage());

    }

  }



  /**
   * @ejb:interface-method view-type="remote"
   * @jboss:method-attributes read-only="true"
   **/
  public ArrayList getArWtcAll(
      Integer AD_CMPNY
  ) {

    Debug.print("ArInvoiceEntryControllerBean getArWtcAll");

    LocalArWithholdingTaxCodeHome arWithholdingTaxCodeHome = null;

    ArrayList list = new ArrayList();

    // Initialize EJB Home

    try {

      arWithholdingTaxCodeHome = (LocalArWithholdingTaxCodeHome) EJBHomeFactory.lookUpLocalHome(
          LocalArWithholdingTaxCodeHome.JNDI_NAME, LocalArWithholdingTaxCodeHome.class);

    } catch (NamingException ex) {

      throw new EJBException(ex.getMessage());

    }

    try {

      Collection arWithholdingTaxCodes = arWithholdingTaxCodeHome.findEnabledWtcAll(AD_CMPNY);

      Iterator i = arWithholdingTaxCodes.iterator();

      while (i.hasNext()) {

        LocalArWithholdingTaxCode arWithholdingTaxCode = (LocalArWithholdingTaxCode) i.next();

        list.add(arWithholdingTaxCode.getWtcName());

      }

      return list;

    } catch (Exception ex) {

      Debug.printStackTrace(ex);
      throw new EJBException(ex.getMessage());

    }

  }



  /**
   * @ejb:interface-method view-type="remote"
   * @jboss:method-attributes read-only="true"
   **/
  public ArrayList getArCstAllForGeneration(
      Integer AD_BRNCH, Integer AD_CMPNY
  ) {

    Debug.print("ArInvoiceEntryControllerBean getArCstAllForGeneration");

    LocalArCustomerHome arCustomerHome = null;

    ArrayList list = new ArrayList();

    // Initialize EJB Home

    try {

      arCustomerHome = (LocalArCustomerHome) EJBHomeFactory
          .lookUpLocalHome(LocalArCustomerHome.JNDI_NAME, LocalArCustomerHome.class);

    } catch (NamingException ex) {

      throw new EJBException(ex.getMessage());

    }

    try {

      Collection arCustomers = arCustomerHome.findEnabledCstAll(AD_BRNCH, AD_CMPNY);

      Iterator i = arCustomers.iterator();

      while (i.hasNext()) {

        LocalArCustomer arCustomer = (LocalArCustomer) i.next();

        // filtering payroll customer to ar customer
        if (arCustomer.getCstEnablePayroll() == EJBCommon.FALSE) {
          list.add(arCustomer.getCstCustomerCode() + "#" + arCustomer.getCstNumbersParking() + "#"
              + arCustomer.getCstSquareMeter() + "#" + arCustomer.getCstRealPropertyTaxRate() + "#"
              + arCustomer.getArCustomerClass().getArTaxCode().getTcName() + "#"
              + arCustomer.getCstAssociationDuesRate());
        }


      }

      return list;

    } catch (Exception ex) {

      Debug.printStackTrace(ex);
      throw new EJBException(ex.getMessage());

    }

  }


  /**
   * @ejb:interface-method view-type="remote"
   * @jboss:method-attributes read-only="true"
   **/
  public ArrayList getAdLvCustomerBatchAll(
      Integer AD_CMPNY
  ) {

    Debug.print("ArInvoiceEntryControllerBean getAdLvCustomerBatchAll");

    LocalAdLookUpValueHome adLookUpValueHome = null;

    ArrayList list = new ArrayList();

    // Initialize EJB Home

    try {

      adLookUpValueHome = (LocalAdLookUpValueHome) EJBHomeFactory
          .lookUpLocalHome(LocalAdLookUpValueHome.JNDI_NAME, LocalAdLookUpValueHome.class);

    } catch (NamingException ex) {

      throw new EJBException(ex.getMessage());

    }

    try {

      Collection adLookUpValues =
          adLookUpValueHome.findByLuName("AR CUSTOMER BATCH - SOA", AD_CMPNY);

      Iterator i = adLookUpValues.iterator();

      while (i.hasNext()) {

        LocalAdLookUpValue adLookUpValue = (LocalAdLookUpValue) i.next();

        list.add(adLookUpValue.getLvName());

      }

      return list;

    } catch (Exception ex) {

      Debug.printStackTrace(ex);
      throw new EJBException(ex.getMessage());

    }

  }


  /**
   * @ejb:interface-method view-type="remote"
   * @jboss:method-attributes read-only="true"
   **/
  public ArrayList getArCstAllByCustomerBatch(
      ArrayList customerBatchList, Integer AD_BRNCH, Integer AD_CMPNY
  ) {

    Debug.print("ArInvoiceEntryControllerBean getArCstAllByCustomerBatch");

    LocalArCustomerHome arCustomerHome = null;

    ArrayList list = new ArrayList();

    // Initialize EJB Home

    try {

      arCustomerHome = (LocalArCustomerHome) EJBHomeFactory
          .lookUpLocalHome(LocalArCustomerHome.JNDI_NAME, LocalArCustomerHome.class);

    } catch (NamingException ex) {

      throw new EJBException(ex.getMessage());

    }

    try {



      for (int x = 0; x < customerBatchList.size(); x++) {


        String CST_BTCH = customerBatchList.get(x).toString();
        Collection arCustomers =
            arCustomerHome.findEnabledCstAllbyCustomerBatch(CST_BTCH, AD_BRNCH, AD_CMPNY);


        Iterator i = arCustomers.iterator();

        while (i.hasNext()) {

          LocalArCustomer arCustomer = (LocalArCustomer) i.next();

          list.add(arCustomer.getCstCustomerCode() + "#" + arCustomer.getCstNumbersParking() + "#"
              + arCustomer.getCstSquareMeter() + "#" + arCustomer.getCstRealPropertyTaxRate() + "#"
              + arCustomer.getArCustomerClass().getArTaxCode().getTcName() + "#"
              + arCustomer.getCstAssociationDuesRate());


        }

      }



      return list;

    } catch (Exception ex) {

      Debug.printStackTrace(ex);
      throw new EJBException(ex.getMessage());

    }

  }

  /**
   * @ejb:interface-method view-type="remote"
   * @jboss:method-attributes read-only="true"
   **/
  public ArrayList getArCstAll(
      Integer AD_BRNCH, Integer AD_CMPNY
  ) {

    Debug.print("ArInvoiceEntryControllerBean getArCstAll");

    LocalArCustomerHome arCustomerHome = null;

    ArrayList list = new ArrayList();

    // Initialize EJB Home

    try {

      arCustomerHome = (LocalArCustomerHome) EJBHomeFactory
          .lookUpLocalHome(LocalArCustomerHome.JNDI_NAME, LocalArCustomerHome.class);

    } catch (NamingException ex) {

      throw new EJBException(ex.getMessage());

    }

    try {

      Collection arCustomers = arCustomerHome.findEnabledCstAll(AD_BRNCH, AD_CMPNY);

      Iterator i = arCustomers.iterator();

      while (i.hasNext()) {

        LocalArCustomer arCustomer = (LocalArCustomer) i.next();
        // System.out.println(
        // arCustomer.getCstSquareMeter()+"#"+arCustomer.getCstNumbersParking());
        list.add(arCustomer.getCstCustomerCode());


      }

      return list;

    } catch (Exception ex) {

      Debug.printStackTrace(ex);
      throw new EJBException(ex.getMessage());

    }

  }

  /**
   * @ejb:interface-method view-type="remote"
   * @jboss:method-attributes read-only="true"
   **/
  public ArrayList getAdLvArFreightAll(
      Integer AD_CMPNY
  ) {

    Debug.print("ArInvoiceEntryControllerBean getAdLvArFreightAll");

    LocalAdLookUpValueHome adLookUpValueHome = null;

    ArrayList list = new ArrayList();

    // Initialize EJB Home

    try {

      adLookUpValueHome = (LocalAdLookUpValueHome) EJBHomeFactory
          .lookUpLocalHome(LocalAdLookUpValueHome.JNDI_NAME, LocalAdLookUpValueHome.class);

    } catch (NamingException ex) {

      throw new EJBException(ex.getMessage());

    }

    try {

      Collection adLookUpValues = adLookUpValueHome.findByLuName("AR FREIGHT", AD_CMPNY);

      Iterator i = adLookUpValues.iterator();

      while (i.hasNext()) {

        LocalAdLookUpValue adLookUpValue = (LocalAdLookUpValue) i.next();

        list.add(adLookUpValue.getLvName());

      }

      return list;

    } catch (Exception ex) {

      Debug.printStackTrace(ex);
      throw new EJBException(ex.getMessage());

    }

  }


  /**
   * @ejb:interface-method view-type="remote"
   * @jboss:method-attributes read-only="true"
   **/
  public ArrayList getArSmlAll(
      Integer AD_BRNCH, Integer AD_CMPNY
  ) {

    Debug.print("ArInvoiceEntryControllerBean getArSmlAll");

    LocalArStandardMemoLineHome arStandardMemoLineHome = null;

    ArrayList list = new ArrayList();

    // Initialize EJB Home

    try {

      arStandardMemoLineHome = (LocalArStandardMemoLineHome) EJBHomeFactory.lookUpLocalHome(
          LocalArStandardMemoLineHome.JNDI_NAME, LocalArStandardMemoLineHome.class);

    } catch (NamingException ex) {

      throw new EJBException(ex.getMessage());

    }

    try {

      Collection arStandardMemoLines = arStandardMemoLineHome.findEnabledSmlAll(AD_BRNCH, AD_CMPNY);

      Iterator i = arStandardMemoLines.iterator();

      while (i.hasNext()) {

        LocalArStandardMemoLine arStandardMemoLine = (LocalArStandardMemoLine) i.next();
        list.add(arStandardMemoLine.getSmlName());

      }

      return list;

    } catch (Exception ex) {

      Debug.printStackTrace(ex);
      throw new EJBException(ex.getMessage());

    }

  }

  /**
   * @ejb:interface-method view-type="remote"
   * @jboss:method-attributes read-only="true"
   **/
  public ArrayList getInvLocAll(
      Integer AD_CMPNY
  ) {

    Debug.print("ArInvoiceEntryControllerBean getInvLocAll");

    LocalInvLocationHome invLocationHome = null;
    Collection invLocations = null;
    ArrayList list = new ArrayList();

    // Initialize EJB Home

    try {

      invLocationHome = (LocalInvLocationHome) EJBHomeFactory
          .lookUpLocalHome(LocalInvLocationHome.JNDI_NAME, LocalInvLocationHome.class);

    } catch (NamingException ex) {

      throw new EJBException(ex.getMessage());

    }

    try {

      invLocations = invLocationHome.findLocAll(AD_CMPNY);

      if (invLocations.isEmpty()) {

        return null;

      }

      Iterator i = invLocations.iterator();

      while (i.hasNext()) {

        LocalInvLocation invLocation = (LocalInvLocation) i.next();
        String details = invLocation.getLocName();

        list.add(details);

      }

      return list;

    } catch (Exception ex) {

      Debug.printStackTrace(ex);
      throw new EJBException(ex.getMessage());

    }

  }

  /**
   * @ejb:interface-method view-type="remote"
   * @jboss:method-attributes read-only="true"
   **/
  public ArrayList getAdLvInvShiftAll(
      Integer AD_CMPNY
  ) {

    Debug.print("ArInvoiceEntryControllerBean getAdLvInvShiftAll");

    LocalAdLookUpValueHome adLookUpValueHome = null;

    ArrayList list = new ArrayList();

    // Initialize EJB Home

    try {

      adLookUpValueHome = (LocalAdLookUpValueHome) EJBHomeFactory
          .lookUpLocalHome(LocalAdLookUpValueHome.JNDI_NAME, LocalAdLookUpValueHome.class);

    } catch (NamingException ex) {

      throw new EJBException(ex.getMessage());

    }

    try {

      Collection adLookUpValues = adLookUpValueHome.findByLuName("INV SHIFT", AD_CMPNY);

      Iterator i = adLookUpValues.iterator();

      while (i.hasNext()) {

        LocalAdLookUpValue adLookUpValue = (LocalAdLookUpValue) i.next();

        list.add(adLookUpValue.getLvName());

      }

      return list;

    } catch (Exception ex) {

      Debug.printStackTrace(ex);
      throw new EJBException(ex.getMessage());

    }

  }

  /**
   * @ejb:interface-method view-type="remote"
   * @jboss:method-attributes read-only="true"
   **/
  public ArrayList getInvUomByIiName(
      String II_NM, Integer AD_CMPNY
  ) {

    Debug.print("ArInvoiceEntryControllerBean getInvUomByIiName");

    LocalInvUnitOfMeasureHome invUnitOfMeasureHome = null;
    LocalInvItemHome invItemHome = null;
    ArrayList list = new ArrayList();

    // Initialize EJB Home

    try {

      invUnitOfMeasureHome = (LocalInvUnitOfMeasureHome) EJBHomeFactory
          .lookUpLocalHome(LocalInvUnitOfMeasureHome.JNDI_NAME, LocalInvUnitOfMeasureHome.class);
      invItemHome = (LocalInvItemHome) EJBHomeFactory.lookUpLocalHome(LocalInvItemHome.JNDI_NAME,
          LocalInvItemHome.class);

    } catch (NamingException ex) {

      throw new EJBException(ex.getMessage());
    }

    try {

      LocalInvItem invItem = null;
      LocalInvUnitOfMeasure invItemUnitOfMeasure = null;

      invItem = invItemHome.findByIiName(II_NM, AD_CMPNY);
      invItemUnitOfMeasure = invItem.getInvUnitOfMeasure();

      Collection invUnitOfMeasures = null;
      Iterator i = invUnitOfMeasureHome
          .findByUomAdLvClass(invItemUnitOfMeasure.getUomAdLvClass(), AD_CMPNY).iterator();
      while (i.hasNext()) {

        LocalInvUnitOfMeasure invUnitOfMeasure = (LocalInvUnitOfMeasure) i.next();
        InvModUnitOfMeasureDetails details = new InvModUnitOfMeasureDetails();
        details.setUomName(invUnitOfMeasure.getUomName());

        if (invUnitOfMeasure.getUomName().equals(invItemUnitOfMeasure.getUomName())) {

          details.setDefault(true);

        }

        list.add(details);

      }

      return list;

    } catch (Exception ex) {

      Debug.printStackTrace(ex);
      throw new EJBException(ex.getMessage());

    }

  }


  /**
   * @ejb:interface-method view-type="remote"
   * @jboss:method-attributes read-only="true"
   **/
  public double getIiSalesPriceByInvCstCustomerCodeAndIiNameAndUomName(
      String CST_CSTMR_CODE, String II_NM, String UOM_NM, Integer AD_CMPNY
  ) {

    Debug.print("ArInvoiceEntryControllerBean getInvIiSalesPriceByIiNameAndUomName");

    LocalInvItemHome invItemHome = null;
    LocalInvPriceLevelHome invPriceLevelHome = null;
    LocalArCustomerHome arCustomerHome = null;
    LocalInvUnitOfMeasureConversionHome invUnitOfMeasureConversionHome = null;

    // Initialize EJB Home

    try {

      invItemHome = (LocalInvItemHome) EJBHomeFactory.lookUpLocalHome(LocalInvItemHome.JNDI_NAME,
          LocalInvItemHome.class);
      invPriceLevelHome = (LocalInvPriceLevelHome) EJBHomeFactory
          .lookUpLocalHome(LocalInvPriceLevelHome.JNDI_NAME, LocalInvPriceLevelHome.class);
      arCustomerHome = (LocalArCustomerHome) EJBHomeFactory
          .lookUpLocalHome(LocalArCustomerHome.JNDI_NAME, LocalArCustomerHome.class);
      invUnitOfMeasureConversionHome = (LocalInvUnitOfMeasureConversionHome) EJBHomeFactory
          .lookUpLocalHome(LocalInvUnitOfMeasureConversionHome.JNDI_NAME,
              LocalInvUnitOfMeasureConversionHome.class);

    } catch (NamingException ex) {

      throw new EJBException(ex.getMessage());

    }

    try {

      LocalArCustomer arCustomer = null;

      try {

        arCustomer = arCustomerHome.findByCstCustomerCode(CST_CSTMR_CODE, AD_CMPNY);

      } catch (FinderException ex) {

        return 0d;

      }

      double unitPrice = 0d;

      LocalInvItem invItem = invItemHome.findByIiName(II_NM, AD_CMPNY);

      LocalInvPriceLevel invPriceLevel = null;

      try {

        invPriceLevel = invPriceLevelHome.findByIiNameAndAdLvPriceLevel(II_NM,
            arCustomer.getCstDealPrice(), AD_CMPNY);



        if (invPriceLevel.getPlAmount() == 0) {

          unitPrice = invItem.getIiSalesPrice();

        } else {

          unitPrice = invPriceLevel.getPlAmount();

        }



      } catch (FinderException ex) {

        unitPrice = invItem.getIiSalesPrice();
      }

      LocalInvUnitOfMeasureConversion invUnitOfMeasureConversion =
          invUnitOfMeasureConversionHome.findUmcByIiNameAndUomName(II_NM, UOM_NM, AD_CMPNY);
      LocalInvUnitOfMeasureConversion invDefaultUomConversion = invUnitOfMeasureConversionHome
          .findUmcByIiNameAndUomName(II_NM, invItem.getInvUnitOfMeasure().getUomName(), AD_CMPNY);

      return EJBCommon.roundIt(
          unitPrice * invDefaultUomConversion.getUmcConversionFactor()
              / invUnitOfMeasureConversion.getUmcConversionFactor(),
          this.getGlFcPrecisionUnit(AD_CMPNY));


    } catch (Exception ex) {

      Debug.printStackTrace(ex);
      throw new EJBException(ex.getMessage());

    }

  }



  /**
   * @ejb:interface-method view-type="remote"
   * @jboss:method-attributes read-only="true"
   **/
  public double getIiSalesPriceByInvCstCustomerCodeAndIiNameAndUomName(
      String CST_CSTMR_CODE, String II_NM, Date INV_DT, String UOM_NM, Integer AD_CMPNY
  ) {

    Debug.print("ArInvoiceEntryControllerBean getInvIiSalesPriceByIiNameAndUomName");

    LocalInvItemHome invItemHome = null;
    LocalInvPriceLevelHome invPriceLevelHome = null;
    LocalInvPriceLevelDateHome invPriceLevelDateHome = null;
    LocalArCustomerHome arCustomerHome = null;
    LocalInvUnitOfMeasureConversionHome invUnitOfMeasureConversionHome = null;

    // Initialize EJB Home

    try {

      invItemHome = (LocalInvItemHome) EJBHomeFactory.lookUpLocalHome(LocalInvItemHome.JNDI_NAME,
          LocalInvItemHome.class);
      invPriceLevelHome = (LocalInvPriceLevelHome) EJBHomeFactory
          .lookUpLocalHome(LocalInvPriceLevelHome.JNDI_NAME, LocalInvPriceLevelHome.class);
      invPriceLevelDateHome = (LocalInvPriceLevelDateHome) EJBHomeFactory
          .lookUpLocalHome(LocalInvPriceLevelDateHome.JNDI_NAME, LocalInvPriceLevelDateHome.class);
      arCustomerHome = (LocalArCustomerHome) EJBHomeFactory
          .lookUpLocalHome(LocalArCustomerHome.JNDI_NAME, LocalArCustomerHome.class);
      invUnitOfMeasureConversionHome = (LocalInvUnitOfMeasureConversionHome) EJBHomeFactory
          .lookUpLocalHome(LocalInvUnitOfMeasureConversionHome.JNDI_NAME,
              LocalInvUnitOfMeasureConversionHome.class);

    } catch (NamingException ex) {

      throw new EJBException(ex.getMessage());

    }

    try {

      LocalArCustomer arCustomer = null;

      try {

        arCustomer = arCustomerHome.findByCstCustomerCode(CST_CSTMR_CODE, AD_CMPNY);

      } catch (FinderException ex) {

        return 0d;

      }

      double unitPrice = 0d;

      LocalInvItem invItem = invItemHome.findByIiName(II_NM, AD_CMPNY);

      LocalInvPriceLevel invPriceLevel = null;


      try {

        invPriceLevel = invPriceLevelHome.findByIiNameAndAdLvPriceLevel(II_NM,
            arCustomer.getCstDealPrice(), AD_CMPNY);

        Collection invPriceLevelDates = invPriceLevelDateHome.findPdByIiCodeAndPdPriceLevel(
            "ENABLE", invItem.getIiCode(), arCustomer.getCstDealPrice(), AD_CMPNY);

        unitPrice = invItem.getIiSalesPrice();


        if (invPriceLevelDates.size() <= 0) {

          if (invPriceLevel.getPlAmount() == 0) {

            unitPrice = invItem.getIiSalesPrice();

          } else {

            unitPrice = invPriceLevel.getPlAmount();

          }

        } else {

          boolean pricelLevelDateFound = false;
          Iterator i = invPriceLevelDates.iterator();

          while (i.hasNext()) {
            System.out.println("searching price level date");

            LocalInvPriceLevelDate invPriceLevelDate = (LocalInvPriceLevelDate) i.next();

            System.out.println("invoice date: " + INV_DT);

            System.out.println("deal date from date: " + invPriceLevelDate.getPdDateFrom());

            System.out.println("deal date to: " + invPriceLevelDate.getPdDateTo());

            if (INV_DT.equals(invPriceLevelDate.getPdDateFrom())
                || INV_DT.equals(invPriceLevelDate.getPdDateTo())) {
              pricelLevelDateFound = true;

              System.out.println("priceleveldate found");
              if (invPriceLevelDate.getPdAmount() == 0) {

                unitPrice = invItem.getIiSalesPrice();

              } else {

                unitPrice = invPriceLevelDate.getPdAmount();

              }

              break;

            }


            if (INV_DT.after(invPriceLevelDate.getPdDateFrom())
                && INV_DT.before(invPriceLevelDate.getPdDateTo())) {

              pricelLevelDateFound = true;
              System.out.println("priceleveldate found");
              if (invPriceLevelDate.getPdAmount() == 0) {

                unitPrice = invItem.getIiSalesPrice();

              } else {

                unitPrice = invPriceLevelDate.getPdAmount();

              }

              break;

            }


          }


        }


      } catch (FinderException ex) {

        System.out.println("got deal price not found");

        unitPrice = invItem.getIiSalesPrice();
      }

      LocalInvUnitOfMeasureConversion invUnitOfMeasureConversion =
          invUnitOfMeasureConversionHome.findUmcByIiNameAndUomName(II_NM, UOM_NM, AD_CMPNY);
      LocalInvUnitOfMeasureConversion invDefaultUomConversion = invUnitOfMeasureConversionHome
          .findUmcByIiNameAndUomName(II_NM, invItem.getInvUnitOfMeasure().getUomName(), AD_CMPNY);

      return EJBCommon.roundIt(
          unitPrice * invDefaultUomConversion.getUmcConversionFactor()
              / invUnitOfMeasureConversion.getUmcConversionFactor(),
          this.getGlFcPrecisionUnit(AD_CMPNY));


    } catch (Exception ex) {

      Debug.printStackTrace(ex);
      throw new EJBException(ex.getMessage());

    }

  }


  /**
   * @ejb:interface-method view-type="remote"
   * @jboss:method-attributes read-only="true"
   **/
  public short getAdPrfArInvoiceLineNumber(
      Integer AD_CMPNY
  ) {

    Debug.print("ArInvoiceEntryControllerBean getAdPrfArInvoiceLineNumber");

    LocalAdPreferenceHome adPreferenceHome = null;


    // Initialize EJB Home

    try {

      adPreferenceHome = (LocalAdPreferenceHome) EJBHomeFactory
          .lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);

    } catch (NamingException ex) {

      throw new EJBException(ex.getMessage());

    }


    try {

      LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);

      return adPreference.getPrfArInvoiceLineNumber();

    } catch (Exception ex) {

      Debug.printStackTrace(ex);
      throw new EJBException(ex.getMessage());

    }

  }

  /**
   * @ejb:interface-method view-type="remote"
   * @jboss:method-attributes read-only="true"
   **/
  public ArModInvoiceDetails getArInvByInvCode(
      Integer INV_CODE, Integer AD_CMPNY
  ) throws GlobalNoRecordFoundException {

    Debug.print("ArInvoiceEntryControllerBean getArInvByInvCode");

    LocalArInvoiceHome arInvoiceHome = null;
    LocalHrEmployeeHome hrEmployeeHome = null;
    LocalHrDeployedBranchHome hrDeployedBranchHome = null;

    LocalAdCompanyHome adCompanyHome = null;
    LocalGlSetOfBookHome glSetOfBookHome = null;
    // Initialize EJB Home

    try {

      arInvoiceHome = (LocalArInvoiceHome) EJBHomeFactory
          .lookUpLocalHome(LocalArInvoiceHome.JNDI_NAME, LocalArInvoiceHome.class);

      hrDeployedBranchHome = (LocalHrDeployedBranchHome) EJBHomeFactory
          .lookUpLocalHome(LocalHrDeployedBranchHome.JNDI_NAME, LocalHrDeployedBranchHome.class);


      hrEmployeeHome = (LocalHrEmployeeHome) EJBHomeFactory
          .lookUpLocalHome(LocalHrEmployeeHome.JNDI_NAME, LocalHrEmployeeHome.class);

      adCompanyHome = (LocalAdCompanyHome) EJBHomeFactory
          .lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);

      glSetOfBookHome = (LocalGlSetOfBookHome) EJBHomeFactory
          .lookUpLocalHome(LocalGlSetOfBookHome.JNDI_NAME, LocalGlSetOfBookHome.class);

    } catch (NamingException ex) {

      throw new EJBException(ex.getMessage());

    }

    try {

      LocalArInvoice arInvoice = null;
      LocalArTaxCode arTaxCode = null;


      try {

        arInvoice = arInvoiceHome.findByPrimaryKey(INV_CODE);
        arTaxCode = arInvoice.getArTaxCode();

      } catch (FinderException ex) {

        throw new GlobalNoRecordFoundException();

      }

      ArrayList list = new ArrayList();

      // get invoice line items if any

      Collection arInvoiceLineItems = arInvoice.getArInvoiceLineItems();
      Collection arInvoiceLines = arInvoice.getArInvoiceLines();
      Collection arSalesOrderInvoiceLines = arInvoice.getArSalesOrderInvoiceLines();
      Collection arJobOrderinvoiceLines = arInvoice.getArJobOrderInvoiceLines();


      System.out.println("arInvoiceLineItems=" + arInvoice.getArInvoiceLineItems().size());
      System.out.println("arInvoiceLines=" + arInvoice.getArInvoiceLines().size());
      System.out
          .println("arSalesOrderInvoiceLines=" + arInvoice.getArSalesOrderInvoiceLines().size());

      if (!arInvoiceLineItems.isEmpty()) {
        System.out.println("items search");
        Iterator i = arInvoiceLineItems.iterator();

        while (i.hasNext()) {

          LocalArInvoiceLineItem arInvoiceLineItem = (LocalArInvoiceLineItem) i.next();

          ArModInvoiceLineItemDetails iliDetails = new ArModInvoiceLineItemDetails();

          iliDetails.setIliCode(arInvoiceLineItem.getIliCode());
          iliDetails.setIliLine(arInvoiceLineItem.getIliLine());
          iliDetails.setIliQuantity(arInvoiceLineItem.getIliQuantity());
          iliDetails.setIliUnitPrice(arInvoiceLineItem.getIliUnitPrice());
          iliDetails.setIliIiName(arInvoiceLineItem.getInvItemLocation().getInvItem().getIiName());
          iliDetails
              .setIliLocName(arInvoiceLineItem.getInvItemLocation().getInvLocation().getLocName());
          iliDetails.setIliUomName(arInvoiceLineItem.getInvUnitOfMeasure().getUomName());
          iliDetails.setIliIiDescription(
              arInvoiceLineItem.getInvItemLocation().getInvItem().getIiDescription());
          iliDetails.setIliEnableAutoBuild(arInvoiceLineItem.getIliEnableAutoBuild());
          iliDetails
              .setIliIiClass(arInvoiceLineItem.getInvItemLocation().getInvItem().getIiClass());
          iliDetails.setIliDiscount1(arInvoiceLineItem.getIliDiscount1());
          iliDetails.setIliDiscount2(arInvoiceLineItem.getIliDiscount2());
          iliDetails.setIliDiscount3(arInvoiceLineItem.getIliDiscount3());
          iliDetails.setIliDiscount4(arInvoiceLineItem.getIliDiscount4());
          iliDetails.setIliTotalDiscount(arInvoiceLineItem.getIliTotalDiscount());
          iliDetails.setIliMisc(arInvoiceLineItem.getIliMisc());
          iliDetails.setIliTax(arInvoiceLineItem.getIliTax());
          System.out.println("apPurchaseOrderLine.getPlMisc() : " + arInvoiceLineItem.getIliMisc());

          if (arTaxCode.getTcType().equals("INCLUSIVE")) {

            iliDetails.setIliAmount(
                arInvoiceLineItem.getIliAmount() + arInvoiceLineItem.getIliTaxAmount());

          } else {

            iliDetails.setIliAmount(arInvoiceLineItem.getIliAmount());

          }
          iliDetails
              .setTraceMisc(arInvoiceLineItem.getInvItemLocation().getInvItem().getIiTraceMisc());

          if (iliDetails.getTraceMisc() == 1) {
            ArrayList tagList = this.getInvTagList(arInvoiceLineItem);

            iliDetails.setIliTagList(tagList);
          }

          list.add(iliDetails);

        }

      } else if (!arInvoiceLines.isEmpty()) {

        // get invoice lines
        System.out.println("invoice lines is not  empty");
        Iterator i = arInvoiceLines.iterator();

        while (i.hasNext()) {

          LocalArInvoiceLine arInvoiceLine = (LocalArInvoiceLine) i.next();

          ArModInvoiceLineDetails mdetails = new ArModInvoiceLineDetails();

          mdetails.setIlCode(arInvoiceLine.getIlCode());
          mdetails.setIlDescription(arInvoiceLine.getIlDescription());
          mdetails.setIlQuantity(arInvoiceLine.getIlQuantity());
          mdetails.setIlUnitPrice(arInvoiceLine.getIlUnitPrice());
          if (arTaxCode.getTcType().equals("INCLUSIVE")) {

            mdetails.setIlAmount(arInvoiceLine.getIlAmount() + arInvoiceLine.getIlTaxAmount());

          } else {

            mdetails.setIlAmount(arInvoiceLine.getIlAmount());

          }
          mdetails.setIlTax(arInvoiceLine.getIlTax());
          mdetails.setIlSmlName(arInvoiceLine.getArStandardMemoLine().getSmlName());

          list.add(mdetails);

        }
        System.out.println("invoice lines is not  empty finish" + list.size());
      } else if (!arSalesOrderInvoiceLines.isEmpty()) {

        Iterator i = arSalesOrderInvoiceLines.iterator();

        while (i.hasNext()) {

          LocalArSalesOrderInvoiceLine arSalesOrderInvoiceLine =
              (LocalArSalesOrderInvoiceLine) i.next();
          LocalArSalesOrderLine arSalesOrderLine = arSalesOrderInvoiceLine.getArSalesOrderLine();

          ArModSalesOrderLineDetails mdetails = new ArModSalesOrderLineDetails();

          mdetails.setSolCode(arSalesOrderLine.getSolCode());
          mdetails.setSolLine(arSalesOrderLine.getSolLine());
          mdetails.setSolIiName(arSalesOrderLine.getInvItemLocation().getInvItem().getIiName());
          mdetails.setSolIiDescription(
              arSalesOrderLine.getInvItemLocation().getInvItem().getIiDescription());
          mdetails
              .setSolLocName(arSalesOrderLine.getInvItemLocation().getInvLocation().getLocName());

          Iterator j = arSalesOrderLine.getArSalesOrderInvoiceLines().iterator();
          double SOLD_ITEMS = 0d;

          while (j.hasNext()) {

            LocalArSalesOrderInvoiceLine arSOILine = (LocalArSalesOrderInvoiceLine) j.next();
            SOLD_ITEMS += arSOILine.getSilQuantityDelivered();

          }

          mdetails.setSolRemaining(arSalesOrderLine.getSolQuantity() - SOLD_ITEMS);
          mdetails.setSolQuantity(arSalesOrderInvoiceLine.getSilQuantityDelivered());
          mdetails.setSolUomName(arSalesOrderLine.getInvUnitOfMeasure().getUomName());
          mdetails.setSolUnitPrice(arSalesOrderLine.getSolUnitPrice());

          if (arTaxCode.getTcType().equals("INCLUSIVE")) {

            mdetails.setSolAmount(
                arSalesOrderInvoiceLine.getSilAmount() + arSalesOrderInvoiceLine.getSilTaxAmount());

          } else {

            mdetails.setSolAmount(arSalesOrderInvoiceLine.getSilAmount());

          }

          mdetails.setSolIssue(true);
          mdetails.setSolDiscount1(arSalesOrderInvoiceLine.getSilDiscount1());
          mdetails.setSolDiscount2(arSalesOrderInvoiceLine.getSilDiscount2());
          mdetails.setSolDiscount3(arSalesOrderInvoiceLine.getSilDiscount3());
          mdetails.setSolDiscount4(arSalesOrderInvoiceLine.getSilDiscount4());
          mdetails.setSolTotalDiscount(arSalesOrderInvoiceLine.getSilTotalDiscount());
          mdetails.setSolMisc(arSalesOrderInvoiceLine.getSilImei());
          mdetails.setSolTax(arSalesOrderInvoiceLine.getSilTax());

          mdetails
              .setTraceMisc(arSalesOrderLine.getInvItemLocation().getInvItem().getIiTraceMisc());

          if (mdetails.getTraceMisc() == 1) {
            ArrayList tagList = this.getInvTagList(arSalesOrderInvoiceLine);

            mdetails.setSolTagList(tagList);
          }



          list.add(mdetails);

        }

      } else if (!arJobOrderinvoiceLines.isEmpty()) {

        Iterator i = arJobOrderinvoiceLines.iterator();

        while (i.hasNext()) {

          LocalArJobOrderInvoiceLine arJobOrderInvoiceLine = (LocalArJobOrderInvoiceLine) i.next();
          LocalArJobOrderLine arJobOrderLine = arJobOrderInvoiceLine.getArJobOrderLine();

          ArModJobOrderLineDetails mdetails = new ArModJobOrderLineDetails();

          mdetails.setJolCode(arJobOrderLine.getJolCode());
          mdetails.setJolLine(arJobOrderLine.getJolLine());
          mdetails.setJolIiName(arJobOrderLine.getInvItemLocation().getInvItem().getIiName());
          mdetails.setJolIiDescription(
              arJobOrderLine.getInvItemLocation().getInvItem().getIiDescription());
          mdetails.setJolLocName(arJobOrderLine.getInvItemLocation().getInvLocation().getLocName());

          Iterator j = arJobOrderLine.getArJobOrderInvoiceLines().iterator();
          double SOLD_ITEMS = 0d;

          while (j.hasNext()) {

            LocalArJobOrderInvoiceLine arJOILine = (LocalArJobOrderInvoiceLine) j.next();
            SOLD_ITEMS += arJOILine.getJilQuantityDelivered();

          }

          mdetails.setJolRemaining(arJobOrderLine.getJolQuantity() - SOLD_ITEMS);
          mdetails.setJolQuantity(arJobOrderInvoiceLine.getJilQuantityDelivered());
          mdetails.setJolUomName(arJobOrderLine.getInvUnitOfMeasure().getUomName());
          mdetails.setJolUnitPrice(arJobOrderLine.getJolUnitPrice());

          if (arTaxCode.getTcType().equals("INCLUSIVE")) {

            mdetails.setJolAmount(
                arJobOrderInvoiceLine.getJilAmount() + arJobOrderInvoiceLine.getJilTaxAmount());

          } else {

            mdetails.setJolAmount(arJobOrderInvoiceLine.getJilAmount());

          }

          mdetails.setJolIssue(true);
          mdetails.setJolDiscount1(arJobOrderInvoiceLine.getJilDiscount1());
          mdetails.setJolDiscount2(arJobOrderInvoiceLine.getJilDiscount2());
          mdetails.setJolDiscount3(arJobOrderInvoiceLine.getJilDiscount3());
          mdetails.setJolDiscount4(arJobOrderInvoiceLine.getJilDiscount4());
          mdetails.setJolTotalDiscount(arJobOrderInvoiceLine.getJilTotalDiscount());
          mdetails.setJolMisc(arJobOrderInvoiceLine.getJilImei());
          mdetails.setJolTax(arJobOrderInvoiceLine.getJilTax());

          mdetails.setTraceMisc(arJobOrderLine.getInvItemLocation().getInvItem().getIiTraceMisc());

          if (mdetails.getTraceMisc() == 1) {
            ArrayList tagList = this.getInvTagList(arJobOrderInvoiceLine);

            mdetails.setJolTagList(tagList);
          }



          list.add(mdetails);

        }

      }

      ArModInvoiceDetails mInvDetails = new ArModInvoiceDetails();

      mInvDetails.setInvCode(arInvoice.getInvCode());
      mInvDetails.setInvDocumentType(arInvoice.getInvDocumentType());
      mInvDetails.setInvType(arInvoice.getInvType());
      mInvDetails.setInvDescription(arInvoice.getInvDescription());
      mInvDetails.setInvDate(arInvoice.getInvDate());
      mInvDetails.setInvNumber(arInvoice.getInvNumber());
      mInvDetails.setInvReferenceNumber(arInvoice.getInvReferenceNumber());
      mInvDetails.setInvUploadNumber(arInvoice.getInvUploadNumber());
      mInvDetails.setInvConversionDate(arInvoice.getInvConversionDate());
      mInvDetails.setInvMemo(arInvoice.getInvMemo());
      mInvDetails.setInvConversionRate(arInvoice.getInvConversionRate());
      mInvDetails.setInvPreviousReading(arInvoice.getInvPreviousReading());
      mInvDetails.setInvPresentReading(arInvoice.getInvPresentReading());
      mInvDetails.setInvAmountDue(arInvoice.getInvAmountDue());
      mInvDetails.setInvDownPayment(arInvoice.getInvDownPayment());
      mInvDetails.setInvAmountPaid(arInvoice.getInvAmountPaid());
      mInvDetails.setInvPenaltyDue(arInvoice.getInvPenaltyDue());
      mInvDetails.setInvPenaltyPaid(arInvoice.getInvPenaltyPaid());
      mInvDetails.setInvAmountUnearnedInterest(arInvoice.getInvAmountUnearnedInterest());
      mInvDetails.setInvBillToAddress(arInvoice.getInvBillToAddress());
      mInvDetails.setInvBillToContact(arInvoice.getInvBillToContact());
      mInvDetails.setInvBillToAltContact(arInvoice.getInvBillToAltContact());
      mInvDetails.setInvBillToPhone(arInvoice.getInvBillToPhone());
      mInvDetails.setInvBillingHeader(arInvoice.getInvBillingHeader());
      mInvDetails.setInvBillingFooter(arInvoice.getInvBillingFooter());
      mInvDetails.setInvBillingHeader2(arInvoice.getInvBillingHeader2());
      mInvDetails.setInvBillingFooter2(arInvoice.getInvBillingFooter2());
      mInvDetails.setInvBillingHeader3(arInvoice.getInvBillingHeader3());
      mInvDetails.setInvBillingFooter3(arInvoice.getInvBillingFooter3());
      mInvDetails.setInvBillingSignatory(arInvoice.getInvBillingSignatory());
      mInvDetails.setInvSignatoryTitle(arInvoice.getInvSignatoryTitle());
      mInvDetails.setInvShipToAddress(arInvoice.getInvShipToAddress());
      mInvDetails.setInvShipToContact(arInvoice.getInvShipToContact());
      mInvDetails.setInvShipToAltContact(arInvoice.getInvShipToAltContact());
      mInvDetails.setInvShipToPhone(arInvoice.getInvShipToPhone());
      mInvDetails.setInvShipDate(arInvoice.getInvShipDate());
      mInvDetails.setInvLvFreight(arInvoice.getInvLvFreight());
      mInvDetails.setInvApprovalStatus(arInvoice.getInvApprovalStatus());
      mInvDetails.setInvReasonForRejection(arInvoice.getInvReasonForRejection());
      mInvDetails.setInvPosted(arInvoice.getInvPosted());
      mInvDetails.setInvVoid(arInvoice.getInvVoid());

      mInvDetails.setInvInterestNextRunDate(arInvoice.getInvInterestNextRunDate());
      mInvDetails.setInvDisableInterest(arInvoice.getInvDisableInterest());
      mInvDetails.setInvInterest(arInvoice.getInvInterest());

      mInvDetails.setInvInterestReferenceNumber(arInvoice.getInvInterestReferenceNumber());
      mInvDetails.setInvInterestAmount(arInvoice.getInvInterestAmount());
      mInvDetails.setInvInterestCreatedBy(arInvoice.getInvInterestCreatedBy());
      mInvDetails.setInvInterestDateCreated(arInvoice.getInvInterestDateCreated());
      mInvDetails.setInvInterestNextRunDate(arInvoice.getInvInterestNextRunDate());
      mInvDetails.setInvInterestLastRunDate(arInvoice.getInvInterestLastRunDate());

      mInvDetails.setInvCreatedBy(arInvoice.getInvCreatedBy());
      mInvDetails.setInvDateCreated(arInvoice.getInvDateCreated());
      mInvDetails.setInvLastModifiedBy(arInvoice.getInvLastModifiedBy());
      mInvDetails.setInvDateLastModified(arInvoice.getInvDateLastModified());
      mInvDetails.setInvApprovedRejectedBy(arInvoice.getInvApprovedRejectedBy());
      mInvDetails.setInvDateApprovedRejected(arInvoice.getInvDateApprovedRejected());
      mInvDetails.setInvPostedBy(arInvoice.getInvPostedBy());
      mInvDetails.setInvDatePosted(arInvoice.getInvDatePosted());
      mInvDetails.setInvFcName(arInvoice.getGlFunctionalCurrency().getFcName());
      mInvDetails.setInvTcName(arInvoice.getArTaxCode().getTcName());
      mInvDetails.setInvWtcName(arInvoice.getArWithholdingTaxCode().getWtcName());
      mInvDetails.setInvCstCustomerCode(arInvoice.getArCustomer().getCstCustomerCode());
      System.out.println("AR_CSTMR_CODE=" + arInvoice.getArCustomer().getCstCustomerCode());
      mInvDetails.setInvPytName(arInvoice.getAdPaymentTerm().getPytName());
      mInvDetails.setInvIbName(
          arInvoice.getArInvoiceBatch() != null ? arInvoice.getArInvoiceBatch().getIbName() : null);
      mInvDetails.setInvLvShift(arInvoice.getInvLvShift());
      mInvDetails.setInvSoNumber(arInvoice.getInvSoNumber());
      mInvDetails.setInvJoNumber(arInvoice.getInvJoNumber());
      mInvDetails.setInvDebitMemo(arInvoice.getInvDebitMemo());
      mInvDetails.setInvSubjectToCommission(arInvoice.getInvSubjectToCommission());
      mInvDetails.setInvCstName(arInvoice.getArCustomer().getCstName());
      mInvDetails.setInvTcRate(arInvoice.getArTaxCode().getTcRate());
      mInvDetails.setInvTcType(arInvoice.getArTaxCode().getTcType());
      mInvDetails.setInvClientPO(arInvoice.getInvClientPO());
      mInvDetails.setInvEffectivityDate(arInvoice.getInvEffectivityDate());
      mInvDetails.setInvRecieveDate(arInvoice.getInvRecieveDate());
      mInvDetails.setReportParameter(arInvoice.getReportParameter());
      // HRIS
      if (arInvoice.getArCustomer().getHrEmployee() != null) {

        mInvDetails.setInvHrEmployeeNumber(
            arInvoice.getArCustomer().getHrEmployee().getEmpEmployeeNumber());
        mInvDetails.setInvHrBioNumber(arInvoice.getArCustomer().getHrEmployee().getEmpBioNumber());
        mInvDetails.setInvHrManagingBranch(arInvoice.getInvHrManagingBranch());
        mInvDetails.setInvHrCurrentJobPosition(arInvoice.getInvHrCurrentJobPosition());

      }

      if (arInvoice.getHrDeployedBranch() != null) {

        mInvDetails.setInvHrDeployedBranchName(arInvoice.getHrDeployedBranch().getDbClientName());

      }



      // PMA

      if (arInvoice.getPmProject() != null) {

        mInvDetails.setInvPmProjectCode(arInvoice.getPmProject().getPrjProjectCode());

      }

      if (arInvoice.getPmProjectTypeType() != null) {

        mInvDetails.setInvPmProjectTypeCode(
            arInvoice.getPmProjectTypeType().getPmProjectType().getPtProjectTypeCode());

      }

      if (arInvoice.getPmProjectPhase() != null) {

        mInvDetails.setInvPmProjectPhaseName(arInvoice.getPmProjectPhase().getPpName());

      }

      if (arInvoice.getPmContractTerm() != null) {

        mInvDetails.setInvPmContractTermName(arInvoice.getPmContractTerm().getCtTermDescription());

      }


      System.out.println("CHECK A");
      /*
       * LocalGlSetOfBook glJournalSetOfBook = null;
       * 
       * try {
       * 
       * glJournalSetOfBook = glSetOfBookHome.findByDate(arInvoice.getInvDate(), AD_CMPNY);
       * 
       * } catch (FinderException ex) {
       * 
       * throw new GlJREffectiveDateNoPeriodExistException();
       * 
       * }
       */

      if (arInvoice.getArSalesperson() != null) {
        System.out.println("salesperson check");
        mInvDetails.setInvSlpSalespersonCode(arInvoice.getArSalesperson().getSlpSalespersonCode());
        mInvDetails.setInvSlpName(arInvoice.getArSalesperson().getSlpName());

      }

      if (!arInvoiceLineItems.isEmpty()) {

        System.out.println("items size is : " + list.size());
        mInvDetails.setInvIliList(list);

      } else if (!arInvoiceLines.isEmpty()) {
        System.out.println("memo line size is " + list.size());
        mInvDetails.setInvIlList(list);

      } else if (!arSalesOrderInvoiceLines.isEmpty()) {


        mInvDetails.setInvSoNumber(arInvoice.getInvSoNumber());
        mInvDetails.setInvSoList(list);

      } else if (!arJobOrderinvoiceLines.isEmpty()) {


        mInvDetails.setInvJoNumber(arInvoice.getInvJoNumber());
        mInvDetails.setInvJoList(list);

      }

      // get last due date
      System.out.println("enter payment schedule");
      Collection arInvoicePaymentSchedules = arInvoice.getArInvoicePaymentSchedules();

      if (!arInvoicePaymentSchedules.isEmpty()) {

        ArrayList arInvoicePaymentScheduleList = new ArrayList(arInvoicePaymentSchedules);

        LocalArInvoicePaymentSchedule arInvoicePaymentSchedule =
            (LocalArInvoicePaymentSchedule) arInvoicePaymentScheduleList
                .get(arInvoicePaymentScheduleList.size() - 1);

        mInvDetails.setInvDueDate(arInvoicePaymentSchedule.getIpsDueDate());

      }


      return mInvDetails;

    } catch (GlobalNoRecordFoundException ex) {

      throw ex;

    } catch (Exception ex) {

      Debug.printStackTrace(ex);
      throw new EJBException(ex.getMessage());

    }

  }


  /**
   * @ejb:interface-method view-type="remote"
   * @jboss:method-attributes read-only="true"
   **/
  public ArModCustomerDetails getArCstByCstCustomerCode(
      String CST_CSTMR_CODE, Integer AD_CMPNY
  ) throws GlobalNoRecordFoundException {

    Debug.print("ArInvoiceEntryControllerBean getArCstByCstCustomerCode");

    LocalArCustomerHome arCustomerHome = null;
    LocalArSalespersonHome arSalespersonHome = null;
    // Initialize EJB Home

    try {

      arSalespersonHome = (LocalArSalespersonHome) EJBHomeFactory
          .lookUpLocalHome(LocalArSalespersonHome.JNDI_NAME, LocalArSalespersonHome.class);
      arCustomerHome = (LocalArCustomerHome) EJBHomeFactory
          .lookUpLocalHome(LocalArCustomerHome.JNDI_NAME, LocalArCustomerHome.class);

    } catch (NamingException ex) {

      throw new EJBException(ex.getMessage());

    }

    try {

      LocalArCustomer arCustomer = null;


      try {

        arCustomer = arCustomerHome.findByCstCustomerCode(CST_CSTMR_CODE, AD_CMPNY);

      } catch (FinderException ex) {

        throw new GlobalNoRecordFoundException();

      }

      ArModCustomerDetails mdetails = new ArModCustomerDetails();



      mdetails.setCstCustomerCode(arCustomer.getCstCustomerCode());

      mdetails.setCstCcName((arCustomer.getArCustomerClass() == null) ? null
          : arCustomer.getArCustomerClass().getCcName());

      System.out.println(arCustomer.getArCustomerClass().getCcName() + " frm cntrller");
      mdetails.setCstPytName(
          arCustomer.getAdPaymentTerm() != null ? arCustomer.getAdPaymentTerm().getPytName()
              : null);
      mdetails.setCstCcWtcName(arCustomer.getArCustomerClass().getArWithholdingTaxCode() != null
          ? arCustomer.getArCustomerClass().getArWithholdingTaxCode().getWtcName()
          : null);
      mdetails.setCstBillToAddress(arCustomer.getCstBillToAddress());
      mdetails.setCstShipToAddress(arCustomer.getCstShipToAddress());
      mdetails.setCstBillToAddress(arCustomer.getCstBillToAddress());
      mdetails.setCstBillToContact(arCustomer.getCstBillToContact());
      mdetails.setCstBillToAltContact(arCustomer.getCstBillToAltContact());
      mdetails.setCstBillToPhone(arCustomer.getCstBillToPhone());
      mdetails.setCstBillingHeader(arCustomer.getCstBillingHeader());
      mdetails.setCstBillingFooter(arCustomer.getCstBillingFooter());
      mdetails.setCstBillingHeader2(arCustomer.getCstBillingHeader2());
      mdetails.setCstBillingFooter2(arCustomer.getCstBillingFooter2());
      mdetails.setCstBillingHeader3(arCustomer.getCstBillingHeader3());
      mdetails.setCstBillingFooter3(arCustomer.getCstBillingFooter3());
      mdetails.setCstBillingSignatory(arCustomer.getCstBillingSignatory());
      mdetails.setCstSignatoryTitle(arCustomer.getCstSignatoryTitle());
      mdetails.setCstShipToAddress(arCustomer.getCstShipToAddress());
      mdetails.setCstShipToContact(arCustomer.getCstShipToContact());
      mdetails.setCstShipToAltContact(arCustomer.getCstShipToAltContact());
      mdetails.setCstShipToPhone(arCustomer.getCstShipToPhone());
      mdetails.setCstName(arCustomer.getCstName());
      mdetails.setCstNumbersParking(arCustomer.getCstNumbersParking());
      mdetails.setCstSquareMeter(arCustomer.getCstSquareMeter());
      mdetails.setCstAssociationDuesRate(arCustomer.getCstAssociationDuesRate());
      mdetails.setCstRealPropertyTaxRate(arCustomer.getCstRealPropertyTaxRate());
      if (arCustomer.getArSalesperson() != null && arCustomer.getCstArSalesperson2() == null) {

        mdetails.setCstSlpSalespersonCode(arCustomer.getArSalesperson().getSlpSalespersonCode());
        mdetails.setCstSlpName(arCustomer.getArSalesperson().getSlpName());

      } else if (arCustomer.getArSalesperson() == null
          && arCustomer.getCstArSalesperson2() != null) {

        LocalArSalesperson arSalesperson2 = null;
        arSalesperson2 = arSalespersonHome.findByPrimaryKey(arCustomer.getCstArSalesperson2());

        mdetails.setCstSlpSalespersonCode(arSalesperson2.getSlpSalespersonCode());
        mdetails.setCstSlpName(arSalesperson2.getSlpName());

      }
      if (arCustomer.getArSalesperson() != null && arCustomer.getCstArSalesperson2() != null) {

        mdetails.setCstSlpSalespersonCode(arCustomer.getArSalesperson().getSlpSalespersonCode());
        mdetails.setCstSlpName(arCustomer.getArSalesperson().getSlpName());
        LocalArSalesperson arSalesperson2 = null;
        arSalesperson2 = arSalespersonHome.findByPrimaryKey(arCustomer.getCstArSalesperson2());

        mdetails.setCstSlpSalespersonCode2(arSalesperson2.getSlpSalespersonCode());
        mdetails.setCstSlpName2(arSalesperson2.getSlpName());
      }

      if (arCustomer.getArCustomerClass().getArTaxCode() != null) {

        mdetails.setCstCcTcName(arCustomer.getArCustomerClass().getArTaxCode().getTcName());
        mdetails.setCstCcTcRate(arCustomer.getArCustomerClass().getArTaxCode().getTcRate());
        mdetails.setCstCcTcType(arCustomer.getArCustomerClass().getArTaxCode().getTcType());

      }

      if (arCustomer.getHrDeployedBranch() != null) {

        mdetails.setCstHrDeployedBranchName(arCustomer.getHrDeployedBranch().getDbClientCode());
      }

      if (arCustomer.getHrEmployee() != null) {


        mdetails.setCstHrEmployeeNumber(arCustomer.getHrEmployee().getEmpEmployeeNumber());
        mdetails.setCstHrBioNumber(arCustomer.getHrEmployee().getEmpBioNumber());
        mdetails.setCstHrManagingBranch(arCustomer.getHrEmployee().getEmpManagingBranch());
        mdetails.setCstHrCurrentJobPosition(arCustomer.getHrEmployee().getEmpCurrentJobPosition());
      }

      if (arCustomer.getInvLineItemTemplate() != null) {
        mdetails.setCstLitName(arCustomer.getInvLineItemTemplate().getLitName());
      }

      return mdetails;

    } catch (GlobalNoRecordFoundException ex) {

      throw ex;

    } catch (Exception ex) {

      Debug.printStackTrace(ex);
      throw new EJBException(ex.getMessage());

    }

  }


  /**
   * @ejb:interface-method view-type="remote"
   * @jboss:method-attributes read-only="true"
   **/
  public com.util.ArStandardMemoLineDetails getArSmlByCstCstmrCodeSmlNm(
      String CST_CSTMR_CODE, String SML_NM, int AD_BRNCH, Integer AD_CMPNY
  ) throws GlobalNoRecordFoundException {

    Debug.print("ArInvoiceEntryControllerBean getArSmlByCcNmSmlNm");

    LocalArStandardMemoLineHome arStandardMemoLineHome = null;
    LocalArStandardMemoLineClassHome arStandardMemoLineClassHome = null;
    LocalArCustomerHome arCustomerHome = null;

    try {

      arStandardMemoLineHome = (LocalArStandardMemoLineHome) EJBHomeFactory.lookUpLocalHome(
          LocalArStandardMemoLineHome.JNDI_NAME, LocalArStandardMemoLineHome.class);

      arStandardMemoLineClassHome =
          (LocalArStandardMemoLineClassHome) EJBHomeFactory.lookUpLocalHome(
              LocalArStandardMemoLineClassHome.JNDI_NAME, LocalArStandardMemoLineClassHome.class);

      arCustomerHome = (LocalArCustomerHome) EJBHomeFactory
          .lookUpLocalHome(LocalArCustomerHome.JNDI_NAME, LocalArCustomerHome.class);

    } catch (NamingException ex) {

      throw new EJBException(ex.getMessage());

    }

    try {
      LocalArStandardMemoLine arStandardMemoLine = null;
      LocalArStandardMemoLineClass arStandardMemoLineClass = null;
      LocalArCustomer arCustomer = null;



      ArStandardMemoLineDetails details = new ArStandardMemoLineDetails();

      try {

        arStandardMemoLine = arStandardMemoLineHome.findBySmlName(SML_NM, AD_CMPNY);
        details.setSmlDescription(arStandardMemoLine.getSmlDescription());
        details.setSmlUnitPrice(arStandardMemoLine.getSmlUnitPrice());
        details.setSmlTax(arStandardMemoLine.getSmlTax());

      } catch (FinderException ex) {

        throw new GlobalNoRecordFoundException();

      }



      try {

        arCustomer = arCustomerHome.findByCstCustomerCode(CST_CSTMR_CODE, AD_CMPNY);

        if (arCustomer.getArCustomerClass() != null) {
          String CC_NM = arCustomer.getArCustomerClass().getCcName();



          // find memo line class in null customer code

          try {
            System.out.println("CST CODE:" + CST_CSTMR_CODE);
            arStandardMemoLineClass =
                arStandardMemoLineClassHome.findSmcByCcNameSmlNameCstCstmrCodeAdBrnch(CC_NM, SML_NM,
                    CST_CSTMR_CODE, AD_BRNCH, AD_CMPNY);
            details.setSmlDescription(arStandardMemoLineClass.getSmcStandardMemoLineDescription());
            details.setSmlUnitPrice(arStandardMemoLineClass.getSmcUnitPrice());


          } catch (FinderException ex) {
            System.out.println("no memo line class with cst code");
            // find memo line class in not null customre code
            arStandardMemoLineClass = arStandardMemoLineClassHome
                .findSmcByCcNameSmlNameAdBrnch(CC_NM, SML_NM, AD_BRNCH, AD_CMPNY);
            details.setSmlDescription(arStandardMemoLineClass.getSmcStandardMemoLineDescription());
            details.setSmlUnitPrice(arStandardMemoLineClass.getSmcUnitPrice());
          }

        }
      } catch (FinderException ex) {

        System.out.println("no smc found");
        // no smc exist

      }


      return details;

    } catch (GlobalNoRecordFoundException ex) {

      throw ex;

    } catch (Exception ex) {

      Debug.printStackTrace(ex);
      throw new EJBException(ex.getMessage());

    }



  }



  /**
   * @ejb:interface-method view-type="remote"
   * @jboss:method-attributes read-only="true"
   **/
  public com.util.ArStandardMemoLineDetails getArSmlBySmlName(
      String SML_NM, Integer AD_CMPNY
  ) throws GlobalNoRecordFoundException {

    Debug.print("ArInvoiceEntryControllerBean getArSmlBySmlName");

    LocalArStandardMemoLineHome arStandardMemoLineHome = null;

    // Initialize EJB Home

    try {

      arStandardMemoLineHome = (LocalArStandardMemoLineHome) EJBHomeFactory.lookUpLocalHome(
          LocalArStandardMemoLineHome.JNDI_NAME, LocalArStandardMemoLineHome.class);

    } catch (NamingException ex) {

      throw new EJBException(ex.getMessage());

    }

    try {

      LocalArStandardMemoLine arStandardMemoLine = null;


      try {

        arStandardMemoLine = arStandardMemoLineHome.findBySmlName(SML_NM, AD_CMPNY);

      } catch (FinderException ex) {

        throw new GlobalNoRecordFoundException();

      }

      ArStandardMemoLineDetails details = new ArStandardMemoLineDetails();
      details.setSmlDescription(arStandardMemoLine.getSmlDescription());
      details.setSmlUnitPrice(arStandardMemoLine.getSmlUnitPrice());
      details.setSmlTax(arStandardMemoLine.getSmlTax());

      return details;

    } catch (GlobalNoRecordFoundException ex) {

      throw ex;

    } catch (Exception ex) {

      Debug.printStackTrace(ex);
      throw new EJBException(ex.getMessage());

    }

  }

  /**
   * @ejb:interface-method view-type="remote"
   * @jboss:method-attributes read-only="true"
   **/
  public ArModSalesOrderDetails getArSoBySoDocumentNumberAndCstCustomerCodeAndAdBranch(
      String SO_DCMNT_NMBR, String CST_CSTMR_CODE, Integer AD_BRNCH, Integer AD_CMPNY
  ) throws GlobalNoRecordFoundException, ArINVNoSalesOrderLinesFoundException {

    Debug.print(
        "ArInvoiceEntryControllerBean getArSoBySoDocumentNumberAndCstCustomerCodeAndAdBranch");

    LocalArSalesOrderHome arSalesOrderHome = null;
    LocalArSalesOrderLineHome arSalesOrderLineHome = null;
    LocalArSalesOrderInvoiceLineHome arSalesOrderInvoiceLineHome = null;
    ArrayList list = new ArrayList();

    // Initialize EJB Home

    try {

      arSalesOrderHome = (LocalArSalesOrderHome) EJBHomeFactory
          .lookUpLocalHome(LocalArSalesOrderHome.JNDI_NAME, LocalArSalesOrderHome.class);
      arSalesOrderLineHome = (LocalArSalesOrderLineHome) EJBHomeFactory
          .lookUpLocalHome(LocalArSalesOrderLineHome.JNDI_NAME, LocalArSalesOrderLineHome.class);
      arSalesOrderInvoiceLineHome =
          (LocalArSalesOrderInvoiceLineHome) EJBHomeFactory.lookUpLocalHome(
              LocalArSalesOrderInvoiceLineHome.JNDI_NAME, LocalArSalesOrderInvoiceLineHome.class);

    } catch (NamingException ex) {

      throw new EJBException(ex.getMessage());
    }

    LocalArSalesOrder arSalesOrder = null;

    try {

      if (CST_CSTMR_CODE == null || CST_CSTMR_CODE.equals("")) {
        arSalesOrder =
            arSalesOrderHome.findBySoDocumentNumberAndBrCode(SO_DCMNT_NMBR, AD_BRNCH, AD_CMPNY);

      } else {
        arSalesOrder = arSalesOrderHome.findBySoDocumentNumberAndCstCustomerCodeAndBrCode(
            SO_DCMNT_NMBR, CST_CSTMR_CODE, AD_BRNCH, AD_CMPNY);
      }

      if (arSalesOrder.getSoPosted() != EJBCommon.TRUE) {

        throw new GlobalNoRecordFoundException();

      }

      // get sales order line

      Collection arSalesOrderLines = arSalesOrder.getArSalesOrderLines();

      Iterator i = arSalesOrderLines.iterator();

      while (i.hasNext()) {
        LocalArSalesOrderLine arSalesOrderLine = (LocalArSalesOrderLine) i.next();

        ArModSalesOrderLineDetails solDetails = new ArModSalesOrderLineDetails();

        solDetails.setSolCode(arSalesOrderLine.getSolCode());
        solDetails.setSolLine(arSalesOrderLine.getSolLine());
        solDetails.setSolUnitPrice(arSalesOrderLine.getSolUnitPrice());
        solDetails.setSolIiName(arSalesOrderLine.getInvItemLocation().getInvItem().getIiName());
        solDetails
            .setSolLocName(arSalesOrderLine.getInvItemLocation().getInvLocation().getLocName());
        solDetails.setSolUomName(arSalesOrderLine.getInvUnitOfMeasure().getUomName());
        // solDetails.setSolIiDescription(arSalesOrderLine.getInvItemLocation().getInvItem().getIiDescription());
        solDetails.setSolIiDescription(arSalesOrderLine.getSolLineIDesc());

        Iterator j = arSalesOrderLine.getArSalesOrderInvoiceLines().iterator();
        double QUANTITY_SOLD = 0d;
        double TOTAL_SOLD_AMNT = 0d;
        double TOTAL_DSCNT_AMNT = 0d;

        // get qty sold
        while (j.hasNext()) {
          LocalArSalesOrderInvoiceLine arSalesOrderInvoiceLine =
              (LocalArSalesOrderInvoiceLine) j.next();

          if (arSalesOrderInvoiceLine.getArInvoice().getInvPosted() == EJBCommon.TRUE) {
            QUANTITY_SOLD += arSalesOrderInvoiceLine.getSilQuantityDelivered();
            if (arSalesOrder.getArTaxCode().getTcType().equals("INCLUSIVE")) {
              TOTAL_SOLD_AMNT += arSalesOrderInvoiceLine.getSilAmount()
                  + arSalesOrderInvoiceLine.getSilTaxAmount();
            } else {
              TOTAL_SOLD_AMNT += arSalesOrderInvoiceLine.getSilAmount();
            }

            TOTAL_DSCNT_AMNT += arSalesOrderInvoiceLine.getSilTotalDiscount();


          }

        }

        // total qty - qty sold

        double TOTAL_REMAINING_QTY = arSalesOrderLine.getSolQuantity() - QUANTITY_SOLD;
        double TOTAL_REMAINING_AMNT = arSalesOrderLine.getSolAmount() - TOTAL_SOLD_AMNT;
        double TOTAL_REMAINING_DSCNT_AMNT =
            arSalesOrderLine.getSolTotalDiscount() - TOTAL_DSCNT_AMNT;

        if (TOTAL_REMAINING_QTY == 0)
          continue;

        solDetails.setSolRemaining(TOTAL_REMAINING_QTY);
        solDetails.setSolQuantity(TOTAL_REMAINING_QTY);
        solDetails.setSolIssue(false);
        solDetails.setSolDiscount1(arSalesOrderLine.getSolDiscount1());
        solDetails.setSolDiscount2(arSalesOrderLine.getSolDiscount2());
        solDetails.setSolDiscount3(arSalesOrderLine.getSolDiscount3());
        solDetails.setSolDiscount4(arSalesOrderLine.getSolDiscount4());
        solDetails.setSolTotalDiscount(TOTAL_REMAINING_DSCNT_AMNT);
        solDetails.setSolMisc(arSalesOrderLine.getSolMisc());
        solDetails.setSolTax(arSalesOrderLine.getSolTax());
        solDetails.setSolAmount(TOTAL_REMAINING_AMNT);

        solDetails
            .setTraceMisc(arSalesOrderLine.getInvItemLocation().getInvItem().getIiTraceMisc());



        if (solDetails.getTraceMisc() == 1) {
          ArrayList tagList = this.getInvTagList(arSalesOrderLine);

          solDetails.setSolTagList(tagList);
        }



        list.add(solDetails);

      }
      if (list == null || list.size() == 0) {

        throw new ArINVNoSalesOrderLinesFoundException();

      }
      ArModSalesOrderDetails mdetails = new ArModSalesOrderDetails();

      mdetails.setSoCode(arSalesOrder.getSoCode());
      mdetails.setSoMemo(arSalesOrder.getSoMemo());
      mdetails.setSoTcName(arSalesOrder.getArTaxCode().getTcName());
      mdetails.setSoReferenceNumber(arSalesOrder.getSoReferenceNumber());
      mdetails.setSoTcType(arSalesOrder.getArTaxCode().getTcType());
      mdetails.setSoTcRate(arSalesOrder.getArTaxCode().getTcRate());
      mdetails.setSoFcName(arSalesOrder.getGlFunctionalCurrency().getFcName());
      mdetails.setSoPytName(arSalesOrder.getAdPaymentTerm().getPytName());
      mdetails.setSoSlpSalespersonCode(arSalesOrder.getArSalesperson() != null
          ? arSalesOrder.getArSalesperson().getSlpSalespersonCode()
          : null);
      mdetails.setSoConversionDate(arSalesOrder.getSoConversionDate());
      mdetails.setSoConversionRate(arSalesOrder.getSoConversionRate());
      mdetails.setSoShipTo(arSalesOrder.getSoShipTo());
      mdetails.setSoBillTo(arSalesOrder.getSoBillTo());
      if (CST_CSTMR_CODE == null || CST_CSTMR_CODE.equals("")) {
        mdetails.setSoCstCustomerCode(arSalesOrder.getArCustomer().getCstCustomerCode());
        mdetails.setSoCstName(arSalesOrder.getArCustomer().getCstName());
      }
      mdetails.setSoSolList(list);
      mdetails.setReportParameter(arSalesOrder.getReportParameter());
      return mdetails;

    } catch (FinderException ex) {

      throw new GlobalNoRecordFoundException();

    } catch (GlobalNoRecordFoundException ex) {

      getSessionContext().setRollbackOnly();
      throw ex;

    } catch (ArINVNoSalesOrderLinesFoundException ex) {

      getSessionContext().setRollbackOnly();
      throw ex;

    } catch (Exception ex) {

      Debug.printStackTrace(ex);
      throw new EJBException(ex.getMessage());

    }

  }



  /**
   * @ejb:interface-method view-type="remote"
   * @jboss:method-attributes read-only="true"
   **/
  public ArModJobOrderDetails getArJoByJoDocumentNumberAndCstCustomerCodeAndAdBranch(
      String JO_DCMNT_NMBR, String CST_CSTMR_CODE, Integer AD_BRNCH, Integer AD_CMPNY
  ) throws GlobalNoRecordFoundException, ArINVNoSalesOrderLinesFoundException {

    Debug.print(
        "ArInvoiceEntryControllerBean getArJoByJoDocumentNumberAndCstCustomerCodeAndAdBranch");

    LocalArJobOrderHome arJobOrderHome = null;
    LocalArJobOrderLineHome arJobOrderLineHome = null;
    LocalArJobOrderInvoiceLineHome arJobOrderInvoiceLineHome = null;
    ArrayList list = new ArrayList();

    // Initialize EJB Home

    try {

      arJobOrderHome = (LocalArJobOrderHome) EJBHomeFactory
          .lookUpLocalHome(LocalArJobOrderHome.JNDI_NAME, LocalArJobOrderHome.class);
      arJobOrderLineHome = (LocalArJobOrderLineHome) EJBHomeFactory
          .lookUpLocalHome(LocalArJobOrderLineHome.JNDI_NAME, LocalArJobOrderLineHome.class);
      arJobOrderInvoiceLineHome = (LocalArJobOrderInvoiceLineHome) EJBHomeFactory.lookUpLocalHome(
          LocalArJobOrderInvoiceLineHome.JNDI_NAME, LocalArJobOrderInvoiceLineHome.class);

    } catch (NamingException ex) {

      throw new EJBException(ex.getMessage());
    }

    LocalArJobOrder arJobOrder = null;

    try {

      if (CST_CSTMR_CODE == null || CST_CSTMR_CODE.equals("")) {
        arJobOrder =
            arJobOrderHome.findByJoDocumentNumberAndBrCode(JO_DCMNT_NMBR, AD_BRNCH, AD_CMPNY);

      } else {
        arJobOrder = arJobOrderHome.findByJoDocumentNumberAndCstCustomerCodeAndBrCode(JO_DCMNT_NMBR,
            CST_CSTMR_CODE, AD_BRNCH, AD_CMPNY);
      }

      if (arJobOrder.getJoPosted() != EJBCommon.TRUE) {

        throw new GlobalNoRecordFoundException();

      }

      // get sales order line

      Collection arJobOrderLines = arJobOrder.getArJobOrderLines();

      Iterator i = arJobOrderLines.iterator();

      while (i.hasNext()) {
        LocalArJobOrderLine arJobOrderLine = (LocalArJobOrderLine) i.next();

        ArModJobOrderLineDetails jolDetails = new ArModJobOrderLineDetails();

        jolDetails.setJolCode(arJobOrderLine.getJolCode());
        jolDetails.setJolLine(arJobOrderLine.getJolLine());
        jolDetails.setJolUnitPrice(arJobOrderLine.getJolUnitPrice());
        jolDetails.setJolIiName(arJobOrderLine.getInvItemLocation().getInvItem().getIiName());
        jolDetails.setJolLocName(arJobOrderLine.getInvItemLocation().getInvLocation().getLocName());
        jolDetails.setJolUomName(arJobOrderLine.getInvUnitOfMeasure().getUomName());
        // solDetails.setJolIiDescription(arSalesOrderLine.getInvItemLocation().getInvItem().getIiDescription());
        jolDetails.setJolIiDescription(arJobOrderLine.getJolLineIDesc());

        Iterator j = arJobOrderLine.getArJobOrderInvoiceLines().iterator();
        double QUANTITY_SOLD = 0d;
        double TOTAL_SOLD_AMNT = 0d;
        double TOTAL_DSCNT_AMNT = 0d;

        // get qty sold
        while (j.hasNext()) {
          LocalArJobOrderInvoiceLine arJobOrderInvoiceLine = (LocalArJobOrderInvoiceLine) j.next();

          if (arJobOrderInvoiceLine.getArInvoice().getInvPosted() == EJBCommon.TRUE) {
            QUANTITY_SOLD += arJobOrderInvoiceLine.getJilQuantityDelivered();
            if (arJobOrder.getArTaxCode().getTcType().equals("INCLUSIVE")) {
              TOTAL_SOLD_AMNT +=
                  arJobOrderInvoiceLine.getJilAmount() + arJobOrderInvoiceLine.getJilTaxAmount();
            } else {
              TOTAL_SOLD_AMNT += arJobOrderInvoiceLine.getJilAmount();
            }

            TOTAL_DSCNT_AMNT += arJobOrderInvoiceLine.getJilTotalDiscount();


          }

        }

        // total qty - qty sold

        double TOTAL_REMAINING_QTY = arJobOrderLine.getJolQuantity() - QUANTITY_SOLD;
        double TOTAL_REMAINING_AMNT = arJobOrderLine.getJolAmount() - TOTAL_SOLD_AMNT;
        double TOTAL_REMAINING_DSCNT_AMNT = arJobOrderLine.getJolTotalDiscount() - TOTAL_DSCNT_AMNT;

        if (TOTAL_REMAINING_QTY == 0)
          continue;


        jolDetails.setJolRemaining(TOTAL_REMAINING_QTY);
        jolDetails.setJolQuantity(TOTAL_REMAINING_QTY);
        jolDetails.setJolIssue(false);
        jolDetails.setJolDiscount1(arJobOrderLine.getJolDiscount1());
        jolDetails.setJolDiscount2(arJobOrderLine.getJolDiscount2());
        jolDetails.setJolDiscount3(arJobOrderLine.getJolDiscount3());
        jolDetails.setJolDiscount4(arJobOrderLine.getJolDiscount4());
        jolDetails.setJolTotalDiscount(TOTAL_REMAINING_DSCNT_AMNT);
        jolDetails.setJolMisc(arJobOrderLine.getJolMisc());
        jolDetails.setJolTax(arJobOrderLine.getJolTax());
        jolDetails.setJolAmount(TOTAL_REMAINING_AMNT);

        jolDetails.setTraceMisc(arJobOrderLine.getInvItemLocation().getInvItem().getIiTraceMisc());



        if (jolDetails.getTraceMisc() == 1) {
          ArrayList tagList = this.getInvTagList(arJobOrderLine);

          jolDetails.setJolTagList(tagList);
        }



        list.add(jolDetails);

      }
      if (list == null || list.size() == 0) {

        throw new ArINVNoSalesOrderLinesFoundException();

      }
      ArModJobOrderDetails mdetails = new ArModJobOrderDetails();

      mdetails.setJoCode(arJobOrder.getJoCode());
      mdetails.setJoMemo(arJobOrder.getJoMemo());
      mdetails.setJoTcName(arJobOrder.getArTaxCode().getTcName());
      mdetails.setJoReferenceNumber(arJobOrder.getJoReferenceNumber());
      mdetails.setJoTcType(arJobOrder.getArTaxCode().getTcType());
      mdetails.setJoTcRate(arJobOrder.getArTaxCode().getTcRate());
      mdetails.setJoFcName(arJobOrder.getGlFunctionalCurrency().getFcName());
      mdetails.setJoPytName(arJobOrder.getAdPaymentTerm().getPytName());
      mdetails.setJoSlpSalespersonCode(arJobOrder.getArSalesperson() != null
          ? arJobOrder.getArSalesperson().getSlpSalespersonCode()
          : null);
      mdetails.setJoConversionDate(arJobOrder.getJoConversionDate());
      mdetails.setJoConversionRate(arJobOrder.getJoConversionRate());
      mdetails.setJoShipTo(arJobOrder.getJoShipTo());
      mdetails.setJoBillTo(arJobOrder.getJoBillTo());
      if (CST_CSTMR_CODE == null || CST_CSTMR_CODE.equals("")) {
        mdetails.setJoCstCustomerCode(arJobOrder.getArCustomer().getCstCustomerCode());
        mdetails.setJoCstName(arJobOrder.getArCustomer().getCstName());
      }
      mdetails.setJoJolList(list);

      return mdetails;

    } catch (FinderException ex) {

      throw new GlobalNoRecordFoundException();

    } catch (GlobalNoRecordFoundException ex) {

      getSessionContext().setRollbackOnly();
      throw ex;

    } catch (ArINVNoSalesOrderLinesFoundException ex) {

      getSessionContext().setRollbackOnly();
      throw ex;

    } catch (Exception ex) {

      Debug.printStackTrace(ex);
      throw new EJBException(ex.getMessage());

    }

  }



  /**
   * @ejb:interface-method view-type="remote"
   * @jboss:method-attributes read-only="true"
   **/
  public ArrayList getAdUsrAll(
      Integer AD_CMPNY
  ) {

    Debug.print("ApReceivingItemEntryControllerBean getAdUsrAll");

    LocalAdUserHome adUserHome = null;

    LocalAdUser adUser = null;

    Collection adUsers = null;

    ArrayList list = new ArrayList();

    // Initialize EJB Home

    try {

      adUserHome = (LocalAdUserHome) EJBHomeFactory.lookUpLocalHome(LocalAdUserHome.JNDI_NAME,
          LocalAdUserHome.class);

    } catch (NamingException ex) {

      throw new EJBException(ex.getMessage());

    }

    try {

      adUsers = adUserHome.findUsrAll(AD_CMPNY);

    } catch (FinderException ex) {

    } catch (Exception ex) {

      throw new EJBException(ex.getMessage());
    }

    if (adUsers.isEmpty()) {

      return null;

    }

    Iterator i = adUsers.iterator();

    while (i.hasNext()) {

      adUser = (LocalAdUser) i.next();

      list.add(adUser.getUsrName());

    }

    return list;

  }


  /**
   * @ejb:interface-method view-type="remote"
   **/
  public Integer saveArInvEntry(
      com.util.ArInvoiceDetails details, String PYT_NM, String TC_NM, String WTC_NM, String FC_NM,
      String CST_CSTMR_CODE, String IB_NM, ArrayList ilList, boolean isDraft,
      String SLP_SLSPRSN_CODE,

      String PRJ_PRJCT_CODE, String PT_PRJCT_TYP_CODE, String PP_PRJCT_PHSE_NM,
      String CT_CNTRCT_TRM_NM,

      Integer AD_BRNCH, Integer AD_CMPNY
  ) throws

  GlobalRecordAlreadyDeletedException, GlobalDocumentNumberNotUniqueException,
      GlobalConversionDateNotExistException, GlobalPaymentTermInvalidException,
      ArINVAmountExceedsCreditLimitException, GlobalTransactionAlreadyApprovedException,
      GlobalTransactionAlreadyPendingException, GlobalTransactionAlreadyPostedException,
      GlobalTransactionAlreadyVoidException, GlobalNoApprovalRequesterFoundException,
      GlobalNoApprovalApproverFoundException, GlJREffectiveDateNoPeriodExistException,
      GlJREffectiveDatePeriodClosedException, GlobalJournalNotBalanceException,
      GlobalBranchAccountNumberInvalidException, ArInvDuplicateUploadNumberException {

    Debug.print("ArInvoiceEntryControllerBean saveArInvEntry");

    LocalPmProjectHome pmProjectHome = null;
    LocalPmProjectTypeTypeHome pmProjectTypeTypeHome = null;
    LocalPmProjectPhaseHome pmProjectPhaseHome = null;
    LocalPmContractTermHome pmContractTermHome = null;

    LocalArInvoiceHome arInvoiceHome = null;
    LocalArInvoiceBatchHome arInvoiceBatchHome = null;
    LocalAdCompanyHome adCompanyHome = null;
    LocalGlFunctionalCurrencyHome glFunctionalCurrencyHome = null;
    LocalAdDocumentSequenceAssignmentHome adDocumentSequenceAssignmentHome = null;
    LocalAdBranchDocumentSequenceAssignmentHome adBranchDocumentSequenceAssignmentHome = null;
    LocalAdPaymentTermHome adPaymentTermHome = null;
    LocalAdPaymentScheduleHome adPaymentScheduleHome = null;
    LocalAdDiscountHome adDiscountHome = null;
    LocalArTaxCodeHome arTaxCodeHome = null;
    LocalArWithholdingTaxCodeHome arWithholdingTaxCodeHome = null;
    LocalArCustomerHome arCustomerHome = null;
    LocalGlFunctionalCurrencyRateHome glFunctionalCurrencyRateHome = null;
    LocalArInvoicePaymentScheduleHome arInvoicePaymentScheduleHome = null;
    LocalAdApprovalHome adApprovalHome = null;
    LocalAdAmountLimitHome adAmountLimitHome = null;
    LocalAdApprovalUserHome adApprovalUserHome = null;
    LocalAdApprovalQueueHome adApprovalQueueHome = null;
    LocalAdApprovalDocumentHome adApprovalDocumentHome = null;
    LocalAdPreferenceHome adPreferenceHome = null;
    LocalArCustomerBalanceHome arCustomerBalanceHome = null;
    LocalArReceiptHome arReceiptHome = null;
    LocalAdBranchCustomerHome adBranchCustomerHome = null;
    LocalArSalespersonHome arSalespersonHome = null;
    LocalGlSetOfBookHome glSetOfBookHome = null;
    LocalAdBranchArTaxCodeHome adBranchArTaxCodeHome = null;

    LocalArInvoice arInvoice = null;


    // Initialize EJB Home

    try {

      pmProjectHome = (LocalPmProjectHome) EJBHomeFactory
          .lookUpLocalHome(LocalPmProjectHome.JNDI_NAME, LocalPmProjectHome.class);

      pmProjectTypeTypeHome = (LocalPmProjectTypeTypeHome) EJBHomeFactory
          .lookUpLocalHome(LocalPmProjectTypeTypeHome.JNDI_NAME, LocalPmProjectTypeTypeHome.class);

      pmProjectPhaseHome = (LocalPmProjectPhaseHome) EJBHomeFactory
          .lookUpLocalHome(LocalPmProjectPhaseHome.JNDI_NAME, LocalPmProjectPhaseHome.class);

      pmContractTermHome = (LocalPmContractTermHome) EJBHomeFactory
          .lookUpLocalHome(LocalPmContractTermHome.JNDI_NAME, LocalPmContractTermHome.class);


      adBranchArTaxCodeHome = (LocalAdBranchArTaxCodeHome) EJBHomeFactory
          .lookUpLocalHome(LocalAdBranchArTaxCodeHome.JNDI_NAME, LocalAdBranchArTaxCodeHome.class);

      arInvoiceHome = (LocalArInvoiceHome) EJBHomeFactory
          .lookUpLocalHome(LocalArInvoiceHome.JNDI_NAME, LocalArInvoiceHome.class);
      arInvoiceBatchHome = (LocalArInvoiceBatchHome) EJBHomeFactory
          .lookUpLocalHome(LocalArInvoiceBatchHome.JNDI_NAME, LocalArInvoiceBatchHome.class);
      adCompanyHome = (LocalAdCompanyHome) EJBHomeFactory
          .lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
      glFunctionalCurrencyHome = (LocalGlFunctionalCurrencyHome) EJBHomeFactory.lookUpLocalHome(
          LocalGlFunctionalCurrencyHome.JNDI_NAME, LocalGlFunctionalCurrencyHome.class);
      adDocumentSequenceAssignmentHome = (LocalAdDocumentSequenceAssignmentHome) EJBHomeFactory
          .lookUpLocalHome(LocalAdDocumentSequenceAssignmentHome.JNDI_NAME,
              LocalAdDocumentSequenceAssignmentHome.class);
      adBranchDocumentSequenceAssignmentHome =
          (LocalAdBranchDocumentSequenceAssignmentHome) EJBHomeFactory.lookUpLocalHome(
              LocalAdBranchDocumentSequenceAssignmentHome.JNDI_NAME,
              LocalAdBranchDocumentSequenceAssignmentHome.class);
      adPaymentTermHome = (LocalAdPaymentTermHome) EJBHomeFactory
          .lookUpLocalHome(LocalAdPaymentTermHome.JNDI_NAME, LocalAdPaymentTermHome.class);
      adPaymentScheduleHome = (LocalAdPaymentScheduleHome) EJBHomeFactory
          .lookUpLocalHome(LocalAdPaymentScheduleHome.JNDI_NAME, LocalAdPaymentScheduleHome.class);
      adDiscountHome = (LocalAdDiscountHome) EJBHomeFactory
          .lookUpLocalHome(LocalAdDiscountHome.JNDI_NAME, LocalAdDiscountHome.class);
      arTaxCodeHome = (LocalArTaxCodeHome) EJBHomeFactory
          .lookUpLocalHome(LocalArTaxCodeHome.JNDI_NAME, LocalArTaxCodeHome.class);
      arWithholdingTaxCodeHome = (LocalArWithholdingTaxCodeHome) EJBHomeFactory.lookUpLocalHome(
          LocalArWithholdingTaxCodeHome.JNDI_NAME, LocalArWithholdingTaxCodeHome.class);
      arCustomerHome = (LocalArCustomerHome) EJBHomeFactory
          .lookUpLocalHome(LocalArCustomerHome.JNDI_NAME, LocalArCustomerHome.class);
      glFunctionalCurrencyRateHome =
          (LocalGlFunctionalCurrencyRateHome) EJBHomeFactory.lookUpLocalHome(
              LocalGlFunctionalCurrencyRateHome.JNDI_NAME, LocalGlFunctionalCurrencyRateHome.class);
      arInvoicePaymentScheduleHome =
          (LocalArInvoicePaymentScheduleHome) EJBHomeFactory.lookUpLocalHome(
              LocalArInvoicePaymentScheduleHome.JNDI_NAME, LocalArInvoicePaymentScheduleHome.class);
      adApprovalHome = (LocalAdApprovalHome) EJBHomeFactory
          .lookUpLocalHome(LocalAdApprovalHome.JNDI_NAME, LocalAdApprovalHome.class);
      adAmountLimitHome = (LocalAdAmountLimitHome) EJBHomeFactory
          .lookUpLocalHome(LocalAdAmountLimitHome.JNDI_NAME, LocalAdAmountLimitHome.class);
      adApprovalUserHome = (LocalAdApprovalUserHome) EJBHomeFactory
          .lookUpLocalHome(LocalAdApprovalUserHome.JNDI_NAME, LocalAdApprovalUserHome.class);
      adApprovalQueueHome = (LocalAdApprovalQueueHome) EJBHomeFactory
          .lookUpLocalHome(LocalAdApprovalQueueHome.JNDI_NAME, LocalAdApprovalQueueHome.class);
      adApprovalDocumentHome = (LocalAdApprovalDocumentHome) EJBHomeFactory.lookUpLocalHome(
          LocalAdApprovalDocumentHome.JNDI_NAME, LocalAdApprovalDocumentHome.class);
      adPreferenceHome = (LocalAdPreferenceHome) EJBHomeFactory
          .lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);
      arCustomerBalanceHome = (LocalArCustomerBalanceHome) EJBHomeFactory
          .lookUpLocalHome(LocalArCustomerBalanceHome.JNDI_NAME, LocalArCustomerBalanceHome.class);
      arReceiptHome = (LocalArReceiptHome) EJBHomeFactory
          .lookUpLocalHome(LocalArReceiptHome.JNDI_NAME, LocalArReceiptHome.class);
      adBranchCustomerHome = (LocalAdBranchCustomerHome) EJBHomeFactory
          .lookUpLocalHome(LocalAdBranchCustomerHome.JNDI_NAME, LocalAdBranchCustomerHome.class);
      arSalespersonHome = (LocalArSalespersonHome) EJBHomeFactory
          .lookUpLocalHome(LocalArSalespersonHome.JNDI_NAME, LocalArSalespersonHome.class);
      glSetOfBookHome = (LocalGlSetOfBookHome) EJBHomeFactory
          .lookUpLocalHome(LocalGlSetOfBookHome.JNDI_NAME, LocalGlSetOfBookHome.class);

    } catch (NamingException ex) {

      throw new EJBException(ex.getMessage());

    }


    try {

      // validate if invoice is already deleted

      try {

        if (details.getInvCode() != null) {

          arInvoice = arInvoiceHome.findByPrimaryKey(details.getInvCode());

        }

      } catch (FinderException ex) {

        throw new GlobalRecordAlreadyDeletedException();

      }

      // validate if invoice is already posted, void, approved or pending

      if (details.getInvCode() != null) {

        if (arInvoice.getInvApprovalStatus() != null) {

          if (arInvoice.getInvApprovalStatus().equals("APPROVED")
              || arInvoice.getInvApprovalStatus().equals("N/A")) {

            throw new GlobalTransactionAlreadyApprovedException();


          } else if (arInvoice.getInvApprovalStatus().equals("PENDING")) {

            throw new GlobalTransactionAlreadyPendingException();

          }

        }

        if (arInvoice.getInvPosted() == EJBCommon.TRUE) {

          throw new GlobalTransactionAlreadyPostedException();

        } else if (arInvoice.getInvVoid() == EJBCommon.TRUE) {

          throw new GlobalTransactionAlreadyVoidException();

        }

      }

      // invoice void

      if (details.getInvCode() != null && details.getInvVoid() == EJBCommon.TRUE
          && arInvoice.getInvPosted() == EJBCommon.FALSE) {

        arInvoice.setInvVoid(EJBCommon.TRUE);
        arInvoice.setInvLastModifiedBy(details.getInvLastModifiedBy());
        arInvoice.setInvDateLastModified(details.getInvDateLastModified());

        return arInvoice.getInvCode();

      }

      // validate if document number is unique document number is automatic then set next sequence

      LocalAdBranchDocumentSequenceAssignment adBranchDocumentSequenceAssignment = null;
      LocalAdDocumentSequenceAssignment adDocumentSequenceAssignment = null;
      Debug.print("ArInvoiceEntryControllerBean saveArInvEntry A");
      if (details.getInvCode() == null) {


        String documentType = details.getInvDocumentType();

        try {
          if (documentType != null) {
            adDocumentSequenceAssignment =
                adDocumentSequenceAssignmentHome.findByDcName(documentType, AD_CMPNY);
          } else {
            documentType = "AR INVOICE";
          }
        } catch (FinderException ex) {
          documentType = "AR INVOICE";
        }



        try {

          adDocumentSequenceAssignment =
              adDocumentSequenceAssignmentHome.findByDcName(documentType, AD_CMPNY);

        } catch (FinderException ex) {

        }

        try {
          System.out.println("adDocumentSequenceAssignment.getDsaCode()= "
              + adDocumentSequenceAssignment.getDsaCode());
          adBranchDocumentSequenceAssignment =
              adBranchDocumentSequenceAssignmentHome.findBdsByDsaCodeAndBrCode(
                  adDocumentSequenceAssignment.getDsaCode(), AD_BRNCH, AD_CMPNY);

        } catch (FinderException ex) {

        }

        LocalArInvoice arExistingInvoice = null;

        try {
          System.out.println("the inv num:" + details.getInvNumber());
          arExistingInvoice = arInvoiceHome.findByInvNumberAndInvCreditMemoAndBrCode(
              details.getInvNumber(), EJBCommon.FALSE, AD_BRNCH, AD_CMPNY);

        } catch (FinderException ex) {
        }

        if (arExistingInvoice != null) {

          throw new GlobalDocumentNumberNotUniqueException();

        }
        /*
         * if (adDocumentSequenceAssignment.getAdDocumentSequence().getDsNumberingType() == 'A' &&
         * (details.getInvNumber() == null || details.getInvNumber().trim().length() == 0)) {
         * 
         * while (true) {
         * 
         * if (adBranchDocumentSequenceAssignment == null ||
         * adBranchDocumentSequenceAssignment.getBdsNextSequence() == null) {
         * 
         * try {
         * 
         * arInvoiceHome.findByInvNumberAndInvCreditMemoAndBrCode(adDocumentSequenceAssignment.
         * getDsaNextSequence(), EJBCommon.FALSE, AD_BRNCH, AD_CMPNY);
         * adDocumentSequenceAssignment.setDsaNextSequence(EJBCommon.incrementStringNumber(
         * adDocumentSequenceAssignment.getDsaNextSequence()));
         * 
         * } catch (FinderException ex) {
         * 
         * details.setInvNumber(adDocumentSequenceAssignment.getDsaNextSequence());
         * adDocumentSequenceAssignment.setDsaNextSequence(EJBCommon.incrementStringNumber(
         * adDocumentSequenceAssignment.getDsaNextSequence())); break;
         * 
         * }
         * 
         * } else {
         * 
         * try {
         * 
         * arInvoiceHome.findByInvNumberAndInvCreditMemoAndBrCode(adBranchDocumentSequenceAssignment
         * .getBdsNextSequence(), EJBCommon.FALSE, AD_BRNCH, AD_CMPNY);
         * adBranchDocumentSequenceAssignment.setBdsNextSequence(EJBCommon.incrementStringNumber(
         * adBranchDocumentSequenceAssignment.getBdsNextSequence()));
         * 
         * } catch (FinderException ex) {
         * 
         * details.setInvNumber(adBranchDocumentSequenceAssignment.getBdsNextSequence());
         * adBranchDocumentSequenceAssignment.setBdsNextSequence(EJBCommon.incrementStringNumber(
         * adBranchDocumentSequenceAssignment.getBdsNextSequence())); break;
         * 
         * }
         * 
         * }
         * 
         * }
         * 
         * }
         */
        // Confirm if Upload Number have duplicate

        if (details.getInvUploadNumber() != null) {
          if (!details.getInvUploadNumber().equals("")) {
            try {

              arInvoiceHome.findByUploadNumberAndCompanyCode(details.getInvUploadNumber(),
                  AD_CMPNY);


              // throw exception if found duplicate upload number
              throw new ArInvDuplicateUploadNumberException();


            } catch (FinderException ex) {

            }
          }



        }



      } else {

        LocalArInvoice arExistingInvoice = null;

        try {

          arExistingInvoice = arInvoiceHome.findByInvNumberAndInvCreditMemoAndBrCode(
              details.getInvNumber(), EJBCommon.FALSE, AD_BRNCH, AD_CMPNY);

        } catch (FinderException ex) {
        }

        if (arExistingInvoice != null
            && !arExistingInvoice.getInvCode().equals(details.getInvCode())) {

          throw new GlobalDocumentNumberNotUniqueException();

        }

        if (arInvoice.getInvNumber() != details.getInvNumber()
            && (details.getInvNumber() == null || details.getInvNumber().trim().length() == 0)) {

          details.setInvNumber(arInvoice.getInvNumber());

        }

        // Confirm if Upload Number have duplicate

        if (details.getInvUploadNumber() != null) {
          if (!details.getInvUploadNumber().trim().equals("")) {
            if (!details.getInvUploadNumber().equals(arExistingInvoice.getInvUploadNumber())) {
              try {
                arInvoiceHome.findByUploadNumberAndCompanyCode(details.getInvUploadNumber(),
                    AD_CMPNY);


                // throw exception if found duplicate upload number
                throw new ArInvDuplicateUploadNumberException();


              } catch (FinderException ex) {

              }
            }
          }


        }

      }
      Debug.print("ArInvoiceEntryControllerBean saveArInvEntry A Done");

      // validate if conversion date exists

      try {

        if (details.getInvConversionDate() != null) {

          LocalGlFunctionalCurrency glValidateFunctionalCurrency =
              glFunctionalCurrencyHome.findByFcName(FC_NM, AD_CMPNY);
          LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);

          if (!glValidateFunctionalCurrency.getFcName().equals("USD")) {

            LocalGlFunctionalCurrencyRate glFunctionalCurrencyRate = glFunctionalCurrencyRateHome
                .findByFcCodeAndDate(glValidateFunctionalCurrency.getFcCode(),
                    details.getInvConversionDate(), AD_CMPNY);

          } else if (!adCompany.getGlFunctionalCurrency().getFcName().equals("USD")) {

            LocalGlFunctionalCurrencyRate glFunctionalCurrencyRate = glFunctionalCurrencyRateHome
                .findByFcCodeAndDate(adCompany.getGlFunctionalCurrency().getFcCode(),
                    details.getInvConversionDate(), AD_CMPNY);

          }

        }

      } catch (FinderException ex) {

        throw new GlobalConversionDateNotExistException();

      }

      // validate if payment term has at least one payment schedule

      if (adPaymentTermHome.findByPytName(PYT_NM, AD_CMPNY).getAdPaymentSchedules().isEmpty()) {

        throw new GlobalPaymentTermInvalidException();

      }



      // used in checking if invoice should re-generate distribution records and re-calculate taxes
      boolean isRecalculate = true;

      // create invoice
      Debug.print("ArInvoiceEntryControllerBean saveArInvEntry B");


      Calendar calendar = new GregorianCalendar();
      // passing month-1 because 0-->jan, 1-->feb... 11-->dec
      calendar.setTime(details.getInvDate());
      calendar.set(Calendar.DATE, calendar.getActualMaximum(Calendar.DATE));
      calendar.add(Calendar.DAY_OF_MONTH, 1);

      details.setInvInterestNextRunDate(calendar.getTime());
      // details.setInvInterestLastRunDate();



      if (details.getInvCode() == null) {
        System.out.println("CHECKING A");

        arInvoice = arInvoiceHome.create(details.getInvType(), EJBCommon.FALSE,
            details.getInvDescription(), details.getInvDate(), details.getInvNumber(),
            details.getInvReferenceNumber(), details.getInvUploadNumber(), null, null, 0d, 0d, 0d,
            0d, 0d, 0d, details.getInvConversionDate(), details.getInvConversionRate(),
            details.getInvMemo(), details.getInvPreviousReading(), details.getInvPresentReading(),
            details.getInvBillToAddress(), details.getInvBillToContact(),
            details.getInvBillToAltContact(), details.getInvBillToPhone(),
            details.getInvBillingHeader(), details.getInvBillingFooter(),
            details.getInvBillingHeader2(), details.getInvBillingFooter2(),
            details.getInvBillingHeader3(), details.getInvBillingFooter3(),
            details.getInvBillingSignatory(), details.getInvSignatoryTitle(),
            details.getInvShipToAddress(), details.getInvShipToContact(),
            details.getInvShipToAltContact(), details.getInvShipToPhone(), details.getInvShipDate(),
            details.getInvLvFreight(), null, null, EJBCommon.FALSE, null, EJBCommon.FALSE,
            EJBCommon.FALSE, EJBCommon.FALSE, details.getInvDisableInterest(),
            details.getInvInterest(), "", 0d, null, null,
            details.getInvInterestNextRunDate()/* next run date */,
            details.getInvInterestLastRunDate(), details.getInvCreatedBy(),
            details.getInvDateCreated(), details.getInvLastModifiedBy(),
            details.getInvDateLastModified(), null, null, null, null, EJBCommon.FALSE, null, null,
            null, details.getInvDebitMemo(), details.getInvSubjectToCommission(), null,
            details.getInvEffectivityDate(), AD_BRNCH, AD_CMPNY);

      } else {


        // check if critical fields are changed

        if (!arInvoice.getArTaxCode().getTcName().equals(TC_NM)
            || !arInvoice.getArWithholdingTaxCode().getWtcName().equals(WTC_NM)
            || !arInvoice.getArCustomer().getCstCustomerCode().equals(CST_CSTMR_CODE)
            || !arInvoice.getAdPaymentTerm().getPytName().equals(PYT_NM)
            || !arInvoice.getInvDate().equals(details.getInvDate())
            || !arInvoice.getInvEffectivityDate().equals(details.getInvEffectivityDate())
            || ilList.size() != arInvoice.getArInvoiceLines().size()) {

          isRecalculate = true;

        } else if (ilList.size() == arInvoice.getArInvoiceLines().size()) {
          System.out.println("CHECK");
          Iterator ilIter = arInvoice.getArInvoiceLines().iterator();
          Iterator ilListIter = ilList.iterator();

          while (ilIter.hasNext()) {

            LocalArInvoiceLine arInvoiceLine = (LocalArInvoiceLine) ilIter.next();
            ArModInvoiceLineDetails mdetails = (ArModInvoiceLineDetails) ilListIter.next();
            if (!arInvoiceLine.getArStandardMemoLine().getSmlName()
                .equals(mdetails.getIlSmlName())) {
              System.out.println("SML");
            }

            if (arInvoiceLine.getIlQuantity() != mdetails.getIlQuantity()) {
              System.out.println("QTY");
            }

            if (arInvoiceLine.getIlUnitPrice() != mdetails.getIlUnitPrice()) {
              System.out.println("UPRICE");
            }

            if (arInvoiceLine.getIlTax() != mdetails.getIlTax()) {
              System.out.println("TAX");
            }

            if (!arInvoiceLine.getArStandardMemoLine().getSmlName().equals(mdetails.getIlSmlName())
                || arInvoiceLine.getIlQuantity() != mdetails.getIlQuantity()
                || arInvoiceLine.getIlUnitPrice() != mdetails.getIlUnitPrice()
                || arInvoiceLine.getIlTax() != mdetails.getIlTax()) {
              System.out.println("NO!");
              isRecalculate = true;
              break;

            }

            arInvoiceLine.setIlDescription(mdetails.getIlDescription());

            isRecalculate = false;

          }

        } else {

          isRecalculate = false;

        }
        Debug.print("ArInvoiceEntryControllerBean saveArInvEntry B Done");

        arInvoice.setInvType(details.getInvType());
        arInvoice.setInvDocumentType(details.getInvDocumentType());
        arInvoice.setInvDescription(details.getInvDescription());
        arInvoice.setInvMemo(details.getInvMemo());
        arInvoice.setInvDate(details.getInvDate());
        arInvoice.setInvNumber(details.getInvNumber());
        arInvoice.setInvReferenceNumber(details.getInvReferenceNumber());
        arInvoice.setInvUploadNumber(details.getInvUploadNumber());
        arInvoice.setInvConversionDate(details.getInvConversionDate());
        arInvoice.setInvConversionRate(details.getInvConversionRate());
        arInvoice.setInvPreviousReading(arInvoice.getInvPreviousReading());
        arInvoice.setInvPresentReading(arInvoice.getInvPresentReading());
        arInvoice.setInvBillToAddress(details.getInvBillToAddress());
        arInvoice.setInvBillToContact(details.getInvBillToContact());
        arInvoice.setInvBillToAltContact(details.getInvBillToAltContact());
        arInvoice.setInvBillToPhone(details.getInvBillToPhone());
        arInvoice.setInvBillingHeader(details.getInvBillingHeader());
        arInvoice.setInvBillingFooter(details.getInvBillingFooter());
        arInvoice.setInvBillingHeader2(details.getInvBillingHeader2());
        arInvoice.setInvBillingFooter2(details.getInvBillingFooter2());
        arInvoice.setInvBillingHeader3(details.getInvBillingHeader3());
        arInvoice.setInvBillingFooter3(details.getInvBillingFooter3());
        arInvoice.setInvBillingSignatory(details.getInvBillingSignatory());
        arInvoice.setInvSignatoryTitle(details.getInvSignatoryTitle());
        arInvoice.setInvShipToAddress(details.getInvShipToAddress());
        arInvoice.setInvShipToContact(details.getInvShipToContact());
        arInvoice.setInvShipToAltContact(details.getInvShipToAltContact());
        arInvoice.setInvShipToPhone(details.getInvShipToPhone());
        arInvoice.setInvShipDate(details.getInvShipDate());
        arInvoice.setInvLvFreight(details.getInvLvFreight());

        arInvoice.setInvLastModifiedBy(details.getInvLastModifiedBy());
        arInvoice.setInvDateLastModified(details.getInvDateLastModified());
        arInvoice.setInvReasonForRejection(null);
        arInvoice.setInvDebitMemo(details.getInvDebitMemo());
        arInvoice.setInvSubjectToCommission(details.getInvSubjectToCommission());
        arInvoice.setInvEffectivityDate(details.getInvEffectivityDate());
        arInvoice.setInvRecieveDate(details.getInvRecieveDate());
        arInvoice.setInvDisableInterest(details.getInvDisableInterest());
        arInvoice.setInvInterest(details.getInvInterest());
        arInvoice.setInvInterestNextRunDate(details.getInvInterestNextRunDate());
        arInvoice.setInvInterestLastRunDate(details.getInvInterestLastRunDate());

      }

      arInvoice.setReportParameter(details.getReportParameter());

      arInvoice.setInvDocumentType(details.getInvDocumentType());
      Debug.print("ArInvoiceEntryControllerBean saveArInvEntry C");

      LocalAdPaymentTerm adPaymentTerm = adPaymentTermHome.findByPytName(PYT_NM, AD_CMPNY);
      // adPaymentTerm.addArInvoice(arInvoice);
      arInvoice.setAdPaymentTerm(adPaymentTerm);

      LocalGlFunctionalCurrency glFunctionalCurrency =
          glFunctionalCurrencyHome.findByFcName(FC_NM, AD_CMPNY);
      // glFunctionalCurrency.addArInvoice(arInvoice);
      arInvoice.setGlFunctionalCurrency(glFunctionalCurrency);

      LocalArTaxCode arTaxCode = arTaxCodeHome.findByTcName(TC_NM, AD_CMPNY);
      // arTaxCode.addArInvoice(arInvoice);
      arInvoice.setArTaxCode(arTaxCode);
      System.out.println("CST_CSTMR_CODE: " + CST_CSTMR_CODE);
      System.out.println("WTC_NM: " + WTC_NM + " part 2 " + AD_CMPNY + " <<");
      LocalArWithholdingTaxCode arWithholdingTaxCode =
          arWithholdingTaxCodeHome.findByWtcName(WTC_NM, AD_CMPNY);
      // arWithholdingTaxCode.addArInvoice(arInvoice);


      arInvoice.setArWithholdingTaxCode(arWithholdingTaxCode);

      System.out.println("CST_CSTMR_CODE: " + CST_CSTMR_CODE);
      LocalArCustomer arCustomer = arCustomerHome.findByCstCustomerCode(CST_CSTMR_CODE, AD_CMPNY);
      // arCustomer.addArInvoice(arInvoice);
      arInvoice.setArCustomer(arCustomer);



      try {

        LocalPmProject pmProject = pmProjectHome.findByPrjProjectCode(PRJ_PRJCT_CODE, AD_CMPNY);

        arInvoice.setPmProject(pmProject);

      } catch (FinderException ex) {

      }

      LocalPmProjectTypeType pmProjectTypeType = null;

      try {

        pmProjectTypeType = pmProjectTypeTypeHome.findPttByPrjProjectCodeAndPtProjectTypeCode(
            PRJ_PRJCT_CODE, PT_PRJCT_TYP_CODE, AD_CMPNY);

        arInvoice.setPmProjectTypeType(pmProjectTypeType);

        if (pmProjectTypeType.getPmContract() != null) {
          LocalPmContractTerm pmContractTerm =
              pmContractTermHome.findCtByCtrCodeAndCtTermDescription(
                  pmProjectTypeType.getPmContract().getCtrCode(), CT_CNTRCT_TRM_NM, AD_CMPNY);

          arInvoice.setPmContractTerm(pmContractTerm);

        }

      } catch (FinderException ex) {

      }


      try {

        LocalPmProjectPhase pmProjectPhase =
            pmProjectPhaseHome.findPpByPrjProjectCodeAndPtProjectTypeCodeAndPpName(PRJ_PRJCT_CODE,
                PT_PRJCT_TYP_CODE, PP_PRJCT_PHSE_NM, AD_CMPNY);

        arInvoice.setPmProjectPhase(pmProjectPhase);

      } catch (FinderException ex) {

      }



      LocalArSalesperson arSalesperson = null;

      if (SLP_SLSPRSN_CODE != null && SLP_SLSPRSN_CODE.length() > 0
          && !SLP_SLSPRSN_CODE.equalsIgnoreCase("NO RECORD FOUND")) {

        // if he tagged a salesperson for this invoice
        arSalesperson = arSalespersonHome.findBySlpSalespersonCode(SLP_SLSPRSN_CODE, AD_CMPNY);

        // arSalesperson.addArInvoice(arInvoice);
        arInvoice.setArSalesperson(arSalesperson);


      } else {

        // if he untagged a salesperson for this invoice
        if (arInvoice.getArSalesperson() != null) {

          arSalesperson = arSalespersonHome.findBySlpSalespersonCode(
              arInvoice.getArSalesperson().getSlpSalespersonCode(), AD_CMPNY);
          arSalesperson.dropArInvoice(arInvoice);

        }

        // if no salesperson is set, invoice should not be subject to commission
        arInvoice.setInvSubjectToCommission((byte) 0);

      }

      try {

        LocalArInvoiceBatch arInvoiceBatch =
            arInvoiceBatchHome.findByIbName(IB_NM, AD_BRNCH, AD_CMPNY);
        // arInvoiceBatch.addArInvoice(arInvoice);
        arInvoice.setArInvoiceBatch(arInvoiceBatch);

      } catch (FinderException ex) {

      }
      Debug.print("ArInvoiceEntryControllerBean saveArInvEntry C Done");
      double amountDue = 0;

      Debug.print("ArInvoiceEntryControllerBean saveArInvEntry D");
      if (isRecalculate) {

        // remove all invoice line items

        Collection arInvoiceLineItems = arInvoice.getArInvoiceLineItems();

        Iterator i = arInvoiceLineItems.iterator();

        while (i.hasNext()) {

          LocalArInvoiceLineItem arInvoiceLineItem = (LocalArInvoiceLineItem) i.next();


          // remove all inv tag
          Collection invTags = arInvoiceLineItem.getInvTags();

          Iterator x = invTags.iterator();

          while (x.hasNext()) {

            LocalInvTag invTag = (LocalInvTag) x.next();

            x.remove();

            invTag.remove();
          }


          double convertedQuantity =
              this.convertByUomFromAndItemAndQuantity(arInvoiceLineItem.getInvUnitOfMeasure(),
                  arInvoiceLineItem.getInvItemLocation().getInvItem(),
                  arInvoiceLineItem.getIliQuantity(), AD_CMPNY);
          arInvoiceLineItem.getInvItemLocation().setIlCommittedQuantity(
              arInvoiceLineItem.getInvItemLocation().getIlCommittedQuantity() - convertedQuantity);

          i.remove();

          arInvoiceLineItem.remove();

        }

        // remove all invoice lines

        Collection arInvoiceLines = arInvoice.getArInvoiceLines();

        i = arInvoiceLines.iterator();

        while (i.hasNext()) {

          LocalArInvoiceLine arInvoiceLine = (LocalArInvoiceLine) i.next();


          i.remove();


          arInvoiceLine.remove();

        }

        // remove all sales order lines

        Collection arSalesOrderInvoiceLines = arInvoice.getArSalesOrderInvoiceLines();

        i = arSalesOrderInvoiceLines.iterator();

        while (i.hasNext()) {

          LocalArSalesOrderInvoiceLine arSalesOrderInvoiceLine =
              (LocalArSalesOrderInvoiceLine) i.next();



          // remove all inv tag
          Collection invTags = arSalesOrderInvoiceLine.getInvTags();

          Iterator x = invTags.iterator();

          while (x.hasNext()) {

            LocalInvTag invTag = (LocalInvTag) x.next();

            x.remove();

            invTag.remove();
          }



          i.remove();

          arSalesOrderInvoiceLine.remove();

        }


        // remove all distribution records

        Collection arDistributionRecords = arInvoice.getArDistributionRecords();

        i = arDistributionRecords.iterator();

        while (i.hasNext()) {

          LocalArDistributionRecord arDistributionRecord = (LocalArDistributionRecord) i.next();

          i.remove();

          arDistributionRecord.remove();

        }

        LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);

        // add new invoice lines and distribution record

        double TOTAL_TAX = 0d;
        double TOTAL_LINE = 0d;
        double TOTAL_UNTAXABLE = 0d;

        i = ilList.iterator();

        while (i.hasNext()) {

          ArModInvoiceLineDetails mInvDetails = (ArModInvoiceLineDetails) i.next();

          LocalArInvoiceLine arInvoiceLine = this.addArIlEntry(mInvDetails, arInvoice, AD_CMPNY);


          // add receivable/debit distributions
          // double LINE = arInvoiceLine.getIlAmount();
          // double TAX = arInvoiceLine.getIlTaxAmount();



          double LINE = this.convertForeignToFunctionalCurrency(
              arInvoice.getGlFunctionalCurrency().getFcCode(),
              arInvoice.getGlFunctionalCurrency().getFcName(), arInvoice.getInvConversionDate(),
              arInvoice.getInvConversionRate(), arInvoiceLine.getIlAmount(), AD_CMPNY);

          double TAX = this.convertForeignToFunctionalCurrency(
              arInvoice.getGlFunctionalCurrency().getFcCode(),
              arInvoice.getGlFunctionalCurrency().getFcName(), arInvoice.getInvConversionDate(),
              arInvoice.getInvConversionRate(), arInvoiceLine.getIlTaxAmount(), AD_CMPNY);

          double UNTAXABLE = 0d;
          double W_TAX = 0d;
          double DISCOUNT = 0d;



          if (arInvoiceLine.getIlTax() == EJBCommon.FALSE)
            UNTAXABLE += LINE;


          // compute w-tax per line


          if (arWithholdingTaxCode.getWtcRate() != 0
              && adPreference.getPrfArWTaxRealization().equals("INVOICE")) {

            W_TAX =
                EJBCommon.roundIt((LINE - UNTAXABLE) * (arWithholdingTaxCode.getWtcRate() / 100),
                    this.getGlFcPrecisionUnit(AD_CMPNY));

          }

          if (adPaymentTerm.getPytDiscountOnInvoice() == EJBCommon.TRUE) {

            Collection adPaymentSchedules =
                adPaymentScheduleHome.findByPytCode(adPaymentTerm.getPytCode(), AD_CMPNY);
            ArrayList adPaymentScheduleList = new ArrayList(adPaymentSchedules);
            LocalAdPaymentSchedule adPaymentSchedule =
                (LocalAdPaymentSchedule) adPaymentScheduleList.get(0);

            Collection adDiscounts =
                adDiscountHome.findByPsCode(adPaymentSchedule.getPsCode(), AD_CMPNY);
            ArrayList adDiscountList = new ArrayList(adDiscounts);
            LocalAdDiscount adDiscount = (LocalAdDiscount) adDiscountList.get(0);


            double rate = adDiscount.getDscDiscountPercent();
            DISCOUNT = (LINE + TAX) * (rate / 100d);
          }


          // add revenue/credit distributions

          // Check If Memo Line Type is Service Charge

          if (arInvoiceLine.getArStandardMemoLine().getSmlType().equals("SC")) {

            if (arInvoiceLine.getArStandardMemoLine().getSmlInterimAccount() == null)
              throw new GlobalBranchAccountNumberInvalidException();

            this.addArDrEntry(arInvoice.getArDrNextLine(), "SC", EJBCommon.FALSE, LINE,
                arInvoiceLine.getArStandardMemoLine().getSmlInterimAccount(),
                this.getArGlCoaRevenueAccount(arInvoiceLine, AD_BRNCH, AD_CMPNY), arInvoice,
                AD_BRNCH, AD_CMPNY);
          } else {

            this.addArDrEntry(arInvoice.getArDrNextLine(), "REVENUE", EJBCommon.FALSE, LINE,
                this.getArGlCoaRevenueAccount(arInvoiceLine, AD_BRNCH, AD_CMPNY), null, arInvoice,
                AD_BRNCH, AD_CMPNY);


            /*
             * if(this.getAdPrfArDetailedReceivable(AD_CMPNY) == (byte)1 ){
             * 
             * this.addArDrEntry(arInvoice.getArDrNextLine(), "RECEIVABLE", EJBCommon.TRUE, LINE +
             * TAX - W_TAX - DISCOUNT, this.getArGlCoaReceivableAccount(arInvoiceLine, AD_BRNCH,
             * AD_CMPNY), null, arInvoice, AD_BRNCH, AD_CMPNY);
             * 
             * }
             */



          }


          TOTAL_LINE += arInvoiceLine.getIlAmount();
          TOTAL_TAX += arInvoiceLine.getIlTaxAmount();

          if (arInvoiceLine.getIlTax() == EJBCommon.FALSE)
            TOTAL_UNTAXABLE += arInvoiceLine.getIlAmount();

        }


        // add tax distribution if necessary

        if (!arTaxCode.getTcType().equals("NONE") && !arTaxCode.getTcType().equals("EXEMPT")) {

          double TOTAL_TAX_CONV = this.convertForeignToFunctionalCurrency(
              arInvoice.getGlFunctionalCurrency().getFcCode(),
              arInvoice.getGlFunctionalCurrency().getFcName(), arInvoice.getInvConversionDate(),
              arInvoice.getInvConversionRate(), TOTAL_TAX, AD_CMPNY);

          if (arTaxCode.getTcInterimAccount() == null) {

            // add branch tax code
            LocalAdBranchArTaxCode adBranchTaxCode = null;
            Debug.print("ArInvoiceEntryControllerBean saveArInvIliEntry J");
            try {
              adBranchTaxCode = adBranchArTaxCodeHome.findBtcByTcCodeAndBrCode(
                  arInvoice.getArTaxCode().getTcCode(), AD_BRNCH, AD_CMPNY);

            } catch (FinderException ex) {

            }

            if (adBranchTaxCode != null) {

              this.addArDrEntry(arInvoice.getArDrNextLine(), "TAX", EJBCommon.FALSE, TOTAL_TAX_CONV,
                  adBranchTaxCode.getBtcGlCoaTaxCode(), null, arInvoice, AD_BRNCH, AD_CMPNY);

            } else {

              this.addArDrEntry(arInvoice.getArDrNextLine(), "TAX", EJBCommon.FALSE, TOTAL_TAX_CONV,
                  arTaxCode.getGlChartOfAccount().getCoaCode(), null, arInvoice, AD_BRNCH,
                  AD_CMPNY);
            }



          } else {

            this.addArDrEntry(arInvoice.getArDrNextLine(), "DEFERRED TAX", EJBCommon.FALSE,
                TOTAL_TAX_CONV, arTaxCode.getTcInterimAccount(), null, arInvoice, AD_BRNCH,
                AD_CMPNY);

          }

        }

        // add wtax distribution if necessary

        adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);

        double W_TAX_AMOUNT = 0d;

        if (arWithholdingTaxCode.getWtcRate() != 0
            && adPreference.getPrfArWTaxRealization().equals("INVOICE")) {

          W_TAX_AMOUNT = EJBCommon.roundIt(
              (TOTAL_LINE - TOTAL_UNTAXABLE) * (arWithholdingTaxCode.getWtcRate() / 100),
              this.getGlFcPrecisionUnit(AD_CMPNY));

          double W_TAX_AMOUNT_CONV = this.convertForeignToFunctionalCurrency(
              arInvoice.getGlFunctionalCurrency().getFcCode(),
              arInvoice.getGlFunctionalCurrency().getFcName(), arInvoice.getInvConversionDate(),
              arInvoice.getInvConversionRate(), W_TAX_AMOUNT, AD_CMPNY);

          this.addArDrEntry(arInvoice.getArDrNextLine(), "W-TAX", EJBCommon.TRUE, W_TAX_AMOUNT_CONV,
              arWithholdingTaxCode.getGlChartOfAccount().getCoaCode(), null, arInvoice, AD_BRNCH,
              AD_CMPNY);

        }

        // add payment discount if necessary

        double DISCOUNT_AMT = 0d;

        if (adPaymentTerm.getPytDiscountOnInvoice() == EJBCommon.TRUE) {

          Collection adPaymentSchedules =
              adPaymentScheduleHome.findByPytCode(adPaymentTerm.getPytCode(), AD_CMPNY);
          ArrayList adPaymentScheduleList = new ArrayList(adPaymentSchedules);
          LocalAdPaymentSchedule adPaymentSchedule =
              (LocalAdPaymentSchedule) adPaymentScheduleList.get(0);

          Collection adDiscounts =
              adDiscountHome.findByPsCode(adPaymentSchedule.getPsCode(), AD_CMPNY);
          ArrayList adDiscountList = new ArrayList(adDiscounts);
          LocalAdDiscount adDiscount = (LocalAdDiscount) adDiscountList.get(0);


          double rate = adDiscount.getDscDiscountPercent();
          DISCOUNT_AMT = (TOTAL_LINE + TOTAL_TAX) * (rate / 100d);

          double DISCOUNT_AMT_CONV = this.convertForeignToFunctionalCurrency(
              arInvoice.getGlFunctionalCurrency().getFcCode(),
              arInvoice.getGlFunctionalCurrency().getFcName(), arInvoice.getInvConversionDate(),
              arInvoice.getInvConversionRate(), DISCOUNT_AMT, AD_CMPNY);


          this.addArDrIliEntry(arInvoice.getArDrNextLine(), "DISCOUNT", EJBCommon.TRUE,
              DISCOUNT_AMT_CONV, adPaymentTerm.getGlChartOfAccount().getCoaCode(), arInvoice,
              AD_BRNCH, AD_CMPNY);

        }

        // add receivable distribution

        amountDue = TOTAL_LINE + TOTAL_TAX - W_TAX_AMOUNT - DISCOUNT_AMT;

        try {

          LocalAdBranchCustomer adBranchCustomer = adBranchCustomerHome.findBcstByCstCodeAndBrCode(
              arInvoice.getArCustomer().getCstCode(), AD_BRNCH, AD_CMPNY);
          double AMNT_DUE_CONV = this.convertForeignToFunctionalCurrency(
              arInvoice.getGlFunctionalCurrency().getFcCode(),
              arInvoice.getGlFunctionalCurrency().getFcName(), arInvoice.getInvConversionDate(),
              arInvoice.getInvConversionRate(), amountDue, AD_CMPNY);


          this.addArDrEntry(arInvoice.getArDrNextLine(), "RECEIVABLE", EJBCommon.TRUE,
              AMNT_DUE_CONV, adBranchCustomer.getBcstGlCoaReceivableAccount(), null, arInvoice,
              AD_BRNCH, AD_CMPNY);

        } catch (FinderException ex) {

        }



        // compute invoice amount due



        // set invoice amount due

        arInvoice.setInvAmountDue(amountDue);

        // remove all payment schedule

        Collection arInvoicePaymentSchedules = arInvoice.getArInvoicePaymentSchedules();

        i = arInvoicePaymentSchedules.iterator();

        while (i.hasNext()) {

          LocalArInvoicePaymentSchedule arInvoicePaymentSchedule =
              (LocalArInvoicePaymentSchedule) i.next();

          i.remove();

          arInvoicePaymentSchedule.remove();

        }


        // create invoice payment schedule

        short precisionUnit = this.getGlFcPrecisionUnit(AD_CMPNY);
        double TOTAL_PAYMENT_SCHEDULE = 0d;
        double TOTAL_PAYMENT_INT = 0d;
        double TOTAL_PAYMENT_SCHEDULE_INT = TOTAL_LINE + TOTAL_TAX;


        GregorianCalendar gcPrevDateDue = new GregorianCalendar();
        GregorianCalendar gcDateDue = new GregorianCalendar();
        gcPrevDateDue.setTime(arInvoice.getInvEffectivityDate());

        Collection adPaymentSchedules = adPaymentTerm.getAdPaymentSchedules();

        i = adPaymentSchedules.iterator();

        while (i.hasNext()) {

          LocalAdPaymentSchedule adPaymentSchedule = (LocalAdPaymentSchedule) i.next();

          // get date due

          if (arInvoice.getAdPaymentTerm().getPytScheduleBasis().equals("DEFAULT")) {

            gcDateDue.setTime(arInvoice.getInvEffectivityDate());
            gcDateDue.add(Calendar.DATE, adPaymentSchedule.getPsDueDay());

          } else if (arInvoice.getAdPaymentTerm().getPytScheduleBasis().equals("MONTHLY")) {

            gcDateDue = gcPrevDateDue;
            gcDateDue.add(Calendar.MONTH, 1);
            gcPrevDateDue = gcDateDue;

          } else if (arInvoice.getAdPaymentTerm().getPytScheduleBasis().equals("BI-MONTHLY")) {

            gcDateDue = gcPrevDateDue;

            if (gcPrevDateDue.get(Calendar.MONTH) != 1) {
              if (gcPrevDateDue.getActualMaximum(Calendar.DATE) == 31
                  && gcPrevDateDue.get(Calendar.DATE) > 15
                  && gcPrevDateDue.get(Calendar.DATE) < 31) {
                gcDateDue.add(Calendar.DATE, 16);
              } else {
                gcDateDue.add(Calendar.DATE, 15);
              }
            } else if (gcPrevDateDue.get(Calendar.MONTH) == 1) {
              if (gcPrevDateDue.getActualMaximum(Calendar.DATE) == 28
                  && gcPrevDateDue.get(Calendar.DATE) == 14) {
                gcDateDue.add(Calendar.DATE, 14);
              } else if (gcPrevDateDue.getActualMaximum(Calendar.DATE) == 28
                  && gcPrevDateDue.get(Calendar.DATE) >= 15
                  && gcPrevDateDue.get(Calendar.DATE) < 28) {
                gcDateDue.add(Calendar.DATE, 13);
              } else if (gcPrevDateDue.getActualMaximum(Calendar.DATE) == 29
                  && gcPrevDateDue.get(Calendar.DATE) >= 15
                  && gcPrevDateDue.get(Calendar.DATE) < 29) {
                gcDateDue.add(Calendar.DATE, 14);
              } else {
                gcDateDue.add(Calendar.DATE, 15);
              }
            }

            gcPrevDateDue = gcDateDue;

          }

          // create a payment schedule

          double PAYMENT_SCHEDULE_AMOUNT = 0;
          double PAYMENT_SCHEDULE_INT = 0;
          double PAYMENT_SCHEDULE_PRINCIPAL = 0;

          // if last payment schedule subtract to avoid rounding difference error

          if (i.hasNext()) {

            PAYMENT_SCHEDULE_AMOUNT = EJBCommon.roundIt(
                (adPaymentSchedule.getPsRelativeAmount() / adPaymentTerm.getPytBaseAmount())
                    * arInvoice.getInvAmountDue(),
                precisionUnit);
            System.out
                .println("------------>TOTAL_PAYMENT_SCHEDULE_INT=" + TOTAL_PAYMENT_SCHEDULE_INT);


            PAYMENT_SCHEDULE_INT = EJBCommon.roundIt(
                (TOTAL_PAYMENT_SCHEDULE_INT) * (adPaymentTerm.getPytMonthlyInterestRate() / 100),
                precisionUnit);

            PAYMENT_SCHEDULE_PRINCIPAL = -(PAYMENT_SCHEDULE_AMOUNT) + PAYMENT_SCHEDULE_INT;


          } else {

            System.out
                .println("------------>TOTAL_PAYMENT_SCHEDULE_INT=" + TOTAL_PAYMENT_SCHEDULE_INT);


            PAYMENT_SCHEDULE_AMOUNT = arInvoice.getInvAmountDue() - TOTAL_PAYMENT_SCHEDULE;

            PAYMENT_SCHEDULE_INT = EJBCommon.roundIt(
                (TOTAL_PAYMENT_SCHEDULE_INT) * (adPaymentTerm.getPytMonthlyInterestRate() / 100),
                precisionUnit);

            PAYMENT_SCHEDULE_PRINCIPAL = -(PAYMENT_SCHEDULE_AMOUNT) + PAYMENT_SCHEDULE_INT;


          }

          LocalArInvoicePaymentSchedule arInvoicePaymentSchedule =
              arInvoicePaymentScheduleHome.create(gcDateDue.getTime(),
                  adPaymentSchedule.getPsLineNumber(), PAYMENT_SCHEDULE_AMOUNT, 0d, EJBCommon.FALSE,
                  (short) 0, gcDateDue.getTime(), 0d, 0d, AD_CMPNY);


          arInvoicePaymentSchedule.setIpsInterestDue(PAYMENT_SCHEDULE_INT);
          System.out.println("PAYMENT_SCHEDULE_INT=" + PAYMENT_SCHEDULE_INT);
          arInvoice.addArInvoicePaymentSchedule(arInvoicePaymentSchedule);

          TOTAL_PAYMENT_SCHEDULE += PAYMENT_SCHEDULE_AMOUNT;
          TOTAL_PAYMENT_INT += PAYMENT_SCHEDULE_INT;
          TOTAL_PAYMENT_SCHEDULE_INT += PAYMENT_SCHEDULE_PRINCIPAL;
        }

      }

      Debug.print("ArInvoiceEntryControllerBean saveArInvEntry D Done");
      // generate approval status

      String INV_APPRVL_STATUS = null;

      Debug.print("ArInvoiceEntryControllerBean saveArInvEntry E");
      if (!isDraft) {

        LocalAdApproval adApproval = adApprovalHome.findByAprAdCompany(AD_CMPNY);

        // validate if amount due + unposted invoices' amount + current balance + unposted receipts'
        // amount
        // does not exceed customer's credit limit

        double balance = 0;
        LocalAdApprovalDocument adInvoiceApprovalDocument =
            adApprovalDocumentHome.findByAdcType("AR INVOICE", AD_CMPNY);

        if (arCustomer.getCstCreditLimit() > 0) {

          balance = computeTotalBalance(details.getInvCode(), CST_CSTMR_CODE, AD_CMPNY);

          balance += amountDue;

          if (arCustomer.getCstCreditLimit() < balance && (adApproval
              .getAprEnableArInvoice() == EJBCommon.FALSE
              || (adApproval.getAprEnableArInvoice() == EJBCommon.TRUE && adInvoiceApprovalDocument
                  .getAdcEnableCreditLimitChecking() == EJBCommon.FALSE))) {

            throw new ArINVAmountExceedsCreditLimitException();

          }

        }

        // find overdue invoices
        Collection arOverdueInvoices =
            arInvoicePaymentScheduleHome.findOverdueIpsByInvDateAndCstCustomerCode(
                arInvoice.getInvDate(), CST_CSTMR_CODE, AD_CMPNY);

        // check if ar invoice approval is enabled

        if (adApproval.getAprEnableArInvoice() == EJBCommon.FALSE
            || (arCustomer.getCstCreditLimit() > balance && arOverdueInvoices.size() == 0)) {

          INV_APPRVL_STATUS = "N/A";

        } else {

          // check if invoice is self approved

          LocalAdAmountLimit adAmountLimit = null;

          try {

            adAmountLimit = adAmountLimitHome.findByAdcTypeAndAuTypeAndUsrName("AR INVOICE",
                "REQUESTER", details.getInvLastModifiedBy(), AD_CMPNY);

          } catch (FinderException ex) {

            throw new GlobalNoApprovalRequesterFoundException();

          }

          if (arInvoice.getInvAmountDue() < adAmountLimit.getCalAmountLimit()
              && (arCustomer.getCstCreditLimit() == 0
                  || arCustomer.getCstCreditLimit() > balance)) {

            INV_APPRVL_STATUS = "N/A";

          } else {

            // for approval, create approval queue

            Collection adAmountLimits = adAmountLimitHome.findByAdcTypeAndGreaterThanCalAmountLimit(
                "AR INVOICE", adAmountLimit.getCalAmountLimit(), AD_CMPNY);

            if (adAmountLimits.isEmpty()) {

              Collection adApprovalUsers = adApprovalUserHome.findByAuTypeAndCalCode("APPROVER",
                  adAmountLimit.getCalCode(), AD_CMPNY);

              if (adApprovalUsers.isEmpty()) {

                throw new GlobalNoApprovalApproverFoundException();

              }

              Iterator j = adApprovalUsers.iterator();

              while (j.hasNext()) {

                LocalAdApprovalUser adApprovalUser = (LocalAdApprovalUser) j.next();

                LocalAdApprovalQueue adApprovalQueue =
                    adApprovalQueueHome.create(EJBCommon.TRUE, "AR INVOICE", arInvoice.getInvCode(),
                        arInvoice.getInvNumber(), arInvoice.getInvDate(),
                        adAmountLimit.getCalAndOr(), adApprovalUser.getAuOr(), AD_BRNCH, AD_CMPNY);

                adApprovalUser.getAdUser().addAdApprovalQueue(adApprovalQueue);

              }

            } else {

              boolean isApprovalUsersFound = false;

              Iterator i = adAmountLimits.iterator();

              while (i.hasNext()) {

                LocalAdAmountLimit adNextAmountLimit = (LocalAdAmountLimit) i.next();

                if (arInvoice.getInvAmountDue() < adNextAmountLimit.getCalAmountLimit()) {

                  Collection adApprovalUsers = adApprovalUserHome.findByAuTypeAndCalCode("APPROVER",
                      adAmountLimit.getCalCode(), AD_CMPNY);

                  Iterator j = adApprovalUsers.iterator();

                  while (j.hasNext()) {

                    isApprovalUsersFound = true;

                    LocalAdApprovalUser adApprovalUser = (LocalAdApprovalUser) j.next();

                    LocalAdApprovalQueue adApprovalQueue = adApprovalQueueHome.create(
                        EJBCommon.TRUE, "AR INVOICE", arInvoice.getInvCode(),
                        arInvoice.getInvNumber(), arInvoice.getInvDate(),
                        adAmountLimit.getCalAndOr(), adApprovalUser.getAuOr(), AD_BRNCH, AD_CMPNY);

                    adApprovalUser.getAdUser().addAdApprovalQueue(adApprovalQueue);

                  }

                  break;

                } else if (!i.hasNext()) {

                  Collection adApprovalUsers = adApprovalUserHome.findByAuTypeAndCalCode("APPROVER",
                      adNextAmountLimit.getCalCode(), AD_CMPNY);

                  Iterator j = adApprovalUsers.iterator();

                  while (j.hasNext()) {

                    isApprovalUsersFound = true;

                    LocalAdApprovalUser adApprovalUser = (LocalAdApprovalUser) j.next();

                    LocalAdApprovalQueue adApprovalQueue =
                        adApprovalQueueHome.create(EJBCommon.TRUE, "AR INVOICE",
                            arInvoice.getInvCode(), arInvoice.getInvNumber(),
                            arInvoice.getInvDate(), adNextAmountLimit.getCalAndOr(),
                            adApprovalUser.getAuOr(), AD_BRNCH, AD_CMPNY);

                    adApprovalUser.getAdUser().addAdApprovalQueue(adApprovalQueue);

                  }

                  break;

                }

                adAmountLimit = adNextAmountLimit;

              }

              if (!isApprovalUsersFound) {

                throw new GlobalNoApprovalApproverFoundException();

              }

            }

            INV_APPRVL_STATUS = "PENDING";
          }
        }
      }

      Debug.print("ArInvoiceEntryControllerBean saveArInvEntry E Done");
      LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);

      if (INV_APPRVL_STATUS != null && INV_APPRVL_STATUS.equals("N/A")
          && adPreference.getPrfArGlPostingType().equals("AUTO-POST UPON APPROVAL")) {

        this.executeArInvPost(arInvoice.getInvCode(), arInvoice.getInvLastModifiedBy(), AD_BRNCH,
            AD_CMPNY);

      }

      // set invoice approval status

      arInvoice.setInvApprovalStatus(INV_APPRVL_STATUS);

      return arInvoice.getInvCode();


    } catch (GlobalRecordAlreadyDeletedException ex) {

      getSessionContext().setRollbackOnly();
      throw ex;

    } catch (GlobalDocumentNumberNotUniqueException ex) {

      getSessionContext().setRollbackOnly();
      throw ex;

    } catch (GlobalConversionDateNotExistException ex) {

      getSessionContext().setRollbackOnly();
      throw ex;

    } catch (GlobalPaymentTermInvalidException ex) {

      getSessionContext().setRollbackOnly();
      throw ex;

    } catch (ArINVAmountExceedsCreditLimitException ex) {

      getSessionContext().setRollbackOnly();
      throw ex;

    } catch (GlobalTransactionAlreadyApprovedException ex) {

      getSessionContext().setRollbackOnly();
      throw ex;

    } catch (GlobalTransactionAlreadyPendingException ex) {

      getSessionContext().setRollbackOnly();
      throw ex;

    } catch (GlobalTransactionAlreadyPostedException ex) {

      getSessionContext().setRollbackOnly();
      throw ex;

    } catch (GlobalTransactionAlreadyVoidException ex) {

      getSessionContext().setRollbackOnly();
      throw ex;

    } catch (GlobalNoApprovalRequesterFoundException ex) {

      getSessionContext().setRollbackOnly();
      throw ex;

    } catch (GlobalNoApprovalApproverFoundException ex) {

      getSessionContext().setRollbackOnly();
      throw ex;

    } catch (GlJREffectiveDateNoPeriodExistException ex) {

      getSessionContext().setRollbackOnly();
      throw ex;

    } catch (GlJREffectiveDatePeriodClosedException ex) {

      getSessionContext().setRollbackOnly();
      throw ex;

    } catch (GlobalJournalNotBalanceException ex) {

      getSessionContext().setRollbackOnly();
      throw ex;

    } catch (GlobalBranchAccountNumberInvalidException ex) {

      getSessionContext().setRollbackOnly();
      throw ex;

    } catch (ArInvDuplicateUploadNumberException ex) {

      getSessionContext().setRollbackOnly();
      throw ex;

    } catch (Exception ex) {

      Debug.printStackTrace(ex);
      getSessionContext().setRollbackOnly();
      throw new EJBException(ex.getMessage());

    }


  }

  //
  /**
   * @ejb:interface-method view-type="remote"
   **/
  public String[] generateInvEntryByArStndrdMml(
      String[][] values, String user_acc, Integer AD_BRNCH, Integer AD_CMPNY
  ) {

    Debug.print("ArInvoiceEntryControllerBean saveArInvEntry");

    LocalArInvoiceHome arInvoiceHome = null;
    LocalArInvoiceBatchHome arInvoiceBatchHome = null;
    LocalAdCompanyHome adCompanyHome = null;
    LocalGlFunctionalCurrencyHome glFunctionalCurrencyHome = null;
    LocalAdDocumentSequenceAssignmentHome adDocumentSequenceAssignmentHome = null;
    LocalAdBranchDocumentSequenceAssignmentHome adBranchDocumentSequenceAssignmentHome = null;
    LocalAdPaymentTermHome adPaymentTermHome = null;
    LocalAdPaymentScheduleHome adPaymentScheduleHome = null;
    LocalAdDiscountHome adDiscountHome = null;
    LocalArTaxCodeHome arTaxCodeHome = null;

    LocalArWithholdingTaxCodeHome arWithholdingTaxCodeHome = null;
    LocalArCustomerHome arCustomerHome = null;
    LocalGlFunctionalCurrencyRateHome glFunctionalCurrencyRateHome = null;
    LocalArInvoicePaymentScheduleHome arInvoicePaymentScheduleHome = null;
    LocalAdApprovalHome adApprovalHome = null;
    LocalAdAmountLimitHome adAmountLimitHome = null;
    LocalAdApprovalUserHome adApprovalUserHome = null;
    LocalAdApprovalQueueHome adApprovalQueueHome = null;
    LocalAdApprovalDocumentHome adApprovalDocumentHome = null;
    LocalAdPreferenceHome adPreferenceHome = null;
    LocalArCustomerBalanceHome arCustomerBalanceHome = null;
    LocalArReceiptHome arReceiptHome = null;
    LocalAdBranchCustomerHome adBranchCustomerHome = null;
    LocalArSalespersonHome arSalespersonHome = null;
    LocalGlSetOfBookHome glSetOfBookHome = null;
    LocalAdBranchArTaxCodeHome adBranchArTaxCodeHome = null;

    LocalArInvoice arInvoice = null;

    try {

      adBranchArTaxCodeHome = (LocalAdBranchArTaxCodeHome) EJBHomeFactory
          .lookUpLocalHome(LocalAdBranchArTaxCodeHome.JNDI_NAME, LocalAdBranchArTaxCodeHome.class);
      arInvoiceHome = (LocalArInvoiceHome) EJBHomeFactory
          .lookUpLocalHome(LocalArInvoiceHome.JNDI_NAME, LocalArInvoiceHome.class);
      arInvoiceBatchHome = (LocalArInvoiceBatchHome) EJBHomeFactory
          .lookUpLocalHome(LocalArInvoiceBatchHome.JNDI_NAME, LocalArInvoiceBatchHome.class);
      adCompanyHome = (LocalAdCompanyHome) EJBHomeFactory
          .lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
      glFunctionalCurrencyHome = (LocalGlFunctionalCurrencyHome) EJBHomeFactory.lookUpLocalHome(
          LocalGlFunctionalCurrencyHome.JNDI_NAME, LocalGlFunctionalCurrencyHome.class);
      adDocumentSequenceAssignmentHome = (LocalAdDocumentSequenceAssignmentHome) EJBHomeFactory
          .lookUpLocalHome(LocalAdDocumentSequenceAssignmentHome.JNDI_NAME,
              LocalAdDocumentSequenceAssignmentHome.class);
      adBranchDocumentSequenceAssignmentHome =
          (LocalAdBranchDocumentSequenceAssignmentHome) EJBHomeFactory.lookUpLocalHome(
              LocalAdBranchDocumentSequenceAssignmentHome.JNDI_NAME,
              LocalAdBranchDocumentSequenceAssignmentHome.class);
      adPaymentTermHome = (LocalAdPaymentTermHome) EJBHomeFactory
          .lookUpLocalHome(LocalAdPaymentTermHome.JNDI_NAME, LocalAdPaymentTermHome.class);
      adPaymentScheduleHome = (LocalAdPaymentScheduleHome) EJBHomeFactory
          .lookUpLocalHome(LocalAdPaymentScheduleHome.JNDI_NAME, LocalAdPaymentScheduleHome.class);
      adDiscountHome = (LocalAdDiscountHome) EJBHomeFactory
          .lookUpLocalHome(LocalAdDiscountHome.JNDI_NAME, LocalAdDiscountHome.class);
      arTaxCodeHome = (LocalArTaxCodeHome) EJBHomeFactory
          .lookUpLocalHome(LocalArTaxCodeHome.JNDI_NAME, LocalArTaxCodeHome.class);
      arWithholdingTaxCodeHome = (LocalArWithholdingTaxCodeHome) EJBHomeFactory.lookUpLocalHome(
          LocalArWithholdingTaxCodeHome.JNDI_NAME, LocalArWithholdingTaxCodeHome.class);
      arCustomerHome = (LocalArCustomerHome) EJBHomeFactory
          .lookUpLocalHome(LocalArCustomerHome.JNDI_NAME, LocalArCustomerHome.class);
      glFunctionalCurrencyRateHome =
          (LocalGlFunctionalCurrencyRateHome) EJBHomeFactory.lookUpLocalHome(
              LocalGlFunctionalCurrencyRateHome.JNDI_NAME, LocalGlFunctionalCurrencyRateHome.class);
      arInvoicePaymentScheduleHome =
          (LocalArInvoicePaymentScheduleHome) EJBHomeFactory.lookUpLocalHome(
              LocalArInvoicePaymentScheduleHome.JNDI_NAME, LocalArInvoicePaymentScheduleHome.class);
      adApprovalHome = (LocalAdApprovalHome) EJBHomeFactory
          .lookUpLocalHome(LocalAdApprovalHome.JNDI_NAME, LocalAdApprovalHome.class);
      adAmountLimitHome = (LocalAdAmountLimitHome) EJBHomeFactory
          .lookUpLocalHome(LocalAdAmountLimitHome.JNDI_NAME, LocalAdAmountLimitHome.class);
      adApprovalUserHome = (LocalAdApprovalUserHome) EJBHomeFactory
          .lookUpLocalHome(LocalAdApprovalUserHome.JNDI_NAME, LocalAdApprovalUserHome.class);
      adApprovalQueueHome = (LocalAdApprovalQueueHome) EJBHomeFactory
          .lookUpLocalHome(LocalAdApprovalQueueHome.JNDI_NAME, LocalAdApprovalQueueHome.class);
      adApprovalDocumentHome = (LocalAdApprovalDocumentHome) EJBHomeFactory.lookUpLocalHome(
          LocalAdApprovalDocumentHome.JNDI_NAME, LocalAdApprovalDocumentHome.class);
      adPreferenceHome = (LocalAdPreferenceHome) EJBHomeFactory
          .lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);
      arCustomerBalanceHome = (LocalArCustomerBalanceHome) EJBHomeFactory
          .lookUpLocalHome(LocalArCustomerBalanceHome.JNDI_NAME, LocalArCustomerBalanceHome.class);
      arReceiptHome = (LocalArReceiptHome) EJBHomeFactory
          .lookUpLocalHome(LocalArReceiptHome.JNDI_NAME, LocalArReceiptHome.class);
      adBranchCustomerHome = (LocalAdBranchCustomerHome) EJBHomeFactory
          .lookUpLocalHome(LocalAdBranchCustomerHome.JNDI_NAME, LocalAdBranchCustomerHome.class);
      arSalespersonHome = (LocalArSalespersonHome) EJBHomeFactory
          .lookUpLocalHome(LocalArSalespersonHome.JNDI_NAME, LocalArSalespersonHome.class);
      glSetOfBookHome = (LocalGlSetOfBookHome) EJBHomeFactory
          .lookUpLocalHome(LocalGlSetOfBookHome.JNDI_NAME, LocalGlSetOfBookHome.class);

    } catch (NamingException ex) {

      throw new EJBException(ex.getMessage());

    }


    // Preliminary checking

    ArrayList invoice_list = new ArrayList();
    ArrayList mmlList = new ArrayList();


    HashMap invoice_data = new HashMap();

    /*
     * CSV columns identify
     *
     * 1 - Control Number 2 - Customer Code 3 - SML Name 4 - Previous 5 - Present 6 - Quantity/Cons
     * 7 - Unit Price/Rate 8 - Amount 9 - Date 10 - Invoice Number 11 - Reference Number 12 -
     * Payment Terms 13 - Taxable 14 - Withholding Tax 15 - Description 16 - Batch Name
     *
     */
    try {

      ArInvoiceDetails invDetails = new ArInvoiceDetails();
      String curr_control = "";
      Integer line_read = 0;
      String cst_code = "";
      String[] error_data = new String[2];
      double inv_previous = 0;
      double inv_present = 0;
      Date inv_date;
      String inv_number = "";
      String ref_number = "";
      String payment_term = "";
      String tax_code = "";
      String wtax_code = "";
      String description = "";
      String batch_name = "";

      String sml_name = "";
      double mml_quantity = 0;
      double mml_unitprice = 0;
      double mml_amount = 0;

      boolean new_transaction = true;

      ArrayList batchNameList = getArOpenIbAll(AD_BRNCH, AD_CMPNY);
      ArrayList memoLineList = getArSmlAll(AD_BRNCH, AD_CMPNY);
      ArrayList customerList = getArCstAll(AD_BRNCH, AD_CMPNY);
      ArrayList wtaxList = getArWtcAll(AD_CMPNY);
      ArrayList taxList = getArTcAll(AD_CMPNY);
      ArrayList paymentList = getAdPytAll(AD_CMPNY);
      ArrayList currencyList = getGlFcAllWithDefault(AD_CMPNY);

      System.out.println("pasok ---- 1");
      for (int x = 1; x < values.length; x++) {

        System.out.println("pasok ---- 2");

        if (values[x][0].trim().equals("")) {
          getSessionContext().setRollbackOnly();
          error_data[0] = "invoiceEntry.error.generateErrorControl";
          error_data[1] = line_read.toString();
          return error_data;
        }
        System.out.println("pasok ---- 3");
        if (curr_control.equals(values[x][0].trim())) {
          //
          new_transaction = false;
          // MemoLines checking 2nd line of control -------------------------------

          System.out.println("pasok ---- 3.1");
          // check memo line name
          sml_name = values[x][2].trim();

          boolean ifSmlExist = false;
          System.out.println("pasok ---- 3.2");
          for (int cnt = 0; cnt < memoLineList.size(); cnt++) {
            if (sml_name.equals(memoLineList.get(cnt))) {
              ifSmlExist = true;
            }
          }
          System.out.println("pasok ---- 3.3");
          if (ifSmlExist == false || sml_name.equals("")) {

            getSessionContext().setRollbackOnly();
            error_data[0] = "invoiceEntry.error.generateErrorMemoLineNotExist";
            error_data[1] = line_read.toString();
            return error_data;


          }
          System.out.println("pasok ---- 3.4");

          System.out.println("pasok ---- 4");
          // check memo line quantity
          try {
            mml_quantity = Double.parseDouble(values[x][5].trim());
          } catch (NumberFormatException e) {

            getSessionContext().setRollbackOnly();
            error_data[0] = "invoiceEntry.error.generateErrorMemoLineQuantityFormat";
            error_data[1] = line_read.toString();
            return error_data;

          }

          // check memo line unit price
          try {
            mml_unitprice = Double.parseDouble(values[x][6].trim());
          } catch (NumberFormatException e) {

            getSessionContext().setRollbackOnly();
            error_data[0] = "invoiceEntry.error.generateErrorMemoLineUnitPriceFormat";
            error_data[1] = line_read.toString();
            return error_data;

          }

          // check memo line amount
          try {
            mml_amount = Double.parseDouble(values[x][7].trim());
          } catch (NumberFormatException e) {

            getSessionContext().setRollbackOnly();
            error_data[0] = "invoiceEntry.error.generateErrorMemoLineAmountFormat";
            error_data[1] = line_read.toString();
            return error_data;

          }


          ArModInvoiceLineDetails mdetails = new ArModInvoiceLineDetails();

          mdetails.setIlSmlName(sml_name);
          mdetails.setIlDescription(sml_name);
          mdetails.setIlQuantity(mml_quantity);
          mdetails.setIlUnitPrice(mml_unitprice);
          mdetails.setIlAmount(mml_amount);
          mdetails.setIlTax(EJBCommon.FALSE);

          mmlList.add(mdetails);



        } else {

          System.out.println("new trans 1");
          new_transaction = true;
          // check of cst code exist in CSV 0 column in CSV
          if (values[x][1].toString().equals("")) {

            getSessionContext().setRollbackOnly();
            error_data[0] = "invoiceEntry.error.generateErrorCstCode";
            error_data[1] = line_read.toString();
            return error_data;


          }
          System.out.println("new trans 2");
          mmlList.clear();

          // new/next control/transaction
          curr_control = values[x][0].trim();
          invDetails = new ArInvoiceDetails();



          // check of cst code exist in database 1st column in CSV
          System.out.println("new trans 3");
          cst_code = values[x][1].trim();
          boolean IfCstExist = false;
          System.out.println("customer in process : " + values[x][1].trim());
          for (int cnt = 0; cnt < customerList.size(); cnt++) {

            if (cst_code.equals(customerList.get(cnt).toString())) {

              IfCstExist = true;

            }

          }
          System.out.println("new trans 4");
          if (!IfCstExist) {

            getSessionContext().setRollbackOnly();
            error_data[0] = "invoiceEntry.error.generateErrorCstCode";
            error_data[1] = line_read.toString();
            return error_data;


          }

          // check if previous reading is valid format 3rd column in CSV
          System.out.println("new trans 5");
          try {
            inv_previous = Double.parseDouble(values[x][3].trim());
          } catch (NumberFormatException e) {

            getSessionContext().setRollbackOnly();
            error_data[0] = "invoiceEntry.error.generateErrorPreviousError";
            error_data[1] = line_read.toString();
            return error_data;

          }
          System.out.println("new trans 6");
          // check if present reading is valid format 4th column in CSV
          try {
            inv_previous = Double.parseDouble(values[x][4].trim());
          } catch (NumberFormatException e) {

            getSessionContext().setRollbackOnly();
            error_data[0] = "invoiceEntry.error.generateErrorPresentError";
            error_data[1] = line_read.toString();
            return error_data;


          }

          System.out.println("new trans 7");

          // check if date is valid 8th column in CSV
          try {
            SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
            inv_date = sdf.parse(values[x][8].trim());

          } catch (ParseException ex) {

            getSessionContext().setRollbackOnly();
            error_data[0] = "invoiceEntry.error.generateErrorPresentError";
            error_data[1] = line_read.toString();
            return error_data;

          }


          // check invoice Number if duplicate or not or if blank 9th column in CSV
          System.out.println("new trans 8   " + values[x][9].trim());
          inv_number = values[x][9].trim();
          LocalAdBranchDocumentSequenceAssignment adBranchDocumentSequenceAssignment = null;
          LocalAdDocumentSequenceAssignment adDocumentSequenceAssignment = null;
          System.out.println("new trans 9");
          if (inv_number.equals("")) {
            System.out.println("new trans 9.1");
            inv_number = "";
            System.out.println("new trans 10");
            try {

              adDocumentSequenceAssignment =
                  adDocumentSequenceAssignmentHome.findByDcName("AR INVOICE", AD_CMPNY);

            } catch (FinderException ex) {

            }

            try {
              System.out.println("adDocumentSequenceAssignment.getDsaCode()= "
                  + adDocumentSequenceAssignment.getDsaCode());
              adBranchDocumentSequenceAssignment =
                  adBranchDocumentSequenceAssignmentHome.findBdsByDsaCodeAndBrCode(
                      adDocumentSequenceAssignment.getDsaCode(), AD_BRNCH, AD_CMPNY);

            } catch (FinderException ex) {

            }
            System.out.println("new trans 11");
            LocalArInvoice arExistingInvoice = null;

            try {
              // System.out.println("the inv num:" + inv_number);
              arExistingInvoice = arInvoiceHome.findByInvNumberAndInvCreditMemoAndBrCode(null,
                  EJBCommon.FALSE, AD_BRNCH, AD_CMPNY);

            } catch (FinderException ex) {
            }

            if (arExistingInvoice != null) {


              getSessionContext().setRollbackOnly();
              error_data[0] = "invoiceEntry.error.generateErrorDocumentError";
              error_data[1] = line_read.toString();
              return error_data;


            }
            System.out.println("new trans 12");
            if (adDocumentSequenceAssignment.getAdDocumentSequence().getDsNumberingType() == 'A'
                && (inv_number == null || inv_number.trim().length() == 0)) {

              while (true) {

                if (adBranchDocumentSequenceAssignment == null
                    || adBranchDocumentSequenceAssignment.getBdsNextSequence() == null) {

                  try {

                    arInvoiceHome.findByInvNumberAndInvCreditMemoAndBrCode(
                        adDocumentSequenceAssignment.getDsaNextSequence(), EJBCommon.FALSE,
                        AD_BRNCH, AD_CMPNY);
                    adDocumentSequenceAssignment.setDsaNextSequence(EJBCommon
                        .incrementStringNumber(adDocumentSequenceAssignment.getDsaNextSequence()));

                  } catch (FinderException ex) {


                    inv_number = adDocumentSequenceAssignment.getDsaNextSequence();

                    adDocumentSequenceAssignment.setDsaNextSequence(EJBCommon
                        .incrementStringNumber(adDocumentSequenceAssignment.getDsaNextSequence()));
                    break;

                  }

                } else {

                  try {

                    arInvoiceHome.findByInvNumberAndInvCreditMemoAndBrCode(
                        adBranchDocumentSequenceAssignment.getBdsNextSequence(), EJBCommon.FALSE,
                        AD_BRNCH, AD_CMPNY);
                    adBranchDocumentSequenceAssignment
                        .setBdsNextSequence(EJBCommon.incrementStringNumber(
                            adBranchDocumentSequenceAssignment.getBdsNextSequence()));

                  } catch (FinderException ex) {

                    inv_number = adBranchDocumentSequenceAssignment.getBdsNextSequence();

                    adBranchDocumentSequenceAssignment
                        .setBdsNextSequence(EJBCommon.incrementStringNumber(
                            adBranchDocumentSequenceAssignment.getBdsNextSequence()));
                    break;

                  }

                }

              }

            }
          } else {
            LocalArInvoice arExistingInvoice = null;
            System.out.println("has inv num 1 " + inv_number);
            try {
              System.out.println("has inv num 2");
              arExistingInvoice = arInvoiceHome.findByInvNumberAndInvCreditMemoAndBrCode(inv_number,
                  EJBCommon.FALSE, AD_BRNCH, AD_CMPNY);

            } catch (FinderException ex) {
            }
            System.out.println("has inv num 3");
            if (arExistingInvoice != null && !arExistingInvoice.getInvCode().equals(inv_number)) {
              getSessionContext().setRollbackOnly();
              error_data[0] = "invoiceEntry.error.generateErrorDocumentError";
              error_data[1] = line_read.toString();
              return error_data;


            }
            System.out.println("has inv num 4");
            /*
             * if (arInvoice.getInvNumber() != inv_number && (inv_number == null ||
             * inv_number.trim().length() == 0)) {
             * 
             * inv_number = arInvoice.getInvNumber();
             * 
             * 
             * }
             */

          }

          System.out.println("new trans 13");
          // check reference Number 10th column in CSV
          ref_number = values[x][10].trim();

          if (ref_number.equals("")) {
            ref_number = null;
          }

          System.out.println("new trans 14");
          // check if payment term exist 11th column in CSV
          payment_term = values[x][11].trim();

          boolean IfPTExist = false;
          for (int cnt = 0; cnt < paymentList.size(); cnt++) {

            if (payment_term.equals(paymentList.get(cnt).toString())) {

              IfPTExist = true;

            }

          }
          System.out.println("new trans 15");
          if (!IfPTExist) {

            getSessionContext().setRollbackOnly();
            error_data[0] = "invoiceEntry.error.generateErrorPaymentTermNotExist";
            error_data[1] = line_read.toString();
            return error_data;
          }


          System.out.println("new trans 16");
          // check tax format 12th column in CSV
          tax_code = values[x][12].trim();

          boolean ifTaxExist = false;
          for (int cnt = 0; cnt < taxList.size(); cnt++) {

            if (tax_code.equals(taxList.get(cnt).toString())) {

              ifTaxExist = true;

            }

          }
          System.out.println("new trans 17");
          if (!ifTaxExist) {

            getSessionContext().setRollbackOnly();
            error_data[0] = "invoiceEntry.error.generateErrorTaxNotExist";
            error_data[1] = line_read.toString();
            return error_data;

          }

          System.out.println("new trans 18");
          // check withholding tax 13th column in CSV
          wtax_code = values[x][13].trim();

          boolean ifWTaxExist = false;
          for (int cnt = 0; cnt < wtaxList.size(); cnt++) {
            System.out.println(
                "wtax items: " + wtaxList.get(cnt).toString() + " to compare to: " + wtax_code);
            String tempwtax = wtaxList.get(cnt).toString().trim();
            if (wtax_code.equals(tempwtax)) {
              System.out.println("wtax detected");
              ifWTaxExist = true;

            }

          }
          System.out.println("is: " + ifWTaxExist);
          System.out.println("new trans 19");
          if (!ifWTaxExist) {
            System.out.println("got errorin wtax");
            getSessionContext().setRollbackOnly();
            error_data[0] = "invoiceEntry.error.generateErrorWTaxNotExist";
            error_data[1] = line_read.toString();
            return error_data;

          }

          // check description 14th column in CSV
          description = values[x][14].trim();
          System.out.println("new trans 20");
          if (description.equals("")) {

            getSessionContext().setRollbackOnly();
            error_data[0] = "invoiceEntry.error.generateErrorDescription";
            error_data[1] = line_read.toString();
            return error_data;


          }

          // check batch name if exist 15th column in CSV
          batch_name = values[x][15].trim();
          System.out.println("new trans 21");
          boolean ifBatchNameExist = false;
          for (int cnt = 0; cnt < batchNameList.size(); cnt++) {
            System.out.println(
                " b name: " + batchNameList.get(cnt).toString() + " compare to: " + batch_name);
            if (batch_name.equals(batchNameList.get(cnt).toString())) {

              ifBatchNameExist = true;

            }

          }
          System.out.println("new trans 22");
          if (!ifBatchNameExist) {
            System.out.println("batch name error: " + batch_name);
            getSessionContext().setRollbackOnly();
            error_data[0] = "invoiceEntry.error.generateErrorBatchNameNotExist";
            error_data[1] = line_read.toString();
            return error_data;

          }



          // Assigning invoice information
          System.out.println("new trans 23");
          invDetails = new ArInvoiceDetails();

          invDetails.setInvCode(null);
          invDetails.setInvDate(inv_date);
          invDetails.setInvNumber(inv_number);
          invDetails.setInvMemo("");
          invDetails.setInvReferenceNumber(ref_number);
          // invDetails.setInvUploadNumber(null);
          invDetails.setInvVoid(EJBCommon.FALSE);
          invDetails.setInvInterest(EJBCommon.FALSE);
          invDetails.setInvDescription(description);
          invDetails.setInvBillToAddress("");
          invDetails.setInvBillToContact("");
          invDetails.setInvBillToAltContact("");
          invDetails.setInvBillToPhone("");
          invDetails.setInvBillingHeader("");
          invDetails.setInvBillingFooter("");
          invDetails.setInvBillingHeader2("");
          invDetails.setInvBillingFooter2("");
          invDetails.setInvBillingHeader3("");
          invDetails.setInvBillingFooter3("");
          invDetails.setInvBillingSignatory("");
          invDetails.setInvSignatoryTitle("");
          invDetails.setInvShipToAddress("");
          invDetails.setInvShipToAltContact("");
          invDetails.setInvShipToPhone("");
          invDetails.setInvLvFreight("");
          invDetails.setInvShipDate(null);
          invDetails.setInvConversionDate(null);
          invDetails.setInvConversionRate(1);
          invDetails.setInvPreviousReading(inv_previous);
          invDetails.setInvPresentReading(inv_present);
          invDetails.setInvDebitMemo(EJBCommon.FALSE);
          invDetails.setInvSubjectToCommission(EJBCommon.FALSE);
          // ref number , questionable
          invDetails.setInvClientPO(ref_number);
          invDetails.setInvEffectivityDate(inv_date);
          invDetails.setInvRecieveDate(inv_date);
          invDetails.setInvCreatedBy(user_acc);
          invDetails.setInvDateCreated(new java.util.Date());
          invDetails.setInvLastModifiedBy(user_acc);
          invDetails.setInvDateLastModified(new java.util.Date());


          Calendar calendar = new GregorianCalendar();
          // passing month-1 because 0-->jan, 1-->feb... 11-->dec
          calendar.setTime(inv_date);
          calendar.set(Calendar.DATE, calendar.getActualMaximum(Calendar.DATE));
          calendar.add(Calendar.DAY_OF_MONTH, 1);

          invDetails.setInvInterestNextRunDate(calendar.getTime());

          // MemoLines checking -------------------------------


          // check memo line name
          sml_name = values[x][2].trim();

          boolean ifSmlExist = false;

          for (int cnt = 0; cnt < memoLineList.size(); cnt++) {
            System.out.println("memo line: " + memoLineList.get(cnt) + " compare to: " + sml_name);
            if (sml_name.equals(memoLineList.get(cnt))) {
              ifSmlExist = true;
            }
          }

          if (ifSmlExist == false || sml_name.equals("")) {

            getSessionContext().setRollbackOnly();
            error_data[0] = "invoiceEntry.error.generateErrorMemoLineNotExist";
            error_data[1] = line_read.toString();
            return error_data;

          }



          // check memo line quantity
          try {
            mml_quantity = Double.parseDouble(values[x][5].trim());
          } catch (NumberFormatException e) {

            getSessionContext().setRollbackOnly();
            error_data[0] = "invoiceEntry.error.generateErrorMemoLineQuantityFormat";
            error_data[1] = line_read.toString();
            return error_data;


          }

          // check memo line unit price
          try {
            mml_unitprice = Double.parseDouble(values[x][6].trim());
          } catch (NumberFormatException e) {

            getSessionContext().setRollbackOnly();
            error_data[0] = "invoiceEntry.error.generateErrorMemoLineUnitPriceFormat";
            error_data[1] = line_read.toString();
            return error_data;

          }

          // check memo line amount
          try {
            mml_amount = Double.parseDouble(values[x][7].trim());
          } catch (NumberFormatException e) {

            getSessionContext().setRollbackOnly();
            error_data[0] = "invoiceEntry.error.generateErrorMemoLineAmountFormat";
            error_data[1] = line_read.toString();
            return error_data;

          }


          ArModInvoiceLineDetails mdetails = new ArModInvoiceLineDetails();

          mdetails.setIlSmlName(sml_name);
          mdetails.setIlDescription(sml_name);
          mdetails.setIlQuantity(mml_quantity);
          mdetails.setIlUnitPrice(mml_unitprice);
          mdetails.setIlAmount(mml_amount);
          mdetails.setIlTax(EJBCommon.FALSE);

          mmlList.add(mdetails);
          invoice_data.put("header", invDetails);
          invoice_data.put("payment term", payment_term);
          invoice_data.put("tax code", tax_code);
          invoice_data.put("wtax code", wtax_code);
          invoice_data.put("currency", "PHP");
          invoice_data.put("cusomer code", cst_code);
          invoice_data.put("batch name", batch_name);
          invoice_data.put("lines", mmlList);

          invoice_list.add(invoice_data);



        }

        // (com.util.ArInvoiceDetails details, String PYT_NM, String TC_NM,
        // String WTC_NM, String FC_NM, String CST_CSTMR_CODE, String IB_NM, ArrayList ilList,
        // boolean isDraft, String SLP_SLSPRSN_CODE, Integer AD_BRNCH, Integer AD_CMPNY)
        //


        /*
         * invoice_data.add("header", invDetails); invoice_data.add("payment term", payment_term);
         * invoice_data.add("tax code", tax_code); invoice_data.add("wtax_code",wtax_code);
         * invoice_data.add("currency", "PHP"); invoice_data.add(cst_code);
         * invoice_data.add("batch name", batch_name); invoice_data.add("lines", mmlList);
         * invoice_data.add("branch", user.getCurrentBranch().getBrCode());
         * invoice_data.add("company", user.getCmpCode());
         *
         *
         */



        line_read++;
      }
      System.out.println("pasok ---- 3");
      if (!new_transaction) {

        System.out.println("pasok ---- 4");
        invoice_data.put("header", invDetails);
        invoice_data.put("payment term", payment_term);
        invoice_data.put("tax code", tax_code);
        invoice_data.put("wtax code", wtax_code);
        invoice_data.put("currency", "PHP");
        invoice_data.put("cusomer code", cst_code);
        invoice_data.put("batch name", batch_name);
        invoice_data.put("lines", mmlList);
        invoice_list.add(invoice_data);
      }



      // no error add all invoice transactions in invoice_list


      System.out.println("invoice entry , no errors found");



      for (int x = 0; x < invoice_list.size(); x++) {

        HashMap invoice_info_data = (HashMap) invoice_list.get(x);

        ArrayList ilList = (ArrayList) invoice_info_data.get("lines");

        ArInvoiceDetails invDetails_data = (ArInvoiceDetails) invoice_info_data.get("header");

        String PYT_NM = (String) invoice_info_data.get("payment term");
        String TC_NM = (String) invoice_info_data.get("tax code");
        String FC_NM = (String) invoice_info_data.get("currency");
        String WTC_NM = (String) invoice_info_data.get("wtax code");
        String CST_CSTMR_CODE = (String) invoice_info_data.get("cusomer code");
        String IB_NM = (String) invoice_info_data.get("batch name");



        arInvoice = arInvoiceHome.create(invDetails_data.getInvType(), EJBCommon.FALSE,
            invDetails_data.getInvDescription(), invDetails_data.getInvDate(),
            invDetails_data.getInvNumber(), invDetails_data.getInvReferenceNumber(),
            invDetails_data.getInvUploadNumber(), null, null, 0d, 0d, 0d, 0d, 0d, 0d,
            invDetails_data.getInvConversionDate(), invDetails_data.getInvConversionRate(),
            invDetails_data.getInvMemo(), invDetails_data.getInvPreviousReading(),
            invDetails_data.getInvPresentReading(), invDetails_data.getInvBillToAddress(),
            invDetails_data.getInvBillToContact(), invDetails_data.getInvBillToAltContact(),
            invDetails_data.getInvBillToPhone(), invDetails_data.getInvBillingHeader(),
            invDetails_data.getInvBillingFooter(), invDetails_data.getInvBillingHeader2(),
            invDetails_data.getInvBillingFooter2(), invDetails_data.getInvBillingHeader3(),
            invDetails_data.getInvBillingFooter3(), invDetails_data.getInvBillingSignatory(),
            invDetails_data.getInvSignatoryTitle(), invDetails_data.getInvShipToAddress(),
            invDetails_data.getInvShipToContact(), invDetails_data.getInvShipToAltContact(),
            invDetails_data.getInvShipToPhone(), invDetails_data.getInvShipDate(),
            invDetails_data.getInvLvFreight(), null, null, EJBCommon.FALSE, null, EJBCommon.FALSE,
            EJBCommon.FALSE, EJBCommon.FALSE, invDetails_data.getInvDisableInterest(),
            EJBCommon.FALSE, null, 0d, null, null, null, null, invDetails_data.getInvCreatedBy(),
            invDetails_data.getInvDateCreated(), invDetails_data.getInvLastModifiedBy(),
            invDetails_data.getInvDateLastModified(), null, null, null, null, EJBCommon.FALSE, null,
            null, null, invDetails_data.getInvDebitMemo(),
            invDetails_data.getInvSubjectToCommission(), null,
            invDetails_data.getInvEffectivityDate(), AD_BRNCH, AD_CMPNY);

        Debug.print("ArInvoiceEntryControllerBean saveArInvEntry C");

        arInvoice.setInvDocumentType(invDetails.getInvDocumentType());

        LocalAdPaymentTerm adPaymentTerm = adPaymentTermHome.findByPytName(PYT_NM, AD_CMPNY);
        // adPaymentTerm.addArInvoice(arInvoice);
        arInvoice.setAdPaymentTerm(adPaymentTerm);

        LocalGlFunctionalCurrency glFunctionalCurrency =
            glFunctionalCurrencyHome.findByFcName(FC_NM, AD_CMPNY);
        // glFunctionalCurrency.addArInvoice(arInvoice);
        arInvoice.setGlFunctionalCurrency(glFunctionalCurrency);

        LocalArTaxCode arTaxCode = arTaxCodeHome.findByTcName(TC_NM, AD_CMPNY);
        // arTaxCode.addArInvoice(arInvoice);
        arInvoice.setArTaxCode(arTaxCode);

        System.out.println("WTC_NM: " + WTC_NM + " COMPNY :" + AD_CMPNY);
        LocalArWithholdingTaxCode arWithholdingTaxCode =
            arWithholdingTaxCodeHome.findByWtcName(WTC_NM, AD_CMPNY);
        System.out.println("PASS 1");

        // arWithholdingTaxCode.addArInvoice(arInvoice);
        arInvoice.setArWithholdingTaxCode(arWithholdingTaxCode);
        System.out.println("2");
        System.out.println("CST_CSTMR_CODE: " + CST_CSTMR_CODE);
        LocalArCustomer arCustomer = arCustomerHome.findByCstCustomerCode(CST_CSTMR_CODE, AD_CMPNY);
        // arCustomer.addArInvoice(arInvoice);
        arInvoice.setArCustomer(arCustomer);

        arInvoice.setInvSubjectToCommission((byte) 0);

        try {

          LocalArInvoiceBatch arInvoiceBatch =
              arInvoiceBatchHome.findByIbName(IB_NM, AD_BRNCH, AD_CMPNY);
          // arInvoiceBatch.addArInvoice(arInvoice);
          arInvoice.setArInvoiceBatch(arInvoiceBatch);

        } catch (FinderException ex) {

        }
        Debug.print("ArInvoiceEntryControllerBean saveArInvEntry C Done");
        double amountDue = 0;

        Debug.print("ArInvoiceEntryControllerBean saveArInvEntry D");



        // remove all invoice line items

        Collection arInvoiceLineItems = arInvoice.getArInvoiceLineItems();

        Iterator i = arInvoiceLineItems.iterator();

        while (i.hasNext()) {

          LocalArInvoiceLineItem arInvoiceLineItem = (LocalArInvoiceLineItem) i.next();

          double convertedQuantity =
              this.convertByUomFromAndItemAndQuantity(arInvoiceLineItem.getInvUnitOfMeasure(),
                  arInvoiceLineItem.getInvItemLocation().getInvItem(),
                  arInvoiceLineItem.getIliQuantity(), AD_CMPNY);
          arInvoiceLineItem.getInvItemLocation().setIlCommittedQuantity(
              arInvoiceLineItem.getInvItemLocation().getIlCommittedQuantity() - convertedQuantity);

          i.remove();

          arInvoiceLineItem.remove();

        }

        // remove all invoice lines

        Collection arInvoiceLines = arInvoice.getArInvoiceLines();

        i = arInvoiceLines.iterator();

        while (i.hasNext()) {

          LocalArInvoiceLine arInvoiceLine = (LocalArInvoiceLine) i.next();

          i.remove();

          arInvoiceLine.remove();

        }

        // remove all sales order lines

        Collection arSalesOrderInvoiceLines = arInvoice.getArSalesOrderInvoiceLines();

        i = arSalesOrderInvoiceLines.iterator();

        while (i.hasNext()) {

          LocalArSalesOrderInvoiceLine arSalesOrderInvoiceLine =
              (LocalArSalesOrderInvoiceLine) i.next();

          i.remove();

          arSalesOrderInvoiceLine.remove();

        }


        // remove all distribution records

        Collection arDistributionRecords = arInvoice.getArDistributionRecords();

        i = arDistributionRecords.iterator();

        while (i.hasNext()) {

          LocalArDistributionRecord arDistributionRecord = (LocalArDistributionRecord) i.next();

          i.remove();

          arDistributionRecord.remove();

        }

        LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);

        // add new invoice lines and distribution record

        double TOTAL_TAX = 0d;
        double TOTAL_LINE = 0d;
        double TOTAL_UNTAXABLE = 0d;

        i = ilList.iterator();

        while (i.hasNext()) {

          ArModInvoiceLineDetails mInvDetails = (ArModInvoiceLineDetails) i.next();

          LocalArInvoiceLine arInvoiceLine = this.addArIlEntry(mInvDetails, arInvoice, AD_CMPNY);


          // add receivable/debit distributions
          double LINE = arInvoiceLine.getIlAmount();
          double TAX = arInvoiceLine.getIlTaxAmount();
          double UNTAXABLE = 0d;
          double W_TAX = 0d;
          double DISCOUNT = 0d;



          if (arInvoiceLine.getIlTax() == EJBCommon.FALSE)
            UNTAXABLE += arInvoiceLine.getIlAmount();


          // compute w-tax per line


          if (arWithholdingTaxCode.getWtcRate() != 0
              && adPreference.getPrfArWTaxRealization().equals("INVOICE")) {

            W_TAX =
                EJBCommon.roundIt((LINE - UNTAXABLE) * (arWithholdingTaxCode.getWtcRate() / 100),
                    this.getGlFcPrecisionUnit(AD_CMPNY));

          }

          if (adPaymentTerm.getPytDiscountOnInvoice() == EJBCommon.TRUE) {

            Collection adPaymentSchedules =
                adPaymentScheduleHome.findByPytCode(adPaymentTerm.getPytCode(), AD_CMPNY);
            ArrayList adPaymentScheduleList = new ArrayList(adPaymentSchedules);
            LocalAdPaymentSchedule adPaymentSchedule =
                (LocalAdPaymentSchedule) adPaymentScheduleList.get(0);

            Collection adDiscounts =
                adDiscountHome.findByPsCode(adPaymentSchedule.getPsCode(), AD_CMPNY);
            ArrayList adDiscountList = new ArrayList(adDiscounts);
            LocalAdDiscount adDiscount = (LocalAdDiscount) adDiscountList.get(0);


            double rate = adDiscount.getDscDiscountPercent();
            DISCOUNT = (LINE + TAX) * (rate / 100d);
          }


          // add revenue/credit distributions

          // Check If Memo Line Type is Service Charge

          if (arInvoiceLine.getArStandardMemoLine().getSmlType().equals("SC")) {

            if (arInvoiceLine.getArStandardMemoLine().getSmlInterimAccount() == null)
              throw new GlobalBranchAccountNumberInvalidException();

            this.addArDrEntry(arInvoice.getArDrNextLine(), "SC", EJBCommon.FALSE,
                arInvoiceLine.getIlAmount(),
                arInvoiceLine.getArStandardMemoLine().getSmlInterimAccount(),
                this.getArGlCoaRevenueAccount(arInvoiceLine, AD_BRNCH, AD_CMPNY), arInvoice,
                AD_BRNCH, AD_CMPNY);
          } else {

            this.addArDrEntry(arInvoice.getArDrNextLine(), "REVENUE", EJBCommon.FALSE,
                arInvoiceLine.getIlAmount(),
                this.getArGlCoaRevenueAccount(arInvoiceLine, AD_BRNCH, AD_CMPNY), null, arInvoice,
                AD_BRNCH, AD_CMPNY);

            this.addArDrEntry(arInvoice.getArDrNextLine(), "RECEIVABLE", EJBCommon.TRUE,
                LINE + TAX - W_TAX - DISCOUNT,
                this.getArGlCoaReceivableAccount(arInvoiceLine, AD_BRNCH, AD_CMPNY), null,
                arInvoice, AD_BRNCH, AD_CMPNY);


          }


          TOTAL_LINE += arInvoiceLine.getIlAmount();
          TOTAL_TAX += arInvoiceLine.getIlTaxAmount();

          if (arInvoiceLine.getIlTax() == EJBCommon.FALSE)
            TOTAL_UNTAXABLE += arInvoiceLine.getIlAmount();

        }


        // add tax distribution if necessary

        if (!arTaxCode.getTcType().equals("NONE") && !arTaxCode.getTcType().equals("EXEMPT")) {

          if (arTaxCode.getTcInterimAccount() == null) {

            // add branch tax code
            LocalAdBranchArTaxCode adBranchTaxCode = null;
            Debug.print("ArInvoiceEntryControllerBean saveArInvIliEntry J");
            try {
              adBranchTaxCode = adBranchArTaxCodeHome.findBtcByTcCodeAndBrCode(
                  arInvoice.getArTaxCode().getTcCode(), AD_BRNCH, AD_CMPNY);

            } catch (FinderException ex) {

            }

            if (adBranchTaxCode != null) {

              this.addArDrEntry(arInvoice.getArDrNextLine(), "TAX", EJBCommon.FALSE, TOTAL_TAX,
                  adBranchTaxCode.getBtcGlCoaTaxCode(), null, arInvoice, AD_BRNCH, AD_CMPNY);

            } else {

              this.addArDrEntry(arInvoice.getArDrNextLine(), "TAX", EJBCommon.FALSE, TOTAL_TAX,
                  arTaxCode.getGlChartOfAccount().getCoaCode(), null, arInvoice, AD_BRNCH,
                  AD_CMPNY);
            }

          } else {

            this.addArDrEntry(arInvoice.getArDrNextLine(), "DEFERRED TAX", EJBCommon.FALSE,
                TOTAL_TAX, arTaxCode.getTcInterimAccount(), null, arInvoice, AD_BRNCH, AD_CMPNY);

          }

        }

        // add wtax distribution if necessary

        adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);

        double W_TAX_AMOUNT = 0d;

        if (arWithholdingTaxCode.getWtcRate() != 0
            && adPreference.getPrfArWTaxRealization().equals("INVOICE")) {

          W_TAX_AMOUNT = EJBCommon.roundIt(
              (TOTAL_LINE - TOTAL_UNTAXABLE) * (arWithholdingTaxCode.getWtcRate() / 100),
              this.getGlFcPrecisionUnit(AD_CMPNY));

          this.addArDrEntry(arInvoice.getArDrNextLine(), "W-TAX", EJBCommon.TRUE, W_TAX_AMOUNT,
              arWithholdingTaxCode.getGlChartOfAccount().getCoaCode(), null, arInvoice, AD_BRNCH,
              AD_CMPNY);

        }

        // add payment discount if necessary

        double DISCOUNT_AMT = 0d;

        if (adPaymentTerm.getPytDiscountOnInvoice() == EJBCommon.TRUE) {

          Collection adPaymentSchedules =
              adPaymentScheduleHome.findByPytCode(adPaymentTerm.getPytCode(), AD_CMPNY);
          ArrayList adPaymentScheduleList = new ArrayList(adPaymentSchedules);
          LocalAdPaymentSchedule adPaymentSchedule =
              (LocalAdPaymentSchedule) adPaymentScheduleList.get(0);

          Collection adDiscounts =
              adDiscountHome.findByPsCode(adPaymentSchedule.getPsCode(), AD_CMPNY);
          ArrayList adDiscountList = new ArrayList(adDiscounts);
          LocalAdDiscount adDiscount = (LocalAdDiscount) adDiscountList.get(0);


          double rate = adDiscount.getDscDiscountPercent();
          DISCOUNT_AMT = (TOTAL_LINE + TOTAL_TAX) * (rate / 100d);

          this.addArDrIliEntry(arInvoice.getArDrNextLine(), "DISCOUNT", EJBCommon.TRUE,
              DISCOUNT_AMT, adPaymentTerm.getGlChartOfAccount().getCoaCode(), arInvoice, AD_BRNCH,
              AD_CMPNY);

        }

        // add receivable distribution

        /*
         * try {
         * 
         * LocalAdBranchCustomer adBranchCustomer =
         * adBranchCustomerHome.findBcstByCstCodeAndBrCode(arInvoice.getArCustomer().getCstCode(),
         * AD_BRNCH, AD_CMPNY);
         * 
         * this.addArDrEntry(arInvoice.getArDrNextLine(), "RECEIVABLE", EJBCommon.TRUE, TOTAL_LINE +
         * TOTAL_TAX - W_TAX_AMOUNT - DISCOUNT_AMT, RECEIVABLE_ACCNT, null, arInvoice, AD_BRNCH,
         * AD_CMPNY);
         * 
         * } catch(FinderException ex) {
         * 
         * }
         */

        // compute invoice amount due

        amountDue = TOTAL_LINE + TOTAL_TAX - W_TAX_AMOUNT - DISCOUNT_AMT;

        // set invoice amount due

        arInvoice.setInvAmountDue(amountDue);

        // remove all payment schedule

        Collection arInvoicePaymentSchedules = arInvoice.getArInvoicePaymentSchedules();

        i = arInvoicePaymentSchedules.iterator();

        while (i.hasNext()) {

          LocalArInvoicePaymentSchedule arInvoicePaymentSchedule =
              (LocalArInvoicePaymentSchedule) i.next();

          i.remove();

          arInvoicePaymentSchedule.remove();

        }


        // create invoice payment schedule

        short precisionUnit = this.getGlFcPrecisionUnit(AD_CMPNY);
        double TOTAL_PAYMENT_SCHEDULE = 0d;

        GregorianCalendar gcPrevDateDue = new GregorianCalendar();
        GregorianCalendar gcDateDue = new GregorianCalendar();
        gcPrevDateDue.setTime(arInvoice.getInvEffectivityDate());

        Collection adPaymentSchedules = adPaymentTerm.getAdPaymentSchedules();

        i = adPaymentSchedules.iterator();

        while (i.hasNext()) {

          LocalAdPaymentSchedule adPaymentSchedule = (LocalAdPaymentSchedule) i.next();

          // get date due

          if (arInvoice.getAdPaymentTerm().getPytScheduleBasis().equals("DEFAULT")) {

            gcDateDue.setTime(arInvoice.getInvEffectivityDate());
            gcDateDue.add(Calendar.DATE, adPaymentSchedule.getPsDueDay());

          } else if (arInvoice.getAdPaymentTerm().getPytScheduleBasis().equals("MONTHLY")) {

            gcDateDue = gcPrevDateDue;
            gcDateDue.add(Calendar.MONTH, 1);
            gcPrevDateDue = gcDateDue;

          } else if (arInvoice.getAdPaymentTerm().getPytScheduleBasis().equals("BI-MONTHLY")) {

            gcDateDue = gcPrevDateDue;

            if (gcPrevDateDue.get(Calendar.MONTH) != 1) {
              if (gcPrevDateDue.getActualMaximum(Calendar.DATE) == 31
                  && gcPrevDateDue.get(Calendar.DATE) > 15
                  && gcPrevDateDue.get(Calendar.DATE) < 31) {
                gcDateDue.add(Calendar.DATE, 16);
              } else {
                gcDateDue.add(Calendar.DATE, 15);
              }
            } else if (gcPrevDateDue.get(Calendar.MONTH) == 1) {
              if (gcPrevDateDue.getActualMaximum(Calendar.DATE) == 28
                  && gcPrevDateDue.get(Calendar.DATE) == 14) {
                gcDateDue.add(Calendar.DATE, 14);
              } else if (gcPrevDateDue.getActualMaximum(Calendar.DATE) == 28
                  && gcPrevDateDue.get(Calendar.DATE) >= 15
                  && gcPrevDateDue.get(Calendar.DATE) < 28) {
                gcDateDue.add(Calendar.DATE, 13);
              } else if (gcPrevDateDue.getActualMaximum(Calendar.DATE) == 29
                  && gcPrevDateDue.get(Calendar.DATE) >= 15
                  && gcPrevDateDue.get(Calendar.DATE) < 29) {
                gcDateDue.add(Calendar.DATE, 14);
              } else {
                gcDateDue.add(Calendar.DATE, 15);
              }
            }

            gcPrevDateDue = gcDateDue;

          }



          // create a payment schedule

          double PAYMENT_SCHEDULE_AMOUNT = 0;

          // if last payment schedule subtract to avoid rounding difference error

          if (i.hasNext()) {

            PAYMENT_SCHEDULE_AMOUNT = EJBCommon.roundIt(
                (adPaymentSchedule.getPsRelativeAmount() / adPaymentTerm.getPytBaseAmount())
                    * arInvoice.getInvAmountDue(),
                precisionUnit);

          } else {

            PAYMENT_SCHEDULE_AMOUNT = arInvoice.getInvAmountDue() - TOTAL_PAYMENT_SCHEDULE;

          }

          LocalArInvoicePaymentSchedule arInvoicePaymentSchedule =
              arInvoicePaymentScheduleHome.create(gcDateDue.getTime(),
                  adPaymentSchedule.getPsLineNumber(), PAYMENT_SCHEDULE_AMOUNT, 0d, EJBCommon.FALSE,
                  (short) 0, gcDateDue.getTime(), 0d, 0d, AD_CMPNY);

          arInvoice.addArInvoicePaymentSchedule(arInvoicePaymentSchedule);

          TOTAL_PAYMENT_SCHEDULE += PAYMENT_SCHEDULE_AMOUNT;

        }



        Debug.print("ArInvoiceEntryControllerBean saveArInvEntry D Done");
        // generate approval status

        String INV_APPRVL_STATUS = null;

        Debug.print("ArInvoiceEntryControllerBean saveArInvEntry E");



      }



    } catch (Exception ex) {

      System.out.println("pasok ---- 5");

      Debug.printStackTrace(ex);
      getSessionContext().setRollbackOnly();
      throw new EJBException(ex.getMessage());

    }

    return new String[2];
  }



  //
  /**
   * @ejb:interface-method view-type="remote"
   **/
  public void saveRecievedDate(
      com.util.ArInvoiceDetails details, Integer AD_BRNCH, Integer AD_CMPNY
  ) {
    Debug.print("ArInvoiceEntryControllerBean saveRecievedDate");

    LocalArInvoiceHome arInvoiceHome = null;
    LocalArInvoice arInvoice = null;
    // Initialize EJB Home

    try {

      arInvoiceHome = (LocalArInvoiceHome) EJBHomeFactory
          .lookUpLocalHome(LocalArInvoiceHome.JNDI_NAME, LocalArInvoiceHome.class);

      arInvoice = arInvoiceHome.findByPrimaryKey(details.getInvCode());

      arInvoice.setInvRecieveDate(details.getInvRecieveDate());

    } catch (NamingException ex) {

      throw new EJBException(ex.getMessage());

    } catch (FinderException ex) {

      // throw new GlobalRecordAlreadyDeletedException();

    }


  }


  /**
   * @ejb:interface-method view-type="remote"
   **/
  public void saveDisableInterest(
      com.util.ArInvoiceDetails details, Integer AD_BRNCH, Integer AD_CMPNY
  ) {
    Debug.print("ArInvoiceEntryControllerBean saveDisableInterest");

    LocalArInvoiceHome arInvoiceHome = null;
    LocalArInvoice arInvoice = null;
    // Initialize EJB Home

    try {

      arInvoiceHome = (LocalArInvoiceHome) EJBHomeFactory
          .lookUpLocalHome(LocalArInvoiceHome.JNDI_NAME, LocalArInvoiceHome.class);

      arInvoice = arInvoiceHome.findByPrimaryKey(details.getInvCode());


      arInvoice.setInvDisableInterest(details.getInvDisableInterest());

    } catch (NamingException ex) {

      throw new EJBException(ex.getMessage());

    } catch (FinderException ex) {

      // throw new GlobalRecordAlreadyDeletedException();

    }


  }


  /**
   * @ejb:interface-method view-type="remote"
   **/
  public Integer saveArInvIliEntry(
      com.util.ArInvoiceDetails details, String PYT_NM, String TC_NM, String WTC_NM, String FC_NM,
      String CST_CSTMR_CODE, String IB_NM, ArrayList iliList, boolean isDraft,
      String SLP_SLSPRSN_CODE, String DB_NM, Integer AD_BRNCH, Integer AD_CMPNY
  ) throws GlobalRecordAlreadyDeletedException, GlobalDocumentNumberNotUniqueException,
      GlobalConversionDateNotExistException, GlobalPaymentTermInvalidException,
      ArINVAmountExceedsCreditLimitException, GlobalTransactionAlreadyApprovedException,
      GlobalTransactionAlreadyPendingException, GlobalTransactionAlreadyPostedException,
      GlobalTransactionAlreadyVoidException, GlobalNoApprovalRequesterFoundException,
      GlobalNoApprovalApproverFoundException, GlobalInvItemLocationNotFoundException,
      GlJREffectiveDateNoPeriodExistException, GlJREffectiveDatePeriodClosedException,
      GlobalJournalNotBalanceException, GlobalInventoryDateException,
      GlobalBranchAccountNumberInvalidException, AdPRFCoaGlVarianceAccountNotFoundException,
      GlobalRecordInvalidException, GlobalExpiryDateNotFoundException,
      GlobalMiscInfoIsRequiredException, ArInvDuplicateUploadNumberException {

    Debug.print("ArInvoiceEntryControllerBean saveArInvIliEntry");

    LocalArInvoiceHome arInvoiceHome = null;
    LocalHrEmployeeHome hrEmployeeHome = null;
    LocalHrDeployedBranchHome hrDeployedBranchHome = null;
    LocalArInvoiceBatchHome arInvoiceBatchHome = null;
    LocalAdCompanyHome adCompanyHome = null;
    LocalGlFunctionalCurrencyHome glFunctionalCurrencyHome = null;
    LocalAdDocumentSequenceAssignmentHome adDocumentSequenceAssignmentHome = null;
    LocalAdBranchDocumentSequenceAssignmentHome adBranchDocumentSequenceAssignmentHome = null;
    LocalAdPaymentTermHome adPaymentTermHome = null;
    LocalAdPaymentScheduleHome adPaymentScheduleHome = null;
    LocalAdDiscountHome adDiscountHome = null;
    LocalArTaxCodeHome arTaxCodeHome = null;
    LocalArWithholdingTaxCodeHome arWithholdingTaxCodeHome = null;
    LocalArCustomerHome arCustomerHome = null;
    LocalGlFunctionalCurrencyRateHome glFunctionalCurrencyRateHome = null;
    LocalArInvoicePaymentScheduleHome arInvoicePaymentScheduleHome = null;
    LocalAdApprovalHome adApprovalHome = null;
    LocalAdAmountLimitHome adAmountLimitHome = null;
    LocalAdApprovalUserHome adApprovalUserHome = null;
    LocalAdApprovalQueueHome adApprovalQueueHome = null;
    LocalAdApprovalDocumentHome adApprovalDocumentHome = null;
    LocalAdPreferenceHome adPreferenceHome = null;
    LocalInvItemLocationHome invItemLocationHome = null;
    LocalInvCostingHome invCostingHome = null;
    LocalInvItemHome invItemHome = null;
    LocalGlChartOfAccountHome glChartOfAccountHome = null;
    LocalArCustomerBalanceHome arCustomerBalanceHome = null;
    LocalArReceiptHome arReceiptHome = null;
    LocalAdBranchItemLocationHome adBranchItemLocationHome = null;
    LocalAdBranchCustomerHome adBranchCustomerHome = null;
    LocalArSalespersonHome arSalespersonHome = null;
    LocalArInvoiceLineItemHome arInvoiceLineItemHome = null;
    LocalGlSetOfBookHome glSetOfBookHome = null;
    LocalArInvoice arInvoice = null;
    LocalAdBranchArTaxCodeHome adBranchArTaxCodeHome = null;

    InvRepItemCostingControllerHome homeRIC = null;
    InvRepItemCostingController ejbRIC = null;
    
    InvItemEntryControllerHome homeII = null;
    InvItemEntryController ejbII = null;

    // Initialize EJB Home

    Date txnStartDate = new Date();

    try {

      homeRIC = (InvRepItemCostingControllerHome) com.util.EJBHomeFactory
          .lookUpHome("ejb/InvRepItemCostingControllerEJB", InvRepItemCostingControllerHome.class);
          
        homeII = (InvItemEntryControllerHome) com.util.EJBHomeFactory
          .lookUpHome("ejb/InvItemEntryControllerEJB", InvItemEntryControllerHome.class);    

      adBranchArTaxCodeHome = (LocalAdBranchArTaxCodeHome) EJBHomeFactory
          .lookUpLocalHome(LocalAdBranchArTaxCodeHome.JNDI_NAME, LocalAdBranchArTaxCodeHome.class);

      hrDeployedBranchHome = (LocalHrDeployedBranchHome) EJBHomeFactory
          .lookUpLocalHome(LocalHrDeployedBranchHome.JNDI_NAME, LocalHrDeployedBranchHome.class);
      hrEmployeeHome = (LocalHrEmployeeHome) EJBHomeFactory
          .lookUpLocalHome(LocalHrEmployeeHome.JNDI_NAME, LocalHrEmployeeHome.class);

      arInvoiceHome = (LocalArInvoiceHome) EJBHomeFactory
          .lookUpLocalHome(LocalArInvoiceHome.JNDI_NAME, LocalArInvoiceHome.class);


      arInvoiceBatchHome = (LocalArInvoiceBatchHome) EJBHomeFactory
          .lookUpLocalHome(LocalArInvoiceBatchHome.JNDI_NAME, LocalArInvoiceBatchHome.class);
      adCompanyHome = (LocalAdCompanyHome) EJBHomeFactory
          .lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
      glFunctionalCurrencyHome = (LocalGlFunctionalCurrencyHome) EJBHomeFactory.lookUpLocalHome(
          LocalGlFunctionalCurrencyHome.JNDI_NAME, LocalGlFunctionalCurrencyHome.class);
      adDocumentSequenceAssignmentHome = (LocalAdDocumentSequenceAssignmentHome) EJBHomeFactory
          .lookUpLocalHome(LocalAdDocumentSequenceAssignmentHome.JNDI_NAME,
              LocalAdDocumentSequenceAssignmentHome.class);
      adBranchDocumentSequenceAssignmentHome =
          (LocalAdBranchDocumentSequenceAssignmentHome) EJBHomeFactory.lookUpLocalHome(
              LocalAdBranchDocumentSequenceAssignmentHome.JNDI_NAME,
              LocalAdBranchDocumentSequenceAssignmentHome.class);
      adPaymentTermHome = (LocalAdPaymentTermHome) EJBHomeFactory
          .lookUpLocalHome(LocalAdPaymentTermHome.JNDI_NAME, LocalAdPaymentTermHome.class);
      adPaymentScheduleHome = (LocalAdPaymentScheduleHome) EJBHomeFactory
          .lookUpLocalHome(LocalAdPaymentScheduleHome.JNDI_NAME, LocalAdPaymentScheduleHome.class);
      adDiscountHome = (LocalAdDiscountHome) EJBHomeFactory
          .lookUpLocalHome(LocalAdDiscountHome.JNDI_NAME, LocalAdDiscountHome.class);
      arTaxCodeHome = (LocalArTaxCodeHome) EJBHomeFactory
          .lookUpLocalHome(LocalArTaxCodeHome.JNDI_NAME, LocalArTaxCodeHome.class);
      arWithholdingTaxCodeHome = (LocalArWithholdingTaxCodeHome) EJBHomeFactory.lookUpLocalHome(
          LocalArWithholdingTaxCodeHome.JNDI_NAME, LocalArWithholdingTaxCodeHome.class);
      arCustomerHome = (LocalArCustomerHome) EJBHomeFactory
          .lookUpLocalHome(LocalArCustomerHome.JNDI_NAME, LocalArCustomerHome.class);
      glFunctionalCurrencyRateHome =
          (LocalGlFunctionalCurrencyRateHome) EJBHomeFactory.lookUpLocalHome(
              LocalGlFunctionalCurrencyRateHome.JNDI_NAME, LocalGlFunctionalCurrencyRateHome.class);
      arInvoicePaymentScheduleHome =
          (LocalArInvoicePaymentScheduleHome) EJBHomeFactory.lookUpLocalHome(
              LocalArInvoicePaymentScheduleHome.JNDI_NAME, LocalArInvoicePaymentScheduleHome.class);
      adApprovalHome = (LocalAdApprovalHome) EJBHomeFactory
          .lookUpLocalHome(LocalAdApprovalHome.JNDI_NAME, LocalAdApprovalHome.class);
      adAmountLimitHome = (LocalAdAmountLimitHome) EJBHomeFactory
          .lookUpLocalHome(LocalAdAmountLimitHome.JNDI_NAME, LocalAdAmountLimitHome.class);
      adApprovalUserHome = (LocalAdApprovalUserHome) EJBHomeFactory
          .lookUpLocalHome(LocalAdApprovalUserHome.JNDI_NAME, LocalAdApprovalUserHome.class);
      adApprovalQueueHome = (LocalAdApprovalQueueHome) EJBHomeFactory
          .lookUpLocalHome(LocalAdApprovalQueueHome.JNDI_NAME, LocalAdApprovalQueueHome.class);
      adApprovalDocumentHome = (LocalAdApprovalDocumentHome) EJBHomeFactory.lookUpLocalHome(
          LocalAdApprovalDocumentHome.JNDI_NAME, LocalAdApprovalDocumentHome.class);
      adPreferenceHome = (LocalAdPreferenceHome) EJBHomeFactory
          .lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);
      invItemLocationHome = (LocalInvItemLocationHome) EJBHomeFactory
          .lookUpLocalHome(LocalInvItemLocationHome.JNDI_NAME, LocalInvItemLocationHome.class);
      invCostingHome = (LocalInvCostingHome) EJBHomeFactory
          .lookUpLocalHome(LocalInvCostingHome.JNDI_NAME, LocalInvCostingHome.class);
      invItemHome = (LocalInvItemHome) EJBHomeFactory.lookUpLocalHome(LocalInvItemHome.JNDI_NAME,
          LocalInvItemHome.class);
      glChartOfAccountHome = (LocalGlChartOfAccountHome) EJBHomeFactory
          .lookUpLocalHome(LocalGlChartOfAccountHome.JNDI_NAME, LocalGlChartOfAccountHome.class);
      arCustomerBalanceHome = (LocalArCustomerBalanceHome) EJBHomeFactory
          .lookUpLocalHome(LocalArCustomerBalanceHome.JNDI_NAME, LocalArCustomerBalanceHome.class);
      arReceiptHome = (LocalArReceiptHome) EJBHomeFactory
          .lookUpLocalHome(LocalArReceiptHome.JNDI_NAME, LocalArReceiptHome.class);
      adBranchItemLocationHome = (LocalAdBranchItemLocationHome) EJBHomeFactory.lookUpLocalHome(
          LocalAdBranchItemLocationHome.JNDI_NAME, LocalAdBranchItemLocationHome.class);
      adBranchCustomerHome = (LocalAdBranchCustomerHome) EJBHomeFactory
          .lookUpLocalHome(LocalAdBranchCustomerHome.JNDI_NAME, LocalAdBranchCustomerHome.class);
      arSalespersonHome = (LocalArSalespersonHome) EJBHomeFactory
          .lookUpLocalHome(LocalArSalespersonHome.JNDI_NAME, LocalArSalespersonHome.class);
      arInvoiceLineItemHome = (LocalArInvoiceLineItemHome) EJBHomeFactory
          .lookUpLocalHome(LocalArInvoiceLineItemHome.JNDI_NAME, LocalArInvoiceLineItemHome.class);
      glSetOfBookHome = (LocalGlSetOfBookHome) EJBHomeFactory
          .lookUpLocalHome(LocalGlSetOfBookHome.JNDI_NAME, LocalGlSetOfBookHome.class);

    } catch (NamingException ex) {

      throw new EJBException(ex.getMessage());

    }

    try {
      ejbRIC = homeRIC.create();
      ejbII = homeII.create();
    } catch (RemoteException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    } catch (CreateException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }


    int lineNumberError = 0;

    try {

      // validate if invoice is already deleted

      try {

        if (details.getInvCode() != null) {

          arInvoice = arInvoiceHome.findByPrimaryKey(details.getInvCode());

        }

      } catch (FinderException ex) {

        throw new GlobalRecordAlreadyDeletedException();

      }

      // validate if invoice is already posted, void, arproved or pending

      if (details.getInvCode() != null) {
        System.out.println("invoice code is : " + details.getInvCode());
        if (arInvoice.getInvApprovalStatus() != null) {

          if (arInvoice.getInvApprovalStatus().equals("APPROVED")
              || arInvoice.getInvApprovalStatus().equals("N/A")) {

            throw new GlobalTransactionAlreadyApprovedException();


          } else if (arInvoice.getInvApprovalStatus().equals("PENDING")) {

            throw new GlobalTransactionAlreadyPendingException();

          }

        }

        if (arInvoice.getInvPosted() == EJBCommon.TRUE) {

          throw new GlobalTransactionAlreadyPostedException();

        } else if (arInvoice.getInvVoid() == EJBCommon.TRUE) {

          throw new GlobalTransactionAlreadyVoidException();

        }

      }

      // invoice void

      if (details.getInvCode() != null && details.getInvVoid() == EJBCommon.TRUE
          && arInvoice.getInvPosted() == EJBCommon.FALSE) {

        arInvoice.setInvVoid(EJBCommon.TRUE);
        arInvoice.setInvLastModifiedBy(details.getInvLastModifiedBy());
        arInvoice.setInvDateLastModified(details.getInvDateLastModified());

        return arInvoice.getInvCode();

      }

      // validate if document number is unique document number is automatic then set next sequence

      LocalAdBranchDocumentSequenceAssignment adBranchDocumentSequenceAssignment = null;
      LocalAdDocumentSequenceAssignment adDocumentSequenceAssignment = null;

      Debug.print("ArInvoiceEntryControllerBean saveArInvIliEntry A");
      if (details.getInvCode() == null) {

        String documentType = details.getInvDocumentType();

        try {
          if (documentType != null) {
            adDocumentSequenceAssignment =
                adDocumentSequenceAssignmentHome.findByDcName(documentType, AD_CMPNY);
          } else {
            documentType = "AR INVOICE";
          }
        } catch (FinderException ex) {
          documentType = "AR INVOICE";
        }



        try {

          adDocumentSequenceAssignment =
              adDocumentSequenceAssignmentHome.findByDcName(documentType, AD_CMPNY);

        } catch (FinderException ex) {

        }

        try {

          adBranchDocumentSequenceAssignment =
              adBranchDocumentSequenceAssignmentHome.findBdsByDsaCodeAndBrCode(
                  adDocumentSequenceAssignment.getDsaCode(), AD_BRNCH, AD_CMPNY);

        } catch (FinderException ex) {

        }

        LocalArInvoice arExistingInvoice = null;

        try {
     

          arExistingInvoice = arInvoiceHome.findByInvNumberAndInvCreditMemoAndBrCode(
              details.getInvNumber(), EJBCommon.FALSE, AD_BRNCH, AD_CMPNY);

        } catch (FinderException ex) {
          System.out.println("FinderException EX=" + ex.getMessage());

        }

        if (arExistingInvoice != null) {

          throw new GlobalDocumentNumberNotUniqueException();

        }
     
        // Confirm if Upload Number have duplicate

        if (details.getInvUploadNumber() != null) {
          if (!details.getInvUploadNumber().trim().equals("")) {
            try {

              arInvoiceHome.findByUploadNumberAndCompanyCode(details.getInvUploadNumber(),
                  AD_CMPNY);


              // throw exception if found duplicate upload number
              throw new ArInvDuplicateUploadNumberException();


            } catch (FinderException ex) {

            }
          }



        }
      } else {

        LocalArInvoice arExistingInvoice = null;

        try {

          arExistingInvoice = arInvoiceHome.findByInvNumberAndInvCreditMemoAndBrCode(
              details.getInvNumber(), EJBCommon.FALSE, AD_BRNCH, AD_CMPNY);

        } catch (FinderException ex) {
        }

        if (arExistingInvoice != null
            && !arExistingInvoice.getInvCode().equals(details.getInvCode())) {

          throw new GlobalDocumentNumberNotUniqueException();

        }

        if (arInvoice.getInvNumber() != details.getInvNumber()
            && (details.getInvNumber() == null || details.getInvNumber().trim().length() == 0)) {

          details.setInvNumber(arInvoice.getInvNumber());

        }



        // Confirm if Upload Number have duplicate

        if (details.getInvUploadNumber() != null) {


          if (!details.getInvUploadNumber().trim().equals("")) {
            if (!details.getInvUploadNumber().equals(arExistingInvoice.getInvUploadNumber())) {
              try {
                arInvoiceHome.findByUploadNumberAndCompanyCode(details.getInvUploadNumber(),
                    AD_CMPNY);


                // throw exception if found duplicate upload number
                throw new ArInvDuplicateUploadNumberException();


              } catch (FinderException ex) {

              }
            }
          }


        }

      }

      Debug.print("ArInvoiceEntryControllerBean saveArInvIliEntry A Done");

      // validate if conversion date exists

      try {

        if (details.getInvConversionDate() != null) {


          LocalGlFunctionalCurrency glValidateFunctionalCurrency =
              glFunctionalCurrencyHome.findByFcName(FC_NM, AD_CMPNY);
          LocalGlFunctionalCurrencyRate glFunctionalCurrencyRate = glFunctionalCurrencyRateHome
              .findByFcCodeAndDate(glValidateFunctionalCurrency.getFcCode(),
                  details.getInvConversionDate(), AD_CMPNY);

        }

      } catch (FinderException ex) {

        throw new GlobalConversionDateNotExistException();

      }

      // validate if payment term has at least one payment schedule

      if (adPaymentTermHome.findByPytName(PYT_NM, AD_CMPNY).getAdPaymentSchedules().isEmpty()) {

        throw new GlobalPaymentTermInvalidException();

      }

      // used in checking if invoice should re-generate distribution records and re-calculate taxes
      boolean isRecalculate = true;

      // create invoice

      Debug.print("ArInvoiceEntryControllerBean saveArInvIliEntry B");
      if (details.getInvCode() == null) {
        System.out.println("CHECKING B");

        arInvoice = arInvoiceHome.create(details.getInvType(), EJBCommon.FALSE,
            details.getInvDescription(), details.getInvDate(), details.getInvNumber(),
            details.getInvReferenceNumber(), details.getInvUploadNumber(), null, null, 0d,
            details.getInvDownPayment(), 0d, 0d, 0d, 0d, details.getInvConversionDate(),
            details.getInvConversionRate(), details.getInvMemo(), details.getInvPreviousReading(),
            details.getInvPresentReading(), details.getInvBillToAddress(),
            details.getInvBillToContact(), details.getInvBillToAltContact(),
            details.getInvBillToPhone(), details.getInvBillingHeader(),
            details.getInvBillingFooter(), details.getInvBillingHeader2(),
            details.getInvBillingFooter2(), details.getInvBillingHeader3(),
            details.getInvBillingFooter3(), details.getInvBillingSignatory(),
            details.getInvSignatoryTitle(), details.getInvShipToAddress(),
            details.getInvShipToContact(), details.getInvShipToAltContact(),
            details.getInvShipToPhone(), details.getInvShipDate(), details.getInvLvFreight(), null,
            null, EJBCommon.FALSE, null, EJBCommon.FALSE, EJBCommon.FALSE, EJBCommon.FALSE,
            EJBCommon.FALSE, EJBCommon.FALSE, null, 0d, null, null, null, null,
            details.getInvCreatedBy(), details.getInvDateCreated(), details.getInvLastModifiedBy(),
            details.getInvDateLastModified(), null, null, null, null, EJBCommon.FALSE,
            details.getInvLvShift(), null, null, details.getInvDebitMemo(),
            details.getInvSubjectToCommission(), details.getInvClientPO(),
            details.getInvEffectivityDate(), AD_BRNCH, AD_CMPNY);

      } else {

        // check if critical fields are changed

        if (!arInvoice.getArTaxCode().getTcName().equals(TC_NM)
            || !arInvoice.getArWithholdingTaxCode().getWtcName().equals(WTC_NM)
            || !arInvoice.getArCustomer().getCstCustomerCode().equals(CST_CSTMR_CODE)
            || !arInvoice.getAdPaymentTerm().getPytName().equals(PYT_NM)
            || !arInvoice.getInvDate().equals(details.getInvDate())
            || !arInvoice.getInvEffectivityDate().equals(details.getInvEffectivityDate())
            || arInvoice.getInvDownPayment() != details.getInvDownPayment() ||

            iliList.size() != arInvoice.getArInvoiceLineItems().size()) {
          System.out.println("First");
          // isRecalculate = true;

        } else if (iliList.size() == arInvoice.getArInvoiceLineItems().size()) {

          Iterator iliIter = arInvoice.getArInvoiceLineItems().iterator();
          Iterator iliListIter = iliList.iterator();
          System.out.println("Second");
          while (iliIter.hasNext()) {

            LocalArInvoiceLineItem arInvoiceLineItem = (LocalArInvoiceLineItem) iliIter.next();
            ArModInvoiceLineItemDetails mdetails = (ArModInvoiceLineItemDetails) iliListIter.next();


            if (!arInvoiceLineItem.getInvItemLocation().getInvItem().getIiName()
                .equals(mdetails.getIliIiName())
                || !arInvoiceLineItem.getInvItemLocation().getInvLocation().getLocName()
                    .equals(mdetails.getIliLocName())
                || !arInvoiceLineItem.getInvUnitOfMeasure().getUomName()
                    .equals(mdetails.getIliUomName())
                || arInvoiceLineItem.getIliQuantity() != mdetails.getIliQuantity()
                || arInvoiceLineItem.getIliUnitPrice() != mdetails.getIliUnitPrice()
                || arInvoiceLineItem.getIliTotalDiscount() != mdetails.getIliTotalDiscount()
                || arInvoiceLineItem.getIliTax() != mdetails.getIliTax()) {

              System.out.println("Second Checkpoint");
              // isRecalculate = true;
              break;

            }

            // isRecalculate = false;

          }

        } else {

          // isRecalculate = false;

        }

        arInvoice.setInvDownPayment(details.getInvDownPayment());
        arInvoice.setInvDocumentType(details.getInvDocumentType());
        arInvoice.setInvType(details.getInvType());
        arInvoice.setInvDocumentType(details.getInvDocumentType());
        arInvoice.setInvDescription(details.getInvDescription());
        arInvoice.setInvDate(details.getInvDate());
        arInvoice.setInvMemo(details.getInvMemo());
        arInvoice.setInvNumber(details.getInvNumber());
        arInvoice.setInvReferenceNumber(details.getInvReferenceNumber());
        arInvoice.setInvUploadNumber(details.getInvUploadNumber());
        arInvoice.setInvConversionDate(details.getInvConversionDate());
        arInvoice.setInvConversionRate(details.getInvConversionRate());
        arInvoice.setInvPreviousReading(arInvoice.getInvPreviousReading());
        arInvoice.setInvPresentReading(arInvoice.getInvPresentReading());
        arInvoice.setInvBillToAddress(details.getInvBillToAddress());
        arInvoice.setInvBillToContact(details.getInvBillToContact());
        arInvoice.setInvBillToAltContact(details.getInvBillToAltContact());
        arInvoice.setInvBillToPhone(details.getInvBillToPhone());
        arInvoice.setInvBillingHeader(details.getInvBillingHeader());
        arInvoice.setInvBillingFooter(details.getInvBillingFooter());
        arInvoice.setInvBillingHeader2(details.getInvBillingHeader2());
        arInvoice.setInvBillingFooter2(details.getInvBillingFooter2());
        arInvoice.setInvBillingHeader3(details.getInvBillingHeader3());
        arInvoice.setInvBillingFooter3(details.getInvBillingFooter3());
        arInvoice.setInvBillingSignatory(details.getInvBillingSignatory());
        arInvoice.setInvSignatoryTitle(details.getInvSignatoryTitle());
        arInvoice.setInvShipToAddress(details.getInvShipToAddress());
        arInvoice.setInvShipToContact(details.getInvShipToContact());
        arInvoice.setInvShipToAltContact(details.getInvShipToAltContact());
        arInvoice.setInvShipToPhone(details.getInvShipToPhone());
        arInvoice.setInvShipDate(details.getInvShipDate());
        arInvoice.setInvLvFreight(details.getInvLvFreight());
        arInvoice.setInvLastModifiedBy(details.getInvLastModifiedBy());
        arInvoice.setInvDateLastModified(details.getInvDateLastModified());
        arInvoice.setInvReasonForRejection(null);
        arInvoice.setInvLvShift(details.getInvLvShift());
        arInvoice.setInvDebitMemo(details.getInvDebitMemo());
        arInvoice.setInvSubjectToCommission(details.getInvSubjectToCommission());
        arInvoice.setInvClientPO(details.getInvClientPO());
        arInvoice.setInvEffectivityDate(details.getInvEffectivityDate());
        arInvoice.setInvRecieveDate(details.getInvRecieveDate());
        System.out.println("CHECK C");


      }

      arInvoice.setInvDocumentType(details.getInvDocumentType());
      arInvoice.setReportParameter(details.getReportParameter());
      Debug.print("ArInvoiceEntryControllerBean saveArInvIliEntry C");
      LocalAdPaymentTerm adPaymentTerm = adPaymentTermHome.findByPytName(PYT_NM, AD_CMPNY);
      // adPaymentTerm.addArInvoice(arInvoice);
      arInvoice.setAdPaymentTerm(adPaymentTerm);

      LocalGlFunctionalCurrency glFunctionalCurrency =
          glFunctionalCurrencyHome.findByFcName(FC_NM, AD_CMPNY);
      // glFunctionalCurrency.addArInvoice(arInvoice);
      arInvoice.setGlFunctionalCurrency(glFunctionalCurrency);

      LocalArTaxCode arTaxCode = arTaxCodeHome.findByTcName(TC_NM, AD_CMPNY);
      // arTaxCode.addArInvoice(arInvoice);
      arInvoice.setArTaxCode(arTaxCode);

      LocalArWithholdingTaxCode arWithholdingTaxCode =
          arWithholdingTaxCodeHome.findByWtcName(WTC_NM, AD_CMPNY);
      // arWithholdingTaxCode.addArInvoice(arInvoice);
      arInvoice.setArWithholdingTaxCode(arWithholdingTaxCode);

      LocalArCustomer arCustomer = arCustomerHome.findByCstCustomerCode(CST_CSTMR_CODE, AD_CMPNY);
      // arCustomer.addArInvoice(arInvoice);
      arInvoice.setArCustomer(arCustomer);


      try {

        LocalArInvoiceBatch arInvoiceBatch =
            arInvoiceBatchHome.findByIbName(IB_NM, AD_BRNCH, AD_CMPNY);
        // arInvoiceBatch.addArInvoice(arInvoice);
        arInvoice.setArInvoiceBatch(arInvoiceBatch);

      } catch (FinderException ex) {

      }

      try {

        LocalHrDeployedBranch hrDeployedBranch =
            hrDeployedBranchHome.findByDbClientName(DB_NM, AD_CMPNY);

        arInvoice.setHrDeployedBranch(hrDeployedBranch);
      } catch (FinderException ex) {

      }


      if (arInvoice.getArCustomer().getHrEmployee() != null) {

        arInvoice.setInvHrManagingBranch(
            arInvoice.getArCustomer().getHrEmployee().getEmpManagingBranch());
        arInvoice.setInvHrCurrentJobPosition(
            arInvoice.getArCustomer().getHrEmployee().getEmpCurrentJobPosition());

      }



      LocalArSalesperson arSalesperson = null;

      if (SLP_SLSPRSN_CODE != null && SLP_SLSPRSN_CODE.length() > 0
          && !SLP_SLSPRSN_CODE.equalsIgnoreCase("NO RECORD FOUND")) {

        // if he tagged a salesperson for this invoice
        arSalesperson = arSalespersonHome.findBySlpSalespersonCode(SLP_SLSPRSN_CODE, AD_CMPNY);

        // arSalesperson.addArInvoice(arInvoice);
        arInvoice.setArSalesperson(arSalesperson);

      } else {

        // if he untagged a salesperson for this invoice
        if (arInvoice.getArSalesperson() != null) {

          arSalesperson = arSalespersonHome.findBySlpSalespersonCode(
              arInvoice.getArSalesperson().getSlpSalespersonCode(), AD_CMPNY);
          arSalesperson.dropArInvoice(arInvoice);

        }

        // if no salesperson is set, invoice should not be subject to commission
        arInvoice.setInvSubjectToCommission((byte) 0);

      }



      Debug.print("ArInvoiceEntryControllerBean saveArInvIliEntry C Done");
      double amountDue = 0;

      LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);
      System.out.println(isRecalculate);
      if (isRecalculate) {
        System.out.println("RECALC!");
        // remove all invoice line items

        Collection arInvoiceLineItems = arInvoice.getArInvoiceLineItems();

        Iterator i = arInvoiceLineItems.iterator();
        Debug.print("ArInvoiceEntryControllerBean saveArInvIliEntry D");
        while (i.hasNext()) {

          LocalArInvoiceLineItem arInvoiceLineItem = (LocalArInvoiceLineItem) i.next();

          if (arInvoiceLineItem.getIliEnableAutoBuild() == 1 && arInvoiceLineItem
              .getInvItemLocation().getInvItem().getIiClass().equals("Assembly")) {

            double QTY_SLD =
                this.convertByUomFromAndItemAndQuantity(arInvoiceLineItem.getInvUnitOfMeasure(),
                    arInvoiceLineItem.getInvItemLocation().getInvItem(),
                    arInvoiceLineItem.getIliQuantity(), AD_CMPNY);

            Collection invBillOfMaterials =
                arInvoiceLineItem.getInvItemLocation().getInvItem().getInvBillOfMaterials();

            Iterator j = invBillOfMaterials.iterator();

            while (j.hasNext()) {

              LocalInvBillOfMaterial invBillOfMaterial = (LocalInvBillOfMaterial) j.next();

              LocalInvItemLocation invItemLocation = invItemLocationHome.findByLocNameAndIiName(
                  invBillOfMaterial.getBomLocName(), invBillOfMaterial.getBomIiName(), AD_CMPNY);

              // bom conversion
              double convertedQuantity = this.convertByUomFromAndItemAndQuantity(
                  invBillOfMaterial.getInvUnitOfMeasure(), invItemLocation.getInvItem(),
                  EJBCommon.roundIt(invBillOfMaterial.getBomQuantityNeeded() * QTY_SLD,
                      this.getInvGpQuantityPrecisionUnit(AD_CMPNY)),
                  AD_CMPNY);

              invItemLocation.setIlCommittedQuantity(
                  invItemLocation.getIlCommittedQuantity() - convertedQuantity);

            }

          } else {

            double convertedQuantity =
                this.convertByUomFromAndItemAndQuantity(arInvoiceLineItem.getInvUnitOfMeasure(),
                    arInvoiceLineItem.getInvItemLocation().getInvItem(),
                    arInvoiceLineItem.getIliQuantity(), AD_CMPNY);
            arInvoiceLineItem.getInvItemLocation().setIlCommittedQuantity(
                arInvoiceLineItem.getInvItemLocation().getIlCommittedQuantity()
                    - convertedQuantity);
          }

          i.remove();

          arInvoiceLineItem.remove();

        }
        Debug.print("ArInvoiceEntryControllerBean saveArInvIliEntry D Done");
        // remove all invoice lines

        Collection arInvoiceLines = arInvoice.getArInvoiceLines();

        i = arInvoiceLines.iterator();

        while (i.hasNext()) {

          LocalArInvoiceLine arInvoiceLine = (LocalArInvoiceLine) i.next();

          i.remove();

          arInvoiceLine.remove();

        }

        // remove all sales order lines

        Collection arSalesOrderInvoiceLines = arInvoice.getArSalesOrderInvoiceLines();

        i = arSalesOrderInvoiceLines.iterator();

        while (i.hasNext()) {

          LocalArSalesOrderInvoiceLine arSalesOrderInvoiceLine =
              (LocalArSalesOrderInvoiceLine) i.next();

          i.remove();

          arSalesOrderInvoiceLine.remove();

        }

        // remove all job order lines

        Collection arJobOrderInvoiceLines = arInvoice.getArJobOrderInvoiceLines();

        i = arJobOrderInvoiceLines.iterator();

        while (i.hasNext()) {

          LocalArJobOrderInvoiceLine arJobOrderInvoiceLine = (LocalArJobOrderInvoiceLine) i.next();

          i.remove();

          arJobOrderInvoiceLine.remove();

        }

        // remove all distribution records

        Collection arDistributionRecords = arInvoice.getArDistributionRecords();

        i = arDistributionRecords.iterator();

        while (i.hasNext()) {

          LocalArDistributionRecord arDistributionRecord = (LocalArDistributionRecord) i.next();

          i.remove();

          arDistributionRecord.remove();

        }

        // add new invoice line item and distribution record

        double TOTAL_TAX = 0d;
        double TOTAL_LINE = 0d;
        double TOTAL_SA_DEBIT = 0d;
        double TOTAL_DOWN_PAYMENT = arInvoice.getInvDownPayment();

        i = iliList.iterator();

        LocalInvItemLocation invItemLocation = null;
        Debug.print("ArInvoiceEntryControllerBean saveArInvIliEntry E");
        while (i.hasNext()) {

          ArModInvoiceLineItemDetails mIliDetails = (ArModInvoiceLineItemDetails) i.next();
          Debug.print("ArInvoiceEntryControllerBean saveArInvIliEntry E01");
          try {

            invItemLocation = invItemLocationHome.findByLocNameAndIiName(
                mIliDetails.getIliLocName(), mIliDetails.getIliIiName(), AD_CMPNY);

          } catch (FinderException ex) {

            throw new GlobalInvItemLocationNotFoundException(
                String.valueOf(mIliDetails.getIliLine()));

          }

          // start date validation

          if (adPreference.getPrfArAllowPriorDate() == EJBCommon.FALSE) {

            Collection invNegTxnCosting =
                invCostingHome.findNegTxnByGreaterThanCstDateAndIiNameAndLocName(
                    arInvoice.getInvDate(), invItemLocation.getInvItem().getIiName(),
                    invItemLocation.getInvLocation().getLocName(), AD_BRNCH, AD_CMPNY);
            if (!invNegTxnCosting.isEmpty())
              throw new GlobalInventoryDateException(invItemLocation.getInvItem().getIiName());

          }
          Debug.print("ArInvoiceEntryControllerBean saveArInvIliEntry E02");

          LocalArInvoiceLineItem arInvoiceLineItem =
              this.addArIliEntry(mIliDetails, arInvoice, invItemLocation, AD_CMPNY);

          // add cost of sales distribution and inventory

          double COST = arInvoiceLineItem.getInvItemLocation().getInvItem().getIiUnitCost();
          
         
   
          if (invItemLocation.getInvItem().getIiCostMethod().equals("Average")) {

            COST = ejbII.getInvIiUnitCostByIiNameAndUomName(mIliDetails.getIliIiName(), mIliDetails.getIliUomName(),  arInvoice.getInvDate(), AD_CMPNY);
      
          }
          if (invItemLocation.getInvItem().getIiCostMethod().equals("FIFO")) {

            COST = this.getInvFifoCost(arInvoice.getInvDate(), invItemLocation.getIlCode(),
                arInvoiceLineItem.getIliQuantity(), arInvoiceLineItem.getIliUnitPrice(), false,
                AD_BRNCH, AD_CMPNY);

          } else if (invItemLocation.getInvItem().getIiCostMethod()
              .equals("Standard")) {

            COST = invItemLocation.getInvItem().getIiUnitCost();
          }



          double QTY_SLD =
              this.convertByUomFromAndItemAndQuantity(arInvoiceLineItem.getInvUnitOfMeasure(),
                  arInvoiceLineItem.getInvItemLocation().getInvItem(),
                  arInvoiceLineItem.getIliQuantity(), AD_CMPNY);
          Debug.print("ArInvoiceEntryControllerBean saveArInvIliEntry E04");
          LocalAdBranchItemLocation adBranchItemLocation = null;

          System.out.println("COST=" + COST);
          System.out.println("QTY_SLD=" + QTY_SLD);
          try {

            lineNumberError = arInvoiceLineItem.getIliLine();
            adBranchItemLocation = adBranchItemLocationHome.findBilByIlCodeAndBrCode(
                arInvoiceLineItem.getInvItemLocation().getIlCode(), AD_BRNCH, AD_CMPNY);

          } catch (FinderException ex) {

          }

          Debug.print("ArInvoiceEntryControllerBean saveArInvIliEntry E05");
          if (adPreference.getPrfArAutoComputeCogs() == EJBCommon.TRUE && arInvoiceLineItem
              .getInvItemLocation().getInvItem().getIiNonInventoriable() == EJBCommon.FALSE) {

            if (arInvoiceLineItem.getIliEnableAutoBuild() == EJBCommon.FALSE) {

              if (adBranchItemLocation != null) {

                this.addArDrIliEntry(arInvoice.getArDrNextLine(), "COGS", EJBCommon.TRUE,
                    COST * QTY_SLD, adBranchItemLocation.getBilCoaGlCostOfSalesAccount(), arInvoice,
                    AD_BRNCH, AD_CMPNY);

                this.addArDrIliEntry(arInvoice.getArDrNextLine(), "INVENTORY", EJBCommon.FALSE,
                    COST * QTY_SLD, adBranchItemLocation.getBilCoaGlInventoryAccount(), arInvoice,
                    AD_BRNCH, AD_CMPNY);
                Debug.print("ArInvoiceEntryControllerBean saveArInvIliEntry E06");
              } else {

                this.addArDrIliEntry(arInvoice.getArDrNextLine(), "COGS", EJBCommon.TRUE,
                    COST * QTY_SLD,
                    arInvoiceLineItem.getInvItemLocation().getIlGlCoaCostOfSalesAccount(),
                    arInvoice, AD_BRNCH, AD_CMPNY);

                this.addArDrIliEntry(arInvoice.getArDrNextLine(), "INVENTORY", EJBCommon.FALSE,
                    COST * QTY_SLD,
                    arInvoiceLineItem.getInvItemLocation().getIlGlCoaInventoryAccount(), arInvoice,
                    AD_BRNCH, AD_CMPNY);
                Debug.print("ArInvoiceEntryControllerBean saveArInvIliEntry E07");
              }



              // add quantity to item location committed quantity

              double convertedQuantity =
                  this.convertByUomFromAndItemAndQuantity(arInvoiceLineItem.getInvUnitOfMeasure(),
                      arInvoiceLineItem.getInvItemLocation().getInvItem(),
                      arInvoiceLineItem.getIliQuantity(), AD_CMPNY);
              invItemLocation.setIlCommittedQuantity(
                  invItemLocation.getIlCommittedQuantity() + convertedQuantity);

            }

          }

          double LINE = this.convertForeignToFunctionalCurrency(
              arInvoice.getGlFunctionalCurrency().getFcCode(),
              arInvoice.getGlFunctionalCurrency().getFcName(), arInvoice.getInvConversionDate(),
              arInvoice.getInvConversionRate(), arInvoiceLineItem.getIliAmount(), AD_CMPNY);


          // add inventory sale distributions
          Debug.print("ArInvoiceEntryControllerBean saveArInvIliEntry F");
          if (adBranchItemLocation != null) {
            /*
             * this.addArDrIliEntry(arInvoice.getArDrNextLine(), "REVENUE", EJBCommon.FALSE,
             * arInvoiceLineItem.getIliAmount(), adBranchItemLocation.getBilCoaGlSalesAccount(),
             * arInvoice, AD_BRNCH, AD_CMPNY);
             * 
             */
            // this will trigger by services(DEBIT)
            if (adBranchItemLocation.getInvItemLocation().getInvItem()
                .getIiServices() == EJBCommon.TRUE) {

              this.addArDrIliEntry(arInvoice.getArDrNextLine(), "REVENUE", EJBCommon.TRUE, LINE,
                  adBranchItemLocation.getBilCoaGlSalesAccount(), arInvoice, AD_BRNCH, AD_CMPNY);

              this.addArDrIliEntry(arInvoice.getArDrNextLine(), "OTHER", EJBCommon.FALSE, LINE,
                  adBranchItemLocation.getBilCoaGlSalesReturnAccount(), arInvoice, AD_BRNCH,
                  AD_CMPNY);



              TOTAL_SA_DEBIT += LINE;
            } else {
              this.addArDrIliEntry(arInvoice.getArDrNextLine(), "REVENUE", EJBCommon.FALSE, LINE,
                  adBranchItemLocation.getBilCoaGlSalesAccount(), arInvoice, AD_BRNCH, AD_CMPNY);

            }



          } else {

            // this will trigger by services(DEBIT)
            if (arInvoiceLineItem.getInvItemLocation().getInvItem()
                .getIiServices() == EJBCommon.TRUE) {

              this.addArDrIliEntry(arInvoice.getArDrNextLine(), "REVENUE", EJBCommon.TRUE, LINE,
                  arInvoiceLineItem.getInvItemLocation().getIlGlCoaSalesAccount(), arInvoice,
                  AD_BRNCH, AD_CMPNY);

              this.addArDrIliEntry(arInvoice.getArDrNextLine(), "OTHER", EJBCommon.FALSE, LINE,
                  arInvoiceLineItem.getInvItemLocation().getIlGlCoaSalesReturnAccount(), arInvoice,
                  AD_BRNCH, AD_CMPNY);

              TOTAL_SA_DEBIT += arInvoiceLineItem.getIliAmount();
            } else {
              this.addArDrIliEntry(arInvoice.getArDrNextLine(), "REVENUE", EJBCommon.FALSE, LINE,
                  arInvoiceLineItem.getInvItemLocation().getIlGlCoaSalesAccount(), arInvoice,
                  AD_BRNCH, AD_CMPNY);

            }



          }
          Debug.print("ArInvoiceEntryControllerBean saveArInvIliEntry F Done");
          TOTAL_LINE += arInvoiceLineItem.getIliAmount();
          TOTAL_TAX += arInvoiceLineItem.getIliTaxAmount();

          // if auto build is enabled
          Debug.print("ArInvoiceEntryControllerBean saveArInvIliEntry G");
          if (arInvoiceLineItem.getIliEnableAutoBuild() == EJBCommon.TRUE
              && arInvoiceLineItem.getInvItemLocation().getInvItem().getIiClass().equals("Assembly")
              && adPreference.getPrfArAllowPriorDate() == EJBCommon.FALSE) {

            byte DEBIT = EJBCommon.TRUE;

            double TOTAL_AMOUNT = 0;
            double ABS_TOTAL_AMOUNT = 0;

            Collection invBillOfMaterials =
                arInvoiceLineItem.getInvItemLocation().getInvItem().getInvBillOfMaterials();

            Iterator j = invBillOfMaterials.iterator();

            while (j.hasNext()) {

              LocalInvBillOfMaterial invBillOfMaterial = (LocalInvBillOfMaterial) j.next();

              // add bill of material quantity needed to item location

              LocalInvItemLocation invIlRawMaterial = null;
              Debug.print("ArInvoiceEntryControllerBean saveArInvIliEntry G01");
              try {

                invIlRawMaterial = invItemLocationHome.findByLocNameAndIiName(
                    invBillOfMaterial.getBomLocName(), invBillOfMaterial.getBomIiName(), AD_CMPNY);
                Debug.print("ArInvoiceEntryControllerBean saveArInvIliEntry G02");
              } catch (FinderException ex) {

                throw new GlobalInvItemLocationNotFoundException(
                    String.valueOf(mIliDetails.getIliLine()) + " - Raw Mat. ("
                        + invBillOfMaterial.getBomIiName() + ")");

              }
              // bom conversion
              double convertedQuantity = this.convertByUomFromAndItemAndQuantity(
                  invBillOfMaterial.getInvUnitOfMeasure(), invIlRawMaterial.getInvItem(),
                  EJBCommon.roundIt(invBillOfMaterial.getBomQuantityNeeded() * QTY_SLD,
                      this.getInvGpQuantityPrecisionUnit(AD_CMPNY)),
                  AD_CMPNY);
              Debug.print("ArInvoiceEntryControllerBean saveArInvIliEntry G03");
              // start date validation

              Collection invIlRawMatNegTxnCosting =
                  invCostingHome.findNegTxnByGreaterThanCstDateAndIiNameAndLocName(
                      arInvoice.getInvDate(), invIlRawMaterial.getInvItem().getIiName(),
                      invIlRawMaterial.getInvLocation().getLocName(), AD_BRNCH, AD_CMPNY);
              if (!invIlRawMatNegTxnCosting.isEmpty())
                throw new GlobalInventoryDateException(invItemLocation.getInvItem().getIiName()
                    + " - Raw Mat. (" + invIlRawMaterial.getInvItem().getIiName() + ")");
              Debug.print("ArInvoiceEntryControllerBean saveArInvIliEntry G04");
              invIlRawMaterial.setIlCommittedQuantity(
                  invIlRawMaterial.getIlCommittedQuantity() + convertedQuantity);

              LocalInvItem invItem =
                  invItemHome.findByIiName(invBillOfMaterial.getBomIiName(), AD_CMPNY);

              double COSTING = 0d;

              try {

                LocalInvCosting invCosting = invCostingHome
                    .getByMaxCstDateToLongAndMaxCstLineNumberAndLessThanEqualCstDateAndIiNameAndLocName(
                        arInvoice.getInvDate(), invItem.getIiName(),
                        invBillOfMaterial.getBomLocName(), AD_BRNCH, AD_CMPNY);



                // check if rmning vl is not zero and rmng qty is 0
                if (invCosting.getCstRemainingQuantity() <= 0) {

                  HashMap criteria = new HashMap();
                  criteria.put("itemName", invItemLocation.getInvItem().getIiName());
                  criteria.put("location", invItemLocation.getInvLocation().getLocName());

                  ArrayList branchList = new ArrayList();

                  AdBranchDetails mdetails = new AdBranchDetails();
                  mdetails.setBrCode(AD_BRNCH);
                  branchList.add(mdetails);

                  ejbRIC.executeInvFixItemCosting(criteria, branchList, AD_CMPNY);

                  invCosting = invCostingHome
                      .getByMaxCstDateToLongAndMaxCstLineNumberAndLessThanEqualCstDateAndIiNameAndLocName(
                          arInvoice.getInvDate(), invItemLocation.getInvItem().getIiName(),
                          invItemLocation.getInvLocation().getLocName(), AD_BRNCH, AD_CMPNY);



                }


                try {

                  if (invCosting.getCstRemainingQuantity() <= 0) {
                    COSTING = Math.abs(
                        invCosting.getCstRemainingValue() / invCosting.getCstRemainingQuantity());
                  } else {
                    COSTING = invItem.getIiUnitCost();
                  }


                } catch (Exception ex) {
                  COSTING = invItem.getIiUnitCost();
                }
                Debug.print("ArInvoiceEntryControllerBean saveArInvIliEntry G05");
              } catch (FinderException ex) {

                COSTING = invItem.getIiUnitCost();

              }

              // bom conversion
              COSTING = this.convertCostByUom(invIlRawMaterial.getInvItem().getIiName(),
                  invBillOfMaterial.getInvUnitOfMeasure().getUomName(), COSTING, true, AD_CMPNY);

              double BOM_AMOUNT = EJBCommon.roundIt(
                  (QTY_SLD * (invBillOfMaterial.getBomQuantityNeeded() * COSTING)),
                  this.getGlFcPrecisionUnit(AD_CMPNY));

              TOTAL_AMOUNT += BOM_AMOUNT;
              ABS_TOTAL_AMOUNT += Math.abs(BOM_AMOUNT);

              // add inventory account

              LocalAdBranchItemLocation adBilRawMaterial = null;

              try {

                adBilRawMaterial = adBranchItemLocationHome
                    .findBilByIlCodeAndBrCode(invIlRawMaterial.getIlCode(), AD_BRNCH, AD_CMPNY);
                Debug.print("ArInvoiceEntryControllerBean saveArInvIliEntry G06");
              } catch (FinderException ex) {

              }


              if (adBilRawMaterial != null) {

                this.addArDrIliEntry(arInvoice.getArDrNextLine(), "INVENTORY", EJBCommon.FALSE,
                    Math.abs(BOM_AMOUNT), adBilRawMaterial.getBilCoaGlInventoryAccount(), arInvoice,
                    AD_BRNCH, AD_CMPNY);
                Debug.print("ArInvoiceEntryControllerBean saveArInvIliEntry G07");
              } else {

                this.addArDrIliEntry(arInvoice.getArDrNextLine(), "INVENTORY", EJBCommon.FALSE,
                    Math.abs(BOM_AMOUNT), invIlRawMaterial.getIlGlCoaInventoryAccount(), arInvoice,
                    AD_BRNCH, AD_CMPNY);
                Debug.print("ArInvoiceEntryControllerBean saveArInvIliEntry G08");
              }

            }

            // add cost of sales account

            if (adBranchItemLocation != null) {

              this.addArDrIliEntry(arInvoice.getArDrNextLine(), "COGS", EJBCommon.TRUE,
                  Math.abs(TOTAL_AMOUNT), adBranchItemLocation.getBilCoaGlCostOfSalesAccount(),
                  arInvoice, AD_BRNCH, AD_CMPNY);
              Debug.print("ArInvoiceEntryControllerBean saveArInvIliEntry G09");
            } else {

              this.addArDrIliEntry(arInvoice.getArDrNextLine(), "COGS", EJBCommon.TRUE,
                  Math.abs(TOTAL_AMOUNT),
                  arInvoiceLineItem.getInvItemLocation().getIlGlCoaCostOfSalesAccount(), arInvoice,
                  AD_BRNCH, AD_CMPNY);
              Debug.print("ArInvoiceEntryControllerBean saveArInvIliEntry G10");
            }


          }
          Debug.print("ArInvoiceEntryControllerBean saveArInvIliEntry G Done");

        }
        Debug.print("ArInvoiceEntryControllerBean saveArInvIliEntry E Done");
        // add tax distribution if necessary



        double TOTAL_TAX_CONV =
            this.convertForeignToFunctionalCurrency(arInvoice.getGlFunctionalCurrency().getFcCode(),
                arInvoice.getGlFunctionalCurrency().getFcName(), arInvoice.getInvConversionDate(),
                arInvoice.getInvConversionRate(), TOTAL_TAX, AD_CMPNY);


        double TOTAL_LINE_CONV =
            this.convertForeignToFunctionalCurrency(arInvoice.getGlFunctionalCurrency().getFcCode(),
                arInvoice.getGlFunctionalCurrency().getFcName(), arInvoice.getInvConversionDate(),
                arInvoice.getInvConversionRate(), TOTAL_LINE, AD_CMPNY);


        double TOTAL_DOWN_PAYMENT_CONV =
            this.convertForeignToFunctionalCurrency(arInvoice.getGlFunctionalCurrency().getFcCode(),
                arInvoice.getGlFunctionalCurrency().getFcName(), arInvoice.getInvConversionDate(),
                arInvoice.getInvConversionRate(), TOTAL_DOWN_PAYMENT, AD_CMPNY);

        if (!arTaxCode.getTcType().equals("NONE") && !arTaxCode.getTcType().equals("EXEMPT")) {

          if (arTaxCode.getTcInterimAccount() == null) {



            // add branch tax code
            LocalAdBranchArTaxCode adBranchTaxCode = null;
            Debug.print("ArInvoiceEntryControllerBean saveArInvIliEntry J");
            try {
              adBranchTaxCode = adBranchArTaxCodeHome.findBtcByTcCodeAndBrCode(
                  arInvoice.getArTaxCode().getTcCode(), AD_BRNCH, AD_CMPNY);

            } catch (FinderException ex) {

            }

            if (adBranchTaxCode != null) {

              this.addArDrEntry(arInvoice.getArDrNextLine(), "TAX", EJBCommon.FALSE, TOTAL_TAX_CONV,
                  adBranchTaxCode.getBtcGlCoaTaxCode(), null, arInvoice, AD_BRNCH, AD_CMPNY);

            } else {

              this.addArDrEntry(arInvoice.getArDrNextLine(), "TAX", EJBCommon.FALSE, TOTAL_TAX_CONV,
                  arTaxCode.getGlChartOfAccount().getCoaCode(), null, arInvoice, AD_BRNCH,
                  AD_CMPNY);
            }

          } else {

            this.addArDrEntry(arInvoice.getArDrNextLine(), "DEFERRED TAX", EJBCommon.FALSE,
                TOTAL_TAX_CONV, arTaxCode.getTcInterimAccount(), null, arInvoice, AD_BRNCH,
                AD_CMPNY);

          }

        }

        // add un earned interest
        double UNEARNED_INT_AMOUNT = 0d;

        if (arInvoice.getArCustomer().getCstAutoComputeInterest() == EJBCommon.TRUE
            && arInvoice.getArCustomer().getCstMonthlyInterestRate() > 0
            && adPaymentTerm.getPytEnableInterest() == EJBCommon.TRUE) {

          try {

            LocalAdBranchCustomer adBranchCustomer =
                adBranchCustomerHome.findBcstByCstCodeAndBrCode(
                    arInvoice.getArCustomer().getCstCode(), AD_BRNCH, AD_CMPNY);

            System.out.println("adPaymentTerm.getAdPaymentSchedules().size()="
                + adPaymentTerm.getAdPaymentSchedules().size());

            UNEARNED_INT_AMOUNT = EJBCommon.roundIt(
                (TOTAL_LINE + TOTAL_TAX - TOTAL_DOWN_PAYMENT)
                    * adPaymentTerm.getAdPaymentSchedules().size()
                    * (arInvoice.getArCustomer().getCstMonthlyInterestRate() / 100),
                this.getGlFcPrecisionUnit(AD_CMPNY));


            double UNEARNED_INT_AMOUNT_CONV = this.convertForeignToFunctionalCurrency(
                arInvoice.getGlFunctionalCurrency().getFcCode(),
                arInvoice.getGlFunctionalCurrency().getFcName(), arInvoice.getInvConversionDate(),
                arInvoice.getInvConversionRate(), UNEARNED_INT_AMOUNT, AD_CMPNY);

            this.addArDrEntry(arInvoice.getArDrNextLine(), "UNINTEREST", EJBCommon.FALSE,
                UNEARNED_INT_AMOUNT_CONV, adBranchCustomer.getBcstGlCoaUnEarnedInterestAccount(),
                null, arInvoice, AD_BRNCH, AD_CMPNY);

            this.addArDrIliEntry(arInvoice.getArDrNextLine(), "RECEIVABLE INTEREST", EJBCommon.TRUE,
                UNEARNED_INT_AMOUNT_CONV, adBranchCustomer.getBcstGlCoaReceivableAccount(),
                arInvoice, AD_BRNCH, AD_CMPNY);

          } catch (FinderException ex) {

          }


        }

        arInvoice.setInvAmountUnearnedInterest(UNEARNED_INT_AMOUNT);


        // add wtax distribution if necessary

        double W_TAX_AMOUNT = 0d;
        Debug.print("ArInvoiceEntryControllerBean saveArInvIliEntry H");
        if (arWithholdingTaxCode.getWtcRate() != 0
            && adPreference.getPrfArWTaxRealization().equals("INVOICE")) {

          W_TAX_AMOUNT = EJBCommon.roundIt(TOTAL_LINE * (arWithholdingTaxCode.getWtcRate() / 100),
              this.getGlFcPrecisionUnit(AD_CMPNY));

          double W_TAX_AMOUNT_CONV = this.convertForeignToFunctionalCurrency(
              arInvoice.getGlFunctionalCurrency().getFcCode(),
              arInvoice.getGlFunctionalCurrency().getFcName(), arInvoice.getInvConversionDate(),
              arInvoice.getInvConversionRate(), W_TAX_AMOUNT, AD_CMPNY);

          this.addArDrEntry(arInvoice.getArDrNextLine(), "W-TAX", EJBCommon.TRUE, W_TAX_AMOUNT_CONV,
              arWithholdingTaxCode.getGlChartOfAccount().getCoaCode(), null, arInvoice, AD_BRNCH,
              AD_CMPNY);

        }



        // add payment discount if necessary
        ////////////////// DISCOUNT
        double DISCOUNT_AMT = 0d;
        Debug.print("ArInvoiceEntryControllerBean saveArInvIliEntry I");
        if (adPaymentTerm.getPytDiscountOnInvoice() == EJBCommon.TRUE) {

          Collection adPaymentSchedules =
              adPaymentScheduleHome.findByPytCode(adPaymentTerm.getPytCode(), AD_CMPNY);
          ArrayList adPaymentScheduleList = new ArrayList(adPaymentSchedules);
          LocalAdPaymentSchedule adPaymentSchedule =
              (LocalAdPaymentSchedule) adPaymentScheduleList.get(0);

          Collection adDiscounts =
              adDiscountHome.findByPsCode(adPaymentSchedule.getPsCode(), AD_CMPNY);
          ArrayList adDiscountList = new ArrayList(adDiscounts);
          LocalAdDiscount adDiscount = (LocalAdDiscount) adDiscountList.get(0);

          double rate = adDiscount.getDscDiscountPercent();
          DISCOUNT_AMT = (TOTAL_LINE + TOTAL_TAX) * (rate / 100d);

          double DISCOUNT_AMT_CONV = this.convertForeignToFunctionalCurrency(
              arInvoice.getGlFunctionalCurrency().getFcCode(),
              arInvoice.getGlFunctionalCurrency().getFcName(), arInvoice.getInvConversionDate(),
              arInvoice.getInvConversionRate(), DISCOUNT_AMT, AD_CMPNY);


          this.addArDrEntry(arInvoice.getArDrNextLine(), "DISCOUNT", EJBCommon.TRUE,
              DISCOUNT_AMT_CONV, adPaymentTerm.getGlChartOfAccount().getCoaCode(), null, arInvoice,
              AD_BRNCH, AD_CMPNY);

        }


        amountDue = TOTAL_LINE + TOTAL_TAX - W_TAX_AMOUNT - DISCOUNT_AMT + UNEARNED_INT_AMOUNT;

        // add receivable distribution
        Debug.print("ArInvoiceEntryControllerBean saveArInvIliEntry J");
        try {

          LocalAdBranchCustomer adBranchCustomer = adBranchCustomerHome.findBcstByCstCodeAndBrCode(
              arInvoice.getArCustomer().getCstCode(), AD_BRNCH, AD_CMPNY);
          // ADDED " - TOTAL_SA_DEBIT for Services(DEBIT)

          double AMNT_DUE_CONV = this.convertForeignToFunctionalCurrency(
              arInvoice.getGlFunctionalCurrency().getFcCode(),
              arInvoice.getGlFunctionalCurrency().getFcName(), arInvoice.getInvConversionDate(),
              arInvoice.getInvConversionRate(), amountDue, AD_CMPNY);

          this.addArDrIliEntry(arInvoice.getArDrNextLine(), "RECEIVABLE", EJBCommon.TRUE,
              AMNT_DUE_CONV, adBranchCustomer.getBcstGlCoaReceivableAccount(), arInvoice, AD_BRNCH,
              AD_CMPNY);

        } catch (FinderException ex) {

        }



        // compute invoice amount due



        // set invoice amount due

        arInvoice.setInvAmountDue(amountDue);

        // remove all payment schedule

        Collection arInvoicePaymentSchedules = arInvoice.getArInvoicePaymentSchedules();

        i = arInvoicePaymentSchedules.iterator();

        while (i.hasNext()) {

          LocalArInvoicePaymentSchedule arInvoicePaymentSchedule =
              (LocalArInvoicePaymentSchedule) i.next();

          i.remove();

          arInvoicePaymentSchedule.remove();

        }

        // create invoice payment schedule

        System.out.println("create invoice payment schedule  ");

        short precisionUnit = this.getGlFcPrecisionUnit(AD_CMPNY);
        double TOTAL_PAYMENT_SCHEDULE = 0d;
        double TOTAL_PAYMENT_INT = 0d;
        double TOTAL_PAYMENT_SCHEDULE_INT = TOTAL_LINE + TOTAL_TAX - TOTAL_DOWN_PAYMENT;


        GregorianCalendar gcPrevDateDue = new GregorianCalendar();
        GregorianCalendar gcDateDue = new GregorianCalendar();
        System.out.println(arInvoice.getInvEffectivityDate());
        gcPrevDateDue.setTime(arInvoice.getInvEffectivityDate());

        Collection adPaymentSchedules = adPaymentTerm.getAdPaymentSchedules();

        boolean first = true;

        i = adPaymentSchedules.iterator();
        Debug.print("ArInvoiceEntryControllerBean saveArInvIliEntry K");
        while (i.hasNext()) {

          LocalAdPaymentSchedule adPaymentSchedule = (LocalAdPaymentSchedule) i.next();

          // get date due

          if (arInvoice.getAdPaymentTerm().getPytScheduleBasis().equals("DEFAULT")) {

            gcDateDue.setTime(arInvoice.getInvEffectivityDate());
            gcDateDue.add(Calendar.DATE, adPaymentSchedule.getPsDueDay());

          } else if (arInvoice.getAdPaymentTerm().getPytScheduleBasis().equals("MONTHLY")) {

            gcDateDue = gcPrevDateDue;
            gcDateDue.add(Calendar.MONTH, 1);
            gcPrevDateDue = gcDateDue;

          } else if (arInvoice.getAdPaymentTerm().getPytScheduleBasis().equals("BI-MONTHLY")) {

            gcDateDue = gcPrevDateDue;

            System.out.println("gcPrevDateDue=" + gcPrevDateDue);
            System.out
                .println("gcPrevDateDue.get(Calendar.MONTH)=" + gcPrevDateDue.get(Calendar.MONTH));

            if (gcPrevDateDue.get(Calendar.MONTH) != 1) {
              if (gcPrevDateDue.getActualMaximum(Calendar.DATE) == 31
                  && gcPrevDateDue.get(Calendar.DATE) > 15
                  && gcPrevDateDue.get(Calendar.DATE) < 31) {
                gcDateDue.add(Calendar.DATE, 16);
              } else {
                gcDateDue.add(Calendar.DATE, 15);
              }
            } else if (gcPrevDateDue.get(Calendar.MONTH) == 1) {
              if (gcPrevDateDue.getActualMaximum(Calendar.DATE) == 28
                  && gcPrevDateDue.get(Calendar.DATE) == 14) {
                gcDateDue.add(Calendar.DATE, 14);
              } else if (gcPrevDateDue.getActualMaximum(Calendar.DATE) == 28
                  && gcPrevDateDue.get(Calendar.DATE) >= 15
                  && gcPrevDateDue.get(Calendar.DATE) < 28) {
                gcDateDue.add(Calendar.DATE, 13);
              } else if (gcPrevDateDue.getActualMaximum(Calendar.DATE) == 29
                  && gcPrevDateDue.get(Calendar.DATE) >= 15
                  && gcPrevDateDue.get(Calendar.DATE) < 29) {
                gcDateDue.add(Calendar.DATE, 14);
              } else {
                gcDateDue.add(Calendar.DATE, 15);
              }
            }

            gcPrevDateDue = gcDateDue;

          }

          if (first == true && arInvoice.getInvDownPayment() > 0) {

            LocalArInvoicePaymentSchedule arInvoicePaymentScheduleDownPayment =
                arInvoicePaymentScheduleHome.create(arInvoice.getInvEffectivityDate(), (short) 0,
                    arInvoice.getInvDownPayment(), 0d, EJBCommon.FALSE, (short) 0,
                    arInvoice.getInvEffectivityDate(), 0d, 0d, AD_CMPNY);

            arInvoice.addArInvoicePaymentSchedule(arInvoicePaymentScheduleDownPayment);
            first = false;
          }

          // create a payment schedule


          double PAYMENT_SCHEDULE_AMOUNT = 0;
          double PAYMENT_SCHEDULE_INT = 0;
          double PAYMENT_SCHEDULE_PRINCIPAL = 0;


          System.out.println("adPaymentTerm.getPytMonthlyInterestRate() ="
              + adPaymentTerm.getPytMonthlyInterestRate());

          // if last payment schedule subtract to avoid rounding difference error

          if (i.hasNext()) {

            PAYMENT_SCHEDULE_AMOUNT =
                EJBCommon.roundIt(
                    (adPaymentSchedule.getPsRelativeAmount() / adPaymentTerm.getPytBaseAmount())
                        * (arInvoice.getInvAmountDue() - arInvoice.getInvDownPayment()),
                    precisionUnit);
            System.out
                .println("------------>TOTAL_PAYMENT_SCHEDULE_INT=" + TOTAL_PAYMENT_SCHEDULE_INT);


            PAYMENT_SCHEDULE_INT = EJBCommon.roundIt(
                (TOTAL_PAYMENT_SCHEDULE_INT) * (adPaymentTerm.getPytMonthlyInterestRate() / 100),
                precisionUnit);

            PAYMENT_SCHEDULE_PRINCIPAL = -(PAYMENT_SCHEDULE_AMOUNT) + PAYMENT_SCHEDULE_INT;


          } else {

            System.out
                .println("------------>TOTAL_PAYMENT_SCHEDULE_INT=" + TOTAL_PAYMENT_SCHEDULE_INT);


            PAYMENT_SCHEDULE_AMOUNT = (arInvoice.getInvAmountDue() - arInvoice.getInvDownPayment())
                - TOTAL_PAYMENT_SCHEDULE;

            PAYMENT_SCHEDULE_INT = EJBCommon.roundIt(
                (TOTAL_PAYMENT_SCHEDULE_INT) * (adPaymentTerm.getPytMonthlyInterestRate() / 100),
                precisionUnit);

            PAYMENT_SCHEDULE_PRINCIPAL = -(PAYMENT_SCHEDULE_AMOUNT) + PAYMENT_SCHEDULE_INT;


          }

          LocalArInvoicePaymentSchedule arInvoicePaymentSchedule =
              arInvoicePaymentScheduleHome.create(gcDateDue.getTime(),
                  adPaymentSchedule.getPsLineNumber(), PAYMENT_SCHEDULE_AMOUNT, 0d, EJBCommon.FALSE,
                  (short) 0, gcDateDue.getTime(), 0d, 0d, AD_CMPNY);


          arInvoicePaymentSchedule.setIpsInterestDue(PAYMENT_SCHEDULE_INT);
          System.out.println("PAYMENT_SCHEDULE_INT=" + PAYMENT_SCHEDULE_INT);
          arInvoice.addArInvoicePaymentSchedule(arInvoicePaymentSchedule);

          TOTAL_PAYMENT_SCHEDULE += PAYMENT_SCHEDULE_AMOUNT;
          TOTAL_PAYMENT_INT += PAYMENT_SCHEDULE_INT;
          TOTAL_PAYMENT_SCHEDULE_INT += PAYMENT_SCHEDULE_PRINCIPAL;

        }

        Debug.print("ArInvoiceEntryControllerBean saveArInvIliEntry L");

      } else {

        Iterator i = iliList.iterator();

        LocalInvItemLocation invItemLocation = null;
        Debug.print("ArInvoiceEntryControllerBean saveArInvIliEntry M");
        while (i.hasNext()) {

          ArModInvoiceLineItemDetails mIliDetails = (ArModInvoiceLineItemDetails) i.next();

          try {

            invItemLocation = invItemLocationHome.findByLocNameAndIiName(
                mIliDetails.getIliLocName(), mIliDetails.getIliIiName(), AD_CMPNY);
            // LocalArInvoiceLineItem arInvoiceLineItem =
            // arInvoiceLineItemHome.findByInvCodeAndIlCodeAndUomName(arInvoice.getInvCode(),
            // invItemLocation.getIlCode(), mIliDetails.getIliUomName(), AD_CMPNY);
            // arInvoiceLineItem.setIliEnableAutoBuild(mIliDetails.getIliEnableAutoBuild());

          } catch (FinderException ex) {

            throw new GlobalInvItemLocationNotFoundException(
                String.valueOf(mIliDetails.getIliLine()));

          }

          // start date validation
          if (adPreference.getPrfArAllowPriorDate() == EJBCommon.FALSE) {
            Collection invNegTxnCosting =
                invCostingHome.findNegTxnByGreaterThanCstDateAndIiNameAndLocName(
                    arInvoice.getInvDate(), invItemLocation.getInvItem().getIiName(),
                    invItemLocation.getInvLocation().getLocName(), AD_BRNCH, AD_CMPNY);
            if (!invNegTxnCosting.isEmpty())
              throw new GlobalInventoryDateException(invItemLocation.getInvItem().getIiName());
          }


          if (mIliDetails.getIliEnableAutoBuild() == EJBCommon.TRUE
              && invItemLocation.getInvItem().getIiClass().equals("Assembly")) {

            Collection invBillOfMaterials = invItemLocation.getInvItem().getInvBillOfMaterials();

            Iterator j = invBillOfMaterials.iterator();

            while (j.hasNext()) {

              LocalInvBillOfMaterial invBillOfMaterial = (LocalInvBillOfMaterial) j.next();

              LocalInvItemLocation invIlRawMaterial = null;

              try {

                invIlRawMaterial = invItemLocationHome.findByLocNameAndIiName(
                    invBillOfMaterial.getBomLocName(), invBillOfMaterial.getBomIiName(), AD_CMPNY);

              } catch (FinderException ex) {

                throw new GlobalInvItemLocationNotFoundException(
                    String.valueOf(mIliDetails.getIliLine()) + " - Raw Mat. ("
                        + invBillOfMaterial.getBomIiName() + ")");

              }

              // start date validation
              if (adPreference.getPrfArAllowPriorDate() == EJBCommon.FALSE) {
                Collection invIlRawMatNegTxnCosting =
                    invCostingHome.findNegTxnByGreaterThanCstDateAndIiNameAndLocName(
                        arInvoice.getInvDate(), invIlRawMaterial.getInvItem().getIiName(),
                        invIlRawMaterial.getInvLocation().getLocName(), AD_BRNCH, AD_CMPNY);
                if (!invIlRawMatNegTxnCosting.isEmpty())
                  throw new GlobalInventoryDateException(invItemLocation.getInvItem().getIiName()
                      + " - Raw Mat. (" + invIlRawMaterial.getInvItem().getIiName() + ")");
              }

            }

          }

        }


      }


      // insufficient stock checking
      System.out.println("adPreference.getPrfArCheckInsufficientStock()="
          + adPreference.getPrfArCheckInsufficientStock());


      this.checkStockAvailable(isDraft, AD_BRNCH, AD_CMPNY, invCostingHome, arInvoice,
          adPreference);



      // generate approval status

      String INV_APPRVL_STATUS = null;

      if (!isDraft) {

        LocalAdApproval adApproval = adApprovalHome.findByAprAdCompany(AD_CMPNY);

        // validate if total amount + unposted invoices' amount + current balance + unposted
        // receipts's amount
        // does not exceed customer's credit limit

        double balance = 0;
        LocalAdApprovalDocument adInvoiceApprovalDocument =
            adApprovalDocumentHome.findByAdcType("AR INVOICE", AD_CMPNY);


        if (arCustomer.getCstCreditLimit() > 0) {

          balance = computeTotalBalance(details.getInvCode(), CST_CSTMR_CODE, AD_CMPNY);

          balance += amountDue;

          if (arCustomer.getCstCreditLimit() < balance && (adApproval
              .getAprEnableArInvoice() == EJBCommon.FALSE
              || (adApproval.getAprEnableArInvoice() == EJBCommon.TRUE && adInvoiceApprovalDocument
                  .getAdcEnableCreditLimitChecking() == EJBCommon.FALSE))) {

            throw new ArINVAmountExceedsCreditLimitException();

          }
        }
        Debug.print("ArInvoiceEntryControllerBean saveArInvIliEntry O01");
        // find overdue invoices
        Collection arOverdueInvoices =
            arInvoicePaymentScheduleHome.findOverdueIpsByInvDateAndCstCustomerCode(
                arInvoice.getInvDate(), CST_CSTMR_CODE, AD_CMPNY);

        // check if ar invoice approval is enabled

        if (adApproval.getAprEnableArInvoice() == EJBCommon.FALSE
            || (arCustomer.getCstCreditLimit() > balance && arOverdueInvoices.size() == 0)) {

          INV_APPRVL_STATUS = "N/A";

        } else {

          // check if invoice is self approved

          LocalAdAmountLimit adAmountLimit = null;

          try {

            adAmountLimit = adAmountLimitHome.findByAdcTypeAndAuTypeAndUsrName("AR INVOICE",
                "REQUESTER", details.getInvLastModifiedBy(), AD_CMPNY);
            Debug.print("ArInvoiceEntryControllerBean saveArInvIliEntry O02");
          } catch (FinderException ex) {

            throw new GlobalNoApprovalRequesterFoundException();

          }

          if (arInvoice.getInvAmountDue() <= adAmountLimit.getCalAmountLimit()
              && (arCustomer.getCstCreditLimit() == 0
                  || arCustomer.getCstCreditLimit() > balance)) {

            INV_APPRVL_STATUS = "N/A";

          } else {

            // for approval, create approval queue

            Collection adAmountLimits = adAmountLimitHome.findByAdcTypeAndGreaterThanCalAmountLimit(
                "AR INVOICE", adAmountLimit.getCalAmountLimit(), AD_CMPNY);
            Debug.print("ArInvoiceEntryControllerBean saveArInvIliEntry O03");
            if (adAmountLimits.isEmpty()) {

              Collection adApprovalUsers = adApprovalUserHome.findByAuTypeAndCalCode("APPROVER",
                  adAmountLimit.getCalCode(), AD_CMPNY);

              if (adApprovalUsers.isEmpty()) {

                throw new GlobalNoApprovalApproverFoundException();

              }

              Iterator j = adApprovalUsers.iterator();

              while (j.hasNext()) {

                LocalAdApprovalUser adApprovalUser = (LocalAdApprovalUser) j.next();

                LocalAdApprovalQueue adApprovalQueue =
                    adApprovalQueueHome.create(EJBCommon.TRUE, "AR INVOICE", arInvoice.getInvCode(),
                        arInvoice.getInvNumber(), arInvoice.getInvDate(),
                        adAmountLimit.getCalAndOr(), adApprovalUser.getAuOr(), AD_BRNCH, AD_CMPNY);

                adApprovalUser.getAdUser().addAdApprovalQueue(adApprovalQueue);

              }

            } else {

              boolean isApprovalUsersFound = false;

              Iterator i = adAmountLimits.iterator();

              while (i.hasNext()) {

                LocalAdAmountLimit adNextAmountLimit = (LocalAdAmountLimit) i.next();

                if (arInvoice.getInvAmountDue() <= adNextAmountLimit.getCalAmountLimit()) {

                  Collection adApprovalUsers = adApprovalUserHome.findByAuTypeAndCalCode("APPROVER",
                      adAmountLimit.getCalCode(), AD_CMPNY);
                  Debug.print("ArInvoiceEntryControllerBean saveArInvIliEntry O04");
                  Iterator j = adApprovalUsers.iterator();

                  while (j.hasNext()) {

                    isApprovalUsersFound = true;

                    LocalAdApprovalUser adApprovalUser = (LocalAdApprovalUser) j.next();

                    LocalAdApprovalQueue adApprovalQueue = adApprovalQueueHome.create(
                        EJBCommon.TRUE, "AR INVOICE", arInvoice.getInvCode(),
                        arInvoice.getInvNumber(), arInvoice.getInvDate(),
                        adAmountLimit.getCalAndOr(), adApprovalUser.getAuOr(), AD_BRNCH, AD_CMPNY);

                    adApprovalUser.getAdUser().addAdApprovalQueue(adApprovalQueue);

                  }

                  break;

                } else if (!i.hasNext()) {

                  Collection adApprovalUsers = adApprovalUserHome.findByAuTypeAndCalCode("APPROVER",
                      adNextAmountLimit.getCalCode(), AD_CMPNY);

                  Iterator j = adApprovalUsers.iterator();

                  while (j.hasNext()) {

                    isApprovalUsersFound = true;

                    LocalAdApprovalUser adApprovalUser = (LocalAdApprovalUser) j.next();

                    LocalAdApprovalQueue adApprovalQueue =
                        adApprovalQueueHome.create(EJBCommon.TRUE, "AR INVOICE",
                            arInvoice.getInvCode(), arInvoice.getInvNumber(),
                            arInvoice.getInvDate(), adNextAmountLimit.getCalAndOr(),
                            adApprovalUser.getAuOr(), AD_BRNCH, AD_CMPNY);

                    adApprovalUser.getAdUser().addAdApprovalQueue(adApprovalQueue);

                  }

                  break;

                }

                adAmountLimit = adNextAmountLimit;

              }

              if (!isApprovalUsersFound) {

                throw new GlobalNoApprovalApproverFoundException();

              }

            }

            INV_APPRVL_STATUS = "PENDING";
          }
        }
      }
      Debug.print("ArInvoiceEntryControllerBean saveArInvIliEntry O Done");

      if (INV_APPRVL_STATUS != null && INV_APPRVL_STATUS.equals("N/A")
          && adPreference.getPrfArGlPostingType().equals("AUTO-POST UPON APPROVAL")) {

        this.executeArInvPost(arInvoice.getInvCode(), arInvoice.getInvLastModifiedBy(), AD_BRNCH,
            AD_CMPNY);

      }

      // set invoice approval status

      arInvoice.setInvApprovalStatus(INV_APPRVL_STATUS);

      Debug.print("ArInvoiceEntryControllerBean saveArInvIliEntry " + txnStartDate);

      return arInvoice.getInvCode();


    } catch (GlobalRecordAlreadyDeletedException ex) {

      getSessionContext().setRollbackOnly();
      throw ex;

    } catch (GlobalDocumentNumberNotUniqueException ex) {

      getSessionContext().setRollbackOnly();
      throw ex;

    } catch (GlobalConversionDateNotExistException ex) {

      getSessionContext().setRollbackOnly();
      throw ex;

    } catch (GlobalPaymentTermInvalidException ex) {

      getSessionContext().setRollbackOnly();
      throw ex;

    } catch (ArINVAmountExceedsCreditLimitException ex) {

      getSessionContext().setRollbackOnly();
      throw ex;

    } catch (GlobalTransactionAlreadyApprovedException ex) {

      getSessionContext().setRollbackOnly();
      throw ex;

    } catch (GlobalTransactionAlreadyPendingException ex) {

      getSessionContext().setRollbackOnly();
      throw ex;

    } catch (GlobalTransactionAlreadyPostedException ex) {

      getSessionContext().setRollbackOnly();
      throw ex;

    } catch (GlobalTransactionAlreadyVoidException ex) {

      getSessionContext().setRollbackOnly();
      throw ex;

    } catch (GlobalNoApprovalRequesterFoundException ex) {

      getSessionContext().setRollbackOnly();
      throw ex;

    } catch (GlobalNoApprovalApproverFoundException ex) {

      getSessionContext().setRollbackOnly();
      throw ex;

    } catch (GlobalInvItemLocationNotFoundException ex) {

      getSessionContext().setRollbackOnly();
      throw ex;

    } catch (GlJREffectiveDateNoPeriodExistException ex) {

      getSessionContext().setRollbackOnly();
      throw ex;

    } catch (GlJREffectiveDatePeriodClosedException ex) {

      getSessionContext().setRollbackOnly();
      throw ex;

    } catch (GlobalJournalNotBalanceException ex) {

      getSessionContext().setRollbackOnly();
      throw ex;

    } catch (GlobalInventoryDateException ex) {

      getSessionContext().setRollbackOnly();
      throw ex;

    } catch (GlobalBranchAccountNumberInvalidException ex) {

      // Retrive Error a Line Number
      ex.setLineNumberError(lineNumberError);
      getSessionContext().setRollbackOnly();
      throw ex;

    } catch (AdPRFCoaGlVarianceAccountNotFoundException ex) {

      getSessionContext().setRollbackOnly();
      throw ex;

    } catch (GlobalRecordInvalidException ex) {

      getSessionContext().setRollbackOnly();
      throw ex;

    } catch (GlobalExpiryDateNotFoundException ex) {

      getSessionContext().setRollbackOnly();
      throw ex;

    } catch (GlobalMiscInfoIsRequiredException ex) {

      getSessionContext().setRollbackOnly();
      throw ex;
    } catch (ArInvDuplicateUploadNumberException ex) {

      getSessionContext().setRollbackOnly();
      throw ex;
    } catch (Exception ex) {

      Debug.printStackTrace(ex);
      getSessionContext().setRollbackOnly();
      throw new EJBException(ex.getMessage());

    }

  }


  private void checkStockAvailable(
      boolean isDraft, Integer AD_BRNCH, Integer AD_CMPNY, LocalInvCostingHome invCostingHome,
      LocalArInvoice arInvoice, LocalAdPreference adPreference
  ) throws GlobalRecordInvalidException {
    if (adPreference.getPrfArCheckInsufficientStock() == EJBCommon.TRUE && !isDraft) {
      System.out.println("CHECK INSUFFICIENT STOCK");
      boolean hasInsufficientItems = false;
      String insufficientItems = "";

      Collection arInvoiceLineItems = arInvoice.getArInvoiceLineItems();

      Iterator i = arInvoiceLineItems.iterator();

      while (i.hasNext()) {

        LocalArInvoiceLineItem arInvoiceLineItem = (LocalArInvoiceLineItem) i.next();

        if (arInvoiceLineItem.getInvItemLocation().getInvItem()
            .getIiNonInventoriable() == EJBCommon.TRUE)
          continue;

        LocalInvCosting invCosting = null;

        double ILI_QTY = this.convertByUomAndQuantity(arInvoiceLineItem.getInvUnitOfMeasure(),
            arInvoiceLineItem.getInvItemLocation().getInvItem(),
            Math.abs(arInvoiceLineItem.getIliQuantity()), AD_CMPNY);

        try {

          invCosting = invCostingHome
              .getByMaxCstDateToLongAndMaxCstLineNumberAndLessThanEqualCstDateAndIiNameAndLocName(
                  arInvoice.getInvDate(),
                  arInvoiceLineItem.getInvItemLocation().getInvItem().getIiName(),
                  arInvoiceLineItem.getInvItemLocation().getInvLocation().getLocName(), AD_BRNCH,
                  AD_CMPNY);

        } catch (FinderException ex) {

        }

        double LOWEST_QTY = this.convertByUomAndQuantity(arInvoiceLineItem.getInvUnitOfMeasure(),
            arInvoiceLineItem.getInvItemLocation().getInvItem(), 1, AD_CMPNY);



        // System.out.println("invCosting.getCstRemainingQuantity()="+invCosting.getCstRemainingQuantity());
        if ((invCosting == null || invCosting.getCstRemainingQuantity() == 0
            || invCosting.getCstRemainingQuantity() - ILI_QTY <= -LOWEST_QTY)
            && arInvoiceLineItem.getInvItemLocation().getInvItem()
                .getIiNonInventoriable() == (byte) 0) {

          hasInsufficientItems = true;

          insufficientItems +=
              arInvoiceLineItem.getInvItemLocation().getInvItem().getIiName() + ", ";
        }

      }
      if (hasInsufficientItems) {

        throw new GlobalRecordInvalidException(
            insufficientItems.substring(0, insufficientItems.lastIndexOf(",")));
      }
    }
  }

  /**
   * @ejb:interface-method view-type="remote"
   **/
  public Integer saveArInvSolEntry(
      com.util.ArInvoiceDetails details, String PYT_NM, String TC_NM, String WTC_NM, String FC_NM,
      String CST_CSTMR_CODE, String IB_NM, ArrayList solList, boolean isDraft,
      String SLP_SLSPRSN_CODE, Integer AD_BRNCH, Integer AD_CMPNY
  ) throws GlobalRecordAlreadyDeletedException, GlobalDocumentNumberNotUniqueException,
      GlobalConversionDateNotExistException, GlobalPaymentTermInvalidException,
      GlobalTransactionAlreadyApprovedException, GlobalTransactionAlreadyPendingException,
      GlobalTransactionAlreadyPostedException, GlobalTransactionAlreadyVoidException,
      GlobalNoApprovalRequesterFoundException, GlobalNoApprovalApproverFoundException,
      GlobalInvItemLocationNotFoundException, GlJREffectiveDateNoPeriodExistException,
      GlJREffectiveDatePeriodClosedException, GlobalJournalNotBalanceException,
      GlobalTransactionAlreadyLockedException, GlobalInventoryDateException,
      GlobalBranchAccountNumberInvalidException, ArINVAmountExceedsCreditLimitException,
      AdPRFCoaGlVarianceAccountNotFoundException, GlobalExpiryDateNotFoundException,
      GlobalRecordInvalidException,


      ArInvDuplicateUploadNumberException {

    Debug.print("ArInvoiceEntryControllerBean saveArInvSolEntry");

    LocalArInvoiceHome arInvoiceHome = null;
    LocalArSalesOrderInvoiceLineHome arSalesOrderInvoiceLineHome = null;
    LocalAdDocumentSequenceAssignmentHome adDocumentSequenceAssignmentHome = null;
    LocalAdBranchDocumentSequenceAssignmentHome adBranchDocumentSequenceAssignmentHome = null;
    LocalGlFunctionalCurrencyHome glFunctionalCurrencyHome = null;
    LocalGlFunctionalCurrencyRateHome glFunctionalCurrencyRateHome = null;
    LocalAdPaymentTermHome adPaymentTermHome = null;
    LocalArTaxCodeHome arTaxCodeHome = null;
    LocalArWithholdingTaxCodeHome arWithholdingTaxCodeHome = null;
    LocalArCustomerHome arCustomerHome = null;
    LocalArInvoiceBatchHome arInvoiceBatchHome = null;
    LocalAdPreferenceHome adPreferenceHome = null;
    LocalInvItemLocationHome invItemLocationHome = null;
    LocalArSalesOrderHome arSalesOrderHome = null;
    LocalInvCostingHome invCostingHome = null;
    LocalAdPaymentScheduleHome adPaymentScheduleHome = null;
    LocalAdDiscountHome adDiscountHome = null;
    LocalArInvoicePaymentScheduleHome arInvoicePaymentScheduleHome = null;
    LocalAdApprovalHome adApprovalHome = null;
    LocalAdAmountLimitHome adAmountLimitHome = null;
    LocalAdApprovalUserHome adApprovalUserHome = null;
    LocalAdApprovalQueueHome adApprovalQueueHome = null;
    LocalAdApprovalDocumentHome adApprovalDocumentHome = null;
    LocalAdBranchItemLocationHome adBranchItemLocationHome = null;
    LocalAdBranchCustomerHome adBranchCustomerHome = null;
    LocalArSalespersonHome arSalespersonHome = null;
    LocalGlSetOfBookHome glSetOfBookHome = null;
    LocalAdBranchArTaxCodeHome adBranchArTaxCodeHome = null;

    LocalArInvoice arInvoice = null;
    InvRepItemCostingControllerHome homeRIC = null;
    InvRepItemCostingController ejbRIC = null;

    // Initialize EJB Home

    Date txnStartDate = new Date();



    try {

      homeRIC = (InvRepItemCostingControllerHome) com.util.EJBHomeFactory
          .lookUpHome("ejb/InvRepItemCostingControllerEJB", InvRepItemCostingControllerHome.class);

      adBranchArTaxCodeHome = (LocalAdBranchArTaxCodeHome) EJBHomeFactory
          .lookUpLocalHome(LocalAdBranchArTaxCodeHome.JNDI_NAME, LocalAdBranchArTaxCodeHome.class);
      arInvoiceHome = (LocalArInvoiceHome) EJBHomeFactory
          .lookUpLocalHome(LocalArInvoiceHome.JNDI_NAME, LocalArInvoiceHome.class);
      arSalesOrderInvoiceLineHome =
          (LocalArSalesOrderInvoiceLineHome) EJBHomeFactory.lookUpLocalHome(
              LocalArSalesOrderInvoiceLineHome.JNDI_NAME, LocalArSalesOrderInvoiceLineHome.class);
      adDocumentSequenceAssignmentHome = (LocalAdDocumentSequenceAssignmentHome) EJBHomeFactory
          .lookUpLocalHome(LocalAdDocumentSequenceAssignmentHome.JNDI_NAME,
              LocalAdDocumentSequenceAssignmentHome.class);
      adBranchDocumentSequenceAssignmentHome =
          (LocalAdBranchDocumentSequenceAssignmentHome) EJBHomeFactory.lookUpLocalHome(
              LocalAdBranchDocumentSequenceAssignmentHome.JNDI_NAME,
              LocalAdBranchDocumentSequenceAssignmentHome.class);
      glFunctionalCurrencyHome = (LocalGlFunctionalCurrencyHome) EJBHomeFactory.lookUpLocalHome(
          LocalGlFunctionalCurrencyHome.JNDI_NAME, LocalGlFunctionalCurrencyHome.class);
      glFunctionalCurrencyRateHome =
          (LocalGlFunctionalCurrencyRateHome) EJBHomeFactory.lookUpLocalHome(
              LocalGlFunctionalCurrencyRateHome.JNDI_NAME, LocalGlFunctionalCurrencyRateHome.class);
      adPaymentTermHome = (LocalAdPaymentTermHome) EJBHomeFactory
          .lookUpLocalHome(LocalAdPaymentTermHome.JNDI_NAME, LocalAdPaymentTermHome.class);
      arTaxCodeHome = (LocalArTaxCodeHome) EJBHomeFactory
          .lookUpLocalHome(LocalArTaxCodeHome.JNDI_NAME, LocalArTaxCodeHome.class);
      arWithholdingTaxCodeHome = (LocalArWithholdingTaxCodeHome) EJBHomeFactory.lookUpLocalHome(
          LocalArWithholdingTaxCodeHome.JNDI_NAME, LocalArWithholdingTaxCodeHome.class);
      arCustomerHome = (LocalArCustomerHome) EJBHomeFactory
          .lookUpLocalHome(LocalArCustomerHome.JNDI_NAME, LocalArCustomerHome.class);
      arInvoiceBatchHome = (LocalArInvoiceBatchHome) EJBHomeFactory
          .lookUpLocalHome(LocalArInvoiceBatchHome.JNDI_NAME, LocalArInvoiceBatchHome.class);
      adPreferenceHome = (LocalAdPreferenceHome) EJBHomeFactory
          .lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);
      invItemLocationHome = (LocalInvItemLocationHome) EJBHomeFactory
          .lookUpLocalHome(LocalInvItemLocationHome.JNDI_NAME, LocalInvItemLocationHome.class);
      arSalesOrderHome = (LocalArSalesOrderHome) EJBHomeFactory
          .lookUpLocalHome(LocalArSalesOrderHome.JNDI_NAME, LocalArSalesOrderHome.class);
      invCostingHome = (LocalInvCostingHome) EJBHomeFactory
          .lookUpLocalHome(LocalInvCostingHome.JNDI_NAME, LocalInvCostingHome.class);
      adPaymentScheduleHome = (LocalAdPaymentScheduleHome) EJBHomeFactory
          .lookUpLocalHome(LocalAdPaymentScheduleHome.JNDI_NAME, LocalAdPaymentScheduleHome.class);
      adDiscountHome = (LocalAdDiscountHome) EJBHomeFactory
          .lookUpLocalHome(LocalAdDiscountHome.JNDI_NAME, LocalAdDiscountHome.class);
      arInvoicePaymentScheduleHome =
          (LocalArInvoicePaymentScheduleHome) EJBHomeFactory.lookUpLocalHome(
              LocalArInvoicePaymentScheduleHome.JNDI_NAME, LocalArInvoicePaymentScheduleHome.class);
      adApprovalHome = (LocalAdApprovalHome) EJBHomeFactory
          .lookUpLocalHome(LocalAdApprovalHome.JNDI_NAME, LocalAdApprovalHome.class);
      adAmountLimitHome = (LocalAdAmountLimitHome) EJBHomeFactory
          .lookUpLocalHome(LocalAdAmountLimitHome.JNDI_NAME, LocalAdAmountLimitHome.class);
      adApprovalUserHome = (LocalAdApprovalUserHome) EJBHomeFactory
          .lookUpLocalHome(LocalAdApprovalUserHome.JNDI_NAME, LocalAdApprovalUserHome.class);
      adApprovalDocumentHome = (LocalAdApprovalDocumentHome) EJBHomeFactory.lookUpLocalHome(
          LocalAdApprovalDocumentHome.JNDI_NAME, LocalAdApprovalDocumentHome.class);
      adApprovalQueueHome = (LocalAdApprovalQueueHome) EJBHomeFactory
          .lookUpLocalHome(LocalAdApprovalQueueHome.JNDI_NAME, LocalAdApprovalQueueHome.class);
      adBranchItemLocationHome = (LocalAdBranchItemLocationHome) EJBHomeFactory.lookUpLocalHome(
          LocalAdBranchItemLocationHome.JNDI_NAME, LocalAdBranchItemLocationHome.class);
      adBranchCustomerHome = (LocalAdBranchCustomerHome) EJBHomeFactory
          .lookUpLocalHome(LocalAdBranchCustomerHome.JNDI_NAME, LocalAdBranchCustomerHome.class);
      arSalespersonHome = (LocalArSalespersonHome) EJBHomeFactory
          .lookUpLocalHome(LocalArSalespersonHome.JNDI_NAME, LocalArSalespersonHome.class);
      glSetOfBookHome = (LocalGlSetOfBookHome) EJBHomeFactory
          .lookUpLocalHome(LocalGlSetOfBookHome.JNDI_NAME, LocalGlSetOfBookHome.class);

    } catch (NamingException ex) {

      throw new EJBException(ex.getMessage());

    }


    try {
      ejbRIC = homeRIC.create();
    } catch (RemoteException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    } catch (CreateException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }


    int lineNumberError = 0;

    try {

      // validate if invoice is already deleted

      try {

        if (details.getInvCode() != null) {

          arInvoice = arInvoiceHome.findByPrimaryKey(details.getInvCode());

        }

      } catch (FinderException ex) {

        throw new GlobalRecordAlreadyDeletedException();

      }

      // validate if invoice is already posted, void, arproved or pending

      if (details.getInvCode() != null) {

        if (arInvoice.getInvApprovalStatus() != null) {

          if (arInvoice.getInvApprovalStatus().equals("APPROVED")
              || arInvoice.getInvApprovalStatus().equals("N/A")) {

            throw new GlobalTransactionAlreadyApprovedException();


          } else if (arInvoice.getInvApprovalStatus().equals("PENDING")) {

            throw new GlobalTransactionAlreadyPendingException();

          }

        }

        if (arInvoice.getInvPosted() == EJBCommon.TRUE) {

          throw new GlobalTransactionAlreadyPostedException();

        } else if (arInvoice.getInvVoid() == EJBCommon.TRUE) {

          throw new GlobalTransactionAlreadyVoidException();

        }

      }

      // invoice void

      if (details.getInvCode() != null && details.getInvVoid() == EJBCommon.TRUE
          && arInvoice.getInvPosted() == EJBCommon.FALSE) {

        arInvoice.setInvVoid(EJBCommon.TRUE);
        arInvoice.setInvLastModifiedBy(details.getInvLastModifiedBy());
        arInvoice.setInvDateLastModified(details.getInvDateLastModified());

        return arInvoice.getInvCode();

      }

      // validate if document number is unique document number is automatic then set next sequence

      LocalAdBranchDocumentSequenceAssignment adBranchDocumentSequenceAssignment = null;
      LocalAdDocumentSequenceAssignment adDocumentSequenceAssignment = null;

      if (details.getInvCode() == null) {


        String documentType = details.getInvDocumentType();

        try {
          if (documentType != null) {
            adDocumentSequenceAssignment =
                adDocumentSequenceAssignmentHome.findByDcName(documentType, AD_CMPNY);
          } else {
            documentType = "AR INVOICE";
          }
        } catch (FinderException ex) {
          documentType = "AR INVOICE";
        }



        try {

          adDocumentSequenceAssignment =
              adDocumentSequenceAssignmentHome.findByDcName(documentType, AD_CMPNY);

        } catch (FinderException ex) {

        }

        try {

          adBranchDocumentSequenceAssignment =
              adBranchDocumentSequenceAssignmentHome.findBdsByDsaCodeAndBrCode(
                  adDocumentSequenceAssignment.getDsaCode(), AD_BRNCH, AD_CMPNY);

        } catch (FinderException ex) {

        }

        LocalArInvoice arExistingInvoice = null;

        try {

          arExistingInvoice = arInvoiceHome.findByInvNumberAndInvCreditMemoAndBrCode(
              details.getInvNumber(), EJBCommon.FALSE, AD_BRNCH, AD_CMPNY);

        } catch (FinderException ex) {

        }

        if (arExistingInvoice != null) {

          throw new GlobalDocumentNumberNotUniqueException();

        }
        /*
         * if (adDocumentSequenceAssignment.getAdDocumentSequence().getDsNumberingType() == 'A' &&
         * (details.getInvNumber() == null || details.getInvNumber().trim().length() == 0)) {
         * 
         * while (true) {
         * 
         * if (adBranchDocumentSequenceAssignment == null ||
         * adBranchDocumentSequenceAssignment.getBdsNextSequence() == null) {
         * 
         * try {
         * 
         * arInvoiceHome.findByInvNumberAndInvCreditMemoAndBrCode(adDocumentSequenceAssignment.
         * getDsaNextSequence(), EJBCommon.FALSE, AD_BRNCH, AD_CMPNY);
         * adDocumentSequenceAssignment.setDsaNextSequence(EJBCommon.incrementStringNumber(
         * adDocumentSequenceAssignment.getDsaNextSequence()));
         * 
         * } catch (FinderException ex) {
         * 
         * details.setInvNumber(adDocumentSequenceAssignment.getDsaNextSequence());
         * adDocumentSequenceAssignment.setDsaNextSequence(EJBCommon.incrementStringNumber(
         * adDocumentSequenceAssignment.getDsaNextSequence())); break;
         * 
         * }
         * 
         * } else {
         * 
         * try {
         * 
         * arInvoiceHome.findByInvNumberAndInvCreditMemoAndBrCode(adBranchDocumentSequenceAssignment
         * .getBdsNextSequence(), EJBCommon.FALSE, AD_BRNCH, AD_CMPNY);
         * adBranchDocumentSequenceAssignment.setBdsNextSequence(EJBCommon.incrementStringNumber(
         * adBranchDocumentSequenceAssignment.getBdsNextSequence()));
         * 
         * } catch (FinderException ex) {
         * 
         * details.setInvNumber(adBranchDocumentSequenceAssignment.getBdsNextSequence());
         * adBranchDocumentSequenceAssignment.setBdsNextSequence(EJBCommon.incrementStringNumber(
         * adBranchDocumentSequenceAssignment.getBdsNextSequence())); break;
         * 
         * }
         * 
         * }
         * 
         * }
         * 
         * }
         */


        // Confirm if Upload Number have duplicate

        if (details.getInvUploadNumber() != null) {
          if (!details.getInvUploadNumber().trim().equals("")) {
            try {

              arInvoiceHome.findByUploadNumberAndCompanyCode(details.getInvUploadNumber(),
                  AD_CMPNY);


              // throw exception if found duplicate upload number
              throw new ArInvDuplicateUploadNumberException();


            } catch (FinderException ex) {

            }
          }



        }

      } else {

        LocalArInvoice arExistingInvoice = null;

        try {

          arExistingInvoice = arInvoiceHome.findByInvNumberAndInvCreditMemoAndBrCode(
              details.getInvNumber(), EJBCommon.FALSE, AD_BRNCH, AD_CMPNY);

        } catch (FinderException ex) {
        }

        if (arExistingInvoice != null
            && !arExistingInvoice.getInvCode().equals(details.getInvCode())) {

          throw new GlobalDocumentNumberNotUniqueException();

        }

        if (arInvoice.getInvNumber() != details.getInvNumber()
            && (details.getInvNumber() == null || details.getInvNumber().trim().length() == 0)) {

          details.setInvNumber(arInvoice.getInvNumber());

        }

        // Confirm if Upload Number have duplicate

        if (details.getInvUploadNumber() != null) {
          if (!details.getInvUploadNumber().trim().equals("")) {
            if (!details.getInvUploadNumber().equals(arExistingInvoice.getInvUploadNumber())) {
              try {
                arInvoiceHome.findByUploadNumberAndCompanyCode(details.getInvUploadNumber(),
                    AD_CMPNY);


                // throw exception if found duplicate upload number
                throw new ArInvDuplicateUploadNumberException();


              } catch (FinderException ex) {

              }
            }
          }


        }

      }

      // validate if conversion date exists

      try {

        if (details.getInvConversionDate() != null) {


          LocalGlFunctionalCurrency glValidateFunctionalCurrency =
              glFunctionalCurrencyHome.findByFcName(FC_NM, AD_CMPNY);
          LocalGlFunctionalCurrencyRate glFunctionalCurrencyRate = glFunctionalCurrencyRateHome
              .findByFcCodeAndDate(glValidateFunctionalCurrency.getFcCode(),
                  details.getInvConversionDate(), AD_CMPNY);

        }

      } catch (FinderException ex) {

        throw new GlobalConversionDateNotExistException();

      }

      // validate if payment term has at least one payment schedule

      if (adPaymentTermHome.findByPytName(PYT_NM, AD_CMPNY).getAdPaymentSchedules().isEmpty()) {

        throw new GlobalPaymentTermInvalidException();

      }

      // lock sales order

      LocalArSalesOrder arExistingSalesOrder = null;

      try {

        arExistingSalesOrder = arSalesOrderHome
            .findBySoDocumentNumberAndBrCode(details.getInvSoNumber(), AD_BRNCH, AD_CMPNY);


        if ((arInvoice == null || (arInvoice != null && arInvoice.getInvSoNumber() != null
            && (!arInvoice.getInvSoNumber().equals(details.getInvSoNumber()))))) {

          if (arExistingSalesOrder.getSoLock() == EJBCommon.TRUE) {

            throw new GlobalTransactionAlreadyLockedException();

          }

          arExistingSalesOrder.setSoLock(EJBCommon.TRUE);

        }

      } catch (FinderException ex) {

      }

      // used in checking if invoice should re-generate distribution records and re-calculate taxes
      boolean isRecalculate = true;

      // create invoice

      if (details.getInvCode() == null) {
        System.out.println("CHECKING C");

        arInvoice = arInvoiceHome.create(details.getInvType(), EJBCommon.FALSE,
            details.getInvDescription(), details.getInvDate(), details.getInvNumber(),
            details.getInvReferenceNumber(), details.getInvUploadNumber(), null, null, 0d,
            details.getInvDownPayment(), 0d, 0d, 0d, 0d, details.getInvConversionDate(),
            details.getInvConversionRate(), details.getInvMemo(), details.getInvPreviousReading(),
            details.getInvPresentReading(), details.getInvBillToAddress(),
            details.getInvBillToContact(), details.getInvBillToAltContact(),
            details.getInvBillToPhone(), details.getInvBillingHeader(),
            details.getInvBillingFooter(), details.getInvBillingHeader2(),
            details.getInvBillingFooter2(), details.getInvBillingHeader3(),
            details.getInvBillingFooter3(), details.getInvBillingSignatory(),
            details.getInvSignatoryTitle(), details.getInvShipToAddress(),
            details.getInvShipToContact(), details.getInvShipToAltContact(),
            details.getInvShipToPhone(), details.getInvShipDate(), details.getInvLvFreight(), null,
            null, EJBCommon.FALSE, null, EJBCommon.FALSE, EJBCommon.FALSE, EJBCommon.FALSE,
            EJBCommon.FALSE, EJBCommon.FALSE, null, 0d, null, null, null, null,
            details.getInvCreatedBy(), details.getInvDateCreated(), details.getInvLastModifiedBy(),
            details.getInvDateLastModified(), null, null, null, null, EJBCommon.FALSE,
            details.getInvLvShift(), details.getInvSoNumber(), null, details.getInvDebitMemo(),
            details.getInvSubjectToCommission(), details.getInvClientPO(),
            details.getInvEffectivityDate(), AD_BRNCH, AD_CMPNY);

      } else {

        // check if critical fields are changed

        if ((arInvoice.getInvSoNumber() != null
            && !arInvoice.getInvSoNumber().equals(details.getInvSoNumber()))
            || !arInvoice.getArTaxCode().getTcName().equals(TC_NM)
            || !arInvoice.getArWithholdingTaxCode().getWtcName().equals(WTC_NM)
            || !arInvoice.getArCustomer().getCstCustomerCode().equals(CST_CSTMR_CODE)
            || !arInvoice.getAdPaymentTerm().getPytName().equals(PYT_NM)
            || !arInvoice.getInvDate().equals(details.getInvDate())
            || !arInvoice.getInvEffectivityDate().equals(details.getInvEffectivityDate())
            || arInvoice.getInvDownPayment() != details.getInvDownPayment()
            || solList.size() != arInvoice.getArSalesOrderInvoiceLines().size()) {

          isRecalculate = true;

        } else if (solList.size() == arInvoice.getArSalesOrderInvoiceLines().size()) {

          Iterator solIter = arInvoice.getArSalesOrderInvoiceLines().iterator();
          Iterator solListIter = solList.iterator();

          while (solIter.hasNext()) {

            LocalArSalesOrderInvoiceLine arSalesOrderInvoiceLine =
                (LocalArSalesOrderInvoiceLine) solIter.next();
            LocalArSalesOrderLine arSalesOrderLine = arSalesOrderInvoiceLine.getArSalesOrderLine();
            ArModSalesOrderLineDetails mdetails = (ArModSalesOrderLineDetails) solListIter.next();

            if (!arSalesOrderLine.getInvItemLocation().getInvItem().getIiName()
                .equals(mdetails.getSolIiName())
                || !arSalesOrderLine.getInvItemLocation().getInvLocation().getLocName()
                    .equals(mdetails.getSolLocName())
                || !arSalesOrderLine.getInvUnitOfMeasure().getUomName()
                    .equals(mdetails.getSolUomName())
                || arSalesOrderInvoiceLine.getSilQuantityDelivered() != mdetails
                    .getSolQuantityDelivered()
                || arSalesOrderInvoiceLine.getSilTax() != mdetails.getSolTax()
                || arSalesOrderLine.getSolUnitPrice() != mdetails.getSolUnitPrice()) {

              isRecalculate = true;
              break;

            }

            // isRecalculate = false;

          }

        } else {

          // isRecalculate = false;

        }

        arInvoice.setInvDownPayment(details.getInvDownPayment());
        arInvoice.setInvDocumentType(details.getInvDocumentType());
        arInvoice.setInvType(details.getInvType());
        arInvoice.setInvDescription(details.getInvDescription());
        arInvoice.setInvDate(details.getInvDate());
        arInvoice.setInvNumber(details.getInvNumber());
        arInvoice.setInvMemo(details.getInvMemo());
        arInvoice.setInvReferenceNumber(details.getInvReferenceNumber());
        arInvoice.setInvUploadNumber(details.getInvUploadNumber());
        arInvoice.setInvConversionDate(details.getInvConversionDate());
        arInvoice.setInvConversionRate(details.getInvConversionRate());
        arInvoice.setInvPreviousReading(arInvoice.getInvPreviousReading());
        arInvoice.setInvPresentReading(arInvoice.getInvPresentReading());
        arInvoice.setInvBillToAddress(details.getInvBillToAddress());
        arInvoice.setInvBillToContact(details.getInvBillToContact());
        arInvoice.setInvBillToAltContact(details.getInvBillToAltContact());
        arInvoice.setInvBillToPhone(details.getInvBillToPhone());
        arInvoice.setInvBillingHeader(details.getInvBillingHeader());
        arInvoice.setInvBillingFooter(details.getInvBillingFooter());
        arInvoice.setInvBillingHeader2(details.getInvBillingHeader2());
        arInvoice.setInvBillingFooter2(details.getInvBillingFooter2());
        arInvoice.setInvBillingSignatory(details.getInvBillingSignatory());
        arInvoice.setInvSignatoryTitle(details.getInvSignatoryTitle());
        arInvoice.setInvShipToAddress(details.getInvShipToAddress());
        arInvoice.setInvShipToContact(details.getInvShipToContact());
        arInvoice.setInvShipToAltContact(details.getInvShipToAltContact());
        arInvoice.setInvShipToPhone(details.getInvShipToPhone());
        arInvoice.setInvShipDate(details.getInvShipDate());
        arInvoice.setInvLvFreight(details.getInvLvFreight());
        arInvoice.setInvLastModifiedBy(details.getInvLastModifiedBy());
        arInvoice.setInvDateLastModified(details.getInvDateLastModified());
        arInvoice.setInvReasonForRejection(null);
        arInvoice.setInvLvShift(details.getInvLvShift());
        arInvoice.setInvSoNumber(details.getInvSoNumber());
        arInvoice.setInvJoNumber(details.getInvJoNumber());
        arInvoice.setInvDebitMemo(details.getInvDebitMemo());
        arInvoice.setInvSubjectToCommission(details.getInvSubjectToCommission());
        arInvoice.setInvClientPO(details.getInvClientPO());
        arInvoice.setInvEffectivityDate(details.getInvEffectivityDate());
        System.out.println("CHECK D");

      }
      arInvoice.setInvDocumentType(details.getInvDocumentType());
      arInvoice.setReportParameter(details.getReportParameter());

      LocalAdPaymentTerm adPaymentTerm = adPaymentTermHome.findByPytName(PYT_NM, AD_CMPNY);
      // adPaymentTerm.addArInvoice(arInvoice);
      arInvoice.setAdPaymentTerm(adPaymentTerm);

      LocalGlFunctionalCurrency glFunctionalCurrency =
          glFunctionalCurrencyHome.findByFcName(FC_NM, AD_CMPNY);
      // glFunctionalCurrency.addArInvoice(arInvoice);
      arInvoice.setGlFunctionalCurrency(glFunctionalCurrency);

      LocalArTaxCode arTaxCode = arTaxCodeHome.findByTcName(TC_NM, AD_CMPNY);
      // arTaxCode.addArInvoice(arInvoice);
      arInvoice.setArTaxCode(arTaxCode);

      LocalArWithholdingTaxCode arWithholdingTaxCode =
          arWithholdingTaxCodeHome.findByWtcName(WTC_NM, AD_CMPNY);
      // arWithholdingTaxCode.addArInvoice(arInvoice);
      arInvoice.setArWithholdingTaxCode(arWithholdingTaxCode);

      LocalArCustomer arCustomer = arCustomerHome.findByCstCustomerCode(CST_CSTMR_CODE, AD_CMPNY);
      // arCustomer.addArInvoice(arInvoice);
      arInvoice.setArCustomer(arCustomer);

      LocalArSalesperson arSalesperson = null;

      if (SLP_SLSPRSN_CODE != null && SLP_SLSPRSN_CODE.length() > 0
          && !SLP_SLSPRSN_CODE.equalsIgnoreCase("NO RECORD FOUND")) {

        // if he tagged a salesperson for this invoice
        arSalesperson = arSalespersonHome.findBySlpSalespersonCode(SLP_SLSPRSN_CODE, AD_CMPNY);


        // arSalesperson.addArInvoice(arInvoice);
        arInvoice.setArSalesperson(arSalesperson);

      } else {

        // if he untagged a salesperson for this invoice
        if (arInvoice.getArSalesperson() != null) {

          arSalesperson = arSalespersonHome.findBySlpSalespersonCode(
              arInvoice.getArSalesperson().getSlpSalespersonCode(), AD_CMPNY);
          arSalesperson.dropArInvoice(arInvoice);

        }

        // if no salesperson is set, invoice should not be subject to commission
        arInvoice.setInvSubjectToCommission((byte) 0);

      }

      try {

        LocalArInvoiceBatch arInvoiceBatch =
            arInvoiceBatchHome.findByIbName(IB_NM, AD_BRNCH, AD_CMPNY);
        // arInvoiceBatch.addArInvoice(arInvoice);
        arInvoice.setArInvoiceBatch(arInvoiceBatch);


      } catch (FinderException ex) {

      }

      double amountDue = 0;

      LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);

      if (isRecalculate) {

        // remove all invoice line items

        Collection arInvoiceLineItems = arInvoice.getArInvoiceLineItems();

        Iterator i = arInvoiceLineItems.iterator();

        while (i.hasNext()) {

          LocalArInvoiceLineItem arInvoiceLineItem = (LocalArInvoiceLineItem) i.next();

          if (arInvoiceLineItem.getIliEnableAutoBuild() == 1 && arInvoiceLineItem
              .getInvItemLocation().getInvItem().getIiClass().equals("Assembly")) {

            double QTY_SLD =
                this.convertByUomFromAndItemAndQuantity(arInvoiceLineItem.getInvUnitOfMeasure(),
                    arInvoiceLineItem.getInvItemLocation().getInvItem(),
                    arInvoiceLineItem.getIliQuantity(), AD_CMPNY);

            Collection invBillOfMaterials =
                arInvoiceLineItem.getInvItemLocation().getInvItem().getInvBillOfMaterials();

            Iterator j = invBillOfMaterials.iterator();

            while (j.hasNext()) {

              LocalInvBillOfMaterial invBillOfMaterial = (LocalInvBillOfMaterial) j.next();

              LocalInvItemLocation invItemLocation = invItemLocationHome.findByLocNameAndIiName(
                  invBillOfMaterial.getBomLocName(), invBillOfMaterial.getBomIiName(), AD_CMPNY);

              // bom conversion
              double convertedQuantity = this.convertByUomFromAndItemAndQuantity(
                  invBillOfMaterial.getInvUnitOfMeasure(), invItemLocation.getInvItem(),
                  EJBCommon.roundIt(invBillOfMaterial.getBomQuantityNeeded() * QTY_SLD,
                      this.getInvGpQuantityPrecisionUnit(AD_CMPNY)),
                  AD_CMPNY);

              invItemLocation.setIlCommittedQuantity(
                  invItemLocation.getIlCommittedQuantity() - convertedQuantity);

            }

          } else {

            double convertedQuantity =
                this.convertByUomFromAndItemAndQuantity(arInvoiceLineItem.getInvUnitOfMeasure(),
                    arInvoiceLineItem.getInvItemLocation().getInvItem(),
                    arInvoiceLineItem.getIliQuantity(), AD_CMPNY);
            arInvoiceLineItem.getInvItemLocation().setIlCommittedQuantity(
                arInvoiceLineItem.getInvItemLocation().getIlCommittedQuantity()
                    - convertedQuantity);

          }

          i.remove();

          arInvoiceLineItem.remove();

        }

        // remove all invoice lines

        Collection arInvoiceLines = arInvoice.getArInvoiceLines();

        i = arInvoiceLines.iterator();

        while (i.hasNext()) {

          LocalArInvoiceLine arInvoiceLine = (LocalArInvoiceLine) i.next();

          i.remove();

          arInvoiceLine.remove();

        }

        // remove all sales order lines

        Collection arSalesOrderInvoiceLines = arInvoice.getArSalesOrderInvoiceLines();

        i = arSalesOrderInvoiceLines.iterator();

        while (i.hasNext()) {

          LocalArSalesOrderInvoiceLine arSalesOrderInvoiceLine =
              (LocalArSalesOrderInvoiceLine) i.next();

          // release previous sales order
          if (!arExistingSalesOrder.getSoDocumentNumber().equals(arSalesOrderInvoiceLine
              .getArSalesOrderLine().getArSalesOrder().getSoDocumentNumber()))
            arSalesOrderInvoiceLine.getArSalesOrderLine().getArSalesOrder()
                .setSoLock(EJBCommon.FALSE);

          i.remove();

          arSalesOrderInvoiceLine.remove();

        }

        // remove all distribution records

        Collection arDistributionRecords = arInvoice.getArDistributionRecords();

        i = arDistributionRecords.iterator();

        while (i.hasNext()) {

          LocalArDistributionRecord arDistributionRecord = (LocalArDistributionRecord) i.next();

          i.remove();

          arDistributionRecord.remove();

        }

        // add new sales order line and distribution record

        double TOTAL_TAX = 0d;
        double TOTAL_LINE = 0d;
        double TOTAL_SA_DEBIT = 0d;
        double TOTAL_DOWN_PAYMENT = arInvoice.getInvDownPayment();

        i = solList.iterator();

        LocalInvItemLocation invItemLocation = null;

        while (i.hasNext()) {

          ArModSalesOrderLineDetails mSolDetails = (ArModSalesOrderLineDetails) i.next();

          try {

            invItemLocation = invItemLocationHome.findByLocNameAndIiName(
                mSolDetails.getSolLocName(), mSolDetails.getSolIiName(), AD_CMPNY);

          } catch (FinderException ex) {

            throw new GlobalInvItemLocationNotFoundException(
                String.valueOf(mSolDetails.getSolLine()));

          }

          // start date validation

          if (adPreference.getPrfArAllowPriorDate() == EJBCommon.FALSE) {

            Collection invNegTxnCosting =
                invCostingHome.findNegTxnByGreaterThanCstDateAndIiNameAndLocName(
                    arInvoice.getInvDate(), invItemLocation.getInvItem().getIiName(),
                    invItemLocation.getInvLocation().getLocName(), AD_BRNCH, AD_CMPNY);
            if (!invNegTxnCosting.isEmpty())
              throw new GlobalInventoryDateException(invItemLocation.getInvItem().getIiName());

          }


          LocalArSalesOrderInvoiceLine arSalesOrderInvoiceLine =
              this.addArSolEntry(mSolDetails, arInvoice, AD_CMPNY);
          LocalArSalesOrderLine arSalesOrderLine = arSalesOrderInvoiceLine.getArSalesOrderLine();

          // add cost of sales distribution and inventory

          double COST = invItemLocation.getInvItem().getIiUnitCost();

          try {

            LocalInvCosting invCosting = invCostingHome
                .getByMaxCstDateToLongAndMaxCstLineNumberAndLessThanEqualCstDateAndIiNameAndLocName(
                    arInvoice.getInvDate(), invItemLocation.getInvItem().getIiName(),
                    invItemLocation.getInvLocation().getLocName(), AD_BRNCH, AD_CMPNY);

            // check if rmning vl is not zero and rmng qty is 0
            if (invCosting.getCstRemainingQuantity() <= 0) {

              HashMap criteria = new HashMap();
              criteria.put("itemName", invItemLocation.getInvItem().getIiName());
              criteria.put("location", invItemLocation.getInvLocation().getLocName());

              ArrayList branchList = new ArrayList();

              AdBranchDetails mdetails = new AdBranchDetails();
              mdetails.setBrCode(AD_BRNCH);
              branchList.add(mdetails);

              ejbRIC.executeInvFixItemCosting(criteria, branchList, AD_CMPNY);
              invCosting = invCostingHome
                  .getByMaxCstDateToLongAndMaxCstLineNumberAndLessThanEqualCstDateAndIiNameAndLocName(
                      arInvoice.getInvDate(), invItemLocation.getInvItem().getIiName(),
                      invItemLocation.getInvLocation().getLocName(), AD_BRNCH, AD_CMPNY);
            }


            // dist

            if (invCosting.getInvItemLocation().getInvItem().getIiCostMethod().equals("Average"))

              COST = invCosting.getCstRemainingQuantity() <= 0 ? COST
                  : Math.abs(
                      invCosting.getCstRemainingValue() / invCosting.getCstRemainingQuantity());

            if (COST <= 0) {
              COST = invItemLocation.getInvItem().getIiUnitCost();
            }

            else if (invCosting.getInvItemLocation().getInvItem().getIiCostMethod().equals("FIFO"))

              COST = invCosting.getCstRemainingQuantity() == 0 ? COST
                  : this.getInvFifoCost(arInvoice.getInvDate(), invItemLocation.getIlCode(),
                      arSalesOrderInvoiceLine.getSilQuantityDelivered(),
                      arSalesOrderInvoiceLine.getArSalesOrderLine().getSolUnitPrice(), false,
                      AD_BRNCH, AD_CMPNY);

            else if (invCosting.getInvItemLocation().getInvItem().getIiCostMethod()
                .equals("Standard"))

              COST = invItemLocation.getInvItem().getIiUnitCost();

          } catch (FinderException ex) {

            COST = arSalesOrderLine.getInvItemLocation().getInvItem().getIiUnitCost();

          }

          double QTY_SLD =
              this.convertByUomFromAndItemAndQuantity(arSalesOrderLine.getInvUnitOfMeasure(),
                  arSalesOrderLine.getInvItemLocation().getInvItem(),
                  arSalesOrderInvoiceLine.getSilQuantityDelivered(), AD_CMPNY);

          LocalAdBranchItemLocation adBranchItemLocation = null;

          try {
            lineNumberError = arSalesOrderLine.getSolLine();

            System.out
                .println("item loc in: " + arSalesOrderLine.getInvItemLocation().getIlCode());;
            adBranchItemLocation = adBranchItemLocationHome.findBilByIlCodeAndBrCode(
                arSalesOrderLine.getInvItemLocation().getIlCode(), AD_BRNCH, AD_CMPNY);

          } catch (FinderException ex) {

          }

          if (adPreference.getPrfArAutoComputeCogs() == EJBCommon.TRUE && arSalesOrderLine
              .getInvItemLocation().getInvItem().getIiNonInventoriable() == EJBCommon.FALSE) {
            lineNumberError = arInvoice.getArDrNextLine();
            if (adBranchItemLocation != null) {

              this.addArDrIliEntry(arInvoice.getArDrNextLine(), "COGS", EJBCommon.TRUE,
                  COST * QTY_SLD, adBranchItemLocation.getBilCoaGlCostOfSalesAccount(), arInvoice,
                  AD_BRNCH, AD_CMPNY);

              this.addArDrIliEntry(arInvoice.getArDrNextLine(), "INVENTORY", EJBCommon.FALSE,
                  COST * QTY_SLD, adBranchItemLocation.getBilCoaGlInventoryAccount(), arInvoice,
                  AD_BRNCH, AD_CMPNY);

            } else {

              this.addArDrIliEntry(arInvoice.getArDrNextLine(), "COGS", EJBCommon.TRUE,
                  COST * QTY_SLD,
                  arSalesOrderLine.getInvItemLocation().getIlGlCoaCostOfSalesAccount(), arInvoice,
                  AD_BRNCH, AD_CMPNY);

              this.addArDrIliEntry(arInvoice.getArDrNextLine(), "INVENTORY", EJBCommon.FALSE,
                  COST * QTY_SLD,
                  arSalesOrderLine.getInvItemLocation().getIlGlCoaInventoryAccount(), arInvoice,
                  AD_BRNCH, AD_CMPNY);

            }

          }

          // add quantity to item location committed quantity

          double convertedQuantity =
              this.convertByUomFromAndItemAndQuantity(arSalesOrderLine.getInvUnitOfMeasure(),
                  arSalesOrderLine.getInvItemLocation().getInvItem(),
                  arSalesOrderInvoiceLine.getSilQuantityDelivered(), AD_CMPNY);
          invItemLocation
              .setIlCommittedQuantity(invItemLocation.getIlCommittedQuantity() + convertedQuantity);

          // add inventory sale distributions

          double SIL_LINE = this.convertForeignToFunctionalCurrency(
              arInvoice.getGlFunctionalCurrency().getFcCode(),
              arInvoice.getGlFunctionalCurrency().getFcName(), arInvoice.getInvConversionDate(),
              arInvoice.getInvConversionRate(), arSalesOrderInvoiceLine.getSilAmount(), AD_CMPNY);


          double SIL_TAX = this.convertForeignToFunctionalCurrency(
              arInvoice.getGlFunctionalCurrency().getFcCode(),
              arInvoice.getGlFunctionalCurrency().getFcName(), arInvoice.getInvConversionDate(),
              arInvoice.getInvConversionRate(), arSalesOrderInvoiceLine.getSilTaxAmount(),
              AD_CMPNY);


          if (adBranchItemLocation != null) {
            // this will trigger by services(DEBIT)
            if (adBranchItemLocation.getInvItemLocation().getInvItem()
                .getIiServices() == EJBCommon.TRUE) {

              this.addArDrIliEntry(arInvoice.getArDrNextLine(), "REVENUE", EJBCommon.TRUE, SIL_LINE,
                  adBranchItemLocation.getBilCoaGlSalesAccount(), arInvoice, AD_BRNCH, AD_CMPNY);

              this.addArDrIliEntry(arInvoice.getArDrNextLine(), "OTHER", EJBCommon.FALSE, SIL_LINE,
                  adBranchItemLocation.getBilCoaGlSalesReturnAccount(), arInvoice, AD_BRNCH,
                  AD_CMPNY);

              TOTAL_SA_DEBIT += arSalesOrderInvoiceLine.getSilAmount();
            } else {
              this.addArDrIliEntry(arInvoice.getArDrNextLine(), "REVENUE", EJBCommon.FALSE,
                  SIL_LINE, adBranchItemLocation.getBilCoaGlSalesAccount(), arInvoice, AD_BRNCH,
                  AD_CMPNY);
            }



          } else {

            // this will trigger by services(DEBIT)


            if (arSalesOrderLine.getInvItemLocation().getInvItem()
                .getIiServices() == EJBCommon.TRUE) {

              this.addArDrIliEntry(arInvoice.getArDrNextLine(), "REVENUE", EJBCommon.TRUE, SIL_LINE,
                  arSalesOrderLine.getInvItemLocation().getIlGlCoaSalesAccount(), arInvoice,
                  AD_BRNCH, AD_CMPNY);

              this.addArDrIliEntry(arInvoice.getArDrNextLine(), "OTHER", EJBCommon.FALSE, SIL_LINE,
                  arSalesOrderLine.getInvItemLocation().getIlGlCoaSalesReturnAccount(), arInvoice,
                  AD_BRNCH, AD_CMPNY);


              TOTAL_SA_DEBIT += arSalesOrderInvoiceLine.getSilAmount();
            } else {

              this.addArDrIliEntry(arInvoice.getArDrNextLine(), "REVENUE", EJBCommon.FALSE,
                  SIL_LINE, arSalesOrderLine.getInvItemLocation().getIlGlCoaSalesAccount(),
                  arInvoice, AD_BRNCH, AD_CMPNY);
            }



          }

          TOTAL_LINE += arSalesOrderInvoiceLine.getSilAmount();
          TOTAL_TAX += arSalesOrderInvoiceLine.getSilTaxAmount();

        }

        // add tax distribution if necessary

        if (!arTaxCode.getTcType().equals("NONE") && !arTaxCode.getTcType().equals("EXEMPT")) {


          double TOTAL_TAX_CONV = this.convertForeignToFunctionalCurrency(
              arInvoice.getGlFunctionalCurrency().getFcCode(),
              arInvoice.getGlFunctionalCurrency().getFcName(), arInvoice.getInvConversionDate(),
              arInvoice.getInvConversionRate(), TOTAL_TAX, AD_CMPNY);

          if (arTaxCode.getTcInterimAccount() == null) {

            // add branch tax code
            LocalAdBranchArTaxCode adBranchTaxCode = null;
            Debug.print("ArInvoiceEntryControllerBean saveArInvIliEntry J");
            try {
              adBranchTaxCode = adBranchArTaxCodeHome.findBtcByTcCodeAndBrCode(
                  arInvoice.getArTaxCode().getTcCode(), AD_BRNCH, AD_CMPNY);

            } catch (FinderException ex) {

            }



            if (adBranchTaxCode != null) {

              this.addArDrEntry(arInvoice.getArDrNextLine(), "TAX", EJBCommon.FALSE, TOTAL_TAX_CONV,
                  adBranchTaxCode.getBtcGlCoaTaxCode(), null, arInvoice, AD_BRNCH, AD_CMPNY);

            } else {

              this.addArDrEntry(arInvoice.getArDrNextLine(), "TAX", EJBCommon.FALSE, TOTAL_TAX_CONV,
                  arTaxCode.getGlChartOfAccount().getCoaCode(), null, arInvoice, AD_BRNCH,
                  AD_CMPNY);
            }

          } else {

            this.addArDrEntry(arInvoice.getArDrNextLine(), "DEFERRED TAX", EJBCommon.FALSE,
                TOTAL_TAX_CONV, arTaxCode.getTcInterimAccount(), null, arInvoice, AD_BRNCH,
                AD_CMPNY);

          }

        }

        // add un earned interest
        double UNEARNED_INT_AMOUNT = 0d;

        System.out.println(
            "adPaymentTerm.getPytEnableInterest()=" + adPaymentTerm.getPytEnableInterest());
        if (arInvoice.getArCustomer().getCstAutoComputeInterest() == EJBCommon.TRUE
            && arInvoice.getArCustomer().getCstMonthlyInterestRate() > 0
            && adPaymentTerm.getPytEnableInterest() == EJBCommon.TRUE) {

          try {

            LocalAdBranchCustomer adBranchCustomer =
                adBranchCustomerHome.findBcstByCstCodeAndBrCode(
                    arInvoice.getArCustomer().getCstCode(), AD_BRNCH, AD_CMPNY);

            System.out.println("adPaymentTerm.getAdPaymentSchedules().size()="
                + adPaymentTerm.getAdPaymentSchedules().size());



            UNEARNED_INT_AMOUNT = EJBCommon.roundIt(
                (TOTAL_LINE + TOTAL_TAX - TOTAL_DOWN_PAYMENT)
                    * adPaymentTerm.getAdPaymentSchedules().size()
                    * (arInvoice.getArCustomer().getCstMonthlyInterestRate() / 100),
                this.getGlFcPrecisionUnit(AD_CMPNY));

            double UNEARNED_INT_AMOUNT_CONV = this.convertForeignToFunctionalCurrency(
                arInvoice.getGlFunctionalCurrency().getFcCode(),
                arInvoice.getGlFunctionalCurrency().getFcName(), arInvoice.getInvConversionDate(),
                arInvoice.getInvConversionRate(), UNEARNED_INT_AMOUNT, AD_CMPNY);

            this.addArDrEntry(arInvoice.getArDrNextLine(), "UNINTEREST", EJBCommon.FALSE,
                UNEARNED_INT_AMOUNT_CONV, adBranchCustomer.getBcstGlCoaUnEarnedInterestAccount(),
                null, arInvoice, AD_BRNCH, AD_CMPNY);

            this.addArDrIliEntry(arInvoice.getArDrNextLine(), "RECEIVABLE INTEREST", EJBCommon.TRUE,
                UNEARNED_INT_AMOUNT_CONV, adBranchCustomer.getBcstGlCoaReceivableAccount(),
                arInvoice, AD_BRNCH, AD_CMPNY);

          } catch (FinderException ex) {

          }


        }
        arInvoice.setInvAmountUnearnedInterest(UNEARNED_INT_AMOUNT);

        // add wtax distribution if necessary

        double W_TAX_AMOUNT = 0d;

        if (arWithholdingTaxCode.getWtcRate() != 0
            && adPreference.getPrfArWTaxRealization().equals("INVOICE")) {

          W_TAX_AMOUNT = EJBCommon.roundIt(TOTAL_LINE * (arWithholdingTaxCode.getWtcRate() / 100),
              this.getGlFcPrecisionUnit(AD_CMPNY));


          double W_TAX_AMOUNT_CONV = this.convertForeignToFunctionalCurrency(
              arInvoice.getGlFunctionalCurrency().getFcCode(),
              arInvoice.getGlFunctionalCurrency().getFcName(), arInvoice.getInvConversionDate(),
              arInvoice.getInvConversionRate(), W_TAX_AMOUNT, AD_CMPNY);


          this.addArDrEntry(arInvoice.getArDrNextLine(), "W-TAX", EJBCommon.TRUE, W_TAX_AMOUNT_CONV,
              arWithholdingTaxCode.getGlChartOfAccount().getCoaCode(), null, arInvoice, AD_BRNCH,
              AD_CMPNY);

        }

        // add payment discount if necessary

        double DISCOUNT_AMT = 0d;

        if (adPaymentTerm.getPytDiscountOnInvoice() == EJBCommon.TRUE) {

          Collection adPaymentSchedules =
              adPaymentScheduleHome.findByPytCode(adPaymentTerm.getPytCode(), AD_CMPNY);
          ArrayList adPaymentScheduleList = new ArrayList(adPaymentSchedules);
          LocalAdPaymentSchedule adPaymentSchedule =
              (LocalAdPaymentSchedule) adPaymentScheduleList.get(0);

          Collection adDiscounts =
              adDiscountHome.findByPsCode(adPaymentSchedule.getPsCode(), AD_CMPNY);
          ArrayList adDiscountList = new ArrayList(adDiscounts);
          LocalAdDiscount adDiscount = (LocalAdDiscount) adDiscountList.get(0);

          double rate = adDiscount.getDscDiscountPercent();
          DISCOUNT_AMT = (TOTAL_LINE + TOTAL_TAX) * (rate / 100d);


          double DISCOUNT_AMT_CONV = this.convertForeignToFunctionalCurrency(
              arInvoice.getGlFunctionalCurrency().getFcCode(),
              arInvoice.getGlFunctionalCurrency().getFcName(), arInvoice.getInvConversionDate(),
              arInvoice.getInvConversionRate(), DISCOUNT_AMT, AD_CMPNY);


          this.addArDrEntry(arInvoice.getArDrNextLine(), "DISCOUNT", EJBCommon.TRUE,
              DISCOUNT_AMT_CONV, adPaymentTerm.getGlChartOfAccount().getCoaCode(), null, arInvoice,
              AD_BRNCH, AD_CMPNY);

        }


        amountDue = TOTAL_LINE + TOTAL_TAX - W_TAX_AMOUNT - DISCOUNT_AMT + UNEARNED_INT_AMOUNT;


        // add receivable distribution

        try {

          LocalAdBranchCustomer adBranchCustomer = adBranchCustomerHome.findBcstByCstCodeAndBrCode(
              arInvoice.getArCustomer().getCstCode(), AD_BRNCH, AD_CMPNY);
          // ADDED " - TOTAL_SA_DEBIT" for Services (DEBIT)

          double AMNT_DUE_CONV = this.convertForeignToFunctionalCurrency(
              arInvoice.getGlFunctionalCurrency().getFcCode(),
              arInvoice.getGlFunctionalCurrency().getFcName(), arInvoice.getInvConversionDate(),
              arInvoice.getInvConversionRate(), amountDue, AD_CMPNY);

          this.addArDrIliEntry(arInvoice.getArDrNextLine(), "RECEIVABLE", EJBCommon.TRUE,
              AMNT_DUE_CONV, adBranchCustomer.getBcstGlCoaReceivableAccount(), arInvoice, AD_BRNCH,
              AD_CMPNY);

        } catch (FinderException ex) {

        }

        // compute invoice amount due


        // set invoice amount due

        arInvoice.setInvAmountDue(amountDue);

        // remove all payment schedule

        Collection arInvoicePaymentSchedules = arInvoice.getArInvoicePaymentSchedules();

        i = arInvoicePaymentSchedules.iterator();

        while (i.hasNext()) {

          LocalArInvoicePaymentSchedule arInvoicePaymentSchedule =
              (LocalArInvoicePaymentSchedule) i.next();

          i.remove();

          arInvoicePaymentSchedule.remove();

        }

        // create invoice payment schedule

        short precisionUnit = this.getGlFcPrecisionUnit(AD_CMPNY);
        double TOTAL_PAYMENT_SCHEDULE = 0d;
        double TOTAL_PAYMENT_INT = 0d;
        double TOTAL_PAYMENT_SCHEDULE_INT = TOTAL_LINE + TOTAL_TAX - TOTAL_DOWN_PAYMENT;

        GregorianCalendar gcPrevDateDue = new GregorianCalendar();
        GregorianCalendar gcDateDue = new GregorianCalendar();
        gcPrevDateDue.setTime(arInvoice.getInvEffectivityDate());

        Collection adPaymentSchedules = adPaymentTerm.getAdPaymentSchedules();

        boolean first = true;

        i = adPaymentSchedules.iterator();

        while (i.hasNext()) {

          LocalAdPaymentSchedule adPaymentSchedule = (LocalAdPaymentSchedule) i.next();

          // get date due

          if (arInvoice.getAdPaymentTerm().getPytScheduleBasis().equals("DEFAULT")) {

            gcDateDue.setTime(arInvoice.getInvEffectivityDate());
            gcDateDue.add(Calendar.DATE, adPaymentSchedule.getPsDueDay());

          } else if (arInvoice.getAdPaymentTerm().getPytScheduleBasis().equals("MONTHLY")) {

            gcDateDue = gcPrevDateDue;
            gcDateDue.add(Calendar.MONTH, 1);
            gcPrevDateDue = gcDateDue;

          } else if (arInvoice.getAdPaymentTerm().getPytScheduleBasis().equals("BI-MONTHLY")) {

            gcDateDue = gcPrevDateDue;

            if (gcPrevDateDue.get(Calendar.MONTH) != 1) {
              if (gcPrevDateDue.getActualMaximum(Calendar.DATE) == 31
                  && gcPrevDateDue.get(Calendar.DATE) > 15
                  && gcPrevDateDue.get(Calendar.DATE) < 31) {
                gcDateDue.add(Calendar.DATE, 16);
              } else {
                gcDateDue.add(Calendar.DATE, 15);
              }
            } else if (gcPrevDateDue.get(Calendar.MONTH) == 1) {
              if (gcPrevDateDue.getActualMaximum(Calendar.DATE) == 28
                  && gcPrevDateDue.get(Calendar.DATE) == 14) {
                gcDateDue.add(Calendar.DATE, 14);
              } else if (gcPrevDateDue.getActualMaximum(Calendar.DATE) == 28
                  && gcPrevDateDue.get(Calendar.DATE) >= 15
                  && gcPrevDateDue.get(Calendar.DATE) < 28) {
                gcDateDue.add(Calendar.DATE, 13);
              } else if (gcPrevDateDue.getActualMaximum(Calendar.DATE) == 29
                  && gcPrevDateDue.get(Calendar.DATE) >= 15
                  && gcPrevDateDue.get(Calendar.DATE) < 29) {
                gcDateDue.add(Calendar.DATE, 14);
              } else {
                gcDateDue.add(Calendar.DATE, 15);
              }
            }

            gcPrevDateDue = gcDateDue;

          }

          if (first == true && arInvoice.getInvDownPayment() > 0) {

            LocalArInvoicePaymentSchedule arInvoicePaymentScheduleDownPayment =
                arInvoicePaymentScheduleHome.create(arInvoice.getInvEffectivityDate(), (short) 0,
                    arInvoice.getInvDownPayment(), 0d, EJBCommon.FALSE, (short) 0,
                    arInvoice.getInvEffectivityDate(), 0d, 0d, AD_CMPNY);

            arInvoice.addArInvoicePaymentSchedule(arInvoicePaymentScheduleDownPayment);
            first = false;
          }

          // create a payment schedule


          double PAYMENT_SCHEDULE_AMOUNT = 0;
          double PAYMENT_SCHEDULE_INT = 0;
          double PAYMENT_SCHEDULE_PRINCIPAL = 0;


          System.out.println("adPaymentTerm.getPytMonthlyInterestRate() ="
              + adPaymentTerm.getPytMonthlyInterestRate());

          // if last payment schedule subtract to avoid rounding difference error

          if (i.hasNext()) {

            PAYMENT_SCHEDULE_AMOUNT =
                EJBCommon.roundIt(
                    (adPaymentSchedule.getPsRelativeAmount() / adPaymentTerm.getPytBaseAmount())
                        * (arInvoice.getInvAmountDue() - arInvoice.getInvDownPayment()),
                    precisionUnit);
            System.out
                .println("------------>TOTAL_PAYMENT_SCHEDULE_INT=" + TOTAL_PAYMENT_SCHEDULE_INT);


            PAYMENT_SCHEDULE_INT = EJBCommon.roundIt(
                (TOTAL_PAYMENT_SCHEDULE_INT) * (adPaymentTerm.getPytMonthlyInterestRate() / 100),
                precisionUnit);

            PAYMENT_SCHEDULE_PRINCIPAL = -(PAYMENT_SCHEDULE_AMOUNT) + PAYMENT_SCHEDULE_INT;


          } else {

            System.out
                .println("------------>TOTAL_PAYMENT_SCHEDULE_INT=" + TOTAL_PAYMENT_SCHEDULE_INT);


            PAYMENT_SCHEDULE_AMOUNT = (arInvoice.getInvAmountDue() - arInvoice.getInvDownPayment())
                - TOTAL_PAYMENT_SCHEDULE;

            PAYMENT_SCHEDULE_INT = EJBCommon.roundIt(
                (TOTAL_PAYMENT_SCHEDULE_INT) * (adPaymentTerm.getPytMonthlyInterestRate() / 100),
                precisionUnit);

            PAYMENT_SCHEDULE_PRINCIPAL = -(PAYMENT_SCHEDULE_AMOUNT) + PAYMENT_SCHEDULE_INT;


          }

          LocalArInvoicePaymentSchedule arInvoicePaymentSchedule =
              arInvoicePaymentScheduleHome.create(gcDateDue.getTime(),
                  adPaymentSchedule.getPsLineNumber(), PAYMENT_SCHEDULE_AMOUNT, 0d, EJBCommon.FALSE,
                  (short) 0, gcDateDue.getTime(), 0d, 0d, AD_CMPNY);


          arInvoicePaymentSchedule.setIpsInterestDue(PAYMENT_SCHEDULE_INT);
          System.out.println("PAYMENT_SCHEDULE_INT=" + PAYMENT_SCHEDULE_INT);
          arInvoice.addArInvoicePaymentSchedule(arInvoicePaymentSchedule);

          TOTAL_PAYMENT_SCHEDULE += PAYMENT_SCHEDULE_AMOUNT;
          TOTAL_PAYMENT_INT += PAYMENT_SCHEDULE_INT;
          TOTAL_PAYMENT_SCHEDULE_INT += PAYMENT_SCHEDULE_PRINCIPAL;

        }

      }



      // insufficient stock checking
      System.out.println("adPreference.getPrfArCheckInsufficientStock()="
          + adPreference.getPrfArCheckInsufficientStock());


      if (adPreference.getPrfArCheckInsufficientStock() == EJBCommon.TRUE && !isDraft) {
        System.out.println("CHECK INSUFFICIENT STOCK");
        boolean hasInsufficientItems = false;
        String insufficientItems = "";

        Collection arSalesOrderInvoiceLine = arInvoice.getArSalesOrderInvoiceLines();

        Iterator i = arSalesOrderInvoiceLine.iterator();

        while (i.hasNext()) {

          LocalArSalesOrderInvoiceLine arSalesOrderInvoiceLineItem =
              (LocalArSalesOrderInvoiceLine) i.next();

          if (arSalesOrderInvoiceLineItem.getArSalesOrderLine().getInvItemLocation().getInvItem()
              .getIiNonInventoriable() == EJBCommon.TRUE)
            continue;

          LocalInvCosting invCosting = null;

          double ILI_QTY = this.convertByUomAndQuantity(
              arSalesOrderInvoiceLineItem.getArSalesOrderLine().getInvUnitOfMeasure(),
              arSalesOrderInvoiceLineItem.getArSalesOrderLine().getInvItemLocation().getInvItem(),
              Math.abs(arSalesOrderInvoiceLineItem.getSilQuantityDelivered()), AD_CMPNY);

          try {

            invCosting = invCostingHome
                .getByMaxCstDateToLongAndMaxCstLineNumberAndLessThanEqualCstDateAndIiNameAndLocName(
                    arInvoice.getInvDate(),
                    arSalesOrderInvoiceLineItem.getArSalesOrderLine().getInvItemLocation()
                        .getInvItem().getIiName(),
                    arSalesOrderInvoiceLineItem.getArSalesOrderLine().getInvItemLocation()
                        .getInvLocation().getLocName(),
                    AD_BRNCH, AD_CMPNY);

          } catch (FinderException ex) {

          }

          double LOWEST_QTY = this.convertByUomAndQuantity(
              arSalesOrderInvoiceLineItem.getArSalesOrderLine().getInvUnitOfMeasure(),
              arSalesOrderInvoiceLineItem.getArSalesOrderLine().getInvItemLocation().getInvItem(),
              1, AD_CMPNY);



          // System.out.println("invCosting.getCstRemainingQuantity()="+invCosting.getCstRemainingQuantity());
          if ((invCosting == null || invCosting.getCstRemainingQuantity() == 0
              || invCosting.getCstRemainingQuantity() - ILI_QTY <= -LOWEST_QTY)
              && arSalesOrderInvoiceLineItem.getArSalesOrderLine().getInvItemLocation().getInvItem()
                  .getIiNonInventoriable() == (byte) 0) {

            hasInsufficientItems = true;

            insufficientItems += arSalesOrderInvoiceLineItem.getArSalesOrderLine()
                .getInvItemLocation().getInvItem().getIiName() + ", ";
          }

        }
        if (hasInsufficientItems) {

          throw new GlobalRecordInvalidException(
              insufficientItems.substring(0, insufficientItems.lastIndexOf(",")));
        }
      }



      // generate approval status

      String INV_APPRVL_STATUS = null;

      if (!isDraft) {

        LocalAdApproval adApproval = adApprovalHome.findByAprAdCompany(AD_CMPNY);

        // validate if total amount + unposted invoices' amount + current balance + unposted
        // receipts's amount
        // does not exceed customer's credit limit

        double balance = 0;
        LocalAdApprovalDocument adInvoiceApprovalDocument =
            adApprovalDocumentHome.findByAdcType("AR INVOICE", AD_CMPNY);

        if (arCustomer.getCstCreditLimit() > 0) {

          balance = computeTotalBalance(details.getInvCode(), CST_CSTMR_CODE, AD_CMPNY);

          balance += amountDue;

          if (arCustomer.getCstCreditLimit() < balance && (adApproval
              .getAprEnableArInvoice() == EJBCommon.FALSE
              || (adApproval.getAprEnableArInvoice() == EJBCommon.TRUE && adInvoiceApprovalDocument
                  .getAdcEnableCreditLimitChecking() == EJBCommon.FALSE))) {

            throw new ArINVAmountExceedsCreditLimitException();

          }

        }

        // find overdue invoices
        Collection arOverdueInvoices =
            arInvoicePaymentScheduleHome.findOverdueIpsByInvDateAndCstCustomerCode(
                arInvoice.getInvDate(), CST_CSTMR_CODE, AD_CMPNY);


        // check if ar invoice approval is enabled

        if (adApproval.getAprEnableArInvoice() == EJBCommon.FALSE
            || (arCustomer.getCstCreditLimit() > balance && arOverdueInvoices.size() == 0)) {

          INV_APPRVL_STATUS = "N/A";

        } else {

          // check if invoice is self approved

          LocalAdAmountLimit adAmountLimit = null;

          try {

            adAmountLimit = adAmountLimitHome.findByAdcTypeAndAuTypeAndUsrName("AR INVOICE",
                "REQUESTER", details.getInvLastModifiedBy(), AD_CMPNY);

          } catch (FinderException ex) {

            throw new GlobalNoApprovalRequesterFoundException();

          }


          if (arInvoice.getInvAmountDue() <= adAmountLimit.getCalAmountLimit()
              && (arCustomer.getCstCreditLimit() == 0
                  || arCustomer.getCstCreditLimit() > balance)) {

            INV_APPRVL_STATUS = "N/A";

          } else {

            // for approval, create approval queue

            Collection adAmountLimits = adAmountLimitHome.findByAdcTypeAndGreaterThanCalAmountLimit(
                "AR INVOICE", adAmountLimit.getCalAmountLimit(), AD_CMPNY);

            if (adAmountLimits.isEmpty()) {

              Collection adApprovalUsers = adApprovalUserHome.findByAuTypeAndCalCode("APPROVER",
                  adAmountLimit.getCalCode(), AD_CMPNY);

              if (adApprovalUsers.isEmpty()) {

                throw new GlobalNoApprovalApproverFoundException();

              }

              Iterator j = adApprovalUsers.iterator();

              while (j.hasNext()) {

                LocalAdApprovalUser adApprovalUser = (LocalAdApprovalUser) j.next();

                LocalAdApprovalQueue adApprovalQueue =
                    adApprovalQueueHome.create(EJBCommon.TRUE, "AR INVOICE", arInvoice.getInvCode(),
                        arInvoice.getInvNumber(), arInvoice.getInvDate(),
                        adAmountLimit.getCalAndOr(), adApprovalUser.getAuOr(), AD_BRNCH, AD_CMPNY);

                adApprovalUser.getAdUser().addAdApprovalQueue(adApprovalQueue);

              }

            } else {

              boolean isApprovalUsersFound = false;

              Iterator i = adAmountLimits.iterator();

              while (i.hasNext()) {

                LocalAdAmountLimit adNextAmountLimit = (LocalAdAmountLimit) i.next();

                if (arInvoice.getInvAmountDue() <= adNextAmountLimit.getCalAmountLimit()) {

                  Collection adApprovalUsers = adApprovalUserHome.findByAuTypeAndCalCode("APPROVER",
                      adAmountLimit.getCalCode(), AD_CMPNY);

                  Iterator j = adApprovalUsers.iterator();

                  while (j.hasNext()) {

                    isApprovalUsersFound = true;

                    LocalAdApprovalUser adApprovalUser = (LocalAdApprovalUser) j.next();

                    LocalAdApprovalQueue adApprovalQueue = adApprovalQueueHome.create(
                        EJBCommon.TRUE, "AR INVOICE", arInvoice.getInvCode(),
                        arInvoice.getInvNumber(), arInvoice.getInvDate(),
                        adAmountLimit.getCalAndOr(), adApprovalUser.getAuOr(), AD_BRNCH, AD_CMPNY);

                    adApprovalUser.getAdUser().addAdApprovalQueue(adApprovalQueue);

                  }

                  break;

                } else if (!i.hasNext()) {

                  Collection adApprovalUsers = adApprovalUserHome.findByAuTypeAndCalCode("APPROVER",
                      adNextAmountLimit.getCalCode(), AD_CMPNY);

                  Iterator j = adApprovalUsers.iterator();

                  while (j.hasNext()) {

                    isApprovalUsersFound = true;

                    LocalAdApprovalUser adApprovalUser = (LocalAdApprovalUser) j.next();

                    LocalAdApprovalQueue adApprovalQueue =
                        adApprovalQueueHome.create(EJBCommon.TRUE, "AR INVOICE",
                            arInvoice.getInvCode(), arInvoice.getInvNumber(),
                            arInvoice.getInvDate(), adNextAmountLimit.getCalAndOr(),
                            adApprovalUser.getAuOr(), AD_BRNCH, AD_CMPNY);

                    adApprovalUser.getAdUser().addAdApprovalQueue(adApprovalQueue);

                  }

                  break;

                }

                adAmountLimit = adNextAmountLimit;

              }

              if (!isApprovalUsersFound) {

                throw new GlobalNoApprovalApproverFoundException();

              }

            }

            INV_APPRVL_STATUS = "PENDING";
          }
        }

      }

      if (INV_APPRVL_STATUS != null && INV_APPRVL_STATUS.equals("N/A")
          && adPreference.getPrfArGlPostingType().equals("AUTO-POST UPON APPROVAL")) {

        this.executeArInvPost(arInvoice.getInvCode(), arInvoice.getInvLastModifiedBy(), AD_BRNCH,
            AD_CMPNY);

        // Set SO Lock if Fully Served

        Iterator solIter = arExistingSalesOrder.getArSalesOrderLines().iterator();

        boolean isOpenSO = false;

        while (solIter.hasNext()) {

          LocalArSalesOrderLine arSalesOrderLine = (LocalArSalesOrderLine) solIter.next();

          Iterator soInvLnIter = arSalesOrderLine.getArSalesOrderInvoiceLines().iterator();
          double QUANTITY_SOLD = 0d;

          while (soInvLnIter.hasNext()) {

            LocalArSalesOrderInvoiceLine arSalesOrderInvoiceLine =
                (LocalArSalesOrderInvoiceLine) soInvLnIter.next();

            if (arSalesOrderInvoiceLine.getArInvoice().getInvPosted() == EJBCommon.TRUE) {

              QUANTITY_SOLD += arSalesOrderInvoiceLine.getSilQuantityDelivered();

            }

          }

          double TOTAL_REMAINING_QTY = arSalesOrderLine.getSolQuantity() - QUANTITY_SOLD;

          if (TOTAL_REMAINING_QTY > 0) {
            isOpenSO = true;
            break;
          }

        }

        if (isOpenSO)
          arExistingSalesOrder.setSoLock(EJBCommon.FALSE);
        else
          arExistingSalesOrder.setSoLock(EJBCommon.TRUE);



      }

      // set invoice approval status

      arInvoice.setInvApprovalStatus(INV_APPRVL_STATUS);

      return arInvoice.getInvCode();

    } catch (GlobalRecordAlreadyDeletedException ex) {

      getSessionContext().setRollbackOnly();
      throw ex;

    } catch (GlobalDocumentNumberNotUniqueException ex) {

      getSessionContext().setRollbackOnly();
      throw ex;

    } catch (GlobalNoApprovalRequesterFoundException ex) {

      getSessionContext().setRollbackOnly();
      throw ex;

    } catch (GlobalConversionDateNotExistException ex) {

      getSessionContext().setRollbackOnly();
      throw ex;

    } catch (GlobalPaymentTermInvalidException ex) {

      getSessionContext().setRollbackOnly();
      throw ex;

    } catch (GlobalTransactionAlreadyApprovedException ex) {

      getSessionContext().setRollbackOnly();
      throw ex;

    } catch (GlobalTransactionAlreadyPendingException ex) {

      getSessionContext().setRollbackOnly();
      throw ex;

    } catch (GlobalTransactionAlreadyPostedException ex) {

      getSessionContext().setRollbackOnly();
      throw ex;

    } catch (GlobalTransactionAlreadyVoidException ex) {

      getSessionContext().setRollbackOnly();
      throw ex;

    } catch (GlobalRecordInvalidException ex) {

      getSessionContext().setRollbackOnly();
      throw ex;


    } catch (GlobalNoApprovalApproverFoundException ex) {

      getSessionContext().setRollbackOnly();
      throw ex;

    } catch (GlobalInvItemLocationNotFoundException ex) {

      getSessionContext().setRollbackOnly();
      throw ex;

    } catch (GlJREffectiveDateNoPeriodExistException ex) {

      getSessionContext().setRollbackOnly();
      throw ex;

    } catch (GlJREffectiveDatePeriodClosedException ex) {

      getSessionContext().setRollbackOnly();
      throw ex;

    } catch (GlobalJournalNotBalanceException ex) {

      getSessionContext().setRollbackOnly();
      throw ex;

    } catch (GlobalTransactionAlreadyLockedException ex) {

      getSessionContext().setRollbackOnly();
      throw ex;

    } catch (GlobalInventoryDateException ex) {

      getSessionContext().setRollbackOnly();
      throw ex;

    } catch (GlobalBranchAccountNumberInvalidException ex) {

      // Retrive Error a Line Number
      ex.setLineNumberError(lineNumberError);
      getSessionContext().setRollbackOnly();
      throw ex;

    } catch (ArINVAmountExceedsCreditLimitException ex) {

      getSessionContext().setRollbackOnly();
      throw ex;

    } catch (AdPRFCoaGlVarianceAccountNotFoundException ex) {

      getSessionContext().setRollbackOnly();
      throw ex;

    } catch (GlobalExpiryDateNotFoundException ex) {

      getSessionContext().setRollbackOnly();
      throw ex;

    } catch (ArInvDuplicateUploadNumberException ex) {

      getSessionContext().setRollbackOnly();
      throw ex;

    } catch (Exception ex) {

      Debug.printStackTrace(ex);
      getSessionContext().setRollbackOnly();
      throw new EJBException(ex.getMessage());

    }

  }



  /**
   * @ejb:interface-method view-type="remote"
   **/
  public Integer saveArInvJolEntry(
      com.util.ArInvoiceDetails details, String PYT_NM, String TC_NM, String WTC_NM, String FC_NM,
      String CST_CSTMR_CODE, String IB_NM, ArrayList jolList, boolean isDraft,
      String SLP_SLSPRSN_CODE, Integer AD_BRNCH, Integer AD_CMPNY
  ) throws GlobalRecordAlreadyDeletedException, GlobalDocumentNumberNotUniqueException,
      GlobalConversionDateNotExistException, GlobalPaymentTermInvalidException,
      GlobalTransactionAlreadyApprovedException, GlobalTransactionAlreadyPendingException,
      GlobalTransactionAlreadyPostedException, GlobalTransactionAlreadyVoidException,
      GlobalNoApprovalRequesterFoundException, GlobalNoApprovalApproverFoundException,
      GlobalInvItemLocationNotFoundException, GlJREffectiveDateNoPeriodExistException,
      GlJREffectiveDatePeriodClosedException, GlobalJournalNotBalanceException,
      GlobalTransactionAlreadyLockedException, GlobalInventoryDateException,
      GlobalBranchAccountNumberInvalidException, ArINVAmountExceedsCreditLimitException,
      AdPRFCoaGlVarianceAccountNotFoundException, GlobalExpiryDateNotFoundException,
      ArInvDuplicateUploadNumberException {

    Debug.print("ArInvoiceEntryControllerBean saveArInvJolEntry");

    LocalArInvoiceHome arInvoiceHome = null;
    LocalArSalesOrderInvoiceLineHome arSalesOrderInvoiceLineHome = null;
    LocalAdDocumentSequenceAssignmentHome adDocumentSequenceAssignmentHome = null;
    LocalAdBranchDocumentSequenceAssignmentHome adBranchDocumentSequenceAssignmentHome = null;
    LocalGlFunctionalCurrencyHome glFunctionalCurrencyHome = null;
    LocalGlFunctionalCurrencyRateHome glFunctionalCurrencyRateHome = null;
    LocalAdPaymentTermHome adPaymentTermHome = null;
    LocalArTaxCodeHome arTaxCodeHome = null;
    LocalArWithholdingTaxCodeHome arWithholdingTaxCodeHome = null;
    LocalArCustomerHome arCustomerHome = null;
    LocalArInvoiceBatchHome arInvoiceBatchHome = null;
    LocalAdPreferenceHome adPreferenceHome = null;
    LocalInvItemLocationHome invItemLocationHome = null;
    LocalArSalesOrderHome arSalesOrderHome = null;
    LocalInvCostingHome invCostingHome = null;
    LocalAdPaymentScheduleHome adPaymentScheduleHome = null;
    LocalAdDiscountHome adDiscountHome = null;
    LocalArInvoicePaymentScheduleHome arInvoicePaymentScheduleHome = null;
    LocalAdApprovalHome adApprovalHome = null;
    LocalAdAmountLimitHome adAmountLimitHome = null;
    LocalAdApprovalUserHome adApprovalUserHome = null;
    LocalAdApprovalQueueHome adApprovalQueueHome = null;
    LocalAdApprovalDocumentHome adApprovalDocumentHome = null;
    LocalAdBranchItemLocationHome adBranchItemLocationHome = null;
    LocalAdBranchCustomerHome adBranchCustomerHome = null;
    LocalArSalespersonHome arSalespersonHome = null;
    LocalGlSetOfBookHome glSetOfBookHome = null;
    LocalAdBranchArTaxCodeHome adBranchArTaxCodeHome = null;
    LocalArJobOrderHome arJobOrderHome = null;
    LocalArJobOrderLineHome arJobOrderLineHome = null;

    LocalArInvoice arInvoice = null;
    InvRepItemCostingControllerHome homeRIC = null;
    InvRepItemCostingController ejbRIC = null;



    // Initialize EJB Home

    Date txnStartDate = new Date();



    try {

      homeRIC = (InvRepItemCostingControllerHome) com.util.EJBHomeFactory
          .lookUpHome("ejb/InvRepItemCostingControllerEJB", InvRepItemCostingControllerHome.class);

      adBranchArTaxCodeHome = (LocalAdBranchArTaxCodeHome) EJBHomeFactory
          .lookUpLocalHome(LocalAdBranchArTaxCodeHome.JNDI_NAME, LocalAdBranchArTaxCodeHome.class);
      arInvoiceHome = (LocalArInvoiceHome) EJBHomeFactory
          .lookUpLocalHome(LocalArInvoiceHome.JNDI_NAME, LocalArInvoiceHome.class);
      arSalesOrderInvoiceLineHome =
          (LocalArSalesOrderInvoiceLineHome) EJBHomeFactory.lookUpLocalHome(
              LocalArSalesOrderInvoiceLineHome.JNDI_NAME, LocalArSalesOrderInvoiceLineHome.class);
      adDocumentSequenceAssignmentHome = (LocalAdDocumentSequenceAssignmentHome) EJBHomeFactory
          .lookUpLocalHome(LocalAdDocumentSequenceAssignmentHome.JNDI_NAME,
              LocalAdDocumentSequenceAssignmentHome.class);
      adBranchDocumentSequenceAssignmentHome =
          (LocalAdBranchDocumentSequenceAssignmentHome) EJBHomeFactory.lookUpLocalHome(
              LocalAdBranchDocumentSequenceAssignmentHome.JNDI_NAME,
              LocalAdBranchDocumentSequenceAssignmentHome.class);
      glFunctionalCurrencyHome = (LocalGlFunctionalCurrencyHome) EJBHomeFactory.lookUpLocalHome(
          LocalGlFunctionalCurrencyHome.JNDI_NAME, LocalGlFunctionalCurrencyHome.class);
      glFunctionalCurrencyRateHome =
          (LocalGlFunctionalCurrencyRateHome) EJBHomeFactory.lookUpLocalHome(
              LocalGlFunctionalCurrencyRateHome.JNDI_NAME, LocalGlFunctionalCurrencyRateHome.class);
      adPaymentTermHome = (LocalAdPaymentTermHome) EJBHomeFactory
          .lookUpLocalHome(LocalAdPaymentTermHome.JNDI_NAME, LocalAdPaymentTermHome.class);
      arTaxCodeHome = (LocalArTaxCodeHome) EJBHomeFactory
          .lookUpLocalHome(LocalArTaxCodeHome.JNDI_NAME, LocalArTaxCodeHome.class);
      arWithholdingTaxCodeHome = (LocalArWithholdingTaxCodeHome) EJBHomeFactory.lookUpLocalHome(
          LocalArWithholdingTaxCodeHome.JNDI_NAME, LocalArWithholdingTaxCodeHome.class);
      arCustomerHome = (LocalArCustomerHome) EJBHomeFactory
          .lookUpLocalHome(LocalArCustomerHome.JNDI_NAME, LocalArCustomerHome.class);
      arInvoiceBatchHome = (LocalArInvoiceBatchHome) EJBHomeFactory
          .lookUpLocalHome(LocalArInvoiceBatchHome.JNDI_NAME, LocalArInvoiceBatchHome.class);
      adPreferenceHome = (LocalAdPreferenceHome) EJBHomeFactory
          .lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);
      invItemLocationHome = (LocalInvItemLocationHome) EJBHomeFactory
          .lookUpLocalHome(LocalInvItemLocationHome.JNDI_NAME, LocalInvItemLocationHome.class);
      arSalesOrderHome = (LocalArSalesOrderHome) EJBHomeFactory
          .lookUpLocalHome(LocalArSalesOrderHome.JNDI_NAME, LocalArSalesOrderHome.class);
      invCostingHome = (LocalInvCostingHome) EJBHomeFactory
          .lookUpLocalHome(LocalInvCostingHome.JNDI_NAME, LocalInvCostingHome.class);
      adPaymentScheduleHome = (LocalAdPaymentScheduleHome) EJBHomeFactory
          .lookUpLocalHome(LocalAdPaymentScheduleHome.JNDI_NAME, LocalAdPaymentScheduleHome.class);
      adDiscountHome = (LocalAdDiscountHome) EJBHomeFactory
          .lookUpLocalHome(LocalAdDiscountHome.JNDI_NAME, LocalAdDiscountHome.class);
      arInvoicePaymentScheduleHome =
          (LocalArInvoicePaymentScheduleHome) EJBHomeFactory.lookUpLocalHome(
              LocalArInvoicePaymentScheduleHome.JNDI_NAME, LocalArInvoicePaymentScheduleHome.class);
      adApprovalHome = (LocalAdApprovalHome) EJBHomeFactory
          .lookUpLocalHome(LocalAdApprovalHome.JNDI_NAME, LocalAdApprovalHome.class);
      adAmountLimitHome = (LocalAdAmountLimitHome) EJBHomeFactory
          .lookUpLocalHome(LocalAdAmountLimitHome.JNDI_NAME, LocalAdAmountLimitHome.class);
      adApprovalUserHome = (LocalAdApprovalUserHome) EJBHomeFactory
          .lookUpLocalHome(LocalAdApprovalUserHome.JNDI_NAME, LocalAdApprovalUserHome.class);
      adApprovalDocumentHome = (LocalAdApprovalDocumentHome) EJBHomeFactory.lookUpLocalHome(
          LocalAdApprovalDocumentHome.JNDI_NAME, LocalAdApprovalDocumentHome.class);
      adApprovalQueueHome = (LocalAdApprovalQueueHome) EJBHomeFactory
          .lookUpLocalHome(LocalAdApprovalQueueHome.JNDI_NAME, LocalAdApprovalQueueHome.class);
      adBranchItemLocationHome = (LocalAdBranchItemLocationHome) EJBHomeFactory.lookUpLocalHome(
          LocalAdBranchItemLocationHome.JNDI_NAME, LocalAdBranchItemLocationHome.class);
      adBranchCustomerHome = (LocalAdBranchCustomerHome) EJBHomeFactory
          .lookUpLocalHome(LocalAdBranchCustomerHome.JNDI_NAME, LocalAdBranchCustomerHome.class);
      arSalespersonHome = (LocalArSalespersonHome) EJBHomeFactory
          .lookUpLocalHome(LocalArSalespersonHome.JNDI_NAME, LocalArSalespersonHome.class);
      glSetOfBookHome = (LocalGlSetOfBookHome) EJBHomeFactory
          .lookUpLocalHome(LocalGlSetOfBookHome.JNDI_NAME, LocalGlSetOfBookHome.class);
      arJobOrderHome = (LocalArJobOrderHome) EJBHomeFactory
          .lookUpLocalHome(LocalArJobOrderHome.JNDI_NAME, LocalArJobOrderHome.class);
      arJobOrderLineHome = (LocalArJobOrderLineHome) EJBHomeFactory
          .lookUpLocalHome(LocalArJobOrderLineHome.JNDI_NAME, LocalArJobOrderLineHome.class);

    } catch (NamingException ex) {

      throw new EJBException(ex.getMessage());

    }


    try {
      ejbRIC = homeRIC.create();
    } catch (RemoteException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    } catch (CreateException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }


    int lineNumberError = 0;

    try {

      // validate if invoice is already deleted

      try {

        if (details.getInvCode() != null) {

          arInvoice = arInvoiceHome.findByPrimaryKey(details.getInvCode());

        }

      } catch (FinderException ex) {

        throw new GlobalRecordAlreadyDeletedException();

      }

      // validate if invoice is already posted, void, arproved or pending

      if (details.getInvCode() != null) {

        if (arInvoice.getInvApprovalStatus() != null) {

          if (arInvoice.getInvApprovalStatus().equals("APPROVED")
              || arInvoice.getInvApprovalStatus().equals("N/A")) {

            throw new GlobalTransactionAlreadyApprovedException();


          } else if (arInvoice.getInvApprovalStatus().equals("PENDING")) {

            throw new GlobalTransactionAlreadyPendingException();

          }

        }

        if (arInvoice.getInvPosted() == EJBCommon.TRUE) {

          throw new GlobalTransactionAlreadyPostedException();

        } else if (arInvoice.getInvVoid() == EJBCommon.TRUE) {

          throw new GlobalTransactionAlreadyVoidException();

        }

      }

      // invoice void

      if (details.getInvCode() != null && details.getInvVoid() == EJBCommon.TRUE
          && arInvoice.getInvPosted() == EJBCommon.FALSE) {

        arInvoice.setInvVoid(EJBCommon.TRUE);
        arInvoice.setInvLastModifiedBy(details.getInvLastModifiedBy());
        arInvoice.setInvDateLastModified(details.getInvDateLastModified());

        return arInvoice.getInvCode();

      }

      // validate if document number is unique document number is automatic then set next sequence

      LocalAdBranchDocumentSequenceAssignment adBranchDocumentSequenceAssignment = null;
      LocalAdDocumentSequenceAssignment adDocumentSequenceAssignment = null;

      if (details.getInvCode() == null) {

        System.out.println("code is null");

        System.out.println("document type: " + details.getInvDocumentType());
        String documentType =
            details.getInvDocumentType().trim().equals("") ? null : details.getInvDocumentType();



        try {
          if (documentType != null) {
            adDocumentSequenceAssignment =
                adDocumentSequenceAssignmentHome.findByDcName(documentType, AD_CMPNY);
          } else {
            documentType = "AR INVOICE";
          }
        } catch (FinderException ex) {
          documentType = "AR INVOICE";
        }



        try {

          adDocumentSequenceAssignment =
              adDocumentSequenceAssignmentHome.findByDcName(documentType, AD_CMPNY);

        } catch (FinderException ex) {

        }

        try {

          adBranchDocumentSequenceAssignment =
              adBranchDocumentSequenceAssignmentHome.findBdsByDsaCodeAndBrCode(
                  adDocumentSequenceAssignment.getDsaCode(), AD_BRNCH, AD_CMPNY);

        } catch (FinderException ex) {

        }

        LocalArInvoice arExistingInvoice = null;

        try {

          arExistingInvoice = arInvoiceHome.findByInvNumberAndInvCreditMemoAndBrCode(
              details.getInvNumber(), EJBCommon.FALSE, AD_BRNCH, AD_CMPNY);

        } catch (FinderException ex) {

        }

        if (arExistingInvoice != null) {

          throw new GlobalDocumentNumberNotUniqueException();

        }
        /*
         * if (adDocumentSequenceAssignment.getAdDocumentSequence().getDsNumberingType() == 'A' &&
         * (details.getInvNumber() == null || details.getInvNumber().trim().length() == 0)) {
         * 
         * while (true) {
         * 
         * if (adBranchDocumentSequenceAssignment == null ||
         * adBranchDocumentSequenceAssignment.getBdsNextSequence() == null) {
         * 
         * try {
         * 
         * arInvoiceHome.findByInvNumberAndInvCreditMemoAndBrCode(adDocumentSequenceAssignment.
         * getDsaNextSequence(), EJBCommon.FALSE, AD_BRNCH, AD_CMPNY);
         * adDocumentSequenceAssignment.setDsaNextSequence(EJBCommon.incrementStringNumber(
         * adDocumentSequenceAssignment.getDsaNextSequence()));
         * 
         * } catch (FinderException ex) {
         * 
         * details.setInvNumber(adDocumentSequenceAssignment.getDsaNextSequence());
         * adDocumentSequenceAssignment.setDsaNextSequence(EJBCommon.incrementStringNumber(
         * adDocumentSequenceAssignment.getDsaNextSequence())); break;
         * 
         * }
         * 
         * } else {
         * 
         * try {
         * 
         * arInvoiceHome.findByInvNumberAndInvCreditMemoAndBrCode(adBranchDocumentSequenceAssignment
         * .getBdsNextSequence(), EJBCommon.FALSE, AD_BRNCH, AD_CMPNY);
         * adBranchDocumentSequenceAssignment.setBdsNextSequence(EJBCommon.incrementStringNumber(
         * adBranchDocumentSequenceAssignment.getBdsNextSequence()));
         * 
         * } catch (FinderException ex) {
         * 
         * details.setInvNumber(adBranchDocumentSequenceAssignment.getBdsNextSequence());
         * adBranchDocumentSequenceAssignment.setBdsNextSequence(EJBCommon.incrementStringNumber(
         * adBranchDocumentSequenceAssignment.getBdsNextSequence())); break;
         * 
         * }
         * 
         * }
         * 
         * }
         * 
         * }
         */

        // Confirm if Upload Number have duplicate

        if (details.getInvUploadNumber() != null) {
          if (!details.getInvUploadNumber().trim().equals("")) {
            try {

              arInvoiceHome.findByUploadNumberAndCompanyCode(details.getInvUploadNumber(),
                  AD_CMPNY);


              // throw exception if found duplicate upload number
              throw new ArInvDuplicateUploadNumberException();


            } catch (FinderException ex) {

            }
          }



        }

      } else {

        System.out.println("why not null? : " + details.getInvNumber());


        LocalArInvoice arExistingInvoice = null;

        try {

          arExistingInvoice = arInvoiceHome.findByInvNumberAndInvCreditMemoAndBrCode(
              details.getInvNumber(), EJBCommon.FALSE, AD_BRNCH, AD_CMPNY);

        } catch (FinderException ex) {
        }

        if (arExistingInvoice != null
            && !arExistingInvoice.getInvCode().equals(details.getInvCode())) {

          throw new GlobalDocumentNumberNotUniqueException();

        }

        if (arInvoice.getInvNumber() != details.getInvNumber()
            && (details.getInvNumber() == null || details.getInvNumber().trim().length() == 0)) {

          details.setInvNumber(arInvoice.getInvNumber());

        }

        // Confirm if Upload Number have duplicate

        if (details.getInvUploadNumber() != null) {
          if (!details.getInvUploadNumber().trim().equals("")) {
            if (!details.getInvUploadNumber().equals(arExistingInvoice.getInvUploadNumber())) {
              try {
                arInvoiceHome.findByUploadNumberAndCompanyCode(details.getInvUploadNumber(),
                    AD_CMPNY);


                // throw exception if found duplicate upload number
                throw new ArInvDuplicateUploadNumberException();


              } catch (FinderException ex) {

              }
            }
          }


        }

      }

      // validate if conversion date exists

      try {

        if (details.getInvConversionDate() != null) {


          LocalGlFunctionalCurrency glValidateFunctionalCurrency =
              glFunctionalCurrencyHome.findByFcName(FC_NM, AD_CMPNY);
          LocalGlFunctionalCurrencyRate glFunctionalCurrencyRate = glFunctionalCurrencyRateHome
              .findByFcCodeAndDate(glValidateFunctionalCurrency.getFcCode(),
                  details.getInvConversionDate(), AD_CMPNY);

        }

      } catch (FinderException ex) {

        throw new GlobalConversionDateNotExistException();

      }

      // validate if payment term has at least one payment schedule

      if (adPaymentTermHome.findByPytName(PYT_NM, AD_CMPNY).getAdPaymentSchedules().isEmpty()) {

        throw new GlobalPaymentTermInvalidException();

      }

      // lock sales order

      LocalArJobOrder arExistingJobOrder = null;


      try {



        arExistingJobOrder = arJobOrderHome
            .findByJoDocumentNumberAndBrCode(details.getInvJoNumber(), AD_BRNCH, AD_CMPNY);


        if ((arInvoice == null || (arInvoice != null && arInvoice.getInvJoNumber() != null
            && (!arInvoice.getInvJoNumber().equals(details.getInvJoNumber()))))) {

          if (arExistingJobOrder.getJoLock() == EJBCommon.TRUE) {

            throw new GlobalTransactionAlreadyLockedException();

          }

          arExistingJobOrder.setJoLock(EJBCommon.TRUE);

        }

      } catch (FinderException ex) {

      }

      // used in checking if invoice should re-generate distribution records and re-calculate taxes
      boolean isRecalculate = true;

      // create invoice

      if (details.getInvCode() == null) {
        System.out.println("CHECKING C");

        arInvoice = arInvoiceHome.create(details.getInvType(), EJBCommon.FALSE,
            details.getInvDescription(), details.getInvDate(), details.getInvNumber(),
            details.getInvReferenceNumber(), details.getInvUploadNumber(), null, null, 0d,
            details.getInvDownPayment(), 0d, 0d, 0d, 0d, details.getInvConversionDate(),
            details.getInvConversionRate(), details.getInvMemo(), details.getInvPreviousReading(),
            details.getInvPresentReading(), details.getInvBillToAddress(),
            details.getInvBillToContact(), details.getInvBillToAltContact(),
            details.getInvBillToPhone(), details.getInvBillingHeader(),
            details.getInvBillingFooter(), details.getInvBillingHeader2(),
            details.getInvBillingFooter2(), details.getInvBillingHeader3(),
            details.getInvBillingFooter3(), details.getInvBillingSignatory(),
            details.getInvSignatoryTitle(), details.getInvShipToAddress(),
            details.getInvShipToContact(), details.getInvShipToAltContact(),
            details.getInvShipToPhone(), details.getInvShipDate(), details.getInvLvFreight(), null,
            null, EJBCommon.FALSE, null, EJBCommon.FALSE, EJBCommon.FALSE, EJBCommon.FALSE,
            EJBCommon.FALSE, EJBCommon.FALSE, null, 0d, null, null, null, null,
            details.getInvCreatedBy(), details.getInvDateCreated(), details.getInvLastModifiedBy(),
            details.getInvDateLastModified(), null, null, null, null, EJBCommon.FALSE,
            details.getInvLvShift(), null, details.getInvJoNumber(), details.getInvDebitMemo(),
            details.getInvSubjectToCommission(), details.getInvClientPO(),
            details.getInvEffectivityDate(), AD_BRNCH, AD_CMPNY);

      } else {

        // check if critical fields are changed

        if ((arInvoice.getInvJoNumber() != null
            && !arInvoice.getInvJoNumber().equals(details.getInvJoNumber()))
            || !arInvoice.getArTaxCode().getTcName().equals(TC_NM)
            || !arInvoice.getArWithholdingTaxCode().getWtcName().equals(WTC_NM)
            || !arInvoice.getArCustomer().getCstCustomerCode().equals(CST_CSTMR_CODE)
            || !arInvoice.getAdPaymentTerm().getPytName().equals(PYT_NM)
            || !arInvoice.getInvDate().equals(details.getInvDate())
            || !arInvoice.getInvEffectivityDate().equals(details.getInvEffectivityDate())
            || arInvoice.getInvDownPayment() != details.getInvDownPayment()
            || jolList.size() != arInvoice.getArJobOrderInvoiceLines().size()) {

          isRecalculate = true;

        } else if (jolList.size() == arInvoice.getArJobOrderInvoiceLines().size()) {

          Iterator jolIter = arInvoice.getArJobOrderInvoiceLines().iterator();
          Iterator jolListIter = jolList.iterator();

          while (jolIter.hasNext()) {
            LocalArJobOrderInvoiceLine arJobOrderInvoiceLine =
                (LocalArJobOrderInvoiceLine) jolIter.next();
            LocalArJobOrderLine arJobOrderLine = arJobOrderInvoiceLine.getArJobOrderLine();

            ArModJobOrderLineDetails mdetails = (ArModJobOrderLineDetails) jolListIter.next();

            if (!arJobOrderLine.getInvItemLocation().getInvItem().getIiName()
                .equals(mdetails.getJolIiName())
                || !arJobOrderLine.getInvItemLocation().getInvLocation().getLocName()
                    .equals(mdetails.getJolLocName())
                || !arJobOrderLine.getInvUnitOfMeasure().getUomName()
                    .equals(mdetails.getJolUomName())
                || arJobOrderInvoiceLine.getJilQuantityDelivered() != mdetails
                    .getJolQuantityDelivered()
                || arJobOrderInvoiceLine.getJilTax() != mdetails.getJolTax()
                || arJobOrderLine.getJolUnitPrice() != mdetails.getJolUnitPrice()) {

              isRecalculate = true;
              break;

            }

            // isRecalculate = false;

          }

        } else {

          // isRecalculate = false;

        }

        arInvoice.setInvDownPayment(details.getInvDownPayment());
        arInvoice.setInvDocumentType(details.getInvDocumentType());
        arInvoice.setInvType(details.getInvType());
        arInvoice.setInvDescription(details.getInvDescription());
        arInvoice.setInvDate(details.getInvDate());
        arInvoice.setInvNumber(details.getInvNumber());
        arInvoice.setInvReferenceNumber(details.getInvReferenceNumber());
        arInvoice.setInvMemo(details.getInvMemo());
        arInvoice.setInvUploadNumber(details.getInvUploadNumber());
        arInvoice.setInvConversionDate(details.getInvConversionDate());
        arInvoice.setInvConversionRate(details.getInvConversionRate());
        arInvoice.setInvPreviousReading(arInvoice.getInvPreviousReading());
        arInvoice.setInvPresentReading(arInvoice.getInvPresentReading());
        arInvoice.setInvBillToAddress(details.getInvBillToAddress());
        arInvoice.setInvBillToContact(details.getInvBillToContact());
        arInvoice.setInvBillToAltContact(details.getInvBillToAltContact());
        arInvoice.setInvBillToPhone(details.getInvBillToPhone());
        arInvoice.setInvBillingHeader(details.getInvBillingHeader());
        arInvoice.setInvBillingFooter(details.getInvBillingFooter());
        arInvoice.setInvBillingHeader2(details.getInvBillingHeader2());
        arInvoice.setInvBillingFooter2(details.getInvBillingFooter2());
        arInvoice.setInvBillingSignatory(details.getInvBillingSignatory());
        arInvoice.setInvSignatoryTitle(details.getInvSignatoryTitle());
        arInvoice.setInvShipToAddress(details.getInvShipToAddress());
        arInvoice.setInvShipToContact(details.getInvShipToContact());
        arInvoice.setInvShipToAltContact(details.getInvShipToAltContact());
        arInvoice.setInvShipToPhone(details.getInvShipToPhone());
        arInvoice.setInvShipDate(details.getInvShipDate());
        arInvoice.setInvLvFreight(details.getInvLvFreight());
        arInvoice.setInvLastModifiedBy(details.getInvLastModifiedBy());
        arInvoice.setInvDateLastModified(details.getInvDateLastModified());
        arInvoice.setInvReasonForRejection(null);
        arInvoice.setInvLvShift(details.getInvLvShift());
        arInvoice.setInvJoNumber(details.getInvJoNumber());
        arInvoice.setInvDebitMemo(details.getInvDebitMemo());
        arInvoice.setInvSubjectToCommission(details.getInvSubjectToCommission());
        arInvoice.setInvClientPO(details.getInvClientPO());
        arInvoice.setInvEffectivityDate(details.getInvEffectivityDate());
        System.out.println("CHECK D");

      }
      arInvoice.setInvDocumentType(details.getInvDocumentType());
      arInvoice.setReportParameter(details.getReportParameter());

      LocalAdPaymentTerm adPaymentTerm = adPaymentTermHome.findByPytName(PYT_NM, AD_CMPNY);
      // adPaymentTerm.addArInvoice(arInvoice);
      arInvoice.setAdPaymentTerm(adPaymentTerm);

      LocalGlFunctionalCurrency glFunctionalCurrency =
          glFunctionalCurrencyHome.findByFcName(FC_NM, AD_CMPNY);
      // glFunctionalCurrency.addArInvoice(arInvoice);
      arInvoice.setGlFunctionalCurrency(glFunctionalCurrency);

      LocalArTaxCode arTaxCode = arTaxCodeHome.findByTcName(TC_NM, AD_CMPNY);
      // arTaxCode.addArInvoice(arInvoice);
      arInvoice.setArTaxCode(arTaxCode);

      LocalArWithholdingTaxCode arWithholdingTaxCode =
          arWithholdingTaxCodeHome.findByWtcName(WTC_NM, AD_CMPNY);
      // arWithholdingTaxCode.addArInvoice(arInvoice);
      arInvoice.setArWithholdingTaxCode(arWithholdingTaxCode);

      LocalArCustomer arCustomer = arCustomerHome.findByCstCustomerCode(CST_CSTMR_CODE, AD_CMPNY);
      // arCustomer.addArInvoice(arInvoice);
      arInvoice.setArCustomer(arCustomer);

      LocalArSalesperson arSalesperson = null;

      if (SLP_SLSPRSN_CODE != null && SLP_SLSPRSN_CODE.length() > 0
          && !SLP_SLSPRSN_CODE.equalsIgnoreCase("NO RECORD FOUND")) {

        // if he tagged a salesperson for this invoice
        arSalesperson = arSalespersonHome.findBySlpSalespersonCode(SLP_SLSPRSN_CODE, AD_CMPNY);


        // arSalesperson.addArInvoice(arInvoice);
        arInvoice.setArSalesperson(arSalesperson);

      } else {

        // if he untagged a salesperson for this invoice
        if (arInvoice.getArSalesperson() != null) {

          arSalesperson = arSalespersonHome.findBySlpSalespersonCode(
              arInvoice.getArSalesperson().getSlpSalespersonCode(), AD_CMPNY);
          arSalesperson.dropArInvoice(arInvoice);

        }

        // if no salesperson is set, invoice should not be subject to commission
        arInvoice.setInvSubjectToCommission((byte) 0);

      }

      try {

        LocalArInvoiceBatch arInvoiceBatch =
            arInvoiceBatchHome.findByIbName(IB_NM, AD_BRNCH, AD_CMPNY);
        // arInvoiceBatch.addArInvoice(arInvoice);
        arInvoice.setArInvoiceBatch(arInvoiceBatch);


      } catch (FinderException ex) {

      }

      double amountDue = 0;

      LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);

      if (isRecalculate) {

        // remove all invoice line items

        Collection arInvoiceLineItems = arInvoice.getArInvoiceLineItems();

        Iterator i = arInvoiceLineItems.iterator();

        while (i.hasNext()) {

          LocalArInvoiceLineItem arInvoiceLineItem = (LocalArInvoiceLineItem) i.next();

          if (arInvoiceLineItem.getIliEnableAutoBuild() == 1 && arInvoiceLineItem
              .getInvItemLocation().getInvItem().getIiClass().equals("Assembly")) {

            double QTY_SLD =
                this.convertByUomFromAndItemAndQuantity(arInvoiceLineItem.getInvUnitOfMeasure(),
                    arInvoiceLineItem.getInvItemLocation().getInvItem(),
                    arInvoiceLineItem.getIliQuantity(), AD_CMPNY);

            Collection invBillOfMaterials =
                arInvoiceLineItem.getInvItemLocation().getInvItem().getInvBillOfMaterials();

            Iterator j = invBillOfMaterials.iterator();

            while (j.hasNext()) {

              LocalInvBillOfMaterial invBillOfMaterial = (LocalInvBillOfMaterial) j.next();

              LocalInvItemLocation invItemLocation = invItemLocationHome.findByLocNameAndIiName(
                  invBillOfMaterial.getBomLocName(), invBillOfMaterial.getBomIiName(), AD_CMPNY);

              // bom conversion
              double convertedQuantity = this.convertByUomFromAndItemAndQuantity(
                  invBillOfMaterial.getInvUnitOfMeasure(), invItemLocation.getInvItem(),
                  EJBCommon.roundIt(invBillOfMaterial.getBomQuantityNeeded() * QTY_SLD,
                      this.getInvGpQuantityPrecisionUnit(AD_CMPNY)),
                  AD_CMPNY);

              invItemLocation.setIlCommittedQuantity(
                  invItemLocation.getIlCommittedQuantity() - convertedQuantity);

            }

          } else {

            double convertedQuantity =
                this.convertByUomFromAndItemAndQuantity(arInvoiceLineItem.getInvUnitOfMeasure(),
                    arInvoiceLineItem.getInvItemLocation().getInvItem(),
                    arInvoiceLineItem.getIliQuantity(), AD_CMPNY);
            arInvoiceLineItem.getInvItemLocation().setIlCommittedQuantity(
                arInvoiceLineItem.getInvItemLocation().getIlCommittedQuantity()
                    - convertedQuantity);

          }

          i.remove();

          arInvoiceLineItem.remove();

        }

        // remove all invoice lines

        Collection arInvoiceLines = arInvoice.getArInvoiceLines();

        i = arInvoiceLines.iterator();

        while (i.hasNext()) {

          LocalArInvoiceLine arInvoiceLine = (LocalArInvoiceLine) i.next();

          i.remove();

          arInvoiceLine.remove();

        }

        // remove all job order lines

        Collection arJobOrderInvoiceLines = arInvoice.getArJobOrderInvoiceLines();

        i = arJobOrderInvoiceLines.iterator();

        while (i.hasNext()) {

          LocalArJobOrderInvoiceLine arJobOrderInvoiceLine = (LocalArJobOrderInvoiceLine) i.next();

          // release previous sales order
          if (!arExistingJobOrder.getJoDocumentNumber().equals(
              arJobOrderInvoiceLine.getArJobOrderLine().getArJobOrder().getJoDocumentNumber()))
            arJobOrderInvoiceLine.getArJobOrderLine().getArJobOrder().setJoLock(EJBCommon.FALSE);

          i.remove();

          arJobOrderInvoiceLine.remove();

        }



        // remove all distribution records

        Collection arDistributionRecords = arInvoice.getArDistributionRecords();

        i = arDistributionRecords.iterator();

        while (i.hasNext()) {

          LocalArDistributionRecord arDistributionRecord = (LocalArDistributionRecord) i.next();

          i.remove();

          arDistributionRecord.remove();

        }

        // add new sales order line and distribution record

        double TOTAL_TAX = 0d;
        double TOTAL_LINE = 0d;
        double TOTAL_SA_DEBIT = 0d;
        double TOTAL_DOWN_PAYMENT = arInvoice.getInvDownPayment();

        i = jolList.iterator();

        LocalInvItemLocation invItemLocation = null;

        while (i.hasNext()) {



          ArModJobOrderLineDetails mjolDetails = (ArModJobOrderLineDetails) i.next();

          try {

            invItemLocation = invItemLocationHome.findByLocNameAndIiName(
                mjolDetails.getJolLocName(), mjolDetails.getJolIiName(), AD_CMPNY);

          } catch (FinderException ex) {

            throw new GlobalInvItemLocationNotFoundException(
                String.valueOf(mjolDetails.getJolLine()));

          }

          // start date validation

          if (adPreference.getPrfArAllowPriorDate() == EJBCommon.FALSE) {

            Collection invNegTxnCosting =
                invCostingHome.findNegTxnByGreaterThanCstDateAndIiNameAndLocName(
                    arInvoice.getInvDate(), invItemLocation.getInvItem().getIiName(),
                    invItemLocation.getInvLocation().getLocName(), AD_BRNCH, AD_CMPNY);
            if (!invNegTxnCosting.isEmpty())
              throw new GlobalInventoryDateException(invItemLocation.getInvItem().getIiName());

          }


          LocalArJobOrderInvoiceLine arJobOrderInvoiceLine =
              this.addArJolEntry(mjolDetails, arInvoice, AD_CMPNY);
          LocalArJobOrderLine arJobOrderLine = arJobOrderInvoiceLine.getArJobOrderLine();

          // add cost of sales distribution and inventory

          double COST = invItemLocation.getInvItem().getIiUnitCost();

          try {

            LocalInvCosting invCosting = invCostingHome
                .getByMaxCstDateToLongAndMaxCstLineNumberAndLessThanEqualCstDateAndIiNameAndLocName(
                    arInvoice.getInvDate(), invItemLocation.getInvItem().getIiName(),
                    invItemLocation.getInvLocation().getLocName(), AD_BRNCH, AD_CMPNY);

            // check if rmning vl is not zero and rmng qty is 0
            if (invCosting.getCstRemainingQuantity() <= 0) {

              HashMap criteria = new HashMap();
              criteria.put("itemName", invItemLocation.getInvItem().getIiName());
              criteria.put("location", invItemLocation.getInvLocation().getLocName());

              ArrayList branchList = new ArrayList();

              AdBranchDetails mdetails = new AdBranchDetails();
              mdetails.setBrCode(AD_BRNCH);
              branchList.add(mdetails);

              ejbRIC.executeInvFixItemCosting(criteria, branchList, AD_CMPNY);
              invCosting = invCostingHome
                  .getByMaxCstDateToLongAndMaxCstLineNumberAndLessThanEqualCstDateAndIiNameAndLocName(
                      arInvoice.getInvDate(), invItemLocation.getInvItem().getIiName(),
                      invItemLocation.getInvLocation().getLocName(), AD_BRNCH, AD_CMPNY);
            }



            if (invCosting.getInvItemLocation().getInvItem().getIiCostMethod().equals("Average"))

              COST = invCosting.getCstRemainingQuantity() <= 0 ? COST
                  : Math.abs(
                      invCosting.getCstRemainingValue() / invCosting.getCstRemainingQuantity());


            if (COST <= 0) {
              COST = invItemLocation.getInvItem().getIiUnitCost();
            }

            else if (invCosting.getInvItemLocation().getInvItem().getIiCostMethod().equals("FIFO"))

              COST = invCosting.getCstRemainingQuantity() == 0 ? COST
                  : this.getInvFifoCost(arInvoice.getInvDate(), invItemLocation.getIlCode(),
                      arJobOrderInvoiceLine.getJilQuantityDelivered(),
                      arJobOrderInvoiceLine.getArJobOrderLine().getJolUnitPrice(), false, AD_BRNCH,
                      AD_CMPNY);

            else if (invCosting.getInvItemLocation().getInvItem().getIiCostMethod()
                .equals("Standard"))

              COST = invItemLocation.getInvItem().getIiUnitCost();

          } catch (FinderException ex) {

            COST = arJobOrderLine.getInvItemLocation().getInvItem().getIiUnitCost();

          }

          double QTY_SLD =
              this.convertByUomFromAndItemAndQuantity(arJobOrderLine.getInvUnitOfMeasure(),
                  arJobOrderLine.getInvItemLocation().getInvItem(),
                  arJobOrderInvoiceLine.getJilQuantityDelivered(), AD_CMPNY);

          LocalAdBranchItemLocation adBranchItemLocation = null;

          try {
            lineNumberError = arJobOrderLine.getJolLine();

            System.out.println("item loc in: " + arJobOrderLine.getInvItemLocation().getIlCode());;
            adBranchItemLocation = adBranchItemLocationHome.findBilByIlCodeAndBrCode(
                arJobOrderLine.getInvItemLocation().getIlCode(), AD_BRNCH, AD_CMPNY);

          } catch (FinderException ex) {

          }

          // Branch adding in Job Order
          if (adPreference.getPrfArAutoComputeCogs() == EJBCommon.TRUE && arJobOrderLine
              .getInvItemLocation().getInvItem().getIiNonInventoriable() == EJBCommon.FALSE) {
            lineNumberError = arInvoice.getArDrNextLine();
            if (adBranchItemLocation != null) {

              this.addArDrIliEntry(arInvoice.getArDrNextLine(), "COGS", EJBCommon.TRUE,
                  COST * QTY_SLD, adBranchItemLocation.getBilCoaGlCostOfSalesAccount(), arInvoice,
                  AD_BRNCH, AD_CMPNY);

              this.addArDrIliEntry(arInvoice.getArDrNextLine(), "INVENTORY", EJBCommon.FALSE,
                  COST * QTY_SLD, adBranchItemLocation.getBilCoaGlInventoryAccount(), arInvoice,
                  AD_BRNCH, AD_CMPNY);

            } else {

              this.addArDrIliEntry(arInvoice.getArDrNextLine(), "COGS", EJBCommon.TRUE,
                  COST * QTY_SLD,
                  arJobOrderLine.getInvItemLocation().getIlGlCoaCostOfSalesAccount(), arInvoice,
                  AD_BRNCH, AD_CMPNY);

              this.addArDrIliEntry(arInvoice.getArDrNextLine(), "INVENTORY", EJBCommon.FALSE,
                  COST * QTY_SLD, arJobOrderLine.getInvItemLocation().getIlGlCoaInventoryAccount(),
                  arInvoice, AD_BRNCH, AD_CMPNY);

            }

          }

          // add quantity to item location committed quantity

          double convertedQuantity =
              this.convertByUomFromAndItemAndQuantity(arJobOrderLine.getInvUnitOfMeasure(),
                  arJobOrderLine.getInvItemLocation().getInvItem(),
                  arJobOrderInvoiceLine.getJilQuantityDelivered(), AD_CMPNY);
          invItemLocation
              .setIlCommittedQuantity(invItemLocation.getIlCommittedQuantity() + convertedQuantity);

          // add inventory sale distributions

          double JOL_LINE = this.convertForeignToFunctionalCurrency(
              arInvoice.getGlFunctionalCurrency().getFcCode(),
              arInvoice.getGlFunctionalCurrency().getFcName(), arInvoice.getInvConversionDate(),
              arInvoice.getInvConversionRate(), arJobOrderInvoiceLine.getJilAmount(), AD_CMPNY);


          double JOL_TAX = this.convertForeignToFunctionalCurrency(
              arInvoice.getGlFunctionalCurrency().getFcCode(),
              arInvoice.getGlFunctionalCurrency().getFcName(), arInvoice.getInvConversionDate(),
              arInvoice.getInvConversionRate(), arJobOrderInvoiceLine.getJilTaxAmount(), AD_CMPNY);


          if (adBranchItemLocation != null) {
            // this will trigger by services(DEBIT)
            if (adBranchItemLocation.getInvItemLocation().getInvItem()
                .getIiServices() == EJBCommon.TRUE) {

              this.addArDrIliEntry(arInvoice.getArDrNextLine(), "REVENUE", EJBCommon.TRUE, JOL_LINE,
                  adBranchItemLocation.getBilCoaGlSalesAccount(), arInvoice, AD_BRNCH, AD_CMPNY);

              this.addArDrIliEntry(arInvoice.getArDrNextLine(), "OTHER", EJBCommon.FALSE, JOL_LINE,
                  adBranchItemLocation.getBilCoaGlSalesReturnAccount(), arInvoice, AD_BRNCH,
                  AD_CMPNY);

              TOTAL_SA_DEBIT += arJobOrderInvoiceLine.getJilAmount();
            } else {
              this.addArDrIliEntry(arInvoice.getArDrNextLine(), "REVENUE", EJBCommon.FALSE,
                  JOL_LINE, adBranchItemLocation.getBilCoaGlSalesAccount(), arInvoice, AD_BRNCH,
                  AD_CMPNY);
            }



          } else {

            // this will trigger by services(DEBIT)
            if (arJobOrderLine.getInvItemLocation().getInvItem()
                .getIiServices() == EJBCommon.TRUE) {

              this.addArDrIliEntry(arInvoice.getArDrNextLine(), "REVENUE", EJBCommon.TRUE, JOL_LINE,
                  arJobOrderLine.getInvItemLocation().getIlGlCoaSalesAccount(), arInvoice, AD_BRNCH,
                  AD_CMPNY);

              this.addArDrIliEntry(arInvoice.getArDrNextLine(), "OTHER", EJBCommon.FALSE, JOL_LINE,
                  arJobOrderLine.getInvItemLocation().getIlGlCoaSalesReturnAccount(), arInvoice,
                  AD_BRNCH, AD_CMPNY);


              TOTAL_SA_DEBIT += JOL_LINE;
            } else {

              this.addArDrIliEntry(arInvoice.getArDrNextLine(), "REVENUE", EJBCommon.FALSE,
                  JOL_LINE, arJobOrderLine.getInvItemLocation().getIlGlCoaSalesAccount(), arInvoice,
                  AD_BRNCH, AD_CMPNY);
            }



          }

          TOTAL_LINE += arJobOrderInvoiceLine.getJilAmount();
          TOTAL_TAX += arJobOrderInvoiceLine.getJilTaxAmount();

        }

        // add tax distribution if necessary

        if (!arTaxCode.getTcType().equals("NONE") && !arTaxCode.getTcType().equals("EXEMPT")) {

          double TOTAL_TAX_CONV = this.convertForeignToFunctionalCurrency(
              arInvoice.getGlFunctionalCurrency().getFcCode(),
              arInvoice.getGlFunctionalCurrency().getFcName(), arInvoice.getInvConversionDate(),
              arInvoice.getInvConversionRate(), TOTAL_TAX, AD_CMPNY);

          if (arTaxCode.getTcInterimAccount() == null) {

            // add branch tax code
            LocalAdBranchArTaxCode adBranchTaxCode = null;
            Debug.print("ArInvoiceEntryControllerBean saveArInvIliEntry J");
            try {
              adBranchTaxCode = adBranchArTaxCodeHome.findBtcByTcCodeAndBrCode(
                  arInvoice.getArTaxCode().getTcCode(), AD_BRNCH, AD_CMPNY);

            } catch (FinderException ex) {

            }

            if (adBranchTaxCode != null) {

              this.addArDrEntry(arInvoice.getArDrNextLine(), "TAX", EJBCommon.FALSE, TOTAL_TAX_CONV,
                  adBranchTaxCode.getBtcGlCoaTaxCode(), null, arInvoice, AD_BRNCH, AD_CMPNY);

            } else {

              this.addArDrEntry(arInvoice.getArDrNextLine(), "TAX", EJBCommon.FALSE, TOTAL_TAX_CONV,
                  arTaxCode.getGlChartOfAccount().getCoaCode(), null, arInvoice, AD_BRNCH,
                  AD_CMPNY);
            }

          } else {

            this.addArDrEntry(arInvoice.getArDrNextLine(), "DEFERRED TAX", EJBCommon.FALSE,
                TOTAL_TAX_CONV, arTaxCode.getTcInterimAccount(), null, arInvoice, AD_BRNCH,
                AD_CMPNY);

          }

        }

        // add un earned interest
        double UNEARNED_INT_AMOUNT = 0d;

        System.out.println(
            "adPaymentTerm.getPytEnableInterest()=" + adPaymentTerm.getPytEnableInterest());
        if (arInvoice.getArCustomer().getCstAutoComputeInterest() == EJBCommon.TRUE
            && arInvoice.getArCustomer().getCstMonthlyInterestRate() > 0
            && adPaymentTerm.getPytEnableInterest() == EJBCommon.TRUE) {

          try {

            LocalAdBranchCustomer adBranchCustomer =
                adBranchCustomerHome.findBcstByCstCodeAndBrCode(
                    arInvoice.getArCustomer().getCstCode(), AD_BRNCH, AD_CMPNY);

            System.out.println("adPaymentTerm.getAdPaymentSchedules().size()="
                + adPaymentTerm.getAdPaymentSchedules().size());

            UNEARNED_INT_AMOUNT = EJBCommon.roundIt(
                (TOTAL_LINE + TOTAL_TAX - TOTAL_DOWN_PAYMENT)
                    * adPaymentTerm.getAdPaymentSchedules().size()
                    * (arInvoice.getArCustomer().getCstMonthlyInterestRate() / 100),
                this.getGlFcPrecisionUnit(AD_CMPNY));


            double UNEARNED_INT_AMOUNT_CONV = this.convertForeignToFunctionalCurrency(
                arInvoice.getGlFunctionalCurrency().getFcCode(),
                arInvoice.getGlFunctionalCurrency().getFcName(), arInvoice.getInvConversionDate(),
                arInvoice.getInvConversionRate(), UNEARNED_INT_AMOUNT, AD_CMPNY);


            this.addArDrEntry(arInvoice.getArDrNextLine(), "UNINTEREST", EJBCommon.FALSE,
                UNEARNED_INT_AMOUNT_CONV, adBranchCustomer.getBcstGlCoaUnEarnedInterestAccount(),
                null, arInvoice, AD_BRNCH, AD_CMPNY);

            this.addArDrIliEntry(arInvoice.getArDrNextLine(), "RECEIVABLE INTEREST", EJBCommon.TRUE,
                UNEARNED_INT_AMOUNT_CONV, adBranchCustomer.getBcstGlCoaReceivableAccount(),
                arInvoice, AD_BRNCH, AD_CMPNY);

          } catch (FinderException ex) {

          }


        }
        arInvoice.setInvAmountUnearnedInterest(UNEARNED_INT_AMOUNT);

        // add wtax distribution if necessary

        double W_TAX_AMOUNT = 0d;

        if (arWithholdingTaxCode.getWtcRate() != 0
            && adPreference.getPrfArWTaxRealization().equals("INVOICE")) {

          W_TAX_AMOUNT = EJBCommon.roundIt(TOTAL_LINE * (arWithholdingTaxCode.getWtcRate() / 100),
              this.getGlFcPrecisionUnit(AD_CMPNY));

          double W_TAX_AMOUNT_CONV = this.convertForeignToFunctionalCurrency(
              arInvoice.getGlFunctionalCurrency().getFcCode(),
              arInvoice.getGlFunctionalCurrency().getFcName(), arInvoice.getInvConversionDate(),
              arInvoice.getInvConversionRate(), W_TAX_AMOUNT, AD_CMPNY);

          this.addArDrEntry(arInvoice.getArDrNextLine(), "W-TAX", EJBCommon.TRUE, W_TAX_AMOUNT_CONV,
              arWithholdingTaxCode.getGlChartOfAccount().getCoaCode(), null, arInvoice, AD_BRNCH,
              AD_CMPNY);

        }

        // add payment discount if necessary

        double DISCOUNT_AMT = 0d;

        if (adPaymentTerm.getPytDiscountOnInvoice() == EJBCommon.TRUE) {

          Collection adPaymentSchedules =
              adPaymentScheduleHome.findByPytCode(adPaymentTerm.getPytCode(), AD_CMPNY);
          ArrayList adPaymentScheduleList = new ArrayList(adPaymentSchedules);
          LocalAdPaymentSchedule adPaymentSchedule =
              (LocalAdPaymentSchedule) adPaymentScheduleList.get(0);

          Collection adDiscounts =
              adDiscountHome.findByPsCode(adPaymentSchedule.getPsCode(), AD_CMPNY);
          ArrayList adDiscountList = new ArrayList(adDiscounts);
          LocalAdDiscount adDiscount = (LocalAdDiscount) adDiscountList.get(0);

          double rate = adDiscount.getDscDiscountPercent();
          DISCOUNT_AMT = (TOTAL_LINE + TOTAL_TAX) * (rate / 100d);

          double DISCOUNT_AMT_CONV = this.convertForeignToFunctionalCurrency(
              arInvoice.getGlFunctionalCurrency().getFcCode(),
              arInvoice.getGlFunctionalCurrency().getFcName(), arInvoice.getInvConversionDate(),
              arInvoice.getInvConversionRate(), DISCOUNT_AMT, AD_CMPNY);

          this.addArDrEntry(arInvoice.getArDrNextLine(), "DISCOUNT", EJBCommon.TRUE,
              DISCOUNT_AMT_CONV, adPaymentTerm.getGlChartOfAccount().getCoaCode(), null, arInvoice,
              AD_BRNCH, AD_CMPNY);

        }

        // add receivable distribution

        try {

          LocalAdBranchCustomer adBranchCustomer = adBranchCustomerHome.findBcstByCstCodeAndBrCode(
              arInvoice.getArCustomer().getCstCode(), AD_BRNCH, AD_CMPNY);
          // ADDED " - TOTAL_SA_DEBIT" for Services (DEBIT)

          double AMNT_DUE_CONV = this.convertForeignToFunctionalCurrency(
              arInvoice.getGlFunctionalCurrency().getFcCode(),
              arInvoice.getGlFunctionalCurrency().getFcName(), arInvoice.getInvConversionDate(),
              arInvoice.getInvConversionRate(),
              TOTAL_LINE + TOTAL_TAX - W_TAX_AMOUNT - DISCOUNT_AMT - TOTAL_SA_DEBIT, AD_CMPNY);


          this.addArDrIliEntry(arInvoice.getArDrNextLine(), "RECEIVABLE", EJBCommon.TRUE,
              AMNT_DUE_CONV, adBranchCustomer.getBcstGlCoaReceivableAccount(), arInvoice, AD_BRNCH,
              AD_CMPNY);

        } catch (FinderException ex) {

        }

        // compute invoice amount due

        amountDue = TOTAL_LINE + TOTAL_TAX - W_TAX_AMOUNT - DISCOUNT_AMT + UNEARNED_INT_AMOUNT;

        // set invoice amount due

        arInvoice.setInvAmountDue(amountDue);

        // remove all payment schedule

        Collection arInvoicePaymentSchedules = arInvoice.getArInvoicePaymentSchedules();

        i = arInvoicePaymentSchedules.iterator();

        while (i.hasNext()) {

          LocalArInvoicePaymentSchedule arInvoicePaymentSchedule =
              (LocalArInvoicePaymentSchedule) i.next();

          i.remove();

          arInvoicePaymentSchedule.remove();

        }

        // create invoice payment schedule

        short precisionUnit = this.getGlFcPrecisionUnit(AD_CMPNY);
        double TOTAL_PAYMENT_SCHEDULE = 0d;
        double TOTAL_PAYMENT_INT = 0d;
        double TOTAL_PAYMENT_SCHEDULE_INT = TOTAL_LINE + TOTAL_TAX - TOTAL_DOWN_PAYMENT;

        GregorianCalendar gcPrevDateDue = new GregorianCalendar();
        GregorianCalendar gcDateDue = new GregorianCalendar();
        gcPrevDateDue.setTime(arInvoice.getInvEffectivityDate());

        Collection adPaymentSchedules = adPaymentTerm.getAdPaymentSchedules();

        boolean first = true;

        i = adPaymentSchedules.iterator();

        while (i.hasNext()) {

          LocalAdPaymentSchedule adPaymentSchedule = (LocalAdPaymentSchedule) i.next();

          // get date due

          if (arInvoice.getAdPaymentTerm().getPytScheduleBasis().equals("DEFAULT")) {

            gcDateDue.setTime(arInvoice.getInvEffectivityDate());
            gcDateDue.add(Calendar.DATE, adPaymentSchedule.getPsDueDay());

          } else if (arInvoice.getAdPaymentTerm().getPytScheduleBasis().equals("MONTHLY")) {

            gcDateDue = gcPrevDateDue;
            gcDateDue.add(Calendar.MONTH, 1);
            gcPrevDateDue = gcDateDue;

          } else if (arInvoice.getAdPaymentTerm().getPytScheduleBasis().equals("BI-MONTHLY")) {

            gcDateDue = gcPrevDateDue;

            if (gcPrevDateDue.get(Calendar.MONTH) != 1) {
              if (gcPrevDateDue.getActualMaximum(Calendar.DATE) == 31
                  && gcPrevDateDue.get(Calendar.DATE) > 15
                  && gcPrevDateDue.get(Calendar.DATE) < 31) {
                gcDateDue.add(Calendar.DATE, 16);
              } else {
                gcDateDue.add(Calendar.DATE, 15);
              }
            } else if (gcPrevDateDue.get(Calendar.MONTH) == 1) {
              if (gcPrevDateDue.getActualMaximum(Calendar.DATE) == 28
                  && gcPrevDateDue.get(Calendar.DATE) == 14) {
                gcDateDue.add(Calendar.DATE, 14);
              } else if (gcPrevDateDue.getActualMaximum(Calendar.DATE) == 28
                  && gcPrevDateDue.get(Calendar.DATE) >= 15
                  && gcPrevDateDue.get(Calendar.DATE) < 28) {
                gcDateDue.add(Calendar.DATE, 13);
              } else if (gcPrevDateDue.getActualMaximum(Calendar.DATE) == 29
                  && gcPrevDateDue.get(Calendar.DATE) >= 15
                  && gcPrevDateDue.get(Calendar.DATE) < 29) {
                gcDateDue.add(Calendar.DATE, 14);
              } else {
                gcDateDue.add(Calendar.DATE, 15);
              }
            }

            gcPrevDateDue = gcDateDue;

          }

          if (first == true && arInvoice.getInvDownPayment() > 0) {

            LocalArInvoicePaymentSchedule arInvoicePaymentScheduleDownPayment =
                arInvoicePaymentScheduleHome.create(arInvoice.getInvEffectivityDate(), (short) 0,
                    arInvoice.getInvDownPayment(), 0d, EJBCommon.FALSE, (short) 0,
                    arInvoice.getInvEffectivityDate(), 0d, 0d, AD_CMPNY);

            arInvoice.addArInvoicePaymentSchedule(arInvoicePaymentScheduleDownPayment);
            first = false;
          }

          // create a payment schedule


          double PAYMENT_SCHEDULE_AMOUNT = 0;
          double PAYMENT_SCHEDULE_INT = 0;
          double PAYMENT_SCHEDULE_PRINCIPAL = 0;


          System.out.println("adPaymentTerm.getPytMonthlyInterestRate() ="
              + adPaymentTerm.getPytMonthlyInterestRate());

          // if last payment schedule subtract to avoid rounding difference error

          if (i.hasNext()) {

            PAYMENT_SCHEDULE_AMOUNT =
                EJBCommon.roundIt(
                    (adPaymentSchedule.getPsRelativeAmount() / adPaymentTerm.getPytBaseAmount())
                        * (arInvoice.getInvAmountDue() - arInvoice.getInvDownPayment()),
                    precisionUnit);
            System.out
                .println("------------>TOTAL_PAYMENT_SCHEDULE_INT=" + TOTAL_PAYMENT_SCHEDULE_INT);


            PAYMENT_SCHEDULE_INT = EJBCommon.roundIt(
                (TOTAL_PAYMENT_SCHEDULE_INT) * (adPaymentTerm.getPytMonthlyInterestRate() / 100),
                precisionUnit);

            PAYMENT_SCHEDULE_PRINCIPAL = -(PAYMENT_SCHEDULE_AMOUNT) + PAYMENT_SCHEDULE_INT;


          } else {

            System.out
                .println("------------>TOTAL_PAYMENT_SCHEDULE_INT=" + TOTAL_PAYMENT_SCHEDULE_INT);


            PAYMENT_SCHEDULE_AMOUNT = (arInvoice.getInvAmountDue() - arInvoice.getInvDownPayment())
                - TOTAL_PAYMENT_SCHEDULE;

            PAYMENT_SCHEDULE_INT = EJBCommon.roundIt(
                (TOTAL_PAYMENT_SCHEDULE_INT) * (adPaymentTerm.getPytMonthlyInterestRate() / 100),
                precisionUnit);

            PAYMENT_SCHEDULE_PRINCIPAL = -(PAYMENT_SCHEDULE_AMOUNT) + PAYMENT_SCHEDULE_INT;


          }

          LocalArInvoicePaymentSchedule arInvoicePaymentSchedule =
              arInvoicePaymentScheduleHome.create(gcDateDue.getTime(),
                  adPaymentSchedule.getPsLineNumber(), PAYMENT_SCHEDULE_AMOUNT, 0d, EJBCommon.FALSE,
                  (short) 0, gcDateDue.getTime(), 0d, 0d, AD_CMPNY);


          arInvoicePaymentSchedule.setIpsInterestDue(PAYMENT_SCHEDULE_INT);
          System.out.println("PAYMENT_SCHEDULE_INT=" + PAYMENT_SCHEDULE_INT);
          arInvoice.addArInvoicePaymentSchedule(arInvoicePaymentSchedule);

          TOTAL_PAYMENT_SCHEDULE += PAYMENT_SCHEDULE_AMOUNT;
          TOTAL_PAYMENT_INT += PAYMENT_SCHEDULE_INT;
          TOTAL_PAYMENT_SCHEDULE_INT += PAYMENT_SCHEDULE_PRINCIPAL;

        }

      }

      // generate approval status

      String INV_APPRVL_STATUS = null;

      if (!isDraft) {

        LocalAdApproval adApproval = adApprovalHome.findByAprAdCompany(AD_CMPNY);

        // validate if total amount + unposted invoices' amount + current balance + unposted
        // receipts's amount
        // does not exceed customer's credit limit

        double balance = 0;
        LocalAdApprovalDocument adInvoiceApprovalDocument =
            adApprovalDocumentHome.findByAdcType("AR INVOICE", AD_CMPNY);

        if (arCustomer.getCstCreditLimit() > 0) {

          balance = computeTotalBalance(details.getInvCode(), CST_CSTMR_CODE, AD_CMPNY);

          balance += amountDue;

          if (arCustomer.getCstCreditLimit() < balance && (adApproval
              .getAprEnableArInvoice() == EJBCommon.FALSE
              || (adApproval.getAprEnableArInvoice() == EJBCommon.TRUE && adInvoiceApprovalDocument
                  .getAdcEnableCreditLimitChecking() == EJBCommon.FALSE))) {

            throw new ArINVAmountExceedsCreditLimitException();

          }

        }

        // find overdue invoices
        Collection arOverdueInvoices =
            arInvoicePaymentScheduleHome.findOverdueIpsByInvDateAndCstCustomerCode(
                arInvoice.getInvDate(), CST_CSTMR_CODE, AD_CMPNY);


        // check if ar invoice approval is enabled

        if (adApproval.getAprEnableArInvoice() == EJBCommon.FALSE
            || (arCustomer.getCstCreditLimit() > balance && arOverdueInvoices.size() == 0)) {

          INV_APPRVL_STATUS = "N/A";

        } else {

          // check if invoice is self approved

          LocalAdAmountLimit adAmountLimit = null;

          try {

            adAmountLimit = adAmountLimitHome.findByAdcTypeAndAuTypeAndUsrName("AR INVOICE",
                "REQUESTER", details.getInvLastModifiedBy(), AD_CMPNY);

          } catch (FinderException ex) {

            throw new GlobalNoApprovalRequesterFoundException();

          }


          if (arInvoice.getInvAmountDue() <= adAmountLimit.getCalAmountLimit()
              && (arCustomer.getCstCreditLimit() == 0
                  || arCustomer.getCstCreditLimit() > balance)) {

            INV_APPRVL_STATUS = "N/A";

          } else {

            // for approval, create approval queue

            Collection adAmountLimits = adAmountLimitHome.findByAdcTypeAndGreaterThanCalAmountLimit(
                "AR INVOICE", adAmountLimit.getCalAmountLimit(), AD_CMPNY);

            if (adAmountLimits.isEmpty()) {

              Collection adApprovalUsers = adApprovalUserHome.findByAuTypeAndCalCode("APPROVER",
                  adAmountLimit.getCalCode(), AD_CMPNY);

              if (adApprovalUsers.isEmpty()) {

                throw new GlobalNoApprovalApproverFoundException();

              }

              Iterator j = adApprovalUsers.iterator();

              while (j.hasNext()) {

                LocalAdApprovalUser adApprovalUser = (LocalAdApprovalUser) j.next();

                LocalAdApprovalQueue adApprovalQueue =
                    adApprovalQueueHome.create(EJBCommon.TRUE, "AR INVOICE", arInvoice.getInvCode(),
                        arInvoice.getInvNumber(), arInvoice.getInvDate(),
                        adAmountLimit.getCalAndOr(), adApprovalUser.getAuOr(), AD_BRNCH, AD_CMPNY);

                adApprovalUser.getAdUser().addAdApprovalQueue(adApprovalQueue);

              }

            } else {

              boolean isApprovalUsersFound = false;

              Iterator i = adAmountLimits.iterator();

              while (i.hasNext()) {

                LocalAdAmountLimit adNextAmountLimit = (LocalAdAmountLimit) i.next();

                if (arInvoice.getInvAmountDue() <= adNextAmountLimit.getCalAmountLimit()) {

                  Collection adApprovalUsers = adApprovalUserHome.findByAuTypeAndCalCode("APPROVER",
                      adAmountLimit.getCalCode(), AD_CMPNY);

                  Iterator j = adApprovalUsers.iterator();

                  while (j.hasNext()) {

                    isApprovalUsersFound = true;

                    LocalAdApprovalUser adApprovalUser = (LocalAdApprovalUser) j.next();

                    LocalAdApprovalQueue adApprovalQueue = adApprovalQueueHome.create(
                        EJBCommon.TRUE, "AR INVOICE", arInvoice.getInvCode(),
                        arInvoice.getInvNumber(), arInvoice.getInvDate(),
                        adAmountLimit.getCalAndOr(), adApprovalUser.getAuOr(), AD_BRNCH, AD_CMPNY);

                    adApprovalUser.getAdUser().addAdApprovalQueue(adApprovalQueue);

                  }

                  break;

                } else if (!i.hasNext()) {

                  Collection adApprovalUsers = adApprovalUserHome.findByAuTypeAndCalCode("APPROVER",
                      adNextAmountLimit.getCalCode(), AD_CMPNY);

                  Iterator j = adApprovalUsers.iterator();

                  while (j.hasNext()) {

                    isApprovalUsersFound = true;

                    LocalAdApprovalUser adApprovalUser = (LocalAdApprovalUser) j.next();

                    LocalAdApprovalQueue adApprovalQueue =
                        adApprovalQueueHome.create(EJBCommon.TRUE, "AR INVOICE",
                            arInvoice.getInvCode(), arInvoice.getInvNumber(),
                            arInvoice.getInvDate(), adNextAmountLimit.getCalAndOr(),
                            adApprovalUser.getAuOr(), AD_BRNCH, AD_CMPNY);

                    adApprovalUser.getAdUser().addAdApprovalQueue(adApprovalQueue);

                  }

                  break;

                }

                adAmountLimit = adNextAmountLimit;

              }

              if (!isApprovalUsersFound) {

                throw new GlobalNoApprovalApproverFoundException();

              }

            }

            INV_APPRVL_STATUS = "PENDING";
          }
        }

      }

      if (INV_APPRVL_STATUS != null && INV_APPRVL_STATUS.equals("N/A")
          && adPreference.getPrfArGlPostingType().equals("AUTO-POST UPON APPROVAL")) {

        this.executeArInvPost(arInvoice.getInvCode(), arInvoice.getInvLastModifiedBy(), AD_BRNCH,
            AD_CMPNY);

        // Set SO Lock if Fully Served

        Iterator jolIter = arExistingJobOrder.getArJobOrderLines().iterator();

        boolean isOpenSO = false;

        while (jolIter.hasNext()) {

          LocalArJobOrderLine arJobOrderLine = (LocalArJobOrderLine) jolIter.next();

          Iterator soInvLnIter = arJobOrderLine.getArJobOrderInvoiceLines().iterator();
          double QUANTITY_SOLD = 0d;

          while (soInvLnIter.hasNext()) {

            LocalArJobOrderInvoiceLine arJobOrderInvoiceLine =
                (LocalArJobOrderInvoiceLine) soInvLnIter.next();

            if (arJobOrderInvoiceLine.getArInvoice().getInvPosted() == EJBCommon.TRUE) {

              QUANTITY_SOLD += arJobOrderInvoiceLine.getJilQuantityDelivered();

            }

          }

          double TOTAL_REMAINING_QTY = arJobOrderLine.getJolQuantity() - QUANTITY_SOLD;

          if (TOTAL_REMAINING_QTY > 0) {
            isOpenSO = true;
            break;
          }

        }

        if (isOpenSO)
          arExistingJobOrder.setJoLock(EJBCommon.FALSE);
        else
          arExistingJobOrder.setJoLock(EJBCommon.TRUE);



      }

      // set invoice approval status

      arInvoice.setInvApprovalStatus(INV_APPRVL_STATUS);

      return arInvoice.getInvCode();

    } catch (GlobalRecordAlreadyDeletedException ex) {

      getSessionContext().setRollbackOnly();
      throw ex;

    } catch (GlobalDocumentNumberNotUniqueException ex) {

      getSessionContext().setRollbackOnly();
      throw ex;

    } catch (GlobalConversionDateNotExistException ex) {

      getSessionContext().setRollbackOnly();
      throw ex;

    } catch (GlobalPaymentTermInvalidException ex) {

      getSessionContext().setRollbackOnly();
      throw ex;

    } catch (GlobalTransactionAlreadyApprovedException ex) {

      getSessionContext().setRollbackOnly();
      throw ex;

    } catch (GlobalTransactionAlreadyPendingException ex) {

      getSessionContext().setRollbackOnly();
      throw ex;

    } catch (GlobalTransactionAlreadyPostedException ex) {

      getSessionContext().setRollbackOnly();
      throw ex;

    } catch (GlobalTransactionAlreadyVoidException ex) {

      getSessionContext().setRollbackOnly();
      throw ex;

    } catch (GlobalNoApprovalRequesterFoundException ex) {

      getSessionContext().setRollbackOnly();
      throw ex;

    } catch (GlobalNoApprovalApproverFoundException ex) {

      getSessionContext().setRollbackOnly();
      throw ex;

    } catch (GlobalInvItemLocationNotFoundException ex) {

      getSessionContext().setRollbackOnly();
      throw ex;

    } catch (GlJREffectiveDateNoPeriodExistException ex) {

      getSessionContext().setRollbackOnly();
      throw ex;

    } catch (GlJREffectiveDatePeriodClosedException ex) {

      getSessionContext().setRollbackOnly();
      throw ex;

    } catch (GlobalJournalNotBalanceException ex) {

      getSessionContext().setRollbackOnly();
      throw ex;

    } catch (GlobalTransactionAlreadyLockedException ex) {

      getSessionContext().setRollbackOnly();
      throw ex;

    } catch (GlobalInventoryDateException ex) {

      getSessionContext().setRollbackOnly();
      throw ex;

    } catch (GlobalBranchAccountNumberInvalidException ex) {

      // Retrive Error a Line Number
      ex.setLineNumberError(lineNumberError);
      getSessionContext().setRollbackOnly();
      throw ex;

    } catch (ArINVAmountExceedsCreditLimitException ex) {

      getSessionContext().setRollbackOnly();
      throw ex;

    } catch (AdPRFCoaGlVarianceAccountNotFoundException ex) {

      getSessionContext().setRollbackOnly();
      throw ex;

    } catch (GlobalExpiryDateNotFoundException ex) {

      getSessionContext().setRollbackOnly();
      throw ex;

    } catch (ArInvDuplicateUploadNumberException ex) {

      getSessionContext().setRollbackOnly();
      throw ex;

    } catch (Exception ex) {

      Debug.printStackTrace(ex);
      getSessionContext().setRollbackOnly();
      throw new EJBException(ex.getMessage());

    }

  }



  /**
   * @ejb:interface-method view-type="remote"
   **/
  public void deleteArInvEntry(
      Integer INV_CODE, String AD_USR, Integer AD_CMPNY
  ) throws GlobalRecordAlreadyDeletedException {

    Debug.print("ArInvoiceEntryControllerBean deleteArInvEntry");

    LocalArInvoiceHome arInvoiceHome = null;
    LocalAdApprovalQueueHome adApprovalQueueHome = null;
    LocalAdPreferenceHome adPreferenceHome = null;
    LocalInvItemLocationHome invItemLocationHome = null;
    LocalAdDeleteAuditTrailHome adDeleteAuditTrailHome = null;

    // Initialize EJB Home

    try {

      arInvoiceHome = (LocalArInvoiceHome) EJBHomeFactory
          .lookUpLocalHome(LocalArInvoiceHome.JNDI_NAME, LocalArInvoiceHome.class);
      adApprovalQueueHome = (LocalAdApprovalQueueHome) EJBHomeFactory
          .lookUpLocalHome(LocalAdApprovalQueueHome.JNDI_NAME, LocalAdApprovalQueueHome.class);
      adPreferenceHome = (LocalAdPreferenceHome) EJBHomeFactory
          .lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);
      invItemLocationHome = (LocalInvItemLocationHome) EJBHomeFactory
          .lookUpLocalHome(LocalInvItemLocationHome.JNDI_NAME, LocalInvItemLocationHome.class);
      adDeleteAuditTrailHome = (LocalAdDeleteAuditTrailHome) EJBHomeFactory.lookUpLocalHome(
          LocalAdDeleteAuditTrailHome.JNDI_NAME, LocalAdDeleteAuditTrailHome.class);


    } catch (NamingException ex) {

      throw new EJBException(ex.getMessage());

    }

    try {

      LocalArInvoice arInvoice = arInvoiceHome.findByPrimaryKey(INV_CODE);
      LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);

      Collection arInvoiceLineItems = arInvoice.getArInvoiceLineItems();

      Iterator j = arInvoiceLineItems.iterator();

      while (j.hasNext()) {

        LocalArInvoiceLineItem arInvoiceLineItem = (LocalArInvoiceLineItem) j.next();

        double convertedQuantity =
            this.convertByUomFromAndItemAndQuantity(arInvoiceLineItem.getInvUnitOfMeasure(),
                arInvoiceLineItem.getInvItemLocation().getInvItem(),
                arInvoiceLineItem.getIliQuantity(), AD_CMPNY);
        arInvoiceLineItem.getInvItemLocation().setIlCommittedQuantity(
            arInvoiceLineItem.getInvItemLocation().getIlCommittedQuantity() - convertedQuantity);

      }

      if (arInvoice.getInvApprovalStatus() != null
          && arInvoice.getInvApprovalStatus().equals("PENDING")) {

        Collection adApprovalQueues = adApprovalQueueHome
            .findByAqDocumentAndAqDocumentCode("AR INVOICE", arInvoice.getInvCode(), AD_CMPNY);

        Iterator i = adApprovalQueues.iterator();

        while (i.hasNext()) {

          LocalAdApprovalQueue adApprovalQueue = (LocalAdApprovalQueue) i.next();

          adApprovalQueue.remove();

        }

      }

      if (!arInvoice.getArSalesOrderInvoiceLines().isEmpty()) {

        Iterator iterator = arInvoice.getArSalesOrderInvoiceLines().iterator();

        LocalArSalesOrderInvoiceLine arSalesOrderInvoiceLine =
            (LocalArSalesOrderInvoiceLine) iterator.next();
        LocalArSalesOrder arSalesOrder =
            arSalesOrderInvoiceLine.getArSalesOrderLine().getArSalesOrder();
        arSalesOrderInvoiceLine.getArSalesOrderLine().getArSalesOrder().setSoLock(EJBCommon.FALSE);
        arSalesOrder.setSoApprovalStatus(null);
        arSalesOrder.setSoPosted(EJBCommon.FALSE);
        arSalesOrder.setSoPostedBy(null);
        arSalesOrder.setSoDatePosted(null);
      }


      if (!arInvoice.getArJobOrderInvoiceLines().isEmpty()) {

        Iterator iterator = arInvoice.getArJobOrderInvoiceLines().iterator();

        LocalArJobOrderInvoiceLine arJobOrderInvoiceLine =
            (LocalArJobOrderInvoiceLine) iterator.next();

        LocalArJobOrder arJobOrder = arJobOrderInvoiceLine.getArJobOrderLine().getArJobOrder();

        arJobOrderInvoiceLine.getArJobOrderLine().getArJobOrder().setJoLock(EJBCommon.FALSE);
        arJobOrder.setJoApprovalStatus(null);
        arJobOrder.setJoPosted(EJBCommon.FALSE);
        arJobOrder.setJoPostedBy(null);
        arJobOrder.setJoDatePosted(null);
      }


      adDeleteAuditTrailHome.create("AR INVOICE", arInvoice.getInvDate(), arInvoice.getInvNumber(),
          arInvoice.getInvReferenceNumber(), arInvoice.getInvAmountDue(), AD_USR, new Date(),
          AD_CMPNY);

      arInvoice.remove();

    } catch (FinderException ex) {

      getSessionContext().setRollbackOnly();
      throw new GlobalRecordAlreadyDeletedException();

    } catch (Exception ex) {

      Debug.printStackTrace(ex);
      getSessionContext().setRollbackOnly();
      throw new EJBException(ex.getMessage());

    }

  }

  /**
   * @ejb:interface-method view-type="remote"
   * @jboss:method-attributes read-only="true"
   **/
  public short getGlFcPrecisionUnit(
      Integer AD_CMPNY
  ) {

    Debug.print("ArInvoiceEntryControllerBean getGlFcPrecisionUnit");


    LocalAdCompanyHome adCompanyHome = null;


    // Initialize EJB Home

    try {

      adCompanyHome = (LocalAdCompanyHome) EJBHomeFactory
          .lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);

    } catch (NamingException ex) {

      throw new EJBException(ex.getMessage());

    }

    try {

      LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);

      return adCompany.getGlFunctionalCurrency().getFcPrecision();

    } catch (Exception ex) {

      Debug.printStackTrace(ex);
      throw new EJBException(ex.getMessage());

    }

  }



  /**
   * @ejb:interface-method view-type="remote"
   * @jboss:method-attributes read-only="true"
   **/
  public void recalculateCostOfSalesEntries(
  ) {

    Debug.print("ArInvoiceEntryControllerBean recalculateCostOfSalesEntries");


    LocalArInvoiceHome arInvoiceHome = null;
    LocalArReceiptHome arReceiptHome = null;
    LocalArDistributionRecordHome arDistributionRecordHome = null;
    LocalGlJournalHome glJournalHome = null;
    LocalGlJournalLineHome glJournalLineHome = null;
    LocalInvCostingHome invCostingHome = null;
    LocalAdPreferenceHome adPreferenceHome = null;
    LocalInvItemLocationHome invItemLocationHome = null;
    LocalAdBranchItemLocationHome adBranchItemLocationHome = null;
    InvRepItemCostingControllerHome homeRIC = null;
    InvRepItemCostingController ejbRIC = null;


    ArrayList list = new ArrayList();


    // Initialize EJB Home

    try {

      arInvoiceHome = (LocalArInvoiceHome) EJBHomeFactory
          .lookUpLocalHome(LocalArInvoiceHome.JNDI_NAME, LocalArInvoiceHome.class);
      arReceiptHome = (LocalArReceiptHome) EJBHomeFactory
          .lookUpLocalHome(LocalArReceiptHome.JNDI_NAME, LocalArReceiptHome.class);
      arDistributionRecordHome = (LocalArDistributionRecordHome) EJBHomeFactory.lookUpLocalHome(
          LocalArDistributionRecordHome.JNDI_NAME, LocalArDistributionRecordHome.class);
      glJournalHome = (LocalGlJournalHome) EJBHomeFactory
          .lookUpLocalHome(LocalGlJournalHome.JNDI_NAME, LocalGlJournalHome.class);
      glJournalLineHome = (LocalGlJournalLineHome) EJBHomeFactory
          .lookUpLocalHome(LocalGlJournalLineHome.JNDI_NAME, LocalGlJournalLineHome.class);
      invCostingHome = (LocalInvCostingHome) EJBHomeFactory
          .lookUpLocalHome(LocalInvCostingHome.JNDI_NAME, LocalInvCostingHome.class);
      adPreferenceHome = (LocalAdPreferenceHome) EJBHomeFactory
          .lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);
      invItemLocationHome = (LocalInvItemLocationHome) EJBHomeFactory
          .lookUpLocalHome(LocalInvItemLocationHome.JNDI_NAME, LocalInvItemLocationHome.class);
      adBranchItemLocationHome = (LocalAdBranchItemLocationHome) EJBHomeFactory.lookUpLocalHome(
          LocalAdBranchItemLocationHome.JNDI_NAME, LocalAdBranchItemLocationHome.class);
      homeRIC = (InvRepItemCostingControllerHome) com.util.EJBHomeFactory
          .lookUpHome("ejb/InvRepItemCostingControllerEJB", InvRepItemCostingControllerHome.class);


    } catch (NamingException ex) {

      throw new EJBException(ex.getMessage());

    }


    try {
      ejbRIC = homeRIC.create();
    } catch (RemoteException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    } catch (CreateException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }

    Object obj[] = obj = new Object[0];
    StringBuffer jbossQl = new StringBuffer();
    jbossQl.append("SELECT OBJECT(inv) FROM ArInvoice inv");

    try {

      LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(1);



      obj = new Object[0];
      jbossQl = new StringBuffer();
      jbossQl.append(
          "SELECT OBJECT(rct) FROM ArReceipt rct WHERE rct.rctType ='MISC' AND rct.rctVoid = 0");
      Collection arMiscReceipts = arReceiptHome.getRctByCriteria(jbossQl.toString(), obj);

      Iterator i = arMiscReceipts.iterator();


      while (i.hasNext()) {

        LocalArReceipt arMiscReceipt = (LocalArReceipt) i.next();

        Integer AD_CMPNY = arMiscReceipt.getRctAdCompany();
        Integer AD_BRNCH = arMiscReceipt.getRctAdBranch();

        Collection arDistributionRecords = arMiscReceipt.getArDistributionRecords();

        Iterator iDr = arDistributionRecords.iterator();


        while (iDr.hasNext()) {

          LocalArDistributionRecord arDistributionRecord = (LocalArDistributionRecord) iDr.next();


          if (arDistributionRecord.getDrClass().equals("INVENTORY")
              || arDistributionRecord.getDrClass().equals("COGS")) {

            iDr.remove();
            arDistributionRecord.remove();
          }

        }
        Collection arInvoiceLineItems = arMiscReceipt.getArInvoiceLineItems();



        Iterator iIli = arInvoiceLineItems.iterator();


        while (iIli.hasNext()) {

          LocalArInvoiceLineItem arInvoiceLineItem = (LocalArInvoiceLineItem) iIli.next();

          LocalInvItemLocation invItemLocation = arInvoiceLineItem.getInvItemLocation();


          double COST = invItemLocation.getInvItem().getIiUnitCost();
          try {
            LocalInvCosting invCosting = invCostingHome
                .getByMaxCstDateToLongAndMaxCstLineNumberAndLessThanEqualCstDateAndIiNameAndLocName(
                    arMiscReceipt.getRctDate(), invItemLocation.getInvItem().getIiName(),
                    invItemLocation.getInvLocation().getLocName(), AD_BRNCH, AD_CMPNY);

            // check if rmning vl is not zero and rmng qty is 0
            System.out.println("COGS--------------------------------------->");
            System.out.println(
                "invCosting.getCstRemainingQuantity()=" + invCosting.getCstRemainingQuantity());
            System.out
                .println("invCosting.getCstRemainingValue()=" + invCosting.getCstRemainingValue());

            if (invCosting.getCstRemainingQuantity() <= 0
                && invCosting.getCstRemainingValue() <= 0) {
              System.out.println("RE CALC");
              HashMap criteria = new HashMap();
              criteria.put("itemName", invItemLocation.getInvItem().getIiName());
              criteria.put("location", invItemLocation.getInvLocation().getLocName());

              ArrayList branchList = new ArrayList();

              AdBranchDetails mdetails = new AdBranchDetails();
              mdetails.setBrCode(AD_BRNCH);
              branchList.add(mdetails);

              ejbRIC.executeInvFixItemCosting(criteria, branchList, AD_CMPNY);

              invCosting = invCostingHome
                  .getByMaxCstDateToLongAndMaxCstLineNumberAndLessThanEqualCstDateAndIiNameAndLocName(
                      arMiscReceipt.getRctDate(), invItemLocation.getInvItem().getIiName(),
                      invItemLocation.getInvLocation().getLocName(), AD_BRNCH, AD_CMPNY);
            }

            if (invCosting.getInvItemLocation().getInvItem().getIiCostMethod().equals("Average")) {

              COST = invCosting.getCstRemainingQuantity() <= 0
                  ? invCosting.getInvItemLocation().getInvItem().getIiUnitCost()
                  : Math.abs(
                      invCosting.getCstRemainingValue() / invCosting.getCstRemainingQuantity());

              if (COST <= 0) {
                COST = arInvoiceLineItem.getInvItemLocation().getInvItem().getIiUnitCost();
              }
            }


            else if (invCosting.getInvItemLocation().getInvItem().getIiCostMethod()
                .equals("FIFO")) {

              COST = Math.abs(this.getInvFifoCost(invCosting.getCstDate(),
                  invCosting.getInvItemLocation().getIlCode(), arInvoiceLineItem.getIliQuantity(),
                  arInvoiceLineItem.getIliUnitPrice(), false, AD_BRNCH, AD_CMPNY));

            } else if (invCosting.getInvItemLocation().getInvItem().getIiCostMethod()
                .equals("Standard")) {

              COST = invCosting.getInvItemLocation().getInvItem().getIiUnitCost();

            }
          } catch (FinderException ex) {

            COST = arInvoiceLineItem.getInvItemLocation().getInvItem().getIiUnitCost();

          }


          double QTY_SLD =
              this.convertByUomFromAndItemAndQuantity(arInvoiceLineItem.getInvUnitOfMeasure(),
                  arInvoiceLineItem.getInvItemLocation().getInvItem(),
                  arInvoiceLineItem.getIliQuantity(), AD_CMPNY);
          Debug.print("ArMiscReceiptEntryControllerBean saveArRctIliEntry  I02");
          LocalAdBranchItemLocation adBranchItemLocation = null;

          try {

            adBranchItemLocation = adBranchItemLocationHome.findBilByIlCodeAndBrCode(
                arInvoiceLineItem.getInvItemLocation().getIlCode(), AD_BRNCH, AD_CMPNY);

          } catch (FinderException ex) {

          }

          if ((arInvoiceLineItem.getIliEnableAutoBuild() == 0
              || arInvoiceLineItem.getInvItemLocation().getInvItem().getIiClass().equals("Stock"))
              && arInvoiceLineItem.getInvItemLocation().getInvItem()
                  .getIiNonInventoriable() == EJBCommon.FALSE) {

            if (adBranchItemLocation != null) {

              this.addArDrIliEntry(arMiscReceipt.getArDrNextLine(), "COGS", EJBCommon.TRUE,
                  COST * QTY_SLD, adBranchItemLocation.getBilCoaGlCostOfSalesAccount(),
                  arMiscReceipt, AD_BRNCH, AD_CMPNY);

              this.addArDrIliEntry(arMiscReceipt.getArDrNextLine(), "INVENTORY", EJBCommon.FALSE,
                  COST * QTY_SLD, adBranchItemLocation.getBilCoaGlInventoryAccount(), arMiscReceipt,
                  AD_BRNCH, AD_CMPNY);

            } else {

              this.addArDrIliEntry(arMiscReceipt.getArDrNextLine(), "COGS", EJBCommon.TRUE,
                  COST * QTY_SLD,
                  arInvoiceLineItem.getInvItemLocation().getIlGlCoaCostOfSalesAccount(),
                  arMiscReceipt, AD_BRNCH, AD_CMPNY);

              this.addArDrIliEntry(arMiscReceipt.getArDrNextLine(), "INVENTORY", EJBCommon.FALSE,
                  COST * QTY_SLD,
                  arInvoiceLineItem.getInvItemLocation().getIlGlCoaInventoryAccount(),
                  arMiscReceipt, AD_BRNCH, AD_CMPNY);

            }

            // add quantity to item location committed quantity

            double convertedQuantity =
                this.convertByUomFromAndItemAndQuantity(arInvoiceLineItem.getInvUnitOfMeasure(),
                    arInvoiceLineItem.getInvItemLocation().getInvItem(),
                    arInvoiceLineItem.getIliQuantity(), AD_CMPNY);
            invItemLocation.setIlCommittedQuantity(
                invItemLocation.getIlCommittedQuantity() + convertedQuantity);
            Debug.print("ArMiscReceiptEntryControllerBean saveArRctIliEntry  I03");
          }



        }



      }



      obj = new Object[0];
      jbossQl = new StringBuffer();
      jbossQl.append("SELECT OBJECT(inv) FROM ArInvoice inv");
      Collection arInvoices = arInvoiceHome.getInvByCriteria(jbossQl.toString(), obj);



      i = arInvoices.iterator();

      while (i.hasNext()) {

        LocalArInvoice arInvoice = (LocalArInvoice) i.next();

        Integer AD_CMPNY = arInvoice.getInvAdCompany();
        Integer AD_BRNCH = arInvoice.getInvAdBranch();

        // Sales Order
        Iterator iSol = arInvoice.getArSalesOrderInvoiceLines().iterator();

        Collection arDistributionRecords = arInvoice.getArDistributionRecords();

        Iterator iDr = arDistributionRecords.iterator();



        while (iDr.hasNext()) {

          LocalArDistributionRecord arDistributionRecord = (LocalArDistributionRecord) iDr.next();


          if (arDistributionRecord.getDrClass().equals("INVENTORY")
              || arDistributionRecord.getDrClass().equals("COGS")) {

            iDr.remove();
            arDistributionRecord.remove();
          }

        }


        // new dist

        LocalInvItemLocation invItemLocation = null;

        while (iSol.hasNext()) {

          LocalArSalesOrderInvoiceLine arSalesOrderInvoiceLine =
              (LocalArSalesOrderInvoiceLine) iSol.next();

          try {

            invItemLocation = invItemLocationHome.findByLocNameAndIiName(
                arSalesOrderInvoiceLine.getArSalesOrderLine().getInvItemLocation().getInvLocation()
                    .getLocName(),
                arSalesOrderInvoiceLine.getArSalesOrderLine().getInvItemLocation().getInvItem()
                    .getIiName(),
                AD_CMPNY);

          } catch (FinderException ex) {

            throw new GlobalInvItemLocationNotFoundException(
                String.valueOf(arSalesOrderInvoiceLine.getArSalesOrderLine().getSolLine()));

          }

          // start date validation

          if (adPreference.getPrfArAllowPriorDate() == EJBCommon.FALSE) {

            Collection invNegTxnCosting =
                invCostingHome.findNegTxnByGreaterThanCstDateAndIiNameAndLocName(
                    arInvoice.getInvDate(), invItemLocation.getInvItem().getIiName(),
                    invItemLocation.getInvLocation().getLocName(), AD_BRNCH, AD_CMPNY);
            if (!invNegTxnCosting.isEmpty())
              throw new GlobalInventoryDateException(invItemLocation.getInvItem().getIiName());

          }



          // add cost of sales distribution and inventory

          double COST = invItemLocation.getInvItem().getIiUnitCost();

          try {

            LocalInvCosting invCosting = invCostingHome
                .getByMaxCstDateToLongAndMaxCstLineNumberAndLessThanEqualCstDateAndIiNameAndLocName(
                    arInvoice.getInvDate(), invItemLocation.getInvItem().getIiName(),
                    invItemLocation.getInvLocation().getLocName(), AD_BRNCH, AD_CMPNY);

            // check if rmning vl is not zero and rmng qty is 0
            if (invCosting.getCstRemainingQuantity() <= 0) {

              HashMap criteria = new HashMap();
              criteria.put("itemName", invItemLocation.getInvItem().getIiName());
              criteria.put("location", invItemLocation.getInvLocation().getLocName());

              ArrayList branchList = new ArrayList();

              AdBranchDetails mdetails = new AdBranchDetails();
              mdetails.setBrCode(AD_BRNCH);
              branchList.add(mdetails);

              ejbRIC.executeInvFixItemCosting(criteria, branchList, AD_CMPNY);
              invCosting = invCostingHome
                  .getByMaxCstDateToLongAndMaxCstLineNumberAndLessThanEqualCstDateAndIiNameAndLocName(
                      arInvoice.getInvDate(), invItemLocation.getInvItem().getIiName(),
                      invItemLocation.getInvLocation().getLocName(), AD_BRNCH, AD_CMPNY);
            }



            if (invCosting.getInvItemLocation().getInvItem().getIiCostMethod().equals("Average"))

              COST = invCosting.getCstRemainingQuantity() <= 0 ? COST
                  : Math.abs(
                      invCosting.getCstRemainingValue() / invCosting.getCstRemainingQuantity());


            if (COST <= 0) {
              arSalesOrderInvoiceLine.getArSalesOrderLine().getInvItemLocation().getInvItem()
                  .getIiUnitCost();
            }


            else if (invCosting.getInvItemLocation().getInvItem().getIiCostMethod().equals("FIFO"))

              COST = invCosting.getCstRemainingQuantity() == 0 ? COST
                  : this.getInvFifoCost(arInvoice.getInvDate(), invItemLocation.getIlCode(),
                      arSalesOrderInvoiceLine.getSilQuantityDelivered(),
                      arSalesOrderInvoiceLine.getArSalesOrderLine().getSolUnitPrice(), false,
                      AD_BRNCH, AD_CMPNY);

            else if (invCosting.getInvItemLocation().getInvItem().getIiCostMethod()
                .equals("Standard"))

              COST = invItemLocation.getInvItem().getIiUnitCost();

          } catch (FinderException ex) {

            COST = arSalesOrderInvoiceLine.getArSalesOrderLine().getInvItemLocation().getInvItem()
                .getIiUnitCost();

          }

          double QTY_SLD = this.convertByUomFromAndItemAndQuantity(
              arSalesOrderInvoiceLine.getArSalesOrderLine().getInvUnitOfMeasure(),
              arSalesOrderInvoiceLine.getArSalesOrderLine().getInvItemLocation().getInvItem(),
              arSalesOrderInvoiceLine.getSilQuantityDelivered(), AD_CMPNY);

          LocalAdBranchItemLocation adBranchItemLocation = null;

          try {


            System.out.println("item loc in: "
                + arSalesOrderInvoiceLine.getArSalesOrderLine().getInvItemLocation().getIlCode());;
            adBranchItemLocation = adBranchItemLocationHome.findBilByIlCodeAndBrCode(
                arSalesOrderInvoiceLine.getArSalesOrderLine().getInvItemLocation().getIlCode(),
                AD_BRNCH, AD_CMPNY);

          } catch (FinderException ex) {

          }

          if (adPreference.getPrfArAutoComputeCogs() == EJBCommon.TRUE
              && arSalesOrderInvoiceLine.getArSalesOrderLine().getInvItemLocation().getInvItem()
                  .getIiNonInventoriable() == EJBCommon.FALSE) {

            if (adBranchItemLocation != null) {

              this.addArDrIliEntry(arInvoice.getArDrNextLine(), "COGS", EJBCommon.TRUE,
                  COST * QTY_SLD, adBranchItemLocation.getBilCoaGlCostOfSalesAccount(), arInvoice,
                  AD_BRNCH, AD_CMPNY);

              this.addArDrIliEntry(arInvoice.getArDrNextLine(), "INVENTORY", EJBCommon.FALSE,
                  COST * QTY_SLD, adBranchItemLocation.getBilCoaGlInventoryAccount(), arInvoice,
                  AD_BRNCH, AD_CMPNY);

            } else {

              this.addArDrIliEntry(arInvoice.getArDrNextLine(), "COGS", EJBCommon.TRUE,
                  COST * QTY_SLD, arSalesOrderInvoiceLine.getArSalesOrderLine().getInvItemLocation()
                      .getIlGlCoaCostOfSalesAccount(),
                  arInvoice, AD_BRNCH, AD_CMPNY);

              this.addArDrIliEntry(arInvoice.getArDrNextLine(), "INVENTORY", EJBCommon.FALSE,
                  COST * QTY_SLD, arSalesOrderInvoiceLine.getArSalesOrderLine().getInvItemLocation()
                      .getIlGlCoaInventoryAccount(),
                  arInvoice, AD_BRNCH, AD_CMPNY);

            }

          }

        }


        // line item

        Collection arInvoiceLineitems = arInvoice.getArInvoiceLineItems();

        Iterator iIli = arInvoiceLineitems.iterator();


        while (iIli.hasNext()) {

          LocalArInvoiceLineItem arInvoiceLineItem = (LocalArInvoiceLineItem) iIli.next();



          try {

            invItemLocation = invItemLocationHome.findByLocNameAndIiName(
                arInvoiceLineItem.getInvItemLocation().getInvLocation().getLocName(),
                arInvoiceLineItem.getInvItemLocation().getInvItem().getIiName(), AD_CMPNY);

          } catch (FinderException ex) {

            throw new GlobalInvItemLocationNotFoundException(
                String.valueOf(arInvoiceLineItem.getIliLine()));

          }

          // start date validation

          if (adPreference.getPrfArAllowPriorDate() == EJBCommon.FALSE) {

            Collection invNegTxnCosting =
                invCostingHome.findNegTxnByGreaterThanCstDateAndIiNameAndLocName(
                    arInvoice.getInvDate(), invItemLocation.getInvItem().getIiName(),
                    invItemLocation.getInvLocation().getLocName(), AD_BRNCH, AD_CMPNY);
            if (!invNegTxnCosting.isEmpty())
              throw new GlobalInventoryDateException(invItemLocation.getInvItem().getIiName());

          }

          // add cost of sales distribution and inventory

          double COST = invItemLocation.getInvItem().getIiUnitCost();

          try {

            LocalInvCosting invCosting = invCostingHome
                .getByMaxCstDateToLongAndMaxCstLineNumberAndLessThanEqualCstDateAndIiNameAndLocName(
                    arInvoice.getInvDate(), invItemLocation.getInvItem().getIiName(),
                    invItemLocation.getInvLocation().getLocName(), AD_BRNCH, AD_CMPNY);

            // check if rmning vl is not zero and rmng qty is 0
            System.out.println("COGS--------------------------------------->");
            System.out.println(
                "invCosting.getCstRemainingQuantity()=" + invCosting.getCstRemainingQuantity());
            System.out
                .println("invCosting.getCstRemainingValue()=" + invCosting.getCstRemainingValue());

            if (invItemLocation.getInvItem().getIiNonInventoriable() == (byte) 0
                && invCosting.getCstRemainingQuantity() <= 0
                && invCosting.getCstRemainingValue() <= 0) {
              System.out.println("RE CALC");
              HashMap criteria = new HashMap();
              criteria.put("itemName", invItemLocation.getInvItem().getIiName());
              criteria.put("location", invItemLocation.getInvLocation().getLocName());

              ArrayList branchList = new ArrayList();

              AdBranchDetails mdetails = new AdBranchDetails();
              mdetails.setBrCode(AD_BRNCH);
              branchList.add(mdetails);

              ejbRIC.executeInvFixItemCosting(criteria, branchList, AD_CMPNY);

              invCosting = invCostingHome
                  .getByMaxCstDateToLongAndMaxCstLineNumberAndLessThanEqualCstDateAndIiNameAndLocName(
                      arInvoice.getInvDate(), invItemLocation.getInvItem().getIiName(),
                      invItemLocation.getInvLocation().getLocName(), AD_BRNCH, AD_CMPNY);
            }


            if (invCosting.getInvItemLocation().getInvItem().getIiCostMethod().equals("Average")) {

              if (invCosting.getCstRemainingQuantity() <= 0) {
                COST = invItemLocation.getInvItem().getIiUnitCost();
              } else {
                COST = Math
                    .abs(invCosting.getCstRemainingValue() / invCosting.getCstRemainingQuantity());



                if (COST <= 0) {
                  COST = invItemLocation.getInvItem().getIiUnitCost();
                }



              }



            }
            if (invCosting.getInvItemLocation().getInvItem().getIiCostMethod().equals("FIFO")) {

              COST = this.getInvFifoCost(arInvoice.getInvDate(), invItemLocation.getIlCode(),
                  arInvoiceLineItem.getIliQuantity(), arInvoiceLineItem.getIliUnitPrice(), false,
                  AD_BRNCH, AD_CMPNY);

            } else if (invCosting.getInvItemLocation().getInvItem().getIiCostMethod()
                .equals("Standard")) {

              COST = invItemLocation.getInvItem().getIiUnitCost();
            }
            System.out.println(
                invCosting.getInvItemLocation().getInvItem().getIiCostMethod() + "=" + COST);


          } catch (FinderException ex) {

            COST = arInvoiceLineItem.getInvItemLocation().getInvItem().getIiUnitCost();

          }



          Debug.print("ArInvoiceEntryControllerBean saveArInvIliEntry E03");
          double QTY_SLD =
              this.convertByUomFromAndItemAndQuantity(arInvoiceLineItem.getInvUnitOfMeasure(),
                  arInvoiceLineItem.getInvItemLocation().getInvItem(),
                  arInvoiceLineItem.getIliQuantity(), AD_CMPNY);
          Debug.print("ArInvoiceEntryControllerBean saveArInvIliEntry E04");
          LocalAdBranchItemLocation adBranchItemLocation = null;

          System.out.println("COST=" + COST);
          System.out.println("QTY_SLD=" + QTY_SLD);
          try {


            adBranchItemLocation = adBranchItemLocationHome.findBilByIlCodeAndBrCode(
                arInvoiceLineItem.getInvItemLocation().getIlCode(), AD_BRNCH, AD_CMPNY);

          } catch (FinderException ex) {

          }

          Debug.print("ArInvoiceEntryControllerBean saveArInvIliEntry E05");
          if (adPreference.getPrfArAutoComputeCogs() == EJBCommon.TRUE && arInvoiceLineItem
              .getInvItemLocation().getInvItem().getIiNonInventoriable() == EJBCommon.FALSE) {

            if (arInvoiceLineItem.getIliEnableAutoBuild() == EJBCommon.FALSE) {

              if (adBranchItemLocation != null) {

                this.addArDrIliEntry(arInvoice.getArDrNextLine(), "COGS", EJBCommon.TRUE,
                    COST * QTY_SLD, adBranchItemLocation.getBilCoaGlCostOfSalesAccount(), arInvoice,
                    AD_BRNCH, AD_CMPNY);

                this.addArDrIliEntry(arInvoice.getArDrNextLine(), "INVENTORY", EJBCommon.FALSE,
                    COST * QTY_SLD, adBranchItemLocation.getBilCoaGlInventoryAccount(), arInvoice,
                    AD_BRNCH, AD_CMPNY);
                Debug.print("ArInvoiceEntryControllerBean saveArInvIliEntry E06");
              } else {

                this.addArDrIliEntry(arInvoice.getArDrNextLine(), "COGS", EJBCommon.TRUE,
                    COST * QTY_SLD,
                    arInvoiceLineItem.getInvItemLocation().getIlGlCoaCostOfSalesAccount(),
                    arInvoice, AD_BRNCH, AD_CMPNY);

                this.addArDrIliEntry(arInvoice.getArDrNextLine(), "INVENTORY", EJBCommon.FALSE,
                    COST * QTY_SLD,
                    arInvoiceLineItem.getInvItemLocation().getIlGlCoaInventoryAccount(), arInvoice,
                    AD_BRNCH, AD_CMPNY);
                Debug.print("ArInvoiceEntryControllerBean saveArInvIliEntry E07");
              }



            }

          }


        }


        // job order entry

        Collection jobOrderInvoiceLines = arInvoice.getArJobOrderInvoiceLines();

        Iterator �Jol = jobOrderInvoiceLines.iterator();

        while (�Jol.hasNext()) {



          LocalArJobOrderInvoiceLine arJobOrderInvoiceLine =
              (LocalArJobOrderInvoiceLine) �Jol.next();

          try {

            invItemLocation = invItemLocationHome.findByLocNameAndIiName(
                arJobOrderInvoiceLine.getArJobOrderLine().getInvItemLocation().getInvLocation()
                    .getLocName(),
                arJobOrderInvoiceLine.getArJobOrderLine().getInvItemLocation().getInvItem()
                    .getIiName(),
                AD_CMPNY);

          } catch (FinderException ex) {

            throw new GlobalInvItemLocationNotFoundException(
                String.valueOf(arJobOrderInvoiceLine.getArJobOrderLine().getJolLine()));

          }

          // start date validation

          if (adPreference.getPrfArAllowPriorDate() == EJBCommon.FALSE) {

            Collection invNegTxnCosting =
                invCostingHome.findNegTxnByGreaterThanCstDateAndIiNameAndLocName(
                    arInvoice.getInvDate(), invItemLocation.getInvItem().getIiName(),
                    invItemLocation.getInvLocation().getLocName(), AD_BRNCH, AD_CMPNY);
            if (!invNegTxnCosting.isEmpty())
              throw new GlobalInventoryDateException(invItemLocation.getInvItem().getIiName());

          }



          // add cost of sales distribution and inventory

          double COST = invItemLocation.getInvItem().getIiUnitCost();

          try {

            LocalInvCosting invCosting = invCostingHome
                .getByMaxCstDateToLongAndMaxCstLineNumberAndLessThanEqualCstDateAndIiNameAndLocName(
                    arInvoice.getInvDate(), invItemLocation.getInvItem().getIiName(),
                    invItemLocation.getInvLocation().getLocName(), AD_BRNCH, AD_CMPNY);

            // check if rmning vl is not zero and rmng qty is 0
            if (invCosting.getCstRemainingQuantity() <= 0) {

              HashMap criteria = new HashMap();
              criteria.put("itemName", invItemLocation.getInvItem().getIiName());
              criteria.put("location", invItemLocation.getInvLocation().getLocName());

              ArrayList branchList = new ArrayList();

              AdBranchDetails mdetails = new AdBranchDetails();
              mdetails.setBrCode(AD_BRNCH);
              branchList.add(mdetails);

              ejbRIC.executeInvFixItemCosting(criteria, branchList, AD_CMPNY);
              invCosting = invCostingHome
                  .getByMaxCstDateToLongAndMaxCstLineNumberAndLessThanEqualCstDateAndIiNameAndLocName(
                      arInvoice.getInvDate(), invItemLocation.getInvItem().getIiName(),
                      invItemLocation.getInvLocation().getLocName(), AD_BRNCH, AD_CMPNY);
            }



            if (invCosting.getInvItemLocation().getInvItem().getIiCostMethod().equals("Average"))

              COST = invCosting.getCstRemainingQuantity() <= 0 ? COST
                  : Math.abs(
                      invCosting.getCstRemainingValue() / invCosting.getCstRemainingQuantity());


            if (COST <= 0) {
              COST = invItemLocation.getInvItem().getIiUnitCost();
            }

            else if (invCosting.getInvItemLocation().getInvItem().getIiCostMethod().equals("FIFO"))

              COST = invCosting.getCstRemainingQuantity() == 0 ? COST
                  : this.getInvFifoCost(arInvoice.getInvDate(), invItemLocation.getIlCode(),
                      arJobOrderInvoiceLine.getJilQuantityDelivered(),
                      arJobOrderInvoiceLine.getArJobOrderLine().getJolUnitPrice(), false, AD_BRNCH,
                      AD_CMPNY);

            else if (invCosting.getInvItemLocation().getInvItem().getIiCostMethod()
                .equals("Standard"))

              COST = invItemLocation.getInvItem().getIiUnitCost();

          } catch (FinderException ex) {

            COST = arJobOrderInvoiceLine.getArJobOrderLine().getInvItemLocation().getInvItem()
                .getIiUnitCost();

          }

          double QTY_SLD = this.convertByUomFromAndItemAndQuantity(
              arJobOrderInvoiceLine.getArJobOrderLine().getInvUnitOfMeasure(),
              arJobOrderInvoiceLine.getArJobOrderLine().getInvItemLocation().getInvItem(),
              arJobOrderInvoiceLine.getJilQuantityDelivered(), AD_CMPNY);

          LocalAdBranchItemLocation adBranchItemLocation = null;

          try {



            adBranchItemLocation = adBranchItemLocationHome.findBilByIlCodeAndBrCode(
                arJobOrderInvoiceLine.getArJobOrderLine().getInvItemLocation().getIlCode(),
                AD_BRNCH, AD_CMPNY);

          } catch (FinderException ex) {

          }

          // Branch adding in Job Order
          if (adPreference.getPrfArAutoComputeCogs() == EJBCommon.TRUE
              && arJobOrderInvoiceLine.getArJobOrderLine().getInvItemLocation().getInvItem()
                  .getIiNonInventoriable() == EJBCommon.FALSE) {

            if (adBranchItemLocation != null) {

              this.addArDrIliEntry(arInvoice.getArDrNextLine(), "COGS", EJBCommon.TRUE,
                  COST * QTY_SLD, adBranchItemLocation.getBilCoaGlCostOfSalesAccount(), arInvoice,
                  AD_BRNCH, AD_CMPNY);

              this.addArDrIliEntry(arInvoice.getArDrNextLine(), "INVENTORY", EJBCommon.FALSE,
                  COST * QTY_SLD, adBranchItemLocation.getBilCoaGlInventoryAccount(), arInvoice,
                  AD_BRNCH, AD_CMPNY);

            } else {

              this.addArDrIliEntry(arInvoice.getArDrNextLine(), "COGS", EJBCommon.TRUE,
                  COST * QTY_SLD, arJobOrderInvoiceLine.getArJobOrderLine().getInvItemLocation()
                      .getIlGlCoaCostOfSalesAccount(),
                  arInvoice, AD_BRNCH, AD_CMPNY);

              this.addArDrIliEntry(arInvoice.getArDrNextLine(), "INVENTORY", EJBCommon.FALSE,
                  COST * QTY_SLD, arJobOrderInvoiceLine.getArJobOrderLine().getInvItemLocation()
                      .getIlGlCoaInventoryAccount(),
                  arInvoice, AD_BRNCH, AD_CMPNY);

            }

          }

        }



        // create journal entry
        try {
          LocalGlJournal glJournal = glJournalHome.findByJrDocumentNumberAndJsNameAndBrCode(
              arInvoice.getInvNumber(), arInvoice.getInvReferenceNumber(),
              arInvoice.getInvAdBranch(), arInvoice.getInvAdCompany());

          Collection glJournalLines = glJournal.getGlJournalLines();


          Iterator iJl = glJournalLines.iterator();

          while (iJl.hasNext()) {

            LocalGlJournalLine glJournalLine = (LocalGlJournalLine) iJl.next();

            glJournalLine.remove();


          }


          Collection arNewDistributionRecords = arInvoice.getArDistributionRecords();


          Iterator j = arDistributionRecords.iterator();

          while (j.hasNext()) {

            LocalArDistributionRecord arDistributionRecord = (LocalArDistributionRecord) j.next();

            double DR_AMNT = 0d;

            if (arInvoice.getInvCreditMemo() == EJBCommon.FALSE) {

              DR_AMNT = arDistributionRecord.getDrAmount();

            } else {

              DR_AMNT = arDistributionRecord.getDrAmount();
            }

            LocalGlJournalLine glJournalLine =
                glJournalLineHome.create(arDistributionRecord.getDrLine(),
                    arDistributionRecord.getDrDebit(), DR_AMNT, "", AD_CMPNY);

            glJournalLine.setGlChartOfAccount(arDistributionRecord.getGlChartOfAccount());

            glJournalLine.setGlJournal(glJournal);
            arDistributionRecord.setDrImported(EJBCommon.TRUE);



          }



        } catch (FinderException ex) {
          continue;
        }



        // create journal lines



      }



    } catch (Exception ex) {

      getSessionContext().setRollbackOnly();
      Debug.printStackTrace(ex);
      throw new EJBException(ex.getMessage());

    }



  }



  /**
   * @ejb:interface-method view-type="remote"
   * @jboss:method-attributes read-only="true"
   **/
  public ArrayList getAdApprovalNotifiedUsersByInvCode(
      Integer INV_CODE, Integer AD_CMPNY
  ) {

    Debug.print("ArInvoiceEntryControllerBean getAdApprovalNotifiedUsersByInvCode");


    LocalAdApprovalQueueHome adApprovalQueueHome = null;
    LocalArInvoiceHome arInvoiceHome = null;

    ArrayList list = new ArrayList();


    // Initialize EJB Home

    try {

      adApprovalQueueHome = (LocalAdApprovalQueueHome) EJBHomeFactory
          .lookUpLocalHome(LocalAdApprovalQueueHome.JNDI_NAME, LocalAdApprovalQueueHome.class);
      arInvoiceHome = (LocalArInvoiceHome) EJBHomeFactory
          .lookUpLocalHome(LocalArInvoiceHome.JNDI_NAME, LocalArInvoiceHome.class);

    } catch (NamingException ex) {

      throw new EJBException(ex.getMessage());

    }

    try {

      System.out.println("invCode is notified user: " + INV_CODE);
      LocalArInvoice arInvoice = arInvoiceHome.findByPrimaryKey(INV_CODE);

      if (arInvoice.getInvPosted() == EJBCommon.TRUE) {

        list.add("DOCUMENT POSTED");
        return list;

      }

      Collection adApprovalQueues =
          adApprovalQueueHome.findByAqDocumentAndAqDocumentCode("AR INVOICE", INV_CODE, AD_CMPNY);

      Iterator i = adApprovalQueues.iterator();

      while (i.hasNext()) {

        LocalAdApprovalQueue adApprovalQueue = (LocalAdApprovalQueue) i.next();

        list.add(adApprovalQueue.getAdUser().getUsrDescription());

      }

      return list;

    } catch (Exception ex) {

      Debug.printStackTrace(ex);
      throw new EJBException(ex.getMessage());

    }

  }


  /**
   * @ejb:interface-method view-type="remote"
   * @jboss:method-attributes read-only="true"
   **/
  public byte getAdPrfEnableArInvoiceBatch(
      Integer AD_CMPNY
  ) {

    Debug.print("ArInvoiceEntryControllerBean getAdPrfEnableArInvoiceBatch");

    LocalAdPreferenceHome adPreferenceHome = null;


    // Initialize EJB Home

    try {

      adPreferenceHome = (LocalAdPreferenceHome) EJBHomeFactory
          .lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);

    } catch (NamingException ex) {

      throw new EJBException(ex.getMessage());

    }


    try {

      LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);

      return adPreference.getPrfEnableArInvoiceBatch();

    } catch (Exception ex) {

      Debug.printStackTrace(ex);
      throw new EJBException(ex.getMessage());

    }

  }


  /**
   * @ejb:interface-method view-type="remote"
   * @jboss:method-attributes read-only="true"
   **/
  public byte getAdPrfInvoiceSalespersonRequired(
      Integer AD_CMPNY
  ) {

    Debug.print("ArInvoiceEntryControllerBean getAdPrfInvoiceSalespersonRequired");

    LocalAdPreferenceHome adPreferenceHome = null;


    // Initialize EJB Home

    try {

      adPreferenceHome = (LocalAdPreferenceHome) EJBHomeFactory
          .lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);

    } catch (NamingException ex) {

      throw new EJBException(ex.getMessage());

    }


    try {

      LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);

      return adPreference.getPrfArInvcSalespersonRequired();

    } catch (Exception ex) {

      Debug.printStackTrace(ex);
      throw new EJBException(ex.getMessage());

    }

  }


  /**
   * @ejb:interface-method view-type="remote"
   * @jboss:method-attributes read-only="true"
   **/
  public byte getAdPrfEnableInvShift(
      Integer AD_CMPNY
  ) {

    Debug.print("ArInvoiceEntryControllerBean getAdPrfEnableInvShift");

    LocalAdPreferenceHome adPreferenceHome = null;


    // Initialize EJB Home

    try {

      adPreferenceHome = (LocalAdPreferenceHome) EJBHomeFactory
          .lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);

    } catch (NamingException ex) {

      throw new EJBException(ex.getMessage());

    }


    try {

      LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);

      return adPreference.getPrfInvEnableShift();

    } catch (Exception ex) {

      Debug.printStackTrace(ex);
      throw new EJBException(ex.getMessage());

    }

  }


  /**
   * @ejb:interface-method view-type="remote"
   * @jboss:method-attributes read-only="true"
   **/
  public ArrayList getArOpenIbAll(
      Integer AD_BRNCH, Integer AD_CMPNY
  ) {

    Debug.print("ArInvoiceEntryControllerBean getArOpenIbAll");

    LocalArInvoiceBatchHome arInvoiceBatchHome = null;

    ArrayList list = new ArrayList();

    // Initialize EJB Home

    try {

      arInvoiceBatchHome = (LocalArInvoiceBatchHome) EJBHomeFactory
          .lookUpLocalHome(LocalArInvoiceBatchHome.JNDI_NAME, LocalArInvoiceBatchHome.class);

    } catch (NamingException ex) {

      throw new EJBException(ex.getMessage());

    }

    try {

      Collection arInvoiceBatches =
          arInvoiceBatchHome.findOpenIbByIbType("INVOICE", AD_BRNCH, AD_CMPNY);

      Iterator i = arInvoiceBatches.iterator();

      while (i.hasNext()) {

        LocalArInvoiceBatch arInvoiceBatch = (LocalArInvoiceBatch) i.next();

        list.add(arInvoiceBatch.getIbName());

      }

      return list;

    } catch (Exception ex) {

      Debug.printStackTrace(ex);
      throw new EJBException(ex.getMessage());

    }

  }



  /**
   * @ejb:interface-method view-type="remote"
   * @jboss:method-attributes read-only="true"
   **/
  public ArrayList getHrOpenDbAll(
      Integer AD_BRNCH, Integer AD_CMPNY
  ) {

    Debug.print("ArInvoiceEntryControllerBean getHrOpenDbAll");

    LocalHrDeployedBranchHome hrDeployedBranchHome = null;

    ArrayList list = new ArrayList();

    // Initialize EJB Home

    try {

      hrDeployedBranchHome = (LocalHrDeployedBranchHome) EJBHomeFactory
          .lookUpLocalHome(LocalHrDeployedBranchHome.JNDI_NAME, LocalHrDeployedBranchHome.class);

    } catch (NamingException ex) {

      throw new EJBException(ex.getMessage());

    }

    try {

      Collection hrDeployedBranches = hrDeployedBranchHome.findDbAll(AD_CMPNY);

      Iterator i = hrDeployedBranches.iterator();

      while (i.hasNext()) {

        LocalHrDeployedBranch hrDeployedBranch = (LocalHrDeployedBranch) i.next();

        list.add(hrDeployedBranch.getDbClientCode());

      }

      return list;

    } catch (Exception ex) {

      Debug.printStackTrace(ex);
      throw new EJBException(ex.getMessage());

    }

  }



  /**
   * @ejb:interface-method view-type="remote"
   * @jboss:method-attributes read-only="true"
   **/
  public ArrayList getPmOpenPrjAll(
      Integer AD_BRNCH, Integer AD_CMPNY
  ) {

    Debug.print("ArInvoiceEntryControllerBean getPmOpenPrjAll");

    LocalPmProjectHome hrProjectHome = null;

    ArrayList list = new ArrayList();

    // Initialize EJB Home

    try {

      hrProjectHome = (LocalPmProjectHome) EJBHomeFactory
          .lookUpLocalHome(LocalPmProjectHome.JNDI_NAME, LocalPmProjectHome.class);

    } catch (NamingException ex) {

      throw new EJBException(ex.getMessage());

    }

    try {

      Collection hrProjects = hrProjectHome.findPrjAll(AD_CMPNY);

      Iterator i = hrProjects.iterator();

      while (i.hasNext()) {

        LocalPmProject hrProject = (LocalPmProject) i.next();

        list.add(hrProject.getPrjProjectCode());

      }

      return list;

    } catch (Exception ex) {

      Debug.printStackTrace(ex);
      throw new EJBException(ex.getMessage());

    }

  }



  /**
   * @ejb:interface-method view-type="remote"
   * @jboss:method-attributes read-only="true"
   **/
  public ArrayList getPmOpenPttByPrjProjectCode(
      String PRJ_PRJCT_CODE, Integer AD_CMPNY
  ) {

    Debug.print("ArInvoiceEntryControllerBean getPmOpenPttByPrjProjectCode");


    LocalPmProjectTypeTypeHome pmProjectTypeTypeHome = null;

    ArrayList list = new ArrayList();

    // Initialize EJB Home

    try {

      pmProjectTypeTypeHome = (LocalPmProjectTypeTypeHome) EJBHomeFactory
          .lookUpLocalHome(LocalPmProjectTypeTypeHome.JNDI_NAME, LocalPmProjectTypeTypeHome.class);

    } catch (NamingException ex) {

      throw new EJBException(ex.getMessage());

    }

    try {

      Collection pmProjectTypeTypes =
          pmProjectTypeTypeHome.findPttByPrjProjectCode(PRJ_PRJCT_CODE, AD_CMPNY);

      Iterator i = pmProjectTypeTypes.iterator();

      while (i.hasNext()) {

        LocalPmProjectTypeType pmProjectTypeType = (LocalPmProjectTypeType) i.next();

        list.add(pmProjectTypeType.getPmProjectType().getPtProjectTypeCode());

      }

      return list;

    } catch (Exception ex) {

      Debug.printStackTrace(ex);
      throw new EJBException(ex.getMessage());

    }

  }



  /**
   * @ejb:interface-method view-type="remote"
   * @jboss:method-attributes read-only="true"
   **/
  public ArrayList getPmOpenCtByPtProjectTypeCode(
      String PRJ_PRJCT_CODE, String PT_PRJCT_TYP_CODE, Integer AD_CMPNY
  ) {

    Debug.print("ArInvoiceEntryControllerBean getPmOpenCtByPtProjectTypeCode");


    LocalPmProjectTypeTypeHome pmProjectTypeTypeHome = null;

    ArrayList list = new ArrayList();

    // Initialize EJB Home

    try {

      pmProjectTypeTypeHome = (LocalPmProjectTypeTypeHome) EJBHomeFactory
          .lookUpLocalHome(LocalPmProjectTypeTypeHome.JNDI_NAME, LocalPmProjectTypeTypeHome.class);

    } catch (NamingException ex) {

      throw new EJBException(ex.getMessage());

    }

    try {

      LocalPmProjectTypeType pmProjectTypeTypes = pmProjectTypeTypeHome
          .findPttByPrjProjectCodeAndPtProjectTypeCode(PRJ_PRJCT_CODE, PT_PRJCT_TYP_CODE, AD_CMPNY);

      Collection contractTerms = pmProjectTypeTypes.getPmContract().getPmContractTerms();

      Iterator i = contractTerms.iterator();

      while (i.hasNext()) {

        LocalPmContractTerm pmContractTerm = (LocalPmContractTerm) i.next();

        list.add(pmContractTerm.getCtTermDescription());

      }

      return list;

    } catch (Exception ex) {

      Debug.printStackTrace(ex);
      throw new EJBException(ex.getMessage());

    }

  }


  /**
   * @ejb:interface-method view-type="remote"
   * @jboss:method-attributes read-only="true"
   **/
  public ArrayList getPmOpenPpByPrjProjectCodeAndPtProjectTypeCode(
      String PRJ_PRJCT_CODE, String PRJ_PRJCT_TYP_CODE, Integer AD_BRNCH, Integer AD_CMPNY
  ) {

    Debug.print("ArInvoiceEntryControllerBean getPmOpenPpByPrjProjectCodeAndPtProjectTypeCode");

    LocalPmProjectPhaseHome pmProjectPhaseHome = null;

    ArrayList list = new ArrayList();

    // Initialize EJB Home

    try {

      pmProjectPhaseHome = (LocalPmProjectPhaseHome) EJBHomeFactory
          .lookUpLocalHome(LocalPmProjectPhaseHome.JNDI_NAME, LocalPmProjectPhaseHome.class);

    } catch (NamingException ex) {

      throw new EJBException(ex.getMessage());

    }

    try {

      Collection pmProjectPhases = pmProjectPhaseHome
          .findPpByPrjProjectCodeAndPtProjectTypeCode(PRJ_PRJCT_CODE, PRJ_PRJCT_TYP_CODE, AD_CMPNY);

      Iterator i = pmProjectPhases.iterator();

      while (i.hasNext()) {

        LocalPmProjectPhase pmProjectPhase = (LocalPmProjectPhase) i.next();

        list.add(pmProjectPhase.getPpName());

      }

      return list;

    } catch (Exception ex) {

      Debug.printStackTrace(ex);
      throw new EJBException(ex.getMessage());

    }

  }



  /**
   * @ejb:interface-method view-type="remote"
   * @jboss:method-attributes read-only="true"
   **/
  public ArrayList getPmOpenCtByPrjProjectCodeAndPrjProjectPhaseName(
      String PRJ_PRJCT_CODE, String PRJ_PRJCT_PHASE_NM, Integer AD_BRNCH, Integer AD_CMPNY
  ) {

    Debug.print("ArInvoiceEntryControllerBean getPmOpenCtByPrjProjectCodeAndPrjProjectPhaseName");

    LocalPmContractTermHome pmContractTermHome = null;


    ArrayList list = new ArrayList();

    // Initialize EJB Home

    try {

      pmContractTermHome = (LocalPmContractTermHome) EJBHomeFactory
          .lookUpLocalHome(LocalPmContractTermHome.JNDI_NAME, LocalPmContractTermHome.class);

    } catch (NamingException ex) {

      throw new EJBException(ex.getMessage());

    }

    try {

      Collection pmContractTerms = pmContractTermHome.findCtAll(AD_CMPNY);

      Iterator i = pmContractTerms.iterator();

      while (i.hasNext()) {

        LocalPmContractTerm pmContractTerm = (LocalPmContractTerm) i.next();

        list.add(pmContractTerm.getCtTermDescription());

      }

      return list;

    } catch (Exception ex) {

      Debug.printStackTrace(ex);
      throw new EJBException(ex.getMessage());

    }

  }


  /**
   * @ejb:interface-method view-type="remote"
   * @jboss:method-attributes read-only="true"
   **/
  public short getInvGpQuantityPrecisionUnit(
      Integer AD_CMPNY
  ) {

    Debug.print("ArInvoiceEntryControllerBean getInvGpQuantityPrecisionUnit");

    LocalAdPreferenceHome adPreferenceHome = null;

    // Initialize EJB Home

    try {

      adPreferenceHome = (LocalAdPreferenceHome) EJBHomeFactory
          .lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);

    } catch (NamingException ex) {

      throw new EJBException(ex.getMessage());

    }


    try {

      LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);

      return adPreference.getPrfInvQuantityPrecisionUnit();

    } catch (Exception ex) {

      Debug.printStackTrace(ex);
      throw new EJBException(ex.getMessage());

    }

  }


  /**
   * @ejb:interface-method view-type="remote"
   * @jboss:method-attributes read-only="true"
   **/
  public short getInvGpCostPrecisionUnit(
      Integer AD_CMPNY
  ) {

    Debug.print("ArInvoiceEntryControllerBean getInvGpCostPrecisionUnit");

    LocalAdPreferenceHome adPreferenceHome = null;

    // Initialize EJB Home

    try {

      adPreferenceHome = (LocalAdPreferenceHome) EJBHomeFactory
          .lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);

    } catch (NamingException ex) {

      throw new EJBException(ex.getMessage());

    }


    try {

      LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);

      return adPreference.getPrfInvCostPrecisionUnit();

    } catch (Exception ex) {

      Debug.printStackTrace(ex);
      throw new EJBException(ex.getMessage());

    }

  }

  /**
   * @ejb:interface-method view-type="remote"
   * @jboss:method-attributes read-only="true"
   **/
  public byte getAdPrfArUseCustomerPulldown(
      Integer AD_CMPNY
  ) {

    Debug.print("ArInvoiceEntryControllerBean getAdPrfArUseCustomerPulldown");

    LocalAdPreferenceHome adPreferenceHome = null;

    // Initialize EJB Home

    try {

      adPreferenceHome = (LocalAdPreferenceHome) EJBHomeFactory
          .lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);

    } catch (NamingException ex) {

      throw new EJBException(ex.getMessage());

    }


    try {

      LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);

      return adPreference.getPrfArUseCustomerPulldown();

    } catch (Exception ex) {

      Debug.printStackTrace(ex);
      throw new EJBException(ex.getMessage());

    }

  }

  /**
   * @ejb:interface-method view-type="remote"
   * @jboss:method-attributes read-only="true"
   **/
  public byte getAdPrfArDisableSalesPrice(
      Integer AD_CMPNY
  ) {

    Debug.print("ArInvoiceEntryControllerBean getAdPrfArDisableSalesPrice");

    LocalAdPreferenceHome adPreferenceHome = null;

    // Initialize EJB Home

    try {

      adPreferenceHome = (LocalAdPreferenceHome) EJBHomeFactory
          .lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);

    } catch (NamingException ex) {

      throw new EJBException(ex.getMessage());

    }


    try {

      LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);

      return adPreference.getPrfArDisableSalesPrice();

    } catch (Exception ex) {

      Debug.printStackTrace(ex);
      throw new EJBException(ex.getMessage());

    }

  }

  /**
   * @ejb:interface-method view-type="remote"
   */
  public boolean getInvAutoBuildEnabledByIiName(
      String II_NM, Integer AD_CMPNY
  ) throws GlobalNoRecordFoundException {

    Debug.print("ArInvoiceEntryControllerBean getInvAutoBuildEnabledByIiCode");

    LocalInvItemHome invItemHome = null;

    try {

      invItemHome = (LocalInvItemHome) EJBHomeFactory.lookUpLocalHome(LocalInvItemHome.JNDI_NAME,
          LocalInvItemHome.class);

    } catch (NamingException ex) {

      throw new EJBException(ex.getMessage());

    }

    try {

      try {

        LocalInvItem invItem = invItemHome.findByIiName(II_NM, AD_CMPNY);

        if (invItem.getIiEnableAutoBuild() == 1)
          return true;

        return false;

      } catch (FinderException ex) {

        throw new GlobalNoRecordFoundException();

      }

    } catch (GlobalNoRecordFoundException ex) {

      throw ex;

    }

  }

  /**
   * @ejb:interface-method view-type="remote"
   * @jboss:method-attributes read-only="true"
   **/
  public String getInvIiClassByIiName(
      String II_NM, Integer AD_CMPNY
  ) {

    Debug.print("ArInvoiceEntryControllerBean getInvIiClassByIiName");

    LocalInvItemHome invItemHome = null;

    // Initialize EJB Home

    try {

      invItemHome = (LocalInvItemHome) EJBHomeFactory.lookUpLocalHome(LocalInvItemHome.JNDI_NAME,
          LocalInvItemHome.class);

    } catch (NamingException ex) {

      throw new EJBException(ex.getMessage());
    }

    try {

      System.out.println("II_NM=" + II_NM);
      System.out.println("AD_CMPNY=" + AD_CMPNY);

      LocalInvItem invItem = invItemHome.findByIiName(II_NM, AD_CMPNY);


      return invItem.getIiClass();

    } catch (Exception ex) {

      Debug.printStackTrace(ex);
      throw new EJBException(ex.getMessage());

    }

  }



  /**
   * @ejb:interface-method view-type="remote"
   * @jboss:method-attributes read-only="true"
   **/
  public boolean getInvIiNonInventoriableByIiName(
      String II_NM, Integer AD_CMPNY
  ) {

    Debug.print("ArInvoiceEntryControllerBean getInvIiNonInventoriableByIiName");

    LocalInvItemHome invItemHome = null;

    // Initialize EJB Home

    try {

      invItemHome = (LocalInvItemHome) EJBHomeFactory.lookUpLocalHome(LocalInvItemHome.JNDI_NAME,
          LocalInvItemHome.class);

    } catch (NamingException ex) {

      throw new EJBException(ex.getMessage());
    }

    try {

      try {

        LocalInvItem invItem = invItemHome.findByIiName(II_NM, AD_CMPNY);

        if (invItem.getIiNonInventoriable() == 1)
          return false;

        return true;

      } catch (FinderException ex) {

        throw new GlobalNoRecordFoundException();

      }

    } catch (Exception ex) {

      Debug.printStackTrace(ex);
      throw new EJBException(ex.getMessage());

    }

  }

  /**
   * @ejb:interface-method view-type="remote"
   * @jboss:method-attributes read-only="true"
   **/
  public ArrayList getArNotIssuedSolBySoNumberAndCustomer(
      String SO_DCMNT_NMBR, String CST_CSTMR_CODE, ArrayList issuedSolList, Integer AD_CMPNY
  ) throws GlobalNoRecordFoundException {

    Debug.print("ArInvoiceEntryControllerBean getArSolBySoCode");

    LocalArSalesOrderHome arSalesOrderHome = null;

    try {

      arSalesOrderHome = (LocalArSalesOrderHome) EJBHomeFactory
          .lookUpLocalHome(LocalArSalesOrderHome.JNDI_NAME, LocalArSalesOrderHome.class);


    } catch (NamingException ex) {

      throw new EJBException(ex.getMessage());

    }

    try {

      LocalArSalesOrder arSalesOrder = null;

      try {

        arSalesOrder = arSalesOrderHome.findBySoDocumentNumberAndCstCustomerCode(SO_DCMNT_NMBR,
            CST_CSTMR_CODE, AD_CMPNY);

      } catch (FinderException ex) {

        throw new GlobalNoRecordFoundException();

      }

      ArrayList solList = new ArrayList();

      Collection arSalesOrderLines = arSalesOrder.getArSalesOrderLines();

      Iterator i = arSalesOrderLines.iterator();

      while (i.hasNext()) {

        LocalArSalesOrderLine arSalesOrderLine = (LocalArSalesOrderLine) i.next();

        boolean issued = false;

        Iterator j = issuedSolList.iterator();

        while (j.hasNext()) {

          Integer issuedSolCode = (Integer) j.next();

          if (arSalesOrderLine.getSolCode().equals(issuedSolCode)) {

            issued = true;
            break;

          }

        }

        if (!issued) {

          ArModSalesOrderLineDetails mdetails = new ArModSalesOrderLineDetails();

          mdetails.setSolCode(arSalesOrderLine.getSolCode());
          mdetails.setSolLine(arSalesOrderLine.getSolLine());
          mdetails.setSolUnitPrice(arSalesOrderLine.getSolUnitPrice());
          mdetails.setSolIiName(arSalesOrderLine.getInvItemLocation().getInvItem().getIiName());
          mdetails
              .setSolLocName(arSalesOrderLine.getInvItemLocation().getInvLocation().getLocName());
          mdetails.setSolUomName(arSalesOrderLine.getInvUnitOfMeasure().getUomName());
          mdetails.setSolIiDescription(
              arSalesOrderLine.getInvItemLocation().getInvItem().getIiDescription());
          mdetails.setSolRemaining(arSalesOrderLine.getSolQuantity());
          mdetails.setSolQuantity(arSalesOrderLine.getSolQuantity());
          mdetails.setSolIssue(false);
          mdetails.setSolDiscount1(arSalesOrderLine.getSolDiscount1());
          mdetails.setSolDiscount2(arSalesOrderLine.getSolDiscount2());
          mdetails.setSolDiscount3(arSalesOrderLine.getSolDiscount3());
          mdetails.setSolDiscount4(arSalesOrderLine.getSolDiscount4());
          mdetails.setSolTotalDiscount(arSalesOrderLine.getSolTotalDiscount());
          mdetails.setSolTax(arSalesOrderLine.getSolTax());
          mdetails
              .setTraceMisc(arSalesOrderLine.getInvItemLocation().getInvItem().getIiTraceMisc());

          if (mdetails.getTraceMisc() == 1) {
            ArrayList tagList = this.getInvTagList(arSalesOrderLine);

            mdetails.setSolTagList(tagList);

          }


          solList.add(mdetails);

        }

      }

      return solList;

    } catch (GlobalNoRecordFoundException ex) {

      Debug.printStackTrace(ex);
      throw ex;

    } catch (Exception ex) {

      Debug.printStackTrace(ex);
      throw new EJBException(ex.getMessage());

    }

  }


  /**
   * @ejb:interface-method view-type="remote"
   * @jboss:method-attributes read-only="true"
   **/
  public ArrayList getArNotIssuedJolByJoNumberAndCustomer(
      String JO_DCMNT_NMBR, String CST_CSTMR_CODE, ArrayList issuedJolList, Integer AD_CMPNY
  ) throws GlobalNoRecordFoundException {

    Debug.print("ArInvoiceEntryControllerBean getArNotIssuedJolByJoNumberAndCustomer");

    LocalArJobOrderHome arJobOrderHome = null;

    try {

      arJobOrderHome = (LocalArJobOrderHome) EJBHomeFactory
          .lookUpLocalHome(LocalArJobOrderHome.JNDI_NAME, LocalArJobOrderHome.class);


    } catch (NamingException ex) {

      throw new EJBException(ex.getMessage());

    }

    try {

      LocalArJobOrder arJobOrder = null;

      try {

        arJobOrder = arJobOrderHome.findByJoDocumentNumberAndCstCustomerCode(JO_DCMNT_NMBR,
            CST_CSTMR_CODE, AD_CMPNY);

      } catch (FinderException ex) {

        throw new GlobalNoRecordFoundException();

      }

      ArrayList jolList = new ArrayList();

      Collection arJobOrderLines = arJobOrder.getArJobOrderLines();

      Iterator i = arJobOrderLines.iterator();

      while (i.hasNext()) {

        LocalArJobOrderLine arJobOrderLine = (LocalArJobOrderLine) i.next();

        boolean issued = false;

        Iterator j = issuedJolList.iterator();

        while (j.hasNext()) {

          Integer issuedJolCode = (Integer) j.next();

          if (arJobOrderLine.getJolCode().equals(issuedJolCode)) {

            issued = true;
            break;

          }

        }

        if (!issued) {

          ArModJobOrderLineDetails mdetails = new ArModJobOrderLineDetails();

          mdetails.setJolCode(arJobOrderLine.getJolCode());
          mdetails.setJolLine(arJobOrderLine.getJolLine());
          mdetails.setJolUnitPrice(arJobOrderLine.getJolUnitPrice());
          mdetails.setJolIiName(arJobOrderLine.getInvItemLocation().getInvItem().getIiName());
          mdetails.setJolLocName(arJobOrderLine.getInvItemLocation().getInvLocation().getLocName());
          mdetails.setJolUomName(arJobOrderLine.getInvUnitOfMeasure().getUomName());
          mdetails.setJolIiDescription(
              arJobOrderLine.getInvItemLocation().getInvItem().getIiDescription());
          mdetails.setJolRemaining(arJobOrderLine.getJolQuantity());
          mdetails.setJolQuantity(arJobOrderLine.getJolQuantity());
          mdetails.setJolIssue(false);
          mdetails.setJolDiscount1(arJobOrderLine.getJolDiscount1());
          mdetails.setJolDiscount2(arJobOrderLine.getJolDiscount2());
          mdetails.setJolDiscount3(arJobOrderLine.getJolDiscount3());
          mdetails.setJolDiscount4(arJobOrderLine.getJolDiscount4());
          mdetails.setJolTotalDiscount(arJobOrderLine.getJolTotalDiscount());
          mdetails.setJolTax(arJobOrderLine.getJolTax());
          mdetails.setTraceMisc(arJobOrderLine.getInvItemLocation().getInvItem().getIiTraceMisc());

          if (mdetails.getTraceMisc() == 1) {
            ArrayList tagList = this.getInvTagList(arJobOrderLine);

            mdetails.setJolTagList(tagList);

          }


          jolList.add(mdetails);

        }

      }

      return jolList;

    } catch (GlobalNoRecordFoundException ex) {

      Debug.printStackTrace(ex);
      throw ex;

    } catch (Exception ex) {

      Debug.printStackTrace(ex);
      throw new EJBException(ex.getMessage());

    }

  }



  /**
   * @ejb:interface-method view-type="remote"
   * @jboss:method-attributes read-only="true"
   **/
  public ArrayList getArSlpAll(
      Integer AD_BRNCH, Integer AD_CMPNY
  ) {

    Debug.print("ArInvoiceEntryControllerBean getArSlpAll");

    LocalArSalespersonHome arSalespersonHome = null;

    ArrayList list = new ArrayList();

    // Initialize EJB Home

    try {

      arSalespersonHome = (LocalArSalespersonHome) EJBHomeFactory
          .lookUpLocalHome(LocalArSalespersonHome.JNDI_NAME, LocalArSalespersonHome.class);

    } catch (NamingException ex) {

      throw new EJBException(ex.getMessage());

    }

    try {

      Collection arSalespersons = arSalespersonHome.findSlpByBrCode(AD_BRNCH, AD_CMPNY);

      Iterator i = arSalespersons.iterator();

      while (i.hasNext()) {

        LocalArSalesperson arSalesperson = (LocalArSalesperson) i.next();

        ArSalespersonDetails details = new ArSalespersonDetails();
        details.setSlpSalespersonCode(arSalesperson.getSlpSalespersonCode());
        details.setSlpName(arSalesperson.getSlpName());

        list.add(details);

      }

      return list;

    } catch (Exception ex) {

      Debug.printStackTrace(ex);
      throw new EJBException(ex.getMessage());

    }

  }

  /**
   * @ejb:interface-method view-type="remote"
   * @jboss:method-attributes read-only="true"
   **/
  public ArrayList getInvLitByCstLitName(
      String CST_LIT_NAME, Integer AD_CMPNY
  ) throws GlobalNoRecordFoundException {

    Debug.print("ArInvoiceEntryControllerBean getInvLitByCstLitName");

    LocalInvLineItemTemplateHome invLineItemTemplateHome = null;

    // Initialize EJB Home

    try {

      invLineItemTemplateHome = (LocalInvLineItemTemplateHome) EJBHomeFactory.lookUpLocalHome(
          LocalInvLineItemTemplateHome.JNDI_NAME, LocalInvLineItemTemplateHome.class);

    } catch (NamingException ex) {

      throw new EJBException(ex.getMessage());

    }

    try {

      LocalInvLineItemTemplate invLineItemTemplate = null;


      try {

        invLineItemTemplate = invLineItemTemplateHome.findByLitName(CST_LIT_NAME, AD_CMPNY);

      } catch (FinderException ex) {

        throw new GlobalNoRecordFoundException();

      }

      ArrayList list = new ArrayList();

      // get line items if any

      Collection invLineItems = invLineItemTemplate.getInvLineItems();

      if (!invLineItems.isEmpty()) {

        Iterator i = invLineItems.iterator();

        while (i.hasNext()) {

          LocalInvLineItem invLineItem = (LocalInvLineItem) i.next();

          InvModLineItemDetails liDetails = new InvModLineItemDetails();

          liDetails.setLiCode(invLineItem.getLiCode());
          liDetails.setLiLine(invLineItem.getLiLine());
          liDetails.setLiIiName(invLineItem.getInvItemLocation().getInvItem().getIiName());
          liDetails.setLiLocName(invLineItem.getInvItemLocation().getInvLocation().getLocName());
          liDetails.setLiUomName(invLineItem.getInvUnitOfMeasure().getUomName());
          liDetails
              .setLiIiDescription(invLineItem.getInvItemLocation().getInvItem().getIiDescription());

          list.add(liDetails);

        }

      }

      return list;

    } catch (GlobalNoRecordFoundException ex) {

      throw ex;

    } catch (Exception ex) {

      Debug.printStackTrace(ex);
      throw new EJBException(ex.getMessage());

    }

  }

  /**
   * @ejb:interface-method view-type="remote"
   * @jboss:method-attributes read-only="true"
   **/
  public double getFrRateByFrNameAndFrDate(
      String FC_NM, Date CONVERSION_DATE, Integer AD_CMPNY
  ) throws GlobalConversionDateNotExistException {

    Debug.print("ArInvoiceEntryControllerBean getFrRateByFrNameAndFrDate");

    LocalGlFunctionalCurrencyRateHome glFunctionalCurrencyRateHome = null;
    LocalGlFunctionalCurrencyHome glFunctionalCurrencyHome = null;
    LocalAdCompanyHome adCompanyHome = null;

    // Initialize EJB Home

    try {

      glFunctionalCurrencyRateHome =
          (LocalGlFunctionalCurrencyRateHome) EJBHomeFactory.lookUpLocalHome(
              LocalGlFunctionalCurrencyRateHome.JNDI_NAME, LocalGlFunctionalCurrencyRateHome.class);
      glFunctionalCurrencyHome = (LocalGlFunctionalCurrencyHome) EJBHomeFactory.lookUpLocalHome(
          LocalGlFunctionalCurrencyHome.JNDI_NAME, LocalGlFunctionalCurrencyHome.class);
      adCompanyHome = (LocalAdCompanyHome) EJBHomeFactory
          .lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);

    } catch (NamingException ex) {

      throw new EJBException(ex.getMessage());

    }

    try {

      LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
      LocalGlFunctionalCurrency glFunctionalCurrency =
          glFunctionalCurrencyHome.findByFcName(FC_NM, AD_CMPNY);

      double CONVERSION_RATE = 1;

      // Get functional currency rate

      if (!FC_NM.equals("USD")) {

        LocalGlFunctionalCurrencyRate glFunctionalCurrencyRate = glFunctionalCurrencyRateHome
            .findByFcCodeAndDate(glFunctionalCurrency.getFcCode(), CONVERSION_DATE, AD_CMPNY);

        CONVERSION_RATE = glFunctionalCurrencyRate.getFrXToUsd();

      }

      // Get set of book functional currency rate if necessary

      if (!adCompany.getGlFunctionalCurrency().getFcName().equals("USD")) {

        LocalGlFunctionalCurrencyRate glCompanyFunctionalCurrencyRate =
            glFunctionalCurrencyRateHome.findByFcCodeAndDate(
                adCompany.getGlFunctionalCurrency().getFcCode(), CONVERSION_DATE, AD_CMPNY);

        CONVERSION_RATE = CONVERSION_RATE / glCompanyFunctionalCurrencyRate.getFrXToUsd();

      }

      return CONVERSION_RATE;

    } catch (FinderException ex) {

      getSessionContext().setRollbackOnly();
      throw new GlobalConversionDateNotExistException();

    } catch (Exception ex) {

      Debug.printStackTrace(ex);
      throw new EJBException(ex.getMessage());

    }

  }

  /**
   * @ejb:interface-method view-type="remote"
   * @jboss:method-attributes read-only="true"
   **/
  public byte getAdPrfArEnablePaymentTerm(
      Integer AD_CMPNY
  ) {

    Debug.print("ArInvoiceEntryControllerBean getAdPrfArEnablePaymentTerm");

    LocalAdPreferenceHome adPreferenceHome = null;


    // Initialize EJB Home

    try {

      adPreferenceHome = (LocalAdPreferenceHome) EJBHomeFactory
          .lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);

    } catch (NamingException ex) {

      throw new EJBException(ex.getMessage());

    }


    try {

      LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);

      return adPreference.getPrfArEnablePaymentTerm();

    } catch (Exception ex) {

      Debug.printStackTrace(ex);
      throw new EJBException(ex.getMessage());

    }

  }

  // private methods

  private LocalArInvoiceLine addArIlEntry(
      ArModInvoiceLineDetails mdetails, LocalArInvoice arInvoice, Integer AD_CMPNY
  ) {

    Debug.print("ArInvoiceEntryControllerBean addArIlEntry");

    LocalArInvoiceLineHome arInvoiceLineHome = null;
    LocalArStandardMemoLineHome arStandardMemoLineHome = null;


    // Initialize EJB Home

    try {

      arInvoiceLineHome = (LocalArInvoiceLineHome) EJBHomeFactory
          .lookUpLocalHome(LocalArInvoiceLineHome.JNDI_NAME, LocalArInvoiceLineHome.class);
      arStandardMemoLineHome = (LocalArStandardMemoLineHome) EJBHomeFactory.lookUpLocalHome(
          LocalArStandardMemoLineHome.JNDI_NAME, LocalArStandardMemoLineHome.class);

    } catch (NamingException ex) {

      throw new EJBException(ex.getMessage());

    }

    try {

      short precisionUnit = this.getGlFcPrecisionUnit(AD_CMPNY);

      double IL_AMNT = 0d;
      double IL_TAX_AMNT = 0d;

      if (mdetails.getIlTax() == EJBCommon.TRUE) {

        LocalArTaxCode arTaxCode = arInvoice.getArTaxCode();

        // calculate net amount
        IL_AMNT = this.calculateIlNetAmount(mdetails, arTaxCode.getTcRate(), arTaxCode.getTcType(),
            precisionUnit);

        // calculate tax
        IL_TAX_AMNT = this.calculateIlTaxAmount(mdetails, arTaxCode.getTcRate(),
            arTaxCode.getTcType(), IL_AMNT, precisionUnit);

      } else {

        IL_AMNT = mdetails.getIlAmount();

      }

      LocalArInvoiceLine arInvoiceLine =
          arInvoiceLineHome.create(mdetails.getIlDescription(), mdetails.getIlQuantity(),
              mdetails.getIlUnitPrice(), IL_AMNT, IL_TAX_AMNT, mdetails.getIlTax(), AD_CMPNY);

      // arInvoice.addArInvoiceLine(arInvoiceLine);
      arInvoiceLine.setArInvoice(arInvoice);

      LocalArStandardMemoLine arStandardMemoLine =
          arStandardMemoLineHome.findBySmlName(mdetails.getIlSmlName(), AD_CMPNY);
      // arStandardMemoLine.addArInvoiceLine(arInvoiceLine);
      arInvoiceLine.setArStandardMemoLine(arStandardMemoLine);

      return arInvoiceLine;

    } catch (Exception ex) {

      Debug.printStackTrace(ex);
      getSessionContext().setRollbackOnly();
      throw new EJBException(ex.getMessage());

    }

  }


  /*
   * private byte getAdPrfArDetailedReceivable(Integer AD_CMPNY) {
   * 
   * 
   * 
   * Debug.print("ArInvoiceEntryControllerBean addArIlEntry");
   * 
   * 
   * LocalAdPreferenceHome adPreferenceHome = null;
   * 
   * // Initialize EJB Home
   * 
   * try {
   * 
   * adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
   * lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);
   * 
   * 
   * } catch (NamingException ex) {
   * 
   * throw new EJBException(ex.getMessage());
   * 
   * }
   * 
   * try{
   * 
   * LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);
   * 
   * return adPreference.getPrfArDetailedReceivable();
   * 
   * } catch (Exception ex) {
   * 
   * Debug.printStackTrace(ex); getSessionContext().setRollbackOnly(); throw new
   * EJBException(ex.getMessage());
   * 
   * }
   * 
   * 
   * 
   * }
   */

  private double calculateIlNetAmount(
      ArModInvoiceLineDetails mdetails, double tcRate, String tcType, short precisionUnit
  ) {

    double amount = 0d;

    if (tcType.equals("INCLUSIVE")) {

      amount = EJBCommon.roundIt(mdetails.getIlAmount() / (1 + (tcRate / 100)), precisionUnit);

    } else {

      // tax exclusive, none, zero rated or exempt

      amount = mdetails.getIlAmount();

    }

    return amount;

  }

  private double calculateIlTaxAmount(
      ArModInvoiceLineDetails mdetails, double tcRate, String tcType, double amount,
      short precisionUnit
  ) {

    double taxAmount = 0d;

    if (!tcType.equals("NONE") && !tcType.equals("EXEMPT")) {


      if (tcType.equals("INCLUSIVE")) {

        taxAmount = EJBCommon.roundIt(mdetails.getIlAmount() - amount, precisionUnit);

      } else if (tcType.equals("EXCLUSIVE")) {

        taxAmount = EJBCommon.roundIt(mdetails.getIlAmount() * tcRate / 100, precisionUnit);

      } else {

        // tax none zero-rated or exempt

      }

    }

    return taxAmount;

  }


  private void addArDrEntry(
      short DR_LN, String DR_CLSS, byte DR_DBT, double DR_AMNT, Integer COA_CODE, Integer SC_COA,
      LocalArInvoice arInvoice, Integer AD_BRNCH, Integer AD_CMPNY
  ) throws GlobalBranchAccountNumberInvalidException {

    Debug.print("ArInvoiceEntryControllerBean addArDrEntry");

    LocalArDistributionRecordHome arDistributionRecordHome = null;
    LocalGlChartOfAccountHome glChartOfAccountHome = null;
    LocalAdCompanyHome adCompanyHome = null;


    // Initialize EJB Home

    try {

      arDistributionRecordHome = (LocalArDistributionRecordHome) EJBHomeFactory.lookUpLocalHome(
          LocalArDistributionRecordHome.JNDI_NAME, LocalArDistributionRecordHome.class);
      glChartOfAccountHome = (LocalGlChartOfAccountHome) EJBHomeFactory
          .lookUpLocalHome(LocalGlChartOfAccountHome.JNDI_NAME, LocalGlChartOfAccountHome.class);
      adCompanyHome = (LocalAdCompanyHome) EJBHomeFactory
          .lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);


    } catch (NamingException ex) {

      throw new EJBException(ex.getMessage());

    }

    try {

      // get company

      LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);

      System.out.println("COA_CODE is: " + COA_CODE + "  AD_BRNCH is:" + AD_BRNCH);
      LocalGlChartOfAccount glChartOfAccount =
          glChartOfAccountHome.findByCoaCodeAndBranchCode(COA_CODE, AD_BRNCH, AD_CMPNY);

      System.out.println("COA_CODE is: " + COA_CODE);


      // create distribution record

      if (DR_AMNT < 0) {

        DR_AMNT = DR_AMNT * -1;

        if (DR_DBT == 0)
          DR_DBT = 1;
        else if (DR_DBT == 1)
          DR_DBT = 0;

      }

      LocalArDistributionRecord arDistributionRecord =
          arDistributionRecordHome.create(DR_LN, DR_CLSS, DR_DBT,
              EJBCommon.roundIt(DR_AMNT, adCompany.getGlFunctionalCurrency().getFcPrecision()),
              EJBCommon.FALSE, EJBCommon.FALSE, AD_CMPNY);

      // arInvoice.addArDistributionRecord(arDistributionRecord);
      arDistributionRecord.setArInvoice(arInvoice);
      // glChartOfAccount.addArDistributionRecord(arDistributionRecord);
      arDistributionRecord.setGlChartOfAccount(glChartOfAccount);

      if (DR_CLSS.equals("SC")) {
        if (SC_COA == null) {

          throw new GlobalBranchAccountNumberInvalidException();
        }

        else {
          arDistributionRecord.setDrScAccount(SC_COA);
        }

      }

    } catch (FinderException ex) {

      throw new GlobalBranchAccountNumberInvalidException();

    } catch (Exception ex) {

      Debug.printStackTrace(ex);
      getSessionContext().setRollbackOnly();
      throw new EJBException(ex.getMessage());

    }

  }


  private Integer getArGlCoaRevenueAccount(
      LocalArInvoiceLine arInvoiceLine, Integer AD_BRNCH, Integer AD_CMPNY
  ) {

    Debug.print("ArInvoiceLineEntryControllerBean getArGlCoaRevenueAccount");


    LocalArAutoAccountingSegmentHome arAutoAccountingSegmentHome = null;
    LocalGlChartOfAccountHome glChartOfAccountHome = null;
    LocalAdCompanyHome adCompanyHome = null;
    LocalAdBranchStandardMemoLineHome adBranchStandardMemoLineHome = null;

    // Initialize EJB Home

    try {

      arAutoAccountingSegmentHome =
          (LocalArAutoAccountingSegmentHome) EJBHomeFactory.lookUpLocalHome(
              LocalArAutoAccountingSegmentHome.JNDI_NAME, LocalArAutoAccountingSegmentHome.class);
      glChartOfAccountHome = (LocalGlChartOfAccountHome) EJBHomeFactory
          .lookUpLocalHome(LocalGlChartOfAccountHome.JNDI_NAME, LocalGlChartOfAccountHome.class);
      adCompanyHome = (LocalAdCompanyHome) EJBHomeFactory
          .lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
      adBranchStandardMemoLineHome =
          (LocalAdBranchStandardMemoLineHome) EJBHomeFactory.lookUpLocalHome(
              LocalAdBranchStandardMemoLineHome.JNDI_NAME, LocalAdBranchStandardMemoLineHome.class);

    } catch (NamingException ex) {

      throw new EJBException(ex.getMessage());

    }


    // generate revenue account

    try {

      String GL_COA_ACCNT = "";

      LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);

      LocalGenField genField = adCompany.getGenField();

      String FL_SGMNT_SPRTR = String.valueOf(genField.getFlSegmentSeparator());

      LocalAdBranchStandardMemoLine adBranchStandardMemoLine = null;

      try {

        adBranchStandardMemoLine = adBranchStandardMemoLineHome.findBSMLBySMLCodeAndBrCode(
            arInvoiceLine.getArStandardMemoLine().getSmlCode(), AD_BRNCH, AD_CMPNY);

      } catch (FinderException ex) {

      }


      Collection arAutoAccountingSegments =
          arAutoAccountingSegmentHome.findByAaAccountType("REVENUE", AD_CMPNY);

      Iterator i = arAutoAccountingSegments.iterator();

      while (i.hasNext()) {

        LocalArAutoAccountingSegment arAutoAccountingSegment =
            (LocalArAutoAccountingSegment) i.next();

        LocalGlChartOfAccount glChartOfAccount = null;

        if (arAutoAccountingSegment.getAasClassType().equals("AR CUSTOMER")) {


          System.out.println("is null 0");
          System.out.println(
              arInvoiceLine.getArInvoice().getArCustomer().getCstGlCoaRevenueAccount().toString());
          System.out.println("is pass");

          glChartOfAccount = glChartOfAccountHome.findByPrimaryKey(
              arInvoiceLine.getArInvoice().getArCustomer().getCstGlCoaRevenueAccount());

          StringTokenizer st =
              new StringTokenizer(glChartOfAccount.getCoaAccountNumber(), FL_SGMNT_SPRTR);

          int ctr = 0;
          while (st.hasMoreTokens()) {

            ++ctr;

            if (ctr == arAutoAccountingSegment.getAasSegmentNumber()) {

              GL_COA_ACCNT = GL_COA_ACCNT + FL_SGMNT_SPRTR + st.nextToken();

              break;

            } else {

              st.nextToken();

            }
          }


        } else if (arAutoAccountingSegment.getAasClassType().equals("AR STANDARD MEMO LINE")) {

          if (adBranchStandardMemoLine != null) {

            try {
              System.out.println("is null 1");
              System.out.println(adBranchStandardMemoLine.getBsmlGlCoaRevenueAccount().toString());
              System.out.println("is pass");
              glChartOfAccount = glChartOfAccountHome
                  .findByPrimaryKey(adBranchStandardMemoLine.getBsmlGlCoaRevenueAccount());

            } catch (FinderException ex) {

            }


          } else {


            // glChartOfAccount = arInvoiceLine.getArStandardMemoLine().getGlChartOfAccount();
            System.out.println("is null 2");
            System.out.println(
                arInvoiceLine.getArStandardMemoLine().getSmlGlCoaReceivableAccount().toString());
            System.out.println("is pass");
            glChartOfAccount = glChartOfAccountHome.findByPrimaryKey(
                arInvoiceLine.getArStandardMemoLine().getSmlGlCoaReceivableAccount());


          }

          StringTokenizer st =
              new StringTokenizer(glChartOfAccount.getCoaAccountNumber(), FL_SGMNT_SPRTR);

          int ctr = 0;
          while (st.hasMoreTokens()) {

            ++ctr;

            if (ctr == arAutoAccountingSegment.getAasSegmentNumber()) {

              GL_COA_ACCNT = GL_COA_ACCNT + FL_SGMNT_SPRTR + st.nextToken();

              break;

            } else {

              st.nextToken();

            }
          }

        }
      }

      GL_COA_ACCNT = GL_COA_ACCNT.substring(1, GL_COA_ACCNT.length());

      try {

        LocalGlChartOfAccount glGeneratedChartOfAccount =
            glChartOfAccountHome.findByCoaAccountNumber(GL_COA_ACCNT, AD_CMPNY);

        return glGeneratedChartOfAccount.getCoaCode();

      } catch (FinderException ex) {

        if (adBranchStandardMemoLine != null) {

          LocalGlChartOfAccount glChartOfAccount = null;

          try {
            System.out.println("null 2");
            System.out.println(adBranchStandardMemoLine.getBsmlGlCoaRevenueAccount().toString());
            System.out.println("pass");
            glChartOfAccount = glChartOfAccountHome
                .findByPrimaryKey(adBranchStandardMemoLine.getBsmlGlCoaRevenueAccount());;

          } catch (FinderException e) {

          }

          return glChartOfAccount.getCoaCode();

        } else {


          return arInvoiceLine.getArStandardMemoLine().getSmlGlCoaRevenueAccount();

        }

      }


    } catch (Exception ex) {

      Debug.printStackTrace(ex);
      throw new EJBException(ex.getMessage());

    }

  }


  private Integer getArGlCoaReceivableAccount(
      LocalArInvoiceLine arInvoiceLine, Integer AD_BRNCH, Integer AD_CMPNY
  ) {

    Debug.print("ArInvoiceLineEntryControllerBean getArGlCoaReceivableAccount");


    LocalArAutoAccountingSegmentHome arAutoAccountingSegmentHome = null;
    LocalGlChartOfAccountHome glChartOfAccountHome = null;
    LocalAdCompanyHome adCompanyHome = null;
    LocalAdBranchStandardMemoLineHome adBranchStandardMemoLineHome = null;

    // Initialize EJB Home

    try {

      arAutoAccountingSegmentHome =
          (LocalArAutoAccountingSegmentHome) EJBHomeFactory.lookUpLocalHome(
              LocalArAutoAccountingSegmentHome.JNDI_NAME, LocalArAutoAccountingSegmentHome.class);
      glChartOfAccountHome = (LocalGlChartOfAccountHome) EJBHomeFactory
          .lookUpLocalHome(LocalGlChartOfAccountHome.JNDI_NAME, LocalGlChartOfAccountHome.class);
      adCompanyHome = (LocalAdCompanyHome) EJBHomeFactory
          .lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
      adBranchStandardMemoLineHome =
          (LocalAdBranchStandardMemoLineHome) EJBHomeFactory.lookUpLocalHome(
              LocalAdBranchStandardMemoLineHome.JNDI_NAME, LocalAdBranchStandardMemoLineHome.class);

    } catch (NamingException ex) {

      throw new EJBException(ex.getMessage());

    }


    // generate revenue account

    try {

      String GL_COA_ACCNT = "";

      LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);

      LocalGenField genField = adCompany.getGenField();

      String FL_SGMNT_SPRTR = String.valueOf(genField.getFlSegmentSeparator());

      LocalAdBranchStandardMemoLine adBranchStandardMemoLine = null;

      try {

        adBranchStandardMemoLine = adBranchStandardMemoLineHome.findBSMLBySMLCodeAndBrCode(
            arInvoiceLine.getArStandardMemoLine().getSmlCode(), AD_BRNCH, AD_CMPNY);

      } catch (FinderException ex) {

      }



      Collection arAutoAccountingSegments =
          arAutoAccountingSegmentHome.findByAaAccountType("REVENUE", AD_CMPNY);

      Iterator i = arAutoAccountingSegments.iterator();

      while (i.hasNext()) {

        LocalArAutoAccountingSegment arAutoAccountingSegment =
            (LocalArAutoAccountingSegment) i.next();

        LocalGlChartOfAccount glChartOfAccount = null;

        if (arAutoAccountingSegment.getAasClassType().equals("AR CUSTOMER")) {
          System.out.println("AR CUSTOMER---------------->");

          System.out.println("null 3");
          System.out
              .println(arInvoiceLine.getArInvoice().getArCustomer().getCstGlCoaReceivableAccount());
          System.out.println("pass");

          glChartOfAccount = glChartOfAccountHome.findByPrimaryKey(
              arInvoiceLine.getArInvoice().getArCustomer().getCstGlCoaReceivableAccount());

          StringTokenizer st =
              new StringTokenizer(glChartOfAccount.getCoaAccountNumber(), FL_SGMNT_SPRTR);

          int ctr = 0;
          while (st.hasMoreTokens()) {

            ++ctr;

            if (ctr == arAutoAccountingSegment.getAasSegmentNumber()) {

              GL_COA_ACCNT = GL_COA_ACCNT + FL_SGMNT_SPRTR + st.nextToken();

              break;

            } else {

              st.nextToken();

            }
          }


        } else if (arAutoAccountingSegment.getAasClassType().equals("AR STANDARD MEMO LINE")) {
          System.out.println("AR STANDARD MEMO LINE---------------->");
          if (adBranchStandardMemoLine != null) {

            try {

              glChartOfAccount = glChartOfAccountHome
                  .findByPrimaryKey(adBranchStandardMemoLine.getBsmlGlCoaReceivableAccount());

            } catch (FinderException ex) {

            }


          } else {


            // glChartOfAccount = arInvoiceLine.getArStandardMemoLine().getGlChartOfAccount();

            glChartOfAccount = glChartOfAccountHome.findByPrimaryKey(
                arInvoiceLine.getArStandardMemoLine().getSmlGlCoaReceivableAccount());


          }

          StringTokenizer st =
              new StringTokenizer(glChartOfAccount.getCoaAccountNumber(), FL_SGMNT_SPRTR);

          int ctr = 0;
          while (st.hasMoreTokens()) {

            ++ctr;

            if (ctr == arAutoAccountingSegment.getAasSegmentNumber()) {

              GL_COA_ACCNT = GL_COA_ACCNT + FL_SGMNT_SPRTR + st.nextToken();

              break;

            } else {

              st.nextToken();

            }
          }

        }
      }
      System.out.println("GL_COA_ACCNT=" + GL_COA_ACCNT);
      GL_COA_ACCNT = GL_COA_ACCNT.substring(1, GL_COA_ACCNT.length());

      try {

        LocalGlChartOfAccount glGeneratedChartOfAccount =
            glChartOfAccountHome.findByCoaAccountNumber(GL_COA_ACCNT, AD_CMPNY);

        return glGeneratedChartOfAccount.getCoaCode();

      } catch (FinderException ex) {

        if (adBranchStandardMemoLine != null) {

          LocalGlChartOfAccount glChartOfAccount = null;

          try {

            glChartOfAccount = glChartOfAccountHome
                .findByPrimaryKey(adBranchStandardMemoLine.getBsmlGlCoaReceivableAccount());;

          } catch (FinderException e) {

          }

          return glChartOfAccount.getCoaCode();

        } else {


          return arInvoiceLine.getArStandardMemoLine().getSmlGlCoaReceivableAccount();

        }

      }


    } catch (Exception ex) {

      Debug.printStackTrace(ex);
      throw new EJBException(ex.getMessage());

    }

  }

  private void executeArInvPost(
      Integer INV_CODE, String USR_NM, Integer AD_BRNCH, Integer AD_CMPNY
  ) throws GlobalRecordAlreadyDeletedException, GlobalTransactionAlreadyPostedException,
      GlobalTransactionAlreadyVoidException, GlJREffectiveDateNoPeriodExistException,
      GlJREffectiveDatePeriodClosedException, GlobalJournalNotBalanceException,
      GlobalBranchAccountNumberInvalidException, AdPRFCoaGlVarianceAccountNotFoundException,
      GlobalExpiryDateNotFoundException {

    Debug.print("ArInvoiceEntryControllerBean executeApInvPost");

    LocalArInvoiceHome arInvoiceHome = null;
    LocalAdCompanyHome adCompanyHome = null;
    LocalAdPreferenceHome adPreferenceHome = null;
    LocalGlSetOfBookHome glSetOfBookHome = null;
    LocalGlAccountingCalendarValueHome glAccountingCalendarValueHome = null;
    LocalGlJournalHome glJournalHome = null;
    LocalGlJournalBatchHome glJournalBatchHome = null;
    LocalGlSuspenseAccountHome glSuspenseAccountHome = null;
    LocalGlJournalLineHome glJournalLineHome = null;
    LocalGlJournalSourceHome glJournalSourceHome = null;
    LocalGlJournalCategoryHome glJournalCategoryHome = null;
    LocalGlFunctionalCurrencyHome glFunctionalCurrencyHome = null;
    LocalArDistributionRecordHome arDistributionRecordHome = null;
    LocalInvCostingHome invCostingHome = null;
    LocalInvItemHome invItemHome = null;
    LocalGlChartOfAccountHome glChartOfAccountHome = null;
    LocalGlForexLedgerHome glForexLedgerHome = null;
    LocalGlFunctionalCurrencyRateHome glFunctionalCurrencyRateHome = null;

    LocalArInvoice arInvoice = null;
    LocalArInvoice arCreditedInvoice = null;

    LocalInvItemLocationHome invItemLocationHome = null;
    
    InvItemEntryControllerHome homeII = null;
    InvItemEntryController ejbII = null;

    // Initialize EJB Home

    try {
    
      homeII = (InvItemEntryControllerHome) com.util.EJBHomeFactory
          .lookUpHome("ejb/InvItemEntryControllerEJB", InvItemEntryControllerHome.class);   

      arInvoiceHome = (LocalArInvoiceHome) EJBHomeFactory
          .lookUpLocalHome(LocalArInvoiceHome.JNDI_NAME, LocalArInvoiceHome.class);
      adCompanyHome = (LocalAdCompanyHome) EJBHomeFactory
          .lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
      adPreferenceHome = (LocalAdPreferenceHome) EJBHomeFactory
          .lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);
      glSetOfBookHome = (LocalGlSetOfBookHome) EJBHomeFactory
          .lookUpLocalHome(LocalGlSetOfBookHome.JNDI_NAME, LocalGlSetOfBookHome.class);
      glAccountingCalendarValueHome = (LocalGlAccountingCalendarValueHome) EJBHomeFactory
          .lookUpLocalHome(LocalGlAccountingCalendarValueHome.JNDI_NAME,
              LocalGlAccountingCalendarValueHome.class);
      glJournalHome = (LocalGlJournalHome) EJBHomeFactory
          .lookUpLocalHome(LocalGlJournalHome.JNDI_NAME, LocalGlJournalHome.class);
      glJournalBatchHome = (LocalGlJournalBatchHome) EJBHomeFactory
          .lookUpLocalHome(LocalGlJournalBatchHome.JNDI_NAME, LocalGlJournalBatchHome.class);
      glSuspenseAccountHome = (LocalGlSuspenseAccountHome) EJBHomeFactory
          .lookUpLocalHome(LocalGlSuspenseAccountHome.JNDI_NAME, LocalGlSuspenseAccountHome.class);
      glJournalLineHome = (LocalGlJournalLineHome) EJBHomeFactory
          .lookUpLocalHome(LocalGlJournalLineHome.JNDI_NAME, LocalGlJournalLineHome.class);
      glJournalSourceHome = (LocalGlJournalSourceHome) EJBHomeFactory
          .lookUpLocalHome(LocalGlJournalSourceHome.JNDI_NAME, LocalGlJournalSourceHome.class);
      glJournalCategoryHome = (LocalGlJournalCategoryHome) EJBHomeFactory
          .lookUpLocalHome(LocalGlJournalCategoryHome.JNDI_NAME, LocalGlJournalCategoryHome.class);
      glFunctionalCurrencyHome = (LocalGlFunctionalCurrencyHome) EJBHomeFactory.lookUpLocalHome(
          LocalGlFunctionalCurrencyHome.JNDI_NAME, LocalGlFunctionalCurrencyHome.class);
      arDistributionRecordHome = (LocalArDistributionRecordHome) EJBHomeFactory.lookUpLocalHome(
          LocalArDistributionRecordHome.JNDI_NAME, LocalArDistributionRecordHome.class);
      invCostingHome = (LocalInvCostingHome) EJBHomeFactory
          .lookUpLocalHome(LocalInvCostingHome.JNDI_NAME, LocalInvCostingHome.class);
      invItemHome = (LocalInvItemHome) EJBHomeFactory.lookUpLocalHome(LocalInvItemHome.JNDI_NAME,
          LocalInvItemHome.class);
      glChartOfAccountHome = (LocalGlChartOfAccountHome) EJBHomeFactory
          .lookUpLocalHome(LocalGlChartOfAccountHome.JNDI_NAME, LocalGlChartOfAccountHome.class);
      glForexLedgerHome = (LocalGlForexLedgerHome) EJBHomeFactory
          .lookUpLocalHome(LocalGlForexLedgerHome.JNDI_NAME, LocalGlForexLedgerHome.class);
      glFunctionalCurrencyRateHome =
          (LocalGlFunctionalCurrencyRateHome) EJBHomeFactory.lookUpLocalHome(
              LocalGlFunctionalCurrencyRateHome.JNDI_NAME, LocalGlFunctionalCurrencyRateHome.class);
      invItemLocationHome = (LocalInvItemLocationHome) EJBHomeFactory
          .lookUpLocalHome(LocalInvItemLocationHome.JNDI_NAME, LocalInvItemLocationHome.class);

    } catch (NamingException ex) {

      throw new EJBException(ex.getMessage());

    }
    
     try {
    
      ejbII = homeII.create();
    } catch (RemoteException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    } catch (CreateException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }
   

    try {

      LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
      LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);

      // validate if invoice/credit memo is already deleted

      try {

        arInvoice = arInvoiceHome.findByPrimaryKey(INV_CODE);

      } catch (FinderException ex) {

        throw new GlobalRecordAlreadyDeletedException();

      }

      // validate if invoice/credit memo is already posted or void

      if (arInvoice.getInvPosted() == EJBCommon.TRUE) {

        throw new GlobalTransactionAlreadyPostedException();

      } else if (arInvoice.getInvVoid() == EJBCommon.TRUE) {

        throw new GlobalTransactionAlreadyVoidException();
      }

      // regenerate cogs & inventory dr
      // && arInvoiceLineItem.getInvItemLocation().getInvItem().getIiDineInCharge() ==
      // EJBCommon.TRUE
      
      /*
      if (adPreference.getPrfArAutoComputeCogs() == EJBCommon.TRUE) {
        this.regenerateInventoryDr(arInvoice, AD_BRNCH, AD_CMPNY);
      }
            */
      // post invoice/credit memo

      if (arInvoice.getInvCreditMemo() == EJBCommon.FALSE) {

        // increase customer balance
        /*
         * double INV_AMNT =
         * this.convertForeignToFunctionalCurrency(arInvoice.getGlFunctionalCurrency().getFcCode(),
         * arInvoice.getGlFunctionalCurrency().getFcName(), arInvoice.getInvConversionDate(),
         * arInvoice.getInvConversionRate(), arInvoice.getInvAmountDue(), AD_CMPNY);
         */

        double INV_AMNT = arInvoice.getInvAmountDue();
        this.post(arInvoice.getInvDate(), INV_AMNT, arInvoice.getArCustomer(), AD_CMPNY);

        Collection arInvoiceLineItems = arInvoice.getArInvoiceLineItems();
        Collection arSalesOrderInvoiceLines = arInvoice.getArSalesOrderInvoiceLines();
        Collection arJobOrderInvoiceLines = arInvoice.getArJobOrderInvoiceLines();

        if (arInvoiceLineItems != null && !arInvoiceLineItems.isEmpty()) {

          Iterator c = arInvoiceLineItems.iterator();

          while (c.hasNext()) {

            LocalArInvoiceLineItem arInvoiceLineItem = (LocalArInvoiceLineItem) c.next();


            String II_NM = arInvoiceLineItem.getInvItemLocation().getInvItem().getIiName();
            String LOC_NM = arInvoiceLineItem.getInvItemLocation().getInvLocation().getLocName();

            double QTY_SLD =
                this.convertByUomFromAndItemAndQuantity(arInvoiceLineItem.getInvUnitOfMeasure(),
                    arInvoiceLineItem.getInvItemLocation().getInvItem(),
                    arInvoiceLineItem.getIliQuantity(), AD_CMPNY);

            LocalInvCosting invCosting = null;
            
            try {

              invCosting = invCostingHome
                  .getByMaxCstDateToLongAndMaxCstLineNumberAndLessThanEqualCstDateAndIiNameAndLocName(
                      arInvoice.getInvDate(), II_NM, LOC_NM, AD_BRNCH, AD_CMPNY);

            } catch (FinderException ex) {

            }

            double COST = arInvoiceLineItem.getInvItemLocation().getInvItem().getIiUnitCost();

            if (invCosting == null) {

              this.postToInv(arInvoiceLineItem, arInvoice.getInvDate(), QTY_SLD, COST * QTY_SLD,
                  -QTY_SLD, -COST * QTY_SLD, 0d, null, AD_BRNCH, AD_CMPNY);

            } else {

              if (invCosting.getInvItemLocation().getInvItem().getIiCostMethod()
                  .equals("Average")) {

                double avgCost = ejbII.getInvIiUnitCostByIiNameAndUomName( arInvoiceLineItem.getInvItemLocation().getInvItem().getIiName(), arInvoiceLineItem.getInvUnitOfMeasure().getUomName(),  arInvoice.getInvDate(), AD_CMPNY);

                this.postToInv(arInvoiceLineItem, arInvoice.getInvDate(), QTY_SLD,
                    avgCost * QTY_SLD, invCosting.getCstRemainingQuantity() - QTY_SLD,
                    invCosting.getCstRemainingValue() - (QTY_SLD * avgCost), 0d, null, AD_BRNCH,
                    AD_CMPNY);


              } else if (invCosting.getInvItemLocation().getInvItem().getIiCostMethod()
                  .equals("FIFO")) {

                double fifoCost = invCosting.getCstRemainingQuantity() == 0 ? COST
                    : this.getInvFifoCost(invCosting.getCstDate(),
                        invCosting.getInvItemLocation().getIlCode(), QTY_SLD,
                        arInvoiceLineItem.getIliUnitPrice(), true, AD_BRNCH, AD_CMPNY);

                this.postToInv(arInvoiceLineItem, arInvoice.getInvDate(), QTY_SLD,
                    fifoCost * QTY_SLD, invCosting.getCstRemainingQuantity() - QTY_SLD,
                    invCosting.getCstRemainingValue() - (fifoCost * QTY_SLD), 0d, null, AD_BRNCH,
                    AD_CMPNY);

              } else if (invCosting.getInvItemLocation().getInvItem().getIiCostMethod()
                  .equals("Standard")) {

                double standardCost = invCosting.getInvItemLocation().getInvItem().getIiUnitCost();

                this.postToInv(arInvoiceLineItem, arInvoice.getInvDate(), QTY_SLD,
                    standardCost * QTY_SLD, invCosting.getCstRemainingQuantity() - QTY_SLD,
                    invCosting.getCstRemainingValue() - (standardCost * QTY_SLD), 0d, null,
                    AD_BRNCH, AD_CMPNY);


              }

            }

          }



        } else if (arSalesOrderInvoiceLines != null && !arSalesOrderInvoiceLines.isEmpty()) {

          Iterator c = arSalesOrderInvoiceLines.iterator();

          while (c.hasNext()) {

            LocalArSalesOrderInvoiceLine arSalesOrderInvoiceLine =
                (LocalArSalesOrderInvoiceLine) c.next();
            LocalArSalesOrderLine arSalesOrderLine = arSalesOrderInvoiceLine.getArSalesOrderLine();

            String II_NM = arSalesOrderLine.getInvItemLocation().getInvItem().getIiName();
            String LOC_NM = arSalesOrderLine.getInvItemLocation().getInvLocation().getLocName();
            double QTY_SLD =
                this.convertByUomFromAndItemAndQuantity(arSalesOrderLine.getInvUnitOfMeasure(),
                    arSalesOrderLine.getInvItemLocation().getInvItem(),
                    arSalesOrderInvoiceLine.getSilQuantityDelivered(), AD_CMPNY);

            LocalInvCosting invCosting = null;

            try {

              invCosting = invCostingHome
                  .getByMaxCstDateToLongAndMaxCstLineNumberAndLessThanEqualCstDateAndIiNameAndLocName(
                      arInvoice.getInvDate(), II_NM, LOC_NM, AD_BRNCH, AD_CMPNY);

            } catch (FinderException ex) {

            }

            double COST = arSalesOrderLine.getInvItemLocation().getInvItem().getIiUnitCost();

            if (invCosting == null) {


              this.postToInvSo(arSalesOrderInvoiceLine, arInvoice.getInvDate(), QTY_SLD,
                  COST * QTY_SLD, -QTY_SLD, -(QTY_SLD * COST), 0d, null, AD_BRNCH, AD_CMPNY);

            } else {


              if (invCosting.getInvItemLocation().getInvItem().getIiCostMethod()
                  .equals("Average")) {

        

                double avgCost = ejbII.getInvIiUnitCostByIiNameAndUomName( arSalesOrderLine.getInvItemLocation().getInvItem().getIiName(), arSalesOrderLine.getInvUnitOfMeasure().getUomName(),  arInvoice.getInvDate(), AD_CMPNY);

                this.postToInvSo(arSalesOrderInvoiceLine, arInvoice.getInvDate(), QTY_SLD,
                    avgCost * QTY_SLD, invCosting.getCstRemainingQuantity() - QTY_SLD,
                    invCosting.getCstRemainingValue() - (QTY_SLD * avgCost), 0d, null, AD_BRNCH,
                    AD_CMPNY);


              } else if (invCosting.getInvItemLocation().getInvItem().getIiCostMethod()
                  .equals("FIFO")) {

                double fifoCost = this.getInvFifoCost(invCosting.getCstDate(),
                    invCosting.getInvItemLocation().getIlCode(), QTY_SLD,
                    arSalesOrderInvoiceLine.getArSalesOrderLine().getSolUnitPrice(), true, AD_BRNCH,
                    AD_CMPNY);

                this.postToInvSo(arSalesOrderInvoiceLine, arInvoice.getInvDate(), QTY_SLD,
                    fifoCost * QTY_SLD, invCosting.getCstRemainingQuantity() - QTY_SLD,
                    invCosting.getCstRemainingValue() - (fifoCost * QTY_SLD), 0d, null, AD_BRNCH,
                    AD_CMPNY);

              } else if (invCosting.getInvItemLocation().getInvItem().getIiCostMethod()
                  .equals("Standard")) {

                double standardCost = invCosting.getInvItemLocation().getInvItem().getIiUnitCost();

                this.postToInvSo(arSalesOrderInvoiceLine, arInvoice.getInvDate(), QTY_SLD,
                    standardCost * QTY_SLD, invCosting.getCstRemainingQuantity() - QTY_SLD,
                    invCosting.getCstRemainingValue() - (standardCost * QTY_SLD), 0d, null,
                    AD_BRNCH, AD_CMPNY);

              }

            }
          }
        } else if (arJobOrderInvoiceLines != null && !arJobOrderInvoiceLines.isEmpty()) {

          Iterator c = arJobOrderInvoiceLines.iterator();

          while (c.hasNext()) {

            LocalArJobOrderInvoiceLine arJobOrderInvoiceLine =
                (LocalArJobOrderInvoiceLine) c.next();
            LocalArJobOrderLine arJobOrderLine = arJobOrderInvoiceLine.getArJobOrderLine();

            String II_NM = arJobOrderLine.getInvItemLocation().getInvItem().getIiName();
            String LOC_NM = arJobOrderLine.getInvItemLocation().getInvLocation().getLocName();
            double QTY_SLD =
                this.convertByUomFromAndItemAndQuantity(arJobOrderLine.getInvUnitOfMeasure(),
                    arJobOrderLine.getInvItemLocation().getInvItem(),
                    arJobOrderInvoiceLine.getJilQuantityDelivered(), AD_CMPNY);

            LocalInvCosting invCosting = null;

            try {

              invCosting = invCostingHome
                  .getByMaxCstDateToLongAndMaxCstLineNumberAndLessThanEqualCstDateAndIiNameAndLocName(
                      arInvoice.getInvDate(), II_NM, LOC_NM, AD_BRNCH, AD_CMPNY);

            } catch (FinderException ex) {

            }

            double COST = arJobOrderLine.getInvItemLocation().getInvItem().getIiUnitCost();

            if (invCosting == null) {


              this.postToInvJo(arJobOrderInvoiceLine, arInvoice.getInvDate(), QTY_SLD,
                  COST * QTY_SLD, -QTY_SLD, -(QTY_SLD * COST), 0d, null, AD_BRNCH, AD_CMPNY);

            } else {


              if (invCosting.getInvItemLocation().getInvItem().getIiCostMethod()
                  .equals("Average")) {


                 double avgCost = ejbII.getInvIiUnitCostByIiNameAndUomName( arJobOrderLine.getInvItemLocation().getInvItem().getIiName(), arJobOrderLine.getInvUnitOfMeasure().getUomName(),  arInvoice.getInvDate(), AD_CMPNY);


                this.postToInvJo(arJobOrderInvoiceLine, arInvoice.getInvDate(), QTY_SLD,
                    avgCost * QTY_SLD, invCosting.getCstRemainingQuantity() - QTY_SLD,
                    invCosting.getCstRemainingValue() - (QTY_SLD * avgCost), 0d, null, AD_BRNCH,
                    AD_CMPNY);


              } else if (invCosting.getInvItemLocation().getInvItem().getIiCostMethod()
                  .equals("FIFO")) {

                double fifoCost = this.getInvFifoCost(invCosting.getCstDate(),
                    invCosting.getInvItemLocation().getIlCode(), QTY_SLD,
                    arJobOrderInvoiceLine.getArJobOrderLine().getJolUnitPrice(), true, AD_BRNCH,
                    AD_CMPNY);

                this.postToInvJo(arJobOrderInvoiceLine, arInvoice.getInvDate(), QTY_SLD,
                    fifoCost * QTY_SLD, invCosting.getCstRemainingQuantity() - QTY_SLD,
                    invCosting.getCstRemainingValue() - (fifoCost * QTY_SLD), 0d, null, AD_BRNCH,
                    AD_CMPNY);

              } else if (invCosting.getInvItemLocation().getInvItem().getIiCostMethod()
                  .equals("Standard")) {

                double standardCost = invCosting.getInvItemLocation().getInvItem().getIiUnitCost();

                this.postToInvJo(arJobOrderInvoiceLine, arInvoice.getInvDate(), QTY_SLD,
                    standardCost * QTY_SLD, invCosting.getCstRemainingQuantity() - QTY_SLD,
                    invCosting.getCstRemainingValue() - (standardCost * QTY_SLD), 0d, null,
                    AD_BRNCH, AD_CMPNY);

              }

            }
          }
        }


      }

      // set invoice post status

      arInvoice.setInvPosted(EJBCommon.TRUE);
      arInvoice.setInvPostedBy(USR_NM);
      arInvoice.setInvDatePosted(EJBCommon.getGcCurrentDateWoTime().getTime());


      // post to gl if necessary

      if (adPreference.getPrfArGlPostingType().equals("AUTO-POST UPON APPROVAL")) {

        // validate if date has no period and period is closed

        LocalGlSetOfBook glJournalSetOfBook = null;

        try {

          glJournalSetOfBook = glSetOfBookHome.findByDate(arInvoice.getInvDate(), AD_CMPNY);

        } catch (FinderException ex) {

          throw new GlJREffectiveDateNoPeriodExistException();

        }

        LocalGlAccountingCalendarValue glAccountingCalendarValue = glAccountingCalendarValueHome
            .findByAcCodeAndDate(glJournalSetOfBook.getGlAccountingCalendar().getAcCode(),
                arInvoice.getInvDate(), AD_CMPNY);


        if (glAccountingCalendarValue.getAcvStatus() == 'N'
            || glAccountingCalendarValue.getAcvStatus() == 'C'
            || glAccountingCalendarValue.getAcvStatus() == 'P') {

          throw new GlJREffectiveDatePeriodClosedException();

        }

        // check if invoice is balance if not check suspense posting

        LocalGlJournalLine glOffsetJournalLine = null;

        Collection arDistributionRecords =
            arDistributionRecordHome.findImportableDrByInvCode(arInvoice.getInvCode(), AD_CMPNY);

        Iterator j = arDistributionRecords.iterator();

        double TOTAL_DEBIT = 0d;
        double TOTAL_CREDIT = 0d;

        while (j.hasNext()) {

          LocalArDistributionRecord arDistributionRecord = (LocalArDistributionRecord) j.next();

          double DR_AMNT = 0d;

          if (arInvoice.getInvCreditMemo() == EJBCommon.FALSE) {

            /*
             * DR_AMNT =
             * this.convertForeignToFunctionalCurrency(arInvoice.getGlFunctionalCurrency().getFcCode
             * (), arInvoice.getGlFunctionalCurrency().getFcName(),
             * arInvoice.getInvConversionDate(), arInvoice.getInvConversionRate(),
             * arDistributionRecord.getDrAmount(), AD_CMPNY);
             */

            DR_AMNT = arDistributionRecord.getDrAmount();
          } else {
            /*
             * DR_AMNT =
             * this.convertForeignToFunctionalCurrency(arCreditedInvoice.getGlFunctionalCurrency().
             * getFcCode(), arCreditedInvoice.getGlFunctionalCurrency().getFcName(),
             * arCreditedInvoice.getInvConversionDate(), arCreditedInvoice.getInvConversionRate(),
             * arDistributionRecord.getDrAmount(), AD_CMPNY);
             * 
             */
            DR_AMNT = arDistributionRecord.getDrAmount();

          }

          if (arDistributionRecord.getDrDebit() == EJBCommon.TRUE) {

            TOTAL_DEBIT += DR_AMNT;

          } else {

            TOTAL_CREDIT += DR_AMNT;

          }

        }

        TOTAL_DEBIT =
            EJBCommon.roundIt(TOTAL_DEBIT, adCompany.getGlFunctionalCurrency().getFcPrecision());
        TOTAL_CREDIT =
            EJBCommon.roundIt(TOTAL_CREDIT, adCompany.getGlFunctionalCurrency().getFcPrecision());

        if (adPreference.getPrfAllowSuspensePosting() == EJBCommon.TRUE
            && TOTAL_DEBIT != TOTAL_CREDIT) {

          LocalGlSuspenseAccount glSuspenseAccount = null;

          try {

            glSuspenseAccount = glSuspenseAccountHome.findByJsNameAndJcName("ACCOUNTS RECEIVABLES",
                arInvoice.getInvCreditMemo() == EJBCommon.FALSE ? "SALES INVOICES" : "CREDIT MEMOS",
                AD_CMPNY);

          } catch (FinderException ex) {

            throw new GlobalJournalNotBalanceException();

          }

          if (TOTAL_DEBIT - TOTAL_CREDIT < 0) {

            glOffsetJournalLine =
                glJournalLineHome.create((short) (arDistributionRecords.size() + 1), EJBCommon.TRUE,
                    TOTAL_CREDIT - TOTAL_DEBIT, "", AD_CMPNY);

          } else {

            glOffsetJournalLine =
                glJournalLineHome.create((short) (arDistributionRecords.size() + 1),
                    EJBCommon.FALSE, TOTAL_DEBIT - TOTAL_CREDIT, "", AD_CMPNY);

          }

          LocalGlChartOfAccount glChartOfAccount = glSuspenseAccount.getGlChartOfAccount();
          // glChartOfAccount.addGlJournalLine(glOffsetJournalLine);
          glOffsetJournalLine.setGlChartOfAccount(glChartOfAccount);

        } else if (adPreference.getPrfAllowSuspensePosting() == EJBCommon.FALSE
            && TOTAL_DEBIT != TOTAL_CREDIT) {

          throw new GlobalJournalNotBalanceException();

        }

        // create journal batch if necessary

        LocalGlJournalBatch glJournalBatch = null;
        java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat("MM/dd/yyyy");

        try {

          if (adPreference.getPrfEnableArInvoiceBatch() == EJBCommon.TRUE) {

            glJournalBatch =
                glJournalBatchHome.findByJbName("JOURNAL IMPORT " + formatter.format(new Date())
                    + " " + arInvoice.getArInvoiceBatch().getIbName(), AD_BRNCH, AD_CMPNY);

          } else {

            glJournalBatch = glJournalBatchHome.findByJbName(
                "JOURNAL IMPORT " + formatter.format(new Date()) + " SALES INVOICES", AD_BRNCH,
                AD_CMPNY);

          }


        } catch (FinderException ex) {
        }

        if (adPreference.getPrfEnableGlJournalBatch() == EJBCommon.TRUE && glJournalBatch == null) {

          if (adPreference.getPrfEnableArInvoiceBatch() == EJBCommon.TRUE) {

            glJournalBatch = glJournalBatchHome.create(
                "JOURNAL IMPORT " + formatter.format(new Date()) + " "
                    + arInvoice.getArInvoiceBatch().getIbName(),
                "JOURNAL IMPORT", "CLOSED", EJBCommon.getGcCurrentDateWoTime().getTime(), USR_NM,
                AD_BRNCH, AD_CMPNY);

          } else {

            glJournalBatch = glJournalBatchHome.create(
                "JOURNAL IMPORT " + formatter.format(new Date()) + " SALES INVOICES",
                "JOURNAL IMPORT", "CLOSED", EJBCommon.getGcCurrentDateWoTime().getTime(), USR_NM,
                AD_BRNCH, AD_CMPNY);

          }


        }

        // create journal entry

        LocalGlJournal glJournal = glJournalHome.create(arInvoice.getInvReferenceNumber(),
            arInvoice.getInvDescription(), arInvoice.getInvDate(), 0.0d, null,
            arInvoice.getInvNumber(), null, 1d, "N/A", null, 'N', EJBCommon.TRUE, EJBCommon.FALSE,
            USR_NM, new Date(), USR_NM, new Date(), null, null, USR_NM,
            EJBCommon.getGcCurrentDateWoTime().getTime(), arInvoice.getArCustomer().getCstTin(),
            arInvoice.getArCustomer().getCstName(), EJBCommon.FALSE, null, AD_BRNCH, AD_CMPNY);

        LocalGlJournalSource glJournalSource =
            glJournalSourceHome.findByJsName("ACCOUNTS RECEIVABLES", AD_CMPNY);
        glJournal.setGlJournalSource(glJournalSource);

        LocalGlFunctionalCurrency glFunctionalCurrency = glFunctionalCurrencyHome
            .findByFcName(adCompany.getGlFunctionalCurrency().getFcName(), AD_CMPNY);
        glJournal.setGlFunctionalCurrency(glFunctionalCurrency);

        LocalGlJournalCategory glJournalCategory = glJournalCategoryHome.findByJcName(
            arInvoice.getInvCreditMemo() == EJBCommon.FALSE ? "SALES INVOICES" : "CREDIT MEMOS",
            AD_CMPNY);
        glJournal.setGlJournalCategory(glJournalCategory);

        if (glJournalBatch != null) {

          glJournal.setGlJournalBatch(glJournalBatch);
        }


        // create journal lines

        j = arDistributionRecords.iterator();

        while (j.hasNext()) {

          LocalArDistributionRecord arDistributionRecord = (LocalArDistributionRecord) j.next();

          double DR_AMNT = 0d;

          if (arInvoice.getInvCreditMemo() == EJBCommon.FALSE) {
            /*
             * DR_AMNT =
             * this.convertForeignToFunctionalCurrency(arInvoice.getGlFunctionalCurrency().getFcCode
             * (), arInvoice.getGlFunctionalCurrency().getFcName(),
             * arInvoice.getInvConversionDate(), arInvoice.getInvConversionRate(),
             * arDistributionRecord.getDrAmount(), AD_CMPNY);
             */
            DR_AMNT = arDistributionRecord.getDrAmount();

          } else {
            /*
             * DR_AMNT =
             * this.convertForeignToFunctionalCurrency(arCreditedInvoice.getGlFunctionalCurrency().
             * getFcCode(), arCreditedInvoice.getGlFunctionalCurrency().getFcName(),
             * arCreditedInvoice.getInvConversionDate(), arCreditedInvoice.getInvConversionRate(),
             * arDistributionRecord.getDrAmount(), AD_CMPNY);
             */
            DR_AMNT = arDistributionRecord.getDrAmount();
          }

          LocalGlJournalLine glJournalLine =
              glJournalLineHome.create(arDistributionRecord.getDrLine(),
                  arDistributionRecord.getDrDebit(), DR_AMNT, "", AD_CMPNY);

          glJournalLine.setGlChartOfAccount(arDistributionRecord.getGlChartOfAccount());

          glJournalLine.setGlJournal(glJournal);
          arDistributionRecord.setDrImported(EJBCommon.TRUE);

          // for FOREX revaluation

          LocalArInvoice arInvoiceTemp =
              arInvoice.getInvCreditMemo() == EJBCommon.FALSE ? arInvoice : arCreditedInvoice;

          if ((arInvoiceTemp.getGlFunctionalCurrency().getFcCode() != adCompany
              .getGlFunctionalCurrency().getFcCode())
              && glJournalLine.getGlChartOfAccount().getGlFunctionalCurrency() != null
              && (glJournalLine.getGlChartOfAccount().getGlFunctionalCurrency().getFcCode()
                  .equals(arInvoiceTemp.getGlFunctionalCurrency().getFcCode()))) {

            double CONVERSION_RATE = 1;

            if (arInvoiceTemp.getInvConversionRate() != 0
                && arInvoiceTemp.getInvConversionRate() != 1) {

              CONVERSION_RATE = arInvoiceTemp.getInvConversionRate();

            } else if (arInvoiceTemp.getInvConversionDate() != null) {

              CONVERSION_RATE = this.getFrRateByFrNameAndFrDate(
                  glJournalLine.getGlChartOfAccount().getGlFunctionalCurrency().getFcName(),
                  glJournal.getJrConversionDate(), AD_CMPNY);

            }

            Collection glForexLedgers = null;

            try {

              glForexLedgers =
                  glForexLedgerHome.findLatestGlFrlByFrlDateAndByCoaCode(arInvoiceTemp.getInvDate(),
                      glJournalLine.getGlChartOfAccount().getCoaCode(), AD_CMPNY);

            } catch (FinderException ex) {

            }

            LocalGlForexLedger glForexLedger =
                (glForexLedgers.isEmpty() || glForexLedgers == null) ? null
                    : (LocalGlForexLedger) glForexLedgers.iterator().next();

            int FRL_LN = (glForexLedger != null
                && glForexLedger.getFrlDate().compareTo(arInvoiceTemp.getInvDate()) == 0)
                    ? glForexLedger.getFrlLine().intValue() + 1
                    : 1;

            // compute balance
            double COA_FRX_BLNC = glForexLedger == null ? 0 : glForexLedger.getFrlBalance();
            double FRL_AMNT = arDistributionRecord.getDrAmount();

            if (glJournalLine.getGlChartOfAccount().getCoaAccountType().equalsIgnoreCase("ASSET"))
              FRL_AMNT =
                  (glJournalLine.getJlDebit() == EJBCommon.TRUE ? FRL_AMNT : (-1 * FRL_AMNT));
            else
              FRL_AMNT =
                  (glJournalLine.getJlDebit() == EJBCommon.TRUE ? (-1 * FRL_AMNT) : FRL_AMNT);

            COA_FRX_BLNC = COA_FRX_BLNC + FRL_AMNT;

            glForexLedger = glForexLedgerHome.create(arInvoiceTemp.getInvDate(),
                new Integer(FRL_LN), "SI", FRL_AMNT, CONVERSION_RATE, COA_FRX_BLNC, 0d, AD_CMPNY);

            // glJournalLine.getGlChartOfAccount().addGlForexLedger(glForexLedger);
            glForexLedger.setGlChartOfAccount(glJournalLine.getGlChartOfAccount());

            // propagate balances
            try {

              glForexLedgers = glForexLedgerHome.findByGreaterThanFrlDateAndCoaCode(
                  glForexLedger.getFrlDate(), glForexLedger.getGlChartOfAccount().getCoaCode(),
                  glForexLedger.getFrlAdCompany());

            } catch (FinderException ex) {

            }

            Iterator itrFrl = glForexLedgers.iterator();

            while (itrFrl.hasNext()) {

              glForexLedger = (LocalGlForexLedger) itrFrl.next();
              FRL_AMNT = arDistributionRecord.getDrAmount();

              if (glJournalLine.getGlChartOfAccount().getCoaAccountType().equalsIgnoreCase("ASSET"))
                FRL_AMNT =
                    (glJournalLine.getJlDebit() == EJBCommon.TRUE ? FRL_AMNT : (-1 * FRL_AMNT));
              else
                FRL_AMNT =
                    (glJournalLine.getJlDebit() == EJBCommon.TRUE ? (-1 * FRL_AMNT) : FRL_AMNT);

              glForexLedger.setFrlBalance(glForexLedger.getFrlBalance() + FRL_AMNT);

            }

          }

        }

        if (glOffsetJournalLine != null) {

          // glJournal.addGlJournalLine(glOffsetJournalLine);
          glOffsetJournalLine.setGlJournal(glJournal);

        }

        // post journal to gl

        Collection glJournalLines = glJournal.getGlJournalLines();

        Iterator i = glJournalLines.iterator();

        while (i.hasNext()) {

          LocalGlJournalLine glJournalLine = (LocalGlJournalLine) i.next();

          // post current to current acv

          this.postToGl(glAccountingCalendarValue, glJournalLine.getGlChartOfAccount(), true,
              glJournalLine.getJlDebit(), glJournalLine.getJlAmount(), AD_CMPNY);


          // post to subsequent acvs (propagate)

          Collection glSubsequentAccountingCalendarValues =
              glAccountingCalendarValueHome.findSubsequentAcvByAcCodeAndAcvPeriodNumber(
                  glJournalSetOfBook.getGlAccountingCalendar().getAcCode(),
                  glAccountingCalendarValue.getAcvPeriodNumber(), AD_CMPNY);

          Iterator acvsIter = glSubsequentAccountingCalendarValues.iterator();

          while (acvsIter.hasNext()) {

            LocalGlAccountingCalendarValue glSubsequentAccountingCalendarValue =
                (LocalGlAccountingCalendarValue) acvsIter.next();

            this.postToGl(glSubsequentAccountingCalendarValue, glJournalLine.getGlChartOfAccount(),
                false, glJournalLine.getJlDebit(), glJournalLine.getJlAmount(), AD_CMPNY);

          }

          // post to subsequent years if necessary

          Collection glSubsequentSetOfBooks = glSetOfBookHome.findSubsequentSobByAcYear(
              glJournalSetOfBook.getGlAccountingCalendar().getAcYear(), AD_CMPNY);

          if (!glSubsequentSetOfBooks.isEmpty() && glJournalSetOfBook.getSobYearEndClosed() == 1) {

            adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
            LocalGlChartOfAccount glRetainedEarningsAccount = glChartOfAccountHome
                .findByCoaAccountNumber(adCompany.getCmpRetainedEarnings(), AD_CMPNY);

            Iterator sobIter = glSubsequentSetOfBooks.iterator();

            while (sobIter.hasNext()) {

              LocalGlSetOfBook glSubsequentSetOfBook = (LocalGlSetOfBook) sobIter.next();

              String ACCOUNT_TYPE = glJournalLine.getGlChartOfAccount().getCoaAccountType();

              // post to subsequent acvs of subsequent set of book(propagate)

              Collection glAccountingCalendarValues = glAccountingCalendarValueHome.findByAcCode(
                  glSubsequentSetOfBook.getGlAccountingCalendar().getAcCode(), AD_CMPNY);

              Iterator acvIter = glAccountingCalendarValues.iterator();

              while (acvIter.hasNext()) {

                LocalGlAccountingCalendarValue glSubsequentAccountingCalendarValue =
                    (LocalGlAccountingCalendarValue) acvIter.next();

                if (ACCOUNT_TYPE.equals("ASSET") || ACCOUNT_TYPE.equals("LIABILITY")
                    || ACCOUNT_TYPE.equals("OWNERS EQUITY")) {

                  this.postToGl(glSubsequentAccountingCalendarValue,
                      glJournalLine.getGlChartOfAccount(), false, glJournalLine.getJlDebit(),
                      glJournalLine.getJlAmount(), AD_CMPNY);

                } else { // revenue & expense

                  this.postToGl(glSubsequentAccountingCalendarValue, glRetainedEarningsAccount,
                      false, glJournalLine.getJlDebit(), glJournalLine.getJlAmount(), AD_CMPNY);

                }

              }

              if (glSubsequentSetOfBook.getSobYearEndClosed() == 0)
                break;

            }

          }

        }

      }

    } catch (GlJREffectiveDateNoPeriodExistException ex) {

      getSessionContext().setRollbackOnly();
      throw ex;

    } catch (GlJREffectiveDatePeriodClosedException ex) {

      getSessionContext().setRollbackOnly();
      throw ex;

    } catch (GlobalJournalNotBalanceException ex) {

      getSessionContext().setRollbackOnly();
      throw ex;

    } catch (GlobalRecordAlreadyDeletedException ex) {

      getSessionContext().setRollbackOnly();
      throw ex;

    } catch (GlobalTransactionAlreadyPostedException ex) {

      getSessionContext().setRollbackOnly();
      throw ex;

    } catch (GlobalTransactionAlreadyVoidException ex) {

      getSessionContext().setRollbackOnly();
      throw ex;

   

    } catch (AdPRFCoaGlVarianceAccountNotFoundException ex) {

      getSessionContext().setRollbackOnly();
      throw ex;

    } catch (GlobalExpiryDateNotFoundException ex) {

      getSessionContext().setRollbackOnly();
      throw ex;

    } catch (Exception ex) {

      Debug.printStackTrace(ex);
      getSessionContext().setRollbackOnly();
      throw new EJBException(ex.getMessage());

    }

  }


  private double getInvFifoCost(
      Date CST_DT, Integer IL_CODE, double CST_QTY, double CST_COST, boolean isAdjustFifo,
      Integer AD_BRNCH, Integer AD_CMPNY
  ) {

    LocalInvCostingHome invCostingHome = null;
    LocalInvItemLocationHome invItemLocationHome = null;

    // Initialize EJB Home

    try {

      invCostingHome = (LocalInvCostingHome) EJBHomeFactory
          .lookUpLocalHome(LocalInvCostingHome.JNDI_NAME, LocalInvCostingHome.class);
      invItemLocationHome = (LocalInvItemLocationHome) EJBHomeFactory
          .lookUpLocalHome(LocalInvItemLocationHome.JNDI_NAME, LocalInvItemLocationHome.class);
    } catch (NamingException ex) {

      throw new EJBException(ex.getMessage());
    }

    try {

      Collection invFifoCostings =
          invCostingHome.findFifoRemainingQuantityByLessThanOrEqualCstDateAndIlCodeAndBrCode(CST_DT,
              IL_CODE, AD_BRNCH, AD_CMPNY);

      if (invFifoCostings.size() > 0) {

        Iterator x = invFifoCostings.iterator();

        if (isAdjustFifo) {

          // executed during POST transaction

          double totalCost = 0d;
          double cost;

          if (CST_QTY < 0) {

            // for negative quantities
            double neededQty = -(CST_QTY);

            while (x.hasNext() && neededQty != 0) {

              LocalInvCosting invFifoCosting = (LocalInvCosting) x.next();

              if (invFifoCosting.getApPurchaseOrderLine() != null
                  || invFifoCosting.getApVoucherLineItem() != null) {
                cost = invFifoCosting.getCstItemCost() / invFifoCosting.getCstQuantityReceived();
              } else if (invFifoCosting.getArInvoiceLineItem() != null) {
                cost = invFifoCosting.getCstCostOfSales() / invFifoCosting.getCstQuantitySold();
              } else if (invFifoCosting.getInvBuildUnbuildAssemblyLine() != null) {
                cost =
                    invFifoCosting.getCstAssemblyCost() / invFifoCosting.getCstAssemblyQuantity();
              } else {
                cost = invFifoCosting.getCstAdjustCost() / invFifoCosting.getCstAdjustQuantity();
              }

              if (neededQty <= invFifoCosting.getCstRemainingLifoQuantity()) {

                invFifoCosting.setCstRemainingLifoQuantity(
                    invFifoCosting.getCstRemainingLifoQuantity() - neededQty);
                totalCost += (neededQty * cost);
                neededQty = 0d;
              } else {

                neededQty -= invFifoCosting.getCstRemainingLifoQuantity();
                totalCost += (invFifoCosting.getCstRemainingLifoQuantity() * cost);
                invFifoCosting.setCstRemainingLifoQuantity(0);
              }
            }

            // if needed qty is not yet satisfied but no more quantities to fetch, get the default
            // cost
            if (neededQty != 0) {

              LocalInvItemLocation invItemLocation = invItemLocationHome.findByPrimaryKey(IL_CODE);
              totalCost += (neededQty * invItemLocation.getInvItem().getIiUnitCost());
            }

            cost = totalCost / -CST_QTY;
          }

          else {

            // for positive quantities
            cost = CST_COST;
          }
          return cost;
        }

        else {

          // executed during ENTRY transaction

          LocalInvCosting invFifoCosting = (LocalInvCosting) x.next();

          if (invFifoCosting.getApPurchaseOrderLine() != null
              || invFifoCosting.getApVoucherLineItem() != null) {
            return EJBCommon.roundIt(
                invFifoCosting.getCstItemCost() / invFifoCosting.getCstQuantityReceived(),
                this.getGlFcPrecisionUnit(AD_CMPNY));
          } else if (invFifoCosting.getArInvoiceLineItem() != null) {
            return EJBCommon.roundIt(
                invFifoCosting.getCstCostOfSales() / invFifoCosting.getCstQuantitySold(),
                this.getGlFcPrecisionUnit(AD_CMPNY));
          } else if (invFifoCosting.getInvBuildUnbuildAssemblyLine() != null) {
            return EJBCommon.roundIt(
                invFifoCosting.getCstAssemblyCost() / invFifoCosting.getCstAssemblyQuantity(),
                this.getGlFcPrecisionUnit(AD_CMPNY));
          } else {
            return EJBCommon.roundIt(
                invFifoCosting.getCstAdjustCost() / invFifoCosting.getCstAdjustQuantity(),
                this.getGlFcPrecisionUnit(AD_CMPNY));
          }
        }
      } else {

        // most applicable in 1st entries of data
        LocalInvItemLocation invItemLocation = invItemLocationHome.findByPrimaryKey(IL_CODE);
        return invItemLocation.getInvItem().getIiUnitCost();
      }

    } catch (Exception ex) {
      Debug.printStackTrace(ex);
      throw new EJBException(ex.getMessage());
    }
  }


  /**
   * @ejb:interface-method view-type="remote"
   * @jboss:method-attributes read-only="true"
   **/
  public ArrayList getAdLvReportTypeAll(
      String invoiceType, Integer AD_CMPNY
  ) {

    Debug.print("GlRepMonthlyVatDeclarationControllerBean getAdLvReportTypeAll");

    LocalAdLookUpValueHome adLookUpValueHome = null;

    ArrayList list = new ArrayList();

    // Initialize EJB Home

    try {

      adLookUpValueHome = (LocalAdLookUpValueHome) EJBHomeFactory
          .lookUpLocalHome(LocalAdLookUpValueHome.JNDI_NAME, LocalAdLookUpValueHome.class);

    } catch (NamingException ex) {

      throw new EJBException(ex.getMessage());

    }

    try {

      String lookUpName = "";
      if (invoiceType.equals("ITEM")) {
        lookUpName = "AR INVOICE ITEM PRINT TYPE";
      }
      if (invoiceType.equals("MEMO LINE")) {
        lookUpName = "AR INVOICE MEMO LINE PRINT TYPE";
      }
      if (invoiceType.equals("SO MATCHED")) {
        lookUpName = "AR INVOICE SO MATCHED PRINT TYPE";
      }


      Collection adLookUpValues = adLookUpValueHome.findByLuName(lookUpName, AD_CMPNY);

      Iterator i = adLookUpValues.iterator();

      while (i.hasNext()) {

        LocalAdLookUpValue adLookUpValue = (LocalAdLookUpValue) i.next();

        list.add(adLookUpValue.getLvName());

      }

      return list;

    } catch (Exception ex) {

      Debug.printStackTrace(ex);
      throw new EJBException(ex.getMessage());

    }

  }

  private void post(
      Date INV_DT, double INV_AMNT, LocalArCustomer arCustomer, Integer AD_CMPNY
  ) {

    Debug.print("ArInvoiceEntryControllerBean post");

    LocalArCustomerBalanceHome arCustomerBalanceHome = null;

    // Initialize EJB Home

    try {

      arCustomerBalanceHome = (LocalArCustomerBalanceHome) EJBHomeFactory
          .lookUpLocalHome(LocalArCustomerBalanceHome.JNDI_NAME, LocalArCustomerBalanceHome.class);

    } catch (NamingException ex) {

      getSessionContext().setRollbackOnly();
      throw new EJBException(ex.getMessage());

    }

    try {

      // find customer balance before or equal invoice date

      Collection arCustomerBalances =
          arCustomerBalanceHome.findByBeforeOrEqualInvDateAndCstCustomerCode(INV_DT,
              arCustomer.getCstCustomerCode(), AD_CMPNY);

      if (!arCustomerBalances.isEmpty()) {

        // get last invoice

        ArrayList arCustomerBalanceList = new ArrayList(arCustomerBalances);

        LocalArCustomerBalance arCustomerBalance =
            (LocalArCustomerBalance) arCustomerBalanceList.get(arCustomerBalanceList.size() - 1);

        if (arCustomerBalance.getCbDate().before(INV_DT)) {

          // create new balance

          LocalArCustomerBalance apNewCustomerBalance = arCustomerBalanceHome.create(INV_DT,
              arCustomerBalance.getCbBalance() + INV_AMNT, AD_CMPNY);

          // arCustomer.addArCustomerBalance(apNewCustomerBalance);
          apNewCustomerBalance.setArCustomer(arCustomer);

        } else { // equals to invoice date

          arCustomerBalance.setCbBalance(arCustomerBalance.getCbBalance() + INV_AMNT);

        }

      } else {

        // create new balance

        LocalArCustomerBalance apNewCustomerBalance =
            arCustomerBalanceHome.create(INV_DT, INV_AMNT, AD_CMPNY);

        // arCustomer.addArCustomerBalance(apNewCustomerBalance);
        apNewCustomerBalance.setArCustomer(arCustomer);

      }

      // propagate to subsequent balances if necessary

      arCustomerBalances = arCustomerBalanceHome.findByAfterInvDateAndCstCustomerCode(INV_DT,
          arCustomer.getCstCustomerCode(), AD_CMPNY);

      Iterator i = arCustomerBalances.iterator();

      while (i.hasNext()) {

        LocalArCustomerBalance arCustomerBalance = (LocalArCustomerBalance) i.next();

        arCustomerBalance.setCbBalance(arCustomerBalance.getCbBalance() + INV_AMNT);

      }

    } catch (Exception ex) {

      Debug.printStackTrace(ex);
      getSessionContext().setRollbackOnly();
      throw new EJBException(ex.getMessage());

    }

  }

  private double convertForeignToFunctionalCurrency(
      Integer FC_CODE, String FC_NM, Date CONVERSION_DATE, double CONVERSION_RATE, double AMOUNT,
      Integer AD_CMPNY
  ) {

    Debug.print("ArInvoiceEntryControllerBean convertForeignToFunctionalCurrency");


    LocalGlFunctionalCurrencyRateHome glFunctionalCurrencyRateHome = null;
    LocalAdCompanyHome adCompanyHome = null;

    LocalAdCompany adCompany = null;

    // Initialize EJB Homes

    try {

      glFunctionalCurrencyRateHome =
          (LocalGlFunctionalCurrencyRateHome) EJBHomeFactory.lookUpLocalHome(
              LocalGlFunctionalCurrencyRateHome.JNDI_NAME, LocalGlFunctionalCurrencyRateHome.class);
      adCompanyHome = (LocalAdCompanyHome) EJBHomeFactory
          .lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);

    } catch (NamingException ex) {

      throw new EJBException(ex.getMessage());

    }

    // get company and extended precision

    try {

      adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);

    } catch (Exception ex) {

      throw new EJBException(ex.getMessage());

    }


    // Convert to functional currency if necessary

    if (CONVERSION_RATE != 1 && CONVERSION_RATE != 0) {

      AMOUNT = AMOUNT / CONVERSION_RATE;

    }

    return EJBCommon.roundIt(AMOUNT, adCompany.getGlFunctionalCurrency().getFcPrecision());

  }

  private LocalArInvoiceLineItem addArIliEntry(
      ArModInvoiceLineItemDetails mdetails, LocalArInvoice arInvoice,
      LocalInvItemLocation invItemLocation, Integer AD_CMPNY
  ) throws GlobalMiscInfoIsRequiredException {

    Debug.print("ArInvoiceEntryControllerBean addArIliEntry");

    LocalArInvoiceLineItemHome arInvoiceLineItemHome = null;
    LocalInvItemLocationHome invItemLocationHome = null;
    LocalInvUnitOfMeasureHome invUnitOfMeasureHome = null;

    // Initialize EJB Home

    try {

      arInvoiceLineItemHome = (LocalArInvoiceLineItemHome) EJBHomeFactory
          .lookUpLocalHome(LocalArInvoiceLineItemHome.JNDI_NAME, LocalArInvoiceLineItemHome.class);
      invItemLocationHome = (LocalInvItemLocationHome) EJBHomeFactory
          .lookUpLocalHome(LocalInvItemLocationHome.JNDI_NAME, LocalInvItemLocationHome.class);
      invUnitOfMeasureHome = (LocalInvUnitOfMeasureHome) EJBHomeFactory
          .lookUpLocalHome(LocalInvUnitOfMeasureHome.JNDI_NAME, LocalInvUnitOfMeasureHome.class);

    } catch (NamingException ex) {

      throw new EJBException(ex.getMessage());

    }

    try {

      short precisionUnit = this.getGlFcPrecisionUnit(AD_CMPNY);

      double ILI_AMNT = 0d;
      double ILI_TAX_AMNT = 0d;

      LocalArTaxCode arTaxCode = arInvoice.getArTaxCode();

      // calculate net amount
      ILI_AMNT = this.calculateIliNetAmount(mdetails, arTaxCode.getTcRate(), arTaxCode.getTcType(),
          precisionUnit);

      // calculate tax
      ILI_TAX_AMNT = this.calculateIliTaxAmount(mdetails, arTaxCode.getTcRate(),
          arTaxCode.getTcType(), ILI_AMNT, precisionUnit);


      LocalArInvoiceLineItem arInvoiceLineItem = arInvoiceLineItemHome.create(mdetails.getIliLine(),
          mdetails.getIliQuantity(), mdetails.getIliUnitPrice(), ILI_AMNT, ILI_TAX_AMNT,
          mdetails.getIliEnableAutoBuild(), mdetails.getIliDiscount1(), mdetails.getIliDiscount2(),
          mdetails.getIliDiscount3(), mdetails.getIliDiscount4(), mdetails.getIliTotalDiscount(),
          mdetails.getIliTax(), AD_CMPNY);

      arInvoiceLineItem.setArInvoice(arInvoice);
      arInvoiceLineItem.setInvItemLocation(invItemLocation);
      LocalInvUnitOfMeasure invUnitOfMeasure =
          invUnitOfMeasureHome.findByUomName(mdetails.getIliUomName(), AD_CMPNY);
      arInvoiceLineItem.setInvUnitOfMeasure(invUnitOfMeasure);


      if (arInvoiceLineItem.getInvItemLocation().getInvItem().getIiTraceMisc() == 1) {

        System.out.println("trace misc count: " + mdetails.getiIliTagList().size());

        System.out.println("TRACK MISC = 1");
        // NEW CODE
        this.createInvTagList(arInvoiceLineItem, mdetails.getiIliTagList(), AD_CMPNY);

        // OLD CODE TRACE MISC (BACKWARD COMPATIBILITY)
        if (mdetails.getIliMisc() == null || mdetails.getIliMisc() == "") {
          // throw new GlobalMiscInfoIsRequiredException();
        } else {
          System.out.println("addArIliEntry mdetails.getIliMisc(): " + mdetails.getIliMisc());
          int qty2Prpgt = 0;



          String miscList2Prpgt = this.checkExpiryDates(mdetails.getIliMisc(), qty2Prpgt, "False");
          if (miscList2Prpgt != "Error") {
            arInvoiceLineItem.setIliMisc(mdetails.getIliMisc());
          } else {
            // throw new GlobalMiscInfoIsRequiredException();
          }

        }
      } else {
        arInvoiceLineItem.setIliMisc(mdetails.getIliMisc());
      }
      //

      // NEW CODE TRACE MISC



      return arInvoiceLineItem;

    } catch (GlobalMiscInfoIsRequiredException ex) {

      getSessionContext().setRollbackOnly();
      throw ex;
    } catch (Exception ex) {

      Debug.printStackTrace(ex);
      getSessionContext().setRollbackOnly();
      throw new EJBException(ex.getMessage());

    }

  }

  public String checkExpiryDates(
      String misc, double qty, String reverse
  ) throws Exception {
    // ActionErrors errors = new ActionErrors();
    Debug.print("ApReceivingItemControllerBean getExpiryDates");
    System.out.println("misc: " + misc);

    String separator = "";
    if (reverse == "False") {
      separator = "$";
    } else {
      separator = " ";
    }

    // Remove first $ character
    misc = misc.substring(1);
    System.out.println("misc: " + misc);
    // Counter
    int start = 0;
    int nextIndex = misc.indexOf(separator, start);
    int length = nextIndex - start;

    String miscList = new String();
    String miscList2 = "";

    for (int x = 0; x < qty; x++) {

      // Date
      start = nextIndex + 1;
      nextIndex = misc.indexOf(separator, start);
      length = nextIndex - start;
      String g = misc.substring(start, start + length);
      System.out.println("g: " + g);
      System.out.println("g length: " + g.length());
      if (g.length() != 0) {
        if (g != null || g != "" || g != "null") {
          if (g.contains("null")) {
            miscList2 = "Error";
          } else {
            miscList = miscList + "$" + g;
          }
        } else {
          miscList2 = "Error";
        }

        System.out.println("miscList G: " + miscList);
      } else {
        System.out.println("KABOOM");
        miscList2 = "Error";
      }
    }
    System.out.println("miscList2 :" + miscList2);
    if (miscList2 == "") {
      miscList = miscList + "$";
    } else {
      miscList = miscList2;
    }

    System.out.println("miscList :" + miscList);
    return (miscList);
  }


  private double calculateIliNetAmount(
      ArModInvoiceLineItemDetails mdetails, double tcRate, String tcType, short precisionUnit
  ) {

    double amount = 0d;

    if (mdetails.getIliTax() == EJBCommon.FALSE) {
      amount = mdetails.getIliAmount();

      return amount;
    }

    if (tcType.equals("INCLUSIVE")) {

      amount = EJBCommon.roundIt(mdetails.getIliAmount() / (1 + (tcRate / 100)), precisionUnit);

    } else {

      // tax exclusive, none, zero rated or exempt

      amount = mdetails.getIliAmount();

    }

    return amount;

  }

  private double calculateIliTaxAmount(
      ArModInvoiceLineItemDetails mdetails, double tcRate, String tcType, double amount,
      short precisionUnit
  ) {

    double taxAmount = 0d;

    if (mdetails.getIliTax() == EJBCommon.FALSE) {
      return taxAmount;
    }


    if (!tcType.equals("NONE") && !tcType.equals("EXEMPT")) {


      if (tcType.equals("INCLUSIVE")) {

        taxAmount = EJBCommon.roundIt(mdetails.getIliAmount() - amount, precisionUnit);

      } else if (tcType.equals("EXCLUSIVE")) {

        taxAmount = EJBCommon.roundIt(mdetails.getIliAmount() * tcRate / 100, precisionUnit);

      } else {

        // tax none zero-rated or exempt

      }

    }

    return taxAmount;

  }

  private void addArDrIliEntry(
      short DR_LN, String DR_CLSS, byte DR_DBT, double DR_AMNT, Integer COA_CODE,
      LocalArInvoice arInvoice, Integer AD_BRNCH, Integer AD_CMPNY
  ) throws GlobalBranchAccountNumberInvalidException {

    Debug.print("ArInvoiceEntryControllerBean addArDrIliEntry");

    LocalArDistributionRecordHome arDistributionRecordHome = null;
    LocalGlChartOfAccountHome glChartOfAccountHome = null;
    LocalAdCompanyHome adCompanyHome = null;


    // Initialize EJB Home

    try {

      arDistributionRecordHome = (LocalArDistributionRecordHome) EJBHomeFactory.lookUpLocalHome(
          LocalArDistributionRecordHome.JNDI_NAME, LocalArDistributionRecordHome.class);
      glChartOfAccountHome = (LocalGlChartOfAccountHome) EJBHomeFactory
          .lookUpLocalHome(LocalGlChartOfAccountHome.JNDI_NAME, LocalGlChartOfAccountHome.class);
      adCompanyHome = (LocalAdCompanyHome) EJBHomeFactory
          .lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);

    } catch (NamingException ex) {

      throw new EJBException(ex.getMessage());

    }

    try {

      // get company

      LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
      System.out.println("COA_CODE is: " + COA_CODE + "  AD_BRNCH is: " + AD_BRNCH);
      LocalGlChartOfAccount glChartOfAccount =
          glChartOfAccountHome.findByCoaCodeAndBranchCode(COA_CODE, AD_BRNCH, AD_CMPNY);

      // create distribution record

      LocalArDistributionRecord arDistributionRecord =
          arDistributionRecordHome.create(DR_LN, DR_CLSS, DR_DBT,
              EJBCommon.roundIt(DR_AMNT, adCompany.getGlFunctionalCurrency().getFcPrecision()),
              EJBCommon.FALSE, EJBCommon.FALSE, AD_CMPNY);

      // arInvoice.addArDistributionRecord(arDistributionRecord);
      arDistributionRecord.setArInvoice(arInvoice);
      // glChartOfAccount.addArDistributionRecord(arDistributionRecord);
      arDistributionRecord.setGlChartOfAccount(glChartOfAccount);

    } catch (FinderException ex) {

      throw new GlobalBranchAccountNumberInvalidException();

    } catch (Exception ex) {

      Debug.printStackTrace(ex);
      getSessionContext().setRollbackOnly();
      throw new EJBException(ex.getMessage());

    }

  }


  private void addArDrIliEntry(
      short DR_LN, String DR_CLSS, byte DR_DBT, double DR_AMNT, Integer COA_CODE,
      LocalArReceipt arReceipt, Integer AD_BRNCH, Integer AD_CMPNY
  ) throws GlobalBranchAccountNumberInvalidException {

    Debug.print("ArMiscReceiptEntryControllerBean addArDrIliEntry");

    LocalArDistributionRecordHome arDistributionRecordHome = null;
    LocalGlChartOfAccountHome glChartOfAccountHome = null;
    LocalAdCompanyHome adCompanyHome = null;


    // Initialize EJB Home

    try {

      arDistributionRecordHome = (LocalArDistributionRecordHome) EJBHomeFactory.lookUpLocalHome(
          LocalArDistributionRecordHome.JNDI_NAME, LocalArDistributionRecordHome.class);
      glChartOfAccountHome = (LocalGlChartOfAccountHome) EJBHomeFactory
          .lookUpLocalHome(LocalGlChartOfAccountHome.JNDI_NAME, LocalGlChartOfAccountHome.class);
      adCompanyHome = (LocalAdCompanyHome) EJBHomeFactory
          .lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);

    } catch (NamingException ex) {

      throw new EJBException(ex.getMessage());

    }

    try {

      // get company

      LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);

      LocalGlChartOfAccount glChartOfAccount =
          glChartOfAccountHome.findByCoaCodeAndBranchCode(COA_CODE, AD_BRNCH, AD_CMPNY);

      // create distribution record

      LocalArDistributionRecord arDistributionRecord =
          arDistributionRecordHome.create(DR_LN, DR_CLSS, DR_DBT,
              EJBCommon.roundIt(DR_AMNT, adCompany.getGlFunctionalCurrency().getFcPrecision()),
              EJBCommon.FALSE, EJBCommon.FALSE, AD_CMPNY);

      // arReceipt.addArDistributionRecord(arDistributionRecord);
      arDistributionRecord.setArReceipt(arReceipt);
      // glChartOfAccount.addArDistributionRecord(arDistributionRecord);
      arDistributionRecord.setGlChartOfAccount(glChartOfAccount);

    } catch (FinderException ex) {

      // throw new GlobalBranchAccountNumberInvalidException(ex.getMessage(),DR_LN);
      throw new GlobalBranchAccountNumberInvalidException("" + DR_LN);

    } catch (Exception ex) {

      Debug.printStackTrace(ex);
      getSessionContext().setRollbackOnly();
      throw new EJBException(ex.getMessage());

    }

  }

  private LocalArSalesOrderInvoiceLine addArSolEntry(
      ArModSalesOrderLineDetails mdetails, LocalArInvoice arInvoice, Integer AD_CMPNY
  ) {

    Debug.print("ArInvoiceEntryControllerBean addArSolEntry");

    LocalArInvoiceLineItemHome arInvoiceLineItemHome = null;
    LocalInvItemLocationHome invItemLocationHome = null;
    LocalInvUnitOfMeasureHome invUnitOfMeasureHome = null;
    LocalArSalesOrderLineHome arSalesOrderLineHome = null;
    LocalArSalesOrderInvoiceLineHome arSalesOrderInvoiceLineHome = null;

    // Initialize EJB Home

    try {

      arInvoiceLineItemHome = (LocalArInvoiceLineItemHome) EJBHomeFactory
          .lookUpLocalHome(LocalArInvoiceLineItemHome.JNDI_NAME, LocalArInvoiceLineItemHome.class);
      invItemLocationHome = (LocalInvItemLocationHome) EJBHomeFactory
          .lookUpLocalHome(LocalInvItemLocationHome.JNDI_NAME, LocalInvItemLocationHome.class);
      invUnitOfMeasureHome = (LocalInvUnitOfMeasureHome) EJBHomeFactory
          .lookUpLocalHome(LocalInvUnitOfMeasureHome.JNDI_NAME, LocalInvUnitOfMeasureHome.class);
      arSalesOrderLineHome = (LocalArSalesOrderLineHome) EJBHomeFactory
          .lookUpLocalHome(LocalArSalesOrderLineHome.JNDI_NAME, LocalArSalesOrderLineHome.class);
      arSalesOrderInvoiceLineHome =
          (LocalArSalesOrderInvoiceLineHome) EJBHomeFactory.lookUpLocalHome(
              LocalArSalesOrderInvoiceLineHome.JNDI_NAME, LocalArSalesOrderInvoiceLineHome.class);

    } catch (NamingException ex) {

      throw new EJBException(ex.getMessage());

    }

    try {

      short precisionUnit = this.getGlFcPrecisionUnit(AD_CMPNY);

      double ILI_AMNT = 0d;
      double ILI_TAX_AMNT = 0d;

      // calculate net amount

      LocalArTaxCode arTaxCode = arInvoice.getArTaxCode();

      if (arTaxCode.getTcType().equals("INCLUSIVE")) {

        ILI_AMNT = EJBCommon.roundIt(mdetails.getSolAmount() / (1 + (arTaxCode.getTcRate() / 100)),
            precisionUnit);

      } else {

        // tax exclusive, none, zero rated or exempt

        ILI_AMNT = mdetails.getSolAmount();

      }

      // calculate tax

      if (!arTaxCode.getTcType().equals("NONE") && !arTaxCode.getTcType().equals("EXEMPT")) {


        if (arTaxCode.getTcType().equals("INCLUSIVE")) {

          ILI_TAX_AMNT = EJBCommon.roundIt(mdetails.getSolAmount() - ILI_AMNT, precisionUnit);

        } else if (arTaxCode.getTcType().equals("EXCLUSIVE")) {

          ILI_TAX_AMNT = EJBCommon.roundIt(mdetails.getSolAmount() * arTaxCode.getTcRate() / 100,
              precisionUnit);

        } else {

          // tax none zero-rated or exempt

        }

      }

      if (mdetails.getSolTax() == EJBCommon.FALSE) {
        ILI_AMNT = mdetails.getSolAmount();
        ILI_TAX_AMNT = 0;
      }

      LocalArSalesOrderInvoiceLine arSalesOrderInvoiceLine = arSalesOrderInvoiceLineHome.create(
          mdetails.getSolQuantityDelivered(), ILI_AMNT, ILI_TAX_AMNT, mdetails.getSolDiscount1(),
          mdetails.getSolDiscount2(), mdetails.getSolDiscount3(), mdetails.getSolDiscount4(),
          mdetails.getSolTotalDiscount(), mdetails.getSolMisc(), mdetails.getSolTax(), AD_CMPNY);



      arInvoice.addArSalesOrderInvoiceLine(arSalesOrderInvoiceLine);

      LocalArSalesOrderLine arSalesOrderLine =
          arSalesOrderLineHome.findByPrimaryKey(mdetails.getSolCode());
      arSalesOrderLine.addArSalesOrderInvoiceLine(arSalesOrderInvoiceLine);


      ArrayList tagList = new ArrayList();
      if (arSalesOrderInvoiceLine.getArSalesOrderLine().getInvItemLocation().getInvItem()
          .getIiTraceMisc() == 1) {
        System.out.println("SO pasok");
        tagList = mdetails.getSolTagList();
        this.createInvTagList(arSalesOrderInvoiceLine, tagList, AD_CMPNY);
      }



      return arSalesOrderInvoiceLine;

    } catch (Exception ex) {

      Debug.printStackTrace(ex);
      getSessionContext().setRollbackOnly();
      throw new EJBException(ex.getMessage());

    }

  }


  private void postToGl(
      LocalGlAccountingCalendarValue glAccountingCalendarValue,
      LocalGlChartOfAccount glChartOfAccount, boolean isCurrentAcv, byte isDebit, double JL_AMNT,
      Integer AD_CMPNY
  ) {

    Debug.print("ArInvoiceEntryControllerBean postToGl");

    LocalGlChartOfAccountBalanceHome glChartOfAccountBalanceHome = null;
    LocalAdCompanyHome adCompanyHome = null;


    // Initialize EJB Home

    try {

      glChartOfAccountBalanceHome =
          (LocalGlChartOfAccountBalanceHome) EJBHomeFactory.lookUpLocalHome(
              LocalGlChartOfAccountBalanceHome.JNDI_NAME, LocalGlChartOfAccountBalanceHome.class);
      adCompanyHome = (LocalAdCompanyHome) EJBHomeFactory
          .lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);


    } catch (NamingException ex) {

      throw new EJBException(ex.getMessage());

    }

    try {

      LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);

      LocalGlChartOfAccountBalance glChartOfAccountBalance =
          glChartOfAccountBalanceHome.findByAcvCodeAndCoaCode(
              glAccountingCalendarValue.getAcvCode(), glChartOfAccount.getCoaCode(), AD_CMPNY);

      String ACCOUNT_TYPE = glChartOfAccount.getCoaAccountType();
      short FC_EXTNDD_PRCSN = adCompany.getGlFunctionalCurrency().getFcPrecision();



      if (((ACCOUNT_TYPE.equals("ASSET") || ACCOUNT_TYPE.equals("EXPENSE"))
          && isDebit == EJBCommon.TRUE)
          || (!ACCOUNT_TYPE.equals("ASSET") && !ACCOUNT_TYPE.equals("EXPENSE")
              && isDebit == EJBCommon.FALSE)) {

        glChartOfAccountBalance.setCoabEndingBalance(EJBCommon
            .roundIt(glChartOfAccountBalance.getCoabEndingBalance() + JL_AMNT, FC_EXTNDD_PRCSN));

        if (!isCurrentAcv) {

          glChartOfAccountBalance.setCoabBeginningBalance(EJBCommon.roundIt(
              glChartOfAccountBalance.getCoabBeginningBalance() + JL_AMNT, FC_EXTNDD_PRCSN));

        }


      } else {

        glChartOfAccountBalance.setCoabEndingBalance(EJBCommon
            .roundIt(glChartOfAccountBalance.getCoabEndingBalance() - JL_AMNT, FC_EXTNDD_PRCSN));

        if (!isCurrentAcv) {

          glChartOfAccountBalance.setCoabBeginningBalance(EJBCommon.roundIt(
              glChartOfAccountBalance.getCoabBeginningBalance() - JL_AMNT, FC_EXTNDD_PRCSN));

        }

      }

      if (isCurrentAcv) {

        if (isDebit == EJBCommon.TRUE) {

          glChartOfAccountBalance.setCoabTotalDebit(EJBCommon
              .roundIt(glChartOfAccountBalance.getCoabTotalDebit() + JL_AMNT, FC_EXTNDD_PRCSN));

        } else {

          glChartOfAccountBalance.setCoabTotalCredit(EJBCommon
              .roundIt(glChartOfAccountBalance.getCoabTotalCredit() + JL_AMNT, FC_EXTNDD_PRCSN));
        }

      }

    } catch (Exception ex) {

      Debug.printStackTrace(ex);
      throw new EJBException(ex.getMessage());

    }


  }



  private LocalArJobOrderInvoiceLine addArJolEntry(
      ArModJobOrderLineDetails mdetails, LocalArInvoice arInvoice, Integer AD_CMPNY
  ) {

    Debug.print("ArInvoiceEntryControllerBean addArJolEntry");

    LocalArInvoiceLineItemHome arInvoiceLineItemHome = null;
    LocalInvItemLocationHome invItemLocationHome = null;
    LocalInvUnitOfMeasureHome invUnitOfMeasureHome = null;
    LocalArJobOrderLineHome arJobOrderLineHome = null;
    LocalArJobOrderInvoiceLineHome arJobOrderInvoiceLineHome = null;

    // Initialize EJB Home

    try {

      arInvoiceLineItemHome = (LocalArInvoiceLineItemHome) EJBHomeFactory
          .lookUpLocalHome(LocalArInvoiceLineItemHome.JNDI_NAME, LocalArInvoiceLineItemHome.class);
      invItemLocationHome = (LocalInvItemLocationHome) EJBHomeFactory
          .lookUpLocalHome(LocalInvItemLocationHome.JNDI_NAME, LocalInvItemLocationHome.class);
      invUnitOfMeasureHome = (LocalInvUnitOfMeasureHome) EJBHomeFactory
          .lookUpLocalHome(LocalInvUnitOfMeasureHome.JNDI_NAME, LocalInvUnitOfMeasureHome.class);
      arJobOrderLineHome = (LocalArJobOrderLineHome) EJBHomeFactory
          .lookUpLocalHome(LocalArJobOrderLineHome.JNDI_NAME, LocalArJobOrderLineHome.class);
      arJobOrderInvoiceLineHome = (LocalArJobOrderInvoiceLineHome) EJBHomeFactory.lookUpLocalHome(
          LocalArJobOrderInvoiceLineHome.JNDI_NAME, LocalArJobOrderInvoiceLineHome.class);

    } catch (NamingException ex) {

      throw new EJBException(ex.getMessage());

    }

    try {

      short precisionUnit = this.getGlFcPrecisionUnit(AD_CMPNY);

      double ILI_AMNT = 0d;
      double ILI_TAX_AMNT = 0d;

      // calculate net amount

      LocalArTaxCode arTaxCode = arInvoice.getArTaxCode();

      if (arTaxCode.getTcType().equals("INCLUSIVE")) {

        ILI_AMNT = EJBCommon.roundIt(mdetails.getJolAmount() / (1 + (arTaxCode.getTcRate() / 100)),
            precisionUnit);

      } else {

        // tax exclusive, none, zero rated or exempt

        ILI_AMNT = mdetails.getJolAmount();

      }

      // calculate tax

      if (!arTaxCode.getTcType().equals("NONE") && !arTaxCode.getTcType().equals("EXEMPT")) {


        if (arTaxCode.getTcType().equals("INCLUSIVE")) {

          ILI_TAX_AMNT = EJBCommon.roundIt(mdetails.getJolAmount() - ILI_AMNT, precisionUnit);

        } else if (arTaxCode.getTcType().equals("EXCLUSIVE")) {

          ILI_TAX_AMNT = EJBCommon.roundIt(mdetails.getJolAmount() * arTaxCode.getTcRate() / 100,
              precisionUnit);

        } else {

          // tax none zero-rated or exempt

        }

      }

      if (mdetails.getJolTax() == EJBCommon.FALSE) {
        ILI_AMNT = mdetails.getJolAmount();
        ILI_TAX_AMNT = 0;
      }


      LocalArJobOrderInvoiceLine arJobOrderInvoiceLine = arJobOrderInvoiceLineHome.create(
          mdetails.getJolQuantityDelivered(), ILI_AMNT, ILI_TAX_AMNT, mdetails.getJolDiscount1(),
          mdetails.getJolDiscount2(), mdetails.getJolDiscount3(), mdetails.getJolDiscount4(),
          mdetails.getJolTotalDiscount(), mdetails.getJolMisc(), mdetails.getJolTax(), AD_CMPNY);



      arJobOrderInvoiceLine.setArInvoice(arInvoice);


      LocalArJobOrderLine arJobOrderLine =
          arJobOrderLineHome.findByPrimaryKey(mdetails.getJolCode());

      arJobOrderInvoiceLine.setArJobOrderLine(arJobOrderLine);

      ArrayList tagList = new ArrayList();
      if (arJobOrderInvoiceLine.getArJobOrderLine().getInvItemLocation().getInvItem()
          .getIiTraceMisc() == 1) {
        System.out.println("JO pasok");
        tagList = mdetails.getJolTagList();
        this.createInvTagList(arJobOrderInvoiceLine, tagList, AD_CMPNY);
      }



      return arJobOrderInvoiceLine;

    } catch (Exception ex) {

      Debug.printStackTrace(ex);
      getSessionContext().setRollbackOnly();
      throw new EJBException(ex.getMessage());

    }

  }



  private void postToInv(
      LocalArInvoiceLineItem arInvoiceLineItem, Date CST_DT, double CST_QTY_SLD,
      double CST_CST_OF_SLS, double CST_RMNNG_QTY, double CST_RMNNG_VL, double CST_VRNC_VL,
      String USR_NM, Integer AD_BRNCH, Integer AD_CMPNY
  ) throws AdPRFCoaGlVarianceAccountNotFoundException, GlobalExpiryDateNotFoundException {

    Debug.print("ArInvoiceEntryControllerBean postToInv");

    LocalInvCostingHome invCostingHome = null;
    LocalAdPreferenceHome adPreferenceHome = null;
    LocalAdCompanyHome adCompanyHome = null;
    LocalInvAdjustmentLineHome invAdjustmentLineHome = null;

    // Initialize EJB Home

    try {

      invCostingHome = (LocalInvCostingHome) EJBHomeFactory
          .lookUpLocalHome(LocalInvCostingHome.JNDI_NAME, LocalInvCostingHome.class);
      adPreferenceHome = (LocalAdPreferenceHome) EJBHomeFactory
          .lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);
      adCompanyHome = (LocalAdCompanyHome) EJBHomeFactory
          .lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
      invAdjustmentLineHome = (LocalInvAdjustmentLineHome) EJBHomeFactory
          .lookUpLocalHome(LocalInvAdjustmentLineHome.JNDI_NAME, LocalInvAdjustmentLineHome.class);

    } catch (NamingException ex) {

      throw new EJBException(ex.getMessage());

    }

    try {

      LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);
      LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
      LocalInvItemLocation invItemLocation = arInvoiceLineItem.getInvItemLocation();
      int CST_LN_NMBR = 0;

      CST_QTY_SLD = EJBCommon.roundIt(CST_QTY_SLD, adPreference.getPrfInvQuantityPrecisionUnit());
      CST_CST_OF_SLS =
          EJBCommon.roundIt(CST_CST_OF_SLS, adCompany.getGlFunctionalCurrency().getFcPrecision());
      CST_RMNNG_QTY =
          EJBCommon.roundIt(CST_RMNNG_QTY, adPreference.getPrfInvQuantityPrecisionUnit());
      CST_RMNNG_VL =
          EJBCommon.roundIt(CST_RMNNG_VL, adCompany.getGlFunctionalCurrency().getFcPrecision());
          
         
      if (CST_QTY_SLD > 0 && arInvoiceLineItem.getIliEnableAutoBuild() == EJBCommon.FALSE) {

        invItemLocation
            .setIlCommittedQuantity(invItemLocation.getIlCommittedQuantity() - CST_QTY_SLD);

      }

      try {

        // generate line number

        LocalInvCosting invCurrentCosting =
            invCostingHome.getByMaxCstLineNumberAndCstDateToLongAndIiNameAndLocName(
                CST_DT.getTime(), invItemLocation.getInvItem().getIiName(),
                invItemLocation.getInvLocation().getLocName(), AD_BRNCH, AD_CMPNY);
                
                
                
                
                
                
                
                
        CST_LN_NMBR = invCurrentCosting.getCstLineNumber() + 1;

      } catch (FinderException ex) {

        CST_LN_NMBR = 1;

      }

      if (adPreference.getPrfArAllowPriorDate() == EJBCommon.FALSE) {
        // void subsequent cost variance adjustments
        Collection invAdjustmentLines =
            invAdjustmentLineHome.findUnvoidAndIsCostVarianceGreaterThanAdjDateAndIlCodeAndBrCode(
                CST_DT, invItemLocation.getIlCode(), AD_BRNCH, AD_CMPNY);
        Iterator i = invAdjustmentLines.iterator();

        while (i.hasNext()) {

          LocalInvAdjustmentLine invAdjustmentLine = (LocalInvAdjustmentLine) i.next();
          this.voidInvAdjustment(invAdjustmentLine.getInvAdjustment(), AD_BRNCH, AD_CMPNY);

        }
      }

      String prevExpiryDates = "";
      String miscListPrpgt = "";
      double qtyPrpgt = 0;
      double qtyPrpgt2 = 0;
      try {
        LocalInvCosting prevCst = invCostingHome
            .getByMaxCstDateToLongAndMaxCstLineNumberAndLessThanEqualCstDateAndRemainingQuantityNotEqualToZeroAndIlCode(
                CST_DT, invItemLocation.getIlCode(), AD_BRNCH, AD_CMPNY);
        Debug.print("ArInvoicePostControllerBean postToInv C");
        prevExpiryDates = prevCst.getCstExpiryDate();
        qtyPrpgt = prevCst.getCstRemainingQuantity();

        if (prevExpiryDates == null) {
          prevExpiryDates = "";
        }

      } catch (Exception ex) {
        System.out.println("prevExpiryDates CATCH: " + prevExpiryDates);
        prevExpiryDates = "";
      }

      // create costing
      LocalInvCosting invCosting = invCostingHome.create(CST_DT, CST_DT.getTime(), CST_LN_NMBR, 0d,
          0d, 0d, 0d, 0d, 0d, CST_QTY_SLD, CST_CST_OF_SLS, CST_RMNNG_QTY, CST_RMNNG_VL, 0d, 0d, 0d,
          AD_BRNCH, AD_CMPNY);
      // invItemLocation.addInvCosting(invCosting);
      invCosting.setInvItemLocation(invItemLocation);
      invCosting.setArInvoiceLineItem(arInvoiceLineItem);

      // Get Latest Expiry Dates
      String check = "";
      if (arInvoiceLineItem.getInvItemLocation().getInvItem().getIiTraceMisc() != 0) {
        if (prevExpiryDates != null && prevExpiryDates != "" && prevExpiryDates.length() != 0) {
          if (arInvoiceLineItem.getIliMisc() != null && arInvoiceLineItem.getIliMisc() != ""
              && arInvoiceLineItem.getIliMisc().length() != 0) {
            int qty2Prpgt =
                Integer.parseInt(this.getQuantityExpiryDates(arInvoiceLineItem.getIliMisc()));

            String miscList2Prpgt =
                this.propagateExpiryDates(arInvoiceLineItem.getIliMisc(), qty2Prpgt, "False");
            ArrayList miscList = this.expiryDates(arInvoiceLineItem.getIliMisc(), qty2Prpgt);
            String propagateMiscPrpgt = "";
            String ret = "";

            System.out.println("CST_ST_QTY Before Trans: " + CST_QTY_SLD);
            // ArrayList miscList2 = null;
            if (CST_QTY_SLD < 0) {
              prevExpiryDates = prevExpiryDates.substring(1);
              propagateMiscPrpgt = miscList2Prpgt + prevExpiryDates;
            } else {
              Iterator mi = miscList.iterator();
              propagateMiscPrpgt = prevExpiryDates;
              ret = propagateMiscPrpgt;
              String Checker = "";
              while (mi.hasNext()) {
                String miscStr = (String) mi.next();

                Integer qTest = this.checkExpiryDates(ret + "fin$");
                ArrayList miscList2 =
                    this.expiryDates("$" + ret, Double.parseDouble(qTest.toString()));
                Iterator m2 = miscList2.iterator();
                ret = "";
                String ret2 = "false";
                int a = 0;

                while (m2.hasNext()) {
                  String miscStr2 = (String) m2.next();

                  if (ret2 == "1st") {
                    ret2 = "false";
                  }

                  System.out.println("miscStr2: " + miscStr2);
                  System.out.println("miscStr: " + miscStr);
                  if (miscStr2.trim().equals(miscStr.trim())) {
                    if (a == 0) {
                      a = 1;
                      ret2 = "1st";
                      Checker = "true";
                    } else {
                      a = a + 1;
                      ret2 = "true";
                    }
                  }

                  if (!miscStr2.trim().equals(miscStr.trim()) || a > 1) {
                    if ((ret2 != "1st") || (ret2 == "false") || (ret2 == "true")) {
                      if (miscStr2 != "") {
                        miscStr2 = "$" + miscStr2.trim();
                        ret = ret + miscStr2;
                        qtyPrpgt2++;
                        ret2 = "false";
                      }
                    }
                  }

                }
                ret = ret + "$";
                System.out.println("qtyPrpgt: " + qtyPrpgt);
                if (qtyPrpgt2 == 0) {
                  qtyPrpgt2 = qtyPrpgt;
                }
                qtyPrpgt = qtyPrpgt - 1;
              }
              // propagateMiscPrpgt = propagateMiscPrpgt.replace(" ", "$");
              propagateMiscPrpgt = ret;
              System.out.println("propagateMiscPrpgt: " + propagateMiscPrpgt);
              // propagateMiscPrpgt = this.propagateExpiryDates(propagateMiscPrpgt, a, "True");
              if (Checker.equals("true")) {
                // invCosting.setCstExpiryDate(ret);
                System.out.println("check: " + check);
              } else {
                System.out.println("exA");
                // throw new
                // GlobalExpiryDateNotFoundException(arInvoiceLineItem.getInvItemLocation().getInvItem().getIiName());

              }

            }
            invCosting.setCstExpiryDate(propagateMiscPrpgt);

          } else {
            invCosting.setCstExpiryDate(prevExpiryDates);
          }
        } else {
          if (arInvoiceLineItem.getIliMisc() != null && arInvoiceLineItem.getIliMisc() != ""
              && arInvoiceLineItem.getIliMisc().length() != 0) {
            int initialQty =
                Integer.parseInt(this.getQuantityExpiryDates(arInvoiceLineItem.getIliMisc()));
            String initialPrpgt =
                this.propagateExpiryDates(arInvoiceLineItem.getIliMisc(), initialQty, "False");

            invCosting.setCstExpiryDate(initialPrpgt);
          } else {
            invCosting.setCstExpiryDate(prevExpiryDates);
            System.out.println("prevExpiryDates");
          }

        }
      }



      // if cost variance is not 0, generate cost variance for the transaction
      if (CST_VRNC_VL != 0 && adPreference.getPrfArAllowPriorDate() == EJBCommon.FALSE) {

        this.generateCostVariance(invCosting.getInvItemLocation(), CST_VRNC_VL,
            "ARCR" + arInvoiceLineItem.getArReceipt().getRctNumber(),
            arInvoiceLineItem.getArReceipt().getRctDescription(),
            arInvoiceLineItem.getArReceipt().getRctDate(), USR_NM, AD_BRNCH, AD_CMPNY);

      }

      // propagate balance if necessary
      Collection invCostings = invCostingHome.findByGreaterThanCstDateAndIiNameAndLocName(CST_DT,
          invItemLocation.getInvItem().getIiName(), invItemLocation.getInvLocation().getLocName(),
          AD_BRNCH, AD_CMPNY);

      Iterator i = invCostings.iterator();
      String miscList = "";
      ArrayList miscList2 = null;


      System.out.println("miscList Propagate:" + miscList);
      String propagateMisc = "";
      String ret = "";

      System.out.println("CST_ST_QTY: " + CST_QTY_SLD);
      while (i.hasNext()) {
        String Checker = "";
        String Checker2 = "";

        LocalInvCosting invPropagatedCosting = (LocalInvCosting) i.next();

        invPropagatedCosting
            .setCstRemainingQuantity(invPropagatedCosting.getCstRemainingQuantity() - CST_QTY_SLD);
        invPropagatedCosting
            .setCstRemainingValue(invPropagatedCosting.getCstRemainingValue() - CST_CST_OF_SLS);

        if (arInvoiceLineItem.getInvItemLocation().getInvItem().getIiTraceMisc() != 0) {
          if (arInvoiceLineItem.getIliMisc() != null && arInvoiceLineItem.getIliMisc() != ""
              && arInvoiceLineItem.getIliMisc().length() != 0) {
            System.out.println("BAGO ANG MALI: " + arInvoiceLineItem.getIliMisc());

            double qty =
                Double.parseDouble(this.getQuantityExpiryDates(arInvoiceLineItem.getIliMisc()));
            // double qty2 = this.checkExpiryDates2(arInvoiceLineItem.getIliMisc());
            miscList = this.propagateExpiryDates(arInvoiceLineItem.getIliMisc(), qty, "False");
            miscList2 = this.expiryDates(arInvoiceLineItem.getIliMisc(), qty);

            System.out.println("invAdjustmentLine.getAlMisc(): " + arInvoiceLineItem.getIliMisc());
            System.out.println("getAlAdjustQuantity(): " + arInvoiceLineItem.getIliQuantity());

            if (arInvoiceLineItem.getIliQuantity() < 0) {
              Iterator mi = miscList2.iterator();

              propagateMisc = invPropagatedCosting.getCstExpiryDate();
              ret = invPropagatedCosting.getCstExpiryDate();
              while (mi.hasNext()) {
                String miscStr = (String) mi.next();

                Integer qTest = this.checkExpiryDates(ret + "fin$");
                ArrayList miscList3 =
                    this.expiryDates("$" + ret, Double.parseDouble(qTest.toString()));

                // ArrayList miscList3 = this.expiryDates("$" + ret, qtyPrpgt);
                System.out.println("ret: " + ret);
                Iterator m2 = miscList3.iterator();
                ret = "";
                String ret2 = "false";
                int a = 0;
                while (m2.hasNext()) {
                  String miscStr2 = (String) m2.next();

                  if (ret2 == "1st") {
                    ret2 = "false";
                  }
                  System.out.println("2 miscStr: " + miscStr);
                  System.out.println("2 miscStr2: " + miscStr2);
                  if (miscStr2.equals(miscStr)) {
                    if (a == 0) {
                      a = 1;
                      ret2 = "1st";
                      Checker2 = "true";
                    } else {
                      a = a + 1;
                      ret2 = "true";
                    }
                  }
                  System.out.println("Checker: " + Checker2);
                  if (!miscStr2.equals(miscStr) || a > 1) {
                    if ((ret2 != "1st") || (ret2 == "false") || (ret2 == "true")) {
                      if (miscStr2 != "") {
                        miscStr2 = "$" + miscStr2;
                        ret = ret + miscStr2;
                        ret2 = "false";
                      }
                    }
                  }

                }
                if (Checker2 != "true") {
                  // throw new
                  // GlobalExpiryDateNotFoundException(arInvoiceLineItem.getInvItemLocation().getInvItem().getIiName());
                } else {
                  System.out.println("TAE");
                }

                ret = ret + "$";
                qtyPrpgt = qtyPrpgt - 1;
              }
            }
          }


          if (arInvoiceLineItem.getIliMisc() != null && arInvoiceLineItem.getIliMisc() != ""
              && arInvoiceLineItem.getIliMisc().length() != 0) {



            if (CST_QTY_SLD < 0) {
              // miscList = miscList.substring(1);
              // propagateMisc = invPropagatedCosting.getCstExpiryDate() + miscList;
              propagateMisc = miscList + invPropagatedCosting.getCstExpiryDate().substring(1,
                  invPropagatedCosting.getCstExpiryDate().length());
              System.out.println("propagateMiscPrpgt : " + propagateMisc);

            } else {
              Iterator mi = miscList2.iterator();

              propagateMisc = prevExpiryDates;
              ret = propagateMisc;
              while (mi.hasNext()) {
                String miscStr = (String) mi.next();
                System.out.println("ret123: " + ret);
                System.out.println("qtyPrpgt123: " + qtyPrpgt);
                System.out.println("qtyPrpgt2: " + qtyPrpgt2);
                if (qtyPrpgt <= 0) {
                  qtyPrpgt = qtyPrpgt2;
                }

                Integer qTest = this.checkExpiryDates(ret + "fin$");
                ArrayList miscList3 =
                    this.expiryDates("$" + ret, Double.parseDouble(qTest.toString()));
                Iterator m2 = miscList3.iterator();
                ret = "";
                String ret2 = "false";
                int a = 0;
                while (m2.hasNext()) {
                  String miscStr2 = (String) m2.next();

                  if (ret2 == "1st") {
                    ret2 = "false";
                  }

                  System.out.println("miscStr2: " + miscStr2);
                  System.out.println("miscStr: " + miscStr);
                  if (miscStr2.trim().equals(miscStr.trim())) {
                    if (a == 0) {
                      a = 1;
                      ret2 = "1st";
                      Checker = "true";
                    } else {
                      a = a + 1;
                      ret2 = "true";
                    }
                  }

                  if (!miscStr2.trim().equals(miscStr.trim()) || a > 1) {
                    if ((ret2 != "1st") || (ret2 == "false") || (ret2 == "true")) {
                      if (miscStr2 != "") {
                        miscStr2 = "$" + miscStr2.trim();
                        ret = ret + miscStr2;
                        ret2 = "false";
                      }
                    }
                  }

                }
                ret = ret + "$";
                qtyPrpgt = qtyPrpgt - 1;
              }
              propagateMisc = ret;
              System.out.println("propagateMiscPrpgt: " + propagateMisc);

              if (Checker == "true") {
                // invPropagatedCosting.setCstExpiryDate(propagateMisc);
                System.out.println("Yes");
              } else {
                System.out.println("ex1");
                // throw new
                // GlobalExpiryDateNotFoundException(invBranchStockTransferLine.getInvItemLocation().getInvItem().getIiName());

              }
            }

            invPropagatedCosting.setCstExpiryDate(propagateMisc);
          } else {
            invPropagatedCosting.setCstExpiryDate(prevExpiryDates);
          }

        }


      }

      // regenerate cost variance
      if (adPreference.getPrfArAllowPriorDate() == EJBCommon.FALSE) {
        this.regenerateCostVariance(invCostings, invCosting, AD_BRNCH, AD_CMPNY);
      }

    } catch (AdPRFCoaGlVarianceAccountNotFoundException ex) {

      throw ex;

    } catch (GlobalExpiryDateNotFoundException ex) {
      throw ex;
    } catch (Exception ex) {

      Debug.printStackTrace(ex);
      getSessionContext().setRollbackOnly();
      throw new EJBException(ex.getMessage());

    }

  }

  public static int checkExpiryDates(
      String misc
  ) throws Exception {

    String separator = "$";

    // Remove first $ character
    misc = misc.substring(1);
    // System.out.println("misc: " + misc);
    // Counter
    int start = 0;
    int nextIndex = misc.indexOf(separator, start);
    int length = nextIndex - start;
    int numberExpry = 0;
    String miscList = new String();
    String miscList2 = "";
    String g = "";
    try {
      while (g != "fin") {
        // Date
        start = nextIndex + 1;
        nextIndex = misc.indexOf(separator, start);
        length = nextIndex - start;
        g = misc.substring(start, start + length);
        if (g.length() != 0) {
          if (g != null || g != "" || g != "null") {
            if (g.contains("null")) {
              miscList2 = "Error";
            } else {
              miscList = miscList + "$" + g;
              numberExpry++;
            }
          } else {
            miscList2 = "Error";
          }

        } else {
          miscList2 = "Error";
        }
      }
    } catch (Exception e) {

    }

    if (miscList2 == "") {
      miscList = miscList + "$";
    } else {
      miscList = miscList2;
    }

    return (numberExpry);
  }

  public String propagateExpiryDates(
      String misc, double qty, String reverse
  ) throws Exception {
    // ActionErrors errors = new ActionErrors();
    Debug.print("ApReceivingItemControllerBean getExpiryDates");
    System.out.println("misc: " + misc);
    String miscList = new String();
    try {
      String separator = "";
      if (reverse == "False") {
        separator = "$";
      } else {
        separator = " ";
      }

      // Remove first $ character
      misc = misc.substring(1);
      System.out.println("misc: " + misc);
      // Counter
      int start = 0;
      int nextIndex = misc.indexOf(separator, start);
      int length = nextIndex - start;



      for (int x = 0; x < qty; x++) {

        // Date
        start = nextIndex + 1;
        nextIndex = misc.indexOf(separator, start);
        length = nextIndex - start;
        String g = misc.substring(start, start + length);
        System.out.println("g: " + g);
        System.out.println("g length: " + g.length());
        if (g.length() != 0) {
          miscList = miscList + "$" + g;
          System.out.println("miscList G: " + miscList);
        }
      }

      miscList = miscList + "$";
    } catch (Exception e) {
      miscList = "";
    }

    System.out.println("miscList :" + miscList);
    return (miscList);
  }

  private ArrayList expiryDates(
      String misc, double qty
  ) throws Exception {
    Debug.print("ApReceivingItemControllerBean getExpiryDates");
    ArrayList miscList = new ArrayList();
    try {
      System.out.println("misc: " + misc);
      String separator = "$";


      // Remove first $ character
      misc = misc.substring(1);

      // Counter
      int start = 0;
      int nextIndex = misc.indexOf(separator, start);
      int length = nextIndex - start;

      System.out.println("qty" + qty);


      for (int x = 0; x < qty; x++) {

        // Date
        start = nextIndex + 1;
        nextIndex = misc.indexOf(separator, start);
        length = nextIndex - start;
        System.out.println("x" + x);
        String checker = misc.substring(start, start + length);
        System.out.println("checker" + checker);
        if (checker.length() != 0 || checker != "null") {
          miscList.add(checker);
        } else {
          miscList.add("null");
        }
      }
    } catch (Exception e) {

      // miscList = "";
    }



    System.out.println("miscList :" + miscList);
    return miscList;
  }

  public String getQuantityExpiryDates(
      String qntty
  ) {
    String separator = "$";
    String y = "";
    try {
      // Remove first $ character
      qntty = qntty.substring(1);

      // Counter
      int start = 0;
      int nextIndex = qntty.indexOf(separator, start);
      int length = nextIndex - start;

      y = (qntty.substring(start, start + length));
      System.out.println("Y " + y);
    } catch (Exception e) {
      y = "0";
    }


    return y;
  }

  public String propagateExpiryDates(
      String misc, double qty
  ) throws Exception {
    // ActionErrors errors = new ActionErrors();

    Debug.print("ApReceivingItemControllerBean getExpiryDates");

    String separator = "$";
    String miscList = new String();
    // Remove first $ character
    try {
      misc = misc.substring(1);

      // Counter
      int start = 0;
      int nextIndex = misc.indexOf(separator, start);
      int length = nextIndex - start;

      System.out.println("qty" + qty);


      for (int x = 0; x < qty; x++) {

        // Date
        start = nextIndex + 1;
        nextIndex = misc.indexOf(separator, start);
        length = nextIndex - start;
        /*
         * SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy"); sdf.setLenient(false);
         */
        try {

          miscList = miscList + "$" + (misc.substring(start, start + length));
        } catch (Exception ex) {

          throw ex;
        }


      }
      miscList = miscList + "$";
    } catch (Exception e) {

      miscList = "";
    }



    System.out.println("miscList :" + miscList);
    return (miscList);
  }

  private void postToBua(
      LocalArInvoiceLineItem arInvoiceLineItem, Date CST_DT, double CST_ASSMBLY_QTY,
      double CST_ASSMBLY_CST, double CST_RMNNG_QTY, double CST_RMNNG_VL, String II_NM,
      String LOC_NM, double CST_VRNC_VL, String USR_NM, Integer AD_BRNCH, Integer AD_CMPNY
  ) throws AdPRFCoaGlVarianceAccountNotFoundException {

    Debug.print("ArInvoiceEntryControllerBean postToBua");

    LocalInvCostingHome invCostingHome = null;
    LocalAdPreferenceHome adPreferenceHome = null;
    LocalAdCompanyHome adCompanyHome = null;
    LocalInvItemLocationHome invItemLocationHome = null;
    LocalInvAdjustmentLineHome invAdjustmentLineHome = null;

    // Initialize EJB Home

    try {

      invCostingHome = (LocalInvCostingHome) EJBHomeFactory
          .lookUpLocalHome(LocalInvCostingHome.JNDI_NAME, LocalInvCostingHome.class);
      adPreferenceHome = (LocalAdPreferenceHome) EJBHomeFactory
          .lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);
      adCompanyHome = (LocalAdCompanyHome) EJBHomeFactory
          .lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
      invItemLocationHome = (LocalInvItemLocationHome) EJBHomeFactory
          .lookUpLocalHome(LocalInvItemLocationHome.JNDI_NAME, LocalInvItemLocationHome.class);
      invAdjustmentLineHome = (LocalInvAdjustmentLineHome) EJBHomeFactory
          .lookUpLocalHome(LocalInvAdjustmentLineHome.JNDI_NAME, LocalInvAdjustmentLineHome.class);

    } catch (NamingException ex) {

      throw new EJBException(ex.getMessage());

    }

    try {

      LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);
      LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
      LocalInvItemLocation invItemLocation =
          invItemLocationHome.findByLocNameAndIiName(LOC_NM, II_NM, AD_CMPNY);
      int CST_LN_NMBR = 0;

      CST_ASSMBLY_QTY =
          EJBCommon.roundIt(CST_ASSMBLY_QTY, adPreference.getPrfInvQuantityPrecisionUnit());
      CST_ASSMBLY_CST =
          EJBCommon.roundIt(CST_ASSMBLY_CST, adCompany.getGlFunctionalCurrency().getFcPrecision());
      CST_RMNNG_QTY =
          EJBCommon.roundIt(CST_RMNNG_QTY, adPreference.getPrfInvQuantityPrecisionUnit());
      CST_RMNNG_VL =
          EJBCommon.roundIt(CST_RMNNG_VL, adCompany.getGlFunctionalCurrency().getFcPrecision());

      if ((CST_ASSMBLY_QTY < 0 && arInvoiceLineItem.getIliEnableAutoBuild() == 0)
          || (CST_ASSMBLY_QTY < 0 && arInvoiceLineItem.getIliEnableAutoBuild() == 1
              && !arInvoiceLineItem.getInvItemLocation().getInvItem()
                  .equals(invItemLocation.getInvItem()))) {

        invItemLocation.setIlCommittedQuantity(
            invItemLocation.getIlCommittedQuantity() - Math.abs(CST_ASSMBLY_QTY));

      }

      try {

        // generate line number

        LocalInvCosting invCurrentCosting =
            invCostingHome.getByMaxCstLineNumberAndCstDateToLongAndIiNameAndLocName(
                CST_DT.getTime(), invItemLocation.getInvItem().getIiName(),
                invItemLocation.getInvLocation().getLocName(), AD_BRNCH, AD_CMPNY);
        CST_LN_NMBR = invCurrentCosting.getCstLineNumber() + 1;

      } catch (FinderException ex) {

        CST_LN_NMBR = 1;

      }

      if (adPreference.getPrfArAllowPriorDate() == EJBCommon.FALSE) {
        // void subsequent cost variance adjustments
        Collection invAdjustmentLines =
            invAdjustmentLineHome.findUnvoidAndIsCostVarianceGreaterThanAdjDateAndIlCodeAndBrCode(
                CST_DT, invItemLocation.getIlCode(), AD_BRNCH, AD_CMPNY);
        Iterator i = invAdjustmentLines.iterator();

        while (i.hasNext()) {

          LocalInvAdjustmentLine invAdjustmentLine = (LocalInvAdjustmentLine) i.next();
          this.voidInvAdjustment(invAdjustmentLine.getInvAdjustment(), AD_BRNCH, AD_CMPNY);

        }
      }

      // create costing
      LocalInvCosting invCosting = invCostingHome.create(CST_DT, CST_DT.getTime(), CST_LN_NMBR, 0d,
          0d, CST_ASSMBLY_QTY, CST_ASSMBLY_CST, 0d, 0d, 0d, 0d, CST_RMNNG_QTY, CST_RMNNG_VL, 0d, 0d,
          CST_ASSMBLY_QTY > 0 ? CST_ASSMBLY_QTY : 0, AD_BRNCH, AD_CMPNY);
      // invItemLocation.addInvCosting(invCosting);
      invCosting.setInvItemLocation(invItemLocation);
      invCosting.setArInvoiceLineItem(arInvoiceLineItem);

      // if cost variance is not 0, generate cost variance for the transaction
      if (CST_VRNC_VL != 0 && adPreference.getPrfArAllowPriorDate() == EJBCommon.FALSE) {

        this.generateCostVariance(invCosting.getInvItemLocation(), CST_VRNC_VL,
            "ARCR" + arInvoiceLineItem.getArReceipt().getRctNumber(),
            arInvoiceLineItem.getArReceipt().getRctDescription(),
            arInvoiceLineItem.getArReceipt().getRctDate(), USR_NM, AD_BRNCH, AD_CMPNY);

      }

      // propagate balance if necessary
      Collection invCostings = invCostingHome.findByGreaterThanCstDateAndIiNameAndLocName(CST_DT,
          invItemLocation.getInvItem().getIiName(), invItemLocation.getInvLocation().getLocName(),
          AD_BRNCH, AD_CMPNY);

      Iterator i = invCostings.iterator();

      while (i.hasNext()) {

        LocalInvCosting invPropagatedCosting = (LocalInvCosting) i.next();

        invPropagatedCosting.setCstRemainingQuantity(
            invPropagatedCosting.getCstRemainingQuantity() + CST_ASSMBLY_QTY);
        invPropagatedCosting
            .setCstRemainingValue(invPropagatedCosting.getCstRemainingValue() + CST_ASSMBLY_CST);

      }

      // regenerate cost variance
      if (adPreference.getPrfArAllowPriorDate() == EJBCommon.FALSE) {
        this.regenerateCostVariance(invCostings, invCosting, AD_BRNCH, AD_CMPNY);
      }

    } catch (AdPRFCoaGlVarianceAccountNotFoundException ex) {

      throw ex;

    } catch (Exception ex) {

      Debug.printStackTrace(ex);
      getSessionContext().setRollbackOnly();
      throw new EJBException(ex.getMessage());

    }

  }

  private void postToInvSo(
      LocalArSalesOrderInvoiceLine arSalesOrderInvoiceLine, Date CST_DT, double CST_QTY_SLD,
      double CST_CST_OF_SLS, double CST_RMNNG_QTY, double CST_RMNNG_VL, double CST_VRNC_VL,
      String USR_NM, Integer AD_BRNCH, Integer AD_CMPNY
  ) throws AdPRFCoaGlVarianceAccountNotFoundException {

    Debug.print("ArInvoiceEntryControllerBean postToInvSo");

    LocalInvCostingHome invCostingHome = null;
    LocalAdPreferenceHome adPreferenceHome = null;
    LocalAdCompanyHome adCompanyHome = null;
    LocalInvAdjustmentLineHome invAdjustmentLineHome = null;

    // Initialize EJB Home

    try {

      invCostingHome = (LocalInvCostingHome) EJBHomeFactory
          .lookUpLocalHome(LocalInvCostingHome.JNDI_NAME, LocalInvCostingHome.class);
      adPreferenceHome = (LocalAdPreferenceHome) EJBHomeFactory
          .lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);
      adCompanyHome = (LocalAdCompanyHome) EJBHomeFactory
          .lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
      invAdjustmentLineHome = (LocalInvAdjustmentLineHome) EJBHomeFactory
          .lookUpLocalHome(LocalInvAdjustmentLineHome.JNDI_NAME, LocalInvAdjustmentLineHome.class);

    } catch (NamingException ex) {

      throw new EJBException(ex.getMessage());

    }

    try {

      LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);
      LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
      LocalArSalesOrderLine arSalesOrderLine = arSalesOrderInvoiceLine.getArSalesOrderLine();
      LocalInvItemLocation invItemLocation = arSalesOrderLine.getInvItemLocation();
      int CST_LN_NMBR = 0;

      CST_QTY_SLD = EJBCommon.roundIt(CST_QTY_SLD, adPreference.getPrfInvQuantityPrecisionUnit());
      CST_CST_OF_SLS =
          EJBCommon.roundIt(CST_CST_OF_SLS, adCompany.getGlFunctionalCurrency().getFcPrecision());
      CST_RMNNG_QTY =
          EJBCommon.roundIt(CST_RMNNG_QTY, adPreference.getPrfInvQuantityPrecisionUnit());
      CST_RMNNG_VL =
          EJBCommon.roundIt(CST_RMNNG_VL, adCompany.getGlFunctionalCurrency().getFcPrecision());

      try {

        // generate line number

        LocalInvCosting invCurrentCosting =
            invCostingHome.getByMaxCstLineNumberAndCstDateToLongAndIiNameAndLocName(
                CST_DT.getTime(), invItemLocation.getInvItem().getIiName(),
                invItemLocation.getInvLocation().getLocName(), AD_BRNCH, AD_CMPNY);
        CST_LN_NMBR = invCurrentCosting.getCstLineNumber() + 1;

      } catch (FinderException ex) {

        CST_LN_NMBR = 1;

      }

      if (adPreference.getPrfArAllowPriorDate() == EJBCommon.FALSE) {
        // void subsequent cost variance adjustments
        Collection invAdjustmentLines =
            invAdjustmentLineHome.findUnvoidAndIsCostVarianceGreaterThanAdjDateAndIlCodeAndBrCode(
                CST_DT, invItemLocation.getIlCode(), AD_BRNCH, AD_CMPNY);
        Iterator i = invAdjustmentLines.iterator();

        while (i.hasNext()) {

          LocalInvAdjustmentLine invAdjustmentLine = (LocalInvAdjustmentLine) i.next();
          this.voidInvAdjustment(invAdjustmentLine.getInvAdjustment(), AD_BRNCH, AD_CMPNY);

        }
      }

      // create costing
      LocalInvCosting invCosting = invCostingHome.create(CST_DT, CST_DT.getTime(), CST_LN_NMBR, 0d,
          0d, 0d, 0d, 0d, 0d, CST_QTY_SLD, CST_CST_OF_SLS, CST_RMNNG_QTY, CST_RMNNG_VL, 0d, 0d, 0d,
          AD_BRNCH, AD_CMPNY);
      // invItemLocation.addInvCosting(invCosting);
      invCosting.setInvItemLocation(invItemLocation);
      invCosting.setArSalesOrderInvoiceLine(arSalesOrderInvoiceLine);

      // if cost variance is not 0, generate cost variance for the transaction
      if (CST_VRNC_VL != 0 && adPreference.getPrfArAllowPriorDate() == EJBCommon.FALSE) {

        this.generateCostVariance(invCosting.getInvItemLocation(), CST_VRNC_VL,
            "ARCM" + arSalesOrderInvoiceLine.getArInvoice().getInvNumber(),
            arSalesOrderInvoiceLine.getArInvoice().getInvDescription(),
            arSalesOrderInvoiceLine.getArInvoice().getInvDate(), USR_NM, AD_BRNCH, AD_CMPNY);

      }

      // propagate balance if necessary
      Collection invCostings = invCostingHome.findByGreaterThanCstDateAndIiNameAndLocName(CST_DT,
          invItemLocation.getInvItem().getIiName(), invItemLocation.getInvLocation().getLocName(),
          AD_BRNCH, AD_CMPNY);

      Iterator i = invCostings.iterator();

      while (i.hasNext()) {

        LocalInvCosting invPropagatedCosting = (LocalInvCosting) i.next();

        invPropagatedCosting
            .setCstRemainingQuantity(invPropagatedCosting.getCstRemainingQuantity() + CST_QTY_SLD);
        invPropagatedCosting
            .setCstRemainingValue(invPropagatedCosting.getCstRemainingValue() + CST_CST_OF_SLS);

      }

      // regenerate cost varaince
      if (adPreference.getPrfArAllowPriorDate() == EJBCommon.FALSE) {
        this.regenerateCostVariance(invCostings, invCosting, AD_BRNCH, AD_CMPNY);
      }

    } catch (AdPRFCoaGlVarianceAccountNotFoundException ex) {

      getSessionContext().setRollbackOnly();
      throw ex;

    } catch (Exception ex) {

      Debug.printStackTrace(ex);
      getSessionContext().setRollbackOnly();
      throw new EJBException(ex.getMessage());

    }

  }



  private void postToInvJo(
      LocalArJobOrderInvoiceLine arJobOrderInvoiceLine, Date CST_DT, double CST_QTY_SLD,
      double CST_CST_OF_SLS, double CST_RMNNG_QTY, double CST_RMNNG_VL, double CST_VRNC_VL,
      String USR_NM, Integer AD_BRNCH, Integer AD_CMPNY
  ) throws AdPRFCoaGlVarianceAccountNotFoundException {

    Debug.print("ArInvoiceEntryControllerBean postToInvJo");

    LocalInvCostingHome invCostingHome = null;
    LocalAdPreferenceHome adPreferenceHome = null;
    LocalAdCompanyHome adCompanyHome = null;
    LocalInvAdjustmentLineHome invAdjustmentLineHome = null;

    // Initialize EJB Home

    try {

      invCostingHome = (LocalInvCostingHome) EJBHomeFactory
          .lookUpLocalHome(LocalInvCostingHome.JNDI_NAME, LocalInvCostingHome.class);
      adPreferenceHome = (LocalAdPreferenceHome) EJBHomeFactory
          .lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);
      adCompanyHome = (LocalAdCompanyHome) EJBHomeFactory
          .lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
      invAdjustmentLineHome = (LocalInvAdjustmentLineHome) EJBHomeFactory
          .lookUpLocalHome(LocalInvAdjustmentLineHome.JNDI_NAME, LocalInvAdjustmentLineHome.class);

    } catch (NamingException ex) {

      throw new EJBException(ex.getMessage());

    }

    try {

      LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);
      LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
      LocalArJobOrderLine arJobOrderLine = arJobOrderInvoiceLine.getArJobOrderLine();
      LocalInvItemLocation invItemLocation = arJobOrderLine.getInvItemLocation();
      int CST_LN_NMBR = 0;

      CST_QTY_SLD = EJBCommon.roundIt(CST_QTY_SLD, adPreference.getPrfInvQuantityPrecisionUnit());
      CST_CST_OF_SLS =
          EJBCommon.roundIt(CST_CST_OF_SLS, adCompany.getGlFunctionalCurrency().getFcPrecision());
      CST_RMNNG_QTY =
          EJBCommon.roundIt(CST_RMNNG_QTY, adPreference.getPrfInvQuantityPrecisionUnit());
      CST_RMNNG_VL =
          EJBCommon.roundIt(CST_RMNNG_VL, adCompany.getGlFunctionalCurrency().getFcPrecision());

      try {

        // generate line number

        LocalInvCosting invCurrentCosting =
            invCostingHome.getByMaxCstLineNumberAndCstDateToLongAndIiNameAndLocName(
                CST_DT.getTime(), invItemLocation.getInvItem().getIiName(),
                invItemLocation.getInvLocation().getLocName(), AD_BRNCH, AD_CMPNY);
        CST_LN_NMBR = invCurrentCosting.getCstLineNumber() + 1;

      } catch (FinderException ex) {

        CST_LN_NMBR = 1;

      }

      if (adPreference.getPrfArAllowPriorDate() == EJBCommon.FALSE) {
        // void subsequent cost variance adjustments
        Collection invAdjustmentLines =
            invAdjustmentLineHome.findUnvoidAndIsCostVarianceGreaterThanAdjDateAndIlCodeAndBrCode(
                CST_DT, invItemLocation.getIlCode(), AD_BRNCH, AD_CMPNY);
        Iterator i = invAdjustmentLines.iterator();

        while (i.hasNext()) {

          LocalInvAdjustmentLine invAdjustmentLine = (LocalInvAdjustmentLine) i.next();
          this.voidInvAdjustment(invAdjustmentLine.getInvAdjustment(), AD_BRNCH, AD_CMPNY);

        }
      }

      // create costing
      LocalInvCosting invCosting = invCostingHome.create(CST_DT, CST_DT.getTime(), CST_LN_NMBR, 0d,
          0d, 0d, 0d, 0d, 0d, CST_QTY_SLD, CST_CST_OF_SLS, CST_RMNNG_QTY, CST_RMNNG_VL, 0d, 0d, 0d,
          AD_BRNCH, AD_CMPNY);
      // invItemLocation.addInvCosting(invCosting);
      invCosting.setInvItemLocation(invItemLocation);
      invCosting.setArJobOrderInvoiceLine(arJobOrderInvoiceLine);

      // if cost variance is not 0, generate cost variance for the transaction
      if (CST_VRNC_VL != 0 && adPreference.getPrfArAllowPriorDate() == EJBCommon.FALSE) {

        this.generateCostVariance(invCosting.getInvItemLocation(), CST_VRNC_VL,
            "ARCM" + arJobOrderInvoiceLine.getArInvoice().getInvNumber(),
            arJobOrderInvoiceLine.getArInvoice().getInvDescription(),
            arJobOrderInvoiceLine.getArInvoice().getInvDate(), USR_NM, AD_BRNCH, AD_CMPNY);

      }

      // propagate balance if necessary
      Collection invCostings = invCostingHome.findByGreaterThanCstDateAndIiNameAndLocName(CST_DT,
          invItemLocation.getInvItem().getIiName(), invItemLocation.getInvLocation().getLocName(),
          AD_BRNCH, AD_CMPNY);

      Iterator i = invCostings.iterator();

      while (i.hasNext()) {

        LocalInvCosting invPropagatedCosting = (LocalInvCosting) i.next();

        invPropagatedCosting
            .setCstRemainingQuantity(invPropagatedCosting.getCstRemainingQuantity() + CST_QTY_SLD);
        invPropagatedCosting
            .setCstRemainingValue(invPropagatedCosting.getCstRemainingValue() + CST_CST_OF_SLS);

      }

      // regenerate cost varaince
      if (adPreference.getPrfArAllowPriorDate() == EJBCommon.FALSE) {
        this.regenerateCostVariance(invCostings, invCosting, AD_BRNCH, AD_CMPNY);
      }

    } catch (AdPRFCoaGlVarianceAccountNotFoundException ex) {

      getSessionContext().setRollbackOnly();
      throw ex;

    } catch (Exception ex) {

      Debug.printStackTrace(ex);
      getSessionContext().setRollbackOnly();
      throw new EJBException(ex.getMessage());

    }

  }



  private double convertByUomFromAndItemAndQuantity(
      LocalInvUnitOfMeasure invFromUnitOfMeasure, LocalInvItem invItem, double QTY_SLD,
      Integer AD_CMPNY
  ) {

    Debug.print("ArInvoiceEntryControllerBean convertByUomFromAndItemAndQuantity");

    LocalAdPreferenceHome adPreferenceHome = null;
    LocalInvUnitOfMeasureConversionHome invUnitOfMeasureConversionHome = null;

    // Initialize EJB Home

    try {

      adPreferenceHome = (LocalAdPreferenceHome) EJBHomeFactory
          .lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);
      invUnitOfMeasureConversionHome = (LocalInvUnitOfMeasureConversionHome) EJBHomeFactory
          .lookUpLocalHome(LocalInvUnitOfMeasureConversionHome.JNDI_NAME,
              LocalInvUnitOfMeasureConversionHome.class);

    } catch (NamingException ex) {

      throw new EJBException(ex.getMessage());

    }

    try {
      Debug.print("ArInvoiceEntryControllerBean convertByUomFromAndItemAndQuantity A");
      LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);

      LocalInvUnitOfMeasureConversion invUnitOfMeasureConversion =
          invUnitOfMeasureConversionHome.findUmcByIiNameAndUomName(invItem.getIiName(),
              invFromUnitOfMeasure.getUomName(), AD_CMPNY);
      LocalInvUnitOfMeasureConversion invDefaultUomConversion =
          invUnitOfMeasureConversionHome.findUmcByIiNameAndUomName(invItem.getIiName(),
              invItem.getInvUnitOfMeasure().getUomName(), AD_CMPNY);
      Debug.print("ArInvoiceEntryControllerBean convertByUomFromAndItemAndQuantity B");
      return EJBCommon.roundIt(
          QTY_SLD * invDefaultUomConversion.getUmcConversionFactor()
              / invUnitOfMeasureConversion.getUmcConversionFactor(),
          adPreference.getPrfInvQuantityPrecisionUnit());


    } catch (Exception ex) {

      Debug.printStackTrace(ex);
      getSessionContext().setRollbackOnly();
      throw new EJBException(ex.getMessage());

    }

  }

  private double computeTotalBalance(
      Integer invoiceCode, String CST_CSTMR_CODE, Integer AD_CMPNY
  ) {

    LocalArCustomerBalanceHome arCustomerBalanceHome = null;
    LocalArInvoiceHome arInvoiceHome = null;
    LocalArReceiptHome arReceiptHome = null;
    LocalArPdcHome arPdcHome = null;

    try {

      arInvoiceHome = (LocalArInvoiceHome) EJBHomeFactory
          .lookUpLocalHome(LocalArInvoiceHome.JNDI_NAME, LocalArInvoiceHome.class);
      arCustomerBalanceHome = (LocalArCustomerBalanceHome) EJBHomeFactory
          .lookUpLocalHome(LocalArCustomerBalanceHome.JNDI_NAME, LocalArCustomerBalanceHome.class);
      arReceiptHome = (LocalArReceiptHome) EJBHomeFactory
          .lookUpLocalHome(LocalArReceiptHome.JNDI_NAME, LocalArReceiptHome.class);
      arPdcHome = (LocalArPdcHome) EJBHomeFactory.lookUpLocalHome(LocalArPdcHome.JNDI_NAME,
          LocalArPdcHome.class);

    } catch (NamingException ex) {

      throw new EJBException(ex.getMessage());

    }

    double customerBalance = 0;

    try {

      // get latest balance

      Collection arCustomerBalances =
          arCustomerBalanceHome.findByCstCustomerCode(CST_CSTMR_CODE, AD_CMPNY);

      if (!arCustomerBalances.isEmpty()) {

        ArrayList customerBalanceList = new ArrayList(arCustomerBalances);

        customerBalance =
            ((LocalArCustomerBalance) customerBalanceList.get(customerBalanceList.size() - 1))
                .getCbBalance();

      }

      // get amount of unposted invoices/credit memos

      Collection arInvoices =
          arInvoiceHome.findUnpostedInvByCstCustomerCode(CST_CSTMR_CODE, AD_CMPNY);

      Iterator arInvIter = arInvoices.iterator();

      while (arInvIter.hasNext()) {

        LocalArInvoice mdetails = (LocalArInvoice) arInvIter.next();

        if (!mdetails.getInvCode().equals(invoiceCode)) {

          if (mdetails.getInvCreditMemo() == EJBCommon.TRUE) {

            customerBalance = customerBalance - mdetails.getInvAmountDue();

          } else {

            customerBalance =
                customerBalance + (mdetails.getInvAmountDue() - mdetails.getInvAmountPaid());

          }

        }

      }

      // get amount of unposted receipts

      Collection arReceipts =
          arReceiptHome.findUnpostedRctByCstCustomerCode(CST_CSTMR_CODE, AD_CMPNY);

      Iterator arRctIter = arReceipts.iterator();

      while (arRctIter.hasNext()) {

        LocalArReceipt arReceipt = (LocalArReceipt) arRctIter.next();

        customerBalance = customerBalance - arReceipt.getRctAmount();

      }

      // get amount of pdc (unposted or posted) type PR findPdcByPdcType("PR", AD_CMPNY)

      Collection arPdcs = arPdcHome.findPdcByPdcType(AD_CMPNY);

      Iterator arPdcIter = arPdcs.iterator();

      while (arPdcIter.hasNext()) {

        LocalArPdc arPdc = (LocalArPdc) arPdcIter.next();

        customerBalance = customerBalance - arPdc.getPdcAmount();

      }

    } catch (FinderException ex) {

    }

    return customerBalance;

  }

  private void regenerateInventoryDr(
      LocalArInvoice arInvoice, Integer AD_BRNCH, Integer AD_CMPNY
  ) throws GlobalInventoryDateException, GlobalBranchAccountNumberInvalidException {

    Debug.print("ArInvoiceEntryControllerBean regenerateInventoryDr");

    LocalArDistributionRecordHome arDistributionRecordHome = null;
    LocalGlChartOfAccountHome glChartOfAccountHome = null;
    LocalInvCostingHome invCostingHome = null;
    LocalInvItemHome invItemHome = null;
    LocalInvItemLocationHome invItemLocationHome = null;
    LocalAdBranchItemLocationHome adBranchItemLocationHome = null;
    LocalAdPreferenceHome adPreferenceHome = null;

    InvRepItemCostingControllerHome homeRIC = null;
    InvRepItemCostingController ejbRIC = null;

    // Initialize EJB Home

    Date txnStartDate = new Date();


    try {

      homeRIC = (InvRepItemCostingControllerHome) com.util.EJBHomeFactory
          .lookUpHome("ejb/InvRepItemCostingControllerEJB", InvRepItemCostingControllerHome.class);

      arDistributionRecordHome = (LocalArDistributionRecordHome) EJBHomeFactory.lookUpLocalHome(
          LocalArDistributionRecordHome.JNDI_NAME, LocalArDistributionRecordHome.class);
      glChartOfAccountHome = (LocalGlChartOfAccountHome) EJBHomeFactory
          .lookUpLocalHome(LocalGlChartOfAccountHome.JNDI_NAME, LocalGlChartOfAccountHome.class);
      invCostingHome = (LocalInvCostingHome) EJBHomeFactory
          .lookUpLocalHome(LocalInvCostingHome.JNDI_NAME, LocalInvCostingHome.class);
      invItemHome = (LocalInvItemHome) EJBHomeFactory.lookUpLocalHome(LocalInvItemHome.JNDI_NAME,
          LocalInvItemHome.class);
      invItemLocationHome = (LocalInvItemLocationHome) EJBHomeFactory
          .lookUpLocalHome(LocalInvItemLocationHome.JNDI_NAME, LocalInvItemLocationHome.class);
      adBranchItemLocationHome = (LocalAdBranchItemLocationHome) EJBHomeFactory.lookUpLocalHome(
          LocalAdBranchItemLocationHome.JNDI_NAME, LocalAdBranchItemLocationHome.class);
      adPreferenceHome = (LocalAdPreferenceHome) EJBHomeFactory
          .lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);


    } catch (NamingException ex) {

      throw new EJBException(ex.getMessage());

    }


    try {
      ejbRIC = homeRIC.create();
    } catch (RemoteException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    } catch (CreateException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }



    try {

      LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);

      // regenerate inventory distribution records

      Collection arDistributionRecords =
          arDistributionRecordHome.findImportableDrByInvCode(arInvoice.getInvCode(), AD_CMPNY);

      Iterator i = arDistributionRecords.iterator();

      while (i.hasNext()) {

        LocalArDistributionRecord arDistributionRecord = (LocalArDistributionRecord) i.next();

        if (arDistributionRecord.getDrClass().equals("COGS")
            || arDistributionRecord.getDrClass().equals("INVENTORY")) {

          i.remove();
          arDistributionRecord.remove();

        }

      }

      Collection arInvoiceLineItems = arInvoice.getArInvoiceLineItems();
      Collection arSalesOrderInvoiceLines = arInvoice.getArSalesOrderInvoiceLines();

      if (arInvoiceLineItems != null && !arInvoiceLineItems.isEmpty()) {

        i = arInvoiceLineItems.iterator();

        while (i.hasNext()) {

          LocalArInvoiceLineItem arInvoiceLineItem = (LocalArInvoiceLineItem) i.next();
          LocalInvItemLocation invItemLocation = arInvoiceLineItem.getInvItemLocation();

          // start date validation

          if (adPreference.getPrfArAllowPriorDate() == EJBCommon.FALSE) {
            Collection invNegTxnCosting =
                invCostingHome.findNegTxnByGreaterThanCstDateAndIiNameAndLocName(
                    arInvoice.getInvDate(), invItemLocation.getInvItem().getIiName(),
                    invItemLocation.getInvLocation().getLocName(), AD_BRNCH, AD_CMPNY);
            if (!invNegTxnCosting.isEmpty())
              throw new GlobalInventoryDateException(invItemLocation.getInvItem().getIiName());
          }

          // add cost of sales distribution and inventory

          double COST = arInvoiceLineItem.getInvItemLocation().getInvItem().getIiUnitCost();

          try {

            LocalInvCosting invCosting = invCostingHome
                .getByMaxCstDateToLongAndMaxCstLineNumberAndLessThanEqualCstDateAndIiNameAndLocName(
                    arInvoice.getInvDate(), invItemLocation.getInvItem().getIiName(),
                    invItemLocation.getInvLocation().getLocName(), AD_BRNCH, AD_CMPNY);
                    
                    
                    
            // check if rmning vl is not zero and rmng qty is 0
            if (invCosting.getCstRemainingQuantity() <= 0
                && invCosting.getCstRemainingValue() <= 0) {

              HashMap criteria = new HashMap();
              criteria.put("itemName", invItemLocation.getInvItem().getIiName());
              criteria.put("location", invItemLocation.getInvLocation().getLocName());

              ArrayList branchList = new ArrayList();

              AdBranchDetails mdetails = new AdBranchDetails();
              mdetails.setBrCode(AD_BRNCH);
              branchList.add(mdetails);

              ejbRIC.executeInvFixItemCosting(criteria, branchList, AD_CMPNY);
              invCosting = invCostingHome
                  .getByMaxCstDateToLongAndMaxCstLineNumberAndLessThanEqualCstDateAndIiNameAndLocName(
                      arInvoice.getInvDate(), invItemLocation.getInvItem().getIiName(),
                      invItemLocation.getInvLocation().getLocName(), AD_BRNCH, AD_CMPNY);
            }

            if (invCosting.getInvItemLocation().getInvItem().getIiCostMethod().equals("Average"))

              COST = invCosting.getCstRemainingQuantity() <= 0 ? COST
                  : Math.abs(
                      invCosting.getCstRemainingValue() / invCosting.getCstRemainingQuantity());


            if (COST <= 0) {
              COST = invItemLocation.getInvItem().getIiUnitCost();
            }

            else if (invCosting.getInvItemLocation().getInvItem().getIiCostMethod().equals("FIFO"))

              COST = invCosting.getCstRemainingQuantity() == 0 ? COST
                  : this.getInvFifoCost(arInvoice.getInvDate(), invItemLocation.getIlCode(),
                      arInvoiceLineItem.getIliQuantity(), arInvoiceLineItem.getIliUnitPrice(),
                      false, AD_BRNCH, AD_CMPNY);

            else if (invCosting.getInvItemLocation().getInvItem().getIiCostMethod()
                .equals("Standard"))

              COST = invItemLocation.getInvItem().getIiUnitCost();

          } catch (FinderException ex) {
          }

          double QTY_SLD =
              this.convertByUomFromAndItemAndQuantity(arInvoiceLineItem.getInvUnitOfMeasure(),
                  arInvoiceLineItem.getInvItemLocation().getInvItem(),
                  arInvoiceLineItem.getIliQuantity(), AD_CMPNY);

          LocalAdBranchItemLocation adBranchItemLocation = null;

          try {

            adBranchItemLocation = adBranchItemLocationHome.findBilByIlCodeAndBrCode(
                arInvoiceLineItem.getInvItemLocation().getIlCode(), AD_BRNCH, AD_CMPNY);

          } catch (FinderException ex) {

          }

          if (arInvoiceLineItem.getIliEnableAutoBuild() == EJBCommon.FALSE && arInvoiceLineItem
              .getInvItemLocation().getInvItem().getIiNonInventoriable() == EJBCommon.FALSE) {

            if (arInvoice.getInvCreditMemo() == EJBCommon.FALSE) {

              if (adBranchItemLocation != null) {

                this.addArDrIliEntry(arInvoice.getArDrNextLine(), "COGS", EJBCommon.TRUE,
                    COST * QTY_SLD, adBranchItemLocation.getBilCoaGlCostOfSalesAccount(), arInvoice,
                    AD_BRNCH, AD_CMPNY);

                this.addArDrIliEntry(arInvoice.getArDrNextLine(), "INVENTORY", EJBCommon.FALSE,
                    COST * QTY_SLD, adBranchItemLocation.getBilCoaGlInventoryAccount(), arInvoice,
                    AD_BRNCH, AD_CMPNY);

              } else {

                this.addArDrIliEntry(arInvoice.getArDrNextLine(), "COGS", EJBCommon.TRUE,
                    COST * QTY_SLD,
                    arInvoiceLineItem.getInvItemLocation().getIlGlCoaCostOfSalesAccount(),
                    arInvoice, AD_BRNCH, AD_CMPNY);

                this.addArDrIliEntry(arInvoice.getArDrNextLine(), "INVENTORY", EJBCommon.FALSE,
                    COST * QTY_SLD,
                    arInvoiceLineItem.getInvItemLocation().getIlGlCoaInventoryAccount(), arInvoice,
                    AD_BRNCH, AD_CMPNY);

              }

            } else {

              if (adBranchItemLocation != null) {

                this.addArDrIliEntry(arInvoice.getArDrNextLine(), "COGS", EJBCommon.FALSE,
                    COST * QTY_SLD, adBranchItemLocation.getBilCoaGlCostOfSalesAccount(), arInvoice,
                    AD_BRNCH, AD_CMPNY);

                this.addArDrIliEntry(arInvoice.getArDrNextLine(), "INVENTORY", EJBCommon.TRUE,
                    COST * QTY_SLD, adBranchItemLocation.getBilCoaGlInventoryAccount(), arInvoice,
                    AD_BRNCH, AD_CMPNY);

              } else {

                this.addArDrIliEntry(arInvoice.getArDrNextLine(), "COGS", EJBCommon.FALSE,
                    COST * QTY_SLD,
                    arInvoiceLineItem.getInvItemLocation().getIlGlCoaCostOfSalesAccount(),
                    arInvoice, AD_BRNCH, AD_CMPNY);

                this.addArDrIliEntry(arInvoice.getArDrNextLine(), "INVENTORY", EJBCommon.TRUE,
                    COST * QTY_SLD,
                    arInvoiceLineItem.getInvItemLocation().getIlGlCoaInventoryAccount(), arInvoice,
                    AD_BRNCH, AD_CMPNY);

              }

            }

          }

          if (arInvoiceLineItem.getIliEnableAutoBuild() == EJBCommon.TRUE && arInvoiceLineItem
              .getInvItemLocation().getInvItem().getIiClass().equals("Assembly")) {

            byte DEBIT = EJBCommon.TRUE;

            double TOTAL_AMOUNT = 0;
            double ABS_TOTAL_AMOUNT = 0;

            LocalGlChartOfAccount glInventoryChartOfAccount = glChartOfAccountHome.findByPrimaryKey(
                arInvoiceLineItem.getInvItemLocation().getIlGlCoaInventoryAccount());

            Collection invBillOfMaterials =
                arInvoiceLineItem.getInvItemLocation().getInvItem().getInvBillOfMaterials();

            Iterator j = invBillOfMaterials.iterator();

            while (j.hasNext()) {

              LocalInvBillOfMaterial invBillOfMaterial = (LocalInvBillOfMaterial) j.next();

              // add bill of material quantity needed to item location

              LocalInvItemLocation invIlRawMaterial = null;

              try {

                invIlRawMaterial = invItemLocationHome.findByLocNameAndIiName(
                    invBillOfMaterial.getBomLocName(), invBillOfMaterial.getBomIiName(), AD_CMPNY);

              } catch (FinderException ex) {

                throw new GlobalInvItemLocationNotFoundException(
                    String.valueOf(arInvoiceLineItem.getIliLine()) + " - Raw Mat. ("
                        + invBillOfMaterial.getBomIiName() + ")");

              }

              // bom conversion
              double quantityNeeded = this.convertByUomFromAndItemAndQuantity(
                  invBillOfMaterial.getInvUnitOfMeasure(), invIlRawMaterial.getInvItem(),
                  EJBCommon.roundIt(invBillOfMaterial.getBomQuantityNeeded() * QTY_SLD,
                      this.getInvGpQuantityPrecisionUnit(AD_CMPNY)),
                  AD_CMPNY);

              // start date validation
              if (adPreference.getPrfArAllowPriorDate() == EJBCommon.FALSE) {
                Collection invIlRawMatNegTxnCosting =
                    invCostingHome.findNegTxnByGreaterThanCstDateAndIiNameAndLocName(
                        arInvoice.getInvDate(), invIlRawMaterial.getInvItem().getIiName(),
                        invIlRawMaterial.getInvLocation().getLocName(), AD_BRNCH, AD_CMPNY);
                if (!invIlRawMatNegTxnCosting.isEmpty())
                  throw new GlobalInventoryDateException(invItemLocation.getInvItem().getIiName()
                      + " - Raw Mat. (" + invIlRawMaterial.getInvItem().getIiName() + ")");
              }

              LocalInvItem invItem =
                  invItemHome.findByIiName(invBillOfMaterial.getBomIiName(), AD_CMPNY);

              double COSTING = 0d;

              try {

                LocalInvCosting invCosting = invCostingHome
                    .getByMaxCstDateToLongAndMaxCstLineNumberAndLessThanEqualCstDateAndIiNameAndLocName(
                        arInvoice.getInvDate(), invItem.getIiName(),
                        invBillOfMaterial.getBomLocName(), AD_BRNCH, AD_CMPNY);

                // check if rmning vl is not zero and rmng qty is 0
                if (invCosting.getCstRemainingQuantity() <= 0) {

                  HashMap criteria = new HashMap();
                  criteria.put("itemName", invItemLocation.getInvItem().getIiName());
                  criteria.put("location", invItemLocation.getInvLocation().getLocName());

                  ArrayList branchList = new ArrayList();

                  AdBranchDetails mdetails = new AdBranchDetails();
                  mdetails.setBrCode(AD_BRNCH);
                  branchList.add(mdetails);

                  ejbRIC.executeInvFixItemCosting(criteria, branchList, AD_CMPNY);

                  invCosting = invCostingHome
                      .getByMaxCstDateToLongAndMaxCstLineNumberAndLessThanEqualCstDateAndIiNameAndLocName(
                          arInvoice.getInvDate(), invItemLocation.getInvItem().getIiName(),
                          invItemLocation.getInvLocation().getLocName(), AD_BRNCH, AD_CMPNY);
                }

                if (invCosting.getCstRemainingQuantity() <= 0) {
                  COSTING = Math.abs(
                      invCosting.getCstRemainingValue() / invCosting.getCstRemainingQuantity());
                } else {
                  COSTING = invItem.getIiUnitCost();
                }



              } catch (FinderException ex) {

                COSTING = invItem.getIiUnitCost();

              }

              // bom conversion
              COSTING = this.convertCostByUom(invIlRawMaterial.getInvItem().getIiName(),
                  invBillOfMaterial.getInvUnitOfMeasure().getUomName(), COSTING, true, AD_CMPNY);

              double BOM_AMOUNT = EJBCommon.roundIt(
                  (QTY_SLD * (invBillOfMaterial.getBomQuantityNeeded() * COSTING)),
                  this.getGlFcPrecisionUnit(AD_CMPNY));

              TOTAL_AMOUNT += BOM_AMOUNT;
              ABS_TOTAL_AMOUNT += Math.abs(BOM_AMOUNT);

              // add inventory account

              LocalAdBranchItemLocation adBilRawMaterial = null;

              try {

                adBilRawMaterial = adBranchItemLocationHome
                    .findBilByIlCodeAndBrCode(invIlRawMaterial.getIlCode(), AD_BRNCH, AD_CMPNY);

                if (arInvoice.getInvCreditMemo() == EJBCommon.FALSE) {

                  this.addArDrIliEntry(arInvoice.getArDrNextLine(), "INVENTORY", EJBCommon.FALSE,
                      Math.abs(BOM_AMOUNT), adBilRawMaterial.getBilCoaGlInventoryAccount(),
                      arInvoice, AD_BRNCH, AD_CMPNY);

                } else {

                  this.addArDrIliEntry(arInvoice.getArDrNextLine(), "INVENTORY", EJBCommon.TRUE,
                      Math.abs(BOM_AMOUNT), adBilRawMaterial.getBilCoaGlInventoryAccount(),
                      arInvoice, AD_BRNCH, AD_CMPNY);

                }

              } catch (FinderException ex) {

                if (arInvoice.getInvCreditMemo() == EJBCommon.FALSE) {

                  this.addArDrIliEntry(arInvoice.getArDrNextLine(), "INVENTORY", EJBCommon.FALSE,
                      Math.abs(BOM_AMOUNT), invIlRawMaterial.getIlGlCoaInventoryAccount(),
                      arInvoice, AD_BRNCH, AD_CMPNY);

                } else {

                  this.addArDrIliEntry(arInvoice.getArDrNextLine(), "INVENTORY", EJBCommon.TRUE,
                      Math.abs(BOM_AMOUNT), invIlRawMaterial.getIlGlCoaInventoryAccount(),
                      arInvoice, AD_BRNCH, AD_CMPNY);

                }

              }

            }

            // add cost of sales account

            if (arInvoice.getInvCreditMemo() == EJBCommon.FALSE) {
              if (adBranchItemLocation != null) {

                this.addArDrIliEntry(arInvoice.getArDrNextLine(), "COGS", EJBCommon.TRUE,
                    Math.abs(TOTAL_AMOUNT),
                    arInvoiceLineItem.getInvItemLocation().getIlGlCoaCostOfSalesAccount(),
                    arInvoice, AD_BRNCH, AD_CMPNY);

              } else {

                this.addArDrIliEntry(arInvoice.getArDrNextLine(), "COGS", EJBCommon.TRUE,
                    Math.abs(TOTAL_AMOUNT),
                    arInvoiceLineItem.getInvItemLocation().getIlGlCoaCostOfSalesAccount(),
                    arInvoice, AD_BRNCH, AD_CMPNY);

              }


            } else {

              if (adBranchItemLocation != null) {

                this.addArDrIliEntry(arInvoice.getArDrNextLine(), "COGS", EJBCommon.FALSE,
                    Math.abs(TOTAL_AMOUNT), adBranchItemLocation.getBilCoaGlCostOfSalesAccount(),
                    arInvoice, AD_BRNCH, AD_CMPNY);

              } else {

                this.addArDrIliEntry(arInvoice.getArDrNextLine(), "COGS", EJBCommon.FALSE,
                    Math.abs(TOTAL_AMOUNT),
                    arInvoiceLineItem.getInvItemLocation().getIlGlCoaCostOfSalesAccount(),
                    arInvoice, AD_BRNCH, AD_CMPNY);

              }

            }

          }

        }

      } else if (arSalesOrderInvoiceLines != null && !arSalesOrderInvoiceLines.isEmpty()) {

        i = arSalesOrderInvoiceLines.iterator();

        while (i.hasNext()) {

          LocalArSalesOrderInvoiceLine arSalesOrderInvoiceLine =
              (LocalArSalesOrderInvoiceLine) i.next();
          LocalInvItemLocation invItemLocation =
              arSalesOrderInvoiceLine.getArSalesOrderLine().getInvItemLocation();

          // start date validation
          if (adPreference.getPrfArAllowPriorDate() == EJBCommon.FALSE) {
            Collection invNegTxnCosting =
                invCostingHome.findNegTxnByGreaterThanCstDateAndIiNameAndLocName(
                    arInvoice.getInvDate(), invItemLocation.getInvItem().getIiName(),
                    invItemLocation.getInvLocation().getLocName(), AD_BRNCH, AD_CMPNY);
            if (!invNegTxnCosting.isEmpty())
              throw new GlobalInventoryDateException(invItemLocation.getInvItem().getIiName());
          }


          // add cost of sales distribution and inventory

          double COST = invItemLocation.getInvItem().getIiUnitCost();

          try {

            LocalInvCosting invCosting = invCostingHome
                .getByMaxCstDateToLongAndMaxCstLineNumberAndLessThanEqualCstDateAndIiNameAndLocName(
                    arInvoice.getInvDate(), invItemLocation.getInvItem().getIiName(),
                    invItemLocation.getInvLocation().getLocName(), AD_BRNCH, AD_CMPNY);

            // check if rmning vl is not zero and rmng qty is 0
            if (invCosting.getCstRemainingQuantity() <= 0) {

              HashMap criteria = new HashMap();
              criteria.put("itemName", invItemLocation.getInvItem().getIiName());
              criteria.put("location", invItemLocation.getInvLocation().getLocName());

              ArrayList branchList = new ArrayList();

              AdBranchDetails mdetails = new AdBranchDetails();
              mdetails.setBrCode(AD_BRNCH);
              branchList.add(mdetails);

              ejbRIC.executeInvFixItemCosting(criteria, branchList, AD_CMPNY);

              invCosting = invCostingHome
                  .getByMaxCstDateToLongAndMaxCstLineNumberAndLessThanEqualCstDateAndIiNameAndLocName(
                      arInvoice.getInvDate(), invItemLocation.getInvItem().getIiName(),
                      invItemLocation.getInvLocation().getLocName(), AD_BRNCH, AD_CMPNY);
            }

            if (invCosting.getInvItemLocation().getInvItem().getIiCostMethod().equals("Average"))

              if (invCosting.getCstRemainingQuantity() <= 0) {


                COST = invItemLocation.getInvItem().getIiUnitCost();
              } else {
                COST = Math
                    .abs(invCosting.getCstRemainingValue() / invCosting.getCstRemainingQuantity());

                if (COST <= 0) {
                  COST = invItemLocation.getInvItem().getIiUnitCost();
                }
              }



            else if (invCosting.getInvItemLocation().getInvItem().getIiCostMethod().equals("FIFO"))

              COST = this.getInvFifoCost(arInvoice.getInvDate(), invItemLocation.getIlCode(),
                  arSalesOrderInvoiceLine.getSilQuantityDelivered(),
                  arSalesOrderInvoiceLine.getArSalesOrderLine().getSolUnitPrice(), false, AD_BRNCH,
                  AD_CMPNY);

            else if (invCosting.getInvItemLocation().getInvItem().getIiCostMethod()
                .equals("Standard"))

              COST = invItemLocation.getInvItem().getIiUnitCost();

          } catch (FinderException ex) {

            COST = arSalesOrderInvoiceLine.getArSalesOrderLine().getInvItemLocation().getInvItem()
                .getIiUnitCost();

          }

          double QTY_SLD = this.convertByUomFromAndItemAndQuantity(
              arSalesOrderInvoiceLine.getArSalesOrderLine().getInvUnitOfMeasure(),
              invItemLocation.getInvItem(), arSalesOrderInvoiceLine.getSilQuantityDelivered(),
              AD_CMPNY);

          LocalAdBranchItemLocation adBranchItemLocation = null;

          try {

            adBranchItemLocation = adBranchItemLocationHome.findBilByIlCodeAndBrCode(
                arSalesOrderInvoiceLine.getArSalesOrderLine().getInvItemLocation().getIlCode(),
                AD_BRNCH, AD_CMPNY);

            this.addArDrIliEntry(arInvoice.getArDrNextLine(), "COGS", EJBCommon.TRUE,
                COST * QTY_SLD, adBranchItemLocation.getBilCoaGlCostOfSalesAccount(), arInvoice,
                AD_BRNCH, AD_CMPNY);

            this.addArDrIliEntry(arInvoice.getArDrNextLine(), "INVENTORY", EJBCommon.FALSE,
                COST * QTY_SLD, adBranchItemLocation.getBilCoaGlInventoryAccount(), arInvoice,
                AD_BRNCH, AD_CMPNY);

          } catch (FinderException ex) {

            this.addArDrIliEntry(arInvoice.getArDrNextLine(), "COGS", EJBCommon.TRUE,
                COST * QTY_SLD, arSalesOrderInvoiceLine.getArSalesOrderLine().getInvItemLocation()
                    .getIlGlCoaCostOfSalesAccount(),
                arInvoice, AD_BRNCH, AD_CMPNY);

            this.addArDrIliEntry(arInvoice.getArDrNextLine(), "INVENTORY", EJBCommon.FALSE,
                COST * QTY_SLD, arSalesOrderInvoiceLine.getArSalesOrderLine().getInvItemLocation()
                    .getIlGlCoaInventoryAccount(),
                arInvoice, AD_BRNCH, AD_CMPNY);

          }

          // add quantity to item location committed quantity

          double convertedQuantity = this.convertByUomFromAndItemAndQuantity(
              arSalesOrderInvoiceLine.getArSalesOrderLine().getInvUnitOfMeasure(),
              arSalesOrderInvoiceLine.getArSalesOrderLine().getInvItemLocation().getInvItem(),
              arSalesOrderInvoiceLine.getSilQuantityDelivered(), AD_CMPNY);
          invItemLocation
              .setIlCommittedQuantity(invItemLocation.getIlCommittedQuantity() + convertedQuantity);

        }

      }

    } catch (GlobalInventoryDateException ex) {

      getSessionContext().setRollbackOnly();
      throw ex;

    } catch (GlobalBranchAccountNumberInvalidException ex) {

      getSessionContext().setRollbackOnly();
      throw ex;

    } catch (Exception ex) {

      Debug.printStackTrace(ex);
      getSessionContext().setRollbackOnly();
      throw new EJBException(ex.getMessage());

    }

  }

  private double convertCostByUom(
      String II_NM, String UOM_NM, double unitCost, boolean isFromDefault, Integer AD_CMPNY
  ) {

    Debug.print("ArInvoiceEntryControllerBean convertCostByUom");

    LocalInvItemHome invItemHome = null;
    LocalInvUnitOfMeasureConversionHome invUnitOfMeasureConversionHome = null;

    // Initialize EJB Home

    try {

      invUnitOfMeasureConversionHome = (LocalInvUnitOfMeasureConversionHome) EJBHomeFactory
          .lookUpLocalHome(LocalInvUnitOfMeasureConversionHome.JNDI_NAME,
              LocalInvUnitOfMeasureConversionHome.class);
      invItemHome = (LocalInvItemHome) EJBHomeFactory.lookUpLocalHome(LocalInvItemHome.JNDI_NAME,
          LocalInvItemHome.class);

    } catch (NamingException ex) {

      throw new EJBException(ex.getMessage());

    }

    try {

      LocalInvItem invItem = invItemHome.findByIiName(II_NM, AD_CMPNY);
      LocalInvUnitOfMeasureConversion invUnitOfMeasureConversion =
          invUnitOfMeasureConversionHome.findUmcByIiNameAndUomName(II_NM, UOM_NM, AD_CMPNY);
      LocalInvUnitOfMeasureConversion invDefaultUomConversion = invUnitOfMeasureConversionHome
          .findUmcByIiNameAndUomName(II_NM, invItem.getInvUnitOfMeasure().getUomName(), AD_CMPNY);

      if (isFromDefault) {

        return unitCost * invDefaultUomConversion.getUmcConversionFactor()
            / invUnitOfMeasureConversion.getUmcConversionFactor();

      } else {

        return unitCost * invUnitOfMeasureConversion.getUmcConversionFactor()
            / invDefaultUomConversion.getUmcConversionFactor();

      }

    } catch (Exception ex) {

      Debug.printStackTrace(ex);
      getSessionContext().setRollbackOnly();
      throw new EJBException(ex.getMessage());

    }

  }

  private double convertByUomAndQuantity(
      LocalInvUnitOfMeasure invFromUnitOfMeasure, LocalInvItem invItem, double ADJST_QTY,
      Integer AD_CMPNY
  ) {

    Debug.print("ArInvoicePostControllerBean convertByUomFromAndUomToAndQuantity");

    LocalAdPreferenceHome adPreferenceHome = null;
    LocalInvUnitOfMeasureConversionHome invUnitOfMeasureConversionHome = null;

    // Initialize EJB Home

    try {

      adPreferenceHome = (LocalAdPreferenceHome) EJBHomeFactory
          .lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);
      invUnitOfMeasureConversionHome = (LocalInvUnitOfMeasureConversionHome) EJBHomeFactory
          .lookUpLocalHome(LocalInvUnitOfMeasureConversionHome.JNDI_NAME,
              LocalInvUnitOfMeasureConversionHome.class);

    } catch (NamingException ex) {

      throw new EJBException(ex.getMessage());

    }

    try {

      LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);

      LocalInvUnitOfMeasureConversion invUnitOfMeasureConversion =
          invUnitOfMeasureConversionHome.findUmcByIiNameAndUomName(invItem.getIiName(),
              invFromUnitOfMeasure.getUomName(), AD_CMPNY);
      LocalInvUnitOfMeasureConversion invDefaultUomConversion =
          invUnitOfMeasureConversionHome.findUmcByIiNameAndUomName(invItem.getIiName(),
              invItem.getInvUnitOfMeasure().getUomName(), AD_CMPNY);

      return EJBCommon.roundIt(
          ADJST_QTY * invDefaultUomConversion.getUmcConversionFactor()
              / invUnitOfMeasureConversion.getUmcConversionFactor(),
          adPreference.getPrfInvQuantityPrecisionUnit());

    } catch (Exception ex) {

      Debug.printStackTrace(ex);
      getSessionContext().setRollbackOnly();
      throw new EJBException(ex.getMessage());

    }

  }

  private void voidInvAdjustment(
      LocalInvAdjustment invAdjustment, Integer AD_BRNCH, Integer AD_CMPNY
  ) {

    Debug.print("ArMiscReceiptEntryControllerBean voidInvAdjustment");

    try {

      Collection invDistributionRecords = invAdjustment.getInvDistributionRecords();
      ArrayList list = new ArrayList();

      Iterator i = invDistributionRecords.iterator();

      while (i.hasNext()) {

        LocalInvDistributionRecord invDistributionRecord = (LocalInvDistributionRecord) i.next();

        list.add(invDistributionRecord);

      }

      i = list.iterator();

      while (i.hasNext()) {

        LocalInvDistributionRecord invDistributionRecord = (LocalInvDistributionRecord) i.next();

        this.addInvDrEntry(invAdjustment.getInvDrNextLine(), invDistributionRecord.getDrClass(),
            invDistributionRecord.getDrDebit() == EJBCommon.TRUE ? EJBCommon.FALSE : EJBCommon.TRUE,
            invDistributionRecord.getDrAmount(), EJBCommon.TRUE,
            invDistributionRecord.getInvChartOfAccount().getCoaCode(), invAdjustment, AD_BRNCH,
            AD_CMPNY);

      }

      Collection invAdjustmentLines = invAdjustment.getInvAdjustmentLines();
      i = invAdjustmentLines.iterator();
      list.clear();

      while (i.hasNext()) {

        LocalInvAdjustmentLine invAdjustmentLine = (LocalInvAdjustmentLine) i.next();

        list.add(invAdjustmentLine);

      }

      i = list.iterator();

      while (i.hasNext()) {

        LocalInvAdjustmentLine invAdjustmentLine = (LocalInvAdjustmentLine) i.next();

        this.addInvAlEntry(invAdjustmentLine.getInvItemLocation(), invAdjustment,
            (invAdjustmentLine.getAlUnitCost()) * -1, EJBCommon.TRUE, AD_CMPNY);

      }

      invAdjustment.setAdjVoid(EJBCommon.TRUE);

      this.executeInvAdjPost(invAdjustment.getAdjCode(), invAdjustment.getAdjLastModifiedBy(),
          AD_BRNCH, AD_CMPNY);

    } catch (Exception ex) {

      Debug.printStackTrace(ex);
      getSessionContext().setRollbackOnly();
      throw new EJBException(ex.getMessage());

    }

  }

  private void generateCostVariance(
      LocalInvItemLocation invItemLocation, double CST_VRNC_VL, String ADJ_RFRNC_NMBR,
      String ADJ_DSCRPTN, Date ADJ_DT, String USR_NM, Integer AD_BRNCH, Integer AD_CMPNY
  ) throws AdPRFCoaGlVarianceAccountNotFoundException {
    /*
     * Debug.print("ArMiscReceiptEntryControllerBean generateCostVariance");
     * 
     * LocalAdPreferenceHome adPreferenceHome = null; LocalGlChartOfAccountHome glChartOfAccountHome
     * = null; LocalAdBranchItemLocationHome adBranchItemLocationHome = null;
     * 
     * // Initialize EJB Home
     * 
     * try {
     * 
     * adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
     * lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);
     * glChartOfAccountHome = (LocalGlChartOfAccountHome)EJBHomeFactory.
     * lookUpLocalHome(LocalGlChartOfAccountHome.JNDI_NAME, LocalGlChartOfAccountHome.class);
     * adBranchItemLocationHome = (LocalAdBranchItemLocationHome)EJBHomeFactory.
     * lookUpLocalHome(LocalAdBranchItemLocationHome.JNDI_NAME,
     * LocalAdBranchItemLocationHome.class);
     * 
     * 
     * } catch (NamingException ex) {
     * 
     * throw new EJBException(ex.getMessage());
     * 
     * }
     * 
     * try{
     * 
     * LocalInvAdjustment newInvAdjustment = this.saveInvAdjustment(ADJ_RFRNC_NMBR, ADJ_DSCRPTN,
     * ADJ_DT, USR_NM, AD_BRNCH, AD_CMPNY); LocalAdPreference adPreference =
     * adPreferenceHome.findByPrfAdCompany(AD_CMPNY); LocalGlChartOfAccount glCoaVarianceAccount =
     * null;
     * 
     * if(adPreference.getPrfInvGlCoaVarianceAccount() == null) throw new
     * AdPRFCoaGlVarianceAccountNotFoundException();
     * 
     * try{
     * 
     * glCoaVarianceAccount =
     * glChartOfAccountHome.findByPrimaryKey(adPreference.getPrfInvGlCoaVarianceAccount());
     * //glCoaVarianceAccount.addInvAdjustment(newInvAdjustment);
     * newInvAdjustment.setGlChartOfAccount(glCoaVarianceAccount);
     * 
     * } catch (FinderException ex) {
     * 
     * throw new AdPRFCoaGlVarianceAccountNotFoundException();
     * 
     * }
     * 
     * LocalInvAdjustmentLine invAdjustmentLine = this.addInvAlEntry(invItemLocation,
     * newInvAdjustment, CST_VRNC_VL, EJBCommon.FALSE, AD_CMPNY);
     * 
     * // check for branch mapping
     * 
     * LocalAdBranchItemLocation adBranchItemLocation = null;
     * 
     * try{
     * 
     * adBranchItemLocation = adBranchItemLocationHome.findBilByIlCodeAndBrCode(
     * invAdjustmentLine.getInvItemLocation().getIlCode(), AD_BRNCH, AD_CMPNY);
     * 
     * } catch (FinderException ex) {
     * 
     * }
     * 
     * LocalGlChartOfAccount glInventoryChartOfAccount = null;
     * 
     * if (adBranchItemLocation == null) {
     * 
     * glInventoryChartOfAccount = glChartOfAccountHome.findByPrimaryKey(
     * invAdjustmentLine.getInvItemLocation().getIlGlCoaInventoryAccount()); } else {
     * 
     * glInventoryChartOfAccount = glChartOfAccountHome.findByPrimaryKey(
     * adBranchItemLocation.getBilCoaGlInventoryAccount());
     * 
     * }
     * 
     * 
     * boolean isDebit = CST_VRNC_VL < 0 ? false : true;
     * 
     * //inventory dr this.addInvDrEntry(newInvAdjustment.getInvDrNextLine(), "INVENTORY", isDebit
     * == true ? EJBCommon.TRUE : EJBCommon.FALSE, Math.abs(CST_VRNC_VL), EJBCommon.FALSE,
     * glInventoryChartOfAccount.getCoaCode(), newInvAdjustment, AD_BRNCH, AD_CMPNY);
     * 
     * //variance dr this.addInvDrEntry(newInvAdjustment.getInvDrNextLine(),"VARIANCE", !isDebit ==
     * true ? EJBCommon.TRUE : EJBCommon.FALSE, Math.abs(CST_VRNC_VL), EJBCommon.FALSE,
     * glCoaVarianceAccount.getCoaCode(), newInvAdjustment, AD_BRNCH, AD_CMPNY);
     * 
     * this.executeInvAdjPost(newInvAdjustment.getAdjCode(),
     * newInvAdjustment.getAdjLastModifiedBy(), AD_BRNCH, AD_CMPNY);
     * 
     * } catch (AdPRFCoaGlVarianceAccountNotFoundException ex) {
     * 
     * getSessionContext().setRollbackOnly(); throw ex;
     * 
     * } catch (Exception ex) {
     * 
     * Debug.printStackTrace(ex); getSessionContext().setRollbackOnly(); throw new
     * EJBException(ex.getMessage());
     * 
     * }
     */
  }

  private void regenerateCostVariance(
      Collection invCostings, LocalInvCosting invCosting, Integer AD_BRNCH, Integer AD_CMPNY
  ) throws AdPRFCoaGlVarianceAccountNotFoundException {
    /*
     * Debug.print("ArMiscReceiptEntryControllerBean regenerateCostVariance");
     * 
     * try {
     * 
     * Iterator i = invCostings.iterator(); LocalInvCosting prevInvCosting = invCosting;
     * 
     * while (i.hasNext()) {
     * 
     * LocalInvCosting invPropagatedCosting = (LocalInvCosting)i.next();
     * 
     * if(prevInvCosting.getCstRemainingQuantity() < 0) {
     * 
     * double TTL_CST = 0; double QNTY = 0; String ADJ_RFRNC_NMBR = ""; String ADJ_DSCRPTN = "";
     * String ADJ_CRTD_BY = "";
     * 
     * Debug.print("ArMiscReceiptEntryControllerBean regenerateCostVariance A");
     * 
     * // get unit cost adjusment, document number and unit of measure if
     * (invPropagatedCosting.getApPurchaseOrderLine() != null) {
     * 
     * TTL_CST = invPropagatedCosting.getApPurchaseOrderLine().getPlAmount(); QNTY =
     * this.convertByUomFromAndItemAndQuantity(
     * invPropagatedCosting.getApPurchaseOrderLine().getInvUnitOfMeasure(),
     * invPropagatedCosting.getApPurchaseOrderLine().getInvItemLocation().getInvItem(),
     * invPropagatedCosting.getApPurchaseOrderLine().getPlQuantity(), AD_CMPNY);
     * 
     * Debug.print("ArMiscReceiptEntryControllerBean regenerateCostVariance B");
     * 
     * ADJ_DSCRPTN =
     * invPropagatedCosting.getApPurchaseOrderLine().getApPurchaseOrder().getPoDescription();
     * ADJ_CRTD_BY =
     * invPropagatedCosting.getApPurchaseOrderLine().getApPurchaseOrder().getPoPostedBy();
     * ADJ_RFRNC_NMBR = "APRI" +
     * invPropagatedCosting.getApPurchaseOrderLine().getApPurchaseOrder().getPoDocumentNumber();
     * 
     * } else if (invPropagatedCosting.getApVoucherLineItem() != null){
     * 
     * TTL_CST = invPropagatedCosting.getApVoucherLineItem().getVliAmount(); QNTY =
     * this.convertByUomFromAndItemAndQuantity(
     * invPropagatedCosting.getApVoucherLineItem().getInvUnitOfMeasure(),
     * invPropagatedCosting.getApVoucherLineItem().getInvItemLocation().getInvItem(),
     * invPropagatedCosting.getApVoucherLineItem().getVliQuantity(), AD_CMPNY);
     * 
     * Debug.print("ArMiscReceiptEntryControllerBean regenerateCostVariance C");
     * 
     * if (invPropagatedCosting.getApVoucherLineItem().getApVoucher() != null) {
     * 
     * ADJ_DSCRPTN = invPropagatedCosting.getApVoucherLineItem().getApVoucher().getVouDescription();
     * ADJ_CRTD_BY = invPropagatedCosting.getApVoucherLineItem().getApVoucher().getVouPostedBy();
     * ADJ_RFRNC_NMBR = "APVOU" +
     * invPropagatedCosting.getApVoucherLineItem().getApVoucher().getVouDocumentNumber();
     * 
     * } else if (invPropagatedCosting.getApVoucherLineItem().getApCheck() != null) {
     * 
     * ADJ_DSCRPTN = invPropagatedCosting.getApVoucherLineItem().getApCheck().getChkDescription();
     * ADJ_CRTD_BY = invPropagatedCosting.getApVoucherLineItem().getApCheck().getChkPostedBy();
     * ADJ_RFRNC_NMBR = "APCHK" +
     * invPropagatedCosting.getApVoucherLineItem().getApCheck().getChkDocumentNumber();
     * 
     * }
     * 
     * } else if (invPropagatedCosting.getArInvoiceLineItem() != null){
     * 
     * QNTY = this.convertByUomFromAndItemAndQuantity(
     * invPropagatedCosting.getArInvoiceLineItem().getInvUnitOfMeasure(),
     * invPropagatedCosting.getArInvoiceLineItem().getInvItemLocation().getInvItem(),
     * invPropagatedCosting.getArInvoiceLineItem().getIliQuantity(), AD_CMPNY); TTL_CST =
     * prevInvCosting.getCstRemainingValue() - invPropagatedCosting.getCstRemainingValue();
     * 
     * Debug.print("ArMiscReceiptEntryControllerBean regenerateCostVariance D");
     * 
     * if(invPropagatedCosting.getArInvoiceLineItem().getArInvoice() != null){
     * 
     * ADJ_DSCRPTN = invPropagatedCosting.getArInvoiceLineItem().getArInvoice().getInvDescription();
     * ADJ_CRTD_BY = invPropagatedCosting.getArInvoiceLineItem().getArInvoice().getInvPostedBy();
     * ADJ_RFRNC_NMBR = "ARCM" +
     * invPropagatedCosting.getArInvoiceLineItem().getArInvoice().getInvNumber();
     * 
     * } else if(invPropagatedCosting.getArInvoiceLineItem().getArReceipt() != null){
     * 
     * ADJ_DSCRPTN = invPropagatedCosting.getArInvoiceLineItem().getArReceipt().getRctDescription();
     * ADJ_CRTD_BY = invPropagatedCosting.getArInvoiceLineItem().getArReceipt().getRctPostedBy();
     * ADJ_RFRNC_NMBR = "ARMR" +
     * invPropagatedCosting.getArInvoiceLineItem().getArReceipt().getRctNumber();
     * 
     * }
     * 
     * } else if (invPropagatedCosting.getArSalesOrderInvoiceLine() != null){
     * 
     * TTL_CST = prevInvCosting.getCstRemainingValue() -
     * invPropagatedCosting.getCstRemainingValue(); QNTY = this.convertByUomFromAndItemAndQuantity(
     * invPropagatedCosting.getArSalesOrderInvoiceLine().getArSalesOrderLine().getInvUnitOfMeasure()
     * ,
     * invPropagatedCosting.getArSalesOrderInvoiceLine().getArSalesOrderLine().getInvItemLocation().
     * getInvItem(), invPropagatedCosting.getArSalesOrderInvoiceLine().getSilQuantityDelivered(),
     * AD_CMPNY);
     * 
     * Debug.print("ArMiscReceiptEntryControllerBean regenerateCostVariance E");
     * 
     * ADJ_DSCRPTN =
     * invPropagatedCosting.getArSalesOrderInvoiceLine().getArInvoice().getInvDescription();
     * ADJ_CRTD_BY =
     * invPropagatedCosting.getArSalesOrderInvoiceLine().getArInvoice().getInvPostedBy();
     * ADJ_RFRNC_NMBR = "ARCM" +
     * invPropagatedCosting.getArSalesOrderInvoiceLine().getArInvoice().getInvNumber();
     * 
     * } else if (invPropagatedCosting.getInvAdjustmentLine() != null){
     * 
     * ADJ_DSCRPTN =
     * invPropagatedCosting.getInvAdjustmentLine().getInvAdjustment().getAdjDescription();
     * ADJ_CRTD_BY =
     * invPropagatedCosting.getInvAdjustmentLine().getInvAdjustment().getAdjPostedBy();
     * ADJ_RFRNC_NMBR = "INVADJ" +
     * invPropagatedCosting.getInvAdjustmentLine().getInvAdjustment().getAdjDocumentNumber();
     * 
     * if(invPropagatedCosting.getInvAdjustmentLine().getAlAdjustQuantity() != 0) {
     * 
     * TTL_CST = (invPropagatedCosting.getInvAdjustmentLine().getAlUnitCost() *
     * invPropagatedCosting.getInvAdjustmentLine().getAlAdjustQuantity()); QNTY =
     * this.convertByUomFromAndItemAndQuantity(
     * invPropagatedCosting.getInvAdjustmentLine().getInvUnitOfMeasure(),
     * invPropagatedCosting.getInvAdjustmentLine().getInvItemLocation().getInvItem(),
     * invPropagatedCosting.getInvAdjustmentLine().getAlAdjustQuantity(), AD_CMPNY);
     * 
     * }
     * 
     * } else if (invPropagatedCosting.getInvAssemblyTransferLine() != null){
     * 
     * QNTY = invPropagatedCosting.getInvAssemblyTransferLine().getAtlAssembleQuantity(); TTL_CST =
     * invPropagatedCosting.getInvAssemblyTransferLine().getAtlAssembleCost();
     * 
     * ADJ_DSCRPTN =
     * invPropagatedCosting.getInvAssemblyTransferLine().getInvAssemblyTransfer().getAtrDescription(
     * ); ADJ_CRTD_BY =
     * invPropagatedCosting.getInvAssemblyTransferLine().getInvAssemblyTransfer().getAtrPostedBy();
     * ADJ_RFRNC_NMBR = "INVAT" +
     * invPropagatedCosting.getInvAssemblyTransferLine().getInvAssemblyTransfer().
     * getAtrDocumentNumber();
     * 
     * } else if (invPropagatedCosting.getInvBranchStockTransferLine() != null){
     * 
     * if(invPropagatedCosting.getInvBranchStockTransferLine().getInvBranchStockTransfer().
     * getBstTransferOutNumber() != null) {
     * 
     * TTL_CST = invPropagatedCosting.getInvBranchStockTransferLine().getBslAmount(); QNTY =
     * this.convertByUomFromAndItemAndQuantity(
     * invPropagatedCosting.getInvBranchStockTransferLine().getInvUnitOfMeasure(),
     * invPropagatedCosting.getInvBranchStockTransferLine().getInvItemLocation().getInvItem(),
     * invPropagatedCosting.getInvBranchStockTransferLine().getBslQuantityReceived(), AD_CMPNY);
     * 
     * } else {
     * 
     * TTL_CST = invPropagatedCosting.getInvBranchStockTransferLine().getBslAmount(); QNTY =
     * this.convertByUomFromAndItemAndQuantity(
     * invPropagatedCosting.getInvBranchStockTransferLine().getInvUnitOfMeasure(),
     * invPropagatedCosting.getInvBranchStockTransferLine().getInvItemLocation().getInvItem(),
     * invPropagatedCosting.getInvBranchStockTransferLine().getBslQuantity(), AD_CMPNY); }
     * 
     * ADJ_DSCRPTN =
     * invPropagatedCosting.getInvBranchStockTransferLine().getInvBranchStockTransfer().
     * getBstDescription(); ADJ_CRTD_BY =
     * invPropagatedCosting.getInvBranchStockTransferLine().getInvBranchStockTransfer().
     * getBstPostedBy(); ADJ_RFRNC_NMBR = "INVBST" +
     * invPropagatedCosting.getInvBranchStockTransferLine().getInvBranchStockTransfer().getBstNumber
     * ();
     * 
     * } else if (invPropagatedCosting.getInvBuildUnbuildAssemblyLine() != null){
     * 
     * TTL_CST = prevInvCosting.getCstRemainingValue() -
     * invPropagatedCosting.getCstRemainingValue(); QNTY =
     * invPropagatedCosting.getInvBuildUnbuildAssemblyLine().getBlBuildQuantity(); ADJ_DSCRPTN =
     * invPropagatedCosting.getInvBuildUnbuildAssemblyLine().getInvBuildUnbuildAssembly().
     * getBuaDescription(); ADJ_CRTD_BY =
     * invPropagatedCosting.getInvBuildUnbuildAssemblyLine().getInvBuildUnbuildAssembly().
     * getBuaPostedBy(); ADJ_RFRNC_NMBR = "INVBUA" +
     * invPropagatedCosting.getInvBuildUnbuildAssemblyLine().getInvBuildUnbuildAssembly().
     * getBuaDocumentNumber();
     * 
     * } else if (invPropagatedCosting.getInvStockIssuanceLine()!= null){
     * 
     * TTL_CST = invPropagatedCosting.getInvStockIssuanceLine().getSilIssueCost(); QNTY =
     * this.convertByUomFromAndItemAndQuantity(
     * invPropagatedCosting.getInvStockIssuanceLine().getInvUnitOfMeasure(),
     * invPropagatedCosting.getInvStockIssuanceLine().getInvItemLocation().getInvItem(),
     * invPropagatedCosting.getInvStockIssuanceLine().getSilIssueQuantity(), AD_CMPNY);
     * 
     * Debug.print("ArMiscReceiptEntryControllerBean regenerateCostVariance F");
     * 
     * ADJ_DSCRPTN =
     * invPropagatedCosting.getInvStockIssuanceLine().getInvStockIssuance().getSiDescription();
     * ADJ_CRTD_BY =
     * invPropagatedCosting.getInvStockIssuanceLine().getInvStockIssuance().getSiPostedBy();
     * ADJ_RFRNC_NMBR = "INVSI" +
     * invPropagatedCosting.getInvStockIssuanceLine().getInvStockIssuance().getSiDocumentNumber();
     * 
     * } else if (invPropagatedCosting.getInvStockTransferLine()!= null) {
     * 
     * TTL_CST = invPropagatedCosting.getInvStockTransferLine().getStlAmount(); QNTY =
     * this.convertByUomFromAndItemAndQuantity(
     * invPropagatedCosting.getInvStockTransferLine().getInvUnitOfMeasure(),
     * invPropagatedCosting.getInvStockTransferLine().getInvItem(),
     * invPropagatedCosting.getInvStockTransferLine().getStlQuantityDelivered(), AD_CMPNY);
     * Debug.print("ArMiscReceiptEntryControllerBean regenerateCostVariance G"); ADJ_DSCRPTN =
     * invPropagatedCosting.getInvStockTransferLine().getInvStockTransfer().getStDescription();
     * ADJ_CRTD_BY =
     * invPropagatedCosting.getInvStockTransferLine().getInvStockTransfer().getStPostedBy();
     * ADJ_RFRNC_NMBR = "INVST" +
     * invPropagatedCosting.getInvStockTransferLine().getInvStockTransfer().getStDocumentNumber();
     * 
     * } else {
     * 
     * prevInvCosting = invPropagatedCosting; continue;
     * 
     * }
     * 
     * // if quantity is equal 0, no variance. if(QNTY == 0) continue;
     * 
     * // compute new cost variance double UNT_CST = TTL_CST/QNTY; double CST_VRNC_VL =
     * (invPropagatedCosting.getCstRemainingQuantity() * UNT_CST -
     * invPropagatedCosting.getCstRemainingValue());
     * 
     * if(CST_VRNC_VL != 0) this.generateCostVariance(invPropagatedCosting.getInvItemLocation(),
     * CST_VRNC_VL, ADJ_RFRNC_NMBR, ADJ_DSCRPTN, invPropagatedCosting.getCstDate(), ADJ_CRTD_BY,
     * AD_BRNCH, AD_CMPNY);
     * 
     * }
     * 
     * // set previous costing prevInvCosting = invPropagatedCosting;
     * 
     * }
     * 
     * } catch (AdPRFCoaGlVarianceAccountNotFoundException ex){
     * 
     * throw ex;
     * 
     * } catch (Exception ex) {
     * 
     * Debug.printStackTrace(ex); getSessionContext().setRollbackOnly(); throw new
     * EJBException(ex.getMessage());
     * 
     * }
     */
  }

  private void addInvDrEntry(
      short DR_LN, String DR_CLSS, byte DR_DBT, double DR_AMNT, byte DR_RVRSL, Integer COA_CODE,
      LocalInvAdjustment invAdjustment, Integer AD_BRNCH, Integer AD_CMPNY
  )

      throws GlobalBranchAccountNumberInvalidException {

    Debug.print("ArMiscReceiptEntryControllerBean addInvDrEntry");

    LocalAdCompanyHome adCompanyHome = null;
    LocalInvDistributionRecordHome invDistributionRecordHome = null;
    LocalGlChartOfAccountHome glChartOfAccountHome = null;

    // Initialize EJB Home

    try {

      adCompanyHome = (LocalAdCompanyHome) EJBHomeFactory
          .lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
      invDistributionRecordHome = (LocalInvDistributionRecordHome) EJBHomeFactory.lookUpLocalHome(
          LocalInvDistributionRecordHome.JNDI_NAME, LocalInvDistributionRecordHome.class);
      glChartOfAccountHome = (LocalGlChartOfAccountHome) EJBHomeFactory
          .lookUpLocalHome(LocalGlChartOfAccountHome.JNDI_NAME, LocalGlChartOfAccountHome.class);


    } catch (NamingException ex) {

      throw new EJBException(ex.getMessage());

    }

    try {

      // get company

      LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);

      // validate coa

      LocalGlChartOfAccount glChartOfAccount = null;

      try {

        glChartOfAccount =
            glChartOfAccountHome.findByCoaCodeAndBranchCode(COA_CODE, AD_BRNCH, AD_CMPNY);

      } catch (FinderException ex) {

        throw new GlobalBranchAccountNumberInvalidException();

      }

      // create distribution record

      LocalInvDistributionRecord invDistributionRecord =
          invDistributionRecordHome.create(DR_LN, DR_CLSS, DR_DBT,
              EJBCommon.roundIt(DR_AMNT, adCompany.getGlFunctionalCurrency().getFcPrecision()),
              DR_RVRSL, EJBCommon.FALSE, AD_CMPNY);

      // invAdjustment.addInvDistributionRecord(invDistributionRecord);
      invDistributionRecord.setInvAdjustment(invAdjustment);
      // glChartOfAccount.addInvDistributionRecord(invDistributionRecord);
      invDistributionRecord.setInvChartOfAccount(glChartOfAccount);

    } catch (GlobalBranchAccountNumberInvalidException ex) {

      throw new GlobalBranchAccountNumberInvalidException();

    } catch (Exception ex) {

      Debug.printStackTrace(ex);
      getSessionContext().setRollbackOnly();
      throw new EJBException(ex.getMessage());

    }

  }

  private void executeInvAdjPost(
      Integer ADJ_CODE, String USR_NM, Integer AD_BRNCH, Integer AD_CMPNY
  ) throws GlobalRecordAlreadyDeletedException, GlobalTransactionAlreadyPostedException,
      GlJREffectiveDateNoPeriodExistException, GlJREffectiveDatePeriodClosedException,
      GlobalJournalNotBalanceException, GlobalBranchAccountNumberInvalidException {

    Debug.print("ArMiscReceiptEntryControllerBean executeInvAdjPost");

    LocalInvAdjustmentHome invAdjustmentHome = null;
    LocalAdCompanyHome adCompanyHome = null;
    LocalAdPreferenceHome adPreferenceHome = null;
    LocalGlSetOfBookHome glSetOfBookHome = null;
    LocalGlAccountingCalendarValueHome glAccountingCalendarValueHome = null;
    LocalGlJournalHome glJournalHome = null;
    LocalGlJournalBatchHome glJournalBatchHome = null;
    LocalGlSuspenseAccountHome glSuspenseAccountHome = null;
    LocalGlJournalLineHome glJournalLineHome = null;
    LocalGlJournalSourceHome glJournalSourceHome = null;
    LocalGlJournalCategoryHome glJournalCategoryHome = null;
    LocalGlFunctionalCurrencyHome glFunctionalCurrencyHome = null;
    LocalInvDistributionRecordHome invDistributionRecordHome = null;
    LocalInvCostingHome invCostingHome = null;
    LocalGlChartOfAccountHome glChartOfAccountHome = null;
    LocalInvAdjustmentLineHome invAdjustmentLineHome = null;


    // Initialize EJB Home

    try {

      invAdjustmentHome = (LocalInvAdjustmentHome) EJBHomeFactory
          .lookUpLocalHome(LocalInvAdjustmentHome.JNDI_NAME, LocalInvAdjustmentHome.class);
      adCompanyHome = (LocalAdCompanyHome) EJBHomeFactory
          .lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
      adPreferenceHome = (LocalAdPreferenceHome) EJBHomeFactory
          .lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);
      glSetOfBookHome = (LocalGlSetOfBookHome) EJBHomeFactory
          .lookUpLocalHome(LocalGlSetOfBookHome.JNDI_NAME, LocalGlSetOfBookHome.class);
      glAccountingCalendarValueHome = (LocalGlAccountingCalendarValueHome) EJBHomeFactory
          .lookUpLocalHome(LocalGlAccountingCalendarValueHome.JNDI_NAME,
              LocalGlAccountingCalendarValueHome.class);
      glJournalHome = (LocalGlJournalHome) EJBHomeFactory
          .lookUpLocalHome(LocalGlJournalHome.JNDI_NAME, LocalGlJournalHome.class);
      glJournalBatchHome = (LocalGlJournalBatchHome) EJBHomeFactory
          .lookUpLocalHome(LocalGlJournalBatchHome.JNDI_NAME, LocalGlJournalBatchHome.class);
      glSuspenseAccountHome = (LocalGlSuspenseAccountHome) EJBHomeFactory
          .lookUpLocalHome(LocalGlSuspenseAccountHome.JNDI_NAME, LocalGlSuspenseAccountHome.class);
      glJournalLineHome = (LocalGlJournalLineHome) EJBHomeFactory
          .lookUpLocalHome(LocalGlJournalLineHome.JNDI_NAME, LocalGlJournalLineHome.class);
      glJournalSourceHome = (LocalGlJournalSourceHome) EJBHomeFactory
          .lookUpLocalHome(LocalGlJournalSourceHome.JNDI_NAME, LocalGlJournalSourceHome.class);
      glJournalCategoryHome = (LocalGlJournalCategoryHome) EJBHomeFactory
          .lookUpLocalHome(LocalGlJournalCategoryHome.JNDI_NAME, LocalGlJournalCategoryHome.class);
      glFunctionalCurrencyHome = (LocalGlFunctionalCurrencyHome) EJBHomeFactory.lookUpLocalHome(
          LocalGlFunctionalCurrencyHome.JNDI_NAME, LocalGlFunctionalCurrencyHome.class);
      invDistributionRecordHome = (LocalInvDistributionRecordHome) EJBHomeFactory.lookUpLocalHome(
          LocalInvDistributionRecordHome.JNDI_NAME, LocalInvDistributionRecordHome.class);
      invCostingHome = (LocalInvCostingHome) EJBHomeFactory
          .lookUpLocalHome(LocalInvCostingHome.JNDI_NAME, LocalInvCostingHome.class);
      glChartOfAccountHome = (LocalGlChartOfAccountHome) EJBHomeFactory
          .lookUpLocalHome(LocalGlChartOfAccountHome.JNDI_NAME, LocalGlChartOfAccountHome.class);
      invAdjustmentLineHome = (LocalInvAdjustmentLineHome) EJBHomeFactory
          .lookUpLocalHome(LocalInvAdjustmentLineHome.JNDI_NAME, LocalInvAdjustmentLineHome.class);


    } catch (NamingException ex) {

      throw new EJBException(ex.getMessage());

    }

    try {

      // validate if adjustment is already deleted

      LocalInvAdjustment invAdjustment = null;

      try {

        invAdjustment = invAdjustmentHome.findByPrimaryKey(ADJ_CODE);

      } catch (FinderException ex) {

        throw new GlobalRecordAlreadyDeletedException();

      }

      // validate if adjustment is already posted or void

      if (invAdjustment.getAdjPosted() == EJBCommon.TRUE) {

        if (invAdjustment.getAdjVoid() != EJBCommon.TRUE)
          throw new GlobalTransactionAlreadyPostedException();

      }

      Collection invAdjustmentLines = null;

      if (invAdjustment.getAdjVoid() == EJBCommon.FALSE)
        invAdjustmentLines = invAdjustmentLineHome.findByAlVoidAndAdjCode(EJBCommon.FALSE,
            invAdjustment.getAdjCode(), AD_CMPNY);
      else
        invAdjustmentLines = invAdjustmentLineHome.findByAlVoidAndAdjCode(EJBCommon.TRUE,
            invAdjustment.getAdjCode(), AD_CMPNY);

      Iterator i = invAdjustmentLines.iterator();

      while (i.hasNext()) {


        LocalInvAdjustmentLine invAdjustmentLine = (LocalInvAdjustmentLine) i.next();

        LocalInvCosting invCosting = invCostingHome
            .getByMaxCstDateToLongAndMaxCstLineNumberAndLessThanEqualCstDateAndIiNameAndLocName(
                invAdjustment.getAdjDate(),
                invAdjustmentLine.getInvItemLocation().getInvItem().getIiName(),
                invAdjustmentLine.getInvItemLocation().getInvLocation().getLocName(), AD_BRNCH,
                AD_CMPNY);



        this.postInvAdjustmentToInventory(invAdjustmentLine, invAdjustment.getAdjDate(), 0,
            invAdjustmentLine.getAlUnitCost(), invCosting.getCstRemainingQuantity(),
            invCosting.getCstRemainingValue() + invAdjustmentLine.getAlUnitCost(), AD_BRNCH,
            AD_CMPNY);

      }

      // post to gl if necessary

      LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);
      LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);

      // validate if date has no period and period is closed

      LocalGlSetOfBook glJournalSetOfBook = null;

      try {

        glJournalSetOfBook = glSetOfBookHome.findByDate(invAdjustment.getAdjDate(), AD_CMPNY);

      } catch (FinderException ex) {

        throw new GlJREffectiveDateNoPeriodExistException();

      }

      LocalGlAccountingCalendarValue glAccountingCalendarValue = glAccountingCalendarValueHome
          .findByAcCodeAndDate(glJournalSetOfBook.getGlAccountingCalendar().getAcCode(),
              invAdjustment.getAdjDate(), AD_CMPNY);


      if (glAccountingCalendarValue.getAcvStatus() == 'N'
          || glAccountingCalendarValue.getAcvStatus() == 'C'
          || glAccountingCalendarValue.getAcvStatus() == 'P') {

        throw new GlJREffectiveDatePeriodClosedException();

      }

      // check if invoice is balance if not check suspense posting

      LocalGlJournalLine glOffsetJournalLine = null;

      Collection invDistributionRecords = null;

      if (invAdjustment.getAdjVoid() == EJBCommon.FALSE) {

        invDistributionRecords = invDistributionRecordHome.findImportableDrByDrReversedAndAdjCode(
            EJBCommon.FALSE, invAdjustment.getAdjCode(), AD_CMPNY);

      } else {

        invDistributionRecords = invDistributionRecordHome.findImportableDrByDrReversedAndAdjCode(
            EJBCommon.TRUE, invAdjustment.getAdjCode(), AD_CMPNY);

      }


      Iterator j = invDistributionRecords.iterator();

      double TOTAL_DEBIT = 0d;
      double TOTAL_CREDIT = 0d;

      while (j.hasNext()) {

        LocalInvDistributionRecord invDistributionRecord = (LocalInvDistributionRecord) j.next();

        double DR_AMNT = 0d;

        DR_AMNT = invDistributionRecord.getDrAmount();

        if (invDistributionRecord.getDrDebit() == EJBCommon.TRUE) {

          TOTAL_DEBIT += DR_AMNT;

        } else {

          TOTAL_CREDIT += DR_AMNT;

        }

      }

      TOTAL_DEBIT =
          EJBCommon.roundIt(TOTAL_DEBIT, adCompany.getGlFunctionalCurrency().getFcPrecision());
      TOTAL_CREDIT =
          EJBCommon.roundIt(TOTAL_CREDIT, adCompany.getGlFunctionalCurrency().getFcPrecision());

      if (adPreference.getPrfAllowSuspensePosting() == EJBCommon.TRUE
          && TOTAL_DEBIT != TOTAL_CREDIT) {

        LocalGlSuspenseAccount glSuspenseAccount = null;

        try {

          glSuspenseAccount = glSuspenseAccountHome.findByJsNameAndJcName("INVENTORY",
              "INVENTORY ADJUSTMENTS", AD_CMPNY);

        } catch (FinderException ex) {

          throw new GlobalJournalNotBalanceException();

        }

        if (TOTAL_DEBIT - TOTAL_CREDIT < 0) {

          glOffsetJournalLine =
              glJournalLineHome.create((short) (invDistributionRecords.size() + 1), EJBCommon.TRUE,
                  TOTAL_CREDIT - TOTAL_DEBIT, "", AD_CMPNY);

        } else {

          glOffsetJournalLine =
              glJournalLineHome.create((short) (invDistributionRecords.size() + 1), EJBCommon.FALSE,
                  TOTAL_DEBIT - TOTAL_CREDIT, "", AD_CMPNY);

        }

        LocalGlChartOfAccount glChartOfAccount = glSuspenseAccount.getGlChartOfAccount();
        // glChartOfAccount.addGlJournalLine(glOffsetJournalLine);
        glOffsetJournalLine.setGlChartOfAccount(glChartOfAccount);


      } else if (adPreference.getPrfAllowSuspensePosting() == EJBCommon.FALSE
          && TOTAL_DEBIT != TOTAL_CREDIT) {

        throw new GlobalJournalNotBalanceException();

      }

      // create journal batch if necessary

      LocalGlJournalBatch glJournalBatch = null;
      java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat("MM/dd/yyyy");

      try {

        glJournalBatch = glJournalBatchHome.findByJbName(
            "JOURNAL IMPORT " + formatter.format(new Date()) + " INVENTORY ADJUSTMENTS", AD_BRNCH,
            AD_CMPNY);

      } catch (FinderException ex) {

      }

      if (glJournalBatch == null) {

        glJournalBatch = glJournalBatchHome.create(
            "JOURNAL IMPORT " + formatter.format(new Date()) + " INVENTORY ADJUSTMENTS",
            "JOURNAL IMPORT", "CLOSED", EJBCommon.getGcCurrentDateWoTime().getTime(), USR_NM,
            AD_BRNCH, AD_CMPNY);

      }

      // create journal entry

      LocalGlJournal glJournal = glJournalHome.create(invAdjustment.getAdjReferenceNumber(),
          invAdjustment.getAdjDescription(), invAdjustment.getAdjDate(), 0.0d, null,
          invAdjustment.getAdjDocumentNumber(), null, 1d, "N/A", null, 'N', EJBCommon.TRUE,
          EJBCommon.FALSE, USR_NM, new Date(), USR_NM, new Date(), null, null, USR_NM,
          EJBCommon.getGcCurrentDateWoTime().getTime(), null, null, EJBCommon.FALSE, null, AD_BRNCH,
          AD_CMPNY);

      LocalGlJournalSource glJournalSource =
          glJournalSourceHome.findByJsName("INVENTORY", AD_CMPNY);
      glJournal.setGlJournalSource(glJournalSource);

      LocalGlFunctionalCurrency glFunctionalCurrency = glFunctionalCurrencyHome
          .findByFcName(adCompany.getGlFunctionalCurrency().getFcName(), AD_CMPNY);
      glJournal.setGlFunctionalCurrency(glFunctionalCurrency);

      LocalGlJournalCategory glJournalCategory =
          glJournalCategoryHome.findByJcName("INVENTORY ADJUSTMENTS", AD_CMPNY);
      glJournal.setGlJournalCategory(glJournalCategory);

      if (glJournalBatch != null) {

        glJournal.setGlJournalBatch(glJournalBatch);

      }

      // create journal lines

      j = invDistributionRecords.iterator();

      while (j.hasNext()) {

        LocalInvDistributionRecord invDistributionRecord = (LocalInvDistributionRecord) j.next();

        double DR_AMNT = 0d;

        DR_AMNT = invDistributionRecord.getDrAmount();

        LocalGlJournalLine glJournalLine =
            glJournalLineHome.create(invDistributionRecord.getDrLine(),
                invDistributionRecord.getDrDebit(), DR_AMNT, "", AD_CMPNY);

        glJournalLine.setGlChartOfAccount(invDistributionRecord.getInvChartOfAccount());

        glJournalLine.setGlJournal(glJournal);

        invDistributionRecord.setDrImported(EJBCommon.TRUE);


      }

      if (glOffsetJournalLine != null) {

        glOffsetJournalLine.setGlJournal(glJournal);

      }

      // post journal to gl

      Collection glJournalLines = glJournal.getGlJournalLines();

      i = glJournalLines.iterator();

      while (i.hasNext()) {

        LocalGlJournalLine glJournalLine = (LocalGlJournalLine) i.next();

        // post current to current acv

        this.postToGl(glAccountingCalendarValue, glJournalLine.getGlChartOfAccount(), true,
            glJournalLine.getJlDebit(), glJournalLine.getJlAmount(), AD_CMPNY);


        // post to subsequent acvs (propagate)

        Collection glSubsequentAccountingCalendarValues =
            glAccountingCalendarValueHome.findSubsequentAcvByAcCodeAndAcvPeriodNumber(
                glJournalSetOfBook.getGlAccountingCalendar().getAcCode(),
                glAccountingCalendarValue.getAcvPeriodNumber(), AD_CMPNY);

        Iterator acvsIter = glSubsequentAccountingCalendarValues.iterator();

        while (acvsIter.hasNext()) {

          LocalGlAccountingCalendarValue glSubsequentAccountingCalendarValue =
              (LocalGlAccountingCalendarValue) acvsIter.next();

          this.postToGl(glSubsequentAccountingCalendarValue, glJournalLine.getGlChartOfAccount(),
              false, glJournalLine.getJlDebit(), glJournalLine.getJlAmount(), AD_CMPNY);

        }

        // post to subsequent years if necessary

        Collection glSubsequentSetOfBooks = glSetOfBookHome.findSubsequentSobByAcYear(
            glJournalSetOfBook.getGlAccountingCalendar().getAcYear(), AD_CMPNY);

        if (!glSubsequentSetOfBooks.isEmpty() && glJournalSetOfBook.getSobYearEndClosed() == 1) {

          adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
          LocalGlChartOfAccount glRetainedEarningsAccount =
              glChartOfAccountHome.findByCoaAccountNumberAndBranchCode(
                  adCompany.getCmpRetainedEarnings(), AD_BRNCH, AD_CMPNY);

          Iterator sobIter = glSubsequentSetOfBooks.iterator();

          while (sobIter.hasNext()) {

            LocalGlSetOfBook glSubsequentSetOfBook = (LocalGlSetOfBook) sobIter.next();

            String ACCOUNT_TYPE = glJournalLine.getGlChartOfAccount().getCoaAccountType();

            // post to subsequent acvs of subsequent set of book(propagate)

            Collection glAccountingCalendarValues = glAccountingCalendarValueHome.findByAcCode(
                glSubsequentSetOfBook.getGlAccountingCalendar().getAcCode(), AD_CMPNY);

            Iterator acvIter = glAccountingCalendarValues.iterator();

            while (acvIter.hasNext()) {

              LocalGlAccountingCalendarValue glSubsequentAccountingCalendarValue =
                  (LocalGlAccountingCalendarValue) acvIter.next();

              if (ACCOUNT_TYPE.equals("ASSET") || ACCOUNT_TYPE.equals("LIABILITY")
                  || ACCOUNT_TYPE.equals("OWNERS EQUITY")) {

                this.postToGl(glSubsequentAccountingCalendarValue,
                    glJournalLine.getGlChartOfAccount(), false, glJournalLine.getJlDebit(),
                    glJournalLine.getJlAmount(), AD_CMPNY);

              } else { // revenue & expense

                this.postToGl(glSubsequentAccountingCalendarValue, glRetainedEarningsAccount, false,
                    glJournalLine.getJlDebit(), glJournalLine.getJlAmount(), AD_CMPNY);

              }

            }

            if (glSubsequentSetOfBook.getSobYearEndClosed() == 0)
              break;

          }

        }

      }

      invAdjustment.setAdjPosted(EJBCommon.TRUE);

    } catch (GlJREffectiveDateNoPeriodExistException ex) {

      getSessionContext().setRollbackOnly();
      throw ex;

    } catch (GlJREffectiveDatePeriodClosedException ex) {

      getSessionContext().setRollbackOnly();
      throw ex;

    } catch (GlobalJournalNotBalanceException ex) {

      getSessionContext().setRollbackOnly();
      throw ex;

    } catch (GlobalRecordAlreadyDeletedException ex) {

      getSessionContext().setRollbackOnly();
      throw ex;

    } catch (GlobalTransactionAlreadyPostedException ex) {

      getSessionContext().setRollbackOnly();
      throw ex;

    } catch (Exception ex) {

      Debug.printStackTrace(ex);
      getSessionContext().setRollbackOnly();
      throw new EJBException(ex.getMessage());

    }

  }

  private LocalInvAdjustmentLine addInvAlEntry(
      LocalInvItemLocation invItemLocation, LocalInvAdjustment invAdjustment, double CST_VRNC_VL,
      byte AL_VD, Integer AD_CMPNY
  ) {

    Debug.print("ArMiscReceiptEntryControllerBean addInvAlEntry");

    LocalInvAdjustmentLineHome invAdjustmentLineHome = null;

    // Initialize EJB Home

    try {

      invAdjustmentLineHome = (LocalInvAdjustmentLineHome) EJBHomeFactory
          .lookUpLocalHome(LocalInvAdjustmentLineHome.JNDI_NAME, LocalInvAdjustmentLineHome.class);

    } catch (NamingException ex) {

      throw new EJBException(ex.getMessage());

    }

    try {

      // create dr entry
      LocalInvAdjustmentLine invAdjustmentLine = null;
      invAdjustmentLine =
          invAdjustmentLineHome.create(CST_VRNC_VL, null, null, 0, 0, AL_VD, AD_CMPNY);

      // map adjustment, unit of measure, item location
      // invAdjustment.addInvAdjustmentLine(invAdjustmentLine);
      invAdjustmentLine.setInvAdjustment(invAdjustment);
      // invItemLocation.getInvItem().getInvUnitOfMeasure().addInvAdjustmentLine(invAdjustmentLine);
      invAdjustmentLine.setInvItemLocation(invItemLocation);
      // invItemLocation.addInvAdjustmentLine(invAdjustmentLine);
      invAdjustmentLine.setInvItemLocation(invItemLocation);

      return invAdjustmentLine;

    } catch (Exception ex) {

      Debug.printStackTrace(ex);
      getSessionContext().setRollbackOnly();
      throw new EJBException(ex.getMessage());

    }

  }

  private LocalInvAdjustment saveInvAdjustment(
      String ADJ_RFRNC_NMBR, String ADJ_DSCRPTN, Date ADJ_DATE, String USR_NM, Integer AD_BRNCH,
      Integer AD_CMPNY
  ) {

    Debug.print("ArMiscReceiptEntryControllerBean saveInvAdjustment");

    LocalInvAdjustmentHome invAdjustmentHome = null;
    LocalAdDocumentSequenceAssignmentHome adDocumentSequenceAssignmentHome = null;
    LocalAdBranchDocumentSequenceAssignmentHome adBranchDocumentSequenceAssignmentHome = null;
    // Initialize EJB Home

    try {

      invAdjustmentHome = (LocalInvAdjustmentHome) EJBHomeFactory
          .lookUpLocalHome(LocalInvAdjustmentHome.JNDI_NAME, LocalInvAdjustmentHome.class);
      adDocumentSequenceAssignmentHome = (LocalAdDocumentSequenceAssignmentHome) EJBHomeFactory
          .lookUpLocalHome(LocalAdDocumentSequenceAssignmentHome.JNDI_NAME,
              LocalAdDocumentSequenceAssignmentHome.class);
      adBranchDocumentSequenceAssignmentHome =
          (LocalAdBranchDocumentSequenceAssignmentHome) EJBHomeFactory.lookUpLocalHome(
              LocalAdBranchDocumentSequenceAssignmentHome.JNDI_NAME,
              LocalAdBranchDocumentSequenceAssignmentHome.class);

    } catch (NamingException ex) {

      throw new EJBException(ex.getMessage());

    }

    try {

      // generate adj document number
      String ADJ_DCMNT_NMBR = null;

      LocalAdBranchDocumentSequenceAssignment adBranchDocumentSequenceAssignment = null;
      LocalAdDocumentSequenceAssignment adDocumentSequenceAssignment = null;

      try {

        adDocumentSequenceAssignment =
            adDocumentSequenceAssignmentHome.findByDcName("INV ADJUSTMENT", AD_CMPNY);

      } catch (FinderException ex) {

      }

      try {

        adBranchDocumentSequenceAssignment =
            adBranchDocumentSequenceAssignmentHome.findBdsByDsaCodeAndBrCode(
                adDocumentSequenceAssignment.getDsaCode(), AD_BRNCH, AD_CMPNY);

      } catch (FinderException ex) {

      }

      while (true) {

        if (adBranchDocumentSequenceAssignment == null
            || adBranchDocumentSequenceAssignment.getBdsNextSequence() == null) {

          try {

            invAdjustmentHome.findByAdjDocumentNumberAndBrCode(
                adDocumentSequenceAssignment.getDsaNextSequence(), AD_BRNCH, AD_CMPNY);
            adDocumentSequenceAssignment.setDsaNextSequence(
                EJBCommon.incrementStringNumber(adDocumentSequenceAssignment.getDsaNextSequence()));

          } catch (FinderException ex) {

            ADJ_DCMNT_NMBR = adDocumentSequenceAssignment.getDsaNextSequence();
            adDocumentSequenceAssignment.setDsaNextSequence(
                EJBCommon.incrementStringNumber(adDocumentSequenceAssignment.getDsaNextSequence()));
            break;

          }

        } else {

          try {

            invAdjustmentHome.findByAdjDocumentNumberAndBrCode(
                adBranchDocumentSequenceAssignment.getBdsNextSequence(), AD_BRNCH, AD_CMPNY);
            adBranchDocumentSequenceAssignment.setBdsNextSequence(EJBCommon
                .incrementStringNumber(adBranchDocumentSequenceAssignment.getBdsNextSequence()));

          } catch (FinderException ex) {

            ADJ_DCMNT_NMBR = adBranchDocumentSequenceAssignment.getBdsNextSequence();
            adBranchDocumentSequenceAssignment.setBdsNextSequence(EJBCommon
                .incrementStringNumber(adBranchDocumentSequenceAssignment.getBdsNextSequence()));
            break;

          }

        }

      }

      LocalInvAdjustment invAdjustment = invAdjustmentHome.create(ADJ_DCMNT_NMBR, ADJ_RFRNC_NMBR,
          ADJ_DSCRPTN, ADJ_DATE, "COST-VARIANCE", "N/A", EJBCommon.FALSE, USR_NM, ADJ_DATE, USR_NM,
          ADJ_DATE, null, null, USR_NM, ADJ_DATE, null, null, EJBCommon.TRUE, EJBCommon.FALSE,
          AD_BRNCH, AD_CMPNY);

      return invAdjustment;

    } catch (Exception ex) {

      Debug.printStackTrace(ex);
      getSessionContext().setRollbackOnly();
      throw new EJBException(ex.getMessage());

    }


  }

  private void postInvAdjustmentToInventory(
      LocalInvAdjustmentLine invAdjustmentLine, Date CST_DT, double CST_ADJST_QTY,
      double CST_ADJST_CST, double CST_RMNNG_QTY, double CST_RMNNG_VL, Integer AD_BRNCH,
      Integer AD_CMPNY
  ) {

    Debug.print("ArMiscReceiptEntryControllerBean postInvAdjustmentToInventory");

    LocalInvCostingHome invCostingHome = null;
    LocalAdPreferenceHome adPreferenceHome = null;
    LocalAdCompanyHome adCompanyHome = null;

    // Initialize EJB Home

    try {

      invCostingHome = (LocalInvCostingHome) EJBHomeFactory
          .lookUpLocalHome(LocalInvCostingHome.JNDI_NAME, LocalInvCostingHome.class);
      adPreferenceHome = (LocalAdPreferenceHome) EJBHomeFactory
          .lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);
      adCompanyHome = (LocalAdCompanyHome) EJBHomeFactory
          .lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);

    } catch (NamingException ex) {

      getSessionContext().setRollbackOnly();
      throw new EJBException(ex.getMessage());

    }

    try {

      LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);
      LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
      LocalInvItemLocation invItemLocation = invAdjustmentLine.getInvItemLocation();
      int CST_LN_NMBR = 0;

      CST_ADJST_QTY =
          EJBCommon.roundIt(CST_ADJST_QTY, adPreference.getPrfInvQuantityPrecisionUnit());
      CST_ADJST_CST =
          EJBCommon.roundIt(CST_ADJST_CST, adCompany.getGlFunctionalCurrency().getFcPrecision());
      CST_RMNNG_QTY =
          EJBCommon.roundIt(CST_RMNNG_QTY, adPreference.getPrfInvQuantityPrecisionUnit());
      CST_RMNNG_VL =
          EJBCommon.roundIt(CST_RMNNG_VL, adCompany.getGlFunctionalCurrency().getFcPrecision());

      if (CST_ADJST_QTY < 0) {

        invItemLocation.setIlCommittedQuantity(
            invItemLocation.getIlCommittedQuantity() - Math.abs(CST_ADJST_QTY));

      }

      // create costing

      try {

        // generate line number

        LocalInvCosting invCurrentCosting =
            invCostingHome.getByMaxCstLineNumberAndCstDateToLongAndIiNameAndLocName(
                CST_DT.getTime(), invItemLocation.getInvItem().getIiName(),
                invItemLocation.getInvLocation().getLocName(), AD_BRNCH, AD_CMPNY);
        CST_LN_NMBR = invCurrentCosting.getCstLineNumber() + 1;

      } catch (FinderException ex) {

        CST_LN_NMBR = 1;

      }

      LocalInvCosting invCosting = invCostingHome.create(CST_DT, CST_DT.getTime(), CST_LN_NMBR, 0d,
          0d, 0d, 0d, CST_ADJST_QTY, CST_ADJST_CST, 0d, 0d, CST_RMNNG_QTY, CST_RMNNG_VL, 0d, 0d,
          CST_ADJST_QTY > 0 ? CST_ADJST_QTY : 0, AD_BRNCH, AD_CMPNY);
      // invItemLocation.addInvCosting(invCosting);
      invCosting.setInvItemLocation(invItemLocation);
      invCosting.setInvAdjustmentLine(invAdjustmentLine);

      // propagate balance if necessary

      Collection invCostings = invCostingHome.findByGreaterThanCstDateAndIiNameAndLocName(CST_DT,
          invItemLocation.getInvItem().getIiName(), invItemLocation.getInvLocation().getLocName(),
          AD_BRNCH, AD_CMPNY);

      Iterator i = invCostings.iterator();

      while (i.hasNext()) {

        LocalInvCosting invPropagatedCosting = (LocalInvCosting) i.next();

        invPropagatedCosting.setCstRemainingQuantity(
            invPropagatedCosting.getCstRemainingQuantity() + CST_ADJST_QTY);
        invPropagatedCosting
            .setCstRemainingValue(invPropagatedCosting.getCstRemainingValue() + CST_ADJST_CST);

      }


    } catch (Exception ex) {

      Debug.printStackTrace(ex);
      getSessionContext().setRollbackOnly();
      throw new EJBException(ex.getMessage());

    }



  }


  /**
   * @ejb:interface-method view-type="remote"
   * @jboss:method-attributes read-only="true"
   **/
  public ArrayList getArInvoiceReportParameters(
      Integer AD_CMPNY
  ) {

    Debug.print("ArInvoiceEntryControllerBean getArInvoiceReportParameters");

    ArrayList list = new ArrayList();
    LocalAdLookUpValueHome adLookUpValueHome = null;


    // Initialize EJB Home

    try {

      adLookUpValueHome = (LocalAdLookUpValueHome) EJBHomeFactory
          .lookUpLocalHome(LocalAdLookUpValueHome.JNDI_NAME, LocalAdLookUpValueHome.class);

    } catch (NamingException ex) {

      throw new EJBException(ex.getMessage());

    }


    try {

      Collection adLookUpValues =
          adLookUpValueHome.findByLuName("AR PRINT INVOICE PARAMETER", AD_CMPNY);


      if (adLookUpValues.size() <= 0) {
        return list;
      }

      Iterator i = adLookUpValues.iterator();

      while (i.hasNext()) {

        LocalAdLookUpValue adLookUpValue = (LocalAdLookUpValue) i.next();
        System.out.println(adLookUpValue.getLvName());
        list.add(adLookUpValue.getLvName());
      }


      return list;



    } catch (Exception ex) {

      Debug.printStackTrace(ex);
      throw new EJBException(ex.getMessage());

    }

  }


  /**
   * @ejb:interface-method view-type="remote"
   * @jboss:method-attributes read-only="true"
   **/
  public boolean getArTraceMisc(
      String II_NAME, Integer AD_CMPNY
  ) {

    Debug.print("ArInvoiceEntryControllerBean getArTraceMisc");

    LocalInvLocationHome invLocationHome = null;
    LocalInvItemHome invItemHome = null;
    Collection invLocations = null;
    ArrayList list = new ArrayList();
    boolean isTraceMisc = false;
    // Initialize EJB Home

    try {

      invLocationHome = (LocalInvLocationHome) EJBHomeFactory
          .lookUpLocalHome(LocalInvLocationHome.JNDI_NAME, LocalInvLocationHome.class);
      invItemHome = (LocalInvItemHome) EJBHomeFactory.lookUpLocalHome(LocalInvItemHome.JNDI_NAME,
          LocalInvItemHome.class);

    } catch (NamingException ex) {

      throw new EJBException(ex.getMessage());

    }

    try {

      LocalInvItem invItem = invItemHome.findByIiName(II_NAME, AD_CMPNY);
      if (invItem.getIiTraceMisc() == 1) {
        isTraceMisc = true;
      }
      return isTraceMisc;
    } catch (Exception ex) {

      Debug.printStackTrace(ex);
      throw new EJBException(ex.getMessage());

    }

  }



  private void createInvTagList(
      LocalArInvoiceLineItem arInvoiceLineItem, ArrayList list, Integer AD_CMPNY
  ) throws Exception {

    Debug.print("ArInvoiceEntryControllerBean createInvTagList");

    LocalAdUserHome adUserHome = null;
    LocalInvTagHome invTagHome = null;

    // Initialize EJB Home

    try {
      adUserHome = (LocalAdUserHome) EJBHomeFactory.lookUpLocalHome(LocalAdUserHome.JNDI_NAME,
          LocalAdUserHome.class);
      invTagHome = (LocalInvTagHome) EJBHomeFactory.lookUpLocalHome(LocalInvTagHome.JNDI_NAME,
          LocalInvTagHome.class);

    } catch (NamingException ex) {

      throw new EJBException(ex.getMessage());

    }



    try {
      System.out.println("aabot?");
      // Iterator t = apPurchaseOrderLine.getInvTag().iterator();
      Iterator t = list.iterator();

      LocalInvTag invTag = null;
      System.out.println("umabot?");
      while (t.hasNext()) {
        InvModTagListDetails tgLstDetails = (InvModTagListDetails) t.next();


        System.out.println(tgLstDetails.getTgCustodian() + "<== custodian");
        System.out.println(tgLstDetails.getTgSpecs() + "<== specs");
        System.out.println(tgLstDetails.getTgPropertyCode() + "<== propertyCode");
        System.out.println(tgLstDetails.getTgExpiryDate() + "<== expiryDate");
        System.out.println(tgLstDetails.getTgSerialNumber() + "<== serial number");

        if (tgLstDetails.getTgCode() == null) {
          System.out.println("ngcreate ba?");
          invTag =
              invTagHome.create(tgLstDetails.getTgPropertyCode(), tgLstDetails.getTgSerialNumber(),
                  null, tgLstDetails.getTgExpiryDate(), tgLstDetails.getTgSpecs(), AD_CMPNY,
                  tgLstDetails.getTgTransactionDate(), tgLstDetails.getTgType());

          invTag.setArInvoiceLineItem(arInvoiceLineItem);
          invTag.setInvItemLocation(arInvoiceLineItem.getInvItemLocation());
          LocalAdUser adUser = null;
          try {
            adUser = adUserHome.findByUsrName(tgLstDetails.getTgCustodian(), AD_CMPNY);
          } catch (FinderException ex) {

          }
          invTag.setAdUser(adUser);
          System.out.println("ngcreate ba?");
        }

      }



    } catch (Exception ex) {
      throw ex;
    }
  }


  private void createInvTagList(
      LocalArSalesOrderInvoiceLine arSalesOrderInvoiceLine, ArrayList list, Integer AD_CMPNY
  ) throws Exception {

    Debug.print("ArInvoiceEntryControllerBean createInvTagList");

    LocalAdUserHome adUserHome = null;
    LocalInvTagHome invTagHome = null;

    // Initialize EJB Home

    try {
      adUserHome = (LocalAdUserHome) EJBHomeFactory.lookUpLocalHome(LocalAdUserHome.JNDI_NAME,
          LocalAdUserHome.class);
      invTagHome = (LocalInvTagHome) EJBHomeFactory.lookUpLocalHome(LocalInvTagHome.JNDI_NAME,
          LocalInvTagHome.class);

    } catch (NamingException ex) {

      throw new EJBException(ex.getMessage());

    }



    try {
      System.out.println("aabot?");
      // Iterator t = apPurchaseOrderLine.getInvTag().iterator();
      Iterator t = list.iterator();

      LocalInvTag invTag = null;
      System.out.println("umabot?");
      while (t.hasNext()) {
        InvModTagListDetails tgLstDetails = (InvModTagListDetails) t.next();
        System.out.println(tgLstDetails.getTgCustodian() + "<== custodian");
        System.out.println(tgLstDetails.getTgSpecs() + "<== specs");
        System.out.println(tgLstDetails.getTgPropertyCode() + "<== propertyCode");
        System.out.println(tgLstDetails.getTgExpiryDate() + "<== expiryDate");
        System.out.println(tgLstDetails.getTgSerialNumber() + "<== serial number");

        if (tgLstDetails.getTgCode() == null) {
          System.out.println("ngcreate ba?");
          invTag =
              invTagHome.create(tgLstDetails.getTgPropertyCode(), tgLstDetails.getTgSerialNumber(),
                  null, tgLstDetails.getTgExpiryDate(), tgLstDetails.getTgSpecs(), AD_CMPNY,
                  tgLstDetails.getTgTransactionDate(), tgLstDetails.getTgType());

          invTag.setArSalesOrderInvoiceLine(arSalesOrderInvoiceLine);
          invTag.setInvItemLocation(
              arSalesOrderInvoiceLine.getArSalesOrderLine().getInvItemLocation());
          LocalAdUser adUser = null;
          try {
            adUser = adUserHome.findByUsrName(tgLstDetails.getTgCustodian(), AD_CMPNY);
          } catch (FinderException ex) {

          }
          invTag.setAdUser(adUser);
          System.out.println("ngcreate ba?");
        }

      }



    } catch (Exception ex) {
      throw ex;
    }
  }



  private void createInvTagList(
      LocalArJobOrderInvoiceLine arJobOrderInvoiceLine, ArrayList list, Integer AD_CMPNY
  ) throws Exception {

    Debug.print("ArInvoiceEntryControllerBean createInvTagList");

    LocalAdUserHome adUserHome = null;
    LocalInvTagHome invTagHome = null;

    // Initialize EJB Home

    try {
      adUserHome = (LocalAdUserHome) EJBHomeFactory.lookUpLocalHome(LocalAdUserHome.JNDI_NAME,
          LocalAdUserHome.class);
      invTagHome = (LocalInvTagHome) EJBHomeFactory.lookUpLocalHome(LocalInvTagHome.JNDI_NAME,
          LocalInvTagHome.class);

    } catch (NamingException ex) {

      throw new EJBException(ex.getMessage());

    }



    try {
      System.out.println("aabot?");
      // Iterator t = apPurchaseOrderLine.getInvTag().iterator();
      Iterator t = list.iterator();

      LocalInvTag invTag = null;
      System.out.println("umabot?");
      while (t.hasNext()) {
        InvModTagListDetails tgLstDetails = (InvModTagListDetails) t.next();
        System.out.println(tgLstDetails.getTgCustodian() + "<== custodian");
        System.out.println(tgLstDetails.getTgSpecs() + "<== specs");
        System.out.println(tgLstDetails.getTgPropertyCode() + "<== propertyCode");
        System.out.println(tgLstDetails.getTgExpiryDate() + "<== expiryDate");
        System.out.println(tgLstDetails.getTgSerialNumber() + "<== serial number");

        if (tgLstDetails.getTgCode() == null) {
          System.out.println("ngcreate ba?");
          invTag =
              invTagHome.create(tgLstDetails.getTgPropertyCode(), tgLstDetails.getTgSerialNumber(),
                  null, tgLstDetails.getTgExpiryDate(), tgLstDetails.getTgSpecs(), AD_CMPNY,
                  tgLstDetails.getTgTransactionDate(), tgLstDetails.getTgType());

          invTag.setArJobOrderInvoiceLine(arJobOrderInvoiceLine);
          invTag.setInvItemLocation(arJobOrderInvoiceLine.getArJobOrderLine().getInvItemLocation());
          LocalAdUser adUser = null;
          try {
            adUser = adUserHome.findByUsrName(tgLstDetails.getTgCustodian(), AD_CMPNY);
          } catch (FinderException ex) {

          }
          invTag.setAdUser(adUser);
          System.out.println("ngcreate ba?");
        }

      }



    } catch (Exception ex) {
      throw ex;
    }
  }



  private ArrayList getInvTagList(
      LocalArSalesOrderLine arSalesOrderLine
  ) {

    ArrayList list = new ArrayList();

    Collection invTags = arSalesOrderLine.getInvTags();
    Iterator x = invTags.iterator();
    while (x.hasNext()) {
      LocalInvTag invTag = (LocalInvTag) x.next();
      InvModTagListDetails tgLstDetails = new InvModTagListDetails();
      tgLstDetails.setTgPropertyCode(invTag.getTgPropertyCode());
      tgLstDetails.setTgSpecs(invTag.getTgSpecs());
      tgLstDetails.setTgExpiryDate(invTag.getTgExpiryDate());
      tgLstDetails.setTgSerialNumber(invTag.getTgSerialNumber());
      try {

        tgLstDetails.setTgCustodian(invTag.getAdUser().getUsrName());
      } catch (Exception ex) {
        tgLstDetails.setTgCustodian("");
      }

      list.add(tgLstDetails);

      System.out
          .println(tgLstDetails.getTgPropertyCode() + "<== property code inside controllerbean ");
      System.out.println(tgLstDetails.getTgSpecs() + "<== specs inside controllerbean ");
      System.out.println(list + "<== taglist inside controllerbean ");

    }

    return list;

  }



  private ArrayList getInvTagList(
      LocalArJobOrderLine arJobOrderLine
  ) {

    ArrayList list = new ArrayList();

    Collection invTags = arJobOrderLine.getInvTags();
    Iterator x = invTags.iterator();
    while (x.hasNext()) {
      LocalInvTag invTag = (LocalInvTag) x.next();
      InvModTagListDetails tgLstDetails = new InvModTagListDetails();
      tgLstDetails.setTgPropertyCode(invTag.getTgPropertyCode());
      tgLstDetails.setTgSpecs(invTag.getTgSpecs());
      tgLstDetails.setTgExpiryDate(invTag.getTgExpiryDate());
      tgLstDetails.setTgSerialNumber(invTag.getTgSerialNumber());
      try {

        tgLstDetails.setTgCustodian(invTag.getAdUser().getUsrName());
      } catch (Exception ex) {
        tgLstDetails.setTgCustodian("");
      }

      list.add(tgLstDetails);

      System.out
          .println(tgLstDetails.getTgPropertyCode() + "<== property code inside controllerbean ");
      System.out.println(tgLstDetails.getTgSpecs() + "<== specs inside controllerbean ");
      System.out.println(list + "<== taglist inside controllerbean ");

    }

    return list;

  }



  private ArrayList getInvTagList(
      LocalArJobOrderInvoiceLine arJobOrderInvoiceLine
  ) {

    ArrayList list = new ArrayList();

    Collection invTags = arJobOrderInvoiceLine.getInvTags();
    Iterator x = invTags.iterator();
    while (x.hasNext()) {
      LocalInvTag invTag = (LocalInvTag) x.next();
      InvModTagListDetails tgLstDetails = new InvModTagListDetails();
      tgLstDetails.setTgPropertyCode(invTag.getTgPropertyCode());
      tgLstDetails.setTgSpecs(invTag.getTgSpecs());
      tgLstDetails.setTgExpiryDate(invTag.getTgExpiryDate());
      tgLstDetails.setTgSerialNumber(invTag.getTgSerialNumber());
      try {

        tgLstDetails.setTgCustodian(invTag.getAdUser().getUsrName());
      } catch (Exception ex) {
        tgLstDetails.setTgCustodian("");
      }

      list.add(tgLstDetails);

      System.out
          .println(tgLstDetails.getTgPropertyCode() + "<== property code inside controllerbean ");
      System.out.println(tgLstDetails.getTgSpecs() + "<== specs inside controllerbean ");
      System.out.println(list + "<== taglist inside controllerbean ");

    }

    return list;

  }


  private ArrayList getInvTagList(
      LocalArSalesOrderInvoiceLine arSalsOrderInvoiceLine
  ) {

    ArrayList list = new ArrayList();

    Collection invTags = arSalsOrderInvoiceLine.getInvTags();
    Iterator x = invTags.iterator();
    while (x.hasNext()) {
      LocalInvTag invTag = (LocalInvTag) x.next();
      InvModTagListDetails tgLstDetails = new InvModTagListDetails();
      tgLstDetails.setTgPropertyCode(invTag.getTgPropertyCode());
      tgLstDetails.setTgSpecs(invTag.getTgSpecs());
      tgLstDetails.setTgExpiryDate(invTag.getTgExpiryDate());
      tgLstDetails.setTgSerialNumber(invTag.getTgSerialNumber());
      try {

        tgLstDetails.setTgCustodian(invTag.getAdUser().getUsrName());
      } catch (Exception ex) {
        tgLstDetails.setTgCustodian("");
      }

      list.add(tgLstDetails);

      System.out
          .println(tgLstDetails.getTgPropertyCode() + "<== property code inside controllerbean ");
      System.out.println(tgLstDetails.getTgSpecs() + "<== specs inside controllerbean ");
      System.out.println(list + "<== taglist inside controllerbean ");

    }

    return list;

  }



  private ArrayList getInvTagList(
      LocalArInvoiceLineItem arInvoiceLineItem
  ) {

    ArrayList list = new ArrayList();

    Collection invTags = arInvoiceLineItem.getInvTags();
    Iterator x = invTags.iterator();
    while (x.hasNext()) {
      LocalInvTag invTag = (LocalInvTag) x.next();
      InvModTagListDetails tgLstDetails = new InvModTagListDetails();
      tgLstDetails.setTgPropertyCode(invTag.getTgPropertyCode());
      tgLstDetails.setTgSpecs(invTag.getTgSpecs());
      tgLstDetails.setTgExpiryDate(invTag.getTgExpiryDate());
      tgLstDetails.setTgSerialNumber(invTag.getTgSerialNumber());
      try {

        tgLstDetails.setTgCustodian(invTag.getAdUser().getUsrName());
      } catch (Exception ex) {
        tgLstDetails.setTgCustodian("");
      }

      list.add(tgLstDetails);

      System.out
          .println(tgLstDetails.getTgPropertyCode() + "<== property code inside controllerbean ");
      System.out.println(tgLstDetails.getTgSpecs() + "<== specs inside controllerbean ");
      System.out.println(list + "<== taglist inside controllerbean ");

    }

    return list;

  }



  /**
   * @ejb:interface-method view-type="remote"
   * @jboss:method-attributes read-only="true"
   **/
  public ArrayList getDocumentTypeList(
      String DCMNT_TYP, Integer AD_CMPNY
  ) {

    Debug.print("ArInvoiceEntryControllerBean getDocumentTypeList");

    ArrayList list = new ArrayList();
    LocalAdLookUpValueHome adLookUpValueHome = null;


    // Initialize EJB Home

    try {

      adLookUpValueHome = (LocalAdLookUpValueHome) EJBHomeFactory
          .lookUpLocalHome(LocalAdLookUpValueHome.JNDI_NAME, LocalAdLookUpValueHome.class);

    } catch (NamingException ex) {

      throw new EJBException(ex.getMessage());

    }


    try {

      Collection adLookUpValues =
          adLookUpValueHome.findByLuName("AR INVOICE DOCUMENT TYPE", AD_CMPNY);

      if (adLookUpValues.size() <= 0) {
        return list;
      }

      Iterator i = adLookUpValues.iterator();

      while (i.hasNext()) {

        LocalAdLookUpValue adLookUpValue = (LocalAdLookUpValue) i.next();
        System.out.println(adLookUpValue.getLvName());
        list.add(adLookUpValue.getLvName());
      }


      return list;



    } catch (Exception ex) {

      Debug.printStackTrace(ex);
      throw new EJBException(ex.getMessage());

    }

  }

  // SessionBean methods

  /**
   * @ejb:create-method view-type="remote"
   **/
  public void ejbCreate(
  ) throws CreateException {

    Debug.print("ArInvoiceEntryControllerBean ejbCreate");

  }

}
