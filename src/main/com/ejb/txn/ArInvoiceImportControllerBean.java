package com.ejb.txn;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.GregorianCalendar;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;
import javax.naming.NamingException;

import com.ejb.ad.LocalAdAmountLimitHome;
import com.ejb.ad.LocalAdApprovalHome;
import com.ejb.ad.LocalAdApprovalQueueHome;
import com.ejb.ad.LocalAdApprovalUserHome;
import com.ejb.ad.LocalAdCompany;
import com.ejb.ad.LocalAdCompanyHome;
import com.ejb.ad.LocalAdDiscount;
import com.ejb.ad.LocalAdDiscountHome;
import com.ejb.ad.LocalAdPaymentSchedule;
import com.ejb.ad.LocalAdPaymentScheduleHome;
import com.ejb.ad.LocalAdPaymentTerm;
import com.ejb.ad.LocalAdPaymentTermHome;
import com.ejb.ad.LocalAdPreference;
import com.ejb.ad.LocalAdPreferenceHome;
import com.ejb.ar.LocalArCustomer;
import com.ejb.ar.LocalArCustomerBalance;
import com.ejb.ar.LocalArCustomerBalanceHome;
import com.ejb.ar.LocalArCustomerHome;
import com.ejb.ar.LocalArDistributionRecord;
import com.ejb.ar.LocalArDistributionRecordHome;
import com.ejb.ar.LocalArInvoice;
import com.ejb.ar.LocalArInvoiceBatch;
import com.ejb.ar.LocalArInvoiceBatchHome;
import com.ejb.ar.LocalArInvoiceHome;
import com.ejb.ar.LocalArInvoiceLine;
import com.ejb.ar.LocalArInvoiceLineItem;
import com.ejb.ar.LocalArInvoiceLineItemHome;
import com.ejb.ar.LocalArInvoicePaymentSchedule;
import com.ejb.ar.LocalArInvoicePaymentScheduleHome;
import com.ejb.ar.LocalArReceipt;
import com.ejb.ar.LocalArReceiptHome;
import com.ejb.ar.LocalArSalesOrderInvoiceLine;
import com.ejb.ar.LocalArTaxCode;
import com.ejb.ar.LocalArTaxCodeHome;
import com.ejb.ar.LocalArWithholdingTaxCode;
import com.ejb.ar.LocalArWithholdingTaxCodeHome;
import com.ejb.exception.ArINVAmountExceedsCreditLimitException;
import com.ejb.exception.GlobalConversionDateNotExistException;
import com.ejb.exception.GlobalDocumentNumberNotUniqueException;
import com.ejb.exception.GlobalInvItemLocationNotFoundException;
import com.ejb.exception.GlobalInventoryDateException;
import com.ejb.exception.GlobalJournalNotBalanceException;
import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.exception.GlobalPaymentTermInvalidException;
import com.ejb.exception.GlobalRecordAlreadyDeletedException;
import com.ejb.exception.GlobalTransactionAlreadyApprovedException;
import com.ejb.exception.GlobalTransactionAlreadyPendingException;
import com.ejb.exception.GlobalTransactionAlreadyPostedException;
import com.ejb.exception.GlobalTransactionAlreadyVoidException;
import com.ejb.gl.LocalAdDocumentSequenceAssignmentHome;
import com.ejb.gl.LocalGlChartOfAccount;
import com.ejb.gl.LocalGlChartOfAccountHome;
import com.ejb.gl.LocalGlFunctionalCurrency;
import com.ejb.gl.LocalGlFunctionalCurrencyHome;
import com.ejb.gl.LocalGlFunctionalCurrencyRateHome;
import com.ejb.inv.LocalInvBillOfMaterial;
import com.ejb.inv.LocalInvCosting;
import com.ejb.inv.LocalInvCostingHome;
import com.ejb.inv.LocalInvItem;
import com.ejb.inv.LocalInvItemHome;
import com.ejb.inv.LocalInvItemLocation;
import com.ejb.inv.LocalInvItemLocationHome;
import com.ejb.inv.LocalInvUnitOfMeasure;
import com.ejb.inv.LocalInvUnitOfMeasureHome;
import com.ejb.inv.LocalInvUnitOfMeasureConversion;
import com.ejb.inv.LocalInvUnitOfMeasureConversionHome;
import com.util.AbstractSessionBean;
import com.util.ArModInvoiceDetails;
import com.util.ArModInvoiceLineItemDetails;
import com.util.Debug;
import com.util.EJBCommon;
import com.util.EJBHomeFactory;

/**
 * @ejb:bean name="ArInvoiceImportControllerEJB"
 *           display-name="used for importing invoice CSV"
 *           type="Stateless"
 *           view-type="remote"
 *           jndi-name="ejb/ArInvoiceImportControllerEJB"
 *
 * @ejb:interface remote-class="com.ejb.txn.ArInvoiceImportController"
 *                extends="javax.ejb.EJBObject"
 *
 * @ejb:home remote-class="com.ejb.txn.ArInvoiceImportControllerHome"
 *           extends="javax.ejb.EJBHome"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="aruser"
 *                        role-link="aruserlink"
 *
 * @ejb:permission role-name="aruser"
 *
 */

public class ArInvoiceImportControllerBean extends AbstractSessionBean {

	/**
	 * @ejb:interface-method view-type="remote"
	 **/
	public void importInvoice(ArrayList list, String PYT_NM, String TC_NM,
			String WTC_NM, String FC_NM, String IB_NM, Integer AD_BRNCH, Integer AD_CMPNY) throws
			GlobalRecordAlreadyDeletedException,
			GlobalDocumentNumberNotUniqueException,
			GlobalConversionDateNotExistException,
			GlobalPaymentTermInvalidException,
			ArINVAmountExceedsCreditLimitException,
			GlobalTransactionAlreadyApprovedException,
			GlobalTransactionAlreadyPendingException,
			GlobalTransactionAlreadyPostedException,
			GlobalTransactionAlreadyVoidException,
			GlobalInvItemLocationNotFoundException,
			GlobalInventoryDateException,
			GlobalNoRecordFoundException,
			GlobalJournalNotBalanceException {

		Debug.print("ArInvoiceImportControllerBean importInvoice");

		LocalArInvoiceHome arInvoiceHome = null;
		LocalArInvoiceBatchHome arInvoiceBatchHome = null;
		LocalAdCompanyHome adCompanyHome = null;
		LocalGlFunctionalCurrencyHome glFunctionalCurrencyHome = null;
		LocalAdDocumentSequenceAssignmentHome adDocumentSequenceAssignmentHome = null;
		LocalAdPaymentTermHome adPaymentTermHome = null;
		LocalAdPaymentScheduleHome adPaymentScheduleHome = null;
		LocalAdDiscountHome adDiscountHome = null;
		LocalArTaxCodeHome arTaxCodeHome = null;
		LocalArWithholdingTaxCodeHome arWithholdingTaxCodeHome = null;
		LocalArCustomerHome arCustomerHome = null;
		LocalGlFunctionalCurrencyRateHome glFunctionalCurrencyRateHome = null;
		LocalArInvoicePaymentScheduleHome arInvoicePaymentScheduleHome = null;
		LocalAdApprovalHome adApprovalHome = null;
		LocalAdAmountLimitHome adAmountLimitHome = null;
		LocalAdApprovalUserHome adApprovalUserHome = null;
		LocalAdApprovalQueueHome adApprovalQueueHome = null;
		LocalAdPreferenceHome adPreferenceHome = null;
		LocalInvItemLocationHome invItemLocationHome = null;
		LocalInvCostingHome invCostingHome = null;
		LocalInvItemHome invItemHome = null;
		LocalGlChartOfAccountHome glChartOfAccountHome = null;
		LocalArCustomerBalanceHome arCustomerBalanceHome = null;
		LocalArReceiptHome arReceiptHome = null;

		LocalArInvoice arInvoice = null;

		// Initialize EJB Home

		try {

			arInvoiceHome = (LocalArInvoiceHome)EJBHomeFactory.
			lookUpLocalHome(LocalArInvoiceHome.JNDI_NAME, LocalArInvoiceHome.class);
			arInvoiceBatchHome = (LocalArInvoiceBatchHome)EJBHomeFactory.
			lookUpLocalHome(LocalArInvoiceBatchHome.JNDI_NAME, LocalArInvoiceBatchHome.class);
			adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
			glFunctionalCurrencyHome = (LocalGlFunctionalCurrencyHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlFunctionalCurrencyHome.JNDI_NAME, LocalGlFunctionalCurrencyHome.class);
			adDocumentSequenceAssignmentHome = (LocalAdDocumentSequenceAssignmentHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdDocumentSequenceAssignmentHome.JNDI_NAME, LocalAdDocumentSequenceAssignmentHome.class);
			adPaymentTermHome = (LocalAdPaymentTermHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdPaymentTermHome.JNDI_NAME, LocalAdPaymentTermHome.class);
			adPaymentScheduleHome = (LocalAdPaymentScheduleHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdPaymentScheduleHome.JNDI_NAME, LocalAdPaymentScheduleHome.class);
			adDiscountHome = (LocalAdDiscountHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdDiscountHome.JNDI_NAME, LocalAdDiscountHome.class);
			arTaxCodeHome = (LocalArTaxCodeHome)EJBHomeFactory.
			lookUpLocalHome(LocalArTaxCodeHome.JNDI_NAME, LocalArTaxCodeHome.class);
			arWithholdingTaxCodeHome = (LocalArWithholdingTaxCodeHome)EJBHomeFactory.
			lookUpLocalHome(LocalArWithholdingTaxCodeHome.JNDI_NAME, LocalArWithholdingTaxCodeHome.class);
			arCustomerHome = (LocalArCustomerHome)EJBHomeFactory.
			lookUpLocalHome(LocalArCustomerHome.JNDI_NAME, LocalArCustomerHome.class);
			glFunctionalCurrencyRateHome = (LocalGlFunctionalCurrencyRateHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlFunctionalCurrencyRateHome.JNDI_NAME, LocalGlFunctionalCurrencyRateHome.class);
			arInvoicePaymentScheduleHome = (LocalArInvoicePaymentScheduleHome)EJBHomeFactory.
			lookUpLocalHome(LocalArInvoicePaymentScheduleHome.JNDI_NAME, LocalArInvoicePaymentScheduleHome.class);
			adApprovalHome = (LocalAdApprovalHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdApprovalHome.JNDI_NAME, LocalAdApprovalHome.class);
			adAmountLimitHome = (LocalAdAmountLimitHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdAmountLimitHome.JNDI_NAME, LocalAdAmountLimitHome.class);
			adApprovalUserHome = (LocalAdApprovalUserHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdApprovalUserHome.JNDI_NAME, LocalAdApprovalUserHome.class);
			adApprovalQueueHome = (LocalAdApprovalQueueHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdApprovalQueueHome.JNDI_NAME, LocalAdApprovalQueueHome.class);
			adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);
			invItemLocationHome = (LocalInvItemLocationHome)EJBHomeFactory.
			lookUpLocalHome(LocalInvItemLocationHome.JNDI_NAME, LocalInvItemLocationHome.class);
			invCostingHome = (LocalInvCostingHome)EJBHomeFactory.
			lookUpLocalHome(LocalInvCostingHome.JNDI_NAME, LocalInvCostingHome.class);
			invItemHome = (LocalInvItemHome)EJBHomeFactory.
			lookUpLocalHome(LocalInvItemHome.JNDI_NAME, LocalInvItemHome.class);
			glChartOfAccountHome = (LocalGlChartOfAccountHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlChartOfAccountHome.JNDI_NAME, LocalGlChartOfAccountHome.class);
			arCustomerBalanceHome = (LocalArCustomerBalanceHome)EJBHomeFactory.
			lookUpLocalHome(LocalArCustomerBalanceHome.JNDI_NAME, LocalArCustomerBalanceHome.class);
			arReceiptHome = (LocalArReceiptHome)EJBHomeFactory.
			lookUpLocalHome(LocalArReceiptHome.JNDI_NAME, LocalArReceiptHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}

		try {

			Iterator hdrItr = list.iterator();

			while (hdrItr.hasNext()) {

				ArModInvoiceDetails details = (ArModInvoiceDetails)hdrItr.next();

				LocalArCustomer arInvCustomer = null;

				try {

					arInvCustomer = arCustomerHome.findByCstName(details.getInvCstName(), AD_BRNCH, AD_CMPNY);

				} catch (FinderException ex) {

					throw new GlobalNoRecordFoundException(details.getInvCstName());

				}

				String CST_CSTMR_CODE = arInvCustomer.getCstCustomerCode();

				// validate if document number is unique

				if (details.getInvCode() == null) {

					LocalArInvoice arExistingInvoice = null;

					try {

						arExistingInvoice = arInvoiceHome.findByInvNumberAndInvCreditMemoAndBrCode(
								details.getInvNumber(), EJBCommon.FALSE, AD_BRNCH, AD_CMPNY);

					} catch (FinderException ex) {

					}

					if (arExistingInvoice != null) {

						throw new GlobalDocumentNumberNotUniqueException(details.getInvNumber());

					}

				}

				// validate if payment term has at least one payment schedule

				if (adPaymentTermHome.findByPytName(PYT_NM, AD_CMPNY).getAdPaymentSchedules().isEmpty()) {

					throw new GlobalPaymentTermInvalidException();

				}

				// used in checking if invoice should re-generate distribution records and re-calculate taxes
				boolean isRecalculate = true;

				// create invoice

				if (details.getInvCode() == null) {

					arInvoice = arInvoiceHome.create(details.getInvType(),EJBCommon.FALSE,
							details.getInvDescription(), details.getInvDate(),
							details.getInvNumber(), details.getInvReferenceNumber(),details.getInvUploadNumber(), null, null,
							0d, 0d, 0d, 0d, 0d, 0d, details.getInvConversionDate(), details.getInvConversionRate(),
							details.getInvMemo(),
							0d, 0d, details.getInvBillToAddress(), details.getInvBillToContact(), details.getInvBillToAltContact(),
							details.getInvBillToPhone(), details.getInvBillingHeader(), details.getInvBillingFooter(),
							details.getInvBillingHeader2(), details.getInvBillingFooter2(), details.getInvBillingHeader3(),
							details.getInvBillingFooter3(), details.getInvBillingSignatory(), details.getInvSignatoryTitle(),
							details.getInvShipToAddress(), details.getInvShipToContact(), details.getInvShipToAltContact(),
							details.getInvShipToPhone(), details.getInvShipDate(), details.getInvLvFreight(),
							null, null,
							EJBCommon.FALSE,
							null, EJBCommon.FALSE, EJBCommon.FALSE,
							EJBCommon.FALSE,  EJBCommon.FALSE,
							EJBCommon.FALSE, null, 0d, null, null, null, null,
							details.getInvCreatedBy(), details.getInvDateCreated(),
							details.getInvLastModifiedBy(), details.getInvDateLastModified(),
							null, null, null, null, EJBCommon.FALSE, details.getInvLvShift(), null, null, EJBCommon.FALSE, EJBCommon.FALSE,
							null, details.getInvDate(), AD_BRNCH, AD_CMPNY);

				}

				LocalAdPaymentTerm adPaymentTerm = adPaymentTermHome.findByPytName(PYT_NM, AD_CMPNY);
				adPaymentTerm.addArInvoice(arInvoice);

				LocalGlFunctionalCurrency glFunctionalCurrency = glFunctionalCurrencyHome.findByFcName(FC_NM, AD_CMPNY);
				glFunctionalCurrency.addArInvoice(arInvoice);

				LocalArTaxCode arTaxCode = arTaxCodeHome.findByTcName(TC_NM, AD_CMPNY);
				arTaxCode.addArInvoice(arInvoice);

				LocalArWithholdingTaxCode arWithholdingTaxCode = arWithholdingTaxCodeHome.findByWtcName(WTC_NM, AD_CMPNY);
				arWithholdingTaxCode.addArInvoice(arInvoice);

				LocalArCustomer arCustomer = arCustomerHome.findByCstCustomerCode(CST_CSTMR_CODE, AD_CMPNY);
				arCustomer.addArInvoice(arInvoice);

				try {

					LocalArInvoiceBatch arInvoiceBatch = arInvoiceBatchHome.findByIbName(IB_NM, AD_BRNCH, AD_CMPNY);
					arInvoiceBatch.addArInvoice(arInvoice);

				} catch (FinderException ex) {

					// create new batch
					LocalArInvoiceBatch arInvoiceBatch = arInvoiceBatchHome.create(IB_NM, "", "OPEN", "INVOICE", details.getInvDateCreated(), details.getInvCreatedBy(), AD_BRNCH, AD_CMPNY);
					arInvoiceBatch.addArInvoice(arInvoice);

				}

				if (isRecalculate) {

					// remove all invoice line items

					LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);

					Collection arInvoiceLineItems = arInvoice.getArInvoiceLineItems();

					Iterator i = arInvoiceLineItems.iterator();

					while (i.hasNext()) {

						LocalArInvoiceLineItem arInvoiceLineItem = (LocalArInvoiceLineItem)i.next();

						if (arInvoiceLineItem.getIliEnableAutoBuild() == 1 && arInvoiceLineItem.getInvItemLocation().getInvItem().getIiClass().equals("Assembly")) {

						    double QTY_SLD = this.convertByUomFromAndItemAndQuantity(arInvoiceLineItem.getInvUnitOfMeasure(),
	                                arInvoiceLineItem.getInvItemLocation().getInvItem(), arInvoiceLineItem.getIliQuantity(), AD_CMPNY);

							Collection invBillOfMaterials = arInvoiceLineItem.getInvItemLocation().getInvItem().getInvBillOfMaterials();

							Iterator j = invBillOfMaterials.iterator();

							while (j.hasNext()) {

								LocalInvBillOfMaterial invBillOfMaterial = (LocalInvBillOfMaterial) j.next();

								LocalInvItemLocation invItemLocation = invItemLocationHome.findByLocNameAndIiName(invBillOfMaterial.getBomLocName(), invBillOfMaterial.getBomIiName(), AD_CMPNY);

								// bom conversion
	                            double convertedQuantity = this.convertByUomFromAndItemAndQuantity(
	                                   invBillOfMaterial.getInvUnitOfMeasure(), invItemLocation.getInvItem(),
	                                   EJBCommon.roundIt(invBillOfMaterial.getBomQuantityNeeded() * QTY_SLD,
	                                   this.getInvGpQuantityPrecisionUnit(AD_CMPNY)), AD_CMPNY);

							    invItemLocation.setIlCommittedQuantity(invItemLocation.getIlCommittedQuantity() - convertedQuantity);

							}

						} else {

							double convertedQuantity = this.convertByUomFromAndItemAndQuantity(arInvoiceLineItem.getInvUnitOfMeasure(), arInvoiceLineItem.getInvItemLocation().getInvItem(), arInvoiceLineItem.getIliQuantity(), AD_CMPNY);
							arInvoiceLineItem.getInvItemLocation().setIlCommittedQuantity(arInvoiceLineItem.getInvItemLocation().getIlCommittedQuantity() - convertedQuantity);

						}

						i.remove();

						arInvoiceLineItem.remove();

					}

					// remove all invoice lines

					Collection arInvoiceLines = arInvoice.getArInvoiceLines();

					i = arInvoiceLines.iterator();

					while (i.hasNext()) {

						LocalArInvoiceLine arInvoiceLine = (LocalArInvoiceLine)i.next();

						i.remove();

						arInvoiceLine.remove();

					}

					// remove all sales order lines

					Collection arSalesOrderInvoiceLines = arInvoice.getArSalesOrderInvoiceLines();

					i = arSalesOrderInvoiceLines.iterator();

					while (i.hasNext()) {

						LocalArSalesOrderInvoiceLine arSalesOrderInvoiceLine = (LocalArSalesOrderInvoiceLine)i.next();

						i.remove();

						arSalesOrderInvoiceLine.remove();

					}

					// remove all distribution records

					Collection arDistributionRecords = arInvoice.getArDistributionRecords();

					i = arDistributionRecords.iterator();

					while (i.hasNext()) {

						LocalArDistributionRecord arDistributionRecord = (LocalArDistributionRecord)i.next();

						i.remove();

						arDistributionRecord.remove();

					}

					// add new voucher line item and distribution record

					double TOTAL_TAX = 0d;
					double TOTAL_LINE = 0d;

					i = details.getInvIliList().iterator();

					while (i.hasNext()) {

						ArModInvoiceLineItemDetails mIliDetails = (ArModInvoiceLineItemDetails) i.next();

						Collection invItemLocations = null;

						try {

							invItemLocations = invItemLocationHome.findByIiName(mIliDetails.getIliIiName(), AD_CMPNY);

						} catch (FinderException ex) {

							throw new GlobalInvItemLocationNotFoundException(arInvoice.getInvNumber() + " - " + mIliDetails.getIliIiName());

						}

						if(invItemLocations == null || invItemLocations.isEmpty()) {

							throw new GlobalInvItemLocationNotFoundException(mIliDetails.getIliIiName());

						}

						LocalInvItemLocation invItemLocation = null;

						Iterator k = invItemLocations.iterator();

						while (k.hasNext()) {

							 invItemLocation = (LocalInvItemLocation)k.next();
							 break;

						}

						// set unit of measure

						LocalInvUnitOfMeasure invUnitOfMeasure = invItemLocation.getInvItem().getInvUnitOfMeasure();
						mIliDetails.setIliUomName(invUnitOfMeasure.getUomName());

						/* start date validation

						Collection invNegTxnCosting = invCostingHome.findNegTxnByGreaterThanCstDateAndIiNameAndLocName(
								arInvoice.getInvDate(), invItemLocation.getInvItem().getIiName(),
								invItemLocation.getInvLocation().getLocName(), AD_CMPNY);
						if(!invNegTxnCosting.isEmpty()) throw new GlobalInventoryDateException(invItemLocation.getInvItem().getIiName());
						*/

						LocalArInvoiceLineItem arInvoiceLineItem = this.addArIliEntry(mIliDetails, arInvoice, invItemLocation, AD_CMPNY);

						// add cost of sales distribution and inventory

						double COST = 0d;

						try {

							LocalInvCosting invCosting  = invCostingHome.getByMaxCstDateToLongAndMaxCstLineNumberAndLessThanEqualCstDateAndIiNameAndLocName(
									arInvoice.getInvDate(), invItemLocation.getInvItem().getIiName(), invItemLocation.getInvLocation().getLocName(), AD_BRNCH, AD_CMPNY);

							COST = Math.abs(invCosting.getCstRemainingValue() / invCosting.getCstRemainingQuantity());

						} catch (FinderException ex) {

							COST = arInvoiceLineItem.getInvItemLocation().getInvItem().getIiUnitCost();

						}

						double QTY_SLD = this.convertByUomFromAndItemAndQuantity(arInvoiceLineItem.getInvUnitOfMeasure(),
								arInvoiceLineItem.getInvItemLocation().getInvItem(), arInvoiceLineItem.getIliQuantity(), AD_CMPNY);

						if (arInvoiceLineItem.getIliEnableAutoBuild() == EJBCommon.FALSE) {

							this.addArDrIliEntry(arInvoice.getArDrNextLine(),
									"COGS", EJBCommon.TRUE, COST * QTY_SLD,
									arInvoiceLineItem.getInvItemLocation().getIlGlCoaCostOfSalesAccount(), arInvoice, AD_CMPNY);

							this.addArDrIliEntry(arInvoice.getArDrNextLine(),
									"INVENTORY", EJBCommon.FALSE, COST * QTY_SLD,
									arInvoiceLineItem.getInvItemLocation().getIlGlCoaInventoryAccount(), arInvoice, AD_CMPNY);

							// add quantity to item location committed quantity

							double convertedQuantity = this.convertByUomFromAndItemAndQuantity(arInvoiceLineItem.getInvUnitOfMeasure(), arInvoiceLineItem.getInvItemLocation().getInvItem(), arInvoiceLineItem.getIliQuantity(), AD_CMPNY);
							invItemLocation.setIlCommittedQuantity(invItemLocation.getIlCommittedQuantity() + convertedQuantity);

						}

						// add inventory sale distributions

						this.addArDrIliEntry(arInvoice.getArDrNextLine(),
								"REVENUE", EJBCommon.FALSE, arInvoiceLineItem.getIliAmount(),
								arInvoiceLineItem.getInvItemLocation().getIlGlCoaSalesAccount(), arInvoice, AD_CMPNY);


						TOTAL_LINE += arInvoiceLineItem.getIliAmount();
						TOTAL_TAX += arInvoiceLineItem.getIliTaxAmount();

						// if auto build is enable

						if (arInvoiceLineItem.getIliEnableAutoBuild() == EJBCommon.TRUE && arInvoiceLineItem.getInvItemLocation().getInvItem().getIiClass().equals("Assembly")) {

							byte DEBIT = EJBCommon.TRUE;

							double TOTAL_AMOUNT = 0;
							double ABS_TOTAL_AMOUNT = 0;

							Collection invBillOfMaterials = arInvoiceLineItem.getInvItemLocation().getInvItem().getInvBillOfMaterials();

							Iterator j = invBillOfMaterials.iterator();

							while (j.hasNext()) {

								LocalInvBillOfMaterial invBillOfMaterial = (LocalInvBillOfMaterial) j.next();

								// add bill of material quantity needed to item location

	                            LocalInvItemLocation invIlRawMaterial = null;

								try {

									invIlRawMaterial = invItemLocationHome.findByLocNameAndIiName(
											invBillOfMaterial.getBomLocName(), invBillOfMaterial.getBomIiName(), AD_CMPNY);

								} catch(FinderException ex) {

									throw new GlobalInvItemLocationNotFoundException(String.valueOf(mIliDetails.getIliLine()) +
											" - Raw Mat. (" +invBillOfMaterial.getBomIiName() + ")");

								}

								// bom conversion
	                            double convertedQuantity = this.convertByUomFromAndItemAndQuantity(
	                                    invBillOfMaterial.getInvUnitOfMeasure(), invIlRawMaterial.getInvItem(),
	                                    EJBCommon.roundIt(invBillOfMaterial.getBomQuantityNeeded() * QTY_SLD, this.getInvGpQuantityPrecisionUnit(AD_CMPNY)),
	                                    AD_CMPNY);


								/* start date validation

								Collection invIlRawMatNegTxnCosting = invCostingHome.findNegTxnByGreaterThanCstDateAndIiNameAndLocName(
										arInvoice.getInvDate(), invIlRawMaterial.getInvItem().getIiName(),
										invIlRawMaterial.getInvLocation().getLocName(),	AD_CMPNY);
								if(!invIlRawMatNegTxnCosting.isEmpty()) throw new GlobalInventoryDateException(
										invItemLocation.getInvItem().getIiName() + " - Raw Mat. (" + invIlRawMaterial.getInvItem().getIiName() + ")");
								*/

								invIlRawMaterial.setIlCommittedQuantity(invIlRawMaterial.getIlCommittedQuantity() + convertedQuantity);

								LocalInvItem invItem = invItemHome.findByIiName(invBillOfMaterial.getBomIiName(), AD_CMPNY);

								double COSTING = 0d;

								try {

									LocalInvCosting invCosting  = invCostingHome.getByMaxCstDateToLongAndMaxCstLineNumberAndLessThanEqualCstDateAndIiNameAndLocName(
											arInvoice.getInvDate(), invItem.getIiName(), invBillOfMaterial.getBomLocName(), AD_BRNCH, AD_CMPNY);

									COSTING = Math.abs(invCosting.getCstRemainingValue() / invCosting.getCstRemainingQuantity());

								} catch (FinderException ex) {

									COSTING = invItem.getIiUnitCost();

								}

								// bom conversion
								COSTING = this.convertCostByUom(invIlRawMaterial.getInvItem().getIiName(),
	                                    invBillOfMaterial.getInvUnitOfMeasure().getUomName(), COSTING, true, AD_CMPNY);

								double BOM_AMOUNT = EJBCommon.roundIt(
										(QTY_SLD * (invBillOfMaterial.getBomQuantityNeeded() * COSTING)),
										this.getGlFcPrecisionUnit(AD_CMPNY));

								TOTAL_AMOUNT += BOM_AMOUNT;
								ABS_TOTAL_AMOUNT += Math.abs(BOM_AMOUNT);

								// add inventory account

								this.addArDrIliEntry(arInvoice.getArDrNextLine(), "INVENTORY", EJBCommon.FALSE, Math.abs(BOM_AMOUNT),
										invIlRawMaterial.getIlGlCoaInventoryAccount(), arInvoice, AD_CMPNY);

							}

							// add cost of sales account

							this.addArDrIliEntry(arInvoice.getArDrNextLine(),
									"COGS", EJBCommon.TRUE, Math.abs(TOTAL_AMOUNT),
									arInvoiceLineItem.getInvItemLocation().getIlGlCoaCostOfSalesAccount(), arInvoice, AD_CMPNY);

						}

					}

					if (EJBCommon.roundIt(TOTAL_LINE + TOTAL_TAX, this.getGlFcPrecisionUnit(AD_CMPNY)) != details.getInvAmountDue()) {

						throw new GlobalJournalNotBalanceException(details.getInvNumber());

					}

					// add tax distribution if necessary

					if (!arTaxCode.getTcType().equals("NONE") &&
							!arTaxCode.getTcType().equals("EXEMPT")) {

						if (arTaxCode.getTcInterimAccount() == null) {

							this.addArDrEntry(arInvoice.getArDrNextLine(),
									"TAX", EJBCommon.FALSE, TOTAL_TAX, arTaxCode.getGlChartOfAccount().getCoaCode(),
									arInvoice, AD_CMPNY);

						} else {

							this.addArDrEntry(arInvoice.getArDrNextLine(),
									"DEFERRED TAX", EJBCommon.FALSE, TOTAL_TAX, arTaxCode.getTcInterimAccount(),
									arInvoice, AD_CMPNY);

						}

					}

					// add wtax distribution if necessary

					double W_TAX_AMOUNT = 0d;

					if (arWithholdingTaxCode.getWtcRate() != 0 && adPreference.getPrfArWTaxRealization().equals("INVOICE")) {

						W_TAX_AMOUNT = EJBCommon.roundIt(TOTAL_LINE * (arWithholdingTaxCode.getWtcRate() / 100), this.getGlFcPrecisionUnit(AD_CMPNY));

						this.addArDrEntry(arInvoice.getArDrNextLine(), "W-TAX",
								EJBCommon.TRUE, W_TAX_AMOUNT, arWithholdingTaxCode.getGlChartOfAccount().getCoaCode(),
								arInvoice, AD_CMPNY);

					}

					// add payment discount if necessary

					double DISCOUNT_AMT = 0d;

					if (adPaymentTerm.getPytDiscountOnInvoice() == EJBCommon.TRUE) {

						Collection adPaymentSchedules = adPaymentScheduleHome.findByPytCode(adPaymentTerm.getPytCode(),AD_CMPNY);
						ArrayList adPaymentScheduleList = new ArrayList(adPaymentSchedules);
						LocalAdPaymentSchedule adPaymentSchedule = (LocalAdPaymentSchedule)adPaymentScheduleList.get(0);

						Collection adDiscounts = adDiscountHome.findByPsCode(adPaymentSchedule.getPsCode(), AD_CMPNY);
						ArrayList adDiscountList = new ArrayList(adDiscounts);
						LocalAdDiscount adDiscount = (LocalAdDiscount)adDiscountList.get(0);

						double rate = adDiscount.getDscDiscountPercent();
						DISCOUNT_AMT = (TOTAL_LINE + TOTAL_TAX) * (rate / 100d);

						this.addArDrEntry(arInvoice.getArDrNextLine(), "DISCOUNT",
								EJBCommon.TRUE, DISCOUNT_AMT,
								adPaymentTerm.getGlChartOfAccount().getCoaCode(),
								arInvoice, AD_CMPNY);

					}

					// add receivable distribution

					this.addArDrIliEntry(arInvoice.getArDrNextLine(), "RECEIVABLE",
							EJBCommon.TRUE, TOTAL_LINE + TOTAL_TAX - W_TAX_AMOUNT - DISCOUNT_AMT,
							arInvoice.getArCustomer().getCstGlCoaReceivableAccount(),
							arInvoice, AD_CMPNY);

					// compute invoice amount due

					double amountDue = TOTAL_LINE + TOTAL_TAX - W_TAX_AMOUNT - DISCOUNT_AMT;


					//validate if total amount + unposted invoices' amount + current balance + unposted receipts's amount
					//does not exceed customer's credit limit

					if(arCustomer.getCstCreditLimit() > 0) {

						double balance = computeTotalBalance(details.getInvCode(), CST_CSTMR_CODE, AD_CMPNY);

						balance += amountDue;

						if(arCustomer.getCstCreditLimit() < balance) {

							throw new ArINVAmountExceedsCreditLimitException();

						}

					}

					//set invoice amount due

					arInvoice.setInvAmountDue(TOTAL_LINE + TOTAL_TAX - W_TAX_AMOUNT - DISCOUNT_AMT);

					// remove all payment schedule

					Collection arInvoicePaymentSchedules = arInvoice.getArInvoicePaymentSchedules();

					i = arInvoicePaymentSchedules.iterator();

					while (i.hasNext()) {

						LocalArInvoicePaymentSchedule arInvoicePaymentSchedule = (LocalArInvoicePaymentSchedule)i.next();

						i.remove();

						arInvoicePaymentSchedule.remove();

					}

					// create invoice payment schedule

					short precisionUnit = this.getGlFcPrecisionUnit(AD_CMPNY);
					double TOTAL_PAYMENT_SCHEDULE =  0d;

					Collection adPaymentSchedules = adPaymentTerm.getAdPaymentSchedules();

					i = adPaymentSchedules.iterator();

					while (i.hasNext()) {

						LocalAdPaymentSchedule adPaymentSchedule = (LocalAdPaymentSchedule)i.next();

						// get date due

						GregorianCalendar gcDateDue = new GregorianCalendar();
						gcDateDue.setTime(arInvoice.getInvDate());
						gcDateDue.add(Calendar.DATE, adPaymentSchedule.getPsDueDay());

						// create a payment schedule

						double PAYMENT_SCHEDULE_AMOUNT = 0;

						// if last payment schedule subtract to avoid rounding difference error

						if (i.hasNext()) {

							PAYMENT_SCHEDULE_AMOUNT = EJBCommon.roundIt((adPaymentSchedule.getPsRelativeAmount() / adPaymentTerm.getPytBaseAmount()) * arInvoice.getInvAmountDue(), precisionUnit);

						} else {

							PAYMENT_SCHEDULE_AMOUNT = arInvoice.getInvAmountDue() - TOTAL_PAYMENT_SCHEDULE;

						}

						LocalArInvoicePaymentSchedule arInvoicePaymentSchedule =
							arInvoicePaymentScheduleHome.create(gcDateDue.getTime(),
									adPaymentSchedule.getPsLineNumber(),
									PAYMENT_SCHEDULE_AMOUNT,
									0d, EJBCommon.FALSE,
									(short)0, gcDateDue.getTime(), 0d, 0d,
									AD_CMPNY);

						arInvoice.addArInvoicePaymentSchedule(arInvoicePaymentSchedule);

						TOTAL_PAYMENT_SCHEDULE += PAYMENT_SCHEDULE_AMOUNT;

					}

				}

				// set invoice approval status

				arInvoice.setInvApprovalStatus(null);

			}

		} catch (GlobalDocumentNumberNotUniqueException ex) {

			getSessionContext().setRollbackOnly();
			throw ex;

		} catch (GlobalPaymentTermInvalidException ex) {

			getSessionContext().setRollbackOnly();
			throw ex;

		} catch (ArINVAmountExceedsCreditLimitException ex) {

			getSessionContext().setRollbackOnly();
			throw ex;

		} catch (GlobalInvItemLocationNotFoundException ex) {

			getSessionContext().setRollbackOnly();
			throw ex;

		} catch (GlobalNoRecordFoundException ex) {

			getSessionContext().setRollbackOnly();
			throw ex;

		} catch (GlobalJournalNotBalanceException ex) {

			getSessionContext().setRollbackOnly();
			throw ex;

		} catch (Exception ex) {

			Debug.printStackTrace(ex);
			getSessionContext().setRollbackOnly();
			throw new EJBException(ex.getMessage());

		}

	}

	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public short getGlFcPrecisionUnit(Integer AD_CMPNY) {

		Debug.print("ArInvoiceImportControllerBean getGlFcPrecisionUnit");


		LocalAdCompanyHome adCompanyHome = null;


		// Initialize EJB Home

		try {

			adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}

		try {

			LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);

			return  adCompany.getGlFunctionalCurrency().getFcPrecision();

		} catch (Exception ex) {

			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());

		}

	}

	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public short getInvGpQuantityPrecisionUnit(Integer AD_CMPNY) {

		Debug.print("ArInvoiceImportControllerBean getInvGpQuantityPrecisionUnit");

		LocalAdPreferenceHome adPreferenceHome = null;

		// Initialize EJB Home

		try {

			adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}


		try {

			LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);

			return adPreference.getPrfInvQuantityPrecisionUnit();

		} catch (Exception ex) {

			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());

		}

	}

	private void addArDrEntry(short DR_LN, String DR_CLSS,
			byte DR_DBT, double DR_AMNT, Integer COA_CODE, LocalArInvoice arInvoice, Integer AD_CMPNY) {

		Debug.print("ArInvoiceImportControllerBean addArDrEntry");

		LocalArDistributionRecordHome arDistributionRecordHome = null;
		LocalGlChartOfAccountHome glChartOfAccountHome = null;
		LocalAdCompanyHome adCompanyHome = null;


		// Initialize EJB Home

		try {

			arDistributionRecordHome = (LocalArDistributionRecordHome)EJBHomeFactory.
			lookUpLocalHome(LocalArDistributionRecordHome.JNDI_NAME, LocalArDistributionRecordHome.class);
			glChartOfAccountHome = (LocalGlChartOfAccountHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlChartOfAccountHome.JNDI_NAME, LocalGlChartOfAccountHome.class);
			adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);


		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}

		try {

			// get company

			LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);

			LocalGlChartOfAccount glChartOfAccount = glChartOfAccountHome.findByPrimaryKey(COA_CODE);

			// create distribution record

			LocalArDistributionRecord arDistributionRecord = arDistributionRecordHome.create(
					DR_LN, DR_CLSS, DR_DBT, EJBCommon.roundIt(DR_AMNT, adCompany.getGlFunctionalCurrency().getFcPrecision()),
					EJBCommon.FALSE, EJBCommon.FALSE, AD_CMPNY);

			arInvoice.addArDistributionRecord(arDistributionRecord);
			glChartOfAccount.addArDistributionRecord(arDistributionRecord);

		} catch (Exception ex) {

			Debug.printStackTrace(ex);
			getSessionContext().setRollbackOnly();
			throw new EJBException(ex.getMessage());

		}

	}


	private LocalArInvoiceLineItem addArIliEntry(ArModInvoiceLineItemDetails mdetails, LocalArInvoice arInvoice, LocalInvItemLocation invItemLocation, Integer AD_CMPNY) {

		Debug.print("ArInvoiceImportControllerBean addArIliEntry");

		LocalArInvoiceLineItemHome arInvoiceLineItemHome = null;
		LocalInvItemLocationHome invItemLocationHome = null;
		LocalInvUnitOfMeasureHome invUnitOfMeasureHome = null;

		// Initialize EJB Home

		try {

			arInvoiceLineItemHome = (LocalArInvoiceLineItemHome)EJBHomeFactory.
			lookUpLocalHome(LocalArInvoiceLineItemHome.JNDI_NAME, LocalArInvoiceLineItemHome.class);
			invItemLocationHome = (LocalInvItemLocationHome)EJBHomeFactory.
			lookUpLocalHome(LocalInvItemLocationHome.JNDI_NAME, LocalInvItemLocationHome.class);
			invUnitOfMeasureHome = (LocalInvUnitOfMeasureHome)EJBHomeFactory.
			lookUpLocalHome(LocalInvUnitOfMeasureHome.JNDI_NAME, LocalInvUnitOfMeasureHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}

		try {

			short precisionUnit = this.getGlFcPrecisionUnit(AD_CMPNY);

			double ILI_AMNT = 0d;
			double ILI_TAX_AMNT = 0d;

			LocalArTaxCode arTaxCode = arInvoice.getArTaxCode();

			// calculate net amount
			ILI_AMNT = this.calculateIliNetAmount(mdetails, arTaxCode.getTcRate(), arTaxCode.getTcType(), precisionUnit);

			// calculate tax
			ILI_TAX_AMNT = this.calculateIliTaxAmount(mdetails, arTaxCode.getTcRate(), arTaxCode.getTcType(), ILI_AMNT, precisionUnit);


			LocalArInvoiceLineItem arInvoiceLineItem = arInvoiceLineItemHome.create(
					mdetails.getIliLine(), mdetails.getIliQuantity(), mdetails.getIliUnitPrice(),
					ILI_AMNT, ILI_TAX_AMNT, mdetails.getIliEnableAutoBuild(), mdetails.getIliDiscount1(),
					mdetails.getIliDiscount2(),mdetails.getIliDiscount3(), mdetails.getIliDiscount4(),
					mdetails.getIliTotalDiscount(), mdetails.getIliTax(), AD_CMPNY);

			arInvoice.addArInvoiceLineItem(arInvoiceLineItem);

			invItemLocation.addArInvoiceLineItem(arInvoiceLineItem);

			LocalInvUnitOfMeasure invUnitOfMeasure = invUnitOfMeasureHome.findByUomName(mdetails.getIliUomName(), AD_CMPNY);
			invUnitOfMeasure.addArInvoiceLineItem(arInvoiceLineItem);

			return arInvoiceLineItem;

		} catch (Exception ex) {

			Debug.printStackTrace(ex);
			getSessionContext().setRollbackOnly();
			throw new EJBException(ex.getMessage());

		}

	}

	private void addArDrIliEntry(short DR_LN, String DR_CLSS, byte DR_DBT, double DR_AMNT, Integer COA_CODE, LocalArInvoice arInvoice, Integer AD_CMPNY) {

		Debug.print("ArInvoiceImportControllerBean addArDrIliEntry");

		LocalArDistributionRecordHome arDistributionRecordHome = null;
		LocalGlChartOfAccountHome glChartOfAccountHome = null;
		LocalAdCompanyHome adCompanyHome = null;


		// Initialize EJB Home

		try {

			arDistributionRecordHome = (LocalArDistributionRecordHome)EJBHomeFactory.
			lookUpLocalHome(LocalArDistributionRecordHome.JNDI_NAME, LocalArDistributionRecordHome.class);
			glChartOfAccountHome = (LocalGlChartOfAccountHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlChartOfAccountHome.JNDI_NAME, LocalGlChartOfAccountHome.class);
			adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}

		try {

			// get company

			LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);

			LocalGlChartOfAccount glChartOfAccount = glChartOfAccountHome.findByPrimaryKey(COA_CODE);

			// create distribution record

			LocalArDistributionRecord arDistributionRecord = arDistributionRecordHome.create(
					DR_LN, DR_CLSS, DR_DBT, EJBCommon.roundIt(DR_AMNT, adCompany.getGlFunctionalCurrency().getFcPrecision()),
					EJBCommon.FALSE, EJBCommon.FALSE, AD_CMPNY);

			arInvoice.addArDistributionRecord(arDistributionRecord);
			glChartOfAccount.addArDistributionRecord(arDistributionRecord);

		} catch (Exception ex) {

			Debug.printStackTrace(ex);
			getSessionContext().setRollbackOnly();
			throw new EJBException(ex.getMessage());

		}

	}


	private double calculateIliNetAmount(ArModInvoiceLineItemDetails mdetails, double tcRate, String tcType, short precisionUnit) {

		double amount = 0d;

		if (tcType.equals("INCLUSIVE") && mdetails.getIliTax()==EJBCommon.TRUE) {

			amount = EJBCommon.roundIt(mdetails.getIliAmount() / (1 + (tcRate / 100)), precisionUnit);

		} else {

			// tax exclusive, none, zero rated or exempt

			amount = mdetails.getIliAmount();

		}

		return amount;

	}

	private double calculateIliTaxAmount(ArModInvoiceLineItemDetails mdetails, double tcRate, String tcType, double amount, short precisionUnit) {

		double taxAmount = 0d;

		if (!tcType.equals("NONE") &&
				!tcType.equals("EXEMPT")) {

			if(mdetails.getIliTax()==EJBCommon.TRUE) {
				if (tcType.equals("INCLUSIVE")) {

					taxAmount = EJBCommon.roundIt(mdetails.getIliAmount() - amount, precisionUnit);

				} else if (tcType.equals("EXCLUSIVE")) {

					taxAmount = EJBCommon.roundIt(mdetails.getIliAmount() * tcRate / 100, precisionUnit);

				} else {

					// tax none zero-rated or exempt

				}
			}


		}

		return taxAmount;

	}

	private double convertByUomFromAndItemAndQuantity(LocalInvUnitOfMeasure invFromUnitOfMeasure, LocalInvItem invItem, double QTY_SLD, Integer AD_CMPNY) {

		Debug.print("ArInvoiceImportControllerBean convertByUomFromAndItemAndQuantity");

		LocalAdPreferenceHome adPreferenceHome = null;
		LocalInvUnitOfMeasureConversionHome invUnitOfMeasureConversionHome = null;

		// Initialize EJB Home

		try {

			adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);
			invUnitOfMeasureConversionHome = (LocalInvUnitOfMeasureConversionHome)EJBHomeFactory.
			lookUpLocalHome(LocalInvUnitOfMeasureConversionHome.JNDI_NAME, LocalInvUnitOfMeasureConversionHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}

		try {

			LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);

			LocalInvUnitOfMeasureConversion invUnitOfMeasureConversion = invUnitOfMeasureConversionHome.findUmcByIiNameAndUomName(invItem.getIiName(), invFromUnitOfMeasure.getUomName(), AD_CMPNY);
            LocalInvUnitOfMeasureConversion invDefaultUomConversion = invUnitOfMeasureConversionHome.findUmcByIiNameAndUomName(invItem.getIiName(), invItem.getInvUnitOfMeasure().getUomName(), AD_CMPNY);

        	return EJBCommon.roundIt(QTY_SLD * invDefaultUomConversion.getUmcConversionFactor() / invUnitOfMeasureConversion.getUmcConversionFactor(), adPreference.getPrfInvQuantityPrecisionUnit());

		} catch (Exception ex) {

			Debug.printStackTrace(ex);
			getSessionContext().setRollbackOnly();
			throw new EJBException(ex.getMessage());

		}

	}

	private double computeTotalBalance(Integer invoiceCode, String CST_CSTMR_CODE, Integer AD_CMPNY) {

		LocalArCustomerBalanceHome arCustomerBalanceHome = null;
		LocalArInvoiceHome arInvoiceHome = null;
		LocalArReceiptHome arReceiptHome = null;

		try {

			arInvoiceHome = (LocalArInvoiceHome)EJBHomeFactory.
			lookUpLocalHome(LocalArInvoiceHome.JNDI_NAME, LocalArInvoiceHome.class);
			arCustomerBalanceHome = (LocalArCustomerBalanceHome)EJBHomeFactory.
			lookUpLocalHome(LocalArCustomerBalanceHome.JNDI_NAME, LocalArCustomerBalanceHome.class);
			arReceiptHome = (LocalArReceiptHome)EJBHomeFactory.
			lookUpLocalHome(LocalArReceiptHome.JNDI_NAME, LocalArReceiptHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}

		double customerBalance = 0;

		try {

			//get latest balance

			Collection arCustomerBalances = arCustomerBalanceHome.findByCstCustomerCode(CST_CSTMR_CODE, AD_CMPNY);

			if(!arCustomerBalances.isEmpty()) {

				ArrayList customerBalanceList = new ArrayList(arCustomerBalances);

				customerBalance = ((LocalArCustomerBalance) customerBalanceList.get(customerBalanceList.size() - 1)).getCbBalance();

			}

			//get amount of unposted invoices/credit memos

			Collection arInvoices = arInvoiceHome.findUnpostedInvByCstCustomerCode(CST_CSTMR_CODE, AD_CMPNY);

			Iterator arInvIter = arInvoices.iterator();

			while(arInvIter.hasNext()) {

				LocalArInvoice mdetails = (LocalArInvoice)arInvIter.next();

				if(!mdetails.getInvCode().equals(invoiceCode)) {

					if(mdetails.getInvCreditMemo() == EJBCommon.TRUE) {

						customerBalance = customerBalance - mdetails.getInvAmountDue();

					} else {

						customerBalance = customerBalance + (mdetails.getInvAmountDue() - mdetails.getInvAmountPaid());

					}

				}

			}

			//get amount of unposted receipts

			Collection arReceipts = arReceiptHome.findUnpostedRctByCstCustomerCode(CST_CSTMR_CODE, AD_CMPNY);

			Iterator arRctIter = arReceipts.iterator();

			while(arRctIter.hasNext()) {

				LocalArReceipt arReceipt = (LocalArReceipt)arRctIter.next();

				customerBalance = customerBalance - arReceipt.getRctAmount();

			}

		} catch (FinderException ex) {

		}

		return customerBalance;

	}

	private double convertCostByUom(String II_NM, String UOM_NM, double unitCost, boolean isFromDefault, Integer AD_CMPNY) {

        Debug.print("ArInvoiceEntryControllerBean convertCostByUom");

        LocalInvItemHome invItemHome = null;
        LocalInvUnitOfMeasureConversionHome invUnitOfMeasureConversionHome = null;

        // Initialize EJB Home

        try {

            invUnitOfMeasureConversionHome = (LocalInvUnitOfMeasureConversionHome)EJBHomeFactory.
            lookUpLocalHome(LocalInvUnitOfMeasureConversionHome.JNDI_NAME, LocalInvUnitOfMeasureConversionHome.class);
            invItemHome = (LocalInvItemHome)EJBHomeFactory.
            lookUpLocalHome(LocalInvItemHome.JNDI_NAME, LocalInvItemHome.class);

        } catch (NamingException ex) {

            throw new EJBException(ex.getMessage());

        }

        try {

            LocalInvItem invItem = invItemHome.findByIiName(II_NM, AD_CMPNY);
            LocalInvUnitOfMeasureConversion invUnitOfMeasureConversion = invUnitOfMeasureConversionHome.findUmcByIiNameAndUomName(II_NM, UOM_NM, AD_CMPNY);
            LocalInvUnitOfMeasureConversion invDefaultUomConversion = invUnitOfMeasureConversionHome.findUmcByIiNameAndUomName(II_NM, invItem.getInvUnitOfMeasure().getUomName(), AD_CMPNY);

            if (isFromDefault) {

                return unitCost * invDefaultUomConversion.getUmcConversionFactor() / invUnitOfMeasureConversion.getUmcConversionFactor();

            } else {

                return unitCost * invUnitOfMeasureConversion.getUmcConversionFactor() / invDefaultUomConversion.getUmcConversionFactor();

            }

        } catch (Exception ex) {

            Debug.printStackTrace(ex);
            getSessionContext().setRollbackOnly();
            throw new EJBException(ex.getMessage());

        }

    }

	/**
	 * @ejb:create-method view-type="remote"
	 **/
	public void ejbCreate() throws CreateException {

		Debug.print("ArInvoiceImportControllerBean ejbCreate");

	}

}