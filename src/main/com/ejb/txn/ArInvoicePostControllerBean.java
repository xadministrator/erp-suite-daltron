
/*
 * ArInvoicePostControllerBean.java
 *
 * Created on March 10, 2004, 9:30 AM
 *
 * @author  Dennis M. Hilario
 */

package com.ejb.txn;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;
import javax.naming.NamingException;

import com.ejb.ad.LocalAdBranchDocumentSequenceAssignment;
import com.ejb.ad.LocalAdBranchDocumentSequenceAssignmentHome;
import com.ejb.ad.LocalAdBranchItemLocation;
import com.ejb.ad.LocalAdBranchItemLocationHome;
import com.ejb.ad.LocalAdCompany;
import com.ejb.ad.LocalAdCompanyHome;
import com.ejb.ad.LocalAdPreference;
import com.ejb.ad.LocalAdPreferenceHome;
import com.ejb.ar.LocalArCustomer;
import com.ejb.ar.LocalArCustomerBalance;
import com.ejb.ar.LocalArCustomerBalanceHome;
import com.ejb.ar.LocalArCustomerHome;
import com.ejb.ar.LocalArDistributionRecord;
import com.ejb.ar.LocalArDistributionRecordHome;
import com.ejb.ar.LocalArInvoice;
import com.ejb.ar.LocalArInvoiceBatch;
import com.ejb.ar.LocalArInvoiceBatchHome;
import com.ejb.ar.LocalArInvoiceHome;
import com.ejb.ar.LocalArInvoiceLineItem;
import com.ejb.ar.LocalArInvoicePaymentSchedule;
import com.ejb.ar.LocalArSalesOrder;
import com.ejb.ar.LocalArSalesOrderInvoiceLine;
import com.ejb.ar.LocalArSalesOrderLine;
import com.ejb.exception.AdPRFCoaGlVarianceAccountNotFoundException;
import com.ejb.exception.GlJREffectiveDateNoPeriodExistException;
import com.ejb.exception.GlJREffectiveDatePeriodClosedException;
import com.ejb.exception.GlobalBranchAccountNumberInvalidException;
import com.ejb.exception.GlobalConversionDateNotExistException;
import com.ejb.exception.GlobalExpiryDateNotFoundException;
import com.ejb.exception.GlobalInvItemLocationNotFoundException;
import com.ejb.exception.GlobalInventoryDateException;
import com.ejb.exception.GlobalJournalNotBalanceException;
import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.exception.GlobalRecordAlreadyDeletedException;
import com.ejb.exception.GlobalRecordInvalidException;
import com.ejb.exception.GlobalTransactionAlreadyPostedException;
import com.ejb.exception.GlobalTransactionAlreadyVoidException;
import com.ejb.gl.LocalAdDocumentSequenceAssignment;
import com.ejb.gl.LocalAdDocumentSequenceAssignmentHome;
import com.ejb.gl.LocalGlAccountingCalendarValue;
import com.ejb.gl.LocalGlAccountingCalendarValueHome;
import com.ejb.gl.LocalGlChartOfAccount;
import com.ejb.gl.LocalGlChartOfAccountBalance;
import com.ejb.gl.LocalGlChartOfAccountBalanceHome;
import com.ejb.gl.LocalGlChartOfAccountHome;
import com.ejb.gl.LocalGlForexLedger;
import com.ejb.gl.LocalGlForexLedgerHome;
import com.ejb.gl.LocalGlFunctionalCurrency;
import com.ejb.gl.LocalGlFunctionalCurrencyHome;
import com.ejb.gl.LocalGlFunctionalCurrencyRate;
import com.ejb.gl.LocalGlFunctionalCurrencyRateHome;
import com.ejb.gl.LocalGlJournal;
import com.ejb.gl.LocalGlJournalBatch;
import com.ejb.gl.LocalGlJournalBatchHome;
import com.ejb.gl.LocalGlJournalCategory;
import com.ejb.gl.LocalGlJournalCategoryHome;
import com.ejb.gl.LocalGlJournalHome;
import com.ejb.gl.LocalGlJournalLine;
import com.ejb.gl.LocalGlJournalLineHome;
import com.ejb.gl.LocalGlJournalSource;
import com.ejb.gl.LocalGlJournalSourceHome;
import com.ejb.gl.LocalGlSetOfBook;
import com.ejb.gl.LocalGlSetOfBookHome;
import com.ejb.gl.LocalGlSuspenseAccount;
import com.ejb.gl.LocalGlSuspenseAccountHome;
import com.ejb.inv.LocalInvAdjustment;
import com.ejb.inv.LocalInvAdjustmentHome;
import com.ejb.inv.LocalInvAdjustmentLine;
import com.ejb.inv.LocalInvAdjustmentLineHome;
import com.ejb.inv.LocalInvBillOfMaterial;
import com.ejb.inv.LocalInvCosting;
import com.ejb.inv.LocalInvCostingHome;
import com.ejb.inv.LocalInvDistributionRecord;
import com.ejb.inv.LocalInvDistributionRecordHome;
import com.ejb.inv.LocalInvItem;
import com.ejb.inv.LocalInvItemHome;
import com.ejb.inv.LocalInvItemLocation;
import com.ejb.inv.LocalInvItemLocationHome;
import com.ejb.inv.LocalInvUnitOfMeasure;
import com.ejb.inv.LocalInvUnitOfMeasureConversion;
import com.ejb.inv.LocalInvUnitOfMeasureConversionHome;
import com.util.AbstractSessionBean;
import com.util.ArModInvoiceDetails;
import com.util.Debug;
import com.util.EJBCommon;
import com.util.EJBHomeFactory;

/**
 * @ejb:bean name="ArInvoicePostControllerEJB"
 *           display-name="Used for posting invoices"
 *           type="Stateless"
 *           view-type="remote"
 *           jndi-name="ejb/ArInvoicePostControllerEJB"
 *
 * @ejb:interface remote-class="com.ejb.txn.ArInvoicePostController"
 *                extends="javax.ejb.EJBObject"
 *
 * @ejb:home remote-class="com.ejb.txn.ArInvoicePostControllerHome"
 *           extends="javax.ejb.EJBHome"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="aruser"
 *                        role-link="aruserlink"
 *
 * @ejb:permission role-name="aruser"
 * 
*/

public class ArInvoicePostControllerBean extends AbstractSessionBean {

    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList getArCstAll(Integer AD_BRNCH, Integer AD_CMPNY) {
                    
        Debug.print("ArInvoicePostControllerBean getArCstAll");
        
        LocalArCustomerHome arCustomerHome = null;               
        
        ArrayList list = new ArrayList();
        
        // Initialize EJB Home
        
        try {
            
            arCustomerHome = (LocalArCustomerHome)EJBHomeFactory.
                lookUpLocalHome(LocalArCustomerHome.JNDI_NAME, LocalArCustomerHome.class);
            
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }        
        
        try {
        	
        	Collection arCustomers = arCustomerHome.findEnabledCstAll(AD_BRNCH, AD_CMPNY);
        	
        	Iterator i = arCustomers.iterator();
        	
        	while (i.hasNext()) {
        		
        		LocalArCustomer arCustomer = (LocalArCustomer)i.next();
        		
        		list.add(arCustomer.getCstCustomerCode());
        		
        	}
        	
        	return list;
        	
        } catch (Exception ex) {
        	
        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());
        	
        }
            
    }   
    
    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList getGlFcAll(Integer AD_CMPNY) {
                    
        Debug.print("ArInvoicePostControllerBean getGlFcAll");
        
        LocalGlFunctionalCurrencyHome glFunctionalCurrencyHome = null;

        ArrayList list = new ArrayList();
        
        // Initialize EJB Home
        
        try {
            
            glFunctionalCurrencyHome = (LocalGlFunctionalCurrencyHome)EJBHomeFactory.
                lookUpLocalHome(LocalGlFunctionalCurrencyHome.JNDI_NAME, LocalGlFunctionalCurrencyHome.class);
            
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {
            
            Collection glFunctionalCurrencies = glFunctionalCurrencyHome.findFcAll(AD_CMPNY);
	        
	        Iterator i = glFunctionalCurrencies.iterator();
	               
	        while (i.hasNext()) {
	        	
	        	LocalGlFunctionalCurrency glFunctionalCurrency = (LocalGlFunctionalCurrency)i.next();
	        	
	        	list.add(glFunctionalCurrency.getFcName());
	        	
	        }
	        
	        return list;
          
        } catch (Exception ex) {
        	
        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());
        	
        }
            
    }
    
    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList getArOpenIbAll(Integer AD_BRNCH, Integer AD_CMPNY) {
                    
        Debug.print("ArInvoicePostControllerBean getArOpenIbAll");
        
        LocalArInvoiceBatchHome arInvoiceBatchHome = null;               
        
        ArrayList list = new ArrayList();
        
        // Initialize EJB Home
        
        try {
            
        	arInvoiceBatchHome = (LocalArInvoiceBatchHome)EJBHomeFactory.
                lookUpLocalHome(LocalArInvoiceBatchHome.JNDI_NAME, LocalArInvoiceBatchHome.class);
            
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }        
        
        try {
        	
        	Collection arInvoiceBatches = arInvoiceBatchHome.findOpenIbAll(AD_BRNCH, AD_CMPNY);
        	
        	Iterator i = arInvoiceBatches.iterator();
        	
        	while (i.hasNext()) {
        		
        		LocalArInvoiceBatch arInvoiceBatch = (LocalArInvoiceBatch)i.next();
        		
        		list.add(arInvoiceBatch.getIbName());
        		
        	}
        	
        	return list;
        	
        } catch (Exception ex) {
        	
        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());
        	
        }
            
    }  
    
    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
     public byte getAdPrfEnableArInvoiceBatch(Integer AD_CMPNY) {

        Debug.print("ArInvoicePostControllerBean getAdPrfEnableArInvoiceBatch");
                   
        LocalAdPreferenceHome adPreferenceHome = null;
       
       
        // Initialize EJB Home
         
        try {
             
           adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
               lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);       
             
        } catch (NamingException ex) {
             
           throw new EJBException(ex.getMessage());
             
        }
       

        try {
       	
           LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);
          
           return adPreference.getPrfEnableArInvoiceBatch();
          
        } catch (Exception ex) {
         	 
           Debug.printStackTrace(ex);
           throw new EJBException(ex.getMessage());
          
        }
       
     } 
    
    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList getArInvPostableByCriteria(HashMap criteria,
        Integer OFFSET, Integer LIMIT, String ORDER_BY, Integer AD_BRNCH, Integer AD_CMPNY) 
        throws GlobalNoRecordFoundException {
                    
        Debug.print("ArInvoicePostControllerBean getArInvPostableByCriteria");
        
        LocalArInvoiceHome arInvoiceHome = null;
        
        ArrayList list = new ArrayList();
        
        //initialized EJB Home
        
        try {
            
            arInvoiceHome = (LocalArInvoiceHome)EJBHomeFactory.
                lookUpLocalHome(LocalArInvoiceHome.JNDI_NAME, LocalArInvoiceHome.class);
            
        } catch (NamingException ex) 	{
            
            throw new EJBException(ex.getMessage());
            
        }
        
		try { 
           
		  StringBuffer jbossQl = new StringBuffer();
		  jbossQl.append("SELECT OBJECT(inv) FROM ArInvoice inv ");
		  
	      boolean firstArgument = true;
	      short ctr = 0;
		  int criteriaSize = criteria.size() + 2;	      
	      
	      Object obj[];	      
		
		  // Allocate the size of the object parameter
		
	      if (criteria.containsKey("referenceNumber")) {
	      	
	      	 criteriaSize--;
	      	 
	      }	      
	      	      
	      obj = new Object[criteriaSize];
		       	      
		  if (criteria.containsKey("referenceNumber")) {
		  	
		  	 if (!firstArgument) {
		  	 
		  	    jbossQl.append("AND ");	
		  	 	
		     } else {
		     	
		     	firstArgument = false;
		     	jbossQl.append("WHERE ");
		     	
		     }
		     
		  	 jbossQl.append("inv.invReferenceNumber LIKE '%" + (String)criteria.get("referenceNumber") + "%' ");
		  	 
		  }
		  
		  if (criteria.containsKey("batchName")) {
		   	
		   	  if (!firstArgument) {		       	  	
		   	     jbossQl.append("AND ");		       	     
		   	  } else {		       	  	
		   	  	 firstArgument = false;
		   	  	 jbossQl.append("WHERE ");		       	  	 
		   	  }
		   	  
		   	  jbossQl.append("inv.arInvoiceBatch.ibName=?" + (ctr+1) + " ");
		   	  obj[ctr] = (String)criteria.get("batchName");
		   	  ctr++;
	   	  
	      }
			
		  if (criteria.containsKey("customerCode")) {
	   	
		   	  if (!firstArgument) {		       	  	
		   	     jbossQl.append("AND ");		       	     
		   	  } else {		       	  	
		   	  	 firstArgument = false;
		   	  	 jbossQl.append("WHERE ");		       	  	 
		   	  }
		   	  
		   	  jbossQl.append("inv.arCustomer.cstCustomerCode=?" + (ctr+1) + " ");
		   	  obj[ctr] = (String)criteria.get("customerCode");
		   	  ctr++;
	   	  
	      }	

	      	
	      if (!firstArgument) {
	  	 	jbossQl.append("AND ");
	  	  } else {
	  	 	firstArgument = false;
	  	 	jbossQl.append("WHERE ");
	  	  }
	  	 
	  	  jbossQl.append("inv.invCreditMemo=?" + (ctr+1) + " ");
	  	  obj[ctr] = (Byte)criteria.get("creditMemo");
	      ctr++;		     
          			
	      if (criteria.containsKey("dateFrom")) {
		      	
		  	 if (!firstArgument) {
		  	 	jbossQl.append("AND ");
		  	 } else {
		  	 	firstArgument = false;
		  	 	jbossQl.append("WHERE ");
		  	 }
		  	 jbossQl.append("inv.invDate>=?" + (ctr+1) + " ");
		  	 obj[ctr] = (Date)criteria.get("dateFrom");
		  	 ctr++;
		  }  
		      
		  if (criteria.containsKey("dateTo")) {
		  	
		  	 if (!firstArgument) {
		  	 	jbossQl.append("AND ");
		  	 } else {
		  	 	firstArgument = false;
		  	 	jbossQl.append("WHERE ");
		  	 }
		  	 jbossQl.append("inv.invDate<=?" + (ctr+1) + " ");
		  	 obj[ctr] = (Date)criteria.get("dateTo");
		  	 ctr++;
		  	 
		  }    
		  
		  if (criteria.containsKey("invoiceNumberFrom")) {
		      	
		  	 if (!firstArgument) {
		  	 	jbossQl.append("AND ");
		  	 } else {
		  	 	firstArgument = false;
		  	 	jbossQl.append("WHERE ");
		  	 }
		  	 jbossQl.append("inv.invNumber>=?" + (ctr+1) + " ");
		  	 obj[ctr] = (String)criteria.get("invoiceNumberFrom");
		  	 ctr++;
		  }  
		      
		  if (criteria.containsKey("invoiceNumberTo")) {
		  	
		  	 if (!firstArgument) {
		  	 	jbossQl.append("AND ");
		  	 } else {
		  	 	firstArgument = false;
		  	 	jbossQl.append("WHERE ");
		  	 }
		  	 jbossQl.append("inv.invNumber<=?" + (ctr+1) + " ");
		  	 obj[ctr] = (String)criteria.get("invoiceNumberTo");
		  	 ctr++;
		  	 
		  }

	      if (criteria.containsKey("currency")) {
	       	
	       	  if (!firstArgument) {
	       	  	
	       	     jbossQl.append("AND ");
	       	     
	       	  } else {
	       	  	
	       	  	 firstArgument = false;
	       	  	 jbossQl.append("WHERE ");
	       	  	 
	       	  }
	       	  
	       	  jbossQl.append("inv.glFunctionalCurrency.fcName=?" + (ctr+1) + " ");
	       	  obj[ctr] = (String)criteria.get("currency");
	       	  ctr++;
	       	  
	      }
	      
	      if (criteria.containsKey("approvalStatus")) {
	       	
	       	  if (!firstArgument) {
	       	  	
	       	     jbossQl.append("AND ");
	       	     
	       	  } else {
	       	  	
	       	  	 firstArgument = false;
	       	  	 jbossQl.append("WHERE ");
	       	  	 
	       	  }
	       	  
	       	  jbossQl.append("inv.invApprovalStatus=?" + (ctr+1) + " ");
	       	  obj[ctr] = (String)criteria.get("approvalStatus");
	       	  ctr++;
	       	  
	      } else {
	      	
	      	  if (!firstArgument) {
	       	  	
	       	     jbossQl.append("AND ");
	       	     
	       	  } else {
	       	  	
	       	  	 firstArgument = false;
	       	  	 jbossQl.append("WHERE ");
	       	  	 
	       	  }
	       	  
	       	  jbossQl.append("(inv.invApprovalStatus='APPROVED' OR inv.invApprovalStatus='N/A') ");
	      	
	      }
	      
	      
	      if (!firstArgument) {
	       	  	
	   	     jbossQl.append("AND ");
	   	     
	   	  } else {
	   	  	
	   	  	 firstArgument = false;
	   	  	 jbossQl.append("WHERE ");
	   	  	 
	   	  }
	   	  
	   	  jbossQl.append("inv.invPosted = 0 AND inv.invVoid = 0 AND inv.invAdBranch=" + AD_BRNCH + " AND inv.invAdCompany=" + AD_CMPNY + " ");
	   	  
		  String orderBy = null;
		  
		  if (ORDER_BY.equals("CUSTOMER CODE")) {	          
		      	      		
		  	  orderBy = "inv.arCustomer.cstCustomerCode";

		  } else if (ORDER_BY.equals("INVOICE NUMBER")) {
		
		  	  orderBy = "inv.invNumber";
		  	
		  }

	  	  if (orderBy != null) {
		
		  	  jbossQl.append("ORDER BY " + orderBy + ", inv.invDate");

		  } else {
		  	
		  	  jbossQl.append("ORDER BY inv.invDate");
		  	
		  }
		  
		  jbossQl.append(" OFFSET ?" + (ctr + 1));	        
		  obj[ctr] = OFFSET;	      
		  ctr++;
		  
		  jbossQl.append(" LIMIT ?" + (ctr + 1));
		  obj[ctr] = LIMIT;
		  ctr++;
		  
		  System.out.println("jbossQl.toString()="+jbossQl.toString());
			  	      	
	      Collection arInvoices = arInvoiceHome.getInvByCriteria(jbossQl.toString(), obj);	         
		  
		  if (arInvoices.size() == 0)
		     throw new GlobalNoRecordFoundException();
		     
		  Iterator i = arInvoices.iterator();
		
		  while (i.hasNext()) {
		  	
		  	LocalArInvoice arInvoice = (LocalArInvoice)i.next();   	  
		  	
		  	ArModInvoiceDetails mdetails = new ArModInvoiceDetails();
		  	mdetails.setInvCode(arInvoice.getInvCode());
		  	mdetails.setInvCstCustomerCode(arInvoice.getArCustomer().getCstCustomerCode());
		  	mdetails.setInvDate(arInvoice.getInvDate());
		  	mdetails.setInvNumber(arInvoice.getInvNumber());
		  	mdetails.setInvReferenceNumber(arInvoice.getInvCreditMemo() == EJBCommon.FALSE ? arInvoice.getInvReferenceNumber() : arInvoice.getInvCmInvoiceNumber());
		  	mdetails.setInvAmountDue(arInvoice.getInvAmountDue());		  	  

		  	if(!arInvoice.getArInvoiceLineItems().isEmpty()) {
		  		
		  		mdetails.setInvType("ITEMS");
		  	
		  	} else {
    			
    			mdetails.setInvType("MEMO LINES");
    		
    		}
		  	
		  	
		  	list.add(mdetails);
		  	
		  }
			     
		  return list;
		      
	  } catch (GlobalNoRecordFoundException ex) {
	  	 
	  	  throw ex;
	  	
	  } catch (Exception ex) {
	  	

	  	  ex.printStackTrace();
	  	  throw new EJBException(ex.getMessage());
	  	
	  }
        
    }
    
    /**
     * @ejb:interface-method view-type="remote"
     **/
    public void executeArInvPost(Integer INV_CODE, String USR_NM, Integer AD_BRNCH, Integer AD_CMPNY) throws
        GlobalRecordAlreadyDeletedException,		
		GlobalTransactionAlreadyPostedException,
		GlobalTransactionAlreadyVoidException,
		GlJREffectiveDateNoPeriodExistException,
		GlJREffectiveDatePeriodClosedException,
		GlobalJournalNotBalanceException,
		GlobalInventoryDateException,
		GlobalBranchAccountNumberInvalidException,
		AdPRFCoaGlVarianceAccountNotFoundException, 
		GlobalExpiryDateNotFoundException {
                    
        Debug.print("ArInvoicePostControllerBean executeApInvPost");
        
        LocalArInvoiceHome arInvoiceHome = null;        
        LocalAdCompanyHome adCompanyHome = null;
        LocalAdPreferenceHome adPreferenceHome = null;
        LocalGlSetOfBookHome glSetOfBookHome = null;
        LocalGlAccountingCalendarValueHome glAccountingCalendarValueHome = null;
        LocalGlJournalHome glJournalHome = null;
        LocalGlJournalBatchHome glJournalBatchHome = null;
        LocalGlSuspenseAccountHome glSuspenseAccountHome = null;
        LocalGlJournalLineHome glJournalLineHome = null;
        LocalGlJournalSourceHome glJournalSourceHome = null;
        LocalGlJournalCategoryHome glJournalCategoryHome = null;
        LocalGlFunctionalCurrencyHome glFunctionalCurrencyHome = null;
        LocalArDistributionRecordHome arDistributionRecordHome = null;
        LocalInvCostingHome invCostingHome = null;
        LocalInvItemHome invItemHome = null;
        LocalGlChartOfAccountHome glChartOfAccountHome = null;
        LocalArCustomerBalanceHome arCustomerBalanceHome = null;
        LocalArCustomerHome arCustomerHome = null;
 		LocalGlForexLedgerHome glForexLedgerHome = null;
 		LocalGlFunctionalCurrencyRateHome glFunctionalCurrencyRateHome = null;
 		
        LocalArInvoice arInvoice = null;        
        LocalArInvoice arCreditedInvoice = null;
                
        // Initialize EJB Home
        
        Date txnStartDate = new Date();
        
        try {
            
            arInvoiceHome = (LocalArInvoiceHome)EJBHomeFactory.
                lookUpLocalHome(LocalArInvoiceHome.JNDI_NAME, LocalArInvoiceHome.class);
            adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
            adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
            	lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);
            glSetOfBookHome = (LocalGlSetOfBookHome)EJBHomeFactory.
            	lookUpLocalHome(LocalGlSetOfBookHome.JNDI_NAME, LocalGlSetOfBookHome.class);
            glAccountingCalendarValueHome = (LocalGlAccountingCalendarValueHome)EJBHomeFactory.
            	lookUpLocalHome(LocalGlAccountingCalendarValueHome.JNDI_NAME, LocalGlAccountingCalendarValueHome.class);
            glJournalHome = (LocalGlJournalHome)EJBHomeFactory.
            	lookUpLocalHome(LocalGlJournalHome.JNDI_NAME, LocalGlJournalHome.class);
            glJournalBatchHome = (LocalGlJournalBatchHome)EJBHomeFactory.
            	lookUpLocalHome(LocalGlJournalBatchHome.JNDI_NAME, LocalGlJournalBatchHome.class);
            glSuspenseAccountHome = (LocalGlSuspenseAccountHome)EJBHomeFactory.
            	lookUpLocalHome(LocalGlSuspenseAccountHome.JNDI_NAME, LocalGlSuspenseAccountHome.class);
            glJournalLineHome = (LocalGlJournalLineHome)EJBHomeFactory.
            	lookUpLocalHome(LocalGlJournalLineHome.JNDI_NAME, LocalGlJournalLineHome.class);
            glJournalSourceHome = (LocalGlJournalSourceHome)EJBHomeFactory.
            	lookUpLocalHome(LocalGlJournalSourceHome.JNDI_NAME, LocalGlJournalSourceHome.class);
            glJournalCategoryHome = (LocalGlJournalCategoryHome)EJBHomeFactory.
            	lookUpLocalHome(LocalGlJournalCategoryHome.JNDI_NAME, LocalGlJournalCategoryHome.class);
            glFunctionalCurrencyHome = (LocalGlFunctionalCurrencyHome)EJBHomeFactory.
            	lookUpLocalHome(LocalGlFunctionalCurrencyHome.JNDI_NAME, LocalGlFunctionalCurrencyHome.class);
            arDistributionRecordHome = (LocalArDistributionRecordHome)EJBHomeFactory.
            	lookUpLocalHome(LocalArDistributionRecordHome.JNDI_NAME, LocalArDistributionRecordHome.class);
            invCostingHome = (LocalInvCostingHome)EJBHomeFactory.
            	lookUpLocalHome(LocalInvCostingHome.JNDI_NAME, LocalInvCostingHome.class);
            invItemHome = (LocalInvItemHome)EJBHomeFactory.
            	lookUpLocalHome(LocalInvItemHome.JNDI_NAME, LocalInvItemHome.class);
            glChartOfAccountHome = (LocalGlChartOfAccountHome)EJBHomeFactory.
              	lookUpLocalHome(LocalGlChartOfAccountHome.JNDI_NAME, LocalGlChartOfAccountHome.class);
            arCustomerBalanceHome = (LocalArCustomerBalanceHome)EJBHomeFactory.
          		lookUpLocalHome(LocalArCustomerBalanceHome.JNDI_NAME, LocalArCustomerBalanceHome.class);
            arCustomerHome = (LocalArCustomerHome)EJBHomeFactory.
      			lookUpLocalHome(LocalArCustomerHome.JNDI_NAME, LocalArCustomerHome.class);
 			glForexLedgerHome  = (LocalGlForexLedgerHome)EJBHomeFactory.
				lookUpLocalHome(LocalGlForexLedgerHome.JNDI_NAME, LocalGlForexLedgerHome.class);
			glFunctionalCurrencyRateHome = (LocalGlFunctionalCurrencyRateHome)EJBHomeFactory.
				lookUpLocalHome(LocalGlFunctionalCurrencyRateHome.JNDI_NAME, LocalGlFunctionalCurrencyRateHome.class);
			
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }       
        
        try {
        	
        	LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
        	LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);
        	
        	// validate if invoice/credit memo is already deleted
        	
        	try {
        		
        		arInvoice = arInvoiceHome.findByPrimaryKey(INV_CODE);
        		
        	} catch (FinderException ex) {
        		
        		throw new GlobalRecordAlreadyDeletedException();
        		
        	}
        	
        	// validate if invoice/credit memo is already posted or void
        	
        	if (arInvoice.getInvPosted() == EJBCommon.TRUE) {
        		
        		throw new GlobalTransactionAlreadyPostedException();
        		
        	} else if (arInvoice.getInvVoid() == EJBCommon.TRUE) {
        		
        		throw new GlobalTransactionAlreadyVoidException();
        	}
        	
        	
        	// regenerate inventory dr
        	if (adPreference.getPrfArAutoComputeCogs() == EJBCommon.TRUE && arInvoice.getInvCreditMemo() == EJBCommon.FALSE) {
        		this.regenerateInventoryDr(arInvoice, AD_BRNCH, AD_CMPNY);
           	}
        	
        	
        	// post invoice/credit memo
        	
        	if (arInvoice.getInvCreditMemo() == EJBCommon.FALSE) {
        		
        		// increase customer balance
        		
        		double INV_AMNT = this.convertForeignToFunctionalCurrency(arInvoice.getGlFunctionalCurrency().getFcCode(),
        				arInvoice.getGlFunctionalCurrency().getFcName(),
						arInvoice.getInvConversionDate(), arInvoice.getInvConversionRate(),
						arInvoice.getInvAmountDue(), AD_CMPNY);
        		
        		this.post(arInvoice.getInvDate(), INV_AMNT, arInvoice.getArCustomer(), AD_CMPNY);
        		Collection arSalesOrderInvoiceLines = arInvoice.getArSalesOrderInvoiceLines();
        		

        		Collection arInvoiceLineItems = arInvoice.getArInvoiceLineItems();
        		
        		if (arInvoiceLineItems != null && !arInvoiceLineItems.isEmpty()) {
        			
        			Iterator c = arInvoiceLineItems.iterator();
        			
        			while(c.hasNext()) {
        				
        				LocalArInvoiceLineItem arInvoiceLineItem = (LocalArInvoiceLineItem) c.next();
        				
        					String II_NM = arInvoiceLineItem.getInvItemLocation().getInvItem().getIiName();
        					String LOC_NM = arInvoiceLineItem.getInvItemLocation().getInvLocation().getLocName();
        					
        					double QTY_SLD = this.convertByUomFromAndItemAndQuantity(arInvoiceLineItem.getInvUnitOfMeasure(), 
        							arInvoiceLineItem.getInvItemLocation().getInvItem(), arInvoiceLineItem.getIliQuantity(), AD_CMPNY);

    		                LocalInvCosting invCosting = null;
    		                try {
        						
        						invCosting  = invCostingHome.getByMaxCstDateToLongAndMaxCstLineNumberAndLessThanEqualCstDateAndIiNameAndLocName(arInvoice.getInvDate(), II_NM, LOC_NM, AD_BRNCH, AD_CMPNY);

        					} catch (FinderException ex) { }
        					
    						
        					
        					double COST = arInvoiceLineItem.getInvItemLocation().getInvItem().getIiUnitCost();

        					
        					if (invCosting == null ) {
        						
        						this.postToInv(arInvoiceLineItem, arInvoice.getInvDate(), 
        								QTY_SLD, COST * QTY_SLD , 
        								-QTY_SLD, -COST * QTY_SLD , 
        								0d, null, AD_BRNCH, AD_CMPNY);

        					} else {
        						
        							if(invCosting.getInvItemLocation().getInvItem().getIiCostMethod().equals("Average")){
 
        								double avgCost = invCosting.getCstRemainingQuantity() == 0 ? COST :
        										Math.abs(invCosting.getCstRemainingValue() / invCosting.getCstRemainingQuantity());
        								
        								this.postToInv(arInvoiceLineItem, arInvoice.getInvDate(), 
        										QTY_SLD, avgCost * QTY_SLD,
        										invCosting.getCstRemainingQuantity() - QTY_SLD, invCosting.getCstRemainingValue() - (avgCost * QTY_SLD), 
        										0d, null, AD_BRNCH, AD_CMPNY);
        	
        							} else if(invCosting.getInvItemLocation().getInvItem().getIiCostMethod().equals("FIFO")) {
        								
        								double fifoCost = invCosting.getCstRemainingQuantity() == 0 ? COST :
        										this.getInvFifoCost(invCosting.getCstDate(), invCosting.getInvItemLocation().getIlCode(), 
        										QTY_SLD, arInvoiceLineItem.getIliUnitPrice(), true, AD_BRNCH, AD_CMPNY);

        								this.postToInv(arInvoiceLineItem, arInvoice.getInvDate(), 
        										QTY_SLD, fifoCost * QTY_SLD, 
        										invCosting.getCstRemainingQuantity() - QTY_SLD, invCosting.getCstRemainingValue() - (fifoCost * QTY_SLD), 
        										0d, null, AD_BRNCH, AD_CMPNY);
        					
        							} else if(invCosting.getInvItemLocation().getInvItem().getIiCostMethod().equals("Standard")) {
        								
        								double standardCost = invCosting.getInvItemLocation().getInvItem().getIiUnitCost();

        								this.postToInv(arInvoiceLineItem, arInvoice.getInvDate(), 
        										QTY_SLD, standardCost * QTY_SLD, 
        										invCosting.getCstRemainingQuantity() - QTY_SLD, invCosting.getCstRemainingValue() - (standardCost * QTY_SLD),
        										0d, null, AD_BRNCH, AD_CMPNY);
        				
        							}
        						


        					}
        					
        				
        				
        			}
                  
        			
        		}  else if (arSalesOrderInvoiceLines != null && !arSalesOrderInvoiceLines.isEmpty()) {
                    
                    Iterator c = arSalesOrderInvoiceLines.iterator();
                    
                    while(c.hasNext()) {
                        
                        LocalArSalesOrderInvoiceLine arSalesOrderInvoiceLine = (LocalArSalesOrderInvoiceLine) c.next();	    					    				
                        LocalArSalesOrderLine arSalesOrderLine = arSalesOrderInvoiceLine.getArSalesOrderLine();
                        
                        String II_NM = arSalesOrderLine.getInvItemLocation().getInvItem().getIiName();
                        String LOC_NM = arSalesOrderLine.getInvItemLocation().getInvLocation().getLocName();
                        
                        double QTY_SLD = this.convertByUomFromAndItemAndQuantity(arSalesOrderLine.getInvUnitOfMeasure(), 
                                arSalesOrderLine.getInvItemLocation().getInvItem(), arSalesOrderInvoiceLine.getSilQuantityDelivered(), AD_CMPNY);
                        
                        LocalInvCosting invCosting = null;
                        
                        try {
                            
                            invCosting  = invCostingHome.getByMaxCstDateToLongAndMaxCstLineNumberAndLessThanEqualCstDateAndIiNameAndLocName(arInvoice.getInvDate(), II_NM, LOC_NM, AD_BRNCH, AD_CMPNY);
                            
                        } catch (FinderException ex) {
                            
                        }
                        
                        double COST = arSalesOrderLine.getInvItemLocation().getInvItem().getIiUnitCost();
          
                        if (invCosting == null  ) {
                            

                            this.postToInvSo(arSalesOrderInvoiceLine, arInvoice.getInvDate(), 
                            		QTY_SLD,  COST * QTY_SLD, 
                            		-QTY_SLD, -COST * QTY_SLD, 
                            		0d, null, AD_BRNCH, AD_CMPNY);
                            
                        } else {
                        	
                        	
                            if(invCosting.getInvItemLocation().getInvItem().getIiCostMethod().equals("Average")) {
                            	
	                            double avgCost = invCosting.getCstRemainingQuantity() == 0 ? COST :
	                            		Math.abs(invCosting.getCstRemainingValue() / invCosting.getCstRemainingQuantity());
	                            
                            	this.postToInvSo(arSalesOrderInvoiceLine, arInvoice.getInvDate(), 
                            			QTY_SLD, avgCost * QTY_SLD ,
	                            		invCosting.getCstRemainingQuantity() - QTY_SLD, invCosting.getCstRemainingValue() - (avgCost * QTY_SLD), 
	                            		0d, null, AD_BRNCH, AD_CMPNY);
	                            
	                           
	                            
                            } else if(invCosting.getInvItemLocation().getInvItem().getIiCostMethod().equals("FIFO")) {
                            	
        	        			double fifoCost = invCosting.getCstRemainingQuantity() == 0 ? COST :
        	        				this.getInvFifoCost(invCosting.getCstDate(), invCosting.getInvItemLocation().getIlCode(), 
        	        					QTY_SLD, arSalesOrderInvoiceLine.getArSalesOrderLine().getSolUnitPrice(), true, AD_BRNCH, AD_CMPNY);
        	        			
        	        			
        	        				this.postToInvSo(arSalesOrderInvoiceLine, arInvoice.getInvDate(), 
        	        						QTY_SLD, fifoCost * QTY_SLD, 
            	        					invCosting.getCstRemainingQuantity() - QTY_SLD, invCosting.getCstRemainingValue() - (fifoCost * QTY_SLD), 
            	        					0d, null, AD_BRNCH, AD_CMPNY);

        	        			
        	        			
                            } else if(invCosting.getInvItemLocation().getInvItem().getIiCostMethod().equals("Standard")) {
                            	
        	        			double standardCost = invCosting.getInvItemLocation().getInvItem().getIiUnitCost();
        	        			
		        	        			this.postToInvSo(arSalesOrderInvoiceLine, arInvoice.getInvDate(), 
		        	        					QTY_SLD, standardCost * QTY_SLD, 
		        	        					invCosting.getCstRemainingQuantity() - QTY_SLD, invCosting.getCstRemainingValue() - (standardCost * QTY_SLD), 
		        	        					0d, null, AD_BRNCH, AD_CMPNY);
        	        			
                            }
                        	
                        }	    					    					                			
                    }
	    			
        		}   		
        		
        		
        	} else {

        		
        		if (arInvoice.getInvVoid() == EJBCommon.TRUE && arInvoice.getInvVoidPosted() == EJBCommon.FALSE) {
            		System.out.println("VOID CREDIT MEMO---------------------------->");
            		
            		// get credited invoice
            		
            		arCreditedInvoice = arInvoiceHome.findByInvNumberAndInvCreditMemoAndBrCode(
            				arInvoice.getInvCmInvoiceNumber(), EJBCommon.FALSE, AD_BRNCH, AD_CMPNY);
            		
            		// increase customer balance
            		
            		double INV_AMNT = this.convertForeignToFunctionalCurrency(arCreditedInvoice.getGlFunctionalCurrency().getFcCode(),
            				arCreditedInvoice.getGlFunctionalCurrency().getFcName(),
    						arCreditedInvoice.getInvConversionDate(), arCreditedInvoice.getInvConversionRate(),
    						arInvoice.getInvAmountDue(), AD_CMPNY) ;
            		
            		this.post(arInvoice.getInvDate(), INV_AMNT, arInvoice.getArCustomer(), AD_CMPNY);
            		
            		// decrease invoice and ips amounts and release lock        		
            		
            		double CREDIT_PERCENT = EJBCommon.roundIt(arInvoice.getInvAmountDue() / arCreditedInvoice.getInvAmountDue(), (short)6);
            		
            		arCreditedInvoice.setInvAmountPaid(arCreditedInvoice.getInvAmountPaid() - arInvoice.getInvAmountDue());
            		
            		double TOTAL_INVOICE_PAYMENT_SCHEDULE =  0d;
            		
            		Collection arInvoicePaymentSchedules = arCreditedInvoice.getArInvoicePaymentSchedules();
            		
            		Iterator i = arInvoicePaymentSchedules.iterator();
            		
            		while (i.hasNext()) {
            			
            			LocalArInvoicePaymentSchedule arInvoicePaymentSchedule = 
            				(LocalArInvoicePaymentSchedule)i.next();
            			
            			double INVOICE_PAYMENT_SCHEDULE_AMOUNT = 0;
            			
            			// if last payment schedule subtract to avoid rounding difference error
            			
            			if (i.hasNext()) {
            				
            				INVOICE_PAYMENT_SCHEDULE_AMOUNT = EJBCommon.roundIt(arInvoicePaymentSchedule.getIpsAmountDue() * CREDIT_PERCENT, this.getGlFcPrecisionUnit(AD_CMPNY));
            				
            			} else {
            				
            				INVOICE_PAYMENT_SCHEDULE_AMOUNT = arInvoice.getInvAmountDue() - TOTAL_INVOICE_PAYMENT_SCHEDULE;
            				
            			}
            			
            			arInvoicePaymentSchedule.setIpsAmountPaid(arInvoicePaymentSchedule.getIpsAmountPaid() - INVOICE_PAYMENT_SCHEDULE_AMOUNT);
            			
            			arInvoicePaymentSchedule.setIpsLock(EJBCommon.FALSE);
            			
            			TOTAL_INVOICE_PAYMENT_SCHEDULE += INVOICE_PAYMENT_SCHEDULE_AMOUNT;
            			
            		}
            		
            		Collection arInvoiceLineItems = arInvoice.getArInvoiceLineItems();
            		
            		if (arInvoiceLineItems != null && !arInvoiceLineItems.isEmpty()) {
            			
            			Iterator c = arInvoiceLineItems.iterator();
            			
            			while(c.hasNext()) {
            				
            				LocalArInvoiceLineItem arInvoiceLineItem = (LocalArInvoiceLineItem) c.next();

        					String II_NM = arInvoiceLineItem.getInvItemLocation().getInvItem().getIiName();
        					String LOC_NM = arInvoiceLineItem.getInvItemLocation().getInvLocation().getLocName();
        					
        					double QTY_SLD = this.convertByUomFromAndItemAndQuantity(arInvoiceLineItem.getInvUnitOfMeasure(), 
        							arInvoiceLineItem.getInvItemLocation().getInvItem(), arInvoiceLineItem.getIliQuantity(), AD_CMPNY);
        					
        					LocalInvCosting invCosting = null;
        					
        					try {
        					
        						invCosting  = invCostingHome.getByMaxCstDateToLongAndMaxCstLineNumberAndLessThanEqualCstDateAndIiNameAndLocName(arInvoice.getInvDate(), II_NM, LOC_NM, AD_BRNCH, AD_CMPNY);
        						
        					} catch (FinderException ex) {
        						
        					}
        					
        					double COST = arInvoiceLineItem.getInvItemLocation().getInvItem().getIiUnitCost();
        		
        					
        					if (invCosting == null) {

        						this.postToInv(arInvoiceLineItem, arInvoice.getInvDate(), 
        								QTY_SLD, COST * QTY_SLD, 
        								-QTY_SLD, -COST * QTY_SLD, 
        								0d, null, AD_BRNCH, AD_CMPNY);
        						
        					} else {
        						
        						
        						if(invCosting.getInvItemLocation().getInvItem().getIiCostMethod().equals("Average")) {
                                    
        							double avgCost = invCosting.getCstRemainingQuantity() == 0 ? COST :
        									Math.abs(invCosting.getCstRemainingValue() / invCosting.getCstRemainingQuantity());
     
                            		this.postToInv(arInvoiceLineItem, arInvoice.getInvDate(), 
                            				QTY_SLD, avgCost * QTY_SLD,
                                    		invCosting.getCstRemainingQuantity() - QTY_SLD, invCosting.getCstRemainingValue() - (avgCost * QTY_SLD), 
                                    		0d, null, AD_BRNCH, AD_CMPNY);
                                    
                                } else if(invCosting.getInvItemLocation().getInvItem().getIiCostMethod().equals("FIFO")) {
                                	
            	        			double fifoCost = invCosting.getCstRemainingQuantity() == 0 ? COST :
            	        				this.getInvFifoCost(invCosting.getCstDate(), invCosting.getInvItemLocation().getIlCode(), 
            	        					QTY_SLD, arInvoiceLineItem.getIliUnitPrice(), true, AD_BRNCH, AD_CMPNY);
            	        			
            	        		       			
            	        			this.postToInv(arInvoiceLineItem, arInvoice.getInvDate(), 
            	        					QTY_SLD, fifoCost * QTY_SLD, 
            	        					invCosting.getCstRemainingQuantity() - QTY_SLD, invCosting.getCstRemainingValue() - (fifoCost * QTY_SLD),
            	        					0d, null, AD_BRNCH, AD_CMPNY);

                                } else if(invCosting.getInvItemLocation().getInvItem().getIiCostMethod().equals("Standard")) {
                                	
            	        			double standardCost = invCosting.getInvItemLocation().getInvItem().getIiUnitCost();
            	        			
            	        	     			
            	        			this.postToInv(arInvoiceLineItem, arInvoice.getInvDate(), 
            	        					QTY_SLD, standardCost * QTY_SLD, 
            	        					invCosting.getCstRemainingQuantity() - QTY_SLD, invCosting.getCstRemainingValue() - (standardCost * QTY_SLD), 
            	        					0d, null, AD_BRNCH, AD_CMPNY);

                                }
        					}
            					

            			}
            			
            			
            			
            		}
            		
            		// set cmAdjustment post status
                    
            		arInvoice.setInvVoidPosted(EJBCommon.TRUE);
            		arInvoice.setInvPostedBy(USR_NM);
            		arInvoice.setInvDatePosted(EJBCommon.getGcCurrentDateWoTime().getTime());
      	           
            		
            	} else if(arInvoice.getInvVoid() == EJBCommon.FALSE && arInvoice.getInvPosted() == EJBCommon.FALSE) {
            		System.out.println("POST CREDIT MEMO---------------------------->");
            		// get credited invoice
            		
            		arCreditedInvoice = arInvoiceHome.findByInvNumberAndInvCreditMemoAndBrCode(
            				arInvoice.getInvCmInvoiceNumber(), EJBCommon.FALSE, AD_BRNCH, AD_CMPNY);
            		
            		// decrease customer balance
            		
            		double INV_AMNT = this.convertForeignToFunctionalCurrency(arCreditedInvoice.getGlFunctionalCurrency().getFcCode(),
            				arCreditedInvoice.getGlFunctionalCurrency().getFcName(),
    						arCreditedInvoice.getInvConversionDate(), arCreditedInvoice.getInvConversionRate(),
    						arInvoice.getInvAmountDue(), AD_CMPNY) * -1;
            		
            		this.post(arInvoice.getInvDate(), INV_AMNT, arInvoice.getArCustomer(), AD_CMPNY);
            		
            		// decrease invoice and ips amounts and release lock        		
            		
            		double CREDIT_PERCENT = EJBCommon.roundIt(arInvoice.getInvAmountDue() / arCreditedInvoice.getInvAmountDue(), (short)6);
            		
            		arCreditedInvoice.setInvAmountPaid(arCreditedInvoice.getInvAmountPaid() + arInvoice.getInvAmountDue());
            		
            		double TOTAL_INVOICE_PAYMENT_SCHEDULE =  0d;
            		
            		Collection arInvoicePaymentSchedules = arCreditedInvoice.getArInvoicePaymentSchedules();
            		
            		Iterator i = arInvoicePaymentSchedules.iterator();
            	
            		while (i.hasNext()) {
            			
            			LocalArInvoicePaymentSchedule arInvoicePaymentSchedule = 
            				(LocalArInvoicePaymentSchedule)i.next();
            			
            			double INVOICE_PAYMENT_SCHEDULE_AMOUNT = 0;
            			
            			// if last payment schedule subtract to avoid rounding difference error
            			
            			if (i.hasNext()) {
            				
            				INVOICE_PAYMENT_SCHEDULE_AMOUNT = EJBCommon.roundIt(arInvoicePaymentSchedule.getIpsAmountDue() * CREDIT_PERCENT, this.getGlFcPrecisionUnit(AD_CMPNY));
            				
            			} else {
            				
            				INVOICE_PAYMENT_SCHEDULE_AMOUNT = arInvoice.getInvAmountDue() - TOTAL_INVOICE_PAYMENT_SCHEDULE;
            				
            			}
            			
            			arInvoicePaymentSchedule.setIpsAmountPaid(arInvoicePaymentSchedule.getIpsAmountPaid() + INVOICE_PAYMENT_SCHEDULE_AMOUNT);
            			
            			arInvoicePaymentSchedule.setIpsLock(EJBCommon.FALSE);
            			
            			TOTAL_INVOICE_PAYMENT_SCHEDULE += INVOICE_PAYMENT_SCHEDULE_AMOUNT;
            			
            		}
           
            		Collection arInvoiceLineItems = arInvoice.getArInvoiceLineItems();
            	
            		if (arInvoiceLineItems != null && !arInvoiceLineItems.isEmpty()) {
            			
            			Iterator c = arInvoiceLineItems.iterator();

            			while(c.hasNext()) {
            				
            				LocalArInvoiceLineItem arInvoiceLineItem = (LocalArInvoiceLineItem) c.next();

            					String II_NM = arInvoiceLineItem.getInvItemLocation().getInvItem().getIiName();
            					String LOC_NM = arInvoiceLineItem.getInvItemLocation().getInvLocation().getLocName();
            					
            					double QTY_SLD = this.convertByUomFromAndItemAndQuantity(arInvoiceLineItem.getInvUnitOfMeasure(), 
            							arInvoiceLineItem.getInvItemLocation().getInvItem(), arInvoiceLineItem.getIliQuantity(), AD_CMPNY);
            					
            					LocalInvCosting invCosting = null;
            					
            					try {
            						
            						invCosting  = invCostingHome.getByMaxCstDateToLongAndMaxCstLineNumberAndLessThanEqualCstDateAndIiNameAndLocName(arInvoice.getInvDate(), II_NM, LOC_NM, AD_BRNCH, AD_CMPNY);

            					} catch (FinderException ex) { }
            					
            					double COST = arInvoiceLineItem.getInvItemLocation().getInvItem().getIiUnitCost();
            					
            					if (invCosting == null) {


            						this.postToInv(arInvoiceLineItem, arInvoice.getInvDate(), 
            								-QTY_SLD, -COST * QTY_SLD, 
            								QTY_SLD, COST * QTY_SLD, 
            								0d, null, AD_BRNCH, AD_CMPNY);
            						
            					} else {
            						

            						if(invCosting.getInvItemLocation().getInvItem().getIiCostMethod().equals("Average")) {

                						double avgCost = invCosting.getCstRemainingQuantity() <= 0 ? COST :
                								Math.abs(invCosting.getCstRemainingValue() / invCosting.getCstRemainingQuantity());
    	
    	            					this.postToInv(arInvoiceLineItem, arInvoice.getInvDate(), 
    	            							-QTY_SLD, -avgCost * QTY_SLD,
    	            							invCosting.getCstRemainingQuantity() + QTY_SLD, invCosting.getCstRemainingValue() + (avgCost * QTY_SLD), 
    	            							0d, null, AD_BRNCH, AD_CMPNY);
            						
            						
            						} else if(invCosting.getInvItemLocation().getInvItem().getIiCostMethod().equals("FIFO")) {
            							
            		        			double fifoCost = invCosting.getCstRemainingQuantity() == 0 ? COST :
            		        					this.getInvFifoCost(invCosting.getCstDate(), invCosting.getInvItemLocation().getIlCode(), 
            		        					QTY_SLD, arInvoiceLineItem.getIliUnitPrice() * QTY_SLD, true, AD_BRNCH, AD_CMPNY);
            		        			
            		        			//post entries to database        			
            		        			this.postToInv(arInvoiceLineItem, arInvoice.getInvDate(),
            		        					-QTY_SLD, -fifoCost * QTY_SLD, 
            		        					invCosting.getCstRemainingQuantity() + QTY_SLD, invCosting.getCstRemainingValue() + (fifoCost * QTY_SLD), 
            		        					0d, null, AD_BRNCH, AD_CMPNY);
            		        			
            		        			
            						} else if(invCosting.getInvItemLocation().getInvItem().getIiCostMethod().equals("Standard")) {
            		        			double standardCost = invCosting.getInvItemLocation().getInvItem().getIiUnitCost();
            		        			
            		        			//post entries to database        			
            		        			this.postToInv(arInvoiceLineItem, arInvoice.getInvDate(), 
            		        					-QTY_SLD, -standardCost * QTY_SLD, 
            		        					invCosting.getCstRemainingQuantity() + QTY_SLD, invCosting.getCstRemainingValue() + (standardCost * QTY_SLD), 
            		        					0d, null, AD_BRNCH, AD_CMPNY);
            						}
            					}

            			}
            			
            		}       			           		
            		
            	}
        		
        	}
        	
        	// set invoice post status
        	
        	arInvoice.setInvPosted(EJBCommon.TRUE);
        	arInvoice.setInvPostedBy(USR_NM);
        	arInvoice.setInvDatePosted(EJBCommon.getGcCurrentDateWoTime().getTime());
        	
        	
        	// post to gl if necessary
        	
        	if (adPreference.getPrfArGlPostingType().equals("USE SL POSTING")) {
        		
        		// validate if date has no period and period is closed
        		
        		LocalGlSetOfBook glJournalSetOfBook = null;
        		
        		try {
        			
        			glJournalSetOfBook = glSetOfBookHome.findByDate(arInvoice.getInvDate(), AD_CMPNY);
        			
        		} catch (FinderException ex) {
        			
        			throw new GlJREffectiveDateNoPeriodExistException();
        			
        		}
        		
        		LocalGlAccountingCalendarValue glAccountingCalendarValue = 
        			glAccountingCalendarValueHome.findByAcCodeAndDate(
        					glJournalSetOfBook.getGlAccountingCalendar().getAcCode(), arInvoice.getInvDate(), AD_CMPNY);
        		
        		
        		if (glAccountingCalendarValue.getAcvStatus() == 'N' ||
        				glAccountingCalendarValue.getAcvStatus() == 'C' ||
						glAccountingCalendarValue.getAcvStatus() == 'P') {
        			
        			throw new GlJREffectiveDatePeriodClosedException();
        			
        		}
        		
        		// check if invoice is balance if not check suspense posting
        		
        		LocalGlJournalLine glOffsetJournalLine = null;
        		
        		Collection arDistributionRecords = arDistributionRecordHome.findImportableDrByInvCode(arInvoice.getInvCode(), AD_CMPNY);
        		
        		Iterator j = arDistributionRecords.iterator();
        		        		
        		double TOTAL_DEBIT = 0d;
        		double TOTAL_CREDIT = 0d;
        		
        		while (j.hasNext()) {
        			
        			LocalArDistributionRecord arDistributionRecord = (LocalArDistributionRecord)j.next();
        			
        			double DR_AMNT = 0d;
        			
        			if (arInvoice.getInvCreditMemo() == EJBCommon.FALSE) {
        				
        				DR_AMNT = this.convertForeignToFunctionalCurrency(arInvoice.getGlFunctionalCurrency().getFcCode(),
        						arInvoice.getGlFunctionalCurrency().getFcName(), 
								arInvoice.getInvConversionDate(),
								arInvoice.getInvConversionRate(),
								arDistributionRecord.getDrAmount(), AD_CMPNY);
        				
        			} else {
       				
        				
        				DR_AMNT = this.convertForeignToFunctionalCurrency(arCreditedInvoice.getGlFunctionalCurrency().getFcCode(),
        						arCreditedInvoice.getGlFunctionalCurrency().getFcName(), 
								arCreditedInvoice.getInvConversionDate(),
								arCreditedInvoice.getInvConversionRate(),
								arDistributionRecord.getDrAmount(), AD_CMPNY);
        				
        			}
        			
        			if (arDistributionRecord.getDrDebit() == EJBCommon.TRUE) {
        				
        				TOTAL_DEBIT += DR_AMNT;
        				
        			} else {
        				
        				TOTAL_CREDIT += DR_AMNT;
        				
        			}
        			
        		}
        		        		
        		TOTAL_DEBIT = EJBCommon.roundIt(TOTAL_DEBIT, adCompany.getGlFunctionalCurrency().getFcPrecision());
        		TOTAL_CREDIT = EJBCommon.roundIt(TOTAL_CREDIT, adCompany.getGlFunctionalCurrency().getFcPrecision());
        		        		
        		if (adPreference.getPrfAllowSuspensePosting() == EJBCommon.TRUE &&
        				TOTAL_DEBIT != TOTAL_CREDIT) {
        			
        			LocalGlSuspenseAccount glSuspenseAccount = null;
        			
        			try { 	
        				
        				glSuspenseAccount = glSuspenseAccountHome.findByJsNameAndJcName("ACCOUNTS RECEIVABLES", arInvoice.getInvCreditMemo() == EJBCommon.FALSE ? "SALES INVOICES" : "CREDIT MEMOS", AD_CMPNY);
        				
        			} catch (FinderException ex) {
        				
        				throw new GlobalJournalNotBalanceException();
        				
        			}
        			
        			if (TOTAL_DEBIT - TOTAL_CREDIT < 0) {
        				
        				glOffsetJournalLine = glJournalLineHome.create((short)(arDistributionRecords.size() + 1),
								EJBCommon.TRUE, TOTAL_CREDIT - TOTAL_DEBIT, "", AD_CMPNY);
        				
        			} else {
        				
        				glOffsetJournalLine = glJournalLineHome.create((short)(arDistributionRecords.size() + 1),
								EJBCommon.FALSE, TOTAL_DEBIT - TOTAL_CREDIT, "", AD_CMPNY);
        				
        			}
        			
        			LocalGlChartOfAccount glChartOfAccount = glSuspenseAccount.getGlChartOfAccount();
        			//glChartOfAccount.addGlJournalLine(glOffsetJournalLine);
        			glOffsetJournalLine.setGlChartOfAccount(glChartOfAccount);
        			
        			
        		} else if (adPreference.getPrfAllowSuspensePosting() == EJBCommon.FALSE &&
        				TOTAL_DEBIT != TOTAL_CREDIT) {
        			
        			throw new GlobalJournalNotBalanceException();		    	
        			
        		}
        		
        		// create journal batch if necessary
        		
        		LocalGlJournalBatch glJournalBatch = null;
        		java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat("MM/dd/yyyy");
        		
        		try {
        			
        			if (adPreference.getPrfEnableArInvoiceBatch() == EJBCommon.TRUE) {
        				
        				glJournalBatch = glJournalBatchHome.findByJbName("JOURNAL IMPORT " + formatter.format(new Date()) + " " + arInvoice.getArInvoiceBatch().getIbName(), AD_BRNCH, AD_CMPNY);
        				
        			} else {
        				
        				glJournalBatch = glJournalBatchHome.findByJbName("JOURNAL IMPORT " + formatter.format(new Date()) + " SALES INVOICES", AD_BRNCH, AD_CMPNY);
        				
        			}
        			
        			
        		} catch (FinderException ex) {
        		}
        		
        		if (adPreference.getPrfEnableGlJournalBatch() ==  EJBCommon.TRUE && 
        				glJournalBatch == null) {
        			
        			if (adPreference.getPrfEnableArInvoiceBatch() == EJBCommon.TRUE) {
        				
        				glJournalBatch = glJournalBatchHome.create("JOURNAL IMPORT " + formatter.format(new Date()) + " " + arInvoice.getArInvoiceBatch().getIbName(), "JOURNAL IMPORT", "CLOSED", EJBCommon.getGcCurrentDateWoTime().getTime(), USR_NM, AD_BRNCH, AD_CMPNY);
        				
        			} else {
        				
        				glJournalBatch = glJournalBatchHome.create("JOURNAL IMPORT " + formatter.format(new Date()) + " SALES INVOICES", "JOURNAL IMPORT", "CLOSED", EJBCommon.getGcCurrentDateWoTime().getTime(), USR_NM, AD_BRNCH, AD_CMPNY);
        				
        			}
        			
        			
        		}
        		
        		// create journal entry			            	
        		
        		LocalGlJournal glJournal = glJournalHome.create(arInvoice.getInvCreditMemo() == EJBCommon.FALSE ? arInvoice.getInvReferenceNumber() : arInvoice.getInvCmInvoiceNumber(),
        				arInvoice.getInvDescription(), arInvoice.getInvDate(),
						0.0d, null, arInvoice.getInvNumber(), null, 1d, "N/A", null,
						'N', EJBCommon.TRUE, EJBCommon.FALSE,
						USR_NM, new Date(),
						USR_NM, new Date(),
						null, null,
						USR_NM, EJBCommon.getGcCurrentDateWoTime().getTime(), 
						arInvoice.getArCustomer().getCstTin(), 
						arInvoice.getArCustomer().getCstName(), EJBCommon.FALSE,
						null, 
						AD_BRNCH, AD_CMPNY);
        		
        		LocalGlJournalSource glJournalSource = glJournalSourceHome.findByJsName("ACCOUNTS RECEIVABLES", AD_CMPNY);
        		glJournal.setGlJournalSource(glJournalSource);
        		
        		LocalGlFunctionalCurrency glFunctionalCurrency = glFunctionalCurrencyHome.findByFcName(adCompany.getGlFunctionalCurrency().getFcName(), AD_CMPNY);
        		glJournal.setGlFunctionalCurrency(glFunctionalCurrency);
        		
        		LocalGlJournalCategory glJournalCategory = glJournalCategoryHome.findByJcName(arInvoice.getInvCreditMemo() == EJBCommon.FALSE ? "SALES INVOICES" : "CREDIT MEMOS", AD_CMPNY);
        		glJournal.setGlJournalCategory(glJournalCategory);
        		
        		if (glJournalBatch != null) {

        			glJournal.setGlJournalBatch(glJournalBatch);
        			
        		}           		    
        		
        		
        		// create journal lines
        		
        		j = arDistributionRecords.iterator();
        		
        		while (j.hasNext()) {
        			
        			LocalArDistributionRecord arDistributionRecord = (LocalArDistributionRecord)j.next();
        			
        			double DR_AMNT = 0d;
        			
        			if (arInvoice.getInvCreditMemo() == EJBCommon.FALSE) {
        				
        				DR_AMNT = this.convertForeignToFunctionalCurrency(arInvoice.getGlFunctionalCurrency().getFcCode(),
        						arInvoice.getGlFunctionalCurrency().getFcName(), 
								arInvoice.getInvConversionDate(),
								arInvoice.getInvConversionRate(),
								arDistributionRecord.getDrAmount(), AD_CMPNY);
        				
        			} else {
        				
        				DR_AMNT = this.convertForeignToFunctionalCurrency(arCreditedInvoice.getGlFunctionalCurrency().getFcCode(),
        						arCreditedInvoice.getGlFunctionalCurrency().getFcName(), 
								arCreditedInvoice.getInvConversionDate(),
								arCreditedInvoice.getInvConversionRate(),
								arDistributionRecord.getDrAmount(), AD_CMPNY);
        				
        			}
        			
        			LocalGlJournalLine glJournalLine = glJournalLineHome.create(arDistributionRecord.getDrLine(),	            			
							arDistributionRecord.getDrDebit(), DR_AMNT, "", AD_CMPNY);
        			
        			glJournalLine.setGlChartOfAccount(arDistributionRecord.getGlChartOfAccount());

        			glJournalLine.setGlJournal(glJournal);
        			
        			arDistributionRecord.setDrImported(EJBCommon.TRUE);
        			
    		  	  	// for FOREX revaluation
            	    
            	    LocalArInvoice arInvoiceTemp = arInvoice.getInvCreditMemo() == EJBCommon.FALSE ?
            	    	arInvoice: arCreditedInvoice;
            	    		
    		  	  	if((arInvoiceTemp.getGlFunctionalCurrency().getFcCode() !=
    		  	  			adCompany.getGlFunctionalCurrency().getFcCode()) &&
    					glJournalLine.getGlChartOfAccount().getGlFunctionalCurrency() != null &&
    		  	  		(glJournalLine.getGlChartOfAccount().getGlFunctionalCurrency().getFcCode().equals(
    		  	  		arInvoiceTemp.getGlFunctionalCurrency().getFcCode()))){
    		  	  		
    		  	  		double CONVERSION_RATE = 1;
    		  	  		
    		  	  		if (arInvoiceTemp.getInvConversionRate() != 0 && arInvoiceTemp.getInvConversionRate() != 1) {
    		  	  			
    		  	  			CONVERSION_RATE = arInvoiceTemp.getInvConversionRate();
    		  	  			
    		  	  		} else if (arInvoice.getInvConversionDate() != null){
    		  	  			
    		  	  			CONVERSION_RATE = this.getFrRateByFrNameAndFrDate(
    			  	  				glJournalLine.getGlChartOfAccount().getGlFunctionalCurrency().getFcName(), 
    								glJournal.getJrConversionDate(), AD_CMPNY);
    		  	  			
    		  	  		}
    		  	  		
    		  	  		Collection glForexLedgers = null;
    		  	  		
    		  	  		try {
    		  	  			
    		  	  			glForexLedgers = glForexLedgerHome.findLatestGlFrlByFrlDateAndByCoaCode(
    		  	  				arInvoiceTemp.getInvDate(), glJournalLine.getGlChartOfAccount().getCoaCode(), AD_CMPNY);
    		  	  			
    		  	  		} catch(FinderException ex) {
    		  	  			
    		  	  		}
    		  	  		
    		  	  		LocalGlForexLedger glForexLedger =
    		  	  			(glForexLedgers.isEmpty() || glForexLedgers == null) ? null :
    		  	  				(LocalGlForexLedger) glForexLedgers.iterator().next();
    		  	  		
    		  	  		int FRL_LN = (glForexLedger != null &&
    		  	  				glForexLedger.getFrlDate().compareTo(arInvoiceTemp.getInvDate()) == 0) ?
    		  	  						glForexLedger.getFrlLine().intValue() + 1 : 1;
    		  	  		
    		  	  		// compute balance
    		  	  		double COA_FRX_BLNC = glForexLedger == null ? 0 : glForexLedger.getFrlBalance();
    		  	  		double FRL_AMNT = arDistributionRecord.getDrAmount();
    		  	  		
    		  	  		if(glJournalLine.getGlChartOfAccount().getCoaAccountType().equalsIgnoreCase("ASSET"))
    		  	  			FRL_AMNT = (glJournalLine.getJlDebit() == EJBCommon.TRUE  ? FRL_AMNT : (- 1 * FRL_AMNT));  
    		  	  		else
    		  	  			FRL_AMNT = (glJournalLine.getJlDebit() == EJBCommon.TRUE  ? (- 1 * FRL_AMNT) : FRL_AMNT);
    		  	  		
    		  	  		COA_FRX_BLNC = COA_FRX_BLNC + FRL_AMNT;
    		  	  		
    		  	  		glForexLedger = glForexLedgerHome.create(arInvoiceTemp.getInvDate(), new Integer (FRL_LN),
    		  	  			arInvoice.getInvCreditMemo() == EJBCommon.FALSE ? "SI" : "CM", FRL_AMNT, CONVERSION_RATE,
    		  	  			COA_FRX_BLNC, 0d, AD_CMPNY);
    		  	  		
    		  	  		glJournalLine.getGlChartOfAccount().addGlForexLedger(glForexLedger);
    		  	  		
    		  	  		// propagate balances
    		  	  		try{
    		  	  			
    		  	  			glForexLedgers = glForexLedgerHome.findByGreaterThanFrlDateAndCoaCode(
    		  	  				glForexLedger.getFrlDate(), glForexLedger.getGlChartOfAccount().getCoaCode(),
								glForexLedger.getFrlAdCompany());
    		  	  			
    		  	  		} catch (FinderException ex) {
    		  	  			
    		  	  		}
    		  	  		
    		  	  		Iterator itrFrl = glForexLedgers.iterator();
    		  	  		
    		  	  		while (itrFrl.hasNext()) {
    		  	  			
    		  	  			glForexLedger = (LocalGlForexLedger) itrFrl.next();
    		  	  			FRL_AMNT = arDistributionRecord.getDrAmount();
    		  	  			
    		  	  			if(glJournalLine.getGlChartOfAccount().getCoaAccountType().equalsIgnoreCase("ASSET"))
    		  	  				FRL_AMNT = (glJournalLine.getJlDebit() == EJBCommon.TRUE  ? FRL_AMNT :
    		  	  					(- 1 * FRL_AMNT));
    		  	  			else
    		  	  				FRL_AMNT = (glJournalLine.getJlDebit() == EJBCommon.TRUE  ? (- 1 * FRL_AMNT) :
    		  	  					FRL_AMNT);
    		  	  			
    		  	  			glForexLedger.setFrlBalance(glForexLedger.getFrlBalance() + FRL_AMNT);
    		  	  			
    		  	  		}
    		  	  		
    		  	  	}
    		  	  	
        		}
        		
        		if (glOffsetJournalLine != null) {
        			
        			//glJournal.addGlJournalLine(glOffsetJournalLine);
        			glOffsetJournalLine.setGlJournal(glJournal);
        			
        		}		
        		
        		// post journal to gl
        		
        		Collection glJournalLines = glJournal.getGlJournalLines();
        		
        		Iterator i = glJournalLines.iterator();
        		
        		while (i.hasNext()) {
        			
        			LocalGlJournalLine glJournalLine = (LocalGlJournalLine)i.next();	
        			
        			// post current to current acv
        			
        			this.postToGl(glAccountingCalendarValue,
        					glJournalLine.getGlChartOfAccount(),
							true, glJournalLine.getJlDebit(), glJournalLine.getJlAmount(), AD_CMPNY);
        			
        			
        			// post to subsequent acvs (propagate)
        			
        			Collection glSubsequentAccountingCalendarValues = 
        				glAccountingCalendarValueHome.findSubsequentAcvByAcCodeAndAcvPeriodNumber( 
        						glJournalSetOfBook.getGlAccountingCalendar().getAcCode(),
								glAccountingCalendarValue.getAcvPeriodNumber(), AD_CMPNY);     
        			
        			Iterator acvsIter = glSubsequentAccountingCalendarValues.iterator();
        			
        			while (acvsIter.hasNext()) {
        				
        				LocalGlAccountingCalendarValue glSubsequentAccountingCalendarValue = 
        					(LocalGlAccountingCalendarValue)acvsIter.next();
        				
        				this.postToGl(glSubsequentAccountingCalendarValue,
        						glJournalLine.getGlChartOfAccount(),
								false, glJournalLine.getJlDebit(), glJournalLine.getJlAmount(), AD_CMPNY);
        				
        			}
        			
        			// post to subsequent years if necessary
        			
        			Collection glSubsequentSetOfBooks = glSetOfBookHome.findSubsequentSobByAcYear(glJournalSetOfBook.getGlAccountingCalendar().getAcYear(), AD_CMPNY);
        			
        			if (!glSubsequentSetOfBooks.isEmpty() && glJournalSetOfBook.getSobYearEndClosed() == 1) {
        				
        				adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);		  	  	
        				LocalGlChartOfAccount glRetainedEarningsAccount = glChartOfAccountHome.findByCoaAccountNumber(adCompany.getCmpRetainedEarnings(), AD_CMPNY); 	  	
        				
        				Iterator sobIter = glSubsequentSetOfBooks.iterator();
        				
        				while (sobIter.hasNext()) {
        					
        					LocalGlSetOfBook glSubsequentSetOfBook = (LocalGlSetOfBook)sobIter.next();
        					
        					String ACCOUNT_TYPE = glJournalLine.getGlChartOfAccount().getCoaAccountType();
        					
        					// post to subsequent acvs of subsequent set of book(propagate)
        					
        					Collection glAccountingCalendarValues = 
        						glAccountingCalendarValueHome.findByAcCode(glSubsequentSetOfBook.getGlAccountingCalendar().getAcCode(), AD_CMPNY);     
        					
        					Iterator acvIter = glAccountingCalendarValues.iterator();
        					
        					while (acvIter.hasNext()) {
        						
        						LocalGlAccountingCalendarValue glSubsequentAccountingCalendarValue = 
        							(LocalGlAccountingCalendarValue)acvIter.next();
        						
        						if (ACCOUNT_TYPE.equals("ASSET") || ACCOUNT_TYPE.equals("LIABILITY") ||
        								ACCOUNT_TYPE.equals("OWNERS EQUITY")) {
        							
        							this.postToGl(glSubsequentAccountingCalendarValue,
        									glJournalLine.getGlChartOfAccount(),
											false, glJournalLine.getJlDebit(), glJournalLine.getJlAmount(), AD_CMPNY);
        							
        						} else { // revenue & expense
        							
        							this.postToGl(glSubsequentAccountingCalendarValue,
        									glRetainedEarningsAccount,
											false, glJournalLine.getJlDebit(), glJournalLine.getJlAmount(), AD_CMPNY);					        
        							
        						}
        						
        					}
        					
        					if (glSubsequentSetOfBook.getSobYearEndClosed() == 0) break;
        					
        				}
        				
        			}

        		}	   			   
        		
        	}
        	
        	Debug.print("ArInvoicePostControllerBean executeApInvPost " + txnStartDate);
        	
        } catch (GlJREffectiveDateNoPeriodExistException ex) {
        	
        	getSessionContext().setRollbackOnly();
        	throw ex;
        	
        } catch (GlJREffectiveDatePeriodClosedException ex) {
        	
        	getSessionContext().setRollbackOnly();
        	throw ex;
        	
        } catch (GlobalJournalNotBalanceException ex) {
        	
        	getSessionContext().setRollbackOnly();
        	throw ex;
        
        } catch (GlobalRecordAlreadyDeletedException ex) {
        	
        	getSessionContext().setRollbackOnly();
        	throw ex;
        	        	
        } catch (GlobalTransactionAlreadyPostedException ex) {
        	
        	getSessionContext().setRollbackOnly();
        	throw ex;
        	
        } catch (GlobalTransactionAlreadyVoidException ex) {
        	
        	getSessionContext().setRollbackOnly();
        	throw ex;        	
        	
        } catch (GlobalInventoryDateException ex) {
        	
        	getSessionContext().setRollbackOnly();
        	throw ex;        	        
        	
        } catch (GlobalBranchAccountNumberInvalidException ex) {
        	
        	getSessionContext().setRollbackOnly();
        	throw ex;        	        
        	
        } catch (AdPRFCoaGlVarianceAccountNotFoundException ex) {
        	
        	getSessionContext().setRollbackOnly();
        	throw ex;

        } catch (GlobalExpiryDateNotFoundException ex) {
        	
        	getSessionContext().setRollbackOnly();
        	throw ex;

    

        } catch (Exception ex) {
    		
    		Debug.printStackTrace(ex);
    		getSessionContext().setRollbackOnly();
    		throw new EJBException(ex.getMessage());
    		
    	}
    	
    }


   /**
    * @ejb:interface-method view-type="remote"
    * @jboss:method-attributes read-only="true"
    **/
    public short getGlFcPrecisionUnit(Integer AD_CMPNY) {

       Debug.print("ArInvoicePostControllerBean getGlFcPrecisionUnit");

       	
       LocalAdCompanyHome adCompanyHome = null;
      
     
       // Initialize EJB Home
        
       try {
            
           adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
              lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);       
            
       } catch (NamingException ex) {
            
           throw new EJBException(ex.getMessage());
            
       }

       try {
       	
         LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
            
         return  adCompany.getGlFunctionalCurrency().getFcPrecision();
                                     
       } catch (Exception ex) {
       	 
       	 Debug.printStackTrace(ex);
         throw new EJBException(ex.getMessage());
         
       }

    }
    
    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public byte getAdPrfArUseCustomerPulldown(Integer AD_CMPNY) {
    	
    	Debug.print("ArInvoicePostControllerBean getAdPrfArUseCustomerPulldown");
    	
    	LocalAdPreferenceHome adPreferenceHome = null;         
    	
    	// Initialize EJB Home
    	
    	try {
    		
    		adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);       
    		
    	} catch (NamingException ex) {
    		
    		throw new EJBException(ex.getMessage());
    		
    	}
    	
    	
    	try {
    		
    		LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);
    		
    		return adPreference.getPrfArUseCustomerPulldown();
    		
    	} catch (Exception ex) {
    		
    		Debug.printStackTrace(ex);
    		throw new EJBException(ex.getMessage());
    		
    	}
    	
    }
    
    // private methods
    
    
    private double getInvFifoCost(Date CST_DT, Integer IL_CODE, double CST_QTY, double CST_COST, 
   		 boolean isAdjustFifo, Integer AD_BRNCH, Integer AD_CMPNY)
	 {
	  	 
		 LocalInvCostingHome invCostingHome = null;
	  	 LocalInvItemLocationHome invItemLocationHome = null;
	       
	     // Initialize EJB Home
	        
	     try {
	         
	    	 invCostingHome = (LocalInvCostingHome)EJBHomeFactory.
	    	 	lookUpLocalHome(LocalInvCostingHome.JNDI_NAME, LocalInvCostingHome.class);
	    	 invItemLocationHome = (LocalInvItemLocationHome)EJBHomeFactory.
	    	 	lookUpLocalHome(LocalInvItemLocationHome.JNDI_NAME, LocalInvItemLocationHome.class);
	     } 
	     catch (NamingException ex) {
	            
	    	 throw new EJBException(ex.getMessage());
	     }
	    	
		try {
			
			Collection invFifoCostings = invCostingHome.findFifoRemainingQuantityByLessThanOrEqualCstDateAndIlCodeAndBrCode(CST_DT, IL_CODE, AD_BRNCH, AD_CMPNY);
			
			if (invFifoCostings.size() > 0) {
				
				Iterator x = invFifoCostings.iterator();
			
	  			if (isAdjustFifo) {
	  				
	  				//executed during POST transaction
	  				
	  				double totalCost = 0d;
	  				double cost;
	  				
	  				if(CST_QTY < 0) {
	  					
	  					//for negative quantities
	 	  				double neededQty = -(CST_QTY);
	 	  				
	 	  				while(x.hasNext() && neededQty != 0) {
	 	 	  				
	 	 	  				LocalInvCosting invFifoCosting = (LocalInvCosting)x.next();
	
	 		 	  			if (invFifoCosting.getApPurchaseOrderLine() != null || invFifoCosting.getApVoucherLineItem() != null) {  				
	 		 	  				cost = invFifoCosting.getCstItemCost() / invFifoCosting.getCstQuantityReceived();  				
	 		 	  			} else if(invFifoCosting.getArInvoiceLineItem() != null) {
	 		 	  				cost = invFifoCosting.getCstCostOfSales() / invFifoCosting.getCstQuantitySold();
	 		 	  			} else if (invFifoCosting.getInvBuildUnbuildAssemblyLine() != null) {  				
	 		 	  				cost = invFifoCosting.getCstAssemblyCost() / invFifoCosting.getCstAssemblyQuantity();
	 		 	  			} else {
	 		 	  				cost = invFifoCosting.getCstAdjustCost() /invFifoCosting.getCstAdjustQuantity();
	 		 	  			}
	
	 	  					if(neededQty <= invFifoCosting.getCstRemainingLifoQuantity()) {
	 	  						
	 			  				invFifoCosting.setCstRemainingLifoQuantity(invFifoCosting.getCstRemainingLifoQuantity() - neededQty);
	 			  				totalCost += (neededQty * cost);
	 			  				neededQty = 0d;	 			  				 
	 	  					} else {
	 	  						
	 	  						neededQty -= invFifoCosting.getCstRemainingLifoQuantity();
	 	  						totalCost += (invFifoCosting.getCstRemainingLifoQuantity() * cost);
	 	  						invFifoCosting.setCstRemainingLifoQuantity(0);
	 	  					}
	 	  				}
	 	  				
	 	  				//if needed qty is not yet satisfied but no more quantities to fetch, get the default cost
	 	  				if(neededQty != 0) {
	 	  					
	 	  					LocalInvItemLocation invItemLocation = invItemLocationHome.findByPrimaryKey(IL_CODE);
	 	  					totalCost += (neededQty * invItemLocation.getInvItem().getIiUnitCost());
	 	  				}
	 	  				
	 	  				cost = totalCost / -CST_QTY;
	  				} 
	  				
	  				else {
	  					
	  					//for positive quantities
	  					cost = CST_COST;
	  				}
	  				return cost;
	  			}
	  			
	  			else {
	  				
	  				//executed during ENTRY transaction
	  				
	  				LocalInvCosting invFifoCosting = (LocalInvCosting)x.next();
	  				
	 	  			if (invFifoCosting.getApPurchaseOrderLine() != null || invFifoCosting.getApVoucherLineItem() != null) {
	 	  				return EJBCommon.roundIt(invFifoCosting.getCstItemCost() / invFifoCosting.getCstQuantityReceived(), this.getGlFcPrecisionUnit(AD_CMPNY));
	 	  			} else if(invFifoCosting.getArInvoiceLineItem() != null) {
		 	  			return  EJBCommon.roundIt(invFifoCosting.getCstCostOfSales() / invFifoCosting.getCstQuantitySold(), this.getGlFcPrecisionUnit(AD_CMPNY));
	 	  			} else if (invFifoCosting.getInvBuildUnbuildAssemblyLine() != null) {
	 	  				return EJBCommon.roundIt(invFifoCosting.getCstAssemblyCost() / invFifoCosting.getCstAssemblyQuantity(), this.getGlFcPrecisionUnit(AD_CMPNY));
	 	  			} else {
	 	  				return EJBCommon.roundIt(invFifoCosting.getCstAdjustCost() /invFifoCosting.getCstAdjustQuantity(), this.getGlFcPrecisionUnit(AD_CMPNY));
	 	  			}
	  			}
			} 
			else {
				
				//most applicable in 1st entries of data
				LocalInvItemLocation invItemLocation = invItemLocationHome.findByPrimaryKey(IL_CODE);
				return invItemLocation.getInvItem().getIiUnitCost();
			}
				
		}
		catch (Exception ex) {
			Debug.printStackTrace(ex);
		    throw new EJBException(ex.getMessage());
		}
	}     

    
    
    private void post(Date INV_DT, double INV_AMNT, LocalArCustomer arCustomer, Integer AD_CMPNY) {
    	
       Debug.print("ArInvoicePostControllerBean post");
    	
       LocalArCustomerBalanceHome arCustomerBalanceHome = null;
        
       // Initialize EJB Home
        
       try {
                        
           arCustomerBalanceHome = (LocalArCustomerBalanceHome)EJBHomeFactory.
               lookUpLocalHome(LocalArCustomerBalanceHome.JNDI_NAME, LocalArCustomerBalanceHome.class);              
            
       } catch (NamingException ex) {
        	
           getSessionContext().setRollbackOnly();            
           throw new EJBException(ex.getMessage());
            
       }
        
       try {
        		        	
	       // find customer balance before or equal invoice date
	    	
	       Collection arCustomerBalances = arCustomerBalanceHome.findByBeforeOrEqualInvDateAndCstCustomerCode(INV_DT, arCustomer.getCstCustomerCode(), AD_CMPNY);
	    	
	       if (!arCustomerBalances.isEmpty()) {
	    		
	    	   // get last invoice
	    	    
	    	   ArrayList arCustomerBalanceList = new ArrayList(arCustomerBalances);
	    	    	    	    
	    	   LocalArCustomerBalance arCustomerBalance = (LocalArCustomerBalance)arCustomerBalanceList.get(arCustomerBalanceList.size() - 1);
	    	    
	    	   if (arCustomerBalance.getCbDate().before(INV_DT)) {
	    	    		    	    	
	    	       // create new balance
	    	    	
	    	       LocalArCustomerBalance apNewCustomerBalance = arCustomerBalanceHome.create(
		    	       INV_DT, arCustomerBalance.getCbBalance() + INV_AMNT, AD_CMPNY);

		           //arCustomer.addArCustomerBalance(apNewCustomerBalance);
		           apNewCustomerBalance.setArCustomer(arCustomer);
		           		        	
	    	   } else { // equals to invoice date
	    	   
	    	       arCustomerBalance.setCbBalance(arCustomerBalance.getCbBalance() + INV_AMNT);
	    	   
	    	   } 
	    	    
	    	} else {        	
	    	
	    	    // create new balance
	    	    
		    	LocalArCustomerBalance apNewCustomerBalance = arCustomerBalanceHome.create(
		    		INV_DT, INV_AMNT, AD_CMPNY);

		        //arCustomer.addArCustomerBalance(apNewCustomerBalance);
		        apNewCustomerBalance.setArCustomer(arCustomer);
		        		        		        
	     	}
	     	
	     	// propagate to subsequent balances if necessary
	     	
	     	arCustomerBalances = arCustomerBalanceHome.findByAfterInvDateAndCstCustomerCode(INV_DT, arCustomer.getCstCustomerCode(), AD_CMPNY);
	     	
	     	Iterator i = arCustomerBalances.iterator();
	     	
	     	while (i.hasNext()) {
	     		
	     		LocalArCustomerBalance arCustomerBalance = (LocalArCustomerBalance)i.next();
	     		
	     		arCustomerBalance.setCbBalance(arCustomerBalance.getCbBalance() + INV_AMNT);
	     		
	     	}
	     		     	        	
        } catch (Exception ex) {
        	
        	Debug.printStackTrace(ex);
        	getSessionContext().setRollbackOnly();            
            throw new EJBException(ex.getMessage());
            
        }
    	
	}
		    
    private double convertForeignToFunctionalCurrency(Integer FC_CODE, String FC_NM, 
	    Date CONVERSION_DATE, double CONVERSION_RATE, double AMOUNT, Integer AD_CMPNY) {
	    	
	    Debug.print("ArInvoicePostControllerBean convertForeignToFunctionalCurrency");
	    
	    
        LocalGlFunctionalCurrencyRateHome glFunctionalCurrencyRateHome = null;
        LocalAdCompanyHome adCompanyHome = null;
         
        LocalAdCompany adCompany = null;
                 
        // Initialize EJB Homes
         
        try {
         	
            glFunctionalCurrencyRateHome = (LocalGlFunctionalCurrencyRateHome)EJBHomeFactory.
               lookUpLocalHome(LocalGlFunctionalCurrencyRateHome.JNDI_NAME, LocalGlFunctionalCurrencyRateHome.class);                         
            adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
               lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);             
                   
         } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
         }
         
         // get company and extended precision
         
         try {
         	
             adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
         	             
         } catch (Exception ex) {
         	
             throw new EJBException(ex.getMessage());
         	
         }	     
        
         
         // Convert to functional currency if necessary
         
         if (CONVERSION_RATE != 1 && CONVERSION_RATE != 0 ) {
         	         
             AMOUNT = AMOUNT / CONVERSION_RATE;
             	
         }
         
         return EJBCommon.roundIt(AMOUNT, adCompany.getGlFunctionalCurrency().getFcPrecision());
         
	}
    
    
    
	
   private void postToGl(LocalGlAccountingCalendarValue glAccountingCalendarValue, 
      LocalGlChartOfAccount glChartOfAccount, 
      boolean isCurrentAcv, byte isDebit, double JL_AMNT, Integer AD_CMPNY) {
      	
      Debug.print("ArInvoicePostControllerBean postToGl");
      
      LocalGlChartOfAccountBalanceHome glChartOfAccountBalanceHome = null;
      LocalAdCompanyHome adCompanyHome = null;
      
     
       // Initialize EJB Home
        
       try {
            
           glChartOfAccountBalanceHome = (LocalGlChartOfAccountBalanceHome)EJBHomeFactory.
              lookUpLocalHome(LocalGlChartOfAccountBalanceHome.JNDI_NAME, LocalGlChartOfAccountBalanceHome.class);                   
           adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
              lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);                   
              
            
       } catch (NamingException ex) {
            
           throw new EJBException(ex.getMessage());
            
       }
       
       try {          
               
               LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);                       
       	   
	       	   LocalGlChartOfAccountBalance glChartOfAccountBalance = 
		            glChartOfAccountBalanceHome.findByAcvCodeAndCoaCode(
		               	  glAccountingCalendarValue.getAcvCode(),
		               	  glChartOfAccount.getCoaCode(), AD_CMPNY);
	       	   Debug.print("ArInvoicePostControllerBean postToGl A");    	  
	           String ACCOUNT_TYPE = glChartOfAccount.getCoaAccountType();
	           short FC_EXTNDD_PRCSN = adCompany.getGlFunctionalCurrency().getFcPrecision();
	           
	           
	               	  
	           if (((ACCOUNT_TYPE.equals("ASSET") || ACCOUNT_TYPE.equals("EXPENSE")) &&
				    isDebit == EJBCommon.TRUE) ||
				    (!ACCOUNT_TYPE.equals("ASSET") && !ACCOUNT_TYPE.equals("EXPENSE") &&
				    isDebit == EJBCommon.FALSE)) {				    
				    			
 			        glChartOfAccountBalance.setCoabEndingBalance(
				       EJBCommon.roundIt(glChartOfAccountBalance.getCoabEndingBalance() + JL_AMNT, FC_EXTNDD_PRCSN)); 					   
				       
				    if (!isCurrentAcv) {
				    	
				    	glChartOfAccountBalance.setCoabBeginningBalance(
				       		EJBCommon.roundIt(glChartOfAccountBalance.getCoabBeginningBalance() + JL_AMNT, FC_EXTNDD_PRCSN)); 					   
				    	
					}
				    
		
			  } else {
	
				    glChartOfAccountBalance.setCoabEndingBalance(
				       EJBCommon.roundIt(glChartOfAccountBalance.getCoabEndingBalance() - JL_AMNT, FC_EXTNDD_PRCSN));
				       
				    if (!isCurrentAcv) {
				    	
				    	glChartOfAccountBalance.setCoabBeginningBalance(
				       		EJBCommon.roundIt(glChartOfAccountBalance.getCoabBeginningBalance() - JL_AMNT, FC_EXTNDD_PRCSN)); 					   
				    	
					}
	
		 	  }
		 	  
		 	  if (isCurrentAcv) { 
		 	 
			 	 if (isDebit == EJBCommon.TRUE) {
			 	 	
		 			glChartOfAccountBalance.setCoabTotalDebit(
		 				EJBCommon.roundIt(glChartOfAccountBalance.getCoabTotalDebit() + JL_AMNT, FC_EXTNDD_PRCSN));	 			
		 				
		 		 } else {
		 		 	
		 			glChartOfAccountBalance.setCoabTotalCredit(
		 				EJBCommon.roundIt(glChartOfAccountBalance.getCoabTotalCredit() + JL_AMNT, FC_EXTNDD_PRCSN));	 			
		 		 }       	   
		 		 
		 	}
       	
       } catch (Exception ex) {
       	
       	   Debug.printStackTrace(ex);
       	   throw new EJBException(ex.getMessage());
       	
       }
      
      
   }
   
   private void postToInv(LocalArInvoiceLineItem arInvoiceLineItem, Date CST_DT, double CST_QTY_SLD, double CST_CST_OF_SLS,
   		double CST_RMNNG_QTY, double CST_RMNNG_VL, double CST_VRNC_VL, String USR_NM, Integer AD_BRNCH, Integer AD_CMPNY) throws
		AdPRFCoaGlVarianceAccountNotFoundException, GlobalExpiryDateNotFoundException {
    
    	Debug.print("ArInvoicePostControllerBean postToInv");
    	
    	LocalInvCostingHome invCostingHome = null;
    	LocalAdPreferenceHome adPreferenceHome = null;
    	LocalAdCompanyHome adCompanyHome = null;
    	LocalInvAdjustmentLineHome invAdjustmentLineHome = null;
    	
        // Initialize EJB Home
        
        try {
            
          invCostingHome = (LocalInvCostingHome)EJBHomeFactory.
              lookUpLocalHome(LocalInvCostingHome.JNDI_NAME, LocalInvCostingHome.class);
          adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
              lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);
          adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
              lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);              
          invAdjustmentLineHome = (LocalInvAdjustmentLineHome)EJBHomeFactory.
      	  	  lookUpLocalHome(LocalInvAdjustmentLineHome.JNDI_NAME, LocalInvAdjustmentLineHome.class);              
            
        } catch (NamingException ex) {
            
          throw new EJBException(ex.getMessage());
            
        }
      
        try {
        
           LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);
           LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
           LocalInvItemLocation invItemLocation = arInvoiceLineItem.getInvItemLocation();
           int CST_LN_NMBR = 0;
           
           CST_QTY_SLD = EJBCommon.roundIt(CST_QTY_SLD, adPreference.getPrfInvQuantityPrecisionUnit());
           CST_CST_OF_SLS = EJBCommon.roundIt(CST_CST_OF_SLS, adCompany.getGlFunctionalCurrency().getFcPrecision());
           CST_RMNNG_QTY = EJBCommon.roundIt(CST_RMNNG_QTY, adPreference.getPrfInvQuantityPrecisionUnit());
           CST_RMNNG_VL = EJBCommon.roundIt(CST_RMNNG_VL, adCompany.getGlFunctionalCurrency().getFcPrecision());
           
           if (CST_QTY_SLD > 0 && arInvoiceLineItem.getIliEnableAutoBuild() == EJBCommon.FALSE) {
           
       	       	invItemLocation.setIlCommittedQuantity(invItemLocation.getIlCommittedQuantity() - CST_QTY_SLD);

           }
           
           try {
           
           	   // generate line number
               LocalInvCosting invCurrentCosting = invCostingHome.getByMaxCstLineNumberAndCstDateToLongAndIiNameAndLocName(CST_DT.getTime(), invItemLocation.getInvItem().getIiName(), invItemLocation.getInvLocation().getLocName(), AD_BRNCH, AD_CMPNY);
               CST_LN_NMBR = invCurrentCosting.getCstLineNumber() + 1;
               Debug.print("ArInvoicePostControllerBean postToInv A");
           } catch (FinderException ex) {
           
           	   CST_LN_NMBR = 1;
           
           }

           /*if (adPreference.getPrfArAllowPriorDate() == EJBCommon.FALSE) {
	           //void subsequent cost variance adjustments
	           Collection invAdjustmentLines = invAdjustmentLineHome.findUnvoidAndIsCostVarianceGreaterThanAdjDateAndIlCodeAndBrCode(
	           		CST_DT, invItemLocation.getIlCode(), AD_BRNCH, AD_CMPNY);
	           Iterator i = invAdjustmentLines.iterator();
	           Debug.print("ArInvoicePostControllerBean postToInv B");
	           while (i.hasNext()){
	           	
	           	LocalInvAdjustmentLine invAdjustmentLine = (LocalInvAdjustmentLine) i.next();
	           	this.voidInvAdjustment(invAdjustmentLine.getInvAdjustment(), AD_BRNCH, AD_CMPNY);
	           	
	           }
           }*/
           
           String prevExpiryDates = "";
           String miscListPrpgt ="";
           double qtyPrpgt = 0;
           double qtyPrpgt2 = 0;
           try {           
        	   LocalInvCosting prevCst = invCostingHome.getByMaxCstDateToLongAndMaxCstLineNumberAndLessThanEqualCstDateAndRemainingQuantityNotEqualToZeroAndIlCode(
        			   CST_DT, invItemLocation.getIlCode(), AD_BRNCH, AD_CMPNY);
        	   Debug.print("ArInvoicePostControllerBean postToInv C");
        	   prevExpiryDates = prevCst.getCstExpiryDate();
        	   qtyPrpgt = prevCst.getCstRemainingQuantity();
        	   
        	   if (prevExpiryDates==null){
        		   prevExpiryDates="";
        	   }

           }catch (Exception ex){
        	   System.out.println("prevExpiryDates CATCH: " + prevExpiryDates);
        	   prevExpiryDates="";
           }

           // create costing
           LocalInvCosting invCosting = invCostingHome.create(CST_DT, CST_DT.getTime(), CST_LN_NMBR, 0d, 0d, 0d, 0d, 0d, 0d, CST_QTY_SLD, CST_CST_OF_SLS, CST_RMNNG_QTY, CST_RMNNG_VL, 0d, 0d, 
        		   0d, AD_BRNCH, AD_CMPNY);
           //invItemLocation.addInvCosting(invCosting);
           invCosting.setInvItemLocation(invItemLocation);
           invCosting.setArInvoiceLineItem(arInvoiceLineItem);   
           Debug.print("ArInvoicePostControllerBean postToInv D");
           String check="";
//         Get Latest Expiry Dates           
           
           if(arInvoiceLineItem.getInvItemLocation().getInvItem().getIiTraceMisc()!=0){
        	   if (arInvoiceLineItem.getArInvoice().getInvCreditMemo() == EJBCommon.FALSE) {
        		   if(prevExpiryDates!=null && prevExpiryDates!="" && prevExpiryDates.length()!=0){
        			   if(arInvoiceLineItem.getIliMisc()!=null && arInvoiceLineItem.getIliMisc()!="" && arInvoiceLineItem.getIliMisc().length()!=0){
        				   int qty2Prpgt = Integer.parseInt(this.getQuantityExpiryDates(arInvoiceLineItem.getIliMisc()));

        				   String miscList2Prpgt = this.propagateExpiryDates(arInvoiceLineItem.getIliMisc(), qty2Prpgt, "False");
        				   ArrayList miscList = this.expiryDates(arInvoiceLineItem.getIliMisc(), qty2Prpgt);
        				   String propagateMiscPrpgt = "";
        				   String ret = "";

        				   System.out.println("CST_ST_QTY Before Trans: " + CST_QTY_SLD);
        				   //ArrayList miscList2 = null;
        				   if(CST_QTY_SLD<0){
        					   prevExpiryDates = prevExpiryDates.substring(1);
        					   propagateMiscPrpgt = miscList2Prpgt + prevExpiryDates;
        				   }else{
        					   Iterator mi = miscList.iterator();
        					   propagateMiscPrpgt = prevExpiryDates;
        					   ret = propagateMiscPrpgt;
        					   String Checker = "";
        					   while(mi.hasNext()){
        						   String miscStr = (String)mi.next();

        						   Integer qTest = this.checkExpiryDates(ret+"fin$");
        						   ArrayList miscList2 = this.expiryDates("$"+ret, Double.parseDouble(qTest.toString()));
        						   Iterator m2 = miscList2.iterator();
        						   ret = "";
        						   String ret2 = "false";
        						   int a = 0;

        						   while(m2.hasNext()){
        							   String miscStr2 = (String)m2.next();

        							   if(ret2=="1st"){
        								   ret2 = "false";
        							   }

        							   System.out.println("miscStr2: " + miscStr2);
        							   System.out.println("miscStr: " + miscStr);
        							   if(miscStr2.trim().equals(miscStr.trim())){
        								   if(a==0){
        									   a = 1;
        									   ret2 = "1st";
        									   Checker = "true";
        								   }else{
        									   a = a+1;
        									   ret2 = "true";
        								   }
        							   }

        							   if(!miscStr2.trim().equals(miscStr.trim()) || a>1){
        								   if((ret2!="1st")||(ret2=="false")||(ret2=="true")){
        									   if (miscStr2!=""){
        										   miscStr2 = "$" + miscStr2.trim();
        										   ret = ret + miscStr2;
        										   qtyPrpgt2++;
        										   ret2 = "false";
        									   }
        								   }
        							   }

        						   }
        						   ret = ret + "$";
        						   System.out.println("qtyPrpgt: " + qtyPrpgt);
        						   if(qtyPrpgt2==0){
        							   qtyPrpgt2 = qtyPrpgt;
        						   }
        						   qtyPrpgt= qtyPrpgt -1;
        					   }
        					   //propagateMiscPrpgt = propagateMiscPrpgt.replace(" ", "$");
        					   propagateMiscPrpgt = ret;
        					   System.out.println("propagateMiscPrpgt: " + propagateMiscPrpgt);
        					   //propagateMiscPrpgt = this.propagateExpiryDates(propagateMiscPrpgt, a, "True");
        					   if(Checker.equals("true")){
        						   //invCosting.setCstExpiryDate(ret);
        						   System.out.println("check: " + check);
        					   }else{  	  					   
        						   System.out.println("exA");
        						   throw new GlobalExpiryDateNotFoundException(arInvoiceLineItem.getInvItemLocation().getInvItem().getIiName());

        					   }

        				   }
        				   invCosting.setCstExpiryDate(propagateMiscPrpgt);

        			   }else{
        				   invCosting.setCstExpiryDate(prevExpiryDates);
        			   }
        		   }else{
        			   if(arInvoiceLineItem.getIliMisc()!=null && arInvoiceLineItem.getIliMisc()!="" && arInvoiceLineItem.getIliMisc().length()!=0){
        				   int initialQty = Integer.parseInt(this.getQuantityExpiryDates(arInvoiceLineItem.getIliMisc()));
        				   String initialPrpgt = this.propagateExpiryDates(arInvoiceLineItem.getIliMisc(), initialQty, "False");

        				   invCosting.setCstExpiryDate(initialPrpgt);
        			   }else{
        				   invCosting.setCstExpiryDate(prevExpiryDates);
        				   System.out.println("prevExpiryDates");
        			   }

        		   }

        	   }else{
        		   if(prevExpiryDates!=""){
        			   System.out.println("apPurchaseOrderLine.getVliMisc(): "+arInvoiceLineItem.getIliMisc().length());
        			   if(arInvoiceLineItem.getIliMisc()!=null && arInvoiceLineItem.getIliMisc()!="" && arInvoiceLineItem.getIliMisc().length()!=0){
        				   int qty2Prpgt = Integer.parseInt(this.getQuantityExpiryDates(arInvoiceLineItem.getIliMisc()));
        				   String miscList2Prpgt = this.propagateExpiryDates(arInvoiceLineItem.getIliMisc(), qty2Prpgt, "False");
        				   prevExpiryDates = prevExpiryDates.substring(1);
        				   String propagateMiscPrpgt = miscList2Prpgt + prevExpiryDates;

        				   invCosting.setCstExpiryDate(propagateMiscPrpgt);
        			   }else{
        				   invCosting.setCstExpiryDate(prevExpiryDates);
        			   }


        		   }else{
        			   if(arInvoiceLineItem.getIliMisc()!=null && arInvoiceLineItem.getIliMisc()!="" && arInvoiceLineItem.getIliMisc().length()!=0){
        				   int initialQty = Integer.parseInt(this.getQuantityExpiryDates(arInvoiceLineItem.getIliMisc()));
        				   String initialPrpgt = this.propagateExpiryDates(arInvoiceLineItem.getIliMisc(), initialQty, "False");

        				   invCosting.setCstExpiryDate(initialPrpgt);
        			   }else{
        				   invCosting.setCstExpiryDate(prevExpiryDates);
        			   }

        		   }
        	   }
           }
           

			// if cost variance is not 0, generate cost variance for the transaction 
			/*if(CST_VRNC_VL != 0 && adPreference.getPrfArAllowPriorDate() == EJBCommon.FALSE) {
				
				this.generateCostVariance(invCosting.getInvItemLocation(), CST_VRNC_VL,
						"ARCM" + arInvoiceLineItem.getArInvoice().getInvNumber(),
						arInvoiceLineItem.getArInvoice().getInvDescription(),
						arInvoiceLineItem.getArInvoice().getInvDate(), USR_NM, AD_BRNCH, AD_CMPNY);
				Debug.print("ArInvoicePostControllerBean postToInv D");
			}*/
           
           // propagate balance if necessary           
           Collection invCostings = invCostingHome.findByGreaterThanCstDateAndIiNameAndLocName(CST_DT, invItemLocation.getInvItem().getIiName(), invItemLocation.getInvLocation().getLocName(), AD_BRNCH, AD_CMPNY);
           
           Iterator i = invCostings.iterator();

           String miscList ="";
           ArrayList miscList2 = null;
           
           
           System.out.println("miscList Propagate:" + miscList);
           String propagateMisc ="";
		   	String ret = "";
		   	
           System.out.println("CST_ST_QTY: " + CST_QTY_SLD);
           
           while (i.hasNext()) {
        	   String Checker = "";
        	   String Checker2 = "";
               LocalInvCosting invPropagatedCosting = (LocalInvCosting)i.next();
               
               if(arInvoiceLineItem.getInvItemLocation().getInvItem().getIiTraceMisc()!=0){
            	   if(arInvoiceLineItem.getIliMisc()!=null && arInvoiceLineItem.getIliMisc()!="" && arInvoiceLineItem.getIliMisc().length()!=0){

                	   double qty = Double.parseDouble(this.getQuantityExpiryDates(arInvoiceLineItem.getIliMisc()));
                	   //double qty2 = this.checkExpiryDates2(arInvoiceLineItem.getIliMisc());
                	   miscList = this.propagateExpiryDates(arInvoiceLineItem.getIliMisc(), qty, "False");
                	   miscList2 = this.expiryDates(arInvoiceLineItem.getIliMisc(), qty);
                	   
                	   System.out.println("invAdjustmentLine.getAlMisc(): "+arInvoiceLineItem.getIliMisc());
         	           System.out.println("getAlAdjustQuantity(): "+arInvoiceLineItem.getIliQuantity());

         	           if(arInvoiceLineItem.getIliQuantity()<0){
         	        	   Iterator mi = miscList2.iterator();

         	        	   propagateMisc = invPropagatedCosting.getCstExpiryDate();
         	        	   ret = invPropagatedCosting.getCstExpiryDate();
         	        	   while(mi.hasNext()){
         	        		   String miscStr = (String)mi.next();

         	        		   Integer qTest = this.checkExpiryDates(ret+"fin$");
         	        		   ArrayList miscList3 = this.expiryDates("$" + ret, Double.parseDouble(qTest.toString()));

         	        		   // ArrayList miscList3 = this.expiryDates("$" + ret, qtyPrpgt);
         	        		   System.out.println("ret: " + ret);
         	        		   Iterator m2 = miscList3.iterator();
         	        		   ret = "";
         	        		   String ret2 = "false";
         	        		   int a = 0;
         	        		   while(m2.hasNext()){
         	        			   String miscStr2 = (String)m2.next();

         	        			   if(ret2=="1st"){
         	        				   ret2 = "false";
         	        			   }
         	        			   System.out.println("2 miscStr: "+miscStr);
         	        			   System.out.println("2 miscStr2: "+miscStr2);
         	        			   if(miscStr2.equals(miscStr)){
         	        				   if(a==0){
         	        					   a = 1;
         	        					   ret2 = "1st";
         	        					   Checker2 = "true";
         	        				   }else{
         	        					   a = a+1;
         	        					   ret2 = "true";
         	        				   }
         	        			   }
         	        			   System.out.println("Checker: "+Checker2);
         	        			   if(!miscStr2.equals(miscStr) || a>1){
         	        				   if((ret2!="1st")||(ret2=="false")||(ret2=="true")){
         	        					   if (miscStr2!=""){
         	        						   miscStr2 = "$" + miscStr2;
         	        						   ret = ret + miscStr2;
         	        						   ret2 = "false";
         	        					   }
         	        				   }
         	        			   }

         	        		   }
         	        		   if(Checker2!="true"){
         	        			   throw new GlobalExpiryDateNotFoundException(arInvoiceLineItem.getInvItemLocation().getInvItem().getIiName());
         	        		   }else{
         	        			   System.out.println("TAE");
         	        		   }

         	        		   ret = ret + "$";
         	        		   qtyPrpgt= qtyPrpgt -1;
         	        	   }
         	           }
                   }
               }
               
               
               invPropagatedCosting.setCstRemainingQuantity(invPropagatedCosting.getCstRemainingQuantity() - CST_QTY_SLD);
               invPropagatedCosting.setCstRemainingValue(invPropagatedCosting.getCstRemainingValue() - CST_CST_OF_SLS);
               
               if(arInvoiceLineItem.getInvItemLocation().getInvItem().getIiTraceMisc()!=0){
            	   if (arInvoiceLineItem.getArInvoice().getInvCreditMemo() == EJBCommon.FALSE) {
            		   if(arInvoiceLineItem.getIliMisc()!=null && arInvoiceLineItem.getIliMisc()!="" && arInvoiceLineItem.getIliMisc().length()!=0){
            			   if(CST_QTY_SLD<0){            
            				   //miscList = miscList.substring(1);
            				   //propagateMisc = invPropagatedCosting.getCstExpiryDate() + miscList;
            				   propagateMisc = miscList+invPropagatedCosting.getCstExpiryDate().substring(1, invPropagatedCosting.getCstExpiryDate().length());
            				   System.out.println("propagateMiscPrpgt : "+propagateMisc);

            			   }else{
            				   Iterator mi = miscList2.iterator();

            				   propagateMisc = prevExpiryDates;
            				   ret = propagateMisc;
            				   while(mi.hasNext()){
            					   String miscStr = (String)mi.next();
            					   System.out.println("ret123: " + ret);
            					   System.out.println("qtyPrpgt123: " + qtyPrpgt);
            					   System.out.println("qtyPrpgt2: "+qtyPrpgt2);
            					   if(qtyPrpgt<=0){
            						   qtyPrpgt = qtyPrpgt2;
            					   }

            					   Integer qTest = this.checkExpiryDates(ret+"fin$");
            					   ArrayList miscList3 = this.expiryDates("$" + ret, Double.parseDouble(qTest.toString()));
            					   Iterator m2 = miscList3.iterator();
            					   ret = "";
            					   String ret2 = "false";
            					   int a = 0;
            					   while(m2.hasNext()){
            						   String miscStr2 = (String)m2.next();

            						   if(ret2=="1st"){
            							   ret2 = "false";
            						   }

            						   System.out.println("miscStr2: " + miscStr2);
            						   System.out.println("miscStr: " + miscStr);
            						   if(miscStr2.trim().equals(miscStr.trim())){
            							   if(a==0){
            								   a = 1;
            								   ret2 = "1st";
            								   Checker = "true";
            							   }else{
            								   a = a+1;
            								   ret2 = "true";
            							   }
            						   }

            						   if(!miscStr2.trim().equals(miscStr.trim()) || a>1){
            							   if((ret2!="1st")||(ret2=="false")||(ret2=="true")){
            								   if (miscStr2!=""){
            									   miscStr2 = "$" + miscStr2.trim();
            									   ret = ret + miscStr2;
            									   ret2 = "false";
            								   }
            							   }
            						   }

            					   }
            					   ret = ret + "$";
            					   qtyPrpgt= qtyPrpgt -1;
            				   }
            				   propagateMisc = ret;
            				   System.out.println("propagateMiscPrpgt: " + propagateMisc);

            				   if(Checker=="true"){
            					   //invPropagatedCosting.setCstExpiryDate(propagateMisc);
            					   System.out.println("Yes");
            				   }else{
            					   System.out.println("ex1");
            					   //throw new GlobalExpiryDateNotFoundException(invBranchStockTransferLine.getInvItemLocation().getInvItem().getIiName());                			

            				   }
            			   }

            			   invPropagatedCosting.setCstExpiryDate(propagateMisc);
            		   }else{
            			   invPropagatedCosting.setCstExpiryDate(prevExpiryDates);
            		   }

            	   }else{

            	   }
               }
               
           }                           
           Debug.print("ArInvoicePostControllerBean postToInv E");
           // regenerate cost varaince
           /*if (adPreference.getPrfArAllowPriorDate() == EJBCommon.FALSE) {
        	   
        	   this.regenerateCostVariance(invCostings, invCosting, AD_BRNCH, AD_CMPNY);
        	   
           }
           

        } catch (AdPRFCoaGlVarianceAccountNotFoundException ex) {
        	
        	getSessionContext().setRollbackOnly();
        	throw ex;*/
        	
        } catch(GlobalExpiryDateNotFoundException ex){
        	
        	getSessionContext().setRollbackOnly();
        	throw ex;
        	
        }catch (Exception ex) {
      	
      	   Debug.printStackTrace(ex);
      	   getSessionContext().setRollbackOnly();
           throw new EJBException(ex.getMessage());
         
        }
        	    
    }
    
   public static int checkExpiryDates(String misc) throws Exception {

		String separator ="$";

		// Remove first $ character
		misc = misc.substring(1);
		//System.out.println("misc: " + misc);
		// Counter
		int start = 0;
		int nextIndex = misc.indexOf(separator, start);
		int length = nextIndex - start;	
		int numberExpry=0;
		String miscList = new String();
		String miscList2 = "";
		String g= "";
		try{
			while(g!="fin"){
				// Date
				start = nextIndex + 1;
				nextIndex = misc.indexOf(separator, start);
				length = nextIndex - start;
				g= misc.substring(start, start + length);
				if(g.length()!=0){
					if(g!=null || g!="" || g!="null"){
						if(g.contains("null")){
							miscList2 = "Error";
						}else{
							miscList = miscList + "$" + g;
							numberExpry++;
						}
					}else{
						miscList2 = "Error";
					}

				}else{
					miscList2 = "Error";
				}
			}	
		}catch(Exception e){

		}

		if(miscList2==""){
			miscList = miscList+"$";
		}else{
			miscList = miscList2;
		}

		return (numberExpry);
	}
   
   public String getQuantityExpiryDates(String qntty){
   	String separator = "$";
	String y="";
	try{
//		Remove first $ character
		qntty = qntty.substring(1);

		// Counter
		int start = 0;
		int nextIndex = qntty.indexOf(separator, start);
		int length = nextIndex - start;	

		y = (qntty.substring(start, start + length));
		System.out.println("Y " + y);
	}catch(Exception e){
		y="0";
	}


	return y;
}

   private ArrayList expiryDates(String misc, double qty) throws Exception{
   	Debug.print("ApReceivingItemControllerBean getExpiryDates");
	ArrayList miscList = new ArrayList();
	try{
		System.out.println("misc: " + misc);
		String separator ="$";


		// Remove first $ character
		misc = misc.substring(1);

		// Counter
		int start = 0;
		int nextIndex = misc.indexOf(separator, start);
		int length = nextIndex - start;	

		System.out.println("qty" + qty);
		

		for(int x=0; x<qty; x++) {

			// Date
			start = nextIndex + 1;
			nextIndex = misc.indexOf(separator, start);
			length = nextIndex - start;
			System.out.println("x"+x);
			String checker = misc.substring(start, start + length);
			System.out.println("checker"+checker);
			if(checker.length()!=0 || checker!="null"){
				miscList.add(checker);
			}else{
				miscList.add("null");
			}
		}	
	}catch(Exception e){

		//miscList = "";
	}

	

	System.out.println("miscList :" + miscList);
	return miscList;
}
   
   public String propagateExpiryDates(String misc, double qty, String reverse) throws Exception {
	   //ActionErrors errors = new ActionErrors();
	   Debug.print("ApReceivingItemControllerBean getExpiryDates");
	   System.out.println("misc: " + misc);
	   String miscList = new String();
	   try{
		   String separator = "";
		   if(reverse=="False"){
			   separator ="$";
		   }else{
			   separator =" ";
		   }

		   // Remove first $ character
		   misc = misc.substring(1);
		   System.out.println("misc: " + misc);
		   // Counter
		   int start = 0;
		   int nextIndex = misc.indexOf(separator, start);
		   int length = nextIndex - start;	



		   for(int x=0; x<qty; x++) {

			   // Date
			   start = nextIndex + 1;
			   nextIndex = misc.indexOf(separator, start);
			   length = nextIndex - start;
			   String g= misc.substring(start, start + length);
			   System.out.println("g: " + g);
			   System.out.println("g length: " + g.length());
			   if(g.length()!=0){
				   miscList = miscList + "$" + g;	
				   System.out.println("miscList G: " + miscList);
			   }
		   }	

		   miscList = miscList+"$";
	   }catch(Exception e){
		   miscList="";
	   }

	   System.out.println("miscList :" + miscList);
	   return (miscList);
   }
   
   
   
    private void postToBua(LocalArInvoiceLineItem arInvoiceLineItem, Date CST_DT, double CST_ASSMBLY_QTY, double CST_ASSMBLY_CST,
    		double CST_RMNNG_QTY, double CST_RMNNG_VL, String II_NM, String LOC_NM, double CST_VRNC_VL, String USR_NM, 
			Integer AD_BRNCH, Integer AD_CMPNY) throws
    		AdPRFCoaGlVarianceAccountNotFoundException {
    
    	Debug.print("ArInvoicePostControllerBean postToBua");Debug.print("ArInvoicePostControllerBean postToBua");
    	
    	LocalInvCostingHome invCostingHome = null;
    	LocalAdPreferenceHome adPreferenceHome = null;
    	LocalAdCompanyHome adCompanyHome = null;
    	LocalInvItemLocationHome invItemLocationHome = null;
    	LocalInvAdjustmentLineHome invAdjustmentLineHome = null;
    	
        // Initialize EJB Home
        
        try {
            
          invCostingHome = (LocalInvCostingHome)EJBHomeFactory.
              lookUpLocalHome(LocalInvCostingHome.JNDI_NAME, LocalInvCostingHome.class);
          adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
              lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);
          adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
              lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
          invItemLocationHome = (LocalInvItemLocationHome)EJBHomeFactory.
              lookUpLocalHome(LocalInvItemLocationHome.JNDI_NAME, LocalInvItemLocationHome.class);              
          invAdjustmentLineHome = (LocalInvAdjustmentLineHome)EJBHomeFactory.
		  	lookUpLocalHome(LocalInvAdjustmentLineHome.JNDI_NAME, LocalInvAdjustmentLineHome.class);              
            
        } catch (NamingException ex) {
            
          throw new EJBException(ex.getMessage());
            
        }
      
        try {
        
           LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);
           LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
           LocalInvItemLocation invItemLocation = invItemLocationHome.findByLocNameAndIiName(LOC_NM, II_NM, AD_CMPNY);
           int CST_LN_NMBR = 0;
           Debug.print("ArInvoicePostControllerBean postToBua A");
           CST_ASSMBLY_QTY = EJBCommon.roundIt(CST_ASSMBLY_QTY, adPreference.getPrfInvQuantityPrecisionUnit());
           CST_ASSMBLY_CST = EJBCommon.roundIt(CST_ASSMBLY_CST, adCompany.getGlFunctionalCurrency().getFcPrecision());
           CST_RMNNG_QTY = EJBCommon.roundIt(CST_RMNNG_QTY, adPreference.getPrfInvQuantityPrecisionUnit());
           CST_RMNNG_VL = EJBCommon.roundIt(CST_RMNNG_VL, adCompany.getGlFunctionalCurrency().getFcPrecision());
                    
           if ((CST_ASSMBLY_QTY < 0 && arInvoiceLineItem.getIliEnableAutoBuild() == 0) || 
           		(CST_ASSMBLY_QTY < 0 && arInvoiceLineItem.getIliEnableAutoBuild() == 1 && !arInvoiceLineItem.getInvItemLocation().getInvItem().equals(invItemLocation.getInvItem()))) {	
                      
           	   invItemLocation.setIlCommittedQuantity(invItemLocation.getIlCommittedQuantity() - Math.abs(CST_ASSMBLY_QTY));
           
           }
           
           try {
           
           	   // generate line number
           
               LocalInvCosting invCurrentCosting = invCostingHome.getByMaxCstLineNumberAndCstDateToLongAndIiNameAndLocName(CST_DT.getTime(), invItemLocation.getInvItem().getIiName(), invItemLocation.getInvLocation().getLocName(), AD_BRNCH, AD_CMPNY);
               CST_LN_NMBR = invCurrentCosting.getCstLineNumber() + 1;
               Debug.print("ArInvoicePostControllerBean postToBua B");
           } catch (FinderException ex) {
           
           	   CST_LN_NMBR = 1;
           
           }
           
           /*if (adPreference.getPrfArAllowPriorDate() == EJBCommon.FALSE) {
	           //void subsequent cost variance adjustments
	           Collection invAdjustmentLines = invAdjustmentLineHome.findUnvoidAndIsCostVarianceGreaterThanAdjDateAndIlCodeAndBrCode(
	           		CST_DT, invItemLocation.getIlCode(), AD_BRNCH, AD_CMPNY);
	           Iterator i = invAdjustmentLines.iterator();
	           Debug.print("ArInvoicePostControllerBean postToBua C");
	           while (i.hasNext()){
	           	
	           	LocalInvAdjustmentLine invAdjustmentLine = (LocalInvAdjustmentLine) i.next();
	           	this.voidInvAdjustment(invAdjustmentLine.getInvAdjustment(), AD_BRNCH, AD_CMPNY);
	           	
	           }
           }*/
           	
           // create costing
           LocalInvCosting invCosting = invCostingHome.create(CST_DT, CST_DT.getTime(), CST_LN_NMBR, 0d, 0d, CST_ASSMBLY_QTY, CST_ASSMBLY_CST, 0d, 0d, 0d, 0d, CST_RMNNG_QTY, CST_RMNNG_VL, 0d, 0d, CST_ASSMBLY_QTY > 0 ? CST_ASSMBLY_QTY : 0, AD_BRNCH, AD_CMPNY);
           //invItemLocation.addInvCosting(invCosting);
           invCosting.setInvItemLocation(invItemLocation);
           invCosting.setArInvoiceLineItem(arInvoiceLineItem);
           
			// if cost variance is not 0, generate cost variance for the transaction 
			if(CST_VRNC_VL != 0 && adPreference.getPrfArAllowPriorDate() == EJBCommon.FALSE) {
				
				this.generateCostVariance(invCosting.getInvItemLocation(), CST_VRNC_VL,
						"ARCM" + arInvoiceLineItem.getArInvoice().getInvNumber(),
						arInvoiceLineItem.getArInvoice().getInvDescription(),
						arInvoiceLineItem.getArInvoice().getInvDate(), USR_NM, AD_BRNCH, AD_CMPNY);
				Debug.print("ArInvoicePostControllerBean postToBua D");
			}

           // propagate balance if necessary           
                      
           Collection invCostings = invCostingHome.findByGreaterThanCstDateAndIiNameAndLocName(CST_DT, invItemLocation.getInvItem().getIiName(), invItemLocation.getInvLocation().getLocName(), AD_BRNCH, AD_CMPNY);
           Debug.print("ArInvoicePostControllerBean postToBua E");
           Iterator i = invCostings.iterator();
           
           while (i.hasNext()) {
           
               LocalInvCosting invPropagatedCosting = (LocalInvCosting)i.next();
               
               invPropagatedCosting.setCstRemainingQuantity(invPropagatedCosting.getCstRemainingQuantity() + CST_ASSMBLY_QTY);
               invPropagatedCosting.setCstRemainingValue(invPropagatedCosting.getCstRemainingValue() + CST_ASSMBLY_CST);
           
           }                           

           // regenerate cost varaince
           
           /*if (adPreference.getPrfArAllowPriorDate() == EJBCommon.FALSE) {
        	   this.regenerateCostVariance(invCostings, invCosting, AD_BRNCH, AD_CMPNY);
        	   Debug.print("ArInvoicePostControllerBean postToBua F");
           }

        } catch (AdPRFCoaGlVarianceAccountNotFoundException ex) {
        	
        	getSessionContext().setRollbackOnly();
        	throw ex;*/
        	
        } catch (Exception ex) {
      	
      	   Debug.printStackTrace(ex);
      	   getSessionContext().setRollbackOnly();
           throw new EJBException(ex.getMessage());
         
        }
    
        
        
    }

	private void postToInvSo(LocalArSalesOrderInvoiceLine arSalesOrderInvoiceLine, Date CST_DT, double CST_QTY_SLD,
			double CST_CST_OF_SLS, double CST_RMNNG_QTY, double CST_RMNNG_VL, double CST_VRNC_VL, String USR_NM, Integer AD_BRNCH,
			Integer AD_CMPNY) throws 
			AdPRFCoaGlVarianceAccountNotFoundException {
	        
		   	Debug.print("ArInvoiceEntryControllerBean postToInvSo");
		   	
		   	LocalInvCostingHome invCostingHome = null;
		   	LocalAdPreferenceHome adPreferenceHome = null;
		   	LocalAdCompanyHome adCompanyHome = null;
		   	LocalInvAdjustmentLineHome invAdjustmentLineHome = null;
		   	
		       // Initialize EJB Home
		       
		       try {
		           
		         invCostingHome = (LocalInvCostingHome)EJBHomeFactory.
		             lookUpLocalHome(LocalInvCostingHome.JNDI_NAME, LocalInvCostingHome.class);
		         adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
		             lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);
		         adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
		             lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);              
		         invAdjustmentLineHome = (LocalInvAdjustmentLineHome)EJBHomeFactory.
				     lookUpLocalHome(LocalInvAdjustmentLineHome.JNDI_NAME, LocalInvAdjustmentLineHome.class);              
		           
		       } catch (NamingException ex) {
		           
		         throw new EJBException(ex.getMessage());
		           
		       }
		     
		       try {
		       
		          LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);
		          LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
		          LocalArSalesOrderLine arSalesOrderLine = arSalesOrderInvoiceLine.getArSalesOrderLine();
		          LocalInvItemLocation invItemLocation = arSalesOrderLine.getInvItemLocation();
		          int CST_LN_NMBR = 0;
		          
		          CST_QTY_SLD = EJBCommon.roundIt(CST_QTY_SLD, adPreference.getPrfInvQuantityPrecisionUnit());
		          CST_CST_OF_SLS = EJBCommon.roundIt(CST_CST_OF_SLS, adCompany.getGlFunctionalCurrency().getFcPrecision());
		          CST_RMNNG_QTY = EJBCommon.roundIt(CST_RMNNG_QTY, adPreference.getPrfInvQuantityPrecisionUnit());
		          CST_RMNNG_VL = EJBCommon.roundIt(CST_RMNNG_VL, adCompany.getGlFunctionalCurrency().getFcPrecision());
		          
		          try {
		          
		          	   // generate line number
		          
		              LocalInvCosting invCurrentCosting = invCostingHome.getByMaxCstLineNumberAndCstDateToLongAndIiNameAndLocName(CST_DT.getTime(), invItemLocation.getInvItem().getIiName(), invItemLocation.getInvLocation().getLocName(), AD_BRNCH, AD_CMPNY);
		              CST_LN_NMBR = invCurrentCosting.getCstLineNumber() + 1;
		          
		          } catch (FinderException ex) {
		          
		          	   CST_LN_NMBR = 1;
		          
		          }

		          if (adPreference.getPrfArAllowPriorDate() == EJBCommon.FALSE) {
			           //void subsequent cost variance adjustments
			           Collection invAdjustmentLines = invAdjustmentLineHome.findUnvoidAndIsCostVarianceGreaterThanAdjDateAndIlCodeAndBrCode(
			           		CST_DT, invItemLocation.getIlCode(), AD_BRNCH, AD_CMPNY);
			           Iterator i = invAdjustmentLines.iterator();
			           
			           while (i.hasNext()){
			           	
			           	LocalInvAdjustmentLine invAdjustmentLine = (LocalInvAdjustmentLine) i.next();
			           	this.voidInvAdjustment(invAdjustmentLine.getInvAdjustment(), AD_BRNCH, AD_CMPNY);
			           	
			           }
		          }
    	                     
		          // create costing
		          LocalInvCosting invCosting = invCostingHome.create(CST_DT, CST_DT.getTime(), CST_LN_NMBR, 0d, 0d, 0d, 0d, 0d, 0d, CST_QTY_SLD, CST_CST_OF_SLS, CST_RMNNG_QTY, CST_RMNNG_VL, 0d, 0d, 0d, AD_BRNCH, AD_CMPNY);
		          //invItemLocation.addInvCosting(invCosting);
		          invCosting.setInvItemLocation(invItemLocation);
		          invCosting.setArSalesOrderInvoiceLine(arSalesOrderInvoiceLine);
		          
		          // if cost variance is not 0, generate cost variance for the transaction 
		          if(CST_VRNC_VL != 0 && adPreference.getPrfArAllowPriorDate() == EJBCommon.FALSE) {
		          	
		          	this.generateCostVariance(invCosting.getInvItemLocation(), CST_VRNC_VL,
		          			"ARCM" + arSalesOrderInvoiceLine.getArInvoice().getInvNumber(),
		          			arSalesOrderInvoiceLine.getArInvoice().getInvDescription(),
		          			arSalesOrderInvoiceLine.getArInvoice().getInvDate(), USR_NM, AD_BRNCH, AD_CMPNY);
		          	
		          }

		          // propagate balance if necessary           
		          Collection invCostings = invCostingHome.findByGreaterThanCstDateAndIiNameAndLocName(CST_DT, invItemLocation.getInvItem().getIiName(), invItemLocation.getInvLocation().getLocName(), AD_BRNCH, AD_CMPNY);
		          
		          Iterator i = invCostings.iterator();
		          
		          while (i.hasNext()) {
		          
		              LocalInvCosting invPropagatedCosting = (LocalInvCosting)i.next();
		              
		              invPropagatedCosting.setCstRemainingQuantity(invPropagatedCosting.getCstRemainingQuantity() + CST_QTY_SLD);
		              invPropagatedCosting.setCstRemainingValue(invPropagatedCosting.getCstRemainingValue() + CST_CST_OF_SLS);
		          
		          }                           
		          
		           // regenerate cost varaince
		          if (adPreference.getPrfArAllowPriorDate() == EJBCommon.FALSE) {
		        	  this.regenerateCostVariance(invCostings, invCosting, AD_BRNCH, AD_CMPNY);
		          }

		        } catch (AdPRFCoaGlVarianceAccountNotFoundException ex) {
		        	
		        	getSessionContext().setRollbackOnly();
		        	throw ex;
		        	
		       } catch (Exception ex) {
		     	
		     	   Debug.printStackTrace(ex);
		     	   getSessionContext().setRollbackOnly();
		          throw new EJBException(ex.getMessage());
		        
		       }
		       	    
	   }
    
    private double convertByUomFromAndItemAndQuantity(LocalInvUnitOfMeasure invFromUnitOfMeasure, LocalInvItem invItem, double QTY_SLD, Integer AD_CMPNY) {
    	
    	Debug.print("ArInvoicePostControllerBean convertByUomFromAndItemAndQuantity");		        
    	
    	LocalAdPreferenceHome adPreferenceHome = null;
		LocalInvUnitOfMeasureConversionHome invUnitOfMeasureConversionHome = null;
                
        // Initialize EJB Home
        
        try {
            
            adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
                 lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);   
            invUnitOfMeasureConversionHome = (LocalInvUnitOfMeasureConversionHome)EJBHomeFactory.
        		lookUpLocalHome(LocalInvUnitOfMeasureConversionHome.JNDI_NAME, LocalInvUnitOfMeasureConversionHome.class);   
                    
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
                
        try {
        	Debug.print("ArInvoicePostControllerBean convertByUomFromAndItemAndQuantity A");
            LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);               	          	        	
        	  
            LocalInvUnitOfMeasureConversion invUnitOfMeasureConversion = invUnitOfMeasureConversionHome.findUmcByIiNameAndUomName(invItem.getIiName(), invFromUnitOfMeasure.getUomName(), AD_CMPNY);
            LocalInvUnitOfMeasureConversion invDefaultUomConversion = invUnitOfMeasureConversionHome.findUmcByIiNameAndUomName(invItem.getIiName(), invItem.getInvUnitOfMeasure().getUomName(), AD_CMPNY);
            Debug.print("ArInvoicePostControllerBean convertByUomFromAndItemAndQuantity B");
        	return EJBCommon.roundIt(QTY_SLD * invDefaultUomConversion.getUmcConversionFactor() / invUnitOfMeasureConversion.getUmcConversionFactor(), adPreference.getPrfInvQuantityPrecisionUnit());
        	       	        		        		       	
        						    		        		
        } catch (Exception ex) {
        	
        	Debug.printStackTrace(ex);
        	getSessionContext().setRollbackOnly();
        	throw new EJBException(ex.getMessage());
        	
        }
    	
    }
    
    private double convertByUomAndQuantity(LocalInvUnitOfMeasure invFromUnitOfMeasure, LocalInvItem invItem, double ADJST_QTY, Integer AD_CMPNY) {
        
        Debug.print("ArInvoicePostControllerBean convertByUomFromAndUomToAndQuantity");		        
        
        LocalAdPreferenceHome adPreferenceHome = null;
        LocalInvUnitOfMeasureConversionHome invUnitOfMeasureConversionHome = null;
        
        // Initialize EJB Home
        
        try {
            
            adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
            lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);   
            invUnitOfMeasureConversionHome = (LocalInvUnitOfMeasureConversionHome)EJBHomeFactory.
    		lookUpLocalHome(LocalInvUnitOfMeasureConversionHome.JNDI_NAME, LocalInvUnitOfMeasureConversionHome.class);   
       
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {
            
            LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);               	          	        	
            
            LocalInvUnitOfMeasureConversion invUnitOfMeasureConversion = invUnitOfMeasureConversionHome.findUmcByIiNameAndUomName(invItem.getIiName(), invFromUnitOfMeasure.getUomName(), AD_CMPNY);
            LocalInvUnitOfMeasureConversion invDefaultUomConversion = invUnitOfMeasureConversionHome.findUmcByIiNameAndUomName(invItem.getIiName(), invItem.getInvUnitOfMeasure().getUomName(), AD_CMPNY);
            
            return EJBCommon.roundIt(ADJST_QTY * invDefaultUomConversion.getUmcConversionFactor() / invUnitOfMeasureConversion.getUmcConversionFactor(), adPreference.getPrfInvQuantityPrecisionUnit());       	        		        		       	
            
        } catch (Exception ex) {
            
            Debug.printStackTrace(ex);
            getSessionContext().setRollbackOnly();
            throw new EJBException(ex.getMessage());
            
        }
        
    }	    


	private void regenerateInventoryDr(LocalArInvoice arInvoice, Integer AD_BRNCH, Integer AD_CMPNY) 
    throws GlobalInventoryDateException, GlobalBranchAccountNumberInvalidException {
    	
    	Debug.print("ArInvoicePostControllerBean regenerateInventoryDr");		        
    	
    	LocalArDistributionRecordHome arDistributionRecordHome = null;
    	LocalGlChartOfAccountHome glChartOfAccountHome = null;
    	LocalInvCostingHome invCostingHome = null;
    	LocalInvItemHome invItemHome = null;
    	LocalInvItemLocationHome invItemLocationHome = null;
    	LocalAdBranchItemLocationHome adBranchItemLocationHome = null;
    	LocalAdPreferenceHome adPreferenceHome = null;
        
    	// Initialize EJB Home
    	
    	try {
    		
    		arDistributionRecordHome = (LocalArDistributionRecordHome)EJBHomeFactory.
				lookUpLocalHome(LocalArDistributionRecordHome.JNDI_NAME, LocalArDistributionRecordHome.class); 
    		glChartOfAccountHome = (LocalGlChartOfAccountHome)EJBHomeFactory.
				lookUpLocalHome(LocalGlChartOfAccountHome.JNDI_NAME, LocalGlChartOfAccountHome.class); 
    		invCostingHome = (LocalInvCostingHome)EJBHomeFactory.
				lookUpLocalHome(LocalInvCostingHome.JNDI_NAME, LocalInvCostingHome.class); 
    		invItemHome = (LocalInvItemHome)EJBHomeFactory.
				lookUpLocalHome(LocalInvItemHome.JNDI_NAME, LocalInvItemHome.class);  
    		invItemLocationHome = (LocalInvItemLocationHome)EJBHomeFactory.
				lookUpLocalHome(LocalInvItemLocationHome.JNDI_NAME, LocalInvItemLocationHome.class); 
    		adBranchItemLocationHome = (LocalAdBranchItemLocationHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdBranchItemLocationHome.JNDI_NAME, LocalAdBranchItemLocationHome.class); 
    		adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
    		lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);
    		
    	} catch (NamingException ex) {
    		
    		throw new EJBException(ex.getMessage());
    		
    	}
    	
    	try {
    		
    		LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);
    		
    		// regenerate inventory distribution records
    		
    		Collection arDistributionRecords = arDistributionRecordHome.findImportableDrByInvCode(arInvoice.getInvCode(), AD_CMPNY);
    		
    		Iterator i = arDistributionRecords.iterator();
    		
    		while (i.hasNext()) {
    			
    			LocalArDistributionRecord arDistributionRecord = (LocalArDistributionRecord)i.next();
    			
    			if(arDistributionRecord.getDrClass().equals("COGS") || arDistributionRecord.getDrClass().equals("INVENTORY")){
    				
    				i.remove();
    				arDistributionRecord.remove();
    				
    			}
    			
    		}
    		
    		Collection arInvoiceLineItems = arInvoice.getArInvoiceLineItems();
    		Collection arSalesOrderInvoiceLines = arInvoice.getArSalesOrderInvoiceLines();
    		
    		if(arInvoiceLineItems != null && !arInvoiceLineItems.isEmpty()) {
    			
    			i = arInvoiceLineItems.iterator();
    			
    			while(i.hasNext()) {
    				
    				LocalArInvoiceLineItem arInvoiceLineItem = (LocalArInvoiceLineItem)i.next();
    				LocalInvItemLocation invItemLocation = arInvoiceLineItem.getInvItemLocation();
    				
    				// start date validation
    				if (adPreference.getPrfArAllowPriorDate() == EJBCommon.FALSE) {
    					Collection invNegTxnCosting = invCostingHome.findNegTxnByGreaterThanCstDateAndIiNameAndLocName(
    							arInvoice.getInvDate(), invItemLocation.getInvItem().getIiName(),
    							invItemLocation.getInvLocation().getLocName(), AD_BRNCH, AD_CMPNY);
    					if(!invNegTxnCosting.isEmpty())  throw new GlobalInventoryDateException(invItemLocation.getInvItem().getIiName());
    				}
    				
    				// add cost of sales distribution and inventory
    				
    				
    				double COST = arInvoiceLineItem.getInvItemLocation().getInvItem().getIiUnitCost();
    				
    				try {
    					
    					LocalInvCosting invCosting  = invCostingHome.getByMaxCstDateToLongAndMaxCstLineNumberAndLessThanEqualCstDateAndIiNameAndLocName(
    						arInvoice.getInvDate(), invItemLocation.getInvItem().getIiName(), invItemLocation.getInvLocation().getLocName(), AD_BRNCH, AD_CMPNY);
    					
    					
    					if(invCosting.getInvItemLocation().getInvItem().getIiCostMethod().equals("Average"))
 	  	    				
 	  	    				COST = invCosting.getCstRemainingQuantity() <= 0 ? COST :
 	  	    					Math.abs(invCosting.getCstRemainingValue() / invCosting.getCstRemainingQuantity());
 	  	    			
	 	  	    			if(COST<=0){
	                               COST = invItemLocation.getInvItem().getIiUnitCost();
                            }	
 	  	    			else if(invCosting.getInvItemLocation().getInvItem().getIiCostMethod().equals("FIFO"))
 	  	    				
 	  	    				COST =  invCosting.getCstRemainingQuantity() == 0 ? COST :
 	  	    					Math.abs(this.getInvFifoCost(invCosting.getCstDate(), invCosting.getInvItemLocation().getIlCode(),
 	  	    						arInvoiceLineItem.getIliQuantity(), arInvoiceLineItem.getIliUnitPrice(), false, AD_BRNCH, AD_CMPNY));
    					
 	  	    			else if(invCosting.getInvItemLocation().getInvItem().getIiCostMethod().equals("Standard"))
 	  	    				
 	  	    				COST = invCosting.getInvItemLocation().getInvItem().getIiUnitCost();
	
    				} catch (FinderException ex) { }
    				
    				double QTY_SLD = this.convertByUomFromAndItemAndQuantity(arInvoiceLineItem.getInvUnitOfMeasure(), 
    						arInvoiceLineItem.getInvItemLocation().getInvItem(), arInvoiceLineItem.getIliQuantity(), AD_CMPNY);
    				
    				LocalAdBranchItemLocation adBranchItemLocation = null;
    				
    				try {
    					
    					adBranchItemLocation = adBranchItemLocationHome.findBilByIlCodeAndBrCode(arInvoiceLineItem.getInvItemLocation().getIlCode(), AD_BRNCH, AD_CMPNY);
    					
    				} catch(FinderException ex) {
    					
    				}
    				
    				if (arInvoiceLineItem.getIliEnableAutoBuild() == EJBCommon.FALSE) {
    				
    					// Use AdBranchItemLocation Accounts
    					if (adBranchItemLocation != null) {

        					if(arInvoice.getInvCreditMemo() == EJBCommon.FALSE) {
        						
        						this.addArDrIliEntry(arInvoice.getArDrNextLine(), 
        								"COGS", EJBCommon.TRUE, COST * QTY_SLD,
    									adBranchItemLocation.getBilCoaGlCostOfSalesAccount(), arInvoice, AD_BRNCH, AD_CMPNY);
        						
        						this.addArDrIliEntry(arInvoice.getArDrNextLine(), 
        								"INVENTORY", EJBCommon.FALSE, COST * QTY_SLD,
        								adBranchItemLocation.getBilCoaGlInventoryAccount(), arInvoice, AD_BRNCH, AD_CMPNY);
        						
        					} else {
        						
        						this.addArDrIliEntry(arInvoice.getArDrNextLine(), 
        								"COGS", EJBCommon.FALSE, COST * QTY_SLD,
        								adBranchItemLocation.getBilCoaGlCostOfSalesAccount(), arInvoice, AD_BRNCH, AD_CMPNY);
        						
        						this.addArDrIliEntry(arInvoice.getArDrNextLine(), 
        								"INVENTORY", EJBCommon.TRUE, COST * QTY_SLD,
        								adBranchItemLocation.getBilCoaGlInventoryAccount(), arInvoice, AD_BRNCH, AD_CMPNY);
        						
        					}
    						
    					} else {

    						// Use default accounts
        					if(arInvoice.getInvCreditMemo() == EJBCommon.FALSE) {
        						
        						this.addArDrIliEntry(arInvoice.getArDrNextLine(), 
        								"COGS", EJBCommon.TRUE, COST * QTY_SLD,
    									arInvoiceLineItem.getInvItemLocation().getIlGlCoaCostOfSalesAccount(), arInvoice, AD_BRNCH, AD_CMPNY);
        						
        						this.addArDrIliEntry(arInvoice.getArDrNextLine(), 
        								"INVENTORY", EJBCommon.FALSE, COST * QTY_SLD,
    									arInvoiceLineItem.getInvItemLocation().getIlGlCoaInventoryAccount(), arInvoice, AD_BRNCH, AD_CMPNY);
        						
        					} else {
        						
        						this.addArDrIliEntry(arInvoice.getArDrNextLine(), 
        								"COGS", EJBCommon.FALSE, COST * QTY_SLD,
    									arInvoiceLineItem.getInvItemLocation().getIlGlCoaCostOfSalesAccount(), arInvoice, AD_BRNCH, AD_CMPNY);
        						
        						this.addArDrIliEntry(arInvoice.getArDrNextLine(), 
        								"INVENTORY", EJBCommon.TRUE, COST * QTY_SLD,
    									arInvoiceLineItem.getInvItemLocation().getIlGlCoaInventoryAccount(), arInvoice, AD_BRNCH, AD_CMPNY);
        						
        					}
    						
    					}
    					
    				} 
    				
    				if (arInvoiceLineItem.getIliEnableAutoBuild() == EJBCommon.TRUE && arInvoiceLineItem.getInvItemLocation().getInvItem().getIiClass().equals("Assembly")) {
    					
    					byte DEBIT = EJBCommon.TRUE;
    					
    					double TOTAL_AMOUNT = 0;
    					double ABS_TOTAL_AMOUNT = 0;
    					
    					if (adBranchItemLocation != null) {

    						LocalGlChartOfAccount glInventoryChartOfAccount = glChartOfAccountHome.findByPrimaryKey(
    								adBranchItemLocation.getBilCoaGlInventoryAccount());

    					} else {
    						
    						LocalGlChartOfAccount glInventoryChartOfAccount = glChartOfAccountHome.findByPrimaryKey(
    								arInvoiceLineItem.getInvItemLocation().getIlGlCoaInventoryAccount());
    					
    					}
    					
    					Collection invBillOfMaterials = arInvoiceLineItem.getInvItemLocation().getInvItem().getInvBillOfMaterials();
    					
    					Iterator j = invBillOfMaterials.iterator();
    					
    					while (j.hasNext()) {
    						
    						LocalInvBillOfMaterial invBillOfMaterial = (LocalInvBillOfMaterial) j.next();
    						
    						// add bill of material quantity needed to item location
    						
    						LocalInvItemLocation invIlRawMaterial = null;
    						
    						try {
    							
    							invIlRawMaterial = invItemLocationHome.findByLocNameAndIiName(
    									invBillOfMaterial.getBomLocName(), invBillOfMaterial.getBomIiName(), AD_CMPNY);
    							
    						} catch(FinderException ex) {
    							
    							throw new GlobalInvItemLocationNotFoundException(String.valueOf(arInvoiceLineItem.getIliLine()) +
    									" - Raw Mat. (" +invBillOfMaterial.getBomIiName() + ")");
    							
    						}
    						
    						double convertedQuantity = this.convertByUomFromAndItemAndQuantity(
                                    invBillOfMaterial.getInvUnitOfMeasure(), invIlRawMaterial.getInvItem(),
                                    EJBCommon.roundIt(invBillOfMaterial.getBomQuantityNeeded() * QTY_SLD,
                                    this.getInvGpQuantityPrecisionUnit(AD_CMPNY)), AD_CMPNY);

    						// start date validation
    						if (adPreference.getPrfArAllowPriorDate() == EJBCommon.FALSE) {
    							Collection invIlRawMatNegTxnCosting = invCostingHome.findNegTxnByGreaterThanCstDateAndIiNameAndLocName(
    									arInvoice.getInvDate(), invIlRawMaterial.getInvItem().getIiName(),
    									invIlRawMaterial.getInvLocation().getLocName(),	AD_BRNCH, AD_CMPNY);
    							if(!invIlRawMatNegTxnCosting.isEmpty()) throw new GlobalInventoryDateException(
    									invItemLocation.getInvItem().getIiName() + " - Raw Mat. (" + invIlRawMaterial.getInvItem().getIiName() + ")");
    						}
    						
    						LocalInvItem invItem = invItemHome.findByIiName(invBillOfMaterial.getBomIiName(), AD_CMPNY);												
    						
    						double COSTING = 0d;
    						
    						try {
    							
    							LocalInvCosting invCosting  = invCostingHome.getByMaxCstDateToLongAndMaxCstLineNumberAndLessThanEqualCstDateAndIiNameAndLocName(
    									arInvoice.getInvDate(), invItem.getIiName(), invBillOfMaterial.getBomLocName(), AD_BRNCH, AD_CMPNY);		 	  	    		    		 	  	    			 	  	    		    	 	  	    	
    							
    							COSTING = Math.abs(invCosting.getCstRemainingValue() / invCosting.getCstRemainingQuantity());
    							
    						} catch (FinderException ex) {	 
    							
    							COSTING = invItem.getIiUnitCost();	  	        	 	  	        	
    							
    						}
    						
    						// bom conversion
                            COSTING = this.convertCostByUom(invIlRawMaterial.getInvItem().getIiName(),
                                    invBillOfMaterial.getInvUnitOfMeasure().getUomName(), COSTING, true, AD_CMPNY);
                           
                            double BOM_AMOUNT = EJBCommon.roundIt(
    								(QTY_SLD * (invBillOfMaterial.getBomQuantityNeeded() * COSTING)),
									this.getGlFcPrecisionUnit(AD_CMPNY));
    						
    						TOTAL_AMOUNT += BOM_AMOUNT;
    						ABS_TOTAL_AMOUNT += Math.abs(BOM_AMOUNT); 
    						
    						// add inventory account
    						
    						LocalAdBranchItemLocation adBranchItemLocationRM = null;
    						
    						try {
    							
    							adBranchItemLocationRM = adBranchItemLocationHome.findBilByIlCodeAndBrCode(invIlRawMaterial.getIlCode(), AD_BRNCH, AD_CMPNY);
    							
    						} catch(FinderException ex) {
    							
    						}
    		
    						if (adBranchItemLocationRM != null) {
    							
    							// Use AdBranchItemLocation Accounts
    							if(arInvoice.getInvCreditMemo() == EJBCommon.FALSE) {
    								
    								this.addArDrIliEntry(arInvoice.getArDrNextLine(), "INVENTORY", EJBCommon.FALSE, Math.abs(BOM_AMOUNT),
    										adBranchItemLocation.getBilCoaGlInventoryAccount(), arInvoice, AD_BRNCH, AD_CMPNY);
    								
    							} else {
    								
    								this.addArDrIliEntry(arInvoice.getArDrNextLine(), "INVENTORY", EJBCommon.TRUE, Math.abs(BOM_AMOUNT),
    										adBranchItemLocation.getBilCoaGlInventoryAccount(), arInvoice, AD_BRNCH, AD_CMPNY);
    								
    							}

    						} else {
    							
    							// Use default Accounts
    							if(arInvoice.getInvCreditMemo() == EJBCommon.FALSE) {
    								
    								this.addArDrIliEntry(arInvoice.getArDrNextLine(), "INVENTORY", EJBCommon.FALSE, Math.abs(BOM_AMOUNT),
    										invIlRawMaterial.getIlGlCoaInventoryAccount(), arInvoice, AD_BRNCH, AD_CMPNY);
    								
    							} else {
    								
    								this.addArDrIliEntry(arInvoice.getArDrNextLine(), "INVENTORY", EJBCommon.TRUE, Math.abs(BOM_AMOUNT),
    										invIlRawMaterial.getIlGlCoaInventoryAccount(), arInvoice, AD_BRNCH, AD_CMPNY);
    								
    							}
    						
    						}
    						
    					}
    					
    					// add cost of sales account
    					
    					if (adBranchItemLocation != null) {
    						
    						if(arInvoice.getInvCreditMemo() == EJBCommon.FALSE) {
    							
    							this.addArDrIliEntry(arInvoice.getArDrNextLine(), 
    									"COGS", EJBCommon.TRUE, Math.abs(TOTAL_AMOUNT),
										adBranchItemLocation.getBilCoaGlCostOfSalesAccount(), arInvoice, AD_BRNCH, AD_CMPNY);
    							
    						} else {
    							
    							this.addArDrIliEntry(arInvoice.getArDrNextLine(), 
    									"COGS", EJBCommon.FALSE, Math.abs(TOTAL_AMOUNT),
    									adBranchItemLocation.getBilCoaGlCostOfSalesAccount(), arInvoice, AD_BRNCH, AD_CMPNY);
    							
    						}

    					} else {
    						
    						if(arInvoice.getInvCreditMemo() == EJBCommon.FALSE) {
    							
    							this.addArDrIliEntry(arInvoice.getArDrNextLine(), 
    									"COGS", EJBCommon.TRUE, Math.abs(TOTAL_AMOUNT),
										arInvoiceLineItem.getInvItemLocation().getIlGlCoaCostOfSalesAccount(), arInvoice, AD_BRNCH, AD_CMPNY);
    							
    						} else {
    							
    							this.addArDrIliEntry(arInvoice.getArDrNextLine(), 
    									"COGS", EJBCommon.FALSE, Math.abs(TOTAL_AMOUNT),
										arInvoiceLineItem.getInvItemLocation().getIlGlCoaCostOfSalesAccount(), arInvoice, AD_BRNCH, AD_CMPNY);
    							
    						}
    						
    					}
    					
    				}
    				
    			}
    			
    		} else if (arSalesOrderInvoiceLines != null && !arSalesOrderInvoiceLines.isEmpty()) {
    			    			
    			i = arSalesOrderInvoiceLines.iterator();
    			
    			while (i.hasNext()) {
    				
    				LocalArSalesOrderInvoiceLine arSalesOrderInvoiceLine = (LocalArSalesOrderInvoiceLine)i.next();
    				LocalInvItemLocation invItemLocation = arSalesOrderInvoiceLine.getArSalesOrderLine().getInvItemLocation();
    				
    				// start date validation
    				if (adPreference.getPrfArAllowPriorDate() == EJBCommon.FALSE) {
    					Collection invNegTxnCosting = invCostingHome.findNegTxnByGreaterThanCstDateAndIiNameAndLocName(
    							arInvoice.getInvDate(), invItemLocation.getInvItem().getIiName(),
    							invItemLocation.getInvLocation().getLocName(), AD_BRNCH, AD_CMPNY);
    					if(!invNegTxnCosting.isEmpty()) throw new GlobalInventoryDateException(invItemLocation.getInvItem().getIiName());
    				}
	 	  	    	
	 	  	    	// add cost of sales distribution and inventory
    				
    				double COST =  COST = invItemLocation.getInvItem().getIiUnitCost();
    				
    				try {
    					
    					LocalInvCosting invCosting  = invCostingHome.getByMaxCstDateToLongAndMaxCstLineNumberAndLessThanEqualCstDateAndIiNameAndLocName(
    							arInvoice.getInvDate(), invItemLocation.getInvItem().getIiName(), invItemLocation.getInvLocation().getLocName(), AD_BRNCH, AD_CMPNY);
    					
    					if(invCosting.getInvItemLocation().getInvItem().getIiCostMethod().equals("Average"))
 	  	    				
 	  	    			//	COST = Math.abs(invCosting.getCstRemainingValue() / invCosting.getCstRemainingQuantity());
 	  	    			
 	  	    				COST = invCosting.getCstRemainingQuantity() <= 0 ? COST:
                        		Math.abs(invCosting.getCstRemainingValue() / invCosting.getCstRemainingQuantity());

                        	if(COST<=0){
                               COST = invItemLocation.getInvItem().getIiUnitCost();
                            }	
                        	
 	  	    			else if(invCosting.getInvItemLocation().getInvItem().getIiCostMethod().equals("FIFO"))
 	  	    				
 	  	    				COST = Math.abs(this.getInvFifoCost(invCosting.getCstDate(), invCosting.getInvItemLocation().getIlCode(),
 	  	    						arSalesOrderInvoiceLine.getSilQuantityDelivered(), arSalesOrderInvoiceLine.getSilAmount(), false, AD_BRNCH, AD_CMPNY));
    					
 	  	    			else if(invCosting.getInvItemLocation().getInvItem().getIiCostMethod().equals("FIFO"))
 	  	    				
 	  	    				COST = invCosting.getInvItemLocation().getInvItem().getIiUnitCost();
    					
    				} catch (FinderException ex) {
    					
    					COST = arSalesOrderInvoiceLine.getArSalesOrderLine().getInvItemLocation().getInvItem().getIiUnitCost();
    					
    				}
    				
    				double QTY_SLD = this.convertByUomFromAndItemAndQuantity(arSalesOrderInvoiceLine.getArSalesOrderLine().getInvUnitOfMeasure(), 
    						invItemLocation.getInvItem(), arSalesOrderInvoiceLine.getSilQuantityDelivered(), AD_CMPNY);
    				
    				LocalAdBranchItemLocation adBranchItemLocationSO = null;
    				
    				try {
    					
    					adBranchItemLocationSO = adBranchItemLocationHome.findBilByIlCodeAndBrCode(arSalesOrderInvoiceLine.getArSalesOrderLine().getInvItemLocation().getIlCode(), AD_BRNCH, AD_CMPNY);
    					
    				} catch(FinderException ex) {
    					
    				}    				
    				
    				if (adBranchItemLocationSO != null) {
    					
    					// Use AdBranchItemLocation Accounts
    					this.addArDrIliEntry(arInvoice.getArDrNextLine(), 
    							"COGS", EJBCommon.TRUE, COST * QTY_SLD,
								adBranchItemLocationSO.getBilCoaGlCostOfSalesAccount(), arInvoice, AD_BRNCH, AD_CMPNY);
    					
    					this.addArDrIliEntry(arInvoice.getArDrNextLine(), 
    							"INVENTORY", EJBCommon.FALSE, COST * QTY_SLD,
    							adBranchItemLocationSO.getBilCoaGlInventoryAccount(), arInvoice, AD_BRNCH, AD_CMPNY);
    					
    				} else {
    					
    					// Use default Accounts
    					this.addArDrIliEntry(arInvoice.getArDrNextLine(), 
    							"COGS", EJBCommon.TRUE, COST * QTY_SLD,
								arSalesOrderInvoiceLine.getArSalesOrderLine().getInvItemLocation().getIlGlCoaCostOfSalesAccount(), arInvoice, AD_BRNCH, AD_CMPNY);
    					
    					this.addArDrIliEntry(arInvoice.getArDrNextLine(), 
    							"INVENTORY", EJBCommon.FALSE, COST * QTY_SLD,
								arSalesOrderInvoiceLine.getArSalesOrderLine().getInvItemLocation().getIlGlCoaInventoryAccount(), arInvoice, AD_BRNCH, AD_CMPNY);
    					
    				} 
    				// add quantity to item location committed quantity
		 	  	    	
    				double convertedQuantity = this.convertByUomFromAndItemAndQuantity(arSalesOrderInvoiceLine.getArSalesOrderLine().getInvUnitOfMeasure(), arSalesOrderInvoiceLine.getArSalesOrderLine().getInvItemLocation().getInvItem(), arSalesOrderInvoiceLine.getSilQuantityDelivered(), AD_CMPNY); 	  	    		 	  	    		 	  	    
    				invItemLocation.setIlCommittedQuantity(invItemLocation.getIlCommittedQuantity() + convertedQuantity);
    				
    			}
    			
    		}
    		
    	} catch (GlobalInventoryDateException ex) {
    		
    		getSessionContext().setRollbackOnly();
    		throw ex;
    		
    	} catch (GlobalBranchAccountNumberInvalidException ex) {
    		
    		getSessionContext().setRollbackOnly();
    		throw ex;
    		
    	} catch (Exception ex) {
    		
    		Debug.printStackTrace(ex);
    		getSessionContext().setRollbackOnly();
    		throw new EJBException(ex.getMessage());
    		
    	}
    	
    }
    
    private void addArDrIliEntry(short DR_LN, String DR_CLSS, byte DR_DBT, double DR_AMNT, Integer COA_CODE, LocalArInvoice arInvoice, Integer AD_BRNCH, Integer AD_CMPNY) 
    	throws GlobalBranchAccountNumberInvalidException {
    	
    	Debug.print("ArInvoicePostControllerBean addArDrIliEntry");
    	
    	LocalArDistributionRecordHome arDistributionRecordHome = null;        
    	LocalGlChartOfAccountHome glChartOfAccountHome = null;
    	LocalAdCompanyHome adCompanyHome = null;
    	
    	
    	// Initialize EJB Home
    	
    	try {
    		
    		arDistributionRecordHome = (LocalArDistributionRecordHome)EJBHomeFactory.
			lookUpLocalHome(LocalArDistributionRecordHome.JNDI_NAME, LocalArDistributionRecordHome.class);            
    		glChartOfAccountHome = (LocalGlChartOfAccountHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlChartOfAccountHome.JNDI_NAME, LocalGlChartOfAccountHome.class);            
    		adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);            
    		
    	} catch (NamingException ex) {
    		
    		throw new EJBException(ex.getMessage());
    		
    	}            
    	
    	try {
    		
    		// get company
    		
    		LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
    		
    		LocalGlChartOfAccount glChartOfAccount = glChartOfAccountHome.findByCoaCodeAndBranchCode(COA_CODE, AD_BRNCH, AD_CMPNY);

    		// create distribution record 
    		
    		LocalArDistributionRecord arDistributionRecord = arDistributionRecordHome.create(
    				DR_LN, DR_CLSS, DR_DBT, EJBCommon.roundIt(DR_AMNT, adCompany.getGlFunctionalCurrency().getFcPrecision()),
					EJBCommon.FALSE, EJBCommon.FALSE, AD_CMPNY);
    		
    		//arInvoice.addArDistributionRecord(arDistributionRecord);
    		arDistributionRecord.setArInvoice(arInvoice);
    		//glChartOfAccount.addArDistributionRecord(arDistributionRecord);
    		arDistributionRecord.setGlChartOfAccount(glChartOfAccount);
    		
    	} catch (FinderException ex) {
			
			throw new GlobalBranchAccountNumberInvalidException(ex.getMessage());
			
		} catch (Exception ex) {
    		
    		Debug.printStackTrace(ex);
    		getSessionContext().setRollbackOnly();
    		throw new EJBException(ex.getMessage());
    		
    	}
    	
    }
    
    private short getInvGpQuantityPrecisionUnit(Integer AD_CMPNY) {
    	
    	Debug.print("ArInvoicePostControllerBean getInvGpQuantityPrecisionUnit");
    	
    	LocalAdPreferenceHome adPreferenceHome = null;         
    	
    	// Initialize EJB Home
    	
    	try {
    		
    		adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);       
    		
    	} catch (NamingException ex) {
    		
    		throw new EJBException(ex.getMessage());
    		
    	}
    	
    	
    	try {
    		
    		LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);
    		
    		return adPreference.getPrfInvQuantityPrecisionUnit();
    		
    	} catch (Exception ex) {
    		
    		Debug.printStackTrace(ex);
    		throw new EJBException(ex.getMessage());
    		
    	}
    	
    }
    
    private double convertCostByUom(String II_NM, String UOM_NM, double unitCost, boolean isFromDefault, Integer AD_CMPNY) {
        
        Debug.print("ArInvoiceEntryControllerBean convertCostByUom");		        
        
        LocalInvItemHome invItemHome = null;
        LocalInvUnitOfMeasureConversionHome invUnitOfMeasureConversionHome = null;
        
        // Initialize EJB Home
        
        try {
            
            invUnitOfMeasureConversionHome = (LocalInvUnitOfMeasureConversionHome)EJBHomeFactory.
            lookUpLocalHome(LocalInvUnitOfMeasureConversionHome.JNDI_NAME, LocalInvUnitOfMeasureConversionHome.class);   
            invItemHome = (LocalInvItemHome)EJBHomeFactory.
            lookUpLocalHome(LocalInvItemHome.JNDI_NAME, LocalInvItemHome.class);
            
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {
            
            LocalInvItem invItem = invItemHome.findByIiName(II_NM, AD_CMPNY);
            LocalInvUnitOfMeasureConversion invUnitOfMeasureConversion = invUnitOfMeasureConversionHome.findUmcByIiNameAndUomName(II_NM, UOM_NM, AD_CMPNY);
            LocalInvUnitOfMeasureConversion invDefaultUomConversion = invUnitOfMeasureConversionHome.findUmcByIiNameAndUomName(II_NM, invItem.getInvUnitOfMeasure().getUomName(), AD_CMPNY);
            
            if (isFromDefault) {	        	
                
                return unitCost * invDefaultUomConversion.getUmcConversionFactor() / invUnitOfMeasureConversion.getUmcConversionFactor();
                
            } else {
                
                return unitCost * invUnitOfMeasureConversion.getUmcConversionFactor() / invDefaultUomConversion.getUmcConversionFactor();
                
            }
            
        } catch (Exception ex) {
            
            Debug.printStackTrace(ex);
            getSessionContext().setRollbackOnly();
            throw new EJBException(ex.getMessage());
            
        }
        
    }
	
    private double getFrRateByFrNameAndFrDate(String FC_NM, Date CONVERSION_DATE, Integer AD_CMPNY) 
    throws GlobalConversionDateNotExistException {
    	
    	Debug.print("ArInvoicePostControllerBean getFrRateByFrNameAndFrDate");
    	
    	LocalGlFunctionalCurrencyRateHome glFunctionalCurrencyRateHome = null; 
    	LocalGlFunctionalCurrencyHome glFunctionalCurrencyHome = null;
    	LocalAdCompanyHome adCompanyHome = null;
    	
    	// Initialize EJB Home
    	
    	try {
    		
    		glFunctionalCurrencyRateHome = (LocalGlFunctionalCurrencyRateHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlFunctionalCurrencyRateHome.JNDI_NAME, LocalGlFunctionalCurrencyRateHome.class);
    		glFunctionalCurrencyHome = (LocalGlFunctionalCurrencyHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlFunctionalCurrencyHome.JNDI_NAME, LocalGlFunctionalCurrencyHome.class);
    		adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
    		
    	} catch (NamingException ex) {
    		
    		throw new EJBException(ex.getMessage());
    		
    	}
    	
    	try {
    		
    		LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
    		LocalGlFunctionalCurrency glFunctionalCurrency = glFunctionalCurrencyHome.findByFcName(FC_NM, AD_CMPNY);
    		
    		double CONVERSION_RATE = 1;
    		
    		// Get functional currency rate
    		
    		if (!FC_NM.equals("USD")) {
    			
    			LocalGlFunctionalCurrencyRate glFunctionalCurrencyRate =
    				glFunctionalCurrencyRateHome.findByFcCodeAndDate(glFunctionalCurrency.getFcCode(),
    						CONVERSION_DATE, AD_CMPNY);
    			
    			CONVERSION_RATE = glFunctionalCurrencyRate.getFrXToUsd();
    			
    		}
    		
    		// Get set of book functional currency rate if necessary
    		
    		if (!adCompany.getGlFunctionalCurrency().getFcName().equals("USD")) {
    			
    			LocalGlFunctionalCurrencyRate glCompanyFunctionalCurrencyRate =
    				glFunctionalCurrencyRateHome.findByFcCodeAndDate(adCompany.getGlFunctionalCurrency(). getFcCode(),
    						CONVERSION_DATE, AD_CMPNY);
    			
    			CONVERSION_RATE = CONVERSION_RATE / glCompanyFunctionalCurrencyRate.getFrXToUsd();
    			
    		}
    		
    		return CONVERSION_RATE;
    		
    	} catch (FinderException ex) {	
    		
    		getSessionContext().setRollbackOnly();
    		throw new GlobalConversionDateNotExistException();  
    		
    	} catch (Exception ex) {
    		
    		Debug.printStackTrace(ex);
    		throw new EJBException(ex.getMessage());
    		
    	}
    	
    }
    
    private void voidInvAdjustment(LocalInvAdjustment invAdjustment, Integer AD_BRNCH, Integer AD_CMPNY){
    	
    	Debug.print("ArInvoicePostControllerBean voidInvAdjustment");
    	
    	try{

    			Collection invDistributionRecords = invAdjustment.getInvDistributionRecords();
    			ArrayList list = new ArrayList();
    			
    			Iterator i = invDistributionRecords.iterator();
    			
    			while (i.hasNext()) {
    				
    				LocalInvDistributionRecord invDistributionRecord = (LocalInvDistributionRecord)i.next();
    				
    				list.add(invDistributionRecord);
    				
    			}
    			
    			i = list.iterator();
    			
    			while (i.hasNext()) {
    				
    				LocalInvDistributionRecord invDistributionRecord = (LocalInvDistributionRecord)i.next();    	    			    			
    				
    				this.addInvDrEntry(invAdjustment.getInvDrNextLine(), invDistributionRecord.getDrClass(), 
    						invDistributionRecord.getDrDebit() == EJBCommon.TRUE ? EJBCommon.FALSE : EJBCommon.TRUE,
    								invDistributionRecord.getDrAmount(), EJBCommon.TRUE,
									invDistributionRecord.getInvChartOfAccount().getCoaCode(), invAdjustment, AD_BRNCH, AD_CMPNY);
    				
    			}	    
    			

    			Collection invAdjustmentLines = invAdjustment.getInvAdjustmentLines();
    			i = invAdjustmentLines.iterator();
    			list.clear();

    			while (i.hasNext()) {
    				
    				LocalInvAdjustmentLine invAdjustmentLine = (LocalInvAdjustmentLine)i.next();
    				
    				list.add(invAdjustmentLine);
    				
    			}
    			
    			i = list.iterator();
    			
    			while (i.hasNext()) {
    				
    				LocalInvAdjustmentLine invAdjustmentLine = (LocalInvAdjustmentLine)i.next();    	    			    			
    				
    				this.addInvAlEntry(invAdjustmentLine.getInvItemLocation(),
    						invAdjustment, (invAdjustmentLine.getAlUnitCost()) * - 1, EJBCommon.TRUE, AD_CMPNY);

    			}
    			invAdjustment.setAdjVoid(EJBCommon.TRUE);
    			
    			this.executeInvAdjPost(invAdjustment.getAdjCode(), invAdjustment.getAdjLastModifiedBy(), AD_BRNCH, AD_CMPNY);

    	} catch (Exception ex) {
    		
    		Debug.printStackTrace(ex);
    		getSessionContext().setRollbackOnly();
    		throw new EJBException(ex.getMessage());
    		
    	}
    	
    }
    
    private void generateCostVariance(LocalInvItemLocation invItemLocation, double CST_VRNC_VL, String ADJ_RFRNC_NMBR,
    		String ADJ_DSCRPTN, Date ADJ_DT, String USR_NM, Integer AD_BRNCH, Integer AD_CMPNY) throws
			AdPRFCoaGlVarianceAccountNotFoundException {
    	/*
    	Debug.print("ArInvoicePostControllerBean generateCostVariance");
    	
    	LocalAdPreferenceHome adPreferenceHome = null;
    	LocalGlChartOfAccountHome glChartOfAccountHome = null;
    	LocalAdBranchItemLocationHome adBranchItemLocationHome = null;
    	
    	// Initialize EJB Home
    	
    	try {
    		
    		adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);       
    		glChartOfAccountHome = (LocalGlChartOfAccountHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlChartOfAccountHome.JNDI_NAME, LocalGlChartOfAccountHome.class);
    		adBranchItemLocationHome = (LocalAdBranchItemLocationHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdBranchItemLocationHome.JNDI_NAME, LocalAdBranchItemLocationHome.class);       
    		
    		
    	} catch (NamingException ex) {
    		
    		throw new EJBException(ex.getMessage());
    		
    	}
    	
    	try{ 
    		
    		
    		
    		LocalInvAdjustment newInvAdjustment = this.saveInvAdjustment(ADJ_RFRNC_NMBR, ADJ_DSCRPTN, ADJ_DT, USR_NM, AD_BRNCH,
    				AD_CMPNY);
    		LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);
    		LocalGlChartOfAccount glCoaVarianceAccount = null;
    		
    		
    		if(adPreference.getPrfInvGlCoaVarianceAccount() == null)
    			throw new AdPRFCoaGlVarianceAccountNotFoundException();
    		
    		try{
    			
    			glCoaVarianceAccount = glChartOfAccountHome.findByPrimaryKey(adPreference.getPrfInvGlCoaVarianceAccount());
    			//glCoaVarianceAccount.addInvAdjustment(newInvAdjustment);
    			newInvAdjustment.setGlChartOfAccount(glCoaVarianceAccount);
    			
    		} catch (FinderException ex) {
    			
    			throw new AdPRFCoaGlVarianceAccountNotFoundException();
    			
    		}
    		
    		LocalInvAdjustmentLine invAdjustmentLine = this.addInvAlEntry(invItemLocation, newInvAdjustment,
    				CST_VRNC_VL, EJBCommon.FALSE, AD_CMPNY);
    		
    		// check for branch mapping
    		
    		LocalAdBranchItemLocation adBranchItemLocation = null;
    		
    		try{
    			
    			adBranchItemLocation = adBranchItemLocationHome.findBilByIlCodeAndBrCode(
    					invAdjustmentLine.getInvItemLocation().getIlCode(), AD_BRNCH, AD_CMPNY);
    			
    		} catch (FinderException ex) {
    			
    		}
    		
    		LocalGlChartOfAccount glInventoryChartOfAccount = null;
    		
    		if (adBranchItemLocation == null) {
    			
    			glInventoryChartOfAccount = glChartOfAccountHome.findByPrimaryKey(
    					invAdjustmentLine.getInvItemLocation().getIlGlCoaInventoryAccount());
    		} else {
    			
    			glInventoryChartOfAccount = glChartOfAccountHome.findByPrimaryKey(
    					adBranchItemLocation.getBilCoaGlInventoryAccount());
    			
    		}
    		
    		
    		boolean isDebit = CST_VRNC_VL < 0 ? false : true;
    		
    		//inventory dr
    		this.addInvDrEntry(newInvAdjustment.getInvDrNextLine(), "INVENTORY",
    				isDebit == true ? EJBCommon.TRUE : EJBCommon.FALSE, Math.abs(CST_VRNC_VL), EJBCommon.FALSE,
    						glInventoryChartOfAccount.getCoaCode(), newInvAdjustment, AD_BRNCH, AD_CMPNY);
    		
    		//variance dr
    		this.addInvDrEntry(newInvAdjustment.getInvDrNextLine(),"VARIANCE", 
    				!isDebit == true ? EJBCommon.TRUE : EJBCommon.FALSE, Math.abs(CST_VRNC_VL), EJBCommon.FALSE,
    						glCoaVarianceAccount.getCoaCode(), newInvAdjustment, AD_BRNCH, AD_CMPNY);
    		
    		this.executeInvAdjPost(newInvAdjustment.getAdjCode(), newInvAdjustment.getAdjLastModifiedBy(), AD_BRNCH,
    				AD_CMPNY);
    		
    	} catch (AdPRFCoaGlVarianceAccountNotFoundException ex) {
    		
    		getSessionContext().setRollbackOnly();
    		throw ex;

    	} catch (Exception ex) {
    		
    		Debug.printStackTrace(ex);
    		getSessionContext().setRollbackOnly();
    		throw new EJBException(ex.getMessage());
    		
    	}
    	*/
    }

    private void regenerateCostVariance(Collection invCostings, LocalInvCosting invCosting, Integer AD_BRNCH, Integer AD_CMPNY)
    throws AdPRFCoaGlVarianceAccountNotFoundException {
    	/*
    	Debug.print("ArInvoicePostControllerBean regenerateCostVariance");
    	
    	try {
    		
    		Iterator i = invCostings.iterator();
    		LocalInvCosting prevInvCosting = invCosting;
    		
    		while (i.hasNext()) {
    			
    			LocalInvCosting invPropagatedCosting = (LocalInvCosting)i.next();
    			Debug.print("ArInvoicePostControllerBean regenerateCostVariance A");
    			if(prevInvCosting.getCstRemainingQuantity() < 0) {
    				
    				double TTL_CST = 0;
    				double QNTY = 0;
    				String ADJ_RFRNC_NMBR = "";
    				String ADJ_DSCRPTN = "";
    				String ADJ_CRTD_BY = "";

    				// get unit cost adjusment, document number and unit of measure
    				if (invPropagatedCosting.getApPurchaseOrderLine() != null) {

    					TTL_CST = invPropagatedCosting.getApPurchaseOrderLine().getPlAmount();
						QNTY =  this.convertByUomFromAndItemAndQuantity(
								invPropagatedCosting.getApPurchaseOrderLine().getInvUnitOfMeasure(), 
								invPropagatedCosting.getApPurchaseOrderLine().getInvItemLocation().getInvItem(),
								invPropagatedCosting.getApPurchaseOrderLine().getPlQuantity(), AD_CMPNY);
						Debug.print("ArInvoicePostControllerBean regenerateCostVariance B");
    					ADJ_DSCRPTN = invPropagatedCosting.getApPurchaseOrderLine().getApPurchaseOrder().getPoDescription();
    					ADJ_CRTD_BY = invPropagatedCosting.getApPurchaseOrderLine().getApPurchaseOrder().getPoPostedBy();
    					ADJ_RFRNC_NMBR = "APRI" +
						invPropagatedCosting.getApPurchaseOrderLine().getApPurchaseOrder().getPoDocumentNumber();
    					
    				} else if (invPropagatedCosting.getApVoucherLineItem() != null){

    					TTL_CST = invPropagatedCosting.getApVoucherLineItem().getVliAmount();
						QNTY = this.convertByUomFromAndItemAndQuantity(
								invPropagatedCosting.getApVoucherLineItem().getInvUnitOfMeasure(), 
								invPropagatedCosting.getApVoucherLineItem().getInvItemLocation().getInvItem(),
								invPropagatedCosting.getApVoucherLineItem().getVliQuantity(), AD_CMPNY);
						Debug.print("ArInvoicePostControllerBean regenerateCostVariance C");
    					if (invPropagatedCosting.getApVoucherLineItem().getApVoucher() != null) {
    						
    						ADJ_DSCRPTN = invPropagatedCosting.getApVoucherLineItem().getApVoucher().getVouDescription();
    						ADJ_CRTD_BY = invPropagatedCosting.getApVoucherLineItem().getApVoucher().getVouPostedBy();
    						ADJ_RFRNC_NMBR = "APVOU" +
							invPropagatedCosting.getApVoucherLineItem().getApVoucher().getVouDocumentNumber();
    						
    					} else if (invPropagatedCosting.getApVoucherLineItem().getApCheck() != null) {
    						
    						ADJ_DSCRPTN = invPropagatedCosting.getApVoucherLineItem().getApCheck().getChkDescription();
    						ADJ_CRTD_BY = invPropagatedCosting.getApVoucherLineItem().getApCheck().getChkPostedBy();
    						ADJ_RFRNC_NMBR = "APCHK" +
							invPropagatedCosting.getApVoucherLineItem().getApCheck().getChkDocumentNumber();
    						
    					}
    					
    				} else if (invPropagatedCosting.getArInvoiceLineItem() != null){

    					QNTY = this.convertByUomFromAndItemAndQuantity(
								invPropagatedCosting.getArInvoiceLineItem().getInvUnitOfMeasure(), 
								invPropagatedCosting.getArInvoiceLineItem().getInvItemLocation().getInvItem(),
								invPropagatedCosting.getArInvoiceLineItem().getIliQuantity(), AD_CMPNY);
   						TTL_CST = prevInvCosting.getCstRemainingValue() - invPropagatedCosting.getCstRemainingValue();
   						Debug.print("ArInvoicePostControllerBean regenerateCostVariance D");
   						if(invPropagatedCosting.getArInvoiceLineItem().getArInvoice() != null){
   	   						
   	   							ADJ_DSCRPTN = invPropagatedCosting.getArInvoiceLineItem().getArInvoice().getInvDescription();
   	   							ADJ_CRTD_BY = invPropagatedCosting.getArInvoiceLineItem().getArInvoice().getInvPostedBy();
   	   							ADJ_RFRNC_NMBR = "ARCM" + 
   								invPropagatedCosting.getArInvoiceLineItem().getArInvoice().getInvNumber();
   	    					
   	   						} else if(invPropagatedCosting.getArInvoiceLineItem().getArReceipt() != null){

   	   							ADJ_DSCRPTN = invPropagatedCosting.getArInvoiceLineItem().getArReceipt().getRctDescription();
   	   							ADJ_CRTD_BY = invPropagatedCosting.getArInvoiceLineItem().getArReceipt().getRctPostedBy();
   	   							ADJ_RFRNC_NMBR = "ARMR" + 
   								invPropagatedCosting.getArInvoiceLineItem().getArReceipt().getRctNumber();

   	   						}
   	   						
    				} else if (invPropagatedCosting.getArSalesOrderInvoiceLine() != null){

   						TTL_CST = prevInvCosting.getCstRemainingValue() - invPropagatedCosting.getCstRemainingValue();
    					QNTY = this.convertByUomFromAndItemAndQuantity(
								invPropagatedCosting.getArSalesOrderInvoiceLine().getArSalesOrderLine().getInvUnitOfMeasure(), 
								invPropagatedCosting.getArSalesOrderInvoiceLine().getArSalesOrderLine().getInvItemLocation().getInvItem(),
								invPropagatedCosting.getArSalesOrderInvoiceLine().getSilQuantityDelivered(), AD_CMPNY);
    					Debug.print("ArInvoicePostControllerBean regenerateCostVariance E");
    					ADJ_DSCRPTN = invPropagatedCosting.getArSalesOrderInvoiceLine().getArInvoice().getInvDescription();
    					ADJ_CRTD_BY = invPropagatedCosting.getArSalesOrderInvoiceLine().getArInvoice().getInvPostedBy();
    					ADJ_RFRNC_NMBR = "ARCM" + 
						invPropagatedCosting.getArSalesOrderInvoiceLine().getArInvoice().getInvNumber();
    					
    				} else if (invPropagatedCosting.getInvAdjustmentLine() != null){

    					ADJ_DSCRPTN = invPropagatedCosting.getInvAdjustmentLine().getInvAdjustment().getAdjDescription();
    					ADJ_CRTD_BY = invPropagatedCosting.getInvAdjustmentLine().getInvAdjustment().getAdjPostedBy();
    					ADJ_RFRNC_NMBR = "INVADJ" +
						invPropagatedCosting.getInvAdjustmentLine().getInvAdjustment().getAdjDocumentNumber();
    					
    					if(invPropagatedCosting.getInvAdjustmentLine().getAlAdjustQuantity() != 0) {
    						
    						TTL_CST = (invPropagatedCosting.getInvAdjustmentLine().getAlUnitCost() * 
    								invPropagatedCosting.getInvAdjustmentLine().getAlAdjustQuantity());
    						QNTY =  this.convertByUomFromAndItemAndQuantity(
    								invPropagatedCosting.getInvAdjustmentLine().getInvUnitOfMeasure(), 
									invPropagatedCosting.getInvAdjustmentLine().getInvItemLocation().getInvItem(),
									invPropagatedCosting.getInvAdjustmentLine().getAlAdjustQuantity(), AD_CMPNY);
    						Debug.print("ArInvoicePostControllerBean regenerateCostVariance F");
    					}

    				} else if (invPropagatedCosting.getInvAssemblyTransferLine() != null){

    					TTL_CST = invPropagatedCosting.getInvAssemblyTransferLine().getAtlAssembleCost();
    					QNTY = invPropagatedCosting.getInvAssemblyTransferLine().getAtlAssembleQuantity();
    					ADJ_DSCRPTN = invPropagatedCosting.getInvAssemblyTransferLine().getInvAssemblyTransfer().getAtrDescription();
    					ADJ_CRTD_BY = invPropagatedCosting.getInvAssemblyTransferLine().getInvAssemblyTransfer().getAtrPostedBy();
    					ADJ_RFRNC_NMBR = "INVAT" +
						invPropagatedCosting.getInvAssemblyTransferLine().getInvAssemblyTransfer().getAtrDocumentNumber();
    					
    				} else if (invPropagatedCosting.getInvBranchStockTransferLine() != null){

    					if(invPropagatedCosting.getInvBranchStockTransferLine().getInvBranchStockTransfer().getBstTransferOutNumber()
    							!= null) {

    						TTL_CST = invPropagatedCosting.getInvBranchStockTransferLine().getBslAmount();
							QNTY =  this.convertByUomFromAndItemAndQuantity(
									invPropagatedCosting.getInvBranchStockTransferLine().getInvUnitOfMeasure(), 
									invPropagatedCosting.getInvBranchStockTransferLine().getInvItemLocation().getInvItem(),
									invPropagatedCosting.getInvBranchStockTransferLine().getBslQuantityReceived(), AD_CMPNY);
							Debug.print("ArInvoicePostControllerBean regenerateCostVariance G");
    					} else {

    						TTL_CST = invPropagatedCosting.getInvBranchStockTransferLine().getBslAmount();
							QNTY =  this.convertByUomFromAndItemAndQuantity(
									invPropagatedCosting.getInvBranchStockTransferLine().getInvUnitOfMeasure(), 
									invPropagatedCosting.getInvBranchStockTransferLine().getInvItemLocation().getInvItem(),
									invPropagatedCosting.getInvBranchStockTransferLine().getBslQuantity(), AD_CMPNY);
							Debug.print("ArInvoicePostControllerBean regenerateCostVariance H");
    					}
    					
    					ADJ_DSCRPTN =
    						invPropagatedCosting.getInvBranchStockTransferLine().getInvBranchStockTransfer().getBstDescription();
    					ADJ_CRTD_BY =
    						invPropagatedCosting.getInvBranchStockTransferLine().getInvBranchStockTransfer().getBstPostedBy();
    					ADJ_RFRNC_NMBR = "INVBST" + 
						invPropagatedCosting.getInvBranchStockTransferLine().getInvBranchStockTransfer().getBstNumber();
    					
    				} else if (invPropagatedCosting.getInvBuildUnbuildAssemblyLine() != null){

   						TTL_CST = prevInvCosting.getCstRemainingValue() - invPropagatedCosting.getCstRemainingValue();
    					QNTY =  invPropagatedCosting.getInvBuildUnbuildAssemblyLine().getBlBuildQuantity();
    					ADJ_DSCRPTN =
    						invPropagatedCosting.getInvBuildUnbuildAssemblyLine().getInvBuildUnbuildAssembly().getBuaDescription();
    					ADJ_CRTD_BY =
    						invPropagatedCosting.getInvBuildUnbuildAssemblyLine().getInvBuildUnbuildAssembly().getBuaPostedBy();
    					ADJ_RFRNC_NMBR = "INVBUA" + 
						invPropagatedCosting.getInvBuildUnbuildAssemblyLine().getInvBuildUnbuildAssembly().getBuaDocumentNumber();
    					
    				} else if (invPropagatedCosting.getInvStockIssuanceLine()!= null){

    					TTL_CST = invPropagatedCosting.getInvStockIssuanceLine().getSilIssueCost();
						QNTY =  this.convertByUomFromAndItemAndQuantity(
								invPropagatedCosting.getInvStockIssuanceLine().getInvUnitOfMeasure(), 
								invPropagatedCosting.getInvStockIssuanceLine().getInvItemLocation().getInvItem(),
								invPropagatedCosting.getInvStockIssuanceLine().getSilIssueQuantity(), AD_CMPNY);
						Debug.print("ArInvoicePostControllerBean regenerateCostVariance I");
    					ADJ_DSCRPTN = invPropagatedCosting.getInvStockIssuanceLine().getInvStockIssuance().getSiDescription();
    					ADJ_CRTD_BY = invPropagatedCosting.getInvStockIssuanceLine().getInvStockIssuance().getSiPostedBy();
    					ADJ_RFRNC_NMBR = "INVSI" +
						invPropagatedCosting.getInvStockIssuanceLine().getInvStockIssuance().getSiDocumentNumber();
    					
    				} else if (invPropagatedCosting.getInvStockTransferLine()!= null) {

    					TTL_CST = invPropagatedCosting.getInvStockTransferLine().getStlAmount();
						QNTY =  this.convertByUomFromAndItemAndQuantity(
								invPropagatedCosting.getInvStockTransferLine().getInvUnitOfMeasure(), 
								invPropagatedCosting.getInvStockTransferLine().getInvItem(),
								invPropagatedCosting.getInvStockTransferLine().getStlQuantityDelivered(), AD_CMPNY);
						Debug.print("ArInvoicePostControllerBean regenerateCostVariance J");
    					ADJ_DSCRPTN = invPropagatedCosting.getInvStockTransferLine().getInvStockTransfer().getStDescription();
    					ADJ_CRTD_BY = invPropagatedCosting.getInvStockTransferLine().getInvStockTransfer().getStPostedBy();
    					ADJ_RFRNC_NMBR = "INVST" +
						invPropagatedCosting.getInvStockTransferLine().getInvStockTransfer().getStDocumentNumber();
    					
    				} else {
    					
    					prevInvCosting = invPropagatedCosting;
    					continue;
    					
    				}
    				
    				// if quantity is equal 0, no variance.
    				if(QNTY == 0) continue;
    				
    				// compute new cost variance
    				double UNT_CST = TTL_CST/QNTY;
    				double CST_VRNC_VL = (invPropagatedCosting.getCstRemainingQuantity() * UNT_CST -
    						invPropagatedCosting.getCstRemainingValue());
    				
    				if(CST_VRNC_VL != 0)
    					this.generateCostVariance(invPropagatedCosting.getInvItemLocation(), CST_VRNC_VL, ADJ_RFRNC_NMBR,
    							ADJ_DSCRPTN, invPropagatedCosting.getCstDate(), ADJ_CRTD_BY, AD_BRNCH, AD_CMPNY);
    				Debug.print("ArInvoicePostControllerBean regenerateCostVariance K");
    			}
    			
    			// set previous costing
    			Debug.print("ArInvoicePostControllerBean regenerateCostVariance M");
    			prevInvCosting = invPropagatedCosting;
    			
    		}
    		
    	} catch (AdPRFCoaGlVarianceAccountNotFoundException ex){
    		
    		throw ex;
    		
    	} catch (Exception ex) {
    		
    		Debug.printStackTrace(ex);
    		getSessionContext().setRollbackOnly();
    		throw new EJBException(ex.getMessage());
    		
    	}     */   	
    }
    
    private void addInvDrEntry(short DR_LN, String DR_CLSS, byte DR_DBT, double DR_AMNT, byte DR_RVRSL, Integer COA_CODE, 
    		LocalInvAdjustment invAdjustment, Integer AD_BRNCH, Integer AD_CMPNY)
    
    throws GlobalBranchAccountNumberInvalidException {
    	
    	Debug.print("ArInvoicePostControllerBean addInvDrEntry");
    	
    	LocalAdCompanyHome adCompanyHome = null;
    	LocalInvDistributionRecordHome invDistributionRecordHome = null;        
    	LocalGlChartOfAccountHome glChartOfAccountHome = null;           
    	
    	// Initialize EJB Home
    	
    	try {
    		
    		adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);    
    		invDistributionRecordHome = (LocalInvDistributionRecordHome)EJBHomeFactory.
			lookUpLocalHome(LocalInvDistributionRecordHome.JNDI_NAME, LocalInvDistributionRecordHome.class);            
    		glChartOfAccountHome = (LocalGlChartOfAccountHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlChartOfAccountHome.JNDI_NAME, LocalGlChartOfAccountHome.class);                       
    		
    		
    	} catch (NamingException ex) {
    		
    		throw new EJBException(ex.getMessage());
    		
    	}            
    	
    	try {        
    		
    		// get company
    		
    		LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
    		
    		// validate coa
    		
    		LocalGlChartOfAccount glChartOfAccount = null;
    		
    		try {
    			
    			glChartOfAccount = glChartOfAccountHome.findByCoaCodeAndBranchCode(COA_CODE, AD_BRNCH, AD_CMPNY);
    			
    		} catch(FinderException ex) {
    			
    			throw new GlobalBranchAccountNumberInvalidException ();
    			
    		}
    		
    		// create distribution record        
    		
    		LocalInvDistributionRecord invDistributionRecord = invDistributionRecordHome.create(DR_LN, DR_CLSS, DR_DBT,
    				EJBCommon.roundIt(DR_AMNT, adCompany.getGlFunctionalCurrency().getFcPrecision()), DR_RVRSL, EJBCommon.FALSE,
					 AD_CMPNY);
    		
    		//invAdjustment.addInvDistributionRecord(invDistributionRecord);
    		invDistributionRecord.setInvAdjustment(invAdjustment);
    		//glChartOfAccount.addInvDistributionRecord(invDistributionRecord);
    		invDistributionRecord.setInvChartOfAccount(glChartOfAccount);
    		
    	} catch(GlobalBranchAccountNumberInvalidException ex) {
    		
    		throw new GlobalBranchAccountNumberInvalidException ();
    		
    	} catch (Exception ex) {
    		
    		Debug.printStackTrace(ex);
    		getSessionContext().setRollbackOnly();
    		throw new EJBException(ex.getMessage());
    		
    	}
    	
    }
    
    private void executeInvAdjPost(Integer ADJ_CODE, String USR_NM, Integer AD_BRNCH, Integer AD_CMPNY) throws
	GlobalRecordAlreadyDeletedException,		
	GlobalTransactionAlreadyPostedException,
	GlJREffectiveDateNoPeriodExistException,
	GlJREffectiveDatePeriodClosedException,
	GlobalJournalNotBalanceException,
	GlobalBranchAccountNumberInvalidException {
    	
    	Debug.print("ArInvoicePostControllerBean executeInvAdjPost");
    	
    	LocalInvAdjustmentHome invAdjustmentHome = null;        
    	LocalAdCompanyHome adCompanyHome = null;
    	LocalAdPreferenceHome adPreferenceHome = null;
    	LocalGlSetOfBookHome glSetOfBookHome = null;
    	LocalGlAccountingCalendarValueHome glAccountingCalendarValueHome = null;
    	LocalGlJournalHome glJournalHome = null;
    	LocalGlJournalBatchHome glJournalBatchHome = null;
    	LocalGlSuspenseAccountHome glSuspenseAccountHome = null;
    	LocalGlJournalLineHome glJournalLineHome = null;
    	LocalGlJournalSourceHome glJournalSourceHome = null;
    	LocalGlJournalCategoryHome glJournalCategoryHome = null;
    	LocalGlFunctionalCurrencyHome glFunctionalCurrencyHome = null;
    	LocalInvDistributionRecordHome invDistributionRecordHome = null;
    	LocalInvCostingHome invCostingHome = null;
    	LocalGlChartOfAccountHome glChartOfAccountHome = null;
    	LocalInvAdjustmentLineHome invAdjustmentLineHome = null;
    	
    	
    	// Initialize EJB Home
    	
    	try {
    		
    		invAdjustmentHome = (LocalInvAdjustmentHome)EJBHomeFactory.
			lookUpLocalHome(LocalInvAdjustmentHome.JNDI_NAME, LocalInvAdjustmentHome.class);
    		adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
    		adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);
    		glSetOfBookHome = (LocalGlSetOfBookHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlSetOfBookHome.JNDI_NAME, LocalGlSetOfBookHome.class);
    		glAccountingCalendarValueHome = (LocalGlAccountingCalendarValueHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlAccountingCalendarValueHome.JNDI_NAME, LocalGlAccountingCalendarValueHome.class);
    		glJournalHome = (LocalGlJournalHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlJournalHome.JNDI_NAME, LocalGlJournalHome.class);
    		glJournalBatchHome = (LocalGlJournalBatchHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlJournalBatchHome.JNDI_NAME, LocalGlJournalBatchHome.class);
    		glSuspenseAccountHome = (LocalGlSuspenseAccountHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlSuspenseAccountHome.JNDI_NAME, LocalGlSuspenseAccountHome.class);
    		glJournalLineHome = (LocalGlJournalLineHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlJournalLineHome.JNDI_NAME, LocalGlJournalLineHome.class);
    		glJournalSourceHome = (LocalGlJournalSourceHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlJournalSourceHome.JNDI_NAME, LocalGlJournalSourceHome.class);
    		glJournalCategoryHome = (LocalGlJournalCategoryHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlJournalCategoryHome.JNDI_NAME, LocalGlJournalCategoryHome.class);
    		glFunctionalCurrencyHome = (LocalGlFunctionalCurrencyHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlFunctionalCurrencyHome.JNDI_NAME, LocalGlFunctionalCurrencyHome.class);
    		invDistributionRecordHome = (LocalInvDistributionRecordHome)EJBHomeFactory.
			lookUpLocalHome(LocalInvDistributionRecordHome.JNDI_NAME, LocalInvDistributionRecordHome.class);
    		invCostingHome = (LocalInvCostingHome)EJBHomeFactory.
			lookUpLocalHome(LocalInvCostingHome.JNDI_NAME, LocalInvCostingHome.class);
    		glChartOfAccountHome = (LocalGlChartOfAccountHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlChartOfAccountHome.JNDI_NAME, LocalGlChartOfAccountHome.class);
    		invAdjustmentLineHome = (LocalInvAdjustmentLineHome)EJBHomeFactory.
			lookUpLocalHome(LocalInvAdjustmentLineHome.JNDI_NAME, LocalInvAdjustmentLineHome.class);
    		
    	} catch (NamingException ex) {
    		
    		throw new EJBException(ex.getMessage());
    		
    	}       
    	
    	try {
    		
    		// validate if adjustment is already deleted
    		
    		LocalInvAdjustment invAdjustment = null;
    		
    		try {
    			
    			invAdjustment = invAdjustmentHome.findByPrimaryKey(ADJ_CODE);
    			
    		} catch (FinderException ex) {
    			
    			throw new GlobalRecordAlreadyDeletedException();
    			
    		}
    		
    		// validate if adjustment is already posted or void
    		
    		if (invAdjustment.getAdjPosted() == EJBCommon.TRUE) {

    			if (invAdjustment.getAdjVoid() != EJBCommon.TRUE)
    			throw new GlobalTransactionAlreadyPostedException();
    			
    		}
    		
    		Collection invAdjustmentLines = null;
    		
    		if(invAdjustment.getAdjVoid() == EJBCommon.FALSE)
    			invAdjustmentLines = invAdjustmentLineHome.findByAlVoidAndAdjCode(EJBCommon.FALSE, invAdjustment.getAdjCode(), AD_CMPNY);
    		else
    			invAdjustmentLines = invAdjustmentLineHome.findByAlVoidAndAdjCode(EJBCommon.TRUE, invAdjustment.getAdjCode(), AD_CMPNY);
    		
    		Iterator i = invAdjustmentLines.iterator();
    		
    		while(i.hasNext()) {
    			
    			
    			LocalInvAdjustmentLine invAdjustmentLine = (LocalInvAdjustmentLine) i.next();

    			LocalInvCosting invCosting =
    				invCostingHome.getByMaxCstDateToLongAndMaxCstLineNumberAndLessThanEqualCstDateAndIiNameAndLocName(
    						invAdjustment.getAdjDate(), invAdjustmentLine.getInvItemLocation().getInvItem().getIiName(),
    						invAdjustmentLine.getInvItemLocation().getInvLocation().getLocName(), AD_BRNCH, AD_CMPNY);
    			
				this.postInvAdjustmentToInventory(invAdjustmentLine, invAdjustment.getAdjDate(), 0,
						invAdjustmentLine.getAlUnitCost(), invCosting.getCstRemainingQuantity(),
						invCosting.getCstRemainingValue() + invAdjustmentLine.getAlUnitCost(), AD_BRNCH, AD_CMPNY);
    			
    		}			
    		
    		// post to gl if necessary
    		
    		LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);
    		LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
    			
    			// validate if date has no period and period is closed
    			
    			LocalGlSetOfBook glJournalSetOfBook = null;
    			
    			try {
    				
    				glJournalSetOfBook = glSetOfBookHome.findByDate(invAdjustment.getAdjDate(), AD_CMPNY);
    				
    			} catch (FinderException ex) {
    				
    				throw new GlJREffectiveDateNoPeriodExistException();
    				
    			}
    			
    			LocalGlAccountingCalendarValue glAccountingCalendarValue = 
    				glAccountingCalendarValueHome.findByAcCodeAndDate(
    						glJournalSetOfBook.getGlAccountingCalendar().getAcCode(), invAdjustment.getAdjDate(), AD_CMPNY);
    			
    			
    			if (glAccountingCalendarValue.getAcvStatus() == 'N' ||
    					glAccountingCalendarValue.getAcvStatus() == 'C' ||
						glAccountingCalendarValue.getAcvStatus() == 'P') {
    				
    				throw new GlJREffectiveDatePeriodClosedException();
    				
    			}
    			
    			// check if invoice is balance if not check suspense posting
    			
    			LocalGlJournalLine glOffsetJournalLine = null;
    			
    			Collection invDistributionRecords = null;
    			
    			if (invAdjustment.getAdjVoid() == EJBCommon.FALSE) {
    				
    				invDistributionRecords = invDistributionRecordHome.findImportableDrByDrReversedAndAdjCode(EJBCommon.FALSE,
    						invAdjustment.getAdjCode(), AD_CMPNY);
    				
    			} else {
    				
    				invDistributionRecords = invDistributionRecordHome.findImportableDrByDrReversedAndAdjCode(EJBCommon.TRUE,
    						invAdjustment.getAdjCode(), AD_CMPNY);
    				
    			}

    			
    			Iterator j = invDistributionRecords.iterator();
    			
    			double TOTAL_DEBIT = 0d;
    			double TOTAL_CREDIT = 0d;
    			
    			while (j.hasNext()) {
    				
    				LocalInvDistributionRecord invDistributionRecord = (LocalInvDistributionRecord)j.next();
    				
    				double DR_AMNT = 0d;
    				
    				DR_AMNT = invDistributionRecord.getDrAmount();
    				
    				if (invDistributionRecord.getDrDebit() == EJBCommon.TRUE) {
    					
    					TOTAL_DEBIT += DR_AMNT;
    					
    				} else {
    					
    					TOTAL_CREDIT += DR_AMNT;
    					
    				}
    				
    			}
    			
    			TOTAL_DEBIT = EJBCommon.roundIt(TOTAL_DEBIT, adCompany.getGlFunctionalCurrency().getFcPrecision());
    			TOTAL_CREDIT = EJBCommon.roundIt(TOTAL_CREDIT, adCompany.getGlFunctionalCurrency().getFcPrecision());
    			
    			if (adPreference.getPrfAllowSuspensePosting() == EJBCommon.TRUE &&
    					TOTAL_DEBIT != TOTAL_CREDIT) {
    				
    				LocalGlSuspenseAccount glSuspenseAccount = null;
    				
    				try { 	
    					
    					glSuspenseAccount = glSuspenseAccountHome.findByJsNameAndJcName("INVENTORY", "INVENTORY ADJUSTMENTS",
    							AD_CMPNY);
    					
    				} catch (FinderException ex) {
    					
    					throw new GlobalJournalNotBalanceException();
    					
    				}
    				
    				if (TOTAL_DEBIT - TOTAL_CREDIT < 0) {
    					
    					glOffsetJournalLine = glJournalLineHome.create((short)(invDistributionRecords.size() + 1), EJBCommon.TRUE, 
    							TOTAL_CREDIT - TOTAL_DEBIT, "", AD_CMPNY);
    					
    				} else {
    					
    					glOffsetJournalLine = glJournalLineHome.create((short)(invDistributionRecords.size() + 1), EJBCommon.FALSE,
    							TOTAL_DEBIT - TOTAL_CREDIT, "", AD_CMPNY);
    					
    				}
    				
    				LocalGlChartOfAccount glChartOfAccount = glSuspenseAccount.getGlChartOfAccount();
    				//glChartOfAccount.addGlJournalLine(glOffsetJournalLine);
    				glOffsetJournalLine.setGlChartOfAccount(glChartOfAccount);
    				
    				
    			} else if (adPreference.getPrfAllowSuspensePosting() == EJBCommon.FALSE &&
    					TOTAL_DEBIT != TOTAL_CREDIT) {
    				
    				throw new GlobalJournalNotBalanceException();		    	
    				
    			}
    			
    			// create journal batch if necessary
    			
    			LocalGlJournalBatch glJournalBatch = null;
    			java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat("MM/dd/yyyy");
    			
    			try {
    				
    				glJournalBatch = glJournalBatchHome.findByJbName("JOURNAL IMPORT " + formatter.format(new Date()) +
    						" INVENTORY ADJUSTMENTS", AD_BRNCH, AD_CMPNY);
    				
    			} catch (FinderException ex) {
    				
    			}
    			
    			if (glJournalBatch == null) {
    				
    				glJournalBatch = glJournalBatchHome.create("JOURNAL IMPORT " + formatter.format(new Date()) +
    						" INVENTORY ADJUSTMENTS", "JOURNAL IMPORT", "CLOSED", EJBCommon.getGcCurrentDateWoTime().getTime(),
							USR_NM, AD_BRNCH, AD_CMPNY);
    				
    			}
    			
    			// create journal entry			            	
    			
    			LocalGlJournal glJournal = glJournalHome.create(invAdjustment.getAdjReferenceNumber(),
    					invAdjustment.getAdjDescription(), invAdjustment.getAdjDate(),
						0.0d, null, invAdjustment.getAdjDocumentNumber(), null, 1d, "N/A", null,
						'N', EJBCommon.TRUE, EJBCommon.FALSE,
						USR_NM, new Date(),
						USR_NM, new Date(),
						null, null,
						USR_NM, EJBCommon.getGcCurrentDateWoTime().getTime(), 
						null, null, EJBCommon.FALSE, null, 

						AD_BRNCH, AD_CMPNY);
    			
    			LocalGlJournalSource glJournalSource = glJournalSourceHome.findByJsName("INVENTORY", AD_CMPNY);
    			glJournal.setGlJournalSource(glJournalSource);
    			
    			LocalGlFunctionalCurrency glFunctionalCurrency = glFunctionalCurrencyHome.findByFcName(adCompany.getGlFunctionalCurrency().getFcName(), AD_CMPNY);
    			glJournal.setGlFunctionalCurrency(glFunctionalCurrency);
    			
    			LocalGlJournalCategory glJournalCategory = glJournalCategoryHome.findByJcName("INVENTORY ADJUSTMENTS", AD_CMPNY);
    			glJournal.setGlJournalCategory(glJournalCategory);
    			
    			if (glJournalBatch != null) {

    				glJournal.setGlJournalBatch(glJournalBatch);
    				
    			}           		    
    			
    			// create journal lines
    			
    			j = invDistributionRecords.iterator();
    			
    			while (j.hasNext()) {
    				
    				LocalInvDistributionRecord invDistributionRecord = (LocalInvDistributionRecord)j.next();
    				
    				double DR_AMNT = 0d;
    				
    				DR_AMNT = invDistributionRecord.getDrAmount();
    				
    				LocalGlJournalLine glJournalLine = glJournalLineHome.create(invDistributionRecord.getDrLine(),	            			
    						invDistributionRecord.getDrDebit(), DR_AMNT, "", AD_CMPNY);
    				
    				//invDistributionRecord.getInvChartOfAccount().addGlJournalLine(glJournalLine);
    				glJournalLine.setGlChartOfAccount(invDistributionRecord.getInvChartOfAccount());
    				
    				//glJournal.addGlJournalLine(glJournalLine);
    				glJournalLine.setGlJournal(glJournal);
    				
    				invDistributionRecord.setDrImported(EJBCommon.TRUE);
    				
    				
    			}
    			
    			if (glOffsetJournalLine != null) {
    				
    				//glJournal.addGlJournalLine(glOffsetJournalLine);
    				glOffsetJournalLine.setGlJournal(glJournal);
    				
    			}		
    			
    			// post journal to gl
    			
    			Collection glJournalLines = glJournal.getGlJournalLines();
    			
    			i = glJournalLines.iterator();
    			
    			while (i.hasNext()) {
    				
    				LocalGlJournalLine glJournalLine = (LocalGlJournalLine)i.next();	
    				
    				// post current to current acv
    				
    				this.postToGl(glAccountingCalendarValue,
    						glJournalLine.getGlChartOfAccount(),
							true, glJournalLine.getJlDebit(), glJournalLine.getJlAmount(), AD_CMPNY);
    				
    				
    				// post to subsequent acvs (propagate)
    				
    				Collection glSubsequentAccountingCalendarValues = 
    					glAccountingCalendarValueHome.findSubsequentAcvByAcCodeAndAcvPeriodNumber( 
    							glJournalSetOfBook.getGlAccountingCalendar().getAcCode(),
								glAccountingCalendarValue.getAcvPeriodNumber(), AD_CMPNY);     
    				
    				Iterator acvsIter = glSubsequentAccountingCalendarValues.iterator();
    				
    				while (acvsIter.hasNext()) {
    					
    					LocalGlAccountingCalendarValue glSubsequentAccountingCalendarValue = 
    						(LocalGlAccountingCalendarValue)acvsIter.next();
    					
    					this.postToGl(glSubsequentAccountingCalendarValue,
    							glJournalLine.getGlChartOfAccount(),
								false, glJournalLine.getJlDebit(), glJournalLine.getJlAmount(), AD_CMPNY);
    					
    				}
    				
    				// post to subsequent years if necessary
    				
    				Collection glSubsequentSetOfBooks = glSetOfBookHome.findSubsequentSobByAcYear(
    						glJournalSetOfBook.getGlAccountingCalendar().getAcYear(), AD_CMPNY);
    				
    				if (!glSubsequentSetOfBooks.isEmpty() && glJournalSetOfBook.getSobYearEndClosed() == 1) {
    					
    					adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);		  	  	
    					LocalGlChartOfAccount glRetainedEarningsAccount =
    						glChartOfAccountHome.findByCoaAccountNumberAndBranchCode(adCompany.getCmpRetainedEarnings(),
    								AD_BRNCH, AD_CMPNY); 	  	
    					
    					Iterator sobIter = glSubsequentSetOfBooks.iterator();
    					
    					while (sobIter.hasNext()) {
    						
    						LocalGlSetOfBook glSubsequentSetOfBook = (LocalGlSetOfBook)sobIter.next();
    						
    						String ACCOUNT_TYPE = glJournalLine.getGlChartOfAccount().getCoaAccountType();
    						
    						// post to subsequent acvs of subsequent set of book(propagate)
    						
    						Collection glAccountingCalendarValues = 
    							glAccountingCalendarValueHome.findByAcCode(
    									glSubsequentSetOfBook.getGlAccountingCalendar().getAcCode(), AD_CMPNY);     
    						
    						Iterator acvIter = glAccountingCalendarValues.iterator();
    						
    						while (acvIter.hasNext()) {
    							
    							LocalGlAccountingCalendarValue glSubsequentAccountingCalendarValue = 
    								(LocalGlAccountingCalendarValue)acvIter.next();
    							
    							if (ACCOUNT_TYPE.equals("ASSET") || ACCOUNT_TYPE.equals("LIABILITY") ||
    									ACCOUNT_TYPE.equals("OWNERS EQUITY")) {
    								
    								this.postToGl(glSubsequentAccountingCalendarValue,
    										glJournalLine.getGlChartOfAccount(),
											false, glJournalLine.getJlDebit(), glJournalLine.getJlAmount(), AD_CMPNY);
    								
    							} else { // revenue & expense
    								
    								this.postToGl(glSubsequentAccountingCalendarValue,
    										glRetainedEarningsAccount,
											false, glJournalLine.getJlDebit(), glJournalLine.getJlAmount(), AD_CMPNY);					        
    								
    							}
    							
    						}
    						
    						if (glSubsequentSetOfBook.getSobYearEndClosed() == 0) break;
    						
    					}
    					
    				}
    				
    			}
			
			invAdjustment.setAdjPosted(EJBCommon.TRUE);

    	} catch (GlJREffectiveDateNoPeriodExistException ex) {
    		
    		getSessionContext().setRollbackOnly();
    		throw ex;
    		
    	} catch (GlJREffectiveDatePeriodClosedException ex) {
    		
    		getSessionContext().setRollbackOnly();
    		throw ex;
    		
    	} catch (GlobalJournalNotBalanceException ex) {
    		
    		getSessionContext().setRollbackOnly();
    		throw ex;
    		
    	} catch (GlobalRecordAlreadyDeletedException ex) {
    		
    		getSessionContext().setRollbackOnly();
    		throw ex;
    		
    	} catch (GlobalTransactionAlreadyPostedException ex) {
    		
    		getSessionContext().setRollbackOnly();
    		throw ex;
    		
    	} catch (Exception ex) {
    		
    		Debug.printStackTrace(ex);
    		getSessionContext().setRollbackOnly();
    		throw new EJBException(ex.getMessage());
    		
    	}
    	
    }

    private LocalInvAdjustmentLine addInvAlEntry(LocalInvItemLocation invItemLocation, LocalInvAdjustment invAdjustment,
    		double CST_VRNC_VL, byte AL_VD, Integer AD_CMPNY) {
    	
    	Debug.print("ArInvoicePostControllerBean addInvAlEntry");
    	
    	LocalInvAdjustmentLineHome invAdjustmentLineHome = null;
    	
    	// Initialize EJB Home
    	
    	try {
    		
    		invAdjustmentLineHome = (LocalInvAdjustmentLineHome)EJBHomeFactory.
			lookUpLocalHome(LocalInvAdjustmentLineHome.JNDI_NAME, LocalInvAdjustmentLineHome.class); 
    		
    	} catch (NamingException ex) {
    		
    		throw new EJBException(ex.getMessage());
    		
    	}            
    	
    	try {
    		
    		// create dr entry
    		LocalInvAdjustmentLine invAdjustmentLine = null;
    		invAdjustmentLine = invAdjustmentLineHome.create(CST_VRNC_VL,null, null, 0,0, AL_VD, AD_CMPNY);
    		
    		// map adjustment, unit of measure, item location
    		//invAdjustment.addInvAdjustmentLine(invAdjustmentLine);
    		invAdjustmentLine.setInvAdjustment(invAdjustment);
    		//invItemLocation.getInvItem().getInvUnitOfMeasure().addInvAdjustmentLine(invAdjustmentLine);
    		invAdjustmentLine.setInvUnitOfMeasure(invItemLocation.getInvItem().getInvUnitOfMeasure());
    		//invItemLocation.addInvAdjustmentLine(invAdjustmentLine);
    		invAdjustmentLine.setInvItemLocation(invItemLocation);
    		
    		return invAdjustmentLine;
    		
    	} catch (Exception ex) {
    		
    		Debug.printStackTrace(ex);
    		getSessionContext().setRollbackOnly();
    		throw new EJBException(ex.getMessage());
    		
    	}
    	
    } 
    
    private LocalInvAdjustment saveInvAdjustment(String ADJ_RFRNC_NMBR, String ADJ_DSCRPTN,
    		Date ADJ_DATE, String USR_NM, Integer AD_BRNCH, Integer AD_CMPNY){
    	
    	Debug.print("ArInvoicePostControllerBean saveInvAdjustment");
    	
    	LocalInvAdjustmentHome invAdjustmentHome = null;
    	LocalAdDocumentSequenceAssignmentHome adDocumentSequenceAssignmentHome = null;
    	LocalAdBranchDocumentSequenceAssignmentHome adBranchDocumentSequenceAssignmentHome = null;
    	// Initialize EJB Home    	
    	
    	try{
    	
    		invAdjustmentHome = (LocalInvAdjustmentHome)EJBHomeFactory.
			lookUpLocalHome(LocalInvAdjustmentHome.JNDI_NAME, LocalInvAdjustmentHome.class);
			adDocumentSequenceAssignmentHome = (LocalAdDocumentSequenceAssignmentHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdDocumentSequenceAssignmentHome.JNDI_NAME, LocalAdDocumentSequenceAssignmentHome.class);
			adBranchDocumentSequenceAssignmentHome = (LocalAdBranchDocumentSequenceAssignmentHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdBranchDocumentSequenceAssignmentHome.JNDI_NAME, LocalAdBranchDocumentSequenceAssignmentHome.class); 

    	} catch (NamingException ex) {
    		
    		throw new EJBException(ex.getMessage());
    		
    	}       
    	
    	try{
    		
    		// generate adj document number
    		String ADJ_DCMNT_NMBR = null;
    		
    		LocalAdBranchDocumentSequenceAssignment adBranchDocumentSequenceAssignment = null;
    		LocalAdDocumentSequenceAssignment adDocumentSequenceAssignment = null;
    		
    		try {
    			
    			adDocumentSequenceAssignment = adDocumentSequenceAssignmentHome.findByDcName("INV ADJUSTMENT", AD_CMPNY);
    			
    		} catch (FinderException ex) {
    			
    		}
    		
    		try {
    			
    			adBranchDocumentSequenceAssignment = adBranchDocumentSequenceAssignmentHome.findBdsByDsaCodeAndBrCode(
    					adDocumentSequenceAssignment.getDsaCode(), AD_BRNCH, AD_CMPNY);
    			
    		} catch (FinderException ex) {
    			
    		}
    		
    		while (true) {
    			
    			if (adBranchDocumentSequenceAssignment == null || adBranchDocumentSequenceAssignment.getBdsNextSequence() == null) {
    				
    				try {
    					
    					invAdjustmentHome.findByAdjDocumentNumberAndBrCode(adDocumentSequenceAssignment.getDsaNextSequence(),
    							AD_BRNCH, AD_CMPNY);		            		
    					adDocumentSequenceAssignment.setDsaNextSequence(EJBCommon.incrementStringNumber(
    							adDocumentSequenceAssignment.getDsaNextSequence()));	
    					
    				} catch (FinderException ex) {
    					
    					ADJ_DCMNT_NMBR = adDocumentSequenceAssignment.getDsaNextSequence();
    					adDocumentSequenceAssignment.setDsaNextSequence(EJBCommon.incrementStringNumber(
    							adDocumentSequenceAssignment.getDsaNextSequence()));	
    					break;
    					
    				}
    				
    			} else {
    				
    				try {
    					
    					invAdjustmentHome.findByAdjDocumentNumberAndBrCode(
    							adBranchDocumentSequenceAssignment.getBdsNextSequence(), AD_BRNCH, AD_CMPNY);		            		
    					adBranchDocumentSequenceAssignment.setBdsNextSequence(EJBCommon.incrementStringNumber(
    							adBranchDocumentSequenceAssignment.getBdsNextSequence()));
    					
    				} catch (FinderException ex) {
    					
    					ADJ_DCMNT_NMBR = adBranchDocumentSequenceAssignment.getBdsNextSequence();
    					adBranchDocumentSequenceAssignment.setBdsNextSequence(EJBCommon.incrementStringNumber(
    							adBranchDocumentSequenceAssignment.getBdsNextSequence()));	
    					break;
    					
    				}
    				
    			}
    			
    		}		            
    		
    		LocalInvAdjustment invAdjustment = invAdjustmentHome.create(ADJ_DCMNT_NMBR, ADJ_RFRNC_NMBR,
    				ADJ_DSCRPTN, ADJ_DATE, "COST-VARIANCE", "N/A", EJBCommon.FALSE, USR_NM, ADJ_DATE, USR_NM, ADJ_DATE, null, null,
					USR_NM, ADJ_DATE, null, null, EJBCommon.TRUE, EJBCommon.FALSE, AD_BRNCH, AD_CMPNY);
    		
    		return invAdjustment;
    		
    	} catch (Exception ex) {
    		
    		Debug.printStackTrace(ex);
    		getSessionContext().setRollbackOnly();
    		throw new EJBException(ex.getMessage());
    		
    	}       
    	
    	
    }
    
    private void postInvAdjustmentToInventory(LocalInvAdjustmentLine invAdjustmentLine, Date CST_DT, double CST_ADJST_QTY,
    		double CST_ADJST_CST, double CST_RMNNG_QTY, double CST_RMNNG_VL, Integer AD_BRNCH,Integer AD_CMPNY) {
    	
    	Debug.print("ArInvoicePostControllerBean postInvAdjustmentToInventory");
    	
    	LocalInvCostingHome invCostingHome = null;
    	LocalAdPreferenceHome adPreferenceHome = null;
    	LocalAdCompanyHome adCompanyHome = null;
    	
    	// Initialize EJB Home
    	
    	try {
    		
    		invCostingHome = (LocalInvCostingHome)EJBHomeFactory.
			lookUpLocalHome(LocalInvCostingHome.JNDI_NAME, LocalInvCostingHome.class);
    		adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);
    		adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);              
    		
    	} catch (NamingException ex) {
    		
    		getSessionContext().setRollbackOnly();
    		throw new EJBException(ex.getMessage());
    		
    	}
    	
    	try {
    		
    		LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);
    		LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
    		LocalInvItemLocation invItemLocation = invAdjustmentLine.getInvItemLocation();
    		int CST_LN_NMBR = 0;
    		
    		CST_ADJST_QTY = EJBCommon.roundIt(CST_ADJST_QTY, adPreference.getPrfInvQuantityPrecisionUnit());
    		CST_ADJST_CST = EJBCommon.roundIt(CST_ADJST_CST, adCompany.getGlFunctionalCurrency().getFcPrecision());
    		CST_RMNNG_QTY = EJBCommon.roundIt(CST_RMNNG_QTY, adPreference.getPrfInvQuantityPrecisionUnit());
    		CST_RMNNG_VL = EJBCommon.roundIt(CST_RMNNG_VL, adCompany.getGlFunctionalCurrency().getFcPrecision());
    		
    		if (CST_ADJST_QTY < 0) {
    			
    			invItemLocation.setIlCommittedQuantity(invItemLocation.getIlCommittedQuantity() - Math.abs(CST_ADJST_QTY));
    			
    		}
    		
    		// create costing
    		
    		try {
    			
    			// generate line number
    			
    			LocalInvCosting invCurrentCosting = invCostingHome.getByMaxCstLineNumberAndCstDateToLongAndIiNameAndLocName(
    					CST_DT.getTime(), invItemLocation.getInvItem().getIiName(), invItemLocation.getInvLocation().getLocName(),
						AD_BRNCH, AD_CMPNY);
    			CST_LN_NMBR = invCurrentCosting.getCstLineNumber() + 1;
    			
    		} catch (FinderException ex) {
    			
    			CST_LN_NMBR = 1;
    			
    		}
    		
    		LocalInvCosting invCosting = invCostingHome.create(CST_DT, CST_DT.getTime(), CST_LN_NMBR, 0d, 0d, 0d, 0d,
    				CST_ADJST_QTY, CST_ADJST_CST, 0d, 0d, CST_RMNNG_QTY, CST_RMNNG_VL, 0d, 0d, CST_ADJST_QTY > 0 ? CST_ADJST_QTY : 0, AD_BRNCH, AD_CMPNY);
    		//invItemLocation.addInvCosting(invCosting);
    		invCosting.setInvItemLocation(invItemLocation);
    		invCosting.setInvAdjustmentLine(invAdjustmentLine);    		
    		
    		// propagate balance if necessary           
    		
    		Collection invCostings = invCostingHome.findByGreaterThanCstDateAndIiNameAndLocName(CST_DT, invItemLocation.getInvItem().getIiName(), invItemLocation.getInvLocation().getLocName(), AD_BRNCH, AD_CMPNY);
    		
    		Iterator i = invCostings.iterator();
    		
    		while (i.hasNext()) {
    			
    			LocalInvCosting invPropagatedCosting = (LocalInvCosting)i.next();
    			
    			invPropagatedCosting.setCstRemainingQuantity(invPropagatedCosting.getCstRemainingQuantity() + CST_ADJST_QTY);
    			invPropagatedCosting.setCstRemainingValue(invPropagatedCosting.getCstRemainingValue() + CST_ADJST_CST);
    			
    		}                           
    		
    		
    	} catch (Exception ex) {
    		
    		Debug.printStackTrace(ex);
    		getSessionContext().setRollbackOnly();
    		throw new EJBException(ex.getMessage());
    		
    	}
    	
    	
    	
    }
    
    // SessionBean methods

    /**
     * @ejb:create-method view-type="remote"
     **/
    public void ejbCreate() throws CreateException {

       Debug.print("ArInvoicePostControllerBean ejbCreate");
      
    }
}
