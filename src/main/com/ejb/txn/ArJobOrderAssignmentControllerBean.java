/*
 * ArJobOrderAssignmentControllerBean.java
 *
 * Created on January 6, 2019 12:00 PM
 * @author  Ruben P. Lamberte
 */

package com.ejb.txn;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;
import javax.naming.NamingException;

import com.ejb.ad.LocalAdCompany;
import com.ejb.ad.LocalAdCompanyHome;
import com.ejb.ad.LocalAdPreference;
import com.ejb.ad.LocalAdPreferenceHome;
import com.ejb.ar.LocalArJobOrderAssignment;
import com.ejb.ar.LocalArJobOrderAssignmentHome;
import com.ejb.ar.LocalArJobOrderLine;
import com.ejb.ar.LocalArJobOrderLineHome;
import com.ejb.ar.LocalArJobOrderType;
import com.ejb.ar.LocalArJobOrderTypeHome;
import com.ejb.ar.LocalArPersonel;
import com.ejb.ar.LocalArPersonelHome;

import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.exception.GlobalRecordAlreadyExistException;
import com.ejb.exception.GlobalNoRecordFoundException;
import com.util.AbstractSessionBean;
import com.util.ArModJobOrderAssignmentDetails;
import com.util.ArModJobOrderLineDetails;
import com.util.ArPersonelDetails;
import com.util.Debug;
import com.util.EJBCommon;
import com.util.EJBHomeFactory;

/**
 * @ejb:bean name="ArJobOrderAssignmentControllerEJB"
 *           display-name="used for entering job order assignment lines"
 *           type="Stateless"
 *           view-type="remote"
 *           jndi-name="ejb/ArJobOrderAssignmentControllerEJB"
 *
 * @ejb:interface remote-class="com.ejb.txn.ArJobOrderAssignmentController"
 *                extends="javax.ejb.EJBObject"
 *
 * @ejb:home remote-class="com.ejb.txn.ArJobOrderAssignmentControllerHome"
 *           extends="javax.ejb.EJBHome"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="apuser"
 *                        role-link="apuserlink"
 *
 * @ejb:permission role-name="apuser"
 * 
 */

public class ArJobOrderAssignmentControllerBean extends AbstractSessionBean {
	
	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public ArrayList getArPeAll(Integer AD_BRNCH, Integer AD_CMPNY) {
		
		Debug.print("ArJobOrderAssignmentControllerBean getApSplAll");
		
		LocalArPersonelHome arPersonelHome = null;
		

		
		ArrayList list = new ArrayList();
		
		// Initialize EJB Home
		
		try {
			
			arPersonelHome = (LocalArPersonelHome)EJBHomeFactory.
				lookUpLocalHome(LocalArPersonelHome.JNDI_NAME, LocalArPersonelHome.class);
			
		} catch (NamingException ex) {
			
			throw new EJBException(ex.getMessage());
			
		}
		
		try {
			
			Collection arPersonels = arPersonelHome.findPeAll( AD_CMPNY);
			
			Iterator i = arPersonels.iterator();
			
			while (i.hasNext()) {
				
				LocalArPersonel arPersonel = (LocalArPersonel)i.next();
				
				list.add(arPersonel.getPeIdNumber());
				
			}
			
			return list;
			
		} catch (Exception ex) {
			
			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());
			
		}
		
	}
	
	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public ArModJobOrderLineDetails getArJolByJolCode(Integer JOL_CODE, Integer AD_CMPNY) {
		
		Debug.print("ArJobOrderAssignmentControllerBean getArJolByJolCode");
		
		LocalArJobOrderLineHome arJobOrderLineHome = null;

		
		//Initialize EJB Home
		
		try {
			
			arJobOrderLineHome = (LocalArJobOrderLineHome)EJBHomeFactory.
				lookUpLocalHome(LocalArJobOrderLineHome.JNDI_NAME, LocalArJobOrderLineHome.class);
			
		} catch (NamingException ex) {
			
			throw new EJBException(ex.getMessage());
			
		}
		
		try {
			
			LocalArJobOrderLine arJobOrderLine = arJobOrderLineHome.findByPrimaryKey(JOL_CODE);
		
			ArModJobOrderLineDetails details = new ArModJobOrderLineDetails();
		
			details.setJolJoDocumentNumber(arJobOrderLine.getArJobOrder().getJoDocumentNumber());
			details.setJolJoDate(arJobOrderLine.getArJobOrder().getJoDate());
			
			
			details.setJolIiName(arJobOrderLine.getInvItemLocation().getInvItem().getIiName());
			details.setJolLocName(arJobOrderLine.getInvItemLocation().getInvLocation().getLocName());
			details.setJolUomName(arJobOrderLine.getInvUnitOfMeasure().getUomName());
			details.setJolQuantity(arJobOrderLine.getJolQuantity());
			details.setJolUnitPrice(arJobOrderLine.getJolUnitPrice());
			details.setJolAmount(arJobOrderLine.getJolAmount());
			details.setJolIiDescription(arJobOrderLine.getInvItemLocation().getInvItem().getIiDescription());
			details.setJoVoid(arJobOrderLine.getArJobOrder().getJoVoid()==EJBCommon.TRUE?true:false);
			details.setJoPosted(arJobOrderLine.getArJobOrder().getJoPosted()==EJBCommon.TRUE?true:false);
			
			
			
			Collection arJobOrderAssignments = arJobOrderLine.getArJobOrderAssignments();

			//get canvass lines
			
			Iterator i = arJobOrderAssignments.iterator();
			
			while(i.hasNext()) {
				
				LocalArJobOrderAssignment arJobOrderAssignment = (LocalArJobOrderAssignment)i.next();
			
				ArModJobOrderAssignmentDetails jaDetails = new ArModJobOrderAssignmentDetails();
				System.out.println("ja code:" + arJobOrderAssignment.getJaCode());
				jaDetails.setJaCode(arJobOrderAssignment.getJaCode());
				jaDetails.setJaLine(arJobOrderAssignment.getJaLine());
				System.out.println("REMARKS="+arJobOrderAssignment.getJaRemarks());
	
				jaDetails.setJaRemarks(arJobOrderAssignment.getJaRemarks());
				jaDetails.setJaPeIdNumber(arJobOrderAssignment.getArPersonel().getPeIdNumber());
				jaDetails.setJaQuantity(arJobOrderAssignment.getJaQuantity());
				
				System.out.println("unit cost:?" + arJobOrderAssignment.getJaUnitCost());
				jaDetails.setJaUnitCost(arJobOrderAssignment.getJaUnitCost());
				jaDetails.setJaAmount(arJobOrderAssignment.getJaAmount());
				jaDetails.setJaSo(arJobOrderAssignment.getJaSo());
				jaDetails.setJaPeName(arJobOrderAssignment.getArPersonel().getPeName());
				
				details.saveJaList(jaDetails);
				
			}
			
			return details;
			
		} catch (Exception ex) {
			
			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());
			
		}
		
	}
	
	
	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public void saveArJbOrdrAssgnmnt(ArrayList jaList, Integer JOL_CODE, Integer AD_CMPNY) 
		throws GlobalRecordAlreadyExistException {
		
		Debug.print("ArJobOrderAssignmentControllerBean saveArJbOrdrAssgnmnt");

		LocalArJobOrderAssignmentHome arJobOrderAssignmentHome = null;
		
		LocalArJobOrderLineHome arJobOrderLineHome = null;
	
		LocalArPersonelHome arPersonelHome = null;
		
		//Initialize EJB Home
		
		try {
			
			arJobOrderAssignmentHome = (LocalArJobOrderAssignmentHome)EJBHomeFactory.
				lookUpLocalHome(LocalArJobOrderAssignmentHome.JNDI_NAME, LocalArJobOrderAssignmentHome.class);
			arJobOrderLineHome = (LocalArJobOrderLineHome)EJBHomeFactory.
				lookUpLocalHome(LocalArJobOrderLineHome.JNDI_NAME, LocalArJobOrderLineHome.class);
			arPersonelHome = (LocalArPersonelHome)EJBHomeFactory.
				lookUpLocalHome(LocalArPersonelHome.JNDI_NAME, LocalArPersonelHome.class);
			
		} catch (NamingException ex) {
			
			throw new EJBException(ex.getMessage());
			
		}
		
		try {
			
			LocalArJobOrderLine arJobOrderLine = arJobOrderLineHome.findByPrimaryKey(JOL_CODE);
			
			Collection arJobOrderAssignments = arJobOrderLine.getArJobOrderAssignments();
			
			//remove all canvass lines
			
			Iterator i = arJobOrderAssignments.iterator();
			
			while(i.hasNext()) {
				
				LocalArJobOrderAssignment arJobOrderAssignment = (LocalArJobOrderAssignment)i.next();
				
				i.remove();
				
				arJobOrderAssignment.remove();
				
			}
			
			//add new canvass lines to purchase requisition line
			
			double JOL_AMNT = 0d;
			double JOL_QTTY = 0d;
			
			i = jaList.iterator();
			
			while(i.hasNext()) {
				
		
				ArModJobOrderAssignmentDetails mdetails = (ArModJobOrderAssignmentDetails)i.next();
				System.out.println("REMARKS="+mdetails.getJaRemarks());
				
				LocalArJobOrderAssignment arJobOrderAssignment = arJobOrderAssignmentHome.create(mdetails.getJaLine(), mdetails.getJaRemarks(),
						mdetails.getJaQuantity(), mdetails.getJaUnitCost(),
						EJBCommon.roundIt(mdetails.getJaQuantity() * mdetails.getJaUnitCost(), this.getGlFcPrecisionUnit(AD_CMPNY)),
						mdetails.getJaSo(), AD_CMPNY);
				
				arJobOrderAssignments = arJobOrderLine.getArJobOrderAssignments();
				
				LocalArPersonel arPersonel = arPersonelHome.findByPeIdNumber(mdetails.getJaPeIdNumber(), AD_CMPNY);
				
				if(this.hasSupplier(arJobOrderLine, arPersonel.getPeIdNumber())) {
					
					throw new GlobalRecordAlreadyExistException(arPersonel.getPeIdNumber());
					
				}
					
				arJobOrderAssignment.setArPersonel(arPersonel);
			
				
				arJobOrderAssignment.setArJobOrderLine(arJobOrderLine);
				
		
				if(arJobOrderAssignment.getJaSo() == 1){
				JOL_AMNT += arJobOrderAssignment.getJaAmount();
				
				JOL_QTTY += arJobOrderAssignment.getJaQuantity();
				}
				
			}
			
			arJobOrderLine.setJolUnitPrice(JOL_AMNT);
			arJobOrderLine.setJolAmount(JOL_AMNT);
			arJobOrderLine.setJolQuantity(1);
			System.out.println("JOL_QTTY-----"+JOL_QTTY);
			//apPurchaseRequisitionLine.setPrlAmount(PRL_AMNT);
			//apPurchaseRequisitionLine.setPrlQuantity(PRL_QTTY);
			
		} catch (GlobalRecordAlreadyExistException ex) {
			
			getSessionContext().setRollbackOnly();
			throw ex;
			
		} catch (Exception ex) {
			
			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());
			
		}
		
	}
	
	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public short getGlFcPrecisionUnit(Integer AD_CMPNY) {
		
		Debug.print("ArJobOrderAssignmentControllerBean getGlFcPrecisionUnit");
		
		LocalAdCompanyHome adCompanyHome = null;
		
		
		// Initialize EJB Home
		
		try {
			
			adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);       
			
		} catch (NamingException ex) {
			
			throw new EJBException(ex.getMessage());
			
		}
		
		try {
			
			LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
			
			return  adCompany.getGlFunctionalCurrency().getFcPrecision();
			
		} catch (Exception ex) {
			
			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());
			
		}
		
	}
	
	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public short getInvGpQuantityPrecisionUnit(Integer AD_CMPNY) {
		
		Debug.print("ArJobOrderAssignmentControllerBean getInvGpQuantityPrecisionUnit");
		
		LocalAdPreferenceHome adPreferenceHome = null;         
		
		// Initialize EJB Home
		
		try {
			
			adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);       
			
		} catch (NamingException ex) {
			
			throw new EJBException(ex.getMessage());
			
		}
		
		
		try {
			
			LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);
			
			return adPreference.getPrfInvQuantityPrecisionUnit();
			
		} catch (Exception ex) {
			
			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());
			
		}
		
	}
	
	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public short getAdPrfArInvoiceLineNumber(Integer AD_CMPNY) {
		
		Debug.print("ArJobOrderAssignmentControllerBean getAdPrfArInvoiceLineNumber");
		
		LocalAdPreferenceHome adPreferenceHome = null;
		
		// Initialize EJB Home
		
		try {
			
			adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);       
			
		} catch (NamingException ex) {
			
			throw new EJBException(ex.getMessage());
			
		}
		
		
		try {
			
			LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);
			
			return adPreference.getPrfArInvoiceLineNumber();
			
		} catch (Exception ex) {
			
			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());
			
		}
		
	}
	
	
	
	/**
	 * @throws GlobalNoRecordFoundException 
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public ArPersonelDetails getArPrsnlByPeIdNmbr(String PE_ID_NMBR, Integer AD_CMPNY) throws GlobalNoRecordFoundException {
		
		Debug.print("ArJobOrderAssignmentControllerBean getArPrsnlByPeIdNmbr");
		
	
		LocalArPersonelHome arPersonelHome = null;
		// Initialize EJB Home
		
		try {
			
			arPersonelHome = (LocalArPersonelHome)EJBHomeFactory.
				lookUpLocalHome(LocalArPersonelHome.JNDI_NAME, LocalArPersonelHome.class);       
			
		} catch (NamingException ex) {
			
			throw new EJBException(ex.getMessage());
			
		}
		
		
		try {
			
			LocalArPersonel arPersonel = arPersonelHome.findByPeIdNumber(PE_ID_NMBR, AD_CMPNY);
			
			
			ArPersonelDetails details = new ArPersonelDetails();
			
			details.setPeCode(arPersonel.getPeCode());
			details.setPeName(arPersonel.getPeName());
			details.setPeIdNumber(arPersonel.getPeIdNumber());
			details.setPeDescription(arPersonel.getPeDescription());
			details.setPeAddress(arPersonel.getPeAddress());
			details.setPeRate(arPersonel.getPeRate());
		
			
			return details;
		} catch (FinderException ex) {
			
			throw new GlobalNoRecordFoundException(ex.getMessage());
		} catch (Exception ex) {
			
			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());
			
		}
		
	}
	
	
	
	
	
	//private methods
	
	private boolean hasSupplier(LocalArJobOrderLine arJobOrderLine, String PE_ID_NMBR) {
		
		Debug.print("ArJobOrderAssignmentControllerBean hasSupplier");
		
		Collection arJobOrderAssignments = arJobOrderLine.getArJobOrderAssignments();
		
		Iterator i = arJobOrderAssignments.iterator();
		
		while(i.hasNext()) {
			
			LocalArJobOrderAssignment arJobOrderAssignment = (LocalArJobOrderAssignment)i.next();
			
			if(arJobOrderAssignment.getArPersonel().getPeIdNumber().equals(PE_ID_NMBR)) {
				
				return true;
				
			}
			
		}
		
		return false;
		
	}
	
	
	

	
	//	 SessionBean methods
	
	/**
	 * @ejb:create-method view-type="remote"
	 **/
	public void ejbCreate() throws CreateException {
		
		Debug.print("ArJobOrderAssignmentControllerBean ejbCreate");
		
	}
	
}