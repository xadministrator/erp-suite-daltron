 package com.ejb.txn;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;
import javax.naming.NamingException;

import com.ejb.ad.LocalAdAmountLimit;
import com.ejb.ad.LocalAdAmountLimitHome;
import com.ejb.ad.LocalAdApproval;
import com.ejb.ad.LocalAdApprovalHome;
import com.ejb.ad.LocalAdApprovalQueue;
import com.ejb.ad.LocalAdApprovalQueueHome;
import com.ejb.ad.LocalAdApprovalUser;
import com.ejb.ad.LocalAdApprovalUserHome;
import com.ejb.ad.LocalAdBranchDocumentSequenceAssignment;
import com.ejb.ad.LocalAdBranchDocumentSequenceAssignmentHome;
import com.ejb.ad.LocalAdCompany;
import com.ejb.ad.LocalAdCompanyHome;
import com.ejb.ad.LocalAdDeleteAuditTrailHome;
import com.ejb.ad.LocalAdLookUp;
import com.ejb.ad.LocalAdLookUpHome;
import com.ejb.ad.LocalAdLookUpValue;
import com.ejb.ad.LocalAdLookUpValueHome;
import com.ejb.ad.LocalAdPaymentTerm;
import com.ejb.ad.LocalAdPaymentTermHome;
import com.ejb.ad.LocalAdPreference;
import com.ejb.ad.LocalAdPreferenceHome;
import com.ejb.ad.LocalAdUser;
import com.ejb.ad.LocalAdUserHome;
import com.ejb.ap.LocalApPurchaseOrderLine;
import com.ejb.ap.LocalApPurchaseRequisitionLine;
import com.ejb.ap.LocalApPurchaseRequisitionLineHome;
import com.ejb.ar.LocalArAppliedInvoice;
import com.ejb.ar.LocalArCustomer;
import com.ejb.ar.LocalArCustomerHome;
import com.ejb.ar.LocalArDistributionRecord;
import com.ejb.ar.LocalArInvoice;
import com.ejb.ar.LocalArInvoiceHome;
import com.ejb.ar.LocalArInvoicePaymentSchedule;
import com.ejb.ar.LocalArInvoicePaymentScheduleHome;
import com.ejb.ar.LocalArJobOrder;
import com.ejb.ar.LocalArJobOrderAssignment;
import com.ejb.ar.LocalArJobOrderAssignmentHome;
import com.ejb.ar.LocalArJobOrderHome;
import com.ejb.ar.LocalArJobOrderLine;
import com.ejb.ar.LocalArJobOrderLineHome;
import com.ejb.ar.LocalArJobOrderType;
import com.ejb.ar.LocalArJobOrderTypeHome;
import com.ejb.ar.LocalArPersonel;
import com.ejb.ar.LocalArPersonelHome;
import com.ejb.ar.LocalArSalesperson;
import com.ejb.ar.LocalArSalespersonHome;
import com.ejb.ar.LocalArTaxCode;
import com.ejb.ar.LocalArTaxCodeHome;
import com.ejb.cm.LocalCmAdjustment;
import com.ejb.exception.GlobalConversionDateNotExistException;
import com.ejb.exception.GlobalDocumentNumberNotUniqueException;
import com.ejb.exception.GlobalInvItemLocationNotFoundException;
import com.ejb.exception.GlobalJournalNotBalanceException;
import com.ejb.exception.GlobalNoApprovalApproverFoundException;
import com.ejb.exception.GlobalNoApprovalRequesterFoundException;
import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.exception.GlobalPaymentTermInvalidException;
import com.ejb.exception.GlobalRecordAlreadyAssignedException;
import com.ejb.exception.GlobalRecordAlreadyDeletedException;
import com.ejb.exception.GlobalTransactionAlreadyPendingException;
import com.ejb.exception.GlobalTransactionAlreadyVoidException;
import com.ejb.gl.LocalAdDocumentSequenceAssignment;
import com.ejb.gl.LocalAdDocumentSequenceAssignmentHome;
import com.ejb.gl.LocalGlChartOfAccount;
import com.ejb.gl.LocalGlChartOfAccountHome;
import com.ejb.gl.LocalGlFunctionalCurrency;
import com.ejb.gl.LocalGlFunctionalCurrencyHome;
import com.ejb.gl.LocalGlFunctionalCurrencyRate;
import com.ejb.gl.LocalGlFunctionalCurrencyRateHome;
import com.ejb.inv.LocalInvItem;
import com.ejb.inv.LocalInvItemHome;
import com.ejb.inv.LocalInvItemLocation;
import com.ejb.inv.LocalInvItemLocationHome;
import com.ejb.inv.LocalInvLineItem;
import com.ejb.inv.LocalInvLineItemTemplate;
import com.ejb.inv.LocalInvLineItemTemplateHome;
import com.ejb.inv.LocalInvLocation;
import com.ejb.inv.LocalInvLocationHome;
import com.ejb.inv.LocalInvPriceLevel;
import com.ejb.inv.LocalInvPriceLevelHome;
import com.ejb.inv.LocalInvTag;
import com.ejb.inv.LocalInvTagHome;
import com.ejb.inv.LocalInvUnitOfMeasure;
import com.ejb.inv.LocalInvUnitOfMeasureConversion;
import com.ejb.inv.LocalInvUnitOfMeasureConversionHome;
import com.ejb.inv.LocalInvUnitOfMeasureHome;
import com.util.AbstractSessionBean;
import com.util.ArModCustomerDetails;
import com.util.ArModJobOrderAssignmentDetails;
import com.util.ArModJobOrderDetails;
import com.util.ArModJobOrderLineDetails;
import com.util.ArModJobOrderTypeDetails;
import com.util.ArModSalesOrderDetails;
import com.util.ArModSalesOrderLineDetails;
import com.util.ArSalespersonDetails;
import com.util.ArTaxCodeDetails;
import com.util.Debug;
import com.util.EJBCommon;
import com.util.EJBHomeFactory;
import com.util.GlModFunctionalCurrencyDetails;
import com.util.InvModLineItemDetails;
import com.util.InvModTagListDetails;
import com.util.InvModUnitOfMeasureDetails;

/**
 * @ejb:bean name="ArJobOrderEntryControllerEJB"
 *           display-name="used for entergetJoJobServicesng job order"
 *           type="Stateless"
 *           view-type="remote"
 *           jndi-name="ejb/ArJobOrderEntryControllerEJB"
 *
 * @ejb:interface remote-class="com.ejb.txn.ArJobOrderEntryController"
 *                extends="javax.ejb.EJBObject"
 *
 * @ejb:home remote-class="com.ejb.txn.ArJobOrderEntryControllerHome"
 *           extends="javax.ejb.EJBHome"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="aruser"
 *                        role-link="aruserlink"
 *
 * @ejb:permission role-name="aruser"
 *
*/

public class ArJobOrderEntryControllerBean extends AbstractSessionBean {

    /**
	 * @ejb:interface-method view-type="remote"
	 **/
	public Integer saveArJoEntry(com.util.ArModJobOrderDetails details, String PYT_NM, String TC_NM, String FC_NM, String CST_CSTMR_CODE, ArrayList jolList, boolean isDraft, Integer AD_BRNCH, Integer AD_CMPNY) throws
		GlobalRecordAlreadyDeletedException,
		GlobalDocumentNumberNotUniqueException,
		GlobalConversionDateNotExistException,
		GlobalPaymentTermInvalidException,
		GlobalTransactionAlreadyPendingException,
		GlobalTransactionAlreadyVoidException,
		GlobalJournalNotBalanceException,
		GlobalInvItemLocationNotFoundException,
		GlobalNoApprovalApproverFoundException,
		GlobalNoApprovalRequesterFoundException,
		GlobalRecordAlreadyAssignedException {

	    Debug.print("ArJobOrderEntryControllerBean saveArJoEntry");

	    LocalArJobOrderHome arJobOrderHome = null;
	    LocalArJobOrderLineHome arJobOrderLineHome = null;
	    LocalAdDocumentSequenceAssignmentHome adDocumentSequenceAssignmentHome = null;
	    LocalAdBranchDocumentSequenceAssignmentHome adBranchDocumentSequenceAssignmentHome = null;
	    LocalGlFunctionalCurrencyHome glFunctionalCurrencyHome = null;
	    LocalGlFunctionalCurrencyRateHome glFunctionalCurrencyRateHome = null;
	    LocalAdPaymentTermHome adPaymentTermHome = null;
	    LocalArCustomerHome arCustomerHome = null;
	    LocalArTaxCodeHome arTaxCodeHome = null;
	    LocalArSalespersonHome arSalespersonHome = null;
	    LocalInvItemLocationHome invItemLocationHome = null;
	    LocalInvUnitOfMeasureHome invUnitOfMeasureHome = null;
	    LocalAdPreferenceHome adPreferenceHome = null;
	    LocalAdApprovalHome adApprovalHome = null;
	    LocalAdAmountLimitHome adAmountLimitHome = null;
	    LocalAdApprovalUserHome adApprovalUserHome = null;
        LocalAdApprovalQueueHome adApprovalQueueHome = null;
	    LocalAdCompanyHome adCompanyHome = null;
	    LocalArInvoiceHome arInvoiceHome = null;
	    LocalArInvoicePaymentScheduleHome arInvoicePaymentScheduleHome = null;
	    LocalArJobOrder arJobOrder = null;
	    LocalArJobOrderAssignmentHome arJobOrderAssignmentHome = null;
	    LocalArPersonelHome arPersonelHome = null;
	    LocalArJobOrderTypeHome arJobOrderTypeHome = null;
	    // Initialize EJB Home

	    try {

	        arJobOrderHome = (LocalArJobOrderHome)EJBHomeFactory.
				lookUpLocalHome(LocalArJobOrderHome.JNDI_NAME, LocalArJobOrderHome.class);
	        arJobOrderLineHome = (LocalArJobOrderLineHome)EJBHomeFactory.
				lookUpLocalHome(LocalArJobOrderLineHome.JNDI_NAME, LocalArJobOrderLineHome.class);
	        adDocumentSequenceAssignmentHome = (LocalAdDocumentSequenceAssignmentHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdDocumentSequenceAssignmentHome.JNDI_NAME, LocalAdDocumentSequenceAssignmentHome.class);
	        adBranchDocumentSequenceAssignmentHome = (LocalAdBranchDocumentSequenceAssignmentHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdBranchDocumentSequenceAssignmentHome.JNDI_NAME, LocalAdBranchDocumentSequenceAssignmentHome.class);
	        glFunctionalCurrencyHome = (LocalGlFunctionalCurrencyHome)EJBHomeFactory.
				lookUpLocalHome(LocalGlFunctionalCurrencyHome.JNDI_NAME, LocalGlFunctionalCurrencyHome.class);
	        glFunctionalCurrencyRateHome = (LocalGlFunctionalCurrencyRateHome)EJBHomeFactory.
				lookUpLocalHome(LocalGlFunctionalCurrencyRateHome.JNDI_NAME, LocalGlFunctionalCurrencyRateHome.class);
	        adPaymentTermHome = (LocalAdPaymentTermHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdPaymentTermHome.JNDI_NAME, LocalAdPaymentTermHome.class);
	        arCustomerHome = (LocalArCustomerHome)EJBHomeFactory.
				lookUpLocalHome(LocalArCustomerHome.JNDI_NAME, LocalArCustomerHome.class);
	        arTaxCodeHome = (LocalArTaxCodeHome)EJBHomeFactory.
				lookUpLocalHome(LocalArTaxCodeHome.JNDI_NAME, LocalArTaxCodeHome.class);
	        arSalespersonHome = (LocalArSalespersonHome)EJBHomeFactory.
				lookUpLocalHome(LocalArSalespersonHome.JNDI_NAME, LocalArSalespersonHome.class);
	        invItemLocationHome = (LocalInvItemLocationHome)EJBHomeFactory.
				lookUpLocalHome(LocalInvItemLocationHome.JNDI_NAME, LocalInvItemLocationHome.class);
	        invUnitOfMeasureHome = (LocalInvUnitOfMeasureHome)EJBHomeFactory.
				lookUpLocalHome(LocalInvUnitOfMeasureHome.JNDI_NAME, LocalInvUnitOfMeasureHome.class);
			adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);
			adApprovalHome = (LocalAdApprovalHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdApprovalHome.JNDI_NAME, LocalAdApprovalHome.class);
			adAmountLimitHome = (LocalAdAmountLimitHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdAmountLimitHome.JNDI_NAME, LocalAdAmountLimitHome.class);
			adApprovalUserHome = (LocalAdApprovalUserHome)EJBHomeFactory.
    			lookUpLocalHome(LocalAdApprovalUserHome.JNDI_NAME, LocalAdApprovalUserHome.class);
			adApprovalQueueHome = (LocalAdApprovalQueueHome)EJBHomeFactory.
    			lookUpLocalHome(LocalAdApprovalQueueHome.JNDI_NAME, LocalAdApprovalQueueHome.class);
			adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME,LocalAdCompanyHome.class);
			arInvoiceHome = (LocalArInvoiceHome)EJBHomeFactory.
				lookUpLocalHome(LocalArInvoiceHome.JNDI_NAME, LocalArInvoiceHome.class);
			arInvoicePaymentScheduleHome = (LocalArInvoicePaymentScheduleHome)EJBHomeFactory.
				lookUpLocalHome(LocalArInvoicePaymentScheduleHome.JNDI_NAME, LocalArInvoicePaymentScheduleHome.class);
			arJobOrderAssignmentHome = (LocalArJobOrderAssignmentHome)EJBHomeFactory.
					lookUpLocalHome(LocalArJobOrderAssignmentHome.JNDI_NAME, LocalArJobOrderAssignmentHome.class);
			arPersonelHome = (LocalArPersonelHome)EJBHomeFactory.
					lookUpLocalHome(LocalArPersonelHome.JNDI_NAME, LocalArPersonelHome.class);
			arJobOrderTypeHome = (LocalArJobOrderTypeHome)EJBHomeFactory.
					lookUpLocalHome(LocalArJobOrderTypeHome.JNDI_NAME, LocalArJobOrderTypeHome.class);
	    } catch(NamingException ex) {

	        throw new EJBException(ex.getMessage());

	    }

	    try {

	        // validate if sales order is already deleted

	        try {

	            if(details.getJoCode() != null) {

	                arJobOrder = arJobOrderHome.findByPrimaryKey(details.getJoCode());

	            }

	        } catch(FinderException ex) {

	            throw new GlobalRecordAlreadyDeletedException();
	        }

	        // sales order void

			if (details.getJoCode() != null && details.getJoVoid() == EJBCommon.TRUE) {

				Collection arJobOrderLines = arJobOrder.getArJobOrderLines();
				Iterator i = arJobOrderLines.iterator();

				while (i.hasNext()) {

					LocalArJobOrderLine arJobOrderLine = (LocalArJobOrderLine) i.next();

					if (!arJobOrderLine.getArJobOrderInvoiceLines().isEmpty())
						throw new GlobalRecordAlreadyAssignedException();
				}

				arJobOrder.setJoVoid(EJBCommon.TRUE);
				arJobOrder.setJoLastModifiedBy(details.getJoLastModifiedBy());
				arJobOrder.setJoDateLastModified(details.getJoDateLastModified());

				return arJobOrder.getJoCode();

			}

	        // validate if sales order is already posted, void, approved or pending

			if (details.getJoCode() != null) {
				//report Parameter saved
				arJobOrder.setReportParameter(details.getReportParameter());
	        	if (arJobOrder.getJoApprovalStatus() != null) {

	        		if (arJobOrder.getJoApprovalStatus().equals("APPROVED") ||
	        				arJobOrder.getJoApprovalStatus().equals("N/A")) {

	        		    return arJobOrder.getJoCode();

	        		} else if (arJobOrder.getJoApprovalStatus().equals("PENDING")) {

	        			throw new GlobalTransactionAlreadyPendingException();

	        		}
	        	}

        		if (arJobOrder.getJoPosted() == EJBCommon.TRUE) {

        			return arJobOrder.getJoCode();

        		} else if (arJobOrder.getJoVoid() == EJBCommon.TRUE) {

        			throw new GlobalTransactionAlreadyVoidException();

        		}

        	}

			// validate if document number is unique document number is automatic then set next sequence

			LocalArJobOrder arExistingJobOrder = null;

			try {

				arExistingJobOrder = arJobOrderHome.findByJoDocumentNumberAndBrCode(
						details.getJoDocumentNumber(), AD_BRNCH, AD_CMPNY);

			} catch (FinderException ex) {
			}

			LocalAdBranchDocumentSequenceAssignment adBranchDocumentSequenceAssignment = null;
 			LocalAdDocumentSequenceAssignment adDocumentSequenceAssignment = null;

			if (details.getJoCode() == null) {


				String documentType = details.getJoDocumentType();

				try {
					if(documentType != null) {
						adDocumentSequenceAssignment = adDocumentSequenceAssignmentHome.findByDcName(documentType , AD_CMPNY);
					}else {
						documentType = "AR JOB ORDER";
					}
				} catch(FinderException ex) {
					documentType = "AR JOB ORDER";
				}



				try {

					adDocumentSequenceAssignment = adDocumentSequenceAssignmentHome.findByDcName(documentType, AD_CMPNY);


 				} catch (FinderException ex) {

 				}

 				try {

 					adBranchDocumentSequenceAssignment = adBranchDocumentSequenceAssignmentHome.findBdsByDsaCodeAndBrCode(adDocumentSequenceAssignment.getDsaCode(), AD_BRNCH, AD_CMPNY);

 				} catch (FinderException ex) {

 				}

				if (arExistingJobOrder != null) {

					throw new GlobalDocumentNumberNotUniqueException();

				}

				if (adDocumentSequenceAssignment.getAdDocumentSequence().getDsNumberingType() == 'A' &&
						(details.getJoDocumentNumber() == null || details.getJoDocumentNumber().trim().length() == 0)) {

					while (true) {

						if (adBranchDocumentSequenceAssignment == null || adBranchDocumentSequenceAssignment.getBdsNextSequence() == null) {

							try {

							    arJobOrderHome.findByJoDocumentNumberAndBrCode(adDocumentSequenceAssignment.getDsaNextSequence(), AD_BRNCH, AD_CMPNY);
								adDocumentSequenceAssignment.setDsaNextSequence(EJBCommon.incrementStringNumber(adDocumentSequenceAssignment.getDsaNextSequence()));

							} catch (FinderException ex) {

								details.setJoDocumentNumber(adDocumentSequenceAssignment.getDsaNextSequence());
								adDocumentSequenceAssignment.setDsaNextSequence(EJBCommon.incrementStringNumber(adDocumentSequenceAssignment.getDsaNextSequence()));
								break;

							}

						} else {

							try {

								arJobOrderHome.findByJoDocumentNumberAndBrCode(adBranchDocumentSequenceAssignment.getBdsNextSequence(), AD_BRNCH, AD_CMPNY);
								adBranchDocumentSequenceAssignment.setBdsNextSequence(EJBCommon.incrementStringNumber(adBranchDocumentSequenceAssignment.getBdsNextSequence()));

							} catch (FinderException ex) {

								details.setJoDocumentNumber(adBranchDocumentSequenceAssignment.getBdsNextSequence());
								adBranchDocumentSequenceAssignment.setBdsNextSequence(EJBCommon.incrementStringNumber(adBranchDocumentSequenceAssignment.getBdsNextSequence()));
								break;

							}

						}

					}

				}

			} else {

				if (arExistingJobOrder != null &&
						!arExistingJobOrder.getJoCode().equals(details.getJoCode())) {

					throw new GlobalDocumentNumberNotUniqueException();

				}

				if (arJobOrder.getJoDocumentNumber() != details.getJoDocumentNumber() &&
						(details.getJoDocumentNumber() == null || details.getJoDocumentNumber().trim().length() == 0)) {

					details.setJoDocumentNumber(arJobOrder.getJoDocumentNumber());

				}

			}

			// validate if conversion date exists

			try {

				if (details.getJoConversionDate() != null) {

					LocalGlFunctionalCurrency glValidateFunctionalCurrency = glFunctionalCurrencyHome.findByFcName(FC_NM, AD_CMPNY);
					LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);

					if (!glValidateFunctionalCurrency.getFcName().equals("USD")) {

						LocalGlFunctionalCurrencyRate glFunctionalCurrencyRate =
							glFunctionalCurrencyRateHome.findByFcCodeAndDate(glValidateFunctionalCurrency.getFcCode(),
									details.getJoConversionDate(), AD_CMPNY);

					} else if (!adCompany.getGlFunctionalCurrency().getFcName().equals("USD")){

						LocalGlFunctionalCurrencyRate glFunctionalCurrencyRate =
							glFunctionalCurrencyRateHome.findByFcCodeAndDate(
									adCompany.getGlFunctionalCurrency().getFcCode(), details.getJoConversionDate(), AD_CMPNY);

					}

				}

			} catch (FinderException ex) {

				throw new GlobalConversionDateNotExistException();

			}

			// validate if payment term has at least one payment schedule

			if (adPaymentTermHome.findByPytName(PYT_NM, AD_CMPNY).getAdPaymentSchedules().isEmpty()) {

				throw new GlobalPaymentTermInvalidException();

			}

			boolean isRecalculate = true;

			// create sales order
			System.out.println("1");
			if (details.getJoCode() == null) {

				arJobOrder = arJobOrderHome.create(details.getJoDate(), details.getJoDocumentNumber(),
		                details.getJoReferenceNumber(), details.getJoTransactionType(), details.getJoDescription(), details.getJoBillTo(),
		                details.getJoShipTo(), details.getJoTechnician(), details.getJoConversionDate(), details.getJoConversionRate(),
		                EJBCommon.FALSE, details.getJoMobile(),  null, EJBCommon.FALSE, null, details.getJoCreatedBy(),
		                details.getJoDateCreated(), details.getJoLastModifiedBy(), details.getJoDateLastModified(),
		                null, null, null, null, EJBCommon.FALSE, EJBCommon.FALSE, details.getJoMemo(), details.getJoJobOrderStatus(), AD_BRNCH, AD_CMPNY);

			} else {

				// check if critical fields are changed
				System.out.println("2");
				if (!arJobOrder.getArTaxCode().getTcName().equals(TC_NM) ||
						!arJobOrder.getArCustomer().getCstCustomerCode().equals(CST_CSTMR_CODE) ||
						!arJobOrder.getAdPaymentTerm().getPytName().equals(PYT_NM) ||
						jolList.size() != arJobOrder.getArJobOrderLines().size()) {

					isRecalculate = true;
					System.out.println("3");
				} else if (jolList.size() == arJobOrder.getArJobOrderLines().size()) {
					System.out.println("4");
					Iterator ilIter = arJobOrder.getArJobOrderLines().iterator();
					Iterator ilListIter = jolList.iterator();

					while (ilIter.hasNext()) {

						LocalArJobOrderLine arJobOrderLine = (LocalArJobOrderLine)ilIter.next();
						ArModJobOrderLineDetails mdetails = (ArModJobOrderLineDetails)ilListIter.next();
						System.out.println("4a");
						System.out.println(arJobOrderLine.getInvItemLocation().getInvItem().getIiName()+"==="+mdetails.getJolIiName());
			            System.out.println(arJobOrderLine.getInvItemLocation().getInvItem().getIiDescription()+"==="+mdetails.getJolIiDescription());
			            System.out.println(arJobOrderLine.getInvItemLocation().getInvLocation().getLocName()+"==="+mdetails.getJolLocName());
			            System.out.println(arJobOrderLine.getInvUnitOfMeasure().getUomName()+"==="+mdetails.getJolUomName());
			            System.out.println(arJobOrderLine.getJolQuantity()+"==="+mdetails.getJolQuantity());
			            System.out.println(arJobOrderLine.getJolUnitPrice()+"==="+mdetails.getJolUnitPrice());

			            if (!arJobOrderLine.getInvItemLocation().getInvItem().getIiName().equals(mdetails.getJolIiName()) ||
			                !arJobOrderLine.getInvItemLocation().getInvItem().getIiDescription().equals(mdetails.getJolIiDescription()) ||
			                !arJobOrderLine.getInvItemLocation().getInvLocation().getLocName().equals(mdetails.getJolLocName()) ||
			                !arJobOrderLine.getInvUnitOfMeasure().getUomName().equals(mdetails.getJolUomName()) ||
			                arJobOrderLine.getJolQuantity() != mdetails.getJolQuantity() ||
			                arJobOrderLine.getJolUnitPrice() != mdetails.getJolUnitPrice() ||
			                arJobOrderLine.getJolTax() != mdetails.getJolTax() ||
			                arJobOrderLine.getJolMisc() != null && (!arJobOrderLine.getJolMisc().equals(mdetails.getJolMisc()))
								) {

							isRecalculate = true;

						//	mdetails.getJaList().clear();
							System.out.println("4b");
							break;

						}
						System.out.println("4c");
					//	isRecalculate = false;

					}
					System.out.println("4d");
				} else {
					System.out.println("4");
				//	isRecalculate = false;

				}

				arJobOrder.setJoDate(details.getJoDate());
		        arJobOrder.setJoDocumentType(details.getJoDocumentType());
		        arJobOrder.setJoDocumentNumber(details.getJoDocumentNumber());
		        arJobOrder.setJoReferenceNumber(details.getJoReferenceNumber());
		        arJobOrder.setJoTransactionType(details.getJoTransactionType());
		        arJobOrder.setJoTransactionType(details.getJoTransactionType());
		        arJobOrder.setJoDescription(details.getJoDescription());
		        arJobOrder.setJoBillTo(details.getJoBillTo());
		        arJobOrder.setJoShipTo(details.getJoShipTo());
		        arJobOrder.setJoTechnician(details.getJoTechnician());
		        arJobOrder.setJoVoid(details.getJoVoid( ));
		        arJobOrder.setJoMobile(details.getJoMobile());
		        arJobOrder.setJoConversionDate(details.getJoConversionDate());
		        arJobOrder.setJoConversionRate(details.getJoConversionRate());
		        arJobOrder.setJoLastModifiedBy(details.getJoLastModifiedBy());
		        arJobOrder.setJoDateLastModified(details.getJoDateLastModified());
		        arJobOrder.setJoReasonForRejection(null);
		        arJobOrder.setJoMemo(details.getJoMemo());
		        arJobOrder.setJoJobOrderStatus(details.getJoJobOrderStatus());

			}

			LocalArJobOrderType arJobOrderType = arJobOrderTypeHome.findByJotName(details.getJoType(), AD_CMPNY);

			arJobOrder.setArJobOrderType(arJobOrderType);


			arJobOrder.setJoDocumentType(details.getJoDocumentType());

			arJobOrder.setReportParameter(details.getReportParameter());

			LocalArCustomer arCustomer = arCustomerHome.findByCstCustomerCode(CST_CSTMR_CODE, AD_CMPNY);


			arJobOrder.setArCustomer(arCustomer);

			LocalAdPaymentTerm adPaymentTerm = adPaymentTermHome.findByPytName(PYT_NM, AD_CMPNY);


			arJobOrder.setAdPaymentTerm(adPaymentTerm);

			LocalArTaxCode arTaxCode = arTaxCodeHome.findByTcName(TC_NM, AD_CMPNY);


			arJobOrder.setArTaxCode(arTaxCode);

			LocalGlFunctionalCurrency glFunctionalCurrency = glFunctionalCurrencyHome.findByFcName(FC_NM, AD_CMPNY);


			arJobOrder.setGlFunctionalCurrency(glFunctionalCurrency);

			LocalArSalesperson arSalesperson = details.getJoSlpSalespersonCode() == null ? null :
				arSalespersonHome.findBySlpSalespersonCode(details.getJoSlpSalespersonCode(), AD_CMPNY);

			if (arSalesperson != null)
			arJobOrder.setArSalesperson(arSalesperson);

			double ABS_TOTAL_AMOUNT = 0d;
			System.out.println("5");
			if(isRecalculate) {

			    // remove all sales order line items

				Collection arJobOrderLines = arJobOrder.getArJobOrderLines();


				Iterator i = arJobOrderLines.iterator();
				System.out.println("6");
				while (i.hasNext()) {

					LocalArJobOrderLine arJobOrderLine = (LocalArJobOrderLine)i.next();

					Collection invTags = arJobOrderLine.getInvTags();

		  	   	    Iterator x = invTags.iterator();

		  	   	    while (x.hasNext()){

		  	   	    	LocalInvTag invTag = (LocalInvTag)x.next();

		  	   	    	x.remove();

		  	   	    	invTag.remove();
		  	   	    }



					System.out.println("7");
					i.remove();

					arJobOrderLine.remove();
					System.out.println("8");
				}

				// add new purchase order line item

				i = jolList.iterator();

				LocalInvItemLocation invItemLocation = null;

				while (i.hasNext()) {

					ArModJobOrderLineDetails mJolDetails = (ArModJobOrderLineDetails) i.next();


					LocalArJobOrderLine arJobOrderLine = arJobOrderLineHome.create(
							mJolDetails.getJolLine(), mJolDetails.getJolLineIDesc(), mJolDetails.getJolQuantity(),
							mJolDetails.getJolUnitPrice(), mJolDetails.getJolAmount(), mJolDetails.getJolDiscount1(),
							mJolDetails.getJolDiscount2(),mJolDetails.getJolDiscount3(), mJolDetails.getJolDiscount4(),
							mJolDetails.getJolTotalDiscount(), 0d, mJolDetails.getJolMisc(), mJolDetails.getJolTax(), AD_CMPNY);



					arJobOrderLine.setArJobOrder(arJobOrder);

				//	Collection jaList = arJobOrderAssignmentHome.findByJolCodeAndJaSo(mJolDetails.getJolCode(), EJBCommon.TRUE, AD_CMPNY);
					ArrayList jaList = mJolDetails.getJaList();

					System.out.println("1 ja size is: " + mJolDetails.getJaList().size());
					System.out.println("1 ja size is: " + mJolDetails.getJaList().size());
					Iterator ji = jaList.iterator();



					while(ji.hasNext()) {

						ArModJobOrderAssignmentDetails jaDetails = (ArModJobOrderAssignmentDetails)ji.next();


						LocalArJobOrderAssignment arJobOrderAssignment = arJobOrderAssignmentHome.create(jaDetails.getJaLine(), jaDetails.getJaRemarks(), jaDetails.getJaQuantity(), jaDetails.getJaUnitCost(), jaDetails.getJaAmount(), jaDetails.getJaSo(), AD_CMPNY);

						LocalArPersonel arPersonel = arPersonelHome.findByPeIdNumber(jaDetails.getJaPeIdNumber(), AD_CMPNY);

						arJobOrderAssignment.setArPersonel(arPersonel);

						arJobOrderAssignment.setArJobOrderLine(arJobOrderLine);

					}

					try {

						invItemLocation = invItemLocationHome.findByLocNameAndIiName(
								mJolDetails.getJolLocName(), mJolDetails.getJolIiName(), AD_CMPNY);

					} catch (FinderException ex) {

						throw new GlobalInvItemLocationNotFoundException(String.valueOf(mJolDetails.getJolLine()));

					}

					ABS_TOTAL_AMOUNT += arJobOrderLine.getJolAmount();


					arJobOrderLine.setInvItemLocation(invItemLocation);

					LocalInvUnitOfMeasure invUnitOfMeasure = invUnitOfMeasureHome.findByUomName(
							mJolDetails.getJolUomName(), AD_CMPNY);



					arJobOrderLine.setInvUnitOfMeasure(invUnitOfMeasure);

					if (arJobOrderLine.getInvItemLocation().getInvItem().getIiTraceMisc() == 1){
						this.createInvTagList(arJobOrderLine,  mJolDetails.getJolTagList(), AD_CMPNY);

					}



				}

			}

			// ORDER STATUS

			Iterator invcIter = arCustomer.getArInvoices().iterator();
			//arSalesOrder.setSoOrderStatus("Good");
			String OrderStatus="";

			while(invcIter.hasNext()){

				LocalArInvoice arInvoice = (LocalArInvoice)invcIter.next();

				if(arInvoice.getInvPosted() == EJBCommon.FALSE)
					continue;

				Iterator ipsIter = arInvoice.getArInvoicePaymentSchedules().iterator();

				while(ipsIter.hasNext()){

					LocalArInvoicePaymentSchedule ips = (LocalArInvoicePaymentSchedule)ipsIter.next();

					try{
						if(ips.getIpsAmountPaid()==0 && OrderStatus.indexOf("Bad1")<0){
							OrderStatus="Bad1";
						}

						if(ips.getIpsAmountPaid()<ips.getIpsAmountDue() && OrderStatus.indexOf("Bad2")<0){

							if(OrderStatus!="")
								OrderStatus=OrderStatus+"/";

							OrderStatus=OrderStatus+"Bad2";

						}
						int result = 0;

						try{
							result = ips.getIpsDueDate().compareTo(((LocalArAppliedInvoice)ips.getArAppliedInvoices().toArray()[0]).getArReceipt().getRctDate());
							System.out.print("HERE IS: " +result);
						}catch(Exception ex){
							ex.printStackTrace();
						}


						if (result==-1 && OrderStatus.indexOf("Bad3")<0){
							if(OrderStatus!="")
								OrderStatus=OrderStatus+"/";

							OrderStatus=OrderStatus+"Bad3";
						}

					}catch(Exception ex){
						ex.printStackTrace();
						arJobOrder.setJoOrderStatus(OrderStatus);
					}

				}
			}

			if(OrderStatus==""){
				OrderStatus="Good";
			}

			arJobOrder.setJoOrderStatus(OrderStatus);


			// set sales order approval status

			String SO_APPRVL_STATUS = null;

			if(!isDraft) {

				LocalAdApproval adApproval = adApprovalHome.findByAprAdCompany(AD_CMPNY);

				// check if ar sales order approval is enabled

				if (adApproval.getAprEnableArSalesOrder() == EJBCommon.FALSE) {

					SO_APPRVL_STATUS = "N/A";

				} else {

					// check if sales order is self approved

					LocalAdAmountLimit adAmountLimit = null;

					try {

						adAmountLimit = adAmountLimitHome.findByAdcTypeAndAuTypeAndUsrName("AR SALES ORDER", "REQUESTER", details.getJoLastModifiedBy(), AD_CMPNY);

					} catch (FinderException ex) {

						throw new GlobalNoApprovalRequesterFoundException();

					}

					if (ABS_TOTAL_AMOUNT <= adAmountLimit.getCalAmountLimit()) {

						SO_APPRVL_STATUS = "N/A";

					} else {

						// for approval, create approval queue

						Collection adAmountLimits = adAmountLimitHome.findByAdcTypeAndGreaterThanCalAmountLimit("AR SALES ORDER", adAmountLimit.getCalAmountLimit(), AD_CMPNY);

						if (adAmountLimits.isEmpty()) {

							Collection adApprovalUsers = adApprovalUserHome.findByAuTypeAndCalCode("APPROVER", adAmountLimit.getCalCode(), AD_CMPNY);

							if (adApprovalUsers.isEmpty()) {

								throw new GlobalNoApprovalApproverFoundException();

							}

							Iterator j = adApprovalUsers.iterator();

							while (j.hasNext()) {

								LocalAdApprovalUser adApprovalUser = (LocalAdApprovalUser)j.next();

								LocalAdApprovalQueue adApprovalQueue = adApprovalQueueHome.create(EJBCommon.TRUE, "AR SALES ORDER", arJobOrder.getJoCode(),
										arJobOrder.getJoDocumentNumber(), arJobOrder.getJoDate(), adAmountLimit.getCalAndOr(), adApprovalUser.getAuOr(), AD_BRNCH, AD_CMPNY);

								adApprovalUser.getAdUser().addAdApprovalQueue(adApprovalQueue);

							}

						} else {

							boolean isApprovalUsersFound = false;

							Iterator i = adAmountLimits.iterator();

							while (i.hasNext()) {

								LocalAdAmountLimit adNextAmountLimit = (LocalAdAmountLimit)i.next();

								if (ABS_TOTAL_AMOUNT <= adNextAmountLimit.getCalAmountLimit()) {

									Collection adApprovalUsers = adApprovalUserHome.findByAuTypeAndCalCode("APPROVER", adAmountLimit.getCalCode(), AD_CMPNY);

									Iterator j = adApprovalUsers.iterator();

									while (j.hasNext()) {

										isApprovalUsersFound = true;

										LocalAdApprovalUser adApprovalUser = (LocalAdApprovalUser)j.next();

										LocalAdApprovalQueue adApprovalQueue = adApprovalQueueHome.create(EJBCommon.TRUE, "AR SALES ORDER", arJobOrder.getJoCode(),
												arJobOrder.getJoDocumentNumber(), arJobOrder.getJoDate(), adAmountLimit.getCalAndOr(), adApprovalUser.getAuOr(), AD_BRNCH, AD_CMPNY);

										adApprovalUser.getAdUser().addAdApprovalQueue(adApprovalQueue);

									}

									break;

								} else if (!i.hasNext()) {

									Collection adApprovalUsers = adApprovalUserHome.findByAuTypeAndCalCode("APPROVER", adNextAmountLimit.getCalCode(), AD_CMPNY);

									Iterator j = adApprovalUsers.iterator();

									while (j.hasNext()) {

										isApprovalUsersFound = true;

										LocalAdApprovalUser adApprovalUser = (LocalAdApprovalUser)j.next();

										LocalAdApprovalQueue adApprovalQueue = adApprovalQueueHome.create(EJBCommon.TRUE, "AR SALES ORDER", arJobOrder.getJoCode(),
												arJobOrder.getJoDocumentNumber(), arJobOrder.getJoDate(), adNextAmountLimit.getCalAndOr(), adApprovalUser.getAuOr(), AD_BRNCH, AD_CMPNY);

										adApprovalUser.getAdUser().addAdApprovalQueue(adApprovalQueue);

									}

									break;

								}

								adAmountLimit = adNextAmountLimit;

							}

							if (!isApprovalUsersFound) {

								throw new GlobalNoApprovalApproverFoundException();

							}

						}

						SO_APPRVL_STATUS = "PENDING";

					}

				}

				arJobOrder.setJoApprovalStatus(SO_APPRVL_STATUS);





				// set post purchase order

				if(SO_APPRVL_STATUS.equals("N/A")) {

					arJobOrder.setJoPosted(EJBCommon.TRUE);
					arJobOrder.setJoPosted(EJBCommon.TRUE);
					arJobOrder.setJoOrderStatus("Good");
					arJobOrder.setJoPostedBy(arJobOrder.getJoLastModifiedBy());
					arJobOrder.setJoDatePosted(EJBCommon.getGcCurrentDateWoTime().getTime());

				}
				//arSalesOrder.setSoOrderStatus("Good");

			}

			//REFERENCE NUMBER GENERATOR
			LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
			String newReferenceNumber = this.createCodedReferenceNumber(arJobOrder,adCompany);

			arJobOrder.setJoReferenceNumber(newReferenceNumber);

 	  	    return arJobOrder.getJoCode();

	    } catch (GlobalRecordAlreadyDeletedException ex) {

			getSessionContext().setRollbackOnly();
			throw ex;

		} catch (GlobalDocumentNumberNotUniqueException ex) {

			getSessionContext().setRollbackOnly();
			throw ex;

		} catch (GlobalJournalNotBalanceException ex) {

            getSessionContext().setRollbackOnly();
            throw ex;

        } catch (GlobalConversionDateNotExistException ex) {

			getSessionContext().setRollbackOnly();
			throw ex;

		} catch (GlobalPaymentTermInvalidException ex) {

			getSessionContext().setRollbackOnly();
			throw ex;

		} catch (GlobalTransactionAlreadyPendingException ex) {

			getSessionContext().setRollbackOnly();
			throw ex;

		} catch (GlobalTransactionAlreadyVoidException ex) {

			getSessionContext().setRollbackOnly();
			throw ex;

		} catch (GlobalInvItemLocationNotFoundException ex) {

			getSessionContext().setRollbackOnly();
			throw ex;

		} catch (GlobalNoApprovalApproverFoundException ex) {

			getSessionContext().setRollbackOnly();
			throw ex;

		} catch (GlobalNoApprovalRequesterFoundException ex) {

			getSessionContext().setRollbackOnly();
			throw ex;

		} catch (GlobalRecordAlreadyAssignedException ex) {

			getSessionContext().setRollbackOnly();
			throw ex;

		} catch (Exception ex) {

			Debug.printStackTrace(ex);
			getSessionContext().setRollbackOnly();
			throw new EJBException(ex.getMessage());

		}

	}

	/**
	 * @ejb:interface-method view-type="remote"
	 **/
	public void deleteArJoEntry(Integer JO_CODE, String AD_USR, Integer AD_CMPNY) throws
	GlobalRecordAlreadyDeletedException {

		Debug.print("ArJobOrderEntryControllerBean deleteArSoEntry");

		LocalArJobOrderHome arJobOrderHome = null;
		LocalAdApprovalQueueHome adApprovalQueueHome = null;
		LocalAdDeleteAuditTrailHome adDeleteAuditTrailHome = null;

		// Initialize EJB Home

		try {

			arJobOrderHome = (LocalArJobOrderHome)EJBHomeFactory.
			lookUpLocalHome(LocalArJobOrderHome.JNDI_NAME, LocalArJobOrderHome.class);
            adApprovalQueueHome = (LocalAdApprovalQueueHome)EJBHomeFactory.
            lookUpLocalHome(LocalAdApprovalQueueHome.JNDI_NAME, LocalAdApprovalQueueHome.class);
            adDeleteAuditTrailHome = (LocalAdDeleteAuditTrailHome)EJBHomeFactory.
            			lookUpLocalHome(LocalAdDeleteAuditTrailHome.JNDI_NAME, LocalAdDeleteAuditTrailHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}

		try {

			System.out.println("JO_CODE="+JO_CODE);
			System.out.println("AD_USR="+AD_USR);


		    LocalArJobOrder arJobOrder = arJobOrderHome.findByPrimaryKey(JO_CODE);
			System.out.println("1");
            if (arJobOrder.getJoApprovalStatus() != null && arJobOrder.getJoApprovalStatus().equals("PENDING")) {
            	System.out.println("2");
                Collection adApprovalQueues = adApprovalQueueHome.findByAqDocumentAndAqDocumentCode("AR SALES ORDER",
                		arJobOrder.getJoCode(), AD_CMPNY);

                Iterator i = adApprovalQueues.iterator();

                while(i.hasNext()) {

                    LocalAdApprovalQueue adApprovalQueue = (LocalAdApprovalQueue)i.next();

                    adApprovalQueue.remove();

                }
                System.out.println("3");
            }

            adDeleteAuditTrailHome.create("AR JOB ORDER", arJobOrder.getJoDate(), arJobOrder.getJoDocumentNumber(), arJobOrder.getJoReferenceNumber(),
 					0d, AD_USR, new Date(), AD_CMPNY);
            System.out.println("4"+arJobOrder.getJoCode()+"SO Number"+arJobOrder.getJoDocumentNumber());
            arJobOrder.remove();


		    System.out.println("5");
		} catch (FinderException ex) {

			getSessionContext().setRollbackOnly();
			throw new GlobalRecordAlreadyDeletedException();

		} catch (Exception ex) {

			Debug.printStackTrace(ex);
			getSessionContext().setRollbackOnly();
			throw new EJBException(ex.getMessage());

		}

	}

	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public ArrayList getArCstAll(Integer AD_BRNCH, Integer AD_CMPNY) {

		Debug.print("ArJobOrderEntryControllerBean getApCstAll");

		LocalArCustomerHome arCustomerHome = null;

		ArrayList list = new ArrayList();

		// Initialize EJB Home

		try {

		    arCustomerHome = (LocalArCustomerHome)EJBHomeFactory.
			lookUpLocalHome(LocalArCustomerHome.JNDI_NAME, LocalArCustomerHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}

		try {

			Collection arCustomers = arCustomerHome.findEnabledCstAll(AD_BRNCH, AD_CMPNY);

			Iterator i = arCustomers.iterator();

			while (i.hasNext()) {

				LocalArCustomer arCustomer = (LocalArCustomer)i.next();

				list.add(arCustomer.getCstCustomerCode());

			}

			return list;

		} catch (Exception ex) {

			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());

		}

	}

	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public ArrayList getAdPytAll(Integer AD_CMPNY) {

		Debug.print("ArJobOrderEntryControllerBean getAdPytAll");

		LocalAdPaymentTermHome adPaymentTermHome = null;

		ArrayList list = new ArrayList();

		// Initialize EJB Home

		try {

			adPaymentTermHome = (LocalAdPaymentTermHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdPaymentTermHome.JNDI_NAME, LocalAdPaymentTermHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}

		try {

			Collection adPaymentTerms = adPaymentTermHome.findEnabledPytAll(AD_CMPNY);

			Iterator i = adPaymentTerms.iterator();

			while (i.hasNext()) {

				LocalAdPaymentTerm adPaymentTerm = (LocalAdPaymentTerm)i.next();

				list.add(adPaymentTerm.getPytName());

			}

			return list;

		} catch (Exception ex) {

			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());

		}

	}



    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList getAdUsrAll(Integer AD_CMPNY) {

        Debug.print("ArJobOrderEntryControllerBean getAdUsrAll");

        LocalAdUserHome adUserHome = null;

        LocalAdUser adUser = null;

        Collection adUsers = null;

        ArrayList list = new ArrayList();

        // Initialize EJB Home

        try {

            adUserHome = (LocalAdUserHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdUserHome.JNDI_NAME, LocalAdUserHome.class);

        } catch (NamingException ex) {

            throw new EJBException(ex.getMessage());

        }

        try {

            adUsers = adUserHome.findUsrAll(AD_CMPNY);

        } catch (FinderException ex) {

        } catch (Exception ex) {

            throw new EJBException(ex.getMessage());
        }

        if (adUsers.isEmpty()) {

            return null;

        }

        Iterator i = adUsers.iterator();

        while (i.hasNext()) {

            adUser = (LocalAdUser)i.next();

            list.add(adUser.getUsrName());

        }

        return list;

    }


	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public ArrayList getArTcAll(Integer AD_CMPNY) {

		Debug.print("ArJobOrderEntryControllerBean getArTcAll");

		LocalArTaxCodeHome arTaxCodeHome = null;

		ArrayList list = new ArrayList();

		// Initialize EJB Home

		try {

			arTaxCodeHome = (LocalArTaxCodeHome)EJBHomeFactory.
			lookUpLocalHome(LocalArTaxCodeHome.JNDI_NAME, LocalArTaxCodeHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}

		try {

			Collection arTaxCodes = arTaxCodeHome.findEnabledTcAll(AD_CMPNY);

			Iterator i = arTaxCodes.iterator();

			while (i.hasNext()) {

				LocalArTaxCode arTaxCode = (LocalArTaxCode)i.next();

        		list.add(arTaxCode.getTcName());

			}

			return list;

		} catch (Exception ex) {

			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());

		}

	}

	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public ArTaxCodeDetails getArTcByTcName(String TC_NM, Integer AD_CMPNY) {

		Debug.print("ArJobOrderEntryControllerBean getArTcByTcName");

		LocalArTaxCodeHome arTaxCodeHome = null;

		ArrayList list = new ArrayList();

		// Initialize EJB Home

		try {

			arTaxCodeHome = (LocalArTaxCodeHome)EJBHomeFactory.
			lookUpLocalHome(LocalArTaxCodeHome.JNDI_NAME, LocalArTaxCodeHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}

		try {

			LocalArTaxCode arTaxCode = arTaxCodeHome.findByTcName(TC_NM, AD_CMPNY);

			ArTaxCodeDetails details = new ArTaxCodeDetails();
			details.setTcType(arTaxCode.getTcType());
			details.setTcRate(arTaxCode.getTcRate());

			return details;

		} catch (Exception ex) {

			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());

		}

	}

	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public ArrayList getGlFcAllWithDefault(Integer AD_CMPNY) {

		Debug.print("ArJobOrderEntryControllerBean getGlFcAllWithDefault");

		LocalGlFunctionalCurrencyHome glFunctionalCurrencyHome = null;
		LocalAdCompanyHome adCompanyHome = null;

		Collection glFunctionalCurrencies = null;

		LocalGlFunctionalCurrency glFunctionalCurrency = null;
		LocalAdCompany adCompany = null;


		ArrayList list = new ArrayList();

		// Initialize EJB Home

		try {

			glFunctionalCurrencyHome = (LocalGlFunctionalCurrencyHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlFunctionalCurrencyHome.JNDI_NAME, LocalGlFunctionalCurrencyHome.class);
			adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}

		try {

			adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);

			glFunctionalCurrencies = glFunctionalCurrencyHome.findFcAllEnabled(
					EJBCommon.getGcCurrentDateWoTime().getTime(), AD_CMPNY);

		} catch (Exception ex) {

			throw new EJBException(ex.getMessage());
		}

		if (glFunctionalCurrencies.isEmpty()) {

			return null;

		}

		Iterator i = glFunctionalCurrencies.iterator();

		while (i.hasNext()) {

			glFunctionalCurrency = (LocalGlFunctionalCurrency)i.next();

			GlModFunctionalCurrencyDetails mdetails = new GlModFunctionalCurrencyDetails(
					glFunctionalCurrency.getFcCode(), glFunctionalCurrency.getFcName(),
					adCompany.getGlFunctionalCurrency().getFcName().equals(glFunctionalCurrency.getFcName()) ?
							EJBCommon.TRUE : EJBCommon.FALSE);

			list.add(mdetails);

		}

		return list;

	}

	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public ArrayList getInvLocAll(Integer AD_CMPNY) {

		Debug.print("ArJobOrderEntryControllerBean getInvLocAll");

		LocalInvLocationHome invLocationHome = null;
		Collection invLocations = null;
		ArrayList list = new ArrayList();

		// Initialize EJB Home

		try {

			invLocationHome = (LocalInvLocationHome)EJBHomeFactory.
			lookUpLocalHome(LocalInvLocationHome.JNDI_NAME, LocalInvLocationHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}

		try {

			invLocations = invLocationHome.findLocAll(AD_CMPNY);

			if (invLocations.isEmpty()) {

				return null;

			}

			Iterator i = invLocations.iterator();

			while (i.hasNext()) {

				LocalInvLocation invLocation = (LocalInvLocation)i.next();
				String details = invLocation.getLocName();

				list.add(details);

			}

			return list;

		} catch (Exception ex) {

			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());

		}

	}

	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public short getGlFcPrecisionUnit(Integer AD_CMPNY) {

		Debug.print("ArJobOrderEntryControllerBean getGlFcPrecisionUnit");


		LocalAdCompanyHome adCompanyHome = null;


		// Initialize EJB Home

		try {

			adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}

		try {

			LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);

			return  adCompany.getGlFunctionalCurrency().getFcPrecision();

		} catch (Exception ex) {

			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());

		}

	}

	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public short getInvGpQuantityPrecisionUnit(Integer AD_CMPNY) {

		Debug.print("ArJobOrderEntryControllerBean getInvGpQuantityPrecisionUnit");

		LocalAdPreferenceHome adPreferenceHome = null;

		// Initialize EJB Home

		try {

			adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}


		try {

			LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);

			return adPreference.getPrfInvQuantityPrecisionUnit();

		} catch (Exception ex) {

			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());

		}

	}

	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public short getAdPrfArJournalLineNumber(Integer AD_CMPNY) {

		Debug.print("ArJobEntryControllerBean getAdPrfArJournalLineNumber");

		LocalAdPreferenceHome adPreferenceHome = null;


		// Initialize EJB Home

		try {

			adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}


		try {

			LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);

			return adPreference.getPrfArInvoiceLineNumber();

		} catch (Exception ex) {

			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());

		}

	}

	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public ArModJobOrderDetails getArJoBySoCode(Integer JO_CODE, Integer AD_CMPNY)
	throws GlobalNoRecordFoundException {

		Debug.print("ArJobOrderEntryControllerBean getArSoBySoCode");

		LocalArJobOrderHome arJobOrderHome = null;

		// Initialize EJB Home

		try {

			arJobOrderHome = (LocalArJobOrderHome)EJBHomeFactory.
					lookUpLocalHome(LocalArJobOrderHome.JNDI_NAME, LocalArJobOrderHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}

		try {

			LocalArJobOrder arJobOrder = null;


			try {

				arJobOrder = arJobOrderHome.findByPrimaryKey(JO_CODE);

			} catch (FinderException ex) {

				throw new GlobalNoRecordFoundException();

			}

			ArrayList list = new ArrayList();

			// get sales order lines if any

			Collection arJobOrderLines = arJobOrder.getArJobOrderLines();

			Iterator i = arJobOrderLines.iterator();

			while (i.hasNext()) {

				LocalArJobOrderLine arJobOrderLine = (LocalArJobOrderLine) i.next();

				ArModJobOrderLineDetails jolDetails = new ArModJobOrderLineDetails();

				jolDetails.setJolCode(arJobOrderLine.getJolCode());
		        jolDetails.setJolLine(arJobOrderLine.getJolLine());
		        jolDetails.setJolIiName(arJobOrderLine.getInvItemLocation().getInvItem().getIiName());
		        jolDetails.setJolLocName(arJobOrderLine.getInvItemLocation().getInvLocation().getLocName());
		        jolDetails.setJolQuantity(arJobOrderLine.getJolQuantity());
		        jolDetails.setJolUomName(arJobOrderLine.getInvUnitOfMeasure().getUomName());
		        jolDetails.setJolUnitPrice(arJobOrderLine.getJolUnitPrice());
		        jolDetails.setJolIiDescription(arJobOrderLine.getInvItemLocation().getInvItem().getIiDescription());
		        jolDetails.setJolEDesc(arJobOrderLine.getJolLineIDesc());
		        jolDetails.setJolDiscount1(arJobOrderLine.getJolDiscount1());
		        jolDetails.setJolDiscount2(arJobOrderLine.getJolDiscount2());
		        jolDetails.setJolDiscount3(arJobOrderLine.getJolDiscount3());
		        jolDetails.setJolDiscount4(arJobOrderLine.getJolDiscount4());
		        jolDetails.setJolTotalDiscount(arJobOrderLine.getJolTotalDiscount());
		        jolDetails.setJolMisc(arJobOrderLine.getJolMisc());
		        jolDetails.setJolAmount(arJobOrderLine.getJolAmount());
		        jolDetails.setJolTax(arJobOrderLine.getJolTax());
		        jolDetails.setJolIiServices(arJobOrderLine.getInvItemLocation().getInvItem().getIiServices()==EJBCommon.TRUE?true:false);
		        jolDetails.setJolIiJobServices(arJobOrderLine.getInvItemLocation().getInvItem().getIiJobServices()==EJBCommon.TRUE?true:false);
		        jolDetails.setTraceMisc(arJobOrderLine.getInvItemLocation().getInvItem().getIiTraceMisc());
		        
		        System.out.println("job services is : "  + arJobOrderLine.getInvItemLocation().getInvItem().getIiJobServices());
		            System.out.println(jolDetails.getTraceMisc() + "<== getTraceMisc under getInvAdjByAdjCode controllerbean");
		            if (jolDetails.getTraceMisc() == 1){

		              ArrayList tagList = new ArrayList();

		              tagList = this.getInvTagList(arJobOrderLine);
		              jolDetails.setJolTagList(tagList);
		              jolDetails.setTraceMisc(jolDetails.getTraceMisc());


	            }

		        double totalHours = 0d;
		        double totalRatePerHours = 0d;

		        Iterator jai = arJobOrderLine.getArJobOrderAssignments().iterator();

		        while(jai.hasNext()) {

		        	LocalArJobOrderAssignment arJobOrderAssignment = (LocalArJobOrderAssignment)jai.next();

		        	ArModJobOrderAssignmentDetails jaDetails = new ArModJobOrderAssignmentDetails();
		        	jaDetails.setJaCode(jaDetails.getJaCode());
					jaDetails.setJaLine(arJobOrderAssignment.getJaLine());
					jaDetails.setJaRemarks(arJobOrderAssignment.getJaRemarks());
					jaDetails.setJaPeIdNumber(arJobOrderAssignment.getArPersonel().getPeIdNumber());
					jaDetails.setJaQuantity(arJobOrderAssignment.getJaQuantity());
					jaDetails.setJaUnitCost(arJobOrderAssignment.getJaUnitCost());
					jaDetails.setJaAmount(arJobOrderAssignment.getJaAmount());
					jaDetails.setJaSo(arJobOrderAssignment.getJaSo());
					jaDetails.setJaPeName(arJobOrderAssignment.getArPersonel().getPeName());

					if(arJobOrderAssignment.getJaSo()==EJBCommon.TRUE) {
						totalHours += arJobOrderAssignment.getJaQuantity();
						totalRatePerHours += arJobOrderAssignment.getJaAmount();
					}
					
					
					
					jolDetails.saveJaList(jaDetails);
					
					


		        }

		        jolDetails.setJolTotalHours(totalHours);
		        jolDetails.setJolTotalRatePerHours(totalRatePerHours);



				list.add(jolDetails);

			}

			ArModJobOrderDetails mJoDetails = new ArModJobOrderDetails();

			mJoDetails.setJoType(arJobOrder.getArJobOrderType()!=null?arJobOrder.getArJobOrderType().getJotName():"");
			mJoDetails.setJoCode(arJobOrder.getJoCode());
		      mJoDetails.setJoDate(arJobOrder.getJoDate());
		      mJoDetails.setJoDocumentType(arJobOrder.getJoDocumentType());
		      mJoDetails.setJoDocumentNumber(arJobOrder.getJoDocumentNumber());
		      mJoDetails.setJoReferenceNumber(arJobOrder.getJoReferenceNumber());
		      mJoDetails.setJoTransactionType(arJobOrder.getJoTransactionType());
		      mJoDetails.setJoDescription(arJobOrder.getJoDescription());
		      mJoDetails.setJoVoid(arJobOrder.getJoVoid());
		      mJoDetails.setJoMobile(arJobOrder.getJoMobile());
		      mJoDetails.setJoBillTo(arJobOrder.getJoBillTo());
		      mJoDetails.setJoShipTo(arJobOrder.getJoShipTo());
		      mJoDetails.setJoTechnician(arJobOrder.getJoTechnician());
		      mJoDetails.setJoConversionDate(arJobOrder.getJoConversionDate());
		      mJoDetails.setJoConversionRate(arJobOrder.getJoConversionRate());
		      mJoDetails.setJoApprovalStatus(arJobOrder.getJoApprovalStatus());
		      mJoDetails.setJoPosted(arJobOrder.getJoPosted());
		      mJoDetails.setJoCreatedBy(arJobOrder.getJoCreatedBy());
		      mJoDetails.setJoDateCreated(arJobOrder.getJoDateCreated());
		      mJoDetails.setJoLastModifiedBy(arJobOrder.getJoLastModifiedBy());
		      mJoDetails.setJoDateLastModified(arJobOrder.getJoDateLastModified());
		      mJoDetails.setJoApprovedRejectedBy(arJobOrder.getJoApprovedRejectedBy());
		      mJoDetails.setJoDateApprovedRejected(arJobOrder.getJoDateApprovedRejected());
		      mJoDetails.setJoPostedBy(arJobOrder.getJoPostedBy());
		      mJoDetails.setJoDatePosted(arJobOrder.getJoDatePosted());
		      mJoDetails.setJoReasonForRejection(arJobOrder.getJoReasonForRejection());
		      mJoDetails.setJoCstCustomerCode(arJobOrder.getArCustomer().getCstCustomerCode());
		      mJoDetails.setJoCstName(arJobOrder.getArCustomer().getCstName());
		      mJoDetails.setJoPytName(arJobOrder.getAdPaymentTerm().getPytName());
		      mJoDetails.setJoTcName(arJobOrder.getArTaxCode().getTcName());
		      mJoDetails.setJoFcName(arJobOrder.getGlFunctionalCurrency().getFcName());
		          mJoDetails.setJoTcRate(arJobOrder.getArTaxCode().getTcRate());
		          mJoDetails.setJoTcType(arJobOrder.getArTaxCode().getTcType());
		          mJoDetails.setJoApprovedRejectedBy(arJobOrder.getJoApprovedRejectedBy());
		          mJoDetails.setJoDateApprovedRejected(arJobOrder.getJoDateApprovedRejected());
		          mJoDetails.setJoMemo(arJobOrder.getJoMemo());
		          
		          mJoDetails.setJoJobOrderStatus(arJobOrder.getJoJobOrderStatus());
		          mJoDetails.setJoOrderStatus(arJobOrder.getJoOrderStatus());
		          mJoDetails.setReportParameter(arJobOrder.getReportParameter());

		      if (arJobOrder.getArSalesperson() != null){

		        mJoDetails.setJoSlpSalespersonCode(arJobOrder.getArSalesperson().getSlpSalespersonCode());
		        mJoDetails.setJoSlpName(arJobOrder.getArSalesperson().getSlpName());

		      }


			//get So Advance if have
			double JO_ADVNC_AMNT = 0d;


			Iterator i2 = arJobOrder.getCmAdjustments().iterator();


			while(i2.hasNext()){

				LocalCmAdjustment cmAdjustment = (LocalCmAdjustment)i2.next();

				if(cmAdjustment.getAdjVoid()==EJBCommon.FALSE &&
				   cmAdjustment.getAdjReconciled() == EJBCommon.FALSE) {
					JO_ADVNC_AMNT  +=  cmAdjustment.getAdjAmount();
				}



			}


			mJoDetails.setJoAdvanceAmount(JO_ADVNC_AMNT);

			mJoDetails.setJoJolList(list);

			return mJoDetails;

		} catch (GlobalNoRecordFoundException ex) {

			throw ex;

		} catch (Exception ex) {

			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());

		}

	}

	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public ArModCustomerDetails getArCstByCstCustomerCode(String CST_CSTMR_CODE, Integer AD_CMPNY)
	throws GlobalNoRecordFoundException {

		Debug.print("ArJobOrderEntryControllerBean getArCstByCstCustomerCode");

		LocalArCustomerHome arCustomerHome = null;
		LocalArSalespersonHome arSalespersonHome = null;
		// Initialize EJB Home

		try {
			arSalespersonHome = (LocalArSalespersonHome)EJBHomeFactory.
			lookUpLocalHome(LocalArSalespersonHome.JNDI_NAME, LocalArSalespersonHome.class);
		    arCustomerHome = (LocalArCustomerHome)EJBHomeFactory.
			lookUpLocalHome(LocalArCustomerHome.JNDI_NAME, LocalArCustomerHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}

		try {

			LocalArCustomer arCustomer = null;


			try {

				arCustomer = arCustomerHome.findByCstCustomerCode(CST_CSTMR_CODE, AD_CMPNY);

			} catch (FinderException ex) {

				throw new GlobalNoRecordFoundException();

			}

			ArModCustomerDetails mdetails = new ArModCustomerDetails();

			mdetails.setCstPytName(arCustomer.getAdPaymentTerm() != null ?
					arCustomer.getAdPaymentTerm().getPytName() : null);
			mdetails.setCstName(arCustomer.getCstName());

			if (arCustomer.getArSalesperson() != null && arCustomer.getCstArSalesperson2() == null) {

				mdetails.setCstSlpSalespersonCode(arCustomer.getArSalesperson().getSlpSalespersonCode());
				mdetails.setCstSlpName(arCustomer.getArSalesperson().getSlpName());

			} else if (arCustomer.getArSalesperson() == null && arCustomer.getCstArSalesperson2() != null) {

				LocalArSalesperson arSalesperson2 = null;
				arSalesperson2 = arSalespersonHome.findByPrimaryKey(arCustomer.getCstArSalesperson2());

				mdetails.setCstSlpSalespersonCode(arSalesperson2.getSlpSalespersonCode());
				mdetails.setCstSlpName(arSalesperson2.getSlpName());

			} if (arCustomer.getArSalesperson() != null && arCustomer.getCstArSalesperson2() != null) {

				mdetails.setCstSlpSalespersonCode(arCustomer.getArSalesperson().getSlpSalespersonCode());
				mdetails.setCstSlpName(arCustomer.getArSalesperson().getSlpName());
				LocalArSalesperson arSalesperson2 = null;
				arSalesperson2 = arSalespersonHome.findByPrimaryKey(arCustomer.getCstArSalesperson2());

				mdetails.setCstSlpSalespersonCode2(arSalesperson2.getSlpSalespersonCode());
				mdetails.setCstSlpName2(arSalesperson2.getSlpName());
			}

			if(arCustomer.getArCustomerClass().getArTaxCode() != null) {

				mdetails.setCstCcTcName(arCustomer.getArCustomerClass().getArTaxCode().getTcName());
				mdetails.setCstCcTcRate(arCustomer.getArCustomerClass().getArTaxCode().getTcRate());
				mdetails.setCstCcTcType(arCustomer.getArCustomerClass().getArTaxCode().getTcType());

			}

			if(arCustomer.getInvLineItemTemplate() != null) {
				mdetails.setCstLitName(arCustomer.getInvLineItemTemplate().getLitName());
			}

			return mdetails;

		} catch (GlobalNoRecordFoundException ex) {

			throw ex;

		} catch (Exception ex) {

			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());

		}

	}

	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public ArrayList getInvUomByIiName(String II_NM, Integer AD_CMPNY) {

		Debug.print("ArSalesOrderEntryControllerBean getInvUomByIiName");

		LocalInvUnitOfMeasureHome invUnitOfMeasureHome = null;
		LocalInvItemHome invItemHome = null;
		ArrayList list = new ArrayList();

		// Initialize EJB Home

		try {

			invUnitOfMeasureHome = (LocalInvUnitOfMeasureHome)EJBHomeFactory.
			lookUpLocalHome(LocalInvUnitOfMeasureHome.JNDI_NAME, LocalInvUnitOfMeasureHome.class);
			invItemHome = (LocalInvItemHome)EJBHomeFactory.
			lookUpLocalHome(LocalInvItemHome.JNDI_NAME, LocalInvItemHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());
		}

		try {

			LocalInvItem invItem = null;
			LocalInvUnitOfMeasure invItemUnitOfMeasure = null;

			invItem = invItemHome.findByIiName(II_NM, AD_CMPNY);
			invItemUnitOfMeasure = invItem.getInvUnitOfMeasure();

			Collection invUnitOfMeasures = null;
			Iterator i = invUnitOfMeasureHome.findByUomAdLvClass(
					invItemUnitOfMeasure.getUomAdLvClass(), AD_CMPNY).iterator();
			while (i.hasNext()) {

				LocalInvUnitOfMeasure invUnitOfMeasure = (LocalInvUnitOfMeasure) i.next();
				InvModUnitOfMeasureDetails details = new InvModUnitOfMeasureDetails();
				details.setUomName(invUnitOfMeasure.getUomName());

				if (invUnitOfMeasure.getUomName().equals(invItemUnitOfMeasure.getUomName())) {

					details.setDefault(true);

				}

				list.add(details);

			}

			return list;

		} catch (Exception ex) {

			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());

		}

	}

	/**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
     public double getIiSalesPriceByInvCstCustomerCodeAndIiNameAndUomName(String CST_CSTMR_CODE, String II_NM, String UOM_NM, Integer AD_CMPNY) {

        Debug.print("ArJobOrderEntryControllerBean getInvIiSalesPriceByIiNameAndUomName");

        LocalInvItemHome invItemHome = null;
        LocalInvPriceLevelHome invPriceLevelHome = null;
        LocalArCustomerHome arCustomerHome = null;
        LocalInvUnitOfMeasureConversionHome invUnitOfMeasureConversionHome = null;

        // Initialize EJB Home

        try {

        	invItemHome = (LocalInvItemHome)EJBHomeFactory.
               lookUpLocalHome(LocalInvItemHome.JNDI_NAME, LocalInvItemHome.class);
         	invPriceLevelHome = (LocalInvPriceLevelHome)EJBHomeFactory.
        		lookUpLocalHome(LocalInvPriceLevelHome.JNDI_NAME, LocalInvPriceLevelHome.class);
        	arCustomerHome = (LocalArCustomerHome)EJBHomeFactory.
    			lookUpLocalHome(LocalArCustomerHome.JNDI_NAME, LocalArCustomerHome.class);
            invUnitOfMeasureConversionHome = (LocalInvUnitOfMeasureConversionHome)EJBHomeFactory.
    			lookUpLocalHome(LocalInvUnitOfMeasureConversionHome.JNDI_NAME, LocalInvUnitOfMeasureConversionHome.class);

        } catch (NamingException ex) {

           throw new EJBException(ex.getMessage());

        }

        try {

        	LocalArCustomer arCustomer = null;

        	try {

        		arCustomer = arCustomerHome.findByCstCustomerCode(CST_CSTMR_CODE, AD_CMPNY);

        	} catch (FinderException ex) {

        		return 0d;

        	}

        	double unitPrice = 0d;

        	LocalInvItem invItem = invItemHome.findByIiName(II_NM, AD_CMPNY);

        	LocalInvPriceLevel invPriceLevel = null;

        	try {

        		invPriceLevel = invPriceLevelHome.findByIiNameAndAdLvPriceLevel(II_NM, arCustomer.getCstDealPrice(), AD_CMPNY);

        		if (invPriceLevel.getPlAmount() == 0){

            		unitPrice = invItem.getIiSalesPrice();

            	} else {

            		unitPrice = invPriceLevel.getPlAmount();

            	}

        	} catch (FinderException ex) {

        		unitPrice = invItem.getIiSalesPrice();

        	}

        	LocalInvUnitOfMeasureConversion invUnitOfMeasureConversion = invUnitOfMeasureConversionHome.findUmcByIiNameAndUomName(II_NM, UOM_NM, AD_CMPNY);
        	LocalInvUnitOfMeasureConversion invDefaultUomConversion = invUnitOfMeasureConversionHome.findUmcByIiNameAndUomName(II_NM, invItem.getInvUnitOfMeasure().getUomName(), AD_CMPNY);

        	return EJBCommon.roundIt(unitPrice * invDefaultUomConversion.getUmcConversionFactor() / invUnitOfMeasureConversion.getUmcConversionFactor(), this.getGlFcPrecisionUnit(AD_CMPNY));

        } catch (Exception ex) {

           Debug.printStackTrace(ex);
           throw new EJBException(ex.getMessage());

        }

     }

	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public byte getAdPrfArUseCustomerPulldown(Integer AD_CMPNY) {

		Debug.print("ArJobOrderEntryControllerBean getAdPrfArUseCustomerPulldown");

		LocalAdPreferenceHome adPreferenceHome = null;

		// Initialize EJB Home

		try {

			adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}

		try {

			LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);

			return  adPreference.getPrfArUseCustomerPulldown();

		} catch (Exception ex) {

			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());

		}

	}

	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public ArrayList getArSlpAll(Integer AD_BRNCH, Integer AD_CMPNY) {

	    Debug.print("ArJobOrderEntryControllerBean getArSlpAll");

	    LocalArSalespersonHome arSalespersonHome = null;

	    ArrayList list = new ArrayList();

	    // Initialize EJB Home

	    try {

	        arSalespersonHome = (LocalArSalespersonHome)EJBHomeFactory.
	            lookUpLocalHome(LocalArSalespersonHome.JNDI_NAME, LocalArSalespersonHome.class);

	    } catch (NamingException ex) {

	        throw new EJBException(ex.getMessage());

	    }

	    try {

	        Collection arSalespersons = arSalespersonHome.findSlpByBrCode(AD_BRNCH, AD_CMPNY);

	        Iterator i = arSalespersons.iterator();

	        while (i.hasNext()) {

	        	LocalArSalesperson arSalesperson = (LocalArSalesperson)i.next();

	        	ArSalespersonDetails details = new ArSalespersonDetails();

	        	details.setSlpSalespersonCode(arSalesperson.getSlpSalespersonCode());
	        	details.setSlpName(arSalesperson.getSlpName());

	        	list.add(details);

	        }

	        return list;

	    } catch (Exception ex) {

	    	Debug.printStackTrace(ex);
	    	throw new EJBException(ex.getMessage());

	    }

	}
	
	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public ArModJobOrderTypeDetails getArJobOrderTypeByJotNm(String JOT_NM, Integer AD_CMPNY) {
		
		LocalArJobOrderTypeHome arJobOrderTypeHome = null;
		LocalGlChartOfAccountHome glCoaHome = null;
		
		
		Debug.print("ArJobOrderEntryControllerBean getArJobOrderTypeByJotNm");
		
		try {

			arJobOrderTypeHome = (LocalArJobOrderTypeHome)EJBHomeFactory.
	            lookUpLocalHome(LocalArJobOrderTypeHome.JNDI_NAME, LocalArJobOrderTypeHome.class);
			
			glCoaHome = (LocalGlChartOfAccountHome)EJBHomeFactory.
		            lookUpLocalHome(LocalGlChartOfAccountHome.JNDI_NAME, LocalGlChartOfAccountHome.class);

	    } catch (NamingException ex) {

	        throw new EJBException(ex.getMessage());

	    }
		try {
			

			ArModJobOrderTypeDetails details = new ArModJobOrderTypeDetails();
			
			
			LocalArJobOrderType arJobOrderType = arJobOrderTypeHome.findByJotName(JOT_NM, AD_CMPNY);
			
			
			details.setJotCode(arJobOrderType.getJotCode());
			details.setJotName(arJobOrderType.getJotName());
			details.setJotDescription(arJobOrderType.getJotDescription());
			details.setJotDocumentType(arJobOrderType.getJotDocumentType());
			details.setJotReportType(arJobOrderType.getJotReportType());
			details.setJotEnable(arJobOrderType.getJotEnable());
			if(arJobOrderType.getJotGlCoaJobOrderAccount()!=null) {
				
				
				try {
					LocalGlChartOfAccount glCoa = glCoaHome.findByCoaAccountNumber(arJobOrderType.getJotGlCoaJobOrderAccount().toString(), AD_CMPNY);
					details.setJotGlCoaJobOrderAccount(arJobOrderType.getJotGlCoaJobOrderAccount());
					details.setJotGlCoaJobOrderAccountDescription(glCoa.getCoaAccountDescription());
				}catch(FinderException ex) {
					return details;
				}
				
				
				
			}
			
			if(arJobOrderType.getArTaxCode()!=null) {
				details.setJotTcName(arJobOrderType.getArTaxCode().getTcName());
			}
			
			if(arJobOrderType.getArWithholdingTaxCode()!=null) {
				details.setJotTcName(arJobOrderType.getArWithholdingTaxCode().getWtcName());
			}
			
			
			return details;
			
			
		} catch (Exception ex) {

	    	Debug.printStackTrace(ex);
	    	throw new EJBException(ex.getMessage());

	    }
		
		
	}
	
	

	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public ArrayList getAdApprovalNotifiedUsersBySoCode(Integer SO_CODE, Integer AD_CMPNY) {

		Debug.print("ArJobOrderEntryControllerBean getAdApprovalNotifiedUsersBySoCode");

		LocalAdApprovalQueueHome adApprovalQueueHome = null;
		LocalArJobOrderHome arJobOrderHome = null;

		ArrayList list = new ArrayList();


		// Initialize EJB Home

		try {

			adApprovalQueueHome = (LocalAdApprovalQueueHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdApprovalQueueHome.JNDI_NAME, LocalAdApprovalQueueHome.class);
			arJobOrderHome = (LocalArJobOrderHome)EJBHomeFactory.
			lookUpLocalHome(LocalArJobOrderHome.JNDI_NAME, LocalArJobOrderHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}

		try {

			LocalArJobOrder arJobOrder = arJobOrderHome.findByPrimaryKey(SO_CODE);

			if (arJobOrder.getJoPosted() == EJBCommon.TRUE) {

				list.add("DOCUMENT POSTED");
				return list;

			}

			Collection adApprovalQueues = adApprovalQueueHome.findByAqDocumentAndAqDocumentCode("AR SALES ORDER", SO_CODE, AD_CMPNY);

			Iterator i = adApprovalQueues.iterator();

			while(i.hasNext()) {

				LocalAdApprovalQueue adApprovalQueue = (LocalAdApprovalQueue)i.next();

				list.add(adApprovalQueue.getAdUser().getUsrDescription());

			}

			return list;

		} catch (Exception ex) {

			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());

		}

	}

	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public double getFrRateByFrNameAndFrDate(String FC_NM, Date CONVERSION_DATE, Integer AD_CMPNY)
	throws GlobalConversionDateNotExistException {

		Debug.print("ArJobOrderEntryControllerBean getFrRateByFrNameAndFrDate");

		LocalGlFunctionalCurrencyRateHome glFunctionalCurrencyRateHome = null;
		LocalGlFunctionalCurrencyHome glFunctionalCurrencyHome = null;
		LocalAdCompanyHome adCompanyHome = null;

		// Initialize EJB Home

		try {

			glFunctionalCurrencyRateHome = (LocalGlFunctionalCurrencyRateHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlFunctionalCurrencyRateHome.JNDI_NAME, LocalGlFunctionalCurrencyRateHome.class);
			glFunctionalCurrencyHome = (LocalGlFunctionalCurrencyHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlFunctionalCurrencyHome.JNDI_NAME, LocalGlFunctionalCurrencyHome.class);
			adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}

		try {

			LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
			LocalGlFunctionalCurrency glFunctionalCurrency = glFunctionalCurrencyHome.findByFcName(FC_NM, AD_CMPNY);

			double CONVERSION_RATE = 1;

			// Get functional currency rate

			if (!FC_NM.equals("USD")) {

				LocalGlFunctionalCurrencyRate glFunctionalCurrencyRate =
					glFunctionalCurrencyRateHome.findByFcCodeAndDate(glFunctionalCurrency.getFcCode(),
							CONVERSION_DATE, AD_CMPNY);

				CONVERSION_RATE = glFunctionalCurrencyRate.getFrXToUsd();

			}

			// Get set of book functional currency rate if necessary

			if (!adCompany.getGlFunctionalCurrency().getFcName().equals("USD")) {

				LocalGlFunctionalCurrencyRate glCompanyFunctionalCurrencyRate =
					glFunctionalCurrencyRateHome.findByFcCodeAndDate(adCompany.getGlFunctionalCurrency(). getFcCode(),
							CONVERSION_DATE, AD_CMPNY);

				CONVERSION_RATE = CONVERSION_RATE / glCompanyFunctionalCurrencyRate.getFrXToUsd();

			}

			return CONVERSION_RATE;

		} catch (FinderException ex) {

			getSessionContext().setRollbackOnly();
			throw new GlobalConversionDateNotExistException();

		} catch (Exception ex) {

			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());

		}

	}

    /**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public ArrayList getInvLitByCstLitName(String CST_LIT_NAME, Integer AD_CMPNY)
	    throws GlobalNoRecordFoundException {

	    Debug.print("ArJobOrderEntryControllerBean getInvLitByCstLitName");

	    LocalInvLineItemTemplateHome invLineItemTemplateHome = null;

	    // Initialize EJB Home

	    try {

	    	invLineItemTemplateHome = (LocalInvLineItemTemplateHome)EJBHomeFactory.
	            lookUpLocalHome(LocalInvLineItemTemplateHome.JNDI_NAME, LocalInvLineItemTemplateHome.class);

	    } catch (NamingException ex) {

	        throw new EJBException(ex.getMessage());

	    }

	    try {

	    	LocalInvLineItemTemplate invLineItemTemplate = null;


	    	try {

	    		invLineItemTemplate = invLineItemTemplateHome.findByLitName(CST_LIT_NAME, AD_CMPNY);

	    	} catch (FinderException ex) {

	    		throw new GlobalNoRecordFoundException();

	    	}

	    	ArrayList list = new ArrayList();

	    	// get line items if any

	    	Collection invLineItems = invLineItemTemplate.getInvLineItems();

	    	if (!invLineItems.isEmpty()) {

	    		Iterator i = invLineItems.iterator();

	    		while (i.hasNext()) {

	    			LocalInvLineItem invLineItem = (LocalInvLineItem) i.next();

	    			InvModLineItemDetails liDetails = new InvModLineItemDetails();

	    			liDetails.setLiCode(invLineItem.getLiCode());
	    			liDetails.setLiLine(invLineItem.getLiLine());
	    			liDetails.setLiIiName(invLineItem.getInvItemLocation().getInvItem().getIiName());
	    			liDetails.setLiLocName(invLineItem.getInvItemLocation().getInvLocation().getLocName());
	    			liDetails.setLiUomName(invLineItem.getInvUnitOfMeasure().getUomName());
	    			liDetails.setLiIiDescription(invLineItem.getInvItemLocation().getInvItem().getIiDescription());

	    			list.add(liDetails);

	    		}

	    	}

			return list;

	    } catch (GlobalNoRecordFoundException ex) {

	    	throw ex;

	    } catch (Exception ex) {

	    	Debug.printStackTrace(ex);
	    	throw new EJBException(ex.getMessage());

	    }

	}

	/**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public String getInvIiClassByIiName(String II_NM, Integer AD_CMPNY) {

        Debug.print("ArJobOrderEntryControllerBean getInvIiClassByIiName");

        LocalInvItemHome invItemHome = null;

        // Initialize EJB Home

        try {

            invItemHome = (LocalInvItemHome)EJBHomeFactory.
            lookUpLocalHome(LocalInvItemHome.JNDI_NAME, LocalInvItemHome.class);

        } catch (NamingException ex) {

            throw new EJBException(ex.getMessage());
        }

        try {

            LocalInvItem invItem = invItemHome.findByIiName(II_NM, AD_CMPNY);

            return invItem.getIiClass();

        } catch (Exception ex) {

            Debug.printStackTrace(ex);
            throw new EJBException(ex.getMessage());

        }

    }

    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public byte getAdPrfArEnablePaymentTerm(Integer AD_CMPNY) {

        Debug.print("ArJobOrderEntryControllerBean getAdPrfArEnablePaymentTerm");

        LocalAdPreferenceHome adPreferenceHome = null;


        // Initialize EJB Home

        try {

            adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
            lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);

        } catch (NamingException ex) {

            throw new EJBException(ex.getMessage());

        }


        try {

            LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);

            return adPreference.getPrfArEnablePaymentTerm();

        } catch (Exception ex) {

            Debug.printStackTrace(ex);
            throw new EJBException(ex.getMessage());

        }

    }


    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public boolean getJoTraceMisc(String II_NAME, Integer AD_CMPNY) {

        Debug.print("ArJobOrderEntryControllerBean getJoTraceMisc");

        LocalInvLocationHome invLocationHome = null;
        LocalInvItemHome invItemHome = null;
        Collection invLocations = null;
        ArrayList list = new ArrayList();
        boolean isTraceMisc = false;
        // Initialize EJB Home

        try {

        	invLocationHome = (LocalInvLocationHome)EJBHomeFactory.
                lookUpLocalHome(LocalInvLocationHome.JNDI_NAME, LocalInvLocationHome.class);
        	invItemHome = (LocalInvItemHome)EJBHomeFactory.
                    lookUpLocalHome(LocalInvItemHome.JNDI_NAME, LocalInvItemHome.class);

        } catch (NamingException ex) {

            throw new EJBException(ex.getMessage());

        }

        try {

            LocalInvItem invItem = invItemHome.findByIiName(II_NAME, AD_CMPNY);
            if (invItem.getIiTraceMisc() == 1){
            	isTraceMisc = true;
            }
            return isTraceMisc;
    	} catch (Exception ex) {

    		Debug.printStackTrace(ex);
    		throw new EJBException(ex.getMessage());

    	}

    }
    
    
    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public boolean getJoJobServices(String II_NAME, Integer AD_CMPNY) {

        Debug.print("ArJobOrderEntryControllerBean getJoTraceMisc");

        LocalInvLocationHome invLocationHome = null;
        LocalInvItemHome invItemHome = null;
        Collection invLocations = null;
        ArrayList list = new ArrayList();
        boolean isJobServices = false;
        // Initialize EJB Home

        try {

        	invLocationHome = (LocalInvLocationHome)EJBHomeFactory.
                lookUpLocalHome(LocalInvLocationHome.JNDI_NAME, LocalInvLocationHome.class);
        	invItemHome = (LocalInvItemHome)EJBHomeFactory.
                    lookUpLocalHome(LocalInvItemHome.JNDI_NAME, LocalInvItemHome.class);

        } catch (NamingException ex) {

            throw new EJBException(ex.getMessage());

        }

        try {

            LocalInvItem invItem = invItemHome.findByIiName(II_NAME, AD_CMPNY);
            
            System.out.println("controller job services is: "+  invItem.getIiJobServices());
            if (invItem.getIiJobServices() == EJBCommon.TRUE){
            	isJobServices = true;
            }
            return isJobServices;
    	} catch (Exception ex) {

    		Debug.printStackTrace(ex);
    		throw new EJBException(ex.getMessage());

    	}

    }

    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public byte getAdPrfArDisableSalesPrice(Integer AD_CMPNY) {

        Debug.print("ArJobOrderEntryControllerBean getAdPrfArDisableSalesPrice");

        LocalAdPreferenceHome adPreferenceHome = null;


        // Initialize EJB Home

        try {

            adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
            lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);

        } catch (NamingException ex) {

            throw new EJBException(ex.getMessage());

        }


        try {

            LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);

            return adPreference.getPrfArDisableSalesPrice();

        } catch (Exception ex) {

            Debug.printStackTrace(ex);
            throw new EJBException(ex.getMessage());

        }

    }
    
    
    
    
    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList getAllTechnician(Integer AD_CMPNY) {

        Debug.print("ArJobOrderEntryControllerBean getAllTechnician");

        ArrayList list = new ArrayList();
        LocalAdLookUpValueHome adLookUpValueHome = null;


        // Initialize EJB Home

        try {

        	adLookUpValueHome = (LocalAdLookUpValueHome)EJBHomeFactory.
            lookUpLocalHome(LocalAdLookUpValueHome.JNDI_NAME, LocalAdLookUpValueHome.class);

        } catch (NamingException ex) {

            throw new EJBException(ex.getMessage());

        }


        try {

        	Collection adLookUpValues = adLookUpValueHome.findByLuName("AR JOB ORDER - TECHNICIANS", AD_CMPNY);

        	if(adLookUpValues.size() <=0 ) {
        		return list;
        	}

        	Iterator i = adLookUpValues.iterator();

        	while(i.hasNext()) {

        		LocalAdLookUpValue adLookUpValue = (LocalAdLookUpValue)i.next();
        		System.out.println(adLookUpValue.getLvName());
        		list.add(adLookUpValue.getLvName());
        	}


        	return list;



        } catch (Exception ex) {

            Debug.printStackTrace(ex);
            throw new EJBException(ex.getMessage());

        }

    }
    
    
    
    
    
    
    
    
    
    

    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList getArJobOrderReportParameters(Integer AD_CMPNY) {

        Debug.print("ArJobOrderEntryControllerBean getArJobOrderReportParameters");

        ArrayList list = new ArrayList();
        LocalAdLookUpValueHome adLookUpValueHome = null;


        // Initialize EJB Home

        try {

        	adLookUpValueHome = (LocalAdLookUpValueHome)EJBHomeFactory.
            lookUpLocalHome(LocalAdLookUpValueHome.JNDI_NAME, LocalAdLookUpValueHome.class);

        } catch (NamingException ex) {

            throw new EJBException(ex.getMessage());

        }


        try {

        	Collection adLookUpValues = adLookUpValueHome.findByLuName("AR PRINT JOB ORDER PARAMETER", AD_CMPNY);

        	if(adLookUpValues.size() <=0 ) {
        		return list;
        	}

        	Iterator i = adLookUpValues.iterator();

        	while(i.hasNext()) {

        		LocalAdLookUpValue adLookUpValue = (LocalAdLookUpValue)i.next();
        		System.out.println(adLookUpValue.getLvName());
        		list.add(adLookUpValue.getLvName());
        	}


        	return list;



        } catch (Exception ex) {

            Debug.printStackTrace(ex);
            throw new EJBException(ex.getMessage());

        }

    }


    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList getDocumentTypeList(String DCMNT_TYP, Integer AD_CMPNY) {

        Debug.print("ArJobOrderEntryControllerBean getDocumentTypeList");

        ArrayList list = new ArrayList();
        LocalAdLookUpValueHome adLookUpValueHome = null;


        // Initialize EJB Home

        try {

        	adLookUpValueHome = (LocalAdLookUpValueHome)EJBHomeFactory.
            lookUpLocalHome(LocalAdLookUpValueHome.JNDI_NAME, LocalAdLookUpValueHome.class);

        } catch (NamingException ex) {

            throw new EJBException(ex.getMessage());

        }


        try {

        	Collection adLookUpValues = adLookUpValueHome.findByLuName("AR JOB ORDER DOCUMENT TYPE", AD_CMPNY);

        	if(adLookUpValues.size() <=0 ) {
        		return list;
        	}

        	Iterator i = adLookUpValues.iterator();

        	while(i.hasNext()) {

        		LocalAdLookUpValue adLookUpValue = (LocalAdLookUpValue)i.next();
        		System.out.println(adLookUpValue.getLvName());
        		list.add(adLookUpValue.getLvName());
        	}


        	return list;



        } catch (Exception ex) {

            Debug.printStackTrace(ex);
            throw new EJBException(ex.getMessage());

        }

    }



    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList getArJoTransactionTypeList(Integer AD_CMPNY) {

        Debug.print("ArJobOrderEntryControllerBean getArJoTransactionTypeList");

        ArrayList list = new ArrayList();
        LocalAdLookUpValueHome adLookUpValueHome = null;


        // Initialize EJB Home

        try {

        	adLookUpValueHome = (LocalAdLookUpValueHome)EJBHomeFactory.
            lookUpLocalHome(LocalAdLookUpValueHome.JNDI_NAME, LocalAdLookUpValueHome.class);

        } catch (NamingException ex) {

            throw new EJBException(ex.getMessage());

        }


        try {

        	Collection adLookUpValues = adLookUpValueHome.findByLuName("AR JOB ORDER TRANSACTION TYPE", AD_CMPNY);

        	if(adLookUpValues.size() <=0 ) {
        		return list;
        	}

        	Iterator i = adLookUpValues.iterator();

        	while(i.hasNext()) {

        		LocalAdLookUpValue adLookUpValue = (LocalAdLookUpValue)i.next();
        		System.out.println(adLookUpValue.getLvName());
        		list.add(adLookUpValue.getLvName());
        	}


        	return list;



        } catch (Exception ex) {

            Debug.printStackTrace(ex);
            throw new EJBException(ex.getMessage());

        }

    }

    private String createCodedReferenceNumber(LocalArJobOrder arJobOrder, LocalAdCompany adCompany) {

    	System.out.println("create createCodedReferenceNumber" );
    	String newReferenceNumber = "";
    	if(!arJobOrder.getJoReferenceNumber().equals("")) {
    		return arJobOrder.getJoReferenceNumber();
    	}

    	//ICEI
    	if(adCompany.getCmpShortName().equals("ICEI") && arJobOrder.getJoDocumentType() !=null) {

    		// "TransactionType" + "-" + "last two digit date year" + "-" + "sonumber"

    		//get trans
    		//String transTypeWord = arSalesOrder.getSoTransactionType().equals("")?"F":arSalesOrder.getSoTransactionType();

    		//get trans
    		String transTypeWord = arJobOrder.getJoDocumentType().equals("Air Freight")?"AF":arJobOrder.getJoTransactionType();

    		//get date year (2 digit)
    		DateFormat df = new SimpleDateFormat("yy"); // Just the year, with 2 digits
    		String formattedDate = df.format(arJobOrder.getJoDate());

    		//get SO Number
    		String joNumber = arJobOrder.getJoDocumentNumber();


    		newReferenceNumber = transTypeWord + "-" + formattedDate  + "-" + joNumber;

    	}





    	return newReferenceNumber;

    }

    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public boolean getInvTraceMisc(String II_NAME, Integer AD_CMPNY) {

        Debug.print("ArJobOrderEntryControllerBean getInvLocAll");

        LocalInvLocationHome invLocationHome = null;
        LocalInvItemHome invItemHome = null;
        Collection invLocations = null;
        ArrayList list = new ArrayList();
        boolean isTraceMisc = false;
        // Initialize EJB Home

        try {

        	invLocationHome = (LocalInvLocationHome)EJBHomeFactory.
                lookUpLocalHome(LocalInvLocationHome.JNDI_NAME, LocalInvLocationHome.class);
        	invItemHome = (LocalInvItemHome)EJBHomeFactory.
                    lookUpLocalHome(LocalInvItemHome.JNDI_NAME, LocalInvItemHome.class);

        } catch (NamingException ex) {

            throw new EJBException(ex.getMessage());

        }

        try {

            LocalInvItem invItem = invItemHome.findByIiName(II_NAME, AD_CMPNY);
            if (invItem.getIiTraceMisc() == 1){
            	isTraceMisc = true;
            }
            return isTraceMisc;
    	} catch (Exception ex) {

    		Debug.printStackTrace(ex);
    		throw new EJBException(ex.getMessage());

    	}

    }



    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
	public ArrayList getAdLvReportTypeAll(Integer AD_CMPNY) {

		Debug.print("ArJobOrderEntryControllerBean getAdLvReportTypeAll");

		LocalAdLookUpValueHome adLookUpValueHome = null;

		ArrayList list = new ArrayList();

		// Initialize EJB Home

		try {

			adLookUpValueHome = (LocalAdLookUpValueHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdLookUpValueHome.JNDI_NAME, LocalAdLookUpValueHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}

		try {

			String lookUpName = "AR REPORT TYPE - JOB ORDER";



			Collection adLookUpValues = adLookUpValueHome.findByLuName(lookUpName, AD_CMPNY);

			Iterator i = adLookUpValues.iterator();

			while (i.hasNext()) {

				LocalAdLookUpValue adLookUpValue = (LocalAdLookUpValue)i.next();

				list.add(adLookUpValue.getLvName());

			}

			return list;

		} catch (Exception ex) {

			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());

		}

	}
	
	
	
	 /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
	public ArrayList getAdLvJobOrderStatus(Integer AD_CMPNY) {

		Debug.print("ArJobOrderEntryControllerBean getAdLvJobOrderStatus");

		LocalAdLookUpValueHome adLookUpValueHome = null;

		ArrayList list = new ArrayList();

		// Initialize EJB Home

		try {

			adLookUpValueHome = (LocalAdLookUpValueHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdLookUpValueHome.JNDI_NAME, LocalAdLookUpValueHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}

		try {

			String lookUpName = "AR JOB ORDER STATUS";



			Collection adLookUpValues = adLookUpValueHome.findByLuName(lookUpName, AD_CMPNY);

			Iterator i = adLookUpValues.iterator();

			while (i.hasNext()) {

				LocalAdLookUpValue adLookUpValue = (LocalAdLookUpValue)i.next();

				list.add(adLookUpValue.getLvName());

			}

			return list;

		} catch (Exception ex) {

			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());

		}

	}
	
	
	private void createInvTagList(LocalArJobOrderLine arJobOrderLine, ArrayList list, Integer AD_CMPNY) throws Exception {

    	Debug.print("ArJobOrderEntryControllerBean createInvTagList");

    	LocalAdUserHome adUserHome = null;
    	LocalInvTagHome invTagHome = null;

    	// Initialize EJB Home

    	try{
    		adUserHome = (LocalAdUserHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdUserHome.JNDI_NAME, LocalAdUserHome.class);
    		invTagHome = (LocalInvTagHome)EJBHomeFactory.
			lookUpLocalHome(LocalInvTagHome.JNDI_NAME, LocalInvTagHome.class);

    	} catch (NamingException ex) {

    		throw new EJBException(ex.getMessage());

    	}



    	try {
			System.out.println("aabot?");
	  	    	//Iterator t = apPurchaseOrderLine.getInvTag().iterator();
  	  	    Iterator t = list.iterator();

  	  	    LocalInvTag invTag  = null;
  	  	    System.out.println("umabot?");
  	  	    while (t.hasNext()){
  	  	    	InvModTagListDetails tgLstDetails = (InvModTagListDetails)t.next();
  	  	    	System.out.println(tgLstDetails.getTgCustodian() + "<== custodian");
  	  	    	System.out.println(tgLstDetails.getTgSpecs() + "<== specs");
  	  	    	System.out.println(tgLstDetails.getTgPropertyCode() + "<== propertyCode");
  	  	    	System.out.println(tgLstDetails.getTgExpiryDate() + "<== expiryDate");
  	  	    	System.out.println(tgLstDetails.getTgSerialNumber() + "<== serial number");

  	  	    	if (tgLstDetails.getTgCode()==null){
  	  	    		System.out.println("ngcreate ba?");
      	  	    	invTag = invTagHome.create(tgLstDetails.getTgPropertyCode(),
      	  	    			tgLstDetails.getTgSerialNumber(),null,tgLstDetails.getTgExpiryDate(),
      	  	    			tgLstDetails.getTgSpecs(), AD_CMPNY, tgLstDetails.getTgTransactionDate(),
      	  	    			tgLstDetails.getTgType());

      	  	    	invTag.setArJobOrderLine(arJobOrderLine);
      	  	    	invTag.setInvItemLocation(arJobOrderLine.getInvItemLocation());
      	  	    	LocalAdUser adUser = null;
      	  	    	try {
      	  	    		adUser = adUserHome.findByUsrName(tgLstDetails.getTgCustodian(), AD_CMPNY);
      	  	    	}catch(FinderException ex){

      	  	    	}
      	  	    	invTag.setAdUser(adUser);
      	  	    	System.out.println("ngcreate ba?");
  	  	    	}

  	  	    }




		}catch(Exception ex) {
			throw ex;
		}
    }


    private ArrayList getInvTagList(LocalArJobOrderLine arJobOrderLine) {

    	ArrayList list = new ArrayList();

    	Collection invTags = arJobOrderLine.getInvTags();
		Iterator x = invTags.iterator();
		while (x.hasNext()) {
			LocalInvTag invTag = (LocalInvTag) x.next();
			InvModTagListDetails tgLstDetails = new InvModTagListDetails();
			tgLstDetails.setTgPropertyCode(invTag.getTgPropertyCode());
			tgLstDetails.setTgSpecs(invTag.getTgSpecs());
			tgLstDetails.setTgExpiryDate(invTag.getTgExpiryDate());
			tgLstDetails.setTgSerialNumber(invTag.getTgSerialNumber());
			try{

				tgLstDetails.setTgCustodian(invTag.getAdUser().getUsrName());
			}
			catch(Exception ex){
				tgLstDetails.setTgCustodian("");
			}

			list.add(tgLstDetails);

			System.out.println(tgLstDetails.getTgPropertyCode() + "<== property code inside controllerbean ");
			System.out.println(tgLstDetails.getTgSpecs() + "<== specs inside controllerbean ");
			System.out.println(list+ "<== taglist inside controllerbean ");

		}

    	return list;

    }



    /**
	 * @ejb:interface-method view-type="remote"
	 **/
	public ArrayList getAllJobOrderTypeName(Integer AD_CMPNY) {

		Debug.print("ArJobOrderEntryControllerBean getAllJobOrderType");

		LocalArJobOrderTypeHome arJobOrderTypeHome = null;

		//Initialize EJB Home

		try {

			arJobOrderTypeHome = (LocalArJobOrderTypeHome)EJBHomeFactory.
				lookUpLocalHome(LocalArJobOrderTypeHome.JNDI_NAME, LocalArJobOrderTypeHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}

		try {

			Collection arJobOrderTypes = arJobOrderTypeHome.findJotAll(AD_CMPNY);
			ArrayList list = new ArrayList();


			Iterator i = arJobOrderTypes.iterator();

			while(i.hasNext()) {

				LocalArJobOrderType arJobOrderType = (LocalArJobOrderType)i.next();

				list.add(arJobOrderType.getJotName());

			}

			return list;

		} catch (Exception ex) {

			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());

		}

	}





	/**
	 * @ejb:interface-method view-type="remote"
	 **/
	public Integer getArJolCodeByJolLineAndJoCode(short JOL_LN, Integer JO_CODE, Integer AD_BRNCH, Integer AD_CMPNY) {

		Debug.print("ArJobOrderEntryControllerBean getArJolCodeByJolLineAndJoCode");


		LocalArJobOrderLineHome arJobOrderLineHome = null;
		//Initialize EJB Home

		try {

			arJobOrderLineHome = (LocalArJobOrderLineHome)EJBHomeFactory.
				lookUpLocalHome(LocalArJobOrderLineHome.JNDI_NAME, LocalArJobOrderLineHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}

		try {

			LocalArJobOrderLine arJobOrderLine = arJobOrderLineHome.findByJoCodeAndJolLineAndBrCode(JO_CODE, JOL_LN, AD_BRNCH, AD_CMPNY);

			return arJobOrderLine.getJolCode();

		} catch (Exception ex) {

			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());

		}

	}



	// private methods

	// SessionBean methods

	/**
	 * @ejb:create-method view-type="remote"
	 **/
	public void ejbCreate() throws CreateException {

		Debug.print("ArJobOrderEntryControllerBean ejbCreate");

	}

}
