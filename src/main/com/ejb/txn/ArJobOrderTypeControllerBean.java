
/*
 * ArJobOrderTypeControllerBean.java
 *
 * Created on January 10, 2019, 3:15 AM
 *
 * @author  Ruben P. Lamberte
 */
 
package com.ejb.txn;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;
import javax.naming.NamingException;

import com.ejb.ad.LocalAdCompany;
import com.ejb.ad.LocalAdCompanyHome;
import com.ejb.ad.LocalAdLookUpValue;
import com.ejb.ad.LocalAdLookUpValueHome;
import com.ejb.ar.LocalArAutoAccountingSegmentHome;
import com.ejb.ar.LocalArCustomerClass;
import com.ejb.ar.LocalArCustomerClassHome;
import com.ejb.ar.LocalArJobOrderType;
import com.ejb.ar.LocalArJobOrderTypeHome;
import com.ejb.ar.LocalArTaxCode;
import com.ejb.ar.LocalArTaxCodeHome;
import com.ejb.ar.LocalArWithholdingTaxCode;
import com.ejb.ar.LocalArWithholdingTaxCodeHome;
import com.ejb.exception.ArCCCoaGlEarnedInterestAccountNotFoundException;
import com.ejb.exception.ArCCCoaGlEarnedPenaltyAccountNotFoundException;
import com.ejb.exception.ArCCCoaGlReceivableAccountNotFoundException;
import com.ejb.exception.ArCCCoaGlRevenueAccountNotFoundException;
import com.ejb.exception.ArCCCoaGlUnEarnedInterestAccountNotFoundException;
import com.ejb.exception.ArCCCoaGlUnEarnedPenaltyAccountNotFoundException;
import com.ejb.exception.ArJOTCoaGlJobOrderAccountNotFoundException;
import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.exception.GlobalRecordAlreadyAssignedException;
import com.ejb.exception.GlobalRecordAlreadyDeletedException;
import com.ejb.exception.GlobalRecordAlreadyExistException;
import com.ejb.gl.LocalGlChartOfAccount;
import com.ejb.gl.LocalGlChartOfAccountHome;
import com.util.AbstractSessionBean;
import com.util.ArModCustomerClassDetails;
import com.util.ArModJobOrderTypeDetails;
import com.util.Debug;
import com.util.EJBCommon;
import com.util.EJBHomeFactory;

/**
 * @ejb:bean name="ArJobOrderTypeControllerEJB"
 *           display-name="Used for entering job order type"
 *           type="Stateless"
 *           view-type="remote"
 *           jndi-name="ejb/ArJobOrderTypeControllerEJB"
 *
 * @ejb:interface remote-class="com.ejb.txn.ArJobOrderTypeController"
 *                extends="javax.ejb.EJBObject"
 *
 * @ejb:home remote-class="com.ejb.txn.ArJobOrderTypeControllerHome"
 *           extends="javax.ejb.EJBHome"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="aruser"
 *                        role-link="aruserlink"
 *
 * @ejb:permission role-name="aruser"
 * 
*/

public class ArJobOrderTypeControllerBean extends AbstractSessionBean {
	
	
	
	


    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList getArJotAll(Integer AD_CMPNY) 
        throws GlobalNoRecordFoundException {
                    
        Debug.print("ArJobOrderTypeControllerBean getArJotAll");

        
        LocalArJobOrderTypeHome arJobOrderTypeHome = null;
        LocalGlChartOfAccountHome glChartOfAccountHome = null;


        ArrayList list = new ArrayList();
        
        // Initialize EJB Home

        try {

        	arJobOrderTypeHome = (LocalArJobOrderTypeHome)EJBHomeFactory.
                lookUpLocalHome(LocalArJobOrderTypeHome.JNDI_NAME, LocalArJobOrderTypeHome.class);
            glChartOfAccountHome = (LocalGlChartOfAccountHome)EJBHomeFactory.
                lookUpLocalHome(LocalGlChartOfAccountHome.JNDI_NAME, LocalGlChartOfAccountHome.class);                

        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {
        	        	
	        Collection arJobOrderTypes = arJobOrderTypeHome.findJotAll(AD_CMPNY);
	
	        if (arJobOrderTypes.isEmpty()) {
	
	            throw new GlobalNoRecordFoundException();
	        	
	        }
	            
	        Iterator i = arJobOrderTypes.iterator();
	               
	        while (i.hasNext()) {
	        	
	       
	        	LocalArJobOrderType arJobOrderType = (LocalArJobOrderType)i.next();
	        	LocalGlChartOfAccount glJobOrderTypeAccount = null;
	        	
	        	ArModJobOrderTypeDetails mdetails = new ArModJobOrderTypeDetails();
	        	try {
	        		
	        		glJobOrderTypeAccount = glChartOfAccountHome.findByPrimaryKey(arJobOrderType.getJotGlCoaJobOrderAccount()==null?0:arJobOrderType.getJotGlCoaJobOrderAccount());		        		        
	    		      
	        	}catch(FinderException ex) {
	        		
	        	}
		        
		     
		        if (arJobOrderType.getJotGlCoaJobOrderAccount() != null) {               
		        			        	                             		        	
		        	glJobOrderTypeAccount = glChartOfAccountHome.findByPrimaryKey(arJobOrderType.getJotGlCoaJobOrderAccount());    
		        	   mdetails.setJotGlCoaJobOrderAccountNumber(glJobOrderTypeAccount.getCoaAccountNumber());
		                
		                mdetails.setJotGlCoaJobOrderAccountDescription(glJobOrderTypeAccount.getCoaAccountDescription());
		        	
		        }
		      	        	                             
        		
        		mdetails.setJotCode(arJobOrderType.getJotCode());        		
                mdetails.setJotName(arJobOrderType.getJotName());    
                mdetails.setJotDocumentType(arJobOrderType.getJotDocumentType());
                mdetails.setJotReportType(arJobOrderType.getJotReportType());
                mdetails.setJotDescription(arJobOrderType.getJotDescription());
               
                
                                                

             
              
                mdetails.setJotEnable(arJobOrderType.getJotEnable());
              
			      
        		list.add(mdetails);
	        		
	        }              
	                                                        		        		        
	        return list;
	        
        } catch (GlobalNoRecordFoundException ex) {
        	
        	throw ex;
        	        	
        } catch (Exception ex) {
        	
        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());
        	
        }
            
    }
    
    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList getArTcAll(Integer AD_CMPNY) {
                    
        Debug.print("ArJobOrderTypeControllerBean getArTcAll");
        
        LocalArTaxCodeHome arTaxCodeHome = null;
        
        Collection arTaxCodes = null;
        
        LocalArTaxCode arTaxCode = null;
        
        ArrayList list = new ArrayList();
        
        // Initialize EJB Home
        
        try {
            
            arTaxCodeHome = (LocalArTaxCodeHome)EJBHomeFactory.
                lookUpLocalHome(LocalArTaxCodeHome.JNDI_NAME, LocalArTaxCodeHome.class);
            
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {
            
            arTaxCodes = arTaxCodeHome.findEnabledTcAll(AD_CMPNY);
            
	        Iterator i = arTaxCodes.iterator();
	               
	        while (i.hasNext()) {
	        	
	            arTaxCode = (LocalArTaxCode)i.next();
	        	
	            list.add(arTaxCode.getTcName());
	        	
	        }
	        
	        return list;
	        
        } catch (Exception ex) {
        	
        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());
        	
        }	        
            
    }  
    
    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList getArWtcAll(Integer AD_CMPNY) {
                    
        Debug.print("ArJobOrderTypeControllerBean getArWtcAll");
        
        LocalArWithholdingTaxCodeHome arWithholdingTaxCodeHome = null;
        
        Collection arWithholdingTaxCodes = null;
        
        LocalArWithholdingTaxCode arWithholdingTaxCode = null;
        
        ArrayList list = new ArrayList();
        
        // Initialize EJB Home
        
        try {
            
            arWithholdingTaxCodeHome = (LocalArWithholdingTaxCodeHome)EJBHomeFactory.
                lookUpLocalHome(LocalArWithholdingTaxCodeHome.JNDI_NAME, LocalArWithholdingTaxCodeHome.class);
            
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {
            
            arWithholdingTaxCodes = arWithholdingTaxCodeHome.findEnabledWtcAll(AD_CMPNY);

	        Iterator i = arWithholdingTaxCodes.iterator();
	               
	        while (i.hasNext()) {
	        	
	            arWithholdingTaxCode = (LocalArWithholdingTaxCode)i.next();
	        	
	            list.add(arWithholdingTaxCode.getWtcName());
	        	
	        }
        
	        return list;
	        
        } catch (Exception ex) {
        	
        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());
        	
        }        
            
    }            
    
    /**
     * @ejb:interface-method view-type="remote"
     **/    
    public void addArJotEntry(com.util.ArModJobOrderTypeDetails mdetails,Integer AD_CMPNY) 
        throws GlobalRecordAlreadyExistException,               
        ArJOTCoaGlJobOrderAccountNotFoundException
			  
    		{
                    
        Debug.print("ArJobOrderTypeControllerBean addArJotEntry");
        
        LocalArJobOrderTypeHome arJobOrderTypeHome = null;
        LocalGlChartOfAccountHome glChartOfAccountHome = null;
        LocalArTaxCodeHome arTaxCodeHome = null;
        LocalArWithholdingTaxCodeHome arWithholdingTaxCodeHome = null;
            
        ArrayList list = new ArrayList();
        
        // Initialize EJB Home
        
        try {
            
        	arJobOrderTypeHome = (LocalArJobOrderTypeHome)EJBHomeFactory.
                lookUpLocalHome(LocalArJobOrderTypeHome.JNDI_NAME, LocalArJobOrderTypeHome.class);
            glChartOfAccountHome = (LocalGlChartOfAccountHome)EJBHomeFactory.
                lookUpLocalHome(LocalGlChartOfAccountHome.JNDI_NAME, LocalGlChartOfAccountHome.class);
            arTaxCodeHome = (LocalArTaxCodeHome)EJBHomeFactory.
                lookUpLocalHome(LocalArTaxCodeHome.JNDI_NAME, LocalArTaxCodeHome.class);
            arWithholdingTaxCodeHome = (LocalArWithholdingTaxCodeHome)EJBHomeFactory.
                lookUpLocalHome(LocalArWithholdingTaxCodeHome.JNDI_NAME, LocalArWithholdingTaxCodeHome.class);                
                
                                                      
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {
        	
        	LocalArJobOrderType arJobOrderType = null;

	        LocalGlChartOfAccount glJobOrderTypeAccount = null;
	

	        try { 
	            
	        	arJobOrderType = arJobOrderTypeHome.findByJotName(mdetails.getJotName(), AD_CMPNY);
	            
	            throw new GlobalRecordAlreadyExistException();
	            
	        } catch (FinderException ex) {
	         	
	        	 	
	        }
	         
	        // get receivable and revenue account to validate accounts 
	        
	        
	        try {

	        	glJobOrderTypeAccount = glChartOfAccountHome.findByCoaAccountNumber(mdetails.getJotGlCoaJobOrderAccountNumber(), AD_CMPNY);	            	
	            	
	        } catch (FinderException ex) {
	        
	         //  throw new ArJOTCoaGlJobOrderAccountNotFoundException();
	                       
	        }
	        
	     
	      
	            		            	
	    	// create new customer class
	    				    	        	        	    	
	        arJobOrderType = arJobOrderTypeHome.create(mdetails.getJotName(),
	    	        mdetails.getJotDescription(),
	    	        mdetails.getJotGlCoaJobOrderAccount(),
	    	        mdetails.getJotEnable(),
	    	         AD_CMPNY);

	     	        
        } catch (GlobalRecordAlreadyExistException ex) {
        	
        	getSessionContext().setRollbackOnly();
        	throw ex;
        	        	
    //    } catch (ArJOTCoaGlJobOrderAccountNotFoundException ex) {
        	
      //  	getSessionContext().setRollbackOnly();
      //  	throw ex;

       
        	        	
        } catch (Exception ex) {
        	
        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());
        	
        }
            
    }    
    
    /**
     * @ejb:interface-method view-type="remote"
     **/
    public void updateArJotEntry(com.util.ArModJobOrderTypeDetails mdetails, Integer AD_CMPNY) 
        throws GlobalRecordAlreadyExistException,                
        ArJOTCoaGlJobOrderAccountNotFoundException
			  
			   
    	{
                    
        Debug.print("ArJobOrderTypeControllerBean updateArJoEntry");
        
        LocalArJobOrderTypeHome arJobOrderTypeHome = null;
        LocalGlChartOfAccountHome glChartOfAccountHome = null;
        LocalArTaxCodeHome arTaxCodeHome = null;
        LocalArWithholdingTaxCodeHome arWithholdingTaxCodeHome = null;
                
        ArrayList list = new ArrayList();
        
        // Initialize EJB Home
        
        try {

        	arJobOrderTypeHome = (LocalArJobOrderTypeHome)EJBHomeFactory.
                lookUpLocalHome(LocalArJobOrderTypeHome.JNDI_NAME, LocalArJobOrderTypeHome.class);
            glChartOfAccountHome = (LocalGlChartOfAccountHome)EJBHomeFactory.
                lookUpLocalHome(LocalGlChartOfAccountHome.JNDI_NAME, LocalGlChartOfAccountHome.class);
            arTaxCodeHome = (LocalArTaxCodeHome)EJBHomeFactory.
                lookUpLocalHome(LocalArTaxCodeHome.JNDI_NAME, LocalArTaxCodeHome.class);
            arWithholdingTaxCodeHome = (LocalArWithholdingTaxCodeHome)EJBHomeFactory.
                lookUpLocalHome(LocalArWithholdingTaxCodeHome.JNDI_NAME, LocalArWithholdingTaxCodeHome.class);                
                         
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {
        
        	LocalArJobOrderType arJobOrderType = null;
         
	        LocalGlChartOfAccount glJobOrderAccount = null;

        	
        	try {
        	                   
        		LocalArJobOrderType arExistingArJobOrderType = arJobOrderTypeHome.findByJotName(mdetails.getJotName(), AD_CMPNY);
	            
	            if (!arExistingArJobOrderType.getJotCode().equals(mdetails.getJotCode())) {
	            
	                 throw new GlobalRecordAlreadyExistException();
	                 
	            }
	            
	        } catch (FinderException ex) {
	        	
	        }
            
	        // get receivable and revenue account to validate accounts 
	        	        
	        try {

	        	glJobOrderAccount = glChartOfAccountHome.findByCoaAccountNumber(mdetails.getJotGlCoaJobOrderAccountNumber()
	            	, AD_CMPNY);	  
	        	
	        	  arJobOrderType.setJotGlCoaJobOrderAccount(glJobOrderAccount.getCoaCode());
	            	
	        } catch (FinderException ex) {
	        
	       //     throw new ArJOTCoaGlJobOrderAccountNotFoundException();
	                       
	        }
	        
	       
	        

			// find and update customer class
	        System.out.println("--------------------------->");
	        arJobOrderType = arJobOrderTypeHome.findByPrimaryKey(mdetails.getJotCode());

	        arJobOrderType.setJotName(mdetails.getJotName());
	        arJobOrderType.setJotDocumentType(mdetails.getJotDocumentType());
	        arJobOrderType.setJotDescription(mdetails.getJotDescription());


	      

	        arJobOrderType.setJotEnable(mdetails.getJotEnable());
	        arJobOrderType.setJotDocumentType(mdetails.getJotDocumentType());
	        arJobOrderType.setJotReportType(mdetails.getJotReportType());
          
		    		
        } catch (GlobalRecordAlreadyExistException ex) {
        	
        	getSessionContext().setRollbackOnly();
        	throw ex;
        	        	
  //      } catch (ArJOTCoaGlJobOrderAccountNotFoundException ex) {
        	
  //      	getSessionContext().setRollbackOnly();
  //      	throw ex;

   	        	        	
        } catch (Exception ex) {
        	
        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());
        	
        }
            
    }    
    
   /**
    * @ejb:interface-method view-type="remote"
    **/
    public void deleteArJotEntry(Integer JOT_CODE, Integer AD_CMPNY) 
        throws GlobalRecordAlreadyDeletedException,
                GlobalRecordAlreadyAssignedException {

        Debug.print("ArJobOrderTypeControllerBean deleteArJotEntry");
      
        
        LocalArJobOrderTypeHome arJobOrderTypeHome = null;
      
        // Initialize EJB Home
        
        try {

        	arJobOrderTypeHome = (LocalArJobOrderTypeHome)EJBHomeFactory.
                lookUpLocalHome(LocalArJobOrderTypeHome.JNDI_NAME, LocalArJobOrderTypeHome.class);           
      
        } catch (NamingException ex) {

            throw new EJBException(ex.getMessage());

        }
      
        try {
      	
        	LocalArJobOrderType arJobOrderType = null;
      	            
      
	        try {
	      	
	        	arJobOrderType = arJobOrderTypeHome.findByPrimaryKey(JOT_CODE);
	         
	        } catch (FinderException ex) {
	      	
	            throw new GlobalRecordAlreadyDeletedException();
	            
	        }
	         
	      
	                            	
	        arJobOrderType.remove();
	    
        } catch (GlobalRecordAlreadyDeletedException ex) {
        	
        	getSessionContext().setRollbackOnly();
        	throw ex;
        	
        
        	        	
        } catch (Exception ex) {
        	
        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());
        	
        }
            
    }
    
   /**
    * @ejb:interface-method view-type="remote"
    * @jboss:method-attributes read-only="true"
    **/
    public short getGlFcPrecisionUnit(Integer AD_CMPNY) {

       Debug.print("ArJobOrderTypeControllerBean getGlFcPrecisionUnit");

      
       LocalAdCompanyHome adCompanyHome = null;
      
     
       // Initialize EJB Home
        
       try {
            
           adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
              lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);       
            
       } catch (NamingException ex) {
            
           throw new EJBException(ex.getMessage());
            
       }

       try {
       	
         LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
            
         return  adCompany.getGlFunctionalCurrency().getFcPrecision();
                                     
       } catch (Exception ex) {
       	 
       	 Debug.printStackTrace(ex);
         throw new EJBException(ex.getMessage());
         
       }

    }
    
    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList getDocumentTypeList(String DCMNT_TYP, Integer AD_CMPNY) {

        Debug.print("ArJobOrderTypeControllerBean getDocumentTypeList");

        ArrayList list = new ArrayList();
        LocalAdLookUpValueHome adLookUpValueHome = null;


        // Initialize EJB Home

        try {

        	adLookUpValueHome = (LocalAdLookUpValueHome)EJBHomeFactory.
            lookUpLocalHome(LocalAdLookUpValueHome.JNDI_NAME, LocalAdLookUpValueHome.class);

        } catch (NamingException ex) {

            throw new EJBException(ex.getMessage());

        }


        try {

        	Collection adLookUpValues = adLookUpValueHome.findByLuName("AR JOB ORDER DOCUMENT TYPE", AD_CMPNY);

        	if(adLookUpValues.size() <=0 ) {
        		return list;
        	}

        	Iterator i = adLookUpValues.iterator();

        	while(i.hasNext()) {

        		LocalAdLookUpValue adLookUpValue = (LocalAdLookUpValue)i.next();
        		System.out.println(adLookUpValue.getLvName());
        		list.add(adLookUpValue.getLvName());
        	}


        	return list;



        } catch (Exception ex) {

            Debug.printStackTrace(ex);
            throw new EJBException(ex.getMessage());

        }

    }
    
    
    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
	public ArrayList getAdLvReportTypeAll(Integer AD_CMPNY) {

		Debug.print("ArJobOrderTypeControllerBean getAdLvReportTypeAll");

		LocalAdLookUpValueHome adLookUpValueHome = null;

		ArrayList list = new ArrayList();

		// Initialize EJB Home

		try {

			adLookUpValueHome = (LocalAdLookUpValueHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdLookUpValueHome.JNDI_NAME, LocalAdLookUpValueHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}

		try {

			String lookUpName = "AR REPORT TYPE - JOB ORDER";



			Collection adLookUpValues = adLookUpValueHome.findByLuName(lookUpName, AD_CMPNY);

			Iterator i = adLookUpValues.iterator();

			while (i.hasNext()) {

				LocalAdLookUpValue adLookUpValue = (LocalAdLookUpValue)i.next();

				list.add(adLookUpValue.getLvName());

			}

			return list;

		} catch (Exception ex) {

			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());

		}

	}
                
   
    // SessionBean methods

    /**
     * @ejb:create-method view-type="remote"
     **/
    public void ejbCreate() throws CreateException {

        Debug.print("ArJobOrderTypeControllerBean ejbCreate");
      
    }
    
}

