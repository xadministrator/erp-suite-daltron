
/*
 * ArJournalControllerBean.java
 *
 * Created on March 10, 2004, 11:08 AM
 *
 * @author  Neil Andrew M. Ajero
 */

package com.ejb.txn;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;
import javax.naming.NamingException;

import com.ejb.ad.LocalAdCompany;
import com.ejb.ad.LocalAdCompanyHome;
import com.ejb.ad.LocalAdPreference;
import com.ejb.ad.LocalAdPreferenceHome;
import com.ejb.ar.LocalArAppliedInvoice;
import com.ejb.ar.LocalArAppliedInvoiceHome;
import com.ejb.ar.LocalArDistributionRecord;
import com.ejb.ar.LocalArDistributionRecordHome;
import com.ejb.ar.LocalArInvoice;
import com.ejb.ar.LocalArInvoiceHome;
import com.ejb.ar.LocalArReceipt;
import com.ejb.ar.LocalArReceiptHome;
import com.ejb.exception.GlobalAccountNumberInvalidException;
import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.gl.LocalGlChartOfAccount;
import com.ejb.gl.LocalGlChartOfAccountHome;
import com.util.AbstractSessionBean;
import com.util.ArModDistributionRecordDetails;
import com.util.Debug;
import com.util.EJBCommon;
import com.util.EJBHomeFactory;

/**
 * @ejb:bean name="ArJournalControllerEJB"
 *           display-name="Used for editing ar journals"
 *           type="Stateless"
 *           view-type="remote"
 *           jndi-name="ejb/ArJournalControllerEJB"
 *
 * @ejb:interface remote-class="com.ejb.txn.ArJournalController"
 *                extends="javax.ejb.EJBObject"
 *
 * @ejb:home remote-class="com.ejb.txn.ArJournalControllerHome"
 *           extends="javax.ejb.EJBHome"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="aruser"
 *                        role-link="aruserlink"
 *
 * @ejb:permission role-name="aruser"
 * 
*/

public class ArJournalControllerBean extends AbstractSessionBean {
    
    
    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList getArDrByInvCode(Integer INV_CODE, Integer AD_CMPNY) 
        throws GlobalNoRecordFoundException {
                    
        Debug.print("ArJournalControllerBean getArDrByInvCode");
        
        LocalArInvoiceHome arInvoiceHome = null;
        LocalArDistributionRecordHome arDistributionRecordHome = null;

        // Initialize EJB Home
        
        try {
            
            arInvoiceHome = (LocalArInvoiceHome)EJBHomeFactory.
                lookUpLocalHome(LocalArInvoiceHome.JNDI_NAME, LocalArInvoiceHome.class);
            arDistributionRecordHome = (LocalArDistributionRecordHome)EJBHomeFactory.
            	lookUpLocalHome(LocalArDistributionRecordHome.JNDI_NAME, LocalArDistributionRecordHome.class);

        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {
        	
        	LocalArInvoice arInvoice = null;
        
        	try {
            
            	arInvoice = arInvoiceHome.findByPrimaryKey(INV_CODE);

        	} catch (FinderException ex) {
        		
        		throw new GlobalNoRecordFoundException();
        		
        	}
        	
        	ArrayList list = new ArrayList();

        	// get distribution records
        	
        	Collection arDistributionRecords = arDistributionRecordHome.findByInvCode(arInvoice.getInvCode(), AD_CMPNY);      	                    	

        	short lineNumber = 1;
        	
	        Iterator i = arDistributionRecords.iterator();
	               
	        while (i.hasNext()) {
	        	
	        	LocalArDistributionRecord arDistributionRecord = (LocalArDistributionRecord)i.next();
	        	
	        	ArModDistributionRecordDetails mdetails = new ArModDistributionRecordDetails();
	        	
	        	mdetails.setDrCode(arDistributionRecord.getDrCode());
	        	mdetails.setDrLine(lineNumber);
	        	mdetails.setDrClass(arDistributionRecord.getDrClass());
	        	mdetails.setDrDebit(arDistributionRecord.getDrDebit());
	        	mdetails.setDrAmount(arDistributionRecord.getDrAmount());
	        	mdetails.setDrReversed(arDistributionRecord.getDrReversed());
	        	mdetails.setDrCoaAccountNumber(arDistributionRecord.getGlChartOfAccount().getCoaAccountNumber());
			    mdetails.setDrCoaAccountDescription(arDistributionRecord.getGlChartOfAccount().getCoaAccountDescription());
			    
			    list.add(mdetails);
			    
			    lineNumber++;

	        }
	        
        	if (list.isEmpty()) {
        		
        		throw new GlobalNoRecordFoundException();
        		
        	}
        	        	        	
        	return list;
        	
        } catch (GlobalNoRecordFoundException ex) {
        	
        	throw ex;
        	        	
        } catch (Exception ex) {
        	
        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());
        	
        }
            
    }
    
    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList getArDrByRctCode(Integer RCT_CODE, Integer AD_CMPNY) 
        throws GlobalNoRecordFoundException {
                    
        Debug.print("ArJournalControllerBean getArDrByRctCode");
        
        LocalArReceiptHome arReceiptHome = null;
        LocalArDistributionRecordHome arDistributionRecordHome = null;

        // Initialize EJB Home
        
        try {
            
            arReceiptHome = (LocalArReceiptHome)EJBHomeFactory.
                lookUpLocalHome(LocalArReceiptHome.JNDI_NAME, LocalArReceiptHome.class);
            arDistributionRecordHome = (LocalArDistributionRecordHome)EJBHomeFactory.
            	lookUpLocalHome(LocalArDistributionRecordHome.JNDI_NAME, LocalArDistributionRecordHome.class);

        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {
        	
        	LocalArReceipt arReceipt = null;
        
        	try {
            
            	arReceipt = arReceiptHome.findByPrimaryKey(RCT_CODE);

        	} catch (FinderException ex) {
        		
        		throw new GlobalNoRecordFoundException();
        		
        	}
        	
        	ArrayList list = new ArrayList();

        	// get distribution records
        	
        	Collection arDistributionRecords = arDistributionRecordHome.findByRctCode(arReceipt.getRctCode(), AD_CMPNY);     	                    	

        	short lineNumber = 1;
        	
	        Iterator i = arDistributionRecords.iterator();
	               
	        while (i.hasNext()) {
	        	
	        	LocalArDistributionRecord arDistributionRecord = (LocalArDistributionRecord)i.next();
	        	
	        	ArModDistributionRecordDetails mdetails = new ArModDistributionRecordDetails();
	        	
	        	mdetails.setDrCode(arDistributionRecord.getDrCode());
	        	mdetails.setDrLine(lineNumber);
	        	mdetails.setDrClass(arDistributionRecord.getDrClass());
	        	mdetails.setDrDebit(arDistributionRecord.getDrDebit());
	        	mdetails.setDrAmount(arDistributionRecord.getDrAmount());
	        	mdetails.setDrReversed(arDistributionRecord.getDrReversed());
	        	mdetails.setDrCoaAccountNumber(arDistributionRecord.getGlChartOfAccount().getCoaAccountNumber());
			    mdetails.setDrCoaAccountDescription(arDistributionRecord.getGlChartOfAccount().getCoaAccountDescription());
			    
			    list.add(mdetails);
			    
			    lineNumber++;

	        }
	        
        	if (list.isEmpty()) {
        		
        		throw new GlobalNoRecordFoundException();
        		
        	}
        	        	        	
        	return list;
        	
        } catch (GlobalNoRecordFoundException ex) {
        	
        	throw ex;
        	        	
        } catch (Exception ex) {
        	
        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());
        	
        }
            
    }
    
    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public short getGlFcPrecisionUnit(Integer AD_CMPNY) {

       Debug.print("ArJournalControllerBean getGlFcPrecisionUnit");

      
       LocalAdCompanyHome adCompanyHome = null;
      
     
       // Initialize EJB Home
        
       try {
            
           adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
              lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);       
            
       } catch (NamingException ex) {
            
           throw new EJBException(ex.getMessage());
            
       }

       try {
       	
         LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
            
         return  adCompany.getGlFunctionalCurrency().getFcPrecision();
                                     
       } catch (Exception ex) {
       	 
       	 Debug.printStackTrace(ex);
         throw new EJBException(ex.getMessage());
         
       }

    }
    
    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
     public short getAdPrfArInvoiceLineNumber(Integer AD_CMPNY) {

        Debug.print("ArJournalControllerBean getAdPrfArInvoiceLineNumber");
                   
        LocalAdPreferenceHome adPreferenceHome = null;
       
       
        // Initialize EJB Home
         
        try {
             
           adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
               lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);       
             
        } catch (NamingException ex) {
             
           throw new EJBException(ex.getMessage());
             
        }
       
        try {
       	
           LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);
          
           return adPreference.getPrfArInvoiceLineNumber();
          
        } catch (Exception ex) {
         	 
           Debug.printStackTrace(ex);
           throw new EJBException(ex.getMessage());
          
        }
       
     }    
     
    /**
     * @ejb:interface-method view-type="remote"
     **/
    public void saveArDrEntry(Integer PRMRY_KEY, ArrayList drList, String transactionType, Integer AD_BRNCH, Integer AD_CMPNY) 
       throws GlobalAccountNumberInvalidException {
       
       Debug.print("ArJournalControllerBean saveArDrEntry");

       LocalArDistributionRecordHome arDistributionRecordHome = null;
       LocalGlChartOfAccountHome glChartOfAccountHome = null;
       LocalArInvoiceHome arInvoiceHome = null;
       LocalArReceiptHome arReceiptHome = null;
       

       // Initialize EJB Home
      
       try {
            
           arDistributionRecordHome = (LocalArDistributionRecordHome)EJBHomeFactory.
               lookUpLocalHome(LocalArDistributionRecordHome.JNDI_NAME, LocalArDistributionRecordHome.class);
           glChartOfAccountHome = (LocalGlChartOfAccountHome)EJBHomeFactory.
               lookUpLocalHome(LocalGlChartOfAccountHome.JNDI_NAME, LocalGlChartOfAccountHome.class);
           arInvoiceHome = (LocalArInvoiceHome)EJBHomeFactory.
           	   lookUpLocalHome(LocalArInvoiceHome.JNDI_NAME, LocalArInvoiceHome.class);
           arReceiptHome = (LocalArReceiptHome)EJBHomeFactory.
       	   	   lookUpLocalHome(LocalArReceiptHome.JNDI_NAME, LocalArReceiptHome.class);
            
       } catch (NamingException ex) {
            
           throw new EJBException(ex.getMessage());
            
       }
       
       try {
       
	       LocalArInvoice arInvoice = null;
	       LocalArReceipt arReceipt = null;
	       
	       Collection arDistributionRecords = null;
	       Iterator i;
	       
	       if (transactionType.equals("INVOICE") || transactionType.equals("CREDIT MEMO")) {
	    	   
	    	   arInvoice = arInvoiceHome.findByPrimaryKey(PRMRY_KEY);
	    	   
	    	   // remove all distribution records
	    	   
	    	   arDistributionRecords = arInvoice.getArDistributionRecords();
	    	   
	    	   i = arDistributionRecords.iterator();     	  
	    	   	    	   
	    	   Integer drScAccount = null;
	    	   while (i.hasNext()) {
	    		   
	    		   LocalArDistributionRecord arDistributionRecord = (LocalArDistributionRecord)i.next();
	    		   
	    		   if(arDistributionRecord.getDrClass().equals("SC")) drScAccount = arDistributionRecord.getDrScAccount();
	    		   
	    		   i.remove();
	    		   
	    		   arDistributionRecord.remove();
	    		   
	    	   }
	    	   
	    	   // add new distribution records
	    	   
	    	   i = drList.iterator();
	    	   
	    	   while (i.hasNext()) {
	    		   
	    		   ArModDistributionRecordDetails mDrDetails = (ArModDistributionRecordDetails) i.next();
	    		   
	    		   // Set SC Entries
	    		   if(mDrDetails.getDrClass().equals("SC")) mDrDetails.setDrScAccount(drScAccount);
	    		   
	    		   this.addArDrEntry(mDrDetails, arInvoice, null, null, AD_BRNCH, AD_CMPNY);
	    		   
	    	   }	    	   
	    	   
	       } else if (transactionType.equals("RECEIPT") || transactionType.equals("MISC RECEIPT")) {

	    	   ArrayList appliedInvoiceList = new ArrayList(); 

	    	   arReceipt = arReceiptHome.findByPrimaryKey(PRMRY_KEY);

	    	   // remove all distribution records

	    	   arDistributionRecords = arReceipt.getArDistributionRecords();

	    	   i = arDistributionRecords.iterator();     	  

	    	   while (i.hasNext()) {

	    		   LocalArDistributionRecord arDistributionRecord = (LocalArDistributionRecord)i.next();
	    		   appliedInvoiceList.add(arDistributionRecord.getArAppliedInvoice());
	    		   i.remove();

	    		   arDistributionRecord.remove();

	    	   }

	    	   // add new distribution records

	    	   i = drList.iterator();
	    	   int ctr = 0;
	    	   while (i.hasNext()) {
	    		   ArModDistributionRecordDetails mDrDetails = (ArModDistributionRecordDetails) i.next(); 
	    		   try{     	  	  
		    		   LocalArAppliedInvoice arAppliedInvoice  = (LocalArAppliedInvoice)appliedInvoiceList.toArray()[ctr];
		    		   this.addArDrEntry(mDrDetails, null, arReceipt, arAppliedInvoice, AD_BRNCH, AD_CMPNY);
	    		   }catch(Exception e){ 
	    			   this.addArDrEntry(mDrDetails, null, arReceipt, null, AD_BRNCH, AD_CMPNY);
	    		   }
	    		   ctr++;
	    	   }

	       }
                   
       } catch (GlobalAccountNumberInvalidException ex) {
       	    
       	   getSessionContext().setRollbackOnly();
       	   throw ex;

       } catch (Exception ex) {
          
       	  Debug.printStackTrace(ex);
          getSessionContext().setRollbackOnly();
          throw new EJBException(ex.getMessage());
       }

       
    }
    
    // private methods
    
    private void addArDrEntry(ArModDistributionRecordDetails mdetails, LocalArInvoice arInvoice, LocalArReceipt arReceipt, LocalArAppliedInvoice arAppliedInvoice, Integer AD_BRNCH, Integer AD_CMPNY) 
    	throws GlobalAccountNumberInvalidException {
		
    	Debug.print("ArJournalControllerBean addArDrEntry");
	
    	LocalArDistributionRecordHome arDistributionRecordHome = null;        
    	LocalGlChartOfAccountHome glChartOfAccountHome = null;
    	LocalAdCompanyHome adCompanyHome = null;
    	//LocalArAppliedInvoiceHome arAppliedInvoiceHome = null;
            
	    // Initialize EJB Home
	    
	    try {
	        
	        arDistributionRecordHome = (LocalArDistributionRecordHome)EJBHomeFactory.
	            lookUpLocalHome(LocalArDistributionRecordHome.JNDI_NAME, LocalArDistributionRecordHome.class);            
	        glChartOfAccountHome = (LocalGlChartOfAccountHome)EJBHomeFactory.
	            lookUpLocalHome(LocalGlChartOfAccountHome.JNDI_NAME, LocalGlChartOfAccountHome.class);            
	        adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
	            lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);  
	        /*arAppliedInvoiceHome = (LocalArAppliedInvoiceHome)EJBHomeFactory.
            	lookUpLocalHome(LocalArAppliedInvoiceHome.JNDI_NAME, LocalArAppliedInvoiceHome.class); 
	         
	                */    
	    } catch (NamingException ex) {
	        
	        throw new EJBException(ex.getMessage());
	        
	    }            
	            
	    try {
	    	
	    	// get company
	    	
	    	LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
	    	
	    	// validate if coa exists
	    	
	    	LocalGlChartOfAccount glChartOfAccount = null;
	    	
	    	try {
	    	
	    		glChartOfAccount = glChartOfAccountHome.findByCoaAccountNumberAndBranchCode(mdetails.getDrCoaAccountNumber(), AD_BRNCH, AD_CMPNY);
	    		
	    		if (glChartOfAccount.getCoaEnable() == EJBCommon.FALSE) 
	    		    throw new GlobalAccountNumberInvalidException(String.valueOf(mdetails.getDrLine()));
	    		
	    	} catch (FinderException ex) {
	    		
	    		throw new GlobalAccountNumberInvalidException(String.valueOf(mdetails.getDrLine()));
	    		
	    	}
	    	        			    
		    // create distribution record 
		    
		    LocalArDistributionRecord arDistributionRecord = arDistributionRecordHome.create(
			    mdetails.getDrLine(), 
			    mdetails.getDrClass(), mdetails.getDrDebit(), 
			    EJBCommon.roundIt(mdetails.getDrAmount(), adCompany.getGlFunctionalCurrency().getFcPrecision()),
			    EJBCommon.FALSE, EJBCommon.FALSE, AD_CMPNY);
		    
		    //glChartOfAccount.addArDistributionRecord(arDistributionRecord);
		    arDistributionRecord.setGlChartOfAccount(glChartOfAccount);
		    
		    if(arDistributionRecord.getDrClass().equals("SC")){
		    	
		    	arDistributionRecord.setDrScAccount(mdetails.getDrScAccount());
		    	
		    }
			    
		    if (arReceipt != null) {
		    	arReceipt.addArDistributionRecord(arDistributionRecord);
		    					
		    } else if (arInvoice != null) {
		    	
		    	arInvoice.addArDistributionRecord(arDistributionRecord);			
		    
		    }
		    		    
		 // to be used by gl journal interface for cross currency receipts
			if (arAppliedInvoice != null) {
				arAppliedInvoice.addArDistributionRecord(arDistributionRecord);				
			}
			
		} catch (GlobalAccountNumberInvalidException ex) {        	
	    	
	    	getSessionContext().setRollbackOnly();
	    	throw ex;
		    		            		
	    } catch (Exception ex) {
	    	
	    	Debug.printStackTrace(ex);
	    	getSessionContext().setRollbackOnly();
	    	throw new EJBException(ex.getMessage());
	    	
	    }
	
    }

    // SessionBean methods

    /**
     * @ejb:create-method view-type="remote"
     **/
    public void ejbCreate() throws CreateException {

       Debug.print("ArJournalControllerBean ejbCreate");
      
    }      
}
