
/*
 * ArMiscReceiptControllerBean.java
 *
 * Created on March 09, 2004, 1:31 PM
 *
 * @author Neil Andrew M. Ajero
 */

package com.ejb.txn;

import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.StringTokenizer;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;
import javax.naming.NamingException;

import com.ejb.ad.*;
import com.ejb.ap.LocalApSupplier;
import com.ejb.ar.*;
import com.ejb.gl.*;
import com.ejb.inv.*;
import com.ejb.exception.AdPRFCoaGlVarianceAccountNotFoundException;
import com.ejb.exception.GlJREffectiveDateNoPeriodExistException;
import com.ejb.exception.GlJREffectiveDatePeriodClosedException;
import com.ejb.exception.GlobalBranchAccountNumberInvalidException;
import com.ejb.exception.GlobalConversionDateNotExistException;
import com.ejb.exception.GlobalDocumentNumberNotUniqueException;
import com.ejb.exception.GlobalExpiryDateNotFoundException;
import com.ejb.exception.GlobalInvItemLocationNotFoundException;
import com.ejb.exception.GlobalInventoryDateException;
import com.ejb.exception.GlobalJournalNotBalanceException;
import com.ejb.exception.GlobalMiscInfoIsRequiredException;
import com.ejb.exception.GlobalNoApprovalApproverFoundException;
import com.ejb.exception.GlobalNoApprovalRequesterFoundException;
import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.exception.GlobalRecordAlreadyAssignedException;
import com.ejb.exception.GlobalRecordAlreadyDeletedException;
import com.ejb.exception.GlobalRecordInvalidException;
import com.ejb.exception.GlobalTransactionAlreadyApprovedException;
import com.ejb.exception.GlobalTransactionAlreadyPendingException;
import com.ejb.exception.GlobalTransactionAlreadyPostedException;
import com.ejb.exception.GlobalTransactionAlreadyVoidException;
import com.ejb.exception.GlobalTransactionAlreadyVoidPostedException;
import com.ejb.exception.GlobalMiscInfoIsRequiredException;
import com.ejb.genfld.LocalGenField;
import com.util.AbstractSessionBean;
import com.util.AdBranchDetails;
import com.util.ArModCustomerDetails;
import com.util.ArModInvoiceLineDetails;
import com.util.ArModInvoiceLineItemDetails;
import com.util.ArModReceiptDetails;
import com.util.ArSalespersonDetails;
import com.util.ArStandardMemoLineDetails;
import com.util.ArTaxCodeDetails;
import com.util.Debug;
import com.util.EJBCommon;
import com.util.EJBHomeFactory;
import com.util.GlModFunctionalCurrencyDetails;
import com.util.InvModLineItemDetails;
import com.util.InvModTagListDetails;
import com.util.InvModUnitOfMeasureDetails;

/**
 * @ejb:bean name="ArMiscReceiptEntryControllerEJB" display-name="used for entering misc receipt"
 *           type="Stateless" view-type="remote" jndi-name="ejb/ArMiscReceiptEntryControllerEJB"
 *
 * @ejb:interface remote-class="com.ejb.txn.ArMiscReceiptEntryController"
 *                extends="javax.ejb.EJBObject"
 *
 * @ejb:home remote-class="com.ejb.txn.ArMiscReceiptEntryControllerHome" extends="javax.ejb.EJBHome"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="aruser" role-link="aruserlink"
 *
 * @ejb:permission role-name="aruser"
 *
 */

public class ArMiscReceiptEntryControllerBean extends AbstractSessionBean {



  /**
   * @ejb:interface-method view-type="remote"
   * @jboss:method-attributes read-only="true"
   **/
  public byte getPrfEnableArMiscReceiptBatch(
      Integer AD_CMPNY
  ) {

    Debug.print("ArReceiptEntryControllerBean getPrfEnableArMiscReceiptBatch");

    LocalAdPreferenceHome adPreferenceHome = null;


    // Initialize EJB Home

    try {

      adPreferenceHome = (LocalAdPreferenceHome) EJBHomeFactory
          .lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);

    } catch (NamingException ex) {

      throw new EJBException(ex.getMessage());

    }


    try {

      LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);

      return adPreference.getPrfEnableArMiscReceiptBatch();

    } catch (Exception ex) {

      Debug.printStackTrace(ex);
      throw new EJBException(ex.getMessage());

    }

  }


  /**
   * @ejb:interface-method view-type="remote"
   * @jboss:method-attributes read-only="true"
   **/
  public ArrayList getGlFcAllWithDefault(
      Integer AD_CMPNY
  ) {

    Debug.print("ArMiscReceiptEntryControllerBean getGlFcAllWithDefault");

    LocalGlFunctionalCurrencyHome glFunctionalCurrencyHome = null;
    LocalAdCompanyHome adCompanyHome = null;

    Collection glFunctionalCurrencies = null;

    LocalGlFunctionalCurrency glFunctionalCurrency = null;
    LocalAdCompany adCompany = null;


    ArrayList list = new ArrayList();

    // Initialize EJB Home

    try {

      glFunctionalCurrencyHome = (LocalGlFunctionalCurrencyHome) EJBHomeFactory.lookUpLocalHome(
          LocalGlFunctionalCurrencyHome.JNDI_NAME, LocalGlFunctionalCurrencyHome.class);
      adCompanyHome = (LocalAdCompanyHome) EJBHomeFactory
          .lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);

    } catch (NamingException ex) {

      throw new EJBException(ex.getMessage());

    }

    try {

      adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);

      glFunctionalCurrencies = glFunctionalCurrencyHome
          .findFcAllEnabled(EJBCommon.getGcCurrentDateWoTime().getTime(), AD_CMPNY);

    } catch (Exception ex) {

      throw new EJBException(ex.getMessage());
    }

    if (glFunctionalCurrencies.isEmpty()) {

      return null;

    }

    Iterator i = glFunctionalCurrencies.iterator();

    while (i.hasNext()) {

      glFunctionalCurrency = (LocalGlFunctionalCurrency) i.next();

      GlModFunctionalCurrencyDetails mdetails = new GlModFunctionalCurrencyDetails(
          glFunctionalCurrency.getFcCode(), glFunctionalCurrency.getFcName(),
          adCompany.getGlFunctionalCurrency().getFcName().equals(glFunctionalCurrency.getFcName())
              ? EJBCommon.TRUE
              : EJBCommon.FALSE);

      list.add(mdetails);

    }

    return list;

  }


  /**
   * @ejb:interface-method view-type="remote"
   * @jboss:method-attributes read-only="true"
   **/
  public ArrayList getAdBaAll(
      Integer AD_BRNCH, Integer AD_CMPNY
  ) {

    Debug.print("ArMiscReceiptEntryControllerBean getAdBaAll");

    LocalAdBankAccountHome adBankAccountHome = null;

    ArrayList list = new ArrayList();

    // Initialize EJB Home

    try {

      adBankAccountHome = (LocalAdBankAccountHome) EJBHomeFactory
          .lookUpLocalHome(LocalAdBankAccountHome.JNDI_NAME, LocalAdBankAccountHome.class);

    } catch (NamingException ex) {

      throw new EJBException(ex.getMessage());

    }

    try {

      Collection adBankAccounts = adBankAccountHome.findEnabledBaAll(AD_BRNCH, AD_CMPNY);

      Iterator i = adBankAccounts.iterator();

      while (i.hasNext()) {

        LocalAdBankAccount adBankAccount = (LocalAdBankAccount) i.next();

        list.add(adBankAccount.getBaName());

      }

      return list;

    } catch (Exception ex) {

      Debug.printStackTrace(ex);
      throw new EJBException(ex.getMessage());

    }

  }


  /**
   * @ejb:interface-method view-type="remote"
   * @jboss:method-attributes read-only="true"
   **/
  public ArrayList getArTcAll(
      Integer AD_CMPNY
  ) {

    Debug.print("ArMiscReceiptEntryControllerBean getArTcAll");

    LocalArTaxCodeHome arTaxCodeHome = null;

    ArrayList list = new ArrayList();

    // Initialize EJB Home

    try {

      arTaxCodeHome = (LocalArTaxCodeHome) EJBHomeFactory
          .lookUpLocalHome(LocalArTaxCodeHome.JNDI_NAME, LocalArTaxCodeHome.class);

    } catch (NamingException ex) {

      throw new EJBException(ex.getMessage());

    }

    try {

      Collection arTaxCodes = arTaxCodeHome.findEnabledTcAll(AD_CMPNY);

      Iterator i = arTaxCodes.iterator();

      while (i.hasNext()) {

        LocalArTaxCode arTaxCode = (LocalArTaxCode) i.next();

        list.add(arTaxCode.getTcName());

      }

      return list;

    } catch (Exception ex) {

      Debug.printStackTrace(ex);
      throw new EJBException(ex.getMessage());

    }

  }


  /**
   * @ejb:interface-method view-type="remote"
   * @jboss:method-attributes read-only="true"
   **/
  public ArTaxCodeDetails getArTcByTcName(
      String TC_NM, Integer AD_CMPNY
  ) {

    Debug.print("ArMiscReceiptEntryControllerBean getArTcByTcName");

    LocalArTaxCodeHome arTaxCodeHome = null;

    ArrayList list = new ArrayList();

    // Initialize EJB Home

    try {

      arTaxCodeHome = (LocalArTaxCodeHome) EJBHomeFactory
          .lookUpLocalHome(LocalArTaxCodeHome.JNDI_NAME, LocalArTaxCodeHome.class);

    } catch (NamingException ex) {

      throw new EJBException(ex.getMessage());

    }

    try {

      LocalArTaxCode arTaxCode = arTaxCodeHome.findByTcName(TC_NM, AD_CMPNY);

      ArTaxCodeDetails details = new ArTaxCodeDetails();
      details.setTcType(arTaxCode.getTcType());
      details.setTcRate(arTaxCode.getTcRate());

      return details;

    } catch (Exception ex) {

      Debug.printStackTrace(ex);
      throw new EJBException(ex.getMessage());

    }

  }


  /**
   * @ejb:interface-method view-type="remote"
   * @jboss:method-attributes read-only="true"
   **/
  public ArrayList getArWtcAll(
      Integer AD_CMPNY
  ) {

    Debug.print("ArMiscReceiptEntryControllerBean getArWtcAll");

    LocalArWithholdingTaxCodeHome arWithholdingTaxCodeHome = null;

    ArrayList list = new ArrayList();

    // Initialize EJB Home

    try {

      arWithholdingTaxCodeHome = (LocalArWithholdingTaxCodeHome) EJBHomeFactory.lookUpLocalHome(
          LocalArWithholdingTaxCodeHome.JNDI_NAME, LocalArWithholdingTaxCodeHome.class);

    } catch (NamingException ex) {

      throw new EJBException(ex.getMessage());

    }

    try {

      Collection arWithholdingTaxCodes = arWithholdingTaxCodeHome.findEnabledWtcAll(AD_CMPNY);

      Iterator i = arWithholdingTaxCodes.iterator();

      while (i.hasNext()) {

        LocalArWithholdingTaxCode arWithholdingTaxCode = (LocalArWithholdingTaxCode) i.next();

        list.add(arWithholdingTaxCode.getWtcName());

      }

      return list;

    } catch (Exception ex) {

      Debug.printStackTrace(ex);
      throw new EJBException(ex.getMessage());

    }

  }


  /**
   * @ejb:interface-method view-type="remote"
   * @jboss:method-attributes read-only="true"
   **/
  public ArrayList getArCstAll(
      Integer AD_BRNCH, Integer AD_CMPNY
  ) {

    Debug.print("ArMiscReceiptEntryControllerBean getArCstAll");

    LocalArCustomerHome arCustomerHome = null;

    ArrayList list = new ArrayList();

    // Initialize EJB Home

    try {

      arCustomerHome = (LocalArCustomerHome) EJBHomeFactory
          .lookUpLocalHome(LocalArCustomerHome.JNDI_NAME, LocalArCustomerHome.class);

    } catch (NamingException ex) {

      throw new EJBException(ex.getMessage());

    }

    try {

      Collection arCustomers = arCustomerHome.findEnabledCstAll(AD_BRNCH, AD_CMPNY);

      Iterator i = arCustomers.iterator();

      while (i.hasNext()) {

        LocalArCustomer arCustomer = (LocalArCustomer) i.next();

        list.add(arCustomer.getCstCustomerCode());

      }

      return list;

    } catch (Exception ex) {

      Debug.printStackTrace(ex);
      throw new EJBException(ex.getMessage());

    }

  }

  /**
   * @ejb:interface-method view-type="remote"
   * @jboss:method-attributes read-only="true"
   **/
  public ArrayList getArSmlAll(
      Integer AD_BRNCH, Integer AD_CMPNY
  ) {

    Debug.print("ArMiscReceiptEntryControllerBean getArSmlAll");

    LocalArStandardMemoLineHome arStandardMemoLineHome = null;

    ArrayList list = new ArrayList();

    // Initialize EJB Home

    try {

      arStandardMemoLineHome = (LocalArStandardMemoLineHome) EJBHomeFactory.lookUpLocalHome(
          LocalArStandardMemoLineHome.JNDI_NAME, LocalArStandardMemoLineHome.class);

    } catch (NamingException ex) {

      throw new EJBException(ex.getMessage());

    }

    try {

      Collection arStandardMemoLines = arStandardMemoLineHome.findEnabledSmlAll(AD_BRNCH, AD_CMPNY);

      Iterator i = arStandardMemoLines.iterator();

      while (i.hasNext()) {

        LocalArStandardMemoLine arStandardMemoLine = (LocalArStandardMemoLine) i.next();

        list.add(arStandardMemoLine.getSmlName());

      }

      return list;

    } catch (Exception ex) {

      Debug.printStackTrace(ex);
      throw new EJBException(ex.getMessage());

    }

  }

  /**
   * @ejb:interface-method view-type="remote"
   * @jboss:method-attributes read-only="true"
   **/
  public ArrayList getInvLocAll(
      Integer AD_CMPNY
  ) {

    Debug.print("ArMiscReceiptEntryControllerBean getInvLocAll");

    LocalInvLocationHome invLocationHome = null;
    Collection invLocations = null;
    ArrayList list = new ArrayList();

    // Initialize EJB Home

    try {

      invLocationHome = (LocalInvLocationHome) EJBHomeFactory
          .lookUpLocalHome(LocalInvLocationHome.JNDI_NAME, LocalInvLocationHome.class);

    } catch (NamingException ex) {

      throw new EJBException(ex.getMessage());

    }

    try {

      invLocations = invLocationHome.findLocAll(AD_CMPNY);

      if (invLocations.isEmpty()) {

        return null;

      }

      Iterator i = invLocations.iterator();

      while (i.hasNext()) {

        LocalInvLocation invLocation = (LocalInvLocation) i.next();
        String details = invLocation.getLocName();

        list.add(details);

      }

      return list;

    } catch (Exception ex) {

      Debug.printStackTrace(ex);
      throw new EJBException(ex.getMessage());

    }

  }

  /**
   * @ejb:interface-method view-type="remote"
   * @jboss:method-attributes read-only="true"
   **/
  public ArrayList getAdLvInvShiftAll(
      Integer AD_CMPNY
  ) {

    Debug.print("ArMiscReceiptEntryControllerBean getAdLvInvShiftAll");

    LocalAdLookUpValueHome adLookUpValueHome = null;

    ArrayList list = new ArrayList();

    // Initialize EJB Home

    try {

      adLookUpValueHome = (LocalAdLookUpValueHome) EJBHomeFactory
          .lookUpLocalHome(LocalAdLookUpValueHome.JNDI_NAME, LocalAdLookUpValueHome.class);

    } catch (NamingException ex) {

      throw new EJBException(ex.getMessage());

    }

    try {

      Collection adLookUpValues = adLookUpValueHome.findByLuName("INV SHIFT", AD_CMPNY);

      Iterator i = adLookUpValues.iterator();

      while (i.hasNext()) {

        LocalAdLookUpValue adLookUpValue = (LocalAdLookUpValue) i.next();

        list.add(adLookUpValue.getLvName());

      }

      return list;

    } catch (Exception ex) {

      Debug.printStackTrace(ex);
      throw new EJBException(ex.getMessage());

    }

  }

  /**
   * @ejb:interface-method view-type="remote"
   * @jboss:method-attributes read-only="true"
   **/
  public ArrayList getInvUomByIiName(
      String II_NM, Integer AD_CMPNY
  ) {

    Debug.print("ArMiscReceiptEntryControllerBean getInvUomByIiName");

    LocalInvUnitOfMeasureHome invUnitOfMeasureHome = null;
    LocalInvItemHome invItemHome = null;
    ArrayList list = new ArrayList();

    // Initialize EJB Home

    try {

      invUnitOfMeasureHome = (LocalInvUnitOfMeasureHome) EJBHomeFactory
          .lookUpLocalHome(LocalInvUnitOfMeasureHome.JNDI_NAME, LocalInvUnitOfMeasureHome.class);
      invItemHome = (LocalInvItemHome) EJBHomeFactory.lookUpLocalHome(LocalInvItemHome.JNDI_NAME,
          LocalInvItemHome.class);

    } catch (NamingException ex) {

      throw new EJBException(ex.getMessage());
    }

    try {

      LocalInvItem invItem = null;
      LocalInvUnitOfMeasure invItemUnitOfMeasure = null;

      invItem = invItemHome.findByIiName(II_NM, AD_CMPNY);
      invItemUnitOfMeasure = invItem.getInvUnitOfMeasure();

      Collection invUnitOfMeasures = null;
      Iterator i = invUnitOfMeasureHome
          .findByUomAdLvClass(invItemUnitOfMeasure.getUomAdLvClass(), AD_CMPNY).iterator();
      while (i.hasNext()) {

        LocalInvUnitOfMeasure invUnitOfMeasure = (LocalInvUnitOfMeasure) i.next();
        InvModUnitOfMeasureDetails details = new InvModUnitOfMeasureDetails();
        details.setUomName(invUnitOfMeasure.getUomName());

        if (invUnitOfMeasure.getUomName().equals(invItemUnitOfMeasure.getUomName())) {

          details.setDefault(true);

        }

        list.add(details);

      }

      return list;

    } catch (Exception ex) {

      Debug.printStackTrace(ex);
      throw new EJBException(ex.getMessage());

    }

  }

  /**
   * @ejb:interface-method view-type="remote"
   * @jboss:method-attributes read-only="true"
   **/
  public double getIiSalesPriceByInvCstCustomerCodeAndIiNameAndUomName(
      String CST_CSTMR_CODE, String II_NM, Date RCT_DT, String UOM_NM, Integer AD_CMPNY
  ) {

    Debug.print("ArMiscReceiptEntryControllerBean getInvIiSalesPriceByIiNameAndUomName");

    LocalInvItemHome invItemHome = null;
    LocalInvUnitOfMeasureHome invUnitOfMeasureHome = null;
    LocalInvPriceLevelHome invPriceLevelHome = null;
    LocalInvPriceLevelDateHome invPriceLevelDateHome = null;
    LocalArCustomerHome arCustomerHome = null;
    LocalInvUnitOfMeasureConversionHome invUnitOfMeasureConversionHome = null;

    // Initialize EJB Home

    try {

      invItemHome = (LocalInvItemHome) EJBHomeFactory.lookUpLocalHome(LocalInvItemHome.JNDI_NAME,
          LocalInvItemHome.class);
      invPriceLevelHome = (LocalInvPriceLevelHome) EJBHomeFactory
          .lookUpLocalHome(LocalInvPriceLevelHome.JNDI_NAME, LocalInvPriceLevelHome.class);
      invPriceLevelDateHome = (LocalInvPriceLevelDateHome) EJBHomeFactory
          .lookUpLocalHome(LocalInvPriceLevelDateHome.JNDI_NAME, LocalInvPriceLevelDateHome.class);
      arCustomerHome = (LocalArCustomerHome) EJBHomeFactory
          .lookUpLocalHome(LocalArCustomerHome.JNDI_NAME, LocalArCustomerHome.class);
      invUnitOfMeasureConversionHome = (LocalInvUnitOfMeasureConversionHome) EJBHomeFactory
          .lookUpLocalHome(LocalInvUnitOfMeasureConversionHome.JNDI_NAME,
              LocalInvUnitOfMeasureConversionHome.class);

    } catch (NamingException ex) {

      throw new EJBException(ex.getMessage());

    }

    try {

      LocalArCustomer arCustomer = null;

      try {

        arCustomer = arCustomerHome.findByCstCustomerCode(CST_CSTMR_CODE, AD_CMPNY);

      } catch (FinderException ex) {

        return 0d;

      }

      double unitPrice = 0d;

      LocalInvItem invItem = invItemHome.findByIiName(II_NM, AD_CMPNY);

      LocalInvPriceLevel invPriceLevel = null;



      try {

        invPriceLevel = invPriceLevelHome.findByIiNameAndAdLvPriceLevel(II_NM,
            arCustomer.getCstDealPrice(), AD_CMPNY);

        Collection invPriceLevelDates = invPriceLevelDateHome.findPdByIiCodeAndPdPriceLevel(
            "ENABLE", invItem.getIiCode(), arCustomer.getCstDealPrice(), AD_CMPNY);

        unitPrice = invItem.getIiSalesPrice();
        if (invPriceLevelDates.size() <= 0) {

          if (invPriceLevel.getPlAmount() == 0) {

            unitPrice = invItem.getIiSalesPrice();

          } else {

            unitPrice = invPriceLevel.getPlAmount();

          }

        } else {

          boolean pricelLevelDateFound = false;
          Iterator i = invPriceLevelDates.iterator();

          while (i.hasNext()) {
            System.out.println("searching price level date");

            LocalInvPriceLevelDate invPriceLevelDate = (LocalInvPriceLevelDate) i.next();

            System.out.println("invoice date: " + RCT_DT);

            System.out.println("deal date from date: " + invPriceLevelDate.getPdDateFrom());

            System.out.println("deal date to: " + invPriceLevelDate.getPdDateTo());

            if (RCT_DT.equals(invPriceLevelDate.getPdDateFrom())
                || RCT_DT.equals(invPriceLevelDate.getPdDateTo())) {
              pricelLevelDateFound = true;

              System.out.println("priceleveldate found");
              if (invPriceLevelDate.getPdAmount() == 0) {

                unitPrice = invItem.getIiSalesPrice();

              } else {

                unitPrice = invPriceLevelDate.getPdAmount();

              }

              break;

            }


            if (RCT_DT.after(invPriceLevelDate.getPdDateFrom())
                && RCT_DT.before(invPriceLevelDate.getPdDateTo())) {

              pricelLevelDateFound = true;
              System.out.println("priceleveldate found");
              if (invPriceLevelDate.getPdAmount() == 0) {

                unitPrice = invItem.getIiSalesPrice();

              } else {

                unitPrice = invPriceLevelDate.getPdAmount();

              }

              break;

            }


          }


        }


      } catch (FinderException ex) {

        unitPrice = invItem.getIiSalesPrice();

      }

      LocalInvUnitOfMeasureConversion invUnitOfMeasureConversion =
          invUnitOfMeasureConversionHome.findUmcByIiNameAndUomName(II_NM, UOM_NM, AD_CMPNY);
      LocalInvUnitOfMeasureConversion invDefaultUomConversion = invUnitOfMeasureConversionHome
          .findUmcByIiNameAndUomName(II_NM, invItem.getInvUnitOfMeasure().getUomName(), AD_CMPNY);

      return EJBCommon.roundIt(
          unitPrice * invDefaultUomConversion.getUmcConversionFactor()
              / invUnitOfMeasureConversion.getUmcConversionFactor(),
          this.getGlFcPrecisionUnit(AD_CMPNY));

    } catch (Exception ex) {

      Debug.printStackTrace(ex);
      throw new EJBException(ex.getMessage());

    }

  }



  /**
   * @ejb:interface-method view-type="remote"
   * @jboss:method-attributes read-only="true"
   **/
  public double getIiSalesPriceByInvCstCustomerCodeAndIiNameAndUomName(
      String CST_CSTMR_CODE, String II_NM, String UOM_NM, Integer AD_CMPNY
  ) {

    Debug.print("ArMiscReceiptEntryControllerBean getInvIiSalesPriceByIiNameAndUomName");

    LocalInvItemHome invItemHome = null;
    LocalInvUnitOfMeasureHome invUnitOfMeasureHome = null;
    LocalInvPriceLevelHome invPriceLevelHome = null;
    LocalArCustomerHome arCustomerHome = null;
    LocalInvUnitOfMeasureConversionHome invUnitOfMeasureConversionHome = null;

    // Initialize EJB Home

    try {

      invItemHome = (LocalInvItemHome) EJBHomeFactory.lookUpLocalHome(LocalInvItemHome.JNDI_NAME,
          LocalInvItemHome.class);
      invPriceLevelHome = (LocalInvPriceLevelHome) EJBHomeFactory
          .lookUpLocalHome(LocalInvPriceLevelHome.JNDI_NAME, LocalInvPriceLevelHome.class);
      arCustomerHome = (LocalArCustomerHome) EJBHomeFactory
          .lookUpLocalHome(LocalArCustomerHome.JNDI_NAME, LocalArCustomerHome.class);
      invUnitOfMeasureConversionHome = (LocalInvUnitOfMeasureConversionHome) EJBHomeFactory
          .lookUpLocalHome(LocalInvUnitOfMeasureConversionHome.JNDI_NAME,
              LocalInvUnitOfMeasureConversionHome.class);

    } catch (NamingException ex) {

      throw new EJBException(ex.getMessage());

    }

    try {

      LocalArCustomer arCustomer = null;

      try {

        arCustomer = arCustomerHome.findByCstCustomerCode(CST_CSTMR_CODE, AD_CMPNY);

      } catch (FinderException ex) {

        return 0d;

      }

      double unitPrice = 0d;

      LocalInvItem invItem = invItemHome.findByIiName(II_NM, AD_CMPNY);

      LocalInvPriceLevel invPriceLevel = null;

      try {

        invPriceLevel = invPriceLevelHome.findByIiNameAndAdLvPriceLevel(II_NM,
            arCustomer.getCstDealPrice(), AD_CMPNY);

        if (invPriceLevel.getPlAmount() == 0) {

          unitPrice = invItem.getIiSalesPrice();

        } else {

          unitPrice = invPriceLevel.getPlAmount();

        }

      } catch (FinderException ex) {

        unitPrice = invItem.getIiSalesPrice();

      }

      LocalInvUnitOfMeasureConversion invUnitOfMeasureConversion =
          invUnitOfMeasureConversionHome.findUmcByIiNameAndUomName(II_NM, UOM_NM, AD_CMPNY);
      LocalInvUnitOfMeasureConversion invDefaultUomConversion = invUnitOfMeasureConversionHome
          .findUmcByIiNameAndUomName(II_NM, invItem.getInvUnitOfMeasure().getUomName(), AD_CMPNY);

      return EJBCommon.roundIt(
          unitPrice * invDefaultUomConversion.getUmcConversionFactor()
              / invUnitOfMeasureConversion.getUmcConversionFactor(),
          this.getGlFcPrecisionUnit(AD_CMPNY));

    } catch (Exception ex) {

      Debug.printStackTrace(ex);
      throw new EJBException(ex.getMessage());

    }

  }


  /**
   * @ejb:interface-method view-type="remote"
   * @jboss:method-attributes read-only="true"
   **/
  public short getAdPrfArInvoiceLineNumber(
      Integer AD_CMPNY
  ) {

    Debug.print("ArMiscReceiptEntryControllerBean getAdPrfArInvoiceLineNumber");

    LocalAdPreferenceHome adPreferenceHome = null;


    // Initialize EJB Home

    try {

      adPreferenceHome = (LocalAdPreferenceHome) EJBHomeFactory
          .lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);

    } catch (NamingException ex) {

      throw new EJBException(ex.getMessage());

    }


    try {

      LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);

      return adPreference.getPrfArInvoiceLineNumber();

    } catch (Exception ex) {

      Debug.printStackTrace(ex);
      throw new EJBException(ex.getMessage());

    }

  }


  /**
   * @ejb:interface-method view-type="remote"
   * @jboss:method-attributes read-only="true"
   **/
  public ArModReceiptDetails getArRctByRctCode(
      Integer RCT_CODE, Integer AD_CMPNY
  ) throws GlobalNoRecordFoundException {

    Debug.print("ArMiscReceiptEntryControllerBean getArRctByRctCode");

    LocalArReceiptHome arReceiptHome = null;
    LocalAdCompanyHome adCompanyHome = null;

    // Initialize EJB Home

    try {

      arReceiptHome = (LocalArReceiptHome) EJBHomeFactory
          .lookUpLocalHome(LocalArReceiptHome.JNDI_NAME, LocalArReceiptHome.class);
      adCompanyHome = (LocalAdCompanyHome) EJBHomeFactory
          .lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);

    } catch (NamingException ex) {

      throw new EJBException(ex.getMessage());

    }

    try {

      LocalArReceipt arReceipt = null;
      LocalArTaxCode arTaxCode = null;


      try {

        arReceipt = arReceiptHome.findByPrimaryKey(RCT_CODE);
        arTaxCode = arReceipt.getArTaxCode();

      } catch (FinderException ex) {

        throw new GlobalNoRecordFoundException();

      }

      ArrayList list = new ArrayList();

      // get invoice line items if any

      Collection arInvoiceLineItems = arReceipt.getArInvoiceLineItems();

      if (!arInvoiceLineItems.isEmpty()) {

        Iterator i = arInvoiceLineItems.iterator();

        while (i.hasNext()) {

          LocalArInvoiceLineItem arInvoiceLineItem = (LocalArInvoiceLineItem) i.next();

          ArModInvoiceLineItemDetails iliDetails = new ArModInvoiceLineItemDetails();

          iliDetails.setIliCode(arInvoiceLineItem.getIliCode());
          iliDetails.setIliLine(arInvoiceLineItem.getIliLine());
          iliDetails.setIliQuantity(arInvoiceLineItem.getIliQuantity());
          iliDetails.setIliUnitPrice(arInvoiceLineItem.getIliUnitPrice());
          iliDetails.setIliIiName(arInvoiceLineItem.getInvItemLocation().getInvItem().getIiName());
          iliDetails
              .setIliLocName(arInvoiceLineItem.getInvItemLocation().getInvLocation().getLocName());
          iliDetails.setIliUomName(arInvoiceLineItem.getInvUnitOfMeasure().getUomName());
          iliDetails.setIliIiDescription(
              arInvoiceLineItem.getInvItemLocation().getInvItem().getIiDescription());
          iliDetails.setIliEnableAutoBuild(arInvoiceLineItem.getIliEnableAutoBuild());
          iliDetails.setIliDiscount1(arInvoiceLineItem.getIliDiscount1());
          iliDetails.setIliDiscount2(arInvoiceLineItem.getIliDiscount2());
          iliDetails.setIliDiscount3(arInvoiceLineItem.getIliDiscount3());
          iliDetails.setIliDiscount4(arInvoiceLineItem.getIliDiscount4());
          iliDetails.setIliTotalDiscount(arInvoiceLineItem.getIliTotalDiscount());
          iliDetails.setIliTax(arInvoiceLineItem.getIliTax());



          if (arInvoiceLineItem.getInvItemLocation().getInvItem().getIiTraceMisc() == 1) {

            // taglist if exist
            // new code
            if (arInvoiceLineItem.getInvTags().size() > 0) {
              ArrayList tagList = this.getInvTagList(arInvoiceLineItem);

              iliDetails.setIliTagList(tagList);
              iliDetails.setTraceMisc((byte) 1);

            }



          }



          String misc = "";
          try {
            misc = getQuantityExpiryDates(arInvoiceLineItem.getIliMisc());
            misc = arInvoiceLineItem.getIliMisc();
          } catch (Exception e) {
            misc = "$" + arInvoiceLineItem.getIliQuantity() + "$" + arInvoiceLineItem.getIliMisc();
          }

          iliDetails.setIliMisc(misc);
          if (arTaxCode.getTcType().equals("INCLUSIVE")) {

            iliDetails.setIliAmount(
                arInvoiceLineItem.getIliAmount() + arInvoiceLineItem.getIliTaxAmount());



          } else {

            iliDetails.setIliAmount(arInvoiceLineItem.getIliAmount());

          }



          list.add(iliDetails);

        }

      } else {

        // get receipt lines

        Collection arInvoiceLines = arReceipt.getArInvoiceLines();

        Iterator i = arInvoiceLines.iterator();

        while (i.hasNext()) {

          LocalArInvoiceLine arInvoiceLine = (LocalArInvoiceLine) i.next();

          ArModInvoiceLineDetails mdetails = new ArModInvoiceLineDetails();

          mdetails.setIlCode(arInvoiceLine.getIlCode());
          mdetails.setIlDescription(arInvoiceLine.getIlDescription());
          mdetails.setIlQuantity(arInvoiceLine.getIlQuantity());
          mdetails.setIlUnitPrice(arInvoiceLine.getIlUnitPrice());
          if (arTaxCode.getTcType().equals("INCLUSIVE")) {

            mdetails.setIlAmount(arInvoiceLine.getIlAmount() + arInvoiceLine.getIlTaxAmount());

          } else {

            mdetails.setIlAmount(arInvoiceLine.getIlAmount());

          }
          mdetails.setIlTax(arInvoiceLine.getIlTax());
          mdetails.setIlSmlName(arInvoiceLine.getArStandardMemoLine().getSmlName());

          list.add(mdetails);

        }

      }

      ArModReceiptDetails mRctDetails = new ArModReceiptDetails();

      mRctDetails.setRctCode(arReceipt.getRctCode());
      mRctDetails.setRctType(arReceipt.getRctType());
      mRctDetails.setRctDocumentType(arReceipt.getRctDocumentType());
      mRctDetails.setRctDescription(arReceipt.getRctDescription());
      mRctDetails.setRctDate(arReceipt.getRctDate());
      mRctDetails.setRctNumber(arReceipt.getRctNumber());
      mRctDetails.setRctReferenceNumber(arReceipt.getRctReferenceNumber());

      mRctDetails.setRctChequeNumber(arReceipt.getRctChequeNumber());
      mRctDetails.setRctVoucherNumber(arReceipt.getRctVoucherNumber());
      mRctDetails.setRctCardNumber1(arReceipt.getRctCardNumber1());
      mRctDetails.setRctCardNumber2(arReceipt.getRctCardNumber2());
      mRctDetails.setRctCardNumber3(arReceipt.getRctCardNumber3());

      mRctDetails.setRctAmount(arReceipt.getRctAmount());
      mRctDetails.setRctConversionDate(arReceipt.getRctConversionDate());
      mRctDetails.setRctConversionRate(arReceipt.getRctConversionRate());
      mRctDetails.setRctSoldTo(arReceipt.getArCustomer().getCstBillToAddress());

      if (arReceipt.getRctPaymentMethod() == null)
        mRctDetails.setRctPaymentMethod(arReceipt.getArCustomer().getCstPaymentMethod());
      else
        mRctDetails.setRctPaymentMethod(arReceipt.getRctPaymentMethod());

      mRctDetails.setRctCustomerDeposit(arReceipt.getRctCustomerDeposit());
      mRctDetails.setRctAppliedDeposit(arReceipt.getRctAppliedDeposit());
      mRctDetails.setRctApprovalStatus(arReceipt.getRctApprovalStatus());
      mRctDetails.setRctPosted(arReceipt.getRctPosted());
      mRctDetails.setRctVoidApprovalStatus(arReceipt.getRctVoidApprovalStatus());
      mRctDetails.setRctVoidPosted(arReceipt.getRctVoidPosted());
      mRctDetails.setRctReasonForRejection(arReceipt.getRctReasonForRejection());
      mRctDetails.setRctVoid(arReceipt.getRctVoid());
      mRctDetails.setRctReconciled(arReceipt.getRctReconciled());
      mRctDetails.setRctCreatedBy(arReceipt.getRctCreatedBy());
      mRctDetails.setRctDateCreated(arReceipt.getRctDateCreated());
      mRctDetails.setRctLastModifiedBy(arReceipt.getRctLastModifiedBy());
      mRctDetails.setRctDateLastModified(arReceipt.getRctDateLastModified());
      mRctDetails.setRctApprovedRejectedBy(arReceipt.getRctApprovedRejectedBy());
      mRctDetails.setRctDateApprovedRejected(arReceipt.getRctDateApprovedRejected());
      mRctDetails.setRctPostedBy(arReceipt.getRctPostedBy());
      mRctDetails.setRctDatePosted(arReceipt.getRctDatePosted());
      mRctDetails.setRctFcName(arReceipt.getGlFunctionalCurrency().getFcName());
      mRctDetails.setRctTcName(arReceipt.getArTaxCode().getTcName());
      mRctDetails.setRctWtcName(arReceipt.getArWithholdingTaxCode().getWtcName());
      mRctDetails.setRctCstCustomerCode(arReceipt.getArCustomer().getCstCustomerCode());
      mRctDetails.setRctInvtrInvestorFund(arReceipt.getRctInvtrInvestorFund());
      mRctDetails.setRctInvtrNextRunDate(arReceipt.getRctInvtrNextRunDate());

      mRctDetails.setRctBaName(arReceipt.getAdBankAccount().getBaName());
      try {
        mRctDetails.setRctBaCard1Name(arReceipt.getAdBankAccountCard1().getBaName() == null ? "NONE"
            : arReceipt.getAdBankAccountCard1().getBaName());
      } catch (Exception ex) {
      }
      try {
        mRctDetails.setRctBaCard2Name(arReceipt.getAdBankAccountCard2().getBaName() == null ? "NONE"
            : arReceipt.getAdBankAccountCard2().getBaName());
      } catch (Exception ex) {
      }
      try {
        mRctDetails.setRctBaCard3Name(arReceipt.getAdBankAccountCard3().getBaName() == null ? "NONE"
            : arReceipt.getAdBankAccountCard3().getBaName());
      } catch (Exception ex) {
      }

      mRctDetails.setRctAmountCash(arReceipt.getRctAmountCash());
      mRctDetails.setRctAmountCheque(arReceipt.getRctAmountCheque());
      mRctDetails.setRctAmountVoucher(arReceipt.getRctAmountVoucher());
      mRctDetails.setRctAmountCard1(arReceipt.getRctAmountCard1());
      mRctDetails.setRctAmountCard2(arReceipt.getRctAmountCard2());
      mRctDetails.setRctAmountCard3(arReceipt.getRctAmountCard3());

      mRctDetails.setRctRbName(
          arReceipt.getArReceiptBatch() != null ? arReceipt.getArReceiptBatch().getRbName() : null);
      mRctDetails.setRctLvShift(arReceipt.getRctLvShift());
      mRctDetails.setRctSubjectToCommission(arReceipt.getRctSubjectToCommission());
      mRctDetails.setRctCstName(
          arReceipt.getRctCustomerName() == null || arReceipt.getRctCustomerName().equals("")
              ? arReceipt.getArCustomer().getCstName()
              : arReceipt.getRctCustomerName());
      mRctDetails.setRctTcRate(arReceipt.getArTaxCode().getTcRate());
      mRctDetails.setRctTcType(arReceipt.getArTaxCode().getTcType());

      mRctDetails.setReportParameter(arReceipt.getReportParameter());

      if (arReceipt.getArSalesperson() != null) {

        mRctDetails.setRctSlpSalespersonCode(arReceipt.getArSalesperson().getSlpSalespersonCode());
        mRctDetails.setRctSlpName(arReceipt.getArSalesperson().getSlpName());

      }


      if (arReceipt.getArCustomer().getApSupplier() != null) {
        mRctDetails.setIsInvestorSupplier(true);
      } else {
        mRctDetails.setIsInvestorSupplier(false);
      }

      if (!arInvoiceLineItems.isEmpty()) {

        mRctDetails.setInvIliList(list);

      } else {

        mRctDetails.setInvIlList(list);

      }

      return mRctDetails;

    } catch (GlobalNoRecordFoundException ex) {

      throw ex;

    } catch (Exception ex) {

      Debug.printStackTrace(ex);
      throw new EJBException(ex.getMessage());

    }

  }


  /**
   * @ejb:interface-method view-type="remote"
   * @jboss:method-attributes read-only="true"
   **/
  public ArModCustomerDetails getArCstByCstCustomerCode(
      String CST_CSTMR_CODE, Integer AD_CMPNY
  ) throws GlobalNoRecordFoundException {

    Debug.print("ArMiscReceiptEntryControllerBean getArCstByCstCustomerCode");

    LocalArCustomerHome arCustomerHome = null;
    LocalArSalespersonHome arSalespersonHome = null;

    // Initialize EJB Home

    try {
      arSalespersonHome = (LocalArSalespersonHome) EJBHomeFactory
          .lookUpLocalHome(LocalArSalespersonHome.JNDI_NAME, LocalArSalespersonHome.class);
      arCustomerHome = (LocalArCustomerHome) EJBHomeFactory
          .lookUpLocalHome(LocalArCustomerHome.JNDI_NAME, LocalArCustomerHome.class);

    } catch (NamingException ex) {

      throw new EJBException(ex.getMessage());

    }

    try {

      LocalArCustomer arCustomer = null;


      try {

        arCustomer = arCustomerHome.findByCstCustomerCode(CST_CSTMR_CODE, AD_CMPNY);

      } catch (FinderException ex) {

        throw new GlobalNoRecordFoundException();

      }

      ArModCustomerDetails mdetails = new ArModCustomerDetails();

      mdetails.setCstCustomerCode(arCustomer.getCstCustomerCode());
      mdetails.setCstAdBaName(
          arCustomer.getAdBankAccount() != null ? arCustomer.getAdBankAccount().getBaName() : null);
      mdetails.setCstCcWtcName(arCustomer.getArCustomerClass().getArWithholdingTaxCode() != null
          ? arCustomer.getArCustomerClass().getArWithholdingTaxCode().getWtcName()
          : null);
      mdetails.setCstBillToAddress(arCustomer.getCstBillToAddress());
      mdetails.setCstPaymentMethod(arCustomer.getCstPaymentMethod());
      mdetails.setCstName(arCustomer.getCstName());

      if (arCustomer.getApSupplier() != null) {
        mdetails.setCstSupplierCode(arCustomer.getApSupplier().getSplName());
      } else {
        mdetails.setCstSupplierCode(null);
      }


      if (arCustomer.getArSalesperson() != null && arCustomer.getCstArSalesperson2() == null) {

        mdetails.setCstSlpSalespersonCode(arCustomer.getArSalesperson().getSlpSalespersonCode());
        mdetails.setCstSlpName(arCustomer.getArSalesperson().getSlpName());

      } else if (arCustomer.getArSalesperson() == null
          && arCustomer.getCstArSalesperson2() != null) {

        LocalArSalesperson arSalesperson2 = null;
        arSalesperson2 = arSalespersonHome.findByPrimaryKey(arCustomer.getCstArSalesperson2());

        mdetails.setCstSlpSalespersonCode(arSalesperson2.getSlpSalespersonCode());
        mdetails.setCstSlpName(arSalesperson2.getSlpName());

      }
      if (arCustomer.getArSalesperson() != null && arCustomer.getCstArSalesperson2() != null) {

        mdetails.setCstSlpSalespersonCode(arCustomer.getArSalesperson().getSlpSalespersonCode());
        mdetails.setCstSlpName(arCustomer.getArSalesperson().getSlpName());
        LocalArSalesperson arSalesperson2 = null;
        arSalesperson2 = arSalespersonHome.findByPrimaryKey(arCustomer.getCstArSalesperson2());

        mdetails.setCstSlpSalespersonCode2(arSalesperson2.getSlpSalespersonCode());
        mdetails.setCstSlpName2(arSalesperson2.getSlpName());
      }


      if (arCustomer.getArCustomerClass().getArTaxCode() != null) {

        mdetails.setCstCcTcName(arCustomer.getArCustomerClass().getArTaxCode().getTcName());
        mdetails.setCstCcTcRate(arCustomer.getArCustomerClass().getArTaxCode().getTcRate());
        mdetails.setCstCcTcType(arCustomer.getArCustomerClass().getArTaxCode().getTcType());

      }

      if (arCustomer.getInvLineItemTemplate() != null) {
        mdetails.setCstLitName(arCustomer.getInvLineItemTemplate().getLitName());
      }

      return mdetails;

    } catch (GlobalNoRecordFoundException ex) {

      throw ex;

    } catch (Exception ex) {

      Debug.printStackTrace(ex);
      throw new EJBException(ex.getMessage());

    }

  }



  /**
   * @ejb:interface-method view-type="remote"
   * @jboss:method-attributes read-only="true"
   **/
  public com.util.ArStandardMemoLineDetails getArSmlByCstCstmrCodeSmlNm(
      String CST_CSTMR_CODE, String SML_NM, int AD_BRNCH, Integer AD_CMPNY
  ) throws GlobalNoRecordFoundException {

    Debug.print("ArMiscReceiptEntryControllerBean getArSmlByCcNmSmlNm");

    LocalArStandardMemoLineHome arStandardMemoLineHome = null;
    LocalArStandardMemoLineClassHome arStandardMemoLineClassHome = null;
    LocalArCustomerHome arCustomerHome = null;

    try {

      arStandardMemoLineHome = (LocalArStandardMemoLineHome) EJBHomeFactory.lookUpLocalHome(
          LocalArStandardMemoLineHome.JNDI_NAME, LocalArStandardMemoLineHome.class);

      arStandardMemoLineClassHome =
          (LocalArStandardMemoLineClassHome) EJBHomeFactory.lookUpLocalHome(
              LocalArStandardMemoLineClassHome.JNDI_NAME, LocalArStandardMemoLineClassHome.class);

      arCustomerHome = (LocalArCustomerHome) EJBHomeFactory
          .lookUpLocalHome(LocalArCustomerHome.JNDI_NAME, LocalArCustomerHome.class);

    } catch (NamingException ex) {

      throw new EJBException(ex.getMessage());

    }

    try {
      LocalArStandardMemoLine arStandardMemoLine = null;
      LocalArStandardMemoLineClass arStandardMemoLineClass = null;
      LocalArCustomer arCustomer = null;



      ArStandardMemoLineDetails details = new ArStandardMemoLineDetails();

      try {

        arStandardMemoLine = arStandardMemoLineHome.findBySmlName(SML_NM, AD_CMPNY);
        details.setSmlDescription(arStandardMemoLine.getSmlDescription());
        details.setSmlUnitPrice(arStandardMemoLine.getSmlUnitPrice());
        details.setSmlTax(arStandardMemoLine.getSmlTax());

      } catch (FinderException ex) {

        throw new GlobalNoRecordFoundException();

      }



      try {

        arCustomer = arCustomerHome.findByCstCustomerCode(CST_CSTMR_CODE, AD_CMPNY);


        if (arCustomer.getArCustomerClass() != null) {
          String CC_NM = arCustomer.getArCustomerClass().getCcName();

          // find memo line class in null customer code

          try {
            System.out.println("CST CODE:" + CST_CSTMR_CODE);
            arStandardMemoLineClass =
                arStandardMemoLineClassHome.findSmcByCcNameSmlNameCstCstmrCodeAdBrnch(CC_NM, SML_NM,
                    CST_CSTMR_CODE, AD_BRNCH, AD_CMPNY);
            details.setSmlDescription(arStandardMemoLineClass.getSmcStandardMemoLineDescription());
            details.setSmlUnitPrice(arStandardMemoLineClass.getSmcUnitPrice());


          } catch (FinderException ex) {
            System.out.println("no memo line class with cst code");
            // find memo line class in not null customre code
            arStandardMemoLineClass = arStandardMemoLineClassHome
                .findSmcByCcNameSmlNameAdBrnch(CC_NM, SML_NM, AD_BRNCH, AD_CMPNY);
            details.setSmlDescription(arStandardMemoLineClass.getSmcStandardMemoLineDescription());
            details.setSmlUnitPrice(arStandardMemoLineClass.getSmcUnitPrice());
          }
        }
      } catch (FinderException ex) {

        System.out.println("no smc found");
        // no smc exist

      }


      return details;

    } catch (GlobalNoRecordFoundException ex) {

      throw ex;

    } catch (Exception ex) {

      Debug.printStackTrace(ex);
      throw new EJBException(ex.getMessage());

    }



  }



  /**
   * @ejb:interface-method view-type="remote"
   * @jboss:method-attributes read-only="true"
   **/
  public com.util.ArStandardMemoLineDetails getArSmlBySmlName(
      String SML_NM, Integer AD_CMPNY
  ) throws GlobalNoRecordFoundException {

    Debug.print("ArMiscReceiptEntryControllerBean getArSmlBySmlName");

    LocalArStandardMemoLineHome arStandardMemoLineHome = null;

    // Initialize EJB Home

    try {

      arStandardMemoLineHome = (LocalArStandardMemoLineHome) EJBHomeFactory.lookUpLocalHome(
          LocalArStandardMemoLineHome.JNDI_NAME, LocalArStandardMemoLineHome.class);

    } catch (NamingException ex) {

      throw new EJBException(ex.getMessage());

    }

    try {

      LocalArStandardMemoLine arStandardMemoLine = null;


      try {

        arStandardMemoLine = arStandardMemoLineHome.findBySmlName(SML_NM, AD_CMPNY);

      } catch (FinderException ex) {

        throw new GlobalNoRecordFoundException();

      }

      ArStandardMemoLineDetails details = new ArStandardMemoLineDetails();
      details.setSmlDescription(arStandardMemoLine.getSmlDescription());
      details.setSmlUnitPrice(arStandardMemoLine.getSmlUnitPrice());
      details.setSmlTax(arStandardMemoLine.getSmlTax());

      return details;

    } catch (GlobalNoRecordFoundException ex) {

      throw ex;

    } catch (Exception ex) {

      Debug.printStackTrace(ex);
      throw new EJBException(ex.getMessage());

    }

  }


  /**
   * @ejb:interface-method view-type="remote"
   **/
  public Integer saveArRctEntry(
      com.util.ArReceiptDetails details, String BA_NM, String TC_NM, String WTC_NM, String FC_NM,
      String CST_CSTMR_CODE, String RB_NM, ArrayList ilList, boolean isDraft,
      String SLP_SLSPRSN_CODE, Integer AD_BRNCH, Integer AD_CMPNY
  ) throws GlobalRecordAlreadyDeletedException, GlobalDocumentNumberNotUniqueException,
      GlobalConversionDateNotExistException, GlobalTransactionAlreadyApprovedException,
      GlobalTransactionAlreadyPendingException, GlobalTransactionAlreadyPostedException,
      GlobalTransactionAlreadyVoidException, GlobalNoApprovalRequesterFoundException,
      GlobalNoApprovalApproverFoundException, GlJREffectiveDateNoPeriodExistException,
      GlJREffectiveDatePeriodClosedException, GlobalJournalNotBalanceException,
      GlobalBranchAccountNumberInvalidException, GlobalRecordAlreadyAssignedException {

    Debug.print("ArMiscReceiptEntryControllerBean saveArRctEntry");

    LocalArReceiptHome arReceiptHome = null;
    LocalArReceiptBatchHome arReceiptBatchHome = null;
    LocalAdCompanyHome adCompanyHome = null;
    LocalGlFunctionalCurrencyHome glFunctionalCurrencyHome = null;
    LocalAdDocumentSequenceAssignmentHome adDocumentSequenceAssignmentHome = null;
    LocalAdBranchDocumentSequenceAssignmentHome adBranchDocumentSequenceAssignmentHome = null;
    LocalAdBankAccountHome adBankAccountHome = null;
    LocalArTaxCodeHome arTaxCodeHome = null;
    LocalArWithholdingTaxCodeHome arWithholdingTaxCodeHome = null;
    LocalArCustomerHome arCustomerHome = null;
    LocalGlFunctionalCurrencyRateHome glFunctionalCurrencyRateHome = null;
    LocalAdApprovalHome adApprovalHome = null;
    LocalAdAmountLimitHome adAmountLimitHome = null;
    LocalAdApprovalUserHome adApprovalUserHome = null;
    LocalAdApprovalQueueHome adApprovalQueueHome = null;
    LocalAdPreferenceHome adPreferenceHome = null;
    LocalArSalespersonHome arSalespersonHome = null;
    LocalArAppliedInvoiceHome arAppliedInvoiceHome = null;
    LocalAdBranchBankAccountHome adBranchBankAccountHome = null;
    LocalAdBranchArTaxCodeHome adBranchArTaxCodeHome = null;

    InvItemEntryControllerHome homeII = null;
    InvItemEntryController ejbII = null;

    LocalArReceipt arReceipt = null;

    // Initialize EJB Home

    try {

      homeII = (InvItemEntryControllerHome) com.util.EJBHomeFactory
          .lookUpHome("ejb/InvItemEntryControllerEJB", InvItemEntryControllerHome.class);


      adBranchArTaxCodeHome = (LocalAdBranchArTaxCodeHome) EJBHomeFactory
          .lookUpLocalHome(LocalAdBranchArTaxCodeHome.JNDI_NAME, LocalAdBranchArTaxCodeHome.class);

      arReceiptHome = (LocalArReceiptHome) EJBHomeFactory
          .lookUpLocalHome(LocalArReceiptHome.JNDI_NAME, LocalArReceiptHome.class);
      arReceiptBatchHome = (LocalArReceiptBatchHome) EJBHomeFactory
          .lookUpLocalHome(LocalArReceiptBatchHome.JNDI_NAME, LocalArReceiptBatchHome.class);
      adCompanyHome = (LocalAdCompanyHome) EJBHomeFactory
          .lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
      glFunctionalCurrencyHome = (LocalGlFunctionalCurrencyHome) EJBHomeFactory.lookUpLocalHome(
          LocalGlFunctionalCurrencyHome.JNDI_NAME, LocalGlFunctionalCurrencyHome.class);
      adDocumentSequenceAssignmentHome = (LocalAdDocumentSequenceAssignmentHome) EJBHomeFactory
          .lookUpLocalHome(LocalAdDocumentSequenceAssignmentHome.JNDI_NAME,
              LocalAdDocumentSequenceAssignmentHome.class);
      adBranchDocumentSequenceAssignmentHome =
          (LocalAdBranchDocumentSequenceAssignmentHome) EJBHomeFactory.lookUpLocalHome(
              LocalAdBranchDocumentSequenceAssignmentHome.JNDI_NAME,
              LocalAdBranchDocumentSequenceAssignmentHome.class);
      adBankAccountHome = (LocalAdBankAccountHome) EJBHomeFactory
          .lookUpLocalHome(LocalAdBankAccountHome.JNDI_NAME, LocalAdBankAccountHome.class);
      arTaxCodeHome = (LocalArTaxCodeHome) EJBHomeFactory
          .lookUpLocalHome(LocalArTaxCodeHome.JNDI_NAME, LocalArTaxCodeHome.class);
      arWithholdingTaxCodeHome = (LocalArWithholdingTaxCodeHome) EJBHomeFactory.lookUpLocalHome(
          LocalArWithholdingTaxCodeHome.JNDI_NAME, LocalArWithholdingTaxCodeHome.class);
      arCustomerHome = (LocalArCustomerHome) EJBHomeFactory
          .lookUpLocalHome(LocalArCustomerHome.JNDI_NAME, LocalArCustomerHome.class);
      glFunctionalCurrencyRateHome =
          (LocalGlFunctionalCurrencyRateHome) EJBHomeFactory.lookUpLocalHome(
              LocalGlFunctionalCurrencyRateHome.JNDI_NAME, LocalGlFunctionalCurrencyRateHome.class);
      adApprovalHome = (LocalAdApprovalHome) EJBHomeFactory
          .lookUpLocalHome(LocalAdApprovalHome.JNDI_NAME, LocalAdApprovalHome.class);
      adAmountLimitHome = (LocalAdAmountLimitHome) EJBHomeFactory
          .lookUpLocalHome(LocalAdAmountLimitHome.JNDI_NAME, LocalAdAmountLimitHome.class);
      adApprovalUserHome = (LocalAdApprovalUserHome) EJBHomeFactory
          .lookUpLocalHome(LocalAdApprovalUserHome.JNDI_NAME, LocalAdApprovalUserHome.class);
      adApprovalQueueHome = (LocalAdApprovalQueueHome) EJBHomeFactory
          .lookUpLocalHome(LocalAdApprovalQueueHome.JNDI_NAME, LocalAdApprovalQueueHome.class);
      adPreferenceHome = (LocalAdPreferenceHome) EJBHomeFactory
          .lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);
      arSalespersonHome = (LocalArSalespersonHome) EJBHomeFactory
          .lookUpLocalHome(LocalArSalespersonHome.JNDI_NAME, LocalArSalespersonHome.class);
      arAppliedInvoiceHome = (LocalArAppliedInvoiceHome) EJBHomeFactory
          .lookUpLocalHome(LocalArAppliedInvoiceHome.JNDI_NAME, LocalArAppliedInvoiceHome.class);
      adBranchBankAccountHome = (LocalAdBranchBankAccountHome) EJBHomeFactory.lookUpLocalHome(
          LocalAdBranchBankAccountHome.JNDI_NAME, LocalAdBranchBankAccountHome.class);

    } catch (NamingException ex) {

      throw new EJBException(ex.getMessage());

    }


    try {

      ejbII = homeII.create();
    } catch (RemoteException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    } catch (CreateException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }


    try {


      // validate if misc receipt is already deleted

      try {

        if (details.getRctCode() != null) {

          arReceipt = arReceiptHome.findByPrimaryKey(details.getRctCode());

        }

      } catch (FinderException ex) {

        throw new GlobalRecordAlreadyDeletedException();

      }

      // validate if misc receipt is already posted, void, approved or pending

      if (details.getRctCode() != null && details.getRctVoid() == EJBCommon.FALSE) {

        if (arReceipt.getRctApprovalStatus() != null) {

          if (arReceipt.getRctApprovalStatus().equals("APPROVED")
              || arReceipt.getRctApprovalStatus().equals("N/A")) {

            throw new GlobalTransactionAlreadyApprovedException();

          } else if (arReceipt.getRctApprovalStatus().equals("PENDING")) {

            throw new GlobalTransactionAlreadyPendingException();

          }

        }

        if (arReceipt.getRctPosted() == EJBCommon.TRUE) {

          throw new GlobalTransactionAlreadyPostedException();

        } else if (arReceipt.getRctVoid() == EJBCommon.TRUE) {

          throw new GlobalTransactionAlreadyVoidException();

        }

      }

      // misc receipt is void

      if (details.getRctCode() != null && details.getRctVoid() == EJBCommon.TRUE) {

        if (arReceipt.getRctVoid() == EJBCommon.TRUE) {

          throw new GlobalTransactionAlreadyVoidException();

        }

        // check if misc receipt is applied for customer deposit

        if (arReceipt.getRctType().equals("MISC")
            && arReceipt.getRctCustomerDeposit() == EJBCommon.TRUE) {

          if (arReceipt.getRctAppliedDeposit() > 0) {

            throw new GlobalRecordAlreadyAssignedException();

          }

          double draftAppliedDeposit = 0d;

          Collection arUnpostedAppliedInvoices = null;

          try {

            arUnpostedAppliedInvoices =
                arAppliedInvoiceHome.findUnpostedAiWithDepositByCstCustomerCode(
                    arReceipt.getArCustomer().getCstCustomerCode(), AD_BRNCH, AD_CMPNY);

          } catch (FinderException ex) {

          }

          Iterator i = arUnpostedAppliedInvoices.iterator();

          while (i.hasNext()) {

            LocalArAppliedInvoice arUnpostedAppliedInvoice = (LocalArAppliedInvoice) i.next();

            draftAppliedDeposit += arUnpostedAppliedInvoice.getAiAppliedDeposit();

          }

          Collection arDepositReceipts = null;

          try {

            arDepositReceipts = arReceiptHome.findOpenDepositEnabledPostedRctByCstCustomerCode(
                arReceipt.getArCustomer().getCstCustomerCode(), AD_CMPNY);

          } catch (FinderException ex) {

          }

          i = arDepositReceipts.iterator();

          while (i.hasNext()) {

            LocalArReceipt arDepositReceipt = (LocalArReceipt) i.next();

            if (arDepositReceipt.getRctCode().equals(arReceipt.getRctCode())
                && draftAppliedDeposit > 0) {

              throw new GlobalRecordAlreadyAssignedException();

            }

            draftAppliedDeposit -=
                arDepositReceipt.getRctAmount() - arDepositReceipt.getRctAppliedDeposit();

          }


        }

        // check if receipt is already deposited

        if (!arReceipt.getCmFundTransferReceipts().isEmpty()) {

          throw new GlobalRecordAlreadyAssignedException();

        }

        if (arReceipt.getRctPosted() == EJBCommon.TRUE) {

          // generate approval status

          String RCT_APPRVL_STATUS = null;

          if (!isDraft) {

            LocalAdApproval adApproval = adApprovalHome.findByAprAdCompany(AD_CMPNY);

            // check if ar receipt approval is enabled

            if (adApproval.getAprEnableArReceipt() == EJBCommon.FALSE) {

              RCT_APPRVL_STATUS = "N/A";

            } else {

              // check if receipt is self approved

              LocalAdAmountLimit adAmountLimit = null;

              try {

                adAmountLimit = adAmountLimitHome.findByAdcTypeAndAuTypeAndUsrName("AR RECEIPT",
                    "REQUESTER", details.getRctLastModifiedBy(), AD_CMPNY);

              } catch (FinderException ex) {

                throw new GlobalNoApprovalRequesterFoundException();

              }

              if (arReceipt.getRctAmount() <= adAmountLimit.getCalAmountLimit()) {

                RCT_APPRVL_STATUS = "N/A";

              } else {

                // for approval, create approval queue

                Collection adAmountLimits =
                    adAmountLimitHome.findByAdcTypeAndGreaterThanCalAmountLimit("AR RECEIPT",
                        adAmountLimit.getCalAmountLimit(), AD_CMPNY);

                if (adAmountLimits.isEmpty()) {

                  Collection adApprovalUsers = adApprovalUserHome.findByAuTypeAndCalCode("APPROVER",
                      adAmountLimit.getCalCode(), AD_CMPNY);

                  if (adApprovalUsers.isEmpty()) {

                    throw new GlobalNoApprovalApproverFoundException();

                  }

                  Iterator j = adApprovalUsers.iterator();

                  while (j.hasNext()) {

                    LocalAdApprovalUser adApprovalUser = (LocalAdApprovalUser) j.next();

                    LocalAdApprovalQueue adApprovalQueue = adApprovalQueueHome.create(
                        EJBCommon.TRUE, "AR RECEIPT", arReceipt.getRctCode(),
                        arReceipt.getRctNumber(), arReceipt.getRctDate(),
                        adAmountLimit.getCalAndOr(), adApprovalUser.getAuOr(), AD_BRNCH, AD_CMPNY);

                    adApprovalUser.getAdUser().addAdApprovalQueue(adApprovalQueue);

                  }

                } else {

                  boolean isApprovalUsersFound = false;

                  Iterator i = adAmountLimits.iterator();

                  while (i.hasNext()) {

                    LocalAdAmountLimit adNextAmountLimit = (LocalAdAmountLimit) i.next();

                    if (arReceipt.getRctAmount() <= adNextAmountLimit.getCalAmountLimit()) {

                      Collection adApprovalUsers = adApprovalUserHome
                          .findByAuTypeAndCalCode("APPROVER", adAmountLimit.getCalCode(), AD_CMPNY);

                      Iterator j = adApprovalUsers.iterator();

                      while (j.hasNext()) {

                        isApprovalUsersFound = true;

                        LocalAdApprovalUser adApprovalUser = (LocalAdApprovalUser) j.next();

                        LocalAdApprovalQueue adApprovalQueue =
                            adApprovalQueueHome.create(EJBCommon.TRUE, "AR RECEIPT",
                                arReceipt.getRctCode(), arReceipt.getRctNumber(),
                                arReceipt.getRctDate(), adAmountLimit.getCalAndOr(),
                                adApprovalUser.getAuOr(), AD_BRNCH, AD_CMPNY);

                        adApprovalUser.getAdUser().addAdApprovalQueue(adApprovalQueue);

                      }

                      break;

                    } else if (!i.hasNext()) {

                      Collection adApprovalUsers = adApprovalUserHome.findByAuTypeAndCalCode(
                          "APPROVER", adNextAmountLimit.getCalCode(), AD_CMPNY);

                      Iterator j = adApprovalUsers.iterator();

                      while (j.hasNext()) {

                        isApprovalUsersFound = true;

                        LocalAdApprovalUser adApprovalUser = (LocalAdApprovalUser) j.next();

                        LocalAdApprovalQueue adApprovalQueue =
                            adApprovalQueueHome.create(EJBCommon.TRUE, "AR RECEIPT",
                                arReceipt.getRctCode(), arReceipt.getRctNumber(),
                                arReceipt.getRctDate(), adNextAmountLimit.getCalAndOr(),
                                adApprovalUser.getAuOr(), AD_BRNCH, AD_CMPNY);

                        adApprovalUser.getAdUser().addAdApprovalQueue(adApprovalQueue);

                      }

                      break;

                    }

                    adAmountLimit = adNextAmountLimit;

                  }

                  if (!isApprovalUsersFound) {

                    throw new GlobalNoApprovalApproverFoundException();

                  }

                }

                RCT_APPRVL_STATUS = "PENDING";
              }
            }
          }


          // reverse distribution records

          Collection arDistributionRecords = arReceipt.getArDistributionRecords();
          ArrayList list = new ArrayList();

          Iterator i = arDistributionRecords.iterator();

          while (i.hasNext()) {

            LocalArDistributionRecord arDistributionRecord = (LocalArDistributionRecord) i.next();

            list.add(arDistributionRecord);

          }

          i = list.iterator();

          while (i.hasNext()) {

            LocalArDistributionRecord arDistributionRecord = (LocalArDistributionRecord) i.next();

            this.addArDrEntry(arReceipt.getArDrNextLine(), arDistributionRecord.getDrClass(),
                arDistributionRecord.getDrDebit() == EJBCommon.TRUE ? EJBCommon.FALSE
                    : EJBCommon.TRUE,
                arDistributionRecord.getDrAmount(),
                arDistributionRecord.getGlChartOfAccount().getCoaCode(), EJBCommon.TRUE, arReceipt,
                AD_BRNCH, AD_CMPNY);

          }

          LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);

          if (RCT_APPRVL_STATUS != null && RCT_APPRVL_STATUS.equals("N/A")
              && adPreference.getPrfArGlPostingType().equals("AUTO-POST UPON APPROVAL")) {

            arReceipt.setRctVoid(EJBCommon.TRUE);
            this.executeArRctPost(arReceipt.getRctCode(), details.getRctLastModifiedBy(), AD_BRNCH,
                AD_CMPNY);

          }

          // set void approval status

          arReceipt.setRctVoidApprovalStatus(RCT_APPRVL_STATUS);

        }

        arReceipt.setRctVoid(EJBCommon.TRUE);
        arReceipt.setRctLastModifiedBy(details.getRctLastModifiedBy());
        arReceipt.setRctDateLastModified(details.getRctDateLastModified());

        return arReceipt.getRctCode();

      }

      // validate if receipt number is unique receipt number is automatic then set next sequence

      LocalAdBranchDocumentSequenceAssignment adBranchDocumentSequenceAssignment = null;
      LocalAdDocumentSequenceAssignment adDocumentSequenceAssignment = null;

      if (details.getRctCode() == null) {


        String documentType = details.getRctDocumentType();

        try {
          if (documentType != null) {
            adDocumentSequenceAssignment =
                adDocumentSequenceAssignmentHome.findByDcName(documentType, AD_CMPNY);
          } else {
            documentType = "AR RECEIPT";
          }
        } catch (FinderException ex) {
          documentType = "AR RECEIPT";
        }



        try {

          adDocumentSequenceAssignment =
              adDocumentSequenceAssignmentHome.findByDcName(documentType, AD_CMPNY);

        } catch (FinderException ex) {

        }

        try {

          adBranchDocumentSequenceAssignment =
              adBranchDocumentSequenceAssignmentHome.findBdsByDsaCodeAndBrCode(
                  adDocumentSequenceAssignment.getDsaCode(), AD_BRNCH, AD_CMPNY);

        } catch (FinderException ex) {

        }

        LocalArReceipt arExistingReceipt = null;

        try {

          arExistingReceipt =
              arReceiptHome.findByRctNumberAndBrCode(details.getRctNumber(), AD_BRNCH, AD_CMPNY);

        } catch (FinderException ex) {
        }

        if (arExistingReceipt != null) {

          throw new GlobalDocumentNumberNotUniqueException();

        }

        if (adDocumentSequenceAssignment.getAdDocumentSequence().getDsNumberingType() == 'A'
            && (details.getRctNumber() == null || details.getRctNumber().trim().length() == 0)) {

          while (true) {

            if (adBranchDocumentSequenceAssignment == null
                || adBranchDocumentSequenceAssignment.getBdsNextSequence() == null) {

              try {

                arReceiptHome.findByRctNumberAndBrCode(
                    adDocumentSequenceAssignment.getDsaNextSequence(), AD_BRNCH, AD_CMPNY);
                adDocumentSequenceAssignment.setDsaNextSequence(EJBCommon
                    .incrementStringNumber(adDocumentSequenceAssignment.getDsaNextSequence()));

              } catch (FinderException ex) {

                details.setRctNumber(adDocumentSequenceAssignment.getDsaNextSequence());
                adDocumentSequenceAssignment.setDsaNextSequence(EJBCommon
                    .incrementStringNumber(adDocumentSequenceAssignment.getDsaNextSequence()));
                break;

              }

            } else {

              try {

                arReceiptHome.findByRctNumberAndBrCode(
                    adBranchDocumentSequenceAssignment.getBdsNextSequence(), AD_BRNCH, AD_CMPNY);
                adBranchDocumentSequenceAssignment
                    .setBdsNextSequence(EJBCommon.incrementStringNumber(
                        adBranchDocumentSequenceAssignment.getBdsNextSequence()));

              } catch (FinderException ex) {

                details.setRctNumber(adBranchDocumentSequenceAssignment.getBdsNextSequence());
                adBranchDocumentSequenceAssignment
                    .setBdsNextSequence(EJBCommon.incrementStringNumber(
                        adBranchDocumentSequenceAssignment.getBdsNextSequence()));
                break;

              }

            }

          }

        }

      } else {

        LocalArReceipt arExistingReceipt = null;

        try {

          arExistingReceipt =
              arReceiptHome.findByRctNumberAndBrCode(details.getRctNumber(), AD_BRNCH, AD_CMPNY);

        } catch (FinderException ex) {
        }

        if (arExistingReceipt != null
            && !arExistingReceipt.getRctCode().equals(details.getRctCode())) {

          throw new GlobalDocumentNumberNotUniqueException();

        }

        if (arReceipt.getRctNumber() != details.getRctNumber()
            && (details.getRctNumber() == null || details.getRctNumber().trim().length() == 0)) {

          details.setRctNumber(arReceipt.getRctNumber());

        }

      }

      // validate if conversion date exists

      try {

        if (details.getRctConversionDate() != null) {

          LocalGlFunctionalCurrency glValidateFunctionalCurrency =
              glFunctionalCurrencyHome.findByFcName(FC_NM, AD_CMPNY);
          LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);

          if (!glValidateFunctionalCurrency.getFcName().equals("USD")) {

            LocalGlFunctionalCurrencyRate glFunctionalCurrencyRate = glFunctionalCurrencyRateHome
                .findByFcCodeAndDate(glValidateFunctionalCurrency.getFcCode(),
                    details.getRctConversionDate(), AD_CMPNY);

          } else if (!adCompany.getGlFunctionalCurrency().getFcName().equals("USD")) {

            LocalGlFunctionalCurrencyRate glFunctionalCurrencyRate = glFunctionalCurrencyRateHome
                .findByFcCodeAndDate(adCompany.getGlFunctionalCurrency().getFcCode(),
                    details.getRctConversionDate(), AD_CMPNY);

          }

        }

      } catch (FinderException ex) {

        throw new GlobalConversionDateNotExistException();

      }

      // used in checking if receipt should re-generate distribution records and re-calculate taxes
      boolean isRecalculate = true;

      // create misc receipt

      if (details.getRctCode() == null) {

        arReceipt = arReceiptHome.create("MISC", details.getRctDescription(), details.getRctDate(),
            details.getRctNumber(), details.getRctReferenceNumber(), details.getRctCheckNo(),
            details.getRctPayfileReferenceNumber(), details.getRctChequeNumber(), null, null, null,
            null, 0d, 0d, 0d, 0d, 0d, 0d, 0d, details.getRctConversionDate(),
            details.getRctConversionRate(), details.getRctSoldTo(), details.getRctPaymentMethod(),
            details.getRctCustomerDeposit(), 0d, null, null, EJBCommon.FALSE, null, EJBCommon.FALSE,
            EJBCommon.FALSE, EJBCommon.FALSE, EJBCommon.FALSE, EJBCommon.FALSE, EJBCommon.FALSE,
            EJBCommon.FALSE, null, null, null, null, null, details.getRctCreatedBy(),
            details.getRctDateCreated(), details.getRctLastModifiedBy(),
            details.getRctDateLastModified(), null, null, null, null, EJBCommon.FALSE, null,
            EJBCommon.FALSE, details.getRctSubjectToCommission(), null, null,
            details.getRctInvtrBeginningBalance(), details.getRctInvtrInvestorFund(),
            details.getRctInvtrNextRunDate(), AD_BRNCH, AD_CMPNY);


      } else {

        // check if critical fields are changed

        if (!arReceipt.getArTaxCode().getTcName().equals(TC_NM)
            || !arReceipt.getArWithholdingTaxCode().getWtcName().equals(WTC_NM)
            || !arReceipt.getArCustomer().getCstCustomerCode().equals(CST_CSTMR_CODE)
            || !arReceipt.getAdBankAccount().getBaName().equals(BA_NM)
            || ilList.size() != arReceipt.getArInvoiceLines().size()) {

          isRecalculate = true;

        } else if (ilList.size() == arReceipt.getArInvoiceLines().size()) {

          Iterator ilIter = arReceipt.getArInvoiceLines().iterator();
          Iterator ilListIter = ilList.iterator();

          while (ilIter.hasNext()) {

            LocalArInvoiceLine arInvoiceLine = (LocalArInvoiceLine) ilIter.next();
            ArModInvoiceLineDetails mdetails = (ArModInvoiceLineDetails) ilListIter.next();

            if (!arInvoiceLine.getArStandardMemoLine().getSmlName().equals(mdetails.getIlSmlName())
                || arInvoiceLine.getIlQuantity() != mdetails.getIlQuantity()
                || arInvoiceLine.getIlUnitPrice() != mdetails.getIlUnitPrice()
                || arInvoiceLine.getIlTax() != mdetails.getIlTax()) {

              isRecalculate = true;
              break;

            }

            isRecalculate = false;

          }

        } else {

          isRecalculate = false;

        }

        arReceipt.setRctDescription(details.getRctDescription());
        arReceipt.setRctDocumentType(details.getRctDocumentType());
        arReceipt.setRctDate(details.getRctDate());
        arReceipt.setRctNumber(details.getRctNumber());
        arReceipt.setRctReferenceNumber(details.getRctReferenceNumber());
        arReceipt.setRctConversionDate(details.getRctConversionDate());
        arReceipt.setRctConversionRate(details.getRctConversionRate());
        arReceipt.setRctSoldTo(details.getRctSoldTo());
        arReceipt.setRctPaymentMethod(details.getRctPaymentMethod());
        arReceipt.setRctCustomerDeposit(details.getRctCustomerDeposit());
        arReceipt.setRctLastModifiedBy(details.getRctLastModifiedBy());
        arReceipt.setRctDateLastModified(details.getRctDateLastModified());
        arReceipt.setRctReasonForRejection(null);
        arReceipt.setRctSubjectToCommission(details.getRctSubjectToCommission());
        arReceipt.setRctCustomerName(null);
        arReceipt.setRctInvtrInvestorFund(details.getRctInvtrInvestorFund());
        arReceipt.setRctInvtrNextRunDate(details.getRctInvtrNextRunDate());
        arReceipt.setRctInvtrBeginningBalance(details.getRctInvtrBeginningBalance());

      }

      arReceipt.setRctDocumentType(details.getRctDocumentType());
      arReceipt.setReportParameter(details.getReportParameter());

      LocalAdBankAccount adBankAccount = adBankAccountHome.findByBaName(BA_NM, AD_CMPNY);
      // adBankAccount.addArReceipt(arReceipt);
      arReceipt.setAdBankAccount(adBankAccount);

      LocalGlFunctionalCurrency glFunctionalCurrency =
          glFunctionalCurrencyHome.findByFcName(FC_NM, AD_CMPNY);
      // glFunctionalCurrency.addArReceipt(arReceipt);
      arReceipt.setGlFunctionalCurrency(glFunctionalCurrency);

      LocalArCustomer arCustomer = arCustomerHome.findByCstCustomerCode(CST_CSTMR_CODE, AD_CMPNY);
      // arCustomer.addArReceipt(arReceipt);
      arReceipt.setArCustomer(arCustomer);

      if (details.getRctCustomerName().length() > 0
          && !arCustomer.getCstName().equals(details.getRctCustomerName())) {
        arReceipt.setRctCustomerName(details.getRctCustomerName());
      }

      LocalArTaxCode arTaxCode = arTaxCodeHome.findByTcName(TC_NM, AD_CMPNY);
      // arTaxCode.addArReceipt(arReceipt);
      arReceipt.setArTaxCode(arTaxCode);

      LocalArWithholdingTaxCode arWithholdingTaxCode =
          arWithholdingTaxCodeHome.findByWtcName(WTC_NM, AD_CMPNY);
      // arWithholdingTaxCode.addArReceipt(arReceipt);
      arReceipt.setArWithholdingTaxCode(arWithholdingTaxCode);

      LocalArSalesperson arSalesperson = null;

      if (SLP_SLSPRSN_CODE != null && SLP_SLSPRSN_CODE.length() > 0
          && !SLP_SLSPRSN_CODE.equalsIgnoreCase("NO RECORD FOUND")) {

        // if he tagged a salesperson for this receipt
        arSalesperson = arSalespersonHome.findBySlpSalespersonCode(SLP_SLSPRSN_CODE, AD_CMPNY);
        // arSalesperson.addArReceipt(arReceipt);
        arReceipt.setArSalesperson(arSalesperson);

      } else {

        // if he untagged a salesperson for this receipt
        if (arReceipt.getArSalesperson() != null) {

          arSalesperson = arSalespersonHome.findBySlpSalespersonCode(
              arReceipt.getArSalesperson().getSlpSalespersonCode(), AD_CMPNY);
          arSalesperson.dropArReceipt(arReceipt);

        }

        // if no salesperson is set, invoice should not be subject to commission
        arReceipt.setRctSubjectToCommission((byte) 0);

      }

      try {

        LocalArReceiptBatch arReceiptBatch =
            arReceiptBatchHome.findByRbName(RB_NM, AD_BRNCH, AD_CMPNY);
        // arReceiptBatch.addArReceipt(arReceipt);
        arReceipt.setArReceiptBatch(arReceiptBatch);

      } catch (FinderException ex) {

      }

      if (isRecalculate) {

        // remove all invoice line items

        Collection arInvoiceLineItems = arReceipt.getArInvoiceLineItems();

        Iterator i = arInvoiceLineItems.iterator();

        while (i.hasNext()) {

          LocalArInvoiceLineItem arInvoiceLineItem = (LocalArInvoiceLineItem) i.next();

          double convertedQuantity =
              this.convertByUomFromAndItemAndQuantity(arInvoiceLineItem.getInvUnitOfMeasure(),
                  arInvoiceLineItem.getInvItemLocation().getInvItem(),
                  arInvoiceLineItem.getIliQuantity(), AD_CMPNY);
          arInvoiceLineItem.getInvItemLocation().setIlCommittedQuantity(
              arInvoiceLineItem.getInvItemLocation().getIlCommittedQuantity() - convertedQuantity);

          Collection invTags = arInvoiceLineItem.getInvTags();

          Iterator x = invTags.iterator();

          while (x.hasNext()) {

            LocalInvTag invTag = (LocalInvTag) x.next();

            x.remove();

            invTag.remove();
          }


          i.remove();

          arInvoiceLineItem.remove();

        }

        // remove all invoice lines

        Collection arInvoiceLines = arReceipt.getArInvoiceLines();

        i = arInvoiceLines.iterator();

        while (i.hasNext()) {

          LocalArInvoiceLine arInvoiceLine = (LocalArInvoiceLine) i.next();

          i.remove();

          arInvoiceLine.remove();

        }

        // remove all distribution records

        Collection arDistributionRecords = arReceipt.getArDistributionRecords();

        i = arDistributionRecords.iterator();

        while (i.hasNext()) {

          LocalArDistributionRecord arDistributionRecord = (LocalArDistributionRecord) i.next();

          i.remove();

          arDistributionRecord.remove();

        }

        // add new invoice lines and distribution record

        double TOTAL_TAX = 0d;
        double TOTAL_LINE = 0d;
        double TOTAL_UNTAXABLE = 0d;


        i = ilList.iterator();

        while (i.hasNext()) {

          ArModInvoiceLineDetails mInvDetails = (ArModInvoiceLineDetails) i.next();

          LocalArInvoiceLine arInvoiceLine = this.addArIlEntry(mInvDetails, arReceipt, AD_CMPNY);

          // add revenue/credit distributions

          this.addArDrEntry(arReceipt.getArDrNextLine(), "REVENUE", EJBCommon.FALSE,
              arInvoiceLine.getIlAmount(),
              this.getArGlCoaRevenueAccount(arInvoiceLine, AD_BRNCH, AD_CMPNY), EJBCommon.FALSE,
              arReceipt, AD_BRNCH, AD_CMPNY);

          TOTAL_LINE += arInvoiceLine.getIlAmount();
          TOTAL_TAX += arInvoiceLine.getIlTaxAmount();

          if (arInvoiceLine.getIlTax() == EJBCommon.FALSE)
            TOTAL_UNTAXABLE += arInvoiceLine.getIlAmount();

        }


        // add tax distribution if necessary

        if (!arTaxCode.getTcType().equals("NONE") && !arTaxCode.getTcType().equals("EXEMPT")) {
          // add branch tax code
          LocalAdBranchArTaxCode adBranchTaxCode = null;
          Debug.print("ArInvoiceEntryControllerBean saveArInvIliEntry J");
          try {
            adBranchTaxCode = adBranchArTaxCodeHome
                .findBtcByTcCodeAndBrCode(arReceipt.getArTaxCode().getTcCode(), AD_BRNCH, AD_CMPNY);

          } catch (FinderException ex) {

          }

          if (adBranchTaxCode != null) {
            this.addArDrEntry(arReceipt.getArDrNextLine(), "TAX", EJBCommon.FALSE, TOTAL_TAX,
                adBranchTaxCode.getBtcGlCoaTaxCode(), EJBCommon.FALSE, arReceipt, AD_BRNCH,
                AD_CMPNY);

          } else {

            this.addArDrEntry(arReceipt.getArDrNextLine(), "TAX", EJBCommon.FALSE, TOTAL_TAX,
                arReceipt.getArTaxCode().getGlChartOfAccount().getCoaCode(), EJBCommon.FALSE,
                arReceipt, AD_BRNCH, AD_CMPNY);
          }


        }

        // add wtax distribution if necessary

        double W_TAX_AMOUNT = 0d;

        if (arWithholdingTaxCode.getWtcRate() != 0) {

          W_TAX_AMOUNT = EJBCommon.roundIt(
              (TOTAL_LINE - TOTAL_UNTAXABLE) * (arWithholdingTaxCode.getWtcRate() / 100),
              this.getGlFcPrecisionUnit(AD_CMPNY));

          this.addArDrEntry(arReceipt.getArDrNextLine(), "W-TAX", EJBCommon.TRUE, W_TAX_AMOUNT,
              arWithholdingTaxCode.getGlChartOfAccount().getCoaCode(), EJBCommon.FALSE, arReceipt,
              AD_BRNCH, AD_CMPNY);

        }


        // add cash distribution
        LocalAdBranchBankAccount adBranchBankAccount = null;
        Debug.print("ArInvoiceEntryControllerBean saveArInvIliEntry J");
        try {
          adBranchBankAccount = adBranchBankAccountHome.findBbaByBaCodeAndBrCode(
              arReceipt.getAdBankAccount().getBaCode(), AD_BRNCH, AD_CMPNY);

        } catch (FinderException ex) {

        }

        if (adBranchBankAccount != null) {

          this.addArDrEntry(arReceipt.getArDrNextLine(), "CASH", EJBCommon.TRUE,
              TOTAL_LINE + TOTAL_TAX - W_TAX_AMOUNT, adBranchBankAccount.getBbaGlCoaCashAccount(),
              EJBCommon.FALSE, arReceipt, AD_BRNCH, AD_CMPNY);


        } else {

          this.addArDrEntry(arReceipt.getArDrNextLine(), "CASH", EJBCommon.TRUE,
              TOTAL_LINE + TOTAL_TAX - W_TAX_AMOUNT,
              arReceipt.getAdBankAccount().getBaCoaGlCashAccount(), EJBCommon.FALSE, arReceipt,
              AD_BRNCH, AD_CMPNY);


        }



        // set receipt amount

        arReceipt.setRctAmount(TOTAL_LINE + TOTAL_TAX - W_TAX_AMOUNT);

      }

      // generate approval status

      String RCT_APPRVL_STATUS = null;

      if (!isDraft) {

        LocalAdApproval adApproval = adApprovalHome.findByAprAdCompany(AD_CMPNY);

        // check if ar receipt approval is enabled

        if (adApproval.getAprEnableArReceipt() == EJBCommon.FALSE) {

          RCT_APPRVL_STATUS = "N/A";

        } else {

          // check if receipt is self approved

          LocalAdAmountLimit adAmountLimit = null;

          try {

            adAmountLimit = adAmountLimitHome.findByAdcTypeAndAuTypeAndUsrName("AR RECEIPT",
                "REQUESTER", details.getRctLastModifiedBy(), AD_CMPNY);

          } catch (FinderException ex) {

            throw new GlobalNoApprovalRequesterFoundException();

          }

          if (arReceipt.getRctAmount() <= adAmountLimit.getCalAmountLimit()) {

            RCT_APPRVL_STATUS = "N/A";

          } else {

            // for approval, create approval queue

            Collection adAmountLimits = adAmountLimitHome.findByAdcTypeAndGreaterThanCalAmountLimit(
                "AR RECEIPT", adAmountLimit.getCalAmountLimit(), AD_CMPNY);

            if (adAmountLimits.isEmpty()) {

              Collection adApprovalUsers = adApprovalUserHome.findByAuTypeAndCalCode("APPROVER",
                  adAmountLimit.getCalCode(), AD_CMPNY);

              if (adApprovalUsers.isEmpty()) {

                throw new GlobalNoApprovalApproverFoundException();

              }

              Iterator j = adApprovalUsers.iterator();

              while (j.hasNext()) {

                LocalAdApprovalUser adApprovalUser = (LocalAdApprovalUser) j.next();

                LocalAdApprovalQueue adApprovalQueue =
                    adApprovalQueueHome.create(EJBCommon.TRUE, "AR RECEIPT", arReceipt.getRctCode(),
                        arReceipt.getRctNumber(), arReceipt.getRctDate(),
                        adAmountLimit.getCalAndOr(), adApprovalUser.getAuOr(), AD_BRNCH, AD_CMPNY);

                adApprovalUser.getAdUser().addAdApprovalQueue(adApprovalQueue);

              }

            } else {

              boolean isApprovalUsersFound = false;

              Iterator i = adAmountLimits.iterator();

              while (i.hasNext()) {

                LocalAdAmountLimit adNextAmountLimit = (LocalAdAmountLimit) i.next();

                if (arReceipt.getRctAmount() <= adNextAmountLimit.getCalAmountLimit()) {

                  Collection adApprovalUsers = adApprovalUserHome.findByAuTypeAndCalCode("APPROVER",
                      adAmountLimit.getCalCode(), AD_CMPNY);

                  Iterator j = adApprovalUsers.iterator();

                  while (j.hasNext()) {

                    isApprovalUsersFound = true;

                    LocalAdApprovalUser adApprovalUser = (LocalAdApprovalUser) j.next();

                    LocalAdApprovalQueue adApprovalQueue = adApprovalQueueHome.create(
                        EJBCommon.TRUE, "AR RECEIPT", arReceipt.getRctCode(),
                        arReceipt.getRctNumber(), arReceipt.getRctDate(),
                        adAmountLimit.getCalAndOr(), adApprovalUser.getAuOr(), AD_BRNCH, AD_CMPNY);

                    adApprovalUser.getAdUser().addAdApprovalQueue(adApprovalQueue);

                  }

                  break;

                } else if (!i.hasNext()) {

                  Collection adApprovalUsers = adApprovalUserHome.findByAuTypeAndCalCode("APPROVER",
                      adNextAmountLimit.getCalCode(), AD_CMPNY);

                  Iterator j = adApprovalUsers.iterator();

                  while (j.hasNext()) {

                    isApprovalUsersFound = true;

                    LocalAdApprovalUser adApprovalUser = (LocalAdApprovalUser) j.next();

                    LocalAdApprovalQueue adApprovalQueue =
                        adApprovalQueueHome.create(EJBCommon.TRUE, "AR RECEIPT",
                            arReceipt.getRctCode(), arReceipt.getRctNumber(),
                            arReceipt.getRctDate(), adNextAmountLimit.getCalAndOr(),
                            adApprovalUser.getAuOr(), AD_BRNCH, AD_CMPNY);

                    adApprovalUser.getAdUser().addAdApprovalQueue(adApprovalQueue);

                  }

                  break;

                }

                adAmountLimit = adNextAmountLimit;

              }

              if (!isApprovalUsersFound) {

                throw new GlobalNoApprovalApproverFoundException();

              }

            }

            RCT_APPRVL_STATUS = "PENDING";
          }
        }
      }

      LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);

      if (RCT_APPRVL_STATUS != null && RCT_APPRVL_STATUS.equals("N/A")
          && adPreference.getPrfArGlPostingType().equals("AUTO-POST UPON APPROVAL")) {

        this.executeArRctPost(arReceipt.getRctCode(), arReceipt.getRctLastModifiedBy(), AD_BRNCH,
            AD_CMPNY);

      }

      // set receipt approval status

      arReceipt.setRctApprovalStatus(RCT_APPRVL_STATUS);

      return arReceipt.getRctCode();


    } catch (GlobalRecordAlreadyDeletedException ex) {

      getSessionContext().setRollbackOnly();
      throw ex;

    } catch (GlobalDocumentNumberNotUniqueException ex) {

      getSessionContext().setRollbackOnly();
      throw ex;

    } catch (GlobalConversionDateNotExistException ex) {

      getSessionContext().setRollbackOnly();
      throw ex;

    } catch (GlobalTransactionAlreadyApprovedException ex) {

      getSessionContext().setRollbackOnly();
      throw ex;

    } catch (GlobalTransactionAlreadyPendingException ex) {

      getSessionContext().setRollbackOnly();
      throw ex;

    } catch (GlobalTransactionAlreadyPostedException ex) {

      getSessionContext().setRollbackOnly();
      throw ex;

    } catch (GlobalTransactionAlreadyVoidException ex) {

      getSessionContext().setRollbackOnly();
      throw ex;

    } catch (GlobalNoApprovalRequesterFoundException ex) {

      getSessionContext().setRollbackOnly();
      throw ex;

    } catch (GlobalNoApprovalApproverFoundException ex) {

      getSessionContext().setRollbackOnly();
      throw ex;

    } catch (GlJREffectiveDateNoPeriodExistException ex) {

      getSessionContext().setRollbackOnly();
      throw ex;

    } catch (GlJREffectiveDatePeriodClosedException ex) {

      getSessionContext().setRollbackOnly();
      throw ex;

    } catch (GlobalJournalNotBalanceException ex) {

      getSessionContext().setRollbackOnly();
      throw ex;

    } catch (GlobalBranchAccountNumberInvalidException ex) {

      getSessionContext().setRollbackOnly();
      throw ex;

    } catch (GlobalRecordAlreadyAssignedException ex) {

      getSessionContext().setRollbackOnly();
      throw ex;

    } catch (Exception ex) {

      Debug.printStackTrace(ex);
      getSessionContext().setRollbackOnly();
      throw new EJBException(ex.getMessage());

    }


  }

  /**
   * @ejb:interface-method view-type="remote"
   **/
  public Integer saveArRctIliEntry(
      com.util.ArReceiptDetails details, String BA_NM, String BA_CRD1_NM, String BA_CRD2_NM,
      String BA_CRD3_NM, String TC_NM, String WTC_NM, String FC_NM, String CST_CSTMR_CODE,
      String RB_NM, ArrayList iliList, boolean isDraft, String SLP_SLSPRSN_CODE, Integer AD_BRNCH,
      Integer AD_CMPNY
  ) throws GlobalRecordAlreadyDeletedException, GlobalDocumentNumberNotUniqueException,
      GlobalConversionDateNotExistException, GlobalTransactionAlreadyApprovedException,
      GlobalTransactionAlreadyPendingException, GlobalTransactionAlreadyPostedException,
      GlobalTransactionAlreadyVoidException, GlobalNoApprovalRequesterFoundException,
      GlobalNoApprovalApproverFoundException, GlobalInvItemLocationNotFoundException,
      GlJREffectiveDateNoPeriodExistException, GlJREffectiveDatePeriodClosedException,
      GlobalJournalNotBalanceException, GlobalInventoryDateException,
      GlobalBranchAccountNumberInvalidException, GlobalRecordAlreadyAssignedException,
      AdPRFCoaGlVarianceAccountNotFoundException, GlobalExpiryDateNotFoundException,
      GlobalMiscInfoIsRequiredException, GlobalRecordInvalidException {

    Debug.print("ArMiscReceiptEntryControllerBean saveArRctIliEntry");

    LocalArReceiptHome arReceiptHome = null;
    LocalArReceiptBatchHome arReceiptBatchHome = null;
    LocalAdCompanyHome adCompanyHome = null;
    LocalGlFunctionalCurrencyHome glFunctionalCurrencyHome = null;
    LocalAdDocumentSequenceAssignmentHome adDocumentSequenceAssignmentHome = null;
    LocalAdBranchDocumentSequenceAssignmentHome adBranchDocumentSequenceAssignmentHome = null;
    LocalAdBankAccountHome adBankAccountHome = null;
    LocalArTaxCodeHome arTaxCodeHome = null;
    LocalArWithholdingTaxCodeHome arWithholdingTaxCodeHome = null;
    LocalArCustomerHome arCustomerHome = null;
    LocalGlFunctionalCurrencyRateHome glFunctionalCurrencyRateHome = null;
    LocalAdApprovalHome adApprovalHome = null;
    LocalAdAmountLimitHome adAmountLimitHome = null;
    LocalAdApprovalUserHome adApprovalUserHome = null;
    LocalAdApprovalQueueHome adApprovalQueueHome = null;
    LocalAdPreferenceHome adPreferenceHome = null;
    LocalInvItemLocationHome invItemLocationHome = null;
    LocalInvCostingHome invCostingHome = null;
    LocalInvItemHome invItemHome = null;
    LocalGlChartOfAccountHome glChartOfAccountHome = null;
    LocalAdBranchItemLocationHome adBranchItemLocationHome = null;
    LocalArSalespersonHome arSalespersonHome = null;
    LocalArInvoiceLineItemHome arInvoiceLineItemHome = null;
    LocalAdBranchBankAccountHome adBranchBankAccountHome = null;
    LocalAdBranchArTaxCodeHome adBranchArTaxCodeHome = null;

    InvRepItemCostingControllerHome homeRIC = null;
    InvRepItemCostingController ejbRIC = null;
    
     InvItemEntryControllerHome homeII = null;
    InvItemEntryController ejbII = null;


    LocalArReceipt arReceipt = null;
    Date txnStartDate = new Date();
    // Initialize EJB Home

    try {
    
     homeII = (InvItemEntryControllerHome) com.util.EJBHomeFactory
          .lookUpHome("ejb/InvItemEntryControllerEJB", InvItemEntryControllerHome.class);   


      homeRIC = (InvRepItemCostingControllerHome) com.util.EJBHomeFactory
          .lookUpHome("ejb/InvRepItemCostingControllerEJB", InvRepItemCostingControllerHome.class);


      adBranchArTaxCodeHome = (LocalAdBranchArTaxCodeHome) EJBHomeFactory
          .lookUpLocalHome(LocalAdBranchArTaxCodeHome.JNDI_NAME, LocalAdBranchArTaxCodeHome.class);

      arReceiptHome = (LocalArReceiptHome) EJBHomeFactory
          .lookUpLocalHome(LocalArReceiptHome.JNDI_NAME, LocalArReceiptHome.class);
      arReceiptBatchHome = (LocalArReceiptBatchHome) EJBHomeFactory
          .lookUpLocalHome(LocalArReceiptBatchHome.JNDI_NAME, LocalArReceiptBatchHome.class);
      adCompanyHome = (LocalAdCompanyHome) EJBHomeFactory
          .lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
      glFunctionalCurrencyHome = (LocalGlFunctionalCurrencyHome) EJBHomeFactory.lookUpLocalHome(
          LocalGlFunctionalCurrencyHome.JNDI_NAME, LocalGlFunctionalCurrencyHome.class);
      adDocumentSequenceAssignmentHome = (LocalAdDocumentSequenceAssignmentHome) EJBHomeFactory
          .lookUpLocalHome(LocalAdDocumentSequenceAssignmentHome.JNDI_NAME,
              LocalAdDocumentSequenceAssignmentHome.class);
      adBranchDocumentSequenceAssignmentHome =
          (LocalAdBranchDocumentSequenceAssignmentHome) EJBHomeFactory.lookUpLocalHome(
              LocalAdBranchDocumentSequenceAssignmentHome.JNDI_NAME,
              LocalAdBranchDocumentSequenceAssignmentHome.class);
      adBankAccountHome = (LocalAdBankAccountHome) EJBHomeFactory
          .lookUpLocalHome(LocalAdBankAccountHome.JNDI_NAME, LocalAdBankAccountHome.class);
      arTaxCodeHome = (LocalArTaxCodeHome) EJBHomeFactory
          .lookUpLocalHome(LocalArTaxCodeHome.JNDI_NAME, LocalArTaxCodeHome.class);
      arWithholdingTaxCodeHome = (LocalArWithholdingTaxCodeHome) EJBHomeFactory.lookUpLocalHome(
          LocalArWithholdingTaxCodeHome.JNDI_NAME, LocalArWithholdingTaxCodeHome.class);
      arCustomerHome = (LocalArCustomerHome) EJBHomeFactory
          .lookUpLocalHome(LocalArCustomerHome.JNDI_NAME, LocalArCustomerHome.class);
      glFunctionalCurrencyRateHome =
          (LocalGlFunctionalCurrencyRateHome) EJBHomeFactory.lookUpLocalHome(
              LocalGlFunctionalCurrencyRateHome.JNDI_NAME, LocalGlFunctionalCurrencyRateHome.class);
      adApprovalHome = (LocalAdApprovalHome) EJBHomeFactory
          .lookUpLocalHome(LocalAdApprovalHome.JNDI_NAME, LocalAdApprovalHome.class);
      adAmountLimitHome = (LocalAdAmountLimitHome) EJBHomeFactory
          .lookUpLocalHome(LocalAdAmountLimitHome.JNDI_NAME, LocalAdAmountLimitHome.class);
      adApprovalUserHome = (LocalAdApprovalUserHome) EJBHomeFactory
          .lookUpLocalHome(LocalAdApprovalUserHome.JNDI_NAME, LocalAdApprovalUserHome.class);
      adApprovalQueueHome = (LocalAdApprovalQueueHome) EJBHomeFactory
          .lookUpLocalHome(LocalAdApprovalQueueHome.JNDI_NAME, LocalAdApprovalQueueHome.class);
      adPreferenceHome = (LocalAdPreferenceHome) EJBHomeFactory
          .lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);
      invItemLocationHome = (LocalInvItemLocationHome) EJBHomeFactory
          .lookUpLocalHome(LocalInvItemLocationHome.JNDI_NAME, LocalInvItemLocationHome.class);
      invCostingHome = (LocalInvCostingHome) EJBHomeFactory
          .lookUpLocalHome(LocalInvCostingHome.JNDI_NAME, LocalInvCostingHome.class);
      invItemHome = (LocalInvItemHome) EJBHomeFactory.lookUpLocalHome(LocalInvItemHome.JNDI_NAME,
          LocalInvItemHome.class);
      glChartOfAccountHome = (LocalGlChartOfAccountHome) EJBHomeFactory
          .lookUpLocalHome(LocalGlChartOfAccountHome.JNDI_NAME, LocalGlChartOfAccountHome.class);
      adBranchItemLocationHome = (LocalAdBranchItemLocationHome) EJBHomeFactory.lookUpLocalHome(
          LocalAdBranchItemLocationHome.JNDI_NAME, LocalAdBranchItemLocationHome.class);
      arSalespersonHome = (LocalArSalespersonHome) EJBHomeFactory
          .lookUpLocalHome(LocalArSalespersonHome.JNDI_NAME, LocalArSalespersonHome.class);
      arInvoiceLineItemHome = (LocalArInvoiceLineItemHome) EJBHomeFactory
          .lookUpLocalHome(LocalArInvoiceLineItemHome.JNDI_NAME, LocalArInvoiceLineItemHome.class);
      adBranchBankAccountHome = (LocalAdBranchBankAccountHome) EJBHomeFactory.lookUpLocalHome(
          LocalAdBranchBankAccountHome.JNDI_NAME, LocalAdBranchBankAccountHome.class);

    } catch (NamingException ex) {

      throw new EJBException(ex.getMessage());

    }

    try {
      ejbRIC = homeRIC.create();
      ejbII = homeII.create();
    } catch (RemoteException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    } catch (CreateException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }

    int lineNumberError = 0;

    try {


      // validate if misc receipt is already deleted

      try {

        if (details.getRctCode() != null) {

          arReceipt = arReceiptHome.findByPrimaryKey(details.getRctCode());

        }

      } catch (FinderException ex) {

        throw new GlobalRecordAlreadyDeletedException();

      }

      // validate if misc receipt is already posted, void, approved or pending

      if (details.getRctCode() != null && details.getRctVoid() == EJBCommon.FALSE) {

        if (arReceipt.getRctApprovalStatus() != null) {

          if (arReceipt.getRctApprovalStatus().equals("APPROVED")
              || arReceipt.getRctApprovalStatus().equals("N/A")) {

            throw new GlobalTransactionAlreadyApprovedException();

          } else if (arReceipt.getRctApprovalStatus().equals("PENDING")) {

            throw new GlobalTransactionAlreadyPendingException();

          }

        }

        if (arReceipt.getRctPosted() == EJBCommon.TRUE) {

          throw new GlobalTransactionAlreadyPostedException();

        } else if (arReceipt.getRctVoid() == EJBCommon.TRUE) {

          throw new GlobalTransactionAlreadyVoidException();

        }

      }

      // misc receipt is void

      if (details.getRctCode() != null && details.getRctVoid() == EJBCommon.TRUE) {

        if (arReceipt.getRctVoid() == EJBCommon.TRUE) {

          throw new GlobalTransactionAlreadyVoidException();

        }

        // check if receipt is already deposited

        if (!arReceipt.getCmFundTransferReceipts().isEmpty()) {

          throw new GlobalRecordAlreadyAssignedException();

        }
        Debug.print("ArMiscReceiptEntryControllerBean saveArRctIliEntry A");
        if (arReceipt.getRctPosted() == EJBCommon.TRUE) {

          // generate approval status

          String RCT_APPRVL_STATUS = null;

          if (!isDraft) {

            LocalAdApproval adApproval = adApprovalHome.findByAprAdCompany(AD_CMPNY);

            // check if ar receipt approval is enabled
            Debug.print("ArMiscReceiptEntryControllerBean saveArRctIliEntry B");
            if (adApproval.getAprEnableArReceipt() == EJBCommon.FALSE) {

              RCT_APPRVL_STATUS = "N/A";

            } else {

              // check if receipt is self approved

              LocalAdAmountLimit adAmountLimit = null;

              try {

                adAmountLimit = adAmountLimitHome.findByAdcTypeAndAuTypeAndUsrName("AR RECEIPT",
                    "REQUESTER", details.getRctLastModifiedBy(), AD_CMPNY);

              } catch (FinderException ex) {

                throw new GlobalNoApprovalRequesterFoundException();

              }

              if (arReceipt.getRctAmount() <= adAmountLimit.getCalAmountLimit()) {

                RCT_APPRVL_STATUS = "N/A";

              } else {

                // for approval, create approval queue

                Collection adAmountLimits =
                    adAmountLimitHome.findByAdcTypeAndGreaterThanCalAmountLimit("AR RECEIPT",
                        adAmountLimit.getCalAmountLimit(), AD_CMPNY);

                if (adAmountLimits.isEmpty()) {

                  Collection adApprovalUsers = adApprovalUserHome.findByAuTypeAndCalCode("APPROVER",
                      adAmountLimit.getCalCode(), AD_CMPNY);

                  if (adApprovalUsers.isEmpty()) {

                    throw new GlobalNoApprovalApproverFoundException();

                  }

                  Iterator j = adApprovalUsers.iterator();

                  while (j.hasNext()) {

                    LocalAdApprovalUser adApprovalUser = (LocalAdApprovalUser) j.next();

                    LocalAdApprovalQueue adApprovalQueue = adApprovalQueueHome.create(
                        EJBCommon.TRUE, "AR RECEIPT", arReceipt.getRctCode(),
                        arReceipt.getRctNumber(), arReceipt.getRctDate(),
                        adAmountLimit.getCalAndOr(), adApprovalUser.getAuOr(), AD_BRNCH, AD_CMPNY);

                    adApprovalUser.getAdUser().addAdApprovalQueue(adApprovalQueue);

                  }

                } else {

                  boolean isApprovalUsersFound = false;

                  Iterator i = adAmountLimits.iterator();

                  while (i.hasNext()) {

                    LocalAdAmountLimit adNextAmountLimit = (LocalAdAmountLimit) i.next();

                    if (arReceipt.getRctAmount() <= adNextAmountLimit.getCalAmountLimit()) {

                      Collection adApprovalUsers = adApprovalUserHome
                          .findByAuTypeAndCalCode("APPROVER", adAmountLimit.getCalCode(), AD_CMPNY);

                      Iterator j = adApprovalUsers.iterator();

                      while (j.hasNext()) {

                        isApprovalUsersFound = true;

                        LocalAdApprovalUser adApprovalUser = (LocalAdApprovalUser) j.next();

                        LocalAdApprovalQueue adApprovalQueue =
                            adApprovalQueueHome.create(EJBCommon.TRUE, "AR RECEIPT",
                                arReceipt.getRctCode(), arReceipt.getRctNumber(),
                                arReceipt.getRctDate(), adAmountLimit.getCalAndOr(),
                                adApprovalUser.getAuOr(), AD_BRNCH, AD_CMPNY);

                        adApprovalUser.getAdUser().addAdApprovalQueue(adApprovalQueue);

                      }

                      break;

                    } else if (!i.hasNext()) {

                      Collection adApprovalUsers = adApprovalUserHome.findByAuTypeAndCalCode(
                          "APPROVER", adNextAmountLimit.getCalCode(), AD_CMPNY);

                      Iterator j = adApprovalUsers.iterator();

                      while (j.hasNext()) {

                        isApprovalUsersFound = true;

                        LocalAdApprovalUser adApprovalUser = (LocalAdApprovalUser) j.next();

                        LocalAdApprovalQueue adApprovalQueue =
                            adApprovalQueueHome.create(EJBCommon.TRUE, "AR RECEIPT",
                                arReceipt.getRctCode(), arReceipt.getRctNumber(),
                                arReceipt.getRctDate(), adNextAmountLimit.getCalAndOr(),
                                adApprovalUser.getAuOr(), AD_BRNCH, AD_CMPNY);

                        adApprovalUser.getAdUser().addAdApprovalQueue(adApprovalQueue);

                      }

                      break;

                    }

                    adAmountLimit = adNextAmountLimit;

                  }

                  if (!isApprovalUsersFound) {

                    throw new GlobalNoApprovalApproverFoundException();

                  }

                }

                RCT_APPRVL_STATUS = "PENDING";
              }
            }
          }


          // reverse distribution records

          Collection arDistributionRecords = arReceipt.getArDistributionRecords();
          ArrayList list = new ArrayList();

          Iterator i = arDistributionRecords.iterator();

          while (i.hasNext()) {

            LocalArDistributionRecord arDistributionRecord = (LocalArDistributionRecord) i.next();

            list.add(arDistributionRecord);

          }

          i = list.iterator();

          Debug.print("ArMiscReceiptEntryControllerBean saveArRctIliEntry C");
          while (i.hasNext()) {

            LocalArDistributionRecord arDistributionRecord = (LocalArDistributionRecord) i.next();

            this.addArDrEntry(arReceipt.getArDrNextLine(), arDistributionRecord.getDrClass(),
                arDistributionRecord.getDrDebit() == EJBCommon.TRUE ? EJBCommon.FALSE
                    : EJBCommon.TRUE,
                arDistributionRecord.getDrAmount(),
                arDistributionRecord.getGlChartOfAccount().getCoaCode(), EJBCommon.TRUE, arReceipt,
                AD_BRNCH, AD_CMPNY);

          }

          LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);

          if (RCT_APPRVL_STATUS != null && RCT_APPRVL_STATUS.equals("N/A")
              && adPreference.getPrfArGlPostingType().equals("AUTO-POST UPON APPROVAL")) {

            arReceipt.setRctVoid(EJBCommon.TRUE);
            this.executeArRctPost(arReceipt.getRctCode(), details.getRctLastModifiedBy(), AD_BRNCH,
                AD_CMPNY);

          }

          // set void approval status

          arReceipt.setRctVoidApprovalStatus(RCT_APPRVL_STATUS);

        }

        arReceipt.setRctVoid(EJBCommon.TRUE);
        arReceipt.setRctLastModifiedBy(details.getRctLastModifiedBy());
        arReceipt.setRctDateLastModified(details.getRctDateLastModified());
        Debug.print("ArMiscReceiptEntryControllerBean saveArRctIliEntry D");
        return arReceipt.getRctCode();

      }

      // validate if receipt number is unique receipt number is automatic then set next sequence

      LocalAdBranchDocumentSequenceAssignment adBranchDocumentSequenceAssignment = null;
      LocalAdDocumentSequenceAssignment adDocumentSequenceAssignment = null;
      Debug.print("ArMiscReceiptEntryControllerBean saveArRctIliEntry E");
      if (details.getRctCode() == null) {


        String documentType = details.getRctDocumentType();

        try {
          if (documentType != null) {
            adDocumentSequenceAssignment =
                adDocumentSequenceAssignmentHome.findByDcName(documentType, AD_CMPNY);
          } else {
            documentType = "AR RECEIPT";
          }
        } catch (FinderException ex) {
          documentType = "AR RECEIPT";
        }



        try {

          adDocumentSequenceAssignment =
              adDocumentSequenceAssignmentHome.findByDcName(documentType, AD_CMPNY);

        } catch (FinderException ex) {

        }

        try {

          adBranchDocumentSequenceAssignment =
              adBranchDocumentSequenceAssignmentHome.findBdsByDsaCodeAndBrCode(
                  adDocumentSequenceAssignment.getDsaCode(), AD_BRNCH, AD_CMPNY);

        } catch (FinderException ex) {

        }

        LocalArReceipt arExistingReceipt = null;

        try {

          arExistingReceipt =
              arReceiptHome.findByRctNumberAndBrCode(details.getRctNumber(), AD_BRNCH, AD_CMPNY);

        } catch (FinderException ex) {
        }

        if (arExistingReceipt != null) {

          throw new GlobalDocumentNumberNotUniqueException();

        }

        if (adDocumentSequenceAssignment.getAdDocumentSequence().getDsNumberingType() == 'A'
            && (details.getRctNumber() == null || details.getRctNumber().trim().length() == 0)) {

          while (true) {

            if (adBranchDocumentSequenceAssignment == null
                || adBranchDocumentSequenceAssignment.getBdsNextSequence() == null) {

              try {

                arReceiptHome.findByRctNumberAndBrCode(
                    adDocumentSequenceAssignment.getDsaNextSequence(), AD_BRNCH, AD_CMPNY);
                adDocumentSequenceAssignment.setDsaNextSequence(EJBCommon
                    .incrementStringNumber(adDocumentSequenceAssignment.getDsaNextSequence()));

              } catch (FinderException ex) {

                details.setRctNumber(adDocumentSequenceAssignment.getDsaNextSequence());
                adDocumentSequenceAssignment.setDsaNextSequence(EJBCommon
                    .incrementStringNumber(adDocumentSequenceAssignment.getDsaNextSequence()));
                break;

              }

            } else {

              try {

                arReceiptHome.findByRctNumberAndBrCode(
                    adBranchDocumentSequenceAssignment.getBdsNextSequence(), AD_BRNCH, AD_CMPNY);
                adBranchDocumentSequenceAssignment
                    .setBdsNextSequence(EJBCommon.incrementStringNumber(
                        adBranchDocumentSequenceAssignment.getBdsNextSequence()));

              } catch (FinderException ex) {

                details.setRctNumber(adBranchDocumentSequenceAssignment.getBdsNextSequence());
                adBranchDocumentSequenceAssignment
                    .setBdsNextSequence(EJBCommon.incrementStringNumber(
                        adBranchDocumentSequenceAssignment.getBdsNextSequence()));
                break;

              }

            }

          }

        }
        Debug.print("ArMiscReceiptEntryControllerBean saveArRctIliEntry E Done");
      } else {

        LocalArReceipt arExistingReceipt = null;

        try {

          arExistingReceipt =
              arReceiptHome.findByRctNumberAndBrCode(details.getRctNumber(), AD_BRNCH, AD_CMPNY);

        } catch (FinderException ex) {
        }

        if (arExistingReceipt != null
            && !arExistingReceipt.getRctCode().equals(details.getRctCode())) {

          throw new GlobalDocumentNumberNotUniqueException();

        }

        if (arReceipt.getRctNumber() != details.getRctNumber()
            && (details.getRctNumber() == null || details.getRctNumber().trim().length() == 0)) {

          details.setRctNumber(arReceipt.getRctNumber());

        }
        Debug.print("ArMiscReceiptEntryControllerBean saveArRctIliEntry E02");
      }

      // validate if conversion date exists

      try {

        if (details.getRctConversionDate() != null) {


          LocalGlFunctionalCurrency glValidateFunctionalCurrency =
              glFunctionalCurrencyHome.findByFcName(FC_NM, AD_CMPNY);
          LocalGlFunctionalCurrencyRate glFunctionalCurrencyRate = glFunctionalCurrencyRateHome
              .findByFcCodeAndDate(glValidateFunctionalCurrency.getFcCode(),
                  details.getRctConversionDate(), AD_CMPNY);

        }

      } catch (FinderException ex) {

        throw new GlobalConversionDateNotExistException();

      }

      // used in checking if receipt should re-generate distribution records and re-calculate taxes
      boolean isRecalculate = true;

      // create misc receipt

      Debug.print("ArMiscReceiptEntryControllerBean saveArRctIliEntry F");
      if (details.getRctCode() == null) {

        arReceipt = arReceiptHome.create("MISC", details.getRctDescription(), details.getRctDate(),
            details.getRctNumber(), details.getRctReferenceNumber(), details.getRctCheckNo(),
            details.getRctPayfileReferenceNumber(), details.getRctChequeNumber(),
            details.getRctVoucherNumber(), details.getRctCardNumber1(), details.getRctCardNumber2(),
            details.getRctCardNumber3(), 0d, details.getRctAmountCash(),
            details.getRctAmountCheque(), details.getRctAmountVoucher(),
            details.getRctAmountCard1(), details.getRctAmountCard2(), details.getRctAmountCard3(),
            details.getRctConversionDate(), details.getRctConversionRate(), details.getRctSoldTo(),
            details.getRctPaymentMethod(), EJBCommon.FALSE, 0d, null, null, EJBCommon.FALSE, null,
            EJBCommon.FALSE, EJBCommon.FALSE, EJBCommon.FALSE, EJBCommon.FALSE, EJBCommon.FALSE,
            EJBCommon.FALSE, EJBCommon.FALSE, null, null, null, null, null,
            details.getRctCreatedBy(), details.getRctDateCreated(), details.getRctLastModifiedBy(),
            details.getRctDateLastModified(), null, null, null, null, EJBCommon.FALSE,
            details.getRctLvShift(), EJBCommon.FALSE, details.getRctSubjectToCommission(), null,
            null, details.getRctInvtrBeginningBalance(), EJBCommon.FALSE, null, AD_BRNCH, AD_CMPNY);

      } else {

        // check if critical fields are changed

        if (!arReceipt.getArTaxCode().getTcName().equals(TC_NM)
            || !arReceipt.getArWithholdingTaxCode().getWtcName().equals(WTC_NM)
            || !arReceipt.getArCustomer().getCstCustomerCode().equals(CST_CSTMR_CODE)
            || iliList.size() != arReceipt.getArInvoiceLineItems().size()
            || !(arReceipt.getRctDate().equals(details.getRctDate()))) {

          isRecalculate = true;

        } else if (iliList.size() == arReceipt.getArInvoiceLineItems().size()) {

          Iterator iliIter = arReceipt.getArInvoiceLineItems().iterator();
          Iterator iliListIter = iliList.iterator();

          while (iliIter.hasNext()) {

            LocalArInvoiceLineItem arInvoiceLineItem = (LocalArInvoiceLineItem) iliIter.next();
            ArModInvoiceLineItemDetails mdetails = (ArModInvoiceLineItemDetails) iliListIter.next();
            System.out.println("arInvoiceLineItem.getIliTotalDiscount()="
                + arInvoiceLineItem.getIliTotalDiscount());
            System.out.println("mdetails.getIliTotalDiscount()=" + mdetails.getIliTotalDiscount());
            if (!arInvoiceLineItem.getInvItemLocation().getInvItem().getIiName()
                .equals(mdetails.getIliIiName())
                || !arInvoiceLineItem.getInvItemLocation().getInvLocation().getLocName()
                    .equals(mdetails.getIliLocName())
                || !arInvoiceLineItem.getInvUnitOfMeasure().getUomName()
                    .equals(mdetails.getIliUomName())
                || arInvoiceLineItem.getIliQuantity() != mdetails.getIliQuantity()
                || arInvoiceLineItem.getIliUnitPrice() != mdetails.getIliUnitPrice()
                || arInvoiceLineItem.getIliTotalDiscount() != mdetails.getIliTotalDiscount()
                || arInvoiceLineItem.getIliTax() != mdetails.getIliTax()) {

              System.out.println("------------------->");
              isRecalculate = true;
              break;

            }



            // isRecalculate = false;

          }

        } else {

          // isRecalculate = false;

        }

        arReceipt.setRctDocumentType(details.getRctDocumentType());
        arReceipt.setRctDescription(details.getRctDescription());
        arReceipt.setRctDate(details.getRctDate());
        arReceipt.setRctNumber(details.getRctNumber());
        arReceipt.setRctReferenceNumber(details.getRctReferenceNumber());
        arReceipt.setRctConversionDate(details.getRctConversionDate());
        arReceipt.setRctConversionRate(details.getRctConversionRate());
        arReceipt.setRctSoldTo(details.getRctSoldTo());
        arReceipt.setRctPaymentMethod(details.getRctPaymentMethod());
        arReceipt.setRctLastModifiedBy(details.getRctLastModifiedBy());
        arReceipt.setRctDateLastModified(details.getRctDateLastModified());
        arReceipt.setRctReasonForRejection(null);
        arReceipt.setRctLvShift(details.getRctLvShift());
        arReceipt.setRctSubjectToCommission(details.getRctSubjectToCommission());
        arReceipt.setRctCustomerName(details.getRctCustomerName());
        arReceipt.setRctCustomerAddress(details.getRctCustomerAddress());


        arReceipt.setRctChequeNumber(details.getRctChequeNumber());
        arReceipt.setRctVoucherNumber(details.getRctVoucherNumber());
        arReceipt.setRctCardNumber1(details.getRctCardNumber1());
        arReceipt.setRctCardNumber2(details.getRctCardNumber2());
        arReceipt.setRctCardNumber3(details.getRctCardNumber3());

        arReceipt.setRctAmountCash(details.getRctAmountCash());
        arReceipt.setRctAmountCheque(details.getRctAmountCheque());
        arReceipt.setRctAmountVoucher(details.getRctAmountVoucher());
        arReceipt.setRctAmountCard1(details.getRctAmountCard1());
        arReceipt.setRctAmountCard2(details.getRctAmountCard2());
        arReceipt.setRctAmountCard3(details.getRctAmountCard3());
      }

      arReceipt.setRctDocumentType(details.getRctDocumentType());
      arReceipt.setReportParameter(details.getReportParameter());
      Debug.print("ArMiscReceiptEntryControllerBean saveArRctIliEntry  G");
      LocalAdBankAccount adBankAccount = adBankAccountHome.findByBaName(BA_NM, AD_CMPNY);
      arReceipt.setAdBankAccount(adBankAccount);
      System.out.println("BA_CRD1_NM=" + BA_CRD1_NM);
      if (!BA_CRD1_NM.equals("") && arReceipt.getRctAmountCard1() > 0) {

        LocalAdBankAccount adBankAccountCard1 =
            adBankAccountHome.findByBaName(BA_CRD1_NM, AD_CMPNY);
        arReceipt.setAdBankAccountCard1(adBankAccountCard1);

      }
      try {
        LocalAdBankAccount adBankAccountCard2 =
            adBankAccountHome.findByBaName(BA_CRD2_NM, AD_CMPNY);
        arReceipt.setAdBankAccountCard2(adBankAccountCard2);
      } catch (Exception ex) {
      }


      try {
        LocalAdBankAccount adBankAccountCard3 =
            adBankAccountHome.findByBaName(BA_CRD3_NM, AD_CMPNY);
        arReceipt.setAdBankAccountCard3(adBankAccountCard3);
      } catch (Exception ex) {
      }


      LocalGlFunctionalCurrency glFunctionalCurrency =
          glFunctionalCurrencyHome.findByFcName(FC_NM, AD_CMPNY);
      // glFunctionalCurrency.addArReceipt(arReceipt);
      arReceipt.setGlFunctionalCurrency(glFunctionalCurrency);

      LocalArCustomer arCustomer = arCustomerHome.findByCstCustomerCode(CST_CSTMR_CODE, AD_CMPNY);
      // arCustomer.addArReceipt(arReceipt);
      arReceipt.setArCustomer(arCustomer);

      if (details.getRctCustomerName().length() > 0
          && !arCustomer.getCstName().equals(details.getRctCustomerName())) {
        arReceipt.setRctCustomerName(details.getRctCustomerName());
      }

      System.out.println("txn details " + details.getRctCustomerName());
      System.out.println("txn receipt " + arReceipt.getRctCustomerName());

      LocalArTaxCode arTaxCode = arTaxCodeHome.findByTcName(TC_NM, AD_CMPNY);
      // arTaxCode.addArReceipt(arReceipt);
      arReceipt.setArTaxCode(arTaxCode);

      LocalArWithholdingTaxCode arWithholdingTaxCode =
          arWithholdingTaxCodeHome.findByWtcName(WTC_NM, AD_CMPNY);
      // arWithholdingTaxCode.addArReceipt(arReceipt);
      arReceipt.setArWithholdingTaxCode(arWithholdingTaxCode);

      LocalArSalesperson arSalesperson = null;

      if (SLP_SLSPRSN_CODE != null && SLP_SLSPRSN_CODE.length() > 0
          && !SLP_SLSPRSN_CODE.equalsIgnoreCase("NO RECORD FOUND")) {

        // if he tagged a salesperson for this receipt
        arSalesperson = arSalespersonHome.findBySlpSalespersonCode(SLP_SLSPRSN_CODE, AD_CMPNY);
        // arSalesperson.addArReceipt(arReceipt);
        arReceipt.setArSalesperson(arSalesperson);

      } else {

        // if he untagged a salesperson for this receipt
        if (arReceipt.getArSalesperson() != null) {

          arSalesperson = arSalespersonHome.findBySlpSalespersonCode(
              arReceipt.getArSalesperson().getSlpSalespersonCode(), AD_CMPNY);
          arSalesperson.dropArReceipt(arReceipt);

        }

        // if no salesperson is set, invoice should not be subject to commission
        arReceipt.setRctSubjectToCommission((byte) 0);

      }

      try {

        LocalArReceiptBatch arReceiptBatch =
            arReceiptBatchHome.findByRbName(RB_NM, AD_BRNCH, AD_CMPNY);
        // arReceiptBatch.addArReceipt(arReceipt);
        arReceipt.setArReceiptBatch(arReceiptBatch);

      } catch (FinderException ex) {

      }

      Debug.print("ArMiscReceiptEntryControllerBean saveArRctIliEntry  H");
      LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);
      System.out.println("isRecalculate=" + isRecalculate);
      if (isRecalculate) {
        // remove all voucher line items

        Collection arInvoiceLineItems = arReceipt.getArInvoiceLineItems();

        Iterator i = arInvoiceLineItems.iterator();

        while (i.hasNext()) {

          LocalArInvoiceLineItem arInvoiceLineItem = (LocalArInvoiceLineItem) i.next();
          if (arInvoiceLineItem.getIliEnableAutoBuild() == EJBCommon.TRUE && arInvoiceLineItem
              .getInvItemLocation().getInvItem().getIiClass().equals("Assembly")) {
            double QTY_SLD =
                this.convertByUomFromAndItemAndQuantity(arInvoiceLineItem.getInvUnitOfMeasure(),
                    arInvoiceLineItem.getInvItemLocation().getInvItem(),
                    arInvoiceLineItem.getIliQuantity(), AD_CMPNY);
  
            Collection invBillOfMaterials =
                arInvoiceLineItem.getInvItemLocation().getInvItem().getInvBillOfMaterials();

            Iterator j = invBillOfMaterials.iterator();

            while (j.hasNext()) {

              LocalInvBillOfMaterial invBillOfMaterial = (LocalInvBillOfMaterial) j.next();

              LocalInvItemLocation invItemLocation = invItemLocationHome.findByLocNameAndIiName(
                  invBillOfMaterial.getBomLocName(), invBillOfMaterial.getBomIiName(), AD_CMPNY);

              // bom conversion
              double convertedQuantity = this.convertByUomFromAndItemAndQuantity(
                  invBillOfMaterial.getInvUnitOfMeasure(), invItemLocation.getInvItem(),
                  EJBCommon.roundIt(invBillOfMaterial.getBomQuantityNeeded() * QTY_SLD,
                      this.getInvGpQuantityPrecisionUnit(AD_CMPNY)),
                  AD_CMPNY);
              Debug.print("ArMiscReceiptEntryControllerBean saveArRctIliEntry  H02");
              invItemLocation.setIlCommittedQuantity(
                  invItemLocation.getIlCommittedQuantity() - convertedQuantity);

            }

          } else {
            double convertedQuantity =
                this.convertByUomFromAndItemAndQuantity(arInvoiceLineItem.getInvUnitOfMeasure(),
                    arInvoiceLineItem.getInvItemLocation().getInvItem(),
                    arInvoiceLineItem.getIliQuantity(), AD_CMPNY);
            Debug.print("ArMiscReceiptEntryControllerBean saveArRctIliEntry  H03");
            arInvoiceLineItem.getInvItemLocation().setIlCommittedQuantity(
                arInvoiceLineItem.getInvItemLocation().getIlCommittedQuantity()
                    - convertedQuantity);
          }

          // remove all inv tag
          Collection invTags = arInvoiceLineItem.getInvTags();

          Iterator x = invTags.iterator();

          while (x.hasNext()) {

            LocalInvTag invTag = (LocalInvTag) x.next();

            x.remove();

            invTag.remove();
          }

          i.remove();

          arInvoiceLineItem.remove();

        }

        // remove all invoice lines
        Collection arInvoiceLines = arReceipt.getArInvoiceLines();

        i = arInvoiceLines.iterator();

        while (i.hasNext()) {

          LocalArInvoiceLine arInvoiceLine = (LocalArInvoiceLine) i.next();

          i.remove();

          arInvoiceLine.remove();

        }

        // remove all distribution records

        Collection arDistributionRecords = arReceipt.getArDistributionRecords();

        i = arDistributionRecords.iterator();

        while (i.hasNext()) {

          LocalArDistributionRecord arDistributionRecord = (LocalArDistributionRecord) i.next();

          i.remove();

          arDistributionRecord.remove();

        }
        // add new voucher line item and distribution record

        double TOTAL_TAX = 0d;
        double TOTAL_LINE = 0d;
        double TOTAL_SA_CREDIT = 0d;
        double TOTAL_DISCOUNT = 0d;

        i = iliList.iterator();

        LocalInvItemLocation invItemLocation = null;

        Debug.print("ArMiscReceiptEntryControllerBean saveArRctIliEntry  I");
        while (i.hasNext()) {

          ArModInvoiceLineItemDetails mIliDetails = (ArModInvoiceLineItemDetails) i.next();

          try {

            invItemLocation = invItemLocationHome.findByLocNameAndIiName(
                mIliDetails.getIliLocName(), mIliDetails.getIliIiName(), AD_CMPNY);
            Debug.print("ArMiscReceiptEntryControllerBean saveArRctIliEntry  I01");
          } catch (FinderException ex) {

            throw new GlobalInvItemLocationNotFoundException(
                String.valueOf(mIliDetails.getIliLine()));

          }

          // start date validation
          if (adPreference.getPrfArAllowPriorDate() == EJBCommon.FALSE) {
            Collection invNegTxnCosting =
                invCostingHome.findNegTxnByGreaterThanCstDateAndIiNameAndLocName(
                    arReceipt.getRctDate(), invItemLocation.getInvItem().getIiName(),
                    invItemLocation.getInvLocation().getLocName(), AD_BRNCH, AD_CMPNY);
            if (!invNegTxnCosting.isEmpty())
              throw new GlobalInventoryDateException(invItemLocation.getInvItem().getIiName());
          }

          LocalArInvoiceLineItem arInvoiceLineItem =
              this.addArIliEntry(mIliDetails, arReceipt, invItemLocation, AD_CMPNY);



          // add cost of sales distribution and inventory
 /*
          double COST = arInvoiceLineItem.getInvItemLocation().getInvItem().getIiUnitCost();

          try {


           
             * LocalInvCosting invCosting = invCostingHome.
             * getByMaxCstDateToLongAndMaxCstLineNumberAndLessThanEqualCstDateAndRemainingQuantityNotEqualToZeroAndIlCode(
             * arReceipt.getRctDate(), invItemLocation.getIlCode(), AD_BRNCH, AD_CMPNY);
             */

            /*
            LocalInvCosting invCosting = invCostingHome
                .getByMaxCstDateToLongAndMaxCstLineNumberAndLessThanEqualCstDateAndIiNameAndLocName(
                    arReceipt.getRctDate(), invItemLocation.getInvItem().getIiName(),
                    invItemLocation.getInvLocation().getLocName(), AD_BRNCH, AD_CMPNY);

            // check if rmning vl is not zero and rmng qty is 0
            System.out.println("COGS--------------------------------------->");
            System.out.println(
                "invCosting.getCstRemainingQuantity()=" + invCosting.getCstRemainingQuantity());
            System.out
                .println("invCosting.getCstRemainingValue()=" + invCosting.getCstRemainingValue());

            if (invCosting.getCstRemainingQuantity() <= 0
                && invCosting.getCstRemainingValue() <= 0) {
              System.out.println("RE CALC");
              HashMap criteria = new HashMap();
              criteria.put("itemName", invItemLocation.getInvItem().getIiName());
              criteria.put("location", invItemLocation.getInvLocation().getLocName());

              ArrayList branchList = new ArrayList();

              AdBranchDetails mdetails = new AdBranchDetails();
              mdetails.setBrCode(AD_BRNCH);
              branchList.add(mdetails);

              ejbRIC.executeInvFixItemCosting(criteria, branchList, AD_CMPNY);

              invCosting = invCostingHome
                  .getByMaxCstDateToLongAndMaxCstLineNumberAndLessThanEqualCstDateAndIiNameAndLocName(
                      arReceipt.getRctDate(), invItemLocation.getInvItem().getIiName(),
                      invItemLocation.getInvLocation().getLocName(), AD_BRNCH, AD_CMPNY);
            }

            if (invCosting.getInvItemLocation().getInvItem().getIiCostMethod().equals("Average"))

              COST = invCosting.getCstRemainingQuantity() <= 0
                  ? invCosting.getInvItemLocation().getInvItem().getIiUnitCost()
                  : Math.abs(
                      invCosting.getCstRemainingValue() / invCosting.getCstRemainingQuantity());

            if (COST <= 0) {
              COST = arInvoiceLineItem.getInvItemLocation().getInvItem().getIiUnitCost();
            }

            else if (invCosting.getInvItemLocation().getInvItem().getIiCostMethod().equals("FIFO"))

              COST = Math.abs(this.getInvFifoCost(invCosting.getCstDate(),
                  invCosting.getInvItemLocation().getIlCode(), arInvoiceLineItem.getIliQuantity(),
                  arInvoiceLineItem.getIliUnitPrice(), false, AD_BRNCH, AD_CMPNY));

            else if (invCosting.getInvItemLocation().getInvItem().getIiCostMethod()
                .equals("Standard"))

              COST = invCosting.getInvItemLocation().getInvItem().getIiUnitCost();

            
              
         
          } catch (FinderException ex) {

            COST = arInvoiceLineItem.getInvItemLocation().getInvItem().getIiUnitCost();

          }
            */
         double COST = ejbII.getInvIiUnitCostByIiNameAndUomName( invItemLocation.getInvItem().getIiName(), arInvoiceLineItem.getInvUnitOfMeasure().getUomName(), arReceipt.getRctDate(), AD_CMPNY);
              
       

          double QTY_SLD =
              this.convertByUomFromAndItemAndQuantity(arInvoiceLineItem.getInvUnitOfMeasure(),
                  arInvoiceLineItem.getInvItemLocation().getInvItem(),
                  arInvoiceLineItem.getIliQuantity(), AD_CMPNY);
          Debug.print("ArMiscReceiptEntryControllerBean saveArRctIliEntry  I02");
          LocalAdBranchItemLocation adBranchItemLocation = null;

          try {
            lineNumberError = arInvoiceLineItem.getIliLine();
            adBranchItemLocation = adBranchItemLocationHome.findBilByIlCodeAndBrCode(
                arInvoiceLineItem.getInvItemLocation().getIlCode(), AD_BRNCH, AD_CMPNY);

          } catch (FinderException ex) {

          }

          if ((arInvoiceLineItem.getIliEnableAutoBuild() == 0
              || arInvoiceLineItem.getInvItemLocation().getInvItem().getIiClass().equals("Stock"))
              && arInvoiceLineItem.getInvItemLocation().getInvItem()
                  .getIiNonInventoriable() == EJBCommon.FALSE) {

            if (adBranchItemLocation != null) {

              this.addArDrIliEntry(arReceipt.getArDrNextLine(), "COGS", EJBCommon.TRUE,
                  COST * QTY_SLD, adBranchItemLocation.getBilCoaGlCostOfSalesAccount(), arReceipt,
                  AD_BRNCH, AD_CMPNY);

              this.addArDrIliEntry(arReceipt.getArDrNextLine(), "INVENTORY", EJBCommon.FALSE,
                  COST * QTY_SLD, adBranchItemLocation.getBilCoaGlInventoryAccount(), arReceipt,
                  AD_BRNCH, AD_CMPNY);

            } else {

              this.addArDrIliEntry(arReceipt.getArDrNextLine(), "COGS", EJBCommon.TRUE,
                  COST * QTY_SLD,
                  arInvoiceLineItem.getInvItemLocation().getIlGlCoaCostOfSalesAccount(), arReceipt,
                  AD_BRNCH, AD_CMPNY);

              this.addArDrIliEntry(arReceipt.getArDrNextLine(), "INVENTORY", EJBCommon.FALSE,
                  COST * QTY_SLD,
                  arInvoiceLineItem.getInvItemLocation().getIlGlCoaInventoryAccount(), arReceipt,
                  AD_BRNCH, AD_CMPNY);

            }

            // add quantity to item location committed quantity

            double convertedQuantity =
                this.convertByUomFromAndItemAndQuantity(arInvoiceLineItem.getInvUnitOfMeasure(),
                    arInvoiceLineItem.getInvItemLocation().getInvItem(),
                    arInvoiceLineItem.getIliQuantity(), AD_CMPNY);
            invItemLocation.setIlCommittedQuantity(
                invItemLocation.getIlCommittedQuantity() + convertedQuantity);
            Debug.print("ArMiscReceiptEntryControllerBean saveArRctIliEntry  I03");
          }

          // add inventory sale distributions

          if (adBranchItemLocation != null) {

            this.addArDrIliEntry(arReceipt.getArDrNextLine(), "REVENUE", EJBCommon.FALSE,
                arInvoiceLineItem.getIliAmount(), adBranchItemLocation.getBilCoaGlSalesAccount(),
                arReceipt, AD_BRNCH, AD_CMPNY);

          } else {

            this.addArDrIliEntry(arReceipt.getArDrNextLine(), "REVENUE", EJBCommon.FALSE,
                arInvoiceLineItem.getIliAmount(),
                arInvoiceLineItem.getInvItemLocation().getIlGlCoaSalesAccount(), arReceipt,
                AD_BRNCH, AD_CMPNY);

          }

          TOTAL_LINE += arInvoiceLineItem.getIliAmount();
          TOTAL_TAX += arInvoiceLineItem.getIliTaxAmount();
          TOTAL_DISCOUNT += arInvoiceLineItem.getIliTotalDiscount();



        }

        // add tax distribution if necessary

        if (!arTaxCode.getTcType().equals("NONE") && !arTaxCode.getTcType().equals("EXEMPT")) {

          LocalAdBranchArTaxCode adBranchTaxCode = null;
          Debug.print("ArInvoiceEntryControllerBean saveArInvIliEntry J");
          try {
            adBranchTaxCode = adBranchArTaxCodeHome
                .findBtcByTcCodeAndBrCode(arReceipt.getArTaxCode().getTcCode(), AD_BRNCH, AD_CMPNY);

          } catch (FinderException ex) {

          }

          if (adBranchTaxCode != null) {
            this.addArDrEntry(arReceipt.getArDrNextLine(), "TAX", EJBCommon.FALSE, TOTAL_TAX,
                adBranchTaxCode.getBtcGlCoaTaxCode(), EJBCommon.FALSE, arReceipt, AD_BRNCH,
                AD_CMPNY);

          } else {

            this.addArDrEntry(arReceipt.getArDrNextLine(), "TAX", EJBCommon.FALSE, TOTAL_TAX,
                arReceipt.getArTaxCode().getGlChartOfAccount().getCoaCode(), EJBCommon.FALSE,
                arReceipt, AD_BRNCH, AD_CMPNY);
          }
          Debug.print("ArMiscReceiptEntryControllerBean saveArRctIliEntry  J09");
        }

        // add wtax distribution if necessary

        double W_TAX_AMOUNT = 0d;

        if (arWithholdingTaxCode.getWtcRate() != 0) {

          W_TAX_AMOUNT = EJBCommon.roundIt(TOTAL_LINE * (arWithholdingTaxCode.getWtcRate() / 100),
              this.getGlFcPrecisionUnit(AD_CMPNY));

          this.addArDrEntry(arReceipt.getArDrNextLine(), "W-TAX", EJBCommon.TRUE, W_TAX_AMOUNT,
              arWithholdingTaxCode.getGlChartOfAccount().getCoaCode(), EJBCommon.FALSE, arReceipt,
              AD_BRNCH, AD_CMPNY);
          Debug.print("ArMiscReceiptEntryControllerBean saveArRctIliEntry  J10");
        }



        // add cash distribution
        LocalAdBranchBankAccount adBranchBankAccount = null;
        LocalAdBranchBankAccount adBranchBankAccountCard1 = null;
        LocalAdBranchBankAccount adBranchBankAccountCard2 = null;
        LocalAdBranchBankAccount adBranchBankAccountCard3 = null;


        if (arReceipt.getRctAmountCard1() > 0) {

          try {
            adBranchBankAccountCard1 = adBranchBankAccountHome.findBbaByBaCodeAndBrCode(
                arReceipt.getAdBankAccountCard1().getBaCode(), AD_BRNCH, AD_CMPNY);

          } catch (FinderException ex) {
          }


          if (adBranchBankAccount != null) {

            this.addArDrEntry(arReceipt.getArDrNextLine(), "CARD 1", EJBCommon.TRUE,
                arReceipt.getRctAmountCard1(), adBranchBankAccountCard1.getBbaGlCoaCashAccount(),
                EJBCommon.FALSE, arReceipt, AD_BRNCH, AD_CMPNY);

          } else {

            this.addArDrEntry(arReceipt.getArDrNextLine(), "CARD 1", EJBCommon.TRUE,
                arReceipt.getRctAmountCard1(),
                arReceipt.getAdBankAccountCard1().getBaCoaGlCashAccount(), EJBCommon.FALSE,
                arReceipt, AD_BRNCH, AD_CMPNY);

          }
        }

        if (arReceipt.getRctAmountCard2() > 0) {

          try {
            adBranchBankAccountCard2 = adBranchBankAccountHome.findBbaByBaCodeAndBrCode(
                arReceipt.getAdBankAccountCard2().getBaCode(), AD_BRNCH, AD_CMPNY);

          } catch (FinderException ex) {
          }

          if (adBranchBankAccount != null) {

            this.addArDrEntry(arReceipt.getArDrNextLine(), "CARD 2", EJBCommon.TRUE,
                arReceipt.getRctAmountCard2(), adBranchBankAccountCard2.getBbaGlCoaCashAccount(),
                EJBCommon.FALSE, arReceipt, AD_BRNCH, AD_CMPNY);

          } else {

            this.addArDrEntry(arReceipt.getArDrNextLine(), "CARD 2", EJBCommon.TRUE,
                arReceipt.getRctAmountCard2(),
                arReceipt.getAdBankAccountCard2().getBaCoaGlCashAccount(), EJBCommon.FALSE,
                arReceipt, AD_BRNCH, AD_CMPNY);

          }
        }


        if (arReceipt.getRctAmountCard3() > 0) {

          try {
            adBranchBankAccountCard3 = adBranchBankAccountHome.findBbaByBaCodeAndBrCode(
                arReceipt.getAdBankAccountCard3().getBaCode(), AD_BRNCH, AD_CMPNY);

          } catch (FinderException ex) {
          }

          if (adBranchBankAccount != null) {

            this.addArDrEntry(arReceipt.getArDrNextLine(), "CARD 3", EJBCommon.TRUE,
                arReceipt.getRctAmountCard3(), adBranchBankAccountCard3.getBbaGlCoaCashAccount(),
                EJBCommon.FALSE, arReceipt, AD_BRNCH, AD_CMPNY);

          } else {

            this.addArDrEntry(arReceipt.getArDrNextLine(), "CARD 3", EJBCommon.TRUE,
                arReceipt.getRctAmountCard3(),
                arReceipt.getAdBankAccountCard3().getBaCoaGlCashAccount(), EJBCommon.FALSE,
                arReceipt, AD_BRNCH, AD_CMPNY);

          }
        }

        try {
          adBranchBankAccount = adBranchBankAccountHome.findBbaByBaCodeAndBrCode(
              arReceipt.getAdBankAccount().getBaCode(), AD_BRNCH, AD_CMPNY);

        } catch (FinderException ex) {
        }

        if (adBranchBankAccount != null) {

          this.addArDrEntry(arReceipt.getArDrNextLine(), "CASH", EJBCommon.TRUE,
              TOTAL_LINE + TOTAL_TAX - W_TAX_AMOUNT - TOTAL_DISCOUNT
                  - arReceipt.getRctAmountVoucher() - arReceipt.getRctAmountCheque()
                  - arReceipt.getRctAmountCard1() - arReceipt.getRctAmountCard2()
                  - arReceipt.getRctAmountCard3(),
              adBranchBankAccount.getBbaGlCoaCashAccount(), EJBCommon.FALSE, arReceipt, AD_BRNCH,
              AD_CMPNY);

          if (arReceipt.getRctAmountCheque() > 0) {
            this.addArDrEntry(arReceipt.getArDrNextLine(), "CHEQUE", EJBCommon.TRUE,
                arReceipt.getRctAmountCheque(), adBranchBankAccount.getBbaGlCoaCashAccount(),
                EJBCommon.FALSE, arReceipt, AD_BRNCH, AD_CMPNY);

          }

          if (arReceipt.getRctAmountVoucher() > 0) {

            this.addArDrEntry(arReceipt.getArDrNextLine(), "VOUCHER", EJBCommon.TRUE,
                arReceipt.getRctAmountVoucher(),
                adBranchBankAccount.getBbaGlCoaSalesDiscountAccount(), EJBCommon.FALSE, arReceipt,
                AD_BRNCH, AD_CMPNY);

          }

          if (TOTAL_DISCOUNT > 0) {
            this.addArDrEntry(arReceipt.getArDrNextLine(), "SALES DISCOUNT", EJBCommon.TRUE,
                TOTAL_DISCOUNT, adBranchBankAccount.getBbaGlCoaSalesDiscountAccount(),
                EJBCommon.FALSE, arReceipt, AD_BRNCH, AD_CMPNY);

          }

        } else {

          this.addArDrEntry(arReceipt.getArDrNextLine(), "CASH", EJBCommon.TRUE,
              TOTAL_LINE + TOTAL_TAX - W_TAX_AMOUNT - TOTAL_DISCOUNT
                  - arReceipt.getRctAmountVoucher() - arReceipt.getRctAmountCheque()
                  - arReceipt.getRctAmountCard1() - arReceipt.getRctAmountCard2()
                  - arReceipt.getRctAmountCard3(),
              arReceipt.getAdBankAccount().getBaCoaGlCashAccount(), EJBCommon.FALSE, arReceipt,
              AD_BRNCH, AD_CMPNY);

          if (arReceipt.getRctAmountCheque() > 0) {

            this.addArDrEntry(arReceipt.getArDrNextLine(), "CHEQUE", EJBCommon.TRUE,
                arReceipt.getRctAmountCheque(),
                arReceipt.getAdBankAccount().getBaCoaGlCashAccount(), EJBCommon.FALSE, arReceipt,
                AD_BRNCH, AD_CMPNY);

          }

          if (arReceipt.getRctAmountVoucher() > 0) {

            this.addArDrEntry(arReceipt.getArDrNextLine(), "VOUCHER", EJBCommon.TRUE,
                arReceipt.getRctAmountVoucher(),
                arReceipt.getAdBankAccount().getBaCoaGlSalesDiscount(), EJBCommon.FALSE, arReceipt,
                AD_BRNCH, AD_CMPNY);

          }


          if (TOTAL_DISCOUNT > 0) {
            this.addArDrEntry(arReceipt.getArDrNextLine(), "SALES DISCOUNT", EJBCommon.TRUE,
                TOTAL_DISCOUNT, arReceipt.getAdBankAccount().getBaCoaGlSalesDiscount(),
                EJBCommon.FALSE, arReceipt, AD_BRNCH, AD_CMPNY);

          }

        }

        Debug.print("ArMiscReceiptEntryControllerBean saveArRctIliEntry  J11");
        // set receipt amount

        arReceipt.setRctAmount(TOTAL_LINE + TOTAL_TAX - W_TAX_AMOUNT);
        arReceipt.setRctAmountCash(arReceipt.getRctAmountCash() == 0 ? arReceipt.getRctAmount()
            : arReceipt.getRctAmountCash());

      } else {


        // InvTag reupdate



        Iterator i = iliList.iterator();

        LocalInvItemLocation invItemLocation = null;

        Debug.print("ArMiscReceiptEntryControllerBean saveArRctIliEntry  K");
        while (i.hasNext()) {



          ArModInvoiceLineItemDetails mIliDetails = (ArModInvoiceLineItemDetails) i.next();



          try {
            invItemLocation = invItemLocationHome.findByLocNameAndIiName(
                mIliDetails.getIliLocName(), mIliDetails.getIliIiName(), AD_CMPNY);
            try {
              LocalArInvoiceLineItem arInvoiceLineItem =
                  arInvoiceLineItemHome.findByRctCodeAndIlCodeAndUomName(arReceipt.getRctCode(),
                      invItemLocation.getIlCode(), mIliDetails.getIliUomName(), AD_CMPNY);
              arInvoiceLineItem.setIliEnableAutoBuild(mIliDetails.getIliEnableAutoBuild());
              Debug.print("ArMiscReceiptEntryControllerBean saveArRctIliEntry  K01");
            } catch (FinderException ex) {
            }

          } catch (FinderException ex) {

            throw new GlobalInvItemLocationNotFoundException(
                String.valueOf(mIliDetails.getIliLine()));

          }



          // start date validation
          if (adPreference.getPrfArAllowPriorDate() == EJBCommon.FALSE) {
            Collection invNegTxnCosting =
                invCostingHome.findNegTxnByGreaterThanCstDateAndIiNameAndLocName(
                    arReceipt.getRctDate(), invItemLocation.getInvItem().getIiName(),
                    invItemLocation.getInvLocation().getLocName(), AD_BRNCH, AD_CMPNY);
            Debug.print("ArMiscReceiptEntryControllerBean saveArRctIliEntry  K02");
            if (!invNegTxnCosting.isEmpty())
              throw new GlobalInventoryDateException(invItemLocation.getInvItem().getIiName());
          }


          if (mIliDetails.getIliEnableAutoBuild() == EJBCommon.TRUE
              && invItemLocation.getInvItem().getIiClass().equals("Assembly")) {
            Collection invBillOfMaterials = invItemLocation.getInvItem().getInvBillOfMaterials();

            Iterator j = invBillOfMaterials.iterator();

            while (j.hasNext()) {

              LocalInvBillOfMaterial invBillOfMaterial = (LocalInvBillOfMaterial) j.next();

              LocalInvItemLocation invIlRawMaterial = null;

              try {

                invIlRawMaterial = invItemLocationHome.findByLocNameAndIiName(
                    invBillOfMaterial.getBomLocName(), invBillOfMaterial.getBomIiName(), AD_CMPNY);
                Debug.print("ArMiscReceiptEntryControllerBean saveArRctIliEntry  K03");
              } catch (FinderException ex) {

                throw new GlobalInvItemLocationNotFoundException(
                    String.valueOf(mIliDetails.getIliLine()) + " - Raw Mat. ("
                        + invBillOfMaterial.getBomIiName() + ")");

              }

              // start date validation
              if (adPreference.getPrfArAllowPriorDate() == EJBCommon.FALSE) {
                Collection invIlRawMatNegTxnCosting =
                    invCostingHome.findNegTxnByGreaterThanCstDateAndIiNameAndLocName(
                        arReceipt.getRctDate(), invIlRawMaterial.getInvItem().getIiName(),
                        invIlRawMaterial.getInvLocation().getLocName(), AD_BRNCH, AD_CMPNY);
                Debug.print("ArMiscReceiptEntryControllerBean saveArRctIliEntry  K04");
                if (!invIlRawMatNegTxnCosting.isEmpty())
                  throw new GlobalInventoryDateException(invItemLocation.getInvItem().getIiName()
                      + " - Raw Mat. (" + invIlRawMaterial.getInvItem().getIiName() + ")");
              }

            }

          }

        }

      }

      // insufficient stock checking
      Debug.print("ArMiscReceiptEntryControllerBean saveArRctIliEntry  L");
      // if (adPreference.getPrfArAllowPriorDate() == EJBCommon.TRUE && !isDraft) {
      if (adPreference.getPrfArCheckInsufficientStock() == EJBCommon.TRUE && !isDraft) {
        boolean hasInsufficientItems = false;
        String insufficientItems = "";

        Collection arInvoiceLineItems = arReceipt.getArInvoiceLineItems();

        Iterator i = arInvoiceLineItems.iterator();

        HashMap cstMap = new HashMap();

        while (i.hasNext()) {

          LocalArInvoiceLineItem arInvoiceLineItem = (LocalArInvoiceLineItem) i.next();
          String II_NM = arInvoiceLineItem.getInvItemLocation().getInvItem().getIiName();
          String LOC_NM = arInvoiceLineItem.getInvItemLocation().getInvLocation().getLocName();
          if (arInvoiceLineItem.getIliEnableAutoBuild() == EJBCommon.TRUE && arInvoiceLineItem
              .getInvItemLocation().getInvItem().getIiClass().equals("Assembly")) {

            Collection invBillOfMaterials =
                arInvoiceLineItem.getInvItemLocation().getInvItem().getInvBillOfMaterials();

            Iterator j = invBillOfMaterials.iterator();

            while (j.hasNext()) {

              LocalInvBillOfMaterial invBillOfMaterial = (LocalInvBillOfMaterial) j.next();

              LocalInvItemLocation invItemLocation = invItemLocationHome.findByLocNameAndIiName(
                  invBillOfMaterial.getBomLocName(), invBillOfMaterial.getBomIiName(), AD_CMPNY);

              double ILI_QTY =
                  this.convertByUomFromAndItemAndQuantity(arInvoiceLineItem.getInvUnitOfMeasure(),
                      arInvoiceLineItem.getInvItemLocation().getInvItem(),
                      Math.abs(arInvoiceLineItem.getIliQuantity()), AD_CMPNY);
              Debug.print("ArMiscReceiptEntryControllerBean saveArRctIliEntry  L01");


              LocalInvItem bomItm =
                  invItemHome.findByIiName(invBillOfMaterial.getBomIiName(), AD_CMPNY);

              double NEEDED_QTY =
                  this.convertByUomFromAndItemAndQuantity(invBillOfMaterial.getInvUnitOfMeasure(),
                      bomItm, invBillOfMaterial.getBomQuantityNeeded(), AD_CMPNY);

              double CURR_QTY = 0;

              boolean isIlFound = false;

              if (cstMap.containsKey(invItemLocation.getIlCode().toString())) {

                isIlFound = true;
                CURR_QTY =
                    ((Double) cstMap.get(invItemLocation.getIlCode().toString())).doubleValue();

              } else {

                try {

                  LocalInvCosting invBomCosting = invCostingHome
                      .getByMaxCstDateToLongAndMaxCstLineNumberAndLessThanEqualCstDateAndIlCode(
                          arReceipt.getRctDate(), invItemLocation.getIlCode(), AD_BRNCH, AD_CMPNY);

                  if (invBomCosting != null) {

                    CURR_QTY = this.convertByUomAndQuantity(
                        invBomCosting.getInvItemLocation().getInvItem().getInvUnitOfMeasure(),
                        invBomCosting.getInvItemLocation().getInvItem(),
                        invBomCosting.getCstRemainingQuantity(), AD_CMPNY);
                    // System.out.println("invBomCosting: " + invBomCosting.getCstCode());
                    Debug.print("ArMiscReceiptEntryControllerBean saveArRctIliEntry  L04");
                  }



                  Debug.print("ArMiscReceiptEntryControllerBean saveArRctIliEntry  L02");

                } catch (FinderException ex) {


                }

              }

              if (CURR_QTY == 0 || CURR_QTY < (NEEDED_QTY * ILI_QTY)) {

                hasInsufficientItems = true;

                insufficientItems += arInvoiceLineItem.getInvItemLocation().getInvItem().getIiName()
                    + "-" + invBillOfMaterial.getBomIiName() + ", ";
              }

              CURR_QTY -= (NEEDED_QTY * ILI_QTY);

              if (!isIlFound) {
                cstMap.remove(invItemLocation.getIlCode().toString());
              }

              cstMap.put(invItemLocation.getIlCode().toString(), new Double(CURR_QTY));

              Debug.print("ArMiscReceiptEntryControllerBean saveArRctIliEntry  L03");



            }
          } else if (arInvoiceLineItem.getInvItemLocation().getInvItem()
              .getIiNonInventoriable() == EJBCommon.FALSE) {

            LocalInvCosting invCosting = null;
            double ILI_QTY = this.convertByUomAndQuantity(arInvoiceLineItem.getInvUnitOfMeasure(),
                arInvoiceLineItem.getInvItemLocation().getInvItem(),
                Math.abs(arInvoiceLineItem.getIliQuantity()), AD_CMPNY);

            double CURR_QTY = 0;
            boolean isIlFound = false;

            if (cstMap.containsKey(arInvoiceLineItem.getInvItemLocation().getIlCode().toString())) {
              Debug.print("ArMiscReceiptEntryControllerBean saveArRctIliEntry  L04 - A");
              isIlFound = true;
              CURR_QTY = ((Double) cstMap
                  .get(arInvoiceLineItem.getInvItemLocation().getIlCode().toString()))
                      .doubleValue();

            } else {

              try {

                /*
                 * LocalInvCosting invCosting = invCostingHome.
                 * getByMaxCstDateToLongAndMaxCstLineNumberAndLessThanEqualCstDateAndIlCode(
                 * arReceipt.getRctDate(), arInvoiceLineItem.getInvItemLocation().getIlCode(),
                 * AD_BRNCH, AD_CMPNY);
                 */
                invCosting = invCostingHome
                    .getByMaxCstDateToLongAndMaxCstLineNumberAndLessThanEqualCstDateAndIiNameAndLocName(
                        arReceipt.getRctDate(), II_NM, LOC_NM, AD_BRNCH, AD_CMPNY);
                Debug.print("ArMiscReceiptEntryControllerBean saveArRctIliEntry  L04");
              } catch (FinderException ex) {
                System.out.println("CATCH");
              }
            }

            if (invCosting != null) {

              CURR_QTY = this.convertByUomAndQuantity(
                  arInvoiceLineItem.getInvItemLocation().getInvItem().getInvUnitOfMeasure(),
                  arInvoiceLineItem.getInvItemLocation().getInvItem(),
                  invCosting.getCstRemainingQuantity(), AD_CMPNY);

              Debug.print("ArMiscReceiptEntryControllerBean saveArRctIliEntry  L05");
            }

            double LOWEST_QTY =
                this.convertByUomAndQuantity(arInvoiceLineItem.getInvUnitOfMeasure(),
                    arInvoiceLineItem.getInvItemLocation().getInvItem(), 1, AD_CMPNY);
            Debug.print("ArMiscReceiptEntryControllerBean saveArRctIliEntry  L06");
            System.out.println("CURRENT QTY: " + CURR_QTY);
            System.out.println("ILI QTY: " + ILI_QTY);
            System.out.println("LOWEST_QTY: " + LOWEST_QTY);
            System.out.println("invCosting: " + invCosting);
            if ((invCosting == null && isIlFound == false) || CURR_QTY == 0
                || CURR_QTY - ILI_QTY <= -LOWEST_QTY) {
              hasInsufficientItems = true;
              System.out.println("Insufficient Item: "
                  + arInvoiceLineItem.getInvItemLocation().getInvItem().getIiName());
              insufficientItems +=
                  arInvoiceLineItem.getInvItemLocation().getInvItem().getIiName() + ", ";
            }

            CURR_QTY -= ILI_QTY;

            if (isIlFound) {
              cstMap.remove(arInvoiceLineItem.getInvItemLocation().getIlCode().toString());
            }

            cstMap.put(arInvoiceLineItem.getInvItemLocation().getIlCode().toString(),
                new Double(CURR_QTY));
          }
        }
        if (hasInsufficientItems) {

          throw new GlobalRecordInvalidException(
              insufficientItems.substring(0, insufficientItems.lastIndexOf(",")));
        }
      }

      // generate approval status

      String RCT_APPRVL_STATUS = null;

      if (!isDraft) {

        LocalAdApproval adApproval = adApprovalHome.findByAprAdCompany(AD_CMPNY);

        // check if ar receipt approval is enabled

        if (adApproval.getAprEnableArReceipt() == EJBCommon.FALSE) {

          RCT_APPRVL_STATUS = "N/A";

        } else {

          // check if receipt is self approved

          LocalAdAmountLimit adAmountLimit = null;

          try {

            adAmountLimit = adAmountLimitHome.findByAdcTypeAndAuTypeAndUsrName("AR RECEIPT",
                "REQUESTER", details.getRctLastModifiedBy(), AD_CMPNY);

          } catch (FinderException ex) {

            throw new GlobalNoApprovalRequesterFoundException();

          }

          if (arReceipt.getRctAmount() <= adAmountLimit.getCalAmountLimit()) {

            RCT_APPRVL_STATUS = "N/A";

          } else {

            // for approval, create approval queue

            Collection adAmountLimits = adAmountLimitHome.findByAdcTypeAndGreaterThanCalAmountLimit(
                "AR RECEIPT", adAmountLimit.getCalAmountLimit(), AD_CMPNY);

            if (adAmountLimits.isEmpty()) {

              Collection adApprovalUsers = adApprovalUserHome.findByAuTypeAndCalCode("APPROVER",
                  adAmountLimit.getCalCode(), AD_CMPNY);

              if (adApprovalUsers.isEmpty()) {

                throw new GlobalNoApprovalApproverFoundException();

              }

              Iterator j = adApprovalUsers.iterator();

              while (j.hasNext()) {

                LocalAdApprovalUser adApprovalUser = (LocalAdApprovalUser) j.next();

                LocalAdApprovalQueue adApprovalQueue =
                    adApprovalQueueHome.create(EJBCommon.TRUE, "AR RECEIPT", arReceipt.getRctCode(),
                        arReceipt.getRctNumber(), arReceipt.getRctDate(),
                        adAmountLimit.getCalAndOr(), adApprovalUser.getAuOr(), AD_BRNCH, AD_CMPNY);

                adApprovalUser.getAdUser().addAdApprovalQueue(adApprovalQueue);

              }

            } else {

              boolean isApprovalUsersFound = false;

              Iterator i = adAmountLimits.iterator();

              while (i.hasNext()) {

                LocalAdAmountLimit adNextAmountLimit = (LocalAdAmountLimit) i.next();

                if (arReceipt.getRctAmount() <= adNextAmountLimit.getCalAmountLimit()) {

                  Collection adApprovalUsers = adApprovalUserHome.findByAuTypeAndCalCode("APPROVER",
                      adAmountLimit.getCalCode(), AD_CMPNY);

                  Iterator j = adApprovalUsers.iterator();

                  while (j.hasNext()) {

                    isApprovalUsersFound = true;

                    LocalAdApprovalUser adApprovalUser = (LocalAdApprovalUser) j.next();

                    LocalAdApprovalQueue adApprovalQueue = adApprovalQueueHome.create(
                        EJBCommon.TRUE, "AR RECEIPT", arReceipt.getRctCode(),
                        arReceipt.getRctNumber(), arReceipt.getRctDate(),
                        adAmountLimit.getCalAndOr(), adApprovalUser.getAuOr(), AD_BRNCH, AD_CMPNY);

                    adApprovalUser.getAdUser().addAdApprovalQueue(adApprovalQueue);

                  }

                  break;

                } else if (!i.hasNext()) {

                  Collection adApprovalUsers = adApprovalUserHome.findByAuTypeAndCalCode("APPROVER",
                      adNextAmountLimit.getCalCode(), AD_CMPNY);

                  Iterator j = adApprovalUsers.iterator();

                  while (j.hasNext()) {

                    isApprovalUsersFound = true;

                    LocalAdApprovalUser adApprovalUser = (LocalAdApprovalUser) j.next();

                    LocalAdApprovalQueue adApprovalQueue =
                        adApprovalQueueHome.create(EJBCommon.TRUE, "AR RECEIPT",
                            arReceipt.getRctCode(), arReceipt.getRctNumber(),
                            arReceipt.getRctDate(), adNextAmountLimit.getCalAndOr(),
                            adApprovalUser.getAuOr(), AD_BRNCH, AD_CMPNY);

                    adApprovalUser.getAdUser().addAdApprovalQueue(adApprovalQueue);

                  }

                  break;

                }

                adAmountLimit = adNextAmountLimit;

              }

              if (!isApprovalUsersFound) {

                throw new GlobalNoApprovalApproverFoundException();

              }

            }

            RCT_APPRVL_STATUS = "PENDING";
          }
        }
      }

      if (RCT_APPRVL_STATUS != null && RCT_APPRVL_STATUS.equals("N/A")
          && adPreference.getPrfArGlPostingType().equals("AUTO-POST UPON APPROVAL")) {

        this.executeArRctPost(arReceipt.getRctCode(), arReceipt.getRctLastModifiedBy(), AD_BRNCH,
            AD_CMPNY);

      }

      // set receipt approval status

      arReceipt.setRctApprovalStatus(RCT_APPRVL_STATUS);

      Debug.print("ArMiscReceiptEntryControllerBean saveArRctIliEntry " + txnStartDate);

      return arReceipt.getRctCode();


    } catch (GlobalRecordAlreadyDeletedException ex) {

      getSessionContext().setRollbackOnly();
      throw ex;

    } catch (GlobalDocumentNumberNotUniqueException ex) {

      getSessionContext().setRollbackOnly();
      throw ex;

    } catch (GlobalConversionDateNotExistException ex) {

      getSessionContext().setRollbackOnly();
      throw ex;

    } catch (GlobalTransactionAlreadyApprovedException ex) {

      getSessionContext().setRollbackOnly();
      throw ex;

    } catch (GlobalTransactionAlreadyPendingException ex) {

      getSessionContext().setRollbackOnly();
      throw ex;

    } catch (GlobalTransactionAlreadyPostedException ex) {

      getSessionContext().setRollbackOnly();
      throw ex;

    } catch (GlobalTransactionAlreadyVoidException ex) {

      getSessionContext().setRollbackOnly();
      throw ex;

    } catch (GlobalNoApprovalRequesterFoundException ex) {

      getSessionContext().setRollbackOnly();
      throw ex;

    } catch (GlobalNoApprovalApproverFoundException ex) {

      getSessionContext().setRollbackOnly();
      throw ex;

    } catch (GlobalInvItemLocationNotFoundException ex) {

      getSessionContext().setRollbackOnly();
      throw ex;

    } catch (GlJREffectiveDateNoPeriodExistException ex) {

      getSessionContext().setRollbackOnly();
      throw ex;

    } catch (GlJREffectiveDatePeriodClosedException ex) {

      getSessionContext().setRollbackOnly();
      throw ex;

    } catch (GlobalJournalNotBalanceException ex) {

      getSessionContext().setRollbackOnly();
      throw ex;

    } catch (GlobalInventoryDateException ex) {

      getSessionContext().setRollbackOnly();
      throw ex;

    } catch (GlobalBranchAccountNumberInvalidException ex) {

      // Retrieve Line Number
      ex.setLineNumberError(lineNumberError);
      getSessionContext().setRollbackOnly();
      throw ex;

    } catch (GlobalRecordAlreadyAssignedException ex) {

      getSessionContext().setRollbackOnly();
      throw ex;

    } catch (GlobalRecordInvalidException ex) {

      getSessionContext().setRollbackOnly();
      throw ex;

    } catch (AdPRFCoaGlVarianceAccountNotFoundException ex) {

      getSessionContext().setRollbackOnly();
      throw ex;

    } catch (GlobalExpiryDateNotFoundException ex) {

      getSessionContext().setRollbackOnly();
      throw ex;

    } catch (GlobalMiscInfoIsRequiredException ex) {

      getSessionContext().setRollbackOnly();
      throw ex;

    } catch (Exception ex) {

      Debug.printStackTrace(ex);
      getSessionContext().setRollbackOnly();
      throw new EJBException(ex.getMessage());

    }


  }


  /**
   * @ejb:interface-method view-type="remote"
   * @jboss:method-attributes read-only="true"
   **/
  public ArrayList getAdUsrAll(
      Integer AD_CMPNY
  ) {

    Debug.print("ArMiscReceiptEntryControllerBean getAdUsrAll");

    LocalAdUserHome adUserHome = null;

    LocalAdUser adUser = null;

    Collection adUsers = null;

    ArrayList list = new ArrayList();

    // Initialize EJB Home

    try {

      adUserHome = (LocalAdUserHome) EJBHomeFactory.lookUpLocalHome(LocalAdUserHome.JNDI_NAME,
          LocalAdUserHome.class);

    } catch (NamingException ex) {

      throw new EJBException(ex.getMessage());

    }

    try {

      adUsers = adUserHome.findUsrAll(AD_CMPNY);

    } catch (FinderException ex) {

    } catch (Exception ex) {

      throw new EJBException(ex.getMessage());
    }

    if (adUsers.isEmpty()) {

      return null;

    }

    Iterator i = adUsers.iterator();

    while (i.hasNext()) {

      adUser = (LocalAdUser) i.next();

      list.add(adUser.getUsrName());

    }

    return list;

  }



  /**
   * @ejb:interface-method view-type="remote"
   **/
  public void deleteArRctEntry(
      Integer RCT_CODE, String AD_USR, Integer AD_CMPNY
  ) throws GlobalRecordAlreadyDeletedException {

    Debug.print("ArMiscReceiptEntryControllerBean deleteArRctEntry");

    LocalArReceiptHome arReceiptHome = null;
    LocalAdApprovalQueueHome adApprovalQueueHome = null;
    LocalAdPreferenceHome adPreferenceHome = null;
    LocalInvItemLocationHome invItemLocationHome = null;
    LocalAdDeleteAuditTrailHome adDeleteAuditTrailHome = null;

    // Initialize EJB Home

    try {

      arReceiptHome = (LocalArReceiptHome) EJBHomeFactory
          .lookUpLocalHome(LocalArReceiptHome.JNDI_NAME, LocalArReceiptHome.class);
      adApprovalQueueHome = (LocalAdApprovalQueueHome) EJBHomeFactory
          .lookUpLocalHome(LocalAdApprovalQueueHome.JNDI_NAME, LocalAdApprovalQueueHome.class);
      adPreferenceHome = (LocalAdPreferenceHome) EJBHomeFactory
          .lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);
      invItemLocationHome = (LocalInvItemLocationHome) EJBHomeFactory
          .lookUpLocalHome(LocalInvItemLocationHome.JNDI_NAME, LocalInvItemLocationHome.class);
      adDeleteAuditTrailHome = (LocalAdDeleteAuditTrailHome) EJBHomeFactory.lookUpLocalHome(
          LocalAdDeleteAuditTrailHome.JNDI_NAME, LocalAdDeleteAuditTrailHome.class);

    } catch (NamingException ex) {

      throw new EJBException(ex.getMessage());

    }

    try {

      LocalArReceipt arReceipt = arReceiptHome.findByPrimaryKey(RCT_CODE);

      LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);

      Collection arInvoiceLineItems = arReceipt.getArInvoiceLineItems();

      Iterator j = arInvoiceLineItems.iterator();

      while (j.hasNext()) {

        LocalArInvoiceLineItem arInvoiceLineItem = (LocalArInvoiceLineItem) j.next();

        if (arInvoiceLineItem.getIliEnableAutoBuild() == EJBCommon.TRUE && arInvoiceLineItem
            .getInvItemLocation().getInvItem().getIiClass().equals("Assembly")) {

          double QTY_SLD =
              this.convertByUomFromAndItemAndQuantity(arInvoiceLineItem.getInvUnitOfMeasure(),
                  arInvoiceLineItem.getInvItemLocation().getInvItem(),
                  arInvoiceLineItem.getIliQuantity(), AD_CMPNY);


          System.out.println(
              "ITEM IS in: " + arInvoiceLineItem.getInvItemLocation().getInvItem().getIiName());
          Collection invBillOfMaterials =
              arInvoiceLineItem.getInvItemLocation().getInvItem().getInvBillOfMaterials();

          Iterator k = invBillOfMaterials.iterator();

          while (k.hasNext()) {

            LocalInvBillOfMaterial invBillOfMaterial = (LocalInvBillOfMaterial) k.next();

            LocalInvItemLocation invItemLocation = invItemLocationHome.findByLocNameAndIiName(
                invBillOfMaterial.getBomLocName(), invBillOfMaterial.getBomIiName(), AD_CMPNY);

            // bom conversion
            double convertedQuantity = this.convertByUomFromAndItemAndQuantity(
                invBillOfMaterial.getInvUnitOfMeasure(), invItemLocation.getInvItem(),
                EJBCommon.roundIt(invBillOfMaterial.getBomQuantityNeeded() * QTY_SLD,
                    this.getInvGpQuantityPrecisionUnit(AD_CMPNY)),
                AD_CMPNY);

            invItemLocation.setIlCommittedQuantity(
                invItemLocation.getIlCommittedQuantity() - convertedQuantity);

          }

        } else {

          double convertedQuantity =
              this.convertByUomFromAndItemAndQuantity(arInvoiceLineItem.getInvUnitOfMeasure(),
                  arInvoiceLineItem.getInvItemLocation().getInvItem(),
                  arInvoiceLineItem.getIliQuantity(), AD_CMPNY);
          arInvoiceLineItem.getInvItemLocation().setIlCommittedQuantity(
              arInvoiceLineItem.getInvItemLocation().getIlCommittedQuantity() - convertedQuantity);

        }

      }

      if (arReceipt.getRctApprovalStatus() != null
          && arReceipt.getRctApprovalStatus().equals("PENDING")) {

        Collection adApprovalQueues = adApprovalQueueHome
            .findByAqDocumentAndAqDocumentCode("AR RECEIPT", arReceipt.getRctCode(), AD_CMPNY);

        Iterator i = adApprovalQueues.iterator();

        while (i.hasNext()) {

          LocalAdApprovalQueue adApprovalQueue = (LocalAdApprovalQueue) i.next();

          adApprovalQueue.remove();

        }

      }


      adDeleteAuditTrailHome.create("AR RECEIPT", arReceipt.getRctDate(), arReceipt.getRctNumber(),
          arReceipt.getRctReferenceNumber(), arReceipt.getRctAmount(), AD_USR, new Date(),
          AD_CMPNY);

      arReceipt.remove();

    } catch (FinderException ex) {

      getSessionContext().setRollbackOnly();
      throw new GlobalRecordAlreadyDeletedException();

    } catch (Exception ex) {

      Debug.printStackTrace(ex);
      getSessionContext().setRollbackOnly();
      throw new EJBException(ex.getMessage());

    }

  }

  /**
   * @ejb:interface-method view-type="remote"
   * @jboss:method-attributes read-only="true"
   **/
  public short getGlFcPrecisionUnit(
      Integer AD_CMPNY
  ) {

    Debug.print("ArMiscReceiptEntryControllerBean getGlFcPrecisionUnit");


    LocalAdCompanyHome adCompanyHome = null;


    // Initialize EJB Home

    try {

      adCompanyHome = (LocalAdCompanyHome) EJBHomeFactory
          .lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);

    } catch (NamingException ex) {

      throw new EJBException(ex.getMessage());

    }

    try {

      LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);

      return adCompany.getGlFunctionalCurrency().getFcPrecision();

    } catch (Exception ex) {

      Debug.printStackTrace(ex);
      throw new EJBException(ex.getMessage());

    }

  }

  /**
   * @ejb:interface-method view-type="remote"
   * @jboss:method-attributes read-only="true"
   **/
  public ArrayList getAdApprovalNotifiedUsersByRctCode(
      Integer RCT_CODE, Integer AD_CMPNY
  ) {

    Debug.print("ArMiscReceiptEntryControllerBean getAdApprovalNotifiedUsersByRctCode");


    LocalAdApprovalQueueHome adApprovalQueueHome = null;
    LocalArReceiptHome arReceiptHome = null;

    ArrayList list = new ArrayList();


    // Initialize EJB Home

    try {

      adApprovalQueueHome = (LocalAdApprovalQueueHome) EJBHomeFactory
          .lookUpLocalHome(LocalAdApprovalQueueHome.JNDI_NAME, LocalAdApprovalQueueHome.class);
      arReceiptHome = (LocalArReceiptHome) EJBHomeFactory
          .lookUpLocalHome(LocalArReceiptHome.JNDI_NAME, LocalArReceiptHome.class);

    } catch (NamingException ex) {

      throw new EJBException(ex.getMessage());

    }

    try {

      LocalArReceipt arReceipt = arReceiptHome.findByPrimaryKey(RCT_CODE);

      if (arReceipt.getRctVoid() == EJBCommon.FALSE && arReceipt.getRctPosted() == EJBCommon.TRUE) {

        list.add("DOCUMENT POSTED");
        return list;

      } else if (arReceipt.getRctVoid() == EJBCommon.TRUE
          && arReceipt.getRctVoidPosted() == EJBCommon.TRUE) {

        list.add("DOCUMENT POSTED");
        return list;

      }

      Collection adApprovalQueues =
          adApprovalQueueHome.findByAqDocumentAndAqDocumentCode("AR RECEIPT", RCT_CODE, AD_CMPNY);

      Iterator i = adApprovalQueues.iterator();

      while (i.hasNext()) {

        LocalAdApprovalQueue adApprovalQueue = (LocalAdApprovalQueue) i.next();

        list.add(adApprovalQueue.getAdUser().getUsrDescription());

      }

      return list;

    } catch (Exception ex) {

      Debug.printStackTrace(ex);
      throw new EJBException(ex.getMessage());

    }

  }


  /**
   * @ejb:interface-method view-type="remote"
   * @jboss:method-attributes read-only="true"
   **/
  public byte getAdPrfEnableArMiscReceiptBatch(
      Integer AD_CMPNY
  ) {

    Debug.print("ArMiscReceiptEntryControllerBean getAdPrfEnableArReceiptBatch");

    LocalAdPreferenceHome adPreferenceHome = null;


    // Initialize EJB Home

    try {

      adPreferenceHome = (LocalAdPreferenceHome) EJBHomeFactory
          .lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);

    } catch (NamingException ex) {

      throw new EJBException(ex.getMessage());

    }


    try {

      LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);

      return adPreference.getPrfEnableArMiscReceiptBatch();

    } catch (Exception ex) {

      Debug.printStackTrace(ex);
      throw new EJBException(ex.getMessage());

    }

  }

  /**
   * @ejb:interface-method view-type="remote"
   * @jboss:method-attributes read-only="true"
   **/
  public byte getAdPrfEnableInvShift(
      Integer AD_CMPNY
  ) {

    Debug.print("ArMiscReceiptEntryControllerBean getAdPrfEnableInvShift");

    LocalAdPreferenceHome adPreferenceHome = null;


    // Initialize EJB Home

    try {

      adPreferenceHome = (LocalAdPreferenceHome) EJBHomeFactory
          .lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);

    } catch (NamingException ex) {

      throw new EJBException(ex.getMessage());

    }


    try {

      LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);

      return adPreference.getPrfInvEnableShift();

    } catch (Exception ex) {

      Debug.printStackTrace(ex);
      throw new EJBException(ex.getMessage());

    }

  }


  /**
   * @ejb:interface-method view-type="remote"
   * @jboss:method-attributes read-only="true"
   **/
  public ArrayList getArOpenRbAll(
      Integer AD_BRNCH, Integer AD_CMPNY
  ) {

    Debug.print("ArMiscReceiptEntryControllerBean getArOpenRbAll");

    LocalArReceiptBatchHome arReceiptBatchHome = null;

    ArrayList list = new ArrayList();

    // Initialize EJB Home

    try {

      arReceiptBatchHome = (LocalArReceiptBatchHome) EJBHomeFactory
          .lookUpLocalHome(LocalArReceiptBatchHome.JNDI_NAME, LocalArReceiptBatchHome.class);

    } catch (NamingException ex) {

      throw new EJBException(ex.getMessage());

    }

    try {

      Collection arReceiptBatches =
          arReceiptBatchHome.findOpenRbByRbType("MISC", AD_BRNCH, AD_CMPNY);

      Iterator i = arReceiptBatches.iterator();

      while (i.hasNext()) {

        LocalArReceiptBatch arReceiptBatch = (LocalArReceiptBatch) i.next();

        list.add(arReceiptBatch.getRbName());

      }

      return list;

    } catch (Exception ex) {

      Debug.printStackTrace(ex);
      throw new EJBException(ex.getMessage());

    }

  }

  /**
   * @ejb:interface-method view-type="remote"
   * @jboss:method-attributes read-only="true"
   **/
  public short getInvGpQuantityPrecisionUnit(
      Integer AD_CMPNY
  ) {

    Debug.print("ArMiscReceiptEntryControllerBean getInvGpQuantityPrecisionUnit");

    LocalAdPreferenceHome adPreferenceHome = null;

    // Initialize EJB Home

    try {

      adPreferenceHome = (LocalAdPreferenceHome) EJBHomeFactory
          .lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);

    } catch (NamingException ex) {

      throw new EJBException(ex.getMessage());

    }


    try {

      LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);

      return adPreference.getPrfInvQuantityPrecisionUnit();

    } catch (Exception ex) {

      Debug.printStackTrace(ex);
      throw new EJBException(ex.getMessage());

    }

  }

  /**
   * @ejb:interface-method view-type="remote"
   * @jboss:method-attributes read-only="true"
   **/
  public byte getAdPrfArDisableSalesPrice(
      Integer AD_CMPNY
  ) {

    Debug.print("ArMiscReceiptEntryControllerBean getAdPrfArDisableSalesPrice");

    LocalAdPreferenceHome adPreferenceHome = null;

    // Initialize EJB Home

    try {

      adPreferenceHome = (LocalAdPreferenceHome) EJBHomeFactory
          .lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);

    } catch (NamingException ex) {

      throw new EJBException(ex.getMessage());

    }


    try {

      LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);

      return adPreference.getPrfArDisableSalesPrice();

    } catch (Exception ex) {

      Debug.printStackTrace(ex);
      throw new EJBException(ex.getMessage());

    }

  }

  /**
   * @ejb:interface-method view-type="remote"
   * @jboss:method-attributes read-only="true"
   **/
  public byte getAdPrfArUseCustomerPulldown(
      Integer AD_CMPNY
  ) {

    Debug.print("ArMiscReceiptEntryControllerBean getAdPrfArUseCustomerPulldown");

    LocalAdPreferenceHome adPreferenceHome = null;

    // Initialize EJB Home

    try {

      adPreferenceHome = (LocalAdPreferenceHome) EJBHomeFactory
          .lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);

    } catch (NamingException ex) {

      throw new EJBException(ex.getMessage());

    }


    try {

      LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);

      return adPreference.getPrfArUseCustomerPulldown();

    } catch (Exception ex) {

      Debug.printStackTrace(ex);
      throw new EJBException(ex.getMessage());

    }

  }

  /**
   * @ejb:interface-method view-type="remote"
   */
  public boolean getInvAutoBuildEnabledByIiName(
      String II_NM, Integer AD_CMPNY
  ) throws GlobalNoRecordFoundException {

    Debug.print("ArMiscReceiptEntryControllerBean getInvAutoBuildEnabledByIiCode");

    LocalInvItemHome invItemHome = null;

    try {
      invItemHome = (LocalInvItemHome) EJBHomeFactory.lookUpLocalHome(LocalInvItemHome.JNDI_NAME,
          LocalInvItemHome.class);
    } catch (NamingException ex) {

      throw new EJBException(ex.getMessage());

    }

    try {

      try {

        LocalInvItem invItem = invItemHome.findByIiName(II_NM, AD_CMPNY);

        if (invItem.getIiEnableAutoBuild() == EJBCommon.TRUE)
          return true;

        return false;

      } catch (FinderException ex) {

        throw new GlobalNoRecordFoundException();

      }

    } catch (GlobalNoRecordFoundException ex) {

      throw ex;

    }
  }

  /**
   * @ejb:interface-method view-type="remote"
   */
  public String getInvItemClassByIiName(
      String II_NM, Integer AD_CMPNY
  ) throws GlobalNoRecordFoundException {

    Debug.print("ArMiscReceiptEntryControllerBean getInvItemClassByIiCode");

    LocalInvItemHome invItemHome = null;

    try {
      invItemHome = (LocalInvItemHome) EJBHomeFactory.lookUpLocalHome(LocalInvItemHome.JNDI_NAME,
          LocalInvItemHome.class);
    } catch (NamingException ex) {

      throw new EJBException(ex.getMessage());

    }

    try {

      try {
        System.out.println("II_NM=" + II_NM + "=II_NM");
        LocalInvItem invItem = invItemHome.findByIiName(II_NM, AD_CMPNY);

        return invItem.getIiClass();

      } catch (FinderException ex) {

        throw new GlobalNoRecordFoundException();

      }

    } catch (GlobalNoRecordFoundException ex) {

      throw ex;

    }

  }


  /**
   * @ejb:interface-method view-type="remote"
   * @jboss:method-attributes read-only="true"
   **/
  public ArrayList getArSlpAll(
      Integer AD_BRNCH, Integer AD_CMPNY
  ) {

    Debug.print("ArInvoiceEntryControllerBean getArSlpAll");

    LocalArSalespersonHome arSalespersonHome = null;

    ArrayList list = new ArrayList();

    // Initialize EJB Home

    try {

      arSalespersonHome = (LocalArSalespersonHome) EJBHomeFactory
          .lookUpLocalHome(LocalArSalespersonHome.JNDI_NAME, LocalArSalespersonHome.class);

    } catch (NamingException ex) {

      throw new EJBException(ex.getMessage());

    }

    try {

      Collection arSalespersons = arSalespersonHome.findSlpByBrCode(AD_BRNCH, AD_CMPNY);

      Iterator i = arSalespersons.iterator();

      while (i.hasNext()) {

        LocalArSalesperson arSalesperson = (LocalArSalesperson) i.next();

        ArSalespersonDetails details = new ArSalespersonDetails();
        details.setSlpSalespersonCode(arSalesperson.getSlpSalespersonCode());
        details.setSlpName(arSalesperson.getSlpName());

        list.add(details);

      }

      return list;

    } catch (Exception ex) {

      Debug.printStackTrace(ex);
      throw new EJBException(ex.getMessage());

    }

  }

  /**
   * @ejb:interface-method view-type="remote"
   * @jboss:method-attributes read-only="true"
   **/
  public double getInvIiSalesPriceByIiNameAndUomName(
      String II_NM, String UOM_NM, Integer AD_CMPNY
  ) {

    Debug.print("ArMiscReceiptEntryControllerBean getInvIiSalesPriceByIiNameAndUomName");

    LocalInvItemHome invItemHome = null;
    LocalInvUnitOfMeasureHome invUnitOfMeasureHome = null;

    // Initialize EJB Home

    try {

      invItemHome = (LocalInvItemHome) EJBHomeFactory.lookUpLocalHome(LocalInvItemHome.JNDI_NAME,
          LocalInvItemHome.class);
      invUnitOfMeasureHome = (LocalInvUnitOfMeasureHome) EJBHomeFactory
          .lookUpLocalHome(LocalInvUnitOfMeasureHome.JNDI_NAME, LocalInvUnitOfMeasureHome.class);

    } catch (NamingException ex) {

      throw new EJBException(ex.getMessage());

    }

    try {

      LocalInvItem invItem = invItemHome.findByIiName(II_NM, AD_CMPNY);
      LocalInvUnitOfMeasure invUnitOfMeasure = invUnitOfMeasureHome.findByUomName(UOM_NM, AD_CMPNY);

      return EJBCommon.roundIt(
          invItem.getIiSalesPrice() * invUnitOfMeasure.getUomConversionFactor()
              / invItem.getInvUnitOfMeasure().getUomConversionFactor(),
          this.getGlFcPrecisionUnit(AD_CMPNY));

    } catch (Exception ex) {

      Debug.printStackTrace(ex);
      throw new EJBException(ex.getMessage());

    }

  }

  /**
   * @ejb:interface-method view-type="remote"
   */
  public String getInvIiClassByIiName(
      String II_NM, Integer AD_CMPNY
  ) throws GlobalNoRecordFoundException {

    Debug.print("ArMiscReceiptEntryControllerBean getInvItemClassByIiCode");

    LocalInvItemHome invItemHome = null;

    try {
      invItemHome = (LocalInvItemHome) EJBHomeFactory.lookUpLocalHome(LocalInvItemHome.JNDI_NAME,
          LocalInvItemHome.class);
    } catch (NamingException ex) {

      throw new EJBException(ex.getMessage());

    }

    try {

      try {

        LocalInvItem invItem = invItemHome.findByIiName(II_NM, AD_CMPNY);

        return invItem.getIiClass();

      } catch (FinderException ex) {

        throw new GlobalNoRecordFoundException();

      }

    } catch (GlobalNoRecordFoundException ex) {

      throw ex;

    }

  }

  /**
   * @ejb:interface-method view-type="remote"
   * @jboss:method-attributes read-only="true"
   **/
  public ArrayList getInvLitByCstLitName(
      String CST_LIT_NAME, Integer AD_CMPNY
  ) throws GlobalNoRecordFoundException {

    Debug.print("ArMiscReceiptEntryControllerBean getInvLitByCstLitName");

    LocalInvLineItemTemplateHome invLineItemTemplateHome = null;

    // Initialize EJB Home

    try {

      invLineItemTemplateHome = (LocalInvLineItemTemplateHome) EJBHomeFactory.lookUpLocalHome(
          LocalInvLineItemTemplateHome.JNDI_NAME, LocalInvLineItemTemplateHome.class);

    } catch (NamingException ex) {

      throw new EJBException(ex.getMessage());

    }

    try {

      LocalInvLineItemTemplate invLineItemTemplate = null;


      try {

        invLineItemTemplate = invLineItemTemplateHome.findByLitName(CST_LIT_NAME, AD_CMPNY);

      } catch (FinderException ex) {

        throw new GlobalNoRecordFoundException();

      }

      ArrayList list = new ArrayList();

      // get line items if any

      Collection invLineItems = invLineItemTemplate.getInvLineItems();

      if (!invLineItems.isEmpty()) {

        Iterator i = invLineItems.iterator();

        while (i.hasNext()) {

          LocalInvLineItem invLineItem = (LocalInvLineItem) i.next();

          InvModLineItemDetails liDetails = new InvModLineItemDetails();

          liDetails.setLiCode(invLineItem.getLiCode());
          liDetails.setLiLine(invLineItem.getLiLine());
          liDetails.setLiIiName(invLineItem.getInvItemLocation().getInvItem().getIiName());
          liDetails.setLiLocName(invLineItem.getInvItemLocation().getInvLocation().getLocName());
          liDetails.setLiUomName(invLineItem.getInvUnitOfMeasure().getUomName());
          liDetails
              .setLiIiDescription(invLineItem.getInvItemLocation().getInvItem().getIiDescription());

          list.add(liDetails);

        }

      }

      return list;

    } catch (GlobalNoRecordFoundException ex) {

      throw ex;

    } catch (Exception ex) {

      Debug.printStackTrace(ex);
      throw new EJBException(ex.getMessage());

    }

  }

  /**
   * @ejb:interface-method view-type="remote"
   * @jboss:method-attributes read-only="true"
   **/
  public double getFrRateByFrNameAndFrDate(
      String FC_NM, Date CONVERSION_DATE, Integer AD_CMPNY
  ) throws GlobalConversionDateNotExistException {

    Debug.print("ArMiscReceiptEntryControllerBean getFrRateByFrNameAndFrDate");

    LocalGlFunctionalCurrencyRateHome glFunctionalCurrencyRateHome = null;
    LocalGlFunctionalCurrencyHome glFunctionalCurrencyHome = null;
    LocalAdCompanyHome adCompanyHome = null;

    // Initialize EJB Home

    try {

      glFunctionalCurrencyRateHome =
          (LocalGlFunctionalCurrencyRateHome) EJBHomeFactory.lookUpLocalHome(
              LocalGlFunctionalCurrencyRateHome.JNDI_NAME, LocalGlFunctionalCurrencyRateHome.class);
      glFunctionalCurrencyHome = (LocalGlFunctionalCurrencyHome) EJBHomeFactory.lookUpLocalHome(
          LocalGlFunctionalCurrencyHome.JNDI_NAME, LocalGlFunctionalCurrencyHome.class);
      adCompanyHome = (LocalAdCompanyHome) EJBHomeFactory
          .lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);

    } catch (NamingException ex) {

      throw new EJBException(ex.getMessage());

    }

    try {

      LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
      LocalGlFunctionalCurrency glFunctionalCurrency =
          glFunctionalCurrencyHome.findByFcName(FC_NM, AD_CMPNY);

      double CONVERSION_RATE = 1;

      // Get functional currency rate

      if (!FC_NM.equals("USD")) {

        LocalGlFunctionalCurrencyRate glFunctionalCurrencyRate = glFunctionalCurrencyRateHome
            .findByFcCodeAndDate(glFunctionalCurrency.getFcCode(), CONVERSION_DATE, AD_CMPNY);

        CONVERSION_RATE = glFunctionalCurrencyRate.getFrXToUsd();

      }

      // Get set of book functional currency rate if necessary

      if (!adCompany.getGlFunctionalCurrency().getFcName().equals("USD")) {

        LocalGlFunctionalCurrencyRate glCompanyFunctionalCurrencyRate =
            glFunctionalCurrencyRateHome.findByFcCodeAndDate(
                adCompany.getGlFunctionalCurrency().getFcCode(), CONVERSION_DATE, AD_CMPNY);

        CONVERSION_RATE = CONVERSION_RATE / glCompanyFunctionalCurrencyRate.getFrXToUsd();

      }

      return CONVERSION_RATE;

    } catch (FinderException ex) {

      getSessionContext().setRollbackOnly();
      throw new GlobalConversionDateNotExistException();

    } catch (Exception ex) {

      Debug.printStackTrace(ex);
      throw new EJBException(ex.getMessage());

    }

  }

  // private methods

  private LocalArInvoiceLine addArIlEntry(
      ArModInvoiceLineDetails mdetails, LocalArReceipt arReceipt, Integer AD_CMPNY
  ) {

    Debug.print("ArMiscReceiptEntryControllerBean addArIlEntry");

    LocalArInvoiceLineHome arInvoiceLineHome = null;
    LocalArStandardMemoLineHome arStandardMemoLineHome = null;


    // Initialize EJB Home

    try {

      arInvoiceLineHome = (LocalArInvoiceLineHome) EJBHomeFactory
          .lookUpLocalHome(LocalArInvoiceLineHome.JNDI_NAME, LocalArInvoiceLineHome.class);
      arStandardMemoLineHome = (LocalArStandardMemoLineHome) EJBHomeFactory.lookUpLocalHome(
          LocalArStandardMemoLineHome.JNDI_NAME, LocalArStandardMemoLineHome.class);

    } catch (NamingException ex) {

      throw new EJBException(ex.getMessage());

    }

    try {

      short precisionUnit = this.getGlFcPrecisionUnit(AD_CMPNY);

      double IL_AMNT = 0d;
      double IL_TAX_AMNT = 0d;

      if (mdetails.getIlTax() == EJBCommon.TRUE) {

        // calculate net amount

        LocalArTaxCode arTaxCode = arReceipt.getArTaxCode();

        if (arTaxCode.getTcType().equals("INCLUSIVE")) {

          IL_AMNT = EJBCommon.roundIt(mdetails.getIlAmount() / (1 + (arTaxCode.getTcRate() / 100)),
              precisionUnit);

        } else {

          // tax exclusive, none, zero rated or exempt

          IL_AMNT = mdetails.getIlAmount();

        }

        // calculate tax

        if (!arTaxCode.getTcType().equals("NONE") && !arTaxCode.getTcType().equals("EXEMPT")) {


          if (arTaxCode.getTcType().equals("INCLUSIVE")) {

            IL_TAX_AMNT = EJBCommon.roundIt(mdetails.getIlAmount() - IL_AMNT, precisionUnit);

          } else if (arTaxCode.getTcType().equals("EXCLUSIVE")) {

            IL_TAX_AMNT = EJBCommon.roundIt(mdetails.getIlAmount() * arTaxCode.getTcRate() / 100,
                precisionUnit);

          } else {

            // tax none zero-rated or exempt

          }

        }

      } else {

        IL_AMNT = mdetails.getIlAmount();

      }

      LocalArInvoiceLine arInvoiceLine =
          arInvoiceLineHome.create(mdetails.getIlDescription(), mdetails.getIlQuantity(),
              mdetails.getIlUnitPrice(), IL_AMNT, IL_TAX_AMNT, mdetails.getIlTax(), AD_CMPNY);

      // arReceipt.addArInvoiceLine(arInvoiceLine);
      arInvoiceLine.setArReceipt(arReceipt);

      LocalArStandardMemoLine arStandardMemoLine =
          arStandardMemoLineHome.findBySmlName(mdetails.getIlSmlName(), AD_CMPNY);
      // arStandardMemoLine.addArInvoiceLine(arInvoiceLine);
      arInvoiceLine.setArStandardMemoLine(arStandardMemoLine);

      return arInvoiceLine;

    } catch (Exception ex) {

      Debug.printStackTrace(ex);
      getSessionContext().setRollbackOnly();
      throw new EJBException(ex.getMessage());

    }

  }

  private void addArDrEntry(
      short DR_LN, String DR_CLSS, byte DR_DBT, double DR_AMNT, Integer COA_CODE, byte DR_RVRSD,
      LocalArReceipt arReceipt, Integer AD_BRNCH, Integer AD_CMPNY
  ) throws GlobalBranchAccountNumberInvalidException {

    Debug.print("ArMiscReceiptEntryControllerBean addArDrEntry");

    LocalArDistributionRecordHome arDistributionRecordHome = null;
    LocalGlChartOfAccountHome glChartOfAccountHome = null;
    LocalAdCompanyHome adCompanyHome = null;


    // Initialize EJB Home

    try {

      arDistributionRecordHome = (LocalArDistributionRecordHome) EJBHomeFactory.lookUpLocalHome(
          LocalArDistributionRecordHome.JNDI_NAME, LocalArDistributionRecordHome.class);
      glChartOfAccountHome = (LocalGlChartOfAccountHome) EJBHomeFactory
          .lookUpLocalHome(LocalGlChartOfAccountHome.JNDI_NAME, LocalGlChartOfAccountHome.class);
      adCompanyHome = (LocalAdCompanyHome) EJBHomeFactory
          .lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);


    } catch (NamingException ex) {

      throw new EJBException(ex.getMessage());

    }
    System.out.println("COA_CODE1: " + COA_CODE);
    try {

      // get company

      System.out.println("COA_CODE:2 " + COA_CODE);
      LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);

      LocalGlChartOfAccount glChartOfAccount =
          glChartOfAccountHome.findByCoaCodeAndBranchCode(COA_CODE, AD_BRNCH, AD_CMPNY);

      // create distribution record

      LocalArDistributionRecord arDistributionRecord =
          arDistributionRecordHome.create(DR_LN, DR_CLSS, DR_DBT,
              EJBCommon.roundIt(DR_AMNT, adCompany.getGlFunctionalCurrency().getFcPrecision()),
              EJBCommon.FALSE, DR_RVRSD, AD_CMPNY);

      // arReceipt.addArDistributionRecord(arDistributionRecord);
      arDistributionRecord.setArReceipt(arReceipt);
      // glChartOfAccount.addArDistributionRecord(arDistributionRecord);

      System.out.print(glChartOfAccount.toString());

      arDistributionRecord.setGlChartOfAccount(glChartOfAccount);

    } catch (FinderException ex) {

      throw new GlobalBranchAccountNumberInvalidException(ex.getMessage());

    } catch (Exception ex) {

      Debug.printStackTrace(ex);
      getSessionContext().setRollbackOnly();
      throw new EJBException(ex.getMessage());

    }

  }


  private Integer getArGlCoaRevenueAccount(
      LocalArInvoiceLine arInvoiceLine, Integer AD_BRNCH, Integer AD_CMPNY
  ) {

    Debug.print("ArInvoiceLineEntryControllerBean getArGlCoaRevenueAccount");


    LocalArAutoAccountingSegmentHome arAutoAccountingSegmentHome = null;
    LocalGlChartOfAccountHome glChartOfAccountHome = null;
    LocalAdCompanyHome adCompanyHome = null;
    LocalAdBranchStandardMemoLineHome adBranchStandardMemoLineHome = null;

    // Initialize EJB Home

    try {

      arAutoAccountingSegmentHome =
          (LocalArAutoAccountingSegmentHome) EJBHomeFactory.lookUpLocalHome(
              LocalArAutoAccountingSegmentHome.JNDI_NAME, LocalArAutoAccountingSegmentHome.class);
      glChartOfAccountHome = (LocalGlChartOfAccountHome) EJBHomeFactory
          .lookUpLocalHome(LocalGlChartOfAccountHome.JNDI_NAME, LocalGlChartOfAccountHome.class);
      adCompanyHome = (LocalAdCompanyHome) EJBHomeFactory
          .lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
      adBranchStandardMemoLineHome =
          (LocalAdBranchStandardMemoLineHome) EJBHomeFactory.lookUpLocalHome(
              LocalAdBranchStandardMemoLineHome.JNDI_NAME, LocalAdBranchStandardMemoLineHome.class);

    } catch (NamingException ex) {

      throw new EJBException(ex.getMessage());

    }


    // generate revenue account

    try {

      String GL_COA_ACCNT = "";

      LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);

      LocalGenField genField = adCompany.getGenField();

      String FL_SGMNT_SPRTR = String.valueOf(genField.getFlSegmentSeparator());

      LocalAdBranchStandardMemoLine adBranchStandardMemoLine = null;

      try {

        adBranchStandardMemoLine = adBranchStandardMemoLineHome.findBSMLBySMLCodeAndBrCode(
            arInvoiceLine.getArStandardMemoLine().getSmlCode(), AD_BRNCH, AD_CMPNY);

      } catch (FinderException ex) {

      }


      Collection arAutoAccountingSegments =
          arAutoAccountingSegmentHome.findByAaAccountType("REVENUE", AD_CMPNY);

      Iterator i = arAutoAccountingSegments.iterator();

      while (i.hasNext()) {

        LocalArAutoAccountingSegment arAutoAccountingSegment =
            (LocalArAutoAccountingSegment) i.next();

        LocalGlChartOfAccount glChartOfAccount = null;

        if (arAutoAccountingSegment.getAasClassType().equals("AR CUSTOMER")) {

          glChartOfAccount = glChartOfAccountHome.findByPrimaryKey(
              arInvoiceLine.getArReceipt().getArCustomer().getCstGlCoaRevenueAccount());

          StringTokenizer st =
              new StringTokenizer(glChartOfAccount.getCoaAccountNumber(), FL_SGMNT_SPRTR);

          int ctr = 0;
          while (st.hasMoreTokens()) {

            ++ctr;

            if (ctr == arAutoAccountingSegment.getAasSegmentNumber()) {

              GL_COA_ACCNT = GL_COA_ACCNT + FL_SGMNT_SPRTR + st.nextToken();

              break;

            } else {

              st.nextToken();

            }
          }


        } else if (arAutoAccountingSegment.getAasClassType().equals("AR STANDARD MEMO LINE")) {

          if (adBranchStandardMemoLine != null) {

            try {

              glChartOfAccount = glChartOfAccountHome
                  .findByPrimaryKey(adBranchStandardMemoLine.getBsmlGlAccount());

            } catch (FinderException ex) {

            }

          } else {

            glChartOfAccount = arInvoiceLine.getArStandardMemoLine().getGlChartOfAccount();

          }

          StringTokenizer st =
              new StringTokenizer(glChartOfAccount.getCoaAccountNumber(), FL_SGMNT_SPRTR);

          int ctr = 0;
          while (st.hasMoreTokens()) {

            ++ctr;

            if (ctr == arAutoAccountingSegment.getAasSegmentNumber()) {

              GL_COA_ACCNT = GL_COA_ACCNT + FL_SGMNT_SPRTR + st.nextToken();

              break;

            } else {

              st.nextToken();

            }
          }

        }
      }

      GL_COA_ACCNT = GL_COA_ACCNT.substring(1, GL_COA_ACCNT.length());

      try {

        LocalGlChartOfAccount glGeneratedChartOfAccount =
            glChartOfAccountHome.findByCoaAccountNumber(GL_COA_ACCNT, AD_CMPNY);

        return glGeneratedChartOfAccount.getCoaCode();

      } catch (FinderException ex) {

        if (adBranchStandardMemoLine != null) {

          LocalGlChartOfAccount glChartOfAccount = null;

          try {

            glChartOfAccount =
                glChartOfAccountHome.findByPrimaryKey(adBranchStandardMemoLine.getBsmlGlAccount());;

          } catch (FinderException e) {

          }

          return glChartOfAccount.getCoaCode();

        } else {

          return arInvoiceLine.getArStandardMemoLine().getGlChartOfAccount().getCoaCode();

        }

      }


    } catch (Exception ex) {

      Debug.printStackTrace(ex);
      throw new EJBException(ex.getMessage());

    }

  }


  private Integer getArGlCoaReceivableAccount(
      LocalArInvoiceLine arInvoiceLine, Integer AD_BRNCH, Integer AD_CMPNY
  ) {

    Debug.print("ArInvoiceLineEntryControllerBean getArGlCoaRevenueAccount");


    LocalArAutoAccountingSegmentHome arAutoAccountingSegmentHome = null;
    LocalGlChartOfAccountHome glChartOfAccountHome = null;
    LocalAdCompanyHome adCompanyHome = null;
    LocalAdBranchStandardMemoLineHome adBranchStandardMemoLineHome = null;

    // Initialize EJB Home

    try {

      arAutoAccountingSegmentHome =
          (LocalArAutoAccountingSegmentHome) EJBHomeFactory.lookUpLocalHome(
              LocalArAutoAccountingSegmentHome.JNDI_NAME, LocalArAutoAccountingSegmentHome.class);
      glChartOfAccountHome = (LocalGlChartOfAccountHome) EJBHomeFactory
          .lookUpLocalHome(LocalGlChartOfAccountHome.JNDI_NAME, LocalGlChartOfAccountHome.class);
      adCompanyHome = (LocalAdCompanyHome) EJBHomeFactory
          .lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
      adBranchStandardMemoLineHome =
          (LocalAdBranchStandardMemoLineHome) EJBHomeFactory.lookUpLocalHome(
              LocalAdBranchStandardMemoLineHome.JNDI_NAME, LocalAdBranchStandardMemoLineHome.class);

    } catch (NamingException ex) {

      throw new EJBException(ex.getMessage());

    }


    // generate revenue account

    try {

      String GL_COA_ACCNT = "";

      LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);

      LocalGenField genField = adCompany.getGenField();

      String FL_SGMNT_SPRTR = String.valueOf(genField.getFlSegmentSeparator());

      LocalAdBranchStandardMemoLine adBranchStandardMemoLine = null;

      try {

        adBranchStandardMemoLine = adBranchStandardMemoLineHome.findBSMLBySMLCodeAndBrCode(
            arInvoiceLine.getArStandardMemoLine().getSmlCode(), AD_BRNCH, AD_CMPNY);

      } catch (FinderException ex) {

      }


      Collection arAutoAccountingSegments =
          arAutoAccountingSegmentHome.findByAaAccountType("REVENUE", AD_CMPNY);

      Iterator i = arAutoAccountingSegments.iterator();

      while (i.hasNext()) {

        LocalArAutoAccountingSegment arAutoAccountingSegment =
            (LocalArAutoAccountingSegment) i.next();

        LocalGlChartOfAccount glChartOfAccount = null;

        if (arAutoAccountingSegment.getAasClassType().equals("AR CUSTOMER")) {

          glChartOfAccount = glChartOfAccountHome.findByPrimaryKey(
              arInvoiceLine.getArReceipt().getArCustomer().getCstGlCoaRevenueAccount());

          StringTokenizer st =
              new StringTokenizer(glChartOfAccount.getCoaAccountNumber(), FL_SGMNT_SPRTR);

          int ctr = 0;
          while (st.hasMoreTokens()) {

            ++ctr;

            if (ctr == arAutoAccountingSegment.getAasSegmentNumber()) {

              GL_COA_ACCNT = GL_COA_ACCNT + FL_SGMNT_SPRTR + st.nextToken();

              break;

            } else {

              st.nextToken();

            }
          }


        } else if (arAutoAccountingSegment.getAasClassType().equals("AR STANDARD MEMO LINE")) {

          if (adBranchStandardMemoLine != null) {

            try {

              glChartOfAccount = glChartOfAccountHome
                  .findByPrimaryKey(adBranchStandardMemoLine.getBsmlGlCoaReceivableAccount());

            } catch (FinderException ex) {

            }

          } else {

            glChartOfAccount = glChartOfAccountHome.findByPrimaryKey(
                arInvoiceLine.getArStandardMemoLine().getSmlGlCoaReceivableAccount());


            // glChartOfAccount = arInvoiceLine.getArStandardMemoLine().getSmlGlCoaAccount();

          }

          StringTokenizer st =
              new StringTokenizer(glChartOfAccount.getCoaAccountNumber(), FL_SGMNT_SPRTR);

          int ctr = 0;
          while (st.hasMoreTokens()) {

            ++ctr;

            if (ctr == arAutoAccountingSegment.getAasSegmentNumber()) {

              GL_COA_ACCNT = GL_COA_ACCNT + FL_SGMNT_SPRTR + st.nextToken();

              break;

            } else {

              st.nextToken();

            }
          }

        }
      }

      GL_COA_ACCNT = GL_COA_ACCNT.substring(1, GL_COA_ACCNT.length());

      try {

        LocalGlChartOfAccount glGeneratedChartOfAccount =
            glChartOfAccountHome.findByCoaAccountNumber(GL_COA_ACCNT, AD_CMPNY);

        return glGeneratedChartOfAccount.getCoaCode();

      } catch (FinderException ex) {

        if (adBranchStandardMemoLine != null) {

          LocalGlChartOfAccount glChartOfAccount = null;

          try {

            glChartOfAccount = glChartOfAccountHome
                .findByPrimaryKey(adBranchStandardMemoLine.getBsmlGlCoaReceivableAccount());;

          } catch (FinderException e) {

          }

          return glChartOfAccount.getCoaCode();

        } else {

          return arInvoiceLine.getArStandardMemoLine().getSmlGlCoaReceivableAccount();

        }

      }


    } catch (Exception ex) {

      Debug.printStackTrace(ex);
      throw new EJBException(ex.getMessage());

    }

  }


  private double convertForeignToFunctionalCurrency(
      Integer FC_CODE, String FC_NM, Date CONVERSION_DATE, double CONVERSION_RATE, double AMOUNT,
      Integer AD_CMPNY
  ) {

    Debug.print("ArMiscReceiptEntryControllerBean convertForeignToFunctionalCurrency");


    LocalGlFunctionalCurrencyRateHome glFunctionalCurrencyRateHome = null;
    LocalAdCompanyHome adCompanyHome = null;

    LocalAdCompany adCompany = null;

    // Initialize EJB Homes

    try {

      glFunctionalCurrencyRateHome =
          (LocalGlFunctionalCurrencyRateHome) EJBHomeFactory.lookUpLocalHome(
              LocalGlFunctionalCurrencyRateHome.JNDI_NAME, LocalGlFunctionalCurrencyRateHome.class);
      adCompanyHome = (LocalAdCompanyHome) EJBHomeFactory
          .lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);

    } catch (NamingException ex) {

      throw new EJBException(ex.getMessage());

    }

    // get company and extended precision

    try {

      adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);

    } catch (Exception ex) {

      throw new EJBException(ex.getMessage());

    }


    // Convert to functional currency if necessary

    if (CONVERSION_RATE != 1 && CONVERSION_RATE != 0) {

      AMOUNT = AMOUNT / CONVERSION_RATE;

    }
    return EJBCommon.roundIt(AMOUNT, adCompany.getGlFunctionalCurrency().getFcPrecision());

  }


  private void executeArRctPost(
      Integer RCT_CODE, String USR_NM, Integer AD_BRNCH, Integer AD_CMPNY
  ) throws GlobalRecordAlreadyDeletedException, GlobalTransactionAlreadyPostedException,
      GlobalTransactionAlreadyVoidPostedException, GlJREffectiveDateNoPeriodExistException,
      GlJREffectiveDatePeriodClosedException, GlobalJournalNotBalanceException,
      AdPRFCoaGlVarianceAccountNotFoundException, GlobalExpiryDateNotFoundException {

    Debug.print("ArMiscReceiptEntryControllerBean executeArRctPost");


 InvItemEntryControllerHome homeII = null;
    InvItemEntryController ejbII = null;

    LocalArReceiptHome arReceiptHome = null;
    LocalAdCompanyHome adCompanyHome = null;
    LocalAdPreferenceHome adPreferenceHome = null;
    LocalGlSetOfBookHome glSetOfBookHome = null;
    LocalGlAccountingCalendarValueHome glAccountingCalendarValueHome = null;
    LocalGlJournalHome glJournalHome = null;
    LocalGlJournalBatchHome glJournalBatchHome = null;
    LocalGlSuspenseAccountHome glSuspenseAccountHome = null;
    LocalGlJournalLineHome glJournalLineHome = null;
    LocalGlJournalSourceHome glJournalSourceHome = null;
    LocalGlJournalCategoryHome glJournalCategoryHome = null;
    LocalGlFunctionalCurrencyHome glFunctionalCurrencyHome = null;
    LocalArDistributionRecordHome arDistributionRecordHome = null;
    LocalInvCostingHome invCostingHome = null;
    LocalInvItemHome invItemHome = null;
    LocalGlChartOfAccountHome glChartOfAccountHome = null;
    LocalAdBankAccountHome adBankAccountHome = null;
    LocalAdBankAccountBalanceHome adBankAccountBalanceHome = null;
    LocalGlForexLedgerHome glForexLedgerHome = null;
    LocalGlFunctionalCurrencyRateHome glFunctionalCurrencyRateHome = null;

    LocalArReceipt arReceipt = null;

    // Initialize EJB Home

    try {
    
    
 homeII = (InvItemEntryControllerHome) com.util.EJBHomeFactory
          .lookUpHome("ejb/InvItemEntryControllerEJB", InvItemEntryControllerHome.class);   


      arReceiptHome = (LocalArReceiptHome) EJBHomeFactory
          .lookUpLocalHome(LocalArReceiptHome.JNDI_NAME, LocalArReceiptHome.class);
      adCompanyHome = (LocalAdCompanyHome) EJBHomeFactory
          .lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
      adPreferenceHome = (LocalAdPreferenceHome) EJBHomeFactory
          .lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);
      glSetOfBookHome = (LocalGlSetOfBookHome) EJBHomeFactory
          .lookUpLocalHome(LocalGlSetOfBookHome.JNDI_NAME, LocalGlSetOfBookHome.class);
      glAccountingCalendarValueHome = (LocalGlAccountingCalendarValueHome) EJBHomeFactory
          .lookUpLocalHome(LocalGlAccountingCalendarValueHome.JNDI_NAME,
              LocalGlAccountingCalendarValueHome.class);
      glJournalHome = (LocalGlJournalHome) EJBHomeFactory
          .lookUpLocalHome(LocalGlJournalHome.JNDI_NAME, LocalGlJournalHome.class);
      glJournalBatchHome = (LocalGlJournalBatchHome) EJBHomeFactory
          .lookUpLocalHome(LocalGlJournalBatchHome.JNDI_NAME, LocalGlJournalBatchHome.class);
      glSuspenseAccountHome = (LocalGlSuspenseAccountHome) EJBHomeFactory
          .lookUpLocalHome(LocalGlSuspenseAccountHome.JNDI_NAME, LocalGlSuspenseAccountHome.class);
      glJournalLineHome = (LocalGlJournalLineHome) EJBHomeFactory
          .lookUpLocalHome(LocalGlJournalLineHome.JNDI_NAME, LocalGlJournalLineHome.class);
      glJournalSourceHome = (LocalGlJournalSourceHome) EJBHomeFactory
          .lookUpLocalHome(LocalGlJournalSourceHome.JNDI_NAME, LocalGlJournalSourceHome.class);
      glJournalCategoryHome = (LocalGlJournalCategoryHome) EJBHomeFactory
          .lookUpLocalHome(LocalGlJournalCategoryHome.JNDI_NAME, LocalGlJournalCategoryHome.class);
      glFunctionalCurrencyHome = (LocalGlFunctionalCurrencyHome) EJBHomeFactory.lookUpLocalHome(
          LocalGlFunctionalCurrencyHome.JNDI_NAME, LocalGlFunctionalCurrencyHome.class);
      arDistributionRecordHome = (LocalArDistributionRecordHome) EJBHomeFactory.lookUpLocalHome(
          LocalArDistributionRecordHome.JNDI_NAME, LocalArDistributionRecordHome.class);
      invCostingHome = (LocalInvCostingHome) EJBHomeFactory
          .lookUpLocalHome(LocalInvCostingHome.JNDI_NAME, LocalInvCostingHome.class);
      invItemHome = (LocalInvItemHome) EJBHomeFactory.lookUpLocalHome(LocalInvItemHome.JNDI_NAME,
          LocalInvItemHome.class);
      glChartOfAccountHome = (LocalGlChartOfAccountHome) EJBHomeFactory
          .lookUpLocalHome(LocalGlChartOfAccountHome.JNDI_NAME, LocalGlChartOfAccountHome.class);
      adBankAccountHome = (LocalAdBankAccountHome) EJBHomeFactory
          .lookUpLocalHome(LocalAdBankAccountHome.JNDI_NAME, LocalAdBankAccountHome.class);
      adBankAccountBalanceHome = (LocalAdBankAccountBalanceHome) EJBHomeFactory.lookUpLocalHome(
          LocalAdBankAccountBalanceHome.JNDI_NAME, LocalAdBankAccountBalanceHome.class);
      glForexLedgerHome = (LocalGlForexLedgerHome) EJBHomeFactory
          .lookUpLocalHome(LocalGlForexLedgerHome.JNDI_NAME, LocalGlForexLedgerHome.class);
      glFunctionalCurrencyRateHome =
          (LocalGlFunctionalCurrencyRateHome) EJBHomeFactory.lookUpLocalHome(
              LocalGlFunctionalCurrencyRateHome.JNDI_NAME, LocalGlFunctionalCurrencyRateHome.class);

    } catch (NamingException ex) {

      throw new EJBException(ex.getMessage());

    }
    
       try {
    
      ejbII = homeII.create();
    } catch (RemoteException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    } catch (CreateException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }

    try {

      LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
      LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);

      // validate if receipt is already deleted

      try {

        arReceipt = arReceiptHome.findByPrimaryKey(RCT_CODE);

      } catch (FinderException ex) {

        throw new GlobalRecordAlreadyDeletedException();

      }

      // validate if receipt is already posted

      if (arReceipt.getRctVoid() == EJBCommon.FALSE && arReceipt.getRctPosted() == EJBCommon.TRUE) {

        throw new GlobalTransactionAlreadyPostedException();

        // validate if receipt void is already posted

      } else if (arReceipt.getRctVoid() == EJBCommon.TRUE
          && arReceipt.getRctVoidPosted() == EJBCommon.TRUE) {

        throw new GlobalTransactionAlreadyVoidPostedException();

      }



      if (arReceipt.getRctVoid() == EJBCommon.FALSE && arReceipt.getRctType().equals("MISC")
          && !arReceipt.getArInvoiceLineItems().isEmpty()) {

        // regenerate inventory dr

       //this.regenerateInventoryDr(arReceipt, AD_BRNCH, AD_CMPNY);

      }

      // post receipt

      if (arReceipt.getRctVoid() == EJBCommon.FALSE
          && arReceipt.getRctPosted() == EJBCommon.FALSE) {


        if (arReceipt.getRctType().equals("MISC") && !arReceipt.getArInvoiceLineItems().isEmpty()) {

          Iterator c = arReceipt.getArInvoiceLineItems().iterator();

          while (c.hasNext()) {

            LocalArInvoiceLineItem arInvoiceLineItem = (LocalArInvoiceLineItem) c.next();

            String II_NM = arInvoiceLineItem.getInvItemLocation().getInvItem().getIiName();
            String LOC_NM = arInvoiceLineItem.getInvItemLocation().getInvLocation().getLocName();

            double QTY_SLD =
                this.convertByUomFromAndItemAndQuantity(arInvoiceLineItem.getInvUnitOfMeasure(),
                    arInvoiceLineItem.getInvItemLocation().getInvItem(),
                    arInvoiceLineItem.getIliQuantity(), AD_CMPNY);

            LocalInvCosting invCosting = null;

            try {

              invCosting = invCostingHome
                  .getByMaxCstDateToLongAndMaxCstLineNumberAndLessThanEqualCstDateAndIiNameAndLocName(
                      arReceipt.getRctDate(), II_NM, LOC_NM, AD_BRNCH, AD_CMPNY);

            } catch (FinderException ex) {

            }

            double COST =  arInvoiceLineItem.getInvItemLocation().getInvItem().getIiUnitCost();
            if (invCosting == null) {

              this.postToInv(arInvoiceLineItem, arReceipt.getRctDate(), QTY_SLD, COST * QTY_SLD,
                  -QTY_SLD, -COST * QTY_SLD, 0d, null, AD_BRNCH, AD_CMPNY);
            } else {

              if (invCosting.getInvItemLocation().getInvItem().getIiCostMethod()
                  .equals("Average")) {

                double avgCost =  ejbII.getInvIiUnitCostByIiNameAndUomName(II_NM, arInvoiceLineItem.getInvUnitOfMeasure().getUomName(), arReceipt.getRctDate(), AD_CMPNY);


                this.postToInv(arInvoiceLineItem, arReceipt.getRctDate(), QTY_SLD,
                    avgCost * QTY_SLD, invCosting.getCstRemainingQuantity() - QTY_SLD,
                    invCosting.getCstRemainingValue() - (avgCost * QTY_SLD), 0d, null, AD_BRNCH,
                    AD_CMPNY);

              } else if (invCosting.getInvItemLocation().getInvItem().getIiCostMethod()
                  .equals("FIFO")) {

                double fifoCost = invCosting.getCstRemainingQuantity() == 0 ? COST
                    : this.getInvFifoCost(invCosting.getCstDate(),
                        invCosting.getInvItemLocation().getIlCode(), QTY_SLD,
                        arInvoiceLineItem.getIliUnitPrice(), true, AD_BRNCH, AD_CMPNY);

                this.postToInv(arInvoiceLineItem, arReceipt.getRctDate(), QTY_SLD,
                    fifoCost * QTY_SLD, invCosting.getCstRemainingQuantity() - QTY_SLD,
                    invCosting.getCstRemainingValue() - (fifoCost * QTY_SLD), 0d, null, AD_BRNCH,
                    AD_CMPNY);

              } else if (invCosting.getInvItemLocation().getInvItem().getIiCostMethod()
                  .equals("Standard")) {

                double standardCost = invCosting.getInvItemLocation().getInvItem().getIiUnitCost();

                this.postToInv(arInvoiceLineItem, arReceipt.getRctDate(), QTY_SLD,
                    standardCost * QTY_SLD, invCosting.getCstRemainingQuantity() - QTY_SLD,
                    invCosting.getCstRemainingValue() - (standardCost * QTY_SLD), 0d, null,
                    AD_BRNCH, AD_CMPNY);
              }

            }



          }

        }

        // increase bank balance CASH
        if (arReceipt.getRctAmountCash() > 0) {

          LocalAdBankAccount adBankAccount =
              adBankAccountHome.findByPrimaryKey(arReceipt.getAdBankAccount().getBaCode());

          try {

            // find bankaccount balance before or equal receipt date

            Collection adBankAccountBalances = adBankAccountBalanceHome
                .findByBeforeOrEqualDateAndBaCodeAndType(arReceipt.getRctDate(),
                    arReceipt.getAdBankAccount().getBaCode(), "BOOK", AD_CMPNY);

            if (!adBankAccountBalances.isEmpty()) {

              // get last check

              ArrayList adBankAccountBalanceList = new ArrayList(adBankAccountBalances);

              LocalAdBankAccountBalance adBankAccountBalance =
                  (LocalAdBankAccountBalance) adBankAccountBalanceList
                      .get(adBankAccountBalanceList.size() - 1);

              if (adBankAccountBalance.getBabDate().before(arReceipt.getRctDate())) {

                // create new balance

                LocalAdBankAccountBalance adNewBankAccountBalance =
                    adBankAccountBalanceHome.create(arReceipt.getRctDate(),
                        adBankAccountBalance.getBabBalance() + arReceipt.getRctAmountCash(), "BOOK",
                        AD_CMPNY);

                // adBankAccount.addAdBankAccountBalance(adNewBankAccountBalance);
                adNewBankAccountBalance.setAdBankAccount(adBankAccount);
              } else { // equals to check date

                adBankAccountBalance.setBabBalance(
                    adBankAccountBalance.getBabBalance() + arReceipt.getRctAmountCash());

              }

            } else {

              // create new balance
              System.out.println("arReceipt.getRctAmountCash()=" + arReceipt.getRctAmountCash());
              LocalAdBankAccountBalance adNewBankAccountBalance = adBankAccountBalanceHome
                  .create(arReceipt.getRctDate(), (arReceipt.getRctAmountCash()), "BOOK", AD_CMPNY);

              // adBankAccount.addAdBankAccountBalance(adNewBankAccountBalance);
              adNewBankAccountBalance.setAdBankAccount(adBankAccount);

            }

            // propagate to subsequent balances if necessary

            adBankAccountBalances = adBankAccountBalanceHome.findByAfterDateAndBaCodeAndType(
                arReceipt.getRctDate(), arReceipt.getAdBankAccount().getBaCode(), "BOOK", AD_CMPNY);

            Iterator i = adBankAccountBalances.iterator();

            while (i.hasNext()) {

              LocalAdBankAccountBalance adBankAccountBalance = (LocalAdBankAccountBalance) i.next();

              adBankAccountBalance.setBabBalance(
                  adBankAccountBalance.getBabBalance() + arReceipt.getRctAmountCash());

            }

          } catch (Exception ex) {

            ex.printStackTrace();

          }
        }

        // increase bank balance CARD 1
        if (arReceipt.getRctAmountCard1() > 0) {


          LocalAdBankAccount adBankAccount =
              adBankAccountHome.findByPrimaryKey(arReceipt.getAdBankAccountCard1().getBaCode());

          try {

            // find bankaccount balance before or equal receipt date

            Collection adBankAccountBalances = adBankAccountBalanceHome
                .findByBeforeOrEqualDateAndBaCodeAndType(arReceipt.getRctDate(),
                    arReceipt.getAdBankAccountCard1().getBaCode(), "BOOK", AD_CMPNY);

            if (!adBankAccountBalances.isEmpty()) {

              // get last check

              ArrayList adBankAccountBalanceList = new ArrayList(adBankAccountBalances);

              LocalAdBankAccountBalance adBankAccountBalance =
                  (LocalAdBankAccountBalance) adBankAccountBalanceList
                      .get(adBankAccountBalanceList.size() - 1);

              if (adBankAccountBalance.getBabDate().before(arReceipt.getRctDate())) {

                // create new balance

                LocalAdBankAccountBalance adNewBankAccountBalance =
                    adBankAccountBalanceHome.create(arReceipt.getRctDate(),
                        adBankAccountBalance.getBabBalance() + arReceipt.getRctAmountCard1(),
                        "BOOK", AD_CMPNY);

                // adBankAccount.addAdBankAccountBalance(adNewBankAccountBalance);
                adNewBankAccountBalance.setAdBankAccount(adBankAccount);
              } else { // equals to check date

                adBankAccountBalance.setBabBalance(
                    adBankAccountBalance.getBabBalance() + arReceipt.getRctAmountCard1());

              }

            } else {

              // create new balance
              System.out.println("arReceipt.getRctAmountCard1()=" + arReceipt.getRctAmountCard1());
              LocalAdBankAccountBalance adNewBankAccountBalance = adBankAccountBalanceHome.create(
                  arReceipt.getRctDate(), (arReceipt.getRctAmountCard1()), "BOOK", AD_CMPNY);

              // adBankAccount.addAdBankAccountBalance(adNewBankAccountBalance);
              adNewBankAccountBalance.setAdBankAccount(adBankAccount);

            }

            // propagate to subsequent balances if necessary

            adBankAccountBalances =
                adBankAccountBalanceHome.findByAfterDateAndBaCodeAndType(arReceipt.getRctDate(),
                    arReceipt.getAdBankAccountCard1().getBaCode(), "BOOK", AD_CMPNY);

            Iterator i = adBankAccountBalances.iterator();

            while (i.hasNext()) {

              LocalAdBankAccountBalance adBankAccountBalance = (LocalAdBankAccountBalance) i.next();

              adBankAccountBalance.setBabBalance(
                  adBankAccountBalance.getBabBalance() + arReceipt.getRctAmountCard1());

            }

          } catch (Exception ex) {

            ex.printStackTrace();

          }

        }

        // increase bank balance CARD 2
        if (arReceipt.getRctAmountCard2() > 0) {
          LocalAdBankAccount adBankAccount =
              adBankAccountHome.findByPrimaryKey(arReceipt.getAdBankAccountCard2().getBaCode());

          try {

            // find bankaccount balance before or equal receipt date

            Collection adBankAccountBalances = adBankAccountBalanceHome
                .findByBeforeOrEqualDateAndBaCodeAndType(arReceipt.getRctDate(),
                    arReceipt.getAdBankAccountCard2().getBaCode(), "BOOK", AD_CMPNY);

            if (!adBankAccountBalances.isEmpty()) {

              // get last check

              ArrayList adBankAccountBalanceList = new ArrayList(adBankAccountBalances);

              LocalAdBankAccountBalance adBankAccountBalance =
                  (LocalAdBankAccountBalance) adBankAccountBalanceList
                      .get(adBankAccountBalanceList.size() - 1);

              if (adBankAccountBalance.getBabDate().before(arReceipt.getRctDate())) {

                // create new balance

                LocalAdBankAccountBalance adNewBankAccountBalance =
                    adBankAccountBalanceHome.create(arReceipt.getRctDate(),
                        adBankAccountBalance.getBabBalance() + arReceipt.getRctAmountCard2(),
                        "BOOK", AD_CMPNY);

                // adBankAccount.addAdBankAccountBalance(adNewBankAccountBalance);
                adNewBankAccountBalance.setAdBankAccount(adBankAccount);
              } else { // equals to check date

                adBankAccountBalance.setBabBalance(
                    adBankAccountBalance.getBabBalance() + arReceipt.getRctAmountCard2());

              }

            } else {

              // create new balance
              System.out.println("arReceipt.getRctAmountCard2()=" + arReceipt.getRctAmountCard2());
              LocalAdBankAccountBalance adNewBankAccountBalance = adBankAccountBalanceHome.create(
                  arReceipt.getRctDate(), (arReceipt.getRctAmountCard2()), "BOOK", AD_CMPNY);

              // adBankAccount.addAdBankAccountBalance(adNewBankAccountBalance);
              adNewBankAccountBalance.setAdBankAccount(adBankAccount);

            }

            // propagate to subsequent balances if necessary

            adBankAccountBalances =
                adBankAccountBalanceHome.findByAfterDateAndBaCodeAndType(arReceipt.getRctDate(),
                    arReceipt.getAdBankAccountCard2().getBaCode(), "BOOK", AD_CMPNY);

            Iterator i = adBankAccountBalances.iterator();

            while (i.hasNext()) {

              LocalAdBankAccountBalance adBankAccountBalance = (LocalAdBankAccountBalance) i.next();

              adBankAccountBalance.setBabBalance(
                  adBankAccountBalance.getBabBalance() + arReceipt.getRctAmountCard2());

            }

          } catch (Exception ex) {

            ex.printStackTrace();

          }
        }

        // increase bank balance CARD 3
        if (arReceipt.getRctAmountCard3() > 0) {
          LocalAdBankAccount adBankAccount =
              adBankAccountHome.findByPrimaryKey(arReceipt.getAdBankAccountCard3().getBaCode());

          try {

            // find bankaccount balance before or equal receipt date

            Collection adBankAccountBalances = adBankAccountBalanceHome
                .findByBeforeOrEqualDateAndBaCodeAndType(arReceipt.getRctDate(),
                    arReceipt.getAdBankAccountCard3().getBaCode(), "BOOK", AD_CMPNY);

            if (!adBankAccountBalances.isEmpty()) {

              // get last check

              ArrayList adBankAccountBalanceList = new ArrayList(adBankAccountBalances);

              LocalAdBankAccountBalance adBankAccountBalance =
                  (LocalAdBankAccountBalance) adBankAccountBalanceList
                      .get(adBankAccountBalanceList.size() - 1);

              if (adBankAccountBalance.getBabDate().before(arReceipt.getRctDate())) {

                // create new balance

                LocalAdBankAccountBalance adNewBankAccountBalance =
                    adBankAccountBalanceHome.create(arReceipt.getRctDate(),
                        adBankAccountBalance.getBabBalance() + arReceipt.getRctAmountCard3(),
                        "BOOK", AD_CMPNY);

                // adBankAccount.addAdBankAccountBalance(adNewBankAccountBalance);
                adNewBankAccountBalance.setAdBankAccount(adBankAccount);
              } else { // equals to check date

                adBankAccountBalance.setBabBalance(
                    adBankAccountBalance.getBabBalance() + arReceipt.getRctAmountCard3());

              }

            } else {

              // create new balance
              System.out.println("arReceipt.getRctAmountCard3()=" + arReceipt.getRctAmountCard3());
              LocalAdBankAccountBalance adNewBankAccountBalance = adBankAccountBalanceHome.create(
                  arReceipt.getRctDate(), (arReceipt.getRctAmountCard3()), "BOOK", AD_CMPNY);

              // adBankAccount.addAdBankAccountBalance(adNewBankAccountBalance);
              adNewBankAccountBalance.setAdBankAccount(adBankAccount);

            }

            // propagate to subsequent balances if necessary

            adBankAccountBalances =
                adBankAccountBalanceHome.findByAfterDateAndBaCodeAndType(arReceipt.getRctDate(),
                    arReceipt.getAdBankAccountCard3().getBaCode(), "BOOK", AD_CMPNY);

            Iterator i = adBankAccountBalances.iterator();

            while (i.hasNext()) {

              LocalAdBankAccountBalance adBankAccountBalance = (LocalAdBankAccountBalance) i.next();

              adBankAccountBalance.setBabBalance(
                  adBankAccountBalance.getBabBalance() + arReceipt.getRctAmountCard3());

            }

          } catch (Exception ex) {

            ex.printStackTrace();

          }
        }
        // increase bank balance CHEQUE
        if (arReceipt.getRctAmountCheque() > 0) {
          LocalAdBankAccount adBankAccount =
              adBankAccountHome.findByPrimaryKey(arReceipt.getAdBankAccount().getBaCode());

          try {

            // find bankaccount balance before or equal receipt date

            Collection adBankAccountBalances = adBankAccountBalanceHome
                .findByBeforeOrEqualDateAndBaCodeAndType(arReceipt.getRctDate(),
                    arReceipt.getAdBankAccount().getBaCode(), "BOOK", AD_CMPNY);

            if (!adBankAccountBalances.isEmpty()) {

              // get last check

              ArrayList adBankAccountBalanceList = new ArrayList(adBankAccountBalances);

              LocalAdBankAccountBalance adBankAccountBalance =
                  (LocalAdBankAccountBalance) adBankAccountBalanceList
                      .get(adBankAccountBalanceList.size() - 1);

              if (adBankAccountBalance.getBabDate().before(arReceipt.getRctDate())) {

                // create new balance

                LocalAdBankAccountBalance adNewBankAccountBalance =
                    adBankAccountBalanceHome.create(arReceipt.getRctDate(),
                        adBankAccountBalance.getBabBalance() + arReceipt.getRctAmountCheque(),
                        "BOOK", AD_CMPNY);

                // adBankAccount.addAdBankAccountBalance(adNewBankAccountBalance);
                adNewBankAccountBalance.setAdBankAccount(adBankAccount);
              } else { // equals to check date

                adBankAccountBalance.setBabBalance(
                    adBankAccountBalance.getBabBalance() + arReceipt.getRctAmountCheque());

              }

            } else {

              // create new balance
              System.out
                  .println("arReceipt.getRctAmountCheque()=" + arReceipt.getRctAmountCheque());
              LocalAdBankAccountBalance adNewBankAccountBalance = adBankAccountBalanceHome.create(
                  arReceipt.getRctDate(), (arReceipt.getRctAmountCheque()), "BOOK", AD_CMPNY);

              // adBankAccount.addAdBankAccountBalance(adNewBankAccountBalance);
              adNewBankAccountBalance.setAdBankAccount(adBankAccount);

            }

            // propagate to subsequent balances if necessary

            adBankAccountBalances = adBankAccountBalanceHome.findByAfterDateAndBaCodeAndType(
                arReceipt.getRctDate(), arReceipt.getAdBankAccount().getBaCode(), "BOOK", AD_CMPNY);

            Iterator i = adBankAccountBalances.iterator();

            while (i.hasNext()) {

              LocalAdBankAccountBalance adBankAccountBalance = (LocalAdBankAccountBalance) i.next();

              adBankAccountBalance.setBabBalance(
                  adBankAccountBalance.getBabBalance() + arReceipt.getRctAmountCheque());

            }

          } catch (Exception ex) {

            ex.printStackTrace();

          }
        }



        // set receipt post status

        arReceipt.setRctPosted(EJBCommon.TRUE);
        arReceipt.setRctPostedBy(USR_NM);
        arReceipt.setRctDatePosted(EJBCommon.getGcCurrentDateWoTime().getTime());


      } else if (arReceipt.getRctVoid() == EJBCommon.TRUE
          && arReceipt.getRctVoidPosted() == EJBCommon.FALSE) { // void receipt
        // VOIDING MISC RECEIPT
        if (arReceipt.getRctType().equals("MISC") && !arReceipt.getArInvoiceLineItems().isEmpty()) {

          Iterator c = arReceipt.getArInvoiceLineItems().iterator();

          while (c.hasNext()) {

            LocalArInvoiceLineItem arInvoiceLineItem = (LocalArInvoiceLineItem) c.next();

            String II_NM = arInvoiceLineItem.getInvItemLocation().getInvItem().getIiName();
            String LOC_NM = arInvoiceLineItem.getInvItemLocation().getInvLocation().getLocName();

            double QTY_SLD =
                this.convertByUomFromAndItemAndQuantity(arInvoiceLineItem.getInvUnitOfMeasure(),
                    arInvoiceLineItem.getInvItemLocation().getInvItem(),
                    arInvoiceLineItem.getIliQuantity(), AD_CMPNY);

            LocalInvCosting invCosting = null;

            try {

              invCosting = invCostingHome
                  .getByMaxCstDateToLongAndMaxCstLineNumberAndLessThanEqualCstDateAndIiNameAndLocName(
                      arReceipt.getRctDate(), II_NM, LOC_NM, AD_BRNCH, AD_CMPNY);

            } catch (FinderException ex) {

            }

            double COST = arInvoiceLineItem.getInvItemLocation().getInvItem().getIiUnitCost();

            if (invCosting == null) {

              this.postToInv(arInvoiceLineItem, arReceipt.getRctDate(), -QTY_SLD, -COST * QTY_SLD,
                  QTY_SLD, COST * QTY_SLD, 0d, null, AD_BRNCH, AD_CMPNY);

            } else {


              if (invCosting.getInvItemLocation().getInvItem().getIiCostMethod()
                  .equals("Average")) {



                double avgCost = ejbII.getInvIiUnitCostByIiNameAndUomName(II_NM, arInvoiceLineItem.getInvUnitOfMeasure().getUomName(), arReceipt.getRctDate(), AD_CMPNY);
                if (invCosting.getCstRemainingQuantity() <= 0) {
                  avgCost = arInvoiceLineItem.getIliUnitPrice();
                }


                this.postToInv(arInvoiceLineItem, arReceipt.getRctDate(), -QTY_SLD,
                    -avgCost * QTY_SLD, invCosting.getCstRemainingQuantity() + QTY_SLD,
                    invCosting.getCstRemainingValue() + (avgCost * QTY_SLD), 0d, USR_NM, AD_BRNCH,
                    AD_CMPNY);

              } else if (invCosting.getInvItemLocation().getInvItem().getIiCostMethod()
                  .equals("FIFO")) {

                double fifoCost = this.getInvFifoCost(invCosting.getCstDate(),
                    invCosting.getInvItemLocation().getIlCode(), QTY_SLD,
                    arInvoiceLineItem.getIliUnitPrice(), true, AD_BRNCH, AD_CMPNY);

                this.postToInv(arInvoiceLineItem, arReceipt.getRctDate(), -QTY_SLD,
                    -fifoCost * QTY_SLD, invCosting.getCstRemainingQuantity() + QTY_SLD,
                    invCosting.getCstRemainingValue() + (fifoCost * QTY_SLD), 0d, USR_NM, AD_BRNCH,
                    AD_CMPNY);

              } else if (invCosting.getInvItemLocation().getInvItem().getIiCostMethod()
                  .equals("Standard")) {

                double standardCost = invCosting.getInvItemLocation().getInvItem().getIiUnitCost();

                this.postToInv(arInvoiceLineItem, arReceipt.getRctDate(), -QTY_SLD,
                    -standardCost * QTY_SLD, invCosting.getCstRemainingQuantity() + QTY_SLD,
                    invCosting.getCstRemainingValue() + (standardCost * QTY_SLD), 0d, USR_NM,
                    AD_BRNCH, AD_CMPNY);

              }

            }

          }

        }

        // decrease bank balance cash

        if (arReceipt.getRctAmountCash() > 0) {

          LocalAdBankAccount adBankAccount =
              adBankAccountHome.findByPrimaryKey(arReceipt.getAdBankAccount().getBaCode());

          try {

            // find bankaccount balance before or equal receipt date

            Collection adBankAccountBalances = adBankAccountBalanceHome
                .findByBeforeOrEqualDateAndBaCodeAndType(arReceipt.getRctDate(),
                    arReceipt.getAdBankAccount().getBaCode(), "BOOK", AD_CMPNY);

            if (!adBankAccountBalances.isEmpty()) {

              // get last check

              ArrayList adBankAccountBalanceList = new ArrayList(adBankAccountBalances);

              LocalAdBankAccountBalance adBankAccountBalance =
                  (LocalAdBankAccountBalance) adBankAccountBalanceList
                      .get(adBankAccountBalanceList.size() - 1);

              if (adBankAccountBalance.getBabDate().before(arReceipt.getRctDate())) {

                // create new balance

                LocalAdBankAccountBalance adNewBankAccountBalance =
                    adBankAccountBalanceHome.create(arReceipt.getRctDate(),
                        adBankAccountBalance.getBabBalance() - arReceipt.getRctAmountCash(), "BOOK",
                        AD_CMPNY);

                // adBankAccount.addAdBankAccountBalance(adNewBankAccountBalance);
                adNewBankAccountBalance.setAdBankAccount(adBankAccount);
              } else { // equals to check date

                adBankAccountBalance.setBabBalance(
                    adBankAccountBalance.getBabBalance() - arReceipt.getRctAmountCash());

              }

            } else {

              // create new balance
              System.out.println("arReceipt.getRctAmountCash()=" + arReceipt.getRctAmountCash());
              LocalAdBankAccountBalance adNewBankAccountBalance = adBankAccountBalanceHome.create(
                  arReceipt.getRctDate(), (-arReceipt.getRctAmountCash()), "BOOK", AD_CMPNY);

              // adBankAccount.addAdBankAccountBalance(adNewBankAccountBalance);
              adNewBankAccountBalance.setAdBankAccount(adBankAccount);

            }

            // propagate to subsequent balances if necessary

            adBankAccountBalances = adBankAccountBalanceHome.findByAfterDateAndBaCodeAndType(
                arReceipt.getRctDate(), arReceipt.getAdBankAccount().getBaCode(), "BOOK", AD_CMPNY);

            Iterator i = adBankAccountBalances.iterator();

            while (i.hasNext()) {

              LocalAdBankAccountBalance adBankAccountBalance = (LocalAdBankAccountBalance) i.next();

              adBankAccountBalance.setBabBalance(
                  adBankAccountBalance.getBabBalance() - arReceipt.getRctAmountCash());

            }

          } catch (Exception ex) {

            ex.printStackTrace();

          }
        }

        // decrease bank balance CARD 1
        if (arReceipt.getRctAmountCard1() > 0) {


          LocalAdBankAccount adBankAccount =
              adBankAccountHome.findByPrimaryKey(arReceipt.getAdBankAccountCard1().getBaCode());

          try {

            // find bankaccount balance before or equal receipt date

            Collection adBankAccountBalances = adBankAccountBalanceHome
                .findByBeforeOrEqualDateAndBaCodeAndType(arReceipt.getRctDate(),
                    arReceipt.getAdBankAccountCard1().getBaCode(), "BOOK", AD_CMPNY);

            if (!adBankAccountBalances.isEmpty()) {

              // get last check

              ArrayList adBankAccountBalanceList = new ArrayList(adBankAccountBalances);

              LocalAdBankAccountBalance adBankAccountBalance =
                  (LocalAdBankAccountBalance) adBankAccountBalanceList
                      .get(adBankAccountBalanceList.size() - 1);

              if (adBankAccountBalance.getBabDate().before(arReceipt.getRctDate())) {

                // create new balance

                LocalAdBankAccountBalance adNewBankAccountBalance =
                    adBankAccountBalanceHome.create(arReceipt.getRctDate(),
                        adBankAccountBalance.getBabBalance() - arReceipt.getRctAmountCard1(),
                        "BOOK", AD_CMPNY);

                // adBankAccount.addAdBankAccountBalance(adNewBankAccountBalance);
                adNewBankAccountBalance.setAdBankAccount(adBankAccount);
              } else { // equals to check date

                adBankAccountBalance.setBabBalance(
                    adBankAccountBalance.getBabBalance() - arReceipt.getRctAmountCard1());

              }

            } else {

              // create new balance
              System.out.println("arReceipt.getRctAmountCard1()=" + arReceipt.getRctAmountCard1());
              LocalAdBankAccountBalance adNewBankAccountBalance = adBankAccountBalanceHome.create(
                  arReceipt.getRctDate(), (-arReceipt.getRctAmountCard1()), "BOOK", AD_CMPNY);

              // adBankAccount.addAdBankAccountBalance(adNewBankAccountBalance);
              adNewBankAccountBalance.setAdBankAccount(adBankAccount);

            }

            // propagate to subsequent balances if necessary

            adBankAccountBalances =
                adBankAccountBalanceHome.findByAfterDateAndBaCodeAndType(arReceipt.getRctDate(),
                    arReceipt.getAdBankAccountCard1().getBaCode(), "BOOK", AD_CMPNY);

            Iterator i = adBankAccountBalances.iterator();

            while (i.hasNext()) {

              LocalAdBankAccountBalance adBankAccountBalance = (LocalAdBankAccountBalance) i.next();

              adBankAccountBalance.setBabBalance(
                  adBankAccountBalance.getBabBalance() - arReceipt.getRctAmountCard1());

            }

          } catch (Exception ex) {

            ex.printStackTrace();

          }

        }

        // decrease bank balance CARD 2
        if (arReceipt.getRctAmountCard2() > 0) {
          LocalAdBankAccount adBankAccount =
              adBankAccountHome.findByPrimaryKey(arReceipt.getAdBankAccountCard2().getBaCode());

          try {

            // find bankaccount balance before or equal receipt date

            Collection adBankAccountBalances = adBankAccountBalanceHome
                .findByBeforeOrEqualDateAndBaCodeAndType(arReceipt.getRctDate(),
                    arReceipt.getAdBankAccountCard2().getBaCode(), "BOOK", AD_CMPNY);

            if (!adBankAccountBalances.isEmpty()) {

              // get last check

              ArrayList adBankAccountBalanceList = new ArrayList(adBankAccountBalances);

              LocalAdBankAccountBalance adBankAccountBalance =
                  (LocalAdBankAccountBalance) adBankAccountBalanceList
                      .get(adBankAccountBalanceList.size() - 1);

              if (adBankAccountBalance.getBabDate().before(arReceipt.getRctDate())) {

                // create new balance

                LocalAdBankAccountBalance adNewBankAccountBalance =
                    adBankAccountBalanceHome.create(arReceipt.getRctDate(),
                        adBankAccountBalance.getBabBalance() - arReceipt.getRctAmountCard2(),
                        "BOOK", AD_CMPNY);

                // adBankAccount.addAdBankAccountBalance(adNewBankAccountBalance);
                adNewBankAccountBalance.setAdBankAccount(adBankAccount);
              } else { // equals to check date

                adBankAccountBalance.setBabBalance(
                    adBankAccountBalance.getBabBalance() - arReceipt.getRctAmountCard2());

              }

            } else {

              // create new balance
              System.out.println("arReceipt.getRctAmountCard2()=" + arReceipt.getRctAmountCard2());
              LocalAdBankAccountBalance adNewBankAccountBalance = adBankAccountBalanceHome.create(
                  arReceipt.getRctDate(), (-arReceipt.getRctAmountCard2()), "BOOK", AD_CMPNY);

              // adBankAccount.addAdBankAccountBalance(adNewBankAccountBalance);
              adNewBankAccountBalance.setAdBankAccount(adBankAccount);

            }

            // propagate to subsequent balances if necessary

            adBankAccountBalances =
                adBankAccountBalanceHome.findByAfterDateAndBaCodeAndType(arReceipt.getRctDate(),
                    arReceipt.getAdBankAccountCard2().getBaCode(), "BOOK", AD_CMPNY);

            Iterator i = adBankAccountBalances.iterator();

            while (i.hasNext()) {

              LocalAdBankAccountBalance adBankAccountBalance = (LocalAdBankAccountBalance) i.next();

              adBankAccountBalance.setBabBalance(
                  adBankAccountBalance.getBabBalance() - arReceipt.getRctAmountCard2());

            }

          } catch (Exception ex) {

            ex.printStackTrace();

          }
        }

        // decrease bank balance CARD 3
        if (arReceipt.getRctAmountCard3() > 0) {
          LocalAdBankAccount adBankAccount =
              adBankAccountHome.findByPrimaryKey(arReceipt.getAdBankAccountCard3().getBaCode());

          try {

            // find bankaccount balance before or equal receipt date

            Collection adBankAccountBalances = adBankAccountBalanceHome
                .findByBeforeOrEqualDateAndBaCodeAndType(arReceipt.getRctDate(),
                    arReceipt.getAdBankAccountCard3().getBaCode(), "BOOK", AD_CMPNY);

            if (!adBankAccountBalances.isEmpty()) {

              // get last check

              ArrayList adBankAccountBalanceList = new ArrayList(adBankAccountBalances);

              LocalAdBankAccountBalance adBankAccountBalance =
                  (LocalAdBankAccountBalance) adBankAccountBalanceList
                      .get(adBankAccountBalanceList.size() - 1);

              if (adBankAccountBalance.getBabDate().before(arReceipt.getRctDate())) {

                // create new balance

                LocalAdBankAccountBalance adNewBankAccountBalance =
                    adBankAccountBalanceHome.create(arReceipt.getRctDate(),
                        adBankAccountBalance.getBabBalance() - arReceipt.getRctAmountCard3(),
                        "BOOK", AD_CMPNY);

                // adBankAccount.addAdBankAccountBalance(adNewBankAccountBalance);
                adNewBankAccountBalance.setAdBankAccount(adBankAccount);
              } else { // equals to check date

                adBankAccountBalance.setBabBalance(
                    adBankAccountBalance.getBabBalance() + arReceipt.getRctAmountCard3());

              }

            } else {

              // create new balance
              System.out.println("arReceipt.getRctAmountCard3()=" + arReceipt.getRctAmountCard3());
              LocalAdBankAccountBalance adNewBankAccountBalance = adBankAccountBalanceHome.create(
                  arReceipt.getRctDate(), (-arReceipt.getRctAmountCard3()), "BOOK", AD_CMPNY);

              // adBankAccount.addAdBankAccountBalance(adNewBankAccountBalance);
              adNewBankAccountBalance.setAdBankAccount(adBankAccount);

            }

            // propagate to subsequent balances if necessary

            adBankAccountBalances =
                adBankAccountBalanceHome.findByAfterDateAndBaCodeAndType(arReceipt.getRctDate(),
                    arReceipt.getAdBankAccountCard3().getBaCode(), "BOOK", AD_CMPNY);

            Iterator i = adBankAccountBalances.iterator();

            while (i.hasNext()) {

              LocalAdBankAccountBalance adBankAccountBalance = (LocalAdBankAccountBalance) i.next();

              adBankAccountBalance.setBabBalance(
                  adBankAccountBalance.getBabBalance() - arReceipt.getRctAmountCard3());

            }

          } catch (Exception ex) {

            ex.printStackTrace();

          }
        }
        // decrease bank balance CHEQUE
        if (arReceipt.getRctAmountCheque() > 0) {
          LocalAdBankAccount adBankAccount =
              adBankAccountHome.findByPrimaryKey(arReceipt.getAdBankAccount().getBaCode());

          try {

            // find bankaccount balance before or equal receipt date

            Collection adBankAccountBalances = adBankAccountBalanceHome
                .findByBeforeOrEqualDateAndBaCodeAndType(arReceipt.getRctDate(),
                    arReceipt.getAdBankAccount().getBaCode(), "BOOK", AD_CMPNY);

            if (!adBankAccountBalances.isEmpty()) {

              // get last check

              ArrayList adBankAccountBalanceList = new ArrayList(adBankAccountBalances);

              LocalAdBankAccountBalance adBankAccountBalance =
                  (LocalAdBankAccountBalance) adBankAccountBalanceList
                      .get(adBankAccountBalanceList.size() - 1);

              if (adBankAccountBalance.getBabDate().before(arReceipt.getRctDate())) {

                // create new balance

                LocalAdBankAccountBalance adNewBankAccountBalance =
                    adBankAccountBalanceHome.create(arReceipt.getRctDate(),
                        adBankAccountBalance.getBabBalance() - arReceipt.getRctAmountCheque(),
                        "BOOK", AD_CMPNY);

                // adBankAccount.addAdBankAccountBalance(adNewBankAccountBalance);
                adNewBankAccountBalance.setAdBankAccount(adBankAccount);
              } else { // equals to check date

                adBankAccountBalance.setBabBalance(
                    adBankAccountBalance.getBabBalance() - arReceipt.getRctAmountCheque());

              }

            } else {

              // create new balance
              System.out
                  .println("arReceipt.getRctAmountCheque()=" + arReceipt.getRctAmountCheque());
              LocalAdBankAccountBalance adNewBankAccountBalance = adBankAccountBalanceHome.create(
                  arReceipt.getRctDate(), (-arReceipt.getRctAmountCheque()), "BOOK", AD_CMPNY);

              // adBankAccount.addAdBankAccountBalance(adNewBankAccountBalance);
              adNewBankAccountBalance.setAdBankAccount(adBankAccount);

            }

            // propagate to subsequent balances if necessary

            adBankAccountBalances = adBankAccountBalanceHome.findByAfterDateAndBaCodeAndType(
                arReceipt.getRctDate(), arReceipt.getAdBankAccount().getBaCode(), "BOOK", AD_CMPNY);

            Iterator i = adBankAccountBalances.iterator();

            while (i.hasNext()) {

              LocalAdBankAccountBalance adBankAccountBalance = (LocalAdBankAccountBalance) i.next();

              adBankAccountBalance.setBabBalance(
                  adBankAccountBalance.getBabBalance() - arReceipt.getRctAmountCheque());

            }

          } catch (Exception ex) {

            ex.printStackTrace();

          }
        }



        // set receipt post status

        arReceipt.setRctVoidPosted(EJBCommon.TRUE);
        arReceipt.setRctPostedBy(USR_NM);
        arReceipt.setRctDatePosted(EJBCommon.getGcCurrentDateWoTime().getTime());

      }

      // post to gl if necessary

      if (adPreference.getPrfArGlPostingType().equals("AUTO-POST UPON APPROVAL")) {

        // validate if date has no period and period is closed

        LocalGlSetOfBook glJournalSetOfBook = null;

        try {

          glJournalSetOfBook = glSetOfBookHome.findByDate(arReceipt.getRctDate(), AD_CMPNY);

        } catch (FinderException ex) {

          throw new GlJREffectiveDateNoPeriodExistException();

        }

        LocalGlAccountingCalendarValue glAccountingCalendarValue = glAccountingCalendarValueHome
            .findByAcCodeAndDate(glJournalSetOfBook.getGlAccountingCalendar().getAcCode(),
                arReceipt.getRctDate(), AD_CMPNY);


        if (glAccountingCalendarValue.getAcvStatus() == 'N'
            || glAccountingCalendarValue.getAcvStatus() == 'C'
            || glAccountingCalendarValue.getAcvStatus() == 'P') {

          throw new GlJREffectiveDatePeriodClosedException();

        }

        // check if invoice is balance if not check suspense posting

        LocalGlJournalLine glOffsetJournalLine = null;

        Collection arDistributionRecords = null;

        if (arReceipt.getRctVoid() == EJBCommon.FALSE) {

          arDistributionRecords = arDistributionRecordHome.findImportableDrByDrReversedAndRctCode(
              EJBCommon.FALSE, arReceipt.getRctCode(), AD_CMPNY);

        } else {

          arDistributionRecords = arDistributionRecordHome.findImportableDrByDrReversedAndRctCode(
              EJBCommon.TRUE, arReceipt.getRctCode(), AD_CMPNY);

        }


        Iterator j = arDistributionRecords.iterator();

        double TOTAL_DEBIT = 0d;
        double TOTAL_CREDIT = 0d;

        while (j.hasNext()) {

          LocalArDistributionRecord arDistributionRecord = (LocalArDistributionRecord) j.next();
          if (arDistributionRecord.getDrClass().equals("COGS")
              || arDistributionRecord.getDrClass().equals("INVENTORY"))
            continue;
          double DR_AMNT = 0d;

          if (arDistributionRecord.getArAppliedInvoice() != null) {

            LocalArInvoice arInvoice = arDistributionRecord.getArAppliedInvoice()
                .getArInvoicePaymentSchedule().getArInvoice();

            DR_AMNT = this.convertForeignToFunctionalCurrency(
                arInvoice.getGlFunctionalCurrency().getFcCode(),
                arInvoice.getGlFunctionalCurrency().getFcName(), arInvoice.getInvConversionDate(),
                arInvoice.getInvConversionRate(), arDistributionRecord.getDrAmount(), AD_CMPNY);

          } else {

            DR_AMNT = this.convertForeignToFunctionalCurrency(
                arReceipt.getGlFunctionalCurrency().getFcCode(),
                arReceipt.getGlFunctionalCurrency().getFcName(), arReceipt.getRctConversionDate(),
                arReceipt.getRctConversionRate(), arDistributionRecord.getDrAmount(), AD_CMPNY);

          }

          if (arDistributionRecord.getDrDebit() == EJBCommon.TRUE) {

            TOTAL_DEBIT += DR_AMNT;

          } else {

            TOTAL_CREDIT += DR_AMNT;

          }

        }

        TOTAL_DEBIT =
            EJBCommon.roundIt(TOTAL_DEBIT, adCompany.getGlFunctionalCurrency().getFcPrecision());
        TOTAL_CREDIT =
            EJBCommon.roundIt(TOTAL_CREDIT, adCompany.getGlFunctionalCurrency().getFcPrecision());

        System.out.println("TOTAL_DEBIT=" + TOTAL_DEBIT);
        System.out.println("TOTAL_CREDIT=" + TOTAL_CREDIT);
        if (adPreference.getPrfAllowSuspensePosting() == EJBCommon.TRUE
            && TOTAL_DEBIT != TOTAL_CREDIT) {

          LocalGlSuspenseAccount glSuspenseAccount = null;

          try {

            glSuspenseAccount = glSuspenseAccountHome.findByJsNameAndJcName("ACCOUNTS RECEIVABLES",
                "SALES RECEIPTS", AD_CMPNY);

          } catch (FinderException ex) {

            throw new GlobalJournalNotBalanceException();

          }

          if (TOTAL_DEBIT - TOTAL_CREDIT < 0) {

            glOffsetJournalLine =
                glJournalLineHome.create((short) (arDistributionRecords.size() + 1), EJBCommon.TRUE,
                    TOTAL_CREDIT - TOTAL_DEBIT, "", AD_CMPNY);

          } else {

            glOffsetJournalLine =
                glJournalLineHome.create((short) (arDistributionRecords.size() + 1),
                    EJBCommon.FALSE, TOTAL_DEBIT - TOTAL_CREDIT, "", AD_CMPNY);

          }

          LocalGlChartOfAccount glChartOfAccount = glSuspenseAccount.getGlChartOfAccount();
          // glChartOfAccount.addGlJournalLine(glOffsetJournalLine);
          glOffsetJournalLine.setGlChartOfAccount(glChartOfAccount);


        } else if (adPreference.getPrfAllowSuspensePosting() == EJBCommon.FALSE
            && TOTAL_DEBIT != TOTAL_CREDIT) {

          System.out.println("GlobalJournalNotBalanceException()");
          throw new GlobalJournalNotBalanceException();

        }

        // create journal batch if necessary

        LocalGlJournalBatch glJournalBatch = null;
        java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat("MM/dd/yyyy");

        try {

          if (adPreference.getPrfEnableArReceiptBatch() == EJBCommon.TRUE) {

            glJournalBatch =
                glJournalBatchHome.findByJbName("JOURNAL IMPORT " + formatter.format(new Date())
                    + " " + arReceipt.getArReceiptBatch().getRbName(), AD_BRNCH, AD_CMPNY);

          } else {

            glJournalBatch = glJournalBatchHome.findByJbName(
                "JOURNAL IMPORT " + formatter.format(new Date()) + " SALES RECEIPTS", AD_BRNCH,
                AD_CMPNY);

          }


        } catch (FinderException ex) {
        }

        if (adPreference.getPrfEnableGlJournalBatch() == EJBCommon.TRUE && glJournalBatch == null) {

          if (adPreference.getPrfEnableArReceiptBatch() == EJBCommon.TRUE) {

            glJournalBatch = glJournalBatchHome.create(
                "JOURNAL IMPORT " + formatter.format(new Date()) + " "
                    + arReceipt.getArReceiptBatch().getRbName(),
                "JOURNAL IMPORT", "CLOSED", EJBCommon.getGcCurrentDateWoTime().getTime(), USR_NM,
                AD_BRNCH, AD_CMPNY);

          } else {

            glJournalBatch = glJournalBatchHome.create(
                "JOURNAL IMPORT " + formatter.format(new Date()) + " SALES RECEIPTS",
                "JOURNAL IMPORT", "CLOSED", EJBCommon.getGcCurrentDateWoTime().getTime(), USR_NM,
                AD_BRNCH, AD_CMPNY);

          }

        }

        // create journal entry
        String customerName =
            arReceipt.getRctCustomerName() == null ? arReceipt.getArCustomer().getCstName()
                : arReceipt.getRctCustomerName();

        LocalGlJournal glJournal = glJournalHome.create(arReceipt.getRctReferenceNumber(),
            arReceipt.getRctDescription(), arReceipt.getRctDate(), 0.0d, null,
            arReceipt.getRctNumber(), null, 1d, "N/A", null, 'N', EJBCommon.TRUE, EJBCommon.FALSE,
            USR_NM, new Date(), USR_NM, new Date(), null, null, USR_NM,
            EJBCommon.getGcCurrentDateWoTime().getTime(), arReceipt.getArCustomer().getCstTin(),
            customerName, EJBCommon.FALSE, null, AD_BRNCH, AD_CMPNY);

        LocalGlJournalSource glJournalSource =
            glJournalSourceHome.findByJsName("ACCOUNTS RECEIVABLES", AD_CMPNY);
        glJournal.setGlJournalSource(glJournalSource);

        LocalGlFunctionalCurrency glFunctionalCurrency = glFunctionalCurrencyHome
            .findByFcName(adCompany.getGlFunctionalCurrency().getFcName(), AD_CMPNY);
        glJournal.setGlFunctionalCurrency(glFunctionalCurrency);

        LocalGlJournalCategory glJournalCategory =
            glJournalCategoryHome.findByJcName("SALES RECEIPTS", AD_CMPNY);
        glJournal.setGlJournalCategory(glJournalCategory);

        if (glJournalBatch != null) {

          glJournal.setGlJournalBatch(glJournalBatch);
        }


        // create journal lines

        j = arDistributionRecords.iterator();

        while (j.hasNext()) {

          LocalArDistributionRecord arDistributionRecord = (LocalArDistributionRecord) j.next();

          double DR_AMNT = 0d;

          LocalArInvoice arInvoice = null;

          if (arDistributionRecord.getArAppliedInvoice() != null) {

            arInvoice = arDistributionRecord.getArAppliedInvoice().getArInvoicePaymentSchedule()
                .getArInvoice();

            DR_AMNT = this.convertForeignToFunctionalCurrency(
                arInvoice.getGlFunctionalCurrency().getFcCode(),
                arInvoice.getGlFunctionalCurrency().getFcName(), arInvoice.getInvConversionDate(),
                arInvoice.getInvConversionRate(), arDistributionRecord.getDrAmount(), AD_CMPNY);

          } else {

            DR_AMNT = this.convertForeignToFunctionalCurrency(
                arReceipt.getGlFunctionalCurrency().getFcCode(),
                arReceipt.getGlFunctionalCurrency().getFcName(), arReceipt.getRctConversionDate(),
                arReceipt.getRctConversionRate(), arDistributionRecord.getDrAmount(), AD_CMPNY);

          }

          LocalGlJournalLine glJournalLine =
              glJournalLineHome.create(arDistributionRecord.getDrLine(),
                  arDistributionRecord.getDrDebit(), DR_AMNT, "", AD_CMPNY);

          // arDistributionRecord.getGlChartOfAccount().addGlJournalLine(glJournalLine);
          glJournalLine.setGlChartOfAccount(arDistributionRecord.getGlChartOfAccount());

          // glJournal.addGlJournalLine(glJournalLine);
          glJournalLine.setGlJournal(glJournal);

          arDistributionRecord.setDrImported(EJBCommon.TRUE);

          // for FOREX revaluation

          int FC_CODE = arDistributionRecord.getArAppliedInvoice() != null
              ? arInvoice.getGlFunctionalCurrency().getFcCode().intValue()
              : arReceipt.getGlFunctionalCurrency().getFcCode().intValue();

          if ((FC_CODE != adCompany.getGlFunctionalCurrency().getFcCode().intValue())
              && glJournalLine.getGlChartOfAccount().getGlFunctionalCurrency() != null
              && (FC_CODE == glJournalLine.getGlChartOfAccount().getGlFunctionalCurrency()
                  .getFcCode().intValue())) {

            double CONVERSION_RATE = arDistributionRecord.getArAppliedInvoice() != null
                ? arInvoice.getInvConversionRate()
                : arReceipt.getRctConversionRate();

            Date DATE = arDistributionRecord.getArAppliedInvoice() != null
                ? arInvoice.getInvConversionDate()
                : arReceipt.getRctConversionDate();

            if (DATE != null && (CONVERSION_RATE == 0 || CONVERSION_RATE == 1)) {

              CONVERSION_RATE = this.getFrRateByFrNameAndFrDate(
                  glJournalLine.getGlChartOfAccount().getGlFunctionalCurrency().getFcName(),
                  glJournal.getJrConversionDate(), AD_CMPNY);

            } else if (CONVERSION_RATE == 0) {

              CONVERSION_RATE = 1;

            }

            Collection glForexLedgers = null;

            DATE = arDistributionRecord.getArAppliedInvoice() != null ? arInvoice.getInvDate()
                : arReceipt.getRctDate();

            try {

              glForexLedgers = glForexLedgerHome.findLatestGlFrlByFrlDateAndByCoaCode(DATE,
                  glJournalLine.getGlChartOfAccount().getCoaCode(), AD_CMPNY);

            } catch (FinderException ex) {

            }

            LocalGlForexLedger glForexLedger =
                (glForexLedgers.isEmpty() || glForexLedgers == null) ? null
                    : (LocalGlForexLedger) glForexLedgers.iterator().next();

            int FRL_LN = (glForexLedger != null && glForexLedger.getFrlDate().compareTo(DATE) == 0)
                ? glForexLedger.getFrlLine().intValue() + 1
                : 1;

            // compute balance
            double COA_FRX_BLNC = glForexLedger == null ? 0 : glForexLedger.getFrlBalance();
            double FRL_AMNT = arDistributionRecord.getDrAmount();

            if (glJournalLine.getGlChartOfAccount().getCoaAccountType().equalsIgnoreCase("ASSET"))
              FRL_AMNT =
                  (glJournalLine.getJlDebit() == EJBCommon.TRUE ? FRL_AMNT : (-1 * FRL_AMNT));
            else
              FRL_AMNT =
                  (glJournalLine.getJlDebit() == EJBCommon.TRUE ? (-1 * FRL_AMNT) : FRL_AMNT);

            COA_FRX_BLNC = COA_FRX_BLNC + FRL_AMNT;

            glForexLedger = glForexLedgerHome.create(DATE, new Integer(FRL_LN), "OR", FRL_AMNT,
                CONVERSION_RATE, COA_FRX_BLNC, 0d, AD_CMPNY);

            // glJournalLine.getGlChartOfAccount().addGlForexLedger(glForexLedger);
            glForexLedger.setGlChartOfAccount(glJournalLine.getGlChartOfAccount());

            // propagate balances
            try {

              glForexLedgers = glForexLedgerHome.findByGreaterThanFrlDateAndCoaCode(
                  glForexLedger.getFrlDate(), glForexLedger.getGlChartOfAccount().getCoaCode(),
                  glForexLedger.getFrlAdCompany());

            } catch (FinderException ex) {

            }

            Iterator itrFrl = glForexLedgers.iterator();

            while (itrFrl.hasNext()) {

              glForexLedger = (LocalGlForexLedger) itrFrl.next();
              FRL_AMNT = arDistributionRecord.getDrAmount();

              if (glJournalLine.getGlChartOfAccount().getCoaAccountType().equalsIgnoreCase("ASSET"))
                FRL_AMNT =
                    (glJournalLine.getJlDebit() == EJBCommon.TRUE ? FRL_AMNT : (-1 * FRL_AMNT));
              else
                FRL_AMNT =
                    (glJournalLine.getJlDebit() == EJBCommon.TRUE ? (-1 * FRL_AMNT) : FRL_AMNT);

              glForexLedger.setFrlBalance(glForexLedger.getFrlBalance() + FRL_AMNT);

            }

          }

        }

        if (glOffsetJournalLine != null) {

          // glJournal.addGlJournalLine(glOffsetJournalLine);
          glOffsetJournalLine.setGlJournal(glJournal);
        }

        // post journal to gl

        Collection glJournalLines = glJournal.getGlJournalLines();

        Iterator i = glJournalLines.iterator();

        while (i.hasNext()) {

          LocalGlJournalLine glJournalLine = (LocalGlJournalLine) i.next();

          // post current to current acv

          this.postToGl(glAccountingCalendarValue, glJournalLine.getGlChartOfAccount(), true,
              glJournalLine.getJlDebit(), glJournalLine.getJlAmount(), AD_CMPNY);


          // post to subsequent acvs (propagate)

          Collection glSubsequentAccountingCalendarValues =
              glAccountingCalendarValueHome.findSubsequentAcvByAcCodeAndAcvPeriodNumber(
                  glJournalSetOfBook.getGlAccountingCalendar().getAcCode(),
                  glAccountingCalendarValue.getAcvPeriodNumber(), AD_CMPNY);

          Iterator acvsIter = glSubsequentAccountingCalendarValues.iterator();

          while (acvsIter.hasNext()) {

            LocalGlAccountingCalendarValue glSubsequentAccountingCalendarValue =
                (LocalGlAccountingCalendarValue) acvsIter.next();

            this.postToGl(glSubsequentAccountingCalendarValue, glJournalLine.getGlChartOfAccount(),
                false, glJournalLine.getJlDebit(), glJournalLine.getJlAmount(), AD_CMPNY);

          }

          // post to subsequent years if necessary

          Collection glSubsequentSetOfBooks = glSetOfBookHome.findSubsequentSobByAcYear(
              glJournalSetOfBook.getGlAccountingCalendar().getAcYear(), AD_CMPNY);

          if (!glSubsequentSetOfBooks.isEmpty() && glJournalSetOfBook.getSobYearEndClosed() == 1) {

            adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
            LocalGlChartOfAccount glRetainedEarningsAccount = glChartOfAccountHome
                .findByCoaAccountNumber(adCompany.getCmpRetainedEarnings(), AD_CMPNY);

            Iterator sobIter = glSubsequentSetOfBooks.iterator();

            while (sobIter.hasNext()) {

              LocalGlSetOfBook glSubsequentSetOfBook = (LocalGlSetOfBook) sobIter.next();

              String ACCOUNT_TYPE = glJournalLine.getGlChartOfAccount().getCoaAccountType();

              // post to subsequent acvs of subsequent set of book(propagate)

              Collection glAccountingCalendarValues = glAccountingCalendarValueHome.findByAcCode(
                  glSubsequentSetOfBook.getGlAccountingCalendar().getAcCode(), AD_CMPNY);

              Iterator acvIter = glAccountingCalendarValues.iterator();

              while (acvIter.hasNext()) {

                LocalGlAccountingCalendarValue glSubsequentAccountingCalendarValue =
                    (LocalGlAccountingCalendarValue) acvIter.next();

                if (ACCOUNT_TYPE.equals("ASSET") || ACCOUNT_TYPE.equals("LIABILITY")
                    || ACCOUNT_TYPE.equals("OWNERS EQUITY")) {

                  this.postToGl(glSubsequentAccountingCalendarValue,
                      glJournalLine.getGlChartOfAccount(), false, glJournalLine.getJlDebit(),
                      glJournalLine.getJlAmount(), AD_CMPNY);

                } else { // revenue & expense

                  this.postToGl(glSubsequentAccountingCalendarValue, glRetainedEarningsAccount,
                      false, glJournalLine.getJlDebit(), glJournalLine.getJlAmount(), AD_CMPNY);

                }

              }

              if (glSubsequentSetOfBook.getSobYearEndClosed() == 0)
                break;

            }

          }


        }



        if (arReceipt.getArCustomer().getApSupplier() != null) {

          // Post Investors Account balance
          byte scLedger =
              arReceipt.getArCustomer().getApSupplier().getApSupplierClass().getScLedger();


          // post current to current acv
          if (scLedger == EJBCommon.TRUE) {

            this.postToGlInvestor(glAccountingCalendarValue,
                arReceipt.getArCustomer().getApSupplier(), true,
                arReceipt.getRctInvtrBeginningBalance(), (byte) 0, arReceipt.getRctAmount(),
                AD_CMPNY);


            // post to subsequent acvs (propagate)

            Collection glSubsequentAccountingCalendarValues =
                glAccountingCalendarValueHome.findSubsequentAcvByAcCodeAndAcvPeriodNumber(
                    glJournalSetOfBook.getGlAccountingCalendar().getAcCode(),
                    glAccountingCalendarValue.getAcvPeriodNumber(), AD_CMPNY);

            Iterator acvsIter = glSubsequentAccountingCalendarValues.iterator();

            while (acvsIter.hasNext()) {

              LocalGlAccountingCalendarValue glSubsequentAccountingCalendarValue =
                  (LocalGlAccountingCalendarValue) acvsIter.next();


              this.postToGlInvestor(glSubsequentAccountingCalendarValue,
                  arReceipt.getArCustomer().getApSupplier(), false, (byte) 0, (byte) 0,
                  arReceipt.getRctAmount(), AD_CMPNY);

            }


            // post to subsequent years if necessary

            Collection glSubsequentSetOfBooks = glSetOfBookHome.findSubsequentSobByAcYear(
                glJournalSetOfBook.getGlAccountingCalendar().getAcYear(), AD_CMPNY);

            if (!glSubsequentSetOfBooks.isEmpty()
                && glJournalSetOfBook.getSobYearEndClosed() == 1) {

              adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);

              Iterator sobIter = glSubsequentSetOfBooks.iterator();

              while (sobIter.hasNext()) {

                LocalGlSetOfBook glSubsequentSetOfBook = (LocalGlSetOfBook) sobIter.next();

                // post to subsequent acvs of subsequent set of book(propagate)

                Collection glAccountingCalendarValues = glAccountingCalendarValueHome.findByAcCode(
                    glSubsequentSetOfBook.getGlAccountingCalendar().getAcCode(), AD_CMPNY);

                Iterator acvIter = glAccountingCalendarValues.iterator();

                while (acvIter.hasNext()) {

                  LocalGlAccountingCalendarValue glSubsequentAccountingCalendarValue =
                      (LocalGlAccountingCalendarValue) acvIter.next();

                  this.postToGlInvestor(glSubsequentAccountingCalendarValue,
                      arReceipt.getArCustomer().getApSupplier(), false,
                      arReceipt.getRctInvtrBeginningBalance(), (byte) 0, arReceipt.getRctAmount(),
                      AD_CMPNY);

                }

                if (glSubsequentSetOfBook.getSobYearEndClosed() == 0)
                  break;

              }

            }
          }



        }



      }

    } catch (GlJREffectiveDateNoPeriodExistException ex) {

      getSessionContext().setRollbackOnly();
      throw ex;

    } catch (GlJREffectiveDatePeriodClosedException ex) {

      getSessionContext().setRollbackOnly();
      throw ex;

    } catch (GlobalJournalNotBalanceException ex) {

      getSessionContext().setRollbackOnly();
      throw ex;

    } catch (GlobalRecordAlreadyDeletedException ex) {

      getSessionContext().setRollbackOnly();
      throw ex;

    } catch (GlobalTransactionAlreadyPostedException ex) {

      getSessionContext().setRollbackOnly();
      throw ex;

    } catch (GlobalTransactionAlreadyVoidPostedException ex) {

      getSessionContext().setRollbackOnly();
      throw ex;

    } catch (AdPRFCoaGlVarianceAccountNotFoundException ex) {

      throw ex;

    } catch (GlobalExpiryDateNotFoundException ex) {

      getSessionContext().setRollbackOnly();
      throw ex;

    } catch (Exception ex) {

      Debug.printStackTrace(ex);
      getSessionContext().setRollbackOnly();
      throw new EJBException(ex.getMessage());

    }

  }

  private double getInvFifoCost(
      Date CST_DT, Integer IL_CODE, double CST_QTY, double CST_COST, boolean isAdjustFifo,
      Integer AD_BRNCH, Integer AD_CMPNY
  ) {

    LocalInvCostingHome invCostingHome = null;
    LocalInvItemLocationHome invItemLocationHome = null;

    // Initialize EJB Home

    try {

      invCostingHome = (LocalInvCostingHome) EJBHomeFactory
          .lookUpLocalHome(LocalInvCostingHome.JNDI_NAME, LocalInvCostingHome.class);
      invItemLocationHome = (LocalInvItemLocationHome) EJBHomeFactory
          .lookUpLocalHome(LocalInvItemLocationHome.JNDI_NAME, LocalInvItemLocationHome.class);
    } catch (NamingException ex) {

      throw new EJBException(ex.getMessage());
    }

    try {

      Collection invFifoCostings =
          invCostingHome.findFifoRemainingQuantityByLessThanOrEqualCstDateAndIlCodeAndBrCode(CST_DT,
              IL_CODE, AD_BRNCH, AD_CMPNY);

      if (invFifoCostings.size() > 0) {

        Iterator x = invFifoCostings.iterator();

        if (isAdjustFifo) {

          // executed during POST transaction

          double totalCost = 0d;
          double cost;

          if (CST_QTY < 0) {

            // for negative quantities
            double neededQty = -(CST_QTY);

            while (x.hasNext() && neededQty != 0) {

              LocalInvCosting invFifoCosting = (LocalInvCosting) x.next();

              if (invFifoCosting.getApPurchaseOrderLine() != null
                  || invFifoCosting.getApVoucherLineItem() != null) {
                cost = invFifoCosting.getCstItemCost() / invFifoCosting.getCstQuantityReceived();
              } else if (invFifoCosting.getArInvoiceLineItem() != null) {
                cost = invFifoCosting.getCstCostOfSales() / invFifoCosting.getCstQuantitySold();
              } else if (invFifoCosting.getInvBuildUnbuildAssemblyLine() != null) {
                cost =
                    invFifoCosting.getCstAssemblyCost() / invFifoCosting.getCstAssemblyQuantity();
              } else {
                cost = invFifoCosting.getCstAdjustCost() / invFifoCosting.getCstAdjustQuantity();
              }

              if (neededQty <= invFifoCosting.getCstRemainingLifoQuantity()) {

                invFifoCosting.setCstRemainingLifoQuantity(
                    invFifoCosting.getCstRemainingLifoQuantity() - neededQty);
                totalCost += (neededQty * cost);
                neededQty = 0d;
              } else {

                neededQty -= invFifoCosting.getCstRemainingLifoQuantity();
                totalCost += (invFifoCosting.getCstRemainingLifoQuantity() * cost);
                invFifoCosting.setCstRemainingLifoQuantity(0);
              }
            }

            // if needed qty is not yet satisfied but no more quantities to fetch, get the default
            // cost
            if (neededQty != 0) {

              LocalInvItemLocation invItemLocation = invItemLocationHome.findByPrimaryKey(IL_CODE);
              totalCost += (neededQty * invItemLocation.getInvItem().getIiUnitCost());
            }

            cost = totalCost / -CST_QTY;
          }

          else {

            // for positive quantities
            cost = CST_COST;
          }
          return cost;
        }

        else {

          // executed during ENTRY transaction

          LocalInvCosting invFifoCosting = (LocalInvCosting) x.next();

          if (invFifoCosting.getApPurchaseOrderLine() != null
              || invFifoCosting.getApVoucherLineItem() != null) {
            return EJBCommon.roundIt(
                invFifoCosting.getCstItemCost() / invFifoCosting.getCstQuantityReceived(),
                this.getGlFcPrecisionUnit(AD_CMPNY));
          } else if (invFifoCosting.getArInvoiceLineItem() != null) {
            return EJBCommon.roundIt(
                invFifoCosting.getCstCostOfSales() / invFifoCosting.getCstQuantitySold(),
                this.getGlFcPrecisionUnit(AD_CMPNY));
          } else if (invFifoCosting.getInvBuildUnbuildAssemblyLine() != null) {
            return EJBCommon.roundIt(
                invFifoCosting.getCstAssemblyCost() / invFifoCosting.getCstAssemblyQuantity(),
                this.getGlFcPrecisionUnit(AD_CMPNY));
          } else {
            return EJBCommon.roundIt(
                invFifoCosting.getCstAdjustCost() / invFifoCosting.getCstAdjustQuantity(),
                this.getGlFcPrecisionUnit(AD_CMPNY));
          }
        }
      } else {

        // most applicable in 1st entries of data
        LocalInvItemLocation invItemLocation = invItemLocationHome.findByPrimaryKey(IL_CODE);
        return invItemLocation.getInvItem().getIiUnitCost();
      }

    } catch (Exception ex) {
      Debug.printStackTrace(ex);
      throw new EJBException(ex.getMessage());
    }
  }


  private void post(
      Date RCT_DT, double RCT_AMNT, LocalArCustomer arCustomer, Integer AD_CMPNY
  ) {

    Debug.print("ArMiscReceiptEntryControllerBean post");

    LocalArCustomerBalanceHome arCustomerBalanceHome = null;

    // Initialize EJB Home

    try {

      arCustomerBalanceHome = (LocalArCustomerBalanceHome) EJBHomeFactory
          .lookUpLocalHome(LocalArCustomerBalanceHome.JNDI_NAME, LocalArCustomerBalanceHome.class);

    } catch (NamingException ex) {

      getSessionContext().setRollbackOnly();
      throw new EJBException(ex.getMessage());

    }

    try {

      // find customer balance before or equal invoice date

      Collection arCustomerBalances =
          arCustomerBalanceHome.findByBeforeOrEqualInvDateAndCstCustomerCode(RCT_DT,
              arCustomer.getCstCustomerCode(), AD_CMPNY);

      if (!arCustomerBalances.isEmpty()) {

        // get last invoice

        ArrayList arCustomerBalanceList = new ArrayList(arCustomerBalances);

        LocalArCustomerBalance arCustomerBalance =
            (LocalArCustomerBalance) arCustomerBalanceList.get(arCustomerBalanceList.size() - 1);

        if (arCustomerBalance.getCbDate().before(RCT_DT)) {

          // create new balance

          LocalArCustomerBalance arNewCustomerBalance = arCustomerBalanceHome.create(RCT_DT,
              arCustomerBalance.getCbBalance() + RCT_AMNT, AD_CMPNY);

          // arCustomer.addArCustomerBalance(arNewCustomerBalance);
          arNewCustomerBalance.setArCustomer(arCustomer);

        } else { // equals to invoice date

          arCustomerBalance.setCbBalance(arCustomerBalance.getCbBalance() + RCT_AMNT);

        }

      } else {

        // create new balance

        LocalArCustomerBalance arNewCustomerBalance =
            arCustomerBalanceHome.create(RCT_DT, RCT_AMNT, AD_CMPNY);

        // arCustomer.addArCustomerBalance(arNewCustomerBalance);
        arNewCustomerBalance.setArCustomer(arCustomer);

      }

      // propagate to subsequent balances if necessary

      arCustomerBalances = arCustomerBalanceHome.findByAfterInvDateAndCstCustomerCode(RCT_DT,
          arCustomer.getCstCustomerCode(), AD_CMPNY);

      Iterator i = arCustomerBalances.iterator();

      while (i.hasNext()) {

        LocalArCustomerBalance arCustomerBalance = (LocalArCustomerBalance) i.next();

        arCustomerBalance.setCbBalance(arCustomerBalance.getCbBalance() + RCT_AMNT);

      }

    } catch (Exception ex) {

      Debug.printStackTrace(ex);
      getSessionContext().setRollbackOnly();
      throw new EJBException(ex.getMessage());

    }

  }

  private void postToGl(
      LocalGlAccountingCalendarValue glAccountingCalendarValue,
      LocalGlChartOfAccount glChartOfAccount, boolean isCurrentAcv, byte isDebit, double JL_AMNT,
      Integer AD_CMPNY
  ) {

    Debug.print("ArMiscReceiptEntryControllerBean postToGl");

    LocalGlChartOfAccountBalanceHome glChartOfAccountBalanceHome = null;
    LocalAdCompanyHome adCompanyHome = null;


    // Initialize EJB Home

    try {

      glChartOfAccountBalanceHome =
          (LocalGlChartOfAccountBalanceHome) EJBHomeFactory.lookUpLocalHome(
              LocalGlChartOfAccountBalanceHome.JNDI_NAME, LocalGlChartOfAccountBalanceHome.class);
      adCompanyHome = (LocalAdCompanyHome) EJBHomeFactory
          .lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);


    } catch (NamingException ex) {

      throw new EJBException(ex.getMessage());

    }

    try {

      LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);

      LocalGlChartOfAccountBalance glChartOfAccountBalance =
          glChartOfAccountBalanceHome.findByAcvCodeAndCoaCode(
              glAccountingCalendarValue.getAcvCode(), glChartOfAccount.getCoaCode(), AD_CMPNY);

      String ACCOUNT_TYPE = glChartOfAccount.getCoaAccountType();
      short FC_EXTNDD_PRCSN = adCompany.getGlFunctionalCurrency().getFcPrecision();



      if (((ACCOUNT_TYPE.equals("ASSET") || ACCOUNT_TYPE.equals("EXPENSE"))
          && isDebit == EJBCommon.TRUE)
          || (!ACCOUNT_TYPE.equals("ASSET") && !ACCOUNT_TYPE.equals("EXPENSE")
              && isDebit == EJBCommon.FALSE)) {

        glChartOfAccountBalance.setCoabEndingBalance(EJBCommon
            .roundIt(glChartOfAccountBalance.getCoabEndingBalance() + JL_AMNT, FC_EXTNDD_PRCSN));

        if (!isCurrentAcv) {

          glChartOfAccountBalance.setCoabBeginningBalance(EJBCommon.roundIt(
              glChartOfAccountBalance.getCoabBeginningBalance() + JL_AMNT, FC_EXTNDD_PRCSN));

        }


      } else {

        glChartOfAccountBalance.setCoabEndingBalance(EJBCommon
            .roundIt(glChartOfAccountBalance.getCoabEndingBalance() - JL_AMNT, FC_EXTNDD_PRCSN));

        if (!isCurrentAcv) {

          glChartOfAccountBalance.setCoabBeginningBalance(EJBCommon.roundIt(
              glChartOfAccountBalance.getCoabBeginningBalance() - JL_AMNT, FC_EXTNDD_PRCSN));

        }

      }

      if (isCurrentAcv) {

        if (isDebit == EJBCommon.TRUE) {

          glChartOfAccountBalance.setCoabTotalDebit(EJBCommon
              .roundIt(glChartOfAccountBalance.getCoabTotalDebit() + JL_AMNT, FC_EXTNDD_PRCSN));

        } else {

          glChartOfAccountBalance.setCoabTotalCredit(EJBCommon
              .roundIt(glChartOfAccountBalance.getCoabTotalCredit() + JL_AMNT, FC_EXTNDD_PRCSN));
        }

      }

    } catch (Exception ex) {

      Debug.printStackTrace(ex);
      throw new EJBException(ex.getMessage());

    }

  }



  private void postToGlInvestor(
      LocalGlAccountingCalendarValue glAccountingCalendarValue, LocalApSupplier apSupplier,
      boolean isCurrentAcv, byte isBeginningBalance, byte isDebit, double JL_AMNT, Integer AD_CMPNY
  ) {

    Debug.print("ArMiscReceiptEntryControllerBean postToGlInvestor");

    LocalGlChartOfAccountBalanceHome glChartOfAccountBalanceHome = null;
    LocalAdCompanyHome adCompanyHome = null;

    LocalGlInvestorAccountBalanceHome glInvestorAccountBalanceHome = null;


    // Initialize EJB Home

    try {

      glChartOfAccountBalanceHome =
          (LocalGlChartOfAccountBalanceHome) EJBHomeFactory.lookUpLocalHome(
              LocalGlChartOfAccountBalanceHome.JNDI_NAME, LocalGlChartOfAccountBalanceHome.class);
      adCompanyHome = (LocalAdCompanyHome) EJBHomeFactory
          .lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
      glInvestorAccountBalanceHome =
          (LocalGlInvestorAccountBalanceHome) EJBHomeFactory.lookUpLocalHome(
              LocalGlInvestorAccountBalanceHome.JNDI_NAME, LocalGlInvestorAccountBalanceHome.class);



    } catch (NamingException ex) {

      throw new EJBException(ex.getMessage());

    }

    try {

      LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);


      LocalGlInvestorAccountBalance glInvestorAccountBalance =
          glInvestorAccountBalanceHome.findByAcvCodeAndSplCode(
              glAccountingCalendarValue.getAcvCode(), apSupplier.getSplCode(), AD_CMPNY);


      short FC_EXTNDD_PRCSN = adCompany.getGlFunctionalCurrency().getFcPrecision();


      glInvestorAccountBalance.setIrabEndingBalance(EJBCommon
          .roundIt(glInvestorAccountBalance.getIrabEndingBalance() + JL_AMNT, FC_EXTNDD_PRCSN));


      if (!isCurrentAcv) {

        glInvestorAccountBalance.setIrabBeginningBalance(EJBCommon.roundIt(
            glInvestorAccountBalance.getIrabBeginningBalance() + JL_AMNT, FC_EXTNDD_PRCSN));

      }

      if (isCurrentAcv) {

        glInvestorAccountBalance.setIrabTotalCredit(EJBCommon
            .roundIt(glInvestorAccountBalance.getIrabTotalCredit() + JL_AMNT, FC_EXTNDD_PRCSN));

      }


      if (isBeginningBalance != 0) {
        glInvestorAccountBalance.setIrabBonus(EJBCommon.TRUE);
        glInvestorAccountBalance.setIrabInterest(EJBCommon.TRUE);
      } else {
        glInvestorAccountBalance.setIrabBonus(EJBCommon.FALSE);
        glInvestorAccountBalance.setIrabInterest(EJBCommon.FALSE);
      }

    } catch (Exception ex) {

      Debug.printStackTrace(ex);
      throw new EJBException(ex.getMessage());

    }

  }



  private double convertByUomFromAndItemAndQuantity(
      LocalInvUnitOfMeasure invFromUnitOfMeasure, LocalInvItem invItem, double ADJST_QTY,
      Integer AD_CMPNY
  ) {

    Debug.print("ArMiscReceiptEntryControllerBean convertByUomFromAndItemAndQuantity");

    LocalAdPreferenceHome adPreferenceHome = null;
    LocalInvUnitOfMeasureConversionHome invUnitOfMeasureConversionHome = null;

    // Initialize EJB Home

    try {

      adPreferenceHome = (LocalAdPreferenceHome) EJBHomeFactory
          .lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);
      invUnitOfMeasureConversionHome = (LocalInvUnitOfMeasureConversionHome) EJBHomeFactory
          .lookUpLocalHome(LocalInvUnitOfMeasureConversionHome.JNDI_NAME,
              LocalInvUnitOfMeasureConversionHome.class);

    } catch (NamingException ex) {

      throw new EJBException(ex.getMessage());

    }

    try {
      Debug.print("ArMiscReceiptEntryControllerBean convertByUomFromAndItemAndQuantity A");
      LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);

      LocalInvUnitOfMeasureConversion invUnitOfMeasureConversion =
          invUnitOfMeasureConversionHome.findUmcByIiNameAndUomName(invItem.getIiName(),
              invFromUnitOfMeasure.getUomName(), AD_CMPNY);
      LocalInvUnitOfMeasureConversion invDefaultUomConversion =
          invUnitOfMeasureConversionHome.findUmcByIiNameAndUomName(invItem.getIiName(),
              invItem.getInvUnitOfMeasure().getUomName(), AD_CMPNY);
      Debug.print("ArMiscReceiptEntryControllerBean convertByUomFromAndItemAndQuantity B");
      return EJBCommon.roundIt(
          ADJST_QTY * invDefaultUomConversion.getUmcConversionFactor()
              / invUnitOfMeasureConversion.getUmcConversionFactor(),
          adPreference.getPrfInvQuantityPrecisionUnit());


    } catch (Exception ex) {

      Debug.printStackTrace(ex);
      getSessionContext().setRollbackOnly();
      throw new EJBException(ex.getMessage());

    }

  }

  private double convertByUomAndQuantity(
      LocalInvUnitOfMeasure invFromUnitOfMeasure, LocalInvItem invItem, double ADJST_QTY,
      Integer AD_CMPNY
  ) {

    Debug.print("ArInvoicePostControllerBean convertByUomAndQuantity");

    LocalAdPreferenceHome adPreferenceHome = null;
    LocalInvUnitOfMeasureConversionHome invUnitOfMeasureConversionHome = null;

    // Initialize EJB Home

    try {

      adPreferenceHome = (LocalAdPreferenceHome) EJBHomeFactory
          .lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);
      invUnitOfMeasureConversionHome = (LocalInvUnitOfMeasureConversionHome) EJBHomeFactory
          .lookUpLocalHome(LocalInvUnitOfMeasureConversionHome.JNDI_NAME,
              LocalInvUnitOfMeasureConversionHome.class);

    } catch (NamingException ex) {

      throw new EJBException(ex.getMessage());

    }

    try {

      LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);

      LocalInvUnitOfMeasureConversion invUnitOfMeasureConversion =
          invUnitOfMeasureConversionHome.findUmcByIiNameAndUomName(invItem.getIiName(),
              invFromUnitOfMeasure.getUomName(), AD_CMPNY);
      LocalInvUnitOfMeasureConversion invDefaultUomConversion =
          invUnitOfMeasureConversionHome.findUmcByIiNameAndUomName(invItem.getIiName(),
              invItem.getInvUnitOfMeasure().getUomName(), AD_CMPNY);

      return EJBCommon.roundIt(
          ADJST_QTY * invDefaultUomConversion.getUmcConversionFactor()
              / invUnitOfMeasureConversion.getUmcConversionFactor(),
          adPreference.getPrfInvQuantityPrecisionUnit());

    } catch (Exception ex) {

      Debug.printStackTrace(ex);
      getSessionContext().setRollbackOnly();
      throw new EJBException(ex.getMessage());

    }

  }


  private LocalArInvoiceLineItem addArIliEntry(
      ArModInvoiceLineItemDetails mdetails, LocalArReceipt arReceipt,
      LocalInvItemLocation invItemLocation, Integer AD_CMPNY
  ) throws GlobalMiscInfoIsRequiredException {

    Debug.print("ArMiscReceiptEntryControllerBean addArIliEntry");

    LocalArInvoiceLineItemHome arInvoiceLineItemHome = null;
    LocalInvItemLocationHome invItemLocationHome = null;
    LocalInvUnitOfMeasureHome invUnitOfMeasureHome = null;

    // Initialize EJB Home

    try {

      arInvoiceLineItemHome = (LocalArInvoiceLineItemHome) EJBHomeFactory
          .lookUpLocalHome(LocalArInvoiceLineItemHome.JNDI_NAME, LocalArInvoiceLineItemHome.class);
      invItemLocationHome = (LocalInvItemLocationHome) EJBHomeFactory
          .lookUpLocalHome(LocalInvItemLocationHome.JNDI_NAME, LocalInvItemLocationHome.class);
      invUnitOfMeasureHome = (LocalInvUnitOfMeasureHome) EJBHomeFactory
          .lookUpLocalHome(LocalInvUnitOfMeasureHome.JNDI_NAME, LocalInvUnitOfMeasureHome.class);

    } catch (NamingException ex) {

      throw new EJBException(ex.getMessage());

    }

    try {

      short precisionUnit = this.getGlFcPrecisionUnit(AD_CMPNY);

      double ILI_AMNT = 0d;
      double ILI_TAX_AMNT = 0d;

      // calculate net amount

      LocalArTaxCode arTaxCode = arReceipt.getArTaxCode();

      if (arTaxCode.getTcType().equals("INCLUSIVE") && mdetails.getIliTax() == EJBCommon.TRUE) {

        ILI_AMNT = EJBCommon.roundIt(mdetails.getIliAmount() / (1 + (arTaxCode.getTcRate() / 100)),
            precisionUnit);

      } else {

        // tax exclusive, none, zero rated or exempt

        ILI_AMNT = mdetails.getIliAmount();

      }

      // calculate tax

      if (!arTaxCode.getTcType().equals("NONE") && !arTaxCode.getTcType().equals("EXEMPT")) {

        if (mdetails.getIliTax() == EJBCommon.TRUE) {
          if (arTaxCode.getTcType().equals("INCLUSIVE")) {

            ILI_TAX_AMNT = EJBCommon.roundIt(mdetails.getIliAmount() - ILI_AMNT, precisionUnit);

          } else if (arTaxCode.getTcType().equals("EXCLUSIVE")) {

            ILI_TAX_AMNT = EJBCommon.roundIt(mdetails.getIliAmount() * arTaxCode.getTcRate() / 100,
                precisionUnit);

          } else {

            // tax none zero-rated or exempt

          }
        }



      }
      System.out.println("mdetails.getIliTotalDiscount()=" + mdetails.getIliTotalDiscount());
      LocalArInvoiceLineItem arInvoiceLineItem = arInvoiceLineItemHome.create(mdetails.getIliLine(),
          mdetails.getIliQuantity(), mdetails.getIliUnitPrice(), ILI_AMNT, ILI_TAX_AMNT,
          mdetails.getIliEnableAutoBuild(), mdetails.getIliDiscount1(), mdetails.getIliDiscount2(),
          mdetails.getIliDiscount3(), mdetails.getIliDiscount4(), mdetails.getIliTotalDiscount(),
          mdetails.getIliTax(), AD_CMPNY);

      // arReceipt.addArInvoiceLineItem(arInvoiceLineItem);
      arInvoiceLineItem.setArReceipt(arReceipt);
      // invItemLocation.addArInvoiceLineItem(arInvoiceLineItem);
      arInvoiceLineItem.setInvItemLocation(invItemLocation);

      LocalInvUnitOfMeasure invUnitOfMeasure =
          invUnitOfMeasureHome.findByUomName(mdetails.getIliUomName(), AD_CMPNY);
      // invUnitOfMeasure.addArInvoiceLineItem(arInvoiceLineItem);
      arInvoiceLineItem.setInvUnitOfMeasure(invUnitOfMeasure);

      if (arInvoiceLineItem.getInvItemLocation().getInvItem().getIiTraceMisc() == 1) {

        this.createInvTagList(arInvoiceLineItem, mdetails.getiIliTagList(), AD_CMPNY);
      }



      // validate misc
      /*
       * if(arInvoiceLineItem.getInvItemLocation().getInvItem().getIiTraceMisc()==1){
       * if(mdetails.getIliMisc()==null || mdetails.getIliMisc()==""){
       * 
       * 
       * throw new GlobalMiscInfoIsRequiredException();
       * 
       * }else{ System.out.println("mdetails.getIliMisc(): " + mdetails.getIliMisc()); int qty2Prpgt
       * =0; try{ qty2Prpgt = Integer.parseInt(this.getQuantityExpiryDates(mdetails.getIliMisc()));
       * }catch(Exception e){ qty2Prpgt =this.checkExpiryDates(mdetails.getIliMisc()+"fin$"); }
       * 
       * String miscList2Prpgt = this.checkExpiryDates(mdetails.getIliMisc(), qty2Prpgt, "False");
       * if(miscList2Prpgt!="Error"){ arInvoiceLineItem.setIliMisc(mdetails.getIliMisc()); }else{
       * throw new GlobalMiscInfoIsRequiredException(); }
       * 
       * } }else{ arInvoiceLineItem.setIliMisc(mdetails.getIliMisc()); }
       * 
       * arInvoiceLineItem.setIliMisc(mdetails.getIliMisc());
       */
      return arInvoiceLineItem;

    } /*
       * catch (GlobalMiscInfoIsRequiredException ex){
       * 
       * getSessionContext().setRollbackOnly(); throw ex;
       * 
       * }
       */catch (Exception ex) {

      Debug.printStackTrace(ex);
      getSessionContext().setRollbackOnly();
      throw new EJBException(ex.getMessage());

    }

  }

  public static int checkExpiryDates(
      String misc
  ) throws Exception {

    String separator = "$";

    // Remove first $ character
    misc = misc.substring(1);
    // System.out.println("misc: " + misc);
    // Counter
    int start = 0;
    int nextIndex = misc.indexOf(separator, start);
    int length = nextIndex - start;
    int numberExpry = 0;
    String miscList = new String();
    String miscList2 = "";
    String g = "";
    try {
      while (g != "fin") {
        // Date
        start = nextIndex + 1;
        nextIndex = misc.indexOf(separator, start);
        length = nextIndex - start;
        g = misc.substring(start, start + length);
        if (g.length() != 0) {
          if (g != null || g != "" || g != "null") {
            if (g.contains("null")) {
              miscList2 = "Error";
            } else {
              miscList = miscList + "$" + g;
              numberExpry++;
            }
          } else {
            miscList2 = "Error";
          }

        } else {
          miscList2 = "Error";
        }
      }
    } catch (Exception e) {

    }

    if (miscList2 == "") {
      miscList = miscList + "$";
    } else {
      miscList = miscList2;
    }

    return (numberExpry);
  }

  public String checkExpiryDates(
      String misc, double qty, String reverse
  ) throws Exception {
    // ActionErrors errors = new ActionErrors();
    Debug.print("ApReceivingItemControllerBean getExpiryDates");
    System.out.println("misc: " + misc);

    String separator = "";
    if (reverse == "False") {
      separator = "$";
    } else {
      separator = " ";
    }

    // Remove first $ character
    misc = misc.substring(1);
    System.out.println("misc: " + misc);
    // Counter
    int start = 0;
    int nextIndex = misc.indexOf(separator, start);
    int length = nextIndex - start;

    String miscList = new String();
    String miscList2 = "";

    for (int x = 0; x < qty; x++) {

      // Date
      start = nextIndex + 1;
      nextIndex = misc.indexOf(separator, start);
      length = nextIndex - start;
      String g = misc.substring(start, start + length);
      System.out.println("g: " + g);
      System.out.println("g length: " + g.length());
      if (g.length() != 0) {
        if (g != null || g != "" || g != "null") {
          if (g.contains("null")) {
            miscList2 = "Error";
          } else {
            miscList = miscList + "$" + g;
          }
        } else {
          miscList2 = "Error";
        }

        System.out.println("miscList G: " + miscList);
      } else {
        miscList2 = "Error";
      }
    }
    System.out.println("miscList2 :" + miscList2);
    if (miscList2 == "") {
      miscList = miscList + "$";
    } else {
      miscList = miscList2;
    }

    System.out.println("miscList :" + miscList);
    return (miscList);
  }

  private void addArDrIliEntry(
      short DR_LN, String DR_CLSS, byte DR_DBT, double DR_AMNT, Integer COA_CODE,
      LocalArReceipt arReceipt, Integer AD_BRNCH, Integer AD_CMPNY
  ) throws GlobalBranchAccountNumberInvalidException {

    Debug.print("ArMiscReceiptEntryControllerBean addArDrIliEntry");

    LocalArDistributionRecordHome arDistributionRecordHome = null;
    LocalGlChartOfAccountHome glChartOfAccountHome = null;
    LocalAdCompanyHome adCompanyHome = null;


    // Initialize EJB Home

    try {

      arDistributionRecordHome = (LocalArDistributionRecordHome) EJBHomeFactory.lookUpLocalHome(
          LocalArDistributionRecordHome.JNDI_NAME, LocalArDistributionRecordHome.class);
      glChartOfAccountHome = (LocalGlChartOfAccountHome) EJBHomeFactory
          .lookUpLocalHome(LocalGlChartOfAccountHome.JNDI_NAME, LocalGlChartOfAccountHome.class);
      adCompanyHome = (LocalAdCompanyHome) EJBHomeFactory
          .lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);

    } catch (NamingException ex) {

      throw new EJBException(ex.getMessage());

    }

    try {

      // get company

      LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);

      LocalGlChartOfAccount glChartOfAccount =
          glChartOfAccountHome.findByCoaCodeAndBranchCode(COA_CODE, AD_BRNCH, AD_CMPNY);

      // create distribution record

      LocalArDistributionRecord arDistributionRecord =
          arDistributionRecordHome.create(DR_LN, DR_CLSS, DR_DBT,
              EJBCommon.roundIt(DR_AMNT, adCompany.getGlFunctionalCurrency().getFcPrecision()),
              EJBCommon.FALSE, EJBCommon.FALSE, AD_CMPNY);

      // arReceipt.addArDistributionRecord(arDistributionRecord);
      arDistributionRecord.setArReceipt(arReceipt);
      // glChartOfAccount.addArDistributionRecord(arDistributionRecord);
      arDistributionRecord.setGlChartOfAccount(glChartOfAccount);

    } catch (FinderException ex) {

      // throw new GlobalBranchAccountNumberInvalidException(ex.getMessage(),DR_LN);
      throw new GlobalBranchAccountNumberInvalidException("" + DR_LN);

    } catch (Exception ex) {

      Debug.printStackTrace(ex);
      getSessionContext().setRollbackOnly();
      throw new EJBException(ex.getMessage());

    }

  }

  private void postToInv(
      LocalArInvoiceLineItem arInvoiceLineItem, Date CST_DT, double CST_QTY_SLD,
      double CST_CST_OF_SLS, double CST_RMNNG_QTY, double CST_RMNNG_VL, double CST_VRNC_VL,
      String USR_NM, Integer AD_BRNCH, Integer AD_CMPNY
  ) throws AdPRFCoaGlVarianceAccountNotFoundException, GlobalExpiryDateNotFoundException {

    Debug.print("ArMiscReceiptEntryControllerBean postToInv");

    LocalInvCostingHome invCostingHome = null;
    LocalAdPreferenceHome adPreferenceHome = null;
    LocalAdCompanyHome adCompanyHome = null;
    LocalInvAdjustmentLineHome invAdjustmentLineHome = null;

    // Initialize EJB Home

    try {

      invCostingHome = (LocalInvCostingHome) EJBHomeFactory
          .lookUpLocalHome(LocalInvCostingHome.JNDI_NAME, LocalInvCostingHome.class);
      adPreferenceHome = (LocalAdPreferenceHome) EJBHomeFactory
          .lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);
      adCompanyHome = (LocalAdCompanyHome) EJBHomeFactory
          .lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
      invAdjustmentLineHome = (LocalInvAdjustmentLineHome) EJBHomeFactory
          .lookUpLocalHome(LocalInvAdjustmentLineHome.JNDI_NAME, LocalInvAdjustmentLineHome.class);

    } catch (NamingException ex) {

      throw new EJBException(ex.getMessage());

    }

    try {

      LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);
      LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
      LocalInvItemLocation invItemLocation = arInvoiceLineItem.getInvItemLocation();
      int CST_LN_NMBR = 0;

      CST_QTY_SLD = EJBCommon.roundIt(CST_QTY_SLD, adPreference.getPrfInvQuantityPrecisionUnit());
      CST_CST_OF_SLS =
          EJBCommon.roundIt(CST_CST_OF_SLS, adCompany.getGlFunctionalCurrency().getFcPrecision());
      CST_RMNNG_QTY =
          EJBCommon.roundIt(CST_RMNNG_QTY, adPreference.getPrfInvQuantityPrecisionUnit());
      CST_RMNNG_VL =
          EJBCommon.roundIt(CST_RMNNG_VL, adCompany.getGlFunctionalCurrency().getFcPrecision());

      if (CST_QTY_SLD > 0 && arInvoiceLineItem.getIliEnableAutoBuild() == EJBCommon.FALSE) {

        invItemLocation
            .setIlCommittedQuantity(invItemLocation.getIlCommittedQuantity() - CST_QTY_SLD);

      }

      try {

        // generate line number
        LocalInvCosting invCurrentCosting =
            invCostingHome.getByMaxCstLineNumberAndCstDateToLongAndIiNameAndLocName(
                CST_DT.getTime(), invItemLocation.getInvItem().getIiName(),
                invItemLocation.getInvLocation().getLocName(), AD_BRNCH, AD_CMPNY);
        CST_LN_NMBR = invCurrentCosting.getCstLineNumber() + 1;

      } catch (FinderException ex) {

        CST_LN_NMBR = 1;

      }

      // void subsequent cost variance adjustments
      Collection invAdjustmentLines =
          invAdjustmentLineHome.findUnvoidAndIsCostVarianceGreaterThanAdjDateAndIlCodeAndBrCode(
              CST_DT, invItemLocation.getIlCode(), AD_BRNCH, AD_CMPNY);
      Iterator i = invAdjustmentLines.iterator();

      while (i.hasNext()) {

        LocalInvAdjustmentLine invAdjustmentLine = (LocalInvAdjustmentLine) i.next();
        this.voidInvAdjustment(invAdjustmentLine.getInvAdjustment(), AD_BRNCH, AD_CMPNY);

      }

      String prevExpiryDates = "";
      String miscListPrpgt = "";
      double qtyPrpgt = 0;
      try {
        LocalInvCosting prevCst = invCostingHome
            .getByMaxCstDateToLongAndMaxCstLineNumberAndLessThanEqualCstDateAndRemainingQuantityNotEqualToZeroAndIlCode(
                CST_DT, invItemLocation.getIlCode(), AD_BRNCH, AD_CMPNY);
        Debug.print("ArReceiptPostControllerBean postToInv B");
        prevExpiryDates = prevCst.getCstExpiryDate();
        qtyPrpgt = prevCst.getCstRemainingQuantity();

        if (prevExpiryDates == null) {
          prevExpiryDates = "";
        }

      } catch (Exception ex) {
        System.out.println("prevExpiryDates CATCH: " + prevExpiryDates);
        prevExpiryDates = "";
      }

      // create costing
      LocalInvCosting invCosting = invCostingHome.create(CST_DT, CST_DT.getTime(), CST_LN_NMBR, 0d,
          0d, 0d, 0d, 0d, 0d, CST_QTY_SLD, CST_CST_OF_SLS, CST_RMNNG_QTY, CST_RMNNG_VL, 0d, 0d, 0d,
          AD_BRNCH, AD_CMPNY);
      invCosting.setInvItemLocation(invItemLocation);
      invCosting.setArInvoiceLineItem(arInvoiceLineItem);

      // Get Latest Expiry Dates
      if (arInvoiceLineItem.getInvItemLocation().getInvItem().getIiTraceMisc() != 0) {
        if (prevExpiryDates != null && prevExpiryDates != "" && prevExpiryDates.length() != 0) {
          if (arInvoiceLineItem.getIliMisc() != null && arInvoiceLineItem.getIliMisc() != ""
              && arInvoiceLineItem.getIliMisc().length() != 0) {
            int qty2Prpgt =
                Integer.parseInt(this.getQuantityExpiryDates(arInvoiceLineItem.getIliMisc()));

            String miscList2Prpgt =
                this.propagateExpiryDates(arInvoiceLineItem.getIliMisc(), qty2Prpgt, "False");
            ArrayList miscList = this.expiryDates(arInvoiceLineItem.getIliMisc(), qty2Prpgt);
            String propagateMiscPrpgt = "";
            String ret = "";
            String exp = "";
            String Checker = "";

            Iterator mi = miscList.iterator();

            propagateMiscPrpgt = prevExpiryDates;
            ret = propagateMiscPrpgt;
            while (mi.hasNext()) {
              String miscStr = (String) mi.next();

              Integer qTest = this.checkExpiryDates(ret + "fin$");
              ArrayList miscList2 =
                  this.expiryDates("$" + ret, Double.parseDouble(qTest.toString()));

              // ArrayList miscList2 = this.expiryDates("$" + ret, qtyPrpgt);
              Iterator m2 = miscList2.iterator();
              ret = "";
              String ret2 = "false";
              int a = 0;
              while (m2.hasNext()) {
                String miscStr2 = (String) m2.next();

                if (ret2 == "1st") {
                  ret2 = "false";
                }
                System.out.println("miscStr: " + miscStr);
                System.out.println("miscStr2: " + miscStr2);

                if (miscStr2.equals(miscStr)) {
                  if (a == 0) {
                    a = 1;
                    ret2 = "1st";
                    Checker = "true";
                  } else {
                    a = a + 1;
                    ret2 = "true";
                  }
                }
                System.out.println("Checker: " + Checker);
                System.out.println("ret2: " + ret2);
                if (!miscStr2.equals(miscStr) || a > 1) {
                  if ((ret2 != "1st") && ((ret2 == "false") || (ret2 == "true"))) {
                    if (miscStr2 != "") {
                      miscStr2 = "$" + miscStr2;
                      ret = ret + miscStr2;
                      System.out.println("ret " + ret);
                      ret2 = "false";
                    }
                  }
                }

              }
              if (ret != "") {
                ret = ret + "$";
              }
              System.out.println("ret una: " + ret);
              exp = exp + ret;
              qtyPrpgt = qtyPrpgt - 1;
            }
            System.out.println("ret fin " + ret);
            System.out.println("exp fin " + exp);
            // propagateMiscPrpgt = propagateMiscPrpgt.replace(" ", "$");
            propagateMiscPrpgt = ret;
            // propagateMiscPrpgt = this.propagateExpiryDates(propagateMiscPrpgt, a, "True");
            if (Checker == "true") {
              // invCosting.setCstExpiryDate(propagateMiscPrpgt);
            } else {
              System.out.println("Exp Not Found");
              throw new GlobalExpiryDateNotFoundException(
                  arInvoiceLineItem.getInvItemLocation().getInvItem().getIiName());
            }
            invCosting.setCstExpiryDate(propagateMiscPrpgt);
          } else {
            invCosting.setCstExpiryDate(prevExpiryDates);
          }

        } else {
          if (arInvoiceLineItem.getIliMisc() != null && arInvoiceLineItem.getIliMisc() != ""
              && arInvoiceLineItem.getIliMisc().length() != 0) {
            int initialQty =
                Integer.parseInt(this.getQuantityExpiryDates(arInvoiceLineItem.getIliMisc()));
            String initialPrpgt =
                this.propagateExpiryDates(arInvoiceLineItem.getIliMisc(), initialQty, "False");

            invCosting.setCstExpiryDate(initialPrpgt);
          } else {
            invCosting.setCstExpiryDate(prevExpiryDates);
          }

        }
      }



      // if cost variance is not 0, generate cost variance for the transaction
      if (CST_VRNC_VL != 0) {

        this.generateCostVariance(invCosting.getInvItemLocation(), CST_VRNC_VL,
            "APMR" + arInvoiceLineItem.getArReceipt().getRctNumber(),
            arInvoiceLineItem.getArReceipt().getRctDescription(),
            arInvoiceLineItem.getArReceipt().getRctDate(), USR_NM, AD_BRNCH, AD_CMPNY);

      }

      // propagate balance if necessary
      Collection invCostings = invCostingHome.findByGreaterThanCstDateAndIiNameAndLocName(CST_DT,
          invItemLocation.getInvItem().getIiName(), invItemLocation.getInvLocation().getLocName(),
          AD_BRNCH, AD_CMPNY);
      String miscList = "";
      ArrayList miscList2 = null;
      i = invCostings.iterator();
      String propagateMisc = "";
      String ret = "";
      while (i.hasNext()) {
        String Checker = "";
        String Checker2 = "";

        LocalInvCosting invPropagatedCosting = (LocalInvCosting) i.next();

        invPropagatedCosting
            .setCstRemainingQuantity(invPropagatedCosting.getCstRemainingQuantity() - CST_QTY_SLD);
        invPropagatedCosting
            .setCstRemainingValue(invPropagatedCosting.getCstRemainingValue() - CST_CST_OF_SLS);

        if (arInvoiceLineItem.getInvItemLocation().getInvItem().getIiTraceMisc() != 0) {
          if (arInvoiceLineItem.getIliMisc() != null && arInvoiceLineItem.getIliMisc() != ""
              && arInvoiceLineItem.getIliMisc().length() != 0) {
            double qty =
                Double.parseDouble(this.getQuantityExpiryDates(arInvoiceLineItem.getIliMisc()));
            // invPropagatedCosting.getInvAdjustmentLine().getAlMisc();
            miscList = this.propagateExpiryDates(arInvoiceLineItem.getIliMisc(), qty, "False");
            miscList2 = this.expiryDates(arInvoiceLineItem.getIliMisc(), qty);
            System.out.println("arInvoiceLineItem.getIliMisc(): " + arInvoiceLineItem.getIliMisc());
            System.out.println("getAlAdjustQuantity(): " + arInvoiceLineItem.getIliQuantity());

            if (arInvoiceLineItem.getIliQuantity() < 0) {
              Iterator mi = miscList2.iterator();

              propagateMisc = invPropagatedCosting.getCstExpiryDate();
              ret = invPropagatedCosting.getCstExpiryDate();
              while (mi.hasNext()) {
                String miscStr = (String) mi.next();

                Integer qTest = this.checkExpiryDates(ret + "fin$");
                ArrayList miscList3 =
                    this.expiryDates("$" + ret, Double.parseDouble(qTest.toString()));

                // ArrayList miscList3 = this.expiryDates("$" + ret, qtyPrpgt);
                System.out.println("ret: " + ret);
                Iterator m2 = miscList3.iterator();
                ret = "";
                String ret2 = "false";
                int a = 0;
                while (m2.hasNext()) {
                  String miscStr2 = (String) m2.next();

                  if (ret2 == "1st") {
                    ret2 = "false";
                  }
                  System.out.println("2 miscStr: " + miscStr);
                  System.out.println("2 miscStr2: " + miscStr2);
                  if (miscStr2.equals(miscStr)) {
                    if (a == 0) {
                      a = 1;
                      ret2 = "1st";
                      Checker2 = "true";
                    } else {
                      a = a + 1;
                      ret2 = "true";
                    }
                  }
                  System.out.println("Checker: " + Checker2);
                  if (!miscStr2.equals(miscStr) || a > 1) {
                    if ((ret2 != "1st") || (ret2 == "false") || (ret2 == "true")) {
                      if (miscStr2 != "") {
                        miscStr2 = "$" + miscStr2;
                        ret = ret + miscStr2;
                        ret2 = "false";
                      }
                    }
                  }

                }
                if (Checker2 != "true") {
                  throw new GlobalExpiryDateNotFoundException(
                      arInvoiceLineItem.getInvItemLocation().getInvItem().getIiName());
                }

                ret = ret + "$";
                qtyPrpgt = qtyPrpgt - 1;
              }
            }

          }


          if (arInvoiceLineItem.getIliMisc() != null && arInvoiceLineItem.getIliMisc() != ""
              && arInvoiceLineItem.getIliMisc().length() != 0) {
            if (prevExpiryDates != null && prevExpiryDates != "" && prevExpiryDates.length() != 0) {

              Iterator mi = miscList2.iterator();

              propagateMisc = invPropagatedCosting.getCstExpiryDate();
              ret = propagateMisc;
              while (mi.hasNext()) {
                String miscStr = (String) mi.next();

                Integer qTest = this.checkExpiryDates(ret + "fin$");
                ArrayList miscList3 =
                    this.expiryDates("$" + ret, Double.parseDouble(qTest.toString()));

                // ArrayList miscList3 = this.expiryDates("$" + ret, qtyPrpgt);
                System.out.println("ret: " + ret);
                Iterator m2 = miscList3.iterator();
                ret = "";
                String ret2 = "false";
                int a = 0;
                while (m2.hasNext()) {
                  String miscStr2 = (String) m2.next();

                  if (ret2 == "1st") {
                    ret2 = "false";
                  }
                  System.out.println("2 miscStr: " + miscStr);
                  System.out.println("2 miscStr2: " + miscStr2);
                  if (miscStr2.equals(miscStr)) {
                    if (a == 0) {
                      a = 1;
                      ret2 = "1st";
                      Checker = "true";
                    } else {
                      a = a + 1;
                      ret2 = "true";
                    }
                  }
                  System.out.println("Checker: " + Checker);
                  if (!miscStr2.equals(miscStr) || a > 1) {
                    if ((ret2 != "1st") || (ret2 == "false") || (ret2 == "true")) {
                      if (miscStr2 != "") {
                        miscStr2 = "$" + miscStr2;
                        ret = ret + miscStr2;
                        ret2 = "false";
                      }
                    }
                  }

                }
                ret = ret + "$";
                qtyPrpgt = qtyPrpgt - 1;
              }
              propagateMisc = ret;
            }

          } else {
            try {
              propagateMisc = miscList + invPropagatedCosting.getCstExpiryDate().substring(1,
                  invPropagatedCosting.getCstExpiryDate().length());
            } catch (Exception e) {
              propagateMisc = miscList;
            }
          }
          invPropagatedCosting.setCstExpiryDate(propagateMisc);
        }

      }

      if (adPreference.getPrfArAllowPriorDate() == EJBCommon.FALSE) {
        // regenerate cost variance
        this.regenerateCostVariance(invCostings, invCosting, AD_BRNCH, AD_CMPNY);
      }

    } catch (AdPRFCoaGlVarianceAccountNotFoundException ex) {

      throw ex;

    } catch (GlobalExpiryDateNotFoundException ex) {
      throw ex;
    } catch (Exception ex) {

      Debug.printStackTrace(ex);
      getSessionContext().setRollbackOnly();
      throw new EJBException(ex.getMessage());

    }

  }

  public String getQuantityExpiryDates(
      String qntty
  ) {
    String separator = "$";
    String y = "";
    try {
      // Remove first $ character
      qntty = qntty.substring(1);

      // Counter
      int start = 0;
      int nextIndex = qntty.indexOf(separator, start);
      int length = nextIndex - start;

      y = (qntty.substring(start, start + length));
      System.out.println("Y " + y);
    } catch (Exception e) {
      y = "0";
    }


    return y;
  }

  private ArrayList expiryDates(
      String misc, double qty
  ) throws Exception {
    Debug.print("ApReceivingItemControllerBean getExpiryDates");
    System.out.println("misc: " + misc);
    String separator = "$";


    // Remove first $ character
    misc = misc.substring(1);

    // Counter
    int start = 0;
    int nextIndex = misc.indexOf(separator, start);
    int length = nextIndex - start;

    System.out.println("qty" + qty);
    ArrayList miscList = new ArrayList();

    for (int x = 0; x < qty; x++) {

      // Date
      start = nextIndex + 1;
      nextIndex = misc.indexOf(separator, start);
      length = nextIndex - start;
      System.out.println("x" + x);
      String checker = misc.substring(start, start + length);
      System.out.println("checker" + checker);
      if (checker.length() != 0 || checker != "null") {
        miscList.add(checker);
      } else {
        miscList.add("null");
      }
    }

    System.out.println("miscList :" + miscList);
    return miscList;
  }

  public String propagateExpiryDates(
      String misc, double qty, String reverse
  ) throws Exception {
    // ActionErrors errors = new ActionErrors();
    Debug.print("ApReceivingItemControllerBean getExpiryDates");
    System.out.println("misc: " + misc);

    String separator = "";
    if (reverse == "False") {
      separator = "$";
    } else {
      separator = " ";
    }

    // Remove first $ character
    misc = misc.substring(1);
    System.out.println("misc: " + misc);
    // Counter
    int start = 0;
    int nextIndex = misc.indexOf(separator, start);
    int length = nextIndex - start;

    String miscList = new String();

    for (int x = 0; x < qty; x++) {

      // Date
      start = nextIndex + 1;
      nextIndex = misc.indexOf(separator, start);
      length = nextIndex - start;
      String g = misc.substring(start, start + length);
      System.out.println("g: " + g);
      System.out.println("g length: " + g.length());
      if (g.length() != 0) {
        miscList = miscList + "$" + g;
        System.out.println("miscList G: " + miscList);
      }
    }

    miscList = miscList + "$";
    System.out.println("miscList :" + miscList);
    return (miscList);
  }

  private void postToBua(
      LocalArInvoiceLineItem arInvoiceLineItem, Date CST_DT, double CST_ASSMBLY_QTY,
      double CST_ASSMBLY_CST, double CST_RMNNG_QTY, double CST_RMNNG_VL, String II_NM,
      String LOC_NM, double CST_VRNC_VL, String USR_NM, Integer AD_BRNCH, Integer AD_CMPNY
  ) throws AdPRFCoaGlVarianceAccountNotFoundException {

    Debug.print("ArMiscReceiptEntryControllerBean postToBua");

    LocalInvCostingHome invCostingHome = null;
    LocalAdPreferenceHome adPreferenceHome = null;
    LocalAdCompanyHome adCompanyHome = null;
    LocalInvItemLocationHome invItemLocationHome = null;
    LocalInvAdjustmentLineHome invAdjustmentLineHome = null;

    // Initialize EJB Home

    try {

      invCostingHome = (LocalInvCostingHome) EJBHomeFactory
          .lookUpLocalHome(LocalInvCostingHome.JNDI_NAME, LocalInvCostingHome.class);
      adPreferenceHome = (LocalAdPreferenceHome) EJBHomeFactory
          .lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);
      adCompanyHome = (LocalAdCompanyHome) EJBHomeFactory
          .lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
      invItemLocationHome = (LocalInvItemLocationHome) EJBHomeFactory
          .lookUpLocalHome(LocalInvItemLocationHome.JNDI_NAME, LocalInvItemLocationHome.class);
      invAdjustmentLineHome = (LocalInvAdjustmentLineHome) EJBHomeFactory
          .lookUpLocalHome(LocalInvAdjustmentLineHome.JNDI_NAME, LocalInvAdjustmentLineHome.class);

    } catch (NamingException ex) {

      throw new EJBException(ex.getMessage());

    }

    try {

      LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);
      LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
      LocalInvItemLocation invItemLocation =
          invItemLocationHome.findByLocNameAndIiName(LOC_NM, II_NM, AD_CMPNY);
      int CST_LN_NMBR = 0;

      CST_ASSMBLY_QTY =
          EJBCommon.roundIt(CST_ASSMBLY_QTY, adPreference.getPrfInvQuantityPrecisionUnit());
      CST_ASSMBLY_CST =
          EJBCommon.roundIt(CST_ASSMBLY_CST, adCompany.getGlFunctionalCurrency().getFcPrecision());
      CST_RMNNG_QTY =
          EJBCommon.roundIt(CST_RMNNG_QTY, adPreference.getPrfInvQuantityPrecisionUnit());
      CST_RMNNG_VL =
          EJBCommon.roundIt(CST_RMNNG_VL, adCompany.getGlFunctionalCurrency().getFcPrecision());

      if ((CST_ASSMBLY_QTY < 0 && arInvoiceLineItem.getIliEnableAutoBuild() == 0)
          || (CST_ASSMBLY_QTY < 0 && arInvoiceLineItem.getIliEnableAutoBuild() == 1
              && !arInvoiceLineItem.getInvItemLocation().getInvItem()
                  .equals(invItemLocation.getInvItem()))) {

        invItemLocation.setIlCommittedQuantity(
            invItemLocation.getIlCommittedQuantity() - Math.abs(CST_ASSMBLY_QTY));

      }

      try {

        // generate line number
        LocalInvCosting invCurrentCosting =
            invCostingHome.getByMaxCstLineNumberAndCstDateToLongAndIiNameAndLocName(
                CST_DT.getTime(), invItemLocation.getInvItem().getIiName(),
                invItemLocation.getInvLocation().getLocName(), AD_BRNCH, AD_CMPNY);
        CST_LN_NMBR = invCurrentCosting.getCstLineNumber() + 1;

      } catch (FinderException ex) {

        CST_LN_NMBR = 1;

      }

      // void subsequent cost variance adjustments
      Collection invAdjustmentLines =
          invAdjustmentLineHome.findUnvoidAndIsCostVarianceGreaterThanAdjDateAndIlCodeAndBrCode(
              CST_DT, invItemLocation.getIlCode(), AD_BRNCH, AD_CMPNY);
      Iterator i = invAdjustmentLines.iterator();

      while (i.hasNext()) {

        LocalInvAdjustmentLine invAdjustmentLine = (LocalInvAdjustmentLine) i.next();
        this.voidInvAdjustment(invAdjustmentLine.getInvAdjustment(), AD_BRNCH, AD_CMPNY);

      }

      // create costing
      LocalInvCosting invCosting = invCostingHome.create(CST_DT, CST_DT.getTime(), CST_LN_NMBR, 0d,
          0d, CST_ASSMBLY_QTY, CST_ASSMBLY_CST, 0d, 0d, 0d, 0d, CST_RMNNG_QTY, CST_RMNNG_VL, 0d, 0d,
          CST_ASSMBLY_QTY > 0 ? CST_ASSMBLY_QTY : 0, AD_BRNCH, AD_CMPNY);
      // invItemLocation.addInvCosting(invCosting);
      invCosting.setInvItemLocation(invItemLocation);

      // if cost variance is not 0, generate cost variance for the transaction
      if (CST_VRNC_VL != 0) {

        this.generateCostVariance(invCosting.getInvItemLocation(), CST_VRNC_VL,
            "ARMR" + arInvoiceLineItem.getArReceipt().getRctNumber(),
            arInvoiceLineItem.getArReceipt().getRctDescription(),
            arInvoiceLineItem.getArReceipt().getRctDate(), USR_NM, AD_BRNCH, AD_CMPNY);

      }

      // propagate balance if necessary

      Collection invCostings = invCostingHome.findByGreaterThanCstDateAndIiNameAndLocName(CST_DT,
          invItemLocation.getInvItem().getIiName(), invItemLocation.getInvLocation().getLocName(),
          AD_BRNCH, AD_CMPNY);

      i = invCostings.iterator();

      while (i.hasNext()) {

        LocalInvCosting invPropagatedCosting = (LocalInvCosting) i.next();

        invPropagatedCosting.setCstRemainingQuantity(
            invPropagatedCosting.getCstRemainingQuantity() + CST_ASSMBLY_QTY);
        invPropagatedCosting
            .setCstRemainingValue(invPropagatedCosting.getCstRemainingValue() + CST_ASSMBLY_CST);

      }

      if (adPreference.getPrfArAllowPriorDate() == EJBCommon.FALSE) {
        // regenerate cost variance
        this.regenerateCostVariance(invCostings, invCosting, AD_BRNCH, AD_CMPNY);
      }

    } catch (AdPRFCoaGlVarianceAccountNotFoundException ex) {

      throw ex;

    } catch (Exception ex) {

      Debug.printStackTrace(ex);
      getSessionContext().setRollbackOnly();
      throw new EJBException(ex.getMessage());

    }

  }

  private void regenerateInventoryDr(
      LocalArReceipt arReceipt, Integer AD_BRNCH, Integer AD_CMPNY
  ) throws GlobalInventoryDateException, GlobalBranchAccountNumberInvalidException {

    Debug.print("ArMiscReceiptEntryControllerBean regenerateInventoryDr");

    LocalArDistributionRecordHome arDistributionRecordHome = null;
    LocalInvCostingHome invCostingHome = null;
    LocalInvItemHome invItemHome = null;
    LocalInvItemLocationHome invItemLocationHome = null;
    LocalAdBranchItemLocationHome adBranchItemLocationHome = null;
    LocalAdPreferenceHome adPreferenceHome = null;

    InvRepItemCostingControllerHome homeRIC = null;
    InvRepItemCostingController ejbRIC = null;

    // Initialize EJB Home

    try {

      homeRIC = (InvRepItemCostingControllerHome) com.util.EJBHomeFactory
          .lookUpHome("ejb/InvRepItemCostingControllerEJB", InvRepItemCostingControllerHome.class);


      arDistributionRecordHome = (LocalArDistributionRecordHome) EJBHomeFactory.lookUpLocalHome(
          LocalArDistributionRecordHome.JNDI_NAME, LocalArDistributionRecordHome.class);
      invCostingHome = (LocalInvCostingHome) EJBHomeFactory
          .lookUpLocalHome(LocalInvCostingHome.JNDI_NAME, LocalInvCostingHome.class);
      invItemHome = (LocalInvItemHome) EJBHomeFactory.lookUpLocalHome(LocalInvItemHome.JNDI_NAME,
          LocalInvItemHome.class);
      invItemLocationHome = (LocalInvItemLocationHome) EJBHomeFactory
          .lookUpLocalHome(LocalInvItemLocationHome.JNDI_NAME, LocalInvItemLocationHome.class);
      adBranchItemLocationHome = (LocalAdBranchItemLocationHome) EJBHomeFactory.lookUpLocalHome(
          LocalAdBranchItemLocationHome.JNDI_NAME, LocalAdBranchItemLocationHome.class);
      adPreferenceHome = (LocalAdPreferenceHome) EJBHomeFactory
          .lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);

    } catch (NamingException ex) {

      throw new EJBException(ex.getMessage());

    }

    try {
      ejbRIC = homeRIC.create();
    } catch (RemoteException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    } catch (CreateException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }

    try {

      LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);

      // regenerate inventory distribution records

      Collection arDistributionRecords = null;

      if (arReceipt.getRctVoid() == EJBCommon.FALSE) {

        arDistributionRecords = arDistributionRecordHome.findImportableDrByDrReversedAndRctCode(
            EJBCommon.FALSE, arReceipt.getRctCode(), AD_CMPNY);

      } else {

        arDistributionRecords = arDistributionRecordHome.findImportableDrByDrReversedAndRctCode(
            EJBCommon.TRUE, arReceipt.getRctCode(), AD_CMPNY);

      }

      Iterator i = arDistributionRecords.iterator();

      while (i.hasNext()) {

        LocalArDistributionRecord arDistributionRecord = (LocalArDistributionRecord) i.next();

        if (arDistributionRecord.getDrClass().equals("COGS")
            || arDistributionRecord.getDrClass().equals("INVENTORY")) {

          i.remove();
          arDistributionRecord.remove();

        }

      }

      Collection arInvoiceLineItems = arReceipt.getArInvoiceLineItems();

      if (arInvoiceLineItems != null && !arInvoiceLineItems.isEmpty()) {

        i = arInvoiceLineItems.iterator();

        while (i.hasNext()) {

          LocalArInvoiceLineItem arInvoiceLineItem = (LocalArInvoiceLineItem) i.next();
          LocalInvItemLocation invItemLocation = arInvoiceLineItem.getInvItemLocation();
          if (adPreference.getPrfArAllowPriorDate() == EJBCommon.FALSE) {
            Collection invNegTxnCosting =
                invCostingHome.findNegTxnByGreaterThanCstDateAndIiNameAndLocName(
                    arReceipt.getRctDate(), invItemLocation.getInvItem().getIiName(),
                    invItemLocation.getInvLocation().getLocName(), AD_BRNCH, AD_CMPNY);
            if (!invNegTxnCosting.isEmpty())
              throw new GlobalInventoryDateException(invItemLocation.getInvItem().getIiName());
          }

          // add cost of sales distribution and inventory


          double COST = arInvoiceLineItem.getInvItemLocation().getInvItem().getIiUnitCost();

          try {

            LocalInvCosting invCosting = invCostingHome
                .getByMaxCstDateToLongAndMaxCstLineNumberAndLessThanEqualCstDateAndIiNameAndLocName(
                    arReceipt.getRctDate(), invItemLocation.getInvItem().getIiName(),
                    invItemLocation.getInvLocation().getLocName(), AD_BRNCH, AD_CMPNY);

            if (invCosting.getCstRemainingQuantity() <= 0
                && invCosting.getCstRemainingValue() <= 0) {
              System.out.println("RE CALC");
              HashMap criteria = new HashMap();
              criteria.put("itemName", invItemLocation.getInvItem().getIiName());
              criteria.put("location", invItemLocation.getInvLocation().getLocName());

              ArrayList branchList = new ArrayList();

              AdBranchDetails mdetails = new AdBranchDetails();
              mdetails.setBrCode(AD_BRNCH);
              branchList.add(mdetails);

              ejbRIC.executeInvFixItemCosting(criteria, branchList, AD_CMPNY);

              invCosting = invCostingHome
                  .getByMaxCstDateToLongAndMaxCstLineNumberAndLessThanEqualCstDateAndIiNameAndLocName(
                      arReceipt.getRctDate(), invItemLocation.getInvItem().getIiName(),
                      invItemLocation.getInvLocation().getLocName(), AD_BRNCH, AD_CMPNY);
            }


            if (invCosting.getInvItemLocation().getInvItem().getIiCostMethod().equals("Average"))

              COST = invCosting.getCstRemainingQuantity() <= 0 ? COST
                  : Math.abs(
                      invCosting.getCstRemainingValue() / invCosting.getCstRemainingQuantity());


            if (COST <= 0) {
              invCosting.getInvItemLocation().getInvItem().getIiUnitCost();
            }

            else if (invCosting.getInvItemLocation().getInvItem().getIiCostMethod().equals("FIFO"))

              COST = invCosting.getCstRemainingQuantity() == 0 ? COST
                  : Math.abs(this.getInvFifoCost(invCosting.getCstDate(),
                      invCosting.getInvItemLocation().getIlCode(),
                      arInvoiceLineItem.getIliQuantity(), arInvoiceLineItem.getIliUnitPrice(),
                      false, AD_BRNCH, AD_CMPNY));

            else if (invCosting.getInvItemLocation().getInvItem().getIiCostMethod()
                .equals("Standard"))

              COST = invCosting.getInvItemLocation().getInvItem().getIiUnitCost();

          } catch (FinderException ex) {
          }



          double QTY_SLD =
              this.convertByUomFromAndItemAndQuantity(arInvoiceLineItem.getInvUnitOfMeasure(),
                  arInvoiceLineItem.getInvItemLocation().getInvItem(),
                  arInvoiceLineItem.getIliQuantity(), AD_CMPNY);

          LocalAdBranchItemLocation adBranchItemLocation = null;

          try {

            adBranchItemLocation = adBranchItemLocationHome.findBilByIlCodeAndBrCode(
                arInvoiceLineItem.getInvItemLocation().getIlCode(), AD_BRNCH, AD_CMPNY);

          } catch (FinderException ex) {

          }

          if (arInvoiceLineItem.getIliEnableAutoBuild() == EJBCommon.FALSE && arInvoiceLineItem
              .getInvItemLocation().getInvItem().getIiNonInventoriable() == EJBCommon.FALSE) {

            if (adBranchItemLocation != null) {

              this.addArDrIliEntry(arReceipt.getArDrNextLine(), "COGS", EJBCommon.TRUE,
                  COST * QTY_SLD, adBranchItemLocation.getBilCoaGlCostOfSalesAccount(), arReceipt,
                  AD_BRNCH, AD_CMPNY);

              this.addArDrIliEntry(arReceipt.getArDrNextLine(), "INVENTORY", EJBCommon.FALSE,
                  COST * QTY_SLD, adBranchItemLocation.getBilCoaGlInventoryAccount(), arReceipt,
                  AD_BRNCH, AD_CMPNY);

            } else {

              this.addArDrIliEntry(arReceipt.getArDrNextLine(), "COGS", EJBCommon.TRUE,
                  COST * QTY_SLD,
                  arInvoiceLineItem.getInvItemLocation().getIlGlCoaCostOfSalesAccount(), arReceipt,
                  AD_BRNCH, AD_CMPNY);

              this.addArDrIliEntry(arReceipt.getArDrNextLine(), "INVENTORY", EJBCommon.FALSE,
                  COST * QTY_SLD,
                  arInvoiceLineItem.getInvItemLocation().getIlGlCoaInventoryAccount(), arReceipt,
                  AD_BRNCH, AD_CMPNY);

            }

          }



        }
      }

    } catch (GlobalInventoryDateException ex) {

      getSessionContext().setRollbackOnly();
      throw ex;

    } catch (GlobalBranchAccountNumberInvalidException ex) {

      getSessionContext().setRollbackOnly();
      throw ex;

    } catch (Exception ex) {

      Debug.printStackTrace(ex);
      getSessionContext().setRollbackOnly();
      throw new EJBException(ex.getMessage());

    }

  }

  private double convertCostByUom(
      String II_NM, String UOM_NM, double unitCost, boolean isFromDefault, Integer AD_CMPNY
  ) {

    Debug.print("ArInvoiceEntryControllerBean convertCostByUom");

    LocalInvItemHome invItemHome = null;
    LocalInvUnitOfMeasureConversionHome invUnitOfMeasureConversionHome = null;

    // Initialize EJB Home

    try {

      invUnitOfMeasureConversionHome = (LocalInvUnitOfMeasureConversionHome) EJBHomeFactory
          .lookUpLocalHome(LocalInvUnitOfMeasureConversionHome.JNDI_NAME,
              LocalInvUnitOfMeasureConversionHome.class);
      invItemHome = (LocalInvItemHome) EJBHomeFactory.lookUpLocalHome(LocalInvItemHome.JNDI_NAME,
          LocalInvItemHome.class);

    } catch (NamingException ex) {

      throw new EJBException(ex.getMessage());

    }

    try {

      LocalInvItem invItem = invItemHome.findByIiName(II_NM, AD_CMPNY);
      LocalInvUnitOfMeasureConversion invUnitOfMeasureConversion =
          invUnitOfMeasureConversionHome.findUmcByIiNameAndUomName(II_NM, UOM_NM, AD_CMPNY);
      LocalInvUnitOfMeasureConversion invDefaultUomConversion = invUnitOfMeasureConversionHome
          .findUmcByIiNameAndUomName(II_NM, invItem.getInvUnitOfMeasure().getUomName(), AD_CMPNY);

      if (isFromDefault) {

        return unitCost * invDefaultUomConversion.getUmcConversionFactor()
            / invUnitOfMeasureConversion.getUmcConversionFactor();

      } else {

        return unitCost * invUnitOfMeasureConversion.getUmcConversionFactor()
            / invDefaultUomConversion.getUmcConversionFactor();

      }

    } catch (Exception ex) {

      Debug.printStackTrace(ex);
      getSessionContext().setRollbackOnly();
      throw new EJBException(ex.getMessage());

    }

  }

  private void voidInvAdjustment(
      LocalInvAdjustment invAdjustment, Integer AD_BRNCH, Integer AD_CMPNY
  ) {

    Debug.print("ArMiscReceiptEntryControllerBean voidInvAdjustment");

    try {

      Collection invDistributionRecords = invAdjustment.getInvDistributionRecords();
      ArrayList list = new ArrayList();

      Iterator i = invDistributionRecords.iterator();

      while (i.hasNext()) {

        LocalInvDistributionRecord invDistributionRecord = (LocalInvDistributionRecord) i.next();

        list.add(invDistributionRecord);

      }

      i = list.iterator();

      while (i.hasNext()) {

        LocalInvDistributionRecord invDistributionRecord = (LocalInvDistributionRecord) i.next();

        this.addInvDrEntry(invAdjustment.getInvDrNextLine(), invDistributionRecord.getDrClass(),
            invDistributionRecord.getDrDebit() == EJBCommon.TRUE ? EJBCommon.FALSE : EJBCommon.TRUE,
            invDistributionRecord.getDrAmount(), EJBCommon.TRUE,
            invDistributionRecord.getInvChartOfAccount().getCoaCode(), invAdjustment, AD_BRNCH,
            AD_CMPNY);

      }

      Collection invAdjustmentLines = invAdjustment.getInvAdjustmentLines();
      i = invAdjustmentLines.iterator();
      list.clear();

      while (i.hasNext()) {

        LocalInvAdjustmentLine invAdjustmentLine = (LocalInvAdjustmentLine) i.next();

        list.add(invAdjustmentLine);

      }

      i = list.iterator();

      while (i.hasNext()) {

        LocalInvAdjustmentLine invAdjustmentLine = (LocalInvAdjustmentLine) i.next();

        this.addInvAlEntry(invAdjustmentLine.getInvItemLocation(), invAdjustment,
            (invAdjustmentLine.getAlUnitCost()) * -1, EJBCommon.TRUE, AD_CMPNY);

      }

      invAdjustment.setAdjVoid(EJBCommon.TRUE);

      this.executeInvAdjPost(invAdjustment.getAdjCode(), invAdjustment.getAdjLastModifiedBy(),
          AD_BRNCH, AD_CMPNY);

    } catch (Exception ex) {

      Debug.printStackTrace(ex);
      getSessionContext().setRollbackOnly();
      throw new EJBException(ex.getMessage());

    }

  }

  private void generateCostVariance(
      LocalInvItemLocation invItemLocation, double CST_VRNC_VL, String ADJ_RFRNC_NMBR,
      String ADJ_DSCRPTN, Date ADJ_DT, String USR_NM, Integer AD_BRNCH, Integer AD_CMPNY
  ) throws AdPRFCoaGlVarianceAccountNotFoundException {
    /*
     * Debug.print("ArMiscReceiptEntryControllerBean generateCostVariance");
     * 
     * LocalAdPreferenceHome adPreferenceHome = null; LocalGlChartOfAccountHome glChartOfAccountHome
     * = null; LocalAdBranchItemLocationHome adBranchItemLocationHome = null;
     * 
     * // Initialize EJB Home
     * 
     * try {
     * 
     * adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
     * lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);
     * glChartOfAccountHome = (LocalGlChartOfAccountHome)EJBHomeFactory.
     * lookUpLocalHome(LocalGlChartOfAccountHome.JNDI_NAME, LocalGlChartOfAccountHome.class);
     * adBranchItemLocationHome = (LocalAdBranchItemLocationHome)EJBHomeFactory.
     * lookUpLocalHome(LocalAdBranchItemLocationHome.JNDI_NAME,
     * LocalAdBranchItemLocationHome.class);
     * 
     * 
     * } catch (NamingException ex) {
     * 
     * throw new EJBException(ex.getMessage());
     * 
     * }
     * 
     * try{
     * 
     * LocalInvAdjustment newInvAdjustment = this.saveInvAdjustment(ADJ_RFRNC_NMBR, ADJ_DSCRPTN,
     * ADJ_DT, USR_NM, AD_BRNCH, AD_CMPNY); LocalAdPreference adPreference =
     * adPreferenceHome.findByPrfAdCompany(AD_CMPNY); LocalGlChartOfAccount glCoaVarianceAccount =
     * null;
     * 
     * if(adPreference.getPrfInvGlCoaVarianceAccount() == null) throw new
     * AdPRFCoaGlVarianceAccountNotFoundException();
     * 
     * try{
     * 
     * glCoaVarianceAccount =
     * glChartOfAccountHome.findByPrimaryKey(adPreference.getPrfInvGlCoaVarianceAccount());
     * //glCoaVarianceAccount.addInvAdjustment(newInvAdjustment);
     * newInvAdjustment.setGlChartOfAccount(glCoaVarianceAccount);
     * 
     * } catch (FinderException ex) {
     * 
     * throw new AdPRFCoaGlVarianceAccountNotFoundException();
     * 
     * }
     * 
     * LocalInvAdjustmentLine invAdjustmentLine = this.addInvAlEntry(invItemLocation,
     * newInvAdjustment, CST_VRNC_VL, EJBCommon.FALSE, AD_CMPNY);
     * 
     * // check for branch mapping
     * 
     * LocalAdBranchItemLocation adBranchItemLocation = null;
     * 
     * try{
     * 
     * adBranchItemLocation = adBranchItemLocationHome.findBilByIlCodeAndBrCode(
     * invAdjustmentLine.getInvItemLocation().getIlCode(), AD_BRNCH, AD_CMPNY);
     * 
     * } catch (FinderException ex) {
     * 
     * }
     * 
     * LocalGlChartOfAccount glInventoryChartOfAccount = null;
     * 
     * if (adBranchItemLocation == null) {
     * 
     * glInventoryChartOfAccount = glChartOfAccountHome.findByPrimaryKey(
     * invAdjustmentLine.getInvItemLocation().getIlGlCoaInventoryAccount()); } else {
     * 
     * glInventoryChartOfAccount = glChartOfAccountHome.findByPrimaryKey(
     * adBranchItemLocation.getBilCoaGlInventoryAccount());
     * 
     * }
     * 
     * 
     * boolean isDebit = CST_VRNC_VL < 0 ? false : true;
     * 
     * //inventory dr this.addInvDrEntry(newInvAdjustment.getInvDrNextLine(), "INVENTORY", isDebit
     * == true ? EJBCommon.TRUE : EJBCommon.FALSE, Math.abs(CST_VRNC_VL), EJBCommon.FALSE,
     * glInventoryChartOfAccount.getCoaCode(), newInvAdjustment, AD_BRNCH, AD_CMPNY);
     * 
     * //variance dr this.addInvDrEntry(newInvAdjustment.getInvDrNextLine(),"VARIANCE", !isDebit ==
     * true ? EJBCommon.TRUE : EJBCommon.FALSE, Math.abs(CST_VRNC_VL), EJBCommon.FALSE,
     * glCoaVarianceAccount.getCoaCode(), newInvAdjustment, AD_BRNCH, AD_CMPNY);
     * 
     * this.executeInvAdjPost(newInvAdjustment.getAdjCode(),
     * newInvAdjustment.getAdjLastModifiedBy(), AD_BRNCH, AD_CMPNY);
     * 
     * } catch (AdPRFCoaGlVarianceAccountNotFoundException ex) {
     * 
     * getSessionContext().setRollbackOnly(); throw ex;
     * 
     * } catch (Exception ex) {
     * 
     * Debug.printStackTrace(ex); getSessionContext().setRollbackOnly(); throw new
     * EJBException(ex.getMessage());
     * 
     * }
     */
  }

  private void regenerateCostVariance(
      Collection invCostings, LocalInvCosting invCosting, Integer AD_BRNCH, Integer AD_CMPNY
  ) throws AdPRFCoaGlVarianceAccountNotFoundException {
    /*
     * Debug.print("ArMiscReceiptEntryControllerBean regenerateCostVariance");
     * 
     * try {
     * 
     * Iterator i = invCostings.iterator(); LocalInvCosting prevInvCosting = invCosting;
     * 
     * while (i.hasNext()) {
     * 
     * LocalInvCosting invPropagatedCosting = (LocalInvCosting)i.next();
     * 
     * Debug.print("ArMiscReceiptEntryControllerBean regenerateCostVariance A");
     * if(prevInvCosting.getCstRemainingQuantity() < 0) {
     * 
     * double TTL_CST = 0; double QNTY = 0; String ADJ_RFRNC_NMBR = ""; String ADJ_DSCRPTN = "";
     * String ADJ_CRTD_BY = "";
     * 
     * // get unit cost adjusment, document number and unit of measure if
     * (invPropagatedCosting.getApPurchaseOrderLine() != null) {
     * 
     * TTL_CST = invPropagatedCosting.getApPurchaseOrderLine().getPlAmount(); QNTY =
     * this.convertByUomFromAndItemAndQuantity(
     * invPropagatedCosting.getApPurchaseOrderLine().getInvUnitOfMeasure(),
     * invPropagatedCosting.getApPurchaseOrderLine().getInvItemLocation().getInvItem(),
     * invPropagatedCosting.getApPurchaseOrderLine().getPlQuantity(), AD_CMPNY);
     * Debug.print("ArMiscReceiptEntryControllerBean regenerateCostVariance B"); ADJ_DSCRPTN =
     * invPropagatedCosting.getApPurchaseOrderLine().getApPurchaseOrder().getPoDescription();
     * ADJ_CRTD_BY =
     * invPropagatedCosting.getApPurchaseOrderLine().getApPurchaseOrder().getPoPostedBy();
     * ADJ_RFRNC_NMBR = "APRI" +
     * invPropagatedCosting.getApPurchaseOrderLine().getApPurchaseOrder().getPoDocumentNumber();
     * 
     * } else if (invPropagatedCosting.getApVoucherLineItem() != null){
     * 
     * TTL_CST = invPropagatedCosting.getApVoucherLineItem().getVliAmount(); QNTY =
     * this.convertByUomFromAndItemAndQuantity(
     * invPropagatedCosting.getApVoucherLineItem().getInvUnitOfMeasure(),
     * invPropagatedCosting.getApVoucherLineItem().getInvItemLocation().getInvItem(),
     * invPropagatedCosting.getApVoucherLineItem().getVliQuantity(), AD_CMPNY);
     * Debug.print("ArMiscReceiptEntryControllerBean regenerateCostVariance C"); if
     * (invPropagatedCosting.getApVoucherLineItem().getApVoucher() != null) {
     * 
     * ADJ_DSCRPTN = invPropagatedCosting.getApVoucherLineItem().getApVoucher().getVouDescription();
     * ADJ_CRTD_BY = invPropagatedCosting.getApVoucherLineItem().getApVoucher().getVouPostedBy();
     * ADJ_RFRNC_NMBR = "APVOU" +
     * invPropagatedCosting.getApVoucherLineItem().getApVoucher().getVouDocumentNumber();
     * 
     * } else if (invPropagatedCosting.getApVoucherLineItem().getApCheck() != null) {
     * 
     * ADJ_DSCRPTN = invPropagatedCosting.getApVoucherLineItem().getApCheck().getChkDescription();
     * ADJ_CRTD_BY = invPropagatedCosting.getApVoucherLineItem().getApCheck().getChkPostedBy();
     * ADJ_RFRNC_NMBR = "APCHK" +
     * invPropagatedCosting.getApVoucherLineItem().getApCheck().getChkDocumentNumber();
     * 
     * }
     * 
     * } else if (invPropagatedCosting.getArInvoiceLineItem() != null){
     * 
     * QNTY = this.convertByUomFromAndItemAndQuantity(
     * invPropagatedCosting.getArInvoiceLineItem().getInvUnitOfMeasure(),
     * invPropagatedCosting.getArInvoiceLineItem().getInvItemLocation().getInvItem(),
     * invPropagatedCosting.getArInvoiceLineItem().getIliQuantity(), AD_CMPNY); TTL_CST =
     * prevInvCosting.getCstRemainingValue() - invPropagatedCosting.getCstRemainingValue();
     * Debug.print("ArMiscReceiptEntryControllerBean regenerateCostVariance D");
     * if(invPropagatedCosting.getArInvoiceLineItem().getArInvoice() != null){
     * 
     * ADJ_DSCRPTN = invPropagatedCosting.getArInvoiceLineItem().getArInvoice().getInvDescription();
     * ADJ_CRTD_BY = invPropagatedCosting.getArInvoiceLineItem().getArInvoice().getInvPostedBy();
     * ADJ_RFRNC_NMBR = "ARCM" +
     * invPropagatedCosting.getArInvoiceLineItem().getArInvoice().getInvNumber();
     * 
     * } else if(invPropagatedCosting.getArInvoiceLineItem().getArReceipt() != null){
     * 
     * ADJ_DSCRPTN = invPropagatedCosting.getArInvoiceLineItem().getArReceipt().getRctDescription();
     * ADJ_CRTD_BY = invPropagatedCosting.getArInvoiceLineItem().getArReceipt().getRctPostedBy();
     * ADJ_RFRNC_NMBR = "ARMR" +
     * invPropagatedCosting.getArInvoiceLineItem().getArReceipt().getRctNumber();
     * 
     * }
     * 
     * } else if (invPropagatedCosting.getArSalesOrderInvoiceLine() != null){
     * 
     * TTL_CST = prevInvCosting.getCstRemainingValue() -
     * invPropagatedCosting.getCstRemainingValue(); QNTY = this.convertByUomFromAndItemAndQuantity(
     * invPropagatedCosting.getArSalesOrderInvoiceLine().getArSalesOrderLine().getInvUnitOfMeasure()
     * ,
     * invPropagatedCosting.getArSalesOrderInvoiceLine().getArSalesOrderLine().getInvItemLocation().
     * getInvItem(), invPropagatedCosting.getArSalesOrderInvoiceLine().getSilQuantityDelivered(),
     * AD_CMPNY); Debug.print("ArMiscReceiptEntryControllerBean regenerateCostVariance E");
     * ADJ_DSCRPTN =
     * invPropagatedCosting.getArSalesOrderInvoiceLine().getArInvoice().getInvDescription();
     * ADJ_CRTD_BY =
     * invPropagatedCosting.getArSalesOrderInvoiceLine().getArInvoice().getInvPostedBy();
     * ADJ_RFRNC_NMBR = "ARCM" +
     * invPropagatedCosting.getArSalesOrderInvoiceLine().getArInvoice().getInvNumber();
     * 
     * } else if (invPropagatedCosting.getInvAdjustmentLine() != null){
     * 
     * ADJ_DSCRPTN =
     * invPropagatedCosting.getInvAdjustmentLine().getInvAdjustment().getAdjDescription();
     * ADJ_CRTD_BY =
     * invPropagatedCosting.getInvAdjustmentLine().getInvAdjustment().getAdjPostedBy();
     * ADJ_RFRNC_NMBR = "INVADJ" +
     * invPropagatedCosting.getInvAdjustmentLine().getInvAdjustment().getAdjDocumentNumber();
     * 
     * if(invPropagatedCosting.getInvAdjustmentLine().getAlAdjustQuantity() != 0) {
     * 
     * TTL_CST = (invPropagatedCosting.getInvAdjustmentLine().getAlUnitCost() *
     * invPropagatedCosting.getInvAdjustmentLine().getAlAdjustQuantity()); QNTY =
     * this.convertByUomFromAndItemAndQuantity(
     * invPropagatedCosting.getInvAdjustmentLine().getInvUnitOfMeasure(),
     * invPropagatedCosting.getInvAdjustmentLine().getInvItemLocation().getInvItem(),
     * invPropagatedCosting.getInvAdjustmentLine().getAlAdjustQuantity(), AD_CMPNY);
     * Debug.print("ArMiscReceiptEntryControllerBean regenerateCostVariance F"); }
     * 
     * } else if (invPropagatedCosting.getInvAssemblyTransferLine() != null){
     * 
     * QNTY = invPropagatedCosting.getInvAssemblyTransferLine().getAtlAssembleQuantity(); TTL_CST =
     * invPropagatedCosting.getInvAssemblyTransferLine().getAtlAssembleCost();
     * 
     * ADJ_DSCRPTN =
     * invPropagatedCosting.getInvAssemblyTransferLine().getInvAssemblyTransfer().getAtrDescription(
     * ); ADJ_CRTD_BY =
     * invPropagatedCosting.getInvAssemblyTransferLine().getInvAssemblyTransfer().getAtrPostedBy();
     * ADJ_RFRNC_NMBR = "INVAT" +
     * invPropagatedCosting.getInvAssemblyTransferLine().getInvAssemblyTransfer().
     * getAtrDocumentNumber();
     * 
     * } else if (invPropagatedCosting.getInvBranchStockTransferLine() != null){
     * 
     * if(invPropagatedCosting.getInvBranchStockTransferLine().getInvBranchStockTransfer().
     * getBstTransferOutNumber() != null) {
     * 
     * TTL_CST = invPropagatedCosting.getInvBranchStockTransferLine().getBslAmount(); QNTY =
     * this.convertByUomFromAndItemAndQuantity(
     * invPropagatedCosting.getInvBranchStockTransferLine().getInvUnitOfMeasure(),
     * invPropagatedCosting.getInvBranchStockTransferLine().getInvItemLocation().getInvItem(),
     * invPropagatedCosting.getInvBranchStockTransferLine().getBslQuantityReceived(), AD_CMPNY);
     * Debug.print("ArMiscReceiptEntryControllerBean regenerateCostVariance G"); } else {
     * 
     * TTL_CST = invPropagatedCosting.getInvBranchStockTransferLine().getBslAmount(); QNTY =
     * this.convertByUomFromAndItemAndQuantity(
     * invPropagatedCosting.getInvBranchStockTransferLine().getInvUnitOfMeasure(),
     * invPropagatedCosting.getInvBranchStockTransferLine().getInvItemLocation().getInvItem(),
     * invPropagatedCosting.getInvBranchStockTransferLine().getBslQuantity(), AD_CMPNY);
     * Debug.print("ArMiscReceiptEntryControllerBean regenerateCostVariance H"); }
     * 
     * ADJ_DSCRPTN =
     * invPropagatedCosting.getInvBranchStockTransferLine().getInvBranchStockTransfer().
     * getBstDescription(); ADJ_CRTD_BY =
     * invPropagatedCosting.getInvBranchStockTransferLine().getInvBranchStockTransfer().
     * getBstPostedBy(); ADJ_RFRNC_NMBR = "INVBST" +
     * invPropagatedCosting.getInvBranchStockTransferLine().getInvBranchStockTransfer().getBstNumber
     * ();
     * 
     * } else if (invPropagatedCosting.getInvBuildUnbuildAssemblyLine() != null){
     * 
     * TTL_CST = prevInvCosting.getCstRemainingValue() -
     * invPropagatedCosting.getCstRemainingValue(); QNTY =
     * invPropagatedCosting.getInvBuildUnbuildAssemblyLine().getBlBuildQuantity(); ADJ_DSCRPTN =
     * invPropagatedCosting.getInvBuildUnbuildAssemblyLine().getInvBuildUnbuildAssembly().
     * getBuaDescription(); ADJ_CRTD_BY =
     * invPropagatedCosting.getInvBuildUnbuildAssemblyLine().getInvBuildUnbuildAssembly().
     * getBuaPostedBy(); ADJ_RFRNC_NMBR = "INVBUA" +
     * invPropagatedCosting.getInvBuildUnbuildAssemblyLine().getInvBuildUnbuildAssembly().
     * getBuaDocumentNumber();
     * 
     * } else if (invPropagatedCosting.getInvStockIssuanceLine()!= null){
     * 
     * TTL_CST = invPropagatedCosting.getInvStockIssuanceLine().getSilIssueCost(); QNTY =
     * this.convertByUomFromAndItemAndQuantity(
     * invPropagatedCosting.getInvStockIssuanceLine().getInvUnitOfMeasure(),
     * invPropagatedCosting.getInvStockIssuanceLine().getInvItemLocation().getInvItem(),
     * invPropagatedCosting.getInvStockIssuanceLine().getSilIssueQuantity(), AD_CMPNY);
     * Debug.print("ArMiscReceiptEntryControllerBean regenerateCostVariance I"); ADJ_DSCRPTN =
     * invPropagatedCosting.getInvStockIssuanceLine().getInvStockIssuance().getSiDescription();
     * ADJ_CRTD_BY =
     * invPropagatedCosting.getInvStockIssuanceLine().getInvStockIssuance().getSiPostedBy();
     * ADJ_RFRNC_NMBR = "INVSI" +
     * invPropagatedCosting.getInvStockIssuanceLine().getInvStockIssuance().getSiDocumentNumber();
     * 
     * } else if (invPropagatedCosting.getInvStockTransferLine()!= null) {
     * 
     * TTL_CST = invPropagatedCosting.getInvStockTransferLine().getStlAmount(); QNTY =
     * this.convertByUomFromAndItemAndQuantity(
     * invPropagatedCosting.getInvStockTransferLine().getInvUnitOfMeasure(),
     * invPropagatedCosting.getInvStockTransferLine().getInvItem(),
     * invPropagatedCosting.getInvStockTransferLine().getStlQuantityDelivered(), AD_CMPNY);
     * Debug.print("ArMiscReceiptEntryControllerBean regenerateCostVariance J"); ADJ_DSCRPTN =
     * invPropagatedCosting.getInvStockTransferLine().getInvStockTransfer().getStDescription();
     * ADJ_CRTD_BY =
     * invPropagatedCosting.getInvStockTransferLine().getInvStockTransfer().getStPostedBy();
     * ADJ_RFRNC_NMBR = "INVST" +
     * invPropagatedCosting.getInvStockTransferLine().getInvStockTransfer().getStDocumentNumber();
     * 
     * } else {
     * 
     * prevInvCosting = invPropagatedCosting; continue;
     * 
     * }
     * 
     * // if quantity is equal 0, no variance. if(QNTY == 0) continue;
     * 
     * // compute new cost variance double UNT_CST = TTL_CST/QNTY; double CST_VRNC_VL =
     * (invPropagatedCosting.getCstRemainingQuantity() * UNT_CST -
     * invPropagatedCosting.getCstRemainingValue());
     * 
     * if(CST_VRNC_VL != 0) this.generateCostVariance(invPropagatedCosting.getInvItemLocation(),
     * CST_VRNC_VL, ADJ_RFRNC_NMBR, ADJ_DSCRPTN, invPropagatedCosting.getCstDate(), ADJ_CRTD_BY,
     * AD_BRNCH, AD_CMPNY);
     * 
     * }
     * 
     * // set previous costing prevInvCosting = invPropagatedCosting;
     * 
     * }
     * 
     * } catch (AdPRFCoaGlVarianceAccountNotFoundException ex){
     * 
     * throw ex;
     * 
     * } catch (Exception ex) {
     * 
     * Debug.printStackTrace(ex); getSessionContext().setRollbackOnly(); throw new
     * EJBException(ex.getMessage());
     * 
     * }
     */
  }

  private void addInvDrEntry(
      short DR_LN, String DR_CLSS, byte DR_DBT, double DR_AMNT, byte DR_RVRSL, Integer COA_CODE,
      LocalInvAdjustment invAdjustment, Integer AD_BRNCH, Integer AD_CMPNY
  )

      throws GlobalBranchAccountNumberInvalidException {

    Debug.print("ArMiscReceiptEntryControllerBean addInvDrEntry");

    LocalAdCompanyHome adCompanyHome = null;
    LocalInvDistributionRecordHome invDistributionRecordHome = null;
    LocalGlChartOfAccountHome glChartOfAccountHome = null;

    // Initialize EJB Home

    try {

      adCompanyHome = (LocalAdCompanyHome) EJBHomeFactory
          .lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
      invDistributionRecordHome = (LocalInvDistributionRecordHome) EJBHomeFactory.lookUpLocalHome(
          LocalInvDistributionRecordHome.JNDI_NAME, LocalInvDistributionRecordHome.class);
      glChartOfAccountHome = (LocalGlChartOfAccountHome) EJBHomeFactory
          .lookUpLocalHome(LocalGlChartOfAccountHome.JNDI_NAME, LocalGlChartOfAccountHome.class);


    } catch (NamingException ex) {

      throw new EJBException(ex.getMessage());

    }

    try {

      // get company

      LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);

      // validate coa

      LocalGlChartOfAccount glChartOfAccount = null;

      try {

        glChartOfAccount =
            glChartOfAccountHome.findByCoaCodeAndBranchCode(COA_CODE, AD_BRNCH, AD_CMPNY);

      } catch (FinderException ex) {

        throw new GlobalBranchAccountNumberInvalidException();

      }

      // create distribution record

      LocalInvDistributionRecord invDistributionRecord =
          invDistributionRecordHome.create(DR_LN, DR_CLSS, DR_DBT,
              EJBCommon.roundIt(DR_AMNT, adCompany.getGlFunctionalCurrency().getFcPrecision()),
              DR_RVRSL, EJBCommon.FALSE, AD_CMPNY);

      // invAdjustment.addInvDistributionRecord(invDistributionRecord);
      invDistributionRecord.setInvAdjustment(invAdjustment);
      // glChartOfAccount.addInvDistributionRecord(invDistributionRecord);
      invDistributionRecord.setInvChartOfAccount(glChartOfAccount);


    } catch (GlobalBranchAccountNumberInvalidException ex) {

      throw new GlobalBranchAccountNumberInvalidException();

    } catch (Exception ex) {

      Debug.printStackTrace(ex);
      getSessionContext().setRollbackOnly();
      throw new EJBException(ex.getMessage());

    }

  }

  private void executeInvAdjPost(
      Integer ADJ_CODE, String USR_NM, Integer AD_BRNCH, Integer AD_CMPNY
  ) throws GlobalRecordAlreadyDeletedException, GlobalTransactionAlreadyPostedException,
      GlJREffectiveDateNoPeriodExistException, GlJREffectiveDatePeriodClosedException,
      GlobalJournalNotBalanceException, GlobalBranchAccountNumberInvalidException {

    Debug.print("ArMiscReceiptEntryControllerBean executeInvAdjPost");

    LocalInvAdjustmentHome invAdjustmentHome = null;
    LocalAdCompanyHome adCompanyHome = null;
    LocalAdPreferenceHome adPreferenceHome = null;
    LocalGlSetOfBookHome glSetOfBookHome = null;
    LocalGlAccountingCalendarValueHome glAccountingCalendarValueHome = null;
    LocalGlJournalHome glJournalHome = null;
    LocalGlJournalBatchHome glJournalBatchHome = null;
    LocalGlSuspenseAccountHome glSuspenseAccountHome = null;
    LocalGlJournalLineHome glJournalLineHome = null;
    LocalGlJournalSourceHome glJournalSourceHome = null;
    LocalGlJournalCategoryHome glJournalCategoryHome = null;
    LocalGlFunctionalCurrencyHome glFunctionalCurrencyHome = null;
    LocalInvDistributionRecordHome invDistributionRecordHome = null;
    LocalInvCostingHome invCostingHome = null;
    LocalGlChartOfAccountHome glChartOfAccountHome = null;
    LocalInvAdjustmentLineHome invAdjustmentLineHome = null;


    // Initialize EJB Home

    try {

      invAdjustmentHome = (LocalInvAdjustmentHome) EJBHomeFactory
          .lookUpLocalHome(LocalInvAdjustmentHome.JNDI_NAME, LocalInvAdjustmentHome.class);
      adCompanyHome = (LocalAdCompanyHome) EJBHomeFactory
          .lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
      adPreferenceHome = (LocalAdPreferenceHome) EJBHomeFactory
          .lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);
      glSetOfBookHome = (LocalGlSetOfBookHome) EJBHomeFactory
          .lookUpLocalHome(LocalGlSetOfBookHome.JNDI_NAME, LocalGlSetOfBookHome.class);
      glAccountingCalendarValueHome = (LocalGlAccountingCalendarValueHome) EJBHomeFactory
          .lookUpLocalHome(LocalGlAccountingCalendarValueHome.JNDI_NAME,
              LocalGlAccountingCalendarValueHome.class);
      glJournalHome = (LocalGlJournalHome) EJBHomeFactory
          .lookUpLocalHome(LocalGlJournalHome.JNDI_NAME, LocalGlJournalHome.class);
      glJournalBatchHome = (LocalGlJournalBatchHome) EJBHomeFactory
          .lookUpLocalHome(LocalGlJournalBatchHome.JNDI_NAME, LocalGlJournalBatchHome.class);
      glSuspenseAccountHome = (LocalGlSuspenseAccountHome) EJBHomeFactory
          .lookUpLocalHome(LocalGlSuspenseAccountHome.JNDI_NAME, LocalGlSuspenseAccountHome.class);
      glJournalLineHome = (LocalGlJournalLineHome) EJBHomeFactory
          .lookUpLocalHome(LocalGlJournalLineHome.JNDI_NAME, LocalGlJournalLineHome.class);
      glJournalSourceHome = (LocalGlJournalSourceHome) EJBHomeFactory
          .lookUpLocalHome(LocalGlJournalSourceHome.JNDI_NAME, LocalGlJournalSourceHome.class);
      glJournalCategoryHome = (LocalGlJournalCategoryHome) EJBHomeFactory
          .lookUpLocalHome(LocalGlJournalCategoryHome.JNDI_NAME, LocalGlJournalCategoryHome.class);
      glFunctionalCurrencyHome = (LocalGlFunctionalCurrencyHome) EJBHomeFactory.lookUpLocalHome(
          LocalGlFunctionalCurrencyHome.JNDI_NAME, LocalGlFunctionalCurrencyHome.class);
      invDistributionRecordHome = (LocalInvDistributionRecordHome) EJBHomeFactory.lookUpLocalHome(
          LocalInvDistributionRecordHome.JNDI_NAME, LocalInvDistributionRecordHome.class);
      invCostingHome = (LocalInvCostingHome) EJBHomeFactory
          .lookUpLocalHome(LocalInvCostingHome.JNDI_NAME, LocalInvCostingHome.class);
      glChartOfAccountHome = (LocalGlChartOfAccountHome) EJBHomeFactory
          .lookUpLocalHome(LocalGlChartOfAccountHome.JNDI_NAME, LocalGlChartOfAccountHome.class);
      invAdjustmentLineHome = (LocalInvAdjustmentLineHome) EJBHomeFactory
          .lookUpLocalHome(LocalInvAdjustmentLineHome.JNDI_NAME, LocalInvAdjustmentLineHome.class);

    } catch (NamingException ex) {

      throw new EJBException(ex.getMessage());

    }

    try {

      // validate if adjustment is already deleted

      LocalInvAdjustment invAdjustment = null;

      try {

        invAdjustment = invAdjustmentHome.findByPrimaryKey(ADJ_CODE);

      } catch (FinderException ex) {

        throw new GlobalRecordAlreadyDeletedException();

      }

      // validate if adjustment is already posted or void

      if (invAdjustment.getAdjPosted() == EJBCommon.TRUE) {

        if (invAdjustment.getAdjVoid() != EJBCommon.TRUE)
          throw new GlobalTransactionAlreadyPostedException();

      }

      Collection invAdjustmentLines = null;

      if (invAdjustment.getAdjVoid() == EJBCommon.FALSE)
        invAdjustmentLines = invAdjustmentLineHome.findByAlVoidAndAdjCode(EJBCommon.FALSE,
            invAdjustment.getAdjCode(), AD_CMPNY);
      else
        invAdjustmentLines = invAdjustmentLineHome.findByAlVoidAndAdjCode(EJBCommon.TRUE,
            invAdjustment.getAdjCode(), AD_CMPNY);

      Iterator i = invAdjustmentLines.iterator();

      while (i.hasNext()) {


        LocalInvAdjustmentLine invAdjustmentLine = (LocalInvAdjustmentLine) i.next();

        LocalInvCosting invCosting = invCostingHome
            .getByMaxCstDateToLongAndMaxCstLineNumberAndLessThanEqualCstDateAndIiNameAndLocName(
                invAdjustment.getAdjDate(),
                invAdjustmentLine.getInvItemLocation().getInvItem().getIiName(),
                invAdjustmentLine.getInvItemLocation().getInvLocation().getLocName(), AD_BRNCH,
                AD_CMPNY);

        this.postInvAdjustmentToInventory(invAdjustmentLine, invAdjustment.getAdjDate(), 0,
            invAdjustmentLine.getAlUnitCost(), invCosting.getCstRemainingQuantity(),
            invCosting.getCstRemainingValue() + invAdjustmentLine.getAlUnitCost(), AD_BRNCH,
            AD_CMPNY);

      }

      // post to gl if necessary

      LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);
      LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);

      // validate if date has no period and period is closed

      LocalGlSetOfBook glJournalSetOfBook = null;

      try {

        glJournalSetOfBook = glSetOfBookHome.findByDate(invAdjustment.getAdjDate(), AD_CMPNY);

      } catch (FinderException ex) {

        throw new GlJREffectiveDateNoPeriodExistException();

      }

      LocalGlAccountingCalendarValue glAccountingCalendarValue = glAccountingCalendarValueHome
          .findByAcCodeAndDate(glJournalSetOfBook.getGlAccountingCalendar().getAcCode(),
              invAdjustment.getAdjDate(), AD_CMPNY);


      if (glAccountingCalendarValue.getAcvStatus() == 'N'
          || glAccountingCalendarValue.getAcvStatus() == 'C'
          || glAccountingCalendarValue.getAcvStatus() == 'P') {

        throw new GlJREffectiveDatePeriodClosedException();

      }

      // check if invoice is balance if not check suspense posting

      LocalGlJournalLine glOffsetJournalLine = null;

      Collection invDistributionRecords = null;

      if (invAdjustment.getAdjVoid() == EJBCommon.FALSE) {

        invDistributionRecords = invDistributionRecordHome.findImportableDrByDrReversedAndAdjCode(
            EJBCommon.FALSE, invAdjustment.getAdjCode(), AD_CMPNY);

      } else {

        invDistributionRecords = invDistributionRecordHome.findImportableDrByDrReversedAndAdjCode(
            EJBCommon.TRUE, invAdjustment.getAdjCode(), AD_CMPNY);

      }

      Iterator j = invDistributionRecords.iterator();

      double TOTAL_DEBIT = 0d;
      double TOTAL_CREDIT = 0d;

      while (j.hasNext()) {

        LocalInvDistributionRecord invDistributionRecord = (LocalInvDistributionRecord) j.next();

        double DR_AMNT = 0d;

        DR_AMNT = invDistributionRecord.getDrAmount();

        if (invDistributionRecord.getDrDebit() == EJBCommon.TRUE) {

          TOTAL_DEBIT += DR_AMNT;

        } else {

          TOTAL_CREDIT += DR_AMNT;

        }

      }

      TOTAL_DEBIT =
          EJBCommon.roundIt(TOTAL_DEBIT, adCompany.getGlFunctionalCurrency().getFcPrecision());
      TOTAL_CREDIT =
          EJBCommon.roundIt(TOTAL_CREDIT, adCompany.getGlFunctionalCurrency().getFcPrecision());

      if (adPreference.getPrfAllowSuspensePosting() == EJBCommon.TRUE
          && TOTAL_DEBIT != TOTAL_CREDIT) {

        LocalGlSuspenseAccount glSuspenseAccount = null;

        try {

          glSuspenseAccount = glSuspenseAccountHome.findByJsNameAndJcName("INVENTORY",
              "INVENTORY ADJUSTMENTS", AD_CMPNY);

        } catch (FinderException ex) {

          throw new GlobalJournalNotBalanceException();

        }

        if (TOTAL_DEBIT - TOTAL_CREDIT < 0) {

          glOffsetJournalLine =
              glJournalLineHome.create((short) (invDistributionRecords.size() + 1), EJBCommon.TRUE,
                  TOTAL_CREDIT - TOTAL_DEBIT, "", AD_CMPNY);

        } else {

          glOffsetJournalLine =
              glJournalLineHome.create((short) (invDistributionRecords.size() + 1), EJBCommon.FALSE,
                  TOTAL_DEBIT - TOTAL_CREDIT, "", AD_CMPNY);

        }

        LocalGlChartOfAccount glChartOfAccount = glSuspenseAccount.getGlChartOfAccount();
        // glChartOfAccount.addGlJournalLine(glOffsetJournalLine);
        glOffsetJournalLine.setGlChartOfAccount(glChartOfAccount);

      } else if (adPreference.getPrfAllowSuspensePosting() == EJBCommon.FALSE
          && TOTAL_DEBIT != TOTAL_CREDIT) {

        throw new GlobalJournalNotBalanceException();

      }

      // create journal batch if necessary

      LocalGlJournalBatch glJournalBatch = null;
      java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat("MM/dd/yyyy");

      try {

        glJournalBatch = glJournalBatchHome.findByJbName(
            "JOURNAL IMPORT " + formatter.format(new Date()) + " INVENTORY ADJUSTMENTS", AD_BRNCH,
            AD_CMPNY);

      } catch (FinderException ex) {

      }

      if (glJournalBatch == null) {

        glJournalBatch = glJournalBatchHome.create(
            "JOURNAL IMPORT " + formatter.format(new Date()) + " INVENTORY ADJUSTMENTS",
            "JOURNAL IMPORT", "CLOSED", EJBCommon.getGcCurrentDateWoTime().getTime(), USR_NM,
            AD_BRNCH, AD_CMPNY);

      }

      // create journal entry

      LocalGlJournal glJournal = glJournalHome.create(invAdjustment.getAdjReferenceNumber(),
          invAdjustment.getAdjDescription(), invAdjustment.getAdjDate(), 0.0d, null,
          invAdjustment.getAdjDocumentNumber(), null, 1d, "N/A", null, 'N', EJBCommon.TRUE,
          EJBCommon.FALSE, USR_NM, new Date(), USR_NM, new Date(), null, null, USR_NM,
          EJBCommon.getGcCurrentDateWoTime().getTime(), null, null, EJBCommon.FALSE, null, AD_BRNCH,
          AD_CMPNY);

      LocalGlJournalSource glJournalSource =
          glJournalSourceHome.findByJsName("INVENTORY", AD_CMPNY);
      glJournal.setGlJournalSource(glJournalSource);

      LocalGlFunctionalCurrency glFunctionalCurrency = glFunctionalCurrencyHome
          .findByFcName(adCompany.getGlFunctionalCurrency().getFcName(), AD_CMPNY);
      glJournal.setGlFunctionalCurrency(glFunctionalCurrency);

      LocalGlJournalCategory glJournalCategory =
          glJournalCategoryHome.findByJcName("INVENTORY ADJUSTMENTS", AD_CMPNY);
      glJournal.setGlJournalCategory(glJournalCategory);

      if (glJournalBatch != null) {

        glJournal.setGlJournalBatch(glJournalBatch);
      }

      // create journal lines

      j = invDistributionRecords.iterator();

      while (j.hasNext()) {

        LocalInvDistributionRecord invDistributionRecord = (LocalInvDistributionRecord) j.next();

        double DR_AMNT = 0d;

        DR_AMNT = invDistributionRecord.getDrAmount();

        LocalGlJournalLine glJournalLine =
            glJournalLineHome.create(invDistributionRecord.getDrLine(),
                invDistributionRecord.getDrDebit(), DR_AMNT, "", AD_CMPNY);

        glJournalLine.setGlChartOfAccount(invDistributionRecord.getInvChartOfAccount());

        glJournalLine.setGlJournal(glJournal);

        invDistributionRecord.setDrImported(EJBCommon.TRUE);


      }

      if (glOffsetJournalLine != null) {

        glOffsetJournalLine.setGlJournal(glJournal);

      }

      // post journal to gl

      Collection glJournalLines = glJournal.getGlJournalLines();

      i = glJournalLines.iterator();

      while (i.hasNext()) {

        LocalGlJournalLine glJournalLine = (LocalGlJournalLine) i.next();

        // post current to current acv

        this.postToGl(glAccountingCalendarValue, glJournalLine.getGlChartOfAccount(), true,
            glJournalLine.getJlDebit(), glJournalLine.getJlAmount(), AD_CMPNY);


        // post to subsequent acvs (propagate)

        Collection glSubsequentAccountingCalendarValues =
            glAccountingCalendarValueHome.findSubsequentAcvByAcCodeAndAcvPeriodNumber(
                glJournalSetOfBook.getGlAccountingCalendar().getAcCode(),
                glAccountingCalendarValue.getAcvPeriodNumber(), AD_CMPNY);

        Iterator acvsIter = glSubsequentAccountingCalendarValues.iterator();

        while (acvsIter.hasNext()) {

          LocalGlAccountingCalendarValue glSubsequentAccountingCalendarValue =
              (LocalGlAccountingCalendarValue) acvsIter.next();

          this.postToGl(glSubsequentAccountingCalendarValue, glJournalLine.getGlChartOfAccount(),
              false, glJournalLine.getJlDebit(), glJournalLine.getJlAmount(), AD_CMPNY);

        }

        // post to subsequent years if necessary

        Collection glSubsequentSetOfBooks = glSetOfBookHome.findSubsequentSobByAcYear(
            glJournalSetOfBook.getGlAccountingCalendar().getAcYear(), AD_CMPNY);

        if (!glSubsequentSetOfBooks.isEmpty() && glJournalSetOfBook.getSobYearEndClosed() == 1) {

          adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
          LocalGlChartOfAccount glRetainedEarningsAccount =
              glChartOfAccountHome.findByCoaAccountNumberAndBranchCode(
                  adCompany.getCmpRetainedEarnings(), AD_BRNCH, AD_CMPNY);

          Iterator sobIter = glSubsequentSetOfBooks.iterator();

          while (sobIter.hasNext()) {

            LocalGlSetOfBook glSubsequentSetOfBook = (LocalGlSetOfBook) sobIter.next();

            String ACCOUNT_TYPE = glJournalLine.getGlChartOfAccount().getCoaAccountType();

            // post to subsequent acvs of subsequent set of book(propagate)

            Collection glAccountingCalendarValues = glAccountingCalendarValueHome.findByAcCode(
                glSubsequentSetOfBook.getGlAccountingCalendar().getAcCode(), AD_CMPNY);

            Iterator acvIter = glAccountingCalendarValues.iterator();

            while (acvIter.hasNext()) {

              LocalGlAccountingCalendarValue glSubsequentAccountingCalendarValue =
                  (LocalGlAccountingCalendarValue) acvIter.next();

              if (ACCOUNT_TYPE.equals("ASSET") || ACCOUNT_TYPE.equals("LIABILITY")
                  || ACCOUNT_TYPE.equals("OWNERS EQUITY")) {

                this.postToGl(glSubsequentAccountingCalendarValue,
                    glJournalLine.getGlChartOfAccount(), false, glJournalLine.getJlDebit(),
                    glJournalLine.getJlAmount(), AD_CMPNY);

              } else { // revenue & expense

                this.postToGl(glSubsequentAccountingCalendarValue, glRetainedEarningsAccount, false,
                    glJournalLine.getJlDebit(), glJournalLine.getJlAmount(), AD_CMPNY);

              }

            }

            if (glSubsequentSetOfBook.getSobYearEndClosed() == 0)
              break;

          }

        }

      }

      invAdjustment.setAdjPosted(EJBCommon.TRUE);

    } catch (GlJREffectiveDateNoPeriodExistException ex) {

      getSessionContext().setRollbackOnly();
      throw ex;

    } catch (GlJREffectiveDatePeriodClosedException ex) {

      getSessionContext().setRollbackOnly();
      throw ex;

    } catch (GlobalJournalNotBalanceException ex) {

      getSessionContext().setRollbackOnly();
      throw ex;

    } catch (GlobalRecordAlreadyDeletedException ex) {

      getSessionContext().setRollbackOnly();
      throw ex;

    } catch (GlobalTransactionAlreadyPostedException ex) {

      getSessionContext().setRollbackOnly();
      throw ex;

    } catch (Exception ex) {

      Debug.printStackTrace(ex);
      getSessionContext().setRollbackOnly();
      throw new EJBException(ex.getMessage());

    }

  }

  private LocalInvAdjustmentLine addInvAlEntry(
      LocalInvItemLocation invItemLocation, LocalInvAdjustment invAdjustment, double CST_VRNC_VL,
      byte AL_VD, Integer AD_CMPNY
  ) {

    Debug.print("ArMiscReceiptEntryControllerBean addInvAlEntry");

    LocalInvAdjustmentLineHome invAdjustmentLineHome = null;

    // Initialize EJB Home

    try {

      invAdjustmentLineHome = (LocalInvAdjustmentLineHome) EJBHomeFactory
          .lookUpLocalHome(LocalInvAdjustmentLineHome.JNDI_NAME, LocalInvAdjustmentLineHome.class);

    } catch (NamingException ex) {

      throw new EJBException(ex.getMessage());

    }

    try {

      // create dr entry
      LocalInvAdjustmentLine invAdjustmentLine = null;
      invAdjustmentLine =
          invAdjustmentLineHome.create(CST_VRNC_VL, null, null, 0, 0, AL_VD, AD_CMPNY);

      // map adjustment, unit of measure, item location
      // invAdjustment.addInvAdjustmentLine(invAdjustmentLine);
      invAdjustmentLine.setInvAdjustment(invAdjustment);
      // invItemLocation.getInvItem().getInvUnitOfMeasure().addInvAdjustmentLine(invAdjustmentLine);
      invAdjustmentLine.setInvUnitOfMeasure(invItemLocation.getInvItem().getInvUnitOfMeasure());
      // invItemLocation.addInvAdjustmentLine(invAdjustmentLine);
      invAdjustmentLine.setInvItemLocation(invItemLocation);

      return invAdjustmentLine;

    } catch (Exception ex) {

      Debug.printStackTrace(ex);
      getSessionContext().setRollbackOnly();
      throw new EJBException(ex.getMessage());

    }

  }

  private LocalInvAdjustment saveInvAdjustment(
      String ADJ_RFRNC_NMBR, String ADJ_DSCRPTN, Date ADJ_DATE, String USR_NM, Integer AD_BRNCH,
      Integer AD_CMPNY
  ) {

    Debug.print("ArMiscReceiptEntryControllerBean saveInvAdjustment");

    LocalInvAdjustmentHome invAdjustmentHome = null;
    LocalAdDocumentSequenceAssignmentHome adDocumentSequenceAssignmentHome = null;
    LocalAdBranchDocumentSequenceAssignmentHome adBranchDocumentSequenceAssignmentHome = null;
    // Initialize EJB Home

    try {

      invAdjustmentHome = (LocalInvAdjustmentHome) EJBHomeFactory
          .lookUpLocalHome(LocalInvAdjustmentHome.JNDI_NAME, LocalInvAdjustmentHome.class);
      adDocumentSequenceAssignmentHome = (LocalAdDocumentSequenceAssignmentHome) EJBHomeFactory
          .lookUpLocalHome(LocalAdDocumentSequenceAssignmentHome.JNDI_NAME,
              LocalAdDocumentSequenceAssignmentHome.class);
      adBranchDocumentSequenceAssignmentHome =
          (LocalAdBranchDocumentSequenceAssignmentHome) EJBHomeFactory.lookUpLocalHome(
              LocalAdBranchDocumentSequenceAssignmentHome.JNDI_NAME,
              LocalAdBranchDocumentSequenceAssignmentHome.class);

    } catch (NamingException ex) {

      throw new EJBException(ex.getMessage());

    }

    try {

      // generate adj document number
      String ADJ_DCMNT_NMBR = null;

      LocalAdBranchDocumentSequenceAssignment adBranchDocumentSequenceAssignment = null;
      LocalAdDocumentSequenceAssignment adDocumentSequenceAssignment = null;

      try {

        adDocumentSequenceAssignment =
            adDocumentSequenceAssignmentHome.findByDcName("INV ADJUSTMENT", AD_CMPNY);

      } catch (FinderException ex) {

      }

      try {

        adBranchDocumentSequenceAssignment =
            adBranchDocumentSequenceAssignmentHome.findBdsByDsaCodeAndBrCode(
                adDocumentSequenceAssignment.getDsaCode(), AD_BRNCH, AD_CMPNY);

      } catch (FinderException ex) {

      }

      while (true) {

        if (adBranchDocumentSequenceAssignment == null
            || adBranchDocumentSequenceAssignment.getBdsNextSequence() == null) {

          try {

            invAdjustmentHome.findByAdjDocumentNumberAndBrCode(
                adDocumentSequenceAssignment.getDsaNextSequence(), AD_BRNCH, AD_CMPNY);
            adDocumentSequenceAssignment.setDsaNextSequence(
                EJBCommon.incrementStringNumber(adDocumentSequenceAssignment.getDsaNextSequence()));

          } catch (FinderException ex) {

            ADJ_DCMNT_NMBR = adDocumentSequenceAssignment.getDsaNextSequence();
            adDocumentSequenceAssignment.setDsaNextSequence(
                EJBCommon.incrementStringNumber(adDocumentSequenceAssignment.getDsaNextSequence()));
            break;

          }

        } else {

          try {

            invAdjustmentHome.findByAdjDocumentNumberAndBrCode(
                adBranchDocumentSequenceAssignment.getBdsNextSequence(), AD_BRNCH, AD_CMPNY);
            adBranchDocumentSequenceAssignment.setBdsNextSequence(EJBCommon
                .incrementStringNumber(adBranchDocumentSequenceAssignment.getBdsNextSequence()));

          } catch (FinderException ex) {

            ADJ_DCMNT_NMBR = adBranchDocumentSequenceAssignment.getBdsNextSequence();
            adBranchDocumentSequenceAssignment.setBdsNextSequence(EJBCommon
                .incrementStringNumber(adBranchDocumentSequenceAssignment.getBdsNextSequence()));
            break;

          }

        }

      }

      LocalInvAdjustment invAdjustment = invAdjustmentHome.create(ADJ_DCMNT_NMBR, ADJ_RFRNC_NMBR,
          ADJ_DSCRPTN, ADJ_DATE, "COST-VARIANCE", "N/A", EJBCommon.FALSE, USR_NM, ADJ_DATE, USR_NM,
          ADJ_DATE, null, null, USR_NM, ADJ_DATE, null, null, EJBCommon.TRUE, EJBCommon.FALSE,
          AD_BRNCH, AD_CMPNY);

      return invAdjustment;

    } catch (Exception ex) {

      Debug.printStackTrace(ex);
      getSessionContext().setRollbackOnly();
      throw new EJBException(ex.getMessage());

    }


  }

  private void postInvAdjustmentToInventory(
      LocalInvAdjustmentLine invAdjustmentLine, Date CST_DT, double CST_ADJST_QTY,
      double CST_ADJST_CST, double CST_RMNNG_QTY, double CST_RMNNG_VL, Integer AD_BRNCH,
      Integer AD_CMPNY
  ) {

    Debug.print("ArMiscReceiptEntryControllerBean postInvAdjustmentToInventory");

    LocalInvCostingHome invCostingHome = null;
    LocalAdPreferenceHome adPreferenceHome = null;
    LocalAdCompanyHome adCompanyHome = null;

    // Initialize EJB Home

    try {

      invCostingHome = (LocalInvCostingHome) EJBHomeFactory
          .lookUpLocalHome(LocalInvCostingHome.JNDI_NAME, LocalInvCostingHome.class);
      adPreferenceHome = (LocalAdPreferenceHome) EJBHomeFactory
          .lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);
      adCompanyHome = (LocalAdCompanyHome) EJBHomeFactory
          .lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);

    } catch (NamingException ex) {

      getSessionContext().setRollbackOnly();
      throw new EJBException(ex.getMessage());

    }

    try {

      LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);
      LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
      LocalInvItemLocation invItemLocation = invAdjustmentLine.getInvItemLocation();
      int CST_LN_NMBR = 0;

      CST_ADJST_QTY =
          EJBCommon.roundIt(CST_ADJST_QTY, adPreference.getPrfInvQuantityPrecisionUnit());
      CST_ADJST_CST =
          EJBCommon.roundIt(CST_ADJST_CST, adCompany.getGlFunctionalCurrency().getFcPrecision());
      CST_RMNNG_QTY =
          EJBCommon.roundIt(CST_RMNNG_QTY, adPreference.getPrfInvQuantityPrecisionUnit());
      CST_RMNNG_VL =
          EJBCommon.roundIt(CST_RMNNG_VL, adCompany.getGlFunctionalCurrency().getFcPrecision());

      if (CST_ADJST_QTY < 0) {

        invItemLocation.setIlCommittedQuantity(
            invItemLocation.getIlCommittedQuantity() - Math.abs(CST_ADJST_QTY));

      }

      // create costing

      try {

        // generate line number

        LocalInvCosting invCurrentCosting =
            invCostingHome.getByMaxCstLineNumberAndCstDateToLongAndIiNameAndLocName(
                CST_DT.getTime(), invItemLocation.getInvItem().getIiName(),
                invItemLocation.getInvLocation().getLocName(), AD_BRNCH, AD_CMPNY);
        CST_LN_NMBR = invCurrentCosting.getCstLineNumber() + 1;

      } catch (FinderException ex) {

        CST_LN_NMBR = 1;

      }

      LocalInvCosting invCosting = invCostingHome.create(CST_DT, CST_DT.getTime(), CST_LN_NMBR, 0d,
          0d, 0d, 0d, CST_ADJST_QTY, CST_ADJST_CST, 0d, 0d, CST_RMNNG_QTY, CST_RMNNG_VL, 0d, 0d,
          CST_ADJST_QTY > 0 ? CST_ADJST_QTY : 0, AD_BRNCH, AD_CMPNY);
      // invItemLocation.addInvCosting(invCosting);
      invCosting.setInvItemLocation(invItemLocation);
      invCosting.setInvAdjustmentLine(invAdjustmentLine);

      // propagate balance if necessary

      Collection invCostings = invCostingHome.findByGreaterThanCstDateAndIiNameAndLocName(CST_DT,
          invItemLocation.getInvItem().getIiName(), invItemLocation.getInvLocation().getLocName(),
          AD_BRNCH, AD_CMPNY);

      Iterator i = invCostings.iterator();

      while (i.hasNext()) {

        LocalInvCosting invPropagatedCosting = (LocalInvCosting) i.next();

        invPropagatedCosting.setCstRemainingQuantity(
            invPropagatedCosting.getCstRemainingQuantity() + CST_ADJST_QTY);
        invPropagatedCosting
            .setCstRemainingValue(invPropagatedCosting.getCstRemainingValue() + CST_ADJST_CST);

      }


    } catch (Exception ex) {

      Debug.printStackTrace(ex);
      getSessionContext().setRollbackOnly();
      throw new EJBException(ex.getMessage());

    }



  }


  /**
   * @ejb:interface-method view-type="remote"
   * @jboss:method-attributes read-only="true"
   **/
  public ArrayList getArMiscReceiptReportParameters(
      Integer AD_CMPNY
  ) {

    Debug.print("ArInvoiceEntryControllerBean getArInvoiceReportParameters");

    ArrayList list = new ArrayList();
    LocalAdLookUpValueHome adLookUpValueHome = null;


    // Initialize EJB Home

    try {

      adLookUpValueHome = (LocalAdLookUpValueHome) EJBHomeFactory
          .lookUpLocalHome(LocalAdLookUpValueHome.JNDI_NAME, LocalAdLookUpValueHome.class);

    } catch (NamingException ex) {

      throw new EJBException(ex.getMessage());

    }


    try {

      Collection adLookUpValues =
          adLookUpValueHome.findByLuName("AR PRINT INVOICE PARAMETER", AD_CMPNY);

      if (adLookUpValues.size() <= 0) {
        return list;
      }

      Iterator i = adLookUpValues.iterator();

      while (i.hasNext()) {

        LocalAdLookUpValue adLookUpValue = (LocalAdLookUpValue) i.next();
        System.out.println(adLookUpValue.getLvName());
        list.add(adLookUpValue.getLvName());
      }


      return list;



    } catch (Exception ex) {

      Debug.printStackTrace(ex);
      throw new EJBException(ex.getMessage());

    }

  }


  /**
   * @ejb:interface-method view-type="remote"
   * @jboss:method-attributes read-only="true"
   **/
  public boolean getArTraceMisc(
      String II_NAME, Integer AD_CMPNY
  ) {

    Debug.print("ArInvoiceEntryControllerBean getArTraceMisc");

    LocalInvLocationHome invLocationHome = null;
    LocalInvItemHome invItemHome = null;
    Collection invLocations = null;
    ArrayList list = new ArrayList();
    boolean isTraceMisc = false;
    // Initialize EJB Home

    try {

      invLocationHome = (LocalInvLocationHome) EJBHomeFactory
          .lookUpLocalHome(LocalInvLocationHome.JNDI_NAME, LocalInvLocationHome.class);
      invItemHome = (LocalInvItemHome) EJBHomeFactory.lookUpLocalHome(LocalInvItemHome.JNDI_NAME,
          LocalInvItemHome.class);

    } catch (NamingException ex) {

      throw new EJBException(ex.getMessage());

    }

    try {

      LocalInvItem invItem = invItemHome.findByIiName(II_NAME, AD_CMPNY);
      if (invItem.getIiTraceMisc() == 1) {
        isTraceMisc = true;
      }
      return isTraceMisc;
    } catch (Exception ex) {

      Debug.printStackTrace(ex);
      throw new EJBException(ex.getMessage());

    }

  }



  private void createInvTagList(
      LocalArInvoiceLineItem arInvoiceLineItem, ArrayList list, Integer AD_CMPNY
  ) throws Exception {

    Debug.print("ArMiscReceiptEntryController createInvTagList");

    LocalAdUserHome adUserHome = null;
    LocalInvTagHome invTagHome = null;

    // Initialize EJB Home

    try {
      adUserHome = (LocalAdUserHome) EJBHomeFactory.lookUpLocalHome(LocalAdUserHome.JNDI_NAME,
          LocalAdUserHome.class);
      invTagHome = (LocalInvTagHome) EJBHomeFactory.lookUpLocalHome(LocalInvTagHome.JNDI_NAME,
          LocalInvTagHome.class);

    } catch (NamingException ex) {

      throw new EJBException(ex.getMessage());

    }



    try {
      System.out.println("aabot?");
      // Iterator t = apPurchaseOrderLine.getInvTag().iterator();
      Iterator t = list.iterator();

      LocalInvTag invTag = null;
      System.out.println("umabot?");
      while (t.hasNext()) {
        InvModTagListDetails tgLstDetails = (InvModTagListDetails) t.next();
        System.out.println(tgLstDetails.getTgCustodian() + "<== custodian");
        System.out.println(tgLstDetails.getTgSpecs() + "<== specs");
        System.out.println(tgLstDetails.getTgPropertyCode() + "<== propertyCode");
        System.out.println(tgLstDetails.getTgExpiryDate() + "<== expiryDate");
        System.out.println(tgLstDetails.getTgSerialNumber() + "<== serial number");

        if (tgLstDetails.getTgCode() == null) {
          System.out.println("ngcreate ba?");
          invTag =
              invTagHome.create(tgLstDetails.getTgPropertyCode(), tgLstDetails.getTgSerialNumber(),
                  null, tgLstDetails.getTgExpiryDate(), tgLstDetails.getTgSpecs(), AD_CMPNY,
                  tgLstDetails.getTgTransactionDate(), tgLstDetails.getTgType());

          invTag.setArInvoiceLineItem(arInvoiceLineItem);
          invTag.setInvItemLocation(arInvoiceLineItem.getInvItemLocation());
          LocalAdUser adUser = null;
          try {
            adUser = adUserHome.findByUsrName(tgLstDetails.getTgCustodian(), AD_CMPNY);
          } catch (FinderException ex) {

          }
          invTag.setAdUser(adUser);
          System.out.println("ngcreate ba?");
        }

      }



    } catch (Exception ex) {
      throw ex;
    }
  }



  private ArrayList getInvTagList(
      LocalArInvoiceLineItem arInvoiceLineItem
  ) {

    ArrayList list = new ArrayList();

    Collection invTags = arInvoiceLineItem.getInvTags();
    Iterator x = invTags.iterator();
    while (x.hasNext()) {
      LocalInvTag invTag = (LocalInvTag) x.next();
      InvModTagListDetails tgLstDetails = new InvModTagListDetails();
      tgLstDetails.setTgPropertyCode(invTag.getTgPropertyCode());
      tgLstDetails.setTgSpecs(invTag.getTgSpecs());
      tgLstDetails.setTgExpiryDate(invTag.getTgExpiryDate());
      tgLstDetails.setTgSerialNumber(invTag.getTgSerialNumber());
      try {

        tgLstDetails.setTgCustodian(invTag.getAdUser().getUsrName());
      } catch (Exception ex) {
        tgLstDetails.setTgCustodian("");
      }

      list.add(tgLstDetails);

      System.out
          .println(tgLstDetails.getTgPropertyCode() + "<== property code inside controllerbean ");
      System.out.println(tgLstDetails.getTgSpecs() + "<== specs inside controllerbean ");
      System.out.println(list + "<== taglist inside controllerbean ");

    }

    return list;

  }


  /**
   * @ejb:interface-method view-type="remote"
   * @jboss:method-attributes read-only="true"
   **/
  public ArrayList getDocumentTypeList(
      String DCMNT_TYP, Integer AD_CMPNY
  ) {

    Debug.print("ArMiscReceiptEntryControllerBean getDocumentTypeList");

    ArrayList list = new ArrayList();
    LocalAdLookUpValueHome adLookUpValueHome = null;


    // Initialize EJB Home

    try {

      adLookUpValueHome = (LocalAdLookUpValueHome) EJBHomeFactory
          .lookUpLocalHome(LocalAdLookUpValueHome.JNDI_NAME, LocalAdLookUpValueHome.class);

    } catch (NamingException ex) {

      throw new EJBException(ex.getMessage());

    }


    try {

      Collection adLookUpValues =
          adLookUpValueHome.findByLuName("AR RECEIPT DOCUMENT TYPE", AD_CMPNY);

      if (adLookUpValues.size() <= 0) {
        return list;
      }

      Iterator i = adLookUpValues.iterator();

      while (i.hasNext()) {

        LocalAdLookUpValue adLookUpValue = (LocalAdLookUpValue) i.next();
        System.out.println(adLookUpValue.getLvName());
        list.add(adLookUpValue.getLvName());
      }


      return list;



    } catch (Exception ex) {

      Debug.printStackTrace(ex);
      throw new EJBException(ex.getMessage());

    }

  }


  // SessionBean methods

  /**
   * @ejb:create-method view-type="remote"
   **/
  public void ejbCreate(
  ) throws CreateException {

    Debug.print("ArMiscReceiptEntryControllerBean ejbCreate");

  }

}
